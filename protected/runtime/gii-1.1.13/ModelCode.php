<?php
return array (
  'template' => 'compact',
  'connectionId' => 'db',
  'tablePrefix' => 'gas',
  'modelPath' => 'application.models',
  'baseClass' => 'CActiveRecord',
  'buildRelations' => '1',
);
