<?php
class User_profileController extends MemberController
{
	
	
	/**
	 * Declares class-based actions.
	 */
	
	public function actions()
	{
		return array(
				// captcha action renders the CAPTCHA image displayed on the contact page
				'captcha'=>array(
						'class'=>'CCaptchaAction',
						'backColor'=>0xFFFFFF,
				),
				// page action renders "static" pages stored under 'protected/views/site/pages'
				// They can be accessed via: index.php?r=site/page&view=FileName
				'page'=>array(
						'class'=>'CViewAction',
				),
		);
	}
	
	/**
	 * This is the action update profile member
	 */	
	public function actionUpdateProfile()
	{
		
		//$id = Yii::app()->user->id;
 		$id = Yii::app()->user->id=3;

		$model = Users::model()->findByPk($id);

  		if(isset($_POST['Users'])) 		{
  			$model->attributes=$_POST['Users'];
  			
  			if($model->gender !='FEMALE' && $model->gender !='MALE') $model->gender ='CHECK GENDER';
  			
  			if($model->save());
  				//$this->redirect(array('/member/user_profile/viewProfile'));
  		}
	
 		$this->render('updateProfile',array(
 				'model'=>$model,
 		));
	}

	public function actionchange_password()
	{
		//$this->setPageTitle('65doctor - change password');
		$model = Users::model()->findByPk(Yii::app()->user->id);
		if(!isset(Yii::app()->user->id))
			Yii::app()->user->loginRequired();
		if(isset($_POST['Users']))
		{
			if($model->password_hash != md5($_POST['Users']['password']))
				$model->addError('password','Old password isn´t correct.');
			else if(trim($_POST['Users']['new_password'])< 6)
				$model->addError('new_password','Password at least 6 characters');
			else if($_POST['Users']['new_password'] != $_POST['Users']['new_password_repeat'])
				$model->addError('new_password_repeat','Confirm new password does not match');
			else{
				$model->password_hash = md5(trim($_POST['Users']['new_password']));
				$model->temp_password = ($_POST['Users']['new_password']);
				if($model->update())
					Yii::app()->user->setFlash('success', "Password has changed!");
			}
		}
	
		$this->render('change_password', array(
				'model'=>$model,
				'passw'=>$_POST['Users']['password'],
		));
	}
		
	/**
	 * This is the action to handle external exceptions.
	 */
	public function actionError()
	{
		if($error=Yii::app()->errorHandler->error)
		{
			if(Yii::app()->request->isAjaxRequest)
				echo $error['message'];
			else
				$this->render('error', $error);
		}
	}
	
	
	
	
}