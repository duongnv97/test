<?php

class ManageadminController extends AdminController
{
	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return MainController::checkControllerAccessRules('manageadmin',BE);
	} 

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new Users('create');
		$model->scenario = 'createAdmin';
		// Uncomment the following line if AJAX validation is needed
		 $this->performAjaxValidation($model);

		if(isset($_POST['Users']))
		{
			$model->attributes=$_POST['Users'];
            $model->temp_password = $model->password_hash;
            $model->password_hash = md5($model->password_hash);
            $model->password_confirm = md5($model->password_confirm);
            $model->created_date = date("Y-m-d H:i:s");
            $model->application_id = 1; //save user for back end
			if($model->save())
            {
				$this->redirect(array('view','id'=>$model->id));
            }
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);
		$model->scenario = 'editAdmin';
		// Uncomment the following line if AJAX validation is needed
		$this->performAjaxValidation($model);

		if(isset($_POST['Users']))
		{
			$old_password = $model->password_hash;
                        //var_dump($model->password_hash);
            $model->attributes=$_POST['Users'];
            
            //var_dump($model->password_hash);die;
            if(empty($model->password_hash) && empty($model->password_confirm)){
                $model->password_hash = $old_password;
                $model->password_confirm = $old_password;
                
            } else{
                $model->temp_password = $model->password_hash;
                $model->password_hash = md5($model->password_hash);
                $model->password_confirm = md5($model->password_confirm);
            }
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}
	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		if(Yii::app()->request->isPostRequest)
		{
         	if ($id != 2)
				$this->loadModel($id)->delete();

			// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
			if(!isset($_GET['ajax']))
				$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('index'));
		}
		else
			throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
	}

	/**
	 * Manages all models.
	 */
	public function actionIndex()
	{
		$model=new Users('searchAdmin');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Users']))
			$model->attributes=$_GET['Users']; 
                $model->role_id = Roles::loadItemsBeJustId();                
		$this->render('index',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the ID of the model to be loaded
	 */
	public function loadModel($id)
	{
		$model=Users::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='admin-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
        
        public function actionupdate_my_profile()
	{
	    if(Yii::app()->user->id == '')
             $this->redirect(array('login'));

		$model=$this->loadModel(Yii::app()->user->id);
                $model->scenario = 'updateMyProfile';
                //$model->md5pass = $model->password_hash;

		if(isset($_POST['Users']))
		{
                    $model->attributes=$_POST['Users'];
                    if($model->validate())
                    {
                        if($model->save()) {
                            Yii::app()->user->setFlash('successUpdateMyProfile', "Your profile information has been successfully updated.");
                            $this->redirect(array('manageadmin/update_my_profile'));
                        }
                    }
		}

		$this->render('update_my_profile',array(
			'model'=>$model,
		));
	}

        public function actionchange_my_password()
	{
	    if(Yii::app()->user->id == '')
             $this->redirect(array('login'));

		$model=$this->loadModel(Yii::app()->user->id);
                $model->scenario = 'changeMyPassword';
                $model->md5pass = $model->password_hash;

		if(isset($_POST['Users']))
		{
                    $model->attributes=$_POST['Users'];
                    if($model->validate())
                    {
                        $model->password_hash = md5($model->newpassword);
                        $model->temp_password = $model->newpassword;
                        if($model->save()) {
                            Yii::app()->user->setFlash('successChangeMyPassword', "Your password has been successfully changed.");
                            $this->redirect(array('manageadmin/change_my_password'));
                        }
                    }
		}

		$this->render('change_my_password',array(
			'model'=>$model,
		));
	}        
        
}
