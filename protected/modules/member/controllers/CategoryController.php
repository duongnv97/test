<?php
/**
 * VerzDesignCMS
 *
 * LICENSE
 *
 * @copyright	Copyright (c) 2012 Verz Design (http://www.verzdesign.com)
 * @version 	$Id: CategoryController.php
 * @author  duytoan
 */
class CategoryController extends MemberController{

	public function actionview_category_info($slug){
		  //get id	
          $id = Categories::getCategoryId($slug);
          $model = CategoriesPosts::model()->findAll('category_id='.$id);
          
          $arrid= array();
          foreach($model as $v){
          	$arrid[]=$v->post_id;
          }

          $arrid = array_unique($arrid);
          $arrid = implode(',', $arrid);
          $model = Posts::model()->findAll('id IN(' . trim($arrid) .')');
       
          if(count($model)>0){
            $this->render('view_category_info', array(
                 'model'=>$model,
            	 'cat' =>$slug,	
            		));             
          }else
            $this->redirect(Yii::app()->createAbsoluteUrl('/'));		
	}

}