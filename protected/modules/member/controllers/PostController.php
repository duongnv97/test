<?php
/**
 * VerzDesignCMS
 *
 * LICENSE
 *
 * @copyright	Copyright (c) 2012 Verz Design (http://www.verzdesign.com)
 * @version 	$Id: Postcontroller.php
 * @author  duytoan
 */

class PostController extends MemberController{
	

	/*
	 * Show all post
	 */
	public function actionListpost($cat){
		$idcat = Categories::getCategoryId($cat);
		$model =  Posts::model()->with('categoriesPost')->findAll('status=1  and categoriesPost.category_id='.$idcat);
        if(count($model)>0){
        	$this->render('listpost', array(
        			'model'=>$model,
        			'cat'=>$cat,	
        			));
        }else
        	$this->redirect(Yii::app()->createAbsoluteUrl('/'));	
	}	
	
	
	/*
	 * Show detail a post
	*/	
	public function actionDetail_post($slug){

        $model =  Posts::model()->findAll('slug="'. Verz_filter::string($slug) .'"');

         if(count($model)>0){
            $this->render('detail_post', array(
                'model'=>$model));             
         }else
            $this->redirect(Yii::app()->createAbsoluteUrl('/'));	
	}

}