<?php
class TagController extends MemberController{
	

	
	public function actionview_tag_info($name){
		
         $id    = Tag::getTagId($name);
         $model = TagsPosts::model()->findAll('tag_id='.$id);
         $arrid= array();
         
         foreach($model as $v){
         	$arrid[]=$v->post_id;
         }
         
         $arrid = implode(',', $arrid);

         $model = Posts::model()->findAll('id IN(' . trim($arrid) .')');

         if(count($model)>0){
            $this->render('view_tag_info', array(
                'model'=>$model));             
         }else
            $this->redirect(Yii::app()->createAbsoluteUrl('/'));	
				
	}

}