<div id="tab_right_top">

    <div id="tab_right_title">
        <?php
        if (Yii::app()->user->isMember) {
                $this->widget('zii.widgets.CMenu',array(
					'encodeLabel'=>false,
                    'items'=>array(
						array('label'=>'<img src="'.Yii::app()->theme->baseUrl.'/images/lock_unlock.png">', 'url'=>array('/member/changePassword'),'itemOptions'=>array('title'=>Yii::t('translation','site.change_password.Change_password'))),
                        array('label'=>'<img src="'.Yii::app()->theme->baseUrl.'/images/user_blue_edit.png">', 'url'=>array('/member/updateProfile'), 'itemOptions'=>array('title'=>Yii::t('translation','site.viewProfile.Edit_Profile'))),				
                    ),
                )); 
            }
        ?>

        <?php  echo Yii::t('translation','site.updateProfile.My_profile');?>
    </div>

    <?php 
	if(Yii::app()->user->isClinic) {
		$this->widget('zii.widgets.CDetailView', array(
			'data'=>$model,
			'attributes'=>array(
				'username',
				'email',
				
				'contact_name',
				'address',
//	            'nric',
//	            'mcr',
 				array(
                    'name'=>'contact_number',
                    'value'=>(!empty($model->area_code_id)?$model->areaCodeRelation->area_code:"+").$model->phone,
                ),
      
				'post_code',
				'zone',
			),
		)); 
	}
	
	if(Yii::app()->user->isDoctor) {
		$this->widget('zii.widgets.CDetailView', array(
			'data'=>$model,
			'attributes'=>array(
				'username',
				'email',
				
//				'contact_name',
//				'address',
	            'nric',
	            'mcr',
				array(
                    'name'=>'contact_number',
                    'value'=>(!empty($model->area_code_id)?$model->areaCodeRelation->area_code:"+").$model->phone,
                ),         
//				'post_code',
//				'zone',
			),
		)); 
	}
	
	

    ?>
</div>