<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'users-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'email'); ?>
		<?php echo $form->textField($model,'email',array('size'=>45,'maxlength'=>250)); ?>
		<?php echo $form->error($model,'email'); ?>
	</div>

	<div class="clr"></div>
    <div class="row">
        <?php echo $form->labelEx($model,'first_name', array('label'=>'Full Name')); ?>
        <?php echo $form->textField($model,'first_name',array('size'=>20,'maxlength'=>50,'placeholder'=>'First Name')); ?>
        <?php echo $form->textField($model,'last_name',array('size'=>20,'maxlength'=>50,'placeholder'=>'Last Name')); ?>
        <?php echo $form->error($model,'first_name'); ?>
        <?php echo $form->error($model,'last_name'); ?>
    </div>
    <div class="clr"></div>

    <div class="row">
        <?php echo $form->labelEx($model,'gender'); ?>
        <?php echo $form->dropDownList($model,'gender',Users::$gender); ?>
        <?php echo $form->error($model,'gender'); ?>
    </div>
	<div class="clr"></div>

    <div class="row">
        <?php echo $form->labelEx($model,'phone'); ?>
        <?php echo $form->textField($model,'phone',array('size'=>20,'maxlength'=>20)); ?>
        <?php echo $form->error($model,'phone'); ?>
    </div>

    <div class="clr"></div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->

