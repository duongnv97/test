<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/passwordStrengthMeter.js"></script>

<div class="form">
    <section>
    <h1 class="title"><strong>Change password</strong></h1>
        <div class="content" >
            <?php $form=$this->beginWidget('CActiveForm', array(
                    'id'=>'register-form',
            )); ?>
            <style>
                .group-patient ._l{
                    float: left;
                    width: 150px;
                    font-weight: bold;

                }
                .group-patient ._r{
                    float: left;
                    font-weight: bold;
                }
            </style>        

                <div class="group-patient" style="width:600px; float: left;padding-left: 20px;">
                    <div class="not-member" style="display: block;">
                        <?php 
                                foreach(Yii::app()->user->getFlashes() as $key => $message) {
                                echo '<div class="flash-' . $key . '">' . $message . "</div>\n";
                        }?>

                        <div class="row">
                            <?php echo $form->labelEx($model,'password',array('label'=>'Current password','style'=>'width:150px;')); ?>
                            <?php echo $form->passwordField($model,'password',array('size'=>28,'maxlength'=>50, 'value'=>'')); ?>
                            <?php echo $form->error($model,'password',array('style'=>'padding-left: 150px;')); ?>
                        </div>

                        <div class="row">
                            <?php echo $form->labelEx($model,'new_password',array('label'=>'New password','style'=>'width:150px;')); ?>
                            <?php echo $form->passwordField($model,'new_password',array('size'=>28,'maxlength'=>50, 'value'=>'')); ?>
                            <div class="ajaxValidateSuccess" style="display: none;"></div>
                            <div style="color:green;float: left;width: 100%;padding-left: 150px;" id='result'></div> 
                            <?php echo $form->error($model,'new_password',array('style'=>'padding-left: 150px;')); ?>
                            <div class="errorMessage" style="display:none;padding-left: 150px;">Password is required</div>
                            
                        </div>

                        <div class="row">
                            <?php echo $form->labelEx($model,'new_password_repeat',array('label'=>'Confirm new password','style'=>'width:150px;')); ?>
                            <?php echo $form->passwordField($model,'new_password_repeat',array('size'=>28,'maxlength'=>50, 'value'=>'')); ?>
                            <div class="ajaxValidateSuccess" style="display: none;"></div>
                            <?php echo $form->error($model,'new_password_repeat',array('style'=>'padding-left: 150px;')); ?>
                            <div class="errorMessage" style="display:none;padding-left: 150px;">Password is required</div>
                        </div>

                    </div>
                    <div class="row buttons">
                        <span class="edit">
                            <input type="button" id="btn_submit" value="Change"/>
                        </span>
                        <span class="edit"><input type="button" value="Cancel" onclick="location.href='<?php echo Yii::app()->createAbsoluteUrl('member/users/profile') ?>'"></span>
                    </div>
                </div>
            <div class="clear"></div>         

            <?php $this->endWidget(); ?>
            </div>
    </section>
</div>

<script type="text/javascript">
    $(document).ready(function(){
        $('#Users_new_password').keyup(function(){$('#result').html(passwordStrength($('#Users_new_password').val(),''))})
        
        
        $('#Users_new_password').bind('blur keyup', function(){
        $(this).parent('div').find('.errorMessage').hide();
        $(this).parent('div').find('.ajaxValidateSuccess').hide();

        if($(this).val().length==0){
            $(this).parent('div').find('.errorMessage').show().text('Password is required');
        }
        else if($(this).val().length<6){
            $(this).parent('div').find('.errorMessage').show().text('Password at least 6 characters');
        }
        else if($(this).val().length>5){
            $(this).parent('div').find('.errorMessage').hide();
            $(this).parent('div').find('.ajaxValidateSuccess').show();
            $('#Users_new_password_repeat').trigger('blur');
        }
        
    });
            
    $('#Users_new_password_repeat').bind('blur keyup', function(){
        $(this).parent('div').find('.errorMessage').hide();
        $(this).parent('div').find('.ajaxValidateSuccess').hide();
        
        var passw = $('#Users_new_password').val();
        var confirmPassw = $(this).val();
        if(passw!=confirmPassw || $('#Users_new_password').val().length==0 ){
            $(this).parent('div').find('.errorMessage').show().text('Confirm password does not match');
            $(this).parent('div').find('.ajaxValidateSuccess').hide();
        }else{
            $(this).parent('div').find('.errorMessage').hide();
            $(this).parent('div').find('.ajaxValidateSuccess').show();
        }
    });        
        
    $('#Users_password').unbind('blur keyup').bind('blur keyup', function(){        
        $(this).parent('div').find('.errorMessage').hide();
    });        
    
        
   $('#btn_submit').unbind("click").click(function(){
       $('#Users_new_password').trigger('blur');
        var checkE=true;
        $('.errorMessage').each(function(){
            if($(this).css('display')=='block')
                checkE=false; 
        });
        
        if(checkE) $('#register-form').submit();
//        $('#register-form').unbind("submit").bind('submit',function(){
//            alert(checkE);
//              return checkE;
//        });
            
    });    
    
    
    
    });
</script>
<style>

div.form input[type=text],
div.form textarea,
div.form select
{
	margin: 0.2em 0 0.5em 0;
	border: #CDCDCD solid 1px;
	padding: 3px;
}

div.form fieldset
{
	border: 1px solid #DDD;
	padding: 10px;
	margin: 0 0 10px 0;
    -moz-border-radius:7px;
}

div.form .row label
{
	font-weight: bold;
	font-size: 0.9em;
	float:left;
	width: 110px;
}

div.form .row_align label
{
	font-weight: bold;
	font-size: 0.9em;
	display: block;
	float:none;
	display: inline-block;
}


div.form .row
{
	margin: 5px 0;
}

div.form .col
{
	with: 45%;
	float: left;
	margin-right: 4% !important
}

div.form .row_align
{
	padding: 0px 0px 0px 110px;
}


div.form .hint
{
	margin: 0;
	padding: 0;
	color: #999;
}

div.form .note
{
	font-style: italic;
}

div.form .center{text-align: center}

div.form span.required
{
	color: red;
}

div.form div.error label:first-child,
div.form label.error,
div.form span.error
{
	color: #C00;
}

div.form div.error input,
div.form div.error textarea,
div.form div.error select,
div.form input.error,
div.form textarea.error,
div.form select.error
{
	background: #FEE;
	border-color: #C00;
}

div.form div.success input,
div.form div.success textarea,
div.form div.success select,
div.form input.success,
div.form textarea.success,
div.form select.success
{
	background: #E6EFC2;
	border-color: #C6D880;
}


div.form .errorSummary
{
	border: 2px solid #C00;
	padding: 7px 7px 12px 7px;
	margin: 0 0 20px 0;
	background: #FEE;
	font-size: 0.9em;
}

div.form .errorMessage
{
	color: red;
	font-size: 0.9em;
}

div.form .errorSummary p
{
	margin: 0;
	padding: 5px;
}

div.form .errorSummary ul
{
	margin: 0;
	padding: 0 0 0 20px;
}

div.wide.form label
{
	float: left;
	margin-right: 10px;
	position: relative;
	text-align: right;
	width: 150px;
}

div.wide.form .row
{
	clear: left;
}


div.wide.form .buttons, div.wide.form .hint, div.wide.form .errorMessage
{
	clear: left;
	padding-left: 110px;
}

div.form .buttons
{

	padding-left: 110px;
}

.buttons .btn-submit input { margin-left:50px; background:url(../images/bg-btn.png) repeat-x; }
.form select {width:148px;}
.form .btn-active select {width:140px !important;}
.search-form select {width:192px;}
.form .cmb-active select {width:90px;}
.form .btn-submit input {margin-left: 48px;}

.form .image-watermark {
	width:250px;
	padding:5px;
	float:left;
}

div.form input[type= submit],
div.form input[type= button],
div.form input[type= reset],
div.form input[type= file]{
	min-width: 80px;
	min-height: 25px;
	padding: 0 5px 0 5px;
}

div.flash-success
{
    background: none repeat scroll 0 0 #E6EFC2;
    border-color: #C6D880;
    color: green;
    font-size: 15px;
    font-weight: bold;
    margin-bottom: 20px;
    padding-left: 150px;}
   
</style>