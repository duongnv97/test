<section>
        <h1 class="title"><strong><?php echo $model->title;?></strong></h1>
    <div class="content document">
        <?php if(!empty($model->banner)) echo CHtml::image(Yii::app()->baseUrl.'/upload/cms/banner/'.$model->banner,'',array('class'=>'left','width'=>189,'height'=>155));?>
        <?php
              if($model)
                 echo $model->cms_content;
        ?>
    
        
        <?php $modelCms = Cms::model()->findAll('creator_id="'.$model->creator_id.'"'); ?>
        <br/><br/>
        <?php if(count($modelCms)>0):?>
            <p style="font-weight: bold;font-size: 13px;color: #1B98BE;">Articles of <?php echo $model->creator->first_name.' '.$model->creator->last_name;?> </p>
            <ul class="list">
                <?php foreach($modelCms as $m) :?>
                    <li><a href="<?php echo Yii::app()->createAbsoluteUrl('articles/'.$m->slug); ?>"><?php echo $m->title;?></a></li>
                <?php endforeach; ?>
            </ul>
        <?php endif;?>
    </div>    
</section>
<?php //Yii::app()->clientScript->registerCoreScript('jquery'); ?>
<script type="text/javascript">
$(document).ready(function(){
});
</script>

