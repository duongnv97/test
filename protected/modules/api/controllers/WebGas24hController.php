<?php
/**
 * @Author: ANH DUNG Jan 29, 2018
 * @Todo: Handle some action app Gas24h
 */
class WebGas24hController extends ApiController
{
    /**
     * @Author: ANH DUNG Mar 02, 2018
     * @Todo: save info employee from gas24h.com.vn
     * @Resource: webGas24h/addCandidate
     * * @ex_json_request: {"token":"c3f3832926eeb9a2410ff7bf8d93b944","page":"0"}
     */
    public function actionAddCandidate(){
        try {
            Logger::WriteLog("Data Web gas24h: ".json_encode($_POST));
            $q = json_decode($_POST['q'], true);
            $mWebGas24h = new WebGas24h();
            $mWebGas24h->handleCandidate($q);
            $result = ApiModule::$defaultSuccessResponse;
            $result['message'] = 'ok';
            ApiModule::sendResponse($result, $this);
        } catch (Exception $ex) {
            MyFormat::ApiCatchError($ex, $this);
        }
    }

}
