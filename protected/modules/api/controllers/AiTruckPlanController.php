<?php
/**
 * @Author: DungNT May 22, 2019
 * @Todo: Handle action TruckPlan
 */
class AiTruckPlanController extends ApiController
{
    /** @Author: DungNT May 22, 2019
     * @Todo: render list lịch nhận hàng của xe tải
     * @Resource: aiTruckPlan/truckImportList
     * * @ex_json_request: {"token":"b0ba131a5ee86324cac63ce6b84ef667","page":"0", "customer_id":"","date_from":"01-05-2019","date_to":"01-06-2019"}
     */
    public function actionTruckImportList(){
        try {
            $result = ApiModule::$defaultResponse;
            $this->checkRequest();
            $q = json_decode($_POST['q']);
            $this->checkRequiredParams($q, array('token', 'page'));
            $mUser = $this->getUserByToken($result, $q->token);// Oct 12, 2015 for check inactive or delete User
            $this->mUserApp = $mUser;
            $result = ApiModule::$defaultSuccessResponse;
            $mTruckPlanDetail = new TruckPlanDetail();
            $mTruckPlanDetail->type_order         = TruckPlan::TYPE_ORDER_IMPORT;
            $mTruckPlanDetail->appAction          = GasConst::ACTION_LIST;
            $mTruckPlanDetail->mAppUserLogin      = $mUser;
            $mTruckPlanDetail->handleApiList($result, $q);
            ApiModule::sendResponse($result, $this);
        } catch (Exception $ex) {
            MyFormat::ApiCatchError($ex, $this);
        }
    }
    
    /** @Author: DungNT May 22, 2019
     * @Todo: render list lịch giao hàng của xe tải
     * @Resource: aiTruckPlan/truckExportList
     * * @ex_json_request: {"token":"8bd307c75295594eebce9639dc898292","page":"0", "code_no":""}
     */
    public function actionTruckExportList(){
        try {
            $result = ApiModule::$defaultResponse;
            $this->checkRequest();
            $q = json_decode($_POST['q']);
            $this->checkRequiredParams($q, array('token', 'page'));
            $mUser = $this->getUserByToken($result, $q->token);// Oct 12, 2015 for check inactive or delete User
            $this->mUserApp = $mUser;
            $result = ApiModule::$defaultSuccessResponse;
            $mTruckPlanDetail = new TruckPlanDetail();
            $mTruckPlanDetail->type_order           = TruckPlan::TYPE_ORDER_EXPORT;
            $mTruckPlanDetail->appAction            = GasConst::ACTION_LIST;
            $mTruckPlanDetail->mAppUserLogin        = $mUser;
            $mTruckPlanDetail->agent_id             = $this->getAgentOfEmployee($mUser);
            $mTruckPlanDetail->handleApiList($result, $q);
            ApiModule::sendResponse($result, $this);
        } catch (Exception $ex) {
            MyFormat::ApiCatchError($ex, $this);
        }
    }

    
    /** 
     * @Author: DungNT May 22, 2019
     * @Todo: android action Nhận Hủy order xe tải của tài xế
     * @Resource: aiTruckPlan/truckPlanSetEvent
     * @ex_json_request: {"token":"b0ba131a5ee86324cac63ce6b84ef667","truck_plan_detail_id":"1", "action_type":1, "latitude":"10.6615461","longitude":"106.7742044","qty_real":"15000","driver_note":"tai xe test note" }
     * có 2 action sử dụng api này:
     * 1. GN nhận ĐH
     * 2. GN hủy nhận ĐH
     */    
    public function actionTruckPlanSetEvent(){
        try{
        $result = ApiModule::$defaultResponse;
        $this->checkRequest();
        $q = json_decode($_POST['q']);
        $this->checkRequiredParams($q, array('token', 'truck_plan_detail_id','action_type', 'latitude', 'longitude'));
        $this->validateLocationGps($q);
        $mUser      = $this->getUserByToken($result, $q->token);// Oct 12, 2015 for check inactive or delete User
        $mTruckPlanDetail = $this->loadModel($q->truck_plan_detail_id);
        $mTruckPlanDetail->mAppUserLogin = $mUser;
        $this->setEventValidateFile($q, $mTruckPlanDetail);
//        $mTruckPlanDetail->list_id_image = !empty($q->list_id_image) ? $q->list_id_image : []; Jun2219 DungNT ko cho xóa file dưới local
        $mTruckPlanDetail->list_id_image = [];
        $mTruckPlanDetail->handleSetEvent($q);
        if($mTruckPlanDetail->hasErrors()){
            $result['message'] = HandleLabel::FortmatErrorsModel($mTruckPlanDetail->getErrors());
            ApiModule::sendResponse($result, $this);
        }
        $mTruckPlanDetail->handleAppSaveFile(GasFile::TYPE_19_TRUCK_PLAN);
        $result = ApiModule::$defaultSuccessResponse;
        $result['message'] = 'ok';
        ApiModule::sendResponse($result, $this);
        } catch (Exception $ex) {
            MyFormat::ApiCatchError($ex, $this);
        }
    }
    
    /** @Author: DungNT May 24, 2019
     *  @Todo: validate file for some event
     **/
    public function setEventValidateFile($q, &$mTruckPlanDetail) {
        if($q->action_type != AppConst::EVENT_ORDER_COMPLETE){
            return ;
        }
        $mTruckPlanDetail->requiredFileUpload = true;
        if(count($mTruckPlanDetail->rFile) > 0){
            $mTruckPlanDetail->requiredFileUpload = false;
        }
        $mTruckPlanDetail->handleAppFileValidate();
        $this->responseIfError($mTruckPlanDetail);
    }
    
    /**
     * @Author: DungNT May 22, 2019
     * @Todo: TruckPlan view
     * @Resource: aiTruckPlan/truckPlanView
     * @ex_json_request: {"token":"403acbe7932b3c7f2f3ce5a906757c29","truck_plan_detail_id":"1"}
     */
    public function actionTruckPlanView()
    {
        try{
            $result = ApiModule::$defaultResponse;
            $this->checkRequest();
            $q = json_decode($_POST['q']);
            $this->checkRequiredParams($q, array('token', 'truck_plan_detail_id'));
            $mUser      = $this->getUserByToken($result, $q->token);
            $mTruckPlanDetail = $this->loadModel($q->truck_plan_detail_id);
//            $mAppOrder = GasAppOrder::model()->findByPk(1);// only dev test
            $mTruckPlanDetail->mAppUserLogin    = $mUser;
            $mTruckPlanDetail->appAction        = GasConst::ACTION_VIEW;
            $result                             = ApiModule::$defaultSuccessResponse;
            $result['message']                  = '';
            $result['record']                   = $mTruckPlanDetail->appView();
            
            ApiModule::sendResponse($result, $this);
        } catch (Exception $ex) {
            MyFormat::ApiCatchError($ex, $this);
        }
    }
    
    /** @Author: HOANG NAM 26/06/2018
     *  @Todo: load model
     **/
    public function loadModel($id) {
        try {
            $model = TruckPlanDetail::model()->findByPk($id);
            if ($model === null) {
                Yii::log("The requested page does not exist.");
                throw new CHttpException(404, 'The requested page does not exist.');
            }
            return $model;
        } catch (Exception $exc) {
            GasCheck::CatchAllExeptiong($exc);
        }
    }

}
