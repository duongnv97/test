<?php

class Customer_bk_Oct0519Controller extends ApiController
{
    public $mUserLogin = null, $mSpjCode = null, $mCustomer = null, $mFixAddress = null;
    /**
     * @Author: DungNT Aug 31, 2016
     * @Todo: android create customer hgd
     * @Resource: customer/hgdCreate
     * @ex_json_request: {"token":"403acbe7932b3c7f2f3ce5a906757c29"}
     */
    public function actionHgdCreate(){
    try {
        $result = ApiModule::$defaultResponse;
        $this->checkRequest();
        $q = json_decode($_POST['q']);
        $this->checkRequiredParams($q, array('platform','token','agent_id', 'phone', 'first_name',
            'province_id', 'district_id', 'ward_id', 'house_numbers', 'street_id', 'serial', 'latitude', 'longitude'
        ));
        $this->checkPlatformCancle($q);
        $mUser      = $this->getUserByToken($result, $q->token);// Oct 12, 2015 for check inactive or delete User
        $this->mUserLogin = $mUser;
        $mCustomer  = new Users('AndroidCreateHgd');
        $mCustomer->mUsersRef = new UsersRef();
        $this->hgdCreateGetPost($mUser, $result, $q, $mCustomer);
        $result = ApiModule::$defaultSuccessResponse;
        $result['message'] = 'Tạo mới khách hàng thành công';
        MyFunctionCustom::saveCustomerStoreCard($mCustomer);
        $mCustomer->mUsersRef->user_id = $mCustomer->id;
        $mCustomer->SaveUsersRef();// Sep 29, 2015
        // May 13, 2016 update Phone KH
        $this->doUpdateOtherTable($mUser, $mCustomer);
        $this->mCustomer    = $mCustomer;
        $this->updateCustomerIdToSpjCode();
        $this->doSaveNewTableHgdPttt($mUser, $mCustomer);
//        $this->savePhoneHgd($mUser, $mCustomer);// Sep3019 Moveto doUpdateOtherTable
//        $this->saveFixAddress($mCustomer);// Sep3019 Moveto doUpdateOtherTable
//        $this->addPhoneHgdSmsPoint($mCustomer, $mUser);// Sep3019 Moveto doUpdateOtherTable
        $result['id']       = $mCustomer->id."";
        ApiModule::sendResponse($result, $this);
    } catch (Exception $ex) {
        MyFormat::ApiCatchError($ex, $this);
    }
    }
    
    /**
     * @Author: DungNT Aug 31, 2016
     * @Todo: android create customer hgd
     * @Resource: customer/hgdUpdate
     * @ex_json_request: {"token":"403acbe7932b3c7f2f3ce5a906757c29", "customer_id":123}
     */
    public function actionHgdUpdate(){
    try {
        $result = ApiModule::$defaultResponse;
        $this->checkRequest();
        $q = json_decode($_POST['q']);
        $this->checkRequiredParams($q, array('token', 'customer_id','agent_id', 'phone', 'first_name',
            'province_id', 'district_id', 'ward_id', 'house_numbers', 'street_id', 'serial', 'latitude', 'longitude'
        ));
        $mUser      = $this->getUserByToken($result, $q->token);// Oct 12, 2015 for check inactive or delete User
        $this->mUserLogin = $mUser;
        $mCustomer  = $this->getHgdCustomer($mUser, $q, $result);
        $this->checkAllowUpdate($mCustomer);

        $this->removeSpjCodeOld($mCustomer);
        $this->hgdCreateGetPost($mUser, $result, $q, $mCustomer);
        $result = ApiModule::$defaultSuccessResponse;
        $result['message'] = 'Cập nhật khách hàng thành công';
        $mCustomer->update();
        if($this->isAllowUpdate($mUser) && $mCustomer->is_maintain == STORE_CARD_HGD_CCS){
            $mCustomer->SaveUsersRef();// Sep 29, 2015
            $this->mCustomer    = $mCustomer;
            $this->updateCustomerIdToSpjCode();
        }
//        $this->savePhoneHgd($mUser, $mCustomer);// May 13, 2016 update Phone KH
//        $this->saveFixAddress($mCustomer);
//        $this->addPhoneHgdSmsPoint($mCustomer, $mUser);
        $this->doUpdateOtherTable($mUser, $mCustomer);
        
        $result['id'] = $mCustomer->id."";
        ApiModule::sendResponse($result, $this);
    } catch (Exception $ex) {
        MyFormat::ApiCatchError($ex, $this);
    }
    }
    
    /** @Author: DungNT Sep 29, 2019
     *  @Todo: update to other table
     **/
    public function doUpdateOtherTable($mUserLogin, $mCustomer) {
        $this->saveFixAddress($mCustomer);
        // May 13, 2016 update Phone KH
        $this->savePhoneHgd($mUserLogin, $mCustomer);
        $this->addPhoneHgdSmsPoint($mCustomer, $mUserLogin);
    }
    
    /** @Author: DungNT Sep 29, 2019
     *  @Todo: save to table gas_users_hgd_pttt
     **/
    public function doSaveNewTableHgdPttt($mUserLogin, $mCustomer) {
        $mUsersHgdPttt = new UsersHgdPttt();
        $mUsersHgdPttt->appAdd($mCustomer);
    }

    /** @Author: DungNT Jul 22, 2017
     * @Todo: save phone customer CCS - cho số phone của CCS vào hệ thống luôn, những số nào hợp lệ và chưa tồn tại thì cho vào
     */
    public function savePhoneHgd($mUser, $mCustomer) {
        $mCustomer->solrAdd();// Jan0819 move to this place
        if(empty($mCustomer->phone) || $mUser->role_id == ROLE_SALE || $mCustomer->is_maintain != STORE_CARD_HGD_CCS){
            return ;
        }
        if($this->mUserLogin->role_id == ROLE_EMPLOYEE_MAINTAIN){
            return ;// chỗ này là PVKH cập nhật đ/c KH ở đơn hàng, không update phone
        }
        $mUserPhone = new UsersPhone();
        $mUserPhone->phone          = $mCustomer->phone;
        $mUserPhone->role_id        = ROLE_CUSTOMER;
        $mUserPhone->is_maintain    = CmsFormatter::$aTypeIdHgd;

//        if(strlen($mCustomer->phone) < 13){// nếu ghi 2 phone số thì không validate
//            if(!UsersPhone::isValidCellPhone($mCustomer->phone)){
////                return ; Apr1618 không return nữa, văng lỗi bắt nhập đúng số đt luôn
//                throw new Exception("Số điện thoại $mCustomer->phone không là số di động hợp lệ, vui lòng kiểm tra lại");
//            }
//        }
        if($mUserPhone->countByPhoneAndType()){// count nếu có rồi thì không insert nữa
            return ;
        }// Aug0719  DungNT Jul3119 open insert all data for target CCS 2019
        UsersPhone::savePhone($mCustomer);
    }
    
    public function validatePhoneHgd($mUser, $mCustomer) {
        if(!empty($mCustomer->phone) && strlen($mCustomer->phone) < 13){// nếu ghi 2 phone số thì không validate
            if(!UsersPhone::isValidCellPhoneOrLandline($mCustomer->phone)){
                $mCustomer->addError('temp_password', "Số điện thoại $mCustomer->phone không là số điện thoại hợp lệ, vui lòng kiểm tra lại");
            }
        }
        if($mCustomer->phone < 1000000){// Jan0619 add validate phone
            $mCustomer->addError('temp_password', "Số điện thoại $mCustomer->phone không là số điện thoại hợp lệ, vui lòng kiểm tra lại");
        }
        
        if(strlen($mCustomer->phone) > 11){
            $mCustomer->addError('temp_password', "Mỗi khách hàng chỉ được nhập 1 số điện thoại, vui lòng kiểm tra lại");
        }
    }

    /**
     * @Author: DungNT Aug 31, 2016   serial, latitude, longitude
     * map attribute to model form $q. get post and validate
     */
    public function hgdCreateGetPost($mUser, $result, $q, &$mCustomer) {
        $mAgent = Users::model()->findByPk($q->agent_id);
        if($mAgent && $mAgent->gender != Users::IS_AGENT){
            throw new Exception('Đại lý không hợp lệ, vui lòng chọn đúng đại lý. API CCS WRONG AGENT');
        }
        
        $this->mFixAddress                  = new UsersFixAddress();
        $this->mFixAddress->address_old     = $mCustomer->first_name. '; '.$mCustomer->address;
        $this->mFixAddress->transaction_history_id = isset($q->transaction_history_id) ? $q->transaction_history_id : 0;

        $mCustomer->area_code_id    = $q->agent_id;
        $mCustomer->phone           = str_replace(MyFormat::$BAD_CHAR, '', trim($q->phone));
        $mCustomer->first_name      = trim($q->first_name);
        $mCustomer->province_id     = $q->province_id;
        $mCustomer->district_id     = $q->district_id;
        $mCustomer->ward_id         = $q->ward_id;
        $mCustomer->house_numbers   = trim($q->house_numbers);
        $mCustomer->street_id       = $q->street_id;
        $mCustomer->ip_address      = isset($q->list_hgd_invest) ? $q->list_hgd_invest : [];
        $mCustomer->ip_address      = implode(',', $mCustomer->ip_address);// Đầu tư HGD
        if(!empty($mCustomer->ip_address)){
            $mCustomer->ip_address = ",$mCustomer->ip_address,";
        }
//        $mCustomer->code_account    = "seri".$q->serial; // Sử dụng cột code_account để lưu seri bình, để nó build dc chuỗi search, khi đó sẽ search like: seri899
        if(is_null($mCustomer->id)){
            $mCustomer->slug            = $q->latitude.",$q->longitude";
        }
        // Jun 15, 2017 sử dụng cho PVKH update thông tin KH từ app
        if( $this->isAllowUpdate($mUser) && ($mCustomer->is_maintain == STORE_CARD_HGD_CCS || is_null($mCustomer->id)) ){
            $mCustomer->created_by      = $mUser->id;
//            if($mUser->role_id == ROLE_EMPLOYEE_MAINTAIN){// Oct1618 review lại đk này, xem có cần cho BC ko
//            $mCustomer->sale_id         = $mUser->id;// Close Apr1618, sale_id là của phòng telesale
//            }
            $mCustomer->payment_day     = $q->hgd_time_use;// đổi chức năng 2 cột payment_day, parent_id với loại user HGD CCS
            $mCustomer->parent_id       = $q->hgd_type;// 1 Tiếp xúc, 2: Bảo trì
            $mCustomer->is_maintain     = STORE_CARD_HGD_CCS;
            $mCustomer->channel_id      = Users::CON_LAY_HANG;
        }
        $mCustomer->validate();
        if($this->mUserLogin->role_id != ROLE_EMPLOYEE_MAINTAIN){
            $mCustomer->checkPhoneHgd();// Sep3019 close to debug  - will open
        }
        $this->mUserApp = $mUser; // $mCustomer->checkAddressHgd();
        $this->checkVersionCode($q, $mCustomer);

        if( $this->isAllowUpdate($mUser) && ($mCustomer->is_maintain == STORE_CARD_HGD_CCS || is_null($mCustomer->id)) ){
            $mCustomer->LoadUsersRef();
            // $mCustomer->mUsersRef = new UsersRef('AndroidCreateHgd'); // Sep 29, 2015, sử dụng 1 số biến json có sẵn của KH bò mối để save cho HGD 
            $mCustomer->mUsersRef->contact_technical_name       = trim($q->serial);
            $mCustomer->mUsersRef->contact_technical_phone      = trim($q->hgd_thuong_hieu);
            $mCustomer->mUsersRef->contact_technical_landline   = trim($q->hgd_doi_thu);
            $mCustomer->temp_password   = isset($q->pttt_code) ? strtoupper(trim($q->pttt_code)) : '';// add Jun 08, 2017
            if(empty($mCustomer->temp_password)){
                throw new Exception('PTTT CODE không được bỏ trống');
            }
            
            $mSpjCode = new SpjCode();
            $mSpjCode->code     = $mCustomer->temp_password;
            $mSpjCode->user_id  = $mUser->id;
            $mSpjCode->status   = [SpjCode::STATUS_NEW, SpjCode::STATUS_USED];
            $mSpjCode           = $mSpjCode->getModelCodeUser();
            $this->mSpjCode     = $mSpjCode;
            if(!empty($mCustomer->temp_password)){
                if(is_null($mSpjCode) || ($mSpjCode && $mSpjCode->pttt_customer_id !=0 && $mSpjCode->pttt_customer_id != $mCustomer->id) ){
                    $mCustomer->addError('temp_password', 'Mã code PTTT đã sử dụng hoặc không hợp lệ');
                }
            }
        }
        $this->validatePhoneHgd($mUser, $mCustomer);

        if($mCustomer->hasErrors()){
            $result['message'] = HandleLabel::FortmatErrorsModel($mCustomer->getErrors());
            ApiModule::sendResponse($result, $this);
        }
        return $mCustomer;
    }

    /** @Author: DungNT Jul 27, 2017
     * @Todo: cho phép role ccs được update KH
     */
    public function isAllowUpdate($mUser) {
//        $aRole = [ROLE_EMPLOYEE_MAINTAIN, ROLE_SALE, ROLE_MONITORING_MARKET_DEVELOPMENT, ROLE_EMPLOYEE_MARKET_DEVELOPMENT];
        $aRole = [ROLE_SALE, ROLE_MONITORING_MARKET_DEVELOPMENT, ROLE_EMPLOYEE_MARKET_DEVELOPMENT];
        if(in_array($mUser->role_id, $aRole)){
            return true;
        }
        return false;
    }

    /** @Author: DungNT Jun 08, 2017
     * @Todo: cập nhật customer id to spj code
     */
    public function updateCustomerIdToSpjCode() {
        if(is_null($this->mSpjCode) || is_null($this->mCustomer)){
            $info = "SpjCode Không thể lưu mã code: {$this->mCustomer->temp_password}. Vui lòng thử lại";
            SendEmail::bugToDev($info, ['title'=>$info]);// sửa hàm mail to dev cho vào queue không gửi trực tiếp
            throw new Exception($info);
        }// Aug1617 theo dõi lỗi không lưu pttt_customer_id vào table code
        $this->mSpjCode->pttt_customer_id = $this->mCustomer->id;
        $this->mSpjCode->setCustomerIdToCode();// Sep3019 close to debug  - will open
    }

    public function removeSpjCodeOld($mCustomer) {
        if(empty($mCustomer->temp_password)){
           return ;
        }
        $mSpjCode             = new SpjCode();
        $mSpjCode->code       = $mCustomer->temp_password;
        $mSpjCode->status     = SpjCode::STATUS_USED;
        $mSpjCode             = $mSpjCode->getModelCodeUser();
        if($mSpjCode){
            $mSpjCode->status               = SpjCode::STATUS_NEW;
            $mSpjCode->pttt_customer_id     = 0;
            $mSpjCode->update();
        }
    }
    
    /** @Author: DungNT Dec 04, 2017
     *  @Todo: kiểm tra KH cập nhật có hợp lệ không
     **/
    public function checkAllowUpdate($mCustomer) {
        if(in_array($mCustomer->id, GasConst::getIdHgdNotUpdate())){
            throw new Exception('Đây là KH trả thẻ, không thể cập nhật khách hàng này');
        }
//        if($mCustomer->is_maintain == UsersExtend::STORE_CARD_HGD_APP){
//            throw new Exception('Đây là KH App, không thể cập nhật khách hàng này');
//        }// Close Oct0618 allow PVKH update KH app - ý kiến của Mã Hoàng Thiện
    }

    /**
     * @Author: DungNT Sep 16, 2016
     * @Todo: get hgd update
     */
    public function getHgdCustomer($mUser, $q, $result) {
        $mCustomer  = Users::model()->findByPk($q->customer_id);
//        if(is_null($mCustomer) || ($mCustomer->created_by != $mUser->id && $mCustomer->is_maintain == STORE_CARD_HGD_CCS)){
        if(is_null($mCustomer)){
            $result['message'] = 'Khách hàng không hợp lệ, cập nhật thất bại';
            ApiModule::sendResponse($result, $this);
        }
        $mCustomer->scenario = "AndroidCreateHgd";
        return $mCustomer;
    }

    /** @Author: DungNT Jun 30, 2017
     * @Todo: cập nhật sang table fix address
     */
    public function saveFixAddress($mCustomer) {
        if(empty($this->mFixAddress->transaction_history_id)){
            return ;
        }
        $this->mFixAddress->address_new = $mCustomer->first_name. '; '.$mCustomer->address;
        $this->mFixAddress->customer_id = $mCustomer->id;
        $this->mFixAddress->saveRecord();
    }
    
    /** @Author: DungNT Jun 08, 2019 
     *  @Todo: flow save số phone để gửi SMS điểm tích lũy cho KH của Kiên Miền Tây 
     **/
    public function addPhoneHgdSmsPoint($mCustomer, $mUserLogin) {
        $cRole = $mUserLogin->role_id;
        $mUsersPhoneExt2 = new UsersPhoneExt2();
        $mUsersPhoneExt2->handleHgdSmsPoint($mCustomer, $cRole);
    }

    /**
     * @Author: DungNT Aug 31, 2016
     * @Todo: android list customer hgd
     * @Resource: customer/hgdList
     * @ex_json_request: {"token":"403acbe7932b3c7f2f3ce5a906757c29","page":"0"}
     */
    public function actionHgdList()
    {
        try{
        $result = ApiModule::$defaultResponse;
        $this->checkRequest();
        $q = json_decode($_POST['q']);
        $this->checkRequiredParams($q, array('token', 'page'));
        $mUser = $this->getUserByToken($result, $q->token);// Oct 12, 2015 for check inactive or delete User
        $result = ApiModule::$defaultSuccessResponse;
        $result['message'] = 'Success';
        $this->handleHgdList($result, $q, $mUser);
        ApiModule::sendResponse($result, $this);
        } catch (Exception $ex) {
            MyFormat::ApiCatchError($ex, $this);
        }
    }

    /**
     * @Author: DungNT Oct 27, 2015
     */
    public function handleHgdList(&$result, $q, $mUser) {
        // 1. get list order by user id
        $mUserExtend    = new UsersExtend();
        $dataProvider   = $mUserExtend->ApiListing($q, $mUser);
        $models         = $dataProvider->data;
        $CPagination    = $dataProvider->pagination;
        $result['total_record'] = $CPagination->itemCount;
        $result['total_page']   = $CPagination->pageCount;
        $hgdShortReport = "";
        if($q->page == 0){
            $mSpjCode = new SpjCode();
            $mSpjCode->user_id = $mUser->id;
            $hgdShortReport = "Hôm nay: ".$mUserExtend->ccsCountToday($mUser)." - Tổng số: ".$mUserExtend->ccsCountAll($mUser)." - Bình quay về: ".  $mSpjCode->countGoBackCode();
        }
        $result['hgd_short_report']   = $hgdShortReport;

        if( $q->page >= $CPagination->pageCount ){
            $result['record'] = array();
        }else{
            foreach($models as $mCustomer){
                $timeUse = $code = '';
                if(!empty($mCustomer->payment_day)){
                    $timeUse = " - TGSD $mCustomer->payment_day ngày";
                }
                if(!empty($mCustomer->temp_password)){
                    $code = " - $mCustomer->temp_password";
                }
                $created_date = MyFormat::dateConverYmdToDmy($mCustomer->created_date, "d/m/Y H:i");
                $temp = array();
                $temp['id']             = $mCustomer->id;
                $temp['name']           = $mCustomer->getFullName().$code.$timeUse;
                $temp['phone']          = $mCustomer->getPhoneApiAndroid()." - Ngày Tạo: $created_date";
                $temp['address']        = $mCustomer->getAddress();
                $temp['created_date']   = $created_date;
                $result['record'][]     = $temp;
            }
        }
    }

    /**
     * @Author: DungNT Aug 31, 2016
     * @Todo: android view customer hgd
     * @Resource: customer/hgdView
     * @ex_json_request: {"token":"403acbe7932b3c7f2f3ce5a906757c29", "customer_id":123}
     */
    public function actionHgdView()
    {
        try{
        $result = ApiModule::$defaultResponse;
        $this->checkRequest();
        $q = json_decode($_POST['q']);
        $this->checkRequiredParams($q, array('token', 'customer_id'));
        $mCustomer = Users::model()->findByPk($q->customer_id);
        if(is_null($mCustomer)){
            $result['message'] = "Yêu cầu không hợp lệ";
        }else{
            $mUser = $this->getUserByToken($result, $q->token);// Oct 12, 2015 for check inactive or delete User
            $result = ApiModule::$defaultSuccessResponse;
            $result['message'] = 'Success';
            $this->handleHgdView($result, $q, $mCustomer);
        }
        ApiModule::sendResponse($result, $this);
        } catch (Exception $ex) {
            MyFormat::ApiCatchError($ex, $this);
        }
    }

    /**
     * @Author: DungNT Aug 31, 2016
     */
    public function handleHgdView(&$result, $q, $mCustomer) {
        $temp                   = array();
        $temp['id']             = $mCustomer->id;
        $temp['name']           = $mCustomer->getFullName();
        $temp['phone']          = $mCustomer->getPhoneApiAndroid();
        $temp['address']        = $mCustomer->getAddress();
        $temp['created_date']   = MyFormat::dateConverYmdToDmy($mCustomer->created_date, "d/m/Y H:i");
        $temp['created_by']     = $mCustomer->getCreatedBy();
        $temp['latitude_longitude'] = $mCustomer->slug;
        $temp['customer_type']  = $mCustomer->getTypeCustomerText();
        $temp['can_update']     = $mCustomer->canUpdateHgdAppAndroid() ? '1' : '0';

        $temp['hgd_type']       = UsersExtend::getHgdTypePointText($mCustomer->parent_id);
        $temp['hgd_time_use']   = $mCustomer->payment_day;
        $temp['serial']         = $mCustomer->getUserRefField("contact_technical_name");
        $temp['hgd_thuong_hieu'] = $mCustomer->getVoPTTT();
        $temp['hgd_doi_thu']    = $mCustomer->getUserRefField("contact_technical_landline");
        $temp['list_hgd_invest_text'] = GasConst::getInvestHgdText($mCustomer->ip_address);

        $temp['agent_id']       = $mCustomer->area_code_id;
        $temp['province_id']    = $mCustomer->province_id;
        $temp['district_id']    = $mCustomer->district_id;
        $temp['ward_id']        = $mCustomer->ward_id;
        $temp['house_numbers']  = $mCustomer->house_numbers;
        $temp['street_id']      = $mCustomer->street_id;
        $temp['hgd_type_id']    = $mCustomer->parent_id;
        $temp['list_hgd_invest'] = explode(",", $mCustomer->ip_address);
        $temp['pttt_code']      = $mCustomer->temp_password;

        $result['record']       = $temp;
    }

    /**
     * @Author: DungNT Now 22, 2016
     * @Todo: app customer register
     * @Resource: customer/register
     * @ex_json_request: {"first_name":"Nguyen Ngoc Kien","phone":"0988180386"}
     */
    public function actionRegister()
    {
        try{
        $result = ApiModule::$defaultResponse;
        $this->checkRequest();
        $q = json_decode($_POST['q']);
        $this->checkRequiredParams($q, array('first_name', 'phone'));
        $mSignup = new AppSignup('AppSignup');
        throw new Exception('Phiên bản app của bạn đã cũ, vui lòng cập nhật bản mới để sử dụng');
        $this->registerGetPost($mSignup, $q, $result);
        $mSignup->makeRecord();
        $mSignup->sendConfirmCode();// send sms to PhoneNumber of user

        $result = ApiModule::$defaultSuccessResponse;
        $result['message']  = 'Mã xác thực đã được gửi tới số điện thoại '.$q->phone.'. Vui lòng nhập mã xác thực, sau đó đăng nhập bằng thông tin đã gửi vào số điện thoại '.$q->phone;
        $result['record']   = $mSignup->token;
        $result['token']    = $mSignup->token;
        ApiModule::sendResponse($result, $this);
        } catch (Exception $ex) {
            MyFormat::ApiCatchError($ex, $this);
        }
    }

    /**
     * @Author: Trung Sept 29, 2017
     * @Todo: generate one time password for user
     * @Resource: customer/generateOTP
     * 1. Close all older OTP code
     * 2. Generate new OTP code
     * 3. Send to client
     * @ex_json_request: {"phone":"0988180386"}
     */
    public function actionGenerateOTP()
    {
        try {
//            $a = 'POST: '.json_encode($_POST). ' GET: '.json_encode($_GET); 
//            Logger::WriteLog($a);
            $result = ApiModule::$defaultResponse;
            $this->checkRequest();
            $q = json_decode($_POST['q']);
            $this->checkRequiredParams($q, array('phone'));
            $q->phone = trim($q->phone);
            if(empty($q->phone)){
                $result['message'] = 'Số điện thoại không hợp lệ, vui lòng kiểm tra lại';
                ApiModule::sendResponse($result, $this);
            }

            // Close all last OTP code
            AppSignup::model()->updateAll(['status' => AppSignup::STATUS_CLOSE], "phone='$q->phone'");

            $mSignup = new AppSignup('AppGenerateOTP');
            $mSignup->phone         = UsersPhone::formatPhoneOnly($q->phone);
            $mSignup->first_name    = $mSignup->phone;
            $mSignup->validate();
            $mSignup->checkDeviceLock($q);
            if ($mSignup->hasErrors()) {
                $result['message'] = HandleLabel::FortmatErrorsModel($mSignup->getErrors());
                ApiModule::sendResponse($result, $this);
            }
            if(!$mSignup->isApplePhone() && Yii::app()->setting->getItem('EnableUpdateSell') == 'no'){
                throw new Exception('Chức năng này hiện đang cập nhật, thời gian dự kiến hoàn thành: 01/12/2017');
            }
            if(!$mSignup->isApplePhone()){
                $mSignup->makeRecord($q);
                if(GasCheck::isServerLive() && !in_array($mSignup->first_name, $this->getPhoneNotSendSms())){
                    $mSignup->sendOTP();// Anh Dũng Oct1617 close test ko gửi SMS. mặc dịnh là 1111
                }
            }
            $mSignup->sendNotifyPinTest();

            $result = ApiModule::$defaultSuccessResponse;
            $result['message']  = 'Mã xác thực đã được gửi tới số điện thoại '.$q->phone.'. Vui lòng nhập mã xác thực, sau đó đăng nhập bằng thông tin đã gửi vào số điện thoại '.$q->phone;
            $result['record']   = $mSignup->token;
            $result['token']    = $mSignup->token;
            ApiModule::sendResponse($result, $this);
        } catch (Exception $ex) {
            MyFormat::ApiCatchError($ex, $this);
        }
    }
    
    /** @Author: DungNT Now 09, 2018
     *  @Todo: list phone ko send sms login để test
     **/
    public function getPhoneNotSendSms() {
        return [];
        return [
            '0903972241',
        ];
    }

    /**
     * @Author: DungNT Nov 22, 2016
     * @todo: xử lý get post và validate request
     * 1. validate phone hop le
     * 2. validate phone có trên hệ thống chưa
     * 3. xử lý forgot pass
     */
    public function registerGetPost(&$mSignup, $q, $result) {
        $mSignup->first_name   = trim($q->first_name);
        $mSignup->phone        = UsersPhone::formatPhoneOnly($q->phone);
        $mSignup->validate();
        if($mSignup->hasErrors()){
            $result['message'] = HandleLabel::FortmatErrorsModel($mSignup->getErrors());
            ApiModule::sendResponse($result, $this);
        }
    }

    /**
     * @Author: DungNT Now 22, 2016
     * @Todo: app customer register confirm
     * @Resource: customer/registerConfirm
     * @ex_json_request: {"confirm_code":"0988180386"}
     */
    public function actionRegisterConfirm()
    {
        try{
        $result = ApiModule::$defaultResponse;
        $this->checkRequest();
        $q = json_decode($_POST['q']);
        $this->checkRequiredParams($q, array('token', 'confirm_code'));
        $mSignup = new AppSignup();
        $ok = $mSignup->handleConfirmCode($q);
        if(!$ok){
            $result['message'] = "Mã xác thực không đúng";
            ApiModule::sendResponse($result, $this);
        }
        $result = ApiModule::$defaultSuccessResponse;
        $result['message'] = 'Xác thực thành công';
        ApiModule::sendResponse($result, $this);
        } catch (Exception $ex) {
            MyFormat::ApiCatchError($ex, $this);
        }
    }

    /**
     * @Author: DungNT Feb 05, 2017
     * @Todo: app Gas24h customer forgot password
     * @Resource: customer/forgotPassword
     * @ex_json_request: {"phone":"01684331552"}
     */
    public function actionForgotPassword()
    {
        try{
        $result = ApiModule::$defaultResponse;
        $this->checkRequest();
        $q = json_decode($_POST['q']);
        $this->checkRequiredParams($q, array('phone'));
        $mSignup = new AppSignup('AppForgotPass');
//        throw new Exception('Chức năng này hiện đang cập nhật, bạn vui lòng quay lại sau.');
        $this->forgotPasswordGetPost($mSignup, $q, $result);
        $mSignup->makeRecord();
        $mSignup->sendConfirmCodeForgotPassword();// send sms to PhoneNumber of user

        $result = ApiModule::$defaultSuccessResponse;
        $result['message']  = 'Mã xác thực đã được gửi tới số điện thoại '.$q->phone.'. Vui lòng nhập mã xác thực, sau khi xác thực thành công đăng nhập bằng mật khẩu mới đã gửi vào số điện thoại '.$q->phone;
        $result['record']   = $mSignup->token;
        $result['token']    = $mSignup->token;
        ApiModule::sendResponse($result, $this);
        } catch (Exception $ex) {
            MyFormat::ApiCatchError($ex, $this);
        }
    }

     /**
     * @Author: DungNT Feb 05, 2017
     * @todo: xử lý get post và validate request
     * 1. validate phone hợp lệ, có quá số lần cho phép request chưa (5 lần)
     * 3. xử lý forgot pass
     */
    public function forgotPasswordGetPost(&$mSignup, $q, $result) {
        $mSignup->phone         = UsersPhone::formatPhoneOnly($q->phone);
        $mSignup->type          = AppSignup::TYPE_FORGOT_PASS;
        $mSignup->validate();
        if($mSignup->hasErrors()){
            $result['message'] = HandleLabel::FortmatErrorsModel($mSignup->getErrors());
            ApiModule::sendResponse($result, $this);
        }
    }

    /**
     * @Author: DungNT Feb 05, 2017
     * @Todo: app customer forgotPassword confirm
     * @Resource: customer/forgotPasswordConfirm
     * @ex_json_request: {"token":"9e890c64c58ff2c5dfd95bc9405082f6","confirm_code":"0386"}
     */
    public function actionForgotPasswordConfirm()
    {
        try{
        $result = ApiModule::$defaultResponse;
        $this->checkRequest();
        $q = json_decode($_POST['q']);
        $this->checkRequiredParams($q, array('token', 'confirm_code'));
        $mSignup = new AppSignup();
        $ok = $mSignup->handleConfirmCodeForgotPass($q);
        if(!$ok){
            $result['message'] = "Mã xác thực không đúng";
            ApiModule::sendResponse($result, $this);
        }
        $result = ApiModule::$defaultSuccessResponse;
        $result['message'] = 'Xác thực thành công';
        ApiModule::sendResponse($result, $this);
        } catch (Exception $ex) {
            MyFormat::ApiCatchError($ex, $this);
        }
    }

    /**
     * @Author: DungNT Now 25, 2016
     * @Todo: app customer add promotion
     * @Resource: customer/promotionAdd
     * @ex_json_request: {"token":"ssff3234","code":"sj50"}
     */
    public function actionPromotionAdd()
    {
        try {
//            GasCheck::randomSleep(); 
            $result = ApiModule::$defaultResponse;
            $this->checkRequest();
            $q = json_decode($_POST['q']);
            $this->checkRequiredParams($q, array('token', 'code'));
            $mUser = $this->getUserByToken($result, $q->token);

            $mPromotion = $this->getModelPromotion($q, $mUser);
            $result = ApiModule::$defaultSuccessResponse;

            $mPromotionUser = new AppPromotionUser();
            $mPromotionUser->mAppUserLogin  = $mUser;
            $mPromotionUser->qParam         = $q;
            $mPromotionUser->setLocation();
            $mPromotionUser->checkInputNotValid();
            $mPromotionUser->addUser($mUser, $mPromotion, $result);
            
            if ($mPromotionUser->hasErrors()) {
                $result = ApiModule::$defaultResponse;
                $result['message'] = HandleLabel::FortmatErrorsModel($mPromotionUser->getErrors());
                ApiModule::sendResponse($result, $this);
            }

            ApiModule::sendResponse($result, $this);
        } catch (Exception $ex) {
            MyFormat::ApiCatchError($ex, $this);
        }
    }

    /**
     * @Author: DungNT Nov 25, 2016
     * @Todo: get valid model promotion
     */
    public function getModelPromotion($q, $mUser) {
        try {
            $mPromotion             = new AppPromotion();
            $mPromotion->code_no    = $q->code;
            $mPromotion             = $mPromotion->getByCode();
            if(is_null($mPromotion)){
                throw new Exception('Mã khuyến mãi không hợp lệ', SpjError::SELL001);
            }
            $mPromotion->checkAgentLock($q, $mUser);
            $mPromotion->mAppUserLogin = $mUser;
//            $mPromotion->checkRadiusAllow($q);
            $mPromotion->checkProvinceAllow($q);
            $mPromotion->handleSomeCheck($q); 
            $mPromotion->checkRadiusAgent($q);// tam close lai t2 se mo ra
            $mPromotion->checkIpLimit($q);// tam close lai t2 se mo ra
            return $mPromotion;
        } catch (Exception $exc) {
            throw new Exception($exc->getMessage(), $exc->getCode());
        }
    }

    /**
     * @Author: DungNT Now 25, 2016
     * @Todo: android list promotion
     * @Resource: customer/promotionList
     * @ex_json_request: {"token":"403acbe7932b3c7f2f3ce5a906757c29","page":"0"}
     */
    public function actionPromotionList()
    {
        try{
        $result = ApiModule::$defaultResponse;
        $this->checkRequest();
        $q = json_decode($_POST['q']);
        $this->checkRequiredParams($q, array('token', 'page'));
        $mUser = $this->getUserByToken($result, $q->token);// Oct 12, 2015 for check inactive or delete User
        $result = ApiModule::$defaultSuccessResponse;
        $result['message'] = 'List';
        $mPromotionUser = new AppPromotionUser();
        $mPromotionUser->handlePromotionList($result, $q, $mUser);
        ApiModule::sendResponse($result, $this);
        } catch (Exception $ex) {
            MyFormat::ApiCatchError($ex, $this);
        }
    }

    /**
     * @Author: DungNT Jun 06, 2017
     * @Todo: android list PTTT code
     * @Resource: customer/ptttCodeList
     * @ex_json_request: {"token":"f2dbaeb8233c4166a7dcfda302f6801d","page":"0"}
     */
    public function actionPtttCodeList()
    {
        try{
        $result = ApiModule::$defaultResponse;
        $this->checkRequest();
        $q = json_decode($_POST['q']);
        $this->checkRequiredParams($q, array('token', 'page'));
        $mUser = $this->getUserByToken($result, $q->token);// Oct 12, 2015 for check inactive or delete User
        $this->mUserApp = $mUser;
        $this->checkVersionCode($q, $mUser);
        $result = ApiModule::$defaultSuccessResponse;
        $mSpjCode = new SpjCode();
        $mSpjCode->autoGenCodeFromList($mUser);
        $mSpjCode->handleApiList($result, $q, $mUser);
        ApiModule::sendResponse($result, $this);
        } catch (Exception $ex) {
            MyFormat::ApiCatchError($ex, $this);
        }
    }

}
