<?php
/* xử lý những action supoort user
 */
class SupportController extends ApiController
{
    /**
     * @Author: ANH DUNG May 14, 2017
     * @Todo: ticket list for user
     * @Resource: support/ticketList
     * @ex_json_request: {"token":"4660157f8fa340f07f15e2b47a7509c1","page":"0", "status":1}
     */
    public function actionTicketList()
    {
        try{
            $result = ApiModule::$defaultResponse;
            $this->checkRequest();
            $q = json_decode($_POST['q']);
            $this->checkRequiredParams($q, array('token', 'page'));
            $mUser = $this->getUserByToken($result, $q->token);// Oct 12, 2015 for check inactive or delete User
/*            for test
            $temp = '{"token":"403acbe7932b3c7f2f3ce5a906757c29","page":"0"}';
            $q = json_decode($temp);
            $mUser = Users::model()->findByPk(121257);
 */
            $result = ApiModule::$defaultSuccessResponse;
            $this->getHelpMessage($mUser, $result);
            $mTicket = new GasTickets();
            $mTicket->handleApiList($result, $q, $mUser);
            ApiModule::sendResponse($result, $this);
        } catch (Exception $ex) {
            MyFormat::ApiCatchError($ex, $this);
        }
    }
    
    public function getHelpMessage($mUser, &$result) {
        $result['message'] = "Hỗ trợ các vấn đề lỗi khi sử dụng App "
                                . "\n 1. Tạo hỗ trợ chọn người xử lý Kim Phương"
                                . "\n 2. Gọi hỗ trợ trực tiếp "
                                . "\n Nam: 0981 471 595"
                                . "\n Đương: 0935 714 733"
                                . "\n Trung (android + ios): 0389 945 321"
                                . "";
        if($mUser->role_id == ROLE_CUSTOMER){
            $result['message'] = "Phản ánh các vấn đề khi sử dụng App ( về nhân viên giao nhận, tổng đài)";
        }
    }
    
    /**
     * @Author: ANH DUNG May 14, 2017
     * @Todo: ticket Create for user
     * @Resource: support/ticketCreate
     * @ex_json_request: {"token":"5875774c46522fa372164303308814f1","send_to_id":"163618", "title":"new ticket", "message":"test ticket msg"}
     */
    public function actionTicketCreate()
    {
        try{
            $result = ApiModule::$defaultResponse;
            $this->checkRequest();
            $q = json_decode($_POST['q']);
            $this->checkRequiredParams($q, array('token', 'send_to_id'));
            $mUser      = $this->getUserByToken($result, $q->token);
            $mTicket    = new GasTickets('ApiCreate');
            $mTicket->mAppUserLogin   = $mUser;
            $mTicket->handleAppPost($q);
            
            if($mTicket->hasErrors()){
                $result['message'] = HandleLabel::FortmatErrorsModel($mTicket->getErrors());
                ApiModule::sendResponse($result, $this);
            }
            $mTicket->SaveNewTicket();
            $mTicket->mDetail->handleAppSaveFile(GasFile::TYPE_13_TICKET);
            $result             = ApiModule::$defaultSuccessResponse;
            $result['message']  = 'Tạo ticket thành công';
            $result['record']   = $mTicket->id;
            ApiModule::sendResponse($result, $this);
        } catch (Exception $ex) {
            MyFormat::ApiCatchError($ex, $this);
        }
    }
    
    /**
     * @Author: ANH DUNG May 14, 2017
     * @Todo: ticket Reply for user
     * @Resource: support/ticketReply
     * @ex_json_request: {"token":"5875774c46522fa372164303308814f1","id":"163618", "message":"test ticket msg"}
     */
    public function actionTicketReply(){
        try{
            $result = ApiModule::$defaultResponse;
            $this->checkRequest();
            $q = json_decode($_POST['q']);
            $this->checkRequiredParams($q, array('token', 'id'));
            $mUser      = $this->getUserByToken($result, $q->token);
            $mTicket            = GasTickets::model()->findByPk($q->id);
            $mTicket->scenario  = 'reply';
            $mTicket->message   = $q->message;
            $mTicket->mAppUserLogin   = $mUser;
            $mTicket->validate();
            if($mTicket->hasErrors()){
                $result['message'] = HandleLabel::FortmatErrorsModel($mTicket->getErrors());
                ApiModule::sendResponse($result, $this);
            }
            
            $mTicket->uid_post    = $mUser->id;
            $mTicket->SaveOneMessageDetail();
            $mTicket->mDetail->handleAppSaveFile(GasFile::TYPE_13_TICKET); 
            $result             = ApiModule::$defaultSuccessResponse;
            $result['message']  = 'Trả lời ticket thành công';
            $result['record']   = [];
            ApiModule::sendResponse($result, $this);
        } catch (Exception $ex) {
            MyFormat::ApiCatchError($ex, $this);
        }
    }
    
    /**
     * @Author: ANH DUNG May 14, 2017
     * @Todo: ticket Reply for user
     * @Resource: support/ticketView
     * @ex_json_request: {"token":"5875774c46522fa372164303308814f1","id":"163618"}
     */
    public function actionTicketView(){
        try{
            $result = ApiModule::$defaultResponse;
            $this->checkRequest();
            $q = json_decode($_POST['q']);
            $this->checkRequiredParams($q, array('token', 'id'));
            $mUser      = $this->getUserByToken($result, $q->token);
            $mTicket    = GasTickets::model()->findByPk($q->id);
            if(is_null($mTicket)){
                throw new Exception('Ticket yêu cầu không hợp lệ');
            }
            $mTicket->mAppUserLogin = $mUser;//
            $result                         = ApiModule::$defaultSuccessResponse;
            $result['message']              = 'View';
            $result['record']               = $mTicket->getAppReply();
            $result['record']['list_image'] = $mTicket->handleAppFileView();
            ApiModule::sendResponse($result, $this);
        } catch (Exception $ex) {
            MyFormat::ApiCatchError($ex, $this);
        }
    }
    
    /**
     * @Author: ANH DUNG May 14, 2017
     * @Todo: ticket Close for user
     * @Resource: support/ticketClose
     * @ex_json_request: {"token":"5875774c46522fa372164303308814f1","id":"9"}
     */
    public function actionTicketClose()
    {
        try{
            $result = ApiModule::$defaultResponse;
            $this->checkRequest();
            $q = json_decode($_POST['q']);
            $this->checkRequiredParams($q, array('token', 'id'));
            $mUser      = $this->getUserByToken($result, $q->token);
            $mTicket            = GasTickets::model()->findByPk($q->id);
            $mTicket->mAppUserLogin   = $mUser;
            if($mTicket->uid_login != $mUser->id){
                throw new Exception('Yêu cầu không hợp lệ, bạn không thể Close ticket này');
            }
            $mTicket->uid_post    = $mUser->id;
            $mTicket->CloseTicket();
            $result             = ApiModule::$defaultSuccessResponse;
            $result['message']  = 'Close ticket thành công';
            $result['record']   = [];
            ApiModule::sendResponse($result, $this);
        } catch (Exception $ex) {
            MyFormat::ApiCatchError($ex, $this);
        }
    }
    
    /**
     * @Author: ANH DUNG Aug 05, 2017
     * @Todo: mail to dev errors app
     * @Resource: support/appLog
     * @ex_json_request: {"token":"403a56672771ba9a808b42e9e5751883","msg":"test app errors log","version_code":1236,"platform":1}
     */
    public function actionAppLog()
    {
        try{
            $result = ApiModule::$defaultResponse;
            $this->checkRequest();
            $q = json_decode($_POST['q']);
            $this->checkRequiredParams($q, array('token', 'message', 'version_code', 'platform'));
            $mUser = $this->getUserByToken($result, $q->token);// Oct 12, 2015 for check inactive or delete User
            $this->getHelpMessage($mUser, $result);
            $platform = 'Unknow';
            if($q->platform == UsersTokens::PLATFORM_IOS){
                $platform = 'Ios';
            }elseif($q->platform == UsersTokens::PLATFORM_ANDROID){
                $platform = 'Android';
            }
            $needMore = [];
            $needMore['title']      = "App Log Platform: $platform Version $q->version_code. UserId: $mUser->id - $mUser->username";
            $needMore['list_mail']  = ['team.dev@spj.vn'];
            SendEmail::bugToDev($q->message, $needMore);
            $result = ApiModule::$defaultSuccessResponse;
            ApiModule::sendResponse($result, $this);
        } catch (Exception $ex) {
            MyFormat::ApiCatchError($ex, $this);
        }
    }
    
    /**
     * @Author: ANH DUNG Oct 27, 2017
     * @Todo: make notify test 
     * @Resource: support/notifyTest
     * @ex_json_request: {"token":"5875774c46522fa372164303308814f1"}
     */
    public function actionNotifyTest()
    {
        try{
            $result = ApiModule::$defaultResponse;
            $this->checkRequest();
            $q = json_decode($_POST['q']);
            $this->checkRequiredParams($q, array('token'));
            $mUser = $this->getUserByToken($result, $q->token);// Oct 12, 2015 for check inactive or delete User
            $mNotify =new GasScheduleNotify();
            $mNotify->user_id = $mUser->id;
            $mNotify->testApp();
            $result = ApiModule::$defaultSuccessResponse;
            $result['message'] = 'Hệ thống nhận được yêu cầu test notify thông báo, vui lòng chờ xử lý';
            ApiModule::sendResponse($result, $this);
        } catch (Exception $ex) {
            MyFormat::ApiCatchError($ex, $this);
        }
    }
    
    /** @Author: ANH DUNG Mar 31, 2018
     *  @Todo: way2 update location of Agent Car
     *  Sử dụng cùng hàm actionNotifyTest nhưng chưa put location 
     **/
    public function setLocationAgentCar($mUser, $q) {
        $model = new UsersLocation();
        $model->user_id     = $mUser->id;
        $model->latitude    = $q->latitude;
        $model->longitude   = $q->longitude;
        $aRoleCheck = [ROLE_EMPLOYEE_MAINTAIN];
        if(in_array($mUser->role_id, $aRoleCheck)){ // way 1 update location of Agent Car
            $model->agent_id   = $this->getAgentOfEmployee($mUser);
            $model->setAgentLocation();
        }
    }
    
    /**
     * @Author: ANH DUNG Oct 29, 2017
     * @Todo: tracking location user
     * @Resource: support/locationUser
     * @ex_json_request: {"token":"5875774c46522fa372164303308814f1","latitude":123,"longitude":222}
     */
    public function actionLocationUser()
    {
        try{
            $result = ApiModule::$defaultResponse;
            $this->checkRequest();
            $q = json_decode($_POST['q']);
            $this->checkRequiredParams($q, array('token', 'latitude', 'longitude'));
            $mUser = $this->getUserByToken($result, $q->token);// Oct 12, 2015 for check inactive or delete User
//            Logger::WriteLog(json_encode($q));
            $model = new UsersLocation();
            $model->user_id     = $mUser->id;
            $model->latitude    = $q->latitude;
            $model->longitude   = $q->longitude;
            $cHours = date('H');
            if($cHours > 4 && $cHours < 22 ) {
                $model->doSave();
            }
            $aRoleCheck = [ROLE_EMPLOYEE_MAINTAIN];
            if(in_array($mUser->role_id, $aRoleCheck)){ // way 1 update location of Agent Car
                $model->agent_id   = $this->getAgentOfEmployee($mUser);
                $model->setAgentLocation();
            }
            
            $result = ApiModule::$defaultSuccessResponse;
            $result['message'] = 'ok';
            ApiModule::sendResponse($result, $this);
        } catch (Exception $ex) {
            MyFormat::ApiCatchError($ex, $this);
        }
    }
    
    /**
     * @Author: ANH DUNG Now 27, 2017
     * @Todo: get app view page
     * @Resource: support/viewPage
     * @ex_json_request: {"token":"5875774c46522fa372164303308814f1","id":1}
     */
    public function actionViewPage()
    {
        try{
            $result = ApiModule::$defaultResponse;
            $this->checkRequest();
            $q = json_decode($_POST['q']);
            $this->checkRequiredParams($q, array('token', 'id'));
            $mUser = $this->getUserByToken($result, $q->token);// Oct 12, 2015 for check inactive or delete User
            $mCms       =new Cms();
            $mCms->id   = $q->id;
            $result = ApiModule::$defaultSuccessResponse;
            $result['message']  = 'v';
            $result['record']   = $mCms->getAppView();
            ApiModule::sendResponse($result, $this);
        } catch (Exception $ex) {
            MyFormat::ApiCatchError($ex, $this);
        }
    }
    
    /**
     * @Author: ANH DUNG Apr 13, 2018
     * @Todo: get map agent
     * @Resource: support/mapAgent
     * @ex_json_request: {"token":"5875774c46522fa372164303308814f1"}
     */
    public function actionMapAgent()
    {
        try{
            $result = ApiModule::$defaultResponse;
            $this->checkRequest();
            $q = json_decode($_POST['q']);
            $this->checkRequiredParams($q, array('token'));
            $result = ApiModule::$defaultSuccessResponse;
            $mAppOrder = new AppOrder();
            $result['message']                  = 'ok';
            $result['record']['agent']          = $mAppOrder->handleGetConfig();
            $result['record']['allow_search']   = 1;// 1: cho phép search và request address, 0 là tắt hẳn, chỉ hiển thị địa chỉ của KH ở tọa độ hiện tại
            ApiModule::sendResponse($result, $this);
        } catch (Exception $ex) {
            MyFormat::ApiCatchError($ex, $this);
        }
    }
    
    /**
     * @Author: ANH DUNG Oct 03, 2018
     * @Todo: save event user call from app  
     * @Resource: support/eventClickCall
     * @ex_json_request: {"token":"5875774c46522fa372164303308814f1","phone":"0988180386","type":1,"obj_id":2}
     */
    public function actionEventClickCall()
    {
        try{
            $result = ApiModule::$defaultResponse;
            $this->checkRequest();
            $q = json_decode($_POST['q']);
            $this->checkRequiredParams($q, array('token', 'phone', 'type', 'obj_id'));
            $mUser          = $this->getUserByToken($result, $q->token);
            $mClickCall     = new ClickCall();
            $mClickCall->mAppUserLogin   = $mUser;
            $mClickCall->handleAppPost($q);
            
            if($mClickCall->hasErrors()){
                $result['message'] = HandleLabel::FortmatErrorsModel($mClickCall->getErrors());
                ApiModule::sendResponse($result, $this);
            }
            $mClickCall->save();
            $result             = ApiModule::$defaultSuccessResponse;
            $result['message']  = 'ok';
            ApiModule::sendResponse($result, $this);
        } catch (Exception $ex) {
            MyFormat::ApiCatchError($ex, $this);
        }
    }
    
    

}
