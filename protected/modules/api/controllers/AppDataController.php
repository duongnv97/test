<?php
/** @Author: HOANG NAM 26/06/2018
 **/
class AppDataController extends ApiController
{
    
    /** @Author: HOANG NAM 26/06/2018
     *  @Todo: get material_vo 
     *  @Resource: appData/clientCache
     *  @ex_json_request: {"token":"2013756e651ef0493be2dbf7820e0f24"}
     *  @Response:
     **/
    public function actionClientCache(){
        $result = ApiModule::$defaultResponse;
        $q = json_decode($_POST['q']);
        $this->checkRequiredParams($q, array('token'));
        $mUser = $this->getUserByToken($result, $q->token);
        $mCustomerRequest =new GasSupportCustomerRequest(); $mGasSupportCustomer =new GasSupportCustomer();
        $mModules = new Modules();
        $result = ApiModule::$defaultSuccessResponse;
//        $result['cache_request_materials']          = $mCustomerRequest->getDataMaterial();
        $appFormat = 'd/m/Y'; $searchDateTo = date('Y-m-d');
        $result['cache_module']                     = $mModules->getCustomArrAllModule();
        $result['list_action_customer_request']     = HandleLabel::JsonIdName($mGasSupportCustomer->getListActionInvest());
        $result['search_date_from']                 = MyFormat::modifyDays($searchDateTo, 15, '-', 'day', $appFormat);
        $result['search_date_to']                   = MyFormat::dateConverYmdToDmy($searchDateTo, $appFormat);
        ApiModule::sendResponse($result, $this);
    }
}

