<?php

class DefaultController extends ApiController
{
    public function actionIndex()
    {
        $this->render('index');
    }

    /**
     * This is the action to handle external exceptions.
     */
    public function actionError()
    {
        if($error = Yii::app()->errorHandler->error)
        {
            $arr = array(
                'status' => 0,
                'code' => $error['code'],
                'message'=>$error['message']
            );
            Apimodule::sendResponse($arr, $this);
        }
    }

    /**
     * @Author: ANH DUNG May 03, 2016
     * @Todo: xử lý tracking lại máy tính đại lý từ C# window
     * @Resource: default/monitorAgent
     * @ex_request: http://spj.daukhimiennam.com/api/default/monitorAgent?code_account=ch1
     * @ex_request: http://localhost/gas/api/default/monitorAgent?code_account=ch1
     */
    public function actionMonitorAgent()
    {
        try{
            $model = new GasMonitorAgent();
            $this->checkRequestMonitorAgent($model);
            if(isset($_POST['code_account'])){
                $model->SaveRecord($_POST['code_account']);
            }
            die;
        } catch (Exception $ex) {
            MyFormat::ApiCatchError($ex, $this);
        }
    }
    
    /**
     * @Author: ANH DUNG May 03, 2016
     * @Todo: chỉ cho phép request từ 22h tối đến 23h59' tối
     * @param: $model is model GasMonitorAgent
     */
    public function checkRequestMonitorAgent($model) {
        $hours = date('G');// 24-hour format of an hour without leading zeros
        $minutes = date('i');//
        if($hours < $model->timeBegin){
            die;
        }
    }
    
    /**
     * @Author: ANH DUNG May 13, 2016
     * @Todo: xử lý get thông tin KH từ số phone gọi đến tổng đài điều phối từ C# window
     * @Resource: default/getCustomerByPhone
     * @ex_request: http://spj.daukhimiennam.com/api/default/getCustomerByPhone
     * @ex_request: http://localhost/gas/api/default/getCustomerByPhone
     * @from: window platform
     */
    public function actionGetCustomerByPhone()
    {
        try{
            $result = ApiModule::$defaultResponse;
            $model = new UsersPhone();
            if(isset($_POST['phone'])){
                $window_customer_type = isset($_POST['window_customer_type']) ? $_POST['window_customer_type'] : UsersPhone::WINDOW_TYPE_BO_MOI;
                $aCustomer = $model->getByPhone($_POST['phone'], $window_customer_type);// Reopen on Mar 17, 2017 -- Close ON Mar 14, 2017
                /** @note: Mar 17, 2017 không nên sử dụng hàm bên dưới getByPhoneFixQuery vì khi nó find bên model User có vẻ lâu hơn do table User dữ liệu nhiều
                 * khi find bên UsersPhone thì nhanh hơn vì data ít 
                 */
//                $aCustomer = $model->getByPhoneFixQuery($_POST['phone'], $window_customer_type);// Close ON Mar 17, 2017
                if(count($aCustomer) == 0){
                    $result['message'] = "Tài khoản không tồn tại với số điện thoại: ".$_POST['phone'];
                    ApiModule::sendResponse($result, $this);
                }
                 HandleArrayResponse::GetCustomerByPhone($aCustomer, $this);// ReOpen on Mar 17, 2017 có thể do find chậm. Close ON Mar 14, 2017
//                HandleArrayResponse::GetCustomerByPhoneFix($aCustomer, $this);// Close on Mar 17, 2017
            }
        } catch (Exception $ex) {
            MyFormat::ApiCatchError($ex, $this);
        }
    }
    
    /**
     * @Author: ANH DUNG May 17, 2016
     * @Todo: xử lý get thông tin KH từ keyword search from điều phối từ C# window
     * @Resource: default/getCustomerByKeyword
     * @ex_request: http://spj.daukhimiennam.com/api/default/getCustomerByKeyword
     * @ex_request: http://localhost/gas/api/default/getCustomerByKeyword
     * @from: window platform
     */
    public function actionGetCustomerByKeyword()
    {
        try{
            $result = ApiModule::$defaultResponse;
            $model = new UsersPhone();
            if(isset($_POST['keyword'])){
//                $window_customer_type = isset($_POST['window_customer_type']) ? $_POST['window_customer_type'] : UsersPhone::WINDOW_TYPE_BO_MOI;
                $mUsersPhone    = new UsersPhone();
                $aCustomer      = $mUsersPhone->fixWindowSearch($_POST['keyword']);
                HandleArrayResponse::GetCustomerByKeyword($aCustomer, $this);
            }
        } catch (Exception $ex) {
            MyFormat::ApiCatchError($ex, $this);
        }
    }
    
    /**
     * @Author: ANH DUNG May 17, 2016
     * @Todo: update phone tin KH from điều phối từ C# window
     * @Resource: default/updateCustomerPhone
     * @ex_request: http://spj.daukhimiennam.com/api/default/updateCustomerPhone
     * @ex_request: http://localhost/gas/api/default/updateCustomerPhone
     * @from: window platform
     */
    public function actionUpdateCustomerPhone()
    {
        try{
//            $this->log();
            $result = ApiModule::$defaultResponse;
            $model = new UsersPhone();
            if(!isset($_POST['customer_id'])){
                ApiModule::sendResponse($result, $this);
            }
            $mUser = Users::model()->findByPk($_POST['customer_id']);
            $aRoleUpdate = array(ROLE_CUSTOMER, ROLE_AGENT);// Jul 19, 2016
            if(is_null($mUser) || !in_array($mUser->role_id, $aRoleUpdate)){
                throw new Exception('Customer not found');
            }// Cần review lại chỗ này, có thể post sql ở đây dc
            if($mUser->id == GasConst::UID_VANG_LAI){// Aug 
                throw new Exception('Vui lòng không cập nhật số điện thoại cho KH vãng lai');
            }
            
            $mUser->phone = MyFormat::removeBadCharacters($_POST['phone']);
            $mSms = new GasScheduleSms();
            $mSms->phone = $mUser->phone;
            /* Aug 13, 2016 chỗ này đang xử lý update phone của KH bò mối + phone của đại lý nhận SMS đặt hàng từ điều phối
             */
//            if($mUser->role_id == ROLE_AGENT && !UsersPhone::isValidCellPhone($mUser->phone)){
            if($mUser->role_id == ROLE_AGENT && ( !$mSms->isAgentPhoneValid() || !UsersPhone::isValidCellPhone($mUser->phone) )){
                $errors = implode(" - ", $mSms->aErrors);
                throw new Exception("$errors. Uid: $mUser->id - Phone: $mSms->phone Số điện thoại phải là số di động của Viettel. Từ ".GasScheduleSms::AGENT_HOURS_CHECK."h trở đi bạn có thể nhập số của các mạng Viettel, Vina, Mobi");
            }
            $mUser->update(array('phone', 'address_vi'));// May 23, 2016 update lai address_vi để search ở web cho đúng
            UsersPhone::savePhone($mUser);
            $mUser->solrAdd();
            $result = ApiModule::$defaultSuccessResponse;
            $result['message'] = 'Cập nhật số điện thoại thành công';
            ApiModule::sendResponse($result, $this);
        } catch (Exception $ex) {
            MyFormat::ApiCatchError($ex, $this);
        }
    }

    /**
     * @Author: ANH DUNG Jun 03, 2016
     * @Todo: get config for user kế toán BH, điều phối of window
     * @Resource: http://spj.daukhimiennam.com/api/default/windowGetConfig
     * @word_index:  Method: POST 
     * @ex_json_request: {"token":"dc36cc6fb2db91e515372db1821b6275","agent_id":111}
     * @from: window platform
     */
    public function actionWindowGetConfig()
    {
        try{
        $result = ApiModule::$defaultResponse;
        $this->checkRequest();
        $q = json_decode($_POST['q']);
        $this->checkRequiredParams($q, array('token'));
        $mUser = $this->getUserByToken($result, $q->token);// Oct 12, 2015 for check inactive or delete User
        HandleArrayResponse::WindowGetConfig($mUser, $this, $q);
        } catch (Exception $ex) {
            MyFormat::ApiCatchError($ex, $this);
        }
    }
    
    /**
     * @Author: ANH DUNG Jun 03, 2016
     * @Todo: get config for user kế toán BH, điều phối of window
     * @Resource: http://spj.daukhimiennam.com/api/default/windowGetInfoAgent
     * @word_index:  Method: POST 
     * @ex_json_request: {"token":"dc36cc6fb2db91e515372db1821b6275","agent_id"}
     * @from: window platform
     */
    public function actionWindowGetInfoAgent()
    {
        try{
        $result = ApiModule::$defaultResponse;
        $this->checkRequest();
        $q = json_decode($_POST['q']);
        $this->checkRequiredParams($q, array('token','agent_id'));
        $mUser = $this->getUserByToken($result, $q->token);// Oct 12, 2015 for check inactive or delete User
        HandleArrayResponse::windowGetInfoAgent($mUser, $this, $q);
        } catch (Exception $ex) {
            MyFormat::ApiCatchError($ex, $this);
        }
    }
    
    /**
     * @Author: ANH DUNG Jun 13, 2016
     * @Todo: get order from windown
     * @Resource: http://spj.daukhimiennam.com/api/default/windowOrderCreate
     * @word_index:  Method: POST 
     * @ex_json_request: {"token":"98985e3ae4f0b83b46ac9b8fbbb00e4d","customer_id":123456,
     * "created_date"":"","order_type":2,"type_amount":300000
     * "agent_id":113,"employee_maintain_id":777,"monitor_market_development_id":888,"order_detail":[{"materials_id":35,"materials_type_id":2,"qty":1,"price":500000,"amount":500000,"seri":"A1263"},{"materials_id":38,"materials_type_id":3,"qty":1,"price":650000,"amount":650000,"seri":"61263"}]}
     * @from: window platform
     */
    public function actionWindowOrderCreate()
    {
        try{
//            $this->log();// only for dev debug
        $result = ApiModule::$defaultResponse;
        $this->checkRequest();
        $q = json_decode($_POST['q']);
        $this->checkRequiredParams($q, array('token', 'customer_id', 'agent_id', 'order_detail'));
        $mUser = $this->getUserByToken($result, $q->token);// Oct 12, 2015 for check inactive or delete User
        // 2. get post and validate
        $mSell = new Sell('WindowCreate');
        $mSell->windowGetPost($q, $mUser);
        if(in_array($q->agent_id, UsersExtend::getAgentRunAppGN())){
            throw new Exception('Không thể tạo đơn hàng. Đơn hàng đại lý bạn phải được tạo bởi tổng đài hộ GĐ');
        }
        if($mSell->hasErrors()){
            $result['message'] = HandleLabel::FortmatErrorsModel($mSell->getErrors());
            ApiModule::sendResponse($result, $this);
        }

        $result = ApiModule::$defaultSuccessResponse;
        $result['message'] = 'Tạo mới đơn hàng thành công';
        $mSell->windowSave();
        $mSell->makeTransactionHistory();// Dec 10, 2016 only run at Window create from PMBH C#
//        $mSell->autoSaveStoreCardHgd();// Close on Aug 21, 2016 Fix by cho chay cron 1h 1 lần
        $result['id'] = $mSell->id.'';
        ApiModule::sendResponse($result, $this);
        } catch (Exception $ex) {
            MyFormat::ApiCatchError($ex, $this);
        }
    }
    
    /**
     * @Author: ANH DUNG Jun 13, 2016
     * @Todo: get order update from windown
     * @Resource: http://spj.daukhimiennam.com/api/default/windowOrderUpdate
     * @word_index:  Method: POST 
     * @ex_json_request: {"token":"98985e3ae4f0b83b46ac9b8fbbb00e4d","id":113,"order_detail":[{"materials_id":35,"materials_type_id":2,"qty":1,"price":500000,"amount":500000,"seri":"A1263"},{"materials_id":38,"materials_type_id":3,"qty":1,"price":650000,"amount":650000,"seri":"61263"}]}
     * @from: window platform
     */
    public function actionWindowOrderUpdate()
    {
        try{
        $result = ApiModule::$defaultResponse;
        $this->checkRequest();
        $q = json_decode($_POST['q']);
        $this->checkRequiredParams($q, array('token', 'id', 'order_detail'));
        $mUser = $this->getUserByToken($result, $q->token);// Oct 12, 2015 for check inactive or delete User
        // 2. get post and validate
        $mSell = Sell::model()->findByPk($q->id);
        if(is_null($mSell)){
            throw new Exception("Đơn hàng không hợp lệ hoặc đã bị xóa");
        }
        if(in_array($mSell->agent_id, UsersExtend::getAgentRunAppGN())){
            throw new Exception('Không thể cập nhật đơn hàng. NV Giao Nhận phải cập nhật qua App Gas Service');
        }
//        $mSell->checkDayUpdate(); // đã check ở hàm windowUpdateDetail
        
        $mSell->scenario = 'WindowUpdate';
        $mSell->windowUpdateDetail($q);
        if($mSell->hasErrors()){
            $result['message'] = HandleLabel::FortmatErrorsModel($mSell->getErrors());
            ApiModule::sendResponse($result, $this);
        }
//        $mSell->autoSaveStoreCardHgd();// Close on Aug 21, 2016 Fix by cho chay cron 1h 1 lần
        $result = ApiModule::$defaultSuccessResponse;
        $result['message'] = 'Cập nhật đơn hàng thành công';
        $result['id'] = $mSell->id.'';
        ApiModule::sendResponse($result, $this);
        } catch (Exception $ex) {
            MyFormat::ApiCatchError($ex, $this);
        }
    }
    
    
    /**
     * @Author: ANH DUNG Jun 25, 2016
     * @Todo: save customer ho GD from windown
     * @Resource: http://spj.daukhimiennam.com/api/default/windowCustomerCreate
     * @word_index:  Method: POST 
     * @ex_json_request: {"token":"98985e3ae4f0b83b46ac9b8fbbb00e4d","agent_id":123456,"first_name":"test name","phone":777,"province_id":888,"district_id":"1","ward_id"=>2,"street_id"=>3,"house_numbers"=>"so 3 duong so 3"}
     * @from: window platform
     */
    public function actionWindowCustomerCreate()
    {
        try{
        $result = ApiModule::$defaultResponse;
        $this->checkRequest();
        $q = json_decode($_POST['q']);
        $this->checkRequiredParams($q, array('token', 'agent_id','first_name', 'phone', 'province_id', 'district_id', 'ward_id', 'street_id', 'house_numbers'));
        $mUser = $this->getUserByToken($result, $q->token);// Oct 12, 2015 for check inactive or delete User
        // 2. get post and validate
        usleep(rand(100000, 3000000));// 0.5->3s nên test kỹ lại với 3 đến 4 máy cùng 1 user submit 1 lúc random sleep khoảng 0.5s -> 3 second 
        $mCustomer = new Users('WindowCreateHgd');
        $mCustomer->windowGetPost($q, $mUser);
        if($mCustomer->hasErrors()){
            $result['message'] = HandleLabel::FortmatErrorsModel($mCustomer->getErrors());
            ApiModule::sendResponse($result, $this);
        }

        $result = ApiModule::$defaultSuccessResponse;
        $result['message'] = 'Tạo mới khách hàng thành công';
        MyFunctionCustom::saveCustomerStoreCard($mCustomer);
        // May 13, 2016 update Phone KH
        UsersPhone::savePhone($mCustomer);
        $mCustomer->solrAdd();
        // Jun 30, 2016 trả về kiểu search phone cho KH
        $mUserPhone = new UsersPhone();
        $aCustomer = $mUserPhone->getByPhone($mCustomer->phone, UsersPhone::WINDOW_TYPE_HGD);
        HandleArrayResponse::GetCustomerByPhone($aCustomer, $this);
        // Jun 30, 2016 trả về kiểu search phone cho KH
        } catch (Exception $ex) {
            MyFormat::ApiCatchError($ex, $this);
        }
    }
   
    /**
     * @Author: ANH DUNG Jul 11, 2016
     * @Todo: create store card (thẻ kho) from windown
     * @Resource: http://spj.daukhimiennam.com/api/default/windowStoreCardCreate
     * @word_index:  Method: POST 
     * @ex_json_request: {"token":"03584fb7f71aa6d23c9ddae45ed434d1","customer_id":367071,"agent_id":106,"note":"ADMIN TEST","b50":1,"b45":1,"b12":1,"b6":3,"note_customer":"tesst note_customer","date_delivery":"18-03-2017","type":1}
     * {"token":"03584fb7f71aa6d23c9ddae45ed434d1","customer_id":367071,"agent_id":106,"note":"ADMIN TEST","b50":1,"b45":0,"b12":0,"b6":0,"note_customer":"tesst note_customer","date_delivery":"28-04-2017","type":1}
     * @from: window platform
     */
    public function actionWindowStoreCardCreate()
    {
        try{
        $result = ApiModule::$defaultResponse;
        $this->checkRequest();
        $q = json_decode($_POST['q']);
        $this->checkRequiredParams($q, array('token', 'customer_id', 'agent_id', 'note'));
        $mUser = $this->getUserByToken($result, $q->token);// Oct 12, 2015 for check inactive or delete User
        
        $mStoreCard = new GasStoreCard();
        $mStoreCard->date_delivery = $this->windowStoreCardCreateCheckRequest($q);
        $mStoreCard->mAppUserLogin = $mUser;
        $mUsersPrice =new UsersPrice();
        $aInfoPrice = $mUsersPrice->getPriceOfCustomer(date('m'), date('Y'), $q->customer_id);
        if($mStoreCard->needSplit($q) && $q->type == GasAppOrder::STORECARD_NORMAL){
//            Logger::WriteLog('Debug actionWindowStoreCardCreate Chia thẻ kho: '.json_encode($q));
            $mStoreCard->handleSplitOrderBig($q, $aInfoPrice);
        }else{
//            Logger::WriteLog('Debug actionWindowStoreCardCreate Thẻ kho Binh Thuong: '.json_encode($q));
            $mStoreCard->makeOneStoreCard($q, $aInfoPrice);
        }
        
        $result = ApiModule::$defaultSuccessResponse;
        $result['message'] = 'Tạo mới thẻ kho thành công';
        $result['id'] = '1';
        ApiModule::sendResponse($result, $this);
        } catch (Exception $ex) {
            MyFormat::ApiCatchError($ex, $this);
        }
    }
    
    /**
     * @Author: ANH DUNG Apr 28, 2017
     * @Todo: xử lý check request hợp lệ
     */
    public function windowStoreCardCreateCheckRequest($q) {
        try{
            $aAgentLimit = [112, GasConst::AGENT_QUAN_4, MyFormat::KHO_BEN_CAT, MyFormat::KHO_PHUOC_TAN];
            if(in_array($q->agent_id, $aAgentLimit)){
                throw new Exception('Thủ Đức 1, Đại lý Quận 4, Kho Bến Cát, Kho Phước Tân không thể tạo thẻ kho. Vui lòng chọn đại lý khác');
            }

            $date_delivery  = MyFormat::dateDmyToYmdForAllIndexSearch($q->date_delivery);
            $dateCheck      = MyFormat::modifyDays(date('Y-m-d'), 1);
            if(MyFormat::compareTwoDate($date_delivery, $dateCheck)){
                throw new Exception('Ngày tạo thẻ kho không hợp lệ: '.$q->date_delivery);
            }
            return $date_delivery;
        } catch (Exception $ex) {
            MyFormat::ApiCatchError($ex, $this);
        }
    }
    
    /**
    * @Author: ANH DUNG Jul 12, 2016
    * @Todo: map post to model
    
    public function windowGetPostStoreCard($q, &$mStoreCard, $mUser) {
        $aInfoPrice = UsersPrice::getPriceOfCustomer(date('m'), date('Y'), $q->customer_id);
        $mStoreCard->user_id_create = $q->agent_id;
        $mStoreCard->customer_id    = $q->customer_id;
        $mStoreCard->note           = trim($q->note).'. '.$aInfoPrice['sPrice'];
        $mStoreCard->uid_login      = $mUser->id;
        $mStoreCard->date_delivery  = MyFormat::dateDmyToYmdForAllIndexSearch($q->date_delivery);
        $mStoreCard->getTypeCustomer();// get type_user
        $mStoreCard->type_store_card    = TYPE_STORE_CARD_EXPORT;
        $mStoreCard->type_in_out        = STORE_CARD_TYPE_3;
        $mStoreCard->pay_now            = GasStoreCard::CREATE_FROM_WINDOW;
        $mStoreCard->buildStoreCardNo();
        $mStoreCard->validate();
        $mStoreCard->checkChanHang();
   }
    */
   
    /**
    * @Author: ANH DUNG Oct 18, 2016
    * @Todo: sinh tự động phiếu nhập vỏ
     * @param: $mStoreCard model storecard xuất bán
//    public function windowGetPostStoreCardMakeNhapVo($q, $mStoreCard, $mUser) {
//        $mStoreCardNhapVo                       = new GasStoreCard('WindowCreate');
//        $mStoreCardNhapVo->user_id_create       = $mStoreCard->user_id_create;
//        $mStoreCardNhapVo->customer_id          = $mStoreCard->customer_id;
//        $mStoreCardNhapVo->uid_login            = $mUser->id;
//        $mStoreCardNhapVo->date_delivery        = $mStoreCard->date_delivery;
//        $mStoreCardNhapVo->type_user            = $mStoreCard->type_user;// loai KH
//        $mStoreCardNhapVo->type_store_card      = TYPE_STORE_CARD_IMPORT;
//        $mStoreCardNhapVo->type_in_out          = STORE_CARD_TYPE_5;
//        $mStoreCardNhapVo->pay_now              = GasStoreCard::CREATE_FROM_WINDOW;
//        $mStoreCardNhapVo->buildStoreCardNo();
//        if($q->type == GasAppOrder::STORECARD_THU_VO){
//            $mStoreCardNhapVo->note             = $mStoreCard->note;
//        }
//        $mStoreCardNhapVo->save();
//        return $mStoreCardNhapVo;
////        $info = json_encode($mStoreCardNhapVo->getErrors());
////        Logger::WriteLog($info);
//    }
   */
    /**
     * @Author: ANH DUNG Jan 21, 2017
     * @Todo: make record AppOrder
     
    public function makeAppOrder($q, &$mAppOrder, $mUser, $result, $mStoreCard) {
        $mAppOrder->mAppUserLogin   = $mUser;
        $mAppOrder->customer_id     = $q->customer_id;
        $mAppOrder->agent_id        = $q->agent_id;
        $mAppOrder->note_employee   = $mStoreCard->note;
        $mAppOrder->date_delivery   = $mStoreCard->date_delivery;// add Mar 19, 2017
        $mAppOrder->type            = GasAppOrder::DELIVERY_NOW;// add Mar 19, 2017
        $mAppOrder->isDieuPhoiCreate = true;
        $mAppOrder->handlePost($q);
        if($mAppOrder->hasErrors()){
            $result['message'] = HandleLabel::FortmatErrorsModel($mAppOrder->getErrors());
            ApiModule::sendResponse($result, $this);
        }
        $mAppOrder->status = GasAppOrder::STATUS_CONFIRM;
        $mAppOrder->handleSave();
        $mAppOrder->notifyEmployeeMaintain();
    }
     */
   
    /**
     * @Author: ANH DUNG Aug 26, 2016
     * @Todo: get order history of customer
     * @Resource: http://spj.daukhimiennam.com/api/default/windowGetCustomerHistory
     * @word_index:  Method: POST 
     * @ex_json_request: {"token":"dc36cc6fb2db91e515372db1821b6275","customer_id":123}
     * @from: window platform
     */
    public function actionWindowGetCustomerHistory()
    {
        try{
        $result = ApiModule::$defaultResponse;
        $this->checkRequest();
        $q = json_decode($_POST['q']);
        $this->checkRequiredParams($q, array('token','customer_id'));
        $mUser = $this->getUserByToken($result, $q->token);// Oct 12, 2015 for check inactive or delete User
        
        $type = isset($q->type) ? $q->type : CallHistory::TYPE_HGD;
        
        $result             = ApiModule::$defaultSuccessResponse;
        $result['message']  = 'Get customer history success';
        if($type == CallHistory::TYPE_HGD){
            $mSell              = new Sell();
            $mSell->customer_id = $q->customer_id;
            $result['record']   = $mSell->windowGetHistory();
        }else{
            $mStorecard         = new GasStoreCard();
            $mStorecard->customer_id = $q->customer_id;
            $result['record']   = $mStorecard->windowGetHistory();
        }
        ApiModule::sendResponse($result, $this);
        
        } catch (Exception $ex) {
            MyFormat::ApiCatchError($ex, $this);
        }
    }
    
    /**
     * @Author: ANH DUNG Oct 26, 2016
     * @Todo: save call history from windown
     * @Resource: http://spj.daukhimiennam.com/api/default/callHistorySave
     * @word_index:  Method: POST 
     * @ex_json_request: {"token":"98985e3ae4f0b83b46ac9b8fbbb00e4d","customer_id":123456,"agent_id":113,"note":"123"}
     * @from: window platform
     */
    public function actionCallHistorySave()
    {
        try{
        $result = ApiModule::$defaultResponse;
        $this->checkRequest();
        $q = json_decode($_POST['q']);
        $this->checkRequiredParams($q, array('token', 'agent_id', 'call_id'));
        $mUser = $this->getUserByToken($result, $q->token);// Oct 12, 2015 for check inactive or delete User
        // 1. get list agent from cache
        $mAppCache = new AppCache();
        $aCacheAgent = $mAppCache->getAgent();
        // 2. get post and validate
        $mCallHistory = CallHistory::loadModelById($q);
        $mCallHistory->windowGetPost($q, $mUser, $aCacheAgent);

        if($mCallHistory->hasErrors()){
            $result['message'] = HandleLabel::FortmatErrorsModel($mCallHistory->getErrors());
            ApiModule::sendResponse($result, $this);
        }

        $mCallHistory->windowSave();
        $result = ApiModule::$defaultSuccessResponse;
        $result['message']  = 'Lưu call history thành công';
        $result['id']       = $mCallHistory->id.'';
        ApiModule::sendResponse($result, $this);
        } catch (Exception $ex) {
            MyFormat::ApiCatchError($ex, $this);
        }
    }
    
    /**
     * @Author: ANH DUNG Jan 23, 2017
     * @Todo: create đơn hàng xe tải from windown
     * @Resource: http://spj.daukhimiennam.com/api/default/windowOrderCar
     * @word_index:  Method: POST 
     * @ex_json_request: {"token":"98985e3ae4f0b83b46ac9b8fbbb00e4d","customer_id":123456,"user_id_executive":113,"b50":"3","b45":"3","b12":"3","b6":"2"}
     * @from: window platform
     */
    public function actionWindowOrderCar(){
        try{
        $result = ApiModule::$defaultResponse;
        $this->checkRequest();
        $q = json_decode($_POST['q']);
        $this->checkRequiredParams($q, array('token', 'customer_id', 'user_id_executive', 'b50', 'b45', 'b12', 'b6'));
        $mUser = $this->getUserByToken($result, $q->token);// Oct 12, 2015 for check inactive or delete User
        // 2. get post and validate
        
        // Jan 23, 2017 for test
//        $q = new stdClass();
//        $q->user_id_executive = 109107;
//        $q->customer_id = 163619;
//        $q->b50 = 12;
//        $q->b45 = 62;
//        $q->b12 = 63;
//        $q->b6 = 69;
//        $mUser = Users::model()->findByPk(4216);
        // Jan 23, 2017 for test
        
        // 1. Check chặn hàng
        $mStoreCard                 = new GasStoreCard();
        $mStoreCard->customer_id    = $mUser->customer_id;
        $mStoreCard->checkChanHang();
        $error  = $mStoreCard->getError('customer_id').'';
        if(!empty($error)){
            throw new Exception($error);
        }
        
        $date_delivery = MyFormat::getNextDay();
        $mOrderCar = GasOrders::getByDateAndUserExecutive($date_delivery, $q->user_id_executive, $mUser->id);
        if(empty($mOrderCar->id)){
            throw new Exception('Dữ liệu không hợp lệ, vui lòng kiểm tra lại');
        }
        $mOrderCar->date_delivery   = $date_delivery;
        $mOrderCar->mAppUserLogin   = $mUser;
        $mOrderCar->scenario        = 'WindowCreate';
        $mOrderCar->windowGetPost($q, $mUser);
        
        if($mOrderCar->hasErrors()){
            $result['message'] = HandleLabel::FortmatErrorsModel($mOrderCar->getErrors());
            ApiModule::sendResponse($result, $this);
        }

        $result = ApiModule::$defaultSuccessResponse;
        $result['message'] = 'Tạo mới đơn hàng xe tải thành công';
        $mOrderCar->saveWindowRecord();
        
//        $mStoreCard->makeSocketNotify($mUser);// Oct 02, 2016
//        GasScheduleSms::storeCardCreate($mStoreCard, $this);// Jul 19, 2016
        $result['id'] = $mOrderCar->id.'';
        ApiModule::sendResponse($result, $this);
        } catch (Exception $ex) {
            MyFormat::ApiCatchError($ex, $this);
        }
    }
    
    /**
{"type":2,"app_type":2,"version_code":"81130","platform":1,"token":"","acc":"huydd","username":"huydd","password":"123123","gcm_device_token":"","apns_device_token":"","device_name":"","device_imei":"354603070127314","device_os_version":"6.0.1"}
     * @Author: ANH DUNG Dec0218
     * @Todo: tạo cân xe tải lần 1 from windown
     * @Resource: http://spj.daukhimiennam.com/api/default/scalesFirst
     * @word_index:  Method: POST 
     * @ex_json_request: {"token":"98985e3ae4f0b83b46ac9b8fbbb00e4d","id":0,"type":1,"car_number":113,"car_owner":"3","cargo_owner":"3","cargo_name":"3","weight_1":"2","weight_2":"0"}
     * @from: window platform
     */
    public function actionScalesFirst(){
        try{
//            echo '******** ANH DUNG DUMP - POST ***********<pre>';
//            print_r($_POST);
//            echo '</pre><hr><br>';
//            echo '******** ANH DUNG DUMP - FILE ***********<pre>';
//            print_r($_FILES);
//            echo '</pre><hr>';
//            die; 
        $result = ApiModule::$defaultResponse;
        $this->checkRequest();
        $q = json_decode($_POST['q']);
//        Logger::WriteLog("Debug action actionScalesFirst ".$_POST['q']);
        $this->checkRequiredParams($q, array('token', 'id', 'type', 'car_number', 'car_owner', 'cargo_owner', 'cargo_name', 'weight_1', 'weight_2'));
        $mUser = $this->getUserByToken($result, $q->token);// Oct 12, 2015 for check inactive or delete User
        // 2. get post and validate
        $mTruckScales                   = new TruckScales();
        $mTruckScales->mAppUserLogin    = $mUser;
        $mTruckScales->scenario         = 'WindowCreate1';
        $mTruckScales->windowGetPostFirst($q);
        // need validate file upload
//        $this->hanleFileValidate($mTruckScales);
        $mTruckScales->handleAppFileValidate();
        if($mTruckScales->hasErrors()){
            $result['message'] = HandleLabel::FortmatErrorsModel($mTruckScales->getErrors());
            ApiModule::sendResponse($result, $this);
        }
        $mTruckScales->saveScalesFirst();
        $mTruckScales->handleAppSaveFile(GasFile::TYPE_14_SCALES_1);

//        $this->hanleFileSave($mTruckScales, GasFile::TYPE_14_SCALES_1);
//        $this->hanleFileDelete($mTruckScales, GasFile::TYPE_14_SCALES_1);
        $mTruckScales->setCarNumberFromImage();
        $result = ApiModule::$defaultSuccessResponse;
        $result['message']  = 'Tạo mới cân lần 1 thành công';
        $mTruckScales       = $mTruckScales->getModelAppView();
        $result['record']   = $mTruckScales->formatAppItemList();
        // next will handle save file upload
        
        ApiModule::sendResponse($result, $this);
        } catch (Exception $ex) {
            MyFormat::ApiCatchError($ex, $this);
        }
    }
    
    /**
     * @Author: ANH DUNG Dec0218
     * @Todo: tạo cân xe tải lần 2 from windown
     * @Resource: http://spj.daukhimiennam.com/api/default/scalesSecond
     * @word_index:  Method: POST 
     * @ex_json_request: {"token":"98985e3ae4f0b83b46ac9b8fbbb00e4d","id":123,"type":1,"car_number":113,"car_owner":"3","cargo_owner":"3","cargo_name":"3","weight_1":"2","weight_2":"5"}
     * @from: window platform 
     */
    public function actionScalesSecond(){
        try{
        $result = ApiModule::$defaultResponse;
        $this->checkRequest();
        $q = json_decode($_POST['q']);
        $this->checkRequiredParams($q, array('token', 'id', 'type', 'car_number', 'car_owner', 'cargo_owner', 'cargo_name', 'weight_1', 'weight_2'));
        $mUser = $this->getUserByToken($result, $q->token);// Oct 12, 2015 for check inactive or delete User
        // 2. get post and validate
        $mTruckScales                   = new TruckScales();
        $mTruckScales->id               = $q->id;
//        $mTruckScales->uid_login        = $mUser->id;//  Jan419 do 1 xe cân ở 2 người khác nhau (2 ca), nên sẽ chỉ find theo id
        $mTruckScales                   = $mTruckScales->getModelApp();
        if(is_null($mTruckScales)){
            throw new Exception('Phiếu cân yêu cầu không hợp lệ 1');
        }
        if(!empty($mTruckScales->time_2) && $mTruckScales->time_2 != '0000-00-00 00:00:00'){
            throw new Exception('Phiếu cân yêu cầu không hợp lệ, đã cân xong lần 2 không thể cân tiếp');
        }
            
        $mTruckScales->scenario         = 'WindowCreate2';
        $mTruckScales->windowGetPostSecond($q);
        // need validate file upload
//        $this->hanleFileValidate($mTruckScales);
        $mTruckScales->handleAppFileValidate();
        if($mTruckScales->hasErrors()){
            $result['message'] = HandleLabel::FortmatErrorsModel($mTruckScales->getErrors());
            ApiModule::sendResponse($result, $this);
        }
        $mTruckScales->saveScalesSecond($q);
        // next will handle save file upload
        $mTruckScales->handleAppSaveFile(GasFile::TYPE_15_SCALES_2);
//        $this->hanleFileSave($mTruckScales, GasFile::TYPE_15_SCALES_2);
//        $this->hanleFileDelete($mTruckScales, GasFile::TYPE_15_SCALES_2);
        
        $result = ApiModule::$defaultSuccessResponse;
        $result['message'] = 'Cập nhật cân lần 2 thành công';
        $mTruckScales       = $mTruckScales->getModelAppView();
        $result['record']   = $mTruckScales->formatAppItemList();
        ApiModule::sendResponse($result, $this);
        } catch (Exception $ex) {
            MyFormat::ApiCatchError($ex, $this);
        }
    }
    
    /**
     * @Author: ANH DUNG Dec0218
     * @Todo: Xem phiếu cân xe tải from windown
     * @Resource: http://spj.daukhimiennam.com/api/default/scalesView
     * @word_index:  Method: POST 
     * @ex_json_request: {"token":"98985e3ae4f0b83b46ac9b8fbbb00e4d","id":123}
     * @from: window platform
     */
    public function actionScalesView(){
        try{
        $result = ApiModule::$defaultResponse;
        $this->checkRequest();
        $q = json_decode($_POST['q']);
        $this->checkRequiredParams($q, array('token', 'id'));
        $mUser = $this->getUserByToken($result, $q->token);// Oct 12, 2015 for check inactive or delete User
        // 2. get post and validate
        $mTruckScales                   = new TruckScales();
        $mTruckScales->id               = $q->id;
        $mTruckScales                   = $mTruckScales->getModelAppView();
        if(is_null($mTruckScales)){
            throw new Exception('Phiếu cân yêu cầu không hợp lệ');
        }
        $mTruckScales->mAppUserLogin    = $mUser;
        $result = ApiModule::$defaultSuccessResponse;
        $result['message']  = '';
        $result['record']   = $mTruckScales->formatAppItemList();
        ApiModule::sendResponse($result, $this);
        } catch (Exception $ex) {
            MyFormat::ApiCatchError($ex, $this);
        }
    }
    
    /**
     * @Author: ANH DUNG Dec0218
     * @Todo: List phiếu cân xe tải from windown
     * @Resource: http://spj.daukhimiennam.com/api/default/scalesList
     * @word_index:  Method: POST 
     * @ex_json_request: {"token":"3d2ea54fe2904905d8c3630f32638e95","page":"0","date_from":"15-12-2018","date_to":"15-12-2018"}
     * @from: window platform
     */
    public function actionScalesList(){
        try{
        $result = ApiModule::$defaultResponse;
        $this->checkRequest();
        $q = json_decode($_POST['q']);
        $this->checkRequiredParams($q, array('token', 'page', 'date_from', 'date_to'));
        $mUser = $this->getUserByToken($result, $q->token);// Oct 12, 2015 for check inactive or delete User
        // 2. get post and validate
        $agent_id   = $this->getAgentOfEmployee($mUser);
        $mTruckScales                   = new TruckScales();
        $mTruckScales->mAppUserLogin    = $mUser;
        $mTruckScales->agent_id         = $agent_id;
        $mTruckScales->mAppUserLogin    = $mUser;
        
        $result = ApiModule::$defaultSuccessResponse;
        $mTruckScales->handleApiList($result, $q);
        ApiModule::sendResponse($result, $this);
        } catch (Exception $ex) {
            MyFormat::ApiCatchError($ex, $this);
        }
    }
    
    /**
{"type":2,"app_type":2,"version_code":"81130","platform":1,"token":"","acc":"huydd","username":"huydd","password":"123123","gcm_device_token":"","apns_device_token":"","device_name":"","device_imei":"354603070127314","device_os_version":"6.0.1"}
     * @Author: ANH DUNG Dec0218
     * @Todo: tạo cân xe tải lần 1 from windown
     * @Resource: http://spj.daukhimiennam.com/api/default/scalesSyncOffline
     * @word_index:  Method: POST 
     * @ex_json_request: {"token":"98985e3ae4f0b83b46ac9b8fbbb00e4d","id":0,"type":1,"car_number":113,"car_owner":"3","cargo_owner":"3","cargo_name":"3","weight_1":"2","weight_2":"6","time_1":"2018-12-25 10:45:00","time_2":"2018-12-25 11:10:46"}
     * @from: window platform
     */
    public function actionScalesSyncOffline(){
        try{
        $result = ApiModule::$defaultResponse;
        $this->checkRequest();
        $q = json_decode($_POST['q']);
        $this->checkRequiredParams($q, array('token', 'id', 'type', 'car_number', 'car_owner', 'cargo_owner', 'cargo_name', 'weight_1', 'weight_2'));
        $mUser = $this->getUserByToken($result, $q->token);// Oct 12, 2015 for check inactive or delete User
        // 2. get post and validate
        $mTruckScales                   = new TruckScales();
        $mTruckScales->mAppUserLogin    = $mUser;
        $mTruckScales->scenario         = 'WindowSyncOffline';
        $mTruckScales->is_online        = TruckScales::MODE_OFFLINE;
        $mTruckScales->windowGetPostFirst($q);
        $mTruckScales->windowGetPostOffline($q);
        // need validate file upload
//        $this->hanleFileValidate($mTruckScales);
        $mTruckScales->handleAppFileValidate();
        if($mTruckScales->hasErrors()){
            $result['message'] = HandleLabel::FortmatErrorsModel($mTruckScales->getErrors());
            ApiModule::sendResponse($result, $this);
        }
        $mTruckScales->saveScalesFirst();
        $mTruckScales->handleAppSaveFile(GasFile::TYPE_14_SCALES_1);
        $mTruckScales->setCarNumberFromImage();
        $result = ApiModule::$defaultSuccessResponse;
        $result['message']  = 'Đồng bộ record thành công';
        $mTruckScales       = $mTruckScales->getModelAppView();
        $result['record']   = $mTruckScales->formatAppItemList();
        // next will handle save file upload
        
        ApiModule::sendResponse($result, $this);
        } catch (Exception $ex) {
            MyFormat::ApiCatchError($ex, $this);
        }
    }
    
    /**
     * @Author: ANH DUNG Dec2618
     * @Todo: List xe tải cân lần 1 from windown
     * @Resource: http://spj.daukhimiennam.com/api/default/scalesListCar
     * @word_index:  Method: POST 
     * @ex_json_request: {"token":"57d6ec2de869c53e7a7c0c3e38187478","page":"0"}
     * @from: window platform
     */
    public function actionScalesListCar(){
        try{
        $result = ApiModule::$defaultResponse;
        $this->checkRequest();
        $q = json_decode($_POST['q']);
        $this->checkRequiredParams($q, array('token', 'page'));
        $mUser = $this->getUserByToken($result, $q->token);// Oct 12, 2015 for check inactive or delete User
        // 2. get post and validate
        $agent_id   = $this->getAgentOfEmployee($mUser);
        $mTruckPlanDetail                   = new TruckPlanDetail();
        $mTruckPlanDetail->mAppUserLogin    = $mUser;
        $mTruckPlanDetail->customer_id      = $agent_id;
        
        $result = ApiModule::$defaultSuccessResponse;
        $mTruckPlanDetail->handleApiListTruckPlan($result, $q);
        ApiModule::sendResponse($result, $this);
        } catch (Exception $ex) {
            MyFormat::ApiCatchError($ex, $this);
        }
    }
    
    
}
