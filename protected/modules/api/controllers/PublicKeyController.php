<?php 
class PublicKeyController extends ApiController
{

    /** @Author: NamNH Nov 24, 2018
     *  @Todo: Lấy public key dựa vào id truyền lên
     *  @Resource: PublicKey/GetPublicKey
     *  @ex_json_request: {"id":"1"}
     **/
    public function actionGetPublicKey(){
        $result     = ApiModule::$defaultResponse;
        $this->checkRequest();
        $q          = json_decode($_POST['q']);
        $this->checkRequiredParams($q, array('id'));
        $mPublicKey = new PublicKey();
        $aApiPublic = $mPublicKey->apiPublicKey($q->id);
        if(empty($aApiPublic)){
            $result['message']           = 'Không tìm thấy public key';
        }else{
            $result     = ApiModule::$defaultSuccessResponse;
            $result['record']           = $aApiPublic;
        }
        ApiModule::sendResponse($result, $this);
    }
    
    /** @Author: NamNH Nov 24, 2018
     *  @Todo: check otp
     *  @Resource: PublicKey/CheckOtp
     *  @ex_json_request: {"token":"d63794b42dcf2a183dd8b1bb46026a62","otp":"123456"}
     **/
    public function actionCheckOtp(){
        $result     = ApiModule::$defaultResponse;
        $this->checkRequest();
        $q          = json_decode($_POST['q']);
        $this->checkRequiredParams($q, array('token','otp'));
        $mUser      = $this->getUserByToken($result, $q->token);
        $mPublicKey = new PublicKey();
        $mPublicKey->mAppUserLogin = $mUser;
        $isOtp      = $mPublicKey->apiCheckOtp($q->otp);
        if($isOtp){
            $result     = ApiModule::$defaultSuccessResponse;
            $result['message']           = 'Xác nhận thành công';
        }else{
            $result['message']           = 'OTP không hợp lệ';
        }
        ApiModule::sendResponse($result, $this);
    }
    
}

