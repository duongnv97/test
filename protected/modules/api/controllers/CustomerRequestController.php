<?php
/** @Author: HOANG NAM 21/06/2018
 *  @Todo: xử lý phiếu đề xuất thay thế vật tư
 **/
class CustomerRequestController extends ApiController
{
    
    /** @Author: HOANG NAM 21/06/2018
     *  @Todo: lâý danh sách gasSupportCustomer theo token user
     *  @Resource: CustomerRequest/CustomerRequestList
     *  @ex_json_request: {"token":"31c52d8b392104ad22208664e86d1105","page":0}
     *  @Response: 
     **/
    public function actionCustomerRequestList(){
        $result = ApiModule::$defaultResponse;
        $this->checkRequest();
        $mGasSupportCustomerRequest = new GasSupportCustomerRequest();
        $q = json_decode($_POST['q']);
        $this->checkRequiredParams($q, array('token'));
        $mUser = $this->getUserByToken($result, $q->token);
        $mGasSupportCustomerRequest->mAppUserLogin = $mUser;
        $result = ApiModule::$defaultSuccessResponse;
        $mGasSupportCustomerRequest->handleApiList($result,$q);
        ApiModule::sendResponse($result, $this);
    }
    
    /** @Author: HOANG NAM 25/06/2018
     *  @Todo: create gasSupportCustomer 
     *  @Resource: CustomerRequest/CustomerRequestCreate
     *  @ex_json_request: {"token":"31c52d8b392104ad22208664e86d1105","customer_id":2,"json":[{"module_type":1,"module_id":1,"qty":3}],"note":"test","action_invest":1}
     *  @Response:
     **/
    public function actionCustomerRequestCreate(){
        $mGasSupportCustomerRequest = new GasSupportCustomerRequest('apiCreate');
        $this->checkRequest();
        $result = ApiModule::$defaultResponse;
        $q = json_decode($_POST['q']);
        $this->checkRequiredParams($q, array('token','json','customer_id','action_invest'));
        $this->apiCheckJson($q);
        $this->checkEmpty($q,['action_invest']);
        $this->hanleCustomerId($q);
        $mUser = $this->getUserByToken($result, $q->token);
        $mGasSupportCustomerRequest->mAppUserLogin      = $mUser;
        $mGasSupportCustomerRequest->requiredFileUpload = true;
        $this->appValidateFile($q, $mGasSupportCustomerRequest);
        $mGasSupportCustomerRequest->handleApiAttributes($q);
        $mGasSupportCustomerRequest->list_id_image = !empty($q->list_id_image) ? $q->list_id_image : [];
        if(!$mGasSupportCustomerRequest->hasErrors() && $mGasSupportCustomerRequest->save()){
//            đã xử lý dưới model cho web và api chung
//            $this->hanleFileSave($mGasSupportCustomerRequest,GasFile::TYPE_12_SUPPORT_CUSTOMER_REQUEST);
//            $this->hanleFileDelete($mGasSupportCustomerRequest,GasFile::TYPE_12_SUPPORT_CUSTOMER_REQUEST);
            $mGasSupportCustomerRequest->sendEmailAfterCreate();
            $result = ApiModule::$defaultSuccessResponse;
            $temp                       = $mGasSupportCustomerRequest->getApiItem();
            $temp['list_image']         = $mGasSupportCustomerRequest->handleAppFileView();
            $result['record']           = $temp;
            $result['record'] = $temp;
        }
        ApiModule::sendResponse($result, $this);
    }
    
    /** @Author: DungNT May 24, 2019
     *  @Todo: validate file for some event
     **/
    public function appValidateFile($q, $mGasSupportCustomerRequest) {
        $mGasSupportCustomerRequest->handleAppFileValidate();
        $this->responseIfError($mGasSupportCustomerRequest);
    }
    
    /** @Author: HOANG NAM 25/06/2018
     *  @Todo: update gasSupportCustomer
     *  @Resource: CustomerRequest/CustomerRequestUpdate
     *  @ex_json_request: {"token":"31c52d8b392104ad22208664e86d1105","id":205,"customer_id":2,"json":[{"module_type":1,"module_id":1,"qty":4}],"note":"test","action_invest":1}
     *  @Response:
     **/
    public function actionCustomerRequestUpdate(){
        $result = ApiModule::$defaultResponse;
        $this->checkRequest();
        $this->checkRequest();
        $q = json_decode($_POST['q']);
        $this->checkRequiredParams($q, array('token','json','customer_id'));
        $this->apiCheckJson($q);
        $this->checkEmpty($q,['action_invest']);
        $this->hanleCustomerId($q);
        $mGasSupportCustomerRequest = $this->loadModel($q->id);
        $mGasSupportCustomerRequest->scenario = 'apiUpdate';
        if($mGasSupportCustomerRequest->status == GasSupportCustomerRequest::STATUS_NEW || $mGasSupportCustomerRequest->status == GasSupportCustomerRequest::STATUS_PROCESS){
            $mUser = $this->getUserByToken($result, $q->token);
            $mGasSupportCustomerRequest->mAppUserLogin = $mUser;
//            $this->hanleFileValidate($mGasSupportCustomerRequest);
            $this->appValidateFile($q, $mGasSupportCustomerRequest);
            $mGasSupportCustomerRequest->handleApiAttributes($q);
            $mGasSupportCustomerRequest->list_id_image = !empty($q->list_id_image) ? $q->list_id_image : [];
            if(!$mGasSupportCustomerRequest->hasErrors() && $mGasSupportCustomerRequest->update()){
//            đã xử lý dưới model cho web và api chung
//                $this->hanleFileSave($mGasSupportCustomerRequest,GasFile::TYPE_12_SUPPORT_CUSTOMER_REQUEST);
//                $this->hanleFileDelete($mGasSupportCustomerRequest,GasFile::TYPE_12_SUPPORT_CUSTOMER_REQUEST);
                $result = ApiModule::$defaultSuccessResponse;
                $temp                           = $mGasSupportCustomerRequest->getApiItem();
                $temp['list_image']             = $mGasSupportCustomerRequest->handleAppFileView();
                $result['record']               = $temp;
                $result['record'] = $temp;
            }
        }else{
            $result['message'] = 'Yêu cầu đã được phê duyệt';
        }
        ApiModule::sendResponse($result, $this);
    }
    
    /** @Author: HOANG NAM 25/06/2018
     *  @Todo: view gasSupportCustomer 
     *  @Resource: CustomerRequest/CustomerRequestView
     *  @ex_json_request: {"token":"31c52d8b392104ad22208664e86d1105","id":6}
     *  @Response:
     **/
    public function actionCustomerRequestView(){
        $result                     = ApiModule::$defaultResponse;
        $this->checkRequest();
        $q                          = json_decode($_POST['q']);
        $this->checkRequiredParams($q, array('token'));
        $mUser                      = $this->getUserByToken($result, $q->token);
        $result                     = ApiModule::$defaultSuccessResponse;
        $mGasSupportCustomerRequest = $this->loadModel($q->id);
        $mGasSupportCustomerRequest->mAppUserLogin = $mUser;
        $temp                       = $mGasSupportCustomerRequest->getApiItem();
        $temp['list_image']         = $mGasSupportCustomerRequest->handleAppFileView();
        $result['record']           = $temp;
        ApiModule::sendResponse($result, $this);
    }
    
    /** @Author: HOANG NAM 26/06/2018
     *  @Todo: load model
     **/
    public function loadModel($id) {
        try {
            $model = GasSupportCustomerRequest::model()->findByPk($id);
            if ($model === null) {
                Yii::log("The requested page does not exist.");
                throw new CHttpException(404, 'The requested page does not exist.');
            }
            return $model;
        } catch (Exception $exc) {
            GasCheck::CatchAllExeptiong($exc);
        }
    }
    
    /** @Author: HOANG NAM 26/06/2018
     *  @Todo: check json submit
     **/
    public function apiCheckJson($q){
        $isValid = true;
        $arMaterial = $q->json;
        if(is_array($arMaterial) && count($arMaterial) > 0){
            foreach ($arMaterial as $key => $objMaterial) {
                if(!array_key_exists('module_type', $objMaterial) 
                        || !array_key_exists('module_id', $objMaterial) 
                        || !array_key_exists('qty', $objMaterial)){
                    $isValid = false;
                }
                if(!isset($objMaterial->qty) || $objMaterial->qty <= 0 || empty($objMaterial->qty)){
                    $isValid = false;
                }
            }
        }else{
            $isValid = false;
        }
        if(!$isValid)
        {
            $result = ApiModule::$defaultResponse;
//            $result['message'] = 'Định dạng mảng không đúng, mẫu: [{ "module_type":1,"module_id":2,"qty":1,"detail":[{"materials_type_id":74,"materials_id":9,"qty":1}] }]';
            $result['message'] = 'Chưa chọn vật tư hoặc số lượng vật tư không đúng';
            ApiModule::sendResponse($result, $this);
        }
    }
    
    public function hanleCustomerId($q){
        $mUsersCUs = Users::model()->findByPk($q->customer_id);
        if(empty($mUsersCUs)){
            $result = ApiModule::$defaultResponse;
            $result['message'] = 'Khách hàng không tồn tại';
            ApiModule::sendResponse($result, $this);
        }
    }
    
    /**
     * check  empty of param
     * @param object $q
     * @param arr $arrayOfInvalidFields
     */
    public function checkEmpty($q, $arrayOfInvalidFields){
        $isValid = true;
        foreach ($arrayOfInvalidFields as $key => $value) {
            if(empty($q->$value)){
                $isValid    = false;
            }
        }
        if(!$isValid)
        {
            $result = ApiModule::$defaultResponse;
            $result['message'] = 'Missing param. Reference in record: '.json_encode($arrayOfInvalidFields);
            ApiModule::sendResponse($result, $this);
        }
    }
    
}

