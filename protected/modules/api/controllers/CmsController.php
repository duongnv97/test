<?php
/** @Author: NGUYEN KHANH TOAN 2018
*  @Todo: thêm chức năng thông báo cho app gas24h
*  @Param:
**/
class CmsController extends ApiController
{
    /** @Author: NGUYEN KHANH TOAN Sep 08 2018
     *  @Todo: get type category
     *  @Resource: cms/listCategory
     *  @ex_json_request: {"token":"af9fac7d332e8c26c8c6e96127007176"}
     **/
    public function actionListCategory() {
        try {
            $result = ApiModule::$defaultResponse;
            $this->checkRequest();
            $q = json_decode($_POST['q']);
            $this->checkRequiredParams($q, array('token'));
            $mUser = $this->getUserByToken($result, $q->token);// Oct 12, 2015 for check inactive or delete User
            $this->mUserApp = $mUser;
            $result = ApiModule::$defaultSuccessResponse;
            $result['message'] = 'Type';
            $mCms = new Cms();
            $mCms->handleApiListCategory($result, $q);
            ApiModule::sendResponse($result, $this);
        } catch (Exception $ex) {
            MyFormat::ApiCatchError($ex, $this);
        }
    }
    
    /** @Author: NGUYEN KHANH TOAN Sep 08 2018
     *  @Todo: render list category
     *  @Resource: cms/itemList
     *  @ex_json_request: {"token":"af9fac7d332e8c26c8c6e96127007176","category_id":"2","page":"0"}
     **/
    public function actionItemList() {
        try {
            $result = ApiModule::$defaultResponse;
            $this->checkRequest();
            $q = json_decode($_POST['q']);
            $this->checkRequiredParams($q, array('token','category_id','page'));
            $_GET['platform']       = isset($q->platform) ? $q->platform : UsersTokens::PLATFORM_ANDROID;
            $_GET['version_code']   = isset($q->version_code) ? $q->version_code : 1;
            $mUser = $this->getUserByToken($result, $q->token);// Oct 12, 2015 for check inactive or delete User
            $this->mUserApp = $mUser;
            $result = ApiModule::$defaultSuccessResponse;
            $result['message'] = 'List';
            $mCms = new Cms();
            $mCms->mAppUserLogin = $mUser;
            $mCms->handleApiItemList($result, $q);
            ApiModule::sendResponse($result, $this);
        } catch (Exception $ex) {
            MyFormat::ApiCatchError($ex, $this);
        }
    }
    
    /** @Author: NGUYEN KHANH TOAN Sep 08 2018
     *  @Todo: render view item
     *  @Resource: cms/itemView/id/36
     *  @ex_json_request: 
     **/
    public function actionItemView($id){
        try {
        $mCms       = $this->getCmsRecord($id);
        $linkCss    = $mCms->getAppLinkCss();

        $content = "<!DOCTYPE html>
                <html lang='en'>
                <head>
                    <meta charset='UTF-8'>
                    <meta name='viewport' content='width=device-width, initial-scale=1.0'>
                    <meta http-equiv='X-UA-Compatible' content='ie=edge'>
                    <title>Document</title>
                    {$linkCss}
                </head>
                <body>
                    <div class='appContent'>{$mCms->cms_content}</div>
                </body>
                </html>";
        echo $content; 
       
        } catch (Exception $ex) {
            MyFormat::ApiCatchError($ex, $this);
        }
    }
    
    /** @Author: ANH DUNG Oct 07, 2018
     *  @Todo: get record cms valid
     **/
    public function getCmsRecord($id) {
        $mCms               = new Cms();
        $mCms->id           = $id;
        $mCms               = $mCms->getNewsView();
        if(is_null($mCms)){
            throw new Exception('Yêu cầu không hợp lệ');
        }
        return $mCms;
    }
    
    /** @Author: ANH DUNG Oct 07, 2018
     *  @Todo: render list category
     *  @Resource: cms/cmsView
     *  @ex_json_request: {"token":"af9fac7d332e8c26c8c6e96127007176","id":"2"}
     **/
    public function actionCmsView() {
        try {
            $result = ApiModule::$defaultResponse;
            $this->checkRequest();
            $q = json_decode($_POST['q']);
            $this->checkRequiredParams($q, array('token','id'));
            $mUser = $this->getUserByToken($result, $q->token);// Oct 12, 2015 for check inactive or delete User
            $result = ApiModule::$defaultSuccessResponse;
            $mCms                   = $this->getCmsRecord($q->id);
            $mCms->mAppUserLogin    = $mUser;
            $this->writeTracking($mCms, StaView::TYPE_2_GAS_SERVICE_ANOUCE);
            $result['record']       = $mCms->formatAppItemListCms();
            ApiModule::sendResponse($result, $this);
        } catch (Exception $ex) {
            MyFormat::ApiCatchError($ex, $this);
        }
    }
    
    
}



