<?php
/**
 * @Author: DungNT Oct 09, 2015
 * @Todo: something
 */

class SiteController extends ApiController
{
    /** 
     * http://localhost/gas/test/example/client/twoleggedTest.php
     * http://verzview.com/verzownhome/demo/test/example/client/twoleggedTest.php
     * @Author: DungNT Oct 09, 2015
     * @Todo: app login
     * @Resource: GET: site/login?label=1
     * @Resource: POST: site/login
     * @word_index: 2.1
     * @ex_json_request: 
     {"type":2,"app_type":0,"version_code":"81130","platform":1,"token":"","acc":"hoantv","username":"quangnd","password":"221192","gcm_device_token":"","apns_device_token":"","device_name":"","device_imei":"354603070127314","device_os_version":"6.0.1"}
     * 
     {"email":"verz@yahoo.com","password":"hue123","apns_device_token":"1234556","gcm_device_token":""}
     * @ex_array_encode: $paramSend = array('q'=>'{"email":"hue@yahoo.com","password":"hue123"}');
     */
    public function actionLogin() {
        try{
        $result = ApiModule::$defaultResponse;
        $this->HandleLoginGet();
        
        $this->checkRequest();
        $q = json_decode($_POST['q']);
        $this->checkRequiredParams($q, array('username','password'));
        $mUsersTokens = new UsersTokens();
        $mUser = $this->getUserLogin($q);
        $mUsersTokens->validateLogin($this, $mUser, $result, $q);
        if (!$mUser) {// Trung add Oct0517
            $mUser = $this->getUserLogin($q);
        }
        
        $this->handleLoginPost($mUser, $q);
        } catch (Exception $ex) {
            MyFormat::ApiCatchError($ex, $this);
        }
    }

     /**
     * @Author: DungNT Oct 09, 2015
     * @Todo: handle login and save new token
     */
    public function handleLoginPost($mUser, $q) {
        try{
        // 1. check role allow once login in one time in Mobile
        $this->checkRemoveAllToken($mUser);
        // 1.1 save info device login + check lock device
        $mAppLogin = new AppLogin();
        $mAppLogin->handleSave($mUser, $q);
        // 2. make new token for new login
        $mUsersTokens = new UsersTokens();
        $mUsersTokens = $mUsersTokens->makeNewToken($mUser, $q);
        // 3. remove required logout
        MonitorUpdate::removeMobileRequiredLogout($mUser->id);
        // 4. build array respone
        $mResponse = new HandleArrayResponse();
        $mResponse->login($mUsersTokens, $mUser, $this);
        } catch (Exception $ex) {
            throw new Exception($ex->getMessage(), $ex->getCode());
        }
    }
    
    
    /**
     * @Author: DungNT Oct 29, 2017
     * @Todo: kiểm tra user có phải remove all token không
     */
    public function checkRemoveAllToken($mUser) {
        $mAppLogin = new AppLogin();
        $aRoleDeleteToken = [ROLE_EMPLOYEE_MAINTAIN, ROLE_EMPLOYEE_MARKET_DEVELOPMENT];
        // check role allow once login in one time in Mobile
        if((UsersTokens::isCustomerGas24h($mUser) || (in_array($mUser->role_id, $aRoleDeleteToken) && GasCheck::isServerLive())) 
                && Yii::app()->setting->getItem('EnableChangeExt') == 'no'){
            if($mAppLogin->canLoginMultiDevice($mUser->id)){
                return ;
            }
            UsersTokens::deleteAllTokenMobile($mUser->id);
        }// nếu EnableChangeExt = yes thì cho phép login same account để debug
    }

     /**
     * @Author: DungNT Oct 09, 2015
     * @Todo: handle login Get some text for render interface
     */
    public function HandleLoginGet() {
        if(isset($_GET['label'])){
            HandleLabel::Login($this);
        }
    }
    /**
     * @Author: DungNT Jul 14, 2015 copy from Ownhome
     * @Todo: handle logout with token
     * @Resource: site/logout
     * @word_index: 2.3
     * @ex_json_request: {"token":"24fb16785c3b31f8f82a7b055734228e"}
     */
    public function actionLogout()
    {
        try{
        $result = ApiModule::$defaultResponse;
        $this->checkRequest();
        $q = json_decode($_POST['q']);
        $this->checkRequiredParams($q, array('token'));
        $mUser = $this->getUserByToken($result, $q->token);
        // Dec 20, 2015 mở tạm để test
        $mUsersTokens = new UsersTokens();
        $mUsersTokens->user_id  = $mUser->id;
        $mUsersTokens->role_id  = $mUser->role_id;
        $mUsersTokens->token    = $q->token;
        $mUsersTokens->saveTrackLogout($q);
        UsersTokens::logout($q->token); // Now 28, 2015 không xử lý xóa token khi logout vì sẽ để cho user nhận dc notify khi nó logout ra
//        UsersTokens::logout($q->token); Now 28, 2015 không xử lý xóa token khi logout vì sẽ để cho user nhận dc notify khi nó logout ra
        $result = ApiModule::$defaultSuccessResponse;
        $result['message'] = 'Logout success';
        ApiModule::sendResponse($result, $this);
        } catch (Exception $ex) {
            $result = ApiModule::$defaultResponse;
            $result['message'] = $ex->getMessage();
            ApiModule::sendResponse($result, $this);
        }
    }
    
    /**
     * @Author: DungNT Oct 23, 2015
     * @Todo: Member signup Gas account, get data label for radio, select box, check box...
     * @Resource: site/signupDataLabel xử lý gưt Province là chính
     * @word_index: 3.4.1
     * @ex_json_request: {"signup_code":""}
     * @Method: POST 
     */
    public function actionSignupDataLabel()
    {
       try{
            $this->checkRequest();
            $q = json_decode($_POST['q']);
            $this->checkRequiredParams($q, array('signup_code'));
            $result = ApiModule::$defaultSuccessResponse;
            $result['message'] = 'Success';
            HandleLabel::SignupDataLabel($this, $q);
        } catch (Exception $ex) {
            MyFormat::ApiCatchError($ex, $this);
        }
    }

    /**
     * @Author: DungNT Oct 23, 2015
     * @Todo: handle Member signup
     * @Resource: site/signup
     * @word_index: 3.4.2 Method: POST 
     * @ex_json_request: {"signup_code":"5d0600794fd2e9a62d29f7521acec864","phone":"66888999","password":"123123","password_confirm":"123123","first_name":"api first name","province_id":"1","district_id":"2","ward_id":"2","house_numbers":"306","street":"Duong Quang Ham"}
     */
    public function actionSignup()
    {
        try{
        $result = ApiModule::$defaultResponse;
        $this->checkRequest();
        $q = json_decode($_POST['q']);
        $this->checkRequiredParams($q, array('signup_code','phone','password', 'password_confirm', 'first_name',
            'province_id', 'district_id', 'ward_id', 'house_numbers', 'street'
            ));
        $this->SignupClose();
        // 1. check valid signup_code, đếm số lần post request xem có vượt giới hạn không
        ApiCheckSignup::CheckLimitPostRequest($q->signup_code, $this);
        // 2. cộng 1 cho 1 signup_code
        ApiCheckSignup::PlusPostRequest($this);
        // 3. get post and validate
        $mUser = $this->HandleSignupGetPost($result, $q);
        // 4. and save, after save delete record of signup_code
        $result = ApiModule::$defaultSuccessResponse;
        $result['message'] = 'Đăng ký thành công';
        $this->HandleSignupSaveUser($mUser);
        
        ApiModule::sendResponse($result, $this);
        } catch (Exception $ex) {
            MyFormat::ApiCatchError($ex, $this);
        }
    }
        
    /**
     * @Author: DungNT Nov 23, 2015
     * @Todo: show message tạm dừng đăng ký
     */
    public function SignupClose() {
        $result['message'] = 'Chức năng đăng ký tạm thời ngưng, vui lòng quay lại sau';
        ApiModule::sendResponse($result, $this);
    }
    
    /**
     * @Author: DungNT Oct 23, 2015
     * map attribute to model form $q. get post and validate
     */
    public function HandleSignupGetPost($result, $q) {
        die; // Aug 30, 2016 chưa xử lý 
        $mUser = new Users('ApiSignup');
        $mUser->username = $q->phone;
        $mUser->phone = $q->phone;
        $mUser->password = $q->password;
        $mUser->password_confirm = $q->password_confirm;
        $mUser->first_name = $q->first_name;
        $mUser->province_id = $q->province_id;
        $mUser->district_id = $q->district_id;
        $mUser->ward_id = $q->ward_id;
        $mUser->house_numbers = $q->house_numbers;
        $mUser->address_temp = $q->street;
        $mUser->street = $q->street;
        // more of user info
        $mUser->role_id = ROLE_CUSTOMER;
        $mUser->type = CUSTOMER_TYPE_API_HO_GD;
        $mUser->application_id = BE;
        $mUser->validate();
        if($mUser->hasErrors()){
            $result['message'] = HandleLabel::FortmatErrorsModel($mUser->getErrors());
            ApiModule::sendResponse($result, $this);
        }
        return $mUser;
    }
    
    /**
     * @Author: DungNT Oct 26, 2015
     * save info signup to model Users
     */
    public function HandleSignupSaveUser($mUser) {
        $mUser->password_hash = md5($mUser->password);
        $mUser->temp_password = $mUser->password;
        $mUser->save();
    }
    
    /**
     * @Author: DungNT Oct 26, 2015
     * @Todo: Member signup Gas account, get data for select box district
     * @Resource: site/signupGetDistrict
     * @word_index: 3.4.3
     * @ex_json_request: {"province_id":1}
     * @Method: POST 
     */
    public function actionSignupGetDistrict()
    {
       try{
            $this->checkRequest();
            $q = json_decode($_POST['q']);
            $this->checkRequiredParams($q, array('province_id'));
            HandleLabel::GetDistrict($this, $q);
        } catch (Exception $ex) {
            MyFormat::ApiCatchError($ex, $this);
        }
    }

    /**
     * @Author: DungNT Oct 26, 2015
     * @Todo: Member signup Gas account, get data for select box ward
     * @Resource: site/signupGetWard
     * @word_index: 3.4.4
     * @ex_json_request: {"province_id":1,"district_id":1}
     * @Method: POST 
     */
    public function actionSignupGetWard()
    {
       try{
            $this->checkRequest();
            $q = json_decode($_POST['q']);
            $this->checkRequiredParams($q, array('province_id'));
            HandleLabel::GetWard($this, $q);
        } catch (Exception $ex) {
            MyFormat::ApiCatchError($ex, $this);
        }
    }

    /**
     * @Author: DungNT Oct 27, 2015
     * @Todo: Order – Create new – get data label for radio, select box, check box...
     * @Resource: site/orderCreateDataLabel
     * @word_index: 3.7.1
     * @Method: POST 
     * @ex_json_request: {"token":"dc36cc6fb2db91e515372db1821b6275"}
     */
    public function actionOrderCreateDataLabel()
    {
       try{
            $result = ApiModule::$defaultResponse;
            $this->checkRequest();
            $q = json_decode($_POST['q']);
            $this->checkRequiredParams($q, array('token'));

            // 1. get user by token
            $mUser = $this->getUserByToken($result, $q->token);// Oct 12, 2015 for check inactive or delete User
            HandleLabel::OrderCreateDataLabel($this);
        } catch (Exception $ex) {
            MyFormat::ApiCatchError($ex, $this);
        }
    }
    
    /**
     * @Author: DungNT Oct 27, 2015
     * @Todo: Member đặt hàng gas
     * @Resource: site/orderCreate
     * @word_index: 3.7.2 Method: POST 
     * @ex_json_request: {"token":"dc36cc6fb2db91e515372db1821b6275","qty_12":"12","qty_50":"21","note_customer":"123123"}
     */
    public function actionOrderCreate()
    {
        try{
        $result = ApiModule::$defaultResponse;
        $this->checkRequest();
        $q = json_decode($_POST['q']);
        $this->checkRequiredParams($q, array('token','qty_12','qty_50', 'note_customer'));
        
        // 1. get user by token
        $mUser = $this->getUserByToken($result, $q->token);// Oct 12, 2015 for check inactive or delete User
        // 2. get post and validate
        $mOrder = $this->OrderCreateGetPost($result, $q, $mUser);
        // 3. and save
        $result = ApiModule::$defaultSuccessResponse;
        $result['message'] = Yii::t('systemmsg','Tạo mới đặt hàng thành công');
        $this->OrderCreateSave($mOrder);
        
        ApiModule::sendResponse($result, $this);
        } catch (Exception $ex) {
            MyFormat::ApiCatchError($ex, $this);
        }
    }
    
    /**
     * @Author: DungNT Oct 27, 2015
     * map attribute to model form $q. get post and validate
     */
    public function OrderCreateGetPost($result, $q, $mUser) {
        $mOrder = new GasAppOrder('ApiCreate');
        $mOrder->customer_id = $mUser->id;
        $mOrder->qty_12 = $q->qty_12;
        $mOrder->qty_50 = $q->qty_50;
        $mOrder->note_customer = $q->note_customer;
//        $mOrder->agent_id = // get agent id cua customer;
        $mOrder->validate();
        if($mOrder->hasErrors()){
            $result['message'] = HandleLabel::FortmatErrorsModel($mOrder->getErrors());
            ApiModule::sendResponse($result, $this);
        }
        return $mOrder;
    }
    
    /**
     * @Author: DungNT Oct 27, 2015
     * save info signup to model Users
     */
    public function OrderCreateSave($mOrder) {
        $mOrder->save();
    }
    
    /**
     * @Author: DungNT Oct 27, 2015
     * @Todo: List danh sách đặt gas của member
     * @Resource: site/orderList
     * @word_index: 3.7.3
     * @ex_json_request: 
     {"token":"dc36cc6fb2db91e515372db1821b6275","page":"0"}
     */
    public function actionOrderList()
    {
        try{
        $result = ApiModule::$defaultResponse;
        $this->checkRequest();
        $q = json_decode($_POST['q']);
        $this->checkRequiredParams($q, array('token', 'page'));
        $mUser = $this->getUserByToken($result, $q->token);// Oct 12, 2015 for check inactive or delete User
        $result = ApiModule::$defaultSuccessResponse;
        $result['message'] = 'Success';
        $this->HandleOrderList($result, $q, $mUser);
        ApiModule::sendResponse($result, $this);
        } catch (Exception $ex) {
            MyFormat::ApiCatchError($ex, $this);
        }
    }
    
    /**
     * @Author: DungNT Oct 27, 2015
     */
    public function HandleOrderList(&$result, $q, $mUser) {
        // 1. get list order by user id
        $dataProvider = GasAppOrder::ApiListing($q, $mUser);
        $models = $dataProvider->data;
        $CPagination = $dataProvider->pagination;
        $result['total_record'] = $CPagination->itemCount;
        $result['total_page'] = $CPagination->pageCount;
        if( $q->page >= $CPagination->pageCount ){
            $result['record'] = array();
        }else{
            foreach($models as $mOrder){
                $temp = array();
                $temp['id'] = $mOrder->id;
                $temp['code_no'] = $mOrder->code_no;
                $temp['status'] = $mOrder->getStatusText();
                $temp['qty_12'] = $mOrder->getQty_12();
                $temp['qty_50'] = $mOrder->getQty_50();
                $temp['note_customer'] = $mOrder->note_customer;
                $temp['created_date'] = MyFormat::dateConverYmdToDmy($mOrder->created_date, "d/m/Y H:i");
                $result['record'][] = $temp;
            }
        }
    }
    
    /**
     * @Author: DungNT Oct 27, 2015
     * @Todo: List danh sách tin tức của hệ thống
     * @Resource: site/newsList
     * @word_index: 3.8.1
     * @ex_json_request: 
     {"token":"dc36cc6fb2db91e515372db1821b6275","page":"0"}
     */
    public function actionNewsList()
    {
        try{
        $result = ApiModule::$defaultResponse;
        $this->checkRequest();
        $q = json_decode($_POST['q']);
        $this->checkRequiredParams($q, array('token', 'page'));
        $mUser = $this->getUserByToken($result, $q->token);// Oct 12, 2015 for check inactive or delete User
        $result = ApiModule::$defaultSuccessResponse;
        $result['message'] = 'Success';
        $this->HandleNewsList($result, $q, $mUser);
        ApiModule::sendResponse($result, $this);
        } catch (Exception $ex) {
            MyFormat::ApiCatchError($ex, $this);
        }
    }
    
    /**
     * @Author: DungNT Oct 27, 2015
     */
    public function HandleNewsList(&$result, $q, $mUser) {
        // 1. get list order by user id
        $dataProvider = GasAppNews::ApiListing($q, $mUser);
        $models = $dataProvider->data;
        $CPagination = $dataProvider->pagination;
        $result['total_record'] = $CPagination->itemCount;
        $result['total_page'] = $CPagination->pageCount;
        if( $q->page >= $CPagination->pageCount ){
            $result['record'] = array();
        }else{
            foreach($models as $mNews){
                $temp = array();
                $temp['id'] = $mNews->id;
                $temp['title'] = $mNews->getTitle();
                $temp['short_content'] = $mNews->getShortContent();
//                $temp['content'] = $mNews->getContent();
                $temp['content'] = "";
                $temp['created_date'] = MyFormat::dateConverYmdToDmy($mNews->created_date, "d/m/Y");
                $result['record'][] = $temp;
            }
        }
    }
    
    /**
     * @Author: DungNT Now 11, 2015
     * @Todo: View tin tức của hệ thống
     * @Resource: site/newsView
     * @word_index: 3.8.2
     * @ex_json_request: 
     {"token":"dc36cc6fb2db91e515372db1821b6275","news_id":"1"}
     */
    public function actionNewsView()
    {
        try{
        $result = ApiModule::$defaultResponse;
        $this->checkRequest();
        $q = json_decode($_POST['q']);
        $this->checkRequiredParams($q, array('token', 'news_id'));
        $mNews = GasAppNews::model()->findByPk($q->news_id);
        if(is_null($mNews)){
            $result['message'] = "Yêu cầu không hợp lệ";
        }else{
            $mUser = $this->getUserByToken($result, $q->token);// Oct 12, 2015 for check inactive or delete User
            $result = ApiModule::$defaultSuccessResponse;
            $result['message'] = 'Success';
            $this->HandleNewsView($result, $q, $mNews);
        }
        ApiModule::sendResponse($result, $this);
        } catch (Exception $ex) {
            MyFormat::ApiCatchError($ex, $this);
        }
    }
    
    /**
     * @Author: DungNT Now 11, 2015
     */
    public function HandleNewsView(&$result, $q, $mNews) {
        $temp                   = array();
        $temp['id']             = $mNews->id;
        $temp['title']          = $mNews->getTitle();
//        $temp['short_content'] = $mNews->getShortContent();
        $temp['short_content']  = "";
        $temp['content']        = $mNews->getContent();
        $temp['created_date']   = MyFormat::dateConverYmdToDmy($mNews->created_date, "d/m/Y");
        $result['record']       = $temp;
    }
    
    /**
     * @Author: DungNT Oct 27, 2015
     * @Todo: List danh sách lịch bảo trì của member
     * @Resource: site/upholdList
     * @word_index: 3.8
     * @ex_json_request: 
     {"token":"dc36cc6fb2db91e515372db1821b6275","page":"0"}
     */
    public function actionUpholdList()
    {
        try{
        $result = ApiModule::$defaultResponse;
        $this->checkRequest();
        $q = json_decode($_POST['q']);
        $this->checkRequiredParams($q, array('token', 'page', 'type'));
        $mUser = $this->getUserByToken($result, $q->token);// Oct 12, 2015 for check inactive or delete User
        $result = ApiModule::$defaultSuccessResponse;
        $result['message'] = 'Success';
        $this->HandleUpholdList($result, $q, $mUser);
        ApiModule::sendResponse($result, $this);
        } catch (Exception $ex) {
            MyFormat::ApiCatchError($ex, $this);
        }
    }
    
    /**
     * @Author: DungNT Oct 27, 2015
     */
    public function HandleUpholdList(&$result, $q, $mUser) {
        $mUpholdTemp            = new GasUphold();
        $UserRole               = $mUser->role_id;
        $mUpholdTemp->mAppUserLogin = $mUser;
        // 1. get list order by user id
        $dataProvider           = $mUpholdTemp->ApiListing($q, $mUser);
        $models                 = $dataProvider->data;
        $CPagination            = $dataProvider->pagination;
        $result['total_record'] = $CPagination->itemCount;
        $result['total_page']   = $CPagination->pageCount;
        if( $q->page >= $CPagination->pageCount ){
            $result['record'] = array();
        }else{
            if($UserRole == ROLE_CUSTOMER && $q->type == GasUphold::TYPE_DINH_KY){// list BT dinh ky cua KH se phai xu ly khac
                $mUphold = GasUphold::ApiGetRowCustomerDinhKy($q, $mUser);
                if(empty($mUphold)){
                    return $result['record'] = null;
                }
                foreach($models as $key => $mUpholdReply){
                    $mUphold->mAppUserLogin         = $mUser;
                    if(empty($key)){
                        $mUpholdReply->getCodeComplete  = true;
                    }
                    $result['record'][]     = $this->UpholdFormatRecordCustomerReply($mUphold, $mUpholdReply);
                }
            }else{
                foreach($models as $mUphold){
                    $mUphold->mAppUserLogin = $mUser;
                    $result['record'][]     = $this->UpholdFormatRecord($mUphold);
                }
            }
        }
    }
    
    public function UpholdFormatRecord($mUphold, $get_reply = false) {
        $temp = array();
        $temp['id']             = $mUphold->id;
        $temp['reply_id']       = '0';// để xử lý chung cho view của 1 reply
        $temp['uphold_type']    = $mUphold->type;
        $temp['code_no']        = $mUphold->code_no;
        $temp['customer_name']  = $mUphold->getCustomer();
        $temp['customer_address'] = $mUphold->getCustomer('address');
        $temp['level_type']     = $mUphold->getTextLevel();
        $temp['type_uphold']    = $mUphold->getAppCodeComplete(). $mUphold->getTextUpholdType();
        $temp['schedule_type']  = $mUphold->getArrayScheduleTypeText();// Aug 29, 2016 not up live
        $temp['content']        = $mUphold->getContentApi();
        $temp['contact_person'] = $mUphold->getContactPerson();
        $temp['contact_tel']    = $mUphold->getContactTel();
        $temp['status']         = $mUphold->getTextStatus();
        $temp['sale_name']      = $mUphold->getSale();
        $temp['created_by']     = $mUphold->getUidLogin();
        $temp['created_date']   = $mUphold->getAppDateShow();
        $temp['employee_name']  = $mUphold->getByRelationAndFieldName('rEmployee');
        $temp['employee_phone'] = $mUphold->getByRelationAndFieldName('rEmployee', 'phone');
        $tmp['employee_image']  = '';// Jan 16, 2017
        if($mUphold->rEmployee){
            $tmp['employee_image']  = ImageProcessing::bindImageByModel($mUphold->rEmployee,'','',array('size'=>'size2'));
        }
        $temp['report_wrong']   = $mUphold->getReportWrongApi();
        $temp['status_number']  = $mUphold->status;
        $temp['customer_id']    = $mUphold->customer_id;
        // Apr 16, 2016 - fix for rating app
//        $temp['data_custom'] = $mUphold->apiGetDataCustom();// có thể sẽ xử lý kiểu này
        $temp['rating_status']  = $mUphold->rating_status;
        $sRatingTypeValue       = HandleLabel::JsonIdName(GasUpholdRating::getByUpholdId($mUphold->id, array('listData'=>1)));
        $temp['rating_type']    = $sRatingTypeValue;
        $temp['rating_note']    = $mUphold->getRatingNote();
        // Apr 16, 2016 - fix for rating app
        
        $last_reply_message = "";
        // Now 28, 2015 bổ sung thêm cho bảo trì định kỳ
        $temp['schedule_month'] = $mUphold->getScheduleText();
        if(! $get_reply){
            $temp['content'] = "";// vì ở trang list không lấy content nên sẽ không trả về
        }
        if($get_reply){
            $mDetail = $mUphold->getListReply();
            foreach($mDetail as $key => $mUpholdDetail){
                $tempDetail = $this->UpholdDetailFormatRecord($mUpholdDetail);
                if($key == 0){
                    $last_reply_message = $tempDetail["note"];
                }
                $temp['reply_item'][] = $tempDetail;
            }
        }
        $temp['last_reply_message']     = $last_reply_message;// Lấy comment lastest của BT
        $temp['customer_name_chain']    = $mUphold->getCustomerNameChain();
        
        return $temp;
    }
    
    public function UpholdFormatRecordCustomerReply($mUphold, $mUpholdReply, $get_reply = false) {
        $temp = array();
        $temp['id']             = $mUpholdReply->id;
        $temp['reply_id']       = $mUpholdReply->id;
        $temp['uphold_type']    = $mUphold->type;
        $temp['code_no']        = $mUphold->code_no;
        $temp['customer_name']  = $mUphold->getCustomer();
        $temp['customer_address'] = $mUphold->getCustomer('address');
        $temp['level_type']     = $mUphold->getTextLevel();
        $temp['type_uphold']    = '';
        $temp['content']        = $mUphold->getContentApi();
        $temp['contact_person'] = $mUphold->getContactPerson();
        $temp['contact_tel']    = $mUphold->getContactTel();
        $code = $mUpholdReply->getCodeComplete ? $mUphold->getAppCodeComplete() : '';
        $mUpholdTemp = new GasUphold();
        $mUpholdTemp->status    = $mUpholdReply->status;
        $temp['status']         = $mUpholdTemp->getTextStatus();
        
        $temp['sale_name']      = $mUphold->getSale();
        $temp['created_by']     = $mUphold->getUidLogin();
        $temp['created_date']   = MyFormat::dateConverYmdToDmy($mUpholdReply->created_date, 'd/m/Y H:i');
        $temp['employee_name']  = $code.$mUphold->getByRelationAndFieldName('rEmployee');
        $temp['employee_phone'] = $mUphold->getByRelationAndFieldName('rEmployee', 'phone');
        $tmp['employee_image']  = '';// Jan 16, 2017
        if($mUphold->rEmployee){
            $tmp['employee_image']  = ImageProcessing::bindImageByModel($mUphold->rEmployee,'','',array('size'=>'size2'));
        }
        $temp['report_wrong']   = $mUphold->getReportWrongApi();
        $temp['status_number']  = $mUpholdReply->status;
        $temp['customer_id']    = $mUphold->customer_id;
        
        // Apr 22, 2016 - fix for rating app -- cho đồng bộ với response BT sự cố
        $temp['rating_status'] = "";
        $sRatingTypeValue = HandleLabel::JsonIdName(array());
        $temp['rating_type'] = $sRatingTypeValue;
        $temp['rating_note'] = "";
        // Apr 22, 2016 - fix for rating app -- cho đồng bộ với response BT sự cố
        
        // Dec 02, 2015 Fix View bổ sung thêm cho bảo trì định kỳ
//        $temp['schedule_month'] = $mUphold->getScheduleText();
        $temp['schedule_month'] = '';
        if($get_reply){
            $temp['reply_item'][] = $this->UpholdDetailFormatRecord($mUpholdReply);
        }
        $temp['last_reply_message'] = $mUpholdReply->getApiNote();// Lấy comment lastest của BT

        return $temp;
    }
    
    /**
     * @Author: DungNT Oct 27, 2015
     * @Todo: View 1 bảo trì của member
     * @Resource: site/upholdView
     * @word_index: 3.9.2
     * @ex_json_request: 
     {"token":"dc36cc6fb2db91e515372db1821b6275","uphold_id":"1"}
     */
    public function actionUpholdView()
    {
        try{
        $result = ApiModule::$defaultResponse;
        $this->checkRequest();
        $q = json_decode($_POST['q']);
        $this->checkRequiredParams($q, array('token', 'uphold_id'));
        $mUser = $this->getUserByToken($result, $q->token);// Oct 12, 2015 for check inactive or delete User
        $result = ApiModule::$defaultSuccessResponse;
        $result['message'] = 'Success';
        $this->HandleUpholdView($result, $q, $mUser);
        ApiModule::sendResponse($result, $this);
        } catch (Exception $ex) {
            MyFormat::ApiCatchError($ex, $this);
        }
    }
    
    /** Xử lý view cho 2 loại
     * 1: kh view
     * 2: NV bảo trì và điều phối view
     */
    public function HandleUpholdView(&$result, $q, $mUser) {
        try{
        // 1. KH view
        if(!empty($q->reply_id) && $mUser->role_id == ROLE_CUSTOMER){
            // Dec 04, 2015 bị lỗi khi không xử lý thêm check ROLE_CUSTOMER, do đó khi bảo trì View notify sẽ bị lỗi
            $this->HandleUpholdViewDinhKy($result, $q, $mUser);
            return;
        }
        
        // 2. Bao TRi + Dieu Phoi view
        $mUphold = $this->getModelUphold($q);
        if($mUser->id == $mUphold->employee_id){// Bảo trì xem thì mới cập nhật cột read của record
            $mUphold->UpdateHasRead();
        }
        $mUphold->mAppUserLogin = $mUser;
        $aTypeNotify = array(GasScheduleNotify::UPHOLD_ALERT_10, GasScheduleNotify::UPHOLD_CREATE, GasScheduleNotify::UPHOLD_DINH_KY_1_DAY);
        // Dec 22, 2015 update status view notify history
        GasScheduleNotifyHistory::updateStatusViewByAttribute($mUser->id, $aTypeNotify, $mUphold->id);
        $result['model_uphold'] = $this->UpholdFormatRecord($mUphold, true);
//        $mDetail = $mUphold->rUpholdReply; // Now 10, 2015 di chuyen vao trong model cha
//        foreach($mDetail as $mUpholdDetail){
//            $result['record'][] = $this->UpholdDetailFormatRecord($mUpholdDetail);
//        }
        } catch (Exception $ex) {
            throw new Exception($ex->getMessage());
        }
    }
    
    /**
     * @Author: DungNT Apr 15, 2016
     * @Todo: get model uphold view
     * @Param: $q OBJECT POST
     */
    public function getModelUphold($q) {
        try{
            $mUphold = GasUphold::model()->findByPk($q->uphold_id); // get model uphold 
            if(is_null($mUphold)){
                throw new Exception('Null mUphold line 648');
            }
            return $mUphold;
        } catch (Exception $ex) {
            throw new Exception($ex->getMessage());
        }
    }
    
    /**
     * @Author: DungNT Dec 02, 2015
     * @Todo: xử lý view đinh kỳ cho UI khách hàng
     */
    public function HandleUpholdViewDinhKy(&$result, $q, $mUser) {
        // 1. get model uphold
        $mUpholdReply = GasUpholdReply::model()->findByPk($q->reply_id);
        $mUphold = GasUphold::model()->findByPk($mUpholdReply->uphold_id);
        if(is_null($mUpholdReply) || is_null($mUphold)){
            throw new Exception('Invalid request null mUpholdReply OR mUphold line 674');
        }
        // Dec 22, 2015 update status view notify history
        GasScheduleNotifyHistory::updateStatusViewByAttribute($mUser->id, GasScheduleNotify::UPHOLD_DINH_KY_1_DAY, $mUphold->id);
        $mUphold->mAppUserLogin = $mUser;
        $result['model_uphold'] = $this->UpholdFormatRecordCustomerReply($mUphold, $mUpholdReply, true);
    }
    
    public function UpholdDetailFormatRecord($mUpholdDetail) {
        $temp = array();
        $temp['id']                 = $mUpholdDetail->id;
        $temp['date_time_handle']   = MyFormat::dateConverYmdToDmy($mUpholdDetail->date_time_handle, "d/m/Y H:i");
        $temp['hours_handle']       = ActiveRecord::formatNumberInput($mUpholdDetail->hours_handle)." giờ";
        $temp['note']               = $mUpholdDetail->getApiNote();
        $temp['contact_phone']      = $mUpholdDetail->getApiContactPhone();
        $temp['note_internal']      = $mUpholdDetail->getApiNoteInternal();
        $temp['uid_login']          = $mUpholdDetail->getUidLogin();
        $temp['images']             = $mUpholdDetail->ApiGetFileUpload();
        $temp['report_wrong']       = $mUpholdDetail->getReportWrongApi();
        
        $mUphold = new GasUphold();
        $mUphold->status        = $mUpholdDetail->status;
        $temp['status']         = $mUphold->getTextStatus();
        $temp['created_date']   = MyFormat::dateConverYmdToDmy($mUpholdDetail->created_date, "d/m/Y H:i");
        return $temp;
    }
    
    /**
     * @Author: DungNT Oct 27, 2015
     * @Todo: Reply lịch bảo trì của member – get data for select box and label 
     * @Resource: site/upholdReplyDataLabel
     * @word_index: 3.9.3
     * @ex_json_request: 
     {"token":"dc36cc6fb2db91e515372db1821b6275"}
     */
    public function actionUpholdReplyDataLabel()
    {
        try{
        $result = ApiModule::$defaultResponse;
        $this->checkRequest();
        $q = json_decode($_POST['q']);
        $this->checkRequiredParams($q, array('token'));
        $mUser = $this->getUserByToken($result, $q->token);// Oct 12, 2015 for check inactive or delete User
        HandleLabel::UpholdReplyDataLabel($this, $q);
        ApiModule::sendResponse($result, $this);
        } catch (Exception $ex) {
            MyFormat::ApiCatchError($ex, $this);
        }
    }
 
    /**
     * @Author: DungNT Oct 27, 2015
     * @Todo: Member đặt hàng gas
     * @Resource: site/upholdReply
     * @word_index: 3.9.4 Method: POST 
     * @ex_json_request: {"token":"dc36cc6fb2db91e515372db1821b6275","uphold_id":"1","status":"2","hours_handle":"1.5","note":"test reply api"}
     */
    public function actionUpholdReply()
    {
        try{
        $result = ApiModule::$defaultResponse;
        $this->checkRequest();
        $q = json_decode($_POST['q']);
        $this->checkRequiredParams($q, array('token','uphold_id','status', 'hours_handle','note'));
        $_GET['platform'] = isset($q->platform) ? $q->platform : UsersTokens::PLATFORM_ANDROID;
        // 1. get user by token
        $mUser = $this->getUserByToken($result, $q->token);// Oct 12, 2015 for check inactive or delete User
        $mUphold = GasUphold::model()->findByPk($q->uphold_id);
        if(is_null($mUphold)){
            throw new Exception('Invalid request');
        }
        // 2. get post and validate
        $mUpholdReply = $this->UpholdReplyGetPost($result, $q, $mUser, $mUphold);
        // 4. and save
        $result = ApiModule::$defaultSuccessResponse;
        $result['message'] = 'Reply thành công';

        if($mUphold){
            if($this->isDinhKyCoDinh($mUphold)){
                $this->UpholdReplyDinhKySave($mUpholdReply);
            }else{
                $this->UpholdReplySave($mUpholdReply);
            }
            // 5. save file
            if(!is_null($mUpholdReply->id)){
                $mUpholdReply->updateToPlan();
                GasFile::ApiSaveRecordFile($mUpholdReply, GasFile::TYPE_3_UPHOLD_REPLY);
            }
            $mUpholdReply->genCodeDinhKy();// Dec2217 Open when live
        }
        ApiModule::sendResponse($result, $this);
        } catch (Exception $ex) {
            MyFormat::ApiCatchError($ex, $this);
        }
    }
    
    public function UpholdReplyGetPost($result, $q, $mUser, $mUphold) {
        $mUpholdReply = new GasUpholdReply('ApiReply');
        $mUpholdReply->uid_login        = $mUser->id;
        $mUpholdReply->uphold_id        = $q->uphold_id;
        $mUpholdReply->status           = $q->status;
        $mUpholdReply->hours_handle     = $q->hours_handle;
        $mUpholdReply->formatHoursHandle();
        $mUpholdReply->date_time_handle = MyFormat::addDays(date('Y-m-d H:i:s'), round($mUpholdReply->hours_handle*60), "+", 'minutes', 'Y-m-d H:i:s');
        $mUpholdReply->note             = trim($q->note);
        $mUpholdReply->contact_phone    = isset($q->contact_phone) ? trim($q->contact_phone) : "";
        $mUpholdReply->note_internal    = trim($q->note_internal);
        $mUpholdReply->report_wrong     = $q->report_wrong;
        $mUpholdReply->employee_id      = $mUphold->employee_id;
        $mUpholdReply->customer_id      = $mUphold->customer_id;
        $mUpholdReply->type             = $mUphold->type;
        $mUpholdReply->code_complete    = isset($q->code_complete) ? $q->code_complete : '';
//        if($_GET['platform'] == UsersTokens::PLATFORM_ANDROID && (empty($q->latitude) || empty($q->longitude)) ){
        if((empty($q->latitude) || empty($q->longitude))){
            throw new Exception('Không cập nhật được vị trí của bạn, vui lòng tắt bật lại định vị GPS của máy');
        }
        $mUpholdReply->google_map       = $q->latitude.",$q->longitude";
        
        $mUpholdReply->mUphold = $mUphold;
        $mUpholdReply->validate();
        $this->CheckReplyDinhKy($mUpholdReply, $mUphold);
        $mUpholdReply->apiValidateReply();
        // validate file upload
//        $this->UpholdReplyFileUploadValidate($q, $mUpholdReply);// Feb0518 close để nhập mã hoàn thành
        if($mUpholdReply->hasErrors()){
            $result['message'] = HandleLabel::FortmatErrorsModel($mUpholdReply->getErrors());
            ApiModule::sendResponse($result, $this);
        }
        return $mUpholdReply;
    }
    
    /**
     * @Author: DungNT Dec 03, 2015
     * @Todo: validate xem có là ngày hợp lệ để reply cho BT định kỳ không
     */
    public function CheckReplyDinhKy($mUpholdReply, $mUphold) {
        if($this->isDinhKyCoDinh($mUphold)){
            $mUphold->GetMultiDay();
            $cDay = date('d')*1;
            // trong trường hợp yc chuyển thì cho reply thoải mái, ngược lại check ngày
            if($mUpholdReply->status != GasUphold::STATUS_CANCEL ){
                // kiểm tra thêm nếu trạng thái trc đó là YC chuyển thì không báo lỗi, cho phép Reply
                if(!in_array($cDay, $mUphold->day_checkbox) ){
                    $error = true;
                    $mLastReply = isset($mUphold->rUpholdReply[0]) ? $mUphold->rUpholdReply[0] : 0;
                    if($mLastReply  && $mLastReply->status == GasUphold::STATUS_CANCEL ){
                        $error = false;
                    }
                    if($error){
                        $mUpholdReply->addError('status',"Bạn không được gửi trả lời. Hôm nay không phải là ngày bảo trì của khách hàng". $mLastReply->id);
                    }
                }
            }
        }
        if(!$mUphold->canReplyDinhKyMonth()){
            $mUpholdReply->addError('status',"Bạn không được hoàn thành. Khách hàng này đã hoàn thành 1 bảo trì trong tháng rồi");
        }
 
        $mUphold->validateReplyDinhKy($mUpholdReply);
    }
    
    /**
     * @Author: DungNT Dec 04, 2015
     * @Todo: cộng thêm 2 ngày sau ngày được thiếp lập của 
     */
    public function ModifyDayCheckbox() { }
    
    /**
     * @Author: DungNT Dec 31, 2015
     * @Todo: KH Tạo Bảo trì sự cố
     * @Resource: site/upholdCreate
     * @word_index: 3.11.1 Method: POST 
     * @ex_json_request: {"token":"dc36cc6fb2db91e515372db1821b6275","title":"tieu de","problem":"12","message":"21"}
     */
    public function actionUpholdCreate()
    {
        try{
        $result = ApiModule::$defaultResponse;
        $this->checkRequest();
        $q = json_decode($_POST['q']);
        $this->checkRequiredParams($q, array('token','customer_id'));        
        // 1. get user by token
        $mUser = $this->getUserByToken($result, $q->token);// Oct 12, 2015 for check inactive or delete User
        // 2. get post and validate
        $mUphold = new GasUphold('ApiCreate');
        $mUphold->mAppUserLogin = $mUser;
        $mUphold->handlePost($q);
        $mUphold->checkUserInactive();
        $this->responseIfError($mUphold);

        // 3. and save
        $result = ApiModule::$defaultSuccessResponse;
        $result['message'] = 'Tạo mới bảo trì thành công';
        $mUphold->apiSaveCreate();
        $result['id'] = $mUphold->id."";
        $mUphold->makeSocketNotify();// May 05, 2017
        
        ApiModule::sendResponse($result, $this);
        } catch (Exception $ex) {
            MyFormat::ApiCatchError($ex, $this);
        }
    }
        
    /**
     * @Author: DungNT Dec 04, 2015
     */
    public function isDinhKyCoDinh($mUphold) {
        if($mUphold->type == GasUphold::TYPE_DINH_KY && $mUphold->schedule_type == GasUphold::SCHEDULE_CO_DINH){
            return true;
        }
        return false;
    }
    
    /**
     * @Author: DungNT Aug 17, 2015
     * validate file
     */
    public function UpholdReplyFileUploadValidate($q, &$mUpholdReply) {
        // 1. validate file only, add error to model $mReview if have
        GasUpholdReply::ApiValidateFile($mUpholdReply);
    }
    
    /**
     * @Author: DungNT Oct 27, 2015
     * save info signup to model Users
     */
    public function UpholdReplySave(&$mUpholdReply) {
        $mUpholdReply->save();
        $mUpholdReply = GasUpholdReply::model()->findByPk($mUpholdReply->id);
    }
    /**
     * @Author: DungNT Dec 03, 2015
     * update info Uphold Reply của BT định kỳ
     */
    public function UpholdReplyDinhKySave(&$mUpholdReply) {
        $mLatestReply = $mUpholdReply->findRecordDinhKy();
        if($mLatestReply){
            $mLatestReply->date_time_handle = date('Y-m-d H:i:s');
            $mLatestReply->status           = $mUpholdReply->status;
            $mLatestReply->note             = $mUpholdReply->note;
            $mLatestReply->contact_phone    = $mUpholdReply->contact_phone;
            $mLatestReply->uid_login        = $mUpholdReply->uid_login;
            $mLatestReply->note_internal    = $mUpholdReply->note_internal;
            $mLatestReply->update(array('status','note', 'contact_phone','note_internal', 'uid_login', 'date_time_handle'));
            $mUpholdReply = $mLatestReply;
        }
    }
    
    /**
     * @Author: DungNT Now 17, 2015
     * @Todo: get some data for use at client, for android + ios
     * @Resource: site/updateConfig
     * @word_index: 3.7.1
     * @Method: POST 
     * @ex_json_request: {"token":"dc36cc6fb2db91e515372db1821b6275"}
     */
    public function actionUpdateConfig()
    {
       try{
            $result = ApiModule::$defaultResponse;
            $this->checkRequest();
            $q = json_decode($_POST['q']);
            $this->checkRequiredParams($q, array('token'));
            // 1. get user by token
            $mUser = $this->getUserByToken($result, $q->token);// Oct 12, 2015 for check inactive or delete User
            UsersTokens::setLastActive($q->token);
//            GasScheduleNotify::ResetCountRun($mUser->id);// Dec 26, 2015 reset count_run of notify
            // May 05, 2017 Chỉ cần gửi 1 lần server chắc chắn sẽ gửi đến client nếu online,  old =>>>tạm close hàm này lại, vì nó sẽ làm hệ thống gửi nhiều cho ios, vì ios chưa có cơ chế confirm notify
            $mResponse = new HandleArrayResponse();
            $mResponse->updateConfig($mUser, $this);
        } catch (Exception $ex) {
            MyFormat::ApiCatchError($ex, $this);
        }
    }
    /**
     * @Author: DungNT Mar 02, 2017
     * @Todo: get some data to Cache at client, for android + ios
     * @Resource: site/getDataCache
     * @Method: POST 
     * @ex_json_request: {"token":"641a7db2fbe6f29077a00adca133e9ea"}
     */
    public function actionGetDataCache()
    {
       try{
            $result = ApiModule::$defaultResponse;
            $this->checkRequest();
            $q = json_decode($_POST['q']);
            $this->checkRequiredParams($q, array('token'));
            // 1. get user by token
            $mUser = $this->getUserByToken($result, $q->token);// Oct 12, 2015 for check inactive or delete User
            $mResponse = new HandleArrayResponse();
            $mResponse->getDataCache($mUser, $this);
        } catch (Exception $ex) {
            MyFormat::ApiCatchError($ex, $this);
        }
    }
    
    /**
     * @Author: DungNT Now 24, 2015
     * @Todo: Member phản ánh sự việc
     * @Resource: site/issueCreate
     * @word_index: 3.11.1 Method: POST 
     * @ex_json_request: {"token":"dc36cc6fb2db91e515372db1821b6275","title":"tieu de","problem":"12","message":"21"}
     */
    public function actionIssueCreate()
    {
//            echo '******** DungNT DUMP - POST ***********<pre>';
//            print_r($_POST);
//            echo '</pre><hr><br>';
//            echo '******** DungNT DUMP - FILE ***********<pre>';
//            print_r($_FILES);
//            echo '</pre><hr>';
//            die; 
        try{
        $result = ApiModule::$defaultResponse;
        $this->checkRequest();
        $q = json_decode($_POST['q']);
        $this->checkRequiredParams($q, array('token','message'));
        
        // 1. get user by token
        $mUser = $this->getUserByToken($result, $q->token);// Oct 12, 2015 for check inactive or delete User
        // 2. get post and validate
        $mIssue = new GasIssueTickets("ApiCreate");
        $mIssue = $this->IssueCreateGetPost($result, $q, $mUser, $mIssue);
        // 3. and save
        $result = ApiModule::$defaultSuccessResponse;
        $result['message'] = 'Tạo mới phản ánh sự việc thành công';
        $mIssue->apiSaveRecordBeginIssue($mUser);// Save record Root + detail + file 
        $result['id'] = $mIssue->id."";
        
        ApiModule::sendResponse($result, $this);
        } catch (Exception $ex) {
            MyFormat::ApiCatchError($ex, $this);
        }
    }
        
    /**
     * @Author: DungNT Now 22, 2015
     * map attribute to model form $q. get post and validate
     * @param: $result, $q, $mUser
     * @param: $model is $mIssue
     */
    public function IssueCreateGetPost($result, $q, $mUser, &$model) {
        $model->title = isset($q->title) ? $q->title : "";
        $model->message = $q->message;
        if(isset($q->customer_id)){
            $model->customer_id = $q->customer_id;
        }
        if(isset($q->chief_monitor_id)){
            $model->chief_monitor_id =  $q->chief_monitor_id;
        }
        if(isset($q->monitor_agent_id)){
            $model->monitor_agent_id = $q->monitor_agent_id;
        }
        if(isset($q->accounting_id)){// Dec 20, 2015
            $model->accounting_id = $q->accounting_id;
        }
        $model->problem = isset($q->problem) ? $q->problem : GasIssueTickets::PROBLEM_NHA_CC;
        
        if($model->canRequiredMessageOnApp($mUser->role_id)){
            $model->validate();
        }
        
        // validate file upload
        $this->IssueCreateFileUploadValidate($q, $model);
        if($model->hasErrors()){
            $result['message'] = HandleLabel::FortmatErrorsModel($model->getErrors());
            ApiModule::sendResponse($result, $this);
        }
        return $model;
    }
    
    /**
     * @Author: DungNT Now 22, 2015
     * validate file
     */
    public function IssueCreateFileUploadValidate($q, &$model) {
        // 1. validate file only, add error to model $mReview if have
        GasIssueTickets::ApiValidateFile($model);
    }
    
    
    /**
     * @Author: DungNT Now 24, 2015
     * @Todo: list phản ánh sự việc cua member
     * @Resource: site/issueList
     * @word_index: 3.11.2 Method: POST 
     * @ex_json_request: {"token":"dc36cc6fb2db91e515372db1821b6275","page":"0","type":"1"}
     */
    public function actionIssueList()
    {
        try{
        $result = ApiModule::$defaultResponse;
        $this->checkRequest();
        $q = json_decode($_POST['q']);
        $this->checkRequiredParams($q, array('token', 'page', 'type'));
        $mUser = $this->getUserByToken($result, $q->token);// Oct 12, 2015 for check inactive or delete User
        $result = ApiModule::$defaultSuccessResponse;
        $result['message'] = 'Success';
        $this->HandleIssueList($result, $q, $mUser);
        ApiModule::sendResponse($result, $this);
        } catch (Exception $ex) {
            MyFormat::ApiCatchError($ex, $this);
        }
    }
    
    /**
     * @Author: DungNT Now 25, 2015
     */
    public function HandleIssueList(&$result, $q, $mUser) {
        // 1. get list order by user id
        $model = new GasIssueTickets();
        $dataProvider   = $model->apiListing($q, $mUser);
        $models         = $dataProvider->data;
        $CPagination    = $dataProvider->pagination;
        $result['total_record']     = $CPagination->itemCount;
        $result['total_page']       = $CPagination->pageCount;
        if( $q->page >= $CPagination->pageCount ){
            $result['record'] = array();
        }else{
            foreach($models as $mIssue){
                $result['record'][] = $this->IssueFormatRecord($mIssue, $mUser);
            }
        }
    }
    
    // belong to HandleIssueList
    public function IssueFormatRecord($mIssue, $mUser, $get_reply = false) {
        $temp = array();
        $temp['id']             = $mIssue->id;
        $temp['code_no']        = $mIssue->code_no;
        $temp['customer_name']  = $mIssue->getCustomer();
        $temp['customer_address'] = $mIssue->getCustomer('address');
        $temp['title']          = $mIssue->getTitle();
        $temp['sale_name']      = $mIssue->getSale();
        $temp['created_by']     = $mIssue->getUidLogin();
        $temp['created_date']   = MyFormat::dateConverYmdToDmy($mIssue->created_date, "d/m/Y H:i");
        $temp['can_reply']      = $mIssue->canReplyOnApp($mUser);// Dec 10, 2015 check xem user có dc Reply dưới app không, sẽ modify lại sau
        $temp['can_close']      = $mIssue->canCloseOnApp($mUser);// Dec 10, 2015 check xem user có dc Reply dưới app không, sẽ modify lại sau
        $temp['can_reopen']     = $mIssue->canReopenOnApp($mUser);// Dec 10, 2015 check xem user có dc Reply dưới app không, sẽ modify lại sau
//        throw new Exception('kkkkkk');
        $temp['status_text']    = $mIssue->getStatus();// Open or Close
        $temp['problem_text']   = $mIssue->getProblem();
        $temp['problem_id']     = $mIssue->problem;
        $temp['required_message'] = $mIssue->canRequiredMessageOnApp($mUser->role_id);
        $temp['customer_phone'] = $mIssue->getCustomer('phone');// Dec 21, 2015
        $temp['contact_person'] = $mIssue->getCustomer('contact_person');// Dec 21, 2015
        
        if($get_reply){ // chưa làm phần view nên tạm close lại
            $mDetail = $mIssue->rTicketDetail;
            foreach($mDetail as $mIssueDetail){
                $temp['reply_item'][] = $this->IssueDetailFormatRecord($mIssueDetail);
            }
        }
        return $temp;
    }
        
    public function IssueDetailFormatRecord($mIssueDetail) {
        $msg = '';
        if(!empty($mIssueDetail->move_to_uid)){
            $msg = $mIssueDetail->getMoveToUid()."\n";
        }
        $temp = array();
        $temp['id']             = $mIssueDetail->id;
        $temp['message']        = $msg.$mIssueDetail->getMessageApp();
        $temp['created_by']     = $mIssueDetail->getUidLoginByApp();
//        $temp['created_by']     = '';// Mar2219 hide thong tin nguoi Post tren app
        $temp['images']         = $mIssueDetail->ApiGetFileUpload();
        $temp['created_date']   = MyFormat::dateConverYmdToDmy($mIssueDetail->created_date, "d/m/Y H:i");
        return $temp;
    }
    
    /**
     * @Author: DungNT Oct 25, 2015
     * @Todo: View 1 phản ánh sự việc của member
     * @Resource: site/issueView
     * @word_index: 3.11.3
     * @ex_json_request: 
     {"token":"dc36cc6fb2db91e515372db1821b6275","issue_id":"1"}
     */
    public function actionIssueView()
    {
        try{
        $result = ApiModule::$defaultResponse;
        $this->checkRequest();
        $q = json_decode($_POST['q']);
        $this->checkRequiredParams($q, array('token', 'issue_id'));
        $mUser = $this->getUserByToken($result, $q->token);// Oct 12, 2015 for check inactive or delete User
        $result = ApiModule::$defaultSuccessResponse;
        $result['message'] = 'Success';
        $this->HandleIssueView($result, $q, $mUser);
        ApiModule::sendResponse($result, $this);
        } catch (Exception $ex) {
            MyFormat::ApiCatchError($ex, $this);
        }
    }
    
    public function HandleIssueView(&$result, $q, $mUser) {
        // 1. get model uphold
        $mIssue = GasIssueTickets::model()->findByPk($q->issue_id);
        if(is_null($mIssue)){
            throw new Exception('Invalid request');
        }
        // Dec 22, 2015 update status view notify history
        GasScheduleNotifyHistory::updateStatusViewByAttribute($mUser->id, GasScheduleNotify::ISSUE_TICKET, $mIssue->id);
        $result['model_issue'] = $this->IssueFormatRecord($mIssue, $mUser, true);
    }

    /**
     * @Author: DungNT Now 26, 2015
     * @Todo: Member reply phản ánh sự việc
     * @Resource: site/issueReply
     * @word_index: 3.7.2 Method: POST 
     * @ex_json_request: {"token":"dc36cc6fb2db91e515372db1821b6275","issue_id":"1","message":"21"}
     */
    public function actionIssueReply()
    {
//            echo '******** DungNT DUMP - POST ***********<pre>';
//            print_r($_POST);
//            echo '</pre><hr><br>';
//            echo '******** DungNT DUMP - FILE ***********<pre>';
//            print_r($_FILES);
//            echo '</pre><hr>';
//            die; 
        try{
        $result = ApiModule::$defaultResponse;
        $this->checkRequest();
        $q = json_decode($_POST['q']);
        $this->checkRequiredParams($q, array('token','issue_id','message'));
        
        // 1. get user by token
        $mUser = $this->getUserByToken($result, $q->token);// Oct 12, 2015 for check inactive or delete User
        // 2. get post and validate
        $mIssue = GasIssueTickets::model()->findByPk($q->issue_id);
        if(is_null($mIssue)){
            throw new Exception('Invalid request');
        }
        $mIssue->scenario = "ApiReply";
        $move_to_uid = "";
        $this->IssueCreateGetPost($result, $q, $mUser, $mIssue);
        // 3. and save
        $result = ApiModule::$defaultSuccessResponse;
        $result['message'] = Yii::t('systemmsg','Reply phản ánh sự việc thành công');
        // 1. Dec 10, 2015 save chief and monitor agent
        $mIssue->setChiefMonitor($q, $move_to_uid);
        // 2.save detail
        $mIssue->apiSaveRecordDetail($mUser->id, $move_to_uid);// Save record Root + detail + file 
        // Chưa tìm dc cách build list notify cho các step như logic ???
        $mIssue->updateStatusApp($mUser, $q);
        //  notify S2 here - Dec 10, 2015
        $mIssue->ApiBuildNotifyS2345();
        
        ApiModule::sendResponse($result, $this);
        } catch (Exception $ex) {
            MyFormat::ApiCatchError($ex, $this);
        }
    }    
    
    /**
     * @Author: DungNT Dec 04, 2015
     * @Todo: autoComplete Search
     * @Resource: site/autocompleteUser
     * @ex_json_request: 
     {"token":"641a7db2fbe6f29077a00adca133e9ea","keyword":"ng"}
     */
    public function actionAutocompleteUser()
    {
        try{
        $result = ApiModule::$defaultResponse;
        $this->checkRequest();
        $q = json_decode($_POST['q']);
        $this->checkRequiredParams($q, array('token', 'keyword'));
        $mUser = $this->getUserByToken($result, $q->token);// Oct 12, 2015 for check inactive or delete User
        $this->setOverrideAgentOfEmployee($mUser);// Jun 05, 2017
//        $aModelUser = Users::getDataAutocomplete($q->keyword, $mUser->role_id, $mUser->id, $q, $needMore);
//        $aModelUser = $mUser->getAppAutocomplete($q, $needMore);
        $aModelUser = $this->routeSearchBoMoi($q, $mUser);

        HandleArrayResponse::AutocompleteUser($aModelUser, $mUser, $this, $q);
        ApiModule::sendResponse($result, $this);
        } catch (Exception $ex) {
            MyFormat::ApiCatchError($ex, $this);
        }
    }

    public function routeSearchBoMoi($q, $mUser) {
        $needMore = [];
        if(!GasCheck::isServerLive()){
            $aModelUser = Users::getDataAutocompleteBk($q->keyword, $mUser->role_id, $mUser->id, $q, $needMore);
        }else{
            $aModelUser = $mUser->getAppAutocomplete($q, $needMore);
        }
        return $aModelUser;
    }

    /**
     * @Author: DungNT Dec 10, 2015
     * @Todo: Close 1 phản ánh sự việc của member
     * @Resource: site/issueClose
     * @word_index: 3.11
     * @ex_json_request: 
     {"token":"dc36cc6fb2db91e515372db1821b6275","issue_id":"1"}
     */
    public function actionIssueClose()
    {
        throw new Exception('Không thể thực hiện hành động Close trên app, vui lòng liên hệ quản lý');
        try{
        $result = ApiModule::$defaultResponse;
        $this->checkRequest();
        $q = json_decode($_POST['q']);
        $this->checkRequiredParams($q, array('token', 'issue_id'));
        $mUser = $this->getUserByToken($result, $q->token);// Oct 12, 2015 for check inactive or delete User
        $result = ApiModule::$defaultSuccessResponse;
        $result['message'] = 'Success';
        // 2. get post and validate
        $mIssue = GasIssueTickets::model()->findByPk($q->issue_id);
        if(is_null($mIssue)){
            throw new Exception('Invalid request');
        }
        $mIssue->ApiCloseTicket($mUser->id);
        ApiModule::sendResponse($result, $this);
        } catch (Exception $ex) {
            MyFormat::ApiCatchError($ex, $this);
        }
    }    
    
    /**
     * @Author: DungNT Dec 10, 2015
     * @Todo: Reopen 1 phản ánh sự việc của member
     * @Resource: site/issueReopen
     * @word_index: 3.11
     * @ex_json_request: 
     {"token":"dc36cc6fb2db91e515372db1821b6275","issue_id":"1"}
     */
    public function actionIssueReopen()
    {
        try{
        $result = ApiModule::$defaultResponse;
        $this->checkRequest();
        $q = json_decode($_POST['q']);
        $this->checkRequiredParams($q, array('token', 'issue_id'));
        $mUser = $this->getUserByToken($result, $q->token);// Oct 12, 2015 for check inactive or delete User
        $result = ApiModule::$defaultSuccessResponse;
        $result['message'] = 'Success';
        // 2. get post and validate
        $mIssue = GasIssueTickets::model()->findByPk($q->issue_id);
        if(is_null($mIssue)){
            throw new Exception('Invalid request');
        }
        // Khi Reopen chắc sẽ chuyển về trạng thái 2 STATUS_2_SALE_REPLY
        GasIssueTickets::ReopenTicket($mIssue);
        ApiModule::sendResponse($result, $this);
        } catch (Exception $ex) {
            MyFormat::ApiCatchError($ex, $this);
        }
    }    
    
    /**
     * @Author: DungNT Dec 22, 2015
     * @Todo: client confirm đã nhận được notify, sẽ send request confirm lên server
     * lúc này xóa bên table schedule đi và insert vào table history
     * @Resource: site/confirmNotify
     * @word_index: 
     * @ex_json_request: 
     {"token":"dc36cc6fb2db91e515372db1821b6275","notify_id":"1","type":"1","obj_id":"1"}
     */
    public function actionConfirmNotify()
    {
        try{
        $result = ApiModule::$defaultResponse;
        $this->checkRequest();
        $q = json_decode($_POST['q']);
        $this->checkRequiredParams($q, array('token', 'notify_id', "type", 'obj_id'));
        $mUser = $this->getUserByToken($result, $q->token);// Oct 12, 2015 for check inactive or delete User
        $result = ApiModule::$defaultSuccessResponse;
        $result['message'] = 'Success';
        // 2. get post and validate
        if(!empty($q->notify_id) && !empty($q->type) && !empty($q->obj_id)){
            $mScheduleNotify = GasScheduleNotify::getRecord($mUser->id, $q->notify_id, $q->type, $q->obj_id);
            if($mScheduleNotify){
                GasScheduleNotifyHistory::InsertNew($mScheduleNotify);
            }
        }
        
        ApiModule::sendResponse($result, $this);
        } catch (Exception $ex) {
            MyFormat::ApiCatchError($ex, $this);
        }
    }  

    /**
     * @Author: DungNT Dec 22, 2015
     * @Todo: list notify của member
     * @Resource: site/notifyList
     * @word_index:  Method: POST 
     * @ex_json_request: {"token":"dc36cc6fb2db91e515372db1821b6275","page":"0","type":"1"}
     * type = 1 những notify chưa view
     * type = 2 đã view
     */
    public function actionNotifyList()
    {
        try{
        $result = ApiModule::$defaultResponse;
        $this->checkRequest();
        $q = json_decode($_POST['q']);
        $this->checkRequiredParams($q, array('token', 'page', 'type'));
        $mUser = $this->getUserByToken($result, $q->token);// Oct 12, 2015 for check inactive or delete User
        $result = ApiModule::$defaultSuccessResponse;
        $result['message'] = 'Success';
        $this->HandleNotifyList($result, $q, $mUser);
        ApiModule::sendResponse($result, $this);
        } catch (Exception $ex) {
            MyFormat::ApiCatchError($ex, $this);
        }
    }
    
    /**
     * @Author: DungNT Dec 22, 2015
     */
    public function HandleNotifyList(&$result, $q, $mUser) {
        // 1. get list notify history by user id
        $dataProvider = GasScheduleNotifyHistory::ApiListing($q, $mUser);
        $models = $dataProvider->data;
        $CPagination = $dataProvider->pagination;
        $result['total_record'] = $CPagination->itemCount;
        $result['total_page'] = $CPagination->pageCount;
        if( $q->page >= $CPagination->pageCount ){
            $result['record'] = array();
        }else{
            foreach($models as $mNotifyHistory){
                $result['record'][] = $this->NotifyFormatRecord($mNotifyHistory, $mUser);
            }
        }
    }
    
    // belong to HandleNotifyList
    public function NotifyFormatRecord($mNotifyHistory, $mUser) {
        $temp = array();
        $temp['id']             = $mNotifyHistory->id;
        $temp['status']         = $mNotifyHistory->status;
//        $temp['user_id'] = $mNotifyHistory->user_id;
        $temp['type']           = $mNotifyHistory->type;
        $temp['obj_id']         = $mNotifyHistory->obj_id;
        $temp['time_send']      = MyFormat::dateConverYmdToDmy($mNotifyHistory->time_send, "d/m/Y H:i");
        $temp['created_date']   = MyFormat::dateConverYmdToDmy($mNotifyHistory->time_send, "d/m/Y H:i");
        $temp['created_date_on_history'] = MyFormat::dateConverYmdToDmy($mNotifyHistory->time_send, "d/m/Y H:i");
        $temp['title']          = $mNotifyHistory->title;// Dec 10, 2015 check xem user có dc Reply dưới app không, sẽ modify lại sau
        $temp['reply_id']       = $mNotifyHistory->getFromJsonVar('reply_id');
        return $temp;
    }
    
    
    /**
     * @Author: DungNT Dec 22, 2015
     * @Todo: redirect view notify của member
     * @Resource: site/notifyView
     * @word_index:  Method: POST 
     * @ex_json_request: {"token":"dc36cc6fb2db91e515372db1821b6275","id":"1"}
     */
    public function actionNotifyView()
    {
        try{
        $result = ApiModule::$defaultResponse;
        $this->checkRequest();
        $q = json_decode($_POST['q']);
        $this->checkRequiredParams($q, array('token', 'id'));
        $mUser = $this->getUserByToken($result, $q->token);// Oct 12, 2015 for check inactive or delete User
        if(!empty($q->id)){
            $mNotifyHistory = GasScheduleNotifyHistory::getByUserId($q->id, $mUser->id);
            if($mNotifyHistory){
                $mNotifyHistory->setStatus(GasScheduleNotifyHistory::STATUS_VIEW);
                $result = ApiModule::$defaultSuccessResponse;
                $result['message'] = 'Success';
            }
        }
        ApiModule::sendResponse($result, $this);
        } catch (Exception $ex) {
            MyFormat::ApiCatchError($ex, $this);
        }
    }
    
    /**
     * @Author: DungNT Dec 22, 2015
     * @Todo: count new notify của member, luôn chạy khi user click vào home menu
     * @Resource: site/notifyCount
     * @word_index:  Method: POST 
     * @ex_json_request: {"token":"dc36cc6fb2db91e515372db1821b6275"}
     */
    public function actionNotifyCount()
    {
        try {
            $result = ApiModule::$defaultResponse;
            $this->checkRequest();
            $q = json_decode($_POST['q']);
            $this->checkRequiredParams($q, array('token'));
            $mUser = $this->getUserByToken($result, $q->token);// Oct 12, 2015 for check inactive or delete User
            UsersTokens::setLastActive($q->token);
            $result = ApiModule::$defaultSuccessResponse;
            $result['message'] = 'Success';
//      Close on mar 12, 2017  $CountNotifyNew = GasScheduleNotifyHistory::countByUserId($mUser->id, GasScheduleNotifyHistory::STATUS_NEW);
            $CountNotifyNewText = '';
            $CountNotifyNew = 0;
//        if($mUser->role_id == ROLE_CUSTOMER){ //Close on mar 12, 2017
//            $CountNotifyNew = 0;
//        }
//        if($CountNotifyNew>0){
//            $CountNotifyNewText = "Bạn có $CountNotifyNew tin nhắn mới";
//        }
            $aOtherInfo = array();
            GasUphold::apiGetLatestUphold($mUser, $aOtherInfo);
            $result['NotifyCountText'] = $CountNotifyNewText;
            $result['OtherInfo']        = $aOtherInfo;
            $result['app_version_code'] = Yii::app()->setting->getItem('AppVersionCode');
//            $result['app_version_code'] = 1;// Dec2617 bỏ kiểu check cũ này đi, vì đã có hàm riêng để check version code rồi. Kiều này không handle đc cho 2 app

            // Load address
            $result['address']              = $mUser->getAddressApp();
            ApiModule::sendResponse($result, $this);
        } catch (Exception $ex) {
            MyFormat::ApiCatchError($ex, $this);
        }
    }
    
    /**
     * @Author: DungNT Apr 15, 2015
     * @Todo: Tạo rating bảo trì
     * @Resource: site/upholdCustomerRating
     * @word_index: 3.11.1 Method: POST
     * @ex_json_request: {"token":"dc36cc6fb2db91e515372db1821b6275","uphold_id":"1","rating_status":"2","rating_type":{1:5, 2:3, 3:3, 4:2},"rating_note":"rate note api"}
     * array ('q' => '{"rating_status":"","uphold_id":"3378","rating_type":{"3":0,"2":0,"1":5,"4":0},"rating_note":"","token":"241eff87cf0a5cefb97be7ed45865d55"}',)
     */
    public function actionUpholdCustomerRating()
    {
        try{
        $result = ApiModule::$defaultResponse;
        $this->checkRequest();
        $q = json_decode($_POST['q']);
        $this->checkRequiredParams($q, array('token','uphold_id'));
        // 1. get user by token
        $mUser = $this->getUserByToken($result, $q->token);// Oct 12, 2015 for check inactive or delete User
        // 2. get post and validate
        $mUphold = $this->getModelUphold($q);
        $this->UpholdCustomerRatingGetPost($result, $q, $mUser, $mUphold);
        // 3. and save
        $result = ApiModule::$defaultSuccessResponse;
        $result['message'] = 'Đánh giá bảo trì thành công';
//        $mUphold->apiSaveCreate();
        $result['id'] = $mUphold->id."";
        
        ApiModule::sendResponse($result, $this);
        } catch (Exception $ex) {
            MyFormat::ApiCatchError($ex, $this);
        }
    }
    
    /**
     * @Author: DungNT Apr 16, 2016
     * map attribute to model form $q. get post and validate
     * @param: $result, $q, $mUser
     * @param: $model is $mUphold
     */
    public function UpholdCustomerRatingGetPost($result, $q, $mUser, &$mUphold) {
        // Sau 2 ngày không cho đánh giá nữa
//        if(!$mUphold->canRating() || empty($q->rating_status)){
//            return;
//        }// sẽ open khi live, chỉ close lại khi tes
        $mUphold->rating_status = $q->rating_status;
        $mUphold->rating_note = InputHelper::removeScriptTag($q->rating_note);
        $aUpdate = array('rating_status', 'rating_note');
        $mUphold->update($aUpdate);
        $qArray = json_decode($_POST['q'], true);
        $aRatingType = $qArray['rating_type'];
        GasUpholdRating::doCreate($mUphold, $aRatingType);
    }
    
    
}
