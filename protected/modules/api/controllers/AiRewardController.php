<?php
/** @Author: NGUYEN KHANH TOAN  Mar 10 2019
*  @Todo: 
*  @Param:
**/
class AiRewardController extends ApiController
{
    /** @Author: NamNh
     *  @Todo: get list category
     *  @Resource: reward/getCategory
     *  @ex_json_request: {"token":"f3a678185e63438199eff591ae7e22de"}
     **/
    public function actionGetCategory() {
        try {
            $result = ApiModule::$defaultResponse;
            $this->checkRequest();
            $q = json_decode($_POST['q']);
            $this->checkRequiredParams($q, array('token'));
            $_GET['platform']       = isset($q->platform) ? $q->platform : UsersTokens::PLATFORM_ANDROID;
            $_GET['version_code']   = isset($q->version_code) ? $q->version_code : 1;
            $mUser = $this->getUserByToken($result, $q->token);// Oct 12, 2015 for check inactive or delete User
            $this->mUserApp = $mUser;
            $result = ApiModule::$defaultSuccessResponse;
            $result['message'] = 'List';
            $mReward = new Reward();
            $mReward->mAppUserLogin = $mUser;
            $mReward->handleApiListCategory($result, $q);
            ApiModule::sendResponse($result, $this);
        } catch (Exception $ex) {
            MyFormat::ApiCatchError($ex, $this);
        }
    }
    
     /** @Author: NGUYEN KHANH TOAN Mar 07 2018
     *  @Todo: get list reward
     *  @Resource: reward/listReward
     *  @ex_json_request: {"token":"f3a678185e63438199eff591ae7e22de","page":"0"}
     **/
    public function actionListReward() {
        try {
            $result = ApiModule::$defaultResponse;
            $this->checkRequest();
            $q = json_decode($_POST['q']);
            $this->checkRequiredParams($q, array('token'));
            $_GET['platform']       = isset($q->platform) ? $q->platform : UsersTokens::PLATFORM_ANDROID;
            $_GET['version_code']   = isset($q->version_code) ? $q->version_code : 1;
            $mUser = $this->getUserByToken($result, $q->token);// Oct 12, 2015 for check inactive or delete User
            $this->mUserApp = $mUser;
            $result = ApiModule::$defaultSuccessResponse;
            $result['message'] = 'List';
            $mReward = new Reward();
            $mReward->mAppUserLogin = $mUser;
            $mReward->handleApiListReward($result, $q);
            ApiModule::sendResponse($result, $this);
        } catch (Exception $ex) {
            MyFormat::ApiCatchError($ex, $this);
        }
    }
    
    /** @Author: NamNH Apr 24, 2019
     *  @Todo: get rewards by category
     *  reward/getRewardsByCategory
     *  {"token":"bde88a68cb11aeb08ba9069429ff22db","catrgory":"1","page":"0"}
     **/
    public function actionGetRewardsByCategory() {
        try {
            $result = ApiModule::$defaultResponse;
            $this->checkRequest();
            $q = json_decode($_POST['q']);
            $this->checkRequiredParams($q, array('token','page','type'));
            $_GET['platform']       = isset($q->platform) ? $q->platform : UsersTokens::PLATFORM_ANDROID;
            $_GET['version_code']   = isset($q->version_code) ? $q->version_code : 1;
            $mUser = $this->getUserByToken($result, $q->token);// Oct 12, 2015 for check inactive or delete User
            $this->mUserApp = $mUser;
            $result = ApiModule::$defaultSuccessResponse;
            $mReward = new Reward();
            $mReward->mAppUserLogin = $mUser;
            $mReward->handleApiListRewardByCategory($result, $q);
            ApiModule::sendResponse($result, $this);
        } catch (Exception $ex) {
            MyFormat::ApiCatchError($ex, $this);
        }
    }
    
    /** @Author: NamNH Apr 24, 2019
     *  @Todo: get rewards by category
     *  reward/getPoint
     *  {"token":"bde88a68cb11aeb08ba9069429ff22db"}
     **/
    public function actionGetPoint() {
        try {
            $result = ApiModule::$defaultResponse;
            $this->checkRequest();
            $q = json_decode($_POST['q']);
            $this->checkRequiredParams($q, array('token'));
            $_GET['platform']       = isset($q->platform) ? $q->platform : UsersTokens::PLATFORM_ANDROID;
            $_GET['version_code']   = isset($q->version_code) ? $q->version_code : 1;
            $mUser = $this->getUserByToken($result, $q->token);// Oct 12, 2015 for check inactive or delete User
            $this->mUserApp = $mUser;
            $result = ApiModule::$defaultSuccessResponse;
            $result['message'] = 'List';
            $mReward = new RewardPoint();
            $mReward->mAppUserLogin = $mUser;
            $mReward->handlePoint($result, $q);
            ApiModule::sendResponse($result, $this);
        } catch (Exception $ex) {
            MyFormat::ApiCatchError($ex, $this);
        }
    }
    
    /** @Author: KHANH TOAN Mar 07 2019
     *  @Todo: render view reward
     *  @Resource: reward/rewardView
     *  @ex_json_request: {"token":"f3a678185e63438199eff591ae7e22de","id":"2"}
     **/
    public function actionRewardView() {
        try {
            $result = ApiModule::$defaultResponse;
            $this->checkRequest();
            $q = json_decode($_POST['q']);
            $this->checkRequiredParams($q, array('token','id'));
            $mUser = $this->getUserByToken($result, $q->token);// Oct 12, 2015 for check inactive or delete User
            $result = ApiModule::$defaultSuccessResponse;
            $mReward                = $this->getRewardRecord($q->id);
            $mReward->mAppUserLogin    = $mUser;
            $result['record']       = $mReward->formatAppItemListReward(true);
            ApiModule::sendResponse($result, $this);
        } catch (Exception $ex) {
            MyFormat::ApiCatchError($ex, $this);
        }
    }
    
    /** @Author: KHANH TOAN Mar 07 2019
     *  @Todo: get record reward valid
     **/
    public function getRewardRecord($id) {
        $mReward               = new Reward();
        $mReward->id           = $id;
        $mReward               = $mReward->getRewardView();
        if(is_null($mReward)){
            throw new Exception('Yêu cầu không hợp lệ');
        }
        return $mReward;
    }
    
    /** @Author: NGUYEN KHANH TOAN Mar 07 2018
     *  @Todo: get list history exchange
     *  @Resource: reward/historyExchange
     *  @ex_json_request: {"token":"f3a678185e63438199eff591ae7e22de","page":"0","customer_id":"868408"}
     **/
     public function actionHistoryExchange() {
        try {
            $result = ApiModule::$defaultResponse;
            $this->checkRequest();
            $q = json_decode($_POST['q']);
            $this->checkRequiredParams($q, array('token','page','type'));
            $_GET['platform']       = isset($q->platform) ? $q->platform : UsersTokens::PLATFORM_ANDROID;
            $_GET['version_code']   = isset($q->version_code) ? $q->version_code : 1;
            $mUser = $this->getUserByToken($result, $q->token);// Oct 12, 2015 for check inactive or delete User
            $this->mUserApp = $mUser;
            $result = ApiModule::$defaultSuccessResponse;
            switch ($q->type){
                case Reward::TYPE_POINT:
                    $mCodePartner = new RewardPoint();
                    $mCodePartner->mAppUserLogin = $mUser;
                    $mCodePartner->handleApiListPoint($result, $q);
                    break;
                case Reward::TYPE_USE:
                    $mCodePartner = new CodePartner();
                    $mCodePartner->mAppUserLogin = $mUser;
                    $mCodePartner->handleApiListExchangeReward($result, $q);
                    break;
                
            }
            ApiModule::sendResponse($result, $this);
        } catch (Exception $ex) {
            MyFormat::ApiCatchError($ex, $this);
        }
    }
    
     /** @Author: NGUYEN KHANH TOAN Mar 12 2019
     *  @Todo: exchange point
     *  @Resource: reward/ExchangePoint
     *  @ex_json_request: {"token":"f3a678185e63438199eff591ae7e22de","customer_id":"1700427","reward_id":"2"}
     **/
    public function actionExchangePoint(){
        try {
            $result = ApiModule::$defaultResponse;
            $this->checkRequest();
            $q = json_decode($_POST['q']);
            $this->checkRequiredParams($q, array('token','reward_id'));
            $_GET['platform']       = isset($q->platform) ? $q->platform : UsersTokens::PLATFORM_ANDROID;
            $_GET['version_code']   = isset($q->version_code) ? $q->version_code : 1;
            $mUser = $this->getUserByToken($result, $q->token);// Oct 12, 2015 for check inactive or delete User
            $this->mUserApp = $mUser;
            $result = ApiModule::$defaultSuccessResponse;
            $mCodePartner = new CodePartner();
            $mCodePartner->mAppUserLogin = $mUser;
            $mCodePartner->handleApiExchangePoint($result, $q);
            ApiModule::sendResponse($result, $this);
        } catch (Exception $ex) {
            MyFormat::ApiCatchError($ex, $this);
        }
    }
    
    /** @Author: NGUYEN KHANH TOAN Mar 07 2018
     *  @Todo: get list reward setup
     *  @Resource: rewardSetup/listRewardSetup
     *  @ex_json_request: {"token":"f3a678185e63438199eff591ae7e22de","page":"0"}
     **/
     public function actionListRewardSetup() {
        try {
            $result = ApiModule::$defaultResponse;
            $this->checkRequest();
            $q = json_decode($_POST['q']);
            $this->checkRequiredParams($q, array('token', 'page'));
            $_GET['platform']       = isset($q->platform) ? $q->platform : UsersTokens::PLATFORM_ANDROID;
            $_GET['version_code']   = isset($q->version_code) ? $q->version_code : 1;
            $mUser = $this->getUserByToken($result, $q->token);// Oct 12, 2015 for check inactive or delete User
            $this->mUserApp = $mUser;
            $result = ApiModule::$defaultSuccessResponse;
            $result['message'] = 'List';
            $mRewardSetup = new RewardSetup();
            $mRewardSetup->mAppUserLogin = $mUser;
            $mRewardSetup->handleApiListRewardSetup($result, $q);
            ApiModule::sendResponse($result, $this);
        } catch (Exception $ex) {
            MyFormat::ApiCatchError($ex, $this);
        }
    }
    
    /** @Author: NamNh Jun 26,2019
     *  @Todo: view code
     *  @Resource: aiReward/codeView
     *  @ex_json_request: {"token":"335045e2bd4ca691a5cd121c06a72b92","code_id":"1"}
     **/
    public function actionCodeView(){
        try {
            $result = ApiModule::$defaultResponse;
            $this->checkRequest();
            $q = json_decode($_POST['q']);
            $this->checkRequiredParams($q, array('token', 'code_id'));
            $_GET['platform']       = isset($q->platform) ? $q->platform : UsersTokens::PLATFORM_ANDROID;
            $_GET['version_code']   = isset($q->version_code) ? $q->version_code : 1;
            $mUser = $this->getUserByToken($result, $q->token);// Oct 12, 2015 for check inactive or delete User
            $this->mUserApp = $mUser;
            $result = ApiModule::$defaultSuccessResponse;
            $mCodePartner = new CodePartner();
            $mCodePartner->mAppUserLogin = $mUser;
            $mCodePartner->handleViewReward($result,$q);
            ApiModule::sendResponse($result, $this);
        } catch (Exception $ex) {
            MyFormat::ApiCatchError($ex, $this);
        }
    }
}
