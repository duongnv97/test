<?php
/**
 * @Author: DuongNV Aug2119
 * @Todo: API quay số gas24h
 */
class AiSpinController extends ApiController
{
    
    /** @Author: DuongNV Aug2119
     *  @Todo: ds vé số của user
     *  @Resource: aiSpin/GetListNumber
     *  @ex_json_request: {"token":"c836180fd1a4d4da46531465693ea970"}
     **/
    public function actionGetListNumber() {
        try {
            $result                 = ApiModule::$defaultResponse;
            $this->checkRequest();
            $q                      = json_decode($_POST['q']);
            $this->checkRequiredParams($q, array('token'));
            $_GET['platform']       = isset($q->platform) ? $q->platform : UsersTokens::PLATFORM_ANDROID;
            $_GET['version_code']   = isset($q->version_code) ? $q->version_code : 1;
            $mUser = $this->getUserByToken($result, $q->token);// Oct 12, 2015 for check inactive or delete User
            $this->mUserApp         = $mUser;
            $result                 = ApiModule::$defaultSuccessResponse;
            $result['message']      = 'List numbers';
            $mSpinNumber            = new SpinNumbers();
            $mSpinNumber->mAppUserLogin = $mUser;
            $mSpinNumber->apiGetListNumber($result, $q);
            ApiModule::sendResponse($result, $this);
        } catch (Exception $ex) {
            MyFormat::ApiCatchError($ex, $this);
        }
    }
    
    /** @Author: DuongNV Aug2119
     *  @Todo: lịch sử nhận vé
     *  @Resource: aiSpin/GetHistoryNumber
     *  @ex_json_request: {"token":"c836180fd1a4d4da46531465693ea970","page":"0"}
     **/
    public function actionGetHistoryNumber() {
        try {
            $result                 = ApiModule::$defaultResponse;
            $this->checkRequest();
            $q                      = json_decode($_POST['q']);
            $this->checkRequiredParams($q, array('token', 'page'));
            $_GET['platform']       = isset($q->platform) ? $q->platform : UsersTokens::PLATFORM_ANDROID;
            $_GET['version_code']   = isset($q->version_code) ? $q->version_code : 1;
            $mUser = $this->getUserByToken($result, $q->token);// Oct 12, 2015 for check inactive or delete User
            $this->mUserApp         = $mUser;
            $result                 = ApiModule::$defaultSuccessResponse;
            $result['message']      = 'Number history';
            $mSpinNumber            = new SpinNumbers();
            $mSpinNumber->mAppUserLogin = $mUser;
            $mSpinNumber->apiGetNumberHistory($result, $q);
            ApiModule::sendResponse($result, $this);
        } catch (Exception $ex) {
            MyFormat::ApiCatchError($ex, $this);
        }
    }
    
    /** @Author: DuongNV Aug2219
     *  @Todo: 
     *  @Resource: aiSpin/GetHistoryCampaign
     *  @ex_json_request: {"token":"c836180fd1a4d4da46531465693ea970","page":"0"}
     **/
    public function actionGetHistoryCampaign() {
        try {
            $result                 = ApiModule::$defaultResponse;
            $this->checkRequest();
            $q                      = json_decode($_POST['q']);
            $this->checkRequiredParams($q, array('token', 'page'));
            $_GET['platform']       = isset($q->platform) ? $q->platform : UsersTokens::PLATFORM_ANDROID;
            $_GET['version_code']   = isset($q->version_code) ? $q->version_code : 1;
            $mUser = $this->getUserByToken($result, $q->token);// Oct 12, 2015 for check inactive or delete User
            $this->mUserApp         = $mUser;
            $result                 = ApiModule::$defaultSuccessResponse;
            $result['message']      = 'Campaign history';
            $mSpinCampaign          = new SpinCampaigns();
            $mSpinCampaign->mAppUserLogin = $mUser;
            $mSpinCampaign->apiGetCampaignHistory($result, $q);
            ApiModule::sendResponse($result, $this);
        } catch (Exception $ex) {
            MyFormat::ApiCatchError($ex, $this);
        }
    }
    
    

}
