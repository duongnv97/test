<?php 
class ForecastController extends ApiController
{
 
    /** @Author: NamNH 09/2018
     *  @Todo: Lấy dánh sách khách hàng có thông báo hết gas hôm nay
     *  @Resource: Forecast/forecastView
     *  @ex_json_request: {"token":"d63794b42dcf2a183dd8b1bb46026a62"}
     **/
    public function actionForecastView(){
        $result     = ApiModule::$defaultResponse;
        $this->checkRequest();
        $q          = json_decode($_POST['q']);
        $this->checkRequiredParams($q, array('token'));
        $mUser      = $this->getUserByToken($result, $q->token);
        $result     = ApiModule::$defaultSuccessResponse;
        $mForecast  = new Forecast();
        $mForecast->mAppUserLogin = $mUser;
//        set result
        $result['record']           = $mForecast->apiViewForecast();
        $result['record']['schedule']           = $this->getViewSchedule($mUser);
        ApiModule::sendResponse($result, $this);
    }
    
    /** @Author: NamNH Oct 02, 2018
     *  @Todo: get last order and set attributes new order
     **/
    public function getSell($idCustomer,$aMoreCondition = []){
        $criteria           = new CDbCriteria;
        $criteria->compare('t.customer_id',$idCustomer);
        $criteria->order    = 't.id DESC';
        if(isset($aMoreCondition['status'])){
            $criteria->addInCondition('t.status', $aMoreCondition['status']);
        }
        if(isset($aMoreCondition['timer'])){
            $criteria->addCondition('t.delivery_timer IS NOT NULL');
        }
        if(isset($aMoreCondition['is_timer'])){
            $criteria->compare('t.is_timer',$aMoreCondition['is_timer']);
        }
        $mSell              = Sell::model()->find($criteria);
        return $mSell;
    }
    
    /** @Author: NamNH Oct 02, 2018
     *  @Todo: get last transaction and set attributes new transaction
     **/
    public function getTransaction($idCustomer,$aMoreCondition = []){
        $criteria           = new CDbCriteria;
        $criteria->compare('t.customer_id',$idCustomer);
        $criteria->order    = 't.id DESC';
        if(isset($aMoreCondition['status'])){
            $criteria->addInCondition('t.status', $aMoreCondition['status']);
        }
        if(isset($aMoreCondition['not_in_status'])){
            $criteria->addNotInCondition('t.status', $aMoreCondition['not_in_status']);
        }
        $mSell              = TransactionHistory::model()->find($criteria);
        return $mSell;
    }
    
    /** @Author: NamNH Oct 10, 2018
     *  @Todo: get view schedule
     **/
    public function getViewSchedule($mUser){
        $mTransaction   = new Transaction();
        $aParamNew         = $mTransaction->getAppStatusNew();
        $aResult                    = null;
        //        get order
        $MoreCondition              = [
                'status'  =>   $aParamNew,
        ];
        $newTransaction                   = $this->getTransaction($mUser->id,$MoreCondition);
        if(!empty($newTransaction)){
            $aResult = $newTransaction->apiTimerGetRecord();
        }
        return $aResult;
    }
    
}

