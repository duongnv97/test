<?php

/**
 * @Author: ANH DUNG Aug 5, 2017
 * @Todo: Handle referral
 */
class ReferralController extends ApiController
{
    /** referral/referralInfo
     * get user referral info
     */
    public function actionReferralInfo()
    {
        try {
            $result = ApiModule::$defaultResponse;
            $record = [];
            $this->checkRequest();
            $q = json_decode($_POST['q']);
            $this->checkRequiredParams($q, array('token'));
            /** @var Users $mUser */
            $mUser = $this->getUserByToken($result, $q->token);
            
            // Get invited list
            $invitedList = [];
            /** @var ReferralTracking $child */
            $mReferralTracking = new ReferralTracking();
            $mReferralTracking->ref_id = $mUser->id;
            $mChildRefs = $mReferralTracking->getChildReferrals();
            foreach ($mChildRefs as $child) {
//                if ($child->invited_user) { // NOw1018 AnhDung close, do app chưa xử lý phân trang, cũng ko show data nên ko cần thiết phải lấy data từ relation
                    $invitedList[] = [
//                        'name'  => $child->invited_user->getFullName(),
//                        'status' => $child->getRefStatus()
                        'name'      => 'n',
                        'status'    => 's',
                    ];
//                }
            }

            /** @var AppPromotion $mPromotionCodeInfo info of referral code for each user, may null */
            $mPromotionCodeInfo                 = new AppPromotion();
            $mPromotionCodeInfo->owner_id       = $mUser->id;
            $mPromotionCodeInfo                 = $mPromotionCodeInfo->getMyReferralInfo();
            $mReferralTracking                  = new ReferralTracking();
            $mReferralTracking->invited_id      = $mUser->id;
            $mReferralTracking->mAppPromotion   = $mPromotionCodeInfo;

            $record['is_invited']   = $mReferralTracking->isInvitedUser();
            $record['invite_code']  = $mPromotionCodeInfo ? $mPromotionCodeInfo->code_no : '';
            $record['invited_list'] = $invitedList;
            $record['current_point'] = $mReferralTracking->getCurrentPoint();
            $mRewardPoint = new RewardPoint();
            $record['current_point_text'] = $mRewardPoint->getPointText($mUser->id);
            $record['total_referral_text'] = $mReferralTracking->getCountchildText();

            // Set data to record
            $result = ApiModule::$defaultSuccessResponse;
            $result['record'] = $record;

            ApiModule::sendResponse($result, $this);
        } catch (Exception $ex) {
            MyFormat::ApiCatchError($ex, $this);
        }
    }

    public function actionCode($code)
    {
        // Example for referral code
        $requestUri = "http://" . $_SERVER['HTTP_HOST'] . Yii::app()->request->requestUri;
        $paramEncoded = urlencode('referral_id=' . $code);

        echo '<font size="10"></br></br><a href ="http://android.huongminhgroup.com/apk/gas24h.apk"> Install App test</a></font>';
    }
}
