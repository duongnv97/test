<?php
/**
 * @Author: ANH DUNG Now 22, 2018
 * @Todo: Handle action partner app Gas24h
 */
class PartnerController extends ApiController
{
    /**
     * @Author: ANH DUNG Now 22, 2018
     * @Todo: render list code partner
     * @Resource: partner/codeList
     * * @ex_json_request: {"token":"8bd307c75295594eebce9639dc898292","page":"0", "code_no":""}
     */
    public function actionCodeList()
    {
        try {
            $result = ApiModule::$defaultResponse;
            $this->checkRequest();
            $q = json_decode($_POST['q']);
            $this->checkRequiredParams($q, array('token', 'page', 'code_no'));
            $mUser = $this->getUserByToken($result, $q->token);// Oct 12, 2015 for check inactive or delete User
            $this->mUserApp = $mUser;
            $agent_id   = $this->getAgentOfEmployee($mUser);
            $result = ApiModule::$defaultSuccessResponse;
            $mCodePartner = new CodePartner();
            $mCodePartner->mAppUserLogin        = $mUser;
            $mCodePartner->agent_id             = $agent_id;
            $mCodePartner->handleApiList($result, $q);
            ApiModule::sendResponse($result, $this);
        } catch (Exception $ex) {
            MyFormat::ApiCatchError($ex, $this);
        }
    }

    /**
     * @Author: ANH DUNG Now 22, 2018
     * @Todo: app add code partner 
     * @Resource: partner/codeAdd
     * @ex_json_request: {"token":"8bd307c75295594eebce9639dc898292","code_no":"153225"}
     */
    public function actionCodeAdd()
    {
        try {
            $result = ApiModule::$defaultResponse;
            $this->checkRequest();
            $q = json_decode($_POST['q']);
            $this->checkRequiredParams($q, array('token', 'code_no'));
            $mUser = $this->getUserByToken($result, $q->token);
            $agent_id   = $this->getAgentOfEmployee($mUser);

            $mCodePartner = new CodePartner();
            $mCodePartner->mAppUserLogin        = $mUser;
            $mCodePartner->agent_id             = $agent_id;
            $mCodePartner->code_no              = $q->code_no;
            $mCodePartner->trackingPlusOneTimeInput();
            $mCodePartner                       = $mCodePartner->getByCodeFromApp();// validate code_no
            if ($mCodePartner->hasErrors()) {
                $result['message'] = HandleLabel::FortmatErrorsModel($mCodePartner->getErrors());
                ApiModule::sendResponse($result, $this);
            }
            $result = ApiModule::$defaultSuccessResponse;
            $result['message'] = "Nhập mã {$q->code_no} thành công";
            $mCodePartner->appApplyCode();
            ApiModule::sendResponse($result, $this);
        } catch (Exception $ex) {
            MyFormat::ApiCatchError($ex, $this);
        }
    }
    

}
