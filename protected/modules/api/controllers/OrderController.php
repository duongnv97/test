<?php

class OrderController extends ApiController
{ 
    /**
     * @Author: ANH DUNG Oct 19, 2016
     * @Todo: app dashboard
     * @Resource: order/getConfig
     */
    public function actionGetConfig()
    {
        try {
            $result = ApiModule::$defaultSuccessResponse;
            $result['status'] = 0;
//            $q = json_decode($_POST['q']);
//            $this->checkRequiredParams($q, array('token'));
//            $mUser = $this->getUserByToken($result, $q->token);// Oct 12, 2015 for check inactive or delete User
            $mAppOrder = new AppOrder();
            $result['message'] = 'Success config';
            $result['record']['agent'] = $mAppOrder->handleGetConfig();
            $result['record']['distance_1'] = Yii::app()->params['distanceAgentApp'];
            $result['record']['distance_2'] = Yii::app()->params['distanceAgentApp'];
            $result['status'] = 1;
            ApiModule::sendResponse($result, $this);
        } catch (Exception $ex) {
            MyFormat::ApiCatchError($ex, $this);
        }
    }

    /**
     * @Author: ANH DUNG Oct 24, 2016
     * @Todo: app gen transaction_key for one connection
     * @Resource: order/transactionStart
     * @ex_json_request: {"token":"dc36cc6fb2db91e515372db1821b6275"}
     */
    public function actionTransactionStart()
    {
        try {
//            GasCheck::randomSleep();// Aug1619 có thể làm slow server, tạm tắt 
            $result = ApiModule::$defaultResponse;
            $this->checkRequest();
            $q = json_decode($_POST['q']);
            $this->checkRequiredParams($q, array('token'));
            $mUser = $this->getUserByToken($result, $q->token);
            $this->checkVersionCodeGas24h($q, $mUser);

            $mTransaction = new Transaction();
            $mTransaction->customer_id = $mUser->id;
//            $mTransactionOld            = $mTransaction->getByCustomer();
//            if($mTransactionOld){
//                $this->transactionStartLog();// Dec0617 tạm close lại đã
//            }

            $mTransaction->genNewTransactionKey();
            $mTransaction->type_customer = $mUser->is_maintain;
            $mTransaction->platform = $q->platform;
            $mTransaction->save();

            $mTransactionHistory = new TransactionHistory();
            $mTransactionHistory->customer_id = $mUser->id;
            $mTransactionHistory->mAppUserLogin = $mUser;

            $result = ApiModule::$defaultSuccessResponse;
            $result['message'] = 'Transaction start';
            $result['record']['session_id'] = $mTransaction->id;
            $result['record']['session_key'] = $mTransaction->transaction_key;
            $result['record']['last_order'] = $mTransactionHistory->getAppViewLastestHistory();
            ApiModule::sendResponse($result, $this);
        } catch (Exception $ex) {
            MyFormat::ApiCatchError($ex, $this);
        }
    }

    public function transactionStartLog()
    {
        $json = isset($_POST) ? MyFormat::jsonEncode($_POST) : ' Nodata';
        $info = 'API ERROR -- Duplicate Request api/order/transactionStart -- DATA: ' . $json;
        Logger::WriteLog($info);
        throw new Exception('Không có đại lý trong khu vực của bạn');
    }

    /**
     * @Author: ANH DUNG Oct 24, 2016
     * @Todo: transaction complete
     * @Resource: order/transactionComplete
     */
    public function actionTransactionComplete()
    {
        try {
//            GasCheck::randomSleep();// DungNT Close Jul2419
            $result = ApiModule::$defaultResponse;
            $this->checkRequest();
            $q = json_decode($_POST['q']);
//            $this->checkRequiredParams($q, array('token', 'session_id', 'session_key', 'google_address','transaction_type','first_name','phone','device_phone','note','email','agent_id','latitude','longitude','order_detail'));
            $this->checkRequiredParams($q, array('token', 'session_id', 'session_key', 'transaction_type','first_name','phone','device_phone','note','email','agent_id','latitude','longitude','order_detail'));
            $mUser = $this->getUserByToken($result, $q->token);

            $mTransaction = Transaction::getByIdAndKey($q, $mUser->id);
            if (is_null($mTransaction)) {
                $result['code'] = GasConst::E2247;
                $result['message'] = 'Thao tác đặt hàng không thành công. Vui lòng thử lại';
                ApiModule::sendResponse($result, $this);
            }
            $mTransaction->scenario         = 'AndroidCreate';
            $mTransaction->mAppUserLogin    = $mUser;
            $mTransaction->handlePost($q);

            if ($mTransaction->hasErrors()) {
                $result['message'] = HandleLabel::FortmatErrorsModel($mTransaction->getErrors());
                ApiModule::sendResponse($result, $this);
            }

            if (isset($q->review)) {
                $this->doOrderReview($q, $result, $mTransaction, $mUser);
            } else {
                $this->doOrderDone($q, $result, $mTransaction, $mUser);
            }

            ApiModule::sendResponse($result, $this);
        } catch (Exception $ex) {
            MyFormat::ApiCatchError($ex, $this);
        }
    }

    /** @Author: ANH DUNG Oct 20, 2017
     * @Todo: check user can create more one order, when demo game
     */
    public function canMakeMoreOrder($mUser)
    {
        $mTransactionHistory = new TransactionHistory();
        $mTransactionHistory->customer_id = $mUser->id;
        $countOrder = $mTransactionHistory->countOrderBycustomer();
        if ($countOrder) {
            $result = ApiModule::$defaultResponse;
            $result['message'] = 'Bạn đã đặt thành công đơn hàng, không thể đặt đơn hàng lần 2';
            ApiModule::sendResponse($result, $this);
        }
    }

    /** @Author: ANH DUNG Oct 17, 2017
     * @Todo: tạo Transaction History khi KH hoàn thành đặt hàng trên app
     */
    public function doOrderDone($q, &$result, &$mTransaction, $mUser)
    {
//        $this->canMakeMoreOrder($mUser);// for demo only
        // Make transaction
        $mTransaction->handleSave();
        $result = ApiModule::$defaultSuccessResponse;
        $result['message'] = 'View';

        // Confirm transaction
        $mTransaction->handlePostConfirm($q);
        if ($mTransaction->hasErrors()) {
            $result['message'] = HandleLabel::FortmatErrorsModel($mTransaction->getErrors());
            ApiModule::sendResponse($result, $this);
        }

        $mTransaction->applyPromotionConfirm();
//        [TASKTIMER]
        $mTransaction->applyGoldTime();
        
        $mTransaction->directionInput = Sell::SOURCE_APP;
        $mTransactionHistory = $mTransaction->copyToHistory();
        $mTransactionHistory->mAppUserLogin = $mUser;
        $mTransactionHistory->handleOrderSecond($q);
        $mTransaction->delete();
        $this->writeLogCustomerErrors($mTransactionHistory);
        
        $result['record'] = $mTransactionHistory->formatLatestTransaction();
        $result['record']['transaction_id'] = $mTransactionHistory->id;
    }

    /** @Author: ANH DUNG Oct 17, 2017
     * @Todo: move code review Order của Trung ra 1 function
     */
    public function doOrderReview($q, &$result, &$mTransaction, $mUser)
    {
        // Review only
        $mTransaction->applyPromotion();
        $mTransaction->handleBuildJsonDetail();
//                $mTransaction->applyPromotionConfirm();
        $mTransactionHistory = $mTransaction->copyToHistory(false);
        $mTransactionHistory->mAppUserLogin = $mUser;

        $result = ApiModule::$defaultSuccessResponse;
        $result['message'] = 'View';

        $result['record'] = $mTransactionHistory->formatLatestTransaction();
        $result['record']['transaction_id'] = $mTransactionHistory->id;
    }

    /**
     * @Author: Trung Sept 19, 2017
     * @Todo: transaction status
     * Get last transaction status
     * if existed transaction id, check both new and completed status
     * @Resource: order/transactionStatus
     */
    public function actionTransactionStatus()
    {
//        $this->returnNull();
        try {
//            GasCheck::randomSleep(); Aug1619 DungNT Close CPU high
            $result = ApiModule::$defaultResponse;
//            $this->checkRequest();
            $q = json_decode($_POST['q']);
            $this->checkRequiredParams($q, array('token', 'transaction_id'));
            $mUser = $this->getUserByToken($result, $q->token);

            $result = ApiModule::$defaultSuccessResponse;
            $result['message'] = 'View';
            if (!empty($q->transaction_id)) {
                $mTransactionHistory = TransactionHistory::getByCompleteTransactionId($q, $mUser->id);
            } else {
                $mTransactionHistory = TransactionHistory::getLastPendingTransaction($mUser->id);
            }
            if (is_null($mTransactionHistory)) {
                $result['record'] = null;
            } else {
                $mTransactionHistory->mAppUserLogin = $mUser;
                $result['record'] = $mTransactionHistory->formatLatestTransaction();
                $result['record']['transaction_id'] = $mTransactionHistory->id;
            }

            ApiModule::sendResponse($result, $this);
        } catch (Exception $ex) {
            MyFormat::ApiCatchError($ex, $this);
        }
    }
    
    /** @Author: DungNT Aug 16, 2019
     *  @Todo: tạm tắt action này đi
     **/
    public function returnNull() {
        $result = ApiModule::$defaultSuccessResponse;
        $result['record'] = null;
        ApiModule::sendResponse($result, $this);
    }

    /**
     * @Author: ANH DUNG Oct 24, 2016
     * @Todo: only for dev test error
     * @Resource: order/testErrorHtml
     */
    public function actionTestErrorHtml()
    {
        try {
            $result = ApiModule::$defaultResponse;
            $mTransaction = new GasOrders();
            $this->testErrorHtml();
            $result = ApiModule::$defaultSuccessResponse;
            ApiModule::sendResponse($result, $this);
        } catch (Exception $ex) {
            MyFormat::ApiCatchError($ex, $this);
        }
    }

    /**
     * @Author: ANH DUNG Oct 19, 2016
     */
    public function testErrorHtml()
    {
        $mTransaction = GasOrders::model()->findByPk(-1);;
        $mTransaction->id = 2321;
    }

    /**
     * @Author: ANH DUNG Now 05, 2016
     * @Todo:  Method: POST  get lastes transaction history of customer
     * @Resource: http://spj.daukhimiennam.com/api/order/transactionHistory
     * @ex_json_request: {"token":"dc36cc6fb2db91e515372db1821b6275"}
     * @from: android + ios platform
     */
    public function actionTransactionHistory()
    {
        try {
            $result = ApiModule::$defaultResponse;
            $this->checkRequest();
            $q = json_decode($_POST['q']);
            $this->checkRequiredParams($q, array('token'));
            $mUser = $this->getUserByToken($result, $q->token);// Oct 12, 2015 for check inactive or delete User
            $result = ApiModule::$defaultSuccessResponse;
            $result['message'] = 'Transaction history success';
            $mTransactionHistory = new TransactionHistory();
            $mTransactionHistory->customer_id = $mUser->id;
//        $mTransactionHistory->customer_id   = $_GET['id'];
            $result['record'] = $mTransactionHistory->getLastestHistory()->formatLatestTransaction();
            ApiModule::sendResponse($result, $this);
        } catch (Exception $ex) {
            MyFormat::ApiCatchError($ex, $this);
        }
    }

    /**
     * @Author: ANH DUNG Now 05, 2016
     * @Todo: view transaction history of customer
     * @Resource: http://spj.daukhimiennam.com/api/order/transactionView
     * @word_index:  Method: POST
     * @ex_json_request: {"token":"dc36cc6fb2db91e515372db1821b6275", "transaction_id":123}
     * @from: android + ios platform
     */
    public function actionTransactionView()
    {
        try {
            $result = ApiModule::$defaultResponse;
            $this->checkRequest();
            $q = json_decode($_POST['q']);
            $this->checkRequiredParams($q, array('token', 'transaction_history_id'));
            $mUser = $this->getUserByToken($result, $q->token);// Oct 12, 2015 for check inactive or delete User
            
            $mTransactionHistory = TransactionHistory::model()->findByPk($q->transaction_history_id);
            if (is_null($mTransactionHistory) || (!$mTransactionHistory->canViewOnApp($mUser))) {
                $idText = !is_null($mTransactionHistory) ? "TransId $mTransactionHistory->id" : 'EmptyId';
                $eMsg   = "$idText Đơn hàng yêu cầu không hợp lệ, hoặc khách hàng đã hủy App. User: $mUser->id";
                $result['message'] = $eMsg;
                ApiModule::sendResponse($result, $this);
            }
//        $mTransactionHistory = $mTransaction->copyToHistoryViewOnly();
            $result                             = ApiModule::$defaultSuccessResponse;
            $result['message']                  = 'Transaction view';
            $mTransactionHistory->mAppUserLogin = $mUser;
            $result['record'] = $mTransactionHistory->formatLatestTransaction();
            ApiModule::sendResponse($result, $this);
        } catch (Exception $ex) {
            MyFormat::ApiCatchError($ex, $this);
        }
    }

    /**
     * @Author: ANH DUNG Now 25, 2016
     * @Todo: view - Confirm Order
     * @Resource: http://spj.daukhimiennam.com/api/order/transactionViewConfirm
     * @word_index:  xử lý step cuối xác nhận đơn hàng
     * @ex_json_request: {"token":"dc36cc6fb2db91e515372db1821b6275", "transaction_id":123}
     * @from: android + ios platform
     */
    public function actionTransactionViewConfirm_RemoveJul1519DungNT()
    {
        try {
            $result = ApiModule::$defaultResponse;
            $this->checkRequest();
            $q = json_decode($_POST['q']);
            $this->checkRequiredParams($q, array('token', 'transaction_id'));
            $mUser = $this->getUserByToken($result, $q->token);// Oct 12, 2015 for check inactive or delete User


            $mTransaction = Transaction::model()->findByPk($q->transaction_id);
            if (is_null($mTransaction) || ($mTransaction->customer_id != $mUser->id)) {
                throw new Exception('Đơn hàng yêu cầu không hợp lệ');
            }
            $mTransaction->handlePostConfirm($q);
            if ($mTransaction->hasErrors()) {
                $result['message'] = HandleLabel::FortmatErrorsModel($mTransaction->getErrors());
                ApiModule::sendResponse($result, $this);
            }

            $mTransaction->applyPromotionConfirm();
            $mTransactionHistory = $mTransaction->copyToHistory();
            $mTransactionHistory->makeNotify();
            $mTransaction->delete();
            $result = ApiModule::$defaultSuccessResponse;
            $result['message'] = 'Đặt hàng thành công. Nhân viên của Gas24h sẽ liên lạc với bạn sớm nhất để xác nhận đơn hàng';
            $result['record'] = $mTransactionHistory->id;
            ApiModule::sendResponse($result, $this);
        } catch (Exception $ex) {
            MyFormat::ApiCatchError($ex, $this);
        }
    }
    
    /** @Author: ANH DUNG Dec 28, 2018
     *  @Todo: gửi mail với KH bị lỗi, cần theo dõi
     * Hiện tại có 1 KH lỗi nhập mã KM Gas24h 2 lần
     **/
    public function writeLogCustomerErrors($mTransactionHistory) {
        $aCustomerMonitor = [
            1756217, // 0917667377 - Phạm Văn Phước
//            1622560, // Admin test 0384331552
        ];
        if(!in_array($mTransactionHistory->customer_id, $aCustomerMonitor)){
            return ;
        }
        $mTransactionHistory = TransactionHistory::model()->findByPk($mTransactionHistory->id);
        SendEmail::bugToDev("Info Model: ". MyFormat::jsonEncode($mTransactionHistory->getAttributes()), ['title'=> "[App Gas24h big bug] Theo dõi Lỗi KH nhập mã Gas24h trên 1 lần"]);
    }

    /**
     * @Author: ANH DUNG Now 25, 2016
     * @Todo: KH hủy app Gas24h - view - Cancel Order
     * @Resource: http://spj.daukhimiennam.com/api/order/transactionViewCancel
     * @word_index:  Method: POST
     * @ex_json_request: {"token":"dc36cc6fb2db91e515372db1821b6275", "transaction_id":123}
     * @from: android + ios platform
     */
    public function actionTransactionViewCancel()
    {
        try {
            $result = ApiModule::$defaultResponse;
            $this->checkRequest();
            $q = json_decode($_POST['q']);
            $this->checkRequiredParams($q, array('token', 'transaction_id'));
            $mUser = $this->getUserByToken($result, $q->token);// Oct 12, 2015 for check inactive or delete User
            /** @var TransactionHistory $mTransactionHistory */
            $mTransactionHistory = TransactionHistory::model()->findByPk($q->transaction_id);
            if (is_null($mTransactionHistory) || ($mTransactionHistory->customer_id != $mUser->id) 
                || !empty($mTransactionHistory->employee_maintain_id) ) {
                $eMsg = 'Không thể hủy đơn hàng, nhân viên '.$mTransactionHistory->getUserInfo('rEmployeeMaintain', 'first_name').' đang giao đơn hàng này của bạn';
                $result['message'] = $eMsg;
                ApiModule::sendResponse($result, $this);
            }
            $result = ApiModule::$defaultSuccessResponse;
            $mTransactionHistory->saveCancelOrderByAppCustomer();
            $result['message'] = 'Hủy đặt hàng thành công';
            ApiModule::sendResponse($result, $this);
        } catch (Exception $ex) {
            MyFormat::ApiCatchError($ex, $this);
        }
    }

    /**
     * @Author: ANH DUNG Now 24, 2016
     * @Todo: android list order
     * @Resource: order/transactionList
     * @ex_json_request: {"token":"403acbe7932b3c7f2f3ce5a906757c29","page":"0"}
     */
    public function actionTransactionList()
    {
//        Logger::WriteLog("actionTransactionList Load debug url: ".$_SERVER['HTTP_HOST']);
        try {
            $result = ApiModule::$defaultResponse;
            $this->checkRequest();
            $q = json_decode($_POST['q']);
            $this->checkRequiredParams($q, array('token', 'page'));
            $mUser = $this->getUserByToken($result, $q->token);// Oct 12, 2015 for check inactive or delete User
            $this->mUserApp = $mUser;
            $this->checkVersionCode($q, $mUser);
            $this->checkRequiredLogout();
            if ($mUser->hasErrors()) {
                $result['message'] = HandleLabel::FortmatErrorsModel($mUser->getErrors());
                ApiModule::sendResponse($result, $this);
            }
            $result = ApiModule::$defaultSuccessResponse;
            $result['message'] = 'List';
            $mTransactionHistory = new TransactionHistory();
            $mTransactionHistory->mAppUserLogin = $mUser;
            $mTransactionHistory->handleApiList($result, $q);
            ApiModule::sendResponse($result, $this);
        } catch (Exception $ex) {
            MyFormat::ApiCatchError($ex, $this);
        }
    }

    /**
     * @Author: ANH DUNG Dec 13, 2016
     * @Todo: android list order for GN
     * @Resource: order/transactionSetEvent
     * @ex_json_request: {"token":"403acbe7932b3c7f2f3ce5a906757c29","transaction_history_id":"0", "action_type":1,
     * "latitude":132485,"longitude":4546, "change_type":1, "status_cancel":2}
     * $q->latitude.",$q->longitude"; status_cancel (1: sai thông tin, 2: giá cao, 3: sai quà, 4: giao chậm, 5: khác), "order_detail"=>array
     * có 3 action sử dụng api này:
     * 1. GN nhận ĐH
     * 2. GN hủy nhận ĐH
     * 3. GN hủy KH không lấy gas
     * 4: KH hủy không lấy gas
     */
    public function actionTransactionSetEvent()
    {
        try {
            $result = ApiModule::$defaultResponse;
            $this->checkRequest();
            $q = json_decode($_POST['q']);
            $this->checkRequiredParams($q, array('token', 'transaction_history_id', 'action_type', 'status_cancel', 'latitude', 'longitude', 'order_detail'));
            $this->validateLocationGps($q);
            $mUser = $this->getUserByToken($result, $q->token);// Oct 12, 2015 for check inactive or delete User
            $mTransactionHistory = TransactionHistory::model()->findByPk($q->transaction_history_id);
            $mTransactionHistory->promotion_amount_old = $mTransactionHistory->promotion_amount;

            $aAgent = Users::GetKeyInfo($mUser, UsersExtend::JSON_ARRAY_AGENT);
            if (is_null($mTransactionHistory) || ($mUser->role_id == ROLE_EMPLOYEE_MAINTAIN && is_array($aAgent) && !in_array($mTransactionHistory->agent_id, $aAgent))) {
                throw new Exception('Đơn hàng yêu cầu không hợp lệ. Kiểm tra lại đại lý hiện tại của bạn hoặc làm mới danh sách đơn hàng. User: '.$mUser->id);
            }// Sep1217 chỗ này đang giới hạn với ROLE_EMPLOYEE_MAINTAIN, còn các role khác thì allow all
            
            $mTransactionHistory->apiValidateFile();
            if ($mTransactionHistory->hasErrors()) {
                $result['message'] = HandleLabel::FortmatErrorsModel($mTransactionHistory->getErrors());
                ApiModule::sendResponse($result, $this);
            }
            
            $mTransactionHistory->mAppUserLogin = $mUser;
            if ($mTransactionHistory->changeAgentOrder($q)) {
                $result = ApiModule::$defaultSuccessResponse;
                ApiModule::sendResponse($result, $this);
            }
            
            $mTransactionHistory->handleSetEvent($q, $mUser);
            if ($mTransactionHistory->hasErrors()) {
                $result['message'] = HandleLabel::FortmatErrorsModel($mTransactionHistory->getErrors());
                ApiModule::sendResponse($result, $this);
            }
            
            $result = ApiModule::$defaultSuccessResponse;
            $result['message'] = 'ok';
            $result['record'] = $mTransactionHistory->formatLatestTransaction();
            ApiModule::sendResponse($result, $this);
        } catch (Exception $ex) {
            MyFormat::ApiCatchError($ex, $this);
        }
    }

    /**
     * @Author: ANH DUNG Feb 05, 2016
     * @Todo: KH rating comment đơn hàng Hộ GĐ
     * @Resource: order/transactionRating
     * @ex_json_request: {"token":"4ab6006c08d430ba12ce16aef0352420","id":"47","rating":"3","rating_comment":"test comment"}
     */
    public function actionTransactionRating()
    {
        try {
            $result = ApiModule::$defaultResponse;
            $this->checkRequest();
            $q = json_decode($_POST['q']);
            $this->checkRequiredParams($q, array('token', 'id'));
            $mUser = $this->getUserByToken($result, $q->token);// Oct 12, 2015 for check inactive or delete User

            $mTransactionHistory = TransactionHistory::model()->findByPk($q->id);
            if (is_null($mTransactionHistory) || ($mTransactionHistory->customer_id != $mUser->id)) {
                throw new Exception('Đơn hàng yêu cầu không hợp lệ');
            }

            $mTransactionHistory->handleSetRating($q, $mUser);
            if ($mTransactionHistory->hasErrors()) {
                $result['message'] = HandleLabel::FortmatErrorsModel($mTransactionHistory->getErrors());
                ApiModule::sendResponse($result, $this);
            }

            $result = ApiModule::$defaultSuccessResponse;
            $result['message'] = 'Gửi đánh giá thành công';
            ApiModule::sendResponse($result, $this);
        } catch (Exception $ex) {
            MyFormat::ApiCatchError($ex, $this);
        }
    }

    /**
     * @Author: ANH DUNG Feb 05, 2017
     * @Todo: APP Gas24h check required (version_code) user update app to new version
     * @Resource: order/checkVersionCodeGas24h
     * @ex_json_request: {"version_code":"113"}
     */
    public function actionCheckVersionCodeGas24h()
    {
        try {
            $result = ApiModule::$defaultResponse;
            $this->checkRequest();
            $q = json_decode($_POST['q']);
            $this->checkRequiredParams($q, array('version_code'));
            $mSignup = new AppSignup();// model temp, không có ý nghĩa gì
            $this->checkVersionCodeGas24h($q, $mSignup);
            if ($mSignup->hasErrors()) {
                $result['message'] = HandleLabel::FortmatErrorsModel($mSignup->getErrors());
                ApiModule::sendResponse($result, $this);
            }
            $result = ApiModule::$defaultSuccessResponse;
            $result['message'] = '';
            ApiModule::sendResponse($result, $this);
        } catch (Exception $ex) {
            MyFormat::ApiCatchError($ex, $this);
        }
    }

    /**
     * @Author: ANH DUNG Dec 13, 2017
     * @Todo: tự động tạo đơn hộ GĐ KH trả thẻ
     * @Resource: order/transactionCreate
     * @ex_json_request: Method: POST {"token":"8d78315f96964464495d4ba4b3a9c7a3"}
     * @from: android + ios platform
     */
    public function actionTransactionCreate()
    {
        try {
            throw new Exception('Chức năng này bị tắt, vui lòng tạo ticket gửi thông tin đầy đủ địa chỉ KH để Ngọc hỗ trợ trả thẻ');
            $result = ApiModule::$defaultResponse;
            $this->checkRequest();
            $q = json_decode($_POST['q']);
            $this->checkRequiredParams($q, array('token'));
            $mUser = $this->getUserByToken($result, $q->token);// Oct 12, 2015 for check inactive or delete User
            $agent_id = $this->getAgentOfEmployee($mUser);
            $mTransactionHistory = new TransactionHistory();
            if (!in_array($mUser->role_id, $mTransactionHistory->getRoleCanHandleHgd())) {
                throw new Exception('Bạn không có quyền hoặc yêu cầu không hợp lệ');
            }

            $result = ApiModule::$defaultSuccessResponse;
            $mTransactionHistory->agent_id = $agent_id;
            $mTransactionHistory->mAppUserLogin = $mUser;
            $mTransactionNew = $mTransactionHistory->autoCreateOrder();
            $result['record'] = $mTransactionNew->id;
            ApiModule::sendResponse($result, $this);
        } catch (Exception $ex) {
            MyFormat::ApiCatchError($ex, $this);
        }
    }
    public function backupGetAgentNearest(){
        try {
            $q = json_decode($_POST['q']);
            $this->checkRequiredParams($q, array('latitude', 'longitude', 'app_type'));
            $result = ApiModule::$defaultSuccessResponse;
            $result['message']  = 'ok';
            $result['record']   = null;
            $_GET['app_type'] = isset($q->app_type) ? $q->app_type : 0;
            $_GET['agent_id'] = 100;
            $mAppOrder  = new AppOrder();
            $aData      = $mAppOrder->handleGetConfig();

            $earthRadius = 6371000;
            $maxRange = 3000; // 3km
            $maxRadiusRange = $maxRange / ($earthRadius * M_PI) * 180;
            $currentMinRadiusRange = $maxRadiusRange;


            // Filter agent in range
            if ($q->latitude != 0 && $q->longitude != 0) {
                foreach ($aData as $data) {
                    $currentAgentLat = $data['info_agent']['agent_latitude'];
                    $currentAgentLong = $data['info_agent']['agent_longitude'];
                    $distanceX = abs($q->latitude - $currentAgentLat);
                    $distanceY = abs($q->longitude - $currentAgentLong);
                    if ($distanceX < $maxRadiusRange && $distanceY < $maxRadiusRange) {
                        $distanceXY = sqrt(pow($distanceX, 2) + pow($distanceY, 2));
                        if ($currentMinRadiusRange >= $distanceXY) {
                            $currentMinRadiusRange = $distanceXY;
                            $result['record'] = $data;
                        }
                    }
                }
            }
            ApiModule::sendResponse($result, $this);
        } catch (Exception $ex) {
            MyFormat::ApiCatchError($ex, $this);
        }
    }

    /**
     * @Author: ANH DUNG Feb 23, 2018
     * @Todo: APP Gas24h find Agent nearest with location of user
     * @Resource: order/getAgentNearest
     * @ex_json_request: Method: POST {"app_type":1,"version_code":"80510","platform":1,"token":"82d0e98ec9934de624cf40e1ad4bdddb","latitude":"10.8310823","longitude":"106.6862171","username":"01684331552"}
     */
    public function actionGetAgentNearest()
    {
        try {
            $result = ApiModule::$defaultResponse;
            $this->checkRequest();
//            Logger::WriteLog($_POST['q']);// dev test request
            $q = json_decode($_POST['q']);
            $this->checkRequiredParams($q, array('latitude', 'longitude', 'app_type'));
            $mUser = $this->getUserByToken($result, $q->token);// Oct 12, 2015 for check inactive or delete User
            $this->checkVersionCodeGas24h($q, $mUser);
            $mAppOrder          = new AppOrder();
            $mapLib             = new MapLib();
            $mapLib->username   = isset($q->username) ? $q->username : '';
            $agent_id           = $mapLib->gas24hGetAgentNearest($q->latitude, $q->longitude);
//            $agent_id = 658920;// BT2 for test
            if(empty($agent_id) || in_array($agent_id, $mAppOrder->getListAgentLockApp())){
                $result['message']  = 'Không tìm thấy đại lý quanh vị trí của bạn';
                $result['record']   = null;
                ApiModule::sendResponse($result, $this);
            }
            $this->setAgentNewUserGas24h($mUser, $agent_id, $q);
            $_GET['app_type']       = isset($q->app_type) ? $q->app_type : 0;
            $_GET['agent_id']       = $agent_id;
            
            $aData                  = $mAppOrder->handleGetConfig();
            if(isset($aData[0]['info_gas']) && count($aData[0]['info_gas']) < 1){
//                SendEmail::bugToDev("App Gas24h actionGetAgentNearest lỗi đại lý bị empty tồn kho, fix gấp ". $aData[0]['info_agent']['agent_name_spj']);
                $eMsg = $aData[0]['info_agent']['agent_name_spj']." đang cập nhật dữ liệu, không thể đặt Gas. Chi tiết liên hệ 19001565";
                $result['message']  = $eMsg;
                $result['record']   = null;
                ApiModule::sendResponse($result, $this);
            }
            if (0) { // nếu record 0, không có đại lý phù hợp - Tracking lại để phân tích
                // write log những tọa độ không tìm được đại lý, tính  toán luôn khoảng cách của tọa độ đó đến đại lý gần nhất để xử lý
            }
            $result                 = ApiModule::$defaultSuccessResponse;
            $result['record']       = isset($aData[0]) ? $aData[0] : null;
            $result['message']      = 'ok';
            ApiModule::sendResponse($result, $this);
        } catch (Exception $ex) {
            MyFormat::ApiCatchError($ex, $this);
        }
    }
    
    /** @Author: ANH DUNG Apr 01, 2018
     *  @Todo: set đại lý gần nhất cho user mới login app Gas24h
     **/
    public function setAgentNewUserGas24h($mUser, $agent_id, $q) {
        if(!empty($mUser->area_code_id)){
            return ;
        }
        $mUser->area_code_id    = $agent_id;
        $mUser->province_id     = MyFormat::convertAgentToProvinceId($agent_id);
        $mUser->slug            = "$q->latitude,$q->longitude";// set tọa độ cho KH
        $mUser->update(['area_code_id', 'province_id', 'slug']);
    }
    
    /** @Author: NamNH Oct 02,2018
     *  @Todo: Hẹn giờ tự động tạo hoá đơn cho khách hàng
     *  @Resource: Order/SetTimer
     *  @ex_json_request: {"token":"951373dece382e77c88ff88a15f5c329","platform":"2","days":"3","hour":"10","latitude":"10.796551","longitude":"106.7053714"}
     **/
    public function actionSetTimer(){
        try{
            $result                     = ApiModule::$defaultResponse;
            $mTransaction               = new Transaction();
            $aParamNew                  = $mTransaction->getAppStatusNew();
            $this->checkRequest();
            $q                          = json_decode($_POST['q']);
            $this->checkRequiredParams($q, array('token','days','hour','platform'));
            $mUser                      = $this->getUserByToken($result, $q->token);
            $mForecast  = new Forecast();
            if(!$mForecast->canUseForecast($mUser)){
                $result['message']      = 'Chức năng này đang cập nhật, vui lòng quay lại sau';
                ApiModule::sendResponse($result, $this);
            }
//            check days hour
            $this->checkParams($q);
//            update timer
            $this->checkUpdateTimer($result,$q);
//            Check isset sell
            /* Jan2419 DungNT close !empty($newSell) 
            $MoreCondition              = [
                'status'  =>   [Sell::STATUS_NEW],
                'is_timer'  =>  Forecast::TYPE_IMMEDIATE,
            ];
            $newSell                   = $this->getSell($mUser->id, $MoreCondition);
            */
            $MoreCondition              = [
                'is_timer'  =>  Forecast::TYPE_IMMEDIATE,
                'status'    =>   $aParamNew,
            ];
            $newTransaction                   = $this->getTransaction($mUser->id,$MoreCondition);
            if(!empty($newTransaction)){
                $result['message']      = 'Tài khoản đang có đơn hàng giao ngay không thể cập nhật lịch hẹn.';
                ApiModule::sendResponse($result, $this);
            }
//            convert to obj
            $mTransaction               = $this->createTransaction($mUser,$q);
            $qFake                      = $this->getDetailOldSell($mUser,$q, $mTransaction);
            $mTransaction->mAppUserLogin= $mUser;
            $mTransaction->scenario     = 'AndroidCreate';
            $mTransaction->handlePost($qFake);
            if ($mTransaction->hasErrors()) {
                $result['message'] = HandleLabel::FortmatErrorsModel($mTransaction->getErrors());
                ApiModule::sendResponse($result, $this);
            }
//            time
            $this->setSellTimer($mTransaction, $q);
//            create sell
            $this->doOrderDone($qFake, $result, $mTransaction, $mUser);
//            get sell timer
            
            $MoreCondition              = [
                    'is_timer'  =>  Forecast::TYPE_TIMER,
                    'status'    =>   $aParamNew,
            ];
            $newTransaction                 = $this->getTransaction($mUser->id,$MoreCondition);
            if(empty($newTransaction)){
                $result                     = ApiModule::$defaultResponse;
                $result['message']          = 'Thao tác đặt hàng không thành công. Vui lòng thử lại';
                ApiModule::sendResponse($result, $this);
            }
            $result                 = ApiModule::$defaultSuccessResponse;
            $result['message']      = 'Đơn hàng đã được gửi';
            $result['record']       = $newTransaction->apiTimerGetRecord();
            ApiModule::sendResponse($result, $this);
        } catch (Exception $ex) {
            MyFormat::ApiCatchError($ex, $this);
        }
    }
    
    /** @Author: NamNH Oct 31, 2018
     *  @Todo: set $q order from order old
     **/
    public function getDetailOldSell($mUser,$q, &$mTransaction){
//            create new sell
        /*
        $MoreCondition              = [
            'status'  =>   [Sell::STATUS_PAID],
        ];
        $lastSell                   = $this->getSell($mUser->id,$MoreCondition);
         */
        $mTransactionHistory                = new TransactionHistory();
        $mTransactionHistory->customer_id   = $mUser->id;
        $mTransactionHistoryOld             = $mTransactionHistory->getLastestHistory();
        if(empty($mTransactionHistoryOld)){
            $mTransaction->isFirstOrder = true;
            return $this->buildParamFirstTimer($mUser, $q);// Jan2419 DungNT fix allow first order make Order Timer
//            $result['message']      = 'Chức năng gas hẹn giờ chỉ áp dụng cho khách hàng đã từng đặt gas.';
//            ApiModule::sendResponse($result, $this);
        }
    //            set array detail and save
        $qFake = [];
        $qFake['google_address']    = isset($q->google_address) ? $q->google_address : '';
        $qFake['order_detail']      = [];
        
        $aDetail    = json_decode($mTransactionHistoryOld->json_detail, true);
        if(!is_array($aDetail)){
            SendEmail::bugToDev("Check function getDetailOldSell()  empty detail cua mTransactionHistoryOld");
            throw new Exception('Đơn hàng không hợp lệ, vui lòng khởi động lại ứng dụng và đặt Gas lại');
        }
        
        // Jan2419 DungNT change: only get data from $mTransactionHistoryOld
        foreach($aDetail as $detail):
            if($detail['materials_type_id'] != GasMaterialsType::MATERIAL_BINH_12KG){
                continue ;
            }
            $obj                                        = [];
            $obj['materials_id']                        = $detail['materials_id'];
            $obj['materials_type_id']                   = $detail['materials_type_id'];
            $obj['seri']                                = '';
            $obj['qty']                                 = 1;
            $obj['price']                               = $detail['price_root'];
            $obj['kg_empty']                            = 0;
            $obj['kg_has_gas']                          = 0;
            $obj                                        = (object)$obj;
            $qFake['order_detail'][]                    = $obj;
        endforeach;
        
//        if(!empty($lastSell->rDetail)){ DungNT Jan2419 close
//            foreach ($lastSell->rDetail as $key => $mDetail){
//                if($mDetail->materials_type_id == GasMaterialsType::MATERIAL_BINH_12KG){
//                    $obj                                        = [];
//                    $obj['materials_id']                        = $mDetail->materials_id;
//                    $obj['materials_type_id']                   = $mDetail->materials_type_id;
//                    $obj['seri']                                = $mDetail->seri;
//                    $obj['qty']                                 = $mDetail->qty;
//                    $obj['price']                               = $mDetail->price;
//                    $obj['kg_empty']                            = $mDetail->kg_empty;
//                    $obj['kg_has_gas']                          = $mDetail->kg_has_gas;
//                    $obj                                        = (object)$obj;
//                    $qFake['order_detail'][]                    = $obj;
//                }
//            }
//        }
//            set old field
        $qFake['transaction_type']          = Transaction::STEP_NORMAL;
        $qFake['first_name']                = $mUser->first_name;
        $qFake['phone']                     = $mUser->phone;
        $qFake['email']                     = $mUser->email;
        $qFake['device_phone']              = $mTransactionHistoryOld->device_phone;
        $qFake['agent_id']                  = $mTransactionHistoryOld->agent_id;
        $qFake['note']                      = '';
        $aNewGoogle                         = explode(',', $mTransactionHistoryOld->google_map, 2);
        $qFake['latitude']                  = isset($aNewGoogle[0]) ? $aNewGoogle[0] : '';
        $qFake['longitude']                 = isset($aNewGoogle[1]) ? $aNewGoogle[1] : '';
        $qFake['google_address']            = $mTransactionHistoryOld->google_address;
        return (object)$qFake;
    }
    
    /** @Author: DungNT Jan 22, 2019
     *  @Todo: build param for first order timer of new customer, cho phép cả KH chưa đặt lần nào dùng function hẹn giờ
     **/
    public function buildParamFirstTimer($mUser,$q) {
        //            set array detail and save
        $qFake = [];
        $qFake['google_address']    = isset($q->google_address) ? $q->google_address : '';
        $qFake['order_detail']      = [];
//            set old field
        $qFake['transaction_type']          = Transaction::STEP_NORMAL;
        $qFake['first_name']                = $mUser->first_name;
        $qFake['phone']                     = $mUser->phone;
        $qFake['email']                     = $mUser->email;
        $qFake['device_phone']              = $mUser->phone;
        $qFake['agent_id']                  = $mUser->area_code_id;
        $qFake['note']                      = '';
        $qFake['latitude']                  = '';
        $qFake['longitude']                 = '';
        $qFake['google_address']            = '';
        return (object)$qFake;
    }
    
    /** @Author: NamNH Oct 02,2018
     *  @Todo: Hẹn giờ tự động tạo hoá đơn cho khách hàng
     *  @Resource: Order/DeleteTimer
     *  @ex_json_request: {"token":"951373dece382e77c88ff88a15f5c329","id":"357"}
     **/
    public function actionDeleteTimer(){
        try{
            $result                     = ApiModule::$defaultResponse;
            $mTransaction   = new Transaction();
            $aParamNew         = $mTransaction->getAppStatusNew();
            $this->checkRequest();
            $q                          = json_decode($_POST['q']);
            $this->checkRequiredParams($q, array('token','id'));
            $mUser                      = $this->getUserByToken($result, $q->token);
            $mForecast  = new Forecast();
            if(!$mForecast->canUseForecast($mUser)){
                $result['message']      = 'Chức năng không áp dụng cho tài khoản này';
                ApiModule::sendResponse($result, $this);
            }
            $MoreCondition              = [
                'status'  =>   $aParamNew,
//              'timer'   =>  true,
               'id'     =>  $q->id,
            ];
            $mTranSaction                      = $this->getTransaction($mUser->id,$MoreCondition);
            if(!isset($mTranSaction)){
                $result['code'] = GasConst::E2247;
                $result['message']      = 'Không tìm thấy dữ liệu';
                if(!empty($this->checkExistsNewOrder($mUser))){
                    $result['message']      = 'Đã cập nhật lại lịch hẹn cho đơn hàng';
                }
                ApiModule::sendResponse($result, $this);
            }
            if($this->checkEmployeeMaintain($mTranSaction,'employee_maintain_id')){
                $result['message']      = 'Đơn hàng đã xác nhận không được huỷ';
                ApiModule::sendResponse($result, $this);
            }
            if(isset($mTranSaction)){
                $mTranSaction->deleteNotify();
                /* AnhDung Close Now0818 
                $mSellDelete = Sell::model()->findByPk($mTranSaction->sell_id);
                if(!empty($mSellDelete)){
                    $mSellDelete->delete();
                }
                $mTranSaction->delete();
                 */
                $mTranSaction->saveCancelOrderByAppCustomer(); // AnhDung Add Now0818 
            }
            $result                 = ApiModule::$defaultSuccessResponse;
            $result['message']      = 'Huỷ đơn hàng thành công';
            ApiModule::sendResponse($result, $this);
        } catch (Exception $ex) {
            MyFormat::ApiCatchError($ex, $this);
        }
    }
    
    
    /** @Author: NamNH Oct 02, 2018
     *  @Todo: get last order and set attributes new order
     **/
    public function getSell($idCustomer,$aMoreCondition = []){
        $criteria           = new CDbCriteria;
        $criteria->compare('t.customer_id',$idCustomer);
        $criteria->order    = 't.id DESC';
        if(isset($aMoreCondition['status'])){
            $criteria->addInCondition('t.status', $aMoreCondition['status']);
        }
        if(isset($aMoreCondition['timer'])){
            $criteria->addCondition('t.delivery_timer IS NOT NULL');
        }
        if(isset($aMoreCondition['is_timer'])){
            $criteria->compare('t.is_timer',$aMoreCondition['is_timer']);
        }
        if(isset($aMoreCondition['id'])){
            $criteria->compare('t.id', $aMoreCondition['id']);
        }
        $mSell              = Sell::model()->find($criteria);
        return $mSell;
    }
    
    /** @Author: NamNH Oct 03, 2018
     *  @Todo: check update
     **/
    public function checkUpdateTimer(&$result,$q){
        $mUser                      = $this->getUserByToken($result, $q->token);
        $mTransaction               = new Transaction();
        $aParamNew                  = $mTransaction->getAppStatusNew();
        /* Jan2419 DungNT khong nen find $mSell kieu nay, co nhieu Sell status new gay ra bug
        $MoreCondition              = [
          'status'  =>  [Sell::STATUS_NEW],
          'timer'   =>  true,
        ];
        $mSell                      = $this->getSell($mUser->id, $MoreCondition); 
         */
        $MoreCondition              = [
                'is_timer'  =>  Forecast::TYPE_TIMER,
                'status'  =>   $aParamNew,
        ];
        $mNewTransactionHistory         = $this->getTransaction($mUser->id, $MoreCondition);
        if(empty($mNewTransactionHistory)){
            return;
        }
        $mSell                      = Sell::model()->findByPk($mNewTransactionHistory->sell_id);
        if($this->checkEmployeeMaintain($mNewTransactionHistory, 'employee_maintain_id')){
            $result['message']      = 'Đơn hàng đã xác nhận không được cập nhật';
            ApiModule::sendResponse($result, $this);
        }

        if(!empty($mSell)){
            $mSell->delete();
        }
        if(!empty($mNewTransactionHistory)){
            // remove GasScheduleNotify
            $mNewTransactionHistory->deleteNotify();
            $mNewTransactionHistory->delete();
        }
    }
    
    /** @Author: NamNH Oct 03, 2018
     *  @Todo: check update
     **/
    public function checkParams($q){
//        'days','hour'
        if($q->days < 0 || $q->hour < 0 || $q->hour > 23){
            $result                     = ApiModule::$defaultResponse;
            $result['code'] = GasConst::E2247;
            $result['message'] = 'Ngày phải lớn hơn hoặc bằng 0 và phút phải nằm trong khoảng 0-23';
            ApiModule::sendResponse($result, $this);
        }
        $isFail     = false;
        if($q->days < 0){
            $isFail = true;
        }
        $hourNow    = (int)date('H');
        $minutes    = (int)date('i');
        if($q->days <= 0){
            if($q->hour <= $hourNow + 1){
                $isFail = true;
            }
        }
        if($isFail){
            $result                     = ApiModule::$defaultResponse;
            $result['code'] = GasConst::E2247;
            $result['message'] = 'Thời gian giao phải lớn hơn thời điểm hiện tại ít nhất 1 giờ';
            ApiModule::sendResponse($result, $this);
        }
        
    }
    
    /** @Author: NamNH Oct 03, 2018
     *  @Todo: set timer of sell
     **/
    public function setSellTimer(&$mTransaction,$q){
        $day_timer_only                     = MyFormat::addDays(date('Y-m-d'), $q->days);
        $day_timer                          = $day_timer_only . ' ' . $q->hour.':00:00';
        $mTransaction->delivery_timer       = $day_timer;
        $mTransaction->is_timer             = Forecast::TYPE_TIMER;
        $mTransaction->created_date_only    = $day_timer_only;
    }
    
    /** @Author: NamNH Oct 03, 2018
     *  @Todo: create transaction for timer
     **/
    public function createTransaction($mUser,$q){
        $mTransaction = new Transaction();
        $mTransaction->customer_id      = $mUser->id;
        $mTransaction->genNewTransactionKey();
        $mTransaction->type_customer    = $mUser->is_maintain;
        $mTransaction->platform         = $q->platform;
        $mTransaction->save();
        return $mTransaction;
    }
    
    /** @Author: NamNH Oct 02, 2018
     *  @Todo: get last transaction and set attributes new transaction
     **/
    public function getTransaction($idCustomer,$aMoreCondition = []){
        $criteria           = new CDbCriteria;
        $criteria->compare('t.customer_id',$idCustomer);
        $criteria->order    = 't.id DESC';
        if(isset($aMoreCondition['status'])){
            $criteria->addInCondition('t.status', $aMoreCondition['status']);
        }
        if(isset($aMoreCondition['is_timer'])){
            $criteria->compare('t.is_timer',$aMoreCondition['is_timer']);
        }
        if(isset($aMoreCondition['id'])){
            $criteria->compare('t.id', $aMoreCondition['id']);
        }
        if(isset($aMoreCondition['not_in_status'])){
            $criteria->addNotInCondition('t.status', $aMoreCondition['not_in_status']);
        }
        $mSell              = TransactionHistory::model()->find($criteria);
        return $mSell;
    }
    
    /** @Author: NamNH Oct 12, 2018
     *  @Todo: check employee
     **/
    public function checkEmployeeMaintain($model,$field){
        $result = false;
        if(isset($model->$field)){
            if($model->$field > 0){
                $result =  true;
            }
        }
        return $result;
    }
    
    /** @Author: NamNH Oct 30, 2018
     *  @Todo: Check exists new order
     **/
    public function checkExistsNewOrder($mUser){
        $mTransaction   = new Transaction();
        $aParamNew         = $mTransaction->getAppStatusNew();
        $MoreCondition              = [
                'status'  =>   $aParamNew,
        ];
        return $this->getTransaction($mUser->id,$MoreCondition); 
    }
}
