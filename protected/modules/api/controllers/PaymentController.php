<?php

class PaymentController extends ApiController
{
    const PAYMENT_CODE_SUCCESS      = 200; // For successful payment
    const PAYMENT_CODE_INVALID      = 404; // For invalid payment
    const PAYMENT_CODE_NOT_MATCH    = 403; // For not match info
    public static $PAYMENT_MESSAGES = [
        PaymentController::PAYMENT_CODE_SUCCESS => 'Transaction successful',
        PaymentController::PAYMENT_CODE_INVALID => 'Transaction invalid',
        PaymentController::PAYMENT_CODE_NOT_MATCH => 'Not macth data'
    ];

    /** @Author: TrungNguyen Nov 02, 2018
     * Callback when payment sucess from external bank
     * Need json data:
     * {
            "processTime" : 1541142647340,  # Time in milisecond (UTC)
            "refId" : "123456789",          # Referral id from SPJ
            "bankRefId" : "123456789",      # Referral id from bank
            "amount" : 300000               # Total amount for payment
        }
     **/
    public function actionPrepaid()
    {
        $result = [
            'code'          => PaymentController::PAYMENT_CODE_INVALID,
            'message'      => PaymentController::$PAYMENT_MESSAGES[PaymentController::PAYMENT_CODE_INVALID]
        ];
        # Get as an object
        $json_str = file_get_contents('php://input');
        $json_obj = json_decode($json_str);
        Logger::WriteLog('API OCB callback', $level='info',$category='anhdung');
        if(!is_null($json_obj)){
            $aCallback = get_object_vars($json_obj);
//            Logger::WriteLog('API OCB amount :'.$aCallback['amount'], $level='info',$category='anhdung');
            $mLogPayment = new LogPayment();
            $mLogPayment->process_time = isset($aCallback['processTime']) ? $aCallback['processTime'] : '';
            $mLogPayment->ref_id = isset($aCallback['refId']) ? $aCallback['refId'] : '';// transaction id
            $mLogPayment->bank_ref_Id = isset($aCallback['bankRefId']) ? $aCallback['bankRefId'] : '';
            $mLogPayment->amount = isset($aCallback['amount']) ? $aCallback['amount'] : '';
            $mLogPayment->json = json_encode($aCallback);
            $mLogPayment->save();
            $result = [
                'code'      => PaymentController::PAYMENT_CODE_SUCCESS,
                'message'  => PaymentController::$PAYMENT_MESSAGES[PaymentController::PAYMENT_CODE_SUCCESS]
            ];
        Logger::WriteLog('API OCB success ', $level='info',$category='anhdung');
        }else{
            Logger::WriteLog('API OCB callback api/payment/prepaid in null value', $level='info',$category='anhdung');
        }
        # Check valid data from json
        ApiModule::sendResponse($result, $this);
    }
}
