<?php
/**
 * @Author: ANH DUNG Apr 14, 2017
 * @Todo: employee action
 */
class EmployeeController extends ApiController
{
    /**
     * @Author: ANH DUNG Apr 14, 2017
     * @Todo: Employee list Uphold HGD
     * @Resource: employee/upholdHgdList
     * @ex_json_request: {"token":"403acbe7932b3c7f2f3ce5a906757c29","page":"0"}
     */
    public function actionUpholdHgdList()
    {
        try{
            $result = ApiModule::$defaultResponse;
            $this->checkRequest();
            $q = json_decode($_POST['q']);
            $this->checkRequiredParams($q, array('token', 'page'));
            $mUser = $this->getUserByToken($result, $q->token);// Oct 12, 2015 for check inactive or delete User
/*            for test
            $temp = '{"token":"403acbe7932b3c7f2f3ce5a906757c29","page":"0"}';
            $q = json_decode($temp);
            $mUser = Users::model()->findByPk(121257);
 */
            $result = ApiModule::$defaultSuccessResponse;
            $result['message'] = 'L';
            $mUpholdHgd = new UpholdHgd();
            $mUpholdHgd->mAppUserLogin = $mUser;
            $mUpholdHgd->handleApiList($result, $q);
            ApiModule::sendResponse($result, $this);
        } catch (Exception $ex) {
            MyFormat::ApiCatchError($ex, $this);
        }
    }

    /**
     * @Author: ANH DUNG Apr 14, 2017
     * @Todo: Employee list Uphold HGD
     * @Resource: employee/upholdHgdView
     * @ex_json_request: {"token":"403acbe7932b3c7f2f3ce5a906757c29","id":"1"}
     */
    public function actionUpholdHgdView()
    {
        try{
            $result = ApiModule::$defaultResponse;
            $this->checkRequest();
            $q = json_decode($_POST['q']);
            $this->checkRequiredParams($q, array('token', 'id'));
            $mUser      = $this->getUserByToken($result, $q->token);
            $mUpholdHgd = UpholdHgd::model()->findByPk($q->id);
            if(is_null($mUpholdHgd)){
                throw new Exception('Yêu cầu bảo trì không hợp lệ');
            }
//            $mAppOrder = GasAppOrder::model()->findByPk(1);// only dev test
            $mUpholdHgd->mAppUserLogin = $mUser;
            $result             = ApiModule::$defaultSuccessResponse;
            $result['message']  = 'V';
            $result['record']   = $mUpholdHgd->appView();
            ApiModule::sendResponse($result, $this);
        } catch (Exception $ex) {
            MyFormat::ApiCatchError($ex, $this);
        }
    }
    
    /** 
     * @Author: ANH DUNG Apr 14, 2017
     * @Todo: android action Nhận Hủy Uphold HGD của NVGN
     * @Resource: employee/upholdHgdSetEvent
     * @ex_json_request: {"token":"338591ae323ed479cee4d64e761682b3","id":"2", "action_type":1,"latitude":"","longitude":"","note_employee":"ghi chu nV"}
     * có 2 action sử dụng api này:
     * 1. GN nhận ĐH
     * 2. GN hủy nhận ĐH
     */    
    public function actionUpholdHgdSetEvent()
    {
        try{
        $result = ApiModule::$defaultResponse;
        $this->checkRequest();
        $q = json_decode($_POST['q']);
        $this->checkRequiredParams($q, array('token', 'id','action_type', 'latitude', 'longitude'));
        $mUser = $this->getUserByToken($result, $q->token);// Oct 12, 2015 for check inactive or delete User
        
        $mUpholdHgd = UpholdHgd::model()->findByPk($q->id);
        if(is_null($mUpholdHgd)){
            throw new Exception('Yêu cầu bảo trì không hợp lệ');
        }
        $mUpholdHgd->mAppUserLogin = $mUser;
        $mUpholdHgd->handleSetEvent($q);
        if($mUpholdHgd->hasErrors()){
            $result['message'] = HandleLabel::FortmatErrorsModel($mUpholdHgd->getErrors());
            ApiModule::sendResponse($result, $this);
        }
        
        $result = ApiModule::$defaultSuccessResponse;
        $result['message'] = 'ok';
        ApiModule::sendResponse($result, $this);
        } catch (Exception $ex) {
            MyFormat::ApiCatchError($ex, $this);
        }
    }
    
   
}
