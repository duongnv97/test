<?php

class SpjController extends ApiController
{
    // ip of 90 NCV  "115.79.39.86", "183.80.100.139"
//    ip South Telecom '123.30.142.10', '123.30.142.22', '123.30.142.18'
//    Jul0918 ip South Telecom New '113.161.113.133', '113.161.113.131', '123.30.142.17', 113.161.113.129, 123.20.71.55, 14.187.133.5
    public $ipAllow = array('113.161.113.133', '113.161.113.131', '14.187.133.5', '123.20.71.55', '113.161.113.129', '123.30.142.17', '123.30.142.10', '123.30.142.22', '123.30.142.18', '115.79.39.86', '128.199.168.40');
//    public $ipKey = 'dc36cc6fb2db91e515372db1821b6275';// Jul 05, 2016 Remember this, do not remove
    public $ipKey   = 'dc36cc';

    /**
     * @Author: ANH DUNG Fix Mar 12, 2017 ==== Jul 05, 2016
     * @Todo: xử lý trả về ext của đại lý khi post lên số phone của KH. server của worldfone pbx của South Telecom
     * -- bỏ => xử lý trả về ext của đại lý khi post lên số phone của KH. server của FPT phần mềm Phone Call
     * @Resource: spj/getExt
     * @ex_request: http://spj.daukhimiennam.com/api/spj/getExt
     */
    public function actionGetExt()
    {
        try{
            $result = ApiModule::$defaultSuccessResponse;
            $result['extension']    = '';
            $result['queue']        = Call::DEFAULT_EXT;
            Logger::WriteLog(json_encode($_GET));// dev only debug
            if(!$this->checkIpOnly()){
                throw new Exception('Ip or Key not valid');
            }
//            if(isset($_GET['callernumber'])){
////                $result['queue'] = $model->getExtOfCallCenter($_GET['callernumber']);
//            }
            
            ApiModule::sendResponse($result, $this);
        } catch (Exception $ex) {
            MyFormat::ApiCatchError($ex, $this);
        }
    }

    /**
     * @Author: ANH DUNG Jul 05, 2016
     * @Todo: chỉ cho phép request từ 1 IP nhất định, xử lý cho phần mềm Call Center 1900
     * @param: $model is model UsersPhone
     */
    public function checkIp() {
        $ok = false;
        $ip_address = MyFormat::getIpUser();
        if(in_array($ip_address, $this->ipAllow)){
            if(isset($_POST['key']) && $_POST['key'] == $this->ipKey){
                $ok = true;
            }
        }
        return $ok;
    }
    
    /**
     * @Author: ANH DUNG Aug 13, 2016
     * @Todo: chỉ cho phép request từ 1 IP nhất định, chỉ check IP
     */
    public function checkIpOnly() {
        $ip_address = MyFormat::getIpUser();
        if(in_array($ip_address, $this->ipAllow)){
            return true;
        }
        return false;
    }
    
   /**
     * @Author: ANH DUNG Aug 13, 2016
     * @Todo: xử lý trả về json string tọa độ google map của agent
     * @Resource: spj/getLocationAgent
     * @ex_request: http://spj.daukhimiennam.com/api/spj/getLocationAgent
     */
    public function actionGetLocationAgent()
    {
        try{
//            if(!$this->checkIpOnly()){
//                throw new Exception('Ip or Key not valid');
//            }// Jan2518 tạm bỏ đoạn check này đi để Trung get
            $needMore = [];
            $mGateApi = new GateApi();
            if(isset($_GET['GetAppMap'])){
                $needMore['GetAppMap'] = 1;
                $aData = $mGateApi->getLocationAgent($needMore);
                echo CJavaScript::jsonEncode($aData);die; 
            }
            echo $mGateApi->getLocationAgent($needMore);die;
        } catch (Exception $ex) {
            MyFormat::ApiCatchError($ex, $this);
        }
    }

    
    /**
     * @Author: ANH DUNG Mar 12, 2017
     * @Todo: xử lý trả save all event của 1 cuộc gọi
     * @Resource: spj/callEvent
     * @ex_request: http://spj.daukhimiennam.com/api/spj/callEvent
     */
    public function actionCallEvent()
    {
        try{
            $this->callEventValidateUrl();
//            Logger::WriteLog(json_encode($_GET));// dev only debug
            $model = new CallTemp();
            $model->saveEvent();
            die;
        } catch (Exception $ex) {
            $json = isset($_GET) ? MyFormat::jsonEncode($_GET) : ' Nodata';
            $info = 'API ERROR -- api/spj/callEvent '.$ex->getMessage(). ' -- DATA: '.$json;
            Logger::WriteLog($info);
            die;
        }
    }
    
    /**
     * @Author: ANH DUNG Mar 12, 2017
     * @Todo: callEventValidateUrl
     */
    public function callEventValidateUrl() {
//        if(!isset($_GET['secret']) || $_GET['secret'] != $this->getSecret()){
//            throw new Exception('Request not valid');
//        }// Jun2518 tạm bỏ đoạn check này đi, thấy không cần thiết check lắm, vì check ip đã đủ rồi
        
        if(!$this->checkIpOnly()){
            throw new Exception('CallEventValidateUrl Ip or Key not valid');
        }
    }
    
    /** @Author: DungNT Aug 25, 2019
     *  @Todo: sync cache for server load balancer
     *  @Resource: api/spj/syncCache
     * $_POST['ListCacheKey'] is string ex: APP_CONFIG_AGENT,ARR_MODEL_AGENT4168,ARR_MODEL_PROVINCE
     **/
    public function actionSyncCache() {
        try{
            if(!isset($_POST['cacheKey'])){
                Logger::WriteLog($_SERVER['SERVER_ADDR']." Run actionSyncCache listCacheFileName: empty ******************* ");
                die();
            }
            $mCronTask = new CronTask();
            $mCronTask->syncUpdateCacheFileName();
        } catch (Exception $ex) {
            $json = isset($_POST) ? MyFormat::jsonEncode($_POST) : ' Nodata';
            $info = 'API ERROR -- api/spj/syncCache  '.$ex->getMessage(). ' -- DATA: '.$json;
            Logger::WriteLog($info);
            die;
        }
    }
    
    /**
     * @Author: DuongNV Oct0819
     * @Todo: xử lý trả về json string tọa độ google map của agent
     * @Resource: spj/getListAgent
     * @ex_request: http://dev.spj.vn/api/spj/getListAgent?province_id=1
     */
    public function actionGetListAgent()
    {
        try{
            $pageSize       = 12;
            $mUsersExtend   = new UsersExtend();
            $mUsersExtend->province_id = empty($_GET['province_id']) ? '' : $_GET['province_id'];
            $mUsersExtend->district_id = empty($_GET['district_id']) ? '' : $_GET['district_id'];
            $mUsersExtend->date_from   = empty($_GET['date_from']) ? '' : $_GET['date_from'];
            $mUsersExtend->date_to     = empty($_GET['date_to']) ? '' : $_GET['date_to'];
            $page                      = empty($_GET['page']) ? 0 : $_GET['page'];
            $result                    = [];
            $mUsersExtend->apiGetListAgent($result, $pageSize, $page);
            echo CJavaScript::jsonEncode($result);die;
        } catch (Exception $ex) {
            MyFormat::ApiCatchError($ex, $this);
        }
    }
    
}
