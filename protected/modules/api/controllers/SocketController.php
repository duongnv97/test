<?php

class SocketController extends ApiController
{
    // ip of 90 NCV  "115.79.39.86", "183.80.100.139"
    public $ipAllow = array('128.199.130.14', '128.199.166.98');

    /**
     * @Author: ANH DUNG Oct 02, 2016
     * @Todo: add socket notify, nhận notify từ server chính (spj) có thể trigger gửi luôn
     * @Resource: socket/notifyAdd
     * @ex_request: http://io.huongminhgroup.com/api/socket/notifyAdd
     */
    public function actionNotifyAdd()
    {
        try{
            $result = ApiModule::$defaultResponse;
            if(!$this->checkIpOnly()){
                throw new Exception('Ip not valid');
            }
            if(isset($_POST['q'])){
                $q = json_decode($_POST['q'], true);
                $result = ApiModule::$defaultSuccessResponse;
//                SyncData::apiCreateUpdate($q, 'GasSocketNotify', array('id', 'created_date'));// Close Now2918
                SyncData::apiCreateUpdate($q, 'GasSocketNotify', array('id'));
                PushServerCommand::checkNotification(); // => không thể run kiểu này dc, sẽ trả về error do hàm echo ở checkNotification bung lỗi
                $result['message'] = 'success';
            }
            ApiModule::sendResponse($result, $this);
        } catch (Exception $ex) {
            MyFormat::ApiCatchError($ex, $this);
        }
    }
    
    /**
     * @Author: ANH DUNG Oct 03, 2016
     * @Todo: add token login
     * @Resource: socket/tokenAdd
     * @ex_request: http://io.huongminhgroup.com/api/socket/tokenAdd
     * @ex_request: 
     */
    public function actionTokenAdd()
    {
        try{
            $result = ApiModule::$defaultResponse;
            if(!$this->checkIpOnly()){
                throw new Exception('Ip not valid');
            }
            if(isset($_POST['q'])){
                $q = json_decode($_POST['q'], true);
                $result = ApiModule::$defaultSuccessResponse;
//                SyncData::apiCreateUpdate($q, 'UsersTokens', array('id', 'created_date'));// Close Now2918
                SyncData::apiCreateUpdate($q, 'UsersTokens', array('id'));
                $result['message'] = 'success';
            }
            ApiModule::sendResponse($result, $this);
        } catch (Exception $ex) {
            MyFormat::ApiCatchError($ex, $this);
        }
    }

    /**
     * @Author: ANH DUNG Jul 05, 2016
     * @Todo: chỉ cho phép request từ 1 IP nhất định, xử lý cho phần mềm Call Center 1900
     * @param: $model is model UsersPhone
     */
    public function checkIp($model) {
        return true;// Aug2419 DungNT
        $ok = false;
        $ip_address = MyFormat::getIpUser();
        if(in_array($ip_address, $this->ipAllow)){
            if(isset($_POST['key']) && $_POST['key'] == $this->ipKey){
                $ok = true;
            }
        }
        return $ok;
    }
    
    /**
     * @Author: ANH DUNG Aug 13, 2016
     * @Todo: chỉ cho phép request từ 1 IP nhất định, chỉ check IP
     */
    public function checkIpOnly() {
        return true;// // Aug2419 DungNT -- only dev test, need close
        $ip_address = MyFormat::getIpUser();
        if(in_array($ip_address, $this->ipAllow)){
            return true;
        }
        return false;
    }

    /**
     * @Author: ANH DUNG Oct 02, 2016
     * @Todo: socket notifyReceived 
     * @Resource: socket/notifyReceived
     * @ex_request: http://spj.daukhimiennam.com/api/socket/notifyReceived
     */
    public function actionNotifyReceived()
    {
        try{
            $result = ApiModule::$defaultResponse;
            $this->checkRequest();
            $q = json_decode($_POST['q']);
            $this->checkRequiredParams($q, array('token','notify_id', 'received_id','received_name', 'note'));
            $mUser      = $this->getUserByToken($result, $q->token);// Oct 12, 2015 for check inactive or delete User
            
            $mSocketNotify = GasSocketNotify::model()->findByPk($q->notify_id);
            if(is_null($mSocketNotify)){
                throw new Exception('Request not valid');
            }
            $mSocketNotify->updateAgentConfirmOrder($q, $mUser);
            
            $result = ApiModule::$defaultSuccessResponse;
            $result['message'] = 'success';
            ApiModule::sendResponse($result, $this);
        } catch (Exception $ex) {
            MyFormat::ApiCatchError($ex, $this);
        }
    }
    
    /**
     * @Author: ANH DUNG Oct 18, 2016
     * @Todo: xử lý khi client nhận được notify thì update biến status cho IO notify để không gửi xuông nữa
     * @Resource: socket/clientReceivedNotify
     * @ex_request: http://io.huongminhgroup.com/api/socket/clientReceivedNotify
     * @note: hàm này sẽ được gọi từ bên server daukhimienam, không gọi = ajax nữa
     */
    public function actionClientReceivedNotify()
    {
        try{
            $result = ApiModule::$defaultResponse;
            if(!isset($_POST['server_io_notify_id'])){
                die;
            }
            GasSocketNotify::setStatusByPk($_POST['server_io_notify_id'], GasSocketNotify::STATUS_COMPLETED);
            die;
        } catch (Exception $ex) {
            MyFormat::ApiCatchError($ex, $this);
        }
    }
    
    /**
     * @Author: ANH DUNG Oct 02, 2016
     * @Todo: socket notifyReceived 
     * @Resource: socket/notifyDevTest
     * @ex_request: http://io.huongminhgroup.com/api/socket/notifyDevTest
     * @ex_request: http://spj.daukhimiennam.com/api/socket/notifyDevTest
     */
    public function actionNotifyDevTest()
    {
        try{
            $result = ApiModule::$defaultSuccessResponse;
            $this->checkRequest();
            $q = json_decode($_POST['q']);
            $this->checkRequiredParams($q, array('token','agent_id'));
            $mUser      = $this->getUserByToken($result, $q->token);// Oct 12, 2015 for check inactive or delete User
            
            $mCustomer  = Users::model()->findByPk(874212);
            $mAgent     = Users::model()->findByPk($q->agent_id);
//            public static function addMessage($sender_id, $userId, $code, $message, $json)
            $message = "Đặt hàng: {$mCustomer->getFullName()}. Test nội dung đặt hàng";
            $json = array();
            $json['agent_name']     = $mAgent->getFullName();
//            $json['agent_name']     = 'admin agent';
            $json['customer_name']  = $mCustomer->getFullName();
            $json['type_customer']  = $mCustomer->getTypeCustomerText();
            $json['note']           = "Ghi chú đặt hàng";
            GasSocketNotify::addMessage(0, GasTickets::DIEU_PHOI_CHUONG, $mUser->id, GasSocketNotify::CODE_STORECARD, $message, $json);
            // for send luôn
//            GasSocketNotify::pushToClient();
            ApiModule::sendResponse($result, $this);
        } catch (Exception $ex) {
            MyFormat::ApiCatchError($ex, $this);
        }
    }
    
    /**
     * @Author: ANH DUNG Dec 04, 2016
     * @Todo: xử lý get các info user của socket
     * @Resource: socket/currentOnlineUser
     * @ex_request: http://io.huongminhgroup.com/api/socket/currentOnlineUser
     * @note: hàm này sẽ được gọi từ bên server daukhimienam, không gọi = ajax nữa
     */
    public function actionCurrentOnlineUser()
    {
        try{
            $post_string='@super_admin@';
            $host="tcp://io.huongminhgroup.com:8004";
//            $host="tcp://localhost:8004";

            $sock = stream_socket_client($host, $errno, $errstr);
            fwrite($sock, "GET /SpjOnlineUser=1 HTTP/1.1\r\nHost: localhost\r\nUpgrade: websocket\r\nConnection: Upgrade\r\nSec-WebSocket-Key: tQXaRIOk4sOhgoq7SBs43g==\r\nSec-WebSocket-Version: 13\r\n\r\n");
            $response = fread($sock, 10000000);
            
            $remove = "\r\n";
//            $remove = "\n";// or
            $split = explode($remove, $response);
            $array[] = null;
            $tab = "\t";
            foreach ($split as $string)
            {
                $row = explode($tab, $string);
                array_push($array,$row);
            }
            $myLastElement = end($array);
            // http://stackoverflow.com/questions/5329866/remove-everything-before-the-first-in-a-string
            $string = preg_replace('/^[^{]*{\s*/', '', $myLastElement[0]);
            $string = '{'.$string;
            echo '<pre>';
            print_r(json_decode($string, true));
            echo '</pre>';
            die;
            
            GasSocketNotify::setStatusByPk($_POST['server_io_notify_id'], GasSocketNotify::STATUS_COMPLETED);
            die;
        } catch (Exception $ex) {
            MyFormat::ApiCatchError($ex, $this);
        }
    }

}
