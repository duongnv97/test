<?php
/**
 * @Author: DungNT Jan 11, 2017
 * @Todo: xử lý đặt hàng của KH bò mối
 */
class BoMoiController extends ApiController
{
    /**
     * @Author: DungNT Jan 11, 2017
     * @Todo: Customer list order - dùng chung cho Driver list
     * @Resource: boMoi/boMoiList
     * @ex_json_request: {"token":"403acbe7932b3c7f2f3ce5a906757c29","page":"0"}
     */
    public function actionBoMoiList()
    {
        try{
            $result = ApiModule::$defaultResponse;
            $this->checkRequest();
            $q = json_decode($_POST['q']);
            $this->checkRequiredParams($q, array('token', 'page'));
            $mUser      = $this->getUserByToken($result, $q->token);// Oct 12, 2015 for check inactive or delete User
            $this->mUserApp = $mUser;
            $this->checkVersionCode($q, $mUser);
            if($mUser->hasErrors()){
                $result['message'] = HandleLabel::FortmatErrorsModel($mUser->getErrors());
                ApiModule::sendResponse($result, $this);
            }
/*            for test
            $temp = '{"token":"403acbe7932b3c7f2f3ce5a906757c29","page":"0"}';
            $q = json_decode($temp);
            $mUser = Users::model()->findByPk(121257);
 */
            
            $result = ApiModule::$defaultSuccessResponse;
            $result['message'] = 'List';
            $mAppOrder = new GasAppOrder();
            $mAppOrder->mAppUserLogin   = $mUser;
            $mAppOrder->handleApiList($result, $q);
            ApiModule::sendResponse($result, $this);
        } catch (Exception $ex) {
            MyFormat::ApiCatchError($ex, $this);
        }
    }

    /**
     * @Author: DungNT Jan 11, 2017
     * @Todo: Customer BoMoi create đơn hàng BB From App, B12 B45 50
     * @Resource: boMoi/boMoiCreate
     * @ex_json_request: {"token":"3ba8bfa6221c671fe682f965491fd86c","b50":"1", "b45":"0", "b12":"2", "b6":"0", "note_customer":""}
     */
    public function actionBoMoiCreate()
    {
        try{
//            throw new Exception('Chức năng này hiện đang được xây dựng, bạn vui lòng quay lại sau.');
//            $from = time();
            $result = ApiModule::$defaultResponse;
            $this->checkRequest();
            $q = json_decode($_POST['q']);
            $this->checkRequiredParams($q, array('token'));
            $mUser  = $this->getUserByToken($result, $q->token);
            $mAppOrder = new GasAppOrder('ApiCreate');
            
            $mAppOrder->mAppUserLogin = $mUser;
            $mAppOrder->handlePost($q);
            $mAppOrder->checkLimitCreate();
            $this->responseIfError($mAppOrder);
            $mAppOrder->handleSave();
//            $mAppOrder = GasAppOrder::model()->findByPk(1);
            $result             = ApiModule::$defaultSuccessResponse;
            $result['message']  = 'Đặt hàng thành công, nhân viên công ty có thể liên hệ với quý khách để xác nhận đơn hàng';
            $result['record']   = $mAppOrder->appView();
            $mAppOrder->makeSocketNotify();// May 05, 2017
            
//            $to = time();
//            $second = $to-$from;
//            $info = "boMoi/boMoiCreate HGD ".' done in: '.($second).'  Second  <=> '.($second/60).' Minutes';
//            Logger::WriteLog($info);
            ApiModule::sendResponse($result, $this);
        } catch (Exception $ex) {
            MyFormat::ApiCatchError($ex, $this);
        }
    }

    /**
     * @Author: DungNT Jan 11, 2017
     * @Todo: Customer BoMoi view - dùng chung cho Driver View
     * @Resource: boMoi/boMoiView
     * @ex_json_request: {"token":"403acbe7932b3c7f2f3ce5a906757c29","order_id":"1"}
     */
    public function actionBoMoiView()
    {
        try{
            $result = ApiModule::$defaultResponse;
            $this->checkRequest();
            $q = json_decode($_POST['q']);
            $this->checkRequiredParams($q, array('token', 'order_id'));
            $mUser      = $this->getUserByToken($result, $q->token);
            $mAppOrder     = GasAppOrder::getModelView($mUser, $q->order_id);
            if(is_null($mAppOrder)){
                throw new Exception('Đơn hàng yêu cầu không hợp lệ, kiểm tra lại danh sách', SpjError::SELL001);
            }
//            $mAppOrder = GasAppOrder::model()->findByPk(1);// only dev test
            $mAppOrder->mAppUserLogin = $mUser;
            $result             = ApiModule::$defaultSuccessResponse;
            $result['message']  = '';
            $result['record']   = $mAppOrder->appView();
            
            ApiModule::sendResponse($result, $this);
        } catch (Exception $ex) {
            MyFormat::ApiCatchError($ex, $this);
        }
    }
    
    /**
     * @Author: DungNT Jan 11, 2017
     * @Todo: Driver update order
     * @Resource: boMoi/boMoiDriverUpdate
     * @ex_json_request: {"token":"403acbe7932b3c7f2f3ce5a906757c29","order_id":"1","note_employee":"test note"
     * order_detail => array item: materials_type_id, materials_id, qty, price, amount, qty_real, seri, kg_empty, kg_has_gas
     */
    public function actionBoMoiDriverUpdate()
    {
        try{
            $result = ApiModule::$defaultResponse;
            $this->checkRequest();
            $q = json_decode($_POST['q']);
            $this->checkRequiredParams($q, array('token', 'order_id'));
            $mUser  = $this->getUserByToken($result, $q->token);
            $mAppOrder = new GasAppOrder('ApiCreate');
            $mAppOrder->driver_id = $mAppOrder->employee_maintain_id = $mUser->id;
            $mAppOrder->id        = $q->order_id;
            $mAppOrder = $mAppOrder->getModelUpdate();
            if(is_null($mAppOrder)){
                throw new Exception('Đơn hàng yêu cầu không hợp lệ, kiểm tra lại danh sách', SpjError::SELL001);
            }
            $mTransactionHistory = new TransactionHistory();
            $mAppOrder->mAppUserLogin = $mUser;
            $aSeri = $mAppOrder->getArraySeriVo();
            $mAppOrder->handlePostDriverUpdate($q);
            if(in_array($mUser->role_id, $mTransactionHistory->getRoleCanHandleHgd())){
//                $mAppOrder->requiredFile = false;// Now2217 required file với toàn bộ đơn hàng của giao nhận
            }elseif($mUser->role_id == ROLE_DRIVER){ // Sep0617 tạm tắt up hình với app xe tải của ios, sẽ bật lên sau
//                if(count($mAppOrder->rFile) > 0 || $q->platform == UsersTokens::PLATFORM_IOS){
//                if(count($mAppOrder->rFile) > 0){
//                    $mAppOrder->requiredFile = false; 
//                }
            }
            if(count($mAppOrder->rFile) > 0 || $mAppOrder->isOrderAgent()){
                $mAppOrder->requiredFile = false;
            }

            $mAppOrder->apiValidateFile();// Tạm close lại đã vì chưa có app cho user up image
            if($mAppOrder->hasErrors()){
                $result['message'] = HandleLabel::FortmatErrorsModel($mAppOrder->getErrors());
                ApiModule::sendResponse($result, $this);
            }
            $mAppOrder->setGasRemainStatus(GasRemain::STATUS_NOT_EXPORT);
            $mAppOrder->makeGasRemainDeleteOld($aSeri);
            $mAppOrder->handleSaveDriverUpdate();
            $mAppOrder->makeGasRemain();// only dev test cập nhật Gas Dư Apr 28, 2017
            $mAppOrder->saveFile();// cập nhật nợ cho đơn hàng
            $mAppOrder = GasAppOrder::model()->findByPk($mAppOrder->id);
            $mAppOrder->mAppUserLogin = $mUser;
            $result             = ApiModule::$defaultSuccessResponse;
            $result['message']  = 'Cập nhật đơn hàng thành công';
            $result['record']   = $mAppOrder->appView();
            ApiModule::sendResponse($result, $this);
        } catch (Exception $ex) {
            MyFormat::ApiCatchError($ex, $this);
        }
    }
    
    /** 
     * @Author: DungNT Jan 19, 2017
     * @Todo: android action Nhận Hủy order Bò Mối của NVGN
     * @Resource: boMoi/boMoiSetEvent
     * @ex_json_request: {"token":"403acbe7932b3c7f2f3ce5a906757c29","app_order_id":"1", "action_type":1}
     * có 2 action sử dụng api này:
     * 1. GN nhận ĐH
     * 2. GN hủy nhận ĐH
     */    
    public function actionBoMoiSetEvent()
    {
        try{
        $result = ApiModule::$defaultResponse;
        $this->checkRequest();
        $q = json_decode($_POST['q']);
        $this->checkRequiredParams($q, array('token', 'app_order_id','action_type', 'latitude', 'longitude'));
        $this->validateLocationGps($q);
        $mUser = $this->getUserByToken($result, $q->token);// Oct 12, 2015 for check inactive or delete User
        
        $mAppOrder = GasAppOrder::model()->findByPk($q->app_order_id);
//        $aAgent = Users::GetKeyInfo($mUser, UsersExtend::JSON_ARRAY_AGENT);
//        if(is_null($mAppOrder) || ( is_array($aAgent) && !in_array($mTransactionHistory->agent_id, $aAgent) )){
        if(is_null($mAppOrder)){
            throw new Exception("Đơn hàng yêu cầu không hợp lệ. User: $mUser->id order_id = $q->app_order_id", SpjError::SELL001);
        }
        $mAppOrder->mAppUserLogin = $mUser;
        if($q->platform == UsersTokens::PLATFORM_IOS){// Now2217 ios cần fix chỗ này, hiện tại hoàn thành ko up load file được
//            $mAppOrder->requiredFile = false; // Jun0918 yêu cầu up hình 100% các đơn, nếu ko có thì yêu cầu chuyển sang android dùng
        }
        $mAppOrder->appCheckFile($q);
        $mAppOrder->handleSetEvent($q);
        if($mAppOrder->hasErrors()){
            $result['message'] = HandleLabel::FortmatErrorsModel($mAppOrder->getErrors());
            ApiModule::sendResponse($result, $this);
        }
        $mAppOrder->saveFile();
        $result = ApiModule::$defaultSuccessResponse;
        $result['message'] = 'ok';
        ApiModule::sendResponse($result, $this);
        } catch (Exception $ex) {
            MyFormat::ApiCatchError($ex, $this);
        }
    }
    
    /** @Author: DungNT Dec 09, 2017
     *  @Todo: không cho tạo thẻ kho với cty DKMN
     **/
    public function checkAgentLock($mStorecard) {
        $aAgentLock = [768411];
        if(in_array($mStorecard->customer_id, $aAgentLock)){
            throw new Exception('Không thể tạo thẻ kho cho đại lý này. Bạn thử lại với Kho 90 Nguyễn Cửu Vân');
        }
        $aRoleMonitor = [ROLE_MONITORING_MARKET_DEVELOPMENT, ROLE_ACCOUNTING_ZONE];// Jul0919 ko cho KTKV + CV tạo phiếu nhập xuất cho các kho trạm
        if(in_array($mStorecard->mAppUserLogin->role_id, $aRoleMonitor) && 
            (in_array($mStorecard->customer_id, GasCheck::getAgentNotGentAuto()) || in_array($mStorecard->user_id_create, GasCheck::getAgentNotGentAuto()) )
        ){
            throw new Exception("Tài khoản của bạn đang ở {$mStorecard->getAgent()}. KTKV, Giám Sát không thể tạo thẻ kho cho kho trạm, liên hệ IT để hỗ trợ");
        }
    }
    
    /**
     * @Author: DungNT Feb 25, 2017
     * @Todo: NVGN tạo thẻ kho
     * @Resource: boMoi/storecardCreate
     * @ex_json_request: {"token":"83a2319196f5131f54b162e26aa4cf07","customer_id":"163618", "type_in_out":"4", "date_delivery":"25\/02\/2017", "note":"test note","order_detail":[{"materials_id":864,"qty":3},{"materials_id":872,"qty":5}]}
     * {"note":1,"order_detail":[{"materials_id":864,"qty":3},{"materials_id":872,"qty":5}]}
     */
    public function actionStorecardCreate()
    {
        try{
            $result = ApiModule::$defaultResponse;
            $this->checkRequest();
            $q = json_decode($_POST['q']);
            $this->checkRequiredParams($q, array('token', 'customer_id','type_in_out', 'date_delivery', 'order_detail', 'note'));
            $mUser      = $this->getUserByToken($result, $q->token);
            $agent_id   = $this->getAgentOfEmployee($mUser);
            $aAgentLimit = [GasCheck::DL_KHO_BEN_CAT, GasCheck::KHO_PHUOC_TAN];
            if($mUser->role_id == ROLE_DRIVER && in_array($agent_id, $aAgentLimit) 
                && $mUser->id != 740868 ){// van cho tai xe tao the kho. Mở cho Tài xế Thành PT tạo thẻ kho - Nguyễn Văn Linh - Nguyễn Văn Tố
                throw new Exception('StorecardCreate - Chức năng này đã bị tắt, không thể tạo thẻ kho. Liên hệ Dũng IT để hỗ trợ');
            }
            $mStorecard = new GasStoreCard('ApiCreate');
            $mStorecard->mAppUserLogin  = $mUser;
            $mStorecard->user_id_create = $agent_id;
            $mStorecard->handlePost($q);
            $this->checkAgentLock($mStorecard);
            $mStorecard->apiValidateFile();
            $mStorecard->checkChanHang();
            if($mStorecard->hasErrors()){
                $result['message'] = HandleLabel::FortmatErrorsModel($mStorecard->getErrors());
                ApiModule::sendResponse($result, $this);
            }
            $mStorecard->handleSave($q);
            $mStorecard->sourceFrom = BaseSpj::SOURCE_FROM_APP;
            $mStorecard->autoMakeRecordForAgent();
            $result             = ApiModule::$defaultSuccessResponse;
            $result['message']  = 'Tạo thẻ kho thành công';
            $mStorecard->created_date = date('Y-m-d H:i:s');
            $result['record']   = $mStorecard->appView();
            
            ApiModule::sendResponse($result, $this);
        } catch (Exception $ex) {
            MyFormat::ApiCatchError($ex, $this);
        }
    }
    
    /**
     * @Author: DungNT Mar 04, 2017
     * @Todo: NVGN update thẻ kho
     * @Resource: boMoi/storecardUpdate
     * @ex_json_request: {"token":"83a2319196f5131f54b162e26aa4cf07","id":123,"customer_id":"163618", "type_in_out":"4", "date_delivery":"25\/02\/2017", "note":"test note","order_detail":[{"materials_id":864,"qty":3},{"materials_id":872,"qty":5}]}
     * {"note":1,"order_detail":[{"materials_id":864,"qty":3},{"materials_id":872,"qty":5}]}
     */
    public function actionStorecardUpdate()
    {
        try{
            $result = ApiModule::$defaultResponse;
            $this->checkRequest();
            $q = json_decode($_POST['q']);
            $this->checkRequiredParams($q, array('token', 'customer_id','type_in_out', 'date_delivery', 'order_detail', 'note'));
            $mUser  = $this->getUserByToken($result, $q->token);
            
            $mStorecard = new GasStoreCard('ApiCreate');
            $mStorecard->mAppUserLogin = $mUser;
            $mStorecard = $mStorecard->loadAppUpdate($q);
            if(is_null($mStorecard) || !empty($mStorecard->parent_id)){
                throw new Exception('Cập nhật thẻ kho không hợp lệ');
            }
            $mStorecard->mAppUserLogin  = $mUser;
            $mStorecard->scenario       = 'ApiCreate';
            $mStorecard->handlePost($q);
            $this->checkAgentLock($mStorecard);
            $mStorecard->apiValidateFile();
            if($mStorecard->hasErrors()){
                $result['message'] = HandleLabel::FortmatErrorsModel($mStorecard->getErrors());
                ApiModule::sendResponse($result, $this);
            }
            $mStorecard->handleSave($q);
            $mStorecard->sourceFrom = BaseSpj::SOURCE_FROM_APP;
            $mStorecard->autoMakeRecordForAgent();
            $result             = ApiModule::$defaultSuccessResponse;
            $result['message']  = 'Cập nhật thẻ kho thành công';
            $result['record']   = $mStorecard->appView();
            
            ApiModule::sendResponse($result, $this);
        } catch (Exception $ex) {
            MyFormat::ApiCatchError($ex, $this);
        }
    }
    
    /**
     * @Author: DungNT Feb 27, 2017
     * @Todo: giao nhận storecardCreate view
     * @Resource: boMoi/storecardView
     * @ex_json_request: {"token":"83a2319196f5131f54b162e26aa4cf07","id":"268"}
     */
    public function actionStorecardView()
    {
        try{
            $result = ApiModule::$defaultResponse;
            $this->checkRequest();
            $q = json_decode($_POST['q']);
            $this->checkRequiredParams($q, array('token', 'id'));
            $mUser          = $this->getUserByToken($result, $q->token);
            $agent_id = $this->getAgentOfEmployee($mUser);
            $mStorecard     = new GasStoreCard();
            $mStorecard->mAppUserLogin = $mUser;
//            $mStorecard->mAppUserLogin->parent_id = $agent_id;// Jun 26, 2017 close lại không check đk này nữa
            $mStorecard     = $mStorecard->getModelApp($q->id);
            if(is_null($mStorecard)){
                throw new Exception('Thẻ kho yêu cầu không hợp lệ. View thẻ kho', SpjError::SELL001);
            }
//            $mStorecard = GasStoreCard::model()->findByPk(268);// only dev test
            $mStorecard->mAppUserLogin = $mUser;
            $result             = ApiModule::$defaultSuccessResponse;
            $result['message']  = '';
            $result['record']   = $mStorecard->appView();
            ApiModule::sendResponse($result, $this);
        } catch (Exception $ex) {
            MyFormat::ApiCatchError($ex, $this);
        }
    }
    
    /**
     * @Author: DungNT Feb 27, 2017
     * @Todo: giao nhận storecardList
     * @Resource: boMoi/storecardList
     * @ex_json_request: {"token":"403acbe7932b3c7f2f3ce5a906757c29","page":"0"}
     */
    public function actionStorecardList()
    {
        try{
            $result = ApiModule::$defaultResponse;
            $this->checkRequest();
            $q = json_decode($_POST['q']);
            $this->checkRequiredParams($q, array('token', 'page'));
            $mUser = $this->getUserByToken($result, $q->token);
            $agent_id = $this->getAgentOfEmployee($mUser);
/*            for test
            $temp = '{"token":"403acbe7932b3c7f2f3ce5a906757c29","page":"0"}';
            $q = json_decode($temp);
            $mUser = Users::model()->findByPk(121257);
 */
            $result = ApiModule::$defaultSuccessResponse;
            $result['message']          = 'List';
            $mStorecard                 = new GasStoreCard();
            $mStorecard->user_id_create = $agent_id;
            $mStorecard->handleApiList($result, $q, $mUser);
            ApiModule::sendResponse($result, $this);
        } catch (Exception $ex) {
            MyFormat::ApiCatchError($ex, $this);
        }
    }
    
    /**
     * @Author: DungNT Mar 06, 2017
     * @Todo: NVGN tạo thu chi
     * @Resource: boMoi/employeeCashbookCreate
     * @ex_json_request: {"token":"1960bceed46aafd1bd8ae15799fd4856","customer_id":"163618", "master_lookup_id":"4", "date_input":"25\/02\/2017", "amount":500000,"note":"test note"}
     * 
     */
    public function actionEmployeeCashbookCreate()
    {
        try{
            $result = ApiModule::$defaultResponse;
            $this->checkRequest();
            $q = json_decode($_POST['q']);
            $this->checkRequiredParams($q, array('token', 'customer_id','master_lookup_id', 'date_input', 'amount', 'note'));
            $mUser      = $this->getUserByToken($result, $q->token);
            $agent_id   = $this->getAgentOfEmployee($mUser);
            
            $mEmployeeCashbook = new EmployeeCashbook('ApiCreate');
            $mEmployeeCashbook->mAppUserLogin   = $mUser;
            $mEmployeeCashbook->employee_id     = $mUser->id;
            $mEmployeeCashbook->agent_id        = $agent_id;
            $mEmployeeCashbook->app_order_id    = isset($q->app_order_id) ? $q->app_order_id : 0;
            if($mUser->role_id == ROLE_DRIVER || !empty($mEmployeeCashbook->app_order_id)){
                $mEmployeeCashbook->requiredFile = false;
            }
            if(!empty($mEmployeeCashbook->app_order_id) && $q->platform == UsersTokens::PLATFORM_IOS){// Now2217 ios cần fix chỗ này, hiện tại hoàn thành ko up load file được
                $mEmployeeCashbook->requiredFile = true;
            }
            
            $mEmployeeCashbook->handlePost($q);
            $mEmployeeCashbook->checkAmountGoBank();
            $mEmployeeCashbook->apiValidateFile();
            if($mEmployeeCashbook->hasErrors()){
                $result['message'] = HandleLabel::FortmatErrorsModel($mEmployeeCashbook->getErrors());
                ApiModule::sendResponse($result, $this);
            }
            $mScheduleEmployeeCashbook = $mEmployeeCashbook->mapToModelSchedule($q, $mUser);
            if(!empty($mScheduleEmployeeCashbook)){
                $mEmployeeCashbook = $mScheduleEmployeeCashbook;
            }
            $mEmployeeCashbook->setNotifySmsToCustomer();
            $mEmployeeCashbook->handleSave($q);
            $result             = ApiModule::$defaultSuccessResponse;
            $result['message']  = 'Tạo thu chi thành công';
            $mEmployeeCashbook->created_date = date('Y-m-d H:i:s');
            $result['record']   = $mEmployeeCashbook->appView();
            
            ApiModule::sendResponse($result, $this);
        } catch (Exception $ex) {
            MyFormat::ApiCatchError($ex, $this);
        }
    }
    
    /**
     * @Author: DungNT Mar 06, 2017
     * @Todo: NVGN update thu chi
     * @Resource: boMoi/employeeCashbookUpdate
     * @ex_json_request: {"token":"1960bceed46aafd1bd8ae15799fd4856","customer_id":"163618", "id":3,"master_lookup_id":"4", "date_input":"25\/02\/2017", "amount":500333,"note":"test note 333"}
     * 
     */
    public function actionEmployeeCashbookUpdate()
    {
        try{
            $result = ApiModule::$defaultResponse;
            $this->checkRequest();
            $q = json_decode($_POST['q']);
            $this->checkRequiredParams($q, array('token', 'id', 'customer_id','master_lookup_id', 'date_input', 'amount', 'note'));
            $mUser  = $this->getUserByToken($result, $q->token);
            
            
            $mEmployeeCashbook = new EmployeeCashbook('ApiCreate');
            $mEmployeeCashbook->mAppUserLogin = $mUser;
            $mEmployeeCashbook = $mEmployeeCashbook->loadAppUpdate($q);
            if(is_null($mEmployeeCashbook)){
                throw new Exception('Cập nhật thu chi không hợp lệ');
            }
            $mEmployeeCashbook->mAppUserLogin    = $mUser;
            $mEmployeeCashbook->scenario         = 'ApiCreate';
            $mEmployeeCashbook->oldAmount        = $mEmployeeCashbook->amount;
            $mEmployeeCashbook->handlePost($q);
            $mEmployeeCashbook->checkAmountGoBank();
            $mEmployeeCashbook->apiValidateFile();
            if($mEmployeeCashbook->hasErrors()){
                $result['message'] = HandleLabel::FortmatErrorsModel($mEmployeeCashbook->getErrors());
                ApiModule::sendResponse($result, $this);
            }
            $mEmployeeCashbook->last_update_by      = $mUser->id;
            $mEmployeeCashbook->last_update_time    = date('Y-m-d H:i:s');
            
            $mEmployeeCashbook->handleSave($q);
            $result             = ApiModule::$defaultSuccessResponse;
            $result['message']  = 'Cập nhật thu chi thành công';
            $result['record']   = $mEmployeeCashbook->appView();
            
            ApiModule::sendResponse($result, $this);
        } catch (Exception $ex) {
            MyFormat::ApiCatchError($ex, $this);
        }
    }
    
    /**
     * @Author: DungNT Mar 06, 2017
     * @Todo: giao nhận Thu Chi view
     * @Resource: boMoi/employeeCashbookView
     * @ex_json_request: {"token":"83a2319196f5131f54b162e26aa4cf07","id":"268"}
     */
    public function actionEmployeeCashbookView()
    {
        try{
            $result = ApiModule::$defaultResponse;
            $this->checkRequest();
            $q = json_decode($_POST['q']);
            $this->checkRequiredParams($q, array('token', 'id'));
            $mUser          = $this->getUserByToken($result, $q->token);
            $mEmployeeCashbook     = new EmployeeCashbook();
            $mEmployeeCashbook->mAppUserLogin = $mUser;
            $mEmployeeCashbook     = $mEmployeeCashbook->getModelApp($q->id);
            if(is_null($mEmployeeCashbook)){
                throw new Exception('Phiếu thu chi yêu cầu không hợp lệ');
            }
//            $mStorecard = GasStoreCard::model()->findByPk(268);// only dev test
            $mEmployeeCashbook->mAppUserLogin = $mUser;
            $result             = ApiModule::$defaultSuccessResponse;
            $result['message']  = '';
            $result['record']   = $mEmployeeCashbook->appView();
            ApiModule::sendResponse($result, $this);
        } catch (Exception $ex) {
            MyFormat::ApiCatchError($ex, $this);
        }
    }
    
    /**
     * @Author: DungNT Mar 06, 2017
     * @Todo: giao nhận employeeCashbookList
     * @Resource: boMoi/employeeCashbookList
     * @ex_json_request: {"token":"1960bceed46aafd1bd8ae15799fd4856","page":"0"}
     */
    public function actionEmployeeCashbookList()
    {
//        Logger::WriteLog("Load debug url: ".$_SERVER['HTTP_HOST']);
        try{
            $result = ApiModule::$defaultResponse;
            $this->checkRequest();
            $q = json_decode($_POST['q']);
            $this->checkRequiredParams($q, array('token', 'page'));
            $mUser = $this->getUserByToken($result, $q->token);
/*            for test
            $temp = '{"token":"403acbe7932b3c7f2f3ce5a906757c29","page":"0"}';
            $q = json_decode($temp);
            $mUser = Users::model()->findByPk(121257);
 */
            $result = ApiModule::$defaultSuccessResponse;
            $result['message'] = 'List';
            $mEmployeeCashbook = new EmployeeCashbook();
            $mEmployeeCashbook->mAppUserLogin = $mUser;
            $mEmployeeCashbook->handleApiList($result, $q);
            ApiModule::sendResponse($result, $this);
        } catch (Exception $ex) {
            MyFormat::ApiCatchError($ex, $this);
        }
    }
    
    /**
     * @Author: DungNT May 19, 2017
     * @Todo: Customer BoMoi set debit
     * @Resource: boMoi/boMoiSetDebit
     * @ex_json_request: {"token":"403acbe7932b3c7f2f3ce5a906757c29","id":"1"}
     */
    public function actionBoMoiSetDebit()
    {
        try{
            $result = ApiModule::$defaultResponse;
            $this->checkRequest();
            $q = json_decode($_POST['q']);
            $this->checkRequiredParams($q, array('token', 'id'));
            $mUser      = $this->getUserByToken($result, $q->token);
            $mAppOrder     = GasAppOrder::getModelView($mUser, $q->id);
            if(is_null($mAppOrder)){
                throw new Exception('Đơn hàng yêu cầu không hợp lệ, kiểm tra lại danh sách', SpjError::SELL001);
            }
            if($mAppOrder->status_debit == GasAppOrder::DEBIT_YES){
                throw new Exception('Đơn hàng đã được cập nhật nợ thành công');
            }
//            if($mUser->role_id == ROLE_DRIVER){
//                $mAppOrder->requiredFile = false;
//            }
            $mAppOrder->requiredFile = false;// Now2217 required file với toàn bộ đơn hàng của giao nhận
            if($q->platform == UsersTokens::PLATFORM_IOS){// Now2217 ios cần fix chỗ này, hiện tại hoàn thành ko up load file được
                $mAppOrder->requiredFile = true;
            }
            
            $mAppOrder->apiValidateFile();// Tạm close lại đã vì chưa có app cho user up image
            $mAppOrder->canSetDebitOnApp();
            if($mAppOrder->hasErrors()){
                $result['message'] = HandleLabel::FortmatErrorsModel($mAppOrder->getErrors());
                ApiModule::sendResponse($result, $this);
            }
            $mAppOrder->mAppUserLogin = $mUser;
            $mAppOrder->setDebit();
            $result             = ApiModule::$defaultSuccessResponse;
            $result['message']  = 'Cập nhật khách hàng nợ đơn hàng thành công';
            $result['record']   = [];
            ApiModule::sendResponse($result, $this);
        } catch (Exception $ex) {
            MyFormat::ApiCatchError($ex, $this);
        }
    }
    
    /**
     * @Author: DungNT Oct 23, 2017
     * @Todo: giao nhận gasRemainList
     * @Resource: boMoi/gasRemainList
     * @ex_json_request: {"token":"b80203d07d5f7aa0796dd316c51e0823","page":"0","type":1,"date_from":"24-09-2017","date_to":"24-10-2017","customer_id":"0"}
     */
    public function actionGasRemainList()
    {
        try{
            $result = ApiModule::$defaultResponse;
            $this->checkRequest();
            $q = json_decode($_POST['q']);
            $this->checkRequiredParams($q, array('token', 'page', 'type', 'date_from', 'date_to'));
            $mUser      = $this->getUserByToken($result, $q->token);
            $agent_id   = $this->getAgentOfEmployee($mUser);
            $result = ApiModule::$defaultSuccessResponse;
            $result['message'] = 'List';
            $mGasRemain = new GasRemain();
            $mGasRemain->mAppUserLogin      = $mUser;
            $mGasRemain->agent_id           = $agent_id;
            $mGasRemain->handleApiList($result, $q);
            ApiModule::sendResponse($result, $this);
        } catch (Exception $ex) {
            MyFormat::ApiCatchError($ex, $this);
        }
    }
    
    /**
     * @Author: DungNT Oct 23, 2017
     * @Todo: giao nhận gasRemainSetExport
     * @Resource: boMoi/gasRemainSetExport
     * @ex_json_request: {"token":"b80203d07d5f7aa0796dd316c51e0823","id":"633"}
     */
    public function actionGasRemainSetExport()
    {
        try{
            $result = ApiModule::$defaultResponse;
            $this->checkRequest();
            $q = json_decode($_POST['q']);
            $this->checkRequiredParams($q, array('token', 'id'));
            $mUser          = $this->getUserByToken($result, $q->token);
            
            $mGasRemain                     = new GasRemain('ApiCreate');
            $mGasRemain->id                 = $q->id;
            $mGasRemain                     = $mGasRemain->getModelApp();
            if(is_null($mGasRemain)){
                throw new Exception('Phiếu gas dư yêu cầu không hợp lệ');
            }
//            $mStorecard = GasStoreCard::model()->findByPk(268);// only dev test
            $mGasRemain->appSetExport();
            $result             = ApiModule::$defaultSuccessResponse;
            $result['message']  = '';
            $result['record']   = [];
            ApiModule::sendResponse($result, $this);
        } catch (Exception $ex) {
            MyFormat::ApiCatchError($ex, $this);
        }
    }
    /**
     * @Author: DungNT Oct 23, 2017
     * @Todo: giao nhận gasRemainView
     * @Resource: boMoi/gasRemainView
     * @ex_json_request: {"token":"b80203d07d5f7aa0796dd316c51e0823","id":"633"}
     */
    public function actionGasRemainView()
    {
        try{
            $result = ApiModule::$defaultResponse;
            $this->checkRequest();
            $q = json_decode($_POST['q']);
            $this->checkRequiredParams($q, array('token', 'id'));
            $mUser          = $this->getUserByToken($result, $q->token);
            
            $mGasRemain                     = new GasRemain('ApiCreate');
            $mGasRemain->id                 = $q->id;
            $mGasRemain                     = $mGasRemain->getModelApp();
            if(is_null($mGasRemain)){
                throw new Exception('Phiếu gas dư yêu cầu không hợp lệ');
            }
//            $mStorecard = GasStoreCard::model()->findByPk(268);// only dev test
            $mGasRemain->mAppUserLogin      = $mUser;
            $mGasRemain->gas_remain_type    = isset($q->gas_remain_type) ? $q->gas_remain_type : GasRemain::APP_CREATE_NORMAL;
            $result             = ApiModule::$defaultSuccessResponse;
            $result['message']  = '';
            $result['record']   = $mGasRemain->appView();
            ApiModule::sendResponse($result, $this);
        } catch (Exception $ex) {
            MyFormat::ApiCatchError($ex, $this);
        }
    }
    
    /** @Author: DungNT Oct 23, 2017
     * @Todo: giao nhận gasRemainCreate
     * @Resource: boMoi/gasRemainCreate
     * @ex_json_request: {"token":"b80203d07d5f7aa0796dd316c51e0823","date_input":"24-10-2017","customer_id":"874197","order_detail":[{"materials_id":"98","materials_type_id":"10","kg_empty":"12","kg_has_gas":"15.2","seri":"5678"},{"materials_id":"251","materials_type_id":"10","kg_empty":"12","kg_has_gas":"15.2","seri":"5678"}]}
     */
    public function actionGasRemainCreate()
    {
        try{
           $result = ApiModule::$defaultResponse;
            $this->checkRequest();
            $q = json_decode($_POST['q']);
            $this->checkRequiredParams($q, array('token', 'customer_id', 'date_input'));
            $mUser      = $this->getUserByToken($result, $q->token);
            $agent_id   = $this->getAgentOfEmployee($mUser);
            
            $mGasRemain                     = new GasRemain('ApiCreate');
            $mGasRemain->mAppUserLogin      = $mUser;
            $mGasRemain->agent_id           = $agent_id;
            $mGasRemain->mCustomer          = Users::model()->findByPk($q->customer_id);
            $mGasRemain->car_id             = isset($q->car_id) ? $q->car_id : 0;// Jun2018 xử lý Thủ Kho Cân lần 2 chọn xe để cân
            $mGasRemain->handlePost($q);
            
            if($mGasRemain->hasErrors()){
                $result['message'] = HandleLabel::FortmatErrorsModel($mGasRemain->getErrors());
                ApiModule::sendResponse($result, $this);
            }
            
            $mGasRemain->handleSaveCreate($q);
            $result             = ApiModule::$defaultSuccessResponse;
            $result['message']  = 'Tạo gas dư thành công';
            $result['record']   = [];
            
            ApiModule::sendResponse($result, $this);
        } catch (Exception $ex) {
            MyFormat::ApiCatchError($ex, $this);
        }
    }
    /**
     * @Author: DungNT Oct 23, 2017
     * @Todo: giao nhận gasRemainUpdate
     * @Resource: boMoi/gasRemainUpdate
     * @ex_json_request: {"token":"b80203d07d5f7aa0796dd316c51e0823","id":"633","date_input":"25-10-2017","customer_id":"874197","materials_id":"251","materials_type_id":"10","kg_empty":"13","kg_has_gas":"16.2","seri":"56789"}
     */
    public function actionGasRemainUpdate(){
        try{
            $result = ApiModule::$defaultResponse;
            $this->checkRequest();
            $q = json_decode($_POST['q']);
            $this->checkRequiredParams($q, array('token', 'id','customer_id', 'materials_id', 'date_input', 'seri', 'kg_empty', 'kg_has_gas'));
            $mUser      = $this->getUserByToken($result, $q->token);
            
            $mGasRemain                     = new GasRemain('ApiUpdate');
            $mGasRemain->id                 = $q->id;
            $mGasRemain                     = $mGasRemain->getModelApp();
            if(is_null($mGasRemain)){
                throw new Exception('Phiếu gas dư yêu cầu không hợp lệ');
            }
            $mGasRemain->mCustomer          = Users::model()->findByPk($q->customer_id);
            $mGasRemain->handlePost($q);
            
            if($mGasRemain->hasErrors()){
                $result['message'] = HandleLabel::FortmatErrorsModel($mGasRemain->getErrors());
                ApiModule::sendResponse($result, $this);
            }
            
            $mGasRemain->handleSaveUpdate($q);
            $mGasRemain->mAppUserLogin      = $mUser;
            $result             = ApiModule::$defaultSuccessResponse;
            $result['message']  = 'Cập nhật thành công';
            $result['record']   = $mGasRemain->appView();
            
            ApiModule::sendResponse($result, $this);
        } catch (Exception $ex) {
            MyFormat::ApiCatchError($ex, $this);
        }
    }
    
    /**
     * @Author: DungNT Now 15, 2017
     * @Todo: thủ kho get list seri để cân lại ở Xưởng PT
     * @Resource: boMoi/gasRemainGetSeriReweighed
     * @ex_json_request: {"token":"90a5ae30b55f49aeb84c254f1676275b"}
     */
    public function actionGasRemainGetSeriReweighed()
    {
        try{
            $result = ApiModule::$defaultResponse;
            $this->checkRequest();
            $q = json_decode($_POST['q']);
            $this->checkRequiredParams($q, array('token'));
            $mUser      = $this->getUserByToken($result, $q->token);
            $agent_id   = $this->getAgentOfEmployee($mUser);
            
            $mGasRemain                     = new GasRemain();
            $mGasRemain->mAppUserLogin      = $mUser;
            $mGasRemain->agent_id           = $agent_id;
            $mGasRemain->car_id             = isset($q->car_id) ? $q->car_id : 0;
            $result             = ApiModule::$defaultSuccessResponse;
            $result['message']  = 'ok';
            $result['record']   = $mGasRemain->getJsonSeriApp();
            
            ApiModule::sendResponse($result, $this);
        } catch (Exception $ex) {
            MyFormat::ApiCatchError($ex, $this);
        }
    }
    
    /**
     * @Author: DungNT Jun 05, 2018
     * @Todo: PVKH get list gas dư chờ xuất để xuất cho xe tải, hoặc 1 đại lý khác trên hệ thống
     * @Resource: boMoi/gasRemainGetItemWaitExport
     * @ex_json_request: {"token":"90a5ae30b55f49aeb84c254f1676275b","customer_id":123}
     */
    public function actionGasRemainGetItemWaitExport(){
        try{
            $result = ApiModule::$defaultResponse;
            $this->checkRequest();
            $q = json_decode($_POST['q']);
            $this->checkRequiredParams($q, array('token'));
            $mUser      = $this->getUserByToken($result, $q->token);
            $agent_id   = $this->getAgentOfEmployee($mUser);
            $mCustomer  = Users::model()->findByPk($q->customer_id);
            if($mCustomer->role_id != ROLE_AGENT){
                $result['record']   = null;
                ApiModule::sendResponse($result, $this);
            }
            
            $mGasRemain                     = new GasRemain();
            $mGasRemain->mAppUserLogin      = $mUser;
            $mGasRemain->agent_id           = $agent_id;
            $mGasRemain->gas_remain_type    = GasRemain::APP_EXPORT_TO_AGENT;
            $mGasRemain->mCustomer          = $mCustomer;
            if($mUser->role_id == ROLE_DRIVER){
                $mGasRemain->driver_id      = $mUser->id;
            }
            $result                         = ApiModule::$defaultSuccessResponse;
            $result['message']              = 'ok';
            $result['record']['list_item']              = $mGasRemain->getJsonItemWaitExport();
            $result['record']['list_user_of_agent']     = HandleLabel::JsonIdName($mGasRemain->getListdataUserOfAgent()); 
            
            ApiModule::sendResponse($result, $this);
        } catch (Exception $ex) {
            MyFormat::ApiCatchError($ex, $this);
        }
    }
    
    /** @Author: HOANG NAM 24/07/2018
     *  @Todo: get list BoMoi
     *  @Resource: boMoi/stockList
     *  @ex_json_request: {"token":"31c52d8b392104ad22208664e86d1105","page":"0"}
     **/
    public function actionStockList(){
        try{
            $result = ApiModule::$defaultResponse;
            $this->checkRequest();
            $q = json_decode($_POST['q']);
            $this->checkRequiredParams($q, array('token', 'page'));
            $mUser = $this->getUserByToken($result, $q->token);// Oct 12, 2015 for check inactive or delete User
            $agent_id   = $this->getAgentOfEmployee($mUser);
            $this->mUserApp = $mUser;
//            $this->checkVersionCode($q, $mUser);
            if($mUser->hasErrors()){
                $result['message'] = HandleLabel::FortmatErrorsModel($mUser->getErrors());
                ApiModule::sendResponse($result, $this);
            }
            $result = ApiModule::$defaultSuccessResponse;
            $result['message'] = 'List';
            $mAppOrder = new GasAppOrder();
            $mAppOrder->mAppUserLogin   = $mUser;
            $mAppOrder->agent_id        = $agent_id;
            $mAppOrder->apiStockList    = GasAppOrder::API_STOCK;
//            $mAppOrder->handleApiStockList($result, $q, $mUser);
            $mAppOrder->handleApiList($result, $q);
            ApiModule::sendResponse($result, $this);
        } catch (Exception $ex) {
            MyFormat::ApiCatchError($ex, $this);
        }
    }
    
    /** @Author: HOANG NAM 24/07/2018
     *  @Todo: create/update stock
     *  @Resource: boMoi/stockUpdate
     *  @ex_json_request: {"token":"31c52d8b392104ad22208664e86d1105","app_order_id":"1173","record":[{"seri":"3","materials_id":"883","materials_type_id":"1"},{"seri":"2","materials_id":"883","materials_type_id":"1"}]}
     **/
    public function actionStockUpdate(){
        try{
            $result = ApiModule::$defaultResponse;
            $this->checkRequest();
            $q = json_decode($_POST['q']);
            $this->checkRequiredParams($q, array('token', 'record','app_order_id'));
            $mUser = $this->getUserByToken($result, $q->token);// Oct 12, 2015 for check inactive or delete User
            $this->mUserApp = $mUser;
    //            $this->checkVersionCode($q, $mUser);
            if($mUser->hasErrors()){
                $result['message'] = HandleLabel::FortmatErrorsModel($mUser->getErrors());
                ApiModule::sendResponse($result, $this);
            }
            $mStock  = new Stock('apiUpdate');
            $mStock->mAppUserLogin = $mUser;
            $mStock->app_order_id = $q->app_order_id;
            if(!is_array($q->record)){
                $result['message'] = 'Record must be array';
                ApiModule::sendResponse($result, $this);
            }
            $result = ApiModule::$defaultSuccessResponse;
            $mStock->handleSave($q->record);
            $result['record'] = $mStock->getListStock();
            ApiModule::sendResponse($result, $this);
        } catch (Exception $ex) {
            MyFormat::ApiCatchError($ex, $this);
        }
    }
    
    /** @Author: HOANG NAM 24/07/2018
     *  @Todo: view stock
     *  @Resource: boMoi/stockView
     *  @ex_json_request: {"token":"31c52d8b392104ad22208664e86d1105","app_order_id":"1173"}
     **/
    public function actionStockView(){
        try{
            $result                     = ApiModule::$defaultResponse;
            $this->checkRequest();
            $q                          = json_decode($_POST['q']);
            $this->checkRequiredParams($q, array('token','app_order_id'));
            $mUser                      = $this->getUserByToken($result, $q->token);// Oct 12, 2015 for check inactive or delete User
            $this->mUserApp = $mUser;
    //            $this->checkVersionCode($q, $mUser);
            if($mUser->hasErrors()){
                $result['message'] = HandleLabel::FortmatErrorsModel($mUser->getErrors());
                ApiModule::sendResponse($result, $this);
            }
            $result = ApiModule::$defaultSuccessResponse;
            $mStock                     = new Stock('search');
//            get list stock
            $mStock->app_order_id       = $q->app_order_id;
            $mStock->viewFromList       = true;
            $mStock->mAppUserLogin      = $mUser;
            $result['record']           = $mStock->getListStock();
            ApiModule::sendResponse($result, $this);
        } catch (Exception $ex) {
            MyFormat::ApiCatchError($ex, $this);
        }
    }
    
    /** @Author: HOANG NAM 26/07/2018
     *  @Todo: view stock real
     *  @Resource: boMoi/stockRealView
     *  @ex_json_request: {"token":"31c52d8b392104ad22208664e86d1105","app_order_id":"1173"}
     **/
    public function actionStockRealView(){
        try{
            $result                     = ApiModule::$defaultResponse;
            $this->checkRequest();
            $q                          = json_decode($_POST['q']);
            $this->checkRequiredParams($q, array('token','app_order_id'));
            $mUser                      = $this->getUserByToken($result, $q->token);// Oct 12, 2015 for check inactive or delete User
            $this->mUserApp = $mUser;
    //            $this->checkVersionCode($q, $mUser);
            if($mUser->hasErrors()){
                $result['message'] = HandleLabel::FortmatErrorsModel($mUser->getErrors());
                ApiModule::sendResponse($result, $this);
            }
            $result = ApiModule::$defaultSuccessResponse;
            $mStock                     = new Stock('search');
//            get list stock
            $mStock->app_order_id       = $q->app_order_id;
            $mStock->mAppUserLogin      = $mUser;
            $mStock->viewConfirm        = true;
            $result['record']           = $mStock->getListStock(true);
            ApiModule::sendResponse($result, $this);
        } catch (Exception $ex) {
            MyFormat::ApiCatchError($ex, $this);
        }
    }
    
    /** @Author: HOANG NAM 26/07/2018
     *  @Todo: update stock real, tài xế nhập STT thực tế giao cho KH
     *  @Resource: boMoi/stockRealUpdate
     *  @ex_json_request: {"token":"31c52d8b392104ad22208664e86d1105","app_order_id":"1173","record":[{"id":"148","seri_real":"45"},{"id":"149","seri_real":"11"}]}
     **/
    public function actionStockRealUpdate(){
        try{
            $result = ApiModule::$defaultResponse;
            $this->checkRequest();
            $q = json_decode($_POST['q']);
            $this->checkRequiredParams($q, array('token', 'record','app_order_id'));
            $mUser = $this->getUserByToken($result, $q->token);// Oct 12, 2015 for check inactive or delete User
            $this->mUserApp = $mUser;
    //            $this->checkVersionCode($q, $mUser);
            if($mUser->hasErrors()){
                $result['message'] = HandleLabel::FortmatErrorsModel($mUser->getErrors());
                ApiModule::sendResponse($result, $this);
            }
            $mStock  = new Stock('apiUpdate');
            $mStock->mAppUserLogin = $mUser;
            $mStock->app_order_id = $q->app_order_id;
            if(!is_array($q->record) || count($q->record) < 1 ){
                $result['message'] = 'Dữ liệu không hợp lệ, không thể cập nhật STT';
                ApiModule::sendResponse($result, $this);
            }
            if(!$mStock->canUpdateSttReal()){
                throw new Exception('Không thể cập nhật STT, đơn hàng đã bị khóa');
            }
            $mStock->handleSaveReal($q->record);
            if($mStock->hasErrors()){
                $result['message']  = HandleLabel::FortmatErrorsModel($mStock->getErrors());
                ApiModule::sendResponse($result, $this);
            }
            $result             = ApiModule::$defaultSuccessResponse;
            $result['record']   = $mStock->getListStock(true);
            ApiModule::sendResponse($result, $this);
        } catch (Exception $ex) {
            MyFormat::ApiCatchError($ex, $this);
        }
    }
}
