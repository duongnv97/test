<?php
/**
 * @Author: DungNT Jul 15, 2019
 * @Todo: Handle action with Viettel Pay
 */
class AiViettelPayController extends ApiController
{
    public $nameParamCode = 'user_code', $mViettelPay = null;
    
    /** @Author: DungNT Jul 15, 2019
     *  @Todo: lấy danh sách các khoản phải thu của 1 NV ở 1 ĐL
     *  @Resource: aiViettelPay/getListAgentBill
     * * @ex_json_request: $_POST['user_code']
     */
    public function actionGetListAgentBill(){
        try {
            $result = ApiModule::$defaultResponse;
            $this->checkRequestVTPay();
            $mUser          = $this->loadModel($_POST[$this->nameParamCode]);
            $this->mUserApp = $mUser;
            $mViettelPay    = new ViettelPay();
            $result = ApiModule::$defaultSuccessResponse;
            $result['record'] = $mViettelPay->getListAgentBill($mUser);
            ApiModule::sendResponse($result, $this);
        } catch (Exception $ex) {
            MyFormat::ApiCatchError($ex, $this);
        }
    }
    
    /** @Author: DungNT Jul 15, 2019
     *  @Todo: 2/ Khi NV thanh toán xong 1 khoản thì VTpay gửi request sang cho bên SPJ, bao gồm 1 số param custom ở request1: vd agent_id, user_id 
     *  @Resource: aiViettelPay/agentBillDone
     */
    public function actionAgentBillDone(){
        
    }
    

    /** @Author: DungNT Jul 15, 2019
     *  @Todo: check request of VTPay
     **/
    public function checkRequestVTPay() {
        try {
            if(!isset($_POST[$this->nameParamCode])){
                Yii::log("VT Pay yêu cầu không hợp lệ user_code");
                throw new CHttpException(404, "VT Pay yêu cầu không hợp lệ user_code");
            }
        } catch (Exception $exc) {
            GasCheck::CatchAllExeptiong($exc);
        }
    }
    
    /** @Author: DungNT Jul 15, 2019
     *  @Todo: load model User
     **/
    public function loadModel($user_code) {
        try {
            $mViettelPay = new ViettelPay();
            $mUser = $mViettelPay->loadByUserCode($user_code);
            if ($mUser === null) {
                Yii::log("VT Pay yêu cầu không hợp lệ user_code = $user_code");
                throw new CHttpException(404, "VT Pay yêu cầu không hợp lệ user_code = $user_code");
            }
            return $mUser;
        } catch (Exception $exc) {
            GasCheck::CatchAllExeptiong($exc);
        }
    }
    
    

}
