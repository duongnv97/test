<?php
/** @Author: NGUYEN KHANH TOAN 2019
*  @Todo: 
*  @Param:
**/
class RewardSetupController extends ApiController{
    
    /** @Author: NGUYEN KHANH TOAN Mar 07 2018
     *  @Todo: get list reward setup
     *  @Resource: rewardSetup/listRewardSetup
     *  @ex_json_request: {"token":"f3a678185e63438199eff591ae7e22de","page":"0"}
     **/
     public function actionListRewardSetup() {
        try {
            $result = ApiModule::$defaultResponse;
            $this->checkRequest();
            $q = json_decode($_POST['q']);
            $this->checkRequiredParams($q, array('token', 'page'));
            $_GET['platform']       = isset($q->platform) ? $q->platform : UsersTokens::PLATFORM_ANDROID;
            $_GET['version_code']   = isset($q->version_code) ? $q->version_code : 1;
            $mUser = $this->getUserByToken($result, $q->token);// Oct 12, 2015 for check inactive or delete User
            $this->mUserApp = $mUser;
            $result = ApiModule::$defaultSuccessResponse;
            $result['message'] = 'List';
            $mRewardSetup = new RewardSetup();
            $mRewardSetup->mAppUserLogin = $mUser;
            $mRewardSetup->handleApiListRewardSetup($result, $q);
            ApiModule::sendResponse($result, $this);
        } catch (Exception $ex) {
            MyFormat::ApiCatchError($ex, $this);
        }
    }
    
}
