<?php

class UserController extends ApiController
{
    /** 
     * @Author: ANH DUNG Dec 15, 2015
     * @Todo: app dashboard
     * @Resource: user/dashboard
     * @word_index: 2.4
     * @ex_json_request: {"token":"403acbe7932b3c7f2f3ce5a906757c29"}
     * 98985e3ae4f0b83b46ac9b8fbbb00e4d
     {"token":"5929ac7c5461d768aebf0afdc052e5c6"}
     */    
    public function actionDashboard()
    {
        try {
            $result = ApiModule::$defaultResponse;
            $this->checkRequest();
            $q = json_decode($_POST['q']);
            $this->checkRequiredParams($q, array('token'));
            $mUser = $this->getUserByToken($result, $q->token);// Oct 12, 2015 for check inactive or delete User
            $this->HandleDashboard($mUser);

        } catch (Exception $ex) {
            MyFormat::ApiCatchError($ex, $this);
        }
    }
    
    
    /** 
     * @Author: ANH DUNG Dec 20, 2015
     * @Todo: app show info user profile
     * @Resource: user/profile
     * @word_index: 2.5.1
     * @ex_json_request: {"token":"dc36cc6fb2db91e515372db1821b6275"}
     */    
    public function actionProfile()
    {
        try {
            $result = ApiModule::$defaultResponse;
            $this->checkRequest();
            $q = json_decode($_POST['q']);
            $this->checkRequiredParams($q, array('token'));
            $mUser = $this->getUserByToken($result, $q->token);// Oct 12, 2015 for check inactive or delete User
            $this->HandleProfile($mUser);
            
        } catch (Exception $ex) {
            MyFormat::ApiCatchError($ex, $this);
        }
    }
    
    /**
     * @Author: ANH DUNG Dec 20, 2015
     * @Todo: handle profile page
     * @Param: $mUser model
     */
    public function HandleProfile($mUser) {
        $agent_name = $code_account = '';
        if($mUser->role_id != ROLE_CUSTOMER){
            $code_account = "Mã NV: $mUser->code_account - ";
        }
        $mUser->LoadUsersRefImageSign();
        // 1. build array respone
        $result = ApiModule::$defaultSuccessResponse;
        $result['message'] = Yii::t('systemmsg','success');
        $result['record'] = array(
            'first_name'    => $code_account . $mUser->getFullName(),
            'phone'         => $this->getPhoneProfile($mUser),
            'address'       => $mUser->getAddressApp(),
            'image_avatar'  => ImageProcessing::bindImageByModel($mUser->mUsersRef,'','',array('size'=>'size2')),
//            'image_avatar'  => 'http://android.huongminhgroup.com/upload/temp_image/giaonhan.png',
            'customer_info' => $mUser->apiGetInfoCustomer(),

            'email'         => $mUser->email,
            'province_id'   => $mUser->province_id,
            'district_id'   => $mUser->district_id,
            'ward_id'       => $mUser->ward_id,
            'street_id'     => $mUser->street_id,
            'house_numbers' => $mUser->house_numbers,
            'agent_id'      => $this->getCurrentAgentOfUser($mUser, $agent_name),
            'agent_name'    => $agent_name,

            // có nên view image_avatar 1 hình không? hay 2 hình (1 to, 1 nhỏ) click thì view phóng to lên
        );
        ApiModule::sendResponse($result, $this);
    }
    
    public function getPhoneProfile($mUser) {// Jul 19, 2017
        $res = $mUser->getPhone();
        if($mUser->role_id == ROLE_CUSTOMER && $mUser->is_maintain == UsersExtend::STORE_CARD_HGD_APP){
            return $mUser->username;
        }
        if($mUser->role_id == ROLE_CUSTOMER){
            $mUser->loadExtPhone2();
            $res = $mUser->phone_ext2;
        }
        return $res;
    }
    
    /**
     * @Author: ANH DUNG Apr 14, 2017
     * @Todo: lấy đại lý hiện tại mà User đang được gắn vào
     * $aAgent format Array
        (
            [114] => 114
        )
     */
    public function getCurrentAgentOfUser($mUser, &$agent_name) {
        $agent_name = $mUser->getParent();
        return $mUser->parent_id;// Jul1319 DungNT change cách lấy agent name + agent_id trong app profile
        /*
        $aRollAllow = [ROLE_EMPLOYEE_MAINTAIN, ROLE_EMPLOYEE_MARKET_DEVELOPMENT];
        if(!in_array($mUser->role_id, $aRollAllow)){
            return ;
        }
        $agent_id = 0;
        $aCacheListdataAgent = AppCache::getAgentListdata();
        $aAgent = Users::GetKeyInfo($mUser, UsersExtend::JSON_ARRAY_AGENT);
        foreach($aAgent as $item){
            $agent_id = $item;
            break;
        }
        if(isset($aCacheListdataAgent[$agent_id])){
            $agent_name = $aCacheListdataAgent[$agent_id];
        }
        return $agent_id;// chỉ lấy agent đầu tiên, mà 1 NV Giao Nhận cũng chỉ thuộc 1 agent trong 1 thời điểm nhất định 
        */
    }
    
    /** 
     * @Author: ANH DUNG Dec 05, 2015
     * @Todo: app change password user
     * @Resource: user/changePass
     * @word_index: 2.5.1
     * @ex_json_request: {"token":"75722ace2885dd847906cddaf6bb6f65","old_password":"178793","new_password":"123456","new_password_confirm":"123456"}
     */    
    public function actionChangePass()
    {
        try {
            $result = ApiModule::$defaultResponse;
            $this->checkRequest();
            $q = json_decode($_POST['q']);
            $this->checkRequiredParams($q, array('token','old_password','new_password','new_password_confirm'));
            $mUser = $this->getUserByToken($result, $q->token);// Oct 12, 2015 for check inactive or delete User
            $error = $mUser->validateChangePass($q->old_password, $q->new_password, $q->new_password_confirm);
            if(!empty($error)){
                $result['message'] = $error;
                ApiModule::sendResponse($result, $this);
            }
            $this->handleChangePass($mUser, $q);
            
        } catch (Exception $ex) {
            MyFormat::ApiCatchError($ex, $this);
        }
    }
    
    /**
     * @Author: ANH DUNG Dec 05, 2015
     * @Todo: handle change pass user
     * @Param: $mUser model
     */
    public function handleChangePass($mUser, $q) {
        // 1. build array respone
        $mUser->handleChangePass($q->new_password);
        $result = ApiModule::$defaultSuccessResponse;
        $result['message'] = 'Đổi mật khẩu thành công';
        ApiModule::sendResponse($result, $this);
    }
    
    /**
     * @Author: ANH DUNG Oct 06, 2016
     * @Todo: Tạo Bảo báo cáo công việc của NV ngoài văn phòng
     * @Resource: user/workingReportCreate
     * @word_index: 3.11.1 Method: POST 
     * @ex_json_request: {"token":"dc36cc6fb2db91e515372db1821b6275","content":"tieu de","latitude":"12","longitude":"21"}
     */
    public function actionWorkingReportCreate()
    {
        try{
        $result = ApiModule::$defaultResponse;
        $this->checkRequest();
        $q = json_decode($_POST['q']);
        $this->checkRequiredParams($q, array('token'));        
        // 1. get user by token
        $mUser = $this->getUserByToken($result, $q->token);// Oct 12, 2015 for check inactive or delete User
        // Check empty customer
        if(empty($q->user_report) && empty($q->user_id)){
            $result['message'] = 'Chưa nhập thông tin khách hàng';
            ApiModule::sendResponse($result, $this);
        }
        // 2. get post and validate
        $mWorkReport = new WorkReport("ApiCreate");
        $mWorkReport->apiGetPost($result, $q, $mUser, $this);
        // 3. and save
        $result = ApiModule::$defaultSuccessResponse;
        $result['message'] = 'Tạo mới báo cáo công việc thành công';
        $result['id'] = $mWorkReport->id."";
        ApiModule::sendResponse($result, $this);
        } catch (Exception $ex) {
            MyFormat::ApiCatchError($ex, $this);
        }
    }
    
    /**
     * @Author: ANH DUNG Oct 06, 2016
     * @Todo: Tạo Bảo báo cáo công việc của NV ngoài văn phòng
     * @Resource: user/workingReportView
     * @word_index: 3.11.1 Method: POST 
     * @ex_json_request: {"token":"dc36cc6fb2db91e515372db1821b6275","id":"123"}
     */
    public function actionWorkingReportView()
    {
        try{
        $result = ApiModule::$defaultResponse;
        $this->checkRequest();
        $q = json_decode($_POST['q']);
        $this->checkRequiredParams($q, array('token', 'id'));
        // 1. get user by token
        $mUser = $this->getUserByToken($result, $q->token);// Oct 12, 2015 for check inactive or delete User
        // 2. get view by id
        $mWorkReport = WorkReport::model()->findByPk($q->id);
        if(is_null($mWorkReport)){
            throw new Exception('Invalid request');
        }
        $result = ApiModule::$defaultSuccessResponse;
        $result['message'] = 'Xem báo cáo công việc thành công';
        $result['model_record'] = $mWorkReport->apiFormatRecord($q, $mUser);
        ApiModule::sendResponse($result, $this);
        } catch (Exception $ex) {
            MyFormat::ApiCatchError($ex, $this);
        }
    }
    
    
    /**
     * @Author: ANH DUNG Oct 06, 2016
     * @Todo: list Xem báo cáo công việc
     * @Resource: user/workingReportList
     * @word_index: 3.11.2 Method: POST 
     * @ex_json_request: {"token":"dc36cc6fb2db91e515372db1821b6275","page":"0","type":"1"}
     */
    public function actionWorkingReportList()
    {
        try{
        $result = ApiModule::$defaultResponse;
        $this->checkRequest();
        $q = json_decode($_POST['q']);
        $this->checkRequiredParams($q, array('token', 'page'));
        $mUser = $this->getUserByToken($result, $q->token);// Oct 12, 2015 for check inactive or delete User
        $result = ApiModule::$defaultSuccessResponse;
        $result['message'] = 'Success';
        $this->handleWorkingReportList($result, $q, $mUser);
        ApiModule::sendResponse($result, $this);
        } catch (Exception $ex) {
            MyFormat::ApiCatchError($ex, $this);
        }
    }
    
    public function handleWorkingReportList(&$result, $q, $mUser) {
        $mWorkReport = new WorkReport();
        // 1. get list order by user id
        $dataProvider = $mWorkReport->apiListing($q, $mUser);
        $models = $dataProvider->data;
        $CPagination = $dataProvider->pagination;
        $result['total_record'] = $CPagination->itemCount;
        $result['total_page'] = $CPagination->pageCount;
        if( $q->page >= $CPagination->pageCount ){
            $result['record'] = array();
        }else{
            foreach($models as $mWorkReport){
                $result['record'][] = $mWorkReport->apiFormatRecord($q, $mUser);
            }
        }
    }
    
    
    /** 
     * @Author: ANH DUNG Dec 08, 2016
     * @Todo: app change info user profile
     * @Resource: user/changeProfile
     * @word_index: 2.5.1
     * @ex_json_request: {"token":"dc36cc6fb2db91e515372db1821b6275"}
     */    
    public function actionChangeProfile()
    {
        try {
            $result = ApiModule::$defaultResponse;
            $this->checkRequest();
            $q = json_decode($_POST['q']);
            $this->checkRequiredParams($q, array('token', 'first_name'));
            $mUser = $this->getUserByToken($result, $q->token);// Oct 12, 2015 for check inactive or delete User
            if(UsersTokens::isCustomerGas24h($mUser)){// không cho KH bò mối + NV chỉnh sửa thông tin
                $this->handleChangeProfile($result, $mUser, $q);// Dec 08, 2016 chỉ cho phép customer của Gas24h chỉnh sửa tên + địa chỉ
            }
            if($this->allowChangeAgent($mUser)){
                $this->handleChangeAgent($mUser, $q);
            }
            
            $result = ApiModule::$defaultSuccessResponse;
            $result['message'] = 'Success';
            ApiModule::sendResponse($result, $this);
            
        } catch (Exception $ex) {
            MyFormat::ApiCatchError($ex, $this);
        }
    }
    
    public function handleChangeProfile($result, $mUser, $q) {
        $mMyFormat      = new MyFormat();
        $maxLengthName  = 30;
        $mUser->scenario        = 'AppGas24hUpdateProfile';
        
        $mUser->first_name       = trim($q->first_name);
        $mUser->email            = trim($q->email);
        $mUser->province_id      = trim($q->province_id);
        $mUser->district_id      = trim($q->district_id);
        $mUser->ward_id          = trim($q->ward_id);
        $mUser->street_id        = trim($q->street_id);
        $mUser->house_numbers    = trim($q->house_numbers);
        $mUser->validate();
        if(!$mMyFormat->isValidLength($mUser->first_name, $maxLengthName)){
            $mUser->addError('first_name', "Họ tên không được quá $maxLengthName ký tự, vui lòng đặt tên ngắn hơn");
        }
        if($mUser->hasErrors()){
            $result['message'] = HandleLabel::FortmatErrorsModel($mUser->getErrors());
            ApiModule::sendResponse($result, $this);
        }
        $aUpdate = array('first_name', 'email', 'province_id', 'district_id', 'ward_id', 'street_id', 'house_numbers', 'address_vi', 'address');
        $mUser->update($aUpdate);
        $mUser->solrAdd();
    }
    
    /**
     * @Author: ANH DUNG Apr 14, 2017
     * @Todo: NV GN Change Agent on App
     */
    public function handleChangeAgent($mUser, $q) {
        if(empty($q->agent_id)){
            return ;// Apr 15, 2017 chỗ này gây bug lớn, nếu agent_id=0 thì nó xóa hết type ONE_AGENT_MAINTAIN
        }
        $mTransHistory = new TransactionHistory();
        $mTransHistory->employee_maintain_id    = $mUser->id;
        $mTransHistory->agent_id                = $q->agent_id;
        $mTransHistory->changeAgentEmployee();
    }
    // Apr 14, 17 check user can change agent ?
    public function allowChangeAgent($mUser) {
//        return false; // Aug1817 cấm hết, không cho đổi Agent trên app :d
//        return true; // Jun 18, 2017 cho phép all NVGN change agent
        $aRollAllow = [ROLE_EMPLOYEE_MAINTAIN];
        if(in_array($mUser->role_id, $aRollAllow) && in_array($mUser->id, UsersExtend::getUidAllowChangeAgent())){
            return true;
        }
        return false;
    }
    
    /** 
     * @Author: ANH DUNG Apr 20, 2017
     * @Todo: get các menu report với các loại user
     * @Resource: user/reportList
     {"token":"5929ac7c5461d768aebf0afdc052e5c6"}
     */    
    public function actionReportList()
    {
        try {
            $result = ApiModule::$defaultResponse;
            $this->checkRequest();
            $q = json_decode($_POST['q']);
            $this->checkRequiredParams($q, array('token'));
            $mUser = $this->getUserByToken($result, $q->token);// Oct 12, 2015 for check inactive or delete User
            $this->reportListGetData($mUser);

        } catch (Exception $ex) {
            MyFormat::ApiCatchError($ex, $this);
        }
    }
    
    public function reportListGetData($mUser) {
        $aRoleAllow = GasConst::getRoleAppReport();
        if(!in_array($mUser->role_id, $aRoleAllow)){
            return [];
        }
        $aRes = [];
        if($mUser->role_id == ROLE_EMPLOYEE_MAINTAIN){
        }
        $aRes[] = [ 'id'=>'report_inventory', 'name'=>'Tồn kho'];
        $aRes[] = [ 'id'=>'report_hgd', 'name'=>'Hộ gia đình'];
        $aRes[] = [ 'id'=>'report_cashbook', 'name'=>'Quỹ tiền mặt'];
        $result = ApiModule::$defaultSuccessResponse;
        $result['message']  = 'L';
        $result['record']   = $aRes;
        ApiModule::sendResponse($result, $this);
    }
    
    /** 
     * @Author: ANH DUNG Jul 26, 2018
     * @Todo: ghi nhận chấm công cho user 
     * @Resource: user/handUp
     * @ex_json_request: ?
     */    
    public function actionHandUp()
    {
        try {
            $result = ApiModule::$defaultResponse;
            $get    = isset($_GET) ? MyFormat::jsonEncode($_GET) : '';
            $post   = isset($_POST) ? MyFormat::jsonEncode($_POST) : '';
            Logger::WriteLog("<hr><br>user/handUp ------------ GET ------------<br>$get");
            Logger::WriteLog("<hr><br>user/handUp ------------ POST ------------<br>$post");
            die;
        } catch (Exception $ex) {
            MyFormat::ApiCatchError($ex, $this);
        }
    }
    
    /** @Author: PHAM THANH NGHIA Oct 10, 2018
     *  @Resource: user/infoProfile
     *  @ex_json_request: {"token":"c6c6487a84ae9e96d128840c91e2806b", "code_account":"NNNN0001"}
     */
    public function actionInfoProfile()
    {
        try {
        $result = ApiModule::$defaultResponse;
        $this->checkRequest();
        $q = json_decode($_POST['q']);
        $this->checkRequiredParams($q, array('token','code_account'));
        $_GET['platform'] = isset($q->platform) ? $q->platform : UsersTokens::PLATFORM_ANDROID;
        $mUser = $this->getUserByToken($result, $q->token);// Oct 12, 2015 for check inactive or delete User
        $this->mUserApp = $mUser;
        $result = ApiModule::$defaultSuccessResponse;
        $result['message'] = 'view';
        $mUsersExtend = new UsersExtend();
        $mUsersExtend->mAppUserLogin = $mUser;
        $mUsersExtend->handleApiInfoProfile($result, $q);
        ApiModule::sendResponse($result, $this);
        } catch (Exception $ex) {
            MyFormat::ApiCatchError($ex, $this);
        }
    }
}
