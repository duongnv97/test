<?php
/** @Author: NGUYEN KHANH TOAN  Mar 12 2019
*  @Todo: 
*  @Param:
**/
class ExchangeRewardController extends ApiController
{
     /** @Author: NGUYEN KHANH TOAN Mar 07 2018
     *  @Todo: get list history exchange
     *  @Resource: exchangeReward/historyExchange
     *  @ex_json_request: {"token":"f3a678185e63438199eff591ae7e22de","page":"0","customer_id":"868408"}
     **/
     public function actionHistoryExchange() {
        try {
            $result = ApiModule::$defaultResponse;
            $this->checkRequest();
            $q = json_decode($_POST['q']);
            $this->checkRequiredParams($q, array('token','page','customer_id'));
            $_GET['platform']       = isset($q->platform) ? $q->platform : UsersTokens::PLATFORM_ANDROID;
            $_GET['version_code']   = isset($q->version_code) ? $q->version_code : 1;
            $mUser = $this->getUserByToken($result, $q->token);// Oct 12, 2015 for check inactive or delete User
            $this->mUserApp = $mUser;
            $result = ApiModule::$defaultSuccessResponse;
            $result['message'] = 'List';
            $mCodePartner = new CodePartner();
            $mCodePartner->mAppUserLogin = $mUser;
            $mCodePartner->handleApiListExchangeReward($result, $q);
            ApiModule::sendResponse($result, $this);
        } catch (Exception $ex) {
            MyFormat::ApiCatchError($ex, $this);
        }
    }
    
     /** @Author: NGUYEN KHANH TOAN Mar 12 2019
     *  @Todo: exchange point
     *  @Resource: exchangeReward/ExchangePoint
     *  @ex_json_request: {"token":"f3a678185e63438199eff591ae7e22de","customer_id":"1700427","reward_id":"2"}
     **/
    public function actionExchangePoint(){
        try {
            $result = ApiModule::$defaultResponse;
            $this->checkRequest();
            $q = json_decode($_POST['q']);
            $this->checkRequiredParams($q, array('token','customer_id','reward_id'));
            $_GET['platform']       = isset($q->platform) ? $q->platform : UsersTokens::PLATFORM_ANDROID;
            $_GET['version_code']   = isset($q->version_code) ? $q->version_code : 1;
            $mUser = $this->getUserByToken($result, $q->token);// Oct 12, 2015 for check inactive or delete User
            $this->mUserApp = $mUser;
            $result = ApiModule::$defaultSuccessResponse;
            $mCodePartner = new CodePartner();
            $mCodePartner->mAppUserLogin = $mUser;
            $mCodePartner->handleApiExchangePoint($result, $q);
            ApiModule::sendResponse($result, $this);
        } catch (Exception $ex) {
            MyFormat::ApiCatchError($ex, $this);
        }
    }

    
    
}
