<?php
/**
 * @Author: DungNT Jan 29, 2018
 * @Todo: Handle some action app Gas24h
 */
class Gas24hController extends ApiController
{
    /**
     * @Author: DungNT Jan 29, 2018
     * @Todo: render news list at home app Gas24h
     * @Resource: gas24h/newsList
     * * @ex_json_request: {"token":"c3f3832926eeb9a2410ff7bf8d93b944","page":"0"}
     */
    public function actionNewsList()
    {
        try {
            $result = ApiModule::$defaultResponse;
            $this->checkRequest();
            $q = json_decode($_POST['q']);
            $this->checkRequiredParams($q, array('token', 'page'));
            $_GET['platform']       = isset($q->platform) ? $q->platform : UsersTokens::PLATFORM_ANDROID;
            $_GET['version_code']   = isset($q->version_code) ? $q->version_code : 1;

            $mUser = $this->getUserByToken($result, $q->token);// Oct 12, 2015 for check inactive or delete User
            $this->mUserApp = $mUser;
            $result = ApiModule::$defaultSuccessResponse;
            $result['message'] = 'List';
            $mAppPromotion = new AppPromotion();
            $mAppPromotion->mAppUserLogin   = $mUser;
            $mAppPromotion->appAction       = GasConst::ACTION_LIST;
            $mAppPromotion->dirMenuList     = isset($q->menu_list) ? $q->menu_list : 0;
            $cacheData = $mAppPromotion->getGas24hCacheNewsList();

//            $mAppPromotion->handleApiList($result, $q);
//            $mAppPromotion->setGas24hCacheNewsList($result); 
//            Logger::WriteLog("Cache ". json_encode($result));
            if(empty($cacheData)){
                $mAppPromotion->handleApiList($result, $q);
                $this->devDebugNews($mUser, $result);
                $mAppPromotion->setGas24hCacheNewsList($result);
            }else{
                $result = json_decode($cacheData, true);
            }
            
            ApiModule::sendResponse($result, $this);
        } catch (Exception $ex) {
            MyFormat::ApiCatchError($ex, $this);
        }
    }
    
    /** @Author: DungNT Dec 23, 2018
     **/
    public function devDebugNews($mUser, $result) {
        foreach($result['record'] as $item){
            Logger::WriteLog("UserId {$mUser->id} debug AppGas24h news : ". MyFunctionCustom::remove_vietnamese_accents($item['title']));
        }
            
    }

    /** @Author: DungNT Jan 29, 2018
     *  @Todo: render news popup at home app Gas24h
     *  @Resource: gas24h/newsPopup
     *  @ex_json_request: {"token":"c3f3832926eeb9a2410ff7bf8d93b944"}
     */
    public function actionNewsPopup()
    {
        try {
        $result = ApiModule::$defaultResponse;
        $this->checkRequest();
        $q = json_decode($_POST['q']);
        $this->checkRequiredParams($q, array('token'));
        $_GET['platform'] = isset($q->platform) ? $q->platform : UsersTokens::PLATFORM_ANDROID;
        $mUser = $this->getUserByToken($result, $q->token);// Oct 12, 2015 for check inactive or delete User
        $this->mUserApp = $mUser;
        $result = ApiModule::$defaultSuccessResponse;
        $result['message'] = 'popup';
        $mAppPromotion                  = new AppPromotion();
        $mAppPromotion->mAppUserLogin   = $mUser;
        $result['record']               = $mAppPromotion->formatNewsPopup();
        ApiModule::sendResponse($result, $this);
        } catch (Exception $ex) {
            MyFormat::ApiCatchError($ex, $this);
        }
    }
    
    /** @Author: DungNT Apr 03, 2018
     *  @Todo: render view news app Gas24h
     *  @Resource: gas24h/newsView
     *  @ex_json_request: {"token":"c3f3832926eeb9a2410ff7bf8d93b944", "id":1}
     */
    public function actionNewsView()
    {
        try {
        $result = ApiModule::$defaultResponse;
        $this->checkRequest();
        $q = json_decode($_POST['q']);
        $this->checkRequiredParams($q, array('token','id'));
        $_GET['platform'] = isset($q->platform) ? $q->platform : UsersTokens::PLATFORM_ANDROID;
        $mUser = $this->getUserByToken($result, $q->token);// Oct 12, 2015 for check inactive or delete User
        $this->mUserApp = $mUser;
        $result = ApiModule::$defaultSuccessResponse;
        $result['message'] = 'view';
        
        $mAppPromotion                  = new AppPromotion();
        $mAppPromotion->id              = $q->id;
        $mAppPromotion->mAppUserLogin   = $mUser;
        $this->writeTracking($mAppPromotion, StaView::TYPE_1_APP_NEWS);

//        $mAppPromotion = $mAppPromotion->getNewsView();
//        if(is_null($mAppPromotion)){ 
//            throw new Exception('Yêu cầu không hợp lệ');
//        }
//        $mAppPromotion->setGas24hCacheNews();// for dev test
        $result['record']               = $mAppPromotion->getGas24hCacheNews();
        ApiModule::sendResponse($result, $this);
        } catch (Exception $ex) {
            MyFormat::ApiCatchError($ex, $this);
        }
    }
    
    /** @Author: DungNT Jul 17, 2018
     *  @Todo: test web view ios
     *  @Resource: gas24h/newsViewTest
     *  @ex_json_request: {"token":"c3f3832926eeb9a2410ff7bf8d93b944", "id":1}
     */
    public function actionNewsViewTest()
    {
        try {
        $mAppPromotion                  = new AppPromotion();
        $mAppPromotion->id              = 192;
        $mAppPromotion->setGas24hCacheNews();
        $aData = $mAppPromotion->getGas24hCacheNews();
        echo $aData['news_content'];
        die;
        ApiModule::sendResponse($result, $this);
        } catch (Exception $ex) {
            MyFormat::ApiCatchError($ex, $this);
        }
    }

}
