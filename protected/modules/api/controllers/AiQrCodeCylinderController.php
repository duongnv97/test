<?php
/**
 * @Author: DungNT May 22, 2019
 * @Todo: Handle action TruckPlan
 */
class AiQrCodeCylinderController extends ApiController
{
    /** @Author: DungNT Jun 09, 2019
     * @Todo: handle scan QR CODE when filling Gas to cylinder at warehouse
     * @Resource: aiQrCodeCylinder/warehouseFilling
     * * @ex_json_request: {"token":"b0ba131a5ee86324cac63ce6b84ef667","page":"0", "customer_id":"","date_from":"01-05-2019","date_to":"01-06-2019"}
     */
    public function actionWarehouseFilling(){
        try {
            $result = ApiModule::$defaultResponse;
            $this->checkRequest();
            $q = json_decode($_POST['q']);
            $this->checkRequiredParams($q, array('token', 'weight_of_empty_cylinder', 'weight_before_filling', 'weight_after_filling', 'filling_no', 'cylinder_number'));
            $mUser = $this->getUserByToken($result, $q->token);// Oct 12, 2015 for check inactive or delete User
            $this->mUserApp = $mUser;
//            ApiModule::sendResponse($result, $this);
            $agent_id           = $this->getAgentOfEmployee($mUser);
            Logger::WriteLog("agent_id = $agent_id q = ".$_POST['q']);
            $result             = ApiModule::$defaultSuccessResponse;
            $result['message']  = "Lưu thành công ".$q->cylinder_number;
            ApiModule::sendResponse($result, $this);
        } catch (Exception $ex) {
            MyFormat::ApiCatchError($ex, $this);
        }
    }
    

}
