<?php
/**
 * @author bb qbao <quocbao1087@gmail.com>
 * @copyright (c) 2013, Quoc Bao
 */
class ApiModule extends CWebModule
{
    public static $defaultResponse = array('status'=>0,
                                    'code'=>400,
                                    'message'=>'Invalid request. Please check your http verb, action url and param'
                                    );
    public static $defaultSuccessResponse = array('status'=>1,
                                    'code'=>5335,
                                    'message'=>'Success'
                                    );
    public static $_CODE_LOGOUT             = 1987; // Anh Dũng Oct 23, 2015 xử lý member bị Delete hay Inactive
    public static $_CODE_LOST_CONNECTION    = 1988; // lost connection
    public static $_CODE_UNKNOWN            = 1989;
    public static $_CODE_OLD_VERSION        = 1990; // required update app
    public static function setDefaultSuccessResponse($result = NULL)
    {
        if(!$result)
            return self::$defaultSuccessResponse;
        else
        {
            $arr = self::$defaultSuccessResponse;
            foreach ($result as $k=>$v)
            {
                if(isset($arr[$k]))
                {
                    $result[$k] = self::$defaultSuccessResponse[$k];
                }
            }
            return $result;
        }
    }
    public static function setDefaultUnSuccessResponse($result = NULL)
    {
        if(!$result)
            return self::$defaultResponse;
        else
        {
            $arr = self::$defaultResponse;
            foreach ($result as $k=>$v)
            {
                if(isset($arr[$k]))
                {
                    $result[$k] = self::$defaultResponse[$k];
                }
            }
            return $result;
        }
    }
    
    public function init()
    {
        // Set up CHttpException Processing action
        Yii::app()->errorHandler->errorAction = "api/default/error";
        Yii::app()->language = 'vi';
        // import the module-level models and components
        $this->setImport(array(
                'api.models.*',
                'api.components.*',
        ));
    }

    public function beforeControllerAction($controller, $action)
    {
        return parent::beforeControllerAction($controller, $action);
    }

    public static function json_encode_unicode($data) {
        if (defined('JSON_UNESCAPED_UNICODE')) {
            return json_encode($data, JSON_UNESCAPED_UNICODE);
        }
        return preg_replace_callback('/(?<!\\\\)\\\\u([0-9a-f]{4})/i',
            function ($m) {
                $d = pack("H*", $m[1]);
                $r = mb_convert_encoding($d, "UTF8", "UTF-16BE");
                return $r!=="?" && $r!=="" ? $r : $m[0];
            }, json_encode($data)
        );
    }

    /** api Output data output json Format.
     * @param: $data is array response
     * @param: $objController object controller tương ứng
    */
    public static function sendResponse($data, $objController)
    {
        // $str = "無效請求";
        // $data = array("msg"=>$str);
        echo $json = self::json_encode_unicode($data);
//        $json = var_export($data, true);
        $objController->updateLogResponse($json);// Anh Dũng Oct 12, 2015, trick để ghi thêm log response
        exit;
    }

}
