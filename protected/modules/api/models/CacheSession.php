<?php
class CacheSession
{
    const CURRENT_USER_EXT = 'COOKIE_USER_EXT';

     /** @Author: DungNT Now 10, 2016
      * Xử lý cache some info của các model vào session để get ra ở list
     */
    public static function getListAgent() {
        $session = Yii::app()->session;
        if(!isset($session['CACHE_LIST_AGENT'])){
            $session['CACHE_LIST_AGENT'] = self::getListAgentData();
        }
        return $session['CACHE_LIST_AGENT'];
    }
    
    public static function getListAgentData() {
        $aRes   = array();
        $models = Users::getArrObjectUserByRole(ROLE_AGENT, array(), array());
        foreach($models as $model){
            $tmp = array();
            $tmp['first_name']      = $model->first_name;
            $tmp['code_account']    = $model->code_account;
            $tmp['code_bussiness']  = $model->code_bussiness;
            $aRes[$model->id] = $tmp;
        }
        return $aRes;
    }

    public static function getAgentName($agent_id) {
        $aAgent = self::getListAgent();
        return isset($aAgent[$agent_id]) ? $aAgent[$agent_id]['first_name'] : '';
    }
    
    /**
     * @Author: DungNT Now 10, 2016
      * Xử lý cache some info của model material vào session để get ra ở list
     */
    public static function getListMaterial() {
        $session = Yii::app()->session;
        if(!isset($session['CACHE_LIST_MATERIAL'])){
            $session['CACHE_LIST_MATERIAL'] = self::getListMaterialData();
        }
        return $session['CACHE_LIST_MATERIAL'];
    }
    public static function getListMaterialData() {
        $criteria = new CDbCriteria();
        $criteria->addCondition('t.parent_id<>0');
//        $criteria->addCondition('t.status=' . STATUS_ACTIVE);
        $models = GasMaterials::model()->findAll($criteria);
        return CHtml::listData($models,'id','name');
    }
    
    public static function getArrayModelMaterial() {
        $session = Yii::app()->session;
        if(!isset($session['CACHE_ARRAY_MODEL_MATERIAL'])){
            $session['CACHE_ARRAY_MODEL_MATERIAL'] = self::setArrayModelMaterial();
        }
        return $session['CACHE_ARRAY_MODEL_MATERIAL'];
    }
        public static function setArrayModelMaterial() {
        $criteria = new CDbCriteria();
        $criteria->addCondition('t.parent_id<>0');
//        $criteria->addCondition('t.status=' . STATUS_ACTIVE);
        $models = GasMaterials::model()->findAll($criteria);
        $aRes = array();
        foreach($models as $model){
            $aRes[$model->id] = $model->getAttributes();
        }
        return $aRes;
    }
    
    /**
     * @Author: DungNT Now 27, 2016
      * Xử lý cache some info của material json vào session để get ra ở list
     */
    public static function getListMaterialJson() {
        $session = Yii::app()->session;
        if(!isset($session['CACHE_MATERIAL_JSON'])){
//            $session['CACHE_MATERIAL_JSON'] = MyFunctionCustom::getMaterialsJson(array('GetAll'=>1));
            $session['CACHE_MATERIAL_JSON'] = MyFunctionCustom::getMaterialsJson(['GetThietBi'=>1]);// Jun 26, 2017 tắt vật tư inactive đi
        }
        return $session['CACHE_MATERIAL_JSON'];
    }
    public static function getListMaterialJsonGas() {
        $session = Yii::app()->session;
        if(!isset($session['CACHE_MATERIAL_GAS_VT'])){
            $session['CACHE_MATERIAL_GAS_VT'] = MyFunctionCustom::getMaterialsJsonByType(GasMaterialsType::$ARR_WINDOW_GAS);
        }
        return $session['CACHE_MATERIAL_GAS_VT'];
    }
    public static function getListMaterialJsonVo() {
        $session = Yii::app()->session;
        if(!isset($session['CACHE_MATERIAL_VO'])){
            $session['CACHE_MATERIAL_VO'] = MyFunctionCustom::getMaterialsJsonByType(GasMaterialsType::$ARR_WINDOW_VO);
        }
        return $session['CACHE_MATERIAL_VO'];
    }

    /**
     * @Author: DungNT Mar 11, 2017
     * @Todo: set cookie
     */
    public static function setCookie($name, $value, $expire='') {
        if(empty($expire)){
            $expire = time() + 1000 * 24 * 60 * 60;// 1000 day
        }
//        Yii::app()->request->cookies[$name] = new CHttpCookie($name, $value);
        if(GasCheck::isLocalhost()){
            setcookie($name, $value, $expire);
        }elseif(GasCheck::isServerAndroid()){
            setcookie($name, $value, $expire, '/', 'spj.vn');// Now1017
        }else{
            setcookie($name, $value, $expire, '/', 'daukhimiennam.com');// Mar 12, 2017 test ok for live
        }
        
    }
    public static function getCookie($name) {
        return isset($_COOKIE[$name]) ? $_COOKIE[$name] : '';
    }
    
    /**
     * @Author: DungNT Apr 05, 2017
     * @Todo: get session Price giá KH bò mối tháng hiện tại
     */
    public static function getPriceBoMoi() {
        $mGasPrice =new GasPrice();
        $session = Yii::app()->session;
        if(!isset($session['CACHE_PRICE_BOMOI'])){
            $session['CACHE_PRICE_BOMOI'] = $mGasPrice->getArrayCodeGByMonthYear(date('m'), date('Y'));
        }
        return $session['CACHE_PRICE_BOMOI'];
    }
    
    /**
     * @Author: DungNT Apr 09, 2017
     * @Todo: get session array NPP
     */
    public static function getDistributor() {
        $session = Yii::app()->session;
        if(!isset($session['CACHE_USERS_DISTRIBUTOR'])){
            $aDistributor = UsersExtend::getListDistributor();
            $aRes = [];
            foreach($aDistributor as $item){
                $aRes[$item->id] = $item->getAttributes();
            }
            $session['CACHE_USERS_DISTRIBUTOR'] = $aRes;
        }
        return $session['CACHE_USERS_DISTRIBUTOR'];
    }
    
    /**
     * @Author: DungNT May 22, 2017
     * @Todo: get session model user - ok test get relation
     */
    public static function getModelUser($user_id) {
        $session    = Yii::app()->session;
        $name       = 'CACHE_LIST_M_USER';
        $mCacheUser = null;
        if(!isset($session[$name])){
            $mUser = Users::model()->findByPk($user_id);
            $aRes = [];
            $aRes[$mUser->id]   = $mUser;
            $mCacheUser         = $mUser;
            $session[$name]     = $aRes;
        }else{
            $oldData = $session[$name];
            if(!isset($session[$name][$user_id])){
                $mUser = Users::model()->findByPk($user_id);
                $oldData[$mUser->id]   = $mUser;
                $mCacheUser = $mUser;
                $session[$name]     = $oldData;
            }else{
                $mCacheUser = $session[$name][$user_id];
//                Logger::WriteLog('get from session '.time());
            }
        }
        return $mCacheUser;
    }
    
    public static function getListdataByRole($role_id) {
        $session    = Yii::app()->session;
        if(is_array($role_id)){
            $sParamsIn = implode(',',  $role_id);
            $c = "t.role_id IN ($sParamsIn)";
            $roleKey = implode('_', $role_id);
        }else{
            $c          = 't.role_id='.$role_id;
            $roleKey    =  $role_id;
        }
        $key   = 'SessionListdataUserRole_'.$roleKey;
        if(!isset($session[$key])){
            $criteria = new CDbCriteria();
            $criteria->addCondition($c. ' AND t.status='.STATUS_ACTIVE);
            $models = Users::model()->findAll($criteria);
            $session[$key] = CHtml::listData($models, 'id', 'first_name');
        }
        return $session[$key];
    }
    
    /** @Author: DungNT Sep 17, 2017
      * Xử lý cache các agent đã tắt vào session để check
     */
    public static function getListAgentInactive() {
        $session = Yii::app()->session;
        if(!isset($session['CACHE_LIST_AGENT_INACTIVE'])){
            $session['CACHE_LIST_AGENT_INACTIVE'] = CHtml::listData(CacheSession::findAgentInactive(), 'id', 'id');
        }
        return $session['CACHE_LIST_AGENT_INACTIVE'];
    }
    public static function findAgentInactive() {
        $criteria = new CDbCriteria();
        $criteria->addCondition('t.role_id='. ROLE_AGENT. ' AND t.status='.STATUS_INACTIVE);
        return Users::model()->findAll($criteria);
    }
    /** @Author: DungNT Now 02, 2018
      * Xử lý cache các id telesale
     */
    public static function getListIdTelesale() {
        $session = Yii::app()->session;
        if(!isset($session['CACHE_LIST_TELESALE_ID'])){
            $mAppCache = new AppCache();
            $listdata = $mAppCache->getListdataUserByRole(ROLE_TELESALE);
            $aData = [];
            foreach($listdata as $user_id => $name){
                $aData[$user_id] = $user_id;
            }
            $session['CACHE_LIST_TELESALE_ID'] = $aData;
        }
        return $session['CACHE_LIST_TELESALE_ID'];
    }
    
    // get list data id => name Callcenter + dieu phoi
    public static function getListdataCallcenter() {
        $session = Yii::app()->session;
        if(!isset($session['CACHE_LIST_DATA_CALLCENTER'])){
            $mAppCache = new AppCache();
            $l1     = $mAppCache->getListdataUserByRole(ROLE_CALL_CENTER);
            $l2     = $mAppCache->getListdataUserByRole(ROLE_DIEU_PHOI);
            $session['CACHE_LIST_DATA_CALLCENTER'] = $l1 + $l2;
        }
        return $session['CACHE_LIST_DATA_CALLCENTER'];
    }
    
}