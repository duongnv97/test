<?php
/**
 * @Author: DungNT Jul 15, 2019
 * @Todo: Handle action with Viettel Pay
 */
class ViettelPay
{
    
    /** @Author: DungNT Jul 15, 2019
     *  @Todo: find model User by code - with pay salary yes
     **/
    public function loadByUserCode($user_code) {
        $criteria = new CDbCriteria();
        $criteria->addCondition('role_id <> '.ROLE_CUSTOMER );
        $criteria->addCondition('t.payment_day='. UsersProfile::SALARY_YES);
        $criteria->compare('code_account', $user_code);
        return Users::model()->find($criteria);
    }
    
    /** @Author: DungNT Jul 15, 2019
     *  @Todo: find model User by code - with pay salary yes
     **/
    public function getListAgentBill($mUser) {
        $mCashBookDetail = new GasCashBookDetail();
        $aData = $mCashBookDetail->getClosingByAgent($mUser->id);
        $aRes = [];
        if(!isset($aData[$mUser->id])){
            return $aRes;
        }
        foreach($aData[$mUser->id] as $agent_id => $aInfo):
            $temp = [];
            $temp['user_id']        = $mUser->id;
            $temp['agent_id']       = $agent_id;
            $temp['description']    = 'Nộp tiền '.$aInfo['agentName'];
            $temp['amount']         = $aInfo['closing'];
            $temp['amount_text']    = ActiveRecord::formatCurrency($aInfo['closing']);
            $aRes[] = $temp;
        endforeach;
        return $aRes;
    }
    
}