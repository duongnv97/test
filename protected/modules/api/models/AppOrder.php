<?php

class AppOrder
{
    public $app_type = 0, $Gas24hApplyPrice = 0, $Gas24hPrice = 60000, $agent_id = 0 ;// is UsersTokens::APP_TYPE_GAS_SERVICE
    public $aModelGas24h = [], $aModelMaterias = [], $aIdGas = [], $minPriceApp = 100000;
    
    /** @Author: DungNT May 09, 2018
     *  @Todo: danh sách agent chạn  không cho đặt app
     **/
    public function getListAgentLockApp() {
        return [
//            1161356,// Jun0718 Đại Lý An Hiệp
            1,// agent test
        ];
    }
     /**
     * @Author: DungNT Oct 19, 2016
     * có thể xử lý chỗ này save dạng json vào setting rồi get từ setting ra trả về cho nhanh
     * 1. get all agent
     * 2. get all vật tư bán + giá bán dc setup cho từng vùng (cache)
     * 3. get all hàng KM
     */
    public function handleGetConfig() {
        $this->app_type         = isset($_GET['app_type']) ? $_GET['app_type'] : UsersTokens::APP_TYPE_GAS_SERVICE;
        $this->agent_id         = isset($_GET['agent_id']) ? trim($_GET['agent_id']) : 0;// Xử lý cho ios lấy info của 1 agent khi ở home
        $this->Gas24hApplyPrice = Yii::app()->params['Gas24hApplyPrice'];
        $mMaterialTmp           =  new GasMaterials();// xử lý gọi 1 lần getAppPrice, không nên đưa vào vòng for
        $this->Gas24hPrice      = $mMaterialTmp->getAppPrice();
        $cacheKey               = AppCache::APP_CONFIG_AGENT;
        if($this->app_type == UsersTokens::APP_TYPE_GAS24H){
            $cacheKey           = AppCache::APP_CONFIG_AGENT_GAS_24;
        }
//        $aProvinceGas24 = [GasProvince::TINH_VUNG_TAU, GasProvince::TINH_KHANH_HOA];
        $aProvinceGas24 = [];// chặn app ở các tỉnh khác
        $this->aIdGas           = $mMaterialTmp->getIdApp();
        $mAppCache = new AppCache();
//        if($mAppCache->getCache(AppCache::APP_CONFIG_AGENT)){
//            return $mAppCache->getCacheDecode(AppCache::APP_CONFIG_AGENT);
//        }
//        Sta2::setCacheInventoryAllAgent();// only open when change data, không nên mở vì nó sẽ chạy thống kê nhiều
        $aRes           = [];
        $aAgent         = Users::getArrObjectUserByRole(ROLE_AGENT, [], array('gender'=>Users::IS_AGENT));
        $aInventory     = $mAppCache->getCacheInventoryAllAgent(AppCache::APP_AGENT_INVENTORY);
        $aDataCache     = $mAppCache->getPriceHgd(AppCache::PRICE_HGD);
        $this->aModelMaterias   = $mMaterialTmp->getAllActive();
        $this->aModelGas24h     = $this->getModelMaterial([], true);
        
        foreach($aAgent as $mAgent){
            $oneAgentConfig = $tmp = [];
//            if(empty($mAgent->slug) || (!empty($this->agent_id) && $this->agent_id != $mAgent->id )){
//                continue;
//            }
//            if($this->app_type == UsersTokens::APP_TYPE_GAS24H && !in_array($mAgent->province_id, $aProvinceGas24)){
//                continue;
//            }// Close on Mar1618, allow all agent book app
//            if($this->app_type == UsersTokens::APP_TYPE_GAS24H && in_array($mAgent->id, UsersExtend::getAgentAnhHieu())){
//                continue;
//            }// Close on Mar1618, allow all agent book app
            if(!$this->isAgentValid($mAgent, $aProvinceGas24)){
                continue;
            }

            $priceHgd       = MyFunctionCustom::getHgdPriceSetup($aDataCache, $mAgent->id);
            $location       = explode(',', $mAgent->slug);
            $tmp['agent_id']            = $mAgent->id;
            $tmp['agent_name_spj']      = $mAgent->getFullName();
            $tmp['agent_name']          = 'Gas24h';// Change on Apr2718 cho map hệ thống đại lý trên app Gas24h
            $tmp['agent_phone']         = $mAgent->getAgentLandline();
            $tmp['agent_cell_phone']    = $mAgent->getPhone();
            $tmp['agent_phone_support'] = GasConst::PHONE_HGD;
            $tmp['agent_address']       = $mAgent->address;
            $tmp['agent_latitude']      = $location[0];
            $tmp['agent_longitude']     = $location[1];
            $tmp['agent_radius']        = $mAgent->price_other;
            $oneAgentConfig['info_agent'] = $tmp;
            
            $zone_id        = MyFunctionCustom::getZoneId(0, array('province_id' => $mAgent->province_id));
            $aMaterialId    = $this->getInventoryMaterialFromCache($aInventory, $mAgent->id, GasMaterialsType::MATERIAL_BINH_12KG);
            $aModelMaterial = $this->getModelMaterial($aMaterialId);
            $oneAgentConfig['info_gas'] = $this->formatMaterial($mAgent, $zone_id, $priceHgd, $aModelMaterial);
            
            $aMaterialId    = $this->getInventoryMaterialFromCache($aInventory, $mAgent->id, GasMaterialsType::MATERIAL_TYPE_PROMOTION);
            $aModelMaterial = $this->getModelMaterial($aMaterialId);
            $oneAgentConfig['info_promotion'] = $this->formatMaterial($mAgent, $zone_id, $priceHgd, $aModelMaterial);
            $aRes[] = $oneAgentConfig;
        }
        if(empty($this->agent_id)){
            $mAppCache = new AppCache();
            // Aug2719 DungNT tạm close lại, do chưa apply cách dùng biến này
//            $mAppCache->setCache($cacheKey, json_encode($aRes, JSON_UNESCAPED_UNICODE));
        }
        return $aRes;
    }
    
    /** @Author: DungNT Mar 20, 2018
     *  @Todo: check 1 số điều kiện agent có hợp lệ không
     *  @return: true hợp lệ, false không hợp lệ
     **/
    public function isAgentValid($mAgent, $aProvinceGas24) {
        $ok = true;
        // 1. chưa có tọa độ, hoặc không phải agent selected 
        if(empty($mAgent->slug) || (!empty($this->agent_id) && $this->agent_id != $mAgent->id )){
            $ok = false;
        }
        if($this->app_type == UsersTokens::APP_TYPE_GAS24H && in_array($mAgent->province_id, $aProvinceGas24)){
            $ok = false;
        }// Close on Mar1618, allow all agent book app
        // 2. nếu là app gas24h mà không phải các tỉnh đc chạy thì ko đc
//        if($this->app_type == UsersTokens::APP_TYPE_GAS24H && !in_array($mAgent->province_id, $aProvinceGas24)){
//            $ok = false;
//        }
        // 3. close các agent anh Hiếu
//        if($this->app_type == UsersTokens::APP_TYPE_GAS24H && in_array($mAgent->id, UsersExtend::getAgentAnhHieu())){
//            $ok = false;
//        }// Mar2018 Kiên để KV anh Hiếu vẫn chạy app
        return $ok;
    }
            
    /**
     * @Author: DungNT Oct 19, 2016
     * @Todo: xử lý format array vật tư
     */
    public function formatMaterial($mAgent, $zone_id, $priceHgd, $aModelMaterial) {
        if($this->app_type == UsersTokens::APP_TYPE_GAS_SERVICE){
            return $this->formatMaterialGasService($mAgent, $zone_id, $priceHgd, $aModelMaterial);
        }elseif($this->app_type == UsersTokens::APP_TYPE_GAS24H){
            return $this->formatMaterialGas24h($mAgent, $zone_id, $priceHgd, $aModelMaterial);
        }
        return [];
    }
    public function formatMaterialGasService($mAgent, $zone_id, $priceHgd, $aModelMaterial) {
        $aRes   = [];
        foreach($aModelMaterial as $mMaterial){
            $tmp    = [];
            $price  = isset($priceHgd[$mMaterial->id]) ? $priceHgd[$mMaterial->id] : 0;
            $tmp['materials_id']        = $mMaterial->id;
            $tmp['materials_type_id']   = $mMaterial->materials_type_id;
            $tmp['materials_name']      = $mMaterial->getName();
            $tmp['materials_name_short'] = $mMaterial->getName();
            $tmp['material_price']      = ActiveRecord::formatCurrency($price);
            $tmp['price']               = $price.'';
            $tmp['material_image']      = $mMaterial->getImageUrl();
            $tmp['price_root']          = $tmp['price'];
            $aRes[] = $tmp;
        }
        return $aRes;
    }
    public function formatMaterialGas24h($mAgent, $zone_id, $priceHgd, $aModelMaterial) {
        $aRes   = [];
        $mSell  = $this->initModelSell($mAgent);

        foreach($aModelMaterial as $mMaterial){// load tồn thực tế của đại lý
//        foreach($this->aModelGas24h as $mMaterial){// close Apr2018 load 5 bình fix cứng 
            if($mMaterial->materials_type_id != GasMaterialsType::MATERIAL_BINH_12KG){
                continue;
            }
            $tmp = [];
            $tmp['materials_id']        = $mMaterial->id;
            $tmp['materials_type_id']   = $mMaterial->materials_type_id;
            $tmp['materials_name']      = $mMaterial->getName();
            $tmp['materials_name_short'] = $mMaterial->getNameShort();
            if(empty($tmp['materials_name_short'])){
                $tmp['materials_name_short'] = $mMaterial->getName();
            }
            $price      = isset($priceHgd[$mMaterial->id]) ? $priceHgd[$mMaterial->id] : 0;
            $price_root = $price;
            if($this->Gas24hApplyPrice == 'yes' && $mMaterial->materials_type_id == GasMaterialsType::MATERIAL_BINH_12KG){
                $mSellDetail = new SellDetail();
                $mSellDetail->agent_id          = $mAgent->id;
                $mSellDetail->materials_type_id = $mMaterial->materials_type_id;
                $mSellDetail->price             = $price;
                $mSellDetail->price_root        = $price;
                $mSell->calcPriceItemGasApp($mSellDetail);

                $price = $mSellDetail->price;
                $this->writeLogError($price, $mMaterial->id);
            }
//            if(!in_array($mMaterial->id, $this->aIdGas)){// Close Apr0118, lấy tồn kho
////            if($mMaterial->materials_type_id != GasMaterialsType::MATERIAL_BINH_12KG){
//                continue ;
//            }
            if($price_root == $price || empty($price_root)){
                $tmp['price_root'] = '0';// không hiển thị giá gốc gạch ngang trên app nếu không giảm giá
            }else{
                $tmp['price_root'] = ActiveRecord::formatCurrency($price_root);
            }
            
            $tmp['material_price']      = ActiveRecord::formatCurrency($price);
            $tmp['price']               = $price.'';
            $tmp['material_image']      = $mMaterial->getImageUrl();
            $aRes[] = $tmp;
        }
        return $aRes;
    }
    
    public function initModelSell($mAgent) {
        $mSell = new Sell();
        $mSell->agent_id            = $mAgent->id;
        $mSell->province_id         = $mAgent->province_id;
        $mSell->setDiscountGas24h();
        $mSell->source              = Sell::SOURCE_APP;
        return $mSell;
    }
    
    public function writeLogError(&$price, $materials_id) {
//        if($price <= $this->minPriceApp){// có thể KH ở vùng mà đại lý không có loại gas này nên price sẽ = 0
//            Logger::WriteLog($price.' - Giá vật tư app Gas24h sai: id '.$materials_id);
//        }
        $price = ($price > $this->minPriceApp) ? $price : 999000;// kiểm tra lại giá chắc chắn không nhỏ hơn 0
    }
    
    /**
     * @Author: DungNT Nov 06, 2016
     * @Todo: lấy những vật tư mà đại lý có tồn kho từ cache
     */
    public function getInventoryMaterialFromCache($aInventory, $agent_id, $materials_type_id) {
        if(!isset($aInventory[$agent_id][$materials_type_id])){
            return [];//không thể close đc vì sẽ gây Bug => 
        }// Close on Jun 08, 2017 cho load hết xuống, vì có đại lý không search được vật tư
        $aRes = [];
        foreach($aInventory[$agent_id][$materials_type_id] as $materials_id => $qty){
            $aRes[] = $materials_id;
        }
        return $aRes;
    }
    
    
    /** @Author: DungNT Jan 02, 2018
     *  @Todo: get all model materials 
     * xử lý không truy vấn vào db để lấy model trong vòng foreach Agent nữa -> nhiểu query vào db quá
     * lấy hết model đưa vào biến, khi nào cần lấy model nào thì sẽ for để so sánh
     **/
    public function getModelMaterial($aMaterialId, $isGas24h = false) {
        $aRes = [];
        if($isGas24h){
            foreach ($this->aModelMaterias as $id => $mMaterial){
                if(in_array($id, $this->aIdGas)){// Close Apr0118, lấy tồn kho
                    $aRes[] = $mMaterial;
                }
            }
        }else{
            foreach ($this->aModelMaterias as $id => $mMaterial){
                if(in_array($id, $aMaterialId)){
                    $aRes[] = $mMaterial;
                }
            }
        }
        return $aRes;
    }

}