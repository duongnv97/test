<?php
class AppCache
{
    public $isSyncCacheFile = true; // flag đồng bộ file by Cron, true is insert row CronTask
    
    const APP_CONFIG_AGENT          = 'APP_CONFIG_AGENT';
    const APP_CONFIG_AGENT_GAS_24   = 'APP_CONFIG_AGENT_GAS_24';
    const APP_AGENT_INVENTORY       = 'APP_AGENT_INVENTORY';
    
    const ARR_MODEL_AGENT           = 'ARR_MODEL_AGENT';// Oct 26, 2016 cache list model agent
    const ARR_MODEL_PROVINCE        = 'ARR_MODEL_PROVINCE';// for getMasterModel
    const ARR_MODEL_DISTRICT        = 'ARR_MODEL_DISTRICT';// for getMasterModel
    const ARR_MODEL_WARD            = 'ARR_MODEL_WARD';// for getMasterModel
    const ARR_MODEL_STREET          = 'ARR_MODEL_STREET';// for getMasterModel
    const ARR_MODEL_MATERIAL        = 'ARR_MODEL_MATERIAL';// getMasterModel
    const ARR_MODEL_MATERIAL_TYPE   = 'ARR_MODEL_MATERIAL_TYPE';// for getMasterModel
    const ARR_MODEL_MATERIAL_GAS    = 'ARR_MODEL_MATERIAL_GAS';// for getMasterModel gas
    const ARR_MODEL_MATERIAL_VO     = 'ARR_MODEL_MATERIAL_VO';// for getMasterModel Vỏ
    const ARR_MODEL_MATERIAL_HGD    = 'ARR_MODEL_MATERIAL_HGD';// for getMasterModel các loại vật tư HGĐ
    const ARR_APP_MATERIAL_STORECARD    = 'ARR_APP_MATERIAL_STORECARD';// for lấy các vật tư thẻ kho active đẩy xuống app
    const ARR_MODEL_LOOKUP              = 'ARR_MODEL_LOOKUP';// for GasMasterLookup
    const ARR_MODEL_MATERIAL_SUPPORT    = 'ARR_MODEL_MATERIAL_SUPPORT';// Aug0918 vật tư thay thế + đầu tư cho KH

    const PRICE_HGD             = 'PRICE_HGD';
    const BU_VO_PRICE           = 'BU_VO_PRICE';
    const ARR_MODEL_GAS12KG     = 'ARR_MODEL_GAS12KG'; 
    
    const LISTDATA_ROLE         = 'LISTDATA_ROLE';// for getListdata id=>name
    const LISTDATA_MATERIAL     = 'LISTDATA_MATERIAL';
    const LISTDATA_MATERIAL_TYPE = 'LISTDATA_MATERIAL_TYPE';
    const LISTDATA_PROVINCE     = 'LISTDATA_PROVINCE';
    const LISTDATA_DISTRICT     = 'LISTDATA_DISTRICT';
    const LISTDATA_STREET       = 'LISTDATA_STREET';
    const LISTDATA_AGENT        = 'LISTDATA_AGENT';// for getListdata id=>name
    const LISTDATA_MATERIAL_TYPE_VO         = 'LISTDATA_MATERIAL_TYPE_VO';
    const LISTDATA_MATERIAL_GAS_VO          = 'LISTDATA_MATERIAL_GAS_VO';
    const LISTDATA_BO_MOI                   = 'LISTDATA_BO_MOI';
    const LISTDATA_BO_MOI_CODE_ACCOUNT      = 'LISTDATA_BO_MOI_CODE_ACCOUNT';
    const LISTDATA_CHUYENVIEN_AGENT         = 'LISTDATA_CHUYENVIEN_AGENT';
    const LISTDATA_KTKV_AGENT               = 'LISTDATA_KTKV_AGENT';

    const ALLOW_IP_ACCOUNTING               = 'ALLOW_IP_ACCOUNTING';// save list IP for allow KE TOAN login
    const QTY_EXPORT_IN_DAY_OF_AGENT        = 'QTY_EXPORT_IN_DAY_OF_AGENT';// Feb 02, 2017 save list agent và những vật tư gas + SL xuất bán trong ngày của agent
    
    const EXT_AGENT                         = 'EXT_AGENT';// Cache agent => ext lại
    const EXT_EMPLOYEE                      = 'EXT_EMPLOYEE';// Cache ext => user login
    const USER_CREDIT                       = 'USER_CREDIT';//
    const GAS24H_AGENT_RADIUS               = 'GAS24H_AGENT_RADIUS';//
    const LISTDATA_CUSTOMER_INVEST          = 'LISTDATA_CUSTOMER_INVEST'; // NghiaPT Nov 16,18 save list customer_id => invest
    const ARR_TREE_TEAM_CCS                 = 'ARR_TREE_TEAM_CCS'; // AnhDung Dec0718 cache tree quản lý của 1 NV ccs
    const LISTDATA_CUSTOMER_DEBIT           = 'LISTDATA_CUSTOMER_DEBIT'; // NghiaPT Dec 10, 18 danh sách khách hàng bò nợ
    const LISTDATA_GAS_ANNOUNCE             = 'LISTDATA_GAS_ANNOUNCE'; //LOC007 locnv Jun 12, 2019 danh sách khuyến mãi
     /**
     * @Author: DungNT Oct 19, 2016
     * có thể xử lý disk cache http://www.yiiframework.com/doc/guide/1.1/en/caching.overview
     * http://www.yiiframework.com/doc/guide/1.1/en/caching.data
     * $timeout: 3600 second  = 1 hours. 0 means never expire.
     */
    public function setCache($id, $value, $timeout = 3600) {
        Yii::app()->cache->set($id, $value, $timeout);
        if($this->isSyncCacheFile && $this->isNeedSync($id)){
            $mCronTask = new CronTask();
            $mCronTask->addTaskSyncAppCache(CronTask::TYPE_SYNC_APPCACHE, $id, 0);
        }
    }
    
    /** @Return: data if true else if FALSE need compare === FALSE
     **/
    public function getCache($id) {
        return Yii::app()->cache->get($id);
    }
    
    public function getCacheDecode($id) {
        return json_decode(Yii::app()->cache->get($id), true);
    }

        
    /** @Author: DungNT Aug 27, 2019
     *  @Todo: get prefix of listdata user By Role
     **/
    public function getPrefixListdataRole() {
        return 'CacheListdataUserRole';
    }
    public function getPrefixUserIdByRole() {
        return 'ArrayIdRole';
    }
    public function getPrefixUserIdMaintainOfAgent() {
        return 'CacheUserMaintainOfAgent';
    }
    public function getPrefixModelUserRole() {
        return 'CacheModelUserRole';
    }
    public function getPrefixCustomerLimitOfAgent() {
        return 'CustomerLimitOfAgent';
    }
    
    
    /**
     * @Author: DungNT Nov 05, 2016
     * @Todo: get array model Province from Cache
     * @param: $className name class model ex: GasMaterials, GasMaterialsType ...
     * @param: $cacheKey key of cache ex: AppCache::ARR_MODEL_MATERIAL , AppCache::ARR_MODEL_MATERIAL_TYPE ...
     */
    public function getMasterModel($className, $cacheKey) {
        $value = $this->getCache($cacheKey);
        if(empty($value)){
            $value = $this->setMasterModel($className, $cacheKey);
        }
//        else{
//            Logger::WriteLog("$className Get From Cache");
//        }
        return json_decode($value, true);
    }
    public function setMasterModel($className, $cacheKey) {
        $model_ = call_user_func(array($className, 'model'));
        $models = $model_->findAll();
        $aRes = array();
        foreach($models as $model){
            $aRes[$model->id] = $model->getAttributes();
        }
        $value = MyFormat::jsonEncode($aRes);
        $this->setCache($cacheKey, $value, 86400);// 24h
        return $value;
    }
    
    
    /**
     * @Author: DungNT Mar 06, 2017
     * @Todo: get array model loại thu chi (cashbook)from Cache
     * @param: $type_lookup name class model GasMasterLookup
     */
    public function getModelTypeLookup($type_lookup, $needMore=[]) {
        $cacheKey = 'ModelTypeLookup'.$type_lookup;
        $value = $this->getCache($cacheKey);
        if(empty($value)){
            $value = $this->setModelTypeLookup($type_lookup, $needMore);
        }
        return json_decode($value, true);
    }
    public function setModelTypeLookup($type_lookup, $needMore) {
        $cacheKey = 'ModelTypeLookup'.$type_lookup;
        $criteria = new CDbCriteria();
        $criteria->addCondition('t.type_lookup='.$type_lookup );
        $criteria->order = 't.type DESC, t.display_order ASC'; 
        $models = GasMasterLookup::model()->findAll($criteria);
        $aRes = array();
        foreach($models as $model){
            $aRes[$model->id] = $model->getAttributes();
        }
        $value = json_encode($aRes, JSON_UNESCAPED_UNICODE);
        $this->setCache($cacheKey, $value, 0);
        return $value;
    }
    
    /**
    * @Author: DungNT Oct 26, 2016
    * @Todo: get model cache of agent
    */
   public function getAgent() {
        $value = $this->getCache(AppCache::ARR_MODEL_AGENT);
        if(empty($value)){
            $value = $this->setAgent();
        }
        return json_decode($value, true);
   }
   public function setAgent() {
        $value = UsersS1::getJsonModelByRole(ROLE_AGENT, ['GetAll'=>1]);
        $this->setCache(AppCache::ARR_MODEL_AGENT, $value, 86400);
        return $value;
   }
   
    /** @Author: DungNT Dec 09, 2016
    *  @Todo: get listdata cache of agent
    */
   public function getAgentListdata($needMore = []) {
        $value = $this->getCache(AppCache::LISTDATA_AGENT);
        if(empty($value)){
            $value = $this->setAgentListdata($needMore);
        }
        return json_decode($value, true);
   }
   public function setAgentListdata($needMore = []) {
        $aParams = ['GetAll'=>1];
        if(isset($needMore['GetActive'])){
            $aParams = [];
        }
        $models     = Users::getArrObjectUserByRole(ROLE_AGENT, [], $aParams);
        $lisdata    = CHtml::listData($models, 'id', 'first_name');
        $value      = MyFormat::jsonEncode($lisdata);
        $this->setCache(AppCache::LISTDATA_AGENT, $value, 86400);
        return $value;
   }
   
    /**
     * @Author: DungNT Nov 05, 2016
     * @Todo: get array model User By Role from Cache
     * @param: $role_id role user
     * @return: format array(id=>array_Attribute)
     */
    public function getUserByRole($role_id, $needMore=[]) {
        $cacheKey   = "{$this->getPrefixModelUserRole()}-$role_id";
        $value      = $this->getCache($cacheKey);
        if(empty($value)){
            $value = $this->setUserByRole($role_id, $needMore);
        }
        return json_decode($value, true);
    }
   
    public function setUserByRole($role_id, $needMore=[]) {
        $cacheKey   = "{$this->getPrefixModelUserRole()}-$role_id";
        $criteria = new CDbCriteria();
        $criteria->addCondition('t.role_id='.$role_id);
        if(isset($needMore['GetActive'])){
            $criteria->addCondition('t.status=1');
        }
        $models = Users::model()->findAll($criteria);
        $aRes = array();
        foreach($models as $model){
            $aRes[$model->id] = $model->getAttributes();
        }
        $value = json_encode($aRes, JSON_UNESCAPED_UNICODE);
        $this->setCache($cacheKey, $value);
        return $value;
    }
    
    /**
     * @Author: DungNT Jan 23, 2017 -- test ok at Jan23, 2017 -- nghia change sep 24 -2018
     * @Todo: get array listdata(id,name) User By Role from Cache
     * @param: $role_id role user
     */
    public function getListdataUserByRole($role_id) {
        $cacheKey   = "{$this->getPrefixListdataRole()}-$role_id";
        $value      = $this->getCache($cacheKey);
        if( empty($value) || $value === []){
            $value = $this->setListdataUserByRole($role_id);
        }else{
        }
        $aRes = json_decode($value, true);
        return is_array($aRes) ? $aRes : [];
    }
   
    public function setListdataUserByRole($role_id) {
        $cacheKey   = "{$this->getPrefixListdataRole()}-$role_id";
        $criteria = new CDbCriteria();
        $criteria->addCondition('t.role_id='.$role_id);
        $models = Users::model()->findAll($criteria);
        $aRes = CHtml::listData($models, 'id', 'first_name');
        $value = json_encode($aRes, JSON_UNESCAPED_UNICODE);
        $this->setCache($cacheKey, $value); // chắc biến này cập nhật liên tục 1h 1 lần
        return $value;
    }
    
    /**
     * @Author: DungNT Feb 27, 2017
     * @Todo: get array list giao nhận id By agent id, những giao nhận của 1 đại lý
     */
    public function getUserMaintainOfAgent($agent_id) {
        $cacheKey   = "{$this->getPrefixUserIdMaintainOfAgent()}-$agent_id";
        $value      = $this->getCache($cacheKey);
        if(empty($value)){
            $value = $this->setUserMaintainOfAgent($agent_id);
        }else{
        }
        return json_decode($value, true);
    }
   
    public function setUserMaintainOfAgent($agent_id) {
        $cacheKey   = "{$this->getPrefixUserIdMaintainOfAgent()}-$agent_id";
        $aRes   = GasOneMany::getArrOfManyIdByType(ONE_AGENT_MAINTAIN, $agent_id);
        $value  = json_encode($aRes, JSON_UNESCAPED_UNICODE);
        $this->setCache($cacheKey, $value, 72000);
        return $value;
    }
    
    /**
     * @Author: DungNT Nov 06, 2016
     * @Todo: get giá bán gas hộ GD của all agent: AppCache::getPriceHgd(AppCache::PRICE_HGD);
     * @note_Jan012017: hiện tại có 3 chỗ lấy giá HGD này: 
     * 1. lấy giá cho PMBH ở window: MyFunctionCustom::getMaterialsJsonByType()
     * 2. ở BH Hộ trên web phục vụ cho create + update Sell: file /admin/views/sell/_form_detail.php
     * 3. ở hàm get config giá của APP GAS24h AppOrder::handleGetConfig()
     * 4. hàm thống kê xuất excel Sta2::OutputAgentData()
     */
    public function getPriceHgd($cacheKey) {
        $value = $this->getCache($cacheKey);
        if(empty($value)){
            $value = $this->setPriceHgd($cacheKey);
//            Logger::WriteLog('DELETE CACHE AFTER 1H - START NEW getPriceHgd');
        }
        else{
//            Logger::WriteLog('Window Login getPriceHgd Get From Cache');
        }
        return json_decode($value, true);
    }
    public function setPriceHgd($cacheKey, $date = '') {
        if(empty($date)){
            $date = date('Y-m-d');
        }
        // Close on Dec 31, 2016 22:00 để chạy setup giá mới
//        $value = json_encode(UsersPrice::getPriceHgd($date), JSON_UNESCAPED_UNICODE);
        $value = json_encode(UsersPriceHgd::getPriceHgd($date), JSON_UNESCAPED_UNICODE);// Dec 30, 2016 for new setup price
        $this->setCache($cacheKey, $value, 0);// Aug0118 fix time expired = 0. không nên set time ngắn 1,2h nữa
        return $value;
    }
    
    /**
     * @Author: DungNT Nov 09, 2016
     * @Todo: get cache inventory
     */
    public function getCacheInventoryAllAgent($cacheKey) {
        $value = $this->getCache($cacheKey);
        if(empty($value)){
            Sta2::setCacheInventoryAllAgent();
        }
//        else{
//            Logger::WriteLog("$className Get From Cache");
//        }
        return $this->getCacheDecode($cacheKey);
    }
    
    /**
    * @Author: DungNT Now 22, 2016
    * @Todo: get cache bù vỏ
    */
   public function getBuVo() {
        $value = $this->getCache(AppCache::BU_VO_PRICE);
        if(empty($value)){
            $value = $this->setBuVo();
        }
//        else{
//            Logger::WriteLog("Call history get Agent From Cache");
//        }
        return json_decode($value, true);
   }
   public function setBuVo() {
        $value = BuVo::buildDataCache();
        $value = json_encode($value, JSON_UNESCAPED_UNICODE);
        $this->setCache(AppCache::BU_VO_PRICE, $value, 86400);
        return $value;
   }
   
   /*** One Block ***/
    /**
     * @Author: DungNT Nov 29, 2016
     * @Todo: get array id=>name from Cache
     * @param: $className name class model ex: GasMaterials, GasMaterialsType ...
     * @param: $cacheKey key of cache ex: GasMaterials => AppCache::LISTDATA_MATERIAL , GasMaterialsType => AppCache::LISTDATA_MATERIAL_TYPE ...
     */
    public function getListdata($className, $cacheKey, $name_field = 'name') {
        $value = $this->getCache($cacheKey);
        if(empty($value)){
            $value = $this->setListdata($className, $cacheKey, $name_field);
        }
        return json_decode($value, true);
    }
    public function setListdata($className, $cacheKey, $name_field) {
        $model_ = call_user_func(array($className, 'model'));
        $aRes = CHtml::listData($model_->findAll(), 'id', $name_field);
        $value = json_encode($aRes, JSON_UNESCAPED_UNICODE);
        $this->setCache($cacheKey, $value);
        return $value;
    }
    /*** One Block ***/
 
    /**
     * @Author: DungNT Dec 14, 2016
     * @Todo: get array model List Gas 12 for HGD
     * @param: $className name class model ex: GasMaterials, GasMaterialsType ...
     * @param: $cacheKey key of cache ex: AppCache::ARR_MODEL_MATERIAL , AppCache::ARR_MODEL_MATERIAL_TYPE ...
     */
    public function getModelGas12Kg() {
        $value = $this->getCache(AppCache::ARR_MODEL_GAS12KG);
        if(empty($value)){
            $value = $this->setModelGas12Kg();
        }
//        else{
//            Logger::WriteLog("$className Get From Cache");
//        }
        return json_decode($value, true);
    }
    public function setModelGas12Kg() {
        $criteria = new CDbCriteria();
        $sParamsIn = implode(',', array(GasMaterialsType::MATERIAL_BINH_12KG));
        $criteria->addCondition("t.materials_type_id IN ($sParamsIn)");
        $models = GasMaterials::model()->findAll($criteria);
        $aRes = array();
        foreach($models as $model){
            $aRes[$model->id] = $model->getAttributes();
        }
        $value = json_encode($aRes, JSON_UNESCAPED_UNICODE);
        $this->setCache(AppCache::ARR_MODEL_GAS12KG, $value, 86400);
        return $value;
    }
    
    
    /**
     * @Author: DungNT Dec 14, 2016
     * @Todo: get array id=>name from của vật tư theo loại vật tư 
     * @param: $aMaterialType is array material type
     * @param: $cacheKey key of cache ex: AppCache::LISTDATA_MATERIAL_TYPE_VO, AppCache::LISTDATA_MATERIAL_GAS_VO ...
     */
    public function getListdataMaterialByType($aMaterialType, $cacheKey, $name_field = 'name') {
        $value = $this->getCache($cacheKey);
        if(empty($value)){
            $value = $this->setListdataMaterialByType($aMaterialType, $cacheKey, $name_field);
        }
        return json_decode($value, true);
    }
    public function setListdataMaterialByType($aMaterialType, $cacheKey, $name_field) {
        $criteria = new CDbCriteria();
        $sParamsIn = implode(',', $aMaterialType);
        $criteria->addCondition('t.parent_id<>0');
        $criteria->addCondition("t.materials_type_id IN ($sParamsIn)");
        $models = GasMaterials::model()->findAll($criteria);
        $aRes = CHtml::listData($models, 'id', $name_field);
        $value = json_encode($aRes, JSON_UNESCAPED_UNICODE);
        $this->setCache($cacheKey, $value, 86400);
        return $value;
    }

    /**
     * @Author: DungNT Feb 10, 2017
     * @Todo: get array obj field custom from của vật tư theo loại vật tư 
     * @param: $aMaterialType name class model ex: GasMaterials, GasMaterialsType ...
     * @param: $cacheKey key of cache ex: AppCache::ARR_MODEL_MATERIAL , AppCache::ARR_MODEL_MATERIAL_TYPE ...
     */
    public function getObjMaterialByType($aMaterialType, $cacheKey, $needMore=[]) {
        $value = $this->getCache($cacheKey);
        if(empty($value)){
            $value = $this->setObjMaterialByType($aMaterialType, $cacheKey, $needMore);
        }
        return json_decode($value, true);
    }
    public function setObjMaterialByType($aMaterialType, $cacheKey, $needMore) {
        $criteria = new CDbCriteria();
        $criteria->addCondition('t.parent_id<>0');
        $sParamsIn = implode(',', $aMaterialType);
        $criteria->addCondition("t.materials_type_id IN ($sParamsIn)");
        if(isset($needMore['GetActive'])){
            $criteria->compare('t.status',STATUS_ACTIVE);
        }
        $models = GasMaterials::model()->findAll($criteria);
        $aRes = [];
        foreach($models as $item){
            $tmp = [];
            $tmp['materials_id']        = $item->id;
            $tmp['materials_type_id']   = $item->materials_type_id;
            $tmp['materials_name']      = $item->name;
            $aRes[] = $tmp;
        }
        $value = json_encode($aRes, JSON_UNESCAPED_UNICODE);
        $this->setCache($cacheKey, $value, 86400);
        return $value;
    }

    /**
     * @Author: DungNT Mar 02, 2017
     * @Todo: get array vật tư thẻ kho của app
     * @param: $aMaterialType name class model ex: GasMaterials, GasMaterialsType ...
     * @param: $cacheKey key of cache ex: AppCache::ARR_MODEL_MATERIAL , AppCache::ARR_MODEL_MATERIAL_TYPE ...
     */
    public function getAppMaterialsStorecard($cacheKey, $needMore=[]) {
        $value = $this->getCache($cacheKey);
        if(empty($value)){
            $value = $this->setAppMaterialsStorecard($cacheKey, $needMore);
        }
        return json_decode($value, true);
    }
    public function setAppMaterialsStorecard($cacheKey, $needMore) {
        $mMaterials = new GasMaterials();
        $aRes = $mMaterials->getMaterialsForStorecard();
        $value = json_encode($aRes, JSON_UNESCAPED_UNICODE);
        $this->setCache($cacheKey, $value, 86400);
        return $value;
    }
    
    /**
     * @Author: DungNT Feb 02, 2017
     * @Todo: set value empty array for any key reset
     */
    public function setEmpty($cacheKey) {
        $value = json_encode([]);
        $this->setCache($cacheKey, $value, 360000);// 360000s = 100 hour
    }
    
    /**
     * @Author: DungNT Mar 11, 2017
     * @Todo: set CacheAgentExt for VOIP
     */
    public function setCacheAgentExt() {
        $criteria = new CDbCriteria();
        $criteria->addCondition('t.role_id=' . ROLE_AGENT);
        $aData  = CHtml::listData(Users::model()->findAll($criteria), 'id', 'username');
        $this->setCache(AppCache::EXT_AGENT, json_encode($aData), 0);
    }
    /**
     * @Author: DungNT Mar 12, 2017
     * @Todo: set setCallCenterExt set NV nào đang ngồi ext nào
     */
    public function setCallCenterExt($ext, $uesr_id) {
        $aCurrentExt = $this->getCacheDecode(AppCache::EXT_EMPLOYEE);
        if(!is_array($aCurrentExt)){
            $aCurrentExt = [];
        }
        $aCurrentExt[$ext] = $uesr_id;
        $this->setCache(AppCache::EXT_EMPLOYEE, json_encode($aCurrentExt), 0);
    }
    /**
     * @Author: DungNT Jun 14, 2017
     * @Todo: set all array ext
     * @param: $aCurrentExt array
     */
    public function setCallCenterExtAll($aCurrentExt) {
        $this->setCache(AppCache::EXT_EMPLOYEE, json_encode($aCurrentExt), 0);
    }
        
    /**
     * @Author: DungNT Mar 15, 2017
     * @Todo: get array KH bị limit của Agent. Những KH phân quyền cho đại lý xem
     */
    public function getCustomerLimitOfAgent($agent_id) {
        $cacheKey   = "{$this->getPrefixCustomerLimitOfAgent()}-$agent_id";
        $value = $this->getCache($cacheKey);
        if(empty($value)){
            $value = $this->setCustomerLimitOfAgent($agent_id);
        }
        return json_decode($value, true);
    }
    public function setCustomerLimitOfAgent($agent_id) {
        $cacheKey   = "{$this->getPrefixCustomerLimitOfAgent()}-$agent_id";
        $aRes = GasAgentCustomer::getCustomerOfAgent($agent_id);
        $value = MyFormat::jsonEncode($aRes);
        $this->setCache($cacheKey, $value);
        return $value;
    }
    
    /** @Author: DungNT jun 11, 2017
     *  @Todo: get all array id của 1 role
     */
    public function getArrayIdRole($role_id) {
        $cacheKey = "{$this->getPrefixUserIdByRole()}-$role_id";
        $value = $this->getCache($cacheKey);
        if(empty($value)){
            $value = $this->setArrayIdRole($role_id);
        }
        return json_decode($value, true);
    }
    public function setArrayIdRole($role_id) {
        $cacheKey = "{$this->getPrefixUserIdByRole()}-$role_id";
        $criteria = new CDbCriteria();
        $criteria->addCondition('t.role_id='.$role_id.' AND t.status=1');
        $models = Users::model()->findAll($criteria);
        $value = json_encode(CHtml::listData($models, 'id', 'id'), JSON_UNESCAPED_UNICODE);
        $this->setCache($cacheKey, $value);
        return $value;
    }
    
    /** @Author: DungNT Jul 26, 2017
     * @Todo: cache listdata gas và vỏ tương ứng của loại gas đó
     */
    public function getListdataGasVo() {
        $cacheKey = AppCache::LISTDATA_MATERIAL_GAS_VO;
        $value = $this->getCache($cacheKey);
        if(empty($value)){
            $value = $this->setListdataGasVo($cacheKey);
        }
        return json_decode($value, true);
    }
    public function setListdataGasVo($cacheKey) {
        $criteria = new CDbCriteria();
        $aMaterialType = [GasMaterialsType::MATERIAL_BINH_50KG, GasMaterialsType::MATERIAL_BINH_45KG, GasMaterialsType::MATERIAL_BINH_12KG, GasMaterialsType::MATERIAL_BINH_6KG, GasMaterialsType::MATERIAL_BINH_4KG];
        $sParamsIn = implode(',', $aMaterialType);
        $criteria->addCondition('t.parent_id<>0');
        $criteria->addCondition("t.materials_type_id IN ($sParamsIn)");
        $models = GasMaterials::model()->findAll($criteria);
        $aRes   = CHtml::listData($models, 'id', 'materials_id_vo');
        $value  = MyFormat::jsonEncode($aRes);
        $this->setCache($cacheKey, $value, 0);
        return $value;
    }
    
    /** @Author: DungNT Sep2817
     *  @Todo: get array user ký quỹ USER_CREDIT
     *  @return [id => type_credit ]
     */
    public function getUserCredit() {
        $cacheKey = AppCache::USER_CREDIT;
        $value = $this->getCache($cacheKey);
        if(empty($value)){
            $value = $this->setUserCredit($cacheKey);
        }
        return json_decode($value, true);
    }
    public function setUserCredit($cacheKey) {
        $criteria = new CDbCriteria();
        $criteria->addCondition('t.role_id='.ROLE_EMPLOYEE_MAINTAIN. ' AND t.status=' . STATUS_ACTIVE);
        $criteria->addCondition('t.price>0'); // flag ky quy
        $models = Users::model()->findAll($criteria);
        $aRes   = CHtml::listData($models, 'id', 'price');
        $value  = MyFormat::jsonEncode($aRes);
        $this->setCache($cacheKey, $value, 86400);
        return $value;
    }
    
    /** @Author: DungNT Oct0617
    *  @Todo: get listdata cache of customer bò mối, xe rao
    */
   public function getCustomerListdata($needMore = []) {
        $cacheKey  = AppCache::LISTDATA_BO_MOI;
        $value = $this->getCache($cacheKey);
        if(empty($value)){
            $value = $this->setCustomerListdata($cacheKey, $needMore);
        }
        return json_decode($value, true);
   }
   public function setCustomerListdata($cacheKey, $needMore) {
        $criteria = new CDbCriteria();
        $criteria->addCondition('t.role_id='.ROLE_CUSTOMER. ' AND t.channel_id=' . Users::CON_LAY_HANG);
        $sParamsIn = implode(',', CmsFormatter::$aTypeIdMakeUsername);
        $criteria->addCondition("t.is_maintain IN ($sParamsIn)");
        $models = Users::model()->findAll($criteria);
        $aRes   = [];
        if($cacheKey == AppCache::LISTDATA_BO_MOI){
            $aRes   = CHtml::listData($models, 'id', 'first_name');
        }elseif($cacheKey == AppCache::LISTDATA_BO_MOI_CODE_ACCOUNT){
            $aRes   = CHtml::listData($models, 'id', 'code_account');
        }
        $value  = MyFormat::jsonEncode($aRes);
        $this->setCache($cacheKey, $value, 86400);// 72000s = 20 hours
        return $value;
    }
    
    /** @Author: DungNT Oct0617
    *  @Todo: get listdata cache of customer bò mối, xe rao
    */
    public function getCustomerCodeAccountListdata($needMore = []) {
        $cacheKey  = AppCache::LISTDATA_BO_MOI_CODE_ACCOUNT;
        $value = $this->getCache($cacheKey);
        if(empty($value)){
            $value = $this->setCustomerListdata($cacheKey, $needMore);
        }
        return json_decode($value, true);
    }
   
    /** @Author: DungNT Feb 27, 2018
     *  @Todo: set location + radius agent for app gas 24h
     **/
    public function setGas24hAgentRadius() {
        $criteria = new CDbCriteria();
        $criteria->addCondition('t.role_id=' . ROLE_AGENT . ' AND t.status=1 AND t.gender='.Users::IS_AGENT);
        $models = Users::model()->findAll($criteria);
        $aRes = [];
        foreach($models as $item){
            if($item->price_other < 2){
                continue;
            }
            $tmp = [];
            $tmp['location']    = $item->slug;
            $tmp['radius']      = $item->price_other;
            $aRes[$item->id]    = $tmp;
        }
        $json = MyFormat::jsonEncode($aRes);
        $this->setCache(AppCache::GAS24H_AGENT_RADIUS, $json, 0);
        return $json;
    }
    public function getGas24hAgentRadius() {
        $cacheKey   = AppCache::GAS24H_AGENT_RADIUS;
        $value      = $this->getCache($cacheKey);
        if(empty($value)){
            $value = $this->setGas24hAgentRadius();
        }
        return json_decode($value, true);
    }
    
    /** @Author: DungNT Sep 30, 2018 -- agent_id => user_id
     *  @Todo: save cache array agent_id => id chuyên viên
     **/
    public function setListdataCvAgent() {
        $criteria = new CDbCriteria();
        $criteria->addCondition("t.employee_maintain_id IN (". implode(',', array_keys(GasConst::getArrayGdkv())).")");
        $models     = GasAgentCustomer::model()->findAll($criteria);
        $listdata   = CHtml::listData($models, 'maintain_agent_id', 'employee_maintain_id');
        $value      = MyFormat::jsonEncode($listdata);
        $this->setCache(AppCache::LISTDATA_CHUYENVIEN_AGENT, $value, 0);
        return $value;
    }
    
    /** @Author: DungNT Sep 30, 2018 
     *  @Todo: get cache array agent_id => id chuyên viên
     **/
    public function getListdataCvAgent() {
        $value = $this->getCache(AppCache::LISTDATA_CHUYENVIEN_AGENT);
        if(empty($value)){
            $value = $this->setListdataCvAgent();
        }
        return json_decode($value, true);
    }
    
    /** @Author: DungNT Now 02, 2018 -- agent_id => user_id
     *  @Todo: save cache array agent_id => id KTKV
     **/
    public function setListdataKtkvAgent() {
        $listdata = [];
        $mSetupTeam = new SetupTeam();
        $mSetupTeam->date_from      = date('Y-m').'-01';
        $mSetupTeam->date_to        = date('Y-m-t');
        $aType = [SetupTeam::TYPE_KTKV_AGENT];
        $aData = $mSetupTeam->getByType($aType);
        foreach($aData as $user_id => $aAgentId){
            foreach($aAgentId as $agent_id){
                $listdata[$agent_id] = $user_id;
            }
        }
        $value      = MyFormat::jsonEncode($listdata);
        $this->setCache(AppCache::LISTDATA_KTKV_AGENT, $value, 0);
        return $value;
    }
    
    /** @Author: DungNT Now 02, 2018
     *  @Todo: get cache array agent_id => id chuyên viên
     **/
    public function getListdataKtkvAgent() {
        $value = $this->getCache(AppCache::LISTDATA_KTKV_AGENT);
        if(empty($value)){
            $value = $this->setListdataKtkvAgent();
        }
        return json_decode($value, true);
    }

    /** @Author: LOCNV Jun 12, 2019
     *  @Todo: set cache array gas announce
     *  @Param:
     **/
    public function setListdataGasAnnounce() {
        $listdata   = [];
        $mAnnounce  = new Announce();
        $date_now   = date('Y-m-d');
//        $mAnnounce->getListAnnounceNow($date_now, true);
        $listdata   = $mAnnounce->getListAnnounceNow($date_now);
        $mAnnounce->setInactiveAnnounce();
        $value      = MyFormat::jsonEncode($listdata);
        $this->setCache(AppCache::LISTDATA_GAS_ANNOUNCE, $value, 0);
        return $value;
    }

    /** @Author: LOCNV Jun 12, 2019
     *  @Todo: get cache array gas announce
     *  @Param:
     **/
    public function getListdataGasAnnounce() {
        $value = $this->getCache(AppCache::LISTDATA_GAS_ANNOUNCE);
        if(empty($value)){ // close when testing
            $value = $this->setListdataGasAnnounce();
        }
        return json_decode($value, true);
    }
    
    /** @Author: DungNT Aug 27, 2019
     *  @Todo: get some cache not sync
     * vd: AppGas24hNews-123, AppGas24hNewsList-1
     **/
    public function isNeedSync($cacheKey) {
        $needSync       = true;
        $temp           = explode('-', $cacheKey);
        $cacheKeyName   = isset($temp[0]) ? $temp[0] : $cacheKey;
        $role_id        = isset($temp[1]) ? $temp[1] : '';
        switch ($cacheKeyName) {
            case 'AppGas24hNews':
            case 'AppGas24hNewsList':
            case 'ALLOW_IP_ACCOUNTING':
                $needSync = false;
                break;
        }
        return $needSync;
    }
    
    /** @Author: DungNT Aug 27, 2019
     *  @Todo: get cache by key, fix some cache không đc khởi tạo ở server nội bộ.
     *  ở Server noibo 1 số Cache bị empty, do đó phải tự khởi tạo mới ở bên server noibo
     *  nêu ko khởi tạo thì khi đông bộ với S1,2,3 thì đồng bộ empty sang, do đó sẽ phải khởi tạo ở server root (noibo)
     **/
    public function getOneCacheByKey($cacheKey) {
        $value = '';
        switch ($cacheKey) {
            case AppCache::LISTDATA_STREET:
                $value = $this->setListdata('GasStreet', AppCache::LISTDATA_STREET, 'name');
                break;
            case AppCache::ARR_MODEL_MATERIAL_GAS:
                $value = $this->setObjMaterialByType(GasMaterialsType::getTypeBoMoi(), AppCache::ARR_MODEL_MATERIAL_GAS, ['GetActive'=>1]);
                break;
            case AppCache::ARR_APP_MATERIAL_STORECARD:
                $value = $this->setAppMaterialsStorecard(AppCache::ARR_APP_MATERIAL_STORECARD, []);
                break;
            case AppCache::ARR_MODEL_MATERIAL_HGD:
                $value = $this->setObjMaterialByType(GasMaterialsType::$ARR_VT_HGD, AppCache::ARR_MODEL_MATERIAL_HGD, ['GetActive'=>1]);
                break;
            case AppCache::ARR_MODEL_MATERIAL_VO:
                $value = $this->setObjMaterialByType(GasMaterialsType::$ARR_WINDOW_VO, AppCache::ARR_MODEL_MATERIAL_VO, ['GetActive'=>1]);
                break;
            case AppCache::ARR_MODEL_GAS12KG:
                $value = $this->setModelGas12Kg();
                break;
            case AppCache::LISTDATA_MATERIAL_TYPE_VO:
                $value = $this->setListdataMaterialByType(GasMaterialsType::$ARR_WINDOW_VO, AppCache::LISTDATA_MATERIAL_TYPE_VO, 'name');
                break;
            case AppCache::GAS24H_AGENT_RADIUS:
                $value = $this->setGas24hAgentRadius();
                break;
            case AppCache::LISTDATA_CUSTOMER_DEBIT:
                $mOneMany = new GasOneMany();
                $value = $mOneMany->setCacheCustomerAllowDebit();
                break;
            case AppCache::LISTDATA_ROLE:
                $value = $this->setListdata('Roles', AppCache::LISTDATA_ROLE, 'role_name');
                break;
        }
        // handle cache name format like => 'CacheListdataUserRole-'.$role_id;
        $temp           = explode('-', $cacheKey);
        $cacheKeyName   = isset($temp[0]) ? $temp[0] : '';
        $role_id        = isset($temp[1]) ? $temp[1] : '';
        switch ($cacheKeyName) {
            case $this->getPrefixListdataRole():
                $value = $this->setListdataUserByRole($role_id);
                break;
            case $this->getPrefixUserIdByRole():
                $value = $this->setArrayIdRole($role_id);
                break;
            case $this->getPrefixUserIdMaintainOfAgent():
                $value = $this->setUserMaintainOfAgent($role_id);// $role_id is $agent_id name is: CacheUserMaintainOfAgent-2030779
                break;
            case $this->getPrefixModelUserRole():
                $needMore = ['GetActive' => 1];
                $value = $this->setUserByRole($role_id, $needMore);// $role_id is $agent_id name is: CacheUserMaintainOfAgent-2030779
                break;
            case $this->getPrefixCustomerLimitOfAgent():
                $value = $this->setCustomerLimitOfAgent($role_id);// $role_id is $agent_id name is: CustomerLimitOfAgent-2030779
                break;
        }
        return $value;
    }
    
}