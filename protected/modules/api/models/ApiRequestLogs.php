<?php

/**
 * This is the model class for table "{{_api_request_logs}}".
 *
 * The followings are the available columns in table '{{_api_request_logs}}':
 * @property string $id 
 * @property string $method
 * @property string $content
 * @property string $created_date
 */
class ApiRequestLogs extends BaseSpj {
		
    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{_api_request_logs}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        return array(
            array('id, method, content, created_date', 'safe'),
        );
    }

    /**
     * @return array relational rules. 
     */
    public function relations()
    {
        return [];
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => Yii::t('translation','ID'),
            'method' => Yii::t('translation','Method'),
            'content' => Yii::t('translation','Content'),
            'created_date' => Yii::t('translation','Created Date'),
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.
        $criteria=new CDbCriteria;

        $criteria->compare('id',$this->id);
        $criteria->order = 't.id DESC';
        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
            'pagination'=>array(
                'pageSize'=> 50,
            ),
        ));
    }



    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return ApiRequestLogs the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }
    /** @Author: ANH DUNG
     **/
    protected function beforeSave() {
        $this->ip_address   = MyFormat::getIpUser();
        $this->country      = GasCheck::ApiGetCountryUser($this->ip_address);
        
        return parent::beforeSave();
    }
    
    /** @Author: ANH DUNG Now 20, 2018
     *  @Todo: clean Log
     **/
    public function cleanLog() {
        ApiRequestLogs::model()->deleteAll();
    }
}
