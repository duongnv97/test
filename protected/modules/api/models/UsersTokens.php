<?php

/**
 * This is the model class for table "{{_users_tokens}}".
 *
 * The followings are the available columns in table '{{_users_tokens}}':
 * @property string $id
 * @property string $user_id
 * @property string $token
 * @property integer $has_updated_profile_firsttime_open_wallet
 * @property string $last_login
 * @property integer $has_expired
 */
class UsersTokens extends BaseSpj {
	
    public $mPromotion = null, $device_token;
    const LOGIN_ANDROID = 1; // MAY 31, 2016
    const LOGIN_WINDOW  = 2;
    const LOGIN_IOS     = 3;
    const LOGIN_WEB     = 4; // Now 27, 2016
//    const LOGIN_UPDATE_SAU = 3;
    
    const APP_TYPE_GAS_SERVICE      = 0; // app gas service
    const APP_TYPE_GAS24H           = 1; // app Gas 24h
    const APP_TYPE_TRUCK_SCALES     = 2; // app windows PM cân
    
    const PLATFORM_ANDROID = 1; // MAY 16, 2017
    const PLATFORM_IOS     = 2;
    const PLATFORM_WINDOW  = 3;
    
    const DAY_KEEP_TOKEN_CUSTOMER = 300; // 300 days 
    
    public static $ARR_LOGIN_WINDOW = array(
        UsersTokens::LOGIN_WINDOW,
    );// May 31, 2016
    public static $ARR_LOGIN_MOBILE = array(
        UsersTokens::LOGIN_ANDROID,
        UsersTokens::LOGIN_IOS
    );
    
    public function getTypeAppMobile() {
        return [
            UsersTokens::APP_TYPE_GAS_SERVICE,
            UsersTokens::APP_TYPE_GAS24H
        ];
    }
    
    /** @Author: DungNT Aug 02, 2017
     * @Todo: get array type token
     */
    public function getArrayType() {
        return [
            UsersTokens::LOGIN_ANDROID  => 'Android',
            UsersTokens::LOGIN_WINDOW   => 'Window',
            UsersTokens::LOGIN_IOS      => 'Ios',
            UsersTokens::LOGIN_WEB      => 'Web',
        ];
    }

    public function getType() {
        $aType = $this->getArrayType();
        return isset($aType[$this->type]) ? $aType[$this->type] : '';
    }
    
    /** @Author: DungNT Dec 14, 2018
     *  @Todo: list role can login PM Can - windows
     **/
    public function getRoleWindows() {
        return [
            ROLE_CRAFT_WAREHOUSE,
            ROLE_SECURITY,
        ];
    }
    
    /**
    * @return string the associated database table name
    */
   public function tableName()
   {
           return '{{_api_users_tokens}}';
   }

   /**
    * @return array validation rules for model attributes.
    */
   public function rules()
   {
        return array(
             array('user_id, token', 'required', 'on'=>'MakeNewToken'),
             array('type, id, user_id, role_id, token, has_updated_profile_firsttime_open_wallet, last_login, has_expired, mobile_number', 'safe'),
        );
   }

   /**
    * @return array relational rules.
    */
   public function relations() { 
       return array(
        'rUser' => array(self::BELONGS_TO, 'Users', 'user_id'),
        );
   }

   /**
    * @return array customized attribute labels (name=>label)
    */
   public function attributeLabels()
   {
        return array();
   }

   public function search()
   {
        $criteria=new CDbCriteria;
        $criteria->compare('user_id',$this->user_id);
        $criteria->compare('has_expired',$this->has_expired);
        return new CActiveDataProvider($this, array(
                'criteria'=>$criteria,
                'pagination'=>array(
                    'pageSize'=> Yii::app()->params['defaultPageSize'],
                ),
        ));
   }

   public static function model($className=__CLASS__)
   {
        return parent::model($className);
   }
        
    public function checkToken($mobile_number, $token)
    {
        $criteria = new CDbCriteria;
        $criteria->compare('t.mobile_number', $mobile_number);
        $criteria->compare('t.token', $token);
        $criteria->compare('t.has_expired', 0);
        if (self::model()->count($criteria) > 0)
            return true;
        return false;
    }

    public function getByMobileNumber($mobile_number)//for apns
    {
        $criteria = new CDbCriteria;
        $criteria->select = 'DISTINCT t.apns_device_token, t.gcm_device_token';
        $criteria->compare('t.mobile_number', $mobile_number);
        $criteria->compare('t.has_expired', 0);
        $criteria->addCondition('t.apns_device_token <>"" OR t.gcm_device_token <> ""');
        $models = self::model()->findAll($criteria);
        if ($models)
            return $models; 
        return array();
    }
    
    /** BACK UP VERSION
     * @Author: DungNT Now 27, 2016
     * @Todo: create new token for onece login * chỉ xử lý mở màn hình login
     */
    public static function MakeNewTokenBackUp($mUser, $q) {
//        $mUsersTokens = new UsersTokens('MakeNewToken');
//        $mUsersTokens->user_id      = $mUser->id;
//        $mUsersTokens->last_active  = date('Y-m-d H:i:s');
//        $mUsersTokens->role_id      = $mUsersTokens->rUser->role_id;
//        // 1. delete all token of user - trong 1 thời điểm chỉ có 1 login của 1 user dc accept
//        // Now 24, 2015 Open delete all only ở live 
////        $mUsersTokens->deleteAllTokenOfUser(); /// Now 10, 2015 tạm đóng lại, cho phép nhiều token cùng sử dụng, vì đang dev nên để vậy cho tiện
////        Sẽ mở deleteAllTokenOfUser ra khi live 
//        // 2. save new record
//        $mUsersTokens->mobile_number = $mUser->username;
//        $mUsersTokens->token = MyFormat::generateSessionIdByModel('UsersTokens', 'token');
//        
//        if(!empty($q->gcm_device_token)){
//            $mUsersTokens->gcm_device_token = $q->gcm_device_token;
//        }elseif(!empty($q->apns_device_token)){
//            $mUsersTokens->apns_device_token = $q->apns_device_token;
//            $mUsersTokens->type              = UsersTokens::LOGIN_IOS;
//        }
//        
//        if(isset($q->type) && !empty($q->type) && in_array($q->type, UsersTokens::$ARR_LOGIN_WINDOW)){
//            $mUsersTokens->type = $q->type;
//        }// May 31, 2016 fix mặc định type = 1 là android login
//        
//        $mUsersTokens->save();
//        $mUsersTokens->saveTrackLogin();
//        $mUsersTokens->syncDataToken();
//        return $mUsersTokens;
    }
    
    /**
     * @Author: DungNT Jul 13, 2015
     * @Todo: create new token for onece login
     * chỉ xử lý mở màn hình login
     * @Param: $model
     */
    public function makeNewToken($mUser, $q) {
        $token              = '';
        $gcm_device_token   = '';
        $apns_device_token  = '';
        $type               = '';
        if(!empty($q->gcm_device_token)){
            $gcm_device_token = $q->gcm_device_token;
            $type              = UsersTokens::LOGIN_ANDROID;
        }elseif(!empty($q->apns_device_token)){
            $apns_device_token = $q->apns_device_token;
            $type              = UsersTokens::LOGIN_IOS;
        }
//        if(isset($q->type) && !empty($q->type) && in_array($q->type, UsersTokens::$ARR_LOGIN_WINDOW)){
        if(isset($q->type) && !empty($q->type)){
            $type = $q->type;
        }// May 31, 2016 fix mặc định type = 1 là android login
        return $this->makeNewTokenSaveData($mUser, $token, $gcm_device_token, $apns_device_token, $type);
    }
    
    /**
     * @Author: DungNT Nov 27, 2016
     * @Todo: saveData tách hàm ra để xử lý save token from login web
     */
    public function makeNewTokenSaveData($mUser, $token, $gcm_device_token, $apns_device_token, $type) {
        $mUsersTokens                   = new UsersTokens('MakeNewToken');
        $mUsersTokens->user_id          = $mUser->id;
        $mUsersTokens->role_id          = $mUser->role_id;
        $mUsersTokens->is_maintain      = $mUser->is_maintain;
        $mUsersTokens->mobile_number    = $mUser->username;
        $mUsersTokens->last_active      = date('Y-m-d H:i:s');
        $mUsersTokens->created_date     = $mUsersTokens->last_active;
        // 1. delete all token of user - trong 1 thời điểm chỉ có 1 login của 1 user dc accept
        // Now 24, 2015 Open delete all only ở live 
//        $mUsersTokens->deleteAllTokenOfUser(); /// Now 10, 2015 tạm đóng lại, cho phép nhiều token cùng sử dụng, vì đang dev nên để vậy cho tiện
//        Sẽ mở deleteAllTokenOfUser ra khi live 
        // 2. save new record
        if(empty($token)){
            $mUsersTokens->token = MyFormat::generateSessionIdByModel('UsersTokens', 'token');
        }else{
            $mUsersTokens->token = $token;
        }
        
        if(!empty($gcm_device_token)){
            $mUsersTokens->gcm_device_token     = $gcm_device_token;
        }elseif(!empty($apns_device_token)){
            $mUsersTokens->apns_device_token    = $apns_device_token;
            $mUsersTokens->type                 = UsersTokens::LOGIN_IOS;
        }
        
        if(!empty($type)){
            $mUsersTokens->type = $type;
        }// May 31, 2016 fix mặc định type = 1 là android login
        
        $mUsersTokens->save();
        $mUsersTokens->saveTrackLogin();
        $mUsersTokens->syncDataToken();// đồng bộ token sang io.huongminhgroup.com
        return $mUsersTokens;

    }
    
    /** @Author: DungNT Aug 16, 2016
     * @Todo: tracklogin
     */
    public function saveTrackLogin() {
        if($this->type == UsersTokens::LOGIN_WEB){
            return ;
        }
        
        $type = GasTrackLogin::TYPE_ANDROID;
        if($this->type == UsersTokens::LOGIN_WINDOW){
            $type = GasTrackLogin::TYPE_WINDOW;
        }elseif($this->type == UsersTokens::LOGIN_IOS){
            $type = GasTrackLogin::TYPE_IOS;
        }
        $mTrackLogin = $this->mapTrackLogin();
        GasTrackLogin::SaveRecord($type, '', $this->user_id, $this->role_id, $mTrackLogin);
    }
    /** @Author: DungNT May 12, 2018
     *  @Todo: map some info GasTrackLogin
     **/
    public function mapTrackLogin() {
        $mTrackLogin = new GasTrackLogin();
        $mTrackLogin->apns_device_token     = $this->apns_device_token;
        $mTrackLogin->gcm_device_token      = $this->gcm_device_token;
        return $mTrackLogin;
    }
    
    
    /** @Author: DungNT Aug 29, 2017
     * @Todo: tracklogout
     */
    public function saveTrackLogout($q) {
        $type = GasTrackLogin::TYPE_LOGOUT_UNKNOW;
        if(!isset($q->platform)){
            Logger::WriteLog('API Error request logout: '. json_encode($q));
            return ;
        }
        if($q->platform == UsersTokens::PLATFORM_ANDROID){
            $type = GasTrackLogin::TYPE_LOGOUT_ANDROID;
        }elseif($q->platform == UsersTokens::PLATFORM_IOS){
            $type = GasTrackLogin::TYPE_LOGOUT_IOS;
        }
        $mUsersTokens = $this->getByToken();
        if(!empty($mUsersTokens)){
            $mTrackLogin = $mUsersTokens->mapTrackLogin();
        }
        GasTrackLogin::SaveRecord($type, '', $this->user_id, $this->role_id, $mTrackLogin);
    }
    
    /**
     * @Author: DungNT Oct 19, 2015
     * @Todo: xử lý xóa token của android, những token của cái khác không xóa
     * @note_fix: Jul 09, 2017 xóa hết token của User không cần biết type là gì
     */
    public function deleteAllTokenOfUser() {
        if(empty($this->user_id)){
            return ;
        }
        $criteria = new CDbCriteria;
        $criteria->compare('user_id', $this->user_id);
//        $criteria->compare("type", UsersTokens::LOGIN_ANDROID);// May 31, 2016 vì cho phép window login sử dụng chung bảng token nên khi get list gcm để gửi thì phải thêm điều kiện này nữa
        self::model()->deleteAll($criteria);
    }
    
    /** @Author: DungNT Aug 19, 2017
     *  @Todo: xử lý xóa token của android + ios, những token của cái khác không xóa
     */
    public static function deleteAllTokenMobile($user_id) {
        if(empty($user_id)){
            return ;
        }
        $criteria = new CDbCriteria;
        $criteria->compare("user_id", $user_id);
        $criteria->addInCondition("type", UsersTokens::$ARR_LOGIN_MOBILE);// May 31, 2016 vì cho phép window login sử dụng chung bảng token nên khi get list gcm để gửi thì phải thêm điều kiện này nữa
        self::model()->deleteAll($criteria);
    }
    
    /**
     * @Author: DungNT Now 27, 2016
     * @Todo: xử lý xóa token của user by type. Đã đưa cả token của login web vào table này, nên chia ra nhiều type, khi xóa phải theo type
     * @important: cần thận khi dùng hàm này, vì 1 user có thể login ở nhiều type và nhiều thiết bị
     */
    public static function deleteAllTokenUserByType($user_id, $type) {
        $criteria = new CDbCriteria;
        $criteria->compare("user_id", $user_id);
        $criteria->compare("type", $type);// May 31, 2016 vì cho phép window login sử dụng chung bảng token nên khi get list gcm để gửi thì phải thêm điều kiện này nữa
        self::model()->deleteAll($criteria);
    }
    
    /**
     * @Author: DungNT Jun 29, 2016
     */
    public static function deleteByUser($user_id) {
        $model = new UsersTokens();
        $model->user_id = $user_id;
        $model->deleteAllTokenOfUser();
    }
    
    /**
     * @Author: DungNT Jul 14, 2015
     * @Todo: delete token
     * @note Now 28, 2015 không xử lý xóa token khi logout vì sẽ để cho user nhận dc notify khi nó logout ra
     * @Param: $token
     */
    public static function logout($token)
    {
        if(empty($token)){
            return ;
        }
        $criteria = new CDbCriteria;
        $criteria->compare('token', $token);
        UsersTokens::model()->deleteAll($criteria);
    }
    
    /**
     * @Author: DungNT Jul 15, 2015
     * @Todo: get model User by token
     * @Param: $token
     */
    public static function getModelUser($token)
    {
        $token = trim($token);
        if(empty($token)){
            return null;
        }
        $criteria = new CDbCriteria;
        $criteria->compare('token', $token);
        $model = UsersTokens::model()->find($criteria);
        if($model){
            return Users::model()->findByPk($model->user_id);
        }
        return null;
    }

    /**
     * @Author: DungNT Jul 12, 2018
     * @Todo: get model by token
     */
    public function getByToken(){
        if(empty($this->token)){
            return null;
        }
        $criteria = new CDbCriteria;
        $criteria->compare('t.token', $this->token);
        return UsersTokens::model()->find($criteria);
    }
    
    /**
     * @Author: DungNT Sep 21, 2015
     * @Todo: get list gcm_device_token, apns_device_token
     * @Param: $field_name
     */
    public static function getListDeviceToken($field_name, $aUid, $type) {
        if(count($aUid) < 1){
            return [];
        }
        $criteria = new CDbCriteria;
        $criteria->select = "distinct(t.$field_name) as device_token, t.user_id";
        $criteria->addCondition("t.$field_name IS NOT NULL AND t.$field_name<>'' ");
        $sParamsIn = implode(',', $aUid);
        $criteria->addCondition("t.user_id IN ($sParamsIn)");
//        $criteria->addInCondition('t.user_id', $aUid);
        $criteria->addCondition('t.type='.$type);// May 31, 2016 vì cho phép window login sử dụng chung bảng token nên khi get list gcm để gửi thì phải thêm điều kiện này nữa
//        $criteria->group = "t.$field_name";// only send one time for one device
        // close ddan >group = "t.$field_nam Apr 12, 2016 có thể sẽ fix là send tất cả device của 1  tài khoản
        return self::model()->findAll($criteria);
    }
    
    /**
     * @Author: DungNT Sep 21, 2015
     * @Todo: get list gcm_device_token, apns_device_token
     * @Param: $aUid array user id
     * @Param: $field_name
     */
    public static function getDeviceTokenByUid($aUid, $field_name = 'gcm_device_token') {
        if(count($aUid) < 1){
            return array();
        }
        $criteria = new CDbCriteria;
        $criteria->select = "distinct(t.$field_name) as device_token";
        $criteria->addCondition("t.$field_name IS NOT NULL AND t.$field_name<>'' ");
        $sParamsIn = implode(',', $aUid);
        $criteria->addCondition("t.user_id IN ($sParamsIn)");
//        $criteria->addInCondition("t.user_id", $aUid);
        $criteria->compare("t.type", UsersTokens::LOGIN_ANDROID);// May 31, 2016 vì cho phép window login sử dụng chung bảng token nên khi get list gcm để gửi thì phải thêm điều kiện này nữa
        $criteria->group = "t.$field_name";// only send one time for one device
        return self::model()->findAll($criteria);
    }
    
    public function getAllOfUid() {
        if(empty($this->user_id)){
            return [];
        }
        $criteria = new CDbCriteria;
        $criteria->addCondition("t.user_id = $this->user_id");
        return self::model()->findAll($criteria);
    }
    
    /**
     * @Author: DungNT Dec 22, 2015
     * @Todo: kiểm tra user id này có login = app chưa
     * nếu chưa login = app thì sẽ không build notify
     */
    public static function getByUserId($user_id) {
        $criteria = new CDbCriteria;
        $criteria->compare('t.user_id', $user_id);
        $sParamsIn = implode(',', array(UsersTokens::LOGIN_ANDROID, UsersTokens::LOGIN_IOS));
        $criteria->addCondition("t.type IN ($sParamsIn)");
//        $criteria->compare("t.type", UsersTokens::LOGIN_ANDROID);// May 31, 2016 vì cho phép window login sử dụng chung bảng token nên khi get list gcm để gửi thì phải thêm điều kiện này nữa
        return UsersTokens::model()->find($criteria);
    }
    
    /**
     * @Author: DungNT Apr 13, 2016
     * @Todo: set lastest date_time active of user
     * @Param: $token user login
     */
    public static function setLastActive($token) {
        $criteria = new CDbCriteria;
        $criteria->compare('token', $token);
        $aUpdate = array('last_active' => date("Y-m-d H:i:s"));
        UsersTokens::model()->updateAll($aUpdate, $criteria);
    }
    
    /**
     * @Author: DungNT Apr 25, 2016
     * @Todo: xóa những token login mà không active trong vòng 5 ngày hoặc last_active NULL
     */
    public static function cronRemoveTokenNotUse() {
        $days_check = 30;
        $aRoleNotRemove = array(ROLE_CUSTOMER);
        $criteria = new CDbCriteria;
        $criteria->addNotInCondition('role_id', $aRoleNotRemove);
        $criteria->addCondition("last_active IS NULL OR DATE_ADD(last_active, INTERVAL $days_check DAY) < NOW()");
        UsersTokens::model()->deleteAll($criteria);
//        $models = UsersTokens::model()->findAll($criteria);
//        Users::deleteArrModel($models);
        
        // May 31, 2016 xóa những token window cua 1 ngay hom truoc
        $aTypeDelete = array(UsersTokens::LOGIN_WINDOW, UsersTokens::LOGIN_WEB);
        $days_check = 1;
        $criteria = new CDbCriteria;
        $criteria->addInCondition('type', $aTypeDelete);// May 31, 2016 vì cho phép window login sử dụng chung bảng token nên khi get list gcm để gửi thì phải thêm điều kiện này nữa
        $criteria->addCondition("DATE_ADD(created_date, INTERVAL $days_check DAY) < NOW()");
        UsersTokens::model()->deleteAll($criteria);
//        $models = UsersTokens::model()->findAll($criteria);
//        Users::deleteArrModel($models);
        // May 31, 2016 xóa những token android + ios app gas24h sau 6 thang = 180 ngay khong active
        $aTypeDelete = array(UsersTokens::LOGIN_ANDROID, UsersTokens::LOGIN_IOS);
        $days_check = UsersTokens::DAY_KEEP_TOKEN_CUSTOMER;
        $criteria = new CDbCriteria;
        $criteria->addInCondition('type', $aTypeDelete);// May 31, 2016 vì cho phép window login sử dụng chung bảng token nên khi get list gcm để gửi thì phải thêm điều kiện này nữa
        $criteria->addCondition("DATE_ADD(last_active, INTERVAL $days_check DAY) < NOW()");
        $criteria->addCondition('role_id=' . ROLE_CUSTOMER);
        $criteria->addCondition('is_maintain=' . UsersExtend::STORE_CARD_HGD_APP);
        UsersTokens::model()->deleteAll($criteria);
    }
    
    /**
     * @Author: DungNT Oct 03, 2016
     * @Todo: đồng bộ token user login sang server io
     */
    public function syncDataToken() {
        $aTypeSync = array(UsersTokens::LOGIN_WINDOW, UsersTokens::LOGIN_WEB);
        if(!in_array($this->type, $aTypeSync)){
            return ;
        }
        if(!GasCheck::isServerLive()){// Add May0619 DungNT
            return ;
        }
//        if(strpos($_SERVER['HTTP_HOST'], "daukhimiennam") == FALSE && strpos($_SERVER['HTTP_HOST'], "ocalhost") == FALSE){// chỉ đồng bộ từ server chính đi thôi, con những server test ko chạy gì cả
//            return ;
//        }// May0619 DungNT Close ---- Now 30, 2016 tạm close lại, luôn sync qua server io để test
        $mSync = new SyncData(SyncData::SERVER_IO_SOCKET, 'socket/tokenAdd');
        $mSync->tokenAdd($this);
    }
        
    /**
     * @Author: DungNT Feb 24, 2017
     * @Todo: check is live server hay không
     */
    public function isLiveServer() {
        if(strpos($_SERVER['HTTP_HOST'], "daukhimiennam") == FALSE){// chỉ đồng bộ từ server chính đi thôi, con những server test ko chạy gì cả
            return false;
        }
        return true;
    }

    /**
     * @Author: DungNT Nov 22, 2016
     * @Todo: validate login
     */
    public function validateLogin($objController, $mUser, $result, $q) {
        $isGas24hOTP = isset($q->otp_code);
        if(isset($q->app_type) && $q->app_type == UsersTokens::APP_TYPE_GAS24H){ // now 22, 2016 xử lý login cho user app Gas24h
            if ($isGas24hOTP) {
                $mSignup = new AppSignup('AppOTP');
                $mSignup->phone             = $q->username;
                $mSignup->token             = $q->token;
                $mSignup->confirm_code      = $q->otp_code;
                if (!$mSignup->checkOTPLogin()){
                    $result['message'] = "Mã xác nhận hoặc thông tin không chính xác!";
                    ApiModule::sendResponse($result, $objController);
                }
                if (!$mUser) {
                   // No need to check more with new user
                    return;
                }
            }else{
                if(!UsersTokens::isCustomerGas24h($mUser)){
                    $result['message'] = "Tài khoản không tồn tại V";
                    ApiModule::sendResponse($result, $objController);
                }
            }
        }elseif(isset($q->app_type) && $q->app_type == UsersTokens::APP_TYPE_GAS_SERVICE && $mUser){ // now 22, 2016 xử lý login cho user app Gas24h
            if(UsersTokens::isCustomerGas24h($mUser)){
                $result['message'] = "Tài khoản không tồn tại V1";
                ApiModule::sendResponse($result, $objController);
            }
        }

        if(!$mUser){
            $result['message'] = 'Tài khoản không tồn tại';
            ApiModule::sendResponse($result, $objController);
        }elseif($mUser->password_hash != md5(trim($q->password)) && !$isGas24hOTP){
            $result['message'] = 'Sai mật khẩu';
            ApiModule::sendResponse($result, $objController);
        }elseif($mUser->status == STATUS_INACTIVE){ // inactive
            $result['message'] = 'Tài khoản không tồn tại';
            ApiModule::sendResponse($result, $objController);
        }elseif(in_array($q->app_type, $this->getTypeAppMobile()) &&  empty($q->gcm_device_token) && empty($q->apns_device_token)){// empty gcm
            $msg = 'Bạn chưa cấp quyền nhận thông báo cho App, không thể đăng nhập, vui lòng xóa app đi tải lại và cấp đủ quyền để vào app';// Jul1319 DungNT
//            $result['message'] = 'Đăng nhập không thành công, vui lòng thử lại';
            $result['message'] = $msg;
            ApiModule::sendResponse($result, $objController);
        }elseif(isset($q->app_type) && $q->app_type == UsersTokens::APP_TYPE_TRUCK_SCALES && !in_array($mUser->role_id, $this->getRoleWindows() ) ){ // inactive
            $result['message'] = 'Đăng nhập không thành công, vui lòng thử lại, chưa setup UsersTokens->getRoleWindows';
            ApiModule::sendResponse($result, $objController);
        }
        
    }
    
    /** @Author: DungNT Nov 22, 2016
     *  @Todo: validate login
     */
    public static function validateLoginBK($objController, $mUser, $result, $q) {
        $isGas24hOTP = isset($q->otp_code);
        if(isset($q->app_type) && $q->app_type == UsersTokens::APP_TYPE_GAS24H){ // now 22, 2016 xử lý login cho user app Gas24h
            if ($isGas24hOTP) {
                $mSignup = new AppSignup('AppOTP');
                $mSignup->phone = $q->username;
                $mSignup->token = $q->token;
                $mSignup->confirm_code = $q->otp_code;
                if (!$mSignup->checkOTPLogin()){
                    $result['message'] = "Mã xác nhận hoặc thông tin không chính xác!";
                    ApiModule::sendResponse($result, $objController);
                }

                if (!$mUser) {
                   // No need to check more with new user
                    return;
                }
            } else {
//            if($mUser->role_id != ROLE_CUSTOMER || ($mUser->role_id == ROLE_CUSTOMER && $mUser->is_maintain != UsersExtend::STORE_CARD_HGD_APP) ){
                if (!UsersTokens::isCustomerGas24h($mUser)) {
                    $result['message'] = "Tài khoản không tồn tại V";
                    ApiModule::sendResponse($result, $objController);
                }

                if(!$mUser){
                    $result['message'] = "Tài khoản không tồn tại";
                    ApiModule::sendResponse($result, $objController);
                }elseif($mUser->password_hash != md5($q->password)){
                    $result['message'] = "Sai mật khẩu";
                    ApiModule::sendResponse($result, $objController);
                }
            }
        }elseif(isset($q->app_type) && $q->app_type == UsersTokens::APP_TYPE_GAS_SERVICE){ // now 22, 2016 xử lý login cho user app Gas24h
//            if($mUser->role_id != ROLE_CUSTOMER || ($mUser->role_id == ROLE_CUSTOMER && $mUser->is_maintain != UsersExtend::STORE_CARD_HGD_APP) ){
            if(UsersTokens::isCustomerGas24h($mUser)){
                $result['message'] = "Tài khoản không tồn tại V1";
                ApiModule::sendResponse($result, $objController);
            }

            if(!$mUser){
                $result['message'] = "Tài khoản không tồn tại";
                ApiModule::sendResponse($result, $objController);
            }elseif($mUser->password_hash != md5($q->password)){
                $result['message'] = "Sai mật khẩu";
                ApiModule::sendResponse($result, $objController);
            }
        }
        
        if($mUser->status == STATUS_INACTIVE){ // inactive
            $result['message'] = "Tài khoản không tồn tại";
            ApiModule::sendResponse($result, $objController);
        }elseif(empty($q->gcm_device_token) && empty($q->apns_device_token)){// empty gcm
            $result['message'] = "Đăng nhập không thành công, vui lòng thử lại";
            ApiModule::sendResponse($result, $objController);
        }
    }
    
    /**
     * @Author: DungNT Nov 22, 2016
     * @Todo: check user có phải là user của app Gas24h không
     * @return: true là có Gas24h, false là no
     */
    public static function isCustomerGas24h($mUser) {
        if($mUser->role_id == ROLE_CUSTOMER && $mUser->is_maintain == UsersExtend::STORE_CARD_HGD_APP ){
            return true;
        }
        return false;
    }
    
    
    /**
     * @Author: DungNT Apr 10, 2018
     * @Todo: get list gcm_device_token, apns_device_token by platform of user
     * @Param: $type: android or ios
     * @Param: $limit
     * user_id = 1219935: ios
     */
    public function getUserGas24hByPlatform($type, $limit) {
        $field_name = 'gcm_device_token';
        if($type == UsersTokens::LOGIN_IOS){
            $field_name = 'apns_device_token';
        }
        $criteria = new CDbCriteria;
//        $aUserTest = [1430946, 1219935, 1384960];
        $aUserTest = [
            1219935, // ios Kiên
            1420931, // ios My Chau
            1282326, // ios Chi Hạnh
            1396362, // ios My Ngan
            1384960,// android Trung 45321
            1430946, // Android Dũng 331552
        ];
//        $criteria->addInCondition('t.user_id', $aUserTest);// only dev test
        $criteria->addCondition("t.$field_name IS NOT NULL AND t.$field_name<>'' ");
        $criteria->addCondition('t.role_id=' . ROLE_CUSTOMER);
        $criteria->addCondition('t.is_maintain=' . UsersExtend::STORE_CARD_HGD_APP);
        $criteria->addCondition('t.type=' . $type);
        $criteria->addCondition('t.cron_lock=0');
//        $this->limitUserAlreadyPromotion($criteria);// không cần thiết lắm Close on Oct0718
        $criteria->limit = $limit;// only send one time for one device
        return self::model()->findAll($criteria);
    }
    
    /** @Author: DungNT Apr 12, 2018
     *  @Todo: loại các user đã nhập promotion ra, không gửi notify nữa
     **/
    public function limitUserAlreadyPromotion(&$criteria) {
        if(empty($this->mPromotion) || empty($this->mPromotion->code_no)){
            return ;
        }
        $criteria->addCondition("t.user_id NOT IN ( SELECT SubQ.user_id FROM gas_app_promotion_user as SubQ WHERE SubQ.promotion_id='".$this->mPromotion->id."')");
    }
    

    /**
     * @Author: DungNT Oct 07, 2018
     * @Todo: get list gcm_device_token, apns_device_token by platform of user
     * @Param: $type: android or ios UsersTokens::LOGIN_ANDROID, UsersTokens::LOGIN_IOS
     * @Param: $limit if 0 is unlimit
     * @Param: $role_id value can be number or array
     * user_id = 1219935: ios
     */
    public function getUserGasServiceByPlatform($type, $role_id, $limit=0) {
        $field_name = 'gcm_device_token';
        if($type == UsersTokens::LOGIN_IOS){
            $field_name = 'apns_device_token';
        }
        
        $criteria = new CDbCriteria;
        $sTypeLogin = implode(',', UsersTokens::$ARR_LOGIN_MOBILE);
        $criteria->addCondition("t.type IN ($sTypeLogin)");
        $criteria->addCondition("t.$field_name IS NOT NULL AND t.$field_name<>'' ");
        if($role_id == ROLE_CUSTOMER){
            $sParamsIn = implode(',', CmsFormatter::$aTypeIdMakeUsername);
            $criteria->addCondition('t.role_id ='. ROLE_CUSTOMER);
            $criteria->addCondition("t.is_maintain IN ($sParamsIn)");
        }else{
            if(is_array($role_id)){// can limit role here [2,3,4]
                $sParamsIn = implode(',', $role_id);
                $criteria->addCondition("t.role_id IN ($sParamsIn)");
            }
            $criteria->addCondition("t.role_id <> ".ROLE_CUSTOMER);
        }
        if($limit){
            $criteria->limit = $limit;
        }
        return self::model()->findAll($criteria);
    }
    
    
    /** @Author: DungNT Oct 09, 2018
     *  @Todo: get list user login app by role 
     **/
    public function getByRole() {
        if(!is_array($this->role_id)){
            return ;
        }
        $criteria = new CDbCriteria;
        $criteria->addInCondition('t.role_id', $this->role_id);
        $sTypeLogin = implode(',', UsersTokens::$ARR_LOGIN_MOBILE);
        $criteria->addCondition("t.type IN ($sTypeLogin)");
        return self::model()->findAll($criteria);
    }
    
}
