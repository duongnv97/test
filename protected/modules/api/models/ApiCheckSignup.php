<?php

/**
 * This is the model class for table "{{_api_check_signup}}".
 *
 * The followings are the available columns in table '{{_api_check_signup}}':
 * @property string $id
 * @property string $code
 * @property string $ip_address
 * @property string $created_date
 */
class ApiCheckSignup extends BaseSpj
{
    const MAX_REQUEST       = 50;
    const MAX_POST_REQUEST  = 20; // Oct 25, 2015
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{_api_check_signup}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        return array(
            array('count_post_request, id, code, ip_address, created_date', 'safe'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        return array(
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'code' => 'Code',
            'ip_address' => 'Ip Address',
            'created_date' => 'Created Date',
        );
    }

    public function search()
    {
        $criteria=new CDbCriteria;
        $criteria->compare('t.id',$this->id,true);
        $criteria->compare('t.code',$this->code,true);
        $criteria->compare('t.ip_address',$this->ip_address,true);
        $criteria->compare('t.created_date',$this->created_date,true);

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
            'pagination'=>array(
                'pageSize'=> Yii::app()->user->getState('pageSize',Yii::app()->params['defaultPageSize']),
            ),
        ));
    }
    
    /**
     * @Author: ANH DUNG Oct 23, 2015
     * @Todo: make new record
     * check 1 user request chỉ dc 1 lần
     * tránh việc user post nhiều request signup lên server nên ta thêm 1 biến code để kiểm tra request hợp lệ không
     */
    public static function NewRecord() {
        $model = new ApiCheckSignup();
        $model->code = MyFormat::generateSessionIdByModel('ApiCheckSignup', 'code');
        $model->ip_address = MyFormat::getIpUser();
        $model->save();
        return $model->code;
    }
    
    /**
     * @Author: ANH DUNG Oct 23, 2015
     */
    public static function getByCode($code) {
        $criteria = new CDbCriteria;
        $criteria->compare("code", $code);
        return self::model()->find($criteria);
    }
    
    /**
     * @Author: ANH DUNG Oct 23, 2015
     */
    public static function countByIp($ip_address) {
        $criteria = new CDbCriteria;
        $criteria->compare("ip_address", $ip_address);
        return self::model()->count($criteria);
    }
    
    /**
     * @Author: ANH DUNG Oct 23, 2015
     * @Todo: get code of request, nếu chưa có sẽ tạo mới
     */
    public static function getSignupCode($signup_code, $objController) {
        try{
            if(!empty($signup_code)){
                $mCheckSignup = self::getByCode($signup_code);
                if($mCheckSignup){
                    $signup_code = $mCheckSignup->code;
                }else{
                    throw new Exception('Yêu cầu không hợp lệ');
                }
            }else{
                self::CheckIpRequest($objController);
                $signup_code = self::NewRecord();
            }
            return $signup_code;
        } catch (Exception $ex) {
            MyFormat::ApiCatchError($ex, $objController);
        }
    }
    
    /**
     * @Author: ANH DUNG Oct 23, 2015
     * @Todo: kiểm tra chỉ cho phép 1 ip sẽ get 50 code để signup
     * @Param: 
     */
    public static function CheckIpRequest($objController) {
        try{
            $ok = true;
            $ip_address = MyFormat::getIpUser();
            $count = self::countByIp($ip_address);
            if($count > self::MAX_REQUEST){
                $ok = false;
                throw new Exception('Yêu cầu không hợp lệ');
            }
            return $ok;
        } catch (Exception $ex) {
            MyFormat::ApiCatchError($ex, $objController);
        }
    }
    
        
    /**
     * @Author: ANH DUNG Oct 25, 2015
     * @Todo: check valid signup_code, đếm số lần post request xem có vượt giới hạn không
     * tránh việc user post nhiều request signup lên server nên ta thêm 1 biến code để kiểm tra request hợp lệ không
     */
    public static function CheckLimitPostRequest($signup_code, $objController) {
        try{
            if(!empty($signup_code)){
                $mCheckSignup = self::getByCode($signup_code);
                if($mCheckSignup){
                    if($mCheckSignup->count_post_request >= self::MAX_POST_REQUEST){
                        throw new Exception('Yêu cầu không hợp lệ, code 3535, liên hệ admin báo lỗi');
                    }
                    $objController->mCheckSignup = $mCheckSignup;
                }else{
                    throw new Exception('Yêu cầu không hợp lệ, code post not valid');
                }
            }else{
                throw new Exception('Yêu cầu không hợp lệ, code post empty');
            }
            return $signup_code;
        } catch (Exception $ex) {
            MyFormat::ApiCatchError($ex, $objController);
        }
    }
    
    /**
     * @Author: ANH DUNG Oct 25, 2015
     * @Todo: cộng số lần post request cho một signup_code
     * tránh việc user post nhiều request signup lên server nên ta thêm 1 biến code để kiểm tra request hợp lệ không
     */
    public static function PlusPostRequest($objController) {
        if($objController->mCheckSignup){
            $objController->mCheckSignup->count_post_request += 1;
            $objController->mCheckSignup->update(array('count_post_request'));
        }
    }
    
}