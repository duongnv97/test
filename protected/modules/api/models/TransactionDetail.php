<?php

/**
 * This is the model class for table "{{_transaction_detail}}".
 *
 * The followings are the available columns in table '{{_transaction_detail}}':
 * @property string $id
 * @property string $transaction_id
 * @property integer $status
 * @property integer $materials_type_id
 * @property integer $materials_id
 * @property string $qty
 * @property string $price
 * @property string $amount
 * @property string $created_date
 */
class TransactionDetail extends CActiveRecord
{
    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return TransactionDetail the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{_transaction_detail}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        return array(
            array('price_root, kg_empty, kg_has_gas, gas_remain, gas_remain_amount, seri, name, id, transaction_id, status, materials_type_id, materials_id, qty, price, amount, created_date', 'safe'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        return array(
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
            return array(
                'id' => 'ID',
                'transaction_id' => 'Transaction',
                'status' => 'Status',
                'materials_type_id' => 'Materials Type',
                'materials_id' => 'Materials',
                'qty' => 'Qty',
                'price' => 'Price',
                'amount' => 'Amount',
                'created_date' => 'Created Date',
            );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        $criteria=new CDbCriteria;
        $criteria->compare('t.id',$this->id,true);
        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
            'pagination'=>array(
                'pageSize'=> Yii::app()->user->getState('pageSize',Yii::app()->params['defaultPageSize']),
            ),
        ));
    }
    
    
}