<?php
/** @Author: DungNT Feb 02, 2018 
 * @todo: handle api event from web gas24h.com.vn
 * **/
class WebGas24h {
    /** @Author: DungNT Feb 02, 2018
     *  @Todo: gửi mail cho chị Hạnh thông tin ứng viên
     *  @param: $aParams array info
     **/
    public function handleCandidate($aParams) {
        $needMore = [];
        $bodyMail = $this->handleCandidateMapInfo($aParams, $needMore);
        $needMore['title']      = "Ứng viên tuyển dụng {$aParams['first_name']}";
//        $needMore['list_mail']  = ['dungnt@spj.vn', 'ngocpt@spj.vn', 'chaulnm@spj.vn'];
        $needMore['list_mail'][]  = 'dungnt@spj.vn';
        SendEmail::bugToDev($bodyMail, $needMore);
    }
   
    public function handleCandidateMapInfo($aParams, &$needMore) {
        $mAppCache = new AppCache();
        $aProvince = $mAppCache->getListdata('GasProvince', AppCache::LISTDATA_PROVINCE);
        $aDistrict = $mAppCache->getListdata('GasDistrict', AppCache::LISTDATA_DISTRICT);
        
        $mUsersProfile  = new UsersProfile();
        $mUsersProfile->position_apply = $aParams['position_apply'];
        $this->handleCandidateMapEmailManager($aParams, $needMore, $mUsersProfile);
        $gender         = Users::$aGender[$aParams['gender']];
        $province       = isset($aProvince[$aParams['province_id']]) ? $aProvince[$aParams['province_id']] : '';
        $district       = isset($aDistrict[$aParams['district_id']]) ? $aDistrict[$aParams['district_id']] : '';
        
        $bodyMail       = "<b>Thông tin ứng viên tuyển dụng từ web gas24h.com.vn </b>";
        $bodyMail       .= "<br><br>";
        $bodyMail       .= "<p><b>Vị trí ứng tuyển</b>: ".$mUsersProfile->getPositionApply().'</p>';
        $bodyMail       .= "<p><b>Tên ứng viên</b>: {$aParams['first_name']}</p>";
        $bodyMail       .= "<p><b>Giới tính</b>: $gender</p>";
        $bodyMail       .= "<p><b>Số CMND</b>: {$aParams['id_number']}</p>";
        $bodyMail       .= "<p><b>Điện thoại</b>: {$aParams['phone']}</p>";
        $bodyMail       .= "<p><b>Nơi ở hiện tại</b>: $district, $province</p>";
        $bodyMail       .= "<p><b>Ngày gửi</b>: ".date('d/m/Y H:i').'</p>';
        $bodyMail       .= "<br><b>Bộ phận tuyển dụng gọi xác nhận với ứng viên, sau đó tạo thông tin ứng viên trên web spj.daukhimiennam.com</b>";
        return $bodyMail;
    }
    
    /** @Author: DungNT May 08, 2018
     *  @Todo: set email người quản lý ứng với vị trí tuyển dụng
     **/
    public function handleCandidateMapEmailManager($aParams, &$needMore, $mUsersProfile) {
        $aPosition = $mUsersProfile->getManagerPosition();
        $haveEmail = false;
        if(isset($aPosition[$mUsersProfile->position_apply])){
            $aModelUserMail = Users::getOnlyArrayModelByArrayId($aPosition[$mUsersProfile->position_apply]);
            foreach($aModelUserMail as $mUser){
                $haveEmail = true;
                $needMore['list_mail'][] = $mUser->email;
            }
        }
        if(!$haveEmail){
            $needMore['list_mail'][] = 'chaulnm@spj.vn';
        }
    }

    
}