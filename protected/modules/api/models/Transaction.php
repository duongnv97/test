<?php
            
/**
 * This is the model class for table "{{_transaction}}".
 *
 * The followings are the available columns in table '{{_transaction}}':
 * @property string $id
 * @property string $transaction_key
 * @property integer $status
 * @property string $expiry_date
 * @property string $first_name
 * @property string $customer_id
 * @property integer $province_id
 * @property integer $district_id
 * @property integer $ward_id
 * @property integer $street_id
 * @property string $house_numbers
 * @property string $ip_address
 * @property string $created_date
 * @property string $agent_id
 */
class Transaction extends BaseSpj
{
    public $isFirstOrder = false, $directionInput = 0, $listdataMaterial = [], $mTransactionHistory = null, $aDetail = [], $autocomplete_name;
    const STATUS_NEW        = 1;
    const STATUS_COMPLETE   = 2;
    const STATUS_TRY_APP    = 3;
    const STATUS_PRICE_HIGH = 4;
    const STATUS_OTHER      = 5;
    const STATUS_PROCESSING = 6;// đang xử lý, là lúc KTBH xác nhận với KH xong và tạo dc đơn hàng, nhưng chưa có GN
    const STATUS_DELIVERY_PROCESSING    = 7;// đang giao hàng
    const STATUS_CANCEL_BY_CUSTOMER     = 8;// KH hủy +> Không thành công
    const STATUS_CANCEL_BY_EMPLOYEE     = 9;// NV GN hủy => Không thành công
    const STATUS_DONE                   = 10;// xử lý xong đơn hàng

    const EMPLOYEE_CHANGE_AGENT     = -2;// GN CHANGE AGENT
    const EMPLOYEE_CONFIRM          = -1;// Feb 11, 2017 điều phối xác nhận đơn hàng, bên Sell không dùng Status này, cái này của AppOrder
    const EMPLOYEE_FREE         = 0;// new chưa có nhân viên nào nhận
    const EMPLOYEE_NHAN_GIAO_HANG        = 1;// nhận đơn hàng
    const EMPLOYEE_HUY_GIAO_HANG = 2;// HỦy nhận đơn hàng
    const EMPLOYEE_DROP         = 3;// Hủy không lấy gas
    const EMPLOYEE_CHANGE       = 4;// change đơn hàng gas
    const EMPLOYEE_COMPLETE     = 5;// done thu tiền
    const EMPLOYEE_CHANGE_NOT_PAID  = 6;// nhân viên đổi trạng thái chưa thu tiền đơn hàng để giao nhận chỉnh sửa lại đơn hàng
    const CALLCENTER_CHANGE         = 7;// callcenter change đơn hàng trên web
    const CHANGE_DISCOUNT           = 8;// CV nhập giảm giá
    const CUSTOMER_DROP             = 9;// Kh hủy luôn đơn
    const TYPE_RESET_PASS_CUSTOMER  = 10;// For KH bò mối - Web Reset pass default 

    const EMPLOYEE_CHANGE_GAS   = 1;// change GAs là change_type ở order/transactionSetEvent
    const EMPLOYEE_CHANGE_GIFT  = 2;// change quà
    const EMPLOYEE_CHANGE_VO    = 3;// vỏ

    const ORDER_COMPLETE    = 1;
    const ORDER_CANCEL      = 2;
    const ORDER_PROCESSING  = 3; // đang xử lý


    const STEP_NORMAL   = 1;
    const STEP_SHORT    = 2;// đặt hàng ngay

    const STATUS_READ_NEW       = 1;// mới
    const STATUS_READ_CONFIRM   = 2;// đã đọc

    const CANCEL_WRONG_INFO     = 1;// sai thông tin
    const CANCEL_PRICE_HIGHT    = 2;// giá cao
    const CANCEL_WORNG_GIFT     = 3;// sai quà
    const CANCEL_SLOW_DELIVERY  = 4;// giao chậm
    const CANCEL_OTHER          = 5;// khác
    const CANCEL_DONOT_BU_VO    = 6;// không bù vỏ
    const CANCEL_CALL_2_BEN     = 7;// KH gọi 2 bên
    const CANCEL_WRONG_GAS      = 8;// Mang nhầm gas

    const CANCEL_GIAO_XA        = 9;// giao xa
    const CANCEL_KH_DI_VANG     = 10;// KH đi có việc
    const CANCEL_KH_CON_GAS     = 11;// KH còn gas
    const CANCEL_AGENT_WRONG    = 12;// Nhầm đại lý
    const CANCEL_DIEU_SAI       = 13;// Điều sai
    const CANCEL_CUSTOMER_DEBIT = 14;// KH nợ tiền
    const CANCEL_DOUBLE_ORDER   = 15;// Điều 2 lần
    const CANCEL_CANNOT_CALL    = 16;// Gọi khách không nghe máy
    const CANCEL_KHM            = 17;// Khách hàng mối
    const CANCEL_BY_APP_CUSTOMER        = 18;// Khách hàng hủy app
    const CANCEL_TEST_APP               = 19;
    const CANCEL_SUPPORT_AGENT_BUSY     = 20;// Hỗ trợ chuyển đại lý
    const CANCEL_DOUBLE_CALL            = 21;// KH gọi 2 lần
    const CANCEL_MAKE_NEW_ORDER         = 22;// Đã giao lại được
    const CANCEL_MONITOR_RECHECK        = 23;// Giám sát kiểm tra
    const CANCEL_KHONGCO_NHU_CAU        = 24;// KH Không có nhu cầu

    public function getArrayStatusCancel() {
        return array(
            self::CANCEL_DONOT_BU_VO    => 'Không bù vỏ',
            self::CANCEL_CALL_2_BEN     => 'KH gọi 2 bên',
            self::CANCEL_WRONG_GAS      => 'Mang nhầm gas',
            self::CANCEL_WRONG_INFO     => 'Sai địa chỉ',
            self::CANCEL_PRICE_HIGHT    => 'Giá cao',
            self::CANCEL_WORNG_GIFT     => 'Sai quà',
            self::CANCEL_SLOW_DELIVERY  => 'Giao chậm',
//            self::CANCEL_OTHER          => 'Khác',
            self::CANCEL_GIAO_XA        => 'Giao xa',
            self::CANCEL_KH_DI_VANG     => 'KH đi có việc',
            self::CANCEL_KH_CON_GAS     => 'KH còn gas',
            self::CANCEL_DIEU_SAI       => 'Điều sai loại gas',
            self::CANCEL_CUSTOMER_DEBIT => 'KH nợ tiền',
            self::CANCEL_AGENT_WRONG            => 'Tổng đài điều nhầm đại lý',
            self::CANCEL_DOUBLE_ORDER           => 'Tổng đài điều 2 lần',
            self::CANCEL_CANNOT_CALL            => 'Gọi khách không nghe máy',
            Transaction::CANCEL_SUPPORT_AGENT_BUSY     => 'Nhờ đại lý khác giao hỗ trợ',
            Transaction::CANCEL_KHM             => 'Khách hàng mối',
            Transaction::CANCEL_BY_APP_CUSTOMER => 'Khách hàng hủy app',
            Transaction::CANCEL_TEST_APP        => 'Test app',
            Transaction::CANCEL_DOUBLE_CALL     => 'KH gọi 2 lần',
            Transaction::CANCEL_MAKE_NEW_ORDER  => 'Đã giao lại được',
            Transaction::CANCEL_MONITOR_RECHECK => 'Giám sát kiểm tra',
            Transaction::CANCEL_KHONGCO_NHU_CAU => 'KH Không có nhu cầu',
        );
    }

    public function getAppStatusCancel() {
        $aStatus = $this->getArrayStatusCancel();
        unset($aStatus[Transaction::CANCEL_BY_APP_CUSTOMER]);
        unset($aStatus[Transaction::CANCEL_MAKE_NEW_ORDER]);
        unset($aStatus[Transaction::CANCEL_MONITOR_RECHECK]);
        
        unset($aStatus[Transaction::CANCEL_WRONG_GAS]);
        unset($aStatus[Transaction::CANCEL_WRONG_INFO]);
        unset($aStatus[Transaction::CANCEL_WORNG_GIFT]);
        unset($aStatus[Transaction::CANCEL_DIEU_SAI]);
        return $aStatus;
    }

    /**
     * @Author: DungNT Feb 19, 2017
     * @Todo: những status ở tab new trên app
     */
    public function getAppStatusNew() {
        return [
            Transaction::STATUS_NEW,
            Transaction::STATUS_COMPLETE,
            Transaction::STATUS_PROCESSING,
            Transaction::STATUS_DELIVERY_PROCESSING,
        ];
    }

//    const EMPLOYEE_CONFIRM     = -1;// Feb 11, 2017 điều phối xác nhận đơn hàng, bên Sell không dùng Status này, cái này của AppOrder
//    const EMPLOYEE_FREE        = 0;// new chưa có nhân viên nào nhận
//    const EMPLOYEE_NHAN_GIAO_HANG        = 1;// nhận đơn hàng
//    const EMPLOYEE_HUY_GIAO_HANG = 2;// HỦy nhận đơn hàng
//    const EMPLOYEE_DROP        = 3;// Hủy không lấy gas
//    const EMPLOYEE_CHANGE      = 4;// change đơn hàng gas
//    const EMPLOYEE_COMPLETE    = 5;// done thu tiền
//    const EMPLOYEE_CHANGE_NOT_PAID   = 6;// nhân viên đổi trạng thái chưa thu tiền đơn hàng để giao nhận chỉnh sửa lại đơn hàng

    public function getArrayActionType() {
        return [
            self::EMPLOYEE_CONFIRM          => 'Điều phối xác nhận',
            self::EMPLOYEE_FREE             => 'ĐH mới',
            self::EMPLOYEE_NHAN_GIAO_HANG   => 'Nhận giao hàng',
            self::EMPLOYEE_HUY_GIAO_HANG    => 'Hủy nhận giao hàng',
            self::EMPLOYEE_DROP             => 'Hủy đơn hàng',
            self::EMPLOYEE_COMPLETE         => 'Hoàn thành ĐH',
            self::EMPLOYEE_CHANGE_NOT_PAID  => 'Click đơn hàng nợ',
            self::EMPLOYEE_CHANGE_AGENT     => 'Đổi đại lý giao',
            self::CALLCENTER_CHANGE         => 'Tổng đài cập nhật Web',
            self::CHANGE_DISCOUNT           => 'Thay đổi tiền giảm giá',
            self::CUSTOMER_DROP             => 'Khách hàng hủy',
            self::TYPE_RESET_PASS_CUSTOMER  => 'Reset mật khẩu KH bò mối',
        ];
    }
    public function getArrayActionTypeBoMoi() {
        return [
            Transaction::EMPLOYEE_NHAN_GIAO_HANG   => 'Nhận giao hàng',
            Transaction::EMPLOYEE_HUY_GIAO_HANG    => 'Hủy nhận giao hàng',
            GasAppOrder::STATUS_CANCEL      => 'Hủy đơn hàng',
            GasAppOrder::STATUS_COMPPLETE   => 'Hoàn thành ĐH',
            GasAppOrder::WEB_SET_STATUS_NEW => 'Web chuyển trạng thái mới',
            GasAppOrder::WEB_SET_ORDER_DEBIT => 'App set đơn hàng nợ',
            GasAppOrder::WEB_PAY_DEBIT      => 'App thu tiền nợ KH',
            GasAppOrder::WEB_UPDATE         => 'Web Cập nhật',
            Transaction::EMPLOYEE_CONFIRM   => 'Tổng đài xác nhận App',
        ];
    }

    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return Transaction the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{_transaction}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules(){
        return array(
            array('transaction_key', 'required', 'on' => 'AndroidCreate'),
            array('agent_id', 'required', 'on' => 'AndroidCreate', 'message'=>'Không có đại lý trong khu vực của bạn'),
            array('email', 'email', 'on' => 'AndroidCreate'),
            array('phone', 'checkPhone', 'on' => 'AndroidCreate, TransactionConfirm'),
            array('google_map, agent_id, email, id, transaction_key, status, expiry_date, first_name, phone, province_id, district_id, ward_id, street_id, house_numbers, ip_address, created_date', 'safe'),
            array('qty_discount, amount_bu_vo, google_address, device_phone, customer_id, type_customer', 'safe'),
        );
    }

    /**
     * @Author: DungNT Nov 05, 2016
     * @Todo: xử lý check length phone có hợp lệ ko
     *  có thể sẽ xử lý check chỉ cho phép số di động
     */
    public function checkPhone($attribute, $params){
        if(!empty($this->phone)){
            $mSms = new GasScheduleSms();
            $phone = $this->phone;
            if(!$mSms->isPhoneRealLength($phone)){
                $this->addError('phone','Số điện thoại không hợp lệ, dư hoặc thiếu số');
                return ;
            }
            $this->phone = $phone;
//            if(!GasScheduleSms::isValidPhone($this->phone)){// cho phép số máy bàn
//                $this->addError('phone','Số điện thoại phải là số di động');
//            }
        }
    }

    public function relations(){
        return array(
            'rAgent' => array(self::BELONGS_TO, 'Users', 'agent_id'),
            'rCustomer' => array(self::BELONGS_TO, 'Users', 'customer_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'transaction_key' => 'Transaction Key',
            'status' => 'Status',
            'expiry_date' => 'Expiry Date',
            'first_name' => 'Họ tên',
            'phone' => 'Điện thoại',
            'province_id' => 'Province',
            'district_id' => 'District',
            'ward_id' => 'Ward',
            'street_id' => 'Street',
            'house_numbers' => 'House Numbers',
            'ip_address' => 'Ip Address',
            'created_date' => 'Ngày tạo',
            'address' => 'Địa chỉ',
            'agent_id' => 'Đại lý',
            'note' => 'Ghi chú',
        );
    }

    public function search(){
        $criteria=new CDbCriteria;
        $cRole = MyFormat::getCurrentRoleId();
        if($cRole == ROLE_SUB_USER_AGENT){
            $criteria->addCondition('t.agent_id', MyFormat::getAgentId());
        }
        if(!empty($this->agent_id)){
            $criteria->addCondition('t.agent_id='.$this->agent_id);
        }
        $criteria->compare("t.phone", trim($this->phone), true);
        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
            'pagination'=>array(
                'pageSize'=> 50,
            ),
        ));
    }

    /**
     * @Author: DungNT Oct 24, 2016
     * @Todo: gen new transaction_key for one request
     */
    public function genNewTransactionKey() {
        $this->ip_address       = MyFormat::getIpUser();
        $sUnique = time().$this->ip_address.ActiveRecord::randString(10);
        $this->transaction_key  = md5($sUnique);
    }

    /**
     * @Author: DungNT Oct 24, 2016
     * @Todo: get one transaction by id and key
     */
    public static function getByIdAndKey($q, $customer_id) {
        $criteria = new CDbCriteria();
        $criteria->compare("t.id", $q->session_id);
        $criteria->compare("t.transaction_key", $q->session_key);
        $criteria->compare("t.customer_id", $customer_id);
        return self::model()->find($criteria);
    }
    
    /**
     * @Author: DungNT Dec 04, 2017
     * @Todo: get one transaction by customer_id
     */
    public function getByCustomer() {
        $criteria = new CDbCriteria();
        $criteria->addCondition('t.customer_id=' . $this->customer_id);
        return self::model()->find($criteria);
    }

    /**
     * @Author: DungNT Oct 24, 2016
     * @Todo: trim data
     */
    public function handlePostFormatData() {
        $this->first_name       = trim($this->first_name);
        $this->phone            = trim($this->phone);
        $this->province_id      = trim($this->province_id);
        $this->district_id      = trim($this->district_id);
        $this->ward_id          = trim($this->ward_id);
        $this->street_id        = trim($this->street_id);
        $this->house_numbers    = trim($this->house_numbers);
        $this->note             = trim($this->note);
        $this->email            = trim($this->email);
    }
    /**
     * @Author: DungNT Oct 24, 2016
     * @Todo: handle get post client
     */
    public function handlePostConfirm($q) {
        $this->scenario = 'TransactionConfirm';
        $this->status   = Transaction::STATUS_COMPLETE;
        $this->phone    = isset($q->phone) ? MyFormat::escapeValues($q->phone) : '';
        $this->google_address    = isset($q->google_address) ? MyFormat::escapeValues($q->google_address) : '';
        $this->address  = $this->google_address;
        $this->validate();
    }

    /**
     * @Author: DungNT Oct 24, 2016
     * @Todo: handle get post client
     */
    public function handlePost($q) {
        $this->transaction_type = MyFormat::escapeValues($q->transaction_type);
        $this->first_name       = MyFormat::escapeValues($q->first_name);
        $this->phone            = MyFormat::escapeValues($q->phone);
        $this->device_phone     = MyFormat::escapeValues($q->device_phone);
//        $this->province_id      = MyFormat::escapeValues($q->province_id);
//        $this->district_id      = MyFormat::escapeValues($q->district_id);
//        $this->ward_id          = MyFormat::escapeValues($q->ward_id);
//        $this->street_id        = MyFormat::escapeValues($q->street_id);
//        $this->house_numbers    = MyFormat::escapeValues($q->house_numbers);
        $this->note             = MyFormat::escapeValues($q->note);// Jun 30, 2016 xử lý get biến note ở đây cho cả create + update

        $this->google_address   = isset($q->google_address) ? MyFormat::escapeValues($q->google_address) : '';
        $this->email            = MyFormat::escapeValues($q->email);
        $this->agent_id         = MyFormat::escapeValues($q->agent_id);
        $this->setInfoFromOrderOld();
        $this->province_id      = MyFormat::convertAgentToProvinceId($this->agent_id);// Aug1918 fix lỗi BT2 không lấy đc giảm giá TPHCM 60k
        $this->google_map       = MyFormat::escapeValues($q->latitude).','.MyFormat::escapeValues($q->longitude);
        $this->setCustomerInfo();
        $this->handlePostFormatData();
        $this->validate();
        if($this->scenario == 'AutoCreateOrder'){
            return ;
        }
        $this->getPostDetail($q);
        $this->checkDiscount();
    }
    
    /** @Author: DungNT Now 30, 2017
     *  @Todo: set customer name if empty
     **/
    public function setCustomerInfo() {
        if(!empty($this->first_name)){
            return ;
        }
        $mCustomer = $this->rCustomer;
        if($mCustomer){
            $this->first_name       = $mCustomer->first_name;
            $this->type_customer    = $mCustomer->is_maintain;
        }
    }
    
    /** @Author: DungNT Dec 17, 2018
     *  @Todo: override old agent_id if is order second, do khi KH đứng gần vị trí 1 đại lý khác ĐL cũ, hệ thống sẽ tính sai giá nếu ở 2 tỉnh khác nhau:
     *  @case: S187FNS8A KH quận 9 đứng ở gần ĐL Bình Đường đặt, hệ thống tính sai giá của Bình Gas 
     *  Do đó phải override agent_id cũ ở đây
     **/
    public function setInfoFromOrderOld() {
        $mTransactionHistory                = new TransactionHistory();
        $mTransactionHistory->customer_id   = $this->customer_id;
        $mTransactionHistoryOld             = $mTransactionHistory->getLastestHistory();
        if(!is_null($mTransactionHistoryOld)){
            $this->agent_id = $mTransactionHistoryOld->agent_id;
        }
    }
    
    /** @Author: DungNT Aug 18, 2018
     *  @Todo: auto add vỏ cho KH đặt lần 2
     **/
    public function autoAddVoCustomerOld() {
        try{
        $mSell = new Sell();
        $mSell->customer_id = $this->customer_id;
        $mDetailVo = $mSell->autoAddVoGetData();
        if(empty($mDetailVo)){
            return null;
        }
        $this->addItemVo($mDetailVo);
        } catch (Exception $ex) {
            Logger::WriteLog("Model Transaction autoAddVoCustomerOld: Telesale check error ".$ex->getMessage());
            return ;
        }
    }
    
    /** @Author: DungNT Aug 18, 2018
     *  @Todo: add Item vo
     **/
    public function addItemVo($mDetailVo) {
        $mDetail                    = new TransactionDetail($this->scenario);
        $mDetail->transaction_id    = $this->id;
        $mDetail->status            = $this->status;
        $mDetail->materials_type_id = $mDetailVo->materials_type_id;
        $mDetail->materials_id      = $mDetailVo->materials_id;
        $mDetail->qty               = 1;
        $mDetail->price             = 0;
        $mDetail->amount            = 0;
        $mDetail->created_date      = '';
        $mDetail->name              = isset($this->listdataMaterial[$mDetail->materials_id]) ? $this->listdataMaterial[$mDetail->materials_id] : '';
        $mDetail->price_root        = 0;
//        $this->aDetail[]            = $mDetail;
        array_unshift($this->aDetail , $mDetail);
    }

    /**
     * @Author: DungNT Oct 24, 2016
     * map attribute to model detail form $q. get post and validate
     */
    public function getPostDetail($q) {
        try{
        $mAppCache = new AppCache();
        $this->listdataMaterial = $mAppCache->getListdata('GasMaterials', AppCache::LISTDATA_MATERIAL);
        $aDataCache     = $mAppCache->getPriceHgd(AppCache::PRICE_HGD);
        $priceHgd       = MyFunctionCustom::getHgdPriceSetup($aDataCache, $this->agent_id);
        $hasError       = true;
        $this->aDetail  = [];
        $mAppOrder      = new AppOrder();
        $mMaterial      = new GasMaterials();
        $Gas24hApplyPrice   = Yii::app()->setting->getItem('Gas24hApplyPrice');
        $Gas24hPrice        = $mMaterial->getAppPrice();

        $mAppOrder->Gas24hPrice     = $Gas24hPrice;
        $mAgent                     = Users::model()->findByPk($this->agent_id);
        $mSell                      = $mAppOrder->initModelSell($mAgent);

        foreach($q->order_detail as $objDetail){
            $mDetail                    = new TransactionDetail($this->scenario);
            $mDetail->transaction_id    = $this->id;
            $mDetail->status            = $this->status;
            $mDetail->materials_type_id = MyFormat::escapeValues($objDetail->materials_type_id);
            $mDetail->materials_id      = MyFormat::escapeValues($objDetail->materials_id);
            $mDetail->qty               = MyFormat::removeComma(MyFormat::escapeValues($objDetail->qty));
            if($mDetail->qty < 1){
                $mDetail->qty = 1;
            }
//            $mDetail->price             = MyFormat::removeComma(MyFormat::escapeValues($objDetail->price));
            $price = isset($priceHgd[$mDetail->materials_id]) ? $priceHgd[$mDetail->materials_id] : 0; 
            $mDetail->price_root         = $price;// Add May2818
            if($Gas24hApplyPrice == 'yes' && $mDetail->materials_type_id == GasMaterialsType::MATERIAL_BINH_12KG){
                $mSellDetail = new SellDetail();
                $mSellDetail->agent_id          = $mAgent->id;
                $mSellDetail->materials_type_id = $objDetail->materials_type_id;
                $mSellDetail->price             = $price;
                $mSellDetail->price_root        = $price;
                $mSell->calcPriceItemGasApp($mSellDetail);
                $priceTmp = $mSellDetail->price;
                $mAppOrder->writeLogError($priceTmp, $objDetail->materials_id);
                $mSellDetail->price = $priceTmp;
            }

            $mDetail->price             = $mSellDetail->price;
            $mDetail->amount            = $mDetail->qty*$mDetail->price;
            $mDetail->created_date      = '';
            $mDetail->name              = isset($this->listdataMaterial[$mDetail->materials_id]) ? $this->listdataMaterial[$mDetail->materials_id] : '';
            $mDetail->validate();
            if(!$mDetail->hasErrors()){
                $this->aDetail[] = $mDetail;
                $hasError = false;
            }
        }

        if(!$this->isFirstOrder && $hasError && !$this->hasErrors()){
            $this->addError('id',"Chi tiết đơn hàng không hợp lệ");
            return ;
        }
//        $this->autoAddVoCustomerOld();// Aug1818 không thể thêm ở đây đc, nên thêm ở đoạn copy to Sell - Au1818 chưa xử lý đc
        } catch (Exception $ex) {
            throw new Exception($ex->getMessage());
        }
    }

    /**
     * @Author: DungNT Dec 08, 2016
     * @Todo: Xác định trong 1 order HGD có lấy KM hay không, nếu Không chọn KM dánh dấu cờ qty_discount > 0 có chiết khấu
     */
    public function checkDiscount() {// Kiểm tra có chiết khấu hay không
        $qtyPromotion       = 0;
        $qtyGas             = 0;
        $promotionVanDay    = false;
        foreach($this->aDetail as $mDetail){
            if(in_array($mDetail->materials_type_id, GasMaterialsType::$ARR_WINDOW_PROMOTION)){
                $qtyPromotion           += $mDetail->qty;
            }elseif(in_array($mDetail->materials_type_id, GasMaterialsType::$SETUP_PRICE_GAS_12)){
                $qtyGas                 += $mDetail->qty;
            }elseif(in_array($mDetail->materials_type_id, GasMaterialsType::$ARR_WINDOW_VO)){
            }
            if($mDetail->price == 0 && in_array($mDetail->materials_type_id, GasMaterialsType::$ARR_WINDOW_DISCOUNT_LIKE_PROMOTION)){
                $promotionVanDay    = true;
            }
        }
        $qty_discount = $qtyGas - $qtyPromotion;
        if($promotionVanDay){// Now 25, 2016 nếu tặng val hoặc dây rồi thì nên ck = 0
            $qty_discount = 0;
        }// Sep 26, 2016 vì đã cho sửa ck ở dưới nên cách tính ck giống đơn hàng BT chứ ko tính như trên nữa
        if($qty_discount > 0){
            $this->qty_discount = $qty_discount;
        }else{
            $this->qty_discount = 0;
        }
        
        $this->qty_discount = 0;// Now1817 xử lý không có CK của đơn hàng đặt APP gas24h
    }

    /**
     * @Author: DungNT Nov 25, 2016
     * @Todo: xử lý add promotion nếu có của user
     */
    public function applyPromotion() {
        /* 1. find ben AppPromotionUser xem user có promotion nào hợp lệ không
         * 2. map promotion đó vào, update đã use promotion và lock Promotion lại 
         * -- next xử lý API add promotion của user
         */
        $mPromotionUser             = new AppPromotionUser();
        $mPromotionUser->user_id    = $this->customer_id;
        $mPromotionUser             = $mPromotionUser->getActivePromotion();
        if(is_null($mPromotionUser) || $this->isAgentLockPromotion()){
            return ;
        }
        $this->applyPromotionDetail($mPromotionUser);
        // sẽ không apply promotion ở đây, chỉ map vào thôi, bước confirm mới apply Promotion vào
//        $mPromotionUser->setUserApply();
    }
    
    /** @Author: DungNT Aug 14, 2017
     * @Todo: xử lý check các kiểu apply promotion
     */
    public function applyPromotionDetail($mPromotionUser) {
        $this->app_promotion_user_id    = $mPromotionUser->id;
        $this->promotion_id             = $mPromotionUser->promotion_id;
        $this->promotion_amount         = $mPromotionUser->promotion_amount;
        $this->promotion_type           = $mPromotionUser->promotion_type;
        $total = $this->getTotalBeforePromotion();
        if($mPromotionUser->promotion_type == AppPromotion::TYPE_REFERENCE_USER){
            if($total <= $mPromotionUser->promotion_amount){
                $this->promotion_amount = $total;
            }
        }elseif($mPromotionUser->promotion_type == AppPromotion::TYPE_FREE_ONE_GAS){
            $this->promotion_amount = $total;
        }
    }
    
    /** @Author: DungNT Jan 19, 2019
     *  @Todo: 
     *  @Param: 
     **/
    public function isAgentLockPromotion() {
        $mPromotion = new AppPromotion();
        return in_array($this->mAppUserLogin->area_code_id, $mPromotion->getAgentLockPromotion());
    }
    
    /**
     * @Author: DungNT Nov 28, 2016
     * @Todo: xử lý chính thức add promotion hợp lệ nếu có của user
     * bên trên chỉ là copy thông tin promotion vào để user view thấy, còn khi user confirm thì mới tính apply
     */
    public function applyPromotionConfirm() {
//        GasCheck::randomSleep(); Jan2519 DungNT close, do thấy không cần thiết sử dụng
        $mPromotionUser             = new AppPromotionUser();
        $mPromotionUser->user_id    = $this->customer_id;
        $mPromotionUser             = $mPromotionUser->getActivePromotion();
        if(is_null($mPromotionUser) || $this->isAgentLockPromotion()){
            $this->applyPromotionReset();
            return ;
        }
        $mPromotionUser->modelOld = clone $mPromotionUser; // Jan2619 for save log
        if($mPromotionUser->promotion_type == AppPromotion::TYPE_REFERENCE_USER){
            $mPromotionUser->promotion_amount -= $this->promotion_amount;// trừ tiền trong tài khoản KM giới thiệu
        }
        $mPromotionUser->setUserApply();
        // Jan2619 dùng tạm biến appAction để truyền user_id từ ngoài vào saveLogUpdate
        $mPromotionUser->appAction  = $this->customer_id;
        $mPromotionUser->saveLogUpdate(LogUpdate::TYPE_6_APPLY_PROMOTION);
    }

    /**
     * @Author: DungNT Nov 28, 2016
     * @Todo: xử lý xóa promotion trong 1 transaction
     * khi có 2 request có cùng 1 mã promotion nếu cái nào apply trc thì sẽ dc nhận promotion, cái nào sau thì sẽ không được apply
     */
    public function applyPromotionReset() {
        $this->app_promotion_user_id    = 0;
        $this->promotion_id             = 0;
        $this->promotion_amount         = 0;
        $this->promotion_type           = 0;
        $this->setGrandTotal();
//        $this->update();// có thể không cần update ở đây nữa, vì bên ngoài sẽ delete luôn rồi
    }

    /**
     * @Author: DungNT Nov 02, 2016
     */
    public function handleBuildJsonDetail() {
        $this->setTotal();
        $this->setGrandTotal();
    }

    public function setTotal() {
        $aJson      = [];
        $sumAmount  = 0;
        foreach($this->aDetail as $mDetail){
            $aJson[] = $mDetail->getAttributes();
            $sumAmount  += ($mDetail->amount*1);
        }
        $this->json_detail  = json_encode($aJson, JSON_UNESCAPED_UNICODE);
        // tính toán total và discount của promotion
        $this->total        = $sumAmount;
    }
    
    /**
     * @Author: DungNT Feb 11, 2017
     */
    public function setGrandTotal() {
        $this->grand_total  = $this->total - $this->promotion_amount - ($this->qty_discount * Sell::PROMOTION_DISCOUNT);
    }
    public function getTotalBeforePromotion() {
        $this->setTotal();
        return $this->total - ($this->qty_discount * Sell::PROMOTION_DISCOUNT);
    }

    /**
     * @Author: DungNT Oct 24, 2016
     * save record from window
     */
    public function handleSave() {
        $this->applyPromotion();
        $this->handleBuildJsonDetail();
//        $this->buildAddress();// Dec 17, 2016 không xử lý hàm này nữa
        $this->update();
        // Now 02, 2016 không save vào db nữa mà đưa vào json, sau khi KTBH confirm đơn hàng thì mới xử lý tiếp
//        $aRowInsert=[];
//        foreach($this->aDetail as $mDetail){
//            $this->handleSaveDetailBuildSql($aRowInsert, $mDetail);
//        }
//        MyFormat::deleteAllByRootId($this->id, 'TransactionDetail', 'transaction_id');
//        $this->handleSaveDetailRunSql($aRowInsert);
    }

    /**
     * @Author: DungNT Nov 05, 2016
     * @Todo: copy row Transaction to history
     */
    public function copyToHistory($isSave = true) {
        $mHistory = new TransactionHistory();
        $aFieldNotCopy = array('id');
        // handle add promotion of user
        MyFormat::copyFromToTable($this, $mHistory, $aFieldNotCopy);
        $mHistory->transaction_id   = $this->id;
        $mHistory->source           = Sell::SOURCE_APP;
        if($this->mTransactionHistory){
            $mHistory->employee_maintain_id         = $this->mTransactionHistory->employee_maintain_id;
            $mHistory->employee_accounting_id       = $this->mTransactionHistory->employee_accounting_id;
            $mHistory->action_type                  = $this->mTransactionHistory->action_type;
            $mHistory->status                       = $this->mTransactionHistory->status;
            $mHistory->note                         = 'GN Trả thẻ';
        }
        if($this->directionInput == Sell::SOURCE_APP){// Sep3018 fix assign random user CallCenter
//            $mHistory->employee_accounting_id       = $mHistory->getRandomUserHgdOnline();
            //16h05 Sep3018 bất cập nhảy không đều, khi NV DND điện thoại ra ngoài thì không xử lý được đơn. => Thả cho ai rảnh thì nhận đơn
        }
        
        if($this->scenario == 'AutoCreateOrder'){
            $mHistory->source       = Sell::SOURCE_WEB;
        }
        if ($isSave){
            // Decide save or not
            $mHistory->save();
        }
        return $mHistory;
    }

    /**
     * @Author: DungNT Nov 25, 2016
     * @Todo: xử lý copy sang TransactionHistory phục vụ view lại Order, muốn sử dụng chung code với model TransactionHistory
     */
    public function copyToHistoryViewOnly() {
        $mHistory = new TransactionHistory();
        $aFieldNotCopy = array('id');
        // handle add promotion of user
        MyFormat::copyFromToTable($this, $mHistory, $aFieldNotCopy);
        $mHistory->transaction_id = $this->id;
        return $mHistory;
    }

    /**
     * @Author: DungNT Nov 25, 2016
     * @Todo: cron delete những Order không hoàn thành nếu tạo từ 2h trc mà chưa complete
     * doing......
     */
    public function cronDeleteDraft() {
        $hours = 2;
        $criteria = new CDbCriteria();
        $criteria->addCondition("DATE_ADD(created_date,INTERVAL $hours HOUR) < NOW()");
        $models = Transaction::model()->findAll($criteria);
        foreach($models as $model){
            $model->delete();
        }
    }

    /**
     * @Author: DungNT Oct 24, 2016
     * @Todo: one query to insert detail
     */
    public function handleSaveDetailBuildSql(&$aRowInsert, $mDetail) {
        $aRowInsert[] = "('$mDetail->transaction_id',
                '$mDetail->status',
                '$mDetail->materials_type_id',
                '$mDetail->materials_id',
                '$mDetail->qty',
                '$mDetail->price',
                '$mDetail->amount',
                '$mDetail->created_date'
            )";
    }

    /**
     * @Author: DungNT Oct 24, 2016
     * @Todo: Save one query to insert detail
     */
    public function handleSaveDetailRunSql($aRowInsert) {
        $tableName  = TransactionDetail::model()->tableName();
        $sql        = "insert into $tableName (
                        transaction_id,
                        status,
                        materials_type_id,
                        materials_id,
                        qty,
                        price,
                        amount,
                        created_date
                        ) values ".implode(',', $aRowInsert);
        if(count($aRowInsert)>0){
            Yii::app()->db->createCommand($sql)->execute();
        }
    }

    /**
     * @Author: DungNT Nov 05, 2016
     * @Todo: dev test function copy
     */
    public static function testCopy() {
        $mTransaction = self::model()->findByPk(1);
        $mTransaction->copyToHistory();
        die;
    }

    public function getAgent() {
        $mAppCache = new AppCache();
        $aAgent = $mAppCache->getAgent();
        return isset($aAgent[$this->agent_id]) ? $aAgent[$this->agent_id]['first_name'] : '';
    }

    public function getFirstName() {
        return $this->first_name;
    }

    public function getEmail() {
        return $this->email;
    }
    public function getPhone() {
        return '0'.$this->phone;
    }
    public function getPhoneSms() {
        return '0'.$this->phone;
    }
    public function getAddress() {
        return $this->address;
    }
    public function getNote() {
        return nl2br($this->note);
    }
//    public function getCreatedDate() {
//        return MyFormat::dateConverYmdToDmy($this->created_date, 'd/m/Y H:i');
//    }
    public function getTextInfoMap() {
        return '<b>'.$this->getFirstName().'</b><br>ĐT: '.$this->getPhone().'<br>Đ/C: '.$this->getAddress();
    }

    protected function beforeSave() {
        return parent::beforeSave();
    }

    /**
     * @Author: DungNT Nov 05, 2016
     */
    public function buildAddress() {
        try{
//        $from = time();
        $address = '';
        $mAppCache = new AppCache();
        if(!empty($this->house_numbers)){
            $address = $this->house_numbers;
        }

        if(empty($this->street_id)){// Jun 30, 2016
            $this->street_id = GasStreet::UNKNOWN;
        }

        if(!empty($this->street_id)){
            $aCacheStreet   = $mAppCache->getMasterModel('GasStreet', AppCache::ARR_MODEL_STREET);
            if(isset($aCacheStreet[$this->street_id])){
                $address        .= ', '.$aCacheStreet[$this->street_id]['name'];
            }
        }
        if(!empty($this->ward_id)){
            $aCacheWard     = $mAppCache->getMasterModel('GasWard', AppCache::ARR_MODEL_WARD);
            if(isset($aCacheWard[$this->ward_id])){
                $address        .= ', '.$aCacheWard[$this->ward_id]['name'];
            }
        }
//        else{
//            $address.= ', Không rõ';// xử lý cho phần mềm Nguyên Tạo KH Hộ GD ở dưới C# Jun 30, 2016
//        }
        if(!empty($this->district_id)){
            $aCacheDistrict     = $mAppCache->getMasterModel('GasDistrict', AppCache::ARR_MODEL_DISTRICT);
            if(isset($aCacheDistrict[$this->district_id])){
                $address        .= ', '.$aCacheDistrict[$this->district_id]['name'];
            }
//              Close on Feb 13, 2016 giảm index cho Table User  $address_vi.= ' '.MyFunctionCustom::remove_vietnamese_accents(trim($district));
        }
        if(!empty($this->province_id)){
            $aCacheProvince     = $mAppCache->getMasterModel('GasProvince', AppCache::ARR_MODEL_PROVINCE);
            if(isset($aCacheProvince[$this->province_id])){
                $address        .= ', '.$aCacheProvince[$this->province_id]['name'];
            }
//              Close on Feb 13, 2016 giảm index cho Table User  $address_vi.= ' '.MyFunctionCustom::remove_vietnamese_accents(trim($province));
        }
        $this->address = trim($address, ',');
        $this->address = trim($this->address);

//        $to = time();
//        $second = $to-$from;
//        $info = "Transaction history Build address from cache:  done in: $second  Second  <=> ".($second/60)." Minutes";
//        Logger::WriteLog($info);
        } catch (Exception $ex) {
            throw new Exception($ex->getMessage());
        }
    }

    public function saveNewCustomer($mUser) {
        $mUser->setUsernameLogin($mUser->phone);
        $mUser->save();
        return $mUser;
    }

    /**
     * @Author: DungNT Nov 08, 2016
     * @Todo: kiểm tra KH đã có account chưa
     * nếu chưa có thì tạo mới và gửi username + pass qua SMS
     */
    public function signupNewCustomerAccount() {
        $msgNormal  = 'Bạn đã đặt hàng thành công. Nhân viên của Gas24h sẽ liên lạc với bạn sớm nhất để xác nhận đơn hàng';
        $msgSMS     = 'Bạn đã đặt hàng thành công. Tài khoản đăng nhập App Gas24h đã được gửi vào số điện thoại bạn đăng ký. Vui lòng đăng nhập để theo dõi đơn hàng';
        try{
        // không tạo TK với đặt hàng ngay short
        if($this->transaction_type == Transaction::STEP_SHORT){
            return $msgNormal;
        }
        /* 1. find user có số đt đó.
         * 1.1 Kiểm tra với loại KH do KTBH tạo và App tạo, ko check KH của CCS
         * 2. kiểm tra xem có tài khoản ứng với số đt đó chưa
         * 3.1 nếu chưa có user thì tạo acc và gửi sms
         * 3.2 nếu có user mà chưa có username thì tạo acc và gửi sms
         * 4. return cờ có gửi sms xuống cho client
         */
        $mUserNew = new Users();
        $this->mapToModelUser($mUserNew);
        $mUserPhone = $mUserNew->getModelPhoneApp($this->getPhoneSms());
        if($mUserPhone){// đã có user hệ thống
            $mUserOld = Users::model()->findByPk($mUserPhone->user_id);
            if($mUserOld && empty($mUserOld->username)){// và chưa có tài khoản login app
                // function build and send SMS
                $mUserOld->makeUsernameLogin($this->getPhoneSms());
                GasScheduleSms::customerInfoLoginApp($mUserOld, $this->getPhoneSms());
                return $msgSMS;
            }
        }else{// chưa có user -> tạo mới hoàn toàn
            $mUserNew = $this->saveNewCustomer($mUserNew);
            GasScheduleSms::customerInfoLoginApp($mUserNew, $this->getPhoneSms());
            return $msgSMS;
        }
        return $msgNormal;
        }catch (Exception $exc){
            throw new Exception('signupNewCustomerAccount - '.$exc->getMessage());
        }
    }
    
    /** @Author: NamNH Oct 16, 2018
     *  @Todo: set discount Application
     **/
    public function applyGoldTime() {
        $mForecast = new Forecast();
        if(!empty($this->mAppUserLogin)){
            if(!$mForecast->canUseForecast($this->mAppUserLogin)){
                return ;
            }
        }
//        Check first order gold timer
        if($this->v1_discount_id != Forecast::TYPE_PAYMENT_FIRST_ORDER && $this->v1_discount_id != Forecast::TYPE_PAYMENT_SECOND_ORDER){
            $this->getDiscountApp(Forecast::TYPE_GOLD_TIME);
//          update transactionHistory
            $this->applyGoldTimeUpdateAmount();
        }
    }
    
    /** @Author: DungNT May 06, 2019 
     *  @Todo: fix cộng 2 lần KM vào 1 đơn: S199YXLIB, S197ZKHJ6
     * ưu tiên khuyến mãi cao cho đơn hàng, bỏ KM giờ vàng đi
     * fix cho các đơn hẹn giờ vừa có mã giới thiệu vừa đặt hẹn giờ: vd S19FW1FZN
     **/
    public function applyGoldTimeUpdateAmount() {
        if($this->promotion_id > 0){// DungNT May0619  Nếu có KM rồi thì return không tính thêm KM giờ vàng nữa
            $this->v1_discount_amount = 0;
            return ;
        }
        if(!empty($this->promotion_amount)){
            $this->promotion_amount += $this->v1_discount_amount;
        }else{
            $this->promotion_amount = $this->v1_discount_amount;
        }
        $this->grand_total -= $this->v1_discount_amount;
    }
    
    /** @Author: NamNH Oct 16, 2018
     *  @Todo: set discount Application
     **/
    public function getDiscountApp($type){
        $result = 0;
        switch ($type){
            case Forecast::TYPE_GOLD_TIME:
                if($this->is_timer == Forecast::TYPE_TIMER){
                    $mSell = new Sell();
                    $HourOrder  = (int)MyFormat::dateConverYmdToDmy($this->delivery_timer, "H");
                    $aGoldTime  = $mSell->getArrayGoldTime();
                    if(in_array($HourOrder, $aGoldTime)){
                        $discount       =  $mSell->getDiscountGoldTime();
                        $result         = $discount;
                        $this->v1_discount_id = Forecast::TYPE_GOLD_TIME;
                    }
                }
                break;
            default:
                break;
        }
        $this->v1_discount_amount = $result;
    }
    
}