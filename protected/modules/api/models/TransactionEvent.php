<?php

/**
 * This is the model class for table "{{_transaction_event}}".
 *
 * The followings are the available columns in table '{{_transaction_event}}':
 * @property string $id
 * @property string $transaction_history_id
 * @property string $user_id
 * @property integer $action_type
 * @property string $google_map
 * @property integer $change_type
 * @property integer $status_cancel
 * @property string $json_data
 * @property string $created_date
 */
class TransactionEvent extends BaseSpj
{
    const TYPE_SELL         = 1;// bán hàng HGD
    const TYPE_BO_MOI       = 2;// bán hàng bò mối Feb 11, 2017
    const TYPE_CASHBOOK     = 3;// thu chi giao nhận Oct0617
    const TYPE_RESET_PASS_CUSTOMER      = 4;// reset password customer Now0117
    const TYPE_TRUCK_PLAN               = 5;// Xe gas bồn May2319
    public $aAgent = [], $mEventComplete = null;
    
    const MONTH_DELETE_LOG = 5;// xóa log event HGD, bò mối sau 5 tháng
    
    /** @Author: DungNT Now 01, 2017
     *  @Todo: get type delete by cron, some type not need delete
     */
    public function getTypeDelete() {
        return [
            TransactionEvent::TYPE_SELL,
            TransactionEvent::TYPE_BO_MOI,
            TransactionEvent::TYPE_CASHBOOK,
            TransactionEvent::TYPE_TRUCK_PLAN,
        ];
    }

    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{_transaction_event}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        return array(
            array('id, transaction_history_id, user_id, action_type, google_map, change_type, status_cancel, json_data, created_date', 'safe'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        return array(
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
            return array(
                'id' => 'ID',
                'transaction_history_id' => 'Transaction History',
                'user_id' => 'User',
                'action_type' => 'Action Type',
                'google_map' => 'Google Map',
                'change_type' => 'Change Type',
                'status_cancel' => 'Status Cancel',
                'json_data' => 'Json Data',
                'created_date' => 'Created Date',
            );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        $criteria=new CDbCriteria;
        $criteria->compare('t.id',$this->id,true);
        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
            'pagination'=>array(
                'pageSize'=> Yii::app()->user->getState('pageSize',Yii::app()->params['defaultPageSize']),
            ),
        ));
    }
    
    
    /** @Author: DungNT Jun 13, 2017
     * @Todo: get list event of Sell ID today khi KH hối gas
     */
    public function getToday() {
        if(empty($this->transaction_history_id)){
            return '';
        }
        $mAppCache          = new AppCache();
        $aEmployee          = $mAppCache->getListdataUserByRole(ROLE_EMPLOYEE_MAINTAIN);
        $aEmployeeCall      = $mAppCache->getListdataUserByRole(ROLE_CALL_CENTER);
        $aEmployeeDieuPhoi  = $mAppCache->getListdataUserByRole(ROLE_DIEU_PHOI);
        $aEmployee          = $aEmployee + $aEmployeeCall + $aEmployeeDieuPhoi;// tạm close lại cho đỡ chậm, vì cũng chưa cần lắm
        $aAgent             = $mAppCache->getAgentListdata();
        $criteria = new CDbCriteria();
        $criteria->addCondition('t.transaction_history_id=' . $this->transaction_history_id .' AND t.type='.$this->type);
        $criteria->order = 't.id DESC';
        $criteria->limit = 6;
        $models = self::model()->findAll($criteria);
        $res = ''; $mTransaction = new Transaction();
        $aActiontype = $mTransaction->getArrayActionType();
        if($this->type== self::TYPE_BO_MOI){
            $aActiontype = $mTransaction->getArrayActionTypeBoMoi();
        }
        foreach($models as $model){
            if($model->action_type == Transaction::EMPLOYEE_COMPLETE){
                $this->mEventComplete = $model;
            }
            $model->aAgent  = $aAgent;
            $res .= $model->formatView($aEmployee, $aActiontype);
        }
        return $res;
    }
    
    /** @Author: DungNT Jul 14, 2017
     * @Todo: format one record event
     */
    public function formatView($aEmployee, $aActiontype) {
        $res = '';
//        $temp       = explode(' ', $this->created_date);
        $dateShow   = MyFormat::dateConverYmdToDmy($this->created_date, 'd/m/Y H:i');
        $nameUser   = isset($aEmployee[$this->user_id]) ? $aEmployee[$this->user_id] : $this->user_id;
        if($this->type == self::TYPE_RESET_PASS_CUSTOMER){
            $mUser      = Users::model()->findByPk($this->user_id);
            $nameUser   = $mUser ? $mUser->first_name : '';
            $dateShow   = $this->created_date;
        }
        
        if(isset($aActiontype[$this->action_type])){
            $actionText =  $aActiontype[$this->action_type];
            $res = "<br>{$dateShow}&nbsp;$nameUser - $actionText - $this->google_map {$this->getChangeData()}";
        }
        return $res;
    }
    
    /** @Author: DungNT Aug 19, 2017
     * @Todo: format json change web
     */
    public function getChangeData() {
        $res = '';
        $aTypeAllow = [Transaction::CALLCENTER_CHANGE, Transaction::CHANGE_DISCOUNT];
        if($this->type != self::TYPE_SELL || !in_array($this->action_type, $aTypeAllow)){
            return '';
        }
        $infoTrans = json_decode($this->json_data, true);
        
        $agentName      = isset($this->aAgent[$infoTrans['agent_id']]) ? $this->aAgent[$infoTrans['agent_id']] : '';
        $customerName   = $infoTrans['first_name'] . ". {$infoTrans['address']}";
        $customerPhone  = $infoTrans['phone'];
        $note           = !empty($infoTrans['note']) ? '<br><b>Ghi Chú:</b>'.$infoTrans['note'] : '';
        $strDetail = '';
        $aDetail        = isset($infoTrans['json_detail']) ? json_decode($infoTrans['json_detail'], true) : [];
        foreach($aDetail as $key => $item){
            $br = ($key != 0) ? '<br>' : '';
            $strDetail .= $br.' <b>SL: </b>'.$item['qty'].' - '.$item['name'];
        }
        $discountText = '';
        if($infoTrans['promotion_amount'] > 0){
            $discountText = "<br><b>Giảm giá:</b> ". ActiveRecord::formatCurrency($infoTrans['promotion_amount']);
        }
        
//        $res .= "Đại Lý, Khách Hàng, Liên Hệ, Ghi Chú, Chi Tiết ";
        $res .= "<div style='margin-left: 20px;'><b>Đại Lý:</b> $agentName <br><b>Khách Hàng:</b> $customerName<br><b>Liên Hệ:</b> $customerPhone $note <br><b>Chi Tiết:</b> $strDetail $discountText</div>";
        return $res;
    }
    
    /** @Author: DungNT Jul 14, 2017
     * @Todo: get list event of Sell ID today by List transaction history id
     */
    public function getByArrayTransactionHistoryId() {
        if(count($this->transaction_history_id) < 1){
            return [];
        }
        $sParamsIn = implode(',', $this->transaction_history_id);
        $criteria = new CDbCriteria();
        $criteria->addCondition("t.transaction_history_id IN ($sParamsIn)" .' AND t.type='.$this->type);
        $criteria->order = 't.id DESC';
        $models = self::model()->findAll($criteria);
        $aRes = [];
        foreach($models as $item){
            $aRes[$item->transaction_history_id][] = $item;
        }
        return $aRes;
    }
    
    /** @Author: DungNT Jul 28, 2017
     * @Todo: get by action_type and date
     */
    public function getByTypeAndDate() {
        $criteria = new CDbCriteria();
        $criteria->addCondition("DATE(t.created_date)='$this->created_date'");
        $criteria->addCondition("t.action_type=$this->action_type AND t.type=$this->type");
        $criteria->order = 't.id DESC';
        return self::model()->findAll($criteria);
    }
    
    
    /** @Author: DungNT Jul 27, 2017
     * @Todo: cron delete những record tạo 2 tháng trước
     */
    public static function cronDelete() {
        $month = TransactionEvent::MONTH_DELETE_LOG; $from = time();
        $criteria = new CDbCriteria();
        $criteria->addCondition("DATE_ADD(created_date,INTERVAL $month MONTH) < NOW()");
        $sParamsIn = implode(',', $this->getTypeDelete());
        $criteria->addCondition("type IN ($sParamsIn)");
        self::model()->deleteAll($criteria);        
        $to = time();
        $second = $to-$from;
        Logger::WriteLog("TransactionEvent delete record 2 month ". ' done in: '.($second).'  Second  <=> '.($second/60).' Minutes');
    }
    
    /**
     * @Author: DungNT Now 01, 2017
     * @Todo: ghi log lại user nào reset pass customer
     */
    public static function resetPassCustomer($mCustomer) {
        $mEvent = new TransactionEvent();
        $mEvent->type                       = TransactionEvent::TYPE_RESET_PASS_CUSTOMER;
        $mEvent->transaction_history_id     = $mCustomer->id;
        $mEvent->user_id                    = MyFormat::getCurrentUid();
        $mEvent->action_type                = Transaction::TYPE_RESET_PASS_CUSTOMER;
        $mEvent->save();
    }
    
    /** @Author: DungNT Now 08, 2017
     * @Todo: get order complete
     */
    public function getOrderComplete() {
        if(empty($this->transaction_history_id)){
            return '';
        }
        $criteria = new CDbCriteria();
        $criteria->addCondition('t.transaction_history_id=' . $this->transaction_history_id .' AND t.type='.$this->type);
        $statusComplete = Transaction::EMPLOYEE_COMPLETE;
        if($this->type== TransactionEvent::TYPE_BO_MOI){
            $statusComplete = GasAppOrder::STATUS_COMPPLETE;
        }
        $criteria->addCondition('t.action_type=' . $statusComplete);
        return self::model()->find($criteria);
    }

    
    /** @Author: DungNT Mar 05, 2019 
     *  @Todo: get record by obj id and type 
     **/
    public function getByObjIdAndType($transaction_history_id, $type, $action_type) {
        $criteria = new CDbCriteria();
        $criteria->addCondition('t.transaction_history_id=' . $transaction_history_id);
        $criteria->addCondition('t.type=' . $type);
        $criteria->addCondition('t.action_type=' . $action_type);
        return self::model()->find($criteria);
    }
}