<?php
/**
 * Oct 02, 2016 to do sync data from two server
 */
class SyncData { 
    public $URL_REQUEST;
    const  SERVER_IO_SOCKET = 'http://io.huongminhgroup.com/api/';
//    const  SERVER_IO_SOCKET = 'http://io.daukhimiennam.com/api/';// May 22, 2017 backup trong trường hợp domain huongminhgroup down
    const  SERVER_LIVE_SPJ      = 'http://spj.daukhimiennam.com/api/';
    const  DOMAIN_LIVE_NOIBO    = 'http://noibo.daukhimiennam.com';
    const  ZIP_URL_CACHE        = '/protected/runtime/cache';
    const  ZIP_URL_EXTRACT      = '/assets';
    const  ZIP_DEFAULT_NAME     = 'SpjCacheFile';
//    public static $aLiveUrl = ['spj.daukhimiennam.com', 'noibo.daukhimiennam.com'];
    public static $aLiveUrl = ['spj.daukhimiennam.com', 'load.daukhimiennam.com', 'noibo.daukhimiennam.com'];
    public static $aIpLoadBalancer = [
        '167.71.198.37', // s1
        '178.128.216.73', // s2
        '134.209.105.43', // s3
    ];
    
    public $resource, $ERRORS = [], $INFO = [];
    const NEED_SYNC     = 1;
    const IS_CREATE     = 1;
    const IS_UPADTE     = 2;
    const RECORD_LIMIT  = 50;
    const CODE_200      = 200;
    
    /**
     * @Author: DungNT Dec 09, 2015
     * @Todo: construct khởi tạo 1 request 
     * @Param: $base_url là SERVER_IO_SOCKET hoặc SERVER_LIVE_SPJ http://io.huongminhgroup.com/api/
     * @Param: $action là controller/action vd: site/userCreate
     */
    function __construct($base_url, $action) {
        $this->URL_REQUEST = $base_url.$action;
    }
    
    
    /**
     * @Author: DungNT Sep 06, 2018
     * @Todo: make request Get send to server
     */
    public function doRequestGet(){
        //open connection
        $ch = curl_init();

        //set the url, number of POST vars, POST data
        curl_setopt($ch,CURLOPT_URL, $this->URL_REQUEST);
        curl_setopt( $ch, CURLOPT_CUSTOMREQUEST, 'GET' );
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        //execute post
        $result = curl_exec($ch);
        //close connection
        curl_close($ch);
        return $result;
    }
    
    /**
     * @Author: DungNT Dec 09, 2015
     * @Todo: make request send to server
     */
    public function doRequest($json_data){
        //set POST variables
        $fields = array(
            'q' => $json_data,
        );

        $fields_string = "";
        //url-ify the data for the POST
        foreach($fields as $key=>$value) 
        { 
            $fields_string .= $key.'='.$value.'&'; 
            
        }
        $fields_string = rtrim($fields_string, '&');

        //open connection
        $ch = curl_init();

        //set the url, number of POST vars, POST data
        curl_setopt($ch,CURLOPT_URL, $this->URL_REQUEST);
        curl_setopt($ch,CURLOPT_POST, count($fields));
        curl_setopt($ch,CURLOPT_POSTFIELDS, $fields_string);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        //execute post
        $result = curl_exec($ch);

        //close connection
        curl_close($ch);
        return $result;
    }
    
    /**
     * @Author: DungNT Now 29, 2016 
     * @Todo: make POST request send to server
     * @param: $params array key=>value
     * Asynchronous PHP calls? http://stackoverflow.com/questions/962915/how-do-i-make-an-asynchronous-get-request-in-php
     * http://stackoverflow.com/questions/124462/asynchronous-php-calls
     */
    public function doRequestNormal($params){
        foreach ($params as $key => &$val) {
          if (is_array($val)) $val = implode(',', $val);
            $post_params[] = $key.'='.urlencode($val);
        }
        $post_string = implode('&', $post_params);

        $parts = parse_url($this->URL_REQUEST);

        $fp = fsockopen($parts['host'],
            isset($parts['port'])?$parts['port']:80,
            $errno, $errstr, 30);

        $out = "POST ".$parts['path']." HTTP/1.1\r\n";
        $out.= "Host: ".$parts['host']."\r\n";
        $out.= "Content-Type: application/x-www-form-urlencoded\r\n";
        $out.= "Content-Length: ".strlen($post_string)."\r\n";
        $out.= "Connection: Close\r\n\r\n";
        if (isset($post_string)) $out.= $post_string;

        fwrite($fp, $out);
        fclose($fp);
    }
    
    /**
     * @Author: DungNT Dec 09, 2015
     * @Todo: encode json post
     * @Param: array $data
     */
    public function formatJsonRequest($data) {
        return json_encode($data, JSON_UNESCAPED_UNICODE);
    }
    
    /**
     * @Author: DungNT Oct 02, 2016
     * @Todo: đồng bộ Socket notify
     * @Resource: POST: socket/notifyAdd
     * 1. send request lên
     * 2. chờ respnse về và cập nhật need_sync về 0
     */
    public function notifyAdd($mSocketNotify=null) {
        try{
        $from = time();
        $aIdUpdate = $data = [];
        $data['type'] = SyncData::IS_CREATE;
        
        if(is_null($mSocketNotify)){
            $data['record'] = $this->getSyncCreateDataJson('GasSocketNotify',$aIdUpdate, SyncData::IS_CREATE);
            $totalRecord = count($aIdUpdate);
            if($totalRecord < 1){
                return ;
            }
        }else{// Mar 13, 2014 xử lý đồng bộ luôn 1 record của CallEvent để notify luôn xuống cho NV CallCenter
            if(is_array($mSocketNotify)){
                foreach($mSocketNotify as $item){
                    $data['record'][] = $item->getAttributes();
                }
            }else{
                $data['record'][] = $mSocketNotify->getAttributes();
            }
        }

        $json_data  = $this->formatJsonRequest($data);
        $result     = $this->doRequest($json_data);
        $response   = json_decode($result, true);
        
        if(!isset($response['status'])){
            $jsonRes    = MyFormat::jsonEncode($response);
            $jsonSend   = MyFormat::jsonEncode($data['record']);
            throw new Exception("ERROR Cron Sync socket notify Không thể parse Json. DATA SEND $jsonSend ============  ============ Json Response $jsonRes  ============ ============   ");
        }
        
        if(isset($response['status']) && $response['status'] == 1){
            if(is_null($mSocketNotify)){
                $this->updateAfterSync('GasSocketNotify', $aIdUpdate);
                if($totalRecord){
                    $this->INFO[] = $totalRecord." new notify sync successful";
                }
            }
        }else{
            $this->ERRORS[] = "Error when sync new notify. ".$response['message'].". Data: $json_data";
            // send mail when errors
            $info = implode("-", $this->ERRORS);
            SendEmail::bugToDev($info);
        }
        
        if(count($this->ERRORS)){
            $aLog  = array_merge($this->INFO, $this->ERRORS);
            $to = time();
            $second = $to-$from;
            $info = "Cron Sync socket notify: ".implode("-", $aLog)." done in: $second  Second  <=> ".($second/60)." Minutes";
            Logger::WriteLog($info);
        }
        
        } catch (Exception $ex) {
            throw new Exception($ex->getMessage());
        }
    }
    
    /**
     * @Author: DungNT Oct 03, 2016
     * @Todo: đồng bộ token
     * @Resource: POST: socket/tokenAdd
     * 1. send request lên
     */
    public function tokenAdd($mUserToken) {
        try{
        $from = time();
        $data = array();
        $data['type']   = SyncData::IS_CREATE;
        $data['record'] = array($mUserToken->getAttributes());
        $json_data      = $this->formatJsonRequest($data);
        $result         = $this->doRequest($json_data);
        $response       = json_decode($result, true);
        if(!isset($response['status'])){
            throw new Exception("ERROR Cron Sync token Không thể parse Json.");
        }
        if(isset($response['status']) && $response['status'] == 1){
            $this->INFO[] = "1 new token sync successful";
        }else{
            $this->ERRORS[] = "Error when sync token. ".$response['message'];
            // send mail when errors
            $info = implode("-", $this->ERRORS);
            SendEmail::bugToDev($info);
        }

        if(count($this->ERRORS)){
            $aLog  = array_merge($this->INFO, $this->ERRORS);
            $to = time();// Now 23, 2016 bỏ log đi
            $second = $to-$from;
            $info = "Cron Sync token notify: ".implode("-", $aLog)." done in: $second  Second  <=> ".($second/60)." Minutes";
            Logger::WriteLog($info);
        }

        } catch (Exception $ex) {
            throw new Exception($ex->getMessage());
        }
    }
    
    /**
     * @Author: DungNT Dec 09, 2015
     * @Todo: lấy những user create new cần đồng bộ 
     * @param: $create_or_update 1: create, 2: update
     * @note: hiện tại mới sử dụng cho table: GasSocketNotify
     */
    public function getSyncCreate($ClassName, $create_or_update) {
        $model = call_user_func(array($ClassName, 'model'));
        $criteria = new CDbCriteria();
        $criteria->addCondition("t.need_sync=".SyncData::NEED_SYNC. ' AND t.remote_id > 0');
//        $criteria->compare("t.create_or_update", $create_or_update);
        $criteria->addCondition("t.updated_date <= NOW()"); // biến updated_date coi như là time hẹn gửi, những cái nào chưa đến time gửi thì ko lấy
        $criteria->limit = SyncData::RECORD_LIMIT;
        return $model->findAll($criteria);
    }
    
    /**
     * @Author: DungNT Dec 09, 2015
     * @Todo: lấy json những user create new cần đồng bộ 
     * @param: $aIdUpdate array các id sẽ update về 0 để xác nhận đã đồng bộ rồi
     */
    public function getSyncCreateDataJson($ClassName, &$aIdUpdate, $create_or_update) {
        $models = $this->getSyncCreate($ClassName, $create_or_update);
        $aRes = array();
        foreach($models as $model){
            $aIdUpdate[] = $model->id;
            $aRes[] = $model->getAttributes(); // foreach($model->getAttributes() as $field_name=>$field_value){
        }
        return $aRes;
    }
    
    /**
     * @Author: DungNT Dec 09, 2015
     * @Todo: update record đã được đồng bộ
     * @Param: $aIdUpdate array id
     */
    public function updateAfterSync($ClassName, $aIdUpdate) {
        $model = call_user_func(array($ClassName, 'model'));
        $criteria = new CDbCriteria();
//        $criteria->addInCondition('id', $aIdUpdate);
        $sParamsIn = implode(',', $aIdUpdate);
        $criteria->addCondition("id IN ($sParamsIn)");
        $aUpdate = array('need_sync' => 0);
        $model->updateAll($aUpdate, $criteria);
    }
    
    /**
     * @Author: DungNT Oct 02, 2016
     * @Todo: run cron every minutest sync data
     */
    public static function cronNotifyAdd() {
        try{
            $mSync = new SyncData(SyncData::SERVER_IO_SOCKET, 'socket/notifyAdd');
            $mSync->notifyAdd();
        } catch (Exception $exc) {
            GasCheck::CatchAllExeptiong($exc);
        }
    }
    
    /**
     * @Author: DungNT Dec 09, 2015
     * @Todo: insert or update data
     * @Param: $q
     * @Param: $ClassName name class model
     */
    public static function apiCreateUpdate($q, $ClassName, $aFieldNotSave=array()) {
    try {
        if($q['type'] == SyncData::IS_CREATE){
            self::apiSaveCreate($q, $ClassName, $aFieldNotSave);
        }elseif($q['type'] == SyncData::IS_UPADTE){
            echo 'not setup this update';die;
//            self::apiSaveUpdate($q, $ClassName);
        }
    } catch (Exception $exc) {
        throw new Exception($exc->getMessage());
    }
    }
    
    /**
     * @Author: DungNT Dec 17, 2015
     * @Todo: save new caution
     * @Param: $q array record
     * @Param: $ClassName name class model
     */
    public static function apiSaveCreate($q, $ClassName, $aFieldNotSave) {
        $aRowInsert = array();
//        $aFieldNotSave = array('create_or_update', 'need_sync');
        $getFieldName = true;
        $sFieldName = "";
        try {
            if(isset($q['record'])){
                foreach($q['record'] as $aInfoModel){
                    $stringValueTemp = "(";
                    foreach($aInfoModel as $field_name=>$field_value){
                        if(!in_array($field_name, $aFieldNotSave)){
                            $stringValueTemp .= "'".MyFormat::escapeValues($field_value)."',";
                            if($getFieldName){
                                $sFieldName .= "`$field_name`,";
                            }
                        }
                    }
                    
                    $stringValueTemp = rtrim($stringValueTemp, ',');
                    $sFieldName = rtrim($sFieldName, ',');
                    $stringValueTemp .= ")";
                    
                    $aRowInsert[] = $stringValueTemp;
                    $getFieldName = false;
                }

                $model_ = call_user_func(array($ClassName, 'model'));
                $tableName = $model_->tableName();
                $sql = "insert into $tableName ( $sFieldName ) values ".implode(',', $aRowInsert);
                if(count($aRowInsert)>0)
                    Yii::app()->db->createCommand($sql)->execute();
            }
        } catch (Exception $exc) {
//            throw new Exception("Errors user");
            throw new Exception($exc->getMessage());
        }
    }
    
    /** @Author: NamNH Aug 26,2019
     *  @Todo: zip folder https://stackoverflow.com/questions/4914750/how-to-zip-a-whole-folder-using-php
     *  @param: $urlFrom name folder zip ex: '/protected/runtime/cache'
     *  @param: $urlTo name folder save ex: '/assets'
     *  @param: $fileNameSave file name ex: 'SpjCacheFile'
     **/
    public function zipFile($urlFrom = SyncData::ZIP_URL_CACHE, $urlTo = SyncData::ZIP_URL_EXTRACT, $fileNameSave = SyncData::ZIP_DEFAULT_NAME) {
        $zipFolder      = Yii::getPathOfAlias("webroot").$urlFrom;
        $saveToFolder   = Yii::getPathOfAlias("webroot").$urlTo;
        // Get real path for our folder
        $rootPath       = realpath($zipFolder);
        $rootToPath     = realpath($saveToFolder);
        $zipTo          = $rootToPath.'/'.$fileNameSave.'.zip';//include file  name
        // Initialize archive object
        $zip = new ZipArchive();
        $zip->open($zipTo, ZipArchive::CREATE | ZipArchive::OVERWRITE);
        // Create recursive directory iterator
        /** @var SplFileInfo[] $files */
        $files = new RecursiveIteratorIterator(
            new RecursiveDirectoryIterator($rootPath),
            RecursiveIteratorIterator::LEAVES_ONLY
        );
        foreach ($files as $name => $file)
        {
            // Skip directories (they would be added automatically)
            if (!$file->isDir())
            {
                // Get real and relative path for current file
                $filePath = $file->getRealPath();
                $relativePath = substr($filePath, strlen($rootPath) + 1);

                // Add current file to archive
                $zip->addFile($filePath, $relativePath);
            }
        }
        // Zip archive will be created only after closing object
        return $zip->close();
    }
    
    /** @Author: NamNH Aug 26,2019
     *  @Todo: unzip local file https://stackoverflow.com/questions/8967539/how-to-unzip-a-zip-folder-using-php-code
     *  @Param: $urlOpen ex: /assets/SpjCacheFile.zip;
     *  @Param: $urlExtractTo = '/assets/SpjCacheFile';
     **/
    public function unzipFile($urlOpen, $urlExtractTo) {
        $mImageProcessing = new ImageProcessing();
        $mImageProcessing->createDirectory($urlExtractTo);
        $urlOpenReal        = Yii::getPathOfAlias("webroot").$urlOpen;
        $urlOpenReal        = realpath($urlOpenReal);
        
        $urlExtractToReal   = Yii::getPathOfAlias("webroot").$urlExtractTo;
        $urlExtractToReal   = realpath($urlExtractToReal);
        $result = false;
        $zip = new ZipArchive();
        if ($zip->open($urlOpenReal) === TRUE) {
            $result = $zip->extractTo($urlExtractToReal);
            $zip->close();
        }
        return $result;
    }
    
    /** @Author: DuongNV Aug 26,2019
     *  @Todo: copy file
     *  @Param: $from /assets/SpjCacheFile.zip;
     *  @Param: $to '/assets/SpjCacheFileCopy.zip';
     **/
    public function copyFile($from, $to) {
        $dirTo = dirname($to);
        if( !is_dir($dirTo) ){
            $mImageProcessing = new ImageProcessing();
            $mImageProcessing->createDirectory($dirTo);
        }
        if( file_exists($from) ){
            return copy($from, $to);
        }
        return false;
    }
    
}
