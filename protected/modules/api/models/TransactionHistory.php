<?php
            
/**
 * This is the model class for table "{{_transaction_history}}".
 *
 * The followings are the available columns in table '{{_transaction_history}}':
 * @property string $source
 * @property string $sell_id
 * @property string $customer_id
 * @property integer $type_customer
 * @property string $transaction_key
 * @property integer $status
 * @property string $expiry_date
 * @property string $first_name
 * @property string $agent_id
 * @property string $google_map
 * @property string $email
 * @property string $phone
 * @property integer $province_id
 * @property integer $district_id
 * @property integer $ward_id
 * @property integer $street_id
 * @property string $house_numbers
 * @property string $ip_address
 * @property string $note
 * @property string $created_date
 * @property string $json_detail
 * @property string $created_date_history
 */
class TransactionHistory extends BaseSpj
{
    public $idChuyenVien = 0, $aIdUser = [], $mSellToCheckSpjCode = null,$mTransaction = null, $promotion_amount_old = 0, $mSpjCode = null, $file_name, $list_id_image=[], $date_from, $date_to, $aDetail, $autocomplete_name, $oldJsonDetail, $mAppUserLogin = null, $discount_type;
    const MAX_DISCOUNT = 50000;// Now0518 chỉ cho phép bấm giảm tối đa 50k
    
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }
    
    /** @Author: DungNT Sep 19, 2019
     *  @Todo: check current role là điều phối bò mối không
     *  @return: true if isRoleBoMoi ELSE false 
     **/
    public function isRoleBoMoi($cRole) {
        $aRoleAllow = [ROLE_DIEU_PHOI, ROLE_ADMIN];
        return in_array($cRole, $aRoleAllow);
    }
    
    public function getArrayStatusCancel() {
        return array(Transaction::STATUS_TRY_APP, 
            Transaction::STATUS_PRICE_HIGH, 
            Transaction::STATUS_OTHER,
            Transaction::STATUS_CANCEL_BY_EMPLOYEE,
            Transaction::STATUS_CANCEL_BY_CUSTOMER,
        );
    }
    public function getArrayStatusTabNew() {
        return array(
            Transaction::STATUS_NEW,
            Transaction::STATUS_PROCESSING,// DungNT Jul2519 add for check user nhận 2 đơn 1 lúc
            Transaction::STATUS_COMPLETE, 
        );
    }

    public function getStatusChange() {
        return array(
            Transaction::STATUS_TRY_APP     => 'Thử app, không lấy gas',
            Transaction::STATUS_PRICE_HIGH  => 'Giá cao',
            Transaction::STATUS_OTHER       => 'Lý do khác'
        );
    }
    public function getStatusAll() {
        return array(
            Transaction::STATUS_NEW         => 'Mới',
            Transaction::STATUS_COMPLETE    => 'Mới',// chưa rõ làm ntn
            Transaction::STATUS_DONE        => 'Hoàn thành',
            Transaction::STATUS_PROCESSING  => 'Đang giao hàng',
            Transaction::STATUS_DELIVERY_PROCESSING  => 'Đang giao hàng',
            Transaction::STATUS_TRY_APP     => 'Thử app, không lấy gas',
            Transaction::STATUS_PRICE_HIGH  => 'Giá cao',
            Transaction::STATUS_OTHER       => 'Lý do khác'
        );
    }
    
    public function getStatusMonitor() {
        return array(
            Transaction::STATUS_NEW                 => 'Mới',
            Transaction::STATUS_DELIVERY_PROCESSING => 'Đang giao hàng',
            Transaction::STATUS_CANCEL_BY_EMPLOYEE  => 'Hủy',
        );
    }
    public function getCaseAllowDrop() {
        return [
            Transaction::CANCEL_GIAO_XA, 
            Transaction::CANCEL_AGENT_WRONG, 
            Transaction::CANCEL_SUPPORT_AGENT_BUSY, 
        ];
    }
    public function getArrayRating() {
        $res = [];
        for($i= 1; $i <6; $i++){
            $res[$i] = $i;
        }
        return $res;
    }
    
    /** @Author: DungNT Aug 18, 2018
     *  @Todo: get array agent sẽ đưa luôn đơn app xuống PVKH, ko qua tổng đài nữa 
     **/
    public function getAgentPutOrderPvkh() {
        return [
            // Sep1418 agent HCM
            101, 
            112,
            235,
            945621,
            100,
            102,
            103,
            104,
            105,
            106,
            107,
            108,
            112,
            113,
            120,
            122,
            126,
            235,
            27740,
            211393,
            242369,
            658920,
            768408,
            768409,
            868845,
            945418,
            1058061,
            1372582,
            1599181,
            
            2152168,// Đại Lý Tân Phú 2
            2152727,// Đại Lý Quận 12
            2213980,// Đại lý Quận 7.2
            2213999, // Đại Lý Quận 8.3
            2214080,// Đại Lý Hóc Môn 2
            2215880,// Đại Lý Tân Phú 3
            2250579,// Đại Lý Nhà Bè
            2250585,// Đại Lý Quận 9.2
            2250597,// Đại Lý Củ Chi
            2250715,// Đại Lý Quận 12.2
//            CodePartner::AGENT_THANH_SON,
        ];
    }
    
    /**
     * @Author: DungNT Apr 20, 2017
     * @Todo: get array support
     */
    public function getArraySupport() {
        return [0=>'Chọn loại hỗ trợ', 1=>'Giao Xa', 2=>'Giao chung cư lầu 2', 3=>'Giao chung cư lầu 3'];
    }

    public function getSupport() {
        $aSupport = $this->getArraySupport();
        if(isset($aSupport[$this->support_id]) && !empty($this->support_id)){
            return $aSupport[$this->support_id];
        }
        return '';
    }
    public function getRoleCanHandleHgd() {
        return [ROLE_EMPLOYEE_MAINTAIN, ROLE_EMPLOYEE_MARKET_DEVELOPMENT];
    }
    public function getRoleMonitor() {
        return [ROLE_MONITORING_MARKET_DEVELOPMENT, ROLE_CHIEF_MONITOR];
    }
    public function getUidMonitor() {
        $mAppCache = new AppCache();
//        return $mAppCache->getListdataCvAgent();
        return GasConst::getUidMonitorKhoan();// close Oct2318
    }
    public function getRoleViewGas24h() {
        return [ROLE_TELESALE, ROLE_ADMIN, ROLE_CALL_CENTER, ROLE_DIEU_PHOI];
    }
    
    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{_transaction_history}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        return array(
//            array('created_date, created_date_history', 'required'),
            array('address, id, sell_id, customer_id, type_customer, transaction_key, status, expiry_date, first_name, agent_id, google_map, email, phone, province_id, ip_address, note, created_date, json_detail, created_date_history', 'safe'),
            array('promotion_amount_old, employee_maintain_id, source, pttt_code, rating, rating_comment, discount_type, order_type, type_amount', 'safe'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        return array(
            'rAgent' => array(self::BELONGS_TO, 'Users', 'agent_id'),
            'rCustomer' => array(self::BELONGS_TO, 'Users', 'customer_id'),
            'rPromotion' => array(self::BELONGS_TO, 'AppPromotion', 'promotion_id'),
            
            'rEmployeeMaintain' => array(self::BELONGS_TO, 'Users', 'employee_maintain_id'),
            'rEmployeeAccounting' => array(self::BELONGS_TO, 'Users', 'employee_accounting_id'),
            'rSell' => array(self::BELONGS_TO, 'Sell', 'sell_id'),
            'rFile' => array(self::HAS_MANY, 'GasFile', 'belong_id',
                'on'=>'rFile.type='.  GasFile::TYPE_11_HGD_PTTT_CODE,
                'order'=>'rFile.id ASC',
            ),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
            return array(
                'id' => 'ID',
                'sell_id' => 'Sell',
                'customer_id' => 'Customer',
                'type_customer' => 'Type Customer',
                'transaction_key' => 'Transaction Key',
                'status' => 'Status',
                'expiry_date' => 'Expiry Date',
                'first_name' => 'First Name',
                'agent_id' => 'Đại lý',
                'google_map' => 'Google Map',
                'email' => 'Email',
                'phone' => 'Số ĐT',
                'province_id' => 'Province',
//                'district_id' => 'District',
//                'ward_id' => 'Ward',
//                'street_id' => 'Street',
//                'house_numbers' => 'House Numbers',
                'ip_address' => 'Ip Address',
                'note' => 'Note',
                'created_date' => 'Ngày tạo',
                'json_detail' => 'Json Detail',
                'created_date_history' => 'Created Date History',
                'address' => 'Address',
                'code_no' => 'Mã đơn hàng',
                'employee_maintain_id' => 'Nhân viên giao nhận',
                'rating' => 'Đánh giá',
            );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        $criteria=new CDbCriteria;
        $cRole = MyFormat::getCurrentRoleId();
        $criteria->addCondition('t.transaction_id>0 AND t.source='.Sell::SOURCE_APP);
        if( in_array($cRole, GasSocketNotify::getRoleUpdateNotify()) ){
            $criteria->addCondition('t.agent_id='.MyFormat::getAgentId());
        }
        if(!empty($this->agent_id)){
            $criteria->addCondition('t.agent_id='.$this->agent_id);
        }
        $aStatusCancel = $this->getArrayStatusCancel();
        $sParamsIn = implode(',', $aStatusCancel);
        if(isset($_GET['order_status'])){
            if($_GET['order_status'] == Transaction::ORDER_COMPLETE){
                $sParamsIn = implode(',', array(Transaction::STATUS_DONE, Transaction::STATUS_PROCESSING));
                $criteria->addCondition("t.status IN ($sParamsIn)");
//                $criteria->addCondition("t.status NOT IN ($sParamsIn)");
            }elseif($_GET['order_status'] == Transaction::ORDER_CANCEL){
                $criteria->addCondition("t.status IN ($sParamsIn)");
            }

        }else{
            $sParamsIn = implode(',', $this->getArrayStatusTabNew());
//            $criteria->addCondition('t.status='.Transaction::STATUS_COMPLETE);
            $criteria->addCondition("t.status IN ($sParamsIn) AND t.sell_id=0");
        }
        $criteria->compare("t.phone", trim($this->phone));
        
        $criteria->order = 't.id ASC';// đơn hàng nào cũ sẽ lên đầu
        if(isset($_GET['order_status'])){
            $criteria->order = 't.id DESC';
        }

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
            'pagination'=>array(
                'pageSize'=> 10,
            ),
        ));
    }
    
    /**
     * @Author: DungNT Nov 25, 2016
     */
    protected function beforeSave() {
//        $this->buildAddress();
        if($this->isNewRecord){
//            echo $milliseconds = round(microtime(true) * 1000) . '<br>';usleep(rand(1000,3000));
//            $this->code_no = MyFunctionCustom::getNextId('TransactionHistory', date('ym'), 9, 'code_no');
            /* Apr 27, 2017 bỏ đoạn này đi, mã này không cần sử dụng nữ, vì không có tác dụng lắm
            usleep(rand(10000, 30000));// cho phép trùng code_no nếu khác ngày hoặc tháng
            // để xác định 1 code_no duy nhất thì gồm date + code_no
            $this->code_no              = round(microtime(true) * 1000);
            $this->code_no              = substr($this->code_no, -8);
            */
            if(empty($this->created_date_only)){
                $this->created_date_only    = date('Y-m-d');
            }
            $this->created_date_history    = $this->getCreatedDateDb();
        }
        $this->created_date_only_bigint = strtotime($this->created_date_only);
        $this->setCodeNoSell();
//        $mPromotion = $this->rPromotion;
//        if($mPromotion){
//            $this->promotion_code_no = $mPromotion->code_no;
//        }
        return parent::beforeSave();
    }
    
    /**
     * @Author: DungNT Apr 27, 2017
     * @Todo: get code no from Sell table for search
     */
    public function setCodeNoSell() {
        if(!empty($this->code_no_sell) || empty($this->sell_id)){
            return ;
        }
        $mSell = $this->rSell;
        if($mSell){
            $this->code_no_sell = $mSell->code_no;
        }
    }

    /**
     * @Author: DungNT Dec 06, 2016
     */
    public function getUserInfo($relation, $field_name='') {
        $mUser = $this->$relation;
        if($mUser){
            if($field_name != ''){
                return $mUser->$field_name;
            }
//            return "<b>".$mUser->code_bussiness."-".$mUser->first_name."</b><br>".$mUser->address."<br><b>Phone: </b>".$mUser->phone;
            return $mUser->code_bussiness."-".$mUser->first_name;
        }
        return '';
    }
    
    public function getCodeNo() {
        return $this->getCodeNoSell();// Apr 27, 2017 sửa lại code này mới search dc bên bán hàng hộ GĐ
//        return '#'.$this->code_no;
    }
    public function getCodeNoSell() {
        return $this->code_no_sell;
    }
    public function getFirstName() {
        return $this->first_name;
    }
    public function getFirstNameApp() {
        if($this->mAppUserLogin->role_id ==  ROLE_CUSTOMER){
            return $this->first_name;
        }
        $highPrice = $pvkhName = '';
        if($this->high_price){
            $highPrice = '[Giá Cao] ';
        }
        if($this->source == Sell::SOURCE_APP){
            $highPrice = '[KH App] ';
        }
        $aRoleNotShow = [ROLE_CUSTOMER, ROLE_EMPLOYEE_MAINTAIN, ROLE_EMPLOYEE_MARKET_DEVELOPMENT];
        if(!in_array($this->mAppUserLogin->role_id, $aRoleNotShow) && !empty($this->employee_maintain_id)){
            $pvkhName = "[GN: {$this->getUserInfo('rEmployeeMaintain', 'first_name')}] ";
        }
        
        return $pvkhName.$this->getDeliveryTimerAppView().' '.$highPrice.$this->first_name;
    }
    public function getAgent($field_name='first_name') {
        $mAppCache = new AppCache();
        $aAgent = $mAppCache->getAgent();
        return isset($aAgent[$this->agent_id]) ? $aAgent[$this->agent_id][$field_name] : '';
    }
    public function getEmail() {
        return $this->email;
    }
    // Oct0218 check hide info Customer when complete order
    public function hideInfo() {
        return false;// Sếp nói không ẩn, vì PVKH có thể lấy bất cứ khi nào nếu muốn
        if($this->mAppUserLogin->role_id == ROLE_CUSTOMER){
            return false;
        }
        $aStatusHide = [GasAppOrder::STATUS_COMPPLETE, GasAppOrder::STATUS_CANCEL];
        if(in_array($this->getStatusAppNumber(), $aStatusHide)){
            return true;
        }
        return false;
    }
    public function getPhone() {
        if($this->customer_id == GasConst::HGD_TRA_THE){
            return '';
        }
        return '0'.$this->phone;
    }
    public function getPhoneApp() {
        if($this->customer_id == GasConst::HGD_TRA_THE){
            return '';
        }
        if($this->hideInfo()){
            return '';
        }
        return '0'.$this->phone;
    }
    public function getPhoneWebView() {
        return UsersPhone::formatPhoneView($this->getPhone());
    }
    
    public function getAddress() {
        return $this->address;
    }
    public function getAddressApp() {
        if($this->hideInfo()){
            return '';
        }
        return $this->address;
    }
    public function getGoogleAddress() {
        return $this->google_address;
    }
    public function getNote() {
        return nl2br($this->note);
    }
    public function getCreatedDate($fomat = 'd/m/Y H:i') {
        return MyFormat::dateConverYmdToDmy($this->created_date_history, 'd/m/Y H:i');
    }
    public function getCreatedDateOnly() {
        return MyFormat::dateConverYmdToDmy($this->created_date_only);
    }
    public function getTotal($format = false) {
        if($format){
            return ActiveRecord::formatCurrencyRound($this->total);
        }
        return $this->total;
    }
    public function getGrandTotal($format = false) {
        if($format){
            return ActiveRecord::formatCurrencyRound($this->grand_total);
        }
        return $this->grand_total;
    }
    public function getTypeAmount($format = false) {
        if($format){
            return ActiveRecord::formatCurrency($this->type_amount);
        }
        return $this->type_amount;
    }
    public function getAmountBuVo($format = false) {
        if($format){
            return ActiveRecord::formatCurrency($this->amount_bu_vo);
        }
        return $this->amount_bu_vo;
    }
    public function getGasRemainAmount($format = false) {
        if($format){
            return ActiveRecord::formatCurrency($this->gas_remain_amount);
        }
        return $this->gas_remain_amount;
    }
    public function getRating() {
        return $this->rating;
    }
    public function getRatingComment() {
        return $this->rating_comment;
    }
    public function getGasRemainAmountAppView() {
        if($this->gas_remain <= 0){
            return '';
        }
        return ($this->gas_remain*1)." kg = {$this->getGasRemainAmount(true)}";
    }
    public function getDeliveryTimer($format = 'H:i d/m/Y') {
        if(empty($this->delivery_timer) || $this->delivery_timer == '0000-00-00 00:00:00'){
            return '';
        }
        return MyFormat::dateConverYmdToDmy($this->delivery_timer, $format);
    }
    public function getDeliveryTimerAppView() {
        $text = $this->getDeliveryTimer('H:i d/m');
        if(empty($text)){
            return '';
        }
        $timeText = 'Hẹn giao';
        if($this->is_timer == Forecast::TYPE_TIMER){
            $timeText = 'App Gas24h hẹn giao';
        }
        return " [$timeText: $text]";
    }
    public function getCallEndTime() {
        if($this->mAppUserLogin->role_id == ROLE_CUSTOMER){
            return '';
        }
        $mSell = $this->rSell;
        if($mSell){
            return $mSell->getCallEndTimeWeb();
        }
        return '';
    }

    public function getHtmlDetail() {
        $res = '';
        $res .= 'Tên KH: <span style="font-size: 15px;">'.$this->getFirstName().'</span>';
        $res .= '<br>ĐT: &nbsp;<b style="font-size: 14px;">'.$this->getPhoneWebView().'</b>';
        $res .= '<br>Đ/C: '.$this->getAddress();
        
        $res .= '<br>Đại lý: '.$this->getAgent();
        $res .= '<br>Mã số: '.$this->getCodeNo();
        return $res;
    }
    public function getHtmlAction() {
        $res = '';
        $res .= '<b>'.$this->getUrlMakeOrder().'</b>';
        if(!empty($this->getChangeStatus())){
            $res .= '<br>'.$this->getChangeStatus().'';
        }
        $res .= '<br><br>'.$this->getUrlView().'';
        $res .= '<br><br><b>'.$this->getUrlChangeNotPaid().'</b>';
        $res .= '<br><br>'.$this->getCreatedDate().'';
        return $res;
    }
    
    public function getStatusApp() {
        $aStatusCancel = array(
            Transaction::STATUS_TRY_APP,
            Transaction::STATUS_PRICE_HIGH,
            Transaction::STATUS_OTHER,
            Transaction::STATUS_CANCEL_BY_CUSTOMER,
            Transaction::STATUS_CANCEL_BY_EMPLOYEE,
        );
        if(in_array($this->status, $aStatusCancel)){
            return 'Hủy';
        }
        $aAllStatus = $this->getStatusAll();
        return isset($aAllStatus[$this->status]) ? $aAllStatus[$this->status] : '';
    }
    /**
     * @Author: DungNT Jan 10, 2017
     * @Todo: get status number để hiện icon dưới view
    public static final String ORDER_NEW = "1";
    public static final String ORDER_PROCESSING = "3";
    public static final String ORDER_COMPLETE = "4";
    public static final String ORDER_CANCEL = "5";
     * 
    GasAppOrder::STATUS_NEW             => 'Mới',
    GasAppOrder::STATUS_CONFIRM         => 'Đã xác nhận',
    GasAppOrder::STATUS_PROCESSING      => 'Đang giao',
    GasAppOrder::STATUS_COMPPLETE       => 'Hoàn thành',
    GasAppOrder::STATUS_CANCEL          => 'Hủy bỏ',
    public function getStatusAppNumber() {
        $res = Transaction::ORDER_PROCESSING;
        if($this->status == Transaction::STATUS_DONE){
            $res = Transaction::ORDER_COMPLETE;
        }elseif(in_array($this->status, $this->getArrayStatusCancel())){
            $res = Transaction::ORDER_CANCEL;
        }
        return $res.'';
    }
    */
    
    public function getStatusAppNumber() {
        $res = GasAppOrder::STATUS_NEW;
        if($this->status == Transaction::STATUS_DONE){
            $res = GasAppOrder::STATUS_COMPPLETE;
        }elseif(in_array($this->status, $this->getArrayStatusCancel())){
            $res = GasAppOrder::STATUS_CANCEL;
        }elseif($this->status == Transaction::STATUS_DELIVERY_PROCESSING){
            $res = GasAppOrder::STATUS_PROCESSING;
        }

        return $res.'';
    }

    public function showAppComplete() {
        $aAgentId = $this->getListAgentKhoan();
        if(count($aAgentId) > 0 && $this->mAppUserLogin->role_id != ROLE_EMPLOYEE_MAINTAIN){
            return 0;
        }
//        $aStatusShow = [Transaction::STATUS_DONE, Transaction::STATUS_CANCEL_BY_EMPLOYEE, Transaction::STATUS_CANCEL_BY_CUSTOMER];
        $aStatusShow = [Transaction::STATUS_DELIVERY_PROCESSING];
        if(in_array($this->status, $aStatusShow)){
            return 1;
        }
        return 0;
    }
    public function showAppCancel() {
        $aStatusShow = [Transaction::STATUS_DONE, Transaction::STATUS_CANCEL_BY_EMPLOYEE, Transaction::STATUS_CANCEL_BY_CUSTOMER];
        if(!in_array($this->status, $aStatusShow)){
            return 1;
        }
        return 0;
    }
    public function showAppSave() {
        if($this->myLuongUpdate()){// Cho phép Mỹ Luông sửa trên app từ đầu tháng
            return 1;
        }
        $res = 0;// không cho sửa nếu đã hoàn thành hoặc hủy rồi
        $aStatusShow = [Transaction::STATUS_DONE, Transaction::STATUS_CANCEL_BY_EMPLOYEE, Transaction::STATUS_CANCEL_BY_CUSTOMER];
//        $aStatusShow = [Transaction::STATUS_CANCEL_BY_EMPLOYEE, Transaction::STATUS_CANCEL_BY_CUSTOMER];
        $aAgentId = $this->getListAgentKhoan();
        if(in_array($this->status, $aStatusShow)){
            $res = 0;
        }elseif($this->mAppUserLogin->id == $this->employee_maintain_id){
            $res = 0; // Sep2317 remove button save, chỉ để button hoàn thành trên app giao nhận
//            $res = 1;
        }elseif(count($aAgentId) > 0){
            $res = 1;
        }
        return $res;
    }
    
    /** @Author: DungNT Sep 12, 2017
     *  @Todo: xử lý show input promotion amount cho chuyên viên nhập giảm giá, sau đó giao nhận refresh đơn để thấy giá mới
     */
    public function showInputPromotionAmount() {
        if($this->myLuongUpdate()){// Cho phép Mỹ Luông sửa trên app từ đầu tháng
            return 1;
        }
        if($this->source == Sell::SOURCE_APP){
            return 0;
        }
        
        $aAgentId       = $this->getListAgentKhoan();
        $aStatusShow    = [Transaction::STATUS_NEW, Transaction::STATUS_DELIVERY_PROCESSING];
        if( ( in_array($this->status, $aStatusShow) && in_array($this->agent_id, GasConst::getAgentKhoanAllowUpdate()) ) || // chỗ này là cho Giao Nhận các Agent Miền tây sửa giá
            (in_array($this->status, $aStatusShow) && count($aAgentId) > 0) // chỗ này là cho chuyên viên show input sửa discount
        ){
            return 1;
        }
        return 0;
    }
    public function showAppUpdateCustomer() {
//        return 1;// Jun 3017 luôn cho update KH
        $dayAllow = date('Y-m-d');// chỉ cho phép nhân viên CallCenter cập nhật trong ngày
        $dayAllow = MyFormat::modifyDays($dayAllow, 3, '-');// Aug0917 cho phép update trong 3 ngày
        if(!empty($this->employee_maintain_id) && MyFormat::compareTwoDate($this->created_date_only, $dayAllow)){
            return 1;
        }
        return 0;
        
        $mCustomer = $this->rCustomer;
        if(empty($mCustomer->district_id) || empty($mCustomer->ward_id) || empty($mCustomer->house_numbers)){
            return 1;
        }
        return 0;
    }
    
    public function showAppChangeAgent() {
//        return 0;// Aug2119 Reopen Sếp yêu cầu - Aug0517 chặn luôn không cho chuyển dưới app
        $aActionType    = [Transaction::EMPLOYEE_FREE, Transaction::EMPLOYEE_NHAN_GIAO_HANG];
        $aRoleAllow     = [ROLE_MONITORING_MARKET_DEVELOPMENT];
        if( in_array($this->mAppUserLogin->role_id, $aRoleAllow) &&
            in_array($this->action_type, $aActionType)
        ){
            return 1;
        }
        return 0;
    }
    
    /**
     * @Author: DungNT Dec 13, 2016
     */
    public function getStatusCancelText() {
        $mTransaction = new Transaction();
        $aStatusCancel = $mTransaction->getArrayStatusCancel();
        return isset($aStatusCancel[$this->status_cancel]) ? $aStatusCancel[$this->status_cancel] : '';
    }
    
    /**
     * @Author: DungNT Nov 06, 2016
     * @Todo: get html change status for transaction, move to transaction history
     */
    public function getChangeStatus() {
        $aStatusCancel = $this->getArrayStatusCancel();
        if(in_array($this->status, $aStatusCancel) || $this->status  == Transaction::STATUS_DONE ){
            return '';
        }

        $url = Yii::app()->createAbsoluteUrl('admin/transaction/index', array('id'=>$this->id,'ChangeStatus'=>1));
        $dropdown = CHtml::dropDownList('ChangeAgent', "", 
            $this->getStatusChange(), array('class'=>'ChangeAgentValue', 'empty'=>'Lý do không lấy gas'));
        $html       = '<br><a class="ShowChangeAgent" href="javascript:;">Hủy đơn hàng</a>';
        $htmlSave   = "<span class='display_none WrapChangeAgentDropdown'>$dropdown<br><br><a class='SaveChangeAgent' next='$url' href='javascript:;'>Save</a>&nbsp;&nbsp;&nbsp;<a class='CancelChangeAgent' href='javascript:;'>Cancel</a></span>";
        $html       .= $htmlSave;
        
        return $html;
    }
    
    /**
     * @Author: DungNT Nov 06, 2016
     * @Todo: xử lý change status, Tổng đài hủy đơn app 
     */
    public function handleChangeStatus() {
        $cUid   = MyFormat::getCurrentUid();
        if(!empty($this->employee_accounting_id) && $this->employee_accounting_id != $cUid){
            return ;// phải là người xử lý đơn hàng hủy đơn đó
        }

        $this->status                   = $_POST['status'];
        $this->employee_accounting_id   = $cUid;
        $this->complete_time            = date('Y-m-d H:i:s');
        $this->update();
        $this->rollbackPromotionByHand();
//        $this->copyToHistory();
//        $this->delete();
    }
    public function handleTransConfirmRead() {
        $cRole = MyFormat::getCurrentRoleId();
        if($cRole != ROLE_ADMIN && MyFormat::getAgentId() != $this->agent_id){
            return ;
        }
        $this->status_read = Transaction::STATUS_READ_CONFIRM;
        $this->update(array('status_read'));
    }
    
    /**
     * @Author: DungNT Nov 06, 2016
     * @Todo: build url make sell from transaction
     */
    public function getUrlMakeOrder() {
//        if(!empty($this->sell_id) || $this->status != Transaction::STATUS_COMPLETE){
        if(!empty($this->sell_id) || !in_array($this->status, $this->getArrayStatusTabNew())){
            return '';
        }
        $url    = Yii::app()->createAbsoluteUrl('admin/sell/create', array('transaction_id'=>$this->id,'MakeOrder'=>1));
        $html   = '<a class="UrlMakeSell" href="'.$url.'" target="_blank">Tạo bán hàng </a>';
        return $html;
    }
    public function getUrlView() {
        $url    = Yii::app()->createAbsoluteUrl('admin/transaction/view', array('id'=>$this->id));
        $html   = '<a class="item_b" href="'.$url.'" target="_blank">Xem vị trí</a>';
        return $html;
    }
    public function getUrlHideOrder() {
//        if(!empty($this->sell_id) || $this->status != Transaction::STATUS_COMPLETE){
//            return '';
//        }
        $html   = '<a class="item_b HideOrder f_size_15" href="javascript:;">Ẩn đơn hàng</a>';
        return $html;
    }
    /**
     * @Author: DungNT Dec 12, 2016
     * @Todo: build url change Status TransactionHistory to not paid
     * đổi trạng thái của đơn thành thành chưa thu tiền, trong trường hợp giao nhận bấm nhầm thu tiền
     */
    public function getUrlChangeNotPaid() {
        return '';// Dec0917 chưa rõ làm gì với function này
        if(!$this->allowChangeNotPaid()){
            return '';
        }
        if($this->status != Transaction::STATUS_DONE){
            return '';
        }
        $url    = Yii::app()->createAbsoluteUrl('admin/transaction/index', array('id'=>$this->id,'ChangeNotPaid'=>1));
        $html   = '<a class="btn_closed_tickets" href="'.$url.'" alert_text="Bạn chắc chắn muốn chuyển chưa thu tiền?">Chuyển chưa thu tiền </a>';
        return $html;
    }
    
    /**
     * @Author: DungNT Dec 17, 2016
     * @Todo: get list uid allow  ChangeNotPaid
     */
    public function allowChangeNotPaid() {
        $cUid = MyFormat::getCurrentUid();
        $aUidAllow =  array(2);
        return in_array($cUid, $aUidAllow);
    }

    /**
     * @Author: DungNT Dec 17, 2016
     * @Todo: change Status TransactionHistory to not paid
     */
    public function handleChangeNotPaid() {
        $cUid = MyFormat::getCurrentUid();
        $this->action_type  = Transaction::EMPLOYEE_NHAN_GIAO_HANG;
        $this->status       = Transaction::STATUS_DELIVERY_PROCESSING;
        $aUpdate = array('action_type', 'status');
        $this->update($aUpdate);
        // need update to table Sell
        $mSell = $this->rSell;
        if($mSell){
            $mSell->setStatus(Sell::STATUS_NEW, '');
        }
        
        $this->saveEvent($cUid, Transaction::EMPLOYEE_CHANGE_NOT_PAID, 0, 0, '');
    }
    
    public function canUpdate() {
        return true;
    }
    
    public function getPhoneSms() {
        return '0'.$this->phone;
    }
    
    /**
     * @Author: DungNT Nov 08, 2016
     * @Todo: map info user to model user
     */
    public function mapToModelUser(&$mUser) {
        $mUser->first_name      = $this->first_name;
        $mUser->phone           = $this->getPhoneSms();
        $mUser->province_id     = $this->province_id;
        $mUser->created_by      = MyFormat::getCurrentUid();
        $mUser->area_code_id    = $this->agent_id;
        $mUser->ip_address      = $this->ip_address;
        $mUser->slug            = $this->google_map;

        $mUser->is_maintain     = UsersExtend::STORE_CARD_HGD_APP;
        $mUser->channel_id      = Users::CON_LAY_HANG;
    }
    
    public function getClassRowSpan() {
        return '<span class="'.$this->getClassRowName().'" style="display:none;"></span>';
    }
    
    public function getClassRowName() {
        return 'row_trans_'.$this->id;
    }
    
    public function getImageNew() {
        if($this->status_read == Transaction::STATUS_READ_CONFIRM){
            return '';
        }
        $cRole  = MyFormat::getCurrentRoleId();
        if( !in_array($cRole, GasSocketNotify::getRoleUpdateNotify()) ){
            // cho phép giao nhận và KTBH có thể cập nhật notify confirm
            return $this->getImageIconNewNotify();
        }
        $title = 'Click để xác nhận và tắt âm thanh';
        return '<a class="TransConfirmRead" title="'.$title.'" href="javascript:;" next="'.$this->getUrlConfirm().'">'.$this->getImageIconNewNotify().'</a>';
    }
    public function getUrlConfirm() {
        return Yii::app()->createAbsoluteUrl('admin/transaction/index', array('confirm_read'=>1, 'id'=>  $this->id));
    }
    
    /**
     * @Author: DungNT Nov 06, 2016
     * @Todo: get detail view
     */
    public function getDetailView() {
        
        $str = $this->getClassRowSpan();
        if(empty($this->json_detail)){
            return ;
        }
        $aDetail    = json_decode($this->json_detail, true);
        if(!is_array($aDetail) && count($aDetail) < 1){
            return ;
        }

        $mAppCache  = new AppCache();
        $aMaterial  = $mAppCache->getMasterModel('GasMaterials', AppCache::ARR_MODEL_MATERIAL);
        foreach($aDetail as $key => $detail){
            $qty    = ActiveRecord::formatCurrency($detail['qty']);
            $name   = isset($aMaterial[$detail['materials_id']]) ? $aMaterial[$detail['materials_id']]['name'] : '';
            $br = '';
            if($key != 0){
                $br = '<br>';
            }
            $str .= $br.' <b>SL: </b>'.$qty.' - '.$name;
        }
        return $str;
    }
    
    /**
     * @Author: DungNT Dec 06, 2016
     * @Todo: get title show on List
     */
    public function getAppTitle() {
        $res = '';
        $aDetail    = json_decode($this->json_detail, true);
        if(!is_array($aDetail)){
            return '';
        }
        foreach($aDetail as $key => $detail){
            if($key != 0){
                $res .= ', ';
            }
            $res .= isset($detail['name']) ? (int)$detail['qty'].' '.$detail['name'] : '';
        }
        return $res;
    }

    /**
     * @Author: DungNT Nov 05, 2016
     */
    public function buildAddress() {
        return 'not use this address function';
        try{
        $from = time();
        $address = '';
        $mAppCache = new AppCache();
        if(!empty($this->house_numbers)){
            $address = $this->house_numbers;
        }

        if(empty($this->street_id)){// Jun 30, 2016
            $this->street_id = GasStreet::UNKNOWN;
        }

        if(!empty($this->street_id)){
            $aCacheStreet   = $mAppCache->getMasterModel('GasStreet', AppCache::ARR_MODEL_STREET);
            if(isset($aCacheStreet[$this->street_id])){
                $address        .= ', '.$aCacheStreet[$this->street_id]['name'];
            }
        }
        if(!empty($this->ward_id)){
            $aCacheWard     = $mAppCache->getMasterModel('GasWard', AppCache::ARR_MODEL_WARD);
            if(isset($aCacheWard[$this->ward_id])){
                $address        .= ', '.$aCacheWard[$this->ward_id]['name'];
            }
        }
//        else{
//            $address.= ', Không rõ';// xử lý cho phần mềm Nguyên Tạo KH Hộ GD ở dưới C# Jun 30, 2016
//        }
        if(!empty($this->district_id)){
            $aCacheDistrict     = $mAppCache->getMasterModel('GasDistrict', AppCache::ARR_MODEL_DISTRICT);
            if(isset($aCacheDistrict[$this->district_id])){
                $address        .= ', '.$aCacheDistrict[$this->district_id]['name'];
            }
//              Close on Feb 13, 2016 giảm index cho Table User  $address_vi.= ' '.MyFunctionCustom::remove_vietnamese_accents(trim($district));
        }
        if(!empty($this->province_id)){
            $aCacheProvince     = $mAppCache->getMasterModel('GasProvince', AppCache::ARR_MODEL_PROVINCE);
            if(isset($aCacheProvince[$this->province_id])){
                $address        .= ', '.$aCacheProvince[$this->province_id]['name'];
            }
//              Close on Feb 13, 2016 giảm index cho Table User  $address_vi.= ' '.MyFunctionCustom::remove_vietnamese_accents(trim($province));
        }
        $this->address = trim($address, ',');
        $this->address = trim($this->address);
        
        $to = time();
        $second = $to-$from;
        $info = "Transaction history Build address from cache:  done in: $second  Second  <=> ".($second/60)." Minutes";
        Logger::WriteLog($info);
        } catch (Exception $ex) {
            throw new Exception($ex->getMessage());
        }
    }

    /**
     * @Author: DungNT Nov 05, 2016
     * @Todo: get latest Transaction
     */
    public function getLastestHistory() {
        $criteria = new CDbCriteria();
        $criteria->addCondition('t.customer_id=' . $this->customer_id);
        $criteria->addCondition('t.status=' . Transaction::STATUS_DONE);
        $criteria->addCondition('t.source=' . Sell::SOURCE_APP);
        $criteria->limit = 3;// Mar0819 Fix bug đơn hàng trống khi đơn trước mua dây đen
        $criteria->order = 't.id DESC';
        $models = self::model()->findAll($criteria);
        foreach($models as $mTransactionHistory):// Mar0819 Fix bug đơn hàng trống khi đơn trước mua dây đen
            $aDetail    = json_decode($mTransactionHistory->json_detail, true);
            if(is_array($aDetail)){
                foreach($aDetail as $detail){// chi lay don hang co B12
                    if($detail['materials_type_id'] == GasMaterialsType::MATERIAL_BINH_12KG){
                        return $mTransactionHistory;
                    }
                }
            }
        endforeach;
        return null;
    }
    public function getAppViewLastestHistory() {
        $model = $this->getLastestHistory();
        if(empty($model)){
            return null;
        }
        $model->mAppUserLogin = $this->mAppUserLogin;
        return $model->formatLatestTransaction();
    }
    
    /**
     * @Author: DungNT Nov 26, 2016
     * @Todo: xác định chiết khấu mặc định hay chỉnh sửa
     */
    public function getDiscountType() {
        $this->discount_type = Sell::DISCOUNT_DEFAULT;
        if($this->amount_discount > 0){
            $this->discount_type = Sell::DISCOUNT_CHANGE;
        }
        return $this->discount_type;
    }
    
    /**
     * @Author: DungNT Jan 04, 2017
     * @Todo: get amount_discount 
     */
    public function getDiscountNormal($format = false) {
        if($this->amount_discount > 1){
            return $this->getAmountDiscount($format);
        }
        $amount = $this->qty_discount*$this->getPromotionDiscount();
        if($format){
            return ActiveRecord::formatCurrency($amount);
        }
        return $amount;
    }
    
    public function getAmountDiscount($format = false) {
        if($format){
            return ActiveRecord::formatCurrency($this->amount_discount);
        }
        return $this->amount_discount;
    }
    
    public function getPromotionDiscount() {
        return Sell::model()->getPromotionDiscount();
    }
    public function getPromotionAmount($format = false) {
        if($format){
            return ActiveRecord::formatCurrency($this->promotion_amount);
        }
        return $this->promotion_amount;
    }
    
    /**
     * @Author: DungNT Jan 04, 2017
     * @Todo: get info nhân viên giao nhận để show
     */
    public function getInfoEmployee(&$tmp) {
        // chô này nên tách ra hàm
        if(!empty($this->employee_maintain_id)){
            $mUser = $this->rEmployeeMaintain;
            $tmp['employee_name']   = $mUser->getFullName();
            $tmp['employee_code']   = $mUser->code_bussiness;
            $tmp['employee_phone']  = $mUser->getPhoneApiAndroid();
            $tmp['employee_image']  = ImageProcessing::bindImageByModel($mUser->rUsersRefProfile,'','',array('size'=>'size2'));
        }else{
            $tmp['employee_name']   = '';
            $tmp['employee_code']   = '';
            $tmp['employee_phone']  = '';
            $tmp['employee_image']  = '';
        }
        // Add employee location
        $mAgent = $this->rAgent;
        if(!empty($mAgent)){
            $aMap = explode(',', $mAgent->slug);
            $tmp['employee_latitude']    = isset($aMap[0]) ? $aMap[0] : '0';
            $tmp['employee_longitude']   = isset($aMap[1]) ? $aMap[1] : '0';
        }else{
            $tmp['employee_latitude']    = '0';
            $tmp['employee_longitude']   = '0';
        }
    }
    /**
     * @Author: DungNT Nov 05, 2016
     * @Todo: app view format response latest Transaction
     */
    public function formatLatestTransaction() {
        $aRes   = [];// order_view, order_edit
        $mAppCache = new AppCache();
//        $aMaterial     = $mAppCache->setMasterModel('GasMaterials', AppCache::ARR_MODEL_MATERIAL);
        $aMaterial     = $mAppCache->getMasterModel('GasMaterials', AppCache::ARR_MODEL_MATERIAL);
        
//        $mTransHistory = new TransactionHistory();
        $tmp    = [];
        $tmp['id']          = $this->id;
        $tmp['first_name']  = $this->getCallEndTime().$this->getFirstNameApp();
        $tmp['phone']       = $this->getPhoneApp();
        $tmp['email']       = $this->getEmail();
        $tmp['agent_id']    = $this->agent_id;
        $tmp['agent_name']  = $this->getAgent();
        $tmp['agent_phone'] = $this->getAgent('code_bussiness');// đổi code_bussiness là số máy bàn của ĐL
//        $tmp['agent_phone'] = '08 38 988 988 - 0862490409';// đổi code_bussiness là số máy bàn của ĐL
        $tmp['note']        = $this->note. $this->getDeliveryTimerAppView();
        $tmp['address']     = $this->getAddressApp();
        $tmp['code_no']     = $this->getCodeNo();
        $tmp['allow_update']    = $this->employeeCanUpdate($this->mAppUserLogin);
//        $tmp['change_qty']      = '0';// 1 cho phép sửa qty, 0 ko cho phép sửa
        $tmp['change_qty']      = '1';// 1 cho phép sửa qty, 0 ko cho phép sửa
        
        $tmp['province_id'] = $this->province_id;
        $tmp['district_id'] = '';
        $tmp['ward_id']     = '';
        $tmp['street_id']   = '';
        $tmp['house_numbers'] = '';
        $aMap = explode(',', $this->google_map);
        $tmp['latitude']    = !empty($aMap[0]) ? $aMap[0] : '0';// Mar419 fix !isset -> !empty 
        $tmp['longitude']   = !empty($aMap[1]) ? $aMap[1] : '0';
        
        $this->getInfoEmployee($tmp);
        
        $tmp['order_type']      = $this->order_type.'';
        $tmp['type_amount']     = $this->type_amount.'';
        $tmp['amount_discount'] = $this->amount_discount;// sử dụng để tính toán, có biến discount_amount show text bên dưới rồi
        $tmp['discount_type']   = $this->getDiscountType().'';
        $tmp['status_cancel']   = $this->status_cancel.'';
        $tmp['status_number']   = $this->getStatusAppNumber();
        
        $tmp['rating']          = $this->getRating();
        $tmp['rating_comment']  = $this->getRatingComment();
        
        $tmp['show_nhan_giao_hang']    = ''.$this->canPick();
        $tmp['show_huy_giao_hang']     = ''.$this->canPickCancel($this->mAppUserLogin);
        $tmp['support_id']      = $this->support_id;
        $tmp['support_text']    = $this->getSupport();
        $tmp['created_date']    = $this->getCreatedDate();
        $tmp['show_button_complete']        = $this->showAppComplete();
        $tmp['show_button_save']            = $this->showAppSave();
        $tmp['list_image']                  = GasFile::apiGetFileUpload($this->rFile);
        $tmp['pttt_code']                   = $this->pttt_code;
        $tmp['show_button_update_customer'] = $this->showAppUpdateCustomer();
        $tmp['show_button_change_agent']    = $this->showAppChangeAgent();
        $tmp['customer_id']                 = $this->customer_id;
        $tmp['show_button_cancel']          = $this->showAppCancel();
        $tmp['show_input_promotion_amount'] = $this->showInputPromotionAmount();

        $this->formatDetailView($tmp, $aMaterial);
        return $tmp;
    }
    
    /**
     * @Author: DungNT Nov 06, 2016
     * @Todo: appview format order detail view
     */
    public function formatDetailView(&$aRes, $aMaterial) {
        $aDetail    = json_decode($this->json_detail, true);
        $sumQty     = 0; $sumAmount = 0; $aDetailGas = [];
        if(is_array($aDetail)){
            foreach($aDetail as $detail){
                $tmp = [];
                $tmp['materials_type_id'] = $detail['materials_type_id'];
                $tmp['materials_id']      = $detail['materials_id'];// Feb 02, 2017 thêm S vào materials_id
                $tmp['materials_name']    = isset($aMaterial[$detail['materials_id']]) ? $aMaterial[$detail['materials_id']]['name'] : '';
                $tmp['materials_name_short']    = isset($aMaterial[$detail['materials_id']]) ? $aMaterial[$detail['materials_id']]['name_store_card'] : '';
                if(empty($tmp['materials_name_short'])){
                    $tmp['materials_name_short'] = $tmp['materials_name'];
                }
                $tmp['material_price']    = ActiveRecord::formatCurrency($detail['price']);
                $tmp['price']             = empty($detail['price']) ? '0' : $detail['price'];
                $tmp['qty']               = ActiveRecord::formatCurrency($detail['qty']);
                $tmp['amount']            = ActiveRecord::formatCurrencyRound($detail['amount']);
                $tmp['material_image']    = isset($aMaterial[$detail['materials_id']]) ? $aMaterial[$detail['materials_id']]['image'] : '';
                $tmp['seri']                = isset($detail['seri']) ? $detail['seri'] : '';
                $price_root                 = isset($detail['price_root']) ? $detail['price_root'] : 0;
                if($price_root == $tmp['price'] || empty($price_root)){
                    $tmp['price_root'] = '0';
                }else{
                    $tmp['price_root'] = ActiveRecord::formatCurrency($price_root);
                }

                $tmp['kg_empty']            = isset($detail['kg_empty']) ? ($detail['kg_empty'] > 0 ? $detail['kg_empty']*1 : '') : '';
                $tmp['kg_has_gas']          = isset($detail['kg_has_gas']) ? ($detail['kg_has_gas'] > 0 ? $detail['kg_has_gas']*1 : '') : '';

                if(in_array($detail['materials_type_id'], GasMaterialsType::$SETUP_PRICE_GAS_12) ){
                    array_unshift($aDetailGas , $tmp);
                }else{// luôn đưa gas lên item first của array
                    $aDetailGas[] = $tmp;
                }
                $sumQty     += $detail['qty'];
                $sumAmount  += $detail['amount'];
            }
            $aRes['order_detail'] = $aDetailGas;
        }else{
            $aRes['order_detail'] = [];
        }
        
        $aRes['total_qty']          = ActiveRecord::formatCurrency($sumQty);
        $aRes['promotion_amount']   = $this->getPromotionAmount(true);// only for show, không sử dụng để cộng trừ
        $aRes['discount_amount']    = $this->getDiscountNormal(true);// for show client view
        $aRes['total']              = $this->getTotal(true);
        $aRes['grand_total']        = $this->getGrandTotal(true);
        $aRes['order_type_text']    = Sell::model()->getOrderTypeAppView($this->order_type);
        $aRes['order_type_amount']  = $this->getTypeAmount(true);
        $aRes['amount_bu_vo']       = $this->getAmountBuVo(true);
        $aRes['gas_remain_amount']  = $this->getGasRemainAmountAppView();
        
        $aRes['input_promotion_amount']   = !empty($this->promotion_amount) ? $this->promotion_amount : '';// Sep1217 sử dụng để tính toán, cho nhập số tiền giảm của đơn hàng

    }
    
    /**
     * @Author: DungNT move to model => Dec 18, 2017 -- handle listing api
     * @param: $apiController api controller 
     */
    public function handleApiList(&$result, $q) {
        // 1. get list order by user id
        $dataProvider           = $this->ApiListing($q);
        $models                 = $dataProvider->data;
        $CPagination            = $dataProvider->pagination;
        $result['total_record'] = $CPagination->itemCount;
        $result['total_page']   = $CPagination->pageCount;
        if ($q->page >= $CPagination->pageCount) {
            $result['record'] = [];
        } else {
            foreach ($models as $mTransHistory) {
                $mTransHistory->mAppUserLogin = $this->mAppUserLogin;
                $temp = array();
                $temp['id']             = $mTransHistory->id;
                $temp['title']          = $mTransHistory->getAppTitle();
                $temp['code_no']        = $mTransHistory->getCodeNo();
                $temp['grand_total']    = $mTransHistory->getGrandTotal(true);
                $temp['status']         = $mTransHistory->getStatusApp();
                $temp['created_date']   = $mTransHistory->getCallEndTime().$mTransHistory->getCreatedDate();
                $temp['first_name']     = $mTransHistory->getFirstNameApp();
                $temp['status_number']  = $mTransHistory->getStatusAppNumber();
                $temp['note']           = $mTransHistory->getAppNoteAtList();

                $temp['phone']          = $mTransHistory->getPhone();
                $temp['address']        = $mTransHistory->getAddress();
                $temp['show_nhan_giao_hang']    = '' . $mTransHistory->canPick();
                $temp['show_huy_giao_hang']     = '' . $mTransHistory->canPickCancel($this->mAppUserLogin);

                $result['record'][] = $temp;
            }
        }
    }
    

    /**
    * @Author: DungNT Now 24, 2016
    * @Todo: get listing transaction history
    * @param: $q object post params
    */
    public function ApiListing($q) {
        $criteria = new CDbCriteria();
        $agent_id = 0;
        
//        if($this->mAppUserLogin->role_id != ROLE_CUSTOMER && GasCheck::isServerLive()){
//            $criteria->addCondition('t.id > 271157');// Jan1618 OK để giới hạn record giảm đc time của query không? bình thường đang hơn 2 second - sellId=448513-01/01/2018
//        }// Oct2218 close lại, chuyển sang kiểu ORDER BY t.created_date_only_bigint DESC 

        if($this->mAppUserLogin->role_id == ROLE_CUSTOMER){
            $criteria->addCondition('t.customer_id='.$this->mAppUserLogin->id);
        }elseif(in_array($this->mAppUserLogin->role_id, $this->getRoleCanHandleHgd())){
            $aAgent = Users::GetKeyInfo($this->mAppUserLogin, UsersExtend::JSON_ARRAY_AGENT);
//            $criteria->addCondition('t.employee_maintain_id='.$this->mAppUserLogin->id);
            $sParamsIn = implode(',', $aAgent);
            if($sParamsIn != ''){
                $agent_id = reset($aAgent);
                $criteria->addCondition(' (t.agent_id IN ('.$sParamsIn.') AND t.action_type=0 )OR t.employee_maintain_id='.$this->mAppUserLogin->id);
            }else{
                $criteria->addCondition('t.agent_id=1');
            }
            $criteria->addCondition('t.sell_id > 0');// Tạo Sell thì mới load xuống cho Giao Nhận
        }elseif(in_array($this->mAppUserLogin->role_id, $this->getRoleMonitor())){
            $this->handleLimitByRole($q, $criteria);
        }else{// Aug2817 chưa xử lý cho các role khác xem HGĐ trên app
            $criteria->addCondition('t.agent_id=1');
        }
        
//        $sParamsIn = implode(',', array(Transaction::STATUS_NEW, Transaction::STATUS_COMPLETE));
//        $criteria->addCondition("t.status NOT IN ($sParamsIn)");
        if(isset($q->status)){
            $sParamsIn = implode(',', Transaction::model()->getAppStatusNew());
            if($q->status == Transaction::STATUS_NEW){
                $criteria->addCondition('t.status IN ('.$sParamsIn.')');
                
                $days_check = Yii::app()->params['DaysUpdateAppBoMoi'];// Apr 20, 2017 giữ lại notify của ĐP
                if(!GasCheck::isServerLive()){
                    $days_check = 100;
                }
                /**  Apr 21, 2017 khi mà tất cả các đại lý chạy thì set status về hoàn thành rồi close dòng DATE_ADD này lại 
                 * xử lý ẩn các record cũ cho các đại lý mới chạy đi, không hiện lên nó lại bấm lung tung
                 * khi nào run hết hcm thì bỏ điều kiện này đi, cho đỡ load
                 */
                if(!empty($agent_id) && in_array($agent_id, UsersExtend::getAgentNotRunApp())){
                    $criteria->addCondition("DATE_ADD(t.created_date_only,INTERVAL $days_check DAY) >= CURDATE()");
                }else{
//                    $criteria->addCondition("DATE_ADD(t.created_date_only,INTERVAL $days_check DAY) >= CURDATE()");
//                    $criteria->addCondition("t.created_date_only = CURDATE()");
                }
                
            }elseif($q->status == Transaction::STATUS_COMPLETE){
                $criteria->addCondition('t.status NOT IN ('.$sParamsIn.')');
            }
        }
        $this->handleMoreCondition($q, $criteria);
        
        $criteria->order = 't.created_date_only_bigint DESC';
        return new CActiveDataProvider($this,array(
            'criteria' => $criteria,
            'pagination'=>array(
                'pageSize' => GasAppOrder::API_LISTING_ITEM_PER_PAGE,
//                'pageSize' => 5,
                'currentPage' => (int)$q->page,
            ),
        ));
    }
    
    /** @Author: DungNT Aug2817
     * @Todo: xử lý add thêm điều kiện giới hạn view cho quản lý của đại lý khoán: vd Võ Tấn Huy
     */
    public function handleLimitByRole($q, &$criteria) { 
        $this->idChuyenVien = $this->mAppUserLogin->id;
        $aAgentId           = $this->getArrayAgentOfChuyenVien();// May1519 fix chuyên viên xem order zone agent quan ly
//        $aAgentId = $this->getListAgentKhoan();
        if(count($aAgentId) < 1){
            $criteria->addCondition('t.agent_id=1');// không cho User xem đơn hàng HGĐ nếu chưa có setup trên code
            return ;
        }
        $sParamsIn = implode(',', $aAgentId);
        $criteria->addCondition('t.agent_id IN ('.$sParamsIn.')');
    }
    
    /** @Author: DungNT Sep 19, 2017
     *  @Todo: get list agent khoán để hiện input giảm giá trên app
     */
    public function getListAgentKhoan() {
        $aAgentId = [];
        foreach($this->getUidMonitor() as $agent_id=>$uid){
            if($this->mAppUserLogin->id == $uid){
                $aAgentId[] = $agent_id;
            }
        }
        return $aAgentId;
    }

    /** @Author: DungNT May 23, 2017
     * @Todo: xử lý add thêm điều kiện search apilisting
     */
    public function handleMoreCondition($q, &$criteria) {
        if(!empty($q->date_from) && !empty($q->date_to) ){
            $date_from  = MyFormat::dateDmyToYmdForAllIndexSearch($q->date_from);
            $date_to    = MyFormat::dateDmyToYmdForAllIndexSearch($q->date_to);
//            $criteria->addCondition("t.created_date_only>='$date_from'");
            DateHelper::searchBetween($date_from, $date_to, 'created_date_only_bigint', $criteria, false);
        }
//        if(!empty($q->date_to)){
//            $date_to = MyFormat::dateDmyToYmdForAllIndexSearch($q->date_to);
//            $criteria->addCondition("t.created_date_only<='$date_to'");
//        }
    }

    /**
     * @Author: DungNT Nov 29, 2016
     * @Todo: notify to CallCenter when new Order Gas24h
     */
    public function makeNotify() {
        if(!GasCheck::isServerLive()){
            return ;
        }
        
        $mCustomer  = $this->rCustomer;
        $mAppCache  = new AppCache();
        $aUid       = $mAppCache->getArrayIdRole(ROLE_CALL_CENTER);

        $message    = "App đặt hàng: {$mCustomer->getFullName()}. $mCustomer->phone";
        $json       = $needMore = [];
        foreach($aUid as $toUserId){
//            if($toUserId != 1029818){// Call202
//                continue ;// only dev test
//            }
            $aModelSocket[] = GasSocketNotify::addMessage($this->id, $mCustomer->id, $toUserId, GasSocketNotify::CODE_APP_ORDER_NEW, $message, $json, $needMore);
        }
        $mSync = new SyncData(SyncData::SERVER_IO_SOCKET, 'socket/notifyAdd');
        $mSync->notifyAdd($aModelSocket);
    }
    /**
     * @Author: DungNT Nov 29, 2016
     * @Todo: xử lý put event Remove row notify Order App Gas24h when one user has confirm
     */
    public function makeNotifyConfirm() {
        if(!GasCheck::isServerLive()){
            return ;
        }
        $mAppCache = new AppCache();
        $aUid = $mAppCache->getArrayIdRole(ROLE_CALL_CENTER);
        $message    = '';
        $json       = $needMore = $aModelSocket = [];
        foreach($aUid as $toUserId){
            $aModelSocket[] = GasSocketNotify::addMessage($this->id, 2, $toUserId, GasSocketNotify::CODE_APP_ORDER_CONFIRM, $message, $json, $needMore);
        }
        $mSync = new SyncData(SyncData::SERVER_IO_SOCKET, 'socket/notifyAdd');
        $mSync->notifyAdd($aModelSocket);
    }
    
    /**
     * @Author: DungNT Dec 16, 2016
     * @Todo: lấy list giao nhận của đại lý
     */
    public function getListEmployeeMaintainOfAgent() {
        return GasOneMany::getArrOfManyIdByType(ONE_AGENT_MAINTAIN, $this->agent_id);
    }
    
    /** @NOTE các notify của 1 function nên viết liền nhau để còn review không bị sót
     * @Author: DungNT Dec 06, 2016
     * @Todo: Make Phone App notify to customer + NVGN + KTBH nếu có change NVGN
     * @param: $aUid có thể là 1 nv hoặc list NV
     */
    public function notifyPhoneAppEmployee($aUid, $title = '') {
        if(!is_array($aUid)){
            return ;
        }
        $source = ($this->source == Sell::SOURCE_APP) ? '[KH APP] - ' : '';
        if(empty($title)){
            $title  = $source. $this->getFirstName(). ' - '. $this->getAddress() .' - '.$this->getPhone();
        }
        $this->runInsertNotify(GasScheduleNotify::TRANSACTION_CHANGE_EMPLOYEE, $aUid, $title, true); // Send Now Fix Fcm nodejs Sep 11, 2018
//        $this->runInsertNotify(GasScheduleNotify::TRANSACTION_CHANGE_EMPLOYEE, $aUid, $title, false); // Send By Cron Feb 23, 2017
        // xử lý tách riêng notify ra nhiều kieu notify
    }
    public function notifyPhoneAppCustomer($title = '') {
        $aUid   = array($this->customer_id);
        if(empty($title)){
            $title  = $this->getUserInfo('rEmployeeMaintain', 'first_name')." đang giao đơn hàng của bạn {$this->getCreatedDateOnly()}";
        }
        $this->runInsertNotify(GasScheduleNotify::CUSTOMER_HGD_ORDER, $aUid, $title, true); // Send Now Fix Fcm nodejs Sep 11, 2018
//        $this->runInsertNotify(GasScheduleNotify::CUSTOMER_HGD_ORDER, $aUid, $title, false); // Send By Cron Feb 23, 2017
    }
    
    /**
     * @Author: DungNT Dec 06, 2016
     * @Todo: insert to table notify
     * @Param: $aUid array user id need notify
     */
    public function runInsertNotify($notifyType, $aUid, $title, $sendNow = false) {
//            [TASKTIMER]        
        if($this->is_timer == Forecast::TYPE_TIMER){
            $this->runInsertNotifyTimer($notifyType, $aUid, $title, $sendNow);
            return ;
        }
        if(!empty($this->created_date)){// Jul 12, 2017 không notify khi update trên web
            $temp  = explode(' ', $this->created_date);
            $today = date('Y-m-d');
            if(isset($temp[0]) && $temp[0] != $today){
//                Logger::WriteLog($this->id.' Debug not send notify when Sell web Update ngày cũ '.$this->created_date);
                return ;
            }
        }

        $json_var = [];
        foreach($aUid as $uid) {
            if( !empty($uid) ){
                $mScheduleNotify = GasScheduleNotify::InsertRecord($uid, $notifyType, $this->id, '', $title, $json_var);
                if($sendNow && !is_null($mScheduleNotify)){
                    $mScheduleNotify->sendImmediateForSomeUser();
                }// Jan 05, 2016 tạm close send luôn lại, vì nó gây chậm bên PMBH khi send
                // sẽ gửi = cron
            }
        }
    }
    
    /** @Author: DungNT Oct 12, 2018
     *  @Todo: handle notify Order Timer 
     **/
    public function runInsertNotifyTimer($notifyType, $aUid, $title, $sendNow) {
        $minutesAdd = 60;
        $dateNow    = date('Y-m-d H:i:s');
        $timeSend   = $this->delivery_timer;
        $json_var = [];
        
        foreach($aUid as $uid) {
            if(empty($uid) ){
               continue ; 
            }
            if($sendNow){
                $mScheduleNotify = GasScheduleNotify::InsertRecord($uid, $notifyType, $this->id, '', $title, $json_var);
                if(!is_null($mScheduleNotify)){
                    $mScheduleNotify->sendImmediateForSomeUser();
                }
            }
            $minutes    = MyFormat::getMinuteTwoDate($dateNow, $timeSend);
            foreach (Forecast::$ArrayHourSend as $key => $hourSend) {
                if($hourSend*$minutesAdd < $minutes){
                    $timeSendModify   = MyFormat::addDays($timeSend, $minutesAdd*$hourSend, '-', 'minutes', 'Y-m-d H:i:s');
                    $mScheduleNotify = GasScheduleNotify::InsertRecord($uid, $notifyType, $this->id, $timeSendModify, $title, $json_var); 
                }

            }
        }
    }
    
    /**
     * @Author: DungNT Dec 14, 2016
     * @Todo: map data from Sell to TransactionHistory
     * map rồi có thể save vào TransactionHistory hoặc không save cũng dc
     */
    public function saveDataFromSell($mSell) {
        if(empty($mSell->employee_maintain_id)){// vì là khi chưa có GN nào thì trạng thái là đang xử lý, khi GN pick đơn hàng thì sẽ là đang giao
//            $this->status           = Transaction::STATUS_PROCESSING;// Jul 13, 2017 không để trạng thái là processin nữa, không cần thiết
            $this->status           = Transaction::STATUS_NEW;
            $this->action_type      = Transaction::EMPLOYEE_FREE;
        }
        if($mSell->status == Sell::STATUS_PAID){// Jan 09, 2017 xử lý update back lại transaction khi PMBH hoàn thành - thu tiền
            $this->status           = Transaction::STATUS_DONE;
            $this->action_type      = Transaction::EMPLOYEE_COMPLETE;
        }elseif($mSell->status == Sell::STATUS_CANCEL){
            $this->status           = Transaction::STATUS_CANCEL_BY_EMPLOYEE;
            $this->action_type      = Transaction::EMPLOYEE_DROP;
        }elseif($mSell->status == Sell::STATUS_NEW){
//            if(empty($mSell->employee_maintain_id)){
//                $this->status           = Transaction::STATUS_DELIVERY_PROCESSING;
//                $this->action_type      = Transaction::EMPLOYEE_FREE;
//            }// chỗ này chưa biết xử lý sao, chỗ này làm để NV CallCenter sửa khi NVGN gọi lên sửa đơn hàng            
        }
        $this->agent_id     = $mSell->agent_id;
        $this->sell_id      = $mSell->id;
        $this->source       = $mSell->source;
        // copyEmployeeToTransactionHistory KTBH + giao nhan
        $this->employee_accounting_id    = $mSell->uid_login;// KTBH
        $this->employee_maintain_id      = $mSell->employee_maintain_id;// NVGN
        $this->address                   = $mSell->address;// address update ttrên giao diện
        $this->note                      = $mSell->note;// address update ttrên giao diện
        $this->phone                     = $mSell->phone;// phone update ttrên giao diện

        $this->total             = $mSell->total;
        $this->grand_total       = $mSell->grand_total;
        $this->json_detail       = $mSell->transactionEncodeJsonDetail();
        $this->qty_discount      = $mSell->qty_discount;
        $this->amount_discount   = $mSell->amount_discount;
        $this->amount_bu_vo      = $mSell->amount_bu_vo;
        
        $this->order_type       = $mSell->order_type;
        $this->type_amount      = $mSell->type_amount;
        $this->customer_id      = $mSell->customer_id;// Apr 09, 2017 cho phép đổi customer_id - chưa rõ có allow không nhưng cứ làm vậy trc đã
        $this->type_customer    = $mSell->type_customer;
        $this->first_name       = !empty($mSell->first_name) ? $mSell->first_name : $mSell->getCustomer('first_name');// Apr 09, 2017 cho phép đổi customer_id - chưa rõ có allow không nhưng cứ làm vậy trc đã
        $this->support_id       = $mSell->support_id;
        $this->province_id      = $mSell->province_id;
        
        // code from Sep1217
        $this->promotion_amount = $mSell->promotion_amount;
        $this->high_price       = $mSell->high_price;
        
        $this->gas_remain       = $mSell->gas_remain;
        $this->gas_remain_amount= $mSell->gas_remain_amount;
        $this->kg_empty         = $mSell->kg_empty;
        $this->kg_has_gas       = $mSell->kg_has_gas;
        $this->created_date_only= $mSell->created_date_only;
        $this->delivery_timer   = $mSell->delivery_timer;
        $this->status_cancel    = $mSell->status_cancel;
        $this->is_timer         = $mSell->is_timer;

//        $this->update($aUpdate); 
        $this->update();// Dec 20, 2016 nên update hết không 
        if($mSell->status == Sell::STATUS_CANCEL){
            if(!empty($mSell->mBeforeUpdate) && $mSell->mBeforeUpdate->status != $mSell->status){
                $this->rollbackPromotionByHand();
            }
            $this->deleteNotify(false);
        }
    }
    
    /**
     * @Author: DungNT Dec 13, 2016
     * @Todo: check xem ai có thể view đơn hàng trên app
     */
    public function canViewOnApp($mUserLogin) {
        $aRoleAllow = GasConst::getRoleAppReport();
        if ($this->customer_id == $mUserLogin->id) {
            // Customer must to see their own order
            return true;
        } else if ($mUserLogin->role_id == ROLE_CUSTOMER && $this->customer_id != $mUserLogin->id) {
            return false;
        } elseif (!in_array($mUserLogin->role_id, $aRoleAllow)) {
            return false;
        }
        return true;
    }
    
    /**
     * @Author: DungNT Dec 13, 2016
     * @Todo: check employee can pick Order
     */
    public function canPick() {
        $aStatusAllow = [Transaction::STATUS_NEW, Transaction::STATUS_COMPLETE];
        if(!in_array($this->mAppUserLogin->role_id, $this->getRoleCanHandleHgd()) || !in_array($this->status, $aStatusAllow)){
            return 0;
        }
        if($this->action_type == Transaction::EMPLOYEE_FREE){
            return 1;
        }
        return 0;
    }
    public function canPickCancel($mUserLogin) {
        if(!in_array($this->mAppUserLogin->role_id, $this->getRoleCanHandleHgd())){
            return 0;
        }
//        if($this->action_type == Transaction::EMPLOYEE_NHAN_GIAO_HANG && $this->employee_maintain_id == $mUserLogin->id){
        if(!is_null($mUserLogin) && $this->employee_maintain_id == $mUserLogin->id
            && $this->action_type == Transaction::EMPLOYEE_NHAN_GIAO_HANG){
            return 1;
        }
        return 0;
    }
    
    /**
     * @Author: DungNT Dec 13, 2016
     * @Todo: xử lý các hành động của user PVKH với đơn hàng
     */
    public function handleSetEvent($q, $mUserLogin) {
        try{
        $this->oldJsonDetail = MyFormat::jsonEncode($this->getAttributes());
        $status_obj = '';
        if(isset($q->list_id_image) && is_array($q->list_id_image)){
            $this->list_id_image = $q->list_id_image;
        }
        switch ($q->action_type) {
            case Transaction::CHANGE_DISCOUNT:// Sep1217
                $this->employeeChangeDiscount($q);
                break;
            case Transaction::EMPLOYEE_NHAN_GIAO_HANG:
                $this->employeePick($q, $mUserLogin);
                $status_obj = MonitorUpdate::STATUS_CONFIRM;
                break;
            case Transaction::EMPLOYEE_HUY_GIAO_HANG:
                $this->employeeCancelPick($q, $mUserLogin);
                $status_obj = MonitorUpdate::STATUS_CANCEL;
                break;
            case Transaction::EMPLOYEE_DROP:
                $this->employeeDrop($q, $mUserLogin);
                $status_obj = MonitorUpdate::STATUS_CANCEL;
                break;
            case Transaction::EMPLOYEE_CHANGE:
                $this->employeeChange($q, $mUserLogin);
                $this->saveFile();
                $this->deleteFile();
                break;
            case Transaction::EMPLOYEE_COMPLETE:
                $this->employeeComplete($q, $mUserLogin);
                $this->saveFile();
                $this->deleteFile();
                $status_obj = MonitorUpdate::STATUS_COMPLETE;
                break;

            default:
                $this->addError('id', 'Yêu cầu không hợp lệ');
                return ; 
        }
        
        $location = $q->latitude.",$q->longitude";
        if(!empty($status_obj)){
            MonitorUpdate::updateLocationEmployee($mUserLogin->id, $status_obj, $location, $this);
        }
        
        /* 1. update to model sell
         * 2. save new event
         */
//        $this->saveEvent($q, $mUserLogin);// luôn save vào event với mỗi sự kiện của user
        $this->saveEvent($mUserLogin->id, $q->action_type, $q->status_cancel, 0, $location);
        } catch (Exception $ex) {
            throw new Exception($ex->getMessage(), SpjError::SELL001);// Sep0119 nên để SpjError::SELL001 Hay $ex->getCode() ???
        }
    }
    
    /** @Author: DungNT Sep 12, 2017
     *  @Todo: handle employee CV change discount
     * 1. đưa vô và check lại xem tính toán đúng không ở Model Sell và TransactionHistory
     * 2. notify cho giao nhận của đại lý đó
     * 3. done
     */
    public function employeeChangeDiscount($q) {
        throw new Exception('Chức năng này đã bị tắt, vui lòng liên hệ Dũng IT để hỗ trợ: 0384 331 552 ');
        $amountDiscount = isset($q->input_promotion_amount) ? MyFormat::removeComma($q->input_promotion_amount)*1 : 0;
        if(!$this->showInputPromotionAmount()){
            throw new Exception('Bạn không thể nhập giảm giá cho đơn hàng này');
        }
        $mSell = $this->rSell;
        if($mSell){
            $mSell->promotion_amount = isset($q->input_promotion_amount) ? MyFormat::removeComma($q->input_promotion_amount) : $mSell->promotion_amount;
            $mSell->mapTransactionChange($q, $this);
        }
    }
    
    /** @Author: DungNT Dec 13, 2016
     *  @Todo: handle employee Pick (nhận đơn hàng)
     */
    public function employeePick($q, $mUserLogin) {
    try {
        if(!$this->canPick()){
            $nameEmployee = $this->getUserInfo('rEmployeeMaintain', 'first_name');
            throw new Exception('Đơn hàng đã có '.$nameEmployee.' nhận giao, bạn không thể nhận đơn hàng này');
        }
        $this->canConfirmMoreOrder($mUserLogin->id);
        $this->employee_maintain_id = $mUserLogin->id;
        $this->action_type          = Transaction::EMPLOYEE_NHAN_GIAO_HANG;
        $this->status               = Transaction::STATUS_DELIVERY_PROCESSING;
        $aUpdate = array('employee_maintain_id', 'action_type', 'status');
        $this->update($aUpdate);
        $this->deleteNotify();
        $this->setSellEmployee();
        // notify cho KH và NV giao nhận
        $this->notifyPhoneAppCustomer();
//        $aUid = $this->getListEmployeeMaintainOfAgent(); // không cần notify khi nhận nữa
//        $title  = $this->getUserInfo('rEmployeeMaintain', 'first_name').' nhận đơn hàng - '.$this->getFirstName(). ' - '.$this->getAddress();
//        $this->notifyPhoneAppEmployee($aUid, $title);
        } catch (Exception $exc) {
            $this->addError('id', $exc->getMessage());
            return;
        }
    }
    
    /** @Author: DungNT Now 19, 2017
     *  @Todo: Khi GN xác nhận App thì xóa các notify lỗi chưa xác nhận đơn hàng 5,10,20 phút
     **/
//    public function removeErrorsHgdConfirm() {
//        $this->deleteNotify();
//    }DungNT Close May0819
    
    /** @Author: DungNT Dec 13, 2016
     *  @Todo: handle employee Cancel Pick (Hủy nhận đơn hàng)
     */
    public function employeeCancelPick($q, $mUserLogin) {
        $employeeMaintainName = $this->getUserInfo('rEmployeeMaintain', 'first_name');
        if(!$this->employeeCanModify($mUserLogin) || $this->status == Transaction::EMPLOYEE_COMPLETE){
            $this->addError('id', 'Bạn không thể hủy nhận đơn hàng này');
            return;
        }
        $this->employee_maintain_id = 0;
        $this->action_type          = Transaction::EMPLOYEE_FREE;
        $this->status               = Transaction::STATUS_NEW;
        $this->pttt_code            = '';
        $aUpdate = array('employee_maintain_id', 'action_type', 'status', 'pttt_code');
        $this->update($aUpdate);
        $this->setSellEmployee();
        
        $aUid   = $this->getListEmployeeMaintainOfAgent();
        $title  = $employeeMaintainName.' HỦY NHẬN ĐƠN HÀNG '.$this->getFirstName(). ' - '.$this->getAddress();
        $this->notifyPhoneAppEmployee($aUid, $title);
        $this->notifyOrderNotConfirm();
    }
    
    /** @Author: DungNT Dec 13, 2016
     *  @Todo: check xem nhân viên giao nhận có đang thao tác đúng trên đơn hàng của mình nhận không
     */
    public function employeeCanModify($mUserLogin) {
        if($this->employee_maintain_id != $mUserLogin->id){
            return false;
        }
        return true;
    }
    // May1118Check hẹn giờ giao
    public function checkDeliveryTimer() {
        if(empty($this->delivery_timer) || $this->delivery_timer == '0000-00-00 00:00:00'){
            return '';
        }
        if(MyFormat::compareTwoDate($this->delivery_timer, date('Y-m-d H:i:s'))){
            throw new Exception("Không thể hủy đơn hàng, sau thời gian hẹn giao hàng {$this->getDeliveryTimer()} bạn mới được hủy đơn. Thời gian bắt đầu tính để hoàn thành đơn từ: {$this->getDeliveryTimer()}");
        }
    }
    
    /** @Author: DungNT Dec 14, 2016
     *  @Todo: cập nhật employee_maintain_id back lại model sell khi có NVGN nhận hoặc hủy nhận giao hàng
     */
    public function setSellEmployee() {
        // need update to table Sell
        $mSell = $this->rSell;
        if($mSell){
            $mSell->setEmployeeMaintainId($this->employee_maintain_id);
            if(!empty($this->employee_maintain_id)){// Feb 11, 2017 nếu NVGN nhận đơn hàng thì hủy notify
                GasSocketNotify::deleteByObjIdAndCode($mSell->id, GasSocketNotify::CODE_SELL_ALERT_KTBH);
            }else{// nếu hủy nhận đơn hàng thì tạo lại notify cho KTBH
                $mSell->makeSocketNotify();
            }
        }
    }
    /** @Author: DungNT Dec 13, 2016
     *  @Todo: handle employee Drop (Hủy đơn hàng - không giao hàng được)
     */
    public function employeeDrop($q, $mUserLogin) {
        if(!$this->employeeCanModify($mUserLogin)){
            $this->addError('id', 'Bạn không thể hủy đơn hàng này');
            return;
        }
        if($this->source == Sell::SOURCE_APP){
//        if(!in_array($q->status_cancel, $this->getCaseAllowDrop())){
//            $this->addError('id', 'Chỉ có thể hủy các lý do sau: Giao xa, Đại lý khác hỗ trợ, Nhầm đại lý. Những lý do khác bạn gửi ticket cho Ngọc để hủy.');
//            $this->addError('id', 'Không thể hủy đơn hàng App, Bạn gửi ticket cho Ngọc để hủy.');
//            return;
        }
        $this->checkDeliveryTimer();
        $this->doDrop($q);
        $this->doDropMakeSocketNotify();
    }
    
    /** @Author: DungNT Oct 19, 2016
     *  @Todo: handle App Employee + App Customer Drop (Hủy đơn hàng - không giao hàng được)
     */
    public function doDrop($q) {
        $this->removeSpjCodeOld();
        $infoCustomer = ''; $agentName = $this->getAgent();
        if($q->action_type == Transaction::EMPLOYEE_DROP){
            $this->status_cancel        = $q->status_cancel;
            $this->action_type          = Transaction::EMPLOYEE_DROP;
            $this->status               = Transaction::STATUS_CANCEL_BY_EMPLOYEE;
            $infoCustomer               = " {$this->getCodeNoSell()} - $agentName ";
        }elseif($q->action_type == Transaction::CUSTOMER_DROP){
            $this->status_cancel        = $q->status_cancel;
            $this->action_type          = Transaction::CUSTOMER_DROP;
            $this->status               = Transaction::STATUS_CANCEL_BY_CUSTOMER;
            $infoCustomer               = "{$this->getFirstName()} - {$this->getPhone()} - {$this->getAddress()}";
        }
        $this->pttt_code            = '';
        $this->complete_time        = date('Y-m-d H:i:s');
        $aUpdate = array('status_cancel', 'action_type', 'status', 'pttt_code', 'complete_time');
        $this->update($aUpdate);
        // need update to table Sell
        $mSell = $this->rSell;
        if($mSell){
            $mSell->mTransaction    = $this;
            $mSell->complete_time   = $this->complete_time;
            $mSell->setStatus(Sell::STATUS_CANCEL, $this->getStatusCancelText());
//            NamNH Jun 15, 2019 add change status of codePartner
            $mCodePartner = new CodePartner();
            $mCodePartner->updateCancel($mSell);
        }
//        $title  = "{$this->getCodeNo()} - {$this->getFirstName()}. Trạng thái: hủy" ;
        $employeeName   = $this->getUserInfo('rEmployeeMaintain', 'first_name');
        $employeeName   = !empty($employeeName) ? ''.$employeeName : ' KH';
        $title          = "Hủy đơn hàng {$this->getCreatedDateOnly()} ".$employeeName." $infoCustomer. Lý do: ".$this->getStatusCancelText();
        $this->notifyPhoneAppCustomer($title);
        
        $aUid = [];
        if(!empty($this->employee_maintain_id)){
            $aUid   = $this->getListEmployeeMaintainOfAgent();
            $this->getUidGdkv($aUid);
//            $this->alertAgentKhoan();// Close Sep3018 sẽ notify cho list CV + GĐKV 
        }else{// Oct0818 khi KH hủy app cũng notify cho GĐKV
//            $this->getUidGdkv($aUid);// Now1718 close lại, ko notify cho GĐKV khi KH hủy app nữa
        }        
        $this->notifyPhoneAppEmployee($aUid, $title);
        $this->deleteNotify(false);
        $this->rollbackPromotionByHand();
        
    }
    
    /** @Author: DungNT Jun 21, 2017
     * @Todo: notify cho chuyên viên nhận khoán
     */
    public function alertAgentKhoan() {
        if(in_array($this->agent_id, UsersExtend::getAgentAnhHieu())){
            $aUidNotify = [GasLeave::UID_CHIEF_MONITOR];
            $this->alertAgentKhoanDoInsert($aUidNotify);
            return ;
        }

        $uidKhoan = $this->getUidKhoan();
        if ($uidKhoan) {
            $aUidNotify = [$uidKhoan];
            $this->alertAgentKhoanDoInsert($aUidNotify);
            return ;
        }
    }
    
    /** @Author: DungNT Sep 15, 2017
     * @Todo: get user id Khoán - Chuyen Vien Hoac NV PVKHH
     */
    public function getUidKhoan() {
        $aAgentNotify = $this->getUidMonitor();// xử lý cho Võ Tấn Huy + Trưởng Gia Lai
        if (array_key_exists($this->agent_id, $aAgentNotify)) {
            return $aAgentNotify[$this->agent_id];
        }
        return false;
    }
    
    public function alertAgentKhoanDoInsert($aUidNotify) {
        $employeeMaintainName = $this->getUserInfo('rEmployeeMaintain', 'first_name');
        $title  = "$employeeMaintainName Hủy đơn hàng- {$this->getFirstName()}. {$this->getAddress()}".' - '.$this->getPhone() ;
        $this->notifyPhoneAppEmployee($aUidNotify, $title);
    }
    
    /** @Author: DungNT Aug2817
     * @Todo: notify cho chuyên viên nhận khoán khi có đơn hàng mới
     * 1. Võ Tấn Huy Q.6 và Q.10
     */
    public function alertAgentKhoanCreateNew() {
        $aAgentNotify = $this->getUidMonitor();
        if (!array_key_exists($this->agent_id, $aAgentNotify)) {
            return ;
        }
        $uid = $aAgentNotify[$this->agent_id];
        if(in_array($uid, GasConst::getUidKhoanOnlyHighPrice())){
            return ;// đại lý anh Hiêu ko gửi notify với đơn thường
        }
        $aUidNotify = [$uid];
        $this->notifyPhoneAppEmployee($aUidNotify);
    }
    
    /** @Author: DungNT Now3017
     * @Todo: notify cho chuyên viên khi có đơn hàng App
     */
    public function alertOrderGas24h() {
        return ;// Tạm bỏ, ko cần thiết
        if($this->source != Sell::SOURCE_APP){
            return ;
        }
        $aUidNotify = GasConst::getUidAlertOrderGas24h();
        $this->notifyPhoneAppEmployee($aUidNotify);
    }
    
    /** @Author: DungNT Sep1517
     * @Todo: notify cho chuyên viên nhận khoán khi có đơn hàng gía cao
     */
    public function notifyHighPrice() {
        if(empty($this->high_price)){
            return ;
        }
        $aAgentNotify = $this->getUidMonitor();
        if (!array_key_exists($this->agent_id, $aAgentNotify)) {
            return ;
        }
        $aUidNotify = [$aAgentNotify[$this->agent_id]];
        $title = "Đơn hàng giá cao: {$this->getFirstName()} - {$this->getAddress()}";
        $this->notifyPhoneAppEmployee($aUidNotify, $title);
    }

    /**
     * @Author: DungNT Dec 14, 2016
     * @Todo: handle employee Change - đổi gas, quà, nhập vỏ, thu tiền
     */
    public function employeeChange($q, $mUserLogin) {
        if(!$this->employeeCanUpdate($mUserLogin)){
            $this->addError('id', 'Bạn không thể chỉnh sửa đơn hàng này. Đơn hàng đã thu tiền hoặc bị hủy');
            return;
        }
        if(!is_array($q->order_detail)){
            return;
        }
        $this->checkDiscount($q);
        /* 1. copy sang model Sell để tính toán chiết khấu + bù vỏ
         * 2. save new info to model Transaction History
         */
        $mSell = $this->rSell;
        if($mSell){
            $mSell->promotion_amount = isset($q->input_promotion_amount) ? MyFormat::removeComma($q->input_promotion_amount) : $mSell->promotion_amount;
            $mSell->mapTransactionChange($q, $this);
            $this->setEventChangeDiscount($q);
            $this->updateSellIdToSpjCode($mSell);
        }
    }
    
    /** @Author: DungNT Now 13, 2018
     *  @Todo: kiểm tra hợp lệ giảm giá
     * 50k x SL b12 của đơn
     **/
    public function checkDiscount($q) {
        $mSell = $this->rSell;
        $currentDiscount = isset($q->input_promotion_amount) ? MyFormat::removeComma($q->input_promotion_amount)*1 : 0;
        $currentDiscount += $this->amount_discount;
        $currentDiscount += ($this->qty_discount * $mSell->getPromotionDiscount());// Fix Apr1019
        
        $qtyB12 = 0;
        foreach($q->order_detail as $objDetail){
            if($objDetail->materials_type_id == GasMaterialsType::MATERIAL_BINH_12KG){
                $qtyB12++;
            }
        }
        $maxDiscount = TransactionHistory::MAX_DISCOUNT * $qtyB12;
//        Logger::WriteLog("SellId = $mSell->id -- TransId = $this->id ---  maxDiscount = $maxDiscount -- currentDiscount = $currentDiscount");
        if(empty($this->promotion_id) && $currentDiscount > $maxDiscount ){// Now0518 fix max allow discount
            throw new Exception('Giảm giá không thể quá ' .ActiveRecord::formatCurrency($maxDiscount). '. Đơn hàng này có chiết khấu hiện tại là: ' .ActiveRecord::formatCurrency($currentDiscount). '. Chi tiết liên hệ 0918 318 328');
        }
    }
    
    /** @Author: DungNT Sep 15, 2017
     *  @Todo: set event va notify change discount
     */
    public function setEventChangeDiscount($q) {
        if($this->promotion_amount_old == $this->promotion_amount || empty($this->promotion_amount)){
            return ;
        }
        $location = $q->latitude.",$q->longitude";
        $this->saveEvent($this->mAppUserLogin->id, Transaction::CHANGE_DISCOUNT, 0, 0, $location);
        $aUid   = $this->getListEmployeeMaintainOfAgent(); // notify lại cho all Giao nhận
        $title  = 'Đã xác nhận giảm giá đơn hàng - '.$this->getFirstName(). ' - '.$this->getAddress();
        $this->notifyPhoneAppEmployee($aUid, $title);
    }
    
    /**
     * @Author: DungNT Jun 08, 2017
     * @Todo: check code user PVKH nhập lên App có hợp lệ không
     */
    public function checkSpjCode() {
    try {
        if(empty($this->pttt_code)){
           return ; 
        }
        $correctSeri = false;
        $this->mSpjCode             = new SpjCode();
        $this->mSpjCode->code       = $this->pttt_code;
        $this->mSpjCode->status     = SpjCode::STATUS_USED;
        $this->mSpjCode             = $this->mSpjCode->getModelCodeUser();
        if(!empty($this->mSpjCode)){
            $this->checkSpjCodeMoreCondition($this->mSpjCode, $correctSeri);
        }

        if(empty($this->mSpjCode) || (!empty($this->mSpjCode) && !$correctSeri)){
            $msg = 'Mã code PTTT không hợp lệ';
            if(!empty($this->mSpjCode) && !$correctSeri){
                $msg = 'Mã code PTTT: Seri hoặc thương hiệu vỏ không đúng';
            }
            throw new Exception($msg);
        }
    } catch (Exception $exc) {
        throw new Exception($exc->getMessage());
    }
    }
    /** Feb1118 kiểm tra thêm điều kiện Phone, vỏ hoặc seri phải khớp khi nhập Code PTTT bình quay về 
     *  @param: $mSpjCode model current spj code
     */
    public function checkSpjCodeMoreCondition($mSpjCode, &$correctSeri) {
        try {
//        $correctSeri = true;
//        return ;// Feb1518 tạm bỏ check $correctSeri vì có mã PTTT ko vào nhà KH đc, nên ko ghi seri và thương hiệu vỏ
//        
        $mUser = Users::model()->findByPk($mSpjCode->pttt_customer_id);
        $mUsersHgdPttt = UsersHgdPttt::model()->findByPk($mSpjCode->pttt_hgd_id);
        $mSell = $this->mSellToCheckSpjCode;
        if((empty($mUser) && empty($mUsersHgdPttt)) || empty($mSell)){
            return ;
        }
        
        $mSell->checkOrderPtttCode();// add Mar1118 thêm điều kiện nếu KH đã mua hàng 1 lần rồi thì không tính bình quay về nữa
        $mSell->checkExpiredDatePttt();// add May0918 thêm điều kiện nếu chương trình PTTT đã kết thúc thì ko cho nhệp mã code nữa
//        $dateBeginCheck = '2018-02-22';// Feb2118 Phúc yêu cầu chỉ check những KH tạo từ ngày 22-02-18
//        $dateBeginCheck = '2017-11-01';// Mar0918 Châu yêu cầu chỉ check những KH tạo từ ngày 22-02-18
        $ptttSeri = $ptttVo = -1;
        if(!empty($mUser)){
            $mUser->LoadUsersRef();
            $ptttSeri   = $mUser->mUsersRef->contact_technical_name;// seri và loại vỏ mà PTTT đã nhập trc đây
            $ptttVo     = $mUser->mUsersRef->contact_technical_phone;
        }else{
            $mUser = new Users();
            $this->reloadSeriAndPtttCode($ptttSeri,$ptttVo,$mUser,$mUsersHgdPttt);
        }
            
        $mTargetDaily       = new TargetDaily();
        $countMonth         = $mTargetDaily->getMonthCheck($mSell);
        $dateBeginCheck     = MyFormat::modifyDays(date('Y-m-d'), $countMonth, '-', 'month');// Mayy0619 Ngọc - Y/C chỉ tính BQV trong vòng 3 tháng
        if(MyFormat::compareTwoDate($dateBeginCheck, $mUser->created_date)){
            throw new Exception("Mã code đã quá 60 ngày, hệ thống không cho nhập. User {$mUser->id} Ngày nhân viên CCS tạo mã: " . $mUser->getCreatedDate());
        }
        
        foreach($mSell->aDetail as $mDetail):
            if($mDetail->materials_type_id == GasMaterialsType::MATERIAL_VO_12){
                if((!empty($mDetail->seri) && $mDetail->seri == $ptttSeri) || $mDetail->materials_id == $ptttVo){
                    $correctSeri = true; 
                }
            }
        endforeach;
        
        $phoneSell = '0'.$this->phone;
        if($phoneSell == $mUser->phone){
            $correctSeri = true;// Jul1219 DungNT tạm ko check phone nữa, Ngọc nói dễ bùa chỉ check seri + vỏ 
        }
    } catch (Exception $exc) {
        throw new Exception($exc->getMessage());
    }
    }
    
    /** @Author: NamNH Oct0719
    *  @Todo:update by model usersHgdPttt, update $mUsers->created_date;
    *  @Param:param
    **/
    public function reloadSeriAndPtttCode(&$ptttSeri,&$ptttVo,&$mUsers,$mUsersHgdPttt){
        if(empty($mUsersHgdPttt)){
            return;
        }
        $ptttSeri   = $mUsersHgdPttt->username;
        $ptttVo     = $mUsersHgdPttt->email;
        $mUsers->created_date = $mUsersHgdPttt->created_date;
        $mUsers->phone = $mUsersHgdPttt->phone;
    }
    
    public function removeSpjCodeOld() {
        if(empty($this->pttt_code)){
           return ; 
        }
        $mSpjCode             = new SpjCode();
        $mSpjCode->code       = $this->pttt_code;
        $mSpjCode->status     = SpjCode::STATUS_GO_BACK;
        $mSpjCode             = $mSpjCode->getModelCodeUser();
        if($mSpjCode){
            $mSpjCode->status       = SpjCode::STATUS_USED;
            $mSpjCode->sell_id      = 0;
            $mSpjCode->date_delivery = null;
            $mSpjCode->update();
        }
    }
    
    /**
     * @Author: DungNT Jun 08, 2017
     * @Todo: Cập nhật sell id và status lại model SpjCode
     */
    public function updateSellIdToSpjCode($mSell) {
        if(is_null($this->mSpjCode)){
            return ;
        }
        $this->mSpjCode->updateSellIdToSpjCode($mSell);
    }
    
    /**
     * @Author: DungNT Dec 14, 2016
     * @Todo: handle employee Complete - xác nhận thu tiền
     */
    public function employeeComplete($q, $mUserLogin) {
        if(!$this->employeeCanUpdate($mUserLogin) || $this->status == Transaction::STATUS_CANCEL_BY_CUSTOMER){
            $this->addError('id', 'Bạn không thể chỉnh sửa đơn hàng này. Đơn hàng đã thu tiền hoặc bị hủy');
            return;
        }
        $this->action_type          = Transaction::EMPLOYEE_COMPLETE;
        $this->status               = Transaction::STATUS_DONE;
        $this->complete_time        = date('Y-m-d H:i:s');
//        $aUpdate = array('action_type', 'status');// Hàm này đã xử lý update hết rồi $mTransactionHistory->saveDataFromSell($this);
//        $this->update($aUpdate);// Close on Jan 08, 2017 xử lý trong mapTransactionChange
        $mSell = $this->rSell;// need update to table Sell
        if($mSell){
            $mSell->promotion_amount = isset($q->input_promotion_amount) ? MyFormat::removeComma($q->input_promotion_amount) : $mSell->promotion_amount;
            $mSell->status = Sell::STATUS_PAID;
            $mSell->mapTransactionChange($q, $this);
            $this->updateSellIdToSpjCode($mSell);
            $this->activeFirstBookApp();
            $mSell->employeeCompleteApp = true;// Feb2318 xử lý không run add Gas Remain
            $mSell->addGasRemain();
//            $this->checkErrorComplete($mSell);// Close Oct0218 change new flow
            $this->doFlowCompleteLate($mSell);
            $mFollowCustomer = new FollowCustomer();
            $mFollowCustomer->updateBySell($mSell);
        }

        $title  = $this->getUserInfo('rEmployeeMaintain', 'first_name'). " hoàn thành đơn hàng của bạn";
        $this->notifyPhoneAppCustomer($title);
        $this->deleteNotify(false);

//            [TASKTIMER]
//        update forecast Sep 27, 2018
        $mForecast = new Forecast();
        $mForecast->handleGenerateForecastSell([$this->customer_id]);
//            [TASK PARTNER CODE - Now2318 ]
//        $mCodePartner = new CodePartner(); // June1519 DungNT close
//        $mCodePartner->makeNewCode($mSell);
//            [TASK REWARD]
        $mRewardPoint = new RewardPoint();
        $mRewardPoint->addPoint($mSell);
        $this->updateAppPromotionUser();//NamNH Sep1819 update app promotion users
        $this->addSpinNumber($mSell); //DungNT close Sep2919 debug not write log complete
    }
    
    /** @Author: DuongNV Sep1919 Nhận dc vé số khi mua hàng
    /** @Fix: DungNT Sep 29, 2019 move separate function 
     **/
    public function addSpinNumber($mSell) {
        // DuongNV Sep1919 Nhận dc vé số khi mua hàng
        $mSpinNumbers           = new SpinNumbers();
        $mSpinNumbers->phone    = $mSell->phone;
        $numberSource           = SpinNumbers::SOURCE_TYPE_APP;
        if($mSell->source == Sell::SOURCE_WEB){
            $numberSource       = SpinNumbers::SOURCE_TYPE_CALL;
        }
        $mSpinNumbers->saveUserNumber($mSell->customer_id, $numberSource, $mSell->code_no);
    }
    
    /** @Author: NamNH Sep1819
     *  @Todo: cập nhật trạng thái promotion khi hoàn thành đơn hàng
     **/
    public function updateAppPromotionUser() {
        if(empty($this->app_promotion_user_id)){
            return;
        }
        $mAppPromotionUser         = AppPromotionUser::model()->findByPk($this->app_promotion_user_id);
        if(empty($mAppPromotionUser)){
            return;
        }
        $mAppPromotionUser->setUserApply();
    }
    /** @Author: DungNT Aug 14, 2017
     * @Todo: active first book app
     * 1. check source order
     * 2. check first order of customer
     * 3. if is first order then update set flag first BookApp to User 
     */
    public function activeFirstBookApp() {
        if($this->source != Sell::SOURCE_APP){
            return ;
        }
        // Tắt hàm để model referral tracking kiểm tra. Ko kiểm tra 2 lần.
        if(false && !$this->isFirstBookApp()){
            return ;
        }
//        Logger::WriteLog("activeFirstBookApp Complete TransactionHistory id: $this->id book app. Customer id: $this->customer_id");
        $mReferralTracking = new ReferralTracking();
        $mReferralTracking->invited_id = $this->customer_id;
        $mReferralTracking->activateFirstOrder();
    }
    
    /** @Author: DungNT Aug 14, 2017
     * 2. check first order of customer
     * 3. if is first order then update set flag first BookApp = 1 (first_char) to User 
     */
    public function isFirstBookApp() {
        $mCustomer = $this->rCustomer;
        if($mCustomer->first_char == 1){// đã có đơn đầu tiên
            return false;
        }
        // nếu không phải là đơn đầu tiên thì set flag đơn đầu tiên = 1
        $mCustomer->first_char = 1;
        $mCustomer->update(['first_char']);
        return true;
    }

    /**
     * @Author: DungNT Dec 13, 2016
     * @Todo: save event each time user action to Order
     */
//    public function saveEvent($q, $mUserLogin) {
    public function saveEvent($user_id, $action_type, $status_cancel, $change_type, $google_map) {
        $mEvent = new TransactionEvent();
        $mEvent->transaction_history_id     = $this->id;
        $mEvent->user_id                    = $user_id;
        $mEvent->action_type                = $action_type;
        $mEvent->google_map                 = $_SERVER['SERVER_ADDR']. " - $google_map";
        $aTypeSave = [Transaction::EMPLOYEE_CHANGE, Transaction::CHANGE_DISCOUNT];
        if(in_array($mEvent->action_type, $aTypeSave)){
            $mEvent->change_type    = $change_type;
            $mEvent->json_data      = $this->oldJsonDetail;
        }
        $mEvent->status_cancel              = $status_cancel;
        $mEvent->save();
    }
    
    /** @Author: DungNT Sep 26, 2017
     * @Todo: 
     */
    public function myLuongUpdate() {
        return false;// Now1917 close
        if($this->agent_id == 970958 && (date('d') == 26 || date('d') == 27) ){// Cho phép Mỹ Luông sửa trên app từ đầu tháng
            return true;
        }
        return false;
    }
    
    
    /** @Author: DungNT Dec 14, 2016
     *  @Todo: check xem User có được quyền sửa Order không
     */
    public function employeeCanUpdate($mUserLogin) {
        if($this->myLuongUpdate()){// Cho phép Mỹ Luông sửa trên app từ đầu tháng
            return '1';
        }
//        return '1'; // only dev test need close on live
        if(is_null($mUserLogin)){
            return '0';
        }
        $aStatusNotAllow = [Transaction::STATUS_DONE, Transaction::STATUS_CANCEL_BY_EMPLOYEE, Transaction::STATUS_CANCEL_BY_CUSTOMER];
        if(in_array($this->status, $aStatusNotAllow)){
            return '0';
        }

        $aAgentId = $this->getListAgentKhoan();
        if(count($aAgentId) > 0){// xử lý cho chuyên viên CCS save discount
            return '1';
        }
        if($this->employee_maintain_id != $mUserLogin->id){// May 22, 2017 Big Bug, nếu không check cái này thì NV sẽ không xác nhận mà tự hoàn thành luôn => fix oải
            return '0';
        }
        $date_check = MyFormat::modifyDays($this->created_date_only, Yii::app()->params['DaysUpdateAppBoMoi']);
        if(!MyFormat::compareTwoDate($date_check, date('Y-m-d'))){
            return '0';
        }// May 17,  2017 allow update in day
        
        return '1';
    }
    
    /**
     * @Author: DungNT Feb 05, 2017
     * @Todo: set rating của customer, chỉ cho set 1 lần
     */
    public function handleSetRating($q, $mUser) {
        if($this->rating){
            return ;// nếu có rating rồi ko cho rating nữa
        }
        $this->rating           = $q->rating;
        $this->rating_comment   = $q->rating_comment;
        $aUpdate = ['rating', 'rating_comment'];;
        $this->update($aUpdate);
        $this->ratingAlertEmail();
    }
    
    /** @Author: DungNT Dec 05, 2018
     *  @Todo: cảnh báo những rating từ 3 trở xuống, để audit gọi lại
     **/
    public function ratingAlertEmail() {
        if($this->rating > 3 || empty($this->rating)){
            return ;
        }
        $title = "Đơn hàng: {$this->code_no_sell} KH App Gas24h có rating $this->rating cần kiểm tra lại";
        $needMore               = ['title'=> $title];
        $needMore['list_mail']  = ['dungnt@spj.vn', 'callcenter.audit@spj.vn'];
        $body = "$title";
        SendEmail::bugToDev($body, $needMore);
    }
    
    /**
     * @Author: DungNT Apr 06, 2017
     * @Todo: xóa record by Sell id
     */
    public static function deleteBySell($sell_id) {
        if(empty($sell_id)) return ;
        $criteria = new CDbCriteria();
        $criteria->addCondition('sell_id=' . $sell_id);
        TransactionHistory::model()->deleteAll($criteria);
    }
    
    /********** FOR FILE HANDLE ************************/
     /**
     * @Author: DungNT Jun 08, 2017
     * @Todo: Api validate multi file
     * @param: $this là model chứa error
     * ở đây là model GasUpholdReply
     */
    public function apiValidateFile() {
    try{
        $ok=false; // for check if no file submit
        if(isset($_FILES['file_name']['name'])  && count($_FILES['file_name']['name'])){
            foreach($_FILES['file_name']['name'] as $key=>$item){
                $mFile = new GasFile('UploadFile');
                $mFile->file_name  = CUploadedFile::getInstanceByName( "file_name[$key]");
                $mFile->validate();
                if(!is_null($mFile->file_name) && !$mFile->hasErrors() ){
                    $ok=true;
                    MyFormat::IsImageFile($_FILES['file_name']['tmp_name'][$key]);
//                    $FileName = MyFunctionCustom::remove_vietnamese_accents($mFile->file_name->getName());
//                    if(strlen($FileName) > 100 ){
//                        $mFile->addError('file_name', "Tên file không được quá 100 ký tự, vui lòng đặt tên ngắn hơn");
//                    }// không cần lấy file_name
                }
                if($mFile->hasErrors()){
                    $this->addError('file_name', $mFile->getError('file_name'));
                }
            }
        }
        
//        if(!$ok){
//            if($this->isNewRecord && in_array($this->master_lookup_id, $this->getTypeRequiredFile())) {
//                $this->addError('file_name', 'Có lỗi. Bạn phải chụp hình chứng từ của phiếu chi');
//            }
//        }
        
    } catch (Exception $ex) {
        throw new Exception($ex->getMessage());
    }
    }
    
    /** @Author: DungNT May 18, 2017
     * @Todo: save record file if have
     */
    public function saveFile() {
        // 5. save file
        if(!is_null($this->id)){
            GasFile::ApiSaveRecordFile($this, GasFile::TYPE_11_HGD_PTTT_CODE);
        }
    }
    public function deleteFile() {
        if(!is_array($this->list_id_image) || count($this->list_id_image) < 1){
            return ;
        }
        foreach($this->list_id_image as $file_id){
            $mFile = GasFile::model()->findByPk($file_id);
            if(is_null($mFile)){
                continue;
            }
            if($mFile->type == GasFile::TYPE_11_HGD_PTTT_CODE){
                $mFile->delete();
            }
        }
    }
    /** @Author: DungNT May 27, 2016
    *   @Todo: get html file view on grid
    */
    public function getFileOnly() {
        $res = '';
        $cmsFormater = new CmsFormatter();
        $res .= $cmsFormater->formatBreakTaskDailyFile($this, array('width'=>30, 'height'=>30));
        return $res;
    }
    /********** FOR FILE HANDLE ************************/
    
    /**
     * @Author: DungNT Jun 11, 2017
     * @Todo: đổi role  2 chiều từ hộ GĐ sang Bò Mối
     */
    public function renderHtmlChangeRole() {
        $cRole = MyFormat::getCurrentRoleId();
        $aRoleAllow = [ROLE_CALL_CENTER, ROLE_DIEU_PHOI];
        if(!in_array($cRole, $aRoleAllow)){
            return '';
        }

        $changeTo = 'Hộ GĐ';
        if($cRole == ROLE_CALL_CENTER){
            $changeTo = 'Bò Mối';
        }
        $url = Yii::app()->createAbsoluteUrl('admin/transaction/index', ['ChangeRole'=>1]);
        $alertText = "Bạn chắc chắn muốn chuyển sang $changeTo?";
        $html = "<a class='btn_cancel f_size_12 btn_closed_tickets' alert_text='$alertText' href='$url'>Chuyển $changeTo</a>";
        return $html;
    }
    /**
     * @Author: DungNT Jul 27, 2018
     * @Todo: render list ext online
     */
    public function renderListExtOnline() {
        $mAppCache = new AppCache();
        $aExt = $mAppCache->getCacheDecode(AppCache::EXT_EMPLOYEE);
        if(empty($aExt)){
            return '';
        }
        $aExt = array_keys($aExt);
        return " Online: ".  implode(', ', $aExt);
    }
    /**
     * @Author: DungNT Sep 14, 2018
     * @Todo: get list user HGD online by EXT
     * @return: user_id
     */
    public function getRandomUserHgdOnline() {
        $aExt = $this->getExtHgdOnline();
        $k = array_rand($aExt);
        return !empty($aExt[$k]) ? $aExt[$k] : 0;
    }
    
    /** @Author: DungNT Sep 21, 2018
     *  @Todo: get list ext HGD online 
     * 1. xử lý ở GasScheduleNotify->moveToRandomCallCenter()
     **/
    public function getExtHgdOnline() {
        $mAppCache = new AppCache();
        $aExt       = $mAppCache->getCacheDecode(AppCache::EXT_EMPLOYEE);
        if(!is_array($aExt)){
            return [];
        }
        unset($aExt[214]);
        foreach($aExt as $ext => $user_id){
            if($ext > Call::MAX_EXT_HGD){
                unset($aExt[$ext]);
            }
        }
        return $aExt;
    }
    
    /** @Author: DungNT Sep 27, 2018
     *  @Todo: get list ext HGD online only CallCenter
     * 1. isExtCallCenter for check render view 
     * 2. copyToHistory -> $mHistory->employee_accounting_id = $mHistory->getRandomUserHgdOnline();
     **/
    public function getExtHgdOnlineOnlyCallCenter() {
        $mAppCache = new AppCache();
        $aExt       = $mAppCache->getCacheDecode(AppCache::EXT_EMPLOYEE);
        if(!is_array($aExt)){
            return [];
        }
        foreach($aExt as $ext => $user_id){
            if($ext > Call::MAX_EXT_HGD){
                unset($aExt[$ext]);
            }
        }
        return $aExt;
    }
    
    /**
     * @Author: DungNT Jun 11, 2017
     * @Todo: đổi role  2 chiều từ hộ GĐ sang Bò Mối
     */
    public function handleChangeRole() {
        if(isset($_GET['ChangeRole'])){
            $cRole = MyFormat::getCurrentRoleId();
            $aRoleAllow = [ROLE_CALL_CENTER, ROLE_DIEU_PHOI];
            if(!in_array($cRole, $aRoleAllow)){
                return '';
            }
            $mUser = Users::model()->findByPk(MyFormat::getCurrentUid());
            $mMonitorUpdate = new MonitorUpdate();
            $mMonitorUpdate->checkCanChangeRole(MyFormat::getCurrentUid());
            
            if($mUser->role_id == ROLE_CALL_CENTER){
                $mUser->role_id = ROLE_DIEU_PHOI;
            }elseif($mUser->role_id == ROLE_DIEU_PHOI){
                $mUser->role_id = ROLE_CALL_CENTER;
            }
            $cRoleText      = MyFormat::getRoleName($cRole);
            $cRoleTextNew   = MyFormat::getRoleName($mUser->role_id);
            $description = "Change Role From [$cRoleText] to [$cRoleTextNew] ";
            GasTrackLogin::SaveRecord(GasTrackLogin::TYPE_CHANGE_ROLE, $description, $mUser->id, $cRole);
            $mUser->update(['role_id']);
            $this->setCacheRole();
            GasCheck::LogoutUser();
        }
    }
    
    /** @Author: DungNT Aug 29, 2019
     *  @Todo: set some cache of ROLE_DIEU_PHOI, ROLE_CALL_CENTER
     **/
    public function setCacheRole() {
        $mAppCache = new AppCache();
        $mAppCache->setArrayIdRole(ROLE_DIEU_PHOI);
        $mAppCache->setArrayIdRole(ROLE_CALL_CENTER);
    }
    
    /** @Author: DungNT Jun 19, 2017 addErrorsHgdConfirm
     *  @Todo: notify cho giám sát nếu sau 5 phút không xác nhận đơn hàng
     *  notify không xác nhận đơn hàng HGĐ: 5 phút, 10 phút, 20 phút
     * @Flow:
     * @A1: Xác nhận đơn 
     * 1. Các mốc nhắc nhở xác nhận đơn: 2 phút, 3 phút - PVKH
     * 2. Các mốc nhắc nhở xác nhận đơn: 5, 8 phút, CV, GĐKV 
     * 3. Các mốc phạt xác nhận đơn: 5,10,15 phút- PVKH, CV, GĐKV 
     * @A2: Hoàn thành đơn 
     * 1. Các mốc nhắc nhở Hoàn thành đơn: 18,28,38 phút- đơn App phạt CV, đơn App phạt  GDKV, Ho GD
     */
    public function notifyOrderNotConfirm() {
        if(in_array($this->agent_id, UsersExtend::getAgentNotRunApp())){
            return ;
        }
        $timeNow = date('Y-m-d H:i:s');
//            [TASKTIMER]
        if($this->is_timer == Forecast::TYPE_TIMER){
            $timeNow = $this->delivery_timer;
        }
        $agentName = $this->getAgent(); $aRowInsert = [];
        $time5      = 5; // minutes
        $time10     = 10;
        $time20     = 20;
        $time3      = 2;
        $time15     = 15;
        $time3Gdkv  = 3;// Sau 3 phut khong xac nhan thi gui cho GDKV
        $time8      = 8;// Sau  8 phut khong xac nhan thi gui cho GDKV va chuyen vien
        
        $time18     = 18;// notify 18p hoàn thành đơn hàng app - alert GDKV Kien
        $time28     = 28;// notify 28p hoàn thành đơn hàng app - alert GDKV Kien
        $time38     = 38;// notify 38p hoàn thành đơn hàng hộ - alert GDKV Kien
        $timeAlert5     = MyFormat::addDays($timeNow, $time5, '+', 'minutes', 'Y-m-d H:i:s');
        $timeAlert10    = MyFormat::addDays($timeNow, $time10, '+', 'minutes', 'Y-m-d H:i:s');
        $timeAlert20    = MyFormat::addDays($timeNow, $time20, '+', 'minutes', 'Y-m-d H:i:s');
        $timeAlert3     = MyFormat::addDays($timeNow, $time3, '+', 'minutes', 'Y-m-d H:i:s');
        $timeAlert15    = MyFormat::addDays($timeNow, $time15, '+', 'minutes', 'Y-m-d H:i:s');
        $timeAlert3Gdkv = MyFormat::addDays($timeNow, $time3Gdkv, '+', 'minutes', 'Y-m-d H:i:s');
        $timeAlert8     = MyFormat::addDays($timeNow, $time8, '+', 'minutes', 'Y-m-d H:i:s');
        
        $timeAlert18    = MyFormat::addDays($timeNow, $time18, '+', 'minutes', 'Y-m-d H:i:s');
        $timeAlert28    = MyFormat::addDays($timeNow, $time28, '+', 'minutes', 'Y-m-d H:i:s');
        $timeAlert38    = MyFormat::addDays($timeNow, $time38, '+', 'minutes', 'Y-m-d H:i:s');
//        $message5       = "$time5 phút - {$this->getPhone()} -".$agentName. " chưa xác nhận DH: {$this->getFirstName()} - {$this->getAddress()}";
        $message5       = "$time5 phút - $agentName chưa xác nhận DH: {$this->getCodeNoSell()} - {$this->getFirstName()} - {$this->getAddress()}";
        $message10      = "$time10 phút - $agentName chưa xác nhận DH: {$this->getCodeNoSell()} - {$this->getFirstName()} - {$this->getAddress()}";
        $message20      = "$time20 phút - $agentName chưa xác nhận DH: {$this->getCodeNoSell()} - {$this->getFirstName()} - {$this->getAddress()}";
        $message3       = "Nhắc nhở $time3 phút - $agentName chưa xác nhận DH: {$this->getCodeNoSell()} - {$this->getFirstName()} - {$this->getAddress()}";
        $message15      = "$time15 phút - $agentName chưa xác nhận DH: {$this->getCodeNoSell()} - {$this->getFirstName()} - {$this->getAddress()}";
        $message3Gdkv   = "$time3Gdkv phút - $agentName chưa xác nhận DH: {$this->getCodeNoSell()} - {$this->getFirstName()} - {$this->getAddress()}";
        $message8     = "$time8 phút - $agentName chưa xác nhận DH: {$this->getCodeNoSell()} - {$this->getFirstName()} - {$this->getAddress()}";
        
        $message18      = "$time18 phút - $agentName chưa hoàn thành DH: {$this->getCodeNoSell()} - {$this->getFirstName()} - {$this->getAddress()}";
        $message28      = "$time28 phút - $agentName chưa hoàn thành DH: {$this->getCodeNoSell()} - {$this->getFirstName()} - {$this->getAddress()}";
        $message38      = "$time38 phút - $agentName chưa hoàn thành DH: {$this->getCodeNoSell()} - {$this->getFirstName()} - {$this->getAddress()}";

        $json_var       = [];
        $aUid           = $this->getListEmployeeMaintainOfAgent();
        $aUidMonitor    = $this->getListUidMonitor();// Aug0918 mở để ngọc theo dõi đơn hộ GĐ chưa confirm
        $idGdkv         = $this->getOnlyIdGdkv();
        
        $created_date       = date('Y-m-d H:i:s');
        $typeNotify3        = GasScheduleNotify::ERROR_HGD_ALERT_3;
        $typeNotify5        = GasScheduleNotify::ERROR_HGD_ALERT_5;
        $typeNotify8        = GasScheduleNotify::ERROR_HGD_ALERT_8;
        $typeNotify10       = GasScheduleNotify::ERROR_HGD_ALERT_10;
        $typeNotify15       = GasScheduleNotify::ERROR_HGD_ALERT_15;
        $typeNotify3Gdkv    = GasScheduleNotify::ERROR_HGD_ALERT_3_GDKV;
        $typeNotify18       = GasScheduleNotify::ERROR_HGD_ALERT_COMPLETE_18;
        $typeNotify28       = GasScheduleNotify::ERROR_HGD_ALERT_COMPLETE_28;
        $typeNotify38       = GasScheduleNotify::ERROR_HGD_ALERT_COMPLETE_38;
        
        // 1. PVKH
        foreach($aUid as $uid){// Sep3018 không cảnh báo 10, 20 phút cho PVKH nữa chuyển sang cho chuyên viên và GĐKV
//            GasScheduleNotify::InsertRecord($uid, GasScheduleNotify::ERROR_HGD_ALERT_3, $this->id, $timeAlert3, $message3, $json_var);
            $this->notifyMapData($aRowInsert, $uid, $typeNotify3, $timeAlert3, $message3, $created_date);
            $this->notifyMapData($aRowInsert, $uid, $typeNotify5, $timeAlert5, $message5, $created_date);
        }
        
        // 2. Chuyen Vien
        if(!empty($this->idChuyenVien)){
            $this->notifyMapData($aRowInsert, $this->idChuyenVien, $typeNotify3, $timeAlert3, $message3, $created_date);
            $this->notifyMapData($aRowInsert, $this->idChuyenVien, $typeNotify5, $timeAlert5, $message5, $created_date);
            $this->notifyMapData($aRowInsert, $this->idChuyenVien, $typeNotify8, $timeAlert8, $message8, $created_date);
            $this->notifyMapData($aRowInsert, $this->idChuyenVien, $typeNotify10, $timeAlert10, $message10, $created_date);
            $this->notifyMapData($aRowInsert, $this->idChuyenVien, $typeNotify18, $timeAlert18, $message18, $created_date);
            $this->notifyMapData($aRowInsert, $this->idChuyenVien, $typeNotify38, $timeAlert38, $message38, $created_date);
        }
        
        // 3. Gdkv
        $this->notifyMapData($aRowInsert, $idGdkv, $typeNotify3, $timeAlert3, $message3, $created_date);
        $this->notifyMapData($aRowInsert, $idGdkv, $typeNotify3Gdkv, $timeAlert3Gdkv, $message3Gdkv, $created_date);
        $this->notifyMapData($aRowInsert, $idGdkv, $typeNotify5, $timeAlert5, $message5, $created_date);
        $this->notifyMapData($aRowInsert, $idGdkv, $typeNotify8, $timeAlert8, $message8, $created_date);
        $this->notifyMapData($aRowInsert, $idGdkv, $typeNotify15, $timeAlert15, $message15, $created_date);
        $this->notifyMapData($aRowInsert, $idGdkv, $typeNotify18, $timeAlert18, $message18, $created_date);
        $this->notifyMapData($aRowInsert, $idGdkv, $typeNotify28, $timeAlert28, $message28, $created_date);
        $this->notifyMapData($aRowInsert, $idGdkv, $typeNotify38, $timeAlert38, $message38, $created_date);
        
        $mScheduleNotify = new GasScheduleNotify();
        $mScheduleNotify->runOneQuery($aRowInsert);
    }
    
    /** @Author: DungNT May 08, 2019
     *  @Todo: build array data insert 
     **/
    public function notifyMapData(&$aRowInsert, $uid, $typeNotify, $timeAlert, $message, $created_date) {
        $platform = $apns_device_token = $gcm_device_token = '';
        $message = MyFormat::escapeValues($message);
        $aRowInsert[] = "(
                '$uid',
                '$typeNotify',
                '{$this->id}',
                '{$timeAlert}',
                '{$message}',
                '{$platform}',
                '{$apns_device_token}',
                '{$gcm_device_token}',
                '{$created_date}'
            )";
    }
    
    /** @Author: DungNT May 08, 2019
     *  @Todo: delete notfiy alert + compelte hgd
     **/
    public function deleteNotify($keepNotifyComplete = true) {
        $mScheduleNotify = new GasScheduleNotify();
        $mScheduleNotify->deleteTypeAlertHgd($this->id, $keepNotifyComplete);// DungNT May819 fix move to function model GasScheduleNotify
    }
    
    /** @Author: DungNT Sep 28, 2018
     *  @Todo: get list uid get all notify system
     **/
    public function getArrayUidMonitorAll() {
        return [
//            GasConst::UID_NGOC_PT => GasConst::UID_NGOC_PT,
//            GasLeave::UID_DIRECTOR => GasLeave::UID_DIRECTOR, // Close Feb1919 do bi tinh lỗi 15' 2 lần
        ];
    }
    
    /** @Author: DungNT Sep 11, 2018
     *  @Todo: get list user id will monitor order
     **/
    public function getListUidMonitor() {
        $aUidMonitor = $this->getArrayUidMonitorAll();// Aug0918 mở để ngọc theo dõi đơn hộ GĐ chưa confirm
        $this->getUidGdkv($aUidMonitor);
        return $aUidMonitor;
    }
    
    /** @Author: DungNT Sep 30, 2018
     *  @Todo: get only id kế toán khu vực giám sát ĐL
     **/
    public function getOnlyIdKeToanKv() {
        $mAppCache = new AppCache();
        $listdataAgent = $mAppCache->getListdataKtkvAgent();
        return isset($listdataAgent[$this->agent_id]) ? $listdataAgent[$this->agent_id] : 0;
    }
    /** @Author: DungNT Sep 30, 2018
     *  @Todo: get only id chuyên viên giám sát ĐL
     **/
    public function getOnlyIdChuyenVien() {
        $mAppCache = new AppCache();
        $listdataCvAgent    = $mAppCache->getListdataCvAgent();
        $this->idChuyenVien = isset($listdataCvAgent[$this->agent_id]) ? $listdataCvAgent[$this->agent_id] : 0;
        return $this->idChuyenVien;
    }
    /** @Author: DungNT May 15, 2019
     *  @Todo: get list agent of Chuyen Vien from cache file
     **/
    public function getArrayAgentOfChuyenVien() {
        $mAppCache          = new AppCache();
        $listdataCvAgent    = $mAppCache->getListdataCvAgent();
        $aRes = [];
        foreach($listdataCvAgent as $agent_id => $user_id):
            if($user_id == $this->idChuyenVien){
                $aRes[$agent_id] = $agent_id;
            }
        endforeach;
        return $aRes;
    }
    
    
    public function getOnlyIdGdkv() {// get id giám đốc khu vực - gửi cho 4 GĐ KV
//        if(in_array($this->agent_id, UsersExtend::getAgentAnhHieu())){
        if(in_array($this->province_id, GasOrders::$TARGET_ZONE_PROVINCE_MIEN_TAY)){
            $res = GasLeave::UID_HEAD_OF_LEGAL;
        }elseif($this->province_id != GasProvince::TINH_KHANH_HOA 
                && (in_array($this->province_id, GasOrders::$TARGET_ZONE_PROVINCE_MIEN_TRUNG))
        ){
//            $res = GasConst::UID_PHUONG_ND; May1919 Tu thang 5 anh Phuong khong nhan khoan dai ly nua, nen khong tinh loi
            $res = GasLeave::UID_DIRECTOR;
        }elseif($this->province_id == GasProvince::TINH_BINH_DUONG){
//            $res = GasLeave::PHUC_HV;
            $res = GasLeave::UID_DIRECTOR;
        }else{// còn lại là tổng GĐ
            $res = GasLeave::UID_DIRECTOR;
        }
        return $res;
    }
    
    /** @Author: DungNT Jul 14, 2019
     *  @Todo: get define id gdkv
     **/
    public function getOnlyIdGdkvAllList() {
        return [
            GasLeave::UID_DIRECTOR,
            GasLeave::UID_HEAD_OF_LEGAL
        ];
    }
    
    /** @Author: DungNT Sep 11, 2018
     *  @Todo: get uid GDKV by province_id
     *  @note: dùng chung cho model GasScheduleNotify->saveErrorsHgdSms()
     **/
    public function getUidGdkv(&$aUidMonitor) {
        $idChuyenVien       = $this->getOnlyIdChuyenVien();
        if(!empty($idChuyenVien)){
            $aUidMonitor[$idChuyenVien]   = $idChuyenVien;
        }
        $idGdkv                 = $this->getOnlyIdGdkv();
        $aUidMonitor[$idGdkv]   = $idGdkv;
        $this->aIdUser          = $aUidMonitor;
        unset($this->aIdUser[GasConst::UID_NGOC_PT]);
        unset($this->aIdUser[GasLeave::UID_DIRECTOR]);
    }
 
    /** @Author: DungNT Jul 12, 2017
     * @Todo: get record to monitor Order
     */
    public function getDataMonitor($needMore = []) {
        $criteria = new CDbCriteria();
        if(!empty($this->agent_id)){
            $criteria->addCondition('t.agent_id='.$this->agent_id);
        }
        if(!empty($this->employee_accounting_id)){
            $criteria->addCondition('t.employee_accounting_id='.$this->employee_accounting_id);
        }
        $sParamsIn = implode(',', UsersExtend::getAgentNotRunApp());
        $criteria->addCondition("t.agent_id NOT IN ($sParamsIn)");
        $criteria->addCondition("t.status IN ($this->status)");
        $criteria->addCondition("t.created_date_only='$this->created_date_only'");
        $criteria->order = 't.agent_id ASC, t.id ASC';
        if(isset($needMore['order'])){
            $criteria->order = $needMore['order'];
        }
//        $criteria->limit = 15;
        return self::model()->findAll($criteria);
    }
    
    /** @Author: DungNT Jul 21, 2017
     * @Todo: chuyển đại lý đơn hàng hộ GĐ
     */
    public function changeAgentOrder($q) {
        /* 1. change agent_id at TransactionHistory
         * 2. change agent_id at Sell + Sell Detail
         * 3. make notify for new agent
         * 4. delete old notify alert + complete order
         * 5. Make new notify alert + complete
         * 6. write event log
         */
//        if($q->action_type != self::EMPLOYEE_CHANGE_AGENT){
//            return ;
//        }
        if(!isset($q->change_to_agent) || empty($q->change_to_agent) || $q->change_to_agent == $this->agent_id){
            return false;
//            throw new Exception('Đại lý chuyển không hợp lệ');// Open với check bên trên action_type != self::EMPLOYEE_CHANGE_AGENT
        }
        $mAppCache = new AppCache();
        $aAgent = $mAppCache->getAgent();
        $this->province_id = isset($aAgent[$q->change_to_agent]) ? $aAgent[$q->change_to_agent]['province_id'] : $this->province_id;
        
        // 1. change agent_id at TransactionHistory
        $this->agent_id             = $q->change_to_agent;
        $this->action_type          = Transaction::EMPLOYEE_FREE;
        $this->employee_maintain_id = 0;
        $this->status               = Transaction::STATUS_NEW;
        $this->update(['agent_id', 'province_id', 'action_type', 'employee_maintain_id', 'status']);
        
        // 2. change agent_id at Sell + Sell Detail
        $mSell = $this->rSell;
        if($mSell){
            $mSell->agent_id                = $this->agent_id;
            $mSell->province_id             = $this->province_id;
            $mSell->employee_maintain_id    = 0;
            $mSell->update(['agent_id', 'province_id', 'employee_maintain_id']);

            $criteria = new CDbCriteria();
            $criteria->addCondition('sell_id='.$mSell->id);
            $aUpdate = ['agent_id' => $mSell->agent_id, 'province_id' => $mSell->province_id,'employee_maintain_id' => $mSell->employee_maintain_id];
            SellDetail::model()->updateAll($aUpdate, $criteria);
        }
        // 3. make notify for new agent
        $aUid   = $this->getListEmployeeMaintainOfAgent();
        $this->notifyPhoneAppEmployee($aUid, '');
        // 4. delete old notify alert + complete order
        $this->deleteNotify(false);
        // 5. Make new notify alert + complete
        $this->notifyOrderNotConfirm();
        // 6. write event log change agent
        $location = $q->latitude.",$q->longitude";
        $this->saveEvent($this->mAppUserLogin->id, Transaction::EMPLOYEE_CHANGE_AGENT, 0, 0, $location);
        return true;
    }
    
    /** @Author: DungNT Jul 28, 2017
     * @Todo: get note at App list 
     */
    public function getAppNoteAtList() {
        $res = $this->note;
        if(in_array($this->status, $this->getArrayStatusCancel()) ){
            $res = 'Hủy: '.$this->getStatusCancelText() .'. '. $res;
        }
        return $res;
    }
    
    /** @Author: DungNT Aug 13, 2017
    * @Todo: get url update note at monitor HGD
    */
    public function getUrlMonitorNote($needMore = []) {
        $cRole = MyFormat::getCurrentRoleId();
        $aRoleAllow = [ROLE_ADMIN, ROLE_CALL_CENTER, ROLE_DIEU_PHOI];
        if( !in_array($cRole, $aRoleAllow) ){
            return '';
        }
        $ItemHide = isset($needMore['ItemHide']) ? $needMore['ItemHide'] : '';
        
        $url    = Yii::app()->createAbsoluteUrl('admin/transaction/index', ['MonitorNote'=>1, 'transaction_history_id' => $this->id]);
        $urlHideRow   = Yii::app()->createAbsoluteUrl('admin/transaction/index', ['HideNote'=>1, 'transaction_history_id' => $this->id]);
        $urlShowRow   = Yii::app()->createAbsoluteUrl('admin/transaction/index', ['HideNote'=>2, 'transaction_history_id' => $this->id]);
        $input  = '<input class="MonitorNoteInput '.$ItemHide.'" type="text">';
        
        $showRow = $hideRow = '';
        if($ItemHide == 'ItemHide'){
            $showRow = "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b><a class='ShowNote' href='javascript:;' next='$urlShowRow'>Hiện đơn hàng</a></b>";
        }else{
            $hideRow = "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b><a class='HideNote' href='javascript:;' next='$urlHideRow'>Ẩn đơn hàng</a></b>";
        }
        
        return "<br><div class='WrapMonitorNote item_c'>$input<br><b><a class='MonitorNote' href='javascript:;' next='$url'>Add Note</a></b>$hideRow $showRow</div>";
    }
    
    /** @Author: DungNT Aug 19, 2017
     * @Todo: đổi change agent của giao nhận
     * 1. handleChangeAgent
     * 2. save log tracking user change agent
     */
    public function changeAgentEmployee() {
        if(empty($this->employee_maintain_id) || empty($this->agent_id)){
            return false;
        }
        $mUser      = Users::model()->findByPk($this->employee_maintain_id);
        $oldAgent   = $mUser->getAgentOfEmployee();
        $mAppCache  = new AppCache();
        $aAgent     = $mAppCache->getAgentListdata();

        // 1. set trong field json của model User
        $mUser->setListAgentOfUser($this->agent_id, false);

        // 2. xử lý xóa trong GasOneMany, xóa hết những record gắn NV vào agent trên hệ thống
        GasOneMany::deleteManyByType([$mUser->id], ONE_AGENT_MAINTAIN);
        
        // 3. xử lý save lại vào table GasOneMany
//        $_POST['GasOneMany']['many_id'][] = $mUser->id; --> sai, vì nó xóa hết NVGN của đại lý đi
//        GasOneMany::saveArrOfManyId($this->agent_id, ONE_AGENT_MAINTAIN); --> sai
        GasOneMany::saveOneRecord($this->agent_id, $mUser->id, ONE_AGENT_MAINTAIN);

        // 4. set vào cache
        $mAppCache->setUserMaintainOfAgent($this->agent_id);
        
        // 5. save log tracking user change agent
        $cName = Yii::app()->user->first_name;
        $sInfo = date('d/m/Y H:i:s').' '.$cName. ': chuyển từ '. (isset($aAgent[$oldAgent]) ? $aAgent[$oldAgent] : '').' sang '.$aAgent[$this->agent_id];
        $cUid = MyFormat::getCurrentUid();
        // note lưu agent_id là employee_maintain_id
        MonitorUpdate::makeRecordParams($this->employee_maintain_id, MonitorUpdate::TYPE_CHANGE_AGENT_USER, $cUid, 0, 0, $sInfo);
        return true;
    }
    
    /** @Author: DungNT Jul 09, 2019
     * @Todo: change agent - Nơi công tác của KTKV+CV
     * 1. handleChangeAgent
     * 2. save log tracking user change agent
     */
    public function changeAgentKtkv() {
        if(empty($this->employee_maintain_id) || empty($this->agent_id)){
            return false;
        }
        $mUser      = Users::model()->findByPk($this->employee_maintain_id);
        $oldAgent   = $mUser->getAgentOfEmployee();
        $mAppCache  = new AppCache();
        $aAgent     = $mAppCache->getAgentListdata();

        // 1. set trong field parent_id của model User
        $mUser->parent_id = $this->agent_id;
        $mUser->update(['parent_id']);
        // 2. save log tracking user change agent
        $cName = Yii::app()->user->first_name;
        $sInfo = date('d/m/Y H:i:s').' '.$cName. ': chuyển từ '. (isset($aAgent[$oldAgent]) ? $aAgent[$oldAgent] : '').' sang '.$aAgent[$this->agent_id];
        $cUid = MyFormat::getCurrentUid();
        // note lưu agent_id là employee_maintain_id
        MonitorUpdate::makeRecordParams($this->employee_maintain_id, MonitorUpdate::TYPE_CHANGE_AGENT_USER, $cUid, 0, 0, $sInfo);
        return true;
    }

    /**
     * @Author: Trung Sept 19, 2017
     * @Todo: get user's transaction if it's completed
     */
    public static function getByCompleteTransactionId($q, $customer_id)
    {
        $criteria = new CDbCriteria();
        $criteria->compare('t.id', $q->transaction_id);
        $criteria->compare('t.customer_id', $customer_id);
        return self::model()->find($criteria);
    }

    /**
     * @Author: Trung Sept 19, 2017
     * @Todo: get last pending user's transaction. It's only new transaction
     */
    public static function getLastPendingTransaction($customer_id)
    {
        $mTransaction = new Transaction();
        $criteria = new CDbCriteria;
        $criteria->addCondition('t.customer_id=' . $customer_id);
        $sParamsIn = $mTransaction->getAppStatusNew();
        $sParamsIn = implode(',', $sParamsIn);
        $criteria->addCondition("t.status IN ($sParamsIn)");
        $criteria->order = 't.id DESC';
        $criteria->limit = 1;
        return self::model()->find($criteria);
    }

    /**
     * @Author: Trung Sept 20, 2017
     * @Todo: Save cancel order by user
     */
    public function saveCancelOrderByAppCustomer()
    {
        $q = new stdClass();
        $q->action_type     = Transaction::CUSTOMER_DROP;
        $q->status_cancel   = Transaction::CANCEL_BY_APP_CUSTOMER;
        $this->doDrop($q);
        $this->makeNotifyConfirm();
    }
    /** @Author: DungNT Sep 22, 2017
     * @Todo: 23h every day set empty agent của CCS PTTT - run by cron
     */
    public function setEmptyAgentEmployee() {
        $from = time();
        $criteria = new CDbCriteria();
        $criteria->addCondition('t.role_id=' . ROLE_EMPLOYEE_MARKET_DEVELOPMENT );
        $criteria->addCondition('t.status=' . STATUS_ACTIVE );
        $models = Users::model()->findAll($criteria);
        foreach($models as $mUser){
            Users::UpdateKeyInfo($mUser, UsersExtend::JSON_ARRAY_AGENT, []);
            // 2. xử lý xóa trong GasOneMany, xóa hết những record gắn NV vào agent trên hệ thống
            GasOneMany::deleteManyByType([$mUser->id], ONE_AGENT_MAINTAIN);
        }
        $to = time();
        $second = $to-$from;
        $info = "Cron setEmptyAgentEmployee Qty CCS: ".count($models).' done in: '.($second).'  Second  <=> '.($second/60).' Minutes';
        Logger::WriteLog($info);
    }
    
    /** @Author: DungNT Now 10, 2017
     *  @Todo: trường hợp KH đặt App lần 2 thì đẩy luôn xuống cho giao nhận gọi điện
     **/
    public function handleOrderSecond($q) {
//        $this->alertOrderGas24h();// notify giám sát
//        $this->makeNotify(); close on Aug1818
//        return ;// Dec1017 fix luôn gửi Order lên tổng đài, vì giao nhận chưa thể gọi lại KH luôn đc
        
        $mTransactionHistoryOld = $this->getLastestHistory();
        if(empty($mTransactionHistoryOld)){// nếu chưa có đơn hàng nào thì sẽ lên CallCenter
            $this->makeNotify();// chỉ notify cho Tổng đài Đơn hàng đầu tiên
            return ;
        }
        $cHours = date('H')*1;
//        [TASKTIMER]
        if($this->is_timer == Forecast::TYPE_TIMER){
            $this->makeNotify();
            return; // Báo lên tổng đài nếu là hoá đơn hẹn giờ.
        }
            
        $this->agent_id                 = $mTransactionHistoryOld->agent_id; // Aug2118 lấy agent đơn cũ
        // Aug1818 mở send order Second xuống luôn PVKH, chưa đến 7h sáng thì vẫn lên tổng đài
        if($cHours < 7 || !in_array($this->agent_id, $this->getAgentPutOrderPvkh())){
            $this->makeNotify();// nếu có đơn hàng rồi, mà ko nằm trong list gửi Order cho PVKH thì vẫn lên tổng đài
            return ;
        }
        $this->setAddress();// Fix Jan719 phải update lại address không thì hệ thống tự lấy address của google đưa xuống cho PVKH
        $this->employee_accounting_id   = GasConst::UID_ADMIN; // add Sep3018 trc khi xử lý set random NV tổng đài nhận đơn hàng app lần 1, ko để tự bấm nhận
        $this->province_id              = MyFormat::convertAgentToProvinceId($this->agent_id);
        if($this->agent_id == GasConst::UID_AGENT_XE_TAI_1){// nếu vào ĐL xe tải thì chuyển cho BT2
            $this->agent_id = GasConst::UID_AGENT_BINH_THANH_2;// BT2
        }
        if($this->agent_id == 103){// nếu vào Đại lý Quận 8.1 thì chuyển cho Đại Lý Quận 8.1 (Mới)
            $this->agent_id = 2213999;// Oct1119 DungNT Đại Lý Quận 8.1 (Mới)
        }

        $this->update(['address', 'agent_id', 'province_id', 'employee_accounting_id']);
        $this->copyToSell($q);// Đơn hàng lần 2 auto create Sell để xuống app giao nhận
    }
    
    
    /** @Author: DungNT Oct 17, 2017
     *  @Todo: Copy model TransactionHistory to model Sell 
     * @note: đơn hàng App Gas24h trường hợp KH đặt lần 2 thì đẩy luôn xuống cho giao nhận gọi điện
     */
    public function copyToSell($q) {
        $mSell          = new Sell();
        $aFieldNotCopy  = array('id', 'created_date');
        // handle add promotion of user
        MyFormat::copyFromToTable($this, $mSell, $aFieldNotCopy);
        $mSell->transaction_history_id      = $this->id;
        $mSell->uid_login                   = $this->employee_accounting_id; // GasConst::UID_ADMIN;// để tạm là admin, không biết nên để NV nào
        $mSell->status                      = Sell::STATUS_NEW;
        $mSell->save();
        if(empty($mSell->id)){
            Logger::WriteLog('App Gas24h lỗi khi tạo đơn hàng');
            throw new Exception('Không thể tạo đơn hàng vui lòng thử lại');
        }
        $this->sell_id      = $mSell->id;
        $this->code_no_sell = $mSell->code_no;
        $this->update();
        // save sell detail
        $mAppCache      = new AppCache();
        $aModelGas12    = $mAppCache->getModelGas12Kg();
        $aDataCache     = $mAppCache->getPriceHgd(AppCache::PRICE_HGD);
        $priceHgd       = MyFunctionCustom::getHgdPriceSetup($aDataCache, $this->agent_id);
        $haveVo = $haveGas = true;// customer nên ko check cờ này
        $mSell->isCustomerBookOrder = true;
        $mSell->mapTransactionDetail($q, $priceHgd, $aModelGas12, $haveVo, $haveGas);
        $mSell->runUpdateStatusTransaction = false;
        $mSell->webSaveDetail();
        
        $aUid   = $this->getListEmployeeMaintainOfAgent();
        $this->notifyPhoneAppEmployee($aUid, '');
        $this->notifyOrderNotConfirm();
    }
    
    /** @Author: DungNT Oct 20, 2017
     *  @Todo: giới hạn mỗi người chỉ đc book 1 đơn hàng
     */
    public function countOrderBycustomer() {
        $criteria = new CDbCriteria();
        $criteria->addCondition('t.customer_id=' . $this->customer_id);
        return self::model()->count($criteria);
    }
    
    public function getEmployeeAccounting() {
        $mUser = $this->rEmployeeAccounting;
        if($mUser){
            return $mUser->first_name;
        }
        return '';
    }
    public function getTextInfoMap() {
        return '<b>'.$this->getFirstName().'</b><br>ĐT: '.$this->getPhone().'<br>Đ/C: '.$this->getAddress();
    }
    
    /** @Author: DungNT Jan 07, 2019
     *  @Todo: set new address of customer
     **/
    public function setAddress() {
        if($this->rCustomer){
            $this->address = $this->rCustomer->address;
        }
    }
    
    /** @Author: DungNT Now 10, 2017
     *  @Todo: set user CallCenter click tạo bán hàng cho đặt app
     **/
    public function setUserCallCenter() {
        $cRole = MyFormat::getCurrentRoleId();
        if(!empty($this->employee_accounting_id) || $cRole == ROLE_ADMIN){
            return ;
        }
        $this->employee_accounting_id   = MyFormat::getCurrentUid();
        $this->status                   = Transaction::STATUS_PROCESSING;// Add Jul2519 Trạng thái khi KTKV nhận app, để check 1 user ở 1 thời điểm chi cho nhận 1 app
        $this->update(['employee_accounting_id', 'status']);

//        $totalAppCatch                  = $this->countAppCatch();
//        if($totalAppCatch <= 1){
//        }else{
//            throw new Exception('Bạn chỉ nhận được 1 app xử lý trong 1 thời điểm, vui lòng tạo xong đơn hàng và nhập app mới', SpjError::SELL001);
//        }
    }
    public function setEmptyUserCallCenter() {
        $this->employee_accounting_id = 0;
        $this->update(['employee_accounting_id']);
    }
    
    /** @Author: DungNT Jan 07, 2019
     *  @Todo: check ko cho user nhan don hang khi co nguoi khac dang tao don roi
     **/
    public function checkUserCallCenter() {
        $cRole = MyFormat::getCurrentRoleId();
        if(empty($this->employee_accounting_id) || $cRole == ROLE_ADMIN){
            return ;
        }
        $cUid = MyFormat::getCurrentUid();
        if($this->employee_accounting_id != $cUid){
            throw new Exception($this->getEmployeeAccounting().' đang tạo đơn hàng này, vui lòng kiểm tra lại', SpjError::SELL001);
        }
    }
    
    /** @Author: DungNT Jul 24, 2019
     *  @Todo: checkUserOverQuotaApp check khong cho 1 user nhan 2 don app 1 luc
     **/
    public function totalAppCatch() {
        $cRole  = MyFormat::getCurrentRoleId();
        $cUid   = MyFormat::getCurrentUid();
        if(empty($this->employee_accounting_id) || $cRole != ROLE_CALL_CENTER){
            return ;
        }
        $today = date('Y-m-d');
        $criteria = new CDbCriteria();
        $criteria->addCondition("t.id <> $this->id");
        $criteria->compare('t.employee_accounting_id', $cUid);
        $criteria->compare('t.status', Transaction::STATUS_PROCESSING);
        DateHelper::searchBetween($today, $today, 'created_date_bigint', $criteria, true);
        return $totalAppCatch = TransactionHistory::model()->count($criteria);
    }
    
    /** @Author: DungNT Now 26, 2017
     *  @Todo: tập trung vào 1 request rollback promotion của user
     *  Thời gian đầu sẽ cho rollback promotin khi hủy đơn, về sau sẽ tắt chế độ rollback này đi -> nên phải tách riêng function để xử lý tập trung
     * 1. xử lý CallCenter hủy đơn khi chưa tạo Sell (đơn hàng cho giao nhận) -- done
     * 2. xử lý KH hủy trên app -- done
     * 3. xử lý khi giao nhận hủy trên app -- done
     * 4. xử lý khi Audit hủy đơn Sell trên web -- done
     **/
    public function rollbackPromotionByHand() {
        if(empty($this->app_promotion_user_id)){
            return ;
        }
        $mPromotionUser = new AppPromotionUser();
        $mPromotionUser->mTransactionHistory = $this;
        $mPromotionUser->rollbackApply();
    }
    
    /**
     * @Author: DungNT Nov 30, 2017
     * @Todo: cron delete những Order không hoàn thành nếu tạo từ 1h trc mà chưa tạo đc đơn hàng
     */
    public function cronDeleteDraft() {
        $mTrans = new Transaction();
        $mTrans->cronDeleteDraft();
        
//        return ;// Dec1317 để lại, hiện tại sẽ không xóa
        // Jan0818 xóa những đơn hàng đc tạo từ 10 trước mà không có tạo Sell
//        $cHours = date('H');
//        if($cHours < 6) {
//            $hours = 2;
//        }
        $today      = date('Y-m-d');
        $date_from  = MyFormat::modifyDays($today, 40, '-');
        $date_to    = MyFormat::modifyDays($today, 30, '-');
        
        $criteria = new CDbCriteria();
        DateHelper::searchBetween($date_from, $date_to, 'created_date_only_bigint', $criteria);
        $criteria->addCondition('source='.Sell::SOURCE_APP);
        $criteria->addCondition('sell_id=0');
        $models = TransactionHistory::model()->findAll($criteria);
        foreach($models as $model){
// Dec0517 chỗ rollbackApply này có thể gây bug khi user đặt và hủy nhiều lần, mà những lần đó đều có KM, lần cuối user đặt thành công, KM của những lần hủy bị rollack => sai
//            AppPromotionUser::rollbackApply($model->app_promotion_user_id);
            $model->delete();
        }
    }
    
    /** @Author: DungNT Dec 13, 2017
     *  @Todo: tự động tạo đơn trả thẻ
     **/
    public function autoCreateOrder() {
        $this->mTransaction = new Transaction('AutoCreateOrder');
        $mTransactionHistory = $this->autoCreateOrderMapCustomer();
        $mTransactionHistory->autoCreateOrderCopyToSell();
        return $mTransactionHistory;
    }
    public function autoCreateOrderMapCustomer() {
//        $mCustomer = Users::model()->findByPk(GasConst::HGD_TRA_THE);
        $mCustomer = Users::model()->findByPk(869974);
        $this->mTransaction->customer_id = $mCustomer->id;
        $q = new stdClass();
        $q->transaction_type        = Transaction::STEP_NORMAL;
        $q->first_name              = $mCustomer->first_name;
        $q->phone                   = '';
        $q->device_phone            = '';
        $q->note                    = '';
        $q->google_address          = '';
        $q->email                   = '';
        $q->agent_id                = $this->agent_id;
        $q->google_map              = '';
        $q->latitude                = '';
        $q->longitude               = '';
        
        $this->employee_maintain_id         = $this->mAppUserLogin->id;
        $this->employee_accounting_id       = GasConst::UID_ADMIN;
        $this->action_type                  = Transaction::EMPLOYEE_NHAN_GIAO_HANG;
        $this->status                       = Transaction::STATUS_DELIVERY_PROCESSING;
        $this->mTransaction->mTransactionHistory    = $this;// xử lý đẩy biến sang hàm copyToHistory
        
        $this->mTransaction->handlePost($q);
        $this->mTransaction->json_detail    = json_encode([]);
        $this->mTransaction->address        = $mCustomer->address;
        $this->mTransaction->status         = Transaction::STATUS_COMPLETE;
        $this->mTransaction->type_customer  = $mCustomer->is_maintain;
        $this->mTransaction->total = $this->mTransaction->grand_total = 0;
        
        $this->mTransaction->created_date = date('Y-m-d H:i:s');
        $this->mTransaction->save();
        $this->mTransaction->scenario = 'AutoCreateOrder';
        $mTransactionHistory = $this->mTransaction->copyToHistory();
        $this->mTransaction->delete();
        return $mTransactionHistory;
    }
    
    /** @Author: DungNT Dec 13, 2017
     *  @Todo: copy to Sell
     **/
    public function autoCreateOrderCopyToSell() {
        $mSell          = new Sell();
        $aFieldNotCopy  = array('id', 'created_date');
        // handle add promotion of user
        MyFormat::copyFromToTable($this, $mSell, $aFieldNotCopy);
        $mSell->transaction_history_id      = $this->id;
        $mSell->uid_login                   = $this->employee_accounting_id;
        $mSell->status                      = Sell::STATUS_NEW;
        $mSell->save();
        if(empty($mSell->id)){
            Logger::WriteLog('App Gas24h lỗi khi tạo đơn hàng');
            throw new Exception('Không thể tạo đơn hàng vui lòng thử lại');
        }
        $this->sell_id      = $mSell->id;
        $this->code_no_sell = $mSell->code_no;
        $this->update();
        // save sell detail
//        $mSell->webSaveDetail();
    }
    
    /** @Author: DungNT Mar 19, 2018
     *  @Todo: kiểm tra thời gian có bị vượt so với 1 khoảng thời gian check không
     * @dateTimeCheck: ngày giờ kiểm tra, format Y-m-d H:i:s
     * @minutesPlus: số phút cộng vào kiểm tra so với time hiện tại 
     * @return: true nếu bị over time, else return false
     **/
    public function isOverTime($dateTimeCheck, $minutesPlus, $now='') {
        $isOver = true;
        if(empty($now)){
            $now = date('Y-m-d H:i:s');
        }
        // Apr2319 xử lý bỏ lỗi khi hoàn thanh sau 24h 
        $minutesPass = MyFormat::getMinuteTwoDate($dateTimeCheck, date('Y-m-d H:i:s'));
        if($minutesPass > 1440){ // 1440 min = 24h
            return false;// ko tính lỗi
        }
        
        $dateTimePlus  = MyFormat::addDays($dateTimeCheck, $minutesPlus, '+', 'minutes', 'Y-m-d H:i:s');
        if(MyFormat::compareTwoDate($dateTimePlus, $now)){
            $isOver = false;
        }
        return $isOver;
    }
    
    /** @Author: DungNT Mar 10, 2018
     *  @Todo: check lỗi hoàn thành HGD 60 phút
     **/
    public function checkErrorComplete($mSell) {
        $timeLimit = MonitorUpdate::TIME_COMPLETE_HGD;
        if($this->source == Sell::SOURCE_APP){
            $timeLimit = MonitorUpdate::TIME_COMPLETE_HGD_APP;
        }
        $datetimeCheck = $mSell->created_date;
        if(!empty($mSell->delivery_timer) && $mSell->delivery_timer != '0000-00-00 00:00:00'){
            $datetimeCheck = $mSell->delivery_timer;
        }
        
        if(!$this->isOverTime($datetimeCheck, $timeLimit)){
            return ;
        }
        /*  vượt thời gian 60 phút -> tính lỗi
         * 1. save table Errors MonitorUpdate
         * 2. make notify to pvkh
         */
        $mMonitorUpdate = new MonitorUpdate();
        $mMonitorUpdate->saveAllCaseError(MonitorUpdate::TYPE_ERROR_HGD_COMPLETE, $this->employee_maintain_id, $this->id);
        
        $aUid   = [$this->employee_maintain_id];
        $title  = $this->code_no_sell. "Bạn bị lỗi hoàn thành ĐH HGĐ quá {$timeLimit} phút";
        $this->notifyPhoneAppEmployee($aUid, $title);
        $sendEmail = new SendEmail();
        $sendEmail->alertCompleteOrderLate($this);
    }
    
    /** @Author: DungNT Oct 02, 2018
     *  @Todo: kiểm tra lỗi hoàn thành trễ
     *  @1-App: - 20' < HT < 30' phạt Chuyên viên 10K  + GDKV 10K
     *           30 phút < HT < 40 phút phạt GĐKV 10k
                - HT > 40 phút phạt PVKH 5k
     *  @2-Call: - 40 < HT < 60 phạt CV 10K , GĐKV 10K 
     *          - > 60 phút phạt PVKH 20k
     **/
    public function doFlowCompleteLate($mSell) {
        try{
        $timeComplete = $mSell->getTimeOverOnlyNumber();
        if( ($this->source == Sell::SOURCE_APP && $timeComplete <= GasConst::TIME_20_MIN) ||
            ($this->source != Sell::SOURCE_APP && $timeComplete <= GasConst::TIME_40_MIN) 
        ){
            return ;// Đơn App < 20', đơn Call < 40' ko tính lỗi
        }
        /* 1. 20' < HT < 30' Đơn app xác định chuyên viên 
         * 2.  > 30 phút phạt GDKV 10k
         * 3. 40 < HT < 60 phạt PVKH 5k, CV 10K , GĐKV 10K 
         * 4. > 60 phút phạt PVKH 20k
         * 5. làm 1  hàm truyền vào user id + số tiền phạt + số phút hoàn thành gắn trong nội dung SMS
         */
        $aPvkh5k            = []; $textApp = '';
        $idGdkv             = $this->getOnlyIdGdkv();
        $idChuyenVien       = $this->getOnlyIdChuyenVien();
        $idChuyenVien       = !empty($idChuyenVien) ? $idChuyenVien : $idGdkv;
        $agentName          = $this->getAgent();
        $mEmployeeProblems  = new EmployeeProblems();
        $mScheduleNotify    = new GasScheduleNotify();
        $mScheduleNotify->type = 0;
        
        // 1.Jun1519 fix ko tinh loi 2 thang NV hoc viec + ko tính lỗi Chuyên Viên + GĐKV
        $mUser = $this->rEmployeeMaintain;
        $mGasTickets = new GasTickets();
        if($mGasTickets->isExceptionPunish($mUser)){
            return ;
        }
        
        if($this->source == Sell::SOURCE_APP){
            $textApp = 'APP';
            if($timeComplete > GasConst::TIME_20_MIN && $timeComplete <= GasConst::TIME_30_MIN){// CV
                $mScheduleNotify->aUidSend[$idChuyenVien] = $idChuyenVien;
                $mEmployeeProblems->object_type = EmployeeProblems::PROBLEM_HGD_COMPLETE_20_MIN;
            }elseif($timeComplete > GasConst::TIME_30_MIN && $timeComplete <= GasConst::TIME_40_MIN){// Gdkv
                $mScheduleNotify->aUidSend[$idGdkv] = $idGdkv;
                $mEmployeeProblems->object_type     = EmployeeProblems::PROBLEM_HGD_COMPLETE_30_MIN;
            }elseif($timeComplete > GasConst::TIME_40_MIN){// PVKH
                $aPvkh5k[$this->employee_maintain_id]       = $this->employee_maintain_id;
            }
        }else{
            if($timeComplete > GasConst::TIME_40_MIN && $timeComplete <= GasConst::TIME_60_MIN){ // phạt PVKH 5k, CV 10K , GĐKV 10K 
                $mScheduleNotify->aUidSend[$idChuyenVien]   = $idChuyenVien;
                $mScheduleNotify->aUidSend[$idGdkv]         = $idGdkv;
                $mEmployeeProblems->object_type = EmployeeProblems::PROBLEM_HGD_COMPLETE_40_MIN;
            }elseif($timeComplete > GasConst::TIME_60_MIN){// Pvkh
                $mScheduleNotify->aUidSend[$this->employee_maintain_id] = $this->employee_maintain_id;
                $mEmployeeProblems->object_type = EmployeeProblems::PROBLEM_HGD_COMPLETE_60_MIN;
            }
        }
        $mEmployeeProblems->money = $mEmployeeProblems->getPrice();
        $mScheduleNotify->mEmployeeProblems = $mEmployeeProblems;
        $mScheduleNotify->title = "[E01 Tinh loi] HT $textApp $timeComplete phut - $agentName: {$this->getCodeNoSell()} - {$this->getFirstName()} - {$this->getAddress()}";
        $mScheduleNotify->saveErrorsHgdSms($this, []);
        if(count($aPvkh5k)){// HT APP > 40 phút phạt PVKH 5k
            $mScheduleNotify->aUidSend      = $aPvkh5k;
            $mEmployeeProblems->object_type = EmployeeProblems::PROBLEM_HGD_COMPLETE_APP_40_MIN;
            $mEmployeeProblems->money       = GasConst::PUNISH_5K;
            $mScheduleNotify->mEmployeeProblems = $mEmployeeProblems;
            $mScheduleNotify->title = "[E01 Tinh loi] HT $textApp $timeComplete phut - $agentName: {$this->getCodeNoSell()} - {$this->getFirstName()} - {$this->getAddress()}";
            $mScheduleNotify->saveErrorsHgdSms($this, []);
        }
        } catch (Exception $ex) {
            throw new Exception($ex->getMessage());
        }
    }
    
    
    /** @Author: DungNT Sep 14, 2018 
     * @Todo: khi NV PVKH hủy đơn thì Put socket notify xuống NV tất cả CallCenter để alert
     */
    public function doDropMakeSocketNotify() {
        $appAdmin = '';
        if($this->employee_accounting_id == GasConst::UID_ADMIN){
            $appAdmin = '[APP ADMIN] ';
        }
        $mCallTemp      = new CallTemp();
        $aUserId        = $mCallTemp->getArrayUserIdOfExt(Call::QUEUE_HGD);
        $message        = "$appAdmin [Hủy đơn: {$this->code_no_sell}] Lý do: {$this->getStatusCancelText()} - NVTĐ: {$this->getEmployeeAccounting()} - PVKH: {$this->mAppUserLogin->first_name} - Đại lý {$this->getAgent()}: ". date('H:i:s');
        $json = $needMore = [];
        // với notify 5 phut sẽ gửi luôn và set need_sync == 0
        $aModelSocket = []; // xử lý gửi 1 lần 1 list sự kiện
        foreach($aUserId as $toUserId){
            $aModelSocket[] = GasSocketNotify::addMessage(0, GasConst::UID_ADMIN, $toUserId, GasSocketNotify::CODE_ALERT_CALL_CENTER, $message, $json, $needMore);
        }
        $mSync = new SyncData(SyncData::SERVER_IO_SOCKET, 'socket/notifyAdd');
        $mSync->notifyAdd($aModelSocket);
    }
    
    /** @Author: DungNT Sep 27, 2018
     *  @Todo: check ext user is callcenter or not
     **/
    public function isExtCallCenter() {
        $cRole = MyFormat::getCurrentRoleId();
        if($cRole == ROLE_ADMIN){
            return true;
        }
        $cExt       = CacheSession::getCookie(CacheSession::CURRENT_USER_EXT);
        $aExtHgd    = $this->getExtHgdOnlineOnlyCallCenter();
        if(!empty($cExt) && array_key_exists($cExt, $aExtHgd)){
            return true;
        }
        return false;
        
    }
    
    /** @Author: NamNH Oct 10, 2018
     *  @Todo: get record timer api
     **/
    public function apiTimerGetRecord(){
        $dateNow            = date('Y-m-d');
        $dateTimer          = MyFormat::dateConverYmdToDmy($this->delivery_timer,'Y-m-d');
        $result             = [];
        $result['id']       = $this->id;
        $result['days']     = $dateTimer > $dateNow ? (int)MyFormat::getNumberOfDayBetweenTwoDate($dateNow,$dateTimer) : 0;
        $result['hour']     = (!empty($this->delivery_timer) && $this->delivery_timer != '0000-00-00' &&  $this->delivery_timer != '0000-00-00 00:00:00') ? (int)MyFormat::dateConverYmdToDmy($this->delivery_timer,'H') : 0;
        $result['date']     = (!empty($this->delivery_timer) && $this->delivery_timer != '0000-00-00' &&  $this->delivery_timer != '0000-00-00 00:00:00') ? MyFormat::dateConverYmdToDmy($this->delivery_timer,'H\hi d/m/Y') : MyFormat::dateConverYmdToDmy($this->created_date,'H\hi d/m/Y');
        return $result;
    }
    
//    [TASKTIMER]
    /** @Author: NamNH Oct 29, 2018
     *  @Todo: Change minutes timer. overwrite
     **/
    public function getTimePass(){
        if($this->is_timer == Forecast::TYPE_TIMER){
            $minutes = 0;
            if(!MyFormat::compareTwoDate($this->delivery_timer, date('Y-m-d H:i:s'))){
                $minutes = MyFormat::getMinuteTwoDate($this->delivery_timer, MyFormat::getDateTimeNow());
            }
            return $minutes;
        }
        return MyFormat::getMinuteTwoDate($this->created_date, MyFormat::getDateTimeNow());
    }
    
    /** @Author: DungNT Dec 18, 2018 
     *  @Todo: get list agent not check confirm order 
     **/
    public function getAgentNotCheckConfirm() {
        return [
//            1048571, // Đại Lý Cà Mau Petro - Phường 8
//            1161320, // Đại Lý Cà Mau Petro - Phường 9
//            1079730, // Đại Lý Bạc Liêu 
            103, // Đại lý Quận 8.1
            104, // Đại lý Quận 8.2
        ];
    }
    
    /** @Author: DungNT Dec 09, 2018
     *  @Todo: check PVKH xác nhận đơn hàng, nếu đl có từ 2 NV thì kiểm tra để số đơn giữa 2 không chênh lệch nhau quá 2 đơn
     *  @return: true is allow confirm, false is not allow confirm this order
     **/
    public function canConfirmMoreOrder($currentUserId) {
        /* 1. đơn hẹn giờ thì luôn cho xác nhận
        /* 1. get tổng số NV của đại lý 
         * 2. get tổng số đơn hàng đang giao của đl, trong ngày hiện tại
         * 3. tìm min của đl, so sánh với số đơn hiện tại của $currentUserId đang confirm
         *  nếu min + 1 < total_of_$currentUserId thì ko cho nhận
         * kiểm tra xem hiện tại ai ít hơn cho nhận đơn mới
         */
        if((!empty($this->delivery_timer) && $this->delivery_timer != '0000-00-00 00:00:00')){
            return true;// Dec1518 allow đơn hẹn giờ thì luôn cho xác nhận
        }
        if(in_array($this->agent_id, $this->getAgentNotCheckConfirm())){
            return true;// Dec1818 allow 1 số agent ko check
        }
        $aUid       = $this->getListEmployeeMaintainOfAgent();// [many_id => many_id]
        $aUidNew    = $aCountSell = []; $mSell = new Sell();
        foreach($aUid as $user_id){
            $mUsersTokens = UsersTokens::getByUserId($user_id);
            if(!empty($mUsersTokens)){// ko tính những user đã off, ko login trên App
                $aUidNew[$user_id] = $user_id;
            }
        }
        
        if(count($aUidNew) < 2){
            return true;// allow nếu ĐL có 1 người
        }
        
        $aCountSell[$currentUserId] = 0;
        foreach($aUidNew as $user_id){
            $aCountSell[$user_id] = 0;
        }
        $aSellConfirm = $mSell->getOrderConfirmOfAgent($this->agent_id, $this->created_date_only);
        foreach($aSellConfirm as $mSell){
            if((!empty($mSell->delivery_timer) && $mSell->delivery_timer != '0000-00-00 00:00:00')){
                continue ;
            }
            if(isset($aCountSell[$mSell->employee_maintain_id])){
                $aCountSell[$mSell->employee_maintain_id] += 1;
            }
        }
        if($aCountSell[$currentUserId] < 2){
            return true;//allow nếu $currentUserId chưa có đơn nào, hoặc đã có 1 đơn
        }
        $minOneUser = min($aCountSell);
        $minOneUser++;
        if($minOneUser < $aCountSell[$currentUserId]){
            throw new Exception("Bạn đan có {$aCountSell[$currentUserId]} đơn hàng chưa hoàn thành. Vui lòng báo nhân viên khác nhận giao đơn hàng");
        }
    }
    
    /** @Author: DungNT Dec 14, 2018
     *  @Todo: get model mới tạo hoặc đang giao của KH, để chặn không cho nhập KM trong thời gian đang giao hàng 
     *  do bị lỗi khi đơn đang giao thì đc phép nhập thêm khuyến mãi, không qua flow check của Dev
     **/
    public function getOrderRunning() {
        $today = date('Y-m-d');
        $mTransaction = new Transaction();
        $criteria = new CDbCriteria();
        $criteria->addCondition('t.customer_id = ' . $this->customer_id);
        $sParamsIn = implode(',',  $mTransaction->getAppStatusNew());
        $criteria->addCondition("t.status IN ($sParamsIn)");
//        DateHelper::searchBetween($today, $today, 'created_date_only_bigint', $criteria);
        return TransactionHistory::model()->find($criteria);
    }
    
}