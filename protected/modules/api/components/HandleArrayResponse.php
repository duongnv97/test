<?php
/**
 * @Author: ANH DUNG Oct 09, 2015
 * @Todo: xử lý các biến sau khi post or GET trả xuống cho app
 */
class HandleArrayResponse
{
    public $aMenu = [], $mUser = null, $mUsersTokens = null;
    /**
     * @Author: ANH DUNG Oct 09, 2015
     */
    public function login($mUsersTokens, $mUser, $objController)  {
        $this->mUser           = $mUser;
        $this->mUsersTokens    = $mUsersTokens;
        $result = ApiModule::$defaultSuccessResponse;
        $result['message']      = 'Login success';
        $result['token']        = $mUsersTokens->token;
        if(in_array($mUsersTokens->type, UsersTokens::$ARR_LOGIN_WINDOW)){
            $this->loginAppWindows($result);
        }else{
            $this->loginAppPhone($result);
        }
        
        $result['record'] = array(
             'first_name'   => $mUser->getFullName(),
             'email'        => $mUser->getEmail(),
         );
        $mReward = new RewardPoint();
        $result['reward_point'] = $mReward->getPointFromUsersInfo($mUser->id);
        ApiModule::sendResponse($result, $objController);
    }
    
    /** @Author: ANH DUNG Dec 02, 2018
     *  @Todo: handle login app ios + android
     **/
    public function loginAppPhone(&$result) {
        if(empty($this->mUser)){
            throw new Exception('User not create on system');
        }
        $mUser = $this->mUser;
        $mAppCache = new AppCache();
        if(UsersTokens::isCustomerGas24h($mUser)){
            $result['gas24h'] = 1;
            $this->getSomeConfigGas24h($result, $mUser);
        }else{
            $result['gas24h'] = 0;
            $this->getSomeConfig($result, $mUser);
            // Feb 13, 2017 trả về các loại gas ở login
            $result['material_gas'] = $mAppCache->getObjMaterialByType(GasMaterialsType::getTypeBoMoi(), AppCache::ARR_MODEL_MATERIAL_GAS, ['GetActive'=>1]);
        }
        $this->getParamGas24h($result, $mUser);
    }
    /** @Author: ANH DUNG Dec 02, 2018
     *  @Todo: handle login app Windows
     **/
    public function loginAppWindows(&$result) {
        $result['auto_print']               = 1;// tự động print khi cân lần 2 xong
        $result['print_company_name']       = 'Chi Nhánh Công Ty TNHH Hướng Minh ';
        $result['print_company_add']        = '20A, Ấp Tân Cang - Xã Phước Tân - Biên Hòa - ĐN';
        $result['print_company_phone']      = '';
        $result['print_company_fax']        = '';
        $result['max_kg_change']                    = 10;
        $result['allow_print']                      = 1;// cho phép bấm nút in hay không
        $result['allow_report']                     = 1;// cho phép xem report hay không
        $result['allow_internal_input']             = 1;// cho phép cân nội bộ nhập số xe
        $result['auto_restart_app_change_info']     = 1;// Tự động tắt chương trình khi thay đổi thông số đầu cân
        $result['config_pass']                      = 'Sj456456';// pass to access config client
        $result['day_keep_images']                  = 180;// số ngày lưu file ảnh trên máy tính
        $result['support_text']                     = 'Công ty CP Dầu Khí Miền Nam. Hỗ trợ IT Dũng 0384 331 552';
    }
    
    
    /**
     * @Author: ANH DUNG Dec 22, 2015
     * cái name sẽ như là value để check ở APP: có 2 value là 0 và 1
     */
    public static function getCheckMenuUser($mUser) {
        $aCheckMenu = array(
            array(
                'id'=>'issue_create',
                'name'=> self::checkIssueCreate($mUser),
            ),
            array(
                'id'=>'uphold_create',
                'name'=> self::checkUpholdCreate($mUser),
            ),
            array(
                'id'=>'uphold_list',
                'name'=> self::checkUpholdList($mUser),
            ),
            array(
                'id'=>'uphold_rating',
                'name'=> self::checkUpholdRating($mUser),
            ),
        );
        return $aCheckMenu;
    }
    
    /**
     * @Author: ANH DUNG Dec 22, 2015
     * check xem user app có được tạo issue không
     */
    public static function checkIssueCreate($mUser) {
        $res = '0';
//        $aRoleNotIn = array(ROLE_CUSTOMER);// view message notify list
        $aRoleNotIn = [];// Apr 12, 2016 cho phép customer tạo phản ánh trên server android.huongminhgroups
        if( !in_array($mUser->role_id, $aRoleNotIn) ){
            $res = '1';
        }
        return $res;
    }
    
    /**
     * @Author: ANH DUNG Dec 31, 2015
     * check xem user app có được tạo uphold không
     */
    public static function checkUpholdCreate($mUser) {
        $res = '0';
//        $aAllow = array(ROLE_DIEU_PHOI, ROLE_CUSTOMER);// view message notify list
        // Apr 20,2 016 không cho điều phối tạo bảo trì
        $aAllow = array(ROLE_CUSTOMER);// view message notify list
        if( in_array($mUser->role_id, $aAllow) ){
            $res = '1';
        }
        return $res;
    }
    
    public static function checkUpholdList($mUser) {// Sep 07, 2016
        $res = '0';
        $aAllow = array(ROLE_E_MAINTAIN, ROLE_CUSTOMER);// view message notify list
        if( in_array($mUser->role_id, $aAllow) ){
            $res = '1';
        }
        return $res;
    }
    public static function checkUpholdRating($mUser) {// Sep 07, 2016
        $res = '0';
        $aAllow = array(ROLE_CUSTOMER);// view message notify list
        if( in_array($mUser->role_id, $aAllow) ){
            $res = '1';
        }
        return $res;
    }
    
    /**
     * @Author: ANH DUNG Now 22, 2016
     */
    public function loginGetMenuGas24h($mUser) {
        $mForecast = new Forecast();

        $this->aMenu[] = array(
            'id'=>'home',
            'name'=>'Home',
        );
        $this->aMenu[] = array(
                'id'=>'order_list',
                'name'=>'Đơn hàng',
            );
        $mReward = new Reward();
        if(!GasCheck::isServerLive() || $mReward->isReward($mUser)){
            $this->aMenu[] = array(
                    'id'=>'reward_list',
                    'name'=>'Ưu đãi',
                );
        }
        
        if($mForecast->canUseForecast($mUser)){
            $this->aMenu[] = array(
                    'id'=>'gas_forecast',
                    'name'=>'Dự báo lượng Gas',
                );
        }
        $this->aMenu[] = array(
                'id'=>'promotion_list',
                'name'=>'Khuyến mãi',
            );
        $this->aMenu[] = array(
                'id'=>'announce_list',
                'name'=>'Thông báo',
            );
        $this->aMenu[] = array(
                'id'=>'map_agent',
                'name'=>'Hệ thống đại lý',
            );
        $this->aMenu[] = array(
                'id'=>'id_page_guide',
                'name'=>'Hướng dẫn sử dụng',
            );
        $this->aMenu[] = array(
                'id'=>'scan_qr_code',
                'name'=>'Quét QR code',
            );
        $this->aMenu[] = array(
                'id'=>'user_profile',
                'name'=>'Tài khoản',
            );
        
        $mSpinNumbers = new SpinNumbers();
        if(!GasCheck::isServerLive() || $mSpinNumbers->canViewXoSoMobile($mUser->username)){
            $this->aMenu[] = array(
                    'id'=>'lottery',
                    'name'=>'Xổ số',
                );
        }
        return $this->aMenu;
    }
    
    
    /**
     * @Author: ANH DUNG Oct 09, 2015 
     * Jul0919 flow add 1 role có chức năng giống PVKH, phải setup agent thì mới dùng đc: vd Nhập xuất kho cho KTKV + Chuyên Viên
     * 
     */
    public function loginGetMenuUser($mUser) {
        $aRoleIssueList = array(ROLE_EMPLOYEE_MAINTAIN, ROLE_E_MAINTAIN, ROLE_DIEU_PHOI, ROLE_HEAD_OF_MAINTAIN, 
            ROLE_SALE, ROLE_HEAD_GAS_BO, ROLE_HEAD_GAS_MOI, ROLE_DIRECTOR_BUSSINESS, ROLE_DIRECTOR, ROLE_CHIEF_MONITOR, ROLE_MONITOR_AGENT,
            ROLE_AUDIT, ROLE_ACCOUNTING, ROLE_ACCOUNTING_ZONE, ROLE_MONITORING,ROLE_MONITORING_MARKET_DEVELOPMENT, ROLE_HEAD_TECHNICAL, ROLE_SALE_ADMIN,
            ROLE_DRIVER, ROLE_EMPLOYEE_MARKET_DEVELOPMENT, ROLE_PHU_XE
        );// Apr 18, 2016
        $aRoleOrderCreate = $aRoleOrderList = [];
        $aRoleUpholdList = array(ROLE_DIRECTOR, ROLE_DIRECTOR_BUSSINESS, ROLE_HEAD_GAS_BO, ROLE_HEAD_GAS_MOI,
            ROLE_E_MAINTAIN, ROLE_DIEU_PHOI, ROLE_CUSTOMER, ROLE_HEAD_OF_MAINTAIN, ROLE_SALE,
            ROLE_MONITORING,ROLE_MONITORING_MARKET_DEVELOPMENT,ROLE_HEAD_TECHNICAL,ROLE_CHIEF_MONITOR, ROLE_MONITOR_AGENT,
            ROLE_EXECUTIVE_OFFICER,ROLE_SALE_ADMIN,
            ROLE_EMPLOYEE_MAINTAIN, ROLE_EMPLOYEE_MARKET_DEVELOPMENT
            );
        $aRoleMessageNotIn          = [ROLE_CUSTOMER];// view message notify list
        $aRoleReportWorkingNotIn    = [ROLE_CUSTOMER, ROLE_EXECUTIVE_OFFICER, ROLE_DRIVER];
        
//        $aRoleCustomerHgdList   = [ROLE_EMPLOYEE_MAINTAIN, ROLE_EMPLOYEE_MARKET_DEVELOPMENT, ROLE_MONITORING_MARKET_DEVELOPMENT, ROLE_EMPLOYEE_MARKET_DEVELOPMENT, ROLE_SALE];// view message notify list
        $aRoleCustomerHgdList   = [ROLE_EMPLOYEE_MARKET_DEVELOPMENT, ROLE_MONITORING_MARKET_DEVELOPMENT, ROLE_EMPLOYEE_MARKET_DEVELOPMENT, ROLE_SALE];// view message notify list
        $aRoleTransactionList   = [ROLE_CHIEF_MONITOR, ROLE_EMPLOYEE_MAINTAIN, ROLE_MONITORING_MARKET_DEVELOPMENT, ROLE_EMPLOYEE_MARKET_DEVELOPMENT];
        $aRoleBoMoiList         = [ROLE_CHIEF_MONITOR, ROLE_EMPLOYEE_MAINTAIN, ROLE_DRIVER, ROLE_CUSTOMER, ROLE_DIEU_PHOI, ROLE_SALE, ROLE_MONITORING_MARKET_DEVELOPMENT, ROLE_SALE_ADMIN, ROLE_EMPLOYEE_MARKET_DEVELOPMENT];
        $aRoleStorecardList     = [ROLE_ACCOUNTING, ROLE_ACCOUNTING_ZONE, ROLE_MONITORING_MARKET_DEVELOPMENT, ROLE_EMPLOYEE_MARKET_DEVELOPMENT, ROLE_EMPLOYEE_MAINTAIN, ROLE_DRIVER];// Sep2517 off ROLE_DRIVER 
        $aRoleCashbookList          = [ROLE_ACCOUNTING, ROLE_ACCOUNTING_ZONE, ROLE_HEAD_GAS_BO, ROLE_CRAFT_WAREHOUSE, ROLE_SALE, ROLE_MONITORING_MARKET_DEVELOPMENT, ROLE_EMPLOYEE_MAINTAIN, ROLE_DRIVER, ROLE_DEBT_COLLECTION, ROLE_EMPLOYEE_MARKET_DEVELOPMENT];
        $aRoleCashbookScheduleList  = [ROLE_SALE, ROLE_MONITORING_MARKET_DEVELOPMENT, ROLE_DEBT_COLLECTION];
        $aRoleTicketList        = [ROLE_E_MAINTAIN, ROLE_CUSTOMER, ROLE_EMPLOYEE_MAINTAIN, ROLE_EMPLOYEE_MARKET_DEVELOPMENT, ROLE_MONITORING_MARKET_DEVELOPMENT, ROLE_DRIVER, ROLE_SALE_ADMIN, ROLE_MONITOR_AGENT];
        $aRoleGasRemain         = [ROLE_CRAFT_WAREHOUSE, ROLE_DRIVER];
//        $aRoleCustomerRequest   = [ROLE_EMPLOYEE_MAINTAIN, ROLE_E_MAINTAIN, ROLE_SALE, ROLE_MONITORING_MARKET_DEVELOPMENT];
        $aRoleManageStt         = [ROLE_DRIVER, ROLE_CRAFT_WAREHOUSE];
        $aRoleTruckPlan         = [ROLE_DRIVER, ROLE_CRAFT_WAREHOUSE];
        
        $mCodePartner   = new CodePartner(); $showMenu = true;
        $userRole       = $mUser->role_id;
        
        if(in_array($mUser->id, $mCodePartner->getUidLimitMenu())){// Dec0118 limit menu với PVKH - ĐLLK
             $showMenu = false;
        }
        
        $this->aMenu[] = array(
            'id'=>'home',
            'name'=>'Home',
        );

        if( in_array($userRole, $aRoleTransactionList) && $showMenu){
            $this->aMenu[] = array(
                'id'=>'transaction_list',
                'name'=>'ĐH HGD',
            );
        }
        if( in_array($userRole, $aRoleBoMoiList) && $showMenu){
            $text = 'ĐH bò mối';
            if($userRole == ROLE_CUSTOMER){
                $text = 'Đơn hàng';
            }
            $this->aMenu[] = array(
                'id'    => 'bomoi_list',
                'name'  => $text,
            );
        }
        
        if( in_array($userRole, $aRoleStorecardList) && $showMenu ){
            $this->aMenu[] = array(
                'id'=>'store_card_list',
                'name'=>'Nhập - Xuất kho',
            );
        }
        if( (in_array($userRole, $aRoleTransactionList) || in_array($userRole, $aRoleGasRemain)) && $showMenu ){
            $this->aMenu[] = array(
                'id'=>'gasremain_list',
                'name'=>'Gas dư',
            );
        }
        if( in_array($userRole, $aRoleManageStt) && $showMenu ){
            $this->aMenu[] = array(
                'id'=>'stt_list',
                'name'=>'Nhập Xuất STT',
            );
        }

        if( in_array($userRole, $aRoleCashbookList) && $showMenu ){
            $this->aMenu[] = array(
                'id'=>'cashbook_list',
                'name'=>'Thu - Chi tiền',
            );
        }
        if( in_array($userRole, $aRoleCashbookScheduleList) && $showMenu ){
            $this->aMenu[] = array(
                'id'=>'cashbook_schedule',
                'name'=>'Lịch thu tiền',
            );
        }

        if( in_array($userRole, $aRoleTransactionList) && $showMenu ){
            $this->aMenu[] = array(
                'id'=>'uphold_hgd_list',
                'name'=>'Bảo trì HGĐ',
            );
        }
        
        if( in_array($userRole, $aRoleUpholdList) && $showMenu){
            $text = 'Bảo trì bò mối';
            if($userRole == ROLE_CUSTOMER){
                $text = 'Bảo trì';
            }
            $this->aMenu[] = array(
                'id'=>'uphold_list',
                'name'=> $text,
            );
        }
        
        if( $userRole != ROLE_CUSTOMER && $showMenu ){
            $this->aMenu[] = array(
                'id'=>'customer_request_list',
                'name'=>'Yêu cầu vật tư KH',
            );
        }
        
        if( in_array($userRole, $aRoleTruckPlan) && $showMenu ){
            $this->aMenu[] = array(
                'id'=>'truck_plan_import',
                'name'=>'Gas bồn nhận',
            );
            $this->aMenu[] = array(
                'id'=>'truck_plan_export',
                'name'=>'Gas bồn giao',
            );
        }
        
        if( $userRole == ROLE_EMPLOYEE_MAINTAIN ){
            $this->aMenu[] = array(
                'id'=>'sj_list_code',
                'name'=>'SJ Code',
            );
        }
        
        if( in_array($userRole, $aRoleCashbookList) && $showMenu ){
            $this->aMenu[] = array(
                'id'=>'report',
                'name'=>'Báo cáo',
            );
        }

        if(in_array($userRole, $aRoleIssueList) && $showMenu){
            $this->aMenu[] = array(
                'id'=>'issue_list',
                'name'=>'Phản ánh sự việc',
            );
        }
        
        if( 0 ){
//        if( !in_array($userRole, $aRoleMessageNotIn) ){
            $this->aMenu[] = array(
                'id'=>'map_checker',
                'name'=>'Đánh dấu bản đồ',
            );
        }
        if( in_array($userRole, $aRoleCustomerHgdList) && $showMenu ){
            $this->aMenu[] = array(
                'id'=>'customer_list',
                'name'=>'KH hộ gia đình',
            );
            $this->aMenu[] = array(
                'id'=>'pttt_code_list',
                'name'=>'PTTT code',
            );
        }
        if($userRole != ROLE_CUSTOMER && $showMenu){
            $this->aMenu[] = array(
                'id'=>'scan_qr_code',
                'name'=>'Quét QR code',
            );
        }

        if( !in_array($userRole, $aRoleReportWorkingNotIn)  && $showMenu){
            $this->aMenu[] = array(
                'id'=>'working_report_list',
                'name'=>'Báo cáo công việc',
            );
        }
        
        if( in_array($userRole, $aRoleTicketList) ){
             $this->aMenu[] = array(
                'id'=>'ticket_list',
                'name'=>'Hỗ trợ',
            );
        }
        
        if( !in_array($userRole, $aRoleMessageNotIn) && $showMenu ){// luôn để menu tin nhắn ở dưới cùng
            $this->aMenu[] = array(
                'id'=>'message',
                'name'=>'Tin nhắn',
            );
        }
        if( in_array($userRole, $aRoleBoMoiList) ){
             $this->aMenu[] = array(
                'id'=>'google_map',
                'name'=>'Bản đồ',
            );
        }

        $this->aMenu[] = array(
            'id'=>'news_list',
            'name'=>'Thông báo',
        );
        
        $this->aMenu[] = array(
            'id'=>'user_profile',
            'name'=>'Tài khoản',
        );
        return $this->aMenu;
    }
    
    /**
     * @Author: ANH DUNG Now 24, 2015
     * @Todo: get menu + label + data 
     */
    public function getSomeConfig(&$result, $mUser) {
        $result['menu']             = $this->loginGetMenuUser($mUser);
        $result['max_upload']       = 10;// max upload image form app
        $result['role_id']          = $mUser->role_id;
        $result['user_id']          = $mUser->id; // Dec 11, 2015
        $result['user_info']        = self::UserInfo($mUser);// Dec 11, 2015
        $result['check_menu']       = self::getCheckMenuUser($mUser);// Dec 11, 2015
        $result['need_change_pass'] = $mUser->apiCheckChangePass();// Apr 18, 2016
        $result['need_update_app']  = 0;// Chưa xử lý
        $result['role_name']        = Roles::GetRoleNameById($mUser->role_id);
        // cần xử lý cache đoạn dưới
        $result['data_issue']       = HandleLabel::IssueLabel();
        $result['data_uphold']      = HandleLabel::UpholdReplyLabel();
        $result['list_street']      = HandleLabel::listStreet($mUser);
        $result['list_agent']       = HandleLabel::listAgent($mUser);
        $result['list_hgd_type']    = HandleLabel::listHgdTypePoint($mUser);
        $result['list_hgd_invest']  = HandleLabel::JsonIdName(GasConst::getInvestHgd());
        $result['call_center_uphold'] = self::getCallCenterUphold();
        $result['hotline']          = self::getHotline();
    }
    /**
     * @Author: ANH DUNG Now 22, 2016
     * @Todo: get menu + label + data 
     */
    public function getSomeConfigGas24h(&$result, $mUser) {
        $result['menu']             = $this->loginGetMenuGas24h($mUser);
        $result['max_upload']       = 10;// max upload image form app
        $result['role_id']          = $mUser->role_id;
        $result['user_id']          = $mUser->id; // Dec 11, 2015
        $result['user_info']        = self::UserInfo($mUser);// Dec 11, 2015
        $result['check_menu']       = [];// Dec 11, 2015
        $result['need_change_pass'] = '0';
        $result['list_street']      = HandleLabel::listStreet($mUser);
//        $result['update_profile']   = empty($mUser->province_id) ? '1' : '0';
        $result['update_profile']   = '0';
        
        // cần xử lý cache đoạn dưới
        $result['data_issue']       = [];
        $result['data_uphold']      = [];
//        $result['list_street']      = [];
        $result['list_agent']       = [];
        $result['list_hgd_type']    = [];
        $result['list_hgd_invest']  = [];
        $result['call_center_uphold'] = self::getCallCenterUphold();
        $result['hotline']          = self::getHotline();
        
        $mPromotionCode                 = new AppPromotion();
        $mPromotionCode->owner_id       = $mUser->id;
        $result['my_invite_code']       = $mPromotionCode->getMyInviteCode();
    }
    
    public static function UserInfo($mUser) {
        return array(
            array(
                'id'    => 'first_name',
                'name'  => "$mUser->first_name",
            ),
            array(
                'id'    => 'province_id',
                'name'  => "$mUser->province_id",
            ),
            array(
                'id'    => 'district_id',
                'name'  => "$mUser->district_id",
            ),
            array(
                'id'    => 'ward_id',
                'name'  => "$mUser->ward_id",
            ),
            array(
                'id'    => 'username',
                'name'  => "$mUser->username",
            ),
            array(
                'id'    => 'agent_id',
                'name'  => $mUser->getAgentOfEmployee().'',
            ),
            array(
                'id'    => 'address',
                'name'  => $mUser->getAddressApp().'',
            ),
        );
    }
    
    /** site/updateConfig
     * @Author: ANH DUNG Now 17, 2015
     */
    public function updateConfig($mUser, $objController)  {
        $result = ApiModule::$defaultSuccessResponse;
        $result['message'] = 'Login success';
        if(UsersTokens::isCustomerGas24h($mUser)){
            $result['gas24h'] = UsersTokens::APP_TYPE_GAS24H;
            $this->getSomeConfigGas24h($result, $mUser);
        }else{
            $result['gas24h'] = UsersTokens::APP_TYPE_GAS_SERVICE;
            $this->getSomeConfig($result, $mUser);
        }
        $mAppCache                      = new AppCache();
        $result['material_vo']          = $mAppCache->getObjMaterialByType(GasMaterialsType::$ARR_WINDOW_VO, AppCache::ARR_MODEL_MATERIAL_VO, ['GetActive'=>1]);
        $result['material_hgd']         = $mAppCache->getObjMaterialByType(GasMaterialsType::$ARR_VT_HGD, AppCache::ARR_MODEL_MATERIAL_HGD, ['GetActive'=>1]);
        $mTransaction = new Transaction();
        $result['order_status_cancel']  = HandleLabel::JsonIdName($mTransaction->getAppStatusCancel());
        $aTypeSell = Sell::model()->getArrayOrderType();
        unset($aTypeSell[Sell::ORDER_TYPE_THE_CHAN]);
        $result['order_list_type']      = HandleLabel::JsonIdName($aTypeSell);
        $result['order_list_discount_type'] = HandleLabel::JsonIdName(Sell::model()->getArrayDiscount());
        $result['storecard_status_cancel']  = HandleLabel::JsonIdName(GasStoreCard::model()->getStatusFalse());
        $result['limit_favorite']       = 6;
        $result['list_support_employee']    = HandleLabel::JsonIdName(TransactionHistory::model()->getArraySupport());
        $result['resize_width']         = 1000;// May 26, 2017 khoảng này cũng tạm đc,theo dõi thêm xem cần nâng lên 1400x1600 ko
        $result['resize_height']        = 1200;
        $result['customer_chain_store'] = HandleLabel::JsonIdName($mUser->getChainAccountApp());
        $mRemain = new GasRemain();
        $needMore = ['status'=>1, 'GetNameOnly'=>1, 'AppGasSerice'=>1];
        $result['gas_remain_type']          = $mRemain->getAppCreateType($mUser);
        $result['gas_remain_car']           = $mRemain->getListdataCar($mUser->parent_id, $mUser->role_id, $needMore);
        $result['gas_remain_driver']        = $mRemain->getListdataDriver($mUser->parent_id, $mUser->role_id, $needMore);
        $this->getParamGas24h($result, $mUser);
        
//        $result['time_put_location']        = 5;// 5 phút put location 1 lần - Update Mar3118, để lấy vị trí của xe tải
        $result['time_put_location']        = 10;// 5 phút put location 1 lần
        $result['gas24h_time_check_order']  = 60;// 60 second 
        // name field support_id
        
//        NamNH Nov 08, 2018 add code_account
        $result['code_account'] = isset($mUser->code_account) ? $mUser->code_account : '';
//        NamNH Apr 14,2019 send update config reward
//        [reward][open]
        $mReward = new RewardPoint();
        $result['reward_point'] = $mReward->getPointFromUsersInfo($mUser->id);

        ApiModule::sendResponse($result, $objController);
    }
    
    public function getParamGas24h(&$result, $mUser) {
        $result['gas24h_menu_text']         = AppText::GAS_24H_MENU_TEXT;
        $mReferralTracking = new ReferralTracking();
        $mReferralTracking->invited_id      = $mUser->id;
        $result['g24_can_input_invite_code']= $mReferralTracking->canInputInviteCode();
    }
    
    /**
     * @Author: ANH DUNG Dec 04, 2015
     */
    public static function AutocompleteUser($aModelUser, $mUser, $objController, $q)  {
        $result = ApiModule::$defaultSuccessResponse;
        $result['message'] = 'Success';
        if(isset($q->type)){
            foreach($aModelUser as $item){
                $result['record'][] = self::formatUserApp($item);
            }
        }else{
            $result['record'] = HandleLabel::formatCustomerName($aModelUser);
        }
        ApiModule::sendResponse($result, $objController);
    }
    
    /**
     * @Author: ANH DUNG Mar 02, 2017
     * @Todo: format data response autocomplete
     */
    public static function formatUserApp($mUser) {
        $tmp = [];
        $tmp['id']          = $mUser->id;
        $tmp['first_name']  = $mUser->code_bussiness.'-'.$mUser->first_name;
        $tmp['address']     = $mUser->address;
//        $tmp['phone']       = $mUser->phone;// Jun0618 tạm bỏ số phone này đi, vì dài quá và ko cần thiết ở app
        $tmp['phone']       = '';
        return $tmp;
    }
    
    /** @Author: ANH DUNG May 13, 2016 */
    public static function GetCustomerByPhone($aCustomer, $objController)  {
        $result = ApiModule::$defaultSuccessResponse;
        $result['message'] = 'Phone ok';
        foreach($aCustomer as $mUserPhone){
            $mUser      = $mUserPhone->rUser;
            $mUserRef   = $mUser->rUsersRef;
            $mSale      = $mUser->sale;
            $result['record'][] = HandleArrayResponse::handleInfoCustomer($mUser, $mUserRef, $mSale);
        }
        ApiModule::sendResponse($result, $objController);
    }
    
    /** @Author: ANH DUNG Mar 14, 2017 */
    public static function GetCustomerByPhoneFix($aCustomer, $objController)  {
        $result = ApiModule::$defaultSuccessResponse;
        $result['message'] = 'Phone ok';
        foreach($aCustomer as $mUser){
            $mUserRef   = $mUser->rUsersRef;
            $mSale      = $mUser->sale;
            $result['record'][] = HandleArrayResponse::handleInfoCustomer($mUser, $mUserRef, $mSale);
        }
        ApiModule::sendResponse($result, $objController);
    }
    
    /** @Author: ANH DUNG Jan 06, 2017
     * @Todo: get phone tổng đài bảo trì
     */
    public static function getCallCenterUphold() {
        return GasConst::PHONE_BOMOI;
    }
    public static function getHotline() {
        return '0918 318 328';
    }
    
    /**
     * @Author: ANH DUNG Jul 09, 2016
     * @Todo: get info customer
     */
    public static function handleInfoCustomer($mUser, $mUserRef, $mSale) {
        $contact = $contact_note = $sale_name = $sale_phone = $sale_type = '';
        if($mUserRef){
            $contact = $mUserRef->getFieldContact('contact_person_name');
            $contact_note = $mUserRef->getFieldContact('contact_note');
        }
        if($mSale){
            $sale_name  = $mSale->getFullName();
            $sale_phone = $mSale->getPhone();
            $sale_type  = $mSale->getSaleType();
        }

        return array(
            'customer_id'       => $mUser->id,
            'customer_name'     => $mUser->apiPhoneGetNameUser(),
            'customer_address'  => $mUser->getAddress(),
            'customer_phone'    => $mUser->getPhone(),
            'customer_agent'    => $mUser->getAgentOfCustomer(array('GetPhone'=>1)),
            'customer_type'     => $mUser->getTypeCustomerText().". Hạn TT: {$mUser->getPaymentDayCustomer()}",
            'customer_delivery_agent'       => UsersPhone::getDeliveryAgent($mUser),
            'customer_delivery_agent_id'    => $mUser->maintain_agent_id,// Aug 30, 2016 xử lý trả về đại lý gần nhất của KH bò mối ghi tạm vào cột maintain_agent_id
            'role_id'           => $mUser->role_id,
            'agent_id'          => $mUser->area_code_id,
            'contact'           => $contact,
            'contact_note'      => $contact_note,
            'sale_name'         => $sale_name,
            'sale_phone'        => $sale_phone,
            'sale_type'         => $sale_type,
        );
    }
    
    
    /**
     * @Author: ANH DUNG May 17, 2016
     */
    public static function GetCustomerByKeyword($aCustomer, $objController)  {
        $result = ApiModule::$defaultSuccessResponse;
        $result['message'] = 'Request ok';
        $result['record'] = $aCustomer;
        ApiModule::sendResponse($result, $objController);
    }
    
    /**
     * @Author: ANH DUNG Jun 03, 2016
     * @todo: get some config of user after login window
     */
    public static function WindowGetConfig($mUser, $objController, $q)  {
        $result = ApiModule::$defaultSuccessResponse;
        $result['message'] = 'Success';
        // 1 get dc đại lý của user đang login
        // 2. get nv giao nhận của đại lý đó
        if(isset($q->agent_id) && !empty($q->agent_id)){
            $agentId = $q->agent_id;
        }else{
            $agentId = GasOneMany::getOneIdByManyId($mUser->id, ONE_AGENT_ACCOUNTING);
        }

        if(empty($agentId)){
            $agentId = GasConst::UID_AGENT_CH1;
        }
        $agentList = HandleLabel::JsonIdName(Users::getArrDropdown(ROLE_AGENT));
        
        $result['record'] = array(
            'agent_list'                   => $agentList,
            'list_province'                => GasProvince::apiGetDataAddress(),
            'list_street'                  => HandleLabel::JsonIdName(GasStreet::listData()),
         );
        self::getAgentRecordInfo($result, $agentId);
        Logger::WriteLog('Window Login Agent: '. $agentId .' User ID: '.$mUser->id. ' - '.$mUser->username . ' Token: '.$q->token);
        
        ApiModule::sendResponse($result, $objController);
    }
    
    /**
     * @Author: ANH DUNG Jul 08, 2016
     * @Todo: get some info of agent
     */
    public static function getAgentRecordInfo(&$result, $agentId) {
        if(empty($agentId)){
            return ;
        }
        $aUid = GasOneMany::getArrOfManyIdByType(ONE_AGENT_MAINTAIN, $agentId);
//        Logger::WriteLog("Log gent new:::: ".json_encode($aUid).json_encode($agentId));
        $aEmployeeMaintain = HandleLabel::JsonIdName(Users::getListOptions($aUid));
        $mAgent     = Users::model()->findByPk($agentId);
        $aUidCCS    = GasOneMany::getArrOfManyId($agentId, ONE_AGENT_CCS);// Jul 09, 2016 xử lý limit CCS cho đại lý
        $aCCS       = HandleLabel::JsonIdName(Users::getListOptions($aUidCCS)); // Nhân viên CCS
        $aSellReason = HandleLabel::JsonIdName(GasConst::getSellReason()); // Nơ 08, 2016 
        
        $agentName      = $mAgent->getFullName();
        $agentPhone     = $mAgent->getAgentLandline();// Jul 19, 2016
        $agentCellPhone = $mAgent->getPhone();// Số di động của đại lý nhận SMS tạo thẻ kho Jul 19, 2016
        $agentAddress   = $mAgent->address;
        $agentProvince  = $mAgent->province_id;
        $agentDistrict  = $mAgent->district_id;
        
        // Jun 26, 2016 khởi tạo session cho price hgd put xuống window
//        UsersPrice::initSessionPriceHgd(date("Y-m-d"));// close on Now 06, 2016 đã lấy từ cache
        // Jun 26, 2016 khởi tạo session cho price hgd put xuống window
        
        $needMore = array('get_only_array'=>1, 'ModelAgent'=>$mAgent);
        // Jul 08, 2016 xử lý giới hạn loại gas và hàng Khuyến mãi của đại lý trên server
        $needMoreGas        = array('get_only_array'=>1, 'ModelAgent'=>$mAgent, 'MaterialGrop'=>GasMaterialsType::GROUP_GAS);
        $needMorePromotion  = array('get_only_array'=>1, 'ModelAgent'=>$mAgent, 'MaterialGrop'=>GasMaterialsType::GROUP_PROMOTION, 'GetAll'=>1);
        
        $result['record']['agent_id']           = $agentId;
        $result['record']['agent_name']         = $agentName;
        $result['record']['agent_phone']        = $agentPhone;
        $result['record']['agent_cell_phone']   = $agentCellPhone;
        $result['record']['agent_address']      = $agentAddress;
        $result['record']['agent_province']     = $agentProvince;
        $result['record']['agent_district']     = $agentDistrict;
        $result['record']['employee_maintain']  = $aEmployeeMaintain;
        $result['record']['monitor_market_development']  = $aCCS;
        $result['record']['list_order_reason']  = $aSellReason;
        $result['record']['material_gas']       = MyFunctionCustom::getMaterialsJsonByType(GasMaterialsType::$ARR_WINDOW_GAS, $needMoreGas);
        $result['record']['material_vo']        = MyFunctionCustom::getMaterialsJsonByType(GasMaterialsType::$ARR_WINDOW_VO, $needMore);
        $result['record']['material_promotion'] = MyFunctionCustom::getMaterialsJsonByType(GasMaterialsType::$ARR_WINDOW_PROMOTION, $needMorePromotion);
        $result['record']['can_update_price_gas'] = UsersS1::canUpdatePriceGas($mAgent);
        $mAppCache = new AppCache();
        $result['record']['list_user_executive']  = HandleLabel::JsonIdName($mAppCache->getListdataUserByRole(ROLE_SCHEDULE_CAR));
    }
    
    /**
     * @Author: ANH DUNG Jun 14, 2016
     * @todo: get some config of agent
     */
    public static function windowGetInfoAgent($mUser, $objController, $q)  {
        $result = ApiModule::$defaultSuccessResponse;
        $result['message'] = 'Success';
        // 1 get dc đại lý của user đang login
        // 2. get nv giao nhận của đại lý đó
        $agentId = $q->agent_id;
        $result['record'] = [];
        self::getAgentRecordInfo($result, $agentId);
        Logger::WriteLog('Window get windowGetInfoAgent Agent: '. $agentId .' User ID: '.$mUser->id. ' - '.$mUser->username . ' Token: '.$q->token);
        ApiModule::sendResponse($result, $objController);
    }
    
    /**  @Author: ANH DUNG Mar 02, 2017 */
    public function getDataCache($mUser, $objController)  {
        $result = ApiModule::$defaultSuccessResponse;
        $result['message'] = 'ok';
        $result['record']['list_type_in_out']           = HandleLabel::JsonIdName(CmsFormatter::getTypeInOutApp());
        $mAppCache                                      = new AppCache();
//        $mAppCache->setAppMaterialsStorecard(AppCache::ARR_APP_MATERIAL_STORECARD, []);// only dev test
        $result['record']['list_all_material']          = $mAppCache->getAppMaterialsStorecard(AppCache::ARR_APP_MATERIAL_STORECARD);
        $result['record']['list_cashbook_master_lookup']= HandleLabel::JsonIdName(EmployeeCashbook::model()->getListModelLookup(['listdata'=>1, 'limit_type'=>1]));
        $result['record']['list_ticket_handle']         = HandleLabel::JsonIdName(GasTickets::getAppHandle($mUser));
        
        ApiModule::sendResponse($result, $objController);
    }
    
}