<?php
/**
 * @Author: ANH DUNG Sep 21, 2015
 * @Todo: send notify to ios and android
 * @href: https://github.com/bryglen/yii-apns-gcm 
 */

class ApiNotify
{ 
    const TYPE_UPHOLD                   = 'VIEW_UPHOLD';
    const TYPE_ISSUE                    = 'VIEW_ISSUE';
    const TYPE_TRANSACTION              = 'VIEW_TRANSACTION';
    const TYPE_ORDER_BO_MOI             = 'TYPE_ORDER_BO_MOI';
    const TYPE_UPHOLD_HGD               = 'TYPE_UPHOLD_HGD';
    const TYPE_TICKET_EVENT             = 'TYPE_TICKET_EVENT';
    const TYPE_SPJ_CODE_GOBACK          = 'TYPE_SPJ_CODE_GOBACK';
    const TYPE_CUSTOMER_PAY_DEBIT       = 'TYPE_CUSTOMER_PAY_DEBIT';
    const TYPE_GAS24H_ORDER             = 'TYPE_GAS24H_ORDER';
    const TYPE_GAS24H_ANNOUNCE          = 'TYPE_GAS24H_ANNOUNCE';
    const TYPE_CMS_NEWS                 = 'TYPE_CMS_NEWS';
    const TYPE_GAS24H_OUT_OF_GAS        = 'TYPE_GAS24H_OUT_OF_GAS';
    const TYPE_GAS24H_REMAIN            = 'TYPE_GAS24H_REMAIN';
    const TYPE_GAS_SERVICE_REMAIN       = 'TYPE_GAS_SERVICE_REMAIN';
    const TYPE_SUPPORT_REQUEST_ITEM     = 'TYPE_SUPPORT_REQUEST_ITEM'; 
    const TYPE_TRUCK_PLAN               = 'TYPE_TRUCK_PLAN'; 
    const TYPE_GAS24H_SPIN              = 'TYPE_GAS24H_SPIN'; // DuongNV Sep1919 quay số gas24h

    const TYPE_ = '2'; 
    /**
     * @Author: ANH DUNG Sep 21, 2015
     * @Todo: send to android
     * @Param: $push_tokens is gcm of phone
     * @Param: $message text notify 
     * @Param: $customerProperty some property need to redirect link
     * array(
            'customProperty_1' => 'Hello',
            'customProperty_2' => 'World'
          ),
     */
    public static function android($push_tokens, $message, $customerProperty, $sendMulti=false)  {
        /* @var $apnsGcm YiiApnsGcm */
        $apnsGcm = Yii::app()->apnsGcm;
        $args = array(
                  'timeToLive' => 3
                );
        if($sendMulti){
            $apnsGcm->sendMulti(YiiApnsGcm::TYPE_GCM, $push_tokens, $message,
                $customerProperty,
                $args
              );
        }else{
            $apnsGcm->send(YiiApnsGcm::TYPE_GCM, $push_tokens, $message,
                $customerProperty,
                $args
              );
        }
    }

    /** @Author: ANH DUNG Sep 11, 2018
     *  @Todo: move to fcm server
     **/
    public static function androidFcm($mDeviceToken, $message, $customerProperty)  {
        //URL to POST JSON data to
        $url = "http://localhost:3000/push";
        $customerProperty['message']    = $message;
        $customerProperty['reply_id']   = isset($customerProperty['reply_id']) ? $customerProperty['reply_id'] : '';
        $android                        = ['priority' => 'high'];
        $apns                           = ['headers' => ['apns-priority' => '10']];
        $data_string = json_encode(array(
            'tokens'    => $mDeviceToken,
            'data'      => $customerProperty,
            'android'   => $android,
            'apns'      => $apns,
            'priority'  => 'high',
            'platform'  => 2,// Ju1419 1: android, 2: ios
            'title'     => 'Gas24H notification!1',// build xử lý theo type notify, mỗi Gas Service, khác Gas24h
            'app_type'  => 'high',// 0: Gas Service, 1: Gas24h
        ));

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
        curl_setopt($ch, CURLOPT_VERBOSE, TRUE);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
        curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                'Content-Type: application/json',
                'Content-Length: ' . strlen($data_string))
        );
        $result = curl_exec($ch);

        if (curl_error($ch) && $result != 'OK') {
            // echo 'Curl error: ' . curl_error($ch);
            $ip = isset($_SERVER['SERVER_ADDR']) ? $_SERVER['SERVER_ADDR'] : 'console run not have IP ';
            SendEmail::bugToDev("$ip Server IP Errors fcm notify stop, need check ".curl_error($ch));
        } else {
            // Done
            // echo "success: \n\n";
        }
        curl_close($ch);
    }
    
    /**
     * @Author: ANH DUNG Sep 21, 2015
     * @Todo: send to ios
     * @Param: $push_tokens is apns of phone
     * @Param: $message text notify
     * @Param: $customerProperty some property need to redirect link
     */
    public static function ios($push_tokens, $message, $customerProperty, $sendMulti=false)  {
//        $customerProperty['spj_message'] = $message;// không rõ tại sao Nguyên không lấy dc $message, nên phải dùng cách này
        /* @var $apnsGcm YiiApnsGcm */
//        Logger::WriteLog('Debug IOS Send '.  json_encode($push_tokens));
        $apnsGcm = Yii::app()->apnsGcm;
        $args = array(
                  'sound'=>'default',
                  'badge'=>1
                );
        $res = [];
        if($sendMulti){
            $res = $apnsGcm->sendMulti(YiiApnsGcm::TYPE_APNS, $push_tokens, $message,
                $customerProperty,
                $args
            );
        }else{
            $res = $apnsGcm->send(YiiApnsGcm::TYPE_APNS, $push_tokens, $message,
                $customerProperty,
                $args
            );
        }
//        if(!GasCheck::isServerLive()){
//            Logger::WriteLog(MyFormat::jsonEncode($res));
//        }
    }
    
    /**
     * @Author: ANH DUNG Sep 21, 2015
     * @Todo: something
     * @Param: $model
     */
    public static function BuildToSend( $arrayDeviceToken, $message, $customerProperty, $type ) {
        if(count($arrayDeviceToken)){
            $mDeviceToken = [];
            foreach($arrayDeviceToken as $model):
                if(empty($model->device_token)){
                    continue ;
                }
                $mDeviceToken[] = $model->device_token;
                // send one Close on Now 27, 2016
//                if($type == YiiApnsGcm::TYPE_GCM){// android
//                    ApiNotify::android($model->device_token, $message, $customerProperty);
//                }else{ // ios
//                    ApiNotify::ios($model->device_token, $message, $customerProperty);                    
//                } 
            endforeach;
            
            // send multi đã test Now 27, 2016 nhận dc với 2 device android
            if($type == YiiApnsGcm::TYPE_GCM){// android
//                 ApiNotify::android($mDeviceToken, $message, $customerProperty, true);
                 ApiNotify::androidFcm($mDeviceToken, $message, $customerProperty);
            }else{// ios
                ApiNotify::ios($mDeviceToken, $message, $customerProperty, true);
            }
        }
    }
    
}