<?php
/**
 * @author bb qbao <quocbao1087@gmail.com>
 * @copyright (c) 2013, Quoc Bao
 */

class ApiBackupController extends MainController
{
    public $mUserApp = null;
    /**
     * @Author: ANH DUNG Jan 19, 2015
     * @Todo: check server bao tri
     * @Param: $mUser thong tin user dang login, luon lay thong tin moi nhat 
     */
    public function checkServerMaintenance(){
        $ok = true;
        if(Yii::app()->setting->getItem('server_maintenance') == 'yes'){
            $ok=false;
        }
        return $ok;
    }
    
    public function init()
    {
        if(!$this->checkServerMaintenance()){
            $result = ApiModule::$defaultResponse;
            $result['message'] = 'Máy chủ bảo trì, vui lòng quay lại sau';
            ApiModule::sendResponse($result, $this);
        }
        parent::init();
    }
    
    public $mLogRequest;
    public $mLogUserId;
    public $mCheckSignup; // Oct 25, 2015 model của signup_code
    
    /**
     * 
     * @param type $DataProvider
     * @return json
     * @author bb   <quocbao1087@gmail.com>
     */
    public function dataProviderToArray($DataProvider) {
        if(is_object($DataProvider)) {
            $result = array();
            $data = $DataProvider->data;
            foreach($data as $item){
                $result[] = $item->attributes;
            }
            return $result;
        }
    }
    
    
    public function log()
    {
        $model = new ApiRequestLogs();
        if($_SERVER['REQUEST_METHOD'] == 'POST')
            $data = $_POST;
        elseif($_SERVER['REQUEST_METHOD'] == 'GET')
            $data = $_GET;
        if(isset($_SERVER['REQUEST_URI']))
//            $requestURI = $_SERVER['REQUEST_URI'];
            $requestURI = Yii::app()->controller->id."/".Yii::app()->controller->action->id;
        else
            $requestURI='';
        
        $model->method = $_SERVER['REQUEST_METHOD']. ' - '.$requestURI;
        
//        if(is_array($data))
//            $model->content = json_encode($data);
//        else
//            $model->content = $data;
        $model->content = var_export($data, true);
        
        $model->created_date = date('Y-m-d H:i:s');
        $model->save();
        $this->mLogRequest = $model;
    }
    
    /**
     * @Author: ANH DUNG Oct 12, 2015
     * @Todo: ghi log của response của request đó
     */
    public function updateLogResponse($response) {
        if($this->mLogRequest){
           $aUpdate = array('response', 'user_id', 'responsed_date');
           $this->mLogRequest->response =  $response;
           $this->mLogRequest->responsed_date =  date('Y-m-d H:i:s');
           $this->mLogRequest->update($aUpdate);
        }
    }
        
    /**
     * @Author: ANH DUNG Oct 12, 2015
     * @Todo: get User login and get id user for log
     * @Param: $q array request. after decode json
     */
    public function getUserLogin($q) {
        $q->username = trim($q->username);
        $mUser = Users::ApiGetUserLogin(strtolower($q->username));
        if($mUser){
            $this->setLogUserId($mUser->id);
        }
        return $mUser;
    }

    /**
     * @Author: ANH DUNG Oct 12, 2015
     * @Todo: update user id neu co vao log
     */
    public function setLogUserId($user_id) {
        if($this->mLogRequest){
            $this->mLogRequest->user_id = $user_id;
        }
    }
    
    public function checkRequest()
    {
//        $this->log();//ok, nguy hiểm khi sử dụng, phải close lại khi test xong
        $method = $_SERVER['REQUEST_METHOD'];
        if($method == 'POST')
        {
            if(empty($_POST['q']))
            {
                $result = ApiModule::$defaultResponse;
                $result['message'] = 'Missing q as a param. Ex: user/loginFirstTime?q={"username":"0909456789"}';
                ApiModule::sendResponse($result, $this);
            }
            else{
               $q = json_decode($_POST['q'], true);
               if(!is_array($q))
               {
                   $result = ApiModule::$defaultResponse;
                   $result['message'] = 'Invalid JSON encode format';
                   ApiModule::sendResponse($result, $this);
               }
            }
        }
        else//default
        {
            $result['message'] = 'Invalid request. Please check your http verb';
            ApiModule::sendResponse($result, $this);
        }
    }
    
    /** @Author: ANH DUNG Jun 09, 2018
     *  @Todo: khởi tạo thêm 1 số biến sử dụng ở bên trong
     **/
    public function initSomeParams($q) {
        $_GET['platform']       = isset($q->platform) ? $q->platform : UsersTokens::PLATFORM_IOS;
        $_GET['version_code']   = isset($q->version_code) ? $q->version_code : 1;
    }
    
    public function checkRequiredParams($q, $arrayOfFieldNames)
    {
        $this->initSomeParams($q);
        $arrayOfInvalidFields = array();
        $isValid = true;
        if(!is_array($q))
        {
            foreach ($arrayOfFieldNames as $f)
            {
                if(!isset($q->$f))
                {
                    $isValid = false;
                    $arrayOfInvalidFields[] = $f;
                }
            }
        }
        else
        {
            foreach ($arrayOfFieldNames as $f)
            {
                if(!isset($q[$f]))
                {
                    $isValid = false;
                    $arrayOfInvalidFields[] = $f;
                }
            }
        }
        
        if(!$isValid)
        {
            $result = ApiModule::$defaultResponse;
            $result['message'] = 'Missing param. Reference in record: '.json_encode($arrayOfInvalidFields);
//            $result['record'] = ''; Now0317 bỏ để phù hợp với parse của Trung
            ApiModule::sendResponse($result, $this);
        }
    }
    
    /**
     * @Author: ANH DUNG Jul 20, 2015
     * @Todo: get user from token
     * @Param: $result, $q->token
     */
    public function getUserByToken($result, $token) {
        $mUser = UsersTokens::getModelUser($token);
        if(is_null($mUser) || ( $mUser && $mUser->status == STATUS_INACTIVE ))
        {
            $result['message'] = 'Phiên làm việc của bạn đã hết hạn. Vui lòng đăng nhập lại';
            $result['code'] = ApiModule::$_CODE_LOGOUT;
            ApiModule::sendResponse($result, $this);
        }
        $this->setLogUserId($mUser->id);
        return $mUser;
    }
    
    /**
     * @Author: ANH DUNG Oct 20, 2016
     * @Todo: check required (version_code) user update app to new version
     */
    public function checkVersionCode($q, &$model, $fieldNameAddError='id') {
        if($this->mUserApp && $this->mUserApp->role_id == ROLE_CUSTOMER){
//            Logger::WriteLog('is customer checkVersionCode'. Yii::app()->setting->getItem('AppVersionCode'));
            return ;// Jul2517 không check version với KH
        }
        $versionServerAndroid  = Yii::app()->setting->getItem('AppVersionCode');
        $versionClient  = isset($q->version_code) ? $q->version_code : 1;
        if($versionClient < $versionServerAndroid && $q->platform == UsersTokens::PLATFORM_ANDROID){
            $result = ApiModule::$defaultResponse;
            $result['message']  = 'Phiên bản app đã cũ, vui lòng cập nhật phiên bản mới hơn để sử dụng';
            $result['code']     = ApiModule::$_CODE_OLD_VERSION;
            ApiModule::sendResponse($result, $this);
//            $model->addError($fieldNameAddError, "Phiên bản app đã cũ, vui lòng cập nhật phiên bản mới hơn để sử dụng");
        }
    }
    
    /** @Author: ANH DUNG Feb 05, 2017
     *  @Todo: APP Gas24h check required (version_code) user update app to new version
     */
    public function checkVersionCodeGas24h($q, &$model) {
        $versionServerAndroid   = Yii::app()->setting->getItem('AppVersionCodeGas24hAndroid');
        $versionServerIos       = Yii::app()->setting->getItem('AppVersionCodeGas24hIos');
        $versionClient          = isset($q->version_code) ? $q->version_code : 1;
        if( ($versionClient < $versionServerAndroid && $q->platform == UsersTokens::PLATFORM_ANDROID)
            || ($versionClient < $versionServerIos && $q->platform == UsersTokens::PLATFORM_IOS)
        ){
            $json = isset($_POST) ? MyFormat::jsonEncode($_POST) : ' Nodata';
            $info = 'Gas24h API ERROR -- Phiên bản app đã cũ: '.$json;
            Logger::WriteLog($info);
            $result = ApiModule::$defaultResponse;
            $result['message']  = 'Phiên bản app đã cũ, vui lòng cập nhật phiên bản mới hơn để tiếp tục sử dụng';
            $result['code']     = ApiModule::$_CODE_OLD_VERSION;
            ApiModule::sendResponse($result, $this);
        }
    }
    
    /**
     * @Author: ANH DUNG Apr 21, 2017
     * @Todo: get agent của NV Giao nhận
     */
    public function getAgentOfEmployee($mUser) {
        $result = ApiModule::$defaultResponse;
        $aRoleAllow = GasConst::getRoleAppReport();
        if(!in_array($mUser->role_id, $aRoleAllow)){
            return 0;
        }
        $aRoleLikeDriver = [ROLE_DRIVER, ROLE_DEBT_COLLECTION, ROLE_CRAFT_WAREHOUSE];
        if(in_array($mUser->role_id, $aRoleLikeDriver)){
            if(empty($mUser->parent_id)){
                throw new Exception('Bạn chưa được setup đại lý, vui lòng liên hệ Dũng 01684 331 552 để được hỗ trợ');
            }
            return $mUser->parent_id;
        }
        $aAgent     = Users::GetKeyInfo($mUser, UsersExtend::JSON_ARRAY_AGENT);
        if(!in_array($mUser->role_id, $aRoleAllow) || !is_array($aAgent)){
            $result['message'] = 'Yêu cầu không hợp lệ 1. Chưa cài đặt đại lý';
            ApiModule::sendResponse($result, $this);
        }
        $agent_id   = reset($aAgent);
        if(empty($agent_id)){
            $result['message'] = 'Yêu cầu không hợp lệ 2. Chưa cài đặt đại lý';
            ApiModule::sendResponse($result, $this);
        }
        return $agent_id;
    }
    
    /**
     * @Author: ANH DUNG Jun 05, 2017
     * @Todo: set override agent của Bất kỳ nhân viên nào nếu được
     */
    public function setOverrideAgentOfEmployee(&$mUser) {
        $aRoleAllow = [ROLE_EMPLOYEE_MAINTAIN];
        if(in_array($mUser->role_id, $aRoleAllow)){
            $mUser->parent_id = $this->getAgentOfEmployee($mUser);
        }
    }

    /**
     * @Author: ANH DUNG Mar 19, 2017
     * @Todo: get secret của bên Server VOIP PBX South Telecom
     */
    public function getSecret() {
        return '1923d10f88daae995b1f461c04f15165';
    }
    
    public function checkRequiredLogout() {
        if(!is_null($this->mUserApp) && MonitorUpdate::isMobileRequiredLogout($this->mUserApp->id)){
            throw new Exception('Phiên làm việc của bạn đã hết hạn. Vui lòng đăng nhập lại');
        }
    }
    
    /** @Author: ANH DUNG Dec 12, 2017
     *  @Todo: send response if error
     */
    public function responseIfError($model) {
        if($model->hasErrors()){
            $result = ApiModule::$defaultResponse;
            $result['message'] = HandleLabel::FortmatErrorsModel($model->getErrors());
            ApiModule::sendResponse($result, $this);
        }
    }
    
    /** @Author: ANH DUNG Jun 11, 2018
     *  @Todo: check required location
     */
    public function validateLocationGps($q) {
        if(empty($q->latitude) || empty($q->longitude)){
            throw new Exception('Chưa cập nhật được vị trí của bạn, vui lòng bật tắt lại GPS');
        }
    }
}
