<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'gas-one-many-form',
	'enableAjaxValidation'=>false,
        'htmlOptions' => array('enctype' => 'multipart/form-data'),
)); ?>

    <p class="note">Dòng có dấu <span class="required">*</span> là bắt buộc.</p>
    <?php echo MyFormat::BindNotifyMsg(); ?>
    
        		
    <div class="row">
        <?php echo $form->labelEx($model,'supplier_id'); ?>
        <?php echo $form->dropdownList($model,'supplier_id', TruckPlan::model()->getArraySupplier(), array('class'=>'w-300', 'empty' => 'Select')); ?>
        <?php echo $form->error($model,'supplier_id'); ?>
    </div>
    
    <div class="row ">
        <?php echo $form->labelEx($model,'supplier_warehouse_id'); ?>
        <?php echo $form->dropdownList($model,'supplier_warehouse_id', TruckPlan::model()->getArraySupplierWarehouse(), array('class'=>'w-300', 'empty' => 'Select')); ?>
        <?php echo $form->error($model,'supplier_warehouse_id'); ?>
    </div>
    <div class="row">
        <?php echo $form->labelEx($model,'qty_contract', array('label' => 'SL ký hợp đồng (tấn)')); ?>
        <?php echo $form->textField($model,'qty_contract', ['class'=>'f_size_15 w-300 ad_fix_currency number_only_v1', 'maxlength' => 8]); ?>
        <?php echo $form->error($model,'qty_contract'); ?>
    </div>
    <div class="row">
        <?php echo $form->labelEx($model, 'month'); ?>
        <?php 
        $this->widget('zii.widgets.jui.CJuiDatePicker', array(
            'model' => $model,

            'attribute' => 'month',
            'options' => array(
                'showAnim' => 'fold',
                'dateFormat'=> ActiveRecord::getDateFormatJquery(),
                'changeMonth' => true,
                'changeYear' => true,
                'showOn' => 'button',
                'buttonImage' => Yii::app()->theme->baseUrl . '/admin/images/icon_calendar_r.gif',
                'buttonImageOnly' => true,

            ),

            'htmlOptions' => array(
                'class' => 'w-16',
                'style' => 'height:20px;',
                'readonly' => 'readonly',
            ),
        ));
        ?>  
        <?php echo $form->error($model, 'month'); ?>
    </div>
	<div class="row buttons" style="padding-left: 141px;">
		        <?php $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType'=>'submit',
            'label'=>$model->isNewRecord ? 'Create' :'Save',
            'type'=>'null', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
            'size'=>'small', // null, 'large', 'small' or 'mini'
            //'htmlOptions' => array('style' => 'margin-bottom: 10px; float: right;'),
        )); ?>	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->
<script>
    $(document).ready(function(){
        $('.form').find('button:submit').click(function(){
            $.blockUI({ overlayCSS: { backgroundColor: '#fff' } }); 
        });
        
    });
</script>