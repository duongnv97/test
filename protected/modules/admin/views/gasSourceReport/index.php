<?php
$this->breadcrumbs=array(
	$this->pageTitle,
);

$menus=array(
            array('label'=>"Create $this->pageTitle", 'url'=>array('create')),
);
$this->menu= ControllerActionsName::createMenusRoles($menus, $actions);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('gas-source-report-grid', {
                url : $(this).attr('action'),
		data: $(this).serialize()
	});
	return false;
});
");

Yii::app()->clientScript->registerScript('ajaxupdate', "
$('#gas-source-report-grid a.ajaxupdate').live('click', function() {
    $.fn.yiiGridView.update('gas-source-report-grid', {
        type: 'POST',
        url: $(this).attr('href'),
        success: function() {
            $.fn.yiiGridView.update('gas-source-report-grid');
        }
    });
    return false;
});
");
?>

<h1><?php echo $this->pageTitle; ?></h1>


<?php echo CHtml::link('Tìm Kiếm Nâng Cao','#',array('class'=>'search-button')); ?>
<div class="search-form">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'gas-source-report-grid',
	'dataProvider'=>$model->search(),
        'afterAjaxUpdate'=>'function(id, data){ fnUpdateColorbox();}',
        'template'=>'{pager}{summary}{items}{pager}{summary}',
//    'itemsCssClass' => 'items custom_here',
//    'htmlOptions'=> array('class'=>'grid-view custom_here'),
        'pager' => array(
            'maxButtonCount'=>  CmsFormatter::$PAGE_MAX_BUTTON,
        ),
	'enableSorting' => false,
	//'filter'=>$model,
	'columns'=>array(
        array(
            'header' => 'S/N',
            'type' => 'raw',
            'value' => '$this->grid->dataProvider->pagination->currentPage * $this->grid->dataProvider->pagination->pageSize + ($row+1)',
            'headerHtmlOptions' => array('width' => '10px','style' => 'text-align:center;'),
            'htmlOptions' => array('style' => 'text-align:center;')
        ),
            array(
                'header' => 'Tháng',
                'name' => 'month',
                'type'=>'raw',
                'value' => '$data->getMonth()',
                'htmlOptions' => array('style' => 'text-align:right;'),
            ),
            array(
                'name' => 'supplier_id',
                'type' => 'raw',
                'value'=>'$data->getSupplier()',
            ),
            array(
                'name' => 'supplier_warehouse_id',
                'type' => 'raw',
                'value'=>'$data->getSupplierWarehouse()',
                'footer' => 'Tổng',
                'footerHtmlOptions' => array('class'=>'item_b item_r'),
            ),
            array(
                'header' => 'SL ký hợp đồng (kg)',
                'name' => 'qty_contract',
                'type' => 'raw',
                'value' => '$data->getNumOfSignedContracts()',
                'htmlOptions' => array('style' => 'text-align:right;'),
                'footer' => $model->getTotal($model->search()->data, 'qty_contract'),
                'footerHtmlOptions' => array('class'=>'item_b item_r'),
            ),
            array(
                'header' => 'Đã nhận (kg)',
                'type'=>'raw',
                'value' => '$data->getLinkDetailTruckPlan()',
                'htmlOptions' => array('style' => 'text-align:right;'),
                'footer' => $model->getTotal($model->search()->data, 'getReceived', true),
                'footerHtmlOptions' => array('class'=>'item_b item_r'),
            ),
            array(
                'header' => 'Còn lại (kg)',
                'type'=>'raw',
                'value' => '$data->getRest()',
                'htmlOptions' => array('style' => 'text-align:right;'),
                'footer' => $model->getTotal($model->search()->data, 'getRawRest', true),
                'footerHtmlOptions' => array('class'=>'item_b item_r'),
            ),
		array(
                    'header' => 'Actions',
                    'class'=>'CButtonColumn',
                    'template'=> ControllerActionsName::createIndexButtonRoles($actions),
                    'buttons'=>array(
//                        'update' => array(
//                            'visible' => '$data->canUpdate()',
//                        ),
//                        'delete'=>array(
//                            'visible'=> 'GasCheck::canDeleteData($data)',
//                        ),
                    ),
		),
	),
)); ?>

<script>
$(document).ready(function() {
    fnUpdateColorbox();
});

function fnUpdateColorbox(){   
    fixTargetBlank();
}
</script>