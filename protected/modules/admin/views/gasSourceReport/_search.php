<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=> GasCheck::getCurl(),
	'method'=>'get',
)); ?>

    
        <div class="row more_col">
            <div class="col1">
                <?php echo $form->label($model,'supplier_id'); ?>
                <?php echo $form->dropdownList($model,'supplier_id',TruckPlan::model()->getArraySupplier(),array('class'=>'w-200', 'empty'=>'Select')); ?>
            </div>
            <div class="col2">
                <?php echo $form->labelEx($model,'supplier_warehouse_id', array('label' => 'Kho NCC')); ?>
            <?php echo $form->dropdownList($model,'supplier_warehouse_id', TruckPlan::model()->getArraySupplierWarehouse(), array('class'=>'w-200', 'empty' => 'Select')); ?>
            </div>
        </div>
         <div class="row more_col">
            <div class="col1">
                <?php echo $form->labelEx($model, 'dateFrom', ['label'=>'Từ ngày']); ?>
                <?php 
                $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                    'model' => $model,
                    'attribute' => 'dateFrom',
                    'options' => array(
                        'showAnim' => 'fold',
                        'dateFormat' => MyFormat::$dateFormatSearch,
                        'changeMonth' => true,
                        'changeYear' => true,
                        'showOn' => 'button',
                        'buttonImage' => Yii::app()->theme->baseUrl . '/admin/images/icon_calendar_r.gif',
                        'buttonImageOnly' => true,

                    ),

                    'htmlOptions' => array(
                        'class' => 'w-16',
                        'style' => 'height:20px;',
                    ),
                ));
                ?> 
            </div>
            <div class="col2">
                <?php echo $form->labelEx($model, 'dateTo', ['label'=>'Đến ngày']); ?>
                <?php 
                $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                    'model' => $model,
                    'attribute' => 'dateTo',
                    'options' => array(
                        'showAnim' => 'fold',
                        'dateFormat' => MyFormat::$dateFormatSearch,
                        'changeMonth' => true,
                        'changeYear' => true,
                        'showOn' => 'button',
                        'buttonImage' => Yii::app()->theme->baseUrl . '/admin/images/icon_calendar_r.gif',
                        'buttonImageOnly' => true,

                    ),

                    'htmlOptions' => array(
                        'class' => 'w-16',
                        'style' => 'height:20px;',
                    ),
                ));
                ?> 
            </div>
        </div>

	<div class="row buttons" style="padding-left: 159px;">
		        <?php $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType'=>'submit',
            'label'=>'Search',
            'type'=>'null', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
            'size'=>'small', // null, 'large', 'small' or 'mini'
            //'htmlOptions' => array('style' => 'margin-bottom: 10px; float: right;'),
        )); ?>	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->