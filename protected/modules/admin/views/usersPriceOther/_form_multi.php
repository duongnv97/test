<div class="row">
    <?php echo $form->labelEx($model,'customer_id'); ?>
    <?php echo $form->textField($model,'customer_id',array("class"=>"customer_id_autocomplete","style"=>"", "size"=>32,"placeholder"=>"Nhập mã hoặc tên KH")); ?>
    <?php echo $form->error($model,'customer_id'); ?>
    <div class="clr"></div>    		
</div>
<div class="row">
    <?php echo $form->label($model,'date_apply'); ?>
    <?php 
        $this->widget('zii.widgets.jui.CJuiDatePicker', array(
            'model'     =>$model,        
            'attribute' =>'date_apply',
            'options'   =>array(
                'showAnim'      =>'fold',
                'dateFormat'    => ActiveRecord::getDateFormatJquery(),
//                'maxDate'       => '0',
                'changeMonth'   => true,
                'changeYear'    => true,
                'showOn'        => 'button',
                'buttonImage'   => Yii::app()->theme->baseUrl.'/admin/images/icon_calendar_r.gif',
                'buttonImageOnly'=> true,                                
            ),        
            'htmlOptions'=>array(
                'class'     =>'w-150',
                'size'      =>'16',
                'readonly'  =>'readonly',
                'value'     => date('d/m/Y'),
                'style'     =>'float:left;',
            ),
        ));
     ?>   
    <?php echo $form->error($model,'date_apply'); ?>
</div>
<div class="row">
    <table class="materials_table cus_table materials_table_root materials_table_th hm_table" style="width: 100%">
        <thead>
            <tr>
                <th class="item_c w-20">#</th>
                <th class="w-350 item_c">Khách Hàng</th>
                <th class="w-350 item_c">NV Kinh Doanh</th>
                <th class="w-350 item_c">Vật tư</th>
                <th class="w-30 item_c last">Xóa</th>
            </tr>
        </thead>
        <tbody class="tBodyCus">
            <!--_form table for info-cus-price-->
                            </tbody>
                        </table>
</div>

<div class="clr"></div>

<?php Yii::app()->clientScript->registerCoreScript('jquery.ui'); ?>
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/autoNumeric-master/autoNumeric.js"></script>
<script>
    var temp = 0;
    $(document).keydown(function(e) {
        if(e.which == 119) {
            fnBuildRow();
        }
    });
    
    // Jul 21, 2016 Begin for table multi customer
    $(document).ready(function(){
        $('.form').find('button:submit').live('click', function(){
            $.blockUI({ overlayCSS: { backgroundColor: '#fff' } }); 
        });
        fnInitCusAutocomplete();
        fnMaterialsAutocomplete(); 
        fnBindRemoveIcon();
        bindEventForHelpNumber();
        gSelectChange(<?php echo UsersRef::PRICE_OTHER;?>);
        fnInitInputCurrency();
    });

    // NhanDT Sep2019 autocomplete for customer
    function fnInitCusAutocomplete(){
        $( ".customer_id_autocomplete" ).autocomplete({
            source: '<?php echo Yii::app()->createAbsoluteUrl('admin/ajax/search_user_by_code');?>',
            close: function( event, ui ) { $( ".customer_id_autocomplete" ).val(''); },
            select: function( event, ui ) {
                var classname   = ($(this).parent().parent().attr('class'));
                var class_item  = 'customer_row_'+ui.item.id;
                if($('.'+class_item).size()<1){ 
                    fnAddRowCus( event, ui);
                }
            }
        });
    }
    
    // NhanDT Sep2019 create multi materials for one customer
    function fnAddRowCus(event, ui){
        var _tr = '';
        var class_item = 'customer_row_'+ui.item.id;
        _tr += '<tr class="'+class_item+'">';
        _tr +=      '<td class="item_c order_no">1</td>';
        _tr +=      '<td style="padding: 10px !important; " class="info_customer">';
        _tr +=          '<h4>'+ui.item.value+'</h4>';
        _tr +=          '<input class="" value="" type="hidden" name="customer_id['+ui.item.id+']">';
        _tr +=      '</td>';
        _tr +=      '<td style="padding: 10px !important; " class="col_sale">';
        _tr +=          '<div class="float_l">';
        _tr +=             '<input class="sale_id_hide" value="" type="hidden" name="customer_id['+ui.item.id+'][sale_id]">';
        _tr +=             '<input class="float_l sale_id_autocomplete w-250"  placeholder="Nhập tên số đt, từ <?php echo MIN_LENGTH_AUTOCOMPLETE; ?> ký tự" maxlength="100" value="" type="text" >';
        _tr +=             '<span class="remove_row_item" onclick="fnRemoveName(this);"></span>';
        _tr +=           '</div>';
        _tr +=      '</td>';
        _tr +=      '<td style="padding: 10px !important; " class="item_l material_name body_add_detail">';
        _tr +=          '<?php echo $form->textField($model,"materials_id",array("class"=>"materials_autocomplete","style"=>"", "size"=>32,"placeholder"=>"Nhập mã vật tư hoặc tên vật tư")); ?>';
        _tr +=          '<?php echo $form->error($model,'materials_id'); ?>';
        _tr +=          '<table style="width: 90%; margin:0 auto !important; margin-bottom: 10px !important;" class="materials_table details_table hm_table" id="materials_table">';
        _tr +=              '<thead> <tr>';
        _tr +=                  '<th class="w-180 item_name item_c">Tên</th>';
        _tr +=                  '<th class="w-100 item_c">Giá</th>';
        _tr +=                  '<th class="item_unit last item_c">Xóa</th>';
        _tr +=              '</tr> </thead>';
        _tr +=              '<tbody class="tBodyMaterials"> </tbody>';
        _tr +=           '</table>';
        _tr +=      '</td>'; 
        _tr +=      '<td class="item_c last"><span remove="" class="remove_icon_only"></span></td>';
        _tr += '</tr>';
        _tr += '</br>';
        $('.tBodyCus').append(_tr);
        fnAddInfoCus(ui);
        fnMaterialsAutocomplete();
        fnRefreshOrderNumber();
        $('.sale_id_autocomplete').each(function(){
            fnSaleAutocomplete($(this));
        });
    }
    
    /**
    * @Author: NhanDT Sep 11, 2019
    * @Todo: add info cus into <td>
    * @Param:  param
    */
    function fnAddInfoCus(ui){
        var table = '';
        table += '<div class="autocomplete_customer_info table_small" style="">';
        table += '<table>';
        table +=    '<tr>';
        table +=        '<td class="_l td_first_t">Mã KH:</td>';
        table +=        '<td class="_r info_code_bussiness td_last_r td_first_t">'+ui.item.code_bussiness+'</td>';
        table +=    '</tr>';

        table +=    '<tr>';
        table +=        '<td class="_l">Tên KH:</td>';
        table +=        '<td class="_r info_name td_last_r">'+ui.item.name_customer+'</td>';
        table +=    '</tr>';
        table +=    '<tr>';
        table +=        '<td class="_l">Địa chỉ:</td>';
        table +=        '<td class="_r info_address td_last_r">'+ui.item.address+'</td>';
        table +=    '</tr>';
        table +=    '<tr>';
        table +=        '<td class="_l">Điện Thoại:</td>';
        table +=        '<td class="_r info_phone td_last_r">'+ui.item.phone+'</td>';
        table +=    '</tr>';
        table += '</table>';
        table += '</div>';
        table += '<div class="clr"></div';
        $('.customer_row_'+ui.item.id+' .info_customer').append(table);
        $('.customer_row_'+ui.item.id+' .info_customer').find('.autocomplete_customer_info').show();
    }

    // NhanDT Sep2019 autocomplete for materials
    function fnMaterialsAutocomplete(){
        var availableMaterials = <?php echo MyFunctionCustom::getMaterialsJson(array('materials_id'=>$model->materials_id)); ?>;
        $( ".materials_autocomplete" ).autocomplete({
            source: availableMaterials,
            close: function( event, ui ) { $( ".materials_autocomplete" ).val(''); },
            select: function( event, ui ) {
                var classname   = ($(this).parent().parent().attr('class'));
                var customer_id = classname.replace('customer_row_', '');
                customer_id     = customer_id.replace(' selected', '');
                var class_item  = 'materials_row_'+ui.item.id;
                console.log(customer_id);
                if($('.customer_row_'+customer_id+' .'+class_item).size()<1){ 
                    fnSetValueMaterial( event, ui, customer_id);
                }
            }
        });
    }
    
    function fnSetValueMaterial(event, ui, customer_id){
        var _tr = '';
        var class_item = 'materials_row_'+ui.item.id;
        _tr += '<tr class="details_row '+class_item+'">';
        _tr +=      '<td class="item_l material_name">';
        _tr +=          '<label>'+ui.item.value+'</label>';
        _tr +=          '<input class="" value="'+ui.item.id+'" type="hidden" name="customer_id['+customer_id+'][materials_id][]">';
        _tr +=          '<input class="" value="'+ui.item.materials_type_id+'" type="hidden" name="customer_id['+customer_id+'][materials_type_id][]">';
        _tr +=      '</td>';
        _tr +=      '<td class="item_c">';
        _tr +=          '<input name="customer_id['+customer_id+'][price_value][]" class="w-80 gPriceValue number_only ad_fix_currency" maxlength="9" value="" type="text" placeholder="Giá vật tư">';
        _tr +=      '</td>'; 
        _tr +=      '<td class="item_c last"><span class="remove_icon_only"></span></td>';
        _tr += '</tr>';
        $('.customer_row_'+customer_id+' .tBodyMaterials').append(_tr);
        fnInitInputCurrency();
    }
    
    // NhanDT Sep2019 autocomplete for sale_id    
    function fnSaleAutocomplete(objInput){
        var parent_div = objInput.closest('td.col_sale');
        console.log('abc');
        objInput.autocomplete({
            source: '<?php echo Yii::app()->createAbsoluteUrl('admin/ajax/search_for_user_login');?>',
            minLength: '<?php echo MIN_LENGTH_AUTOCOMPLETE;?>',
            select: function( event, ui ) {
                objInput.attr('readonly',true);  
                parent_div.find('.sale_id_hide').val(ui.item.id);   
                parent_div.find('.autocomplete_customer_info').remove(); 
                var tableInfo = fnBuildTableSaleInfo(ui.item.code_bussiness, ui.item.name_customer, ui.item.address, ui.item.phone);
                parent_div.append(tableInfo);
                parent_div.find('.autocomplete_customer_info').show();
            }

        });
    }
    
    //NhanDT Sep2019 add table info sale_id 
    function fnBuildTableSaleInfo(info_code_bussiness, info_name, info_address, info_phone){
        var table = '';
        table += '<div class="autocomplete_customer_info table_small" style="">';
        table +=    '<table>';
        table +=        '<tr>';
        table +=            '<td class="_l td_first_t">Mã NV:</td>';
        table +=            '<td class="_r info_code_bussiness td_last_r td_first_t">'+info_code_bussiness+'</td>';
        table +=        '</tr>';

        table +=        '<tr>';
        table +=            '<td class="_l">Tên NV:</td>';
        table +=            '<td class="_r info_name td_last_r">'+info_name+'</td>';
        table +=        '</tr>';
        
        table +=        '<tr>';
        table +=            '<td class="_l">Địa chỉ:</td>';
        table +=            '<td class="_r info_address td_last_r">'+info_address+'</td>';
        table +=        '</tr>';
        
        table +=        '<tr>';
        table +=            '<td class="_l">Điện Thoại:</td>';
        table +=            '<td class="_r info_phone td_last_r">'+info_phone+'</td>';
        table +=        '</tr>';
        table += '</table>';
        table += '</div>';
        table += '<div class="clr"></div>';
        return table;
    }
    
    
    function fnRemoveName(this_){
        var parent_div = $(this_).closest('td.col_sale');
        var tr = $(this_).closest('tr.cash_book_row');
        parent_div.find('.sale_id_autocomplete').attr("readonly",false);                                     
        parent_div.find('.sale_id_autocomplete').val("");             
        parent_div.find('.sale_id_hide').val("");
        parent_div.find('.autocomplete_customer_info').hide();
    }
    
    
</script>
 