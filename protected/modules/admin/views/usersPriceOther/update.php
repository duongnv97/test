<?php
/* @var $this UsersPriceOtherController */
/* @var $model UsersPriceOther */

//echo '<pre>';
//print_r($model->attributes);
//echo '</pre>';
//die;

$this->breadcrumbs=array(
	'Quản Lý '.$this->pluralTitle =>array('index'),
	'Thông tin KH'                =>array('view','id'=>$model->id),
	'Cập nhật '.$this->pluralTitle,
);

$this->menu=array(
	array('label'=>'Quản lý '.$this->singleTitle, 'url'=>array('index')),
	array('label'=>'Tạo mới '.$this->singleTitle, 'url'=>array('create')),
	array('label'=>'Xem '.$this->singleTitle, 'url'=>array('view', 'id'=>$model->id)),
);
?>
<h1>Cập nhật <?php echo $this->singleTitle.': '.$model->getCustomer('code_bussiness') .' - '. $model->getCustomer('first_name')?></h1>

<?php echo $this->renderPartial('_form_update'  , array('model'=>$model)); ?>