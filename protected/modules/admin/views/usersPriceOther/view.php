<?php
$this->breadcrumbs=array(
    'Quản Lý '.$this->pluralTitle => array('index'),
    'Thông Tin ' . $this->singleTitle,
);

$menus = array(
	array('label'=>"Quản Lý $this->pluralTitle", 'url'=>array('index')),
	array('label'=>"Tạo Mới $this->singleTitle", 'url'=>array('create')),
	array('label'=>"Cập Nhật $this->singleTitle", 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>"Xóa $this->singleTitle", 'url'=>array('delete'), 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Bạn chắc chắn muốn xóa ?')),
);
$this->menu= ControllerActionsName::createMenusRoles($menus, $actions);
?>

<h1>Xem <?php echo $this->singleTitle.': '.$model->getCustomer("first_name"); ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
            array(
                'name'  => 'customer_id', 
                'type'  =>'html',
                'value' => $model->getCustomer("code_bussiness") ." - ". $model->getCustomer("first_name"),
            ),
            array(
                'name'  => 'type_customer',
                'value' => $model->getTypeCustomer(),
            ),
            array(
                'name'  => 'Địa chỉ',
                'value' => $model->getAddressCus(),
            ),
            array(
                'name'  => 'sale_id',
                'value' => $model->getSale(),
            ),
            array(
                'name'  => 'materials_id',
                'value' => $model->getMaterials(),
            ),
            array(
                'name'  => 'materials_type_id',
                'value' => $model->getMaterialsType(),
            ),
            array(
                'name'  => 'price_value',
                'value' => $model->getPriceValue()
            )
	),
)); ?>
