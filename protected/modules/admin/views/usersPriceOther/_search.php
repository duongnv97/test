<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=> GasCheck::getCurl(),
	'method'=>'get',
)); ?>

    <div class="row">
        <?php echo $form->labelEx($model,'customer_id'); ?>
        <?php echo $form->hiddenField($model,'customer_id', array('class'=>'')); ?>
        <?php 
            // widget auto complete search user customer and supplier
            $aData = array(
                'model'=>$model,
                'field_customer_id'=>'customer_id',
                'name_relation_user'=>'rCustomer',
                'url' => Yii::app()->createAbsoluteUrl('admin/ajax/search_user_by_code'),
                'ClassAdd' => 'w-500',
            );
            $this->widget('ext.SpaAutocompleteCustomer.SpaAutocompleteCustomer',
                array('data'=>$aData));                                        
        ?>
    </div>
    <div class="row">
        <?php echo $form->label($model,'customer_parent_id'); ?>
        <?php echo $form->hiddenField($model,'customer_parent_id'); ?>
        <?php 
            // widget auto complete search user customer and supplier
            $aData = array(
                'model'=>$model,
                'field_autocomplete_name'=>'file_excel',
                'field_customer_id'=>'customer_parent_id',
                'name_relation_user'=>'rCustomerParent',
                'field_autocomplete_name' => 'autocomplete_name_3',
                'ClassAdd' => 'w-500',
                'url' => Yii::app()->createAbsoluteUrl('admin/ajax/search_user_by_code'),
            );
            $this->widget('ext.SpaAutocompleteCustomer.SpaAutocompleteCustomer',
                array('data'=>$aData));                                        
        ?>
    </div>
    <div class="row">
    <?php echo $form->labelEx($model,'agent_id'); ?>
    <?php echo $form->hiddenField($model,'agent_id', array('class'=>'')); ?>
    <?php
            // 1. limit search kh của sale
            $url = Yii::app()->createAbsoluteUrl('admin/ajax/search_by_role', array('role'=> ROLE_AGENT));
            // widget auto complete search user customer and supplier
            $aData = array(
                'model'=>$model,
                'field_customer_id'=>'agent_id',
                'url'=> $url,
                'name_relation_user'=>'rAgent',
                'ClassAdd' => 'w-500',
                'field_autocomplete_name' => 'autocomplete_name_2',
                'placeholder'=>'Nhập mã hoặc tên đại lý',
            );
            $this->widget('ext.SpaAutocompleteCustomer.SpaAutocompleteCustomer',
                array('data'=>$aData));
            ?>
        <?php echo $form->error($model,'employee_maintain_id'); ?>
    </div>
    <div class="row more_col">
        <div class="col1">
            <?php echo $form->labelEx($model,'Tháng',array('style' => ''));?>
            <?php echo $form->dropDownList($model,'month', ActiveRecord::getMonthVn(),array('class' => 'w-200', 'empty'=>'Select')); ?>
        </div>
        <div class="col2">
            <?php echo $form->labelEx($model,'Năm'); ?>
            <?php echo $form->dropDownList($model,'year', ActiveRecord::getRangeYear(2016, date('Y')+35),array('class' => 'w-200', 'empty'=>'Select')); ?>
        </div>
    </div>
    <div class="row more_col">
        <div class="col1">
            <?php echo $form->label($model,'type_customer', array('label'=>'Loại KH')); ?>
            <?php echo $form->dropDownList($model,'type_customer', CmsFormatter::$CUSTOMER_BO_MOI,array('empty'=>'Select', 'class' => 'w-200')); ?>
        </div>
        <div class="col2">
            <?php echo $form->label($model,'Trạng thái KH'); ?>
            <?php echo $form->dropDownList($model,'customerStatus', Users::$STATUS_LAY_HANG,array('class'=>'w-200', 'empty'=>'Select')); ?>
            <?php echo $form->error($model,'customerStatus'); ?>
	</div>
    </div>
    <div class="row">
        <?php echo $form->labelEx($model,'materials_id'); ?>
        <?php echo $form->hiddenField($model,'materials_id'); ?>		
        <?php 
            // widget auto complete search material
            $aData = array(
                'model'=>$model,
                'field_material_id'=>'materials_id',
                'name_relation_material'=>'rMaterials',
                'field_autocomplete_name'=>'autocomplete_materials',
            );
            $this->widget('ext.GasAutocompleteMaterial.GasAutocompleteMaterial',
                array('data'=>$aData));                                        
        ?>             
        <?php echo $form->error($model,'materials_id'); ?>
    </div>
    <div class="row ">
        <?php echo $form->labelEx($model,'sale_id'); ?>
        <?php echo $form->hiddenField($model,'sale_id', array('class'=>'')); ?>
        <?php 
            // widget auto complete search user customer and supplier
            $aData = array(
                'model'=>$model,
                'field_customer_id'=>'sale_id',
                'name_relation_user'=>'rSale',
                'field_autocomplete_name' => 'autocomplete_name_1',
                'url' => Yii::app()->createAbsoluteUrl('admin/ajax/search_for_user_login'),
                'ClassAdd' => 'w-500',
            );
            $this->widget('ext.SpaAutocompleteCustomer.SpaAutocompleteCustomer',
                array('data'=>$aData));                                        
        ?>
        <?php echo $form->error($model,'sale_id'); ?>
    </div>
    <div class="row ">
        <?php echo $form->labelEx($model,'created_by'); ?>
        <?php echo $form->hiddenField($model,'created_by', array('class'=>'')); ?>
        <?php 
            // widget auto complete search user customer and supplier
            $aData = array(
                'model'=>$model,
                'field_customer_id'=>'created_by',
                'name_relation_user'=>'rCreateBy',
                'field_autocomplete_name' => 'autocomplete_name_4',
                'url' => Yii::app()->createAbsoluteUrl('admin/ajax/search_for_user_login'),
                'ClassAdd' => 'w-500',
            );
            $this->widget('ext.SpaAutocompleteCustomer.SpaAutocompleteCustomer',
                array('data'=>$aData));                                        
        ?>
        <?php echo $form->error($model,'created_by'); ?>
    </div>

    <div class="row buttons" style="padding-left: 159px;">
        <?php $this->widget('bootstrap.widgets.TbButton', array(
        'buttonType'=>'submit',
        'label'=>'Search',
        'type'=>'null', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
        'size'=>'small', // null, 'large', 'small' or 'mini'
        //'htmlOptions' => array('style' => 'margin-bottom: 10px; float: right;'),
        )); ?>	 
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a class='btn_cancel btnReset'>Reset</a>    
        </div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->
<script>
$(function(){
        $('.btnReset').click(function(){
            var div = $(this).closest('form');
            div.find('input').val('');
            div.find('Select').val('');
            div.find('.remove_row_item').trigger('click');
        });
    });
</script>