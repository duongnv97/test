<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'users-price-form',
	'enableAjaxValidation'=>false,
        'htmlOptions' => array('enctype' => 'multipart/form-data'),
)); ?>
    <p class="note">Dòng có dấu <span class="required">*</span> là bắt buộc.</p>
    <?php echo MyFormat::BindNotifyMsg(); ?>
    <table class="materials_table materials_table_root materials_table_th hm_table" style="width:100%;">
        <thead>
            <tr>
                <th class="item_c w-20">#</th>
                <th class="w-300 item_c">Khách Hàng</th>
                <th class="w-300 item_c">NV kinh doanh</th>
                <th class="w-200 item_c">Vật tư</th>
                <th class="w-150 item_c">Ngày áp dụng</th>
                <th class="w-80 item_c">Giá</th>
            </tr>
        </thead>
        <tbody>
        <?php foreach ($model->aModelUpdate as $key=>$mUpdate): ?>
            <tr class="cash_book_row row_input">
                <td class="item_c order_no"><?php echo $key+1;?></td>
                <td class="w-300 info_customer">
                    <div class="row row_sale_autocomplete">
                        <div class="float_l display_none">
                            <input class="customer_id_hide" value="<?php echo $mUpdate->customer_id;?>" type="hidden" name="UsersPriceOther[customer_id]">
                            <input value="<?php echo $mUpdate->id;?>" type="hidden" name="id[]">
                            <input class="float_l sale_autocomplete w-250"  placeholder="Nhập tên số đt, từ <?php echo MIN_LENGTH_AUTOCOMPLETE; ?> ký tự" maxlength="100" value="" type="text" >
                            <span class="remove_row_item" onclick="fnRemoveName(this);"></span>
                        </div>
                        <?php 
                            $mCustomer = $mUpdate->rCustomer;
                            if ($mCustomer){
                                $aData = array(
                                    'class_custom'=>'table_small',
                                    'info_code_bussiness'=>$mCustomer->code_bussiness,
                                    'info_name'=>$mCustomer->first_name,
                                    'info_address'=>$mCustomer->address,
                                    'info_phone'=>$mCustomer->phone,
                                );
                                $this->widget('ext.GasTableCustomerInfo.GasTableCustomerInfo',
                                    array('data'=>$aData));
                            }                                    
                        ?>
                    </div>
                </td>
                <td style="padding: 10px !important; " class="col_sale">
                    <div class="row row_sale_autocomplete">
                        <div class="float_l">
                            <input class="sale_id_hide" value="<?php echo $mUpdate->sale_id;?>" type="hidden" name="UsersPriceOther[sale_id]">
                            <input class="float_l sale_autocomplete w-250"  placeholder="Nhập tên số đt, từ <?php echo MIN_LENGTH_AUTOCOMPLETE; ?> ký tự" maxlength="100" value="" type="text" >
                            <span class="remove_row_item" onclick="fnRemoveName(this);"></span>
                        </div>
                        <?php 
                            $mSale = $mUpdate->rSale;
                            if ($mSale){
                        ?>
                        <div class="autocomplete_customer_info table_small" style="display: block;" >
                            <table>
                                <tr>
                                    <td class="_l td_first_t">Mã NV:</td>
                                    <td class="_r info_code_bussiness td_first_t td_last_r"><?php echo $mSale->code_bussiness;?></td>
                                </tr>
                                <tr>
                                    <td class="_l">Tên NV:</td>
                                    <td class="_r info_name td_last_r"><?php echo $mSale->first_name.' - '.$mSale->getRoleName();?></td>
                                </tr>
                                <tr>
                                    <td class="_l">Địa chỉ:</td>
                                    <td class="_r info_address td_last_r"><?php echo $mSale->address;?></td>
                                </tr>
                                <tr>
                                    <td class="_l">Điện Thoại:</td>
                                    <td class="_r info_phone td_last_r"><?php echo $mSale->phone;?></td>
                                </tr>
                            </table>
                        </div>
                        <div class="clr"></div>
                        <?php }; ?>
                    </div>
                </td>
                <td class="item_c w-150" class="col_material">
                    <input class="materials_type_id_hide" id="materials_type_id" value="<?php echo $mUpdate->materials_type_id;?>" type="hidden" name="UsersPriceOther[materials_type_id]">
                    <div class="row">
                        <?php echo $form->hiddenField($model,'materials_id'); ?>		
                        <?php 
                            // widget auto complete search material
                            $aData = array(
                                'model'                         =>$model,
                                'field_material_id'             =>'materials_id',
                                'name_relation_material'        =>'rMaterials',
                                'field_autocomplete_name'       =>'autocomplete_materials',
                                'width'                         => '150px',
                            );
                            $this->widget('ext.GasAutocompleteMaterial.GasAutocompleteMaterial',
                                array('data'=>$aData));                                        
                        ?>             
                        <?php echo $form->error($model,'materials_id'); ?>
                    </div>
                </td>
                <td class="item_c">
                    <div class="row">
                        <?php 
                            $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                                'model'=>$model,        
                                'attribute'=>'date_apply',
                                'options'=>array(
                                    'showAnim'=>'fold',
                                    'dateFormat'=> ActiveRecord::getDateFormatJquery(),
//                                    'maxDate'=> '0',
                                    'changeMonth' => true,
                                    'changeYear' => true,
                                    'showOn' => 'button',
                                    'buttonImage'=> Yii::app()->theme->baseUrl.'/admin/images/icon_calendar_r.gif',
                                    'buttonImageOnly'=> true,                                
                                ),        
                                'htmlOptions'=>array(
                                    'class'=>'w-150 DateTo',
                                    'size'=>'16',
                                    'readonly'=>'readonly',
                                    'style'=>'float:left;',
                                ),
                            ));
                         ?>   
                    </div>
                </td>
                <td class="item_c">
                    <input name="UsersPriceOther[price_value]" class="w-80 gPriceValue number_only ad_fix_currency" maxlength="9" value="<?php echo ActiveRecord::formatNumberInput($mUpdate->price_value);?>" type="text" placeholder="Giá vật tư">
                </td>
            </tr>
        <?php endforeach; ?>
        </tbody>
    </table>
    <div class="row buttons" style="padding-left: 141px;">
        <?php $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType'=>'submit',
            'label' => 'Save',
            'type'=>'null', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
            'size'=>'small', // null, 'large', 'small' or 'mini'
            //'htmlOptions' => array('style' => 'margin-bottom: 10px; float: right;'),
        )); ?>	
    </div>

<?php $this->endWidget(); ?>

</div><!-- form -->
<script>
    $(document).ready(function(){
        $('.form').find('button:submit').click(function(){
            $.blockUI({ overlayCSS: { backgroundColor: '#fff' } }); 
        });
    });
</script>


<?php Yii::app()->clientScript->registerCoreScript('jquery.ui'); ?>
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/autoNumeric-master/autoNumeric.js"></script>
<script>
    jQuery(document).ready(function(){
        fnBindRemoveIcon();
        gSelectChange(<?php echo UsersRef::PRICE_OTHER;?>);
        fnInitInputCurrency();
    });
    
    $(document).ready(function(){
        $('.form').find('button:submit').live('click', function(){
            $.blockUI({ overlayCSS: { backgroundColor: '#fff' } }); 
        });
        fnBindAllAutocomplete();
        bindEventForHelpNumber();
        fnBindRemoveIcon();
        gSelectChange(<?php echo UsersRef::PRICE_OTHER;?>);
        fnInitInputCurrency();
    });

    function fnBindAllAutocomplete(){
        $('.sale_autocomplete').each(function(){
            fnBindAutocomplete($(this));
        });
    }
    
    //NhanDT autocomplete for Sale_id
    function fnBindAutocomplete(objInput){
        var parent_div = objInput.closest('td.col_sale');
        objInput.autocomplete({
            source: '<?php echo Yii::app()->createAbsoluteUrl('admin/ajax/search_for_user_login');?>',
            minLength: '<?php echo MIN_LENGTH_AUTOCOMPLETE;?>',
            select: function( event, ui ) {
                objInput.attr('readonly',true);  
                parent_div.find('.sale_id_hide').val(ui.item.id);  
                parent_div.find('.autocomplete_customer_info').remove();  
                var tableInfo = fnBuildTableSaleInfo(ui.item.code_bussiness, ui.item.name_customer, ui.item.address, ui.item.phone);
                parent_div.append(tableInfo);
                parent_div.find('.autocomplete_customer_info').show();
            }

        });
    }
    
    //NhanDT add table info sale_id
    function fnBuildTableSaleInfo(info_code_bussiness, info_name, info_address, info_phone){
        var table = '';
        table += '<div class="autocomplete_customer_info table_small" style="">';
        table +=    '<table>';
        table +=        '<tr>';
        table +=            '<td class="_l td_first_t">Mã NV:</td>';
        table +=            '<td class="_r info_code_bussiness td_last_r td_first_t">'+info_code_bussiness+'</td>';
        table +=        '</tr>';

        table +=        '<tr>';
        table +=            '<td class="_l">Tên NV:</td>';
        table +=            '<td class="_r info_name td_last_r">'+info_name+'</td>';
        table +=        '</tr>';
        
        table +=        '<tr>';
        table +=            '<td class="_l">Địa chỉ:</td>';
        table +=            '<td class="_r info_address td_last_r">'+info_address+'</td>';
        table +=        '</tr>';
        
        table +=        '<tr>';
        table +=            '<td class="_l">Điện Thoại:</td>';
        table +=            '<td class="_r info_phone td_last_r">'+info_phone+'</td>';
        table +=        '</tr>';
        table += '</table>';
        table += '</div>';
        table += '<div class="clr"></div';
        return table;
    }
    
    function fnRemoveName(this_){
        var parent_div = $(this_).closest('td.col_sale');
        var tr = $(this_).closest('tr.cash_book_row');
        parent_div.find('.sale_autocomplete').attr("readonly",false);                                     
        parent_div.find('.sale_autocomplete').val("");             
        parent_div.find('.sale_id_hide').val("");
        parent_div.find('.autocomplete_customer_info').hide();
    }
    
    /**
    * @Author: NhanDT Sep 25, 2019
    * @Todo: todo
    * @Param:  param
    */
    function afterMaterialSelect(idField, ui){
        console.log(ui.item.materials_type_id);
        $('#materials_type_id').val(ui.item.materials_type_id);
    }

</script>