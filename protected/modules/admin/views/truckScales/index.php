<?php
$this->breadcrumbs=array(
	$this->singleTitle,
);

$menus=array(
            array('label'=>"Create $this->singleTitle", 'url'=>array('create')),
);
$this->menu= ControllerActionsName::createMenusRoles($menus, $actions);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('truck-scales-grid', {
                url : $(this).attr('action'),
		data: $(this).serialize()
	});
	return false;
});
");

Yii::app()->clientScript->registerScript('ajaxupdate', "
$('#truck-scales-grid a.ajaxupdate').live('click', function() {
    $.fn.yiiGridView.update('truck-scales-grid', {
        type: 'POST',
        url: $(this).attr('href'),
        success: function() {
            $.fn.yiiGridView.update('truck-scales-grid');
        }
    });
    return false;
});
");
?>

<h1>Danh Sách <?php echo $this->pluralTitle; ?></h1>


<?php echo CHtml::link('Tìm Kiếm Nâng Cao','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'truck-scales-grid',
	'dataProvider'=>$model->searchIndex(),
        'afterAjaxUpdate'=>'function(id, data){ fnUpdateColorbox();}',
        'template'=>'{pager}{summary}{items}{pager}{summary}',
//    'itemsCssClass' => 'items custom_here',
//    'htmlOptions'=> array('class'=>'grid-view custom_here'),
        'pager' => array(
            'maxButtonCount'=>  CmsFormatter::$PAGE_MAX_BUTTON,
        ),
	'enableSorting' => false,
	//'filter'=>$model,
	'columns'=>array(
        array(
            'header' => 'S/N',
            'type' => 'raw',
            'value' => '$this->grid->dataProvider->pagination->currentPage * $this->grid->dataProvider->pagination->pageSize + ($row+1)',
            'headerHtmlOptions' => array('width' => '10px','style' => 'text-align:center;'),
            'htmlOptions' => array('style' => 'text-align:center;')
        ),
//		'type',
//		'is_online',
            array(
                'name'=>'code_no',
                'type'=>'raw',
                'value'=>'$data->getCodeNo().$data->getFileOnly()',
            ),
            array(
                'name'=>'type',
                'type'=>'raw',
                'value'=>'$data->getType()',
            ),
            array(
                'name'=>'is_online',
                'type'=>'raw',
                'value'=>'$data->getMode()',
            ),
            array(
                'name'=>'agent_id',
                'type'=>'raw',
                'value'=>'$data->getAgent()',
            ),
            array(
                'name'=>'car_number',
                'type'=>'raw',
                'value'=>'$data->getCarNumber()',
            ),
            array(
                'name'=>'car_owner',
                'type'=>'raw',
                'value'=>'$data->getCarOwner()',
            ),
            array(
                'name'=>'cargo_owner',
                'type'=>'raw',
                'value'=>'$data->getCargoOwner()',
            ),
            array(
                'name'=>'cargo_name',
                'type'=>'raw',
                'value'=>'$data->getCargoName()',
            ),
            array(
                'name'=>'weight_1',
                'type'=>'raw',
                'htmlOptions' => array('style' => 'text-align:right;'),
                'value'=>'$data->getWeight1(true)',
            ),
            array(
                'name'=>'weight_2',
                'type'=>'raw',
                'htmlOptions' => array('style' => 'text-align:right;'),
                'value'=>'$data->getWeight2(true)',
            ),
            array(
                'name'=>'time_1',
                'type'=>'raw',
                'value'=>'$data->getTime1()',
            ),
            array(
                'name'=>'time_2',
                'type'=>'raw',
                'value'=>'$data->getTime2()',
            ),
            array(
                'name'=>'uid_login',
                'type'=>'raw',
                'value'=>'$data->getUidLogin()',
            ),
//            array(
//                'name'=>'created_date',
//                'type'=>'raw',
//                'value'=>'$data->getCreatedDate()',
//            ),
            array(
                'header' => 'Actions',
                'class'=>'CButtonColumn',
                'template'=> ControllerActionsName::createIndexButtonRoles($actions),
                'buttons'=>array(
                    'update' => array(
                        'visible' => '$data->canUpdate()',
                    ),
                    'delete'=>array(
                        'visible'=> 'GasCheck::canDeleteData($data)',
                    ),
                ),
            ),
	),
)); ?>

<script>
$(document).ready(function() {
    fnUpdateColorbox();
});

function fnUpdateColorbox(){   
    fixTargetBlank();
    $(".gallery").colorbox({iframe:true,innerHeight:'1800', innerWidth: '1100',close: "<span title='close'>close</span>"});
}
</script>