<?php
$this->breadcrumbs=array(
    $this->pluralTitle => array('index'),
    ' ' . $this->singleTitle . ' : ',
);

$menus = array(
	array('label'=>"$this->pluralTitle Management", 'url'=>array('index')),
	array('label'=>"Tạo Mới $this->singleTitle", 'url'=>array('create')),
	array('label'=>"Cập Nhật $this->singleTitle", 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>"Xóa $this->singleTitle", 'url'=>array('delete'), 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Bạn chắc chắn muốn xóa ?')),
);
$this->menu= ControllerActionsName::createMenusRoles($menus, $actions);
?>

<h1>Xem: <?php echo $this->singleTitle; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		
            array(
                'name' => 'type',      
                'value' => $model->getType()
            ),
            array(
                'name' => 'is_online',      
                'value' => $model->getMode()
            ),
            array(
                'name' => 'code_no',      
                'value' => $model->getCodeNo()
            ),
            array(
                'name' => 'car_number',      
                'value' => $model->getCarNumber()
            ),
            array(
                'name' => 'car_owner',      
                'value' => $model->getCarOwner()
            ),
            array(
                'name' => 'cargo_owner',      
                'value' => $model->getCargoOwner()
            ),
            array(
                'name' => 'cargo_name',      
                'value' => $model->getCargoName()
            ),
            array(
                'name' => 'weight_1',      
                'value' => $model->getWeight1(true)
            ),
            array(
                'name' => 'weight_2',      
                'value' => $model->getWeight2(true)
            ),
            array(
                'name' => 'time_1',      
                'value' => $model->getTime1()
            ),
             array(
                'name' => 'time_2',      
                'value' => $model->getTime2()
            ),
             array(
                'name' => 'uid_login',      
                'value' => $model->getUidLogin()
            ),
             array(
                'name' => 'created_date',      
                'value' => $model->getCreatedDate()
            ),
		
	),
)); ?>
