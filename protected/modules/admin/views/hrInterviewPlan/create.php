<?php
$this->breadcrumbs=array(
	$this->pluralTitle=>array('hrInterviewPlan/index/plan_id/'.$model->plan_id),
	'Tạo Mới',
);

$menus = array(		
        array('label'=>"$this->singleTitle Management", 'url'=>array('index','plan_id'=>$model->plan_id)),
);
$this->menu= ControllerActionsName::createMenusRoles($menus, $actions);
?>

<h1>Tạo Mới <?php echo $this->singleTitle; ?></h1>

<?php
//remove plan_id when load form create
$model->removePlanIdUrl();
echo $this->renderPartial('_form', array('model'=>$model)); 
?>