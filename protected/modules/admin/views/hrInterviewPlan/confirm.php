<?php
$this->breadcrumbs = array(
	$this->pluralTitle => array('index'),
        $model->id=>array('view','id'=>$model->id),
	'Cập Nhật ' . $this->singleTitle,
);

$menus = array(	
    array('label' => $this->pluralTitle, 'url' => array('index')),
    array('label' => 'Xem ' . $this->singleTitle, 'url' => array('view', 'id' => $model->id)),	
    array('label' => 'Tạo Mới ' . $this->singleTitle, 'url' => array('create')),
);
$this->menu= ControllerActionsName::createMenusRoles($menus, $actions);

?>

<h1>Xác nhận lịch phỏng vấn: <?php echo $this->singleTitle; ?></h1>
<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
//		'id',
		'code_no',
		array(
                   'name'=>'plan_id',
                   'value'=> isset($model->rPlan) ? $model->rPlan->code_no : '',
                ),
		array(
                   'name'=>'user_id',
                   'value'=> isset($model->rUser) ? $model->rUser->first_name : '',
                ),
		array(
                   'name'=>'candidate_id',
                   'value'=> isset($model->rCandidate) ? $model->rCandidate->first_name : '',
                ),
                array(
                    'label' => 'Vị trí ứng tuyển',
                    'type'=>'RoleNameUser',
                    'value' => $model->role_id,
                    //'htmlOptions' => array('style' => 'text-align:center;')
                ),
//		'interview_date',
//		'score',
//		'interview_date_next',
//                array(
//                    'name' => 'interview_date_next',
//                    'value' => isset($model->interview_date_next) ? $model->interview_date_next: '',
//                    'visible' => isset($model->interview_date_next),
//                ),
		'note',
//		array(
//                   'name'=>'created_by',
//                   'value'=> isset($model->rCreatedBy) ? $model->rCreatedBy->first_name : '',
//                ),
//		'created_date',
//		array(
//                   'name'=>'status',
//                   'type'=>'Status',
////                    'visible' => CommonProcess::isUserAdmin(),
//                ),
	),
)); ?>
<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'hr-interview-plan-form',
	'enableAjaxValidation'=>false,
        'htmlOptions' => array('enctype' => 'multipart/form-data'),
)); ?>

    <p class="note">Dòng có dấu <span class="required">*</span> là bắt buộc.</p>
    <?php // echo MyFormat::BindNotifyMsg(); ?>

	<?php echo $form->errorSummary($model); ?>
    <div class="row" style="display: none;">
        <?php echo $form->labelEx($model,'plan_id'); ?>
        <?php echo $form->dropDownList($model, 'plan_id', HrPlan::getListHrPlan($model,array(HrPlan::STA_APPROVED),true), array('readonly' => true)); ?>
        <?php echo $form->error($model,'plan_id'); ?>
    </div>
<!--    <div class="row">
        <?php echo $form->labelEx($model,'status'); ?>
        <?php echo $form->textField($model,'status'); ?>
        <?php echo $form->error($model,'status'); ?>
    </div>-->
    <div class="row" style="display: none;">
        <?php echo $form->labelEx($model,'user_id'); ?>
        <?php echo $form->hiddenField($model,'user_id', array('class'=>'', 'readonly' => true)); ?>
        <?php
        // 1. limit search kh của sale
        $url = Yii::app()->createAbsoluteUrl('admin/ajax/search_by_role');
        // widget auto complete search user customer and supplier
        $aData = array(
            'model'=>$model,
            'field_customer_id'=>'user_id',
            'url'=> $url,
            'name_relation_user'=>'rUser',
            'ClassAdd' => 'w-300',
            'field_autocomplete_name' => 'autocomplete_name_user',
            'placeholder'=>'Nhập mã hoặc tên người phỏng vấn',
        );
        $this->widget('ext.SpaAutocompleteCustomer.SpaAutocompleteCustomer',
            array('data'=>$aData));
        ?>
        <?php echo $form->error($model,'user_id'); ?>
    </div>
    <div class="row" style="display: none;">
        <?php echo $form->labelEx($model,'candidate_id'); ?>
        <?php echo $form->hiddenField($model,'candidate_id', array('class'=>'')); ?>
        <?php
        // 1. limit search kh của sale
        $url = Yii::app()->createAbsoluteUrl('admin/ajax/search_by_role', array('role'=> ROLE_CANDIDATE));
        // widget auto complete search user customer and supplier
        $aData = array(
            'model'=>$model,
            'field_customer_id'=>'candidate_id',
            'url'=> $url,
            'name_relation_user'=>'rCandidate',
            'ClassAdd' => 'w-300',
            'field_autocomplete_name' => 'autocomplete_name_candidate',
            'placeholder'=>'Nhập mã hoặc tên ứng viên',
        );
        $this->widget('ext.SpaAutocompleteCustomer.SpaAutocompleteCustomer',
            array('data'=>$aData));
        ?>
        <?php echo $form->error($model,'candidate_id'); ?>
    </div>
    <div class="row" style="display: none;">
        <?php echo $form->labelEx($model,'role_id'); ?>
        <?php echo $form->dropDownList($model,'role_id',Roles::loadItems(),array('class'=>'role_id' ,'style'=>'width:450px', 'readonly' => true)); ?>
        <?php echo $form->error($model,'role_id'); ?>
    </div>
    <div class="row">
        <?php // echo $form->labelEx($model,'interview_date'); ?>
        <label for="HrInterviewPlan_interview_date" class="required">Thay đổi ngày phỏng vấn <span class="required">*</span></label>
        <!--<label for="interview_date">Thay đổi ngày phỏng vấn:</label>-->
        <?php
            if ($model->isNewRecord) {
                $date = CommonProcess::getCurrentDateTime(DomainConst::DATE_FORMAT_3);
            } else {
                $date = CommonProcess::convertDateTime($model->interview_date,
                        DomainConst::DATE_FORMAT_4,
                        DomainConst::DATE_FORMAT_3);
            }
            $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                'model'     => $model,
                'attribute' => 'interview_date',
                'options'   => array(
                    'showAnim'      => 'fold',
                    'dateFormat'    => 'dd/mm/yy',
//                        'maxDate'       => '0',
                    'changeMonth'   => true,
                    'changeYear'    => true,
                    'showOn'        => 'button',
                    'buttonImage'   => Yii::app()->theme->baseUrl . '/admin/images/icon_calendar_r.gif',
                    'buttonImageOnly' => true,
                ),
                'htmlOptions'=>array(
                            'class'=>'w-16',
//                                'style'=>'height:20px;width:166px;',
                            'readonly'=>'readonly',
                            'value' => $date,
                        ),
            ));
            ?>
        <?php echo $form->error($model,'interview_date'); ?>
    </div>
<!--    <div class="row">
        <?php echo $form->labelEx($model,'score'); ?>
        <?php echo $form->textField($model,'score'); ?>
        <?php echo $form->error($model,'score'); ?>
    </div>-->
<!--    <div class="row">
        <?php echo $form->labelEx($model,'interview_date_next'); ?>
        <?php        
            $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                'model'     => $model,
                'attribute' => 'interview_date_next',
                'options'   => array(
                    'showAnim'      => 'fold',
                    'dateFormat'    => 'dd/mm/yy',
//                        'maxDate'       => '0',
                    'changeMonth'   => true,
                    'changeYear'    => true,
                    'showOn'        => 'button',
                    'buttonImage'   => Yii::app()->theme->baseUrl . '/admin/images/icon_calendar_r.gif',
                    'buttonImageOnly' => true,
                ),
                'htmlOptions'=>array(
                            'class'=>'w-16',
//                                'style'=>'height:20px;width:166px;',
                            'readonly'=>'readonly',
                            'value' => $model->isNewRecord ? date('d/m/Y')
                                    : CommonProcess::convertDateTimeWithFormat($model->interview_date_next),
                        ),
            ));
            ?>
        <?php echo $form->error($model,'interview_date_next'); ?>
    </div>-->
    <div class="row" style="display: none;">
        <?php echo $form->labelEx($model,'note'); ?>
        <?php echo $form->textArea($model,'note',array('rows'=>6, 'cols'=>50, 'readonly' => true)); ?>
        <?php echo $form->error($model,'note'); ?>
    </div>
<!--    <div class="row">
        <?php echo $form->labelEx($model,'created_by'); ?>
        <?php echo $form->textField($model,'created_by',array('size'=>11,'maxlength'=>11)); ?>
        <?php echo $form->error($model,'created_by'); ?>
    </div>-->
    <div class="row" style="display: none">
        <?php echo $form->labelEx($model,'created_date'); ?>
        <?php
                echo $form->textField(
                        $model, 'created_date', array(
                            'value' => date('Y-m-d H:i:s'),
                            'readonly' => 'true',
                        )
                );
                ?>
        <?php echo $form->error($model,'created_date'); ?>
    </div>
	<div class="row buttons" style="padding-left: 141px;">
		        <?php $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType'=>'submit',
            'label'=>$model->isNewRecord ? 'Create' :'Xác nhận',
            'type'=>'null', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
            'size'=>'small', // null, 'large', 'small' or 'mini'
            //'htmlOptions' => array('style' => 'margin-bottom: 10px; float: right;'),
        )); ?>	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->
<script>
    $(document).ready(function(){
        $('.form').find('button:submit').click(function(){
            $.blockUI({ overlayCSS: { backgroundColor: '#fff' } }); 
        });
        
    });
</script>