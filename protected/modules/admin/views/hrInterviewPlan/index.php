<?php
$this->breadcrumbs=array(
	$this->singleTitle,
);

$menus=array(
            array('label'=>"Create $this->singleTitle", 'url'=>array('create','plan_id'=>$model->plan_id)),
);
$this->menu= ControllerActionsName::createMenusRoles($menus, $actions);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('hr-interview-plan-grid', {
                url : $(this).attr('action'),
		data: $(this).serialize()
	});
	return false;
});
");

Yii::app()->clientScript->registerScript('ajaxupdate', "
$('#hr-interview-plan-grid a.ajaxupdate').live('click', function() {
    $.fn.yiiGridView.update('hr-interview-plan-grid', {
        type: 'POST',
        url: $(this).attr('href'),
        success: function() {
            $.fn.yiiGridView.update('hr-interview-plan-grid');
        }
    });
    return false;
});
");
?>

<h1>Danh Sách <?php echo $this->pluralTitle; ?></h1>


<?php echo CHtml::link('Tìm Kiếm Nâng Cao','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'hr-interview-plan-grid',
	'dataProvider'=>$model->search(),
        'afterAjaxUpdate'=>'function(id, data){ fnUpdateColorbox();}',
        'template'=>'{pager}{summary}{items}{pager}{summary}',
//    'itemsCssClass' => 'items custom_here',
//    'htmlOptions'=> array('class'=>'grid-view custom_here'),
        'pager' => array(
            'maxButtonCount'=>  CmsFormatter::$PAGE_MAX_BUTTON,
        ),
	'enableSorting' => false,
	//'filter'=>$model,
	'columns'=>array(
        array(
            'header' => 'S/N',
            'type' => 'raw',
            'value' => '$this->grid->dataProvider->pagination->currentPage * $this->grid->dataProvider->pagination->pageSize + ($row+1)',
            'headerHtmlOptions' => array('width' => '10px','style' => 'text-align:center;'),
            'htmlOptions' => array('style' => 'text-align:center;')
        ),
		'code_no',
//		'plan_id',
		array(
                    'name'=>'plan_id',
                    'htmlOptions' => array('style' => 'text-align:center;'),
                    'value'=> 'isset($data->rPlan) ? $data->rPlan->code_no : ""',
                ),
		array(
                    'name'=>'user_id',
                    'htmlOptions' => array('style' => 'text-align:center;'),
                    'value'=> 'isset($data->rUser) ? $data->rUser->first_name : ""',
                ),
		array(
                    'name'=>'candidate_id',
                    'htmlOptions' => array('style' => 'text-align:center;'),
                    'value'=> 'isset($data->rCandidate) ? $data->rCandidate->first_name : ""',
                ),
//		'role_id',
                array(
                    'header' => 'Vị trí ứng tuyển',
                    'name' => 'role_id',
                    'type'=>'RoleNameUser',                
                    'value'=>'$data->role_id',   
    //                'htmlOptions' => array('style' => 'width:100px;')
                ),
		'interview_date',
		'interview_date_next',
		'score',
		'note',
		array(
                    'name'=>'created_by',
                    'htmlOptions' => array('style' => 'text-align:center;'),
                    'value'=> 'isset($data->rCreatedBy) ? $data->rCreatedBy->first_name : ""',
                ),
		'created_date',
                array(
                    'name'=>'status',
//                    'type'=>'status',
//                    'value'=>'array("status"=>$data->status,"id"=>$data->id)',
                    'value' => 'HrInterviewPlan::getStatus()[$data->status]',
                    'htmlOptions' => array('style' => 'text-align:center;')
                ),
		array(
                    'header' => 'Actions',
                    'class'=>'CButtonColumn',
                    'template'=> ControllerActionsName::createIndexButtonRoles($actions),
                    'buttons'=>array(
                        'update' => array(
                            'visible' => '$data->canUpdate()',
                        ),
                        'delete'=>array(
                            'visible'=> 'GasCheck::canDeleteData($data)',
                        ),
                    ),
		),
	),
)); ?>

<script>
$(document).ready(function() {
    fnUpdateColorbox();
});

function fnUpdateColorbox(){   
    fixTargetBlank();
}
</script>