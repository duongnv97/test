<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'hr-interview-plan-form',
	'enableAjaxValidation'=>false,
        'htmlOptions' => array('enctype' => 'multipart/form-data'),
)); ?>

    <p class="note">Dòng có dấu <span class="required">*</span> là bắt buộc.</p>
    <?php // echo MyFormat::BindNotifyMsg(); ?>

	<?php echo $form->errorSummary($model); ?>
    
        		
        
<!--    <div class="row">
        <?php echo $form->labelEx($model,'code_no'); ?>
        <?php echo $form->textField($model,'code_no',array('size'=>20,'maxlength'=>20)); ?>
        <?php echo $form->error($model,'code_no'); ?>
    </div>-->
    <div class="row">
        <?php echo $form->labelEx($model,'plan_id'); ?>
        <?php echo $form->dropDownList($model, 'plan_id', HrPlan::getListHrPlan($model,array(HrPlan::STA_APPROVED),true)); ?>
        <?php echo $form->error($model,'plan_id'); ?>
    </div>
<!--    <div class="row">
        <?php echo $form->labelEx($model,'status'); ?>
        <?php echo $form->textField($model,'status'); ?>
        <?php echo $form->error($model,'status'); ?>
    </div>-->
    <div class="row">
        <?php echo $form->labelEx($model, 'user_id'); ?>
        <?php echo $form->dropDownList($model, 'user_id',
            HrInterviewPlan::getListInterviewer()); ?>
        <?php echo $form->error($model, 'user_id'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model,'candidate_id'); ?>
        <?php echo $form->hiddenField($model,'candidate_id', array('class'=>'')); ?>
        <?php
        // 1. limit search kh của sale
        $url = Yii::app()->createAbsoluteUrl('admin/ajax/search_by_role', array('role'=> ROLE_CANDIDATE));
        // widget auto complete search user customer and supplier
        $aData = array(
            'model'=>$model,
            'field_customer_id'=>'candidate_id',
            'url'=> $url,
            'name_relation_user'=>'rCandidate',
            'ClassAdd' => 'w-300',
            'field_autocomplete_name' => 'autocomplete_name_candidate',
            'placeholder'=>'Nhập mã hoặc tên ứng viên',
        );
        $this->widget('ext.SpaAutocompleteCustomer.SpaAutocompleteCustomer',
            array('data'=>$aData));
        ?>
        <?php echo $form->error($model,'candidate_id'); ?>
    </div>
    <div class="row">
        <?php echo $form->labelEx($model,'role_id'); ?>
        <?php echo $form->dropDownList($model,'role_id',$model->getListRole()); ?>
        <?php echo $form->error($model,'role_id'); ?>
    </div>
    <div class="row">
        <?php echo $form->labelEx($model,'interview_date'); ?>
        <?php
            if ($model->isNewRecord) {
                $date = CommonProcess::getCurrentDateTime(DomainConst::DATE_FORMAT_3);
            } else {
                $date = CommonProcess::convertDateTime($model->interview_date,
                        DomainConst::DATE_FORMAT_4,
                        DomainConst::DATE_FORMAT_3);
            }
            $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                'model'     => $model,
                'attribute' => 'interview_date',
                'options'   => array(
                    'showAnim'      => 'fold',
                    'dateFormat'    => 'dd/mm/yy',
//                        'maxDate'       => '0',
                    'changeMonth'   => true,
                    'changeYear'    => true,
                    'showOn'        => 'button',
                    'buttonImage'   => Yii::app()->theme->baseUrl . '/admin/images/icon_calendar_r.gif',
                    'buttonImageOnly' => true,
                ),
                'htmlOptions'=>array(
                            'class'=>'w-16',
//                                'style'=>'height:20px;width:166px;',
                            'readonly'=>'readonly',
                            'value' => $date,
                        ),
            ));
            ?>
        <?php echo $form->error($model,'interview_date'); ?>
    </div>
<!--    <div class="row">
        <?php echo $form->labelEx($model,'score'); ?>
        <?php echo $form->textField($model,'score'); ?>
        <?php echo $form->error($model,'score'); ?>
    </div>-->
<!--    <div class="row">
        <?php echo $form->labelEx($model,'interview_date_next'); ?>
        <?php        
            $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                'model'     => $model,
                'attribute' => 'interview_date_next',
                'options'   => array(
                    'showAnim'      => 'fold',
                    'dateFormat'    => 'dd/mm/yy',
//                        'maxDate'       => '0',
                    'changeMonth'   => true,
                    'changeYear'    => true,
                    'showOn'        => 'button',
                    'buttonImage'   => Yii::app()->theme->baseUrl . '/admin/images/icon_calendar_r.gif',
                    'buttonImageOnly' => true,
                ),
                'htmlOptions'=>array(
                            'class'=>'w-16',
//                                'style'=>'height:20px;width:166px;',
                            'readonly'=>'readonly',
                            'value' => $model->isNewRecord ? date('d/m/Y')
                                    : CommonProcess::convertDateTimeWithFormat($model->interview_date_next),
                        ),
            ));
            ?>
        <?php echo $form->error($model,'interview_date_next'); ?>
    </div>-->
    <div class="row">
        <?php echo $form->labelEx($model,'note'); ?>
        <?php echo $form->textArea($model,'note',array('rows'=>6, 'cols'=>50)); ?>
        <?php echo $form->error($model,'note'); ?>
    </div>
<!--    <div class="row">
        <?php echo $form->labelEx($model,'created_by'); ?>
        <?php echo $form->textField($model,'created_by',array('size'=>11,'maxlength'=>11)); ?>
        <?php echo $form->error($model,'created_by'); ?>
    </div>-->
    <div class="row" style="display: none">
        <?php echo $form->labelEx($model,'created_date'); ?>
        <?php
                echo $form->textField(
                        $model, 'created_date', array(
                            'value' => date('Y-m-d H:i:s'),
                            'readonly' => 'true',
                        )
                );
                ?>
        <?php echo $form->error($model,'created_date'); ?>
    </div>
	<div class="row buttons" style="padding-left: 141px;">
		        <?php $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType'=>'submit',
            'label'=>$model->isNewRecord ? 'Create' :'Save',
            'type'=>'null', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
            'size'=>'small', // null, 'large', 'small' or 'mini'
            //'htmlOptions' => array('style' => 'margin-bottom: 10px; float: right;'),
        )); ?>	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->
<script>
    $(document).ready(function(){
        $('.form').find('button:submit').click(function(){
            $.blockUI({ overlayCSS: { backgroundColor: '#fff' } }); 
        });
        
    });
</script>