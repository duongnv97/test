<?php
$this->breadcrumbs=array(
    $this->pluralTitle => array('index'),
    ' ' . $this->singleTitle . ' : ',
);

$menus = array(
	array('label'=>"$this->pluralTitle Management", 'url'=>array('index','plan_id'=>$model->plan_id)),
	array('label'=>"Tạo Mới $this->singleTitle", 'url'=>array('create','plan_id'=>$model->plan_id)),
	array('label'=>"Cập Nhật $this->singleTitle", 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>"Xóa $this->singleTitle", 'url'=>array('delete'), 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Bạn chắc chắn muốn xóa ?')),
);
$this->menu= ControllerActionsName::createMenusRoles($menus, $actions);
?>

<h1>Xem: <?php echo $this->singleTitle; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
//		'id',
		'code_no',
		array(
                   'name'=>'plan_id',
                   'value'=> isset($model->rPlan) ? $model->rPlan->code_no : '',
                ),
		array(
                   'name'=>'user_id',
                   'value'=> isset($model->rUser) ? $model->rUser->first_name : '',
                ),
		array(
                   'name'=>'candidate_id',
                   'value'=> isset($model->rCandidate) ? $model->rCandidate->first_name : '',
                ),
                array(
                    'label' => 'Vị trí ứng tuyển',
                    'type'=>'RoleNameUser',
                    'value' => $model->role_id,
                    //'htmlOptions' => array('style' => 'text-align:center;')
                ),
                array(
                    'name' => 'interview_date',
                    'value' => CommonProcess::convertDateTime($model->interview_date,
                                DomainConst::DATE_FORMAT_4,
                                DomainConst::DATE_FORMAT_3),
                ),
		'score',
//		'interview_date_next',
                array(
                    'name' => 'interview_date_next',
                    'value' => isset($model->interview_date_next) ? $model->interview_date_next: '',
                    'visible' => isset($model->interview_date_next),
                ),
		'note',
		array(
                   'name'=>'created_by',
                   'value'=> isset($model->rCreatedBy) ? $model->rCreatedBy->first_name : '',
                ),
		'created_date',
		array(
                   'name'=>'status',
//                   'type'=>'Status',
//                    'visible' => CommonProcess::isUserAdmin(),
                    'value' => HrInterviewPlan::getStatus()[$model->status],
                ),
	),
)); ?>

<div id="control">
    <div class="portlet" id="yw2">
        <div class="portlet-content">
            <ul class="operations" id="yw3">
                <li class="create" title="Thêm kết quả">
                    <a href="<?php echo Yii::app()->createAbsoluteUrl('admin/hrInterviewPlanDetail/create?interview_id=' . $model->id);?>">
                        <span>Thêm kết quả</span>
                    </a>
                </li>
            </ul>
        </div>
    </div>
</div>
<h1><?php echo 'Kết quả phỏng vấn:'; ?></h1>

<?php
$this->widget('zii.widgets.grid.CGridView', array(
    'id' => 'detail_info',
    'dataProvider' => $detail_info,
    'columns' => array(
        array(
            'header' => 'STT',
            'type' => 'raw',
            'value' => '$this->grid->dataProvider->pagination->currentPage * $this->grid->dataProvider->pagination->pageSize + ($row+1)',
            'headerHtmlOptions' => array('width' => '30px', 'style' => 'text-align:center;'),
            'htmlOptions' => array('style' => 'text-align:center;')
        ),
        array(
            'name' => 'Ngày giờ',
            'htmlOptions' => array('style' => 'text-align:center;'),
            'value' => '$data->created_date',
        ),
        array(
            'name' => 'Nhận xét',
            'htmlOptions' => array('style' => 'text-align:center;'),
            'value' => 'isset($data->note) ? $data->note : ""',
        ),
    ),
));
?>
