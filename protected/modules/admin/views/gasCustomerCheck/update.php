<?php
$customer_name = $model->rCustomer->first_name;
$this->breadcrumbs=array(
	'Danh sách khách hàng đã kiểm tra'=>array('index'),
	$customer_name=>array('view','id'=>$model->id),
	'Cập Nhật',
);

$menus = array(	
        array('label'=>'Danh sách khách hàng đã kiểm tra', 'url'=>array('index')),
	array('label'=>'Xem Danh sách khách hàng đã kiểm tra', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Tạo Mới Danh sách khách hàng đã kiểm tra', 'url'=>array('create')),	
);
$this->menu= ControllerActionsName::createMenusRoles($menus, $actions);

?>

<h1>Cập nhật khách hàng đã kiểm tra: <?php echo $customer_name; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>