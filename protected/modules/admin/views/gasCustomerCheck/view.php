<?php
$customer_name = $model->rCustomer->first_name;
$this->breadcrumbs=array(
	'Danh sách khách hàng đã kiểm tra'=>array('index'),
	$customer_name,
);

$menus = array(
	array('label'=>'Danh sách khách hàng đã kiểm tra', 'url'=>array('index')),
	array('label'=>'Tạo Mới Danh sách khách hàng đã kiểm tra', 'url'=>array('create')),
	array('label'=>'Cập Nhật khách hàng đã kiểm tra', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Xóa khách hàng đã kiểm tra', 'url'=>array('delete'), 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Bạn chắc chắn muốn xóa ?')),
);
//$this->menu= ControllerActionsName::createMenusRoles($menus, $actions);
?>

<h1>Xem báo cáo kiểm tra khách hàng: <?php echo $customer_name;?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(		
		'code_no',
        array(
            'name' => 'customer_id',
            'type' => 'NameUser',
            'value' => $model->rCustomer,
        ),
		array(
            'label' => 'Địa Chỉ',
            'value' => Users::GetAddressUser($model->rCustomer),
        ),
        array(
            'name' => 'agent_id',
            'type' => 'CustomerCheckNameUser',
            'value' => $model,
        ),
        array(
            'label' => 'Sale',
            'type' => 'OnlyNameUser',
            'value' => $model->rSale,
        ),
        array(
            'name' => 'report_date',
            'type' => 'date',
        ),
        array(
            'label' => 'Chi Tiết',
            'type' => 'html',
            'value' => GasCustomerCheck::GetTableInfo($model),
        ),
        array(
            'name' => 'money_get',
            'type' => 'Currency',
        ),
        array(
            'name' => 'rate_bao_tri',
            'value' => GasCustomerCheck::$ARR_RATE[$model->rate_bao_tri],
        ),
        array(
            'name' => 'rate_dieu_phoi',
            'value' => GasCustomerCheck::$ARR_RATE[$model->rate_dieu_phoi],
        ),
        array(
            'name' => 'rate_giao_nhan',
            'value' => GasCustomerCheck::$ARR_RATE[$model->rate_giao_nhan],
        ),
        array(
            'name' => 'rate_kinh_doanh',
            'value' => GasCustomerCheck::$ARR_RATE[$model->rate_kinh_doanh],
        ),
        array(
            'name' => 'report_handle',
            'type' => 'html',
            'value' => nl2br($model->report_handle),
        ),
		array(
            'name' => 'created_date',
            'type' => 'Datetime',
        ),
        array(
            'name' => 'last_accounting_edit',
            'type' => 'OnlyNameUser',
            'value' => $model->rAccountingEdit,
        ),
        array(
            'name' => 'last_accounting_edit_date',
            'type' => 'Datetime',
        ),
        
        array(
            'name' => 'last_monitor_edit',
            'type' => 'OnlyNameUser',
            'value' => $model->rMonitorgEdit,
        ),
        array(
            'name' => 'last_monitor_edit_date',
            'type' => 'Datetime',
        ),
        
	),
)); ?>
<br>
<img src="<?php echo ImageProcessing::bindImageByModel($model,'','',array('size'=>'size1'));?>">
<script>
    $(window).load(function(){
//        $('.materials_table').floatThead();  // không dùng dc cho popup
        fnResizeColorbox();
    });
    
    function fnResizeColorbox(){
//        var y = $('body').height()+100;
        var y = $('#main_box').height()+100;
        parent.$.colorbox.resize({innerHeight:y});        
    }
      
    
</script>