<div class="row">
    <?php echo Yii::t('translation', $form->labelEx($model,'report_date')); ?>
            <?php 
                $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                    'model'=>$model,        
                    'attribute'=>'report_date',
                    'options'=>array(
                        'showAnim'=>'fold',
                        'dateFormat'=> ActiveRecord::getDateFormatJquery(),
//                            'minDate'=> '0',
                        'maxDate'=> '0',
                        'changeMonth' => true,
                        'changeYear' => true,
                        'showOn' => 'button',
                        'buttonImage'=> Yii::app()->theme->baseUrl.'/admin/images/icon_calendar_r.gif',
                        'buttonImageOnly'=> true,                                
                    ),        
                    'htmlOptions'=>array(
                        'class'=>'w-16',
                        'style'=>'height:20px;',
                            'readonly'=>'readonly',
                    ),
                ));
            ?>     		
    <?php echo $form->error($model,'report_date'); ?>
</div>

<div class="row">
    <?php echo $form->labelEx($model,'rate_bao_tri', array('label'=>'Ý Kiến Khách Hàng')); ?>
</div>
<div class="clr"></div>
<div class="row radio-list-2">
    <?php echo $form->labelEx($model,'rate_bao_tri'); ?>
    <?php echo $form->radioButtonList($model,'rate_bao_tri', GasCustomerCheck::$ARR_RATE, 
            array('separator'=>"",'class'=>'' )); ?>  
    <?php echo $form->error($model,'rate_bao_tri'); ?>
</div>
<div class="clr"></div>

<div class="row radio-list-2">
    <?php echo $form->labelEx($model,'rate_dieu_phoi'); ?>
    <?php echo $form->radioButtonList($model,'rate_dieu_phoi', GasCustomerCheck::$ARR_RATE, 
            array('separator'=>"",'class'=>'' )); ?>  
    <?php echo $form->error($model,'rate_dieu_phoi'); ?>
</div>
<div class="clr"></div>

<div class="row radio-list-2">
    <?php echo $form->labelEx($model,'rate_giao_nhan'); ?>
    <?php echo $form->radioButtonList($model,'rate_giao_nhan', GasCustomerCheck::$ARR_RATE, 
            array('separator'=>"",'class'=>'' )); ?>  
    <?php echo $form->error($model,'rate_giao_nhan'); ?>
</div>
<div class="clr"></div>

<div class="row radio-list-2">
    <?php echo $form->labelEx($model,'rate_kinh_doanh'); ?>
    <?php echo $form->radioButtonList($model,'rate_kinh_doanh', GasCustomerCheck::$ARR_RATE, 
            array('separator'=>"",'class'=>'' )); ?>  
    <?php echo $form->error($model,'rate_kinh_doanh'); ?>
</div>
<div class="clr"></div>

<div class="row">
    <?php echo $form->labelEx($model,'money_get'); ?>
    <div class="fix_number_help">
        <?php echo $form->textField($model,'money_get', array('class'=>'number_only_v1 number_only')); ?>
        <div class="help_number"></div>
    </div>
    <?php echo $form->error($model,'money_get'); ?>
</div>
<div class="clr"></div>

<div class="row">
        <?php echo $form->labelEx($model,'report_handle'); ?>
        <?php echo $form->textArea($model,'report_handle',array('rows'=>5,'cols'=>80,"placeholder"=>"")); ?>
        <?php echo $form->error($model,'report_handle'); ?>
</div>

<div class="row">
    <?php echo $form->labelEx($model,'file_report'); ?>
    <!--<div style="padding-left: 140px;">-->
        <?php echo $form->fileField($model,'file_report'); ?>
    <span class="note">Cho phép định dạng <?php echo GasSalesFileScanDetai::$AllowFile;?></span>
    <!--</div>-->
    <?php if(!empty($model->file_report)): ?>
    <div class="l_padding_200">
        <br> <?php echo $cmsFormater->formatCustomerCheckFile($model);?>
    </div>
    <?php endif;?>
    <?php echo $form->error($model,'file_report'); ?>
</div>
<div class="clr"></div>