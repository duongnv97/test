<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'gas-orders-form',
	'enableAjaxValidation'=>false,
        'htmlOptions' => array('enctype' => 'multipart/form-data'),
)); 
$cmsFormater = new CmsFormatter();
?>

	<h1>Cập nhật báo cáo kiểm tra tháng <?php echo $model->month." - $model->year";?> khách hàng: <?php echo $model->customer_name;?>
        </h1> 
        <?php echo MyFormat::BindNotifyMsg(); ?>
        
        <div class="row">
            <?php echo $form->labelEx($model,'vo_inventory_50'); ?>
            <?php echo $form->textField($model,'vo_inventory_50',array('class'=>'w-300')); ?>
            <?php echo $form->error($model,'vo_inventory_50'); ?>
        </div>
        <div class="row">
            <?php echo $form->labelEx($model,'vo_inventory_45'); ?>
            <?php echo $form->textField($model,'vo_inventory_45',array('class'=>'w-300')); ?>
            <?php echo $form->error($model,'vo_inventory_45'); ?>
        </div>
        <div class="row">
            <?php echo $form->labelEx($model,'vo_inventory_12'); ?>
            <?php echo $form->textField($model,'vo_inventory_12',array('class'=>'w-300')); ?>
            <?php echo $form->error($model,'vo_inventory_12'); ?>
        </div>
        <div class="row">
            <?php echo $form->labelEx($model,'vo_da_thu_50'); ?>
            <?php echo $form->textField($model,'vo_da_thu_50',array('class'=>'w-300')); ?>
            <?php echo $form->error($model,'vo_da_thu_50'); ?>
        </div>
        <div class="row">
            <?php echo $form->labelEx($model,'vo_da_thu_45'); ?>
            <?php echo $form->textField($model,'vo_da_thu_45',array('class'=>'w-300')); ?>
            <?php echo $form->error($model,'vo_da_thu_45'); ?>
        </div>
        <div class="row">
            <?php echo $form->labelEx($model,'vo_da_thu_12'); ?>
            <?php echo $form->textField($model,'vo_da_thu_12',array('class'=>'w-300')); ?>
            <?php echo $form->error($model,'vo_da_thu_12'); ?>
        </div>
        <div class="row">
            <?php echo $form->labelEx($model,'vo_de_xuat_thu_50'); ?>
            <?php echo $form->textField($model,'vo_de_xuat_thu_50',array('class'=>'w-300')); ?>
            <?php echo $form->error($model,'vo_de_xuat_thu_50'); ?>
        </div>
        <div class="row">
            <?php echo $form->labelEx($model,'vo_de_xuat_thu_45'); ?>
            <?php echo $form->textField($model,'vo_de_xuat_thu_45',array('class'=>'w-300')); ?>
            <?php echo $form->error($model,'vo_de_xuat_thu_45'); ?>
        </div>
        <div class="row">
            <?php echo $form->labelEx($model,'vo_de_xuat_thu_12'); ?>
            <?php echo $form->textField($model,'vo_de_xuat_thu_12',array('class'=>'w-300')); ?>
            <?php echo $form->error($model,'vo_de_xuat_thu_12'); ?>
        </div>
            
        <div class="row">
            <?php echo $form->labelEx($model,'file_report'); ?>
            <div style="padding-left: 140px;">
                <?php echo $form->fileField($model,'file_report'); ?>
                cho phép định dạng <?php echo GasSalesFileScanDetai::$AllowFile;?>
            </div>
            <?php echo $form->error($model,'file_report'); ?>
        </div>
        
        <div class="row">
            <?php echo $form->labelEx($model,'report_handle'); ?>
            <?php echo $form->textArea($model,'report_handle',array('rows'=>6, 'style'=>'width:600px')); ?>
            <?php echo $form->error($model,'report_handle'); ?>
	</div>
        
        <div class="row">
            <?php echo $form->labelEx($model,'note_monitor_agent'); ?>
            <?php echo $form->textArea($model,'note_monitor_agent',array('rows'=>3, 'style'=>'width:600px')); ?>
            <?php echo $form->error($model,'note_monitor_agent'); ?>
	</div>
        
        
	<div class="row buttons" style="padding-left: 141px;">
            <?php $this->widget('bootstrap.widgets.TbButton', array(
                'buttonType'=>'submit',
                'label'=>$model->isNewRecord ? 'Create' :'Save',
                'type'=>'null', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
                'size'=>'small', // null, 'large', 'small' or 'mini'
                //'htmlOptions' => array('style' => 'margin-bottom: 10px; float: right;'),
            )); ?>	
            <input class='cancel_iframe' type='button' value='Cancel'>
        </div>

<?php $this->endWidget(); ?>

</div><!-- form -->
<?php // Yii::app()->clientScript->registerCoreScript('jquery.ui'); ?>
<br><img src="<?php echo ImageProcessing::bindImageByModel($model,'','',array('size'=>'size1'));?>">
<!--<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/jquery.floatThead.min.js"></script>    
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/underscore-min.js"></script>    -->

<script>
    $(document).ready(function(){
        $('.form').find('button:submit').click(function(){
            $.blockUI({ overlayCSS: { backgroundColor: '#fff' } }); 
        });
        
        parent.$.fn.yiiGridView.update("gas-customer-check-grid"); 
        validateNumber();
    });
    
    $(window).load(function(){
//        $('.materials_table').floatThead(); // không dùng dc cho popup
        fnResizeColorbox();
    });
    
    function fnResizeColorbox(){
//        var y = $('body').height()+100;
        var y = $('#main_box').height()+100;
        parent.$.colorbox.resize({innerHeight:y});        
    }
      
    
</script>