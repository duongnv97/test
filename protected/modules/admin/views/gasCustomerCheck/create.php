<?php
$this->breadcrumbs=array(
	'Danh sách khách hàng đã kiểm tra'=>array('index'),
	'Tạo Mới',
);

$menus = array(		
        array('label'=>'Danh sách khách hàng đã kiểm tra', 'url'=>array('index')),
);
$this->menu= ControllerActionsName::createMenusRoles($menus, $actions);
?>

<h1>Tạo Mới danh sách khách hàng đã kiểm tra</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>