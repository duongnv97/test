<?php
$this->breadcrumbs=array(
	'Danh sách khách hàng đã kiểm tra',
);

$menus=array(
            array('label'=>'Tạo mới danh sách khách hàng cần kiểm tra', 'url'=>array('create')),
);
$this->menu= ControllerActionsName::createMenusRoles($menus, $actions);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('gas-customer-check-grid', {
                url : $(this).attr('action'),
		data: $(this).serialize()
	});
	return false;
});
");

Yii::app()->clientScript->registerScript('ajaxupdate', "
$('#gas-customer-check-grid a.ajaxupdate').live('click', function() {
    $.fn.yiiGridView.update('gas-customer-check-grid', {
        type: 'POST',
        url: $(this).attr('href'),
        success: function() {
            $.fn.yiiGridView.update('gas-customer-check-grid');
        }
    });
    return false;
});
");
?>

<h1>Danh sách khách hàng đã kiểm tra</h1>

<?php // echo CHtml::link('Tìm Kiếm Nâng Cao','#',array('class'=>'search-button')); ?>
<div class="search-form" style="">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'gas-customer-check-grid',
	'dataProvider'=>$model->search(),
        'afterAjaxUpdate'=>'function(id, data){ fnUpdateColorbox();}',
        'template'=>'{pager}{summary}{items}{pager}{summary}', 
        'pager' => array(
            'maxButtonCount'=>  CmsFormatter::$PAGE_MAX_BUTTON,
        ),            
	'enableSorting' => false,
	//'filter'=>$model,
	'columns'=>array(
        array(
            'header' => 'S/N',
            'type' => 'raw',
            'value' => '$this->grid->dataProvider->pagination->currentPage * $this->grid->dataProvider->pagination->pageSize + ($row+1)',
            'headerHtmlOptions' => array('width' => '10px','style' => 'text-align:center;'),
            'htmlOptions' => array('style' => 'text-align:center;')
        ),
//		'code_no',
        
        array(
            'name' => 'agent_id',
            'type' => 'CustomerCheckNameUser',
            'value' => '$data',
        ),
        array(
            'name' => 'customer_id',
            'type' => 'NameUser',
            'value' => '$data->rCustomer',
        ),
//        array(
//            'header' => 'Địa Chỉ',
//            'value' => 'Users::GetAddressUser($data->rCustomer)',
//            'htmlOptions' => array('style' => 'width:100px;')
//        ),
//        array(
//            'name' => 'agent_id_of_customer',
//            'type' => 'OnlyNameUser',
//            'value' => '$data->rAgentOfCustomer',
//        ),
        array(
            'header' => 'Sale',
            'type' => 'OnlyNameUser',
            'value' => '$data->rSale',
        ),
        array(
            'name' => 'report_date',
            'type' => 'date',
        ),
        array(
            'header' => 'Chi Tiết',
            'type' => 'html',
            'value' => 'GasCustomerCheck::GetTableInfo($data)', 
        ),
        array(
            'header' => 'Tiền Thu Được',
            'name' => 'money_get',
            'type' => 'CustomerCheckMoneyGet',
            'value' => '$data',
            'htmlOptions' => array('class' => 'item_r')
        ),
        array(
            'header' => 'Báo Cáo',
            'type' => 'html',
            'value' => 'nl2br($data->report_handle)',
        ),
        array(
            'header' => 'File Scan',
            'type' => 'CustomerCheckFile',
            'value' => '$data',
        ),
        array(
            'name' => 'status_check',
            'value' => 'GasCustomerCheck::$ARR_STATUS[$data->status_check]',
        ),
        
		array(
            'name' => 'created_date',
            'type' => 'Datetime',
            'htmlOptions' => array('style' => 'width:50px;')
        ),
		
		array(
                'header' => 'Actions',
                'class'=>'CButtonColumn',
                'template'=> ControllerActionsName::createIndexButtonRoles($actions, array('view','update','delete')),
                'buttons'=>array(
                    'update'=>array(
                        'visible'=> 'GasCheck::CanUpdateCustomerCheck($data)',
                    ),
                    'delete'=>array(
                        'visible'=> 'GasCheck::canDeleteData($data)',
                    ),
                ),
		),
	),
)); ?>



<div class="display_none GetHtmlTrLast ">
    <?php echo GasCustomerCheck::GetHtmlTrLast();?>
</div>

<script>
$(document).ready(function() {
    fnUpdateColorbox();
});

function fnUpdateColorbox(){
    fixTargetBlank();
    $(" .view").colorbox({iframe:true,
//        overlayClose :false, escKey:false,
        innerHeight:'2200', 
        innerWidth: '1100', close: "<span title='close'>close</span>"
    });
    $(" .Update_customer").colorbox({iframe:true,
        overlayClose :false, escKey:false,
        innerHeight:'2200', 
        innerWidth: '1100', close: "<span title='close'>close</span>"
    });
    $(".gallery").colorbox({iframe:true,innerHeight:'1500', innerWidth: '1050',close: "<span title='close'>close</span>"});
    fnAddSumTr();
}

function fnAddSumTr(){
    var tr='';
    var sum_debit_vo_12 = 0;
    var sum_debit_vo_50 = 0;
    var sum_debit_vo_45 = 0;
    var sum_debit_money = 0;
    var sum_vo_inventory_12 = 0;
    var sum_vo_inventory_50 = 0;
    var sum_vo_inventory_45 = 0;
    var sum_vo_da_thu_12 = 0;
    var sum_vo_da_thu_50 = 0;
    var sum_vo_da_thu_45 = 0;
    var sum_vo_de_xuat_thu_12 = 0;
    var sum_vo_de_xuat_thu_50 = 0;
    var sum_vo_de_xuat_thu_45 = 0;
    var sum_money_get = 0;
    
    var sum_money_qua_han = 0;
    var sum_money_kho_doi = 0;
    var sum_money_xau = 0;
    var sum_money_goi_dau_chet = 0;
    
    var myArray = ['debit_vo_12','debit_vo_45','debit_vo_50','debit_money',
        'vo_inventory_50','vo_inventory_45','vo_inventory_12',
        'vo_da_thu_50','vo_da_thu_45','vo_da_thu_12',
        'vo_de_xuat_thu_50','vo_de_xuat_thu_45','vo_de_xuat_thu_12',
        'money_get'
    ];
    //for loop
//    for (var i=0; i < myArray.length; i++) {
////      console.log(myArray[i]); //"aa", "bb"
//      var ClassName = myArray[i];
//      $('.'+ClassName).each(function(){
//            if($.trim($(this).val())!='')
//                myArray[i] += parseFloat(''+$(this).val());
//        });
//    }
    
    $('.debit_vo_12').each(function(){
        if($.trim($(this).text())!='')
            sum_debit_vo_12 += parseFloat(''+$(this).text());
    });
    $('.debit_vo_50').each(function(){
        if($.trim($(this).text())!='')
            sum_debit_vo_50 += parseFloat(''+$(this).text());
    });
    $('.debit_vo_45').each(function(){
        if($.trim($(this).text())!='')
            sum_debit_vo_45 += parseFloat(''+$(this).text());
    });
    $('.debit_money').each(function(){
        if($.trim($(this).text())!=''){
            sum_debit_money += parseFloat(''+$(this).text());
        }
    });
    
    $('.vo_inventory_12').each(function(){
        if($.trim($(this).text())!=''){
            sum_vo_inventory_12 += parseFloat(''+$(this).text());
        }
    });
    $('.vo_inventory_50').each(function(){
        if($.trim($(this).text())!=''){
            sum_vo_inventory_50 += parseFloat(''+$(this).text());
        }
    });
    $('.vo_inventory_45').each(function(){
        if($.trim($(this).text())!=''){
            sum_vo_inventory_45 += parseFloat(''+$(this).text());
        }
    });
    $('.vo_da_thu_12').each(function(){
        if($.trim($(this).text())!=''){
            sum_vo_da_thu_12 += parseFloat(''+$(this).text());
        }
    });
    $('.vo_da_thu_50').each(function(){
        if($.trim($(this).text())!=''){
            sum_vo_da_thu_50 += parseFloat(''+$(this).text());
        }
    });
    $('.vo_da_thu_45').each(function(){
        if($.trim($(this).text())!=''){
            sum_vo_da_thu_45 += parseFloat(''+$(this).text());
        }
    });
    $('.vo_de_xuat_thu_12').each(function(){
        if($.trim($(this).text())!=''){
            sum_vo_de_xuat_thu_12 += parseFloat(''+$(this).text());
        }
    });
    $('.vo_de_xuat_thu_50').each(function(){
        if($.trim($(this).text())!=''){
            sum_vo_de_xuat_thu_50 += parseFloat(''+$(this).text());
        }
    });
    $('.vo_de_xuat_thu_45').each(function(){
        if($.trim($(this).text())!=''){
            sum_vo_de_xuat_thu_45 += parseFloat(''+$(this).text());
        }
    });
    $('.money_get').each(function(){
        if($.trim($(this).text())!=''){
            sum_money_get += parseFloat(''+$(this).text());
        }
    });
    
    // Jun 09, 2015
    $('.money_qua_han').each(function(){
        if($.trim($(this).text())!=''){
            sum_money_qua_han += parseFloat(''+$(this).text());
        }
    });
    $('.money_kho_doi').each(function(){
        if($.trim($(this).text())!=''){
            sum_money_kho_doi += parseFloat(''+$(this).text());
        }
    });
    $('.money_xau').each(function(){
        if($.trim($(this).text())!=''){
            sum_money_xau += parseFloat(''+$(this).text());
        }
    });
    $('.money_goi_dau_chet').each(function(){
        if($.trim($(this).text())!=''){
            sum_money_goi_dau_chet += parseFloat(''+$(this).text());
        }
    });
    // Jun 09, 2015
    
    sum_debit_vo_12 = Math.round(sum_debit_vo_12 * 100) / 100;
    sum_debit_vo_50 = Math.round(sum_debit_vo_50 * 100) / 100;
    sum_debit_vo_45 = Math.round(sum_debit_vo_45 * 100) / 100;
    sum_debit_money = Math.round(sum_debit_money * 100) / 100; 
    sum_vo_inventory_12 = Math.round(sum_vo_inventory_12 * 100) / 100; 
    sum_vo_inventory_50 = Math.round(sum_vo_inventory_50 * 100) / 100; 
    sum_vo_inventory_45 = Math.round(sum_vo_inventory_45 * 100) / 100; 
    sum_vo_da_thu_12 = Math.round(sum_vo_da_thu_12 * 100) / 100; 
    sum_vo_da_thu_50 = Math.round(sum_vo_da_thu_50 * 100) / 100; 
    sum_vo_da_thu_45 = Math.round(sum_vo_da_thu_45 * 100) / 100; 
    sum_vo_de_xuat_thu_12 = Math.round(sum_vo_de_xuat_thu_12 * 100) / 100; 
    sum_vo_de_xuat_thu_50 = Math.round(sum_vo_de_xuat_thu_50 * 100) / 100; 
    sum_vo_de_xuat_thu_45 = Math.round(sum_vo_de_xuat_thu_45 * 100) / 100; 
    sum_money_get = Math.round(sum_money_get * 100) / 100; 
    
    sum_money_qua_han = Math.round(sum_money_qua_han * 100) / 100; 
    sum_money_kho_doi = Math.round(sum_money_kho_doi * 100) / 100; 
    sum_money_xau = Math.round(sum_money_xau * 100) / 100; 
    sum_money_goi_dau_chet = Math.round(sum_money_goi_dau_chet * 100) / 100; 
    
    var GetHtmlTrLast = $('.GetHtmlTrLast').html();
    
    tr +='<tr class="f_size_1689 odd sum_page_current">';
        tr +='<td class="item_r item_b" colspan="5">Tổng Cộng</td>';
        tr +='<td class="item_r">'+GetHtmlTrLast+'</td>';
        tr +='<td class="item_r item_b">'+commaSeparateNumber(sum_money_get)+'</td>';
        tr +='<td></td><td></td><td></td><td></td>';
    tr +='</tr>';
    
    if($('.sum_page_current').size()<1){
        $('.items tbody:first').append(tr);
    }
    
    $('.sum_debit_vo_12').html(commaSeparateNumber(sum_debit_vo_12));
    $('.sum_debit_vo_50').html(commaSeparateNumber(sum_debit_vo_50));
    $('.sum_debit_vo_45').html(commaSeparateNumber(sum_debit_vo_45));
    $('.sum_debit_money').html(commaSeparateNumber(sum_debit_money));
    $('.sum_vo_inventory_12').html(commaSeparateNumber(sum_vo_inventory_12));
    $('.sum_vo_inventory_50').html(commaSeparateNumber(sum_vo_inventory_50));
    $('.sum_vo_inventory_45').html(commaSeparateNumber(sum_vo_inventory_45));
    $('.sum_vo_da_thu_12').html(commaSeparateNumber(sum_vo_da_thu_12));
    $('.sum_vo_da_thu_50').html(commaSeparateNumber(sum_vo_da_thu_50));
    $('.sum_vo_da_thu_45').html(commaSeparateNumber(sum_vo_da_thu_45));
    $('.sum_vo_de_xuat_thu_12').html(commaSeparateNumber(sum_vo_de_xuat_thu_12));
    $('.sum_vo_de_xuat_thu_50').html(commaSeparateNumber(sum_vo_de_xuat_thu_50));
    $('.sum_vo_de_xuat_thu_45').html(commaSeparateNumber(sum_vo_de_xuat_thu_45));
    $('.sum_money_get').html(commaSeparateNumber(sum_money_get));
    
    $('.sum_money_qua_han').html(commaSeparateNumber(sum_money_qua_han));
    $('.sum_money_kho_doi').html(commaSeparateNumber(sum_money_kho_doi));
    $('.sum_money_xau').html(commaSeparateNumber(sum_money_xau));
    $('.sum_money_goi_dau_chet').html(commaSeparateNumber(sum_money_goi_dau_chet));
    
}


</script>