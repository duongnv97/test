<div class="row">
    <table class="materials_table materials_table_root materials_table_th" style="">
        <thead>
            <tr> 
                <!--Đã Thu Đề Xuất Thu-->
                <th class="item_c w-150" colspan="3">Nợ Vỏ</th>
                <th class="item_c w-100" colspan="4">Nợ Tiền</th>
            </tr>
            <tr>
                <th class="item_c">12 kg</th>
                <th class="item_c">50 kg</th>
                <th class="item_c">45 kg</th>
                
                <th class="item_c">Nợ quá hạn</th>
                <th class="item_c">Nợ khó đòi</th>
                <th class="item_c">Nợ xấu</th>
                <th class="item_c">Gối đầu chết</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td class="item_c">
                    <?php echo $form->textField($model,'debit_vo_12', array('class'=>'w-30 number_only_v1'));?>
                </td>
                <td class="item_c">
                    <?php echo $form->textField($model,'debit_vo_50', array('class'=>'w-30 number_only_v1')); ?>
                </td>
                <td class="item_c">
                    <?php echo $form->textField($model,'debit_vo_45', array('class'=>'w-30 number_only_v1')); ?>
                </td>
                
                <td class="item_c">
                    <?php echo $form->textField($model,'money_qua_han', array('class'=>'w-100 number_only_v1 number_only')); ?>
                    <div class="help_number"></div>
                </td>
                <td class="item_c">
                    <?php echo $form->textField($model,'money_kho_doi', array('class'=>'w-100 number_only_v1 number_only')); ?>
                    <div class="help_number"></div>
                </td>
                <td class="item_c">
                    <?php echo $form->textField($model,'money_xau', array('class'=>'w-100 number_only_v1 number_only')); ?>
                    <div class="help_number"></div>
                </td>
                <td class="item_c">
                    <?php echo $form->textField($model,'money_goi_dau_chet', array('class'=>'w-100 number_only_v1 number_only')); ?>
                    <div class="help_number"></div>
                </td>
                
            </tr>
        </tbody>
    </table>
</div>