<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>
    <div class="row more_col">
        <div class="col1">
                <?php echo Yii::t('translation', $form->label($model,'date_from')); ?>
                <?php 
                    $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                        'model'=>$model,        
                        'attribute'=>'date_from',
                        'options'=>array(
                            'showAnim'=>'fold',
                            'dateFormat'=> MyFormat::$dateFormatSearch,
//                            'minDate'=> '0',
                            'maxDate'=> '0',
                            'changeMonth' => true,
                            'changeYear' => true,
                            'showOn' => 'button',
                            'buttonImage'=> Yii::app()->theme->baseUrl.'/admin/images/icon_calendar_r.gif',
                            'buttonImageOnly'=> true,                                
                        ),        
                        'htmlOptions'=>array(
                            'class'=>'w-16',
                            'size'=>'16',
                            'style'=>'float:left;',                               
                        ),
                    ));
                ?>     		
        </div>
        <div class="col2">
                <?php echo Yii::t('translation', $form->label($model,'date_to')); ?>
                <?php 
                    $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                        'model'=>$model,        
                        'attribute'=>'date_to',
                        'options'=>array(
                            'showAnim'=>'fold',
                            'dateFormat'=> MyFormat::$dateFormatSearch,
//                            'minDate'=> '0',
                            'maxDate'=> '0',
                            'changeMonth' => true,
                            'changeYear' => true,
                            'showOn' => 'button',
                            'buttonImage'=> Yii::app()->theme->baseUrl.'/admin/images/icon_calendar_r.gif',
                            'buttonImageOnly'=> true,                                
                        ),        
                        'htmlOptions'=>array(
                            'class'=>'w-16',
                            'size'=>'16',
                            'style'=>'float:left;',
                        ),
                    ));
                ?>     		
        </div>
    </div> 
    
	<div class="row">
        <?php echo $form->labelEx($model,'customer_id'); ?>
        <?php echo $form->hiddenField($model,'customer_id'); ?>
            <?php 
                // widget auto complete search user customer and supplier
                $aData = array(
                    'model'=>$model,
                    'name_relation_user'=>'rCustomer',
                    'ClassAdd' => 'w-500',
                );
                
//                $aData = array(
//                    'model'=>$model,
//                    'field_customer_id'=>'sale_id',
//                    'url'=> Yii::app()->createAbsoluteUrl('admin/ajax/search_sale'),
//                    'name_relation_user'=>'sale',
//                    'placeholder'=>'Nhập Tên Sale',
//                    'ClassAdd' => 'w-500',
////                        'fnSelectCustomer'=>'fnAfterSelectSaleOrAgent',
//                );
                
                
                $this->widget('ext.SpaAutocompleteCustomer.SpaAutocompleteCustomer',
                    array('data'=>$aData));                                        
            ?>        
    </div>
    
	<div class="row">
        <?php echo $form->labelEx($model,'uid_login'); ?>
        <?php echo $form->hiddenField($model,'uid_login'); ?>
            <?php 
                // widget auto complete search user customer and supplier
                $aData = array(
                    'model'=>$model,
                    'field_customer_id'=>'uid_login',
                    'url'=> Yii::app()->createAbsoluteUrl('admin/ajax/Search_for_user_login'),
                    'name_relation_user'=>'rUidLogin',
                    'placeholder'=>'Nhập tên giám sát đại lý',
                    'ClassAdd' => 'w-500',
                    'field_autocomplete_name' => 'autocomplete_name_uid_login',
                );
                $this->widget('ext.SpaAutocompleteCustomer.SpaAutocompleteCustomer',
                    array('data'=>$aData));                                        
            ?>        
    </div>
    
    
	<div class="row">
        <?php echo $form->labelEx($model,'sale_id'); ?>
        <?php echo $form->hiddenField($model,'sale_id'); ?>
            <?php 
                // widget auto complete search user customer and supplier
                $aData = array(
                    'model'=>$model,
                    'field_customer_id'=>'sale_id',
                    'url'=> Yii::app()->createAbsoluteUrl('admin/ajax/search_sale'),
                    'name_relation_user'=>'rSale',
                    'placeholder'=>'Nhập tên sale',
                    'ClassAdd' => 'w-500',
                    'field_autocomplete_name' => 'autocomplete_name_sale',
                );
                $this->widget('ext.SpaAutocompleteCustomer.SpaAutocompleteCustomer',
                    array('data'=>$aData));                                        
            ?>        
    </div>

	<div class="row buttons" style="padding-left: 159px;">
		        <?php $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType'=>'submit',
            'label'=>'Search',
            'type'=>'null', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
            'size'=>'small', // null, 'large', 'small' or 'mini'
            //'htmlOptions' => array('style' => 'margin-bottom: 10px; float: right;'),
        )); ?>	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->