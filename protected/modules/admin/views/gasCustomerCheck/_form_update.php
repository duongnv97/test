<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'gas-customer-check-form',
	'enableAjaxValidation'=>false,
)); ?>

    <p class="note">Dòng có dấu <span class="required">*</span> là bắt buộc.</p>
    <?php echo MyFormat::BindNotifyMsg(); ?>
    
    <div class="row">
        <?php echo $form->labelEx($model,'month'); ?>
        <?php echo $form->dropDownList($model,'month', ActiveRecord::getMonthVnWithZeroFirst(),array('class' => 'w-300')); ?>
        <?php echo $form->error($model,'month'); ?>
    </div>
    <div class="row">
        <?php echo $form->labelEx($model,'year'); ?>
        <?php echo $form->dropDownList($model,'year', ActiveRecord::getRangeYear(),array('class' => 'w-300')); ?>
        <?php echo $form->error($model,'year'); ?>
    </div>
    
    <div class="row">
        <?php echo $form->labelEx($model,'monitor_agent_id'); ?>
        <?php echo $form->dropDownList($model,'monitor_agent_id', Users::getArrUserByRole(ROLE_AGENT, array('order'=>'t.code_bussiness')),array('class'=>'w-300','empty'=>'Select')); ?>
        <?php echo $form->error($model,'monitor_agent_id'); ?>
    </div>
    
    <div class="row">
        <?php echo $form->labelEx($model,'customer_name'); ?>
        <?php echo $form->textField($model,'customer_name',array('class'=>'w-300')); ?>
        <?php echo $form->error($model,'customer_name'); ?>
    </div>
    
    <div class="row">
        <?php echo $form->labelEx($model,'customer_address'); ?>
        <?php echo $form->textField($model,'customer_address',array('class'=>'w-300')); ?>
        <?php echo $form->error($model,'customer_address'); ?>
    </div>
    
    <div class="row">
        <?php echo $form->labelEx($model,'sale'); ?>
        <?php echo $form->textField($model,'sale',array('class'=>'w-300')); ?>
        <?php echo $form->error($model,'sale'); ?>
    </div>
    
    <div class="row">
        <?php echo $form->labelEx($model,'qty'); ?>
        <?php echo $form->textField($model,'qty',array('class'=>'w-300')); ?>
        <?php echo $form->error($model,'qty'); ?>
    </div>
    
    <div class="row buttons" style="padding-left: 141px;">
        <?php $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType'=>'submit',
            'label'=>$model->isNewRecord ? 'Create' :'Save',
            'type'=>'null', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
            'size'=>'small', // null, 'large', 'small' or 'mini'
            //'htmlOptions' => array('style' => 'margin-bottom: 10px; float: right;'),
        )); ?>	
    </div>

<?php $this->endWidget(); ?>

</div><!-- form -->
<script>
    $(document).ready(function(){
        $('.form').find('button:submit').click(function(){
            $.blockUI({ overlayCSS: { backgroundColor: '#fff' } }); 
        });
        parent.$.fn.yiiGridView.update("gas-customer-check-grid"); 
    });
</script>