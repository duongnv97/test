<div class="row">
    <table class="materials_table materials_table_root materials_table_th" style="">
        <thead>
            <tr> 
                <!--Đã Thu Đề Xuất Thu-->
                <th class="item_c w-150" colspan="3">Nợ Vỏ</th>
                <th class="item_c w-100" colspan="4">Nợ Tiền</th>
                <th class="item_c w-150" colspan="3">Tồn Thực Tế</th>
                <th class="item_c w-150" colspan="3">Đã Thu</th>
                <th class="item_c w-150" colspan="3">Đề Xuất Thu</th>
            </tr>
            <tr>                
                <th class="item_c">12 kg</th>
                <th class="item_c">50 kg</th>
                <th class="item_c">45 kg</th>
                
                <th class="item_c">Nợ quá hạn</th>
                <th class="item_c">Nợ khó đòi</th>
                <th class="item_c">Nợ xấu</th>
                <th class="item_c">Gối đầu chết</th>
                
                <th class="item_c">12 kg</th>
                <th class="item_c">50 kg</th>
                <th class="item_c">45 kg</th>
                
                <th class="item_c">12 kg</th>
                <th class="item_c">50 kg</th>
                <th class="item_c">45 kg</th>
                
                <th class="item_c">12 kg</th>
                <th class="item_c">50 kg</th>
                <th class="item_c">45 kg</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td class="item_c">
                    <?php echo $model->debit_vo_12;?>
                </td>
                <td class="item_c">
                    <?php echo $model->debit_vo_50;?>
                </td>
                <td class="item_c">
                    <?php echo $model->debit_vo_45;?>
                </td>
                
                <td class="item_c">
                    <?php echo ActiveRecord::formatCurrency($model->money_qua_han);?>
                </td>
                <td class="item_c">
                    <?php echo ActiveRecord::formatCurrency($model->money_kho_doi);?>
                </td>
                <td class="item_c">
                    <?php echo ActiveRecord::formatCurrency($model->money_xau);?>
                </td>
                <td class="item_c">
                    <?php echo ActiveRecord::formatCurrency($model->money_goi_dau_chet);?>
                </td>
                
                <td class="item_c">
                    <?php echo $form->textField($model,'vo_inventory_12', array('class'=>'w-30 number_only_v1'));?>
                </td>
                <td class="item_c">
                    <?php echo $form->textField($model,'vo_inventory_50', array('class'=>'w-30 number_only_v1')); ?>
                </td>
                <td class="item_c">
                    <?php echo $form->textField($model,'vo_inventory_45', array('class'=>'w-30 number_only_v1')); ?>
                </td>
                
                <td class="item_c">
                    <?php echo $form->textField($model,'vo_da_thu_12', array('class'=>'w-30 number_only_v1'));?>
                </td>
                <td class="item_c">
                    <?php echo $form->textField($model,'vo_da_thu_50', array('class'=>'w-30 number_only_v1')); ?>
                </td>
                <td class="item_c">
                    <?php echo $form->textField($model,'vo_da_thu_45', array('class'=>'w-30 number_only_v1')); ?>
                </td>
                
                <td class="item_c">
                    <?php echo $form->textField($model,'vo_de_xuat_thu_12', array('class'=>'w-30 number_only_v1'));?>
                </td>
                <td class="item_c">
                    <?php echo $form->textField($model,'vo_de_xuat_thu_50', array('class'=>'w-30 number_only_v1')); ?>
                </td>
                <td class="item_c">
                    <?php echo $form->textField($model,'vo_de_xuat_thu_45', array('class'=>'w-30 number_only_v1')); ?>
                </td>
            </tr>
        </tbody>
    </table>
</div>