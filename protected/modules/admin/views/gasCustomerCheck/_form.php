<?php
$cRole = Yii::app()->user->role_id;
$SubmitText = $model->isNewRecord ? 'Create' :'Save';
if($cRole == ROLE_ACCOUNTING){
    $SubmitText = "Kế Toán Đã Xác Nhận";
}
?>
<div class="form">
<?php $cmsFormater = new CmsFormatter();?>
<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'gas-customer-check-form',
	'enableAjaxValidation'=>false,
        'htmlOptions' => array('enctype' => 'multipart/form-data'),
)); ?>

    <p class="note">Dòng có dấu <span class="required">*</span> là bắt buộc.</p>
    <?php echo MyFormat::BindNotifyMsg(); ?>
    
    <div class="row">
        <?php echo $form->labelEx($model,'customer_id'); ?>
        <?php echo $form->hiddenField($model,'customer_id'); ?>
            <?php 
                // widget auto complete search user customer and supplier
                $aData = array(
                    'model'=>$model,
                    'name_relation_user'=>'rCustomer',
                );
                $this->widget('ext.SpaAutocompleteCustomer.SpaAutocompleteCustomer',
                    array('data'=>$aData));                                        
            ?>        
        <?php echo $form->error($model,'customer_id'); ?>
    </div>
    
    <?php if( GasCustomerCheck::CanViewApprove()): ?>
        <?php include '_form_table.php';?>
    <?php else:?>
        <?php include '_form_table_2.php';?>
        <?php include '_form_table_report.php';?>
    <?php endif;?>
    
    <div class="row buttons" style="padding-left: 141px;">
        <?php $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType'=>'submit',
            'label'=> $SubmitText,
            'type'=>'null', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
            'size'=>'small', // null, 'large', 'small' or 'mini'
            //'htmlOptions' => array('style' => 'margin-bottom: 10px; float: right;'),
        )); ?>
    </div>

<?php $this->endWidget(); ?>

</div><!-- form -->


<script>
    $(document).ready(function(){
        $('.form').find('button:submit').click(function(){
            $.blockUI({ overlayCSS: { backgroundColor: '#fff' } }); 
        });
        $(".gallery").colorbox({iframe:true,innerHeight:'1500', innerWidth: '1050',close: "<span title='close'>close</span>"});
    });
</script>
