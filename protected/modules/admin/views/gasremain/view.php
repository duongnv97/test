<?php
$this->breadcrumbs=array(
	'Quản Lý Gas Dư'=>array('index'),
	$model->seri,
);

$menus = array(
	array('label'=>'Quản Lý Gas Dư', 'url'=>array('index')),
	array('label'=>'Tạo Mới Gas Dư ', 'url'=>array('create')),
	array('label'=>'Cập Nhật Gas Dư ', 'url'=>array('update', 'id'=>$model->id)),
//	array('label'=>'Xóa Gas Dư', 'url'=>array('delete'), 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Bạn chắc chắn muốn xóa ?')),
);
$this->menu= ControllerActionsName::createMenusRoles($menus, $actions);
?>

<h1>Xem Gas Dư Bình Seri: <?php echo $model->seri; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'date_input:date',
                array(
                    'name'=>'agent_id',
                    'type'=>'NameUser',
                    'value'=>$model->agent?$model->agent:"",
                ), 
                array(
                    'name'=>'customer_id',
                    'type'=>'NameUser',
                    'value'=>$model->customer?$model->customer:"",
                ), 
                array(
                    'name'=>'only_gas_remain',
                    'value'=> GasRemain::$SELECT_ONLY_GAS_REMAIN[$model->only_gas_remain],
                ),
                array(
                    'name'=>'materials_id',
                    'value'=>$model->rMaterials?$model->rMaterials->name:"",
                ), 
            
                'seri',
		'amount_empty:Currency',
		'amount_has_gas:Currency',
		'amount_gas:Currency',
		'amount_gas_2:Currency',
                array(
                    'name'=>'user_update_2',
                    'type'=>'NameUser',
                    'value'=>$model->re_user_update_2?$model->re_user_update_2:"",
                ),   
                'date_update_2:datetime',
		'amount_gas_3:Currency',
                array(
                    'name'=>'user_update_3',
                    'type'=>'NameUser',
                    'value'=>$model->re_user_update_3?$model->re_user_update_3:"",
                ),             
		'date_update_3:datetime',
		'created_date:datetime',
	),
)); ?>
