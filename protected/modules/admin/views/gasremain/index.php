<?php
$this->breadcrumbs=array(
	'Quản Lý Gas Dư',
);

$menus=array(
        array('label'=>'Tạo Mới Gas Dư', 'url'=>array('create')),
        array('label'=> 'Xuất Excel Danh Sách Hiện Tại',
        'url'=>array('ExportExcel'), 
        'htmlOptions'=>array('class'=>'export_excel ','label'=>'Xuất Excel')),
);
$this->menu= ControllerActionsName::createMenusRoles($menus, $actions);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('gas-remain-grid', {
                url : $(this).attr('action'),
		data: $(this).serialize()
	});
	return false;
});
");

Yii::app()->clientScript->registerScript('ajaxupdate', "
$('#gas-remain-grid a.ajaxupdate').live('click', function() {
    $.fn.yiiGridView.update('gas-remain-grid', {
        type: 'POST',
        url: $(this).attr('href'),
        success: function() {
            $.fn.yiiGridView.update('gas-remain-grid');
        }
    });
    return false;
});
");
$session=Yii::app()->session; 
?>

<?php include 'index_button.php'; ?>
<?php $this->widget('ListCronExcelWidget', array('model'=>$model)); ?>
<?php echo CHtml::link('Tìm Kiếm Nâng Cao','#',array('class'=>'search-button')); ?>
<div class="search-form display_none1" style="">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'gas-remain-grid',
	'dataProvider'=>$model->search(),
        'afterAjaxUpdate'=>'function(id, data){ fnUpdateColorbox();}',
        'template'=>''
        . '<div class="AMOUNT_TOTAL_GAS_DU clr hight_light item_b f_size_18">'.(isset($session['AMOUNT_TOTAL_GAS_DU'])?$session['AMOUNT_TOTAL_GAS_DU']:"").'</div>'
        . '{pager}{summary}{items}{pager}{summary}', 
        'pager' => array(
            'maxButtonCount'=>  CmsFormatter::$PAGE_MAX_BUTTON,
        ),            
	//'filter'=>$model,
	'columns'=>array(
        array(
            'header' => 'S/N',
            'type' => 'raw',
            'value' => '$this->grid->dataProvider->pagination->currentPage * $this->grid->dataProvider->pagination->pageSize + ($row+1)',
            'headerHtmlOptions' => array('width' => '10px','style' => 'text-align:center;'),
            'htmlOptions' => array('style' => 'text-align:center;')
        ),            
            array(
                'name' => 'date_input',
                'value' => '$data->getDateInput()."<br>".$data->getSource()',
                'type' => 'raw',
                'htmlOptions' => array('style' => 'text-align:center;')
            ),
            array(
                'name' => 'car_id',
                'value' => '$data->getCar()."<br>".$data->getDriver()',
                'type' => 'raw',
                'htmlOptions' => array('style' => 'text-align:center;')
            ),
            array(
                'name' => 'agent_id',
                'type' => 'raw',
                'value' => '$data->getAgent()."<br>".$data->getCodeNo().$data->getUidLoginGrid()',
//                    'visible'=>Yii::app()->user->role_id!=ROLE_SUB_USER_AGENT,
                'htmlOptions' => array('style' => 'text-align:center;width:100px;')
            ),            
            array(
                'name' => 'customer_id',
                'type' => 'raw',
                'value' => '$data->getCustomer()."<br>".$data->getCustomerOfAgent()." - ".$data->customer_id',
                'htmlOptions' => array('style' => 'text-align:center;width:150px;')
            ),
            array(
                'header' => 'Loại KH',
                'value'=>'$data->getTypeCustomer()',
                'htmlOptions' => array('style' => '')
            ),            
            array(
                'name'=>'materials_id',
                'value'=>'$data->getMaterials()',
            ),             
            array(
                'name' => 'seri',
                'htmlOptions' => array('style' => 'text-align:center;width:50px;')
            ),               
            array(
                'header' => 'KL Vỏ Bình',
                'name' => 'amount_empty',
                'type' => 'Currency',
                'htmlOptions' => array('style' => 'text-align:right;width:50px;')
            ),               
            array(
                'header' => 'KL Gas+Vỏ Bình',
                'name' => 'amount_has_gas',
                'type' => 'Currency',
                'htmlOptions' => array('style' => 'text-align:right;width:50px;')
            ),               
            array(
                'name' => 'amount_gas',
                'type' => 'GasRemain1',
                'value' => '$data',
                'htmlOptions' => array('style' => 'text-align:right;width:40px;')
            ),               
            array(
                'name' => 'amount_gas_2',
                'type' => 'GasRemain2',
                'value' => '$data',
                'htmlOptions' => array('style' => 'text-align:center;width:100px;')
            ), 

//                array(
//                    'name' => 'amount_gas_3',
//                    'type' => 'GasRemain3',
//                    'value' => '$data',
//                    'htmlOptions' => array('style' => 'text-align:center;width:100px;')
//                ),
            array(
                'name' => 'has_export',
                'type' => 'raw',
                'value' => '$data->getTextHasExport()',
                'htmlOptions' => array('style' => 'text-align:center;width:50px;')
            ),
            array(
                'name' => 'amount',
                'type' => 'raw',
                'value' => '$data->getAmountWeb()',
                'htmlOptions' => array('style' => 'text-align:right;'),
            ),

//                array(
//                    'name'=>'only_gas_remain',
//                    'value'=> 'GasRemain::$SELECT_ONLY_GAS_REMAIN[$data->only_gas_remain]',
//                    'htmlOptions' => array('style' => 'text-align:center;')
//                ),
//                array(
//                    'name' => 'update_status_id',
//                    'type' => 'NameUser',
//                    'value' => '$data->update_status?$data->update_status:""',
//                    'visible'=>Yii::app()->user->role_id!=ROLE_SUB_USER_AGENT,
//                    'htmlOptions' => array('style' => 'text-align:center;width:100px;')
//                ),              
//                array(
//                    'name' => 'update_status_date',
//                    'type' => 'datetime',
//                    'htmlOptions' => array('style' => 'text-align:center;width:60px;')
//                ),              
            array(
                'name' => 'created_date',
                'type' => 'datetime',
                'htmlOptions' => array('style' => 'text-align:center;width:60px;')
            ),              
            array(
                    'header' => 'Actions',
                    'class'=>'CButtonColumn',
                    'template'=> ControllerActionsName::createIndexButtonRoles($actions),
                'buttons'=>array(
                        'update'=>array(
                            'visible'=> '$data->canUpdate()',
                        ),    
                        'delete'=>array(
                            'visible'=> 'GasCheck::canDeleteData($data)',
                        )
                    ),            
            ),
        ),
)); 
?>

<script>
$(document).ready(function() {
    fnUpdateColorbox();
    fnRemove_update_gas_remain();
});

function fnUpdateColorbox(){    
    $(".update_gas_remain").colorbox({iframe:true,innerHeight:'350', innerWidth: '650',close: "<span title='close'>close</span>"});
    fnShowhighLightTr();
    $(".view").colorbox({iframe:true,innerHeight:'700', innerWidth: '900',close: "<span title='close'>close</span>"});
    fnAddSumTr();
    fixTargetBlank();   
}

function fnAddSumTr(){
    var tr='';
    var amount_gas = 0;
    var amount_gas_2 = 0;
    var amount_gas_3 = 0;
    var amount = 0;
    $('.amount_gas').each(function(){
        if($.trim($(this).val())!='')
            amount_gas += parseFloat(''+$(this).val());
    });
    $('.amount_gas_2').each(function(){
        if($.trim($(this).val())!='')
            amount_gas_2+= parseFloat(''+$(this).val());
    });
    $('.amount_gas_3').each(function(){
        if($.trim($(this).val())!='')
            amount_gas_3+= parseFloat(''+$(this).val());
    });
    $('.amount').each(function(){
        if($.trim($(this).text())!=''){
            amount+= parseFloat(''+$(this).text());
        }
    });    
    
    amount_gas = Math.round(amount_gas * 100) / 100;
    amount_gas_2 = Math.round(amount_gas_2 * 100) / 100;
    amount_gas_3 = Math.round(amount_gas_3 * 100) / 100;
    amount = Math.round(amount * 100) / 100;    
    
    tr +='<tr class="f_size_18 odd sum_page_current">';
        tr +='<td class="item_r item_b" colspan="10">Tổng Cộng</td>';
        tr +='<td class="item_r item_b">'+commaSeparateNumber(amount_gas)+'</td>';
        tr +='<td class="item_r item_b">'+commaSeparateNumber(amount_gas_2)+'</td>';
//        tr +='<td class="item_r item_b">'+commaSeparateNumber(amount_gas_3)+'</td>';
        tr +='<td class=""></td>';
        <?php // if(Yii::app()->user->role_id==ROLE_ADMIN || Yii::app()->user->role_id==ROLE_SUB_USER_AGENT):?>
        tr +='<td class="item_r item_b">'+commaSeparateNumber(amount)+'</td>';
        tr +='<td></td><td></td><td></td>';
    tr +='</tr>';
    
    if($('.sum_page_current').size()<1){
        $('.items tbody').prepend(tr);
        $('.items tbody').append(tr);
    }
}


function fnRemove_update_gas_remain(){
    $('.remove_update_gas_remain').live('click',function(){
        if(confirm('Bạn chắc chắn muốn hủy lần cập nhật này?')){
            var url_ = $(this).attr('rel');
            $.blockUI({ overlayCSS: { backgroundColor: '<?php echo BLOCK_UI_COLOR;?>' } }); 
            $.ajax({
                url:url_,
                dataType:'json',
                type:'get',
                success:function(data){
                    $.fn.yiiGridView.update("gas-remain-grid");   
                    $.unblockUI();
                }
            });
        }
    });
}

</script>