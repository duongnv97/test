<?php
$this->breadcrumbs=array(
	'Quản Lý Gas Dư'=>array('index'),
	'Tạo Mới',
);

$menus = array(		
        array('label'=>'Quản Lý Gas Dư', 'url'=>array('index')),
);
$this->menu= ControllerActionsName::createMenusRoles($menus, $actions);
?>

<h1>Tạo Mới Gas Dư</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>