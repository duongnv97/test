<div class="form">
    <?php
        $LinkNormal = Yii::app()->createAbsoluteUrl('admin/gasremain/index');
        $LinkDetail = Yii::app()->createAbsoluteUrl('admin/gasremain/index', array( 'lost'=> 1));
    ?>
    <h1>Quản Lý Gas Dư 
        <a class='btn_cancel f_size_14 <?php echo (!isset($_GET['lost'])) ? "active":"";?>' href="<?php echo $LinkNormal;?>">Normal</a>
        <a class='btn_cancel f_size_14 <?php echo isset($_GET['lost']) && $_GET['lost']==1 ? "active":"";?>' href="<?php echo $LinkDetail;?>">Cân thiếu</a>
    </h1> 
</div>