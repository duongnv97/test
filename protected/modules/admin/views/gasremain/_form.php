<?php 
    $display_none = '';
    $aRoleHide = [ROLE_CALL_CENTER, ROLE_DIEU_PHOI];
    $cRole = MyFormat::getCurrentRoleId();
    $cUid = MyFormat::getCurrentUid();
    if(in_array($cRole, $aRoleHide) || in_array($cUid, $model->getUidUpdateVinhLong())){
        $display_none = 'display_none';
    }
    if($cRole == ROLE_ADMIN){
        echo "UID Login = $model->uid_login";
    }
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'gas-remain-form',
	'enableAjaxValidation'=>false,
)); ?>
	<p class="note">Dòng có dấu <span class="required">*</span> là bắt buộc.</p>  
        <?php  if(Yii::app()->user->hasFlash('successUpdate')): ?>
            <div class='success_div'><?php echo Yii::app()->user->getFlash('successUpdate');?></div>         
        <?php endif; ?>
        <div class="row buttons" style="padding-left: 141px;">
            <?php $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType'=>'submit',
            'label'=>$model->isNewRecord ? 'Create' :'Save',
            'type'=>'null', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
            'size'=>'small', // null, 'large', 'small' or 'mini'
            //'htmlOptions' => array('style' => 'margin-bottom: 10px; float: right;'),
            )); ?>	
        </div>
        <div class="row">
            <?php echo $form->labelEx($model,'has_export', array()); ?>
            <?php echo $form->dropDownList($model,'has_export', GasRemain::$TYPE_HAS_EXPORT, array('class'=>'w-300')); ?>
	</div>
            
        <div class="row ">
            <?php echo $form->labelEx($model,'agent_id'); ?>
            <?php echo $form->hiddenField($model,'agent_id', array('class'=>'')); ?>
            <?php
                // 1. limit search kh của sale
                $url = Yii::app()->createAbsoluteUrl('admin/ajax/search_by_role', array('role'=> ROLE_AGENT));
                // widget auto complete search user customer and supplier
                $aData = array(
                    'model'=>$model,
                    'field_customer_id'=>'agent_id',
                    'url'=> $url,
                    'name_relation_user'=>'rAgent',
                    'ClassAdd' => 'w-300',
                    'field_autocomplete_name' => 'autocomplete_name_2',
                    'placeholder'=>'Nhập mã hoặc tên đại lý',
                );
                $this->widget('ext.SpaAutocompleteCustomer.SpaAutocompleteCustomer',
                    array('data'=>$aData));
                ?>
            <?php echo $form->error($model,'agent_id'); ?>
        </div>

            
	<div class="row">
		<?php echo $form->labelEx($model,'date_input'); ?>
		<?php 
                    $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                        'model'=>$model,        
                        'attribute'=>'date_input',
                        'options'=>array(
                            'showAnim'=>'fold',
                            'dateFormat'=> ActiveRecord::getDateFormatJquery(),
//                            'minDate'=> '0',
                            'maxDate'=> '0',
                            'changeMonth' => true,
                            'changeYear' => true,
                            'showOn' => 'button',
                            'buttonImage'=> Yii::app()->theme->baseUrl.'/admin/images/icon_calendar_r.gif',
                            'buttonImageOnly'=> true,                                
                        ),        
                        'htmlOptions'=>array(
                            'class'=>'w-300',
                            'style'=>'height:20px;;',
                                'readonly'=>'readonly',
                        ),
                    ));
                ?>     		
		<?php echo $form->error($model,'date_input'); ?>
	</div>
        <em class="hight_light item_b display_none">Phần chọn này nếu bạn không hiểu, vui lòng gọi Kiên để giải thích</em>
            
        <div class="row">
            <?php echo $form->labelEx($model,'customer_id'); ?>
            <?php echo $form->hiddenField($model,'customer_id', array('class'=>'')); ?>
            <?php 
                // widget auto complete search user customer and supplier
                $aData = array(
                    'model'=>$model,
                    'field_customer_id'=>'customer_id',
                    'name_relation_user'=>'rCustomer',
                    'url' => Yii::app()->createAbsoluteUrl('admin/ajax/search_user_by_code'),
                    'ClassAdd' => 'w-400',
                );
                $this->widget('ext.SpaAutocompleteCustomer.SpaAutocompleteCustomer',
                    array('data'=>$aData));                                        
            ?>
        </div>


        <?php if(!$model->isNewRecord):?>    
        <div class="table_tr_row">
            <?php
                $mText = '';
                if($model->rMaterials){
                    $mText = $model->rMaterials->name;
                }
            ?>
            <div class="row col_material">
                    <?php echo $form->labelEx($model,'materials_id'); ?>
                    <?php echo $form->hiddenField($model,'materials_id',array('class'=>'materials_id_hide')); ?>
                    <input class="float_l material_autocomplete w-250"  placeholder="Nhập mã hoặc tên vật tư" maxlength="100" value="<?php echo $mText; ?>" type="text" <?php echo !empty($mText)?'readonly="1"':'' ?>>
                    <span onclick="" class="remove_material_js remove_item_material"></span>
            </div>
            <div class="clr"></div>
            <div class="row <?php echo $display_none;?>">
                    <?php echo $form->labelEx($model,'seri'); ?>
                    <?php echo $form->textField($model,'seri',array('class'=>'seri' , 'size'=>25,'maxlength'=>25)); ?>
                    <?php echo $form->error($model,'seri'); ?>
            </div>
            <div class="row <?php echo $display_none;?>">
                    <?php echo $form->labelEx($model,'amount_empty'); ?>
                    <?php echo $form->textField($model,'amount_empty',array('class'=>'change_amount amount_empty number_only_v1','size'=>10,'maxlength'=>6)); ?>  Kg
                    <?php echo $form->error($model,'amount_empty'); ?>
            </div>
            <div class="row <?php echo $display_none;?>">
                    <?php echo $form->labelEx($model,'amount_has_gas'); ?>
                    <?php echo $form->textField($model,'amount_has_gas',array('class'=>'change_amount amount_has_gas number_only_v1','size'=>10,'maxlength'=>6)); ?>  Kg
                    <?php echo $form->error($model,'amount_has_gas'); ?>
            </div>
            <div class="row <?php echo $display_none;?>">
                    <?php echo $form->labelEx($model,'amountHasGas2'); ?>
                    <?php echo $form->textField($model,'amountHasGas2',array('class'=>'number_only_v1','size'=>10,'maxlength'=>6)); ?>  Kg
                    <?php echo $form->error($model,'amountHasGas2'); ?>
            </div>
            <?php if(!empty($display_none)): ?>
                <div class="row">
                    <?php echo $form->labelEx($model,'seri', ['label'=> 'Thông tin']); ?>
                    <span class="item_b"><?php echo "Seri: $model->seri  --  Trọng Lượng Vỏ Bình: $model->amount_empty  -- Trọng Lượng Gas+Vỏ Bình: $model->amount_has_gas"; ?></span>
                </div>
            <?php endif; ?>
            
            <div class="row">
                <?php echo $form->labelEx($model,'amount_gas'); ?>
                <span class="amount_gas"><?php echo $model->amount_gas;?></span> Kg
                <?php echo $form->error($model,'amount_gas'); ?>
            </div>
            <div class="row">
                <?php echo $form->labelEx($model,'amount_gas_2'); ?>
                <span class="amount_gas"><?php echo $model->amount_gas_2;?></span> Kg
                <?php echo $form->error($model,'amount_gas_2'); ?>
            </div>
        </div>
        <?php else:?>
        <div class="clr"></div>
        <div class="row">
            <label>&nbsp</label>
            <div>
                <a href="javascript:void(0);" class="text_under_none item_b" style="line-height:25px" onclick="fnBuildRow();">
                    <img style="float: left;margin-right:8px;" src="<?php echo Yii::app()->theme->baseUrl;?>/images/add.png"> 
                    Thêm Dòng ( Phím tắt F8 )
                </a>
            </div>
        </div>
        <div class="clr"></div>
            
        <div class="row">
            <label>&nbsp</label>
            <table class="materials_table hm_table">
                <thead>
                    <tr>
                        <th class="item_c">#</th>
                        <th class="item_c w-250">Vật Tư</th>
                        <th class="item_c w-100">Seri</th>
                        <th class="item_c w-100">Trọng Lượng Vỏ Bình (Kg)</th>
                        <th class="item_c w-100">Trọng Lượng Gas+Vỏ Bình (Kg)</th>
                        <th class="item_c w-100">Gas Dư</th>
                        <th class="last item_c">Xóa</th>
                    </tr>
                </thead>
                <tbody>
                    <tr class="table_tr_row">
                        <td class="order_no item_c">1</td>
                        <td class="item_c col_material">
                            <input class="materials_id_hide" value="" type="hidden" name="GasRemain[materials_id][]">
                            <input class="float_l material_autocomplete w-220"  placeholder="Nhập mã hoặc tên vật tư" maxlength="100" value="" type="text">
                            <span onclick="" class="remove_material_js remove_item_material"></span>
                        </td>
                        <td class="item_c">
                            <input name="GasRemain[seri][]" class="seri" type="text" size="10" maxlength="25">
                        </td>
                        <td class="item_c">
                            <input name="GasRemain[amount_empty][]" class="change_amount_row amount_empty number_only_v1 w-50" type="text"  maxlength="6">
                        </td>
                        <td class="item_c">
                            <input name="GasRemain[amount_has_gas][]" class="change_amount_row amount_has_gas number_only_v1 w-50" type="text" maxlength="6">
                        </td>
                        <td class="item_c">
                            <span class="amount_gas"></span> Kg
                        </td>
                        <td class="item_c last"><span class="remove_icon_only"></span></td>
                    </tr>                    
                </tbody>
            </table>            
        </div>
        <?php endif;?>

	<div class="row buttons" style="padding-left: 141px;">
            <?php $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType'=>'submit',
            'label'=>$model->isNewRecord ? 'Create' :'Save',
            'type'=>'null', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
            'size'=>'small', // null, 'large', 'small' or 'mini'
            //'htmlOptions' => array('style' => 'margin-bottom: 10px; float: right;'),
            )); ?>	
        </div>

<?php $this->endWidget(); ?>

</div><!-- form -->
<style>
    .amount_gas { width: 83px; display: inline-block; text-align: right; font-weight: bold; font-size: 14px;}
</style>
<?php 
    $material_type = CmsFormatter::$MATERIAL_TYPE_BINHBO_INPUT;
//    unset($material_type[2]); // Oct 20, 2015 thêm cả vỏ 12 vào
    unset($material_type[3]);
?>

<script>
    $(document).keydown(function(e) {
        if(e.which == 119) {
            fnBuildRow();
        }
    });
    
    $(document).ready(function(){
        $('.form').find('button:submit').click(function(){
            var msg = isValidSubmit();
            if( msg!='' )
            {
                showMsgWarning(msg); return false;
            }
            $.blockUI({ overlayCSS: { backgroundColor: '#fff' } }); 
        });
        
        $('.table_tr_row:first').find('.remove_icon_only').hide();
        fnBindRemoveIcon();
        fnBindAllAutocomplete();
        fnBindRemoveMaterialFix('materials_id_hide', 'material_autocomplete');
        
        $('.change_amount').live('keyup', function(){
            var change_amount_1 = $('.change_amount').eq(0).val();
            var change_amount_2 = $('.change_amount').eq(1).val();
            var  amount = parseFloat(change_amount_2-change_amount_1);
            amount = Math.round(amount * 100) / 100;
            $('.amount_gas').text(amount);
        });
        
        $('.change_amount_row').live('keyup', function(){
            var tr = $(this).closest('tr');
            var change_amount_1 = tr.find('.change_amount_row').eq(0).val();
            var change_amount_2 = tr.find('.change_amount_row').eq(1).val();
            var  amount = parseFloat(change_amount_2-change_amount_1);
                amount = Math.round(amount * 100) / 100;
            tr.find('.amount_gas').text(amount);
        });
        
    });       
    
    function fnBuildRow(){
        var tr_new = $('.table_tr_row:first').clone();
        tr_new.find('.remove_icon_only').css({'display':'block'});
        tr_new.find('.amount_empty').val('');
        tr_new.find('.amount_has_gas').val('');
        tr_new.find('.seri').val('');        
        tr_new.find('.amount_gas').text('');

        tr_new.find('.materials_id_hide').val('');
        tr_new.find('.material_autocomplete').attr('readonly',false).val('');
        
        tr_new.removeClass('selected');
        tr_new.find('.error').removeClass('error');
        $('.materials_table tbody').append(tr_new);        
        fnRefreshOrderNumber();
        fnBindAllAutocomplete();
    }
    
    function isValidSubmit(){
        var msg = '';
        $('.table_tr_row').each(function(){            
            var seri = $.trim($(this).find('.seri').val());
            var amount_empty = $.trim($(this).find('.amount_empty').val());
            var amount_has_gas = $.trim($(this).find('.amount_has_gas').val());
            var materials_id = $.trim($(this).find('.materials_id_hide').val());
            
//            if(materials_id==''){
//                msg = 'Chưa Chọn Vật Tư';
//                $(this).find('.material_autocomplete').addClass('error');
//            }else{
//                $(this).find('.material_autocomplete').removeClass('error');
//            }
            
            if(seri==''){
                msg = 'Chưa nhập đủ dữ liệu';
                $(this).find('.seri').addClass('error');
            }else{
                $(this).find('.seri').removeClass('error');
            }
                       
            if(amount_empty=='' ){
                msg = 'Chưa nhập đủ dữ liệu';
                $(this).find('.amount_empty').addClass('error');
            }else{
                $(this).find('.amount_empty').removeClass('error');
            }
            if(amount_has_gas=='' ){
                msg = 'Chưa nhập đủ dữ liệu';
                $(this).find('.amount_has_gas').addClass('error');
            }else{
                $(this).find('.amount_has_gas').removeClass('error');
            }
            
//            if( amount_has_gas<amount_empty){
//                msg = 'Khối lượng bình gas không đúng';
//                $(this).find('.amount_empty').addClass('error');
//                $(this).find('.amount_has_gas').addClass('error');
//            }else{
//                if(amount_has_gas!='' && amount_empty!=''){
//                    $(this).find('.amount_empty').removeClass('error');
//                    $(this).find('.amount_has_gas').removeClass('error');
//                }
//            }             
            
            
//            if( parseInt($(this).val())*1==0){
//                msg = 'Số lượng vật tư phải lớn hơn 0';
//                $(this).addClass('error');
//                return false;
//            }else{
//                $(this).removeClass('error');
//            }
            
        });
        return msg;
    }    
    
    
    // to do bind autocompelte for input material
    // @param objInput : is obj input ex  $('.customer_autocomplete')    
    function bindMaterialAutocomplete(objInput){
        var availableMaterials = <?php echo MyFunctionCustom::getMaterialsJsonByType($material_type); ?>;
        var parent_div = objInput.closest('.col_material');
        objInput.autocomplete({
            source: availableMaterials,
//            close: function( event, ui ) { $( "#GasStoreCard_materials_name" ).val(''); },
            select: function( event, ui ) {
                parent_div.find('.materials_id_hide').val(ui.item.id);
                parent_div.find('.material_autocomplete').attr('readonly',true);
            }
        });
    }
    
    function fnBindAllAutocomplete(){
        // material
        $('.material_autocomplete').each(function(){
            bindMaterialAutocomplete($(this));
        });
    }
    
</script>