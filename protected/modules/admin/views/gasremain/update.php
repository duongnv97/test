<?php
$this->breadcrumbs=array(
	'Quản Lý Gas Dư'=>array('index'),
	'Seri: '.$model->seri=>array('view','id'=>$model->id),
	'Cập Nhật',
);

$menus = array(	
        array('label'=>'GasRemain Management', 'url'=>array('index')),
	array('label'=>'Xem GasRemain', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Tạo Mới GasRemain', 'url'=>array('create')),	
);
$this->menu= ControllerActionsName::createMenusRoles($menus, $actions);

?>

<h1>Cập Nhật Gas Dư Bình Seri: <?php echo $model->seri; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>