<?php 
    
?>
<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=> GasCheck::getCurl(),
	'method'=>'get',
));
$needMore = ['status'=>1, 'GetNameOnly'=>1];
$cRole = MyFormat::getCurrentRoleId();
$aRoleLimit = [ROLE_SUB_USER_AGENT, ROLE_CRAFT_WAREHOUSE];
if(!in_array($cRole, $aRoleLimit)){
    $needMore['get_all'] = 1;
}
$aCar = $model->getListdataCar(MyFormat::getAgentId(), '',$needMore);
?>
    
    <div class="row">
    <?php echo $form->labelEx($model,'customer_id'); ?>
    <?php echo $form->hiddenField($model,'customer_id', array('class'=>'')); ?>
    <?php
            // 1. limit search kh của sale
            $url = Yii::app()->createAbsoluteUrl('admin/ajax/search_user_by_code');
            if( in_array( Yii::app()->user->id, GasRemain::$UID_CAN_LAN_2_3)) {
                $url = Yii::app()->createAbsoluteUrl('admin/ajax/search_all_customer_storecard');
            }
            // widget auto complete search user customer and supplier
            $aData = array(
                'model'=>$model,
                'field_customer_id'=>'customer_id',
                'url'=> $url,
                'name_relation_user'=>'agent',
                'ClassAdd' => 'w-400',
                'field_autocomplete_name' => 'autocomplete_name',
            );
            $this->widget('ext.SpaAutocompleteCustomer.SpaAutocompleteCustomer',
                array('data'=>$aData));
            ?>
    </div>
    
    <div class="row more_col">
        <?php if( MyFormat::getCurrentRoleId() != ROLE_SUB_USER_AGENT || in_array( MyFormat::getAgentId(), GasRemain::$agent_view_all) ):?>
        <div class="col1">
            <?php // echo $form->label($model,'agent_id',array()); ?>
            <?php // echo $form->dropDownList($model,'agent_id', Users::getSelectByRole(),array('style'=>'width:250px;','empty'=>'Select')); ?>
        </div>
        <div class="row">
        <?php echo $form->labelEx($model,'agent_id'); ?>
        <?php echo $form->hiddenField($model,'agent_id', array('class'=>'')); ?>
        <?php
                // 1. limit search kh của sale
                $url = Yii::app()->createAbsoluteUrl('admin/ajax/search_by_role', array('role'=> ROLE_AGENT));
                // widget auto complete search user customer and supplier
                $aData = array(
                    'model'=>$model,
                    'field_customer_id'=>'agent_id',
                    'url'=> $url,
                    'name_relation_user'=>'agent',
                    'ClassAdd' => 'w-400',
                    'field_autocomplete_name' => 'autocomplete_name_1',
                    'placeholder'=>'Nhập mã hoặc tên đại lý',
                );
                $this->widget('ext.SpaAutocompleteCustomer.SpaAutocompleteCustomer',
                    array('data'=>$aData));
                ?>
        </div>


        <?php endif;?>
        <?php /*
        <div class="col2">
            <?php echo $form->label($model,'update_status_id',array()); ?>		
            <?php echo $form->dropDownList($model,'update_status_id', Users::getSelectByRole(),array('style'=>'width:250px;','empty'=>'Select')); ?>
        </div>
        <?php endif;?>

        <div class="col3">
            <?php /*echo $form->label($model,'update_status_date',array()); ?>
            <?php 
                $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                    'model'=>$model,        
                    'attribute'=>'update_status_date',
                    'options'=>array(
                        'showAnim'=>'fold',
                        'dateFormat'=> MyFormat::$dateFormatSearch,
                        'maxDate'=> '0',
                        'changeMonth' => true,
                        'changeYear' => true,
                        'showOn' => 'button',
                        'buttonImage'=> Yii::app()->theme->baseUrl.'/admin/images/icon_calendar_r.gif',
                        'buttonImageOnly'=> true,                                
                    ),        
                    'htmlOptions'=>array(
                        'class'=>'w-16',
                        'style'=>'height:20px;width:120px;',
                    ),
                ));
            ?>		
        </div>

         */?>
    </div>
    
    <div class="row more_col">
        <div class="col1">
                <?php echo Yii::t('translation', $form->label($model,'date_from')); ?>
                <?php 
                    $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                        'model'=>$model,        
                        'attribute'=>'date_from',
                        'options'=>array(
                            'showAnim'=>'fold',
                            'dateFormat'=> MyFormat::$dateFormatSearch,
//                            'minDate'=> '0',
                            'maxDate'=> '0',
                            'changeMonth' => true,
                            'changeYear' => true,
                            'showOn' => 'button',
                            'buttonImage'=> Yii::app()->theme->baseUrl.'/admin/images/icon_calendar_r.gif',
                            'buttonImageOnly'=> true,                                
                        ),        
                        'htmlOptions'=>array(
                            'class'=>'w-16',
                            'size'=>'16',
                            'style'=>'float:left;',                               
                        ),
                    ));
                ?>     		
        </div>
        <div class="col2">
                <?php echo Yii::t('translation', $form->label($model,'date_to')); ?>
                <?php 
                    $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                        'model'=>$model,        
                        'attribute'=>'date_to',
                        'options'=>array(
                            'showAnim'=>'fold',
                            'dateFormat'=> MyFormat::$dateFormatSearch,
//                            'minDate'=> '0',
                            'maxDate'=> '0',
                            'changeMonth' => true,
                            'changeYear' => true,
                            'showOn' => 'button',
                            'buttonImage'=> Yii::app()->theme->baseUrl.'/admin/images/icon_calendar_r.gif',
                            'buttonImageOnly'=> true,                                
                        ),        
                        'htmlOptions'=>array(
                            'class'=>'w-16',
                            'size'=>'16',
                            'style'=>'float:left;',
                        ),
                    ));
                ?>     		
        </div>
    </div>

	<div class="row more_col">
            <div class="col1 display_none">
		<?php echo $form->label($model,'date_input',array()); ?>
		<?php 
//                    $this->widget('zii.widgets.jui.CJuiDatePicker', array(
//                        'model'=>$model,        
//                        'attribute'=>'date_input',
//                        'options'=>array(
//                            'showAnim'=>'fold',
//                            'dateFormat'=> MyFormat::$dateFormatSearch,
////                            'minDate'=> '0',
//                            'maxDate'=> '0',
//                            'changeMonth' => true,
//                            'changeYear' => true,
//                            'showOn' => 'button',
//                            'buttonImage'=> Yii::app()->theme->baseUrl.'/admin/images/icon_calendar_r.gif',
//                            'buttonImageOnly'=> true,                                
//                        ),        
//                        'htmlOptions'=>array(
//                            'class'=>'w-16',
//                            'style'=>'height:20px;width:120px;',
//                        ),
//                    ));
                ?>
            </div>
            <div class="col3 display_none">
                <?php echo $form->labelEx($model,'only_gas_remain', array()); ?>
                <?php echo $form->dropDownList($model,'only_gas_remain', GasRemain::$SELECT_ONLY_GAS_REMAIN, array('class'=>'w-200', 'empty'=>"Select")); ?>
            </div>
	</div>
    
    <div class="row">
        <?php echo $form->labelEx($model,'sale_id'); ?>
        <?php echo $form->hiddenField($model,'sale_id', array('class'=>'')); ?>
        <?php
            // 1. limit search kh của sale
            $url = Yii::app()->createAbsoluteUrl('admin/ajax/search_sale');
            // widget auto complete search user customer and supplier
            $aData = array(
                'model'=>$model,
                'field_customer_id'=>'sale_id',
                'url'=> $url,
                'name_relation_user'=>'rSale',
                'ClassAdd' => 'w-400',
                'field_autocomplete_name' => 'autocomplete_name_2',
            );
            $this->widget('ext.SpaAutocompleteCustomer.SpaAutocompleteCustomer',
                array('data'=>$aData));
            ?>
    </div>
    
    <div class="row">
        <?php echo $form->label($model,'driver_id'); ?>
        <?php echo $form->hiddenField($model,'driver_id', array('class'=>'')); ?>
        <?php
            // 1. limit search kh của sale
            $url = Yii::app()->createAbsoluteUrl('admin/ajax/search_by_role', array('role'=> ROLE_DRIVER));
            // widget auto complete search user customer and supplier
            $aData = array(
                'model'=>$model,
                'field_customer_id'=>'driver_id',
                'url'=> $url,
                'name_relation_user'=>'rDriver',
                'ClassAdd' => 'w-400',
                'field_autocomplete_name' => 'date_update_3',
                'placeholder'=>'Nhập mã hoặc tên tài xế',
            );
            $this->widget('ext.SpaAutocompleteCustomer.SpaAutocompleteCustomer',
                array('data'=>$aData));
        ?>
    </div>
    
	<div class="row more_col">
            <div class="col1">
		<?php echo $form->label($model,'created_date',array()); ?>
                <?php 
                    $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                        'model'=>$model,        
                        'attribute'=>'created_date',
                        'options'=>array(
                            'showAnim'=>'fold',
                            'dateFormat'=> MyFormat::$dateFormatSearch,
                            'maxDate'=> '0',
                            'changeMonth' => true,
                            'changeYear' => true,
                            'showOn' => 'button',
                            'buttonImage'=> Yii::app()->theme->baseUrl.'/admin/images/icon_calendar_r.gif',
                            'buttonImageOnly'=> true,                                
                        ),        
                        'htmlOptions'=>array(
                            'class'=>'w-180',
                            'style'=>'height:20px;',
                        ),
                    ));
                ?>		
            </div>
            <div class="col2">
                <?php echo $form->labelEx($model,'car_id'); ?>
                <?php echo $form->dropDownList($model,'car_id', $aCar,array('class'=>'w-200 float_l', 'empty'=>'Select')); ?>
            </div>
            <div class="col3">
                <?php echo $form->labelEx($model,'notReweighed',array('class'=>'checkbox_one_label', 'style'=>'padding-top:3px;')); ?>
                <?php echo $form->checkBox($model,'notReweighed',array('class'=>'float_l')); ?>
            </div>
	</div>
    
        <div class="row more_col">
            <div class="col1">
		<?php echo $form->label($model,'seri',array()); ?>
		<?php echo $form->textField($model,'seri',array('class'=>'w-200','maxlength'=>25)); ?>
            </div>
            <div class="col2">
                <?php echo $form->label($model,'ext_month'); ?>
                <?php echo $form->dropDownList($model,'ext_month', ActiveRecord::getMonthVnWithZeroFirst(), array('class'=>'w-200', 'empty'=>'Select')); ?>		
            </div>

            <div class="col3">
                <?php echo $form->labelEx($model,'ext_year'); ?>
                <?php echo $form->dropDownList($model,'ext_year', ActiveRecord::getRangeYear(), array('class'=>'w-200', 'empty'=>'Select')); ?>		
            </div>
        </div>
    
        <div class="row more_col">
            <?php 
                $aTypeCustomer      = CmsFormatter::$CUSTOMER_BO_MOI;
                $aTypeCustomer[-1]  = 'Không phân loại KH';
                
            ?>    
            <div class="col1">
                <?php echo $form->label($model,'type_customer', array('label'=>'Loại KH')); ?>
                <?php echo $form->dropDownList($model,'type_customer', $aTypeCustomer,array('class'=>'w-200', 'empty'=>'Select')); ?>
            </div>
            <div class="col2">
               <?php echo $form->labelEx($model,'ext_have_customer'); ?>
               <?php echo $form->dropDownList($model,'ext_have_customer', CmsFormatter::$yesNoFormat,array('class'=>'w-200', 'empty'=>'Select')); ?>
            </div>
            <div class="col3">
                <?php echo $form->labelEx($model,'NotViewWarehouse',array('class'=>'checkbox_one_label', 'style'=>'padding-top:3px;')); ?>
                <?php echo $form->checkBox($model,'NotViewWarehouse',array('class'=>'float_l')); ?>
            </div>
        </div>
    
        <div class="row">
            <?php echo $form->label($model,'has_export'); ?>
            <?php // echo $form->dropDownList($model,'has_export', GasRemain::$TYPE_HAS_EXPORT, array('class'=>'w-200', 'empty'=>'Select')); ?>
            <div class="fix-label">
                <?php
                   $this->widget('ext.multiselect.JMultiSelect',array(
                         'model'=>$model,
                         'attribute'=>'has_export',
                         'data'=> GasRemain::$TYPE_HAS_EXPORT,
                         // additional javascript options for the MultiSelect plugin
                        'options'=>array('selectedList' => 30,),
                         // additional style
                         'htmlOptions'=>array('style' => 'width: 200px;'),
                   ));    
               ?>
            </div>
        </div>
    
        <div class="row more_col display_none">
            <div class="col1">
                <?php echo $form->label($model,'date_update_2',array('label'=>'Ngày Cập Nhật')); ?>
		<?php 
                    $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                        'model'=>$model,        
                        'attribute'=>'date_update_2',
                        'options'=>array(
                            'showAnim'=>'fold',
                            'dateFormat'=> MyFormat::$dateFormatSearch,
//                            'minDate'=> '0',
                            'maxDate'=> '0',
                            'changeMonth' => true,
                            'changeYear' => true,
                            'showOn' => 'button',
                            'buttonImage'=> Yii::app()->theme->baseUrl.'/admin/images/icon_calendar_r.gif',
                            'buttonImageOnly'=> true,                                
                        ),        
                        'htmlOptions'=>array(
                            'class'=>'w-16',
                            'style'=>'height:20px;width:120px;',
                        ),
                    ));
                ?>                
            </div>
            <div class="col2">
                <?php echo $form->label($model,'user_update_2', array('label'=>'Người Cập Nhật')); ?>
                <?php echo $form->dropDownList($model,'user_update_2', Users::getListOptions(GasRemain::$UID_CAN_LAN_2_3),array('class'=>'w-200', 'empty'=>'Select')); ?>                
            </div>
        </div>
    
        <div class="row">
            <?php echo $form->labelEx($model,'province_id'); ?>
            <div class="fix-label">
                <?php
                   $this->widget('ext.multiselect.JMultiSelect',array(
                         'model'=>$model,
                         'attribute'=>'province_id',
                         'data'=> GasProvince::getArrAll(),
                         // additional javascript options for the MultiSelect plugin
                        'options'=>array('selectedList' => 30,),
                         // additional style
                         'htmlOptions'=>array('style' => 'width: 800px;'),
                   ));    
               ?>
            </div>
        </div>
        <div class="row more_col">
            <div class="col1">
		<?php echo $form->label($model,'code_no',array()); ?>
		<?php echo $form->textField($model,'code_no',array('class'=>'w-200','maxlength'=>25)); ?>
            </div>
            <div class="col2">
                <?php $aPageSize = GasConst::getPageSize(); 
                    unset($aPageSize[200]);
                ?>
                <?php echo $form->labelEx($model,'pageSize'); ?>
                <?php echo $form->dropDownList($model,'pageSize', $aPageSize,array('class'=>'w-200','empty'=>'Select')); ?>
            </div>
            <div class="col3">
            </div>
        </div>

	<div class="row buttons" style="padding-left: 159px;">
            <?php $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType'=>'submit',
            'label'=>'Search',
            'type'=>'null', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
            'size'=>'small', // null, 'large', 'small' or 'mini'
            //'htmlOptions' => array('style' => 'margin-bottom: 10px; float: right;'),
        )); ?>	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->