<?php
$this->breadcrumbs=array(
    $this->pluralTitle => array('index'),
    ' ' . $this->singleTitle . ' : ',
);

$menus = array(
	array('label'=>"$this->pluralTitle Management", 'url'=>array('index')),
	array('label'=>"Tạo Mới $this->singleTitle", 'url'=>array('frontCreate'), 'htmlOptions' => array('class'=>'create','label'=>'Tạo mới')),
	array('label'=>"Cập Nhật $this->singleTitle", 'url'=>array('frontCreate', 'type_id'=>$model->type_id, 'position_work_list'=>$model->position_work_list, 'position_room_list'=>$model->position_room_list), 'htmlOptions' => array('class'=>'update','label'=>'Update')),
	array('label'=>"Xóa $this->singleTitle", 'url'=>array('delete'), 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Bạn chắc chắn muốn xóa ?')),
);
$this->menu= ControllerActionsName::createMenusRoles($menus, $actions);
?>

<h1>Xem: <?php echo $this->singleTitle; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
            array(
                'name'=>'name',
                'type'=>'raw',
                'value'=>$model->getName(),
            ),
            array(
                'name'=>'function',
                'type'=>'raw',
                'value'=>$model->getUnderstandingFunction(),
            ),
            array(
                'name' => 'type_id',
                'type' => 'raw',
                'value' => $model->getType(),
            ),
            array(
                'name' => 'position_work',
                'type' => 'raw',
                'value' => $model->getPositionWork(),
            ),
            array(
                'name' => 'position_room_list',
                'type' => 'raw',
                'value' => $model->getPositionRoom(),
            ),
            array(
                'name' => 'is_per_day',
                'type' => 'raw',
                'value' => $model->isPerDayText(),
            ),
            array(
                'name'=>'created_by',
                'type'=>'raw',
                'value'=>$model->getUser(),
            ),
            array(
                'name'=>'status',
                'type'=>'raw',
                'value'=>$model->getStatus(),
            ),
            array(
                'name'=>'created_date',
                'type'=>'raw',
                'value'=>$model->getCreatedDate(),
            ),
	),
)); ?>
