<?php
$this->breadcrumbs = array(
    DomainConst::CONTENT10016 => array('index'),
    DomainConst::CONTENT10018
);
$menus = array(
    array('label' => DomainConst::CONTENT10016,
        'url' => array('index'),
        'htmlOptions' => array('class' => 'index ')),
);
$this->menu = ControllerActionsName::createMenusRoles($menus, $actions);
$scriptUrl = Yii::app()->theme->baseUrl . '/admin/js/hr.js';
Yii::app()->clientScript->registerScriptFile($scriptUrl);

$css = Yii::app()->theme->baseUrl . '/admin/css/hr.css';
Yii::app()->clientScript->registerCssFile($css);
?>

<h1>Tạo Mới <?php echo $this->singleTitle; ?></h1>

<div class="form">
    <?php
    $form = $this->beginWidget('CActiveForm', array(
        'id' => 'sell-form',
        'enableAjaxValidation' => false,
        'htmlOptions' => array('enctype' => 'multipart/form-data'),
    ));
    ?>
    <?php echo MyFormat::BindNotifyMsg(); ?>

    <?php
    $mFakeParameters = new HrParameters('Search');
    $mFakeCoefficients = new HrCoefficients('Search');
    $aParameters = $mFakeParameters->getParamsByPosition();
    $aCoefficients = $mFakeCoefficients->getCoefByPosition();

    $countFunction = 0;
    $aFunction = [];
    if($model->scenario == 'update')
        $aFunction = $model->getFuncUpdate();
    ?>
    
    <div class="row">
        <?php echo $form->labelEx($model, 'type_id'); ?>
        <?php echo $form->dropDownList($model, 'type_id', HrFunctionTypes::loadItems(false, true), array('class' => 'w-300','empty'=>'Select')); ?>
        <?php echo $form->error($model, 'type_id'); ?>
    </div>
    
<!--    <div class="row">
        <?php // echo $form->labelEx($model,'position_work'); ?>
        <?php // echo $form->dropdownList($model,'position_work', UsersProfile::model()->getArrWorkRoom(), array('class'=>'number_only_v1 w-300 type_selecter','empty'=>'Select')); ?>
        <?php // echo $form->error($model,'position_work'); ?>
    </div>-->
    
    <div class="row">
        <?php echo $form->labelEx($model, 'position_work_list'); ?>
        <div class="fix-label">
            <?php
            $this->widget('ext.multiselect.JMultiSelect', array(
                'model' => $model,
                'attribute' => 'position_work_list',
                'data' => UsersProfile::model()->getArrWorkRoom(),
                // additional javascript options for the MultiSelect plugin
                'options' => array('selectedList' => 30),
                // additional style
                'htmlOptions' => array('class' => 'w-400'),
            )); 
            ?>
        </div>
        <?php echo $form->error($model, 'position_work_list'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model, 'position_room_list'); ?>
        <div class="fix-label">
            <?php
            $this->widget('ext.multiselect.JMultiSelect', array(
                'model' => $model,
                'attribute' => 'position_room_list',
                'data' => UsersProfile::model()->getArrPositionRoom(),
                // additional javascript options for the MultiSelect plugin
                'options' => array('selectedList' => 30),
                // additional style
                'htmlOptions' => array('class' => 'w-400'),
            )); 
            ?>
        </div>
        <?php echo $form->error($model, 'position_room_list'); ?>
    </div>
    
    <div class="row">
        <?php echo $form->labelEx($model,'search_position', ['label'=>'Xem tham số, hệ số']); ?>
        <?php echo $form->dropdownList($model,'search_position', UsersProfile::model()->getArrWorkRoom(), array('class'=>'w-300 type_selecter','empty'=>'Select')); ?>
        <?php echo $form->error($model,'search_position'); ?>
        <i>** Trường này chỉ dùng để load tham số, hệ số, không liên quan tới phần cài đặt công thức!</i>
    </div>
    
<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/admin/css/dragndrop.css" />
<div class="gird-view" >
    <div class="prams-coef-contaner">

        <!--Table tham số-->
        <div class="pc-col">
            <table><!--class="materials_table hm_table"-->
                <thead>
                    <tr class="table item" style="background: white;">
                        <th style="height:30px;text-align: center;">Danh sách tham số</th>
                    </tr>
                </thead>
                <tbody>
                    <tr class="table item" style="background: white;">
                        <td class="params-container">
                            <?php foreach ($aParameters as $key => $mParameters) { ?>
                                <span class="dragThamSoItem" id="TS<?php echo $mParameters->id; ?>" data-id="<?php echo $mParameters->id; ?>"><?php echo $mParameters->getName(); ?></span>
                            <?php } ?>
                        </td>
                    </tr>
                </tbody>  
            </table>
        </div>

        <!--Table hệ số-->
        <div class="pc-col">
            <table><!--class="materials_table hm_table"-->
                <thead>
                    <tr class="table item" style="background: white;">
                        <th style="height:30px;text-align: center; position:relative;">
                            Danh sách hệ số
                            <a href="<?php echo Yii::app()->createAbsoluteUrl('/admin/hrCoefficients/create', array('ajax' => true)); ?>" class="text_under_none item_b addFactorBtn" style="line-height:25px; position: absolute; right: 5px;">
                                <img style="float: left;margin-right:8px;" src="<?php echo yii::app()->theme->baseUrl; ?>/images/add.png"> 
                                Thêm hệ số
                            </a>
                        </th>
                    </tr>
                </thead>
                <tbody>
                    <tr class="table item" style="background: white;">
                        <td class="coef-container" style="position: relative;">
                            <?php foreach ($aCoefficients as $key => $mCoefficients) { ?>
                                <span class="dragHeSoItem" id="HS<?php echo $mCoefficients->id; ?>" data-id="<?php echo $mCoefficients->id; ?>"><?php echo $mCoefficients->getName(); ?></span>
                            <?php } ?>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>

    </div>


    <div class="row more_col" style="margin-bottom: 5px!important;">
        <div class="col2" style="width:100%;padding-bottom:20px">

            <!--Table công thức-->
            <table class="materials_table hm_table" style="width: 100%!important">
                <thead>
                    <tr class="table item item_c">
                        <th style="width: 5%">Sắp xếp</th>
                        <th style="width: 18%">Tên</th>
                        <th style="width: 20%">Tham số</th>
                        <th style="width: 18%">Hệ Số</th>
                        <th style="width: 24%">Công thức</th>
                        <!--Cột tính theo ngày (Mar 7,2019 ẩn đi vì không dùng nữa)-->
                        <!--<th style="width: 10%"><?php // echo DomainConst::CONTENT10021; ?></th>-->
                        <th style="width: 5%">Action</th>
                    </tr>
                </thead>
                <tbody class="tinhluongBody">
                    
                    <!--Danh sách nếu đã có-->
                    <?php
                    foreach ($aFunction as $key => $mFunctionDraw) {
                        $aParameterOfFunction    = !empty($mFunctionDraw) ? $mFunctionDraw->rParameters : null;
                        $aCoefficientsOfFunction = !empty($mFunctionDraw) ? $mFunctionDraw->rCoefficients : null;
                    ?>
                    <input type="hidden" name="HrFunctions[old_code_no][]" value="<?php echo $mFunctionDraw->getCodeNo(); ?>">
                    <tr class="table item">
                        <!--Cột sắp xếp-->
                        <td>
                            <div class="icon-group">
                                <div class="icon up go-up"></div>
                                <div class="icon down go-down"></div>
                            </div>
                        </td>
                        <!--Cột tên-->
                        <td>
                            <input style='width: 97%' name="HrFunctions[function][<?php echo $countFunction; ?>][name]" type="text" value="<?php echo isset($mFunctionDraw) ? $mFunctionDraw->getName() : ''; ?>" class="formula">
                            <p><b>Code no: </b><?php echo $mFunctionDraw->getCodeNo(); ?></p>
                            <input type="hidden" name="HrFunctions[function][<?php echo $countFunction; ?>][code_no]" value="<?php echo $mFunctionDraw->getCodeNo(); ?>">
                        </td>
                        <!--Cột tham số-->
                        <td class="dragThamSoArea dragTarget" data-current="<?php echo $countFunction; ?>">
                            <?php
                            if (!empty($aParameterOfFunction)) {
                                foreach ($aParameterOfFunction as $key => $mParameter) {
                                    ?>
                                    <span class="btnDefault alreadyIn" data-id="<?php echo $mParameter->id; ?>" draggable="true">TS<?php echo $key + 1; ?>: <?php echo $mParameter->name; ?>
                                        <input class="display_none" name="HrFunctions[function][<?php echo $countFunction; ?>][TS][]" value="<?php echo $mParameter->id; ?>">
                                    </span>
                                    <?php
                                }
                            }
                            ?>
                        </td>
                        <!--Cột hệ số-->
                        <td class="dragHeSoArea dragTarget" data-current="<?php echo $countFunction; ?>">
                            <?php
                            if (!empty($aCoefficientsOfFunction)) {
                                foreach ($aCoefficientsOfFunction as $key => $mCoefficients) {
                                    ?>
                                    <span class="btnDefault alreadyIn" data-id="<?php echo $mCoefficients->id; ?>">HS<?php echo $key + 1; ?>: <?php echo $mCoefficients->name; ?>
                                        <input class="display_none" name="HrFunctions[function][<?php echo $countFunction; ?>][HS][]" value="<?php echo $mCoefficients->id; ?>">
                                    </span>
                                    <?php
                                }
                            }
                            ?>
                        </td>
                        <!--Cột tên công thức-->
                        <td>
                            <textarea style="width:97%;text-transform:uppercase;resize:vertical;" name="HrFunctions[function][<?php echo $countFunction; ?>][func]" type="text" class="formula"><?php echo isset($mFunctionDraw) ? $mFunctionDraw->getFunction() : ''; ?></textarea>
                        </td>
                        <!--Cột tính theo ngày (Mar 7,2019 ẩn đi vì không dùng nữa)-->
<!--                        <td>
                            <input style='width: 97%'
                                   name="HrFunctions[function][<?php // echo $countFunction; ?>][is_per_day]"
                                   type="checkbox" value="1"
                                   <?php // echo ($mFunctionDraw->is_per_day) ? 'checked' : ''; ?>>
                        </td>-->
                        <!--Cột action-->
                        <td>
                            <span class="remove_icon_only"></span>
                        </td>
                    </tr>
                    <?php
                        $countFunction++;
                    }
                    ?>

                    <!-- Row mới nếu rỗng -->
                    <?php if ($countFunction == 0) { ?>
                    <tr class="table item">
                        <!--Cột sắp xếp-->
                        <td>
                            <div class="icon-group">
                                <div class="icon up go-up"></div>
                                <div class="icon down go-down"></div>
                            </div>
                        </td>
                        <!--Cột tên-->
                        <td>
                            <input style='width: 97%' name="HrFunctions[function][<?php echo $countFunction; ?>][name]" type="text" value="<?php echo isset($mFunction) ? $mFunction->function : ''; ?>" class="formula">
                        </td>
                        <!--Cột tham số-->
                        <td class="dragThamSoArea dragTarget" data-current="<?php echo $countFunction; ?>">
                        </td>
                        <!--Cột hệ số-->
                        <td class="dragHeSoArea dragTarget" data-current="<?php echo $countFunction; ?>">
                        </td>
                        <!--Cột tên công thức-->
                        <td>
                            <textarea style="width:97%;text-transform:uppercase;resize:vertical;" name="HrFunctions[function][<?php echo $countFunction; ?>][func]" type="text" class="formula"><?php echo isset($mFunction) ? $mFunction->function : ''; ?></textarea>
                        </td>
                        <!--Cột tính theo ngày (Mar 7,2019 ẩn đi vì không dùng nữa)-->
<!--                        <td>
                            <input style='width: 97%'
                                   name="HrFunctions[function][<?php // echo $countFunction; ?>][is_per_day]"
                                   type="checkbox" value="1">
                        </td>-->
                        <!--Cột action-->
                        <td>
                            <span class="remove_icon_only"></span>
                        </td>
                    <?php $countFunction++; ?>
                    </tr>
                    <?php } ?>
                </tbody>
            </table>

            <a href="javascript:void(0);" class="text_under_none item_b addnewBtn" style="line-height:25px">
                <img style="float: left;margin-right:8px;" src="<?php echo yii::app()->theme->baseUrl; ?>/images/add.png"> 
                Thêm Dòng
            </a>
        </div>
    </div>

    <!--    <div class="row">
    <?php echo $form->labelEx($model, 'type_id'); ?>
    <?php echo $form->dropDownList($model, 'type_id', $model->getArrayType(), array('class' => 'w-300')); ?>
    <?php echo $form->error($model, 'type_id'); ?>
        </div>-->

    <!--        <div class="row">
    <?php echo $form->labelEx($model, 'is_per_day'); ?>
    <?php echo $form->checkBox($model, 'is_per_day'); ?>
    <?php echo $form->error($model, 'is_per_day'); ?>
            </div>-->

    <div class="row buttons">
        <?php
        $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType' => 'submit',
            'label' => DomainConst::CONTENT00377,
            'type' => 'null', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
            'size' => 'small', // null, 'large', 'small' or 'mini'
            'htmlOptions' => array(
                'name' => 'saveBtn',
                'value'=> 1
                ),
        ));
        ?>	
    </div>
</div>
<?php $this->endWidget(); ?>
</div>
<script type="text/javascript">
    currentKey = <?php echo $countFunction; ?>;
    $(document).ready(function () {
        allowDrag("dragThamSoItem", "dragThamSoArea", "id", "TS", true);
        allowDrag("dragHeSoItem", "dragHeSoArea", "id", "HS", true);
        $('.addnewBtn').off('click').on('click', function () {
            var newTr =
                    "<tr class='table item'>" +
//                    Cột sắp xếp
                    "<td>"+
                        "<div class='icon-group'>"+
                            "<div class='icon up go-up'></div>"+
                            "<div class='icon down go-down'></div>"+
                        "</div>"+
                    "</td>"+
//                    Cột tên công thức
                    "<td>" +
                        "<input style='width:97%' " +
                            " name=HrFunctions[function][" + currentKey + "][name] " +
                            " type='text' value='' class='formula'> " +
                    "</td>" +
//                    Cột tham số
                    "<td data-current=" + currentKey + " class='dragThamSoArea'></td>" +
//                    Cột hệ số
                    "<td class='dragHeSoArea' data-current=" + currentKey + "></td>" +
//                    Cột tên công thức
                    "<td>" +
                        "<textarea style='text-transform:uppercase;width:97%;resize:vertical;' " +
                            "type='text' " +
                            "name=HrFunctions[function][" + currentKey + "][func] " +
                            "class='formula'></textarea>" +
                    "</td>" +
//                    Cột tính theo ngày (Mar 7,2019 ẩn đi vì không dùng nữa)
//                    "<td>" +
//                        "<input style='width: 97%'"+
//                            "name='HrFunctions[function][" + currentKey + "][is_per_day]'"+
//                            "type='checkbox' value='1'>" +
//                    "</td>" +
                    "<td><span class='remove_icon_only'></span></td></tr>"
            $('.tinhluongBody').append(newTr);
            currentKey++
            removeRow();
        });
        
        var pos = $('.type_selecter').val();
        reloadCoefficients(pos);
        reloadParameters(pos);
        
        $(document).on('dblclick', '.dragHeSoItem', function () {
            var pos = $('.type_selecter').val();
            $.colorbox({
                href: '<?php echo Yii::app()->createAbsoluteUrl('/admin/hrCoefficients/update'); ?>/id/' + $(this).data('id') + '/ajax/1',
                iframe: true,
                overlayClose: false, escKey: false,
                innerHeight: '400',
                innerWidth: '600',
                close: "<span title='close'>close</span>",
                onClosed: function () { // update view when close colorbox
                    reloadCoefficients(pos);
                }
            });
        });
        
        $(".addFactorBtn").colorbox({
            iframe: true,
            overlayClose: false, escKey: false,
            innerHeight: '400',
            innerWidth: '600', close: "<span title='close'>close</span>",
            onClosed: function () { // update view when close colorbox
                var pos = $('.type_selecter').val();
                reloadCoefficients(pos);
            }
        });
        
            
        $('body').on('change', '.type_selecter', function(){
            var pos = $(this).val();
            $.blockUI({ overlayCSS: { backgroundColor: '#fff'}});
            reloadCoefficients(pos);
            reloadParameters(pos);
        });
    
        removeRow();
        fnBindMoveRow();
    });

    function reloadCoefficients(pos) {
        $.ajax({
            'url': '<?php echo Yii::app()->createAbsoluteUrl("admin/hrFunctions/frontCreate/ajax/1", ['type'=> HrFunctions::KEYWORD_COEFFICIENT]); ?>' + '/pos/'+pos,
            'type': 'get',
            'success': function (html) {
                $('.coef-container').html(html);
                $.unblockUI();
            }
        });
    }
    
    function reloadParameters(pos) {
        $.ajax({
            'url': '<?php echo Yii::app()->createAbsoluteUrl("admin/hrFunctions/frontCreate/ajax/1", ['type'=> HrFunctions::KEYWORD_PARAM]); ?>' + '/pos/'+pos,
            'type': 'get',
            'success': function (html) {
                $('.params-container').html(html);
                $.unblockUI();
            }
        });
    }
</script>

