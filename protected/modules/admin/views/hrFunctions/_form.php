<div class="form">

    <?php
    $form = $this->beginWidget('CActiveForm', array(
        'id' => 'hr-functions-form',
        'enableAjaxValidation' => false,
        'htmlOptions' => array('enctype' => 'multipart/form-data'),
    ));
    ?>

    <?php echo DomainConst::CONTENT00081; ?>
    <?php echo MyFormat::BindNotifyMsg(); ?>

    <div class="row">
        <?php echo $form->labelEx($model, 'function'); ?>
        <?php echo $form->textArea($model, 'function', array('rows' => 6, 'cols' => 50)); ?>
        <?php echo $form->error($model, 'function'); ?>
    </div>
    <div class="row">
        <?php echo $form->labelEx($model, 'role_id'); ?>
        <?php echo $form->dropDownList($model, 'role_id', $model->getArrayRoles(), array('class' => 'w-300')); ?>
        <?php echo $form->error($model, 'role_id'); ?>
    </div>
    <div class="row">
        <?php echo $form->labelEx($model, 'name'); ?>
        <?php echo $form->textArea($model, 'name', array('rows' => 6, 'cols' => 50)); ?>
        <?php echo $form->error($model, 'name'); ?>
    </div>
    <div class="row">
        <?php echo $form->labelEx($model, 'type_id'); ?>
        <?php echo $form->dropDownList($model, 'type_id', $model->getArrayType(), array('class' => 'w-300')); ?>
        <?php echo $form->error($model, 'type_id'); ?>
    </div>
    <div class="row">
        <?php echo $form->labelEx($model, 'status'); ?>
        <?php echo $form->dropDownList($model, 'status', $model->getArrayStatus(), array('class' => 'w-300')); ?>
        <?php echo $form->error($model, 'status'); ?>
    </div>
    <div class="row">
        <?php echo $form->labelEx($model, 'is_per_day'); ?>
        <?php
        echo $form->checkBox($model, 'is_per_day',
                (!empty($model->is_per_day)
                && ($model->is_per_day == HrFunctions::FUNCTION_TYPE_COUNT ))
                ? array('checked' => 'checked') : array());
        ?>
        <?php echo $form->error($model, 'is_per_day'); ?>
    </div>
    <div class="row buttons" style="padding-left: 141px;">
        <?php
        $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType' => 'submit',
            'label' => $model->isNewRecord ? 'Create' : 'Save',
            'type' => 'null', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
            'size' => 'small', // null, 'large', 'small' or 'mini'
                //'htmlOptions' => array('style' => 'margin-bottom: 10px; float: right;'),
        ));
        ?>	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->
<script>
    $(document).ready(function () {
        $('.form').find('button:submit').click(function () {
            $.blockUI({overlayCSS: {backgroundColor: '#fff'}});
        });

    });
</script>