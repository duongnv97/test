<?php

$this->breadcrumbs=array(
	$this->singleTitle,
);
$scriptUrl = Yii::app()->theme->baseUrl . '/admin/css/hr.css';
Yii::app()->clientScript->registerCssFile($scriptUrl);
$menus = array(
    array('label' => "Tạo mới $this->singleTitle", 'url' => array('frontCreate')),
);
$menus=array(
            array('label'=> "Tạo mới $this->singleTitle",
               'url'=>array('frontCreate'), 
               'htmlOptions'=>array('class'=>'create ','label'=>"Tạo mới")),
);

$this->menu= ControllerActionsName::createMenusRoles($menus, $actions);
Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
");
Yii::app()->clientScript->registerScript("create", "
    $('.create').click(function(e){
        var type = $('#typeTabs li.active a').data('type');
        var url = $('.create a').attr('href') +'/role/'+ $('#lv'+type+'Tab li.active a.tabLink').data('role')+'/type/'+type;
        $('.create a').attr('href',url);
    })
");

?>

<?php echo MyFormat::BindNotifyMsg();?>
<h1>Danh Sách <?php echo $this->pluralTitle; ?></h1>

 <!--    TAB-->
<div class='grid-view-loading' style="display: none;" id='loading'></div>
<div id="data" style='padding: 20px'>

<?php
//wiget function types
$aT = [];
$class = 'mainTab';
$dataName = 'type';
$ajax_url_main = '/admin/HrFunctions/UpdateAjax/';
foreach ($aTab as $value) {
    $aT[$value->id] = $value->name;
}
$aNeedMore = array('role'=>$role);
$this->widget('TabWidget', array('ajax_url'=>$ajax_url_main, 
                                 'aTab'=>$aT,
                                 'class'=>$class,
                                 'dataName'=>$dataName,
                                 'dataValue'=>$type,
                                 'tabId'=>'typeTabs',
                                 'aNeedMore'=>$aNeedMore,
                                ));


?>

</div>

<script>
//    var style = '<style>'+
//                    'li:not(.active) a.<?php echo $class ?>{background: teal; font-size: 16px!important}'+
//                    'li.active a.<?php echo $class ?>{font-size: 16px!important}'+
//                    'li:not(.active) a.<?php echo $class ?>:hover{color: white!important; background: #016b6b!important}'+
//                    '#typeTabs .tab-content{padding: 20px; border-top: 1px solid #ddd; border-left: 1px solid #ddd; margin-left: 1px;margin-top: -1px;}'+
//                    '.nav-tabs{border:none}'+
//                '</style>';
//    $('head').append(style);    
</script>