
<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>
	<div class="row">
        <?php echo $form->labelEx($model,'name'); ?>
        <?php echo $form->textField($model,'name', array('class'=>'w-400')); ?>
        </div>
    
        <div class="row">
            <?php echo $form->labelEx($model,'function'); ?>
            <?php echo $form->textField($model,'function', array('class'=>'w-400')); ?>
        </div>
    
        <div class="row">
            <?php echo $form->labelEx($model,'position_work_list'); ?>
            <?php echo $form->dropdownList($model,'position_work_list',UsersProfile::model()->getArrWorkRoom(), array('class'=>'w-400', 'empty'=>'Select')); ?>
        </div>
    
        <div class="row">
            <?php echo $form->labelEx($model,'type_id'); ?>
            <?php echo $form->dropdownList($model,'type_id',HrFunctionTypes::loadItems(false, true), array('class'=>'w-400', 'empty'=>'Select')); ?>
        </div>
    
        <div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->