<?php $role = empty($_GET['role']) ? 3 : $_GET['role']; ?>
<?php $type = empty($_GET['type']) ? '' : $_GET['type']; ?>
<span id="current_role_id" data-value="<?php echo $role; ?>"></span>


<?php 
Yii::app()->clientScript->registerScript('search', "
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('Functions-grid".$type.$role."', {
		data: $(this).serialize()
	});
	return false;
});
");

$data = $model->getFunctionsByRole($role, $type);
$dataHTML = "";
$actions = array('Update','Delete');

$this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'Functions-grid'.$type.$role,
	'dataProvider'=>$data,
        'pager' => array(
            'maxButtonCount'=>  CmsFormatter::$PAGE_MAX_BUTTON,
        ),
	'columns'=>array(
            array(
                'header' => 'S/N',
                'type' => 'raw',
                'value' => '$this->grid->dataProvider->pagination->currentPage * $this->grid->dataProvider->pagination->pageSize + ($row+1)',
                'headerHtmlOptions' => array('width' => '10px','style' => 'text-align:center;'),
                'htmlOptions' => array('style' => 'text-align:center;')
            ),
            array(
                'name'=>'name',
                'type'=>'raw',
                'value'=>'$data->getName()',
            ),
            array(
                'name'=>'function',
                'type'=>'raw',
                'value'=>'$data->getUnderstandingFunction()',
            ),
//            array(
//                'name' => 'role_id',
//                'type' => 'raw',
//                'value' => '$data->getRole()',
//            ),
//            array(
//                'name' => 'type_id',
//                'type' => 'raw',
//                'value' => '$data->getType()',
//            ),
            array(
                'name' => 'is_per_day',
                'type' => 'raw',
                'value' => '$data->isPerDayText()',
            ),
            
            array(
                'name'=>'created_by',
                'type'=>'raw',
                'value'=>'$data->getUser()',
            ),
            array(
                'name'=>'status',
                'type'=>'raw',
                'value'=>'$data->getStatus()',
            ),
            array(
                'name'=>'created_date',
                'type'=>'raw',
                'value'=>'$data->getCreatedDate()',
            ),
            array(
            'header' => 'Actions',
            'class' => 'CButtonColumn',
            'template' => ControllerActionsName::createIndexButtonRoles($actions),
            'buttons' => array(
                    'update' => array(
                        'url' => 'Yii::app()->createUrl("admin/HrFunctions/frontCreate/", array("role"=>$data->role_id, "type"=>$data->type_id))',
                    ),   
                ),
            ),
	),
)); ?>