<?php
$this->breadcrumbs=array(
	$this->singleTitle,
);

$menus=array(
    array('label'=>"Tạo $this->singleTitle", 'url'=>array('frontCreate'), 'htmlOptions' => array('class'=>'create','label'=>'Tạo mới')),
);
$this->menu= ControllerActionsName::createMenusRoles($menus, $actions);
Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('Functions-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>
<?php echo MyFormat::BindNotifyMsg(); ?>
<h1>Danh Sách <?php echo $this->pluralTitle; ?></h1>

<?php echo CHtml::link('Tìm Kiếm Nâng Cao','#',array('class'=>'search-button')); ?>
    <div class="search-form" style="display:none">
    <?php $this->renderPartial('_search',array(
            'model'=>$model,
    )); ?>
    </div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'Functions-grid',
	'dataProvider'=>$model->search(),
        'pager' => array(
            'maxButtonCount'=>  CmsFormatter::$PAGE_MAX_BUTTON,
        ),
	'columns'=>array(
            array(
                'header' => 'S/N',
                'type' => 'raw',
                'value' => '$this->grid->dataProvider->pagination->currentPage * $this->grid->dataProvider->pagination->pageSize + ($row+1)',
                'headerHtmlOptions' => array('width' => '10px','style' => 'text-align:center;'),
                'htmlOptions' => array('style' => 'text-align:center;')
            ),
            array(
                'name'=>'name',
                'type'=>'raw',
                'value'=>'$data->getName()',
            ),
            array(
                'name'=>'function',
                'type'=>'raw',
                'value'=>'$data->getUnderstandingFunction()',
            ),
            array(
                'name' => 'type_id',
                'type' => 'raw',
                'value' => '$data->getType()',
            ),
            array(
                'name' => 'position_work_list',
                'type' => 'raw',
                'value' => '$data->getPositionWork()',
            ),
            array(
                'name' => 'position_room_list',
                'type' => 'raw',
                'value' => '$data->getPositionRoom()',
            ),
            array(
                'name' => 'is_per_day',
                'type' => 'raw',
                'value' => '$data->getIsPerDay()',
            ),
            
            array(
                'name'=>'created_by',
                'type'=>'raw',
                'value'=>'$data->getUser()',
            ),
            array(
            'header' => 'Actions',
            'class' => 'CButtonColumn',
            'template' => ControllerActionsName::createIndexButtonRoles($actions),
            'buttons' => array(
                'update' => array(
                        'url' => 'Yii::app()->createUrl("admin/HrFunctions/frontCreate/", array("type_id"=>$data->type_id, "position_work_list"=>$data->position_work_list,"position_room_list"=>$data->position_room_list))',
                    ),
                ),
            ),
	),
)); ?>