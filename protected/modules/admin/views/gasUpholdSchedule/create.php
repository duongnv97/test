<?php
$this->breadcrumbs=array(
	$this->singleTitle=>array('index'),
	'Tạo Mới',
);

$menus = array(		
        array('label'=>'GasUphold Management', 'url'=>array('index')),
);
$this->menu= ControllerActionsName::createMenusRoles($menus, $actions);
?>

<h1>Tạo Mới <?php echo $this->singleTitle;?></h1>

<?php echo $this->renderPartial('_form_create', array('model'=>$model)); ?>