<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'gas-uphold-form123',
	'method'=>'get',
	'action'=> Yii::app()->createAbsoluteUrl("admin/gasUpholdSchedule/create"),
)); ?>
<?php 
    $mUser = new Users();
    if(isset($_GET['Users'])){
        $mUser->attributes=$_GET['Users'];
    }
?>
<div class="clr"></div>
<div class="row">
    <?php echo $form->labelEx($mUser,'province_id'); ?>
    <?php echo $form->dropDownList($mUser,'province_id', GasProvince::getArrAll(),array('style'=>'','class'=>'w-400','empty'=>'Select')); ?>
    <?php echo $form->error($mUser,'province_id'); ?>
</div> 	

<div class="row">
    <?php echo $form->labelEx($mUser,'district_id'); ?>
    <?php echo $form->dropDownList($mUser,'district_id', GasDistrict::getArrAll($mUser->province_id),array('class'=>'w-400', 'empty'=>"Select")); ?>
    <?php echo $form->error($mUser,'district_id'); ?>
</div>

<div class="row">
    <?php echo $form->labelEx($mUser,'sale_id'); ?>
    <?php echo $form->hiddenField($mUser,'sale_id'); ?>
    <?php // echo $form->dropDownList($mUser,'sale_id', Users::getArrUserByRole(ROLE_SALE, array('order'=>'t.code_bussiness', 'prefix_type_sale'=>1)),array('style'=>'width:500px','empty'=>'Select')); ?>
    <?php 
        // widget auto complete search user customer and supplier
        $aData = array(
            'model'=>$mUser,
            'field_customer_id'=>'sale_id',
            'url'=> Yii::app()->createAbsoluteUrl('admin/ajax/search_sale'),
            'name_relation_user'=>'sale',
            'placeholder'=>'Nhập Tên Sale',
            'ClassAdd' => 'w-500',
//                        'fnSelectCustomer'=>'fnAfterSelectSaleOrAgent',
        );
        $this->widget('ext.SpaAutocompleteCustomer.SpaAutocompleteCustomer',
            array('data'=>$aData));                                        
    ?>
    <?php echo $form->error($mUser,'sale_id'); ?>
</div>

<div class="row">
    <?php echo $form->label($mUser,'is_maintain', array('label'=>'Loại KH')); ?>
    <?php echo $form->dropDownList($mUser,'is_maintain', CmsFormatter::$CUSTOMER_BO_MOI,array('class'=>'w-400','empty'=>'Select')); ?>
    <?php echo $form->error($mUser,'is_maintain'); ?>
</div>	

<div class="row display_none">
    <?php // echo $form->labelEx($mUser,'ward_id'); ?>
    <?php // echo $form->dropDownList($mUser,'ward_id', GasWard::getArrAll($mUser->province_id, $mUser->district_id),array('style'=>'width:376px','empty'=>'Select')); ?>
    <?php // echo $form->error($mUser,'ward_id'); ?>
</div>
<div class="row ">
    <label>&nbsp;</label>
    <button type="submit" class="btn_cancel ">Search</button>
</div>

<div class="clr"></div>
<?php $this->endWidget(); ?>
<script>
    $(document).ready(function(){
        fnBindChangeProvince();
    });
    
    /**
    * @Author: ANH DUNG Apr 20, 2016
    */
    function fnBindChangeProvince(){
        $('body').on('change', '#Users_province_id', function(){
            var province_id = $(this).val();        
            var url_ = "<?php echo Yii::app()->createAbsoluteUrl('admin/ajax/get_slt_district');?>";
            $.blockUI({ overlayCSS: { backgroundColor: '<?php echo BLOCK_UI_COLOR;?>' } }); 
            $.ajax({
                url: url_,
                data: {ajax:1,province_id:province_id},
                type: "get",
                dataType:'json',
                success: function(data){
                    $('#Users_district_id').html(data['html_district']);                
                    $.unblockUI();
                }
            });
        });

//        $('body').on('change', '#Users_district_id', function(){
//            var province_id = $('#Users_province_id').val();   
//            var district_id = $(this).val();        
//            var url_ = "<?php echo Yii::app()->createAbsoluteUrl('admin/ajax/get_slt_ward');?>";
//            $.blockUI({ overlayCSS: { backgroundColor: '<?php echo BLOCK_UI_COLOR;?>' } }); 
//            $.ajax({
//                url: url_,
//                data: {ajax:1,district_id:district_id,province_id:province_id},
//                type: "get",
//                dataType:'json',
//                success: function(data){
//                    $('#Users_ward_id').html(data['html_district']);                
//                    $.unblockUI();
//                }
//            });
//        });
    }
</script>