<?php
$this->breadcrumbs=array(	
    $this->singleTitle,
);

$menus=array(
    array('label'=>"Tạo Mới $this->singleTitle", 'url'=>array('create')),
    array('label'=> 'Xuất Excel Danh Sách Hiện Tại',
            'url'=>array('ExportExcel'), 
            'htmlOptions'=>array('class'=>'export_excel','label'=>'Xuất Excel')),
);
$this->menu= ControllerActionsName::createMenusRoles($menus, $actions);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('gas-uphold-grid', {
                url : $(this).attr('action'),
		data: $(this).serialize()
	});
	return false;
});
");

Yii::app()->clientScript->registerScript('ajaxupdate', "
$('#gas-uphold-grid a.ajaxupdate').live('click', function() {
    $.fn.yiiGridView.update('gas-uphold-grid', {
        type: 'POST',
        url: $(this).attr('href'),
        success: function() {
            $.fn.yiiGridView.update('gas-uphold-grid');
        }
    });
    return false;
});
");
?>

<?php $this->renderPartial('application.modules.admin.views.gasUpholdSchedule.index_button',array());?>
<?php echo CHtml::link('Tìm Kiếm Nâng Cao','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<!--<div class="search-form" style="">-->
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'gas-uphold-grid',
	'dataProvider'=>$model->searchSchedule(),
        'afterAjaxUpdate'=>'function(id, data){ fnUpdateColorbox();}',
        'template'=>'{pager}{summary}{items}{pager}{summary}',
//    'itemsCssClass' => 'items custom_here',
//    'htmlOptions'=> array('class'=>'grid-view custom_here'),
        'pager' => array(
            'maxButtonCount'=>  CmsFormatter::$PAGE_MAX_BUTTON,
        ),
	'enableSorting' => false,
	//'filter'=>$model,
	'columns'=>array(
        array(
            'header' => 'S/N',
            'type' => 'raw',
            'value' => '$this->grid->dataProvider->pagination->currentPage * $this->grid->dataProvider->pagination->pageSize + ($row+1)',
            'headerHtmlOptions' => array('width' => '10px','style' => 'text-align:center;'),
            'htmlOptions' => array('style' => 'text-align:center;')
        ),
                array(
                    'name'=>'code_no',
                    'type'=>'raw',
                    'value'=>'$data->getWebCodeNo()',
                ),
//		'type',
                array(
                    'name'=>'manage_id',
                    'type'=>'html',
                    'value'=>'$data->getManageName()',
                ),
                array(
                    'name'=>'customer_id',
                    'type'=>'html',
                    'value'=>'"<strong>".$data->getCustomer("code_bussiness")." - ".$data->getCustomer()."</strong><br>".$data->getCustomer("address")',
                ),
                array(
                    'name'=>'next_uphold',
                    'value'=>'$data->getNextUphold()',
                ),
                array(
                    'header' => 'Loại KH',
                    'value'=>'$data->getCustomerType()',
                    'htmlOptions' => array('style' => 'width:50px;')
                ),
                array(
                    'name'=>'sale_id',
                    'value'=>'$data->getSale()',
                ),
//                array(
//                    'name'=>'type_uphold',
//                    'value'=>'$data->getTextUpholdType()',
//                ),
                array(
                    'name'=>'contact_person',
                    'value'=>'$data->getContactPerson()',
                ),
                array(
                    'name'=>'contact_tel',
                    'value'=>'UsersPhone::formatPhoneLong($data->getContactTel())',
                ),
                array(
                    'name'=>'employee_id',
                    'value'=>'$data->getEmployee()',
                ),
                array(
                    'name'=>'content',
                    'type'=>'html',
                    'value'=>'$data->getContent()',
                ),
//                array(
//                    'name'=>'has_read',
//                    'value'=>'$data->getHasReadText()',
//                    'htmlOptions' => array('style' => 'text-align:center;')
//                ),
            
//                array(
//                    'name'=>'status',
//                    'value'=>'$data->getTextStatus()',
//                    'htmlOptions' => array('style' => 'text-align:center;')
//                ),
                array(
                    'header'=>'Lịch bảo trì',
                    'value'=>'$data->getScheduleText()',
                ),
                array(
                    'name'=>'uid_login',
                    'value'=>'$data->getUidLogin()',
                ),
//                
                array(
                    'header' => 'Ngày cập nhật',
                    'type' => 'html',
                    'value'=>'$data->getLastUpdateSchedule()',
                ),
            
                array(
                    'header' => 'Trạng Thái',
                    'value' => '$data->getStatusUpholdSchedule()',
                ),
                array(
                    'header' => 'Ngày tạo KH',
                    'value' => '$data->getCustomer("created_date")',
                    'type' => 'Datetime',
                    'htmlOptions' => array('style' => 'width:50px;')
                ),
                array(
                    'header' => 'Lấy Hàng Mới Nhất',
                    'value' => '$data->getCustomer("last_purchase")',
                    'type' => 'Date',
                    'htmlOptions' => array('style' => 'width:50px;')
                ),
                array(
                    'name' => 'created_date',
                    'type' => 'Datetime',
                    'htmlOptions' => array('style' => 'width:50px;')
                ),
		
		/*
		'content',
		'contact_person',
		'contact_tel',
		'status',
		'uid_login',
		'created_date',
		*/
		array(
                    'header' => 'Actions',
                    'class'=>'CButtonColumn',
                    'template'=> ControllerActionsName::createIndexButtonRoles($actions),
                    'buttons'=>array(
                        'update'=>array(
                            'visible'=> '$data->CanUpdateUpholdSchedule()',
                        ),
                        'delete'=>array(
                            'visible'=> 'GasCheck::canDeleteData($data)',
                        ),
                    ),
		),
	),
)); ?>

<script>
$(document).ready(function() {
    fnUpdateColorbox();
});

function fnUpdateColorbox(){   
    fixTargetBlank();
}
</script>