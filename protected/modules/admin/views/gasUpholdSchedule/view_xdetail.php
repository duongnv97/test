<?php $this->widget('application.components.widgets.XDetailView', array(
    'data' => $model,
    'attributes' => array(
        'group11'=>array(
            'ItemColumns' => 3,
            'attributes' => array(
               'code_no',
		array(
                    'name'=>'customer_id',
                    'value'=>$model->getCustomer(),
                ),
                array(
                    'name'=>'next_uphold',
                    'value'=> $model->getNextUphold(),
                ),
//                array(
//                    'name'=>'level_type',
//                    'value'=>$model->getTextLevel(),
//                    'htmlOptions' => array('style' => 'text-align:center;')
//                ),
                array(
                    'name'=>'type_uphold',
                    'value'=>$model->getTextUpholdType(),
                ),
                array(
                    'name'=>'contact_person',
                    'value'=>$model->getContactPerson(),
                ),
                array(
                    'name'=>'contact_tel',
                    'value'=>$model->getContactTel(),
                ),
                array(
                    'name'=>'employee_id',
                    'value'=>$model->getEmployee(),
                ),
                array(
                    'name'=>'content',
                    'type'=>'html',
                    'value'=>$model->getContent(),
                ),
                array(
                    'label'=>'Lịch bảo trì',
                    'type'=>'html',
                    'value'=>$model->getScheduleText(),
                ),
                array(
                    'name'=>'has_read',
                    'value'=>$model->getHasReadText(),
                    'htmlOptions' => array('style' => 'text-align:center;')
                ),
                array(
                    'name'=>'status',
                    'value'=>$model->getTextStatus(),
                    'htmlOptions' => array('style' => 'text-align:center;')
                ),
                array(
                    'name'=>'uid_login',
                    'value'=>$model->getUidLogin(),
                ),
            
                array(
                    'name' => 'created_date',
                    'type' => 'Datetime',
                    'htmlOptions' => array('style' => 'width:90px;')
                ), 
            ),
        ),
    ),
)); ?>


<style>
    .detail-view .detail-view th, .detail-view .detail-view td { font-size: 1.1em;}
</style>
<script>
    $(document).ready(function(){
//        $('.xdetail_c2').each(function(){
//            $(this).find('td:first').addClass("w-300");
//            $(this).find('td').eq(1).addClass("w-100");
//            $(this).find('th').addClass("w-120");
//        });
    });
</script>