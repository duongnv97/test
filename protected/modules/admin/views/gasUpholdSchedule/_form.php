<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'gas-uphold-form',
	'enableAjaxValidation'=>false,
        'htmlOptions' => array('enctype' => 'multipart/form-data'),
)); ?>

    <p class="note">Dòng có dấu <span class="required">*</span> là bắt buộc.</p>
    <?php echo MyFormat::BindNotifyMsg(); ?>
    <div class="row buttons" style="padding-left: 141px;">
        <?php $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType'=>'submit',
            'label'=>$model->isNewRecord ? 'Create' :'Save',
            'type'=>'null', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
            'size'=>'small', // null, 'large', 'small' or 'mini'
            //'htmlOptions' => array('style' => 'margin-bottom: 10px; float: right;'),
        )); ?>	
    </div>
    <div class="row">
        <?php echo $form->labelEx($model,'manage_id'); ?>
        <?php echo $form->dropDownList($model,'manage_id', $model->getArrayManage(),array('style'=>'','class'=>'w-400','empty'=>'Select')); ?>
        <?php echo $form->error($model,'manage_id'); ?>
    </div> 
    
    <div class="row">
        <?php echo $form->labelEx($model,'employee_id'); ?>
        <?php echo $form->hiddenField($model,'employee_id', array('class'=>'')); ?>
        <?php
            // 1. limit search kh của sale
//            $url = Yii::app()->createAbsoluteUrl('admin/ajax/search_by_role', array('role'=>ROLE_E_MAINTAIN.','.ROLE_EMPLOYEE_MAINTAIN.','.ROLE_MONITORING_MARKET_DEVELOPMENT));
            $url = Yii::app()->createAbsoluteUrl('admin/ajax/search_by_role', array('role'=> implode(',', GasUphold::$ROLE_EMPLOYEE_SEARCH_AUTOCOMPLETE)));
            // widget auto complete search user customer and supplier
            $aData = array(
                'model'=>$model,
                'field_customer_id'=>'employee_id',
                'url'=> $url,
                'name_relation_user'=>'rEmployee',
                'field_autocomplete_name'=>'autocomplete_name_second',
                'ClassAdd' => 'w-400',
//                'ShowTableInfo' => 0,
            );
            $this->widget('ext.SpaAutocompleteCustomer.SpaAutocompleteCustomer',
                array('data'=>$aData));                                        
        ?>

        <?php echo $form->error($model,'employee_id'); ?>
    </div>
    
    <div class="row">
    <?php echo $form->labelEx($model,'customer_id'); ?>
    <?php echo $form->hiddenField($model,'customer_id', array('class'=>'')); ?>
    <?php
            // 1. limit search kh của sale
            $url = Yii::app()->createAbsoluteUrl('admin/ajax/search_user_by_code');
            // widget auto complete search user customer and supplier
            $aData = array(
                'model'=>$model,
                'field_customer_id'=>'customer_id',
                'url'=> $url,
                'name_relation_user'=>'rCustomer',
                'ClassAdd' => 'w-400',
                'fnSelectCustomerV2' => "fnSelectCustomerScheduleUphold",
            );
            $this->widget('ext.SpaAutocompleteCustomer.SpaAutocompleteCustomer',
                array('data'=>$aData));                                        
            ?>
        <?php echo $form->error($model,'customer_id'); ?>
    </div>
    
    <div class="BoxOutputCustomer">
    <?php include "_form_statistic_output.php";?>
    </div>
    
    <div class="wrap_schedule_type">
        <div class="row">
            <?php echo $form->labelEx($model,'schedule_type'); ?>
            <?php echo $form->dropDownList($model,'schedule_type', $model->getArrayScheduleType(),array('class'=>'w-400 schedule_type','empty'=>'Select')); ?>
            <?php echo $form->error($model,'schedule_type'); ?>
        </div>
        
        <div class="row schedule_type_1 schedule_type_same_hide fix_custom_label_required">
            <?php echo $form->labelEx($model,'day_checkbox'); ?>
            <div class="fix-label">
                <?php
                   $this->widget('ext.multiselect.JMultiSelect',array(
                         'model'=>$model,
                         'attribute'=>'day_checkbox',
                         'data'=> ActiveRecord::getDay(),
                         // additional javascript options for the MultiSelect plugin
                        'options'=>array('selectedList' => 30,),
                         // additional style
                         'htmlOptions'=>array('style' => 'width: 400px;'),
                   ));    
               ?>
            </div>
            <?php echo $form->error($model,'day_checkbox'); ?>
        </div>

        <div class="row  schedule_type_2 schedule_type_same_hide fix_custom_label_required">
            <?php echo $form->labelEx($model,'schedule_in_month'); ?>
            <?php echo $form->textField($model,'schedule_in_month',array('class'=>'w-400 schedule_in_month','maxlength'=>350)); ?>
            <?php echo $form->error($model,'schedule_in_month'); ?>
        </div>
    </div>
    
    <div class="row">
        <?php echo $form->labelEx($model,'next_uphold'); ?>
        <?php 
            $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                'model'=>$model,        
                'attribute'=>'next_uphold',
//                'language'=>'en-GB',
                'options'=>array(
                    'showAnim'=>'fold',
                    'dateFormat'=> ActiveRecord::getDateFormatJquery(),
                    'minDate'=> '0',
//                    'maxDate'=> '0',
                    'changeMonth' => true,
                    'changeYear' => true,
                    'showOn' => 'button',
                    'buttonImage'=> Yii::app()->theme->baseUrl.'/admin/images/icon_calendar_r.gif',
                    'buttonImageOnly'=> true,                                
                ),        
                'htmlOptions'=>array(
                    'class'=>'w-16',
                    'style'=>'height:20px;',
                    'readonly'=>'readonly',
                ),
            ));
        ?>     		
        <?php echo $form->error($model,'next_uphold'); ?>
    </div>
    
    <div class="row">
        <?php echo $form->labelEx($model,'status_uphold_schedule'); ?>
        <?php echo $form->dropDownList($model,'status_uphold_schedule', $model->getArrayStatusSchedule(),array('style'=>'','class'=>'w-400')); ?>
        <?php echo $form->error($model,'status_uphold_schedule'); ?>
    </div> 

    <div class="row">
        <?php echo $form->labelEx($model,'content'); ?>
        <?php echo $form->textArea($model,'content',array('class'=>'w-400','rows'=>6, 'cols'=>50)); ?>
        <?php echo $form->error($model,'content'); ?>
    </div>
    
    <div class="row">
        <?php echo $form->labelEx($model,'contact_person'); ?>
        <?php echo $form->textField($model,'contact_person',array('class'=>'w-400','maxlength'=>350)); ?>
        <?php echo $form->error($model,'contact_person'); ?>
    </div>
    <div class="row display_none">
        <?php echo $form->labelEx($model,'contact_tel'); ?>
        <?php echo $form->textField($model,'contact_tel',array('class'=>'w-400','maxlength'=>100)); ?>
        <?php echo $form->error($model,'contact_tel'); ?>
    </div>
    
    <div class="row">
        <?php // echo $form->labelEx($model,'status'); ?>
        <?php // echo $form->dropDownList($model,'status', $model->getArrayStatus(),array('class'=>'w-400','empty'=>'Select')); ?>
        <?php // echo $form->error($model,'status'); ?>
    </div>

    <div class="row buttons" style="padding-left: 141px;">
        <?php $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType'=>'submit',
            'label'=>$model->isNewRecord ? 'Create' :'Save',
            'type'=>'null', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
            'size'=>'small', // null, 'large', 'small' or 'mini'
            //'htmlOptions' => array('style' => 'margin-bottom: 10px; float: right;'),
        )); ?>	
    </div>

<?php $this->endWidget(); ?>

</div><!-- form -->
<script>
    $(document).ready(function(){
        $('.form').find('button:submit').click(function(){
            $.blockUI({ overlayCSS: { backgroundColor: '#fff' } });
        });
        fnBindScheduleType();
    });
    
    /**
    * @Author: ANH DUNG Jun 23, 2015
    * @Todo: function này dc gọi từ ext của autocomplete
    * @Param: $model
    */
    function fnSelectCustomerScheduleUphold(ui, idField, idFieldCustomer){
//        if(typeof ui.item.name_role === 'undefined'){
//            return;
//        }
        // Sau khi select customer thì gửi ajax lên lấy table sản lượng KH sau đó ajax tiếp lấy sản lượng bình quân
        var customer_id = ui.item.id;
        var url_ = "<?php echo Yii::app()->createAbsoluteUrl('admin/gasUpholdSchedule/create');?>?customer_id="+customer_id;
        $.ajax({
            url: url_,
            success: function(data){
                $('.BoxOutputCustomer').html($(data).find('.BoxOutputCustomer').html());
//                console.log($(data).find('.BoxOutputCustomer').html());
                fnGetAverage();
            }
        });
    }
    
    function fnGetAverage(){
        var url_ = "<?php echo Yii::app()->createAbsoluteUrl('admin/ajax/getAverageOutput');?>";
        $.blockUI({ overlayCSS: { backgroundColor: '<?php echo BLOCK_UI_COLOR;?>' } }); 
        $.ajax({
            url: url_,
            dataType: "json",
            success: function(data){
                $('.schedule_in_month').val(data['schedule_in_month']);
                $.unblockUI();
            }
        });
    }

    $(window).load(function(){
        $('.schedule_type').trigger('change');                
        fnBindFixLabelRequired();
    });
    
</script>