<?php 
    $saleName = $mUser->getSaleName();
?>
<tr class="">
    <td class="item_c"><?php echo ($key+1); ?></td>
    <td class=""><?php echo $mUser->first_name." - $mUser->address"; ?></td>
    <td class="item_c"><?php echo $aDistrict[$mUser->district_id]; ?> <br> - KH <?php echo $mUser->getTypeCustomerText(); ?></td>
    <td class="item_c w-200">
        <input type="hidden" name="customer_id[]" value="<?php echo $mUser->id; ?>">
        <?php // echo CHtml::dropDownList('employee_id[]', "",
//              $aEmployee, array('class'=>'w-200 ','empty'=>"Select"));?>
        
        <div class="box-cart ">
            <div class="item-ipt-cart WrapUserSearchFix form" style="padding-right: 0; padding-left: 20px;">
                <div class="" style="width: 100%">
                    <input class="call-iptclass UserFullNameFix" placeholder="NV Bảo Trì" type="text" value="<?php // echo $namePhuXe1; ?>" <?php // echo !empty($namePhuXe1) ? 'readonly' : '';?>>
                </div>
                <span class="remove_row_item" onclick="fnRemoveNameAgent(this)" style="margin-left: 0;"></span>
                <input class="UserId" name="employee_id[]" type="hidden" value="<?php // echo $item->phu_xe_1;?>">
            </div>
        </div>

        Sale: <?php echo $saleName; ?>
    </td>
    <td class="item_c wrap_schedule_type">
        <?php echo CHtml::dropDownList('schedule_type[]', "",
              $model->getArrayScheduleType(), array('class'=>'w-150 schedule_type'));?>
        
        <div class="schedule_type_2 schedule_type_same_hide fix_custom_label_required display_none">
            <?php echo $form->textField($model,'schedule_in_month',array("name"=>"schedule_in_month[]", 'class'=>'w-150 schedule_in_month number_only number_only_v1','maxlength'=>1, 'placeholder'=>"nhập số lần bảo trì 1 tháng")); ?>
        </div>
        
        <div class="schedule_type_1 schedule_type_same_hide fix_custom_label_required">
            <div class="fix-label">
                <?php
                   $this->widget('ext.multiselect.JMultiSelect',array(
                         'model'=>$model,
                         'attribute'=>"day_checkbox[$key]",
                         'data'=> ActiveRecord::getDay(),
                         // additional javascript options for the MultiSelect plugin
                        'options'=>array('selectedList' => 30,),
                         // additional style
                         'htmlOptions'=>array("name"=>"day_checkbox[$key]", 'class' => 'w-100', 'id'=>"gas_multi_box_$key"),
                   ));    
               ?>
            </div>
        </div>
        
    </td>
    <td class="item_c w-120">
        <?php echo $form->textArea($model,'content',array("name"=>"content[]",'class'=>'w-100 display_none','rows'=>3, 'id'=>"gas_content$key")); ?>
        <?php include '_form_date.php'; ?>
    </td>
    <td class="item_c">
        <?php echo $form->textField($model,'contact_person',array("name"=>"contact_person[]",'class'=>'w-100','value'=>$mUser->getUserRefField("contact_person_name"))); ?>
    </td>
    <td class="item_c">
        <?php echo $form->textField($model,'contact_tel',array("name"=>"contact_tel[]",'class'=>'w-100','value'=>$mUser->getPhone())); ?>
    </td>
    <td class="item_c last">
        <!--<span next="<?php echo Yii::app()->createAbsoluteUrl('admin/ajax/moveToKhongLayHang', array('customer_id'=>$mUser->id)) ;?>" remove="" class="remove_icon_only"></span>-->
    </td>
    
</tr>