<?php
$tmp = explode(",", $iModel->google_map);
$aDataChart = array();
$contentString = "<div id='content'>";
$contentString .= "<h1 id='firstHeading' class='firstHeading'>{$mCustomer->getFullName()}</h1>";
    $contentString .= "<div id='bodyContent'>";
        $contentString .= "<p><b>Địa chỉ:</b> {$mCustomer->getAddress()}</p>";
        $contentString .= "<p><b>Phone:</b> ".UsersPhone::formatPhoneView($mCustomer->getPhone())."</p>";
        $contentString .= "<p><b>Ngày bảo trì:</b> ".MyFormat::dateConverYmdToDmy($iModel->created_date, 'd/m/Y H:i')."</p>";
    $contentString .= "</div>";
$contentString .= "</div>";

$aDataChart[] = array($contentString, $tmp[0], $tmp[1], 1);
$js_array = json_encode($aDataChart);
?>
<div id="map<?php echo $iModel->id;?>" style="width: 100%; height: 300px;"></div>

<script type="text/javascript">
    var locations = <?php echo $js_array;?>;
    var map = new google.maps.Map(document.getElementById('map<?php echo $iModel->id;?>'), {
      zoom: 16,
      center: new google.maps.LatLng(<?php echo isset($tmp[0]) ? $tmp[0] : 10.818463;?>, <?php echo isset($tmp[1]) ? $tmp[1] : 106.658825;?>),
      mapTypeId: google.maps.MapTypeId.ROADMAP
    });

    var infowindow = new google.maps.InfoWindow();

    var marker, i;
//    var image = 'http://daukhi.huongminhgroup.com/apple-touch-icon.png';

    for (i = 0; i < locations.length; i++) {  
      marker = new google.maps.Marker({
        position: new google.maps.LatLng(locations[i][1], locations[i][2]),
        map: map
//        icon: image
      });

      google.maps.event.addListener(marker, 'click', (function(marker, i) {
        return function() {
          infowindow.setContent(locations[i][0]);
          infowindow.open(map, marker);
        }
      })(marker, i));
    }
</script>