<?php 
$aComment = $model->rUpholdReply;
$cmsFormater = new CmsFormatter();
$mCustomer  = $model->rCustomer;
?>
<script src="http://maps.google.com/maps/api/js?key=AIzaSyCfsbfnHgTh4ODoXLXFfEFhHskDhnRVtjQ" type="text/javascript"></script>
<div class="">
<?php if(count($aComment)):?>
    <h1>Lịch sử báo cáo</h1>
    <table class="detail-view items">
        <tbody>
        <?php foreach($aComment as $iModel): ?>
            <tr>
                <td class="w-250">
                    <?php echo '<b>'.$iModel->getUidLogin()."</b> - {$iModel->id}<br>".$iModel->getReplyContent().'<b>Địa chỉ KH: </b>'.$mCustomer->getAddress(); 
                    ?>
                </td>
                <td class="w-150">
                    <?php echo $cmsFormater->formatBreakTaskDailyFile($iModel); ?>
                </td>
                <td>
                    <?php if(trim($iModel->google_map) != ''): ?>
                        <?php include "view_reply_map.php"; ?>
                    <?php endif; ?>
                </td>
            </tr>

        <?php endforeach; ?>
        </tbody>
    </table>
<div class="clr"></div>
<?php endif;?>
</div>

<script type="text/javascript">
    fnAddClassOddEven('items');
</script>