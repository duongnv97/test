<?php 
    $aCustomer = array();
    if(isset($_GET['Users']['province_id'])){// Apr 21, 2016 add
        $mCronUpdate = new CronUpdate();
        $aCustomer = $mCronUpdate->getCustomerUpholdSchedule();
    }
    $aDistrict = GasDistrict::getArrAllFix();
    $needMore = array('role_id'=> GasUphold::$ROLE_EMPLOYEE_BAOTRI, "order"=>"t.name_agent ASC");
    $aEmployee = Users::getListOptionEmployeeMaintain($needMore);
?>

<div class="form">
<?php include "_form_create_search_customer.php"; ?>
    
<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'gas-uphold-form',
	'enableAjaxValidation'=>false,
        'htmlOptions' => array('enctype' => 'multipart/form-data'),
)); ?>

    <p class="note">Có dấu <span class="required">*</span> là bắt buộc.</p>
    <?php echo MyFormat::BindNotifyMsg(); ?>
    
    
    <div class="row display_none">
        <?php echo $form->labelEx($model,'manage_id'); ?>
        <?php echo $form->dropDownList($model,'manage_id', $model->getArrayManage(),array('style'=>'width:376px','class'=>'','empty'=>'Select')); ?>
        <?php echo $form->error($model,'manage_id'); ?>
    </div> 
    <table class="materials_table materials_table_root materials_table_th hm_table" style="">
        <thead>
            <tr>
                <th class="item_c w-10">#</th>
                <th class="w-150 item_c">Khách Hàng</th>
                <th class="w-50 item_c">Quận/Huyện</th>
                <th class="w-30 item_c">NV Bảo Trì <span class="required item_b">*</span></th>
                <th class="w-30 item_c">Loại Bảo Trì <span class="required">*</span></th>
                <th class="w-30 item_c">Lịch bảo trì</th>
                <th class="w-30 item_c">Người Liên Hệ</th>
                <th class="w-30 item_c">Điện Thoại</th>
                <!--<th class="w-30 item_c last">Xóa</th>-->
                <th class="w-30 item_c last"></th>
            </tr>
        </thead>
        <tbody>
        <?php foreach ($aCustomer as $key=>$mUser): ?>
            <?php include "_form_tr_customer.php"; ?>
        <?php endforeach; ?>
        </tbody>
    </table>

    <?php echo $form->hiddenField($model,'employee_id', array('class'=>'')); ?>
    <div class="row buttons" style="padding-left: 141px;">
        <?php $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType'=>'submit',
            'label'=>$model->isNewRecord ? 'Create' :'Save',
            'type'=>'null', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
            'size'=>'small', // null, 'large', 'small' or 'mini'
            //'htmlOptions' => array('style' => 'margin-bottom: 10px; float: right;'),
        )); ?>	
    </div>

<?php $this->endWidget(); ?>

</div><!-- form -->

<link href="<?php echo Yii::app()->theme->baseUrl; ?>/css/style_sell.css" rel="stylesheet" type="text/css"/>
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/jquery.floatThead.min.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/underscore-min.js"></script>
<script>
    $(document).ready(function(){
        $('#gas-uphold-form').find('button:submit').click(function(){
            if($('#GasUphold_manage_id').val()==''){
                alert('Chưa chọn quản lý bảo trì');
                return false;
            }
            $.blockUI({ overlayCSS: { backgroundColor: '#fff' } }); 
        });
        fnBindScheduleType();
        fnBindRemoveIcon();
        $('.materials_table').floatThead();
    });
    
    function fnBeforeRemoveIcon(this_){
        var url_ = this_.attr('next');
        $.ajax({
            url: url_
        });
    }
    
</script>


<?php Yii::app()->clientScript->registerCoreScript('jquery.ui'); ?>
<script>
    // Aug1017 bind for new autocomplete search view only name
    $(document).ready(function(){
        fnBindAllAutocomplete();
    });
    
    function fnBindAllAutocomplete(){
        // material
        $('.UserFullNameFix').each(function(){
            bindUserAutocomplete($(this));
        });
    }
    
    // to do bind autocompelte for input material
    // @param objInput : is obj input ex  $('.customer_autocomplete')    
    function bindUserAutocomplete(objInput){
        var parent_div = objInput.closest('.WrapUserSearchFix');
        
        objInput.autocomplete({
            source: '<?php echo Yii::app()->createAbsoluteUrl('admin/ajax/search_by_role', array('role'=> implode(',', GasUphold::$ROLE_EMPLOYEE_SEARCH_AUTOCOMPLETE)));?>',
            minLength: '<?php echo MIN_LENGTH_AUTOCOMPLETE;?>',
            close: function( event, ui ) { 
                //$( "#GasStoreCard_materials_name" ).val(''); 
            },
            search: function( event, ui ) { 
                objInput.addClass('grid-view-loading-gas');
            },
            response: function( event, ui ) { 
                objInput.removeClass('grid-view-loading-gas');
                var json = $.map(ui, function (value, key) { return value; });
                if(json.length<1){
                    var error = '<div style=\'padding-left: 0;\' class=\'errorMessage clr autocomplete_name_text\'>Không tìm thấy dữ liệu.</div>';
                    if(parent_div.find('.autocomplete_name_text').size()<1){
                        parent_div.find('.remove_row_item').after(error);
                    }
                    else
                        parent_div.find('.autocomplete_name_text').show();
                }
            },
            select: function( event, ui ) {
                setValueSelectUser(parent_div, objInput, ui.item.id, true);
            }
        });
    }
    
    function setValueSelectUser(parent_div, objInput, user_id, readonly){
        parent_div.find('.UserId').val(user_id);
        parent_div.find('.UserFullNameFix').attr('readonly', readonly);
    }
    
    function fnRemoveNameAgent(this_){
        objInput = $(this_);
        var parent_div = objInput.closest('.WrapUserSearchFix');
        setValueSelectUser(parent_div, objInput, 0, false);
        parent_div.find('.UserFullNameFix').val('');
    }
</script>