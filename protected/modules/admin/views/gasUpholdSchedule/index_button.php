<div class="form">
    <?php
        $LinkNormal = Yii::app()->createAbsoluteUrl('admin/gasUpholdSchedule/index');
        $LinkDetail = Yii::app()->createAbsoluteUrl('admin/gasUpholdSchedule/index', array( 'detail'=> 1));
    ?>
    <h1><?php echo $this->adminPageTitle;?>
        <a class='btn_cancel f_size_14 <?php echo (!isset($_GET['detail'])) ? "active":"";?>' href="<?php echo $LinkNormal;?>">Tổng quát</a>
        <a class='btn_cancel f_size_14 <?php echo isset($_GET['detail']) && $_GET['detail']==1 ? "active":"";?>' href="<?php echo $LinkDetail;?>">Chi tiết</a>
    </h1> 
</div>