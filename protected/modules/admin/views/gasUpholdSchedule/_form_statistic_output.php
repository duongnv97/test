<?php if($model->customer_id):?>

<div class="form">
    <?php
    $month_statistic = 3;
    $aData = array(
        'customer_id'=> $model->customer_id,
        'month_statistic'=> $month_statistic,
    );
    ?>    
    <div class="row">
        <label>&nbsp;</label>
        <span class="item_b">Sản lượng khách hàng trong <?php echo $month_statistic; ?> tháng gần đây</span>
    </div>
    <div class="row">
        <label>&nbsp;</label>
        <?php
        $this->widget('ext.GasStatistic.GasStatistic',
            array('data'=>$aData)); 
        ?>
    </div>
</div>
<?php endif;?>
