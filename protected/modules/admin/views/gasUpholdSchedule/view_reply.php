<hr>
<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'gas-uphold-form',
	'enableAjaxValidation'=>false,
        'htmlOptions' => array('enctype' => 'multipart/form-data'),
)); ?>

    <p class="note">Dòng có dấu <span class="required">*</span> là bắt buộc.</p>
    <?php echo MyFormat::BindNotifyMsg(); ?>
    <div class="row">
        <?php echo $form->labelEx($model->mReply,'status'); ?>
        <?php echo $form->dropDownList($model->mReply,'status', $model->getArrayStatus(),array('class'=>'w-400','empty'=>'Select')); ?>
        <?php echo $form->error($model->mReply,'status'); ?>
    </div>
    
    <div class="row">
        <?php echo $form->labelEx($model->mReply,'date_time_handle'); ?>
        <?php Yii::import('application.extensions.CJuiDateTimePicker.CJuiDateTimePicker');
        $this->widget('CJuiDateTimePicker',array(
            'model'=>$model->mReply, //Model object
            'attribute'=>'date_time_handle', //attribute name
            'mode'=>'datetime', //use "time","date" or "datetime" (default)
            'language'=>'en-GB',
            'options'=>array(
                'showAnim'=>'fold',
                'showButtonPanel'=>true,
                'autoSize'=>true,
                'dateFormat'=>'dd/mm/yy',
                'timeFormat'=>'hh:mm:ss',
                'width'=>'120',
                'separator'=>' ',
                'showOn' => 'button',
                'buttonImage'=> Yii::app()->theme->baseUrl.'/admin/images/icon_calendar_r.gif',
                'buttonImageOnly'=> true,
                'changeMonth' => true,
                'changeYear' => true,
                //                'regional' => 'en-GB'
            ),
            'htmlOptions' => array(
                'style' => 'width:180px;',
                'readonly'=>'readonly',
            ),
        ));
        ?>
        <?php echo $form->error($model->mReply,'date_time_handle'); ?>
    </div>
    
    <div class="row">
        <?php echo $form->labelEx($model->mReply,'note'); ?>
        <?php echo $form->textArea($model->mReply,'note',array('class'=>'w-400','rows'=>6, 'cols'=>50)); ?>
        <?php echo $form->error($model->mReply,'note'); ?>
    </div>
    
    <?php include 'view_reply_upload_file.php';?>
    

    <div class="row buttons" style="padding-left: 141px;">
        <?php $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType'=>'submit',
            'label'=> 'Save',
            'type'=>'null', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
            'size'=>'small', // null, 'large', 'small' or 'mini'
            //'htmlOptions' => array('style' => 'margin-bottom: 10px; float: right;'),
        )); ?>	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->

