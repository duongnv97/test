<div class="row">
     <?php 
        $this->widget('zii.widgets.jui.CJuiDatePicker', array(
            'model'=>$model,        
            'attribute'=>'next_uphold[]',
            'options'=>array(
                'showAnim'=>'fold',
                'dateFormat'=> ActiveRecord::getDateFormatJquery(),
                'minDate'=> '0',
                'changeMonth' => true,
                'changeYear' => true,
                'showOn' => 'button',
                'buttonImage'=> Yii::app()->theme->baseUrl.'/admin/images/icon_calendar_r.gif',
                'buttonImageOnly'=> true,                                
            ),        
            'htmlOptions'=>array(
                'class'=>'w-80',
                'placeholder'=>'Lịch bảo trì',
                'readonly'=>'readonly',
                'id'=>'RenewalDate'.$mUser->id,
            ),
        ));
        ?>  
    <?php echo $form->error($model,'next_uphold'); ?>
</div>