<?php
$this->breadcrumbs=array(
	$this->singleTitle=>array('index'),
	$model->code_no,
);

$menus = array(
	array('label'=>$this->singleTitle, 'url'=>array('index')),
	array('label'=>"Tạo Mới ", 'url'=>array('create')),
	array('label'=>"Cập Nhật $this->singleTitle", 'url'=>array('update', 'id'=>$model->id)),
//	array('label'=>"Xóa $this->singleTitle", 'url'=>array('delete'), 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Bạn chắc chắn muốn xóa ?')),
);
$this->menu= ControllerActionsName::createMenusRoles($menus, $actions);
?>

<h1>Xem <?php echo $this->singleTitle;?>:  <?php echo $model->code_no; ?></h1>

<?php include 'view_xdetail.php'; ?>
<?php include 'view_reply_view.php'; ?>
<?php // include 'view_reply.php'; ?>
<script>
    $(document).ready(function(){
        $('.form').find('button:submit').click(function(){
            $.blockUI({ overlayCSS: { backgroundColor: '#fff' } }); 
        });
        $('.xdetail_c2').each(function(){
            $(this).find('td:first').addClass("w-500");
//            $(this).find('td').eq(1).addClass("w-100");
            $(this).find('th').addClass("w-120");
            $(this).find('th').eq(1).addClass("w-5");
        });
        $(".gallery").colorbox({iframe:true,innerHeight:'1100', innerWidth: '1000',close: "<span title='close'>close</span>"});
    });
</script>