<?php
$this->breadcrumbs=array(
	$this->pageTitle,
);

$menus=array(
            array('label'=>"Create $this->pageTitle", 'url'=>array('create')),
);
$this->menu= ControllerActionsName::createMenusRoles($menus, $actions);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('users-info-grid', {
                url : $(this).attr('action'),
		data: $(this).serialize()
	});
	return false;
});
");

Yii::app()->clientScript->registerScript('ajaxupdate', "
$('#users-info-grid a.ajaxupdate').live('click', function() {
    $.fn.yiiGridView.update('users-info-grid', {
        type: 'POST',
        url: $(this).attr('href'),
        success: function() {
            $.fn.yiiGridView.update('users-info-grid');
        }
    });
    return false;
});
");
?>

<h1>Danh Sách <?php echo $this->pageTitle; ?></h1>


<?php echo CHtml::link('Tìm Kiếm Nâng Cao','#',array('class'=>'search-button')); ?>
<div class="search-form">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'users-info-grid',
	'dataProvider'=>$model->search(),
        'afterAjaxUpdate'=>'function(id, data){ fnUpdateColorbox();}',
        'template'=>'{pager}{summary}{items}{pager}{summary}',
//    'itemsCssClass' => 'items custom_here',
//    'htmlOptions'=> array('class'=>'grid-view custom_here'),
        'pager' => array(
            'maxButtonCount'=>  CmsFormatter::$PAGE_MAX_BUTTON,
        ),
	'enableSorting' => false,
	//'filter'=>$model,
	'columns'=>array(
        array(
            'header' => 'S/N',
            'type' => 'raw',
            'value' => '$this->grid->dataProvider->pagination->currentPage * $this->grid->dataProvider->pagination->pageSize + ($row+1)',
            'headerHtmlOptions' => array('width' => '10px','style' => 'text-align:center;'),
            'htmlOptions' => array('style' => 'text-align:center;')
        ),
            array(
                'type' => 'raw',
                'name' => 'first_name',
                'value' => '$data->getFirstName()'
            ),
            array(
                'type' => 'raw',
                'name' => 'phone',
                'value' => '$data->getPhone()'
            ),
            array(
                'type' => 'raw',
                'name' => 'address',
                'value' => '$data->getAddress()'
            ),
            array(
                'type' => 'raw',
                'name' => 'agent_id',
                'value' => '$data->getAgentName()'
            ),
            array(
                'type' => 'raw',
                'name' => 'is_maintain',
                'value' => '$data->getCustomerType()'
            ),
            array(
                'type' => 'raw',
                'name' => 'days_per_one',
                'value' => '$data->getDayPerOne()'
            ),
            array(
                'type' => 'raw',
                'name' => 'reward_point',
                'value' => '$data->getPoint()'
            ),
            array(
                'type' => 'raw',
                'name' => 'reward_point_used',
                'value' => '$data->getPointUsed()'
            ),
            array(
                'type' => 'raw',
                'name' => 'first_purchase',
                'value' => '$data->getFirstPurchase()'
            ),
            array(
                'type' => 'raw',
                'name' => 'last_purchase',
                'value' => '$data->getLastPurchase()'
            ),
            array(
                'type' => 'raw',
                'name' => 'count_sell',
                'value' => '$data->getCountSell()'
            ),
            array(
                'type' => 'raw',
                'name' => 'sale_id',
                'value' => '$data->getSale()'
            ),
            array(
                'type' => 'raw',
                'name' => 'created_date',
                'value' => '$data->getCreatedDate()'
            ),
            
	),
)); ?>

<script>
$(document).ready(function() {
    fnUpdateColorbox();
});

function fnUpdateColorbox(){   
    fixTargetBlank();
}
</script>