<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=> GasCheck::getCurl(),
	'method'=>'get',
)); ?>

        <div class="row more_col">
        <div class="col1">
                <?php echo Yii::t('translation', $form->label($model,'date_from', array('label' => "Từ ngày" ))); ?>
                <?php 
                    $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                        'model'=>$model,        
                        'attribute'=>'date_from',
                        'options'=>array(
                            'showAnim'=>'fold',
                            'dateFormat'=> MyFormat::$dateFormatSearch,
//                            'minDate'=> '0',
                            'maxDate'=> '0',
                            'changeMonth' => true,
                            'changeYear' => true,
                            'showOn' => 'button',
                            'buttonImage'=> Yii::app()->theme->baseUrl.'/admin/images/icon_calendar_r.gif',
                            'buttonImageOnly'=> true,                                
                        ),        
                        'htmlOptions'=>array(
                            'class'=>'w-16',
                            'size'=>'16',
                            'style'=>'float:left;',
                        ),
                    ));
                ?>     		
        </div>
        <div class="col2">
                <?php echo Yii::t('translation', $form->label($model,'date_to', array('label' => "Đến ngày" ))); ?>
                <?php 
                    $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                        'model'=>$model,        
                        'attribute'=>'date_to',
                        'options'=>array(
                            'showAnim'=>'fold',
                            'dateFormat'=> MyFormat::$dateFormatSearch,
//                            'minDate'=> '0',
                            'maxDate'=> '0',
                            'changeMonth' => true,
                            'changeYear' => true,
                            'showOn' => 'button',
                            'buttonImage'=> Yii::app()->theme->baseUrl.'/admin/images/icon_calendar_r.gif',
                            'buttonImageOnly'=> true,                                
                        ),        
                        'htmlOptions'=>array(
                            'class'=>'w-16',
                            'size'=>'16',
                            'style'=>'float:left;',
                        ),
                    ));
                ?>     		
        </div>
    </div>
        <div class="row">
            <?php echo $form->labelEx($model,'is_maintain'); ?>
            <div class="fix-label">
                <?php
                   $this->widget('ext.multiselect.JMultiSelect',array(
                         'model'=>$model,
                         'attribute'=>'is_maintain',
                         'data'=> CmsFormatter::$CUSTOMER_BO_MOI,
                         // additional javascript options for the MultiSelect plugin
                        'options'=>array('selectedList' => 30,),
                         // additional style
                         'htmlOptions'=>array('class' => 'w-200'),
                   ));    
               ?>
            </div>
        </div>
	<div class="row">
            <?php echo $form->labelEx($model,'user_id'); ?>
            <?php echo $form->hiddenField($model,'user_id'); ?>
            <?php
                $url = Yii::app()->createAbsoluteUrl('admin/ajax/searchHgd');
                $aData = array(
                    'model'=>$model,
                    'field_customer_id'=>'user_id',
                    'url'=> $url,
                    'name_relation_user'=>'rUser',
                    'ClassAdd' => 'w-600',
                    'field_autocomplete_name' => 'autocomplete_user',
                );
                $this->widget('ext.SpaAutocompleteCustomer.SpaAutocompleteCustomer',
                    array('data'=>$aData));
            ?>
        </div>

	<div class="row">
        <?php echo $form->labelEx($model,'agent_id'); ?>
        <?php echo $form->hiddenField($model,'agent_id', array('class'=>'')); ?>
        <?php
                // 1. limit search kh của sale
                $url = Yii::app()->createAbsoluteUrl('admin/ajax/search_by_role', array('role'=> ROLE_AGENT));
                // widget auto complete search user customer and supplier
                $aData = array(
                    'model'=>$model,
                    'field_customer_id'=>'agent_id',
                    'url'=> $url,
                    'name_relation_user'=>'rAgent',
                    'ClassAdd' => 'w-400',
                    'field_autocomplete_name' => 'autocomplete_name_3',
                    'placeholder'=>'Nhập mã hoặc tên đại lý',
                );
                $this->widget('ext.SpaAutocompleteCustomer.SpaAutocompleteCustomer',
                    array('data'=>$aData));
                ?>
            <?php echo $form->error($model,'agent_id'); ?>
        </div>
        <div class="row more_col">
            <div class="col1">
                <?php  echo $form->labelEx($model,'count_sell', ['label'=>'Số hóa đơn đã mua']); ?>
                <?php $mTelesale = new Telesale();?>
                <?php  echo $form->dropDownList($model,'count_sell', $mTelesale->getArrayCountSell(), ['empty'=>'Select','class'=>'w-200']); ?>
            </div>
            <div class="col2">
                <?php  echo $form->labelEx($model,'count_sell', ['label'=>'Đến']); ?>
                <?php $mTelesale = new Telesale();?>
                <?php  echo $form->dropDownList($model,'count_sell_to', $mTelesale->getArrayCountSell(), ['empty'=>'Select','class'=>'w-200']); ?>
            </div>
        </div>
	<div class="row more_col">
            
        </div>
    
	<div class="row more_col">
            <div class="col1">
                <?php  echo $form->labelEx($model,'point_from', ['label'=>'Điểm từ']); ?>
                <?php  echo $form->dropDownList($model,'point_from', $model->getArrayNumber(0,500,'',10), ['empty'=>'Select','class'=>'w-200']); ?>
            </div>
            <div class="col2">
                <?php  echo $form->labelEx($model,'point_to', ['label'=>'Đến']); ?>
                <?php  echo $form->dropDownList($model,'point_to', $model->getArrayNumber(0,500,'',10), ['empty'=>'Select','class'=>'w-200']); ?>
            </div>
        </div>
    
	<div class="row more_col">
            <div class="col1">
                <?php  echo $form->labelEx($model,'per_from', ['label'=>'Tần xuất từ']); ?>
                <?php  echo $form->dropDownList($model,'per_from', $model->getArrayNumber(0,365,'',5), ['empty'=>'Select','class'=>'w-200']); ?>
            </div>
            <div class="col2">
                <?php  echo $form->labelEx($model,'per_to', ['label'=>'Đến']); ?>
                <?php  echo $form->dropDownList($model,'per_to', $model->getArrayNumber(0,365,'',5), ['empty'=>'Select','class'=>'w-200']); ?>
            </div>
        </div>
	
	<div class="row buttons" style="padding-left: 159px;">
		        <?php $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType'=>'submit',
            'label'=>'Search',
            'type'=>'null', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
            'size'=>'small', // null, 'large', 'small' or 'mini'
            //'htmlOptions' => array('style' => 'margin-bottom: 10px; float: right;'),
        )); ?>	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->