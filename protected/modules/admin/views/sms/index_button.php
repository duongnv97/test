<div class="form">
    <?php
        $LinkNormal = Yii::app()->createAbsoluteUrl('admin/sms/index');
        $LinkDetail = Yii::app()->createAbsoluteUrl('admin/sms/index', array( 'report'=> 1));
        $LinkWait   = Yii::app()->createAbsoluteUrl('admin/sms/index', array( 'SmsWait'=> 1));
    ?>
    <h1><?php echo $this->pageTitle;?>
        <a class='btn_cancel f_size_14 <?php echo (!isset($_GET['report']) && !isset($_GET['SmsWait'])) ? "active":"";?>' href="<?php echo $LinkNormal;?>">Chi tiết</a>
        <a class='btn_cancel f_size_14 <?php echo isset($_GET['report']) && $_GET['report']==1 ? "active":"";?>' href="<?php echo $LinkDetail;?>">Tổng quát</a>
        <a class='btn_cancel f_size_14 <?php echo isset($_GET['SmsWait']) && $_GET['SmsWait']==1 ? "active":"";?>' href="<?php echo $LinkWait;?>">SMS Wait</a>
    </h1> 
</div>