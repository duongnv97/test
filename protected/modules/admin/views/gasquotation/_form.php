<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'gas-quotation-form',
	'enableAjaxValidation'=>false,
)); ?>

	<?php echo Yii::t('translation', '<p class="note">Fields with <span class="required">*</span> are required.</p>'); ?>
	
	<div class="row">
		<?php echo Yii::t('translation', $form->labelEx($model,'customer_id')); ?>
		<?php echo $form->hiddenField($model,'customer_id'); ?>
                <?php 
                        $this->widget('zii.widgets.jui.CJuiAutoComplete', array(
                                'attribute'=>'autocomplete_name',
                                'model'=>$model,
                                'sourceUrl'=>Yii::app()->createAbsoluteUrl('admin/gascustomer/autocompleteCustomer'),
                //                                'name'=>'my_input_name',
                                'options'=>array(
                                        'minLength'=>MIN_LENGTH_AUTOCOMPLETE,
                                        'multiple'=> true,
                                    'select'=>"js:function(event, ui) {
                                            $('#GasQuotation_customer_id').val(ui.item.id);
                                            var remove_div = '<span class=\'remove_row_item\' onclick=\'fnRemoveName(this)\'></span>';
                                            $('#GasQuotation_autocomplete_name').parent('div').find('.remove_row_item').remove();
                                            $('#GasQuotation_autocomplete_name').attr('readonly',true).after(remove_div);
                                            fnBuildTableInfo(ui.item);
                                            $('.autocomplete_customer_info').show();
                                    }",
                                ),
                                'htmlOptions'=>array(
                                    'size'=>45,
                                    'maxlength'=>45,
                                    'style'=>'float:left;',
                                    'placeholder'=>'Nhập tên hoặc mã kế toán, kinh doanh',
                                ),
                        )); 
                        ?>        
                        <script>
                            function fnRemoveName(this_){
                                $(this_).prev().attr("readonly",false); 
                                $("#GasQuotation_autocomplete_name").val("");
                                $("#GasQuotation_customer_id").val("");
                                $('.autocomplete_customer_info').hide();
                            }
                        function fnBuildTableInfo(item){
                            $(".info_name").text(item.value);
                            $(".info_name_agent").text(item.name_agent);
                            $(".info_code_account").text(item.code_account);
                            $(".info_code_bussiness").text(item.code_bussiness);
                            $(".info_address").text(item.address);
                            $(".info_name_sale").text(item.sale);
                        }
                            
                        </script>        
                        <div class="clr"></div>		
		<?php echo $form->error($model,'customer_id'); ?>
                <?php $display='display:inline;';
                    $info_name ='';
                    $info_name_agent ='';
                    $info_address ='';
                    $info_code_account ='';
                    $info_code_bussiness ='';
                    $info_name_sale ='';
                        if(empty($model->customer_id)) $display='display: none;';
                        else{
                            $info_name = $model->customer->first_name;
                            $info_name_agent = $model->customer->name_agent;
                            $info_code_account = $model->customer->code_account;
                            $info_code_bussiness = $model->customer->code_bussiness;
                            $info_address = $model->customer->address;
                            $info_name_sale = $model->customer->sale->first_name;
                        }
                ?>                        
                <div class="autocomplete_customer_info" style="<?php echo $display;?>">
                <table>
                    <tr>
                        <td class="_l">Mã kế toán:</td>
                        <td class="_r info_code_account"><?php echo $info_code_account;?></td>
                    </tr>
                    <tr>
                        <td class="_l">mã kinh doanh:</td>
                        <td class="_r info_code_bussiness"><?php echo $info_code_bussiness;?></td>
                    </tr>
                    <tr>
                        <td class="_l">Nhân viên sale:</td>
                        <td class="_r info_name_sale"><?php echo $info_name_sale;?></td>
                    </tr>
                    <tr>
                        <td class="_l">Tên khách hàng:</td>
                        <td class="_r info_name"><?php echo $info_name;?></td>
                    </tr>
                    <tr>
                        <td class="_l">Tên đại lý,:</td>
                        <td class="_r info_name_agent"><?php echo $info_name_agent;?></td>
                    </tr>
                    
                    <tr>
                        <td class="_l">Địa chỉ:</td>
                        <td class="_r info_address"><?php echo $info_address;?></td>
                    </tr>
                </table>
            </div>
            <div class="clr"></div>          
                        
	</div>

	<div class="row">
		<?php echo Yii::t('translation', $form->labelEx($model,'price_50')); ?><div class="fix_number_help">
		<?php echo $form->textField($model,'price_50',array('size'=>16,'maxlength'=>14,'class'=>'number_only')); ?>
		<div class="help_number"></div>
                </div><?php echo $form->error($model,'price_50'); ?>
	</div>
<div class="clr"></div>          
	<div class="row">
		<?php echo Yii::t('translation', $form->labelEx($model,'price_45')); ?><div class="fix_number_help">
		<?php echo $form->textField($model,'price_45',array('size'=>16,'maxlength'=>14,'class'=>'number_only')); ?>
		<div class="help_number"></div>
                </div><?php echo $form->error($model,'price_45'); ?>
	</div>
<div class="clr"></div>
	<div class="row">
		<?php echo Yii::t('translation', $form->labelEx($model,'price_12')); ?><div class="fix_number_help">
		<?php echo $form->textField($model,'price_12',array('size'=>16,'maxlength'=>14,'class'=>'number_only')); ?>
		<div class="help_number"></div>
                </div><?php echo $form->error($model,'price_12'); ?>
	</div>
    <div class="clr"></div>
	<div class="row">
		<?php echo Yii::t('translation', $form->labelEx($model,'price_month')); ?>
		<?php echo $form->dropDownList($model,'price_month', ActiveRecord::getMonthVn(),array('style'=>'width:350px;')); ?>
		<?php echo $form->error($model,'price_month'); ?>
	</div>

	<div class="row">
		<?php echo Yii::t('translation', $form->labelEx($model,'price_year')); ?>
		<?php echo $form->textField($model,'price_year',array('size'=>4,'maxlength'=>4,'class'=>'number_only')); ?>
		<?php echo $form->error($model,'price_year'); ?>
	</div>

	<div class="row buttons" style="padding-left: 115px;">
		        <?php $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType'=>'submit',
            'label'=>$model->isNewRecord ? Yii::t('translation', 'Create') : Yii::t('translation', 'Save'),
            'type'=>'null', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
            'size'=>'small', // null, 'large', 'small' or 'mini'
            //'htmlOptions' => array('style' => 'margin-bottom: 10px; float: right;'),
        )); ?>	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->