<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'customer_id',array()); ?>
		<?php echo $form->hiddenField($model,'customer_id'); ?>
                <?php 
                        $this->widget('zii.widgets.jui.CJuiAutoComplete', array(
                                'attribute'=>'autocomplete_name',
                                'model'=>$model,
                                'sourceUrl'=>Yii::app()->createAbsoluteUrl('admin/gascustomer/autocompleteCustomer'),
                //                                'name'=>'my_input_name',
                                'options'=>array(
                                        'minLength'=>MIN_LENGTH_AUTOCOMPLETE,
                                        'multiple'=> true,
                                    'select'=>"js:function(event, ui) {
                                            $('#GasQuotation_customer_id').val(ui.item.id);
                                            var remove_div = '<span class=\'remove_row_item\' onclick=\'fnRemoveName(this)\'></span>';
                                            $('#GasQuotation_autocomplete_name').parent('div').find('.remove_row_item').remove();
                                            $('#GasQuotation_autocomplete_name').attr('readonly',true).after(remove_div);
                                            fnBuildTableInfo(ui.item);
                                            $('.autocomplete_customer_info').show();
                                    }",
                                ),
                                'htmlOptions'=>array(
                                    'size'=>45,
                                    'maxlength'=>45,
                                    'style'=>'float:left;',
                                    'placeholder'=>'Nhập tên hoặc mã kế toán, kinh doanh',
                                ),
                        )); 
                        ?>   
                        <div class="clr"></div>		
                        <script>
                            function fnRemoveName(this_){
                                $(this_).prev().attr("readonly",false); 
                                $("#GasQuotation_autocomplete_name").val("");
                                $("#GasQuotation_customer_id").val("");
                                $('.autocomplete_customer_info').hide();
                            }
                        function fnBuildTableInfo(item){
                            $(".info_name").text(item.value);
                            $(".info_name_agent").text(item.name_agent);
                            $(".info_code_account").text(item.code_account);
                            $(".info_code_bussiness").text(item.code_bussiness);
                            $(".info_address").text(item.address);
                        }
                            
                        </script>              
	</div>

	<div class="row">
		<?php echo $form->label($model,'price_50',array()); ?>
		<?php echo $form->textField($model,'price_50',array('size'=>16,'maxlength'=>16,'class'=>'number_only')); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'price_45',array()); ?>
		<?php echo $form->textField($model,'price_45',array('size'=>16,'maxlength'=>16,'class'=>'number_only')); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'price_12',array()); ?>
		<?php echo $form->textField($model,'price_12',array('size'=>16,'maxlength'=>16,'class'=>'number_only')); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'price_month',array()); ?>
		<?php echo $form->dropDownList($model,'price_month', ActiveRecord::getMonthVn(),array('style'=>'width:350px;','empty'=>'select')); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'price_year',array()); ?>
		<?php echo $form->textField($model,'price_year',array('size'=>4,'maxlength'=>4,'class'=>'number_only')); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'created_date',array()); ?>
		<?php echo $form->textField($model,'created_date'); ?>
	</div>

	<div class="row buttons" style="padding-left: 159px;">
		        <?php $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType'=>'submit',
            'label'=>Yii::t('translation','Search'),
            'type'=>'null', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
            'size'=>'small', // null, 'large', 'small' or 'mini'
            //'htmlOptions' => array('style' => 'margin-bottom: 10px; float: right;'),
        )); ?>	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->