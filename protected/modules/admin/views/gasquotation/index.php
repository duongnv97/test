<?php
$this->breadcrumbs=array(
	'QL Bảng Giá',
);

$menus=array(
	array('label'=> Yii::t('translation','Create GasQuotation'), 'url'=>array('create')),
);
$this->menu= ControllerActionsName::createMenusRoles($menus, $actions);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('gas-quotation-grid', {
                url : $(this).attr('action'),
		data: $(this).serialize()
	});
	return false;
});
");

Yii::app()->clientScript->registerScript('ajaxupdate', "
$('#gas-quotation-grid a.ajaxupdate').live('click', function() {
    $.fn.yiiGridView.update('gas-quotation-grid', {
        type: 'POST',
        url: $(this).attr('href'),
        success: function() {
            $.fn.yiiGridView.update('gas-quotation-grid');
        }
    });
    return false;
});
");
?>

<h1><?php echo Yii::t('translation', 'QL Bảng Giá'); ?></h1>
<h2>
    <a href="<?php echo Yii::app()->createAbsoluteUrl('admin/gasquotation/import_gasquotation');?>">
        Nhập Bảng Giá từ file Excel
    </a>    
	<br />
</h2>

<?php echo CHtml::link(Yii::t('translation','Advanced Search'),'#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'gas-quotation-grid',
	'dataProvider'=>$model->search(),
	'enableSorting' => false,
	//'filter'=>$model,
	'columns'=>array(
        array(
            'header' => 'S/N',
            'type' => 'raw',
            'value' => '$this->grid->dataProvider->pagination->currentPage * $this->grid->dataProvider->pagination->pageSize + ($row+1)',
            'headerHtmlOptions' => array('width' => '30px','style' => 'text-align:center;'),
            'htmlOptions' => array('style' => 'text-align:center;')
        ),
        array(
            'name' => 'customer_id',
            'value' => '$data->customer?$data->customer->first_name:""',
//            'htmlOptions' => array('style' => 'text-align:right;')
        ),            
        array(
            'name' => 'price_50',
            'type' => 'Currency',
            'htmlOptions' => array('style' => 'text-align:right;')
        ),            
        array(
            'name' => 'price_45',
            'type' => 'Currency',
            'htmlOptions' => array('style' => 'text-align:right;')
        ),            
        array(
            'name' => 'price_12',
            'type' => 'Currency',
            'htmlOptions' => array('style' => 'text-align:right;')
        ),            
        array(
            'name' => 'price_month',
            'htmlOptions' => array('style' => 'text-align:center;')
        ),            
        array(
            'name' => 'price_year',
            'htmlOptions' => array('style' => 'text-align:center;')
        ),            
          
          
        array(
            'header' => 'Sale',
            'value' => '$data->customer?$data->customer->sale->first_name:""',
        ),            
		            
		array(
			'header' => 'Action',
			'class'=>'CButtonColumn',
                        'template'=> ControllerActionsName::createIndexButtonRoles($actions),
		),
	),
)); ?>
