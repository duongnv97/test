<?php
$this->breadcrumbs=array(
	'QL Bảng Giá'=>array('index'),
	$model->customer?$model->customer->first_name:'',
);

$menus = array(
	array('label'=>'GasQuotation Management', 'url'=>array('index')),
	array('label'=>'Create GasQuotation', 'url'=>array('create')),
	array('label'=>'Update GasQuotation', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete GasQuotation', 'url'=>array('delete'), 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
);
$this->menu= ControllerActionsName::createMenusRoles($menus, $actions);
?>

<h1>View Bảng Giá Của: <?php echo $model->customer?$model->customer->first_name:''; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
            array(
                'name'=>'customer_id',
                'value'=>$model->customer?$model->customer->first_name:'',
            ),            
		'price_50:Currency',
		'price_45:Currency',
		'price_12:Currency',
		'price_month',
		'price_year',
array(
            'label' => 'Tên Đại Lý',
            'value' => $model->customer?$model->customer->name_agent:"",
        ),      
            
        array(
            'label' => 'Tỉnh',
            'value' => $model->customer?$model->customer->province->name:"",
        ),            
		 
        array(
            'label' => 'Kênh',
            'value' => $model->customer?$model->customer->channel->name:"",
        ),            
        array(
            'label' => 'Khu Vực',
            'value' => $model->customer?$model->customer->district->name:"",
        ),            
        array(
            'label' => 'Sale',
            'value' => $model->customer?$model->customer->sale->first_name:"",
        ),             
		'created_date:Datetime',
	),
)); ?>
