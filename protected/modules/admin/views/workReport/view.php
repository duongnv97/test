<?php
$this->breadcrumbs=array(
	$this->singleTitle,
);

$menus=array(
        array('label'=>"Create $this->singleTitle", 'url'=>array('create')),
        array('label'=>'Quản Lý Đi Chợ', 'url'=>array('index')),
);
$this->menu= ControllerActionsName::createMenusRoles($menus, $actions);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form1 form').submit(function(){
	$.fn.yiiGridView.update('work-report-grid', {
                url : $(this).attr('action'),
		data: $(this).serialize()
	});
	return false;
});
");

Yii::app()->clientScript->registerScript('ajaxupdate', "
$('#work-report-grid a.ajaxupdate').live('click', function() {
    $.fn.yiiGridView.update('work-report-grid', {
        type: 'POST',
        url: $(this).attr('href'),
        success: function() {
            $.fn.yiiGridView.update('work-report-grid');
        }
    });
    return false;
});
");
$dataProvider = $model->search();
?>

<h1>Danh Sách <?php echo $this->pluralTitle; ?></h1>

<?php include "viewMap.php"; ?>
<div class="clr"></div>
<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'work-report-grid',
	'dataProvider'=>$dataProvider,
        'afterAjaxUpdate'=>'function(id, data){ fnUpdateColorbox();}',
        'template'=>'{pager}{summary}{items}{pager}{summary}',
//    'itemsCssClass' => 'items custom_here',
//    'htmlOptions'=> array('class'=>'grid-view custom_here'),
        'pager' => array(
            'maxButtonCount'=>  CmsFormatter::$PAGE_MAX_BUTTON,
        ),
	'enableSorting' => false,
	//'filter'=>$model,
	'columns'=>array(
        array(
            'header' => 'S/N',
            'type' => 'raw',
            'value' => '$this->grid->dataProvider->pagination->currentPage * $this->grid->dataProvider->pagination->pageSize + ($row+1)',
            'headerHtmlOptions' => array('width' => '10px','style' => 'text-align:center;'),
            'htmlOptions' => array('style' => 'text-align:center;')
        ),
            array(
                'name'=>'code_no',
                'type'=>'raw',
                'value'=>'$data->code_no . "<br>".$data->getFileOnly()',
            ),
            array(
                'name'=>'uid_login',
                'value'=>'$data->getUserLoginName()',
            ),
            array(
                'name'=>'role_id',
                'value'=>'MyFormat::getRoleName($data->role_id)',
            ),
            array(
                'name'=>'content',
                'type'=>'html',
                'value'=>'$data->getContentWebView()',
            ),
            'google_map',
            array(
                'name'=>'created_date',
                'value'=>'$data->getCreatedDate()',
            ),
            array(
                'header' => 'Actions',
                'class'=>'CButtonColumn',
                'template'=> ControllerActionsName::createIndexButtonRoles($actions),
                'buttons'=>array(
                    'delete'=>array(
                        'visible'=> 'GasCheck::canDeleteData($data)',
                    ),
                ),
            ),
	),
)); ?>

<script>
$(document).ready(function() {
    fnUpdateColorbox();
});

function fnUpdateColorbox(){   
    fixTargetBlank();
    $(".gallery").colorbox({iframe:true,innerHeight:'1800', innerWidth: '1100',close: "<span title='close'>close</span>"});
}
</script>