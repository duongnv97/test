<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'work-report-form',
	'enableAjaxValidation'=>false,
        'htmlOptions' => array('enctype' => 'multipart/form-data'),
)); ?>

    <p class="note">Dòng có dấu <span class="required">*</span> là bắt buộc.</p>
    <?php echo MyFormat::BindNotifyMsg(); ?>
    
        		
        
    <div class="row">
        <?php echo $form->labelEx($model,'code_no'); ?>
        <?php echo $form->textField($model,'code_no',array('size'=>15,'maxlength'=>15)); ?>
        <?php echo $form->error($model,'code_no'); ?>
    </div>
    <div class="row">
        <?php echo $form->labelEx($model,'uid_login'); ?>
        <?php echo $form->textField($model,'uid_login',array('size'=>12,'maxlength'=>12)); ?>
        <?php echo $form->error($model,'uid_login'); ?>
    </div>
    <div class="row">
        <?php echo $form->labelEx($model,'role_id'); ?>
        <?php echo $form->textField($model,'role_id'); ?>
        <?php echo $form->error($model,'role_id'); ?>
    </div>
    <div class="row">
        <?php echo $form->labelEx($model,'content'); ?>
        <?php echo $form->textArea($model,'content',array('rows'=>6, 'cols'=>50)); ?>
        <?php echo $form->error($model,'content'); ?>
    </div>
    <div class="row">
        <?php echo $form->labelEx($model,'google_map'); ?>
        <?php echo $form->textArea($model,'google_map',array('rows'=>6, 'cols'=>50)); ?>
        <?php echo $form->error($model,'google_map'); ?>
    </div>
    <div class="row">
        <?php echo $form->labelEx($model,'created_date_only'); ?>
        <?php echo $form->textField($model,'created_date_only'); ?>
        <?php echo $form->error($model,'created_date_only'); ?>
    </div>
    <div class="row">
        <?php echo $form->labelEx($model,'created_date'); ?>
        <?php echo $form->textField($model,'created_date'); ?>
        <?php echo $form->error($model,'created_date'); ?>
    </div>
	<div class="row buttons" style="padding-left: 141px;">
		        <?php $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType'=>'submit',
            'label'=>$model->isNewRecord ? 'Create' :'Save',
            'type'=>'null', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
            'size'=>'small', // null, 'large', 'small' or 'mini'
            //'htmlOptions' => array('style' => 'margin-bottom: 10px; float: right;'),
        )); ?>	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->
<script>
    $(document).ready(function(){
        $('.form').find('button:submit').click(function(){
            $.blockUI({ overlayCSS: { backgroundColor: '#fff' } }); 
        });
        
    });
</script>