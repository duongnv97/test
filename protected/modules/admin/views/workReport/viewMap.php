<?php 
try{
    $models = $dataProvider->data;
}catch (Exception $exc){
    GasCheck::CatchAllExeptiong($exc);
}
?>

<?php if(count($models)): ?>
    <h1>Tổng số điểm tìm thấy: <?php echo count($models);?></h1>
    <?php
        $index = 1; $aDataChart = array(); $tmp = array();
        foreach($models as $mWorkReport):
            if(trim($mWorkReport->google_map) != ""){
                $tmp = explode(",", $mWorkReport->google_map);
                if(count($tmp) == 2){
                    $contentString = "<div id='content'>";
                    $contentString .= "<h1 id='firstHeading' class='firstHeading'>{$mWorkReport->getUserLoginName()}</h1>";
                        $contentString .= "<div id='bodyContent'>";
//                            $contentString .= "<p><b>Địa chỉ:</b> {$mWorkReport->getAddress()}</p>";
                            $contentString .= "<p><b>Ngày tạo:</b> ".$mWorkReport->getCreatedDate()."</p>";
                            $contentString .= "<p><b>Nội dung:</b> {$mWorkReport->getContentWebView()}</p>";
                            
                        $contentString .= "</div>";
                    $contentString .= "</div>";

                    $aDataChart[] = array($contentString, $tmp[0], $tmp[1], $index++);
                }
            }
        endforeach;
        $js_array = json_encode($aDataChart);
    ?>
    <script src="http://maps.google.com/maps/api/js?key=AIzaSyCfsbfnHgTh4ODoXLXFfEFhHskDhnRVtjQ" type="text/javascript"></script>
    <div id="map" style="width: 80%; height: 600px; float: left"></div>
    <div id="directions-panel" style="width: 15%; height: 400px; float: left; margin: 20px"></div>
    <script type="text/javascript">
        var locations = <?php echo $js_array;?>;
        var directionsDisplay = new google.maps.DirectionsRenderer;
        var directionsService = new google.maps.DirectionsService;
        var map = new google.maps.Map(document.getElementById('map'), {
          zoom: 15,
          center: new google.maps.LatLng(<?php echo isset($tmp[0]) ? $tmp[0] : 10.818463;?>, <?php echo isset($tmp[1]) ? $tmp[1] : 106.658825;?>),
          mapTypeId: google.maps.MapTypeId.ROADMAP
        });
        directionsDisplay.setMap(map);
        calculateAndDisplayRoute(directionsService, directionsDisplay);
        var infowindow = new google.maps.InfoWindow();
        
        var marker, i;
    //    var image = 'http://daukhi.huongminhgroup.com/apple-touch-icon.png';
        
        for (i = 0; i < locations.length; i++) {  
          marker = new google.maps.Marker({
            position: new google.maps.LatLng(locations[i][1], locations[i][2]),
            map: map
    //        icon: image
          });

          google.maps.event.addListener(marker, 'click', (function(marker, i) {
            return function() {
              infowindow.setContent(locations[i][0]);
              infowindow.open(map, marker);
            }
          })(marker, i));
        }
        
        function calculateAndDisplayRoute(directionsService, directionsDisplay) {
            var waypts = [];
            for (var i = locations.length - 2; i > 0; i--) {
                waypts.push({
                  location: new google.maps.LatLng(locations[i][1], locations[i][2]),
                  stopover: true
                });
            }
            var start = locations[locations.length -1];
            var end =  locations[0];
            directionsService.route({
              origin: {lat: Number(start[1]), lng: Number(start[2])},
              destination: {lat: Number(end[1]), lng: Number(end[2])}, 
              waypoints: waypts,
              optimizeWaypoints: true,
              travelMode: 'DRIVING'
            }, function(response, status) {
              if (status === 'OK') {
                directionsDisplay.setDirections(response);
                var route = response.routes[0];
                var summaryPanel = document.getElementById('directions-panel');
                summaryPanel.innerHTML = '';
                // For each route, display summary information.
                for (var i = 0; i < route.legs.length; i++) {
                  var routeSegment = i + 1;
                  summaryPanel.innerHTML += '<b>Tuyến đường: ' + routeSegment +
                      '</b><br>';
                  summaryPanel.innerHTML += '<b> Điểm ' + (i+10).toString(36).toUpperCase() + '</b>: ' + route.legs[i].start_address +'<br>';
                  summaryPanel.innerHTML += '<b> Điểm ' + (i+11).toString(36).toUpperCase() + '</b>: ' + route.legs[i].end_address +'<br>';
                  summaryPanel.innerHTML += '<b>' + route.legs[i].distance.text + '</b><br><br>';
                }
                console.log(route);
                var total = 0;
                for (var i = 0; i < route.legs.length; i++) {
                    total += route.legs[i].distance.value;
                }
                summaryPanel.innerHTML += '<b>Tổng cộng: ' + total/1000 + ' km'
                      '</b>';
              } else {
                window.alert('Directions request failed due to ' + status);
              }
            });
      }
      </script>

<?php else: ?>
<p class="f_size_15 item_c">Không có dữ liệu</p>
<?php endif; ?>
