<?php
    $mSocketNotifySearch            = new GasSocketNotify();
    $mSocketNotifySearch->pageSize  = 10;
    $mSocketNotifySearch->code  = [GasSocketNotify::CODE_SELL_ALERT_KTBH, GasSocketNotify::CODE_UPHOLD_NEW, GasSocketNotify::CODE_ORDER_BOMOI_NEW];
    $dataProvider               = $mSocketNotifySearch->searchDieuPhoi();
    $cRole = MyFormat::getCurrentRoleId();
?>
<div class="BoxSystemNotify4 BoxSystemNotify6 BoxSystemNotify7">
<h1 class="item_c" style="padding-top: 0;">Tin nhắn hệ thống</h1>

<?php if(count($dataProvider->data) > 0): ?>
<?php
    $this->widget('zii.widgets.CListView', array(
        'dataProvider' => $dataProvider,
        'viewData' => array( 'cRole' => $cRole ),    // YOUR OWN VARIABLES
        'itemView' => "item/item_order",
//        'ajaxUpdate'=>false, 
        'itemsCssClass'=>'tb hm_table',
        'htmlOptions' => array('class' => 'example', 'style'=>''),
        'pagerCssClass'=>'pager',
        'pager'=> array(
            'maxButtonCount' => 5,
            'class'=>'CLinkPager',
            'header'=> false,
            'footer'=> false,
            'prevPageLabel'=>'Prev',
            'nextPageLabel'=>'Next',
            'lastPageLabel' => '>>',
            'firstPageLabel' => '<<',
        ),
//            'summaryText' => true,
//            'summaryText'=>'Pages({page} of {pages})',
        'itemsTagName'=>'table',
        'template'=>'{pager}{summary}{items}{pager}{summary}',
    ));
?>
<?php else: ?>
    <table class="tb hm_table"><tbody></tbody></table>
<?php endif; ?>
</div>