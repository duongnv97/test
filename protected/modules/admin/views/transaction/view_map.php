<?php $tmp = explode(",", $model->google_map); ?>
<?php
$aDataChart = array();
$aDataChart[] = array($model->getTextInfoMap(), $tmp[0], $tmp[1], 0);
$jsEmployee = json_encode($aDataChart);
// echo "var javascript_array = ". $jsEmployee . ";\n";

$mGateApi = new GateApi();
$jsAgent = $mGateApi->getLocationAgent(['GetLogo24h'=>1]);

?>
<!--http://stackoverflow.com/questions/3059044/google-maps-js-api-v3-simple-multiple-marker-example
https://developers.google.com/maps/documentation/javascript/markers#animated
https://developers.google.com/maps/documentation/javascript/examples/infowindow-simple-max
-->
<script src="http://maps.google.com/maps/api/js?key=AIzaSyCfsbfnHgTh4ODoXLXFfEFhHskDhnRVtjQ" type="text/javascript"></script>
<div id="map" style="width: 100%; height: 500px;"></div>
<script type="text/javascript">
    var locations       = <?php echo $jsEmployee;?>;
    var locationsAgent  = <?php echo $jsAgent;?>;
    var map = new google.maps.Map(document.getElementById('map'), {
      zoom: 15,
      center: new google.maps.LatLng(<?php echo $tmp[0];?>, <?php echo $tmp[1];?>),
      mapTypeId: google.maps.MapTypeId.ROADMAP
    });

    var infowindow = new google.maps.InfoWindow();

    var marker, i;
//    var image = 'http://daukhi.huongminhgroup.com/apple-touch-icon.png';

    // Apr 24, 2017 for agent spj icon
    for (i = 0; i < locationsAgent.length; i++) {
        marker = new google.maps.Marker({
          position: new google.maps.LatLng(locationsAgent[i][1], locationsAgent[i][2]),
          map: map,
          label: locationsAgent[i][7], // agent name
          icon: locationsAgent[i][4]
        });

        google.maps.event.addListener(marker, 'click', (function(marker, i) {
          return function() {
            infowindow.setContent(locationsAgent[i][0]);
            infowindow.open(map, marker);
          }
        })(marker, i));

        // Add the circle for this city to the map.
      var cityCircle = new google.maps.Circle({
        strokeColor: '#81D4FA',
        strokeOpacity: 0.8,
        strokeWeight: 2,
        fillColor: '#B3E5FC',
        fillOpacity: 0.35,
        map: map,
        center: new google.maps.LatLng(locationsAgent[i][1], locationsAgent[i][2]),
        radius: locationsAgent[i][5] // in meters
      });
    }
    // locationsAgent[i][6] // is agent id
    // Apr 24, 2017 for agent spj icon

    for (i = 0; i < locations.length; i++) {  
      marker = new google.maps.Marker({
        position: new google.maps.LatLng(locations[i][1], locations[i][2]),
        map: map
//        icon: image
      });

      google.maps.event.addListener(marker, 'click', (function(marker, i) {
        return function() {
          infowindow.setContent(locations[i][0]);
          infowindow.open(map, marker);
        }
      })(marker, i));
    }
  </script>