<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'transaction-form',
	'enableAjaxValidation'=>false,
        'htmlOptions' => array('enctype' => 'multipart/form-data'),
)); ?>

    <p class="note">Dòng có dấu <span class="required">*</span> là bắt buộc.</p>
    <?php echo MyFormat::BindNotifyMsg(); ?>
    
        		
        
    <div class="row">
        <?php echo $form->labelEx($model,'transaction_key'); ?>
        <?php echo $form->textField($model,'transaction_key',array('size'=>32,'maxlength'=>32)); ?>
        <?php echo $form->error($model,'transaction_key'); ?>
    </div>
    <div class="row">
        <?php echo $form->labelEx($model,'status'); ?>
        <?php echo $form->textField($model,'status'); ?>
        <?php echo $form->error($model,'status'); ?>
    </div>
    <div class="row">
        <?php echo $form->labelEx($model,'expiry_date'); ?>
        <?php echo $form->textField($model,'expiry_date'); ?>
        <?php echo $form->error($model,'expiry_date'); ?>
    </div>
    <div class="row">
        <?php echo $form->labelEx($model,'first_name'); ?>
        <?php echo $form->textArea($model,'first_name',array('rows'=>6, 'cols'=>50)); ?>
        <?php echo $form->error($model,'first_name'); ?>
    </div>
    <div class="row">
        <?php echo $form->labelEx($model,'agent_id'); ?>
        <?php echo $form->textField($model,'agent_id',array('size'=>11,'maxlength'=>11)); ?>
        <?php echo $form->error($model,'agent_id'); ?>
    </div>
    <div class="row">
        <?php echo $form->labelEx($model,'google_map'); ?>
        <?php echo $form->textArea($model,'google_map',array('rows'=>6, 'cols'=>50)); ?>
        <?php echo $form->error($model,'google_map'); ?>
    </div>
    <div class="row">
        <?php echo $form->labelEx($model,'email'); ?>
        <?php echo $form->textField($model,'email',array('size'=>60,'maxlength'=>100)); ?>
        <?php echo $form->error($model,'email'); ?>
    </div>
    <div class="row">
        <?php echo $form->labelEx($model,'phone'); ?>
        <?php echo $form->textField($model,'phone',array('size'=>11,'maxlength'=>11)); ?>
        <?php echo $form->error($model,'phone'); ?>
    </div>
    <div class="row">
        <?php echo $form->labelEx($model,'province_id'); ?>
        <?php echo $form->textField($model,'province_id'); ?>
        <?php echo $form->error($model,'province_id'); ?>
    </div>
    <div class="row">
        <?php echo $form->labelEx($model,'district_id'); ?>
        <?php echo $form->textField($model,'district_id'); ?>
        <?php echo $form->error($model,'district_id'); ?>
    </div>
    <div class="row">
        <?php echo $form->labelEx($model,'ward_id'); ?>
        <?php echo $form->textField($model,'ward_id'); ?>
        <?php echo $form->error($model,'ward_id'); ?>
    </div>
    <div class="row">
        <?php echo $form->labelEx($model,'street_id'); ?>
        <?php echo $form->textField($model,'street_id'); ?>
        <?php echo $form->error($model,'street_id'); ?>
    </div>
    <div class="row">
        <?php echo $form->labelEx($model,'house_numbers'); ?>
        <?php echo $form->textArea($model,'house_numbers',array('rows'=>6, 'cols'=>50)); ?>
        <?php echo $form->error($model,'house_numbers'); ?>
    </div>
    <div class="row">
        <?php echo $form->labelEx($model,'ip_address'); ?>
        <?php echo $form->textField($model,'ip_address',array('size'=>20,'maxlength'=>20)); ?>
        <?php echo $form->error($model,'ip_address'); ?>
    </div>
    <div class="row">
        <?php echo $form->labelEx($model,'note'); ?>
        <?php echo $form->textArea($model,'note',array('rows'=>6, 'cols'=>50)); ?>
        <?php echo $form->error($model,'note'); ?>
    </div>
    <div class="row">
        <?php echo $form->labelEx($model,'created_date'); ?>
        <?php echo $form->textField($model,'created_date'); ?>
        <?php echo $form->error($model,'created_date'); ?>
    </div>
    <div class="row">
        <?php echo $form->labelEx($model,'json_detail'); ?>
        <?php echo $form->textArea($model,'json_detail',array('rows'=>6, 'cols'=>50)); ?>
        <?php echo $form->error($model,'json_detail'); ?>
    </div>
	<div class="row buttons" style="padding-left: 141px;">
		        <?php $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType'=>'submit',
            'label'=>$model->isNewRecord ? 'Create' :'Save',
            'type'=>'null', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
            'size'=>'small', // null, 'large', 'small' or 'mini'
            //'htmlOptions' => array('style' => 'margin-bottom: 10px; float: right;'),
        )); ?>	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->
<script>
    $(document).ready(function(){
        $('.form').find('button:submit').click(function(){
            $.blockUI({ overlayCSS: { backgroundColor: '#fff' } }); 
        });
        
    });
</script>