<link href="<?php echo Yii::app()->theme->baseUrl; ?>/css/style_sell.css" rel="stylesheet" type="text/css"/>
<div class="BoxSell">

<div class="call-group-call clearfix">
    <div class="call-form-call">
        <div class="grp-bar">
            
            <div class="left-from">
                <button class="call-btn-update">Tạo đơn hàng</button>
            </div>
            <div class="right-from">
                <div class="call-tab-bar-fone bg-green">
                    0123 456 789
                </div>
            </div>
        </div>
        <div class="call-tab-bar-fone">
            0123 456 789
        </div>
        <div class="call-tab-bar-fone bg-green">
            0123 456 789 
        </div>
        <div class="call-tab-bar-fone bg-red">
            0123 456 789 <span>Miss call</span>
        </div>
        <div class="call-form-control">

            <div class="call-ipt-form">
                    <input type="text" class="call-iptclass" placeholder="Tìm khách hàng"/>
                <button class="call-btn-update">Cập nhật số điện thoại</button>
            </div>
            <h3 class="call-name-client">
                    Lorem ipsum dolor sit amet, consectetuer adipiscing elit
            </h3>
            <div class="call-grp-client">

               <div class="call-info-client">
                    Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.
               </div>
            </div>
             <div class="call-grp-client">
               <div class="call-info-client">
                    <select class="call-ipt-slc">
                    <option>Trạng thái:</option>
                </select>
               </div>
            </div>
              <div class="call-grp-client">
               <div class="call-info-client">
                            <textarea class="call-txt-area" placeholder="Ghi chú"></textarea>
               </div>
            </div>
             <div class="call-grp-client">
                    <label class="call-lbl2"></label>
               <div class="call-info-client">
                            <button class="call-btn-update">Cập nhật</button>
               </div>
            </div>

        </div>
    </div>


    <div class="call-t-header">Lịch sử lấy hàng</div>
    <table class="call-tbl-list">
        <tr>
            <th>Lorem ipsum</th>
            <th>Lorem ipsum</th>
        </tr>
        <tr>
            <td>Lorem ipsum dolor sit amet, consectetur adipiscing elit. </td>
            <td>Lorem ipsum dolor sit amet, consectetur adipiscing elit. </td>
        </tr>
    </table>
    </div>
<div class="call-other-grp">
    <div class="call-t-header">Danh sách cuộc gọi</div>
    <table class="call-tbl-list">
        <tr>
            <th>STT</th>
            <th>Khách Hàng	</th>
            <th>Ghi chú</th>
            <th>Trạng Thái</th>
        </tr>
        <tr>
            <td>1 </td>
            <td>
                    <div class="info-grp">
                    <p>Tên KH: Vũ Thái Long</p>
                    <p>ĐT: <span class="t-phone">0908 318 328</span></p>
                    <p>Đ/C: 193A Điện Biên Phủ, Phường 15, Bình Thạnh, Hồ Chí Minh</p>
                </div>
            </td>
            <td>Lorem ipsum dolor sit amet, consectetur adipiscing elit. </td>
            <td> </td>
        </tr>
        <tr>
            <td>2</td>
            <td>
                    <div class="call-info-grp">
                    <p>Tên KH: Vũ Thái Long</p>
                    <p>ĐT: <span class="call-t-phone">0908 318 328</span></p>
                    <p>Đ/C: 193A Điện Biên Phủ, Phường 15, Bình Thạnh, Hồ Chí Minh</p>
                </div>
            </td>
            <td>Lorem ipsum dolor sit amet, consectetur adipiscing elit. </td>
            <td> </td>
        </tr>
    </table>
</div>

</div>