<?php $maxTab = 9; ?>
<link href="<?php echo Yii::app()->theme->baseUrl; ?>/css/style_sell.css" rel="stylesheet" type="text/css"/>
<div class="BoxSell">    
    <div id="tabs">
        <ul class="CallUlTab">
            <?php for($i=1; $i < $maxTab; $i++):?>
            <li class="CallLiTab<?php echo $i-1;?>"><a href="#tabs-<?php echo $i;?>">Line <?php echo $i;?></a></li>
            <?php endfor; ?>
        </ul>

        <?php for($i=1; $i < $maxTab; $i++):?>
            <?php include 'index_callcenter_line.php'; ?>
        <?php endfor; ?>
    </div>

    <?php include 'index_callcenter_history.php'; ?>
</div>

<script>
$(document).ready(function() {
     $( "#tabs" ).tabs({ active: 0 });
    changeExt();
    callBindEventElement();
//    popupSelectCustomer();
    fnBindAllAutocomplete();
    handleUpdatePhone();
    handleCallFailedUpdate();
    <?php if($cRole == ROLE_CALL_CENTER && empty(CacheSession::getCookie(CacheSession::CURRENT_USER_EXT))):?>
        popupSelectExt();
    <?php endif;?>
});

</script>
<style>
    .CallBoxUpdateFailed  .portlet-content{ padding: 0; }
</style>