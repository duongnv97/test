<?php
$this->breadcrumbs=array(
    'Danh sách cuộc gọi Call Center',
);
$cRole = MyFormat::getCurrentRoleId();
$cUid = MyFormat::getCurrentUid();
$aRoleHide = [ROLE_EMPLOYEE_MAINTAIN];
$aUidAllow  = [
    2085496, // Kiều Sơn Lâm
    GasConst::UID_PHUONG_NT,
    GasConst::UID_HA_PT, GasConst::UID_ADMIN, GasConst::UID_NGOC_PT];
$menus=[];
if(in_array($cUid, $aUidAllow)):
    $menus[] = ['label'=> 'Xuất Excel Danh Sách Hiện Tại',
                'url'=>array('index', 'ExportExcel'=>1), 
                'htmlOptions'=>array('class'=>'export_excel ','label'=>'Xuất Excel'),
    ];
endif;
$this->menu= ControllerActionsName::createMenusRoles($menus, $actions);

?>

<?php if(!in_array($cRole, $aRoleHide)) $this->widget('CallHistoryWidget', ['mCall'=>$mCall]); ?>

