<?php
$this->breadcrumbs=array(
    'Danh sách cuộc gọi Call Center' => ['index', 'ListCall'=>1],
    ' ' . $this->pageTitle . ' ',
);

$menus = array(
	array('label'=> 'Danh sách cuộc gọi Call Center', 'url'=>array('index', 'ListCall'=>1)),
);
$this->menu= ControllerActionsName::createMenusRoles($menus, $actions);
?>

<h1><?php echo $this->pageTitle; ?></h1>

<?php 
$this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
            array(
            'name'=>'direction',
//            'value'=>'$model->getDirection()."<br>".$model->getStartTime()."<br>".$model->call_uuid',
            'value'=> $model->getDirection(true)."<br>".$model->getStartTime(),
            'type'=>'raw',
            ),
            array(
                'name'=>'caller_number',
                'type'=>'raw',
                'value'=>$model->getCallNumberView(),
            ),
            array(
                'name'=>'destination_number',
                'type'=>'raw',
                'value'=>$model->getDestinationNumberView(),
                'htmlOptions' => array('style' => 'text-align:center;'),
            ),
            array(
                'label'=>'Kết thúc gọi',
                'type'=>'raw',
                'value'=>$model->getEndTime(),
            ),
            array(
                'label'=>'Thời gian gọi',
                'type'=>'raw',
                'value'=>$model->getBillDuration().' giây',
            ),
            array(
                'name'=>'user_id',
                'type'=>'raw',
                'value'=>$model->getUser(),
            ),
            array(
                'name'=>'agent_id',
                'type'=>'raw',
                'value'=>$model->getAgent(),
            ),
            array(
                'name'=>'customer_id',
                'type'=>'raw',
                'value'=>"<b>".$model->getCustomer()."</b><br>".$model->getCustomer("address"),
            ),
            array(
                'label'=>'Tạo bán hàng',
                'value'=>$model->getUrlMakeSell(),
                'type'=>'raw',
                'htmlOptions' => array('style' => 'text-align:center;'),
            ),
            array(
                'label'=>'Status',
                'value'=>$model->getCallStatus(),
                'htmlOptions' => array('style' => 'text-align:center;'),
            ),
            array(
                'label'=>'Trạng thái',
                'type'=>'raw',
                'value'=>$model->getFailedStatus(),
                'htmlOptions' => array('style' => 'text-align:center;'),
            ),
            array(
                'label'=>'Ghi chú',
                'type'=>'raw',
                'value'=>$model->getFailedNote(),
                'htmlOptions' => array('style' => 'text-align:center;'),
            ),
            array(
                'label'=>'Nghe Lại',
                'type'=>'raw',
                'htmlOptions' => array('style' => 'text-align:center;'),
                'value'=>$model->getUrlPlayAudio(),
            ),
            array(
                'name'=> 'created_date',
                'type'=> 'html',
                'value'=> $model->getCreatedDate()."<br>(".$model->id.")",
                'htmlOptions' => array('style' => 'width:50px;'),
            ),
	),
)); 
?>
<script>
    $(function(){
        $('.PlayAudioPbxIcon').click(function(){
            var td = $(this).closest('td');
            td.find('.PlayAudioPbxDiv').toggle();
        });
    });
</script>