<?php 
    $aStatusFailed = Call::model()->getArrayFailedStatus();
    unset($aStatusFailed[GasConst::SELL_COMPLETE]);
    $mSellAuto = new Sell();
    $InputHideAutoId        = 'InputHideAutoId'.$i;
    $InputSearchIdCustom    = 'InputSearchIdCustom'.$i;
?>
<a href="javascript:;" class="call-focus item_b ControlCallBoxUpdateFailed">Cuộc gọi không có bán hàng</a>
<div class="CallBoxUpdateFailed ">

    <div class="call-grp-client">
        <div class="call-info-client">
            <?php echo CHtml::radioButtonList('CallFailedStatus'.$i, 0,
                            $aStatusFailed,
                            array('separator'=>'  ',
                                   'template'=>'<span class="item">{input} {label}</span>',
                                   'class'=>"call-ipt-slc CallFailedStatus "
                                  )
                        ); ?>  
        </div>
     </div>
    
    <div class="call-grp-client">
        <input class="UpdateAgentId " name="Sell[agent_id]" id="<?php echo $InputHideAutoId;?>" type="hidden">
        <?php
            // 1. limit search kh của sale
            $url = Yii::app()->createAbsoluteUrl('admin/ajax/search_by_role', array('role'=> ROLE_AGENT));
            // widget auto complete search user customer and supplier
            $aData = array(
                'model'=>$mSellAuto,
                'field_customer_id'=>'agent_id',
                'url'=> $url,
                'name_relation_user'=>'rAgent',
                'ClassAdd' => 'w-400 call-iptclass UpdateAgentName',
                'field_autocomplete_name' => 'autocomplete_name_3',
                'placeholder'=>'Nhập mã hoặc tên đại lý',
                'InputSearchIdCustom' => $InputSearchIdCustom,
                'InputHideAutoId' => $InputHideAutoId,
            );
            $this->widget('ext.SpaAutocompleteUser.SpaAutocompleteUser',
                array('data'=>$aData));
        ?>
    </div>
    
     <div class="call-grp-client">
        <div class="call-info-client">
            <textarea class="call-txt-area CallFailedNote" name="CallFailedNote" placeholder="Ghi chú"></textarea>
        </div>
     </div>
    <div class="BoxRenderNotify"></div>
    <div class="call-grp-client">
        <label class="call-lbl2"></label>
        <div class="call-info-client">
            <button class="call-btn-update CallFailedBtnUpdate" next="<?php echo Yii::app()->createAbsoluteUrl('admin/ajaxForUpdate/callFailedUpdate');?>">Cập nhật cuộc gọi</button>
        </div>
     </div>
</div>