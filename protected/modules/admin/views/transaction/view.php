<?php $cmsFormat = new CmsFormatter(); ?>
<h1>Xem đặt hàng: <?php echo $model->getFirstName(); ?></h1>
<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
            array(
                'name' => 'agent_id',
                'value' => $model->getAgent(),
            ),
            array(
                'name'=>'first_name',
                'value'=>$model->getFirstName(),
            ),
            array(
                'name'=>'phone',
                'value'=>$model->getPhone(),
            ),
            array(
                'name'=>'email',
                'value'=>$model->getEmail(),
            ),
            array(
                'name'=>'address',
                'value'=>$model->getAddress(),
            ),
            array(
                'name'=>'note',
                'type'=>'html',
                'value'=>$model->getNote(),
            ),
            array(
                'header'=>'Chi tiết',
                'type'=>'html',
                'value'=>$model->getDetailView(),
            ),
            'created_date:Datetime',
	),
)); ?>

<?php include "view_map.php"; ?>