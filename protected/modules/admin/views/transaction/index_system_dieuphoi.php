<div class="BoxSystemNotify1">
<?php
    $mSocketNotifySearch        = new GasSocketNotify();
    $mSocketNotifySearch->code  = GasSocketNotify::CODE_STORECARD;
    $dataProvider               = $mSocketNotifySearch->searchDieuPhoi();
    $aNotify = array();
    $cRole = MyFormat::getCurrentRoleId();
?>
<h1 class="item_c" style="padding-top: 0;">Đơn hàng điều phối</h1>    
<?php include "_search_dieuphoi.php"; ?>
<?php if(count($dataProvider->data) > 0): ?>
<?php
    $this->widget('zii.widgets.CListView', array(
        'dataProvider' => $dataProvider,
        'viewData' => array( 'cRole' => $cRole ),    // YOUR OWN VARIABLES
        'itemView' => "item/item_order",
//        'ajaxUpdate'=>false, 
        'itemsCssClass'=>'tb hm_table',
        'htmlOptions' => array('class' => 'example', 'style'=>''),
        'pagerCssClass'=>'pager',
        'pager'=> array(
            'maxButtonCount' => 5,
            'class'=>'CLinkPager',
            'header'=> false,
            'footer'=> false,
            'prevPageLabel'=>'Prev',
            'nextPageLabel'=>'Next',
            'lastPageLabel' => '>>',
            'firstPageLabel' => '<<',
        ),
//            'summaryText' => true,
//            'summaryText'=>'Pages({page} of {pages})',
        'itemsTagName'=>'table',
        'template'=>'{pager}{summary}{items}{pager}{summary}',
    ));
?>
<?php else: ?>
    <table class="tb hm_table"><tbody></tbody></table>
<?php endif; ?>
</div>