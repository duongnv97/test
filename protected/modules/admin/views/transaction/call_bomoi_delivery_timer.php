<div class="WrapChkDeliveryTimer">
    <input id='ChkDeliveryTimer<?php echo $i;?>' class="JsChkDeliveryTimer float_l" type="checkbox" style="margin-bottom: 0;"> 
    <label for="ChkDeliveryTimer<?php echo $i;?>" class="item_b">Hẹn giờ giao</label>
    <br>
    <div class="DivDeliveryTimer display_none">
        <?php Yii::import('application.extensions.CJuiDateTimePicker.CJuiDateTimePicker');
        $this->widget('CJuiDateTimePicker',array(
    //            'model'=>null, //Model object
    //            'attribute'=>'delivery_timer'.$i, //attribute name
            'name'=>'delivery_timer'.$i, //attribute name
            'mode'=>'datetime', //use "time","date" or "datetime" (default)
            'language'=>'en-GB',
            'options'=>array(
                'minDate'=> '0',
                'maxDate'=> '10',
                'showAnim'=>'fold',
                'showButtonPanel'=>true,
                'autoSize'=>true,
                'dateFormat'=>'dd-mm-yy',
                'timeFormat'=>'hh:mm:ss',
                'width'=>'120',
                'separator'=>' ',
                'showOn' => 'button',
                'buttonImage'=> Yii::app()->theme->baseUrl.'/admin/images/icon_calendar_r.gif',
                'buttonImageOnly'=> true,
//                'changeMonth' => true,
//                'changeYear' => true,
                //                'regional' => 'en-GB'
            ),
            'htmlOptions' => array(
                'class' => 'w-160 OrderBoMoiDeliveryTimer',
                'readonly'=>'readonly',
            ),
        ));
        ?>
    </div>
</div>