<?php $this->breadcrumbs=array(
    $this->pageTitle,
); 
$mSetupTeam = new SetupTeam();
?>
<h1><?php echo $this->pageTitle;?> </h1>
<?php if($mSetupTeam->canMakeNotContact()): ?>
    <?php $this->widget('NotContactButtonWidget'); ?>
<?php endif; ?>


<?php echo MyFormat::BindNotifyMsg(); ?>
<div class="search-form" style="">
    <?php include '_search.php'; ?>
</div><!-- search-form -->
<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/print-store-card.css" />
<?php 
$mAppCache = new AppCache();
//$mAppCache->setAgent();
$mManyAgentUser = GasOneMany::getEmployeeOfMonitor(ONE_AGENT_MAINTAIN);
$aAgent         = $mAppCache->getAgentListdata();
$aAgentFull     = $mAppCache->getAgent();
$aUid = []; $index = 1;
foreach($mManyAgentUser as $agent_id => $aIdUser){
    $aUid = array_merge($aUid, $aIdUser);
}
$aModelUser     = Users::getArrObjectUserByRole('', $aUid);
$aHistoryChange = MonitorUpdate::getHistoryChangeAgent();
$aDriver        = Users::getArrObjectUserByRole(ROLE_DRIVER, [], ['order'=>'t.parent_id DESC']);
$aRoleOther     = [ROLE_CALL_CENTER, ROLE_DIEU_PHOI, ROLE_HEAD_TECHNICAL, ROLE_E_MAINTAIN, ROLE_SALE_ADMIN, ROLE_BRANCH_DIRECTOR, ROLE_SALE, ROLE_HEAD_GAS_BO, ROLE_SCHEDULE_CAR, ROLE_ACCOUNTING, ROLE_MONITORING_MARKET_DEVELOPMENT, ROLE_CHIEF_MONITOR, ROLE_MONITOR_AGENT, ROLE_ACCOUNTING_ZONE, ROLE_HEAD_OF_MAINTAIN];
$aOtherEmployee = Users::getArrObjectUserByRole($aRoleOther, [], ['order'=>'t.role_id ASC']);
$mCallClick             = new Call();
$mUsersTokens           = new UsersTokens();
$mUsersTokens->role_id  = [ROLE_EMPLOYEE_MAINTAIN, ROLE_EMPLOYEE_MARKET_DEVELOPMENT];
$aPvkhLoginApp          = $mUsersTokens->getByRole();
$aPvkhLoginApp          = CHtml::listData($aPvkhLoginApp, 'user_id', 'user_id');
?>


<div id="tabs">
  <ul>
    <li><a href="#tabs-1">Nhân viên giao nhận</a></li>
    <li><a href="#tabs-2">Tài xế</a></li>
    <li><a href="#tabs-3">Nhân viên khác</a></li>
  </ul>
  <div id="tabs-1">
    <?php include 'EmployeeMaintain.php'; ?>
  </div>
  <div id="tabs-2">
    <?php include 'EmployeeDriver.php'; ?>
  </div>
  <div id="tabs-3">
    <?php include 'EmployeeOther.php'; ?>
  </div>
  
</div>


<script>
  $( function() {
    $( "#tabs" ).tabs();
    $('.form').find('button:submit').click(function(){
            $.blockUI({ overlayCSS: { backgroundColor: '#fff' } }); 
        });
  } );
</script>