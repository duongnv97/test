<?php $index = 1; ?>
<table class="tb hm_table f_size_15">
    <thead>
        <tr>
            <th class="item_b">#</th>
            <th class="item_b">Tên nhân viên</th>
            <th class="item_b">Loại tài khoản</th>
            <th class="item_b">Đại lý trực thuộc</th>
            <th class="item_b">Số điện thoại</th>
        </tr>
    </thead>
    <tbody>
    <?php foreach ($aEmployee as $mUser): ?>
    <?php 
        $nameAgent      = isset($aAgent[$mUser->parent_id]) ? $aAgent[$mUser->parent_id] : '';
        $nameVi         = MyFunctionCustom::remove_vietnamese_accents($mUser->first_name);
        $nameUser       = $mUser->first_name.'<br>'.$nameVi;
    ?>
    <tr>
        <td class="item_c"><?php echo $index++;?></td>
        <td><?php echo $nameUser;?></td>
        <td><?php echo $mUser->getRoleName();?></td>
        <td><?php echo $nameAgent;?></td>
        <td>
            <?php echo $mCallClick->buildUrlMakeCall($mUser->phone);?><br>
            <?php 
                if(isset($aHistoryChange[$mUser->id])){
                    echo implode('<br>', $aHistoryChange[$mUser->id]);
                }
            ?>
        </td>
    </tr>
    <?php endforeach; ?>
    </tbody>
</table>