<?php $this->breadcrumbs=array(
    $this->pageTitle, 
); 
$mSetupTeam = new SetupTeam();
?>
<h1><?php echo $this->pageTitle;?> </h1>

<?php echo MyFormat::BindNotifyMsg(); ?>
<div class="search-form" style="">
    <?php include '_searchKtkv.php'; ?>
</div><!-- search-form -->
<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/print-store-card.css" />
<?php 
$mAppCache      = new AppCache();
$aAgent         = $mAppCache->getAgentListdata();
$aAgentFull     = $mAppCache->getAgent();
$aUid = []; $index = 1;
//$aHistoryChange = MonitorUpdate::getHistoryChangeAgent();
$aRoleUser      = [ROLE_ACCOUNTING, ROLE_ACCOUNTING_ZONE, ROLE_MONITORING_MARKET_DEVELOPMENT];
$aEmployee      = Users::getArrObjectUserByRole($aRoleUser, [], ['PaySalary'=>1, 'order'=>'t.role_id ASC']);
$mCallClick     = new Call();
$mUsersTokens   = new UsersTokens();
$aHistoryChange = MonitorUpdate::getHistoryChangeAgent();
?>
<div id="tabs">
  <ul>
    <li><a href="#tabs-1">KTKV + CV</a></li>
    <li><a href="#tabs-2">Tab2</a></li>
    <li><a href="#tabs-3">Tab3</a></li>
  </ul>
  <div id="tabs-1">
    <?php include 'EmployeeKtkv.php'; ?>
  </div>
  <div id="tabs-2">
    <?php // include 'EmployeeDriver.php'; ?>
  </div>
  <div id="tabs-3">
    <?php // include 'EmployeeOther.php'; ?>
  </div>
  
</div>


<script>
  $( function() {
    $( "#tabs" ).tabs();
    $('.form').find('button:submit').click(function(){
            $.blockUI({ overlayCSS: { backgroundColor: '#fff' } }); 
        });
  } );
</script>