<?php $index = 1; ?>
<table class="tb hm_table f_size_15">
    <thead>
        <tr>
            <th class="item_b">#</th>
            <th class="item_b">Chức vụ</th>
            <th class="item_b">Tên nhân viên</th>
            <th class="item_b">Số điện thoại</th>
        </tr>
    </thead>
    <tbody>
    <?php foreach ($aOtherEmployee as $mUser): ?>
    <tr>
        <td class="item_c"><?php echo $index++;?></td>
        <td class="">
        <?php 
            $nameAgent      = isset($aAgent[$mUser->parent_id]) ? $aAgent[$mUser->parent_id] : '';
            $nameVi         = MyFunctionCustom::remove_vietnamese_accents($mUser->first_name);
            $nameUser       = $mUser->first_name.'<br>'.$nameVi;
            echo $mUser->getRoleName();
        ?>
        </td>
        <td><?php echo $nameUser;?></td>
        <td><?php echo $mCallClick->buildUrlMakeCall($mUser->phone);?></td>
    </tr>
    <?php endforeach; ?>
    </tbody>
</table>