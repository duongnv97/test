<table class="tb hm_table f_size_15">
    <thead>
        <tr>
            <th class="item_b">#</th>
            <th class="item_b">Đại lý</th>
            <th class="item_b">Số điện thoại nhân viên</th>
        </tr>
    </thead>
    <tbody>
    <?php foreach ($mManyAgentUser as $agent_id => $aIdUser): ?>
    <?php if(!isset($aAgent[$agent_id])) continue ; 
        if($aAgentFull[$agent_id]['status'] == STATUS_INACTIVE){
//            continue ; // Sep1118 tạm open hết để theo dõi
        }
    ?>
    <tr>
        <td class="item_c"><?php echo $index++;?></td>
        <td class="">
        <?php 
            $name           = $aAgent[$agent_id];
            $address_temp   = $aAgentFull[$agent_id]['address_temp'];
            $agentCode      = $aAgentFull[$agent_id]['username'];
            $nameReal       = json_decode($address_temp, true);
            $nameReal       = isset($nameReal[UsersExtend::AGENT_NAME_REAL]) ? $nameReal[UsersExtend::AGENT_NAME_REAL] : '';
            
            $nameVi = MyFunctionCustom::remove_vietnamese_accents($aAgent[$agent_id]);
            echo $agentCode.' - '.$name.'<br>'.$nameVi.'<br>'.$nameReal;
        ?>
        </td>
        <td class="">
            <table>
            <?php foreach ($aIdUser as $user_id): ?>
                <?php 
                if(!isset($aModelUser[$user_id])){
                    continue ;
                }
                $mUser = $aModelUser[$user_id];
                $first_name = $mUser->getNameWithRole();
                if(!isset($aPvkhLoginApp[$user_id])){
                    $first_name = "<b>$first_name [OFF]</b>";
                }

                ?>
                <tr>
                    <td><?php echo $first_name;?></td>
                    <td><?php echo $mCallClick->buildUrlMakeCall($mUser->phone);?></td>
                    <td>
                    <?php 
                        if(isset($aHistoryChange[$user_id])){
                            echo implode('<br>', $aHistoryChange[$user_id]);
                        }
                    ?>
                    </td>
                </tr>
            <?php endforeach; ?>
            </table>
        </td>
    </tr>
    <?php endforeach; ?>
    </tbody>
</table>