<?php 
    $mTransactionHistory = new TransactionHistory();
    $mTransactionHistory->created_date_only = date('Y-m-d');
    $mTransactionHistory->status            = $model->status;
    if(isset($_GET['status_monitor']) && !empty($_GET['status_monitor'])){
        $mTransactionHistory->status = $_GET['status_monitor'];
    }
    $aTransactionSystem                     = $mTransactionHistory->getDataMonitor();
?>

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>
<div class="row more_col">
    <div class="col1">
        <?php echo $form->labelEx($model,'status', ['label'=>'Trạng thái']); ?>
        <?php echo $form->dropDownList($model,'status', $model->getStatusMonitor(),array('class'=>'w-200 StatusMonitor','empty'=>'Select ')); ?>
    </div>
    <div class="col2 t_padding_5">
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a class='btn_cancel f_size_14 UpdateMonitorBtn' style="text-decoration: none;" title="Cập nhật danh sách" href="javascript:;">Update</a>
    </div>
</div>
<?php $this->endWidget(); ?>