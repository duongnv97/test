<table class="tb hm_table">
    <thead>
        <tr>
            <th class="item_c">#</th>
            <th class="item_c">Bò Mối</th>
            <th class="item_c">Giờ tạo</th>
            <th class="item_c">Đại lý</th>
            <th class="item_c">Trạng thái</th>
            <th class="item_c">Phút Chờ</th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($aOrderBoMoi as $key => $item): ?>
        <?php 
            $temp = explode(' ', $item->created_date);
            $item->mapJsonFieldOneDecode('JSON_FIELD_INFO', 'json_info', 'baseArrayJsonDecodeV1');
        ?>
        <tr>
            <td class="item_c"><?php echo $key+1;?></td>
            <td class=""><?php echo "<b>{$item->getCustomerName()} </b>"; ?></td>
            <td class="item_c"><?php echo $temp[1];?></td>
            <td class="item_c"><?php echo $item->getAgent();?></td>
            <td class="item_c"><?php echo $item->getStatusText();?></td>
            <td class="item_c"><?php echo $item->getTimePass() ?></td>
        </tr>
        <?php endforeach; ?>
    </tbody>
</table>        