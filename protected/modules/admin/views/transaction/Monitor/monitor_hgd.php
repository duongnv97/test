<?php 
    $model->status = implode(',', array(Transaction::STATUS_NEW, Transaction::STATUS_DELIVERY_PROCESSING));
    $model->employee_accounting_id  = ($cRole == ROLE_CALL_CENTER) ? $cUid : 1;
    $model->created_date_only       = date('Y-m-d');
    $aTransactionHgd                = $model->getDataMonitor();
    
    $mTransactionHistory = new TransactionHistory();
    $mTransactionHistory->created_date_only         = date('Y-m-d');
    $mTransactionHistory->employee_accounting_id    = $model->employee_accounting_id;
    $mTransactionHistory->status = implode(',', array(Transaction::STATUS_CANCEL_BY_CUSTOMER, Transaction::STATUS_CANCEL_BY_EMPLOYEE));
    $aTransactionCancel = $mTransactionHistory->getDataMonitor(['order'=>'t.id DESC']);

?>
<div class="form">
    <div class="row more_col">
        <div class="col1 MonitorHgd" style="width: 50%;">
            <h1>ĐH của bạn</h1>
            <?php include 'tb_hgd.php'; ?>
            <h1>ĐH Hủy của bạn</h1>
            <?php $aTransactionHgd = $aTransactionCancel; include 'tb_hgd.php'; ?>
        </div>
        <div class="col2 l_padding_10 BoxOrderAgent" style="width: 45%;">
            <h1>ĐH đại lý</h1>
            <?php include 'tb_agent.php'; ?>
        </div>
    </div>
</div>

<div class="form">
    <div class="row">
        <h1>Toàn hệ thống HGĐ</h1>
        <?php include 'tb_system_search.php'; ?>
        <div class="MonitorSystem">
            <?php $aTransactionHgd = $aTransactionSystem; include 'tb_hgd.php'; ?>
        </div>
    </div>
</div>