<?php 
    // xử lý find 1 lần ra hết event của các đơn hàng đã xác nhận, chứ find trong for thì quá chậm
    $mTransactionEvent = new TransactionEvent();
    $mTransactionEvent->type                    = TransactionEvent::TYPE_SELL;
    $mTransactionEvent->action_type             = Transaction::EMPLOYEE_DROP;
    $mTransactionEvent->created_date            = date('Y-m-d');
//    $mTransactionEvent->created_date            = '2017-07-27';
    $listEventCancel   = $mTransactionEvent->getByTypeAndDate();
    $aModelEvent = $aTranHistoryId = $aModelTranHistory = [];
    foreach($listEventCancel as $item){
        $aModelEvent[$item->transaction_history_id] = $item;
        $aTranHistoryId[$item->transaction_history_id] = $item->transaction_history_id;
    }
    $sParamsIn = implode(',', $aTranHistoryId); $aTranHistory = [];
    $criteria = new CDbCriteria();
    $criteria->addCondition("t.id IN ($sParamsIn)");
    if(count($aTranHistoryId)){
        $aTranHistory = TransactionHistory::model()->findAll($criteria);
        foreach($aTranHistory as $item){
            $aModelTranHistory[$item->id] = $item;
        }
    }
    
    $mTransactionEvent                          = new TransactionEvent();
    $mTransactionEvent->transaction_history_id  = $aTranHistoryId;
    $mTransactionEvent->type                    = TransactionEvent::TYPE_SELL;
    $aEvent = $mTransactionEvent->getByArrayTransactionHistoryId();
    
?>

<table class="tb hm_table">
    <thead>
        <tr>
            <th class="item_c">#</th>
            <th class="item_c">Hộ GĐ</th>
            <th class="item_c">Giờ tạo</th>
            <th class="item_c">Đại lý</th>
            <th class="item_c">Trạng thái</th>
            <th class="item_c">Phút</th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($aModelEvent as $transaction_history_id => $itemEvent): ?>
        <?php 
            if(!isset($aModelTranHistory[$transaction_history_id])){
                continue ;
            }
            $mTransHistory = $aModelTranHistory[$transaction_history_id];
            $temp = explode(' ', $mTransHistory->created_date);
            $note = $eventLog = $employeePhone = $callCenterName = '';
            if(!empty($mTransHistory->note)){
                $note = '<br><b> Ghi chú: '.$mTransHistory->note.'</b>';
            }
            if(!empty($mTransHistory->employee_maintain_id)):
                if(isset($aEvent[$mTransHistory->id])){
                    foreach($aEvent[$mTransHistory->id] as $mEvent){
                        $eventLog   .= $mEvent->formatView($aEmployee, $aActiontype);
                    }
                }
                if(isset($aEmployeeMaintain[$mTransHistory->employee_maintain_id])){
                    $employeePhone = '<br><br>GN: '.$aEmployeeMaintain[$mTransHistory->employee_maintain_id]['phone'];
                }
            endif;
            $callCenterName = isset($aCallCenter1[$mTransHistory->employee_accounting_id]) ? $aCallCenter1[$mTransHistory->employee_accounting_id] : '';
            
        ?>
        <tr>
            <td class="item_c"><?php echo $key+1;?></td>
            <td class="">
                <?php echo "[<i>$callCenterName</i>]<br>"."<b>$mTransHistory->first_name - {$mTransHistory->getPhoneWebView()}</b><br>$mTransHistory->address $note $eventLog"; ?>
            </td>
            <td class="item_c"><?php echo $temp[1];?></td>
            <td class="item_c"><?php echo $mTransHistory->getAgent().$employeePhone;?></td>
            <td class="item_c"><?php echo $mTransHistory->getStatusApp();?></td>
            <td class="item_c"><?php echo $itemEvent->getTimePass() ?></td>
        </tr>
        <?php endforeach; ?>
    </tbody>
</table>        