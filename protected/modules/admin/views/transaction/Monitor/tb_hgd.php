<?php 
    // xử lý find 1 lần ra hết event của các đơn hàng đã xác nhận, chứ find trong for thì quá chậm
    $aTransactionId = $aTransactionIdComment = [];
    foreach ($aTransactionHgd as $key => $item):
        $aTransactionIdComment[] = $item->id;
        if(!empty($item->employee_maintain_id)):
            $aTransactionId[] = $item->id;
        endif;
    endforeach;
    $mTransactionEvent = new TransactionEvent();
    $mTransactionEvent->transaction_history_id = $aTransactionId;
    $mTransactionEvent->type = TransactionEvent::TYPE_SELL;
    $aEvent         = $mTransactionEvent->getByArrayTransactionHistoryId();
    $mAppCache      = new AppCache();
    $aAgent         = $mAppCache->getAgentListdata();
    
    $mComment               = new GasComment();
    $mComment->belong_id    = $aTransactionIdComment;
    $mComment->type         = GasComment::TYPE_3_MONITOR_HGD;
    $aComment = $mComment->getByArrayTransactionHistoryId();
    $session=Yii::app()->session;
    $aHideRow = [];
    if(isset($session['ARR_MONITOR_HIDE'])){
        $aHideRow = $session['ARR_MONITOR_HIDE'];
    }
?>

<table class="tb hm_table">
    <thead>
        <tr>
            <th class="item_c">#</th>
            <th class="item_c">Hộ GĐ</th>
            <th class="item_c">Giờ tạo</th>
            <th class="item_c">Đại lý</th>
            <th class="item_c">Trạng thái</th>
            <th class="item_c">Phút Chờ</th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($aTransactionHgd as $key => $item): ?>
        <?php $temp = explode(' ', $item->created_date);
            $note = $eventLog = $employeePhone = $callCenterName = '';
            if(!empty($item->note)){
                $note = '<br><b> Ghi chú: '.$item->note.'</b>';
            }
            if(!empty($item->employee_maintain_id)):
                if(isset($aEvent[$item->id])){
                    foreach($aEvent[$item->id] as $mEvent){
                        $mEvent->aAgent  = $aAgent;
                        $eventLog   .= $mEvent->formatView($aEmployee, $aActiontype);
                    }
                }
                if(isset($aEmployeeMaintain[$item->employee_maintain_id])){
                    $employeePhone = '<br><br>GN: '.$aEmployeeMaintain[$item->employee_maintain_id]['phone'];
                }
            endif;
            $callCenterName = isset($aCallCenter1[$item->employee_accounting_id]) ? $aCallCenter1[$item->employee_accounting_id] : '';
            $comment = isset($aComment[$item->id]) ? implode('', $aComment[$item->id]) : '';
            $divNote = '<div class="BlockNote item_l">'.$comment.'</div>';
            $itemHide = 'ItemShow';
            if(in_array($item->id, $aHideRow)){
                $itemHide = 'ItemHide';
            }
            $agentName           = $aAgent[$item->agent_id];
            $agentNameVi         = MyFunctionCustom::remove_vietnamese_accents($agentName);
            $agentName              =  $agentName.'<br>'.$agentNameVi;
        ?>
        <tr>
            <td class="item_c"><?php echo $key+1;?></td>
            <td class="">
                <span class="<?php echo $itemHide;?>">
                <?php echo "[<i>$callCenterName</i>] -- {$item->id}<br>"."<b>$item->first_name - {$item->getPhoneWebView()}</b><br>$item->address $note $eventLog"; ?>
                </span>
            </td>
            <td class="item_c"><?php echo $temp[1];?></td>
            <td class="BlockComment">
                <span class="<?php echo $itemHide;?>">
                    <span class="item_c"><?php echo $agentName.$employeePhone?></span>
                    <?php echo $divNote;?>
                </span>
                <?php echo $item->getUrlMonitorNote(['ItemHide'=>$itemHide]);?>
            </td>
            <td class="item_c"><?php echo $item->getStatusApp();?></td>
            <td class="item_c"><?php echo $item->getTimePass() ?></td>
        </tr>
        <?php endforeach; ?>
    </tbody>
</table>        