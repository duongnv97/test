<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>
<div class="row">
    <?php echo $form->hiddenField($model,'agent_id', array('class'=>'agent_id_hide')); ?>
    <?php
        // 1. limit search kh của sale
        $url = Yii::app()->createAbsoluteUrl('admin/ajax/search_by_role', array('role'=> ROLE_AGENT));
        // widget auto complete search user customer and supplier
        $aData = array(
            'model'=>$model,
            'field_customer_id'=>'agent_id',
            'url'=> $url,
            'name_relation_user'=>'rAgent',
            'ClassAdd' => 'w-350',
            'field_autocomplete_name' => 'autocomplete_name',
            'placeholder'=>'Nhập mã hoặc tên đại lý',
            'ShowTableInfo'=> 0,
            'fnSelectCustomer'=>'fnSelectAgent',
        );
        $this->widget('ext.SpaAutocompleteCustomer.SpaAutocompleteCustomer',
            array('data'=>$aData));
        ?>
    <?php echo $form->error($model,'agent_id'); ?>
</div>
<?php $this->endWidget(); ?>

<div class="BoxOrderAgentTb">
<?php if(isset($_GET['agent_id'])): ?>
<?php 
$mTransactionHistory = new TransactionHistory();
$mTransactionHistory->status                    = $model->status;
$mTransactionHistory->employee_accounting_id    = 0;
$mTransactionHistory->agent_id                  = $_GET['agent_id'];
$mTransactionHistory->created_date_only         = date('Y-m-d');
$aTransactionHgd                                = $mTransactionHistory->getDataMonitor();

$mAppOrder = new GasAppOrder();
$mAppOrder->status          = implode(',', $mAppOrder->getAppStatusNew());
$mAppOrder->agent_id        = $_GET['agent_id'];
$mAppOrder->date_delivery   = date('Y-m-d');
$aOrderBoMoi                = $mAppOrder->getDataMonitor();

?>
    <?php include 'tb_hgd.php'; ?>
    <?php include 'tb_bomoi.php'; ?>
<?php endif; ?>
</div>