<?php
$this->breadcrumbs=array(
    $this->pageTitle,
);
$cRole              = MyFormat::getCurrentRoleId();
$cUid               = MyFormat::getCurrentUid();
$mAppCache          = new AppCache();
$aEmployeeMaintain  = $mAppCache->getUserByRole(ROLE_EMPLOYEE_MAINTAIN, ['GetActive'=>1]);

$aEmployee          = $mAppCache->getListdataUserByRole(ROLE_EMPLOYEE_MAINTAIN);
$mTransaction       = new Transaction();
$aActiontype        = $mTransaction->getArrayActionType();

$aCallCenter1 = $mAppCache->getListdataUserByRole(ROLE_DIEU_PHOI);
$aCallCenter2 = $mAppCache->getListdataUserByRole(ROLE_CALL_CENTER);
$aCallCenter1 = $aCallCenter1 + $aCallCenter2;

?>
<div class="form">
    <?php
        $LinkNormal = Yii::app()->createAbsoluteUrl('admin/transaction/index', ['monitor_order'=>1]);
        $LinkCancel = Yii::app()->createAbsoluteUrl('admin/transaction/index', ['monitor_order'=>1, 'monitor_cancel'=>1]);
    ?>
    <h1>
        <?php if(!isset($_GET['monitor_cancel'])): ?>
            <a class='btn_cancel f_size_14 UpdateMonitor' href="javascript:;" next="<?php echo $LinkNormal;?>">Cập nhật</a>
        <?php endif; ?>
        <a class='btn_cancel f_size_14 ' href="<?php echo $LinkNormal;?>" next="">Giám sát</a>
        <a class='btn_cancel f_size_14' href="<?php echo $LinkCancel;?>" next="">Đơn hàng hủy HGĐ</a>
    </h1>
</div>

<?php if(isset($_GET['monitor_cancel'])): ?>
    <?php include 'monitor_cancel.php'; ?>
<?php else: ?>
    <?php if($cRole==ROLE_DIEU_PHOI): ?>
        <?php include 'monitor_bomoi.php'; ?>
    <?php else: ?>
        <?php include 'monitor_hgd.php'; ?>
    <?php endif; ?>
<?php endif; ?>
<style>
    .ItemHide {display: none;}
</style>
<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/print-store-card.css" />
<script>
$(document).ready(function() {
    updateMonitor();
    changeStatus();
    setHideNote();
});

/**
* @Author: ANH DUNG Jul 13, 2017
* @Todo: khi chọn lại đại lý thì load table bên dưới
*/
function fnSelectAgent(user_id, idField, idFieldCustomer){
    var next = '<?php echo Yii::app()->createAbsoluteUrl('admin/transaction/index', ['monitor_order'=>1]);?>/agent_id/' + user_id;
    $.blockUI({ overlayCSS: { backgroundColor: '#fff' } });
    $.ajax({
        url: next,
        type: 'html',
        success:function(data){
            $.unblockUI();
//            console.log($(data).find('.BoxOrderAgentTb').html());
            $('.BoxOrderAgentTb').html($(data).find('.BoxOrderAgentTb').html());
        }
    });   
}
function updateMonitor(){
    $('.UpdateMonitor').click(function(){
        var next        = $(this).attr('next');
        var agent_id    = $('.agent_id_hide').val();
        if(agent_id > 0){
            fnSelectAgent(agent_id, '', '');
        }
        $.blockUI({ overlayCSS: { backgroundColor: '#fff' } });
        $.ajax({
            url: next,
            type: 'html',
            success:function(data){
                $.unblockUI();
    //            console.log($(data).find('.BoxOrderAgentTb').html());
                $('.MonitorHgd').html($(data).find('.MonitorHgd').html());
                $('.MonitorSystem').html($(data).find('.MonitorSystem').html());
            }
        });
    });
    $('.UpdateMonitorBtn').click(function(){
        $(this).closest('.more_col').find('.StatusMonitor').trigger('change');
    });
}
function changeStatus(){
    $('body').on('change', '.StatusMonitor', function(){
        var status_monitor = $(this).val();
        var next = $('.UpdateMonitor').attr('next') + '/status_monitor/'+status_monitor;
//        console.log(status_monitor);return ;
        $.blockUI({ overlayCSS: { backgroundColor: '#fff' } });
        $.ajax({
            url: next,
            dataType: 'html',
            success:function(data){
                $.unblockUI();
                $('.MonitorSystem').html($(data).find('.MonitorSystem').html());
            }
        });
    });
    
    $('body').on('click', '.MonitorNote', function(){
        var div         = $(this).closest('.BlockComment');
        var content     = div.find('.MonitorNoteInput').val();
        var next        = $(this).attr('next');
//        console.log(content);return ;
        $.blockUI({ overlayCSS: { backgroundColor: '#fff' } });
        $.ajax({
            url: next,
            type: 'post',
            data: {content:content},
            dataType: 'json',
            success:function(data){
                $.unblockUI();
                var span = '<span><b>'+data['msg']+'</b> '+content+'<br></span>';
                div.find('.BlockNote').append(span);
                div.find('.MonitorNoteInput').val('');
            }
        });
    });
}

/** @Author: ANH DUNG Aug 20, 2017
*  @Todo: something
*/
function setHideNote(){
    $('.HideNote').click(function(){
        var tr = $(this).closest('tr');
        tr.find('.ItemShow').addClass('display_none');
        var next = $(this).attr('next');
        $.ajax({
            url: next,
            type: 'html',
            success:function(data){
            }
        });   
    });
    $('.ShowNote').click(function(){
        var tr = $(this).closest('tr');
        tr.find('.ItemHide').removeClass('ItemHide');
        return ;
        var next = $(this).attr('next');
        $.ajax({
            url: next,
            type: 'html',
            success:function(data){
            }
        });   
    });
    
    
}

</script>