<?php 
$this->breadcrumbs=array( 'Thông báo đặt hàng' );
$menus=array();
$this->menu= ControllerActionsName::createMenusRoles($menus, $actions);
Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('transaction-grid', {
                url : $(this).attr('action'),
		data: $(this).serialize()
	});
	return false;
});
");

Yii::app()->clientScript->registerScript('ajaxupdate', "
$('#transaction-grid a.ajaxupdate').live('click', function() {
    $.fn.yiiGridView.update('transaction-grid', {
        type: 'POST',
        url: $(this).attr('href'),
        success: function() {
            $.fn.yiiGridView.update('transaction-grid');
        }
    });
    return false;
});
");
?>
<?php echo CHtml::link('Tìm Kiếm Nâng Cao','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display: none;">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<div class="row more_col">
    <div class="col1 w-200">&nbsp;</div>
    <div class="col2 w-140">
        <button onclick="audioPause('AudioPlayer');">Stop audio</button>
    </div>
</div>

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'transaction-grid',
	'dataProvider'=>$model->search(),
        'afterAjaxUpdate'=>'function(id, data){ fnUpdateColorbox();}',
        'template'=>'{pager}{summary}{items}{pager}{summary}',
//    'itemsCssClass' => 'items custom_here',
//    'htmlOptions'=> array('class'=>'grid-view custom_here'),
        'pager' => array(
            'maxButtonCount'=>  CmsFormatter::$PAGE_MAX_BUTTON,
        ),
	'enableSorting' => false,
	//'filter'=>$model,
	'columns'=>array(
        array(
            'header' => 'S/N',
            'type' => 'raw',
            'value' => '($this->grid->dataProvider->pagination->currentPage * $this->grid->dataProvider->pagination->pageSize + ($row+1))',
//            'value' => '$data->getImageNew()',
            'headerHtmlOptions' => array('width' => '10px','style' => 'text-align:center;'),
            'htmlOptions' => array('style' => 'text-align:center;')
        ),
            array(
                'header' => 'Khách Hàng',
                'type'=>'raw',
                'value'=>'$data->getHtmlDetail()',
            ),
            array(
                'header'=>'Chi tiết',
                'type'=>'raw',
                'value'=>'$data->getDetailView()."<br>".$data->getUrlHideOrder()."<br>".$data->getNote()."<br>".$data->getEmployeeAccounting()',
                'htmlOptions' => array('class' => 'w-200'),
            ),
            array(
                'header'=>'Actions',
                'type'=>'raw',
                'value'=>'$data->getHtmlAction()',
                'htmlOptions' => array('class' => 'w-100'),
            ),
            
            array(// Không nên đưa xóa vào chỗ này
                'visible'=>Yii::app()->user->role_id == ROLE_ADMIN,
                'header' => 'Actions',
                'class'=>'CButtonColumn',
                'template'=> ControllerActionsName::createIndexButtonRoles($actions),
                'buttons'=>array(
//                    'update' => array(
//                        'visible' => '$data->canUpdate()',
//                    ),
                    'delete'=>array(
                        'visible'=> 'GasCheck::canDeleteData($data)',
                    ),
                ),
            ),
            
	),
)); ?>