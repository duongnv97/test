<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/jquery.colorbox-min.js"></script>
<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/admin/css/colorbox.css"/>
<div class="form ">
    <h1 class="f_size_12">
        <a class='btn_cancel  display_none' style="text-decoration: none;" href="<?php echo Yii::app()->createAbsoluteUrl('admin');?>">Trang chủ</a>
        &nbsp;&nbsp;<a class="ChangeExt" href="javascript:;" style="text-decoration: none;"> EXT: <span class="CurrentExt"><?php echo CacheSession::getCookie(CacheSession::CURRENT_USER_EXT); ?></span></a> - <?php echo Yii::app()->user->first_name;?>
        <?php echo $model->renderHtmlChangeRole(). $model->renderListExtOnline();?>
    </h1> 
</div>
<input type="hidden" class="TodayText" value="<?php echo date('d-m-Y');?>">
<input type="hidden" class="UrlBoMoiSubmit" value="<?php echo Yii::app()->createAbsoluteUrl('admin/ajaxForUpdate/makeOrderBoMoi');?>">
<?php $mMaterialsType = new GasMaterialsType(); ?>
<script type="text/javascript">
var availableMaterialsOther = <?php echo MyFunctionCustom::getMaterialsJson(array('MaterialsTypeAddIn' => $mMaterialsType->getTypeCallCenterMakeOrderBoMoi())); ?>;
</script>

<?php 
$cRole          = MyFormat::getCurrentRoleId();
$aRoleAllow     = GasSocketNotify::getRoleAllowSocket();
$mUser          = Users::model()->findByPk(MyFormat::getCurrentUid());
?>
<?php if(in_array($cRole, $aRoleAllow) && !isset($_GET['order_status'])): ?>
<?php $this->widget('WebSocketWidget', []); ?>
<?php endif; ?>
<?php echo MyFormat::BindNotifyMsg(); ?>

<div class="f_size_151">
    <div class="float_l" style="width: 50%;">
        <?php if(in_array($cRole, GasSocketNotify::getRoleViewCallCenter())): ?>
            <?php include "index_callcenter.php"; ?>
        <?php endif; ?>
        
        <?php if(!isset($_GET['order_status'])): ?>
            <?php include "_search_dieuphoi.php"; ?>
            <?php include "index_system.php"; ?>
        <?php endif; ?>
    </div>
    <div class="float_l" style="width: 50%;">
        <?php $mPromotionUser = new AppPromotionUser();?>
        <?php if(in_array($cRole, $model->getRoleViewGas24h())): ?>
            <?php if($model->isExtCallCenter()): ?>
                <?php include "index_button.php"; ?>
                <?php include "index_app_order.php"; ?>
            <?php endif; ?>
            <?php include "index_call_monitor.php"; ?>
        <?php endif; ?>
    </div>
    <div class="clr"></div>
</div>
<?php include "index_select_agent.php"; ?>
<?php include "index_auto_other_item_js.php"; ?>

<style>
#cboxClose { position: relative !important;}
.ui-datepicker-calendar {z-index: 5;}
/*.CallBoxUpdateFailed .item {width: 30.5%; display: inline-block; vertical-align: top; font-size: 11px; padding-left: 2px; padding-right: 2px; }*/
</style>
<script>
$(document).ready(function() {
    fnUpdateColorbox();
    bindChangeAgent();
    bindBoMoiAutocompleteOtherItem();
});

function fnUpdateColorbox(){
    fixTargetBlank();
//    $(".view").colorbox({iframe:true,innerHeight:'1000', innerWidth: '800',close: "<span title='close'>close</span>"});
    
    $('body').on('click', '.UrlMakeSell', function(){
        var tr = $(this).closest('tr');
        tr.remove();
        return true;
    });
    $('body').on('click', '.HideOrder', function(){
        var tr = $(this).closest('tr');
        tr.remove();
        return true;
    });
    $(".IframeCreateCustomer").colorbox({closeButton: false,overlayClose: false, top: true, iframe:true, innerHeight:'800', innerWidth: '700',close: "<span title='close'>close</span>"});
}
</script>

<script>
//    http://stackoverflow.com/questions/10311341/confirmation-before-closing-of-tab-browser
//window.onbeforeunload = function (e) {
//    e = e || window.event;
//
//    // For IE and Firefox prior to version 4
//    if (e) {
//        e.returnValue = 'Sure?';
//    }
//
//    // For Safari
//    return 'Sure?';
//};
</script>

<script>

// Mar 15, 2017 request permission on page load
document.addEventListener('DOMContentLoaded', function () {
  if (!Notification) {
    alert('Desktop notifications not available in your browser. Try Chromium.'); 
    return;
  }

  if (Notification.permission !== "granted")
    Notification.requestPermission();
});

function notifyMe($title, $message) {
    audioPlay('AudioPlayerNotify');
  if (Notification.permission !== "granted")
    Notification.requestPermission();
  else {
    var notification = new Notification($title, {
      icon: 'http://spj.daukhimiennam.com/upload/ImageApp/icon/spj-icon-158.png',
      body:  '' + $message
    });

    notification.onclick = function () {
//      window.open("http://stackoverflow.com/a/13328397/1269037");      
    };
  }
}

</script>


<!-- May 21, 2017 for Bo Moi action-->
<script>
$(document).ready(function() {
    fnBindAllAutocompleteAgent();
    bindEventBoMoi();
    fnOrderBoMoiType();
    fnOrderBoMoiSubmit();
    fnCheckDeliveryTimer();
    $('.OrderBoMoiCustomerView').click(function(){
        $('.OrderBoMoiCustomerInfo').toggle();
    });
});

function fnBindAllAutocompleteAgent(){
    var urlSource = '<?php echo Yii::app()->createAbsoluteUrl('admin/ajax/search_by_role', array('role'=> ROLE_AGENT));?>';
    $('.OrderBoMoiAgentName').each(function(){
        fnBindAutocompleteAgent($(this), urlSource);
    });
}

function fnCheckDeliveryTimer(){
    $('body').on('click', '.JsChkDeliveryTimer', function(){
        var div = $(this).closest('.WrapChkDeliveryTimer');
        if($(this).is(':checked')){
            div.find('.DivDeliveryTimer').show();
        }else{
            div.find('.DivDeliveryTimer').hide();
            div.find('.OrderBoMoiDeliveryTimer').val('');
        }
    });
}

</script>

<!-- May 21, 2017 for Bo Moi action-->