<?php 
$display_none_bo_moi = 'display_none';
if($model->isRoleBoMoi($cRole)): 
    $display_none_bo_moi = '';
endif;
$mAppOrder = new GasAppOrder();
?>

<div class="box-cart <?php echo $display_none_bo_moi;?>">
    <!--<div class="call-t-header">Đặt hàng</div>-->
    <?php include 'call_bomoi_action_other_item.php'; ?>
    <div class='list-cart'>
        <div class='item-cart'>
            <div class="lbl-cart">B50</div>
            <div class="grp-ipt-cart WrapChangeQty" NameQty="CallB50">
                <input type="text" class='ipt-cart CallB50 number_only_v1' name="" value="0" maxlength="2">
                <span class="btn-cart cart-up SpjCallUp"></span>
                <span class="btn-cart cart-down SpjCallDown"></span>
            </div>
        </div>
        <div class='item-cart'>
            <div class="lbl-cart">B45</div>
            <div class="grp-ipt-cart WrapChangeQty" NameQty="CallB45">
                <input type="text" class='ipt-cart CallB45 number_only_v1' name="" value="0" maxlength="2">
                <span class="btn-cart cart-up SpjCallUp"></span>
                <span class="btn-cart cart-down SpjCallDown"></span>
            </div>
        </div>
        <div class='item-cart'>
            <div class="lbl-cart">B12</div>
            <div class="grp-ipt-cart WrapChangeQty" NameQty="CallB12">
                <input type="text" class='ipt-cart CallB12 number_only_v1' name="" value="0" maxlength="2">
                <span class="btn-cart cart-up SpjCallUp"></span>
                <span class="btn-cart cart-down SpjCallDown"></span>
            </div>
        </div>
        <div class='item-cart'>
            <div class="lbl-cart">B6</div>
            <div class="grp-ipt-cart WrapChangeQty" NameQty="CallB6">
                <input type="text" class='ipt-cart CallB6 number_only_v1' name="" value="0" maxlength="2">
                <span class="btn-cart cart-up SpjCallUp"></span>
                <span class="btn-cart cart-down SpjCallDown"></span>
            </div>
        </div>
    </div>
    <?php include 'call_bomoi_form.php'; ?>
</div>

