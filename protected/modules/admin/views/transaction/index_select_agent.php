<div class="display_none">
    <div class="BoxSelectExt form w-600" id="BoxSelectExt" style="height: 400px">
        <form method="post">
            <h1 class="item_c RemoveCloseColorBox">Chọn EXT bạn đang ngồi</h1>
            <ul class="">
                <?php foreach (Call::getListExtAgent($mUser) as $ext): ?>
                    <li class="w-100" style="font-size: 28px; float: left; margin: 5px; ">
                        <a class="ExtUserSelect" href="javascript:;" style="color: #2962FF"><?php echo $ext;?></a>
                    </li>
                <?php endforeach; ?>
            </ul>
        </form>
    </div>
</div>
<div class="display_none">
    <div class="BoxSelectCustomer w-700" id="BoxSelectCustomer" style="height: 400px">
        <h1 class="item_c">Số gọi đến: <span class="BoxSelectCustomerPhoneNumber">09337222 223</span></h1>
        <ul class="">
            <?php foreach (Call::getListExtAgent($mUser) as $ext): ?>
                <li class="w-600 float_l f_size_15">
                    <a class="item_b f_size_20 RowCustomerName" href="javascript:;" style="color: #2962FF">Nguyễn Văn Minh Châu</a><br>
                    <p class="RowCustomerPhone">09337222 223-09337222 223-09337222 223-09337222 223-09337222 223<br>
                        301C, Cây Sung, Phường 14, Quận 8, TP Hồ Chí Minh
                    </p>
                </li>
            <?php endforeach; ?>
        </ul>
    </div>
</div>

<?php $urlUpdateExt = Yii::app()->createAbsoluteUrl('admin/transaction/index'); ?>
<script>


/**
* @Author: ANH DUNG Mar 11, 2017
* @Todo: show popup select Current ext
*/
function popupSelectExt(){
    $.colorbox({
        top: true, 
        inline: true, 
        href: "#BoxSelectExt",
        closeButton: false,
        overlayClose: false, 
        escKey: false
    });
}
function popupSelectCustomer(){
    $.colorbox({
        top: true, 
        inline: true, 
        href: "#BoxSelectCustomer",
        closeButton: false,
        overlayClose: false, 
//        escKey: false
    });
}

function changeExt(){
    $('.ChangeExt').click(function(){
        <?php if(Yii::app()->params['EnableChangeExt'] == 'no'):?>
            return ;
        <?php endif;?>
        popupSelectExt();
    });
    
    $('.ExtUserSelect').click(function(){
        $('.CurrentExt').text($(this).text());
        var url_ = '<?php echo $urlUpdateExt;?>'+'?UpdateExt='+$(this).text();
        $.ajax({
            url: url_,
            success:function(data){
                window.location.reload(false);
                $.colorbox.close();
            }
        });
    });
}

function fnBindAllAutocomplete(){
    $('.FindCustomer').each(function(){
        fnBindAutocomplete($(this));
    });
}

// to do bind autocompelte for input
// @param objInput : is obj input ex  $('.customer_autocomplete')
function fnBindAutocomplete(objInput){
    var WrapOneTab = objInput.closest('.WrapOneTab');
    objInput.autocomplete({
        source: objInput.attr('next'),
        minLength: '2',
        close: function( event, ui ) {
//            objInput.val('');// chỗ này search sai bị xóa thông tin search
        },
        search: function( event, ui ) {
            objInput.addClass('grid-view-loading-gas');
        },
        response: function( event, ui ) { 
            objInput.removeClass('grid-view-loading-gas');
            var json = $.map(ui, function (value, key) { return value; });
            if(json.length<1){
                var error = '<div class=\'errorMessage clr autocomplete_name_text\'>Không tìm thấy dữ liệu.</div>';
                if(WrapOneTab.find('.autocomplete_name_text').size()<1){
                    WrapOneTab.find('.remove_row_item').after(error);
                }
                else
                    WrapOneTab.find('.autocomplete_name_text').show();
            }
        },
        select: function( event, ui ) {
            var phone_number = WrapOneTab.find('.CallPhoneNumber').text();
            if($.trim(phone_number) != ''){
                WrapOneTab.find('.BtnUpdatePhone').show();
            }
            setInfoCustomer(WrapOneTab, ui.item.id, ui.item.name_customer, ui.item.phone, ui.item.address);
        }

    });
}


</script>