<?php 
$cRole = MyFormat::getCurrentRoleId();
$aRolePlan = [ROLE_DIRECTOR];
$mMonitorUpdate = new MonitorUpdate();
$js_array_employee = json_encode($mMonitorUpdate->getMapEmployee());

if(in_array($cRole, $aRolePlan)){
    include 'gPlacePlan.php';
}else{
    include 'gPlace.php';
}

?>