<div class="pac-card" id="pac-card">
  <div>
    <div id="title">
      Tìm địa chỉ khách hàng
    </div>
    <div id="type-selector" class="pac-controls">
        <input class="display_none" type="radio" name="type" id="changetype-all" checked="checked">
        <label class="display_none" for="changetype-all">All</label>

        <span style="display: none;">
            <input type="radio" name="type" id="changetype-establishment">
            <label for="changetype-establishment">Establishments</label>

            <input type="radio" name="type" id="changetype-address">
            <label for="changetype-address">Addresses</label>

            <input type="radio" name="type" id="changetype-geocode">
            <label for="changetype-geocode">Geocodes</label>
        </span>
    </div>
    <div id="strict-bounds-selector" class="pac-controls" style="display: none;">
      <input type="checkbox" id="use-strict-bounds" value="">
      <label for="use-strict-bounds">Strict Bounds</label>
    </div>
  </div>
  <div id="pac-container">
    <input id="pac-input" type="text"
        placeholder="Nhập tên đường, phường xã, quận huyện">
  </div>
</div>
<div id="map"></div>
<div id="infowindow-content">
  <img src="" width="16" height="16" id="place-icon">
  <span id="place-name"  class="title"></span><br>
  <span id="place-address"></span>
</div>

<?php
/** for detail =>  https://developers.google.com/maps/documentation/javascript/examples/places-autocomplete
 *  https://developers.google.com/places/web-service/usage?hl=en_US
 */
    $mGateApi = new GateApi();
    $js_array = $mGateApi->getLocationAgent(['GetLogo24h'=>1]);
?>
    <script>
      // This example requires the Places library. Include the libraries=places
      // parameter when you first load the API. For example:
      // <script src="https://maps.googleapis.com/maps/api/js?key=YOUR_API_KEY&libraries=places">

      function initMap() {
        var locations       = <?php echo $js_array;?>;
        var locationsUser   = <?php echo $js_array_employee;?>;
          
        var map = new google.maps.Map(document.getElementById('map'), {
          center: {lat: 10.778171, lng: 106.696266},
          zoom: 12
        });
        var card = document.getElementById('pac-card');
        var input = document.getElementById('pac-input');
        var types = document.getElementById('type-selector');
        var strictBounds = document.getElementById('strict-bounds-selector');

        map.controls[google.maps.ControlPosition.TOP_RIGHT].push(card);

        var autocomplete = new google.maps.places.Autocomplete(input);

        // Bind the map's bounds (viewport) property to the autocomplete object,
        // so that the autocomplete requests use the current map bounds for the
        // bounds option in the request.
        autocomplete.bindTo('bounds', map);

        var infowindow = new google.maps.InfoWindow();
        var infowindowContent = document.getElementById('infowindow-content');
        infowindow.setContent(infowindowContent);
        
        // Apr 24, 2017 for agent spj icon
        for (i = 0; i < locations.length; i++) {
            marker = new google.maps.Marker({
              position: new google.maps.LatLng(locations[i][1], locations[i][2]),
              map: map,
              label: locations[i][7], // agent name
              icon: locations[i][4]
            });

            google.maps.event.addListener(marker, 'click', (function(marker, i) {
              return function() {
                infowindow.setContent(locations[i][0]);
                infowindow.open(map, marker);
              }
            })(marker, i));
            
            // Add the circle for this city to the map.
          var cityCircle = new google.maps.Circle({
            strokeColor: '#81D4FA',
            strokeOpacity: 0.8,
            strokeWeight: 2,
            fillColor: '#B3E5FC',
            fillOpacity: 0.35,
            map: map,
            center: new google.maps.LatLng(locations[i][1], locations[i][2]),
            radius: locations[i][5] // in meters
          });
          
          if(locations[i][6] == 132678){// Xưởng Đồng Nai - Đại Lý
              var cityCircle = new google.maps.Circle({
                strokeColor: '#81D4FA',
                strokeOpacity: 0.8,
                strokeWeight: 2,
                fillColor: '#B3E5FC',
                fillOpacity: 0.25,
                map: map,
                center: new google.maps.LatLng(locations[i][1], locations[i][2]),
                radius: 20000 // 20 Km
              });
              var cityCircle = new google.maps.Circle({
                strokeColor: '#81D4FA',
                strokeOpacity: 0.8,
                strokeWeight: 2,
                fillColor: '#B3E5FC',
                fillOpacity: 0.25,
                map: map,
                center: new google.maps.LatLng(locations[i][1], locations[i][2]),
                radius: 40000 // 50 Km
              });
              var cityCircle = new google.maps.Circle({
                strokeColor: '#81D4FA',
                strokeOpacity: 0.8,
                strokeWeight: 2,
                fillColor: '#B3E5FC',
                fillOpacity: 0.25,
                map: map,
                center: new google.maps.LatLng(locations[i][1], locations[i][2]),
                radius: 60000 // 60 Km
              });
          }
            
        }
        // locations[i][6] // is agent id
        // Apr 24, 2017 for agent spj icon
        
        // May 19, 2017 for user GN location
        for (i = 0; i < locationsUser.length; i++) {
            marker = new google.maps.Marker({
              position: new google.maps.LatLng(locationsUser[i][1], locationsUser[i][2]),
              map: map,
              icon: locationsUser[i][4]
            });

            google.maps.event.addListener(marker, 'click', (function(marker, i) {
              return function() {
                infowindow.setContent(locationsUser[i][0]);
                infowindow.open(map, marker);
              }
            })(marker, i));
        }
        // May 19, 2017 for user GN location
        
        var marker = new google.maps.Marker({
          map: map,
          anchorPoint: new google.maps.Point(0, -29)
        });

        autocomplete.addListener('place_changed', function() {
          infowindow.close();
          marker.setVisible(false);
          var place = autocomplete.getPlace();
          if (!place.geometry) {
            // User entered the name of a Place that was not suggested and
            // pressed the Enter key, or the Place Details request failed.
            window.alert("No details available for input: '" + place.name + "'");
            return;
          }

          // If the place has a geometry, then present it on a map.
          if (place.geometry.viewport) {
            map.fitBounds(place.geometry.viewport);
            map.setZoom(16);  // để 12 để nhìn thấy nhiều đại lý xung quang vị trí search
          } else {
            map.setCenter(place.geometry.location);
            map.setZoom(16);  // để 12 để nhìn thấy nhiều đại lý xung quang vị trí search
          }
          marker.setPosition(place.geometry.location);
          marker.setVisible(true);

          var address = '';
          if (place.address_components) {
            address = [
              (place.address_components[0] && place.address_components[0].short_name || ''),
              (place.address_components[1] && place.address_components[1].short_name || ''),
              (place.address_components[2] && place.address_components[2].short_name || '')
            ].join(' ');
          }

          infowindowContent.children['place-icon'].src = place.icon;
          infowindowContent.children['place-name'].textContent = place.name;
          infowindowContent.children['place-address'].textContent = address;
          infowindow.open(map, marker);
        });

        // Sets a listener on a radio button to change the filter type on Places
        // Autocomplete.
        function setupClickListener(id, types) {
          var radioButton = document.getElementById(id);
          radioButton.addEventListener('click', function() {
            autocomplete.setTypes(types);
          });
        }

        setupClickListener('changetype-all', []);
        setupClickListener('changetype-address', ['address']);
        setupClickListener('changetype-establishment', ['establishment']);
        setupClickListener('changetype-geocode', ['geocode']);

        document.getElementById('use-strict-bounds')
            .addEventListener('click', function() {
              console.log('Checkbox clicked! New state=' + this.checked);
              autocomplete.setOptions({strictBounds: this.checked});
            });
            
            
            
      }
    </script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCfsbfnHgTh4ODoXLXFfEFhHskDhnRVtjQ&libraries=places&callback=initMap"
        async defer></script>
<style>
    /* Always set the map height explicitly to define the size of the div
     * element that contains the map. */
    #map {
      height: 100%;
    }
    /* Optional: Makes the sample page fill the window. */
    html, body {
      height: 100%;
      margin: 0;
      padding: 0;
    }
    #description {
      font-family: Roboto;
      font-size: 15px;
      font-weight: 300;
    }

    #infowindow-content .title {
      font-weight: bold;
    }

    #infowindow-content {
      display: none;
    }

    #map #infowindow-content {
      display: inline;
    }

    .pac-card {
      margin: 10px 10px 0 0;
      border-radius: 2px 0 0 2px;
      box-sizing: border-box;
      -moz-box-sizing: border-box;
      outline: none;
      box-shadow: 0 2px 6px rgba(0, 0, 0, 0.3);
      background-color: #fff;
      font-family: Roboto;
    }

    #pac-container {
      padding-bottom: 12px;
      margin-right: 12px;
    }

    .pac-controls {
      display: inline-block;
      padding: 5px 11px;
    }

    .pac-controls label {
      font-family: Roboto;
      font-size: 13px;
      font-weight: 300;
    }

    #pac-input {
      background-color: #fff;
      font-family: Roboto;
      font-size: 15px;
      font-weight: 300;
      margin-left: 12px;
      padding: 0 11px 0 13px;
      text-overflow: ellipsis;
      width: 400px;
    }

    #pac-input:focus {
      border-color: #4d90fe;
    }

    #title {
      color: #fff;
      background-color: #4d90fe;
      font-size: 25px;
      font-weight: 500;
      padding: 6px 12px;
    }
  </style>