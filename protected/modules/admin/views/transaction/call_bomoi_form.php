<div class="item-ipt-cart WrapOrderType">
<?php  echo CHtml::radioButtonList('OrderBoMoiType'.$i, GasAppOrder::DELIVERY_NOW,
        $mAppOrder->getArrayTypeOrder(),
        array('separator'=>'  ',
               'template'=>'<span class="item">{input} {label}</span>',
               'class'=> 'OrderBoMoiType',
              )
       );
?>
    <div class="float_r ">
    <?php 
    $this->widget('zii.widgets.jui.CJuiDatePicker', array(
        'model'=>$mAppOrder,        
        'attribute'=>'date_delivery'.$i,
//        'language'=>'en-GB',
        'options'=>array(
            'showAnim'=>'fold',
            'dateFormat'=> MyFormat::$dateFormatSearch,
            'minDate'=> '-1',
            'maxDate'=> '20',
            'showOn' => 'button',
            'buttonImage'=> Yii::app()->theme->baseUrl.'/admin/images/icon_calendar_r.gif',
            'buttonImageOnly'=> true,                                
        ),        
        'htmlOptions'=>array(
            'class'=>'f_size_17 w-100 h_30 OrderBoMoiDate',
            'readonly'=>1,
            'value'=> date('d-m-Y'),
        ),
    ));
    ?> 
    </div>
</div>
<div class="item-ipt-cart WrapAgentSearch form">
    <div class="AutoSearchAgent">
        <input class="call-iptclass OrderBoMoiAgentName " placeholder="Nhập tên đại lý" type="text" style="">
        <?php include 'call_bomoi_delivery_timer.php'; ?>
    </div>
    <div class="ChkPayDirect">
        <input id='ChkPay<?php echo $i;?>' class="OrderBoMoiPayDirect float_l" type="checkbox" style="margin-bottom: 0;"> 
        <label for="ChkPay<?php echo $i;?>" class="item_b">Thu tiền liền</label>
    </div>
    
    <span class="remove_row_item" onclick="fnRemoveNameAgent(this)"></span>
    <input class="OrderBoMoiAgentId" type="hidden">
</div>
<div class="item-ipt-cart WrapDieuXe display_none">
    <!--<input class="call-iptclass OrderBoMoiDieuXe">-->
    <?php $mAppCache = new AppCache(); ?>
    <?php echo CHtml::dropDownList('listname', 0, 
              $mAppCache->getListdataUserByRole(ROLE_SCHEDULE_CAR), ['class'=>'OrderBoMoiDieuXeId h_30','style'=>'width: 100%;', 'empty'=>'Chọn điều xe']);
    ?>
</div>

<div class="item-ipt-cart ">
    <input class="call-iptclass OrderBoMoiNote " placeholder="Ghi chú" type="text" style="margin-bottom: 0;">
</div>
<div class="item-ipt-cart DisplayError"></div>
    
<div class="item-ipt-cart item_c" style="margin-top: 0;">
    <button class="call-btn-update OrderBoMoiSubmit">Tạo đơn hàng</button>
    &nbsp;&nbsp;&nbsp;<a class="CallCreateUpholdBoMoiUrl" href="<?php echo $linkCreateUpholdBoMoi;?>" NormalLink="<?php echo $linkCreateUpholdBoMoi;?>" target="_blank"><button class="call-btn-update">Bảo Trì BM</button></a>
</div>