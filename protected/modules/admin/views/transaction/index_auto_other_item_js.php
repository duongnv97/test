<!-- Sep 02, 2019 for Bo Moi action search other item -->
<script>
//var availableMaterialsOther = <?php // echo MyFunctionCustom::getMaterialsJson(array('MaterialsTypeAddIn' => $mMaterialsType->getTypeCallCenterMakeOrderBoMoi())); ?>;
/** @Author: DungNT Sep 02, 2019
*  @Todo: bind js for autocomplete bò mối - chọn vật tư khác: Cồn, bếp, van, dây
*/
function bindBoMoiAutocompleteOtherItem(){
//    fnRefreshOrderNumber();
    fnBindRemoveIcon();
    $('.OrderBoMoiOtherItem').each(function(){
        regAutocompleteOtherItem($(this));
    });
}    

function regAutocompleteOtherItem(objInput) {
    var WrapOneTab = objInput.closest('.WrapOneTab');
    objInput.autocomplete({
        source: availableMaterialsOther,
        close: function( event, ui ) {
            objInput.val('');// chỗ này search sai bị xóa thông tin search
        },
        select: function( event, ui ) {
            var class_item = 'materials_row_'+ui.item.id;
            if(WrapOneTab.find('.'+class_item).size()<1)
                fnBuildRowOtherItem(WrapOneTab, event, ui);
        }
    });
}

function fnBuildRowOtherItem(WrapOneTab, event, ui){
    var class_item = 'materials_row_'+ui.item.id;
    var _tr = '';
        _tr += '<tr class="">';
            _tr += '<td class="item_c order_no"></td>';
            _tr += '<td class="item_l">'+ui.item.name+'</td>';
            _tr += '<td class="item_c">';
                _tr += '<input name="other_materials_qty[]" value="1" class="other_materials_qty w-50 item_c number_only_v1 f_size_15 '+class_item+'" type="text" maxlength="5">';
                _tr += '<input name="other_materials_id[]" value="'+ui.item.id+'" class="other_materials_id" type="hidden">';
            _tr += '</td>';
            _tr += '<td class="item_c">'+ui.item.unit+'</td>';
            _tr += '<td class="item_c last"><span class="remove_icon_only"></span></td>';
        _tr += '</tr>';
    WrapOneTab.find('.materials_table tbody').append(_tr);
    fnRefreshOrderNumber();
}


</script>

<!-- Sep 02, 2019 for Bo Moi action search other item -->