<?php 
Yii::app()->clientScript->registerScript('search-detail', "
$('.search-detail-form form').submit(function(){
	$.fn.yiiGridView.update('detail-grid', {
                url : $(this).attr('action'),
		data: $(this).serialize()
	});
	return false;
});
");

Yii::app()->clientScript->registerScript('ajaxupdate-detail', "
$('#detail-grid a.ajaxupdate').live('click', function() {
    $.fn.yiiGridView.update('detail-grid', {
        type: 'POST',
        url: $(this).attr('href'),
        success: function() {
            $.fn.yiiGridView.update('detail-grid');
        }
    });
    return false;
});
");
?>

<div class="search-detail-form">
<?php $this->renderPartial('_search_detail',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'detail-grid',
	'dataProvider'=>$model->searchDetail(),
        'afterAjaxUpdate'=>'function(id, data){ fnUpdateColorbox();}',
        'template'=>'{pager}{summary}{items}{pager}{summary}',
//    'itemsCssClass' => 'items custom_here',
//    'htmlOptions'=> array('class'=>'grid-view custom_here'),
        'pager' => array(
            'maxButtonCount'=>  CmsFormatter::$PAGE_MAX_BUTTON,
        ),
	'enableSorting' => false,
	//'filter'=>$model,
	'columns'=>array(
        array(
            'header' => 'S/N',
            'type' => 'raw',
            'value' => '$this->grid->dataProvider->pagination->currentPage * $this->grid->dataProvider->pagination->pageSize + ($row+1)',
            'headerHtmlOptions' => array('width' => '10px','style' => 'text-align:center;'),
            'htmlOptions' => array('style' => 'text-align:center;')
        ),
                
                array(
                    'name' => 'users_id',
                    'type'=>'raw',
                    'value'=>'$data->getEmployee("code_bussiness")." - ".$data->getEmployee()."<br>".$data->getEmployee("address")',
                ),
                array(
                    'name' => 'type',
                    'type'=>'raw',
                    'value'=>'$data->getType()',
                    //function of tbl Users
                ), 
                array(
                    'name' => 'file_name',
                    'type'=>'raw',
                    'value'=>'$data->getImages()',
                    //function of tbl Users
                    'htmlOptions' => array('style' => 'text-align:center;')
                ),
                array(
                    'name' => 'created_by',
                    'type'=>'raw',
                    'value'=>'$data->getCreatedBy("code_bussiness")." - ".$data->getCreatedBy()',
                ),
		array(
                    'name'=>'created_date',
                    'type'=>'raw',
                    'value'=>'$data->getCreatedDate()'
                ),
		array(
                    'header' => 'Actions',
                    'class'=>'CButtonColumn',
                    'template'=> ControllerActionsName::createIndexButtonRoles($actions),
                    'buttons'=>array(
                        'update' => array(
                            'visible' => '$data->canUpdate()',
                        ),
                        'delete'=>array(
                            'visible'=> 'GasCheck::canDeleteData($data)',
                        ),
                    ),
		),
	),
)); ?>