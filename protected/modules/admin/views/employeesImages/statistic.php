<?php 
Yii::app()->clientScript->registerScript('search-statistic', "
$('.search-statistic-form form').submit(function(){
    $.fn.yiiGridView.update('statistic-grid', {
            url : $(this).attr('action'),
            data: $(this).serialize()
    });
    return false;
});
");

Yii::app()->clientScript->registerScript('ajaxupdate-statistic', "
$('#statistic-grid a.ajaxupdate').live('click', function() {
    $.fn.yiiGridView.update('statistic-grid', {
        type: 'POST',
        url: $(this).attr('href'),
        success: function() {
            $.fn.yiiGridView.update('statistic-grid');
        }
    });
    return false;
});
");
?>

<div class="search-statistic-form">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'statistic-grid',
	'dataProvider'=>$model->search(),
        'afterAjaxUpdate'=>'function(id, data){ fnUpdateColorbox();}',
        'template'=>'{pager}{summary}{items}{pager}{summary}',
        'pager' => array(
            'maxButtonCount'=>  CmsFormatter::$PAGE_MAX_BUTTON,
        ),
	'enableSorting' => false,
	'columns'=>array(
        array(
            'header' => 'S/N',
            'type' => 'raw',
            'value' => '$this->grid->dataProvider->pagination->currentPage * $this->grid->dataProvider->pagination->pageSize + ($row+1)',
            'headerHtmlOptions' => array('width' => '10px','style' => 'text-align:center;'),
            'htmlOptions' => array('style' => 'text-align:center;')
        ),
                
                array(
                    'name' => 'users_id',
                    'type'=>'raw',
                    'value'=>'$data->getEmployee("code_bussiness")." - ".$data->getEmployee()',
                ),
//                array(
//                    'name' => 'type',
//                    'type'=>'raw',
//                    'value'=>'$data->getType()',
//                ), 
                array(
                    'name' => 'sale_id',
                    'type'=>'raw',
                    'value'=>'$data->getSale()',
                ), 
                array(
                    'name' => 'file_name',
                    'type'=>'raw',
                    'value'=>'$data->getMultiImages()',
                    'htmlOptions' => array('style' => 'text-align:center;')
                ),
//                array(
//                    'name' => 'created_by',
//                    'type'=>'raw',
//                    'value'=>'$data->getCreatedBy("code_bussiness")." - ".$data->getCreatedBy()',
//                ),
//		array(
//                    'name'=>'created_date',
//                    'type'=>'raw',
//                    'value'=>'$data->getCreatedDate()'
//                ),
//		array(
//                    'header' => 'Actions',
//                    'class'=>'CButtonColumn',
//                    'template'=> ControllerActionsName::createIndexButtonRoles($actions),
//                    'buttons'=>array(
//                        'update' => array(
//                            'visible' => '$data->canUpdate()',
//                        ),
//                        'delete'=>array(
//                            'visible'=> 'GasCheck::canDeleteData($data)',
//                        ),
//                    ),
//		),
	),
)); ?>