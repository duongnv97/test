<?php
$this->breadcrumbs=array(
    $this->pluralTitle => array('index'),
    ' ' . $this->singleTitle . ' : ',
);

$menus = array(
	array('label'=>"$this->pluralTitle Management", 'url'=>array('index')),
	array('label'=>"Tạo Mới $this->singleTitle", 'url'=>array('create')),
	array('label'=>"Cập Nhật $this->singleTitle", 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>"Xóa $this->singleTitle", 'url'=>array('delete'), 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Bạn chắc chắn muốn xóa ?')),
);
$this->menu= ControllerActionsName::createMenusRoles($menus, $actions);
?>

<h1>Xem: <?php echo $this->singleTitle; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
                array(
                    'name' => 'file_name',
                    'type' => 'raw',
                    'value' => $model->getImages(),
                ),
                array(
                    'name' => 'type',
                    'type' => 'raw',
                    'value' => $model->getType(),
                ),
                array(
                    'name' => 'users_id',
                    'type' => 'raw',
                    'value' => $model->getEmployee("code_bussiness")." - ".$model->getEmployee(),
                ),
                array(
                    'name' => 'created_by',
                    'type' => 'raw',
                    'value' => $model->getCreatedBy("code_bussiness")." - ".$model->getCreatedBy(),
                ),
                array(
                    'name' => 'created_date',
                    'type' => 'raw',
                    'value' => $model->getCreatedDate(),
                ),
		'update_date',
	),
)); ?>
