<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=> GasCheck::getCurl(),
	'method'=>'get',
)); ?>

<!--    <div class="row">
        <?php echo $form->labelEx($model,'type'); ?>
        <?php echo $form->dropDownList($model,'type', $model->getListTypeAll(),array('empty'=>'Select', 'class'=>'w-400 to_uid_approved',"class_update_val"=>'to_uid_approved')); ?>
    </div>-->
    <div class="row">
        <?php echo $form->labelEx($model,'users_id'); ?>
        <?php echo $form->hiddenField($model,'users_id_statistic'); ?>
        <?php 
            // widget auto complete search user customer and supplier
            $aData = array(
                'model'=>$model,
                'field_customer_id'=>'users_id_statistic',
                'name_relation_user'=>'rUsers',
                'field_autocomplete_name' => 'autocomplete_name_3',
                'url' => Yii::app()->createAbsoluteUrl('admin/ajax/search_user_by_code'),
                'ClassAdd' => 'w-400',
            );
            $this->widget('ext.SpaAutocompleteCustomer.SpaAutocompleteCustomer',
                array('data'=>$aData));                                        
        ?>
    </div>
    <div class="row">
        <?php echo $form->labelEx($model,'employee_id'); ?>
        <?php echo $form->hiddenField($model,'employee_id_statistic'); ?>
        <?php 
            // widget auto complete search user customer and supplier
            $url = Yii::app()->createAbsoluteUrl('admin/ajax/search_by_role', array('GetAll'=>1, 'role'=> implode(",", GasUphold::$ROLE_EMPLOYEE_SEARCH_AUTOCOMPLETE)));

            $aData = array(
                'model'=>$model,
                'field_customer_id'=>'employee_id_statistic',
                'name_relation_user'=>'rEmployee',
                'field_autocomplete_name' => 'autocomplete_name_4',
                'url' => $url,
                'ClassAdd' => 'w-400',
            );
            $this->widget('ext.SpaAutocompleteCustomer.SpaAutocompleteCustomer',
                array('data'=>$aData));                                        
        ?>
    </div>

    <div class="row buttons" style="padding-left: 159px;">
    <?php $this->widget('bootstrap.widgets.TbButton', array(
        'buttonType'=>'submit',
        'label'=>'Search',
        'type'=>'null', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
        'size'=>'small', // null, 'large', 'small' or 'mini'
        //'htmlOptions' => array('style' => 'margin-bottom: 10px; float: right;'),
    )); ?>
    </div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->