<?php
$this->breadcrumbs=array(
    $this->pluralTitle => array('index'),
    ' ' . $this->singleTitle . ' : ',
);

$menus = array(
	array('label'=>"$this->pluralTitle Management", 'url'=>array('index')),
	array('label'=>"Tạo Mới $this->singleTitle", 'url'=>array('create')),
	array('label'=>"Cập Nhật $this->singleTitle", 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>"Xóa $this->singleTitle", 'url'=>array('delete'), 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Bạn chắc chắn muốn xóa ?')),
);
$this->menu= ControllerActionsName::createMenusRoles($menus, $actions);
?>

<h1>Xem: <?php echo $this->singleTitle; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'contract_number',
            array(
                    'name'=>'sign_date',
                    'value'=>$model->getSignDate(),
                ),
            array(
                    'name'=>'customer_id',
                    'value'=>$model->getCustomerName(),
                ),
		'description',[
                    'name'=>'contract_value',
                    'value'=>$model->getContractValue(),
                ],
		'bank_account_number',
		'bank',
		'bank_branch',
            array(
                    'name'=>'province_id',
                    'value'=>$model->getProvince(),
                ),
		'created_date',
             array(
                    'name'=>'created_by',
                    'value'=>$model->getUidLogin(),
                ),
	),
)); ?>
