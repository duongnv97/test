<?php
$this->breadcrumbs=array(
	$this->singleTitle,
);

$menus=array(
            array('label'=>"Create $this->singleTitle", 'url'=>array('create')),
);
$this->menu= ControllerActionsName::createMenusRoles($menus, $actions);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('contract-list-management-grid', {
                url : $(this).attr('action'),
		data: $(this).serialize()
	});
	return false;
});
");

Yii::app()->clientScript->registerScript('ajaxupdate', "
$('#contract-list-management-grid a.ajaxupdate').live('click', function() {
    $.fn.yiiGridView.update('contract-list-management-grid', {
        type: 'POST',
        url: $(this).attr('href'),
        success: function() {
            $.fn.yiiGridView.update('contract-list-management-grid');
        }
    });
    return false;
});
");
?>

<h1>Danh Sách <?php echo $this->pluralTitle; ?></h1>


<?php echo CHtml::link('Tìm Kiếm Nâng Cao','#',array('class'=>'search-button')); ?>
<div class="search-form">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'contract-list-management-grid',
	'dataProvider'=>$model->search(),
        'afterAjaxUpdate'=>'function(id, data){ fnUpdateColorbox();}',
        'template'=>'{pager}{summary}{items}{pager}{summary}',
//    'itemsCssClass' => 'items custom_here',
//    'htmlOptions'=> array('class'=>'grid-view custom_here'),
        'pager' => array(
            'maxButtonCount'=>  CmsFormatter::$PAGE_MAX_BUTTON,
        ),
	'enableSorting' => false,
	//'filter'=>$model,
	'columns'=>array(
        array(
            'header' => 'S/N',
            'type' => 'raw',
            'value' => '$this->grid->dataProvider->pagination->currentPage * $this->grid->dataProvider->pagination->pageSize + ($row+1)',
            'headerHtmlOptions' => array('width' => '10px','style' => 'text-align:center;'),
            'htmlOptions' => array('style' => 'text-align:center;')
        ),
            [
                'name'=>'type',
                'value'=>'$data->getType()',
                'htmlOptions' => array('width' => '50px','style' => 'text-align:center;')
            ],
            [
                'name'=>'contract_number',
                'value'=>'$data->getContractNumber()',
                'htmlOptions' => array('width' => '50px','style' => 'text-align:center;')
            ],
            [
                'name'=>'sign_date',
                'value'=>'$data->getSignDate()',
                'htmlOptions' => array('style' => 'text-align:center;')
            ],
            [
                'name'=>'customer_id',
                'value'=>'$data->getCustomerNameText()',
                'htmlOptions' => array('width' => '100px')

            ],
            [
                'name'=>'description',
                'value'=>'$data->getDescription()',
                'htmlOptions' => array('width' => '200px')

            ],
            [
                'name'=>'contract_value',
                'value'=>'$data->getContractValue()',
                'htmlOptions' => array('width' => '100px','style' => 'text-align:right;')
            ],
            [
                'name'=>'Giá trị thực hiện',
                'value'=>'$data->getTotalCompleteMoney()',
                'htmlOptions' => array('width' => '100px','style' => 'text-align:right;')
            ],
		'bank_account_number',
		'bank',
		'bank_branch',
            [
                'name'=>'province_id',
                'value'=>'$data->getProvince()',
                'htmlOptions' => array('width' => '80px')
            ],
            array(
                'name'=>'status',
                'value'=>'$data->getStatus()',
                'htmlOptions' => array('style' => 'text-align:center;')
            ),
//		'created_date',
//		'created_by',
		array(
                    'header' => 'Actions',
                    'class'=>'CButtonColumn',
                    'template'=> ControllerActionsName::createIndexButtonRoles($actions),
                    'buttons'=>array(
                        'update' => array(
                            'visible' => '$data->canUpdate()',
                        ),
                        'delete'=>array(
                            'visible'=> 'GasCheck::canDeleteData($data)',
                        ),
                    ),
		),
	),
)); ?>

<script>
$(document).ready(function() {
    fnUpdateColorbox();
});

function fnUpdateColorbox(){   
    fixTargetBlank();
}
</script>