<div class="wide form">

    <?php
    $form = $this->beginWidget('CActiveForm', array(
        'action'=> Yii::app()->createUrl($this->route),
        'method' => 'get',
    ));
    ?>

    <div class="row more_col">
        <div class="col1">
            <?php echo $form->label($model, 'contract_number', array()); ?>
            <?php echo $form->textField($model, 'contract_number', array('class' => 'w-300', 'maxlength' => 30)); ?>
        </div>
        <div class="col2">
            <?php echo $form->labelEx($model,'type'); ?>
            <?php echo $form->dropDownList($model,'type', $model->getArrayType(),array('class'=>'','empty'=>'Select')); ?>
            <?php echo $form->error($model,'type'); ?>
        </div>
        <div class="col3">
            <?php echo $form->label($model, 'sign_date'); ?>
            <?php
            $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                'model' => $model,
                'attribute' => 'sign_date',
                'options' => array(
                    'showAnim' => 'fold',
                    'dateFormat' => MyFormat::$dateFormatSearch,
//                            'minDate'=> '0',
//                        'maxDate'=> '0',
                    'changeMonth' => true,
                    'changeYear' => true,
                    'showOn' => 'button',
                    'buttonImage' => Yii::app()->theme->baseUrl . '/admin/images/icon_calendar_r.gif',
                    'buttonImageOnly' => true,
                ),
                'htmlOptions' => array(
                    'class' => 'w-16',
                    'size' => '16',
                    'style' => 'float:left;',
                ),
            ));
            ?>
        </div>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model,'customer_id'); ?>
        <?php echo $form->hiddenField($model,'customer_id', array('class'=>'')); ?>
        <?php
                // 1. limit search kh của sale
                $url = Yii::app()->createAbsoluteUrl('admin/ajax/searchHgd');
                // widget auto complete search user customer and supplier
                $aData = array(
                    'model'=>$model,
                    'field_customer_id'=>'customer_id',
                    'url'=> $url,
                    'name_relation_user'=>'rCustomer',
                    'ClassAdd' => 'w-400',
      //              'fnSelectCustomer'=>'fnSelectCustomer',
                );
                $this->widget('ext.SpaAutocompleteCustomer.SpaAutocompleteCustomer',
                    array('data'=>$aData));
                ?>
            <?php echo $form->error($model,'customer_id'); ?>
    </div>
    
    <div class="row">
        <?php echo $form->labelEx($model,'province_id'); ?>
        <div class="fix-label">
            <?php
               $this->widget('ext.multiselect.JMultiSelect',array(
                     'model'=>$model,
                     'attribute'=>'province_id',
                     'data'=> GasProvince::getArrAll(),
                     // additional javascript options for the MultiSelect plugin
                    'options'=>array('selectedList' => 30,),
                     // additional style
                     'htmlOptions'=>array('style' => 'width: 800px;'),
               ));    
           ?>
        </div>
    </div>
    <div class="row buttons" style="padding-left: 159px;">
        <?php
        $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType' => 'submit',
            'label' => 'Search',
            'type' => 'null', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
            'size' => 'small', // null, 'large', 'small' or 'mini'
                //'htmlOptions' => array('style' => 'margin-bottom: 10px; float: right;'),
        ));
        ?>	</div>

    <?php $this->endWidget(); ?>

</div><!-- search-form -->