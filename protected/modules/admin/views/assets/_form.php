<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'assets-form',
	'enableAjaxValidation'=>false,
        'htmlOptions' => array('enctype' => 'multipart/form-data'),
)); ?>

    <p class="note">Dòng có dấu <span class="required">*</span> là bắt buộc.</p>
    <?php echo MyFormat::BindNotifyMsg(); ?>
    
    <div class="row">
        <?php echo $form->labelEx($model,'first_name'); ?>
        <?php echo $form->textField($model,'first_name',array('class'=>'w-500')); ?>
        <?php echo $form->error($model,'first_name'); ?>
    </div>
    
    <div class="row">
        <?php echo $form->labelEx($model,'code_bussiness'); ?>
        <?php echo $form->textField($model,'code_bussiness',array('class'=>'w-300')); ?>
        <?php echo $form->error($model,'code_bussiness'); ?>
    </div>
    
    <div class="row">
        <?php echo $form->labelEx($model,'last_name'); ?>
        <?php echo $form->textField($model,'last_name',array('class'=>'w-300 w-150 f_size_15 ad_fix_currency item_r')); ?>
        <?php echo $form->error($model,'last_name'); ?>
    </div>
    
    <div class="row">
        <?php echo $form->labelEx($model,'province_id'); ?>
        <?php echo $form->dropdownList($model,'province_id', GasProvince::getArrAll(), array('class'=>'w-300','empty'=>'Select')); ?>
        <?php echo $form->error($model,'province_id'); ?>
    </div>
    
    <div class="row">
            <?php echo $form->labelEx($model,'district_id'); ?>
            <?php echo $form->dropDownList($model,'district_id', GasDistrict::getArrAll($model->province_id),array('class'=>'w-300','empty'=>'Select')); ?>
            <?php echo $form->error($model,'district_id'); ?>
    </div>

    <div class="row">
            <?php echo $form->labelEx($model,'ward_id'); ?>
            <?php echo $form->dropDownList($model,'ward_id', GasWard::getArrAll($model->province_id, $model->district_id),array('class'=>'w-300','empty'=>'Select')); ?>                
            <?php echo $form->error($model,'ward_id'); ?>
    </div>	

    <div class="row">
            <?php echo $form->labelEx($model,'house_numbers') ?>
            <?php echo $form->textField($model,'house_numbers', array('class'=>'w-300')); ?>
            <?php echo $form->error($model,'house_numbers'); ?>
    </div> 	

    <div class="row">
        <?php echo $form->labelEx($model,'street_id') ?>
        <?php echo $form->hiddenField($model,'street_id',array('class'=>'w-300')); ?>
        <?php 
            $this->widget('zii.widgets.jui.CJuiAutoComplete', array(
                    'attribute'=>'autocomplete_name_street',
                    'model'=>$model,
                    'sourceUrl'=>Yii::app()->createAbsoluteUrl('admin/ajax/autocomplete_data_streets'),
                    'options'=>array(
                            'minLength'=>MIN_LENGTH_AUTOCOMPLETE,
                            'multiple'=> true,
                            'search'=>"js:function( event, ui ) {
                                    $('#Users_autocomplete_name_street').addClass('grid-view-loading-gas');
                                    } ",
                            'response'=>"js:function( event, ui ) {
                                        var json = $.map(ui, function (value, key) { return value; });
                                        if(json.length<1){
                                            var error = '<div class=\'errorMessage clr autocomplete_name_text\'>Không tìm thấy dữ liệu.</div>';
                                            if($('.autocomplete_name_text').size()<1)
                                                    $('.autocomplete_name_error').parent('div').find('.add_new_item').after(error);
                                            else
                                                    $('.autocomplete_name_error').parent('div').find('.autocomplete_name_text').show();
                                            $('.remove_row_item').hide();
                                        }

                                    $('#Users_autocomplete_name_street').removeClass('grid-view-loading-gas');
                                    } ",
                            'select'=>"js:function(event, ui) {
                                    $('#Users_street_id').val(ui.item.id);
                                    var remove_div = '<span class=\'remove_row_item\' onclick=\'fnRemoveNameStreet(this)\'></span>';
                                    $('#Users_autocomplete_name_street').parent('div').find('.remove_row_item').remove();
                                    $('#Users_autocomplete_name_street').attr('readonly',true).after(remove_div);
                                                                            $('.autocomplete_name_error').parent('div').find('.autocomplete_name_text').hide();
                            }",
                    ),
                    'htmlOptions'=>array(
                        'class'=>'autocomplete_name_error',
                        'size'=>45,
                        'maxlength'=>45,
                        'style'=>'float:left;width:300px;',
                        'placeholder'=>'Nhập tên đường tiếng việt không dấu',                            
                    ),
            )); 
            ?> 
            <script>
                function fnRemoveNameStreet(this_){
                    $(this_).parent('div').find('input').attr("readonly",false); 
                    $("#Users_autocomplete_name_street").val("");
                    $("#Users_street_id").val("");
                }
            </script>             
            <div class="add_new_item"><a class="iframe_create_street" href="<?php echo Yii::app()->createAbsoluteUrl('admin/ajax/iframe_create_street') ;?>">Tạo Mới Đường</a><em> (Nếu trong danh sách không có)</em></div>
        <div class="clr"></div>
        <?php echo $form->error($model,'street_id'); ?>
    </div> 	    

    <div class="row">
        <?php echo $form->labelEx($model,'status'); ?>
        <?php echo $form->dropDownList($model,'status', Assets::getArrayStatus(), array('class'=>'w-300')); ?>
        <?php echo $form->error($model,'status'); ?>
    </div>
    
	<div class="row buttons" style="padding-left: 141px;">
            <?php $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType'=>'submit',
            'label'=>$model->isNewRecord ? 'Create' :'Save',
            'type'=>'null', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
            'size'=>'small', // null, 'large', 'small' or 'mini'
            //'htmlOptions' => array('style' => 'margin-bottom: 10px; float: right;'),
        )); ?>	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->

<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/autoNumeric-master/autoNumeric.js"></script>
<script>
    $(document).ready(function(){
        fnInitInputCurrency();
        colorboxCreateStreet();
        $('.form').find('button:submit').click(function(){
            $.blockUI({ overlayCSS: { backgroundColor: '#fff' } }); 
        });
        
    });
    
    function colorboxCreateStreet(){
        $(".iframe_create_street").colorbox({
            iframe:true,
            innerHeight:'500', 
            innerWidth: '1050',
            close: "<span title='close'>close</span>"
        });
    }
    
    $('#Assets_province_id').live('change',function(){
        var province_id = $(this).val();        
        var url_ = "<?php echo Yii::app()->createAbsoluteUrl('admin/ajax/get_slt_district');?>";
        $.blockUI({ overlayCSS: { backgroundColor: '<?php echo BLOCK_UI_COLOR;?>' } }); 
        $.ajax({
            url: url_,
            data: {ajax:1,province_id:province_id},
            type: "get",
            dataType:'json',
            success: function(data){
                $('#Assets_district_id').html(data['html_district']);                
                $.unblockUI();
            }
        });
    });
    
    
    $('#Assets_district_id').live('change',function(){
            var province_id = $('#Assets_province_id').val();   
            var district_id = $(this).val();        
            var url_ = "<?php echo Yii::app()->createAbsoluteUrl('admin/ajax/get_slt_ward');?>";
            $.blockUI({ overlayCSS: { backgroundColor: '<?php echo BLOCK_UI_COLOR;?>' } }); 
            $.ajax({
                    url: url_,
                    data: {ajax:1,district_id:district_id,province_id:province_id},
                    type: "get",
                    dataType:'json',
                    success: function(data){
                            $('#Assets_ward_id').html(data['html_district']);                
                            $.unblockUI();
                    }
            });

    });  
</script>