<?php
$this->breadcrumbs=array(
    $this->pluralTitle => array('index'),
    ' ' . $this->singleTitle . ' : ',
);

$menus = array(
	array('label'=>"$this->pluralTitle Management", 'url'=>array('index')),
	array('label'=>"Tạo Mới $this->singleTitle", 'url'=>array('create')),
	array('label'=>"Cập Nhật $this->singleTitle", 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>"Xóa $this->singleTitle", 'url'=>array('delete'), 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Bạn chắc chắn muốn xóa ?')),
);
$this->menu= ControllerActionsName::createMenusRoles($menus, $actions);
?>

<h1>Xem: <?php echo $this->singleTitle; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'first_name',
		'code_bussiness',
                array(
                    'name' => 'last_name',
                    'type' => 'raw',
                    'value' => $model->getAssetsValuation(),
                ),
                array(
                    'name' => 'province_id',
                    'type' => 'raw',
                    'value' => $model->getProvince(),
                ),
                array(
                    'name' => 'district_id',
                    'type' => 'raw',
                    'value' => $model->getDistrict(),
                ),
                array(
                    'name' => 'ward_id',
                    'type' => 'raw',
                    'value' => $model->getWard(),
                ),
                array(
                    'name' => 'street_id',
                    'type' => 'raw',
                    'value' => $model->getStreet(),
                ),
		'house_numbers',
                'address',
                array(
                    'name' => 'created_date',
                    'type' => 'raw',
                    'value' => $model->getCreatedDate(),
                ),
                array(
                    'name' => 'created_date',
                    'type' => 'raw',
                    'value' => $model->getCreatedBy(),
                ),
                array(
                    'name' => 'status',
                    'type' => 'raw',
                    'value' => $model->getStatus(),
                ),
	),
)); ?>
