<?php
$this->breadcrumbs=array(
	$this->pageTitle,
);

$menus=array(
        array('label'=>"Create $this->pageTitle", 'url'=>array('create')),
);
$this->menu= ControllerActionsName::createMenusRoles($menus, $actions);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('assets-grid', {
                url : $(this).attr('action'),
		data: $(this).serialize()
	});
	return false;
});
");

Yii::app()->clientScript->registerScript('ajaxupdate', "
$('#assets-grid a.ajaxupdate').live('click', function() {
    $.fn.yiiGridView.update('assets-grid', {
        type: 'POST',
        url: $(this).attr('href'),
        success: function() {
            $.fn.yiiGridView.update('assets-grid');
        }
    });
    return false;
});
");
?>

<h1>Danh Sách <?php echo $this->pageTitle; ?></h1>


<?php echo CHtml::link('Tìm Kiếm Nâng Cao','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'assets-grid',
	'dataProvider'=>$model->searchAssets(),
        'afterAjaxUpdate'=>'function(id, data){ fnUpdateColorbox();}',
        'template'=>'{pager}{summary}{items}{pager}{summary}',
//    'itemsCssClass' => 'items custom_here',
//    'htmlOptions'=> array('class'=>'grid-view custom_here'),
        'pager' => array(
            'maxButtonCount'=>  CmsFormatter::$PAGE_MAX_BUTTON,
        ),
	'enableSorting' => false,
	//'filter'=>$model,
	'columns'=>array(
        array(
            'header' => 'S/N',
            'type' => 'raw',
            'value' => '$this->grid->dataProvider->pagination->currentPage * $this->grid->dataProvider->pagination->pageSize + ($row+1)',
            'headerHtmlOptions' => array('width' => '10px','style' => 'text-align:center;'),
            'htmlOptions' => array('style' => 'text-align:center;')
        ),
		'first_name',
		'code_bussiness',
                array(
                    'name' => 'last_name',
                    'type' => 'raw',
                    'value' => '$data->getAssetsValuation()',
                    'htmlOptions' => array('class' => 'item_r')
                ),
		'address',
                array(
                    'name' => 'created_date',
                    'type' => 'raw',
                    'value' => '$data->getCreatedDate("d/m/Y")',
                    'htmlOptions' => array('class' => 'item_c')
                ),
                array(
                    'name' => 'status',
                    'type' => 'raw',
                    'value' => '$data->getStatus()',
                    'htmlOptions' => array('class' => 'item_c')
                ),
//                array(
//                    'name' => 'province_id',
//                    'type' => 'raw',
//                    'value' => '$data->getProvince()',
//                    'htmlOptions' => array('class' => 'item_c')
//                ),
//                array(
//                    'name' => 'district_id',
//                    'type' => 'raw',
//                    'value' => '$data->getDistrict()',
//                    'htmlOptions' => array('class' => 'item_c')
//                ),
//                array(
//                    'name' => 'ward_id',
//                    'type' => 'raw',
//                    'value' => '$data->getWard()',
//                    'htmlOptions' => array('class' => 'item_c')
//                ),
//                array(
//                    'name' => 'street_id',
//                    'type' => 'raw',
//                    'value' => '$data->getStreet()',
//                    'htmlOptions' => array('class' => 'item_c')
//                ),
		/*
		'name_agent',
		'code_account',
		'address_vi',
		'channel_id',
		'storehouse_id',
		'sale_id',
		'payment_day',
		'beginning',
		'first_char',
		'login_attemp',
		'created_date',
		'created_date_bigint',
		'created_time',
		'last_logged_in',
		'ip_address',
		'role_id',
		'application_id',
		'status',
		'gender',
		'phone',
		'verify_code',
		'area_code_id',
		'parent_id',
		'slug',
		'is_maintain',
		'type',
		'address_temp',
		'last_purchase',
		'created_by',
		'price',
		'price_other',
		'percent_target',
		*/
		array(
                    'header' => 'Actions',
                    'class'=>'CButtonColumn',
                    'template'=> ControllerActionsName::createIndexButtonRoles($actions),
                    'buttons'=>array(
                        'update' => array(
                            'visible' => '$data->canUpdate()',
                        ),
                        'delete'=>array(
                            'visible'=> 'GasCheck::canDeleteData($data)',
                        ),
                    ),
		),
	),
)); ?>

<script>
$(document).ready(function() {
    fnUpdateColorbox();
});

function fnUpdateColorbox(){   
    fixTargetBlank();
}
</script>