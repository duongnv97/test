<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=> GasCheck::getCurl(),
	'method'=>'get',
)); ?>

	<div class="row ">
            <?php echo $form->labelEx($model, 'Tên tài sản'); ?>
            <?php echo $form->hiddenField($model, 'id'); ?>
            <?php
            // 1. limit search kh của sale
            $url = Yii::app()->createAbsoluteUrl('admin/ajax/search_by_role', array('role' => ROLE_ASSETS));
            // widget auto complete search user customer and supplier
            $aData = array(
                'model' => $model,
                'field_customer_id' => 'id',
                'url' => $url,
                'name_relation_user' => 'rCustomer',
                'ClassAdd' => 'w-300',
                'placeholder' => 'Nhập mã hoặc tên tài sản',
            );
            $this->widget('ext.SpaAutocompleteCustomer.SpaAutocompleteCustomer', array('data' => $aData));
            ?>
        </div>
    
    <div class="row">
        <?php echo $form->labelEx($model, 'province_id'); ?>
        <div class="fix-label">
            <?php
            $this->widget('ext.multiselect.JMultiSelect', array(
                'model' => $model,
                'attribute' => 'province_id',
                'data' => GasProvince::getArrAll(),
                // additional javascript options for the MultiSelect plugin
                'options' => array('selectedList' => 30),
                // additional style
                'htmlOptions' => array('class' => 'w-300'),
            )); 
            ?>
        </div>
        <?php echo $form->error($model, 'province_id'); ?>
    </div>
    
    <div class="row more_col">
        <div class="col1">
            <?php echo $form->labelEx($model, 'date_from'); ?>
            <?php 
            $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                'model' => $model,

                'attribute' => 'date_from',
                'options' => array(
                    'showAnim' => 'fold',
                    'dateFormat' => MyFormat::$dateFormatSearch,
                    'changeMonth' => true,
                    'changeYear' => true,
                    'showOn' => 'button',
                    'buttonImage' => Yii::app()->theme->baseUrl . '/admin/images/icon_calendar_r.gif',
                    'buttonImageOnly' => true,
                ),
                'htmlOptions' => array(
                    'class' => 'w-16',
                    'style' => 'height:20px;',
                ),
            ));
            ?>  
        </div>
        <div class="col2">
            <?php echo $form->labelEx($model, 'date_to'); ?>
            <?php 
            $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                'model' => $model,

                'attribute' => 'date_to',
                'options' => array(
                    'showAnim' => 'fold',
                    'dateFormat' => MyFormat::$dateFormatSearch,
                    'changeMonth' => true,
                    'changeYear' => true,
                    'showOn' => 'button',
                    'buttonImage' => Yii::app()->theme->baseUrl . '/admin/images/icon_calendar_r.gif',
                    'buttonImageOnly' => true,
                ),
                'htmlOptions' => array(
                    'class' => 'w-16',
                    'style' => 'height:20px;',
                ),
            ));
            ?>  
        </div>
    </div>
    
    <div class="row">
        <?php echo $form->labelEx($model,'status'); ?>
        <?php echo $form->dropDownList($model,'status', Assets::getArrayStatus(), array('class'=>'w-300', 'empty'=>'Select')); ?>
    </div>

	<div class="row buttons" style="padding-left: 159px;">
            <?php $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType'=>'submit',
            'label'=>'Search',
            'type'=>'null', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
            'size'=>'small', // null, 'large', 'small' or 'mini'
            //'htmlOptions' => array('style' => 'margin-bottom: 10px; float: right;'),
        )); ?>	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->