<?php 
//get all work shift
$mWorkShift = new HrWorkShift();
$aWorkShift = $mWorkShift->getArrayWorkShift();

//get all work plan
$mWorkPlan = new HrWorkPlan();
$aWorkPlan = $mWorkPlan->getArrayWorkPlan();
?>
<div class="form">

<?php 

    $form=$this->beginWidget('CActiveForm', array(
            'id'=>'work-schedule-form',
            'enableAjaxValidation'=>false,
            'htmlOptions' => array('enctype' => 'multipart/form-data'),
    )); ?>

    <p class="note">Dòng có dấu <span class="required">*</span> là bắt buộc.</p>
    <?php echo MyFormat::BindNotifyMsg(); ?>

    <div class="row buttons" >
        <?php $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType'=>'submit',
            'label'=>$model->isNewRecord ? 'Create' :'Save',
            'type'=>'null', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
            'size'=>'small', // null, 'large', 'small' or 'mini'
            //'htmlOptions' => array('style' => 'margin-bottom: 10px; float: right;'),
        )); ?>	
    </div>
<!--    <div class="row">
        <?php //echo $form->labelEx($model,'role_id'); ?>
        <?php //echo $form->textField($model,'role_id'); ?>
        <?php //echo $form->error($model,'role_id'); ?>
    </div>-->
    <div class="row">
        <?php echo $form->labelEx($model,'work_day'); ?>
         <?php 
            $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                'model'=>$model,        
                'attribute'=>'work_day',
                'options'=>array(
                    'showAnim'=>'fold',
                    'dateFormat'=> MyFormat::$dateFormatSearch,
            //                            'minDate'=> '0',
            //                            'maxDate'=> '0',
                    'changeMonth' => true,
                    'changeYear' => true,
                    'showOn' => 'button',
                    'buttonImage'=> Yii::app()->theme->baseUrl.'/admin/images/icon_calendar_r.gif',
                    'buttonImageOnly'=> true,                                
                ),        
                'htmlOptions'=>array(
                    'class'=>'w-16',
                    'style'=>'height:20px;',
                    'readonly'=>'readonly',
                    'value'=> isset($_GET['date']) ? $_GET['date'] : '',
                ),
            ));
            ?>  
        <?php echo $form->error($model,'work_day'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model,'work_shift_id'); ?>
        <?php echo $form->dropDownList($model,'work_shift_id', $aWorkShift); ?>
        <?php echo $form->error($model,'work_shift_id'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model,'work_plan_id'); ?>
        <?php echo $form->dropDownList($model,'work_plan_id', $aWorkPlan); ?>
        <?php echo $form->error($model,'work_plan_id'); ?>
    </div>

    <div class="row ">
        <?php echo $form->labelEx($model,'employee_id'); ?>
        <?php echo $form->hiddenField($model,'employee_id'); ?>
        <?php
            // 1. limit search kh của sale
            $aRole = HrSalaryReports::model()->getArrayRoleSalary();
            $sRole = implode(',', array_keys($aRole));
            $url = Yii::app()->createAbsoluteUrl('admin/ajax/search_by_role', array('role' => $sRole));
            // widget auto complete search user customer and supplier
            $aData = array(
                'model'=>$model,
                'field_customer_id'=>'employee_id',
                'url'=> $url,
                'name_relation_user'=>'rEmployee',
                'ClassAdd' => 'w-300',
                'field_autocomplete_name' => 'autocomplete_employee', // tên biến truyền vào khai bao o model
                'placeholder'=>'Nhập mã hoặc tên nhân viên',
            );
            $this->widget('ext.SpaAutocompleteCustomer.SpaAutocompleteCustomer',
                array('data'=>$aData));
            ?>
        <?php echo $form->error($model,'employee_id'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model,'status'); ?>
        <?php echo $form->dropDownList($model,'status', $model->getArrayStatus()); ?>
        <?php echo $form->error($model,'status'); ?>
    </div>
    
    <div class="row buttons">
        <?php $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType'=>'submit',
            'label'=>$model->isNewRecord ? 'Create' :'Save',
            'type'=>'null', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
            'size'=>'small', // null, 'large', 'small' or 'mini'
            //'htmlOptions' => array('style' => 'margin-bottom: 10px; float: right;'),
        )); ?>	
    </div>

<?php $this->endWidget(); ?>
    
</div><!-- form -->
