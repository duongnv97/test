<?php 
Yii::app()->clientScript->registerScript('autogen', "
$('.autogen-button').click(function(){
	$('.autogen-form').toggle();
	return false;
});
");
?>
<?php if($model->canAutoSchedule()): ?>
<?php echo CHtml::link('Tạo lịch tự động','#',array('class'=>'autogen-button')); ?> 
<div class="autogen-form display_none">
    <!--<div class="row display_none">
        <?php // echo $form->labelEx($model,'approver'); ?>
        <?php // echo $form->dropDownList($model,'approver', HrSalaryReports::model()->getArrayApprover(), array('class' => 'w-190')); ?>
        <?php // echo $form->error($model,'approver'); ?>
    </div>-->

    <hr>
    <i>Thông tin dưới đây dùng để tạo lịch tự động</i>
        <div class="row">
            <?php echo $form->labelEx($model,'work_shift_id', ['label' => 'Số người mỗi ca']); ?>
            <!--Load ca lam viec bang ajax ( js function ajaxLoadShift, file workScheduleUI)-->
            <!--Do luc truoc load theo role, gio khong can nua-->
            <div id="HrWorkSchedule_work_shift_id" style="display: block; padding-left: 160px;"></div>
            <?php echo $form->error($model,'work_shift_id'); ?>
        </div>

        <div class="row">
            <label for="HrWorkSchedule_is_off_sunday">Nghỉ chủ nhật</label>
            <input name="ShiftAutoGenerate[is_off_sunday]" id="HrWorkSchedule_is_off_sunday" type="checkbox" value="1" style="margin-left: 79px;">
        </div>

        <div class="row buttons">
            <?php $this->widget('bootstrap.widgets.TbButton', array(
                'buttonType'=>'submit',
                'visible' => $model->canAutoSchedule(),
                'label'=>'Tự động tạo lịch',
                'type'=>'info', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
                'size'=>'small', // null, 'large', 'small' or 'mini'
                'htmlOptions' => array(
                        'style' => 'margin-left: 46px; margin-bottom: 20px;',
                        'name' => 'AutoGenerateSchedule',
                        'value' => '1',
                    ),
            )); ?>	

            <?php 
    //        $this->widget('bootstrap.widgets.TbButton', array(
    //            'buttonType'=>'submit',
    //            'label'=>'Gửi mail xét duyệt',
    //            'type'=>'info', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
    //            'size'=>'small', // null, 'large', 'small' or 'mini'
    //            'visible' => false,
    //            'htmlOptions' => array(
    //                    'style' => 'margin-left: 50px; margin-bottom: 20px;',
    //                    'name' => 'HrWorkSchedule[SendMail]',
    //                    'value' => '1',
    //                ),
    //        )); 
            ?>	
        </div>
</div>
<?php endif; ?>