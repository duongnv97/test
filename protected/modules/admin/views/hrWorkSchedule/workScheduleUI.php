<?php
$this->breadcrumbs=array(
	$this->singleTitle,
);
$css = Yii::app()->theme->baseUrl . '/admin/css/hr.css';
Yii::app()->clientScript->registerCssFile($css);
$css = Yii::app()->theme->baseUrl . '/js/bootstrap-multiselect-master/css/bootstrap-3.1.1.min.css';
Yii::app()->clientScript->registerCssFile($css);

$scriptUrl = Yii::app()->theme->baseUrl . '/admin/js/hr.js';
Yii::app()->clientScript->registerScriptFile($scriptUrl);
$scriptUrl = Yii::app()->theme->baseUrl . '/js/bootstrap-multiselect-master/js/bootstrap-3.1.1.min.js';
Yii::app()->clientScript->registerScriptFile($scriptUrl);
$menus=array(
    array('label'=>'Tạo mới ca làm việc',
            'url' => array('createWorkShift'),
            'htmlOptions'=>array(
                'class'=>'create',
                'label'=>'Tạo ca',
            ),
        ),
    array('label'=>'Danh sách kế hoạch làm việc',
            'url' => array('listWorkPlan'),
            'htmlOptions'=>array(
                'class'=>'index',
                'label'=>'DS Kế hoạch',
            ),
    ),
);

$this->menu= ControllerActionsName::createMenusRoles($menus, $actions);
?>

<?php echo MyFormat::BindNotifyMsg();?>
<h1>Danh Sách <?php echo $this->pluralTitle; ?></h1>

<!--    Search Form     -->
<?php 
    $this->widget('HrSearchWidget', array(
        'model'  => $model,
//        'method' => 'post'
    ));
?>	
 	

<div class="wide form" style="padding: 0;">
<?php $form=$this->beginWidget('CActiveForm', array(
//	'action'=>Yii::app()->createUrl('admin/HrWorkSchedule/workScheduleUI'),
	'method'=>'post',
        'id' => 'form-schedule',
)); ?>
    
    <?php include 'workScheduleUIFormGen.php'; ?>
    
    <!--    TAB-->
    <!--<div class="row" style="margin: 0">-->
        <?php 
//            $this->widget('bootstrap.widgets.TbTabs', array(
//                'id' => 'ws-tabs',
//                'type' => 'tabs',
//                'tabs' => $model->getArrayTabs(),
//            ));
        ?>	
    <!--</div>-->
    
    <div id="data">
        <?php // echo $model->buildHtmlWorkSchedule() ?>
        <?php if(!empty($aData['object_user'])): ?>
        <!--Để redirect sau khi save-->
        <input type="hidden" name="HrWorkSchedule[url]" value="<?php echo Yii::app()->request->url; ?>">
        <!--Lấy danh sách user khi save-->
        <input type="hidden" name="HrWorkSchedule[list_id_user]" value='<?php echo json_encode($aData['list_id_user']); ?>'>
        <input type="hidden" name="HrWorkSchedule[datePlanFrom]" value='<?php echo $model->datePlanFrom; ?>'>
        <input type="hidden" name="HrWorkSchedule[datePlanTo]"   value='<?php echo $model->datePlanTo; ?>'>
        <ul class="list_shift">
            <?php $i = 1; ?>
            <?php foreach ($aData['shift'] as $shift):?>
            <?php 
            $shift_m_color = '';
            $aShift_m_id = [6, 5, 15]; // Id ca m1, m2, m3
            if(in_array($shift->id, $aShift_m_id)){
                $shift_m_color = "shift_color_yellow";
            }
            $aShiftRole = HrSalaryReports::model()->getArrayRoleSalary(true);
            $aShiftType = HrWorkShift::model()->getArrayType();
            $role       = isset($aShiftRole[$shift->role_id]) ? $aShiftRole[$shift->role_id] : "";
            $type       = isset($aShiftType[$shift->type]) ? $aShiftType[$shift->type] : "";
            $hover      = $role . " - " . $type;
            ?>
            <li class='shift_container shift_item shift_color_<?php echo $shift->id . " " . $shift_m_color; ?>' 
                data-shift_name='<?php echo $shift->name ?>' 
                data-shift_id='<?php echo $shift->id ?>'
                title='<?php echo $hover; ?>'>
                    <?php echo $shift->name ?>
            </li>
            <?php endforeach; ?>
        </ul>
        
        <table id='tbl_work_schedule' class='hm_table'>
            <thead>
                <tr>
                    <th class="fix-col">#</th>
                    <th class="fix-col">Nhân viên</th>
                    <?php if($model->is_view_sum): ?>
                        <th class="sum_col fix-col">Thống kê</th>
                        <th class="sum_col_small fix-col">Tổng</th>
                    <?php endif; ?>
                    <?php if($model->canViewJobInfo()): ?>
                        <th class="job_info fix-col">Job info</th>
                        <th class="position_col fix-col">Chức vụ</th>
                    <?php endif; ?>
                    <?php if($model->canViewMonitor()): ?>
                        <th class="position_col fix-col">Chuyên viên</th>
                    <?php endif; ?>
                    <?php if($model->canViewPosition()): ?>
                        <th class="position_col fix-col">BP</th>
                        <th class="province_col fix-col">Tỉnh</th>
                    <?php endif; ?>
                    <?php foreach ($aData['date'] as $key => $value): ?>
                    <th class='wday'>
                        <?php echo $value->format('D'); ?><br>
                        <?php echo $value->format('d'); ?>
                    </th>
                    <?php endforeach; ?>
                </tr>
            </thead>
            <tbody>
                <?php
                $no = 1;
                foreach ($aData['object_user'] as $mUser):
                    // Sum theo nhân viên (mở lúc test) DuongNV close Nov 1,2018
                    $sum_info     = '';
                    $sum_all      = 0;
                    $dateBeginJob = isset($aData['info'][$mUser->id]['date_begin_job']) ? $aData['info'][$mUser->id]['date_begin_job'] : '';
                    $dateLeaveJob = isset($aData['info'][$mUser->id]['leave_date']) ? $aData['info'][$mUser->id]['leave_date'] : '';
                    $periodArray  = iterator_to_array($aData['date']);
                    $startDate    = reset($periodArray)->format('Y-m-d');
                    $endDate      = end($periodArray)->format('Y-m-d');
                    // Nếu nhân viên vào làm sau thời gian tính hoặc nghỉ việc trc thgian tính thì không hiện
                    if($dateBeginJob > $endDate || (!empty($dateLeaveJob) && $dateLeaveJob < $startDate)){
                        continue;
                    }
                    if(isset($aData['sum_column'][$mUser->id])){
//                        $num_break = 0;
                        foreach ($aData['sum_column'][$mUser->id] as $aInfo) {
//                            $num_break++; $br = ', ';
//                            if($num_break == 3){
//                                $num_break = 0;
//                                $br = '<br>';
//                            }
                            $sum_info .= '<b>' . $aInfo['name'] . '</b>' . ': ' . $aInfo['num'] . ", ";
                            $sum_all += $aInfo['num'];
                        }
                    }
                    $sum_info  .= isset($aData['sum_off'][$mUser->id]) ? '<p style="margin:0;"><b>Off:</b> ' . $aData['sum_off'][$mUser->id] . '</p>': '';
                    $sum_info  .= isset($aData['leave'][$mUser->id]) ? '<p style="margin:0;"><b>X:</b> ' . $aData['leave'][$mUser->id] . '</p>': '';
                    $aPosition  = UsersProfile::model()->getArrWorkRoom();
                    $aPosRoom   = UsersProfile::model()->getArrPositionRoom();
                    $aProvince  = GasProvince::getArrAll();
                    $position   = isset($aData['info'][$mUser->id]['position_work']) ? $aData['info'][$mUser->id]['position_work'] : '';
                    $posRoom    = isset($aData['info'][$mUser->id]['position_room']) ? $aData['info'][$mUser->id]['position_room'] : '';
                ?>
                <tr>
                    <td class="fix-col"><?php echo $no++; ?></td>
                    <td class='employee_cell fix-col'><?php echo $mUser->first_name; ?></td>
                    <?php if($model->is_view_sum): ?>
                        <td class="sum_col fix-col"><?php echo $sum_info; ?></td>
                        <td class="sum_col_small fix-col"><?php echo $sum_all; ?></td>
                    <?php endif; ?>
                    <?php if($model->canViewJobInfo()): ?>
                        <td class="job_info fix-col">
                            <b>Begin:</b> <?php echo MyFormat::dateConverYmdToDmy($dateBeginJob); ?><br>
                            <?php echo (empty($dateLeaveJob)) ? '' : '<b>End:</b> '.MyFormat::dateConverYmdToDmy($dateLeaveJob); ?>
                        </td>
                        <td class="position_col fix-col"><?php echo isset($aPosRoom[$posRoom]) ? $aPosRoom[$posRoom] : '' ?></td>
                    <?php endif; ?>
                    <?php if($model->canViewMonitor()): ?>
                        <?php
                        $monitorId   = isset($aData['employee_monitor'][$mUser->id]) ? $aData['employee_monitor'][$mUser->id] : '';
                        $monitorName = isset($aData['model_monitor'][$monitorId]) ? $aData['model_monitor'][$monitorId]->first_name : '';
                        ?>
                        <td class="position_col fix-col"><?php echo $monitorName; ?></td>
                    <?php endif; ?>
                    <?php if($model->canViewPosition()): ?>
                        <td class="position_col fix-col"><?php echo isset($aPosition[$position]) ? $aPosition[$position] : '' ?></td>
                        <td class="province_col fix-col"><?php echo isset($aProvince[$mUser->province_id]) ? $aProvince[$mUser->province_id] : '' ?></td>
                    <?php endif; ?>
                    <?php 
                    foreach ($aData['date'] as $date):
                        $dmy                = $date->format('d-m-Y');
                        $day                = $date->format('l');
                        $css                = '';
                        if($day == 'Sunday') $css .= 'weekend';
                        $i                  = date('dm', strtotime($dmy)); //get key (date is key in array) 01-07 -> 0107
                        $isWorkingDate      = isset($aData['schedule'][$i][$mUser->id]);
                        $isLeaveDate        = isset($aData['leave_date'][$mUser->id][$date->format('Y-m-d')]);
                        $isHoliday          = isset($aData['holiday'][$date->format('Y-m-d')]);
//                        $css                .= ($isLeaveDate || (!$isWorkingDate && $isHoliday)) ? 'cell_container_no_drop' : 'cell_container';
                    ?>
                    <td class='cell_container <?php echo $css; ?>' data-date='<?php echo $dmy; ?>' data-employee='<?php echo $mUser->id; ?>' >
                        <?php
                        // Nếu ngày đó đi làm ( và không nghỉ phép)
                        if( $isWorkingDate ){
                            foreach($aData['schedule'][$i][$mUser->id] as $userShift):
                                $aDataShift = array(
                                    $userShift['id'],       // shift id
                                    $dmy,                   // Date
                                    $userShift['employee'], // User id
                                );
                                $data           = json_encode($aDataShift);
                                $shift_m_color  = '';
                                $aShift_m_id    = [6, 5, 15]; // Id ca m1, m2, m3
                                if(in_array($userShift['id'], $aShift_m_id)){
                                    $shift_m_color = "shift_color_yellow";
                                }
                        ?>
                                <div class='shift_cell shift_color_<?php echo $userShift['id'] . " " . $shift_m_color; ?> alreadyIn' 
                                        data-shift_id='<?php echo $userShift['id']; ?>' 
                                        data-shift_name='<?php echo $userShift['name']; ?>' 
                                        data-date='<?php echo $dmy; ?>' 
                                        data-employee='<?php echo $userShift['employee']; ?>'  >
                                    <?php echo $userShift['name']; ?>
                                    <input type='hidden' name='HrWorkSchedule[data][]' value ='<?php echo $data; ?>' class='unmodify'>
                                </div>
                            <?php continue; // Chỉ hiện 1 ca để khi lưu không bị lỗi ?>
                            <?php endforeach; ?>
                            <?php // continue; ?>
                        <!--Nếu là ngày lễ và không đi làm-->
                        <?php } elseif( $isHoliday ) {
                            echo "<div class='leave-wrap' data-date='".$dmy."' data-employee='".$mUser->id."' >L</div>";
                            continue;
                        } 
                        // Nếu là ngày nghỉ phép đã duyệt
                        if( $isLeaveDate ){
                            echo "<div class='leave-wrap' data-date='".$dmy."' data-employee='".$mUser->id."' >P</div>";
                            continue;
                        }
                        ?>
                        
                    </td>
                    <?php endforeach; ?>
                    </tr>
                <?php endforeach; ?>
                    
                <!--Cột tổng cuối lịch-->
                <?php foreach($aData['sum_row'] as $aSumOfShift): ?>
                <tr class="sum_row">
                    <td></td>
                    <?php if($model->is_view_sum): ?>
                        <td></td>
                        <td></td>
                    <?php endif; ?>
                    <?php if($model->canViewJobInfo()): ?>
                        <td></td>
                        <td></td>
                    <?php endif; ?>
                    <?php if($model->canViewMonitor()): ?>
                        <td></td>
                    <?php endif; ?>
                    <?php if($model->canViewPosition()): ?>
                        <td></td>
                        <td></td>
                    <?php endif; ?>
                    <td><?php echo $aSumOfShift['name']; ?></td>
                    <?php foreach ($aData['date'] as $date): ?>
                        <td>
                        <?php 
                        echo isset($aSumOfShift['num'][$dmy = $date->format('Y-m-d')]) ? $aSumOfShift['num'][$dmy = $date->format('Y-m-d')] : '';
                        ?>
                        </td>
                    <?php endforeach; ?>
                </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
        <i>*Lưu ý: Tổng số ca có nhân hệ số.</i><br>
        <b>L</b><i>: Ngày lễ, nhân viên không đi làm.</i><br>
        <b>P</b><i>: Ngày nghỉ phép đã duyệt.</i><br>
            <?php // if($model->canAutoSchedule()): ?>
            <div class="row buttons" style="padding: 15px;">
            <?php if($model->canEditSchedule()): ?>
                <input class="saveBtn btn btn-small" id="yw2" type="submit" name="saveBtn" value="SAVE">
            <?php endif; ?>
            <?php if($model->canDeleteSchedule()): ?>
                <input class="saveBtn btn btn-small" 
                       type="submit" name="deleteScheduleBtn" 
                       value="Xóa lịch hiện tại" 
                       title="Xóa lịch làm việc này"
                       onclick="return confirm('Bạn có chắc muốn xóa lịch làm việc này?')">
            <?php endif; ?>
            </div>
        <?php endif; ?>
    </div>
    <?php $this->endWidget(); ?>
</div><!-- search-form -->

<script>
    var role_id = '<?php echo !empty($_GET['role']) ? $_GET['role'] : 0; ?>';
    if(role_id != '0'){
        $.blockUI();
        ajaxSchedule(role_id);
        $('.tabLink').parent().removeClass('active');
        $('.tabLink[data-role_id="'+role_id+'"]').parent().addClass('active');
    }
    ajaxLoadShift();
    scrollTable();
    jQuery('body').on('click','.tabLink',function(e){
        var role_id = $(e.target).data('role_id');
        $.blockUI();
        ajaxSchedule(role_id);
        ajaxLoadShift(role_id);
        //return false;
    });
    function ajaxSchedule(role_id){
        jQuery.ajax({
            'url':'<?php echo Yii::app()->createAbsoluteUrl('/admin/HrWorkSchedule/UpdateAjax/datePlanFrom/'.$model->datePlanFrom.'/datePlanTo/'.$model->datePlanTo.'/role') ?>'+'/'+role_id,
            'cache':false,
            'success':function(html){
                $.unblockUI();
                jQuery("#data").html(html)
                scrollTable();
            }
        });
    }
    
    /**
    * @todo load shift auto generate schedule
     * @param {type} role_id
     * @returns {undefined}     */
    function ajaxLoadShift(role_id = ''){
        var role_params = '/role/' + role_id;
        if(role_id == ''){
            role_id = '<?php echo isset($_GET['role']) ? $_GET['role'] : ''; ?>';
            role_params = (role_id == '') ? '' : '/role/' + role_id;
        }
        jQuery.ajax({
            'url':'<?php echo Yii::app()->createAbsoluteUrl('/admin/HrWorkSchedule/UpdateAjax/updateShift/1') ?>' + role_params,
            'cache':false,
            'success':function(html){
                jQuery("#HrWorkSchedule_work_shift_id").html(html)
            }
        });
    }
</script>
    <?php
    // Only allow modify when datePlanFrom >= first date of this month
    //$beforeFirstDateOfMonth = MyFormat::modifyDays(date('01-m-Y'), 1, '-'); // last date of last month
    $month_ini = new DateTime("first day of last month");
    $firstDateOfLastMonth = $month_ini->format('Y-m-d');
    $beforeFirstDateOfMonth = MyFormat::modifyDays($firstDateOfLastMonth, 1, '-'); //Last date of before last month
//    if(MyFormat::compareTwoDate($model->datePlanFrom, $beforeFirstDateOfMonth)){
    if(1){
     ?>
    <!--DRAG N DROP-->
    <script type="text/javascript">
        $(document).ready(function(){
            <?php // if($model->canAutoSchedule()): ?>
            <?php if($model->canEditSchedule()): ?>
                wsallowDrag("shift_container","cell_container","shift_id");
            <?php endif; ?>
            $(document).on('dblclick', ".shift_container", function(){
                $.colorbox({
                    href:'<?php echo Yii::app()->createAbsoluteUrl('/admin/HrWorkShift/update'); ?>/id/'+$(this).data('shift_id')+'/layout/ajax', 
                    iframe: true,
                    overlayClose: false, escKey: false,
                    innerHeight: '400',
                    innerWidth: '600', 
                    close: "<span title='close'>close</span>",
                    onClosed: function () {
                        location.reload();
                    }
                });
            });
        })
    </script>
<?php } ?>
