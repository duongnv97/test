<?php
$this->breadcrumbs=array(
	$this->singleTitle,
);

$menus=array(
    array('label'=>"Create $this->singleTitle", 'url'=>array('WorkScheduleUI'),
            'htmlOptions'=>array(
                'class'=>'create',
                'label'=>'Tạo mới',
            ),),
);

$this->menu= ControllerActionsName::createMenusRoles($menus, $actions);
Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('WorkSchedule-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<?php echo MyFormat::BindNotifyMsg();?>
<h1>Danh Sách <?php echo $this->pluralTitle; ?></h1>

<?php if(!isset($_GET['HrWorkSchedule'])): ?>
    <?php echo CHtml::link('Tìm Kiếm Nâng Cao','#',array('class'=>'search-button')); ?>
    <div class="search-form" style="display:none">
    <?php $this->renderPartial('_search',array(
           'model'=>$model,
    )); ?>
    </div><!-- search-form -->
<?php endif; ?>

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'WorkSchedule-grid',
	'dataProvider'=>$model->search(),
        'template'=>'{pager}{summary}{items}{pager}{summary}',
//    'itemsCssClass' => 'items custom_here',
//    'htmlOptions'=> array('class'=>'grid-view custom_here'),
        'pager' => array(
            'maxButtonCount'=>  CmsFormatter::$PAGE_MAX_BUTTON,
        ),
//	'enableSorting' => false,
	//'filter'=>$model,
	'columns'=>array(
            array(
                'header' => 'S/N',
                'type' => 'raw',
                'value' => '$this->grid->dataProvider->pagination->currentPage * $this->grid->dataProvider->pagination->pageSize + ($row+1)',
                'headerHtmlOptions' => array('width' => '10px','style' => 'text-align:center;'),
                'htmlOptions' => array('style' => 'text-align:center;')
            ),
            array(
                'name'=>'employee_id',
                'type'=>'raw',
                'value'=>'$data->getEmployee()',
            ),
            array(
                'name'=>'work_day',
                'type'=>'raw',
                'value'=>'$data->getWorkDay()',
            ),
            array(
                'name'=>'work_shift_id',
                'type'=>'raw',
                'value'=>'$data->getWorkShift()',
            ),
            array(
                'name'=>'work_plan_id',
                'type'=>'raw',
                'value'=>'$data->getWorkPlan()',
            ),
            array(
                'name'=>'status',
                'type'=>'raw',
                'value'=>'$data->getStatus()',
            ),

            array(
            'header' => 'Actions',
            'class' => 'CButtonColumn',
            'template' => ControllerActionsName::createIndexButtonRoles($actions),
            'buttons' => array(
                
                ),
            ),
	),
)); ?>