<?php
$this->breadcrumbs=array(
	$this->singleTitle,
);
$scriptUrl = Yii::app()->theme->baseUrl . '/admin/css/hr.css';
Yii::app()->clientScript->registerCssFile($scriptUrl);
$scriptUrl = Yii::app()->theme->baseUrl . '/admin/js/hr.js';
Yii::app()->clientScript->registerScriptFile($scriptUrl);
?>

<?php echo MyFormat::BindNotifyMsg();?>
<h2>Xét duyệt kế hoạch làm việc 
    từ ngày <?php echo MyFormat::dateConverYmdToDmy($model->date_from) ?> 
    đến ngày <?php echo MyFormat::dateConverYmdToDmy($model->date_to) ?> 
    của <?php echo Roles::GetRoleNameById($model->role_id) ?>!</h2>


    
<div class="wide form" style="padding: 0;">
<?php $form=$this->beginWidget('CActiveForm', array(
	'method'=>'post',
        'id' => 'form-search',
)); ?>
    
        <div class="row buttons">
            <?php $this->widget('bootstrap.widgets.TbButton', array(
                'buttonType'=>'submit',
                'label'=>'Duyệt',
                'type'=>'info', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
                'size'=>'small', // null, 'large', 'small' or 'mini'
                'htmlOptions' => array(
                        'style' => 'margin-left: 50px; margin-bottom: 20px;',
                        'name' => 'HrWorkPlan[approve]',
                        'value' => '1',
                    ),
            )); ?>
        </div>
    
        <div class="row">
            <?php echo $form->labelEx($model,'notify'); ?>
            <?php echo $form->textArea($model,'notify'); ?>
            <?php echo $form->error($model,'notify'); ?>
        </div>
    
        <div class="row buttons" style="padding-left: 159px;">
            <?php $this->widget('bootstrap.widgets.TbButton', array(
                'buttonType'=>'submit',
                'label'=>'Yêu cầu chỉnh sửa',
                'type'=>'null', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
                'size'=>'small', // null, 'large', 'small' or 'mini'
                'htmlOptions' => array('name' => 'HrWorkPlan[require_update]', 'value' => '1'),
            )); ?>	
        </div>

    <div id="data">
        <?php 
        $from = MyFormat::dateConverYmdToDmy($model->date_from, 'd-m-Y');
        $to   = MyFormat::dateConverYmdToDmy($model->date_to, 'd-m-Y');
        echo HrWorkSchedule::model()->buildHtmlWorkSchedule($from, $to, $model->role_id) ;
        ?>
    </div>
    <?php $this->endWidget(); ?>
</div><!-- search-form -->
<script>
    scrollTable();
</script>

