<?php
$this->breadcrumbs=array(
	'Quản lý STT' =>array('index'),
	$model->id,
);

$menus = array(
	array('label'=>'Quản Lý', 'url'=>array('indexStock'),
            'htmlOptions'=>array('class'=>'index','label'=>'Quản lý')),
	array('label'=>'Cập Nhật', 
            'url'=>array('updateStock', 'id'=>$model->id),
            'htmlOptions'=>array('class'=>'update','label'=>'Update')
            ),
	array('label'=>'Xóa Văn Bản', 
            'url'=>array('deleteStock'),
            'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Bạn chắc chắn muốn xóa ?'),
            'htmlOptions'=>array('class'=>'delete','label'=>'Xóa')
            ),
);
$this->menu= ControllerActionsName::createMenusRoles($menus, $actions);

$mAppOrder = GasAppOrder::model()->findByPk($model->app_order_id);
?>
<h1>Xem thủ kho</h1>
<?php 
    if($mAppOrder){
       $this->widget('zii.widgets.CDetailView', array(
            'data'=>$mAppOrder,
            'attributes'=>array(
                array(
                    'name' =>'code_no',
                    'type' =>'raw',
                    'value' =>$mAppOrder->code_no."<br>".$mAppOrder->getArrayDeliveryText(),
                ),
                array(
                    'name' =>'date_delivery',
                    'type' =>'raw',
                    'value' =>$mAppOrder->getDateDelivery(),
                ),
            ),
        ));  
    }
?>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
            array(
                'name' => 'agent_id',
                'value' => $model->getAgent(),
            ), 
            array(
                'name' => 'seri',
                'value' => $model->getSeri(),
            ),
            array(
                'name' => 'seri_real',
                'value' => $model->getSeriReal(),
            ),
            array(
                'name' => 'materials_id',
                'value' => $model->getMaterial(),
            ), 
            array(
                'name' => 'driver_id',
                'value' => $model->getDriver(),
            ),
            array(
                'name' => 'car_id',
                'value' => $model->getCar(),
            ),
            array(
                'name' => 'customer_id',
                'value' => $model->getCustomer(),
            ),
            array(
                'name' => 'created_by',
                'value' => $model->getCreatedBy(),
            ),
            array(
                'name' => 'created_date',
                'value' => $model->getCreatedDate(),
            ),
	),
)); ?>


<script>
    $(window).load(function(){ 
        $('.form').find('button:submit').click(function(){
            $.blockUI({ overlayCSS: { backgroundColor: '#fff' } }); 
        });
        
        fnResizeColorbox();
        parent.$.fn.yiiGridView.update("gas-support-customer-grid"); 
    });
    
    function fnResizeColorbox(){
//        var y = $('body').height()+100;
        var y = $('#main_box').height()+100;
        parent.$.colorbox.resize({innerHeight:y});
    }
    $(document).ready(function(){
        $('.form').find('button:submit').click(function(){
            $.blockUI({ overlayCSS: { backgroundColor: '#fff' } }); 
        });        
    });
</script>
