<?php
$this->breadcrumbs=array(
	$this->pageTitle =>array('indexStock'),
	$model->id =>array('viewStock','id'=>$model->id),
	'Cập Nhật',
);

$menus = array(	
        array('label'=>$this->pageTitle, 'url'=>array('indexStock'),'htmlOptions'=>array('class'=>'index','label'=>'Quản lý')),
	array('label'=>'Xem ', 'url'=>array('viewStock', 'id'=>$model->id),'htmlOptions'=>array('class'=>'view','label'=>'Xem')),
//	array('label'=>'Tạo Mới ', 'url'=>array('createStock')),	
);
$this->menu= ControllerActionsName::createMenusRoles($menus, $actions);

?>

<h1>Cập Nhật Thủ Kho </h1>

<?php echo $this->renderPartial('stock/_form', array('model'=>$model)); ?>