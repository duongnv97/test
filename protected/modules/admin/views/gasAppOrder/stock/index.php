<?php
$this->breadcrumbs=array(
	$this->pageTitle,
);

//$menus=array(
//        array('label'=>'Create Văn Bản', 'url'=>array('createStock' ),
//'htmlOptions'=>array('label'=>'Tạo mới')),
//);
//$this->menu= ControllerActionsName::createMenusRoles($menus, $actions);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('gas-text-grid', {
                url : $(this).attr('action'),
		data: $(this).serialize()
	});
	return false;
});
");

Yii::app()->clientScript->registerScript('ajaxupdate', "
$('#gas-text-grid a.ajaxupdate').live('click', function() {
    $.fn.yiiGridView.update('gas-text-grid', {
        type: 'POST',
        url: $(this).attr('href'),
        success: function() {
            $.fn.yiiGridView.update('gas-text-grid');
        }
    });
    return false;
});
");
?>

<h1>Quản Lý Nhập Xuất STT</h1>
<?php include 'index_button.php'; ?>
<?php echo CHtml::link('Tìm Kiếm Nâng Cao','#',array('class'=>'search-button')); ?>
<div class="search-form">
<?php 
$this->renderPartial('stock/_search',array(
	'model'=>$model,
));
?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'gas-text-grid',
	'dataProvider'=>$model->search(),
        'afterAjaxUpdate'=>'function(id, data){ fnUpdateColorbox();}',
        'template'=>'{pager}{summary}{items}{pager}{summary}', 
        'pager' => array(
            'maxButtonCount'=>  CmsFormatter::$PAGE_MAX_BUTTON,
        ),            
	'columns'=>array(
        array(
            'header' => 'S/N',
            'type' => 'raw',
            'value' => '$this->grid->dataProvider->pagination->currentPage * $this->grid->dataProvider->pagination->pageSize + ($row+1)',
            'headerHtmlOptions' => array('width' => '10px','style' => 'text-align:center;'),
            'htmlOptions' => array('style' => 'text-align:center;')
        ),
                array(
                    'header' => 'Ngày',
                    'type' => "html",
                    'value' => '$data->getCreatedDateOnly()'
                ),
                array(
                    'name' => 'agent_id',
                    'type' => "html",
                    'value' => '$data->getAgent()'
                ),
                array(
                    'visible'=> MyFormat::getCurrentRoleId()==ROLE_ADMIN,
                    'name' => 'app_order_id',
                    'type' => "html",
                    'value' => '$data->getAppOrderInfo()'
                ), 
                array(
                    'name' => 'driver_id',
                    'type' => "html",
                    'value' => '$data->getDriver()'
                ),
                array(
                    'name' => 'car_id',
                    'type' => "html",
                    'value' => '$data->getCar()'
                ),
                array(
                    'name' => 'customer_id',
                    'type' => "html",
                    'value' => '$data->getCustomer().$data->getLost()'
                ),
                array(
                    'name' => 'seri',
                    'type' => "html",
                    'value' => '$data->getSeri()'
                ),
                array(
                    'name' => 'seri_real',
                    'type' => "html",
                    'value' =>'$data->getSeriReal()',
                ),
                array(
                    'name' => 'materials_id',
                    'type' => "html",
                    'value' => '$data->getMaterial()'
                ),
                array(
                    'name' => 'status',
                    'type' => "html",
                    'value' =>'$data->getStatus()',
                ),
                array(
                    'visible'=> MyFormat::getCurrentRoleId()==ROLE_ADMIN,
                    'name' => 'created_by',
                    'type' => "html",
                    'value' => '$data->getCreatedBy()'
                ),
                array(
                    'visible'=> MyFormat::getCurrentRoleId()==ROLE_ADMIN,
                    'name' => 'created_date',
                    'type' => "html",
                    'value' => '$data->getCreatedDate()'
                ),
		array(
                    'header' => 'Actions',
                    'class'=>'CButtonColumn',
                    'template'=> ControllerActionsName::createIndexButtonRoles($actions,array('viewStock','updateStock','delete')),
                    'buttons'=>array(
                        'updateStock'=>array(
                            'visible'=> '$data->canUpdate()',
                            'label'=>'Cập nhật thủ kho',
                            'url'=>'Yii::app()->createAbsoluteUrl("admin/gasAppOrder/updateStock",array("id"=>$data->id))',
                            'imageUrl'=>Yii::app()->theme->baseUrl . '/admin/images/icon/update_icon.png',
                        ),
                        'viewStock'=>array(
                            'visible'=> '$data->canUpdate()',
                            'label'=>'Xem thủ kho',
                            'url'=>'Yii::app()->createAbsoluteUrl("admin/gasAppOrder/viewStock",array("id"=>$data->id))',
                            'imageUrl'=>Yii::app()->theme->baseUrl . '/admin/images/icon/view_icon.png',
                        ),
                        'delete'=>array(
                            'visible'=> '$data->canUpdate()',
                            'label'=>'Xóa',
                            'url'=>'Yii::app()->createAbsoluteUrl("admin/gasAppOrder/deleteStock",array("id"=>$data->id))',
                            'imageUrl'=>Yii::app()->theme->baseUrl . '/admin/images/deleteDefault.png',
                            'options' => array(
                                'class'=>'delete',
                            ),
                        ),
                    ),
		),
	),
)); ?>

<script>
$(document).ready(function() {
    fnUpdateColorbox();
});
function fnUpdateColorbox(){
    fnShowhighLightTr();
    fixTargetBlank();
    $(".view").colorbox({iframe:true,
        innerHeight:'1000', 
        innerWidth: '1100', escKey:false,close: "<span title='close'>close</span>"
    });
}
</script>