<div class="form">
    <?php
        $LinkNormal         = Yii::app()->createAbsoluteUrl('admin/gasAppOrder/indexStock');
        $LinkType1         = Yii::app()->createAbsoluteUrl('admin/gasAppOrder/indexStock', array( 'type'=> Stock::TYPE_EXPIRED   ));
    ?>
    <h1><?php echo $this->adminPageTitle;?>
        <a class='btn_cancel f_size_14 <?php echo (!isset($_GET['type'])) ? "active":"";?>' href="<?php echo $LinkNormal;?>">STT Xuất</a>
        <a class='btn_cancel f_size_14 <?php echo isset($_GET['type']) && $_GET['type']==Stock::TYPE_EXPIRED ? "active":"";?>' href="<?php echo $LinkType1;?>">STT Chưa quay về</a>
    </h1> 
</div>