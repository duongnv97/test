<?php
$this->breadcrumbs=array(
	$this->pageTitle =>array('indexStock'),
	'Tạo Mới',
);

$menus = array(		
        array('label'=>'Quản Lý', 'url'=>array('indexStock'),
            'htmlOptions'=>array('class'=>'index','label'=>'Quản lý')),
);
$this->menu= ControllerActionsName::createMenusRoles($menus, $actions);
?>

<h1>Tạo Mới Nhập Xuất STT</h1>

<?php echo $this->renderPartial('stock/_form', array('model'=>$model)); ?>