<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

        <div class="row">
        <?php echo $form->labelEx($model,'agent_id'); ?>
        <?php echo $form->hiddenField($model,'agent_id', array('class'=>'')); ?>
        <?php
                // 1. limit search kh của sale
                $url = Yii::app()->createAbsoluteUrl('admin/ajax/search_by_role', array('role'=> ROLE_AGENT));
                // widget auto complete search user customer and supplier
                $aData = array(
                    'model'=>$model,
                    'field_customer_id'=>'agent_id',
                    'url'=> $url,
                    'name_relation_user'=>'rAgent',
                    'ClassAdd' => 'w-400',
                    'field_autocomplete_name' => 'autocomplete_name_2',
                    'placeholder'=>'Nhập mã hoặc tên đại lý',
                );
                $this->widget('ext.SpaAutocompleteCustomer.SpaAutocompleteCustomer',
                    array('data'=>$aData));
                ?>
            <?php echo $form->error($model,'agent_id'); ?>
        </div>
    
        <div class="row">
            <?php echo $form->labelEx($model,'driver_id'); ?>
            <?php echo $form->hiddenField($model,'driver_id', array('class'=>'')); ?>
            <?php
                    // 1. limit search kh của sale
                    $url = Yii::app()->createAbsoluteUrl('admin/ajax/search_by_role', array('role'=> ROLE_DRIVER));
                    // widget auto complete search user customer and supplier
                    $aData = array(
                        'model'=>$model,
                        'field_customer_id'=>'driver_id',
                        'url'=> $url,
                        'name_relation_user'=>'rDriver',
                        'ClassAdd' => 'w-400',
                        'field_autocomplete_name' => 'autocomplete_name_4',
                        'placeholder'=>'Nhập mã hoặc tên lái xe',
                    );
                    $this->widget('ext.SpaAutocompleteCustomer.SpaAutocompleteCustomer',
                        array('data'=>$aData));
                    ?>
            <?php echo $form->error($model,'driver_id'); ?>
        </div>
    
        <div class="row">
            <?php echo $form->labelEx($model,'car_id'); ?>
            <?php echo $form->hiddenField($model,'car_id', array('class'=>'')); ?>
            <?php
                // 1. limit search kh của sale
                $url = Yii::app()->createAbsoluteUrl('admin/ajax/search_by_role', array('role'=> ROLE_CAR));
                // widget auto complete search user customer and supplier
                $aData = array(
                    'model'=>$model,
                    'field_customer_id'=>'car_id',
                    'url'=> $url,
                    'name_relation_user'=>'rCar',
                    'ClassAdd' => 'w-400',
                    'field_autocomplete_name' => 'autocomplete_name_3',
                    'placeholder'=>'Nhập mã hoặc tên xe ô tô tải',
                );
                $this->widget('ext.SpaAutocompleteCustomer.SpaAutocompleteCustomer',
                    array('data'=>$aData));
                ?>
            <?php echo $form->error($model,'car_id'); ?>
        </div>
    
        <div class="row">
            <?php echo $form->labelEx($model,'customer_id'); ?>
            <?php echo $form->hiddenField($model,'customer_id', array('class'=>'')); ?>
            <?php
                // 1. limit search kh của sale
                $url = Yii::app()->createAbsoluteUrl('admin/ajax/search_user_by_code');
                // widget auto complete search user customer and supplier
                $aData = array(
                    'model'=>$model,
                    'field_customer_id'=>'customer_id',
                    'url'=> $url,
                    'name_relation_user'=>'rCustomer',
                    'ClassAdd' => 'w-400',
                    'field_autocomplete_name' => 'autocomplete_name_1',
                    'placeholder'=>'Nhập mã hoặc tên khách hàng',
                );
                $this->widget('ext.SpaAutocompleteCustomer.SpaAutocompleteCustomer',
                    array('data'=>$aData));
                ?>
                <?php echo $form->error($model,'customer_id'); ?>
        </div>
    
        <div class="row">
            <?php echo $form->labelEx($model,'created_by'); ?>
            <?php echo $form->hiddenField($model,'created_by', array('class'=>'')); ?>
            <?php
                $url = Yii::app()->createAbsoluteUrl('admin/ajax/search_by_role', array());
                // widget auto complete search user customer and supplier
                $aData = array(
                    'model'=>$model,
                    'field_customer_id'=>'created_by',
                    'url'=> $url,
                    'name_relation_user'=>'rUserCreate',
                    'ClassAdd' => 'w-400',
                    'field_autocomplete_name' => 'autocomplete_name_5',
                    'placeholder'=>'Nhập mã NV hoặc tên',
                );
                $this->widget('ext.SpaAutocompleteCustomer.SpaAutocompleteCustomer',
                    array('data'=>$aData));
                ?>
            <?php echo $form->error($model,'created_by'); ?>
        </div>
    
        <div class="row more_col">
            <div class="col1">
                <?php echo $form->label($model,'date_from'); ?>
                <?php 
                    $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                        'model'=>$model,        
                        'attribute'=>'date_from',
                        'options'=>array(
                            'showAnim'=>'fold',
                            'dateFormat'=> MyFormat::$dateFormatSearch,
//                            'minDate'=> '0',
//                            'maxDate'=> '0',
                            'changeMonth' => true,
                            'changeYear' => true,
                            'showOn' => 'button',
                            'buttonImage'=> Yii::app()->theme->baseUrl.'/admin/images/icon_calendar_r.gif',
                            'buttonImageOnly'=> true,                                
                        ),        
                        'htmlOptions'=>array(
                            'class'=>'w-16 date_from',
                            'size'=>'16',
                            'style'=>'float:left;',                               
                        ),
                    ));
                ?>  
            </div>
            <div class='col2'>
                <?php echo $form->label($model,'Đến'); ?>
                <?php 
                    $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                        'model'=>$model,        
                        'attribute'=>'date_to',
                        'options'=>array(
                            'showAnim'=>'fold',
                            'dateFormat'=> MyFormat::$dateFormatSearch,
//                            'minDate'=> '0',
//                            'maxDate'=> '0',
                            'changeMonth' => true,
                            'changeYear' => true,
                            'showOn' => 'button',
                            'buttonImage'=> Yii::app()->theme->baseUrl.'/admin/images/icon_calendar_r.gif',
                            'buttonImageOnly'=> true,                                
                        ),        
                        'htmlOptions'=>array(
                            'class'=>'w-16 date_from',
                            'size'=>'16',
                            'style'=>'float:left;',                               
                        ),
                    ));
                ?>  
            </div>                
        </div>
        <div class="row more_col">
            <div class="col1">
                <?php echo $form->label($model,'status'); ?>
                <?php echo $form->dropDownList($model,'status', $model->getArrayStatus(),array('class'=>'w-200','empty'=>'Select')); ?>
            </div>
            <div class='col2'>
                <?php echo $form->label($model,'different'); ?>
                <?php echo $form->dropDownList($model,'different', $model->getArrayAdvanceSearch(),array('class'=>'w-200','empty'=>'Select')); ?>
            </div>
        </div>
        <div class="row more_col">
        <?php if(isset($_GET['type']) && $_GET['type'] == Stock::TYPE_EXPIRED): ?>
            <div class="col1">
                <?php echo $form->label($model,'Số ngày chưa quay về',array()); ?>
                <?php echo $form->dropDownList($model,'date_expired', $model->getArrayExpired(),array('class'=>'w-200')); ?>		
            </div>
        <?php endif; ?>
            <div class='col2'>
                <?php echo $form->label($model,'type_store_card',array()); ?>
                <?php echo $form->dropDownList($model,'type_store_card', CmsFormatter::$STORE_CARD_TYPE,array('class'=>'w-200', 'empty'=>'Select')); ?>
            </div>
        </div>

<!--        <div class="row col_material ">
                <?php // echo $form->labelEx($model,'materials_id'); ?>
                <?php // echo $form->hiddenField($model,'materials_id',array('class'=>'materials_id_hide')); ?>
                <?php // echo $form->hiddenField($model,'materials_type_id',array('class'=>'materials_type_id_hide')); ?>
                <input class="float_l material_autocomplete w-250"  placeholder="Nhập mã hoặc tên vật tư" maxlength="100" value="" type="text" >
                <span onclick="" class="remove_material_js remove_item_material"></span>
        </div>-->
    
	<div class="row buttons" style="padding-left: 159px;">
        <?php $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType'=>'submit',
            'label'=>'Search',
            'type'=>'null', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
            'size'=>'small', // null, 'large', 'small' or 'mini'
            //'htmlOptions' => array('style' => 'margin-bottom: 10px; float: right;'),
        )); ?>	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->

<?php 
    $material_type = CmsFormatter::$MATERIAL_TYPE_BINHBO_INPUT;
//    unset($material_type[2]); // Oct 20, 2015 thêm cả vỏ 12 vào
?>
<script>
//    $(document).keydown(function(e) {
//        if(e.which == 119) {
//            fnBuildRow();
//        }
//    });
//    $(document).ready(function(){
////        $('.form').find('button:submit').click(function(){
////            $.blockUI({ overlayCSS: { backgroundColor: '#fff' } }); 
////        });
//        fnBindRemoveIcon();
//        fnBindAllAutocomplete();
//        fnBindRemoveMaterialFix('materials_id_hide', 'material_autocomplete');
//    });       
//    // to do bind autocompelte for input material
//    // @param objInput : is obj input ex  $('.customer_autocomplete')    
//    function bindMaterialAutocomplete(objInput){
//        var availableMaterials = <?php // echo MyFunctionCustom::getMaterialsJsonByType($material_type); ?>;
//        var parent_div = objInput.closest('.col_material');
//        objInput.autocomplete({
//            source: availableMaterials,
////            close: function( event, ui ) { $( "#GasStoreCard_materials_name" ).val(''); },
//            select: function( event, ui ) {
//                parent_div.find('.materials_id_hide').val(ui.item.id);
//                parent_div.find('.materials_type_id_hide').val(ui.item.materials_type_id);
//                parent_div.find('.material_autocomplete').attr('readonly',true);
//            }
//        });
//    }
//    function fnBindAllAutocomplete(){
//        // material
//        $('.material_autocomplete').each(function(){
//            bindMaterialAutocomplete($(this));
//        });
//    }
</script>