<div class="form">
<?php 
$mAppOrder = GasAppOrder::model()->findByPk($model->app_order_id);

?>

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'gas-text-form',
	'enableAjaxValidation'=>false,
        'htmlOptions' => array('enctype' => 'multipart/form-data'),
)); ?>
        <?php if(!$model->isNewRecord):?>
        <?php // include '_link_view.php';?>
        <?php endif;?>
	<p class="note">Dòng có dấu <span class="required">*</span> là bắt buộc.</p>
        
        <?php  if(Yii::app()->user->hasFlash('successUpdate')): ?>
            <div class='success_div'><?php echo Yii::app()->user->getFlash('successUpdate');?></div>         
        <?php endif; ?>  	
        <?php echo $form->errorSummary($model); ?>
           
        <div class="row">
		<?php echo $form->textField($model,'app_order_id',array('size'=>60,'style'=>'display:none')); ?>
		<?php echo $form->error($model,'app_order_id'); ?>
                <?php 
                if($mAppOrder){
                   $this->widget('zii.widgets.CDetailView', array(
                        'data'=>$mAppOrder,
                        'attributes'=>array(
                            array(
                                'name' =>'status',
                                'type' =>'raw',
                                'htmlOptions' => array('style' => 'text-align:center;'),
                                'value' =>$mAppOrder->getStatusText(),
                            ),
                            array(
                                'name' =>'code_no',
                                'type' =>'raw',
                                'value' =>$mAppOrder->code_no."<br>".$mAppOrder->getArrayDeliveryText(),
                            ),
                            array(
                                'name' =>'date_delivery',
                                'type' =>'raw',
                                'value' =>$mAppOrder->getDateDelivery(),
                            ),
                            array(
                                'name' =>'agent_id',
                                'type' =>'raw',
                                'value' =>$model->getAgent(),
                            ),
                            array(
                                'name' =>'driver_id',
                                'type' =>'raw',
                                'value' =>$model->getDriver(),
                            ),
                            array(
                                'name' =>'car_id',
                                'type' =>'raw',
                                'value' =>$model->getCar(),
                            ),
                            array(
                                'name' =>'customer_id',
                                'type' =>'raw',
                                'value' =>$model->getCustomer(),
                            ),
                            array(
                                'name' =>'Bình Gas',
                                'type' =>'raw',
                                'value' =>$model->getMaterial(),
                                'visible' => $model->canViewMaterial(),
                            ),
                        ),
                    ));  
                }
                ?>
	</div>
        <?php include '_form_create_multi.php'; ?>
        <!--<div class="row ">-->
            <?php // echo $form->labelEx($model,'agent_id'); ?>
            <?php // echo $form->hiddenField($model,'agent_id', array('class'=>'')); ?>
            <?php
//                // 1. limit search kh của sale
//                $url = Yii::app()->createAbsoluteUrl('admin/ajax/search_by_role', array('role'=> ROLE_AGENT));
//                // widget auto complete search user customer and supplier
//                $aData = array(
//                    'model'=>$model,
//                    'field_customer_id'=>'agent_id',
//                    'url'=> $url,
//                    'name_relation_user'=>'rAgent',
//                    'ClassAdd' => 'w-300',
//                    'field_autocomplete_name' => 'autocomplete_name_4',
//                    'placeholder'=>'Nhập mã hoặc tên đại lý',
//                );
//                $this->widget('ext.SpaAutocompleteCustomer.SpaAutocompleteCustomer',
//                    array('data'=>$aData));
//                ?>
            <?php // echo $form->error($model,'agent_id'); ?>
        <!--</div>-->          
        <?php if(!$model->isNewRecord){ ?>
            <div class="row">
		<?php echo $form->labelEx($model,'seri'); ?>
		<?php echo $form->textField($model,'seri',array('size'=>20,'maxlength'=>250)); ?>
		<?php echo $form->error($model,'seri'); ?>
            </div>
            <div class="row">
		<?php echo $form->labelEx($model,'seri_real'); ?>
		<?php echo $form->textField($model,'seri_real',array('size'=>20,'maxlength'=>250)); ?>
		<?php echo $form->error($model,'seri_real'); ?>
            </div>
        <?php } ?>
        
        <!--<div class="row ">-->
            <?php // echo $form->labelEx($model,'driver_id'); ?>
            <?php // echo $form->hiddenField($model,'driver_id', array('class'=>'')); ?>
            <?php
//                // 1. limit search kh của sale
//                $url = Yii::app()->createAbsoluteUrl('admin/ajax/search_by_role', array('role'=> ROLE_DRIVER));
//                // widget auto complete search user customer and supplier
//                $aData = array(
//                    'model'=>$model,
//                    'field_customer_id'=>'driver_id',
//                    'url'=> $url,
//                    'name_relation_user'=>'rDriver',
//                    'ClassAdd' => 'w-300',
//                    'field_autocomplete_name' => 'autocomplete_name_2',
//                    'placeholder'=>'Nhập mã hoặc tên tài xế',
//                );
//                $this->widget('ext.SpaAutocompleteCustomer.SpaAutocompleteCustomer',
//                    array('data'=>$aData));
                ?>
            <?php // echo $form->error($model,'driver_id'); ?>
        <!--</div>-->
            <!--<div class="row ">-->
            <?php // echo $form->labelEx($model,'car_id'); ?>
            <?php // echo $form->hiddenField($model,'car_id', array('class'=>'')); ?>
            <?php
//                // 1. limit search kh của sale
//                $url = Yii::app()->createAbsoluteUrl('admin/ajax/search_by_role', array('role'=> ROLE_CAR));
//                // widget auto complete search user customer and supplier
//                $aData = array(
//                    'model'=>$model,
//                    'field_customer_id'=>'car_id',
//                    'url'=> $url,
//                    'name_relation_user'=>'rCar',
//                    'ClassAdd' => 'w-300',
//                    'field_autocomplete_name' => 'autocomplete_name_1',
//                    'placeholder'=>'Nhập mã hoặc tên xe',
//                );
//                $this->widget('ext.SpaAutocompleteCustomer.SpaAutocompleteCustomer',
//                    array('data'=>$aData));
                ?>
            <?php // echo $form->error($model,'car_id'); ?>
        <!--</div>-->  
        <!--<div class="row">-->
            <?php // echo $form->labelEx($model,'customer_id'); ?>
            <?php // echo $form->hiddenField($model,'customer_id', array('class'=>'')); ?>
            <?php
//                // 1. limit search kh của sale
//                $url = Yii::app()->createAbsoluteUrl('admin/ajax/search_user_by_code');
//                // widget auto complete search user customer and supplier
//                $aData = array(
//                    'model'=>$model,
//                    'field_customer_id'=>'customer_id',
//                    'url'=> $url,
//                    'name_relation_user'=>'rCustomer',
//                    'ClassAdd' => 'w-400',
//                    'field_autocomplete_name' => 'autocomplete_name_3',
//                    'placeholder'=>'Nhập mã NV hoặc tên',
//                );
//                $this->widget('ext.SpaAutocompleteCustomer.SpaAutocompleteCustomer',
//                    array('data'=>$aData));
                ?>
                <?php // echo $form->error($model,'customer_id'); ?>
        <!--</div>-->
        <?php
//            $mText = '';
//            if($model->rMaterials){
//                $mText = $model->rMaterials->name;
//            }
        ?>
<!--        <div class="row col_material ">
                <?php // echo $form->labelEx($model,'materials_id'); ?>
                <?php // echo $form->hiddenField($model,'materials_id',array('class'=>'materials_id_hide')); ?>
                <?php // echo $form->hiddenField($model,'materials_type_id',array('class'=>'materials_type_id_hide')); ?>
                <input class="float_l material_autocomplete w-250"  placeholder="Nhập mã hoặc tên vật tư" maxlength="100" value="<?php // echo $mText; ?>" type="text" <?php echo !empty($mText)?'readonly="1"':'' ?>>
                <span onclick="" class="remove_material_js remove_item_material"></span>
        </div>-->
            <div class="clr"></div>

	<div class="row buttons" style="padding-left: 141px;">
            <?php $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType'=>'submit',
            'label'=>'Save',
            'type'=>'null', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
            'size'=>'small', // null, 'large', 'small' or 'mini'
            'visible' => $model->canCreateStock(),
            //'htmlOptions' => array('style' => 'margin-bottom: 10px; float: right;'),
        )); ?>	</div>

<?php $this->endWidget(); ?>
<?php 
//    $material_type = CmsFormatter::$MATERIAL_TYPE_BINHBO_INPUT;
//    unset($material_type[2]); // Oct 20, 2015 thêm cả vỏ 12 vào
//    unset($material_type[3]);
?>
</div><!-- form -->
<script>
//    $(document).keydown(function(e) {
//        if(e.which == 119) {
//            fnBuildRow();
//        }
//    });
//    $(document).ready(function(){
//        $('.form').find('button:submit').click(function(){
//            $.blockUI({ overlayCSS: { backgroundColor: '#fff' } }); 
//        });
//        fnBindRemoveIcon();
//        fnBindAllAutocomplete();
//        fnBindRemoveMaterialFix('materials_id_hide', 'material_autocomplete');
//    });       
//    // to do bind autocompelte for input material
//    // @param objInput : is obj input ex  $('.customer_autocomplete')    
//    function bindMaterialAutocomplete(objInput){
//        var availableMaterials = <?php // echo MyFunctionCustom::getMaterialsJsonByType($material_type); ?>;
//        var parent_div = objInput.closest('.col_material');
//        objInput.autocomplete({
//            source: availableMaterials,
////            close: function( event, ui ) { $( "#GasStoreCard_materials_name" ).val(''); },
//            select: function( event, ui ) {
//                parent_div.find('.materials_id_hide').val(ui.item.id);
//                parent_div.find('.materials_type_id_hide').val(ui.item.materials_type_id);
//                parent_div.find('.material_autocomplete').attr('readonly',true);
//            }
//        });
//    }
//    function fnBindAllAutocomplete(){
//        // material
//        $('.material_autocomplete').each(function(){
//            bindMaterialAutocomplete($(this));
//        });
//    }
</script>