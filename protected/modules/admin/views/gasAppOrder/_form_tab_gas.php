<?php 
$aInfoGas = $model->getInfoGasVoArray($model->info_gas);
$aInfoVo  = $model->getInfoGasVoArray($model->info_vo);
$index = 1;
$mAppCache = new AppCache();
$aMaterial  = $mAppCache->getMasterModel('GasMaterials', AppCache::ARR_MODEL_MATERIAL);

?>
<div class="clearfix WrapOneTab" id="tabs-1" tab_index="1">
    <div class="clr"></div>
    <div class="row">
        <?php echo $form->labelEx($model,'ext_materials_id'); ?>
        <?php echo $form->textField($model,'ext_materials_id',array('class'=>'MaterialsGas w-300','placeholder'=>'Nhập mã vật tư hoặc tên vật tư')); ?>
        <?php echo $form->error($model,'ext_materials_id'); ?>
        <div class="clr"></div>    		
    </div>
    <table class="materials_table hm_table" style="width: 100%;">
        <thead>
            <tr class="f_size_11">
                <th class="item_c w-20">#</th>
                <th class="item_code item_c w-80">Mã</th>
                <th class="item_name item_c">Tên</th>
                <th class="item_unit item_c">Unit</th>
                <th class="w-50 item_c">SL</th>
                <th class="w-60 item_c">SL Thực Tế</th>
                <th class="w-50 item_c">Giá</th>
                <th class="w-80 item_c">Thành Tiền</th>
                <th class="item_unit last item_c">Xóa</th>
            </tr>
        </thead>
        <tbody>
        <?php foreach ($aInfoGas as $key => $detail): ?>
        <?php 
            $materials_id   = $detail['materials_id'];
            if(!isset($aMaterial[$materials_id])){
                continue ;
            }
            $code           = $aMaterial[$materials_id]['materials_no'];
            $unit           = $aMaterial[$materials_id]['unit'];
            $classFocus = '';
            if(in_array($detail['materials_type_id'], GasMaterialsType::getArrGasBB() )){
                $classFocus  = 'item_b';
            }
        
        ?>
        <tr class="materials_row">
            <td class="item_c order_no"><?php echo $index++;?></td>
            <td class="item_c materials_code <?php echo $classFocus;?>"><?php echo $code;?></td>
            <td class="materials_name <?php echo $classFocus;?>"><?php echo $detail['materials_name'];?></td>
            <td class="item_c materials_unit"><?php echo $unit;?></td>
            <td class="item_c materials_qty"><?php echo  $detail['qty'];?></td>
            <td class="item_c">
                <input type="text" name="qty_real[]" class="item_c w-50 number_only_v1 materials_qty_real" maxlength="6" value="<?php echo  $detail['qty_real'];?>">
                <input class="materials_type_id" type="hidden" name="materials_type_id[]" value="<?php echo  $detail['materials_type_id'];?>">
                <input class="materials_id" type="hidden" name="materials_id[]" value="<?php echo  $detail['materials_id'];?>">
                <input class="materials_qty_hide" type="hidden" name="qty[]" value="<?php echo  $detail['qty'];?>">
                <input class="materials_seri" type="hidden" name="seri[]" value="<?php echo  $detail['seri'];?>">
                <input class="materials_kg_empty" type="hidden" name="kg_empty[]" value="<?php echo  $detail['kg_empty'];?>">
                <input class="materials_kg_has_gas" type="hidden" name="kg_has_gas[]" value="<?php echo  $detail['kg_has_gas'];?>">
            </td>
            <td class="item_c ">
                <?php // echo  $detail['price'] > 0 ? ActiveRecord::formatCurrencyRound($detail['price']) : '';?>
                <input class="materials_price ad_fix_currency item_r w-80" type="text" name="price[]" value="<?php echo  $detail['price'];?>">
            </td>
            <td class="item_r "><?php echo  $detail['amount'] > 0 ? ActiveRecord::formatCurrencyRound($detail['amount']) : '';?></td>
            <td class="item_c last"><span class="remove_icon_only"></span></td>
        </tr>
        <?php endforeach; ?>
        </tbody>
    </table>
</div>