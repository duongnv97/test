<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=> GasCheck::getCurl(),
	'method'=>'get',
)); 
$aCar = Users::getArrDropdown(ROLE_CAR);
?>
    <div class="row more_col">
        <div class="col1">
            <?php echo $form->label($model,'status', []); ?>
            <?php echo $form->dropDownList($model,'status', $model->getArrayStatus(),array('class'=>'w-200', 'empty'=>'Select')); ?>
        </div>
        <div class="col2">
            <?php echo $form->label($model,'type', []); ?>
            <?php echo $form->dropDownList($model,'type', $model->getArrayDelivery(),array('class'=>'w-200', 'empty'=>'Select')); ?>
        </div>
        <div class="col3">
            <?php echo $form->label($model,'receipts_no', []); ?>
            <?php echo $form->textField($model,'receipts_no',array('class'=>'w-200','maxlength'=>20)); ?>            
        </div>
    </div>
    <div class="row more_col">
        <div class="col1">
            <?php echo $form->label($model,'status_debit', []); ?>
            <?php echo $form->dropDownList($model,'status_debit', $model->getArrayStatusDebit(),array('class'=>'w-200', 'empty'=>'Select')); ?>
        </div>
        <div class="col2">
            <?php echo $form->label($model,'paid_gas_remain', []); ?>
            <?php echo $form->dropDownList($model,'paid_gas_remain', CmsFormatter::$yesNoFormat,array('class'=>'w-200', 'empty'=>'Select')); ?>
        </div>
        <div class="col3">
            <?php echo $form->label($model,'pay_direct', []); ?>
            <?php echo $form->dropDownList($model,'pay_direct', $model->getTypePayDirect(), array('class'=>'w-200', 'empty'=>'Select')); ?>
        </div>
    </div>
    
    <div class="row">
        <?php echo $form->label($model,'uid_login'); ?>
        <?php echo $form->hiddenField($model,'uid_login', array('class'=>'')); ?>
        <?php
            // 1. limit search kh của sale
            $url = Yii::app()->createAbsoluteUrl('admin/ajax/search_by_role', array('role'=> ROLE_DIEU_PHOI.','.ROLE_CALL_CENTER));
            // widget auto complete search user customer and supplier
            $aData = array(
                'model'=>$model,
                'field_customer_id'=>'uid_login',
                'url'=> $url,
                'name_relation_user'=>'rUserLogin',
                'ClassAdd' => 'w-400',
                'field_autocomplete_name' => 'autocomplete_name_2',
                'placeholder'=>'Nhập mã hoặc tên NV',
            );
            $this->widget('ext.SpaAutocompleteCustomer.SpaAutocompleteCustomer',
                array('data'=>$aData));
        ?>
    </div>
    <div class="row">
        <?php echo $form->label($model,'uid_confirm'); ?>
        <?php echo $form->hiddenField($model,'uid_confirm', array('class'=>'')); ?>
        <?php
            // 1. limit search kh của sale
            $url = Yii::app()->createAbsoluteUrl('admin/ajax/search_by_role', array('role'=> ROLE_DIEU_PHOI.','.ROLE_CALL_CENTER));
            // widget auto complete search user customer and supplier
            $aData = array(
                'model'=>$model,
                'field_customer_id'=>'uid_confirm',
                'url'=> $url,
                'name_relation_user'=>'rUidConfirm',
                'ClassAdd' => 'w-400',
                'field_autocomplete_name' => 'isChangeAgent',
                'placeholder'=>'Nhập mã hoặc tên NV',
            );
            $this->widget('ext.SpaAutocompleteCustomer.SpaAutocompleteCustomer',
                array('data'=>$aData));
        ?>
    </div>
    
    <div class="row">
        <?php echo $form->label($model,'sale_id'); ?>
        <?php echo $form->hiddenField($model,'sale_id'); ?>
        <?php 
            // widget auto complete search user customer and supplier
            $aData = array(
                'model'=>$model,
                'field_customer_id'=>'sale_id',
                'field_autocomplete_name'=>'obj_detail_id_old',
                'url'=> Yii::app()->createAbsoluteUrl('admin/ajax/search_sale'),
                'name_relation_user'=>'rSale',
                'placeholder'=>'Nhập Tên Sale',
                'ClassAdd' => 'w-400',
//                        'fnSelectCustomer'=>'fnAfterSelectSaleOrAgent',
            );
            $this->widget('ext.SpaAutocompleteCustomer.SpaAutocompleteCustomer',
                array('data'=>$aData));                                        
        ?>
    </div>

    <div class="row">
        <?php echo $form->label($model,'driver_id'); ?>
        <?php echo $form->hiddenField($model,'driver_id', array('class'=>'')); ?>
        <?php
            // 1. limit search kh của sale
            $url = Yii::app()->createAbsoluteUrl('admin/ajax/search_by_role', array('role'=> ROLE_DRIVER));
            // widget auto complete search user customer and supplier
            $aData = array(
                'model'=>$model,
                'field_customer_id'=>'driver_id',
                'url'=> $url,
                'name_relation_user'=>'rDriver',
                'ClassAdd' => 'w-400',
                'field_autocomplete_name' => 'name_driver',
                'placeholder'=>'Nhập mã hoặc tên tài xế',
            );
            $this->widget('ext.SpaAutocompleteCustomer.SpaAutocompleteCustomer',
                array('data'=>$aData));
        ?>
    </div>
    
    <div class="row">
        <?php echo $form->label($model,'agent_id'); ?>
        <?php echo $form->hiddenField($model,'agent_id', array('class'=>'')); ?>
        <?php
            // 1. limit search kh của sale
            $url = Yii::app()->createAbsoluteUrl('admin/ajax/search_by_role', array('role'=> ROLE_AGENT));
            // widget auto complete search user customer and supplier
            $aData = array(
                'model'=>$model,
                'field_customer_id'=>'agent_id',
                'url'=> $url,
                'name_relation_user'=>'rAgent',
                'ClassAdd' => 'w-400',
                'field_autocomplete_name' => 'autocomplete_name_1',
                'placeholder'=>'Nhập mã hoặc tên đại lý',
            );
            $this->widget('ext.SpaAutocompleteCustomer.SpaAutocompleteCustomer',
                array('data'=>$aData));
        ?>
    </div>
    
    <div class="row">
        <?php echo $form->labelEx($model,'customer_id'); ?>
        <?php echo $form->hiddenField($model,'customer_id', array('class'=>'')); ?>
        <?php 
            // widget auto complete search user customer and supplier
            $aData = array(
                'model'=>$model,
                'field_customer_id'=>'customer_id',
                'name_relation_user'=>'rCustomer',
                'url' => Yii::app()->createAbsoluteUrl('admin/ajax/search_user_by_code'),
                'ClassAdd' => 'w-400',
            );
            $this->widget('ext.SpaAutocompleteCustomer.SpaAutocompleteCustomer',
                array('data'=>$aData));                                        
        ?>
    </div>
    
    <div class="row more_col">
        <div class="col1">
            <?php echo $form->label($model,'code_no', []); ?>
            <?php echo $form->textField($model,'code_no',array('class'=>'w-160','maxlength'=>20)); ?>            
        </div>
        <div class="col2">
            <?php echo $form->labelEx($model,'car_id'); ?>
            <?php echo $form->dropDownList($model,'car_id', $aCar,array('class'=>'w-200 float_l', 'empty'=>'Select')); ?>
        </div>
        <div class="col3 display_none">
            <?php echo $form->labelEx($model,'isLocationWrong', ['label'=>'Sai vị trí']); ?>
            <?php echo $form->dropDownList($model,'isLocationWrong', $model->getArraySetLocation(),array('class'=>'w-200 float_l', 'empty'=>'Select')); ?>
        </div>
        <div class="col3">
            <?php echo $form->label($model,'store_card_id_gas', ['label'=>'Mã thẻ kho']); ?>
            <?php echo $form->textField($model,'store_card_id_gas',array('class'=>'w-160','maxlength'=>20)); ?>            
        </div>
    </div>
    
    <div class="row more_col">
        <div class="col1">
            <?php echo $form->label($model,'date_from'); ?>
            <?php 
                $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                    'model'=>$model,        
                    'attribute'=>'date_from',
                    'options'=>array(
                        'showAnim'=>'fold',
                        'dateFormat'=> MyFormat::$dateFormatSearch,
//                            'minDate'=> '0',
//                        'maxDate'=> '0',
                        'changeMonth' => true,
                        'changeYear' => true,
                        'showOn' => 'button',
                        'buttonImage'=> Yii::app()->theme->baseUrl.'/admin/images/icon_calendar_r.gif',
                        'buttonImageOnly'=> true,                                
                    ),        
                    'htmlOptions'=>array(
                        'class'=>'w-150',
                        'style'=>'float:left;',                               
                    ),
                ));
            ?>
        </div>
        <div class="col2">
            <?php echo $form->label($model,'date_to'); ?>
            <?php 
                $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                    'model'=>$model,        
                    'attribute'=>'date_to',
                    'options'=>array(
                        'showAnim'=>'fold',
                        'dateFormat'=> MyFormat::$dateFormatSearch,
//                            'minDate'=> '0',
//                        'maxDate'=> '0',
                        'changeMonth' => true,
                        'changeYear' => true,
                        'showOn' => 'button',
                        'buttonImage'=> Yii::app()->theme->baseUrl.'/admin/images/icon_calendar_r.gif',
                        'buttonImageOnly'=> true,                                
                    ),        
                    'htmlOptions'=>array(
                        'class'=>'w-150',
                        'style'=>'float:left;',
                    ),
                ));
            ?>
        </div>
        <div class="col3">
            <?php echo $form->label($model,'isCustomerCreate', []); ?>
            <?php echo $form->dropDownList($model,'isCustomerCreate', CmsFormatter::$yesNoFormat,array('class'=>'w-200', 'empty'=>'Select')); ?>
        </div>
    </div>
    
    <div class="row">
        <?php echo $form->labelEx($model,'obj_id_old', ['label'=>'Tỉnh']); ?>
        <div class="fix-label">
            <?php
               $this->widget('ext.multiselect.JMultiSelect',array(
                     'model'=>$model,
                     'attribute'=>'obj_id_old',
                     'data'=> GasProvince::getArrAll(),
                     // additional javascript options for the MultiSelect plugin
                    'options'=>array('selectedList' => 30,),
                     // additional style
                     'htmlOptions'=>array('style' => 'width: 800px;'),
               ));    
           ?>
        </div>
    </div>

    <div class="row buttons" style="padding-left: 159px;">
        <?php $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType'=>'submit',
            'label'=>'Search',
            'type'=>'null', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
            'size'=>'small', // null, 'large', 'small' or 'mini'
            //'htmlOptions' => array('style' => 'margin-bottom: 10px; float: right;'),
        )); ?>	
    </div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->