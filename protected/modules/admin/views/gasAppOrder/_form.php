<div class="form">
<?php $cRole            = MyFormat::getCurrentRoleId();
$cUid                   = MyFormat::getCurrentUid();
$uidAllowChangePrice    = [GasConst::UID_ADMIN, GasLeave::THUC_NH, GasLeave::PHUONG_PTK];
$uidSetStatusNew        = [GasConst::UID_ADMIN, GasLeave::PHUONG_PTK];
$allowChangePrice       = false;
if(in_array($cUid, $uidAllowChangePrice)){
    $allowChangePrice   = true;
}
$allowChangeCustomer = true;
if($model->type_of_order_car == GasOrders::TYPE_AGENT && in_array($cUid, $uidAllowChangePrice)
//if($model->uid_login_role == ROLE_CUSTOMER || $model->type_of_order_car == GasOrders::TYPE_AGENT 
//        || ($model->type == GasAppOrder::DELIVERY_BY_CAR && $cRole != ROLE_ADMIN)
){
    $allowChangeCustomer   = false;
}

?>
<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'gas-app-order-form',
	'enableAjaxValidation'=>false,
        'htmlOptions' => array('enctype' => 'multipart/form-data'),
)); ?>

    <p class="note">Dòng có dấu <span class="required">*</span> là bắt buộc.</p>
    <?php echo MyFormat::BindNotifyMsg(); ?>
    <div class="row buttons" style="padding-left: 150px;">
        <?php $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType'=>'submit',
            'label'=>$model->isNewRecord ? 'Create' :'Save',
            'type'=>'null', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
            'size'=>'small', // null, 'large', 'small' or 'mini'
            //'htmlOptions' => array('style' => 'margin-bottom: 10px; float: right;'),
        )); ?>	
    </div>
    
    <div class="row f_size_15">
        <?php echo $form->labelEx($model,'status'); ?>
        <?php echo $model->getStatusText().  ' -- '.$model->getStatusDebit(); ?>
    </div>
    <?php if($allowChangePrice && $model->status == GasAppOrder::STATUS_CANCEL): ?>
    <div class="row ">
        <label>&nbsp;</label>
        <?php echo $form->checkBox($model,'setStatusComplete', ['class'=>'float_l']); ?>
        <?php echo $form->labelEx($model,'setStatusComplete',array('class'=>'checkbox_one_label l_padding_10', 'style'=>'width: auto; margin-top:3px;')); ?>
    </div>
    <div class="clr"></div>
    <?php endif; ?>
    
    <div class="row ">
        <?php echo $form->labelEx($model,'date_delivery'); ?>
        <?php 
//            echo $model->getDateDelivery();
            $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                'model'=>$model,        
                'attribute'=>'date_delivery',
                'options'=>array(
                    'showAnim'=>'fold',
                    'dateFormat'=> MyFormat::$dateFormatSearch,
    //               'minDate'=> '0',
//                    'maxDate'=> '0',
                    'changeMonth' => true,
                    'changeYear' => true,
                    'showOn' => 'button',
                    'buttonImage'=> Yii::app()->theme->baseUrl.'/admin/images/icon_calendar_r.gif',
                    'buttonImageOnly'=> true,                                
                ),        
                'htmlOptions'=>array(
                    'class'=>'w-16',
                    'style'=>'height:20px;',
                        'readonly'=>'readonly',
                ),
            ));
        ?>     		
        <?php echo $form->error($model,'date_delivery'); ?>
    </div>
    <div class="row display_none">
        <label>&nbsp;</label>
        <?php echo $form->checkBox($model,'isUpdateMoney', ['class'=>'float_l']); ?>
        <?php echo $form->labelEx($model,'isUpdateMoney',array('class'=>'checkbox_one_label l_padding_10','label'=>'Cập nhật tiền của đơn hàng', 'style'=>'width: auto; margin-top:3px;')); ?>
    </div>
    <div class="clr"></div>
    <?php if($cRole == ROLE_ADMIN): ?>
    <div class="row ">
        <label>&nbsp;</label>
        <?php echo $form->checkBox($model,'setNotDebit', ['class'=>'float_l']); ?>
        <?php echo $form->labelEx($model,'setNotDebit',array('class'=>'checkbox_one_label l_padding_10', 'style'=>'width: auto; margin-top:3px;')); ?>
    </div>
    <div class="clr"></div>
    <?php endif; ?>
    <?php if($model->canDoSetStatusNew() && in_array($cUid, $uidSetStatusNew)): ?>
        <div class="row">
            <label>&nbsp;</label>
            <div class="add_new_item float_l hight_light item_b" style="padding-left: 0;">Sử dụng Cập nhật trạng thái mới của đơn hàng khi nhân viên hoàn thành nhầm đơn hàng, và muốn đưa về trạng thái mới để NV khác nhận. Nếu chữa rõ vui lòng gọi Dũng IT để được hỗ trợ</div>
        </div>
        <div class="row ">
            <label>&nbsp;</label>
            <?php echo $form->checkBox($model,'setStatusNew', ['class'=>'float_l']); ?>
            <?php echo $form->labelEx($model,'setStatusNew',array('class'=>'checkbox_one_label l_padding_10', 'style'=>'width: auto; margin-top:3px;')); ?>
        </div>
        <div class="clr"></div>
    <?php endif; ?>
    <div class="row ">
        <label>Đơn giá</label>
        <?php echo $model->getPriceString(); ?>
    </div><br>
    <?php if($model->pay_back > 0): ?>
    <div class="row ">
        <label>Số KG thiếu đầu vào</label>
        <?php echo ActiveRecord::formatCurrency($model->pay_back).' kg = '.$model->getPayBack(true); ?>
    </div><br>
    <?php endif; ?>
    <?php if($allowChangePrice): ?>
    <div class="row ">
        <label>&nbsp;</label>
        <?php echo $form->checkBox($model,'allowChangePrice', ['class'=>'float_l']); ?>
        <?php echo $form->labelEx($model,'allowChangePrice',array('class'=>'checkbox_one_label l_padding_10', 'style'=>'width: auto; margin-top:3px;')); ?>
    </div>
    <div class="clr"></div>
    <?php endif; ?>
    
    <div class="row f_size_15">
        <?php echo $form->labelEx($model,'total_gas'); ?>
        <?php echo ActiveRecord::formatCurrency($model->total_gas); ?>
    </div>
    <div class="clr"></div>
    <div class="row f_size_15">
        <?php echo $form->labelEx($model,'total_gas_du'); ?>
        <?php echo $model->totalRemain > 0 ? ( ActiveRecord::formatCurrency($model->totalRemain).' kg = '.ActiveRecord::formatCurrency($model->total_gas_du) ) : ''; ?>
    </div>
    <div class="clr"></div>
    
    <?php // if($allowChangePrice): ?>
    <?php if(1): ?>
    <div class="row ">
        <label>&nbsp;</label>
        <?php echo $form->checkBox($model,'webSetDebit', ['class'=>'float_l']); ?>
        <?php echo $form->labelEx($model,'webSetDebit',array('class'=>'checkbox_one_label l_padding_10', 'style'=>'width: auto; margin-top:3px;')); ?>
    </div>
    <?php endif; ?>
    <div class="clr"></div>
    
    <div class="row">
        <?php echo $form->labelEx($model,'discount'); ?>
        <?php echo $form->textField($model,'discount',array('class'=>'w-150 f_size_15 ad_fix_currency item_r','maxlength'=>100)); ?>
        <?php echo $form->error($model,'discount'); ?>
    </div>
    <?php if($allowChangePrice): ?>
    <div class="row">
        <?php echo $form->labelEx($model,'pay_back', ['label'=>'Số KG thiếu đầu vào']); ?>
        <?php echo $form->textField($model,'pay_back',array('class'=>'w-150 f_size_15 item_r','maxlength'=>100)); ?>
        <?php echo $form->error($model,'pay_back'); ?>
    </div>
    <?php endif; ?>
    
    <div class="row f_size_15">
        <?php echo $form->labelEx($model,'grand_total'); ?>
        <b><?php echo ActiveRecord::formatCurrency($model->grand_total); ?></b>
    </div>
    <div class="clr"></div>
    <?php // if($model->isOrderCarOfCustomer()): ?>
    <?php if(1): ?>
    <div class="row">
        <?php echo $form->labelEx($model,'receipts_no', []); ?>
        <?php echo $form->textField($model,'receipts_no',array('class'=>'w-150 f_size_15','maxlength'=>8)); ?>
        <?php echo $form->error($model,'receipts_no'); ?>
    </div>
    <?php endif; ?>
    <div class="clr"></div>
    <div class="row ">
        <label class="w-150">&nbsp;</label>
        <?php include '_form_tab.php'; ?>
    </div>
    <div class="clr"></div><br>
    
    <div class="row buttons" style="padding-left: 150px;">
        <?php $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType'=>'submit',
            'label'=>$model->isNewRecord ? 'Create' :'Save',
            'type'=>'null', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
            'size'=>'small', // null, 'large', 'small' or 'mini'
            //'htmlOptions' => array('style' => 'margin-bottom: 10px; float: right;'),
        )); ?>	
    </div>

    <div class="row">
        <label>Ghi chú</label>
        <div class="f_size_15" style="padding-left: 141px;"><?php $model->mAppUserLogin = new Users(); echo $model->getNoteCustomerApp();?></div>
    </div>
    <div class="clr"></div>
    <div class="row selectCustomer">
    <?php echo $form->labelEx($model,'customer_id'); ?>
    <?php echo $form->hiddenField($model,'customer_id', array('class'=>'')); ?>
    <?php
            // 1. limit search kh của sale
            $url = Yii::app()->createAbsoluteUrl('admin/ajax/search_user_by_code');
            // widget auto complete search user customer and supplier
            $aData = array(
                'model'=>$model,
                'field_customer_id'=>'customer_id',
                'url'=> $url,
                'name_relation_user'=>'rCustomer',
                'ClassAdd' => 'w-400',
                'field_autocomplete_name' => 'autocomplete_name',
            );
            $this->widget('ext.SpaAutocompleteCustomer.SpaAutocompleteCustomer',
                array('data'=>$aData));
            ?>
    </div>
    
    <div class="row selectAgent">
    <?php echo $form->labelEx($model,'agent_id'); ?>
    <?php echo $form->hiddenField($model,'agent_id', array('class'=>'')); ?>
    <?php
            // 1. limit search kh của sale
            $url = Yii::app()->createAbsoluteUrl('admin/ajax/search_by_role', array('role'=> ROLE_AGENT));
            // widget auto complete search user customer and supplier
            $aData = array(
                'model'=>$model,
                'field_customer_id'=>'agent_id',
                'url'=> $url,
                'name_relation_user'=>'rAgent',
                'ClassAdd' => 'w-400',
                'field_autocomplete_name' => 'autocomplete_name_1',
                'placeholder'=>'Nhập mã hoặc tên đại lý',
            );
            $this->widget('ext.SpaAutocompleteCustomer.SpaAutocompleteCustomer',
                array('data'=>$aData));
            ?>
    </div>
    <?php $model->setListdataPhuXe();?>
    <div class="row">
        <label>Tài xế, phụ xe</label>
        <div class="f_size_15" style="padding-left: 141px;"><?php echo $model->getPhuXe();?></div>
    </div>
    <div class="clr"></div>
    <div class="row selectAgent">
        <label>Lịch sử giao nhận</label>
        <div class="f_size_15" style="padding-left: 141px;"><?php echo $model->getEventLog();?></div>
    </div>
    
<?php $this->endWidget(); ?>
</div><!-- form -->
<?php if($cRole == ROLE_ADMIN): ?>
        <?php echo $model->json.'<hr>'.$model->json_info;?>
<?php endif; ?>

<table class="TrTemplateGas display_none">
    <tbody>
        <tr class="materials_row">
            <td class="item_c order_no"></td>
            <td class="item_c materials_code"></td>
            <td class="materials_name"></td>
            <td class="item_c materials_unit"></td>
            <td class="item_c materials_qty"></td>
            <td class="item_c">
                <input type="text" name="qty_real[]" class="item_c w-50 number_only_v1 materials_qty_real" maxlength="6" value="1">
                <input class="materials_type_id" type="hidden" name="materials_type_id[]" value="">
                <input class="materials_id" type="hidden" name="materials_id[]" value="">
                <input class="materials_qty_hide" type="hidden" name="qty[]" value="">
                <input class="materials_seri" type="hidden" name="seri[]" value="">
                <input class="materials_kg_empty" type="hidden" name="kg_empty[]" value="">
                <input class="materials_kg_has_gas" type="hidden" name="kg_has_gas[]" value="">
            </td>
            <td class="item_r"><input class="materials_price ad_fix_currency item_r w-80" type="text" name="price[]" value=""></td>
            <td class="item_r"></td>
            <td class="item_c last"><span class="remove_icon_only"></span></td>
        </tr>
    </tbody>
</table>
<table class="TrTemplateVo display_none">
    <tbody>
        <tr class="materials_row">
            <td class="item_c order_no"></td>
            <td class="item_c materials_code"></td>
            <td class="materials_name"></td>
            <td class="item_c">
                <input type="text" class="item_c w-30 number_only_v1 materials_qty" name="qty[]" maxlength="6" value="1" readonly="1">
                <input class="materials_qty_real" type="hidden" name="qty_real[]" value="">
                <input class="materials_type_id" type="hidden" name="materials_type_id[]" value="">
                <input class="materials_id" type="hidden" name="materials_id[]" value="">
            </td>
            <td class="item_c">
                <input class="materials_seri number_only_v1 item_c w-50" type="text" name="seri[]" value=""  maxlength="5">
            </td>
            <td class="item_c">
                <input class="materials_kg_empty number_only_v1 item_c w-50" type="text" name="kg_empty[]" value=""  maxlength="6">
            </td>
            <td class="item_c">
                <input class="materials_kg_has_gas number_only_v1 item_c w-50" type="text" name="kg_has_gas[]" value=""  maxlength="6">
            </td>
            <td class="item_r"></td>
            <td class="item_c last"><span class="remove_icon_only"></span></td>
        </tr>
    </tbody>
</table>

<?php CronUpdate::devPrintDbData($model);?>
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/autoNumeric-master/autoNumeric.js"></script>
<script>
    $(document).ready(function(){
        $('.form').find('button:submit').click(function(){
            $.blockUI({ overlayCSS: { backgroundColor: '#fff' } }); 
        });
        <?php if(!$allowChangeCustomer): ?>
        $('.selectCustomer').find('#GasAppOrder_autocomplete_name').remove();
        $('.selectCustomer').find('.remove_row_item').remove();
        <?php endif; ?>
        
        $('.selectAgent').find('#GasAppOrder_autocomplete_name_1').remove();
        $('.selectAgent').find('.remove_row_item').remove();
//        $( "#tabs" ).tabs({ active: 1 });
        $( "#tabs" ).tabs({ active: 0 });
        
        materialsGas = <?php echo CacheSession::getListMaterialJson(); ?>;
        materialsVo = <?php echo CacheSession::getListMaterialJsonVo(); ?>;
        fnInitAutocomplete(materialsGas, '.MaterialsGas');
        fnInitAutocomplete(materialsVo, '.MaterialsVo');
        fnBindRemoveIcon();
        fnInitInputCurrency();
    });
    
    /**
    * @Author: ANH DUNG May 13, 2017
    * @Todo: init autocomplete
    */
    function fnInitAutocomplete(availableMaterials, className){
        var WrapOneTab = $(className).closest('.WrapOneTab');
        $( className ).autocomplete({
            source: availableMaterials,
            close: function( event, ui ) { $(className).val(''); },
            select: function( event, ui ) {
                var class_item = 'materials_row_'+ui.item.id;
                if($('.'+class_item).size()<1){
                    if(className == '.MaterialsGas'){
                        fnBuildRowMaterialGas(ui, WrapOneTab, class_item);
                    }
                }
                if(className == '.MaterialsVo'){
                    fnBuildRowMaterialVo(ui, WrapOneTab, class_item);
                }
            }
        });
    }
    
    function fnBuildRowMaterialGas(ui, WrapOneTab, class_item){
        var TrCopy = $('.TrTemplateGas').find('.materials_row:first').clone();
        TrCopy.find('.materials_name').addClass(class_item);
        TrCopy.find('.materials_code').text(ui.item.materials_no);
        TrCopy.find('.materials_name').text(ui.item.name);
        TrCopy.find('.materials_unit').text(ui.item.unit);
        TrCopy.find('.materials_qty').text(1);
        TrCopy.find('.materials_qty_real').val(1);
        
        TrCopy.find('.materials_type_id').val(ui.item.materials_type_id);
        TrCopy.find('.materials_id').val(ui.item.id);
        TrCopy.find('.materials_qty_hide').val(1);
        
        WrapOneTab.find('.materials_table tbody').append(TrCopy);
        fnRefreshOrderNumber();
        fnInitInputCurrency();
    }
    
    function fnBuildRowMaterialVo(ui, WrapOneTab, class_item){
        var TrCopy = $('.TrTemplateVo').find('.materials_row:first').clone();
        TrCopy.find('.materials_name').addClass(class_item);
        TrCopy.find('.materials_code').text(ui.item.materials_no);
        TrCopy.find('.materials_name').text(ui.item.name);
        
        TrCopy.find('.materials_type_id').val(ui.item.materials_type_id);
        TrCopy.find('.materials_id').val(ui.item.id);
//        alert(ui.item.materials_type_id);
        if(ui.item.materials_type_id == (<?php echo GasMaterialsType::MATERIAL_VO_12;?>) || ui.item.materials_type_id == (<?php echo GasMaterialsType::MATERIAL_VO_6;?>) 
            ||ui.item.materials_type_id == (<?php echo GasMaterialsType::MATERIAL_VO_4;?>)
        ){
            TrCopy.find('.materials_qty').removeAttr("readonly");
        }

        WrapOneTab.find('.materials_table tbody').append(TrCopy);
        fnRefreshOrderNumber();
    }
    
    
</script>
