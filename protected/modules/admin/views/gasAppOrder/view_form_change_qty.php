<?php 
$display_none = 'display_none';
if($model->change_qty){
    $display_none = '';
}
?>
<div class="BoxChangeQty <?php echo $display_none;?>">
<div class="row">
    <?php echo $form->labelEx($model,'b50'); ?>
    <?php echo $form->textField($model,'b50',array('class'=>'w-100 number_only_v1','maxlength'=>2)); ?>
    <?php echo $form->error($model,'b50'); ?>
</div>
<div class="row">
    <?php echo $form->labelEx($model,'b45'); ?>
    <?php echo $form->textField($model,'b45',array('class'=>'w-100 number_only_v1','maxlength'=>2)); ?>
    <?php echo $form->error($model,'b45'); ?>
</div>
<div class="row">
    <?php echo $form->labelEx($model,'b12'); ?>
    <?php echo $form->textField($model,'b12',array('class'=>'w-100 number_only_v1','maxlength'=>2)); ?>
    <?php echo $form->error($model,'b12'); ?>
</div>
<div class="row">
    <?php echo $form->labelEx($model,'b6'); ?>
    <?php echo $form->textField($model,'b6',array('class'=>'w-100 number_only_v1','maxlength'=>2)); ?>
    <?php echo $form->error($model,'b6'); ?>
    <?php echo $form->error($model,'change_qty'); ?>
</div>
    
</div>
