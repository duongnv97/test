<div class="form">
    <?php
        $LinkNormal = Yii::app()->createAbsoluteUrl('admin/gasAppOrder/index');
        $LinkWait   = Yii::app()->createAbsoluteUrl('admin/gasAppOrder/index', array( 'is_confirm'=> GasAppOrder::STATUS_CONFIRM));
    ?>
    <h1><?php echo $this->pluralTitle;?>
        <a class='btn_cancel f_size_14 <?php echo (!isset($_GET['is_confirm'])) ? "active":"";?>' href="<?php echo $LinkNormal;?>">Mới</a>
        <a class='btn_cancel f_size_14 <?php echo isset($_GET['is_confirm']) && $_GET['is_confirm']==GasAppOrder::STATUS_CONFIRM ? "active":"";?>' href="<?php echo $LinkWait;?>">Đã xác nhận</a>
    </h1> 
</div>