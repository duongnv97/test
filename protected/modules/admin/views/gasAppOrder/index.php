<?php
$this->breadcrumbs=array(
	$this->singleTitle,
);

$menus=array(
    array('label'=>"Create $this->singleTitle", 'url'=>array('create')),
);
//$aUidAllow  = [GasConst::UID_THUAN_NH, GasConst::UID_THUY_TT, 1163847, GasConst::UID_NHI_DT, GasLeave::PHUONG_PTK, 867768, GasConst::UID_ADMIN, GasConst::UID_PHUONG_NT, GasConst::UID_NGOC_PT, GasConst::UID_NGAN_DTM];
$mCronExcel = new CronExcel();
if($mCronExcel->canDownload()):
    $menus[] = ['label'=> 'Xuất Excel Danh Sách Hiện Tại',
                'url'=>array('index', 'ExportExcel'=>1), 
                'htmlOptions'=>array('class'=>'export_excel ','label'=>'Xuất Excel'),
    ];
endif;

$this->menu= ControllerActionsName::createMenusRoles($menus, $actions);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('gas-app-order-grid', {
                url : $(this).attr('action'),
		data: $(this).serialize()
	});
	return false;
});
");

Yii::app()->clientScript->registerScript('ajaxupdate', "
$('#gas-app-order-grid a.ajaxupdate').live('click', function() {
    $.fn.yiiGridView.update('gas-app-order-grid', {
        type: 'POST',
        url: $(this).attr('href'),
        success: function() {
            $.fn.yiiGridView.update('gas-app-order-grid');
        }
    });
    return false;
});
");
?>

<!--<h1>Danh Sách <?php echo $this->pluralTitle; ?></h1>-->

<?php include 'index_button.php'; ?>
<?php // $this->widget('ListCronExcelWidget', array('model'=>$model)); ?>
<?php echo CHtml::link('Tìm Kiếm Nâng Cao','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->
    <?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'gas-app-order-grid',
	'dataProvider' => $model->search(),
        'afterAjaxUpdate'=>'function(id, data){ fnUpdateColorbox();}',
        'template'=>'{pager}{summary}{items}{pager}{summary}',
//    'itemsCssClass' => 'items custom_here',
//    'htmlOptions'=> array('class'=>'grid-view custom_here'),
        'pager' => array(
            'maxButtonCount'=>  CmsFormatter::$PAGE_MAX_BUTTON,
        ),
	'enableSorting' => false,
	//'filter'=>$model,
	'columns'=>array(
        array(
            'header' => 'S/N',
            'type' => 'raw',
            'value' => '$this->grid->dataProvider->pagination->currentPage * $this->grid->dataProvider->pagination->pageSize + ($row+1)',
            'headerHtmlOptions' => array('width' => '10px','style' => 'text-align:center;'),
            'htmlOptions' => array('style' => 'text-align:center;')
        ),
            array(
                'name' =>'date_delivery',
                'type' =>'raw',
                'value' =>'$data->getDateDeliveryWeb().$data->getDisplayErrors(). "<br>" .$data->getUrlUpdateCustomer()',
                'htmlOptions' => array('class' => 'w-50'),
            ),
            array(
                'name' =>'code_no',
                'type' =>'raw',
                'value' =>'$data->code_no."<br>".$data->getArrayDeliveryText()',
//                'value' =>'$data->code_no."<br>".$data->getArrayDeliveryText().$data->getStorecardNo()',
                'htmlOptions' => array('style' => 'text-align:center;')
            ),
            array(
                'name' =>'agent_id',
                'type' =>'raw',
                'value' =>'$data->mapJsonOnly().$data->getAgentDisplayGrid()."<br>".$data->getDeliveryTimerGrid()',
            ),
            array(
                'name' =>'customer_id',
                'type' =>'raw',
//                'value' =>'"<b>".$data->getCustomerName()."</b><br>".$data->getCustomerAddress()."<br><b>".$data->getCustomerPhoneOrderGrid()."</b> - ".$data->getNoteCustomer()."<br>".$data->getNoteEmployeeWebShow().$data->getFileOnly()',
                'value' =>'$data->getCustomerInfoV1()."<br><b>".$data->getCustomerPhoneOrderGrid()."</b> - ".$data->getNoteCustomer()."<br>".$data->getNoteEmployeeWebShow().$data->getFileOnly()',
            ),
            array(
                'header' => 'Chi tiết',
                'type' =>'raw',
//                'value' =>'$data->getJsonFieldOneDecode("name_gas", "json_info", "baseArrayJsonDecodeV1")."<br>".$data->getStringGas()',
                'value' =>'$data->setListdataPhuXe().$data->getViewDetail()."<br>".$data->getPhuXeWeb()',
            ),
            array(
                'name' =>'status',
                'type' =>'raw',
                'htmlOptions' => array('style' => 'text-align:center;'),
                'value' =>'$data->getStatusText()."<br><b>".$data->getReasonFalse()."</b>"."<br>".$data->getTimeOverWebView()',
            ),
            array(
                'name' =>'created_date',
                'type' =>'raw',
                'value' =>'$data->getCreatedDate("d/m/Y H:i:s"). "<br>". $data->getInfoCreatedByOnGrid()."<br>".$data->getNameCar()',
                'htmlOptions' => array('class' => 'w-50'),
            ),
            array(
                'header' => 'Actions',
                'class'=>'CButtonColumn',
                'template'=> ControllerActionsName::createIndexButtonRoles($actions,array('createStock','view','update','delete')),
                'buttons'=>array(
                    'createStock'=>array(
                        'label'=>'Cập nhật STT',
                        'imageUrl'=>Yii::app()->theme->baseUrl . '/images/add1.png',
                        'options'=>array('class'=>'createDetail'),
                        'url'=>'Yii::app()->createAbsoluteUrl("admin/gasAppOrder/createStock",
                            array("id"=>$data->id) )',
                         'visible' => '$data->canCreateStock()',

                    ),
                    'update' => array(
                        'visible' => '$data->canUpdateWeb()',
                    ),
                    'delete'=>array(
                        'visible'=> 'GasCheck::canDeleteData($data)',
                    ),
                ),
            ),
	),
)); ?>

<script>
$(document).ready(function() {
    fnUpdateColorbox();
});

function fnUpdateColorbox(){   
    fixTargetBlank();
    $(".view").colorbox({
        iframe: true,
        innerHeight: '1000',
        innerWidth: '900', close: "<span title='close'>close</span>"
    });
    $(".gallery").colorbox({iframe:true,innerHeight:'1800', innerWidth: '1100',close: "<span title='close'>close</span>"});
    fnShowhighLightTr();
}
</script>