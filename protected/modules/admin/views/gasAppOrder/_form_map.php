<?php 
    $mEventComplete = new TransactionEvent();
    $mEventComplete->transaction_history_id = $model->id;
    $mEventComplete->type                   = TransactionEvent::TYPE_BO_MOI;
    $mEventComplete =  $mEventComplete->getOrderComplete();
    $urlLocationCustomer    = Yii::app()->createAbsoluteUrl('admin/gasAppOrder/view', ['id'=>$model->id, 'SetLocationCustomer'=>1]);
    $urlLocationWrong       = Yii::app()->createAbsoluteUrl('admin/gasAppOrder/view', ['id'=>$model->id, 'SetLocationCustomer'=>1, 'SetWrongLocation'=>1]);
?>
<?php if($mEventComplete && $model->status == GasAppOrder::STATUS_COMPPLETE): ?>
<h3 class='title-info'>Vị trí hoàn thành đơn hàng</h3>
<?php // if(GasConst::canUpdateLocation()): ?>
<?php if(0): ?>
    <a class='btn_cancel LocationSet' href='<?php echo $urlLocationCustomer;?>'>Cập nhật tọa độ khách hàng</a>
    
    <div class="form item_r">
        <a class='btn_cancel LocationSet' href='<?php echo $urlLocationWrong;?>'>Sai vị trí</a>
    </div>
<?php endif; ?>
<div class="clr"></div><br>
<?php
$tmp    = explode(",", $mEventComplete->google_map);
$info   = "<b>{$model->getCustomerName()}</b><br> {$model->getCustomerAddress()}";
$aDataChart     = [];
$aDataChart[]   = [$info, $tmp[0], $tmp[1], 0];
$js_array       = json_encode($aDataChart);
// echo "var javascript_array = ". $js_array . ";\n";
?>
<!--http://stackoverflow.com/questions/3059044/google-maps-js-api-v3-simple-multiple-marker-example
https://developers.google.com/maps/documentation/javascript/markers#animated
https://developers.google.com/maps/documentation/javascript/examples/infowindow-simple-max
-->
<script src="http://maps.google.com/maps/api/js?key=AIzaSyCfsbfnHgTh4ODoXLXFfEFhHskDhnRVtjQ" type="text/javascript"></script>
<div id="map" style="width: 100%; height: 400px;"></div>
<script type="text/javascript">
    var locations = <?php echo $js_array;?>;
    var map = new google.maps.Map(document.getElementById('map'), {
      zoom: 20,
      center: new google.maps.LatLng(<?php echo $tmp[0];?>, <?php echo $tmp[1];?>),
      mapTypeId: google.maps.MapTypeId.ROADMAP
    });

    var infowindow = new google.maps.InfoWindow();

    var marker, i;
//    var image = 'http://daukhi.huongminhgroup.com/apple-touch-icon.png';

    for (i = 0; i < locations.length; i++) {
        marker = new google.maps.Marker({
          position: new google.maps.LatLng(locations[i][1], locations[i][2]),
          map: map
  //        icon: image
        });

        google.maps.event.addListener(marker, 'click', (function(marker, i) {
          return function() {
            infowindow.setContent(locations[i][0]);
            infowindow.open(map, marker);
          }
        })(marker, i));
      
        var infowindow2 = new google.maps.InfoWindow({
              content: locations[i][0],
        });
        infowindow2.open(map, marker);
    }

    $(function(){
        $('.LocationSet').click(function(){
            $.blockUI({ overlayCSS: { backgroundColor: '#fff' } }); 
            return true;
        });
    });

  </script>
  
<?php endif; ?>