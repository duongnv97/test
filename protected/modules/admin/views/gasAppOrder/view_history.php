<?php 
    $mStoreCard = new GasStoreCard();
    $mStoreCard->customer_id = $model->customer_id;
    $aStoreCard = $mStoreCard->windowGetHistory();
    $mAppCache  = new AppCache();
    $listdataAgent = $mAppCache->getAgentListdata();
    $aMaterial  = CacheSession::getListMaterial();
?>
<?php foreach ($aStoreCard as $itemStoreCard): ?>
<?php 
    $sDate  = MyFormat::dateConverYmdToDmy($itemStoreCard['created_date'], "d-m-Y");
    $sAgent = isset($listdataAgent[$itemStoreCard['agent_id']]) ? $listdataAgent[$itemStoreCard['agent_id']] : '';
    $sGas   = '';
    foreach($itemStoreCard['order_detail'] as $item){
        $nameMaterial = isset($aMaterial[$item['materials_id']]) ? $aMaterial[$item['materials_id']] : '';
        $sGas .= '<br>&nbsp;&nbsp;&nbsp;&nbsp;  SL:<b>'.  ActiveRecord::formatCurrency($item['qty']).'</b> - '.$nameMaterial;
    }
?>
<p><?php echo $sDate.': '.$sAgent;?><span style="font-weight: normal;"><?php echo $sGas;?></span></p>
<?php endforeach; ?>