<?php 
$model->setListdataPhuXe();
$this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
            array(
                'name' =>'code_no',
                'type' =>'raw',
                'value' =>$model->code_no."<br>".$model->getArrayDeliveryText(),
            ),
            array(
                'name' =>'date_delivery',
                'type' =>'raw',
                'value' =>$model->getDateDelivery(),
            ),
            array(
                'name' =>'delivery_timer',
                'type' =>'raw',
                'value' =>$model->getDeliveryTimerGrid(),
            ),
            array(
                'name' =>'agent_id',
                'type' =>'raw',
                'value' =>$model->getAgentDisplayGrid(),
            ),
            array(
                'name' =>'customer_id',
                'type' =>'raw',
                'value' =>$model->getCustomerInfoV1(),
            ),
            array(
                'label' =>'Điện thoại người đặt',
                'type' =>'raw',
                'value' =>$model->getCustomerPhoneOrder(),
            ),
            array(
                'label' =>'Người đặt ghi chú',
                'type' =>'raw',
                'value' =>$model->getNoteCustomer(),
            ),
            array(
                'label' =>'Điều phối ghi chú',
                'type' =>'raw',
                'value' =>$model->getNoteEmployeeWebShow(),
            ),
            array(
                'label' => 'Chi tiết',
                'name' =>'customer_id',
                'type' =>'raw',
                'value' =>$model->getPriceString().'<br>'.$model->getViewDetail(),
            ),
            array(
                'name' =>'status',
                'type' =>'raw',
                'htmlOptions' => array('style' => 'text-align:center;'),
                'value' =>$model->getStatusText(),
            ),
            array(
                'label' =>'Người xác nhận',
                'type' =>'raw',
                'value' =>$model->getConfirmBy(),
            ),
            array(
                'name' =>'order_detail_note',
                'value' =>$model->getOrderDetailNote(),
            ),
            array(
                'name' =>'order_detail_note_2',
                'value' =>$model->getOrderDetailNote2(),
            ),
            array(
                'label' =>'Phụ xe',
                'value' =>$model->getPhuXe(),
            ),
            array(
                'name' =>'created_date',
                'type' =>'raw',
                'value' => $model->getCreatedDate("d/m/Y H:i:s"). "<br>".$model->getUserInfo("rUserLogin", "first_name"),
            ),
            array(
                'label' =>'Lịch sử giao nhận',
                'type' =>'raw',
                'value' => $model->getEventLog(),
            ),
	),
)); ?>
<div class="clr"></div>
<?php if($model->canConfirm()): ?>
    <?php include 'view_form_confirm.php'; ?>
<?php endif; ?>
<div class="clr"></div>
<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'gas-app-order-form',
	'enableAjaxValidation'=>false,
        'htmlOptions' => array('enctype' => 'multipart/form-data'),
)); ?>
    <?php include '_form_tab.php'; ?>
<?php $this->endWidget(); ?>
<?php Yii::app()->clientScript->registerCoreScript('jquery.ui'); ?>
<?php Yii::app()->clientScript->registerCssFile(Yii::app()->clientScript->getCoreScriptUrl().'/jui/css/base/jquery-ui.css'); ?>
<script>
    $(document).ready(function(){
        $( "#tabs" ).tabs({ active: 0 });
    });
</script>
<div class="clr"></div>