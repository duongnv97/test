<?php
$this->breadcrumbs = array(
    $this->pageTitle,
);
$BINH_6         = GasMaterialsType::MATERIAL_BINH_6KG;
$BINH_12        = GasMaterialsType::MATERIAL_BINH_12KG;
$BINH_45        = GasMaterialsType::MATERIAL_BINH_45KG;
$BINH_50        = GasMaterialsType::MATERIAL_BINH_50KG;
$mAppCache      = new AppCache();
$aAgent         = $mAppCache->getAgent();
$stt            = 0;
$sumBinh6       = $sumBinh12 = $sumBinh45 =$sumBinh50 = 0;
$sumQtyFinal    = 0;
?>
<h1><?php echo $this->pageTitle;?></h1>

<div class="search-form" style="">
    <?php
    $this->renderPartial('reportRevenue/_search', array(
        'model' => $model,
    ));
    ?>
</div>
<?php if(!empty($aData)):
$aRes =$aData['aRes'];
$aAgentAmount =$aData['aAgentAmount'];
$aAgentRemain =$aData['aAgentRemain'];
$aAgentRemainAmount = $aData['aAgentRemainAmount'];
?>
<div>
    <table class="tb hm_table f_size_15">
        <thead>
            <tr>
                <th class="item_c">#</th>
                <th class="item_b w-220">Đại lý</th>
                <th class="item_b">Doanh thu <br>(chưa trừ gas dư)</th>
                <th class="item_b">Bình 6</th>
                <th class="item_b">Bình 12</th>
                <th class="item_b">Bình 45</th>
                <th class="item_b">Bình 50</th>
                <th class="item_b">Gas dư (Kg)</th>
                <th class="item_b">Tiền Gas dư</th>
            </tr>
        </thead>
        <tbody>
        <?php foreach ($aAgentAmount as $agent_id => $amount): 
            $stt++;
        ?>
        
        <tr class="h_30">
            <td><?php echo $stt; ?></td>
            <td><?php echo $aAgent[$agent_id] ? $aAgent[$agent_id]['first_name'] : ''; ?></td>
            <td class="item_r item_b"><?php echo $amount != 0 ? ActiveRecord::formatCurrency($amount) : ''; ?></td>
            <td class="item_r"><?php echo isset($aRes[$agent_id][$BINH_6]) ? ActiveRecord::formatCurrency($aRes[$agent_id][$BINH_6]['qty']) : ''; ?></td>
            <td class="item_r"><?php echo isset($aRes[$agent_id][$BINH_12]) ? ActiveRecord::formatCurrency($aRes[$agent_id][$BINH_12]['qty']) : ''; ?></td>
            <td class="item_r"><?php echo isset($aRes[$agent_id][$BINH_45]) ? ActiveRecord::formatCurrency($aRes[$agent_id][$BINH_45]['qty']) : ''; ?></td>
            <td class="item_r"><?php echo isset($aRes[$agent_id][$BINH_50]) ? ActiveRecord::formatCurrency($aRes[$agent_id][$BINH_50]['qty']) : ''; ?></td>
            <td class="item_r"><?php echo (isset($aAgentRemain[$agent_id]) && $aAgentRemain[$agent_id] != 0) ? ActiveRecord::formatCurrency($aAgentRemain[$agent_id]) : ''; ?></td>
            <td class="item_r"><?php echo isset($aAgentRemainAmount[$agent_id]) ? ActiveRecord::formatCurrency($aAgentRemainAmount[$agent_id]) : ''; ?></td>
        </tr>
        <?php 
        $sumBinh6 += isset($aRes[$agent_id][$BINH_6]) ? $aRes[$agent_id][$BINH_6]['qty']: 0;
        $sumBinh12 += isset($aRes[$agent_id][$BINH_12]) ? $aRes[$agent_id][$BINH_12]['qty']: 0;
        $sumBinh45 += isset($aRes[$agent_id][$BINH_45]) ? $aRes[$agent_id][$BINH_45]['qty']: 0;
        $sumBinh50 += isset($aRes[$agent_id][$BINH_50]) ? $aRes[$agent_id][$BINH_50]['qty']: 0;
        endforeach; ?>
        <tr class="h_30">
            <td></td>
            <td class="item_r item_b">Tổng</td>
            <td class="item_r item_b"><?php echo is_array($aAgentAmount) != 0 ? ActiveRecord::formatCurrency(array_sum($aAgentAmount)) : ''; ?></td>
            <td class="item_r item_b"><?php echo $sumBinh6 != 0 ? ActiveRecord::formatCurrency($sumBinh6): ''; ?></td>
            <td class="item_r item_b"><?php echo $sumBinh12 != 0 ? ActiveRecord::formatCurrency($sumBinh12): ''; ?></td>
            <td class="item_r item_b"><?php echo $sumBinh45 != 0 ? ActiveRecord::formatCurrency($sumBinh45): ''; ?></td>
            <td class="item_r item_b"><?php echo $sumBinh50 != 0 ? ActiveRecord::formatCurrency($sumBinh50) : ''; ?></td>
            <td class="item_r item_b"><?php echo is_array($aAgentRemain) != 0 ? ActiveRecord::formatCurrency(array_sum($aAgentRemain)) : ''; ?></td>
            <td class="item_r item_b"><?php echo is_array($aAgentRemainAmount) != 0 ? ActiveRecord::formatCurrency(array_sum($aAgentRemainAmount)) : ''; ?></td>
        </tr>
        </tbody>
    </table>
</div>
<?php endif; ?>
<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/print-store-card.css" />
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/jquery.floatThead.min.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/underscore-min.js"></script>    
<script>
    $(document).ready(function () {
        $('.hm_table').floatThead(); // lỗi mất border
    });
</script>

