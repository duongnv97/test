<?php 
$index = 1;
$mRemain = new GasRemain();
$readonlySeri = $model->isOrderAgent() ? 'readonly="1"' : '';

?>
<div class="clearfix WrapOneTab" id="tabs-2" tab_index="2">
    <div class="clr"></div>
    <div class="row">
        <?php echo $form->labelEx($model,'obj_detail_id_old', ['label'=>'Nhập vỏ']); ?>
        <?php echo $form->textField($model,'obj_detail_id_old',array('class'=>'MaterialsVo w-300','placeholder'=>'Nhập mã vật tư hoặc tên vật tư')); ?>
        <?php echo $form->error($model,'obj_detail_id_old'); ?>
        <div class="clr"></div>    		
    </div>
    <table class="materials_table hm_table" style="width: 100%;">
        <thead>
            <tr class="f_size_11">
                <th class="item_c w-20">#</th>
                <th class="item_code item_c w-80">Mã</th>
                <th class="item_name item_c">Tên</th>
                <th class="w-30 item_c">SL</th>
                <th class="w-50 item_c"><?php echo $mRemain->getAttributeLabel('seri');?></th>
                <th class="w-40 item_c">KL Vỏ</th>
                <th class="w-40 item_c">KL Cân</th>
                <th class="w-40 item_c">Dư</th>
                <th class="last item_c w-20">Xóa</th>
            </tr>
        </thead>
        <tbody>
        <?php foreach ($aInfoVo as $key => $detail): ?>
        <?php 
            $materials_id   = $detail['materials_id'];
            if(!isset($aMaterial[$materials_id])){
                continue ;
            }
            $code           = $aMaterial[$materials_id]['materials_no'];
            $unit           = $aMaterial[$materials_id]['unit'];
            $remain         = ($detail['kg_has_gas']*1 - $detail['kg_empty']*1)*1;
            $readonly       = ''; $classFocus = '';
            if(in_array($detail['materials_type_id'], GasMaterialsType::$ARR_VO_45KG)){
                $readonly   = 'readonly="1"';
                $classFocus  = 'item_b';
            }
//            $textTest = ' ['.$detail['kg_has_gas'] . ' --  '. $detail['kg_empty'];
            $textTest = '';
        ?>
        <tr class="materials_row">
            <td class="item_c order_no"><?php echo $index++;?></td>
            <td class="item_c materials_code <?php echo $classFocus;?>"><?php echo $code;?></td>
            <td class="materials_name <?php echo $classFocus;?>"><?php echo $detail['materials_name'];?></td>
            <td class="item_c">
                <input type="text" name="qty[]" class="item_c w-30 number_only_v1 materials_qty_hide" maxlength="6" value="<?php echo  $detail['qty'];?>" <?php echo $readonly;?>>
                <input class="materials_qty_real" type="hidden" name="qty_real[]" value="<?php echo  $detail['qty_real'];?>">
                <input class="materials_type_id" type="hidden" name="materials_type_id[]" value="<?php echo  $detail['materials_type_id'];?>">
                <input class="materials_id" type="hidden" name="materials_id[]" value="<?php echo $detail['materials_id'];?>">
            </td>
            <td>
                <input class="materials_seri w-80 item_c number_only_v1" type="text" name="seri[]" value="<?php echo $detail['seri'];?>" maxlength="5" <?php echo $readonlySeri;?> >
            </td>
            <td>
                <input class="materials_kg_empty w-80 item_c number_only_v1" type="text" name="kg_empty[]" value="<?php echo $detail['kg_empty'];?>" maxlength="6" >
            </td>
            <td>
                <input class="materials_kg_has_gas w-80 item_c number_only_v1" type="text" name="kg_has_gas[]" value="<?php echo $detail['kg_has_gas'];?>" maxlength="6" >
            </td>
            <td class="materials_name item_c"><?php echo $remain != 0 ? ActiveRecord::formatCurrency($remain) : '';?></td>
            <td class="item_c last"><span class="remove_icon_only"></span></td>
        </tr>
        <?php endforeach; ?>
        </tbody>
    </table>
</div>