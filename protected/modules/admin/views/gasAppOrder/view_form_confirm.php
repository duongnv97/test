<div class="form">
<?php 
$display_none_user_id_executive     = '' ;
$display_none_agent                 = 'display_none' ;
if($model->type == GasAppOrder::DELIVERY_NOW ){
    $display_none_user_id_executive = 'display_none';
    $display_none_agent             = '' ;
}
$model->date_delivery   =  MyFormat::dateConverYmdToDmy($model->date_delivery, 'd/m/Y');
$model->delivery_timer  = MyFormat::datetimeDbDatetimeUser($model->delivery_timer);
?>
<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'gas-app-order-form',
	'enableAjaxValidation'=>false,
        'htmlOptions' => array('enctype' => 'multipart/form-data'),
)); ?>
    <p class="note">Dòng có dấu <span class="required">*</span> là bắt buộc.</p>
    <?php echo MyFormat::BindNotifyMsg(); ?>
    <?php echo $form->errorSummary($model); ?>
    <div class="row">
        <?php echo $form->labelEx($model,'date_delivery'); ?>
        <?php 
            $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                'model'=>$model,        
                'attribute'=>'date_delivery',
                'language'=>'en-GB',
                'options'=>array(
                    'showAnim'=>'fold',
                    'dateFormat'=> ActiveRecord::getDateFormatJquery(),
                    'minDate'=> '0',
                    'maxDate'=> '5',
                    'changeMonth' => true,
                    'changeYear' => true,
                    'showOn' => 'button',
                    'buttonImage'=> Yii::app()->theme->baseUrl.'/admin/images/icon_calendar_r.gif',
                    'buttonImageOnly'=> true,                                
                ),        
                'htmlOptions'=>array(
                    'class'=>'w-16',
                    'style'=>'height:30px;',
                    'readonly'=> 1,
                ),
            ));
        ?>     		
    </div>
    
    <div class="row">
        <?php echo $form->labelEx($model,'status'); ?>
        <?php echo $form->dropDownList($model,'status', $model->getArrayStatusConfirm(),array('class'=>'')); ?>
        <?php echo $form->error($model,'status'); ?>
    </div>
    
    <div class="row">
        <?php echo $form->labelEx($model,'delivery_timer'); ?>
        <div class="">
            <?php Yii::import('application.extensions.CJuiDateTimePicker.CJuiDateTimePicker');
            $this->widget('CJuiDateTimePicker',array(
                'model'=>$model, //Model object
                'attribute'=>'delivery_timer', //attribute name
//                'name'=>'delivery_timer'.$i, //attribute name
                'mode'=>'datetime', //use "time","date" or "datetime" (default)
                'language'=>'en-GB',
//                'language'=>'de',
                'options'=>array(
                    'minDate'=> '0',
                    'maxDate'=> '10',
                    'showAnim'=>'fold',
                    'showButtonPanel'=>true,
                    'autoSize'=>true,
                    'dateFormat'=>'dd/mm/yy',
                    'timeFormat'=>'hh:mm:ss',
                    'width'=>'120',
                    'separator'=>' ',
                    'showOn' => 'button',
                    'buttonImage'=> Yii::app()->theme->baseUrl.'/admin/images/icon_calendar_r.gif',
                    'buttonImageOnly'=> true,
                    'changeMonth' => true,
                    'changeYear' => true,
                        'regional' => 'en-GB'
//                        'regional' => 'fr'
                ),
                'htmlOptions' => array(
                    'class' => 'w-160',
                    'readonly'=>'readonly',
                    'style'=>'height:30px;',
                ),
            ));
            ?>
        </div>
    </div>
    
    <div class="row ">
        <?php echo $form->labelEx($model,'note_customer'); ?>
        <?php echo $model->note_customer; ?>
    </div>
    <div class='clr'></div>
    <div class="row ">
        <?php echo $form->labelEx($model,'note_employee'); ?>
        <?php echo $form->textArea($model,'note_employee',array('class'=>'w-400 h_60')); ?>
        <?php echo $form->error($model,'note_employee'); ?>
    </div>
    
    <div class="row GasCheckboxList" style=''>
        <?php echo $form->labelEx($model,'type'); ?>
        <?php echo $form->radioButtonList($model,'type', $model->getArrayDelivery(), 
                    array(
                        'separator'=>"",
                        'template'=>'<li>{input}{label}</li>',
                        'container'=>'ul',
                        'class'=>'RadioOrderType' 
                    )); 
        ?>
        <?php echo $form->error($model,'type'); ?>
    </div>
    <div class='clr'></div>
    
    <div class="row fix_custom_label_required <?php echo $display_none_user_id_executive;?>">
        <?php echo $form->labelEx($model,'user_id_executive'); ?>
        <?php echo $form->dropDownList($model,'user_id_executive', $model->getArrayScheduleCar(),array('class'=>'user_id_executive','empty'=>'Select')); ?>
        <?php echo $form->error($model,'user_id_executive'); ?>
    </div>
    
    <div class="row fix_custom_label_required <?php echo $display_none_agent;?>">
        <?php echo $form->labelEx($model,'agent_id'); ?>
        <?php echo $form->hiddenField($model,'agent_id', array('class'=>'agent_id_hide')); ?>
        <?php
            // 1. limit search kh của sale
            $url = Yii::app()->createAbsoluteUrl('admin/ajax/search_by_role', array('role'=> ROLE_AGENT));
            // widget auto complete search user customer and supplier
            $aData = array(
                'model'=>$model,
                'field_customer_id'=>'agent_id',
                'url'=> $url,
                'name_relation_user'=>'rAgent',
                'ClassAdd' => 'w-400',
                'field_autocomplete_name' => 'autocomplete_name',
                'placeholder'=>'Nhập mã hoặc tên đại lý',
            );
            $this->widget('ext.SpaAutocompleteCustomer.SpaAutocompleteCustomer',
                array('data'=>$aData));
            ?>
        <?php echo $form->error($model,'agent_id'); ?>
    </div>
    
    <div class="row" >
        <label>&nbsp;</label>
        <?php echo $form->checkBox($model,'change_qty',array('class'=>'float_l change_qty')); ?>
        <?php echo $form->labelEx($model,'change_qty'); ?>
        <br>
        
    </div>
    <div class='clr'></div>
    <br>
    <?php include 'view_form_change_qty.php'; ?>
    <div class="row buttons" style="padding-left: 141px;">
        <?php $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType'=>'submit',
            'label'=> 'Save',
            'type'=>'null', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
            'size'=>'small', // null, 'large', 'small' or 'mini'
            //'htmlOptions' => array('style' => 'margin-bottom: 10px; float: right;'),
        )); ?>
        <input class='cancel_iframe' type='button' value='Close'>
    </div>
    <div class="row">
        <label>Lịch sử lấy hàng</label>
        <div class="float_l">
            <?php include 'view_history.php'; ?>
        </div>
    </div>
    

<?php $this->endWidget(); ?>

</div><!-- form -->
<script>
    $(document).ready(function(){
        $('.form').find('button:submit').click(function(){
            $.blockUI({ overlayCSS: { backgroundColor: '#fff' } }); 
        }); 
        radioOrderTypeCheck();
        fnBindFixLabelRequired();
//        $.datepicker.setDefaults($.datepicker.regional['en-GB']);
    });
    
    function radioOrderTypeCheck(){
        $('.RadioOrderType').click(function(){
            var check = $(this).val();
            if(check == <?php echo GasAppOrder::DELIVERY_BY_CAR;?>){
                $('.user_id_executive').closest('.row').show();
                $('.agent_id_hide').closest('.row').hide().find('input').val('').attr('readonly', false);
                $('.agent_id_hide').closest('.row').find('.autocomplete_customer_info').hide();
            }else{
                $('.user_id_executive').val('').closest('.row').hide();
                $('.agent_id_hide').closest('.row').show();
            }
        });
        
        $('.change_qty').click(function(){
            if($(this).is(':checked')){
                $('.BoxChangeQty').show();
            }else{
                $('.BoxChangeQty').hide();
            }
        });
        
    }
</script>
