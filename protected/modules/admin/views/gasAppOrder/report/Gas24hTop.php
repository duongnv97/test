<?php // $aData = $mRpGas24h->getReportTop(); ?>
<?php $aData = $mRpGas24h->getReportTop2(); ?>
<div class="search-form ">
    <?php include 'Gas24hTopSearch.php';?>
</div>

<table class="tb hm_table f_size_15">
    <thead>
        <th>#</th>
        <th>Tên</th>
        <th>Mã giới thiệu</th>
        <th>Tổng nhập mã</th>
        <th>Mua hàng</th>
        <th>Tiền tích lũy</th>
        <th>Số tiền còn lại</th>
        <th>Tổng tiền đã mua</th>
    </thead>
    <tbody>
        <?php   $index = 1; 
                $sumCountRef = 0;
                $sumCountConfirm = 0;
                $sumAmount = 0;
                $sumSellAmount = 0;
        ?>
        <tr id = 'target'>
        </tr>
        <?php foreach ($aData as $item): 
            $sumCountRef        += !empty($item['countRef']) ? $item['countRef'] : 0;
            $sumCountConfirm    += !empty($item['countConfirm']) ? $item['countConfirm'] : 0;
            $sumAmount          += !empty($item['amount']) ? $item['amount'] : 0;
            $sumSellAmount      += !empty($item['sell_amount']) ? $item['sell_amount'] : 0;
            $amountConfirm      = $item['countConfirm'] * AppPromotion::P_AMOUNT_OF_OWNER;
        ?>
        <tr>
            <td><?php echo $index++; ?></td>
            <td><?php echo !empty($item['first_name']) ? $item['first_name'] : ''; ?></td>
            <td class='item_c'><?php echo !empty($item['code_no']) ? $item['code_no'] : ''; ?></td>
            <td class='item_r'><?php echo !empty($item['countRef']) ? $item['countRef'] : ''; ?></td>
            <td class='item_r'><?php echo !empty($item['countConfirm']) ? $item['countConfirm'] : ''; ?></td>
            <td class='item_r'><?php echo ActiveRecord::formatCurrency($amountConfirm); ?></td>
            <td class='item_r'><?php echo !empty($item['amount']) ? ActiveRecord::formatCurrency($item['amount']) : ''; ?></td>
            <td class='item_r'><?php echo !empty($item['sell_amount']) ? ActiveRecord::formatCurrency($item['sell_amount']) : ''; ?></td>
        </tr>
        <?php endforeach; ?>
        <tr  id = 'hiddenItem'>
            <td class='item_c item_b' colspan='3'>Tổng</td>
            <td class='item_r item_b'><?php echo $sumCountRef;?></td>
            <td class='item_r item_b'><?php echo $sumCountConfirm;?></td>
            <td class='item_r item_b'></td>
            <td class='item_r item_b'><?php echo ActiveRecord::formatCurrency($sumAmount);?></td>
            <td class='item_r item_b'><?php echo ActiveRecord::formatCurrency($sumSellAmount);?></td>
        </tr>
    </tbody>
</table>
<script>
    $(document).ready(function(){
        $('#target').html($('#hiddenItem').html());
    });
</script>