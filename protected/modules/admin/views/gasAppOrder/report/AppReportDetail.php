<?php
/* 1. get list customer active
 * 2/ find all on User
 * 3. count all order 
 * 4. sort by js
 */
$mMonitorUpdate = new MonitorUpdate();
$mMonitorUpdate->agent_id = $sale_id_select;
$models         = $mMonitorUpdate->appReportActiveOfSale();
$aDataOrders    = $mMonitorUpdate->appReportOrdersOfSale();
$CUSTOMER_ID    = isset($aDataOrders['CUSTOMER_ID']) ? $aDataOrders['CUSTOMER_ID'] : [];
$aCustomer = $aModelUser = $aIdCustomerMore = [];
foreach($models as $item){
    $aCustomer[$item->user_id] = $item->user_id;
}
foreach($CUSTOMER_ID as $customer_id){
    if(!in_array($customer_id, $aCustomer)){
        $aIdCustomerMore[$customer_id] = $customer_id;
    }
    $aCustomer[$customer_id] = $customer_id;
}

if(count($aCustomer)){
    $aModelUser = Users::getArrObjectUserByRole('', $aCustomer);
}
$ORDERS         = isset($aDataOrders['ORDERS']) ? $aDataOrders['ORDERS'] : [];
$index = 1;
?>

<table class="tb hm_table f_size_15 sortable materials_table">
    <thead>
        <tr>
            <th class="item_b">#</th>
            <th class="item_b">Sale: <?php echo $selectSale;?></th>
            <th class="item_b">Loại KH</th>
            <th class="item_b">Ngày kích hoạt</th>
            <th class="item_b SortColumn w-100"><a href="javascript:;">Tổng đơn hàng - Click để sắp xếp</a></th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($models as $mMonitorUpdate): ?>
        <?php 
            $customer_id    = $mMonitorUpdate->user_id;
            $customerName   = isset($aModelUser[$customer_id]) ? $aModelUser[$customer_id]->code_bussiness.' - '.$aModelUser[$customer_id]->first_name : '' ;
            $customerType   = isset($aModelUser[$customer_id]) ? $aModelUser[$customer_id]->getTypeCustomerText() : '' ;
            $qtyOrder       = isset($ORDERS[$customer_id]) ? $ORDERS[$customer_id] : '';
        ?>
        <tr>
            <td class="item_c order_no"><?php echo $index++;?></td>
            <td class=""><?php echo $customerName;?></td>
            <td class="item_c"><?php echo $customerType;?></td>
            <td class="item_c "><?php echo MyFormat::dateConverYmdToDmy($mMonitorUpdate->last_update);?></td>
            <td class="item_c"><?php echo $qtyOrder;?></td>
        </tr>
        <?php endforeach; ?>
        
        <?php foreach ($aIdCustomerMore as $customer_id): ?>
        <?php 
            $customerName   = isset($aModelUser[$customer_id]) ? $aModelUser[$customer_id]->code_bussiness.' - '.$aModelUser[$customer_id]->first_name : '' ;
            $customerType   = isset($aModelUser[$customer_id]) ? $aModelUser[$customer_id]->getTypeCustomerText() : '' ;
            $qtyOrder       = isset($ORDERS[$customer_id]) ? $ORDERS[$customer_id] : '';
        ?>
        <tr>
            <td class="item_c order_no"><?php echo $index++;?></td>
            <td class=""><?php echo $customerName;?></td>
            <td class="item_c"><?php echo $customerType;?></td>
            <td class="item_c "></td>
            <td class="item_c"><?php echo $qtyOrder;?></td>
        </tr>
        <?php endforeach; ?>
        
    </tbody>
</table>

<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/sortElements/jquery.sortElements.js"></script>
<!--https://stackoverflow.com/questions/3160277/jquery-table-sort-->
<script>
$(function(){
var table = $('.sortable');
$('.SortColumn')
    .wrapInner('<span title="sort this column"/>')
    .each(function(){

        var th = $(this),
            thIndex = th.index(),
            inverse = false;

        th.click(function(){

            table.find('td').filter(function(){

                return $(this).index() === thIndex;

            }).sortElements(function(a, b){
                var aText = $.text([a])*1;
                var bText = $.text([b])*1;
//                    aText = parseFloat(aText.replace(/,/g, ''));
//                    bText = parseFloat(bText.replace(/,/g, ''));
//                    aText = aText *1;
//                    bText = bText *1;

                if( aText == bText )
                    return 0;

//                return aText > bText ? // asc 
                return aText < bText ? // DESC
                    inverse ? -1 : 1
                    : inverse ? 1 : -1;

            }, function(){

                // parentNode is the element we want to move
                return this.parentNode; 

            });

            inverse = !inverse;

            fnRefreshOrderNumber();
        });
    });
    
//    $('.SortColumn').trigger('click');
    
    
});
</script>