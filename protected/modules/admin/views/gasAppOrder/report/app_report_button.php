
<div class="form">
    <?php
        $LinkNormal       = Yii::app()->createAbsoluteUrl('admin/gasAppOrder/appReport');
        $LinkPotential    = Yii::app()->createAbsoluteUrl('admin/gasAppOrder/appReport', array( 'potential'=> 1));
    ?>
    <h1>
        <a class='btn_cancel f_size_14 <?php echo (!isset($_GET['potential'])) ? "active":"";?>' href="<?php echo $LinkNormal;?>">KH bò mối</a>
        <a class='btn_cancel f_size_14 <?php echo isset($_GET['potential']) && $_GET['potential']==1 ? "active":"";?>' href="<?php echo $LinkPotential;?>">KH tiềm năng</a>
    </h1> 
</div>
<?php 
if(isset($_GET['potential'])){
    include 'PotentialReport.php';
} else {
    include 'AppReport.php';
}