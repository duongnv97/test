<?php
$this->breadcrumbs=array(
    $this->pageTitle,
);
?>
<h1><?php echo $this->pageTitle; ?></h1>
<div class="search-form ">
<?php include '_searchSoldOut.php'; ?>
</div><!-- search-form -->
<?php 
$mAppCache      = new AppCache();
$aMaterial      = $mAppCache->getListdata('GasMaterials', AppCache::LISTDATA_MATERIAL);
$aMaterialType  = $mAppCache->getListdata('GasMaterialsType', AppCache::LISTDATA_MATERIAL_TYPE);
$sumAllQty = $sumAllRevenue = $sumAllRemain = $sumAllRemainAmount = 0;
?>
<table class="tb hm_table f_size_15">
    <thead>
        <tr>
            <th class="item_c">Vật tư</th>
            <th class="item_c">Số lượng</th>
            <th class="item_c w-150">Doanh thu</th>
            <th class="item_c w-150">Gas dư</th>
            <th class="item_c w-150">Tiền gas dư</th>
            <th class="item_c w-150">Giá trung bình - doanh thu chia số lượng</th>
        </tr>
    <thead>
    <tbody>
    <?php foreach ($rData as $materials_type_id => $aInfo): ?>
        <?php
            $nameMaterialType = isset($aMaterialType[$materials_type_id]) ? $aMaterialType[$materials_type_id] : '';
            $sumQty = $sumRevenue = $sumRemain = $sumRemainAmount = 0;
        ?>
        <?php foreach ($aInfo as $materials_id => $aInfo1): ?>
            <?php 
                $nameMaterial = isset($aMaterial[$materials_id]) ? $aMaterial[$materials_id] : $materials_id;
                $remain = $aInfo1['kg_has_gas'] - $aInfo1['kg_empty'] + $aInfo1['pay_back'];
                $sumQty             += $aInfo1['qty'];
                $sumRevenue         += $aInfo1['amount'];
                $sumRemain          += $remain;
                $sumRemainAmount    += $aInfo1['total_gas_du'];
                
                $sumAllQty             += $aInfo1['qty'];
                $sumAllRevenue         += $aInfo1['amount'];
                $sumAllRemain          += $remain;
                $sumAllRemainAmount    += $aInfo1['total_gas_du'];
                
                $priceAverage = 0;
                if($aInfo1['qty'] !=0){
                    $priceAverage = $aInfo1['amount'] / $aInfo1['qty'];
                }
            ?>
        <tr class="h_30">
            <td><?php echo $nameMaterial; ?></td>
            <td class="item_c"><?php echo ActiveRecord::formatCurrency($aInfo1['qty']); ?></td>
            <td class="item_r"><?php echo $aInfo1['amount'] !=0 ? ActiveRecord::formatCurrency($aInfo1['amount']) : ''; ?></td>
            <td class="item_r"><?php echo $remain != 0 ? ActiveRecord::formatCurrency($remain) : ''; ?></td>
            <td class="item_r"><?php echo $aInfo1['total_gas_du'] != 0 ? ActiveRecord::formatCurrency($aInfo1['total_gas_du']) : ''; ?></td>
            <td class="item_r"><?php echo $priceAverage != 0 ? ActiveRecord::formatCurrencyRound($priceAverage) : ''; ?></td>
        </tr>
        <?php endforeach; ?>
        <?php 
            $priceAverage = 0;
            if($sumQty !=0){
                $priceAverage = $sumRevenue / $sumQty;
            }
        ?>
        <tr class="h_30">
            <th class="item_b"><?php echo $nameMaterialType; ?></th>
            <th class="item_c item_b"><?php echo ActiveRecord::formatCurrency($sumQty); ?></th>
            <th class="item_c item_b"><?php echo $sumRevenue != 0 ? ActiveRecord::formatCurrency($sumRevenue) : ''; ?></th>
            <th class="item_c item_b"><?php echo $sumRemain != 0 ? ActiveRecord::formatCurrency($sumRemain) : ''; ?></th>
            <th class="item_c item_b"><?php echo $sumRemainAmount != 0 ? ActiveRecord::formatCurrency($sumRemainAmount) : ''; ?></th>
            <th class="item_r item_b"><?php echo $priceAverage != 0 ? ActiveRecord::formatCurrencyRound($priceAverage) : ''; ?></td>
        </tr>
    <?php endforeach; ?>
    </tbody>
    <tfoot>
        <tr class="h_30 SumAll">
            <th class="item_r item_b">Sum All</th>
            <th class="item_c item_b"><?php echo ActiveRecord::formatCurrency($sumAllQty); ?></th>
            <th class="item_c item_b"><?php echo ActiveRecord::formatCurrency($sumAllRevenue); ?></th>
            <th class="item_c item_b"><?php echo ActiveRecord::formatCurrency($sumAllRemain); ?></th>
            <th class="item_c item_b"><?php echo ActiveRecord::formatCurrency($sumAllRemainAmount); ?></th>
            <th></th>
        </tr>
    </tfoot>
</table>

<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/print-store-card.css" />

<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/jquery.floatThead.min.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/underscore-min.js"></script>    
<script>
$(document).ready(function() {
    $('.hm_table').floatThead(); // lỗi mất border
    $('.hm_table tbody').prepend($('.SumAll').clone());
});
</script>