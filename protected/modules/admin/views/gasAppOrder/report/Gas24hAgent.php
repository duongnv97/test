<div class="search-form ">
    <?php include 'Gas24hAgentSearch.php';?>
</div>
<?php if(isset($rData['aActiveUser'])): ?>
<?php 
$mAppCache          = new AppCache();
$aAgent             = $mAppCache->getAgentListdata();
$aActiveUser        = $rData['aActiveUser'];
$aInputPromotion    = isset($rData['InputPromotion']) ? $rData['InputPromotion'] : [];
$aOrders            = isset($rData['Orders']) ? $rData['Orders'] : [];
?>

<table class="tb hm_table f_size_15">
    <thead>
        <th>Đại lý</th>
        <th>Cài đặt app</th>
        <th>Nhập KM</th>
        <th>Đặt hàng</th>
        <th>Chưa đặt hàng</th>
    </thead>
    <tbody>
        <tr>
            <td class="item_b">Sum</td>
            <td class="item_b item_c"><?php echo ActiveRecord::formatCurrency($rData['sumActiveUser']);?></td>
            <td class="item_b item_c"><?php echo ActiveRecord::formatCurrency($rData['sumInputPromotion']);?></td>
            <td class="item_b item_c"><?php echo ActiveRecord::formatCurrency($rData['sumOrders']);?></td>
            <td class="item_b item_c"><?php echo ActiveRecord::formatCurrency($rData['sumActiveUser'] - $rData['sumOrders']);?></td>
        </tr>
    <?php foreach ($aActiveUser as $agent_id => $totalUser): ?>
        <?php 
            $itemActive     = isset($aActiveUser[$agent_id]) ? $aActiveUser[$agent_id] : 0;
            $itemPromotion  = isset($aInputPromotion[$agent_id]) ? $aInputPromotion[$agent_id] : 0;
            $itemOrders     = isset($aOrders[$agent_id]) ? $aOrders[$agent_id] : 0;
            $itemNotOrder   = $itemActive - $itemOrders;
        ?>
        <tr>
            <td><?php echo isset($aAgent[$agent_id]) ? $aAgent[$agent_id] : 'Chưa XĐ'; ?></td>
            <td class="item_c"><?php echo $itemActive > 0 ? ActiveRecord::formatCurrency($itemActive) : ''; ?></td>
            <td class="item_c"><?php echo $itemPromotion> 0 ? ActiveRecord::formatCurrency($itemPromotion) : ''; ?></td>
            <td class="item_c"><?php echo $itemOrders > 0 ? ActiveRecord::formatCurrency($itemOrders) : ''; ?></td>
            <td class="item_c"><?php echo $itemNotOrder > 0 ? ActiveRecord::formatCurrency($itemNotOrder) : ''; ?></td>
        </tr>
    <?php endforeach; ?>
    </tbody>
</table>

<?php endif; ?>
