<div class="form">
    <?php
        $LinkNormal = Yii::app()->createAbsoluteUrl('admin/gasAppOrder/reportGas24h');
        $LinkSum    = Yii::app()->createAbsoluteUrl('admin/gasAppOrder/reportGas24h', array( 'type'=> 1));
        $LinkTop    = Yii::app()->createAbsoluteUrl('admin/gasAppOrder/reportGas24h', array( 'type'=> 2));
    ?>
    <h1><?php echo $this->pageTitle;?>
        <a class='btn_cancel f_size_14 <?php echo (!isset($_GET['type'])) ? 'active':'';?>' href="<?php echo $LinkNormal;?>">Tổng quát</a>
        <a class='btn_cancel f_size_14 <?php echo isset($_GET['type']) && $_GET['type']==1 ? 'active':'';?>' href="<?php echo $LinkSum;?>">Đại lý</a>
        <a class='btn_cancel f_size_14 <?php echo isset($_GET['type']) && $_GET['type']==2 ? 'active':'';?>' href="<?php echo $LinkTop;?>">TOP</a>
    </h1> 
</div>