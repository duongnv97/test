<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=> GasCheck::getCurl(),
	'method'=>'get',
)); ?>

    <div class="row">
        <?php echo $form->labelEx($model,'customer_id'); ?>
        <?php echo $form->hiddenField($model,'customer_id', array('class'=>'')); ?>
        <?php 
            // widget auto complete search user customer and supplier
            $aData = array(
                'model'=>$model,
                'field_customer_id'=>'customer_id',
                'name_relation_user'=>'rCustomer',
                'url' => Yii::app()->createAbsoluteUrl('admin/ajax/search_user_by_code'),
                'ClassAdd' => 'w-400',
            );
            $this->widget('ext.SpaAutocompleteCustomer.SpaAutocompleteCustomer',
                array('data'=>$aData));                                        
        ?>
    </div>

    <div class="row">
        <?php echo $form->label($model,'agent_id'); ?>
        <?php echo $form->hiddenField($model,'agent_id', array('class'=>'')); ?>
        <?php
            // 1. limit search kh của sale
            $url = Yii::app()->createAbsoluteUrl('admin/ajax/search_by_role', array('role'=> ROLE_AGENT));
            // widget auto complete search user customer and supplier
            $aData = array(
                'model'=>$model,
                'field_customer_id'=>'agent_id',
                'url'=> $url,
                'name_relation_user'=>'rAgent',
                'ClassAdd' => 'w-400',
                'field_autocomplete_name' => 'autocomplete_name_1',
                'placeholder'=>'Nhập mã hoặc tên đại lý',
            );
            $this->widget('ext.SpaAutocompleteCustomer.SpaAutocompleteCustomer',
                array('data'=>$aData));
        ?>
    </div>
    
    <div class="row">
    <?php echo $form->labelEx($model,'employee_maintain_id', ['label'=>'NV PVKH']); ?>
    <?php echo $form->hiddenField($model,'employee_maintain_id', array('class'=>'')); ?>
    <?php
            // 1. limit search kh của sale
            $url = Yii::app()->createAbsoluteUrl('admin/ajax/search_by_role', array('role'=> implode(',', Sell::$ROLE_EMPLOYEE_SEARCH_AUTOCOMPLETE)));
            // widget auto complete search user customer and supplier
            $aData = array(
                'model'=>$model,
                'field_customer_id'=>'employee_maintain_id',
                'url'=> $url,
                'name_relation_user'=>'rEmployeeMaintain',
                'ClassAdd' => 'w-400',
                'field_autocomplete_name' => 'autocomplete_name_2',
                'placeholder'=>'Nhập mã NV hoặc tên',
            );
            $this->widget('ext.SpaAutocompleteCustomer.SpaAutocompleteCustomer',
                array('data'=>$aData));
            ?>
        <?php echo $form->error($model,'employee_maintain_id'); ?>
    </div>
    
    <div class="row">
        <?php echo $form->label($model,'sale_id'); ?>
        <?php echo $form->hiddenField($model,'sale_id'); ?>
        <?php 
            // widget auto complete search user customer and supplier
            $aData = array(
                'model'=>$model,
                'field_customer_id'=>'sale_id',
                'field_autocomplete_name'=>'obj_detail_id_old',
                'url'=> Yii::app()->createAbsoluteUrl('admin/ajax/search_sale'),
                'name_relation_user'=>'rSale',
                'placeholder'=>'Nhập Tên Sale',
                'ClassAdd' => 'w-400',
//                        'fnSelectCustomer'=>'fnAfterSelectSaleOrAgent',
            );
            $this->widget('ext.SpaAutocompleteCustomer.SpaAutocompleteCustomer',
                array('data'=>$aData));                                        
        ?>
    </div>
    
    <div class="row">
    <?php echo $form->labelEx($model,'driver_id', array('label'=>'Lái xe')); ?>
    <?php echo $form->hiddenField($model,'driver_id'); ?>
    <?php
            // 1. limit search kh của sale
            $url = Yii::app()->createAbsoluteUrl('admin/ajax/search_by_role', array('role'=> ROLE_DRIVER));
            // widget auto complete search user customer and supplier
            $aData = array(
                'model'=>$model,
                'field_customer_id'=>'driver_id',
                'url'=> $url,
                'name_relation_user'=>'rDriver',
                'ClassAdd' => 'w-400',
                'field_autocomplete_name' => 'MAX_ID',
                'placeholder'=>'Nhập mã NV hoặc tên',
            );
            $this->widget('ext.SpaAutocompleteCustomer.SpaAutocompleteCustomer',
                array('data'=>$aData));
            ?>
    </div>

    <div class="row more_col">
        <div class="col1">
            <?php echo $form->label($model,'date_from'); ?>
            <?php 
                $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                    'model'=>$model,        
                    'attribute'=>'date_from',
                    'options'=>array(
                        'showAnim'=>'fold',
                        'dateFormat'=> MyFormat::$dateFormatSearch,
//                            'minDate'=> '0',
//                        'maxDate'=> '0',
                        'changeMonth' => true,
                        'changeYear' => true,
                        'showOn' => 'button',
                        'buttonImage'=> Yii::app()->theme->baseUrl.'/admin/images/icon_calendar_r.gif',
                        'buttonImageOnly'=> true,                                
                    ),        
                    'htmlOptions'=>array(
                        'class'=>'w-16',
                        'size'=>'16',
                        'style'=>'float:left;',
                        'readonly'=> 1,
                    ),
                ));
            ?>
        </div>
        <div class="col2">
            <?php echo $form->label($model,'date_to'); ?>
            <?php 
                $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                    'model'=>$model,        
                    'attribute'=>'date_to',
                    'options'=>array(
                        'showAnim'=>'fold',
                        'dateFormat'=> MyFormat::$dateFormatSearch,
//                            'minDate'=> '0',
//                        'maxDate'=> '0',
                        'changeMonth' => true,
                        'changeYear' => true,
                        'showOn' => 'button',
                        'buttonImage'=> Yii::app()->theme->baseUrl.'/admin/images/icon_calendar_r.gif',
                        'buttonImageOnly'=> true,                                
                    ),        
                    'htmlOptions'=>array(
                        'class'=>'w-16',
                        'size'=>'16',
                        'style'=>'float:left;',
                        'readonly'=> 1,
                    ),
                ));
            ?>
        </div>
    </div>
    
    <div class="row more_col">
        <div class="col1">
            <?php echo $form->label($model,'status_debit',array()); ?>
            <?php echo $form->dropDownList($model,'status_debit', $model->getArrayStatusDebit(),array('class'=>'w-200', 'empty'=>'Select')); ?>
        </div>
        <div class="col2">
            <?php echo $form->label($model,'paid_gas_remain',array()); ?>
            <?php echo $form->dropDownList($model,'paid_gas_remain', CmsFormatter::$yesNoFormat,array('class'=>'w-200', 'empty'=>'Select')); ?>
        </div>
        <div class="col3">
            <?php echo $form->label($model,'pay_direct',array()); ?>
            <?php echo $form->dropDownList($model,'pay_direct', $model->getTypePayDirect(), array('class'=>'w-200', 'empty'=>'Select')); ?>
        </div>
    </div>

    <div class="row more_col">
        <div class="col1">
            <?php echo $form->label($model,'status',array()); ?>
            <?php echo $form->dropDownList($model,'status', $model->getArrayStatus(),array('class'=>'w-200', 'empty'=>'Select')); ?>
        </div>
        <div class="col2">
            <?php echo $form->label($model,'type',array()); ?>
            <?php echo $form->dropDownList($model,'type', $model->getArrayDelivery(),array('class'=>'w-200', 'empty'=>'Select')); ?>
        </div>
        <div class="col3">
            <?php $aCar = Users::getArrDropdown(ROLE_CAR); ?>
            <?php echo $form->labelEx($model,'car_id', ['label'=>'Số xe']); ?>
            <?php echo $form->dropDownList($model,'car_id', $aCar,array('class'=>'w-200 float_l', 'empty'=>'Select')); ?>
        </div>
    </div>
    <div class="row more_col">
        <div class="col1 w-400">
            <?php 
                $aTypeCustomer = CmsFormatter::$CUSTOMER_BO_MOI;
                unset($aTypeCustomer[STORE_CARD_VIP_HGD]);
                unset($aTypeCustomer[STORE_CARD_HGD_CCS]);
                unset($aTypeCustomer[UsersExtend::STORE_CARD_HGD_APP]);
                unset($aTypeCustomer[UsersExtend::STORE_CARD_HEAD_QUATER]);
                unset($aTypeCustomer[UsersExtend::STORE_CARD_NCC]);
            ?>
            <?php echo $form->label($model,'type_customer', ['label'=>'Loại KH']); ?>
            <?php // echo $form->dropDownList($model,'type_customer', CmsFormatter::$CUSTOMER_BO_MOI,array('class'=>'w-200', 'empty'=>'Select')); ?>
            <div class="fix-label">
                <?php
                   $this->widget('ext.multiselect.JMultiSelect',array(
                         'model'=>$model,
                         'attribute'=>'type_customer',
                         'data'=> $aTypeCustomer,
                         // additional javascript options for the MultiSelect plugin
                        'options'=>array('selectedList' => 30,),
                         // additional style
                         'htmlOptions'=>array('style' => 'width: 200px;'),
                   ));    
               ?>
            </div>
        </div>
        <div class="col2">
            <?php $mUser = new Users(); ?>
            <?php echo $form->label($model,'customer_contact', ['label'=>'Nhóm KH']); ?>
            <?php echo $form->dropDownList($model,'customer_contact', $mUser->getArrayGroup(),array('class'=>'w-200', 'empty'=>'Select')); ?>
        </div>
        <div class="col3">
            <?php echo $form->labelEx($model,'customer_phone',array('label'=>'Không xem kho', 'class'=>'checkbox_one_label', 'style'=>'padding-top:3px;')); ?>
            <?php echo $form->checkBox($model,'customer_phone',array('class'=>'float_l')); ?>
        </div>
    </div>
    
    <div class="row">
        <?php echo $form->labelEx($model,'obj_id_old', ['label'=>'Tỉnh']); ?>
        <div class="fix-label">
            <?php
               $this->widget('ext.multiselect.JMultiSelect',array(
                     'model'=>$model,
                     'attribute'=>'obj_id_old',
                     'data'=> GasProvince::getArrAll(),
                     // additional javascript options for the MultiSelect plugin
                    'options'=>array('selectedList' => 30,),
                     // additional style
                     'htmlOptions'=>array('style' => 'width: 800px;'),
               ));    
           ?>
        </div>
    </div>
    

    <div class="row buttons" style="padding-left: 159px;">
        <?php $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType'=>'submit',
            'label'=>'Search',
            'type'=>'null', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
            'size'=>'small', // null, 'large', 'small' or 'mini'
            //'htmlOptions' => array('style' => 'margin-bottom: 10px; float: right;'),
        )); ?>	
    </div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->