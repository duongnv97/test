<div class="search-form">
    <?php  include '_searchPotential.php'; ?>
</div>

<?php 
$i = 1;
$aModelUser = empty($rData['SALE_ID']) ? [] : Users::getArrObjectUserByRole('', $rData['SALE_ID']);
?>
<div class="grid-view display_none">
    <table class="items hm_table freezetablecolumns" id="potential-customer">
        <thead>
            <tr class="h_20">
                <th class="w-20">#</th>
                <th class="w-150">Nhân viên</th>
                <th class="w-30">Tổng</th>
                <?php foreach ($rData['DATE'] as $date): ?>
                <th class="w-30"><?php echo date('d', strtotime($date)); ?></th>
                <?php endforeach; ?>
            </tr>
        </thead>
        <tbody>
            <tr class="h_20">
                <td></td>
                <td class="item_b item_c">Tổng</td>
                <td class="item_b item_c"><?php echo array_sum($rData['SUM_ROW']); ?></td>
                <?php foreach ($rData['DATE'] as $date): ?>
                <td class="item_b item_c"><?php echo isset($rData['SUM_COLUMN'][$date]) ? $rData['SUM_COLUMN'][$date] : ''; ?></td>
                <?php endforeach; ?>
            </tr>
            <?php foreach ($rData['DATA'] as $sale_id => $item): ?>
            <tr class="h_20">
                <td class="item_c"><?php echo $i++; ?></td>
                <td class="item_b"><?php echo isset($aModelUser[$sale_id]) ? $aModelUser[$sale_id]->first_name : '' ; ?></td>
                <td class="item_b item_c"><?php echo $rData['SUM_ROW'][$sale_id]; ?></td>
                <?php foreach ($rData['DATE'] as $date): ?>
                <td class="item_c"><?php echo isset($item[$date]) ? $item[$date] : ''; ?></td>
                <?php endforeach; ?>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
</div>

<?php 
$gridCss = Yii::app()->getAssetManager()->publish(Yii::getPathOfAlias('zii.widgets.assets')) . '/gridview/styles.css';
Yii::app()->getClientScript()->registerCssFile($gridCss);
?>
<script type="text/javascript"
        src="<?php echo Yii::app()->theme->baseUrl; ?>/js/scroll/jquery.freezetablecolumns.1.1.js"></script>
<script type="text/javascript">
    fnAddClassOddEven('items');
    $('.freezetablecolumns').each(function () {
        var id_table = $(this).attr('id');
        $('#' + id_table).freezeTableColumns({
            width:       1100,   // required - bt 1100
            height:      400,   // required
            numFrozen: 3,     // optional
            frozenWidth: 240,   // optional
            clearWidths: true  // optional
        });
    });
    $(window).load(function () {
        var index = 1;
        $('.freezetablecolumns').each(function () {
            if (index == 1)
                $(this).closest('div.grid-view').show();
            index++;
        });
        fnAddClassOddEven('items');
    });
</script>