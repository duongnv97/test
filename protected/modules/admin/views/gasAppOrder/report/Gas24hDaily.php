<?php 
$aDailySellSumDay       = isset($rData['DailySellSumDay']) ? $rData['DailySellSumDay'] : [];
$aDailySellSumAgent     = isset($rData['DailySellSumAgent']) ? $rData['DailySellSumAgent'] : [];
$aDailySellDate         = isset($rData['DailySellDate']) ? $rData['DailySellDate'] : [];
$mAppCache              = new AppCache();
$aAgent                 = $mAppCache->getAgentListdata();

$tempChart = $listUserId = $totalDataChart = $chartAgent = [];
$listTitleChart     = ['Đơn hàng App'];
$chartAgent[]       = ['Đại lý', 'Đơn hàng'];
foreach($aDailySellSumAgent as $agent_id => $total){
    $agentName = isset($aAgent[$agent_id]) ? $aAgent[$agent_id] : '-';
    $chartAgent[] = [$agentName, $total];
}
$tile = 'Thống kê dữ liệu đặt app 1 tháng gần đây';
?>
<h1><?php echo $tile;?></h1>
<table class="tb hm_table f_size_15">
    <thead>
        <tr>
            <th class="item_c">Ngày</th>
        <?php foreach ($aDailySellDate as $day): ?>
            <?php $temp = explode('-', $day); ?>
            <th class="item_b"><?php echo $temp[2];?></th>
        <?php endforeach; ?>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>Đơn hàng</td>
            <?php foreach ($aDailySellDate as $day): ?>
            <?php $value = isset($aDailySellSumDay[$day]) ? $aDailySellSumDay[$day]*1 : 0; 
                $temp = explode('-', $day);
                $totalDataChart[] = array(
                "{$temp[2]}",
                $value
                );
            ?>
            <td class="item_c"><?php echo $value != 0 ? ActiveRecord::formatCurrency($value) : '';?></td>
            <?php endforeach; ?>
        </tr>
    </tbody>
</table>

<?php $jsArrayTotalCall = json_encode($totalDataChart); ?>
<div class="float_l" style="width: 1100px">
    <div id="total_call_chart" style="width: 1100px; height: 500px"></div>
</div>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script type="text/javascript">
    //        fnAddClassOddEven('items');
    google.charts.load("current", {packages: ["line", "corechart"]});
    google.charts.setOnLoadCallback(drawChart);
    function drawChart() {
        var data = new google.visualization.DataTable();
        data.addColumn('string', 'Thời gian');
        <?php
        foreach ($listTitleChart as $value) {
            echo "data.addColumn('number', '$value');\n";
        }
        echo "data.addRows($jsArrayTotalCall);\n";
        ?>
//            data.addColumn('number', 'Guardians of the Galaxy');
//            data.addColumn('number', 'The Avengers');
//
//            data.addRows([
//                [1, 37.8, 80.8],
//                [2, 30.9, 69.5],
//            ]);
        var options = {
            title: 'Tổng số đơn hàng app hàng ngày',
            isStacked: false,
            focusTarget: 'category',
        };
        var chart = new google.charts.Line(document.getElementById('total_call_chart'));
        chart.draw(data, google.charts.Line.convertOptions(options));
    }
</script>

<?php $jsChartAgent = json_encode($chartAgent); ?>
<!--for chart agent sum-->
<div class="float_l" style="width: 1100px">
    <div id="chartAgentSum" style="width: 1100px; height: 500px"></div>
</div>
<script type="text/javascript">
    google.charts.setOnLoadCallback(drawChartAgent);
    function drawChartAgent() {
        // Making a 3D Pie Chart https://developers.google.com/chart/interactive/docs/gallery/piechart?hl=vi
        var data = google.visualization.arrayToDataTable(<?php echo $jsChartAgent; ?>);

        var options = {
            title: '<?php echo $tile;?>',
            is3D: true,
        };
        var chart = new google.visualization.PieChart(document.getElementById('chartAgentSum'));
        chart.draw(data, options);
    }
</script>