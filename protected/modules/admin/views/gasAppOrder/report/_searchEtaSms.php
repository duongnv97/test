<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=> GasCheck::getCurl(),
	'method'=>'get',
)); ?>

    
    <div class="row more_col">
        <div class="col1">
            <?php echo $form->labelEx($model,'province_id'); ?>
            <?php echo $form->dropDownList($model,'province_id', GasProvince::getArrAll(),array('class'=>'w-200','empty'=>'Select')); ?>
        </div>
        <div class="col2">
            <?php echo $form->labelEx($model,'district_id'); ?>
            <?php echo $form->dropDownList($model,'district_id', GasDistrict::getArrAll($model->province_id),array('class'=>'w-200','empty'=>'Select')); ?>
        </div>
        <div class="col3"></div>
    </div>
    
    <div class="row more_col">
        <div class="col1">
            <?php echo $form->label($model,'date_from', ['label'=>'Lấy hàng từ ngày']); ?>
            <?php 
                $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                    'model'=>$model,        
                    'attribute'=>'date_from',
                    'options'=>array(
                        'showAnim'=>'fold',
                        'dateFormat'=> MyFormat::$dateFormatSearch,
//                            'minDate'=> '0',
//                        'maxDate'=> '0',
                        'changeMonth' => true,
                        'changeYear' => true,
                        'showOn' => 'button',
                        'buttonImage'=> Yii::app()->theme->baseUrl.'/admin/images/icon_calendar_r.gif',
                        'buttonImageOnly'=> true,                                
                    ),        
                    'htmlOptions'=>array(
                        'class'=>'w-100',
                        'size'=>'16',
                        'style'=>'float:left;',
                        'readonly'=> 1,
                    ),
                ));
            ?>
        </div>
        <div class="col2">
            <?php echo $form->label($model,'date_to'); ?>
            <?php 
                $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                    'model'=>$model,        
                    'attribute'=>'date_to',
                    'options'=>array(
                        'showAnim'=>'fold',
                        'dateFormat'=> MyFormat::$dateFormatSearch,
//                            'minDate'=> '0',
//                        'maxDate'=> '0',
                        'changeMonth' => true,
                        'changeYear' => true,
                        'showOn' => 'button',
                        'buttonImage'=> Yii::app()->theme->baseUrl.'/admin/images/icon_calendar_r.gif',
                        'buttonImageOnly'=> true,                                
                    ),        
                    'htmlOptions'=>array(
                        'class'=>'w-100',
                        'size'=>'16',
                        'style'=>'float:left;',
                        'readonly'=> 1,
                    ),
                ));
            ?>
        </div>
        <div class="col3">
        </div>
    </div>
    
    
    <div class="row more_col display_none">
        <div class="col1">
            <?php // echo $form->label($model,'last_purchase', ['label' => 'Số tháng chưa mua hàng']); ?>
            <?php // echo $form->dropDownList($model,'last_purchase', MyFormat::BuildNumberOrder(10),array('class'=>'w-200')); ?>
        </div>
    </div>

    <div class="row buttons" style="padding-left: 159px;">
        <?php $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType'=>'submit',
            'label'=>'Search',
            'type'=>'null', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
            'size'=>'small', // null, 'large', 'small' or 'mini'
            //'htmlOptions' => array('style' => 'margin-bottom: 10px; float: right;'),
        )); ?>	
    </div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->
<script>
$(function(){
    fnBindChangeProvince('<?php echo BLOCK_UI_COLOR;?>', "<?php echo Yii::app()->createAbsoluteUrl('admin/ajax/get_slt_district');?>");
});
</script>