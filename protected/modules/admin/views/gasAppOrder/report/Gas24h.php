<?php
$this->breadcrumbs=array(
    $this->pageTitle,
);
?>
<?php include 'Gas24hButton.php'; ?>
<?php if(!isset($_GET['type'])): ?>
    <?php include 'Gas24hAll.php'; ?>
<?php elseif ($_GET['type'] == 2): ?>
    <?php include 'Gas24hTop.php'; ?>
<?php else: ?>
    <?php include 'Gas24hAgent.php'; ?>
<?php endif; ?>
<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/print-store-card.css" />
<script>
$(document).ready(function() {
    $('.form').find('button:submit').click(function(){
        $.blockUI({ overlayCSS: { backgroundColor: '#fff' } }); 
    });
});

</script>