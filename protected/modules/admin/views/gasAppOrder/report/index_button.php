<div class="form">
    <?php
        $LinkNormal = Yii::app()->createAbsoluteUrl('admin/gasAppOrder/daily');
        $LinkSum    = Yii::app()->createAbsoluteUrl('admin/gasAppOrder/daily', array( 'summary'=> 1));
    ?>
    <h1><?php echo $this->pageTitle;?>
        <a class='btn_cancel f_size_14 <?php echo (!isset($_GET['summary'])) ? "active":"";?>' href="<?php echo $LinkNormal;?>">Chi tiết</a>
        <a class='btn_cancel f_size_14 <?php echo isset($_GET['summary']) && $_GET['summary']==1 ? "active":"";?>' href="<?php echo $LinkSum;?>">Tổng quát</a>
    </h1> 
</div>