
<?php 
$aNameParam         = isset($rData['aNameParam']) ? $rData['aNameParam'] : [];
$aActiveUser        = isset($rData['aActiveUser']) ? $rData['aActiveUser'] : [];
$aTotalOrder        = isset($rData['aTotalOrder']) ? $rData['aTotalOrder'] : [];
$aTotalOrderCancel  = isset($rData['aTotalOrderCancel']) ? $rData['aTotalOrderCancel'] : [];
$aKey               = isset($rData['KEY']) ? $rData['KEY']  : [];
$aPromotionDone     = isset($rData['PROMOTION_DONE']) ? $rData['PROMOTION_DONE']  : [];
$aPromotionSum      = isset($rData['PROMOTION_SUM']) ? $rData['PROMOTION_SUM']  : [];
$aPromotion = $rData['aPromotion'];
$aSumKey = [];
foreach ($aNameParam as $paramName => $paramText):
    foreach ($aKey as $key => $nTime):
        $value = isset($rData[$paramName][$key]) ? $rData[$paramName][$key] : '';
        if(!isset($aSumKey[$paramName])){
            $aSumKey[$paramName] = $value;
        }else{
            $aSumKey[$paramName] += $value;
        }
    endforeach;
endforeach;

?>
<!--<p class="item_b">Chú thích: KH kích hoạt / KH đặt thành công</p>
<p class="item_b">1 KH đặt thành công nhiều lần trong 1 tháng được tính 1 lần</p>-->

<table class="tb hm_table f_size_15">
    <thead>
        <tr>
            <th class="item_b">Tháng</th>
            <th class="item_b">Sum</th>
            <?php foreach ($aKey as $key => $nTime): ?>
            <?php // $temp = explode('-', $key) ?>
            <th class="item_b"><?php echo substr($key, -5, 2); // $key Y-m-d ?></th>
            <?php endforeach; ?>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($aNameParam as $paramName => $paramText): ?>
        <?php 
//            $saleName   = isset($aModelUser[$sale_id]) ? $aModelUser[$sale_id]->first_name : '' ;
//            $sumActive  = $sumOrders = 0;
//            $sumOrderDistinct   = isset($aSumOrders[$sale_id]) ? $aSumOrders[$sale_id] : '';
        ?>
        <tr>
            <td class="item_b"><?php echo $paramText;?></td>
            <td class="item_r item_b f_size_15 w-50"><?php echo isset($aSumKey[$paramName]) ? ActiveRecord::formatCurrency($aSumKey[$paramName]) : ''; ?></td>
            <?php foreach ($aKey as $key => $nTime): ?>
            <?php 
                $value      = isset($rData[$paramName][$key]) ? ActiveRecord::formatCurrency($rData[$paramName][$key]) : '';
                if(isset($rData['aCustomerMoreThanOne'][$key]) && $paramName == 'aTotalMoreThanOne'){
                    $link = Yii::app()->createAbsoluteUrl('admin/sell/index', ['sCustomer' => implode(',', $rData['aCustomerMoreThanOne'][$key])]);
                    $value = "<a href='$link' target='_blank'>$value</a>";
                }
            ?>
            <td class="item_r w-50"><?php echo $value;?></td>
            <?php endforeach; ?>
        </tr>
        <?php endforeach; ?>
        <!--for promotion used-->
        <?php foreach ($aPromotionDone as $promotion_id => $aInfo): ?>
        <?php 
            $name = '';
            if(isset($aPromotion[$promotion_id])){
//                $name = $aPromotion[$promotion_id]->code_no. ' - '. $aPromotion[$promotion_id]->title;
                $name = $aPromotion[$promotion_id]->code_no;
            }
        ?>
        <tr>
            <td class="item_b"><?php echo $name;?></td>
            <td class="item_r item_b f_size_15 w-50"><?php echo isset($aPromotionSum[$promotion_id]) ? ActiveRecord::formatCurrency($aPromotionSum[$promotion_id]) : ''; ?></td>
            <?php foreach ($aKey as $key => $nTime): ?>
            <?php 
                $value = isset($rData['PROMOTION_DONE'][$promotion_id][$key]) ? ActiveRecord::formatCurrency($rData['PROMOTION_DONE'][$promotion_id][$key]) : '';
            ?>
            <td class="item_r w-50"><?php echo $value;?></td>
            <?php endforeach; ?>
        </tr>
        <?php endforeach; ?>
        
    </tbody>
</table>
<?php include 'Gas24hDaily.php'; ?>
