<?php
$this->breadcrumbs=array(
    $this->pageTitle,
);
?>
<h1><?php echo $this->pageTitle; ?></h1>
<?php 
    $aOutput        = isset($rData['OUTPUT']) ? $rData['OUTPUT'] : [];
    $mHistorySms    = new GasScheduleSmsHistory();
    $aNetwork       = $mHistorySms->getArrayNetwork();
    $sumCustomer = $sumAmount = 0;
    
    $mUsersSms = new UsersSms();
?>
<div class="search-form ">
<?php include '_searchEtaSms.php'; ?>
</div><!-- search-form -->
<table class="tb hm_table f_size_15">
    <thead>
        <th>Nhà mạng</th>
        <th>Số KH</th>
        <th>Đơn giá</th>
        <th>Thành tiền</th>
    </thead>
    <tbody>
        <?php foreach ($aOutput as $network_id => $total): ?>
        <tr>
            <td>
                <?php echo isset($aNetwork[$network_id]) ? $aNetwork[$network_id] : ''; ?>
            </td>
            <td class="item_c">
                <?php echo ActiveRecord::formatCurrency($total); ?>
            </td>
            <td class="item_c">
                <?php 
                    $price = GasScheduleSmsHistory::PRICE_VIETTEL;
                    if($network_id != GasScheduleSms::NETWORK_VIETTEL){
                        $price = GasScheduleSmsHistory::PRICE_OTHER;
                    }
                    echo $price;
                ?>
            </td>
            <td class="item_r">
                <?php echo ActiveRecord::formatCurrency($total*$price); ?>
            </td>
        </tr>
        <?php   $sumCustomer += $total;
                $sumAmount += ($total*$price);
        ?>
        <?php endforeach; ?>
    </tbody>
    <tfoot>
        <tr>
            <td></td>
            <td class="item_r">
                <?php echo ActiveRecord::formatCurrency($sumCustomer); ?>
            </td>
            <td></td>
            <td class="item_r">
                <?php echo ActiveRecord::formatCurrency($sumAmount); ?>
            </td>
        </tr>
    </tfoot>
</table>
<table class="tb hm_table f_size_15">
    <tbody>
        <tr>
            <td>Số KH cài App</td>
            <td class="item_c"><?php echo $mUsersSms->countOfCode(UsersSms::REPORT_INSTALL_APP); ?></td>
        </tr>
        <tr>
            <td>KH đặt hàng</td>
            <td class="item_c"><?php echo $mUsersSms->countOfCode(UsersSms::REPORT_MAKE_ORDER); ?></td>
        </tr>
    </tbody>
</table>


<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/print-store-card.css" />
<script>
</script>