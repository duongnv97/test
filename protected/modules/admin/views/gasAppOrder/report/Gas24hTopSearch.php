<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=> Yii::app()->createAbsoluteUrl('admin/gasAppOrder/reportGas24h', ['type'=>2]),
	'method'=>'get',
)); ?>
    <div class='row'>
        <?php echo $form->labelEx($model,'Loại'); ?>
        <?php echo $form->dropDownList($model,'type', $mRpGas24h->getTypeSearch(),array('style'=>'','class'=>'w-200','empty'=>'Select')); ?>
    </div>
    <div class='row'>
        <?php echo $form->labelEx($model,'Top'); ?>
        <?php echo $form->dropDownList($model,'parent_id', $mRpGas24h->getTopList(),array('style'=>'','class'=>'w-200','empty'=>'Select')); ?>
    </div>
    <div class='row'>
        <?php echo $form->labelEx($model,'Sắp xếp'); ?>
        <?php echo $form->dropDownList($model,'agent_id_search', $mRpGas24h->getSort(),array('style'=>'','class'=>'w-200','empty'=>'Select')); ?>
    </div>
    <div class="row buttons" style="padding-left: 159px;">
        <?php $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType'=>'submit',
            'label'=>'Search',
            'type'=>'null', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
            'size'=>'small', // null, 'large', 'small' or 'mini'
            //'htmlOptions' => array('style' => 'margin-bottom: 10px; float: right;'),
        )); ?>	
    </div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->