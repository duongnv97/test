<?php
$this->breadcrumbs=array(
    $this->pageTitle,
);
$menus=array(
    array('label'=> 'Xuất Excel Danh Sách Hiện Tại',
            'url'=>array('daily', 'ExportExcel'=>1), 
            'htmlOptions'=>array('class'=>'export_excel ','label'=>'Xuất Excel')),
);
$this->menu= ControllerActionsName::createMenusRoles($menus, $actions);
$cRole = MyFormat::getCurrentRoleId();
Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('gas-app-order-grid', {
                url : $(this).attr('action'),
		data: $(this).serialize()
	});
	return false;
});
");

Yii::app()->clientScript->registerScript('ajaxupdate', "
$('#gas-app-order-grid a.ajaxupdate').live('click', function() {
    $.fn.yiiGridView.update('gas-app-order-grid', {
        type: 'POST',
        url: $(this).attr('href'),
        success: function() {
            $.fn.yiiGridView.update('gas-app-order-grid');
        }
    });
    return false;
});
");
?>

<h1><?php echo $this->pageTitle;?></h1>
<?php $this->widget('ListCronExcelWidget', array('model'=>$model, 'nameDir' => CronExcel::DIR_BOMOI_DAILY)); ?>
<?php echo CHtml::link('Tìm Kiếm Nâng Cao','#',array('class'=>'search-button')); ?>
<div class="search-form ">
<?php include '_search.php'; ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'gas-app-order-grid',
	'dataProvider'=>$model->searchDaily(),
        'afterAjaxUpdate'=>'function(id, data){ fnUpdateColorbox();}',
        'template'=>'{pager}{summary}{items}{pager}{summary}',
//    'itemsCssClass' => 'items custom_here',
//    'htmlOptions'=> array('class'=>'grid-view custom_here'),
        'pager' => array(
            'maxButtonCount'=>  CmsFormatter::$PAGE_MAX_BUTTON,
        ),
	'enableSorting' => false,
	//'filter'=>$model,
	'columns'=>array(
        array(
            'header' => 'S/N',
            'type' => 'raw',
            'value' => '$this->grid->dataProvider->pagination->currentPage * $this->grid->dataProvider->pagination->pageSize + ($row+1)',
            'headerHtmlOptions' => array('width' => '10px','style' => 'text-align:center;'),
            'htmlOptions' => array('style' => 'text-align:center;')
        ),
            array(
                'name' =>'date_delivery',
                'type' =>'raw',
                'value' =>'$data->getDateDelivery()."<br>".$data->getArrayDeliveryText()',
                'htmlOptions' => array('class' => 'w-50'),
            ),
            array(
                'name' =>'agent_id',
                'type' =>'raw',
                'value' =>'$data->getAgentDisplayGrid()',
                'htmlOptions' => array('class' => 'w-80'),
            ),
            array(
                'header' =>'Mã kế toán',
                'type' =>'raw',
                'value' =>'$data->getCustomer("code_account")."<br>".$data->getStatusText()',
                'htmlOptions' => array('style' => 'text-align:center;'),
            ),
            array(
                'name' =>'customer_id',
                'type' =>'raw',
                'value' =>'$data->getCustomerInfoV1()',
                'htmlOptions' => array('class' => 'w-150'),
            ),
            
            array(
                'header' =>'Gas đi',
                'type' =>'raw',
                'value' =>'$data->getStringGasReportDaily()',
                'htmlOptions' => array('class' => 'f_size_13 '),
            ),
            array(
                'header' =>'SL',
                'type' =>'raw',
                'value' =>'$data->sListGasQty',
                'htmlOptions' => array('class' => 'f_size_13 ','style' => 'text-align:center;')
            ),
            array(
                'header' =>'Giá bán',
                'type' =>'raw',
                'value' =>'$data->sListPrice',
                'htmlOptions' => array('class' => 'f_size_13 ', 'style' => 'text-align:right;')
            ),
            array(
                'header' =>'Gas dư',
                'type' =>'raw',
                'value' =>'$data->sListRemain',
                'htmlOptions' => array('style' => 'text-align:right;', 'class' => 'w-30 f_size_13'),
            ),
            array(
                'header' =>'Gas dư tiền',
                'type' =>'raw',
                'value' =>'"<b>".($data->total_gas_du > 0 ? $data->getTotalGasDu(true) : "") ."</b>"',
                'htmlOptions' => array('style' => 'text-align:right;', 'class' => 'w-30 f_size_13'),
            ),
            array(
                'header' =>'Qty',
                'type' =>'raw',
                'value' =>'$data->sListQty',
                'htmlOptions' => array('class' => 'f_size_13 ', 'style' => 'text-align:right;')
            ),
            array(
                'header' =>'Giảm giá',
                'type' =>'raw',
                'value' =>'$data->discount != 0 ? $data->getDiscount(true) : "" ',
                'htmlOptions' => array('class' => ' ', 'style' => 'text-align:right;')
            ),
            array(
                'header' =>'Thành tiền',
                'type' =>'raw',
                'value' =>'"<b>".$data->getTotalGas(true)."</b>"',
                'htmlOptions' => array('class' => 'f_size_13 ', 'style' => 'text-align:right;')
            ),
            array(
                'header' =>'Vỏ về',
                'type' =>'raw',
                'value' =>'"<b>".$data->getStatusDebit()."</b><br>".$data->sListVo',
                'htmlOptions' => array('class' => 'f_size_13 ')
            ),
            array(
                'header' =>'SL',
                'type' =>'raw',
                'value' =>'$data->sListVoQty',
                'htmlOptions' => array('class' => 'f_size_13 ', 'style' => 'text-align:center;')
            ),
//            array(
//                'header' =>'Thu tiền',
//                'value' =>'"- - -"',
//            ),
            
            array(
                'header' =>'Người giao',
                'type' =>'raw',
                'value' =>'$data->getNameDriverApp()."<br>".$data->getNameCar()',
            ),
	),
)); ?>

<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/jquery.floatThead.min.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/underscore-min.js"></script>    
<script>
$(document).ready(function() {
    fnUpdateColorbox();
});

function fnUpdateColorbox(){   
    fixTargetBlank();
    fnAddSumTr();
    $('.items').floatThead();
    <?php if($cRole != ROLE_ADMIN):?>
    $('body').on('click', '.export_excel', function(){
        $(this).remove();
    });
    <?php endif;?>
}

function fnAddSumTr(){
    var tr='';
    var $sumQtyGas = $sumRemain = $sumWeightGas = $sumTotalGas = $sumQtyVo = $sumRemainAmount= 0;
    
    $('.sumQtyGas').each(function(){
        if($.trim($(this).text())!='')
            $sumQtyGas += parseFloat(''+$(this).text());
    });
    $('.sumRemain').each(function(){
        if($.trim($(this).text())!='')
            $sumRemain+= parseFloat(''+$(this).text());
    });
    $('.sumWeightGas').each(function(){
        if($.trim($(this).text())!='')
            $sumWeightGas+= parseFloat(''+$(this).text());
    });
    $('.sumTotalGas').each(function(){
        if($.trim($(this).text())!='')
            $sumTotalGas+= parseFloat(''+$(this).text());
    });
    $('.sumQtyVo').each(function(){
        if($.trim($(this).text())!='')
            $sumQtyVo+= parseFloat(''+$(this).text());
    });
    $('.sumRemainAmount').each(function(){
        if($.trim($(this).text())!='')
            $sumRemainAmount+= parseFloat(''+$(this).text());
    });

    $sumQtyGas       = Math.round($sumQtyGas * 100) / 100;
    $sumRemain       = Math.round($sumRemain * 100) / 100;
    $sumWeightGas    = Math.round($sumWeightGas * 100) / 100;
    $sumTotalGas     = Math.round($sumTotalGas * 100) / 100;
    $sumQtyVo        = Math.round($sumQtyVo * 100) / 100;
    $sumRemainAmount = Math.round($sumRemainAmount * 100) / 100;
    
    tr +='<tr class="f_size_18 odd sum_page_current">';
        tr +='<td class="item_r item_b" colspan="6">Tổng Cộng</td>';
        tr +='<td class="item_r item_b">'+commaSeparateNumber($sumQtyGas)+'</td>';
        tr +='<td></td>';
        tr +='<td class="item_r item_b">'+commaSeparateNumber($sumRemain)+'</td>';
        tr +='<td class="item_r item_b">'+commaSeparateNumber($sumRemainAmount)+'</td>';
        tr +='<td class="item_r item_b">'+commaSeparateNumber($sumWeightGas)+'</td>';
        tr +='<td></td>';
        tr +='<td class="item_r item_b">'+commaSeparateNumber($sumTotalGas)+'</td>';
        tr +='<td></td>';
        tr +='<td class="item_r item_b">'+commaSeparateNumber($sumQtyVo)+'</td>';
        tr +='<td></td>';
    tr +='</tr>';
    
    if($('.sum_page_current').size()<1){
        $('.items tbody').prepend(tr);
        $('.items tbody').append(tr);
    }
}

</script>