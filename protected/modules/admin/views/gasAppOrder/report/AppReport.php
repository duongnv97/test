<?php
$this->breadcrumbs=array(
    $this->pageTitle,
);
?>
<h1><?php echo $this->pageTitle; ?></h1>
<?php 
$aActive    = isset($rData['ACTIVE']) ? $rData['ACTIVE']  : [];
$aOrders    = isset($rData['ORDERS']) ? $rData['ORDERS']  : [];
$aSumOrders = isset($rData['SUM_ORDERS']) ? $rData['SUM_ORDERS']  : [];
$aSale      = isset($rData['SALE_ID']) ? $rData['SALE_ID']  : [];
$aKey       = isset($rData['KEY']) ? $rData['KEY']  : [];
$aModelUser = Users::getArrObjectUserByRole('', $aSale);
$sumCol = []; $sumAllActive = $sumAllOrders = 0;
$sale_id_select = isset($_GET['sale_id']) ? $_GET['sale_id'] : 1;
$selectSale = '';
?>
<p class="item_b">Chú thích: KH kích hoạt / KH đặt thành công</p>
<p class="item_b">1 KH đặt thành công nhiều lần trong 1 tháng được tính 1 lần</p>

<table class="tb hm_table f_size_15">
    <thead>
        <tr>
            <th class="item_b">Nhân viên</th>
            <?php foreach ($aKey as $key): ?>
            <?php $temp = explode('-', $key) ?>
            <th class="item_b"><?php echo $temp[0];?></th>
            <?php endforeach; ?>
            <th class="item_b">Tổng</th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($aSale as $sale_id): ?>
        <?php 
            $saleName   = isset($aModelUser[$sale_id]) ? $aModelUser[$sale_id]->first_name : '' ;
            $sumActive  = $sumOrders = 0;
            $sumOrderDistinct   = isset($aSumOrders[$sale_id]) ? $aSumOrders[$sale_id] : '';
            $sumAllOrders       += $sumOrderDistinct;
            $link = Yii::app()->createAbsoluteUrl('admin/gasAppOrder/appReport', ['sale_id'=>$sale_id]);
            $link = "<a href='$link'>$saleName</a>";
            if($sale_id == $sale_id_select){
                $selectSale = $saleName;
            }
        ?>
        <tr>
            <td class="item_b"><?php echo $link;?></td>
            <?php foreach ($aKey as $key): ?>
            <?php 
                $active = isset($aActive[$sale_id][$key]) ? $aActive[$sale_id][$key] : '';
                $order  = isset($aOrders[$sale_id][$key]) ? $aOrders[$sale_id][$key] : '';
                $text = $active." / $order";
                if(empty($active) && empty($order)){
                    $text = '';
                }
                $sumActive += $active;
                $sumOrders += $order;
                if(!isset($sumCol['active'][$key])){
                    $sumCol['active'][$key] = $active;
                }else{
                    $sumCol['active'][$key] += $active;
                }
                
                if(!isset($sumCol['orders'][$key])){
                    $sumCol['orders'][$key] = $order;
                }else{
                    $sumCol['orders'][$key] += $order;
                }
                
            ?>
            <td class="item_c"><?php echo $text;?></td>
            <?php endforeach; ?>
            <td class="item_c item_b"><?php echo $sumActive." / $sumOrderDistinct";?></td>
        </tr>
        <?php endforeach; ?>
        <tr>
            <td class="item_b">Tổng</td>
            <?php foreach ($aKey as $key): ?>
            <?php 
                $active = isset($sumCol['active'][$key]) ? $sumCol['active'][$key] : '';
                $order  = isset($sumCol['orders'][$key]) ? $sumCol['orders'][$key] : '';
                
                $text = $active." / $order";
                if(empty($active) && empty($order)){
                    $text = '';
                }
                $sumAllActive += $active;
//                $sumAllOrders += $order;
            ?>
            <td class="item_c item_b"><?php echo $text;?></td>
            <?php endforeach; ?>
            <td class="item_c item_b"><?php echo $sumAllActive." / $sumAllOrders";?></td>
        </tr>
    </tbody>
</table>
<?php include 'AppReportDetail.php'; ?>
<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/print-store-card.css" />
<script>
$(document).ready(function() {

});

</script>