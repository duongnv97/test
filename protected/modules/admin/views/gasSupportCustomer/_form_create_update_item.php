<div class="clr"></div>
<div class="row">
    <label>&nbsp</label>
    <table class="materials_table hm_table g_tb1 WrapRowQtyPriceAmount">
        <thead>
            <tr>
                <th class="item_c">#</th>
                <th class="item_code item_c">Tên thiết bị</th>
                <th class="item_c">Đơn Vị</th>
                <th class="item_c">Slg</th>
                <th class="item_c">Giá Tiền</th>
                <th class="item_c">Thành Tiền</th>
                <th class="item_c">% Đầu Tư
                    <?php echo $form->dropDownList($model->mDetail,'percent_draft', $model->getListOptionPercent(),array('class'=>'w-70 percent_draft')); ?>
                </th>
                <th class="item_c">Thành Tiền</th>
            </tr>
        </thead>
        <tbody>
            <?php
                $max_upload = GasSupportCustomer::MAX_ITEM;
                $max_upload_show = 5;
                $grand_total = 0;
            ?>
            <?php if(count($model->aModelDetail)):?>
            <?php foreach($model->aModelDetail as $key=>$item):?>
            <tr class="materials_row RowQtyPriceAmount">
                <td class="item_c order_no"></td>
                <td class="w-300 col_material">
                    <?php echo $form->hiddenField($item,'materials_id[]', array('class'=>'materials_id_hide','value'=>$item->materials_id)); ?>
                    <input class="float_l material_autocomplete w-250"  placeholder="Nhập mã hoặc tên vật tư" maxlength="100" value="<?php echo $item->getMaterialName();?>" type="text" readonly="true">
                    <?php echo $form->hiddenField($item,'id[]', array('class'=>'','value'=>$item->id)); ?>
                </td>
                <td class="item_c w-50">
                    <?php echo $item->unit; ?>
                </td>
                <td class="item_r w-50 r_padding_10">
                    <?php echo ActiveRecord::formatCurrency($item->qty); ?>
                </td>
                <td class="item_r w-100 r_padding_10">
                    <?php echo ActiveRecord::formatCurrency($item->price); ?>
                </td>
                <td class="item_r w-100">
                    <span class="item_b items_amount items_calc r_padding_10" data_gas="<?php echo $item->amount;?>"><?php echo ActiveRecord::formatCurrency($item->amount); ?></span>
                </td>
                <td class="item_c w-100">
                    <?php echo $form->dropDownList($item,'percent[]', $model->getListOptionPercent(),array('class'=>'w-70 support_percent', 
                         'options' => array($item->getPercent()=>array('selected'=>true))
                    )); ?>
                </td>
                <td class="item_b item_r amount_final"></td>
            </tr> 
            <?php $max_upload_show--;
                $grand_total += ($item->qty * $item->price);
            ?>
            <?php endforeach;?>
            <?php endif;?>
        </tbody>
        <tfoot>
            <tr>
                <td class="item_b item_r f_size_16" colspan="5">Tổng Cộng</td>
                <td class="item_b item_r f_size_16 items_grand_total r_padding_10" >
                    <?php echo ActiveRecord::formatCurrency($grand_total); ?>
                </td>
                <td></td><td></td>
            </tr>
        </tfoot>
    </table>
</div>
