<div class="row tb_file" >
    <?php echo $form->labelEx($model,'file_design', array('class'=>'custom_lb_required')); ?>
    <div class="float_l">
        <div>
            <a href="javascript:void(0);" class="text_under_none item_b" style="line-height:25px" onclick="fnBuildRowFile(this);">
                <img style="float: left;margin-right:8px;" src="<?php echo Yii::app()->theme->baseUrl;?>/images/add.png"> 
                Thêm Dòng
            </a>
        </div>
        <?php echo $form->error($model,'file_design'); ?>
        <table class="materials_table hm_table">
            <thead>
                <tr>
                    <th class="item_c">#</th>
                    <th class="item_code item_c"><?php echo $model->mGasFile->getAttributeLabel('file_name');?>. Cho phép <?php echo GasSupportCustomer::$AllowFilePdf;?>. Tên file không quá 100 ký tự</th>
                    <th>Xóa</th>
                </tr>
            </thead>
            <tbody>
                <?php include "_form_file_update.php";?>
                
                <?php for($i=1; $i<=( GasSupportCustomer::IMAGE_MAX_UPLOAD); $i++): ?>
                <?php $display = "";
                    if($i > GasSupportCustomer::IMAGE_MAX_UPLOAD_SHOW)
                        $display = "display_none";
                ?>
                <tr class="materials_row <?php echo $display;?>">
                    <td class="item_c order_no"></td>
                    <td class="item_l w-400">
                        <?php echo $form->fileField($model,'file_name[]',array('class'=>'input_file',)); ?>
                    </td>
                    <td class="item_c last"><span class="remove_icon_only"></span></td>
                </tr>
                <?php endfor;?>
            </tbody>
        </table>
        
    </div>
        
</div>
<div class="clr"></div>

<style>
</style>
<script>
    function fnBuildRowFile(this_){
        $(this_).closest('.tb_file').find('.materials_table').find('tr:visible:last').next('tr').show();
    }
</script>
    