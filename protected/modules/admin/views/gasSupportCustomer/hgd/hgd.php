<?php
$this->breadcrumbs=array(
	$this->adminPageTitle,
);

$menus=array(
            array('label'=>'Tạo Mới', 'url'=>array('create')),
);
$this->menu= ControllerActionsName::createMenusRoles($menus, $actions);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('gas-support-customer-grid', {
                url : $(this).attr('action'),
		data: $(this).serialize()
	});
	return false;
});
");

Yii::app()->clientScript->registerScript('ajaxupdate', "
$('#gas-support-customer-grid a.ajaxupdate').live('click', function() {
    $.fn.yiiGridView.update('gas-support-customer-grid', {
        type: 'POST',
        url: $(this).attr('href'),
        success: function() {
            $.fn.yiiGridView.update('gas-support-customer-grid');
        }
    });
    return false;
});
");
?>
<h1><?php echo $this->adminPageTitle;?></h1>
<?php echo CHtml::link('Tìm Kiếm Nâng Cao','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'gas-support-customer-grid',
	'dataProvider'=>$model->search(),
        'afterAjaxUpdate'=>'function(id, data){ fnUpdateColorbox();}',
        'template'=>'{pager}{summary}{items}{pager}{summary}',
//    'itemsCssClass' => 'items custom_here',
//    'htmlOptions'=> array('class'=>'grid-view custom_here'),
        'pager' => array(
            'maxButtonCount'=>  CmsFormatter::$PAGE_MAX_BUTTON,
        ),
	'enableSorting' => false,
	//'filter'=>$model,
	'columns'=>array(
        array(
            'header' => 'S/N',
            'type' => 'raw',
            'value' => '$this->grid->dataProvider->pagination->currentPage * $this->grid->dataProvider->pagination->pageSize + ($row+1)',
            'headerHtmlOptions' => array('width' => '10px','style' => 'text-align:center;'),
            'htmlOptions' => array('style' => 'text-align:center;')
        ),
//		'code_no',
        array(
            'name'=>'Chi Tiết',
            'type'=>"html",
            'value'=>'$data->SupportCustomerInfo()',
            'htmlOptions' => array('class' => 'w-300')
        ),
//		array(
//            'name' => 'customer_id',
//            'type' => 'html',
//            'value' => 'Users::GetInfoField($data->rCustomer, "first_name")."<br>". Users::GetInfoField($data->rCustomer, "address")',
//        ),
//        array(
//            'name' => 'type_customer',
//            'value'=>'$data->type_customer?CmsFormatter::$CUSTOMER_BO_MOI[$data->type_customer]:""',
//            'htmlOptions' => array('style' => 'text-align:center;')
//        ),
		array(
            'name' => 'item_name',
            'type' => 'html',
            'value' => '$data->formatGasSupportCustomerItemName()',
        ),
//		array(
//            'name' => 'note',
//            'type' => 'html',
//            'value' => 'nl2br($data->note)',
//            'htmlOptions' => array('style' => 'max-width:300px;')
//        ),
		array(
                    'name' => 'uid_login',
                    'type' => 'NameAndRole',
                    'value' => '$data->rUidLogin',
                ),
            
                array(
                    'headerHtmlOptions' => array('style' => 'width:20px;'),
                    'header' => 'Send Mail',
                    'class'=>'CButtonColumn',
                    'visible'=> GasSupportCustomer::ShowColTechSendMail(),
                    'template'=> ControllerActionsName::createIndexButtonRoles($actions, array('create')),
                    'buttons'=>array(
                        'create'=>array(
                            'label'=>'Gửi mail cho NV kinh doanh',
                            'imageUrl'=>Yii::app()->theme->baseUrl . '/admin/images/mail.png',
                            'options'=>array('class'=>'mail_tech_create'),
                            'url'=>'Yii::app()->createAbsoluteUrl("admin/gasSupportCustomer/update",
                                array("id"=>$data->id) )',
                            'visible'=> '$data->ShowIconTechSendMail()',
                        ),
                    ),                          
                ),   
            
                array(
                    'name' => 'status',
                    'value' => 'GasSupportCustomer::GetStatusSupport($data)',
                    'htmlOptions' => array('style' => 'text-align:center;')
                ),
                array(
                    'header' => 'Last Update',
                    'type' => 'html',
                    'value' => '$data->getLastUpdateStatus()',
                ),
		/*
		'approved_uid_level_1',
		'approved_date_level_1',
		'approved_note_level_1',
		'approved_uid_level_2',
		'approved_date_level_2',
		'approved_note_level_2',
		'uid_login',
		'created_date',
		*/
        array(
            'name' => 'created_date',
            'type' => 'Datetime',
            'htmlOptions' => array('style' => 'width:90px;')
        ),
        array(
            'header' => 'Actions',
            'class'=>'CButtonColumn',
            'template'=> ControllerActionsName::createIndexButtonRoles($actions, array('view','update', 'delete','print')),
            'buttons'=>array(
                'update'=>array(
                    'visible'=> 'GasCheck::CanUpdateSupportCustomer($data)',
                ),
                'delete'=>array(
                    'visible'=> 'GasCheck::canDeleteData($data)',
                ),
                'print'=>array(
                    'label'=>'Print hỗ trợ KH',
                    'imageUrl'=>Yii::app()->theme->baseUrl . '/admin/images/print-icon.png',
                    'options'=>array('class'=>''),
                    'url'=>'Yii::app()->createAbsoluteUrl("admin/gasSupportCustomer/print",
                        array("id"=>$data->id) )',
                    'visible'=> '$data->canPrint()',
                ),
            ),
		),
	),
)); ?>

<script>
$(document).ready(function() {
    fnUpdateColorbox();
});

function fnUpdateColorbox(){
    fnShowhighLightTr();
    fixTargetBlank();
    $(".view").colorbox({iframe:true,
        innerHeight:'1000', 
        innerWidth: '1100', escKey:false,close: "<span title='close'>close</span>"
    });
    fnBindSendMail();
}

// Oct 14, 2015
function fnBindSendMail(){
    $('.mail_tech_create').click(function(){
        if(confirm("Bạn chắc chắn muốn gửi mail cho NV Kinh Doanh?")){
            fnRunBlockUi('<?php echo BLOCK_UI_COLOR;?>');
            var this_ = $(this);
            $.ajax({
                url: $(this).attr('href'),
                type:'post',
                data:{'ajax_update_send_mail':1},
                success:function(data){
                    this_.remove();
                    $.unblockUI();
                }
            });
        }
        return false;
    });
}

</script>