<div class="row tb_file" >
    <?php echo $form->labelEx($model,'file_design', array('class'=>'')); ?>
    <div class="float_l">
        <?php echo $form->error($model,'file_design'); ?>
        <table class="materials_table hm_table">
            <thead>
                <tr>
                    <th class="item_c">#</th>
                    <th class="item_code item_c"><?php echo $model->mGasFile->getAttributeLabel('file_name');?>. Cho phép <?php echo GasSupportCustomer::$AllowFilePdf;?></th>
                </tr>
            </thead>
            <tbody>
                <?php foreach($model->getFile() as $mFile):?>
                    <tr class="materials_row">
                        <td class="item_c order_no"></td>
                        <td class="item_l w-400">
                            <?php echo $mFile->getForceLinkDownload(); ?>
                        </td>
                    </tr>
                <?php endforeach;?>
            </tbody>
        </table>
        
    </div>
        
</div>
<div class="clr"></div>

<style>
</style>
<script>
    function fnBuildRowFile(this_){
        $(this_).closest('.tb_file').find('.materials_table').find('tr:visible:last').next('tr').show();
    }
</script>
    