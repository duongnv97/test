<?php
    $CanUpdate = true;
    $Level4Update = false;
    $DisplayApproved = '';
    $cRole = Yii::app()->user->role_id;
    $cUid = Yii::app()->user->id;
    $aStatus = GasSupportCustomer::$ARR_APPROVED_1;
    $field_name = 'approved_note_level_1';
    if($cRole == ROLE_DIRECTOR_BUSSINESS){
        $aStatus = GasSupportCustomer::$ARR_APPROVED_2;
        $field_name = 'approved_note_level_2';
    }
    
//    Close Sep 29, 2015    
//    if( ($model->sale_update==0 && $model->sale_id!=$model->uid_login ) ||
//        (in_array($cRole, GasSupportCustomer::$aRoleLevel1) && in_array($model->status, GasSupportCustomer::$ARR_NOT_ALLOW)) ){
//        $CanUpdate = false;
//    }
    
    // Sep 15, 2015 - Giám Đốc Long duyệt lần 3
    if($cRole == ROLE_DIRECTOR){
        $aStatus = GasSupportCustomer::$ARR_APPROVED_3;
        $field_name = 'approved_note_level_3';
        $model->to_uid_approved = GasLeave::UID_DIRECTOR;
        $DisplayApproved = 'display_none';
    }
    
//    Close Sep 29, 2015
//    if( in_array($cRole, GasSupportCustomer::$aRoleLevel2) && in_array($model->status, GasSupportCustomer::$ARR_NOT_ALLOW_LV2)){
//        $CanUpdate = false;
//    }
    // Sep 15, 2015 - Giám Đốc Long duyệt lần 3
    $aUidApproved = $model->getListUidApproved();
    $model->HideSomeRoleWhenUpdate($aUidApproved);
    if( $model->to_uid_approved != $cUid){
        $CanUpdate = false;
    }

    if( $model->CanUpdateLevel4()){
        $CanUpdate = true;
        $Level4Update = true;
        $DisplayApproved = 'display_none';
        $aStatus = GasSupportCustomer::$ARR_APPROVED_4;
        $field_name = 'approved_note_level_4';
        $model->to_uid_approved = GasLeave::UID_DIRECTOR;
    }
    $model->CanUpdateStatusNew($CanUpdate);
    
//    $CanUpdate = true; // Dec 28, 2015 for test only
//    $Level4Update = true; // Dec 28, 2015  for test only
?>

<?php if($CanUpdate):?>
<div class="form">
<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'gas-gasSupportCustomer',
	'action' => Yii::app()->createAbsoluteUrl('admin/gasSupportCustomer/update_status', array('id' => $model->id )),
	'enableAjaxValidation'=>false,
        'htmlOptions' => array('enctype' => 'multipart/form-data'),
)); ?>

    <p class="note item_b">Duyệt Đề Xuất:</p>
    <?php echo MyFormat::BindNotifyMsg(); ?>
    <?php echo $form->errorSummary($model); ?>
    <div class="row buttons" style="padding-left: 141px;">
        <?php $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType'=>'submit',
            'label'=> "Save",
            'type'=>'null', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
            'size'=>'small', // null, 'large', 'small' or 'mini'
            //'htmlOptions' => array('style' => 'margin-bottom: 10px; float: right;'),
        )); ?>
        <input class='cancel_iframe' type='button' value='Close'>
    </div>
    
    <div class="row <?php echo $DisplayApproved;?>">
        <?php echo $form->labelEx($model,'to_uid_approved'); ?>
        <?php echo $form->dropDownList($model,'to_uid_approved', $aUidApproved, array('class'=>'w-200', 'empty'=>'Select')); ?>
        <?php echo $form->error($model,'to_uid_approved'); ?>
    </div>
    
    <div class="row">
        <?php echo $form->labelEx($model,'status'); ?>
        <?php echo $form->dropDownList($model,'status', $aStatus, array('class'=>'w-200', 'empty'=>'Select')); ?>
        <?php echo $form->error($model,'status'); ?>
    </div>
    
    <div class="row text_area_16">
        <?php echo $form->labelEx($model,$field_name); ?>
        <?php echo $form->textArea($model,$field_name,array("placeholder"=>"Nhập ghi chú")); ?>
        <?php echo $form->error($model,$field_name); ?>
    </div>
    
    <?php if($Level4Update) { 
            include '_update_status_checkbox.php'; 
            include '_update_status_file.php'; 
        }
    ?>
    <?php include '_update_status_item.php';?>
    
    <div class="row buttons" style="padding-left: 141px;">
        <?php $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType'=>'submit',
            'label'=> "Save",
            'type'=>'null', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
            'size'=>'small', // null, 'large', 'small' or 'mini'
            //'htmlOptions' => array('style' => 'margin-bottom: 10px; float: right;'),
        )); ?>
        <input class='cancel_iframe' type='button' value='Close'>
    </div>

<?php $this->endWidget(); ?>

</div><!-- form -->
<?php endif;?>