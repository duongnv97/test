<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=> GasCheck::getCurl(),
	'method'=>'get',
)); ?>
	<div class="row display_none">
            <?php echo $form->label($model,'code_no',array()); ?>
            <?php echo $form->textField($model,'code_no',array('class'=>'w-500','maxlength'=>30)); ?>
	</div>
    
        <div class="row">
		<?php echo $form->label($model,'customer_id',array()); ?>
		<?php echo $form->hiddenField($model,'customer_id', array('class'=>'')); ?>
                <?php
                // 1. limit search kh của sale
                $url = Yii::app()->createAbsoluteUrl('admin/ajax/search_user_by_code');
                // widget auto complete search user customer and supplier
                $aData = array(
                    'model'=>$model,
                    'field_customer_id'=>'customer_id',
                    'url'=> $url,
                    'name_relation_user'=>'rCustomer', 
                    'ClassAdd' => 'w-500',
                );
                $this->widget('ext.SpaAutocompleteCustomer.SpaAutocompleteCustomer',
                    array('data'=>$aData));                                        
                ?>                
	</div>
    
        <div class="row">
            <?php echo $form->labelEx($model,'uid_login'); ?>
            <?php // echo $form->dropDownList($model,'ext_sale_id', Users::getArrUserByRole(ROLE_SALE, array('order'=>'t.code_bussiness', 'prefix_type_sale'=>1)),array('class'=>'w-250', 'style'=>'width:376px')); ?>
            <?php echo $form->hiddenField($model,'uid_login'); ?>
            <?php 
                // widget auto complete search user customer and supplier
                $aData = array(
                    'model'=>$model,
                    'field_autocomplete_name'=>'autocomplete_name_second',
                    'field_customer_id'=>'uid_login',
                    'name_relation_user'=>'rUidLogin',                    
                    'url' => Yii::app()->createAbsoluteUrl('admin/ajax/search_user_login', ['PaySalary' => 1]),
                    'ClassAdd' => 'w-500',
                );
                $this->widget('ext.SpaAutocompleteCustomer.SpaAutocompleteCustomer',
                    array('data'=>$aData));                                        
            ?>
        </div>
    
        <div class="row">
            <?php echo $form->labelEx($model,'deloy_by'); ?>
            <?php echo $form->hiddenField($model,'deloy_by', array('class'=>'')); ?>
            <?php
                // 1. limit search kh của sale
                $url = Yii::app()->createAbsoluteUrl('admin/ajax/searchLimitByFunction', array('type'=>  GasOneManyBig::TYPE_DELOY_BY));
                // widget auto complete search user customer and supplier
                $aData = array(
                    'model'=>$model,
                    'field_customer_id'=>'deloy_by',
                    'url'=> $url,
                    'name_relation_user'=>'rDeloyBy',
                    'field_autocomplete_name'=>'autocomplete_name_five',
                    'ClassAdd' => 'w-500',
                );
                $this->widget('ext.SpaAutocompleteCustomer.SpaAutocompleteCustomer',
                    array('data'=>$aData));                                        
            ?>

            <?php echo $form->error($model,'deloy_by'); ?>
        </div>
    
        <div class="row more_col">
            <div class="col1">
                <?php echo $form->label($model,'status'); ?>
                <?php echo $form->dropDownList($model,'status', GasSupportCustomer::$ARR_APPROVED, array('class'=>'w-200', 'empty'=>'Select')); ?>
            </div>
            <div class="col2">
                <?php echo $form->label($model,'type_customer',array()); ?>
                    <?php echo $form->dropDownList($model,'type_customer', CmsFormatter::$CUSTOMER_BO_MOI,array('class'=>'w-200', 'empty'=>'Select')); ?>
            </div>
        </div>
    
        <div class="row more_col">
            <div class="col1">
                <?php echo $form->label($model,'chk_design_wrong',array('label'=>'Lý do không thực hiện' )); ?>
                <?php echo $form->dropDownList($model,'chk_design_wrong', $model->getArrayNotDoing(), array('class'=>'w-200','empty'=>'Select')); ?>
            </div>
            <div class="col2">
                <?php echo $form->label($model,'action_invest',array()); ?>
                    <?php echo $form->dropDownList($model,'action_invest', $model->getListActionInvest(),array('class'=>'w-200', 'empty'=>'Select')); ?>
            </div>
        </div>
    
    <div class="row more_col">
        <div class="col1">
                <?php echo $form->label($model,'Tạo từ ngày'); ?>
                <?php 
                    $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                        'model'=>$model,        
                        'attribute'=>'date_created_from',
                        'options'=>array(
                            'showAnim'=>'fold',
                            'dateFormat'=> MyFormat::$dateFormatSearch,
//                            'minDate'=> '0',
                            'maxDate'=> '0',
                            'changeMonth' => true,
                            'changeYear' => true,
                            'showOn' => 'button',
                            'buttonImage'=> Yii::app()->theme->baseUrl.'/admin/images/icon_calendar_r.gif',
                            'buttonImageOnly'=> true,                                
                        ),        
                        'htmlOptions'=>array(
                            'class'=>'w-16 date_created_from',
                            'size'=>'16',
                            'style'=>'float:left;',                               
                        ),
                    ));
                ?>     		
        </div>
        <div class="col2">
                <?php echo $form->label($model,'Đến ngày'); ?>
                <?php 
                    $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                        'model'=>$model,        
                        'attribute'=>'date_created_to',
                        'options'=>array(
                            'showAnim'=>'fold',
                            'dateFormat'=> MyFormat::$dateFormatSearch,
//                            'minDate'=> '0',
                            'maxDate'=> '0',
                            'changeMonth' => true,
                            'changeYear' => true,
                            'showOn' => 'button',
                            'buttonImage'=> Yii::app()->theme->baseUrl.'/admin/images/icon_calendar_r.gif',
                            'buttonImageOnly'=> true,                                
                        ),        
                        'htmlOptions'=>array(
                            'class'=>'w-16',
                            'size'=>'16',
                            'style'=>'float:left;',
                        ),
                    ));
                ?>     		
        </div>
    </div>
    
    <div class="row more_col">
        <div class="col1">
                <?php echo Yii::t('translation', $form->label($model,'date_from', array('label'=>'Ngày duyệt'))); ?>
                <?php 
                    $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                        'model'=>$model,        
                        'attribute'=>'date_from',
                        'options'=>array(
                            'showAnim'=>'fold',
                            'dateFormat'=> MyFormat::$dateFormatSearch,
//                            'minDate'=> '0',
                            'maxDate'=> '0',
                            'changeMonth' => true,
                            'changeYear' => true,
                            'showOn' => 'button',
                            'buttonImage'=> Yii::app()->theme->baseUrl.'/admin/images/icon_calendar_r.gif',
                            'buttonImageOnly'=> true,                                
                        ),        
                        'htmlOptions'=>array(
                            'class'=>'w-16',
                            'size'=>'16',
                            'style'=>'float:left;',                               
                        ),
                    ));
                ?>     		
        </div>
    </div>
    
    <div class="row display_none">
        <?php echo $form->label($model,'type',array()); ?>
        <?php echo $form->dropDownList($model,'type', GasSupportCustomer::$ARR_TYPE,array('class'=>'w-500','empty'=>'Select')); ?>
    </div>

    <div class="row buttons" style="padding-left: 159px;">
        <?php $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType'=>'submit',
            'label'=>'Search',
            'type'=>'null', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
            'size'=>'small', // null, 'large', 'small' or 'mini'
            //'htmlOptions' => array('style' => 'margin-bottom: 10px; float: right;'),
        )); ?>	
    </div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->

<script>
    $(function(){
        $('.date_created_from').change(function(){
            var div = $(this).closest('.row');
            div.find('input').val($(this).val());
        });
       
    });
</script>