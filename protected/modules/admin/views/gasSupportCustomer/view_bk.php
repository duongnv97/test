<?php
Sta2::GetOutputOneCustomer($model->customer_id);
$this->breadcrumbs=array(
	$this->adminPageTitle=>array('index'),
	$model->rCustomer->first_name,
);

?>

<h1>Xem <?php echo $this->adminPageTitle;?>: <?php echo $model->rCustomer->first_name; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
                'code_no',
                array(
                    'name' => 'status',
                    'value' => GasSupportCustomer::GetStatusSupport($model),
                ),
                'date_request:date',
                array(
                    'name' => 'customer_id',
                    'type' => 'html',
                    'value' => Users::GetInfoField($model->rCustomer, "first_name")."<br>". Users::GetInfoField($model->rCustomer, "address"),
                ),
                array(
                    'name' => 'type_customer',
                    'value'=>$model->type_customer?CmsFormatter::$CUSTOMER_BO_MOI[$model->type_customer]:"",
                ),
                array(
                    'name' => 'note',
                    'type' => 'html',
                    'value' => nl2br($model->note),
                ),
                array(
                    'name' => 'item_name',
                    'type' => 'GasSupportCustomerItemName',
                    'value' => $model,
                ),
                array(
                    'name' => 'deloy_by',
                    'value' => Users::GetInfoField($model->rDeloyBy, "first_name"),
                ),
                array(
                    'name' => 'contact_person',
                ),
                array(
                    'name' => 'contact_tel',
                ),
                array(
                    'name' => 'time_doing',
                    'value' => $model->getTimeDoing(),
                ),
                array(
                    'name' => 'price_qty',
                ),
                array(
                    'name' => 'file_design',
                    'type' => 'html',
                    'value' => $model->renderListFile(),
                ),
        
                array(
                    'name' => 'approved_uid_level_1',
                    'type' => 'NameAndRole',
                    'value' => $model->rApprovedUidLevel1,
                ),
                'approved_date_level_1:datetime',
                array(
                    'name' => 'approved_note_level_1',
                    'type' => 'html',
                    'value' => nl2br($model->approved_note_level_1),
                ),
        
                array(
                    'name' => 'approved_uid_level_2',
                    'type' => 'NameAndRole',
                    'value' => $model->rApprovedUidLevel2,
                ),
                'approved_date_level_2:datetime',
                array(
                    'name' => 'approved_note_level_2',
                    'type' => 'html',
                    'value' => nl2br($model->approved_note_level_2),
                ),
        
                array(
                    'name' => 'approved_uid_level_3',
                    'type' => 'NameAndRole',
                    'value' => $model->rApprovedUidLevel3,
                ),
                'approved_date_level_3:datetime',
                array(
                    'name' => 'approved_note_level_3',
                    'type' => 'html',
                    'value' => nl2br($model->approved_note_level_3),
                ),
        
                array(
                    'name' => 'uid_login',
                    'type' => 'NameAndRole',
                    'value' => $model->rUidLogin,
                ),
                
                'created_date:datetime',
	),
)); ?>


<?php 
if(GasCheck::isAllowAccess("gasSupportCustomer", "update_status") && GasSupportCustomer::CanUpdateStatus($model)):
    include '_update_status.php';
    include '_statistic.php';
    include '_history.php';
endif;
?>

<script>
    $(window).load(function(){ 
        $('.form').find('button:submit').click(function(){
            $.blockUI({ overlayCSS: { backgroundColor: '#fff' } }); 
        });
        
        fnResizeColorbox();
        parent.$.fn.yiiGridView.update("gas-support-customer-grid"); 
    });
    
    function fnResizeColorbox(){
//        var y = $('body').height()+100;
        var y = $('#main_box').height()+100;
        parent.$.colorbox.resize({innerHeight:y});
    }
    $(document).ready(function(){
        $('.form').find('button:submit').click(function(){
            $.blockUI({ overlayCSS: { backgroundColor: '#fff' } }); 
        });        
    });
</script>