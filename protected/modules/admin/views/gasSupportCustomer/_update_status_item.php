<?php $mDetailItem = new GasSupportCustomerItem(); ?>
<div class="clr"></div>
<div class="row">
    <label>&nbsp</label>
    <table class="materials_table hm_table g_tb1 WrapRowQtyPriceAmount">
        <thead>
            <tr class="f_size_12">
                <th class="item_c" rowspan="2">#</th>
                <th class="item_code item_c" rowspan="2">Tên thiết bị</th>
                <th class="item_c" rowspan="2">Đơn Vị</th>
                <th class="item_c" rowspan="2">Slg</th>
                <th class="item_c" rowspan="2">Giá Tiền</th>
                <th class="item_c" rowspan="2">Thành Tiền</th>
                <th class="item_c" colspan="50">% Đầu Tư
                <?php echo $form->dropDownList($mDetailItem,'percent_draft', $model->getListOptionPercent(),array('class'=>'w-70 percent_draft')); ?>
                </th>
                <!--<th class="item_c">Loại</th>-->
            </tr>
            <tr class="f_size_12">
                <th>NV Kinh Doanh</th>
                <th>TP Bò/Mối</th>
                <th>GĐ Kinh Doanh</th>
                <th>Tổng GĐ</th>
                <th>Thành Tiền</th>
            </tr>
        </thead>
        <tbody>
            <?php 
                $max_upload = GasSupportCustomer::MAX_ITEM;
                $max_upload_show = 5;
                $grand_total = 0;
            ?>
            <?php if(count($model->aModelDetail)):?>
            <?php foreach($model->aModelDetail as $key=>$item):?>
            <tr class="materials_row RowQtyPriceAmount">
                <td class="item_c order_no"></td>
                <td class="w-300 col_material">
                    <?php echo $form->hiddenField($item,'materials_id[]', array('class'=>'materials_id_hide','value'=>$item->materials_id)); ?>
                    <input class="float_l material_autocomplete w-250"  placeholder="Nhập mã hoặc tên vật tư" maxlength="100" value="<?php echo $item->getMaterialName();?>" type="text" readonly="true">
                    <?php echo $form->hiddenField($item,'id[]', array('class'=>'','value'=>$item->id)); ?>
                </td>
                <td class="item_c w-50">
                    <span class=" items_unit"><?php echo $item->unit;?></span>
                </td>
                <td class="item_r w-50 r_padding_10">
                    <?php echo ActiveRecord::formatCurrency($item->qty); ?>
                </td>
                <td class="item_r w-80 r_padding_10">
                    <?php echo ActiveRecord::formatCurrency($item->price); ?>
                </td>
                <td class="item_r w-100">
                    <span class="item_b items_amount items_calc r_padding_10" data_gas="<?php echo $item->amount;?>"><?php echo ActiveRecord::formatCurrency($item->amount); ?></span>
                </td>
                <td class="item_c w-100">
                    <?php echo $item->getPercentText();?>
                </td>
                <td class="item_c w-100">
                    <?php echo $item->getHtmlPercentCol234($model, $form, 2, GasSupportCustomer::$aRoleLevel1); ?>
                </td>
                <td class="item_c w-100">
                    <?php echo $item->getHtmlPercentCol234($model, $form, 3, GasSupportCustomer::$aRoleLevel2); ?>
                </td>
                <td class="item_c w-100">
                    <?php echo $item->getHtmlPercentCol234($model, $form, 4, GasSupportCustomer::$aRoleLevel3); ?>
                </td>
                <td class="item_r amount_final"></td>
                
            </tr> 
            <?php $max_upload_show--;
                $grand_total += ($item->qty * $item->price);
            ?>
            <?php endforeach;?>
            <?php endif;?>
        </tbody>
        <tfoot>
            <tr>
                <td class="item_b item_r f_size_16" colspan="5">Tổng Cộng</td>
                <td class="item_b item_r f_size_16 items_grand_total r_padding_10" >
                    <?php echo ActiveRecord::formatCurrency($grand_total); ?>
                </td>
                <td colspan="4"></td>
            </tr>
        </tfoot>
    </table>
</div>
<script>
    $(document).ready(function(){
        fnRefreshOrderNumber();
    });
</script>