<?php 
/** Sẹp 17, 2015
 * 1. NV kỹ thuật nhập: Danh mục item, file, KH
 * 2. Sale update những thông tin còn lại + % đầu tư
 * 3. các cấp trên duyệt và update lại % đầu tư
 * @note: chỉ mail cho người duyệt và Sale
 */

$display_none = '';
$hideOnHgd = false;
$cRole = Yii::app()->user->role_id;
if(in_array($cRole, GasSupportCustomer::$aRoleCreateExt1)){// Jul 23, 2016
    $hideOnHgd = true;
} 
?>
<div class="form">
<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'gas-support-customer-form',
	'enableAjaxValidation'=>false,
        'htmlOptions' => array('enctype' => 'multipart/form-data'),
)); ?>

    <p class="note">Dòng có dấu <span class="required">*</span> là bắt buộc.</p>
    <?php echo MyFormat::BindNotifyMsg(); ?>
    <?php echo $form->errorSummary($model); ?>
    
    <?php if(in_array($cRole, GasSupportCustomer::$aRoleNewCreate)): ?>
        <?php include '_form_create.php';?>
    <?php else:?>
        <?php if( $model->isScenarioSaleUpdate() ): ?>
            <?php include '_form_create_update.php';?>
    
        <?php elseif( $model->CanUpdateTimeDoingReal() ):?>
            <?php include '_form_create_update_time_doing.php';?>
        <?php else:?>
            <?php 
            // dùng cho user đại lý tạo + trưởng phòng bò mối, Giám đốc KD
            include '_form_normal.php';
//            if(!$hideOnHgd){
                include '_form_update_print.php';
//            }else{
//                include '_form_row_autocomplete_deloyby.php';
//            }
            ?>
        <?php endif;?>
    <?php endif;?>
    
    <?php // include '_form_normal.php';?>
    <?php // include '_form_update_print.php';?>
    
    <?php // if($model->canUpdateToPrint()): $display_none = "display_none"; ?>
        <?php // include '_form_update_print.php';?>
    <?php // endif;?>
    <div class="<?php echo $display_none;?>">
        <?php // include '_form_normal.php';?>
    </div>
    
	<div class="row buttons" style="padding-left: 141px;">
            <?php $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType'=>'submit',
            'label'=>$model->isNewRecord ? 'Create' :'Save',
            'type'=>'null', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
            'size'=>'small', // null, 'large', 'small' or 'mini'
            //'htmlOptions' => array('style' => 'margin-bottom: 10px; float: right;'),
        )); ?>	
        </div>

<?php $this->endWidget(); ?>
</div><!-- form -->

    <?php if( $model->CanUpdateTimeDoingReal() ):?>
        <div class="f_size_14">
        <?php $model= GasSupportCustomer::model()->findByPk($model->id); include 'view_xdetail.php';?>
        </div>
    <?php endif;?>

<script>
    $(document).ready(function(){
        $('.form').find('button:submit').click(function(){
            $.blockUI({ overlayCSS: { backgroundColor: '#fff' } }); 
        });
        fnBindLabelRequired();
        fnRemoveDeleteIcon();
        fnBindAllAutocomplete();
        fnBindRemoveMaterialFix('materials_id_hide', 'material_autocomplete');
        $('.items_qty').trigger('change');
        $('.support_percent').trigger('change');
    });
    
    function fnBindLabelRequired(){
        $('.custom_lb_required').each(function(){
            var label = $(this);
            if(label.find('span.required').size()<1){
                label.append('<span class="required"> *</span>');
            }
        });
    }
    
    function fnRemoveDeleteIcon(){
        $('.RemoveDeleteIcon').closest('.unique_wrap_autocomplete').find('.remove_row_item').remove();
    }
    
    function fnBindAllAutocomplete(){
        // material
        $('.material_autocomplete').each(function(){
            bindMaterialAutocomplete($(this));
        });
    }
    
    // to do bind autocompelte for input material
    // @param objInput : is obj input ex  $('.customer_autocomplete')    
    function bindMaterialAutocomplete(objInput){
        var availableMaterials = <?php echo MyFunctionCustom::getMaterialsJsonByType(GasMaterialsType::$THIET_BI); ?>;
        var parent_div = objInput.closest('.col_material');
        objInput.autocomplete({
            source: availableMaterials,
//            close: function( event, ui ) { $( "#GasStoreCard_materials_name" ).val(''); },
            select: function( event, ui ) {
                parent_div.find('.materials_id_hide').val(ui.item.id);
                parent_div.find('.material_autocomplete').attr('readonly',true);
                fnSetValue(objInput, ui);
            }
        });
    }
    
    function fnSetValue(objInput, ui){
        var tr = objInput.closest('tr');
        tr.find('.items_price').val(ui.item.price);
        tr.find('.items_unit').text(ui.item.unit);
        tr.find('.items_qty').trigger("change");
    }
    
</script>

<script>
    $(document).keydown(function(e) {
        if(e.which == 119) {
            fnBuildRow();
        }
    });
    
    $(document).ready(function(){
        fnRefreshOrderNumber();
        fnBindRemoveIcon();
//        bindEventForHelpNumber();
    });
    
    function fnBuildRow(){
        $('.g_tb1 tbody').find('tr:visible:last').next('tr').show();
    }
    
    /**
    * @Author: ANH DUNG Jun 23, 2015
    * @Todo: function này dc gọi từ ext của autocomplete
    * @Param: $model
    */
    function fnDeloyBySelect(ui, idField, idFieldCustomer){
        if(typeof ui.item.name_role === 'undefined'){
            return;
        }
        var row = $(idField).closest('.row');
        var ClassCheck = "uid_"+ui.item.id;
        var td_name = '<td class="'+ClassCheck+'">'+ui.item.name_role+'</td>';
        var td_remove = '<td class="item_c last"><span class="remove_icon_only"></span></td>';
        var input = '<input name="deloy_by[]"  value="'+ui.item.id+'" type="hidden">';
        var tr = '<tr><td class="item_c order_no"></td>'+td_name+td_remove+input+'</tr>';
        if($('.tb_deloy_by tbody').find('.'+ClassCheck).size() < 1 ) {
            $('.tb_deloy_by tbody').append(tr);
        }
        fnRefreshOrderNumber();
    }
    
</script>
