<div class="row">
    <?php echo $form->labelEx($model,'time_doing_real'); ?>
    <?php Yii::import('application.extensions.CJuiDateTimePicker.CJuiDateTimePicker');
    $this->widget('CJuiDateTimePicker',array(
        'model'=>$model, //Model object
        'attribute'=>'time_doing_real', //attribute name
        'mode'=>'datetime', //use "time","date" or "datetime" (default)
        'language'=>'en-GB',
        'options'=>array(
            'showAnim'=>'fold',
            'showButtonPanel'=>true,
            'autoSize'=>true,
            'dateFormat'=>'dd/mm/yy',
            'timeFormat'=>'hh:mm:ss',
            'width'=>'120',
            'separator'=>' ',
            'showOn' => 'button',
            'buttonImage'=> Yii::app()->theme->baseUrl.'/admin/images/icon_calendar_r.gif',
            'buttonImageOnly'=> true,
            'changeMonth' => true,
            'changeYear' => true,
            //                'regional' => 'en-GB'
        ),
        'htmlOptions' => array(
            'style' => 'width:180px;',
            'readonly'=>'readonly',
        ),
    ));
    ?>
    <?php echo $form->error($model,'time_doing'); ?>
</div>

<div class="row text_area_16">
    <?php echo $form->labelEx($model,'note_update_real_time'); ?>
    <?php echo $form->textArea($model,'note_update_real_time',array("placeholder"=>"Nhập ghi chú")); ?>
    <?php echo $form->error($model,'note_update_real_time'); ?>
</div>

<style>
    /*div.form .row label { width: 190px;}*/
</style>
