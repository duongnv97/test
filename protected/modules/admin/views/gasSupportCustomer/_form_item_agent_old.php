<div class="row">
    <label>&nbsp</label>
    <div>
        <a href="javascript:void(0);" class="text_under_none item_b" style="line-height:25px" onclick="fnBuildRow();">
            <img style="float: left;margin-right:8px;" src="<?php echo Yii::app()->theme->baseUrl;?>/images/add.png"> 
            Thêm Dòng ( Phím tắt F8 )
        </a>
    </div>
</div>
<div class="clr"></div>
<div class="row">
    <label>&nbsp</label>
    <table class="materials_table hm_table g_tb1 WrapRowQtyPriceAmount">
        <thead>
            <tr>
                <th class="item_c">#</th>
                <th class="item_code item_c">Tên thiết bị</th>
                <th class="item_c">Đơn Vị</th>
                <th class="item_c">Slg</th>
                <th class="item_c">Giá Tiền</th>
                <th class="item_c">Thành Tiền</th>
                <th class="item_c">% Đầu Tư
                    <?php echo $form->dropDownList($model->mDetail,'percent_draft', $model->getListOptionPercent(),array('class'=>'w-70 percent_draft')); ?>
                </th>
                <th class="item_c">Thành Tiền</th>
                <th class="item_unit last item_c">Xóa</th>
            </tr>
        </thead>
        <tbody>
            <?php 
                $max_upload = GasSupportCustomer::MAX_ITEM;
                $max_upload_show = 5;
            ?>
            <?php if(count($model->aModelDetail)):?>
            <?php foreach($model->aModelDetail as $key=>$item):?>
            <tr class="materials_row RowQtyPriceAmount">
                <td class="item_c order_no"></td>
                <td class="w-300 col_material">
                    <?php echo $form->hiddenField($item,'materials_id[]', array('class'=>'materials_id_hide','value'=>$item->materials_id)); ?>
                    <input class="float_l material_autocomplete w-250"  placeholder="Nhập mã hoặc tên vật tư" maxlength="100" value="<?php echo $item->getMaterialName();?>" type="text" readonly="true">
                    <span onclick="" class="remove_material_js remove_item_material"></span>
                    
                    <?php echo $form->hiddenField($item,'id[]', array('class'=>'','value'=>$item->id)); ?>
                </td>
                <td class="item_c w-50">
                    <span class=" items_unit"><?php echo $item->unit;?></span>
                </td>
                <td class="item_c w-50">
                    <?php echo $form->textField($item,'qty[]', array('class'=>'w-60 item_r items_qty number_only_v1 items_calc', 'maxlength'=>'8', 'value'=> ActiveRecord::formatNumberInput($item->qty))); ?>
                </td>
                <td class="item_c w-100">
                    <?php // echo $form->textField($item,'price[]', array('class'=>'w-80 items_price items_calc number_only number_only_v1', 'maxlength'=>'8', 'value'=> ActiveRecord::formatNumberInput($item->price), 'readonly'=>1 )); ?>
                    <?php echo $form->textField($item,'price[]', array('class'=>'w-80 items_price items_calc number_only number_only_v1', 'maxlength'=>'8', 'value'=> ActiveRecord::formatNumberInput($item->price) )); ?>
                </td>
                <td class="item_r w-100">
                    <span class="item_b items_amount items_calc r_padding_10"></span>
                </td>
                <td class="item_c w-100">
                    <?php
                    if($item->id){
                        $item->percent = $item->getPercent();
                    }
                    ?>
                    <?php echo $form->dropDownList($item,'percent[]', $model->getListOptionPercent(),array('class'=>'w-70 support_percent', 
                         'options' => array($item->percent=>array('selected'=>true))
                    )); ?>
                </td>
                
<!--                <td class="item_c">
                    <?php echo $form->dropDownList($item,'type[]', GasSupportCustomer::$ARR_TYPE,array('class'=>'w-150',  'empty'=>'Select',
                         'options' => array($item->type=>array('selected'=>true))
                    )); ?>
                </td>-->
                <td class="item_b item_r amount_final"></td>
                <td class="item_c last"><span class="remove_icon_only"></span></td>
            </tr> 
            <?php $max_upload_show--;?>
            <?php endforeach;?>
            <?php endif;?>

            <?php for($i=1; $i<=($max_upload-count($model->aModelDetail)); $i++): ?>
            <?php $display = "";
                if($i>$max_upload_show)
                    $display = "display_none";
            ?>
            <tr class="materials_row RowQtyPriceAmount <?php echo $display;?>">
                <td class="item_c order_no"></td>
                <td class=" col_material w-300">
                    <?php echo $form->hiddenField($model->mDetail,'materials_id[]', array('class'=>'materials_id_hide')); ?>
                    <input class="float_l material_autocomplete w-250"  placeholder="Nhập mã hoặc tên vật tư" maxlength="100" value="" type="text">
                    <span onclick="" class="remove_material_js remove_item_material"></span>
                </td>
                <td class="item_c ">
                    <span class=" items_unit"></span>
                </td>
                <td class="item_c">
                    <?php echo $form->textField($model->mDetail,'qty[]', array('class'=>'w-60 item_r items_qty number_only_v1 items_calc', 'maxlength'=>'8')); ?>
                </td>
                <td class="item_c">
                    <?php // echo $form->textField($model->mDetail,'price[]', array('class'=>'w-80 items_price items_calc number_only number_only_v1', 'maxlength'=>'8', 'readonly'=>1)); ?>
                    <?php echo $form->textField($model->mDetail,'price[]', array('class'=>'w-80 items_price items_calc number_only number_only_v1', 'maxlength'=>'8')); ?>
                    <!--<div class="help_number"></div>-->
                </td>
                <td class="item_r">
                    <span class="item_b items_amount items_calc r_padding_10"></span>
                </td>
                <td class="item_c">
                    <?php echo $form->dropDownList($model->mDetail,'percent[]', $model->getListOptionPercent(), array('class'=>'w-70 support_percent')); ?>
                </td>
                <td class="item_b item_r amount_final"></td>
                
<!--                <td class="item_c">
                    <?php echo $form->dropDownList($model->mDetail,'type[]', GasSupportCustomer::$ARR_TYPE,array('class'=>'w-150', 'empty'=>'Select')); ?>
                </td>-->
                
                <td class="item_c last"><span class="remove_icon_only"></span></td>
            </tr>
            <?php endfor;?>
        </tbody>
        <tfoot>
            <tr>
                <td class="item_b item_r f_size_16" colspan="5">Tổng Cộng</td>
                <td class="item_b item_r f_size_16 items_grand_total r_padding_10" ></td>
                <td class="display_none"></td><td></td><td></td><td></td>
            </tr>
        </tfoot>
    </table>
</div>

<script>
//    $(document).keydown(function(e) {
//        if(e.which == 119) {
//            fnBuildRow();
//        }
//    });
//    
//    $(document).ready(function(){
//        fnRefreshOrderNumber();
//        fnBindRemoveIcon();
////        bindEventForHelpNumber();
//    });
//    
//    function fnBuildRow(){
//        $('.g_tb1 tbody').find('tr:visible:last').next('tr').show();
//    }
</script>