<?php
$this->breadcrumbs=array(
	$this->adminPageTitle=>array('index'),
	$model->rCustomer->first_name,
);
$menus = array(	
    array('label'=> $this->adminPageTitle, 'url'=>array('index')),
	array('label'=>'Xem', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Tạo Mới', 'url'=>array('create')),	
);
$this->menu= ControllerActionsName::createMenusRoles($menus, $actions);
if($model->type_customer == STORE_CARD_KH_BINH_BO){
    $mHeadKinhDoanh = Users::model()->findByPk(GasLeave::UID_HEAD_GAS_BO);
}else{
    $mHeadKinhDoanh = Users::model()->findByPk(GasLeave::UID_HEAD_GAS_MOI);
}
$mGDKinhDoanh = Users::model()->findByPk(GasLeave::UID_DIRECTOR_BUSSINESS);
$mHeadKinhDoanh->LoadUsersRefImageSign();

$mRootDirector = Users::model()->findByPk(GasLeave::UID_DIRECTOR);

$mCustomer = Users::model()->findByPk($model->customer_id);
$mSale = Users::model()->findByPk($mCustomer->sale_id);
$sale_name = $mSale ? $mSale->first_name : "";

$AdminUser = Users::model()->findByPk(2);
$AdminUser->LoadUsersRefImageSign();
?>

<h1>Xem và In <?php echo $this->adminPageTitle;?>: <?php echo $model->rCustomer->first_name; ?></h1>


<div class="row">
    <p><?php echo $model->getAttributeLabel('file_design'); ?></p>
    <?php echo $model->renderListFile();?>
</div>

<div class="sprint" style=" padding-right: 743px;">
    <a class="button_print" href="javascript:void(0);" title="In Đề xuất">
        <img src="<?php echo Yii::app()->theme->baseUrl; ?>/images/print.png">
    </a>
    <a class="l_padding_50 ShowHideText f_size_18 item_b" href="javascript:;" style="text-decoration: none;">Hide Print 1</a>
</div>
<div class="clr"></div>
<script  src="<?php echo Yii::app()->theme->baseUrl; ?>/js/jquery.printElement.min.js"></script>
<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/print-store-card.css" />
<script type="text/javascript">
	$(document).ready(function(){
            $(".button_print").click(function(){
                $('#printElement').printElement({ overrideElementCSS: ['<?php echo Yii::app()->theme->baseUrl;?>/css/print-store-card.css'] });
            });
            $(".button_print_2").click(function(){
                $('#printElement2').printElement({ overrideElementCSS: ['<?php echo Yii::app()->theme->baseUrl;?>/css/print-store-card.css'] });
            });
            BindClickShowHide();
	});
        
        function BindClickShowHide(){
            $('.ShowHideText').live('click',function(){
                $('.BoxPrint1').slideToggle();
           }); 
       }
</script>
<?php include "print_1.php"; ?>
<?php include "print_2.php"; ?>