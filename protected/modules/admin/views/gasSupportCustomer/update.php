<?php
$this->breadcrumbs=array(
	$this->adminPageTitle=>array($model->getActionIndex()),
	$model->rCustomer->first_name=>array('view','id'=>$model->id),
	'Cập Nhật',
);

$menus = array(	
        array('label'=>$this->adminPageTitle, 'url'=>array($model->getActionIndex())),
	array('label'=>'Xem', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Tạo Mới', 'url'=>array('create')),	
);
$this->menu= ControllerActionsName::createMenusRoles($menus, $actions);

?>

<h1>Cập Nhật <?php echo $this->adminPageTitle;?>: <?php echo $model->rCustomer->first_name; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>