<?php
    $aHistory = GasSupportCustomer::GetHistorySupport($model->customer_id, array( $model->id ));
    $count_history = count($aHistory);
?>
<?php if($count_history): ?>
    <?php foreach($aHistory as $item):?>
    <div class="ClickShowHideWrap">
    <h1><a class="ClickShowHide" href="javascript:void(0)"> Đề xuất lần: <?php echo $count_history--; ?></a></h1>
        <div class="ClickShowHideItem display_none">
        <?php $this->widget('zii.widgets.CDetailView', array(
        'data'=>$item,
        'attributes'=>array(
                    'code_no',
                    array(
                        'name' => 'status',
                        'value' => GasSupportCustomer::GetStatusSupport($item),
                    ),
                    'date_request:date',
                    array(
                        'name' => 'customer_id',
                        'type' => 'NameUser',
                        'value' => $item->rCustomer?$item->rCustomer:"",
                    ),
                    array(
                        'name' => 'type_customer',
                        'value'=>$item->type_customer?CmsFormatter::$CUSTOMER_BO_MOI[$item->type_customer]:"",
                    ),
                    array(
                        'name' => 'note',
                        'type' => 'html',
                        'value' => nl2br($item->note),
                    ),
                    array(
                        'name' => 'item_name',
                        'type' => 'GasSupportCustomerItemName',
                        'value' => $item,
                    ),
                    array(
                        'name' => 'contact_person',
                    ),
                    array(
                        'name' => 'contact_tel',
                    ),
                    array(
                        'name' => 'time_doing',
                        'value' => $model->getTimeDoing(),
                    ),
                    array(
                        'name' => 'price_qty',
                    ),

                    array(
                        'name' => 'approved_uid_level_1',
                        'type' => 'NameAndRole',
                        'value' => $item->rApprovedUidLevel1,
                    ),
                    'approved_date_level_1:datetime',
                    array(
                        'name' => 'approved_note_level_1',
                        'type' => 'html',
                        'value' => nl2br($item->approved_note_level_1),
                    ),
                    array(
                        'name' => 'approved_uid_level_2',
                        'type' => 'NameAndRole',
                        'value' => $item->rApprovedUidLevel2,
                    ),
                    'approved_date_level_2:datetime',

                    array(
                        'name' => 'approved_note_level_2',
                        'type' => 'html',
                        'value' => nl2br($item->approved_note_level_2),
                    ),
                    array(
                        'name' => 'uid_login',
                        'type' => 'NameAndRole',
                        'value' => $item->rUidLogin,
                    ),

                    'created_date:datetime',
        ),
        )); ?>
        </div>
    </div>
    <?php endforeach;?>
<?php endif;?>
