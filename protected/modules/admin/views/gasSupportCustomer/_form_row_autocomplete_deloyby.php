<div class="row">
    <label>&nbsp</label>
    <em class="hight_light item_b">Chú Ý: Có thể chọn nhiều Đơn Vị Thực Hiện trong cùng 1 lần tạo </em>
</div>
<div class="row">
    <?php echo $form->labelEx($model,'deloy_by'); ?>
    <?php echo $form->hiddenField($model,'deloy_by', array('class'=>'')); ?>
    <?php
        // 1. limit search kh của sale
        $url = Yii::app()->createAbsoluteUrl('admin/ajax/searchLimitByFunction', array('type'=>  GasOneManyBig::TYPE_DELOY_BY));
        // widget auto complete search user customer and supplier
        $aData = array(
            'model'=>$model,
            'field_customer_id'=>'deloy_by',
            'url'=> $url,
            'name_relation_user'=>'rDeloyBy',
            'field_autocomplete_name'=>'autocomplete_name_second',
            'ClassAdd' => 'w-400',
            'ShowTableInfo' => 0,
            'fnSelectCustomerV2' => "fnDeloyBySelect",
            'doSomethingOnClose' => "doSomethingOnClose",
        );
        $this->widget('ext.SpaAutocompleteCustomer.SpaAutocompleteCustomer',
            array('data'=>$aData));                                        
    ?>    
    <?php echo $form->error($model,'deloy_by'); ?>
</div>

<?php include '_form_create_update_deloy_by.php';?>

<script>
    /**
    * @Author: ANH DUNG Dec 28, 2016
    * @Todo: function này dc gọi từ ext của autocomplete, action close auto
    */
    function doSomethingOnClose(ui, idField, idFieldCustomer){
        var row = $(idField).closest('.row');
        row.find('.remove_row_item').trigger('click');
    }

</script>
