<div class="row tb_file" >
    <?php echo $form->labelEx($model,'file_complete', array('class'=>'')); ?>
    <div class="float_l">
        <div>
            <a href="javascript:void(0);" class="text_under_none item_b" style="line-height:25px" onclick="fnBuildRowFile(this);">
                <img style="float: left;margin-right:8px;" src="<?php echo Yii::app()->theme->baseUrl;?>/images/add.png"> 
                Thêm Dòng
            </a>
        </div>
        <?php echo $form->error($model,'file_complete'); ?>
        <table class="materials_table hm_table">
            <thead>
                <tr>
                    <th class="item_c">#</th>
                    <th class=" item_c f_size_11">Cho phép <?php echo GasFile::$AllowFile;?>. Tên file không quá 100 ký tự</th>
                    <th>Xóa</th>
                </tr>
            </thead>
            <tbody>
                <?php include "_update_status_file_update.php";?>
                
                <?php for($i=1; $i<=( GasSupportCustomer::IMAGE_MAX_UPLOAD); $i++): ?>
                <?php $display = "";
                    if($i > GasSupportCustomer::IMAGE_MAX_UPLOAD_SHOW)
                        $display = "display_none";
                ?>
                <tr class="materials_row <?php echo $display;?>">
                    <td class="item_c order_no"></td>
                    <td class="item_l w-400">
                        <?php // echo $form->fileField($model,'file_name[]',array('class'=>'input_file',)); ?>
                        <?php echo $form->fileField($model,'file_name[]',array('class'=>'input_file','accept'=>'image/*')); ?>
                    </td>
                    <td class="item_c last"><span class="remove_icon_only"></span></td>
                </tr>
                <?php endfor;?>
            </tbody>
        </table>
        
    </div>
        
</div>
<div class="clr"></div>

<style>
</style>
<script>
    function fnBuildRowFile(this_){
        $(this_).closest('.tb_file').find('.materials_table').find('tr:visible:last').next('tr').show();
    }
    
    fnRefreshOrderNumber();
    fnBindRemoveIcon();
</script>
    