<?php 
$aUidApproved = $model->getListUidApproved();
$model->HideSomeRoleWhenUpdate($aUidApproved);
?>
<?php include "_form_row_action_invest.php"; ?>
<div class="row">
    <?php echo $form->labelEx($model,'to_uid_approved'); ?>
    <?php echo $form->dropDownList($model,'to_uid_approved', $aUidApproved,array('class'=>'','empty'=>'Select')); ?>
    <?php echo $form->error($model,'to_uid_approved'); ?>
</div>

<div class="row display_none">
    <?php echo $form->labelEx($model,'date_request'); ?>
    <?php
    Yii::import('application.extensions.CJuiDateTimePicker.CJuiDateTimePicker');
//        $this->widget('zii.widgets.jui.CJuiDatePicker', array(
        $this->widget('CJuiDateTimePicker',array(
            'model'=>$model,        
            'attribute'=>'date_request',
            'mode'=>'date', //use "time","date" or "datetime" (default)
            'language'=>'en-GB',
            'options'=>array(
                'showAnim'=>'fold',
                'dateFormat'=> ActiveRecord::getDateFormatJquery(),
//                            'minDate'=> '0',
                'maxDate'=> '0',
                'changeMonth' => true,
                'changeYear' => true,
                'showOn' => 'button',
                'buttonImage'=> Yii::app()->theme->baseUrl.'/admin/images/icon_calendar_r.gif',
                'buttonImageOnly'=> true,
            ),        
            'htmlOptions'=>array(
                'class'=>'w-16',
                'style'=>'height:20px;',
                    'readonly'=>'readonly',
            ),
        ));
    ?>
    <?php echo $form->error($model,'date_request'); ?>
</div>

<div class="row">
    <?php echo $form->labelEx($model,'customer_id'); ?>
    <?php echo $form->hiddenField($model,'customer_id', array('class'=>'')); ?>
    <?php
        // 1. limit search kh của sale
        $url = Yii::app()->createAbsoluteUrl('admin/ajax/search_user_by_code');
        // widget auto complete search user customer and supplier
        $aData = array(
            'model'=>$model,
            'field_customer_id'=>'customer_id',
            'url'=> $url,
            'name_relation_user'=>'rCustomer',
            'ClassAdd' => 'display_none RemoveDeleteIcon',
        );
        $this->widget('ext.SpaAutocompleteCustomer.SpaAutocompleteCustomer',
            array('data'=>$aData));                                        
        ?>
    <?php echo $form->error($model,'customer_id'); ?>
</div>        

<div class="row text_area_16">
    <?php echo $form->labelEx($model,'note'); ?>
    <?php echo $form->textArea($model,'note',array("placeholder"=>"Nhập ghi chú")); ?>
    <?php echo $form->error($model,'note'); ?>
</div>

<?php include '_form_create_update_item.php';?>
<?php include '_form_create_update_file.php';?> 
<div class="row">
    <?php echo $form->labelEx($model,'note_technical'); ?>
    <div class="float_l"><?php echo $model->getNoteTechnical(); ?></div>
    <?php echo $form->error($model,'note_technical'); ?>
</div>
<div class="clr"></div>
<div class="row">
    <?php echo $form->labelEx($model,'sale_approved'); ?>
    <?php echo $form->dropDownList($model,'sale_approved', $model->ARR_SALE_APPROVED,array('class'=>'','empty'=>'Select')); ?>
    <?php echo $form->error($model,'sale_approved'); ?>
</div>
  
<?php include '_form_create_update_more.php';?>   