<?php
$this->breadcrumbs=array(
	$this->adminPageTitle=>array($model->getActionIndex()),
	'Tạo Mới',
);

$menus = array(		
        array('label' => $this->adminPageTitle, 'url'=>array($model->getActionIndex())),
);
$this->menu= ControllerActionsName::createMenusRoles($menus, $actions);
?>

<h1>Tạo Mới <?php echo $this->adminPageTitle;?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>