<?php
//Sta2::GetOutputOneCustomer($model->customer_id);// Dec 28, 2015 không rõ chỗ này để làm gì?, văng exception
$this->breadcrumbs=array(
	$this->adminPageTitle=>array('index'),
	$model->getCustomer(),
);

$aHistory = GasSupportCustomer::GetHistorySupport($model->customer_id, array( $model->id ));
$count_history = count($aHistory);
?>
<?php echo MyFormat::BindNotifyMsg(); ?>
<h1>Đề Xuất Lần <?php echo $count_history+1;?></h1>
<h1>Xem <?php echo $this->adminPageTitle;?>: <?php echo $model->getCustomer(); ?></h1>

<?php include "view_xdetail.php";?>

<?php 
if(GasCheck::isAllowAccess("gasSupportCustomer", "update_status") && GasSupportCustomer::CanUpdateStatus($model)):
//if(1):
    include '_update_status.php';
    include '_statistic.php';
    include '_history.php';
endif;
?>

<script>
    $(window).load(function(){ 
        $('.form').find('button:submit').click(function(){
            $.blockUI({ overlayCSS: { backgroundColor: '#fff' } }); 
        });
        
        fnResizeColorbox();
        parent.$.fn.yiiGridView.update("gas-support-customer-grid"); 
    });
    
    function fnResizeColorbox(){
//        var y = $('body').height()+100;
        var y = $('#main_box').height()+100;
        parent.$.colorbox.resize({innerHeight:y});
    }
    $(document).ready(function(){
        $('.form').find('button:submit').click(function(){
            $.blockUI({ overlayCSS: { backgroundColor: '#fff' } }); 
        });        
    });
</script>