<?php $this->widget('application.components.widgets.XDetailView', array(
    'data' => $model,
    'attributes' => array(
        'group11'=>array(
            'ItemColumns' => 3,
            'attributes' => array(
                    array(
                        'label' => 'NV Kinh Doanh',
                        'type' => 'html',
                        'value' => $model->getSale(),
                    ),
                    array(
                        'name' => 'status',
                        'value' => GasSupportCustomer::GetStatusSupport($model),
                    ),
                    array(
                        'name' => 'action_invest',
                        'value' => $model->getActionInvest(),
                    ),
//                    'date_request:date',
//                    'code_no',
                    array(
                        'name' => 'customer_id',
                        'type' => 'html',
                        'value' => Users::GetInfoField($model->rCustomer, "first_name")."<br>". Users::GetInfoField($model->rCustomer, "address"),
                    ),
                    array(
                        'name' => 'type_customer',
                        'value'=>$model->type_customer?CmsFormatter::$CUSTOMER_BO_MOI[$model->type_customer]:"",
                    ),
                    array(
                        'name' => 'note',
                        'type' => 'html',
                        'value' => nl2br($model->note),
                    ),
                    array(
                        'name' => 'note_technical',
                        'type' => 'html',
                        'value' => nl2br($model->note_technical),
                    ),
                    array(
                        'name' => 'sale_approved',
                        'type' => 'html',
                        'value' => $model->getSaleApproved(),
                    ),
                    array(
                        'name' => 'note_update_real_time',
                        'type' => 'html',
                        'value' => $model->getNotUpdateRealTime(),
                    ),
            ),
        ),
        
        
        'group12'=>array(
            'ItemColumns' => 1,
            'attributes' => array(
                    array(
                        'name' => 'item_name',
                        'type' => 'html',
                        'value' => $model->formatGasSupportCustomerItemName(),
                    ),
            ),
        ),
        
        'group13'=>array(
            'ItemColumns' => 3,
            'attributes' => array(
                    array(
                        'name' => 'deloy_by',
                        'type' => 'html',
                        'value' => $model->getDeloyHtml(),
                    ),
                    array(
                        'name' => 'contact_person',
                    ),
                    array(
                        'name' => 'contact_tel',
                    ),
                    array(
                        'name' => 'time_doing',
                        'value' => $model->getTimeDoing(),
                    ),
                    array(
                        'name' => 'price_qty',
                        'type' => 'raw',
                        'value' => $model->getGPrice(),
                    ),
                    array(
                        'name' => 'file_design',
                        'type' => 'html',
                        'value' => $model->renderListFile(),
                    ),
                    array(
                        'name' => 'time_doing_real',
                        'value' => $model->getTimeDoingReal(),
                    ),
                    array(
                        'name' => 'uid_login',
                        'type' => 'NameAndRole',
                        'value' => $model->rUidLogin,
                    ),
                    'created_date:datetime',
            ),
        ),
        
        'group2'=>array(
            'ItemColumns' => 3,
            'itemCssClass' => array('even xdetail_c2'),
            'attributes' => array(
                    array(
                        'name' => 'approved_uid_level_1',
                        'type' => 'NameAndRole',
                        'value' => $model->rApprovedUidLevel1,
                        
                    ),
                    array(
                        'name' => 'approved_date_level_1',
                        'type' => 'datetime',
                        'htmlOptions' => array('class' => 'w-100')
                    ),
                    array(
                        'name' => 'approved_note_level_1',
                        'type' => 'html',
                        'value' => nl2br($model->approved_note_level_1),
                    ),
            ),
        ),
        
        'group3'=>array(
            'ItemColumns' => 3,
            'itemCssClass' => array('odd xdetail_c2'),
            'attributes' => array(
                    array(
                        'name' => 'approved_uid_level_2',
                        'type' => 'NameAndRole',
                        'value' => $model->rApprovedUidLevel2,
                        'htmlOptions' => array('class' => 'w-200')
                    ),
                    array(
                        'name' => 'approved_date_level_2',
                        'type' => 'datetime',
                        'htmlOptions' => array('class' => 'w-100')
                    ),
                    array(
                        'name' => 'approved_note_level_2',
                        'type' => 'html',
                        'value' => nl2br($model->approved_note_level_2),
                    ),
            ),
        ),
        
        'group4'=>array(
            'ItemColumns' => 3,
            'itemCssClass' => array('odd xdetail_c2'),
            'attributes' => array(
                    array(
                        'name' => 'approved_uid_level_3',
                        'type' => 'NameAndRole',
                        'value' => $model->rApprovedUidLevel3,
                    ),
                    'approved_date_level_3:datetime',
                    array(
                        'name' => 'approved_note_level_3',
                        'type' => 'html',
                        'value' => nl2br($model->approved_note_level_3),
                    ),
            ),
        ),
        
        'group5'=>array(
            'ItemColumns' => 3,
            'itemCssClass' => array('odd xdetail_c2'),
            'attributes' => array(
                    array(
                        'name' => 'approved_uid_level_4',
                        'type' => 'NameAndRole',
                        'value' => $model->rApprovedUidLevel4,
                    ),
                    'approved_date_level_4:datetime',
                    array(
                        'name' => 'approved_note_level_4',
                        'type' => 'html',
                        'value' => $model->getNoteLevel4(),
                    ),
            ),
        ),
        
        'group6'=>array(
            'ItemColumns' => 1,
            'itemCssClass' => array('odd xdetail_c2'),
            'attributes' => array(
                    array(
                        'name' => 'file_complete',
                        'type' => 'html',
                        'value' => $model->formatFileComplete(),
                    ),
                    
            ),
        ),
        
        
        
    ),
)); ?>
    <script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/jquery.colorbox-min.js"></script>    
    <link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/admin/css/colorbox.css" />
<script>
    $(document).ready(function(){
        $('.xdetail_c2').each(function(){
            $(this).find('td:first').addClass("w-300");
            $(this).find('td').eq(1).addClass("w-100");
            $(this).find('th').addClass("w-120");
        });
        $(".gallery").colorbox({iframe:true,innerHeight:'1100', innerWidth: '1000',close: "<span title='close'>close</span>"});
    });
</script>