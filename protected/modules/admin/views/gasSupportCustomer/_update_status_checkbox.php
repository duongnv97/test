<div class="row more_col ">
    <label class="">&nbsp;</label>
    <div class="col1">
        <?php echo $form->checkBox($model,'chk_design_wrong', array('class'=>'float_l')); ?>
        <?php echo $form->labelEx($model,'chk_design_wrong', array('class'=>'w-180')); ?>
        <?php echo $form->error($model,'chk_design_wrong'); ?>
    </div>
    <div class="col2">
        <?php echo $form->checkBox($model,'chk_material_not_enough',array('class'=>'float_l')); ?>
        <?php echo $form->labelEx($model,'chk_material_not_enough', array('class'=>'w-180')); ?>
        <?php echo $form->error($model,'chk_material_not_enough'); ?>
    </div>
    <div class="col3">
        <?php echo $form->checkBox($model,'chk_deploy_wrong',array('class'=>'float_l')); ?>
        <?php echo $form->labelEx($model,'chk_deploy_wrong', array('class'=>'w-180')); ?>
        <?php echo $form->error($model,'chk_deploy_wrong'); ?>
    </div>
</div>
<div class="clr"></div>