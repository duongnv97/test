<?php
// 1. limit search kh của sale
$url = Yii::app()->createAbsoluteUrl('admin/ajax/search_user_by_code');
//if(in_array($cRole, GasSupportCustomer::$aRoleCreateExt1)){// Jul 23, 2016
//    $url = Yii::app()->createAbsoluteUrl('admin/ajax/searchHgd');
//}
?>

<div class="row">
    <?php echo $form->labelEx($model,'customer_id'); ?>
    <?php echo $form->hiddenField($model,'customer_id', array('class'=>'')); ?>
    <?php
        
        // widget auto complete search user customer and supplier
        $aData = array(
            'model'=>$model,
            'field_customer_id'=>'customer_id',
            'url'=> $url,
            'name_relation_user'=>'rCustomer',                
        );
        $this->widget('ext.SpaAutocompleteCustomer.SpaAutocompleteCustomer',
            array('data'=>$aData));                                        
        ?>
    <?php echo $form->error($model,'customer_id'); ?>
</div>

<div class="row text_area_16">
    <?php echo $form->labelEx($model,'note_technical'); ?>
    <?php echo $form->textArea($model,'note_technical',array("placeholder"=>"Nhập ghi chú")); ?>
    <?php echo $form->error($model,'note_technical'); ?>
</div>

<?php include '_form_item.php';?>
<?php include '_form_file.php'; ?>

