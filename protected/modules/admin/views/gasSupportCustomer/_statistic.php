<div class="form">
    <?php
    $aData = array(
        'customer_id'=> $model->customer_id,
        'month_statistic'=> Yii::app()->params['month_statistic_output_customer'],
    );
    ?>    
    <div class="row">
        <label>&nbsp;</label>
        <span class="item_b">Sản lượng khách hàng trong <?php echo Yii::app()->params['month_statistic_output_customer']; ?> tháng gần đây</span>
    </div>
    <div class="row">
        <label>&nbsp;</label>
        <?php
        $this->widget('ext.GasStatistic.GasStatistic',
            array('data'=>$aData)); 
        ?>
    </div>
</div>