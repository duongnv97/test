<?php include '_form_row_autocomplete_deloyby.php';?>

<div class="row">
    <?php echo $form->labelEx($model,'contact_person'); ?>
    <?php echo $form->textField($model,'contact_person',array('class'=>'w-400')); ?>
    <?php echo $form->error($model,'contact_person'); ?>
</div>
<div class="row">
    <?php echo $form->labelEx($model,'contact_tel'); ?>
    <?php echo $form->textField($model,'contact_tel',array('class'=>'w-400')); ?>
    <?php echo $form->error($model,'contact_tel'); ?>
</div>
<div class="row">
    <?php echo $form->labelEx($model,'time_doing'); ?>
    <?php Yii::import('application.extensions.CJuiDateTimePicker.CJuiDateTimePicker');
    $this->widget('CJuiDateTimePicker',array(
        'model'=>$model, //Model object
        'attribute'=>'time_doing', //attribute name
        'mode'=>'datetime', //use "time","date" or "datetime" (default)
        'language'=>'en-GB',
        'options'=>array(
            'showAnim'=>'fold',
            'showButtonPanel'=>true,
            'autoSize'=>true,
            'dateFormat'=>'dd/mm/yy',
            'timeFormat'=>'hh:mm:ss',
            'width'=>'120',
            'separator'=>' ',
            'showOn' => 'button',
            'buttonImage'=> Yii::app()->theme->baseUrl.'/admin/images/icon_calendar_r.gif',
            'buttonImageOnly'=> true,
            'changeMonth' => true,
            'changeYear' => true,
            //                'regional' => 'en-GB'
        ),
        'htmlOptions' => array(
            'style' => 'width:180px;',
            'readonly'=>'readonly',
        ),
    ));
    ?>
    <?php echo $form->error($model,'time_doing'); ?>
</div>
<div class="row">
    <?php echo $form->labelEx($model,'price_qty'); ?>
    <?php echo $form->textField($model,'price_qty',array('class'=>'w-400')); ?>
    <?php echo $form->error($model,'price_qty'); ?>
</div>
<?php include '_form_file.php'; ?>

<?php
    $cmsFormater = new CmsFormatter();
?>
<div class="row">
    <?php // echo $form->labelEx($model,'item_name'); ?>
    <?php // echo $cmsFormater->formatGasSupportCustomerItemName($model); ?>
</div>