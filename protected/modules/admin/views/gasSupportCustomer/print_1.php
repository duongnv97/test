<div class="container BoxPrint1" id="printElement">
    <table cellpadding="0" cellspacing="0">
        <tr>
            <td style="width: 60%; border-bottom: 2px solid #000; height: 157px;" class="">
                <div class="logo">
                    <img src="<?php echo Yii::app()->theme->baseUrl; ?>/images/gas_logo80.jpg">
                </div>            
            </td>
            <td class="item_r" style="border-bottom: 2px solid #000; vertical-align: bottom">
                <p>
                    <span class="item_b f_size_14">CÔNG TY CỔ PHẦN DẦU KHÍ MIỀN NAM </span>
                </p>
                <p>
                    Đ.C: 86 Nguyễn Cửu Vân, F17, Bình Thạnh, HCM 
                </p>
                <p>
                    Tel: 08.38 409 409 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Fax: 08.6294 7078
                </p>
<!--                <p>
                    <span class="item_b">CÔNG TY CỔ PHẦN DẦU KHÍ MIỀN NAM </span><br>
                    Đ.C: 86 Nguyễn Cửu Vân, F17, Bình Thạnh, HCM <br>
                    Tel: 08.38 409 409 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Fax: 08.6294 7078
                </p>-->
            </td>
        </tr>
        <tr><td class="item_r f_size_14 r_padding_20 t_padding_5" colspan="2">Mẫu số: 02/PKD-DKMN</td></tr>
    </table>
    
    <table cellpadding="0" cellspacing="0">
        <tr>
            <td class="item_c">
                <h2 style="margin: 35px 0 0">GIAO NHIỆM VỤ ĐI LẮP ĐẶT HỆ THỐNG GAS VÀ BẾP</h2>
            </td>
        </tr>
        <tr>
            <td class="agent_info"><br><span class="item_b">Đơn vị thực hiện: </span><?php echo $model->getDeloyHtml();?></td>
        </tr>
    </table>
    
    <table cellpadding="0" cellspacing="0" class="tb hm_table border_black">
        <thead>
            <tr>
                <th class="item_normal">STT</th>
                <th class="item_normal ">NỘI DUNG</th>
                <th class="item_normal">CHI TIẾT</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td class="item_c">1</td>
                <td class="">Tên khách hàng</td>
                <td class="item_b item_c"><?php echo Users::GetInfoField($model->rCustomer, "first_name");?></td>
            </tr>
            <tr>
                <td class="item_c">2</td>
                <td class="">Địa điểm lắp đặt</td>
                <td class="item_b item_c"><?php echo Users::GetInfoField($model->rCustomer, "address");?></td>
            </tr>
            <tr>
                <td class="item_c">3</td>
                <td class="">Người liên hệ  -  Tel</td>
                <td class="item_b item_c"><?php echo $model->getContactPerson();?> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;   <?php echo $model->getContactTel();?></td>
            </tr>
            <tr>
                <td class="item_c">4</td>
                <td class="">Thời gian thi công</td>
                <td class="item_b item_c"><?php echo $model->getTimeDoingReal();?></td>
            </tr>
            <tr>
                <td class="item_c">5</td>
                <td class="">Nhóm giá & sản lượng</td>
                <td class="item_b item_c"><?php echo $model->getPriceQty();?></td>
            </tr>
            <?php $index = 6; ?>
            <tr>
                <td class="item_c"><?php echo $index;?></td>
                <td class="">Tên thiết bị và quy cách</td>
                <td class="">&nbsp;</td>
            </tr>
            <?php foreach($model->rDetail as $key=>$item):?>
                <?php 
                    $percent = $item->getPercent(4);
                    $url_cho_muon = Yii::app()->theme->baseUrl."/images/checkbox-unchecked-16.png";
                    $url_ban = Yii::app()->theme->baseUrl."/images/checkbox-unchecked-16.png";
                    if($percent == 100){
                        $url_cho_muon = Yii::app()->theme->baseUrl."/images/checkbox-checked-16.png";
                    }elseif( $percent < 100 ){
                        $url_ban = Yii::app()->theme->baseUrl."/images/checkbox-checked-16.png";
                    }
                ?>
                <tr>
                    <td class="item_c"><?php echo $index.".".($key+1);?></td>
                    <td class=""><?php echo $item->getMaterialName();?></td>
                    <td class="item_c">
                        <span class="l_padding_100">
                        Cho Mượn &nbsp;&nbsp;&nbsp;&nbsp;<img src="<?php echo $url_cho_muon; ?>">
                        </span>
                        <span class="l_padding_100">
                        Bán &nbsp;&nbsp;&nbsp;&nbsp;<img src="<?php echo $url_ban; ?>">
                        </span>
                    </td>
                </tr>
            <?php endforeach;?>
            
        </tbody>
    </table>
    
    <br>
    <table cellpadding="0" cellspacing="0">
        <tr>
            <td class="" style="width: 70%;">
                <span class="item_b">Ghi chú:</span> <?php echo $model->getNote();?>
            </td>
            <td class="">HCM, ngày <?php echo date('d');?> tháng <?php echo date('m');?> năm <?php echo date('Y');?></td>
        </tr>
    </table>
    
    <br>
    <table cellpadding="0" cellspacing="0">
        <tr>
            <td class="item_c" style="vertical-align: top;">
                <span class="item_b">Nhân Viên Kinh Doanh</span>
                <p style="min-height: 50px;"></p>
<!--            <span class="item_b"><?php echo $sale_name;?></span>-->
            </td>
            
            <td class="item_c" style="vertical-align: top;">
                <span class="item_b">Tp.Kinh Doanh</span>
                <br>
                <?php 
                    if( !empty($model->approved_uid_level_1)){
                        echo "Đã duyệt"; 
                    }
                ?>
                
                <p style="min-height: 50px;">
                <!--<img src="<?php echo ImageProcessing::bindImageByModel($AdminUser->mUsersRef,'','',array('size'=>'size1'));?>">-->
<!--                <br>
                <span class="item_b"><?php echo $mHeadKinhDoanh->first_name;?></span>-->
                </p>
            </td>
            
            <td class="item_c" style="vertical-align: top;">
                <span class="item_b">Giám Đốc Kinh Doanh</span>
                <br>
                <?php 
                    if( !empty($model->approved_uid_level_2)){
                        echo "Đã duyệt"; 
                    }
                ?>
                
                <p style="min-height: 50px;">
                <!--<img src="<?php echo ImageProcessing::bindImageByModel($AdminUser->mUsersRef,'','',array('size'=>'size1'));?>">-->
<!--                <br>
                <span class="item_b"><?php echo $mGDKinhDoanh->first_name;?></span>-->
                </p>
            </td>
            
            <td class="item_c" style="vertical-align: top;">
                <span class="item_b">Giám Đốc</span>
                <br>
                <?php 
                    if( !empty($model->approved_uid_level_3)){
                        echo "Đã duyệt"; 
                    }
                ?>
                
            </td>
            
        </tr>
        <tr>
            <td class="item_c" style=""><span class="item_b"><?php echo $sale_name;?></span></td>
            <td class="item_c" style=""><span class="item_b"><?php echo $mHeadKinhDoanh->first_name;?></span></td>
            <td class="item_c" style=""><span class="item_b"><?php echo $mGDKinhDoanh->first_name;?></span></td>
            <td class="item_c" style=""><span class="item_b"><?php echo $mRootDirector->first_name;?></span></td>            
        </tr>
    </table>
</div>