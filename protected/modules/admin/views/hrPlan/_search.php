<div class="wide form">

    <?php $form = $this->beginWidget('CActiveForm', array(
        'action' => GasCheck::getCurl(),
        'method' => 'get',
    )); ?>

    <div class="row">
        <?php echo $form->label($model, 'code_no', array()); ?>
        <?php echo $form->textField($model, 'code_no', array('size' => 20, 'maxlength' => 20)); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model, 'name', array()); ?>
        <?php echo $form->textField($model, 'name'); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model, 'status', array()); ?>
        <?php echo $form->dropDownList($model, 'status', HrPlan::$LIST_STATUS_TEXT, array('empty' => 'Select', 'class' => 'w-400 status', "class_update_val" => 'status')); ?>
        <?php echo $form->error($model, 'status'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model, 'company_id'); ?>
        <?php echo $form->dropDownList($model, 'company_id', HrPlan::getCompanies(), array('empty' => 'Select', 'class' => 'w-400 company_id', "class_update_val" => 'company_id')); ?>
        <?php echo $form->error($model, 'company_id'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model, 'department_id'); ?>
        <?php echo $form->dropDownList($model, 'department_id', HrPlan::getDepartments(), array('empty' => 'Select', 'class' => 'w-400 department_id', "class_update_val" => 'department_id')); ?>
        <?php echo $form->error($model, 'department_id'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model, 'approved_1'); ?>
        <?php echo $form->dropDownList($model, 'approved_1', GasLeave::ListoptionApprove(), array('empty' => 'Select', 'class' => 'w-400 approved_1', "class_update_val" => 'approved_1')); ?>
        <?php echo $form->error($model, 'approved_1'); ?>
    </div>

    <div class="row buttons" style="padding-left: 159px;">
        <?php $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType' => 'submit',
            'label' => 'Search',
            'type' => 'null', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
            'size' => 'small', // null, 'large', 'small' or 'mini'
        )); ?>    </div>

    <?php $this->endWidget(); ?>

</div><!-- search-form -->