<?php /** @var HrPlan $model */
$model->scenario = 'updateStatus';
$model->handleViewStatus();
$aStatus = HrPlan::$LIST_STATUS_APPROVE_TEXT;
$displayNote = $model->status == HrPlan::STA_NEED_UPDATE ? '' : 'display_none';
?>

<?php if (GasCheck::isAllowAccess("hrPlan", "updateStatus") && $model->canUpdateStatus()): ?>
    <div class="form container">
        <?php $form = $this->beginWidget('CActiveForm', array(
            'id' => 'hr-plan-form',
            'action' => Yii::app()->createAbsoluteUrl('admin/hrPlan/updateStatus', array('id' => $model->id)),
            'enableClientValidation' => true,
            'clientOptions' => array(
                'validateOnSubmit' => true,
            ),
        ));
        ?>
        <?php if (Yii::app()->user->hasFlash('successUpdate')): ?>
            <div class='success_div'><?php echo Yii::app()->user->getFlash('successUpdate'); ?></div>
        <?php endif; ?>
        <div class="row">
            <?php echo $form->labelEx($model, 'status'); ?>
            <?php echo $form->dropDownList($model, 'status', $aStatus, array('style' => 'width:376px;', 'empty' => 'Select','class'=>'DropdownStatus' )); ?>
            <?php echo $form->error($model, 'status'); ?>
        </div>
        <div class="row rowDetailStatus <?php echo $displayNote; ?>">
        <?php echo $form->textArea($model, 'history_note', array("class" => 'display_none')); ?>
        <?php echo $form->labelEx($model, 'note'); ?>
        <?php echo $form->textArea($model, 'note', array('rows' => 5, 'cols' => 45,"placeholder" => $model->getHistoryNote())); ?>
        <?php echo $form->error($model, 'note'); ?>
        </div>
        <div class="row buttons" style="padding-left: 141px;">
            <?php $this->widget('bootstrap.widgets.TbButton', array(
                'buttonType' => 'submit',
                'label' => $model->isNewRecord ? 'Create' : 'Save',
                'type' => 'null', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
                'size' => 'small', // null, 'large', 'small' or 'mini'
                //'htmlOptions' => array('style' => 'margin-bottom: 10px; float: right;'),
            )); ?>
            <input class='cancel_iframe' type='button' value='Cancel'>
        </div>

        <?php $this->endWidget(); ?>
    </div>
<?php endif; ?>

<script>
    $(function(){
        bindStatus();
        var textAreas = document.getElementsByTagName('textarea');
        Array.prototype.forEach.call(textAreas, function(elem) {
            elem.placeholder = elem.placeholder.replace(/\\n/g, '\n');
        });
    });
    
    function bindStatus(){
        console.log($('.DropdownStatus'));
        $('.DropdownStatus').on('change',function(){
            $valueSelect = $(this).val();
            switch($valueSelect){
                case '<?php echo HrPlan::STA_NEED_UPDATE; ?>':
                    $('.rowDetailStatus').removeClass('display_none');        
                    break;
                default:
                    $('.rowDetailStatus').addClass('display_none');
                    $('#HomeContract_date_expired').val('');
                    break;
            }
        })
    }
</script>