<?php
/** @var HrPlan $model */
/** @var Users $mUser */
$mUser = $model->rCreatedBy;
$session = Yii::app()->session;
$cmsFormater = new CmsFormatter();
$ObjCreatedDate = new DateTime($model->created_date);
?>

<div class="container" id="printElement">
    <div style="width: 80%; margin: 0 auto;" class="f_size_14 fix_noborder item_c">
        <div class="item_c">
            <?php include 'view_update_status.php'; ?>
        </div>
        <div>
            <h2 class="item_c">KẾ HOẠCH TUYỂN DỤNG NHÂN SỰ CHO PHÒNG <?php echo $model->getDepartment(); ?></h2>
        </div>
        <p class="item_b"><span class="item_u item_i r_padding_100" style="text-transform: uppercase;">Kính gởi:</span>Ban
            giám đốc <?php echo $model->getCompany(); ?></p>
        <table cellpadding="0" cellspacing="0" class="tb hm_table" style="display: inline">
            <tbody>
            <tr>
                <td><p>Tôi tên: <?php echo $model->getOwnerName(); ?></p></td>
                <td>Bộ phận công tác: <?php echo $session['ROLE_NAME_USER'][$mUser->role_id]; ?></td>
            </tr>
            <tr>
                <td colspan="2">
                    <p>Nay tôi làm đơn này gởi đến Ban Giám đốc cân nhắc kế hoạch tuyển dụng nhân sự như sau:</br>
                        Công ty cần tuyển dụng: <?php echo $model->getCompany(); ?> </br>
                        Phòng ban cần tuyển dụng: <?php echo $model->getDepartment(); ?> </br>
                        Số lượng cần tuyển dụng: <?php echo $model->qty ?> </br>
                        Từ ngày <?php echo $cmsFormater->formatDate($model->plan_from); ?> đến hết
                        ngày <?php echo $cmsFormater->formatDate($model->plan_to); ?>.</br>
                        Mã kế hoạch: <?php echo $model->code_no; ?><br/>
                    </p>
                    <p>Mô tả công việc: <br/>
                        <?php echo nl2br($model->description); ?>.</br>
                        Rất mong sự chấp thuận của Ban Giám đốc.
                    </p>
                </td>
            </tr>
            <tr>
                <td class="item_c" style="width: 50%;">
                    <p class="item_b">*Ý KIẾN CỦA NGƯỜI XÉT DUYỆT</p>
                    <?php if (!empty($model->approved_1)): ?>
                        <p><?php echo isset(HrPlan::$LIST_STATUS_TEXT[$model->status]) ? HrPlan::$LIST_STATUS_TEXT[$model->status] : ''; ?></p>
                    <?php endif; ?>
                    <p><?php echo nl2br($model->description); ?></p>

                    <br>
                    <?php echo $model->getApproveName(); ?>
                </td>
                <td class="item_c">
                    TP.HCM, ngày <?php echo $ObjCreatedDate->format('d'); ?>
                    tháng <?php echo $ObjCreatedDate->format('m'); ?> năm <?php echo $ObjCreatedDate->format('Y'); ?>
                    <br>
                    Người làm đơn<br>
                    (ký tên, ghi rõ họ tên)
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>
                    <?php echo $mUser->first_name; ?>
                </td>
            </tr>
            </tbody>
        </table>
        <br/>
    </div>
</div>