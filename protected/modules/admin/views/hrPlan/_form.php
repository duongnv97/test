<?php /** @var HrPlan $model */
?>
<div class="form">

    <?php $form = $this->beginWidget('CActiveForm', array(
        'id' => 'hr-plan-form',
        'enableAjaxValidation' => false,
        'htmlOptions' => array('enctype' => 'multipart/form-data'),
    )); ?>

    <p class="note">Dòng có dấu <span class="required">*</span> là bắt buộc.</p>
    <?php if (Yii::app()->user->hasFlash('successUpdate')): ?>
        <div class='success_div'><?php echo Yii::app()->user->getFlash('successUpdate'); ?></div>
    <?php endif; ?>

    <?php echo $form->errorSummary($model); ?>

    <div class="row">
        <?php echo $form->labelEx($model, 'name'); ?>
        <?php echo $form->textField($model, 'name'); ?>
        <?php echo $form->error($model, 'name'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model, 'company_id'); ?>
        <?php echo $form->dropDownList($model, 'company_id',
            HrPlan::getCompanies()); ?>
        <?php echo $form->error($model, 'company_id'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model, 'department_id'); ?>
        <?php echo $form->dropDownList($model, 'department_id',
            HrPlan::getDepartments()); ?>
        <?php echo $form->error($model, 'department_id'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model, 'qty'); ?>
        <?php echo $form->textField($model, 'qty', array('class' => 'w-150 number_only', 'maxlength' => 3)); ?>
        <?php echo $form->error($model, 'qty'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model, 'plan_from'); ?>
        <?php
        $this->widget('zii.widgets.jui.CJuiDatePicker', array(
            'model' => $model,
            'attribute' => 'plan_from',
            'options' => array(
                'showAnim' => 'fold',
                'minDate' => '0',
                'dateFormat' => ActiveRecord::getDateFormatJquery(),
                'changeMonth' => true,
                'changeYear' => true,
                'showOn' => 'button',
                'buttonImage' => Yii::app()->theme->baseUrl . '/admin/images/icon_calendar_r.gif',
                'buttonImageOnly' => true,
            ),
            'htmlOptions' => array(
                'class' => 'w-16',
                'style' => 'height:20px;',
                'readonly' => 'readonly',
            ),
        ));
        ?>
        <?php echo $form->error($model, 'plan_from'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model, 'plan_to'); ?>
        <?php
        $this->widget('zii.widgets.jui.CJuiDatePicker', array(
            'model' => $model,
            'attribute' => 'plan_to',
            'options' => array(
                'showAnim' => 'fold',
                'minDate' => '0',
                'dateFormat' => ActiveRecord::getDateFormatJquery(),
                'changeMonth' => true,
                'changeYear' => true,
                'showOn' => 'button',
                'buttonImage' => Yii::app()->theme->baseUrl . '/admin/images/icon_calendar_r.gif',
                'buttonImageOnly' => true,
            ),
            'htmlOptions' => array(
                'class' => 'w-16',
                'style' => 'height:20px;',
                'readonly' => 'readonly',
            ),
        ));
        ?>
        <?php echo $form->error($model, 'plan_to'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model, 'description'); ?>
        <?php echo $form->textArea($model, 'description', array('rows' => 5, 'cols' => 80, "placeholder" => "")); ?>
        <?php echo $form->error($model, 'description'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model, 'approved_1'); ?>
        <?php echo $form->dropDownList($model, 'approved_1',
            GasLeave::ListoptionApprove()); ?>
        <?php echo $form->error($model, 'approved_1'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model, 'status'); ?>
        <?php echo $model->getStatusName(); ?>
        <?php echo $form->error($model, 'status'); ?>
    </div>

    <div class="row buttons" style="padding-left: 141px;">
        <?php $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType' => 'submit',
            'label' => $model->isNewRecord ? 'Create' : 'Save',
            'type' => 'null', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
            'size' => 'small', // null, 'large', 'small' or 'mini'
        )); ?>
        <input class='cancel_iframe' type='button' value='Cancel'>
    </div>

    <?php $this->endWidget(); ?>

</div><!-- form -->
<script>
    $(document).ready(function () {
        $('.form').find('button:submit').click(function () {
            $.blockUI({overlayCSS: {backgroundColor: '#fff'}});
        });

    });

    $(window).load(function () { // không dùng dc cho popup
        fnResizeColorbox();
        parent.$('.SubmitButton').trigger('click');
    });

    function fnResizeColorbox() {
//        var y = $('body').height()+100;
        var y = $('#main_box').height() + 100;
        parent.$.colorbox.resize({innerHeight: y});
    }
</script>