<?php
/** @var HrPlan $model */
$this->breadcrumbs = array(
    $this->singleTitle,
);

$menus = array(
    array('label' => "Create $this->singleTitle", 'url' => array('create')),
);
$this->menu = ControllerActionsName::createMenusRoles($menus, $actions);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('hr-plan-grid', {
                url : $(this).attr('action'),
		data: $(this).serialize()
	});
	return false;
});
");

Yii::app()->clientScript->registerScript('ajaxupdate', "
$('#hr-plan-grid a.ajaxupdate').live('click', function() {
    $.fn.yiiGridView.update('hr-plan-grid', {
        type: 'POST',
        url: $(this).attr('href'),
        success: function() {
            $.fn.yiiGridView.update('hr-plan-grid');
        }
    });
    return false;
});
");
?>

<h1>Danh Sách <?php echo $this->pluralTitle; ?></h1>


<?php echo CHtml::link('Tìm Kiếm Nâng Cao', '#', array('class' => 'search-button')); ?>
<div class="search-form" style="display:none">
    <?php $this->renderPartial('_search', array(
        'model' => $model,
    )); ?>
</div><!-- search-form -->

<?php
$this->widget('zii.widgets.grid.CGridView', array(
    'id' => 'hr-plan-grid',
    'dataProvider' => $model->search(),
    'afterAjaxUpdate' => 'function(id, data){ fnUpdateColorbox();}',
    'template' => '{pager}{summary}{items}{pager}{summary}',
    'pager' => array(
        'maxButtonCount' => CmsFormatter::$PAGE_MAX_BUTTON,
    ),
//    'enableSorting' => false,
    'columns' => array(
        array(
            'header' => 'S/N',
            'type' => 'raw',
            'value' => '$this->grid->dataProvider->pagination->currentPage * $this->grid->dataProvider->pagination->pageSize + ($row+1)',
            'headerHtmlOptions' => array('width' => '10px', 'style' => 'text-align:center;'),
            'htmlOptions' => array('style' => 'text-align:center;')
        ),
        'code_no',
        'name',
        array(
            'name' => 'department_id',
            'htmlOptions' => array('style' => 'text-align:center;'),
            'value' => '$data->getDepartment()',
        ),
        array(
            'name' => 'company_id',
            'htmlOptions' => array('style' => 'text-align:center;'),
            'value' => '$data->getCompany()',
        ),
        'plan_from',
        'plan_to',
        array(
            'name' => 'qty',
            'htmlOptions' => array('style' => 'text-align:center;'),
            'value' => '$data->getQuantityStatus()',
        ),
        'description',
        array(
            'name' => 'created_by',
            'htmlOptions' => array('style' => 'text-align:center;'),
            'value' => 'isset($data->rCreatedBy) ? $data->rCreatedBy->first_name : ""',
        ),
        'created_date',
        array(
            'name' => 'approved_1',
            'htmlOptions' => array('style' => 'text-align:center;'),
            'value' => 'isset($data->rApproved) ? $data->rApproved->first_name : ""',
        ),
        'approved_1_date',
        array(
            'name' => 'status',
            'type' => 'raw',
            'value' => '$data->getStatusNameHtml()',
            'htmlOptions' => array('style' => 'text-align:center;')
        ),
        array(
            'name' => 'note',
            'type' => 'raw',
            'value' => '$data->getHistoryNote()',
            'htmlOptions' => array('style' => 'text-align:center;')
        ),
        array(
            'header' => 'Actions',
            'class' => 'CButtonColumn',
            'template' => ControllerActionsName::createIndexButtonRoles($actions),
            'buttons' => array(
                'update' => array(
                    'visible' => '$data->canUpdate()',
                ),
                'delete' => array(
                    'visible' => 'GasCheck::canDeleteData($data)',
                ),
            ),
        ),
    ),
)); ?>

<script>
    $(document).ready(function () {
        fnUpdateColorbox();
    });

    function fnUpdateColorbox() {
        fixTargetBlank();

        $(" .view").colorbox({
            iframe: true,
            overlayClose: false, escKey: false,
            innerHeight: '1200',
            innerWidth: '1100', close: "<span title='close'>close</span>",
            onClosed: function () { // update view when close colorbox
            $.fn.yiiGridView.update("hr-plan-grid");
            }
        });
    }
</script>