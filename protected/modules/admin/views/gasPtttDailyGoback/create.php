<?php
$this->breadcrumbs=array(
	'Bình Quay Về Hàng Ngày'=>array('index'),
	'Tạo Mới',
);

$menus = array(		
        array('label'=>'Bình Quay Về Hàng Ngày', 'url'=>array('index')),
);
$this->menu= ControllerActionsName::createMenusRoles($menus, $actions);
?>

<h1>Tạo Mới Bình Quay Về Hàng Ngày</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>