<?php
$this->breadcrumbs=array(
	'Bình Quay Về Hàng Ngày',
);

$menus=array(
            array('label'=>'Tạo Bình Quay Về Hàng Ngày', 'url'=>array('create')),
);
$this->menu= ControllerActionsName::createMenusRoles($menus, $actions);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('gas-pttt-daily-goback-grid', {
                url : $(this).attr('action'),
		data: $(this).serialize()
	});
	return false;
});
");

Yii::app()->clientScript->registerScript('ajaxupdate', "
$('#gas-pttt-daily-goback-grid a.ajaxupdate').live('click', function() {
    $.fn.yiiGridView.update('gas-pttt-daily-goback-grid', {
        type: 'POST',
        url: $(this).attr('href'),
        success: function() {
            $.fn.yiiGridView.update('gas-pttt-daily-goback-grid');
        }
    });
    return false;
});
");
?>

<h1>Bình Quay Về Hàng Ngày</h1>

<?php echo CHtml::link('Tìm Kiếm Nâng Cao','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'gas-pttt-daily-goback-grid',
	'dataProvider'=>$model->search(),
        'afterAjaxUpdate'=>'function(id, data){ fnUpdateColorbox();}',
        'template'=>'{pager}{summary}{items}{pager}{summary}',
//    'itemsCssClass' => 'items custom_here',
//    'htmlOptions'=> array('class'=>'grid-view custom_here'),
        'pager' => array(
            'maxButtonCount'=>  CmsFormatter::$PAGE_MAX_BUTTON,
        ),	
	//'filter'=>$model,
	'columns'=>array(
        array(
            'header' => 'S/N',
            'type' => 'raw',
            'value' => '$this->grid->dataProvider->pagination->currentPage * $this->grid->dataProvider->pagination->pageSize + ($row+1)',
            'headerHtmlOptions' => array('width' => '10px','style' => 'text-align:center;'),
            'htmlOptions' => array('style' => 'text-align:center;')
        ),
		'code_no',
		array(
                    'name'=>'agent_id',
                    'value'=>'$data->rAgent?$data->rAgent->first_name:""',
//                    'htmlOptions' => array('style' => 'width:80px;')
                ),                
                array(
                    'name'=>'maintain_date',
                    'type'=>'date',
                    'htmlOptions' => array('style' => 'text-align:center;width:50px;')
                ),                
                array(
                    'name'=>'monitoring_id',
                    'value'=>'$data->rMonitoring?$data->rMonitoring->first_name:""',
//                    'htmlOptions' => array('style' => 'width:80px;')
                ),
                array(
                    'header' => 'Chi Tiết',
                    'type' => 'DetailDailyGobackInfo',
                    'value' => '$data',
//                    'htmlOptions' => array('style' => 'width:80px;')
                ),
		array(
                    'name' => 'created_date',
                    'type' => 'Datetime',
                    'htmlOptions' => array('style' => 'width:100px;')
                ),            
//                array(
//                    'name' => 'last_update_time',
//                    'type' => 'Datetime',
//                    'htmlOptions' => array('style' => 'width:50px;'),
////                    'visible'=>Yii::app()->user->role_id==ROLE_ADMIN,
//                ),
		array(
                    'header' => 'Actions',
                    'class'=>'CButtonColumn',
                    'template'=> ControllerActionsName::createIndexButtonRoles($actions),
                    'buttons'=>array(
                        'update'=>array(
                            'visible'=> 'GasCheck::AgentCanUpdatePTTTDailyGoback($data)',
                        ),
                        'delete'=>array(
                            'visible'=> 'GasCheck::canDeleteData($data)',
                        ),
                    ),
		),
	),
)); ?>

<script>
$(document).ready(function() {
    fnUpdateColorbox();
});

function fnUpdateColorbox(){   
    fixTargetBlank();
}
</script>