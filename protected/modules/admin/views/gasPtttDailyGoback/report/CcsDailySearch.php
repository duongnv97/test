<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
//	'action'=> GasCheck::getCurl(),
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>
    
    <div class="row more_col">
        <div class="col1">
            <?php echo $form->labelEx($model,'zone_id', array('label'=>'Khu vực')); ?>
            <?php echo $form->dropDownList($model,'zone_id', GasOrders::getZoneNew(), array('class'=>'w-200', 'empty'=>'Select'));?>
        </div>
    </div>

    <div class="row more_col">
        <div class="col1">
                <?php echo Yii::t('translation', $form->label($model,'date_from')); ?>
                <?php 
                    $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                        'model'=>$model,        
                        'attribute'=>'date_from',
                        'options'=>array(
                            'showAnim'=>'fold',
                            'dateFormat'=> MyFormat::$dateFormatSearch,
//                            'minDate'=> '0',
                            'maxDate'=> '0',
                            'changeMonth' => true,
                            'changeYear' => true,
                            'showOn' => 'button',
                            'buttonImage'=> Yii::app()->theme->baseUrl.'/admin/images/icon_calendar_r.gif',
                            'buttonImageOnly'=> true,                                
                        ),        
                        'htmlOptions'=>array(
                            'class'=>'w-16',
                            'size'=>'16',
                            'style'=>'float:left;',
                            'readonly' => 1,
                        ),
                    ));
                ?>     		
        </div>
        <div class="col2">
                <?php echo Yii::t('translation', $form->label($model,'date_to')); ?>
                <?php 
                    $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                        'model'=>$model,        
                        'attribute'=>'date_to',
                        'options'=>array(
                            'showAnim'=>'fold',
                            'dateFormat'=> MyFormat::$dateFormatSearch,
//                            'minDate'=> '0',
                            'maxDate'=> '0',
                            'changeMonth' => true,
                            'changeYear' => true,
                            'showOn' => 'button',
                            'buttonImage'=> Yii::app()->theme->baseUrl.'/admin/images/icon_calendar_r.gif',
                            'buttonImageOnly'=> true,                                
                        ),        
                        'htmlOptions'=>array(
                            'class'=>'w-16',
                            'size'=>'16',
                            'style'=>'float:left;',
                            'readonly' => 1,
                        ),
                    ));
                ?>     		
        </div>
    </div>
    
    <div class="row">
        <?php echo $form->labelEx($model,'agent_id'); ?>
        <?php echo $form->hiddenField($model,'agent_id', array('class'=>'')); ?>
        <?php
        // 1. limit search kh của sale
        $url = Yii::app()->createAbsoluteUrl('admin/ajax/search_by_role', array('role'=> ROLE_AGENT));
        // widget auto complete search user customer and supplier
        $aData = array(
            'model'=>$model,
            'field_customer_id'=>'agent_id',
            'url'=> $url,
            'name_relation_user'=>'rAgent',
            'ClassAdd' => 'w-400',
            'field_autocomplete_name' => 'autocomplete_name',
            'placeholder'=>'Nhập mã hoặc tên đại lý',
        );
        $this->widget('ext.SpaAutocompleteCustomer.SpaAutocompleteCustomer',
            array('data'=>$aData));
        ?>
    </div>
    
    <div class="row">
        <?php echo $form->labelEx($model,'monitoring_id'); ?>
        <?php echo $form->hiddenField($model,'monitoring_id', array('class'=>'')); ?>
        <?php
        // 1. limit search kh của sale
        $url = Yii::app()->createAbsoluteUrl('admin/ajax/search_by_role', array('role'=> ROLE_MONITORING_MARKET_DEVELOPMENT ));
        // widget auto complete search user customer and supplier
        $aData = array(
            'model'=>$model,
            'field_customer_id'=>'monitoring_id',
            'url'=> $url,
            'name_relation_user'=>'rMonitoring',
            'ClassAdd' => 'w-400',
            'field_autocomplete_name' => 'ext_maintain_employee_id',
            'placeholder'=>'Nhập mã hoặc tên NV',
        );
        $this->widget('ext.SpaAutocompleteCustomer.SpaAutocompleteCustomer',
            array('data'=>$aData));
        ?>
    </div>
    
    <div class="row">
        <?php echo $form->labelEx($model,'uid_login', ['label'=>'Nhân viên PTTT']); ?>
        <?php echo $form->hiddenField($model,'uid_login', array('class'=>'')); ?>
        <?php
        // 1. limit search kh của sale
        $url = Yii::app()->createAbsoluteUrl('admin/ajax/search_by_role', array('role'=> ROLE_EMPLOYEE_MARKET_DEVELOPMENT.','.ROLE_EMPLOYEE_MAINTAIN));
        // widget auto complete search user customer and supplier
        $aData = array(
            'model'=>$model,
            'field_customer_id'=>'uid_login',
            'url'=> $url,
            'name_relation_user'=>'rUidLogin',
            'ClassAdd' => 'w-400',
            'field_autocomplete_name' => 'last_update_time',
            'placeholder'=>'Nhập mã hoặc tên NV',
        );
        $this->widget('ext.SpaAutocompleteCustomer.SpaAutocompleteCustomer',
            array('data'=>$aData));
        ?>
    </div>
    
    <div class="row">
        <?php echo $form->labelEx($model,'province_id'); ?>
        <div class="fix-label">
            <?php
               $this->widget('ext.multiselect.JMultiSelect',array(
                     'model'=>$model,
                     'attribute'=>'province_id',
                     'data'=> GasProvince::getArrAll(),
                     // additional javascript options for the MultiSelect plugin
                    'options'=>array('selectedList' => 30,),
                     // additional style
                     'htmlOptions'=>array('style' => 'width: 800px;'),
               ));    
           ?>
        </div>
    </div>
    
    <div class="row buttons" style="padding-left: 159px;">
        <?php $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType'=>'submit',
            'label'=>'Xem',
            'type'=>'null', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
            'size'=>'small', // null, 'large', 'small' or 'mini'
            //'htmlOptions' => array('style' => 'margin-bottom: 10px; float: right;'),
        )); ?>	
    </div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->