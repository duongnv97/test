<?php
$this->breadcrumbs=array(
    $this->pageTitle,
);
?>

<h1><?php echo $this->pageTitle; ?></h1>
<div class="search-form" style="">
<?php $this->renderPartial('report/CcsDailySearch',array(
        'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php if(isset($aData['CCS_ID'])): ?>
<?php 
$OUTPUT_POINT   = $aData['OUTPUT_POINT'];
$WORK_DAYS      = $aData['WORK_DAYS'];
$aUid           = $aData['CCS_ID'];
$OUTPUT         = isset($aData['OUTPUT']) ? $aData['OUTPUT'] : [];
$OUTPUT_SUM     = isset($aData['OUTPUT_SUM']) ? $aData['OUTPUT_SUM'] : [];
$ARR_DAYS       = isset($aData['ARR_DAYS']) ? $aData['ARR_DAYS'] : [];
//$listdataCcs    = AppCache::getListdataUserByRole(ROLE_EMPLOYEE_MARKET_DEVELOPMENT);
//$listdataCcs1   = AppCache::getListdataUserByRole(ROLE_MONITORING_MARKET_DEVELOPMENT);
//$listdataCcs    = $listdataCcs + $listdataCcs1;
$aUser = Users::getArrObjectUserByRole([ROLE_EMPLOYEE_MARKET_DEVELOPMENT, ROLE_MONITORING_MARKET_DEVELOPMENT], []);
$index = 1; $aTeam = [];
if(!empty($model->monitoring_id)){
    $aMonitor   = GasOneMany::getEmployeeOfMonitor(ONE_MONITORING_MARKET_DEVELOPMENT);
    $aTeam      = isset($aMonitor[$model->monitoring_id]) ? $aMonitor[$model->monitoring_id] : [];
    $aTeam[$model->monitoring_id] = $model->monitoring_id;
}

// Apr2718 xử lý get list Agent của province_id
$aAgent = [];$checkAgent = false;
if(!empty($model->zone_id)){
    $aZoneProvinceId    =  GasOrders::getZoneNewProvinceId();
    $aProvinceId        = $aZoneProvinceId[$model->zone_id];
    $aAgent             = MyFormat::getAgentOfProvince($aProvinceId);
    $checkAgent         = true;
}
if(is_array($model->province_id)){
    $aAgent             = MyFormat::getAgentOfProvince($model->province_id);
    $checkAgent         = true;
}
$aAgent         = CHtml::listData($aAgent, 'id', 'id');
$aUserIdOnly    = array_keys($aUid);
$aModelUser     = Users::getArrObjectUserByRole('', $aUserIdOnly, ['GetAll' => 1]);

?>
<div class="grid-view display_none">
<table class="hm_table items freezetablecolumns" id="freezetablecolumns_inventory">
    <thead>
        <tr>
            <th class="item_c w-20" >#</th>
            <th class="item_c w-120" >Nhân Viên</th>
            <th class="item_c w-50" >Tổng</th>
            <th class="item_c w-50" >Công TG</th>
            <?php foreach ($ARR_DAYS as $days): ?>
                <?php $temp = explode('-', $days);?>
                <th class="item_c w-30"><?php echo $temp[2]; ?></th>
            <?php endforeach; ?>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($aUid as $user_id=>$sumPoint): ?>
            <?php 
                if(!empty($model->monitoring_id) && !in_array($user_id, $aTeam)){
                    continue;
                }
//                if( empty($sumPoint) || (!empty($model->uid_login) && $user_id != $model->uid_login)){
                if((!empty($model->uid_login) && $user_id != $model->uid_login)){
                    continue;
                }
                $outputSum  = isset($OUTPUT_SUM[$user_id]) ? $OUTPUT_SUM[$user_id] : 0;
                $workDay    = isset($WORK_DAYS[$user_id]) ? $WORK_DAYS[$user_id] : 0;
                $nameEmployee = '';
                if(!isset($aModelUser[$user_id])){
                    continue;
                }
                $mUser = $aModelUser[$user_id];
                if(isset($aUser[$user_id])){
                    $nameEmployee = $aUser[$user_id]->first_name;
                }else{
//                    $mUser          = Users::model()->findByPk($user_id);
                    $nameEmployee   = $mUser ? $mUser->first_name : '';
                }
                if($checkAgent && !in_array($mUser->parent_id, $aAgent)){
                    continue;
                }
                
            ?>
            <tr>
                <td><?php echo $index++;?></td>
                <td class="item_b"><?php echo $nameEmployee;?></td>
                <td class="item_b item_c"><?php echo $sumPoint.'/'.$outputSum;?></td>
                <td class="item_b item_c"><?php echo $workDay;?></td>
                <?php foreach ($ARR_DAYS as $days): ?>
                    <?php 
                        $qtyPoint   = isset($OUTPUT_POINT[$user_id][$days]) ? $OUTPUT_POINT[$user_id][$days] : 0;
                        $qtyGas     = isset($OUTPUT[$user_id][$days]) ? $OUTPUT[$user_id][$days] : 0;
                        $text = $qtyGas > 0 ? $qtyPoint.'/'.$qtyGas : $qtyPoint;
                    ?>
                    <td class="item_c"><?php echo $text; ?></td>
            <?php endforeach; ?>
            </tr>
        <?php endforeach; // end foreach ($aUid ?>
    </tbody>
</table>
</div>
<?php endif; ?>

<?php Yii::app()->clientScript->registerCoreScript('jquery.ui'); ?>
<?php
$gridCss = Yii::app()->getAssetManager()->publish(Yii::getPathOfAlias('zii.widgets.assets')).'/gridview/styles.css';
Yii::app()->getClientScript()->registerCssFile($gridCss);
?>
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/scroll/jquery.freezetablecolumns.1.1.js"></script> 
<script>
$(function() {
//    $( "#tabs" ).tabs();
    $(".items > tbody > tr:odd").addClass("odd");
    $(".items > tbody > tr:even").addClass("even");   
    fnTabMonthClick();
});

function fnTabMonthClick(){
     $('.freezetablecolumns').each(function(){
        var id_table = $(this).attr('id');
        $('#'+id_table).freezeTableColumns({
            width:       <?php echo Users::WIDTH_freezetable;?>,   // required - bt 1100
            height:      400,   // required
            numFrozen:   4,     // optional
            frozenWidth: 290,   // optional
            clearWidths: true  // optional
        });   
    });
}
$(window).load(function(){
    var index = 1;
    $('.freezetablecolumns').each(function(){
        if(index==1)
            $(this).closest('div.grid-view').show();
        index++;
    });    
    fnAddClassOddEven('items');
});

</script>
