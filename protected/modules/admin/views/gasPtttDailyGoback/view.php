<?php
$this->breadcrumbs=array(
	'Bình Quay Về Hàng Ngày'=>array('index'),
	$model->code_no,
);

$menus = array(
	array('label'=>'Bình Quay Về Hàng Ngày', 'url'=>array('index')),
	array('label'=>'Tạo Mới Bình Quay Về Hàng Ngày', 'url'=>array('create')),
	array('label'=>'Cập Nhật Bình Quay Về Hàng Ngày', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Xóa Bình Quay Về Hàng Ngày', 'url'=>array('delete'), 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Bạn chắc chắn muốn xóa ?')),
);
$this->menu= ControllerActionsName::createMenusRoles($menus, $actions);
?>

<h1>Xem Bình Quay Về Hàng Ngày: <?php echo $model->code_no; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'code_no',
                array(
                    'name'=>'agent_id',
                    'value'=> $model->rAgent?$model->rAgent->first_name:"",
                ),                
                array(
                    'name'=>'maintain_date',
                    'type'=>'date',
                    'htmlOptions' => array('style' => 'text-align:center;')
                ),                
                array(
                    'name'=>'monitoring_id',
                    'value'=>$model->rMonitoring?$model->rMonitoring->first_name:"",
                ),
                array(
                    'label' => 'Chi Tiết',
                    'type' => 'DetailDailyGobackInfo',
                    'value' => $model,
                ),
		array(
                    'name' => 'created_date',
                    'type' => 'Datetime',
                    'htmlOptions' => array('style' => 'width:50px;')
                ), 	
		array(
                    'name' => 'last_update_time',
                    'type' => 'Datetime',
                    'htmlOptions' => array('style' => 'width:50px;')
                ),
                array(
                    'name'=>'last_update_by',
                    'value'=> $model->rLastUpdateBy?$model->rLastUpdateBy->first_name:"",
                ),
	),
)); ?>
