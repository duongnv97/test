<?php
$this->breadcrumbs=array(
	'Bình Quay Về Hàng Ngày'=>array('index'),
	$model->code_no=>array('view','id'=>$model->id),
	'Cập Nhật',
);

$menus = array(	
        array('label'=>'Bình Quay Về Hàng Ngày', 'url'=>array('index')),
	array('label'=>'Xem Bình Quay Về Hàng Ngày', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Tạo Mới Bình Quay Về Hàng Ngày', 'url'=>array('create')),	
);
$this->menu= ControllerActionsName::createMenusRoles($menus, $actions);

?>

<h1>Cập Nhật Bình Quay Về Hàng Ngày: <?php echo $model->code_no; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>