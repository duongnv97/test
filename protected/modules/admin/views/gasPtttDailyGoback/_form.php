<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'gas-pttt-daily-goback-form',
	'enableAjaxValidation'=>false,
        'htmlOptions' => array('enctype' => 'multipart/form-data'),
)); ?>

    <p class="note">Dòng có dấu <span class="required">*</span> là bắt buộc.</p>
    <?php echo MyFormat::BindNotifyMsg(); ?>
    
    <div class="row">
    <?php echo $form->labelEx($model,'agent_id'); ?>
    <?php echo $form->hiddenField($model,'agent_id', array('class'=>'')); ?>
    <?php
            // 1. limit search kh của sale
            $url = Yii::app()->createAbsoluteUrl('admin/ajax/search_by_role', array('role'=> ROLE_AGENT));
            // widget auto complete search user customer and supplier
            $aData = array(
                'model'=>$model,
                'field_customer_id'=>'agent_id',
                'url'=> $url,
                'name_relation_user'=>'rAgent',
                'ClassAdd' => 'w-400',
                'field_autocomplete_name' => 'autocomplete_name',
                'placeholder'=>'Nhập mã hoặc tên đại lý',
            );
            $this->widget('ext.SpaAutocompleteCustomer.SpaAutocompleteCustomer',
                array('data'=>$aData));
            ?>
        <?php echo $form->error($model,'agent_id'); ?>
    </div>
    
    <div class="row">
		<?php echo Yii::t('translation', $form->labelEx($model,'maintain_date')); ?>
                <?php 
                    $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                        'model'=>$model,        
                        'attribute'=>'maintain_date',
                        'options'=>array(
                            'showAnim'=>'fold',
                            'dateFormat'=> ActiveRecord::getDateFormatJquery(),
//                            'minDate'=> '0',
                            'maxDate'=> '0',
                            'changeMonth' => true,
                            'changeYear' => true,
                            'showOn' => 'button',
                            'buttonImage'=> Yii::app()->theme->baseUrl.'/admin/images/icon_calendar_r.gif',
                            'buttonImageOnly'=> true,                                
                        ),        
                        'htmlOptions'=>array(
                            'class'=>'w-16',
                            'style'=>'height:20px;',
                                'readonly'=>'readonly',
                        ),
                    ));
                ?>     		
		<?php echo $form->error($model,'maintain_date'); ?>
	</div>
	<div class="row">
            <?php echo $form->labelEx($model,'monitoring_id'); ?>
            <?php  echo $form->dropDownList($model,'monitoring_id', $model->getDropdownMonitor(),array('class'=>'w-350 monitor_select','empty'=>'Select')); ?>
            <?php echo $form->error($model,'monitoring_id'); ?>
	</div>
    
        <div class="row">
            <label>&nbsp</label>
            <table class="materials_table hm_table">
                <thead>
                    <tr>
                        <th class="item_c">#</th>
                        <th class="item_code item_c w-300">Nhân viên CCS</th>
                        <th class="item_unit last item_c w-100">Số lượng bình</th>
                    </tr>
                </thead>
                <tbody>
                    <?php $key=1; ?>
                    <?php foreach($model->aMaintainEmployee as $employee_id => $name): ?>
                    <tr class="materials_row">
                        <td class="item_c order_no"><?php echo $key++;?></td>
                        <td class="item_l ">
                            <?php echo $name; ?>
                        </td>
                        <td class="item_c last">
                            <input class="w-50 number_only_v1 " name="employee_id[<?php echo $employee_id;?>]" value="<?php echo isset($model->aQty[$employee_id])?$model->aQty[$employee_id]:'';?>" type="text" maxlength="3">
                            <input name="employee_name[<?php echo $employee_id;?>]" value="<?php echo $name;?>" type="hidden">
                        </td>
                    </tr>
                    <?php endforeach;?>
                </tbody>
            </table>
        </div>
    
	<div class="row buttons" style="padding-left: 141px;">
            <?php $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType'=>'submit',
            'label'=>$model->isNewRecord ? 'Create' :'Save',
            'type'=>'null', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
            'size'=>'small', // null, 'large', 'small' or 'mini'
            //'htmlOptions' => array('style' => 'margin-bottom: 10px; float: right;'),
        )); ?>	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->
<script>
    $(document).ready(function(){
        $('.form').find('button:submit').click(function(){
            $.blockUI({ overlayCSS: { backgroundColor: '#fff' } }); 
        });
        BindChangeMonitor();
    });
    
    function BindChangeMonitor(){
        $('.monitor_select').change(function(){            
            var monitoring_id = $(this).val();
            if($.trim(monitoring_id) != ''){
                var url_ = "<?php echo Yii::app()->createAbsoluteUrl('admin/gasPtttDailyGoback/create');?>";
                $.blockUI({ overlayCSS: { backgroundColor: '<?php echo BLOCK_UI_COLOR;?>' } }); 
                $.ajax({
                    url : url_,
                    data: {monitoring_id:monitoring_id},
                    type: 'post',
                    success: function(data){
                        $('.materials_table').html($(data).find('.materials_table').html() );
                        validateNumber();
                        $.unblockUI();
                    }
                });
            }
        });
    }
    
</script>