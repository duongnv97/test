<?php
$this->breadcrumbs=array(
	'Logs',
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('logger-grid', {
                url : $(this).attr('action'),
		data: $(this).serialize()
	});
	return false;
});
");

Yii::app()->clientScript->registerScript('ajaxupdate', "
$('#logger-grid a.ajaxupdate').live('click', function() {
    $.fn.yiiGridView.update('logger-grid', {
        type: 'POST',
        url: $(this).attr('href'),
        success: function() {
            $.fn.yiiGridView.update('logger-grid');
        }
    });
    return false;
});
");
?>

<h1>Logs. Mail remain: <?php echo GasScheduleEmail::model()->count();?> -- Notify wait: <?php echo GasScheduleNotify::countWait();?> 
    -- SMS wait: <?php echo GasScheduleSms::model()->count();?> 
</h1>

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'logger-grid',
	'dataProvider'=>$model->search(),
        'afterAjaxUpdate'=>'function(id, data){ fnUpdateColorbox();}',
        'template'=>'{pager}{summary}{items}{pager}{summary}', 
        'pager' => array(
            'maxButtonCount'=>  CmsFormatter::$PAGE_MAX_BUTTON,
        ),            
	'columns'=>array(
        array(
            'header' => 'S/N',
            'type' => 'raw',
            'value' => '$this->grid->dataProvider->pagination->currentPage * $this->grid->dataProvider->pagination->pageSize + ($row+1)',
            'headerHtmlOptions' => array('width' => '10px','style' => 'text-align:center;'),
            'htmlOptions' => array('style' => 'text-align:center;')
        ),
		'ip_address',
		'country',
		array(
                    'name' => 'created_date',
                    'value' => 'MyFormat::dateConverYmdToDmy($data->created_date, "d/m/Y H:i:s")',
                    'htmlOptions' => array('style' => 'width:50px;')
                ),
		'description',
                array(
                    'name' => 'message',
                    'type' => 'raw',
                ),
		/*
		'created_date',
		'description',
		*/
//		array(
//                    'header' => 'Actions',
//                    'class'=>'CButtonColumn',
//                    'template'=> ControllerActionsName::createIndexButtonRoles($actions),
//		),
	),
)); ?>

<script>
$(document).ready(function() {
    fnUpdateColorbox();
});

function fnUpdateColorbox(){   
    fixTargetBlank();
}
</script>