<?php
$this->breadcrumbs=array(
	$this->singleTitle,
);

$menus=array(
            array('label'=>"Create $this->singleTitle", 'url'=>array('create')),
);
$this->menu= ControllerActionsName::createMenusRoles($menus, $actions);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('mortgage-grid', {
                url : $(this).attr('action'),
		data: $(this).serialize()
	});
	return false;
});
");

Yii::app()->clientScript->registerScript('ajaxupdate', "
$('#mortgage-grid a.ajaxupdate').live('click', function() {
    $.fn.yiiGridView.update('mortgage-grid', {
        type: 'POST',
        url: $(this).attr('href'),
        success: function() {
            $.fn.yiiGridView.update('mortgage-grid');
        }
    });
    return false;
});
");
?>

<h1>Danh Sách <?php echo $this->pluralTitle; ?></h1>


<?php echo CHtml::link('Tìm Kiếm Nâng Cao','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'mortgage-grid',
	'dataProvider'=>$model->search(),
        'afterAjaxUpdate'=>'function(id, data){ fnUpdateColorbox();}',
        'template'=>'{pager}{summary}{items}{pager}{summary}',
//    'itemsCssClass' => 'items custom_here',
//    'htmlOptions'=> array('class'=>'grid-view custom_here'),
        'pager' => array(
            'maxButtonCount'=>  CmsFormatter::$PAGE_MAX_BUTTON,
        ),
	'enableSorting' => false,
	//'filter'=>$model,
	'columns'=>array(
        array(
            'header' => 'S/N',
            'type' => 'raw',
            'value' => '$this->grid->dataProvider->pagination->currentPage * $this->grid->dataProvider->pagination->pageSize + ($row+1)',
            'headerHtmlOptions' => array('width' => '10px','style' => 'text-align:center;'),
            'htmlOptions' => array('style' => 'text-align:center;')
        ),
            'code_no',
            [
                'name'=>'company_id',
                'value'=>'$data->getCompany()',
            ],
            [
                'name'=>'bank_id',
                'value'=>'$data->getBank()',
            ],
            [
                'name'=>'type',
                'value'=>'$data->showType()',
                'htmlOptions' => array('style' => 'text-align:center;')
            ],
            [
                'name'=>'asset_id',
                'value'=>'$data->getAsset()',
            ],
            [
                'name'=>'asset_group',
                'value'=>'$data->getAssetGroup()',
                'htmlOptions' => array('style' => 'text-align:center;')
            ],
//            [
//                'header' => 'Diễn giải TS',
//                'value'=>'$data->getAsset("reason_leave")',
//            ],
            [
                'name'=>'asset_amount',
                'value'=>'$data->getAssetAmount(true)',
            ],
            [
                'name'=>'bank_credit',
                'value'=>'$data->getBankCredit(true)',
            ],
            [
                'name'=>'bank_ratio',
                'value'=>'$data->getBankRatio()',
                'htmlOptions' => array('style' => 'text-align:center;')
            ],
            [
                'name'=>'borrow_ration',
                'value'=>'$data->getBorrowRation()',
                'htmlOptions' => array('style' => 'text-align:center;')
            ],
            [
                'name'=>'bank_credit_type',
                'value'=>'$data->getBankCreditType()',
            ],
            [
                'name'=>'borrow_time',
                'value'=>'$data->getBorrowTime()',
            ],
            [
                'name'=>'limit_date',
                'value'=>'$data->getLimitDate()',
            ],
            [
                'name'=>'status',
                'type'=>'status',
                'htmlOptions' => array('style' => 'text-align:center;')
            ],
            [
                'name' => 'created_date',
                'value' => '$data->getCreatedDate()',
                'htmlOptions' => array('style' => 'width:50px;'),
            ],
            array(
                'header' => 'Actions',
                'class'=>'CButtonColumn',
                'template'=> ControllerActionsName::createIndexButtonRoles($actions),
                'buttons'=>array(
                    'update' => array(
                        'visible' => '$data->canUpdate()',
                    ),
                    'delete'=>array(
                        'visible'=> 'GasCheck::canDeleteData($data)',
                    ),
                ),
            ),
	),
)); ?>

<script>
$(document).ready(function() {
    fnUpdateColorbox();
});

function fnUpdateColorbox(){   
    fixTargetBlank();
}
</script>