<?php
$this->breadcrumbs=array(
    $this->pluralTitle => array('index'),
    ' ' . $this->singleTitle . ' : ',
);

$menus = array(
	array('label'=>"$this->pluralTitle Management", 'url'=>array('index')),
	array('label'=>"Tạo Mới $this->singleTitle", 'url'=>array('create')),
	array('label'=>"Cập Nhật $this->singleTitle", 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>"Xóa $this->singleTitle", 'url'=>array('delete'), 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Bạn chắc chắn muốn xóa ?')),
);
$this->menu= ControllerActionsName::createMenusRoles($menus, $actions);
?>

<h1>Xem: <?php echo $this->singleTitle; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
            'code_no',
            [
                'name'=>'company_id',
                'value'=>$model->getCompany(),
            ],
            [
                'name'=>'bank_id',
                'value'=>$model->getBank(),
            ],
            [
                'name'=>'asset_id',
                'value'=>$model->getAsset(),
            ],
            [
                'header' => 'Diễn giải TS',
                'value'=>$model->getAsset("reason_leave"),
            ],
            [
                'name'=>'type',
                'value'=>$model->showType(),
            ],
            [
                'name'=>'asset_amount',
                'value'=>$model->getAssetAmount(true),
            ],
            [
                'name'=>'bank_credit',
                'value'=>$model->getBankCredit(true),
            ],
            [
                'name'=>'limit_date',
                'value'=>$model->getLimitDate('d/m/Y'),
            ],
            [
                'name'=>'bank_ratio',
                'value'=>$model->getBankRatio(),
            ],
            [
                'name'=>'interest_rate',
                'value'=>$model->getInterestRate(),
            ],
            [
                'name'=>'limit_payment_guarantee',
                'value'=>$model->getLimitPaymentGuarantee(),
            ],
            [
                'name'=>'bank_credit_type',
                'value'=>$model->getBankCreditType(),
            ],
            [
                'name'=>'borrow_time',
                'value'=>$model->getBorrowTime(true),
            ],
            [
                'name'=>'note',
                'value'=>$model->getNote(),
            ],
            [
                'name'=>'status',
                'type'=>'status',
                'htmlOptions' => array('style' => 'text-align:center;')
            ],
            [
                'name' => 'created_date',
                'value' => $model->getCreatedDate(),
            ],
	),
)); ?>
