<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=> GasCheck::getCurl(),
	'method'=>'get',
)); ?>
    
    <div class="row">
    <?php echo $form->labelEx($model,'asset_id'); ?>
    <?php echo $form->hiddenField($model,'asset_id', array('class'=>'')); ?>
    <?php
            // 1. limit search kh của sale
            $url = Yii::app()->createAbsoluteUrl('admin/ajax/search_by_role', array('role'=> ROLE_ASSETS));
            // widget auto complete search user customer and supplier
            $aData = array(
                'model'=>$model,
                'field_customer_id'=>'asset_id',
                'url'=> $url,
                'name_relation_user'=>'rAsset',
                'ClassAdd' => 'w-400',
                'field_autocomplete_name' => 'autocomplete_name',
                'placeholder'=>'Nhập mã hoặc tên đại lý',
            );
            $this->widget('ext.SpaAutocompleteCustomer.SpaAutocompleteCustomer',
                array('data'=>$aData));
            ?>
        <?php echo $form->error($model,'asset_id'); ?>
    </div>
    

    <div class="row">
        <?php echo $form->label($model,'company_id',array()); ?>
        <?php echo $form->dropDownList($model,'company_id', $model->getListdataByRole(ROLE_COMPANY), ['class'=>'w-400','empty'=> 'Select']); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model,'bank_id',array()); ?>
        <?php echo $form->dropDownList($model,'bank_id', $model->getListdataByRole(ROLE_BANK), ['class'=>'w-400','empty'=> 'Select']); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model,'bank_credit_type',array()); ?>
        <?php echo $form->dropDownList($model,'bank_credit_type', $model->getArrayCreditType(),array('class'=>' w-400', 'empty'=>'Select')); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model,'status',array()); ?>
        <?php echo $form->dropDownList($model,'status', ActiveRecord::getUserStatus(), ['class'=>'w-400', 'empty'=>'Select']); ?>
    </div>

    <div class="row buttons" style="padding-left: 159px;">
                    <?php $this->widget('bootstrap.widgets.TbButton', array(
        'buttonType'=>'submit',
        'label'=>'Search',
        'type'=>'null', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
        'size'=>'small', // null, 'large', 'small' or 'mini'
        //'htmlOptions' => array('style' => 'margin-bottom: 10px; float: right;'),
    )); ?>	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->