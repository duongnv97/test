<?php $this->breadcrumbs=array(
    $this->pageTitle,
); ?>
<h1><?php echo $this->pageTitle;?></h1>

<div class="search-form" style="">
<?php $this->renderPartial('report/_search',array(
        'model'=>$model,
)); ?>
</div><!-- search-form -->
<?php 
    $mBorrow        = new Borrow();
    $aData          = $model->report();
    $dataRowspan    = $model->calcRowSpan($aData);
//    $mBorrow->borrow_type = $model->bank_credit_type;
    $dataBorrow     = $mBorrow->report();
    $dataDaoHan     = $mBorrow->reportDaoHan();
    
    $DETAIL             = $dataBorrow['DETAIL'];
    $SUM                = $dataBorrow['SUM'];
    $SUM_BORROW_TYPE    = $dataBorrow['SUM_BORROW_TYPE'];
    $BORROW_TYPE        = $dataBorrow['BORROW_TYPE'];
    $rowSpanCompany     = false; $cCompany = 0;
    $rowSpanBank        = false; $cBank    = 0;
    $rowSpanGroupAsset  = false; $cGroupAsset = '';
    $lastBank = $aData[0]->bank_id;
    $sumBank = [];$sumDaoHan = 0;
?>
<table class="tb hm_table f_size_13">
    <thead>
        <tr>
            <th class="item_b">Công ty</th>
            <th class="item_b">Ngân hàng</th>
            <th class="item_b">Hạn mức tín dụng</th>
            <th class="item_b">Hệ số tín chấp</th>
            <th class="item_b">Hệ số cho vay</th>
            <th class="item_b">Tài sản</th>
            <th class="item_b">Định giá</th>
            <th class="item_b">Số tiền được giải ngân</th>
            <th class="item_b">Đã giải ngân</th>
            <th class="item_b">Đáo hạn trong tháng</th>
        </tr>
    </thead>
    <tbody>
    <?php foreach ($dataRowspan['ASSETS_SUM'] as $company_id => $aInfo): ?>
        <?php 
            $aSumCompany['DINH_GIA'] = 0; $aSumCompany['GIAI_NGAN'] = 0;$aSumCompany['DA_GIAI_NGAN'] = 0; 
        ?>
        <?php foreach ($aInfo as $bank_id => $aInfo1): ?>
            <?php 
                $aSumBank['DINH_GIA'] = 0; $aSumBank['GIAI_NGAN'] = 0;
            ?>
            <?php foreach ($aInfo1 as $asset_group => $aInfo2): ?>
                <?php // Số tiền được giải ngân = (Tiền định giá x hệ số cho vay)/ hệ số tín chấp
                     $item =  $aInfo2['data'];
                    $borrowRemain       = isset($DETAIL[$company_id][$bank_id]) ? ActiveRecord::formatCurrencyRound($DETAIL[$company_id][$bank_id]) : '';
                    $assetSum           = $dataRowspan['ASSETS_SUM'][$company_id][$bank_id][$item->asset_group]['sum'];
                    $assetGiaiNganSum   = $dataRowspan['ASSETS_GIAI_NGAN_SUM'][$company_id][$bank_id][$item->asset_group];
                    
                    $amountDaoHan       = isset($dataDaoHan[$company_id][$bank_id]) ? $dataDaoHan[$company_id][$bank_id] : 0;
                    if(!isset($sumBank[$bank_id]['DINH_GIA'])){
                        $sumBank[$bank_id]['DINH_GIA'] = $assetSum;
                    }else{
                        $sumBank[$bank_id]['DINH_GIA'] += $assetSum;
                    }
                    if(!isset($sumBank[$bank_id]['GIAI_NGAN'])){
                        $sumBank[$bank_id]['GIAI_NGAN'] = $assetGiaiNganSum;
                    }else{
                        $sumBank[$bank_id]['GIAI_NGAN'] += $assetGiaiNganSum;
                    }

                    if($cCompany != $company_id){
                        $rowSpanCompany = true;
                    }
                    if($cBank != $bank_id){
                        $rowSpanBank = true;
                    }
                    $cCompany       = $company_id;
                    $cBank          = $bank_id;
                    $aSumBank['DINH_GIA']       += $assetSum;
                    $aSumBank['GIAI_NGAN']      += $assetGiaiNganSum;
                    $aSumCompany['DINH_GIA']    += $assetSum;
                    $aSumCompany['GIAI_NGAN']   += $assetGiaiNganSum;

                ?>
                <tr>
                <?php if($rowSpanCompany): $rowSpanCompany = false; ?>
                <td rowspan="<?php echo ($dataRowspan['COMPANY'][$company_id] + count($aInfo));?>"><?php echo $item->getCompany();?></td>
                <?php endif; ?>

                <?php if($rowSpanBank): ?>
                    <td rowspan="<?php echo $dataRowspan['BANK'][$company_id][$bank_id];?>"><?php echo $item->getBank();?></td>
                <?php endif; ?>


                <td class="item_c"><?php echo $item->getBankCreditShort();?></td>
                <td class="item_c"><?php echo $item->getBankRatio();?></td>
                <td class="item_c"><?php echo $item->getBorrowRation();?></td>
                <td class="item_r"><?php echo $item->getAsset();?></td>
                <td class="item_r"><?php echo ActiveRecord::formatCurrency($assetSum);?></td>
                <td class="item_r"><?php echo ActiveRecord::formatCurrencyRound($assetGiaiNganSum);?></td>

                <?php if($rowSpanBank): ?>
                <td class="item_r" rowspan="<?php echo $dataRowspan['BANK'][$company_id][$bank_id];?>">
                    <?php if(!empty($borrowRemain)): ?>
                    <?php // echo $borrowRemain;?>
                    <?php foreach ($mBorrow->getArrayTypeShortName() as $borrow_type_id => $short_name): ?>
                        <?php if($item->bank_credit_type != $borrow_type_id) {continue;} 
                            $borrow_type_amount = isset($BORROW_TYPE[$company_id][$bank_id][$borrow_type_id]) ? $BORROW_TYPE[$company_id][$bank_id][$borrow_type_id] : 0; 
                            $aSumCompany['DA_GIAI_NGAN']   += $borrow_type_amount;
                            echo $borrow_type_amount > 0 ? "<br>($short_name) ".ActiveRecord::formatCurrency($borrow_type_amount) : '';
                        ?>
                    <?php endforeach; ?>
                    <?php endif; ?>
                </td>
                <?php $rowSpanBank = false; $sumDaoHan += $amountDaoHan;?>
                <td class="item_r" rowspan="<?php echo $dataRowspan['BANK'][$company_id][$bank_id];?>"><?php echo ActiveRecord::formatCurrencyRound($amountDaoHan);?></td>
                <?php endif; ?>
            </tr>
        
            <?php endforeach; // end foreach ($aInfo1 as $asset_group ?>
            <!--for sum each bank_id-->
            <tr>
                <td class="item_b item_r" colspan="5">Tổng cộng</td>
                <td class="item_b item_r"><?php echo ActiveRecord::formatCurrencyRound($aSumBank['DINH_GIA']);?></td>
                <td class="item_b item_r"><?php echo ActiveRecord::formatCurrencyRound($aSumBank['GIAI_NGAN']);?></td>
                <td></td><td></td>
            </tr>
        <?php endforeach; // end foreach ($aInfo as $bank_id ?>
            <!--for sum each company_id-->
            <tr>
                <td class="item_b item_r" colspan="6">Tổng CTY</td>
                <td class="item_b item_r"><?php echo ActiveRecord::formatCurrencyRound($aSumCompany['DINH_GIA']);?></td>
                <td class="item_b item_r"><?php echo ActiveRecord::formatCurrencyRound($aSumCompany['GIAI_NGAN']);?></td>
                <td class="item_b item_r"><?php echo ActiveRecord::formatCurrencyRound($aSumCompany['DA_GIAI_NGAN']);?></td>
                <td></td>
            </tr>
     <?php endforeach; // end foreach ($dataRowspan['ASSETS_SUM'] ?>
    </tbody>
    <!--for sum đáo hạn all-->
    <tr>
        <td class="item_b item_r" colspan="9">Tổng Đáo Hạn</td>
        <td class="item_b item_r"><?php echo ActiveRecord::formatCurrencyRound($sumDaoHan);?></td>
    </tr>
</table>
<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/print-store-card.css" />
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/jquery.floatThead.min.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/underscore-min.js"></script>
<script>
    $(document).ready(function() {
        $('.hm_table').floatThead();
    });
</script>