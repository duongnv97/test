<?php $this->breadcrumbs=array(
    $this->pageTitle,
); ?>
<h1><?php echo $this->pageTitle;?></h1>

<div class="search-form" style="">
<?php $this->renderPartial('report/_search',array(
        'model'=>$model,
)); ?>
</div><!-- search-form -->
<?php 
    $mBorrow        = new Borrow();
    $aData          = $model->report();
    $dataRowspan    = $model->calcRowSpan($aData);
//    $mBorrow->borrow_type = $model->bank_credit_type;
    $dataBorrow     = $mBorrow->report();
    $dataDaoHan     = $mBorrow->reportDaoHan();
    
    $DETAIL             = $dataBorrow['DETAIL'];
    $SUM                = $dataBorrow['SUM'];
    $SUM_BORROW_TYPE    = $dataBorrow['SUM_BORROW_TYPE'];
    $BORROW_TYPE        = $dataBorrow['BORROW_TYPE'];
    $rowSpanCompany     = false; $cCompany = 0;
    $rowSpanBank        = false; $cBank    = 0;
    $rowSpanGroupAsset  = false; $cGroupAsset = '';
    //$lastBank = $aData[0]->bank_id;
    $sumBank = [];$sumDaoHan = 0;
    $sumAll['DINH_GIA'] = 0;
    $sumAll['EXCHANGE'] = 0;
    $sumAll['BORROW_AMOUNT'] = 0;
?>
<table class="tb hm_table f_size_13">
    <thead>
        <tr>
            <th class="item_b">Công ty</th>
            <th class="item_b">Ngân hàng</th>
            <th class="item_b">Tài sản thế chấp</th>
            <th class="item_b">Mục</th>
            <th class="item_b">Định giá</th>
            <th class="item_b">Hệ số TSĐB</th>
            <th class="item_b">Giá trị sau quy đổi</th>
            <th class="item_b">Hạn mức đã sử dụng</th>
            <th class="item_b">Ghi chú</th>
        </tr>
    </thead>
    <tbody>
    <?php if(isset($dataRowspan['ASSETS_SUM'])): ?>
    <?php foreach ($dataRowspan['ASSETS_SUM'] as $company_id => $aInfo): ?>
        <?php 
            $cBank          = -1; // reset this variable for each company
            $aSumCompany['DINH_GIA'] = 0; $aSumCompany['GIAI_NGAN'] = 0;$aSumCompany['BORROW_AMOUNT'] = 0;
            $aSumCompany['EXCHANGE'] = 0;
        ?>
        
        <?php foreach ($aInfo as $bank_id => $aInfo1): ?>
            <?php 
            $aSumBank['DINH_GIA'] = 0; $aSumBank['GIAI_NGAN'] = 0;$aSumBank['EXCHANGE'] = 0;?>
            <?php foreach ($aInfo1 as $asset_id => $aInfo2): ?>
                <?php // Số tiền được giải ngân = (Tiền định giá x hệ số cho vay)/ hệ số tín chấp
                    $item =  $aInfo2['data'];
                     
                    $borrowRemain       = isset($DETAIL[$company_id][$bank_id]) ? ActiveRecord::formatCurrencyRound($DETAIL[$company_id][$bank_id]) : '';
                    $assetSum           = $dataRowspan['ASSETS_SUM'][$company_id][$bank_id][$item->asset_id]['sum'];
                    $assetGiaiNganSum   = $dataRowspan['ASSETS_GIAI_NGAN_SUM'][$company_id][$bank_id][$item->asset_id];
                    $assetAfterExchange = $assetSum*$item->borrow_ration/100;
                    $amountDaoHan       = isset($dataDaoHan[$company_id][$bank_id]) ? $dataDaoHan[$company_id][$bank_id] : 0;
                    if(!isset($sumBank[$bank_id]['DINH_GIA'])){
                        $sumBank[$bank_id]['DINH_GIA'] = $assetSum;
                    }else{
                        $sumBank[$bank_id]['DINH_GIA'] += $assetSum;
                    }
                    if(!isset($sumBank[$bank_id]['EXCHANGE'])){
                        $sumBank[$bank_id]['EXCHANGE'] = $assetAfterExchange;
                    }else{
                        $sumBank[$bank_id]['EXCHANGE'] += $assetAfterExchange;
                    }
                    if(!isset($sumBank[$bank_id]['GIAI_NGAN'])){
                        $sumBank[$bank_id]['GIAI_NGAN'] = $assetGiaiNganSum;
                    }else{
                        $sumBank[$bank_id]['GIAI_NGAN'] += $assetGiaiNganSum;
                    }

                    if($cCompany != $company_id){
                        $rowSpanCompany = true;
                    }
                    if($cBank != $bank_id){
                        $rowSpanBank = true;
                    }
                    
                    $cCompany       = $company_id;

                    $cBank          = $bank_id;
                    $aSumBank['DINH_GIA']       += $assetSum;
                    $aSumBank['GIAI_NGAN']      += $assetGiaiNganSum;
                    $aSumBank['EXCHANGE']       += $assetAfterExchange;
                    $aSumCompany['DINH_GIA']    += $assetSum;
                    $aSumCompany['GIAI_NGAN']   += $assetGiaiNganSum;
                    $aSumCompany['EXCHANGE']    += $assetAfterExchange;
                    $sumAll['DINH_GIA']         += $assetSum;
                    $sumAll['EXCHANGE']         += $assetAfterExchange;

                ?>
                <tr>
                <?php if($rowSpanCompany): $rowSpanCompany = false; ?>
                <td rowspan="<?php echo ($dataRowspan['COMPANY'][$company_id] + count($aInfo));?>"><?php echo $item->getCompany();?></td>
                <?php endif; ?>
                
                
                <?php if($rowSpanBank): ?>
                <td rowspan="<?php echo $dataRowspan['BANK'][$company_id][$bank_id];?>">
                    <?php 
                        echo $item->getBank();
                        echo "<br>Hệ số tín chấp: ".$item->getBankRatio(); 
                        echo "<br>Hạn mức cho vay:<br>".$item->getBankCredit(true); 
                        echo "<br>Hạn mức BLTT:<br>".$item->getLimitPaymentGuarantee(); 
                        echo "<br>Ngày cấp HM :".$item->getLimitDate();
                        echo "<br>Thời gian vay: ".$item->getBorrowTime(true); 
                        echo "<br>Lãi suất năm: ".$item->getInterestRate(); 
                    ?> </td>
                <?php endif; ?>

                <td class="item_l"><?php echo $item->getAsset();?></td>
                <td class="item_c"><?php echo $item->showType();?>
                <td class="item_r"><?php echo ActiveRecord::formatCurrency($assetSum);?></td>
                <td class="item_c"><?php echo $item->getBorrowRation();?></td>
                
                <td class="item_r"><?php echo ActiveRecord::formatCurrencyRound($assetAfterExchange);?></td>
                <?php if($rowSpanBank): ?>
                <td class="item_r" rowspan ="<?php echo $dataRowspan['BANK'][$company_id][$bank_id];?>">
                    <?php // echo $borrowRemain;?>
                    <?php if(!empty($borrowRemain)): ?>
                    <?php foreach ($mBorrow->getArrayTypeShortName() as $borrow_type_id => $short_name): ?>
                        <?php if($item->bank_credit_type != $borrow_type_id) {continue;} 
//                            $borrow_type_amount = isset($BORROW_TYPE[$company_id][$bank_id][$borrow_type_id]) ? $BORROW_TYPE[$company_id][$bank_id][$borrow_type_id] : 0; 
//                            $aSumCompany['BORROW_AMOUNT']   += $borrow_type_amount;
//                            $sumAll['BORROW_AMOUNT'] += $borrow_type_amount;
                            $amount = isset($DETAIL[$company_id][$bank_id]) ? $DETAIL[$company_id][$bank_id] : 0;
                            $aSumCompany['BORROW_AMOUNT']   += $amount;
                            $sumAll['BORROW_AMOUNT'] += $amount;
                            echo $amount > 0 ? ActiveRecord::formatCurrency($amount) : '';
                        ?>
                    <?php endforeach; ?>
                    <?php endif; ?>
                </td>
                <?php endif; ?>
                <?php $rowSpanBank = false;?>
                <td class="item_l"><?php echo $item->getNote();?> </td>
                
            </tr>
        
            <?php endforeach; // end foreach ($aInfo1 as $asset_id ?>
            <!--for sum each bank_id-->
            <tr>
                <td class="item_b item_r" colspan="3">Tổng cộng</td>
                <td class="item_b item_r"><?php echo ActiveRecord::formatCurrencyRound($aSumBank['DINH_GIA']);?></td>
                <td class="item_b item_r"><?php echo "";?></td>
                <td class="item_b item_r"><?php echo ActiveRecord::formatCurrencyRound($aSumBank['EXCHANGE']);?></td>
                <td></td><td></td>
            </tr>
        <?php endforeach; // end foreach ($aInfo as $bank_id ?>
            <!--for sum each company_id-->
            <tr>
                <td class="item_b item_r" colspan="4">Tổng CTY</td>
                <td class="item_b item_r"><?php echo ActiveRecord::formatCurrencyRound($aSumCompany['DINH_GIA']);?></td>
                <td class="item_b item_r"><?php echo "";?></td>
                <td class="item_b item_r"><?php echo ActiveRecord::formatCurrencyRound($aSumCompany['EXCHANGE']);?></td>
                <td class="item_b item_r"><?php echo ActiveRecord::formatCurrencyRound($aSumCompany['BORROW_AMOUNT']);?></td>
                <td></td>
            </tr>
     <?php endforeach; // end foreach ($dataRowspan['ASSETS_SUM'] ?>
    </tbody>
    <!--for sum đáo hạn all-->
    <?php endif; ?>
    <tr>
        <td class="item_b item_l" colspan="4">Tổng Cộng</td>
        <td class="item_b item_r"><?php echo ActiveRecord::formatCurrencyRound($sumAll['DINH_GIA']);?></td>
        <td></td>
        <td class="item_b item_r"><?php echo ActiveRecord::formatCurrencyRound($sumAll['EXCHANGE']);?></td>
        <td class="item_b item_r"><?php echo ActiveRecord::formatCurrencyRound($sumAll['BORROW_AMOUNT']);?></td>
        <td></td>
    </tr>
    
    
</table>
<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/print-store-card.css" />
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/jquery.floatThead.min.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/underscore-min.js"></script>
<script>
    $(document).ready(function() {
        $('.hm_table').floatThead();
    });
</script>