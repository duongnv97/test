<?php $this->breadcrumbs=array(
    $this->pageTitle,
); ?>
<h1><?php echo $this->pageTitle;?></h1>

<div class="search-form" style="">
<?php $this->renderPartial('report/_search',array(
        'model'=>$model,
)); ?>
</div><!-- search-form -->
<?php 
    $mBorrow        = new Borrow();
    $aData          = $model->report();
    $dataRowspan    = $model->calcRowSpan($aData);
//    $mBorrow->borrow_type = $model->bank_credit_type;
    $dataBorrow     = $mBorrow->report();
//    echo '<pre>';
//print_r($dataRowspan);
//echo '</pre>';
//die;
    $DETAIL             = $dataBorrow['DETAIL'];
    $SUM                = $dataBorrow['SUM'];
    $SUM_BORROW_TYPE    = $dataBorrow['SUM_BORROW_TYPE'];
    $BORROW_TYPE        = $dataBorrow['BORROW_TYPE'];
    $rowSpanCompany     = false; $cCompany = 0;
    $rowSpanBank        = false; $cBank    = 0;
    $rowSpanGroupAsset  = false; $cGroupAsset = '';
    $lastBank = $aData[0]->bank_id;
    $sumBank = [];
?>
<table class="tb hm_table f_size_13">
    <thead>
        <tr>
            <th class="item_b">Công ty</th>
            <th class="item_b">Ngân hàng</th>
            <th class="item_b">Hạn mức tín dụng</th>
            <th class="item_b">Hệ số tín chấp</th>
            <th class="item_b">Hệ số cho vay</th>
            <th class="item_b">Tài sản</th>
            <th class="item_b">Định giá</th>
            <th class="item_b">Số tiền được giải ngân</th>
            <th class="item_b">Đã giải ngân</th>
        </tr>
    </thead>
    <tbody>
    <?php foreach ($aData as $item): ?>
    <?php // Số tiền được giải ngân = (Tiền định giá x hệ số cho vay)/ hệ số tín chấp
        $moneyWillReceive = ($item->asset_amount * $item->borrow_ration)/$item->bank_ratio;
        $moneyWillReceive = ActiveRecord::formatCurrencyRound($moneyWillReceive);
        $company_id     = $item->company_id;
        $bank_id        = $item->bank_id;
        $asset_group    = $item->asset_group;
        $borrowRemain = isset($DETAIL[$company_id][$bank_id]) ? ActiveRecord::formatCurrencyRound($DETAIL[$company_id][$bank_id]) : '';
        
        $assetSum           = $dataRowspan['ASSETS_SUM'][$company_id][$bank_id][$item->asset_group];
        $assetGiaiNganSum   = $dataRowspan['ASSETS_GIAI_NGAN_SUM'][$company_id][$bank_id][$item->asset_group];
        if(!isset($sumBank[$bank_id]['DINH_GIA'])){
            $sumBank[$bank_id]['DINH_GIA'] = $assetSum;
        }else{
            $sumBank[$bank_id]['DINH_GIA'] += $assetSum;
        }
        if(!isset($sumBank[$bank_id]['GIAI_NGAN'])){
            $sumBank[$bank_id]['GIAI_NGAN'] = $assetGiaiNganSum;
        }else{
            $sumBank[$bank_id]['GIAI_NGAN'] += $assetGiaiNganSum;
        }
        
        if($cCompany != $company_id){
            $rowSpanCompany = true;
        }
        if($cBank != $bank_id){
            $rowSpanBank = true;
        }
        if($cGroupAsset != $asset_group){
            $rowSpanGroupAsset = true;
        }

        $cCompany       = $company_id;
        $cBank          = $bank_id;
        $cGroupAsset    = $asset_group;
        
    ?>
    <?php if($lastBank != $bank_id): ?>
    <tr>
        <td></td>
        <td>han muc</td>
        <td>hs</td>
        <td>hs cho vay</td>
        <td>Ts</td>
        <td><?php echo ActiveRecord::formatCurrency($sumBank[$lastBank]['DINH_GIA']);?></td>
        <td>Giai ngan</td>
    </tr>
    <?php endif; ?>
    <tr>
        <?php if($rowSpanCompany): $rowSpanCompany = false; ?>
        <td rowspan="<?php echo $dataRowspan['COMPANY'][$company_id];?>"><?php echo $item->getCompany();?></td>
        <?php endif; ?>
        
        <?php if($rowSpanBank): ?>
            <td rowspan="<?php echo $dataRowspan['BANK'][$company_id][$bank_id];?>"><?php echo $item->getBank();?></td>
        <?php endif; ?>

        
        <?php if($rowSpanGroupAsset): $rowSpanGroupAsset = false; ?>
        <td class="item_c" rowspan="<?php echo $dataRowspan['ASSETS'][$company_id][$bank_id][$asset_group];?>"><?php echo $item->getBankCreditShort();?></td>
        <td class="item_c" rowspan="<?php echo $dataRowspan['ASSETS'][$company_id][$bank_id][$asset_group];?>"><?php echo $item->getBankRatio();?></td>
        <td class="item_c" rowspan="<?php echo $dataRowspan['ASSETS'][$company_id][$bank_id][$asset_group];?>"><?php echo $item->getBorrowRation();?></td>
        <td class="item_r" rowspan="<?php echo $dataRowspan['ASSETS'][$company_id][$bank_id][$asset_group];?>"><?php echo $item->getAsset();?></td>
        <td class="item_r" rowspan="<?php echo $dataRowspan['ASSETS'][$company_id][$bank_id][$asset_group];?>"><?php echo ActiveRecord::formatCurrency($assetSum);?></td>
        <td class="item_r" rowspan="<?php echo $dataRowspan['ASSETS'][$company_id][$bank_id][$asset_group];?>"><?php echo ActiveRecord::formatCurrencyRound($assetGiaiNganSum);?></td>
        <?php endif; ?>
        
        <?php if($rowSpanBank): ?>
        <td class="item_r" rowspan="<?php echo $dataRowspan['BANK'][$company_id][$bank_id];?>">
            <?php if(!empty($borrowRemain)): ?>
            <?php // echo $borrowRemain;?>
            <?php foreach ($mBorrow->getArrayTypeShortName() as $borrow_type_id => $short_name): ?>
                <?php if($item->bank_credit_type != $borrow_type_id) continue; $borrow_type_amount = isset($BORROW_TYPE[$company_id][$bank_id][$borrow_type_id]) ? $BORROW_TYPE[$company_id][$bank_id][$borrow_type_id] : 0; 
                    echo $borrow_type_amount > 0 ? "<br>($short_name) ".ActiveRecord::formatCurrency($borrow_type_amount) : '';
                ?>
            <?php endforeach; ?>
        <?php endif; ?>
        </td>
        <?php endif; ?>
    </tr>
    
    
    <?php $rowSpanBank = false; $lastBank = $bank_id; ?>
    <?php endforeach; ?>
    </tbody>
</table>
<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/print-store-card.css" />