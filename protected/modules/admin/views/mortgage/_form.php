<?php
    if($model->isNewRecord){
        $model->initData();
    }
?>
<div class="form">
<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'mortgage-form',
	'enableAjaxValidation'=>false,
        'htmlOptions' => array('enctype' => 'multipart/form-data'),
)); ?>

    <p class="note">Dòng có dấu <span class="required">*</span> là bắt buộc.</p>
    <?php echo MyFormat::BindNotifyMsg(); ?>
    <div class="row">
    <?php echo $form->labelEx($model,'asset_id'); ?>
    <?php echo $form->hiddenField($model,'asset_id', array('class'=>'')); ?>
    <?php
            // 1. limit search kh của sale
            $url = Yii::app()->createAbsoluteUrl('admin/ajax/search_by_role', array('role'=> ROLE_ASSETS));
            // widget auto complete search user customer and supplier
            $aData = array(
                'model'=>$model,
                'field_customer_id'=>'asset_id',
                'url'=> $url,
                'name_relation_user'=>'rAsset',
                'ClassAdd' => 'w-400',
                'field_autocomplete_name' => 'autocomplete_name',
                'placeholder'=>'Nhập mã hoặc tên đại lý',
                'fnSelectCustomer'=>'fnSelectAsset',
            );
            $this->widget('ext.SpaAutocompleteCustomer.SpaAutocompleteCustomer',
                array('data'=>$aData));
            ?>
        <?php echo $form->error($model,'asset_id'); ?>
    </div>
    
    <div class="row f_size_16">
        <?php echo $form->labelEx($model,'asset_amount'); ?>
        <?php echo $form->textField($model,'asset_amount',array('class'=>'w-150 number_only ad_fix_currency f_size_16','maxlength'=>20)); ?>
        <?php echo $form->error($model,'asset_amount'); ?>
    </div>    		
        
    <div class="row">
        <?php echo $form->labelEx($model,'company_id'); ?>
        <?php echo $form->dropDownList($model,'company_id', $model->getListdataByRole(ROLE_COMPANY), ['class'=>'w-400','empty'=> 'Select']); ?>
        <?php echo $form->error($model,'company_id'); ?>
    </div>
    <div class="row">
        <?php echo $form->labelEx($model,'bank_id'); ?>
        <?php echo $form->dropDownList($model,'bank_id', $model->getListdataByRole(ROLE_BANK), ['class'=>'w-400','empty'=> 'Select']); ?>
        <?php echo $form->error($model,'bank_id'); ?>
    </div>
    
    <!--KhueNM-->
    <div class="row">
        <?php echo $form->labelEx($model,'type'); ?>
        <?php echo $form->dropDownList($model,'type',  $model->getType(), ['class'=>'w-400','empty'=> 'Select']); ?>
        <?php echo $form->error($model,'type'); ?>
    </div>
    
    <div class="row">
        <?php echo $form->labelEx($model,'bank_credit'); ?>
        <?php echo $form->textField($model,'bank_credit',array('class'=>'w-150 number_only ad_fix_currency f_size_16','maxlength'=>20)); ?>
        <?php echo $form->error($model,'bank_credit'); ?>
    </div>
    
    <!--KhueNM-->
    <div class="row">
        <?php echo $form->labelEx($model, 'limit_date'); ?>
        <?php 
        $this->widget('zii.widgets.jui.CJuiDatePicker', array(
            'model' => $model,

            'attribute' => 'limit_date',
            'options' => array(
                'showAnim' => 'fold',
                'dateFormat'=> ActiveRecord::getDateFormatJquery(),
                'changeMonth' => true,
                'changeYear' => true,
                'showOn' => 'button',
                'buttonImage' => Yii::app()->theme->baseUrl . '/admin/images/icon_calendar_r.gif',
                'buttonImageOnly' => true,

            ),

            'htmlOptions' => array(
                'class' => 'w-16',
                'style' => 'height:20px;',
                'readonly' => 'readonly',
            ),
        ));
        ?>  
        <?php echo $form->error($model, 'limit_date'); ?>
    </div>
    
    <div class="row">
        <?php echo $form->labelEx($model,'bank_ratio'); ?>
        <?php echo $form->textField($model,'bank_ratio',array('class'=>'w-150 f_size_16 number_only ad_fix_currency','maxlength'=>5)); ?>
        <?php echo $form->error($model,'bank_ratio'); ?>
    </div>
    <div class="row">
        <?php echo $form->labelEx($model,'borrow_ration'); ?>
        <?php echo $form->textField($model,'borrow_ration',array('class'=>'w-150 f_size_16 number_only ad_fix_currency','maxlength'=>5)); ?>
        <?php echo $form->error($model,'borrow_ration'); ?>
    </div>
    <div class="row">
        <?php echo $form->labelEx($model,'interest_rate'); ?>
        <?php echo $form->textField($model,'interest_rate',array('class'=>'w-150 number_only ad_fix_currency f_size_16','maxlength'=>5)); ?>
        <?php echo $form->error($model,'interest_rate'); ?>
    </div>
    <div class="row">
        <?php echo $form->labelEx($model,'borrow_time'); ?>
        <?php echo $form->textField($model,'borrow_time',array('class'=>'w-150 number_only ad_fix_currency f_size_16','maxlength'=>5)); ?>
        <?php echo $form->error($model,'borrow_time'); ?>
    </div>
    <div class="row">
        <?php echo $form->labelEx($model,'limit_payment_guarantee'); ?>
        <?php echo $form->textField($model,'limit_payment_guarantee',array('class'=>'w-150 number_only ad_fix_currency f_size_16','maxlength'=>5)); ?>
        <?php echo $form->error($model,'limit_payment_guarantee'); ?>
    </div>
    <div class="row">
        <?php echo $form->labelEx($model,'bank_credit_type'); ?>
        <?php echo $form->dropDownList($model,'bank_credit_type', $model->getArrayCreditType(),array('class'=>' w-400', 'empty'=>'Select')); ?>
        <?php echo $form->error($model,'bank_credit_type'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model,'note'); ?>
        <?php echo $form->textArea($model,'note',array('class'=>'w-400 f_size_16','maxlength'=>200, 'cols'=>3)); ?>
        <?php echo $form->error($model,'note'); ?>
    </div>
    <div class="row">
        <?php echo $form->labelEx($model,'status'); ?>
        <?php echo $form->dropDownList($model,'status', ActiveRecord::getUserStatus(), ['class'=>'w-400',]); ?>
        <?php echo $form->error($model,'status'); ?>
    </div>
	<div class="row buttons" style="padding-left: 141px;">
            <?php $this->widget('bootstrap.widgets.TbButton', array(
                'buttonType'=>'submit',
                'label'=>$model->isNewRecord ? 'Create' :'Save',
                'type'=>'null', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
                'size'=>'small', // null, 'large', 'small' or 'mini'
                //'htmlOptions' => array('style' => 'margin-bottom: 10px; float: right;'),
            )); ?>	
        </div>

<?php $this->endWidget(); ?>

</div><!-- form -->
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/autoNumeric-master/autoNumeric.js"></script>
<script>
    $(document).ready(function(){
        $('.form').find('button:submit').click(function(){
            $.blockUI({ overlayCSS: { backgroundColor: '#fff' } }); 
        });
        fnInitInputCurrency();
    });
        /**
    * @Author: ANH DUNG Apr 12, 2017
    * @Todo: khi chọn lại đại lý thì load lại giá cho đại lý đó 
    */
    function fnSelectAsset(user_id, idField, idFieldCustomer){
        <?php if($model->isNewRecord):?>
            $(idField).closest('form').find('button:submit').click();
        <?php endif;?>
    }
</script>