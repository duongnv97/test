<?php $this->breadcrumbs=array(
    $this->pageTitle,
); ?>

<h1><?php echo $this->pageTitle;?></h1>
<div class="search-form" style="">
<?php $this->renderPartial('report/_search',array(
        'model'=>$model,
)); 
?>
</div><!-- search-form -->
<?php if( empty($aData['OUTPUT']) ) return; ?>
<?php 
$gridCss = Yii::app()->getAssetManager()->publish(Yii::getPathOfAlias('zii.widgets.assets')) . '/gridview/styles.css';
Yii::app()->getClientScript()->registerCssFile($gridCss);
//$session=Yii::app()->session;

$mAppCache  = new AppCache();
$aDay       = $aData['DATE'];
$aAgentName = $mAppCache->getAgentListdata();
$SUM_AGENT  = $aData['SUM_AGENT'];
$SUM_DAY    = $aData['SUM_DATE'];
$sumAll     = $aData['SUM_ALL'];
$OUT_PUT    = $aData['OUTPUT'];
$index      = 1;
?>
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/scroll/jquery.freezetablecolumns.1.1.js"></script>
<p>* Tổng số code sinh ra / Tổng số code đã nhập / Tổng số hủy</p>
<div class="grid-view display_none ">
        <table id="freezetablecolumns_report_call" class="items hm_table freezetablecolumns">
            <thead>
                <tr>
                    <th class="w-20 item_b">#</th>
                    <th class="w-250 item_b">Đại lý</th>
                    <th class="w-80 item_b">Tổng</th>
                    <?php foreach ($aDay as $day): ?>
                        <th class="w-50 item_b">
                            <?php echo substr($day, -2); ?>
                        </th>
                    <?php endforeach; ?>
                </tr>
            </thead>
            <tbody>
                <!--ROW SUM TREN-->
                <tr>
                    <?php 
                        $sum_all  = '';
                        $sum_all .= isset($sumAll['ALL']) ? $sumAll['ALL'] : 0; // all
                        $sum_all .= isset($sumAll['COMPLETE']) ? '/'.$sumAll['COMPLETE'] : '/0'; // complete
                        $sum_all .= isset($sumAll['CANCEL']) ? '/'.$sumAll['CANCEL'] : '/0'; // cancel
                    ?>
                    <td class="item_b item_r" colspan="2">Tổng</td>
                    <td class="item_c item_b"><?php echo $sum_all == '0/0/0' ? '' : $sum_all; ?></td>
                    <?php foreach ($aDay as $day): ?>
                        <?php 
                        $sum_date  = '';
                        $sum_date .= isset($SUM_DAY[$day]['ALL']) ? $SUM_DAY[$day]['ALL'] : 0; // all
                        $sum_date .= isset($SUM_DAY[$day]['COMPLETE']) ? '/'.$SUM_DAY[$day]['COMPLETE'] : '/0'; // complete
                        $sum_date .= isset($SUM_DAY[$day]['CANCEL']) ? '/'.$SUM_DAY[$day]['CANCEL'] : '/0'; // cancel
                        ?>
                        <td class="item_c item_b">
                            <?php echo $sum_date == '0/0/0' ? '' : $sum_date; ?>
                        </td>
                    <?php endforeach; ?>
                </tr>
                <!--END ROW SUM TREN-->
                
                <!--BODY-->
                <?php foreach ($OUT_PUT as $agent_id => $aReport): ?>
                    <?php 
                    $sum_agent  = '';
                    $sum_agent .= isset($SUM_AGENT[$agent_id]['ALL']) ? $SUM_AGENT[$agent_id]['ALL'] : 0; // all
                    $sum_agent .= isset($SUM_AGENT[$agent_id]['COMPLETE']) ? '/'.$SUM_AGENT[$agent_id]['COMPLETE'] : '/0'; // complete
                    $sum_agent .= isset($SUM_AGENT[$agent_id]['CANCEL']) ? '/'.$SUM_AGENT[$agent_id]['CANCEL'] : '/0'; // cancel
                    ?>
                <tr>
                    <td class="item_c"><?php echo $index++;?></td>
                    <td class="item_b item_l"><?php echo isset($aAgentName[$agent_id]) ? $aAgentName[$agent_id] : '';?></td>
                    <td class="item_b item_c"><?php echo $sum_agent == '0/0/0' ? '' : $sum_agent; ?></td>
                    <?php foreach ($aDay as $day): ?>
                        <?php 
                        $cell_value  = '';
                        $cell_value .= isset($aReport[$day]['ALL']) ? $aReport[$day]['ALL'] : 0; // all
                        $cell_value .= isset($aReport[$day]['COMPLETE']) ? '/'.$aReport[$day]['COMPLETE'] : '/0'; // complete
                        $cell_value .= isset($aReport[$day]['CANCEL']) ? '/'.$aReport[$day]['CANCEL'] : '/0'; // cancel
                        ?>
                        <td class="item_c">
                            <?php echo $cell_value == '0/0/0' ? '' : $cell_value; ?>
                        </td>
                    <?php endforeach; ?>
                </tr>
                <?php endforeach; ?>
                <!--END BODY-->
                
                <!--ROW SUM DUOI-->
                <tr>
                    <?php 
                        $sum_all  = '';
                        $sum_all .= isset($sumAll['ALL']) ? $sumAll['ALL'] : 0; // all
                        $sum_all .= isset($sumAll['COMPLETE']) ? '/'.$sumAll['COMPLETE'] : '/0'; // complete
                        $sum_all .= isset($sumAll['CANCEL']) ? '/'.$sumAll['CANCEL'] : '/0'; // cancel
                    ?>
                    <td class="item_b item_r" colspan="2">Tổng</td>
                    <td class="item_c item_b"><?php echo $sum_all == '0/0/0' ? '' : $sum_all; ?></td>
                    <?php foreach ($aDay as $day): ?>
                        <?php 
                        $sum_date  = '';
                        $sum_date .= isset($SUM_DAY[$day]['ALL']) ? $SUM_DAY[$day]['ALL'] : 0; // all
                        $sum_date .= isset($SUM_DAY[$day]['COMPLETE']) ? '/'.$SUM_DAY[$day]['COMPLETE'] : '/0'; // complete
                        $sum_date .= isset($SUM_DAY[$day]['CANCEL']) ? '/'.$SUM_DAY[$day]['CANCEL'] : '/0'; // cancel
                        ?>
                        <td class="item_c item_b">
                            <?php echo $sum_date == '0/0/0' ? '' : $sum_date; ?>
                        </td>
                    <?php endforeach; ?>
                </tr>
                <!--END ROW SUM DUOI-->
            </tbody>
        </table>
    </div>  
<script>
    $('#freezetablecolumns_report_call').freezeTableColumns({
        width:       <?php echo Users::WIDTH_freezetable;?>,   // required - bt 1100
        height:      400,   // required
        numFrozen: 3,     // optional
        frozenWidth: 380,   // optional
        clearWidths: true  // optional
    });
    $(window).load(function () {
        var index = 1;
        $('.freezetablecolumns').each(function () {
            if (index == 1)
                $(this).closest('div.grid-view').show();
            index++;
        });
        fnAddClassOddEven('items');
    });
</script>
