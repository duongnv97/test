<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'code-partner-form',
	'enableAjaxValidation'=>false,
        'htmlOptions' => array('enctype' => 'multipart/form-data'),
)); ?>

    <p class="note">Dòng có dấu <span class="required">*</span> là bắt buộc.</p>
    <?php echo MyFormat::BindNotifyMsg(); ?>
    
        		
        
    <div class="row">
        <?php echo $form->labelEx($model,'type'); ?>
        <?php echo $form->textField($model,'type'); ?>
        <?php echo $form->error($model,'type'); ?>
    </div>
    <div class="row">
        <?php echo $form->labelEx($model,'code_no'); ?>
        <?php echo $form->textField($model,'code_no',array('size'=>20,'maxlength'=>20)); ?>
        <?php echo $form->error($model,'code_no'); ?>
    </div>
    <div class="row">
        <?php echo $form->labelEx($model,'agent_id'); ?>
        <?php echo $form->textField($model,'agent_id'); ?>
        <?php echo $form->error($model,'agent_id'); ?>
    </div>
    <div class="row">
        <?php echo $form->labelEx($model,'sell_id'); ?>
        <?php echo $form->textField($model,'sell_id'); ?>
        <?php echo $form->error($model,'sell_id'); ?>
    </div>
    <div class="row">
        <?php echo $form->labelEx($model,'customer_id'); ?>
        <?php echo $form->textField($model,'customer_id'); ?>
        <?php echo $form->error($model,'customer_id'); ?>
    </div>
    <div class="row">
        <?php echo $form->labelEx($model,'status'); ?>
        <?php echo $form->textField($model,'status'); ?>
        <?php echo $form->error($model,'status'); ?>
    </div>
    <div class="row">
        <?php echo $form->labelEx($model,'apply_date_bigint'); ?>
        <?php echo $form->textField($model,'apply_date_bigint',array('size'=>12,'maxlength'=>12)); ?>
        <?php echo $form->error($model,'apply_date_bigint'); ?>
    </div>
    <div class="row">
        <?php echo $form->labelEx($model,'apply_by_user'); ?>
        <?php echo $form->textField($model,'apply_by_user'); ?>
        <?php echo $form->error($model,'apply_by_user'); ?>
    </div>
    <div class="row">
        <?php echo $form->labelEx($model,'created_date'); ?>
        <?php echo $form->textField($model,'created_date'); ?>
        <?php echo $form->error($model,'created_date'); ?>
    </div>
    <div class="row">
        <?php echo $form->labelEx($model,'created_date_bigint'); ?>
        <?php echo $form->textField($model,'created_date_bigint',array('size'=>12,'maxlength'=>12)); ?>
        <?php echo $form->error($model,'created_date_bigint'); ?>
    </div>
    <div class="row">
        <?php echo $form->labelEx($model,'sms_phone'); ?>
        <?php echo $form->textField($model,'sms_phone'); ?>
        <?php echo $form->error($model,'sms_phone'); ?>
    </div>
    <div class="row">
        <?php echo $form->labelEx($model,'json'); ?>
        <?php echo $form->textArea($model,'json',array('rows'=>6, 'cols'=>50)); ?>
        <?php echo $form->error($model,'json'); ?>
    </div>
	<div class="row buttons" style="padding-left: 141px;">
		        <?php $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType'=>'submit',
            'label'=>$model->isNewRecord ? 'Create' :'Save',
            'type'=>'null', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
            'size'=>'small', // null, 'large', 'small' or 'mini'
            //'htmlOptions' => array('style' => 'margin-bottom: 10px; float: right;'),
        )); ?>	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->
<script>
    $(document).ready(function(){
        $('.form').find('button:submit').click(function(){
            $.blockUI({ overlayCSS: { backgroundColor: '#fff' } }); 
        });
        
    });
</script>