<?php
$this->breadcrumbs=array(
    $this->pluralTitle => array('index'),
    ' ' . $this->singleTitle . ' : ',
);

$menus = array(
	array('label'=>"$this->pluralTitle Management", 'url'=>array('index')),
	array('label'=>"Tạo Mới $this->singleTitle", 'url'=>array('create')),
	array('label'=>"Cập Nhật $this->singleTitle", 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>"Xóa $this->singleTitle", 'url'=>array('delete'), 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Bạn chắc chắn muốn xóa ?')),
);
$this->menu= ControllerActionsName::createMenusRoles($menus, $actions);
?>

<h1>Xem: <?php echo $this->singleTitle; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		array(
                    'name'=>'type',
                    'type'=>'raw',
                    'value'=>$model->getType($model->type),
                ),
		array(
                    'name'=>'agent_id',
                    'type'=>'raw',
                    'value'=>$model->getAgent($model->agent_id),
                ),
		array(
                    'name'=>'code_no',
                    'type'=>'raw',
                    'value'=>$model->getCodeNo($model->code_no),
                ),
                array(
                    'name'=>'status',
                    'type'=>'raw',
                    'value'=>$model->getStatus($model->status),
                ),
                 array(
                    'name'=>'apply_date_bigint',
                    'type'=>'raw',
                    'value'=>$model->getApplyDate($model->apply_date_bigint),
                ),
		array(
                    'name'=>'customer_id',
                    'type'=>'raw',
                    'value'=>$model->getCustomer($model->customer_id),
                ),
		array(
                    'name'=>'sms_phone',
                    'type'=>'raw',
                    'value'=>$model->getSmsPhone($model->sms_phone),
                ),
		array(
                    'name'=>'apply_by_user',
                    'type'=>'raw',
                    'value'=>$model->getApplyByUser($model->apply_by_user),
                ),
		array(
                    'name'=>'created_date',
                    'type'=>'raw',
                    'value'=>$model->getCreatedDate($model->created_date),
                ),
		
		
	),
)); ?>
