<?php
$this->breadcrumbs=array(
	$this->singleTitle,
);

$menus=array(
            array('label'=>"Create $this->singleTitle", 'url'=>array('create')),
);
$this->menu= ControllerActionsName::createMenusRoles($menus, $actions);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('code-partner-grid', {
                url : $(this).attr('action'),
		data: $(this).serialize()
	});
	return false;
});
");

Yii::app()->clientScript->registerScript('ajaxupdate', "
$('#code-partner-grid a.ajaxupdate').live('click', function() {
    $.fn.yiiGridView.update('code-partner-grid', {
        type: 'POST',
        url: $(this).attr('href'),
        success: function() {
            $.fn.yiiGridView.update('code-partner-grid');
        }
    });
    return false;
});
");
?>

<h1>Danh Sách <?php echo $this->pluralTitle; ?></h1>


<?php echo CHtml::link('Tìm Kiếm Nâng Cao','#',array('class'=>'search-button')); ?>
<div class="search-form display_none" >
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'code-partner-grid',
	'dataProvider'=>$model->search(),
        'afterAjaxUpdate'=>'function(id, data){ fnUpdateColorbox();}',
        'template'=>'{pager}{summary}{items}{pager}{summary}',
//    'itemsCssClass' => 'items custom_here',
//    'htmlOptions'=> array('class'=>'grid-view custom_here'),
        'pager' => array(
            'maxButtonCount'=>  CmsFormatter::$PAGE_MAX_BUTTON,
        ),
	'enableSorting' => false,
	//'filter'=>$model,
	'columns'=>array(
        array(
            'header' => 'S/N',
            'type' => 'raw',
            'value' => '$this->grid->dataProvider->pagination->currentPage * $this->grid->dataProvider->pagination->pageSize + ($row+1)',
            'headerHtmlOptions' => array('width' => '10px','style' => 'text-align:center;'),
            'htmlOptions' => array('style' => 'text-align:center;')
        ),
                array(
                    'name' => 'type',
                    'type' => 'html',
                    'value' => '$data->getType()',
                    'htmlOptions' => array('style' => 'text-align:center;'),
                ),  
                array(
                    'name' => 'agent_id',
                    'type' => 'html',
                    'value' => '$data->getAgent()',
//                    'htmlOptions' => array('style' => 'text-align:center;'),
                ),  
                array(
                    'name' => 'code_no',
                    'type' => 'html',
                    'value' => '$data->getCodeNo()',
                    'htmlOptions' => array('style' => 'text-align:center;'),
                ),   
                array(
                    'name' => 'reward_point',
                    'type' => 'html',
                    'value' => '$data->getPoint()',
                    'htmlOptions' => array('style' => 'text-align:right;'),
                ),   
                array(
                    'name' => 'status',
                    'type' => 'html',
                    'value' => '$data->getStatus()',
                    'htmlOptions' => array('style' => 'text-align:center;'),
                ),
                array(
                    'name' => 'customer_id',
                    'type' => 'html',
                    'value' => '$data->getCustomer()',
//                    'htmlOptions' => array('style' => 'text-align:center;'),
                ),   
                array(
                    'name' => 'sms_phone',
                    'type' => 'html',
                    'value' => '$data->getSmsPhone()',
//                    'htmlOptions' => array('style' => 'text-align:center;'),
                ),
                array(
                    'name' => 'apply_date_bigint',
                    'type' => 'html',
                    'value' => '$data->getApplyDate()',
                    'htmlOptions' => array('style' => 'text-align:center;'),
                ),
                array(
                    'name' => 'apply_by_user',
                    'type' => 'html',
                    'value' => '$data->getApplyByUser()',
//                    'htmlOptions' => array('style' => 'text-align:center;'),
                ),   
                array(
                    'name' => 'created_date',
                    'type' => 'html',
                    'value' => '$data->getCreatedDate()',
                    'htmlOptions' => array('style' => 'text-align:center;'),
                ),   
//		array(
//                    'header' => 'Actions',
//                    'class'=>'CButtonColumn',
//                    'template'=> ControllerActionsName::createIndexButtonRoles($actions),
//                    'buttons'=>array(
//                        'update' => array(
//                            'visible' => '$data->canUpdate()',
//                        ),
//                        'delete'=>array(
//                            'visible'=> 'GasCheck::canDeleteData($data)',
//                        ),
//                    ),
//		),
            
	),
)); ?>

<script>
$(document).ready(function() {
    fnUpdateColorbox();
});

function fnUpdateColorbox(){   
    fixTargetBlank();
}
</script>