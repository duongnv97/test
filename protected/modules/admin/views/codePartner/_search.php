<?php 
    $urlExcel   = Yii::app()->createAbsoluteUrl('admin/codePartner/index', ['to_excel'=>1]);
?>
<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=> GasCheck::getCurl(),
	'method'=>'get',
)); ?>
        <div class="row more_col">
            <div class="col1">
                <?php echo $form->label($model,'type',array()); ?>
                <?php echo $form->dropDownList($model,'type', $model->getArrayType(),array('class'=>'w-200','empty'=>'Select')); ?>
            </div>

            <div class="col2">
                    <?php echo $form->label($model,'code_no',array()); ?>
                    <?php echo $form->textField($model,'code_no',array('size'=>20,'maxlength'=>20)); ?>
            </div>
        </div>
    
        <div class="row more_col">
            <div class="col1">
                    <?php echo $form->label($model,'status',array()); ?>
                    <?php echo $form->dropDownList($model,'status', $model->getArrayStatus(),array('class'=>'w-200','empty'=>'Select')); ?>
            </div>
            <div class="col2">
                    <?php echo $form->label($model,'sms_phone',array()); ?>
                    <?php echo $form->textField($model,'sms_phone'); ?>
            </div>
        </div>
        
	<div class="row">
        <?php echo $form->labelEx($model,'agent_id'); ?>
        <?php echo $form->hiddenField($model,'agent_id', array('class'=>'')); ?>
        <?php
            // 1. limit search kh của sale
            $url = Yii::app()->createAbsoluteUrl('admin/ajax/search_by_role', array('role'=> ROLE_AGENT));
            // widget auto complete search user customer and supplier
            $aData = array(
                'model'=>$model,
                'field_customer_id'=>'agent_id',
                'url'=> $url,
                'name_relation_user'=>'rAgent',
                'ClassAdd' => 'w-400',
                'field_autocomplete_name' => 'autocomplete_name_1',
                'placeholder'=>'Nhập mã hoặc tên đại lý',
            );
            $this->widget('ext.SpaAutocompleteCustomer.SpaAutocompleteCustomer',
                array('data'=>$aData));
            ?>
        <?php echo $form->error($model,'agent_id'); ?>
	</div>

	<div class="row">
        <?php echo $form->labelEx($model,'customer_id'); ?>
        <?php echo $form->hiddenField($model,'customer_id', array('class'=>'')); ?>
        <?php
                // 1. limit search kh của sale
                $url = Yii::app()->createAbsoluteUrl('admin/ajax/searchHgd');
                // widget auto complete search user customer and supplier
                $aData = array(
                    'model'=>$model,
                    'field_customer_id'=>'customer_id',
                    'url'=> $url,
                    'name_relation_user'=>'rCustomer',
                    'ClassAdd' => 'w-400',
    //                'fnSelectCustomer'=>'fnSelectCustomer',
                );
                $this->widget('ext.SpaAutocompleteCustomer.SpaAutocompleteCustomer',
                    array('data'=>$aData));
                ?>
            <?php echo $form->error($model,'customer_id'); ?>
    </div>

	<div class="row">
        <?php echo $form->labelEx($model,'apply_by_user'); ?>
        <?php echo $form->hiddenField($model,'apply_by_user', array('class'=>'')); ?>
        <?php
            // 1. limit search kh của sale
            $url = Yii::app()->createAbsoluteUrl('admin/ajax/search_by_role', array('role'=> ROLE_EMPLOYEE_MAINTAIN));
            // widget auto complete search user customer and supplier
            $aData = array(
                'model'=>$model,
                'field_customer_id'=>'apply_by_user',
                'url'=> $url,
                'name_relation_user'=>'rApply',
                'ClassAdd' => 'w-400',
                'field_autocomplete_name' => 'autocomplete_name_4',
                'placeholder'=>'Nhập mã NV hoặc tên',
            );
            $this->widget('ext.SpaAutocompleteCustomer.SpaAutocompleteCustomer',
                array('data'=>$aData));
        ?>
	</div>

	<div class="row more_col">
            <div class="col1">
                    <?php echo $form->label($model,'created_date'); ?>
                    <?php 
                        $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                            'model'=>$model,        
                            'attribute'=>'date_from',
                            'options'=>array(
                                'showAnim'=>'fold',
                                'dateFormat'=> MyFormat::$dateFormatSearch,
    //                            'minDate'=> '0',
                                'maxDate'=> '0',
                                'changeMonth' => true,
                                'changeYear' => true,
                                'showOn' => 'button',
                                'buttonImage'=> Yii::app()->theme->baseUrl.'/admin/images/icon_calendar_r.gif',
                                'buttonImageOnly'=> true,                                
                            ),        
                            'htmlOptions'=>array(
                                'class'=>'w-180 date_from',
                                'style'=>'float:left;',                               
                            ),
                        ));
                    ?>     		
            </div>
            <div class="col2">
                    <?php echo $form->label($model,'date_to'); ?>
                    <?php 
                        $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                            'model'=>$model,        
                            'attribute'=>'date_to',
                            'options'=>array(
                                'showAnim'=>'fold',
                                'dateFormat'=> MyFormat::$dateFormatSearch,
    //                            'minDate'=> '0',
                                'maxDate'=> '0',
                                'changeMonth' => true,
                                'changeYear' => true,
                                'showOn' => 'button',
                                'buttonImage'=> Yii::app()->theme->baseUrl.'/admin/images/icon_calendar_r.gif',
                                'buttonImageOnly'=> true,                                
                            ),        
                            'htmlOptions'=>array(
                                'class'=>'w-180',
                                'style'=>'float:left;',
                            ),
                        ));
                    ?>     		
            </div>
        </div>
    
	<div class="row more_col">
            <div class="col1">
                    <?php echo $form->label($model,'apply_date'); ?>
                    <?php 
                        $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                            'model'=>$model,        
                            'attribute'=>'date_from_apply',
                            'options'=>array(
                                'showAnim'=>'fold',
                                'dateFormat'=> MyFormat::$dateFormatSearch,
    //                            'minDate'=> '0',
                                'maxDate'=> '0',
                                'changeMonth' => true,
                                'changeYear' => true,
                                'showOn' => 'button',
                                'buttonImage'=> Yii::app()->theme->baseUrl.'/admin/images/icon_calendar_r.gif',
                                'buttonImageOnly'=> true,                                
                            ),        
                            'htmlOptions'=>array(
                                'class'=>'w-180 date_from',
                                'style'=>'float:left;',                               
                            ),
                        ));
                    ?>     		
            </div>
            <div class="col2">
                    <?php echo $form->label($model,'date_to_apply'); ?>
                    <?php 
                        $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                            'model'=>$model,        
                            'attribute'=>'date_to_apply',
                            'options'=>array(
                                'showAnim'=>'fold',
                                'dateFormat'=> MyFormat::$dateFormatSearch,
    //                            'minDate'=> '0',
                                'maxDate'=> '0',
                                'changeMonth' => true,
                                'changeYear' => true,
                                'showOn' => 'button',
                                'buttonImage'=> Yii::app()->theme->baseUrl.'/admin/images/icon_calendar_r.gif',
                                'buttonImageOnly'=> true,                                
                            ),        
                            'htmlOptions'=>array(
                                'class'=>'w-180',
                                'style'=>'float:left;',
                            ),
                        ));
                    ?>     		
            </div>
        </div>

	

	<div class="row buttons" style="padding-left: 159px;">
		        <?php $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType'=>'submit',
            'label'=>'Search',
            'type'=>'null', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
            'size'=>'small', // null, 'large', 'small' or 'mini'
            //'htmlOptions' => array('style' => 'margin-bottom: 10px; float: right;'),
        )); ?>	
         &nbsp;&nbsp;&nbsp;&nbsp;<a class='btn_cancel' href='<?php echo $urlExcel;?>'>Xuất Excel</a>
        </div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->