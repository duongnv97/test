<div class="form">
    <style>
        .remain_label_small label { width: 70px !important; }
    </style>
    
<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'gas-remain-export-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Dòng có dấu <span class="required">*</span> là bắt buộc.</p>
        
        <?php  if(Yii::app()->user->hasFlash('successUpdate')): ?>
            <div class='success_div'><?php echo Yii::app()->user->getFlash('successUpdate');?></div>         
        <?php endif; ?>  	
            	
	<div class="row">
            <?php echo $form->labelEx($model,'parent_id'); ?>
            <?php echo $form->textField($model,'parent_id',array('size'=>11,'maxlength'=>11)); ?>
            <?php echo $form->error($model,'parent_id'); ?>
	</div>
            
        <div class="row remain_label_small">
            <fieldset>
                <legend class="item_b hight_light">Dành Cho Cân Lần 1 - Chọn Khoảng Ngày Nhập Gas Dư</legend>
                <div class="row more_col">
                    <div class="col1">
                            <?php echo Yii::t('translation', $form->label($model,'date_from')); ?>
                            <?php 
                                $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                                    'model'=>$model,        
                                    'attribute'=>'date_from',
                                    'options'=>array(
                                        'showAnim'=>'fold',
                                        'dateFormat'=> MyFormat::$dateFormatSearch,
            //                            'minDate'=> '0',
                                        'maxDate'=> '0',
                                        'changeMonth' => true,
                                        'changeYear' => true,
                                        'showOn' => 'button',
                                        'buttonImage'=> Yii::app()->theme->baseUrl.'/admin/images/icon_calendar_r.gif',
                                        'buttonImageOnly'=> true,                                
                                    ),        
                                    'htmlOptions'=>array(
                                        'class'=>'w-16',
                                        'size'=>'16',
                                        'readonly'=>1,
                                        'style'=>'float:left;',                               
                                    ),
                                ));
                            ?>     		
                    </div>
                    <div class="col2">
                            <?php echo Yii::t('translation', $form->label($model,'date_to')); ?>
                            <?php 
                                $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                                    'model'=>$model,        
                                    'attribute'=>'date_to',
                                    'options'=>array(
                                        'showAnim'=>'fold',
                                        'dateFormat'=> MyFormat::$dateFormatSearch,
            //                            'minDate'=> '0',
                                        'maxDate'=> '0',
                                        'changeMonth' => true,
                                        'changeYear' => true,
                                        'showOn' => 'button',
                                        'buttonImage'=> Yii::app()->theme->baseUrl.'/admin/images/icon_calendar_r.gif',
                                        'buttonImageOnly'=> true,                                
                                    ),        
                                    'htmlOptions'=>array(
                                        'class'=>'w-16',
                                        'size'=>'16',
                                        'readonly'=>1,
                                        'style'=>'float:left;',
                                    ),
                                ));
                            ?>     		
                    </div>
                </div>
                <div class="row l_padding_300">
                    <input class="SearchDateRange" type="button" value="Search">
                </div>
                
            </fieldset>            
        </div>
            
        <div class="row">
            <table class="materials_table materials_table_root materials_table_th" style="width: 100%;">
                <thead>
                    <tr>
                        <th class="item_c w-10" rowspan="2">STT</th>
                        <th class="w-200 item_c" rowspan="2">Tên Hàng</th>
                        <th class="w-80 item_c" rowspan="2">Số Seri</th>
                        <th class="w-80 item_c" rowspan="2">Trọng Lượng Vỏ Bình(kg)</th>
                        <th class="w-80 item_c" rowspan="2">Trọng Lượng Vỏ Bình + Gas(kg)</th>
                        <th class=" item_c" colspan="4">Trọng Lượng Gas Dư</th>
                        <th class="w-150 item_c last" rowspan="2">Ghi Chú</th>
                    </tr>
                    <tr>
                        <th class="w-80 item_c">Khách Hàng Trả</th>
                        <th class="w-80 item_c">Đại Lý Nhận</th>
                        <th class="w-80 item_c">Lái Xe Nhận</th>
                        <th class="w-80 item_c">Trạm Nhận</th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>
            
         
            
            aModelRemain
            
            
            
	
	<div class="row buttons" style="padding-left: 141px;">
		        <?php $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType'=>'submit',
            'label'=>$model->isNewRecord ? 'Create' :'Save',
            'type'=>'null', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
            'size'=>'small', // null, 'large', 'small' or 'mini'
            //'htmlOptions' => array('style' => 'margin-bottom: 10px; float: right;'),
        )); ?>	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->
<script>
    $(document).ready(function(){
        $('.form').find('button:submit').click(function(){
            $.blockUI({ overlayCSS: { backgroundColor: '#fff' } }); 
        });
        fnSearchDateRange();
        
    });
    
    function fnSearchDateRange(){
        $('.SearchDateRange').click(function(){
            if($('#GasRemainExport_date_from').val()=='' || $('#GasRemainExport_date_to').val()==''){
                alert('Chưa Chọn Khoảng Ngày');
                return;
            }
            
            
        });
        
        
    }
    
    
</script>