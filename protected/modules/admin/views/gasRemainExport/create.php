<?php
$this->breadcrumbs=array(
	'Quản Lý Phiếu Giao Nhận Gas Dư'=>array('index'),
	'Tạo Mới',
);

$menus = array(		
        array('label'=>'GasRemainExport Management', 'url'=>array('index')),
);
$this->menu= ControllerActionsName::createMenusRoles($menus, $actions);
?>

<h1>Tạo Mới Phiếu Giao Nhận Gas Dư</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>