<div class="row remain_label_small <?php echo $display_none;?>">
    <fieldset>
        <legend class="item_b hight_light">Dành Cho Những Lần Cân Tiếp Theo - Nhập Những Chữ Số Cuối Của Mã Phiếu Cân Lần 1</legend>
        <div class="row">
            <?php echo $form->hiddenField($model,'parent_id'); ?>
            <?php
//                $className = get_class($model);
                $this->widget('zii.widgets.jui.CJuiAutoComplete', array(
                        'attribute'=>'autocomplete_name',
                        'model'=>$model,
                        'sourceUrl'=>Yii::app()->createAbsoluteUrl('admin/ajax/search_remain_export_code'),
                        'options'=>array(
                                'minLength'=>MIN_LENGTH_AUTOCOMPLETE,
                                'multiple'=> true,
                        'search'=>"js:function( event, ui ) {
                                $('#GasRemainExport_autocomplete_name').addClass('grid-view-loading-gas');
                                } ",
                        'response'=>"js:function( event, ui ) {
                                var json = $.map(ui, function (value, key) { return value; });
                                if(json.length<1){
                                    var error = '<div class=\'errorMessage clr autocomplete_name_text\'>Không tìm thấy dữ liệu.</div>';
                                    if($('.autocomplete_name_text').size()<1)
                                        $('.autocomplete_name_error').after(error);
                                    else
                                        $('.autocomplete_name_error').parent('div').find('.autocomplete_name_text').show();                                    
                                }
                                $('#GasRemainExport_autocomplete_name').removeClass('grid-view-loading-gas');
                                } ",

                            'select'=>"js:function(event, ui) {
                                fnSelectItem(event, ui);
                            }",
                        ),
                        'htmlOptions'=>array(
                            'class'=>'autocomplete_name_error w-350',
                            'maxlength'=>50,
                            'style'=>'float:left;',
                            'placeholder'=>'Nhập số cuối mã phiếu lần 1',
                        ),
                )); 
                ?>   
            
        </div>
        
    </fieldset>            
</div>