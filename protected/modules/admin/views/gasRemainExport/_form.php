<div class="form">
    <style>
        .remain_label_small label { width: 70px !important; }
        .form .autocomplete_name_text { padding-left: 0; }
    </style>
    
<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'gas-remain-export-form',
	'enableAjaxValidation'=>false,
)); 
    $cmsFormater = new CmsFormatter();
    $display_none = "display_none";
    if($model->isNewRecord){
        $display_none = "";
    }
    
?>
        <?php echo $form->errorSummary($model); ?>
	<p class="note">Dòng có dấu <span class="required">*</span> là bắt buộc.</p>
        
        <?php  if(Yii::app()->user->hasFlash('successUpdate')): ?>
            <div class='success_div'><?php echo Yii::app()->user->getFlash('successUpdate');?></div>         
        <?php endif; ?>  	
            	
	<div class="row">
            <?php echo $form->labelEx($model,'type'); ?>
            <?php  echo $form->dropDownList($model,'type', GasRemainExport::$KG_ARR_TYPE,array('empty'=>'Select')); ?>
            <?php echo $form->error($model,'type'); ?>
	</div>
            
        <div class="row remain_label_small <?php echo $display_none;?>">
            <fieldset>
                <legend class="item_b hight_light">Dành Cho Cân Lần 1 - Chọn Khoảng Ngày Nhập Gas Dư</legend>
                <div class="row more_col">
                    <div class="col1">
                            <?php echo Yii::t('translation', $form->label($model,'date_from')); ?>
                            <?php 
                                $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                                    'model'=>$model,        
                                    'attribute'=>'date_from',
                                    'options'=>array(
                                        'showAnim'=>'fold',
                                        'dateFormat'=> MyFormat::$dateFormatSearch,
            //                            'minDate'=> '0',
                                        'maxDate'=> '0',
                                        'changeMonth' => true,
                                        'changeYear' => true,
                                        'showOn' => 'button',
                                        'buttonImage'=> Yii::app()->theme->baseUrl.'/admin/images/icon_calendar_r.gif',
                                        'buttonImageOnly'=> true,                                
                                    ),        
                                    'htmlOptions'=>array(
                                        'class'=>'w-16',
                                        'size'=>'16',
                                        'readonly'=>1,
                                        'style'=>'float:left;',                               
                                    ),
                                ));
                            ?>     		
                    </div>
                    <div class="col2">
                            <?php echo Yii::t('translation', $form->label($model,'date_to')); ?>
                            <?php 
                                $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                                    'model'=>$model,        
                                    'attribute'=>'date_to',
                                    'options'=>array(
                                        'showAnim'=>'fold',
                                        'dateFormat'=> MyFormat::$dateFormatSearch,
            //                            'minDate'=> '0',
                                        'maxDate'=> '0',
                                        'changeMonth' => true,
                                        'changeYear' => true,
                                        'showOn' => 'button',
                                        'buttonImage'=> Yii::app()->theme->baseUrl.'/admin/images/icon_calendar_r.gif',
                                        'buttonImageOnly'=> true,                                
                                    ),        
                                    'htmlOptions'=>array(
                                        'class'=>'w-16',
                                        'size'=>'16',
                                        'readonly'=>1,
                                        'style'=>'float:left;',
                                    ),
                                ));
                            ?>     		
                    </div>
                </div>
                <div class="row l_padding_300">
                    <input class="SearchDateRange" type="button" value="Search">
                </div>
                
            </fieldset>            
        </div>
            
        <?php include '_form_auto.php';?>
            
        <div class="row ajax_box ">
        <?php if(is_array($model->aModelRemain) && count($model->aModelRemain)):?>        
            <table class="materials_table materials_table_root materials_table_th hm_table" style="">
                <thead>
                    <tr>
                        <th class="item_c w-10">STT</th>
                        <th class="w-200 item_c">Khách Hàng</th>
                        <th class="w-200 item_c">Tên Vật Tư</th>
                        <th class="w-80 item_c">Số Seri</th>
                        <th class="w-80 item_c">Trọng Lượng Vỏ Bình(kg)</th>
                        <th class="w-80 item_c">Trọng Lượng Vỏ Bình + Gas(kg)</th>
                        <th class=" item_c">Trọng Lượng Gas Dư</th>
                        <th class="w-200 item_c last">Ghi Chú</th>
                        <?php if(isset($_GET['date_from']) && isset($_GET['date_to'])): ?>
                        <th class="item_c last">Xóa</th>
                        <?php endif;?>
                    </tr>
                    
                </thead>
                <tbody>
                    <?php foreach ($model->aModelRemain as $key=>$item): ?>
                    <?php
                        $amount_has_gas = isset($model->primaryDetail['amount_has_gas'][$item->id])?$model->primaryDetail['amount_has_gas'][$item->id]:$item->amount_has_gas;
                        $amount_gas = isset($model->primaryDetail['amount_gas'][$item->id])?$model->primaryDetail['amount_gas'][$item->id]:$item->amount_gas;
                        $note='';
                        if(isset($_GET['id'])){
                            $note = isset($model->primaryDetail['note'][$item->id])?$model->primaryDetail['note'][$item->id]:'';                        
                        }
                    ?>
                    
                    <tr class="f_size_16">
                        <td class="order_no item_c t_padding_5"><?php echo $key+1;?></td>
                        <td class=""><?php echo $item->customer?$item->customer->first_name:'';?></td>
                        <td class=""><?php echo $item->rMaterials?$item->rMaterials->name:'';?></td>
                        <td class="item_c"><?php echo $item->seri;?></td>
                        <td class="item_c">
                            <?php echo ActiveRecord::formatCurrency($item->amount_empty);?>
                            <span class="amount_empty display_none"><?php echo ActiveRecord::formatCurrency($item->amount_empty);?></span>
                            <input class="" name="gas_remain_id[]" value="<?php echo $item->id;?>" type="hidden">
                        </td>
                        <td class="item_c">
                            <input class="change_amount_row amount_has_gas number_only_v1" name="amount_has_gas[]" value="<?php echo ActiveRecord::formatNumberInput($amount_has_gas) ;?>" size="8" maxlength="6">
                        </td>
                        <td class="item_c amount_gas">
                            <?php echo ActiveRecord::formatCurrency($amount_gas);?>
                        </td>
                        <td class="item_c">
                            <input class="w-180" name="note[]" value="<?php echo $note;?>" maxlength="500">
                        </td>
                        <?php if(isset($_GET['date_from']) && isset($_GET['date_to'])): ?>
                        <td class="item_c last"><span remove="<?php echo $item->id;?>" class="remove_icon_only"></span></td>
                        <?php endif;?>
                    </tr>
                    <?php endforeach;?>
                </tbody>
            </table>
       <?php endif;?>  
       </div><!-- end ajax_box-->
            
            
	<div class="row">
            <?php echo $form->labelEx($model,'note'); ?>
            <?php echo $form->textArea($model,'note',array('rows'=>5,'cols'=>80,"maxlength"=>500)); ?>
            <?php echo $form->error($model,'note'); ?>
	</div>	
	<div class="row buttons" style="padding-left: 141px;">
		        <?php $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType'=>'submit',
            'label'=>$model->isNewRecord ? 'Create' :'Save',
            'type'=>'null', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
            'size'=>'small', // null, 'large', 'small' or 'mini'
            //'htmlOptions' => array('style' => 'margin-bottom: 10px; float: right;'),
        )); ?>	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->

<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/jquery.floatThead.min.js"></script>    
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/underscore-min.js"></script>    
<script>
    $(document).ready(function(){
        $('.form').find('button:submit').click(function(){
            $.blockUI({ overlayCSS: { backgroundColor: '#fff' } }); 
        });
        fnSearchDateRange();
        bindChangeAmount();
        addIconRemove();
        $('.materials_table').floatThead();
        fnBindRemoveIcon();
    });
    
    function bindChangeAmount(){
        $('.amount_has_gas').live('keyup', function(){
            var tr = $(this).closest('tr');
            var amount_empty = tr.find('.amount_empty').text()*1;
            var amount_has_gas = $(this).val()*1;
            var  amount = parseFloat(amount_has_gas-amount_empty);
                amount = Math.round(amount * 100) / 100;
            tr.find('.amount_gas').text(commaSeparateNumber(amount));
        });        
    }
    
    
    function fnSearchDateRange(){
        $('.SearchDateRange').click(function(){
            if($('#GasRemainExport_date_from').val()=='' || $('#GasRemainExport_date_to').val()==''){
                alert('Chưa Chọn Khoảng Ngày');
                return;
            }
            var date_from = $('#GasRemainExport_date_from').val();
            var date_to = $('#GasRemainExport_date_to').val();
            fnRemoveName();
            
            $.blockUI({ overlayCSS: { backgroundColor: '#fff' } }); 
            var url_ = '<?php echo Yii::app()->createAbsoluteUrl('admin/gasRemainExport/create');?>';
            $.ajax({
                url:url_,
                data:{date_from:date_from, date_to:date_to},
                success:function(data){
                    fnAfterAjax(data);
                    $.unblockUI();
                }
            });
            
        });
    }
    
    function fnAfterAjax(data){
        $('.ajax_box').html($(data).find('.ajax_box').html());
        $('.materials_table').floatThead();
        validateNumber();
    }
    
    function addIconRemove(){
        var remove_div = '<span class=\'remove_row_item\' onclick=\'fnRemoveName(this)\'></span>';
        var error = '<div class=\'errorMessage clr autocomplete_name_text display_none\'>Không tìm thấy dữ liệu.</div>';
        $('#GasRemainExport_autocomplete_name').after(error);
        $('#GasRemainExport_autocomplete_name').after(remove_div);
        
        
//        $('#GasRemain_autocomplete_name').attr('readonly',true).after(remove_div);
    }
    
    function fnRemoveName(){
        $('#GasRemainExport_parent_id').val('');
        $('.ajax_box').html('');
        $('#GasRemainExport_autocomplete_name').attr('readonly', false).val('');
    }
    
    function fnAjaxSelectItem(event, ui){
        var parent_id = ui.item.id;
        $.blockUI({ overlayCSS: { backgroundColor: '#fff' } }); 
        var url_ = '<?php echo Yii::app()->createAbsoluteUrl('admin/gasRemainExport/create');?>';
        $.ajax({
            url:url_,
            data:{parent_id:parent_id, ajax_parent:1},
            success:function(data){
                fnAfterAjax(data);
                $.unblockUI();
            }
        });        
    }
    
    function fnSelectItem(event, ui){
        $('#GasRemainExport_parent_id').val(ui.item.id);
        $('#GasRemainExport_autocomplete_name').attr('readonly',true);
        $('#GasRemainExport_date_from').val('');
        $('#GasRemainExport_date_to').val('');
        $('.autocomplete_name_error').parent('div').find('.autocomplete_name_text').hide();
        fnAjaxSelectItem(event, ui);
    }
    
    
</script>