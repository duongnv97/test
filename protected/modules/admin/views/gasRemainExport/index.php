<?php
$this->breadcrumbs=array(
	'Quản Lý Phiếu Giao Nhận Gas Dư',
);

$menus=array(
            array('label'=>'Create Giao Nhận Gas Dư', 'url'=>array('create')),
);
$this->menu= ControllerActionsName::createMenusRoles($menus, $actions);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('gas-remain-export-grid', {
                url : $(this).attr('action'),
		data: $(this).serialize()
	});
	return false;
});
");

Yii::app()->clientScript->registerScript('ajaxupdate', "
$('#gas-remain-export-grid a.ajaxupdate').live('click', function() {
    $.fn.yiiGridView.update('gas-remain-export-grid', {
        type: 'POST',
        url: $(this).attr('href'),
        success: function() {
            $.fn.yiiGridView.update('gas-remain-export-grid');
        }
    });
    return false;
});
");
?>

<h1>Quản Lý Phiếu Giao Nhận Gas Dư</h1>

<?php echo CHtml::link('Tìm Kiếm Nâng Cao','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'gas-remain-export-grid',
	'dataProvider'=>$model->search(),
        'afterAjaxUpdate'=>'function(id, data){ fnUpdateColorbox();}',
        'template'=>'{pager}{summary}{items}{pager}{summary}', 
        'pager' => array(
            'maxButtonCount'=>  CmsFormatter::$PAGE_MAX_BUTTON,
        ),            
	'columns'=>array(
        array(
            'header' => 'S/N',
            'type' => 'raw',
            'value' => '$this->grid->dataProvider->pagination->currentPage * $this->grid->dataProvider->pagination->pageSize + ($row+1)',
            'headerHtmlOptions' => array('width' => '10px','style' => 'text-align:center;'),
            'htmlOptions' => array('style' => 'text-align:center;')
        ),
		
                array(
                    'name'=>'code_no',
                    'htmlOptions' => array('style' => 'width:120px;')
                ),
                array(
                    'name'=>'agent_id',
                    'value'=>'$data->rAgent?$data->rAgent->first_name:""',
                    'htmlOptions' => array('style' => 'width:120px;')
                ),
                
                array(
                    'name'=>'type',
                    'value'=>'GasRemainExport::$KG_ARR_TYPE[$data->type]',
                    'htmlOptions' => array('style' => 'text-align:center;width:110px;')
                ),
                array(
                    'name'=>'parent_id',
                    'value'=>'$data->rParent?$data->rParent->code_no:""',
                    'htmlOptions' => array('style' => 'width:110px;')
                ),            
                array(
                    'name'=>'serial',
                    'htmlOptions' => array('style' => 'text-align:center;width:110px;')
                ),
                array(
                    'name' => 'note',
                    'type' => 'html',
                    'value' => 'nl2br($data->note)',
                ),            
                array(
                    'name' => 'created_date',
                    'type' => 'Datetime',
                    'htmlOptions' => array('style' => 'text-align:center;width:90px;')
                ),            
		array(
                    'header' => 'Actions',
                    'class'=>'CButtonColumn',
                    'template'=> ControllerActionsName::createIndexButtonRoles($actions),
                    'buttons'=>array(
                        'update'=>array(
                            'visible'=> 'GasCheck::AgentCanUpdateRemainExport($data)',
                        ),
                        'delete'=>array(
                            'visible'=> 'GasCheck::canDeleteData($data) && GasRemainExport::canDeleteRecordFirst($data)',
                        ),
                    ),
		),
	),
)); ?>

<script>
$(document).ready(function() {
    fnUpdateColorbox();
});

function fnUpdateColorbox(){   
    fixTargetBlank();
}
</script>