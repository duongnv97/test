<?php
$this->breadcrumbs=array(
	'Quản Lý Phiếu Giao Nhận Gas Dư'=>array('index'),
	$model->code_no=>array('view','id'=>$model->id),
	'Cập Nhật',
);

$menus = array(	
        array('label'=>'Quản Lý Phiếu Giao Nhận Gas Dư', 'url'=>array('index')),
	array('label'=>'Xem Phiếu Giao Nhận Gas Dư', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Tạo Phiếu Mới Giao Nhận Gas Dư', 'url'=>array('create')),	
);
$this->menu= ControllerActionsName::createMenusRoles($menus, $actions);

?>

<h1>Cập Nhật Phiếu Giao Nhận Gas Dư: <?php echo $model->code_no; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>