<?php
$this->breadcrumbs=array(
	'Quản Lý Giao Nhận Gas Dư'=>array('index'),
	$model->code_no,
);

$menus = array(
	array('label'=>'Quản Lý Giao Nhận Gas Dư ', 'url'=>array('index')),
	array('label'=>'Tạo Mới Giao Nhận Gas Dư', 'url'=>array('create')),
	array('label'=>'Cập Nhật Giao Nhận Gas Dư', 'url'=>array('update', 'id'=>$model->id)),
//	array('label'=>'Xóa Giao Nhận Gas Dư', 'url'=>array('delete'), 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Bạn chắc chắn muốn xóa ?')),
);
$this->menu= ControllerActionsName::createMenusRoles($menus, $actions);
?>

<h1>Xem Giao Nhận Gas Dư: <?php echo $model->code_no; ?></h1>

<style>
    .size_print { font-size: 13px !important;}
    
</style>

<div class="sprint" style=" padding-right: 743px;">
    <a class="button_print" href="javascript:void(0);" title="In Biên Bản Giao Nhận Gas Dư">
        <img src="<?php echo Yii::app()->theme->baseUrl; ?>/images/print.png">
    </a>
</div>
<div class="clr"></div>
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/jquery.floatThead.min.js"></script>    
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/underscore-min.js"></script>    
<script  src="<?php echo Yii::app()->theme->baseUrl; ?>/js/jquery.printElement.min.js"></script>
<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/print-store-card.css" />
<script type="text/javascript">
	$(document).ready(function(){
            $(".button_print").click(function(){
                    $('#printElement').printElement({ overrideElementCSS: ['<?php echo Yii::app()->theme->baseUrl;?>/css/print-store-card.css'] });
            });
            $('.materials_table').floatThead();
	});
</script>

<?php 
$index=1;
$data = GasRemainExport::getArraySerialPrint($model);
$RemainExport = $data['RemainExport'];
$Serial = $data['Serial'];
$SumColCustomer = 0;
$SumColAgent = 0;
$SumColDynamic = array();

?>

<div class="container" id="printElement">
    <table cellpadding="0" cellspacing="0">
        <tr>
            <td class="w-400">
                <div class="logo">
                    <img src="<?php echo Yii::app()->theme->baseUrl; ?>/images/logo80x80.png">
                </div>            
                <p style="margin: 5px 0 20px 0;">CÔNG TY TNHH HƯỚNG MINH</p>
            </td>
            <td valign="top"  class="align_r" style="padding-right: 100px;">
                Mẫu: 03a - VT<br>
                Số: ........ / .........
            </td>
        </tr>
        <tr>
            <td class="item_c" colspan="2">
                <h2 style="padding:0; margin:0;">BIÊN BẢN GIAO - NHẬN LPG (gas) DƯ</h2>
            </td>
        </tr>
        <tr>
            <td class="agent_info t_padding_15" colspan="2">
                Vào lúc: ...........h ........... Ngày...........tháng...........năm 20 ...........Số xe: ................................................................................................
            </td>
        </tr>
        <tr>
            <td class="agent_info t_padding_15" colspan="2">
                Địa điểm giao nhận hàng hóa: ................................................................ Lí do giao nhận: ......................................................................
            </td>
        </tr>
        <tr>
            <td class="agent_info t_padding_15" colspan="2">
                Bên giao : .................................................................................... Bên nhận: ...........................................................................................
            </td>
        </tr>
        <tr>
            <td class="agent_info" colspan="2">
                Mã Phiếu:&nbsp;&nbsp;&nbsp; <?php echo $model->code_no;?>
                <?php if($model->rParent):?>
                <br>Mã Phiếu Cân Lần 1: &nbsp;&nbsp;<?php echo $model->rParent?$model->rParent->code_no:"";?>
                <?php endif;?>
            </td>
        </tr>
    </table>   
    
    <table class="materials_table materials_table_root materials_table_th hm_table tb" style="width: 100%;">
        <thead>
            <tr class="size_print">
                <th class="item_c " style="width: 10px;" rowspan="2">STT</th>
                <th class=" item_c"  style="width: 150px;" rowspan="2">Khách Hàng</th>
                <th class=" item_c"  style="width: 150px;" rowspan="2">Tên Vật Tư</th>
                <th class=" item_c"  style="width: 50px;" rowspan="2">Số Seri</th>
                <th class=" item_c" style="width: 60px;" rowspan="2">Trọng Lượng Vỏ Bình(kg)</th>
                <th class=" item_c" style="width: 60px;" rowspan="2">Trọng Lượng Vỏ Bình + Gas(kg)</th>
                <th class=" item_c" colspan="<?php echo count($Serial)+2;?>">Trọng Lượng Gas Dư</th>
                <th class=" item_c last" rowspan="2">Ghi Chú</th>
            </tr>
            <tr class="size_print">
                <th class=" item_c" style="width: 30px;">Khách Hàng Trả</th>
                <th class=" item_c" style="width: 30px;">Đại Lý Nhận</th>
                <?php foreach ($Serial as $value): ?>
                <th class=" item_c" style="width: 30px;"><?php echo GasRemainExport::$KG_ARR_TYPE[$value];?></th>
                <?php endforeach;?>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($model->aModelRemain as $key=>$item): ?>
                <?php
                    $amount_has_gas = isset($model->primaryDetail['amount_has_gas'][$item->id])?$model->primaryDetail['amount_has_gas'][$item->id]:0;
                    $amount_gas = isset($model->primaryDetail['amount_gas'][$item->id])?$model->primaryDetail['amount_gas'][$item->id]:0;
                    
                    $note = isset($model->primaryDetail['note'][$item->id])?$model->primaryDetail['note'][$item->id]:'';
                    $amount_customer = $item->amount_gas;
                    $amount_agent = $item->amount_gas_2;
                    $SumColCustomer+=$amount_customer;
                    $SumColAgent+=$amount_agent;
                ?>

                <tr class="size_print">
                    <td class="item_c t_padding_5"><?php echo $key+1;?></td>
                    <td class=""><?php echo $item->customer?$item->customer->first_name:'';?></td>
                    <td class=""><?php echo $item->rMaterials?$item->rMaterials->name:'';?></td>
                    <td class="item_c"><?php echo $item->seri;?></td>
                    <td class="item_c" >
                        <?php echo ActiveRecord::formatCurrency($item->amount_empty);?>
                    </td>
                    <td class="item_c">
                        <?php echo ActiveRecord::formatCurrency($amount_has_gas) ;?>
                    </td>
                    
                    <td class="item_c">
                        <?php echo $amount_customer!=0?ActiveRecord::formatCurrency($amount_customer):'&nbsp;';?>
                    </td>
                    <td class="item_c">
                        <?php echo $amount_agent!=0?ActiveRecord::formatCurrency($amount_agent):'&nbsp;';?>
                    </td>
                    <?php foreach ($Serial as $serialOne=>$typeOne): ?>
                        <?php
                            $RemainExportOne = isset($RemainExport[$item->id][$serialOne])?$RemainExport[$item->id][$serialOne]:0;
                            if(isset($SumColDynamic[$serialOne]))
                                $SumColDynamic[$serialOne] += $RemainExportOne;
                            else
                                $SumColDynamic[$serialOne] = $RemainExportOne;                            
                        ?>
                        <td class="item_c amount_gas">
                            <?php echo $RemainExportOne!=0?ActiveRecord::formatCurrency($RemainExportOne):'&nbsp;';?>
                        </td>
                    <?php endforeach;?>
                    
                    <td class="item_c">
                        <?php echo $note;?>
                    </td>
                </tr>
            <?php endforeach; // end foreach ($model->aModelRemain as $key=> ?>
            
            <tr>
                <td class="item_r item_b" colspan="6">Tổng Cộng:</td>
                <td class="item_c item_b"><?php echo ActiveRecord::formatCurrency($SumColCustomer);?></td>
                <td class="item_c item_b"><?php echo ActiveRecord::formatCurrency($SumColAgent);?></td>
                <?php foreach ($Serial as $serialOne=>$typeOne): ?>
                    <td class="item_c item_b">
                        <?php 
                        echo isset($SumColDynamic[$serialOne])?ActiveRecord::formatCurrency($SumColDynamic[$serialOne]):"&nbsp;";?>
                    </td>
                <?php endforeach;?>
                    <td>&nbsp;</td>
            </tr>
            
            <?php  // Tính Toán thừa thiếu
                $OneRemain = '';
                $OneLess = '';
                $OneValue = $SumColCustomer-$SumColAgent;
                if($OneValue>0){
                    $OneRemain = ActiveRecord::formatCurrency($OneValue);
                }elseif($OneValue<0){
                    $OneLess = ActiveRecord::formatCurrency(abs($OneValue));
                }
            
                $SecondRemain = '';
                $SecondLess = '';
                if(isset($SumColDynamic[1])){
                    $FirstValueDynamic = isset($SumColDynamic[1])?$SumColDynamic[1]:0;
                    $SecondValue = $SumColAgent-$FirstValueDynamic;
                    if($SecondValue>0){
                        $SecondRemain = ActiveRecord::formatCurrency($SecondValue);
                    }elseif($SecondValue<0){
                        $SecondLess = ActiveRecord::formatCurrency(abs($SecondValue));
                    }
                }
                
                $ThirdRemain = '';
                $ThirdLess = '';
                if(isset($SumColDynamic[2])){
                    $FirstValueDynamic = isset($SumColDynamic[1])?$SumColDynamic[1]:0;
                    $SecondValueDynamic = isset($SumColDynamic[2])?$SumColDynamic[2]:0;
                    $ThirdValue = $FirstValueDynamic-$SecondValueDynamic;
                    if($ThirdValue>0){
                        $ThirdRemain = ActiveRecord::formatCurrency($ThirdValue);
                    }elseif($ThirdValue<0){
                        $ThirdLess = ActiveRecord::formatCurrency(abs($ThirdValue));
                    }
                }
                
            ?>
                
            <tr>
                <td colspan="4" style="vertical-align: top;"><b>Ghi Chú</b>: <?php echo $model->note;?></td>
                <td colspan="<?php echo count($Serial)+5;?>">
                    <p>1. NV giao hàng giao:&nbsp;&nbsp;
                        Thừa (kg) <?php echo $OneRemain;?>&nbsp;&nbsp;&nbsp;Thiếu (kg) <?php echo $OneLess;?> </p>
                    <p>2. KT bán hàng giao:&nbsp;&nbsp;
                        Thừa (kg) <?php echo $SecondRemain;?> &nbsp;&nbsp;&nbsp;Thiếu (kg) <?php echo $SecondLess;?> </p>
                    <p>3. Lái xe giao: &nbsp;&nbsp;
                        Thừa (kg) <?php echo $ThirdRemain;?> &nbsp;&nbsp;&nbsp;Thiếu (kg) <?php echo $ThirdLess;?> </p>
                </td>

            </tr>     
                
        </tbody>
    </table>
    
    <table class=" materials_table_root materials_table_th hm_table tb" style="width: 100%;">
        <tbody>
            <tr>
                <td class="item_c  v_top" rowspan="2"><span class="item_b">Đại diện khách hàng</span><br>(Ký - họ tên)</td>
                <td class="item_c item_b v_top" >NV giao hàng (Ký - họ tên)</td>
                <td class="item_c item_b v_top" >KT bán hàng (Ký - họ tên)</td>
                <td class="item_c item_b v_top" >Lái xe (Ký - họ tên)</td>
                <td class="item_c item_b v_top" >Thủ kho</td>
            </tr>
            <tr>
                <td class="item_l v_top" style="height:50px;">Ký nhận:</td>
                <td class="item_l v_top">Ký nhận:</td>
                <td class="item_l v_top">Ký nhận:</td>
                <td class="item_l v_top">Ký nhận - nhập kho:</td>
            </tr>
            <tr>
                <td class="item_l item_b v_top" style="height:50px;">&nbsp;</td>
                <td class="item_l v_top">Ký giao:</td>
                <td class="item_l v_top">Ký giao:</td>
                <td class="item_l v_top">Ký giao:</td>
                <td class="item_l item_b v_top" >&nbsp;</td>
            </tr>
            
        </tbody>
    </table>
    
</div>



