<div id="tabs-4">
    <div class="column" style="width: 45%;">
            <fieldset>
                <legend>Email Settings</legend>
                <div class="row">
                    <?php echo $form->labelEx($model, 'transportType'); ?>
                    <?php echo $form->dropDownList($model, 'transportType', array('php' => 'PHP', 'smtp' => 'Smtp')); ?>
                    <?php echo $form->error($model, 'transportType'); ?>
                </div>

                <div class="row">
                    <?php echo $form->labelEx($model, 'smtpHost'); ?>
                    <?php echo $form->textField($model, 'smtpHost'); ?>
                    <?php echo $form->error($model, 'smtpHost'); ?>
                </div>

                <div class="row">
                    <?php echo $form->labelEx($model, 'smtpPort'); ?>
                    <?php echo $form->textField($model, 'smtpPort'); ?>
                    <?php echo $form->error($model, 'smtpPort'); ?>
                </div>

                <div class="row">
                    <?php echo $form->labelEx($model, 'smtpUsername'); ?>
                    <?php echo $form->textField($model, 'smtpUsername', array('style'=>'width:350px;')); ?>
                    <?php echo $form->error($model, 'smtpUsername'); ?>
                </div>

                <div class="row">
                    <?php echo $form->labelEx($model, 'smtpPassword'); ?>
                    <?php echo $form->passwordField($model, 'smtpPassword', array('style'=>'width:350px;')); ?>
                    <?php echo $form->error($model, 'smtpPassword'); ?>
                </div>

                <div class="row">
                    <?php echo $form->labelEx($model, 'encryption'); ?>
                    <?php echo $form->dropDownList($model, 'encryption', array('None' => 'none', 'ssl' => 'SSL', 'tls' => 'TLS')); ?>
                    <?php echo $form->error($model, 'encryption'); ?>
                </div>

                <div class="row">
                    <?php echo $form->labelEx($model, 'title_all_mail'); ?>
                    <?php echo $form->textField($model, 'title_all_mail', array('class'=>'w-350')); ?>
                    <?php echo $form->error($model, 'title_all_mail'); ?>
                </div>
                <div class="row">
                    <?php echo $form->labelEx($model, 'email_cron_primary', array('label'=>"Default nkhuongminh@gmail.com. Email Cron Primary - chạy cho văn bản + nghỉ phép ... khi nào send nhiều văn bản sẽ sửa email ở đây, không phải vào code sửa nữa", 'style'=>"width:100%;")); ?>
                    <?php echo $form->textField($model, 'email_cron_primary', array('class'=>'w-350')); ?>
                    <?php echo $form->error($model, 'email_cron_primary'); ?>
                </div>
                
                <div class="row">
                    <?php echo $form->labelEx($model, 'email_cron_reset_pass', array('label'=>"Email Cron Reset Pass Only")); ?>
                    <?php echo $form->textField($model, 'email_cron_reset_pass', array('class'=>'w-350')); ?>
                    <?php echo $form->error($model, 'email_cron_reset_pass'); ?>
                </div>
                
            </fieldset>

        </div> 
    <div class="buttons clear">
        <button type="submit">Submit</button>
    </div>
    <hr>
    <div class="buttons clear item_r">
        <a class="btn_cancel" href="<?php echo Yii::app()->createAbsoluteUrl("admin/setting", array('test_sms'=> 84384331552)); ?>">Test SMS</a>
    </div>
    <hr>
    <div class="buttons clear item_r">
        <a class="btn_cancel" href="<?php echo Yii::app()->createAbsoluteUrl("admin/setting", array('test_send_mail'=>1)); ?>">Test send mail</a>
    </div>
    <hr>
    <div class="buttons clear item_r">
        <a class="btn_cancel" href="<?php echo Yii::app()->createAbsoluteUrl("admin/setting", array('RunScheduleSendMail'=>1)); ?>">Run Schedule Sendmail</a>
    </div>
    <hr><br>
    <div class="buttons clear item_r">
        <a class="btn_cancel btn_closed_tickets" alert_text="Chắc chắn muốn Run Copy Giá G tháng trước KH Bò Mối?" href="<?php echo Yii::app()->createAbsoluteUrl("admin/setting", array('RunCopyGprice'=>1)); ?>">Run Copy Giá G tháng trước KH Bò Mối</a>
    </div>
    <hr><br>
    <div class="buttons clear item_r">
        <a class="btn_cancel btn_closed_tickets" alert_text="Chắc chắn muốn Run HgdCronGenStoreCard?" href="<?php echo Yii::app()->createAbsoluteUrl("admin/setting", array('HgdCronGenStoreCard'=>1)); ?>">Run Hgd Cron GenStoreCard</a>
    </div>
    <hr><br>
    <div class="buttons clear item_r">
        <a class="btn_cancel btn_closed_tickets" alert_text="Chắc chắn muốn Run ViewCacheInventory?" href="<?php echo Yii::app()->createAbsoluteUrl("admin/setting", array('ViewCacheInventory'=>1)); ?>">Xem cache tồn kho agent App Order</a>
    </div>
    <hr><br>
    <div class="buttons clear item_r">
        <a class="btn_cancel btn_closed_tickets" alert_text="Chắc chắn muốn Run Agent2014UpdateImportExport?" href="<?php echo Yii::app()->createAbsoluteUrl("admin/setting", array('Agent2014UpdateImportExport'=>1)); ?>">Run Fix Tồn kho Năm <?php echo date('Y')-1;?> Agent2014UpdateImportExport</a>
    </div>
    <hr><br>
    <div class='clr'></div>
</div><!-- end <div id="tabs-4">-->