<?php $this->breadcrumbs = array('System Configurations', ); ?>
<p style="text-align: center;">
<?php
/*
http://softwareengineering.stackexchange.com/questions/251062/how-many-bytes-can-i-fit-into-a-qr-code-with-low-error-correction
A better strategy is to link the data into an URL and turn the URL into a QR-code; there is no limit on the URL linked resource. You can put images, SVG, html, text file...
 */
//http://www.yiiframework.com/extension/qr-code-generator/
//$this->widget('application.extensions.qrcode.QRCodeGenerator',array(
//    'data' => urlencode('http://www.bryantan.info'),
//    'subfolderVar' => false,
//    'matrixPointSize' => 5,
//    'displayImage'=>true, // default to true, if set to false display a URL path
//    'errorCorrectionLevel'=>'L', // available parameter is L,M,Q,H
//    'matrixPointSize'=>4, // 1 to 10 only
//));
// OK barcode test FEb 04, 2017
/*
$generator = new BarcodeGeneratorPNG();
$code = '081231723897';
echo '<img src="data:image/png;base64,' . base64_encode($generator->getBarcode($code, $generator::TYPE_CODE_128)) . '">';
 */
//echo $code;
//$generator = new BarcodeGeneratorHTML();
//echo $generator->getBarcode('081231723897', $generator::TYPE_CODE_128);
?>
</p>
<?php // include 'index_count_down.php'; ?>
<h1>System configurations</h1>
<?php echo MyFormat::BindNotifyMsg(); ?>
<div class="form fieldset_label">
<?php $form = $this->beginWidget('CActiveForm', array(
    'id' => 'setting-form-admin-form',
    'enableAjaxValidation' => false,
    'method'=>'post',
    'htmlOptions' => array('enctype' => 'multipart/form-data'),
)); ?>

    <p class="note">Fields with <span class="required">*</span> are required.</p>
    <?php echo $form->errorSummary($model); ?>

<?php Yii::app()->clientScript->registerCoreScript('jquery.ui'); ?>
<?php
$gridCss = Yii::app()->getAssetManager()->publish(Yii::getPathOfAlias('zii.widgets.assets')).'/gridview/styles.css';
Yii::app()->getClientScript()->registerCssFile($gridCss);
$mAppCache = new AppCache();
?>
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/tagit/tag-it.min.js"></script>
<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/js/tagit/jquery.tagit.css"/>
<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/js/tagit/tagit.ui-zendesk.css"/>
<script>
$(function() {
    $( "#tabs" ).tabs({ active: 2 });
//    $( "#tabs" ).tabs({ active: 0 });
    var sampleTags = ['c++', 'java', 'php', 'coldfusion', 'javascript', 'asp', 'ruby', 'python', 'c', 'scala', 'groovy', 'haskell', 'perl', 'erlang', 'apl', 'cobol', 'go', 'lua'];
    $('#singleFieldTags2').tagit({
        availableTags: sampleTags
    });
});
</script>
<div id="tabs">
    <ul>
        <li><a href="#tabs-1">Cài đặt chung</a></li>
        <li><a href="#tabs-2">Cài đặt hệ thống</a></li>
        <li><a href="#tabs-3">System Care</a></li>
        <li><a href="#tabs-4">Mail</a></li>
    </ul>
    
    <?php  include 'system_care.php';?>
    <?php  include 'system_mail.php';?>
    
    <div id="tabs-1">
        <div class="column" style="width: 45%">
            <fieldset>
                <legend>Thiết Đặt Hệ Thống</legend>
                    <div class="row">
                        <?php echo $form->labelEx($model, 'time_disable_login', array('label'=>'Giờ cấm truy cập')); ?>
                        <?php echo $form->textField($model, 'time_disable_login', array('size'=>35, 'placeholder'=>'vd: 18:35')); ?>
                        <?php echo $form->error($model, 'time_disable_login'); ?>
                    </div>            
                    <div class="row">
                        <?php echo $form->labelEx($model, 'limit_update_maintain', array('label'=>'Giới hạn số lần cập nhật của bảo trì')); ?>
                        <?php echo $form->textField($model, 'limit_update_maintain', array('size'=>35)); ?>
                        <?php echo $form->error($model, 'limit_update_maintain'); ?>
                    </div>
            </fieldset>

            <fieldset>
                <legend>General Settings</legend>

                <div class="row">
                    <?php echo $form->labelEx($model, 'title'); ?>
                    <?php echo $form->textField($model, 'title', array('style'=>'width:350px;')); ?>
                    <?php echo $form->error($model, 'title'); ?>
                </div>

                <div class="row">
                    <?php echo $form->labelEx($model, 'adminEmail'); ?>
                    <?php echo $form->textField($model, 'adminEmail', array('style'=>'width:350px;')); ?>
                    <?php echo $form->error($model, 'adminEmail'); ?>
                </div>

                <div class="row">
                    <?php echo $form->labelEx($model, 'autoEmail'); ?>
                    <?php echo $form->textField($model, 'autoEmail', array('style'=>'width:350px;')); ?>
                    <?php echo $form->error($model, 'autoEmail'); ?>
                </div>

                <div class="row display_none">
                    <?php echo $form->labelEx($model, 'meta_description'); ?>
                    <?php echo $form->textArea($model, 'meta_description', array('rows'=>5,'cols'=>35,'style'=>'width:350px;')); ?>
                    <?php echo $form->error($model, 'meta_description'); ?>
                </div>

                <div class="row display_none">
                    <?php echo $form->labelEx($model, 'meta_keywords'); ?>
                    <?php echo $form->textArea($model, 'meta_keywords', array('rows'=>5,'cols'=>35,'style'=>'width:350px;')); ?>
                    <?php echo $form->error($model, 'meta_keywords'); ?>
                </div>

                <div class="row">
                    <?php echo $form->labelEx($model, 'login_limit_times'); ?>
                    <?php echo $form->textField($model, 'login_limit_times', array('style'=>'width:350px;')); ?>
                    <?php echo $form->error($model, 'login_limit_times'); ?>
                </div>

                <div class="row">
                    <label for="SettingForm_time_refresh_login">Time Refresh Login(minutes)</label>
                    <?php echo $form->textField($model, 'time_refresh_login', array('style'=>'width:350px;')); ?>
                    <?php echo $form->error($model, 'time_refresh_login'); ?>
                </div>

                <div class="row">
                    <?php echo $form->labelEx($model, 'server_name',array('label'=>'Server Name For Cron Job')); ?>
                    <?php echo $form->textField($model, 'server_name', array('size'=>45)); ?>
                    <?php echo $form->error($model, 'server_name'); ?>
                </div> 
                <div class="row display_none">
                    <?php echo $form->labelEx($model, 'note_type_pay'); ?>
                    <?php echo $form->textArea($model, 'note_type_pay', array('rows'=>5,'cols'=>35,'style'=>'width:350px;')); ?>
                    <?php echo $form->error($model, 'note_type_pay'); ?>
                </div>
            </fieldset>
        </div>
        
        <div class="column" style="width: 45%; ">
            <div class="buttons clear">
                <button type="submit">Submit</button>
            </div>
            <fieldset>
                <legend>Allow IP for Kế Toán</legend>
                    <div class="row">
                        <input name="tags" id="singleFieldTags2" value="<?php echo $mAppCache->getCache(AppCache::ALLOW_IP_ACCOUNTING);?>">
                    </div>            
            </fieldset>
        </div>
        
        <div class='clr'></div>
    </div><!-- end <div id="tabs-1">-->
    <div id="tabs-2">
        
        <div class="buttons clear">
            <button type="submit">Submit</button>
        </div>

        <div class="column" style="width: 45%; ">
            <fieldset>
                <legend>Hệ thống bảo trì</legend>
                <div class="row">
                    <?php echo $form->labelEx($model, 'server_maintenance', array('label'=>'Bảo Trì Server')); ?>
                    <?php echo $form->dropDownList($model, 'server_maintenance', array('yes'=>'Yes','no'=>'No')); ?>
                </div>
                
                <div class="row">
                    <?php echo $form->labelEx($model, 'server_maintenance_message',array('label'=>'Thông Báo Bảo Trì Server')); ?>
                    <?php echo $form->textArea($model, 'server_maintenance_message', array('rows'=>5,'cols'=>35,'style'=>'width:350px;')); ?>
                </div>
                
                <div class="row">
                    <?php echo $form->labelEx($model, 'allow_session_menu', array('label'=>'Allow Session Menu Cache menu vào session')); ?>
                    <?php echo $form->dropDownList($model, 'allow_session_menu', CmsFormatter::$yesNoCharFormat); ?>
                </div>                 
            </fieldset>
        </div>        
        
        <div class='clr'></div>
    </div><!-- end <div id="tabs-2">-->
</div> <!-- end <div id="tabs">-->
    
    <div class='clr'></div>
    <div class="buttons clear">
        <button type="submit">Submit</button>
    </div>

<?php $this->endWidget(); ?>

</div><!-- form -->