<div id="tabs-3">
        <div class="column" style="width: 45%; ">
            <fieldset>
                <legend>Toàn Hệ Thống</legend>
                <div class="row">
                    <?php echo $form->labelEx($model, 'check_login_same_account', array('label'=>'Check Login Same Account')); ?>
                    <?php echo $form->dropDownList($model, 'check_login_same_account', CmsFormatter::$yesNoCharFormat, array('class'=>'w-200') ) ; ?>
                </div>
                <div class="row">
                    <?php echo $form->labelEx($model, 'allow_admin_login', array('label'=>'Allow Admin Login')); ?>
                    <?php echo $form->dropDownList($model, 'allow_admin_login', CmsFormatter::$yesNoCharFormat, array('class'=>'w-200') ) ; ?>
                </div>
                <div class="row">
                    <?php echo $form->labelEx($model, 'allow_use_admin_cookie', array('label'=>'Allow Use Admin Cookie')); ?>
                    <?php echo $form->dropDownList($model, 'allow_use_admin_cookie', CmsFormatter::$yesNoCharFormat, array('class'=>'w-200') ) ; ?>
                </div>
                <div class="row">
                    <?php echo $form->labelEx($model, 'delete_global_days', array('label'=>'Số Ngày Được Xóa')); ?>
                    <?php echo $form->textField($model, 'delete_global_days'); ?> Ngày
                </div>
                <div class="row display_none">
                    <?php echo $form->labelEx($model, 'month_limit_search_pttt', array('label'=>'Giới Hạn Tháng Search KH PTTT')); ?>
                    <?php echo $form->textField($model, 'month_limit_search_pttt'); ?> Tháng
                </div>
                <div class="row">
                    <?php echo $form->labelEx($model, 'cookie_days', array('label'=>'Số Ngày Cookie Login')); ?>
                    <?php echo $form->textField($model, 'cookie_days'); ?> Ngày
                </div>
                <div class="row">
                    <?php echo $form->labelEx($model, 'days_keep_track_login', array('label'=>'Số Ngày Giữ Tracking Login')); ?>
                    <?php echo $form->textField($model, 'days_keep_track_login'); ?> Ngày
                </div>
                <!--Feb 16, 2017 important không nên bật xóa hệ thống-->
                <div class="row ">
                    <?php echo $form->labelEx($model, 'enable_delete', array('label'=>'Next time update 5_minutes')); ?>
                    <label><?php echo $model->getTimeUpdateSetting();?></label>
                    <?php echo $form->hiddenField($model, 'time5MUpdateSetting'); ?>
                    <div class="clr"></div>
                    <?php echo $form->labelEx($model, 'enable_delete', array('label'=>'Cho Phép Xóa Global')); ?>
                    <?php echo $form->dropDownList($model, 'enable_delete', CmsFormatter::$yesNoCharFormat, array('class'=>'w-200') ) ; ?>
                </div>
                <div class="row ">
                    <?php echo $form->labelEx($model, 'EnableChangeExt', array('label'=>'Allow User CallCenter change EXT + Admin Login Giao nhận')); ?>
                    <?php echo $form->dropDownList($model, 'EnableChangeExt', CmsFormatter::$yesNoCharFormat, array('class'=>'w-200') ) ; ?>
                </div>
                <div class="clr"></div>
                <div class="row">
                    <?php echo $form->labelEx($model, 'AppVersionCode', array('label'=>'App Gas Service Version Code')); ?>
                    <?php echo $form->textField($model, 'AppVersionCode'); ?>
                </div>
                <div class="clr"></div>
                <div class="row">
                    <?php echo $form->labelEx($model, 'AppVersionCodeGas24hIos', array('label'=>'App Gas24h Version ios')); ?>
                    <?php echo $form->textField($model, 'AppVersionCodeGas24hIos'); ?>
                </div>
                <div class="clr"></div>
                <div class="row">
                    <?php echo $form->labelEx($model, 'AppVersionCodeGas24hAndroid', array('label'=>'App Gas24h App Version android')); ?>
                    <?php echo $form->textField($model, 'AppVersionCodeGas24hAndroid'); ?>
                </div>
                <div class="clr"></div>
                <div class="row">
                    <?php echo $form->labelEx($model, 'DaysUpdateFixAll', array('label'=>'Ngày sửa data system Nguyệt BT2')); ?>
                    <?php echo $form->textField($model, 'DaysUpdateFixAll'); ?>
                </div>
                <div class="row">
                    <?php echo $form->labelEx($model, 'days_update_support_agent', array('label'=>'Ngày update Xe + tài default 3 - gasOrders/updateCarNumber')); ?>
                    <?php echo $form->textField($model, 'days_update_support_agent'); ?>
                </div> 
            </fieldset>
            <fieldset>
                <legend>Thẻ Kho</legend>
                <div class="row">
                    <?php echo $form->labelEx($model, 'storecard_admin_update', array('label'=>'Số Ngày Admin Cập Nhật')); ?>
                    <?php echo $form->textField($model, 'storecard_admin_update'); ?>
                </div>
                <div class="row">
                    <?php echo $form->labelEx($model, 'storecard_admin_delete', array('label'=>'Số Ngày Admin Xóa')); ?>
                    <?php echo $form->textField($model, 'storecard_admin_delete'); ?>
                </div>
                <div class="row">
                    <?php echo $form->labelEx($model, 'storecard_agent_updateCollectionCustomer', array('label'=>'Số Ngày Đại Lý Cập Nhật Thu Tiền')); ?>
                    <?php echo $form->textField($model, 'storecard_agent_updateCollectionCustomer'); ?>
                </div>
                <div class="row">
                    <?php echo $form->labelEx($model, 'days_update_customer_bo_moi', array('label'=>'Số Ngày Cập Nhật KH Thẻ Kho - Update_customer_store_card')); ?>
                    <?php echo $form->textField($model, 'days_update_customer_bo_moi'); ?>
                </div>
            </fieldset>
            <fieldset>
                <legend>Gas Dư</legend>
                <div class="row ">
                    <?php echo $form->labelEx($model, 'gas_remain_agent_update', array('label'=>'Số Ngày Đại Lý Cập Nhật')); ?> dùng chung với thẻ kho
                    <?php // echo $form->textField($model, 'gas_remain_agent_update'); ?>
                </div><br>
                <div class="row">
                    <?php echo $form->labelEx($model, 'gas_remain_agent_update_remain2_3', array('label'=>'Số Ngày Đại Lý Cập Nhật Lần 2 và 3')); ?>
                    <?php echo $form->textField($model, 'gas_remain_agent_update_remain2_3'); ?>
                </div>
            </fieldset>
            <fieldset>
                <legend>Tickets</legend>
                <div class="row">
                    <?php echo $form->labelEx($model, 'limit_post_ticket', array('label'=>'Số ticket được post trong 1 ngày')); ?>
                    <?php echo $form->textField($model, 'limit_post_ticket'); ?>
                </div>
                <div class="row">
                    <?php echo $form->labelEx($model, 'ticket_page_size', array('label'=>'Page Size Ticket')); ?>
                    <?php echo $form->textField($model, 'ticket_page_size'); ?>
                </div>
            </fieldset>
            
            <fieldset>
                <legend>Kiểm tra khách hàng - gasCustomerCheck</legend>
                <div class="row">
                    <?php echo $form->labelEx($model, 'days_update_customer_check', array('label'=>'Số ngày cập nhật grid - actionUpdate')); ?>
                    <?php echo $form->textField($model, 'days_update_customer_check'); ?>
                </div>
                <div class="row">
                    <?php echo $form->labelEx($model, 'days_update_customer_check_report', array('label'=>'Số ngày cập nhật Report - actionUpdate_customer')); ?>
                    <?php echo $form->textField($model, 'days_update_customer_check_report'); ?>
                </div>
                <br>
            </fieldset>
            
            <fieldset>
                <legend>Phân Công Công Việc - gasBreakTask</legend>
                <div class="row">
                    <?php echo $form->labelEx($model, 'days_update_break_task', array('label'=>'Số ngày cập nhật grid - actionUpdate')); ?>
                    <?php echo $form->textField($model, 'days_update_break_task'); ?>
                </div>
                <br>
            </fieldset>
            <fieldset>
                <legend>Hỗ trợ khách hàng lớn - GasSupportCustomer</legend>
                <div class="row">
                    <?php echo $form->labelEx($model, 'days_update_support_customer', array('label'=>'Số ngày cập nhật grid - actionUpdate')); ?>
                    <?php echo $form->textField($model, 'days_update_support_customer'); ?>
                </div>
                <br>
                <div class="row">
                    <?php echo $form->labelEx($model, 'month_statistic_output_customer', array('label'=>'Số tháng tính thống kê sản lượng trong view')); ?>
                    <?php echo $form->textField($model, 'month_statistic_output_customer'); ?> tháng
                </div>
                <br>
                <div class="row">
                    <?php echo $form->labelEx($model, 'days_update_support_customer_to_print', array('label'=>'Số ngày update to print')); ?>
                    <?php echo $form->textField($model, 'days_update_support_customer_to_print'); ?> ngày
                </div>
                <br>
            </fieldset>
            <fieldset>
                <legend>Quyết toán - GasSettle</legend>
                <div class="row">
                    <?php echo $form->labelEx($model, 'days_update_settle', array('label'=>'Số ngày cập nhật grid - actionUpdate')); ?>
                    <?php echo $form->textField($model, 'days_update_settle'); ?>  ngày
                </div>
                <br>
                <div class="row">
                    <?php echo $form->labelEx($model, 'DaysUpdateSettle', array('label'=>'Số Ngày View Record - view tạm ứng đã duyệt để tạo quyết toán')); ?>
                    <?php echo $form->textField($model, 'DaysUpdateSettle'); ?>
                </div>
                <br>
                <div class="row">
                    <?php echo $form->labelEx($model, 'DaysExpiryTamUng', array('label'=>'Số Ngày hết hạn tạm ứng - quá ngày này ko cho tạm ứng nữa')); ?>
                    <?php echo $form->textField($model, 'DaysExpiryTamUng'); ?>
                </div>
            </fieldset>
            

        </div> <!-- end col left <div class="column" -->
        <div class="buttons clear">
            <button type="submit">Submit</button>
        </div>

        <div class="column" style="width: 45%;">
            
            <fieldset>
                <legend>Bán hàng hộ gia đình - Sell</legend>
                <div class="row display_none">
                    <?php echo $form->labelEx($model, 'days_update_sell', array('label'=>'Số ngày cập nhật dưới C# client')); ?>
                    <?php echo $form->textField($model, 'days_update_sell'); ?>  ngày
                </div>
                <br>
                <div class="row">
                    <?php echo $form->labelEx($model, 'DaysUpdateAppBoMoi', array('label'=>'Ngày GN cập nhật App Bò Mối')); ?>
                    <?php echo $form->textField($model, 'DaysUpdateAppBoMoi'); ?>  ngày
                </div>
                <div class="row">
                    <?php echo $form->labelEx($model, 'DaysSetDebitAppBoMoi', array('label'=>'Ngày Set Nợ App Bò Mối')); ?>
                    <?php echo $form->textField($model, 'DaysSetDebitAppBoMoi'); ?>  ngày
                </div>
                <div class="row">
                    <?php echo $form->labelEx($model, 'listUserDebugLog', array('label'=>'List username write log debug')); ?>
                    <?php echo $form->textField($model, 'listUserDebugLog', ['class' => 'w-300', 'placeholder' => 'vd: username1-username2-username3']); ?>
                </div>
            </fieldset>
<!--            
            <fieldset>
                <legend>Phát Triền Thị Trường</legend>
                <div class="row">
                    <?php echo $form->labelEx($model, 'PTTT_update_file_scan', array('label'=>'Số Ngày Đại Lý Cập Nhật PTTT file scan')); ?>
                    <?php echo $form->textField($model, 'PTTT_update_file_scan'); ?>
                </div>         
            </fieldset>
            <fieldset>
                <legend>Bán Hàng Phát Triền Thị Trường</legend>
                <div class="row">
                    <?php echo $form->labelEx($model, 'days_update_maintain_sell', array('label'=>'Gasmaintainsell Số Ngày Đại Lý Cập Nhật Bán Hàng PTTT ')); ?>
                    <?php echo $form->textField($model, 'days_update_maintain_sell'); ?>
                </div><br>
                <div class="row">
                    <?php echo $form->labelEx($model, 'PTTT_SELL_update_file_scan', array('label'=>'Số Ngày Đại Lý Cập Nhật Bán Hàng PTTT file scan')); ?>
                    <?php echo $form->textField($model, 'PTTT_SELL_update_file_scan'); ?>
                </div>         
            </fieldset>-->
            <fieldset>
                <legend>KH Bò Mối</legend>
                <div class="row">
                    <?php echo $form->labelEx($model, 'can_update_customer_maintain', array('label'=>'Sale cập nhật KH Bò Mối')); ?>
                    <?php echo $form->dropDownList($model, 'can_update_customer_maintain', CmsFormatter::$yesNoCharFormat, ['class'=>'w-200'] ) ; ?>
                </div>    
            </fieldset>
            <fieldset>
                <legend>SPANCOP Báo Cáo Ký Hợp Đồng KH - GasBussinessContract</legend>
                <div class="row">
                    <?php echo $form->labelEx($model, 'days_update_bussiness_contract', array('label'=>'Số Ngày Cập Nhật Báo Cáo KH')); ?>
                    <?php echo $form->textField($model, 'days_update_bussiness_contract'); ?>
                </div>         
                <div class="row">
                    <?php echo $form->labelEx($model, 'days_update_guide_help', array('label'=>'Số Ngày Cập Nhật hướng dẫn thực hiện trong view SPANCOP')); ?>
                    <?php echo $form->textField($model, 'days_update_guide_help'); ?>
                </div><br>
                <div class="row">
                    <?php echo $form->labelEx($model, 'month_limit_update_thuong_luong', array('label'=>'Số tháng search array last id ( mới nhât ) thương lượng, để check chi cho update dòng thương lượng moi nhat')); ?>
                    <?php echo $form->textField($model, 'month_limit_update_thuong_luong'); ?>
                </div><br><br><br>
                <div class="row">
                    <?php echo $form->labelEx($model, 'month_update_first_purchase', array('label'=>'Số tháng limit for CronUpdateFirstPurchase')); ?>
                    <?php echo $form->textField($model, 'month_update_first_purchase'); ?>tháng
                </div>         
            </fieldset>
            <fieldset>
                <legend>Văn Bản - GasText</legend>
                <div class="row">
                    <?php echo $form->labelEx($model, 'days_update_text_file', array('label'=>'Số Ngày Cập Nhật Văn Bản')); ?>
                    <?php echo $form->textField($model, 'days_update_text_file'); ?>
                </div> 
                <div class="row">
                    <?php echo $form->labelEx($model, 'max_van_ban_create_in_day', array('label'=>'Max slg văn bản tạo trong ngày')); ?>
                    <?php echo $form->textField($model, 'max_van_ban_create_in_day'); ?>
                </div>
            </fieldset>
            <fieldset>
                <legend>Hồ Sơ Pháp Lý - GasProfile</legend>
                <div class="row">
                    <?php echo $form->labelEx($model, 'days_update_profile_scan', array('label'=>'Số Ngày Cập Nhật Hồ Sơ Pháp Lý')); ?>
                    <?php echo $form->textField($model, 'days_update_profile_scan'); ?>
                </div>
                <div class="row">
                    <?php echo $form->labelEx($model, 'profile_day_alert_expiry', array('label'=>'Số ngày cảnh báo hết hạn hồ sơ pháp lý')); ?>
                    <?php echo $form->textField($model, 'profile_day_alert_expiry'); ?>
                </div>
            </fieldset>
            <fieldset>
                <legend>Quản lý cấp phát công cụ dụng cụ - GasManageTool</legend>
                <div class="row">
                    <?php echo $form->labelEx($model, 'days_update_manage_tool', array('label'=>'Số Ngày Cập Nhật cấp phát công cụ dụng cụ')); ?>
                    <?php echo $form->textField($model, 'days_update_manage_tool'); ?>
                </div>         
            </fieldset>
            <fieldset>
                <legend> Quản Lý Nghỉ Phép - gasLeave/index</legend>
                <div class="row">
                    <?php echo $form->labelEx($model, 'days_update_leave', array('label'=>'Số Ngày Cập Nhật nghỉ phép')); ?>
                    <?php echo $form->textField($model, 'days_update_leave'); ?>
                </div>         
            </fieldset>
            <fieldset>
                <legend> Quản Lý Biên Bản Họp - gasMeetingMinutes/index</legend>
                <div class="row">
                    <?php echo $form->labelEx($model, 'days_update_meeting_minutes', array('label'=>'Số Ngày Cập Nhật Biên Bản Họp')); ?>
                    <?php echo $form->textField($model, 'days_update_meeting_minutes'); ?>
                </div>         
            </fieldset>
            <fieldset>
                <legend>  Bình Quay Về Hàng Ngày - gasPtttDailyGoback/index</legend>
                <div class="row">
                    <?php echo $form->labelEx($model, 'days_update_PTTT_daily_goback', array('label'=>'Số Ngày Cập Nhật - actionUpdate')); ?>
                    <?php echo $form->textField($model, 'days_update_PTTT_daily_goback'); ?>
                </div>         
            </fieldset>            
            <fieldset>
                <legend>  Bảo Trì - Sự cố - gasUphold/index</legend>
                <div class="row">
                    <?php echo $form->labelEx($model, 'days_update_uphold', array('label'=>'Số Ngày Cập Nhật - actionUpdate')); ?>
                    <?php echo $form->textField($model, 'days_update_uphold'); ?>
                </div>         
            </fieldset>
            <fieldset>
                <legend> Đề xuất sửa chữa - gasSupportAgent/index</legend>
                        
            </fieldset>
            <fieldset>
                <legend>Đặt hàng vật tư - khuyến mãi - gasOrderPromotion/index</legend>
                <div class="row">
                    <?php echo $form->labelEx($model, 'days_update_order_promotion', array('label'=>'Số Ngày Cập Nhật - actionUpdate')); ?>
                    <?php echo $form->textField($model, 'days_update_order_promotion'); ?>
                </div>         
            </fieldset>
            <fieldset>
                <legend>Quản Lý Vay - borrow/index</legend>
                <div class="row">
                    <?php echo $form->labelEx($model, 'DaysUpdateBorrow', array('label'=>'Số Ngày Cập Nhật - actionUpdate')); ?>
                    <?php echo $form->textField($model, 'DaysUpdateBorrow'); ?>
                </div>         
            </fieldset>
            
            <fieldset>
                <legend>KH hộ gia đình CCS - gascustomer/vip240</legend>
                <div class="row">
                    <?php echo $form->labelEx($model, 'DaysUpdateHgd', array('label'=>'Số Ngày Cập Nhật Trên App Android')); ?>
                    <?php echo $form->textField($model, 'DaysUpdateHgd'); ?>
                </div>
            </fieldset>
            <fieldset>
                <legend>App Gas24h</legend>
                <div class="row">
                    <?php echo $form->labelEx($model, 'EnableUpdateSell', array('label'=>'Mở App Gas24h Login')); ?>
                    <?php echo $form->dropDownList($model, 'EnableUpdateSell', CmsFormatter::$yesNoCharFormat, array('class'=>'w-200') ) ; ?>
                </div>
                <br>
                <div class="row">
                    <?php echo $form->labelEx($model, 'Gas24hApplyPrice', array('label'=>'Set cứng 1 giá trên App')); ?>
                    <?php echo $form->dropDownList($model, 'Gas24hApplyPrice', CmsFormatter::$yesNoCharFormat, array('class'=>'w-200') ) ; ?>
                </div>
                <div class="row">
                    <?php echo $form->labelEx($model, 'Gas24hPrice', array('label'=>'Số tiền giảm trên app')); ?>
                    <?php echo $form->textField($model, 'Gas24hPrice'); ?> vnd
                </div>
                <div class="row display_none">
                    <?php echo $form->labelEx($model, 'distanceAgentApp', array('label'=>'Bán kính App đại lý')); ?>
                    <?php echo $form->textField($model, 'distanceAgentApp'); ?> mét
                </div>
                
                
                <div class="row">
                    <?php echo $form->labelEx($model, 'bankDiscountFirstTime', array('label'=>'Giảm thanh toán thẻ lần đầu')); ?>
                    <?php echo $form->textField($model, 'bankDiscountFirstTime'); ?>  %
                </div>
                <div class="row">
                    <?php echo $form->labelEx($model, 'bankDiscountSecondTime', array('label'=>'Giảm thanh toán thẻ lần 2')); ?>
                    <?php echo $form->textField($model, 'bankDiscountSecondTime'); ?>  %
                </div>
                <div class="row">
                    <?php echo $form->labelEx($model, 'goldTimeDiscount', array('label'=>'Giảm đặt giờ vàng')); ?>
                    <?php echo $form->textField($model, 'goldTimeDiscount'); ?>  vnd
                </div>
                <div class="row">
                    <?php echo $form->labelEx($model, 'goldTimeList', array('label'=>'List giờ vàng')); ?>
                    <?php echo $form->textField($model, 'goldTimeList'); ?>
                </div>
                <div class="row">
                    <?php echo $form->labelEx($model, 'goldTimeText', array('label'=>'Text giờ vàng on app')); ?>
                    <?php echo $form->textField($model, 'goldTimeText', ['class' => 'w-300']); ?>
                </div>
                
            </fieldset>
            <fieldset>
                <legend>GasEventMarket</legend>
                <div class="row">
                    <?php echo $form->labelEx($model, 'DaysGasEventMarket', array('label'=>'Số ngày duyệt roadshow')); ?>
                    <?php echo $form->textField($model, 'DaysGasEventMarket', ['class' => 'w-300']); ?>
                </div>
                
            </fieldset>
            
        </div> <!-- end col right <div class="column" -->
        
        <div class='clr'></div>
</div><!-- end <div id="tabs-3">-->