<?php
    include '_count_down.php';
    $mSignup = new AppSignup();
    /* 1. get all record first những KH login trong AppSignup
     * 2. get all record đơn hàng đầu tiên trong model Sell
     * 3. tính khoảng time giữa đơn hàng và login
     */

?>
<div class="">
    <div class="logo">
        <img src="<?php echo Yii::app()->theme->baseUrl; ?>/css/GameApp/logo_ver_en.png" alt=""/>
    </div>
    <div class="box-start">
        <div class="number">
            <div id="CountDownPanel"></div>
            <button class="btn-start" onclick="doStart()">Start</button>
        </div>
    </div>
   <div class="wrap-box-wrap BoxGame">
        <div class="wrap-box1">
                <div class="item-box1">
                    <h4>Cài đặt App</h4>
                <div class="info-box1">
                    <!--<div class="step"><span>Bước 1</span> Tải App</div>-->
                    <div class="step-style2"><span>B1</span> Tải App</div>
                    <p>- Tải app <strong style="color:red">Gas24h</strong> trên <strong>AppStore</strong> hoặc <strong>Google Play</strong> ( từ khóa: gas24h)</p>
                    <p>- Tải bằng QR Code</p>
                    <!--<div class="step"><span>Bước 2</span>Đăng ký</div>-->
                    <div class="step-style2"><span>B2</span> Đăng ký</div>
                    <p>- Dùng số điện thoại để đăng ký tài khoản</p>
                    <p>- Nhập mã  Pin từ tin nhắn SMS để hoàn tất</p>
                    <!--<div class="step"><span>Bước 3</span>Đặt gas</div>-->
                    <div class="step-style2"><span>B3</span> Đặt gas</div>
                    <p>- Đặt gas thành công để nhận phần thưởng</p>
                </div>
            </div>
             <div class="item-box1 mid-box CountDownload">
                <h4 style="text-align:center">số người tải thành công</h4>
                <div class="number-sc">
                    <?php echo $mSignup->countLoginByDate(); ?>
                </div>
                <div class="loading display_none">
                    <img src="<?php echo Yii::app()->theme->baseUrl; ?>/css/GameApp/loading5.gif" alt="">
                </div>
             </div>
             <div class="item-box1">
             <h4>kết quả</h4>
                <?php echo UpdateSql::renderHtmlTopOrder();?>
             </div>
        </div>
   </div>
</div>
<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/GameApp/spj_game.css" type="text/css" />