<span id="minutes"></span>:<span id="seconds"></span>

<?php 
    $countItem = 19;// số item li - tương đương với bao nhiêu số phone
?>
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/jquery.easing.1.3.js"></script>
<script>
    $(document).ready(function () {
        
        var sec = 0;
        function pad ( val ) { return val > 9 ? val : "0" + val; }
        setInterval( function(){
            document.getElementById("seconds").innerHTML=pad(++sec%60);
            document.getElementById("minutes").innerHTML=pad(parseInt(sec/60,10));
        }, 1000);
        

    // Clone the first element and append it to the end of the list
    var list = $('#slotmachine>ul:first');
    var firstItem = list.find('li:first');
    firstItem.clone().appendTo(list);
    var listHeight = <?php echo $countItem;?> * 200;

    function moveTo(val) {
        val = -val % listHeight;
        if (val > 0) val -= listHeight;
        $('#slotmachine').css({
            top: val
        });
    }

    function spin(count) {
        $('#slotmachine').stop().animate({
            top: -listHeight
        }, 2000, 'linear', function () {
            if (count == 0) {
                var slot = Math.floor(Math.random() * <?php echo $countItem;?>),
                    top = -slot * 200,
                    time =  2000 * slot / <?php echo $countItem;?>;
                console.log(count, slot, top, time)
                $(this).css({
                    top: 0
                }).animate({
                    top: top
                },time, 'easeOutQuad')
            } else {
                $(this).css({
                    top: 0
                })
                spin(count - 1)
            };
        });
    }
    $('#start').click(function () {
        $('#slotmachine').css({
            top: 0
        });
        spin(5);
    });

    $('#moveTo').click(function () {
        moveTo($('#pos').val());
    });
});
</script>

<div id="viewbox">
    <div class="wrapper" id="slotmachine">
        <ul>
            <li style="background:#f00">01684331552</li>
            <li style="">09881111</li>
            <li style="background:#ff0">094882222</li>
            <li style="background:#ff0">0988 3333</li>
            <li style="background:#ff0">0988 4444</li>
            <li style="background:#ff0">0988 555</li>
            <li style="background:#ff0">0988 666</li>
            <li style="background:#0f0">3</li>
            <li style="background:#0ff">4</li>
            <li style="background:#00f">5</li>
            <li style="background:#f0f">6</li>
            <li style="background:#f0f">7</li>
            <li style="background:#f0f">8</li>
            <li style="background:#ff0">098841 777</li>
            <li style="background:#f0f">9</li>
            <li style="background:#ff0">098841 888</li>
            <li style="background:#f0f">10</li>
            <li style="background:#ff0">098841 999</li>
            <li style="background:#f0f">11</li>
        </ul>
    </div>
</div>
<input type="button" value="start" id="start" />|
<input type="button" value="move To" id="moveTo" />
<input type="text" id="pos" value="0" size="5" />



<style type="text/css">

#viewbox {
    overflow: hidden;
    /*width: 500px;*/
    height: 200px;
    border: solid 1px #000;
    position: relative;
}
#viewbox .wrapper {
    position: relative;
}
#viewbox ul {
    list-style: none;
    margin: 0;
    padding: 0;
}
#viewbox li {
    display: block;
    /*width: 800px;*/
    height: 200px;
    text-align: center;
    font-size: 170px;
}
</style>