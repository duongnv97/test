<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>CÔNG TY CỔ PHẦN DẦU KHÍ MIỀN NAM</title>
<style type="text/css" media="print">
    body p{ margin-top: 0px; font-size: 16px; color: #333333; margin-bottom: 0px;line-height: 25px }

</style>
</head>
<body  bgcolor="#ffffff" style="BACKGROUND-COLOR:#ffffff;">
<table width="800px" style="margin: 0 auto; border: none;font-family: Arial;border: none; font-size:14px">
	
   <tr>
      <td width= "450px">
        <img src="http://android.huongminhgroup.com/upload/temp_image/mailbaogia/logo.png" alt="">
      </td>
     <td width= "350px;">
        <h3 style="margin: 0;font-size: 18px;">CÔNG TY CỔ PHẦN DẦU KHÍ MIỀN NAM</h3>
        <p style="margin: 0; line-height:25px; font-size:16px">ĐC  : 86 Nguyễn Cửu Vân, P. 17, Q. Bình Thạnh <br/>ĐT  : 08.38 409 409 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Fax: 08.6 294 7087</p>
        
      </td>
   </tr>
   <tr>
    	<td colspan="2"><img src="http://android.huongminhgroup.com/upload/temp_image/mailbaogia/line.png"  style="width:100%" width="100%" alt=""/></td>
    </tr>
    <tr>
    <td colspan="2">
    	<table width="800px">
        	<tr>
            <td  width= "350px;" style="font-family: Arial, sans-serif;font-size: 14px; font-style:italic;">Số: /2017/DKMN - BG</td>
        <td  width= "450px;" style="font-family: Arial, sans-serif;font-size: 14px; font-style:italic;" align="right">TP. Hồ Chí Minh, ngày 01 tháng 01 năm 2017</td>
            </tr>
        </table>
    </td>
    	
    </tr>
    
    <tr>
    	<td colspan="2">
        	<table width="800px">
            	<tr>
                	<td width="130px" style="font-size: 18px;font-weight: bold;">
                    Kính gửi : 
                    </td>
                    <td style=" font-size: 20px;font-weight: bold;font-style: italic;">
                   Công Ty Cổ Phần Nhựa Bình Minh 
                    </td>
                </tr>
                <tr>
                	<td width="130px" style="font-size: 18px;font-weight: bold;">
                  Người Nhận:
                    </td>
                    <td style=" font-size: 18px;font-weight: bold;font-style: italic;">
                   Chị Quỳnh
                    </td>
                </tr>
                 <tr>
                	<td width="130px" style="font-size: 18px;font-weight: bold;">
                  Số điện thoại:
                    </td>
                    <td style=" font-size: 18px;font-weight: bold;font-style: italic;">
                  0903019369 <span style="font-size:18px; margin-left:30px;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Fax</span> -
                    </td>
                </tr>
            </table>
        </td>
    </tr>
   
    
   <tr>
    	<td colspan="2"><img src="http://android.huongminhgroup.com/upload/temp_image/mailbaogia/space.png" alt=""/></td>
    </tr>
   <tr>
    	<td colspan="2" align="center"><p style="text-align: center;font-family: Arial, sans-serif; font-weight: bold;font-size: 27px;margin: 0;">BẢNG BÁO GIÁ GAS THÁNG 01/2017</p></td>
    </tr>
    <tr>
    	<td colspan="2"><img src="http://android.huongminhgroup.com/upload/temp_image/mailbaogia/space.png" alt=""/></td>
    </tr>
    <tr>
    	<td colspan="2" style="font-family: Arial, sans-serif;">
        <p style="margin-top: 0px; font-size: 16px; color: #333333; margin-bottom: 0px;line-height: 25px"><strong>Cty CP Dầu Khí Miền Nam </strong> trước tiên xin gửi tới Quý Công Ty lời cảm ơn trân trong vì sự hợp tác của Quý Công Ty. <br/>Căn cứ vào nhu cầu hoạt động của hai bên, Công ty chúng tôi trân trọng báo giá Gas tháng 01/2017 như sau: </p>
        <p style="margin-top: 0px; font-size: 16px; color: #333333; margin-bottom: 0px;line-height: 25px">1. Giá Bán : <br/>
        	<p style="margin-top: 0px; font-size: 16px; color: #333333; margin-bottom: 0px;line-height: 25px">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span style=" display: inline-block;width: 200px;">a. Bình 45kg & 50kg   : </span>       <strong> 21,281 ₫ /kg</strong></p>
            <p style="margin-top: 0px; font-size: 16px; color: #333333; margin-bottom: 0px;line-height: 25px">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span style="  display: inline-block;width: 200px;">b. Bình 12kg              : </span>       <strong>       293,000 ₫ /bình </strong></p>
        </p>
       <p style="margin-top: 0px; font-size: 16px; color: #333333; margin-bottom: 0px;line-height: 25px">2. Đơn giá: Đã bao gồm 10% thuế VAT.</p>
        <p style="margin-top: 0px; font-size: 16px; color: #333333; margin-bottom: 0px;line-height: 25px">3. Lắp đặt miễn phí hệ thống Gas & Bảo trì định kỳ miễn phí trong suốt quá trình sử dụng     Gas của Công ty chúng tôi.</p>
        <p style="margin-top: 0px; font-size: 16px; color: #333333; margin-bottom: 0px;line-height: 25px">4. Thương hiệu Gas: <strong>S.Gas & SJ Petrol</strong></p>
        <p style="margin-top: 0px; font-size: 16px; color: #333333; margin-bottom: 0px;line-height: 25px">5. Thành phần & Chất lượng gas: C3/C4: 50/50, Gas áp cao.</p>
        <p style="margin-top: 0px; font-size: 16px; color: #333333; margin-bottom: 0px;line-height: 25px">6. Bảo hiểm bình gas & hệ thống gas: 10,000,000,000 đồng (mười tỉ đồng)</p>
        <p style="margin-top: 0px; font-size: 16px; color: #333333; margin-bottom: 0px;line-height: 25px">7. Phương thức thanh toán: Theo hợp đồng ký kết giữa 2 bên.</p>
        <p style="margin-top: 0px; font-size: 16px; color: #333333; margin-bottom: 0px;line-height: 25px">8. Bản chào này có giá trị từ ngày <strong>01/01/2017</strong> đến hết ngày <strong>31/01/2017</strong>.</p>
        </td>
    </tr>
   
   <tr>
    	<td colspan="2"><img src="http://android.huongminhgroup.com/upload/temp_image/mailbaogia/space.png" alt=""/></td>
    </tr>
 
   <tr>
   	<td colspan="2" style="font-family: Arial, sans-serif;" align="right">
    	<table width="800px">
        	<tr>
            	<td width="400px" valign="top" align="left">
                 <p style="font-size:20px; margin-top:0"><strong>Xác nhận của Khách Hàng</strong></p>
                </td>
                <td width="400px" style="text-align: center; font-family: Arial, sans-serif;font-weight: bold;line-height:160%"><span style="font-size: 20px;">CÔNG TY CỔ PHẦN DẦU KHÍ MIỀN NAM</span><br/> TP.Kinh Doanh<br/>
        <img src="http://android.huongminhgroup.com/upload/temp_image/mailbaogia/congty.png" alt=""/><br/>
        <span style="font-size: 20px;padding-top: 6px;display: inline-block; line-height:160%"><strong>Phạm Văn Đức <br/>Tel: 0909875420</strong></span></td>
            </tr>
        </table>
    </td>
   </tr>
 </table>
        
</body>
</html>