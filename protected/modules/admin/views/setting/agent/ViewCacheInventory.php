<?php
$this->breadcrumbs=array(
    'Setting'=>array('index'),
);

$mAppCache      = new AppCache();
$aInventory     = $mAppCache->getCacheDecode(AppCache::APP_AGENT_INVENTORY);
$aMaterials     = $mAppCache->getMasterModel('GasMaterials', AppCache::ARR_MODEL_MATERIAL);
$aMaterialsType = $mAppCache->getMasterModel('GasMaterialsType', AppCache::ARR_MODEL_MATERIAL_TYPE);
$aAgent         = $mAppCache->getAgent();
?>
<div class="form">
<div class="buttons clear item_r">
    <a class="btn_cancel btn_closed_tickets" alert_text="Chắc chắn muốn Run SetCacheInventory?" href="<?php echo Yii::app()->createAbsoluteUrl("admin/setting", array('SetCacheInventory'=>1)); ?>">Run Set Cache Inventory Agent</a>
</div>

<h1>View Cache Inventory</h1>
<?php echo MyFormat::BindNotifyMsg(); ?>
<table class="tb hm_table f_size_15">
    <thead>
        <tr>
            <th class="item_b">Đại Lý / Mã Vật Tư</th>
            <th class="item_b">Vật Tư</th>
            <th class="item_b">Inventory</th>
        </tr>
    </thead>
    <tbody>
    <?php foreach ($aInventory as $agent_id => $aInfo1): ?>
    <tr>
        <td colspan="3">
            <table>
                <tr>
                    <td colspan="3" style="background: #00BCD4;">
                        <?php echo $aAgent[$agent_id]['code_account'].' - '. MyFunctionCustom::remove_vietnamese_accents($aAgent[$agent_id]['first_name']);?>
                    </td>
                </tr>
                <?php foreach ($aInfo1 as $materials_type_id => $aInfo2): ?>
                    <tr>
                        <td colspan="3" style="background: #9d9d9d;">
                            <?php echo isset($aMaterialsType[$materials_type_id]) ? $aMaterialsType[$materials_type_id]['name'] : '';?>
                        </td>
                    </tr>
                    <?php foreach ($aInfo2 as $materials_id => $qty): ?>
                    <tr>
                        <td class="item_c"><?php echo isset($aMaterials[$materials_id]) ? $aMaterials[$materials_id]['materials_no'] : '';?></td>
                        <td class=""><?php echo isset($aMaterials[$materials_id]) ? $aMaterials[$materials_id]['name'] : '';?></td>
                        <td class="item_c"><?php echo ActiveRecord::formatCurrency($qty);?></td>
                    </tr>
                    <?php endforeach; ?>
                <?php endforeach; ?>
            </table>
        </td>
    </tr>
    <?php endforeach; ?>
    </tbody>
</table>
</div>

<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/print-store-card.css" />