<?php
    $aData              = $model->searchReportEventMarket(); 
    $aEventMartket      = $aData['aGasEventMarket'];                       
    $LIST_MATERIAL      = !empty($aData['MATERIALS']['LIST_MATERIAL']) ? $aData['MATERIALS']['LIST_MATERIAL'] : [];
    $LIST_TYPE_MATERIAL = !empty($aData['MATERIALS']['LIST_TYPE_MATERIAL']) ? $aData['MATERIALS']['LIST_TYPE_MATERIAL'] : [];
    $DETAIL             = !empty($aData['MATERIALS']['DETAIL']) ? $aData['MATERIALS']['DETAIL'] : [];
    $TOTAL              = !empty($aData['MATERIALS']['TOTAL']) ? $aData['MATERIALS']['TOTAL'] : [];
    $TOTAL_IMPORT       = !empty($aData['MATERIALS']['TOTAL_IMPORT']) ? $aData['MATERIALS']['TOTAL_IMPORT'] : 0;
    $TOTAL_EXPORT       = !empty($aData['MATERIALS']['TOTAL_EXPORT']) ? $aData['MATERIALS']['TOTAL_EXPORT'] : 0;
    $mGasEventMarketCal = new GasEventMarket();
    $totalAmount        = $totalDiff = 0;
?>
<?php
$this->breadcrumbs=array(
    $this->singleTitle,
);
Yii::app()->clientScript->registerScript('search', "
    $('.search-button').click(function(){
            $('.search-form').toggle();
            return false;
    });
    $('.search-form form').submit(function(){
            $.fn.yiiGridView.update('app-text-grid', {
                    url : $(this).attr('action'),
                    data: $(this).serialize()
            });
            return false;
    });
");
?>
<h1><?php echo $this->pageTitle; ?></h1>
<?php echo CHtml::link('Tìm Kiếm Nâng Cao','#',array('class'=>'search-button')); ?>
<div class="search-form" style="">
<?php $this->renderPartial('reportCostEvent/_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->
<div class="grid-view">
    <table class="tb hm_table" style="width:100%" >
        <thead>
            <tr style="">
                <th rowspan="2" class="w-20">STT</th>
                <th rowspan="2" class="w-150">Tên chợ/Siêu thị</th>
                <th rowspan="2" class="w-150">Đại lý</th>
                <th rowspan="2" class="w-100">Ngày bắt đầu</th>
                <th rowspan="2" class="w-100">Ngày kết thúc</th>
                <th rowspan="2">Số App</th>
                <th rowspan="2">BQV</th>
                <th rowspan="2">Tỉ lệ</th>
                <?php foreach ($LIST_TYPE_MATERIAL as $key_type => $name_type):?>
                <?php 
                    $aMaterialInType = isset($LIST_MATERIAL[$key_type]) ? $LIST_MATERIAL[$key_type] : [];
                ?>
                <th colspan="<?php echo count($aMaterialInType) + 1; ?>" rowspan="<?php echo !empty($aMaterialInType) ? 1 : 2; ?>"><?php echo $name_type;?></th>
                <?php endforeach;?>
                <th rowspan="2">Chênh lệch quà - app</th>
                <th rowspan="2">Tiền chênh lệch</th>
            </tr>
            <tr>
                <?php foreach ($LIST_TYPE_MATERIAL as $key_type => $name_type):?>
                <?php 
                    $aMaterialInType = isset($LIST_MATERIAL[$key_type]) ? $LIST_MATERIAL[$key_type] : [];
                    if(empty($aMaterialInType)):
                        continue;
                    endif;
                ?>
                <?php foreach ($aMaterialInType as $material_id => $material_name):?>
                    <th class="item_c item_b"><?php echo $material_name;?></th>
                <?php endforeach;?>
                <th class="item_c item_b">Tổng</th>
                <?php endforeach;?>
            </tr>
        </thead>
        <tbody>
            <tr id="headerTable"></tr>
        <?php 
            $index = 1;
        ?>
        <?php foreach($aEventMartket as $key =>$mGasEventMarket):
            $countApp       = !empty($aData['APP'][$mGasEventMarket->id]) ? $aData['APP'][$mGasEventMarket->id] : 0;
            $countBQV       = !empty($aData['BQV'][$mGasEventMarket->id]) ? $aData['BQV'][$mGasEventMarket->id] : 0;
            $totalImport    = !empty($TOTAL_IMPORT[$mGasEventMarket->id]) ? $TOTAL_IMPORT[$mGasEventMarket->id] : 0;
            $totalExport    = !empty($TOTAL_EXPORT[$mGasEventMarket->id]) ? $TOTAL_EXPORT[$mGasEventMarket->id] : 0;
            $diff           = ($totalExport -  $totalImport - $countApp) > 0 ? ($totalExport -  $totalImport - $countApp) : 0;
            $amountDiff     = $diff * ($mGasEventMarket->getAverageMaterial());
            $totalAmount    += $amountDiff;
            $totalDiff      += $diff;
        ?>
            <tr>
                <td class="item_c order_no"><?php echo $index++;?></td>
                <td class="item_c"><?php echo $mGasEventMarket->getTitle();?></td>
                <td class="item_c"><?php echo $mGasEventMarket->getFormatJson($mGasEventMarket->agents);?></td>
                <td class="item_c"><?php echo MyFormat::dateConverYmdToDmy($mGasEventMarket->start_date, "d/m/Y H:i");?></td>
                <td class="item_c"><?php echo MyFormat::dateConverYmdToDmy($mGasEventMarket->end_date, "d/m/Y H:i");?></td>
                <td class="item_c"><?php echo $countApp > 0 ? $countApp : '';?></td>
                <td class="item_c"><?php echo $countBQV > 0 ? $countBQV : '';?></td>
                <td class="item_c"><?php echo $mGasEventMarket->getRatioBqvApp($countApp,$countBQV).'%';?></td>
                <?php foreach ($LIST_TYPE_MATERIAL as $key_type => $name_type):?>
                <?php 
                    $aMaterialInType = isset($LIST_MATERIAL[$key_type]) ? $LIST_MATERIAL[$key_type] : [];
                    if(empty($aMaterialInType)): ?>
                        <td></td>
                        <?php
                            continue;
                        endif;
                    ?>
                    <?php foreach ($aMaterialInType as $material_id => $material_name):?>
                        <td class="item_c"><?php echo !empty($DETAIL[$mGasEventMarket->id][$key_type][$material_id]) ? $DETAIL[$mGasEventMarket->id][$key_type][$material_id] : ''; ?></td>
                    <?php endforeach;?>
                    <td class="item_c"><?php echo !empty($DETAIL[$mGasEventMarket->id][$key_type]) ? array_sum($DETAIL[$mGasEventMarket->id][$key_type]) : ''; ?></td>
                <?php endforeach;?>
                <td class="item_c"><?php echo $diff > 0 ? $diff : '';?></td>
                <td class="item_c"><?php echo $amountDiff > 0 ? ActiveRecord::formatCurrency($amountDiff) : '';?></td>
            </tr>
        <?php endforeach;?>
            <tr id="footerTable">
                <?php 
                    $countApp = !empty($aData['APP']) ? array_sum($aData['APP']) : 0;
                    $countBqv = !empty($aData['BQV']) ? array_sum($aData['BQV']) : 0;
                ?>
                <td class="item_r item_b" colspan="5">Tổng</td>
                <td class="item_c item_b"><?php echo $countApp;?></td>
                <td class="item_c item_b"><?php echo $countBqv;?></td>
                <td class="item_c item_b"><?php echo $mGasEventMarketCal->getRatioBqvApp($countApp,$countBqv);?>%</td>
                <?php foreach ($LIST_TYPE_MATERIAL as $key_type => $name_type):?>
                <?php 
                    $aMaterialInType = isset($LIST_MATERIAL[$key_type]) ? $LIST_MATERIAL[$key_type] : [];
                    if(empty($aMaterialInType)): ?>
                        <td></td>
                        <?php
                            continue;
                        endif;
                    ?>
                    <?php foreach ($aMaterialInType as $material_id => $material_name):?>
                        <td class="item_c item_b"><?php echo !empty($TOTAL[$key_type][$material_id]) ? $TOTAL[$key_type][$material_id] : 0; ?></td>
                    <?php endforeach;?>
                        <td class="item_c item_b"><?php echo !empty($TOTAL[$key_type]) ? array_sum($TOTAL[$key_type]) : 0; ?></td>
                <?php endforeach;?>
                <td class="item_c item_b"><?php echo $totalDiff;?></td>
                <td class="item_c item_b"><?php echo ActiveRecord::formatCurrency($totalAmount);?></td>
            </tr>
        </tbody>
    </table>
</div>
<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/print-store-card.css" />
<script>
     $(document).ready(function(){
        $('#headerTable').html($('#footerTable').html());
        $('.form').find('button:submit').click(function(){        
            $.blockUI({ overlayCSS: { backgroundColor: '#fff' } }); 
        });   
        $('.export_excel').click(function(){
                window.location = $(this).attr('next');
        });  
    }); 
</script>