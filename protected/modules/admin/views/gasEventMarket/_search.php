<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>
    
<!--     <div class="row">
        <?php // echo $form->label($model,'title',array()); ?>
        <?php // echo $form->textField($model, 'title', array('class' => 'w-200', 'maxlength' => 20)); ?>
    </div>-->
     <div class="row more_col">
        <div class="col1">
                <?php echo $form->label($model,'status',array()); ?>
                <?php echo $form->dropDownList($model,'status',  $model->getArrayStatus() ,array('class'=>'w-200','empty'=>'Select')); ?>
        </div>

        <div class="col2">
                    <?php echo $form->label($model,'type',array()); ?>
                    <?php echo $form->dropDownList($model,'type',  $model->getArrayType() ,array('class'=>'w-200','empty'=>'Select')); ?>
        </div>
     </div>
    <div class="row more_col">
        <div class="col1">
           <?php echo $form->labelEx($model,'province_id'); ?>
           <?php echo $form->dropDownList($model,'province_id', GasProvince::getArrAll(),array('style'=>'','class'=>'w-200','empty'=>'Select')); ?>
        </div> 	

        <div class="col2">
               <?php echo $form->labelEx($model,'district_id'); ?>
               <?php echo $form->dropDownList($model,'district_id', GasDistrict::getArrAll($model->province_id),array('class'=>'w-200', 'empty'=>"Select")); ?>
        </div>
    
    
    </div>
   
    
    <div class="row more_col">
            <div class =""><?php echo $form->label($model,'start_date'); ?></div>
            <div class="col2">
                    <?php // echo Yii::t('translation', $form->label($model,'start_date_from')); ?>
                    <?php 
                        $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                            'model'=>$model,        
                            'attribute'=>'start_date_from',
                            'options'=>array(
                                'showAnim'=>'fold',
                                'dateFormat'=> MyFormat::$dateFormatSearch,
    //                            'minDate'=> '0',
//                                'maxDate'=> '0',
                                'changeMonth' => true,
                                'changeYear' => true,
                                'showOn' => 'button',
                                'buttonImage'=> Yii::app()->theme->baseUrl.'/admin/images/icon_calendar_r.gif',
                                'buttonImageOnly'=> true,                                
                            ),        
                            'htmlOptions'=>array(
                                'class'=>'w-16',
                                'size'=>'16',
                                'style'=>'float:left;',                               
                            ),
                        ));
                    ?>     		
            </div>
            <div class="col3">
                    <?php echo Yii::t('translation', $form->label($model,'start_date_to')); ?>
                    <?php 
                        $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                            'model'=>$model,        
                            'attribute'=>'start_date_to',
                            'options'=>array(
                                'showAnim'=>'fold',
                                'dateFormat'=> MyFormat::$dateFormatSearch,
    //                            'minDate'=> '0',
//                                'maxDate'=> '0',
                                'changeMonth' => true,
                                'changeYear' => true,
                                'showOn' => 'button',
                                'buttonImage'=> Yii::app()->theme->baseUrl.'/admin/images/icon_calendar_r.gif',
                                'buttonImageOnly'=> true,                                
                            ),        
                            'htmlOptions'=>array(
                                'class'=>'w-16',
                                'size'=>'16',
                                'style'=>'float:left;',
                            ),  
                        ));
                    ?>     		
            </div>
        </div>
	<div class="row buttons" style="padding-left: 159px;">
		        <?php $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType'=>'submit',
            'label'=>'Search',
            'type'=>'null', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
            'size'=>'small', // null, 'large', 'small' or 'mini'
            //'htmlOptions' => array('style' => 'margin-bottom: 10px; float: right;'),
        )); ?>	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->
<script>
    fnSelectProvinceDistrict('#GasEventMarket_province_id', '#GasEventMarket_district_id' , "<?php echo Yii::app()->createAbsoluteUrl('admin/ajax/get_slt_district');?>");
</script>