<?php
$this->breadcrumbs=array(
	$this->singleTitle,
);

$menus=array(
            array('label'=>"Create $this->singleTitle", 'url'=>array('create')),
);
$this->menu= ControllerActionsName::createMenusRoles($menus, $actions);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('gas-event-market-grid', {
                url : $(this).attr('action'),
		data: $(this).serialize()
	});
	return false;
});
$('#gas-event-market-grid a.checkOk').live('click', function() {
    if(confirm('Xác nhận chi tiền?')){
        $.fn.yiiGridView.update('gas-event-market-grid', {
            type: 'POST',
            url: $(this).attr('href'),
            success: function() {
                $.fn.yiiGridView.update('gas-event-market-grid');
            }
        });
    }
    return false;
});
$('#gas-event-market-grid a.reloadEvent').live('click', function() {
    if(confirm('Cập nhật thông tin tải app?')){
        $.fn.yiiGridView.update('gas-event-market-grid', {
            type: 'POST',
            url: $(this).attr('href'),
            success: function() {
                $.fn.yiiGridView.update('gas-event-market-grid');
            }
        });
    }
    return false;
});
");
?>
<style type="text/css">
    .reloadEvent img{
        width: 25px!important;
    }
</style>
<h1>Danh Sách <?php echo $this->pluralTitle; ?></h1>
<?php echo CHtml::link('Tìm Kiếm Nâng Cao','#',array('class'=>'search-button')); ?>
<div class="search-form">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>

</div><!-- search-form -->
<div class="form">
    <?php $form=$this->beginWidget('CActiveForm', array(
        'id'=>'telesale-form',
        'enableAjaxValidation'=>false,
        'htmlOptions' => array('enctype' => 'multipart/form-data'),
        'action'=>Yii::app()->createAbsoluteUrl('admin/gasEventMarket/index', ['ToExcel'=>1]),
    )); ?>
<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'gas-event-market-grid',
	'dataProvider'=>$model->search(),
        'afterAjaxUpdate'=>'function(id, data){ fnUpdateColorbox();}',
        'template'=>'{pager}{summary}{items}{pager}{summary}',
//    'itemsCssClass' => 'items custom_here',
//    'htmlOptions'=> array('class'=>'grid-view custom_here'),
        'pager' => array(
            'maxButtonCount'=>  CmsFormatter::$PAGE_MAX_BUTTON,
        ),
	'enableSorting' => false,
	//'filter'=>$model,
	'columns'=>array(
                array(
                    'header' => 'S/N',
                    'type' => 'raw',
                    'value' => '$this->grid->dataProvider->pagination->currentPage * $this->grid->dataProvider->pagination->pageSize + ($row+1)',
                    'headerHtmlOptions' => array('width' => '10px','style' => 'text-align:center;'),
                    'htmlOptions' => array('style' => 'text-align:center;')
                ),
                array(
                    'class'=>'CCheckBoxColumn',
                    'selectableRows'=>2,
                    'id'=>'roadShowRow',
                    'cssClassExpression'=>'$data->canExcel();',
                ),
                array(
                        'name' => 'type',
                        'type' => 'html',
                        'value' => '$data->getType()',
                        'htmlOptions' => array('style' => 'text-align:center;'),
                ),
                array(
                    'name' => 'title',
                    'type' => 'html',
                    'value' => '$data->getTitle()',
                    ), 
                array(
                    'name' => 'monitor_id',
                    'type' => 'html',
                    'value' => '$data->getMonitor()',
                    ), 
                array(
                    'name' => 'employees',
                    'type' => 'html',
                    'value' => '$data->getFormatJson($data->employees)',
                    'htmlOptions' => array('style' => 'text-align:center;'),
                    ),
		array(
                    'name' => 'agents',
                    'type' => 'html',
                    'value' => '$data->getFormatJson($data->agents)',
                    'htmlOptions' => array('style' => 'text-align:center;'),
                    ),
                array(
                    'name'=> 'start_date',
                    'type' => 'html',
                    'value' => 'MyFormat::dateConverYmdToDmy($data->start_date, "d/m/Y H:i")',
                    'htmlOptions' => array('style' => 'text-align:center;'),
                    ),
                array(
                    'name' => 'end_date',
                    'type' => 'html',
                    'value' => 'MyFormat::dateConverYmdToDmy($data->end_date, "d/m/Y H:i")',
                    'htmlOptions' => array('style' => 'text-align:center;'),
                    ),
                array(
                    'name' => 'amount',
                    'type' => 'html',
                    'value' => '$data->getAmount()',
                    'htmlOptions' => array('style' => 'text-align:center;'),
                    ), 
                array(
                    'name' => 'note',
                    'type' => 'html',
                    'value' => '$data->getNote()',
                    'htmlOptions' => array('style' => 'text-align:center;'),
                    ),  
                array(
                    'name' => 'status',
                    'type' => 'html',
                    'value' => '$data->getStatus()',
                    'htmlOptions' => array('style' => 'text-align:center;'),
                    ),
                array(
                        'header' => 'Tỉnh',
                        'type' => 'html',
                        'value' => '$data->getDisTrict() .", ". $data->getProvince()',
                    ),
                array(
                        'header' => 'App',
                        'type' => 'html',
                        'value' => '$data->getCountApp()',
                    ),
                array(
                        'header' => 'BQV',
                        'type' => 'html',
                        'value' => '$data->getCountBqv()',
                    ),
                array(
                        'name' => 'created_date',
                        'type' => 'Datetime',
                        'htmlOptions' => array('style' => 'text-align:center;'),
                    ),
                array(
                    'name' => 'created_by',
                    'type' => 'html',
                    'value' => '$data->getCreatedBy()',
                    'htmlOptions' => array('style' => 'text-align:center;'),
                    ),
                array(
                    'header'    => 'Actions',
                    'class'     => 'CButtonColumn',
                    'template'  => ControllerActionsName::createIndexButtonRoles($actions, array('view','update','delete','viewRoadShow','createDetailMaterial','payMoney','reloadEvent')),
                    'buttons'   => array(
                        'viewRoadShow' => array(
                            'label'     => 'Chi Tiết Checkin',     //Text label of the button.
                            'url'       => '["viewRoadShow", "id"=>$data->id, "type"=>1]',  
                            'imageUrl'  => Yii::app()->theme->baseUrl . '/admin/images/view.png',
                            'options'   => array('class' => 'detail_road_show'), //HTML options for the button tag..
                            'visible'   => '$data->canViewDetailRoadShow()',
                        ),
                        'update'=>array(
                            'visible'=> '$data->canUpdateData()',
                        ),
                        'delete'=>array(
                            'visible'=> '$data->canDeleteData()',
                        ),
                        'createDetailMaterial'=>array(
                            'label'     => 'Cập nhật vật tư',
                            'url'       => '["createDetailMaterial", "id"=>$data->id]',  
                            'imageUrl'  => Yii::app()->theme->baseUrl . '/images/add2.png',
                            'options'   => array('class' => 'detail_road_show'), //HTML options for the button tag..
                            'visible'   => '$data->canViewMaterial()',
                        ),
                        'payMoney'=>array(
                            'label'     => 'Xác nhận chi tiền',
                            'url'       => '["payMoney", "id"=>$data->id]',  
                            'imageUrl'  => Yii::app()->theme->baseUrl . '/images/dollar.png',
                            'options'   => array('class' => 'checkOk'), //HTML options for the button tag..
                            'visible'   => '$data->canPayMoney()',
                        ),
                        'reloadEvent'=>array(
                            'label'     => 'Cập nhật thông tin tải app',
                            'url'       => '["reloadEvent", "id"=>$data->id]',  
                            'imageUrl'  => Yii::app()->theme->baseUrl . '/images/loading.gif',
                            'options'   => array('class' => 'reloadEvent'), //HTML options for the button tag..
                            'visible'   => '$data->canReload()',
                        ),
                    ),
                    ),
                ),
)); 
?>
<div class="row buttons" style="padding-left: 10px;">
    <?php $this->widget('bootstrap.widgets.TbButton', array(
        'buttonType'=>'submit',
        'label'=>'Xuất Excel',
        'type'=>'null', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
        'size'=>'small', // null, 'large', 'small' or 'mini'
        //'htmlOptions' => array('style' => 'margin-bottom: 10px; float: right;'),
    )); ?>	
</div>
<?php $this->endWidget(); ?>
</div>
<script>
$(document).ready(function() {
    fnUpdateColorbox();
});

function fnUpdateColorbox(){
    fnShowhighLightTr();
    fixTargetBlank();
    fnViewDetailRoadShow();
    $(".view").colorbox({
        iframe:true,
        innerHeight:'1000', 
        innerWidth: '1100', escKey:false,close: "<span title='close'>close</span>"
    });
    $('.isDisplayNone').find('input').attr('disabled',true);
}
function fnViewDetailRoadShow(){
    fnShowhighLightTr();
    fixTargetBlank();
    $(".detail_road_show").colorbox({
        iframe:true,
        innerHeight:'700',
        innerWidth: '1100',
        escKey:false,
        close: "<span title='close'>close</span>"
    });
}
</script>
