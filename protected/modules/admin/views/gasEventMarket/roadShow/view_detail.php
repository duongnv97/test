<?php $this->breadcrumbs=array(
    $this->pageTitle,
); ?>
<?php if($model->type == GasEventMarket::TYPE_ROADSHOW): ?>
<?php $urlExcel = Yii::app()->createAbsoluteUrl("admin/gasEventMarket/ViewRoadShow", array('id' => $id,'ToExcel'=>1)) ?>
<h1><?php echo $this->pageTitle;?></h1>
<div class="form" style="padding: 0 0 30px 0">
    <a class='btn_cancel' href='<?php echo $urlExcel; ?>' style="text-decoration: none; float: right;">Xuất Excel</a>
</div>
<?php endif; ?>
<?php echo MyFormat::BindNotifyMsg(); ?>
<?php if(!empty($data)): ?>
<?php 
$gridCss = Yii::app()->getAssetManager()->publish(Yii::getPathOfAlias('zii.widgets.assets')) . '/gridview/styles.css';
Yii::app()->getClientScript()->registerCssFile($gridCss);
$data1           = $data[$model->id]['aData'];
$aEmployeeAccept = $data[$model->id]['employeeAccept'];
$i               = 1;
?>
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/scroll/jquery.freezetablecolumns.1.1.js"></script>
<div class="grid-view " id="report-grid">
<table class="items work_report_table sortable" >
    <thead>
        <tr>
            <th class="w-20">STT</th>
            <th class="w-20"><input type="checkbox" name="roadShowRow_all" id="roadShowRow_all"></th>
            <th>Nhân viên</th>
            <th>SĐT</th>
            <th>Lần checkin</th>
            <th>Hình ảnh</th>
            <th>Thời gian</th>
            <th>Trạng thái</th>
            <th class="w-100">Hành động</th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($data1 as $employeeId => $aData): ?>
            <?php
                $high_light     = '';
                $aWorkReport    = isset($aData['data']) ? $aData['data'] : [];
                $cWorkReport    = count($aWorkReport);
                if (in_array($employeeId, $aEmployeeAccept)) {
                    $high_light = 'high_light_tr_custom';
                }
            ?>
        <tr style="text-align: center" class='<?php echo $high_light ?>'>
                <td rowspan="<?php echo $cWorkReport + 1; ?>">
                    <?php echo $i++ ?>
                </td>
                <td rowspan="<?php echo $cWorkReport + 1; ?>">
                    <?php if($cWorkReport >= GasEventMarket::COUNT_APPLY_MONEY): ?>
                    <input data-class="employee_row_<?php echo $employeeId;?>" class="employee_row" type="checkbox" value="<?php echo $employeeId;?>" name="employeeRoadShow[]">
                    <?php endif; ?>
                </td>
                <td rowspan="<?php echo $cWorkReport + 1; ?>">
                    <?php echo $aData['employee_name']; ?>
                </td>
                <td rowspan="<?php echo $cWorkReport + 1; ?>">
                    <?php echo $aData['pay_salary_phone']; ?>
                </td>
                <td></td>
                <td></td>
                <td></td>
                <td rowspan="<?php echo $cWorkReport + 1; ?>">
                    <?php echo in_array($employeeId, $aEmployeeAccept) ? 'Đã duyệt' : 'Chưa duyệt'; ?>
                </td>
                <td rowspan="<?php echo $cWorkReport + 1; ?>">
                    <?php if ($cWorkReport >= GasEventMarket::COUNT_APPLY_MONEY && $model->canUpdateStatus()): ?>
                       <form method="post" action="<?php echo Yii::app()->createAbsoluteUrl("admin/gasEventMarket/viewRoadShow", array('id'=>$id)) ?>">
                            <input type="hidden" name="employee_id" value="<?php echo $employeeId; ?>">
                            <input type="hidden" name="event_market_id" value="<?php echo $id; ?>">
                            <?php if (!in_array($employeeId, $aEmployeeAccept)): ?>
                                <input type="hidden" name="status" value="<?php echo GasEventMarketDetail::STATUS_ACCEPT ?>">
                                <button type="submit" name="btn_submit" class="button-image" title='Xác nhận'>
                                    <img src="<?php echo Yii::app()->theme->baseUrl . '/admin/images/select.png' ?>">
                                </button>
                            <?php else: ?>
                                <input type="hidden" name="status" value="<?php echo GasEventMarketDetail::STATUS_NO_ACCEPT ?>">
                                <button type="submit" name="btn_submit" class="button-image" title='Hủy'>
                                    <img src="<?php echo Yii::app()->theme->baseUrl . '/admin/images/delete.png' ?>">
                                </button>
                            <?php endif; ?>
                       </form>
                    <?php endif; ?>
                </td>
            </tr>
            <?php if ($cWorkReport > 0): ?>
                <?php foreach ($aWorkReport as $item => $value): ?>
                    <?php $times = $item + 1; ?>
                    <tr style="text-align: center" class='<?php echo $high_light ?>'>
                        <td>
                            <a target="_blank" href="<?php echo Yii::app()->createUrl("admin/workReport/view", array("id"=>$value['id'])) ?>">
                                <?php echo 'Lần ' . $times . ' - ' . $value['code_no']; ?>
                            </a>
                        </td>
                        <td>
                            <?php echo $value['file']; ?>
                        </td>
                        <td><?php echo MyFormat::dateConverYmdToDmy($value['created_date'], 'd/m/Y H:i'); ?></td>
                    </tr>
                <?php endforeach; ?>
            <?php endif; ?>
        <?php endforeach; ?>
    </tbody>
</table>
    <div class="form">
    <?php if($model->canUpdateStatus()): ?>
    <form id="form_submit_all" method="post" action="<?php echo Yii::app()->createAbsoluteUrl("admin/gasEventMarket/viewRoadShow", array('id'=>$id)) ?>">
        <input type="hidden" name="GasEventMarket[submitMore]" value="1">
        <div class="row buttons" style="padding-left: 10px;">  
        <?php $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType'=>'submit',
            'label'=>'Duyệt',
            'id'   => 'submit_all',
            'type'=>'null', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
            'size'=>'small', // null, 'large', 'small' or 'mini'
        )); ?>
        </div>
    </form>
    <?php endif; ?>
    </div>
</div>
<?php endif; ?>

<script>
$(document).ready(function() {
    fnAddClassOddEven('items');
    fnUpdateColorbox();
});
function changeChecked(){
    $('.employee_row').each(function(){
        //        thực hiện thêm hoặc xóa input trong form submit
        $class = $(this).data('class');
        $value = $(this).val();
        $classSearch = 'input.'+$class;
        if(this.checked == true){
            $countCurrent = $($classSearch).length;
            if($countCurrent <= 0){
                $('#form_submit_all').append('<input class="'+ $class +'" type="hidden" name="GasEventMarket[emloyeeApprove][]" value="'+ $value +'">');
            }
        }else{
            $('#form_submit_all').find($classSearch).remove();
        }
    });
}

function fnUpdateColorbox(){   
    fixTargetBlank();
    $("#roadShowRow_all").click(function(){
        $('input:checkbox:enabled[name="employeeRoadShow[]"]').not(this).prop('checked', this.checked);
        changeChecked();
    });
    $(".employee_row").on('change',function(){
        changeChecked();
        $countInput = $('input.employee_row:checked').length;
        $countInputAll = $('input.employee_row').length;
        if($countInput < $countInputAll){
            $('#roadShowRow_all').prop('checked', false); 
        }
        if($countInputAll > 0 && $countInput == $countInputAll){
            $('#roadShowRow_all').prop('checked', true); 
        }
    });
    $('#submit_all').click(function(){
        
    });
}
</script>
<style>
    .sortable thead th:hover {
        cursor: pointer;
    }
    
    .high_light_tr_custom {
        background: #95d67f !important;
    }
    
    .button-image {
        background: none;
        border: none;
        cursor: pointer;
    }
</style>