<?php
$this->breadcrumbs=array(
	'Quản Lý Đi Chợ'=>array('index'),
//	$model->code_no=>array('view','id'=>$model->id),
	'Cập Nhật',
);

$menus = array(	
        array('label'=>'Quản Lý Đi Chợ', 'url'=>array('index')),
	array('label'=>'Xem Đi Chợ', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Tạo Mới Đi Chợ', 'url'=>array('create')),	
);
$this->menu= ControllerActionsName::createMenusRoles($menus, $actions);

?>

<h1>Cập Nhật Đi Chợ: </h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>