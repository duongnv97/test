<?php
$this->breadcrumbs=array(
	'Quản Lý '=>array('index'),
	
);

$menus = array(
	array('label'=>'Quản Lý ', 'url'=>array('index')),
	array('label'=>'Tạo Mới ', 'url'=>array('create')),
	array('label'=>'Cập Nhật', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Xóa ', 'url'=>array('delete'), 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Bạn chắc chắn muốn xóa ?')),
);
$this->menu= ControllerActionsName::createMenusRoles($menus, $actions);
?>

<h1>Xem </h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
               array(
                        'name' => 'type',
                        'type' => 'raw',
                        'value' => $model->getType(),
                ),
                array(
                    'name' => 'title',
                    'type' => 'raw',
                    'value' => $model->getTitle(),
                ),  
		array(
                    'name'=>'employees',
                    'type'=>'raw',
                    'value'=>$model->getFormatJson($model->employees),
                ),
                array(
                    'name'=>'agents',
                    'type'=>'raw',
                    'value'=>$model->getFormatJson($model->agents),
                ),
                array(
                        'name'=>'start_date',
                        'type'=>'raw',
                        'value'=>$model->formatDateForView($model->start_date),
                    ),
                array(
                        'name'=>'end_date',
                        'type'=>'raw',
                        'value'=>$model->formatDateForView($model->end_date),
                    ),
                'note' ,
                array(
                    'name'=>'status',
                    'type'=>'raw',  
                    'value'=>$model->getStatus(),
                ),
                array(
                           'name'=>'created_date',
                           'type'=>'DateTime',
                           
                       ),
                array(
                        'name' => 'Tỉnh',
                        'type' => 'raw',
                        'value' => $model->getDisTrict().", ".$model->getProvince(),
                ),
//                'created_by' ,
	),
)); ?>

<script>
   $(window).load(function(){ 
        $('.form').find('button:submit').click(function(){
            $.blockUI({ overlayCSS: { backgroundColor: '#fff' } }); 
        });
        
        fnResizeColorbox();
        parent.$.fn.yiiGridView.update("gas-support-customer-grid"); 
    });
    
    function fnResizeColorbox(){
//        var y = $('body').height()+100;
        var y = $('#main_box').height()+100;
        parent.$.colorbox.resize({innerHeight:y});
    }
    $(document).ready(function(){
        $('.form').find('button:submit').click(function(){
            $.blockUI({ overlayCSS: { backgroundColor: '#fff' } }); 
        });        
    });
</script>


