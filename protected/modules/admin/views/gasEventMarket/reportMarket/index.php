<?php $this->breadcrumbs=array(
    $this->pageTitle,
); ?>

<h1><?php echo $this->pageTitle;?></h1>
<div class="search-form" style="">
<?php $this->renderPartial('reportMarket/_search',array(
        'model'=>$model,
)); 
?>
</div><!-- search-form -->
<?php if(!empty($data['OUT_PUT'])): ?>
<?php 
$gridCss = Yii::app()->getAssetManager()->publish(Yii::getPathOfAlias('zii.widgets.assets')) . '/gridview/styles.css';
Yii::app()->getClientScript()->registerCssFile($gridCss);
$session=Yii::app()->session;

$OUT_PUT          = $data['OUT_PUT']; 
$INFO             = $data['INFO']; 
$EMPLOYEE         = $data['EMPLOYEE']; 
$Uid = $data['aUid'];
$aName = Users::getArrObjectUserByRole('', $Uid);
$aUid = CHtml::listData($aName, 'id', 'first_name'); //get array name uid login
?>
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/scroll/jquery.freezetablecolumns.1.1.js"></script>
<div class="grid-view " id="report-grid">
<table class="items materials_table sortable" >
    <thead>
        <tr>
            <th class="w-20">STT</th>
            <th class="w-250">Tiêu đề</th>
            <th class="w-150">Ngày bắt đầu</th>
            <th class="w-150">Ngày kết thúc</th>
            <th >Số App</th>
            <th >BQV</th>
        </tr>
    </thead>
    <tbody>
     <?php $stt = 1;?>
        <?php foreach ($OUT_PUT as $market_id => $market): ?>
        <tr class="odd" value="<?php echo $market_id?>">
            <?php $title         = isset($INFO[$market_id]['TITLE']) ? $INFO[$market_id]['TITLE'] : ""; ?>
            <?php $start_date    = isset($INFO[$market_id]['START_DATE']) ? $INFO[$market_id]['START_DATE'] : ""; ?>
            <?php $end_date      = isset($INFO[$market_id]['END_DATE']) ? $INFO[$market_id]['END_DATE'] : ""; ?>
            <?php $start_day     = isset($INFO['app']) ? $INFO['app'] : ""; ?>
            <?php $app           = isset($market['app']) ? $market['app'] : "0"; ?>
            <?php $bqv           = isset($market['bqv']) ? $market['bqv'] : "0"; ?>
            <td class="w-20 item_c order_no"><?php echo $stt++;?></td>
            <td class="w-250 "><?php echo $title;?></td>
            <td class="item_c "><?php echo MyFormat::dateConverYmdToDmy($start_date);?></td>
            <td class="item_c "><?php echo MyFormat::dateConverYmdToDmy($end_date);?></td>
            <td class="item_c"><?php echo $app?></td>
            <td class="item_c"><?php echo $bqv?></td>
        </tr>
            <?php foreach ($EMPLOYEE[$market_id] as $employee_id => $employee): ?>
                <tr class="odd market<?php echo $market_id?>" style="display: none">
                    <?php $name     = isset($aUid[$employee_id]) ? $aUid[$employee_id] : ""; ?>
                    <?php $employee_app           = isset($employee['app']) ? $employee['app'] : "0"; ?>
                    <?php $employee_bqv           = isset($employee['bqv']) ? $employee['bqv'] : "0"; ?>
                    <td class="w-20 item_c order_no"><?php echo '';?></td>
                    <td class="w-250 "><?php echo $name;?></td>
                    <td class="item_c "><?php echo '';?></td>
                    <td class="item_c "><?php echo ''?></td>
                    <td class="item_c"><?php echo $employee_app?></td>
                    <td class="item_c"><?php echo $employee_bqv?></td>
                </tr>
            <?php endforeach; ?>
            
        <?php endforeach; ?>
    </tbody>
</table>
</div>        
<?php endif; ?>

<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/print-store-card.css" />
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/jquery.floatThead.min.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/underscore-min.js"></script>    

<?php 
$gridCss = Yii::app()->getAssetManager()->publish(Yii::getPathOfAlias('zii.widgets.assets')) . '/gridview/styles.css';
Yii::app()->getClientScript()->registerCssFile($gridCss);
?>
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/sortElements/jquery.sortElements.js"></script>
<script>
$(document).ready(function() {
//    fnAddClassOddEven('items');
    fnUpdateColorbox();
    clickFunction();
});

function fnUpdateColorbox(){   
    fixTargetBlank();
}
function clickFunction(on){
   $("tr").click(function(){
        var cls  = $(this).attr("value");
        cls = "market"+cls;
        var lst = document.getElementsByClassName(cls);
        for(var i = 0; i < lst.length; ++i) {
            if (lst[i].style.display === 'none') {
            lst[i].style.display = '';
            }
            else {
                lst[i].style.display = 'none';    
            }
        }
   });
}

</script>
<style>
    .sortable thead th:hover {cursor: pointer;}
</style>