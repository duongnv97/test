<div class="form">
<?php
$form=$this->beginWidget('CActiveForm', array(
	'id'=>'gas-event-market-form',
	'enableAjaxValidation'=>false,
        'htmlOptions' => array('enctype' => 'multipart/form-data'),
)); ?>
        <?php if(!$model->isNewRecord):?>
        <?php // include '_link_view.php';?>
        <?php endif;?>
	<p class="note">Dòng có dấu <span class="required">*</span> là bắt buộc.</p>
        
        <?php  if(Yii::app()->user->hasFlash('successUpdate')): ?>
            <div class='success_div'><?php echo Yii::app()->user->getFlash('successUpdate');?></div>         
        <?php endif; ?>  	
        <?php echo $form->errorSummary($model); ?>

        <div class="row">
            <?php echo $form->labelEx($model, 'title'); ?>
            <?php echo $form->textField($model, 'title',array('style'=>'','class'=>'w-600')); ?>
            <?php echo $form->error($model, 'title'); ?>
        </div>

        <div class="row">
            <?php echo $form->labelEx($model, 'type'); ?>
            <?php echo $form->dropDownList($model, 'type', $model->getArrayType()); ?>
            <?php echo $form->error($model, 'type'); ?>
        </div>    
            
        <div class="row more_col">
            <div class="col1">
                <?php echo $form->labelEx($model,'province_id'); ?>
                <?php echo $form->dropDownList($model,'province_id', GasProvince::getArrAll(),array('style'=>'','class'=>'w-200','empty'=>'Select')); ?>
                <?php echo $form->error($model,'province_id'); ?>
            </div> 	

            <div class="col2">
                <?php echo $form->labelEx($model,'district_id'); ?>
                <?php echo $form->dropDownList($model,'district_id', GasDistrict::getArrAll($model->province_id),array('class'=>'w-200', 'empty'=>"Select")); ?>
                <?php echo $form->error($model,'district_id'); ?>
            </div>
        </div>  
            
        <div class="row">
            <?php echo $form->labelEx($model,'monitor_id'); ?>
            <?php echo $form->hiddenField($model,'monitor_id', array('class'=>'')); ?>
            <?php
                // 1. limit search kh của sale
                $url = Yii::app()->createAbsoluteUrl('admin/ajax/search_user_login',['PaySalary' => 1]);
                // widget auto complete search user customer and supplier
                $aData = array(
                    'model'=>$model,
                    'field_customer_id'=>'monitor_id',
                    'url'=> $url,
                    'name_relation_user'=>'rMonitor',
                    'ClassAdd' => 'w-400',
                    'field_autocomplete_name' => 'autocomplete_name_1',
                    'placeholder'=>'Nhập mã hoặc tên giám sát',
                );
                $this->widget('ext.SpaAutocompleteCustomer.SpaAutocompleteCustomer',
                    array('data'=>$aData));                                        
            ?>
        </div>
            
        <div class="row">
            <?php echo $form->labelEx($model,'employees', array('label'=>'Nhân Viên')); ?>
            <?php echo $form->hiddenField($model,'employees', array('class'=>'')); ?>
            <?php
                // 1. limit search kh của sale
                $url = Yii::app()->createAbsoluteUrl('admin/ajax/search_user_login',['PaySalary' => 1]);
                // widget auto complete search user customer and supplier
                $aData = array(
                    'model'=>$model,
                    'field_customer_id'=>'employees',
                    'url'=> $url,
                    'name_relation_user'=>'rEmployee',
                    'ClassAdd' => 'w-400',
    //                'ShowTableInfo' => 0,
                    'fnSelectCustomerV2' => "fnDeloyBySelect",
                    'doSomethingOnClose' => "doSomethingOnClose",
                );
                $this->widget('ext.SpaAutocompleteCustomer.SpaAutocompleteCustomer',
                    array('data'=>$aData));                                        
            ?>
        </div>
        
        <?php include '_form_create_update_deloy_by.php';?>
        
        <div class="clr"></div>    
        <div class ='row'>  
        <?php echo $form->labelEx($model,'agents'); ?>
        <?php
                       $this->widget('ext.multiselect.JMultiSelect',array(
                             'model'=>$model,
                             'attribute'=>'agents',
                             'data'=>Users::getArrUserByRole(ROLE_AGENT),
                             // additional javascript options for the MultiSelect plugin
                             'options'=>array(),
                             // additional style
                             'htmlOptions'=>array('style' => 'width: 600px;'),
                       ));    
                   ?>
        <?php echo $form->error($model,'agents'); ?>

        </div>    
        <div class="row more_col">
        <div class="col1">
                <?php echo $form->labelEx($model,'start_date'); ?>
                <?php Yii::import('application.extensions.CJuiDateTimePicker.CJuiDateTimePicker');
                $this->widget('CJuiDateTimePicker',array(
                    'model'=>$model, //Model object
                    'attribute'=>'start_date', //attribute name
                    'mode'=>'datetime', //use "time","date" or "datetime" (default)
                    'language'=>'en-GB',
                    'options'=>array(
                        'showAnim'=>'fold',
                        'showButtonPanel'=>true,
                        'autoSize'=>true,
                        'dateFormat'=>'dd-mm-yy',
                        'timeFormat'=>'hh:mm:ss',
                        'width'=>'120',
                        'separator'=>' ',
                        'showOn' => 'button',
                        'buttonImage'=> Yii::app()->theme->baseUrl.'/admin/images/icon_calendar_r.gif',
                        'buttonImageOnly'=> true,
                        'changeMonth' => true,
                        'changeYear' => true,
                        //                'regional' => 'en-GB'
                    ),
                    'htmlOptions' => array(
                        'style' => 'width:180px;',
                        'placeholder' => 'Thời gian từ',
                    ),
                ));
                ?>     		
                <?php echo $form->error($model,'start_date'); ?>
        </div>
            <!--<label>&nbsp;</label>-->
        <div class="col2">
                <?php echo $form->labelEx($model,'end_date'); ?>
                <?php
                $this->widget('CJuiDateTimePicker',array(
                    'model'=>$model, //Model object
                    'attribute'=>'end_date', //attribute name
                    'mode'=>'datetime', //use "time","date" or "datetime" (default)
                    'language'=>'en-GB',
                    'options'=>array(
                        'showAnim'=>'fold',
                        'showButtonPanel'=>true,
                        'autoSize'=>true,
                        'dateFormat'=>'dd-mm-yy',
                        'timeFormat'=>'hh:mm:ss',
                        'width'=>'120',
                        'separator'=>' ',
                        'showOn' => 'button',
                        'buttonImage'=> Yii::app()->theme->baseUrl.'/admin/images/icon_calendar_r.gif',
                        'buttonImageOnly'=> true,
                        'changeMonth' => true,
                        'changeYear' => true,
                        //                'regional' => 'en-GB'
                    ),
                    'htmlOptions' => array(
                        'style' => 'width:180px;',
                        'placeholder' => 'Đến',
                    ),
                ));
                ?>     
                 <?php echo $form->error($model,'end_date'); ?>
        </div>
        </div>
        
        <div class="row showAmount <?php echo $model->type == GasEventMarket::TYPE_ROADSHOW ?  'display_none' : ''; ?>">
            <?php echo $form->labelEx($model, 'amount'); ?>
            <?php echo $form->textField($model, 'amount', array('class'=>'w-150 number_only ad_fix_currency', 'maxlength'=>20)); ?>
            <?php echo $form->error($model, 'amount'); ?>
        </div>           
            
	<div class="row">
            <?php echo $form->labelEx($model,'note'); ?>
            <?php echo $form->textArea($model, 'note', array('maxlength' => 300, 'rows' => 5, 'cols' => 50)); ?>
            <?php echo $form->error($model,'note'); ?>
	</div>
         <div class="row">
            <?php echo $form->labelEx($model, 'status'); ?>
            <?php echo $form->dropDownList($model, 'status', $model->getArrayStatus()); ?>
            <?php echo $form->error($model, 'status'); ?>
        </div>

	<div class="row" style = "display: none">
           <?php echo $form->labelEx($model,'created_date'); ?>
                <?php 
                    $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                        'model'=>$model,        
                        'attribute'=>'created_date',
                        'options'=>array(
                            'showAnim'=>'fold',
                            'dateFormat'=> ActiveRecord::getDateFormatJquery(),
//                            'minDate'=> '0',
                            'maxDate'=> '0',
                            'changeMonth' => true,
                            'changeYear' => true,
                            'showOn' => 'button',
                            'buttonImage'=> Yii::app()->theme->baseUrl.'/admin/images/icon_calendar_r.gif',
                            'buttonImageOnly'=> true,                                
                        ),        
                         'htmlOptions'=>array(
                            'class'=>'w-16',
                            'style'=>'height:20px;',
                                'readonly'=>'readonly',
                        ),
                    ));
                ?>     
                 <?php echo $form->error($model,'created_date'); ?>
	</div>
            
        <div class="row " style = "display: none">
            <?php echo $form->labelEx($model,'created_by'); ?>
            <?php echo $form->hiddenField($model,'created_by'); ?>
            <?php echo $form->error($model,'created_by'); ?>
	</div>
     
      
            
	<div class="row buttons" style="padding-left: 141px;">
        <?php $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType'=>'submit',
            'label'=>$model->isNewRecord ? 'Create' :'Save',
            'type'=>'null', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
            'size'=>'small', // null, 'large', 'small' or 'mini'
            //'htmlOptions' => array('style' => 'margin-bottom: 10px; float: right;'),
        )); ?>
        </div>

<?php $this->endWidget(); ?>

</div><!-- form -->
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/autoNumeric-master/autoNumeric.js"></script>
<script>
    $(document).ready(function(){
        fnInitInputCurrency();
        $('.form').find('button:submit').click(function(){
            $.blockUI({ overlayCSS: { backgroundColor: '#fff' } }); 
        });
        jQuery('a.gallery').colorbox({ opacity:0.5 , rel:'group1',innerHeight:'1000', innerWidth: '1050' });
        fnBindRemoveIcon();

    });
    
    /**
    * @Author: ANH DUNG Jun 23, 2015
    * @Todo: function này dc gọi từ ext của autocomplete
    * @Param: $model
    */
    function fnDeloyBySelect(ui, idField, idFieldCustomer){
        var row = $(idField).closest('.row');
        var ClassCheck = "uid_"+ui.item.id;
        var td_name = '<td class="'+ClassCheck+'">'+ui.item.name_role+'</td>';
        var td_remove = '<td class="item_c last"><span class="remove_icon_only"></span></td>';
        var input = '<input name="GasEventMarket[employee][]"  value="'+ui.item.id+'" type="hidden">';
        var tr = '<tr><td class="item_c order_no"></td>'+td_name+td_remove+input+'</tr>';
        if($('.tb_deloy_by tbody').find('.'+ClassCheck).size() < 1 ) {
            $('.tb_deloy_by tbody').append(tr);
        }
        fnRefreshOrderNumber();
    }
    /**
    * @Author: ANH DUNG Dec 28, 2016
    * @Todo: function này dc gọi từ ext của autocomplete, action close auto
    */
    function doSomethingOnClose(ui, idField, idFieldCustomer){
        var row = $(idField).closest('.row');
        row.find('.remove_row_item').trigger('click');
    }
    
    $('#GasEventMarket_type').on('change',function(){
       $value = $( "#GasEventMarket_type" ).val() ;
       if($value != <?php echo GasEventMarket::TYPE_ROADSHOW ?>){
           $('.showAmount').removeClass('display_none');
       }else{
            $('#GasEventMarket_amount').val(<?php echo GasEventMarket::AMOUNT_ROADSHOW ?>);
            $('.showAmount').addClass('display_none');
       }
    });
    fnSelectProvinceDistrict('#GasEventMarket_province_id', '#GasEventMarket_district_id' , "<?php echo Yii::app()->createAbsoluteUrl('admin/ajax/get_slt_district');?>");
</script>