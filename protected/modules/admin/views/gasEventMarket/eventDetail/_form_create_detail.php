<link href="<?php echo Yii::app()->theme->baseUrl; ?>/admin/css/jquery-ui-1.8.18.custom.css" type=text/css
          rel=stylesheet>
<link href="<?php echo Yii::app()->theme->baseUrl; ?>/admin/css/main.css" rel="stylesheet" type="text/css"
          media="screen"/>
<?php include 'index_button.php' ?>
<?php echo MyFormat::BindNotifyMsg(); ?>
<div class="form">
<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'modules-form',
	'enableAjaxValidation'=>false,
        'htmlOptions' => array('enctype' => 'multipart/form-data'),
)); ?>
<?php
Yii::app()->clientScript->registerCoreScript('jquery.ui');
$aDetail = ($mDetail->type == GasEventMarketDetail::TYPE_MATERIAL_IMPORT) ? $model->rDetailIm :  $model->rDetailEx;
?>
<div class="row">
    <?php echo $form->labelEx($mDetail,'Vật tư'); ?>
    <?php echo $form->hiddenField($mDetail,'employee_id'); ?>		
    <?php 
        // widget auto complete search material
        $aData = array(
            'model'=>$mDetail,
            'field_material_id'=>'employee_id',
            'name_relation_material'=>'rMaterials',
            'field_autocomplete_name'=>'autocomplete_material',
            'doSomethingOnCloseMaterial' => 'afterMaterialSelectForm',
        );
        $this->widget('ext.GasAutocompleteMaterial.GasAutocompleteMaterial',
            array('data'=>$aData));                                        
    ?> 
    <?php echo $form->error($mDetail,'employee_id'); ?>
</div>

<div class="row">
    <table class="materials_table hm_table">
        <thead>
            <tr>
                <th class="item_c">#</th>
                <th class="item_c">Mã</th>
                <th class="item_name item_c">Tên</th>
                <th class="item_unit item_c">ĐVT</th>
                <th class="w-30 item_c">SL</th>
                <th class="item_unit last item_c">Xóa</th>
            </tr>
        </thead>
        <tbody>
        <?php if(!empty($aDetail)): ?>
            <?php foreach($aDetail as $key => $item): ?>
                <?php $mMaterial = empty($item->rMaterials) ? '' : $item->rMaterials; ?>
                <?php if($mMaterial): ?>
                <tr class="materials_row">
                    <td class="item_c order_no"><?php echo ($key+1);?></td>
                    <td><?php echo $mMaterial->materials_no;?></td>
                    <td class="item_l"><?php echo $mMaterial->name;?></td>
                    <td class="item_c"><?php echo $mMaterial->unit;?></td>
                    <td class="item_c ">
                        <input name="GasEventMarketDetail[status][]" value="<?php echo ActiveRecord::formatNumberInput($item->status);?>" class="item_c item_qty materials_qty number_only_v1 w-30 " type="text" size="6" maxlength="9">
                        <input name="GasEventMarketDetail[employee_id][]" value="<?php echo $mMaterial->id;?>" class="materials_id" type="hidden">
                    </td>
                    <td class="item_c last"><span class="remove_icon_only"></span></td>
                </tr>
                <?php endif;?>
            <?php endforeach;?>
        <?php endif;?>
        </tbody>
    </table>
</div>

<script>
    $(document).ready(function(){
        fnBindRemoveIcon();
        fnRefreshOrderNumber();
    });
    function afterMaterialSelect(idField, ui){
        var class_item = 'materials_row_'+ui.item.id;
        var table = $('.materials_table');
        var tr = '';
        tr  += '<tr class="materials_row">'
            +      '<td class="item_c order_no"></td>'
            +      '<td>'+ui.item.materials_no+'</td>'
            +      '<td class="item_l">'+ui.item.name+'</td>'
            +      '<td class="item_c">'+ui.item.unit+'</td>'
            +      '<td class="item_c">'
            +           '<input name="GasEventMarketDetail[status][]" value="1" class="item_c item_qty w-30 materials_qty number_only_v1 '+class_item+'" type="text" size="6" maxlength="9">'
            +           '<input name="GasEventMarketDetail[employee_id][]" value="'+ui.item.id+'" class="materials_id" type="hidden">'
            +      '</td>'
            +      '<td class="item_c last"><span class="remove_icon_only"></span></td>'
            + '</tr>';
        table.append(tr);
        fnRefreshOrderNumber();
        
    }
     function afterMaterialSelectForm(idField, ui){
         $("#GasEventMarketDetail_employee_id").val('');
         $("#GasEventMarketDetail_autocomplete_material").val('').attr('readonly', false);
     }
</script>
<div class="row buttons" style="padding-left: 141px;">
    <?php $this->widget('bootstrap.widgets.TbButton', array(
        'buttonType'=>'submit',
        'label'=>$model->isNewRecord ? 'Create' :'Save',
        'type'=>'null', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
        'size'=>'small', // null, 'large', 'small' or 'mini'
        //'htmlOptions' => array('style' => 'margin-bottom: 10px; float: right;'),
    )); ?>
</div>

<?php $this->endWidget(); ?>
</div>