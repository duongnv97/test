<div class="form">
    <?php
        $LinkNormal         = Yii::app()->createAbsoluteUrl('admin/gasEventMarket/createDetailMaterial', array('id' => $model->id,'type_search'=> GasEventMarketDetail::TYPE_MATERIAL_EXPORT));
        $LinkImport        = Yii::app()->createAbsoluteUrl('admin/gasEventMarket/createDetailMaterial', array('id' => $model->id,'type_search'=> GasEventMarketDetail::TYPE_MATERIAL_IMPORT));
    ?>
    <h1><?php echo $this->adminPageTitle;?>
        <a style="text-decoration:none;" class='btn_cancel f_size_14 <?php echo $mDetail->type == GasEventMarketDetail::TYPE_MATERIAL_EXPORT ? "active":"";?>' href="<?php echo $LinkNormal;?>">Xuất kho</a>
        <a style="text-decoration:none;" class='btn_cancel f_size_14 <?php echo $mDetail->type == GasEventMarketDetail::TYPE_MATERIAL_IMPORT ? "active":"";?>' href="<?php echo $LinkImport;?>">Thu về</a>
    </h1>
</div>