<?php
/**
 * @var CActiveForm $form
 */
?>
<div class="wide form">

    <?php $form = $this->beginWidget('CActiveForm', array(
        'action' => GasCheck::getCurl(),
        'method' => 'get',
    )); ?>

    <?php include('_row_uid_login.php') ?>

    <?php include('_row_customer_id.php') ?>

    <?php include('_row_sale_id.php') ?>

    <div class="row">
        <?php echo $form->label($model, 'status', array()); ?>
        <?php echo $form->dropDownList($model, 'status', GasReportTrouble::$STATUS_TEXT, array('empty' => 'Select')); ?>
    </div>

    <div class="row buttons" style="padding-left: 159px;">
        <?php $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType' => 'submit',
            'label' => 'Search',
            'type' => 'null', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
            'size' => 'small', // null, 'large', 'small' or 'mini'
            //'htmlOptions' => array('style' => 'margin-bottom: 10px; float: right;'),
        )); ?>    </div>

    <?php $this->endWidget(); ?>

</div><!-- search-form -->