<div class="row">
    <?php
    echo $form->label($model, 'uid_login', array());
    echo $form->hiddenField($model, 'uid_login');
    $aData = array(
        'model' => $model,
        'field_customer_id' => 'uid_login',
        'field_autocomplete_name' => 'autocomplete_login_id',
        'url' => Yii::app()->createAbsoluteUrl('admin/ajax/search_user_leave'),
        'name_relation_user' => 'autocomplete_login',
        'placeholder' => 'Nhập Tên Sale',
        'ClassAdd' => 'w-500',
    );
    $this->widget('ext.SpaAutocompleteCustomer.SpaAutocompleteCustomer',
        array('data' => $aData));
    ?>
</div>