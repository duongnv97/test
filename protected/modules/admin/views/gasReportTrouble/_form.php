<?php
/**
 * @var GasReportTrouble $model
 * @var CActiveForm $form
 */
?>
<div class="form">

    <?php $form = $this->beginWidget('CActiveForm', array(
        'id' => 'gas-report-trouble-form',
        'enableAjaxValidation' => false,
        'htmlOptions' => array('enctype' => 'multipart/form-data'),
    )); ?>

    <p class="note">Dòng có dấu <span class="required">*</span> là bắt buộc.</p>
    <?php echo MyFormat::BindNotifyMsg(); ?>

    <?php include('_row_customer_id.php'); ?>

    <?php if (!$model->getIsNewRecord()) { ?>
        <div class="row">
            <?php echo $form->labelEx($model, 'status'); ?>
            <?php echo $form->dropDownList($model, 'status', GasReportTrouble::$STATUS_TEXT, array('onChange' => 'javascript:updateStatusView(this.value)')); ?>
            <?php echo $form->error($model, 'status'); ?>
        </div>
    <?php } ?>

    <div class="row">
        <?php echo $form->labelEx($model, 'detail'); ?>
        <?php echo $form->textArea($model, 'detail', array('rows' => 6, 'cols' => 50)); ?>
        <?php echo $form->error($model, 'detail'); ?>
    </div>

    <?php if (!$model->getIsNewRecord()) { ?>
        <div class="row row_result fix_custom_label_required">
            <?php echo $form->labelEx($model, 'result'); ?>
            <?php echo $form->textArea($model, 'result', array('rows' => 6, 'cols' => 50)); ?>
            <?php echo $form->error($model, 'result'); ?>
        </div>
    <?php } ?>

    <div class="row">
        <?php echo $form->labelEx($model, 'note'); ?>
        <?php echo $form->textArea($model, 'note', array('rows' => 6, 'cols' => 50)); ?>
        <?php echo $form->error($model, 'note'); ?>
    </div>

    <div class="row buttons" style="padding-left: 141px;">
        <?php $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType' => 'submit',
            'label' => $model->isNewRecord ? 'Create' : 'Save',
            'type' => 'null', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
            'size' => 'small', // null, 'large', 'small' or 'mini'
            //'htmlOptions' => array('style' => 'margin-bottom: 10px; float: right;'),
        )); ?>    </div>

    <?php $this->endWidget(); ?>

</div><!-- form -->
<script>
    function updateStatusView(status) {
        if (status == <?php echo GasReportTrouble::STA_COMPLETE ?>) {
            $('.row_result').show();
        } else {
            $('.row_result').hide();
        }
    }
    $(document).ready(function () {
        $('.form').find('button:submit').click(function () {
            $.blockUI({overlayCSS: {backgroundColor: '#fff'}});
        });
        updateStatusView(<?php echo $model->status; ?>);
        fnBindFixLabelRequired();
    });
</script>