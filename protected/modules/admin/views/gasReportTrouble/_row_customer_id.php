<div class="row">
    <?php echo $form->label($model, 'customer_id'); ?>
    <?php echo $form->hiddenField($model, 'customer_id'); ?>
    <?php
    // widget auto complete search user customer and supplier
    $aData = array(
        'model' => $model,
        'field_customer_id' => 'customer_id',
        'field_autocomplete_name' => 'autocomplete_customer_id',
        'name_relation_user' => 'autocomplete_customer',
        'url' => Yii::app()->createAbsoluteUrl('admin/ajax/search_user_by_code'),
        'ClassAdd' => 'w-500',
    );
    $this->widget('ext.SpaAutocompleteCustomer.SpaAutocompleteCustomer',
        array('data' => $aData));
    ?>
</div>