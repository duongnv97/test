<?php
$this->breadcrumbs = array(
    $this->singleTitle,
);

$menus = array(
    array('label' => "Create $this->singleTitle", 'url' => array('create')),
);
$this->menu = ControllerActionsName::createMenusRoles($menus, $actions);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('gas-report-trouble-grid', {
                url : $(this).attr('action'),
		data: $(this).serialize()
	});
	return false;
});
");

Yii::app()->clientScript->registerScript('ajaxupdate', "
$('#gas-report-trouble-grid a.ajaxupdate').live('click', function() {
    $.fn.yiiGridView.update('gas-report-trouble-grid', {
        type: 'POST',
        url: $(this).attr('href'),
        success: function() {
            $.fn.yiiGridView.update('gas-report-trouble-grid');
        }
    });
    return false;
});
");
?>

<div class="form">
    <?php
    $LinkNormal = Yii::app()->createAbsoluteUrl('admin/gasReportTrouble/index');
    $linkDoing = Yii::app()->createAbsoluteUrl('admin/gasReportTrouble/index', array('status' => GasReportTrouble::STA_PROCESSING));
    $linkCompleted = Yii::app()->createAbsoluteUrl('admin/gasReportTrouble/index', array('status' => GasReportTrouble::STA_COMPLETE));
    ?>
    <h1>Danh Sách <?php echo $this->pluralTitle; ?>
        <a class='btn_cancel f_size_14 <?php echo (!isset($_GET['status'])) ? "active" : ""; ?>'
           href="<?php echo $LinkNormal; ?>">Tất cả</a>
        <a class='btn_cancel f_size_14 <?php echo (isset($_GET['status']) && $_GET['status'] == GasReportTrouble::STA_PROCESSING) ? "active" : ""; ?>'
           href="<?php echo $linkDoing; ?>"><?php echo GasReportTrouble::$STATUS_TEXT[GasReportTrouble::STA_PROCESSING]; ?></a>
        <a class='btn_cancel f_size_14 <?php echo (isset($_GET['status']) && $_GET['status'] == GasReportTrouble::STA_COMPLETE) ? "active" : ""; ?>'
           href="<?php echo $linkCompleted; ?>"><?php echo GasReportTrouble::$STATUS_TEXT[GasReportTrouble::STA_COMPLETE]; ?></a>
    </h1>
</div>

<?php echo CHtml::link('Tìm Kiếm Nâng Cao', '#', array('class' => 'search-button')); ?>
<div class="search-form" style="display:none">
    <?php $this->renderPartial('_search', array(
        'model' => $model,
    )); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
    'id' => 'gas-report-trouble-grid',
    'dataProvider' => $model->search(),
    'afterAjaxUpdate' => 'function(id, data){ fnUpdateColorbox();}',
    'template' => '{pager}{summary}{items}{pager}{summary}',
//    'itemsCssClass' => 'items custom_here',
//    'htmlOptions'=> array('class'=>'grid-view custom_here'),
    'pager' => array(
        'maxButtonCount' => CmsFormatter::$PAGE_MAX_BUTTON,
    ),
    'enableSorting' => false,
    //'filter'=>$model,
    'columns' => array(
        array(
            'header' => 'S/N',
            'type' => 'raw',
            'value' => '$this->grid->dataProvider->pagination->currentPage * $this->grid->dataProvider->pagination->pageSize + ($row+1)',
            'headerHtmlOptions' => array('width' => '10px', 'style' => 'text-align:center;'),
            'htmlOptions' => array('style' => 'text-align:center;')
        ),
        array(
            'name' => 'uid_login',
            'value' => '$data->getNameUserCreate()'
        ),
        array(
            'name' => 'customer_id',
            'value' => '$data->getCustomerName()'
        ),
        array(
            'name' => 'sale_id',
            'value' => '$data->getSaleName()'
        ),
        array(
            'name' => 'status',
            'value' => '$data->getStatusText()'
        ),
        array(
            'name' => 'detail',
            'type' => 'html',
            'value' => '$data->getDetailHtml()'
        ),
        array(
            'header' => 'Actions',
            'class' => 'CButtonColumn',
            'template' => ControllerActionsName::createIndexButtonRoles($actions),
            'buttons' => array(
                'update' => array(
                    'visible' => '$data->canUpdate()',
                ),
                'delete' => array(
                    'visible' => 'GasCheck::canDeleteData($data)',
                ),
            ),
        ),
    ),
)); ?>

<script>
    $(document).ready(function () {
        fnUpdateColorbox();
    });

    function fnUpdateColorbox() {
        fixTargetBlank();
    }
</script>