<div class="row">
    <?php echo Yii::t('translation', $form->label($model, 'sale_id')); ?>
    <?php echo $form->hiddenField($model, 'sale_id'); ?>
    <?php
    // widget auto complete search user customer and supplier
    $aData = array(
        'model' => $model,
        'field_customer_id' => 'sale_id',
        'field_autocomplete_name' => 'autocomplete_sale_id',
        'url' => Yii::app()->createAbsoluteUrl('admin/ajax/search_sale'),
        'name_relation_user' => 'autocomplete_sale',
        'placeholder' => 'Nhập Tên Sale',
        'ClassAdd' => 'w-500',
    );
    $this->widget('ext.SpaAutocompleteCustomer.SpaAutocompleteCustomer',
        array('data' => $aData));
    ?>
    <?php echo $form->error($model, 'sale_id'); ?>
</div>