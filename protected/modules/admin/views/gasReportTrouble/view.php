<?php
/**
 * #@var GasReportTrouble $model
 */
$this->breadcrumbs = array(
    $this->pluralTitle => array('index'),
    ' ' . $this->singleTitle . ' : ',
);

$menus = array(
    array('label' => "$this->pluralTitle Management", 'url' => array('index')),
    array('label' => "Tạo Mới $this->singleTitle", 'url' => array('create')),
    array('label' => "Cập Nhật $this->singleTitle", 'url' => array('update', 'id' => $model->id)),
    array('label' => "Xóa $this->singleTitle", 'url' => array('delete'), 'linkOptions' => array('submit' => array('delete', 'id' => $model->id), 'confirm' => 'Bạn chắc chắn muốn xóa ?')),
);
$this->menu = ControllerActionsName::createMenusRoles($menus, $actions);
?>

<h1>Xem: <?php echo $this->singleTitle; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
    'data' => $model,
    'attributes' => array(
        array(
            'name' => 'uid_login',
            'value' => $model->getNameUserCreate()
        ),
        array(
            'name' => 'customer_id',
            'value' => $model->getCustomerName()
        ),
        array(
            'name' => 'sale_id',
            'value' => $model->getSaleName()
        ),
        array(
            'name' => 'status',
            'value' => $model->getStatusText()
        ),
        array(
            'name' => 'detail',
            'type' => 'html',
            'value' => $model->getDetailHtml()
        ),
    ),
)); ?>
