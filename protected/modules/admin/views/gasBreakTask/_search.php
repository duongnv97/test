<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
            <?php echo $form->label($model,'code_no',array()); ?>
            <?php echo $form->textField($model,'code_no',array('class'=> 'w-300','maxlength'=>30)); ?>
	</div>

	<div class="row">
            <?php echo $form->label($model,'employee_id',array()); ?>
            <?php echo $form->dropDownList($model,'employee_id', GasBreakTask::GetListSelectEmployee(),array('class' => 'w-300' ,'empty'=>'Select')); ?>
	</div>

	<div class="row">
            <?php echo $form->label($model,'job_description',array()); ?>
            <?php echo $form->textField($model,'job_description',array('class'=> 'w-300','maxlength'=>450)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'deadline',array()); ?>
		<?php 
                    $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                        'model'=>$model,        
                        'attribute'=>'deadline',
                        'options'=>array(
                            'showAnim'=>'fold',
                            'dateFormat'=> MyFormat::$dateFormatSearch,
                            'changeMonth' => true,
                            'changeYear' => true,
                            'showOn' => 'button',
                            'buttonImage'=> Yii::app()->theme->baseUrl.'/admin/images/icon_calendar_r.gif',
                            'buttonImageOnly'=> true,                                
                        ),        
                        'htmlOptions'=>array(
                            'class'=>'w-16',                            
                        ),
                    ));
                ?> 
	</div>

	<div class="row">
		<?php echo $form->label($model,'status',array()); ?>
		<?php echo $form->dropDownList($model,'status', GasBreakTask::$ARR_STATUS, array('class'=>'w-300', 'empty'=>'Select')); ?>
	</div>

	<div class="row buttons" style="padding-left: 159px;">
		        <?php $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType'=>'submit',
            'label'=>'Search',
            'type'=>'null', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
            'size'=>'small', // null, 'large', 'small' or 'mini'
            //'htmlOptions' => array('style' => 'margin-bottom: 10px; float: right;'),
        )); ?>	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->