<?php
$cmsFormater = new CmsFormatter();
$NameEmployee = $cmsFormater->formatNameAndRole($model->rEmployeeId);
$this->breadcrumbs=array(
	'Phân Công Công Việc'=>array('index'),
	$NameEmployee => array('view','id'=>$model->id),
	'Cập Nhật',
);

$menus = array(	
        array('label'=>'Phân Công Công Việc', 'url'=>array('index')),
	array('label'=>'Xem Phân Công Công Việc', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Tạo Mới Phân Công Công Việc', 'url'=>array('create')),	
);
$this->menu= ControllerActionsName::createMenusRoles($menus, $actions);

?>

<h1>Cập Nhật Phân Công Công Việc: <?php echo $NameEmployee; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>