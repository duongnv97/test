<?php
$cmsFormater = new CmsFormatter();
$NameEmployee = $cmsFormater->formatNameAndRole($model->rEmployeeId);
$this->breadcrumbs=array(
	'Phân Công Công Việc'=>array('index'),
	$NameEmployee,
);

$menus = array(
	array('label'=>'Phân Công Công Việc', 'url'=>array('index')),
	array('label'=>'Tạo Mới Phân Công Công Việc', 'url'=>array('create')),
	array('label'=>'Cập Nhật Phân Công Công Việc', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Xóa Phân Công Công Việc', 'url'=>array('delete'), 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Bạn chắc chắn muốn xóa ?')),
);
$this->menu= ControllerActionsName::createMenusRoles($menus, $actions);
?>

<h1>Xem Phân Công Công Việc: <?php echo $NameEmployee; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'code_no',
                array(
                    'name' => 'employee_id',
                    'type' => 'NameAndRole',
                    'value' => $model->rEmployeeId,
                ),		
                array(
                    'name'=>'job_description',
                    'type'=>'html',
                    'value'=>nl2br($model->job_description),
                ),  
		'date_begin:date',
		'deadline:date',
		array(
                    'name' => 'status',
                    'type' => 'html',
                    'value' => GasBreakTask::GetStatusText( $model ),
                ),
		array(
                    'name' => 'uid_login',
                    'type' => 'NameAndRole',
                    'value' => $model->rUidLogin,
                ),
		'created_date:datetime',
	),
)); ?>


<?php 
if(GasCheck::isAllowAccess("gasBreakTask", "update_report_daily")):
    include '_report_daily.php';
endif;

if(GasCheck::isAllowAccess("gasBreakTask", "post_leader_help_content")):
    include '_leader_help.php';
endif;

//if(GasCheck::isAllowAccess("gasBreakTask", "delete_report_daily")):
//    include '_report_daily.php';
//endif;
 include '_box_comment.php';
?>

<script>
    $(window).load(function(){ 
        $('.form').find('button:submit').click(function(){
            $.blockUI({ overlayCSS: { backgroundColor: '#fff' } }); 
        });
        
        fnResizeColorbox();
    });
    
    function fnResizeColorbox(){
//        var y = $('body').height()+100;
        var y = $('#main_box').height()+100;
        parent.$.colorbox.resize({innerHeight:y});        
    }
    $(document).ready(function(){
        $('.form').find('button:submit').click(function(){
            $.blockUI({ overlayCSS: { backgroundColor: '#fff' } }); 
        });        
    });
</script>
