<?php
$this->breadcrumbs=array(
	'Phân Công Công Việc'=>array('index'),
	'Tạo Mới',
);

$menus = array(		
        array('label'=>'Phân Công Công Việc', 'url'=>array('index')),
);
$this->menu= ControllerActionsName::createMenusRoles($menus, $actions);
?>

<h1>Tạo Mới Phân Công Công Việc</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>