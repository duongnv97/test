<?php 
$cmsFormater = new CmsFormatter();
?>
<li class="received reply margin_0">

    <?php if(GasCheck::isAllowAccess('gasBreakTask', 'Delete_report_daily')):?>
    <?php // if(0): // DEC 14, 2014 CHUA XU LY CHO XOA  ?>
    <div class="content">
        <div class="actions">
            <form method="post" class="button_to" action="<?php echo Yii::app()->createAbsoluteUrl('admin/gasBreakTask/delete_report_daily', array('id'=>$data->id)) ?>">
                <div>
                    <input type="submit" value="Delete" class="btn digital_delete btn_closed_tickets" title="Xóa vĩnh viễn comment này" alert_text="Xóa vĩnh viễn comment này?"> 
                </div>
            </form>
        </div>
    </div>
    <?php endif;?>
    
    <div class="message">
        <?php if(trim($data->report_date) != ""):?>
        <span class="item_b">Ngày Báo Cáo: </span> <?php echo $cmsFormater->formatDate($data->report_date);?><br>
        <?php endif;?>
        <span class="item_b">Nội Dung: </span><?php echo nl2br($data->report_content);?>
        <?php if(trim($data->job_change) != ""):?>
        <br><span class="item_b">Công Việc Phát Sinh: </span> <?php echo nl2br($data->job_change);?>
        <?php endif;?>
        <?php echo $cmsFormater->formatBreakTaskDailyFile($data);?>
        <span class="posted_on">Ngày gửi <?php echo $cmsFormater->formatDateTime($data->created_date); ?></span>
    </div>
    <div class="author">
        <span class="name item_b">
            <?php echo GasTextComment::ShowNamePost($data->rUidLogin); ?>
        </span>
    </div>
    
</li>