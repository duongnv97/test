<div class="form">
<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'gas-break-task-form_update_report_daily',
	'action' => Yii::app()->createAbsoluteUrl('admin/gasBreakTask/post_leader_help_content', array('id' => $model->id )),
	'enableAjaxValidation'=>false,
        'htmlOptions' => array('enctype' => 'multipart/form-data'),
)); ?>

    <p class="note item_b">Ý Kiến Chỉ Đạo:</p>
    <?php echo MyFormat::BindNotifyMsg(); ?>
    
    <div class="row">
        <?php echo $form->labelEx($mTaskDaily,'current_status'); ?>
        <?php echo $form->dropDownList($mTaskDaily,'current_status', GasBreakTask::$ARR_STATUS_UPDATE_LEADER, array('class'=>'w-200')); ?>
        <?php echo $form->error($mTaskDaily,'current_status'); ?>
    </div>
    <div class="row text_area_16">
        <?php echo $form->labelEx($mTaskDaily,'report_content', array('label'=>'Ý Kiến Chỉ Đạo')); ?>
        <?php echo $form->textArea($mTaskDaily,'report_content',array("placeholder"=>"Nhập Ý Kiến Chỉ Đạo")); ?>
        <?php echo $form->error($mTaskDaily,'report_content'); ?>
    </div>
    
    <div class="row buttons" style="padding-left: 141px;">
        <?php $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType'=>'submit',
            'label'=> "Gửi Ý Kiến Chỉ Đạo",
            'type'=>'null', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
            'size'=>'small', // null, 'large', 'small' or 'mini'
            //'htmlOptions' => array('style' => 'margin-bottom: 10px; float: right;'),
        )); ?>
        <input class='cancel_iframe' type='button' value='Cancel'>
    </div>

<?php $this->endWidget(); ?>

</div><!-- form -->
