<?php
$this->breadcrumbs=array(
	'Phân Công Công Việc',
);

$menus=array(
            array('label'=>'Tạo Mới Phân Công Công Việc', 'url'=>array('create')),
);
$this->menu= ControllerActionsName::createMenusRoles($menus, $actions);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('gas-break-task-grid', {
                url : $(this).attr('action'),
		data: $(this).serialize()
	});
	return false;
});
");

Yii::app()->clientScript->registerScript('ajaxupdate', "
$('#gas-break-task-grid a.ajaxupdate').live('click', function() {
    $.fn.yiiGridView.update('gas-break-task-grid', {
        type: 'POST',
        url: $(this).attr('href'),
        success: function() {
            $.fn.yiiGridView.update('gas-break-task-grid');
        }
    });
    return false;
});
");
?>
<div class="form">
    <?php 
        $LinkNormal = Yii::app()->createAbsoluteUrl('admin/gasBreakTask/index');
        $LinkClosed = Yii::app()->createAbsoluteUrl('admin/gasBreakTask/index', array( 'status'=> GasBreakTask::STATUS_CLOSE_BY_LEADER));
    ?>
    <h1>Phân Công Công Việc
        <a class='btn_cancel f_size_14' href="<?php echo $LinkNormal;?>">Normal Job</a>
        <a class='btn_cancel f_size_14' href="<?php echo $LinkClosed;?>">Closed Job</a>
    </h1> 
</div>

<?php echo CHtml::link('Tìm Kiếm Nâng Cao','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'gas-break-task-grid',
	'dataProvider'=>$model->search(),
        'afterAjaxUpdate'=>'function(id, data){ fnUpdateColorbox();}',
        'template'=>'{pager}{summary}{items}{pager}{summary}',
//    'itemsCssClass' => 'items custom_here',
//    'htmlOptions'=> array('class'=>'grid-view custom_here'),
        'pager' => array(
            'maxButtonCount'=>  CmsFormatter::$PAGE_MAX_BUTTON,
        ),
	'enableSorting' => false,
	//'filter'=>$model,
	'columns'=>array(
        array(
            'header' => 'S/N',
            'type' => 'raw',
            'value' => '$this->grid->dataProvider->pagination->currentPage * $this->grid->dataProvider->pagination->pageSize + ($row+1)',
            'headerHtmlOptions' => array('width' => '10px','style' => 'text-align:center;'),
            'htmlOptions' => array('style' => 'text-align:center;')
        ),
		'code_no',
                array(
                    'name' => 'employee_id',
                    'type' => 'NameAndRole',
                    'value' => '$data->rEmployeeId',
                ),
                array(
                    'name' => 'job_description',
                    'type' => 'html',
                    'value' => 'nl2br($data->job_description)',
//                    'htmlOptions' => array('style' => 'width:120px;')
                ),
                array(
                    'name' => 'date_begin',
                    'type' => 'date',
                    'htmlOptions' => array('style' => 'text-align:center;')
                ),
                array(
                    'name' => 'deadline',
                    'type' => 'date',
                    'htmlOptions' => array('style' => 'text-align:center;')
                ),
		array(
                    'name' => 'status',
                    'type' => 'html',
                    'value' => 'GasBreakTask::GetStatusText( $data )',
                ),
                array(
                    'name' => 'uid_login',
                    'type' => 'NameAndRole',
                    'value' => '$data->rUidLogin',
                ),
                array(
                    'name' => 'created_date',
                    'type' => 'datetime',
                    'htmlOptions' => array('style' => 'text-align:center;')
                ),
            
                array(
                    'header' => 'Cập Nhật Mới Nhất',
                    'type' => 'html',
                    'value' => 'GasBreakTaskDaily::GetLastComment( $data )',
                ),
		array(
                    'header' => 'Actions',
                    'class'=>'CButtonColumn',
                    'template'=> ControllerActionsName::createIndexButtonRoles($actions),
                    'buttons'=>array(
                        'update'=>array(
                            'visible'=> 'GasCheck::CanUpdateBreakTask($data)',
                        ),
                        'delete'=>array(
                            'visible'=> 'GasCheck::canDeleteData($data)',
                        ),
                    ),
		),
	),
)); ?>

<script>
$(document).ready(function() {
    fnUpdateColorbox();
});

function fnUpdateColorbox(){   
    fixTargetBlank();
    $(".view").colorbox({iframe:true,
        innerHeight:'1000', 
        innerWidth: '1100', escKey:false,close: "<span title='close'>close</span>"
    });
    fnShowhighLightTr();
}
</script>