<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/admin/css/huongminh_ticket.css" />
<div class="digital_ticket">
    <div class="tickets">
        <div class="ticket ">
            <!--<h2 class="section-header closed_tickets">Danh Sách Bình Luận</h2>-->
            <h2 class="section-header closed_tickets">Lịch Sử Báo Cáo</h2>
            <?php  // NGUYEN DUNG
                $this->widget('zii.widgets.CListView', array(
                    'dataProvider'=> $mTaskDaily->search(),
                    'itemView' => '_box_comment_item',
            //        'ajaxUpdate'=>false, 
            //        'afterAjaxUpdate'=>'function(id, data){ BindClickView(); }',
                    'itemsCssClass'=>'',
                    'pagerCssClass'=>'pager',
                    'pager'=> array(
                        'maxButtonCount' => 10,
                        'class'=>'CLinkPager',
                        'header'=> false,
                        'footer'=> false,
                    ),
                    'summaryText' => true,
                    'summaryText'=>'Showing <strong>{start} - {end}</strong> of {count} results',
                    'template'=>'{pager}{summary}<ul class="replies clearfix">{items}</ul>{pager}{summary}',
                ));
            ?>

        </div><!-- end  <div class="ticket "> -->
    </div><!-- end  <div class="tickets "> -->
</div><!-- end  <div class="digital_ticket "> -->