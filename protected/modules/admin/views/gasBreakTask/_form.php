<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'gas-break-task-form',
	'enableAjaxValidation'=>false,
        'htmlOptions' => array('enctype' => 'multipart/form-data'),
)); ?>

    <p class="note">Dòng có dấu <span class="required">*</span> là bắt buộc.</p>
    <?php echo MyFormat::BindNotifyMsg(); ?>
    <div class="row">
        <?php // echo $form->labelEx($model,'employee_id'); ?>
        <?php // echo $form->dropDownList($model,'employee_id', GasBreakTask::GetListSelectEmployee(),array('class' => 'w-400' ,'empty'=>'Select')); ?>
        <?php // echo $form->error($model,'employee_id'); ?>
    </div>
    <div class="row">
        <?php echo $form->hiddenField($model, 'employee_id'); ?>
        <?php echo $form->labelEx($model, 'employee_id'); ?>
        <?php
        // Widget auto complete search user
        $aData = array(
            'model'             => $model,
            'field_customer_id' => 'employee_id',
            'url'               => Yii::app()->createAbsoluteUrl('admin/ajax/search_user_login', ['PaySalary' => 1]),
            'name_relation_user'        => 'rEmployeeId',
            'field_autocomplete_name'   => 'autocomplete_user',
            'ClassAdd'                  => 'w-400',
        );
        $this->widget('ext.SpaAutocompleteCustomer.SpaAutocompleteCustomer',
                array('data'    => $aData)
                );
        ?>
        <?php echo $form->error($model, 'employee_id'); ?>
    </div>
    
    
    <div class="row">
        <?php echo $form->labelEx($model,'job_description'); ?>
        <?php echo $form->textArea($model,'job_description',array('rows'=>3,'cols'=>100,"placeholder"=>"")); ?>
        <?php echo $form->error($model,'job_description'); ?>
    </div>
    <div class="row">
        <?php echo $form->labelEx($model,'date_begin'); ?>
        <?php 
            $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                'model'=>$model,        
                'attribute'=>'date_begin',
                'options'=>array(
                    'showAnim'=>'fold',
                    'dateFormat'=> ActiveRecord::getDateFormatJquery(),
                    'changeMonth' => true,
                    'changeYear' => true,
                    'showOn' => 'button',
                    'buttonImage'=> Yii::app()->theme->baseUrl.'/admin/images/icon_calendar_r.gif',
                    'buttonImageOnly'=> true,                                
                ),        
                'htmlOptions'=>array(
                    'class'=>'w-200',
                    'readonly'=>'readonly',
                ),
            ));
        ?>
        <?php echo $form->error($model,'date_begin'); ?>
    </div>
    <div class="row">
        <?php echo $form->labelEx($model,'deadline'); ?>
        <?php 
            $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                'model'=>$model,        
                'attribute'=>'deadline',
                'options'=>array(
                    'showAnim'=>'fold',
                    'dateFormat'=> ActiveRecord::getDateFormatJquery(),
                    'changeMonth' => true,
                    'changeYear' => true,
                    'showOn' => 'button',
                    'buttonImage'=> Yii::app()->theme->baseUrl.'/admin/images/icon_calendar_r.gif',
                    'buttonImageOnly'=> true,                                
                ),        
                'htmlOptions'=>array(
                    'class'=>'w-200',
                    'readonly'=>'readonly',
                ),
            ));
        ?>
        <?php echo $form->error($model,'deadline'); ?>
    </div>
    <div class="row">
        <?php echo $form->labelEx($model,'status'); ?>
        <?php echo $form->dropDownList($model,'status', GasBreakTask::$ARR_STATUS, array('class'=>'w-200')); ?>
        <?php echo $form->error($model,'status'); ?>
    </div>
	<div class="row buttons" style="padding-left: 141px;">
            <?php $this->widget('bootstrap.widgets.TbButton', array(
                'buttonType'=>'submit',
                'label'=>$model->isNewRecord ? 'Create' :'Save',
                'type'=>'null', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
                'size'=>'small', // null, 'large', 'small' or 'mini'
                //'htmlOptions' => array('style' => 'margin-bottom: 10px; float: right;'),
            )); ?>	
        </div>

<?php $this->endWidget(); ?>

</div><!-- form -->
<script>
    $(document).ready(function(){
        $('.form').find('button:submit').click(function(){
            $.blockUI({ overlayCSS: { backgroundColor: '#fff' } }); 
        });
        
    });
</script>