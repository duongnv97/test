<div class="form">
<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'gas-break-task-form_update_report_daily',
	'action' => Yii::app()->createAbsoluteUrl('admin/gasBreakTask/update_report_daily', array('id' => $model->id )),
	'enableAjaxValidation'=>false,
        'htmlOptions' => array('enctype' => 'multipart/form-data'),
)); ?>

    <p class="note item_b">Báo Cáo Công Việc Hàng Ngày:</p>
    <?php echo MyFormat::BindNotifyMsg(); ?>
    
    <div class="row">
        <?php echo $form->labelEx($mTaskDaily,'report_date'); ?>
        <?php 
            $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                'model'=>$mTaskDaily,        
                'attribute'=>'report_date',
                'options'=>array(
                    'showAnim'=>'fold',
                    'dateFormat'=> ActiveRecord::getDateFormatJquery(),
                    'changeMonth' => true,
                    'changeYear' => true,
                    'showOn' => 'button',
                    'buttonImage'=> Yii::app()->theme->baseUrl.'/admin/images/icon_calendar_r.gif',
                    'buttonImageOnly'=> true,                                
                ),        
                'htmlOptions'=>array(
                    'class'=>'w-200',
                    'readonly'=>'readonly',
                ),
            ));
        ?>
        <?php echo $form->error($mTaskDaily,'report_date'); ?>
    </div>
    
    <div class="row">
        <?php echo $form->labelEx($mTaskDaily,'current_status'); ?>
        <?php echo $form->dropDownList($mTaskDaily,'current_status', GasBreakTask::$ARR_STATUS_UPDATE, array('class'=>'w-200')); ?>
        <?php echo $form->error($mTaskDaily,'current_status'); ?>
    </div>
    <div class="row text_area_16">
        <?php echo $form->labelEx($mTaskDaily,'report_content'); ?>
        <?php echo $form->textArea($mTaskDaily,'report_content',array("placeholder"=>"Nhập báo cáo tiến độ")); ?>
        <?php echo $form->error($mTaskDaily,'report_content'); ?>
    </div>
    <div class="row text_area_16">
        <?php echo $form->labelEx($mTaskDaily,'job_change'); ?>
        <?php echo $form->textArea($mTaskDaily,'job_change',array("placeholder"=>"Nhập công việc phát sinh")); ?>
        <?php echo $form->error($mTaskDaily,'job_change'); ?>
    </div>
    
    <?php include '_box_file.php';?>
    
    <div class="row buttons" style="padding-left: 141px;">
        <?php $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType'=>'submit',
            'label'=> "Gửi Báo Cáo",
            'type'=>'null', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
            'size'=>'small', // null, 'large', 'small' or 'mini'
            //'htmlOptions' => array('style' => 'margin-bottom: 10px; float: right;'),
        )); ?>
        <input class='cancel_iframe' type='button' value='Cancel'>
    </div>

<?php $this->endWidget(); ?>

</div><!-- form -->
