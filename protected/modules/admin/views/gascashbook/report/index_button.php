<div class="form">
    <?php
        $cUid = MyFormat::getCurrentUid();
        $LinkAgent              = Yii::app()->createAbsoluteUrl('admin/gascashbook/viewAll', ['target'=> GasCashBookDetail::TARGET_AGENT]);
        $LinkEmployee           = Yii::app()->createAbsoluteUrl('admin/gascashbook/viewAll', ['target'=> GasCashBookDetail::TARGET_EMPLOYEE]);
        $LinkEmployeeOff        = Yii::app()->createAbsoluteUrl('admin/gascashbook/viewAll', ['target'=> GasCashBookDetail::TARGET_EMPLOYEE_OFF]);
        $LinkEmployeeDebit      = Yii::app()->createAbsoluteUrl('admin/gascashbook/viewAll', ['target'=> GasCashBookDetail::TARGET_EMPLOYEE_DEBIT]);
        $linkSale               = Yii::app()->createAbsoluteUrl('admin/gascashbook/viewAll', ['target'=> GasCashBookDetail::TARGET_SALE]);
        $LinkEmployeeByAgent    = Yii::app()->createAbsoluteUrl('admin/gascashbook/viewAll', ['target'=> GasCashBookDetail::TARGET_EMPLOYEE_BY_AGENT]);
        $activeAgent    = '';
        if(!isset($_GET['target']) || isset($_GET['target']) && $_GET['target']== GasCashBookDetail::TARGET_AGENT){
            $activeAgent    = 'active';
        }
    ?>
    <h1>Xem Tồn quỹ 
        <a class='btn_cancel f_size_14 <?php echo $activeAgent;?>' href="<?php echo $LinkAgent;?>">Đại lý</a>
        <a class='btn_cancel f_size_14 <?php echo isset($_GET['target']) && $_GET['target']== GasCashBookDetail::TARGET_EMPLOYEE ? "active":"";?>' href="<?php echo $LinkEmployee;?>">Nhân viên</a>
        <a class='btn_cancel f_size_14 <?php echo isset($_GET['target']) && $_GET['target']== GasCashBookDetail::TARGET_EMPLOYEE_OFF ? "active":"";?>' href="<?php echo $LinkEmployeeOff;?>">NV nghỉ</a>
        <?php if(in_array($cUid, $model->getUserViewDebit())): ?>
            <a class='btn_cancel f_size_14 <?php echo isset($_GET['target']) && $_GET['target']== GasCashBookDetail::TARGET_EMPLOYEE_DEBIT ? "active":"";?>' href="<?php echo $LinkEmployeeDebit;?>">NV thu nợ</a>
            <a class='btn_cancel f_size_14 <?php echo isset($_GET['target']) && $_GET['target']== GasCashBookDetail::TARGET_SALE ? "active":"";?>' href="<?php echo $linkSale;?>">NV kinh doanh</a>
        <?php endif; ?>
        <a class='btn_cancel f_size_14 <?php echo isset($_GET['target']) && $_GET['target']== GasCashBookDetail::TARGET_EMPLOYEE_BY_AGENT ? "active":"";?>' href="<?php echo $LinkEmployeeByAgent;?>">NV theo đại lý</a>
    </h1> 
</div>