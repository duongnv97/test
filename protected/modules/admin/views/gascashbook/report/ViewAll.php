<?php include 'ViewAllHead.php'; ?>
<?php if(isset($model->aData[GasCashBookDetail::P_OPENING_BALANCE])): ?>
<?php $ARR_OPENING  = $model->aData[GasCashBookDetail::P_OPENING_BALANCE];
    $INCURRED   = isset($model->aData[GasCashBookDetail::P_INCURRED]) ? $model->aData[GasCashBookDetail::P_INCURRED] : [];
    $mAppCache = new AppCache();
    $index = 1; $aAgent = $mAppCache->getAgent(); $sumEnd = 0;
?>
<table class="materials_table hm_table f_size_15 sortable">
    <thead>
        <tr>
            <th class="item_c">#</th>
            <th class="w-80 item_c">Mã</th>
            <th class="w-250 item_c">Đại lý</th>
            <th class="w-150 item_c">Tồn đầu</th>
            <th class="w-150 item_c">Thu</th>
            <th class="w-150 item_c">Chi</th>
            <th class="w-150 item_c td_last_r ClosingBalance">Tồn cuối</th>
        </tr>
    </thead>
    <tbody>
        <?php foreach($model->agent_id as $agent_id):?>
        <?php
            $aInfo          = isset($ARR_OPENING[$agent_id]) ? $ARR_OPENING[$agent_id] : ['revenue'=>0, 'cost'=>0];
            $aInfo['cost']  = isset($aInfo['cost']) ? $aInfo['cost'] : 0;
            $open           = $aInfo['revenue'] - $aInfo['cost'];
            $amountRevenue  = isset($INCURRED[$agent_id]['revenue']) ? $INCURRED[$agent_id]['revenue'] : 0;
            $amountCost     = isset($INCURRED[$agent_id]['cost']) ? $INCURRED[$agent_id]['cost'] : 0;
            $end            = $open + $amountRevenue - $amountCost;
            $sumEnd         += $end;
            if(count($aAgentOfUser) && !in_array($agent_id, $aAgentOfUser)){
                continue;
            }
        ?>
        <tr>
            <td class="item_c order_no"><?php echo $index++;?></td>
            <td class=""><?php echo isset($aAgent[$agent_id]) ? $aAgent[$agent_id]['code_account'] : '';?></td>
            <td class=""><?php echo isset($aAgent[$agent_id]) ? $aAgent[$agent_id]['first_name'] : '';?></td>
            <td class="item_r"><?php echo ActiveRecord::formatCurrencyRound($open);?></td>
            <td class="item_r"><?php echo ActiveRecord::formatCurrencyRound($amountRevenue);?></td>
            <td class="item_r"><?php echo ActiveRecord::formatCurrencyRound($amountCost);?></td>
            <td class="item_r item_b td_last_r"><?php echo ActiveRecord::formatCurrencyRound($end);?></td>
        </tr>
        <?php endforeach;?>
    </tbody>
    <tfoot>
        <tr>
            <td class="item_r item_b" colspan="6">Tổng</td>
            <td class="item_r item_b td_last_r"><?php echo $sumEnd != 0 ? ActiveRecord::formatCurrencyRound($sumEnd) : '';?></td>
        </tr>
    </tfoot>
</table>

<?php endif; ?>