<?php
    $target = GasCashBookDetail::TARGET_AGENT;
    if(isset($_GET['target'])){
        $target    = $_GET['target'];
    }
    $url = Yii::app()->createAbsoluteUrl('admin/gascashbook/viewAll', ['target'=> $target]);
?>
<div class="wide form">
<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=> $url,
	'method'=>'get',
)); ?>
    <?php
        if($target == GasCashBookDetail::TARGET_EMPLOYEE_BY_AGENT):
    ?>
    <div class="row">
            <?php echo $form->hiddenField($model, 'employee_id',
                    array(
                        'class' => 'uid_auto_hidden uid_leave_hidden',
                        'class_update_val'  => 'uid_leave_hidden',
                    )); ?>
            <?php echo $form->labelEx($model, 'employee_id'); ?>
            <?php
            // Widget auto complete search user
            $aData = array(
                'model'             => $model,
                'field_customer_id' => 'employee_id',
                'url'               => Yii::app()->createAbsoluteUrl('admin/ajax/search_user_login', ['PaySalary' => 1]),
                'name_relation_user'        => 'rEmployee',
                'field_autocomplete_name'   => 'autocomplete_user',
                'ClassAdd'                  => 'w-400',
            );
            $this->widget('ext.SpaAutocompleteCustomer.SpaAutocompleteCustomer',
                    array('data' => $aData)
                    );
            ?>
            <?php echo $form->error($model, 'employee_id'); ?>
    </div>
    <?php endif; ?>
    <div class="row more_col">
        <div class="col1">
            <?php echo $form->label($model,'date_from'); ?>
            <?php 
                $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                    'model'=>$model,        
                    'attribute'=>'date_from',
                    'options'=>array(
                        'showAnim'=>'fold',
                        'dateFormat'=> MyFormat::$dateFormatSearch,
//                            'minDate'=> '0',
                        'maxDate'=> '0',
                        'changeMonth' => true,
                        'changeYear' => true,
                        'showOn' => 'button',
                        'buttonImage'=> Yii::app()->theme->baseUrl.'/admin/images/icon_calendar_r.gif',
                        'buttonImageOnly'=> true,                                
                    ),        
                    'htmlOptions'=>array(
                        'class'=>'w-16 date_from',
                        'size'=>'16',
                        'style'=>'float:left;',
                        'readonly'=> 1,
                    ),
                ));
            ?>     		
        </div>
        <div class="col2">
            <?php echo $form->label($model,'date_to'); ?>
            <?php 
                $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                    'model'=>$model,        
                    'attribute'=>'date_to',
                    'options'=>array(
                        'showAnim'=>'fold',
                        'dateFormat'=> MyFormat::$dateFormatSearch,
//                            'minDate'=> '0',
                        'maxDate'=> '0',
                        'changeMonth' => true,
                        'changeYear' => true,
                        'showOn' => 'button',
                        'buttonImage'=> Yii::app()->theme->baseUrl.'/admin/images/icon_calendar_r.gif',
                        'buttonImageOnly'=> true,                                
                    ),        
                    'htmlOptions'=>array(
                        'class'=>'w-16',
                        'size'=>'16',
                        'style'=>'float:left;',
                        'readonly'=> 1,
                    ),
                ));
            ?>     		
        </div>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model,'province_id'); ?>
        <div class="fix-label">
            <?php
               $this->widget('ext.multiselect.JMultiSelect',array(
                     'model'=>$model,
                     'attribute'=>'province_id',
                     'data'=> GasProvince::getArrAll(),
                     // additional javascript options for the MultiSelect plugin
                    'options'=>array('selectedList' => 30,),
                     // additional style
                     'htmlOptions'=>array('style' => 'width: 800px;'),
               ));    
           ?>
        </div>
    </div>

    <div class="row buttons" style="padding-left: 159px;">
        <?php $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType'=>'submit',
            'label'=>'Xem',
            'type'=>'null', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
            'size'=>'small', // null, 'large', 'small' or 'mini'
            //'htmlOptions' => array('style' => 'margin-bottom: 10px; float: right;'),
        )); ?>	
    </div>
<?php $this->endWidget(); ?>

</div><!-- search-form -->