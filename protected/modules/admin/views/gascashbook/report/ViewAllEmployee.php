<?php include 'ViewAllHead.php'; ?>
<?php if(isset($model->aData[GasCashBookDetail::P_OPENING_BALANCE])): ?>
<?php $ARR_OPENING  = $model->aData[GasCashBookDetail::P_OPENING_BALANCE];
    $INCURRED   = isset($model->aData[GasCashBookDetail::P_INCURRED]) ? $model->aData[GasCashBookDetail::P_INCURRED] : [];
    $mAppCache = new AppCache();
    $index = 1; $aAgent = $mAppCache->getAgent(); $sumEnd = 0;
    $ARR_USER   = $model->aData['ARR_USER'];
    $sta2       = new Sta2();
    $aAgentId       = $model->getAgentByProvince();
    $aRoleNotShow   = [ROLE_DRIVER];
    $typeView       = isset($_GET['target']) ? $_GET['target'] : 0;
//    foreach($ARR_USER as $employee_id => $mUser):
//        echo "$mUser->id -- $mUser->parent_id **** ";
//    endforeach;
?>
<table class="materials_table hm_table f_size_15 sortable">
    <thead>
        <tr>
            <th class="item_c">#</th>
            <th class="w-80 item_c">Mã</th>
            <th class="w-2501 item_c">Đại lý</th>
            <th class="w-2501 item_c">Nhân viên</th>
            <th class="item_c">Bộ phận</th>
            <th class="w-100 item_c">Tồn đầu</th>
            <th class="w-100 item_c">Thu</th>
            <th class="w-100 item_c">Chi</th>
            <th class="w-100 item_c td_last_r ClosingBalance">Tồn cuối</th>
        </tr>
    </thead>
    <tbody>
        <?php foreach($ARR_USER as $employee_id => $mUser):?>
        <?php
//            $agent_id       = $mUser->getAgentOfEmployee();// Aug2018 ko lấy theo kiểu này nữa
            $agent_id       = $mUser->parent_id;
            if(!$model->allowAccessAgent($agent_id) || !in_array($agent_id, $aAgentId) || ($typeView == GasCashBookDetail::TARGET_SALE && in_array($mUser->role_id, $aRoleNotShow))){
                continue ;
            }
            $aInfo          = isset($ARR_OPENING[$employee_id]) ? $ARR_OPENING[$employee_id] : ['revenue'=>0, 'cost'=>0];
            $aInfo['cost']  = isset($aInfo['cost']) ? $aInfo['cost'] : 0;
            $open           = $aInfo['revenue'] - $aInfo['cost'];
            $amountRevenue  = isset($INCURRED[$employee_id]['revenue']) ? $INCURRED[$employee_id]['revenue'] : 0;
            $amountCost     = isset($INCURRED[$employee_id]['cost']) ? $INCURRED[$employee_id]['cost'] : 0;
            $end            = $open + $amountRevenue - $amountCost;
            $sumEnd         += $end;
            $first_name = $mUser->first_name;
//            if(in_array($employee_id, $sta2->getUidBlock())){
//                $first_name = "<b>$first_name [Đình chỉ]</b>";
//            }
//            if(count($aAgentOfUser) && !in_array($agent_id, $aAgentOfUser)){
//                continue;
//            }// Apr1918 mở cho KTKV xem tồn quỹ của PVKH chạy hỗ trợ
        ?>
        <tr>
            <td class="item_c order_no"><?php echo $index++;?></td>
            <td class=""><?php echo isset($aAgent[$agent_id]) ? $aAgent[$agent_id]['code_account'] : '';?></td>
            <td class=""><?php echo isset($aAgent[$agent_id]) ? $aAgent[$agent_id]['first_name'] : '';?></td>
            <td class=""><?php echo $first_name;?></td>
            <td class=""><?php echo $mUser->getRoleName();?></td>
            <td class="item_r"><?php echo ActiveRecord::formatCurrencyRound($open);?></td>
            <td class="item_r"><?php echo ActiveRecord::formatCurrencyRound($amountRevenue);?></td>
            <td class="item_r"><?php echo ActiveRecord::formatCurrencyRound($amountCost);?></td>
            <td class="item_r item_b td_last_r"><?php echo ActiveRecord::formatCurrencyRound($end);?></td>
        </tr>
        <?php endforeach;?>
    </tbody>
    <tfoot>
        <tr>
            <td class="item_r item_b" colspan="8">Tổng</td>
            <td class="item_r item_b td_last_r"><?php echo $sumEnd != 0 ? ActiveRecord::formatCurrencyRound($sumEnd) : '';?></td>
        </tr>
    </tfoot>
</table>

<?php endif; ?>
