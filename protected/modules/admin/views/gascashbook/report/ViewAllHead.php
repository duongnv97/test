<?php $this->breadcrumbs=array(
    $this->pageTitle,
);
$target = GasCashBookDetail::TARGET_AGENT;
if(isset($_GET['target'])){
    $target    = $_GET['target'];
}
$menus=array(
    ['label'=> 'Xuất Excel Danh Sách Hiện Tại',
        'url'=>array('viewAll', 'ExportExcelAll'=>1,'target'=>$target), 
        'htmlOptions'=>array('class'=>'export_excel ','label'=>'Xuất Excel'),
    ],
);
$this->menu= ControllerActionsName::createMenusRoles($menus, $actions);
$cRole = MyFormat::getCurrentRoleId();
$cUid = MyFormat::getCurrentUid();
$aAgentOfUser = GasAgentCustomer::initSessionAgentFixForApp($cRole, $cUid);
?>

<?php include 'index_button.php'; ?>
<p><?php echo Sell::getAppTextUpdateHgd();?></p>
<div class="search-form" style="">
<?php $this->renderPartial('report/_search',array(
        'model'=>$model,
)); ?>
</div><!-- search-form -->


<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/sortElements/jquery.sortElements.js"></script>
<!--https://stackoverflow.com/questions/3160277/jquery-table-sort-->
<script>
$(function(){
var table = $('.sortable');
$('.ClosingBalance')
    .wrapInner('<span title="sort this column"/>')
    .each(function(){

        var th = $(this),
            thIndex = th.index(),
            inverse = false;

        th.click(function(){

            table.find('td').filter(function(){

                return $(this).index() === thIndex;

            }).sortElements(function(a, b){
                var aText = $.text([a]);
                var bText = $.text([b]);
                    aText = parseFloat(aText.replace(/,/g, ''));
                    bText = parseFloat(bText.replace(/,/g, ''));
                    aText = aText *1;
                    bText = bText *1;

                if( aText == bText )
                    return 0;

//                return aText > bText ? // asc 
                return aText < bText ? // DESC
                    inverse ? -1 : 1
                    : inverse ? 1 : -1;

            }, function(){

                // parentNode is the element we want to move
                return this.parentNode; 

            });

            inverse = !inverse;
            fnRefreshOrderNumber();
        });
    });
    
    $('.ClosingBalance').trigger('click');
    
});
</script>