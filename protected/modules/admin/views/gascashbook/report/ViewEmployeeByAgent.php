<?php include 'ViewAllHead.php'; ?>
<?php if(isset($model->aData)): ?>
<?php 
    $index      = 1;
    $mAppCache  = new AppCache();
    $aAgent     = $mAppCache->getAgent();
    $sumEnd     = 0;
    $ARR_USER   = $model->aData;
    $typeView   = isset($_GET['target']) ? $_GET['target'] : 0;
?>
<table class="materials_table hm_table f_size_15 sortable">
    <thead>
        <tr>
            <th class="item_c">#</th>
            <th class="w-80 item_c">Mã</th>
            <th class="w-2501 item_c">Đại lý</th>
            <th class="w-2501 item_c">Tồn cuối kỳ</th>
        </tr>
    </thead>
    <tbody>
        <?php foreach($ARR_USER as $employee_id => $aAGentWithClosing):?>
            <?php foreach ($aAGentWithClosing as $agent_id => $mAgent): ?>
            <?php
                $sumEnd += $mAgent['closing'];
            ?>
            <tr>
                <td class="item_c order_no"><?php echo $index++;?></td>
                <td class=""><?php echo isset($aAgent[$agent_id]) ? $aAgent[$agent_id]['code_account'] : '' ;?></td>
                <td class=""><?php echo isset($mAgent['agentName']) ? $mAgent['agentName'] : '';?></td>
                <td class="item_r"><?php echo ActiveRecord::formatCurrencyRound($mAgent['closing']);?></td>
            </tr>
            <?php endforeach; ?>
        <?php endforeach;?>
    </tbody>
    <tfoot>
        <tr>
            <td class="item_r item_b" colspan="3">Tổng</td>
            <td class="item_r item_b td_last_r"><?php echo $sumEnd != 0 ? ActiveRecord::formatCurrencyRound($sumEnd) : '';?></td>
        </tr>
    </tfoot>
</table>

<?php endif; ?>
