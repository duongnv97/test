<?php
$this->breadcrumbs=array(
	'Sổ Quỹ'=>array('index'),
	'Tạo Mới',
);

$menus = array(		
        array('label'=>'Sổ Quỹ', 'url'=>array('index')),
);
$this->menu= ControllerActionsName::createMenusRoles($menus, $actions);
?>

<h1>Sổ Quỹ - Tạo Mới</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>