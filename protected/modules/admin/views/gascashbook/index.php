<?php
$this->breadcrumbs=array(
	'Sổ Quỹ',
);
$cRole = MyFormat::getCurrentRoleId();
$menus = [];
if($cRole != ROLE_SUB_USER_AGENT){
    $menus[] = array('label'=> 'Xuất Excel Danh Sách Hiện Tại',
                    'url'=>array('index', 'ExportExcel'=>1), 
                    'htmlOptions'=>array('class'=>'export_excel ','label'=>'Xuất Excel'),
                );
}
if($model->canCreate()){
    $menus[] = array('label'=>'Sổ Quỹ - Tạo Mới', 'url'=>array('create'));
}
$this->menu= ControllerActionsName::createMenusRoles($menus, $actions);
$mAppCache  = new AppCache();
$aAgent     = $mAppCache->getAgent();
$cRole      = MyFormat::getCurrentRoleId();
$cUid       = MyFormat::getCurrentUid();
$aAgentOfUser = GasAgentCustomer::initSessionAgentFixForApp($cRole, $cUid);
$showForm   = true;
if(!empty($model->agent_id) && count($aAgentOfUser) && !in_array($model->agent_id, $aAgentOfUser)){
    $showForm = false;
}

Yii::app()->clientScript->registerScript('ajaxupdate', "
$('#gas-cash-book-grid a.ajaxupdate').live('click', function() {
    $.fn.yiiGridView.update('gas-cash-book-grid', {
        type: 'POST',
        url: $(this).attr('href'),
        success: function() {
            $.fn.yiiGridView.update('gas-cash-book-grid');
        }
    });
    return false;
});
");
?>

<h1>Sổ Quỹ</h1>


<?php echo CHtml::link('Tìm Kiếm Nâng Cao','#',array('class'=>'search-button')); ?>
<div class="search-form" style="">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<script>
function fnUpdateAgentCashBookDetail(data){
    $('.agentCashBookDetail').html($(data).find('.agentCashBookDetail').html());
}
</script>

<div class="row agentCashBookDetail">
    <?php if( $showForm && (!empty($model->agent_id) || !empty($model->employee_id))): // thêm nút check xem báo cáo chi tiết cho  phần search, default sẽ là select 1 đại lý nào đó hoặc không, khi click vào chi tiết sẽ load thêm grid ?>

            <label>&nbsp</label>
            <?php
            
                $aEmployee      = $mAppCache->getListdataUserByRole(ROLE_EMPLOYEE_MAINTAIN);
                $aCcs           = $mAppCache->getListdataUserByRole(ROLE_EMPLOYEE_MARKET_DEVELOPMENT);
                $aDriver        = $mAppCache->getListdataUserByRole(ROLE_DRIVER);
                $aSale          = $mAppCache->getListdataUserByRole(ROLE_SALE);
                $aEmployee      = $aEmployee + $aCcs + $aDriver + $aSale;
//                $days_in_month      = cal_days_in_month(0, $cMonth, $cYear) ;
//                $date_begin_month   = $cYear."-".$cMonth."-01";            
//                $date_end_month     = $cYear."-".$cMonth."-$days_in_month";
                
                $mCashbookDetail = new GasCashBookDetail();
                $mCashbookDetail->agent_id          = $model->agent_id;
                $mCashbookDetail->employee_id       = $model->employee_id;
                $mCashbookDetail->date_from_ymd     = MyFormat::dateDmyToYmdForAllIndexSearch($model->date_from);;
                $mCashbookDetail->date_to_ymd       = MyFormat::dateDmyToYmdForAllIndexSearch($model->date_to);;
                $mCashbookDetail->release_date      = $mCashbookDetail->date_from_ymd;
                $openingBalance         = $mCashbookDetail->getOpeningBalanceOfAgent();
                $aModelCashBookDetail   = $mCashbookDetail->getDaily();
                $textAgent  = $model->getAgent(). ' - ';
                $title      = "$textAgent Sổ Quỹ $model->date_from đến $model->date_to- Tồn Đầu:  ".ActiveRecord::formatCurrency($openingBalance);
                $aData['title']                 = $title;
                $aData['openingBalance']        = $openingBalance;
                $aData['aModelCashBookDetail']  = $aModelCashBookDetail;
                $_SESSION['data-excel'] = $aData;
    //            if(Yii::app()->user->role_id!=ROLE_AGENT){
    //                $textAgent = $model->agent->first_name. ' - ';
    //            }
            ?>
            <table class="materials_table hm_table">
                <thead>
                    <tr>
                        <th colspan="9" class="thead_background item_c"> <?php echo $title;?></th>
                    </tr>
                    <tr>
                        <th class="order_no item_c">Ngày</th>
                        <th class="w-250 item_c">Diễn Giải</th>
                        <th class="w-250 item_c">Loại</th>
                        <th class="w-100 item_c">Nhân Viên</th>
                        <th class="w-100 item_c">Khách Hàng</th>
                        <th class="w-30 item_c">SL</th>
                        <th class="w-70 item_c">Thu</th>
                        <th class="w-70 item_c">Chi</th>
                        <th class="w-70 item_c last">Tồn</th>
                    </tr>
                </thead>
                <tbody>
                        <?php foreach($aModelCashBookDetail as $key=>$item):?>
                        <?php
                            $aDate = explode('-', $item->release_date);
                            $employeeName = isset($aEmployee[$item->employee_id]) ? $aEmployee[$item->employee_id] : $item->name_employee;
                            $agentName = isset($aAgent[$item->agent_id]) ? $aAgent[$item->agent_id]['first_name'] : '';
                        ?>
                        <tr class="cash_book_row">
                            <td class=" item_c"><?php echo $aDate[2];?></td>
                            <td><?php echo $item->description."<br>$agentName";?></td>
                            <td><?php echo $item->master_lookup->name;?>
                                <br><?php echo $item->getCreatedDate();?>
                            </td>
                            <td><?php echo $employeeName;?></td>
                            <td><?php echo $item->getCustomer();?></td>
                            <td class=" item_c"><?php echo !empty($item->qty) ? ActiveRecord::formatCurrency($item->qty) : '';?></td>
                            <?php foreach (CmsFormatter::$MASTER_TYPE_CASHBOOK as $type_id=>$type_name):?>
                            <td class="item_r">
                                <?php if($type_id == MASTER_TYPE_REVENUE &&  $item->type==MASTER_TYPE_REVENUE) // thu
                                    {
                                        echo ActiveRecord::formatCurrency($item->amount);
                                        $openingBalance+= $item->amount;

                                    }elseif($type_id == MASTER_TYPE_COST &&  $item->type==MASTER_TYPE_COST){ // chi
                                        echo ActiveRecord::formatCurrency($item->amount);
                                        $openingBalance-= $item->amount;
                                    }
                                ?>
                            </td>
                            <?php endforeach;?>
                            <td class="item_r last item_b"><?php echo ActiveRecord::formatCurrency($openingBalance);?></td>
                        </tr>                    
                        <?php endforeach;?>                    
                </tbody>
            </table>            

    <?php endif;?>
</div> <!-- end <div class="row agentCashBookDetail"> -->


<?php if(Yii::app()->user->role_id!=ROLE_SUB_USER_AGENT):?>
    <?php if(Yii::app()->user->role_id==ROLE_ADMIN):?>
        <?php $this->widget('zii.widgets.grid.CGridView', array(
                'id'=>'gas-cash-book-grid',
                'dataProvider'=>$model->search(),
                'afterAjaxUpdate'=>'function(id, data){ fnUpdateAgentCashBookDetail(data); }',
                'template'=>'{pager}{summary}{items}{pager}{summary}', 
        //	'enableSorting' => false,
                //'filter'=>$model,
                'pager' => array(
                    'maxButtonCount'=>  CmsFormatter::$PAGE_MAX_BUTTON,
                ),    
                'columns'=>array(
                array(
                    'header' => 'S/N',
                    'type' => 'raw',
                    'value' => '$this->grid->dataProvider->pagination->currentPage * $this->grid->dataProvider->pagination->pageSize + ($row+1)',
                    'headerHtmlOptions' => array('width' => '10px','style' => 'text-align:center;'),
                    'htmlOptions' => array('style' => 'text-align:center;')
                ),
                        array(
                            'name' => 'cash_book_no',
                            'htmlOptions' => array('style' => 'width:60px;')
                        ),                

                        array(
                            'name' => 'agent_id',
                            'value' => '$data->agent?$data->agent->first_name:""',
                            'visible'=>Yii::app()->user->role_id!=ROLE_AGENT,
                            'htmlOptions' => array('style' => 'text-align:center;width:180px;')
                        ),               
                        array(
                            'name' => 'release_date',
                            'type'=>'date',
                            'htmlOptions' => array('style' => 'text-align:center;width:50px;')
                        ),     
                        array(                
                            'header' => 'Chi Tiết',
                            'type'=>'CashBookDetail',
                            'value'=>  '$data',
                        ),                 
        //                array(
        //                    'name' => 'note',
        //                    'htmlOptions' => array('style' => 'width:100px;')
        //                ),                
                        array(
                            'name' => 'created_date',
                            'type'=>'datetime',
                            'htmlOptions' => array('style' => 'text-align:center;width:50px;')
                        ),    
                    
                    
//                        array(
//                            'name' => 'last_update_time',
//                            'type'=>'datetime',
//                            'visible'=>Yii::app()->user->role_id==ROLE_ADMIN,
//                        ),                 
//                        array(
//                            'name' => 'value_before_update',
//                            'type'=>'html',
//                            'visible'=>Yii::app()->user->role_id==ROLE_ADMIN,
//                        ),
                    
        		array(
        			'header' => 'Actions',
        			'class'=>'CButtonColumn',
                                'template'=> ControllerActionsName::createIndexButtonRoles($actions),
        		),
                ),
        )); ?>
    <?php endif;?>
<?php endif;?>
