<?php
$model->release_date = MyFormat::dateConverYmdToDmy($model->release_date);
$this->breadcrumbs=array(
	'Sổ Quỹ'=>array('index'),
	'Xem Ngày: '.$model->release_date,
);

$menus = array(
	array('label'=>'GasCashBook Management', 'url'=>array('index')),
	array('label'=>'Tạo Mới GasCashBook', 'url'=>array('create')),
	array('label'=>'Cập Nhật GasCashBook', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Xóa GasCashBook', 'url'=>array('delete'), 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Bạn chắc chắn muốn xóa ?')),
);
$this->menu= ControllerActionsName::createMenusRoles($menus, $actions);
?>

<h1>Sổ Quỹ - Xem Ngày: <?php echo $model->release_date; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'cash_book_no',
                array(
                    'name' => 'agent_id',
                    'value' => $model->agent?$model->agent->first_name:"",
//                    'visible'=>Yii::app()->user->role_id!=ROLE_AGENT,
                ),             
		'release_date',
		'note',
		'created_date:datetime',
	),
)); ?>
<h1>Chi Tiết Ngày:  <?php echo $model->release_date;?></h1>
<div class="row">
    <label>&nbsp</label>
    <?php 
    
//    $aCalcOpeningBalance = MyFunctionCustom::getOpeningBalanceOfAgent($model->agent_id, MyFormat::dateConverDmyToYmd($model->release_date)); 
//    $openingBalance = isset($aCalcOpeningBalance['opening_balance'])?$aCalcOpeningBalance['opening_balance']:0;
    $mCashbookDetail = new GasCashBookDetail();
    $mCashbookDetail->agent_id          = $model->agent_id;
    $mCashbookDetail->release_date      = $model->release_date;
    $mCashbookDetail->date_from_ymd     = $mCashbookDetail->release_date;
    $mCashbookDetail->date_to_ymd       = $mCashbookDetail->release_date;
    $openingBalance         = $mCashbookDetail->getOpeningBalanceOfAgent();
    ?>
    <table class="materials_table hm_table">
        <thead>
            <tr>
                <th colspan="9" class="thead_background item_c"> 
                    Sổ Quỹ <?php echo $model->release_date; ?> - Tồn Đầu:  <?php echo ActiveRecord::formatCurrency($openingBalance);?></th>
            </tr>
            <tr>
                <th class="order_no item_c">#</th>
                <th class="w-300 item_c">Diễn Giải</th>
                <th class="w-170 item_c">Loại</th>
                <th class="w-150 item_c">Nhân Viên</th>
                <th class="w-150 item_c">Khách Hàng</th>          
                <th class="w-30 item_c">SL</th>
                <th class="w-80 item_c">Thu</th>
                <th class="w-80 item_c">Chi</th>
                <th class="w-80 item_c last">Tồn</th>
            </tr>
        </thead>
        <tbody>
            <?php if(count($model->CashBookDetail))    :?>
                <?php foreach($model->CashBookDetail as $key=>$item):?>
                <?php if($item->master_lookup):?>
                    <tr class="cash_book_row">
                        <td class=" item_c"><?php echo ($key+1);?></td>
                        <td><?php echo $item->description;?></td>
                        <td><?php echo $item->master_lookup->name;?></td>
                        <td><?php echo $item->name_employee;?></td>
                        <td><?php echo MyFormat::formatNameCustomer($item->customer);?></td>                                                        
                        <td class=" item_c"><?php echo $item->qty;?></td>
                        <?php foreach (CmsFormatter::$MASTER_TYPE_CASHBOOK as $type_id=>$type_name):?>
                        <td class="item_r">
                            <?php if($type_id == MASTER_TYPE_REVENUE &&  $item->type==MASTER_TYPE_REVENUE) // thu
                                {
                                    echo ActiveRecord::formatCurrency($item->amount);
                                    $openingBalance+= $item->amount;

                                }elseif($type_id == MASTER_TYPE_COST &&  $item->type==MASTER_TYPE_COST){ // chi
                                    echo ActiveRecord::formatCurrency($item->amount);
                                    $openingBalance-= $item->amount;
                                }   
                            ?>
                        </td>
                        <?php endforeach;?>
                        <td class="item_r last item_b"><?php echo ActiveRecord::formatCurrency($openingBalance);?></td>
                    </tr>     
                <?php endif;?>
                <?php endforeach;?>                    
            <?php endif;?>                    
        </tbody>
    </table>            
</div>