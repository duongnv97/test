<?php
$this->breadcrumbs=array(
	'Sổ Quỹ'=>array('index'),
	$model->cash_book_no=>array('view','id'=>$model->id),
	'Cập Nhật Ngày: '.$model->release_date,
);

$menus = array(	
        array('label'=>'GasCashBook Management', 'url'=>array('index')),
	array('label'=>'Xem GasCashBook', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Tạo Mới GasCashBook', 'url'=>array('create')),	
);
$this->menu= ControllerActionsName::createMenusRoles($menus, $actions);

?>

<h1>Sổ Quỹ - Cập Nhật Ngày: <?php echo $model->release_date; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>