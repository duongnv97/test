<?php
$this->breadcrumbs=array(
	'Sổ Quỹ Theo TĨnh',
);

$cRole      = MyFormat::getCurrentRoleId();
$mAppCache  = new AppCache();
$aAgentName = $mAppCache->getAgentListdata();
$cRole      = MyFormat::getCurrentRoleId();
$cUid       = MyFormat::getCurrentUid();
$aAgentOfUser = GasAgentCustomer::initSessionAgentFixForApp($cRole, $cUid);
$showForm   = true;
if(!empty($model->agent_id) && count($aAgentOfUser) && !in_array($model->agent_id, $aAgentOfUser)){
    $showForm = false;
}

Yii::app()->clientScript->registerScript('ajaxupdate', "
$('#gas-cash-book-grid a.ajaxupdate').live('click', function() {
    $.fn.yiiGridView.update('gas-cash-book-grid', {
        type: 'POST',
        url: $(this).attr('href'),
        success: function() {
            $.fn.yiiGridView.update('gas-cash-book-grid');
        }
    });
    return false;
});
");
?>

<h1>Sổ Quỹ theo Tĩnh</h1>


<?php echo CHtml::link('Tìm Kiếm Nâng Cao','#',array('class'=>'search-button')); ?>
<div class="search-form" style="">
<?php $this->renderPartial('reportExcelForProvinceID/_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<script>
function fnUpdateAgentCashBookDetail(data){
    $('.agentCashBookDetail').html($(data).find('.agentCashBookDetail').html());
}
</script>

<div class="row agentCashBookDetail">
    <?php 
        $aEmployee      = $mAppCache->getListdataUserByRole(ROLE_EMPLOYEE_MAINTAIN);
        $aCcs           = $mAppCache->getListdataUserByRole(ROLE_EMPLOYEE_MARKET_DEVELOPMENT);
        $aDriver        = $mAppCache->getListdataUserByRole(ROLE_DRIVER);
        $aSale          = $mAppCache->getListdataUserByRole(ROLE_SALE);
        $aEmployee      = $aEmployee + $aCcs + $aDriver + $aSale;
    ?>
    <?php if(isset($aData['Agent'])): ?>
    <?php $aAgent = $aData['Agent'];
    ?>
    <table class="materials_table hm_table">
                
            <?php foreach ($aAgent as $key => $agent_id): ?>
                <thead>
                    <tr>
                        <th colspan="9" class="thead_background item_c"> <?php echo $aData[$agent_id]['title'];?></th>
                    </tr>
                    <tr>
                        <th class="order_no item_c">Ngày</th>
                        <th class="w-250 item_c">Diễn Giải</th>
                        <th class="w-250 item_c">Loại</th>
                        <th class="w-100 item_c">Nhân Viên</th>
                        <th class="w-100 item_c">Khách Hàng</th>
                        <th class="w-30 item_c">SL</th>
                        <th class="w-70 item_c">Thu</th>
                        <th class="w-70 item_c">Chi</th>
                        <th class="w-70 item_c last">Tồn</th>
                    </tr>
                </thead>
                <tbody>
                        <?php foreach($aData[$agent_id]['aModelCashBookDetail'] as $key=>$item):?>
                        <?php
                            $aDate = explode('-', $item->release_date);
                            $employeeName = isset($aEmployee[$item->employee_id]) ? $aEmployee[$item->employee_id] : $item->name_employee;
                            $agentName = isset($aAgentName[$item->agent_id]) ? $aAgentName[$item->agent_id] : '';
                        ?>
                        <tr class="cash_book_row">
                            <td class=" item_c"><?php echo $aDate[2];?></td>
                            <td><?php echo $item->description."<br>$agentName";?></td>
                            <td><?php echo $item->master_lookup->name;?>
                                <br><?php echo $item->getCreatedDate();?>
                            </td>
                            <td><?php echo $employeeName;?></td>
                            <td><?php echo $item->getCustomer();?></td>
                            <td class=" item_c"><?php echo !empty($item->qty) ? ActiveRecord::formatCurrency($item->qty) : '';?></td>
                            <?php foreach (CmsFormatter::$MASTER_TYPE_CASHBOOK as $type_id=>$type_name):?>
                            <td class="item_r">
                                <?php if($type_id == MASTER_TYPE_REVENUE &&  $item->type==MASTER_TYPE_REVENUE) // thu
                                    {
                                        echo ActiveRecord::formatCurrency($item->amount);
                                        $aData[$agent_id]['openingBalance']+= $item->amount;

                                    }elseif($type_id == MASTER_TYPE_COST &&  $item->type==MASTER_TYPE_COST){ // chi
                                        echo ActiveRecord::formatCurrency($item->amount);
                                        $aData[$agent_id]['openingBalance']-= $item->amount;
                                    }
                                ?>
                            </td>
                            <?php endforeach;?>
                            <td class="item_r last item_b"><?php echo ActiveRecord::formatCurrency($aData[$agent_id]['openingBalance']);?></td>
                        </tr>                    
                        <?php endforeach;?>                    
                </tbody>
            <?php endforeach; ?>
        
    </table>         
    <?php endif ;?>
</div>

