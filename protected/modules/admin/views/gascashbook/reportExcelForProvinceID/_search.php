<div class="wide form">

<?php $cRole = MyFormat::getCurrentRoleId();
$form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

        <div class="row more_col">
            <div class="col1">
                <?php echo $form->label($model,'date_from'); ?>
                <?php 
                    $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                        'model'=>$model,        
                        'attribute'=>'date_from',
                        'options'=>array(
                            'showAnim'=>'fold',
                            'dateFormat'=> MyFormat::$dateFormatSearch,
    //                            'minDate'=> '0',
                            'maxDate'=> '0',
                            'changeMonth' => true,
                            'changeYear' => true,
                            'showOn' => 'button',
                            'buttonImage'=> Yii::app()->theme->baseUrl.'/admin/images/icon_calendar_r.gif',
                            'buttonImageOnly'=> true,                                
                        ),        
                        'htmlOptions'=>array(
                            'class'=>'w-16 date_from',
                            'size'=>'16',
                            'style'=>'float:left;',
                            'readonly'=> 1,
                        ),
                    ));
                ?>     		
            </div>
            <div class="col2">
                <?php echo $form->label($model,'date_to'); ?>
                <?php 
                    $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                        'model'=>$model,        
                        'attribute'=>'date_to',
                        'options'=>array(
                            'showAnim'=>'fold',
                            'dateFormat'=> MyFormat::$dateFormatSearch,
    //                            'minDate'=> '0',
                            'maxDate'=> '0',
                            'changeMonth' => true,
                            'changeYear' => true,
                            'showOn' => 'button',
                            'buttonImage'=> Yii::app()->theme->baseUrl.'/admin/images/icon_calendar_r.gif',
                            'buttonImageOnly'=> true,                                
                        ),        
                        'htmlOptions'=>array(
                            'class'=>'w-16',
                            'size'=>'16',
                            'style'=>'float:left;',
                            'readonly'=> 1,
                        ),
                    ));
                ?>     		
            </div>

        <?php if($cRole==ROLE_ADMIN):?>
            <div class="col3">
                <?php echo $form->label($model,'cash_book_no',array()); ?>
                <?php echo $form->textField($model,'cash_book_no',array('class'=>'w-100','maxlength'=>20)); ?>
            </div>
        <?php endif;?>
        </div>
    
        <div class="row">
          <?php echo $form->labelEx($model,'province_id_agent'); ?>
          <div class="fix-label">
              <?php
                 $this->widget('ext.multiselect.JMultiSelect',array(
                       'model'=>$model,
                       'attribute'=>'province_id_agent',
                       'data'=> GasProvince::getArrAll(),
                       // additional javascript options for the MultiSelect plugin
                      'options'=>array('selectedList' => 30,),
                       // additional style
                       'htmlOptions'=>array('style' => 'width: 800px;'),
                 ));    
             ?>
          </div>
        </div>

	<div class="row buttons" style="padding-left: 159px;">
            <?php $this->widget('bootstrap.widgets.TbButton', array(
                'buttonType'=>'submit',
                'label'=>'Xuất excel',
                'type'=>'null', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
                'size'=>'small', // null, 'large', 'small' or 'mini'
                
                //'htmlOptions' => array('style' => 'margin-bottom: 10px; float: right;'),
            )); ?>
        </div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->