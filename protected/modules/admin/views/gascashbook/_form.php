<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'gas-cash-book-form',
	'enableAjaxValidation'=>false,
)); ?>
    <?php $htmlSelect = MyFunctionCustom::buildSelectTypeCashBook('GasCashBookDetail[master_lookup_id][]', true); ?>

	<p class="note">Dòng có dấu <span class="required">*</span> là bắt buộc.</p>
        
        <?php  if(Yii::app()->user->hasFlash('successUpdate')): ?>
            <div class='success_div'><?php echo Yii::app()->user->getFlash('successUpdate');?></div>         
        <?php endif; ?>  	
            	
        <?php if(!$model->isNewRecord ): ?>
	<div class="row">
		<?php echo $form->labelEx($model,'cash_book_no'); ?>
                <label><?php echo $model->cash_book_no;?></label>
		<?php // echo $form->textField($model,'cash_book_no',array('size'=>25,'readonly'=>true)); ?>
                
	</div>
        <div class="clr"></div>
        <?php endif;?>    
            
	<div class="row">
                <?php echo $form->labelEx($model,'release_date'); ?>
                <?php if($model->isNewRecord ): ?>
                    <?php 
                        $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                            'model'=>$model,        
                            'attribute'=>'release_date',
                            'options'=>array(
                                'showAnim'=>'fold',
                                'dateFormat'=> ActiveRecord::getDateFormatJquery(),
                                'minDate'=> '-'.GasCashBook::getAgentDaysAllowUpdate(),
                                'maxDate'=> '0',
                                'yearRange'=> (date('Y')-1).":".date('Y'),
                                'changeMonth' => true,
                                'changeYear' => true,
                                'showOn' => 'button',
                                'buttonImage'=> Yii::app()->theme->baseUrl.'/admin/images/icon_calendar_r.gif',
                                'buttonImageOnly'=> true,                                
                            ),        
                            'htmlOptions'=>array(
                                'class'=>'w-16',
                                'style'=>'height:20px;width:166px;',
                                    'readonly'=>'readonly',
                            ),
                        ));
                    ?>     	
                <?php else:?>   
                    <label><?php echo $model->release_date;?></label>
                    <?php echo $form->hiddenField($model,'release_date'); ?>            
                <?php endif;?>    
		<?php echo $form->error($model,'release_date'); ?>            
	</div>
        
        <div class="clr"></div>
        <div class="row">
            <label>&nbsp</label>
            <div>
                <a href="javascript:void(0);" style="line-height:25px"  class="text_under_none item_b" onclick="fnBuildRow();">
                    <img style="float: left;margin-right:8px;" src="<?php echo Yii::app()->theme->baseUrl;?>/images/add.png"> 
                    Thêm Dòng ( Phím tắt F8 )
                </a>
            </div>
        </div>
        <div class="clr"></div>
            
        <div class="row">
            <label>&nbsp</label>
            <table class="materials_table materials_table_root materials_table_th" style="">
                <thead>
                    <tr>
                        <th class="item_c">#</th>
                        <th class="w-350 item_c">Diễn Giải</th>
                        <th class="w-30 item_c">SL</th>
                        <th class="w-100 item_c">Số Tiền</th>
                        <th class="w-150 item_c">Loại Thu Chi</th>
                        <th class="item_unit item_c last">Xóa</th>
                    </tr>
                </thead>
                <tbody>
                    <?php if(count($model->aModelCashBookDetail)): ?>
                        <?php foreach($model->aModelCashBookDetail as $key=>$item):?>
                        <tr class="cash_book_row row_input">
                            <td class="order_no item_c"><?php echo ($key+1);?></td>
                            <td class="col_customer">
                                <input placeholder="Diễn Giải" name="GasCashBookDetail[description][]" class="w-330" maxlength="300" value="<?php echo $item->description;?>" type="text">
                                <div class="row">
                                    <label class="lb_customer_small">Họ Tên NV</label>
                                    <input name="GasCashBookDetail[name_employee][]" class="w-190 float_l" placeholder="Họ Tên NV" maxlength="100" value="<?php echo $item->name_employee;?>" type="text">
                                </div>
                                <div class="clr"></div>
                                <div class="row row_customer_autocomplete">
                                    <label class="lb_customer_small">Khách Hàng</label>
                                    <div class="float_l">
                                        <input class="customer_id_hide" value="<?php echo $item->customer_id;?>" type="hidden" name="GasCashBookDetail[customer_id][]">
                                        <div class="row">
                                            <input class="chk_family" type="checkbox" <?php if($item->customer_id==ID_KH_HO_GIA_DINH) echo "checked='1'"; ?>>
                                            <label class="lb_customer_small">Hộ gia đình</label>
                                        </div>
                                        <input class="float_l customer_autocomplete w-190"  placeholder="Nhập tên số đt, từ <?php echo MIN_LENGTH_AUTOCOMPLETE; ?> ký tự" maxlength="100" value="<?php echo $item->customer?$item->customer->first_name:'';?>" type="text" <?php if($item->customer) echo 'readonly="1"'; ?>>
                                        <span class="remove_row_item" onclick="fnRemoveName(this);"></span>
                                    </div>
                                </div>
                                <?php 
                                    if($item->customer && $item->customer_id!=ID_KH_HO_GIA_DINH ){
                                        $aData = array(
                                            'class_custom'=>'table_small',
                                            'info_code_bussiness'=>$item->customer->code_bussiness,
                                            'info_name'=>$item->customer->first_name,
                                            'info_address'=>$item->customer->address,
                                            'info_phone'=>$item->customer->phone,
                                        );
                                        $this->widget('ext.GasTableCustomerInfo.GasTableCustomerInfo',
                                            array('data'=>$aData));                                        
                                    }
                                ?>
                            </td>
                            <td><input name="GasCashBookDetail[qty][]" class=" w-55 qty number_only_v1" maxlength="6" value="<?php echo $item->qty;?>" type="text"></td>
                            <td>
                                <input name="GasCashBookDetail[amount][]" class="w-70 amount number_only number_only_v1" value="<?php echo $item->amount;?>" maxlength="15"  type="text">
                                <div class="help_number"></div>
                            </td>
                            <td> <?php echo CHtml::dropDownList('GasCashBookDetail[master_lookup_id][]', $item->master_lookup_id, 
                                        MyFunctionCustom::getDataForSelectTypeCashBook(), array('empty'=>'Select'));?></td>
                            <td class="item_c last"><span remove="<?php echo $item->id;?>" class="remove_icon_only"></span></td>
                        </tr>                    
                        <?php endforeach;?>
                    <?php endif;?>
                    
                </tbody>
            </table>
        </div>
           
        <div class="clr"></div>
        <div class="row">
		<?php // echo $form->labelEx($model,'note'); ?>
		<?php // echo $form->textArea($model,'note',array('style'=>'width:680px;height:40px;')); ?>
	</div>
         
            
        <div class="row buttons" style="padding-left: 141px; padding-top: 15px; padding-bottom: 20px;">
            <?php $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType'=>'submit',
            'label'=>$model->isNewRecord ? 'Create' :'Save',
            'type'=>'null', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
            'size'=>'small', // null, 'large', 'small' or 'mini'
            //'htmlOptions' => array('style' => 'margin-bottom: 10px; float: right;'),
        )); ?>	</div>            
            
        <?php if(!$model->isNewRecord && count($model->aModelCashBookDetail))    :?>
            <div class="row">
                <label>&nbsp</label>
                <?php 
//                    $aCalcOpeningBalance = MyFunctionCustom::getOpeningBalanceOfAgent($model->agent_id, MyFormat::dateConverDmyToYmd($model->release_date)); 
//                    $openingBalance = $aCalcOpeningBalance['opening_balance'];
                    $mCashbookDetail = new GasCashBookDetail();
                    $mCashbookDetail->agent_id          = $model->agent_id;
                    $mCashbookDetail->release_date      = MyFormat::dateConverDmyToYmd($model->release_date);
                    $mCashbookDetail->date_from_ymd     = $mCashbookDetail->release_date;
                    $mCashbookDetail->date_to_ymd       = $mCashbookDetail->release_date;
                    $openingBalance         = $mCashbookDetail->getOpeningBalanceOfAgent();
                ?>
                <table class="materials_table hm_table" style="">
                    <thead>
                        <tr>
                            <th colspan="9" class="thead_background item_c"> 
                                Sổ Quỹ <?php echo $model->release_date; ?> - Tồn Đầu:  <?php echo ActiveRecord::formatCurrency($openingBalance);?></th>
                        </tr>
                        <tr>
                            <th class="item_c">#</th>
                            <th class="w-300 item_c">Diễn Giải</th>
                            <th class="w-100 item_c">Loại</th>
                            <th class="w-80 item_c">Nhân Viên</th>
                            <th class="w-80 item_c">Khách Hàng</th>                            
                            <th class="w-30 item_c">SL</th>
                            <th class="w-100 item_c">Thu</th>
                            <th class="w-100 item_c">Chi</th>
                            <th class="w-100 item_c last">Tồn</th>
                        </tr>
                    </thead>
                    <tbody>
                            <?php foreach($model->aModelCashBookDetail as $key=>$item):?>
                            <?php if($item->master_lookup):?>
                                <tr class="cash_book_row">
                                    <td class="index_row item_c"><?php echo ($key+1);?></td>
                                    <td><?php echo $item->description;?></td>
                                    <td><?php echo $item->master_lookup?$item->master_lookup->name:'';?></td>
                                    <td><?php echo $item->name_employee;?></td>
                                    <td><?php echo MyFormat::formatNameCustomer($item->customer);?></td>                                    
                                    <td class=" item_c"><?php echo $item->qty;?></td>
                                    <?php foreach (CmsFormatter::$MASTER_TYPE_CASHBOOK as $type_id=>$type_name):?>
                                    <td class="item_r">
                                        <?php if($type_id == MASTER_TYPE_REVENUE &&  $item->type==MASTER_TYPE_REVENUE) // thu
                                            {
                                                echo ActiveRecord::formatCurrency($item->amount);
                                                $openingBalance+= $item->amount;

                                            }elseif($type_id == MASTER_TYPE_COST &&  $item->type==MASTER_TYPE_COST){ // chi
                                                echo ActiveRecord::formatCurrency($item->amount);
                                                $openingBalance-= $item->amount;
                                            }   
                                        ?>
                                    </td>
                                    <?php endforeach;?>
                                    <td class="item_r last item_b"><?php echo ActiveRecord::formatCurrency($openingBalance);?></td>
                                </tr>                 
                            <?php endif;?>
                            <?php endforeach;?>                    
                    </tbody>
                </table>            
            </div>
        <?php endif;?>
            


<?php $this->endWidget(); ?>

</div><!-- form -->

<?php Yii::app()->clientScript->registerCoreScript('jquery.ui'); ?>
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/jquery.floatThead.min.js"></script>    
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/underscore-min.js"></script>    
<script>
    $(document).keydown(function(e) {
        if(e.which == 119) {
            fnBuildRow();
        }
    });
    
    
    $(document).ready(function(){
        $('.form').find('button:submit').live('click', function(){
            var msg = isValidSubmit();
            if( msg!='' )
            {
                showMsgWarning(msg); return false;
            }
            $.blockUI({ overlayCSS: { backgroundColor: '#fff' } }); 
        });
        
        $('.remove_icon_only').live('click',function(){
            if(confirm('Bạn chắc chắn muốn xóa?')){
                $(this).closest('tr.cash_book_row').remove();
                fnRefreshOrderNumber();
                var id = $(this).attr('remove');
                var className = 'GasCashBookDetail';
                fnRemoveGasCashBookDetail(id,className);
            }
        });
        
        
        fnChangeReleaseDate();
        fnBindAllAutocomplete();
        fnChkFamilyClick();
        $('.materials_table').floatThead();
        $('.number_only_v1').trigger('change');
    });
    
    function fnRemoveGasCashBookDetail(id,className){
        var url_ = "<?php echo Yii::app()->createAbsoluteUrl('admin/ajax/Delete_model');?>";
        $.ajax({
                url: url_,
                data: {id:id,className:className},
                type: "post",
                success: function(data){}
        });        
    }
    
    function isValidSubmit(){
        var msg = '';
        $('.row_input').each(function(){
            var select_ = $(this).find('select');
            var amount_ = $(this).find('.amount');
            if($.trim(amount_.val())==''){
                msg = 'Chưa nhập số tiền';
                amount_.addClass('error');
                return false;
            }
            
            if(  parseInt(amount_.val())*1==0 ){
                msg = 'Số tiền phải lớn hơn 0';
                amount_.addClass('error');
                return false;
            }else{
                amount_.removeClass('error');
            }
            
            if( $.trim(select_.val())== '' ){                
                msg = 'Chưa chọn loại thu chi';                
                select_.addClass('error');
                return false;
            }else{
                select_.removeClass('error');
            }
        });
        
        return msg;
    }
    
    
    function fnBuildRow(){
        var _tr = '';
        _tr += '<tr class="cash_book_row row_input">';
            _tr += '<td class="item_c order_no">1</td>';
            _tr += '<td class="col_customer">';
                _tr += '<input placeholder="Diễn Giải" name="GasCashBookDetail[description][]" class="w-330" maxlength="300" value="" type="text">';
                _tr += '<div class="row">';
                    _tr += '<label class="lb_customer_small">Họ Tên NV</label>';
                    _tr += '<input name="GasCashBookDetail[name_employee][]" class="w-190 float_l" placeholder="Họ Tên NV" maxlength="100" value="" type="text">';
                _tr += '</div>';
                _tr += '<div class="row row_customer_autocomplete">';
                    _tr += '<label class="lb_customer_small">Khách Hàng</label>';
                    _tr += '<div class="float_l">';
                        _tr += '<input class="customer_id_hide" type="hidden" name="GasCashBookDetail[customer_id][]">';
                        _tr += '<div class="row">';
                            _tr += '<input class="chk_family" type="checkbox">';
                            _tr += '<label class="lb_customer_small">Hộ gia đình</label>';
                        _tr += '</div>';
                        _tr += '<input class="float_l customer_autocomplete w-190"  placeholder="Nhập mã, tên hoặc số đt" maxlength="100" value="" type="text">';
                        _tr += '<span class="remove_row_item" onclick="fnRemoveName(this);"></span>';
                    _tr += '</div>';
                _tr += '</div>';

            _tr += '</td>';
            _tr += '<td><input name="GasCashBookDetail[qty][]" class=" w-55 qty number_only_v1" maxlength="6" value="" type="text"></td>';
            _tr += '<td>';
                _tr += '<input name="GasCashBookDetail[amount][]" class="w-70 amount number_only number_only_v1" value="" maxlength="15"  type="text">';
                _tr += '<div class="help_number"></div>';
            _tr += '</td>';
             _tr += '<td><?php echo $htmlSelect;?></td>';
            _tr += '<td class="item_c last"><span class="remove_icon_only"></span></td>';
        _tr += '</tr>';
        
        $('.materials_table_root tbody:first').append(_tr);
        fnRefreshOrderNumber();
        bindEventForHelpNumber();
        fnBindAllAutocomplete();
    }
    
//    function fnRefreshOrderNumber(){ da move ve file huong minh.js
//        validateNumber();
//        var index = 1;
//        $('.materials_table_root').find('.cash_book_row').each(function(){
//            $(this).find('.index_row').text(index++);
//        });
//    }
    
    function fnChangeReleaseDate(){
        $('#GasCashBook_release_date').change(function(){
          var  url_ = '<?php echo Yii::app()->createAbsoluteUrl('admin/gascashbook/create')?>';
          var release_date = $(this).val();
          $.blockUI({ overlayCSS: { backgroundColor: '#fff' } }); 
          $.ajax({
              url: url_,
              data:{'release_date':release_date,'from_ajax':1},
              dataType:'json',
              type:'post',
              success:function(data){
                  if(data['success'])
                    window.location = data['next'];
                  $.unblockUI();
              }
          });
        });
    }
    
    function fnBindAllAutocomplete(){
        $('.customer_autocomplete').each(function(){
            fnBindAutocomplete($(this));
        });
    }
    
    // to do bind autocompelte for input
    // @param objInput : is obj input ex  $('.customer_autocomplete')
    function fnBindAutocomplete(objInput){
        var parent_div = objInput.closest('td.col_customer');
        objInput.autocomplete({
            source: '<?php echo Yii::app()->createAbsoluteUrl('admin/ajax/search_user_cash_book');?>',
            minLength: '<?php echo MIN_LENGTH_AUTOCOMPLETE;?>',
            close: function( event, ui ) { 
                //$( "#GasStoreCard_materials_name" ).val(''); 
            },
            search: function( event, ui ) { 
                    objInput.addClass('grid-view-loading-gas');
            },
            response: function( event, ui ) { 
                objInput.removeClass('grid-view-loading-gas');
                var json = $.map(ui, function (value, key) { return value; });
                if(json.length<1){
                    var error = '<div class=\'errorMessage clr autocomplete_name_text\'>Không tìm thấy dữ liệu.</div>';
                    if(parent_div.find('.autocomplete_name_text').size()<1){
                        parent_div.find('.remove_row_item').after(error);
                    }
                    else
                        parent_div.find('.autocomplete_name_text').show();
                }                    
                    
            },
            select: function( event, ui ) {
                objInput.attr('readonly',true);
                parent_div.find('.autocomplete_name_text').hide();   
                parent_div.find('.customer_id_hide').val(ui.item.id);   
                parent_div.find('.autocomplete_customer_info').remove();   
                var tableInfo = fnBuildTableCustomerInfo(ui.item.code_bussiness, ui.item.name_customer, ui.item.address, ui.item.phone);
                parent_div.append(tableInfo);
                parent_div.find('.autocomplete_customer_info').show();
            }

        });
    }
    
    function fnRemoveName(this_){
        var parent_div = $(this_).closest('td.col_customer');
        if(parent_div.find('.chk_family').is(':checked')) 
        {
            parent_div.find('.chk_family').trigger('click');
        }
        parent_div.find('.customer_autocomplete').attr("readonly",false);                                     
        parent_div.find('.customer_autocomplete').val("");             
        parent_div.find('.customer_id_hide').val("");
        parent_div.find('.autocomplete_customer_info').hide();             
    }
    
    function fnBuildTableCustomerInfo(info_code_bussiness, info_name, info_address, info_phone){
        var table = '';
        table += '<div class="autocomplete_customer_info table_small" style="">';
            table += '<table>';
                table += '<tr>';
                    table += '<td class="_l td_first_t">Mã KH:</td>';
                    table += '<td class="_r info_code_bussiness td_last_r td_first_t">'+info_code_bussiness+'</td>';
                table += '</tr>';

                table += '<tr>';
                    table += '<td class="_l">Tên KH:</td>';
                    table += '<td class="_r info_name td_last_r">'+info_name+'</td>';
                table += '</tr>';
                table += '<tr>';
                    table += '<td class="_l">Địa chỉ:</td>';
                    table += '<td class="_r info_address td_last_r">'+info_address+'</td>';
                table += '</tr>';
                table += '<tr>';
                    table += '<td class="_l">Điện Thoại:</td>';
                    table += '<td class="_r info_phone td_last_r">'+info_phone+'</td>';
                table += '</tr>';
            table += '</table>';
        table += '</div>';
        table += '<div class="clr"></div';
        return table;
    }
    
    function fnChkFamilyClick(){
        var id_ho_gia_dinh = <?php echo ID_KH_HO_GIA_DINH;?>;
        var text = 'KH trả tiền ngay - Hộ gia đình'
        $('.chk_family').live('click',function(){
            var parent_div = $(this).closest('td.col_customer');
            if($(this).is(':checked')){
                parent_div.find('.customer_autocomplete').attr("readonly",true);                                     
                parent_div.find('.customer_autocomplete').val(text);             
                parent_div.find('.customer_id_hide').val(id_ho_gia_dinh);
                parent_div.find('.autocomplete_customer_info').hide();                             
            }else{
                parent_div.find('.customer_autocomplete').attr("readonly",false);                                     
                parent_div.find('.customer_autocomplete').val('');             
                parent_div.find('.customer_id_hide').val('');
                parent_div.find('.autocomplete_customer_info').hide();
            }
                
        });
        
    }
    
</script>


<style>
    /*.row_customer_autocomplete { display: none !important;}*/
</style>