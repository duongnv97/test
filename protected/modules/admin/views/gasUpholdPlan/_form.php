<div class="form">
    <?php $form=$this->beginWidget('CActiveForm', array(
        'id'=>'homecontract-form',
        'enableAjaxValidation'=>false,
               'htmlOptions' => array('enctype' => 'multipart/form-data'),
        )); ?>
    <p class="note">Dòng có dấu <span class="required">*</span> là bắt buộc.</p>
    <?php echo MyFormat::BindNotifyMsg(); ?>
    
    <div class="row">
        <?php echo $form->labelEx($model,'start_date'); ?>
         <?php 
            $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                'model'=>$model,        
                'attribute'=>'start_date',
                'options'=>array(
                    'showAnim'=>'fold',
                    'dateFormat'=> MyFormat::$dateFormatSearch,
                    'changeMonth' => true,
                    'changeYear' => true,
                    'showOn' => 'button',
                    'buttonImage'=> Yii::app()->theme->baseUrl.'/admin/images/icon_calendar_r.gif',
                    'buttonImageOnly'=> true,                                
                ),        
                'htmlOptions'=>array(
                    'class'=>'w-16',
                    'style'=>'height:20px;',
                        'readonly'=>'readonly',
                ),
            ));
            ?>  
        <?php echo $form->error($model,'start_date'); ?>
    </div>
    
    <div class="row">
        <?php echo $form->labelEx($model,'end_date'); ?>
         <?php 
            $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                'model'=>$model,        
                'attribute'=>'end_date',
                'options'=>array(
                    'showAnim'=>'fold',
                    'dateFormat'=> MyFormat::$dateFormatSearch,
                    'changeMonth' => true,
                    'changeYear' => true,
                    'showOn' => 'button',
                    'buttonImage'=> Yii::app()->theme->baseUrl.'/admin/images/icon_calendar_r.gif',
                    'buttonImageOnly'=> true,                                
                ),        
                'htmlOptions'=>array(
                    'class'=>'w-16',
                    'style'=>'height:20px;',
                        'readonly'=>'readonly',
                ),
            ));
            ?>  
        <?php echo $form->error($model,'end_date'); ?>
    </div>
    
    <div class="row ">
    <?php echo $form->labelEx($model,'employee_id'); ?>
    <?php echo $form->hiddenField($model,'employee_id'); ?>
    <?php
        // 1. limit search kh của sale
        $url = Yii::app()->createAbsoluteUrl('admin/ajax/search_by_role', array('role'=> implode(',', GasUphold::$ROLE_EMPLOYEE_SEARCH_AUTOCOMPLETE)));
        // widget auto complete search user customer and supplier
        $aData = array(
            'model'=>$model,
            'field_customer_id'=>'employee_id',
            'url'=> $url,
            'name_relation_user'=>'rEmployee',
            'ClassAdd' => 'w-300',
            'field_autocomplete_name' => 'autocomplete_employee', // tên biến truyền vào
            'placeholder'=>'Nhập mã hoặc tên nhân viên',
        );
        $this->widget('ext.SpaAutocompleteCustomer.SpaAutocompleteCustomer',
            array('data'=>$aData));
        ?>
    <?php echo $form->error($model,'employee_id'); ?>
    </div>
    <div class="row customerRow">
    <?php echo $form->labelEx($model,'customer_id'); ?>
    <?php echo $form->hiddenField($model,'customer_id'); ?>
    <?php
        // 1. limit search kh của sale
        $url = Yii::app()->createAbsoluteUrl('admin/ajax/search_user_by_code');
        // widget auto complete search user customer and supplier
        $aData = array(
            'model'=>$model,
            'field_customer_id'=>'customer_id',
            'url'=> $url,
            'name_relation_user'=>'rCustomer',
            'ClassAdd' => 'w-300',
            'field_autocomplete_name' => 'autocomplete_customer', // tên biến truyền vào
            'placeholder'=>'Nhập mã hoặc tên khách hàng',
            
        );
//        add if create
        if($model->scenario !== 'update') {
            $aData['fnSelectCustomerV2'] = 'fnBuildRow';
            $aData['ShowTableInfo'] = '0';
            $aData['doSomethingOnClose'] = 'fnRemoveAfterSelect';
        }
        
        $this->widget('ext.SpaAutocompleteCustomer.SpaAutocompleteCustomer',
            array('data'=>$aData));
        ?>
    <?php echo $form->error($model,'customer_id'); ?>
    </div>
    
    <?php if($model->scenario !== 'update') { ?>
    <div>
        
        <table class="materials_table hm_table" style="width:80%;">
        <thead>
            <tr>
                <th class="item_c" style="width:10%;">#</th>
                <th class="item_c" style="width:40%;">Khách hàng</th>
                <th class="item_c" style="width:40%;">Địa chỉ</th>
                <th class="item_c" style="width:10%;">Xóa</th>
            </tr>
        </thead>
        <tbody>
            <!--danh sách-->
            
        </tbody>
    </table>
    </div>
    <?php } ?>
    <div class="row buttons" style="padding-left: 141px;">
        <?php $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType'=>'submit',
            'label'=>$model->isNewRecord ? 'Create' :'Save',
            'type'=>'null', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
            'size'=>'small', // null, 'large', 'small' or 'mini'
            //'htmlOptions' => array('style' => 'margin-bottom: 10px; float: right;'),
            )); ?>
    </div>
    <?php $this->endWidget(); ?>
</div>
<?php if($model->scenario !== 'update') { ?>
<script>
function fnBuildRow($ui, $idField, $idFieldCustomer){
    if(issetCustomer($ui.item.id)){
        return;
    }
    $current = $('.materials_table').find('.order_no').length;
    $strNewRow = '<tr class="">';
        $strNewRow += '<td class="item_c order_no">';
        $strNewRow += ++$current;
        $strNewRow += '</td>';
        $strNewRow += '<td class="">';
        $strNewRow += $ui.item.label;
        $strNewRow += '<input name="GasUpholdPlan[customer][]" type="hidden" value="'+$ui.item.id+'">';
        $strNewRow += '</td>';
        $strNewRow += '<td class="item_c">';
        $strNewRow += $ui.item.address;
        $strNewRow += '</td>';
        $strNewRow += '<td class="item_c">';
        $strNewRow += '<span class="remove_icon_only"></span>';
        $strNewRow += '</td>';
    $strNewRow += '</tr>';
    $('.materials_table').find('tbody').append($strNewRow);
}
function fnRemoveAfterSelect(ui,idField, idFieldCustomer){
    $(idField).closest('div.unique_wrap_autocomplete').find('.remove_row_item').click();
}
function issetCustomer($idCustomer) {
    $return = false;
    $('input[name="GasUpholdPlan[customer][]"]').each(function(){
       if($idCustomer === $(this).val()) {
           $return = true;
           return;
       } 
    });
    return $return;
}
</script>
<script>
    $(document).ready(function(){
        fnBindRemoveIcon();
    })
</script>
<?php } ?>
