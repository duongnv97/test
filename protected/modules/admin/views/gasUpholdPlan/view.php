<?php

$menus = array(
	array('label'=>"$this->pluralTitle Management", 'url'=>array('index')),
	array('label'=>"Tạo Mới $this->singleTitle", 'url'=>array('create')),
	array('label'=>"Cập Nhật $this->singleTitle", 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>"Xóa $this->singleTitle", 'url'=>array('delete'), 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Bạn chắc chắn muốn xóa ?')),
);
$this->menu= ControllerActionsName::createMenusRoles($menus, $actions);
?>

<h1><?php echo $this->singleTitle.": ".$model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
            array(
                    'name' =>'start_date',
                    'type'=>'raw',
                    'value' => $model->getStartDate(),
                    'htmlOptions' => array('style' => 'text-align:center;')
                ),
            array(
                    'name' =>'end_date',
                    'type'=>'raw',
                    'value' => $model->getEndDate(),
                    'htmlOptions' => array('style' => 'text-align:center;')
                ),
            array(
                    'name' =>'employee_id',
                    'type'=>'raw',
                    'value' => $model->getEmployee(),
                    'htmlOptions' => array('style' => 'text-align:center;')
                ),
            array(
                    'name' =>'customer_id',
                    'type'=>'raw',
                    'value' => $model->getCustomer(),
                    'htmlOptions' => array('style' => 'text-align:center;')
                ),
            array(
                'name'  => 'created_date',
                'value'  => $model->getCreatedDate(),
                'htmlOptions' => array('style' => 'text-align:center;')
            ),
    ),
));
?>
