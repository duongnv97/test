<div class="wide form">
<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=> Yii::app()->createUrl($this->route),
	'method'=>'get',
)); 

?>  
    <div class="row more_col">
        <div class="col1">
            <?php echo $form->label($model,'uphold_id', ['label' => 'Mã bảo trì']); ?>
            <?php echo $form->textField($model,'uphold_id',array('class'=>'w-200','maxlength'=>50)); ?>
        </div>
    </div>
    
    <div class="row">
        <?php echo $form->labelEx($model,'Từ ngày',array('class'=>'')); ?>
        <?php // echo $form->labelEx($model,'start_date', ['label'=>'Bảo trì']); ?>
        <div style="display: inline-block">
        <?php // echo $form->labelEx($model,'Từ ngày',array('class'=>'w-80')); ?>
        <?php 
            $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                'model'=>$model,        
                'attribute'=>'start_date',
                'options'=>array(
                    'showAnim'=>'fold',
                    'dateFormat'=> MyFormat::$dateFormatSearch,
                    'changeMonth' => true,
                    'changeYear' => true,
                    'showOn' => 'button',
                    'buttonImage'=> Yii::app()->theme->baseUrl.'/admin/images/icon_calendar_r.gif',
                    'buttonImageOnly'=> true,                                
                ),        
                'htmlOptions'=>array(
                    'class'=>'w-16',
                    'style'=>'height:20px;',
                ),
            ));
        ?>
        </div>
        <div style="display: inline-block">
        <?php echo $form->labelEx($model,'Đến ngày',array('class'=>'')); ?>
        <?php 
            $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                'model'=>$model,        
                'attribute'=>'end_date',
                'options'=>array(
                    'showAnim'=>'fold',
                    'dateFormat'=> MyFormat::$dateFormatSearch,
                    'changeMonth' => true,
                    'changeYear' => true,
                    'showOn' => 'button',
                    'buttonImage'=> Yii::app()->theme->baseUrl.'/admin/images/icon_calendar_r.gif',
                    'buttonImageOnly'=> true,                                
                ),        
                'htmlOptions'=>array(
                    'class'=>'w-16',
                    'style'=>'height:20px;',
                ),
            ));
        ?>
        </div>
        <?php echo $form->error($model,'date_expired'); ?>
    </div>   
    <div class="row ">
    <?php echo $form->labelEx($model,'employee_id'); ?>
    <?php echo $form->hiddenField($model,'employee_id'); ?>
    <?php
        // 1. limit search kh của sale
        $url = Yii::app()->createAbsoluteUrl('admin/ajax/search_by_role', array('role'=> implode(",", GasUphold::$ROLE_EMPLOYEE_SEARCH_AUTOCOMPLETE)));
        // widget auto complete search user customer and supplier
        $aData = array(
            'model'=>$model,
            'field_customer_id'=>'employee_id',
            'url'=> $url,
            'name_relation_user'=>'rEmployee',
            'ClassAdd' => 'w-300',
            'field_autocomplete_name' => 'autocomplete_employee', // tên biến truyền vào
            'placeholder'=>'Nhập mã hoặc tên nhân viên',
        );
        $this->widget('ext.SpaAutocompleteCustomer.SpaAutocompleteCustomer',
            array('data'=>$aData));
        ?>
    <?php echo $form->error($model,'agent_id'); ?>
    </div>
    <div class="row customerRow">
    <?php echo $form->labelEx($model,'customer_id'); ?>
    <?php echo $form->hiddenField($model,'customer_id'); ?>
    <?php
        // 1. limit search kh của sale
        $url = Yii::app()->createAbsoluteUrl('admin/ajax/search_user_by_code');
        // widget auto complete search user customer and supplier
        $aData = array(
            'model'=>$model,
            'field_customer_id'=>'customer_id',
            'url'=> $url,
            'name_relation_user'=>'rCustomer',
            'ClassAdd' => 'w-300 customer_field',
            'field_autocomplete_name' => 'autocomplete_customer', // tên biến truyền vào
            'placeholder'=>'Nhập mã hoặc tên khách hàng',
        );
        $this->widget('ext.SpaAutocompleteCustomer.SpaAutocompleteCustomer',
            array('data'=>$aData));
        ?>
    <?php echo $form->error($model,'customer_id'); ?>
    </div>
    
    
    <div class="row more_col">
            <div class="col1">
                <?php echo $form->labelEx($model,'viewExpired'); ?>
                <?php echo $form->dropDownList($model,'viewExpired', ActiveRecord::getYesNo(),array('class'=>'w-200','empty'=>'Select')); ?>
            </div>
            <div class="col3">
            </div>
            
        </div>
    
    <div class="row buttons" style="padding-left: 159px;">
        <?php $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType'=>'submit',
            'label'=>'Search',
            'type'=>'null', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
            'size'=>'small', // null, 'large', 'small' or 'mini'
            //'htmlOptions' => array('style' => 'margin-bottom: 10px; float: right;'),
        )); ?>	
    </div>
<?php $this->endWidget(); ?>

</div><!-- search-form -->