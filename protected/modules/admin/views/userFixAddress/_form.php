<div class="form">

    <?php $form = $this->beginWidget('CActiveForm', array(
        'id' => 'user-fix-address-form',
        'enableAjaxValidation' => false,
    )); ?>

    <?php echo Yii::t('translation', '<p class="note">Fields with <span class="required">*</span> are required.</p>'); ?>

    <div class="row">
        <?php /** @var UsersFixAddress $model */
        echo Yii::t('translation', $form->labelEx($model, 'address_old')); ?>
        <?php echo $model->address_old; ?>
    </div>

    <div class="row">
        <?php echo Yii::t('translation', $form->labelEx($model, 'address_new')); ?>
        <?php // echo $form->textField($model,'name',array('style'=>'width:550px;')); ?>
        <?php echo $form->textArea($model, 'address_new', array('style' => 'width:700px;', 'rows' => 3, 'cols' => 50)); ?>
        <?php echo $form->error($model, 'address_new'); ?>
    </div>

    <div class="row">
        <?php echo Yii::t('translation', $form->labelEx($model, 'uid_login_sell')); ?>
        <?php echo $model->getSaleUser() . '&nbsp;'; ?>
    </div>

    <div class="row">
        <?php echo Yii::t('translation', $form->labelEx($model, 'employee_maintain_id')); ?>
        <?php echo $model->getEmployeeMaintain() . '&nbsp;'; ?>
    </div>

    <div class="row">
        <?php echo Yii::t('translation', $form->labelEx($model, 'last_update')); ?>
        <?php echo $model->last_update . '&nbsp;'; ?>
    </div>

    <div class="row">
        <label>&nbsp;</label>
        <div style="float: left;">
            <?php echo Yii::app()->params['note_type_pay']; ?>
        </div>
    </div>
    <div class="clr"></div>

    <div class="row buttons" style="padding-left: 115px;">
        <?php $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType' => 'submit',
            'label' => $model->isNewRecord ? Yii::t('translation', 'Create') : Yii::t('translation', 'Save'),
            'type' => 'null', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
            'size' => 'small', // null, 'large', 'small' or 'mini'
            //'htmlOptions' => array('style' => 'margin-bottom: 10px; float: right;'),
        )); ?>

        <?php
        if (!$model->isNewRecord && ControllerActionsName::is):
            $this->widget('bootstrap.widgets.TbButton', array(
                'buttonType' => 'button',
                'label' => Yii::t('translation', 'Confirm'),
                'type' => 'primary', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
                'size' => 'small', // null, 'large', 'small' or 'mini'
                //'htmlOptions' => array('style' => 'margin-bottom: 10px; float: right;'),
            ));
        endif; ?>
    </div>

    <?php $this->endWidget(); ?>

</div><!-- form -->