<?php
/** @var UsersFixAddress $model */
$this->breadcrumbs = array(
    Yii::t('translation', 'Danh sách cập nhật địa chỉ khách hàng') => array('index'),
    $model->getCustomer() => array('view', 'id' => $model->id),
    Yii::t('translation', 'Update'),
);

$menus = array(
    array('label' => Yii::t('translation', 'User Address Management'), 'url' => array('index')),
    array('label' => Yii::t('translation', 'View User Address'), 'url' => array('view', 'id' => $model->id)),
    array('label' => Yii::t('translation', 'Create User Address'), 'url' => array('create')),
);
$this->menu = ControllerActionsName::createMenusRoles($menus, $actions);

?>

    <h1><?php echo Yii::t('translation', 'Update địa chỉ khách hàng: ' . $model->getCustomer()); ?></h1>

<?php echo $this->renderPartial('_form', array('model' => $model)); ?>