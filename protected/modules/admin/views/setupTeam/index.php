<?php
$this->breadcrumbs=array(
	$this->singleTitle,
);

$menus=array(
            array('label'=>"Create $this->singleTitle", 'url'=>array('create')),
);
$this->menu= ControllerActionsName::createMenusRoles($menus, $actions);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('setup-team-grid', {
		data: $(this).serialize()
	});
	return false;
});
");

Yii::app()->clientScript->registerScript('ajaxupdate', "
$('#setup-team-grid a.ajaxupdate').live('click', function() {
    $.fn.yiiGridView.update('setup-team-grid', {
        type: 'POST',
        url: $(this).attr('href'),
        success: function() {
            $.fn.yiiGridView.update('setup-team-grid');
        }
    });
    return false;
});
");
$cRole = MyFormat::getCurrentRoleId();
?>

<h1>Danh Sách <?php echo $this->pluralTitle; ?></h1>
<?php if($cRole == ROLE_ADMIN): ?>
    <?php $this->widget('NotContactButtonWidget'); ?>
<?php endif; ?>

<div class="search-form">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php 
$this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'setup-team-grid',
	'dataProvider'=>$model->search(),
        'afterAjaxUpdate'=>'function(id, data){ fnUpdateColorbox();}',
        'template'=>'{pager}{summary}{items}{pager}{summary}',
//    'itemsCssClass' => 'items custom_here',
//    'htmlOptions'=> array('class'=>'grid-view custom_here'),
        'pager' => array(
            'maxButtonCount'=>  CmsFormatter::$PAGE_MAX_BUTTON,
        ),
	'enableSorting' => false,
	//'filter'=>$model,
	'columns'=>array(
        array(
            'header' => 'S/N',
            'type' => 'raw',
            'value' => '$this->grid->dataProvider->pagination->currentPage * $this->grid->dataProvider->pagination->pageSize + ($row+1)',
            'headerHtmlOptions' => array('width' => '10px','style' => 'text-align:center;'),
            'htmlOptions' => array('style' => 'text-align:center;')
        ),
            array(
                'name'  => 'type',
                'value' => '$data->getType()',
            ),
            array(
                'name'  => 'user_id',
                'value' => '$data->getUser()',
            ),
            array(
                'name'  => 'date_apply',
                'value' => '$data->getDateApply()',
                'htmlOptions' => array('style' => 'text-align:center;')
            ),
            array(
                'name'  => 'json',
                'type'  => 'raw',
                'value' => '$data->getJsonView()',
            ),
            array(
                'name'  => 'note',
                'type'  => 'raw',
                'value' => '$data->getNote()',
            ),
            array(
                'name'  => 'created_by',
                'value' => '$data->getCreatedBy()',
            ),
            array(
                'name'  => 'created_date',
                'value' => '$data->getCreatedDate()',
            ),
            array(
                'header' => 'Actions',
                'class'=>'CButtonColumn',
                'template'=> ControllerActionsName::createIndexButtonRoles($actions),
                'buttons'=>array(
                    'update' => array(
                        'visible' => '$data->canUpdate()',
                    ),
                    'delete'=>array(
                        'visible'=> 'GasCheck::canDeleteData($data)',
                    ),
                ),
            ),
	),
)); 
?>
 
<script>
$(document).ready(function() {
    fnUpdateColorbox();
});

function fnUpdateColorbox(){   
    fixTargetBlank();
}
</script>