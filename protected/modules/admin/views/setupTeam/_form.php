<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'setup-team-form',
	'enableAjaxValidation'=>false,
        'htmlOptions' => array('enctype' => 'multipart/form-data'),
//        'action'=> Yii::app()->createAbsoluteUrl("admin/setupTeam/create"),
)); ?>

    <?php echo MyFormat::BindNotifyMsg(); ?>
    
    <?php if($model->isNewRecord): ?>
        <?php include '_formSearch.php'; ?>
    <?php else: ?>
    <div class="row">
        <label>Nhân viên:</label>
        <?php echo $model->getUser();?>
    </div>
    <div class="row">
        <label>Tháng:</label>
        <?php echo $model->getDateApplyShort();?>
    </div>
    <?php endif; ?>
    
    <?php if($model->canShowFormInput()): ?>
    <div class="row">
        <?php 
        $lbl = "Nhân viên";
        $placeholder = "Nhập mã KH/NCC, Số ĐT. Tối thiểu 2 ký tự";
        if($model->type == SetupTeam::TYPE_KTKV_AGENT){
            $lbl = "Đại lý";
            $placeholder = "Nhập mã hoặc tên đại lý";
        }
        ?>
        <?php echo $form->labelEx($model,'user_create', ['label' => $lbl]); ?>
        <?php echo $form->hiddenField($model,'user_create', array('class'=>'')); ?>
        <?php
            // widget auto complete search user customer and supplier
            $aData = array(
                'model'=>$model,
                'field_customer_id'=>'user_create',
                'url'=> $model->getUrlAutocomplete(),
                'name_relation_user'=>'rUser',
                'ClassAdd' => 'w-300',
                'autocomplete_name' => 'autocomplete_name',
                'ShowTableInfo' => 0,
                'placeholder' => $placeholder,
                'fnSelectCustomerV2' => 'fnCallSomeFunctionAfterSelectV2',
                'doSomethingOnClose' => "doSomethingOnClose",
            );
            $this->widget('ext.SpaAutocompleteCustomer.SpaAutocompleteCustomer',
                array('data'=>$aData));                                        
        ?>
    </div>
    
    <table class="materials_table hm_table w-500 tbl-user">
        <thead>
            <tr>
                <th class="item_c w-20">#</th>
                <th class="item_code item_c w-300">Nhân Viên</th>
                <th class="item_unit last item_c">Xóa</th>
            </tr>
        </thead>
        <tbody>
        <?php 
            $aData = $model->getJson(); $no = 1;
        ?>
            <?php foreach ($aData as $id => $name): ?>
            <tr>
                <input name="SetupTeam[user_create][<?php echo $id; ?>]" value="<?php echo $name; ?>" type="hidden">
                <td class="item_c order_no"><?php echo $no++; ?></td>
                <td class="uid_114943"><?php echo $name ?></td>
                <td class="item_c last"><span class="remove_icon_only"></span></td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    
    <div class="row buttons" style="padding-left: 141px;">
        <?php $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType'=>'submit',
            'label'=>'Save',
            'type'=>'null', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
            'size'=>'small', // null, 'large', 'small' or 'mini'
            'htmlOptions' => array(
                    'name' => 'save',
                    'value' => 1
                ),
        )); ?>	
    </div>
    <?php endif; ?>

<?php $this->endWidget(); ?>
    
    

</div><!-- form -->
<script>
    $(document).ready(function(){
        $('.form').find('button:submit').click(function(){
            $.blockUI({ overlayCSS: { backgroundColor: '#fff' } }); 
        });
        fnBindRemoveIcon();
    });
    
    function fnCallSomeFunctionAfterSelectV2(ui, idField, idFieldCustomerID){
        var no = ($('.tbl-user tbody tr')).length+1;
        var tr = '<tr>'
                + '<input name="SetupTeam[user_create]['+ui.item.id+']" value="'+ui.item.label+'" type="hidden">'
                + '<td class="item_c order_no">'+no+'</td>'
                + '<td class="uid_114943">'+ui.item.label+'</td>'
                + '<td class="item_c last"><span class="remove_icon_only"></span></td>'
                + '</tr>';
        $('.tbl-user').append(tr);
        return false;
    }
    
    function doSomethingOnClose(ui, idField, idFieldCustomer){
        var row = $(idField).closest('.row');
        row.find('.remove_row_item').trigger('click');
    }
</script>