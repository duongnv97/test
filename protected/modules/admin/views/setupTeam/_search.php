<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=> GasCheck::getCurl(),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'type'); ?>
		<?php echo $form->dropdownList($model,'type',$model->getArrayTypeSearch(), ['empty'=>'Select' ,'class'=>'w-300']); ?>
	</div>

	<div class="row">
            <?php echo $form->labelEx($model,'user_id', array('label'=>'Nhân Viên')); ?>
            <?php echo $form->hiddenField($model,'user_id', array('class'=>'')); ?>
            <?php
                // 1. limit search kh của sale
                $url = Yii::app()->createAbsoluteUrl('admin/ajax/search_user_login');
                // widget auto complete search user customer and supplier
                $aData = array(
                    'model'=>$model,
                    'field_customer_id'=>'user_id',
                    'url'=> $url,
                    'name_relation_user'=>'rUser',
                    'ClassAdd' => 'w-300',
                    'field_autocomplete_name' => 'autocomplete_name_2'
    //                'ShowTableInfo' => 0,
                );
                $this->widget('ext.SpaAutocompleteCustomer.SpaAutocompleteCustomer',
                    array('data'=>$aData));                                        
            ?>
        </div>
    

        <div class="row">
            <?php echo $form->labelEx($model,'date_apply'); ?>
             <?php 
                $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                    'model'=>$model,        
                    'attribute'=>'date_apply',
                    'options'=>array(
                        'showAnim'=>'fold',
                        'dateFormat'=> "dd-mm-yy",
                //                            'minDate'=> '0',
                //                            'maxDate'=> '0',
                        'changeMonth' => true,
                        'changeYear' => true,
                        'showOn' => 'button',
                        'buttonImage'=> Yii::app()->theme->baseUrl.'/admin/images/icon_calendar_r.gif',
                        'buttonImageOnly'=> true,                                
                    ),        
                    'htmlOptions'=>array(
                        'class'=>'w-16',
                        'style'=>'height:20px;',
                    ),
                ));
                ?>  
        </div>
            
    

	<div class="row buttons" style="padding-left: 159px;">
            <?php $this->widget('bootstrap.widgets.TbButton', array(
                'buttonType'=>'submit',
                'label'=>'Search',
                'type'=>'null', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
                'size'=>'small', // null, 'large', 'small' or 'mini'
                //'htmlOptions' => array('style' => 'margin-bottom: 10px; float: right;'),
            )); ?>	
        </div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->