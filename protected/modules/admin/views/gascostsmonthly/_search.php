<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'costs_id',array()); ?>
		<?php echo $form->dropDownList($model,'costs_id', GasCosts::getAllItem(),array('class'=>'category_ajax', 'style'=>'width:380px;','empty'=>'Select')); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'agent_id',array()); ?>
		<?php echo $form->dropDownList($model,'agent_id', Users::getArrUserByRole(ROLE_AGENT),array('style'=>'width:350px;','empty'=>'Select')); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'costs_month',array()); ?>
		<?php echo $form->dropDownList($model,'costs_month', ActiveRecord::getMonthVn(),array('style'=>'width:350px;','empty'=>'Select')); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'costs_year',array()); ?>
		<?php echo $form->textField($model,'costs_year',array('size'=>4,'maxlength'=>4)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'note',array()); ?>
		<?php echo $form->textField($model,'note',array('size'=>60,'maxlength'=>300)); ?>
	</div>

	<div class="row buttons" style="padding-left: 159px;">
		        <?php $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType'=>'submit',
            'label'=>Yii::t('translation','Search'),
            'type'=>'null', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
            'size'=>'small', // null, 'large', 'small' or 'mini'
            //'htmlOptions' => array('style' => 'margin-bottom: 10px; float: right;'),
        )); ?>	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->