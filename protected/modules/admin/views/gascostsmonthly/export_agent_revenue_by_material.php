<h1><?php echo Yii::t('translation', 'Xuất Báo Cáo Doanh Thu Đại Lý Theo Sản Phẩm'); ?></h1>

<?php //echo CHtml::link(Yii::t('translation','Advanced Search'),'#',array('class'=>'search-button')); ?>
<div class="search-form" style="">
<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'post',
)); ?>
    <?php echo $form->hiddenField($model,'note'); ?>
        <div class="row group_subscriber">
		<?php echo Yii::t('translation', $form->label($model,'agent_id')); ?>
		<?php // echo $form->dropDownList($model,'agent_id', Users::getArrUserByRole(ROLE_AGENT),array('style'=>'width:350px;','empty'=>'Select')); ?>
            
                <div class="fix-label">
                    <?php
                       $this->widget('ext.multiselect.JMultiSelect',array(
                             'model'=>$model,
                             'attribute'=>'agent_id',
                             'data'=>Users::getArrUserByRole(ROLE_AGENT),
                             // additional javascript options for the MultiSelect plugin
                             'options'=>array(),
                             // additional style
                             'htmlOptions'=>array('style' => 'width: 350px;'),
                       ));    
                   ?>
                </div>            
	</div>

	<div class="row group_subscriber">
		<?php echo Yii::t('translation', $form->label($model,'costs_year')); ?>
                <?php // echo $form->dropDownList($model,'costs_month', ActiveRecord::getMonthVn(),array('style'=>'width:350px;','empty'=>'Select')); ?>	
                <div class="fix-label">
                    <?php
                       $this->widget('ext.multiselect.JMultiSelect',array(
                             'model'=>$model,
                             'attribute'=>'costs_year',
                             'data'=>ActiveRecord::getRangeYear(),
                             // additional javascript options for the MultiSelect plugin
                             'options'=>array(),
                             // additional style
                             'htmlOptions'=>array('style' => 'width: 350px;'),
                       ));    
                   ?>
                </div>               
	</div>

	<div class="row buttons" style="padding-left: 159px;">
        <?php $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType'=>'submit',
            'label'=>Yii::t('translation','Xuất Báo Cáo Doanh Thu Đại Lý Theo Sản Phẩm Ra Excel '),
            'type'=>'null', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
            'size'=>'small', // null, 'large', 'small' or 'mini'
            //'htmlOptions' => array('style' => 'margin-bottom: 10px; float: right;'),
        )); ?>	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->
</div><!-- search-form -->
