<?php
$this->breadcrumbs=array(
	Yii::t('translation','Chi Phí Đại Lý')=>array('index'),
	$model->agent?$model->agent->first_name:''=>array('view','id'=>$model->id),
	Yii::t('translation','Update'),
);

$menus = array(	
        array('label'=> Yii::t('translation', 'Chi Phí Đại Lý'), 'url'=>array('index')),
	array('label'=> Yii::t('translation', 'View Chi Phí Đại Lý'), 'url'=>array('view', 'id'=>$model->id)),
	array('label'=> Yii::t('translation', 'Create Chi Phí Đại Lý'), 'url'=>array('create')),	
);
$this->menu= ControllerActionsName::createMenusRoles($menus, $actions);

?>

<h1>Update Chi Phí Đại Lý: <?php echo $model->agent?$model->agent->first_name:''; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>