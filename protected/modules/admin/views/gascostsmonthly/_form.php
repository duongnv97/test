<div class="form">

<?php  $form=$this->beginWidget('CActiveForm', array(
	'id'=>'gas-costs-monthly-form',
	'enableClientValidation'=>true,
//    'enableAjaxValidation'=>true,
		//'htmlOptions'=>array('onsubmit'=>'return fnCheckSubmit();'),
        'clientOptions' => array(
            'validationDelay'=>100000, // trên ie8 (firefox+chrom ko sao) nó tự động trigger validate ajax, nên khi mới load trang nó hiện error luôn để validationDelay lớn thì tránh được
            'validateOnSubmit' => true,
			'afterValidate'=>'js:function(form, data, hasError) {return fnAddCheckSubmit();}'
        ),
    
)); ?>

	<?php echo Yii::t('translation', '<p class="note">Fields with <span class="required">*</span> are required.</p>'); ?>
	
	<div class="row">
		<?php echo Yii::t('translation', $form->labelEx($model,'costs_id')); ?>
		<?php echo $form->dropDownList($model,'costs_id', GasCosts::getAllItem(),array('onchange'=>'fnAjaxUpdate()', 'style'=>'width:380px;','empty'=>'Select')); ?>
		<?php echo $form->error($model,'costs_id'); ?>
	</div>

	<div class="row">
		<?php echo Yii::t('translation', $form->labelEx($model,'costs_month')); ?>
		<?php echo $form->dropDownList($model,'costs_month', ActiveRecord::getMonthVn(),array('onchange'=>'fnAjaxUpdate()','style'=>'width:350px;')); ?>
		<?php echo $form->error($model,'costs_month'); ?>
	</div>

	<div class="row">
		<?php echo Yii::t('translation', $form->labelEx($model,'costs_year')); ?>
		<?php echo $form->textField($model,'costs_year',array('onchange'=>'fnAjaxUpdate()','class'=>'','size'=>4,'maxlength'=>4)); ?>
		<?php echo $form->error($model,'costs_year'); ?>
	</div>
    
        <div class="clr"></div>
        <?php if($model->isNewRecord): ?>
            <div class="row buttons" style="padding-left: 115px;">
                            <?php $this->widget('bootstrap.widgets.TbButton', array(
                'buttonType'=>'submit',
                'label'=>$model->isNewRecord ? Yii::t('translation', 'Create') : Yii::t('translation', 'Save'),
                'type'=>'null', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
                'size'=>'small', // null, 'large', 'small' or 'mini'
                //'htmlOptions' => array('style' => 'margin-bottom: 10px; float: right;'),
            )); ?>	</div>        
        
            <div class="row agent_row">
                <div class="col1"><?php echo $form->labelEx($model,'agent_id'); ?></div>
                <div class="col2"><?php echo $form->labelEx($model,'total'); ?></div>
                <div class="col3"><?php echo $form->labelEx($model,'note'); ?></div>
            </div>
            <div class="row agent_row">
                <div class="col1">Tổng Cộng</div>
                <div class="col2"><span class="sum_total"></span></div>
                <div class="col3"></div>
            </div>                
            <?php $agents = Users::getArrObjectUserByRole(ROLE_AGENT); ?>
            <?php if(count($agents)) foreach($agents as $key=>$item): ?>
            <div class="row agent_row">
                <div class="col1"><?php echo $item->code_account.' - '.$item->first_name;?>
                    <?php echo $form->hiddenField($model,'agent_id[]',array('style'=>'','value'=>$item->id)); ?>
                </div>
                <div class="col2" style="">                    
                    <?php echo $form->textField($model,'total[]',array('class'=>'number_only number_sum ajax_val_'.$item->id,'size'=>16,'maxlength'=>14)); ?>
                    <div class="help_number"></div>
                </div>
                <div class="col3">
                    <?php echo $form->textArea($model,'note[]',array('style'=>'width:450px','rows'=>1)); ?>
                </div>
            </div>
            <?php endforeach;?>
            <div class="row agent_row">
                <div class="col1">Tổng Cộng</div>
                <div class="col2"><span class="sum_total"></span></div>
                <div class="col3"></div>
            </div>        
            <div class="clr"></div>
        <?php else:?>
            <div class="row">
                    <?php echo Yii::t('translation', $form->labelEx($model,'agent_id')); ?>
                    <?php echo $form->dropDownList($model,'agent_id', Users::getArrUserByRole(ROLE_AGENT),array('style'=>'width:350px;','empty'=>'Select')); ?>
                    <?php echo $form->error($model,'agent_id'); ?>
            </div>
            <div class="row clr">
                    <?php echo Yii::t('translation', $form->labelEx($model,'total')); ?>
                    <div class="fix_number_help">
                    <?php echo $form->textField($model,'total',array('class'=>'number_only','maxlength'=>14)); ?>
                    <div class="help_number"></div>
                    </div>
                    <?php echo $form->error($model,'total'); ?>
            </div>
            <div class="clr"></div>
            <div class="row">
                    <?php echo Yii::t('translation', $form->labelEx($model,'note')); ?>
                    <?php echo $form->textArea($model,'note',array('style'=>'width:450px')); ?>
                    <?php echo $form->error($model,'note'); ?>
            </div>
            
        <?php endif;?>
        
	<div class="row buttons" style="padding-left: 115px;">
	
	<?php //echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	<input type="submit" onclick='return fnCheckSubmit(this);' value="<?php echo $model->isNewRecord ? 'Create' : 'Save'; ?>" name="yt1">
		</div>

<?php $this->endWidget(); ?>

</div><!-- form -->

<style>
    .agent_row {width: 100%; float: left;margin:0 !important; padding: 10px; border: 1px solid #333;}
    .agent_row .col1, .agent_row .col2, .agent_row .col3  {float: left;padding-right: 15px;}
    .agent_row .col1 {min-width: 20%;}
    .sum_total {font-weight: bold;font-size: 20px;}
</style>

<script>
    $(document).ready(function(){
        $('.number_sum').keyup(function(){
            fnSumNumber();
        });
    });    
	
    function fnSumNumber(){
            var total = 0;
            $('.number_sum').each(function(){
                if($.isNumeric($(this).val()) && $.trim($(this).val())!='')
                    total += $(this).val()*1;
            });
            $('.sum_total').text(commaSeparateNumber(total));
    }
    
    function fnAjaxUpdate(){
        var materials_id=$('#GasCostsMonthly_costs_id').val();
        var sell_month=$('#GasCostsMonthly_costs_month').val();
        var sell_year=$('#GasCostsMonthly_costs_year').val();

        if(materials_id=='<?php echo ID_CHI_PHI_KHUYEN_MAI;?>'){
            $('.number_sum').val('');
            var url_ = '<?php echo Yii::app()->createAbsoluteUrl('admin/gascostsmonthly/create');?>';
            $.blockUI({ overlayCSS: { backgroundColor: '<?php echo BLOCK_UI_COLOR;?>' } }); 
            $.ajax({
                url:url_,
                type:'get',
                dataType:'json',
                data:{ajax_costs:1,materials_id:materials_id,sell_month:sell_month,sell_year:sell_year},
                success:function(data){
                    var arr_agent_id = $.map(data['agent_res'], function (value, key) { return key });
                    var arr_value_input = $.map(data['agent_res'], function (value, key) { return value; });
                    // arr_res array(agent_id=>sum(total_sell))
                     for(var i=0; i<arr_agent_id.length;i++){
                         $('.ajax_val_'+arr_agent_id[i]).val(arr_value_input[i]);
                     }
                     
                     $('.number_sum').trigger('keyup');
                     fnSumNumber();
                     $.unblockUI();
                }
            });
            
        }
        
    }
    
</script>