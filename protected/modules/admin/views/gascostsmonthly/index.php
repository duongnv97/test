<?php
$this->breadcrumbs=array(
	'Chi Phí Bán Hàng',
);

$menus=array(
	array('label'=> Yii::t('translation','Create Chi Phí Bán Hàng'), 'url'=>array('create')),
);
$this->menu= ControllerActionsName::createMenusRoles($menus, $actions);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('gas-costs-monthly-grid', {
                url : $(this).attr('action'),
		data: $(this).serialize()
	});
	return false;
});
");

Yii::app()->clientScript->registerScript('ajaxupdate', "
$('#gas-costs-monthly-grid a.ajaxupdate').live('click', function() {
    $.fn.yiiGridView.update('gas-costs-monthly-grid', {
        type: 'POST',
        url: $(this).attr('href'),
        success: function() {
            $.fn.yiiGridView.update('gas-costs-monthly-grid');
        }
    });
    return false;
});
");
?>

<h1>Chi Phí Bán Hàng</h1>

<?php echo CHtml::link(Yii::t('translation','Advanced Search'),'#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'gas-costs-monthly-grid',
	'dataProvider'=>$model->search(),
        'template'=>'{pager}{summary}{items}{pager}{summary}',     
	'enableSorting' => false,
	//'filter'=>$model,
	'columns'=>array(
        array(
            'header' => 'S/N',
            'type' => 'raw',
            'value' => '$this->grid->dataProvider->pagination->currentPage * $this->grid->dataProvider->pagination->pageSize + ($row+1)',
            'headerHtmlOptions' => array('width' => '30px','style' => 'text-align:center;'),
            'htmlOptions' => array('style' => 'text-align:center;')
        ),
            
        array(
            'header' => 'Mã KH',
            'value' => '$data->agent?$data->agent->code_account:""',
        ), 		
        array(
            'name' => 'agent',
            'value' => '$data->agent?$data->agent->first_name:""',
            'htmlOptions' => array('style' => 'text-align:left;')
        ),   	
        array(
            'name' => 'costs_id',
            'value' => '$data->costs?$data->costs->name:""',
            'htmlOptions' => array('style' => 'text-align:left;')
        ),   	
            
            array(
                'name' => 'total',
                'type' => 'Currency',
                'htmlOptions' => array('style' => 'text-align:right;')
            ),            
            array(
                'name' => 'costs_month',
                'htmlOptions' => array('style' => 'text-align:center;')
            ),  
            array(
                'name' => 'costs_year',
                'htmlOptions' => array('style' => 'text-align:center;')
            ),  
		 Yii::t('translation','note'),
		array(
			'header' => 'Action',
			'class'=>'CButtonColumn',
                        'template'=> ControllerActionsName::createIndexButtonRoles($actions),
		),
	),
)); ?>
