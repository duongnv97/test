<?php
$this->breadcrumbs=array(
	'Chi Phí Đại Lý'=>array('index'),
	$model->agent?$model->agent->first_name:'',
);

$menus = array(
	array('label'=>'Chi Phí Đại Lý', 'url'=>array('index')),
	array('label'=>'Create Chi Phí Đại Lý', 'url'=>array('create')),
	array('label'=>'Update Chi Phí Đại Lý', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete Chi Phí Đại Lý', 'url'=>array('delete'), 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
);
$this->menu= ControllerActionsName::createMenusRoles($menus, $actions);
?>

<h1>View Chi Phí Đại Lý: <?php echo $model->agent?$model->agent->first_name:''; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
            array(
                'name'=>'agent_id',
                'value'=>$model->agent?$model->agent->first_name:'',
            ),   
            array(
                'name'=>'costs_id',
                'value'=>$model->costs?$model->costs->name:'',
            ),   
		'total:Currency',
		'costs_month',
		'costs_year',
		'note',
	),
)); ?>
