<div class="form editor_area_size form_ajax_submit">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'gas-meeting-minutes-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Dòng có dấu <span class="required">*</span> là bắt buộc.</p>
        
        <?php echo MyFormat::BindNotifyMsg(); ?> 	

        <div class="row">
		<?php echo Yii::t('translation', $form->labelEx($model,'date_published')); ?>
                <?php 
                    $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                        'model'=>$model,        
                        'attribute'=>'date_published',
                        'options'=>array(
                            'showAnim'=>'fold',
                            'dateFormat'=> ActiveRecord::getDateFormatJquery(),
//                            'minDate'=> '0',
                            'maxDate'=> '0',
                            'changeMonth' => true,
                            'changeYear' => true,
                            'showOn' => 'button',
                            'buttonImage'=> Yii::app()->theme->baseUrl.'/admin/images/icon_calendar_r.gif',
                            'buttonImageOnly'=> true,                                
                        ),        
                        'htmlOptions'=>array(
                            'class'=>'w-16',
                            'style'=>'height:20px;',
                                'readonly'=>'readonly',
                        ),
                    ));
                ?>     		
		<?php echo $form->error($model,'date_published'); ?>
	</div>
            
        <div class="row editor_area">
            <?php echo Yii::t('translation', $form->labelEx($model,'content')); ?>
                <div style="padding-left:141px;">
                        <?php
                        $this->widget('ext.niceditor.nicEditorWidget', array(
                                "model" => $model, // Data-Model
                                "attribute" => 'content', // Attribute in the Data-Model        
                                "config" => array(
//                                "maxHeight" => "200px",   
                                "buttonList"=>Yii::app()->params['niceditor_v_1'],
                                ),
                                "width" => GasMeetingMinutes::EDITOR_WIDTH, // Optional default to 100%
                                "height" => GasMeetingMinutes::EDITOR_HEIGHT, // Optional default to 150px
                        ));
                        ?>                                
                </div>			
            <?php echo $form->error($model,'content'); ?>
	</div>
		
	<div class="row buttons" style="padding-left: 141px;">
            <?php $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType'=>'submit',
            'label'=>$model->isNewRecord ? 'Create' :'Save',
            'type'=>'null', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
            'size'=>'small', // null, 'large', 'small' or 'mini'
            //'htmlOptions' => array('style' => 'margin-bottom: 10px; float: right;'),
        )); ?>	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->
<script>
    $(document).ready(function(){
        $('.form').find('button:submit').click(function(){
            $.blockUI({ overlayCSS: { backgroundColor: '#fff' } });
        });
        setInterval( "fnAutoSave()", 60000);// 60 second is 1 min
//        setInterval( "fnAutoSave()", 10000);// 10 second is 
    });
    
    function fnAutoSave(){
        <?php if(!$model->isNewRecord): ?>
            fnUpdateAllNiceEditor();
            var url_ = '<?php echo Yii::app()->createAbsoluteUrl('admin/gasMeetingLaw/update', array('id'=> $model->id,'ajax_auto_save'=>1)) ;?>';
            $.ajax({
                url: url_,
                type: "post",
                data: $('#gas-meeting-minutes-form').serialize(),
                success:function(data){}
            });        
        <?php endif;?>        
    }
    
    function fnUpdateAllNiceEditor(){
        for (var i = 0; i < nicEditors.editors.length; i++) {
            nicEditors.editors[i].nicInstances[0].saveContent();
        }        
    }
    
</script>
<style>    
</style>