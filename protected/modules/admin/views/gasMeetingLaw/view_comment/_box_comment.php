<div class="digital_ticket">
    <div class="tickets">
        <div class="ticket ">
            <?php // if(!$mTextView->stop_comment):?>
            <!-- begin for reply comment -->
            <div class="in collapse">
                <div class="post_reply post_reply2">
                    <h4>Post Comment</h4>
                    <div class="styled-form">
                        <form method="post" id="new_reply" class="simple_form new_reply" action="<?php echo Yii::app()->createAbsoluteUrl('admin/gasMeetingMinutes/text_post_comment', array('id'=>$model->id)) ?>" accept-charset="UTF-8">
                            <div class="control-group text required reply_content">
                                <div class="controls">
                                    <textarea rows="20" placeholder="Nhập <?php echo GasMeetingMinutes::TEXT_HELP;?>" name="GasMeetingMinutes[message]" id="reply_content" cols="40" class=""></textarea>
                                    <div class="errorMessage l_padding_20 display_none" style="">Nhập <?php echo GasMeetingMinutes::TEXT_HELP;?>.</div>
                                </div>
                            </div>
                            <input type="submit" value="Submit Comment" name="commit<?php echo $model->id;?>">
                        </form>
                    </div>
                </div>
            </div>
            <!-- end for reply comment -->
            <?php // endif; // end if(!$mTextView->stop_comment?>
            
            <!--<h2 class="section-header closed_tickets">Danh Sách Bình Luận</h2>-->
            <h2 class="section-header closed_tickets">All Comments</h2>
            <?php  // NGUYEN DUNG
                $this->widget('zii.widgets.CListView', array(
                    'dataProvider'=> $mComment->search(),
                    'itemView' => 'view_comment/_box_comment_item',
            //        'ajaxUpdate'=>false, 
            //        'afterAjaxUpdate'=>'function(id, data){ BindClickView(); }',
                    'itemsCssClass'=>'',
                    'pagerCssClass'=>'pager',
                    'pager'=> array(
                        'maxButtonCount' => 10,
                        'class'=>'CLinkPager',
                        'header'=> false,
                        'footer'=> false,
                    ),
                    'summaryText' => true,
                    'summaryText'=>'Showing <strong>{start} - {end}</strong> of {count} results',
                    'template'=>'{pager}{summary}<ul class="replies clearfix">{items}</ul>{pager}{summary}',
                ));
            ?>

        </div><!-- end  <div class="ticket "> -->
    </div><!-- end  <div class="tickets "> -->
</div><!-- end  <div class="digital_ticket "> -->