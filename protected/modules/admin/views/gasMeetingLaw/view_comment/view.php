<?php
$this->breadcrumbs=array(
	'Biên Bản Họp - Phòng Pháp Lý'=>array('index'),
	$model->date_published,
);

$menus = array(
	array('label'=>'Biên Bản Họp', 'url'=>array('index')),
	array('label'=>'Tạo Mới Biên Bản Họp', 'url'=>array('create')),
	array('label'=>'Cập Nhật Biên Bản Họp', 'url'=>array('update', 'id'=>$model->id)),
);
$this->menu= ControllerActionsName::createMenusRoles($menus, $actions);
?>

<h1>Xem Biên Bản Họp: <?php echo $model->date_published; ?>
    <a href="javascript:void(0);" class="click_show_hide_text"> Click để ẩn/hiện văn bản</a>
</h1>
<?php echo MyFormat::BindNotifyMsg(); ?>

<div class="hm_text_view">
    <?php include '_form_read.php';?>
    <?php // include '_box_comment.php';?>
</div>
<style>
    /*.box_text_view li { list-style: none;}*/
</style>

<script>
    $(document).ready(function(){
        jQuery('a.gallery').colorbox({ opacity:0.5 , rel:'group1' });
        fnUpdateColorbox();
        BindClickReply();
        BindClickShowHide();
    });
    
    function fnUpdateColorbox(){   
        fixTargetBlank();
        BindClickClose();
    }
    
    function BindClickReply(){
        $('.new_reply').find('input:submit').live('click',function(){
            var form = $(this).closest('form');
            var message = form.find('textarea').val();
            if($.trim(message)==''){
                form.find('.errorMessage').show();
                return false;
            }
            $.blockUI({ overlayCSS: { backgroundColor: '<?php echo BLOCK_UI_COLOR;?>' } }); 
            return true;
        }); 
    }
    
    function BindClickShowHide(){
         $('.click_show_hide_text').live('click',function(){
             $('.box_text_view').slideToggle();
        }); 
    }
    
    function BindClickClose(){
        $('.btn_closed_tickets').live('click',function(){
            var alert_text = $(this).attr('alert_text');
            if(confirm(alert_text)){
                $.blockUI({ overlayCSS: { backgroundColor: '<?php echo BLOCK_UI_COLOR;?>' } }); 
                return true;
            }
            return false;
        }); 
    }
</script>
