<?php 
    $OUTPUT         = isset($rData['OUTPUT']) ? $rData['OUTPUT'] : [];
    $OUTPUT_SUM     = isset($rData['OUTPUT_SUM']) ? $rData['OUTPUT_SUM'] : [];
    $SUM_ISSUE      = isset($rData['SUM_ISSUE']) ? $rData['SUM_ISSUE'] : [];
    $ARR_UID        = isset($rData['ARR_UID']) ? $rData['ARR_UID'] : [];
    $ARR_USER       = isset($rData['ARR_USER']) ? $rData['ARR_USER'] : [];
    $mAppCache      = new AppCache();
    $aAgent         = $mAppCache->getAgentListdata();
    
    $index = 1;
    $aEmployee          = $mAppCache->getListdataUserByRole(ROLE_EMPLOYEE_MAINTAIN);
    $aEmployee1         = $mAppCache->getListdataUserByRole(ROLE_EMPLOYEE_MARKET_DEVELOPMENT);
    $aEmployee2         = $mAppCache->getListdataUserByRole(ROLE_DRIVER);
    $aEmployee = $aEmployee + $aEmployee1 + $aEmployee2;
?>

<div class="grid-view display_none123">
    <table id="freezetablecolumns_inventory" class="hm_table items freezetablecolumns" style="">
        <thead>
            <tr class="h_50">
                <th class="w-10">#</th>
                <th class="w-80">Nhân viên</th>
                <th class="w-80">Đại lý</th>
                <th class="w-30">Tổng</th>
                <?php foreach ($model->getArrayIssue() as $issueId => $issueText): ?>
                    <th class="w-40"><?php echo $issueText;?></th>
                <?php endforeach; ?>
            </tr>
        </thead>
        
        <tbody>
            <tr>
                <td class="item_c item_b" colspan="3 ">Sum All</td>
                <td class="item_c item_b"><?php echo array_sum($OUTPUT_SUM); ?></td>
                <?php foreach ($model->getArrayIssue() as $issueId => $issueText): ?>
                    <?php 
                        $sumCol = isset($SUM_ISSUE[$issueId]) ? $SUM_ISSUE[$issueId] : '';
                    ?>
                    <td class="item_c item_b"><?php echo $sumCol;?></td>
                <?php endforeach; ?>
            </tr>
            <?php foreach ($OUTPUT_SUM as $uid_login => $sumRow): ?>
                <?php 
                    $nameEmployee = isset($aEmployee[$uid_login]) ? $aEmployee[$uid_login] : '';
                    $nameAgent = '';
                    if(isset($ARR_USER[$uid_login])){
                        $nameAgent = isset($aAgent[$ARR_USER[$uid_login]->parent_id]) ? $aAgent[$ARR_USER[$uid_login]->parent_id] : '';
                    }
                    if(empty($nameEmployee)){
                        $mUser          = Users::model()->findByPk($uid_login);
                        $nameEmployee   = $mUser->first_name;
                        $nameAgent      = isset($aAgent[$mUser->parent_id]) ? $aAgent[$mUser->parent_id] : '';
                    }
                ?>
                <tr>
                    <td class="item_c"><?php echo $index++;?></td>
                    <td class=""><?php echo $nameEmployee;?></td>
                    <td class=""><?php echo $nameAgent;?></td>
                    <td class="item_c item_b"><?php echo $sumRow != 0 ? $sumRow : '';?></td>
                    <?php foreach ($model->getArrayIssue() as $issueId => $issueText): ?>
                        <?php $v = isset($OUTPUT[$uid_login][$issueId]) ? $OUTPUT[$uid_login][$issueId] : ''; ?>
                    <td class="item_c"><?php echo $v;?></td>
                    <?php endforeach; ?>
                </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
</div>

<?php Yii::app()->clientScript->registerCoreScript('jquery.ui'); ?>
<?php
$gridCss = Yii::app()->getAssetManager()->publish(Yii::getPathOfAlias('zii.widgets.assets')).'/gridview/styles.css';
Yii::app()->getClientScript()->registerCssFile($gridCss);
?>
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/scroll/jquery.freezetablecolumns.1.1.js"></script> 

<script>
$(function() {
//    $( "#tabs" ).tabs();
    $(".items > tbody > tr:odd").addClass("odd");
    $(".items > tbody > tr:even").addClass("even");   
//    fnTabMonthClick();
});

function fnTabMonthClick(){
     $('.freezetablecolumns').each(function(){
        var id_table = $(this).attr('id');
        $('#'+id_table).freezeTableColumns({
            width:       <?php echo Users::WIDTH_freezetable;?>,   // required - bt 1100
            height:      400,   // required
            numFrozen:   4,     // optional
            frozenWidth: 345,   // optional
            clearWidths: true  // optional
        });   
    });
}

$(window).load(function(){
    var index = 1;
    $('.freezetablecolumns').each(function(){
        if(index==1)
            $(this).closest('div.grid-view').show();
        index++;
    });    
    fnAddClassOddEven('items');
});

</script