<?php
$this->breadcrumbs=array(
    'Danh sách' => array('tickets/list'),
);

$menus = array(
	array('label'=>" Management", 'url'=>array('index'))
);
$this->menu= ControllerActionsName::createMenusRoles($menus, $actions);
?>

<h1>Xem: <?php echo ""; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
            array(
                'name'=>'code_no',
                'type'=>'raw',
                'value'=> $model->code_no,
            ),
            array(
                'name'=>'Người gửi',
                'type'=>'raw',
                'value'=> $model->getUidLoginName(),
            ),
            array(
                    'name'=>'Người trả lời',
                    'type'=>'raw',
                    'value'=> $model->getSendToNameForList(),
                ),
            array(
                'name' => 'Nội dung',
                'type' => 'html',
                'value'=> $model->getListMessage(),            
                'htmlOptions' => array('style' => 'width:50px;')
            ),
            
	),
)); ?>