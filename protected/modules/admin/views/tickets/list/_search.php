<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>GasCheck::getCurl(),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'code_no',array()); ?>
		<?php echo $form->textField($model,'code_no',array('size'=>30,'maxlength'=>30)); ?>
	</div>


    <div class="row">
        <?php echo $form->labelEx($model,'Người tạo'); ?>
        <?php echo $form->hiddenField($model,'uid_login', array('class'=>'')); ?>
        <?php
                $url = Yii::app()->createAbsoluteUrl('admin/ajax/search_for_user_login');
                // widget auto complete search user customer and supplier
                $aData = array(
                    'model'=>$model,
                    'field_customer_id'=>'uid_login',
                    'url'=> $url,
                    'name_relation_user'=>'rUidPost',
                    'ClassAdd' => 'w-400',
                    'field_autocomplete_name' => 'autocomplete_name',
                    'placeholder'=>'Nhập mã NV hoặc tên',
                );
                $this->widget('ext.SpaAutocompleteCustomer.SpaAutocompleteCustomer',
                    array('data'=>$aData));
                ?>
            <?php echo $form->error($model,'uid_login'); ?>
    </div>


    <div class="row">
        <?php echo $form->labelEx($model,'Người xử lý'); ?>
        <?php  echo $form->dropDownList($model,'send_to_id', GasTickets::GetListSendTo(),array('class'=>'send_to_id','empty'=>'Chọn người xử lý')); ?>
        <?php echo $form->error($model,'send_to_id', array('style'=>'padding-top:70px;')); ?>
    </div>
    <div class="row more_col">
        <div class="col1">
           <?php echo $form->labelEx($model,'date_from'); ?>
               <?php 
                   $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                       'model'=>$model,        
                       'attribute'=>'date_from',
                       'options'=>array(
                           'showAnim'=>'fold',
                           'dateFormat'=> MyFormat::$dateFormatSearch,
                           'minDate'=> '',
                           'maxDate'=> '',
                           'changeMonth' => true,
                           'changeYear' => true,
                           'showOn' => 'button',
                           'buttonImage'=> Yii::app()->theme->baseUrl.'/admin/images/icon_calendar_r.gif',
                           'buttonImageOnly'=> true,                                
                       ),        
                       'htmlOptions'=>array(
                           'class'=>'w-16',
                           'style'=>'height:20px;width:166px;',
                               'readonly'=>'0',
                       ),
                   ));
               ?>     	
           <?php echo $form->error($model,'date_from'); ?>            
       </div>

       <div class="col2">
           <?php echo $form->labelEx($model,'date_to'); ?>
               <?php 
                   $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                       'model'=>$model,        
                       'attribute'=>'date_to',
                       'options'=>array(
                           'showAnim'=>'fold',
                           'dateFormat'=> MyFormat::$dateFormatSearch,
                           'minDate'=> '',
                           'maxDate'=> '',
                           'changeMonth' => true,
                           'changeYear' => true,
                           'showOn' => 'button',
                           'buttonImage'=> Yii::app()->theme->baseUrl.'/admin/images/icon_calendar_r.gif',
                           'buttonImageOnly'=> true,                                
                       ),        
                       'htmlOptions'=>array(
                           'class'=>'w-16',
                           'style'=>'height:20px;width:166px;',
                               'readonly'=>'0',
                       ),
                   ));
               ?>     	
           <?php echo $form->error($model,'date_to'); ?>            
       </div>
    </div>
	<div class="row buttons" style="padding-left: 159px;">
		        <?php $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType'=>'submit',
            'label'=>'Search',
            'type'=>'null', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
            'size'=>'small', // null, 'large', 'small' or 'mini'
            //'htmlOptions' => array('style' => 'margin-bottom: 10px; float: right;'),
        )); ?>	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->