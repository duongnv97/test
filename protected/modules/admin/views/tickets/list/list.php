<?php
$this->breadcrumbs=array(
	'Danh Sách Tickets',
);



Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('gas-text-grid', {
                url : $(this).attr('action'),
		data: $(this).serialize()
	});
	return false;
});
");

Yii::app()->clientScript->registerScript('ajaxupdate', "
$('#gas-text-grid a.ajaxupdate').live('click', function() {
    $.fn.yiiGridView.update('gas-text-grid', {
        type: 'POST',
        url: $(this).attr('href'),
        success: function() {
            $.fn.yiiGridView.update('gas-text-grid');
        }
    });
    return false;
});
");
?>

<?php echo MyFormat::BindNotifyMsg(); ?>

<h1>Danh Sách Chi tiết Tickets</h1>

<?php if(!isset($_GET['Sell'])): ?>
    <?php echo CHtml::link('Tìm Kiếm Nâng Cao','#',array('class'=>'search-button')); ?>
    <div class="search-form" style="display : none;">
    <?php  $this->renderPartial('list/_search',array(
            'model'=>$model,
     )); 
    ?>
    </div><!-- search-form -->
<?php endif; ?>

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'gas-text-grid',
	'dataProvider'=>$model->search(),
        'afterAjaxUpdate'=>'function(id, data){ fnUpdateColorbox();}',
        'template'=>'{pager}{summary}{items}{pager}{summary}',
//    'itemsCssClass' => 'items custom_here',
//    'htmlOptions'=> array('class'=>'grid-view custom_here'),
        'pager' => array(
            'maxButtonCount'=>  CmsFormatter::$PAGE_MAX_BUTTON,
        ),
//	'enableSorting' => false,
	//'filter'=>$model,
	'columns'=>array(
        array(
            'header' => 'S/N',
            'type' => 'raw',
            'value' => '$this->grid->dataProvider->pagination->currentPage * $this->grid->dataProvider->pagination->pageSize + ($row+1)',
            'headerHtmlOptions' => array('width' => '10px','style' => 'text-align:center;'),
            'htmlOptions' => array('style' => 'text-align:center;')
        ),
        
        array(
            'name'=>'code',
            'type'=>'html',
            'value'=>'"<b>".$data->getTickets()->code_no."</b><br><br>".$data->getCreatedDate()',
            'htmlOptions' => array('style' => 'width:50px;'),
        ),
        
        array(
            'name'=>'Tiêu đề',
            'type'=>'html',
            'value'=>'$data->getTickets()->title',
            'htmlOptions'=> array('style' => 'width:50px;'),
        ),
        
        array(
            'name' => 'Người tạo',
            'type' => 'html',
            'value'=>'$data->getTickets()->getUidLoginName()',
            'htmlOptions' => array('style' => 'width:80px;'),
        ),
        
        array(
            'name' => 'Người trả lời',
            'type' => 'html',
            'value' => '$data->getTickets()->getProcessUserId()',
            'htmlOptions' => array('style' => 'width:60px;'),
        ),
        array(
            'name' => 'Nội dung',
            'type' => 'html',
            'value' => '"<p>".$data->getMessage()."</p>"',
            'htmlOptions' => array('style' => 'width:200px;'),
        ),
//        array(
////            'htmlOptions' => array('style' => 'width:30px;'),// Sep1717 htmlOptions làm mất biến button-column
//            'header' => 'Actions',
//            'class'=>'CButtonColumn',
//            'template'=> ControllerActionsName::createIndexButtonRoles($actions),
//            'buttons'=>array(
//                'update'=>array(
//                    'visible'=> '$data->canUpdateWeb()',
//                ),
//                'delete'=>array(
//                    'visible'=> 'GasCheck::canDeleteData($data)',
//                ),
//                
//            ),
//        ),
            array(
                'name' => 'Actions',
                'type' => 'html',
                'value' => '"<a class=\'view\' title=\'View\' href=\'".yii::app()->createAbsoluteUrl(\'admin/tickets/index\',array(\'ViewTicket\'=>$data->id))."\' target=\'_blank\'><img src=\'".Yii::app()->theme->baseUrl . \'/admin/images/icon/view_icon.png\'."\' alt=\'View\'></a>"',
                'htmlOptions' => array('style' => 'width:50px;text-align:center;'),
            ),
	),
)); 

?>

<script>
$(document).ready(function() {
    fnUpdateColorbox();
});

function fnUpdateColorbox(){   
    fixTargetBlank();
}
</script>