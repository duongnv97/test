<?php 
    $mTicket = new GasTickets();
?>
<?php if($allowChangeUser): ?>
<div class="styled-select" style="width: 100%;">
        <?php echo CHtml::dropDownList('GasTickets[send_to_id]', 0, 
            GasTickets::GetListSendTo(), ['class'=>'send_to_id','empty'=>'Đổi người xử lý']);?>
<br><br><br><br>
</div>
<?php endif;?>
<div class="styled-select" style="width: 100%;">
        <?php echo CHtml::dropDownList('GasTickets[issue_type]', 0, 
            $mTicket->getArrayIssue(), ['class'=>'issue_type','empty'=> $mTicket->getAttributeLabel('issue_type')]);?>
<br><br><br><br>
</div>
