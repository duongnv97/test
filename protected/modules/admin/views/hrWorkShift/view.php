<?php
$this->breadcrumbs=array(
    $this->pluralTitle => array('index'),
    ' ' . $this->singleTitle . ' : ',
);

$menus = array(
	array('label'=>"$this->pluralTitle Management", 'url'=>array('index')),
	array('label'=>"Tạo Mới $this->singleTitle", 'url'=>array('create')),
	array('label'=>"Cập Nhật $this->singleTitle", 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>"Xóa $this->singleTitle", 'url'=>array('delete'), 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Bạn chắc chắn muốn xóa ?')),
);
$this->menu= ControllerActionsName::createMenusRoles($menus, $actions);
?>

<h1>Xem: <?php echo $this->singleTitle; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
            'id',
            'name',
            array(
                'name' => 'shift_from',
                'type' => 'raw',
                'value' => $model->getShiftFrom()
            ),
            array(
                'name' => 'shift_to',
                'type' => 'raw',
                'value' => $model->getShiftTo()
            ),
            array(
                'name' => 'role_id',
                'type' => 'raw',
                'value' => $model->getRole()
            ),
            array(
                'name' => 'type',
                'type' => 'raw',
                'value' => $model->getType()
            ),
            array(
                'name' => 'factor',
                'type' => 'raw',
                'value' => $model->getFactor()
            ),
            array(
                'name' => 'status',
                'type' => 'raw',
                'value' => $model->getStatus()
            ),
	),
)); ?>
