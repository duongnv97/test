<div class="form">

<?php 

    
    $form=$this->beginWidget('CActiveForm', array(
            'id'=>'work-shift-form',
            'enableAjaxValidation'=>false,
            'htmlOptions' => array('enctype' => 'multipart/form-data'),
    )); ?>

    <p class="note">Dòng có dấu <span class="required">*</span> là bắt buộc.</p>
    <?php echo MyFormat::BindNotifyMsg(); ?>

    <div class="row buttons" >
        <?php $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType'=>'submit',
            'label'=>$model->isNewRecord ? 'Create' :'Save',
            'type'=>'null', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
            'size'=>'small', // null, 'large', 'small' or 'mini'
            //'htmlOptions' => array('style' => 'margin-bottom: 10px; float: right;'),
        )); ?>	
    </div>
    
    <div class="row">
        <?php echo $form->labelEx($model,'name'); ?>
        <?php echo $form->textField($model,'name'); ?>
        <?php echo $form->error($model,'name'); ?>
    </div>
    
    <div class="row">
        <?php echo $form->labelEx($model,'shift_from'); ?>
        <?php echo $form->dropDownList($model,'shift_from', $model->getArrayShiftTime()); ?>
        <?php echo $form->error($model,'shift_from'); ?>
    </div>
    
    <div class="row">
        <?php echo $form->labelEx($model,'shift_to'); ?>
        <?php echo $form->dropDownList($model,'shift_to', $model->getArrayShiftTime()); ?>
        <?php echo $form->error($model,'shift_to'); ?>
    </div>
    
    <div class="row">
        <?php echo $form->labelEx($model,'role_id'); ?>
        <?php echo $form->dropDownList($model,'role_id', HrSalaryReports::model()->getArrayRoleSalary(true)); ?> <!-- true : + ROLE_ALL -->
        <?php echo $form->error($model,'role_id'); ?>
    </div>
    
    <div class="row">
        <?php echo $form->labelEx($model,'type'); ?>
        <?php echo $form->dropDownList($model,'type', $model->getArrayType()); ?>
        <?php echo $form->error($model,'type'); ?>
    </div>
    
    <div class="row">
        <?php echo $form->labelEx($model,'factor'); ?>
        <?php echo $form->textField($model,'factor'); ?>
        <?php echo $form->error($model,'factor'); ?>
    </div>
    
    <div class="row">
        <?php echo $form->labelEx($model,'status'); ?>
        <?php echo $form->dropDownList($model,'status', $model->getArrayStatus()); ?>
        <?php echo $form->error($model,'status'); ?>
    </div>
    
    <div class="row buttons">
        <?php $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType'=>'submit',
            'label'=>$model->isNewRecord ? 'Create' :'Save',
            'type'=>'null', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
            'size'=>'small', // null, 'large', 'small' or 'mini'
            //'htmlOptions' => array('style' => 'margin-bottom: 10px; float: right;'),
        )); ?>	
    </div>

<?php $this->endWidget(); ?>
    
</div><!-- form -->
