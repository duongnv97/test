<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'parent_id',array()); ?>
		<?php echo $form->dropDownList($model,'parent_id', GasMaterials::getCatByParentId(0),array('empty'=>'No Parent')); ?>
	</div>
	<div class="row">
		<?php echo $form->label($model,'materials_no',array()); ?>
		<?php echo $form->textField($model,'materials_no',array('size'=>20,'maxlength'=>20)); ?>
	</div>
	<div class="row">
		<?php echo $form->label($model,'name',array()); ?>
		<?php echo $form->textField($model,'name',array('size'=>60,'maxlength'=>300)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'unit',array()); ?>
		<?php echo $form->textField($model,'unit',array('size'=>50,'maxlength'=>50)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'materials_type_id',array()); ?>
		<?php echo $form->dropDownList($model,'materials_type_id', GasMaterialsType::getAllItem(),array('empty'=>'Select')); ?>
	</div>

	<div class="row">
            <?php echo $form->label($model,'status',array()); ?>
            <?php echo $form->dropDownList($model,'status', ActiveRecord::getUserStatus(),array('empty'=>'Select')); ?>
	</div>
    
	<div class="row">
            <?php echo $form->label($model,'need_delete',array()); ?>
            <?php echo $form->dropDownList($model,'need_delete', ActiveRecord::getYesNo(),array('empty'=>'Select')); ?>
	</div>

	<div class="row buttons" style="padding-left: 159px;">
		        <?php $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType'=>'submit',
            'label'=>Yii::t('translation','Search'),
            'type'=>'null', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
            'size'=>'small', // null, 'large', 'small' or 'mini'
            //'htmlOptions' => array('style' => 'margin-bottom: 10px; float: right;'),
        )); ?>	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->