<?php 
$display_none   = 'display_none';
$readonly       = 1;
$cRole = MyFormat::getCurrentRoleId();
$aRoleAllow = [ROLE_ADMIN];
//if(in_array($cRole, $aRoleAllow)){
if(1){
    $display_none   = '';
    $readonly       = '';
}

?>
<div class="form ">
<?php Yii::app()->clientScript->registerCoreScript('jquery.ui'); ?>
<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'gas-materials-form',
	'enableAjaxValidation'=>false,
)); ?>

    <?php if(Yii::app()->user->hasFlash('successUpdate')): ?>
        <div class="success_div"><?php echo Yii::app()->user->getFlash('successUpdate');?></div>         
    <?php endif; ?>   
    <?php echo Yii::t('translation', '<p class="note">Fields with <span class="required">*</span> are required.</p>'); ?>

    <div class="row <?php echo $display_none;?>">
            <?php echo Yii::t('translation', $form->labelEx($model,'parent_id')); ?>
            <?php echo $form->dropDownList($model,'parent_id', GasMaterials::getCatByParentId(0),array('empty'=>'No Parent')); ?>
            <?php echo $form->error($model,'parent_id'); ?>
    </div>

    <div class="row">
            <?php echo Yii::t('translation', $form->labelEx($model,'materials_no')); ?>
            <?php echo $form->textField($model,'materials_no',array('size'=>20, 'readonly'=>$readonly)); ?>
            <?php echo $form->error($model,'materials_no'); ?>
    </div>

    <div class="row">
            <?php echo Yii::t('translation', $form->labelEx($model,'name')); ?>
            <?php echo $form->textField($model,'name',array('size'=>54, 'readonly'=>$readonly)); ?>
            <?php echo $form->error($model,'name'); ?>
    </div>
        
    <div class="row">
        <?php echo Yii::t('translation', $form->labelEx($model,'unit')); ?>
        <?php echo $form->textField($model,'unit',array('size'=>54, 'readonly'=>$readonly)); ?>
        <?php echo $form->error($model,'unit'); ?>
    </div>

    <div class="row">
            <?php echo $form->labelEx($model,'materials_id_vo'); ?>
            <?php // $dataCat = CHtml::listData(GasMaterials::getCatLevel2(GasMaterials::$MATERIAL_GAS),'id','name','group'); ?>
            <?php // echo $form->dropDownList($model,'materials_id_back', $dataCat,array('class'=>'category_ajax', 'style'=>'width:385px;','empty'=>'Select')); ?>		

            <?php echo $form->hiddenField($model,'materials_id_vo'); ?>		
            <?php 
                // widget auto complete search material
                $aData = array(
                    'model'=>$model,
                    'field_material_id'=>'materials_id_vo',
                    'name_relation_material'=>'rMaterialsVo',
                    'field_autocomplete_name'=>'autocomplete_material',
                );
                $this->widget('ext.GasAutocompleteMaterial.GasAutocompleteMaterial',
                    array('data'=>$aData));                                        
            ?>            
            <?php echo $form->error($model,'materials_id_vo'); ?>
    </div>
    
    <div class="row <?php echo $display_none;?>">
        <?php echo Yii::t('translation', $form->labelEx($model,'materials_type_id')); ?>
        <?php echo $form->dropDownList($model,'materials_type_id', GasMaterialsType::getAllItem(),array('empty'=>'Select')); ?>
        <?php echo $form->error($model,'materials_type_id'); ?>
    </div>
    
    <div class="row <?php echo $display_none;?>">
        <?php echo Yii::t('translation', $form->labelEx($model,'name_store_card')); ?>
        <?php echo $form->textField($model,'name_store_card',array('size'=>54,'maxlength'=>300)); ?>
        <?php echo $form->error($model,'name_store_card'); ?>
    </div>
    <div class="row <?php echo $display_none;?>">
        <?php echo Yii::t('translation', $form->labelEx($model,'price')); ?>
        <div class="w-300">
            <?php echo $form->textField($model,'price',array('size'=>54,'class'=>'number_only number_only_v1')); ?>
            <div class="help_number"></div>
            <?php echo $form->error($model,'price'); ?>
        </div>
    </div>
    <div class="row">
        <?php echo $form->labelEx($model,'description'); ?>
        <?php echo $form->textField($model,'description',array('size'=>54,'maxlength'=>300)); ?>
        <?php echo $form->error($model,'description'); ?>
    </div>
    <div class="row <?php echo $display_none;?>">
        <?php echo Yii::t('translation', $form->labelEx($model,'status')); ?>
        <?php echo $form->dropDownList($model,'status', ActiveRecord::getUserStatus()); ?>
        <?php echo $form->error($model,'status'); ?>
    </div>
    <div class="row checkbox_one <?php echo $display_none;?>">
        <label>&nbsp;</label>
        <?php echo $form->checkBox($model,'need_delete',array('class'=>'')); ?>
        <?php echo $form->labelEx($model,'need_delete',array('class'=>'checkbox_one_label')); ?>
    </div>          
    <div class="clr"></div> 
            
    <div class="row <?php echo $display_none;?>">
        <?php echo $form->labelEx($model,'image'); ?>
        <?php echo $form->textField($model,'image',array('class'=>'w-600','placeholder'=>'')); ?>
        <?php echo $form->error($model,'image'); ?>
    </div>
    <?php if(!empty($model->image)): ?>
        <div class="row">
            <label>Ảnh hiện tại</label>
            <p class="l_padding_300"><?php echo $model->getImage(); ?></p>
        </div>
        
    <?php endif; ?>
    <div class='clr'></div>
    <div class="row buttons" style="padding-left: 115px;">
        <?php $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType'=>'submit',
            'label'=>$model->isNewRecord ? Yii::t('translation', 'Create') : Yii::t('translation', 'Save'),
            'type'=>'null', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
            'size'=>'small', // null, 'large', 'small' or 'mini'
            //'htmlOptions' => array('style' => 'margin-bottom: 10px; float: right;'),
        )); ?>	
    </div>

<?php $this->endWidget(); ?>

</div><!-- form -->
<?php 
    if($cRole == ROLE_ADMIN){
        echo "SaleName:";
        echo '<pre>';
        print_r($model->getAttributes());
        echo '</pre>';
    }
?>
<script>
$(document).ready(function() {
    fnUpdateColorbox();
});

function fnUpdateColorbox(){   
//    jQuery('a.gallery').colorbox({ opacity:0.5 , rel:'group1' });
    $(".gallery").colorbox({iframe:true,innerHeight:'800', innerWidth: '800',close: "<span title='close'>close</span>"});
}
</script>