<?php
$this->breadcrumbs=array(
	'Vật Tư',
);

$menus=array(
        array('label'=> 'Xuất Excel Danh Sách Hiện Tại',
                        'url'=>array('index', 'ExportExcel'=>1), 
                        'htmlOptions'=>array('class'=>'export_excel ','label'=>'Xuất Excel'),
                    ),
	array('label'=> Yii::t('translation','Create GasMaterials'), 'url'=>array('create')),
);
$this->menu= ControllerActionsName::createMenusRoles($menus, $actions);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('gas-materials-grid', {
                url : $(this).attr('action'),
		data: $(this).serialize()
	});
	return false;
});
");

Yii::app()->clientScript->registerScript('ajaxupdate', "
$('#gas-materials-grid a.ajaxupdate').live('click', function() {
    $.fn.yiiGridView.update('gas-materials-grid', {
        type: 'POST',
        url: $(this).attr('href'),
        success: function() {
            $.fn.yiiGridView.update('gas-materials-grid');
        }
    });
    return false;
});
");
?>

<h1><?php echo Yii::t('translation', 'Vật Tư'); ?></h1>

<?php echo CHtml::link(Yii::t('translation','Advanced Search'),'#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'gas-materials-grid',
	'dataProvider'=>$model->search(),
	'afterAjaxUpdate'=>'function(id, data){ fnUpdateColorbox();}',
        'template'=>'{pager}{summary}{items}{pager}{summary}',     
	//'filter'=>$model,
	'columns'=>array(
        array(
            'header' => 'S/N',
            'type' => 'raw',
            'value' => '$this->grid->dataProvider->pagination->currentPage * $this->grid->dataProvider->pagination->pageSize + ($row+1)',
            'headerHtmlOptions' => array('width' => '30px','style' => 'text-align:center;'),
            'htmlOptions' => array('style' => 'text-align:center;')
        ),
            array(
                'name'=>'materials_type_id',
                'value'=>'$data->materials_type?$data->materials_type->name:""',
            ),
            array(
                'name'=>'parent_id',
                'value'=>'$data->parent?$data->parent->name:""',
            ),
            array(
                'name'=>'id',
                'htmlOptions' => array('style' => 'text-align:center;')
            ),
            'materials_no',
            'name',
            'unit',
            array(
                'name'=>'description',
                'value'=>'$data->getDescription()',
            ),
            array(
                'name'=>'materials_id_vo',
                'value'=>'$data->getVo()',
            ),  
            'name_store_card',
            array(
                'header' => 'Cần xóa',
                'name'=> 'need_delete',
                'value'=> '$data->getNeedDelete()',
                'htmlOptions' => array('style' => 'text-align:center;'),
            ),	
            array(
                'name'=>'image',
                'type'=>'raw',
                'value'=>'$data->getImage()',
            ),
            'price:Currency',
            array(
                'name'=>'status',
                'type'=>'status',
                'value'=>'array("status"=>$data->status,"id"=>$data->id)',
                'htmlOptions' => array('style' => 'text-align:center;')
            ),              
		array(
			'header' => 'Action',
			'class'=>'CButtonColumn',
                        'template'=> ControllerActionsName::createIndexButtonRoles($actions),
			'buttons'=>array(
                            'delete'=>array(
//                                'visible'=>'$data->canDelete()',
                                'visible'=>'0',
                            ),
			),                    
		),
	),
)); ?>

<script>
$(document).ready(function() {
    fnUpdateColorbox();
});

function fnUpdateColorbox(){   
//    jQuery('a.gallery').colorbox({ opacity:0.5 , rel:'group1' });
    $(".gallery").colorbox({iframe:true,innerHeight:'800', innerWidth: '800',close: "<span title='close'>close</span>"});
}
</script>