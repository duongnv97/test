<?php
$this->breadcrumbs=array(
	Yii::t('translation','Vật Tư')=>array('index'),
	Yii::t('translation','Create'),
);

$menus = array(		
        array('label'=> Yii::t('translation', 'Vật Tư') , 'url'=>array('index')),
);
$this->menu= ControllerActionsName::createMenusRoles($menus, $actions);
?>

<h1><?php echo Yii::t('translation', 'Create Vật Tư'); ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>