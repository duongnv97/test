<?php
$this->breadcrumbs=array(
	'Vật Tư'=>array('index'),
	$model->name,
);

$menus = array(
	array('label'=>'GasMaterials Management', 'url'=>array('index')),
	array('label'=>'Create GasMaterials', 'url'=>array('create')),
	array('label'=>'Update GasMaterials', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete GasMaterials', 'url'=>array('delete'), 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
);
$this->menu= ControllerActionsName::createMenusRoles($menus, $actions);
?>

<h1>View GasMaterials: <?php echo $model->name; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
            array(
                'name'=>'parent_id',
                'value'=>$model->parent?$model->parent->name:'',
            ),	            
            'materials_no',
            'name',
            'unit',
            array(
                'name'=>'description',
                'value'=>$model->getDescription(),
            ),		
            array(
                'name'=>'materials_type_id',
                'value'=>$model->materials_type?$model->materials_type->name:'',
            ),		
            array(
                'name'=>'materials_id_vo',
                'value'=>$model->getVo(),
            ),		

            'name_store_card',
            'price:Currency',
            'status:status',
            array(
                'name'=>'need_delete',
                'value'=>$model->getNeedDelete(),
            ),	
	),
)); ?>
<?php if(!empty($model->image)): ?>
    <div class="row">
        <label>Ảnh hiện tại</label>
        <p class="l_padding_100"><?php echo $model->getImage(true); ?></p>
    </div>
<?php endif; ?>
<script>
$(document).ready(function() {
    fnUpdateColorbox();
});

function fnUpdateColorbox(){   
//    jQuery('a.gallery').colorbox({ opacity:0.5 , rel:'group1' });
    $(".gallery").colorbox({iframe:true,innerHeight:'800', innerWidth: '800',close: "<span title='close'>close</span>"});
}
</script>