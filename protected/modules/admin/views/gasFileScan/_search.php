<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'code_no',array()); ?>
		<?php echo $form->textField($model,'code_no',array('class'=> 'w-200','maxlength'=>30)); ?>
	</div>
    
	<div class="row display_none">
		<?php echo Yii::t('translation', $form->label($model,'maintain_date')); ?>
                <?php 
                    $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                        'model'=>$model,        
                        'attribute'=>'maintain_date',
                        'options'=>array(
                            'showAnim'=>'fold',
                            'dateFormat'=> MyFormat::$dateFormatSearch,
//                            'minDate'=> '0',
                            'maxDate'=> '0',
                            'changeMonth' => true,
                            'changeYear' => true,
                            'showOn' => 'button',
                            'buttonImage'=> Yii::app()->theme->baseUrl.'/admin/images/icon_calendar_r.gif',
                            'buttonImageOnly'=> true,                                
                        ),        
                        'htmlOptions'=>array(
                            'class'=>'w-16',
                            'style'=>'height:20px;',
                        ),
                    ));
                ?> 
	</div>
    
        <div class="row more_col">
                <div class="col1">
                        <?php echo Yii::t('translation', $form->label($model,'date_from')); ?>
                        <?php 
                            $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                                'model'=>$model,        
                                'attribute'=>'date_from',
                                'options'=>array(
                                    'showAnim'=>'fold',
                                    'dateFormat'=> MyFormat::$dateFormatSearch,
        //                            'minDate'=> '0',
                                    'maxDate'=> '0',
                                    'changeMonth' => true,
                                    'changeYear' => true,
                                    'showOn' => 'button',
                                    'buttonImage'=> Yii::app()->theme->baseUrl.'/admin/images/icon_calendar_r.gif',
                                    'buttonImageOnly'=> true,                                
                                ),        
                                'htmlOptions'=>array(
                                    'class'=>'w-16',
                                    'size'=>'16',
                                    'style'=>'float:left;',                               
                                ),
                            ));
                        ?>     		
                </div>
                <div class="col2">
                        <?php echo Yii::t('translation', $form->label($model,'date_to')); ?>
                        <?php 
                            $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                                'model'=>$model,        
                                'attribute'=>'date_to',
                                'options'=>array(
                                    'showAnim'=>'fold',
                                    'dateFormat'=> MyFormat::$dateFormatSearch,
        //                            'minDate'=> '0',
                                    'maxDate'=> '0',
                                    'changeMonth' => true,
                                    'changeYear' => true,
                                    'showOn' => 'button',
                                    'buttonImage'=> Yii::app()->theme->baseUrl.'/admin/images/icon_calendar_r.gif',
                                    'buttonImageOnly'=> true,                                
                                ),        
                                'htmlOptions'=>array(
                                    'class'=>'w-16',
                                    'size'=>'16',
                                    'style'=>'float:left;',
                                ),
                            ));
                        ?>     		
                </div>
            </div>    
    
    
	<div class="row">
		<?php echo Yii::t('translation', $form->label($model,'created_date')); ?>
                <?php 
                    $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                        'model'=>$model,        
                        'attribute'=>'created_date',
                        'options'=>array(
                            'showAnim'=>'fold',
                            'dateFormat'=> MyFormat::$dateFormatSearch,
//                            'minDate'=> '0',
                            'maxDate'=> '0',
                            'changeMonth' => true,
                            'changeYear' => true,
                            'showOn' => 'button',
                            'buttonImage'=> Yii::app()->theme->baseUrl.'/admin/images/icon_calendar_r.gif',
                            'buttonImageOnly'=> true,                                
                        ),        
                        'htmlOptions'=>array(
                            'class'=>'w-16',
                            'style'=>'height:20px;',
                        ),
                    ));
                ?> 
	</div>

    
        <?php if(Yii::app()->user->role_id!=ROLE_SUB_USER_AGENT):?>
            <div class="row">
                <?php echo Yii::t('translation', $form->label($model,'agent_id')); ?>
                <?php echo $form->dropDownList($model,'agent_id', Users::getSelectByRole(),array('style'=>'width:350px;','empty'=>'Select')); ?>
                <?php echo $form->error($model,'agent_id'); ?>
            </div>    
        <?php endif;?>
    
        <div class="row">
                <?php echo Yii::t('translation', $form->label($model,'monitoring_id')); ?>
                <?php  echo $form->dropDownList($model,'monitoring_id', Users::getSelectByRole(ROLE_MONITORING_MARKET_DEVELOPMENT),array('style'=>'width:350px;','empty'=>'Select')); ?>
        </div>

        <div class="row">
                <?php echo Yii::t('translation', $form->label($model,'maintain_employee_id')); ?>
                <?php echo $form->dropDownList($model,'maintain_employee_id', Users::getSelectByRoleForAgent($model->monitoring_id, ONE_MONITORING_MARKET_DEVELOPMENT, $model->agent_id, array('status'=>1)),array('class'=>'category_ajax', 'style'=>'width:350px;','empty'=>'Select')); ?>		
        </div>   

	<div class="row">
		<?php echo $form->label($model,'note',array()); ?>
		<?php echo $form->textField($model,'note',array('size'=>54,'maxlength'=>500)); ?>
	</div>

        <div class="row more_col">
            <div class="col1">
		<?php echo $form->label($model,'ext_customer_name',array()); ?>
		<?php echo $form->textField($model,'ext_customer_name',array('class'=>'w-150','maxlength'=>500)); ?>
            </div>    
            <div class="col2">
                <?php echo $form->label($model,'ext_customer_address',array()); ?>
		<?php echo $form->textField($model,'ext_customer_address',array('class'=>'w-200','maxlength'=>500)); ?>
            </div>    
            <div class="col3">
                <?php echo $form->label($model,'ext_customer_phone',array()); ?>
		<?php echo $form->textField($model,'ext_customer_phone',array('class'=>'w-150','maxlength'=>500)); ?>                
            </div>
        </div>
    
        <div class="row more_col">
            <div class="col1">
		<?php echo $form->label($model,'ext_materials_name',array()); ?>
		<?php echo $form->textField($model,'ext_materials_name',array('class'=>'w-150','maxlength'=>500)); ?>
            </div>    
            <div class="col2">
                <?php echo $form->label($model,'ext_seri',array()); ?>
		<?php echo $form->textField($model,'ext_seri',array('class'=>'w-200','maxlength'=>500)); ?>
            </div>
            <div class="col3">
                <?php echo $form->label($model,'ext_note',array()); ?>
		<?php echo $form->textField($model,'ext_note',array('class'=>'w-150','maxlength'=>500)); ?>
            </div>                        
        </div>   
    
	<div class="row buttons" style="padding-left: 159px;">
		        <?php $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType'=>'submit',
            'label'=>'Search',
            'type'=>'null', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
            'size'=>'small', // null, 'large', 'small' or 'mini'
            //'htmlOptions' => array('style' => 'margin-bottom: 10px; float: right;'),
        )); ?>	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->