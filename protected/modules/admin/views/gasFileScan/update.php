<?php
$this->breadcrumbs=array(
	'Quản Lý PTTT Files Scan'=>array('index'),
	$model->code_no=>array('view','id'=>$model->id),
	'Cập Nhật',
);

$menus = array(	
        array('label'=>'Quản Lý PTTT File Scan', 'url'=>array('index')),
	array('label'=>'Xem File Scan', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Tạo Mới File Scan', 'url'=>array('create')),	
);
$this->menu= ControllerActionsName::createMenusRoles($menus, $actions);

?>

<h1>Cập Nhật PTTT File Scan: <?php echo $model->code_no; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>