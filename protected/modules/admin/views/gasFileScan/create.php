<?php
$this->breadcrumbs=array(
	'Quản Lý PTTT File Scan'=>array('index'),
	'Tạo Mới',
);

$menus = array(		
        array('label'=>'Quản Lý PTTT File Scan', 'url'=>array('index')),
);
$this->menu= ControllerActionsName::createMenusRoles($menus, $actions);
?>

<h1>Tạo Mới PTTT File Scan</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>