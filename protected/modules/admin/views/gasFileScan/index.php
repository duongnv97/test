<?php
$this->breadcrumbs=array(
	'Quản Lý PTTT File Scan',
);

$menus=array(
            array('label'=>'Tạo Mới PTTT File Scan', 'url'=>array('create')),
    array('label'=> 'Xuất Excel Danh Sách Hiện Tại',
            'url'=>array('ExportExcel'), 
            'htmlOptions'=>array('class'=>'export_excel ','label'=>'Xuất Excel')),
);
$this->menu= ControllerActionsName::createMenusRoles($menus, $actions);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('gas-file-scan-grid', {
                url : $(this).attr('action'),
		data: $(this).serialize()
	});
	return false;
});
");

Yii::app()->clientScript->registerScript('ajaxupdate', "
$('#gas-file-scan-grid a.ajaxupdate').live('click', function() {
    $.fn.yiiGridView.update('gas-file-scan-grid', {
        type: 'POST',
        url: $(this).attr('href'),
        success: function() {
            $.fn.yiiGridView.update('gas-file-scan-grid');
        }
    });
    return false;
});
");
?>

<h1>Quản Lý PTTT File Scan</h1>

<?php echo CHtml::link('Tìm Kiếm Nâng Cao','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'gas-file-scan-grid',
	'dataProvider'=>$model->search(),
        'afterAjaxUpdate'=>'function(id, data){ fnUpdateColorbox();}',
        'template'=>'{pager}{summary}{items}{pager}{summary}', 
        'pager' => array(
            'maxButtonCount'=>  CmsFormatter::$PAGE_MAX_BUTTON,
        ),            
//	'enableSorting' => false,
	//'filter'=>$model,
	'columns'=>array(
        array(
            'header' => 'S/N',
            'type' => 'raw',
            'value' => '$this->grid->dataProvider->pagination->currentPage * $this->grid->dataProvider->pagination->pageSize + ($row+1)',
            'headerHtmlOptions' => array('width' => '10px','style' => 'text-align:center;'),
            'htmlOptions' => array('style' => 'text-align:center;')
        ),
                array(
                    'name'=>'code_no',
                    'htmlOptions' => array('style' => 'text-align:center;width:50px;')
                ),                
                array(
                    'name'=>'agent_id',
                    'value'=>'$data->rAgent?$data->rAgent->first_name:""',
                    'htmlOptions' => array('style' => 'width:80px;')
                ),                
                array(
                    'name'=>'maintain_date',
                    'type'=>'date',
                    'htmlOptions' => array('style' => 'text-align:center;width:50px;')
                ),                
                array(
                    'name'=>'monitoring_id',
                    'value'=>'$data->rMonitoring?$data->rMonitoring->first_name:""',
                    'htmlOptions' => array('style' => 'width:80px;')
                ),                
                array(
                    'name'=>'maintain_employee_id',
                    'value'=>'$data->rEmployee?$data->rEmployee->first_name:""',
                    'htmlOptions' => array('style' => 'width:80px;')
                ),
                             
                array(
                    'name' => 'note',
                    'type' => 'html',
                    'value' => 'nl2br($data->note)',
                    'htmlOptions' => array('style' => 'width:120px;')
                ),     
                array(
                    'header' => 'Chi Tiết PTTT',
                    'type' => 'DetailFileScanInfo',
                    'value' => '$data',
//                    'htmlOptions' => array('style' => 'width:80px;')
                ),     
                array(
                    'name' => 'created_date',
                    'type' => 'Datetime',
                    'htmlOptions' => array('style' => 'width:50px;')
                ),            
                array(
                    'name' => 'last_update_time',
                    'type' => 'Datetime',
                    'htmlOptions' => array('style' => 'width:50px;'),
                    'visible'=>Yii::app()->user->role_id==ROLE_ADMIN,
                ),            
		/*nl2br()
		'note',
		'created_date',
		'last_update_by',
		'last_update_time',
		*/
		array(
                    'header' => 'Actions',
                    'class'=>'CButtonColumn',
                    'template'=> ControllerActionsName::createIndexButtonRoles($actions),
                    'buttons'=>array(
                        'update'=>array(
                            'visible'=> 'GasCheck::AgentCanUpdateFileScan($data)',
                        ),
                        'delete'=>array(
                            'visible'=> 'GasCheck::canDeleteData($data) && GasCheck::AgentCanDeleteFileScan($data)',
                        ),
                    ),                    
		),
	),
)); ?>

<script>
$(document).ready(function() {
    fnUpdateColorbox();
});

function fnUpdateColorbox(){   
    fixTargetBlank();
    $(".gallery").colorbox({iframe:true,innerHeight:'1500', innerWidth: '1050',close: "<span title='close'>close</span>"});
}
</script>