<h3 class='title-info'>Nhập Thông Tin Khách Hàng Của File Scan</h3>
<div class="row">
    <label>&nbsp</label>
    <div>
        <a href="javascript:void(0);" style="line-height:25px;" class="text_under_none item_b" onclick="fnBuildRow();">
            <img style="float: left;margin-right:8px;" src="<?php echo Yii::app()->theme->baseUrl;?>/images/add.png"> 
            Thêm Dòng ( Phím tắt F8 )
        </a>
    </div>
</div>
<div class="clr"></div>
<div class="row">
    <label>&nbsp</label>
    <table class="materials_table_info materials_table hm_table materials_table_th" style="">
        <thead>
            <tr>
                <th class="item_c w-10">#</th>
                <th class="w-150 item_c">Tên Khách Hàng ( không được bỏ trống )</th>
                <th class="w-180 item_c">Địa Chỉ</th>
                <th class="w-8 item_c">Điện Thoại</th>
                <th class="w-100 item_c">Thương Hiệu Gas</th>
                <th class="w-50 item_c">Seri</th>
                <th class="w-100 item_c">Ghi Chú</th>
                <th class="item_unit item_c last">Xóa</th>
            </tr>
        </thead>
        <tbody>
            <?php if(count($model->aModelFileScanInfo)):?>
            <?php foreach($model->aModelFileScanInfo as $key=>$item):?>
            <tr class="table_tr_row">
                <td class="order_no item_c"></td>
                <td class="item_c">
                    <input name="GasFileScanInfo[customer_name][]" class="customer_name" type="text" size="20" maxlength="300" value="<?php echo $item->customer_name;?>">
                </td>
                <td class="item_c">
                    <textarea name="GasFileScanInfo[customer_address][]" class="w-250" maxlength="300"><?php echo $item->customer_address;?></textarea>
                </td>
                <td class="item_c">
                    <input name="GasFileScanInfo[customer_phone][]" class="customer_phone  w-100" type="text" size="25" maxlength="30" value="<?php echo $item->customer_phone;?>">
                </td>
                <td class="item_c">
                    <input name="GasFileScanInfo[materials_name][]" class="materials_name" type="text" size="18" maxlength="100" value="<?php echo $item->materials_name;?>">
                </td>
                <td class="item_c">
                    <input name="GasFileScanInfo[seri][]" class="seri w-50" type="text" size="25" maxlength="30" value="<?php echo $item->seri;?>">
                </td>
                <td class="item_c">                    
                    <textarea name="GasFileScanInfo[note][]" class="w-150" maxlength="300"><?php echo $item->note;?></textarea>
                </td>
                <td class="item_c last"><span class="remove_icon_only"></span></td>
                
            </tr>
            <?php endforeach;?>
            <?php endif;?>

        </tbody>
    </table>
</div>


<script>

$(document).keydown(function(e) {
    if(e.which == 119) {
        fnBuildRow();
    }
});


function fnBuildRow(){
    var tr_new = $('.table_tr_row:first').clone();
    tr_new.find('.remove_icon_only').css({'display':'block'});
    tr_new.find('input').val('');
    tr_new.find('textarea').val('');

    tr_new.removeClass('selected');
    $('.materials_table_info tbody').append(tr_new);        
    fnRefreshOrderNumber();
}

    
    
</script>