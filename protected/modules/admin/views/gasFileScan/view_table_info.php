
<div class="clr"></div>
<h3 class='title-info'>Thông Tin Khách Hàng Của File Scan</h3>
<div class="row">
    <label>&nbsp</label>
    <table class="materials_table_info materials_table hm_table materials_table_th" style="">
        <thead>
            <tr>
                <th class="item_c w-10">#</th>
                <th class="w-180 item_c">Tên Khách Hàng</th>
                <th class="w-250 item_c">Địa Chỉ</th>
                <th class="w-120 item_c">Điện Thoại</th>
                <th class="w-120 item_c">Thương Hiệu Gas</th>
                <th class="w-80 item_c">Seri</th>
                <th class="w-150 item_c">Ghi Chú</th>
            </tr>
        </thead>
        <tbody>
            <?php if(count($model->aModelFileScanInfo)):?>
            <?php foreach($model->aModelFileScanInfo as $key=>$item):?>
            <tr class="table_tr_row">
                <td class="order_no item_c"><?php echo $key+1;?></td>
                <td class="item_c">
                    <?php echo $item->customer_name;?>
                </td>
                <td class="">
                    <?php echo $item->customer_address;?>
                </td>
                <td class="item_c">
                    <?php echo $item->customer_phone;?>
                </td>
                <td class="item_c">
                    <?php echo $item->materials_name;?>
                </td>
                <td class="item_c">
                    <?php echo $item->seri;?>
                </td>
                <td class="">                    
                    <?php echo $item->note;?>
                </td>
            </tr>
            <?php endforeach;?>
            <?php endif;?>

        </tbody>
    </table>
</div>