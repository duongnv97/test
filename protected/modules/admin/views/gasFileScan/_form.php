<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'gas-file-scan-form',
	'enableAjaxValidation'=>false,
    'htmlOptions' => array('enctype' => 'multipart/form-data'),
)); ?>

	<p class="note">Dòng có dấu <span class="required">*</span> là bắt buộc.</p>
        <em class="hight_light item_b">Chú Ý: Bạn phải xoay hình để             
            hiển thị bình thường trước khi upload lên. <a target="_blank" href="<?php echo Yii::app()->createAbsoluteUrl('admin/site/news',array('id'=>28));?>">Xem Hướng Dẫn</a></em>
        <?php  if(Yii::app()->user->hasFlash('successUpdate')): ?>
            <div class='success_div'><?php echo Yii::app()->user->getFlash('successUpdate');?>
                <?php if(isset($_GET['view_id'])):?>
                    <?php $mNew = GasFileScan::model()->findByPk($_GET['view_id']); ?>
                    &nbsp;&nbsp;&nbsp;<a href="<?php echo Yii::app()->createAbsoluteUrl('admin/gasFileScan/view',array('id'=>$_GET['view_id'])); ?>" target="_blank" title="Xem lại file upload PTTT"><?php echo $mNew->code_no;?></a>
                <?php endif;?>
            </div>         
        <?php endif; ?>  	
        
        <?php echo $form->errorSummary($model); ?>

        <div class="row">
        <?php echo $form->labelEx($model,'agent_id'); ?>
        <?php echo $form->hiddenField($model,'agent_id', array('class'=>'')); ?>
        <?php
                // 1. limit search kh của sale
                $url = Yii::app()->createAbsoluteUrl('admin/ajax/search_by_role', array('role'=> ROLE_AGENT));
                // widget auto complete search user customer and supplier
                $aData = array(
                    'model'=>$model,
                    'field_customer_id'=>'agent_id',
                    'url'=> $url,
                    'name_relation_user'=>'rAgent',
                    'ClassAdd' => 'w-400',
                    'field_autocomplete_name' => 'autocomplete_name',
                    'placeholder'=>'Nhập mã hoặc tên đại lý',
                );
                $this->widget('ext.SpaAutocompleteCustomer.SpaAutocompleteCustomer',
                    array('data'=>$aData));
                ?>
            <?php echo $form->error($model,'agent_id'); ?>
        </div>
        
        
	<div class="row">
		<?php echo Yii::t('translation', $form->labelEx($model,'maintain_date')); ?>
                <?php 
                    $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                        'model'=>$model,        
                        'attribute'=>'maintain_date',
                        'options'=>array(
                            'showAnim'=>'fold',
                            'dateFormat'=> ActiveRecord::getDateFormatJquery(),
//                            'minDate'=> '0',
                            'maxDate'=> '0',
                            'changeMonth' => true,
                            'changeYear' => true,
                            'showOn' => 'button',
                            'buttonImage'=> Yii::app()->theme->baseUrl.'/admin/images/icon_calendar_r.gif',
                            'buttonImageOnly'=> true,                                
                        ),        
                        'htmlOptions'=>array(
                            'class'=>'w-16',
                            'style'=>'height:20px;',
                                'readonly'=>'readonly',
                        ),
                    ));
                ?>     		
		<?php echo $form->error($model,'maintain_date'); ?>
	</div>
	<div class="row">
		<?php echo $form->labelEx($model,'monitoring_id'); ?>
		<?php  echo $form->dropDownList($model,'monitoring_id', Users::getSelectByRole(ROLE_MONITORING_MARKET_DEVELOPMENT),array('style'=>'width:376px;','empty'=>'Select')); ?>
		<?php echo $form->error($model,'monitoring_id'); ?>
	</div>
	<div class="row">
		<?php echo $form->labelEx($model,'maintain_employee_id'); ?>
		<?php echo $form->dropDownList($model,'maintain_employee_id', Users::getSelectByRoleForAgent($model->monitoring_id, ONE_MONITORING_MARKET_DEVELOPMENT, $model->agent_id, array('status'=>1,'form_create'=>1)),array('class'=>'category_ajax', 'style'=>'width:376px;','empty'=>'Select')); ?>
		<?php echo $form->error($model,'maintain_employee_id'); ?>
	</div>
            
        <div class="row">
            <label>&nbsp</label>
            <table class="materials_table hm_table">
                <thead>
                    <tr>
                        <th class="item_c">#</th>
                        <th class="item_code item_c">File Scan ( cho phép định dạng <?php echo GasFileScanDetail::$AllowFile;?>)</th>
                        <th class="item_unit last item_c">Xóa</th>
                    </tr>
                </thead>
                <tbody>
                    <?php 
                        $max_upload = 10;
                    ?>
                    <?php if(count($model->aModelDetail)):?>
                    <?php foreach($model->aModelDetail as $key=>$item):?>
                    <tr class="materials_row">
                        <td class="item_c order_no"></td>
                        <td class="item_c w-400">
                            <?php // echo $form->fileField($item,'file_name[]'); ?>
                            <?php if(!empty($item->file_name) && !empty($item->maintain_date)): ?>
                            <p>
                                <a rel="group1" class="gallery" href="<?php echo ImageProcessing::bindImageByModel($item);?>"> 
                                    <img width="100" height="70" src="<?php echo ImageProcessing::bindImageByModel($item);?>">
                                </a>
                            </p>
                            <?php endif;?>
                            <?php echo $form->error($item,'file_name'); ?>
                            <?php echo $form->hiddenField($item,'aIdNotIn[]', array('value'=>$item->id)); ?>
                        </td>
                        <td class="item_c last">
                            <?php if(GasFileScan::CanRemoveFile($model)): ?>
                            <span class="remove_icon_only"></span>
                            <?php endif;?>
                        </td>
                    </tr> 
                    <?php $max_upload--;?>
                    <?php endforeach;?>
                    <?php endif;?>
                    
                    <?php for($i=1; $i<=10; $i++): ?>
                    <?php $display = "";
                        if($i>$max_upload)
                            $display = "display_none";
                    ?>
                    <tr class="materials_row <?php echo $display;?>">
                        <td class="item_c order_no"></td>
                        <td class="item_l w-400">
                            <?php echo $form->fileField($model->mDetail,'file_name[]', array('accept'=>'image/*')); ?>
                            <?php // echo $form->error($model->mDetail,'file_name'); ?>                        
                        </td>
                        <td class="item_c last"><span class="remove_icon_only"></span></td>
                    </tr>                    
                    <?php endfor;?>
                </tbody>
            </table>
        </div>            
        
        <!--<em class="hight_light item_b">Chú Ý: Phần ghi chú nhập tiếng việt không dấu</em>-->
        <div class="clr"></div>
            
	<div class="row">
            <?php echo $form->labelEx($model,'note'); ?>
            <?php echo $form->textArea($model,'note',array('rows'=>5,'cols'=>80,"placeholder"=>"Nhập tên các đường có trong file PTTT")); ?>
            <?php echo $form->error($model,'note'); ?>
	</div>
        
	<div class="row">
            <?php echo $form->labelEx($model,'file_excel'); ?>
            <div style="padding-left: 140px;">
                <?php echo $form->fileField($model,'file_excel'); ?>
                <em class="hight_light item_b">Hệ thống chuyển sang nhập từ file excel. Bạn tải biểu mẫu về để thao tác</em>
                <em class="hight_light item_b"><a href="<?php echo Yii::app()->createAbsoluteUrl('/').'/upload/excel/pttt.xlsx';?>">Tải Biểu Mẫu Excel </a></em>
                <br>
                <em class="hight_light item_b">Mỗi lần thao tác nhập file, bạn hãy luôn tải file excel ở link trên về để nhập, tránh bị lỗi.</em>
                <br>
                <!--<em class="hight_light item_b">Quy Định Bắt Buộc Khi Nhập File Excel: Bảng Mã Unicode, kiểu gõ Telex. Nếu bạn gõ kiểu VNI hệ thống sẽ không hỗ trợ tìm kiếm. </em>-->
            </div>
            <?php echo $form->error($model,'file_excel'); ?>
	</div>
        
        <?php // include '_form_info.php';?>
	<br><br>
	<div class="row buttons" style="padding-left: 141px;">
		        <?php $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType'=>'submit',
            'label'=>$model->isNewRecord ? 'Create' :'Save',
            'type'=>'null', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
            'size'=>'small', // null, 'large', 'small' or 'mini'
            //'htmlOptions' => array('style' => 'margin-bottom: 10px; float: right;'),
        )); ?>	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->

<?php include 'view_table_info.php';?>

<script>
    $(document).ready(function(){
        $('.form').find('button:submit').click(function(){
            $.blockUI({ overlayCSS: { backgroundColor: '#fff' } }); 
        });
        fnRefreshOrderNumber();
        fnBindRemoveIcon();
        jQuery('a.gallery').colorbox({ opacity:0.5 , rel:'group1' });
    });
</script>

<!--<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/jquery.floatThead.min.js"></script>    
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/underscore-min.js"></script>    -->

<script>
    $(document).ready(function(){
//        $('.materials_table_info').floatThead();
        $('.table_tr_row:first').find('.remove_icon_only').hide();
	
        $('#GasFileScan_monitoring_id').live('change',function(){			
            var monitoring_id = $(this).val();        
            var url_ = "<?php echo Yii::app()->createAbsoluteUrl('admin/ajax/get_slt_monitoring_employee');?>";
            $.blockUI({ overlayCSS: { backgroundColor: '<?php echo BLOCK_UI_COLOR;?>' } }); 
            $.ajax({
                    url: url_,
                    data: {ajax:1,monitoring_id:monitoring_id},
                    type: "get",
                    dataType:'json',
                    success: function(data){
                            $('#GasFileScan_maintain_employee_id').html(data['html']);                
                            $.unblockUI();
                    }
            });
        });   	
    });


    function fnAfterRemoveIcon(){
    // tại sao lại chỉ xử lý khi update, khi tạo mới lại ko dc???
        <?php if(!$model->isNewRecord): ?>
                $('.materials_table tbody').find('.display_none').eq(0).removeClass('display_none');
        <?php endif;?>
    }
    
</script>
