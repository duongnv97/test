<div class="row w-400 tb_file float_l">
        <div>
            <a href="javascript:void(0);" class="text_under_none item_b" style="line-height:25px" onclick="fnBuildRowFile(this);">
                <img style="float: left;margin-right:8px;" src="<?php echo Yii::app()->theme->baseUrl;?>/images/add.png"> 
                Thêm Dòng
            </a>
        </div>
        <table class="materials_table hm_table">
            <thead>
                <tr>
                    <th class="item_c">#</th>
                    <th class="item_code item_c">Upload chứng từ file đính kèm. Cho phép <?php echo GasFile::$AllowFile;?></th>
                    <th class="item_c">Xóa</th>
                </tr>
            </thead>
            <tbody>
                <?php include "_upload_multi_update.php";?>
                <?php for($i=1; $i<=( GasSettle::MAX_SETTLE_ITEMS); $i++): ?>
                <?php $display = "";
                    if($i > GasFile::IMAGE_MAX_UPLOAD_SHOW)
                        $display = "display_none";
                ?>
                <tr class="materials_row <?php echo $display;?>">
                    <td class="item_c order_no"><?php echo $i;?></td>
                    <td class="item_l w-400">
                        <?php echo $form->fileField($model,'file_image[]',array('class'=>'input_file', 'accept'=>'image/*')); ?>
                    </td>
                    <td class="item_c last"><span class="remove_icon_only"></span></td>
                </tr>
                <?php endfor;?>
            </tbody>
        </table>
    <?php // echo $form->labelEx($mTaskDaily,'file_name'); ?>
    <?php echo $form->error($model,'image'); ?>
</div>
<div class="clr"></div>

<script>
    function fnBuildRowFile(this_){
        $(this_).closest('.tb_file').find('.materials_table').find('tr:visible:last').next('tr').show();
    }
</script>