<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=> GasCheck::getCurl(),
	'method'=>'get',
)); ?>
        <div class="row">
                <?php echo $form->label($model,'type',array()); ?>
                <?php echo $form->dropDownList($model,'type',  $model->getArrayType() ,array('class'=>'w-200','empty'=>'Select')); ?>
        </div>
    
	<div class="row">
		<?php echo $form->label($model,'partner_name',array()); ?>
		<?php echo $form->textField($model,'partner_name'); ?>
	</div>

	<div class="row">
                <?php echo $form->label($model,'reward_category_id',array()); ?>
                <?php echo $form->dropDownList($model,'reward_category_id',  $model->getArrayCategory() ,array('class'=>'w-200','empty'=>'Select')); ?>
	</div>

<!--	<div class="row">
		<?php // echo $form->labelEx($model,'material_id'); ?>
                <?php // echo $form->hiddenField($model,'material_id'); ?>		
                <?php 
//                    // widget auto complete search material
//                    $aData = array(
//                        'model'=>$model,
//                        'field_material_id'=>'material_id',
//                        'name_relation_material'=>'rMaterial',
//                        'field_autocomplete_name'=>'autocomplete_gas',
//                    );
//                    $this->widget('ext.GasAutocompleteMaterial.GasAutocompleteMaterial',
//                        array('data'=>$aData));                                        
                ?>             
                <?php // echo $form->error($model,'material_id'); ?>
	</div>-->

        <div class="row more_col">
            <div class =""><?php echo $form->label($model,'date_from'); ?></div>
            <div class="col2">
                    <?php // echo Yii::t('translation', $form->label($model,'start_date_from')); ?>
                    <?php 
                        $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                            'model'=>$model,        
                            'attribute'=>'start_date_from',
                            'options'=>array(
                                'showAnim'=>'fold',
                                'dateFormat'=> MyFormat::$dateFormatSearch,
    //                            'minDate'=> '0',
//                                'maxDate'=> '0',
                                'changeMonth' => true,
                                'changeYear' => true,
                                'showOn' => 'button',
                                'buttonImage'=> Yii::app()->theme->baseUrl.'/admin/images/icon_calendar_r.gif',
                                'buttonImageOnly'=> true,                                
                            ),        
                            'htmlOptions'=>array(
                                'class'=>'w-16',
                                'size'=>'16',
                                'style'=>'float:left;',                               
                            ),
                        ));
                    ?>     		
            </div>
            <div class="col3">
                    <?php echo Yii::t('translation', $form->label($model,'start_date_to')); ?>
                    <?php 
                        $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                            'model'=>$model,        
                            'attribute'=>'start_date_to',
                            'options'=>array(
                                'showAnim'=>'fold',
                                'dateFormat'=> MyFormat::$dateFormatSearch,
    //                            'minDate'=> '0',
//                                'maxDate'=> '0',
                                'changeMonth' => true,
                                'changeYear' => true,
                                'showOn' => 'button',
                                'buttonImage'=> Yii::app()->theme->baseUrl.'/admin/images/icon_calendar_r.gif',
                                'buttonImageOnly'=> true,                                
                            ),        
                            'htmlOptions'=>array(
                                'class'=>'w-16',
                                'size'=>'16',
                                'style'=>'float:left;',
                            ),  
                        ));
                    ?>     		
            </div>
        </div>
    <div class="row more_col">
            <div class =""><?php echo $form->label($model,'date_to'); ?></div>
            <div class="col2">
                    <?php // echo Yii::t('translation', $form->label($model,'end_date_from')); ?>
                    <?php 
                        $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                            'model'=>$model,        
                            'attribute'=>'end_date_from',
                            'options'=>array(
                                'showAnim'=>'fold',
                                'dateFormat'=> MyFormat::$dateFormatSearch,
    //                            'minDate'=> '0',
//                                'maxDate'=> '0',
                                'changeMonth' => true,
                                'changeYear' => true,
                                'showOn' => 'button',
                                'buttonImage'=> Yii::app()->theme->baseUrl.'/admin/images/icon_calendar_r.gif',
                                'buttonImageOnly'=> true,                                
                            ),        
                            'htmlOptions'=>array(
                                'class'=>'w-16',
                                'size'=>'16',
                                'style'=>'float:left;',                               
                            ),
                        ));
                    ?>     		
            </div>
            <div class="col3">
                    <?php echo Yii::t('translation', $form->label($model,'end_date_to')); ?>
                    <?php 
                        $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                            'model'=>$model,        
                            'attribute'=>'end_date_to',
                            'options'=>array(
                                'showAnim'=>'fold',
                                'dateFormat'=> MyFormat::$dateFormatSearch,
    //                            'minDate'=> '0',
//                                'maxDate'=> '0',
                                'changeMonth' => true,
                                'changeYear' => true,
                                'showOn' => 'button',
                                'buttonImage'=> Yii::app()->theme->baseUrl.'/admin/images/icon_calendar_r.gif',
                                'buttonImageOnly'=> true,                                
                            ),        
                            'htmlOptions'=>array(
                                'class'=>'w-16',
                                'size'=>'16',
                                'style'=>'float:left;',
                            ),
                        ));
                    ?>     		
            </div>
        </div>
    
    
    
	<div class="row buttons" style="padding-left: 159px;">
		        <?php $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType'=>'submit',
            'label'=>'Search',
            'type'=>'null', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
            'size'=>'small', // null, 'large', 'small' or 'mini'
            //'htmlOptions' => array('style' => 'margin-bottom: 10px; float: right;'),
        )); ?>	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->