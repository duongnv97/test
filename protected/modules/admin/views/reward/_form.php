<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'reward-form',
	'enableAjaxValidation'=>false,
        'htmlOptions' => array('enctype' => 'multipart/form-data'),
)); ?>

    <p class="note">Dòng có dấu <span class="required">*</span> là bắt buộc.</p>
    <?php echo MyFormat::BindNotifyMsg(); ?>
     
    <div class="row">
        <?php echo $form->labelEx($model, 'type'); ?>
        <?php echo $form->dropDownList($model, 'type', $model->getArrayType(), array('style'=>'','class'=>'w-200','empty'=>'Select')); ?>
        <?php echo $form->error($model, 'type'); ?>
    </div> 
        
   <div class="row">
        <?php echo $form->labelEx($model, 'title'); ?>
        <?php echo $form->textField($model, 'title',array('style'=>'','class'=>'w-600')); ?>
        <?php echo $form->error($model, 'title'); ?>
    </div>
    <label class="">Mỗi điểm nổi bật cách nhau bởi dấu ';'</label>
    <div class="row">
        <?php echo $form->labelEx($model,'highlights'); ?>
        <?php echo $form->textArea($model,'highlights',array('style'=>'','class'=>'w-600')); ?>
        <?php echo $form->error($model,'highlights'); ?>
    </div>
    
    <div class="row">
        <?php echo $form->labelEx($model,'banner'); ?>
        <div style="display: inline-block">
        <?php echo $model->getViewImg('rFileBanner'); ?>
        <?php echo $form->fileField($model,'banner', array('accept'=>'image/*')); ?>
        </div>
        <?php echo $form->error($model,'banner'); ?>
    </div>
    
    <div class="row">
        <?php echo $form->labelEx($model,'partner_logo'); ?>
        <div style="display: inline-block">
        <?php echo $model->getViewImg('rFileLogo'); ?>
        <?php echo $form->fileField($model,'partner_logo', array('accept'=>'image/*')); ?>
        </div>
        <?php echo $form->error($model,'partner_logo'); ?>
    </div>
    
     <div class="row">
        <?php echo $form->labelEx($model,'partner_name'); ?>
        <?php echo $form->textField($model,'partner_name',array('style'=>'','class'=>'w-400')); ?>
        <?php echo $form->error($model,'partner_name'); ?>
    </div>
    <div class="row">
        <?php echo $form->labelEx($model,'partner_address'); ?>
        <?php echo $form->textArea($model,'partner_address',array('style'=>'','class'=>'w-600')); ?>
        <?php echo $form->error($model,'partner_address'); ?>
    </div>
    <div class="row">
        <?php echo $form->labelEx($model,'partner_phone'); ?>
        <?php echo $form->textArea($model,'partner_phone',array('style'=>'','class'=>'w-600')); ?>
        <?php echo $form->error($model,'partner_phone'); ?>
    </div>
    <div class="row">
        <?php echo $form->labelEx($model,'partner_about'); ?>
        <?php echo $form->textArea($model,'partner_about',array('style'=>'','class'=>'w-600')); ?>
        <?php echo $form->error($model,'partner_about'); ?>
    </div>
   
    <div class="row">
        <?php echo $form->labelEx($model,'reward_category_id'); ?>
        <?php echo $form->dropDownList($model, 'reward_category_id', $model->getArrayCategory(), array('style'=>'','class'=>'w-200','empty'=>'Select')); ?>
        <?php echo $form->error($model,'reward_category_id'); ?>
    </div>
    
    <div class="row">
        <?php echo $form->labelEx($model,'material_id'); ?>
                <?php echo $form->hiddenField($model,'material_id'); ?>		
                <?php 
                    // widget auto complete search material
                    $aData = array(
                        'model'=>$model,
                        'field_material_id'=>'material_id',
                        'name_relation_material'=>'rMaterial',
                        'field_autocomplete_name'=>'autocomplete_gas',
                    );
                    $this->widget('ext.GasAutocompleteMaterial.GasAutocompleteMaterial',
                        array('data'=>$aData));                                        
                ?>             
                <?php echo $form->error($model,'material_id'); ?>
    </div>
    <div class="row">
        <?php echo $form->labelEx($model, 'qty'); ?>
        <?php echo $form->textField($model, 'qty',array('style'=>'','class'=>'w-600')); ?>
        <?php echo $form->error($model, 'qty'); ?>
    </div>
    <label class="">Mỗi điểm cách nhau bởi dấu ;</label> 
    <div class="row">
        <?php echo $form->labelEx($model,'description'); ?>
        <?php echo $form->textArea($model,'description',array('rows'=>3, 'cols'=>50)); ?>
        <?php echo $form->error($model,'description'); ?>
    </div>
    
    <div class="row more_col">
        <div class="col1">
                <?php echo $form->labelEx($model,'date_from'); ?>
                <?php 
                    $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                        'model'=>$model,        
                        'attribute'=>'date_from',
                        'options'=>array(
                            'showAnim'=>'fold',
                            'dateFormat'=> ActiveRecord::getDateFormatJquery(),
            //                            'minDate'=> '0',
//                            'maxDate'=> '1',
                            'changeMonth' => true,
                            'changeYear' => true,
                            'showOn' => 'button',
                            'buttonImage'=> Yii::app()->theme->baseUrl.'/admin/images/icon_calendar_r.gif',
                            'buttonImageOnly'=> true,                                
                        ),        
                        'htmlOptions'=>array(
                            'class'=>'w-16',
                            'style'=>'height:20px;',
                                'readonly'=>'readonly',
                            
                        ),
                    ));
                ?>     		
                <?php echo $form->error($model,'date_from'); ?>
        </div>
            <!--<label>&nbsp;</label>-->
        <div class="col2">
                <?php echo $form->labelEx($model,'date_to'); ?>
                <?php 
                    $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                        'model'=>$model,        
                        'attribute'=>'date_to',
                        'options'=>array(
                            'showAnim'=>'fold',
                            'dateFormat'=> ActiveRecord::getDateFormatJquery(),
//                            'minDate'=> '0',
//                            'maxDate'=> '0',
                            'changeMonth' => true,
                            'changeYear' => true,
                            'showOn' => 'button',
                            'buttonImage'=> Yii::app()->theme->baseUrl.'/admin/images/icon_calendar_r.gif',
                            'buttonImageOnly'=> true,                                
                        ),        
                         'htmlOptions'=>array(
                            'class'=>'w-16',
                            'style'=>'height:20px;',
                                'readonly'=>'readonly',
                        ),
                    ));
                ?>     
                 <?php echo $form->error($model,'date_to'); ?>
        </div>
        </div>
    
    <div class="row">
        <?php echo $form->labelEx($model,'point_apply'); ?>
        <?php echo $form->textField($model,'point_apply',array('size'=>10,'maxlength'=>10)); ?>
        <?php echo $form->error($model,'point_apply'); ?>
    </div>
    <div class="row">
        <?php echo $form->labelEx($model,'count_per_user'); ?>
        <?php echo $form->textField($model,'count_per_user',array('size'=>10,'maxlength'=>10)); ?>
        <?php echo $form->error($model,'count_per_user'); ?>
    </div>
    
    <div class="row">
       <?php echo $form->labelEx($model,'image'); ?>
       <?php include '_upload_multi.php'?>
    </div>
	<div class="row buttons" style="padding-left: 141px;">
		        <?php $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType'=>'submit',
            'label'=>$model->isNewRecord ? 'Create' :'Save',
            'type'=>'null', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
            'size'=>'small', // null, 'large', 'small' or 'mini'
            //'htmlOptions' => array('style' => 'margin-bottom: 10px; float: right;'),
        )); ?>	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/jquery.colorbox-min.js"></script>    
<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/admin/css/colorbox.css" />
<script>
    $(document).ready(function(){
        $('.form').find('button:submit').click(function(){
            $.blockUI({ overlayCSS: { backgroundColor: '#fff' } }); 
        });
        
    });
    $(window).load(function () { // không dùng dc cho popup
        $(".gallery").colorbox({iframe:true,innerHeight:'1100', innerWidth: '1000',close: "<span title='close'>close</span>"});
    });
</script>