<?php
$this->breadcrumbs=array(
    $this->singleTitle,
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('reward-grid', {
                url : $(this).attr('action'),
		data: $(this).serialize()
	});
	return false;
});
");

Yii::app()->clientScript->registerScript('ajaxupdate', "
$('#reward-grid a.ajaxupdate').live('click', function() {
    $.fn.yiiGridView.update('reward-grid', {
        type: 'POST',
        url: $(this).attr('href'),
        success: function() {
            $.fn.yiiGridView.update('reward-grid');
        }
    });
    return false;
});
");
?>

<h1><?php echo $this->pageTitle; ?></h1>


<?php echo CHtml::link('Tìm Kiếm Nâng Cao','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:">
    <?php $this->renderPartial('searchPointCustomer',array(
        'model'=>$model,
    )); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
    'id'=>'reward-grid',
    'dataProvider'=>$model->search(),
    'afterAjaxUpdate'=>'function(id, data){ fnUpdateColorbox();}',
    'template'=>'{pager}{summary}{items}{pager}{summary}',
//    'itemsCssClass' => 'items custom_here',
//    'htmlOptions'=> array('class'=>'grid-view custom_here'),
    'pager' => array(
        'maxButtonCount'=>  CmsFormatter::$PAGE_MAX_BUTTON,
    ),
    'enableSorting' => false,
    //'filter'=>$model,
    'columns'=>array(
        array(
            'header' => 'S/N',
            'type' => 'raw',
            'value' => '$this->grid->dataProvider->pagination->currentPage * $this->grid->dataProvider->pagination->pageSize + ($row+1)',
            'headerHtmlOptions' => array('width' => '10px','style' => 'text-align:center;'),
            'htmlOptions' => array('style' => 'text-align:center;')
        ),
        array(
            'header' => 'Họ tên KH',
            'type' => 'html',
            'value' => '$data->getCustomerName()',
        ),
        array(
            'header' => 'Địa chỉ',
            'type' => 'html',
            'value' => '$data->getAddress()',
        ),
        array(
            'header' => 'Số điện thoại',
            'type' => 'html',
            'value' => '$data->getPhone()',
            'htmlOptions' => array('style' => 'text-align:right;')
        ),
        array(
            'header' => 'Tổng điểm tích lũy',
            'type' => 'raw',
            'value' => '$data->getPoint()',
            'htmlOptions' => array('style' => 'text-align:center;')
        ),
        array(
            'header' => 'Ngày tạo KH',
            'type' => 'html',
            'value' => '$data->getCustomerCreatedDate()',
        ),
    ),
)); ?>

<script>
    $(document).ready(function() {
        fnUpdateColorbox();
    });

    function fnUpdateColorbox(){
        fixTargetBlank();
    }
</script>