<div class="wide form">

    <?php $form=$this->beginWidget('CActiveForm', array(
        'action'=> GasCheck::getCurl(),
        'method'=>'get',
    )); ?>
    <div class="row">
        <?php echo $form->labelEx($model,'user_id',array('label'=>'Khách hàng')); ?>
        <?php echo $form->hiddenField($model,'user_id', array('class'=>'')); ?>
        <?php
        // 1. limit search kh của sale
        $url = Yii::app()->createAbsoluteUrl('admin/ajax/search_user_by_code');
        // widget auto complete search user customer and supplier
        $aData = array(
            'model'=>$model,
            'field_customer_id'=>'user_id',
            'url'=> $url,
            'name_relation_user'=>'rUser',
            'ClassAdd' => 'w-400',
            'field_autocomplete_name' => 'autocomplete_name',
        );
        $this->widget('ext.SpaAutocompleteCustomer.SpaAutocompleteCustomer',
            array('data'=>$aData));
        ?>
    </div>
    <div class="row buttons" style="padding-left: 159px;">
        <?php $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType'=>'submit',
            'label'=>'Search',
            'type'=>'null', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
            'size'=>'small', // null, 'large', 'small' or 'mini'
            //'htmlOptions' => array('style' => 'margin-bottom: 10px; float: right;'),
        )); ?>
    </div>
    <?php $this->endWidget(); ?>

</div><!-- search-form -->