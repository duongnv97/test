<?php
$this->breadcrumbs=array(
	$this->singleTitle,
);

$menus=array(
            array('label'=>"Create $this->singleTitle", 'url'=>array('create')),
);
$this->menu= ControllerActionsName::createMenusRoles($menus, $actions);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('reward-grid', {
                url : $(this).attr('action'),
		data: $(this).serialize()
	});
	return false;
});
");

Yii::app()->clientScript->registerScript('ajaxupdate', "
$('#reward-grid a.ajaxupdate').live('click', function() {
    $.fn.yiiGridView.update('reward-grid', {
        type: 'POST',
        url: $(this).attr('href'),
        success: function() {
            $.fn.yiiGridView.update('reward-grid');
        }
    });
    return false;
});
");
?>

<h1>Danh Sách <?php echo $this->pluralTitle; ?></h1>


<?php echo CHtml::link('Tìm Kiếm Nâng Cao','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'reward-grid',
	'dataProvider'=>$model->search(),
        'afterAjaxUpdate'=>'function(id, data){ fnUpdateColorbox();}',
        'template'=>'{pager}{summary}{items}{pager}{summary}',
//    'itemsCssClass' => 'items custom_here',
//    'htmlOptions'=> array('class'=>'grid-view custom_here'),
        'pager' => array(
            'maxButtonCount'=>  CmsFormatter::$PAGE_MAX_BUTTON,
        ),
	'enableSorting' => false,
	//'filter'=>$model,
	'columns'=>array(
        array(
            'header' => 'S/N',
            'type' => 'raw',
            'value' => '$this->grid->dataProvider->pagination->currentPage * $this->grid->dataProvider->pagination->pageSize + ($row+1)',
            'headerHtmlOptions' => array('width' => '10px','style' => 'text-align:center;'),
            'htmlOptions' => array('style' => 'text-align:center;')
        ),
            array(
                    'name' => 'title',
                    'type' => 'html',
                    'value' => '$data->getTitle()',
                    ),
            array(
                    'name' => 'type',
                    'type' => 'html',
                    'value' => '$data->getType()',
                    ),
            array(
                    'name' => 'point_apply',
                    'type' => 'html',
                    'value' => '$data->getPointApply()',
                    'htmlOptions' => array('style' => 'text-align:center;')
                    ),
            array(
                    'name' => 'image',
                    'type' => 'raw',
                    'value' => '$data->getFileOnly(\'rFileImage\')',
                    ),
            array(
                    'name' => 'banner',
                    'type' => 'html',
                    'value' => '$data->getFileOnly(\'rFileBanner\')',
                    ),
            array(
                    'name' => 'partner_logo',
                    'type' => 'html',
                    'value' => '$data->getFileOnly(\'rFileLogo\')',
                    ),
            array(
                    'name' => 'partner_name',
                    'type' => 'html',
                    'value' => '$data->getPartnerName()',
                    ),
            array(
                    'name' => 'partner_address',
                    'type' => 'html',
                    'value' => '$data->getPartnerAddress()',
                    ),
            array(
                    'name' => 'description',
                    'type' => 'html',
                    'value' => '$data->getDescription()',
                    ),
            array(
                    'name'=> 'date_from',
                    'type' => 'html',
                    'value' => 'MyFormat::dateConverYmdToDmy($data->date_from)',
                    'htmlOptions' => array('style' => 'text-align:center;'),
                    ),
            array(
                'name' => 'date_to',
                'type' => 'html',
                'value' => 'MyFormat::dateConverYmdToDmy($data->date_to)',
                'htmlOptions' => array('style' => 'text-align:center;'),
                ),  
            array(
                   'name' => 'created_date',
                   'type' => 'Datetime',
                   'htmlOptions' => array('style' => 'text-align:center;'),
                ),
		/*
		'reward_category_id',
		'type',
		'material_id',
		'description',
		'date_from',
		'date_to',
		'point_apply',
		'created_date',
		'created_by',
		*/
		array(
                    'header' => 'Actions',
                    'class'=>'CButtonColumn',
                    'template'=> ControllerActionsName::createIndexButtonRoles($actions),
                    'buttons'=>array(
                        'update' => array(
                            'visible' => '$data->canUpdate()',
                        ),
                        'delete'=>array(
                            'visible'=> 'GasCheck::canDeleteData($data)',
                        ),
                    ),
		),
	),
)); ?>

<script>
$(document).ready(function() {
    fnUpdateColorbox();
});

function fnUpdateColorbox(){   
    fixTargetBlank();
    $(".gallery").colorbox({iframe:true,innerHeight:'1800', innerWidth: '1100',close: "<span title='close'>close</span>"});
}
</script>