<?php
$this->breadcrumbs=array(
    $this->pluralTitle => array('index'),
    ' ' . $this->singleTitle . ' : ',
);

$menus = array(
	array('label'=>"$this->pluralTitle Management", 'url'=>array('index')),
	array('label'=>"Tạo Mới $this->singleTitle", 'url'=>array('create')),
	array('label'=>"Cập Nhật $this->singleTitle", 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>"Xóa $this->singleTitle", 'url'=>array('delete'), 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Bạn chắc chắn muốn xóa ?')),
);
$this->menu= ControllerActionsName::createMenusRoles($menus, $actions);
?>

<h1>Xem: <?php echo $this->singleTitle; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
                array(
                    'name'=>'title',
                    'type'=>'raw',
                    'value'=>$model->getTitle(),
                ),
                array(
                    'name'=>'type',
                    'type'=>'raw',  
                    'value'=>$model->getType(),
                ),
                array(
                    'name'=>'point_apply',
                    'type'=>'raw',  
                    'value'=>$model->getPointApply(),
                ),
                array(
                    'name'=>'image',
                    'type'=>'raw',
                    'value'=>$model->getFileOnly('rFileImage'),
                ),
                array(
                    'name'=>'banner',
                    'type'=>'raw',
                    'value'=>$model->getFileOnly('rFileBanner'),
                ),
                array(
                    'name'=>'partner_name',
                    'type'=>'raw',  
                    'value'=>$model->getPartnerName(),
                ),
                array(
                    'name'=>'partner_logo',
                    'type'=>'raw',  
                    'value'=>$model->getFileOnly('rFileLogo'),
                ),
                array(
                    'name'=>'partner_address',
                    'type'=>'raw',  
                    'value'=>$model->getPartnerAddress(),
                ),
                array(
                    'name'=>'description',
                    'type'=>'raw',  
                    'value'=>$model->getDescription(),
                ),
                array(
                    'name'=>'date_from',
                    'type'=>'raw',  
                    'value'=>MyFormat::dateConverYmdToDmy($model->date_from),
                ),
                array(
                    'name'=>'date_to',
                    'type'=>'raw',  
                    'value'=>MyFormat::dateConverYmdToDmy($model->date_to),
                ),
               
               
               array(
                    'name'=>'created_date',
                    'type'=>'DateTime',
                    ),
	),
)); ?>
