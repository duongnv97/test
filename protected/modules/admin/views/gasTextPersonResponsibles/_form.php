<div class="form">

    <?php
    $form = $this->beginWidget('CActiveForm', array(
        'id' => 'gas-text-person-responsibles-form',
        'enableAjaxValidation' => false,
        'htmlOptions' => array('enctype' => 'multipart/form-data'),
    ));
    ?>

    <p class="note">Dòng có dấu <span class="required">*</span> là bắt buộc.</p>
    <?php echo MyFormat::BindNotifyMsg(); ?>

    <div class="row">
        <?php echo $form->hiddenField($model, 'text_id',
                array(
                    'class' => 'uid_auto_hidden text_id_leave_hidden',
                    'class_update_val'  => 'text_id_leave_hidden',
                )); ?>
        <?php echo $form->labelEx($model, 'text_id'); ?>
        <?php
        // Widget auto complete search user
//        print_r($model->autocomplete_gas_text);
        $aData = array(
            'model'                 => $model,
            'field_customer_id'     => 'text_id',
            'url'                   => Yii::app()->createAbsoluteUrl('admin/ajax/searchText'),
            'name_relation_user'    => 'rGasText',
            'ShowTableInfo'         => 0,
            'placeholder'           => 'Nhập mã để tìm kiếm Văn bản',
            'field_autocomplete_name'   => 'autocomplete_gas_text',
        );
        $this->widget('ext.SpaAutocompleteCustomer.SpaAutocompleteCustomer',
                array('data'    => $aData)
                );
        ?>
        <?php
        if (isset($model->rGasText)) {
            echo $model->getGasTextInfo();
        }
        ?>
        <?php echo $form->error($model, 'text_id'); ?>
    </div>
    <div class="row">
        <?php echo $form->hiddenField($model, 'user_id',
                array(
                    'class' => 'uid_auto_hidden uid_leave_hidden',
                    'class_update_val'  => 'uid_leave_hidden',
                )); ?>
        <?php echo $form->labelEx($model, 'user_id'); ?>
        <?php
        // Widget auto complete search user
        $aData = array(
            'model'             => $model,
            'field_customer_id' => 'user_id',
            'url'               => Yii::app()->createAbsoluteUrl('admin/ajax/search_for_user_login'),
            'name_relation_user'        => 'rUser',
            'field_autocomplete_name'   => 'autocomplete_user',
        );
        $this->widget('ext.SpaAutocompleteCustomer.SpaAutocompleteCustomer',
                array('data'    => $aData)
                );
//        ?>
        <?php echo $form->error($model, 'user_id'); ?>
    </div>
    <div class="row">
        <?php echo $form->labelEx($model, 'amount'); ?>
        <?php echo $form->textField($model, 'amount',
                array(
                    "class" =>  "number_only ad_fix_currency",
                    'size'  => 20,
                    'maxlength' => 12)); ?>
        <?php echo $form->error($model, 'amount'); ?>
    </div>
    <div class="row">
        <?php echo $form->labelEx($model, 'status'); ?>
        <?php echo $form->dropDownList($model, 'status', GasTextPersonResponsibles::getStatus()); ?>
        <?php echo $form->error($model, 'status'); ?>
    </div>
    <div class="row buttons" style="padding-left: 141px;">
        <?php
        $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType' => 'submit',
            'label' => $model->isNewRecord ? 'Create' : 'Save',
            'type' => 'null', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
            'size' => 'small', // null, 'large', 'small' or 'mini'
                //'htmlOptions' => array('style' => 'margin-bottom: 10px; float: right;'),
        ));
        ?>	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/autoNumeric-master/autoNumeric.js"></script>
<script>
    $(document).ready(function () {
        $('.form').find('button:submit').click(function () {
            $.blockUI({overlayCSS: {backgroundColor: '#fff'}});
        });
        
        fnInitInputCurrency();
    });
</script>