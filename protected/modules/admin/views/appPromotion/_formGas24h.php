<div class="BoxGas24h <?php echo $model->is_news ? '' : 'display_none';?>">
    <div class="row ">
        <label>&nbsp;</label>
        <?php echo $form->checkBox($model,'is_home_popup', ['class'=>'float_l']); ?>
        <?php echo $form->labelEx($model,'is_home_popup',array('class'=>'checkbox_one_label' , 'style'=>'padding-top:3px;')); ?>
    </div>
    <div class="clr"></div>
    <div class="row">
        <?php echo $form->labelEx($model,'news_title'); ?>
        <?php echo $form->textField($model,'news_title',array('class'=>'w-800')); ?>
        <?php echo $form->error($model,'news_title'); ?>
    </div>
    <div class="row">
        <?php echo $form->labelEx($model,'link_web'); ?>
        <?php echo $form->textField($model,'link_web',array('class'=>'w-800')); ?>
        <?php echo $form->error($model,'link_web'); ?>
    </div>
    <div class="row">
        <?php echo $form->labelEx($model,'url_banner'); ?>
        <?php echo $form->textField($model,'url_banner',array('class'=>'w-800')); ?>
        <?php echo $form->error($model,'url_banner'); ?>
    </div>
    <div class="row">
        <?php echo $form->labelEx($model,'url_banner_popup'); ?>
        <?php echo $form->textField($model,'url_banner_popup',array('class'=>'w-800')); ?>
        <?php echo $form->error($model,'url_banner_popup'); ?>
    </div>
    <div class="row">
        <?php echo $form->labelEx($model,'news_content'); ?>
        <div class="float_l">
            <?php echo $form->textArea($model,'news_content',array('class'=>'ckeditor')); ?>
            <?php echo $form->error($model,'news_content'); ?>
        </div>
    </div>
    <div class="clr"></div>
    
</div>

<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/gasckeditor/ckeditor.js"></script>
<!--<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/gasckeditor/samples/js/sample.js"></script>-->
<!--<link href="<?php echo Yii::app()->theme->baseUrl; ?>/gasckeditor/samples/css/samples.css" rel="stylesheet" type="text/css" media="screen" />-->
<!--<link href="<?php echo Yii::app()->theme->baseUrl; ?>/gasckeditor/samples/toolbarconfigurator/lib/codemirror/neo.css" rel="stylesheet" type="text/css" media="screen" />-->
<script>
    // http://stackoverflow.com/questions/15659390/ckeditor-automatically-strips-classes-from-div
    $(function(){
        $(".ckeditor").each(function(){
           var id = $(this).attr('id');
            CKEDITOR.replace( id, {
//                removePlugins: 'about, iframe, flash',
                removePlugins: 'about, flash',
                height: 300,
                allowedContent: true,// Aug 13, 2016 chắc chắn phải open dòng này, không thì sẽ bị lỗi khi có tab và accordiontab // http://docs.ckeditor.com/#!/guide/dev_allowed_content_rules  == http://ckeditor.com/forums/CKEditor-3.x/ckeditor-remove-tag-attributes
                extraAllowedContent : '*(*)'
            });
        });
        
    });
</script>