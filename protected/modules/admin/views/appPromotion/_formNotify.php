<div class="row">
    <label>&nbsp;</label>
    <?php echo $form->checkBox($model,'makeNotify', ['class'=>'float_l NotifyGas24h']); ?>
    <?php echo $form->labelEx($model,'makeNotify',array('class'=>'checkbox_one_label' , 'style'=>'padding-top:3px; width:auto;')); ?>
</div>
<div class="clr"></div>

<div class="row">
    <?php echo $form->labelEx($model,'time_send_notify'); ?>
    <?php Yii::import('application.extensions.CJuiDateTimePicker.CJuiDateTimePicker');
    $this->widget('CJuiDateTimePicker',array(
        'model'=>$model, //Model object
        'attribute'=>'time_send_notify', //attribute name
        'mode'=>'datetime', //use "time","date" or "datetime" (default)
        'language'=>'en-GB',
        'options'=>array(
            'minDate'=> '0',
            'showAnim'=>'fold',
            'showButtonPanel'=>true,
            'autoSize'=>true,
            'dateFormat'=>'dd/mm/yy',
            'timeFormat'=>'hh:mm:ss',
            'width'=>'120',
            'separator'=>' ',
            'showOn' => 'button',
            'buttonImage'=> Yii::app()->theme->baseUrl.'/admin/images/icon_calendar_r.gif',
            'buttonImageOnly'=> true,
            'changeMonth' => true,
            'changeYear' => true,
            //                'regional' => 'en-GB'
        ),
        'htmlOptions' => array(
            'style' => 'width:180px;',
            'readonly'=>'readonly',
        ),
    ));
    ?>
    <?php echo $form->error($model,'time_send_notify'); ?>
</div>