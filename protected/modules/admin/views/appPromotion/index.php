<?php
$this->breadcrumbs=array(
    $this->singleTitle,
);

$menus=array(
    array('label'=>"Create $this->singleTitle", 'url'=>array('create')),
);
$this->menu= ControllerActionsName::createMenusRoles($menus, $actions);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('app-promotion-grid', {
                url : $(this).attr('action'),
		data: $(this).serialize()
	});
	return false;
});
");

Yii::app()->clientScript->registerScript('ajaxupdate', "
$('#app-promotion-grid a.ajaxupdate').live('click', function() {
    $.fn.yiiGridView.update('app-promotion-grid', {
        type: 'POST',
        url: $(this).attr('href'),
        success: function() {
            $.fn.yiiGridView.update('app-promotion-grid');
        }
    });
    return false;
});
");
?>

<?php include "index_button.php"; ?>

<?php echo CHtml::link('Tìm Kiếm Nâng Cao','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'app-promotion-grid',
	'dataProvider'=>$model->search(),
        'afterAjaxUpdate'=>'function(id, data){ fnUpdateColorbox();}',
        'template'=>'{pager}{summary}{items}{pager}{summary}',
//    'itemsCssClass' => 'items custom_here',
//    'htmlOptions'=> array('class'=>'grid-view custom_here'),
        'pager' => array(
            'maxButtonCount'=>  CmsFormatter::$PAGE_MAX_BUTTON,
        ),
	'enableSorting' => false,
	//'filter'=>$model,
	'columns'=>array(
        array(
            'header' => 'S/N',
            'type' => 'raw',
            'value' => '$this->grid->dataProvider->pagination->currentPage * $this->grid->dataProvider->pagination->pageSize + ($row+1)',
            'headerHtmlOptions' => array('width' => '10px','style' => 'text-align:center;'),
            'htmlOptions' => array('style' => 'text-align:center;')
        ),
            array(
                'name'  => 'title',
                'type'  => 'raw',
                'value'  => '$data->getTitleWeb()',
            ),
		'code_no',
//		'type',
            array(
                'header'  => 'Role',
                'value'  => '$data->getOwnerRole()',
            ),
            array(
                'name'  => 'type',
                'value'  => '$data->getType()',
            ),
            array(
                'header'  => 'Lượt xem',
                'value'  => '$data->getUserViewOnApp()',
                'htmlOptions' => array('style' => 'text-align:center;'),
            ),
            array(
                'name'  => 'amount',
                'value'  => '$data->getAmount(true)',
                'htmlOptions' => array('style' => 'text-align:right;')
            ),
            array(
                'name'  => 'time_use',
                'htmlOptions' => array('style' => 'text-align:center;')
            ),
            array(
                'name'  => 'expiry_date',
                'value'  => '$data->getExpiryDate()',
                'htmlOptions' => array('style' => 'text-align:center;')
            ),
            array(
                'header'  => 'Tháng sử dụng',
                'value'  => '$data->getMonthUse()',
                'htmlOptions' => array('style' => 'text-align:center;')
            ),
            array(
                'name'  => 'note',
                'type'  => 'html',
                'value'  => '$data->getNote()',
            ),
            array(
                'name'  => 'is_news',
                'type'  => 'raw',
                'value'  => '$data->getWebIsNews()',
            ),
            array(
                'name'  => 'status',
                'value'  => '$data->getStatus()',
                'htmlOptions' => array('style' => 'text-align:center;')
            ),
            array(
                'name'  => 'created_date',
                'value'  => '$data->getCreatedDate()',
            ),
            array(
                'header' => 'Actions',
                'class'=>'CButtonColumn',
                'template'=> ControllerActionsName::createIndexButtonRoles($actions),
                'buttons'=>array(
                    'update' => array(
                        'visible' => '$data->canUpdate()',
                    ),
                    'delete'=>array(
                        'visible'=> '$data->canDelete()',
                    ),
                ),
            ),
	),
)); ?>

<script>
$(document).ready(function() {
    fnUpdateColorbox();
});

function fnUpdateColorbox(){   
    fixTargetBlank();
}
</script>