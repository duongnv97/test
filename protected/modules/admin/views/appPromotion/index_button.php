<div class="form">
    <?php
        $LinkNormal         = Yii::app()->createAbsoluteUrl('admin/appPromotion/index');
        $LinkRefCode        = Yii::app()->createAbsoluteUrl('admin/appPromotion/index', array('type'=> AppPromotion::TYPE_REFERENCE_USER));
        $LinkCodeEmployee   = Yii::app()->createAbsoluteUrl('admin/appPromotion/index', array('type'=> AppPromotion::TYPE_COMPANY_EMPLOYEE));
    ?>
    <h1>Danh sách 
        <a class='btn_cancel f_size_14 <?php echo (!isset($_GET['type'])) ? "active":"";?>' href="<?php echo $LinkNormal;?>">Mã khuyến mãi</a>
        <a class='btn_cancel f_size_14 <?php echo isset($_GET['type']) && $_GET['type']==AppPromotion::TYPE_REFERENCE_USER ? 'active':'';?>' href="<?php echo $LinkRefCode;?>">Mã code KH</a>
        <a class='btn_cancel f_size_14 <?php echo isset($_GET['type']) && $_GET['type']==AppPromotion::TYPE_COMPANY_EMPLOYEE ? 'active':'';?>' href="<?php echo $LinkCodeEmployee;?>">Mã code Nhân Viên</a>
    </h1> 
</div>