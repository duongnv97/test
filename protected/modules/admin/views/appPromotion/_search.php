<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=> GasCheck::getCurl(),
	'method'=>'get',
)); ?>
        <div class="row more_col">
            <div class="col1">
                <?php echo $form->label($model,'code_no',array()); ?>
                <?php echo $form->textField($model,'code_no',array('class' => 'w-200')); ?>
            </div>
            <div class="col2">
                <?php echo $form->labelEx($model,'type'); ?>
                <?php echo $form->dropDownList($model,'type', $model->getArrayType(), array('class'=>'w-200', 'empty'=>'Select')); ?>
                <?php echo $form->error($model,'type'); ?>
            </div>
            <div class="col3">
                <?php echo $form->label($model,'time_use',array()); ?>
                <?php echo $form->dropDownList($model,'time_use', MyFormat::BuildNumberOrder(100),array('class'=>'w-200 ', 'empty'=>'Select')); ?>
            </div>
        </div>
        <div class="row more_col">
            <div class="col1">
                <?php echo $form->labelEx($model,'status'); ?>
                <?php echo $form->dropDownList($model,'status', ActiveRecord::getUserStatus(),array('class'=>'w-200 ', 'empty'=>'Select')); ?>
                <?php echo $form->error($model,'status'); ?>
            </div>
            <div class="col2">
            <?php $mProfile = new UsersProfile(); ?>
                <?php echo $form->labelEx($model,'owner_role_id'); ?>
                <?php echo $form->dropDownList($model,'owner_role_id', $mProfile->getArrayRole(),array('class'=>'w-200','empty'=>'Select')); ?>
            </div>
        </div>
        <div class="row">
            <?php echo $form->labelEx($model,'owner_id'); ?>
            <?php echo $form->hiddenField($model,'owner_id'); ?>
            <?php 
                // widget auto complete search user customer and supplier
                $aData = array(
                    'model'=>$model,
                    'field_customer_id'=>'owner_id',
                    'name_relation_user'=>'rOwner',
                    'url' => Yii::app()->createAbsoluteUrl('admin/ajax/searchHgd', ['customer_type'=> UsersExtend::STORE_CARD_HGD_APP]),
                    'ClassAdd' => 'w-400',
                );
                $this->widget('ext.SpaAutocompleteCustomer.SpaAutocompleteCustomer',
                    array('data'=>$aData));                                        
            ?>
            <?php echo $form->error($model,'owner_id'); ?>
        </div>
    
        <div class="row">
            <?php echo $form->labelEx($model,'owner_id_root', ['label'=>'Nhân viên cty']); ?>
            <?php echo $form->hiddenField($model,'owner_id_root'); ?>
            <?php 
                // widget auto complete search user customer and supplier
                $aData = array(
                    'model'=>$model,
                    'field_customer_id'=>'owner_id_root',
                    'name_relation_user'=>'rOwner',
                    'field_autocomplete_name' => 'time_send_notify',
                    'url' => Yii::app()->createAbsoluteUrl('admin/ajax/search_user_login', []),
                    'ClassAdd' => 'w-400',
                );
                $this->widget('ext.SpaAutocompleteCustomer.SpaAutocompleteCustomer',
                    array('data'=>$aData));                                        
            ?>
            <?php echo $form->error($model,'owner_id_root'); ?>
        </div>

	<div class="row buttons" style="padding-left: 159px;">
            <?php $this->widget('bootstrap.widgets.TbButton', array(
                'buttonType'=>'submit',
                'label'=>'Search',
                'type'=>'null', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
                'size'=>'small', // null, 'large', 'small' or 'mini'
                //'htmlOptions' => array('style' => 'margin-bottom: 10px; float: right;'),
            )); ?>
        </div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->