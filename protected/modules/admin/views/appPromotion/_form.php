<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'app-promotion-form',
	'enableAjaxValidation'=>false,
        'htmlOptions' => array('enctype' => 'multipart/form-data'),
)); ?>

    <p class="note">Dòng có dấu <span class="required">*</span> là bắt buộc.</p>
    <?php echo MyFormat::BindNotifyMsg(); ?>
    <div class="row buttons" style="padding-left: 141px;">
        <?php $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType'=>'submit',
            'label'=>$model->isNewRecord ? 'Create' :'Save',
            'type'=>'null', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
            'size'=>'small', // null, 'large', 'small' or 'mini'
            //'htmlOptions' => array('style' => 'margin-bottom: 10px; float: right;'),
        )); ?>	
    </div>
    <div class="row">
        <?php echo $form->labelEx($model,'type'); ?>
        <?php echo $form->dropDownList($model,'type', $model->getArrayType(), array('class'=>'w-400 ')); ?>
        <?php echo $form->error($model,'type'); ?>
    </div>
    <div class="row">
        <?php echo $form->labelEx($model,'title'); ?>
        <?php echo $form->textField($model,'title',array('class'=>'w-400')); ?>
        <?php echo $form->error($model,'title'); ?>
    </div>
    <div class="row">
        <?php echo $form->labelEx($model,'code_no'); ?>
        <?php echo $form->textField($model,'code_no',array('class'=>'w-400','maxlength'=>20)); ?>
        <?php echo $form->error($model,'code_no'); ?>
    </div>
    
    <div class="row">
        <?php echo $form->labelEx($model,'amount'); ?>
        <?php echo $form->textField($model,'amount', array('class'=>'w-150 number_only ad_fix_currency')); ?>
        <?php echo $form->error($model,'amount'); ?>
    </div>
    <div class="row">
        <?php echo $form->labelEx($model,'time_use'); ?>
        <?php echo $form->dropDownList($model,'time_use', MyFormat::BuildNumberOrder(100),array('class'=>'w-400 ')); ?>
        <?php echo $form->error($model,'time_use'); ?>
    </div>
    <div class="row">
        <?php echo $form->labelEx($model,'expiry_date'); ?>
        <?php 
            $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                'model'=>$model,        
                'attribute'=>'expiry_date',
                'language'=>'en-GB',
                'options'=>array(
                    'showAnim'=>'fold',
                    'dateFormat'=> ActiveRecord::getDateFormatJquery(),
//                    'minDate'=> '0',
//                    'maxDate'=> '0',
                    'changeMonth' => true,
                    'changeYear' => true,
                    'showOn' => 'button',
                    'buttonImage'=> Yii::app()->theme->baseUrl.'/admin/images/icon_calendar_r.gif',
                    'buttonImageOnly'=> true,                                
                ),        
                'htmlOptions'=>array(
                    'class'=>'w-16',
                    'style'=>'height:20px;',
                    'readonly'=>'readonly',
                ),
            ));
        ?>     		
        <?php echo $form->error($model,'expiry_date'); ?>
    </div>
    <div class="row">
        <label>&nbsp;</label>
        <label class="color_red" style="width: auto;">Tháng sử dụng sẽ được ưu tiên hơn ngày hết hạn, khi chọn tháng sử dụng, hệ thống sẽ cộng từ ngày hiện tại với số tháng được chọn, bỏ qua ngày hết hạn</label>
    </div>
    <div class="clr"></div>
    <div class="row">
        <?php echo $form->labelEx($model,'plus_month_use'); ?>
        <?php echo $form->dropDownList($model,'plus_month_use', $model->getArrayMonth(24),array('class'=>'w-400 ', 'empty' =>'Chọn số tháng')); ?>
        <?php echo $form->error($model,'plus_month_use'); ?>
    </div>
    
    <div class="row">
        <?php echo $form->labelEx($model,'status'); ?>
        <?php echo $form->dropDownList($model,'status', ActiveRecord::getUserStatus(),array('class'=>'w-400 ')); ?>
        <?php echo $form->error($model,'status'); ?>
    </div>
    <div class="row">
        <?php echo $form->labelEx($model,'note'); ?>
        <?php echo $form->textArea($model,'note',array('rows'=>3, 'cols'=>100)); ?>
        <?php echo $form->error($model,'note'); ?>
    </div>
    <div class="row">
        <?php echo $form->labelEx($model,'ip_limit'); ?>
        <?php echo $form->textArea($model,'ip_limit',array('rows'=>10, 'cols'=>100)); ?>
        <?php echo $form->error($model,'ip_limit'); ?>
    </div>
    
    <div class="row">
        <?php echo $form->labelEx($model,'province_limit'); ?>
        <div class="fix-label">
            <?php
               $this->widget('ext.multiselect.JMultiSelect',array(
                     'model'=>$model,
                     'attribute'=>'province_limit',
                     'data'=> GasProvince::getArrAll(),
                     // additional javascript options for the MultiSelect plugin
                    'options'=>array('selectedList' => 30,),
                     // additional style
                     'htmlOptions'=>array('style' => 'width: 800px;'),
               ));    
           ?>
        </div>
    </div>
    <br>
    <?php include '_formNotify.php'; ?>
    <br>
    <div class="row">
        <label>&nbsp;</label>
        <?php echo $form->checkBox($model,'is_news', ['class'=>'float_l NewsGas24h']); ?>
        <?php echo $form->labelEx($model,'is_news',array('class'=>'checkbox_one_label' , 'style'=>'padding-top:3px;')); ?>
    </div>
    <div class="clr"></div>
    
    <?php include '_formGas24h.php'; ?>
    <br>
    <div class="row buttons" style="padding-left: 141px;">
        <?php $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType'=>'submit',
            'label'=>$model->isNewRecord ? 'Create' :'Save',
            'type'=>'null', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
            'size'=>'small', // null, 'large', 'small' or 'mini'
            //'htmlOptions' => array('style' => 'margin-bottom: 10px; float: right;'),
        )); ?>	
    </div>

<?php $this->endWidget(); ?>

</div><!-- form -->

<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/autoNumeric-master/autoNumeric.js"></script>
<script>
    $(document).ready(function(){
        $('.form').find('button:submit').click(function(){
            $.blockUI({ overlayCSS: { backgroundColor: '#fff' } }); 
        });
        fnInitInputCurrency();
        $('.NewsGas24h').click(function(){
            if($(this).is(':checked')){
                $('.BoxGas24h').show();
            }else{
                $('.BoxGas24h').hide();
            }
        });
    });
</script>