<?php
$this->breadcrumbs=array(
    'Galleries'=>array('galleries/index'),
    $gallery->title=>array('galleries/view','id'=>$gallery_id),
    'Photos'
);

$menus=array(
    //array('label'=>Yii::t('translation','Create Photos'), 'url'=>array('create','gallery_id'=>$gallery_id)),
);
$this->menu= ControllerActionsName::createMenusRoles($menus, $actions);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('photos-grid', {
                url : $(this).attr('action'),
		data: $(this).serialize()
	});
	return false;
});
");

Yii::app()->clientScript->registerScript('ajaxupdate', "
$('#photos-grid a.ajaxupdate').live('click', function() {
    $.fn.yiiGridView.update('photos-grid', {
        type: 'POST',
        url: $(this).attr('href'),
        success: function() {
            $.fn.yiiGridView.update('photos-grid');
        }
    });
    return false;
});
");
?>

<h1><?php echo Yii::t('translation', 'Photo Management'); ?></h1>

<?php echo CHtml::link(Yii::t('translation','Advanced Search'),'#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<div>

    <h2><?php echo Yii::t('ui','Multiple file upload');?></h2>


    <?php $form=$this->beginWidget('CActiveForm', array(
    'action'=>$this->createUrl('upload',array('gallery_id'=>$gallery_id)),
    'method'=>'post',
    'id'=>'upload-photos-form',
    'enableAjaxValidation'=>false,
    'htmlOptions' => array('class'=>'', 'enctype' => 'multipart/form-data'),
)); ?>

    <?php
    foreach(Yii::app()->user->getFlashes() as $key => $message) {
        echo '<div class="flash-' . $key . '">' . $message . "</div>\n";
    }
    ?>

    <?php $this->widget('CMultiFileUpload',array(
//   'model'=>$model,
    'name'=>'file_name',
    'accept'=>'jpg|png|gif',
    'max'=>5,
    'remove'=>Yii::t('ui','Remove'),
    'denied'=>'Only files with these extensions are allowed: jpg, gif, png.', //message that is displayed when a file type is not allowed
    //'duplicate'=>'', //message that is displayed when a file appears twice
    'htmlOptions'=>array('size'=>25),
)); ?>
    <br />
    <?php echo CHtml::submitButton(Yii::t('ui', 'Upload')); ?>&nbsp;
    <?php echo CHtml::endForm(); ?>
    <?php $this->endWidget(); ?>

</div>

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'photos-grid',
	'dataProvider'=>$model->search(),
	'enableSorting' => false,
	//'filter'=>$model,
	'columns'=>array(
        array(
            'header' => 'S/N',
            'type' => 'raw',
            'value' => '$this->grid->dataProvider->pagination->currentPage * $this->grid->dataProvider->pagination->pageSize + ($row+1)',
            'headerHtmlOptions' => array('width' => '30px','style' => 'text-align:center;'),
            'htmlOptions' => array('style' => 'text-align:center;')
        ),
        array(
            'name'=>'file_name',
            'type' => 'html',
            'htmlOptions' => array('style' => 'text-align:center;'),
            'value'=> '!empty($data->file_name) ? CHtml::image(Yii::app()->baseUrl."/upload/admin/galleries/thumbs/".$data->file_name, "gallery", array()):"";'
        ),
        array(
            'name' => 'description',
            'type' => 'html',
            'value' => '$data->description',
        ),
        array(
            'name'=>'created_date',
            'type'=>'datetime',
            'htmlOptions' => array('style' => 'text-align:center;'),
            'value'=>'$data->created_date',
        ),
        array(
            'header' => 'Action',
            'class'=>'CButtonColumn',
            'template'=> ControllerActionsName::createIndexButtonRoles($actions),
        ),
	),
)); ?>
