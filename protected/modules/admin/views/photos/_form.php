<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'photos-form',
	'enableAjaxValidation'=>false,
    'htmlOptions' => array('class'=>'', 'enctype' => 'multipart/form-data'),
)); ?>

	<?php echo Yii::t('translation', '<p class="note">Fields with <span class="required">*</span> are required.</p>'); ?>

    <div class="row">
        <label for="Photos_file_name">Photo <span class="required">*</span></label>
        <?php echo $form->fileField($model,'file_name',array('id'=>'file_name')); ?>
        <?php echo $form->error($model,'file_name'); ?>
    </div>
    <?php if(Yii::app()->controller->action->id == 'update'){?>
    <div class="row">
        <label for="Photos_file_name">&nbsp;</label>
        <div>
            <?php echo CHtml::image(Yii::app()->baseUrl."/upload/admin/galleries/thumbs/".$model->file_name, "photo", array());?>
        </div>
    </div>
    <?php } ?>
    <div class='clr'></div>

	<div class="row">
		<?php echo Yii::t('translation', $form->labelEx($model,'description')); ?>
        <div style="float:left;">
            <?php
            $this->widget('ext.ckeditor.CKEditorWidget',array(
                "model"=>$model,
                "attribute"=>'description',
                "config" => array(
                    "height"=>"150px",
                    "width"=>"500px",
                    "toolbar"=>Yii::app()->params['ckeditor_simple']
                )
            ));
            ?>
        </div>
		<?php echo $form->error($model,'description'); ?>
	</div>

    <div class='clr'></div>

	<div class="row buttons" style="padding-left: 115px;">
		        <?php $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType'=>'submit',
            'label'=>$model->isNewRecord ? Yii::t('translation', 'Create') : Yii::t('translation', 'Save'),
            'type'=>'null', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
            'size'=>'small', // null, 'large', 'small' or 'mini'
            //'htmlOptions' => array('style' => 'margin-bottom: 10px; float: right;'),
        )); ?>	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->