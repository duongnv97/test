<?php
$this->breadcrumbs=array(
    'Galleries'=>array('galleries/index'),
    $gallery->title=>array('galleries/view','id'=>$gallery->id),
    'Photos'=>array('photos/index','gallery_id'=>$gallery->id),
    'Create'
);

$menus = array(
    array('label'=>Yii::t('translation', 'Photos Management'), 'url'=>array('photos/index','gallery_id'=>$gallery->id)),
);
$this->menu= ControllerActionsName::createMenusRoles($menus, $actions);
?>

<h1><?php echo Yii::t('translation', 'Create Photos'); ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>