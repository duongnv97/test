<?php
$this->breadcrumbs=array(
    'Galleries'=>array('galleries/index'),
    $gallery->title=>array('galleries/view','id'=>$gallery->id),
    'Photos'=>array('photos/index','gallery_id'=>$gallery->id),
    'Update',
);

$menus = array(	
        array('label'=> Yii::t('translation', 'Photos Management'), 'url'=>array('index')),
	array('label'=> Yii::t('translation', 'View Photos'), 'url'=>array('view', 'id'=>$model->id)),
	array('label'=> Yii::t('translation', 'Create Photos'), 'url'=>array('create')),	
);
$this->menu= ControllerActionsName::createMenusRoles($menus, $actions);

?>

<h1><?php echo Yii::t('translation', 'Update Photos '.$model->id); ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>