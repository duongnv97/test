<?php
$this->breadcrumbs=array(
	'Photoses'=>array('index'),
	$model->id,
);

$menus = array(
	array('label'=>'Photos Management', 'url'=>array('index')),
	array('label'=>'Create Photos', 'url'=>array('create')),
	array('label'=>'Update Photos', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete Photos', 'url'=>array('delete'), 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
);
$this->menu= ControllerActionsName::createMenusRoles($menus, $actions);
?>

<h1>View Photos #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'gallery_id',
		'file_name',
		'description',
		'created_date',
	),
)); ?>
