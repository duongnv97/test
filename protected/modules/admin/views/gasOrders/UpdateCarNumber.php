<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'gas-orders-form',
	'enableAjaxValidation'=>false,
)); 
$cmsFormater = new CmsFormatter();
$aModelUser = $model->getModelCustomerAndSale();
$aCar       = Users::getSelectByRoleForAgent($model->user_id_executive, ONE_AGENT_CAR, '', array('status'=>1, 'GetNameOnly'=>1));
$aDriver    = Users::getSelectByRoleForAgent($model->user_id_executive, ONE_AGENT_DRIVER, '', array('status'=>1, 'GetNameOnly'=>1));
$aPhuXe     = Users::getSelectByRoleForAgent($model->user_id_executive, ONE_AGENT_PHU_XE, '', array('status'=>1, 'GetNameOnly'=>1));

$jsonPhuXe = $model->formatJsonUserAutocomplete($aPhuXe);

$mStoreCard = new GasStoreCard();
// Users::getArrDropdown(ROLE_DRIVER)

?>
	<p class="note">Cập Nhật Số Xe Cho Đặt Hàng: <?php echo $model->orders_no;?></p>
        <?php echo MyFormat::BindNotifyMsg(); ?>
        <div class="row">
            <?php echo $form->label($model,'user_id_create', ['label'=>'Đơn vị đề nghị']); ?>
            <?php echo $model->getAgentName(); ?>
	</div>
        <div class="row">
            <?php echo $form->label($model,'user_id_create'); ?>
            <?php echo $model->rUserIdCreate?$model->rUserIdCreate->first_name:""; ?>
	</div>
        <div class="row">
            <?php echo $form->label($model,'date_delivery'); ?>
            <?php echo $model->date_delivery; ?>
	</div>

	<div class="row">
            <?php echo $form->label($model,'user_id_executive'); ?>
            <?php echo  $model->rUserIdExecutive?$model->rUserIdExecutive->first_name:''; ?>
	</div>
        <div class="row display_none">
            <?php echo $form->label($model,'created_date'); ?>
            <?php echo $cmsFormater->formatDateTime($model->created_date); ?>
	</div>
        <p>
            <em class="hight_light item_b f_size_18" style="margin-left: 140px;">Chú ý: tất cả các đơn hàng bắt buộc phải nhập số xe và lái xe thì hệ thống mới lưu được.</em>
        </p>
        <?php $hideClass=''; ?>
        <?php if($model->type == GasOrders::TYPE_AGENT): ?>
        <div class="row">
            <?php echo $form->labelEx($model,'date_delivery'); ?>
                <?php 
                    $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                        'model'=>$model,        
                        'attribute'=>'date_delivery',
                        'options'=>array(
                            'showAnim'=>'fold',
                            'dateFormat'=> ActiveRecord::getDateFormatJquery(),
                            'minDate'=> "-".GasOrders::$days_allow_update,
                            'maxDate'=> '1',
                            'changeMonth' => true,
                            'changeYear' => true,
                            'showOn' => 'button',
                            'buttonImage'=> Yii::app()->theme->baseUrl.'/admin/images/icon_calendar_r.gif',
                            'buttonImageOnly'=> true,                                
                        ),        
                        'htmlOptions'=>array(
                            'class'=>'w-16',
                            'style'=>'height:30px;width:166px;',
                                'readonly'=>'readonly',
                        ),
                    ));
                ?>     	
            <?php echo $form->error($model,'date_delivery'); ?>            
        </div>
        <?php endif; ?>

        <div class="row buttons" style="padding-left: 141px;">
            <?php $this->widget('bootstrap.widgets.TbButton', array(
                'buttonType'=>'submit',
                'label'=>$model->isNewRecord ? 'Create' :'Save',
                'type'=>'null', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
                'size'=>'small', // null, 'large', 'small' or 'mini'
                //'htmlOptions' => array('style' => 'margin-bottom: 10px; float: right;'),
            )); ?>
            <input class='cancel_iframe' type='button' value='Cancel'>
        </div>
        
        <div class="row">            
            <table class="materials_table materials_table_root hm_table materials_table_th" style="">
                <thead>
                    <tr>
                        <th class="item_c w-10">#</th>
                        <th class="w-30 item_c">Số Xe</th>
                        <th class="item_c <?php echo $hideClass;?>">Khách Hàng</th>
                        <th rowspan="2">Địa Điểm Giao Hàng</th>
                        <th class="item_c">Nội Dung Công Việc</th>
                        <th class="w-30 item_c">LPG (Tấn)</th>
                        <th class="w-30 item_c">B50</th>
                        <th class="w-30 item_c">B45</th>
                        <th class="w-30 item_c">B12</th>
                        <th class="w-30 item_c">B6</th>
                        <th class="w-30 item_c">Vật Tư Khác</th>
                        <th class="w-30 item_c">Giao Thực Tế</th>
                        <th class=" item_c td_last_r">Ghi Chú</th>
                    </tr>
                </thead>
                <tbody>
                    <?php if(count($model->aModelOrderDetail)): ?>
                        <?php foreach($model->aModelOrderDetail as $key=>$item):?>
                            <?php 
                                $customer_name = '';
                                $customer_add = '';
                                $work_content = $item->work_content;
                                if($item->materials){
                                    $work_content = $item->materials->name;
                                }
                                if(isset($aModelUser[$item->customer_id])){
                                    $mCustomer = $aModelUser[$item->customer_id];
                                    $type_customer = $mCustomer->is_maintain;
                                    $customer_name = $mCustomer->first_name;
                                    $customer_add = $mCustomer->address;
                                }
                            ?>
                        <tr class="cash_book_row row_input">
                            <td class="order_no item_c w-10"><?php echo ($key+1);?></td>
                            <td class=" w-180">
                                <?php include 'UpdateCarDriver.php'; ?>
                            </td>
                            <td class="col_customer <?php echo $hideClass;?>">
                                &nbsp;<?php echo $customer_name;?>
                            </td>
                            <td class="item_l">&nbsp;<?php echo $customer_add;?></td>
                            <td class="item_l">&nbsp;<?php echo $work_content;?></td>
                            <td class="item_r">&nbsp;<?php echo $item->lpg_ton>0?ActiveRecord::formatCurrency($item->lpg_ton):'';?></td>
                            <td class="item_c">&nbsp;<?php echo $item->quantity_50?$item->quantity_50:'';?></td>
                            <td class="item_c">&nbsp;<?php echo $item->quantity_45?$item->quantity_45:'';?></td>
                            <td class="item_c">&nbsp;<?php echo $item->quantity_12?$item->quantity_12:'';?></td>
                            <td class="item_c">&nbsp;<?php echo $item->quantity_6?$item->quantity_6:'';?></td>
                            <td class="item_c">&nbsp;<?php echo $item->quantity_other_material>0?ActiveRecord::formatCurrency($item->quantity_other_material):'';?></td>
                            <td class="item_c">
                                <input name="GasOrdersDetail[real_output][]" class="number_only_v1 w-80" maxlength="12" value="<?php echo $item->real_output>0?ActiveRecord::formatNumberInput($item->real_output):'';?>" type="text" placeholder="SL thực tế">
                            </td>
                            <td class=" td_last_r"><?php echo $item->note;?><br>
                                <textarea name="GasOrdersDetail[note_2][]" class="w-120" maxlength="300"><?php echo $item->note_2;?></textarea>
                                <input name="GasOrdersDetail[id][]" class=" w-80" value="<?php echo $item->id;?>" type="hidden">
                            </td>
                        </tr>                    
                        <?php endforeach;?>
                    <?php endif;?>
                    
                </tbody>
            </table>
        </div>            
            
	<div class="row">
            <?php echo Yii::t('translation', $form->labelEx($model,'note')); ?>
            <div style="padding-left:141px;">
                   <?php echo $model->note; ?>
            </div>
	</div>

	<div class="row buttons" style="padding-left: 141px;">
            <?php $this->widget('bootstrap.widgets.TbButton', array(
                'buttonType'=>'submit',
                'label'=>$model->isNewRecord ? 'Create' :'Save',
                'type'=>'null', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
                'size'=>'small', // null, 'large', 'small' or 'mini'
                //'htmlOptions' => array('style' => 'margin-bottom: 10px; float: right;'),
            )); ?>
            <input class='cancel_iframe' type='button' value='Cancel'>
        </div>

<?php $this->endWidget(); ?>

</div><!-- form -->
<?php Yii::app()->clientScript->registerCoreScript('jquery.ui'); ?>
<link href="<?php echo Yii::app()->theme->baseUrl; ?>/admin/css/jquery-ui-1.8.18.custom.css" type=text/css rel=stylesheet>

<!--<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/jquery.floatThead.min.js"></script>    
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/underscore-min.js"></script>    -->
<link href="<?php echo Yii::app()->theme->baseUrl; ?>/css/style_sell.css" rel="stylesheet" type="text/css"/>
<script>
    $(document).ready(function(){
        $('.form').find('button:submit').click(function(){
            $.blockUI({ overlayCSS: { backgroundColor: '#fff' } }); 
        });
        
        parent.$.fn.yiiGridView.update("gas-orders-grid"); 
        parent.$.fn.yiiGridView.update("gas-orders-grid-update_again_car"); 
        validateNumber();
        <?php if($model->type == GasOrders::TYPE_AGENT): ?>
            changeDriver();
        <?php endif;?>
        fnBindAllAutocomplete();
    });
    
    $(window).load(function(){ // không dùng dc cho popup
//        $('.materials_table').floatThead();
        fnResizeColorbox();
    });
    
    function fnResizeColorbox(){
//        var y = $('body').height()+100;
        var y = $('#main_box').height()+100;
        parent.$.colorbox.resize({innerHeight:y});
    }
      
    function changeDriver(){
        $('.ChangeCar:first').change(function(){
            $('.ChangeCar').val($(this).val());
        });
        $('.ChangeDriver:first').change(function(){
            $('.ChangeDriver').val($(this).val());
        });
    }
    
    function fnBindAllAutocomplete(){
        // material
        $('.OrderDriverName').each(function(){
            bindUserAutocomplete($(this));
        });
    }
    
    // to do bind autocompelte for input material
    // @param objInput : is obj input ex  $('.customer_autocomplete')    
    function bindUserAutocomplete(objInput){
        var availableMaterials = <?php echo $jsonPhuXe; ?>;
        var parent_div = objInput.closest('.WrapDriverSearch');
        objInput.autocomplete({
            source: availableMaterials,
//            close: function( event, ui ) { $( "#GasStoreCard_materials_name" ).val(''); },
            select: function( event, ui ) {
                setValueSelectUser(parent_div, objInput, ui.item.id, true);
            }
        });
    }
    
    function setValueSelectUser(parent_div, objInput, user_id, readonly){
        parent_div.find('.UserId').val(user_id);
        parent_div.find('.OrderDriverName').attr('readonly', readonly);
    }
    
    function fnRemoveNameAgent(this_){
        objInput = $(this_);
        var parent_div = objInput.closest('.WrapDriverSearch');
        setValueSelectUser(parent_div, objInput, 0, false);
        parent_div.find('.OrderDriverName').val('');
    }
    
</script>