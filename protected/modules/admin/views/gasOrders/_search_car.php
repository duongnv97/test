<?php 
    $showField = true;
    $cAction = strtolower(Yii::app()->controller->action->id);
    if($cAction == 'view'){
        $showField = false;
    }
    
?>

<div class="wide form">
<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=> "http://".$_SERVER['HTTP_HOST'].Yii::app()->request->requestUri,
	'method'=>'post',
));
?>
    <?php if($showField): ?>
        <div class="row">
            <?php echo $form->labelEx($model,'user_id_executive'); ?>
            <?php echo $form->dropDownList($model,'user_id_executive', Users::getSelectByRoleNotRoleAgent(ROLE_SCHEDULE_CAR),array('class'=>'')); ?>
            <?php echo $form->error($model,'user_id_executive'); ?>
        </div>
    <?php endif; ?>

    <div class="row">
        <?php echo $form->label($model,'car_number',array('label'=>'Chọn Số Xe')); ?>
        <?php echo $form->dropDownList($model,'car_number', $aCar,array('style'=>'','empty'=>'Select')); ?>
    </div>
    <div class="row">
        <?php echo $form->label($model,'driver_id', []); ?>
        <?php echo $form->dropDownList($model,'driver_id', $aDriver,array('style'=>'','empty'=>'Select')); ?>
    </div>
    
    <?php if($showField): ?>
    <div class="row">
        <?php echo $form->label($model,'reason_wrong', array('label'=>'Loại')); ?>
        <?php echo $form->dropDownList($model,'reason_wrong', GasOrdersDetail::model()->getArrayReasonWrong(),array('style'=>'','empty'=>'Select')); ?>
    </div>
    <?php endif; ?>
    
    <?php if($showField): ?>
    <div class="row">
        <?php echo $form->labelEx($model,'date_delivery'); ?>
            <?php 
                $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                    'model'=>$model,        
                    'attribute'=>'date_delivery',
                    'options'=>array(
                        'showAnim'=>'fold',
                        'dateFormat'=> "dd-mm-yy",
                        'changeMonth' => true,
                        'changeYear' => true,
                        'showOn' => 'button',
                        'buttonImage'=> Yii::app()->theme->baseUrl.'/admin/images/icon_calendar_r.gif',
                        'buttonImageOnly'=> true,                                
                    ),        
                    'htmlOptions'=>array(
                        'class'=>'w-16',
                        'style'=>'height:20px;width:166px;',
                            'readonly'=>'readonly',
                    ),
                ));
            ?>     	
        <?php echo $form->error($model,'date_delivery'); ?>            
    </div>
    <?php endif; ?>

    <div class="row buttons" style="padding-left: 159px;">
        <?php $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType'=>'submit',
            'label'=>'Search',
            'type'=>'null', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
            'size'=>'small', // null, 'large', 'small' or 'mini'
            //'htmlOptions' => array('style' => 'margin-bottom: 10px; float: right;'),
        )); ?>	
    </div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->