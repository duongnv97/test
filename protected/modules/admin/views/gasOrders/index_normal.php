<div class="">

<?php echo CHtml::link('Tìm Kiếm Nâng Cao','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php // if(Yii::app()->user->role_id==ROLE_SCHEDULE_CAR || Yii::app()->user->role_id==ROLE_ADMIN):?>
<?php if(1):?>
<h1>Danh Sách Đặt Hàng Có Thêm Mới Khách Hàng</h1>
<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'gas-orders-grid-update_again_car',
	'dataProvider'=>$model->search_update_again_car(),
        'afterAjaxUpdate'=>'function(id, data){fnUpdateColorbox();}',
        'template'=>'{pager}{summary}{items}{pager}{summary}', 
        'pager' => array(
            'maxButtonCount'=>  CmsFormatter::$PAGE_MAX_BUTTON,
        ),            
	//'filter'=>$model,
	'columns'=>array(
        array(
            'header' => 'S/N',
            'type' => 'raw',
            'value' => '$this->grid->dataProvider->pagination->currentPage * $this->grid->dataProvider->pagination->pageSize + ($row+1)',
            'headerHtmlOptions' => array('width' => '10px','style' => 'text-align:center;'),
            'htmlOptions' => array('style' => 'text-align:center;')
        ),
        array(
            'name' => 'orders_no',
            'type' => 'GasOrderCar',
            'value' => '$data',
        ),                        
        array(
            'header' => 'Đơn vị',
            'value' => '$data->getAgentName()',
        ),
        array(
            'name' => 'user_id_executive',
            'value' => '$data->rUserIdExecutive?$data->rUserIdExecutive->first_name:""',
        ),
        array(                
            'name' => 'date_delivery',
            'type'=>'date',
            'htmlOptions' => array('style' => 'text-align:center;')
        ),
        array(
            'name' => 'created_date',
            'type'=>'datetime',
            'htmlOptions' => array('style' => 'text-align:center;')
        ),
        array(
            'header' => 'Người tạo',
            'value' => '$data->getUserCreated()',
        ),
        array(
            'name' => 'last_update_by',
            'value' => '$data->rUserLastUpdateBy?$data->rUserLastUpdateBy->first_name:""',
            'htmlOptions' => array('class'=>'w-120')
        ),
        array(
            'name' => 'last_update_time',
            'type'=>'datetime',
            'htmlOptions' => array('class'=>'w-50','style' => 'text-align:center;')
        ),

        array(
            'header' => 'Actions',
            'class'=>'CButtonColumn',
            'template'=> ControllerActionsName::createIndexButtonRoles($actions, array('UpdateCarNumber','view')),
            'buttons'=>array(
                'UpdateCarNumber'=>array(
                        'label'=>'Cập nhật số xe cho đặt hàng',
                        'imageUrl'=>Yii::app()->theme->baseUrl . '/admin/images/edit.png',
                        'options'=>array('class'=>'UpdateCarNumber'),
                        'url'=>'Yii::app()->createAbsoluteUrl("admin/gasOrders/updateCarNumber",
                            array("id"=>$data->id,
                                    "update_again_car"=>1) )',
//                                 'visible'=>'( Yii::app()->user->role_id == ROLE_ADMIN ||  (int)$data->update_num < (int)Yii::app()->params["limit_update_maintain"] ||  $data->status==STATUS_NOT_YET_CALL )', 

                    ),
//                            'update'=>array(
//                                'visible'=>'( Yii::app()->user->role_id == ROLE_ADMIN || (int)$data->update_num < (int)Yii::app()->params["limit_update_maintain"] )',
//                            ),                                
            ),
        ),
	),
)); ?>

<?php // endif;?>
<?php // if(Yii::app()->user->role_id==ROLE_SCHEDULE_CAR || Yii::app()->user->role_id==ROLE_ADMIN):?>
<h1>Danh Sách Đặt Hàng Bình Thường</h1>
<?php endif;?>

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'gas-orders-grid',
	'dataProvider'=>$model->search(),
        'afterAjaxUpdate'=>'function(id, data){fnUpdateColorbox();}',
        'template'=>'{pager}{summary}{items}{pager}{summary}', 
        'pager' => array(
            'maxButtonCount'=>  CmsFormatter::$PAGE_MAX_BUTTON,
        ),            
	//'filter'=>$model,
	'columns'=>array(
        array(
            'header' => 'S/N',
            'type' => 'raw',
            'value' => '$this->grid->dataProvider->pagination->currentPage * $this->grid->dataProvider->pagination->pageSize + ($row+1)',
            'headerHtmlOptions' => array('width' => '10px','style' => 'text-align:center;'),
            'htmlOptions' => array('style' => 'text-align:center;')
        ),
            array(
                'name' => 'orders_no',
                'type' => 'GasOrderCar',
                'value' => '$data',
            ),
            array(
                'header' => 'Đơn vị',
                'value' => '$data->getAgentName()',
            ),
            array(
                'name' => 'user_id_executive',
                'value' => '$data->rUserIdExecutive?$data->rUserIdExecutive->first_name:""',
            ),                 
            array(                
                'name' => 'date_delivery',
                'type'=>'date',
                'htmlOptions' => array('style' => 'text-align:center;')
            ),
            array(                
                'name' => 'created_date',
                'type'=>'datetime',
                'htmlOptions' => array('style' => 'text-align:center;')
            ),
            array(
                'header' => 'Người tạo',
                'value' => '$data->getUserCreated()',
            ),
            array(
                'name' => 'last_update_by',
                'value' => '$data->rUserLastUpdateBy?$data->rUserLastUpdateBy->first_name:""',
                'htmlOptions' => array('class'=>'w-120')
            ),
            array(
                'name' => 'last_update_time',
                'type'=>'datetime',
                'htmlOptions' => array('class'=>'w-50','style' => 'text-align:center;')
            ),
            array(
                'header' => 'Chi Tiết',
                'type'=>'OrderViewCreateBy',
                'value'=>'$data',
                'htmlOptions' => array('class'=>'w-50','style' => 'text-align:center;')
            ),
            array(
                'header' => 'Actions',
                'class'=>'CButtonColumn',
                'template'=> ControllerActionsName::createIndexButtonRoles($actions, array('UpdateCarNumber','view','delete')),
                'buttons'=>array(
                    'UpdateCarNumber'=>array(
                            'label'=>'Cập nhật số xe cho đặt hàng',
                            'imageUrl'=>Yii::app()->theme->baseUrl . '/admin/images/edit.png',
                            'options'=>array('class'=>'UpdateCarNumber'),
                            'url'=>'Yii::app()->createAbsoluteUrl("admin/gasOrders/updateCarNumber",
                                array("id"=>$data->id,
                                        "order_id"=>$data->id) )',
                             'visible'=>'GasCheck::allowUpdateCarNumber($data)', 

                        ),
                        'delete'=>array(
                            'visible'=> 'GasCheck::canDeleteData($data)',
                        )
//                            'update'=>array(
//                                'visible'=>'( Yii::app()->user->role_id == ROLE_ADMIN || (int)$data->update_num < (int)Yii::app()->params["limit_update_maintain"] )',
//                            ),    
                ),
            ),
	),
)); ?>

<script>
$(document).ready(function() {
    fnUpdateColorbox();
});

//http://www.jacklmoore.com/colorbox/
function fnUpdateColorbox(){
    fnShowhighLightTr();
    $(".UpdateCarNumber").colorbox({iframe:true,
        innerHeight:'3500', 
//        onComplete :function(){ $.colorbox.resize(); },
        innerWidth: '1200', overlayClose :false, escKey:false,close: "<span title='close'>close</span>"});
//    $(".UpdateCarNumber").colorbox({iframe:true,innerHeight:'550', innerWidth: '950', escKey:false,close: "<span title='close'>close</span>"});
    fixTargetBlank();
    
}
</script>

</div>

<style>
#colorbox{ top:0 !important; }    
</style>