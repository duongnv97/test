<div class="form">
    <?php
        $LinkNormal     = Yii::app()->createAbsoluteUrl('admin/gasOrders/index');
        $LinkWait       = Yii::app()->createAbsoluteUrl('admin/gasOrders/index', array( 'wait'=> GasOrders::WAIT_CAR));
        $LinkInventory  = Yii::app()->createAbsoluteUrl('admin/gasOrders/index', array( 'wait'=> GasOrders::WAIT_VIEW_INVENTORY));
        $aUidAllow  = [GasConst::UID_DIEUXE_KHO_PHUOCTAN, 485407, 129122, 2];
        $cUid       = MyFormat::getCurrentUid();
    ?>
    <h1><?php echo $this->pageTitle;?>
        <a class='btn_cancel f_size_14 <?php echo (!isset($_GET['wait'])) ? "active":"";?>' href="<?php echo $LinkNormal;?>">Normal</a>
        <a class='btn_cancel f_size_14 <?php echo isset($_GET['wait']) && $_GET['wait']==1 ? "active":"";?>' href="<?php echo $LinkWait;?>">In Theo Xe</a>
        <?php if(in_array($cUid, $aUidAllow) ): ?>
            <a class='btn_cancel f_size_14 <?php echo isset($_GET['wait']) && $_GET['wait']== GasOrders::WAIT_VIEW_INVENTORY ? "active":"";?>' href="<?php echo $LinkInventory;?>">Tồn kho đại lý</a>
        <?php endif; ?>
    </h1> 
</div>