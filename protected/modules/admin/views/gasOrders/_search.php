<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
            <?php echo $form->label($model,'orders_no',array()); ?>
            <?php echo $form->textField($model,'orders_no',array('size'=>30,'maxlength'=>30)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'date_delivery',array()); ?>
		<?php 
                    $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                        'model'=>$model,        
                        'attribute'=>'date_delivery',
                        'options'=>array(
                            'showAnim'=>'fold',
                            'dateFormat'=> MyFormat::$dateFormatSearch,
                            'changeMonth' => true,
                            'changeYear' => true,
                            'showOn' => 'button',
                            'buttonImage'=> Yii::app()->theme->baseUrl.'/admin/images/icon_calendar_r.gif',
                            'buttonImageOnly'=> true,                                
                        ),        
                        'htmlOptions'=>array(
                            'class'=>'w-16',
                            'size'=>'16',
                            'style'=>'height:20px;',                               
                        ),
                    ));
                ?> 
	</div>


	<div class="row">
		<?php echo $form->label($model,'created_date',array()); ?>
		<?php 
                    $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                        'model'=>$model,        
                        'attribute'=>'created_date',
                        'options'=>array(
                            'showAnim'=>'fold',
                            'dateFormat'=> MyFormat::$dateFormatSearch,
                            'changeMonth' => true,
                            'changeYear' => true,
                            'showOn' => 'button',
                            'buttonImage'=> Yii::app()->theme->baseUrl.'/admin/images/icon_calendar_r.gif',
                            'buttonImageOnly'=> true,                                
                        ),        
                        'htmlOptions'=>array(
                            'class'=>'w-16',
                            'size'=>'16',
                            'style'=>'height:20px;',                               
                        ),
                    ));
                ?> 
	</div>
    

	<div class="row">
            <?php echo $form->label($model,'user_id_create',array()); ?>
            <?php echo $form->dropDownList($model,'user_id_create', Users::getArrUserByRoleNotCheck(array(ROLE_AGENT, ROLE_DIEU_PHOI)),array('style'=>'','empty'=>'Select')); ?>
	</div>

	<div class="row">
            <?php echo $form->label($model,'user_id_executive',array()); ?>
            <?php echo $form->dropDownList($model,'user_id_executive', Users::getSelectByRoleNotRoleAgent(ROLE_SCHEDULE_CAR),array('style'=>'','empty'=>'Select')); ?>
	</div>
    
        <?php $aReasonWrong = GasOrdersDetail::model()->getArrayReasonWrong();?>
	<div class="row">
            <?php echo $form->label($model,'reason_wrong',array()); ?>
            <?php echo $form->dropDownList($model,'reason_wrong', $aReasonWrong,array('style'=>'','empty'=>'Select')); ?>
	</div>    

	<div class="row buttons" style="padding-left: 159px;">
		        <?php $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType'=>'submit',
            'label'=>'Search',
            'type'=>'null', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
            'size'=>'small', // null, 'large', 'small' or 'mini'
            //'htmlOptions' => array('style' => 'margin-bottom: 10px; float: right;'),
        )); ?>	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->