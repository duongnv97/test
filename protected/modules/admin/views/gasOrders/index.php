<?php
$this->breadcrumbs=array(
    $this->pageTitle,
);

$menus=array(
    array('label'=>'Tạo Đặt Hàng', 'url'=>array('create')),
);
$this->menu= ControllerActionsName::createMenusRoles($menus, $actions);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('gas-orders-grid', {
                url : $(this).attr('action'),
		data: $(this).serialize()
	});
	return false;
});
");

Yii::app()->clientScript->registerScript('ajaxupdate', "
$('#gas-orders-grid a.ajaxupdate').live('click', function() {
    $.fn.yiiGridView.update('gas-orders-grid', {
        type: 'POST',
        url: $(this).attr('href'),
        success: function() {
            $.fn.yiiGridView.update('gas-orders-grid');
        }
    });
    return false;
});
");

?>

<?php include "index_button.php"; ?>

<?php if(isset($_GET['wait']) && $_GET['wait'] == GasOrders::WAIT_CAR): ?>
    <?php if($model->canPrintDieuXe()): ?>
        <?php include "index_print.php"; ?>
    <?php endif; ?>
<?php elseif(isset($_GET['wait']) && $_GET['wait'] == GasOrders::WAIT_VIEW_INVENTORY): ?>
    <?php include "indexInventory.php"; ?>
<?php else: ?>
    <?php include "index_normal.php"; ?>
<?php endif; ?>