<?php
$this->breadcrumbs=array(
	'Đặt Hàng'=>array('index'),
	$model->orders_no . "- Xem chi tiết người tạo đơn hàng",
);
$menus = array(
	array('label'=>'Đặt Hàng', 'url'=>array('index')),
	array('label'=>'Tạo Mới Đặt Hàng', 'url'=>array('create')),
);
$this->menu = ControllerActionsName::createMenusRoles($menus, $actions);
$aCar       = Users::getSelectByRoleForAgent($model->user_id_executive, ONE_AGENT_CAR, '', array('status'=>1, 'GetNameOnly'=>1));
$aDriver    = Users::getSelectByRoleForAgent($model->user_id_executive, ONE_AGENT_DRIVER, '', array('status'=>1, 'GetNameOnly'=>1));
$aPhuXe     = Users::getSelectByRoleForAgent($model->user_id_executive, ONE_AGENT_PHU_XE, '', array('status'=>1, 'GetNameOnly'=>1));
?>

<div class="search-form display_none" style="">
    <?php include "_search_car.php" ?>
</div><!-- search-form -->

<div class="clr"></div>
<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/print-store-card.css" />

<?php 
$sum_lpg_ton = $sum_quantity_50 = $sum_quantity_45 = $sum_quantity_12=$sum_quantity_6=$sum_quantity_other=0;
$index=1;
$from = time();
$aModelUser = $model->getModelCustomerAndSale();
$to = time();
$second = $to-$from;
//echo count($aModelUser).' done in: '.($second).'  Second  <=> '.($second/60).' Minutes';

$mAppOrder  = new GasAppOrder();
$mAppOrder->obj_id  = $model->id;
$aModelAppOrder     = $mAppOrder->getByOrderId();
$aInfoSum           = [];
$aInfoSum['sum']    = [];
?>

<div class="container" id="printElement">
    <table cellpadding="0" cellspacing="0">
        <tr>
            <td>
                <div class="logo">
                    <img src="<?php echo Yii::app()->theme->baseUrl; ?>/images/logo80x80.png">
                </div>            
                <p style="margin: 5px 0 20px 0;">CÔNG TY TNHH HƯỚNG MINH</p>
            </td>
            <td valign="top">
                <h2 style="margin: 35px 0 0">PHIẾU ĐỀ NGHỊ GIAO HÀNG</h2>
            </td>
        </tr>
        <tr>
            <td class="agent_info"><b>Người / Đơn vị đề nghị</b>: <?php echo $model->getAgentName()?></td>
            <td style="text-align: right;"><b>Ngày</b>: <?php echo MyFormat::dateConverYmdToDmy($model->date_delivery);?></td>
        </tr>
        <tr>
            <td class="agent_info"><b>Người / Đơn vị thực hiện</b>: <?php echo $model->rUserIdExecutive?$model->rUserIdExecutive->first_name:'';?></td>
            <td style="text-align: right;"></td>
        </tr>

    </table>
    <table cellpadding="0" cellspacing="0" class="tb hm_table">
        <thead>
           
            <tr>
                <th rowspan="2">STT</th>
                <th rowspan="2">Số Xe</th>
                <th rowspan="2">Tên Khách Hàng</th>
                <th rowspan="2">Địa Điểm Giao Hàng</th>
                <th rowspan="2">Nội Dung Công Việc</th>
                <th colspan="7">Số Lượng / Đơn Vị Tính</th>
                <th rowspan="2">Ghi Chú</th>
                <th rowspan="2">Người Tạo</th>
            </tr>
            <tr>
                <th>LPG (Tấn)</th>
                <th>Bình 50 Kg</th>
                <th>Bình 45 Kg</th>
                <th>Bình 12 Kg</th>
                <th>Bình 6 Kg</th>
                <th>Vật Tư Khác</th>
                <th>Giao Thực Tế</th>
            </tr>
        </thead>
        <tbody>
            <?php if(count($model->rOrderDetail)): ?>
                <?php foreach($model->rOrderDetail as $key=>$item):?>
                <?php 
                $customer_name = '';
                $customer_add = '';
                $customer_phone = '';
                $work_content = $item->work_content;
                if($item->materials){
                    $work_content = $item->materials->name;
                }
                $type_customer = ""; $customer_sale_id = "";
                
                if(isset($aModelUser[$item->customer_id])){
                    $mCustomer = $aModelUser[$item->customer_id];
                    $type_customer = $mCustomer->is_maintain;
                    $customer_name = $mCustomer->first_name;
                    $customer_add = $mCustomer->address;
                    $customer_sale_id = $mCustomer->sale_id;
                    if($mCustomer->phone != NO_PHONE_NUMBER)
                        $customer_phone = "ĐT: ".UsersPhone::formatPhoneLong($mCustomer->phone).". Đ/C: ";
                }
                
                $sale_id = $item->sale_id;
                $sale_name = '';
                $sale_phone = '';
                if($customer_sale_id == GasLeave::KH_CONGTY_BO){
                    $sale_id = GasLeave::UID_HEAD_GAS_BO;
                }
                if(isset($aModelUser[$sale_id]) && $type_customer == STORE_CARD_KH_BINH_BO){
                    $mSale = $aModelUser[$sale_id];
                    $sale_name = "<br><b>Sale: ".$mSale->first_name."</b> - $mSale->phone";
                }
                
                if(isset($model->car_number) && $model->car_number!='' && $model->car_number!=$item->car_number){
                    continue;
                }
                if(isset($model->driver_id) && $model->driver_id!='' && $model->driver_id!=$item->driver_id){
                    continue;
                }
                $sum_lpg_ton+=$item->lpg_ton;
                $sum_quantity_50+=$item->quantity_50;
                $sum_quantity_45+=$item->quantity_45;
                $sum_quantity_12+=$item->quantity_12;
                $sum_quantity_6+=$item->quantity_6;
                $sum_quantity_other+=$item->quantity_other_material;
                $reasonWrong = $item->getReasonWrong();
                
                $sGas = '';
                if(isset($aModelAppOrder[$item->id])){
                    $mAppOrder = $aModelAppOrder[$item->id];
                    if(!empty($mAppOrder->getCustomerPhoneOrder())){
                        $customer_phone = 'ĐT: '.$mAppOrder->getCustomerPhoneOrder().'<br>';
                    }
                    $mAppOrder->mapJsonFieldOneDecode('JSON_FIELD', 'json', 'baseArrayJsonDecode');
                    $sGas = $mAppOrder->getStringGas();
                    $aInfoGas = $mAppOrder->getJsonFieldOneDecode('info_gas', 'json', 'baseArrayJsonDecode');
                    foreach($aInfoGas as $materials_id => $detail){
                        if(!isset($aInfoSum['sum'][$materials_id])){
                            $aInfoSum['sum'][$materials_id] = $detail['qty_real'];
                        }else{
                            $aInfoSum['sum'][$materials_id] += $detail['qty_real'];
                        }
                        $aInfoSum['GasName'][$materials_id] = $detail['materials_name'];
                    }
                }
                
                $mMaterial      = $item->materials;// sum b12 của đại lý tạo
                if($item->type == GasOrders::TYPE_AGENT && $mMaterial){
                    $field_name = 'quantity_12';
                    if($mMaterial->materials_type_id == GasMaterialsType::MATERIAL_BINH_50KG){
                        $field_name = 'quantity_50';
                    }elseif($mMaterial->materials_type_id == GasMaterialsType::MATERIAL_BINH_45KG){
                        $field_name = 'quantity_45';
                    }elseif($mMaterial->materials_type_id == GasMaterialsType::MATERIAL_BINH_6KG){
                        $field_name = 'quantity_6';
                    }
                    
                    $materials_id   = $mMaterial->id;
                    if(!isset($aInfoSum['sum'][$materials_id])){
                        $aInfoSum['sum'][$materials_id] = $item->$field_name;
                    }else{
                        $aInfoSum['sum'][$materials_id] += $item->$field_name;
                    }
                    $aInfoSum['GasName'][$materials_id] = $mMaterial->name;
                }
                $nameDriver = isset($aDriver[$item->driver_id]) ? "<br>".$aDriver[$item->driver_id] : '';
                $namePhuXe1 = isset($aPhuXe[$item->phu_xe_1]) ? "<br>".$aPhuXe[$item->phu_xe_1] : '';
                $namePhuXe2 = isset($aPhuXe[$item->phu_xe_2]) ? "<br>".$aPhuXe[$item->phu_xe_2] : '';
                ?>

                <tr>
                    <td class="item_c <?php echo (empty($reasonWrong) || in_array($item->reason_wrong, $item->getArrayGiaoSau()) ) ? "" : "high_light_tr";?>">
                        <?php echo $index++;?>
                        <?php echo $reasonWrong; ?>
                    </td>
                    <td>
                        <?php echo isset($aCar[$item->car_number]) ? $aCar[$item->car_number] : '';
                            echo $nameDriver.$namePhuXe1.$namePhuXe2;
                        ?>
                    </td>
                    <td><?php echo $customer_name.$sale_name;?></td>
                    <td><?php echo $customer_phone.$customer_add;?></td>
                    <td><?php echo $work_content.'<br>'.$sGas;?></td>
                    <td class="item_r"><?php echo $item->lpg_ton>0?ActiveRecord::formatCurrency($item->lpg_ton):'';?></td>
                    <td class="item_c"><?php echo $item->quantity_50?$item->quantity_50:'';?></td>
                    <td class="item_c"><?php echo $item->quantity_45?$item->quantity_45:'';?></td>
                    <td class="item_c"><?php echo $item->quantity_12?$item->quantity_12:'';?></td>
                    <td class="item_c"><?php echo $item->quantity_6?$item->quantity_6:'';?></td>
                    <td class="item_c"><?php echo $item->quantity_other_material>0?ActiveRecord::formatCurrency($item->quantity_other_material):'';?></td>
                    <td class="item_c"><?php echo $item->real_output>0?ActiveRecord::formatCurrency($item->real_output):'';?></td>
                    <td class=""><?php echo $item->note;?>
                        <?php echo empty($item->note_2)?"":"<br>$item->note_2";?>
                    </td>
                    <td class=""><?php echo $item->getCreatedBy();?><br><?php echo $item->getCreatedDate();?></td>
                </tr>
                <?php endforeach;?>
            <?php endif;?>
        </tbody>
        
        <tfoot>
            <tr>
                <td class="item_r" colspan="5">Tổng Cộng</td>
                <td class="item_r">
                    <?php echo $sum_lpg_ton>0?ActiveRecord::formatCurrency($sum_lpg_ton):'';?>
                </td>
                <td class="item_c">
                    <?php echo $sum_quantity_50>0?ActiveRecord::formatCurrency($sum_quantity_50):'';?>
                </td>
                <td class="item_c">
                    <?php echo $sum_quantity_45>0?ActiveRecord::formatCurrency($sum_quantity_45):'';?>
                </td>
                <td class="item_c">
                    <?php echo $sum_quantity_12>0?ActiveRecord::formatCurrency($sum_quantity_12):'';?>
                </td>
                <td class="item_c">
                    <?php echo $sum_quantity_6>0?ActiveRecord::formatCurrency($sum_quantity_6):'';?>
                </td>
                <td class="item_c">
                    <?php echo $sum_quantity_other>0?ActiveRecord::formatCurrency($sum_quantity_other):'';?>
                </td>
                <td class="item_c">&nbsp;</td>
                <td class="item_c">&nbsp;</td>
                <td class="item_c">&nbsp;</td>
            </tr>
            <tr>
                <td class="item_r" colspan="5">Tổng Loại Gas</td>
                <td colspan="8">
                    <?php foreach ($aInfoSum['sum'] as $materials_id => $qty): ?>
                    <p><b>SL <?php echo ActiveRecord::formatCurrency($qty);?></b> : <?php echo isset($aInfoSum['GasName'][$materials_id]) ? $aInfoSum['GasName'][$materials_id] : '';?></p>
                    <?php endforeach; ?>
                </td>                
            </tr>
        </tfoot>
        
    </table>
    
    <?php if(trim($model->note)!=''):?>
    <p><b>Ghi Chú</b>: <?php echo $model->note;?></p>
    <?php endif;?>
    <?php include "_print_footer.php"; ?>
</div>

<script>
    fnShowhighLightTr();
</script>

