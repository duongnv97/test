<div class="form">
<?php $hideClass='';
    $cRole = MyFormat::getCurrentRoleId();
    $cUid = MyFormat::getCurrentUid();
    if( in_array($cRole, $model->getRoleLikeSubAgent())){
        $hideClass='display_none';
    }
    $DieuPhoiHideClass='';
    if($cRole==ROLE_DIEU_PHOI){
        $DieuPhoiHideClass='display_none';
    }
    
    $agentHideClass='';
    if($cRole==ROLE_SUB_USER_AGENT){
        $agentHideClass='display_none';
    }
?>
<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'gas-orders-form',
	'enableAjaxValidation'=>false,
)); ?>
    <p class="note">Dòng có dấu <span class="required">*</span> là bắt buộc.</p>
    <?php echo MyFormat::BindNotifyMsg(); ?>
    
   <div class="row <?php echo $DieuPhoiHideClass. " $agentHideClass" ;?>">
    <?php echo $form->labelEx($model,'agent_id'); ?>
    <?php echo $form->hiddenField($model,'agent_id', array('class'=>'agent_id')); ?>
    <?php
            // 1. limit search kh của sale
            $url = Yii::app()->createAbsoluteUrl('admin/ajax/search_by_role', array('role'=> ROLE_AGENT));
            // widget auto complete search user customer and supplier
            $aData = array(
                'model'=>$model,
                'field_customer_id'=>'agent_id',
                'url'=> $url,
                'name_relation_user'=>'rAgent',
                'ClassAdd' => 'w-400',
                'field_autocomplete_name' => 'autocomplete_name_1',
                'placeholder'=>'Nhập mã hoặc tên đại lý',
            );
            $this->widget('ext.SpaAutocompleteCustomer.SpaAutocompleteCustomer',
                array('data'=>$aData));
            ?>
        <?php echo $form->error($model,'agent_id'); ?>
    </div>
    
    <div class="row">
        <?php echo $form->labelEx($model,'date_delivery'); ?>
            <?php 
                $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                    'model'=>$model,        
                    'attribute'=>'date_delivery',
                    'options'=>array(
                        'showAnim'=>'fold',
                        'dateFormat'=> ActiveRecord::getDateFormatJquery(),
                        'minDate'=> "-".GasOrders::$days_allow_update,
                        'maxDate'=> '10',
                        'changeMonth' => true,
                        'changeYear' => true,
                        'showOn' => 'button',
                        'buttonImage'=> Yii::app()->theme->baseUrl.'/admin/images/icon_calendar_r.gif',
                        'buttonImageOnly'=> true,                                
                    ),        
                    'htmlOptions'=>array(
                        'class'=>'w-16',
                        'style'=>'height:20px;width:166px;',
                            'readonly'=>'readonly',
                    ),
                ));
            ?>     	
        <?php echo $form->error($model,'date_delivery'); ?>            
    </div>            

    <div class="row">
        <?php echo $form->labelEx($model,'user_id_executive'); ?>
        <?php echo $form->dropDownList($model,'user_id_executive', $model->getArrayDieuXe(),array('class'=>'user_id_executive','empty'=>'Select')); ?>
        <?php echo $form->error($model,'user_id_executive'); ?>
    </div>

    <div class="row">
        <label>&nbsp</label>
        <div>
            <a href="javascript:void(0);" style="line-height:25px;" class="text_under_none item_b" onclick="fnBuildRow();">
                <img style="float: left;margin-right:8px;" src="<?php echo Yii::app()->theme->baseUrl;?>/images/add.png"> 
                Thêm Dòng ( Phím tắt F8 )
            </a>
        </div>
    </div>


    <div class="row">
        <table class="materials_table materials_table_root materials_table_th" style="width:100%">
            <thead>
                <tr>
                    <th class="item_c w-10">#</th>
                    <th class="w-230 item_c <?php echo $hideClass;?>">Khách Hàng</th>
                    <th class="w-280 item_c">Nội Dung Công Việc</th>
                    <th class="w-30 item_c">LPG (Tấn)</th>
                    <th class="w-30 item_c">B50</th>
                    <th class="w-30 item_c">B45</th>
                    <th class="w-30 item_c">B12</th>
                    <th class="w-30 item_c">B6</th>
                    <th class="w-30 item_c">Vật Tư Khác</th>
                    <th class="w-100 item_c">Ghi Chú</th>
                    <th class="item_unit item_c last">Xóa</th>
                </tr>
            </thead>
            <tbody>
                <?php if(count($model->aModelOrderDetail)): ?>
                    <?php foreach($model->aModelOrderDetail as $key=>$item):?>
                    <?php $RowHide = "";// Jun 18, 2015 định thêm kiểm tra không cho điều phối sửa đơn hàng của nhau
                        // nhưng mà thực tế thì không được sửa từ trc rồi
                        if($cUid != $item->created_by){
                            $RowHide = "display_none";
                        }
                    ?>

                    <tr class="cash_book_row row_input <?php echo $RowHide;?>">
                        <td class="order_no item_c w-10"><?php echo ($key+1)    ;?></td>
                        <td class="col_customer <?php echo $hideClass;?>">                                
                            <div class="clr"></div>
                            <div class="row row_customer_autocomplete">
                                <div class="float_l">
                                    <input value="<?php echo $item->id;?>" type="hidden" name="GasOrdersDetail[id][]">
                                    <input class="customer_id_hide" value="<?php echo $item->customer_id;?>" type="hidden" name="GasOrdersDetail[customer_id][]">
                                    <input class="float_l customer_autocomplete w-190"  placeholder="Nhập mã, tên hoặc số đt" maxlength="100" value="<?php echo $item->customer?$item->customer->first_name:'';?>" type="text" <?php if($item->customer) echo 'readonly="1"'; ?>>
                                    <span class="remove_row_item" onclick="fnRemoveNameMulti(this);"></span>
                                </div>
                            </div>
                            <?php 
                                if($item->customer){
                                    $aData = array(
                                        'class_custom'=>'table_small',
                                        'info_code_bussiness'=>$item->customer->code_bussiness,
                                        'info_name'=>$item->customer->first_name,
                                        'info_address'=>$item->customer->address,
                                        'info_phone'=>$item->customer->phone,
                                    );
                                    $this->widget('ext.GasTableCustomerInfo.GasTableCustomerInfo',
                                        array('data'=>$aData));                                        
                                }
                            ?>
                        </td>
                        <td class="col_material <?php echo Yii::app()->user->role_id==ROLE_SUB_USER_AGENT?"w-250":''; ?>">
                            <div class="float_l ">
                                <?php 
                                    $material_name = '';
                                    $material_text = '';
                                    if($item->materials){
                                        $material_name = $item->materials->name;
                                        $material_text = $item->materials->materials_no.' - '.$item->materials->name;
                                    }
                                ?>

                                <textarea  name="GasOrdersDetail[work_content][]" class="work_content w-190 <?php echo $hideClass;?>" maxlength="300" rows="8"><?php echo $item->work_content;?></textarea>
                                <span class="<?php echo $DieuPhoiHideClass;?>">
                                    <input class="other_material_id_hide" value="<?php echo $item->other_material_id;?>" type="hidden" name="GasOrdersDetail[other_material_id][]">
                                    <input class="float_l material_autocomplete w-220"  placeholder="Nhập mã hoặc tên vật tư" maxlength="100" value="<?php echo $material_name;?>" type="text" <?php if($item->materials) echo 'readonly="1"'; ?>>
                                    <span onclick="" class="remove_material_js remove_item_material"></span>
                                    <div class="float_l ">
                                        <span class="material_text display_none"><?php echo $material_text;?>&nbsp;</span>
                                    </div>
                                </span>
                            </div>
                        </td>
                        <td>
                            <input name="GasOrdersDetail[lpg_ton][]" class="lpg_ton w-70 amount number_only number_only_v1" value="<?php echo $item->lpg_ton>0?ActiveRecord::formatCurrency($item->lpg_ton):'';?>" maxlength="9"  type="text">
                            <div class="help_number"></div>
                            <?php // echo '--'.$item->id;?>
                        </td>

                        <td><input name="GasOrdersDetail[quantity_50][]" class="quantity_50 w-20 qty number_only_v1" maxlength="6" value="<?php echo $item->quantity_50?$item->quantity_50:'';?>" type="text"></td>
                        <td><input name="GasOrdersDetail[quantity_45][]" class="quantity_45 w-20 qty number_only_v1" maxlength="3" value="<?php echo $item->quantity_45?$item->quantity_45:'';?>" type="text"></td>
                        <td><input name="GasOrdersDetail[quantity_12][]" class="quantity_12 w-20 qty number_only_v1" maxlength="3" value="<?php echo $item->quantity_12?$item->quantity_12:'';?>" type="text"></td>
                        <td><input name="GasOrdersDetail[quantity_6][]" class="quantity_6 w-20 qty number_only_v1" maxlength="3" value="<?php echo $item->quantity_6?$item->quantity_6:'';?>" type="text"></td>
                        <td>
                            <input name="GasOrdersDetail[quantity_other_material][]" class="quantity_other_material w-50 qty number_only_v1" maxlength="6" value="<?php echo $item->quantity_other_material>0?ActiveRecord::formatCurrency($item->quantity_other_material):'';?>" type="text">
                            <input name="GasOrdersDetail[need_update][]" class="w-20 need_update display_none" value="0" >
                        </td>
                        <td class="">
                            <textarea name="GasOrdersDetail[note][]" class="note w-150" maxlength="300" rows="8"><?php echo $item->note;?></textarea>
                        </td>
                        <td class="item_c last">
                        <?php if($item->canDeleteRow()): ?>
                            <span remove="<?php echo $item->id;?>" class="remove_icon_only"></span>
                        <?php else: ?>
                            <span class="from_app_order"></span>
                        <?php endif; ?>
                        </td>
                    </tr>                    
                    <?php endforeach;?>
                <?php endif;?>

            </tbody>
        </table>
    </div>            

    <div class="row">
        <?php echo $form->labelEx($model,'note'); ?>
            <div style="padding-left:141px;">
                <?php
                $this->widget('ext.niceditor.nicEditorWidget', array(
                        "model" => $model, // Data-Model
                        "attribute" => 'note', // Attribute in the Data-Model        
                        "config" => array(
                                "buttonList"=>Yii::app()->params['niceditor_v_2'],
                        ),
                        "width" => EDITOR_WIDTH, // Optional default to 100%
                        "height" => "60px", // Optional default to 150px
                ));
                ?>                                
            </div>		
        <?php echo $form->error($model,'note'); ?>
    </div>

    <div class="row buttons" style="padding-left: 141px;">
        <?php $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType'=>'submit',
            'label'=>$model->isNewRecord ? 'Create' :'Save',
            'type'=>'null', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
            'size'=>'small', // null, 'large', 'small' or 'mini'
            //'htmlOptions' => array('style' => 'margin-bottom: 10px; float: right;'),
        )); ?>	
    </div>

<?php $this->endWidget(); ?>
</div><!-- form -->

<style>
.note {
    font-size: 13px !important;
    font-weight: normal !important; 
    font-style: normal !important; 
}
</style>

<?php Yii::app()->clientScript->registerCoreScript('jquery.ui'); ?>
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/jquery.floatThead.min.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/underscore-min.js"></script>    
<script>
    $(document).ready(function(){
        $('.form').find('button:submit').click(function(){
            $.blockUI({ overlayCSS: { backgroundColor: '#fff' } }); 
        });
        
        fnBindAllAutocomplete();
        fnRefreshOrderNumber();    
        fnBindRemoveIcon();
        fnBindRemoveMaterial();
        fnChangeReleaseDate();
//        $('.materials_table').floatThead();
        checkChangeData();
        
    });    
    
    $(document).keydown(function(e) {
        if(e.which == 119) {
            fnBuildRow();
        }
    });
    
    function fnChangeReleaseDate(){
        $('#GasOrders_date_delivery, .user_id_executive').change(function(){
          var  url_ = '<?php echo Yii::app()->createAbsoluteUrl('admin/gasOrders/create')?>';
          var date_delivery = $('#GasOrders_date_delivery').val();
          var user_id_executive = $('.user_id_executive').val();
          var agent_id = $('.agent_id').val();
          if($.trim(date_delivery)!='' && $.trim(user_id_executive)!='')
          {
            $.blockUI({ overlayCSS: { backgroundColor: '#fff' } }); 
            $.ajax({
                url: url_,
                data:{agent_id:agent_id, date_delivery:date_delivery,user_id_executive:user_id_executive,'from_ajax':1},
                dataType:'json',
                type:'post',
                success:function(data){
//                    if(data['success'])// Close on Jan 31, 2017 lúc nào cũng phải load lại url
                    window.location = data['next'];
                    $.unblockUI();
                }
                });
            }
        });
    }    
    
    // to do bind autocompelte for input material
    // @param objInput : is obj input ex  $('.customer_autocomplete')    
    function bindMaterialAutocomplete(objInput){
        var availableMaterials = <?php echo MyFunctionCustom::getMaterialsJson(); ?>;
        var parent_div = objInput.closest('td.col_material');
        objInput.autocomplete({
            source: availableMaterials,
//            close: function( event, ui ) { $( "#GasStoreCard_materials_name" ).val(''); },
            select: function( event, ui ) {
                parent_div.find('.other_material_id_hide').val(ui.item.id);
                parent_div.find('.material_text').text(ui.item.materials_no +' - '+ui.item.name);
                parent_div.find('.material_autocomplete').attr('readonly',true);
            }
        });
    }
    
    
    function fnBuildRow(){
        var _tr = '';
        _tr += '<tr class="cash_book_row row_input">';
                            _tr += '<td class="order_no item_c w-10"></td>';
                            _tr += '<td class="col_customer <?php echo $hideClass;?>">';
                                _tr += '<div class="clr"></div>';
                                _tr += '<div class="row row_customer_autocomplete">';
                                    _tr += '<div class="float_l">';
                                        _tr += '<input class="customer_id_hide" value="" type="hidden" name="GasOrdersDetail[customer_id][]">';
                                        _tr += '<input class="float_l customer_autocomplete w-190"  placeholder="Nhập mã, tên hoặc số đt" maxlength="100" value="" type="text">';
                                        _tr += '<span class="remove_row_item" onclick="fnRemoveNameMulti(this);"></span>';
                                    _tr += '</div>';
                                _tr += '</div>';
                            _tr += '</td>';
                            _tr += '<td class="col_material <?php echo Yii::app()->user->role_id==ROLE_SUB_USER_AGENT?"w-250":''; ?>">';
                                _tr += '<div class="float_l">';
                                    _tr += '<textarea name="GasOrdersDetail[work_content][]" class="work_content w-190 <?php echo $hideClass;?>" maxlength="300" rows="8"></textarea>';
                                    _tr += '<span class="<?php echo $DieuPhoiHideClass;?>">';
                                        _tr += '<input class="other_material_id_hide" value="" type="hidden" name="GasOrdersDetail[other_material_id][]">';
                                        _tr += '<input class="float_l material_autocomplete w-220"  placeholder="Nhập mã hoặc tên vật tư" maxlength="100" value="" type="text">';
                                        _tr += '<span onclick="" class="remove_material_js remove_item_material"></span>';
                                        _tr += '<div class="float_l">';
                                            _tr += '<span class="material_text display_none"></span>';
                                        _tr += '</div>';
                                    _tr += '</span>';
                                _tr += '</div>';
                            _tr += '</td>';
                            _tr += '<td>';
                                _tr += '<input name="GasOrdersDetail[lpg_ton][]" class="lpg_ton w-70 amount number_only number_only_v1" value="" maxlength="9"  type="text">';
                                _tr += '<div class="help_number"></div>';
                            _tr += '</td>';
                            
                            _tr += '<td><input name="GasOrdersDetail[quantity_50][]" class="quantity_50 w-20 qty number_only_v1" maxlength="6" value="" type="text"></td>';
                            _tr += '<td><input name="GasOrdersDetail[quantity_45][]" class="quantity_45 w-20 qty number_only_v1" maxlength="3" value="" type="text"></td>';
                            _tr += '<td><input name="GasOrdersDetail[quantity_12][]" class="quantity_12 w-20 qty number_only_v1" maxlength="3" value="" type="text"></td>';
                            _tr += '<td><input name="GasOrdersDetail[quantity_6][]" class="quantity_6 w-20 qty number_only_v1" maxlength="3" value="" type="text"></td>';
                            _tr += '<td><input name="GasOrdersDetail[quantity_other_material][]" class="quantity_other_material w-50 qty number_only_v1" maxlength="6" value="" type="text"></td>';
                            _tr += '<td class="">';
                                _tr += '<textarea name="GasOrdersDetail[note][]" class="note w-150" maxlength="300" rows="8"></textarea>';
                            _tr += '</td>';
                            _tr += '<td class="item_c last"><span remove="" class="remove_icon_only"></span></td>';
                        _tr += '</tr>';
        
        $('.materials_table_root tbody:first').append(_tr);
        fnRefreshOrderNumber();
        bindEventForHelpNumber();
        fnBindAllAutocomplete();
        $('.materials_table').floatThead();
    }    
    
    function fnBindAllAutocomplete(){
        // customer
        $('.customer_autocomplete').each(function(){
            fnAutocompleteCustomer($(this));
        });
        
        // material
        $('.material_autocomplete').each(function(){
            bindMaterialAutocomplete($(this));
        });
    }
    
    // to do bind autocompelte for input
    // @param objInput : is obj input ex  $('.customer_autocomplete')
    function fnAutocompleteCustomer(objInput){
        var parent_div = objInput.closest('td.col_customer');
        var parentTr = parent_div.closest('tr');
        objInput.autocomplete({
            source: '<?php echo Yii::app()->createAbsoluteUrl('admin/ajax/search_user_by_code');?>',
            minLength: '<?php echo MIN_LENGTH_AUTOCOMPLETE;?>',
            close: function( event, ui ) { 
                //$( "#GasStoreCard_materials_name" ).val(''); 
            },
            search: function( event, ui ) { 
                objInput.addClass('grid-view-loading-gas');
            },
            response: function( event, ui ) { 
                objInput.removeClass('grid-view-loading-gas');
                var json = $.map(ui, function (value, key) { return value; });
                if(json.length<1){
                    var error = '<div class=\'errorMessage clr autocomplete_name_text\'>Không tìm thấy dữ liệu.</div>';
                    if(parent_div.find('.autocomplete_name_text').size()<1){
                        parent_div.find('.remove_row_item').after(error);
                    }
                    else
                        parent_div.find('.autocomplete_name_text').show();
                }                    
                    
            },
            select: function( event, ui ) {
                objInput.attr('readonly',true);
                parent_div.find('.autocomplete_name_text').hide();   
                parent_div.find('.customer_id_hide').val(ui.item.id);   
                parent_div.find('.autocomplete_customer_info').remove();   
                var tableInfo = fnBuildTableCustomerInfo(ui.item.code_bussiness, ui.item.name_customer, ui.item.address, ui.item.phone);
                parent_div.append(tableInfo);
                parent_div.find('.autocomplete_customer_info').show();
                fnAjaxGetLastOrder(parentTr, ui);
            }
        });
    }
    
    function fnRemoveNameMulti(this_){
        var parent_div = $(this_).closest('td.col_customer');
        parent_div.find('.customer_autocomplete').attr("readonly",false);                                     
        parent_div.find('.customer_autocomplete').val("");             
        parent_div.find('.customer_id_hide').val("");
        parent_div.find('.autocomplete_customer_info').hide();             
    } 
    
    function fnBuildTableCustomerInfo(info_code_bussiness, info_name, info_address, info_phone){
        var table = '';
        table += '<div class="autocomplete_customer_info table_small" style="">';
            table += '<table>';
                table += '<tr>';
                    table += '<td class="_l td_first_t">Mã KH:</td>';
                    table += '<td class="_r info_code_bussiness td_last_r td_first_t">'+info_code_bussiness+'</td>';
                table += '</tr>';

                table += '<tr>';
                    table += '<td class="_l">Tên KH:</td>';
                    table += '<td class="_r info_name td_last_r">'+info_name+'</td>';
                table += '</tr>';
                table += '<tr>';
                    table += '<td class="_l">Địa chỉ:</td>';
                    table += '<td class="_r info_address td_last_r">'+info_address+'</td>';
                table += '</tr>';
                table += '<tr>';
                    table += '<td class="_l">Điện Thoại:</td>';
                    table += '<td class="_r info_phone td_last_r">'+info_phone+'</td>';
                table += '</tr>';
            table += '</table>';
        table += '</div>';
        table += '<div class="clr"></div';
        return table;
    }
    
    /**
    * @Author: ANH DUNG May 14, 2016
    * @Todo: get info last order of this customer
    * @param: parentTr is object tr 
    * @param: ui  object json info customer
    */
    function fnAjaxGetLastOrder(parentTr, ui){
        var url_ = "<?php echo Yii::app()->createAbsoluteUrl("admin/ajax/getLastOrderCustomer");?>";
        $.blockUI({ overlayCSS: { backgroundColor: '#fff' } }); 
        $.ajax({
            url: url_,
            type: 'post',
            dataType: "json",
            data: {customer_id : ui.item.id},
            success: function(data){
                if(data['success']){
                    if(data['chan_hang'] != ""){
                        alert(data['chan_hang']);
                        parentTr.find('.remove_row_item').trigger('click');
                    }else{
                        parentTr.find('.work_content').val(data['model']['work_content']);
                        parentTr.find('.lpg_ton').val(data['model']['lpg_ton']);
                        parentTr.find('.quantity_50').val(data['model']['quantity_50']);
                        parentTr.find('.quantity_45').val(data['model']['quantity_45']);
                        parentTr.find('.quantity_12').val(data['model']['quantity_12']);
                        parentTr.find('.quantity_6').val(data['model']['quantity_6']);
                        parentTr.find('.quantity_other_material').val(data['model']['quantity_other_material']);
                        parentTr.find('.note').val(data['model']['note']);
                    }
                }else{
                    if(data['chan_hang'] != ""){
                        alert(data['chan_hang']);
                        parentTr.find('.remove_row_item').trigger('click');
                    }
                }
                $.unblockUI();
            }
        });
    }
    
    function checkChangeData(){
        $('.cash_book_row').each(function(){
           $(this).find('input, textarea').change(function(){
               $(this).closest('.cash_book_row').find('.need_update').val(1);
           });
        });
        
        $('.cash_book_row').each(function(){
           $(this).find('.remove_row_item').remove();
        });
        
    }
    
</script>