<?php 
    $namePhuXe1 = isset($aPhuXe[$item->phu_xe_1]) ? $aPhuXe[$item->phu_xe_1] : '';
    $namePhuXe2 = isset($aPhuXe[$item->phu_xe_2]) ? $aPhuXe[$item->phu_xe_2] : '';
    $nameDriver = isset($aDriver[$item->driver_id]) ? $aDriver[$item->driver_id] : '';

?>
<!--<input name="GasOrdersDetail[car_number][]" class=" w-80" maxlength="10" value="<?php echo $item->car_number;?>" type="text" placeholder="Số Xe">-->
<?php echo CHtml::dropDownList('GasOrdersDetail[reason_wrong][]', $item->reason_wrong, 
          $item->getArrayReasonWrong(), array('class'=>'w-180','empty'=>'Chọn trạng thái'));?>
<?php echo CHtml::dropDownList('GasOrdersDetail[road_route][]', $item->road_route, 
          $mStoreCard->getArrRoadRoute(), array('class'=>'w-180 '));?>
<?php echo CHtml::dropDownList('GasOrdersDetail[car_number][]', $item->car_number,
          $aCar, array('class'=>'w-180 ChangeCar','empty'=>'Chọn Xe'));?>
<?php echo CHtml::dropDownList('GasOrdersDetail[driver_id][]', $item->driver_id, 
          $aDriver, array('class'=>'w-180 ChangeDriver','empty'=>'Chọn Lái Xe'));?>

<div class="box-cart ">
    <div class="item-ipt-cart WrapDriverSearch form" style="padding-right: 0; padding-left: 20px;">
        <div class="AutoSearchAgent" style="width: 100%">
            <input class="call-iptclass OrderDriverName" placeholder="Phụ xe 1" type="text" value="<?php echo $namePhuXe1; ?>" <?php echo !empty($namePhuXe1) ? 'readonly' : '';?>>
        </div>
        <span class="remove_row_item" onclick="fnRemoveNameAgent(this)" style="margin-left: 0;"></span>
        <input class="UserId" name="GasOrdersDetail[phu_xe_1][]" type="hidden" value="<?php echo $item->phu_xe_1;?>">
    </div>
</div>

<div class="box-cart ">
    <div class="item-ipt-cart WrapDriverSearch form" style="padding-right: 0; padding-left: 20px;">
        <div class="AutoSearchAgent" style="width: 100%">
            <input class="call-iptclass OrderDriverName" placeholder="Phụ xe 2" type="text" value="<?php echo $namePhuXe2; ?>" <?php echo !empty($namePhuXe2) ? 'readonly' : '';?>>
        </div>
        <span class="remove_row_item" onclick="fnRemoveNameAgent(this)" style="margin-left: 0;"></span>
        <input class="UserId" name="GasOrdersDetail[phu_xe_2][]" type="hidden" value="<?php echo $item->phu_xe_2;?>">
    </div>
</div>