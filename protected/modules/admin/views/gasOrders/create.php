<?php
$this->breadcrumbs=array(
	'Đặt Hàng'=>array('index'),
	'Tạo Mới',
);

$menus = array(		
        array('label'=>'Đặt Hàng', 'url'=>array('index')),
);
$this->menu= ControllerActionsName::createMenusRoles($menus, $actions);
?>

<h1>Tạo Mới Đặt Hàng</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>