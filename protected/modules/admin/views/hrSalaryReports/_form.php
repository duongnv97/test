<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'hr-salary-reports-form',
	'enableAjaxValidation'=>false,
        'htmlOptions' => array('enctype' => 'multipart/form-data'),
)); ?>

    <p class="note">Dòng có dấu <span class="required">*</span> là bắt buộc.</p>
    <?php echo MyFormat::BindNotifyMsg(); ?>
    
        		
        
    <div class="row">
        <?php echo $form->labelEx($model,'start_date'); ?>
        <?php
            if ($model->isNewRecord) {
                $date = CommonProcess::getFirstDateOfCurrentMonth(DomainConst::DATE_FORMAT_3);
            } else {
                $date = CommonProcess::convertDateTime($model->start_date,
                        DomainConst::DATE_FORMAT_4,
                        DomainConst::DATE_FORMAT_3);
            }
            $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                'model'     => $model,
                'attribute' => 'start_date',
                'options'   => array(
                    'showAnim'      => 'fold',
                    'dateFormat'    => DomainConst::DATE_FORMAT_2,
                    'changeMonth'   => true,
                    'changeYear'    => true,
                    'showOn'        => 'button',
                    'buttonImage'   => Yii::app()->theme->baseUrl . '/admin/images/icon_calendar_r.gif',
                    'buttonImageOnly' => true,
                ),
                'htmlOptions'=>array(
                            'class'=>'w-16',
                            'style'=>'height:20px;width:166px;',
                            'readonly'=>'readonly',
                            'value' => $date,
                        ),
            ));
        ?>
        <?php echo $form->error($model,'start_date'); ?>
    </div>
    <div class="row">
        <?php echo $form->labelEx($model,'end_date'); ?>
        <?php
            if ($model->isNewRecord) {
                $date = CommonProcess::getLastDateOfCurrentMonth(DomainConst::DATE_FORMAT_3);
            } else {
                $date = CommonProcess::convertDateTime($model->end_date,
                        DomainConst::DATE_FORMAT_4,
                        DomainConst::DATE_FORMAT_3);
            }
            $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                'model'     => $model,
                'attribute' => 'end_date',
                'options'   => array(
                    'showAnim'      => 'fold',
                    'dateFormat'    => DomainConst::DATE_FORMAT_2,
                    'changeMonth'   => true,
                    'changeYear'    => true,
                    'showOn'        => 'button',
                    'buttonImage'   => Yii::app()->theme->baseUrl . '/admin/images/icon_calendar_r.gif',
                    'buttonImageOnly' => true,
                ),
                'htmlOptions'=>array(
                            'class'=>'w-16',
                            'style'=>'height:20px;width:166px;',
                            'readonly'=>'readonly',
                            'value' => $date,
                        ),
            ));
        ?>
        <?php echo $form->error($model,'end_date'); ?>
    </div>
    <div class="row">
        <?php echo $form->labelEx($model,'name'); ?>
        <?php echo $form->textField($model,'name',array('size'=>60,'maxlength'=>255)); ?>
        <?php echo $form->error($model,'name'); ?>
    </div>
    <div class="row">
        <?php echo $form->labelEx($model,'type_id'); ?>
        <?php echo $form->dropDownList($model, 'type_id', HrFunctionTypes::loadItems()); ?>
        <?php echo $form->error($model,'type_id'); ?>
    </div>
    <div class="row">
        <?php echo $form->labelEx($model,'data'); ?>
        <?php echo $form->textArea($model,'data',array('rows'=>6, 'cols'=>50)); ?>
        <?php echo $form->error($model,'data'); ?>
    </div>
<!--    <div class="row">
        <?php echo $form->labelEx($model,'created_date'); ?>
        <?php echo $form->textField($model,'created_date'); ?>
        <?php echo $form->error($model,'created_date'); ?>
    </div>
    <div class="row">
        <?php echo $form->labelEx($model,'created_by'); ?>
        <?php echo $form->textField($model,'created_by'); ?>
        <?php echo $form->error($model,'created_by'); ?>
    </div>-->
<!--    <div class="row">
        <?php echo $form->labelEx($model,'status'); ?>
        <?php echo $form->textField($model,'status'); ?>
        <?php echo $form->error($model,'status'); ?>
    </div>-->
<!--    <div class="row">
        <?php echo $form->labelEx($model,'approved_by'); ?>
        <?php echo $form->textField($model,'approved_by',array('size'=>11,'maxlength'=>11)); ?>
        <?php echo $form->error($model,'approved_by'); ?>
    </div>-->
<!--    <div class="row">
        <?php echo $form->labelEx($model,'approved_date'); ?>
        <?php echo $form->textField($model,'approved_date'); ?>
        <?php echo $form->error($model,'approved_date'); ?>
    </div>-->
<!--    <div class="row">
        <?php echo $form->labelEx($model,'notify'); ?>
        <?php echo $form->textArea($model,'notify',array('rows'=>6, 'cols'=>50)); ?>
        <?php echo $form->error($model,'notify'); ?>
    </div>-->
	<div class="row buttons" style="padding-left: 141px;">
		        <?php $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType'=>'submit',
            'label'=>$model->isNewRecord ? 'Create' :'Save',
            'type'=>'null', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
            'size'=>'small', // null, 'large', 'small' or 'mini'
            //'htmlOptions' => array('style' => 'margin-bottom: 10px; float: right;'),
        )); ?>	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->
<script>
    $(document).ready(function(){
        $('.form').find('button:submit').click(function(){
            $.blockUI({ overlayCSS: { backgroundColor: '#fff' } }); 
        });
        
    });
</script>