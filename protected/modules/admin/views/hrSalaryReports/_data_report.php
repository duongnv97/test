<?php
Yii::app()->clientScript->registerScript('search', "
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('Report-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<?php echo MyFormat::BindNotifyMsg();?>
<div class='grid-view grid-view-loading' style="width: 20px; height: 20px; display:none;"></div>
<div class="container-fluid" id='contentTabCtn<?php echo $type ?>' style="margin: 10px">
    <div class='row'>
        <ul class="nav nav-tabs child-tab">
            <!--Thay tab đang tính bằng tab đã chốt-->
            <?php 
//            $mType = HrFunctionTypes::model()->find('name = "tinh luong"'); // type tính lương
//            if($mType->id == $type){
            $salaryType = UsersHr::model()->getIdCalcSalary();
            if($salaryType == $type){
            ?>
            <li class="active"><a class='tab final-tab' data-toggle="tab" href="#final<?php echo $type ?>">Đang tính</a></li>
            <?php } else { ?>
            <li class="active"><a class='tab' data-toggle="tab" href="#calculating<?php echo $type ?>">Đang tính</a></li>
            <?php } ?>
            <li><a class="tab" data-toggle="tab" href="#approved<?php echo $type ?>">Đã duyệt</a></li>
        </ul>

        <div class="tab-content">
            <!--Tab đang tính của phần tính lương-->
            <?php 
            if($type == $salaryType){
            ?>
                <div id="final<?php echo $type ?>" class="tab-acting tab-pane fade in active">
                    <?php include '_search_salary.php'; ?>
                    <div class='reportTblContent' style="width: 100%;"></div>
                </div>
            <?php } else { ?>
            <!--Tab đang tính-->
            <div id="calculating<?php echo $type ?>" class="tab-acting tab-pane fade in active">
                <div class="left-menu">
                    <ul class="month list-report" style="overflow-y:scroll;max-height: 405px;">
                        <?php 
                        $model = new HrSalaryReports('search');
                        $aRp   = $model->getArraySalaryReportsByType($type, array(HrSalaryReports::STT_CALCULATING, HrSalaryReports::STT_REQUEST), true);
                        $aStatusColor = array(
                            HrSalaryReports::STT_CALCULATING => '#e6f1fd!important',
                            HrSalaryReports::STT_REQUEST     => '#ffa2a2!important',
                            HrSalaryReports::STT_APPROVED    => '#dbcb7f!important',
                            HrSalaryReports::STT_FINAL       => '#a2ffba!important',
                        );
                        
                        foreach ($aRp as $value) {
                            $st = MyFormat::dateConverYmdToDmy($value->start_date);
                            $en = MyFormat::dateConverYmdToDmy($value->end_date);
                            $code_no = empty($value->code_no) ? '' : "data-code_no='{$value->code_no}'";
                            ?>
                            <li class="ajaxBtn calculatingBtn btn btn-default" 
                                data-action='view' 
                                data-id='<?php echo $value->id ?>' 
                                <?php echo $code_no ?>
                                style="background: <?php echo $aStatusColor[$value->status] ?>">
                                <?php echo '<b>'.$value->name.'</b><br>'.$st.' - '.$en; ?>
                            </li>
                        <?php } ?>
                    </ul> 
                    <ul class="month gr-btn" style="padding-right:15px;">
                        <?php if(GasCheck::isAllowAccess('HrSalaryReports', 'Calculate')):?>
                            <a class="newReportBtn btn btn-primary calculate-btn<?php echo $type; ?>" href="<?php echo Yii::app()->createAbsoluteUrl('/admin/HrSalaryReports/calculate', array('type'=>$type)) ?>">Thêm</a>
                            <a class="recalculateBtn btn btn-primary calculate-btn<?php echo $type; ?>" data-action='calculate' href="<?php echo Yii::app()->createAbsoluteUrl('/admin/HrSalaryReports/calculate', array('type'=>$type, 're'=>1)) ?>">Tính lại</a>
                            <a class="moveToApprovedTab changeSttBtn btn btn-primary" data-type-btn='<?php echo HrSalaryReports::STT_APPROVED ?>'>Duyệt</a>
                        <?php endif; ?>
                        <?php if(GasCheck::isAllowAccess('HrSalaryReports', 'Delete')):?>
                            <a class="deleteReportBtn btn btn-primary" data-action='delete'>Xoá</a>
                        <?php endif; ?>
                    </ul>
                </div>
                <div class='reportTblContent'></div>
            </div>
            <?php } ?>
            
            
            <!--Tab đã duyệt-->
            <div id="approved<?php echo $type ?>" class="tab-acting tab-pane fade">
                <div class="left-menu">
                    <ul class="month list-report" id='list-report-left-bar<?php echo $type ?>' style="overflow-y:scroll;max-height: 405px;">
                        <?php 
                        $model = new HrSalaryReports();
                        $aRp = $model->getArraySalaryReportsByType($type, [HrSalaryReports::STT_APPROVED, HrSalaryReports::STT_FINAL], true);
                        foreach ($aRp as $value) {
                            $st = MyFormat::dateConverYmdToDmy($value->start_date);
                            $en = MyFormat::dateConverYmdToDmy($value->end_date);
                            $code_no = empty($value->code_no) ? '' : "data-code_no='{$value->code_no}'";
                            ?>
                        <li class="ajaxBtn approvedBtn btn btn-default" 
                            data-action='view' 
                            data-id='<?php echo $value->id ?>' 
                            <?php echo $code_no ?>
                            data-status='<?php echo $value->status ?>' 
                            style="background: <?php echo $aStatusColor[$value->status] ?>">
                                <?php echo '<b>'.$value->name.'</b><br>'.$st.' - '.$en; ?>
                            </li>
                        <?php } ?>
                    </ul> 
                    <ul class="month gr-btn" style="padding-right:15px;">
                        <?php if(GasCheck::isAllowAccess('HrSalaryReports', 'Calculate')):?>
                        <a class="okBtn changeSttBtn btn btn-primary disabled" data-type-btn='<?php echo HrSalaryReports::STT_FINAL ?>'>OK</a>
                        <a class="cancelBtn changeSttBtn btn btn-primary disabled moveToCalculatingTab" data-type-btn='<?php echo HrSalaryReports::STT_CALCULATING ?>'>Cancel</a>
                        <?php endif; ?>
                        <?php if(GasCheck::isAllowAccess('HrSalaryReports', 'ExportExcel')):?>
                        <a class="ExportExcelBtn btn btn-primary" data-action='' href="<?php echo Yii::app()->createAbsoluteUrl('admin/HrSalaryReports/ExportExcel') ?>">Export Excel</a>
                        <?php endif; ?>
                    </ul>
                </div>
                <div class='reportTblContent'></div>
            </div>
            
        </div>

    </div>
</div>

<script>
    $('.calculate-btn<?php echo $type ?>').on('click', function(){
        if($(this).hasClass('recalculateBtn')){
            var rId     = $(this).closest('.left-menu').find('ul.month.list-report li.active').data('id');
            var recUrl  = "<?php echo Yii::app()->createAbsoluteUrl('/admin/HrSalaryReports/calculate', array('type'=>$type, 're'=>1)) ?>";
            if(typeof rId == 'undefined'){
                alert('Vui lòng chọn bảng lương cần tính lại!');
                return false;
            }
            $(this).attr('href', recUrl+'/rId/'+rId);
        }
        $('.calculate-btn<?php echo $type ?>').colorbox({
            iframe: true,
    //        href: "<?php // echo Yii::app()->createAbsoluteUrl('/admin/HrSalaryReports/calculate') ?>"+'/type/'+<?php echo $type ?>,
            overlayClose: false, escKey: true,
            innerHeight: '550',
            innerWidth: '1000', close: '<span title=close>close</span>',
            onClosed: function () { // update view when close colorbox
                var contentElm      = $(this).closest('div.left-menu').siblings('.reportTblContent');
                var menuElm         = $(this).closest('div.left-menu').find('ul:first-child');
                var contentUrl      = '<?php echo Yii::app()->createAbsoluteUrl("/admin/HrSalaryReports/UpdateAjax") ?>';
                var menuUrl         = '<?php echo Yii::app()->createAbsoluteUrl("/admin/HrSalaryReports/GetHtmlListReport") ?>';
                var contentType     = 'get';
                var contentObjData = {
                        'type': '<?php echo $type ?>',
                        'action': 'loadLastCalculatingReport',
                    };
                var menuObjData = {
                        'type': '<?php echo $type ?>',
                    };
                ajaxRequest(contentUrl, contentObjData, contentElm);
                ajaxRequest(menuUrl, menuObjData, menuElm);
                menuElm.children().first().addClass('active');
            }
        });
    });
    
    $(document).on('click', '.requestBtn<?php echo $type ?>', function(e){
        var id = $(this).closest('.left-menu').siblings('.reportTblContent').find('input.id-final-report').val();
        if(typeof id == 'undefined') {
            id = '';
            alert('Vui lòng chọn báo cáo để xét duyệt!');
            e.preventDefault();
            return ;
        }
        e.preventDefault();
        $.colorbox({
            iframe: true,
            href: '<?php echo Yii::app()->createAbsoluteUrl('/admin/HrSalaryReports/SendMailReport') ?>' + '/id/' + id,
            overlayClose: false, escKey: true,
            innerHeight: '550',
            innerWidth: '1000', close: '<span title=close>close</span>',
            onClosed: function () { // update view when close colorbox

            }
        });
    })
</script>