<?php echo MyFormat::BindNotifyMsg(); ?>
<?php 
$css = Yii::app()->theme->baseUrl . '/admin/css/hr.css';
Yii::app()->clientScript->registerCssFile($css);
?>
<h2 style="padding-left: 159px;">Xét duyệt bảng lương!</h2>


<div class="wide form" style="padding:0;">
    <?php $form=$this->beginWidget('CActiveForm', array(
            'method'=>'post',
    )); ?>
    
  
    
        <div class="row buttons" style="padding-left: 159px;">
            <?php $this->widget('bootstrap.widgets.TbButton', array(
                'buttonType'=>'submit',
                'label'=>'Đồng ý',
                'type'=>'null', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
                'size'=>'small', // null, 'large', 'small' or 'mini'
                'htmlOptions' => array('name' => 'HrSalaryReports[approved]', 'value' => '1'),
            )); ?>	
        </div>

        <div class="row">
            <?php echo $form->labelEx($model,'notify'); ?>
            <?php echo $form->textArea($model,'notify'); ?>
            <?php echo $form->error($model,'notify'); ?>
        </div>
    
        <div class="row buttons" style="padding-left: 159px;">
            <?php $this->widget('bootstrap.widgets.TbButton', array(
                'buttonType'=>'submit',
                'label'=>'Yêu cầu chỉnh sửa',
                'type'=>'null', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
                'size'=>'small', // null, 'large', 'small' or 'mini'
                'htmlOptions' => array('name' => 'HrSalaryReports[require_update]', 'value' => '1'),
            )); ?>	
        </div>

    <?php $this->endWidget(); ?>
</div><!-- search-form -->

<?php 
$data = json_decode($model->data, true);
$mFuncType = new HrFunctionTypes();
$aType = $mFuncType->getArrayTypes();
$typeName = '';
foreach ($aType as $t) {
    if($t->id == $model->type_id){
        $typeName = $t->name;break;
    }
}
$std = MyFormat::dateConverYmdToDmy($model->start_date);
$ed = MyFormat::dateConverYmdToDmy($model->end_date);
$title = 'Bảng '.$typeName.' từ ngày '.$std.' đến ngày '.$ed;
?>
<h1><b><?php echo $title ?></b></h1>
<p>Tên bảng: <?php echo $model->name ?></p>
<p>Trạng thái: <span class="stt-txt"><?php echo HrSalaryReports::model()->aStatus[$model->status] ?></span></p>
<div class="tbl-container">
    <table class="reportTable" border="1" style="border-collapse: collapse;">
        <?php
        $aWeekendDate = [];
        $aDisplay = [];
        if(isset($data[-1])){
            foreach ($data[-1] as $key => $item){
                $aDisplay[] = $item;
            }
        }
        foreach ($data as $row_key => $row) {
            if ($row_key == 0) { //header table
                echo '<thead>';
                echo '<tr>';
                foreach ($row as $key => $cell) {
                    $cssClassWeekend = '';
                    if (strpos($cell, "T7") !== false || strpos($cell, "CN") !== false) {
                        $aWeekendDate[] = $key;
                        $cssClassWeekend = 'weekend';
                    }
                    $removeIcon = '<i class="remove-icon glyphicon glyphicon-remove"></i>';
                    if($key == 0){
                        $removeIcon = '';
                    }
                    $cssHide = '';
                    if(isset($aDisplay[$key]) && $aDisplay[$key] == 0){
                        $cssHide = ' display_none';
                    }
                    echo '<th class="' . $cssClassWeekend . $cssHide.  '">'
                            . $removeIcon
                            . $cell 
                        . '</th>';
                }
                echo '</tr>';
                echo '</thead>';
                echo '<tbody>';
            } else{ //body table
                if($row_key == -1) continue;
                foreach ($row as $key => $cell) {
                    $cssHide = '';
                    if(isset($aDisplay[$key]) && $aDisplay[$key] == 0){
                        $cssHide = ' display_none';
                    }
                    $cssClassWeekend = '';
                    if (in_array($key, $aWeekendDate)) {
                        $cssClassWeekend = 'weekend';
                    }
                    echo '<td class="' . $cssClassWeekend . $cssHide.  '">' . $cell . '</td>';
                }
                echo '</tr>';
            }
        }
        echo '</tbody>';
        ?>
    </table>
</div>

<p style="padding-left: 159px;">Nhấn vào 
    <a  href="<?php echo Yii::app()->createAbsoluteUrl('admin/hrSalaryReports/report') ?>">
        đây</a> 
    để xem danh sách các bảng lương khác!
</p>

<script>
    $(document).ready(function() {
        $('tbody').scroll(function() { //detect a scroll event on the tbody
            $('thead').css("left", -$(this).scrollLeft()); //fix the thead relative to the body scrolling
            $('thead th:nth-child(1)').css("left", $(this).scrollLeft()); //fix the first cell of the header
            $('tbody td:nth-child(1)').css("left", $(this).scrollLeft()); //fix the first column of tdbody
        });
    });
</script>