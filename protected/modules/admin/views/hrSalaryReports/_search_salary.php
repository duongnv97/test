
<div class="search-form search-container">
    <div class="wide form">

    <?php $form=$this->beginWidget('CActiveForm', array(
            'action'=>Yii::app()->createUrl($this->route),
            'method'=>'get',
            'htmlOptions'=>array(
                'id' => 'search-final-report'
            )
    )); ?>

        <?php 
        $model = new HrSalaryReports();
        ?>
<!--            <div class="row more_col">
                <?php // echo $form->labelEx($model, 'date_from'); ?>
                <?php 
//                $this->widget('zii.widgets.jui.CJuiDatePicker', array(
//                    'model' => $model,
//                    'attribute' => 'date_from',
//                    'options' => array(
//                        'showAnim' => 'fold',
//                        'dateFormat' => MyFormat::$dateFormatSearch,
//                        'changeMonth' => true,
//                        'changeYear' => true,
//                        'showOn' => 'button',
//                        'buttonImage' => Yii::app()->theme->baseUrl . '/admin/images/icon_calendar_r.gif',
//                        'buttonImageOnly' => true,
//                    ),
//                    'htmlOptions' => array(
//                        'class' => 'w-16',
//                        'style' => 'height:20px;',
//                    ),
//                ));
                ?> 
            </div>-->
            <?php 
            $aMonth = [];
            for($i = 1; $i <= 12; $i++){
                $aMonth[$i] = 'Tháng '.$i;
            }
            $model->search_month = intval(date('m'));
            $model->search_year  = date('Y');
            ?>

            <div class="row more_col">
                <div class="col1">
                    <?php echo $form->labelEx($model,'search_month',['label'=>'Chọn tháng']); ?>
                    <?php echo $form->dropdownList($model,'search_month',$aMonth, array('class'=>'w-100','empty'=>'Select')); ?>
                </div>

                <div class="col2">
                    <?php echo $form->labelEx($model,'search_year',['label'=>'Chọn năm']); ?>
                    <?php
                        $aYear = [] ;
                        for($i = -2; $i <= 2; $i++){
                            $aYear[date('Y')+$i] = date('Y')+$i;
                        }
                    ?>
                    <?php echo $form->dropdownList($model,'search_year', $aYear, array('class'=>'w-100','list'=>'list_year')); ?>
                    <?php // echo $form->textField($model,'search_year', array('class'=>'w-300','list'=>'list_year')); ?>
                    <?php // echo '<datalist id="list_year">'; ?>
                    <?php // for($i = -2; $i <= 2; $i++): ?>
                    <?php // echo '<option value="'.(date('Y')+$i).'">'; ?>
                    <?php // endfor; ?>
                    <?php // echo '</datalist>'; ?>
                </div>
            </div>

            <div class="row">
                <?php echo $form->labelEx($model,'search_province', ['label'=>'Tỉnh']); ?>
                <div class="fix-label">
                    <?php
                       $this->widget('ext.multiselect.JMultiSelect',array(
                             'model'=>$model,
                             'attribute'=>'search_province',
                             'data'=> GasProvince::getArrAll(),
                             // additional javascript options for the MultiSelect plugin
                            'options'=>array('selectedList' => 30,),
                             // additional style
                             'htmlOptions'=>array('class' => 'w-500'),
                       ));    
                   ?>
                </div>
            </div>

            <div class="row">
                <?php echo $form->labelEx($model,'search_position', ['label'=>'Bộ phận']); ?>
                <div class="fix-label">
                    <?php
                       $this->widget('ext.multiselect.JMultiSelect',array(
                             'model'=>$model,
                             'attribute'=>'search_position',
                             'data'=> UsersProfile::model()->getArrWorkRoom(),
                             // additional javascript options for the MultiSelect plugin
                            'options'=>array('selectedList' => 30,),
                             // additional style
                             'htmlOptions'=>array('class' => 'w-500'),
                       ));    
                   ?>
                </div>
            </div>

            <div class="row buttons">
                <?php $this->widget('bootstrap.widgets.TbButton', array(
                    'buttonType'=>'submit',
                    'label'=> 'Search',
                    'type'=>'null', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
                    'size'=>'small', // null, 'large', 'small' or 'mini'
                    'htmlOptions' => array('style' => 'margin-left: 49px;', 'class'=>'getListTableBtn'),
                )); ?>
            </div>

    <?php $this->endWidget(); ?>

    </div><!-- search-form -->
</div>  
<script>
    $(function(){
        $('#HrSalaryReports_search_province').multiselect({
            'header':true,
            'selectedList':30,
            'noneSelectedText':'Select an option',
            'selectedText':'# of # selected'
        });
        $('#HrSalaryReports_search_position').multiselect({
            'header':true,
            'selectedList':30,
            'noneSelectedText':'Select an option',
            'selectedText':'# of # selected'
        });
    });
</script>