<?php
/**
 * Content of report, rendered when click calculate button in view _calculating
 */
$data        = json_decode($model->data, true);
$fixedHeader = false;
// Sort header report
if( $model->type_id == UsersHr::model()->getIdCalcSalary() 
        && HrSalaryReports::model()->canSortHeader() ){
    $fixedHeader = true;
    include '_sort_salary_header.php';
}

$aPosition          = UsersProfile::model()->getArrWorkRoom();
$aProvince          = GasProvince::getArrAll();
$aPos               = array_unique( explode(',', $model->position_work) );
$aProv              = array_unique( explode(',', $model->province_id) );
$strPosition        = '';
$strProvince        = '';
foreach ($aPos as $pos_id) {
    $posName        = isset($aPosition[$pos_id]) ? $aPosition[$pos_id] : '';
    $strPosition    .= $posName .', ';
}
foreach ($aProv as $prv_id) {
    $provName        = isset($aProvince[$prv_id]) ? $aProvince[$prv_id] : '';
    $strProvince    .= $provName .', ';
}
?>
<h1><b><?php echo $title ?></b></h1>
<input type="hidden" value="<?php echo $model->id ?>" class="id-final-report">
<p>Tên bảng: <?php echo $model->name.'['.(empty($model->code_no) ? '' : $model->code_no).']'; ?></p>
<p>Tỉnh: <?php echo $strProvince; ?></p>
<p>Bộ phận: <?php echo $strPosition; ?></p>
<p>Trạng thái: <span class="stt-txt"><?php echo HrSalaryReports::model()->aStatus[$model->status] ?></span></p>
<?php 
$this->widget('HrTableViewWidget', ['data'=>$data, 'fixedHeader'=>$fixedHeader]);
?>
