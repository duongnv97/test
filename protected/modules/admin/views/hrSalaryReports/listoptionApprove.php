<?php 

?>
<div class="wide form" style="padding:0;">
    <?php $form=$this->beginWidget('CActiveForm', array(
            'method'=>'post',
    )); ?>

        <div class="row">
            <?php echo $form->labelEx($model,'approved_by'); ?>
            <?php echo $form->dropDownList($model,'approved_by', GasLeave::ListoptionApprove(),array('class'=>'w-400')); ?>
            <?php echo $form->error($model,'approved_by'); ?>
        </div>
    
        <div class="row buttons" style="padding-left: 159px;">
            <?php $this->widget('bootstrap.widgets.TbButton', array(
                'buttonType'=>'submit',
                'label'=>'Gửi xét duyệt',
                'type'=>'null', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
                'size'=>'small', // null, 'large', 'small' or 'mini'
                'htmlOptions' => array('class' => ''),
            )); ?>	
        </div>

    <?php $this->endWidget(); ?>
</div><!-- search-form -->
