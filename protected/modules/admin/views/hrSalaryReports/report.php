<?php

$this->breadcrumbs=array(
	$this->pageTitle,
);
$assets = Yii::app()->getAssetManager()->publish(Yii::getPathOfAlias('ext.multiselect') . '/assets');
$cs = Yii::app()->getClientScript();
$cs->registerScriptFile($assets . '/jquery.multiselect.min.js');
$cs->registerCssFile($assets . '/jquery.multiselect.css');

//Yii::app()->clientScript->registerScript('tab', "");
$css = Yii::app()->theme->baseUrl . '/admin/css/hr.css';
$cs->registerCssFile($css);
$css = Yii::app()->theme->baseUrl . '/js/bootstrap-multiselect-master/css/bootstrap-3.1.1.min.css';
$cs->registerCssFile($css);

$scriptUrl = Yii::app()->theme->baseUrl . '/admin/js/hr.js';
$cs->registerScriptFile($scriptUrl);
$cs->registerCoreScript('jquery.ui');
?>

<?php
    // Sorted tab
    $aTabModel = HrFunctionTypes::model()->getArrayOrderedTypes();
    $aTab = [];$aDataTab = [];
    foreach ($aTabModel as $value) {
        $aDataTab[] = $value->id;
        $aTab[] = $value->name;
    }

//    $aTabModel = HrFunctionTypes::model()->getArrayTypes();
//    $aTab = [];$aDataTab = [];
//    $salaryName = "";
//    foreach ($aTabModel as $value) {
//        if($value->id == UsersHr::model()->getIdCalcSalary()) {
//            $salaryName = $value->name;
//            continue;
//        }
//        $aDataTab[] = $value->id;
//        $aTab[] = $value->name;
//    }
//    $aTab[] = $salaryName;
//    $aDataTab[] = UsersHr::model()->getIdCalcSalary();

    $this->widget('TabWidget', array(
                                'aTab'=>$aTab,
                                'aDataTab' => $aDataTab,
                                'dataName'=>'type',
                                'ajax'=>false,
                            ));
    
?>

<script>
    $(document).ready(function(){
        var type = $('#tabs>ul>li.active>a.tabLink').data('type');
        var objData = {
                'type': type
            };
        var url = '<?php echo Yii::app()->createAbsoluteUrl('/admin/HrSalaryReports/getTypeReportContent') ?>';
        var elm = $('#tabs>div.tab-content>#tabstabLink1');
        ajaxRequest(url, objData, elm);
        
        //get parent tab content
        var urlTypeRp = '<?php echo Yii::app()->createAbsoluteUrl('/admin/HrSalaryReports/getTypeReportContent') ?>';
        fnBindLoadReportType(urlTypeRp);
            
//        var type = $('#tabs>ul>li.active>a.tabLink').data('type');
//        var elm = $('#contentTabCtn1 div.tab-acting tab-pane.fade.in.active div.reportTblContent');
//        var objData = {
//            'type':type
//        };
//        $.ajax({
//            'url': url,
//            'data': objData,
//            'success':function(html){
//                elm.html(html);
//                alert("Yêu cầu phê duyệt đã được gửi!");
//            },
//        });
//    })
    
    /*
         * event handle when get report data by ajax
     */
        var urlContent = '<?php echo Yii::app()->createAbsoluteUrl('/admin/HrSalaryReports/UpdateAjax') ?>';
        fnBinhLoadReportContent(urlContent);
    
        var urlList = '<?php echo Yii::app()->createAbsoluteUrl("admin/hrSalaryReports/getListTable") ?>';
        fnBindSearchReport(urlList);
    
//        var urlChangeRole = '<?php echo Yii::app()->createAbsoluteUrl("admin/hrSalaryReports/getListTable") ?>';
//        fbBindLoadReportOnChangeRole(urlChangeRole);
    
        var urlDelete = '<?php echo Yii::app()->createAbsoluteUrl('/admin/HrSalaryReports/delete') ?>';
        var urlTypeReport = '<?php echo Yii::app()->createAbsoluteUrl('/admin/HrSalaryReports/getTypeReportContent') ?>';
        fnBindDeleteReport(urlDelete, urlTypeReport);
    
        var urlLoadMoreMonth = '<?php echo Yii::app()->createAbsoluteUrl('/admin/HrSalaryReports/LoadMoreMonth') ?>';
        fnBindLoadMoreMonth(urlLoadMoreMonth);
    
        var urlFinalSalary = '<?php echo Yii::app()->createAbsoluteUrl("admin/hrSalaryReports/CalTotalSalary") ?>';
        fnBindCalculateFinalSalary(urlFinalSalary);
    
        var urlSwitchStatus = '<?php echo Yii::app()->createAbsoluteUrl('/admin/HrSalaryReports/changeStatus') ?>';
        var urlListReport = '<?php echo Yii::app()->createAbsoluteUrl("/admin/HrSalaryReports/GetHtmlListReport") ?>';
        fnBindSwitchStatus(urlSwitchStatus, urlListReport);
        
        fnBindShowHideTableColumn();
        toggleOkCancelBtn();
        fnBindGotoMissReport();
    
    });
</script>