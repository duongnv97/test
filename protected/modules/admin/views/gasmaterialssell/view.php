<?php
$this->breadcrumbs=array(
	'Bán Vật Tư'=>array('index'),
	$model->customer?$model->customer->first_name:"",
);

$menus = array(
	array('label'=>'GasMaterialsSell Management', 'url'=>array('index')),
	array('label'=>'Create GasMaterialsSell', 'url'=>array('create')),
	array('label'=>'Update GasMaterialsSell', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete GasMaterialsSell', 'url'=>array('delete'), 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
);
$this->menu= ControllerActionsName::createMenusRoles($menus, $actions);
?>

<h1>View Bán Vật Tư: <?php echo $model->customer?$model->customer->first_name:""; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
            array(
                'label'=>'Mã Đại Lý',
                'value'=>$model->customer?$model->customer->code_account:'',
            ), 
            array(
                'label'=>'Tên Đại Lý',
                'value'=>$model->customer?$model->customer->first_name:'',
            ),    		
            array(
                'name'=>'materials_id',
                'value'=>$model->materials?$model->materials->name:'',
            ),    		

		'qty',
		'price_sell:Currency',
		'total_sell:Currency',
		'price_root:Currency',
		'total_root:Currency',
		'sell_month',
		'sell_year',
	),
)); ?>
