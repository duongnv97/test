<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'agent_id',array()); ?>
		<?php echo $form->dropDownList($model,'agent_id', Users::getSelectByRole(),array('style'=>'width:350px;','empty'=>'Select')); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'materials_id',array()); ?>
		<?php $dataCat = CHtml::listData(GasMaterials::getCatLevel2(),'id','name','group'); ?>
		<?php echo $form->dropDownList($model,'materials_id', $dataCat,array('class'=>'category_ajax', 'style'=>'width:380px;','empty'=>'Select')); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'sell_month',array()); ?>
		<?php echo $form->textField($model,'sell_month',array('size'=>2,'maxlength'=>2)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'sell_year',array()); ?>
		<?php echo $form->textField($model,'sell_year',array('size'=>4,'maxlength'=>4)); ?>
	</div>

	<div class="row buttons" style="padding-left: 159px;">
		        <?php $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType'=>'submit',
            'label'=>Yii::t('translation','Search'),
            'type'=>'null', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
            'size'=>'small', // null, 'large', 'small' or 'mini'
            //'htmlOptions' => array('style' => 'margin-bottom: 10px; float: right;'),
        )); ?>	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->