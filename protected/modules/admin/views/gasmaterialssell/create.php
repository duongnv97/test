<?php
$this->breadcrumbs=array(
	Yii::t('translation','Bán Vật Tư')=>array('index'),
	Yii::t('translation','Create'),
);

$menus = array(		
        array('label'=> Yii::t('translation', 'Bán Vật Tư') , 'url'=>array('index')),
);
$this->menu= ControllerActionsName::createMenusRoles($menus, $actions);
?>

<h1><?php echo Yii::t('translation', 'Create Bán Vật Tư'); ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>