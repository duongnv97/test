<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'gas-materials-sell-form-popup',
	'enableAjaxValidation'=>false,
)); ?>
        <p class="note">Xóa dữ liệu bán hàng </p>
	<div class="row">
		<?php echo Yii::t('translation', $form->labelEx($model,'sell_month')); ?>
		<?php echo $form->dropDownList($model,'sell_month', ActiveRecord::getMonthVn(),array('style'=>'width:350px;')); ?>
		<?php echo $form->error($model,'sell_month'); ?>
	</div>
	<div class="row">
		<?php echo Yii::t('translation', $form->labelEx($model,'sell_year')); ?>
                <?php echo $form->dropDownList($model,'sell_year', ActiveRecord::getRangeYear(),array('style'=>'width:350px;')); ?>
		<?php echo $form->error($model,'sell_year'); ?>
	</div>

	<div class="row buttons" style="padding-left: 140px;">
            <input type="submit" value="Xóa Dữ Liệu" class="btnSubmit" onclick="return fnSubmit();">
        </div>

<?php $this->endWidget(); ?>

</div><!-- form -->
<script>
function fnSubmit(){
    var month = $('#GasMaterialsSell_sell_month').val();
    var year = $('#GasMaterialsSell_sell_year').val();
    if(confirm('Bạn chắc chắn muốn xóa dữ liệu bán hàng tháng '+month+' năm '+year))
        return true;
    return false;
}



</script>