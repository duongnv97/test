<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'gas-materials-sell-form',
	'enableAjaxValidation'=>false,
)); ?>

	<?php echo Yii::t('translation', '<p class="note">Fields with <span class="required">*</span> are required.</p>'); ?>
	
        <div class="row">
            <?php echo Yii::t('translation', $form->labelEx($model,'agent_id')); ?>
            <?php echo $form->dropDownList($model,'agent_id', Users::getSelectByRole(),array('style'=>'width:350px;')); ?>
            <?php echo $form->error($model,'agent_id'); ?>
	</div>    

	<div class="row">
		<?php echo Yii::t('translation', $form->labelEx($model,'materials_id')); ?>
		<?php $dataCat = CHtml::listData(GasMaterials::getCatLevel2(),'id','name','group'); ?>
		<?php echo $form->dropDownList($model,'materials_id', $dataCat,array('class'=>'category_ajax', 'style'=>'width:380px;','empty'=>'Select')); ?>
		
		<?php echo $form->error($model,'materials_id'); ?>
	</div>

	<div class="row">
		<?php echo Yii::t('translation', $form->labelEx($model,'qty')); ?>
		<?php echo $form->textField($model,'qty',array('class'=>'number_only')); ?>
		<?php echo $form->error($model,'qty'); ?>
	</div>

	<div class="row">
		<?php echo Yii::t('translation', $form->labelEx($model,'price_sell')); ?><div class="fix_number_help">
		<?php echo $form->textField($model,'price_sell',array('class'=>'number_only','size'=>16,'maxlength'=>14)); ?>
		<div class="help_number"></div>
                </div> <?php echo $form->error($model,'price_sell'); ?>
	</div>
<div class="clr"></div>
	<div class="row">
		<?php echo Yii::t('translation', $form->labelEx($model,'total_sell')); ?><div class="fix_number_help">
		<?php echo $form->textField($model,'total_sell',array('class'=>'number_only','size'=>16,'maxlength'=>14)); ?>
		<div class="help_number"></div>
                </div> <?php echo $form->error($model,'total_sell'); ?>
	</div>
<div class="clr"></div>
	<div class="row">
		<?php echo Yii::t('translation', $form->labelEx($model,'price_root')); ?><div class="fix_number_help">
		<?php echo $form->textField($model,'price_root',array('class'=>'number_only','size'=>16,'maxlength'=>14)); ?>
		<div class="help_number"></div>
                </div> <?php echo $form->error($model,'price_root'); ?>
	</div>
<div class="clr"></div>
	<div class="row">
		<?php echo Yii::t('translation', $form->labelEx($model,'total_root')); ?><div class="fix_number_help">
		<?php echo $form->textField($model,'total_root',array('class'=>'number_only','size'=>16,'maxlength'=>16)); ?>
		<div class="help_number"></div>
                </div> <?php echo $form->error($model,'total_root'); ?>
	</div>
<div class="clr"></div>
	<div class="row">
		<?php echo Yii::t('translation', $form->labelEx($model,'sell_month')); ?>
		<?php echo $form->dropDownList($model,'sell_month', ActiveRecord::getMonthVn(),array('style'=>'width:350px;')); ?>
		<?php echo $form->error($model,'sell_month'); ?>
	</div>
<div class="clr"></div>
	<div class="row">
		<?php echo Yii::t('translation', $form->labelEx($model,'sell_year')); ?>
		<?php echo $form->textField($model,'sell_year',array('class'=>'number_only','size'=>4,'maxlength'=>4)); ?>
		<?php echo $form->error($model,'sell_year'); ?>
	</div>

	<div class="row buttons" style="padding-left: 115px;">
		        <?php $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType'=>'submit',
            'label'=>$model->isNewRecord ? Yii::t('translation', 'Create') : Yii::t('translation', 'Save'),
            'type'=>'null', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
            'size'=>'small', // null, 'large', 'small' or 'mini'
            //'htmlOptions' => array('style' => 'margin-bottom: 10px; float: right;'),
        )); ?>	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->