<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'gas-materials-sell-form',
	'enableAjaxValidation'=>false,
)); ?>

	<?php echo Yii::t('translation', '<p class="note">Fields with <span class="required">*</span> are required.</p>'); ?>
	
        <div class="row">
		<?php echo Yii::t('translation', $form->labelEx($model,'agent_id')); ?>
                <?php echo $form->hiddenField($model,'agent_id'); ?>
                <?php 
                        $this->widget('zii.widgets.jui.CJuiAutoComplete', array(
                                'attribute'=>'autocomplete_name',
                                'model'=>$model,
                                'sourceUrl'=>Yii::app()->createAbsoluteUrl('admin/gascustomer/autocompleteagent'),
                //                                'name'=>'my_input_name',
                                'options'=>array(
                                        'minLength'=>MIN_LENGTH_AUTOCOMPLETE,
                                        'multiple'=> true,
                                    'select'=>"js:function(event, ui) {
                                            $('#GasMaterialsSell_agent_id').val(ui.item.id);
                                            var remove_div = '<span class=\'remove_row_item\' onclick=\'fnRemoveName(this)\'></span>';
                                            $('#GasMaterialsSell_autocomplete_name').parent('div').find('.remove_row_item').remove();
                                            $('#GasMaterialsSell_autocomplete_name').attr('readonly',true).after(remove_div);
                                            fnBuildTableInfo(ui.item);
                                            $('.autocomplete_customer_info').show();
                                    }",
                                ),
                                'htmlOptions'=>array(
                                    'size'=>45,
                                    'maxlength'=>45,
                                    'style'=>'float:left;',
                                    'placeholder'=>'Nhập tên hoặc mã đại lý',
                                ),
                        )); 
                        ?>        
                        <script>
                            function fnRemoveName(this_){
                                $(this_).prev().attr("readonly",false); 
                                $("#GasMaterialsSell_autocomplete_name").val("");
                                $("#GasMaterialsSell_agent_id").val("");
                                $('.autocomplete_customer_info').hide();
                            }
                        function fnBuildTableInfo(item){
                            $(".info_name").text(item.value);
                            $(".info_name_agent").text(item.name_agent);
                            $(".info_code_account").text(item.code_account);
                            $(".info_code_bussiness").text(item.code_bussiness);
                            $(".info_address").text(item.address);
                            $(".info_name_sale").text(item.sale);
                        }
                            
                        </script>        
                        <div class="clr"></div>		
		<?php echo $form->error($model,'agent_id'); ?>
                <?php $display='display:inline;';
                    $info_name ='';
                    $info_name_agent ='';
                    $info_address ='';
                    $info_code_account ='';
                    $info_code_bussiness ='';
                    $info_name_sale ='';
                        if(empty($model->agent_id)) $display='display: none;';
                        else{
                            $info_name = $model->customer->first_name;
                            $info_name_agent = $model->customer->name_agent;
                            $info_code_account = $model->customer->code_account;
                            $info_code_bussiness = $model->customer->code_bussiness;
                            $info_address = $model->customer->address;
                            $info_name_sale = $model->customer->sale?$model->customer->sale->first_name:'';
                        }
                ?>                        
                <div class="autocomplete_customer_info" style="<?php echo $display;?>">
                <table>
                    <tr>
                        <td class="_l">Mã đại lý:</td>
                        <td class="_r info_code_account"><?php echo $info_code_account;?></td>
                    </tr>

                    <tr>
                        <td class="_l">Tên đại lý:</td>
                        <td class="_r info_name"><?php echo $info_name;?></td>
                    </tr>
                    
                    <tr>
                        <td class="_l">Địa chỉ:</td>
                        <td class="_r info_address"><?php echo $info_address;?></td>
                    </tr>
                </table>
            </div>
            <div class="clr"></div>    		
            <?php echo $form->error($model,'agent_id'); ?>
	</div>    

	<div class="row">
		<?php echo Yii::t('translation', $form->labelEx($model,'materials_id')); ?>
		<?php $dataCat = CHtml::listData(GasMaterials::getCatLevel2(),'id','name','group'); ?>
		<?php echo $form->dropDownList($model,'materials_id', $dataCat,array('class'=>'category_ajax', 'style'=>'width:380px;','empty'=>'Select')); ?>
		
		<?php echo $form->error($model,'materials_id'); ?>
	</div>

	<div class="row">
		<?php echo Yii::t('translation', $form->labelEx($model,'qty')); ?>
		<?php echo $form->textField($model,'qty',array('class'=>'number_only')); ?>
		<?php echo $form->error($model,'qty'); ?>
	</div>

	<div class="row">
		<?php echo Yii::t('translation', $form->labelEx($model,'price_sell')); ?>
		<?php echo $form->textField($model,'price_sell',array('class'=>'number_only','size'=>16,'maxlength'=>14)); ?>
		<?php echo $form->error($model,'price_sell'); ?>
	</div>

	<div class="row">
		<?php echo Yii::t('translation', $form->labelEx($model,'total_sell')); ?>
		<?php echo $form->textField($model,'total_sell',array('class'=>'number_only','size'=>16,'maxlength'=>14)); ?>
		<?php echo $form->error($model,'total_sell'); ?>
	</div>

	<div class="row">
		<?php echo Yii::t('translation', $form->labelEx($model,'price_root')); ?>
		<?php echo $form->textField($model,'price_root',array('class'=>'number_only','size'=>16,'maxlength'=>14)); ?>
		<?php echo $form->error($model,'price_root'); ?>
	</div>

	<div class="row">
		<?php echo Yii::t('translation', $form->labelEx($model,'total_root')); ?>
		<?php echo $form->textField($model,'total_root',array('class'=>'number_only','size'=>16,'maxlength'=>16)); ?>
		<?php echo $form->error($model,'total_root'); ?>
	</div>

	<div class="row">
		<?php echo Yii::t('translation', $form->labelEx($model,'sell_month')); ?>
		<?php echo $form->dropDownList($model,'sell_month', ActiveRecord::getMonthVn(),array('style'=>'width:350px;')); ?>
		<?php echo $form->error($model,'sell_month'); ?>
	</div>

	<div class="row">
		<?php echo Yii::t('translation', $form->labelEx($model,'sell_year')); ?>
		<?php echo $form->textField($model,'sell_year',array('class'=>'number_only','size'=>4,'maxlength'=>4)); ?>
		<?php echo $form->error($model,'sell_year'); ?>
	</div>

	<div class="row buttons" style="padding-left: 115px;">
		        <?php $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType'=>'submit',
            'label'=>$model->isNewRecord ? Yii::t('translation', 'Create') : Yii::t('translation', 'Save'),
            'type'=>'null', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
            'size'=>'small', // null, 'large', 'small' or 'mini'
            //'htmlOptions' => array('style' => 'margin-bottom: 10px; float: right;'),
        )); ?>	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->