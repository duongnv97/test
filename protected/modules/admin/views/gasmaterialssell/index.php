<?php
$this->breadcrumbs=array(
	'Bán Vật Tư',
);

$menus=array(
	array('label'=> Yii::t('translation','Create Bán Vật Tư'), 'url'=>array('create')),
);
$this->menu= ControllerActionsName::createMenusRoles($menus, $actions);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('gas-materials-sell-grid', {
                url : $(this).attr('action'),
		data: $(this).serialize()
	});
	return false;
});
");

Yii::app()->clientScript->registerScript('ajaxupdate', "
$('#gas-materials-sell-grid a.ajaxupdate').live('click', function() {
    $.fn.yiiGridView.update('gas-materials-sell-grid', {
        type: 'POST',
        url: $(this).attr('href'),
        success: function() {
            $.fn.yiiGridView.update('gas-materials-sell-grid');
        }
    });
    return false;
});
");
?>

<h1><?php echo Yii::t('translation', 'Bán Vật Tư'); ?></h1>
<h2>
    <a href="<?php echo Yii::app()->createAbsoluteUrl('admin/gasmaterialssell/import_materialssell');?>">
        Nhập từ file Excel
    </a>
    <?php if(Yii::app()->user->role_id==ROLE_ADMIN): ?>
    <br/>
    <a class="delete_by_month" href="<?php echo Yii::app()->createAbsoluteUrl('admin/gasmaterialssell/delete_by_month');?>">
        Xóa Dữ Liệu
    </a>
    <?php endif;?>
</h2>
<?php echo CHtml::link(Yii::t('translation','Advanced Search'),'#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'gas-materials-sell-grid',
	'dataProvider'=>$model->search(),
        'template'=>'{pager}{summary}{items}{pager}{summary}',     
	'enableSorting' => false,
	//'filter'=>$model,
	'columns'=>array(
        array(
            'header' => 'S/N',
            'type' => 'raw',
            'value' => '$this->grid->dataProvider->pagination->currentPage * $this->grid->dataProvider->pagination->pageSize + ($row+1)',
            'headerHtmlOptions' => array('width' => '30px','style' => 'text-align:center;'),
            'htmlOptions' => array('style' => 'text-align:center;')
        ),
  	
		array(
			'header' => 'Mã Đại Lý',
            'value' => '$data->customer?$data->customer->code_account:""',
            'htmlOptions' => array('style' => 'text-align:left;')
        ),
        array(
			'header' => 'Đại Lý',
            'value' => '$data->customer?$data->customer->first_name:""',
            'htmlOptions' => array('style' => 'text-align:left;')
        ),   	
        array(
			'name' => 'materials_id',
            'value' => '$data->materials?$data->materials->name:""',
            'htmlOptions' => array('style' => 'text-align:left;')
        ),   		

        array(
            'name' => 'qty',
            'htmlOptions' => array('style' => 'text-align:center;')
        ), 				 
        array(
            'name' => 'price_sell',
            'type' => 'Currency',
            'htmlOptions' => array('style' => 'text-align:right;')
        ), 
		        array(
            'name' => 'total_sell',
            'type' => 'Currency',
            'htmlOptions' => array('style' => 'text-align:right;')
        ), 
        array(
            'name' => 'price_root',
            'type' => 'Currency',
            'htmlOptions' => array('style' => 'text-align:right;')
        ), 
        array(
            'name' => 'total_root',
            'type' => 'Currency',
            'htmlOptions' => array('style' => 'text-align:right;')
        ), 		
        array(
            'name' => 'sell_month',
            'htmlOptions' => array('style' => 'text-align:center;')
        ), 	
        array(
            'name' => 'sell_year',
            'htmlOptions' => array('style' => 'text-align:center;')
        ), 			

		array(
			'header' => 'Action',
			'class'=>'CButtonColumn',
                        'template'=> ControllerActionsName::createIndexButtonRoles($actions),
		),
	),
)); ?>


<script>
$(document).ready(function() {
    fnUpdateColorbox();
});

function fnUpdateColorbox(){
    $(".delete_by_month").colorbox({iframe:true,innerHeight:'35%', innerWidth: '45%',close: "<span title='close'>close</span>"});
}
</script>