<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=> Yii::app()->createAbsoluteUrl('admin/gasIssueTickets/index', ['ListUserNotPost'=>1]),
	'method'=>'get',
)); 
$mHrSalaryReports = new HrSalaryReports();
?>
    <div class="row more_col">
        <div class="row">
            <?php echo $form->labelEx($model,'first_name'); ?>
            <?php echo $form->hiddenField($model,'id', array('class'=>'')); ?>
            <?php
                    // 1. limit search kh của sale
                     $url = Yii::app()->createAbsoluteUrl('admin/ajax/search_by_role', array('role'=> implode(',', array_keys($mHrSalaryReports->getArrayRoleSalary()) )));
                    // widget auto complete search user customer and supplier
                    $aData = array(
                        'model'=>$model,
                        'field_customer_id'=>'id',
                        'url'=> $url,
                        'name_relation_user'=>'rCustomer',
                        'ClassAdd' => 'w-350',
                    );
                    $this->widget('ext.SpaAutocompleteCustomer.SpaAutocompleteCustomer',
                        array('data'=>$aData));                                        
                    ?>
            <?php echo $form->error($model,'first_name'); ?>
        </div>
    </div>
    <div class="row more_col">
        <div class="col1">
            <?php echo $form->label($model, 'role_id'); ?>
            <?php echo $form->dropDownList($model, 'role_id', $mHrSalaryReports->getArrayRoleSalary(), array('empty'=>'Select','class' => 'w-200')); ?>
        </div>
        <div class="col2">
            <?php echo $form->label($model,'Số dòng hiển thị'); ?>
            <?php echo $form->dropDownList($model,'pageSize', $model->getArrayPageSize(),array('class'=>'w-200')); ?>
        </div>
    </div>
    <div class="row buttons" style="padding-left: 159px;">
                    <?php $this->widget('bootstrap.widgets.TbButton', array(
        'buttonType'=>'submit',
        'label'=>'Search',
        'type'=>'null', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
        'size'=>'small', // null, 'large', 'small' or 'mini'
        //'htmlOptions' => array('style' => 'margin-bottom: 10px; float: right;'),
    )); ?>	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->