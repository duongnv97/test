<div id="new_ticket" class="in collapse display_none" style="height: auto;">
    <?php $form=$this->beginWidget('CActiveForm', array(
//	'id'=>'new_ticket_form',
	'enableClientValidation' => true,
        'htmlOptions' => array('class' => 'new_ticket','enctype' => 'multipart/form-data'),
        'clientOptions' => array(
            'validateOnSubmit' => true,
//            'afterValidate' => 'js:function(form, attribute, data, hasError){ không nên dùng hàm này nếu ko return true thì ko submit dc  }'
        ),
    )); ?>
    <!--<form method="post" id="" class="">-->        
        <fieldset>
            <div id="ticket_topic_field">
                <!--<input type="text" size="30" placeholder="Subject" name="ticket[topic]" id="ticket_topic" class="">-->
                <div class="row w-350" >
                    <?php // echo $form->labelEx($ModelCreate,'title'); ?>
                    <?php echo $form->textField($ModelCreate,'title',array('class'=>'ticket_topic w-650', 'maxlength'=>250,'placeholder'=>'Tiêu Đề')); ?>
                    <?php echo $form->error($ModelCreate,'title'); ?>
                </div>
            </div>
            <div class="clr"></div>
            <?php include 'index_form_problem.php';?>
            <div class="clr"></div>
            <?php include 'index_auto_search.php';?>
            
            <?php  echo $form->textArea($ModelCreate,'message', array('style'=>'', 'cols'=>40,'placeholder'=>'Nội dung phản ánh sự việc ...')); ?>
            <?php echo $form->error($ModelCreate,'message', array('style'=>'')); ?>
            <?php include 'index_file.php';?>
            <br>
            <input type="submit" value="Create" name="commit" class="button">            
        </fieldset>
    <!--</form>-->
    <?php $this->endWidget(); ?>    
</div><!-- <div id="new_ticket"  -->