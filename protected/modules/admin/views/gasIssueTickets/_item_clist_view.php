<?php $cmsFormater = new CmsFormatter();
    $uidLogin = Yii::app()->user->id;
    $mCustomer = $data->rCustomer;
    $NameCustomer = '';
    if($mCustomer){
        $NameCustomer = $mCustomer->first_name ;
    }
    $color_red = "";
    if($data->expired){
        $color_red = "color_red";
    }
    $mUser = Users::model()->findByPk($uidLogin);
    $data->fromWeb = true;
?>
<li class="ticket">
        <div class="content">
            <h4 class="title ">
                <a class="view collapsed <?php echo $color_red;?>" href="#ticket-<?php echo $data->id;?>"><?php echo $data->getDeadlineOnGrid(); ?> <?php echo $data->getRenewalDateOnGrid(); ?> <?php echo $data->title." - [ {$data->getProblem()} - ".$NameCustomer;?>]</a>
            </h4>
            <?php include '_item_clist_view_action.php';?>
            
            <p class="created_at">
            Issue <strong title="Mã số issue"><?php echo $data->id . ' - ' .$data->code_no;?></strong> 
            - Ngày Tạo <?php echo $cmsFormater->formatDateTime($data->created_date); ?>            
            bởi <span class="label"><?php echo GasTickets::ShowNameReply($data->rUidLogin); ?></span>
            
            <?php if($data->process_status==GasTickets::PROCESS_STATUS_FINISH ): ?>
                <!--- Last reply 1 Day ago by LÀM SAU NẾU CÓ TIME--> 
                - Trả lời vào <?php echo $cmsFormater->formatDateTime($data->process_time); ?> bởi 
                <span class="label"><?php echo GasTickets::ShowNameReply($data->rProcessUserId); ?></span>
            <?php endif;?>

            </p>
        </div><!-- end <div class="content"> -->
        
        <div id="ticket-<?php echo $data->id;?>" class="in collapse display_none" style="">
            <div class="post_reply post_reply<?php echo $data->id;?>" data_class="post_reply<?php echo $data->id;?>">                
                <!--xử lý hiện thông báo cho đại lý biết ai đang xử lý ticket này-->
                <?php if($data->process_status == GasTickets::PROCESS_STATUS_PICK):?>
                <div class="flash alert">
                    <a data-dismiss="alert" class="close" href="javascript:void(0)">&nbsp;</a>
                    Issue này đang được xử lý bởi: <?php echo GasTickets::ShowNameReply($data->rProcessUserId); ?> vào lúc <?php echo $cmsFormater->formatDateTime($data->process_time); ?>
                </div>
                <?php endif;?>
                <!--xử lý hiện thông báo cho đại lý biết ai đang xử lý ticket này-->
                
                <?php
                    // nếu ticket status là finish hoặc là của uid tạo (đại lý) hoặc là người pick xử lý thì cho hiện form reply
//                    if(GasIssueTickets::CanReply($data) && $data->status == GasTickets::STATUS_OPEN && ( Old Close on Dec 21, 2015
//                    if($data->canReplyOnApp($mUser) == 1 && $data->status == GasTickets::STATUS_OPEN && (
//                            $data->process_status == GasTickets::PROCESS_STATUS_FINISH ||
//                            $data->uid_login == $uidLogin ||
////                        ($data->process_status == GasTickets::PROCESS_STATUS_PICK && $data->process_user_id==$uidLogin) ): 
//                            ($data->process_status == GasTickets::PROCESS_STATUS_PICK ) 
//                        )):// Close on Jan 04, 2016, thấy không cần thiết phải pick ticket, ai có nhiệm vụ thì tự vào reply
                    if(1):
                        
                    // chỗ này sẽ không kiếm tra như bên ticket, sẽ cho tất cả các user có quyền trả lời
                    // đã suy nghĩ lại, cứ để phần pick ticket này, và nó sẽ hiện lên thông báo cho người vào trả lời sau để biết thôi
                    // vì lúc đó form reply đã được load ra rồi
                    // chỗ này còn check 1 điều kiện nữa là những user nào được phép reply
                ?>
                    <?php include '_item_clist_reply.php';?>
                <?php  // nếu ticket status là new và không phải của đại lý tạo thì dc phép pick ticket này
                    elseif( $data->status == GasTickets::STATUS_OPEN && $data->process_status == GasTickets::PROCESS_STATUS_NEW &&
                        $data->uid_login != $uidLogin 
                        ):?>
                    <?php if(GasCheck::isAllowAccess('gasIssueTickets', 'pick_ticket')):?>
                        <div class="styled-form">
                            <form method="post" class="" action="<?php echo Yii::app()->createAbsoluteUrl('admin/gasIssueTickets/pick_ticket', array('id'=>$data->id,'GasIssueTickets_page'=> (isset($_GET['GasIssueTickets_page'])?$_GET['GasIssueTickets_page']:1 ))) ?>" accept-charset="UTF-8">
                                <input class="pick_ticket" type="submit" value="Chọn Xử Lý" name="commit<?php echo $data->id;?>" data_msg="Bạn chắc chắn muốn xử lý sự việc này?">
                            </form>
                        </div>
                    <?php endif; ?>
                <?php endif; ?>
            </div>
        
            <?php include "index_item_clist_view_replies.php"; ?>
        </div><!-- end <div id="ticket-->
    </li>