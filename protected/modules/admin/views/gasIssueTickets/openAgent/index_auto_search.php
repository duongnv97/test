<?php $cRole = MyFormat::getCurrentRoleId();
    $urlSearch = Yii::app()->createAbsoluteUrl('admin/ajax/search_all_customer_storecard');
    $placeholder = 'Nhập mã KH/NCC, Số ĐT. Tối thiểu 2 ký tự';
    if($ModelCreate->canCreatePlanOfEmployee()){
        $urlSearch      = $ModelCreate->getUrlSearchEmployee();
        $placeholder    = 'Nhập tên nhân viên xử lý';
    }
?>
<div class="row w-350" >
    <?php // echo $form->textField($ModelCreate,'send_to_id',array('class'=>'ticket_topic', 'size'=>30,'maxlength'=>200,'placeholder'=>'Gửi Cho User Hệ Thống')); ?>    
    <?php echo $form->hiddenField($ModelCreate,'chief_monitor_id', array('class'=>'', 'class_update_val'=>'')); ?>    
    <?php
        // widget auto complete search user customer and supplier
        $aData = array(
            'model'=>$ModelCreate,
            'field_customer_id'=>'chief_monitor_id',
//            'url'=> Yii::app()->createAbsoluteUrl('admin/ajax/search_user_by_code'), Close on Dec 24, 2015
            'url'=> $urlSearch,
            'name_relation_user'=>'rChiefMonitor',
            'ClassAdd'=>'w-350',
            'ClassAddDivWrap'=>'w-650',
            'placeholder'=> $placeholder,
//            'fnSelectCustomer'=>'fnAfterSelectSaleOrAgent',
        );
        $this->widget('ext.SpaAutocompleteCustomer.SpaAutocompleteCustomer',
            array('data'=>$aData));
    ?>
    <?php echo $form->error($ModelCreate,'chief_monitor_id', array('style'=>'')); ?>
</div>




<style>
    .autocomplete_customer_info { margin-left: 0 !important; }
</style>     