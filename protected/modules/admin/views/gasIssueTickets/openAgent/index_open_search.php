<?php echo CHtml::link('Tìm Kiếm Nâng Cao', '#', array('class' => 'searchIssue')); ?>
<div class="collapse hm_issuse_search " style="height: auto; display: none;">
    <?php
    Yii::app()->clientScript->registerCoreScript('jquery.ui');

    $cUrl = Yii::app()->createAbsoluteUrl('admin/gasIssueTickets/index');
    if (isset($_GET['view_type'])) {
        $cUrl = Yii::app()->createAbsoluteUrl('admin/gasIssueTickets/index', ['view_type' => $_GET['view_type']]);
    }
//$model->process_user_id = $model->ext_seach_other_user;// Sep0718 close lai
    $form = $this->beginWidget('CActiveForm', array(
        'action' => Yii::app()->createUrl($this->route),
        'method' => 'post',
    ));
    ?>       
    <div class="row w-200" >
        <?php echo $form->labelEx($model, 'stage_current', array('label' => 'Giai đoạn:','style' => 'margin-left:10px;')); ?>
        <?php echo $form->dropDownList($model, 'stage_current', $model->getArrStage(), array('style' => 'width:200px;margin-left:10px;', 'empty' => 'Select')); ?>
        <?php echo $form->error($model, 'stage_current'); ?>
    </div>
    <div class="clr"></div>
    <div class="row w-200">
        <?php echo $form->labelEx($model, 'expired', array('label' => 'Hết hạn:','style' => 'margin-left:10px;')); ?>
        <?php echo $form->dropDownList($model, 'expired', $model->getArrStatusExpired(), array('style' => 'width:200px;margin-left:10px;', 'empty' => 'Select')); ?>
        <?php echo $form->error($model, 'expired'); ?>
    </div>
    <div class="clr"></div>
    <div class="row">
        <?php // echo $form->label($model,'ext_seach_customer_id'); ?>
        <?php echo $form->hiddenField($model, 'chief_monitor_id'); ?>
        <?php
        // widget auto complete search user customer and supplier
        $aData = array(
            'model' => $model,
            'url' => Yii::app()->createAbsoluteUrl('admin/ajax/search_user_login_not_agent'),
            'name_relation_user' => 'rChiefMonitor',
            'field_autocomplete_name' => 'seach_autocomplete_user_handle',
            'field_customer_id' => 'chief_monitor_id',
            'placeholder' => 'Tìm kiếm theo người xử lý',
            'fnSelectCustomer' => 'fnSelectCustomer',
        );
        $this->widget('ext.SpaAutocompleteCustomer.SpaAutocompleteCustomer', array('data' => $aData));
        ?>
    </div>

    <div class="row">
        <?php // echo $form->label($model,'ext_seach_customer_id'); ?>
        <?php echo $form->hiddenField($model, 'ext_seach_other_user'); ?>
        <?php
        // widget auto complete search user customer and supplier
        $aData = array(
            'model' => $model,
            'url' => Yii::app()->createAbsoluteUrl('admin/ajax/search_user_login_not_agent'),
            'name_relation_user' => 'rProcessUserId',
            'field_autocomplete_name' => 'seach_autocomplete_other_user',
            'field_customer_id' => 'ext_seach_other_user',
            'placeholder' => 'Tìm kiếm người tạo / trả lời',
//                    'placeholder'=>'Tìm Kiếm Theo người xử lý',
            'fnSelectCustomer' => 'fnSelectCustomer',
        );
        $this->widget('ext.SpaAutocompleteCustomer.SpaAutocompleteCustomer', array('data' => $aData));
        ?>
    </div>
    <div class="row ">
        <?php // echo $form->labelEx($ModelCreate,'title');  ?>
        <?php echo $form->textField($model, 'code_no', array('class' => ' w-300 float_l', 'maxlength' => 15, 'placeholder' => 'Mã số issue vd: E1800921 ', 'style' => 'margin-left:10px;')); ?>
        <?php echo $form->error($model, 'code_no'); ?>
    </div>
    <div class="clr"></div>

    <?php $this->endWidget(); ?>

    <script>
        function fnSelectCustomer(user_id, idField, idFieldCustomer) {
//            console.log(idFieldCustomer);
            if ( idFieldCustomer == '#GasIssueTickets_ext_seach_other_user' || idFieldCustomer == '#GasIssueTickets_chief_monitor_id' || idFieldCustomer == '#GasIssueTickets_code_no'
                    || idFieldCustomer == '#GasIssueTickets_stage_current' || idFieldCustomer == '#GasIssueTickets_expired') {
                
                var other_user = $('#GasIssueTickets_ext_seach_other_user').val();
                var user_handle = $('#GasIssueTickets_chief_monitor_id').val();
                var code_no = $('#GasIssueTickets_code_no').val();
                var expired = $('#GasIssueTickets_expired').val();                
                var stage_current = $('#GasIssueTickets_stage_current').val();

                var next = '<?php echo $cUrl; ?>'+"?ext_seach_other_user=" + other_user + "&chief_monitor_id=" + user_handle + "&GasIssueTickets[code_no]=" + code_no + "&expired="+ expired + "&stage_current=" + stage_current;
                $.blockUI({overlayCSS: {backgroundColor: '#fff'}});
                window.location = next;
            }
        }

        $(function () {
            $('.searchIssue').click(function () {
                $('.hm_issuse_search').toggle();
                return false;
            });

            $('body').on('change', '#GasIssueTickets_code_no,#GasIssueTickets_expired,#GasIssueTickets_stage_current', function () {
                var id = $(this).attr('id');
                var selector = '#'+id;
                fnSelectCustomer(0, '', selector);
            });
        });
    </script>
    <style>
        .hm_issuse_search form { padding: 0 !important;}
    </style>
</div>