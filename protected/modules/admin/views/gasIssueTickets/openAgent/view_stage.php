<?php echo MyFormat::BindNotifyMsg(); ?>
<h1>View #<?php echo $model->getTitle(); ?></h1>
<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
            array(
                'name'=>'Tên đại lý',
                'value'=>$model->getTitle()
            ),            
            array(
                'name'=>'Tọa độ',
                'value'=>$model->getGoogleMap()
            ),            
            array(
                'name'=>'Giai đoạn hiện tại',
                'type'=>'raw',
                'value'=>$model->getStageCurrent()
            ),            
            array(
                'name'=>'Trạng thái giai đoạn',
                'value'=>$model->getProcessStatus()==3?'Hoàn thành':'Chưa hoàn thành'
            )     
	
	),
)); ?>
