<?php
$cls_display = 'display_none';
$actionSubmit = false;
if (isset($isProcessNext)):
    $cls_display = '';
    $actionSubmit = Yii::app()->createAbsoluteUrl('admin/gasIssueTickets/reply', ['id' => $id, 'process_next' => 1, 'view_type' => GasIssueTickets::VIEW_TYPE_OPEN_AGENT]);
    ?>
    <link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/admin/css/huongminh_ticket.css" />
    <script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/huongminh_ticket.js"></script>
    <script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/huongminh_issue.js?token=<?php echo time(); ?>"></script>
    <script>
        $(document).ready(function () {           
            fnBindRemoveIcon();
        });
    </script>
    <h1 style="text-align:center">Tạo quy trình tiếp theo </h1>
<?php endif; ?>
<div id="new_ticket" class="in collapse <?php echo $cls_display; ?>" style="height: auto;">
    <?php
    $form = $this->beginWidget('CActiveForm', array(
//	'id'=>'new_ticket_form',
        'action' => $actionSubmit,
        'enableClientValidation' => true,
        'htmlOptions' => array('class' => 'new_ticket', 'enctype' => 'multipart/form-data'),
        'clientOptions' => array(
            'validateOnSubmit' => true,
//            'afterValidate' => 'js:function(form, attribute, data, hasError){ không nên dùng hàm này nếu ko return true thì ko submit dc  }'
        ),
    ));
    ?>
    <!--<form method="post" id="" class="">-->        
    <fieldset>
        <?php
        if (!isset($isProcessNext)):
            ?>
            <div id="ticket_topic_field">
                <!--<input type="text" size="30" placeholder="Subject" name="ticket[topic]" id="ticket_topic" class="">-->
                <div class="row w-350" >
                    <?php // echo $form->labelEx($ModelCreate,'title'); ?>
                    <?php echo $form->textField($ModelCreate, 'title', array('class' => 'ticket_topic w-650', 'maxlength' => 250, 'placeholder' => 'Tên Đại lý')); ?>
                    <?php echo $form->error($ModelCreate, 'title'); ?>
                </div>
                <!--<div class="row w-350" >-->
                    <?php // echo $form->labelEx($ModelCreate,'title'); ?>
                    <?php // echo $form->textField($ModelCreate, 'google_map', array('class' => 'ticket_topic w-650', 'maxlength' => 250, 'placeholder' => 'Vị trí')); ?>
                    <?php // echo $form->error($ModelCreate, 'google_map'); ?>
                <!--</div>-->
            </div>
        <?php endif; ?>
        <div class="clr"></div>
        <?php // include 'index_form_problem.php';?>
        <div class="clr"></div>
        <?php include 'index_auto_search.php'; ?>

        <?php echo $form->textArea($ModelCreate, 'message', array('style' => 'margin-left:0;', 'cols' => 40, 'placeholder' => 'Nội dung phản ánh sự việc ...')); ?>
        <?php echo $form->error($ModelCreate, 'message', array('style' => '')); ?>
        <?php include 'index_file.php'; ?>
        <br>
        <input type="submit" value="Create" name="commit" class="button ">            
    </fieldset>
    <!--</form>-->
    <?php $this->endWidget(); ?>    
</div><!-- <div id="new_ticket"  -->
