<h2 class="section-header closed_tickets">Close Issue</h2>
<?php $this->widget('IssueWidget', array('mIssue'=>$model, 'dataProvider'=> $model->searchClose(), 'needMore' => ['idCListView'=>GasTickets::STATUS_CLOSE])); ?>

<?php  // NGUYEN DUNG Sep0118 move to IssueWidget
//    $this->widget('zii.widgets.CListView', array(
//        'dataProvider' => $model->searchClose(),
//        'itemView' => '_item_clist_view',
//        'id' => 'item_clist_close',
////        'ajaxUpdate'=>false,
//        'afterAjaxUpdate'=>'function(id, data){ fnUpdateColorbox();fnUpdateAjax(); }',
//        'itemsCssClass'=>'',
//        'pagerCssClass'=>'pager',
//        'pager'=> array(
//            'maxButtonCount' => 10,
//            'class'=>'CLinkPager',
//            'header'=> false,
//            'footer'=> false,
//            'id'=>'id-of-pager-ul',
//        ),
//        'summaryText' => true,
//        'summaryText'=>'Showing <strong>{start} - {end}</strong> of {count} results',
//        'template'=>'{pager}{summary}<ul class="tickets clearfix">{items}</ul>{pager}{summary}',
//    ));
?>
