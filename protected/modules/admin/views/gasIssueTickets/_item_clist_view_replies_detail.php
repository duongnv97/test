<?php $cmsFormater = new CmsFormatter();
    $uidLogin = Yii::app()->user->id;
    $mTicket = $data->rIssueTickets;
    $mCustomer = $mTicket->rCustomer;
    
    $NameCustomer = '';
    if($mCustomer){
        $NameCustomer = $mCustomer->first_name ;
    }
    $color_red = "";
    if($mTicket->expired){
        $color_red = "color_red";
    }
    $mUser = Users::model()->findByPk($uidLogin);

?>

<ul class="replies">
    <li class="received reply margin_0">
        <div class="message">
            <?php if($index == 0 && $mTicket->status == GasTickets::STATUS_CLOSE ):?>
                <?php 
                    echo "<b>- Close Issue bởi: </b>".GasTickets::ShowNameReply($mTicket->rCloseUserId)." - ".$cmsFormater->formatDateTime($mTicket->close_date)."<br>";
                ?>
            <?php endif;?>

            <?php if($index == 0 && $mCustomer ): ?>
            <?php
                echo "<b>- Nguyên Nhân: </b>".$mTicket->getProblem();
                echo "<br><b>- NV Kinh Doanh: </b>".$mTicket->getSaleAndPhone();
                echo "<br><b>- Khách Hàng: </b>".$cmsFormater->formatNameUserWithTypeBoMoi($mCustomer);
                echo "<br><b>- Địa Chỉ: </b>".$mCustomer->address;
                echo "<br><b>- Đơn Hàng mới nhất: </b>".$cmsFormater->formatDate($mCustomer->last_purchase)."<br><br>";
            ?>
            <?php endif;?>

            <?php if(!empty($data->move_to_uid)): ?>
                    <?php echo "<br><b>".$data->getMoveToUid()."</b>"; ?>
            <?php endif; ?>
            
            <?php echo nl2br($data->message);?><br>
            
            <?php
                echo $cmsFormater->formatIssueFile($data);
            ?>

            <!--- for delete one detail button -->
            <?php // if(GasCheck::canDeleteData($mTicket) && GasCheck::isAllowAccess('gasIssueTickets', 'DeleteOneDetail')):?>
            <?php if(GasCheck::isAllowAccess('gasIssueTickets', 'DeleteOneDetail')):?>
                <div class="clr"></div>
                <form method="post" class="button_to w-200 padding_0" action="<?php echo Yii::app()->createAbsoluteUrl('admin/gasIssueTickets/deleteOneDetail', array('id'=>$data->id)) ?>">
                    <div class="btn_delete_one_detail">
                        <input type="submit" value="Delete One Detail" class=" w-200 padding_0 padding_5 btn  btn_closed_tickets " title="Delete detail issue này" alert_text="Delete detail issue này?"> 
                    </div>
                </form>
                <div class="clr"></div>
            <?php endif;?> 
            <!--- for delete one detail button -->
            <div class="clr"></div>
            <span class="posted_on">Ngày gửi <?php echo $cmsFormater->formatDateTime($data->created_date); ?></span>
        </div>
        <div class="author">
            <!--<img width="90" height="90" src="https://secure.gravatar.com/avatar/c2da17c95e66c07894e30584b34a9921?default=identicon&amp;secure=true&amp;size=90" alt="Gravatar">-->
            <span class="name item_b"><?php echo GasTickets::ShowNameReplyAtDetailTicket($data->rUidPost, array('mDetailTicket'=>$data)); ?></span>
        </div>
    </li>
</ul>