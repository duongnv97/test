<?php
    $cUid = MyFormat::getCurrentUid();
    $classShow = '';
    if($cUid != GasConst::UID_ADMIN && $ModelCreate->canCreatePlanOfEmployee()){
        $classShow = 'display_none';
    }
?>
<div class="styled-select">
    <div class="row  <?php echo $classShow;?>">
        <?php  echo $form->dropDownList($ModelCreate,'problem', $ModelCreate->getArrayProblem(true), array('class'=>'w-400 problem')); ?>
        <?php echo $form->error($ModelCreate,'problem', array('style'=>'padding-top:70px;')); ?>
    </div>
</div>