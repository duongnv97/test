<?php echo CHtml::link('Tìm Kiếm Nâng Cao','#',array('class'=>'searchIssue')); ?>
<div class="collapse hm_issuse_search " style="height: auto; display: none;">
<?php 
Yii::app()->clientScript->registerCoreScript('jquery.ui');

$cUrl = Yii::app()->createAbsoluteUrl('admin/gasIssueTickets/index');
if(isset($_GET['view_type'])){
    $cUrl = Yii::app()->createAbsoluteUrl('admin/gasIssueTickets/index', ['view_type' => $_GET['view_type']]);
}
//$model->process_user_id = $model->ext_seach_other_user;// Sep0718 close lai
$form=$this->beginWidget('CActiveForm', array(
            'action'=>Yii::app()->createUrl($this->route),
            'method'=>'post',
    )); ?>
        <div class="row">
            <?php // echo $form->label($model,'ext_seach_customer_id'); ?>
            <?php echo $form->hiddenField($model,'ext_seach_customer_id'); ?>
            <?php 
                // widget auto complete search user customer and supplier
                $aData = array(
                    'model'=>$model,
                    'name_relation_user'=>'rCustomer',
                    'field_autocomplete_name'=>'seach_autocomplete_customer',
                    'field_customer_id'=>'ext_seach_customer_id',
                    'placeholder'=>'Tìm kiếm khách hàng: Nhập mã KH/NCC, Số ĐT',
                    'fnSelectCustomer'=>'fnSelectCustomer',
                );
                $this->widget('ext.SpaAutocompleteCustomer.SpaAutocompleteCustomer',
                    array('data'=>$aData));                                        
            ?>
        </div>

        <div class="row display_none">
            <?php // echo $form->label($model,'ext_seach_customer_id'); ?>
            <?php echo $form->hiddenField($model,'ext_seach_user_id'); ?>
            <?php 
                // widget auto complete search user customer and supplier
                $aData = array(
                    'model'=>$model,
                    'url'=> Yii::app()->createAbsoluteUrl('admin/ajax/search_user_login_not_agent'),
                    'name_relation_user'=>'rUidLogin',
                    'field_autocomplete_name'=>'seach_autocomplete_user',
                    'field_customer_id'=>'ext_seach_user_id',
                    'placeholder'=>'Tìm Kiếm Người Tạo',
                    'fnSelectCustomer'=>'fnSelectCustomer',
                );
                $this->widget('ext.SpaAutocompleteCustomer.SpaAutocompleteCustomer',
                    array('data'=>$aData));                                        
            ?>
        </div>

        <div class="row display_none">
            <?php // echo $form->label($model,'ext_seach_customer_id'); ?>
            <?php echo $form->hiddenField($model,'ext_seach_sale_id'); ?>
            <?php 
                // widget auto complete search user customer and supplier
                $aData = array(
                    'model'=>$model,
                    'url'=> Yii::app()->createAbsoluteUrl('admin/ajax/search_user_login_not_agent'),
                    'name_relation_user'=>'rSale',
                    'field_autocomplete_name'=>'seach_autocomplete_sale',
                    'field_customer_id'=>'ext_seach_sale_id',
                    'placeholder'=>'Tìm Kiếm Theo NV Kinh Doanh',
                    'fnSelectCustomer'=>'fnSelectCustomer',
                );
                $this->widget('ext.SpaAutocompleteCustomer.SpaAutocompleteCustomer',
                    array('data'=>$aData));                                        
            ?>
        </div>
    
        <div class="row">
            <?php // echo $form->label($model,'ext_seach_customer_id'); ?>
            <?php echo $form->hiddenField($model,'chief_monitor_id'); ?>
            <?php 
                // widget auto complete search user customer and supplier
                $aData = array(
                    'model'=>$model,
                    'url'=> Yii::app()->createAbsoluteUrl('admin/ajax/search_user_login_not_agent'),
                    'name_relation_user'=>'rChiefMonitor',
                    'field_autocomplete_name'=>'seach_autocomplete_user_handle',
                    'field_customer_id'=>'chief_monitor_id',
                    'placeholder'=>'Tìm kiếm theo người xử lý',
                    'fnSelectCustomer'=>'fnSelectCustomer',
                );
                $this->widget('ext.SpaAutocompleteCustomer.SpaAutocompleteCustomer',
                    array('data'=>$aData));                                        
            ?>
        </div>
    
        <div class="row">
            <?php // echo $form->label($model,'ext_seach_customer_id'); ?>
            <?php echo $form->hiddenField($model,'ext_seach_other_user'); ?>
            <?php 
                // widget auto complete search user customer and supplier
                $aData = array(
                    'model'=>$model,
                    'url'=> Yii::app()->createAbsoluteUrl('admin/ajax/search_user_login_not_agent'),
                    'name_relation_user'=>'rProcessUserId',
                    'field_autocomplete_name'=>'seach_autocomplete_other_user',
                    'field_customer_id'=>'ext_seach_other_user',
                    'placeholder'=>'Tìm kiếm người tạo / trả lời',
//                    'placeholder'=>'Tìm Kiếm Theo người xử lý',
                    'fnSelectCustomer'=>'fnSelectCustomer',
                );
                $this->widget('ext.SpaAutocompleteCustomer.SpaAutocompleteCustomer',
                    array('data'=>$aData));                                        
            ?>
        </div>
        <div class="row ">
            <?php // echo $form->labelEx($ModelCreate,'title'); ?>
            <?php echo $form->textField($model,'code_no',array('class'=>' w-300 float_l', 'maxlength'=>15, 'placeholder'=>'Mã số issue vd: E1800921 ', 'style'=>'margin-left:10px;')); ?>
            <?php echo $form->error($model,'code_no'); ?>
        </div>
    <div class="clr"></div>
    <?php $this->endWidget(); ?>
    
    <script>
        function fnSelectCustomer(user_id, idField, idFieldCustomer){
//            console.log(idFieldCustomer);
            if(idFieldCustomer == '#GasIssueTickets_ext_seach_customer_id' || idFieldCustomer == '#GasIssueTickets_ext_seach_user_id'|| idFieldCustomer == '#GasIssueTickets_ext_seach_sale_id'
                    || idFieldCustomer == '#GasIssueTickets_ext_seach_other_user' || idFieldCustomer == '#GasIssueTickets_chief_monitor_id' || idFieldCustomer == '#GasIssueTickets_code_no'){
                var customer_id = $('#GasIssueTickets_ext_seach_customer_id').val();
                var user_id     = $('#GasIssueTickets_ext_seach_user_id').val();
                var sale_id     = $('#GasIssueTickets_ext_seach_sale_id').val();
                var other_user  = $('#GasIssueTickets_ext_seach_other_user').val();
                var user_handle = $('#GasIssueTickets_chief_monitor_id').val();
                var code_no     = $('#GasIssueTickets_code_no').val();
                
                var next = '<?php echo $cUrl;?>?ext_seach_customer_id='+customer_id+"&ext_seach_user_id="+user_id+"&ext_seach_sale_id="+sale_id+"&ext_seach_other_user="+other_user+"&chief_monitor_id="+user_handle+"&GasIssueTickets[code_no]="+code_no;
                $.blockUI({ overlayCSS: { backgroundColor: '#fff' } });
                window.location = next;
            }
        }
        
        $(function(){
            $('.searchIssue').click(function(){
                $('.hm_issuse_search').toggle();
                return false;
            });
            
            $('body').on('change', '#GasIssueTickets_code_no', function(){
                fnSelectCustomer(0,'', '#GasIssueTickets_code_no');
            });
        });
    </script>
    <style>
        .hm_issuse_search form { padding: 0 !important;}
    </style>
</div>