<?php $cRole = MyFormat::getCurrentRoleId();
    $urlSearch = Yii::app()->createAbsoluteUrl('admin/ajax/search_all_customer_storecard');
    $placeholder = 'Nhập mã KH/NCC, Số ĐT. Tối thiểu 2 ký tự';
?>
<div class="row w-350" >
    <?php // echo $form->textField($ModelCreate,'send_to_id',array('class'=>'ticket_topic', 'size'=>30,'maxlength'=>200,'placeholder'=>'Gửi Cho User Hệ Thống')); ?>    
    <?php echo $form->hiddenField($ModelCreate,'customer_id', array('class'=>'', 'class_update_val'=>'')); ?>    
    <?php
        // widget auto complete search user customer and supplier
        $aData = array(
            'model'=>$ModelCreate,
            'field_customer_id'=>'customer_id',
//            'url'=> Yii::app()->createAbsoluteUrl('admin/ajax/search_user_by_code'), Close on Dec 24, 2015
            'url'=> $urlSearch,
            'name_relation_user'=>'rCustomer',
            'ClassAdd'=>'w-350',
            'ClassAddDivWrap'=>'w-650',
            'placeholder'=> $placeholder,
//            'fnSelectCustomer'=>'fnAfterSelectSaleOrAgent',
        );
        $this->widget('ext.SpaAutocompleteCustomer.SpaAutocompleteCustomer',
            array('data'=>$aData));
    ?>
    <?php echo $form->error($ModelCreate,'customer_id', array('style'=>'')); ?>
</div>

<?php if($ModelCreate->canCreatePlanOfEmployee()): ?>
<div class="row w-350" >
    <?php // echo $form->textField($ModelCreate,'send_to_id',array('class'=>'ticket_topic', 'size'=>30,'maxlength'=>200,'placeholder'=>'Gửi Cho User Hệ Thống')); ?>    
    <?php echo $form->hiddenField($ModelCreate,'chief_monitor_id', array('class'=>'', 'class_update_val'=>'')); ?>    
    <?php
        // widget auto complete search user customer and supplier
        $aData = array(
            'model'=> $ModelCreate,
            'field_customer_id'=>'chief_monitor_id',// field chief_monitor_id là field nhân viên chọn xử lý sự việc 
//            'url'=> Yii::app()->createAbsoluteUrl('admin/ajax/search_user_by_code'), Close on Dec 24, 2015
            'url'=> $ModelCreate->getUrlSearchEmployee(),
            'name_relation_user'=>'rChiefMonitor',
            'ClassAdd'=>'w-350',
            'ClassAddDivWrap'=>'w-650',
            'placeholder'=> 'Nhập tên nhân viên xử lý',
            'field_autocomplete_name'   => 'index_autocomplete_1',
//            'fnSelectCustomer'=>'fnAfterSelectSaleOrAgent',
        );
        $this->widget('ext.SpaAutocompleteCustomer.SpaAutocompleteCustomer',
            array('data'=>$aData));
    ?>
    <?php echo $form->error($ModelCreate,'customer_id', array('style'=>'')); ?>
</div>
<?php endif; ?>

<?php if($ModelCreate->canCreatePlanOfEmployee()): ?>
<div class="row">
     <?php 
        $this->widget('zii.widgets.jui.CJuiDatePicker', array(
            'model'=>$ModelCreate,        
            'attribute'=>'renewal_date',
            'options'=>array(
                'showAnim'=>'fold',
                'dateFormat'=> ActiveRecord::getDateFormatJquery(),
                'minDate'=> '0',
                'changeMonth' => true,
                'changeYear' => true,
                'showOn' => 'button',
                'buttonImage'=> Yii::app()->theme->baseUrl.'/admin/images/icon_calendar_r.gif',
                'buttonImageOnly'=> true,                                
            ),        
            'htmlOptions'=>array(
                'style'=>'display:inline-block;width:150px;',
                'placeholder'=>'Gia hạn đến',
//                'readonly'=>'readonly',
                'id'=>'deadline',
            ),
        ));
        ?>
</div>
<?php endif; ?>


<style>
    .autocomplete_customer_info { margin-left: 0 !important; }
</style>