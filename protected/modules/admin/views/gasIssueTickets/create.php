<?php
$this->breadcrumbs=array(
	'Gas Issue Tickets'=>array('index'),
	'Tạo Mới',
);

$menus = array(		
        array('label'=>'GasIssueTickets Management', 'url'=>array('index')),
);
$this->menu= ControllerActionsName::createMenusRoles($menus, $actions);
?>

<h1>Tạo Mới GasIssueTickets</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>