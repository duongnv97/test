<?php
$this->breadcrumbs=array(
    'Danh sách' => array('gasIssueTickets/index/ViewFullList'),
);

$menus = array(
	array('label'=>" Management", 'url'=>array('index'))
);
$this->menu= ControllerActionsName::createMenusRoles($menus, $actions);
?>

<h1>Xem: <?php echo $model->code_no; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
            array(
                'name'=>'code_no',
                'type'=>'raw',
                'value'=> $model->code_no,
            ),
            array(
                'name'=>'customer_id',
                'type'=>'raw',
                'value'=> $model->getCustomer(),
            ),
            array(
                'label'=>'Tiêu đề',
                'type'=>'raw',
                'value'=> $model->title,
            ),
            array(
                'name'=>'Nguyên nhân',
                'type'=>'raw',
                'value'=> $model->getProblem(),
            ),
            array(
                'name'=>'deadline',
                'type'=>'raw',
                'value'=> $model->getDeadline(),
            ),
            array(
                'name'=>'Người gửi',
                'type'=>'raw',
                'value'=> $model->getUidLoginName(),
            ),
            array(
                'name'=>'Người trả lời',
                'type'=>'raw',
                'value'=> $model->getProcessUserId(),
            ),
            array(
                'name' => 'Nội dung',
                'type' => 'html',
                'value'=> $model->getListMessage(),            
                'htmlOptions' => array('style' => 'width:50px;')
            ),
            
            
	),
)); ?>