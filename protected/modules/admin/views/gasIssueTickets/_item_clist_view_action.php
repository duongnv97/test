<div class="actions">
    <?php if($data->status == GasTickets::STATUS_OPEN ): ?>
        <a class="reply collapsed" href="#ticket-<?php echo $data->id;?>" title="Trả lời issue">Reply</a>
    <?php else:?>
        <a class="view collapsed" href="#ticket-<?php echo $data->id;?>" title="Xem issue">View</a>
    <?php endif;?>
    <!--- for reply button -->

    <!--- for close button -->
    <?php if( $data->canClose() && $data->status == GasTickets::STATUS_OPEN && GasCheck::isAllowAccess('gasIssueTickets', 'close_ticket') ):?>
    <form method="post" class="button_to" action="<?php echo Yii::app()->createAbsoluteUrl('admin/gasIssueTickets/close_ticket', array('id'=>$data->id)) ?>">
        <div>
            <input name="GasCurl" type="hidden" value="<?php echo GasCheck::getCurl();?>">
            <input type="submit" value="Close" class="btn close btn_closed_tickets" title="Chỉ close khi mà vấn đề hỗ trợ này đã được giải quyết xong" alert_text="Chỉ close khi mà vấn đề hỗ trợ này đã được giải quyết xong. Bạn chắc chắn muốn close?">                
        </div>
    </form>
    <?php endif;?>
    <!--- for close button -->

    <!--- for reopen button --> 
    <?php if($data->status == GasTickets::STATUS_CLOSE && GasCheck::isAllowAccess('gasIssueTickets', 'Reopen_ticket')):?>
        <form method="post" class="button_to form_reopen" action="<?php echo Yii::app()->createAbsoluteUrl('admin/gasIssueTickets/reopen_ticket', array('id'=>$data->id)) ?>">
            <div>
                <input type="submit" value="Re Open" class="btn digital_delete btn_closed_tickets" title="Reopen issue này" alert_text="Reopen issue này?"> 
            </div>
        </form>
    <?php endif;?> 
    <!--- for reopen button --> 

    <!--- for delete button --> 
    <?php if(GasCheck::canDeleteData($data)):?>
    <?php // if(1):?>
        <?php if(GasCheck::isAllowAccess('gasIssueTickets', 'delete')):?>
        <form method="post" class="button_to" action="<?php echo Yii::app()->createAbsoluteUrl('admin/gasIssueTickets/delete', array('id'=>$data->id)) ?>">
            <div>
                <input type="submit" value="Delete" class="btn digital_delete btn_closed_tickets" title="Xóa vĩnh viễn issue này" alert_text="Xóa vĩnh viễn issue này?"> 
            </div>
        </form>
        <?php endif;?> 
    <?php endif;?>
    <!--- for delete button --> 

</div>