<div class="row w-350 tb_file" >
    <div>
        <a href="javascript:void(0);" class="text_under_none item_b" style="line-height:25px" onclick="fnBuildRow(this);">
            <img style="float: left;margin-right:8px;" src="<?php echo Yii::app()->theme->baseUrl;?>/images/add.png"> 
            Thêm Dòng
        </a>
    </div>
    <table class="materials_table hm_table">
        <thead>
            <tr>
                <th class="item_c">#</th>
                <th class="item_code item_c"><?php echo $ModelCreate->getAttributeLabel('file_name');?>. Cho phép <?php echo GasFileScanDetail::$AllowFile;?></th>
            </tr>
        </thead>
        <tbody>
            <?php for($i=1; $i<=( GasIssueTickets::IMAGE_MAX_UPLOAD); $i++): ?>
            <?php $display = "";
                if($i > GasIssueTickets::IMAGE_MAX_UPLOAD_SHOW)
                    $display = "display_none";
            ?>
            <tr class="materials_row <?php echo $display;?>">
                <td class="item_c order_no"></td>
                <td class="item_l w-400">
                    <?php echo $form->fileField($ModelCreate,'file_name[]',array('class'=>'input_file',)); ?>
                </td>
                <td class="item_c last"><span class="remove_icon_only"></span></td>
            </tr>
            <?php endfor;?>
        </tbody>
    </table>
    
    <?php // echo $form->labelEx($ModelCreate,'file_name'); ?>
    <?php echo $form->error($ModelCreate,'file_name'); ?>
</div>

<style>
</style>