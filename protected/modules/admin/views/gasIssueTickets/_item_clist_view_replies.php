<ul class="replies">
    <?php foreach($data->rTicketDetail as $key=>$mDetail): ?>
    <li class="received reply margin_0">
        <div class="message">
            <?php if($key==0 && $data->status == GasTickets::STATUS_CLOSE ):?>
                <?php 
                    echo "<b>- Close Issue bởi: </b>".GasTickets::ShowNameReply($data->rCloseUserId)." - ".$cmsFormater->formatDateTime($data->close_date)."<br>";
                ?>
            <?php endif;?>

            <?php if($key==0 && $mCustomer): ?>
            <?php
                $deadline = $data->getDeadline();
                if(!empty($deadline)){
                    $deadline = "<b> - Deadline: $deadline</b>";
                }
                echo "<b>- Nguyên nhân: </b>{$data->getProblem()} $deadline";
                echo "<br><b>- NV kinh doanh: </b>{$data->getSaleAndPhone()}";
                echo "<br><b>- Khách hàng: </b>".$cmsFormater->formatNameUserWithTypeBoMoi($mCustomer);
                echo "<br><b>- Địa chỉ: </b>{$mCustomer->address}";
                echo "<br><b>- Đơn hàng mới nhất: </b>".$cmsFormater->formatDate($mCustomer->last_purchase)."<br><br>";
            ?>
            <?php endif;?>

            
            <?php if(!empty($mDetail->employee_problems_id)): ?>
                <?php echo "<br><b>".$mDetail->getProblemOnGrid()."</b>"; ?>
            <?php endif; ?>
            
            <?php if(!empty($mDetail->move_to_uid)): ?>
                    <?php echo "<br><b>".$mDetail->getMoveToUid()."</b>"; ?>
            <?php endif; ?>
            
            <?php echo $mDetail->getRenewalDateOnGrid();?><br>
            <?php echo nl2br($mDetail->message);?><br>
            
            <?php
                echo $cmsFormater->formatIssueFile($mDetail);
            ?>

            <!--- for delete one detail button -->
            <?php // if(GasCheck::canDeleteData($data) && GasCheck::isAllowAccess('gasIssueTickets', 'DeleteOneDetail')):?>
            <?php if(GasCheck::isAllowAccess('gasIssueTickets', 'DeleteOneDetail')):?>
                <div class="clr"></div>
                <form method="post" class="button_to w-200 padding_0" action="<?php echo Yii::app()->createAbsoluteUrl('admin/gasIssueTickets/deleteOneDetail', array('id'=>$mDetail->id)) ?>">
                    <div class="btn_delete_one_detail">
                        <input type="submit" value="Delete One Detail" class=" w-200 padding_0 padding_5 btn  btn_closed_tickets " title="Delete detail issue này" alert_text="Delete detail issue này?"> 
                    </div>
                </form>
                <div class="clr"></div>
            <?php endif;?> 
            <!--- for delete one detail button -->
            <div class="clr"></div>
            <span class="posted_on">Ngày gửi <?php echo $cmsFormater->formatDateTime($mDetail->created_date); ?></span>
        </div>
        <div class="author">
            <!--<img width="90" height="90" src="https://secure.gravatar.com/avatar/c2da17c95e66c07894e30584b34a9921?default=identicon&amp;secure=true&amp;size=90" alt="Gravatar">-->
            <span class="name item_b"><?php echo $mDetail->uid_post == GasConst::UID_ADMIN ? 'Nguyễn Tiến Dũng<br>[ IT ]' : GasTickets::ShowNameReplyAtDetailTicket($mDetail->rUidPost, array('mDetailTicket'=>$mDetail)); ?></span>
        </div>
    </li>
    <?php endforeach;?>
</ul>