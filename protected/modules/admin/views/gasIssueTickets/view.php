<?php
$this->breadcrumbs=array(
	'Gas Issue Tickets'=>array('index'),
	$model->title,
);

$menus = array(
	array('label'=>'GasIssueTickets Management', 'url'=>array('index')),
	array('label'=>'Tạo Mới GasIssueTickets', 'url'=>array('create')),
	array('label'=>'Cập Nhật GasIssueTickets', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Xóa GasIssueTickets', 'url'=>array('delete'), 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Bạn chắc chắn muốn xóa ?')),
);
$this->menu= ControllerActionsName::createMenusRoles($menus, $actions);
?>

<h1>Xem GasIssueTickets #<?php echo $model->title; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'type',
		'code_no',
		'agent_id',
		'uid_login',
		'title',
		'customer_id',
		'send_to_id',
		'admin_new_message',
		'status',
		'process_status',
		'process_time',
		'process_user_id',
		'created_date',
	),
)); ?>
