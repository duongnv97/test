<?php
$this->breadcrumbs=array(
	'Gas Issue Tickets'=>array('index'),
	$model->title=>array('view','id'=>$model->id),
	'Cập Nhật',
);

$menus = array(	
        array('label'=>'GasIssueTickets Management', 'url'=>array('index')),
	array('label'=>'Xem GasIssueTickets', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Tạo Mới GasIssueTickets', 'url'=>array('create')),	
);
$this->menu= ControllerActionsName::createMenusRoles($menus, $actions);

?>

<h1>Cập Nhật GasIssueTickets <?php echo $model->title; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>