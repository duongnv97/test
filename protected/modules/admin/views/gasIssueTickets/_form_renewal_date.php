<?php if($ModelCreate->showRenewalDate()): ?>
<div class="row">
     <?php 
        $this->widget('zii.widgets.jui.CJuiDatePicker', array(
            'model'=>$ModelCreate,        
            'attribute'=>'renewal_date',
            'options'=>array(
                'showAnim'=>'fold',
                'dateFormat'=> ActiveRecord::getDateFormatJquery(),
                'minDate'=> '0',
                'changeMonth' => true,
                'changeYear' => true,
                'showOn' => 'button',
                'buttonImage'=> Yii::app()->theme->baseUrl.'/admin/images/icon_calendar_r.gif',
                'buttonImageOnly'=> true,                                
            ),        
            'htmlOptions'=>array(
                'style'=>'display:inline-block;width:150px;',
                'placeholder'=>'Ngày gia hạn',
                'readonly'=>'readonly',
                'id'=>'RenewalDate'.$data->id,
            ),
        ));
        ?>  
    <?php echo $form->error($ModelCreate,'renewal_date'); ?>
</div>
<?php endif; ?>