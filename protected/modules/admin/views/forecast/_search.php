<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

    <div class="row">
        <?php echo $form->label($model,'customer_id'); ?>
        <?php echo $form->hiddenField($model,'customer_id'); ?>
        <?php 
            // widget auto complete search user customer and supplier
            $aData = array(
                'model'=>$model,
                'field_customer_id'=>'customer_id',
                'name_relation_user'=>'rCustomer',
                'url' => Yii::app()->createAbsoluteUrl('admin/ajax/search_user_by_code'),
                'ClassAdd' => 'w-500',
            );
            $this->widget('ext.SpaAutocompleteCustomer.SpaAutocompleteCustomer',
                array('data'=>$aData));                                        
        ?>
    </div>

    <div class="row">
        <?php echo Yii::t('translation', $form->label($model,'sale_id')); ?>
                <?php echo $form->hiddenField($model,'sale_id'); ?>
        <?php // echo $form->dropDownList($model,'sale_id', Users::getArrUserByRole(ROLE_SALE, array('order'=>'t.code_bussiness', 'prefix_type_sale'=>1)),array('style'=>'width:500px','empty'=>'Select')); ?>
                <?php 
                    // widget auto complete search user customer and supplier
                    $aData = array(
                        'model'=>$model,
                        'field_customer_id'=>'sale_id',
                        'field_autocomplete_name'=>'autocomplete_name_customer',
                        'url'=> Yii::app()->createAbsoluteUrl('admin/ajax/search_sale'),
                        'name_relation_user'=>'rSale',
                        'placeholder'=>'Nhập Tên Sale',
                        'ClassAdd' => 'w-500',
    //                        'fnSelectCustomer'=>'fnAfterSelectSaleOrAgent',
                    );
                    $this->widget('ext.SpaAutocompleteCustomer.SpaAutocompleteCustomer',
                        array('data'=>$aData));                                        
                ?>
        <?php echo $form->error($model,'sale_id'); ?>
    </div>
    
    <div class="row">
        <?php echo $form->labelEx($model,'agent_id'); ?>
        <?php echo $form->hiddenField($model,'agent_id', array('class'=>'')); ?>
        <?php
            // 1. limit search kh của sale
            $url = Yii::app()->createAbsoluteUrl('admin/ajax/search_by_role', array('role'=> ROLE_AGENT));
            // widget auto complete search user customer and supplier
            $aData = array(
                'model'=>$model,
                'field_customer_id'=>'agent_id',
                'url'=> $url,
                'name_relation_user'=>'rAgent',
                'ClassAdd' => 'w-500',
                'field_autocomplete_name' => 'autocomplete_name_agent',
                'placeholder'=>'Nhập mã hoặc tên đại lý',
            );
            $this->widget('ext.SpaAutocompleteCustomer.SpaAutocompleteCustomer',
                array('data'=>$aData));
            ?>
    </div>
    
    <div class="row more_col">
        <div class="col1">
            <?php echo $form->label($model,'type_customer', array('label'=>'Loại KH')); ?>
            <?php echo $form->dropDownList($model,'type_customer', CmsFormatter::$CUSTOMER_BO_MOI,array('empty'=>'Select', 'class' => 'w-200')); ?>
            <?php echo $form->error($model,'type_customer'); ?>
        </div>	
        <div class="col2">
            <?php echo $form->labelEx($model,'province_id'); ?>
            <?php echo $form->dropDownList($model,'province_id', GasProvince::getArrAll(),array('class'=>'w-200', 'empty'=>'Select')); ?>
        </div>
    </div>

    <div class="row buttons" style="padding-left: 159px;">
        <?php $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType'=>'submit',
            'label'=>Yii::t('translation','Search'),
            'type'=>'null', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
            'size'=>'small', // null, 'large', 'small' or 'mini'
            //'htmlOptions' => array('style' => 'margin-bottom: 10px; float: right;'),
        )); ?>	
    </div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->