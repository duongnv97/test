<?php
$this->breadcrumbs=array(
	$this->singleTitle,
);

$menus=array(
            array('label'=>"Create $this->singleTitle", 'url'=>array('create')),
);
$this->menu= ControllerActionsName::createMenusRoles($menus, $actions);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('forecast-grid', {
                url : $(this).attr('action'),
		data: $(this).serialize()
	});
	return false;
});
");

Yii::app()->clientScript->registerScript('ajaxupdate', "
$('#forecast-grid a.ajaxupdate').live('click', function() {
    $.fn.yiiGridView.update('forecast-grid', {
        type: 'POST',
        url: $(this).attr('href'),
        success: function() {
            $.fn.yiiGridView.update('forecast-grid');
        }
    });
    return false;
});
");

Yii::app()->clientScript->registerScript('updateForecast', "
jQuery('#forecast-grid a.UpdateForecast').live('click',function() {
        var url = $(this).attr('href');
        //  do your post request here
        $.post(url,function(res){
            $.fn.yiiGridView.update('forecast-grid');
        });
        return false;
});
");
?>

<h1>Danh Sách <?php echo $this->pluralTitle; ?></h1>


<?php echo CHtml::link('Tìm Kiếm Nâng Cao','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'forecast-grid',
	'dataProvider'=>$model->search(),
        'afterAjaxUpdate'=>'function(id, data){ fnUpdateColorbox();}',
        'template'=>'{pager}{summary}{items}{pager}{summary}',
//    'itemsCssClass' => 'items custom_here',
//    'htmlOptions'=> array('class'=>'grid-view custom_here'),
        'pager' => array(
            'maxButtonCount'=>  CmsFormatter::$PAGE_MAX_BUTTON,
        ),
	'enableSorting' => false,
	//'filter'=>$model,
	'columns'=>array(
        array(
            'header' => 'S/N',
            'type' => 'raw',
            'value' => '$this->grid->dataProvider->pagination->currentPage * $this->grid->dataProvider->pagination->pageSize + ($row+1)',
            'headerHtmlOptions' => array('width' => '10px','style' => 'text-align:center;'),
            'htmlOptions' => array('style' => 'text-align:center;')
        ),
		array(
                    'name'      => 'customer_id',
                    'type'      => 'raw',
                    'value'     => '$data->getCustomer()'
                ),
                array(
                    'name'      => 'days_per_one',
                    'type'      => 'raw',
                    'value'     => '$data->getDaysPerOne()',
                    'htmlOptions' => array('style' => 'text-align:center;')
                ),
		array(
                    'name'      => 'date_forecast',
                    'type'      => 'raw',
                    'value'     => '$data->getDateForcast()'
                ),
		array(
                    'name'      => 'last_update',
                    'type'      => 'raw',
                    'value'     => '$data->getLastUpdate()'
                ),
                array(
                    'name'      => 'Vượt dự báo',
                    'type'      => 'raw',
                    'value'     => '$data->getDaysExpired(false)',
                    'htmlOptions' => array('style' => 'text-align:right;')
                ),
                array(
                    'name'      => 'Hoá đơn gần nhất',
                    'type'      => 'raw',
                    'value'     => '$data->getViewJsonForecast()',
                    'htmlOptions' => array('width' => '350px;')
                ),
		array(
                    'name'      => 'sale_id',
                    'type'      => 'raw',
                    'value'     => '$data->getSale()'
                ),
		array(
                    'name'      => 'type_customer',
                    'type'      => 'raw',
                    'value'     => '$data->getTypeCustomerText()'
                ),
		array(
                    'name'      => 'agent_id',
                    'type'      => 'raw',
                    'value'     => '$data->getAgent()'
                ),
		array(
                    'header' => 'Actions',
                    'class'=>'CButtonColumn',
                    'template'=> ControllerActionsName::createIndexButtonRoles($actions,['view','sendNotify','sendNotifyRemain','updateForecast']),
                    'buttons'=>array(
                        'sendNotify'=>array(
                            'label'=>'SendNotify',
                            'imageUrl'=>Yii::app()->theme->baseUrl . '/images/add1.png',
                            'options'=>array('class'=>'sendNotify'),
                            'url'=>'Yii::app()->createAbsoluteUrl("admin/forecast/sendNotify", array("id"=>$data->id) )',
                            'visible'=> '$data->canCreateNotify()',
                        ),
                        'sendNotifyRemain'=>array(
                            'label'=>'SendNotifyRemain',
                            'imageUrl'=>Yii::app()->theme->baseUrl . '/images/add2.png',
                            'options'=>array('class'=>'SendNotifyRemain'),
                            'url'=>'Yii::app()->createAbsoluteUrl("admin/forecast/sendNotifyRemain", array("id"=>$data->id) )',
                            'visible'=> '$data->canCreateNotify()',
                        ),
                        'updateForecast'=>array(
                            'label'=>'UpdateForecast',
                            'imageUrl'=>Yii::app()->theme->baseUrl . '/images/bg-select-open.png',
                            'options'=>array('class'=>'UpdateForecast'),
                            'url'=>'Yii::app()->createAbsoluteUrl("admin/forecast/updateForecast", array("id"=>$data->id) )',
                        ),
                    ),
		),
	),
)); ?>

<script>
$(document).ready(function() {
    fnUpdateColorbox();
});

function fnUpdateColorbox(){   
    fixTargetBlank();
}
</script>