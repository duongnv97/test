<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'gas-week-setup-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Dòng có dấu <span class="required">*</span> là bắt buộc.</p>
        
        <?php  if(Yii::app()->user->hasFlash('successUpdate')): ?>
            <div class='success_div'><?php echo Yii::app()->user->getFlash('successUpdate');?></div>         
        <?php endif; ?>  	
            	
	<div class="row">
		<?php echo $form->labelEx($model,'year'); ?>
		<?php echo $form->dropDownList($model,'year', ActiveRecord::getRangeYear(), array('style'=>'')); ?>
		<?php echo $form->error($model,'year'); ?>
	</div>
	<div class="row">
		<?php echo $form->labelEx($model,'month'); ?>
		<?php echo $form->dropDownList($model,'month', ActiveRecord::getMonthVnWithZeroFirst(), array('class'=>'', 'empty'=>'Select')); ?>
		<?php echo $form->error($model,'month'); ?>
	</div>
            
        <?php if($model->isNewRecord):?>    
        <div class="row">
            <label>&nbsp</label>
            <table class="materials_table hm_table">
                <thead>
                    <tr>
                        <th class="item_c">Tuần Số</th>
                        <th class=" item_c">Từ Ngày</th>
                        <th class="last item_c">Đến Ngày</th>
                    </tr>
                </thead>
                <tbody>
                    <?php for($i=1; $i<7; $i++): ?>
                    <tr>
                        <td class="item_c"><?php echo $i;?></td>
                        <td>
                            <?php 
                                $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                                    'model'=>$model,        
                                    'attribute'=>'date_from[]',
                                    'options'=>array(
                                        'showAnim'=>'fold',
                                        'dateFormat'=> ActiveRecord::getDateFormatJquery(),
//                                        'minDate'=> '0',
//                                        'maxDate'=> '0',
                                        'changeMonth' => true,
                                        'changeYear' => true,
                                        'showOn' => 'button',
                                        'buttonImage'=> Yii::app()->theme->baseUrl.'/admin/images/icon_calendar_r.gif',
                                        'buttonImageOnly'=> true,                                
                                    ),        
                                    'htmlOptions'=>array(
                                        'class'=>'w-16',
                                        'id'=>"date_from$i",
                                        'style'=>'height:20px;',
                                    ),
                                ));
                            ?>
                        </td>
                        <td class="last">
                            <?php 
                                $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                                    'model'=>$model,        
                                    'attribute'=>'date_to[]',
                                    'options'=>array(
                                        'showAnim'=>'fold',
                                        'dateFormat'=> ActiveRecord::getDateFormatJquery(),
//                                        'minDate'=> '0',
//                                        'maxDate'=> '0',
                                        'changeMonth' => true,
                                        'changeYear' => true,
                                        'showOn' => 'button',
                                        'buttonImage'=> Yii::app()->theme->baseUrl.'/admin/images/icon_calendar_r.gif',
                                        'buttonImageOnly'=> true,                                
                                    ),        
                                    'htmlOptions'=>array(
                                        'class'=>'w-16',
                                        'id'=>"date_to$i",
                                        'style'=>'height:20px;',
                                    ),
                                ));
                            ?>
                        </td>
                    </tr>
                    <?php endfor;?>
                    
                </tbody>
            </table>
        </div>
            
        <?php else:?>
            
        <div class="row">
            <?php echo $form->labelEx($model,'date_from'); ?>
            <?php 
                    $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                        'model'=>$model,        
                        'attribute'=>'date_from',
                        'options'=>array(
                            'showAnim'=>'fold',
                            'dateFormat'=> ActiveRecord::getDateFormatJquery(),
                            'changeMonth' => true,
                            'changeYear' => true,
                            'showOn' => 'button',
                            'buttonImage'=> Yii::app()->theme->baseUrl.'/admin/images/icon_calendar_r.gif',
                            'buttonImageOnly'=> true,                                
                        ),        
                        'htmlOptions'=>array(
                            'class'=>'w-16',
                            'style'=>'height:20px;',
                        ),
                    ));
                ?>
            <?php echo $form->error($model,'date_from'); ?>
	</div>            
        <div class="row">
            <?php echo $form->labelEx($model,'date_to'); ?>
            <?php 
                    $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                        'model'=>$model,        
                        'attribute'=>'date_to',
                        'options'=>array(
                            'showAnim'=>'fold',
                            'dateFormat'=> ActiveRecord::getDateFormatJquery(),
                            'changeMonth' => true,
                            'changeYear' => true,
                            'showOn' => 'button',
                            'buttonImage'=> Yii::app()->theme->baseUrl.'/admin/images/icon_calendar_r.gif',
                            'buttonImageOnly'=> true,                                
                        ),        
                        'htmlOptions'=>array(
                            'class'=>'w-16',
                            'style'=>'height:20px;',
                        ),
                    ));
                ?>
            <?php echo $form->error($model,'date_to'); ?>
	</div>            
        <?php endif;?>    
            
	<div class="row buttons" style="padding-left: 141px;">
            <?php $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType'=>'submit',
            'label'=>$model->isNewRecord ? 'Create' :'Save',
            'type'=>'null', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
            'size'=>'small', // null, 'large', 'small' or 'mini'
            //'htmlOptions' => array('style' => 'margin-bottom: 10px; float: right;'),
        )); ?>	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->
<script>
    $(document).ready(function(){
        $('.form').find('button:submit').click(function(){
            $.blockUI({ overlayCSS: { backgroundColor: '#fff' } }); 
        });
        
    });
</script>