<?php
$this->breadcrumbs=array(
	'Quản Lý Tuần Của Tháng'=>array('index'),
	$model->id,
);

$menus = array(
	array('label'=>'Quản Lý Tuần Của Tháng', 'url'=>array('index')),
	array('label'=>'Tạo Mới  Tuần Của Tháng', 'url'=>array('create')),
	array('label'=>'Cập Nhật Tuần Của Tháng', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Xóa Tuần Của Tháng', 'url'=>array('delete'), 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Bạn chắc chắn muốn xóa ?')),
);
$this->menu= ControllerActionsName::createMenusRoles($menus, $actions);
?>

<h1>Xem Tuần <?php echo $model->number_week.""; ?> Của Tháng: <?php echo $model->month." - $model->year"; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'year',
		'month',
		'number_week',
		'date_from',
		'date_to',
	),
)); ?>
