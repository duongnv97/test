<?php
$this->breadcrumbs=array(
	'Quản Lý Tuần Của Tháng'=>array('index'),
	'Tạo Mới',
);

$menus = array(
        array('label'=>'Quản Lý Tuần Của Tháng', 'url'=>array('index')),
);
$this->menu= ControllerActionsName::createMenusRoles($menus, $actions);
?>

<h1>Tạo Mới Tuần Của Tháng</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>