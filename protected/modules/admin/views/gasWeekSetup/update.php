<?php
$this->breadcrumbs=array(
	'Quản Lý Tuần Của Tháng'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Cập Nhật',
);

$menus = array(	
        array('label'=>'Quản Lý Tuần Của Tháng', 'url'=>array('index')),
	array('label'=>'Xem Tuần Của Tháng', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Tạo Mới Tuần Của Tháng', 'url'=>array('create')),	
);
$this->menu= ControllerActionsName::createMenusRoles($menus, $actions);

?>

<h1>Cập Nhật Tuần <?php echo $model->number_week.""; ?> Của Tháng: <?php echo $model->month." - $model->year"; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>