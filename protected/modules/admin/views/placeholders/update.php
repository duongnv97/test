<?php
$this->breadcrumbs=array(
	'Place Holders'=>array('index'),
	'Update',
);

$menus=array(
	array('label'=>'List PlaceHolders', 'url'=>array('index')),
	array('label'=>'Create PlaceHolders', 'url'=>array('create')),
);
$this->menu= ControllerActionsName::createMenusRoles($menus, $actions);
?>

<h1>Update PlaceHolders: <?php echo $model->position; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>