<?php
$this->breadcrumbs=array(
	'Place Holders'=>array('index'),
	'Create',
);

$menus=array(
	array('label'=>'List PlaceHolders', 'url'=>array('index')),
);
$this->menu= ControllerActionsName::createMenusRoles($menus, $actions);
?>

<h1>Create PlaceHolders</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>