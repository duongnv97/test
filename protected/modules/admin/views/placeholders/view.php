<?php
$this->breadcrumbs=array(
	'Place Holders'=>array('index'),
	$model->position,
);

$menus=array(
	array('label'=>'List PlaceHolders', 'url'=>array('index')),
	array('label'=>'Create PlaceHolders', 'url'=>array('create')),
	array('label'=>'Update PlaceHolders', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete PlaceHolders', 'url'=>array('delete'), 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
);
$this->menu= ControllerActionsName::createMenusRoles($menus, $actions);
?>

<h1>View PlaceHolders: <?php echo $model->position; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		//'id',
		'position',
	),
)); ?>
