<div class="form">
    <?php $form=$this->beginWidget('CActiveForm', array(
        'id'=>'homecontract-form',
        'enableAjaxValidation'=>false,
               'htmlOptions' => array('enctype' => 'multipart/form-data'),
        )); ?>
    <p class="note">Dòng có dấu <span class="required">*</span> là bắt buộc.</p>
    <?php echo MyFormat::BindNotifyMsg(); ?>

    <!--thông tin đại lý-->
    <div class="row ">
    <?php echo $form->labelEx($model,'agent_id'); ?>
    <?php echo $form->hiddenField($model,'agent_id'); ?>
    <?php
        // 1. limit search kh của sale
        $url = Yii::app()->createAbsoluteUrl('admin/ajax/search_by_role', array('role'=> ROLE_AGENT, 'GetAll'=>1));
        // widget auto complete search user customer and supplier
        $aData = array(
            'model'=>$model,
            'field_customer_id'=>'agent_id',
            'url'=> $url,
            'name_relation_user'=>'rAgent',
            'ClassAdd' => 'w-300',
            'field_autocomplete_name' => 'autocomplete_agent', // tên biến truyền vào
            'placeholder'=>'Nhập mã hoặc tên đại lý',
        );
        $this->widget('ext.SpaAutocompleteCustomer.SpaAutocompleteCustomer',
            array('data'=>$aData));
        ?>
    <?php echo $form->error($model,'agent_id'); ?>
    </div>
    
    <div class="row">
        <?php echo $form->labelEx($model,'start_contract'); ?>
         <?php 
            $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                'model'=>$model,        
                'attribute'=>'start_contract',
                'options'=>array(
                    'showAnim'=>'fold',
                    'dateFormat'=> MyFormat::$dateFormatSearch,
            //                            'minDate'=> '0',
            //                            'maxDate'=> '0',
                    'changeMonth' => true,
                    'changeYear' => true,
                    'showOn' => 'button',
                    'buttonImage'=> Yii::app()->theme->baseUrl.'/admin/images/icon_calendar_r.gif',
                    'buttonImageOnly'=> true,                                
                ),        
                'htmlOptions'=>array(
                    'class'=>'w-16',
                    'style'=>'height:20px;',
                        'readonly'=>'readonly',
                ),
            ));
            ?>  
        <?php echo $form->error($model,'start_contract'); ?>
    </div>
    
    <div class="row">
        <?php echo $form->labelEx($model,'end_contract'); ?>
         <?php 
            $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                'model'=>$model,        
                'attribute'=>'end_contract',
                'options'=>array(
                    'showAnim'=>'fold',
                    'dateFormat'=> MyFormat::$dateFormatSearch,
            //                            'minDate'=> '0',
            //                            'maxDate'=> '0',
                    'changeMonth' => true,
                    'changeYear' => true,
                    'showOn' => 'button',
                    'buttonImage'=> Yii::app()->theme->baseUrl.'/admin/images/icon_calendar_r.gif',
                    'buttonImageOnly'=> true,                                
                ),        
                'htmlOptions'=>array(
                    'class'=>'w-16',
                    'style'=>'height:20px;',
                        'readonly'=>'readonly',
                ),
            ));
            ?>  
        <?php echo $form->error($model,'end_contract'); ?>
    </div>
    
    <div class="row">
        <?php echo $form->labelEx($model,'next_date_pay'); ?>
         <?php 
            $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                'model'=>$model,        
                'attribute'=>'next_date_pay',
                'options'=>array(
                    'showAnim'=>'fold',
                    'dateFormat'=> MyFormat::$dateFormatSearch,
            //                            'minDate'=> '0',
            //                            'maxDate'=> '0',
                    'changeMonth' => true,
                    'changeYear' => true,
                    'showOn' => 'button',
                    'buttonImage'=> Yii::app()->theme->baseUrl.'/admin/images/icon_calendar_r.gif',
                    'buttonImageOnly'=> true,                                
                ),        
                'htmlOptions'=>array(
                    'class'=>'w-16',
                    'style'=>'height:20px;',
                        'readonly'=>'readonly',
                ),
            ));
            ?>  
        <?php echo $form->error($model,'next_date_pay'); ?>
    </div>
    
    <!--trạng thái hợp đồng-->
    <div class="row GasCheckboxList" style=''>
    <?php echo $form->labelEx($model,'status'); ?>
    <?php echo $form->radioButtonList($model,'status', $model->getStatusContract(), 
                array(
                    'separator'=>"",
                    'template'=>'<li>{input}{label}</li>',
                    'container'=>'ul',
                    'class'=>'RadioStatusContract' 
                )); 
    ?>
    <div class="clr"></div>
    <?php $showDateExpired = $model->status == $model->Active_Status ? 'display_none': '' ; ?>
    <div class="rowDetailStatus row <?php echo $showDateExpired; ?>">
        <?php echo $form->labelEx($model,'date_expired'); ?>
         <?php

            $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                'model'=>$model,        
                'attribute'=>'date_expired',
                'options'=>array(
                    'showAnim'=>'fold',
                    'dateFormat'=> MyFormat::$dateFormatSearch,
            //                            'minDate'=> '0',
            //                            'maxDate'=> '0',
                    'changeMonth' => true,
                    'changeYear' => true,
                    'showOn' => 'button',
                    'buttonImage'=> Yii::app()->theme->baseUrl.'/admin/images/icon_calendar_r.gif',
                    'buttonImageOnly'=> true,                                
                ),        
                'htmlOptions'=>array(
                    'class'=>'w-16',
                    'style'=>'height:20px;',
                ),
            ));
            ?>  
        <?php echo $form->error($model,'date_expired'); ?>
    </div>
    </div>
    
    <div class="clr"></div>
    <div class="row">
    <?php echo $form->labelEx($model,'deposit'); ?>
    <?php echo $form->textField($model,'deposit',array('class'=>'w-300 number_only ad_fix_currency',)); ?>
    <?php echo $form->error($model,'deposit'); ?>
    </div>
    
    <div class="row">
    <?php echo $form->labelEx($model,'amount_month_pay'); ?>
    <?php echo $form->dropDownList($model,'amount_month_pay', $model->getArrayAmountMonthPay(),array('empty'=>'Select','class'=>'w-300')); ?>
    <?php echo $form->error($model,'amount_month_pay'); ?>
    </div>
    
    <!--hinh thức thanh toán-->
    <div class="wrapTypePay ">
        <div class="row GasCheckboxList" style=''>
        <?php echo $form->labelEx($model,'type_pay'); ?>
        <?php echo $form->radioButtonList($model,'type_pay', $model->getArrayTypePay(), 
                    array(
                        'separator'=>"",
                        'template'=>'<li>{input}{label}</li>',
                        'container'=>'ul',
                        'class'=>'RadioPayContractType' 
                    )); 
        ?>
        <?php echo $form->error($model,'type_pay'); ?>
        </div>
        <div class="clr"></div>
        <div class="content-wrapTypePay">
            <div class="row">
                <?php echo $form->labelEx($model,'pay_beneficiary'); ?>
                <?php echo $form->textField($model,'pay_beneficiary',array('class'=>'w-300 item_l',)); ?>
                <?php echo $form->error($model,'pay_beneficiary'); ?>
            </div>
            <div class="row">
                <?php echo $form->labelEx($model,'pay_numberPhone'); ?>
                <?php echo $form->textField($model,'pay_numberPhone',array('class'=>'w-300 number_only_v1',)); ?>
                <?php echo $form->error($model,'pay_numberPhone'); ?>
            </div>
            <?php
                $displayTransfer =$model->type_pay == GasSettle::TYPE_BANK_TRANSFER_MULTI ? '' : 'display_none';
            ?>
            <div class="row <?php echo $displayTransfer . ' fieldTransfer';?>">
                <?php echo $form->labelEx($model,'pay_bank'); ?>
                <?php echo $form->textField($model,'pay_bank',array('class'=>'w-300 item_l',)); ?>
                <?php echo $form->error($model,'pay_bank'); ?>
            </div>
            <div class="row <?php echo $displayTransfer . ' fieldTransfer';?>">
                <?php echo $form->labelEx($model,'pay_bankNumber'); ?>
                <?php echo $form->textField($model,'pay_bankNumber',array('class'=>'w-300 item_l',)); ?>
                <?php echo $form->error($model,'pay_bankNumber'); ?>
            </div>
        </div>
    </div>
    
    <div class="row">
            <?php echo $form->labelEx($model,'note'); ?>
            <?php echo $form->textArea($model,'note',array('style'=>'width:300px;height:40px;')); ?>
            <?php echo $form->error($model,'note'); ?>
    </div>
    
    <?php include '_amounts.php'; ?>
    
    <div class="row buttons" style="padding-left: 141px;">
        <?php $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType'=>'submit',
            'label'=>$model->isNewRecord ? 'Create' :'Save',
            'type'=>'null', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
            'size'=>'small', // null, 'large', 'small' or 'mini'
            //'htmlOptions' => array('style' => 'margin-bottom: 10px; float: right;'),
            )); ?>
    </div>
    <?php $this->endWidget(); ?>
</div>
<script>
    $(function(){
        bindTypePayContract();
        bindStatusContract();
    });
    
    function bindTypePayContract(){
        $('.RadioPayContractType').on('click',function(){
            $valuePayType = $(this).val();
            switch($valuePayType){
                case '<?php echo GasSettle::TYPE_BANK_TRANSFER_MULTI; ?>':
                    $('.fieldTransfer').removeClass('display_none');        
                    break;
                default://mật định là đề nghị thanh toán
                    $('.fieldTransfer').addClass('display_none');        
                    break;
            }
        })
    }
    function bindStatusContract(){
        $('.RadioStatusContract').on('click',function(){
            $valuePayType = $(this).val();
            switch($valuePayType){
                case '<?php echo $model->Expired_Status; ?>':
                    $('.rowDetailStatus').removeClass('display_none');        
                    break;
                default://mật định là đề nghị thanh toán
                    $('.rowDetailStatus').addClass('display_none');
                    $('#HomeContract_date_expired').val('');
                    break;
            }
        })
    }
</script>