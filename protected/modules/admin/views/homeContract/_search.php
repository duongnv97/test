<?php $urlExcel = Yii::app()->createAbsoluteUrl('admin/homeContract',['ExportExcel'=>1]); ?>
<div class="wide form">
<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=> Yii::app()->createUrl($this->route),
	'method'=>'get',
)); 

?>  
    <div class="row">
        <?php echo $form->labelEx($model,'start_contract'); ?>
        <div style="display: inline-block">
        <?php echo $form->labelEx($model,'Từ ngày',array('class'=>'w-80')); ?>
        <?php 
            $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                'model'=>$model,        
                'attribute'=>'start_contract[start]',
                'options'=>array(
                    'showAnim'=>'fold',
                    'dateFormat'=> MyFormat::$dateFormatSearch,
                    'changeMonth' => true,
                    'changeYear' => true,
                    'showOn' => 'button',
                    'buttonImage'=> Yii::app()->theme->baseUrl.'/admin/images/icon_calendar_r.gif',
                    'buttonImageOnly'=> true,                                
                ),        
                'htmlOptions'=>array(
                    'class'=>'w-16',
                    'style'=>'height:20px;',
                ),
            ));
        ?>
        </div>
        <div style="display: inline-block">
        <?php echo $form->labelEx($model,'Đến ngày',array('class'=>'w-80')); ?>
        <?php 
            $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                'model'=>$model,        
                'attribute'=>'start_contract[end]',
                'options'=>array(
                    'showAnim'=>'fold',
                    'dateFormat'=> MyFormat::$dateFormatSearch,
                    'changeMonth' => true,
                    'changeYear' => true,
                    'showOn' => 'button',
                    'buttonImage'=> Yii::app()->theme->baseUrl.'/admin/images/icon_calendar_r.gif',
                    'buttonImageOnly'=> true,                                
                ),        
                'htmlOptions'=>array(
                    'class'=>'w-16',
                    'style'=>'height:20px;',
                ),
            ));
        ?>
        </div>
        <?php echo $form->error($model,'date_expired'); ?>
    </div>
    
    <div class="row">
        <?php echo $form->labelEx($model,'end_contract'); ?>
        <div style="display: inline-block">
        <?php echo $form->labelEx($model,'Từ ngày',array('class'=>'w-80')); ?>
        <?php 
            $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                'model'=>$model,        
                'attribute'=>'end_contract[start]',
                'options'=>array(
                    'showAnim'=>'fold',
                    'dateFormat'=> MyFormat::$dateFormatSearch,
                    'changeMonth' => true,
                    'changeYear' => true,
                    'showOn' => 'button',
                    'buttonImage'=> Yii::app()->theme->baseUrl.'/admin/images/icon_calendar_r.gif',
                    'buttonImageOnly'=> true,                                
                ),        
                'htmlOptions'=>array(
                    'class'=>'w-16',
                    'style'=>'height:20px;',
                ),
            ));
        ?>
        </div>
        <div style="display: inline-block">
        <?php echo $form->labelEx($model,'Đến ngày',array('class'=>'w-80')); ?>
        <?php 
            $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                'model'=>$model,        
                'attribute'=>'end_contract[end]',
                'options'=>array(
                    'showAnim'=>'fold',
                    'dateFormat'=> MyFormat::$dateFormatSearch,
                    'changeMonth' => true,
                    'changeYear' => true,
                    'showOn' => 'button',
                    'buttonImage'=> Yii::app()->theme->baseUrl.'/admin/images/icon_calendar_r.gif',
                    'buttonImageOnly'=> true,                                
                ),        
                'htmlOptions'=>array(
                    'class'=>'w-16',
                    'style'=>'height:20px;',
                ),
            ));
        ?>
        </div>
        <?php echo $form->error($model,'date_expired'); ?>
    </div>
    <!--thông tin đại lý-->
    <div class="row ">
    <?php echo $form->labelEx($model,'agent_id'); ?>
    <?php echo $form->hiddenField($model,'agent_id'); ?>
    <?php
        // 1. limit search kh của sale
//        $url = Yii::app()->createAbsoluteUrl('admin/ajax/search_by_role', array('role'=> ROLE_AGENT));
        $url = Yii::app()->createAbsoluteUrl('admin/ajax/search_dropdown_agent');
        // widget auto complete search user customer and supplier
        $aData = array(
            'model'=>$model,
            'field_customer_id'=>'agent_id',
            'url'=> $url,
            'name_relation_user'=>'rAgent',
            'ClassAdd' => 'w-300',
            'field_autocomplete_name' => 'autocomplete_agent', // tên biến truyền vào
            'placeholder'=>'Nhập mã hoặc tên đại lý',
        );
        $this->widget('ext.SpaAutocompleteCustomer.SpaAutocompleteCustomer',
            array('data'=>$aData));
        ?>
    </div>
    
    <!--trạng thái hợp đồng-->
    <div class="row more_col">
        <div class="col1 ">
            <?php echo $form->labelEx($model,'status'); ?>
            <?php echo $form->dropDownList($model,'status', $model->getStatusContract(),array('empty'=>'Select','class'=>'w-200')); ?>
        </div>
        <div class="col2 ">
            <?php echo $form->labelEx($model,'type_pay'); ?>
            <?php echo $form->dropDownList($model,'type_pay', $model->getArrayTypePay(),array('empty'=>'Select','class'=>'w-200')); ?>
        </div>
    </div>
    
    <div class="row">
        <?php echo $form->labelEx($model,'province_id'); ?>
        <div class="fix-label">
            <?php
               $this->widget('ext.multiselect.JMultiSelect',array(
                     'model'=>$model,
                     'attribute'=>'province_id',
                     'data'=> GasProvince::getArrAll(),
                     // additional javascript options for the MultiSelect plugin
                    'options'=>array('selectedList' => 30,),
                     // additional style
                     'htmlOptions'=>array('style' => 'width: 800px;'),
               ));    
           ?>
        </div>
    </div>
    
    <div class="row buttons" style="padding-left: 159px;">
        <?php $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType'=>'submit',
            'label'=>'Search',
            'type'=>'null', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
            'size'=>'small', // null, 'large', 'small' or 'mini'
            //'htmlOptions' => array('style' => 'margin-bottom: 10px; float: right;'),
        )); ?>	
        &nbsp;&nbsp;&nbsp;&nbsp;<a class='btn_cancel' href='<?php echo $urlExcel;?>'>Xuất Excel</a>
    </div>
<?php $this->endWidget(); ?>

</div><!-- search-form -->