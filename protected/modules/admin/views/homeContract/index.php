<?php
$this->breadcrumbs=array(
	$this->singleTitle,
);
$menus=array(
            array('label'=>"Create $this->singleTitle", 'url'=>array('create')),
);
$this->menu= ControllerActionsName::createMenusRoles($menus, $actions);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('xorder-grid', {
                url : $(this).attr('action'),
		data: $(this).serialize()
	});
	return false;
});
");
Yii::app()->clientScript->registerScript('ajaxupdate', "
$('#sell-grid a.ajaxupdate').live('click', function() {
    $.fn.yiiGridView.update('sell-grid', {
        type: 'POST',
        url: $(this).attr('href'),
        success: function() {
            $.fn.yiiGridView.update('sell-grid');
        }
    });
    return false;
});
");
?>
<h1>Danh Sách <?php echo $this->pluralTitle; ?></h1>

<?php echo CHtml::link('Tìm Kiếm Nâng Cao','#',array('class'=>'search-button')); ?>
<div class="search-form display_none" style="">
<?php $this->renderPartial('_search',array(
        'model'=>$model,
)); ?>
</div>

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'xorder-grid',
	'dataProvider'=>$model->search(),
        'afterAjaxUpdate'=>'function(id, data){ fnUpdateColorbox();}',
        'template'=>'{pager}{summary}{items}{pager}{summary}',
//    'itemsCssClass' => 'items custom_here',
//    'htmlOptions'=> array('class'=>'grid-view custom_here'),
        'pager' => array(
            'maxButtonCount'=>  CmsFormatter::$PAGE_MAX_BUTTON,
        ),
	'enableSorting' => true,
	//'filter'=>$model,
	'columns'=>array(
            array(
                'header' => 'S/N',
                'type' => 'raw',
                'value' => '$this->grid->dataProvider->pagination->currentPage * $this->grid->dataProvider->pagination->pageSize + ($row+1)',
                'headerHtmlOptions' => array('width' => '10px','style' => 'text-align:center;'),
                'htmlOptions' => array('style' => 'text-align:center;')
            ),
            array(
                    'name' =>'status',
                    'type'=>'raw',
                    'value' => '$data->getStatus()',
                ),
            array(
                    'name' =>'agent_id',
                    'type'=>'raw',
                    'value' => '$data->getAgent()',
                ),
            array(
                    'name' =>'deposit',
                    'type'=>'raw',
                    'value' => '$data->getDeposit()',
                    'htmlOptions' => array('style' => 'text-align:center;')

                ),
            array(
                    'name' =>'prices_contract',
                    'type'=>'raw',
                    'value' => '$data->getAmountContract()',
                    'htmlOptions' => array('style' => 'width:200px;')
                ),
            array(
                    'name' =>'amount_month_pay',
                    'type'=>'raw',
                    'value' => '$data->getAmountPay()',
                    'htmlOptions' => array('style' => 'text-align:center;')
                ),
            array(
                    'name' =>'next_date_pay',
                    'type'=>'raw',
                    'value' => '$data->getNextDatePay()',
                    'htmlOptions' => array('style' => 'text-align:center;width:80px;')
                ),
            array(
                    'name' =>'start_contract',
                    'type'=>'raw',
                    'value' => '$data->getStartDateContract()',
                    'htmlOptions' => array('style' => 'text-align:center;width:80px;')
                ),
            array(
                    'name' =>'end_contract',
                    'type'=>'raw',
                    'value' => '$data->getsEndDateContract()',
                    'htmlOptions' => array('style' => 'text-align:center;width:80px;')
                ),
            array(
                    'name' =>'type_pay',
                    'type'=>'raw',
                    'value' => '$data->getTypePay()',
                ),
            array(
                    'name' =>'date_expired',
                    'type'=>'raw',
                    'value' => '$data->getExpiredDatePay()',
                    'htmlOptions' => array('style' => 'text-align:center;width:80px;')
                ),
            array(
                    'name' =>'note',
                    'type'=>'raw',
                    'value' => '$data->getNote()',
                ),
            array(
                'name'  => 'created_date',
                'value'  => '$data->getCreatedDate()',
            ),
            array(
                'header' => 'Actions',
                'class'=>'CButtonColumn',
                'template'=> ControllerActionsName::createIndexButtonRoles($actions),
                'buttons'=>array(
                    'update'=>array(
                        'visible'=> '$data->canUpdate()',
                    ),
                    'delete'=>array(
                        'visible'=> 'GasCheck::canDeleteData($data)',
                    ),
                ),
            ),

//                array(
//                    'name' =>'agent_id',
//                    'type'=>'raw',
//                    'value' => '$data->getAgent()',
//                ),
	),
)); ?>

<script>
$(document).ready(function() {
    fnUpdateColorbox();
});

function fnUpdateColorbox(){   
    fixTargetBlank();
    jQuery('a.gallery').colorbox({ opacity:0.5 , rel:'group1',innerHeight:'1000', innerWidth: '1050' });
}
</script>