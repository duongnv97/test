<?php

$menus = array(
	array('label'=>"$this->pluralTitle Management", 'url'=>array('index')),
	array('label'=>"Tạo Mới $this->singleTitle", 'url'=>array('create')),
	array('label'=>"Cập Nhật $this->singleTitle", 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>"Xóa $this->singleTitle", 'url'=>array('delete'), 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Bạn chắc chắn muốn xóa ?')),
);
$this->menu= ControllerActionsName::createMenusRoles($menus, $actions);
?>

<h1><?php echo $this->singleTitle.": ".$model->id .'-'.$model->getAgent(); ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
        array(
                'name' =>'status',
                'type'=>'raw',
                'value' => $model->getStatus(),
            ),
            array(
                'name' =>'agent_id',
                'type'=>'raw',
                'value' => $model->getAgent(),
            ),
        array(
                'name' =>'deposit',
                'type'=>'raw',
                'value' => $model->getDeposit(),
                'htmlOptions' => array('style' => 'text-align:center;')

            ),
        array(
                'name' =>'prices_contract',
                'type'=>'raw',
                'value' => $model->getAmountContract(),
            ),
        array(
                'name' =>'amount_month_pay',
                'type'=>'raw',
                'value' => $model->getAmountPay(),
                'htmlOptions' => array('style' => 'text-align:center;')
            ),
        array(
                'name' =>'next_date_pay',
                'type'=>'raw',
                'value' => $model->getNextDatePay(),
                'htmlOptions' => array('style' => 'text-align:center;width:80px;')
            ),
        array(
                'name' =>'date_expired',
                'type'=>'raw',
                'value' => $model->getExpiredDatePay(),
                'htmlOptions' => array('style' => 'text-align:center;width:80px;')
            ),
        array(
                'name' =>'start_contract',
                'type'=>'raw',
                'value' => $model->getStartDateContract(),
                'htmlOptions' => array('style' => 'text-align:center;width:80px;')
            ),
        array(
                'name' =>'end_contract',
                'type'=>'raw',
                'value' => $model->getsEndDateContract(),
                'htmlOptions' => array('style' => 'text-align:center;width:80px;')
            ),
        array(
                'name' =>'type_pay',
                'type'=>'raw',
                'value' => $model->getTypePay(),
            ),
        array(
                'name' =>'province_id',
                'type'=>'raw',
                'value' => $model->getProvince(),
            ),
        array(
                'name' =>'note',
                'type'=>'raw',
                'value' => $model->getNote(),
            ),
    ),
));
?>
