<div class="row">
     <?php echo $form->labelEx($model,'amounts_contract'); ?>
    <div class="row">
        <div style="display:inline-block">
            <label style="width:auto; margin: 10px;" for="HomeContract_amounts_start">Từ</label>
            <?php 
            $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                'model'=>$model,        
                'attribute'=>'amounts_start[select]',
                'options'=>array(
                    'showAnim'=>'fold',
                    'dateFormat'=> MyFormat::$dateFormatSearch,
            //                            'minDate'=> '0',
            //                            'maxDate'=> '0',
                    'changeMonth' => true,
                    'changeYear' => true,
                    'showOn' => 'button',
                    'buttonImage'=> Yii::app()->theme->baseUrl.'/admin/images/icon_calendar_r.gif',
                    'buttonImageOnly'=> true,                                
                ),        
                'htmlOptions'=>array(
                    'class'=>'w-16',
                    'style'=>'height:20px;',
                    'readonly'=>'readonly',
                    'id'=>'amounts_start',
                ),
            ));
            ?>
        </div>
        <div style="display:inline-block">
            <label style="width:auto; margin: 10px;" for="HomeContract_amounts_end">Đến</label>
            <?php 
            $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                'model'=>$model,        
                'attribute'=>'amounts_end[select]',
                'options'=>array(
                    'showAnim'=>'fold',
                    'dateFormat'=> MyFormat::$dateFormatSearch,
            //                            'minDate'=> '0',
            //                            'maxDate'=> '0',
                    'changeMonth' => true,
                    'changeYear' => true,
                    'showOn' => 'button',
                    'buttonImage'=> Yii::app()->theme->baseUrl.'/admin/images/icon_calendar_r.gif',
                    'buttonImageOnly'=> true,                                
                ),        
                'htmlOptions'=>array(
                    'class'=>'w-16',
                    'style'=>'height:20px;',
                    'readonly'=>'readonly',
                    'id'=>'amounts_end',
                ),
            ));
            ?>
        </div>
        <div style="display:inline-block">
            <label style="width:auto; margin: 10px;" for="HomeContract_amounts_end">Giá</label>
            <?php echo $form->textField($model,'amounts_price',array('class'=>'number_only ad_fix_currency',)); ?>
        </div>
        <?php $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType'=>'button',
            'label'=>'Thêm',
            'type'=>'null', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
            'size'=>'small', // null, 'large', 'small' or 'mini'
            'id'=>'btnAmount',
            'htmlOptions' =>array(
                'style'=>'background: #0080FF;color: white;border: none;padding: 5px 10px;border-radius: 5px;font-weight: bold;cursor: pointer;'
                ),
            )); ?>
    </div>
    <?php echo $form->error($model,'amounts_contract'); ?>
    <!--list content-->
    <div class="row">
        <h3 class="title-info">Chi tiết</h3>
        <table id="content-amounts" class="materials_table hm_table" style="width: 100%;">
            <thead>
                <tr>
                    <th class="item_c" style="width: 10%;">#</th>
                    <th class="item_c" style="width: 30%;">Ngày bắt đầu</th>
                    <th class="item_c" style="width: 30%;">Ngày kết thúc</th>
                    <th class="item_c" style="width: 20%;">Giá thuê</th>
                    <th class="item_c" style="width: 20%;">Xóa</th>
                </tr>
            </thead>
            <tbody>
                <!--nội dung danh sách giá thuê-->
                <?php
                    $keyNext = 1;
                    $keyMax = count($model->prices_contract);
                ?>
                
                <?php
                if(isset($model->prices_contract))
                foreach ($model->getArrayAmountContract() as $key => $value){ ?>
                <tr>
                    <td class="item_c amounts_number">
                        <?php echo $keyNext; ?>
                    </td>
                    <td class="item_c">
                        <?php 
                            $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                                'model'=>$model,        
                                'attribute'=>'amounts_start[value][]',
                                'options'=>array(
                                    'showAnim'=>'fold',
                                    'dateFormat'=> MyFormat::$dateFormatSearch,
                                    'changeMonth' => true,
                                    'changeYear' => true,
                                    'showOn' => 'button',
                                    'buttonImage'=> Yii::app()->theme->baseUrl.'/admin/images/icon_calendar_r.gif',
                                    'buttonImageOnly'=> true,                                
                                ),        
                                'htmlOptions'=>array(
                                    'class'=>'w-16',
                                    'style'=>'height:20px;',
                                    'readonly'=>'readonly',
                                    'id'=>'amount_start_'.$keyNext,
                                    'value'=>$value['start'],
                                ),
                            ));
                        ?>
                    </td>
                    <td class="item_c">
                        <?php 
                            $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                                'model'=>$model,        
                                'attribute'=>'amounts_end[value][]',
                                'options'=>array(
                                    'showAnim'=>'fold',
                                    'dateFormat'=> MyFormat::$dateFormatSearch,
                                    'changeMonth' => true,
                                    'changeYear' => true,
                                    'showOn' => 'button',
                                    'buttonImage'=> Yii::app()->theme->baseUrl.'/admin/images/icon_calendar_r.gif',
                                    'buttonImageOnly'=> true,                                
                                ),        
                                'htmlOptions'=>array(
                                    'class'=>'w-16',
                                    'style'=>'height:20px;',
                                    'readonly'=>'readonly',
                                    'id'=>'amount_end_'.$keyNext,
                                    'value'=>$value['end'],
                                ),
                            ));
                        ?>
                    </td>
                    <td class="item_c">
                        <input value="<?php echo $value['amount']; ?>" class="number_only ad_fix_currency" name="HomeContract[amounts_price][value][]" id="HomeContract_amounts_price<?php echo $keyNext; ?>" type="text">
                    </td>
                    <td class="item_c">
                        <span class="remove_icon_only"></span>
                    </td>
                </tr>
                
                <?php $keyNext++; } ?>
            </tbody>
        </table>
    </div>
</div>
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/autoNumeric-master/autoNumeric.js"></script>
<script>
    $(document).ready(function () {
        newAmount();
        removeRecord();
        fnInitInputCurrency(); 
    });
    function newAmount(){
        $key=<?php echo $keyNext; ?>;
        $maxNumber = <?php echo $keyMax; ?>;
        $('#btnAmount').on('click',function(){
            $strNewAmount = '<tr>';
            $strNewAmount +='<td class="item_c amounts_number">'+ ++$maxNumber +'</td>';
            $strNewAmount +='<td class="item_c">';
                $strNewAmount+='<input class="w-16" style="height:20px;" readonly="readonly" id="amount_start_'+$key+'" name="HomeContract[amounts_start][value][]" type="text">';
                $strNewAmount+='<img data-id="'+$key+'" id="ui-datepicker-trigger-start'+$key+'" src="<?php echo Yii::app()->theme->baseUrl; ?>/admin/images/icon_calendar_r.gif" alt="..." title="..."></td>';
            $strNewAmount +='<td class="item_c">';
                $strNewAmount+='<input class="w-16" style="height:20px;" readonly="readonly" id="amount_end_'+$key+'" name="HomeContract[amounts_end][value][]" type="text">';
                $strNewAmount+='<img data-id="'+$key+'" id="ui-datepicker-trigger-end'+$key+'" src="<?php echo Yii::app()->theme->baseUrl; ?>/admin/images/icon_calendar_r.gif" alt="..." title="..."></td>';
            $strNewAmount+='</td>';
            $strNewAmount +='<td class="item_c">';
                $strNewAmount +='<input class="number_only ad_fix_currency" name="HomeContract[amounts_price][value][]" id="HomeContract_amounts_price'+$key+'" type="text">';
            $strNewAmount+='</td>';
            $strNewAmount +='<td class="item_c">';
            $strNewAmount+='<span class="remove_icon_only"></span>';
            $strNewAmount+='</td>';
            $strNewAmount +='</tr>';
            $('#content-amounts').find('tbody').append($strNewAmount);
            
//            khai bao danh sach id
            $selectorInputStart= '#amount_start_'+$key;
            $selectorInputEnd= '#amount_end_'+$key;
            $selectorImgStart= '#ui-datepicker-trigger-start'+$key;
            $selectorImgEnd= '#ui-datepicker-trigger-end'+$key;
            $selectorInputAmount = '#HomeContract_amounts_price'+$key;
            
            $($selectorInputStart).datepicker({ dateFormat: '<?php echo MyFormat::$dateFormatSearch; ?>' });
            $($selectorInputEnd).datepicker({ dateFormat: '<?php echo MyFormat::$dateFormatSearch; ?>' });
            $($selectorImgStart).on('click',function(){
                $idClick = $(this).data('id');
                $selectorInputClick = '#amount_start_'+$idClick;
                $($selectorInputClick).datepicker('show');
            });
            $($selectorImgEnd).on('click',function(){
                $idClick = $(this).data('id');
                $selectorInputClick = '#amount_end_'+$idClick;
                $($selectorInputClick).datepicker('show');
            });
//            thuc hien thêm dữ liệu value
            $($selectorInputStart).val($('#amounts_start').val());
            $($selectorInputEnd).val($('#amounts_end').val());
            $($selectorInputAmount).val($('#HomeContract_amounts_price').val());
//            thực hiện xóa record
            removeRecord();
            fnInitInputCurrency(); 
            $key++;
        });
        
    }
    function removeRecord(){
        $('.remove_icon_only').off('click').on('click',function(){
            if(confirm('Bạn chắc chắn muốn xóa?')){
                $(this).closest('tr').remove();
//                    thực hiên refresh lại số record
                index = 1;
                $('.amounts_number').each(function(){
                        $(this).text(index++);
                });
                $maxNumber = $('.amounts_number').length;
            }
        });
    }
</script>    