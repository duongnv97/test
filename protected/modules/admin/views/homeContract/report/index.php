<?php
$gridCss = Yii::app()->getAssetManager()->publish(Yii::getPathOfAlias('zii.widgets.assets')) . '/gridview/styles.css';
Yii::app()->getClientScript()->registerCssFile($gridCss);
$this->breadcrumbs = array(
    $this->pageTitle,
);
$mAppCache  = new AppCache();
$aAgent     = $mAppCache->getAgent();
$aProvince  = GasProvince::getArrAll();
$stt        = 0;
$menus      = array();

$menus[] = [
    'label'=> 'Xuất Excel Danh Sách Hiện Tại',
    'url'=>array('report', 'toExcel'=>1), 
    'htmlOptions'=>array('class'=>'export_excel ','label'=>'Xuất Excel'),
];
    
$this->menu= ControllerActionsName::createMenusRoles($menus, $actions);
?>
<h1><?php echo $this->pageTitle;?></h1>
<div class="search-form" style="">
    <?php
    $this->renderPartial('report/_search', array(
        'model' => $model,
    ));
    ?>
</div>

<?php 
if(!empty($aData)):
$aRes =$aData['aRes'];
$aModel =$aData['aModel'];
$aSubDate = $aData['aSubDate'];
$aSumMonth = $aData['aSumMonth'];
?>
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/scroll/jquery.freezetablecolumns.1.1.js"></script>
<div class="grid-view display_none">
    <table id="freezetablecolumns" class="items hm_table freezetablecolumns">
        <thead>
            <tr style="">
                <th class="item_c w-20">#</th>								
                <th class="item_b w-80">Tỉnh</th>
                <th class="item_b w-150">Đại lý</th>
                <th class="item_b w-60">Tình trạng<br> ĐL</th>
                <th class="item_b w-60">Trạng thái<br> HĐ</th>
                <th class="item_b w-80">Kỳ hạn <br>thanh toán</th>
                <th class="item_b w-80">Ngày hết hạn</th>
                <th class="item_b w-80">Tiền đặt cọc</th>
                <th class="item_b w-180">Giá thuê</th>
                <th class="item_b w-60">Thời hạn<br>(năm)</th>
                <th class="item_b w-80">Tổng</th>
                <?php for($i = 1; $i <= 12 ; $i++): ?> 
                <th class="item_b w-60">
                    <?php
                        echo "T/".$i;
                    ?>
                <br>&nbsp;</th>
                <?php endfor;?>
            </tr>
        </thead>
        <tbody>
        <tr class="h_20">
            <td class="item_c"></td>
            <td class="item_c"></td>
            <td class="item_c"></td>
            <td class="item_c"></td>
            <td class="item_c"></td>
            <td class="item_c"></td>
            <td class="item_c"></td>
            <td class="item_c"></td>
            <td class="item_c"></td>
            <td class="item_c"></td>
            <td class="item_c item_b"><?php echo ActiveRecord::formatCurrency(array_sum($aSumMonth)); ?></td>
            <?php for($i = 1; $i <= 12 ; $i++): ?> 
                <td class="item_b item_r w-80"><?php echo isset($aSumMonth[$i]) ? ActiveRecord::formatCurrency($aSumMonth[$i]): ""; ?></td>
            <?php endfor;?>
        </tr>
        <?php foreach ($aModel as $item): 
            $stt++;
        ?>
        <tr class="h_70">
            <td class="item_c"><?php echo $stt; ?></td>
            <td class="item_l"><?php echo $aProvince[$item->province_id]; ?></td>
            <td class="item_l"><?php echo $aAgent[$item->agent_id] ? $aAgent[$item->agent_id]['first_name'] : ''; ?></td>
            <td class="item_l"><?php echo $aAgent[$item->agent_id] ? (($aAgent[$item->agent_id]['status'] == 1) ? 'Active': 'Inactice') : ''; ?></td>
            <td class="item_c"><?php echo $item->getStatus(); ?></td>
            <td class="item_c"><?php echo $item->getAmountPay(); ?></td>
            <td class="item_c"><?php echo $item->getsEndDateContract(); ?></td>
            <td class="item_r"><?php echo $item->getDeposit(); ?></td>
            <td class="item_c"style="font-size: 9px;"><?php echo $item->getAmountContract(); ?></td>
            <td class="item_c"><?php echo isset($aSubDate[$item->id]) ? $aSubDate[$item->id]: ""; ?></td>
            <td class="item_c"><?php echo isset($aRes[$item->id]) ? ActiveRecord::formatCurrency(array_sum($aRes[$item->id])): ""; ?></td>
            <?php for($i = 1; $i <= 12 ; $i++): ?> 
                <td class=" item_r w-80"><?php echo isset($aRes[$item->id][$i]) ? ActiveRecord::formatCurrency($aRes[$item->id][$i]) : ""; ?></td>
            <?php endfor;?>
        </tr>
        <?php endforeach; ?>
        </tbody>
    </table>
</div>
<?php endif; ?>
<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/print-store-card.css" />
<script type="text/javascript">
    var widthInner = window.innerWidth;
    console.log(widthInner);
    fnAddClassOddEven('items');
    $('.freezetablecolumns').each(function () {
        var id_table = $(this).attr('id');
        if(id_table == 'freezetablecolumns'){
            $('#' + id_table).freezeTableColumns({
                width:      widthInner*0.9,<?php //echo Users::WIDTH_freezetable;?>   // required - bt 1100
//                width:     <?php // echo Users::WIDTH_freezetable;?>,   // required - bt 1100
                height:      450,   // required
                numFrozen: 11,     // optional
                frozenWidth: widthInner*0.6,   // optional
                clearWidths: true  // optional
            });
        }
    });
    $(window).load(function () {
        var index = 1;
        $('.freezetablecolumns').each(function () {
            if (index == 1)
                $(this).closest('div.grid-view').show();
            index++;
        });
        fnAddClassOddEven('items');
    });
    
</script>

