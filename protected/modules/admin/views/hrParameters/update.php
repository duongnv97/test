<?php

$this->breadcrumbs = array(
	$this->pluralTitle => array('index'),
);

$menus = array(	
    array('label' => $this->pluralTitle, 'url' => array('index')),
    array('label' => 'Xem ' . $this->singleTitle, 'url' => array('view', 'id' => $model->id)),	
    array('label' => 'Tạo Mới ' . $this->singleTitle, 'url' => array('create')),
);
$this->menu= ControllerActionsName::createMenusRoles($menus, $actions);
$mRole = new Roles();
$role_name = isset($model->role_id) ? $mRole->GetRoleNameById($model->role_id) : '';
?>

<h1>Cập Nhật <?php echo $this->pluralTitle." ".$role_name; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>