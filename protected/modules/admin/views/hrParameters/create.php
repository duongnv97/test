<?php
$this->breadcrumbs=array(
    'Danh sách tham số'=>array('index'),
    'Create'
);
$menus = array(		
        array('label'=>"$this->singleTitle Management", 'url'=>array('index')),
);
$this->menu= ControllerActionsName::createMenusRoles($menus, $actions);

$role_name = ''; 
$mRole = new Roles();
if(isset($_GET['role'])) $role_name = $mRole->GetRoleNameById($_GET['role']);
?>
<h1>Tạo Mới <?php echo $this->singleTitle.' '.$role_name; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>