<?php
$this->breadcrumbs=array(
	$this->singleTitle,
);
// Import hr.js
$scriptUrl = Yii::app()->theme->baseUrl . '/admin/js/hr.js';
Yii::app()->clientScript->registerScriptFile($scriptUrl);
$menus=array(
            array('label'=>"Tạo mới $this->singleTitle", 'url'=>array('create')),
);

$this->menu= ControllerActionsName::createMenusRoles($menus, $actions);
Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
");
?>

<?php echo MyFormat::BindNotifyMsg();?>
<h1>Danh Sách <?php echo $this->pluralTitle; ?></h1>

 <!--    TAB-->
<?php
$ajax_url = '/admin/HrParameters/UpdateAjax';
$aTab = HrSalaryReports::model()->getArrayRoleSalary(true);
$this->widget('TabWidget', array('ajax_url'=>$ajax_url, 'aTab' => $aTab)); 
?>
<div class='grid-view-loading' style="display: none;" id='loading'></div>
<div id="data" style='padding: 20px'>
    
</div>

<script>
    $(document).ready(function(){
        fnBindCreateParametersColorbox();
        
        var role = $('#tabs ul li.active a.tabLink').data('role');
        var oldUrl = '<?php echo Yii::app()->createAbsoluteUrl("admin/hrParameters/create", array('ajax' => '1')) ?>';
        var newUrl = oldUrl + '/role/' + role;
        $('ul.operations li.create a').attr('href', newUrl);
        $(document).on('click', '.tabLink', function(){
            role = $(this).data('role');
            newUrl = oldUrl + '/role/' + role;
            $('ul.operations li.create a').attr('href', newUrl);
        });
    })
 </script>
 