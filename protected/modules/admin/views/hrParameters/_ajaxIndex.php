<?php $role = isset($_GET['role']) ? $_GET['role'] : ''; ?>
<span id="current_role_id" data-value="<?php echo $role; ?>"></span>

<?php
$data = $model->getParametersByRole($role);
$dataHTML = "";
$createUrl = Yii::app()->createAbsoluteUrl('/admin/hrParameters/create', array('ajax'=>1));
$actions = array('Create','Update','Delete', 'View');
Yii::app()->clientScript->registerScript('update', "
$('.search-form form').submit(function(){
        $.fn.yiiGridView.update('Parameters-grid".$role."');
});
");
$this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'Parameters-grid'.$role,
	'dataProvider'=>$data,
//        'afterAjaxUpdate'=>'function(id, data){ fnUpdateColorbox();}',
        'template'=>'{pager}{summary}{items}{pager}{summary}',
//    'itemsCssClass' => 'items custom_here',
//    'htmlOptions'=> array('class'=>'grid-view custom_here'),
        'pager' => array(
            'maxButtonCount'=>  CmsFormatter::$PAGE_MAX_BUTTON,
        ),
//	'enableSorting' => false,
	//'filter'=>$model,
	'columns'=>array(
            array(
                'header' => 'S/N',
                'type' => 'raw',
                'value' => '$this->grid->dataProvider->pagination->currentPage * $this->grid->dataProvider->pagination->pageSize + ($row+1)',
                'headerHtmlOptions' => array('width' => '10px','style' => 'text-align:center;'),
                'htmlOptions' => array('style' => 'text-align:center;')
            ),
            array(
                'name'=>'name',
                'type'=>'raw',
                'value'=>'$data->getName()',
            ),
            array(
                'name'=>'method',
                'type'=>'raw',
                'value'=>'$data->getMethod()',
            ),
            
            array(
                'name'=>'status',
                'type'=>'raw',
                'value'=>'$data->getStatus()',
                'visible'=> $model->canView(),
            ),
            
            array(
            'header' => 'Actions',
            'class' => 'CButtonColumn',
            'template' => ControllerActionsName::createIndexButtonRoles($actions),
            'buttons' => array(
                
                ),
            ),
	),
)); ?>
