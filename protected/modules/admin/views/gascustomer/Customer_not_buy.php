<?php
$this->breadcrumbs=array(
	'Khách hàng không lấy hàng',
);
$menus=array(
	array('label'=> Yii::t('translation','Tạo Mới'), 'url'=>array('create_customer_store_card')),
//	array('label'=> Yii::t('translation','Xuất Excel Danh Sách Hiện Tại'), 
//            'url'=>array('Export_list_customer_maintain'), 
//            'htmlOptions'=>array('class'=>'export_excel','label'=>'Xuất Excel')),    
    
);

if(!GasCheck::AgentCreateCustomerStoreCard()){
    unset($menus[0]);
}    
//$this->menu = ControllerActionsName::createMenusRoles($menus, $actions);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('users-grid', {
                url : $(this).attr('action'),
		data: $(this).serialize()
	});
	return false;
});
");

Yii::app()->clientScript->registerScript('ajaxupdate', "
$('#users-grid a.ajaxupdate').live('click', function() {
    $.fn.yiiGridView.update('users-grid', {
        type: 'POST',
        url: $(this).attr('href'),
        success: function() {
            $.fn.yiiGridView.update('users-grid');
        }
    });
    return false;
});
");
?>

<h1>Danh Sách Khách Hàng không lấy hàng</h1>

<?php echo CHtml::link(Yii::t('translation','Tìm Kiếm Nâng Cao'),'#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search_customer_store_card',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'users-grid',
	'dataProvider'=>$model->search_customer_store_card(),
	'afterAjaxUpdate'=>'function(id, data){ fnUpdateColorbox();}',
        'template'=>'{pager}{summary}{items}{pager}{summary}',     
//	'enableSorting' => false,
	//'filter'=>$model,
        'pager' => array(
            'maxButtonCount'=>  CmsFormatter::$PAGE_MAX_BUTTON,
        ),        
	'columns'=>array(
        array(
            'header' => 'S/N',
            'type' => 'raw',
            'value' => '$this->grid->dataProvider->pagination->currentPage * $this->grid->dataProvider->pagination->pageSize + ($row+1)',
            'headerHtmlOptions' => array('width' => '30px','style' => 'text-align:center;'),
            'htmlOptions' => array('style' => 'text-align:center;')
        ),
//            array(
//                'header' => 'Mã Hệ Thống',
//                'name' => 'code_account',
//                'htmlOptions' => array('style' => 'width:50px;')
//            ), 		
            array(
                'header' => 'Đại Lý',
                'name' => 'area_code_id',
                'value' => '$data->by_agent?$data->by_agent->first_name:""',
                'visible'=>Yii::app()->user->role_id!=ROLE_SUB_USER_AGENT,
                'htmlOptions' => array('style' => 'text-align:center;width:80px;')
            ),  		
            array(
                'header' => 'Mã KH',
                'name' => 'code_bussiness',
                'htmlOptions' => array('style' => 'width:50px;')
            ),     
            array(
                'header' => 'Loại KH',
                'name' => 'is_maintain',
                'value'=>'$data->is_maintain?CmsFormatter::$CUSTOMER_BO_MOI[$data->is_maintain]:""',
                'htmlOptions' => array('style' => 'width:50px;')
            ),     
            array(                
                'name' => 'first_name',
                'type'=>'raw',
                'value'=>'$data->code_bussiness ." - ".$data->getFullName().$data->getInfoSmsPrice()',
//                'htmlOptions' => array('style' => 'width:50px;')
            ),     
            array(
                'name' => 'address',
                'type' => 'html',
                //'htmlOptions' => array('style' => 'width:150px;')
            ),               
			
            array(
                'name' => 'phone',
                'type' => 'html',
                'value' => '$data->getPhoneShow()',
//                'htmlOptions' => array('style' => 'width:150px;')
            ),  		
//            array(
//                'header' => 'Trạng Thái Bảo Trì',
//                'type' => 'StatusCustomerMaintain',
//                'name' => 'is_maintain',
//                'value'=>'$data',
//                'htmlOptions' => array('style' => 'width:80px;text-align:center;')
//            ),  
                array(
                        'name'=>'sale_id',
                        'value' => '$data',
                        'type' => 'SaleAndLevel',
                        'htmlOptions' => array('style' => 'text-align:center;width:130px;'),
    //                    'htmlOptions' => array('style' => 'text-align:center;'),
                    ), 

                array(
                    'header' => 'Đặc điểm KH',
                    'value' => '$data->getUserRefField("contact_note")',
                ),
                array(
                    'name' => 'channel_id',
                    'type' => 'StatusLayHang',
                    'value' => '$data',
                    'htmlOptions' => array('style' => 'text-align:center;')
                ),
                array(
                    'name' => 'last_purchase',
                    'type' => 'Date',
                ),
                array(
                    'name'=>'payment_day',
                    'value' => '$data->payment?$data->payment->name:""',
                    'htmlOptions' => array('style' => 'text-align:center;width:80px;'),
                    'visible'=> in_array(Yii::app()->user->role_id, CmsFormatter::$ROLE_VIEW_KH_BINHBO),
                ), 
                array(
                    'header' => 'Lý Do Không Lấy Hàng',
                    'type' => 'html',
                    'value' => '$data->getCustomerReasonLeave()',
                ),
            
            array(
                'name' => 'created_date',
                'type' => 'Datetime',
                'htmlOptions' => array('style' => 'width:50px;')
            ),               
		 //Yii::t('translation','phone'),
		
		array(
                    'header' => 'Action',
                    'class'=>'CButtonColumn',
                    'template'=> ControllerActionsName::createIndexButtonRoles($actions,array('update_customer_store_card','view','delete')),
                    // tam thoi phan {update} va view co loaij KH nay chua viet dc, nen dong lai??????
                    'buttons'=>array(
                        'update_customer_store_card'=>array(
                            'label'=>'Cập Nhật Khách Hàng',
                            //'imageUrl'=>Yii::app()->theme->baseUrl . '/admin/images/edit.png',
                            'imageUrl'=>Yii::app()->theme->baseUrl . '/admin/images/icon/update_icon.png',
                            'options'=>array('class'=>'update_customer_store_card'),
                            'url'=>'Yii::app()->createAbsoluteUrl("admin/gascustomer/update_customer_store_card",
                                array("id"=>$data->id) )',
                            'visible'=>  'MyFunctionCustom::agentCanUpdateCustomer($data,array("store_card"=>1))',
                        ),
                    ),						
		),
	),
)); ?>

<script>
	$(document).ready(function(){
		$('.portlet-content .create_customer_store_card').find('a').attr('class','create_new');
		$('.create_customer_store_card').removeClass('create_customer_store_card').addClass('create').find('span').text('Tạo Mới');
		//$(".create_new").colorbox({iframe:true,innerHeight:'400', innerWidth: '700',close: "<span title='close'>close</span>"});
		fnUpdateColorbox();
	});
	
	
	function fnUpdateColorbox(){
                fixTargetBlank();
		$(".view").colorbox({iframe:true,innerHeight:'400', innerWidth: '700',close: "<span title='close'>close</span>"});
	}	
</script>
