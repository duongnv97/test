<?php
$actionBack = "customer_store_card";
 $cRole = Yii::app()->user->role_id;
if($cRole == ROLE_EMPLOYEE_MARKET_DEVELOPMENT){
    $actionBack = "vip240";
}elseif($cRole == ROLE_SUB_USER_AGENT){
    $actionBack = "hgd";
}elseif(in_array($cRole, $model->ARR_ROLE_SALE_CREATE)){
    $actionBack = "customerSaleStoreCard";
}
$this->breadcrumbs=array(
	Yii::t('translation','Khách Hàng')=>array($actionBack),
	(empty($model->getFullName()) ? "Tạo mới khách hàng": "Cập nhật: ".$model->getFullName()),
);

$menus = array(		
        array('label'=> Yii::t('translation', 'Khách Hàng') , 'url'=>array($actionBack)),
);
$this->menu= ControllerActionsName::createMenusRoles($menus, $actions);
?>
