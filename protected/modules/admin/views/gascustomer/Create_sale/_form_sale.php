<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/jquery.blockUI.js"></script>    
<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'users-form',
	'enableAjaxValidation'=>false,
)); 
?>
        <?php if(Yii::app()->user->hasFlash('successUpdate')): ?>
            <div class="success_div"><?php echo Yii::app()->user->getFlash('successUpdate');?></div>         
        <?php endif; ?>  		
        <p class="note">Dòng có dấu <span class="required">*</span> là bắt buộc. Sau khi tạo mới xong bạn phải nhấn  f 5 ở trang tạo khách hàng để cập nhật Sale</p>
        <div class="row">
            <?php echo $form->labelEx($model,'parent_id', array('label'=>'Thuộc Đại Lý')); ?>
            <?php echo $form->dropDownList($model,'parent_id', Users::getArrUserByRole(ROLE_AGENT),array('style'=>'width:450px', 'empty'=>'Select')); ?>
            <?php echo $form->error($model,'parent_id'); ?>
	</div>	        
        
	<div class="row">
            <?php echo Yii::t('translation', $form->labelEx($model,'first_name')); ?>
            <?php echo $form->textField($model,'first_name',array('style'=>'width:450px','maxlength'=>150)); ?>
            <?php echo $form->error($model,'first_name'); ?>
	</div>
        
        <div class="row">
            <?php echo $form->labelEx($model,'gender', array('label'=>"Loại Sale")); ?>
            <?php echo $form->dropDownList($model,'gender',Users::$aTypeSale,array('class'=>'SelectSale' ,'style'=>'width:450px', 'empty'=>"Chọn Loại Sale")); ?>
            <?php echo $form->error($model,'gender'); ?>
	</div>        
	
        <div class="row display_none">
            <?php echo $form->labelEx($model,'phone'); ?>
            <?php echo $form->textField($model,'phone',array('style'=>'width:450px','maxlength'=>250)); ?>
            <?php echo $form->error($model,'phone'); ?>
	</div>	
    
	<div class="row">
            <?php echo Yii::t('translation', $form->labelEx($model,'province_id')); ?>
            <?php echo $form->dropDownList($model,'province_id', GasProvince::getArrAll(),array('empty'=>'Select')); ?>
            <?php echo $form->error($model,'province_id'); ?>
	</div>    
    
	<div class="row">
            <?php echo Yii::t('translation', $form->labelEx($model,'district_id')); ?>
            <?php echo $form->dropDownList($model,'district_id', GasDistrict::getArrAll(),array('empty'=>'Select')); ?>
            <?php echo $form->error($model,'district_id'); ?>
	</div>

	<div class="row display_none">
            <?php echo Yii::t('translation', $form->labelEx($model,'status')); ?>
            <?php echo $form->dropDownList($model,'status', ActiveRecord::getUserStatus()); ?>
            <?php echo $form->error($model,'status'); ?>
	</div>

	<div class="row buttons" style="padding-left: 141px;">
            <?php $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType'=>'submit',
            'label'=>$model->isNewRecord ? Yii::t('translation', 'Create') : Yii::t('translation', 'Save'),
            'type'=>'null', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
            'size'=>'small', // null, 'large', 'small' or 'mini'
            'htmlOptions' => array('class' => 'submit-blockui'),
            //'htmlOptions' => array('style' => 'margin-bottom: 10px; float: right;'),
        )); ?>	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->

<script>
$(document).ready(function() {
//    parent.$.fn.yiiGridView.update("users-grid");   
    $('#Users_province_id').live('change',function(){
        var province_id = $(this).val();        
        var url_ = "<?php echo Yii::app()->createAbsoluteUrl('admin/ajax/get_slt_district');?>";
        $.blockUI({ overlayCSS: { backgroundColor: '<?php echo BLOCK_UI_COLOR;?>' } }); 
        $.ajax({
            url: url_,
            data: {ajax:1,province_id:province_id},
            type: "get",
            dataType:'json',
            success: function(data){
                $('#Users_district_id').html(data['html_district']);                
                $.unblockUI();
            }
        });
        
    });    
        
    $('.submit-blockui').live('click',function(){
       $('.form').block({
           message: '', 
           overlayCSS:  { backgroundColor: '#fff' }
      }); 
//           $('.form').unblock(); 
   });    
    
});

</script>
