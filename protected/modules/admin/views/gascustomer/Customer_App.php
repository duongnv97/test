<?php
$this->breadcrumbs=array(
    'Khách hàng APP',
);

$menus=array(	
    array('label'=> 'Tạo Mới KH', 'url'=>array('create_customer_store_card'),
        'htmlOptions'=>array('label'=>'Tạo KH')
        ),
	array('label'=> Yii::t('translation','Xuất Excel Danh Sách Hiện Tại'), 
            'url'=>array('ExportHgd'), 
            'htmlOptions'=>array('class'=>'export_excel','label'=>'Xuất Excel')),        
);


//if(!GasCheck::AgentCreateCustomerStoreCard()){
//    unset($menus[0]);
//} // chỗ này không sử dụng nữa close on Sep 07, 2015

if(!GasOrders::CanCreateCustomer()){
    unset($menus[0]);
}
    
//$actions[] = 'testNotify'; // DuongNV
$this->menu = ControllerActionsName::createMenusRoles($menus, $actions);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('users-grid', {
                url : $(this).attr('action'),
		data: $(this).serialize()
	});
	return false;
});
");

Yii::app()->clientScript->registerScript('ajaxupdate', "
$('#users-grid a.ajaxupdate').live('click', function() {
    $.fn.yiiGridView.update('users-grid', {
        type: 'POST',
        url: $(this).attr('href'),
        success: function() {
            $.fn.yiiGridView.update('users-grid');
        }
    });
    return false;
});
");

$cRole = Yii::app()->user->role_id;
?>

<h1>Danh Sách Khách hàng APP</h1>

<?php echo CHtml::link(Yii::t('translation','Tìm Kiếm Nâng Cao'),'#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search_hgd',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'users-grid',
	'dataProvider'=>$model->searchVip240(),
	'afterAjaxUpdate'=>'function(id, data){ fnUpdateColorbox();}',
        'template'=>'{pager}{summary}{items}{pager}{summary}',     
//    'itemsCssClass' => 'items custom_here ',
//    'htmlOptions'=> array('class'=>'grid-view custom_here'),
//	'enableSorting' => false,
	//'filter'=>$model,
        'pager' => array(
            'maxButtonCount'=>  CmsFormatter::$PAGE_MAX_BUTTON,
        ),        
	'columns'=>array(
        array(
            'header' => 'S/N',
            'type' => 'raw',
            'value' => '$this->grid->dataProvider->pagination->currentPage * $this->grid->dataProvider->pagination->pageSize + ($row+1)',
            'headerHtmlOptions' => array('width' => '30px','style' => 'text-align:center;'),
            'htmlOptions' => array('style' => 'text-align:center;')
        ),
            array(
                'header' => 'Đại Lý',
                'name' => 'area_code_id',
                'value' => '$data->getAgentOfCustomer()',
                'visible'=>$cRole!=ROLE_SUB_USER_AGENT,
                'htmlOptions' => array('style' => 'text-align:center;width:80px;')
            ),
            array(
                'header' => 'Loại KH',
                'name' => 'is_maintain',
                'value'=>'$data->getTypeCustomerText()',
                'htmlOptions' => array('style' => 'width:50px;')
            ),
            array(
                'header' => 'Username',
                'value' => '$data->getInfoLoginMember()',
                'visible'=>$cRole == ROLE_ADMIN,
                'type'=>'raw',
            ),
//            array(
//                'name' => 'code_account',
//                'htmlOptions' => array('style' => 'width:50px;')
//            ),
            array(
                'name' => 'first_name',
                'type'=>'NameUser',
                'value'=>'$data',
//                'htmlOptions' => array('style' => 'width:50px;')
            ),     
            array(
                'name' => 'address',
                'type' => 'html',
                //'htmlOptions' => array('style' => 'width:150px;')
            ),
//            array(
//                'header' => 'Người liên hệ',
//                'value' => '$data->getUserRefField("contact_person_name")',
//            ),
            array(
                'name' => 'phone',
                'value' => '$data->getPhoneShow()',
                'type' => 'html',
            ),
            
            array(
            'name' => 'sale_id',
            'value' => '$data',
            'type' => 'SaleAndLevel',
            'htmlOptions' => array('style' => 'text-align:center;width:130px;'),
            'visible' => in_array(Yii::app()->user->role_id, CmsFormatter::$ROLE_VIEW_KH_BINHBO),
        ),
//            array(
//                'header' => 'Đặc điểm KH',
//                'value' => '$data->getUserRefField("contact_note")',
//            ),
//            array(
//                'name' => 'channel_id',
//                'type' => 'StatusLayHang',
//                'value' => '$data',
//                'htmlOptions' => array('style' => 'text-align:center;')
//            ),
            array(
                'header' => 'Người Tạo',
                'value' => '$data->getCreatedBy()',
            ),
            array(
                'header' => 'Last login',
                'type' => 'Datetime',
                'value' => '$data->last_logged_in',
                'htmlOptions' => array('style' => 'width:50px;')
            ),
            array(
                'name'=>'status',
                'type'=>'status',
                'value'=>'array("status"=>$data->status,"id"=>$data->id)',
                'htmlOptions' => array('style' => 'text-align:center;')
            ),
            array(
                'name' => 'last_purchase',
                'type' => 'Date',
            ),
            array(
                'name' => 'created_date',
                'type' => 'raw',
                'value' => '$data->getCreatedDate()."<br>".$data->id."<br>".Forecast::model()->getLinkTestNotify($data->id)',
                'htmlOptions' => array('style' => 'width:50px;')
            ),
            array(
                'header' => 'Action',
                'class'=>'CButtonColumn',
//                'template'=> ControllerActionsName::createIndexButtonRoles($actions,array('update_customer_store_card','view','delete','testNotify')),
                'template'=> ControllerActionsName::createIndexButtonRoles($actions,array('update_customer_store_card','view','delete')),
                // tam thoi phan {update} va view co loaij KH nay chua viet dc, nen dong lai??????
                'buttons'=>array(
                    'update_customer_store_card'=>array(
                        'label'=>'Cập Nhật Khách Hàng',
                        //'imageUrl'=>Yii::app()->theme->baseUrl . '/admin/images/edit.png',
                        'imageUrl'=>Yii::app()->theme->baseUrl . '/admin/images/icon/update_icon.png',
                        'options'=>array('class'=>'update_customer_store_card'),
                        'url'=>'Yii::app()->createAbsoluteUrl("admin/gascustomer/update_customer_store_card",
                            array("id"=>$data->id) )',
                        'visible'=>  '$data->canUpdateHgd()',
                    ),
//                    'testNotify' => array( // DuongNV test notify
//                        'label'=>'Test send notify',     //Text label of the button.
//                        'url'=>'Yii::app()->createAbsoluteUrl("admin/ajax/testNotify",array("id"=>$data->id) )',
//                        'imageUrl'=>Yii::app()->theme->baseUrl . '/admin/images/Upload.png',
//                        'options'=>array('class' => 'update_agent'), //HTML options for the button tag..
//                        'visible' => 'MyFormat::getCurrentRoleId() == ROLE_ADMIN'
//                    ),
                ),						
            ),
	),
)); ?>

<script>
   $(document).ready(function(){
        $('.portlet-content .create_customer_store_card').find('a').attr('class','create_new');
//        $('.create_customer_store_card').removeClass('create_customer_store_card').addClass('create').find('span').text('Tạo Mới');
        $('.create_customer_store_card').removeClass('create_customer_store_card').addClass('create');
        //$(".create_new").colorbox({iframe:true,innerHeight:'400', innerWidth: '700',close: "<span title='close'>close</span>"});
        $(".gas_new_user a").colorbox({iframe:true,innerHeight:'600', innerWidth: '900',close: "<span title='close'>close</span>",overlayClose :false, escKey:false});
        fnUpdateColorbox();
    });
    function fnUpdateColorbox(){
        fixTargetBlank();
        $(".view").colorbox({iframe:true,innerHeight:'1500', innerWidth: '800',close: "<span title='close'>close</span>"});
            
//        $('.items').attr('id', 'add_id_fix')
//        $('#add_id_fix').freezeTableColumns({
//            width:       <?php echo Users::WIDTH_freezetable;?>,   // required - bt 1100
//            height:      400,   // required
//            numFrozen:   4,     // optional
//            frozenWidth: 342,   // optional
//            clearWidths: true  // optional
//          });
        
    }
    
    $(window).load(function(){});
</script>

<!--<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/scroll/jquery.freezetablecolumns.1.1.js"></script>--> 