<?php if($cRole == ROLE_ADMIN): ?>
    <div class="row">
        <?php echo $form->label($model,'status'); ?>
        <?php echo $form->dropDownList($model,'status', Users::$USER_STATUS,array('class'=>'w-200', 'empty'=>'Select')); ?>
        <?php echo $form->error($model,'status'); ?>
    </div>
<?php endif; ?>

<?php 
    $canChangeSystem = false;
    if(in_array($cRole, GasConst::getRoleChangeCustomerSystem())):
        $canChangeSystem = true;
    endif;
?>
<h3 class='title-info'>Tạo thêm chi nhánh đặt gas trên App</h3>
<div class='clr'></div>
<?php if($canChangeSystem): ?>
<div class="row">
    <div style="width: 100%;float: left;padding-bottom: 5px;padding-left: 140px;">
        <!--<label style="color: red;width: auto; ">Lưu ý: chỉ tạo chuỗi này cho KH Trụ Sở Chính Của hệ thống</label>-->
    </div>
    <?php echo $form->label($model,'agent_id_search', ['label'=>'Nhập mã hoặc tên KH']); ?>
    <?php echo $form->hiddenField($model,'agent_id_search'); ?>
    <?php 
        // widget auto complete search user customer and supplier
        $aData = array(
            'model'=>$model,
            'field_autocomplete_name'=>'assign_employee_sales',
            'ClassAdd' => 'w-400',
            'field_customer_id'=>'agent_id_search',
            'name_relation_user'=>'rParent',
            'url' => Yii::app()->createAbsoluteUrl('admin/ajax/search_user_by_code'),
            'fnSelectCustomerV2'=> 1,
        );
        $this->widget('ext.SpaAutocompleteCustomer.SpaAutocompleteCustomer',
            array('data'=>$aData));                                        
    ?>
</div>
<div class='clr'></div>
<?php endif; ?>

<?php 
    $aModelOneMany = GasOneMany::getArrModelOfManyId($model->id, GasOneMany::TYPE_CUSTOMER_ACCOUNT_APP);
?>
<table class="tb w-900 TbAddAccount materials_table">
    <thead>
        <tr>
            <td class="item_b item_c">#</td>
            <td class="item_b item_c">Khách hàng</td>
            <td class="item_b item_c">Địa chỉ</td>
            <td class="item_b item_c ">action</td>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($aModelOneMany as $key => $mOneMany): ?>
        <?php $c_name = $c_add = $mOneMany->many_id;
            $mUser = $mOneMany->many;
            if($mUser){
                $c_name = $mUser->code_bussiness . ' - '.$mUser->first_name;
                $c_add = $mUser->address;
            }
        ?>
        <tr class="">
            <td class="order_no item_c CheckCopyRow<?php echo $mOneMany->many_id;?>"><?php echo ($key+1);?></td>
            <td><span class="customer_name"><?php echo $c_name;?></span><input class="customer_id" name="GasOneMany[list_customer_id][]" type="hidden" value="<?php echo $mOneMany->many_id;?>"></td>
            <td class="customer_address"><?php echo $c_add;?></td>
            <td>
                <?php if($canChangeSystem): ?>
                <span class="remove_icon_only"></span>
                <?php endif; ?>
            </td>
        </tr>
        <?php endforeach; ?>
    </tbody>
</table>

<div class="display_none TableCopyRow">
    <table>
    <tr class="trClone">
        <td class="order_no item_c"></td>
        <td><span class="customer_name"></span><input class="customer_id" name="GasOneMany[list_customer_id][]" type="hidden"></td>
        <td class="customer_address"></td>
        <td><span class="remove_icon_only"></span></td>
    </tr>
    </table>
</div>