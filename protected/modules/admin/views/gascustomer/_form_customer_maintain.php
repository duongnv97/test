
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/jquery.blockUI.js"></script>    
<div class="form">

        
        <?php $form=$this->beginWidget('CActiveForm', array(
                'id'=>'maintain-form',
                //'enableAjaxValidation'=>false,
				 'enableClientValidation'=>false,
				/* 'clientOptions' => array(
                    'validateOnSubmit' => true,
                ),	 */			 
        )); ?>
                <?php //echo $form->errorSummary($model); ?>
			<?php if(Yii::app()->user->hasFlash('successUpdate')): ?>
				<div class="success_div"><?php echo Yii::app()->user->getFlash('successUpdate');?></div>         
			<?php endif; ?>     
			<p class="note">Dòng có dấu <span class="required">*</span> là bắt buộc.</p>
			<div class="row">
                    <?php // echo $form->labelEx($model,'code_bussiness', array('label'=>'Mã Khách Hàng')); ?>
                    <?php // echo $form->textField($model,'code_bussiness',array('style'=>'width:376px','maxlength'=>20)); ?>
                    <?php // echo $form->error($model,'code_bussiness'); ?>
            </div>
               
            <?php if(Yii::app()->user->role_id==ROLE_SUB_USER_AGENT || Yii::app()->user->role_id==ROLE_ADMIN): ?>
            <div class="row">
                    <?php echo $form->labelEx($model,'first_name',array('label'=>'Tên Khách Hàng')); ?>
                    <?php echo $form->dropDownList($model,'last_name', CmsFormatter::$SALUTATION,array('style'=>'width:120px;','empty'=>'Chọn Xưng Hô')); ?>
                    <?php echo $form->textField($model,'first_name',array('style'=>'width:245px','maxlength'=>80)); ?>
<!--                    <p style="margin: 0;padding:0; padding-left: 138px;">Ghi đầy đủ họ tên, thêm chức danh: anh, chị, cô, chú...<br>
					vd: Anh Nguyễn Tiến Minh Hoặc Anh Tiến Minh					
					</p>-->
                    <?php echo $form->error($model,'last_name'); ?>
                    <?php echo $form->error($model,'first_name'); ?>                    
            </div>
           <?php endif;?>             
			
			<?php if(!$model->isNewRecord): ?>
            <div class="row">
                    <?php echo $form->labelEx($model,'address'); ?>
                    <?php echo $form->textArea($model,'address',array('style'=>'width:376px','maxlength'=>500)); ?>
                    <?php echo $form->error($model,'address'); ?>
            </div>
			<?php endif;?>

            <div class="row">
                    <?php echo $form->labelEx($model,'phone') ?>
                    <?php echo $form->textField($model,'phone',array('class'=>'phone_number_only no_phone_number_text', 'style'=>'width:198px','maxlength'=>20)); ?>
                    <div class="add_new_item">
                        <input type="checkbox" class="no_phone_number" ><em>Không có số điện thoại</em>
                    </div>
                    <?php echo $form->error($model,'phone'); ?>
            </div> 

	<div class="row">
		<?php echo Yii::t('translation', $form->labelEx($model,'province_id')); ?>
		<?php echo $form->dropDownList($model,'province_id', GasProvince::getArrAll(),array('style'=>'width:376px','class'=>'','empty'=>'Select')); ?>
		<?php echo $form->error($model,'province_id'); ?>
	</div> 	

	<div class="row">
		<?php echo Yii::t('translation', $form->labelEx($model,'district_id')); ?>
		<?php echo $form->dropDownList($model,'district_id', GasDistrict::getArrAll($model->province_id, $model->id),array('style'=>'width:376px')); ?>
                <div class="add_new_item"><a class="iframe_create_district" href="<?php echo Yii::app()->createAbsoluteUrl('admin/ajax/iframe_create_district') ;?>">Tạo Mới Quận Huyện</a><em> (Nếu trong danh sách không có)</em></div>
		<?php echo $form->error($model,'district_id'); ?>
	</div>

	<div class="row">
		<?php echo Yii::t('translation', $form->labelEx($model,'ward_id')); ?>
		<?php echo $form->dropDownList($model,'ward_id', GasWard::getArrAll($model->province_id, $model->district_id),array('style'=>'width:376px','empty'=>'Select')); ?>
                <div class="add_new_item"><a class="iframe_create_ward" href="<?php echo Yii::app()->createAbsoluteUrl('admin/ajax/iframe_create_ward') ;?>">Tạo Mới Phường Xã</a><em> (Nếu trong danh sách không có)</em></div>
		<?php echo $form->error($model,'ward_id'); ?>
	</div>	
	
	<div class="row">
                <?php echo $form->labelEx($model,'house_numbers') ?>
                <?php echo $form->textField($model,'house_numbers',array('style'=>'width:369px','maxlength'=>100)); ?>
                <?php echo $form->error($model,'house_numbers'); ?>
	</div> 	
	
	<div class="row">
            <?php echo $form->labelEx($model,'street_id') ?>
            <?php echo $form->hiddenField($model,'street_id',array('style'=>'width:369px','maxlength'=>100)); ?>
            <?php 
                $this->widget('zii.widgets.jui.CJuiAutoComplete', array(
                        'attribute'=>'autocomplete_name_street',
                        'model'=>$model,
                        'sourceUrl'=>Yii::app()->createAbsoluteUrl('admin/ajax/autocomplete_data_streets'),
                        'options'=>array(
                                'minLength'=>MIN_LENGTH_AUTOCOMPLETE,
                                'multiple'=> true,
                                'search'=>"js:function( event, ui ) {
                                        $('#Users_autocomplete_name_street').addClass('grid-view-loading-gas');
                                        } ",
                                'response'=>"js:function( event, ui ) {
										var json = $.map(ui, function (value, key) { return value; });
										if(json.length<1){
											var error = '<div class=\'errorMessage clr autocomplete_name_text\'>Không tìm thấy dữ liệu.</div>';
											if($('.autocomplete_name_text').size()<1)
												$('.autocomplete_name_error').parent('div').find('.add_new_item').after(error);
											else
												$('.autocomplete_name_error').parent('div').find('.autocomplete_name_text').show();
											$('.remove_row_item').hide();
										}
								
                                        $('#Users_autocomplete_name_street').removeClass('grid-view-loading-gas');
                                        } ",
                                'select'=>"js:function(event, ui) {
                                        $('#Users_street_id').val(ui.item.id);
                                        var remove_div = '<span class=\'remove_row_item\' onclick=\'fnRemoveName(this)\'></span>';
                                        $('#Users_autocomplete_name_street').parent('div').find('.remove_row_item').remove();
                                        $('#Users_autocomplete_name_street').attr('readonly',true).after(remove_div);
										$('.autocomplete_name_error').parent('div').find('.autocomplete_name_text').hide();
                                }",
                        ),
                        'htmlOptions'=>array(
							'class'=>'autocomplete_name_error',
                            'size'=>45,
                            'maxlength'=>45,
                            'style'=>'float:left;width:369px;',
                            'placeholder'=>'Nhập tên đường tiếng việt không dấu',                            
                        ),
                )); 
                ?> 
                <script>
                    function fnRemoveName(this_){
                        $(this_).parent('div').find('input').attr("readonly",false); 
                        $("#Users_autocomplete_name_street").val("");
                        $("#Users_street_id").val("");
                    }
                </script>             
                <div class="add_new_item"><a class="iframe_create_street" href="<?php echo Yii::app()->createAbsoluteUrl('admin/ajax/iframe_create_street') ;?>">Tạo Mới Đường</a><em> (Nếu trong danh sách không có)</em></div>
            <div class="clr"></div>
            <?php echo $form->error($model,'street_id'); ?>
	</div> 				
			
			
                <div class="row buttons" style="padding-left: 90px;">
                    <?php $this->widget('bootstrap.widgets.TbButton', array(
                    'buttonType'=>'submit',
                    'label'=>$model->isNewRecord ? Yii::t('translation', 'Create') : Yii::t('translation', 'Save'),
                    'type'=>'null', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
                    'size'=>'small', // null, 'large', 'small' or 'mini'
                    'htmlOptions' => array('class' => 'submit-blockui'),
                )); ?>	
					
				</div>

        <?php $this->endWidget(); ?>

</div><!-- form -->

<style>
	.add_new_item {display:inline;padding-left:20px;}
	.title-info {background-color:#BDBDBD;padding:10px}
</style>

<script>
    $(document).ready(function(){
	
        $('.form').find('button:submit').click(function(){
            $.blockUI({ overlayCSS: { backgroundColor: '<?php echo BLOCK_UI_COLOR;?>' } }); 
        });
        $('.customer_maintain').addClass('index').find('span').text('Quản Lý');		
    $('#Users_province_id').live('change',function(){
        var province_id = $(this).val();        
        var url_ = "<?php echo Yii::app()->createAbsoluteUrl('admin/ajax/get_slt_district');?>";
        $.blockUI({ overlayCSS: { backgroundColor: '<?php echo BLOCK_UI_COLOR;?>' } }); 
        $.ajax({
            url: url_,
            data: {ajax:1,province_id:province_id},
            type: "get",
            dataType:'json',
            success: function(data){
                $('#Users_district_id').html(data['html_district']);                
                $.unblockUI();
            }
        });
        
    });   		
	
		$('#Users_district_id').live('change',function(){
			var province_id = $('#Users_province_id').val();   
			var district_id = $(this).val();        
			var url_ = "<?php echo Yii::app()->createAbsoluteUrl('admin/ajax/get_slt_ward');?>";
			$.blockUI({ overlayCSS: { backgroundColor: '<?php echo BLOCK_UI_COLOR;?>' } }); 
			$.ajax({
				url: url_,
				data: {ajax:1,district_id:district_id,province_id:province_id},
				type: "get",
				dataType:'json',
				success: function(data){
					$('#Users_ward_id').html(data['html_district']);                
					$.unblockUI();
				}
			});
			
		});   		
			
		
		fnUpdateColorbox();
		fnAddReadonlyStreet();
	});
    
    function fnUpdateColorbox(){    
		<?php if($model->isNewRecord):?>	
//			$('#Users_province_id').trigger('change');
//			$('#Users_district_id').trigger('change');
		<?php endif;?>	
        $(".iframe_create_district").colorbox({iframe:true,innerHeight:'400', innerWidth: '700',close: "<span title='close'>close</span>"});
        $(".iframe_create_ward").colorbox({iframe:true,innerHeight:'400', innerWidth: '700',close: "<span title='close'>close</span>"});
        $(".iframe_create_street").colorbox({iframe:true,innerHeight:'400', innerWidth: '700',close: "<span title='close'>close</span>"});        
    }    
	
	function fnAddReadonlyStreet(){
		<?php if(!$model->isNewRecord && !empty($model->street_id)):?>
		var remove_div = '<span class=\'remove_row_item\' onclick=\'fnRemoveName(this)\'></span>';
		$('#Users_autocomplete_name_street').parent('div').find('.remove_row_item').remove();
		$('#Users_autocomplete_name_street').attr('readonly',true).after(remove_div);
		<?php endif;?>
	}
	
</script>