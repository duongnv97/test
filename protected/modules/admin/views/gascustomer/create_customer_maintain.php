<?php
$this->breadcrumbs=array(
	Yii::t('translation','Khách Hàng Bảo Trì')=>array('customer_maintain'),
	Yii::t('translation','Tạo Mới'),
);

$menus = array(		
        array('label'=> Yii::t('translation', 'Khách Hàng Bảo Trì') , 'url'=>array('customer_maintain')),
);
$this->menu= ControllerActionsName::createMenusRoles($menus, $actions);
?>
	<h1>Tạo Mới Khách Hàng Bảo Trì</h1>
<?php echo $this->renderPartial('_form_customer_maintain', array('model'=>$model,'msg'=>$msg)); ?>

<style>
    .h1-in-form {font-size: 16px;text-align: center; margin-bottom: 30px;}
    #yw0{
        margin-left: 50px;
    }
</style>

