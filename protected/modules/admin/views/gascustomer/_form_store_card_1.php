<?php
    $aUidAllowUpdate = array(
//        866213, // Phạm Hồng Quân
    );
    $aRoleAllowAddress = [ROLE_SALE_ADMIN, ROLE_HEAD_GAS_BO, ROLE_DIEU_PHOI, ROLE_ADMIN];
    $displayAdd = $displayGeneral = '';
    $displayGiaoKho = 'display_none';
    $canCreateAdd = false;$readonlyTypeCustomer = false;$classSelectTypeCustomer = '';
    if(in_array($cRole, $aRoleAllowAddress)){
        $canCreateAdd = true;
    }
    $PreventPaste = 'PreventPaste'; $hideSetupGas = '';
    if($cRole == ROLE_ADMIN || in_array($cUid, GasTickets::getUidCreateKhApp()) ){// lấy thông tin mặc định Tỉnh Quận Huyện khi tạo KH 
        $PreventPaste = '';
//        $hideSetupGas = 'display_none';
    }
    $aRoleHide = [ROLE_SALE, ROLE_MONITORING_MARKET_DEVELOPMENT, ROLE_EMPLOYEE_MARKET_DEVELOPMENT];
    if(in_array($cRole, $aRoleHide)  && 
            ((!empty($model->id) && $model->id < 1752578)
        || $model->is_maintain == UsersExtend::STORE_CARD_FOR_QUOTE)
//        && $model->status != STATUS_WAIT_ACTIVE
    ){// Feb 15, 2017 ẩn không cho sale update các thông tin khác
        $displayGeneral = 'display_none';
    }
    
//    if($cRole == ROLE_ADMIN){
        $displayGiaoKho = '';
//    }
    
    
    $model->mapSomeinfoAgent();
    
    $dayAllowUpdateAdd240 = date('Y-m-d');
    $dayAllowUpdateAdd240 = MyFormat::modifyDays($dayAllowUpdateAdd240, 0, '-');
    $canUpdate = MyFormat::compareTwoDate($model->created_date, $dayAllowUpdateAdd240);
    if(in_array($cRole, UsersRef::$ROLE_240_HGD) && !$model->isNewRecord && !$canUpdate && !in_array($cUid, $aUidAllowUpdate)){
        $displayAdd = "display_none";
    }
    $textHelpAddress = "<b>Hỗ trợ tạo địa chỉ Quận, Phường, Đường mới:</b> Vân: 0962 583 254 </b> Phương: 0988 538 360 <br>Dũng: 01684 331 552 ";
    if(!$model->isNewRecord && in_array($model->is_maintain, CmsFormatter::$aTypeIdHgd) && $cRole == ROLE_SUB_USER_AGENT){
        $readonlyTypeCustomer       = true;
        $classSelectTypeCustomer    = 'display_none';
    }
    $showSelectGas = '';
    if(in_array($cUid, GasTickets::getUidCreateKhApp())){
//        $showSelectGas = 'display_none';
    }
    
//    if(in_array($cRole, $modelCustomer->ARR_ROLE_SALE_LIMIT) && $model->sale_id != $cUid){
    if(!empty($model->id) && in_array($cRole, $model->ARR_ROLE_SALE_LIMIT) && $model->id < 1752578 && $model->sale_id != GasConst::UID_TRUNG_HV){
        $showSelectGas = 'display_none';
    }// Now0918 fix tạm cho a Trung sửa thông tin KH, sau đó có thể close lại
    
?>

<div id="tabs-1">
    <?php if(in_array($cRole, GasConst::getRoleViewUsername())): ?>
        <?php include '_form_store_card_1_add_account.php';?>
    <?php endif; ?>
    
    <div class="<?php echo $showSelectGas;?>">
        <?php include '_form_store_card_1_gas.php'; ?>
        <?php include '_form_store_card_1_sms.php'; ?>
    </div>
    <?php include '_form_store_card_1_general.php'; ?>
</div>
