<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

    <div class="row">
        <?php echo $form->label($model,'created_by', array('label'=>'Người tạo')); ?>
        <?php echo $form->hiddenField($model,'created_by'); ?>
        <?php // echo $form->dropDownList($model,'sale_id', Users::getArrUserByRole(ROLE_SALE, array('order'=>'t.code_bussiness', 'prefix_type_sale'=>1)),array('style'=>'width:500px','empty'=>'Select')); ?>
                <?php 
                    $url = Yii::app()->createAbsoluteUrl('admin/ajax/search_by_role', array('role'=> implode(',', Call::getArrayRoleCreateCustomer())));
                    // widget auto complete search user customer and supplier
                    $aData = array(
                        'model'=>$model,
                        'field_customer_id'=>'created_by',
                        'field_autocomplete_name'=>'date_year',
                        'url'=> $url,
                        'name_relation_user'=>'rCreatedBy',
                        'placeholder'=>'Nhập Tên NV',
                        'ClassAdd' => 'w-500',
    //                        'fnSelectCustomer'=>'fnAfterSelectSaleOrAgent',
                    );
                    $this->widget('ext.SpaAutocompleteCustomer.SpaAutocompleteCustomer',
                        array('data'=>$aData));                                        
                ?>
        <?php echo $form->error($model,'sale_id'); ?>
    </div>
    
    <?php if(Yii::app()->user->role_id!=ROLE_SALE):?>
    <div class="row">
        <?php echo $form->label($model,'sale_id', array('label'=>'Nhân viên CCS - Người tạo')); ?>
        <?php echo $form->hiddenField($model,'sale_id'); ?>
        <?php // echo $form->dropDownList($model,'sale_id', Users::getArrUserByRole(ROLE_SALE, array('order'=>'t.code_bussiness', 'prefix_type_sale'=>1)),array('style'=>'width:500px','empty'=>'Select')); ?>
                <?php 
                    $url = Yii::app()->createAbsoluteUrl('admin/ajax/search_by_role', array('role'=> implode(',', Call::getArrayRoleCreateCustomer())));
                    // widget auto complete search user customer and supplier
                    $aData = array(
                        'model'=>$model,
                        'field_customer_id'=>'sale_id',
                        'field_autocomplete_name'=>'autocomplete_name_parent',
                        'url'=> $url,
                        'name_relation_user'=>'sale',
                        'placeholder'=>'Nhập Tên Sale',
                        'ClassAdd' => 'w-500',
    //                        'fnSelectCustomer'=>'fnAfterSelectSaleOrAgent',
                    );
                    $this->widget('ext.SpaAutocompleteCustomer.SpaAutocompleteCustomer',
                        array('data'=>$aData));                                        
                ?>
        <?php echo $form->error($model,'sale_id'); ?>
    </div>
    <?php endif;?>
    
    <div class="row">
        <?php echo $form->label($model,'customer_id'); ?>
        <?php echo $form->hiddenField($model,'customer_id'); ?>
        <?php 
            // widget auto complete search user customer and supplier
            $aData = array(
                'model'=>$model,
                'field_customer_id'=>'customer_id',
                'name_relation_user'=>'customer',
                'url' => Yii::app()->createAbsoluteUrl('admin/ajax/searchHgd', array()),
                'ClassAdd' => 'w-500',
            );
            $this->widget('ext.SpaAutocompleteCustomer.SpaAutocompleteCustomer',
                array('data'=>$aData));                                        
        ?>
    </div>
    
    <div class="row">
    <?php echo $form->labelEx($model,'agent_id_search'); ?>
    <?php echo $form->hiddenField($model,'agent_id_search', array('class'=>'')); ?>
    <?php
            // 1. limit search kh của sale
            $url = Yii::app()->createAbsoluteUrl('admin/ajax/search_by_role', array('role'=> ROLE_AGENT));
            // widget auto complete search user customer and supplier
            $aData = array(
                'model'=>$model,
                'field_customer_id'=>'agent_id_search',
                'url'=> $url,
                'name_relation_user'=>'parent',
                'ClassAdd' => 'w-500',
                'field_autocomplete_name' => 'ward_id',
                'placeholder'=>'Nhập mã hoặc tên đại lý',
            );
            $this->widget('ext.SpaAutocompleteCustomer.SpaAutocompleteCustomer',
                array('data'=>$aData));
            ?>
    </div>

    <div class="row more_col">
        <div class="col1">
        </div>
        <div class="col2">
            <?php echo $form->label($model,'is_maintain', array('label'=>'Loại KH')); ?>
            <?php echo $form->dropDownList($model,'is_maintain', CmsFormatter::$CUSTOMER_BO_MOI,array('class'=>'w-200', 'empty'=>'Select')); ?>
        </div>
        <div class="col3">
            <?php echo $form->label($model,'parent_id', array('label'=>'Loại điểm')); ?>
            <?php echo $form->dropDownList($model,'parent_id', UsersExtend::getHgdTypePoint(), array('class'=>'w-200', 'empty'=>'Select')); ?>
        </div>
    </div>
    
    <div class="row more_col">
        <div class="col1">
            <?php echo $form->labelEx($model,'province_id'); ?>
            <?php echo $form->dropDownList($model,'province_id', GasProvince::getArrAll(),array('class'=>'w-200','empty'=>'Select')); ?>
        </div>
        <div class="col2">
            <?php echo $form->labelEx($model,'district_id'); ?>
            <?php echo $form->dropDownList($model,'district_id', GasDistrict::getArrAll($model->province_id),array('class'=>'w-200','empty'=>'Select')); ?>
        </div>
        <div class="col3"></div>
    </div>
    
    <div class="row more_col">
        <div class="col1">
            <?php echo $form->labelEx($model,'buying'); ?>
            <?php echo $form->dropDownList($model,'buying', UsersExtend::getHgdBuying(),array('class'=>'w-200','empty'=>'Select')); ?>
        </div>
        <div class="col2">
            <?php echo $form->labelEx($model,'payment_day', array('label'=>"Ngày sử dụng dưới")); ?>
            <?php echo $form->dropDownList($model,'payment_day', MyFormat::BuildNumberOrder(100),array('class'=>'w-200','empty'=>'Select')); ?>
        </div>
        <div class="col3">
            <?php echo $form->labelEx($model,'ip_address', array('label'=>"Đầu tư")); ?>
            <?php echo $form->dropDownList($model,'ip_address', GasConst::$ARR_INVERS_HGD, array('class'=>'w-200','empty'=>'Select')); ?>
        </div>
    </div>
    
    <div class="row more_col">
        <div class="col1">
            <?php echo $form->label($model,'phone_ext2', array('label'=>'PTTT CODE')); ?>
            <?php echo $form->textField($model,'phone_ext2', array('class'=>'w-160')); ?>
        </div>
        <div class="col2">
                <?php echo Yii::t('translation', $form->label($model,'date_from')); ?>
                <?php 
                    $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                        'model'=>$model,        
                        'attribute'=>'date_from',
                        'options'=>array(
                            'showAnim'=>'fold',
                            'dateFormat'=> MyFormat::$dateFormatSearch,
//                            'minDate'=> '0',
                            'maxDate'=> '0',
                            'changeMonth' => true,
                            'changeYear' => true,
                            'showOn' => 'button',
                            'buttonImage'=> Yii::app()->theme->baseUrl.'/admin/images/icon_calendar_r.gif',
                            'buttonImageOnly'=> true,                                
                        ),        
                        'htmlOptions'=>array(
                            'class'=>'w-180',
                            'style'=>'float:left;',                               
                        ),
                    ));
                ?>     		
        </div>
        <div class="col3">
                <?php echo Yii::t('translation', $form->label($model,'date_to')); ?>
                <?php 
                    $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                        'model'=>$model,        
                        'attribute'=>'date_to',
                        'options'=>array(
                            'showAnim'=>'fold',
                            'dateFormat'=> MyFormat::$dateFormatSearch,
//                            'minDate'=> '0',
                            'maxDate'=> '0',
                            'changeMonth' => true,
                            'changeYear' => true,
                            'showOn' => 'button',
                            'buttonImage'=> Yii::app()->theme->baseUrl.'/admin/images/icon_calendar_r.gif',
                            'buttonImageOnly'=> true,                                
                        ),        
                        'htmlOptions'=>array(
                            'class'=>'w-180',
                            'style'=>'float:left;',
                        ),
                    ));
                ?>     		
        </div>
        
    </div>
        
    <div class="row buttons" style="padding-left: 159px;">
    <?php $this->widget('bootstrap.widgets.TbButton', array(
        'buttonType'=>'submit',
        'label'=>Yii::t('translation','Search'),
        'type'=>'null', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
        'size'=>'small', // null, 'large', 'small' or 'mini'
        //'htmlOptions' => array('style' => 'margin-bottom: 10px; float: right;'),
    )); ?>	
    </div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->

<script>
$(function(){
    fnBindChangeProvince('<?php echo BLOCK_UI_COLOR;?>', "<?php echo Yii::app()->createAbsoluteUrl('admin/ajax/get_slt_district');?>");
});
</script>