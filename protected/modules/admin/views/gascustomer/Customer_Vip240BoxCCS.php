<?php 
$DATA_CCS = Sta2::groupVip240();
$OUTPUT = GasPtttDailyGoback::GetOutputCCS();
$index = 1;
?>

<?php if(isset($DATA_CCS['DATA'])): ?>
<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/print-store-card.css" />
<table class="tb hm_table">
    <thead>
        <tr>
            <td class="item_c">#</td>
            <td class="item_c">Nhân Viên</td>
            <td class="item_c">SL Khách Hàng</td>
            <?php foreach ($OUTPUT as $month=>$aInfo): ?>
            <td class="item_c">Sản Lượng Tháng <?php echo $month; ?></td>
            <?php endforeach; ?>
        </tr>
    </thead>
    <tbody>
    <?php foreach ($DATA_CCS['DATA'] as $uid=>$qty): ?>
        <tr>
            <td class="item_c"><?php echo $index++; ?></td>
            <td ><?php echo $DATA_CCS['MODEL_USER'][$uid];?></td>
            <td class="item_r"><?php echo ActiveRecord::formatCurrency($qty); ?></td>
            <?php foreach ($OUTPUT as $month=>$aInfo): ?>
                <?php $outputCCS = isset($OUTPUT[$month][$uid]) ? $OUTPUT[$month][$uid] : 0; 
                    $qtyText = !empty($outputCCS) ? ActiveRecord::formatCurrency($outputCCS) : "";
                    $percent = round(( $outputCCS / GasPtttDailyGoback::TARGET_CCS ) * 100) ;
                ?>
                <td class="item_c"><?php echo "<span class='item_b'>".$qtyText. "</span> &nbsp;($percent %) "; ?></td>
            <?php endforeach; ?>
        </tr>
    <?php endforeach; ?>
    </tbody>
</table>
<?php endif; ?>