<?php
$this->breadcrumbs=array(
	'Khách Hàng',
);
$mGasCustomerImages = new GasCustomerImages();
$menus=array(	
//	array('label'=> Yii::t('translation','Xuất Excel Danh Sách Hiện Tại'), 
//            'url'=>array('Export_list_customer_maintain'), 
//            'htmlOptions'=>array('class'=>'export_excel','label'=>'Xuất Excel')),    
//	array('label'=> 'Tạo Mới Sale', 
//            'url'=>array('Create_sale'), 
//            'htmlOptions'=>array('class'=>'gas_new_user','label'=>'Tạo Sale')),
    array('label'=> 'Tạo Mới KH', 'url'=>array('create_customer_store_card'),
        'htmlOptions'=>array('label'=>'Tạo KH')
        ),
	array('label'=> Yii::t('translation','Xuất Excel Danh Sách Hiện Tại'), 
            'url'=>array('ExportStorecard'), 
            'htmlOptions'=>array('class'=>'export_excel','label'=>'Xuất Excel')),        
);


//if(!GasCheck::AgentCreateCustomerStoreCard()){
//    unset($menus[0]);
//} // chỗ này không sử dụng nữa close on Sep 07, 2015

if(!GasOrders::CanCreateCustomer()){
    unset($menus[0]);
}
    
$this->menu = ControllerActionsName::createMenusRoles($menus, $actions);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('users-grid', {
                url : $(this).attr('action'),
		data: $(this).serialize()
	});
	return false;
});
");

Yii::app()->clientScript->registerScript('ajaxupdate', "
$('#users-grid a.ajaxupdate').live('click', function() {
    $.fn.yiiGridView.update('users-grid', {
        type: 'POST',
        url: $(this).attr('href'),
        success: function() {
            $.fn.yiiGridView.update('users-grid');
        }
    });
    return false;
});
");

$cRole = Yii::app()->user->role_id;
?>

<h1>Danh Sách Khách Hàng</h1>
<?php echo MyFormat::BindNotifyMsg(); ?>

<div class="float_r">
<?php
//Xuất excel khách hàng không dùng app trong 3 thang
if($this->canExportApp()){
    $urlApp = Yii::app()->createAbsoluteUrl("admin/gascustomer/ExportStorecard/app/1");
    echo CHtml::link('Xuất excel khách hàng không dùng app', $urlApp); 
}
?>
</div>
<br>

<?php echo CHtml::link(Yii::t('translation','Tìm Kiếm Nâng Cao'),'#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search_customer_store_card',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'users-grid',
	'dataProvider'=>$model->search_customer_store_card(),
	'afterAjaxUpdate'=>'function(id, data){ fnUpdateColorbox();}',
        'template'=>'{pager}{summary}{items}{pager}{summary}',     
//    'itemsCssClass' => 'items custom_here ',
//    'htmlOptions'=> array('class'=>'grid-view custom_here'),
//	'enableSorting' => false,
	//'filter'=>$model,
        'pager' => array(
            'maxButtonCount'=>  CmsFormatter::$PAGE_MAX_BUTTON,
        ),        
	'columns'=>array(
        array(
            'header' => 'S/N',
            'type' => 'raw',
            'value' => '$this->grid->dataProvider->pagination->currentPage * $this->grid->dataProvider->pagination->pageSize + ($row+1)',
            'headerHtmlOptions' => array('width' => '30px','style' => 'text-align:center;'),
            'htmlOptions' => array('style' => 'text-align:center;')
        ),
            array(
                'header' => 'Đại Lý',
                'name' => 'area_code_id',
                'type' => 'html',
                'value' => '$data->getAgentOfCustomer()."<br>".GasCustomerImages::getActionsForGasCustomer($data)',
                'visible'=>$cRole!=ROLE_SUB_USER_AGENT,
                'htmlOptions' => array('style' => 'text-align:center;width:80px;')
            ),
            array(
                'header' => 'Loại KH',
                'name' => 'is_maintain',
                'type' => 'raw',
                'value'=>'$data->getTypeCustomerText()."<br><b>".$data->getGroup()."</b>"',
                'htmlOptions' => array('style' => 'width:50px;')
            ),
            array(
                'header' => 'Username',
                'name' => 'username',
                'type' => 'raw',
                'value' => '$data->getUsernameView()',
                'visible'=>  in_array($cRole, GasConst::getRoleViewUsername()),
            ),
            array(
                'name' => 'code_account',
                'htmlOptions' => array('style' => 'width:50px;')
            ),
            array(
                'name' => 'first_name',
                'type'=>'raw',
                'value'=>'"<b>".$data->code_bussiness ." - ".$data->getFullName()."</b>".$data->getInfoSmsPrice()',
//                'htmlOptions' => array('style' => 'width:50px;')
            ),     
            array(
                'name' => 'address',
                'type' => 'raw',
//                'value'=>'$data->getAddress()."<br><b>- ".$data->getGasBrandText()."</b>"',
                'value'=>'$data->getAddress()',
                //'htmlOptions' => array('style' => 'width:150px;')
            ),
            array(
                'header' => 'Người liên hệ',
                'value' => '$data->getUserRefField("contact_person_name")',
            ),
            array(
                'name' => 'phone',
                'value' => '$data->getPhoneShow()',
                'type' => 'html',
            ),
            
//            array(
//                'header' => 'Trạng Thái Bảo Trì',
//                'type' => 'StatusCustomerMaintain',
//                'name' => 'is_maintain',
//                'value'=>'$data',
//                'htmlOptions' => array('style' => 'width:80px;text-align:center;')
//            ),  
            
                array(
                    'name'=>'sale_id',
                    'value' => '$data',
                    'type' => 'SaleAndLevel',
                    'htmlOptions' => array('style' => 'text-align:center;width:130px;'),
//                    'htmlOptions' => array('style' => 'text-align:center;'),
                    'visible'=>in_array(Yii::app()->user->role_id, CmsFormatter::$ROLE_VIEW_KH_BINHBO),
                ), 
            
//            array(
//                'name'=>'payment_day',
//                'value' => '$data->getMasterLookupName("payment")',
//                'htmlOptions' => array('style' => 'text-align:center;width:80px;'),
//                'visible'=> in_array(Yii::app()->user->role_id, CmsFormatter::$ROLE_VIEW_KH_BINHBO),
//            ), 
            array(
                'header' => 'Đặc điểm KH',
                'value' => '$data->getUserRefField("contact_note")',
            ),
            array(
                'name' => 'channel_id',
                'type' => 'StatusLayHang',
                'value' => '$data',
                'htmlOptions' => array('style' => 'text-align:center;')
            ),
            array(
                'name' => 'last_purchase',
                'type' => 'Date',
            ),
            array(
                'header' => 'Người Tạo',
                'value' => '$data->getCreatedBy()',
            ),
            array(
                'name' => 'created_date',
                'type' => 'raw',
                'value' => '$data->getCreatedDate()."<br>".$data->getMasterLookupName("payment")."<br>".$data->id',
                'htmlOptions' => array('style' => 'width:50px;')
            ),
            
		 //Yii::t('translation','phone'),
		
		array(
                    'header' => 'Action',
                    'class'=>'CButtonColumn',
                    'template'=> ControllerActionsName::createIndexButtonRoles($actions,array('update_customer_store_card','view','delete')).'{ExportPdf}&nbsp;&nbsp;&nbsp;<br>{MailQuotes}&nbsp;&nbsp;&nbsp;{ResetPassCustomer}',
                    // tam thoi phan {update} va view co loaij KH nay chua viet dc, nen dong lai??????
                    'buttons'=>array(
                        'update_customer_store_card'=>array(
                            'label'=>'Cập Nhật Khách Hàng',
                            //'imageUrl'=>Yii::app()->theme->baseUrl . '/admin/images/edit.png',
                            'imageUrl'=>Yii::app()->theme->baseUrl . '/admin/images/icon/update_icon.png',
                            'options'=>array('class'=>'update_customer_store_card'),
                            'url'=>'Yii::app()->createAbsoluteUrl("admin/gascustomer/update_customer_store_card",
                                array("id"=>$data->id) )',
                            'visible'=>  'MyFunctionCustom::agentCanUpdateCustomer($data,array("store_card"=>1))',
                        ),
                        'ExportPdf'=>array(
                            'label'=>'Xuất báo giá pdf',
                            'imageUrl'=>Yii::app()->theme->baseUrl . '/new-icon/pdf.png',
                            'options'=>array('class'=>''),
                            'url'=>'Yii::app()->createAbsoluteUrl("admin/ajax/quotesCustomer",
                                array("id"=>$data->id, "ToPdf"=>1,"uid"=>time()) )',
                        ),
                        
                        'MailQuotes'=>array(
                            'label'=>'Gửi báo giá cho KH qua email',
                            'imageUrl'=>Yii::app()->theme->baseUrl . '/admin/images/mail.png',
                            'options'=>array('class'=>'MailQuotes'),
                            'url'=>'Yii::app()->createAbsoluteUrl("admin/ajax/quotesCustomer",
                                array("id"=>$data->id, "MailQuotes"=>1, "next"=>$data->getActionNext()) )',
                            'click'=>'function(){ if(confirm("Bạn chắc chắn muốn gửi báo giá cho KH qua emai?")) { $.blockUI({ message: null }); return true; } return false;}',
                            'visible' => '$data->canMailQuotes()',
                        ),
                        'ResetPassCustomer'=>array(
                            'label'=>'Đặt lại mật khẩu mặc định cho khách hàng',
                            'imageUrl'=>Yii::app()->theme->baseUrl . '/new-icon/refresh.png',
                            'options'=>array('class'=>'ResetPassCustomer'),
                            'url'=>'Yii::app()->createAbsoluteUrl("admin/ajaxForUpdate/resetPassCustomer",
                                array("id"=>$data->id, "ResetPassCustomer"=>1, "next"=>$data->getActionNext()) )',
//                            'click'=>'function(){ if(confirm("Bạn chắc chắn muốn Đặt lại mật khẩu mặc định cho khách hàng?")) { $.blockUI({ message: null }); return true; } return false;}',
                            'visible' => '$data->canResetPass()',
                        ),
                        
                    ),						
		),
	),
)); ?>

<script>
   $(document).ready(function(){
        $('.portlet-content .create_customer_store_card').find('a').attr('class','create_new');
//        $('.create_customer_store_card').removeClass('create_customer_store_card').addClass('create').find('span').text('Tạo Mới');
        $('.create_customer_store_card').removeClass('create_customer_store_card').addClass('create');
        //$(".create_new").colorbox({iframe:true,innerHeight:'400', innerWidth: '700',close: "<span title='close'>close</span>"});
        $(".gas_new_user a").colorbox({iframe:true,innerHeight:'600', innerWidth: '900',close: "<span title='close'>close</span>",overlayClose :false, escKey:false});
        fnUpdateColorbox();
    });
    function fnUpdateColorbox(){
        fixTargetBlank();
        $(".view").colorbox({iframe:true,innerHeight:'1500', innerWidth: '800',close: "<span title='close'>close</span>"});
            
//        $('.items').attr('id', 'add_id_fix')
//        $('#add_id_fix').freezeTableColumns({
//            width:       <?php echo Users::WIDTH_freezetable;?>,   // required - bt 1100
//            height:      400,   // required
//            numFrozen:   4,     // optional
//            frozenWidth: 342,   // optional
//            clearWidths: true  // optional
//          });
        $('.MailQuotes').attr('target','');
        $('.ResetPassCustomer').click(function(){
            if(!confirm("Bạn chắc chắn muốn Đặt lại mật khẩu mặc định cho khách hàng?")){
                return false;
            }
            var this_ = $(this);
            $.blockUI({ overlayCSS: { backgroundColor: '#fff' } });
            $.ajax({
                url: $(this).attr('href'),
                data: {},
                type: 'post',
                dataType: 'json',
                success:function(data){
                    $.unblockUI();
                    this_.remove();
                    alert(data['msg']);
                    return false; 
                }
            });
            
           return false; 
        });
    $(".targetHtml").colorbox({
        iframe: true,
        overlayClose: false, escKey: true,
        innerHeight: '1000',
        innerWidth: '1100', close: "<span title='close'>close</span>",
        onClosed: function () { // update view when close colorbox
//            $.fn.yiiGridView.update('users-grid');// AnhDung Close Sep0218 - ko can thiet reload
        }
    });
        
    }
    
    $(window).load(function(){});
</script>

<!--<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/scroll/jquery.freezetablecolumns.1.1.js"></script>--> 