<?php echo MyFormat::BindNotifyMsg(); ?>
<div class="form" >

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'gas-text-form',
	'enableAjaxValidation'=>false,
        'htmlOptions' => array('enctype' => 'multipart/form-data'),
)); ?>
<!--@Code: NAM005-->
<?php $this->widget('EmployeePictureWidget', array('fakeEmployeeImages'=>$mGasCustomerImages,
                                                    'onlyView'=>false,
                                                    'user' => $model,
                                                    'relations'=>'rEmployeesImages',
                                                    'title'=> "File hồ sơ: {$model->first_name} <br>Đ/C: {$model->address}",
                                                    'field_name'=>get_class($model).'[file_name]')); ?>
<input name="Users[id]" value="<?php echo $model->id; ?>" style="display:none;">
<div class="row buttons" style="padding-left: 141px;">
    <button class="submitForm btn btn-small" id="yw0" type="submit" onClick="return checkType()" name="yt0">Save</button>
<input class='cancel_iframe' type='button' value='Cancel'>
</div>
<?php $this->endWidget(); ?>

</div><!-- form -->
<script>
    $(document).ready(function(){
        fnBindRemoveIcon();
    });
    
    $('#gas-text-form').submit(function (event) {
        var errors = false;
        $( ".typeSelect" ).each(function( index ) {
            if($( this ).val() == ''){ 
                errors = true;
            }
        }); 
        if (errors == true) {
            event.preventDefault();
            alert('Loại file không được phép rỗng');
            return false;
        }
        $.blockUI({ overlayCSS: { backgroundColor: '<?php echo BLOCK_UI_COLOR;?>' } }); 
        return true;
    });
</script>