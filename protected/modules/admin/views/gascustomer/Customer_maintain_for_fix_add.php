<?php
$this->breadcrumbs=array(
	'Khách Hàng Bảo Trì',
);

$menus=array(
	array('label'=> Yii::t('translation','Tạo Mới'), 'url'=>array('create_customer_maintain')),
	array('label'=> Yii::t('translation','Xuất Excel Danh Sách Hiện Tại'), 
            'url'=>array('Export_list_customer_maintain'), 
            'htmlOptions'=>array('class'=>'export_excel','label'=>'Xuất Excel')),    
    
);
$this->menu = ControllerActionsName::createMenusRoles($menus, $actions);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('users-grid', {
                url : $(this).attr('action'),
		data: $(this).serialize()
	});
	return false;
});
");

Yii::app()->clientScript->registerScript('ajaxupdate', "
$('#users-grid a.ajaxupdate').live('click', function() {
    $.fn.yiiGridView.update('users-grid', {
        type: 'POST',
        url: $(this).attr('href'),
        success: function() {
            $.fn.yiiGridView.update('users-grid');
        }
    });
    return false;
});
");
?>

<h1>Danh Sách Khách Hàng Bảo Trì</h1>

<?php echo CHtml::link(Yii::t('translation','Tìm Kiếm Nâng Cao'),'#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search_customer_maintain',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'users-grid',
	'dataProvider'=>$model->search_customer_maintain(),
	'afterAjaxUpdate'=>'function(id, data){ fnUpdateColorbox();}',
        'template'=>'{pager}{summary}{items}{pager}{summary}',     
//	'enableSorting' => false,
	//'filter'=>$model,
	'columns'=>array(
        array(
            'header' => 'S/N',
            'type' => 'raw',
            'value' => '$this->grid->dataProvider->pagination->currentPage * $this->grid->dataProvider->pagination->pageSize + ($row+1)',
            'headerHtmlOptions' => array('width' => '30px','style' => 'text-align:center;'),
            'htmlOptions' => array('style' => 'text-align:center;')
        ),
            array(
                'header' => 'Mã Hệ Thống',
                'name' => 'code_account',
                'htmlOptions' => array('style' => 'width:50px;')
            ), 		
            array(
                'header' => 'Mã KH',
                'name' => 'code_bussiness',
                'htmlOptions' => array('style' => 'width:50px;')
            ),     
            array(                
                'name' => 'first_name',
                'type'=>'NameUser',
                'value'=>'$data',
//                'htmlOptions' => array('style' => 'width:50px;')
            ),     
            array(
                'name' => 'address',
                'type' => 'html',
                //'htmlOptions' => array('style' => 'width:150px;')
            ),               
			
            array(
                'name' => 'phone',
                'type' => 'html',
                'htmlOptions' => array('style' => 'width:150px;')
            ),  		
            array(
                'header' => 'Trạng Thái Bảo Trì',
                'type' => 'StatusCustomerMaintain',
                'name' => 'is_maintain',
                'value'=>'$data',
                'htmlOptions' => array('style' => 'width:80px;text-align:center;')
            ),  		
            array(
                'name' => 'created_date',
                'type' => 'Datetime',
                'htmlOptions' => array('style' => 'width:50px;')
            ),               
		 //Yii::t('translation','phone'),
		
		array(
                    'header' => 'Action',
                    'class'=>'CButtonColumn',
                    'template'=> ControllerActionsName::createIndexButtonRoles($actions,array('update_customer_maintain','view','delete')),
						// tam thoi phan {update} va view co loaij KH nay chua viet dc, nen dong lai??????
                    'buttons'=>array(
                        'update_customer_maintain'=>array(
                            'label'=>'Cập Nhật Khách Hàng',
                            //'imageUrl'=>Yii::app()->theme->baseUrl . '/admin/images/edit.png',
                            'imageUrl'=>Yii::app()->theme->baseUrl . '/admin/images/icon/update_icon.png',
                            'options'=>array('class'=>'update_customer_maintain'),
                            'url'=>'Yii::app()->createAbsoluteUrl("admin/gascustomer/update_customer_maintain",
                                array("id"=>$data->id) )',
//                            'visible'=>  'MyFunctionCustom::agentCanUpdateCustomer($data)',
                        ),
                    ),						
		),
	),
)); ?>

<script>
	$(document).ready(function(){
		$('.portlet-content .create_customer_maintain').find('a').attr('class','create_new');
		$('.create_customer_maintain').removeClass('create_customer_maintain').addClass('create').find('span').text('Tạo Mới');
		//$(".create_new").colorbox({iframe:true,innerHeight:'400', innerWidth: '700',close: "<span title='close'>close</span>"});
		fnUpdateColorbox();
	});
	
	
	function fnUpdateColorbox(){    
		$(".view").colorbox({iframe:true,innerHeight:'400', innerWidth: '700',close: "<span title='close'>close</span>"});
	}	
</script>
