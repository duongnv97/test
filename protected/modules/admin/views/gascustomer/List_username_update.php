<?php
$this->breadcrumbs=array(
	Yii::t('translation','Khách Hàng')=>array('list_username'),
	Yii::t('translation','Cập Nhật Username Login'),
);

$menus = array(		
        array('label'=> Yii::t('translation', 'QL Khách Hàng') , 'url'=>array('list_username')),
);
$this->menu= ControllerActionsName::createMenusRoles($menus, $actions);
$cmsFormat = new CmsFormatter();
?>
<h1>Cập Nhật Username Login cho Khách Hàng: <?php echo $cmsFormat->formatNameUser($model);?></h1>
<?php echo $this->renderPartial('List_username_form', array('model'=>$model,'msg'=>'')); ?>
<style>
    .h1-in-form {font-size: 16px;text-align: center; margin-bottom: 30px;}
    #yw0{
        margin-left: 50px;
    }
</style>