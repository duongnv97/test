<?php
$this->breadcrumbs=array(
	'Khách Hàng',
);

$menus=array(
	array('label'=> Yii::t('translation','Create Users'), 'url'=>array('create')),
);
$this->menu= ControllerActionsName::createMenusRoles($menus, $actions);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('users-grid', {
                url : $(this).attr('action'),
		data: $(this).serialize()
	});
	return false;
});
");

Yii::app()->clientScript->registerScript('ajaxupdate', "
$('#users-grid a.ajaxupdate').live('click', function() {
    $.fn.yiiGridView.update('users-grid', {
        type: 'POST',
        url: $(this).attr('href'),
        success: function() {
            $.fn.yiiGridView.update('users-grid');
        }
    });
    return false;
});
");
?>

<h1><?php echo Yii::t('translation', 'DS Khách Hàng'); ?></h1>

<?php if(Yii::app()->user->role_id==ROLE_ADMIN):?>
<h2>
    <a href="<?php echo Yii::app()->createAbsoluteUrl('admin/gascustomer/import_customer');?>">
        Nhập từ file Excel
    </a>    
</h2>
<h2>
    <a href="<?php echo Yii::app()->createAbsoluteUrl('admin/gascustomer/index',array('check_duplicate'=>1));?>">
       Click để kiểm tra trùng mã khách hàng kế toàn và Mã KH kinh doanh
    </a>    
</h2>
<?php endif;?>


<?php echo CHtml::link(Yii::t('translation','Advanced Search'),'#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'users-grid',
	'dataProvider'=>$model->search(),
	'enableSorting' => false,
        'template'=>'{pager}{summary}{items}{pager}{summary}',     
	//'filter'=>$model,
        'pager' => array(
            'maxButtonCount'=>  CmsFormatter::$PAGE_MAX_BUTTON,
        ),        
	'columns'=>array(
        array(
            'header' => 'S/N',
            'type' => 'raw',
            'value' => '$this->grid->dataProvider->pagination->currentPage * $this->grid->dataProvider->pagination->pageSize + ($row+1)',
            'headerHtmlOptions' => array('width' => '30px','style' => 'text-align:center;'),
            'htmlOptions' => array('style' => 'text-align:center;')
        ),
            array(
                'name' => 'code_account',
                'htmlOptions' => array('style' => 'width:50px;')
            ), 		
            array(
                'name' => 'code_bussiness',
                'htmlOptions' => array('style' => 'width:50px;')
            ),             
		 Yii::t('translation','first_name'),
//            Yii::t('translation','name_agent'),
			'phone',

            
        array(
            'name' => 'address',
            'type' => 'html',
            'htmlOptions' => array('style' => 'width:150px;')
        ),            
/*         array(
                'name'=>'province_id',
                'value'=>'$data->province?$data->province->name:""',
            ),		
            array(
                'name'=>'channel_id',
                'value'=>'$data->channel?$data->channel->name:""',
            ), */
            array(
                'name'=>'district_id',
                'value'=>'$data->district?$data->district->name:""',
            ),            
//            array(
//                'name'=>'storehouse_id',
//                'value'=>'$data->storehouse?$data->storehouse->name:""',
//            ),            
//            array(
//                'name'=>'sale_id',
//                'value'=>'$data->sale?$data->sale->first_name:""',
//            ),  
//		 Yii::t('translation','payment_day'),
             array(
                'name' => 'created_date',
                'type' => 'Datetime',
                'htmlOptions' => array('style' => 'width:50px;')
            ),
		 //Yii::t('translation','phone'),
		
		array(
			'header' => 'Action',
			'class'=>'CButtonColumn',
                        'template'=> ControllerActionsName::createIndexButtonRoles($actions),
		),
	),
)); ?>
