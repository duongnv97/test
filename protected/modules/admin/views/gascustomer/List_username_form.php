<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/jquery.blockUI.js"></script>    
<div class="form">
    <?php $form=$this->beginWidget('CActiveForm', array(
            'id'=>'maintain-form',
            'enableClientValidation'=>false,
    )); ?>
        <?php //echo $form->errorSummary($model); ?>
        <?php if(Yii::app()->user->hasFlash('successUpdate')): ?>
                <div class="success_div"><?php echo Yii::app()->user->getFlash('successUpdate');?></div>         
        <?php endif; ?>     
        <p class="note">Dòng có dấu <span class="required">*</span> là bắt buộc.</p>
        <div class="row buttons" style="padding-left: 141px;">
            <?php $this->widget('bootstrap.widgets.TbButton', array(
                'buttonType'=>'submit',
                'label'=>$model->isNewRecord ? Yii::t('translation', 'Create') : Yii::t('translation', 'Save'),
                'type'=>'null', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
                'size'=>'small', // null, 'large', 'small' or 'mini'
                'htmlOptions' => array('class' => 'submit-blockui'),
            )); ?>	
        </div>
        <div class="clr"></div>   
         <div class="row ">
            <label>&nbsp;</label>
            <?php echo $form->checkBox($model,'password', ['class'=>'float_l']); ?>
            <?php echo $form->labelEx($model,'password',array('class'=>'checkbox_one_label l_padding_10','label'=>'Set KH đổi mật khẩu', 'style'=>'width: auto; margin-top:3px;')); ?>
        </div>
        <div class="clr"></div>   

        <?php // if(Yii::app()->user->role_id==ROLE_AGENT || Yii::app()->user->role_id==ROLE_ADMIN): ?>
        <div class="row">
                <?php echo $form->labelEx($model,'first_name',array('label'=>'Tên Khách Hàng')); ?>
                <?php echo $form->textField($model,'first_name',array('size'=>'58','readonly'=>1)); ?>
<!--                    <p style="margin: 0;padding:0; padding-left: 138px;">Ghi đầy đủ họ tên, thêm chức danh: anh, chị, cô, chú...<br>
                    vd: Anh Nguyễn Tiến Minh Hoặc Anh Tiến Minh					
                    </p>-->
                <?php echo $form->error($model,'last_name'); ?>
                <?php echo $form->error($model,'first_name'); ?>                    
        </div>
       <?php // endif;?>


    <?php if(!$model->isNewRecord): ?>
        <div class="row">
            <?php echo $form->labelEx($model,'address'); ?>
            <?php echo $form->textArea($model,'address',array('style'=>'width:364px','readonly'=>true)); ?>
            <?php echo $form->error($model,'address'); ?>
        </div>
    <?php endif;?>

    <div class="row">
        <?php echo $form->labelEx($model,'username') ?>
        <?php echo $form->textField($model,'username',array('style'=>'width:369px')); ?>
        <?php echo $form->error($model,'username'); ?>
    </div>

    <div class="row">
        <?php if (!$model->isNewRecord):?>
            <div style="width: 100%;float: left;padding-bottom: 5px;padding-left: 140px;">
                <label style="color: red;width: auto; ">Bỏ trống bên dưới nếu bạn không muốn đổi mật khẩu hiện tại</label>
            </div>
        <?php endif?>
        <?php echo $form->labelEx($model,'password_hash'); ?>
        <?php echo $form->passwordField($model,'password_hash',array('style'=>'width:450px','maxlength'=>50,'value'=>'')); ?>
        <?php echo $form->error($model,'password_hash'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model,'password_confirm'); ?>
        <?php echo $form->passwordField($model,'password_confirm',array('style'=>'width:450px','maxlength'=>50,'value'=>'')); ?>
        <?php echo $form->error($model,'password_confirm'); ?>
    </div>

    <div class="row buttons" style="padding-left: 141px;">
        <?php $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType'=>'submit',
            'label'=>$model->isNewRecord ? Yii::t('translation', 'Create') : Yii::t('translation', 'Save'),
            'type'=>'null', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
            'size'=>'small', // null, 'large', 'small' or 'mini'
            'htmlOptions' => array('class' => 'submit-blockui'),
        )); ?>	
    </div>

    <?php $this->endWidget(); ?>

</div><!-- form -->
<?php $this->widget('TokenWidget', array('user_id' => $model->id)); ?>
<style>
    .add_new_item {display:inline;padding-left:20px;}
    .title-info {padding:10px}
</style>

<script>
    $(document).ready(function(){
        $('.form').find('button:submit').click(function(){
            $.blockUI({ overlayCSS: { backgroundColor: '<?php echo BLOCK_UI_COLOR;?>' } }); 
        });
        $('.list_username').addClass('index').find('span').text('Quản Lý');
    });
</script>