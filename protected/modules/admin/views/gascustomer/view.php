<?php $cmsFormat = new CmsFormatter();
    $mUsersRef = new UsersRef();
?>
<h1>Xem Khách Hàng: <?php echo $cmsFormat->formatNameUser($model); ?></h1>
<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
//            array(
//                'label'=>'Mã Khách Hàng',
//                'name'=>'code_bussiness',
//                'value'=>$model->code_bussiness,
//            ),
            
            array(
                'name' => 'first_name',
                'type'=>'raw',
                'value'=>$model->code_bussiness ." - ".$model->getFullName().$model->getInfoSmsPrice(),
            ),
            "code_account",
            array(
                'label' => 'Loại KH',
                'name' => 'is_maintain',
                'value'=> $model->getTypeCustomerText(),
            ),
            array(
                'label' => 'Đại Lý',
                'name' => 'area_code_id',
                'value' => $model->getAgentOfCustomer(),
                'visible'=>Yii::app()->user->role_id!=ROLE_SUB_USER_AGENT,
            ),
            array(
//                'label' => 'Đại Lý',
                'name' => 'parent_id',
                'value' => $model->getNameByRelation("rParent"),
            ),
            array(
                'name'=>'sale_id',
                'value' => $model,
                'type' => 'SaleAndLevel',
//                'visible'=>in_array(Yii::app()->user->role_id, CmsFormatter::$ROLE_VIEW_KH_BINHBO),
            ), 
            array(
                'name' => 'price',
                'value' => $model->getPrice(),
            ),
            array(
                'label' => 'Người liên hệ',
                'value' => $model->getUserRefField("contact_person_name"),
            ),
            array(
                'label' => 'Đặc điểm KH',
                'value' => $model->getUserRefField("contact_note"),
            ),
            
            array(
                'name'=>'phone',
                'value'=>$model->phone,
            ),
            array(
                'name' => 'channel_id',
                'type' => 'StatusLayHang',
                'value' => $model,
            ),
            array(
                'name'=>'payment_day',
                'value' => $model->getMasterLookupName('payment'),
            ), 
            array(
                'name'=>'address',
                'type' => 'raw',
                'value'=>$model->getAddress()."<br><b>- ".$model->getGasBrandText()."</b>",
            ),               
//            array(
//                'name'=>'address',
//                'type'=>'AddressTempUser',
//                'value'=>$model,
//            ),               
            
//            array(
//                'name'=>'province_id',
//                'value'=>$model->province?$model->province->name:'',
//            ),               
//          
//            array(
//                'name'=>'district_id',
//                'value'=>$model->district?$model->district->name:'',
//            ),               
//          
//            array(
//                'name'=>'ward_id',
//                'value'=>$model->ward?$model->ward->name:'',
//            ),               
//            'house_numbers',
//            array(
//                'name'=>'street_id',
//                'value'=>$model->street?$model->street->name:'',
//            ),
            array(
                'label' => 'Người Tạo',
                'value' => $model->getCreatedBy(),
            ),
            'created_date:Datetime',
            array(
                'label'=>'Last Update By',
                'type'=>'NameUser',
                'value'=>$model->rCreatedBy,
            ),
            array(
                'label'=>'Last Update Time',
                'type'=>'Datetime',
                'name'=>'last_logged_in',
            ),
	),
)); ?>

<?php include "view_map.php"; ?>

<?php if(in_array($model->is_maintain, array(STORE_CARD_KH_BINH_BO, STORE_CARD_KH_MOI))): ?>
<h3 class='title-info'>Thông Tin Chủ Quán</h3>
<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
            
//            array(
//                'label' => $mUsersRef->getAttributeLabel("contact_payment"),
//                'value' => $model->getUserRefField("contact_payment"),
//            ),
//            array(
//                'label' => $mUsersRef->getAttributeLabel("contact_position_delivery"),
//                'value' => $model->getUserRefField("contact_position_delivery"),
//            ),
            
            array(
                'label' => $mUsersRef->getAttributeLabel("contact_boss_name"),
                'value' => $model->getUserRefField("contact_boss_name"),
            ),
            array(
                'label' => $mUsersRef->getAttributeLabel("contact_boss_phone"),
                'value' => $model->getUserRefField("contact_boss_phone"),
            ),
            array(
                'label' => $mUsersRef->getAttributeLabel("contact_boss_landline"),
                'value' => $model->getUserRefField("contact_boss_landline"),
            ),
            array(
                'label' => $mUsersRef->getAttributeLabel("contact_boss_fax"),
                'value' => $model->getUserRefField("contact_boss_fax"),
            ),
            array(
                'label' => $mUsersRef->getAttributeLabel("contact_boss_mail"),
                'value' => $model->getUserRefField("contact_boss_mail"),
            ),
	),
)); ?>
<h3 class='title-info'>Thông Tin Nhân Viên Quản lý</h3>
<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
            array(
                'label' => $mUsersRef->getAttributeLabel("contact_manage_name"),
                'value' => $model->getUserRefField("contact_manage_name"),
            ),
            array(
                'label' => $mUsersRef->getAttributeLabel("contact_manage_phone"),
                'value' => $model->getUserRefField("contact_manage_phone"),
            ),
            array(
                'label' => $mUsersRef->getAttributeLabel("contact_manage_landline"),
                'value' => $model->getUserRefField("contact_manage_landline"),
            ),
            array(
                'label' => $mUsersRef->getAttributeLabel("contact_manage_fax"),
                'value' => $model->getUserRefField("contact_manage_fax"),
            ),
            array(
                'label' => $mUsersRef->getAttributeLabel("contact_manage_mail"),
                'value' => $model->getUserRefField("contact_manage_mail"),
            ),
	),
)); ?>
<h3 class='title-info'>Thông Tin Nhân Viên Kỹ Thuật</h3>
<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
            array(
                'label' => $mUsersRef->getAttributeLabel("contact_technical_name"),
                'value' => $model->getUserRefField("contact_technical_name"),
            ),
            array(
                'label' => $mUsersRef->getAttributeLabel("contact_technical_phone"),
                'value' => $model->getUserRefField("contact_technical_phone"),
            ),
            array(
                'label' => $mUsersRef->getAttributeLabel("contact_technical_landline"),
                'value' => $model->getUserRefField("contact_technical_landline"),
            ),
            array(
                'label' => $mUsersRef->getAttributeLabel("contact_technical_fax"),
                'value' => $model->getUserRefField("contact_technical_fax"),
            ),
            array(
                'label' => $mUsersRef->getAttributeLabel("contact_technical_mail"),
                'value' => $model->getUserRefField("contact_technical_mail"),
            ),
	),
)); ?>
<?php endif; ?>

<div class="clr"></div>
<div class="row selectAgent">
    <label>Reset pass</label>
    <div class="f_size_15" style="padding-left: 141px;"><?php echo $model->getEventLog();?></div>
</div>

<?php $this->widget('CustomerChangeInfoWidget', array('customer_id' => $model->id)); ?>