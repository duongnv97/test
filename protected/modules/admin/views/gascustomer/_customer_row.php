<div class='clr'></div>
<div class="row">
    <?php echo $form->label($model,'parent_id'); ?>
    <?php echo $form->hiddenField($model,'parent_id'); ?>
    <?php 
        // widget auto complete search user customer and supplier
        $aData = array(
            'model'=>$model,
            'field_autocomplete_name'=>'autocomplete_name_parent',
            'field_customer_id'=>'parent_id',
            'name_relation_user'=>'rParent',
            'ClassAdd' => 'w-400',
            'url' => Yii::app()->createAbsoluteUrl('admin/ajax/search_user_by_code'),
        );
        $this->widget('ext.SpaAutocompleteCustomer.SpaAutocompleteCustomer',
            array('data'=>$aData));                                        
    ?>
</div>

<?php /* close at Now 21, 2014
<div class='clr'></div>
<div class=''>
        <div class="row">
            <?php echo $form->labelEx($model,'parent_id'); ?>
            <?php echo $form->hiddenField($model,'parent_id'); ?>
            <?php 
                    $this->widget('zii.widgets.jui.CJuiAutoComplete', array(
                            'attribute'=>'autocomplete_name',
                            'model'=>$model,
                            'sourceUrl'=>Yii::app()->createAbsoluteUrl('admin/ajax/search_user_by_code'),
            //                                'name'=>'my_input_name',
                            'options'=>array(
                                    'minLength'=>MIN_LENGTH_AUTOCOMPLETE,
                                    'multiple'=> true,
                        'search'=>"js:function( event, ui ) {
                                $('#Users_autocomplete_name').addClass('grid-view-loading-gas');
                                } ",
                        'response'=>"js:function( event, ui ) {
                                var json = $.map(ui, function (value, key) { return value; });
                                if(json.length<1){
                                    var error = '<div class=\'errorMessage clr autocomplete_name_text_customer\' style=\'padding-left: 145px; \'>Không tìm thấy dữ liệu.</div>';
                                    if($('.autocomplete_name_text_customer').size()<1)
                                            $('.autocomplete_name_error_customer').after(error);
                                    else
                                        $('.autocomplete_name_error_customer').parent('div').find('.autocomplete_name_text_customer').show();
                                    $('.autocomplete_name_error_customer').parent('div').find('.remove_row_item').hide();                                            
                                }
                                $('#Users_autocomplete_name').removeClass('grid-view-loading-gas');
                                } ",

                                'select'=>"js:function(event, ui) {
                                        $('#Users_parent_id').val(ui.item.id);
                                        fnFormatInputAutocomplete();
                                        
                                        fnBuildTableInfoCustomer(ui.item);
                                        $('.autocomplete_customer_info').show();
                                        $('.autocomplete_name_error_customer').parent('div').find('.autocomplete_name_text_customer').hide();
                                }",
                            ),
                            'htmlOptions'=>array(
                                'class'=>'autocomplete_name_error_customer',
                                'size'=>58,
                                'maxlength'=>45,
                                'style'=>'float:left;',
                                'placeholder'=>'Nhập tên, số ĐT, Địa chỉ hoặc mã hệ thống',
                            ),
                    )); 
                    ?>        
                    <script>
                        function fnRemoveNameCustomer(this_){
                            $(this_).parent('div').find('.autocomplete_name_error_customer').attr("readonly",false); 									
                            $("#Users_autocomplete_name").val("");
                            $("#Users_parent_id").val("");
                            $('.autocomplete_customer_info').hide();
                        }
                        function fnBuildTableInfoCustomer(item){
                            $(".info_name").text(item.name_customer);
                            $(".info_name_agent").text(item.name_agent);
                            $(".info_code_account").text(item.code_account);
                            $(".info_code_bussiness").text(item.code_bussiness);
                            $(".info_address").text(item.address);
                            $(".info_phone").text(item.phone);
                        }
                            
                        function fnFormatInputAutocomplete(){
                            var remove_div = '<span class=\'remove_row_item\' onclick=\'fnRemoveNameCustomer(this)\'></span>';
                            $('#Users_autocomplete_name').parent('div').find('.remove_row_item').remove();
                            $('#Users_autocomplete_name').attr('readonly',true).after(remove_div);
                        }    

                    </script>        
                    <div class="clr"></div>		
            <?php echo $form->error($model,'parent_id'); ?>
            <?php $display='display:inline;';
                $info_name ='';
                $info_name_agent ='';
                $info_address ='';
                $info_code_account ='';
                $info_code_bussiness ='';
                $info_phone ='';
                    if(empty($model->parent_id)) $display='display: none;';
                    else{
                        if($model->customer){
                            $info_name = $model->customer->first_name;
                            $info_name_agent = $model->customer->name_agent;
                            $info_code_account = $model->customer->code_account;
                            $info_code_bussiness = $model->customer->code_bussiness;
                            $info_address = $model->customer->address;
                            $info_phone = $model->customer->phone;
                        }
                    }
            ?>                        
            <div class="autocomplete_customer_info" style="<?php echo $display;?>">
            <table>
                <tr>
                    <td class="_l">Mã khách hàng:</td>
                    <td class="_r info_code_bussiness"><?php echo $info_code_bussiness;?></td>
                </tr>

                <tr>
                    <td class="_l">Tên khách hàng:</td>
                    <td class="_r info_name"><?php echo $info_name;?></td>
                </tr>                    
                <tr>
                    <td class="_l">Địa chỉ:</td>
                    <td class="_r info_address"><?php echo $info_address;?></td>
                </tr>
                <tr>
                    <td class="_l">Điện Thoại:</td>
                    <td class="_r info_phone"><?php echo $info_phone;?></td>
                </tr>


            </table>
        </div>
        <div class="clr"></div>    		
        <?php // echo $form->error($model,'parent_id'); ?>
    </div>	

</div> <!-- end <div class='customer_old'> -->
<?php if(!$model->isNewRecord &&  !empty($model->parent_id)): ?>
<script>
    $(function(){
        fnFormatInputAutocomplete();
    });
</script>
<?php endif;?>
<div class='clr'></div>
*/?>