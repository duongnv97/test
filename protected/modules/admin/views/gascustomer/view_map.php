<?php $tmp = explode(",", $model->slug); ?>
<?php if($model->is_maintain  == STORE_CARD_HGD_CCS && count($tmp) > 1): ?>
<?php
$aDataChart = array();
$aDataChart[] = array($model->first_name."<br>".$model->getAddress(), $tmp[0], $tmp[1], 0);
$js_array = json_encode($aDataChart);
// echo "var javascript_array = ". $js_array . ";\n";

?>
<!--http://stackoverflow.com/questions/3059044/google-maps-js-api-v3-simple-multiple-marker-example
https://developers.google.com/maps/documentation/javascript/markers#animated
https://developers.google.com/maps/documentation/javascript/examples/infowindow-simple-max
-->
<script src="http://maps.google.com/maps/api/js?key=AIzaSyCfsbfnHgTh4ODoXLXFfEFhHskDhnRVtjQ" type="text/javascript"></script>
<div id="map" style="width: 100%; height: 400px;"></div>
<script type="text/javascript">
    var locations = <?php echo $js_array;?>;
    var map = new google.maps.Map(document.getElementById('map'), {
      zoom: 15,
      center: new google.maps.LatLng(<?php echo $tmp[0];?>, <?php echo $tmp[1];?>),
      mapTypeId: google.maps.MapTypeId.ROADMAP
    });

    var infowindow = new google.maps.InfoWindow();

    var marker, i;
//    var image = 'http://daukhi.huongminhgroup.com/apple-touch-icon.png';

    for (i = 0; i < locations.length; i++) {  
      marker = new google.maps.Marker({
        position: new google.maps.LatLng(locations[i][1], locations[i][2]),
        map: map
//        icon: image
      });

      google.maps.event.addListener(marker, 'click', (function(marker, i) {
        return function() {
          infowindow.setContent(locations[i][0]);
          infowindow.open(map, marker);
        }
      })(marker, i));
    }
  </script>
  
<?php endif; ?>