<div class="BoxSetupGeneralOfCustomer <?php echo $displayGeneral;?>">
    <h3 class='title-info'>Thông tin chung</h3>
    <?php if($cRole != ROLE_SUB_USER_AGENT): ?>
    <div class="row">
        <?php // echo $form->labelEx($model,'area_code_id', array('label'=>'Thuộc Đại Lý (Nơi giao)')); ?>
        <?php // echo $form->dropDownList($model,'area_code_id', Users::getArrUserByRole(ROLE_AGENT),array('style'=>'width:376px','empty'=>'Select')); ?>
        <?php // echo $form->error($model,'area_code_id'); ?>
    </div>
    
    <div class="row">
    <?php echo $form->labelEx($model,'area_code_id'); ?>
    <?php echo $form->hiddenField($model,'area_code_id', array('class'=>'')); ?>
    <?php
            // 1. limit search kh của sale
            $url = Yii::app()->createAbsoluteUrl('admin/ajax/search_by_role', array('role'=> ROLE_AGENT));
            // widget auto complete search user customer and supplier
            $aData = array(
                'model'=>$model,
                'field_customer_id'=>'area_code_id',
                'url'=> $url,
                'name_relation_user'=>'by_agent',
                'ClassAdd' => 'w-400',
                'field_autocomplete_name' => 'maintain_agent_id',
                'placeholder'=>'Nhập mã hoặc tên đại lý',
            );
            $this->widget('ext.SpaAutocompleteCustomer.SpaAutocompleteCustomer',
                array('data'=>$aData));
            ?>
        <?php echo $form->error($model,'area_code_id'); ?>
    </div>
    
    <?php endif;?>
    <div class="<?php echo $showSelectGas;?>">
        <div class="row">
            <div style="width: 100%;float: left;padding-bottom: 5px;padding-left: 140px;">
                <label style="color: red;width: auto; ">Chú Ý: Khi sale hoặc CCS tạo mới KH thì để trống mã kế toán. Điều phối sẽ cập nhật sau.</label>
            </div>
            <?php echo $form->labelEx($model,'code_account') ?>
            <?php echo $form->textField($model,'code_account',array('size'=>58)); ?>
            <?php echo $form->error($model,'code_account'); ?>
        </div>
    </div>
    <?php if($cRole == ROLE_ADMIN): ?>
    <div class="row">
        <?php echo $form->labelEx($model,'username') ?>
        <?php echo $form->textField($model,'username',array('size'=>58)); ?>
        <?php echo $form->error($model,'username'); ?>
    </div>
    <?php endif; ?>

    <?php // if(Yii::app()->user->role_id==ROLE_AGENT || Yii::app()->user->role_id==ROLE_ADMIN): ?>
    <div class="row">
        <?php echo $form->labelEx($model,'first_name',array('label'=>'Tên Khách Hàng')); ?>
        <?php // echo $form->dropDownList($model,'last_name', CmsFormatter::$SALUTATION,array('style'=>'width:120px;','empty'=>'Chọn Xưng Hô')); ?>
        <?php echo $form->textField($model,'first_name',array('size'=>'58','maxlength'=>200, 'class'=>"$PreventPaste" )); ?>
    <!--                    <p style="margin: 0;padding:0; padding-left: 138px;">Ghi đầy đủ họ tên, thêm chức danh: anh, chị, cô, chú...<br>
            vd: Anh Nguyễn Tiến Minh Hoặc Anh Tiến Minh					
            </p>-->
        <p style="margin: 0;padding:0; padding-left: 138px;"><span class="item_b">Chuỗi Tìm Kiếm: </span><?php echo $model->address_vi;?></p>
        <?php echo $form->error($model,'last_name'); ?>
        <?php echo $form->error($model,'first_name'); ?>
    </div>
    <?php // endif;?>
    
    <div class="<?php echo $showSelectGas;?>">
        <?php include_once '_customer_row.php'; ?>
    </div>

    <?php if(!$model->isNewRecord): ?>
        <div class="row">
            <?php echo $form->labelEx($model,'address'); ?>
            <?php echo $form->textArea($model,'address',array('style'=>'width:364px','readonly'=>true)); ?>
            <?php echo $form->error($model,'address'); ?>
        </div>
    <?php endif;?>

    <div class="row">
        <div style="width: 100%;float: left;padding-bottom: 5px;padding-left: 140px;">
            <label style="color: red;width: auto; ">Mỗi số phone cách nhau bởi dấu - vd: 0123456789-0988180386-01684331552</label>
        </div>
        <?php if($model->isNewRecord): ?>
        <div style="width: 100%; float: left; padding-bottom: 5px; padding-left: 140px;">
            <?php echo $form->checkBox($model,'agent_name_real',array('class'=>'float_l CopyPhoneExt2')); ?>
            <?php echo $form->labelEx($model,'agent_name_real',array('class'=>'checkbox_one_label w-300 l_padding_10', 'style'=>'padding-top:3px;', 'label'=>'Copy số báo giá')); ?>
        </div>
        <?php endif; ?>
        
        <?php echo $form->labelEx($model,'phone') ?>
        <?php echo $form->textField($model,'phone',array('class'=>"phone_number_only no_phone_number_text w-800 $PreventPaste PhoneExtPaste")); ?>
        <div class="add_new_item">
            <!--<input type="checkbox" class="no_phone_number" ><em>Không có số điện thoại</em>-->
        </div>
        <?php echo $form->error($model,'phone'); ?>
    </div>

    <div class="<?php echo $showSelectGas;?>">
        <div class="row <?php echo $displayHgd;?>">
            <div style="width: 100%;float: left;padding-bottom: 5px;padding-left: 140px;">
                <label style="color: red;width: auto; ">Chú Ý: Đây không phải là setup giá KH, đây chỉ là giá tham khảo cho kế toán<br>Sau khi KH được duyệt bạn phải vào setup giá riêng cho KH ở menu setup giá</label>
            </div>
            <?php echo $form->labelEx($model,'price') ?>
            <?php echo $form->dropDownList($model,'price', $model->mUsersRef->getListoptionPrice(), array('style'=>'width:376px','empty'=>'Select', 'class'=>'contact_output_price')); ?>
            <?php echo $form->error($model,'price'); ?>
        </div>

        <div class="row">
            <?php echo $form->labelEx($model,'price_other') ?>
            <?php echo $form->textField($model,'price_other',array('style'=>'width:376px', 'class'=>'item_l contact_output_price_other number_only number_only_v1', 'maxlength'=>10)); ?>
            <div class="help_number w-200"></div>
            <?php echo $form->error($model,'price_other'); ?>
        </div>
        <div class="row">
            <?php echo $form->labelEx($model->mUsersRef,'contact_person_name') ?>
            <?php echo $form->textField($model->mUsersRef,'contact_person_name',array('style'=>'width:376px', 'class'=>'')); ?>
            <?php echo $form->error($model->mUsersRef,'contact_person_name'); ?>
        </div>
        <div class="row">
            <?php echo $form->labelEx($model->mUsersRef,'contact_note') ?>
            <?php echo $form->textField($model->mUsersRef,'contact_note',array('style'=>'width:376px', 'class'=>'')); ?>
            <?php echo $form->error($model->mUsersRef,'contact_note'); ?>
        </div>

        <div class="row">
            <?php
            $aTypeCustomer = $model->getListDataTypeCustomer();
//            unset($aTypeCustomer[STORE_CARD_KH_MOI]);
                // vì cột is_maintain không dùng trong loại KH của thẻ kho nên ta sẽ dùng cho 1: KH bình bò, 2. KH mối
                echo $form->labelEx($model,'is_maintain'); ?>
            <?php echo $form->dropDownList($model,'is_maintain', $aTypeCustomer,array('class'=>"w-370 $classSelectTypeCustomer",'empty'=>'Select')); ?>
            <?php if($readonlyTypeCustomer): ?>
                <?php echo $model->getTypeCustomerText(); ?>
            <?php endif; ?>
            <?php echo $form->error($model,'is_maintain'); ?>
        </div>
        <div class="row <?php echo $displayGiaoKho;?>">
            <?php // cột first_char không dùng trong loại KH của thẻ kho nên ta sẽ dùng chia nhóm KH tiếp 1: CNDB
                echo $form->labelEx($model,'first_char', ['label'=>'Nhóm KH']); ?>
            <?php echo $form->dropDownList($model,'first_char', $model->getArrayGroup(),array('class'=>"w-370",'empty'=>'Select')); ?>
            <?php echo $form->error($model,'first_char'); ?>
        </div>

        <div class="row">
            <?php
                //May 14, 2015 vì cột channel_id chua dung cho muc dich gi ca
                echo $form->labelEx($model,'channel_id'); ?>
            <?php echo $form->dropDownList($model,'channel_id', Users::$STATUS_LAY_HANG, array('style'=>'width:376px')); ?>
            <?php echo $form->error($model,'channel_id'); ?>
        </div>

        <?php if(!$model->isNewRecord): ?>
        <div class="row">
            <?php echo $form->labelEx($model->mUsersRef,'reason_leave'); ?>
            <?php echo $form->textArea($model->mUsersRef,'reason_leave',array('style'=>'width:364px')); ?>
            <?php echo $form->error($model->mUsersRef,'reason_leave'); ?>
        </div>
        <?php endif;?>

        <div class="row">
                <?php echo Yii::t('translation', $form->labelEx($model,'sale_id')); ?>
                <?php echo $form->hiddenField($model,'sale_id'); ?>
                <?php // echo $form->dropDownList($model,'sale_id', Users::getArrUserByRole(ROLE_SALE, array('order'=>'t.code_bussiness', 'prefix_type_sale'=>1)),array('style'=>'width:500px','empty'=>'Select')); ?>
                <?php 
                    // widget auto complete search user customer and supplier
                    $aData = array(
                        'model'=>$model,
                        'field_customer_id'=>'sale_id',
                        'url'=> Yii::app()->createAbsoluteUrl('admin/ajax/search_sale'),
                        'name_relation_user'=>'sale',
                        'placeholder'=>'Nhập Tên Sale',
                        'ClassAdd' => 'w-500',
        //                        'fnSelectCustomer'=>'fnAfterSelectSaleOrAgent',
                    );
                    $this->widget('ext.SpaAutocompleteCustomer.SpaAutocompleteCustomer',
                        array('data'=>$aData));                                        
                ?>
                <?php echo $form->error($model,'sale_id'); ?>
        </div>

        <div class="row">
            <?php echo Yii::t('translation', $form->labelEx($model,'payment_day')); ?>
            <?php echo $form->dropDownList($model,'payment_day', GasTypePay::getArrAll(), array('style'=>'width:376px','empty'=>'Select')); ?>
            <?php echo $form->error($model,'payment_day'); ?>
        </div>

        <div class="row">
            <?php echo Yii::t('translation', $form->labelEx($model,'beginning')); ?>
            <div class="fix_number_help">
            <?php echo $form->textField($model,'beginning',array('size'=>30,'maxlength'=>12,'class'=>'number_only')); ?>
            <div class="help_number"></div>
            </div>
            <?php echo $form->error($model,'beginning'); ?>
        </div>
        <div class="clr"></div>
    </div>

    <div class="<?php echo $displayAdd;?>">
        <div class="row">
            <?php echo Yii::t('translation', $form->labelEx($model,'province_id')); ?>
            <?php echo $form->dropDownList($model,'province_id', GasProvince::getArrAll(),array('style'=>'width:376px','class'=>'','empty'=>'Select')); ?>
            <?php echo $form->error($model,'province_id'); ?>
        </div>
        <div class="row">
            <?php echo Yii::t('translation', $form->labelEx($model,'district_id')); ?>
            <?php echo $form->dropDownList($model,'district_id', GasDistrict::getArrAll($model->province_id),array('style'=>'width:376px', 'class'=>"float_l")); ?>
            <div class="add_new_item float_l">
                <?php if($canCreateAdd): ?>
                    <a class="iframe_create_district" href="<?php echo Yii::app()->createAbsoluteUrl('admin/ajax/iframe_create_district') ;?>">Tạo Mới Quận Huyện</a><em> (Nếu trong danh sách không có)</em>
                <?php else: ?>
                    <?php echo $textHelpAddress;?>
                <?php endif; ?>
            </div>
            <div class="clr"></div>
            <?php echo $form->error($model,'district_id'); ?>
        </div>

        <div class="clr"></div>

        <div class="row">
            <?php echo Yii::t('translation', $form->labelEx($model,'ward_id')); ?>
            <?php echo $form->dropDownList($model,'ward_id', GasWard::getArrAll($model->province_id, $model->district_id),array('style'=>'width:376px','empty'=>'Select')); ?>
            <?php if($canCreateAdd): ?>
            <div class="add_new_item"><a class="iframe_create_ward" href="<?php echo Yii::app()->createAbsoluteUrl('admin/ajax/iframe_create_ward') ;?>">Tạo Mới Phường Xã</a><em> (Nếu trong danh sách không có)</em></div>
            <?php endif; ?>
            <?php echo $form->error($model,'ward_id'); ?>
        </div>	

        <div class="row">
            <?php echo $form->labelEx($model,'house_numbers') ?>
            <?php echo $form->textField($model,'house_numbers',array('style'=>'width:369px','maxlength'=>100)); ?>
            <?php echo $form->error($model,'house_numbers'); ?>
        </div> 	

        <div class="row">
            <?php echo $form->labelEx($model,'street_id') ?>
            <?php echo $form->hiddenField($model,'street_id',array('style'=>'width:369px','maxlength'=>100)); ?>
            <?php 
                $this->widget('zii.widgets.jui.CJuiAutoComplete', array(
                        'attribute'=>'autocomplete_name_street',
                        'model'=>$model,
                        'sourceUrl'=>Yii::app()->createAbsoluteUrl('admin/ajax/autocomplete_data_streets'),
                        'options'=>array(
                                'minLength'=>MIN_LENGTH_AUTOCOMPLETE,
                                'multiple'=> true,
                                'search'=>"js:function( event, ui ) {
                                        $('#Users_autocomplete_name_street').addClass('grid-view-loading-gas');
                                        } ",
                                'response'=>"js:function( event, ui ) {
                                            var json = $.map(ui, function (value, key) { return value; });
                                            if(json.length<1){
                                                var error = '<div class=\'errorMessage clr autocomplete_name_text\'>Không tìm thấy dữ liệu.</div>';
                                                if($('.autocomplete_name_text').size()<1)
                                                        $('.autocomplete_name_error').parent('div').find('.add_new_item').after(error);
                                                else
                                                        $('.autocomplete_name_error').parent('div').find('.autocomplete_name_text').show();
                                                $('.remove_row_item').hide();
                                            }

                                        $('#Users_autocomplete_name_street').removeClass('grid-view-loading-gas');
                                        } ",
                                'select'=>"js:function(event, ui) {
                                        $('#Users_street_id').val(ui.item.id);
                                        var remove_div = '<span class=\'remove_row_item\' onclick=\'fnRemoveNameStreet(this)\'></span>';
                                        $('#Users_autocomplete_name_street').parent('div').find('.remove_row_item').remove();
                                        $('#Users_autocomplete_name_street').attr('readonly',true).after(remove_div);
                                        $('.autocomplete_name_error').parent('div').find('.autocomplete_name_text').hide();
                                }",
                        ),
                        'htmlOptions'=>array(
                            'class'=>'autocomplete_name_error',
                            'size'=>45,
                            'maxlength'=>45,
                            'style'=>'float:left;width:369px;',
                            'placeholder'=>'Nhập tên đường tiếng việt không dấu',                            
                        ),
                )); 
                ?> 
                <script>
                    function fnRemoveNameStreet(this_){
                        $(this_).parent('div').find('input').attr("readonly",false); 
                        $("#Users_autocomplete_name_street").val("");
                        $("#Users_street_id").val("");
                    }
                </script>
                <?php if($canCreateAdd): ?>
                <div class="add_new_item"><a class="iframe_create_street" href="<?php echo Yii::app()->createAbsoluteUrl('admin/ajax/iframe_create_street') ;?>">Tạo Mới Đường</a><em> (Nếu trong danh sách không có)</em></div>
                <?php endif; ?>
            <div class="clr"></div>
            <?php echo $form->error($model,'street_id'); ?>
        </div>
    </div>
    
</div>