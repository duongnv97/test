<?php
$this->breadcrumbs=array(
	Yii::t('translation','DS Khách Hàng')=>array('index'),
	Yii::t('translation','Nhập DS Từ Excel'),
);

$menus = array(		
        array('label'=> Yii::t('translation', 'Users Management') , 'url'=>array('index')),
);
$this->menu= ControllerActionsName::createMenusRoles($menus, $actions);
?>

<h1><?php echo Yii::t('translation', 'Nhập DS khách hàng từ Excel'); ?></h1>

<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/jquery.blockUI.js"></script>    
<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'users-form',
	'enableAjaxValidation'=>false,
        'enableClientValidation'=>true,
        'htmlOptions' => array('class'=>'', 'enctype' => 'multipart/form-data'),    
)); ?>
	
	<div class="row">
		<?php echo Yii::t('translation', $form->labelEx($model,'file_excel')); ?>
		<?php echo $form->fileField($model,'file_excel',array()); ?>
		<?php echo $form->error($model,'file_excel'); ?>
	</div>

	<div class="row buttons" style="padding-left: 115px;">
		        <?php $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType'=>'submit',
            'label'=>$model->isNewRecord ? Yii::t('translation', 'Tải Lên') : Yii::t('translation', 'Save'),
            'type'=>'null', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
            'size'=>'small', // null, 'large', 'small' or 'mini'
            //'htmlOptions' => array('style' => 'margin-bottom: 10px; float: right;'),
        )); ?>	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->


<script>
$(document).ready(function() {
    $('.buttons .btn').click(function(){        
		$.blockUI({ overlayCSS: { backgroundColor: '<?php echo BLOCK_UI_COLOR;?>' } });         
    });    
})
</script>
