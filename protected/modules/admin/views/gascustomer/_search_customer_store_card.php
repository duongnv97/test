<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

    <div class="row">
        <?php echo $form->label($model,'customer_id'); ?>
        <?php echo $form->hiddenField($model,'customer_id'); ?>
        <?php 
            // widget auto complete search user customer and supplier
            $aData = array(
                'model'=>$model,
                'field_customer_id'=>'customer_id',
                'name_relation_user'=>'customer',
                'url' => Yii::app()->createAbsoluteUrl('admin/ajax/search_user_by_code'),
                'ClassAdd' => 'w-500',
            );
            $this->widget('ext.SpaAutocompleteCustomer.SpaAutocompleteCustomer',
                array('data'=>$aData));                                        
        ?>
    </div>

    <?php if(Yii::app()->user->role_id!=ROLE_SALE):?>
    <div class="row">
        <?php echo Yii::t('translation', $form->label($model,'sale_id')); ?>
                <?php echo $form->hiddenField($model,'sale_id'); ?>
        <?php // echo $form->dropDownList($model,'sale_id', Users::getArrUserByRole(ROLE_SALE, array('order'=>'t.code_bussiness', 'prefix_type_sale'=>1)),array('style'=>'width:500px','empty'=>'Select')); ?>
                <?php 
                    // widget auto complete search user customer and supplier
                    $aData = array(
                        'model'=>$model,
                        'field_customer_id'=>'sale_id',
                        'field_autocomplete_name'=>'autocomplete_name_parent',
                        'url'=> Yii::app()->createAbsoluteUrl('admin/ajax/search_sale'),
                        'name_relation_user'=>'sale',
                        'placeholder'=>'Nhập Tên Sale',
                        'ClassAdd' => 'w-500',
    //                        'fnSelectCustomer'=>'fnAfterSelectSaleOrAgent',
                    );
                    $this->widget('ext.SpaAutocompleteCustomer.SpaAutocompleteCustomer',
                        array('data'=>$aData));                                        
                ?>
        <?php echo $form->error($model,'sale_id'); ?>
    </div>
    <?php endif;?>
    
    <?php if(Yii::app()->user->role_id!=ROLE_SUB_USER_AGENT):?>
        
    <?php endif;?>
    
    <div class="row">
        <?php echo $form->labelEx($model,'agent_id_search'); ?>
        <?php echo $form->hiddenField($model,'agent_id_search', array('class'=>'')); ?>
        <?php
            // 1. limit search kh của sale
            $url = Yii::app()->createAbsoluteUrl('admin/ajax/search_by_role', array('role'=> ROLE_AGENT));
            // widget auto complete search user customer and supplier
            $aData = array(
                'model'=>$model,
                'field_customer_id'=>'agent_id_search',
                'url'=> $url,
                'name_relation_user'=>'ward',
                'ClassAdd' => 'w-500',
                'field_autocomplete_name' => 'autocomplete_name_street',
                'placeholder'=>'Nhập mã hoặc tên đại lý',
            );
            $this->widget('ext.SpaAutocompleteCustomer.SpaAutocompleteCustomer',
                array('data'=>$aData));
            ?>
    </div>
    <div class="row">
        <?php echo $form->label($model,'parent_id'); ?>
        <?php echo $form->hiddenField($model,'parent_id'); ?>
        <?php 
            // widget auto complete search user customer and supplier
            $aData = array(
                'model'=>$model,
                'field_autocomplete_name'=>'file_excel',
                'field_customer_id'=>'parent_id',
                'name_relation_user'=>'rParent',
                'ClassAdd' => 'w-500',
                'url' => Yii::app()->createAbsoluteUrl('admin/ajax/search_user_by_code'),
            );
            $this->widget('ext.SpaAutocompleteCustomer.SpaAutocompleteCustomer',
                array('data'=>$aData));                                        
        ?>
    </div>
    <div class="row more_col">
        <div class="col1">
            <?php echo $form->label($model,'username', array('label'=>'Username KH')); ?>
            <?php echo $form->textField($model,'username',array('class'=>'w-200 float_l')); ?>
        </div>	
    </div>
    
    <div class="row more_col">
        <div class="col1">
            <?php echo $form->label($model,'is_maintain', array('label'=>'Loại KH')); ?>
            <?php echo $form->dropDownList($model,'is_maintain', CmsFormatter::$CUSTOMER_BO_MOI,array('empty'=>'Select', 'class' => 'w-200')); ?>
            <?php echo $form->error($model,'is_maintain'); ?>
        </div>	

        <div class="col2">
            <?php echo $form->label($model,'assign_employee_sales'); ?>
            <?php echo $form->dropDownList($model,'assign_employee_sales', CmsFormatter::$yesNoFormat,array('class'=>'w-200', 'style'=>'','empty'=>'Select')); ?>
        </div>
        <div class="col3">
            <?php echo $form->label($model,'channel_id'); ?>
            <?php echo $form->dropDownList($model,'channel_id', Users::$STATUS_LAY_HANG,array('class'=>'w-200', 'empty'=>'Select')); ?>
            <?php echo $form->error($model,'channel_id'); ?>
	</div>	
    </div>
    
    
<!--    	<div class="row">
            <?php echo $form->label($model,'code_bussiness',array('label'=>'Mã KH')); ?>
            <?php echo $form->textField($model,'code_bussiness',array('size'=>30,'maxlength'=>20,'style'=>'float:left;width:342px;')); ?>
	</div>  
    	<div class="row">
            <?php echo $form->label($model,'code_account',array('label'=>'Mã Hệ Thống')); ?>
            <?php echo $form->textField($model,'code_account',array('size'=>30,'maxlength'=>20,'style'=>'float:left;width:342px;')); ?>
	</div>-->
    
        
    <div class="row more_col">
        <div class="col1">
            <?php echo $form->labelEx($model,'first_char', ['label'=>'Nhóm KH']); ?>
            <?php echo $form->dropDownList($model,'first_char', $model->getArrayGroup(),array('class'=>"w-200",'empty'=>'Select')); ?>
        </div>
        <div class="col2">
            <?php echo $form->labelEx($model,'province_id'); ?>
            <?php echo $form->dropDownList($model,'province_id', GasProvince::getArrAll(),array('class'=>'w-200', 'empty'=>'Select')); ?>
        </div>

        <div class="col3">
            <?php echo $form->labelEx($model,'district_id'); ?>
            <?php echo $form->dropDownList($model,'district_id', GasDistrict::getArrAll($model->province_id),array('class'=>'w-200', 'empty'=>'Select')); ?>
        </div>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model,'phone_ext2', ['label' => 'Chọn nhiều tỉnh']);// lấy tạm phone_ext2 làm search multi tỉnh ?>
        <div class="fix-label">
            <?php
               $this->widget('ext.multiselect.JMultiSelect',array( 
                     'model'=>$model,
                     'attribute'=>'phone_ext2',
                     'data'=> GasProvince::getArrAll(),
                     // additional javascript options for the MultiSelect plugin
                    'options'=>array('selectedList' => 30,),
                     // additional style
                     'htmlOptions'=>array('style' => 'width: 800px;'),
               ));    
           ?>
        </div>
    </div>


    <div class="row buttons" style="padding-left: 159px;">
        <?php $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType'=>'submit',
            'label'=>Yii::t('translation','Search'),
            'type'=>'null', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
            'size'=>'small', // null, 'large', 'small' or 'mini'
            //'htmlOptions' => array('style' => 'margin-bottom: 10px; float: right;'),
        )); ?>	
    </div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->

<script>
$(function(){
    fnBindChangeProvince('<?php echo BLOCK_UI_COLOR;?>', "<?php echo Yii::app()->createAbsoluteUrl('admin/ajax/get_slt_district');?>");
});
</script>