<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
//	'action'=> GasCheck::getCurl(),
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>
    <?php if(Yii::app()->user->role_id!=ROLE_SUB_USER_AGENT):?>
        <div class="row">
        <?php echo $form->labelEx($model,'agent_id_search'); ?>
        <?php echo $form->hiddenField($model,'agent_id_search', array('class'=>'')); ?>
        <?php
                // 1. limit search kh của sale
                $url = Yii::app()->createAbsoluteUrl('admin/ajax/search_by_role', array('role'=> ROLE_AGENT));
                // widget auto complete search user customer and supplier
                $aData = array(
                    'model'=>$model,
                    'field_customer_id'=>'agent_id_search',
                    'url'=> $url,
                    'name_relation_user'=>'by_agent',
                    'ClassAdd' => 'w-400',
                    'field_autocomplete_name' => 'autocomplete_name',
                    'placeholder'=>'Nhập mã hoặc tên đại lý',
                );
                $this->widget('ext.SpaAutocompleteCustomer.SpaAutocompleteCustomer',
                    array('data'=>$aData));
                ?>
        </div>
    <?php endif; ?>
    
    <div class="row">
    <?php echo $form->labelEx($model,'created_by', array('label'=>'Nhân viên CCS')); ?>
    <?php echo $form->hiddenField($model,'created_by', array('class'=>'')); ?>
    <?php
            // 1. limit search kh của sale
            $url = Yii::app()->createAbsoluteUrl('admin/ajax/search_by_role', array('role'=> implode(',', [ROLE_EMPLOYEE_MAINTAIN, ROLE_EMPLOYEE_MARKET_DEVELOPMENT, ROLE_SALE, ROLE_MONITORING_MARKET_DEVELOPMENT])));
            // widget auto complete search user customer and supplier
            $aData = array(
                'model'=>$model,
                'field_customer_id'=>'created_by',
                'url'=> $url,
                'name_relation_user'=>'rCreatedBy',
                'ClassAdd' => 'w-400',
                'field_autocomplete_name' => 'autocomplete_name_parent',
                'placeholder'=>'Nhập mã NV hoặc tên',
            );
            $this->widget('ext.SpaAutocompleteCustomer.SpaAutocompleteCustomer',
                array('data'=>$aData));
            ?>
    </div>
    
    <div class="row more_col">
        <div class="col1">
                <?php echo Yii::t('translation', $form->label($model,'date_from', array('label' => "Từ ngày" ))); ?>
                <?php 
                    $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                        'model'=>$model,        
                        'attribute'=>'date_from',
                        'options'=>array(
                            'showAnim'=>'fold',
                            'dateFormat'=> MyFormat::$dateFormatSearch,
//                            'minDate'=> '0',
                            'maxDate'=> '0',
                            'changeMonth' => true,
                            'changeYear' => true,
                            'showOn' => 'button',
                            'buttonImage'=> Yii::app()->theme->baseUrl.'/admin/images/icon_calendar_r.gif',
                            'buttonImageOnly'=> true,                                
                        ),        
                        'htmlOptions'=>array(
                            'class'=>'w-16',
                            'size'=>'16',
                            'style'=>'float:left;',
                            'readonly' => 1,
                        ),
                    ));
                ?>     		
        </div>
        <div class="col2">
                <?php echo Yii::t('translation', $form->label($model,'date_to', array('label' => "Đến ngày" ))); ?>
                <?php 
                    $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                        'model'=>$model,        
                        'attribute'=>'date_to',
                        'options'=>array(
                            'showAnim'=>'fold',
                            'dateFormat'=> MyFormat::$dateFormatSearch,
//                            'minDate'=> '0',
                            'maxDate'=> '0',
                            'changeMonth' => true,
                            'changeYear' => true,
                            'showOn' => 'button',
                            'buttonImage'=> Yii::app()->theme->baseUrl.'/admin/images/icon_calendar_r.gif',
                            'buttonImageOnly'=> true,                                
                        ),        
                        'htmlOptions'=>array(
                            'class'=>'w-16',
                            'size'=>'16',
                            'style'=>'float:left;',
                            'readonly' => 1,
                        ),
                    ));
                ?>     		
        </div>
    </div>
    <div class="row more_col">
        <div class="col1">
            <?php echo $form->label($model,'parent_id', array('label'=>'Loại điểm')); ?>
            <?php echo $form->dropDownList($model,'parent_id', UsersExtend::getHgdTypePoint(), array('class'=>'w-200', 'empty'=>'Select')); ?>
        </div>
        <div class="col2">
            <?php echo $form->labelEx($model,'province_id'); ?>
            <?php echo $form->dropDownList($model,'province_id', GasProvince::getArrAll(),array('class'=>'w-200','empty'=>'Select')); ?>
        </div>
        <div class="col3">
            <?php echo $form->labelEx($model,'district_id'); ?>
            <?php echo $form->dropDownList($model,'district_id', GasDistrict::getArrAll($model->province_id),array('class'=>'w-200','empty'=>'Select')); ?>
        </div>
        
    </div>
    <div class="row more_col">
        <div class="col1">
            <?php // echo $form->label($model,'created_by', array('label'=>'Nhân viên CCS')); ?>
            <?php // echo $form->dropDownList($model,'created_by', Users::getSelectByRoleForAgent($model->monitoring_id, ONE_MONITORING_MARKET_DEVELOPMENT, "", array('status'=>1)),array('class'=>'category_ajax w-200','empty'=>'Select')); ?>
            <?php // echo $form->label($model,'monitoring_id', array('label'=>'CV CCS')); ?>
            <?php // echo $form->dropDownList($model,'monitoring_id', Users::getSelectByRole(ROLE_MONITORING_MARKET_DEVELOPMENT),array('class'=>'w-300','empty'=>'Select')); ?>
            <?php echo $form->label($model,'is_maintain', array('label'=>'Loại KH')); ?>
            <?php echo $form->dropDownList($model,'is_maintain', CmsFormatter::$CUSTOMER_BO_MOI,array('empty'=>'Select', 'class' => 'w-200')); ?>
            <?php echo $form->error($model,'is_maintain'); ?>
        </div>
        <div class="col2">
            <?php echo $form->labelEx($model,'pageSize', array('label'=>'Số KH hiển thị')); ?>
            <?php echo $form->dropDownList($model,'pageSize', GasConst::getPageSize(),array('class'=>'w-200','empty'=>'Select')); ?>
        </div>
        <div class="col3">
        </div>
    </div>
    
    <div class="row buttons" style="padding-left: 159px;">
        <?php $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType'=>'submit',
            'label'=>'Xem',
            'type'=>'null', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
            'size'=>'small', // null, 'large', 'small' or 'mini'
        //'htmlOptions' => array('style' => 'margin-bottom: 10px; float: right;'),
    )); ?>	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->
<script>
$(function(){
    fnBindChangeProvince('<?php echo BLOCK_UI_COLOR;?>', "<?php echo Yii::app()->createAbsoluteUrl('admin/ajax/get_slt_district');?>");
});
</script>