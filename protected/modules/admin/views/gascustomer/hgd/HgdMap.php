<?php
$this->breadcrumbs=array(
    $this->pageTitle,
);
?>

<h1><?php echo $this->pageTitle;?></h1>

<div class="search-form" style="">
<?php $this->renderPartial('hgd/_search',array(
        'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php // if(!empty($model->created_by)): ?>
<?php if(1): ?>

<?php 
try{
    $models = UsersS1::searchHgd($model);
}catch (Exception $exc){
    GasCheck::CatchAllExeptiong($exc);
}
?>

<?php if(count($models)): ?>
    <h1>Tổng số KH tìm thấy: <?php echo count($models);?></h1>
    <?php
        $index = 1; $aDataChart = array(); $tmp = array();
        foreach($models as $mUser):
            if(trim($mUser->slug) != ""){
                $tmp = explode(",", $mUser->slug);
                if(count($tmp) == 2){
                    $moreInfo = UsersS1::getHgdInfo($mUser);
                    $contentString = "<div id='content'>";
                    $contentString .= "<h1 id='firstHeading' class='firstHeading'>{$mUser->getFullName()}</h1>";
                        $contentString .= "<div id='bodyContent'>";
                            $contentString .= "<p><b>Địa chỉ:</b> {$mUser->getAddress()}</p>";
                            $contentString .= "<p><b>Phone:</b> {$mUser->getPhone()}</p>";
                            $contentString .= "<p><b>Ngày tạo:</b> ".MyFormat::dateConverYmdToDmy($mUser->created_date, 'd/m/Y H:i')."</p>";
                            $contentString .= "<p><b>Thông tin:</b> $moreInfo</p>";
                        $contentString .= "</div>";
                    $contentString .= "</div>";

                    $aDataChart[] = array($contentString, $tmp[0], $tmp[1], $index++);
                }
            }
        endforeach;
        $js_array = json_encode($aDataChart);
    ?>
    <script src="http://maps.google.com/maps/api/js?key=AIzaSyCfsbfnHgTh4ODoXLXFfEFhHskDhnRVtjQ" type="text/javascript"></script>
    <div id="map" style="width: 80%; height: 600px;"></div>
    <script type="text/javascript">
        var locations = <?php echo $js_array;?>;
        var map = new google.maps.Map(document.getElementById('map'), {
          zoom: 15,
          center: new google.maps.LatLng(<?php echo isset($tmp[0]) ? $tmp[0] : 10.818463;?>, <?php echo isset($tmp[1]) ? $tmp[1] : 106.658825;?>),
          mapTypeId: google.maps.MapTypeId.ROADMAP
        });

        var infowindow = new google.maps.InfoWindow();

        var marker, i;
    //    var image = 'http://daukhi.huongminhgroup.com/apple-touch-icon.png';

        for (i = 0; i < locations.length; i++) {  
          marker = new google.maps.Marker({
            position: new google.maps.LatLng(locations[i][1], locations[i][2]),
            map: map
    //        icon: image
          });

          google.maps.event.addListener(marker, 'click', (function(marker, i) {
            return function() {
              infowindow.setContent(locations[i][0]);
              infowindow.open(map, marker);
            }
          })(marker, i));
        }
      </script>

<?php else: ?>
<p class="f_size_15 item_c">Không có dữ liệu</p>
<?php endif; ?>

<?php endif; ?>