<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'code_account',array()); ?>
		<?php echo $form->textField($model,'code_account',array('size'=>30,'maxlength'=>20,'style'=>'float:left;')); ?>
	</div>	
	
	<div class="row">
		<?php echo $form->label($model,'code_bussiness',array()); ?>
		<?php echo $form->textField($model,'code_bussiness',array('size'=>30,'maxlength'=>20,'style'=>'float:left;')); ?>
	</div>
	
	<div class="row">
		<?php echo $form->label($model,'first_name',array()); ?>
		<?php echo $form->textField($model,'first_name',array('size'=>30,'maxlength'=>50,'style'=>'float:left;')); ?>
	</div>
	<div class="row">
		<?php echo $form->label($model,'name_agent',array()); ?>
		<?php echo $form->textField($model,'name_agent',array('size'=>30,'maxlength'=>300)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'phone',array()); ?>
		<?php echo $form->textField($model,'phone',array('size'=>55,'maxlength'=>500,'style'=>'')); ?>
	</div>	
	
	<div class="row">
		<?php echo $form->label($model,'address',array()); ?>
		<?php echo $form->textField($model,'address',array('size'=>55,'maxlength'=>500,'style'=>'')); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'province_id',array()); ?>
		<?php echo $form->dropDownList($model,'province_id', GasProvince::getArrAll(),array('empty'=>'select','style'=>'float:left;')); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'channel_id',array()); ?>
		<?php echo $form->dropDownList($model,'channel_id', GasChannel::getArrAll(),array('empty'=>'select')); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'sale_id',array()); ?>
		<?php echo $form->dropDownList($model,'sale_id', Users::getArrUserByRole(ROLE_SALE),array('empty'=>'select','style'=>'float:left;')); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'payment_day',array()); ?>
		<?php echo $form->textField($model,'payment_day'); ?>
	</div>

	<div class="row buttons" style="padding-left: 159px;">
        <?php $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType'=>'submit',
            'label'=>Yii::t('translation','Search'),
            'type'=>'null', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
            'size'=>'small', // null, 'large', 'small' or 'mini'
            //'htmlOptions' => array('style' => 'margin-bottom: 10px; float: right;'),
        )); ?>	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->