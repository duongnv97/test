<!--<div class="BoxSetupPrice <?php echo $displayGeneral; // $hideSetupGas;?>">-->
<div class="BoxSetupPrice <?php echo $showSelectGas; // $hideSetupGas;?>">
<?php $needMore = array('ListIdName'=>1); 
    $aTypeIdVo      = GasConst::getMaterialsTypeVoOfCustomer();
    $model->mUsersRef->getGasBrandFromJson();
    $maxlengthEmail = 150;
    $maxlengthPhone = 23;
    if($cRole == ROLE_ADMIN){
        $maxlengthEmail = 500;
        $maxlengthPhone = 200;
    }
    $aUidAllowEditPhone = [GasConst::UID_PHUONG_NT, GasConst::UID_TRAM_PTN, GasLeave::UID_HEAD_GAS_BO];
    $readonlyEditPhone = '';
//    if($cRole != ROLE_ADMIN && !$model->isNewRecord && $model->id < 1355018 && !in_array($cUid, $aUidAllowEditPhone)){
//        $readonlyEditPhone = '1';
//    }// Mar1518 không check nữa, để user tự do eddit phone, vì không open cho sale nhập nữa

?>
<h3 class='title-info'>Thông tin nhận báo giá cho KH Bò, Mối</h3>
<div class="row fix_custom_label_required ">
    <?php echo $form->labelEx($model,'storehouse_id', array()); ?>
    <?php echo $form->dropDownList($model,'storehouse_id', $model->getListCompany(),array('class'=>'w-500','empty'=>'Select')); ?>
    <?php echo $form->error($model,'storehouse_id'); ?>
</div> 

<div class="row">
    <div style="width: 100%;float: left;padding-bottom: 2px;padding-left: 140px;">
        <label style="color: red;width: auto; ">Có thể nhập nhiều email, mỗi email cách nhau bởi dấu ; vd: spj@gmail.com;dungnt@spj.vn;kiennn@spj.vn</label>
    </div>
    <div style="width: 100%;float: left;padding-bottom: 5px;padding-left: 140px;">
        <label style="color: red;width: auto; ">Nhập email khách hàng, nếu không có thì bỏ trống</label>
    </div>
    <?php echo $form->labelEx($model->mUsersRef,'contact_boss_mail') ?>
    <?php echo $form->textField($model->mUsersRef,'contact_boss_mail',array('style'=>'', 'class'=>'w-800', 'maxlength'=>$maxlengthEmail)); ?>
    <?php echo $form->error($model->mUsersRef,'contact_boss_mail'); ?>
</div>

<div class="row fix_custom_label_required">
    <?php echo $form->labelEx($model,'phone_ext2') ?>
    <?php echo $form->textField($model,'phone_ext2',array('class'=>"w-500 phone_number_only PhoneExt2", 'readonly'=> $readonlyEditPhone, 'maxlength'=>$maxlengthPhone)); ?>
    <div style="width: 100%;float: left;padding-bottom: 5px;padding-left: 140px;">
        <label style="color: red;width: auto; ">Mỗi số phone cách nhau bởi dấu - vd: 0123456789-0988180386</label>
        <!--<label style="color: red;width: auto; ">Chỉ các tỉnh: TPHCM, Đồng Nai, Bình Dương, Vũng Tàu yêu cầu nhập số di động báo giá</label>-->
    </div>
    <div style="width: 100%;float: left;padding-bottom: 5px;padding-left: 140px;">
        <label style="color: red;width: auto; ">Số nhận báo giá là số di động. Tối đa 2 số nhận báo giá</label>
    </div>
    <div class="clr"></div>
    <?php echo $form->error($model,'phone_ext2'); ?>
</div>

<div class="row fix_custom_label_required">
    <?php echo $form->labelEx($model,'phone_ext3') ?>
    <?php echo $form->textField($model,'phone_ext3',array('class'=>'float_l w-500 phone_number_only PhoneExtPaste', 'maxlength'=> ($maxlengthPhone) )); ?>
    <?php echo $form->checkBox($model,'buying',array('class'=>'float_l CopyPhoneExt2')); ?>
    <?php echo $form->labelEx($model,'buying',array('class'=>'checkbox_one_label w-300 l_padding_10', 'style'=>'padding-top:3px;', 'label'=>'Copy số báo giá')); ?>
    <div style="width: 100%;float: left;padding-bottom: 5px;padding-left: 140px;">
        <label style="color: red;width: auto; ">Số nhận nhân tin bảo trì là số di động. Tối đa 2 số </label>
    </div>
    <div class="clr"></div>
    <?php echo $form->error($model,'phone_ext3'); ?>
</div>

<div class="row fix_custom_label_required">
    <?php echo $form->labelEx($model,'phone_ext4') ?>
    <?php echo $form->textField($model,'phone_ext4',array('class'=>'float_l w-500 phone_number_only PhoneExtPaste', 'maxlength'=> 12 )); ?>
    <?php echo $form->checkBox($model,'tempField1',array('class'=>'float_l CopyPhoneExt2')); ?>
    <?php echo $form->labelEx($model,'tempField1',array('class'=>'checkbox_one_label w-300 l_padding_10', 'style'=>'padding-top:3px;', 'label'=>'Copy số báo giá')); ?>
    <div style="width: 100%;float: left;padding-bottom: 5px;padding-left: 140px;">
        <label style="color: red;width: auto; ">Số nhận nhân tin thu tiền công nợ là số di động. Tối đa 1 số </label>
    </div>
    <div class="clr"></div>
    <?php echo $form->error($model,'phone_ext4'); ?>
</div>

<div class="row">
    <?php echo $form->labelEx($model->mUsersRef,'contact_boss_fax') ?>
    <?php echo $form->textField($model->mUsersRef,'contact_boss_fax',array('class'=>'w-500 phone_number_only', 'maxlength'=>35)); ?>
    <?php echo $form->error($model->mUsersRef,'contact_boss_fax'); ?>
</div>

</div> <!-- <div class="BoxSetupPrice -->
<script>
    $(function(){

    });
    
</script>