<div class="BoxSetupGasOfCustomer <?php echo $hideSetupGas;?>">
<?php 
    $mMaterials = new GasMaterials();
    $needMore   = array('ListIdName'=>1, 'listId' => $mMaterials->getIdSetupBoMoi()); 
    $aTypeIdVo      = GasConst::getMaterialsTypeVoOfCustomer();
    $model->mUsersRef->getGasBrandFromJson();
    
?>
<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/print-store-card.css" />
<h3 class='title-info'>Thương hiệu bán + vỏ - chỉ setup cho KH Bò, Mối</h3>
<?php echo $form->error($model,'first_name'); ?>

<div class="row fix_custom_label_required display_none">
    <?php echo $form->labelEx($model,'application_id', array('label'=>'Line Gas')); ?>
    <?php echo $form->dropDownList($model,'application_id', Forecast::getArrayLine(),array('style'=>'width:376px','empty'=>'Select')); ?>
    <?php echo $form->error($model,'application_id'); ?>
</div>

<table class="tb">
    <thead>
        <tr>
            <td class="item_b item_c">Loại</td>
            <td class="item_b item_c w-400">Gas</td>
            <td class="item_b item_c w-400">Vỏ</td>
        </tr>
    </thead>
    <tbody>

<?php foreach (GasConst::getMaterialsTypeGasOfCustomer() as $materials_type_id => $typeName): ?>
    <tr>
        <td class="item_b item_c" colspan="3"><?php echo $typeName;?></td>
    </tr>
    <tr>
        <td>Chính</td>
        <td>
            <!--<input class="w-300 float_l" name="gas[<?php echo $materials_type_id;?>][]" value="" placeholder="Thương hiệu chính gas <?php echo $typeName;?>">-->
            <?php echo CHtml::dropDownList('gas['.$materials_type_id.'][]', $model->mUsersRef->getGasBrandKey($materials_type_id, UsersRef::PRIMARY_GAS), 
              GasMaterials::getArrayIdByMaterialTypeId($materials_type_id, $needMore),
                    array('empty'=> 'Thương hiệu chính gas '.$typeName));?>
        </td>
        <td>
            <!--<input class="w-300 float_l"  name="vo[<?php echo $materials_type_id;?>][]" value="" placeholder="Thương hiệu chính vỏ <?php echo $typeName;?>">-->
            <?php
                echo CHtml::dropDownList('vo[' . $materials_type_id . '][]', $model->mUsersRef->getGasBrandKey($materials_type_id, UsersRef::PRIMARY_VO),
                        GasMaterials::getArrayIdByMaterialTypeId($aTypeIdVo[$materials_type_id], $needMore), 
                        array('empty'=> 'Thương hiệu chính vỏ '.$typeName)
                    );
            ?>
        </td>
    </tr>
    <tr class="display_none">
        <td>Phụ</td>
        <td>
            <!--<input class="w-300 float_l"  name="gas[<?php echo $materials_type_id;?>][]" value="" placeholder="Thương hiệu phụ gas <?php echo $typeName;?>">-->
            <?php echo CHtml::dropDownList('gas['.$materials_type_id.'][]', $model->mUsersRef->getGasBrandKey($materials_type_id, UsersRef::SECOND_GAS), 
              GasMaterials::getArrayIdByMaterialTypeId($materials_type_id, $needMore),
                    array('empty'=> 'Thương hiệu phụ gas '.$typeName));?>
        </td>
        <td>
            <!--<input class="w-300 float_l"  name="vo[<?php echo $materials_type_id;?>][]" value="" placeholder="Thương hiệu phụ vỏ <?php echo $typeName;?>">-->
            <?php
                echo CHtml::dropDownList('vo[' . $materials_type_id . '][]', $model->mUsersRef->getGasBrandKey($materials_type_id, UsersRef::SECOND_VO),
                        GasMaterials::getArrayIdByMaterialTypeId($aTypeIdVo[$materials_type_id], $needMore), 
                        array('empty'=> 'Thương hiệu phụ vỏ '.$typeName)
                );
            ?>
        </td>
    </tr>
<?php endforeach; ?>
    </tbody>
</table>

</div>

<style>
    select{
        /*margin:40px;*/
        /*background: #81C784;*/
        /*color:#000;*/
        /*text-shadow:0 1px 0 rgba(0,0,0,0.4);*/
    }
    option:not(:checked) { 
        background-color: #FFF; 
    }    
</style>

<script>
    $(function(){
        makeBackgroundSelect();// Feb 02, 2017
    });
    function makeBackgroundSelect(){
        $('.BoxSetupGasOfCustomer').find('select').each(function(){
            if($(this).val() != ''){
                $(this).css({"background": "#81C784"});
            }
        });
    }
</script>