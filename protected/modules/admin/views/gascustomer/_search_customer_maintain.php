<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'code_bussiness',array('label'=>'Mã Khách Hàng')); ?>
		<?php echo $form->textField($model,'code_bussiness',array('size'=>30,'maxlength'=>20,'style'=>'float:left;width:342px;')); ?>
	</div>    
	<div class="row">
		<?php echo $form->label($model,'first_name',array()); ?>
		<?php echo $form->textField($model,'first_name',array('size'=>30,'maxlength'=>50,'style'=>'float:left;width:342px;')); ?>
	</div>
    
	<div class="row">
		<?php echo $form->label($model,'phone',array()); ?>
		<?php echo $form->textField($model,'phone',array('size'=>55,'maxlength'=>50,'style'=>'width:342px;')); ?>
	</div>	
    
	<div class="row">
		<?php echo Yii::t('translation', $form->label($model,'address')); ?>
		<?php echo $form->textField($model,'address',array('style'=>'width:342px;','maxlength'=>100)); ?>
		<?php echo $form->error($model,'address'); ?>
	</div>	

        <?php if(Yii::app()->user->role_id==ROLE_ADMIN):?>
            <div class="row">
                <?php echo Yii::t('translation', $form->labelEx($model,'agent_id_search')); ?>
                <?php echo $form->dropDownList($model,'agent_id_search', Users::getSelectByRole(),array('style'=>'width:350px;','empty'=>'Select')); ?>
            </div>    
        <?php endif;?>

	<div class="row">
		<?php CmsFormatter::$yesNoFormat[0]='Chưa Bảo Trì';?>
		<?php CmsFormatter::$yesNoFormat[1]='Đã Bảo Trì';?>
		<?php echo Yii::t('translation', $form->labelEx($model,'is_maintain')); ?>
		<?php echo $form->dropDownList($model,'is_maintain', CmsFormatter::$yesNoFormat,array('empty'=>'Select')); ?>
		<?php echo $form->error($model,'is_maintain'); ?>
	</div>	
	
	<div class="row buttons" style="padding-left: 159px;">
		        <?php $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType'=>'submit',
            'label'=>Yii::t('translation','Search'),
            'type'=>'null', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
            'size'=>'small', // null, 'large', 'small' or 'mini'
            //'htmlOptions' => array('style' => 'margin-bottom: 10px; float: right;'),
        )); ?>	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->