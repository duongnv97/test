<?php $displayHgd="display_none";
$cRole  = MyFormat::getCurrentRoleId();
$cUid   = MyFormat::getCurrentUid();
//echo '<pre>';
//print_r(GasScheduleSms::countByUserAndType($model->id, GasScheduleSms::TYPE_QUOTES_BO_MOI, ['LimitMonth'=>date('m')]));
//echo '</pre>';

?>
<?php // if(!in_array($model->is_maintain, CmsFormatter::$aTypeIdHgd) && $cRole != ROLE_SUB_USER_AGENT): ?>
<?php if($cRole != ROLE_SUB_USER_AGENT): ?>
    <?php $displayHgd=""; ?>
<?php endif; ?>

<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/jquery.blockUI.js"></script>
<?php Yii::app()->clientScript->registerCoreScript('jquery.ui'); ?>
<div class="form">
    <?php $form=$this->beginWidget('CActiveForm', array(
            'id'=>'maintain-form',
            'enableClientValidation'=>false,
        )); ?>
        <?php echo $form->errorSummary($model); ?>
        <?php echo $form->errorSummary($model->mUsersRef); ?>
        <?php if(Yii::app()->user->hasFlash('successUpdate')): ?>
                <div class="success_div"><?php echo Yii::app()->user->getFlash('successUpdate');?></div>         
        <?php endif; ?>     
        <p class="note">Dòng có dấu <span class="required">*</span> là bắt buộc.</p>
        <div class="row buttons" style="padding-left: 141px;">
            <?php $this->widget('bootstrap.widgets.TbButton', array(
                'buttonType'=>'submit',
                'label'=>$model->isNewRecord ? Yii::t('translation', 'Create') : Yii::t('translation', 'Save'),
                'type'=>'null', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
                'size'=>'small', // null, 'large', 'small' or 'mini'
                'htmlOptions' => array('class' => 'submit-blockui'),
            )); ?>
            <?php if($model->canResendSms()): ?>
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <a class='btn_cancel btn_closed_tickets'  alert_text="Chắc chắn muốn gửi lại SMS giá cho Khách Hàng?" href="<?php echo Yii::app()->createAbsoluteUrl("admin/gascustomer/update_customer_store_card", array('id'=>$model->id, 'ResendSms'=>1)); ?>">Gửi lại SMS giá cho KH</a>
            <?php endif; ?>
            <?php if($cRole == ROLE_ADMIN): ?>
                <a class='btn_cancel btn_closed_tickets'  alert_text="Chắc chắn muốn cập nhật lại loại KH cho BC công nợ tổng quát?" href="<?php echo Yii::app()->createAbsoluteUrl("admin/gascustomer/update_customer_store_card", array('id'=>$model->id, 'UpdateReportDebit'=>1)); ?>">Cập nhật lại loại KH cho BC công nợ tổng quát</a>
            <?php endif; ?>
        </div>
        <div id="tabs" class="grid-view">
            <ul>
                <li>
                    <a class="tab_1" href="#tabs-1">Khách Hàng</a>
                </li>
                <li>
                    <a class="tab_2" href="#tabs-2">Thông Tin Liên Hệ</a>
                </li>
            </ul>

            <?php include '_form_store_card_1.php';?>
            <?php include '_form_store_card_2.php';?>
            
        </div>
        <div class="row buttons" style="padding-left: 141px;">
            <?php $this->widget('bootstrap.widgets.TbButton', array(
                'buttonType'=>'submit',
                'label'=>$model->isNewRecord ? Yii::t('translation', 'Create') : Yii::t('translation', 'Save'),
                'type'=>'null', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
                'size'=>'small', // null, 'large', 'small' or 'mini'
                'htmlOptions' => array('class' => 'submit-blockui'),
            )); ?>
            <?php if($model->canRejectCreateCustomer()): ?>
                <a class="btn_cancel btn_closed_tickets" style="margin-left: 50px;" alert_text='Bạn chắc chắn muốn xóa khách hàng này?' href="<?php echo Yii::app()->createAbsoluteUrl("admin/gascustomer/update_customer_store_card", array('id'=>$model->id, 'reject'=>1)); ?>">Hủy bỏ</a>
            <?php endif; ?>
        </div>

        <?php $this->endWidget(); ?>
        <br>
        <?php $this->widget('TokenWidget', array('user_id' => $model->id)); ?>

        <div class="clr"></div>
        <div class="row selectAgent">
            <label>Reset pass</label>
            <div class="f_size_15" style="padding-left: 141px;"><?php echo $model->getEventLog();?></div>
        </div>
        <div class="clr"></div>
        <?php $this->widget('CustomerChangeInfoWidget', array('customer_id' => $model->id)); ?>
        <?php CronUpdate::devPrintDbData($model);?>
</div><!-- form -->

<style>
    .add_new_item {display:inline;padding-left:20px;}
    .title-info {padding:10px}
</style>

<script>
    $(document).ready(function(){
        $( "#tabs" ).tabs();
        
        $('.form').find('button:submit').click(function(){
            $.blockUI({ overlayCSS: { backgroundColor: '<?php echo BLOCK_UI_COLOR;?>' } }); 
        });
        $('.customer_store_card, .customerSaleStoreCard, .vip240, .hgd').addClass('index').find('span').text('Quản Lý');
        
        $('#Users_province_id').live('change',function(){
            var province_id = $(this).val();        
            var url_ = "<?php echo Yii::app()->createAbsoluteUrl('admin/ajax/get_slt_district');?>";
            $.blockUI({ overlayCSS: { backgroundColor: '<?php echo BLOCK_UI_COLOR;?>' } }); 
            $.ajax({
                url: url_,
                data: {ajax:1,province_id:province_id},
                type: "get",
                dataType:'json',
                success: function(data){
                    $('#Users_district_id').html(data['html_district']);                
                    $.unblockUI();
                }
            });

        });   		
	
        $('#Users_district_id').live('change',function(){
            var province_id = $('#Users_province_id').val();   
            var district_id = $(this).val();        
            var url_ = "<?php echo Yii::app()->createAbsoluteUrl('admin/ajax/get_slt_ward');?>";
            $.blockUI({ overlayCSS: { backgroundColor: '<?php echo BLOCK_UI_COLOR;?>' } }); 
            $.ajax({
                    url: url_,
                    data: {ajax:1,district_id:district_id,province_id:province_id},
                    type: "get",
                    dataType:'json',
                    success: function(data){
                            $('#Users_ward_id').html(data['html_district']);                
                            $.unblockUI();
                    }
            });
        });


        fnUpdateColorbox();
        fnAddReadonlyStreet();
        fnBindChangePrice();
        $('.contact_output_price').trigger('change');
        fnBindFixLabelRequired();
        fnBindRemoveIcon();
        copyPhoneBaoGia();
        $('#Users_assign_employee_sales').closest('.unique_wrap_autocomplete ').find('.remove_row_item').trigger('click');
    });
    
    function fnUpdateColorbox(){    
		<?php if($model->isNewRecord):?>	
//			$('#Users_province_id').trigger('change');
//			$('#Users_district_id').trigger('change');
		<?php endif;?>	
        $(".iframe_create_district").colorbox({iframe:true,innerHeight:'400', innerWidth: '700',close: "<span title='close'>close</span>"});
        $(".iframe_create_ward").colorbox({iframe:true,innerHeight:'400', innerWidth: '700',close: "<span title='close'>close</span>"});
        $(".iframe_create_street").colorbox({iframe:true,innerHeight:'400', innerWidth: '700',close: "<span title='close'>close</span>"});        
    }    
	
    function fnAddReadonlyStreet(){
        <?php if(!$model->isNewRecord && !empty($model->street_id)):?>
        var remove_div = '<span class=\'remove_row_item\' onclick=\'fnRemoveNameStreet(this)\'></span>';
        $('#Users_autocomplete_name_street').parent('div').find('.remove_row_item').remove();
        $('#Users_autocomplete_name_street').attr('readonly',true).after(remove_div);
        <?php endif;?>
    }

    function fnBindChangePrice(){
        $('.contact_output_price').change(function(){
            var price = $(this).val();                
            if(price == <?php echo UsersRef::PRICE_OTHER; ?>){
                $('.contact_output_price_other').closest('.row').show();
            }else{
                $('.contact_output_price_other').val("").closest('.row').hide();
            }
        });
    }

    function fnCallSomeFunctionAfterSelectV2(ui, idField, idFieldCustomer){
        var tr = $('.TableCopyRow').find('.trClone').clone();
        var classCheck = 'CheckCopyRow'+ui.item.id;
        if($('.TbAddAccount').find('.'+classCheck).size() > 0){
            return ;
        }
        tr.find('.customer_name').html(ui.item.label);
        tr.find('.customer_id').val(ui.item.id).addClass(classCheck);
        tr.find('.customer_address').html(ui.item.address);
        $('.TbAddAccount').find('tbody').append(tr);
        fnRefreshOrderNumber();
    }
	
    /**
    * @Author: ANH DUNG Dec 28, 2017
    * @Todo: check box copy phone báo giá xuống phone bảo trì 
    */
    function copyPhoneBaoGia(){
        $('.CopyPhoneExt2').click(function(){
            var divParent = $(this).closest('.row');
            if($(this).is(':checked')){
                divParent.find('.PhoneExtPaste').val($('.PhoneExt2').val());
            }else{
                divParent.find('.PhoneExtPaste').val('');
            }
        });
    }
    
</script>