<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/jquery.blockUI.js"></script>    
<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'users-form',
	'enableAjaxValidation'=>false,
)); ?>

	<?php echo Yii::t('translation', '<p class="note">Fields with <span class="required">*</span> are required.</p>'); ?>
	
	<div class="row">
		<?php echo Yii::t('translation', $form->labelEx($model,'code_account')); ?>
		<?php echo $form->textField($model,'code_account',array('size'=>20,'maxlength'=>20)); ?>
		<?php echo $form->error($model,'code_account'); ?>
	</div>

	<div class="row">
		<?php // echo Yii::t('translation', $form->labelEx($model,'code_bussiness')); ?>
		<?php // echo $form->textField($model,'code_bussiness',array('size'=>20,'maxlength'=>20)); ?>
		<?php // echo $form->error($model,'code_bussiness'); ?>
	</div>

	<div class="row">
		<?php echo Yii::t('translation', $form->labelEx($model,'first_name')); ?>
		<?php echo $form->textField($model,'first_name',array('size'=>80,'maxlength'=>150)); ?>
		<?php echo $form->error($model,'first_name'); ?>
	</div>

	<div class="row">
		<?php echo Yii::t('translation', $form->labelEx($model,'name_agent')); ?>
		<?php echo $form->textField($model,'name_agent',array('size'=>80,'maxlength'=>300)); ?>
		<?php echo $form->error($model,'name_agent'); ?>
	</div>

    
	<div class="row">
		<?php echo Yii::t('translation', $form->labelEx($model,'address')); ?>
		<?php echo $form->textArea($model,'address',array('style'=>'width:495px')); ?>
		<?php echo $form->error($model,'address'); ?>
	</div>

	<div class="row">
		<?php echo Yii::t('translation', $form->labelEx($model,'beginning')); ?>
                <div class="fix_number_help">
		<?php echo $form->textField($model,'beginning',array('size'=>30,'maxlength'=>12,'class'=>'number_only')); ?>
                <div class="help_number"></div>
                </div>
		<?php echo $form->error($model,'beginning'); ?>
	</div>
	<div class="clr"></div>
	<div class="row">
		<?php echo Yii::t('translation', $form->labelEx($model,'channel_id')); ?>
		<?php echo $form->dropDownList($model,'channel_id', GasChannel::getArrAll(),array('class'=>'')); ?>
		<?php echo $form->error($model,'channel_id'); ?>
	</div>

	<div class="row">
		<?php echo Yii::t('translation', $form->labelEx($model,'province_id')); ?>
		<?php echo $form->dropDownList($model,'province_id', GasProvince::getArrAll(),array('class'=>'')); ?>
		<?php echo $form->error($model,'province_id'); ?>
	</div>    
    
	<div class="row">
		<?php echo Yii::t('translation', $form->labelEx($model,'district_id')); ?>
		<?php echo $form->dropDownList($model,'district_id', GasDistrict::getArrAll(),array('class'=>'')); ?>
		<?php echo $form->error($model,'district_id'); ?>
	</div>

	<div class="row">
		<?php // echo Yii::t('translation', $form->labelEx($model,'storehouse_id')); ?>
		<?php // echo $form->dropDownList($model,'storehouse_id', GasStorehouse::getArrAll(),array('class'=>'')); ?>
		<?php // echo $form->error($model,'storehouse_id'); ?>
	</div>

	<div class="row">
		<?php echo Yii::t('translation', $form->labelEx($model,'sale_id')); ?>
		<?php echo $form->dropDownList($model,'sale_id', Users::getArrUserByRole(ROLE_SALE),array('class'=>'')); ?>
		<?php echo $form->error($model,'sale_id'); ?>
	</div>

	<div class="row">
		<?php echo Yii::t('translation', $form->labelEx($model,'payment_day')); ?>
		<?php echo $form->dropDownList($model,'payment_day', GasTypePay::getArrAll(), array('empty'=>'Select')); ?>
		<?php echo $form->error($model,'payment_day'); ?>
	</div>

	<div class="row">
		<?php echo Yii::t('translation', $form->labelEx($model,'status')); ?>
		<?php echo $form->dropDownList($model,'status', ActiveRecord::getUserStatus()); ?>
		<?php echo $form->error($model,'status'); ?>
	</div>

	<div class="row">
		<?php echo Yii::t('translation', $form->labelEx($model,'gender')); ?>
		<?php echo $form->dropDownList($model,'gender', Users::$gender); ?>
		<?php echo $form->error($model,'gender'); ?>
	</div>

	<div class="row">
		<?php echo Yii::t('translation', $form->labelEx($model,'phone')); ?>
		<?php echo $form->textField($model,'phone',array('size'=>20,'maxlength'=>20)); ?>
		<?php echo $form->error($model,'phone'); ?>
	</div>

	<div class="row buttons" style="padding-left: 115px;">
		        <?php $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType'=>'submit',
            'label'=>$model->isNewRecord ? Yii::t('translation', 'Create') : Yii::t('translation', 'Save'),
            'type'=>'null', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
            'size'=>'small', // null, 'large', 'small' or 'mini'
            //'htmlOptions' => array('style' => 'margin-bottom: 10px; float: right;'),
        )); ?>	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->

<script>
$(document).ready(function() {
    $('#Users_province_id').change(function(){
        var province_id = $(this).val();        
//        if($.trim(gender)==''){
//            $('#Products_category_id').html('<option value="">Select category</option>');            
//            return;
//        }                    
        var url_ = "<?php echo Yii::app()->createAbsoluteUrl('admin/gascustomer/create');?>";
//		$('#Products_category_id').html('<option value="">Select category</option>');            
    $.blockUI({ overlayCSS: { backgroundColor: '<?php echo BLOCK_UI_COLOR;?>' } }); 
        $.ajax({
            url: url_,
            data: {ajax:1,province_id:province_id},
            type: "get",
            dataType:'json',
            success: function(data){
                $('#Users_district_id').html(data['html_district']);
                $('#Users_storehouse_id').html(data['html_store']);
                $.unblockUI();
            }
        });
        
    });    
})

</script>
