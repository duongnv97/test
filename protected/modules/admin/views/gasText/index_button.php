<div class="form">
    <?php
        $LinkGeneral   = Yii::app()->createAbsoluteUrl('admin/gasText/', ['type'=> gasText::TAB_APPROVED]);
        $LinkWait      = Yii::app()->createAbsoluteUrl('admin/gasText/', ['type'=> gasText::TAB_WAIT_APPROVE]);
        $type          = isset($_GET['type']) ? $_GET['type'] : gasText::TAB_APPROVED;
    ?>
    <h1>Quản Lý Văn Bản
        <a class='btn_cancel f_size_14 <?php echo $type == gasText::TAB_APPROVED ? "active":"";?>' href="<?php echo $LinkGeneral;?>">Đã duyệt</a>
        <?php if($model->canApprove()): ?>
        <a class='btn_cancel f_size_14 <?php echo $type == gasText::TAB_WAIT_APPROVE ? "active":"";?>' href="<?php echo $LinkWait;?>">Chờ duyệt</a>
        <?php endif; ?>
    </h1>
</div>