<?php
$this->breadcrumbs=array(
	'Quản Lý Văn Bản',
);

$menus=array(
        array('label'=>'Create Văn Bản', 'url'=>array('create')),
);
$this->menu= ControllerActionsName::createMenusRoles($menus, $actions);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('gas-text-grid', {
                url : $(this).attr('action'),
		data: $(this).serialize()
	});
	return false;
});
");

Yii::app()->clientScript->registerScript('ajaxupdate', "
$('#gas-text-grid a.ajaxupdate').live('click', function() {
    $.fn.yiiGridView.update('gas-text-grid', {
        type: 'POST',
        url: $(this).attr('href'),
        success: function() {
            $.fn.yiiGridView.update('gas-text-grid');
        }
    });
    return false;
});
");
?>

<!--<h1>Quản Lý Văn Bản</h1>-->
<?php include('index_button.php'); ?>

<?php echo CHtml::link('Tìm Kiếm Nâng Cao','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php 
$this->renderPartial('_search',array(
	'model'=>$model,
));
$type        = isset($_GET['type']) ? $_GET['type'] : GasText::TAB_APPROVED;
$waitApprove = false;
$url_view_wait_approve = 'Yii::app()->createUrl("admin/GasText/ViewWaitApprove", array("id"=>$data->id))';
$url_view_normal       = 'Yii::app()->createUrl("admin/GasText/view", array("id"=>$data->id))';
if($type == GasText::TAB_WAIT_APPROVE){
    $waitApprove = true;
}
?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'gas-text-grid',
	'dataProvider'=>$model->search($waitApprove),
        'afterAjaxUpdate'=>'function(id, data){ fnUpdateColorbox();}',
        'template'=>'{pager}{summary}{items}{pager}{summary}', 
        'pager' => array(
            'maxButtonCount'=>  CmsFormatter::$PAGE_MAX_BUTTON,
        ),            
	'columns'=>array(
        array(
            'header' => 'S/N',
            'type' => 'raw',
            'value' => '$this->grid->dataProvider->pagination->currentPage * $this->grid->dataProvider->pagination->pageSize + ($row+1)',
            'headerHtmlOptions' => array('width' => '10px','style' => 'text-align:center;'),
            'htmlOptions' => array('style' => 'text-align:center;')
        ),
		'code_no',
                array(
                    'name' => 'role_id',
                    'value' => '$data->getRole()'
                ),
                array(
                    'name' => 'company_id',
                    'type' => "html",
                    'value' => '$data->getCompany()'
                ),
                array(
                    'name' => 'type',
                    'value' => '$data->getTypeShow()',
                ),                        
		'number_text',
		'number_sign',
                array(
                    'name' => 'date_published',
                    'type' => 'date',
                ),
                array(
                    'name' => 'date_expired',
                    'type' => 'date',
                ),
		'short_content',
                array(
                    'name' => 'stop_comment',
                    'value' => 'CmsFormatter::$yesNoFormat[$data->stop_comment]',
                    'htmlOptions' => array('style' => 'text-align:center;'),
                ),
//                array(
//                    'name' => 'Người chịu trách nhiệm',
//                    'type' => 'html',
//                    'value' => '$data->getResponsiblePerson()',
//                    'htmlOptions' => array('class' =>'w-200'),
//                ),
                array(
                    'name' => 'uid_login',
                    'value' => '$data->rUidLogin?$data->rUidLogin->first_name:""',
                ),
                array(
                    'name' => 'created_date',
                    'type' => 'datetime',
                ),
//                array(
//                    'name'=>'status',
//                    'type'=>'status',
//                    'value'=>'array("status"=>$data->status,"id"=>$data->id)',
//                    'htmlOptions' => array('style' => 'text-align:center;'),
//                    'visible'=>Yii::app()->user->role_id==ROLE_ADMIN,
//                ),
            
		array(
                    'header' => 'Actions',
                    'class'=>'CButtonColumn',
                    'template'=> ControllerActionsName::createIndexButtonRoles($actions, ['view','update','delete','approve']),
                    'buttons'=>array(
                        'view'=>array(
                            'url'=> $waitApprove ? $url_view_wait_approve : $url_view_normal,
                        ),
                        'update'=>array(
                            'visible'=> '$data->canUpdate()',
                        ),
                        'delete'=>array(
                            'visible'=> 'GasCheck::canDeleteData($data)',
                        ),
                        'approve'=>array(
                            'visible' => $waitApprove ? 'true':'false',
                            'label'=>'Duyệt',
                            'url' =>'Yii::app()->createUrl("admin/GasText/approve", array("id"=>$data->id))',
                            'options'=>[
                                'class' => 'approve_link'
                            ]
                        ),
                    ),
		),
	),
)); ?>

<script>
$(document).ready(function() {
    fnUpdateColorbox();
    approveColorbox();
});

function fnUpdateColorbox(){   
    fixTargetBlank();
}
function approveColorbox(){
    $(document).on('click', 'a.approve_link', function(e){
        e.preventDefault();
        $.colorbox({
            href: $(this).attr("href"),
            opacity:0.5 , 
            innerHeight:'500', 
            innerWidth: '800',
            iframe: true,
            onClosed: function () { // update view when close colorbox
                location.reload();
            }
        }); 
    });
}
</script>