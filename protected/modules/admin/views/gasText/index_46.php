<?php
$this->breadcrumbs=array(
    $this->pageTitle,
);
?>

<h1><?php echo $this->pageTitle; ?></h1>

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'gas-text-grid',
	'dataProvider'=>$model->search(),
        'afterAjaxUpdate'=>'function(id, data){ fnUpdateColorbox();}',
        'template'=>'{pager}{summary}{items}{pager}{summary}', 
        'pager' => array(
            'maxButtonCount'=>  CmsFormatter::$PAGE_MAX_BUTTON,
        ),            
	'columns'=>array(
        array(
            'header' => 'S/N',
            'type' => 'raw',
            'value' => '$this->grid->dataProvider->pagination->currentPage * $this->grid->dataProvider->pagination->pageSize + ($row+1)',
            'headerHtmlOptions' => array('width' => '10px','style' => 'text-align:center;'),
            'htmlOptions' => array('style' => 'text-align:center;')
        ),
		'code_no',
                array(
                    'name' => 'role_id',
                    'value' => '$data->getRole()'
                ),
                array(
                    'name' => 'company_id',
                    'type' => "html",
                    'value' => '$data->getCompany()'
                ),
                array(
                    'name' => 'type',
                    'value' => '$data->getTypeShow()',
                ),                        
		'number_text',
		'number_sign',
                array(
                    'name' => 'date_published',
                    'type' => 'date',
                ),
                array(
                    'name' => 'date_expired',
                    'type' => 'date',
                ),
		'short_content',
                array(
                    'name' => 'uid_login',
                    'value' => '$data->rUidLogin?$data->rUidLogin->first_name:""',
                ),
                array(
                    'name' => 'created_date',
                    'type' => 'datetime',
                ),
		array(
                    'header' => 'Actions',
                    'class'=>'CButtonColumn',
                    'template'=> ControllerActionsName::createIndexButtonRoles($actions),
                    'buttons'=>array(
                        'update'=>array(
                            'visible'=> '$data->canUpdate()',
                        ),
                        'delete'=>array(
                            'visible'=> 'GasCheck::canDeleteData($data)',
                        ),
                    ),
		),
	),
)); ?>

<div class="ad_read_news">
    <section>
        <h1 class="title"><strong>Thông Báo</strong></h1>
        <div class="content document">
        <?php
        $mCms = new Cms();
        $mCms->role_id = $_GET['role_id'];
        $aData = $mCms->getByRole();
        if(count($aData)): ?>
            <ul>
            <?php foreach($aData as $cms): ?>
                <li>
                    <a class="gas_link" target="_blank" href="<?php echo Yii::app()->createAbsoluteUrl('admin/site/news',array('id'=>$cms->id));?>">
                       <?php echo $cms->title;?>
                   </a>
                </li>
            <?php endforeach;?>
            </ul>
        <?php endif;?>
        </div>
    </section>
</div>


<script>
$(document).ready(function() {
    fnUpdateColorbox();
});

function fnUpdateColorbox(){   
    fixTargetBlank();
}
</script>