<div class="form">
    <h1>Duyệt văn bản <?php echo $model->code_no; ?></h1>
<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'gas-text-form',
	'enableAjaxValidation'=>false,
        //'htmlOptions' => array('enctype' => 'multipart/form-data'),
)); ?>

    <div class="row">
        <?php echo $form->labelEx($model, 'status'); ?>
        <?php echo $form->dropDownList($model, 'status', $model->getArrayStatus(), array('empty'=>'Select','class' => 'w-400')); ?>
        <?php echo $form->error($model, 'status'); ?>
    </div>
    
    <div class="row">
        <?php echo $form->labelEx($model, 'note', ['label'=>'Ghi chú']); ?>
        <?php echo $form->textArea($model, 'note', array('class' => 'w-400', 'rows'=>5)); ?>
        <?php echo $form->error($model, 'note'); ?>
    </div>
    
    <div class="clr"></div>
            
    <div class="row buttons" style="padding-left: 141px;">
        <?php $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType'=>'submit',
            'label' => "Save",
            'type'=>'null', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
            'size'=>'small', // null, 'large', 'small' or 'mini'
            //'htmlOptions' => array('style' => 'margin-bottom: 10px; float: right;'),
        )); ?>	
    </div>

<?php $this->endWidget(); ?>
</div>