<?php
/* @var $this GasTextController */
/* @var $model GasText */
/* @var $form CActiveForm */
?>
<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'gas-text-form',
	'enableAjaxValidation'=>false,
        'htmlOptions' => array('enctype' => 'multipart/form-data'),
)); ?>
        <?php if(!$model->isNewRecord):?>
        <?php // include '_link_view.php';?>
        <?php endif;?>
	<p class="note">Dòng có dấu <span class="required">*</span> là bắt buộc.</p>
        
        <?php  if(Yii::app()->user->hasFlash('successUpdate')): ?>
            <div class='success_div'><?php echo Yii::app()->user->getFlash('successUpdate');?></div>         
        <?php endif; ?>  	
        <?php echo $form->errorSummary($model); ?>

        <div class="row">
            <?php echo $form->labelEx($model, 'role_id'); ?>
            <?php echo $form->dropDownList($model, 'role_id', $model->getArrayRole(), array('empty'=>'Select','class' => 'w-400')); ?>
            <?php echo $form->error($model, 'role_id'); ?>
        </div>
            
        <div class="row">
            <?php echo $form->labelEx($model,'type'); ?>
            <?php echo $form->dropDownList($model,'type', $model->getArrayType(),array('class'=>'w-400 select_text_type','empty'=>'Select')); ?>
            <?php echo $form->error($model,'type'); ?>
	</div>
            
        <div class="row">
            <?php $mGasSettle = new GasSettle(); ?>
            <?php echo $form->labelEx($model, 'company_id'); ?>
            <?php echo $form->dropDownList($model, 'company_id', $mGasSettle->getListDataCompany(), array('empty'=>'Select','class' => 'w-400 select_company')); ?>
            <?php echo $form->error($model, 'company_id'); ?>
        </div>

	<div class="row">
		<?php echo $form->labelEx($model,'date_published'); ?>
		<?php 
                    $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                        'model'=>$model,        
                        'attribute'=>'date_published',
                        'options'=>array(
                            'showAnim'=>'fold',
                            'dateFormat'=> ActiveRecord::getDateFormatJquery(),
//                            'minDate'=> '0',
                            'maxDate'=> '0',
                            'changeMonth' => true,
                            'changeYear' => true,
                            'showOn' => 'button',
                            'buttonImage'=> Yii::app()->theme->baseUrl.'/admin/images/icon_calendar_r.gif',
                            'buttonImageOnly'=> true,                                
                        ),        
                        'htmlOptions'=>array(
                            'class'=>'w-16',
                            'style'=>'height:20px;',
                                'readonly'=>'readonly',
                        ),
                    ));
                ?>   
		<?php echo $form->error($model,'date_published'); ?>
	</div>            
	<div class="row">
		<?php echo $form->labelEx($model,'date_expired'); ?>
		<?php 
                    $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                        'model'=>$model,        
                        'attribute'=>'date_expired',
                        'options'=>array(
                            'showAnim'=>'fold',
                            'dateFormat'=> ActiveRecord::getDateFormatJquery(),
                            'minDate'=> '0',
//                            'maxDate'=> '0',
                            'changeMonth' => true,
                            'changeYear' => true,
                            'showOn' => 'button',
                            'buttonImage'=> Yii::app()->theme->baseUrl.'/admin/images/icon_calendar_r.gif',
                            'buttonImageOnly'=> true,                                
                        ),        
                        'htmlOptions'=>array(
                            'class'=>'w-16',
                            'style'=>'height:20px;',
                        ),
                    ));
                ?>   
		<?php echo $form->error($model,'date_expired'); ?>
	</div>            
	<div class="row ">
		<?php echo $form->labelEx($model,'number_text'); ?>
		<?php echo $form->textField($model,'number_text', array('class'=>'number_only_v1 align_l', 'maxlength'=>5)); ?>
		<?php echo $form->error($model,'number_text'); ?>
	</div>
	<div class="row">
		<?php echo $form->labelEx($model,'number_sign'); ?>
		<?php echo $form->textField($model,'number_sign',array('size'=>60,'maxlength'=>250)); ?>
		<?php echo $form->error($model,'number_sign'); ?>
	</div>

	<div class="row">
            <?php echo $form->labelEx($model,'short_content'); ?>
            <?php echo $form->textArea($model,'short_content',array('rows'=>5,'cols'=>80,"placeholder"=>"")); ?>
            <?php echo $form->error($model,'short_content'); ?>
	</div>
            
        <div class="row">
            <?php echo $form->labelEx($model,'stop_comment'); ?>
            <?php echo $form->dropDownList($model,'stop_comment', CmsFormatter::$yesNoFormat,array('class'=>'w-400','empty'=>'Select')); ?>
            <?php echo $form->error($model,'stop_comment'); ?>
	</div>
            
        <div class="row">
            <?php $person = $person = new GasTextPersonResponsibles('create'); ?>
            <?php echo $form->labelEx($person,'user_id'); ?>
            <?php echo $form->hiddenField($person, "user_id", array()); ?>
                <?php
//              Widget auto complete search user
                $aData = array(
                    'model'             => $person,
                    'field_customer_id' => 'user_id',
                    'url'               => Yii::app()->createAbsoluteUrl('admin/ajax/search_user_login', ['PaySalary' => 1]),
                    'name_relation_user'=> 'rUser',
                    'field_autocomplete_name'   => 'autocomplete_user',
                    'ShowTableInfo'     => 0,
                    'ClassAdd'          => 'w-400',
                    'fnSelectCustomerV2' => 'fnDoAfterSelect',
                    'doSomethingOnClose' => "doSomethingOnClose",
                );
                $this->widget('ext.SpaAutocompleteCustomer.SpaAutocompleteCustomer',
                        array('data'    => $aData)
                        );
                ?>
	</div>
            
        <div class="row" style="padding-left: 141px; margin-bottom:30px;">
            <table id="person_response" class="materials_table hm_table">
                <thead>
                    <tr>
                        <th class="item_c">#</th>
                        <th class="item_c">Nhân viên áp dụng</th>
                        <th class="item_c">Số tiền</th>
                        <th class="item_c">Số tháng</th>
                        <th class="item_unit last item_c">Xóa</th>
                    </tr>
                </thead>
                <tbody>
                    <tr class="materials_row tmp_tr" style="display:none">
                        <td class="item_c order padding_5"></td>
                        <td class="user_info_td padding_5">
                            <p class="user_name_txt"></p>
                            <input class="user_id_value"
                                   name="GasTextPersonResponsibles[user_id][]"
                                   type="hidden" value="">
                        </td>
                        <td class="padding_5">
                            <input class="number_only ad_fix_currency"
                                   name="GasTextPersonResponsibles[amount][]"
                                   type="text" value="0">
                        </td>
                        <td class="padding_5">
                            <input class="number_only ad_fix_currency"
                                   name="GasTextPersonResponsibles[month][]"
                                   type="text" value="1" readonly="1">
                        </td>
                        <td class="item_c last padding_5"><span class="remove_icon_only"></span></td>
                    </tr>
                    <?php 
                    if(!empty($model->rGasDebts)){
                        $aMonthCount = [];
                        $aRole = Roles::loadItems();
                        $aUser = array();
                        $i = 1;
                        foreach ($model->rGasDebts as $debts) :
                            if (in_array($debts->user_id, $aUser)) {
                                continue;
                            } else {
                                $aUser[] = $debts->user_id;
                            }
                            $aMonthCount[$debts->user_id] = isset($aMonthCount[$debts->user_id]) ? $aMonthCount[$debts->user_id]+1 : 1;
                            $objectUser = $debts->rUser;
                            $user_name_format = $objectUser->first_name . ' - ' . $aRole[$objectUser->role_id];
                    ?>
                    <tr class="materials_row">
                                <td class="item_c order_no padding_5"><?php echo $i++; ?></td>
                                <td class="user_info_td padding_5">
                                    <p class="user_name_txt"><?php echo $user_name_format; ?></p>
                                    <input class="user_id_value"
                                           name="GasTextPersonResponsibles[user_id][]"
                                           type="hidden" value="<?php echo $debts->user_id; ?>">
                                </td>
                                <td class="padding_5">
                                    <input class="number_only ad_fix_currency"
                                           name="GasTextPersonResponsibles[amount][]"
                                           type="text" value="<?php echo GasDebts::getAmountTotal($objectUser->id, $model->id); ?>">
                                </td>
                                <td class="padding_5">
                                    <input class="number_only ad_fix_currency"
                                           name="GasTextPersonResponsibles[month][]"
                                           readonly="1" type="text" value="<?php echo GasDebts::getMonthCount($objectUser->id, $model->id); ?>">
                                </td>
                                <td class="item_c last padding_5"><span class="remove_icon_only"></span></td>
                            </tr>
                    <?php
                        endforeach;
                    }
                    ?>
                </tbody>
            </table>
        </div>
        <div class="clr"></div>
        
        <div class="row row_template"></div>
        <div class="row row_sign" style="text-align:right;">
            
        </div>
        
        <div class="row row_file_scan">
            <label>File Scan</label>
            <table id="material" class="materials_table hm_table">
                <thead>
                    <tr>
                        <th class="item_c">#</th>
                        <th class="item_code item_c">File Scan ( cho phép định dạng <?php echo GasSalesFileScanDetai::$AllowFile;?>)</th>
                        <th class="item_c">Thứ Tự</th>
                        <th class="item_unit last item_c">Xóa</th>
                    </tr>
                </thead>
                <tbody>
                    <?php 
                        $max_upload = GasText::$max_upload;
                        $max_upload_show = GasText::$max_upload_show;
                        $aOrderNumber = MyFormat::BuildNumberOrder(GasText::$max_upload);
                    ?>
                    <?php if(count($model->aModelDetail)):?>
                    <?php foreach($model->aModelDetail as $key=>$item):?>
                    <tr class="materials_row">
                        <td class="item_c order_no"></td>
                        <td class="item_c w-400">
                            <?php // echo $form->fileField($item,'file_name[]'); ?>
                            <?php if(!empty($item->file_name) && !empty($item->created_date)): ?>
                            <p>
                                <a rel="group1" class="gallery" href="<?php echo ImageProcessing::bindImageByModel($item,'','',array('size'=>'size2'));?>"> 
                                    <img width="100" height="70" src="<?php echo ImageProcessing::bindImageByModel($item,'','',array('size'=>'size1'));?>">
                                </a>
                            </p>
                            <?php endif;?>
                            <?php echo $form->error($item,'file_name'); ?>
                            <?php echo $form->hiddenField($item,'aIdNotIn[]', array('value'=>$item->id)); ?>
                        </td>
                        <td class="item_c">
                            <?php echo $form->dropDownList($model->mDetail,'order_number[]', $aOrderNumber,array('class'=>'w-50', 
                                 'options' => array($item->order_number=>array('selected'=>true))
                                    )); ?>
                        </td>
                        <td class="item_c last"><span class="remove_icon_only"></span></td>
                    </tr> 
                    <?php $max_upload_show--;?>
                    <?php endforeach;?>
                    <?php endif;?>
                    
                    <?php for($i=1; $i<=$max_upload; $i++): ?>
                    <?php $display = "";
                        if($i>$max_upload_show)
                            $display = "display_none";
                    ?>
                    <tr class="materials_row <?php echo $display;?>">
                        <td class="item_c order_no"></td>
                        <td class="item_l w-400">
                            <?php echo $form->fileField($model->mDetail,'file_name[]', array('accept'=>'image/*')); ?>
                            <?php // echo $form->error($model->mDetail,'file_name'); ?>                        
                        </td>
                        <td class="item_c">
                            <?php echo $form->dropDownList($model->mDetail,'order_number[]', $aOrderNumber,array('class'=>'w-50', 
                                 'options' => array($i=>array('selected'=>true))
                                    )); ?>
                        </td>
                        <td class="item_c last"><span class="remove_icon_only"></span></td>
                    </tr>                    
                    <?php endfor;?>
                </tbody>
            </table>
        </div>
            
        <div class="row row_file_scan_add_row">
            <label>&nbsp</label>
            <div>
                <a href="javascript:void(0);" class="text_under_none item_b" style="line-height:25px" onclick="fnBuildRow();">
                    <img style="float: left;margin-right:8px;" src="<?php echo Yii::app()->theme->baseUrl;?>/images/add.png"> 
                    Thêm Dòng
                </a>
            </div>
        </div>
        <div class="clr"></div>
            
	<div class="row buttons" style="padding-left: 141px;">
		        <?php $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType'=>'submit',
            'label'=>$model->isNewRecord ? 'Create' :'Save',
            'type'=>'null', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
            'size'=>'small', // null, 'large', 'small' or 'mini'
            //'htmlOptions' => array('style' => 'margin-bottom: 10px; float: right;'),
        )); ?>	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->
<script>
    
    $(document).keydown(function(e) {
        if(e.which == 119) {
            fnBuildRow();
        }
    });
    
    function fnBuildRow(){
//        $('.materials_table').find('tr:visible:last').next('tr').show();
        $('#material').find('tr:visible:last').next('tr').show();
    }
    
    function fnBuildRowPersonResponse(){
        $('#person_response').find('tr:visible:last').next('tr').show();
    }
    
    $(document).ready(function(){
        $('.form').find('button:submit').click(function(){
            $.blockUI({ overlayCSS: { backgroundColor: '#fff' } }); 
        });
        
        fnRefreshOrderNumber();
        fnBindRemoveIcon();
        jQuery('a.gallery').colorbox({ opacity:0.5 , rel:'group1' });
        /*
         *  Mar2819 tạm close lại
        bindLoadTemplate();
        $('.select_text_type').on('change', function(){
            bindLoadTemplate();
        });
        
        bindLoadSign();
        $('.select_company').on('change', function(){
            bindLoadSign();
        });
         */
        
    });
</script>

<script>
    function fnAfterRemoveIcon(){
    // tại sao lại chỉ xử lý khi update, khi tạo mới lại ko dc???
        <?php if(!$model->isNewRecord): ?>
                $('.materials_table tbody').find('.display_none').eq(0).removeClass('display_none');
        <?php endif;?>
    }
</script>
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/autoNumeric-master/autoNumeric.js"></script>
<script>
    $(document).ready(function () {
        $('.form').find('button:submit').click(function () {
            $.blockUI({overlayCSS: {backgroundColor: '#fff'}});
        });
        
        fnInitInputCurrency();
    });
    // Rewrite action after select autocomple user (getElementById work but $(#..) not working)
    function fnDoAfterSelect(ui, idField, idFieldCustomerID){
        var tbl = $('table#person_response tbody');
        var tr = tbl.find('tr.tmp_tr').clone();
        tr.find('td.order').addClass('order_no');
        tr.find('td.user_info_td p.user_name_txt').text(ui.item.label);
        tr.find('td.user_info_td input.user_id_value').val(ui.item.id);
        tr.removeClass('tmp_tr');
        tr.attr('style','');
        tbl.append(tr);
        bindEventForHelpNumber();
        fnInitInputCurrency();
        fnRefreshOrderNumber();
        return false;
    }
    
/**
* @Author: ANH DUNG Dec 28, 2016
* @Todo: function này dc gọi từ ext của autocomplete, action close auto
*/
function doSomethingOnClose(ui, idField, idFieldCustomer){
    var row = $(idField).closest('.row');
    row.find('.remove_row_item').trigger('click');
}

function bindLoadTemplate(){
    var _phat        = '<?php echo GasText::TYPE_DECISION_PHAT; ?>';
    var _bo_nhiem    = '<?php echo GasText::TYPE_DECISION_BO_NHIEM; ?>';
    var _luan_chuyen = '<?php echo GasText::TYPE_DECISION_CHANGE_POSITION; ?>';
    var type         = $('.select_text_type').val();
    if( [_phat, _bo_nhiem, _luan_chuyen].includes(type) ){
        $('.row_file_scan').hide();
        $('.row_file_scan_add_row').hide();
        $.blockUI();
        $.ajax({
            'url': '<?php echo Yii::app()->createAbsoluteUrl('admin/gasText/create/ajax_template'); ?>' +'/'+type,
            'data': {
                'id' : '<?php echo $model->id; ?>'
            },
            'success':function(html){
                $('.row_template').html(html);
                $.unblockUI();
            },
        });
    }
}

function bindLoadSign(){
    var _phat        = '<?php echo GasText::TYPE_DECISION_PHAT; ?>';
    var _bo_nhiem    = '<?php echo GasText::TYPE_DECISION_BO_NHIEM; ?>';
    var _luan_chuyen = '<?php echo GasText::TYPE_DECISION_CHANGE_POSITION; ?>';
    var company      = $('.select_company').val();
    var type         = $('.select_text_type').val();
    if( [_phat, _bo_nhiem, _luan_chuyen].includes(type) ){
        $.blockUI();
        $.ajax({
            'url': '<?php echo Yii::app()->createAbsoluteUrl('admin/gasText/create/load_sign'); ?>' + '/' + company,
            'data': {
                'id' : '<?php echo $model->id; ?>'
            },
            'success':function(html){
                var img = '';
                if(html != ""){
                    img = '<img src="'+html+'" alt="Sign" style="width:300px;" class="sign_image">';
                }
                $('.row_sign').html(img);
                $.unblockUI();
            },
        });
    }
}
</script>