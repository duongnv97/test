<?php
$url_print_icon = Yii::app()->theme->baseUrl . "/images/print.png";
$menus = array(
	array('label'=>"Print", 'url'=>array('ViewWaitApprove', 'id'=>$model->id), 'linkOptions' => array('class'=>'print_btn','style'=>'background: url('.$url_print_icon.') no-repeat;background-size: contain;margin-top: -20px; margin-left: -30px;width: 25px; height: 25px;'), 'htmlOptions'=>array('label'=>'In văn bản')),
	array('label'=>"Quản lý", 'url'=>array('index')),
	array('label'=>"Tạo Mới", 'url'=>array('create')),
	array('label'=>"Cập Nhật", 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>"Xóa", 'url'=>array('delete'), 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Bạn chắc chắn muốn xóa ?')),
);
$this->menu= ControllerActionsName::createMenusRoles($menus, $actions);
?>

<h1>Xem GasTrackLogin #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'code_no',
		array(
                    'name' => 'role_id',
                    'value' => $model->getRole()
                ),
                array(
                    'name' => 'company_id',
                    'type' => "html",
                    'value' => $model->getCompany()
                ),
                array(
                    'name' => 'type',
                    'value' => $model->getTypeShow(),
                ),                        
		'number_text',
		'number_sign',
                array(
                    'name' => 'date_published',
                    'type' => 'date',
                ),
                array(
                    'name' => 'date_expired',
                    'type' => 'date',
                ),
		'short_content',
                array(
                    'name' => 'stop_comment',
                    'value' => CmsFormatter::$yesNoFormat[$model->stop_comment],
                    'htmlOptions' => array('style' => 'text-align:center;'),
                ),
                array(
                    'name' => 'uid_login',
                    'value' => $model->rUidLogin?$model->rUidLogin->first_name:"",
                ),
                array(
                    'name' => 'created_date',
                    'type' => 'datetime',
                ),
	),
));
?>
<div id="element_print">
    <?php
    $template = $model->template;
    $model->handleTemplate($template, true);
    echo $template;
    ?>

    <div style="text-align: right">
        <?php
        $aImage    =  GasText::model()->getArraySignImage();
        echo isset($aImage[$model->company_id]) ? '<img src="'.$aImage[$model->company_id].'" style="width: 300px;">' : '';
        ?>
    </div>
</div>

<script>
    $('.print_btn').on('click', function(e){
        e.preventDefault();
        var elm = $('#element_print');
        printElement(elm);
    });
    function printElement(e) {
        var ifr   = document.createElement('iframe');
        ifr.style = 'height: 0px; width: 0px; position: absolute';
        document.body.appendChild(ifr);

        $(e).clone().appendTo(ifr.contentDocument.body);
        ifr.contentWindow.print();
        
        ifr.parentElement.removeChild(ifr);
    }
</script>