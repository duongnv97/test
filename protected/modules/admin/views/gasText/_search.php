<div class="wide form">

<?php $type = isset($_GET['type']) ? $_GET['type'] : gasText::TAB_APPROVED; ?>
<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route, ['type' => $type]),
	'method'=>'get',
)); ?>

    <div class="row more_col">
        <div class="col1">
            <?php echo $form->label($model,'type',array()); ?>
            <?php echo $form->dropDownList($model,'type', $model->getArrayType(),array('class'=>'w-150','empty'=>'Select')); ?>
        </div>
        <div class="col2">
            <?php echo $form->label($model,'number_text',array()); ?>
            <?php echo $form->textField($model,'number_text', array('class'=>'w-150','empty'=>'Select')); ?>
        </div>
        <div class="col3">
            <?php echo $form->label($model,'number_sign',array()); ?>
            <?php echo $form->textField($model,'number_sign', array('class'=>'w-150','empty'=>'Select')); ?>
        </div>
    </div>
    <div class="row more_col">
        <div class="col1">
            <?php echo $form->label($model,'short_content',array()); ?>
            <?php echo $form->textField($model,'short_content', array('class'=>'w-150','empty'=>'Select')); ?>
        </div>
        <div class="col2">
            <?php echo $form->label($model,'status',array()); ?>
            <?php echo $form->dropDownList($model,'status',  ActiveRecord::getUserStatus(),array('class'=>'w-150','empty'=>'Select')); ?>
        </div>
        <div class="col3">
            <?php echo $form->label($model,'stop_comment',array()); ?>
            <?php echo $form->dropDownList($model,'stop_comment', CmsFormatter::$yesNoFormat,array('class'=>'w-150','empty'=>'Select')); ?>
        </div>
    </div>
    
    <div class="row more_col">
            <div class="col1">
                    <?php echo Yii::t('translation', $form->label($model,'date_from')); ?>
                    <?php 
                        $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                            'model'=>$model,        
                            'attribute'=>'date_from',
                            'options'=>array(
                                'showAnim'=>'fold',
                                'dateFormat'=> MyFormat::$dateFormatSearch,
    //                            'minDate'=> '0',
                                'maxDate'=> '0',
                                'changeMonth' => true,
                                'changeYear' => true,
                                'showOn' => 'button',
                                'buttonImage'=> Yii::app()->theme->baseUrl.'/admin/images/icon_calendar_r.gif',
                                'buttonImageOnly'=> true,                                
                            ),        
                            'htmlOptions'=>array(
                                'class'=>'w-16',
                                'size'=>'16',
                                'style'=>'float:left;',                               
                            ),
                        ));
                    ?>     		
            </div>
            <div class="col2">
                    <?php echo Yii::t('translation', $form->label($model,'date_to')); ?>
                    <?php 
                        $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                            'model'=>$model,        
                            'attribute'=>'date_to',
                            'options'=>array(
                                'showAnim'=>'fold',
                                'dateFormat'=> MyFormat::$dateFormatSearch,
    //                            'minDate'=> '0',
                                'maxDate'=> '0',
                                'changeMonth' => true,
                                'changeYear' => true,
                                'showOn' => 'button',
                                'buttonImage'=> Yii::app()->theme->baseUrl.'/admin/images/icon_calendar_r.gif',
                                'buttonImageOnly'=> true,                                
                            ),        
                            'htmlOptions'=>array(
                                'class'=>'w-16',
                                'size'=>'16',
                                'style'=>'float:left;',
                            ),
                        ));
                    ?>     		
            </div>
        </div>
        <div class="row more_col">
            <div class="col1">
                <?php echo $form->label($model, 'role_id'); ?>
                <?php echo $form->dropDownList($model, 'role_id', $model->getArrayRole(), array('empty'=>'Select','class' => 'w-200')); ?>
                <?php echo $form->error($model, 'role_id'); ?>
            </div>
            <div class="col2">
                <?php $mGasSettle = new GasSettle(); ?>
                <?php echo $form->label($model, 'company_id'); ?>
                <?php echo $form->dropDownList($model, 'company_id', $mGasSettle->getListDataCompany(), array('empty'=>'Select','class' => 'w-200')); ?>
                <?php echo $form->error($model, 'company_id'); ?>
            </div>
        </div>
    
        <div class="row">
            <?php echo $form->label($model,'is_view_all',array('label'=>'Xem tất cả')); ?>
            <?php echo $form->checkbox($model,'is_view_all'); ?>
        </div>
    
	<div class="row buttons" style="padding-left: 159px;">
		        <?php $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType'=>'submit',
            'label'=>'Search',
            'type'=>'null', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
            'size'=>'small', // null, 'large', 'small' or 'mini'
            //'htmlOptions' => array('style' => 'margin-bottom: 10px; float: right;'),
        )); ?>	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->