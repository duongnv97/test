<?php
$this->breadcrumbs=array(
	'Quản Lý Văn Bản'=>array('index'),
	$model->code_no,
);

$menus = array(
	array('label'=>'Quản Lý Văn Bản', 'url'=>array('index')),
	array('label'=>'Tạo Mới Văn Bản', 'url'=>array('create')),
	array('label'=>'Cập Nhật Văn Bản', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Xóa Văn Bản', 'url'=>array('delete'), 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Bạn chắc chắn muốn xóa ?')),
);
$this->menu= ControllerActionsName::createMenusRoles($menus, $actions);
$model->aModelDetail = $model->rTextFile;
?>
<?php include '_link_view.php';?>
<h1>Xem Văn Bản: <?php echo $model->code_no; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'code_no',
		array(
                    'name' => 'uid_login',
                    'value' => $model->rUidLogin?$model->rUidLogin->first_name:"",
                ), 
		array(
                    'name' => 'type',
                    'value' => $model->getTypeShow(),
                ), 
		'number_text',
		'number_sign',
		'date_published:date',
		'date_expired:date',
		'short_content',
		'status:status',
		'created_date:datetime',
	),
)); ?>
<div class="row">
        <label>&nbsp</label>
        <table class="materials_table hm_table">
            <thead>
                <tr>
                    <th class="item_c">#</th>
                    <th class="item_code item_c">File Scan</th>
                     <th class="item_c">Thứ Tự</th>
                </tr>
            </thead>
            <tbody>
                <?php if(count($model->aModelDetail)):?>
                <?php foreach($model->aModelDetail as $key=>$item):?>
                <tr class="materials_row">
                    <td class="item_c order_no"><?php echo $key+1;?></td>
                    <td class="item_c w-400 last">
                        <?php // echo $form->fileField($item,'file_name[]'); ?>
                        <p>
                            <a rel="group1" class="gallery" href="<?php echo ImageProcessing::bindImageByModel($item,'','',array('size'=>'size2'));?>"> 
                                <img width="100" height="70" src="<?php echo ImageProcessing::bindImageByModel($item,'','',array('size'=>'size1'));?>">
                            </a>
                        </p>
                    </td>
                    <td class="item_c">
                        <?php echo $item->order_number; ?>
                    </td>
                </tr> 
                <?php endforeach;?>
                <?php endif;?>

            </tbody>
        </table>
    </div>

<script>
    $(document).ready(function(){
        jQuery('a.gallery').colorbox({ opacity:0.5 , rel:'group1' });
    });
</script>
