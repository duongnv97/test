<?php
$this->breadcrumbs=array(
	'Quản Lý Văn Bản'=>array('index'),
	'Tạo Mới',
);

$menus = array(		
        array('label'=>'Quản Lý Văn Bản', 'url'=>array('index')),
);
$this->menu= ControllerActionsName::createMenusRoles($menus, $actions);
?>

<h1>Tạo Mới Văn Bản</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>