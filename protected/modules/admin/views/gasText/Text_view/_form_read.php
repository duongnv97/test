<?php
$mTextView->aModelDetail = $mTextView->rTextFile;
$type = $mTextView->getTypeShow();
?>
<h1>Xem Văn Bản: <?php echo $mTextView->code_no." / $type"; ?> - 
    <a href="javascript:void(0);" class="click_show_hide_text"> Click để ẩn/hiện văn bản</a>
</h1>

<!--for print-->
<div class="sprint" style=" padding-right: 743px;">
    <a class="button_print" href="javascript:void(0);" title="In Văn Bản">
        <img src="<?php echo Yii::app()->theme->baseUrl; ?>/images/print.png">
    </a>
</div>
<script  src="<?php echo Yii::app()->theme->baseUrl; ?>/js/jquery.printElement.min.js"></script>
<script type="text/javascript">
    $(document).ready(function(){
        $(".button_print").click(function(){
                $('#printElement').printElement({ overrideElementCSS: ['<?php echo Yii::app()->theme->baseUrl;?>/css/print-store-card.css'] });
        });
    });
</script>

<!--for print-->

<div class="clr"></div>
<div class="flash notice">
    <?php 
        $date_published = MyFormat::dateConverYmdToDmy($mTextView->date_published);
        $date_expired = MyFormat::dateConverYmdToDmy($mTextView->date_expired);
        echo    " Số: $mTextView->number_text"."."
                . " Số Ký Hiệu: $mTextView->number_sign"."."
                . " Ngày Ban Hành: $date_published"."."
                . " Ngày Hết Hạn: $date_expired"."."
                . " ";
    ?>
    <p>Trích Yếu: <?php echo nl2br($mTextView->short_content);?></p>
</div>

<ul class="box_text_view"  id="printElement">
<?php if(count($mTextView->aModelDetail)):?>
    <?php foreach($mTextView->aModelDetail as $key=>$item):?>
    <li>
        <a rel="group1" class="gallery" href="<?php echo ImageProcessing::bindImageByModel($item,'','',array('size'=>'size2'));?>"> 
            <img src="<?php echo ImageProcessing::bindImageByModel($item,'','',array('size'=>'size2'));?>">
        </a>
    </li>
    <?php endforeach;?>
<?php endif;?>   
</ul>


