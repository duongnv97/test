<?php
Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('gas-text-grid', {
                url : $(this).attr('action'),
		data: $(this).serialize()
	});
	return false;
});
");

Yii::app()->clientScript->registerScript('ajaxupdate', "
$('#gas-text-grid a.ajaxupdate').live('click', function() {
    $.fn.yiiGridView.update('gas-text-grid', {
        type: 'POST',
        url: $(this).attr('href'),
        success: function() {
            $.fn.yiiGridView.update('gas-text-grid');
        }
    });
    return false;
});
");
?>

<?php //echo CHtml::link('Tìm Kiếm Nâng Cao','#',array('class'=>'search-button')); ?>
<div class="search-form" style="">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'gas-text-grid',
	'dataProvider'=>$model->search(),
        'afterAjaxUpdate'=>'function(id, data){ fnUpdateColorbox();}',
        'template'=>'{pager}{summary}{items}{pager}{summary}', 
        'pager' => array(
            'maxButtonCount'=>  CmsFormatter::$PAGE_MAX_BUTTON,
        ),            
	'columns'=>array(
        array(
            'header' => 'S/N',
            'type' => 'raw',
            'value' => '$this->grid->dataProvider->pagination->currentPage * $this->grid->dataProvider->pagination->pageSize + ($row+1)',
            'headerHtmlOptions' => array('width' => '10px','style' => 'text-align:center;'),
            'htmlOptions' => array('style' => 'text-align:center;')
        ),
		'code_no',
                
                array(
                    'name' => 'type',
                    'value' => '$data->getTypeShow()',
                ),                        
		'number_text',
		'number_sign',
                array(
                    'name' => 'date_published',
                    'type' => 'date',
                ),
                array(
                    'name' => 'date_expired',
                    'type' => 'date',
                ),
		'short_content',
                array(
                    'name' => 'uid_login',
                    'value' => '$data->rUidLogin?$data->rUidLogin->first_name:""',
                ),
                array(
                    'name' => 'created_date',
                    'type' => 'datetime',
                ),
                array(
                    'header' => 'Last Comment',
                    'type' => 'html',
                    'value' => 'GasTextComment::GetLastComment($data)',
                ),
//                array(
//                    'name'=>'status',
//                    'type'=>'status',
//                    'value'=>'array("status"=>$data->status,"id"=>$data->id)',
//                    'htmlOptions' => array('style' => 'text-align:center;'),
//                    'visible'=>Yii::app()->user->role_id==ROLE_ADMIN,
//                ),
            
		array(
			'header' => 'Actions',
			'class'=>'CButtonColumn',
            'template'=> ControllerActionsName::createIndexButtonRoles($actions, array('view')),
		),
	),
)); ?>