<?php 
$cmsFormater = new CmsFormatter();
?>
<li class="received reply margin_0">

    <?php if(GasCheck::isAllowAccess('GasText', 'Text_delete_comment')):?>
    <div class="content">
        <div class="actions">
            <form method="post" class="button_to" action="<?php echo Yii::app()->createAbsoluteUrl('admin/gasText/text_delete_comment', array('id'=>$data->id)) ?>">
                <div>
                    <input type="submit" value="Delete" class="btn digital_delete btn_closed_tickets" title="Xóa vĩnh viễn comment này" alert_text="Xóa vĩnh viễn comment này?"> 
                </div>
            </form>
        </div>
    </div>
    <?php endif;?>
    
    <div class="message">
        <?php echo nl2br($data->message);?>
        <span class="posted_on">Ngày gửi <?php echo $cmsFormater->formatDateTime($data->created_date); ?></span>
    </div>
    <div class="author">
        <span class="name item_b">
            <?php echo GasTextComment::ShowNamePost($data->rUidLogin); ?>
        </span>
    </div>
    
</li>