<?php
if(isset($_GET['id'])):
    $this->breadcrumbs=array(
        'Danh Sách Văn Bản'=>array('text_view'),
        "Xem Văn Bản",
    );
else:
    $this->breadcrumbs=array(
        'Danh Sách Văn Bản',
    );
endif;

?>


<?php if(Yii::app()->user->hasFlash('successUpdate')): ?>
    <div class="flash notice">
        <a data-dismiss="alert" class="close" href="javascript:void(0)">×</a>
        <?php echo Yii::app()->user->getFlash('successUpdate');?>
    </div>
<?php endif; ?>  
<?php if(Yii::app()->user->hasFlash('ErrorUpdate')): ?>
    <div class="flash notice_error">
        <a data-dismiss="alert" class="close" href="javascript:void(0)">×</a>
        <?php echo Yii::app()->user->getFlash('ErrorUpdate');?>
    </div>
<?php endif; ?>

<div class="hm_text_view">
    <?php if(isset($_GET['id'])):?>
        <?php include '_form_read.php';?>
        <?php include '_box_comment.php';?>
    <?php else:?>
        <?php include '_list_other_text.php';?>
    <?php endif;?>
</div>
<style>
    .box_text_view li { list-style: none;}
</style>

<script>
    $(document).ready(function(){
        jQuery('a.gallery').colorbox({ opacity:0.5 , rel:'group1' });
        fnUpdateColorbox();
        BindClickReply();
        BindClickShowHide();
        BindClickClose('<?php echo BLOCK_UI_COLOR;?>');
    });
    
    function fnUpdateColorbox(){   
        fixTargetBlank();
        
    }
    
    function BindClickReply(){
        $('.new_reply').find('input:submit').live('click',function(){
            var form = $(this).closest('form');
            var message = form.find('textarea').val();
            if($.trim(message)==''){
                form.find('.errorMessage').show();
                return false;
            }
            $.blockUI({ overlayCSS: { backgroundColor: '<?php echo BLOCK_UI_COLOR;?>' } }); 
            return true;
        }); 
    }
    
    function BindClickShowHide(){
         $('.click_show_hide_text').live('click',function(){
             $('.box_text_view').slideToggle();
        }); 
    }
    
//    function BindClickClose(){ // Dec 26, 2014 close
//        $('.btn_closed_tickets').live('click',function(){
//            var alert_text = $(this).attr('alert_text');
//            if(confirm(alert_text)){
//                $.blockUI({ overlayCSS: { backgroundColor: '<?php echo BLOCK_UI_COLOR;?>' } }); 
//                return true;
//            }
//            return false;
//        }); 
//    }
</script>
