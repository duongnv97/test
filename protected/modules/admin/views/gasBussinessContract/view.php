<?php
$this->breadcrumbs=array(
	'Gas Bussiness Contracts'=>array('index'),
	$model->id,
);
$menus = array(
	array('label'=>'GasBussinessContract Management', 'url'=>array('index')),
	array('label'=>'Tạo Mới GasBussinessContract', 'url'=>array('create')),
	array('label'=>'Cập Nhật GasBussinessContract', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Xóa GasBussinessContract', 'url'=>array('delete'), 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Bạn chắc chắn muốn xóa ?')),
);
//$this->menu= ControllerActionsName::createMenusRoles($menus, $actions);
?>

<h1>Xem SPANCOP Khách Hàng: <?php echo $model->customer_name; ?></h1>
<?php echo MyFormat::BindNotifyMsg(); ?>
<?php include "view_xdetail.php";?>
<?php include "view_comment_post.php";?>
<?php include "view_comment_view.php";?>

<?php // include 'view_update_guide_help.php';?>
<div class="form">
    <div class="row buttons" style="padding-left: 141px;">
        <input class='cancel_iframe' type='button' value='Cancel'>
    </div>
</div>

<script>
    $(window).load(function(){
//        $('.materials_table').floatThead();// không dùng dc cho popup
        fnResizeColorbox();
    });
    
    function fnResizeColorbox(){
//        var y = $('body').height()+100;
        var y = $('#main_box').height()+100;
        parent.$.colorbox.resize({innerHeight:y});
    }
    $(document).ready(function(){
        $('.form').find('button:submit').click(function(){
//            var p_content = $('.p_content').val();
//            if($.trim(p_content) == ''){
//                return false;
//            }
            $.blockUI({ overlayCSS: { backgroundColor: '#fff' } });
        });
    });
</script>
