<?php if(GasCheck::isAllowAccess("gasBussinessContract", "Update_guide_help") ):?>   
<div class="form container">
<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'gas-orders-form',	
	'action'=> Yii::app()->createAbsoluteUrl('admin/gasBussinessContract/update_guide_help', array('id'=>$model->id)),
	'enableClientValidation'=>true,
        'clientOptions' => array(
            'validateOnSubmit' => true,
        ),
    )); 
?>
    
    <div class="row">
        <?php echo $form->labelEx($model,'guide_help'); ?>
        <?php echo $form->textArea($model,'guide_help',array('rows'=>8,'cols'=>100,"placeholder"=>"Nhập hướng dẫn thực hiện")); ?>
        <?php echo $form->error($model,'guide_help'); ?>
    </div>
    <div class="row buttons" style="padding-left: 141px;">
            <?php $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType'=>'submit',
            'label'=>$model->isNewRecord ? 'Create' :'Save',
            'type'=>'null', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
            'size'=>'small', // null, 'large', 'small' or 'mini'
            //'htmlOptions' => array('style' => 'margin-bottom: 10px; float: right;'),
        )); ?>	
        <!--<input class='cancel_iframe' type='button' value='Cancel'>-->
    </div>

<?php $this->endWidget(); ?>
</div>
<?php endif;?>