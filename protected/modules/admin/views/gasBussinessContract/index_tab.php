<?php 
//    $mWeeks = GasWeekSetup::GetWeekNumber($model->year, $model->month);
    $tabActive = 0;
?>
<div id="tabs" class="BindChangeSearchCondition">
    <ul>
        <li>
            <a class="" href="#tabs-1">Khách Hàng Ký HĐ</a>
        </li>
        <li>
            <a class="" href="#tabs-2">Khách Hàng Thương Lượng</a>
        </li>
        <li>
            <a class="" href="#tabs-3">Khách Hàng Phát Sinh Ký HĐ</a>
        </li>
        <li>
            <a class="" href="#tabs-4">Khách Hàng DATA</a>
        </li>
        <li>
            <a class="" href="#tabs-5">KH Chuyên Đề Đặc Biệt</a>
        </li>
    </ul>
    
    <div id="tabs-1">
        <?php include 'index_tab_1.php';?>
    </div>
    <div id="tabs-2">
        <?php include 'index_tab_2.php';?>
    </div>
    <div id="tabs-3">
        <?php include 'index_tab_3.php';?>
    </div>
    <div id="tabs-4">
        <?php include 'index_tab_4.php';?>
    </div>
    <div id="tabs-5">
        <?php include 'index_tab_5.php';?>
    </div>
    
</div>    


<?php Yii::app()->clientScript->registerCoreScript('jquery.ui'); ?>
<script>
$(function() {
    $( "#tabs" ).tabs({ active: <?php echo $tabActive;?> });
});
</script>