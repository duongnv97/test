<?php
$this->breadcrumbs=array(
	'SPANCOP Báo Cáo Tuần',
);

$menus=array(
    array('label'=>'Create BÁO CÁO SPANCOP KHÁCH HÀNG', 'url'=>array('create')),
);
$this->menu= ControllerActionsName::createMenusRoles($menus, $actions);
$cmsFormater = new CmsFormatter();
$session=Yii::app()->session;
$TextTitleInfo = "Báo Cáo SPANCOP Tháng $model->month - Năm $model->year -";
if(!isset($session['YEAR_WEEK_SETUP'][$model->year][$model->month][$model->number_week])){
    unset($session['YEAR_WEEK_SETUP']);
    GasWeekSetup::InitSessionWeekNumberByYear($model->year);
    $session=Yii::app()->session;
}

if(isset($session['YEAR_WEEK_SETUP'][$model->year][$model->month][$model->number_week])){
    $model->mCurrentWeek = $session['YEAR_WEEK_SETUP'][$model->year][$model->month][$model->number_week];
    $TextTitleInfo = "Báo Cáo SPANCOP Tuần $model->number_week Tháng $model->month Từ Ngày ". $cmsFormater->formatDate($model->mCurrentWeek->date_from)." Đến Ngày ".$cmsFormater->formatDate($model->mCurrentWeek->date_to);
}else{
    $model->number_week = 0;
}
?>

<h1>SPANCOP Báo Cáo Tuần</h1>
<style>
    .items td, .items th { font-size: 12px !important; font-family: Arial;}
    /*.items td { padding: 0 !important; }*/
    
</style>
<div class="search-form search-form0 is_tab BindChangeSearchCondition" is_tab="0" style="">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
	'is_tab'=>0,
)); ?>
</div><!-- search-form -->


<?php // if($model->number_week):?>
    <?php include 'index_tab.php';?>
<?php // else:?>
    <!--<h2>Chưa thiết lập dữ liệu tuần</h2>-->
<?php // endif;?>


    
<script type="text/javascript">
$(document).ready(function() {
    $(".operations > .create > a").colorbox({iframe:true,
        innerHeight:'3500', 
        innerWidth: '1100', overlayClose :false, escKey:false,close: "<span title='close'>close</span>"
    });
    fnUpdateColorbox(1);
    fnBindChangeSearchCondition();
});

//http://www.jacklmoore.com/colorbox/
function fnUpdateColorbox(CurrentTab){
    fixTargetBlank();
    fnShowhighLightTr();
    $(" .update").colorbox({iframe:true,
        innerHeight:'3500', 
        innerWidth: '1100', overlayClose :false, escKey:false,close: "<span title='close'>close</span>"
    });
    $(" .view").colorbox({iframe:true,
        innerHeight:'3500', 
        innerWidth: '1100', close: "<span title='close'>close</span>"
    });
    fnAddSumTr();
    
    if(CurrentTab==getSelectedTabIndex())
        $.unblockUI();
}

function fnBindChangeSearchCondition(){
    $('.BindChangeSearchCondition input, .BindChangeSearchCondition select').change(function(){
        var class_update_val = $(this).attr('class_update_val');
        var ValUpdate = $(this).val();
        $('.'+class_update_val).val(ValUpdate);        
    });
    
    $('.SubmitButton').click(function(){
        
//        var div_search = $(this).closest('div.is_tab');
//        var CurrentTab = div_search.attr('is_tab');
/*  xử lý khi click search 1 tab thì trigger search hết những tab còn lại
 * 
 */
        var CurrentTab = getSelectedTabIndex();
        var tab1 = 0;
        var tab2 = 0;
        var tab3 = 0; // is tab 4 ở giao diện
        var tab4 = 0; // is tab 5 ở giao diện
        if(CurrentTab==0){
            tab1 = 2;
            tab2 = 3;
            tab3 = 4;
            tab4 = 5;
        }else if(CurrentTab==1){
            tab1 = 1;
            tab2 = 3;
            tab3 = 4;
            tab4 = 5;
        }else if(CurrentTab==2){
            tab1 = 1;
            tab2 = 2;
            tab3 = 4;
            tab4 = 5;
        }else if(CurrentTab==3){
            tab1 = 1;
            tab2 = 2;
            tab3 = 3;
            tab4 = 5;
        }else if(CurrentTab==4){
            tab1 = 1;
            tab2 = 2;
            tab3 = 3;
            tab4 = 4;
        }
        CurrentTab++;
        $('.search-form'+CurrentTab+' form').submit();
        $('.search-form'+tab1+' form').submit();
        $('.search-form'+tab2+' form').submit();
        $('.search-form'+tab3+' form').submit();
    });    
}

function fnAddSumTr(){
    $('.items').each(function(){
        var tr='';
        var amount_b12 = 0;
        var amount_b45 = 0;
        $(this).find('.amount_b12').each(function(){
            if($.trim($(this).text())!='')
                amount_b12 += parseFloat(''+$(this).text());
        });
        $(this).find('.amount_b45').each(function(){
            if($.trim($(this).text())!='')
                amount_b45+= parseFloat(''+$(this).text());
        });

        amount_b12 = Math.round(amount_b12 * 100) / 100;
        amount_b45 = Math.round(amount_b45 * 100) / 100;

        tr +='<tr class="f_size_18 odd sum_page_current">';
            tr +='<td class="item_r item_b" colspan="5">Tổng Cộng</td>';
            tr +='<td class="item_r item_b">'+commaSeparateNumber(amount_b12)+'</td>';
            tr +='<td class="item_r item_b">'+commaSeparateNumber(amount_b45)+'</td>';
            tr +='<td></td>';
        tr +='</tr>';
        if($(this).find('.sum_page_current').size()<1)
            $(this).find('tbody').append(tr);
    
    });
}

    function getSelectedTabIndex() { 
        return $("#tabs").tabs('option', 'selected');
    }

    function fnAfterSelectSaleOrAgent(user_id, idField, idFieldCustomer){
        fnAddValueUidToInput(user_id);
    }

    function fnAddValueUidToInput(user_id){
        $('.uid_login_hidden').val(user_id);
    }
    function fnRemoveValueUidFromInput(){
        $('.uid_login_hidden').val('');
    }
    function fnAfterRemove(this_, idField, idFieldCustomer){        
        fnRemoveValueUidFromInput();
    }

</script>
