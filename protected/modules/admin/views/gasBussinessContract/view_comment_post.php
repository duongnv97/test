<?php if( $model->status == GasBussinessContract::STATUS_ZONE_SPECIAL ):?>   
<div class="form container">
<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'gas-orders-form',	
    )); 
?>    
    <div class="row">
        <?php echo $form->labelEx($model->mComment,'content', array('label'=>'BC tình hình' )); ?>
        <?php echo $form->textArea($model->mComment,'content',array('class'=>'p_content','rows'=>5,'cols'=>100,"placeholder"=>"Nhập báo cáo tình hình")); ?>
        <?php echo $form->error($model->mComment,'content'); ?>
    </div>
    <div class="row buttons" style="padding-left: 141px;">
            <?php $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType'=>'submit',
            'label' => 'Save',
            'type'=>'null', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
            'size'=>'small', // null, 'large', 'small' or 'mini'
            //'htmlOptions' => array('style' => 'margin-bottom: 10px; float: right;'),
        )); ?>	
        <!--<input class='cancel_iframe' type='button' value='Cancel'>-->
    </div>

<?php $this->endWidget(); ?>
</div>
<?php endif;?>