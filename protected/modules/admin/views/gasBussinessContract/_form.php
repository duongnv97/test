<div class="form">
<?php 
    $display_none = '';
    if($model->row_auto==1 && $model->status == GasBussinessContract::STATUS_THUONG_LUONG){
        $display_none = 'display_none';
    }
    $cRole = Yii::app()->user->role_id;
?>
    
<?php 
$form=$this->beginWidget('CActiveForm', array(
	'id'=>'gas-bussiness-contract-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Dòng có dấu <span class="required">*</span> là bắt buộc.</p>
        
        <?php  if(Yii::app()->user->hasFlash('successUpdate')): ?>
            <div class='success_div'><?php echo Yii::app()->user->getFlash('successUpdate');?></div>         
        <?php endif; ?>  	
            	
	<div class="row ">
            <?php echo $form->labelEx($model,'status'); ?>
            <?php  echo $form->dropDownList($model,'status', GasBussinessContract::$ARR_STATUS,array('style'=>'','empty'=>'Select')); ?>
            <div class="row buttons" style="padding-left: 210px;">
                <em class="hight_light item_b">Nếu khách hàng là khách hàng dữ liệu thì chọn trạng thái là DATA</em>
            </div>
            <?php echo $form->error($model,'status'); ?>
	</div>
        <div class="row ">
            <?php echo $form->labelEx($model,'customer_zone'); ?>
            <?php echo $form->dropDownList($model,'customer_zone', GasBussinessContract::GetCustomerZone(), array('class'=>'', 'empty'=>'Select')); ?>
            <?php echo $form->error($model,'customer_zone'); ?>
	</div>            
            
        <?php if(Yii::app()->user->role_id == ROLE_SUB_USER_AGENT):?>
	<div class="row ">
            <?php echo $form->labelEx($model,'employee_id'); ?>
            <?php echo $form->dropDownList($model,'employee_id', Users::getSelectByRoleForAgent(Yii::app()->user->parent_id , ONE_AGENT_MAINTAIN, $model->agent_id, array('status'=>1)),array('class'=>'category_ajax', 'style'=>'','empty'=>'Select')); ?>
            <?php echo $form->error($model,'employee_id'); ?>
	</div>
        <?php endif;?>

        <?php if(in_array($cRole, $model->SAME_ROLE_MONITOR_AGEN)):?>
	<div class="row ">
            <?php echo $form->labelEx($model,'type_customer'); ?>
            <?php echo $form->dropDownList($model,'type_customer', CmsFormatter::$CUSTOMER_BO_MOI,array('class'=>'', 'empty'=>'Select')); ?>
            <?php echo $form->error($model,'type_customer'); ?>
	</div>
        <?php endif;?>

        <?php /*if( Yii::app()->user->role_id != ROLE_SUB_USER_AGENT && Yii::app()->user->role_id != ROLE_MONITOR_AGENT):?>
	<div class="row display_none">
            <?php echo $form->labelEx($model,'agent_id'); ?>
            <?php echo $form->dropDownList($model,'agent_id',  Users::getSelectByRoleFinal(ROLE_AGENT),array('class'=>'', 'style'=>'','empty'=>'Select')); ?>
            <?php echo $form->error($model,'agent_id'); ?>
	</div>
        <?php endif; */?>
            
	<div class="row <?php // echo $display_none;?>">
		<?php echo $form->labelEx($model,'customer_name'); ?>
		<?php echo $form->textField($model,'customer_name',array('class'=>'w-350','maxlength'=>200)); ?>
		<?php echo $form->error($model,'customer_name'); ?>
	</div>
	<div class="row <?php // echo $display_none;?>">
		<?php echo $form->labelEx($model,'customer_contact'); ?>
		<?php echo $form->textField($model,'customer_contact',array('class'=>'w-350', 'maxlength'=>200)); ?>
		<?php echo $form->error($model,'customer_contact'); ?>
	</div>
	<div class="row <?php // echo $display_none;?>">
		<?php echo $form->labelEx($model,'address'); ?>
                    <?php echo $form->textArea($model,'address',array('style'=>'width:386px','maxlength'=>300)); ?>
                    <div class="row buttons" style="padding-left: 210px;">
                        <em class="hight_light item_b">Địa chỉ ghi đầy đủ số nhà, đường, phường, quận huyện, thành phố</em>
                    </div>
		<?php echo $form->error($model,'address'); ?>
	</div>
	<div class="row <?php // echo $display_none;?>">
		<?php echo $form->labelEx($model,'phone'); ?>
		<?php echo $form->textField($model,'phone',array('class'=>'w-350', 'maxlength'=>100)); ?>
		<?php echo $form->error($model,'phone'); ?>
	</div>
	<div class="row">
		<?php echo $form->labelEx($model,'date_plan'); ?>
                <?php 
                    $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                        'model'=>$model,        
                        'attribute'=>'date_plan',
                        'options'=>array(
                            'showAnim'=>'fold',
                            'dateFormat'=> ActiveRecord::getDateFormatJquery(),
//                            'minDate'=>0,
                            'changeMonth' => true,
                            'changeYear' => true,
                            'showOn' => 'button',
                            'buttonImage'=> Yii::app()->theme->baseUrl.'/admin/images/icon_calendar_r.gif',
                            'buttonImageOnly'=> true,                                
                        ),        
                        'htmlOptions'=>array(
                            'class'=>'w-16',
                            'readonly'=>'readonly',
                        ),
                    ));
                ?>		
		<?php echo $form->error($model,'date_plan'); ?>
	</div>
	<div class="row <?php // echo $display_none;?>">
		<?php echo $form->labelEx($model,'b12'); ?>
		<?php echo $form->textField($model,'b12', array('maxlength'=>5, 'class'=>'')); ?>
		<?php echo $form->error($model,'b12'); ?>
	</div>
	<div class="row <?php // echo $display_none;?>">
		<?php echo $form->labelEx($model,'b45'); ?>
		<?php echo $form->textField($model,'b45', array('maxlength'=>5)); ?>
		<?php echo $form->error($model,'b45'); ?>
	</div>
	<div class="row">
		<?php echo $form->labelEx($model,'note'); ?>
		<?php echo $form->textArea($model,'note',array('rows'=>8, 'style'=>'width:600px','maxlength'=>500)); ?>
		<?php echo $form->error($model,'note'); ?>
	</div>

	<div class="row buttons" style="padding-left: 210px;">
		        <?php $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType'=>'submit',
            'label'=>$model->isNewRecord ? 'Create' :'Save',
            'type'=>'null', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
            'size'=>'small', // null, 'large', 'small' or 'mini'
            //'htmlOptions' => array('style' => 'margin-bottom: 10px; float: right;'),
        )); ?>	
        <input class='cancel_iframe' type='button' value='Cancel'>
        </div>

<?php $this->endWidget(); ?>

</div><!-- form -->
<style>
    div.form .row label { width: 210px;}
</style>

<script>
    $(document).ready(function(){
        $('.form').find('button:submit').click(function(){
            $.blockUI({ overlayCSS: { backgroundColor: '#fff' } }); 
        });        
    });
    $(window).load(function(){ // không dùng dc cho popup
//        $('.materials_table').floatThead();
        fnResizeColorbox();
        parent.$('.SubmitButton').trigger('click');
    });
    
    function fnResizeColorbox(){
//        var y = $('body').height()+100;
        var y = $('#main_box').height()+100;
        parent.$.colorbox.resize({innerHeight:y});        
    }
</script>