<?php
$this->breadcrumbs=array(
	'Gas Bussiness Contracts'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Cập Nhật',
);

$menus = array(	
        array('label'=>'GasBussinessContract Management', 'url'=>array('index')),
	array('label'=>'Xem GasBussinessContract', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Tạo Mới GasBussinessContract', 'url'=>array('create')),	
);
//$this->menu= ControllerActionsName::createMenusRoles($menus, $actions);

?>

<h1>Cập Nhật SPANCOP Khách Hàng: <?php echo $model->customer_name; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>