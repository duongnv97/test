<?php 
$SubmitButtonClass = '';
$ButtonType = 'submit';
$display_none = 'display_none';
if($is_tab==0){
    $display_none = '';    
    $SubmitButtonClass='SubmitButton btn_cancel';
    $ButtonType = 'button';
}

?>
<div class="wide form  <?php echo $display_none; ?>" style="padding:0;">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

    <div class="row more_col">
        <div class="col1">
            <?php echo $form->label($model,'number_week',array()); ?>
            <?php  echo $form->dropDownList($model,'number_week', GasBussinessContract::$ARR_WEEK,array('class'=>'w-150 number_week', 'class_update_val'=>'number_week','empty'=>'Select' )); ?>
        </div>
        <div class="col2">
            <?php echo $form->label($model,'month',array()); ?>
            <?php echo $form->dropDownList($model,'month', ActiveRecord::getMonthVnWithZeroFirst(), array('class'=>'w-150 month', 'class_update_val'=>'month','empty'=>'Select')); ?>
        </div>
        
        <div class="col3">
            <?php echo $form->label($model,'year',array()); ?>
            <?php echo $form->dropDownList($model,'year', ActiveRecord::getRangeYear(), array('class'=>'w-150 year', 'class_update_val'=>'year')); ?>
        </div>        
    </div>
    
    <div class="row more_col">
        <div class="col1">
            <?php echo $form->label($model,'customer_name',array()); ?>
            <?php echo $form->textField($model,'customer_name',array('class'=>'w-150 customer_name', 'class_update_val'=>'customer_name','maxlength'=>200)); ?>
        </div>
        <div class="col2">
            <?php echo $form->label($model,'customer_contact',array()); ?>
            <?php echo $form->textField($model,'customer_contact',array('class'=>'w-150 customer_contact', 'class_update_val'=>'customer_contact','maxlength'=>200)); ?>
        </div>
        
        <div class="col3">
            <?php echo $form->label($model,'address',array()); ?>
            <?php echo $form->textField($model,'address',array('class'=>'w-250 address', 'class_update_val'=>'address','maxlength'=>200)); ?>
        </div>        
    </div>
    <div class="row more_col">
        <div class="col1">
            <?php echo $form->label($model,'phone',array()); ?>
            <?php echo $form->textField($model,'phone',array('class'=>'w-150 phone', 'class_update_val'=>'phone','maxlength'=>200)); ?>
        </div>
        <div class="col2">
            <?php echo $form->label($model,'type',array()); ?>
            <?php echo $form->dropDownList($model,'type', GasBussinessContract::$ARR_TYPE_SALE, array('class'=>'w-150 type', 'class_update_val'=>'type','empty'=>'Select')); ?>
        </div>
        
        <div class="col3">
            <?php echo $form->labelEx($model,'type_customer'); ?>
            <?php echo $form->dropDownList($model,'type_customer', CmsFormatter::$CUSTOMER_BO_MOI,array('class'=>'w-150 type_customer', 'class_update_val'=>'type_customer','empty'=>'Select')); ?>
        </div>        
    </div>
    
    
    <div class="row">      
        <?php /* echo $form->hiddenField($model,'employee_id', array('class'=>'uid_login_hidden', 'class_update_val'=>'employee_id')); ?>
        <?php if($is_tab==0): ?>
        <?php echo $form->labelEx($model,'employee_id', array('label'=>'Sale / Nhân Viên Giao Hàng')); ?>
        <?php 
            // widget auto complete search user customer and supplier
            $aData = array(
                'model'=>$model,
                'field_customer_id'=>'employee_id',
                'url'=> Yii::app()->createAbsoluteUrl('admin/ajax/search_for_bussiness_contract'),
                'name_relation_user'=>'rEmployee',
                'fnSelectCustomer'=>'fnAfterSelectSaleOrAgent',
            );
            $this->widget('ext.SpaAutocompleteCustomer.SpaAutocompleteCustomer',
                array('data'=>$aData));                                        
        ?>                        
        <?php endif; 
        chỗ này có thể xử lý thêm 1 field hidden là role_id vào, rồi xuống dưới phần search if else nó 
         * để quyết định sẽ search theo uid_login hay employee_id ... will resolve
         *          */?>
        
        <?php echo $form->hiddenField($model,'uid_login', array('class'=>'uid_login_hidden uid_login', 'class_update_val'=>'uid_login')); ?>
        <?php if($is_tab==0): ?>
        <?php echo $form->labelEx($model,'uid_login'); ?>
        <?php 
            // widget auto complete search user customer and supplier
            $aData = array(
                'model'=>$model,
                'field_customer_id'=>'uid_login',
                'url'=> Yii::app()->createAbsoluteUrl('admin/ajax/search_for_bussiness_contract'),
                'name_relation_user'=>'rUidLogin',
                'fnSelectCustomer'=>'fnAfterSelectSaleOrAgent',
            );
            $this->widget('ext.SpaAutocompleteCustomer.SpaAutocompleteCustomer',
                array('data'=>$aData));                                        
        ?>                        
        <?php endif; ?>
        
        
    </div>
    
    
    
    
    <div class="row">
        <?php echo $form->label($model,'code_no',array('label' => "Mã CF Đầu Tiên")); ?>
        <?php echo $form->textField($model,'code_no',array('class_update_val'=>'code_no','class'=>'code_no','maxlength'=>30)); ?>
    </div>

    <?php /*
	<div class="row">
		<?php echo $form->label($model,'agent_id',array()); ?>
		<?php echo $form->textField($model,'agent_id',array('size'=>11,'maxlength'=>11)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'uid_login',array()); ?>
		<?php echo $form->textField($model,'uid_login',array('size'=>11,'maxlength'=>11)); ?>
	</div>


	<div class="row">
		<?php echo $form->label($model,'employee_id',array()); ?>
		<?php echo $form->textField($model,'employee_id',array('size'=>11,'maxlength'=>11)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'customer_name',array()); ?>
		<?php echo $form->textField($model,'customer_name',array('size'=>60,'maxlength'=>200)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'customer_contact',array()); ?>
		<?php echo $form->textField($model,'customer_contact',array('size'=>60,'maxlength'=>200)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'address',array()); ?>
		<?php echo $form->textField($model,'address',array('size'=>60,'maxlength'=>300)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'phone',array()); ?>
		<?php echo $form->textField($model,'phone',array('size'=>60,'maxlength'=>100)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'date_plan',array()); ?>
		<?php echo $form->textField($model,'date_plan'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'b12',array()); ?>
		<?php echo $form->textField($model,'b12'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'b45',array()); ?>
		<?php echo $form->textField($model,'b45'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'note',array()); ?>
		<?php echo $form->textField($model,'note',array('size'=>60,'maxlength'=>500)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'created_date',array()); ?>
		<?php echo $form->textField($model,'created_date'); ?>
	</div>
    
    */?>

	<div class="row buttons" style="padding-left: 159px;">
        <?php $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType'=>$ButtonType,
            'label'=>'Search',
            'type'=>'null', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
            'size'=>'small', // null, 'large', 'small' or 'mini'
            'htmlOptions' => array('class' => $SubmitButtonClass),
        )); ?>	
        </div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->