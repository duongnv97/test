<?php 
//    $mWeeks = GasWeekSetup::GetWeekNumber($model->year, $model->month);
    $tabActive = 0;
?>
<div id="tabs" class="">
    <ul>
        <?php foreach($mWeeks as $modelWeek):?>
        <li>
            <a class="" href="#tabs-<?php echo $modelWeek->number_week;?>">Tuần <?php echo $modelWeek->number_week;?></a>
        </li>
        <?php endforeach; ?>
    </ul>
    
    <?php foreach($mWeeks as $modelWeek):?>
    <div id="tabs-<?php echo $modelWeek->number_week;?>">
        <?php include 'index_tab_block.php';?>
    </div>
    <?php endforeach; ?>
    
</div>    


<?php Yii::app()->clientScript->registerCoreScript('jquery.ui'); ?>
<script>
$(function() {
    $( "#tabs" ).tabs({ active: <?php echo $tabActive;?> });
});
</script>