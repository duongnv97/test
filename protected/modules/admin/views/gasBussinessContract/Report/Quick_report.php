<?php
//UpdateSql::UpdateWeekSetupId();
//GasBussinessContract::CronUpdateFirstPurchase();
$this->breadcrumbs=array(
	'Báo Cáo SPANCOP Tóm Tắt',
);
$cmsFormater = new CmsFormatter();
$session=Yii::app()->session;
$TextTitleInfo = "Báo Cáo SPANCOP Tháng $model->month - Năm $model->year -";
if(!isset($session['YEAR_WEEK_SETUP'][$model->year][$model->month][$model->number_week])){
    unset($session['YEAR_WEEK_SETUP']);
    GasWeekSetup::InitSessionWeekNumberByYear($model->year);
    $session=Yii::app()->session;
}

if(isset($session['YEAR_WEEK_SETUP'][$model->year][$model->month][$model->number_week])){
    $model->mCurrentWeek = $session['YEAR_WEEK_SETUP'][$model->year][$model->month][$model->number_week];
    $TextTitleInfo = " Tuần $model->number_week/$model->month Từ Ngày ". $cmsFormater->formatDate($model->mCurrentWeek->date_from)." Đến Ngày ".$cmsFormater->formatDate($model->mCurrentWeek->date_to);
}else{
    $model->number_week = 0;
}
$gridCss = Yii::app()->getAssetManager()->publish(Yii::getPathOfAlias('zii.widgets.assets')).'/gridview/styles.css';
Yii::app()->getClientScript()->registerCssFile($gridCss);
?>

<?php if(Yii::app()->user->hasFlash('successUpdate')): ?>
    <div class="flash notice">
        <?php echo Yii::app()->user->getFlash('successUpdate');?>
    </div>
<?php endif; ?>
<h1>Báo Cáo SPANCOP Tóm Tắt: <?php echo $TextTitleInfo; ?></h1>
<!--<div class="clr hight_light item_b f_size_18">'.$TextTitleInfo.' Khách Hàng Ký HĐ</div>-->

<div class="">

<?php // echo CHtml::link('Tìm Kiếm Nâng Cao','#',array('class'=>'search-button')); ?>
<div class="search-form" style="">
<?php $this->renderPartial('_search_report',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->
<?php include 'Quick_report_form.php';?>

<script>
$(document).ready(function() {
    fnUpdateColorbox();
    $('.runCronUpdateFirstPurchase').click(function(){
        if(confirm('Bạn có chắc chắn cập nhật không?')){
            $.blockUI({ overlayCSS: { backgroundColor: '#fff' } });
            return true;
        }
        return false;
    }); 
});

//http://www.jacklmoore.com/colorbox/
function fnUpdateColorbox(){
//    fnShowhighLightTr();
    $(".update_customer_sign_contract").colorbox({iframe:true,innerHeight:'550', innerWidth: '950', overlayClose :false,escKey:false,close: "<span title='close'>close</span>"});
//    fixTargetBlank();
    
}
</script>

</div>