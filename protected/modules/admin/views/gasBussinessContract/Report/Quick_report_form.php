<?php 
    $cmsFormater = new CmsFormatter();
    $sOK = GasBussinessContract::STATUS_KY_HD;
    $sTL = GasBussinessContract::STATUS_THUONG_LUONG;
    $sHB = GasBussinessContract::STATUS_HUY_BO;
    $aModelUser = array();
    $aModelAgent = array();
    
    if(isset($data['FOR_MODEL'])){
        $aModelUser = Users::getArrayModelByArrayId($data['FOR_MODEL']);
    }
    if(isset($data['FOR_MODEL_AGENT'])){
        $aModelAgent = Users::getArrayModelByArrayId($data['FOR_MODEL_AGENT']);
    }
    $aModelWeekSetup = GasWeekSetup::InitSessionAllWeekSetup();
?>
<style>
    .hm_table td, .hm_table th { font-size: 15px !important; }
</style>
<div class="grid-view">
<table class="items hm_table">
    <thead>
        <tr>
            <th>Loại Sale</th>
            <th>Tên Sale</th>
            <th>Khách hàng phát sinh ký hợp đồng</th>
            <th>Khách hàng thương lượng</th>
            <th>Khách hàng ký hợp đồng</th>
        </tr>
    </thead>
    <tbody>
    <?php foreach(GasBussinessContract::$ARR_TYPE_SALE as $type_id=>$type_sale_text): ?>
        <?php
            $aEmployeeId = isset($data['LOOP'][$type_id])?$data['LOOP'][$type_id]:array();
        ?>
        <?php foreach($aEmployeeId as $uid): ?>
            <tr>
                <td><?php echo $type_sale_text;?></td>
                <td><?php 
                        echo isset($aModelUser[$uid])?$aModelUser[$uid]->first_name:'';
                        if($type_id==GasBussinessContract::TYPE_AGENT){
                            if(isset($data['FOR_MODEL_AGENT'][$uid]) && isset($aModelAgent[$data['FOR_MODEL_AGENT'][$uid]]))
                                echo "<br/><em>".$aModelAgent[$data['FOR_MODEL_AGENT'][$uid]]->first_name."</em>";
                        }
                    ?>
                </td>
                <td>
                    <?php // ở cột 1 này có 4 loại (1)lấy gas,  (2)chưa lấy gas trong tuần, 
                        //(3) ký hd từ trước và đến tuần hiện tại vẫn chưa lấy gas
                        // (4) ký hd từ trước và đến tuần hiện tại lấy gas
                        $listKyHDLayGas = isset($data['PhatSinhHD_ThuongLuong'][$type_id][$uid][$sOK]['first_purchase'])?$data['PhatSinhHD_ThuongLuong'][$type_id][$uid][$sOK]['first_purchase']:array();
                        $listKyHDChuaLayGas = isset($data['PhatSinhHD_ThuongLuong'][$type_id][$uid][$sOK]['not_first_purchase'])?$data['PhatSinhHD_ThuongLuong'][$type_id][$uid][$sOK]['not_first_purchase']:array();
                        $listKyHDPrevWeekChuaLayGas = isset($data['KyHDNotFirstPurchase'][$type_id][$uid][0])?$data['KyHDNotFirstPurchase'][$type_id][$uid][0]:array();
                        $listKyHDHaveFirstPurchaseCurrentWeek = isset($data['KyHDHaveFirstPurchaseCurrentWeek'][$type_id][$uid][0])?$data['KyHDHaveFirstPurchaseCurrentWeek'][$type_id][$uid][0]:array();
                        $CountLayGas = count($listKyHDLayGas);
                        $CountChuaLayGas = count($listKyHDChuaLayGas);
                        $totalOne = $CountLayGas+$CountChuaLayGas;
                        $week_current_text = "Tuần ".$model->number_week."/$model->month: ";
                        $CountLayGasText = $CountLayGas>0?" - <span class='hight_light item_b'>$CountLayGas HĐ đã lấy gas</span>":'';
                    ?>
                    <?php if($totalOne): ?>
                        <span class="item_b"><?php echo $week_current_text.$totalOne;?> HĐ <?php echo $CountLayGasText;?> </span><br>
                    <?php endif;?>
                    
                    <?php $aGroupWeek = array(); ?>
                    <?php foreach($listKyHDPrevWeekChuaLayGas as $modelBC): 
                                $aGroupWeek['WEEK_LOOP'][$modelBC->week_setup_id] = $modelBC->week_setup_id;
                                $aGroupWeek['NOT_GET_GAS'][$modelBC->week_setup_id][] = $modelBC;
                            endforeach;
                            foreach($listKyHDHaveFirstPurchaseCurrentWeek as $modelBC):
                                $aGroupWeek['WEEK_LOOP'][$modelBC->week_setup_id] = $modelBC->week_setup_id;
                                $aGroupWeek['FIRST_GET_GAS'][$modelBC->week_setup_id][] = $modelBC;
                            endforeach;
                    ?>
                    <?php if(isset($aGroupWeek['WEEK_LOOP'])): ?>
                        <?php foreach($aGroupWeek['WEEK_LOOP'] as $week_setup_id): ?>
                            <?php
                                $ChuaLayGas = isset($aGroupWeek['NOT_GET_GAS'][$week_setup_id])?count($aGroupWeek['NOT_GET_GAS'][$week_setup_id])." chưa lấy gas":'';
                                $DaLayGas = isset($aGroupWeek['FIRST_GET_GAS'][$week_setup_id])?"<span class='hight_light item_b'>".count($aGroupWeek['FIRST_GET_GAS'][$week_setup_id])." đã lấy gas</span>":'';
                                $mWeekSetup = $aModelWeekSetup[$week_setup_id];
                                $week_setup_text = $mWeekSetup->number_week."/$mWeekSetup->month";
                            ?>
                            <span class="">Tuần <?php echo $week_setup_text.": $ChuaLayGas. $DaLayGas";?></span><br/>
                        <?php endforeach;?>
                    <?php endif;?>
                </td>
                
                <td>
                    <?php // ở cột 2 này có 1 loại thương lượng phát sinh trong tuần
                        $listThuongLuong = isset($data['PhatSinhHD_ThuongLuong'][$type_id][$uid][$sTL])?$data['PhatSinhHD_ThuongLuong'][$type_id][$uid][$sTL]:array();
                    ?>
                    <?php if($listThuongLuongText = count($listThuongLuong)): ?>
                        <span class="item_b"><?php echo $week_current_text.$listThuongLuongText;?> HĐ </span>                    
                    <?php endif;?>
                </td>
                
                <td>
                    <?php // ở cột 3 này có 6 loại (1)lấy gas,  (2)chưa lấy gas trong tuần, ghi chú=>là tab 1 khi view spancop
                        //(3) ký hd từ trước và đến tuần hiện tại vẫn chưa lấy gas
                        // (4) ký hd từ trước và đến tuần hiện tại lấy gas
                        // (5) thương lượng
                        // (6) hủy bỏ
                        $listKyHDLayGas = isset($data['KyHD'][$type_id][$uid][$sOK]['first_purchase'])?$data['KyHD'][$type_id][$uid][$sOK]['first_purchase']:array();
                        $listKyHDChuaLayGas = isset($data['KyHD'][$type_id][$uid][$sOK]['not_first_purchase'])?$data['KyHD'][$type_id][$uid][$sOK]['not_first_purchase']:array();
                        $listKyHDPrevWeekChuaLayGas = isset($data['KyHDNotFirstPurchase'][$type_id][$uid][1])?$data['KyHDNotFirstPurchase'][$type_id][$uid][1]:array();
                        $listKyHDHaveFirstPurchaseCurrentWeek = isset($data['KyHDHaveFirstPurchaseCurrentWeek'][$type_id][$uid][1])?$data['KyHDHaveFirstPurchaseCurrentWeek'][$type_id][$uid][1]:array();
                        $listThuongLuong = isset($data['KyHD'][$type_id][$uid][$sTL])?$data['KyHD'][$type_id][$uid][$sTL]:array();
                        $listHuyBo = isset($data['KyHD'][$type_id][$uid][$sHB])?$data['KyHD'][$type_id][$uid][$sHB]:array();
//                        $totalOne = count($listKyHDLayGas)+count($listKyHDChuaLayGas);
                        $CountLayGas = count($listKyHDLayGas);
                        $CountChuaLayGas = count($listKyHDChuaLayGas);
                        $totalOne = $CountLayGas+$CountChuaLayGas;
                        $CountLayGasText = $CountLayGas>0?" - <span class='hight_light item_b'>$CountLayGas HĐ đã lấy gas</span>":'';
                    ?>
                    
                    <?php if($totalOne): ?>
                        <span class="item_b"><?php echo $week_current_text.$totalOne;?> HĐ <?php echo $CountLayGasText;?> </span><br>
                    <?php endif;?>                    
                    
                    <?php $aGroupWeek = array(); ?>
                    <?php foreach($listKyHDPrevWeekChuaLayGas as $modelBC): 
                                $aGroupWeek['WEEK_LOOP'][$modelBC->week_setup_id] = $modelBC->week_setup_id;
                                $aGroupWeek['NOT_GET_GAS'][$modelBC->week_setup_id][] = $modelBC;
                            endforeach;
                            foreach($listKyHDHaveFirstPurchaseCurrentWeek as $modelBC):
                                $aGroupWeek['WEEK_LOOP'][$modelBC->week_setup_id] = $modelBC->week_setup_id;
                                $aGroupWeek['FIRST_GET_GAS'][$modelBC->week_setup_id][] = $modelBC;
                            endforeach;
                    ?>
                    <?php if(isset($aGroupWeek['WEEK_LOOP'])): ?>
                        <?php foreach($aGroupWeek['WEEK_LOOP'] as $week_setup_id): ?>
                            <?php
                                $ChuaLayGas = isset($aGroupWeek['NOT_GET_GAS'][$week_setup_id])?count($aGroupWeek['NOT_GET_GAS'][$week_setup_id])." chưa lấy gas":'';
                                $DaLayGas = isset($aGroupWeek['FIRST_GET_GAS'][$week_setup_id])?"<span class='hight_light item_b'>".count($aGroupWeek['FIRST_GET_GAS'][$week_setup_id])." đã lấy gas</span>":'';
                                $mWeekSetup = $aModelWeekSetup[$week_setup_id];
                                $week_setup_text = $mWeekSetup->number_week."/$mWeekSetup->month";
                            ?>
                            <span class="">Tuần <?php echo $week_setup_text.": $ChuaLayGas. $DaLayGas";?></span><br/>
                        <?php endforeach;?>
                    <?php endif;?>

                        
                    <?php if($listThuongLuongText = count($listThuongLuong)): ?>
                    <span class="item_b"><?php echo $week_current_text.$listThuongLuongText;?> HĐ Thương Lượng</span>
                    <?php endif;?>
                    <?php if($listHuyBoText = count($listHuyBo)): ?>
                    <span class="item_b"><br><?php echo $week_current_text.$listHuyBoText;?> HĐ Hủy Bỏ</span>
                    <?php endif;?>
                </td>
            </tr>
        <?php endforeach;?>
    <?php endforeach;?>
    </tbody>
</table>
</div>
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/jquery.floatThead.min.js"></script>    
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/underscore-min.js"></script>    

<script>
    fnAddClassOddEven('items');
    $(document).ready(function(){
        $('.items').floatThead();
    });    
</script>