<?php 
$SubmitButtonClass = '';
$ButtonType = 'submit';
$display_none = 'display_none';
//if($is_tab==0){
//    $display_none = '';    
//    $SubmitButtonClass='SubmitButton btn_cancel';
//    $ButtonType = 'button';
//}

?>
<div class="wide form " style="padding:0;">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

    <div class="row more_col display_none">
        <div class="col1">
            <?php echo $form->label($model,'number_week',array()); ?>
            <?php  echo $form->dropDownList($model,'number_week', GasBussinessContract::$ARR_WEEK,array('class'=>'w-150 number_week', 'class_update_val'=>'number_week','empty'=>'Select' )); ?>
        </div>
        <div class="col2">
            <?php echo $form->label($model,'month',array()); ?>
            <?php echo $form->dropDownList($model,'month', ActiveRecord::getMonthVnWithZeroFirst(), array('class'=>'w-150 month', 'class_update_val'=>'month')); ?>
        </div>
        
        <div class="col3">
            <?php echo $form->label($model,'year',array()); ?>
            <?php echo $form->dropDownList($model,'year', ActiveRecord::getRangeYear(), array('class'=>'w-150 year', 'class_update_val'=>'year')); ?>
        </div>        
    </div>
    
    <div class="row more_col">
        <div class="col1">
            <?php echo $form->label($model,'customer_name',array()); ?>
            <?php echo $form->textField($model,'customer_name',array('class'=>'w-150 customer_name', 'class_update_val'=>'customer_name','maxlength'=>200)); ?>
        </div>
        <div class="col2">
            <?php echo $form->label($model,'customer_contact',array()); ?>
            <?php echo $form->textField($model,'customer_contact',array('class'=>'w-150 customer_contact', 'class_update_val'=>'customer_contact','maxlength'=>200)); ?>
        </div>
        
        <div class="col3">
            <?php echo $form->label($model,'address',array()); ?>
            <?php echo $form->textField($model,'address',array('class'=>'w-250 address', 'class_update_val'=>'address','maxlength'=>200)); ?>
        </div>        
    </div>
    <div class="row more_col">
        <div class="col1">
            <?php echo $form->label($model,'phone',array()); ?>
            <?php echo $form->textField($model,'phone',array('class'=>'w-150 phone', 'class_update_val'=>'phone','maxlength'=>200)); ?>
        </div>
        <div class="col2">
            <?php echo $form->label($model,'type',array()); ?>
            <?php echo $form->dropDownList($model,'type', GasBussinessContract::$ARR_TYPE_SALE, array('class'=>'w-150 type', 'class_update_val'=>'type','empty'=>'Select')); ?>
        </div>
        
        <div class="col3">
            <?php echo $form->label($model,'qty_of_big_customer',array()); ?>
            <?php echo $form->dropDownList($model,'qty_of_big_customer', MyFormat::BuildNumberOrder(50), array('class'=>'w-150','empty'=>'Select')); ?>
        </div>               
    </div>
    
    
    <div class="row">      
        <?php /* echo $form->hiddenField($model,'employee_id', array('class'=>'uid_login_hidden', 'class_update_val'=>'employee_id')); ?>
        <?php if($is_tab==0): ?>
        <?php echo $form->labelEx($model,'employee_id', array('label'=>'Sale / Nhân Viên Giao Hàng')); ?>
        <?php 
            // widget auto complete search user customer and supplier
            $aData = array(
                'model'=>$model,
                'field_customer_id'=>'employee_id',
                'url'=> Yii::app()->createAbsoluteUrl('admin/ajax/search_for_bussiness_contract'),
                'name_relation_user'=>'rEmployee',
                'fnSelectCustomer'=>'fnAfterSelectSaleOrAgent',
            );
            $this->widget('ext.SpaAutocompleteCustomer.SpaAutocompleteCustomer',
                array('data'=>$aData));                                        
        ?>                        
        <?php endif; 
        chỗ này có thể xử lý thêm 1 field hidden là role_id vào, rồi xuống dưới phần search if else nó 
         * để quyết định sẽ search theo uid_login hay employee_id ... will resolve
         *          */?>
        
        <?php 
            $userRole = Yii::app()->user->role_id;
            $aRoleCheck = array(ROLE_SUB_USER_AGENT, ROLE_SALE);
        ?>
        <?php if(!in_array($userRole, $aRoleCheck)):?>
            <?php echo $form->hiddenField($model,'uid_login', array('class'=>'', 'class_update_val'=>'')); ?>
            <?php echo $form->labelEx($model,'uid_login'); ?>
            <?php 
                // widget auto complete search user customer and supplier
                $aData = array(
                    'model'=>$model,
                    'field_customer_id'=>'uid_login',
                    'url'=> Yii::app()->createAbsoluteUrl('admin/ajax/search_for_bussiness_contract'),
                    'name_relation_user'=>'rUidLogin',
                );
                $this->widget('ext.SpaAutocompleteCustomer.SpaAutocompleteCustomer',
                    array('data'=>$aData));                                        
            ?>                        
        <?php endif; ?>
        
    </div>
        
    <div class="row display_none">
        <?php echo $form->label($model,'code_no',array()); ?>
        <?php echo $form->textField($model,'code_no',array('class_update_val'=>'code_no','class'=>'code_no','maxlength'=>30)); ?>
    </div>
    
    <div class="row buttons" style="padding-left: 159px;">
    <?php $this->widget('bootstrap.widgets.TbButton', array(
        'buttonType'=>$ButtonType,
        'label'=>'Search',
        'type'=>'null', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
        'size'=>'small', // null, 'large', 'small' or 'mini'
        'htmlOptions' => array('class' => $SubmitButtonClass),
    )); ?>	
    </div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->