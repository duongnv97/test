
<div class="search-form2 search-form-only-css is_tab" is_tab="2"  style="">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
        'is_tab'=>2,
)); ?>
</div><!-- search-form -->
<?php
Yii::app()->clientScript->registerScript('search2', "
$('.search-form2 form').submit(function(){
	$.fn.yiiGridView.update('gas-bussiness-contract-grid2', {
                url : $(this).attr('action'),
		data: $(this).serialize()
	});                      
	return false;
});
");

Yii::app()->clientScript->registerScript('ajaxupdate2', "
$('#gas-bussiness-contract-grid2 a.ajaxupdate').live('click', function() {
    $.fn.yiiGridView.update('gas-bussiness-contract-grid2', {
        type: 'POST',
        url: $(this).attr('href'),
        success: function() {                        
            $.fn.yiiGridView.update('gas-bussiness-contract-grid2');
        }
    });
    return false;
});
");
?>

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'gas-bussiness-contract-grid2',
	'dataProvider'=>$model->searchThuongLuong(),
        'afterAjaxUpdate'=>'function(id, data){ fnUpdateColorbox(1);}',
        'template'=>''
        . '<div class="clr hight_light item_b f_size_18">'.$TextTitleInfo.' Khách Hàng Thương Lượng</div>'
        . '{pager}{summary}{items}{pager}{summary}',  
        'pager' => array(
            'maxButtonCount'=>  CmsFormatter::$PAGE_MAX_BUTTON,
        ),            
	'columns'=>array(
        array(
            'header' => 'S/N',
            'type' => 'raw',
            'value' => '$this->grid->dataProvider->pagination->currentPage * $this->grid->dataProvider->pagination->pageSize + ($row+1)',
            'headerHtmlOptions' => array('width' => '10px','style' => 'text-align:center;'),
            'htmlOptions' => array('style' => 'text-align:center;')
        ),
//                array(
//                    'name'=>'code_no',
//                    'htmlOptions' => array('style' => 'text-align:center;width:50px;')
//                ),
//                array(
//                    'name'=>'agent_id',
//                    'value'=>'$data->rAgent?$data->rAgent->first_name:""',
//                    'htmlOptions' => array('style' => 'width:100px;')
//                ),
                array(
                    'header' => 'NV Giao Hàng / Sale',
                    'name'=>'employee_id',
                    'value'=>'$data->rEmployee?$data->rEmployee->first_name:""',
                    'htmlOptions' => array('style' => 'width:80px;')
                ),
                array(
                    'name'=>'type_customer',
                    'value'=>'$data->type_customer?CmsFormatter::$CUSTOMER_BO_MOI[$data->type_customer]:""',
                    'htmlOptions' => array('style' => 'width:30px;')
                ),
                array(
                    'name'=>'customer_name',
                    'type'=>'SpancopCustomerName',
                    'value'=>'$data',
                    'htmlOptions' => array('style' => 'width:120px;')
                ),
                array(
                    'name'=>'address',
                    'type'=>'SpancopAddress',
                    'value'=>'$data',
                    'htmlOptions' => array('style' => 'width:120px;')
                ),
//                array(
//                    'name'=>'phone',
//                    'htmlOptions' => array('style' => 'width:80px;')
//                ),
//                array(
//                    'name'=>'customer_contact',
//                    'htmlOptions' => array('style' => 'width:80px;')
//                ),
                array(
                    'name'=>'b12',
                    'type'=>'SpancopB12',
                    'value'=>'$data',
                    'htmlOptions' => array('style' => 'text-align:center;width:30px;')
                ),
                array(
                    'name'=>'b45',
                    'type'=>'SpancopB45',
                    'value'=>'$data',                    
                    'htmlOptions' => array('style' => 'text-align:center;width:30px;')
                ),
                array(
                    'name' => 'note',
                    'type' => 'html',
                    'value' => 'nl2br($data->note)',
//                    'htmlOptions' => array('style' => 'width:120px;')
                ),    
//                array(
//                    'name' => 'guide_help',
//                    'type' => 'html',
//                    'value' => 'nl2br($data->guide_help)',
////                    'htmlOptions' => array('style' => 'width:80px;')
//                ), 
                array(
                    'name'=>'date_plan',
                    'type'=>'date',
                    'htmlOptions' => array('style' => 'text-align:center;width:50px;')
                ),
                array(
                    'name'=>'status',
                    'type'=>'BussinessContractStatus',
                    'value'=>'$data',
                    'htmlOptions' => array('style' => 'text-align:center;width: 45px;')
                ),
                array(
                    'name' => 'created_date',
                    'type' => 'Datetime',
                    'htmlOptions' => array('style' => 'width:50px;')
                ),                           
		array(
                    'header' => 'Actions',
                    'class'=>'CButtonColumn',
                    'template'=> ControllerActionsName::createIndexButtonRoles($actions),
                    'buttons'=>array(
                            'update'=>array(
                                'visible'=> 'GasCheck::AgentCanUpdateBussinessContract($data)',
                            ),
                            'delete'=>array(
                                'visible'=> 'GasCheck::canDeleteData($data)',
                            ),
                    ),                     
		),
	),
)); ?>