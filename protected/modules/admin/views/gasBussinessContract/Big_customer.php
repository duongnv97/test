<?php
$this->breadcrumbs=array(
	'Danh Sách SPANCOP khách hàng lớn',
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('gas-bussiness-contract-grid', {
                url : $(this).attr('action'),
		data: $(this).serialize()
	});
	return false;
});
");

Yii::app()->clientScript->registerScript('ajaxupdate', "
$('#gas-bussiness-contract-grid a.ajaxupdate').live('click', function() {
    $.fn.yiiGridView.update('gas-bussiness-contract-grid', {
        type: 'POST',
        url: $(this).attr('href'),
        success: function() {
            $.fn.yiiGridView.update('gas-bussiness-contract-grid');
        }
    });
    return false;
});
");
?>

<h1>Danh Sách SPANCOP khách hàng lớn</h1>

<div class="">

<?php echo CHtml::link('Tìm Kiếm Nâng Cao','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search_dieuphoi',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'gas-bussiness-contract-grid',
	'dataProvider'=>$model->search_big_customer(),
        'afterAjaxUpdate'=>'function(id, data){fnUpdateColorbox();}',
        'template'=>'{pager}{summary}{items}{pager}{summary}', 
        'pager' => array(
            'maxButtonCount'=>  CmsFormatter::$PAGE_MAX_BUTTON,
        ),            
	//'filter'=>$model,
	'columns'=>array(
        array(
            'header' => 'S/N',
            'type' => 'raw',
            'value' => '$this->grid->dataProvider->pagination->currentPage * $this->grid->dataProvider->pagination->pageSize + ($row+1)',
            'headerHtmlOptions' => array('width' => '10px','style' => 'text-align:center;'),
            'htmlOptions' => array('style' => 'text-align:center;')
        ),
//                'id',
                array(
                    'name'=>'agent_id',
                    'value'=>'$data->rAgent?$data->rAgent->first_name:""',
                    'htmlOptions' => array('style' => 'width:100px;'),
                    'visible'=>Yii::app()->user->role_id!=ROLE_SALE,
                ),
                array(
                    'header' => 'NV Giao Hàng / Sale',
                    'name'=>'employee_id',
                    'value'=>'$data->rEmployee?$data->rEmployee->first_name:""',
                    'htmlOptions' => array('style' => 'width:80px;')
                ),
                array(
                    'name'=>'customer_name',
                    'type'=>'SpancopCustomerName',
                    'value'=>'$data',
                    'htmlOptions' => array('style' => 'width:80px;')
                ),
                array(
                    'name'=>'address',
                    'type'=>'SpancopAddress',
                    'value'=>'$data',
                    'htmlOptions' => array('style' => 'width:100px;')
                ),
                array(
                    'name'=>'phone',
                    'htmlOptions' => array('style' => 'width:80px;')
                ),
                array(
                    'name'=>'customer_contact',
                    'htmlOptions' => array('style' => 'width:80px;')
                ),
                array(
                    'name'=>'b12',
                    'type'=>'SpancopB12',
                    'value'=>'$data',
                    'htmlOptions' => array('style' => 'text-align:center;width:30px;')
                ),
                array(
                    'name'=>'b45',
                    'type'=>'SpancopB45',
                    'value'=>'$data',                    
                    'htmlOptions' => array('style' => 'text-align:center;width:30px;')
                ),
                array(
                    'name' => 'note',
                    'type' => 'html',
                    'value' => 'nl2br($data->note)',
                    'htmlOptions' => array('style' => 'width:120px;')
                ),            
                array(
                    'name'=>'date_plan',
                    'type'=>'date',
                    'htmlOptions' => array('style' => 'text-align:center;width:50px;')
                ),
                array(
                    'name'=>'status',
                    'value'=>'GasBussinessContract::$ARR_STATUS[$data->status]',
                    'htmlOptions' => array('style' => 'text-align:center;width:70px;')
                ), 
                array(
                    'name' => 'created_date',
                    'type' => 'Datetime',
                    'htmlOptions' => array('style' => 'width:50px;')
                ),
	),
)); ?>

<script>
$(document).ready(function() {
    fnUpdateColorbox();
});

//http://www.jacklmoore.com/colorbox/
function fnUpdateColorbox(){
//    fnShowhighLightTr();
    $(".update_customer_sign_contract").colorbox({iframe:true,innerHeight:'550', innerWidth: '950', overlayClose :false,escKey:false,close: "<span title='close'>close</span>"});
//    fixTargetBlank();
    
}
</script>

</div>