<?php 
$SubmitButtonClass = '';
$ButtonType = 'submit';
$display_none = 'display_none';
//if($is_tab==0){
//    $display_none = '';    
//    $SubmitButtonClass='SubmitButton btn_cancel';
//    $ButtonType = 'button';
//}

?>
<div class="wide form " style="padding:0;">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'post',
)); ?>

    <div class="row more_col">
        <div class="col1">
            <?php echo $form->label($model,'number_week',array()); ?>
            <?php  echo $form->dropDownList($model,'number_week', GasBussinessContract::$ARR_WEEK,array('class'=>'w-150 number_week' )); ?>
        </div>
        <div class="col2">
            <?php echo $form->label($model,'month',array()); ?>
            <?php echo $form->dropDownList($model,'month', ActiveRecord::getMonthVnWithZeroFirst(), array('class'=>'w-150 month', 'class_update_val'=>'month')); ?>
        </div>
        
        <div class="col3">
            <?php echo $form->label($model,'year',array()); ?>
            <?php echo $form->dropDownList($model,'year', ActiveRecord::getRangeYear(), array('class'=>'w-150 year', 'class_update_val'=>'year')); ?>
        </div>        
    </div>    
    <div class="row more_col">
        <div class="col2">
            <?php echo $form->label($model,'type',array()); ?>
            <?php echo $form->dropDownList($model,'type', GasBussinessContract::$ARR_TYPE_SALE, array('class'=>'w-200' ,'empty'=>'Select')); ?>
        </div>
    </div>
    <div class="row">      
        <?php 
            $userRole = Yii::app()->user->role_id;
            $aRoleCheck = array(ROLE_SUB_USER_AGENT, ROLE_SALE);
        ?>
        <?php if(!in_array($userRole, $aRoleCheck)):?>
            <?php echo $form->hiddenField($model,'uid_login', array('class'=>'', 'class_update_val'=>'')); ?>
            <?php echo $form->labelEx($model,'uid_login'); ?>
            <?php 
                // widget auto complete search user customer and supplier
                $aData = array(
                    'model'=>$model,
                    'field_customer_id'=>'uid_login',
                    'url'=> Yii::app()->createAbsoluteUrl('admin/ajax/search_for_bussiness_contract'),
                    'name_relation_user'=>'rUidLogin',
                );
                $this->widget('ext.SpaAutocompleteCustomer.SpaAutocompleteCustomer',
                    array('data'=>$aData));                                        
            ?>                        
        <?php endif; ?>
        
    </div>
        
    <div class="row buttons" style="padding-left: 159px;">
    <?php $this->widget('bootstrap.widgets.TbButton', array(
        'buttonType'=>$ButtonType,
        'label'=>'Search',
        'type'=>'null', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
        'size'=>'small', // null, 'large', 'small' or 'mini'
        'htmlOptions' => array('class' => $SubmitButtonClass),
    )); ?>
        <?php if(Yii::app()->user->role_id==ROLE_ADMIN): ?>
        <a href="<?php echo Yii::app()->createAbsoluteUrl('admin/ajax/runCronUpdateFirstPurchase');?>" class="btn_cancel runCronUpdateFirstPurchase">Cập nhật ngày lấy hàng của KH</a>
        <?php endif;?>
    </div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->