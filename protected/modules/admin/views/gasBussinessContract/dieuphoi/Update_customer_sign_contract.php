<h1>Cập nhật khách hàng cho SPANCOP đã ký hợp đồng</h1>
<div class="form">
<?php 
$form=$this->beginWidget('CActiveForm', array(
	'id'=>'gas-bussiness-contract-form',
	'enableAjaxValidation'=>false,
)); ?>

        <?php  if(Yii::app()->user->hasFlash('successUpdate')): ?>
            <div class='success_div'><?php echo Yii::app()->user->getFlash('successUpdate');?></div>         
        <?php endif; ?>  	
            
	<div class="row">
            <?php echo $form->label($model,'employee_id', array('label'=>'Đại Lý')); ?>
            <span><?php echo $model->rAgent?$model->rAgent->first_name:"";?></span>
	</div>
            <div class="clr"></div>
	<div class="row">
            <?php echo $form->label($model,'employee_id', array('label'=>'NV Giao Hàng / Sale')); ?>
            <span><?php 
                $mUser = $model->rEmployee;
                if($mUser){
                    // NV Giao Hàng
                    echo ActiveRecord::FormatNameSale($mUser);
                }
            ?></span>
	</div>
            <div class="clr"></div>
	<div class="row">
            <?php echo $form->label($model,'employee_id', array('label'=>'Khách Hàng')); ?>
            <span><?php echo $model->customer_name;?></span>
	</div>
            <div class="clr"></div>
	<div class="row">
            <?php echo $form->label($model,'employee_id', array('label'=>'Tên Liên Hệ')); ?>
            <span><?php echo $model->customer_contact;?></span>
	</div>
            <div class="clr"></div>
	<div class="row">
            <?php echo $form->label($model,'employee_id', array('label'=>'Địa Chỉ')); ?>
            <span><?php echo $model->address;?></span>
	</div>
            <div class="clr"></div>
	<div class="row">
            <?php echo $form->label($model,'employee_id', array('label'=>'Điện Thoại')); ?>
            <span><?php echo $model->phone;?></span>
	</div>
            <div class="clr"></div>
	<div class="row">
            <?php echo $form->label($model,'customer_id', array()); ?>
            <?php echo $form->hiddenField($model,'customer_id', array('class'=>'')); ?>
            <?php 
                // widget auto complete search user customer and supplier
                $aData = array(
                    'model'=>$model,
                    'field_customer_id'=>'customer_id',
                    'url'=> Yii::app()->createAbsoluteUrl('admin/ajax/search_all_customer_storecard'),
                    'name_relation_user'=>'rCustomer',
                );
                $this->widget('ext.SpaAutocompleteCustomer.SpaAutocompleteCustomer',
                    array('data'=>$aData));                                        
            ?>
	</div>

	<div class="row buttons" style="padding-left: 141px;">
		        <?php $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType'=>'submit',
            'label'=>$model->isNewRecord ? 'Create' :'Save',
            'type'=>'null', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
            'size'=>'small', // null, 'large', 'small' or 'mini'
            //'htmlOptions' => array('style' => 'margin-bottom: 10px; float: right;'),
        )); ?>	
        <input class='cancel_iframe' type='button' value='Cancel'>
        </div>

<?php $this->endWidget(); ?>

</div><!-- form -->
<script>
    $(document).ready(function(){
        $('.form').find('button:submit').click(function(){
            $.blockUI({ overlayCSS: { backgroundColor: '#fff' } }); 
        });        
    });
    $(window).load(function(){ // không dùng dc cho popup
//        $('.materials_table').floatThead();
//        fnResizeColorbox();
    });
    
    function fnResizeColorbox(){
//        var y = $('body').height()+100;
        var y = $('#main_box').height()+100;
        parent.$.colorbox.resize({innerHeight:y});        
    }
</script>