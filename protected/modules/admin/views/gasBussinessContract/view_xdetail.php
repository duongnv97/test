<?php $this->widget('application.components.widgets.XDetailView', array(
    'data' => $model,
    'attributes' => array(
        'group11'=>array(
            'ItemColumns' => 3,
            'attributes' => array(
                'code_no',
                array(
                    'name'=>'agent_id',
                    'value'=>$model->rAgent?$model->rAgent->first_name:'',
                ),
                array(
                    'name'=>'uid_login',
                    'value'=>$model->rUidLogin?$model->rUidLogin->first_name:'',
                ),		
                array(
                    'name'=>'status',
                    'value'=>GasBussinessContract::$ARR_STATUS[$model->status],
                ),				
                array(
                    'name'=>'customer_zone',
                    'value'=>GasBussinessContract::GetCustomerZoneView($model->customer_zone),
                ),				
                array(
                    'name'=>'employee_id',
                    'value'=>$model->rEmployee?$model->rEmployee->first_name:'',
                ),				
                'customer_name',
                'customer_contact',
                array(
                    'name'=>'address',
                    'type'=>'html',
                    'value'=>nl2br($model->address),
                ),
                'phone',
                'date_plan:date',
                array(
                    'name'=>'customer_id',
                    'type'=>'NameUser',
                    'value'=>$model->rCustomer?$model->rCustomer:0,
                ),
                'b12',
                'b45',
                array(
                    'name'=>'note',
                    'type'=>'html',
                    'value'=>nl2br($model->note),
                ),
                
                'created_date:datetime',
                array(
                    'name'=>'guide_help',
                    'type'=>'html',
                    'value'=>nl2br($model->guide_help),
                    'visible' => !empty($model->guide_help_uid),
                ),
                array(
                    'name'=>'guide_help_uid',
                    'value'=>$model->rGuideHelpUid?$model->rGuideHelpUid->first_name:'',
                    'visible' => !empty($model->guide_help_uid),
                ),
                array(
                    'name'=>'guide_help_date',
                    'type'=>'datetime',
                    'visible' => !empty($model->guide_help_uid),
                ),
            ),
        ),
    ),
)); ?>

<script>
    $(document).ready(function(){
        $('.xdetail_c2').each(function(){
            $(this).find('td:first').addClass("w-300");
            $(this).find('td').eq(1).addClass("w-100");
            $(this).find('th').addClass("w-120");
        });
        
    });
</script>