<?php
$this->breadcrumbs=array(
	'Gas Bussiness Contracts'=>array('index'),
	$model->id,
);
$menus = array(
	array('label'=>'GasBussinessContract Management', 'url'=>array('index')),
	array('label'=>'Tạo Mới GasBussinessContract', 'url'=>array('create')),
	array('label'=>'Cập Nhật GasBussinessContract', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Xóa GasBussinessContract', 'url'=>array('delete'), 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Bạn chắc chắn muốn xóa ?')),
);
//$this->menu= ControllerActionsName::createMenusRoles($menus, $actions);
?>

<h1>Xem SPANCOP Khách Hàng: <?php echo $model->customer_name; ?></h1>
<?php echo MyFormat::BindNotifyMsg(); ?>
<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'code_no',
                array(
                    'name'=>'agent_id',
                    'value'=>$model->rAgent?$model->rAgent->first_name:'',
                ),
                array(
                    'name'=>'uid_login',
                    'value'=>$model->rUidLogin?$model->rUidLogin->first_name:'',
                ),		
                array(
                    'name'=>'status',
                    'value'=>GasBussinessContract::$ARR_STATUS[$model->status],
                ),				
                array(
                    'name'=>'customer_zone',
                    'value'=>GasBussinessContract::GetCustomerZoneView($model->customer_zone),
                ),				
                array(
                    'name'=>'employee_id',
                    'value'=>$model->rEmployee?$model->rEmployee->first_name:'',
                ),				
		'customer_name',
		'customer_contact',
                array(
                    'name'=>'address',
                    'type'=>'html',
                    'value'=>nl2br($model->address),
                ),
		'phone',
		'date_plan',
		'b12',
		'b45',
                array(
                    'name'=>'note',
                    'type'=>'html',
                    'value'=>nl2br($model->note),
                ),
                array(
                    'name'=>'customer_id',
                    'type'=>'NameUser',
                    'value'=>$model->rCustomer?$model->rCustomer:0,
                ),
		'created_date',
                array(
                    'name'=>'guide_help',
                    'type'=>'html',
                    'value'=>nl2br($model->guide_help),
                    'visible' => !empty($model->guide_help_uid),
                ),
                array(
                    'name'=>'guide_help_uid',
                    'value'=>$model->rGuideHelpUid?$model->rGuideHelpUid->first_name:'',
                    'visible' => !empty($model->guide_help_uid),
                ),
                array(
                    'name'=>'guide_help_date',
                    'type'=>'datetime',
                    'visible' => !empty($model->guide_help_uid),
                ),
	),
)); 
?>

<?php include 'view_update_guide_help.php';?>
<div class="form">
    <div class="row buttons" style="padding-left: 141px;">
        <input class='cancel_iframe' type='button' value='Cancel'>
    </div>
</div>

<script>
    $(window).load(function(){
//        $('.materials_table').floatThead();// không dùng dc cho popup
        fnResizeColorbox();
    });
    
    function fnResizeColorbox(){
//        var y = $('body').height()+100;
        var y = $('#main_box').height()+100;
        parent.$.colorbox.resize({innerHeight:y});
    }
    $(document).ready(function(){
        $('.form').find('button:submit').click(function(){
            $.blockUI({ overlayCSS: { backgroundColor: '#fff' } });
        });
    });
</script>
