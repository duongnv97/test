<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'bu-vo-form',
	'enableAjaxValidation'=>false,
        'htmlOptions' => array('enctype' => 'multipart/form-data'),
)); ?>

    <p class="note">Dòng có dấu <span class="required">*</span> là bắt buộc.</p>
    <?php echo MyFormat::BindNotifyMsg(); ?>
    <div class="row">
        <?php echo $form->labelEx($model,'zone_id'); ?>
        <?php echo $form->dropDownList($model,'zone_id', $model->getListdataZone(),array('class'=>'', 'empty'=>'Select')); ?>
        <?php echo $form->error($model,'zone_id'); ?>
    </div>
    <div class="row">
        <?php // echo $form->labelEx($model,'list_materials_parent_id_gas'); ?>
        <div class="fix-label">
            <?php
//               $this->widget('ext.multiselect.JMultiSelect',array(
//                     'model'=>$model,
//                     'attribute'=>'list_materials_parent_id_gas',
////                             'data'=>  Users::getSelectByRoleFinal(ROLE_AGENT, array('gender'=>Users::IS_AGENT)),
//                     'data'=> $model->getListdataMaterialParentGas(),
//                     // additional javascript options for the MultiSelect plugin
//                    'options'=>array('selectedList' => 30,),
//                     // additional style
//                     'htmlOptions'=>array('style' => 'width: 800px;'),
//               ));    
           ?>
        </div>
        <?php // echo $form->error($model,'list_materials_parent_id_gas'); ?>
    </div>
    
    <!-- BEGIN Gas -->
    <div class="WrapBoxMaterial" NameInput="BuVo[list_materials_parent_id_gas][]">
        <div class="row">
            <?php echo $form->labelEx($model,'list_materials_parent_id_gas'); ?>
            <?php // $dataCat = CHtml::listData(GasMaterials::getCatLevel2(GasMaterials::$MATERIAL_GAS),'id','name','group'); ?>
            <?php // echo $form->dropDownList($model,'list_materials_parent_id_gas', $dataCat,array('class'=>'category_ajax', 'style'=>'width:385px;','empty'=>'Select')); ?>		

            <?php echo $form->hiddenField($model,'list_hide'); ?>		
            <?php 
                // widget auto complete search material
                $aData = array(
                    'model'=>$model,
                    'MaterialsJson'=> $model->getListdataMaterialParentGasJson(),
                    'field_material_id'=>'list_hide',
                    'name_relation_material'=>'rFake',
                    'field_autocomplete_name'=>'autocomplete_material',
                );
                $this->widget('ext.GasAutocompleteMaterial.GasAutocompleteMaterial',
                    array('data'=>$aData));                                        
            ?>            
            <?php echo $form->error($model,'list_materials_parent_id_gas'); ?>
        </div>

        <div class="clr"></div>
        <div class="row">
            <label>&nbsp</label>
            <table class="materials_table hm_table tb_deloy_by w-500">
                <thead>
                    <tr>
                        <th class="item_c w-20">#</th>
                        <th class="item_code item_c w-300">Vật tư</th>
                        <th class="item_unit last item_c">Xóa</th>
                    </tr>
                </thead>
                <tbody>
                    <?php $index = 1;
                        $aDataMaterial = $model->getListGas(array('getArray'=>1));
                        if(!is_array($aDataMaterial)){
                            $aDataMaterial = [];
                        }
                    ?>
                    <?php foreach($aDataMaterial as $materials_id=>$name): ?>
                    <?php // if(is_null($mUser)) continue; ?>
                    <tr>                        
                        <td class="item_c order_no"><?php echo $index++ ?></td>
                        <td class="uid_<?php echo $materials_id;?>">
                            <input name="BuVo[list_materials_parent_id_gas][]" value="<?php echo $materials_id;?>" type="hidden">
                            <?php echo $name;?>
                        </td>
                        <td class="item_c last"><span class="remove_icon_only"></span></td>
                    </tr>
                    <?php endforeach;?>
                </tbody>

            </table>
        </div>
    </div><!--end <div class="WrapBoxMaterial">-->
    
    <br><br>
    
    <!-- BEGIN VO -->
    <div class="WrapBoxMaterial" NameInput="BuVo[list_materials_id_vo][]">
        <div class="row">
            <?php echo $form->labelEx($model,'list_materials_id_vo'); ?>
            <?php // $dataCat = CHtml::listData(GasMaterials::getCatLevel2(GasMaterials::$MATERIAL_GAS),'id','name','group'); ?>
            <?php // echo $form->dropDownList($model,'list_materials_id_vo', $dataCat,array('class'=>'category_ajax', 'style'=>'width:385px;','empty'=>'Select')); ?>		

            <?php echo $form->hiddenField($model,'list_hide'); ?>		
            <?php 
                // widget auto complete search material
                $aData = array(
                    'model'=>$model,
                    'MaterialsJson'=> $model->getListdataMaterialVoJson(),
                    'field_material_id'=>'list_hide',
                    'name_relation_material'=>'rFake',
                    'field_autocomplete_name'=>'autocomplete_material_1',
                );
                $this->widget('ext.GasAutocompleteMaterial.GasAutocompleteMaterial',
                    array('data'=>$aData));                                        
            ?>            
            <?php echo $form->error($model,'list_materials_id_vo'); ?>
        </div>

        <div class="clr"></div>
        <div class="row">
            <label>&nbsp</label>
            <table class="materials_table hm_table tb_deloy_by w-500">
                <thead>
                    <tr>
                        <th class="item_c w-20">#</th>
                        <th class="item_code item_c w-300">Vật tư</th>
                        <th class="item_unit last item_c">Xóa</th>
                    </tr>
                </thead>
                <tbody>
                    <?php $index = 1;
                        $aDataMaterial = $model->getListVo(array('getArray'=>1));
                        if(!is_array($aDataMaterial)){
                            $aDataMaterial = [];
                        }
                    ?>
                    <?php foreach($aDataMaterial as $materials_id=>$name): ?>
                    <?php // if(is_null($mUser)) continue; ?>
                    <tr>                        
                        <td class="item_c order_no"><?php echo $index++ ?></td>
                        <td class="uid_<?php echo $materials_id;?>">
                            <input name="BuVo[list_materials_id_vo][]" value="<?php echo $materials_id;?>" type="hidden">
                            <?php echo $name;?>
                        </td>
                        <td class="item_c last"><span class="remove_icon_only"></span></td>
                    </tr>
                    <?php endforeach;?>
                </tbody>

            </table>
        </div>
    </div><!--end <div class="WrapBoxMaterial">-->
    
    
    <div class="row">
        <?php // echo $form->labelEx($model,'list_materials_id_vo'); ?>
        <div class="fix-label">
            <?php
//               $this->widget('ext.multiselect.JMultiSelect',array(
//                     'model'=>$model,
//                     'attribute'=>'list_materials_id_vo',
////                             'data'=>  Users::getSelectByRoleFinal(ROLE_AGENT, array('gender'=>Users::IS_AGENT)),
//                     'data'=> $model->getListdataMaterialVo(),
//                     // additional javascript options for the MultiSelect plugin
//                    'options'=>array('selectedList' => 30,),
//                     // additional style
//                     'htmlOptions'=>array('style' => 'width: 800px;'),
//               ));    
           ?>
        </div>
        <?php // echo $form->error($model,'list_materials_id_vo'); ?>
    </div>
    
    <div class="row">
        <?php echo $form->labelEx($model,'price'); ?>
        <?php echo $form->textField($model,'price',array('class'=> 'w-200 number_only ad_fix_currency')); ?>
        <?php echo $form->error($model,'price'); ?>
    </div>
    
    <div class="row buttons" style="padding-left: 141px;">
        <?php $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType'=>'submit',
            'label'=>$model->isNewRecord ? 'Create' :'Save',
            'type'=>'null', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
            'size'=>'small', // null, 'large', 'small' or 'mini'
            //'htmlOptions' => array('style' => 'margin-bottom: 10px; float: right;'),
        )); ?>	
    </div>

<?php $this->endWidget(); ?>
    
</div><!-- form -->
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/autoNumeric-master/autoNumeric.js"></script>
<?php Yii::app()->clientScript->registerCoreScript('jquery.ui'); ?>
<script>
    $(document).ready(function(){
        fnInitInputCurrency();
        $('.form').find('button:submit').click(function(){
            $.blockUI({ overlayCSS: { backgroundColor: '#fff' } }); 
        });
        fnBindRemoveIcon();
    });
    
    function afterMaterialSelect(idField, ui){
        var WrapBoxMaterial = $(idField).closest('.WrapBoxMaterial');
        var nameInput   =   WrapBoxMaterial.attr('NameInput');
        var ClassCheck = "uid_"+ui.item.id;
        var input = '<input type="hidden" name="'+nameInput+'" value="'+ui.item.id+'">';
        
        var td_name = '<td class="'+ClassCheck+'">'+ui.item.name + input + '</td>';
        var td_remove = '<td class="item_c last"><span class="remove_icon_only"></span></td>';
        var tr = '<tr><td class="item_c order_no"></td>'+td_name+td_remove+'</tr>';
        if( WrapBoxMaterial.find('tbody').find('.'+ClassCheck).size() < 1 ) {
            WrapBoxMaterial.find('tbody').append(tr);
        }
        fnRefreshOrderNumber();
    }
    
</script>