<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=> GasCheck::getCurl(),
	'method'=>'get',
)); ?>

	<div class="row">
            <?php echo $form->label($model,'zone_id',array()); ?>
            <?php echo $form->dropDownList($model,'zone_id', $model->getListdataZone(),array('class'=>'w-200', 'empty'=>'Select')); ?>
	</div>

	<div class="row">
            <?php echo $form->label($model,'price',array()); ?>
            <?php echo $form->textField($model,'price',array('class'=>'w-200')); ?>
	</div>
	<div class="row buttons" style="padding-left: 159px;">
            <?php $this->widget('bootstrap.widgets.TbButton', array(
                'buttonType'=>'submit',
                'label'=>'Search',
                'type'=>'null', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
                'size'=>'small', // null, 'large', 'small' or 'mini'
                //'htmlOptions' => array('style' => 'margin-bottom: 10px; float: right;'),
            )); ?>	
        </div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->