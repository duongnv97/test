<?php
$this->breadcrumbs=array(
	'Bán Hàng',
);

$menus=array(
	array('label'=> Yii::t('translation','Thêm Bán Hàng'), 'url'=>array('create')),
);
$this->menu= ControllerActionsName::createMenusRoles($menus, $actions);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('gas-daily-selling-grid', {
                url : $(this).attr('action'),
		data: $(this).serialize()
	});
	return false;
});
");

Yii::app()->clientScript->registerScript('ajaxupdate', "
$('#gas-daily-selling-grid a.ajaxupdate').live('click', function() {
    $.fn.yiiGridView.update('gas-daily-selling-grid', {
        type: 'POST',
        url: $(this).attr('href'),
        success: function() {
            $.fn.yiiGridView.update('gas-daily-selling-grid');
        }
    });
    return false;
});
");
?>

<h1><?php echo Yii::t('translation', 'Dữ Liệu Bán Hàng'); ?></h1>

<?php if(Yii::app()->user->role_id==ROLE_ADMIN):?>
<h2>
    <a href="<?php echo Yii::app()->createAbsoluteUrl('admin/gasdailyselling/import_dailyselling');?>">
        Nhập từ file Excel
    </a>    
	<br />
	<!--
     <a href="<?php echo Yii::app()->createAbsoluteUrl('admin/gasdailyselling/export_dailyselling');?>">
        Xuất ra file Excel
    </a>  	
	-->
</h2>
<?php endif;?>

<?php echo CHtml::link(Yii::t('translation','Advanced Search'),'#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'gas-daily-selling-grid',
	'dataProvider'=>$model->search(),
        'template'=>'{pager}{summary}{items}{pager}{summary}',     
	'enableSorting' => false,
	//'filter'=>$model,
	'columns'=>array(
        array(
            'header' => 'S/N',
            'type' => 'raw',
            'value' => '$this->grid->dataProvider->pagination->currentPage * $this->grid->dataProvider->pagination->pageSize + ($row+1)',
            'headerHtmlOptions' => array('width' => '30px','style' => 'text-align:center;'),
            'htmlOptions' => array('style' => 'text-align:center;')
        ),
	array(
            'header' => 'Mã KH',
            'value' => '$data->customer?$data->customer->code_account:""',
        ), 		
        array(
            'name' => 'customer_id',
            'value' => '$data->customer?$data->customer->first_name:""',
            'htmlOptions' => array('style' => 'text-align:left;width:350px;')
        ),   	
		array(
            'header' => 'Tên Phụ',
            'value' => '$data->customer?$data->customer->name_agent:""',
        ),    
            
        /* array(
            'header' => 'Tỉnh',
            'value' => '$data->customer?($data->customer->province?$data->customer->province->name:""):""',
        ),            
		 
        array(
            'header' => 'Kênh',
            'value' => '$data->customer?$data->customer->channel->name:""',
        ),            
        array(
            'header' => 'Khu Vực',
            'value' => '$data->customer?($data->customer->district?$data->customer->district->name:""):""',
        ),            
        array(
            'header' => 'Sale',
            'value' => '$data->customer?$data->customer->sale->first_name:""',
        ),   */          

		        array(
            'name' => 'date_sell',
            'type' => 'Date',
            'htmlOptions' => array('style' => 'text-align:right;width:50px;')
        ), 		
		        array(
            'name' => 'date_of_payment',
            'type' => 'Date',
            'htmlOptions' => array('style' => 'text-align:right;width:50px;')
        ), 
        array(
            'name' => 'quantity_50',
            'type' => 'Currency',
            'htmlOptions' => array('style' => 'text-align:right;width:20px;')
        ),            
            
        array(
            'name' => 'quantity_45',
            'type' => 'Currency',
            'htmlOptions' => array('style' => 'text-align:right;width:20px;')
        ),            
            
        array(
            'name' => 'quantity_12',
            'type' => 'Currency',
            'htmlOptions' => array('style' => 'text-align:right;width:20px;')
        ),            
        array(
            'name' => 'quantity_remain',
            'type' => 'Currency',
            'htmlOptions' => array('style' => 'text-align:right;width:20px;')
        ),       
        array(
            'name' => 'price_1_kg',
            'type' => 'Currency',
            'htmlOptions' => array('style' => 'text-align:right;width:20px;')
        ),    
        array(
            'name' => 'price_b_12',
			'header' => 'Giá B12',
            'type' => 'Currency',
            'htmlOptions' => array('style' => 'text-align:right;width:20px;')
        ),		
        array(
            'name' => 'total',
            'type' => 'Currency',
            'htmlOptions' => array('style' => 'text-align:right;width:20px;')
        ),             
            
		array(
			'header' => 'Action',
			'class'=>'CButtonColumn',
                        'template'=> ControllerActionsName::createIndexButtonRoles($actions),
		),
	),
)); ?>
