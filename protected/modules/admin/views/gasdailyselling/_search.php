<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'customer_id',array()); ?>
                <?php echo $form->hiddenField($model,'customer_id'); ?>
                <?php 
                        $this->widget('zii.widgets.jui.CJuiAutoComplete', array(
                                'attribute'=>'autocomplete_name',
                                'model'=>$model,
                                'sourceUrl'=>Yii::app()->createAbsoluteUrl('admin/gascustomer/autocompleteCustomer'),
                //                                'name'=>'my_input_name',
                                'options'=>array(
                                        'minLength'=>MIN_LENGTH_AUTOCOMPLETE,
                                        'multiple'=> true,
                                    'select'=>"js:function(event, ui) {
                                            $('#GasDailySelling_customer_id').val(ui.item.id);
                                            var remove_div = '<span class=\'remove_row_item\' onclick=\'fnRemoveName(this)\'></span>';
                                            $('#GasDailySelling_autocomplete_name').parent('div').find('.remove_row_item').remove();
                                            $('#GasDailySelling_autocomplete_name').attr('readonly',true).after(remove_div);
                                            fnBuildTableInfo(ui.item);
                                            $('.autocomplete_customer_info').show();
                                    }",
                                ),
                                'htmlOptions'=>array(
                                    'size'=>45,
                                    'maxlength'=>45,
                                    'style'=>'float:left;',
                                    'placeholder'=>'Nhập tên hoặc mã kế toán, kinh doanh',
                                ),
                        )); 
                        ?>        
                        <script>
                            function fnRemoveName(this_){
                                $(this_).prev().attr("readonly",false); 
                                $("#GasDailySelling_autocomplete_name").val("");
                                $("#GasDailySelling_customer_id").val("");
                                $('.autocomplete_customer_info').hide();
                            }
                        function fnBuildTableInfo(item){
                            $(".info_name").text(item.value);
                            $(".info_name_agent").text(item.name_agent);
                            $(".info_code_account").text(item.code_account);
                            $(".info_code_bussiness").text(item.code_bussiness);
                            $(".info_address").text(item.address);
                        }
                            
                        </script>        
                        <div class="clr"></div>				
	</div>

        <div class="row">
		<?php echo Yii::t('translation', $form->label($model,'date_sell')); ?>
		<?php // echo $form->textField($model,'date_sell'); ?>
                <?php 
                    $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                        'model'=>$model,        
                        'attribute'=>'date_sell',
                        'options'=>array(
                            'showAnim'=>'fold',
                            'dateFormat'=> ActiveRecord::getDateFormatJquery(),
//                            'minDate'=> '0',
                            'changeMonth' => true,
                            'changeYear' => true,
                            'showOn' => 'button',
                            'buttonImage'=> Yii::app()->theme->baseUrl.'/admin/images/icon_calendar_r.gif',
                            'buttonImageOnly'=> true,                                
                        ),        
                        'htmlOptions'=>array(
                            'class'=>'w-16',
                            'style'=>'height:20px;',
//                                'readonly'=>'readonly',
                        ),
                    ));
                ?>            
		<?php echo $form->error($model,'date_sell'); ?>
	</div>

	<div class="row">
		<?php echo Yii::t('translation', $form->label($model,'date_of_payment')); ?>
                <?php 
                    $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                        'model'=>$model,        
                        'attribute'=>'date_of_payment',
                        'options'=>array(
                            'showAnim'=>'fold',
                            'dateFormat'=> ActiveRecord::getDateFormatJquery(),
//                            'minDate'=> '0',
                            'changeMonth' => true,
                            'changeYear' => true,
                            'showOn' => 'button',
                            'buttonImage'=> Yii::app()->theme->baseUrl.'/admin/images/icon_calendar_r.gif',
                            'buttonImageOnly'=> true,                                
                        ),        
                        'htmlOptions'=>array(
                            'class'=>'w-16',
                            'style'=>'height:20px;',
//                                'readonly'=>'readonly',
                        ),
                    ));
                ?>  		
		<?php echo $form->error($model,'date_of_payment'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'quantity_50',array()); ?>
		<?php echo $form->textField($model,'quantity_50',array('size'=>8,'maxlength'=>8)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'quantity_45',array()); ?>
		<?php echo $form->textField($model,'quantity_45',array('size'=>8,'maxlength'=>8)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'quantity_12',array()); ?>
		<?php echo $form->textField($model,'quantity_12',array('size'=>8,'maxlength'=>8)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'quantity_remain',array()); ?>
		<?php echo $form->textField($model,'quantity_remain',array('size'=>8,'maxlength'=>8)); ?>
	</div>

	<div class="row buttons" style="padding-left: 159px;">
		        <?php $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType'=>'submit',
            'label'=>Yii::t('translation','Search'),
            'type'=>'null', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
            'size'=>'small', // null, 'large', 'small' or 'mini'
            //'htmlOptions' => array('style' => 'margin-bottom: 10px; float: right;'),
        )); ?>	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->