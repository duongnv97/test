<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'gas-daily-selling-form',
	'enableAjaxValidation'=>false,
)); ?>

	<?php echo Yii::t('translation', '<p class="note">Fields with <span class="required">*</span> are required.</p>'); ?>
	
	<div class="row">
		<?php echo Yii::t('translation', $form->labelEx($model,'customer_id')); ?>
                <?php echo $form->hiddenField($model,'customer_id'); ?>
                <?php 
                        $this->widget('zii.widgets.jui.CJuiAutoComplete', array(
                                'attribute'=>'autocomplete_name',
                                'model'=>$model,
                                'sourceUrl'=>Yii::app()->createAbsoluteUrl('admin/gascustomer/autocompleteCustomer'),
                //                                'name'=>'my_input_name',
                                'options'=>array(
                                        'minLength'=>MIN_LENGTH_AUTOCOMPLETE,
                                        'multiple'=> true,
                                    'select'=>"js:function(event, ui) {
                                            $('#GasDailySelling_customer_id').val(ui.item.id);
                                            var remove_div = '<span class=\'remove_row_item\' onclick=\'fnRemoveName(this)\'></span>';
                                            $('#GasDailySelling_autocomplete_name').parent('div').find('.remove_row_item').remove();
                                            $('#GasDailySelling_autocomplete_name').attr('readonly',true).after(remove_div);
                                            fnBuildTableInfo(ui.item);
                                            $('.autocomplete_customer_info').show();
                                    }",
                                ),
                                'htmlOptions'=>array(
                                    'size'=>45,
                                    'maxlength'=>45,
                                    'style'=>'float:left;',
                                    'placeholder'=>'Nhập tên hoặc mã kế toán, kinh doanh',
                                ),
                        )); 
                        ?>        
                        <script>
                            function fnRemoveName(this_){
                                $(this_).prev().attr("readonly",false); 
                                $("#GasDailySelling_autocomplete_name").val("");
                                $("#GasDailySelling_customer_id").val("");
                                $('.autocomplete_customer_info').hide();
                            }
                        function fnBuildTableInfo(item){
                            $(".info_name").text(item.value);
                            $(".info_name_agent").text(item.name_agent);
                            $(".info_code_account").text(item.code_account);
                            $(".info_code_bussiness").text(item.code_bussiness);
                            $(".info_address").text(item.address);
                            $(".info_name_sale").text(item.sale);
                        }
                            
                        </script>        
                        <div class="clr"></div>		
		<?php echo $form->error($model,'customer_id'); ?>
                <?php $display='display:inline;';
                    $info_name ='';
                    $info_name_agent ='';
                    $info_address ='';
                    $info_code_account ='';
                    $info_code_bussiness ='';
                    $info_name_sale ='';
                        if(empty($model->customer_id)) $display='display: none;';
                        else{
                            $info_name = $model->customer->first_name;
                            $info_name_agent = $model->customer->name_agent;
                            $info_code_account = $model->customer->code_account;
                            $info_code_bussiness = $model->customer->code_bussiness;
                            $info_address = $model->customer->address;
                            $info_name_sale = $model->customer->sale?$model->customer->sale->first_name:'';
                        }
                ?>                        
                <div class="autocomplete_customer_info" style="<?php echo $display;?>">
                <table>
                    <tr>
                        <td class="_l">Mã kế toán:</td>
                        <td class="_r info_code_account"><?php echo $info_code_account;?></td>
                    </tr>
					<!--
                    <tr>
                        <td class="_l">mã kinh doanh:</td>
                        <td class="_r info_code_bussiness"><?php echo $info_code_bussiness;?></td>
                    </tr>
					-->
                    <tr>
                        <td class="_l">Nhân viên sale:</td>
                        <td class="_r info_name_sale"><?php echo $info_name_sale;?></td>
                    </tr>
                    <tr>
                        <td class="_l">Tên khách hàng:</td>
                        <td class="_r info_name"><?php echo $info_name;?></td>
                    </tr>
                    <tr>
                        <td class="_l">Tên phụ:</td>
                        <td class="_r info_name_agent"><?php echo $info_name_agent;?></td>
                    </tr>
                    
                    <tr>
                        <td class="_l">Địa chỉ:</td>
                        <td class="_r info_address"><?php echo $info_address;?></td>
                    </tr>
                </table>
            </div>
            <div class="clr"></div>    		
            <?php echo $form->error($model,'customer_id'); ?>
	</div>

	<div class="row">
		<?php echo Yii::t('translation', $form->labelEx($model,'date_sell')); ?>
		<?php // echo $form->textField($model,'date_sell'); ?>
                <?php 
                    $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                        'model'=>$model,        
                        'attribute'=>'date_sell',
                        'options'=>array(
                            'showAnim'=>'fold',
                            'dateFormat'=> ActiveRecord::getDateFormatJquery(),
//                            'minDate'=> '0',
                            'changeMonth' => true,
                            'changeYear' => true,
                            'showOn' => 'button',
                            'buttonImage'=> Yii::app()->theme->baseUrl.'/admin/images/icon_calendar_r.gif',
                            'buttonImageOnly'=> true,                                
                        ),        
                        'htmlOptions'=>array(
                            'class'=>'w-16',
                            'style'=>'height:20px;',
//                                'readonly'=>'readonly',
                        ),
                    ));
                ?>            
		<?php echo $form->error($model,'date_sell'); ?>
	</div>

	<div class="row" style="display:none;">
		<?php echo Yii::t('translation', $form->labelEx($model,'date_of_payment')); ?>
                <?php 
                    $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                        'model'=>$model,        
                        'attribute'=>'date_of_payment',
                        'options'=>array(
                            'showAnim'=>'fold',
                            'dateFormat'=> ActiveRecord::getDateFormatJquery(),
//                            'minDate'=> '0',
                            'changeMonth' => true,
                            'changeYear' => true,
                            'showOn' => 'button',
                            'buttonImage'=> Yii::app()->theme->baseUrl.'/admin/images/icon_calendar_r.gif',
                            'buttonImageOnly'=> true,                                
                        ),        
                        'htmlOptions'=>array(
                            'class'=>'w-16',
                            'style'=>'height:20px;',
//                                'readonly'=>'readonly',
                        ),
                    ));
                ?>  		
		<?php echo $form->error($model,'date_of_payment'); ?>
	</div>

	<style>
		.two-col {float:left;width:100%; border-bottom: 1px solid #CDCDCD;}
		.two-col .col-1 {float:left; width:350px;border-right: 1px solid #CDCDCD;margin-right: 10px;}
		.two-col .col-2 {float:left; width:auto;}
	</style>
	
	<div class='two-col'>
		<div class='col-1'>
			<div class="row">
				<?php echo Yii::t('translation', $form->labelEx($model,'quantity_50')); ?><div class="fix_number_help">
				<?php echo $form->textField($model,'quantity_50',array('size'=>8,'maxlength'=>8,'class'=>'number_only')); ?>
				<div class="help_number"></div>
						</div> <?php echo $form->error($model,'quantity_50'); ?>
			</div>
			<div class="clr"></div>
			<div class="row">
				<?php echo Yii::t('translation', $form->labelEx($model,'quantity_45')); ?><div class="fix_number_help">
				<?php echo $form->textField($model,'quantity_45',array('size'=>8,'maxlength'=>8,'class'=>'number_only')); ?>
				<div class="help_number"></div>
						</div> <?php echo $form->error($model,'quantity_45'); ?>
			</div>
			<div class="clr"></div>
			<div class="row">
				<?php echo Yii::t('translation', $form->labelEx($model,'quantity_12')); ?><div class="fix_number_help">
				<?php echo $form->textField($model,'quantity_12',array('size'=>8,'maxlength'=>8,'class'=>'number_only')); ?>
				<div class="help_number"></div>
						</div> <?php echo $form->error($model,'quantity_12'); ?>
			</div>
			<div class="clr"></div>		
		</div><!-- end <div class='col-1'> -->
		
		<div class='col-2'>
			<div class="row">
				<?php echo Yii::t('translation', $form->labelEx($model,'vo_back_50')); ?><div class="fix_number_help">
				<?php echo $form->textField($model,'vo_back_50',array('size'=>8,'maxlength'=>8,'class'=>'number_only')); ?>
				<div class="help_number"></div>
				</div> <?php echo $form->error($model,'vo_back_50'); ?>
			</div>
			<div class="clr"></div>
			<div class="row">
				<?php echo Yii::t('translation', $form->labelEx($model,'vo_back_45')); ?><div class="fix_number_help">
				<?php echo $form->textField($model,'vo_back_45',array('size'=>8,'maxlength'=>8,'class'=>'number_only')); ?>
				<div class="help_number"></div>
                                </div> <?php echo $form->error($model,'vo_back_45'); ?>
			</div>
			<div class="clr"></div>
			<div class="row">
				<?php echo Yii::t('translation', $form->labelEx($model,'vo_back_12')); ?><div class="fix_number_help">
				<?php echo $form->textField($model,'vo_back_12',array('size'=>8,'maxlength'=>8,'class'=>'number_only')); ?>
				<div class="help_number"></div>
                                </div> <?php echo $form->error($model,'vo_back_12'); ?>
			</div>
			<div class="clr"></div>		
		</div><!-- end <div class='col-2'> -->
	</div> <!-- end <div class='two-col'> -->
	
	<div class="clr"></div>	
        
        <div class='two-col'>
            <div class='col-1'>
                <div class="row">
                        <?php echo Yii::t('translation', $form->labelEx($model,'price_1_kg')); ?>
                        <div class="fix_number_help">
                            <?php echo $form->textField($model,'price_1_kg',array('size'=>20,'maxlength'=>14,'class'=>'number_only')); ?>
                        <div class="help_number"></div>
                        </div> <?php echo $form->error($model,'price_1_kg'); ?>
                </div>
                <div class="clr"></div>
                <div class="row">
                        <?php echo Yii::t('translation', $form->labelEx($model,'price_b_12')); ?>
                    <div class="fix_number_help">
                        <?php echo $form->textField($model,'price_b_12',array('size'=>20,'maxlength'=>14,'class'=>'number_only')); ?>
                        <div class="help_number"></div>
                        </div> <?php echo $form->error($model,'price_b_12'); ?>
                </div>	                
            </div>        
            <div class='col-2'>
                <div class="row">
                        <?php echo Yii::t('translation', $form->labelEx($model,'quantity_remain')); ?>
                    <div class="fix_number_help">
                        <?php echo $form->textField($model,'quantity_remain',array('size'=>8,'maxlength'=>8,'class'=>'number_only')); ?>
                    <div class="help_number"></div>
                        </div> 
                        <?php echo $form->error($model,'quantity_remain'); ?>
                </div>
                <div class="clr"></div>
                <div class="row">
                    <?php echo $form->labelEx($model,'agent_id'); ?>
                    <?php echo $form->dropDownList($model,'agent_id', Users::getArrUserByRole(ROLE_AGENT),array('style'=>'width:250px;','empty'=>'Select')); ?>
                    <?php echo $form->error($model,'agent_id'); ?>
                </div>
            </div>        
        </div>        
        
        <div class="clr"></div>	
	<div class="row">
		<?php echo Yii::t('translation', $form->labelEx($model,'total')); ?>
                <div class="fix_number_help">
		<?php echo $form->textField($model,'total',array('size'=>20,'maxlength'=>14,'class'=>'number_only')); ?>
		<div class="help_number"></div>
                </div> <?php echo $form->error($model,'total'); ?>
	</div>
<div class="clr"></div>
	<div class="row buttons " style="padding-left: 141px;">
		        <?php $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType'=>'submit',
            'label'=>$model->isNewRecord ? Yii::t('translation', 'Create') : Yii::t('translation', 'Save'),
            'type'=>'null', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
            'size'=>'small', // null, 'large', 'small' or 'mini'
            //'htmlOptions' => array('style' => 'margin-bottom: 10px; float: right;'),
        )); ?>	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->