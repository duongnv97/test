<?php
$this->breadcrumbs=array(
	Yii::t('translation','Bán Hàng')=>array('index'),
	Yii::t('translation','Thêm Mới'),
);

$menus = array(		
        array('label'=> Yii::t('translation', 'Bán Hàng') , 'url'=>array('index')),
);
$this->menu= ControllerActionsName::createMenusRoles($menus, $actions);
?>

<h1><?php echo Yii::t('translation', 'Thêm Mới Bán Hàng'); ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>