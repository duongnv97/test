<?php
$this->breadcrumbs=array(
	Yii::t('translation','Bán Hàng')=>array('index'),
	($model->customer?$model->customer->first_name:'')=>array('view','id'=>$model->id),
	Yii::t('translation','Cập Nhật'),
);

$menus = array(	
        array('label'=> Yii::t('translation', 'Bán Hàng'), 'url'=>array('index')),
	array('label'=> Yii::t('translation', 'Xem Bán Hàng'), 'url'=>array('view', 'id'=>$model->id)),
	array('label'=> Yii::t('translation', 'Thêm Bán Hàng'), 'url'=>array('create')),	
);
$this->menu= ControllerActionsName::createMenusRoles($menus, $actions);

?>

<h1><?php echo Yii::t('translation', 'Cập Nhật Bán Hàng: '.($model->customer?$model->customer->first_name:'')); ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>