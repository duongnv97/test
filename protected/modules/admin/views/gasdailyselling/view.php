<?php
$this->breadcrumbs=array(
	'Bán Hàng'=>array('index'),
	$model->customer?$model->customer->first_name:'',
);

$menus = array(
	array('label'=>'Bán Hàng', 'url'=>array('index')),
	array('label'=>'Thêm Bán Hàng', 'url'=>array('create')),
	array('label'=>'Cập Nhật Bán Hàng', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Xóa Bán Hàng', 'url'=>array('delete'), 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
);
$this->menu= ControllerActionsName::createMenusRoles($menus, $actions);
?>

<h1>Xem Bán Hàng: <?php echo $model->customer?$model->customer->first_name:''; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
            array(
                'name'=>'customer_id',
                'value'=>$model->customer?$model->customer->first_name:'',
            ),            
		'date_sell:Date',
                'date_of_payment:Date',
		'quantity_50:Currency',
		'quantity_45:Currency',
		'quantity_12:Currency',
		'quantity_remain:Currency',
		'price_1_kg:Currency',
		'price_b_12:Currency',
		'total:Currency',
		'vo_back_50',
		'vo_back_45',
		'vo_back_12',
        array(
            'name'=>'agent_id',
            'value' => $model->agent?$model->agent->first_name:'',
        ),      
        array(
            'label' => 'Tên Đại Lý',
            'value' => $model->customer?$model->customer->name_agent:"",
        ),      
            
        array(
            'label' => 'Tỉnh',
            'value' => $model->customer?$model->customer->province->name:"",
        ),            
		 
        array(
            'label' => 'Kênh',
            'value' => $model->customer?$model->customer->channel->name:"",
        ),            
        array(
            'label' => 'Khu Vực',
            'value' => $model->customer?($model->customer->district?$model->customer->district->name:""):"",
        ),            
        array(
            'label' => 'Sale',
            'value' => $model->customer?$model->customer->sale->first_name:"",
        ),            
		'created_date:Datetime',
	),
)); ?>
