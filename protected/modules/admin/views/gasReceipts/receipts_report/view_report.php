<h1>Xem thông tin <?php echo $this->pluralTitle?> : <?php echo ($model->rUsers->getFullName());?></h1>
<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
        array(
            'label' => 'Tên dãy số',
            'name'  => 'name',                       
            'value' => $model->getNameView(),
        ),
        array(
            'label' => 'Tên NVPVKH hoặc xe tải',
            'type'  => 'html',
            'value' => $model->getObjectName(),
        ),
        array(
            'label' => 'Tỉnh',
            'value' => $model->getProvince()
        ),
        array(
            'label' => 'Đại lý',
            'value' => $model->getAgent()
        ),
        array(
            'label' => 'Dãy số chứng từ',
            'value' =>  $model->getNumber()
        ),
        array(
            'label' => 'Dãy số đã sử dụng',
            'value' => $model->getStrNumberUsed(),
        ),
        array(
            'label' => 'Dãy số chưa sử dụng',
            'value' => $model->getStrNumberNotUsed()
        )
            
    ),
  )); 

?>
