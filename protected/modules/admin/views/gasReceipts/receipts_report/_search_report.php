<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>GasCheck::getCurl(),//Yii::app()->createUrl($this->route), //clean URL 
	'method'=>'get',
)); ?>
<!--    <div class="row">
        <?php //echo $form->labelEx($model, 'Tên chứng từ') ?>
        <?php// echo $form->textField($model, 'name', array('class'=> 'w-400')) ?>
    </div>-->
    <div class="row">
        <?php echo $form->labelEx($model,'object_id'); ?>
        <?php echo $form->hiddenField($model,'object_id', array('class'=>'')); ?>
        <?php
                // 1. limit search kh của sale
                $url = Yii::app()->createAbsoluteUrl('admin/ajax/search_by_role', array(
                            'role'      => implode(',', [ROLE_CAR, ROLE_EMPLOYEE_MAINTAIN]),
//                            'PaySalary' => UsersProfile::SALARY_YES
                        )
                    );
                // widget auto complete search user customer and supplier
                $aData = array(
                    'model'=>$model,
                    'field_customer_id'=>'object_id',
                    'url'=> $url,
                    'name_relation_user'=>'rUsers',
                    'ClassAdd' => 'w-400',
                    'field_autocomplete_name' => 'autocomplete_name',
                    'placeholder'=>'Nhập mã NV hoặc tên',
                );
                $this->widget('ext.SpaAutocompleteCustomer.SpaAutocompleteCustomer',
                    array('data'=>$aData));
                ?>
        </div>
    <div class="row">
        <?php echo $form->label($model, 'status') ?>
        <?php echo $form->dropdownList($model, 'status', GasReceipts::getArrayStatus(),array('empty'=>'Select', 'class' => 'w-400')) ?>
    </div>
    <div class="row">
        <?php echo $form->labelEx($model,'agent_id'); ?>
        <?php echo $form->hiddenField($model,'agent_id', array('class'=>'')); ?>
        <?php
                // 1. limit search kh của sale
                $url = Yii::app()->createAbsoluteUrl('admin/ajax/search_by_role', array('role'=> ROLE_AGENT));
                // widget auto complete search user customer and supplier
                $aData = array(
                    'model'=>$model,
                    'field_customer_id'=>'agent_id',
                    'url'=> $url,
                    'name_relation_user'=>'rUsers',
                    'ClassAdd' => 'w-400',
                    'field_autocomplete_name' => 'autocomplete_name_1',
                    'placeholder'=>'Nhập mã hoặc tên đại lý',
                );
                $this->widget('ext.SpaAutocompleteCustomer.SpaAutocompleteCustomer',
                    array('data'=>$aData));
                ?>
    </div>
    <div class="row">
            <?php echo $form->labelEx($model,'province_id'); ?>
            <div class="fix-label">
                <?php
                   $this->widget('ext.multiselect.JMultiSelect',array(
                         'model'=>$model,
                         'attribute'=>'province_id',
                         'data'=> GasProvince::getArrAll(),
                         // additional javascript options for the MultiSelect plugin
                        'options'=>array('selectedList' => 30,),
                         // additional style
                         'htmlOptions'=>array('style' => 'width: 400px;'),
                   ));    
               ?>
            </div>
        </div>
    <br>
<!--        <div class='row more_col'>
            <div class="col1">
                <?php //echo $form->label($model, 'Dãy số từ'); ?>
                <?php //echo $form->textField($model, 'number_from', ['class' => 'number_only_v1 ', 'maxlength' => 20,]); ?>
            </div>
            <div class="col2">
                <?php //echo $form->label($model, 'Đến'); ?>
                <?php //echo $form->textField($model, 'number_to', ['class' => 'number_only_v1', 'maxlength' => 20]); ?>
            </div>
        </div>-->
        <br/>
        <div style='clear: both' class='row more_col'>
            <div class="col1">
                <?php echo $form->label($model,'Ngày tạo từ'); ?>
                <?php 
                    $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                        'model'=>$model,        
                        'attribute'=>'date_from',
                        'options'=>array(
                            'showAnim'=>'fold',
                            'dateFormat'=> MyFormat::$dateFormatSearch,
//                            'minDate'=> '0',
                            'maxDate'=> '0',
                            'changeMonth' => true,
                            'changeYear' => true,
                            'showOn' => 'button',
                            'buttonImage'=> Yii::app()->theme->baseUrl.'/admin/images/icon_calendar_r.gif',
                            'buttonImageOnly'=> true,                                
                        ),        
                        'htmlOptions'=>array(
                            'class'=>'w-16 date_from',
                            'size'=>'16',
                            'style'=>'float:left;',                               
                        ),
                    ));
                ?>     		
            </div>
            <div class="col2">
                    <?php echo $form->label($model,'Đến'); ?>
                    <?php 
                        $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                            'model'=>$model,        
                            'attribute'=>'date_to',
                            'options'=>array(
                                'showAnim'=>'fold',
                                'dateFormat'=> MyFormat::$dateFormatSearch,
    //                            'minDate'=> '0',
                                'maxDate'=> '0',
                                'changeMonth' => true,
                                'changeYear' => true,
                                'showOn' => 'button',
                                'buttonImage'=> Yii::app()->theme->baseUrl.'/admin/images/icon_calendar_r.gif',
                                'buttonImageOnly'=> true,                                
                            ),        
                            'htmlOptions'=>array(
                                'class'=>'w-16 DateTo',
                                'size'=>'16',
                                'style'=>'float:left;',
                            ),
                        ));
                    ?>     		
            </div>
        </div>
        
        <div class='row'>
            <?php $aPageSize = GasConst::getPageSize();  ?>
            <?php echo $form->label($model, 'pageSize',['label'=>'Số dòng hiển thị']); ?>
            <?php echo $form->dropdownList($model, 'pageSize', $aPageSize, ['class' => 'w-200', 'empty'=>'Select']); ?>
        </div>
        
        <div class='row'>
            <br/>
        </div>
	<div class="row buttons" style="padding-left: 159px;">
            <?php $this->widget('bootstrap.widgets.TbButton', array(
                'buttonType'=>'submit',
                'label'=>'Search',
                'type'=>'null', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
                'size'=>'small', // null, 'large', 'small' or 'mini'
                //'htmlOptions' => array('style' => 'margin-bottom: 10px; float: right;'),
            )); ?>
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a class='btn_cancel btnReset'>Reset</a>
        </div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->

<script>
    $(function(){
        $('.date_from').change(function(){
            var div = $(this).closest('.row');
            div.find('.DateTo').val($(this).val());
        });
        $('.btnReset').click(function(){
            var div = $(this).closest('form');
            div.find('input').val('');
            div.find('Select').val('');
            div.find('.remove_row_item').trigger('click');
        });
    });
</script>