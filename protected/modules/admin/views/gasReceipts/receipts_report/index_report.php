<?php
$this->breadcrumbs=array(
	
);
// register script and css, add libery for view   

Yii::app()->clientScript->registerScript('search', "  
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});

$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('search-grid', {
                url : $(this).attr('action'),
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Báo cáo <?php echo $this->pluralTitle; ?></h1>

<?php echo CHtml::link('Tìm Kiếm Nâng Cao', '#', array('class' => 'search-button')); ?>
&nbsp;&nbsp;&nbsp;&nbsp;
<div class="search-form">
    <?php
    $this->renderPartial('receipts_report/_search_report', array(
        'model' => $model,
    ));
    ?>
</div><!-- search-form -->

    

    
<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'search-grid',
	'dataProvider'=>$model->search(),
        'afterAjaxUpdate'=>'function(id, data){ fnUpdateColorbox();}',
        'template'=>'{pager}{summary}{items}{pager}{summary}',
//    'itemsCssClass' => 'items custom_here',
//    'htmlOptions'=> array('class'=>'grid-view custom_here'),
        'pager' => array(
            'maxButtonCount'=>  CmsFormatter::$PAGE_MAX_BUTTON,
        ),
	'enableSorting' => false,
	//'filter'=>$model,
    'columns'        => array(
            array(
                'header' => 'S/N',
                'type' => 'raw',
                'value' => '$this->grid->dataProvider->pagination->currentPage * $this->grid->dataProvider->pagination->pageSize + ($row+1)',
                'headerHtmlOptions' => array('width' => '10px','style' => 'text-align:center;'),
                'htmlOptions' => array('style' => 'text-align:center;')
            ),
            array(
                'name'      => 'name' ,
                'type'      => 'html',
                'value'     => '$data->getNameView()'
            ),
            array(
                'name'      => 'object_id',
                'value'     => '$data->getObjectName()'
            ),
            array(
                'name'      => 'province_id',
                'value'     => '$data->getProvince()'  
            ),
            array(
                'name'      => 'agent_id',
                'value'     => '$data->getAgent()'
            ),
            array(
                'name'      => 'number',
                'value'     => '$data->getNumber()'
            ),
            array(
                'name'      => 'Số đã sử dụng',
                'value'     => '$data->getStrNumberUsed()'
            ),
            array(
                'name'      => 'Số chưa sử dụng',
                'value'     => '$data->getStrNumberNotUsed()'
            ),
            array(
                'name'      => 'status',
                'type'      => 'html',
                'value'     => '$data->getStatus()'
            ),
            array(
                'name'      => 'Ngày tạo',
                'value'     => '$data->getCreatedDate()'
            )
//            array(
//                'header'    => 'actions',
//                'class'     => 'CButtonColumn',
//                'template'  => '{view}',
//                'buttons'=>[
//                    'view' => array
//                    (
//                        'label'=>'Xem thông tin ',     //Text label of the button.
//                        'url'=>'["ViewReport", "id"=>$data->id]',  
//                        'options'=>array('class' => 'report'), //HTML options for the button tag..
//                    )
//                ]
//            )
    )
));
?>

<script>
$(document).ready(function() {
    fnUpdateColorbox();
});

function fnUpdateColorbox(){  
     fixTargetBlank();
		$(".report").colorbox({iframe:true,height:'350' ,innerHeight:'500', innerWidth: '1050',close: "<span title='close'>close</span>"});
	}
</script>
