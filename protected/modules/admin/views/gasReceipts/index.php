<?php
$this->breadcrumbs=array(
	
);

$menus=array(
            array('label'=>"Tạo mới", 'url'=>array('create')),
);
$this->menu= ControllerActionsName::createMenusRoles($menus, $actions); //create url-buttons  

// register script and css, add libery for view   

Yii::app()->clientScript->registerScript('search', "  
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});

$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('search-grid', {
                url : $(this).attr('action'),
		data: $(this).serialize()
	});
	return false;
});
");
?>



<h1>Quản lý <?php echo $this->pluralTitle; ?></h1>

<?php if (!isset($_GET['NhanOrder'])): ?>
    <?php echo CHtml::link('Tìm Kiếm Nâng Cao', '#', array('class' => 'search-button')); ?>
&nbsp;&nbsp;&nbsp;&nbsp;
    <?php //    echo CHtml::link('Test', Yii::app()->createAbsoluteUrl('admin/NhanOrder/test'), array('class' => '',  'target'=>'_blank')); ?>
    <div class="search-form" style="display: none" >
        <?php
        $this->renderPartial('_search', array(
            'model' => $model,
        ));
        ?>
    </div><!-- search-form -->
<?php endif; ?>

    

    
<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'search-grid',
	'dataProvider'=>$model->search(),
//        'afterAjaxUpdate'=>'function(id, data){ fnUpdateColorbox();}',
        'template'=>'{pager}{summary}{items}{pager}{summary}',
//    'itemsCssClass' => 'items custom_here',
//    'htmlOptions'=> array('class'=>'grid-view custom_here'),
        'pager' => array(
            'maxButtonCount'=>  CmsFormatter::$PAGE_MAX_BUTTON,
        ),
	'enableSorting' => false,
	//'filter'=>$model,
    'columns'        => array(
            array(
                'header' => 'S/N',
                'type' => 'raw',
                'value' => '$this->grid->dataProvider->pagination->currentPage * $this->grid->dataProvider->pagination->pageSize + ($row+1)',
                'headerHtmlOptions' => array('width' => '10px','style' => 'text-align:center;'),
                'htmlOptions' => array('style' => 'text-align:center;')
            ),
            array(
                'name'      => 'Set hết hạn' ,
                'type'      => 'html',
                'value'     => '$data->getName()'
            ),
            array(
                'name'      => 'object_id',
                'value'     => '$data->getObjectName()'
            ),
            array(
                'name'      => 'number',
                'value'     => '$data->getNumber()'
            ),
            array(
                'name'      => 'province_id',
                'value'     => '$data->getProvince()'  
            ),
            array(
                'name'      => 'agent_id',
                'value'     => '$data->getAgent()'
            ),
            array(
                'name'      => 'status',
                'type'      => 'html',  
                'value'     => '$data->getStatus()'
            ),
            array(
                'name'      => 'Ngày tạo',
                'value'     => '$data->getCreatedDate()'
            ),
            array(
                'name'      => 'Người tạo',
                'value'     => '$data->getCreatedBy()',
            ),
            array(
                'header'    => 'actions',
                'class'     => 'CButtonColumn',
                'template'=> ControllerActionsName::createIndexButtonRoles($actions),
                'buttons'=>[
                    'update' => array
                    (
                        'visible' => '$data->canUpdate()',
                    ),   
                ]
            )
    )
));

?>
    
<script>
    $(document).ready(function() {
        fnSetExpired();
    });
    function fnSetExpired(){
        $(document).on('click', '.setExpired', function(){
            if(confirm('Bạn muốn chuyển dãy số chứng từ của NV hoặc xe này thành hết hạn ?')) {
                $.fn.yiiGridView.update('search-grid', { 
                    type:'POST',
                    url:$(this).attr('href'),
                    success:function(data) {
                          $.fn.yiiGridView.update('search-grid');
                    }
                });
            }
            return false;
        });
    }
</script>