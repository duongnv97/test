<?php
$this->breadcrumbs=array(
	'Quản Lý Dãy số chứng từ'=>array('index'),
	'Tạo Mới',
);

$menus = array(		
        array('label'=>'Quản lý chứng từ', 'url'=>array('index')),
);
$this->menu= ControllerActionsName::createMenusRoles($menus, $actions);
?>

<h1><?php echo Yii::t('translation', 'Tạo Mới ' . $this->pluralTitle); ?></h1>
<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>