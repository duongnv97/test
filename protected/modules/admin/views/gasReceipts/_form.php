<?php 
//    if(isset($_GET['type'])){
//        $user_name = ($_GET['type'] == GasReceipts::TYPE_FOR_CAR) ? 'Tên xe' : 'Tên nhân viên';
//    }else{
//        $user_name = 'Tên nhân viên';
//    }
    $user_name = ($model->create_for == GasReceipts::TYPE_FOR_CAR) ? 'Tên xe' : 'Tên nhân viên';
    $placeHolder = ($model->create_for == GasReceipts::TYPE_FOR_CAR) ? 'Nhập mã hoặc tên xe' : 'Nhập mã hoặc tên nhân viên';
?>
<div class="form">
    <?php $form = $this->beginWidget('CActiveForm', array(
        'id' => 'gasreceipts',
        'enableAjaxValidation'=>false,
        'htmlOptions' => array('enctype' => 'multipart/form-data'),
    )); ?>

<div>
    <p class="note">Dòng có dấu <span class="required">*</span> là bắt buộc.</p>
    <?php echo MyFormat::BindNotifyMsg();?>

    <div class="row">
        <?php echo $form->label($model, 'Tạo cho') ?>
        <?php echo $form->radioButtonList($model, 'create_for', GasReceipts::getArrayCreateFor(), 
                array(
                        'separator'=>"",
                        'template'=>'<li style="font-size: 15px;list-style:none;float:left; margin-right: 20px;">{input}{label}</li>',
                        'container'=>"ul",
                        'class'=>'RadioCreateFor' 
                    )) ?>
        <div class="clearfix"></div>
    </div>
    <br>
<!--    <div class="row">
        <?php // echo $form->label($model, 'Tên dãy số') ?>
        <?php // echo $form->textField($model, 'name', ['class'=>'w-300']) ?>
        <?php // echo $form->error($model,'name'); ?>
    </div>-->
    <div class="row">
        <?php echo $form->labelEx($model,$user_name); ?>
        <?php echo $form->hiddenField($model,'object_id', array('class'=>'')); ?>
        <?php
        // .. tất cả các TH dc xử lý trên action ajax roi
        // 1. TH giám sát PTTT search nhân viên trong đội của giám sát đó
        // 2. user đại lý search nhân viên kế toán + nhân viên giao hàng của đại lý đó
        // 3. không search, là những loại user còn lại của hệ thống
        $params = ($model->create_for == GasReceipts::TYPE_FOR_EMPLOYEE) 
                ? ['role' => ROLE_EMPLOYEE_MAINTAIN, 'PaySalary' => UsersProfile::SALARY_YES]
                : ['role' => ROLE_CAR];
        $url = Yii::app()->createAbsoluteUrl('admin/ajax/search_by_role', $params);
        // widget auto complete search user customer and supplier
        $aData = array(
            'model'             =>$model,
            'field_customer_id' =>'object_id',
            'url'               => $url,
            'name_relation_user'=>'rUsers',
            'show_role_name'    => 1,
            'ClassAdd'          => 'w-300',
//            'placeholder'       => 'Nhập tên hoặc mã NVPVKH (Xe tải)'
            'placeholder'       => $placeHolder
        );
        $this->widget('ext.SpaAutocompleteCustomer.SpaAutocompleteCustomer',
            array('data'=>$aData));                                        
        ?>
        <?php echo $form->error($model,'object_id'); ?>
    </div>
    <div class="row">
        <?php echo $form->labelEx($model, 'number_from') ?>
        <?php echo $form->textField($model, 'number_from', ['class'=>'w-300 f_size_15 ad_fix_currency item_r']) ?>
        <?php echo $form->error($model,'number_from'); ?>

    </div>
    <div class="row">
        <?php echo $form->labelEx($model, 'number_to') ?>
        <?php echo $form->textField($model, 'number_to', ['class'=>'w-300 f_size_15 ad_fix_currency item_r']) ?>
        <?php echo $form->error($model,'number_to'); ?>

    </div>
    <div class="row">
        <?php echo $form->label($model, 'status') ?>
        <?php echo $form->dropdownList($model, 'status', GasReceipts::getArrayStatus(), ['class'=>'w-310']) ?>
    </div>
</div>
<div class="row buttons">
    <?php $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType'=>'submit',
            'label'=>$model->isNewRecord ? 'Create' :'Save',
            'type'=>'null', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
            'size'=>'small', // null, 'large', 'small' or 'mini'
            //'htmlOptions' => array('style' => 'margin-bottom: 10px; float: right;'),
        )); ?>	
</div>
<?php $this->endWidget(); ?>
</div>

<?php 
$urlReload = $model->isNewRecord ? Yii::app()->createAbsoluteUrl("/admin/GasReceipts/create") : Yii::app()->createAbsoluteUrl("/admin/GasReceipts/update",['id'=>$model->id]);
if($model->isNewRecord){
    
}
?>
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/autoNumeric-master/autoNumeric.js"></script>
<script>
    $(function(){
        bindRadioCreateFor();
        fnInitInputCurrency();
    });
    function bindRadioCreateFor(){
        $('#GasReceipts_create_for li').each(function(){
            if( $(this).find('input').is(':checked') ){
                $(this).css('background-color', 'rgba(76, 175, 80)');
            }
        });
        $('#GasReceipts_create_for li').on('click', function(){
            $('#GasReceipts_create_for li').css('background-color', 'rgba(0, 0, 0, 0)');
            var radio = $(this).find('input');
            if( radio.is(':checked') ){
                $(this).css('background-color', 'rgba(76, 175, 80)');
            }
            var url = '<?php echo $urlReload; ?>/type/'+radio.val();
            window.location.replace(url);
        });
    }
</script>