<?php
$this->breadcrumbs=array(
	'Quản Lý Dãy Số Chứng Từ'=>array('index'),
         $model->name=>array('view','id'=>$model->id),
	'Cập Nhật',
);

$menus = array(	
        array('label'=>'Quản Lý', 'url'=>array('index')),
	array('label'=>'Xem', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Tạo', 'url'=>array('create')),	
);
$this->menu= ControllerActionsName::createMenusRoles($menus, $actions);

?>


<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>