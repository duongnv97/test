<?php 
$this->breadcrumbs=array(
	'Quản Lý Dãy Số Chứng Từ'=>array('index'),
	$model->name,
);

$menus = array(
	array('label'=>'Quản lý', 'url'=>array('index')),
	array('label'=>'Tạo', 'url'=>array('create')),
	array('label'=>'Xóa', 'url'=>array('delete'), 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
);

 if ($model->canUpdate()){
    $menus[] =  array('label'=>'Cập Nhật', 'url'=>array('update', 'id'=>$model->id));
 }
 
 
$this->menu= ControllerActionsName::createMenusRoles($menus, $actions);
?>


<h1>Xem thông tin <?php echo $this->pluralTitle?> : <?php echo ($model->rUsers->getFullName());?></h1>
 <?php echo MyFormat::BindNotifyMsg();?>
<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
//        array(
//            'label' => 'Tên dãy số',
//            'name'  => 'name',                       
//            'value' => $model->getNameView(),
//        ),
        array(
            'label' => 'Tên NVPVKH hoặc xe tải',
            'type'  =>  'html',
            'value' => $model->getObjectName(),
        ),
        array(
            'label' => 'Dãy số chứng từ',
            'value' =>  $model->getNumber(),
        ),
        array(
            'label' => 'Tỉnh',
            'value' => $model->getProvince()
        ),
        array(
            'label' => 'Đại lý',
            'value' => $model->getAgent()
        ),
        array(
            'label' => 'Ngày tạo',
            'value' => $model->getCreatedDate()
        )
    ),
)); 