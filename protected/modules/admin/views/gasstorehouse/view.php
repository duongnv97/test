<?php
$this->breadcrumbs=array(
	'QL Chi Nhánh'=>array('index'),
	$model->name,
);

$menus = array(
	array('label'=>'GasStorehouse Management', 'url'=>array('index')),
	array('label'=>'Create GasStorehouse', 'url'=>array('create')),
	array('label'=>'Update GasStorehouse', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete GasStorehouse', 'url'=>array('delete'), 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
);
$this->menu= ControllerActionsName::createMenusRoles($menus, $actions);
?>

<h1>View #<?php echo $model->name; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
            array(
                'name'=>'province_id',
                'value'=>$model->province?$model->province->name:'',
            ),            
		'name',
		'short_name',
		'note',
		'status:status',
	),
)); ?>
