<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=> GasCheck::getCurl(),
	'method'=>'get',
)); ?>

        <div class="row">
            <?php echo $form->labelEx($model, "one_id",array('label'=>'Chuyên viên')); ?>
            <?php echo $form->hiddenField($model,'one_id', array('class'=>'')); ?>
            <?php
                // 1. limit search kh của sale
                $url = Yii::app()->createAbsoluteUrl('admin/ajax/search_user_login', ['PaySalary' => 1]);
                // widget auto complete search user customer and supplier
                $aData = array(
                    'model'=>$model,
                    'field_customer_id'=>'one_id',
                    'url'=> $url,
                    'name_relation_user'=>'one',
                    'ClassAdd' => 'w-300',
                    'field_autocomplete_name' => 'autocomplete_name', //khong hieu
                    'placeholder'=>'Nhập mã hoặc tên chuyên viên',
    //                'fnSelectCustomer'=>'fnSelectAgent', //?
                );
                $this->widget('ext.SpaAutocompleteCustomer.SpaAutocompleteCustomer',
                    array('data'=>$aData));
            ?>
            <?php echo $form->error($model,'one_id'); ?>
        </div>
        <div class="row more_col">
            <div class="col1">
                    <?php echo $form->label($model,'han_muc_vo_from',array('label'=>'Hạn mức vỏ từ')); ?>
                    <?php echo $form->textField($model,'han_muc_vo_from',array('size'=>11,'maxlength'=>11, 'class' => 'number_only_v1')); ?>
            </div>
            <div class="col2">
		<?php echo $form->label($model,'han_muc_vo_to',array('label'=>'đến')); ?>
		<?php echo $form->textField($model,'han_muc_vo_to',array('size'=>11,'maxlength'=>11, 'class' => 'number_only_v1')); ?>
            </div>
        </div>
    
	<div class="row buttons" style="padding-left: 159px;">
		        <?php $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType'=>'submit',
            'label'=>'Search',
            'type'=>'null', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
            'size'=>'small', // null, 'large', 'small' or 'mini'
            //'htmlOptions' => array('style' => 'margin-bottom: 10px; float: right;'),
        )); ?>	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->