<?php
$this->breadcrumbs=array(
	$this->singleTitle,
);

$menus=array(
            array('label'=>"Create $this->singleTitle", 'url'=>array('create')),
);
$this->menu= ControllerActionsName::createMenusRoles($menus, $actions);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('gas-one-many-grid', {
                url : $(this).attr('action'),
		data: $(this).serialize()
	});
	return false;
});
");

Yii::app()->clientScript->registerScript('ajaxupdate', "
$('#gas-one-many-grid a.ajaxupdate').live('click', function() {
    $.fn.yiiGridView.update('gas-one-many-grid', {
        type: 'POST',
        url: $(this).attr('href'),
        success: function() {
            $.fn.yiiGridView.update('gas-one-many-grid');
        }
    });
    return false;
});
");
?>

<h1>Danh Sách <?php echo $this->pluralTitle; ?></h1>


<?php echo CHtml::link('Tìm Kiếm Nâng Cao','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'gas-one-many-grid',
	'dataProvider'=>$model->search(),
        'afterAjaxUpdate'=>'function(id, data){ fnUpdateColorbox();}',
        'template'=>'{pager}{summary}{items}{pager}{summary}',
//    'itemsCssClass' => 'items custom_here',
//    'htmlOptions'=> array('class'=>'grid-view custom_here'),
        'pager' => array(
            'maxButtonCount'=>  CmsFormatter::$PAGE_MAX_BUTTON,
        ),
	'enableSorting' => false,
        'rowHtmlOptionsExpression' => 'array("style" => ($data->getConTon() < 0 ? "background: rgb(249, 150, 107);" : ""))',
	//'filter'=>$model,
	'columns'=>array(
        array(
            'header' => 'STT',
            'type' => 'raw',
            'value' => '$this->grid->dataProvider->pagination->currentPage * $this->grid->dataProvider->pagination->pageSize + ($row+1)',
            'headerHtmlOptions' => array('width' => '10px','style' => 'text-align:center;'),
            'htmlOptions' => array('style' => 'text-align:center;')
        ),
            array(
                'header' => 'Tên giám sát / Chuyên viên',
                'name' => 'one_id',
                'type'=>'html',
                'headerHtmlOptions' => array('width' => '400px','style' => 'text-align:center;'),
                'value'=>'"<b>".$data->getInfoChuyenVien()."</b>"',
            ),
            array(
                'header' => 'Hạn mức vỏ',
                'name' => 'many_id',
                'type'=>'raw',
            ),
            array(
                'header' => 'Vỏ thực tế KH nợ',
                'type'=>'raw',
                'value' => '$data->getVoThucTeNo()',
            ),
            array(
                'header' => 'Chênh lệch',
                'type'=>'raw',
                'value' => '$data->getConTon()'
            ),
		array(
                    'header' => 'Actions',
                    'class'=>'CButtonColumn',
                    'template'=> ControllerActionsName::createIndexButtonRoles($actions),
                    'buttons'=>array(
                        'view' => array(
                            'visible' => 'false',
                        ),
                        'update' => array(
//                            'visible' => '$data->canUpdate()',
                        ),
                        'delete'=>array(
//                            'visible'=> 'GasCheck::canDeleteData($data)',
                        ),
                    ),
		),
	),
)); ?>

<script>
$(document).ready(function() {
    fnUpdateColorbox();
});

function fnUpdateColorbox(){   
    fixTargetBlank();
}
</script>