<?php
$this->breadcrumbs=array(
    $this->pluralTitle => array('index'),
    ' ' . $this->singleTitle . ' : ',
);

$saveIconUrl = Yii::app()->theme->baseUrl . '/admin/images/icon/icon-32-save-close.png';
Yii::app()->clientScript->registerCss('menuCss', "
    .operations .export_store.save a{
        background: url({$saveIconUrl}) no-repeat;
    }
");

$menus = array(
        array('label'=>"View PDF", 'url'=>['toPdf', 'id'=>$model->id], 'htmlOptions'=> ['class'=>'export_store', 'label'=>'View PDF'], 'visible'=>$model->canViewPdf()),
        array('label'=>"Download PDF", 'url'=>['toPdf', 'id'=>$model->id, 'download'=>true], 'htmlOptions'=> ['class'=>'export_store save', 'label'=>'Download'], 'visible'=>$model->canViewPdf()),
	array('label'=>"$this->pluralTitle Management", 'url'=>array('index')),
	array('label'=>"Tạo Mới $this->singleTitle", 'url'=>array('create')),
	array('label'=>"Cập Nhật $this->singleTitle", 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>"Xóa $this->singleTitle", 'url'=>array('delete'), 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Bạn chắc chắn muốn xóa ?')),
);
$this->menu= ControllerActionsName::createMenusRoles($menus, $actions);
?>

<h1>Xem: <?php echo $this->singleTitle; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'name',
            [
                'name'=>'user_id',
                'type'=>'raw',
                'value'=>$model->getField("user_id"),
            ],
            [
                'name'=>'contract_type',
                'type'=>'raw',
                'value'=>$model->getField("contract_type"),
            ],
            [
                'name'=>'created_by',
                'type'=>'raw',
                'value'=>$model->getField("created_by"),
            ],
            [
                'name'=>'created_date',
                'type'=>'raw',
                'value'=>$model->getField("created_date"),
            ],
            [
                'name'=>'template',
                'type'=>'raw',
                'value'=>$model->getField("template"),
            ],
	),
)); ?>
