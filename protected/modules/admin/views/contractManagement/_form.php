
<div class="form">

    <?php
    $form = $this->beginWidget('CActiveForm', array(
        'id' => 'contract-management-form',
        'enableAjaxValidation' => false,
        'htmlOptions' => array('enctype' => 'multipart/form-data'),
    ));
    ?>

    <p class="note">Dòng có dấu <span class="required">*</span> là bắt buộc.</p>
    <?php echo MyFormat::BindNotifyMsg(); ?>

    <div class="row">
        <?php echo $form->labelEx($model, 'name'); ?>
        <?php echo $form->textField($model, 'name', array('class' => 'w-600')); ?>
        <?php echo $form->error($model, 'name'); ?>
    </div>
    
    <div class="row">
        <?php echo $form->labelEx($model,'user_id'); ?>
        <?php echo $form->hiddenField($model,'user_id'); ?>
        <?php
            $url = Yii::app()->createAbsoluteUrl('admin/ajax/search_by_role', array('role' => ROLE_CUSTOMER));
            $aData = array(
                'model'=>$model,
                'field_customer_id'=>'user_id',
                'url'=> $url,
                'name_relation_user'=>'rUser',
                'ClassAdd' => 'w-600',
                'field_autocomplete_name' => 'autocomplete_user',
                'placeholder'=>'Nhập mã NV hoặc tên',
            );
            $this->widget('ext.SpaAutocompleteCustomer.SpaAutocompleteCustomer',
                array('data'=>$aData));
        ?>
    </div>
    
    <div class="row">
        <?php echo $form->labelEx($model, 'contract_type'); ?>
        <?php echo $form->dropdownList($model, 'contract_type', $model->getArrayContractType(), ['class'=>'w-600']); ?>
        <?php echo $form->error($model, 'contract_type'); ?>
    </div>

    <div class="template_container"></div>
    
    <div class="row buttons" style="padding-left: 141px;">
        <?php
        $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType' => 'submit',
            'label' => $model->isNewRecord ? 'Create' : 'Save',
            'type' => 'null', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
            'size' => 'small', // null, 'large', 'small' or 'mini'
                //'htmlOptions' => array('style' => 'margin-bottom: 10px; float: right;'),
        ));
        ?>	
    </div>

<?php $this->endWidget(); ?>

</div><!-- form -->
<script>
    $(document).ready(function () {
        $('.form').find('button:submit').click(function () {
            $.blockUI({overlayCSS: {backgroundColor: '#fff'}});
        });
        var type = $('#ContractManagement_contract_type').val();
        bindChangeContractType(type);
    });
    $(document).on('change', '#ContractManagement_contract_type', function(){
        var type = $(this).val();
        bindChangeContractType(type);
    });
    function bindChangeContractType(type){
        $.blockUI();
        $.ajax({
            'url': '<?php echo Yii::app()->createAbsoluteUrl("admin/contractManagement/create"); ?>',
            'data': {
                'id': '<?php echo $model->scenario == 'update' ? $model->id : ''; ?>',
                'contractType':type
            },
            'success':function(html){
                $('.template_container').html(html);
                $.unblockUI();
            },
        });
    }
</script>