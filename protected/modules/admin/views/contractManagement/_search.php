<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=> GasCheck::getCurl(),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'name',array()); ?>
		<?php echo $form->textField($model,'name',array('class'=>'w-300')); ?>
	</div>

	<div class="row">
                <?php echo $form->labelEx($model,'user_id'); ?>
                <?php echo $form->hiddenField($model,'user_id'); ?>
                <?php
                    $url = Yii::app()->createAbsoluteUrl('admin/ajax/search_by_role', array('role' => ROLE_CUSTOMER));
                    $aData = array(
                        'model'=>$model,
                        'field_customer_id'=>'user_id',
                        'url'=> $url,
                        'name_relation_user'=>'rUser',
                        'ClassAdd' => 'w-300',
                        'field_autocomplete_name' => 'autocomplete_user',
                        'placeholder'=>'Nhập mã NV hoặc tên',
                    );
                    $this->widget('ext.SpaAutocompleteCustomer.SpaAutocompleteCustomer',
                        array('data'=>$aData));
                ?>
        </div>

	<div class="row">
		<?php echo $form->label($model,'contract_type',array()); ?>
		<?php echo $form->dropdownList($model,'contract_type', $model->getArrayContractType(),array('class'=>'w-300', 'empty'=>'Select')); ?>
	</div>

        <div class="row">
                <?php echo $form->labelEx($model,'created_by'); ?>
                <?php echo $form->hiddenField($model,'created_by'); ?>
                <?php
                    $url = Yii::app()->createAbsoluteUrl('admin/ajax/search_by_role', array('role' => ROLE_EMPLOYEE_MAINTAIN));
                    $aData = array(
                        'model'=>$model,
                        'field_customer_id'=>'created_by',
                        'url'=> $url,
                        'name_relation_user'=>'rCreatedBy',
                        'ClassAdd' => 'w-300',
                        'field_autocomplete_name' => 'autocomplete_created_by',
                        'placeholder'=>'Nhập mã NV hoặc tên',
                    );
                    $this->widget('ext.SpaAutocompleteCustomer.SpaAutocompleteCustomer',
                        array('data'=>$aData));
                ?>
        </div>

	<div class="row">
            <?php echo $form->labelEx($model,'created_date'); ?>
            <?php 
                $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                    'model'=>$model,        
                    'attribute'=>'created_date',
                    'options'=>array(
                        'showAnim'=>'fold',
                        'dateFormat'=> MyFormat::$dateFormatSearch,
                        'changeMonth' => true,
                        'changeYear' => true,
                        'showOn' => 'button',
                        'buttonImage'=> Yii::app()->theme->baseUrl.'/admin/images/icon_calendar_r.gif',
                        'buttonImageOnly'=> true,                                
                    ),        
                    'htmlOptions'=>array(
                        'class'=>'w-16',
                        'style'=>'height:20px;',
                    ),
                ));
            ?>
        </div>

	<div class="row buttons" style="padding-left: 159px;">
		        <?php $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType'=>'submit',
            'label'=>'Search',
            'type'=>'null', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
            'size'=>'small', // null, 'large', 'small' or 'mini'
            //'htmlOptions' => array('style' => 'margin-bottom: 10px; float: right;'),
        )); ?>	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->