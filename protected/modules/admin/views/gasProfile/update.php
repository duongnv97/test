<?php
$this->breadcrumbs=array(
	'QL Hồ Sơ Pháp Lý'=>array('index'),
	$model->name=>array('view','id'=>$model->id),
	'Cập Nhật',
);

$menus = array(	
        array('label'=>'QL Hồ Sơ Pháp Lý', 'url'=>array('index')),
	array('label'=>'Xem QL Hồ Sơ Pháp Lý', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Tạo Mới QL Hồ Sơ Pháp Lý', 'url'=>array('create')),	
);
$this->menu= ControllerActionsName::createMenusRoles($menus, $actions);

?>

<h1>Cập Nhật Hồ Sơ Pháp Lý: <?php echo $model->name; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>