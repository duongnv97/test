<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'gas-profile-form',
	'enableAjaxValidation'=>false,
    'htmlOptions' => array('enctype' => 'multipart/form-data'),
)); ?>
	<p class="note">Dòng có dấu <span class="required">*</span> là bắt buộc.</p>  
        <?php  if(Yii::app()->user->hasFlash('successUpdate')): ?>
            <div class='success_div'><?php echo Yii::app()->user->getFlash('successUpdate');?></div>         
        <?php endif; ?>  	
        <?php echo $form->errorSummary($model); ?>

        <div class="row buttons" style="padding-left: 141px;">
            <?php $this->widget('bootstrap.widgets.TbButton', array(
                'buttonType'=>'submit',
                'label'=>$model->isNewRecord ? 'Create' :'Save',
                'type'=>'null', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
                'size'=>'small', // null, 'large', 'small' or 'mini'
                //'htmlOptions' => array('style' => 'margin-bottom: 10px; float: right;'),
            )); ?>	
        </div>
	<div class="row">
            <?php echo $form->labelEx($model,'agent_id'); ?>
            <?php echo $form->hiddenField($model,'agent_id', array('class'=>'tb_agent_id')); ?>
            <?php 
            // widget auto complete search user customer and supplier
            $aData = array(
                'model'=>$model,
                'field_customer_id'=>'agent_id',
                'url'=> Yii::app()->createAbsoluteUrl('admin/ajax/search_dropdown_agent'),
                'name_relation_user'=>'rAgent',
                'fnSelectCustomerV2' => '1',
            );
            $this->widget('ext.SpaAutocompleteCustomer.SpaAutocompleteCustomer',
                array('data'=>$aData));                                        
            ?>
            <?php echo $form->error($model,'agent_id'); ?>
	</div>
        <div class="row">
            <?php echo $form->labelEx($model,'status'); ?>
            <?php echo $form->dropDownList($model,'status', ActiveRecord::getUserStatus(),array('class'=>'w-400 ')); ?>
            <?php echo $form->error($model,'status'); ?>
        </div>
	<div class="row">
            <?php echo $form->labelEx($model,'name'); ?>
            <?php echo $form->textField($model,'name',array('class'=>'w-600','maxlength'=>350)); ?>
            <?php echo $form->error($model,'name'); ?>
	</div>
	<div class="row">
            <?php echo $form->labelEx($model,'type'); ?>
            <?php echo $form->dropDownList($model,'type', GasProfile::$ARR_P_TYPE,array('empty'=>'Select', 'class'=>'type_dropdown w-600')); ?>
            <?php echo $form->error($model,'type'); ?>
	</div>
            
        
        <div class="btn_home_contract row clearfix" style="display: none;"  >
            <?php 
            $mGasProfile = new GasProfile();
            echo $mGasProfile->getUrlCreateHomeContract($model->agent_id);
            ?>
        </div>
            
	<div class="row" >
            <?php echo $form->labelEx($model,'order_number'); ?>
            <?php echo $form->dropDownList($model,'order_number', MyFormat::BuildNumberOrder(50),array('class'=>'w-400')); ?>
            <?php echo $form->error($model,'order_number'); ?>
	</div>
            
	<div class="row">
            <?php echo $form->labelEx($model,'list_materials_id'); ?>
            <div class="fix-label">
                <?php
                   $this->widget('ext.multiselect.JMultiSelect',array(
                         'model'=>$model,
                         'attribute'=>'list_materials_id',
//                             'data'=>  Users::getSelectByRoleFinal(ROLE_AGENT, array('gender'=>Users::IS_AGENT)),
                         'data'=> GasMaterials::getListOptionForProfile(),
                         // additional javascript options for the MultiSelect plugin
                        'options'=>array('selectedList' => 30,),
                         // additional style
                         'htmlOptions'=>array('style' => 'width: 800px;'),
                   ));    
               ?>
            </div>
            <?php echo $form->error($model,'list_materials_id'); ?>
	</div>
            
	<div class="row">
		<?php echo $form->labelEx($model,'date_expired'); ?>
		<?php 
                    $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                        'model'=>$model,        
                        'attribute'=>'date_expired',
                        'options'=>array(
                            'showAnim'=>'fold',
                            'dateFormat'=> ActiveRecord::getDateFormatJquery(),
//                            'minDate'=> '0',
//                            'maxDate'=> '0',
                            'changeMonth' => true,
                            'changeYear' => true,
                            'showOn' => 'button',
                            'buttonImage'=> Yii::app()->theme->baseUrl.'/admin/images/icon_calendar_r.gif',
                            'buttonImageOnly'=> true,                                
                        ),        
                        'htmlOptions'=>array(
                            'class'=>'w-16',
                            'style'=>'height:20px;',
//                            'readonly'=>'readonly',
                        ),
                    ));
                ?> 
		<?php echo $form->error($model,'date_expired'); ?>
	</div>
        <div class="row">
            <?php echo $form->labelEx($model,'note'); ?>
            <?php echo $form->textArea($model,'note',array('rows'=>5,'cols'=>80,"placeholder"=>"")); ?>
            <?php echo $form->error($model,'note'); ?>
	</div>            
            
        <div class="row">
            <label>&nbsp</label>
            <div>
                <a href="javascript:void(0);" class="text_under_none item_b" style="line-height:25px" onclick="fnBuildRow();">
                    <img style="float: left;margin-right:8px;" src="<?php echo Yii::app()->theme->baseUrl;?>/images/add.png"> 
                    Thêm Dòng ( Phím tắt F8 )
                </a>
            </div>
        </div>
        <div class="clr"></div>
	
        <div class="row">
            <label>&nbsp</label>
            <table class="materials_table hm_table">
                <thead>
                    <tr>
                        <th class="item_c">#</th>
                        <th class="item_code item_c">File Scan ( cho phép định dạng <?php echo GasProfileDetail::$AllowFile;?>)</th>
                        <th class="item_c">Thứ Tự</th>
                        <th class="item_unit last item_c">Xóa</th>
                    </tr>
                </thead>
                <tbody>
                    <?php 
                        $max_upload = GasProfile::$max_upload;
                        $aOrderNumber = MyFormat::BuildNumberOrder(GasProfile::$max_upload);
                        $max_upload_show = 5;
                    ?>
                    <?php if(count($model->aModelDetail)):?>
                    <?php foreach($model->aModelDetail as $key=>$item):?>
                    <tr class="materials_row">
                        <td class="item_c order_no"></td>
                        <td class="item_c w-400">
                            <?php // echo $form->fileField($item,'file_name[]'); ?>
                            <?php if(!empty($item->file_name) && !empty($item->date_expired)): ?>
                            <p>
                                <a rel="group1" class="gallery" href="<?php echo ImageProcessing::bindImageByModel($item,'','',array('size'=>'size2'));?>"> 
                                    <img width="100" height="70" src="<?php echo ImageProcessing::bindImageByModel($item,'','',array('size'=>'size1'));?>">
                                </a>
                            </p>
                            <?php endif;?>
                            <?php echo $form->error($item,'file_name'); ?>
                            <?php echo $form->hiddenField($item,'aIdNotIn[]', array('value'=>$item->id)); ?>
                        </td>
                        <td class="item_c">
                            <?php echo $form->dropDownList($model->mDetail,'order_number[]', $aOrderNumber,array('class'=>'w-50', 
                                 'options' => array($item->order_number=>array('selected'=>true))
                                    )); ?>
                        </td>
                        <td class="item_c last"><span class="remove_icon_only"></span></td>
                    </tr> 
                    
                    <?php endforeach;?>
                    <?php endif;?>
                    
                    <?php for($i=1; $i<=($max_upload-count($model->aModelDetail)); $i++): ?>
                    <?php $display = "";
                        if($i>$max_upload_show)
                            $display = "display_none";
                    ?>
                    <tr class="materials_row <?php echo $display;?>">
                        <td class="item_c order_no"></td>
                        <td class="item_l w-400">
                            <?php echo $form->fileField($model->mDetail,'file_name[]', array('accept'=>'image/*')); ?>
                            <?php // echo $form->error($model->mDetail,'file_name'); ?>                        
                        </td>
                        <td class="item_c">
                            <?php echo $form->dropDownList($model->mDetail,'order_number[]', $aOrderNumber,array('class'=>'w-50', 
                                 'options' => array($i=>array('selected'=>true))
                                    )); ?>
                        </td>
                        <td class="item_c last"><span class="remove_icon_only"></span></td>
                    </tr>                    
                    <?php endfor;?>
                </tbody>
            </table>
        </div>    

	<div class="row buttons" style="padding-left: 141px;">
            <?php $this->widget('bootstrap.widgets.TbButton', array(
                'buttonType'=>'submit',
                'label'=>$model->isNewRecord ? 'Create' :'Save',
                'type'=>'null', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
                'size'=>'small', // null, 'large', 'small' or 'mini'
                //'htmlOptions' => array('style' => 'margin-bottom: 10px; float: right;'),
            )); ?>	
        </div>
<?php $this->endWidget(); ?>

</div><!-- form -->
<script>
    $(document).keydown(function(e) {
        if(e.which == 119) {
            fnBuildRow();
        }
    });    
    
    $(document).ready(function(){
        $('.form').find('button:submit').click(function(){
            $.blockUI({ overlayCSS: { backgroundColor: '#fff' } }); 
        });
        fnRefreshOrderNumber();
        fnBindRemoveIcon();
        callColorBox();
        initButtonCreateHomeContract();
        jQuery('a.gallery').colorbox({ opacity:0.5 , rel:'group1' });
    });
    
    function fnBuildRow(){
        $('.materials_table').find('tr:visible:last').next('tr').show();
    }
    
    function callColorBox(){
        $(".IframeHomeContract").colorbox({
            iframe:true,
            innerHeight:'1200', 
            innerWidth: '1000',
            close: "<span title='close'>close</span>",
            onClosed: function () { // update view when close colorbox
                loadButtonCreateContract();
            }
        });
    }
    
    $('.type_dropdown').on('change', function() {
        toggleButtonCreateHomeContract(this.value);
    });
    
    function initButtonCreateHomeContract(){
        toggleButtonCreateHomeContract($('#GasProfile_type').val());
    }
    
    function toggleButtonCreateHomeContract(contractType){
        if(contractType == '<?php echo GasProfile::P_TYPE_12 ?>'){
            $('.btn_home_contract').show();
        }else{
            $('.btn_home_contract').hide();
        }
    }
    
    function fnCallSomeFunctionAfterSelectV2(){
        loadButtonCreateContract();
    }
    
    function loadButtonCreateContract(){
        $.ajax({
            url:  '<?php echo Yii::app()->createAbsoluteUrl("admin/gasProfile/create", ['getUrlContract'=>1]); ?>',
            type: "GET",
            data: "agent_id="+$('#GasProfile_agent_id').val(),
            dataType: "html",
            success: function(data){
                $('.btn_home_contract').html(data);
                callColorBox();
            }
        });
    }
</script>