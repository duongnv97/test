<?php
$this->breadcrumbs=array(
	'QL Hồ Sơ Pháp Lý',
);

$menus=array(
            array('label'=>'Create Hồ Sơ Pháp Lý', 'url'=>array('create')),
);
$this->menu= ControllerActionsName::createMenusRoles($menus, $actions);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('gas-profile-grid', {
                url : $(this).attr('action'),
		data: $(this).serialize()
	});
	$.fn.yiiGridView.update('gas-profile-grid-expiry', {
                url : $(this).attr('action'),
		data: $(this).serialize()
	});
	return false;
});
");

Yii::app()->clientScript->registerScript('ajaxupdate', "
$('#gas-profile-grid a.ajaxupdate').live('click', function() {
    $.fn.yiiGridView.update('gas-profile-grid', {
        type: 'POST',
        url: $(this).attr('href'),
        success: function() {
            $.fn.yiiGridView.update('gas-profile-grid');
        }
    });
    return false;
});
");
?>

<h1>QL Hồ Sơ Pháp Lý</h1>

<?php echo CHtml::link('Tìm Kiếm Nâng Cao','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php // if( in_array(Yii::app()->user->role_id, GasProfile::$ROLE_VIEW_EXPIRY) ):?>
<?php if(1):?>
    <?php include "index_expiry.php";?>
    <h1>Hồ Sơ Pháp Lý Bình Thường </h1>
<?php endif;?>

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'gas-profile-grid',
	'dataProvider'=>$model->search(),
        'afterAjaxUpdate'=>'function(id, data){ fnUpdateColorbox();}',
        'template'=>'{pager}{summary}{items}{pager}{summary}', 
        'pager' => array(
            'maxButtonCount'=>  CmsFormatter::$PAGE_MAX_BUTTON,
        ),            
//	'enableSorting' => false,
	//'filter'=>$model,
	'columns'=>array(
        array(
            'header' => 'S/N',
            'type' => 'raw',
            'value' => '$this->grid->dataProvider->pagination->currentPage * $this->grid->dataProvider->pagination->pageSize + ($row+1)',
            'headerHtmlOptions' => array('width' => '10px','style' => 'text-align:center;'),
            'htmlOptions' => array('style' => 'text-align:center;')
        ),
		'code_no',
                array(
                    'name'=>'agent_id',
                    'value'=>'$data->rAgent?$data->rAgent->first_name:""',
//                    'htmlOptions' => array('style' => 'width:80px;'),
                ),
                array(
                    'header' => 'Tên Hồ Sơ',
                    'name'=>'name',
//                    'value'=>'$data->name',
                ),		
                array(
                    'name' => 'type',
                    'value' => 'GasProfile::GetTypeProfile($data)',
                ),
                array(
                    'header' => 'STT',
                    'name' => 'order_number',
                    'htmlOptions' => array('style' => 'text-align:center;width:30px;'),
                ),
                array(
                    'name' => 'list_materials_id',
                    'value'=>'$data',
                    'type'=>'GasProfileMaterial',
                ),
		'date_expired:date',                
                array(
                    'header' => 'Chi tiết file',
                    'value'=>'$data',
                    'type'=>'DetailGasProfile',
                ),
                array(
                    'name'=>'note',
                    'type' => 'html',
                    'value' => 'nl2br($data->note)',
                    'htmlOptions' => array('style' => 'width:80px;'),
                ),
                array(
                    'name'=>'uid_login',
                    'value'=>'$data->rUidLogin?$data->rUidLogin->first_name:""',
//                    'htmlOptions' => array('style' => 'width:80px;'),
//                    'visible'=>Yii::app()->user->role_id==ROLE_ADMIN,
                ),
                array(
                    'name'  => 'status',
                    'value'  => '$data->getStatus()',
                    'htmlOptions' => array('style' => 'text-align:center;')
                ),
		'created_date:datetime',
                array(
                    'name'  => 'update_time',
                    'value'  => '$data->getUpdateTime()',
                    'htmlOptions' => array('style' => 'text-align:center;')
                ),
		array(
                    'header' => 'Actions',
                    'class'=>'CButtonColumn',
                    'template'=> ControllerActionsName::createIndexButtonRoles($actions),
                    'buttons'=>array(
                        'update'=>array(
                            'visible'=> '$data->canUpdate()',
                        ),
                        'delete'=>array(
                            'visible'=> 'GasCheck::canDeleteData($data)',
                        ),
                    ), 
		),
	),
)); ?>

<script>
$(document).ready(function() {
    fnUpdateColorbox();
});

function fnUpdateColorbox(){   
    fixTargetBlank();
//    jQuery('a.gallery').colorbox({ opacity:0.5 , rel:'group1' });
    $(".gallery").colorbox({iframe:true,innerHeight:'1500', innerWidth: '1050',close: "<span title='close'>close</span>"});
}
</script>