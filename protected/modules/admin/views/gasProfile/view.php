<?php
$this->breadcrumbs=array(
	'QL Hồ Sơ Pháp Lý'=>array('index'),
	$model->name,
);

$menus = array(
	array('label'=>'QL Hồ Sơ Pháp Lý', 'url'=>array('index')),
	array('label'=>'Tạo Mới QL Hồ Sơ Pháp Lý', 'url'=>array('create')),
	array('label'=>'Cập Nhật QL Hồ Sơ Pháp Lý', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Xóa QL Hồ Sơ Pháp Lý', 'url'=>array('delete'), 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Bạn chắc chắn muốn xóa ?')),
);
$this->menu= ControllerActionsName::createMenusRoles($menus, $actions);
?>

<h1>Xem Hồ Sơ Pháp Lý: <?php echo $model->name; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(		
		'code_no',
		array(
                    'name'=>'agent_id',
                    'value'=>$model->rAgent?$model->rAgent->first_name:"",
                ),
		'name',
                array(
                    'name'=>'type',
                    'value'=> GasProfile::GetTypeProfile($model),
                ),
		'order_number',
		'date_expired:date',
                array(
                    'name' => 'list_materials_id',
                    'value'=>$model,
                    'type'=>'GasProfileMaterial',
                ),		
		array(
                    'name'=>'note',
                    'type'=>'html',
                    'value'=>nl2br($model->note),
                ),
                array(
                    'name'=>'uid_login',
                    'value'=>$model->rUidLogin?$model->rUidLogin->first_name:"",
                ),
		'created_date:datetime',
	),
)); ?>

<div class="row">
    <label>&nbsp</label>
    <table class="materials_table hm_table">
        <thead>
            <tr>
                <th class="item_c">#</th>
                <th class="item_code item_c">File Scan ( cho phép định dạng <?php echo GasProfileDetail::$AllowFile;?>)</th>
                <th class="item_c">Thứ Tự</th>
            </tr>
        </thead>
        <tbody>
            <?php 
                $max_upload = GasProfile::$max_upload;
                $aOrderNumber = MyFormat::BuildNumberOrder(GasProfile::$max_upload);
                $max_upload_show = 5;
            ?>
            <?php if(count($model->rProfileDetail)):?>
            <?php foreach($model->rProfileDetail as $key=>$item):?>
            <tr class="materials_row">
                <td class="item_c order_no"><?php echo $key+1;?></td>
                <td class="item_c w-400">
                    <?php // echo $form->fileField($item,'file_name[]'); ?>
                    <?php if(!empty($item->file_name) && !empty($item->date_expired)): ?>
                    <p>
                        <a rel="group1" class="gallery" href="<?php echo ImageProcessing::bindImageByModel($item,'','',array('size'=>'size2'));?>"> 
                            <img width="100" height="70" src="<?php echo ImageProcessing::bindImageByModel($item,'','',array('size'=>'size1'));?>">
                        </a>
                    </p>
                    <?php endif;?>
                </td>
                <td class="item_c">
                    <?php echo $item->order_number; ?>
                </td>

            </tr> 

            <?php endforeach;?>
            <?php endif;?>

        </tbody>
    </table>
</div>

<script>
    $(document).ready(function(){
        jQuery('a.gallery').colorbox({ opacity:0.5 , rel:'group1' });
    });

</script>