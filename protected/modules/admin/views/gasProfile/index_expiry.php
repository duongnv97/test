<h1>Hồ Sơ Pháp Lý Sẽ Hết Hạn Trong <?php echo Yii::app()->params['profile_day_alert_expiry']?> ngày tới</h1>
<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'gas-profile-grid-expiry',
	'dataProvider'=>$model->searchExpiry(),
        'afterAjaxUpdate'=>'function(id, data){ fnUpdateColorbox();}',
        'template'=>'{pager}{summary}{items}{pager}{summary}', 
        'pager' => array(
            'maxButtonCount'=>  CmsFormatter::$PAGE_MAX_BUTTON,
        ),            
//	'enableSorting' => false,
	//'filter'=>$model,
	'columns'=>array(
        array(
            'header' => 'S/N',
            'type' => 'raw',
            'value' => '$this->grid->dataProvider->pagination->currentPage * $this->grid->dataProvider->pagination->pageSize + ($row+1)',
            'headerHtmlOptions' => array('width' => '10px','style' => 'text-align:center;'),
            'htmlOptions' => array('style' => 'text-align:center;')
        ),
		'code_no',
                array(
                    'name'=>'agent_id',
                    'value'=>'$data->rAgent?$data->rAgent->first_name:""',
//                    'htmlOptions' => array('style' => 'width:80px;'),
                ),
		array(
                    'header' => 'Tên Hồ Sơ',
                    'name'=>'name',
                ),		
                array(
                    'name' => 'type',
                    'value' => 'GasProfile::GetTypeProfile($data)',
                ),
                array(
                    'header' => 'STT',
                    'name' => 'order_number',
                    'htmlOptions' => array('style' => 'text-align:center;width:30px;'),
                ),
                array(
                    'name' => 'list_materials_id',
                    'value'=>'$data',
                    'type'=>'GasProfileMaterial',
                ),
		'date_expired:date',                
                array(
                    'header' => 'Chi tiết file',
                    'value'=>'$data',
                    'type'=>'DetailGasProfile',
                ),
                array(
                    'name'=>'note',
                    'type' => 'html',
                    'value' => 'nl2br($data->note)',
                    'htmlOptions' => array('style' => 'width:80px;'),
                ),
                array(
                    'name'=>'uid_login',
                    'value'=>'$data->rUidLogin?$data->rUidLogin->first_name:""',
//                    'htmlOptions' => array('style' => 'width:80px;'),
//                    'visible'=>Yii::app()->user->role_id==ROLE_ADMIN,
                ),
                array(
                    'name'  => 'status',
                    'value'  => '$data->getStatus()',
                    'htmlOptions' => array('style' => 'text-align:center;')
                ),
		'created_date:datetime',
                array(
                    'name'  => 'update_time',
                    'value'  => '$data->getUpdateTime()',
                    'htmlOptions' => array('style' => 'text-align:center;')
                ),
		array(
                    'header' => 'Actions',
                    'class'=>'CButtonColumn',
                    'template'=> ControllerActionsName::createIndexButtonRoles($actions),
                    'buttons'=>array(
                        'update'=>array(
                            'visible'=> '$data->canUpdate()',
                        ),
                        'delete'=>array(
                            'visible'=> 'GasCheck::canDeleteData($data)',
                        ),
                    ), 
		),
	),
)); ?>