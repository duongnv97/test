<?php
$this->breadcrumbs=array(
	'QL Hồ Sơ Pháp Lý'=>array('index'),
	'Tạo Mới',
);

$menus = array(		
        array('label'=>'QL Hồ Sơ Pháp Lý', 'url'=>array('index')),
);
$this->menu= ControllerActionsName::createMenusRoles($menus, $actions);
?>

<h1>Tạo Mới Hồ Sơ Pháp Lý</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>