<?php
$this->breadcrumbs=array(
    'Báo Cáo Hồ Sơ Pháp Lý Đại Lý',
);

$menus=array(
    array('label'=>'QL Hồ Sơ Pháp Lý', 'url'=>array('index')),
    array('label'=>'Create Hồ Sơ Pháp Lý', 'url'=>array('create')),
);
$this->menu= ControllerActionsName::createMenusRoles($menus, $actions);
?>

<h1>Báo Cáo Hồ Sơ Pháp Lý Đại Lý</h1>
<?php Yii::app()->clientScript->registerCoreScript('jquery.ui'); ?>
<?php
$gridCss = Yii::app()->getAssetManager()->publish(Yii::getPathOfAlias('zii.widgets.assets')).'/gridview/styles.css';
Yii::app()->getClientScript()->registerCssFile($gridCss);
?>
<?php
/** @Author: HOANG NAM 06/02/2018
 *  @Todo: form search
 *  @Code: NAM007
 *  @Param: 
 **/
?>
<?php include '_search_form.php'; ?>

<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/scroll/jquery.freezetablecolumns.1.1.js"></script> 
<script>
$(function() {
    $('#freezetablecolumns').freezeTableColumns({
        width:       <?php echo Users::WIDTH_freezetable;?>,   // required - bt 1100
        height:      400,   // required
        numFrozen:   1,     // optional
        frozenWidth: 150,   // optional
        clearWidths: true  // optional
      });
});

$(window).load(function(){
    fnAddClassOddEven('items');
    $('.GRID_WILL_SHOW').show();
    fnFixNoWrapFreezeTheadTable();
});

function fnFixNoWrapFreezeTheadTable(){
    $('.grid-view thead th').css({"white-space" : "normal"});
}

</script>
<?php  $aTypeProfile = GasProfile::GetArrayTypeProfile(); ?>

<div class="grid-view display_none  GRID_WILL_SHOW">
    <table class="items freezetablecolumns" id="freezetablecolumns" style="">
        <thead>
            <tr class="h_60">
                <th class="w-150">Đại lý</th>
                <?php foreach($aTypeProfile as $type_id=>$type_text): ?>
                <th class="w-150" style="">
                    <?php echo $type_text;?>
                </th>
                <?php endforeach;?>
            </tr>
        </thead>
        <tbody>
            <?php foreach($rModelAgent as $agent_id => $mUser ): ?>
            <tr class="h_30">
                <td><?php echo $mUser->first_name;?> </td>
                <?php foreach($aTypeProfile as $type_id=>$type_text): ?>
                <?php
                    $link_icon = Yii::app()->theme->baseUrl."/images/ico-validate-error.png";
                    $title = "Chưa Có";
                    if(isset($rData[$agent_id][$type_id])){
                        $link_icon = Yii::app()->theme->baseUrl."/images/ico-validate-success.png";
                        $title = "Có Rồi";
                    }
                ?>
                <td class="item_c" style="">
                    <img src="<?php echo $link_icon;?>" title="<?php echo $title;?>">
                </td>
                <?php endforeach;?>
            </tr>
            <?php endforeach;?>
        </tbody>
    </table>
    
</div>
<!--end div GRID_WILL_SHOW-->