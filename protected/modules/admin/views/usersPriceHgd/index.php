<?php
$this->breadcrumbs=array(
	$this->singleTitle,
);

$menus=array(
            array('label'=>"Create $this->singleTitle", 'url'=>array('create')),
);
$this->menu= ControllerActionsName::createMenusRoles($menus, $actions);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('users-price-hgd-grid', {
                url : $(this).attr('action'),
		data: $(this).serialize()
	});
	return false;
});
");

Yii::app()->clientScript->registerScript('ajaxupdate', "
$('#users-price-hgd-grid a.ajaxupdate').live('click', function() {
    $.fn.yiiGridView.update('users-price-hgd-grid', {
        type: 'POST',
        url: $(this).attr('href'),
        success: function() {
            $.fn.yiiGridView.update('users-price-hgd-grid');
        }
    });
    return false;
});
");
?>

<?php include "index_button.php"; ?>

<?php if(isset($_GET['setup'])): ?>
    <?php include "index_setup.php"; ?>
<?php else: ?>
    <?php include "index_grid.php"; ?>
<?php endif; ?>