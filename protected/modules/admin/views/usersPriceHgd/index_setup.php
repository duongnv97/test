<?php
    $aMonth = ActiveRecord::getMonthVn();
    foreach($aMonth as $month => $text){
        if($month != 1 && $month < date('m')*1){
            unset($aMonth[$month]);
        }
    }

?>
<div class="form">
<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'users-price-hgd-form',
	'enableAjaxValidation'=>false,
        'htmlOptions' => array('enctype' => 'multipart/form-data'),
)); ?>
    <?php echo MyFormat::BindNotifyMsg(); ?>
    <div class="row">
        <label>&nbsp</label>
        <em class="hight_light item_b">Chú Ý: nếu chọn tháng nào thì giá của những tháng đó sẽ chạy theo thiết lập mới</em>
    </div>
    <div class="row ">
        <?php echo $form->labelEx($model,'c_month',array('style' => ''));?>
        <?php echo $form->dropDownList($model,'c_month', $aMonth,array('class' => 'w-200', 'empty'=>"Select")); ?>
    </div>
    <div class="row ">
        <?php echo $form->labelEx($model,'c_year'); ?>
        <?php echo $form->dropDownList($model,'c_year', ActiveRecord::getRangeYear(2016, date('Y')+35),array('class' => 'w-200', 'empty'=>"Select")); ?>
    </div>
    <div class="row ">
        <label>&nbsp</label>
        <em class="hight_light item_b">Chú Ý: định dạng tăng thì nhập <span class="f_size_15">1000</span>, giảm thì nhập <span class="f_size_19">-1000</span></em>
    </div>
    <div class="row">
        <?php echo $form->labelEx($model,'HgdPriceInit'); ?>
        
        <?php echo $form->textField($model,'HgdPriceInit',array('class'=>'w-200 ad_fix_currency','maxlength'=>20)); ?>
        <?php echo $form->error($model,'HgdPriceInit'); ?>
    </div>
    <div class="row display_none">
        <?php echo $form->labelEx($model,'HgdAmountAdjust'); ?>
        <?php echo $form->textField($model,'HgdAmountAdjust',array('class'=>'w-200 ad_fix_currency','maxlength'=>20)); ?>
        <?php echo $form->error($model,'HgdAmountAdjust'); ?>
    </div>
    <div class="row display_none">
        <?php echo $form->labelEx($model,'HgdAmountAdjustGas'); ?>
        <?php echo $form->textField($model,'HgdAmountAdjustGas',array('class'=>'w-200 ad_fix_currency','maxlength'=>20)); ?>
        <?php echo $form->error($model,'HgdAmountAdjustGas'); ?>
    </div>
    
    <div class="row display_none">
        <label>&nbsp</label>
        <em class="hight_light item_b">Chú Ý: chỉ cho phép set giá vào ngày 1 đầu tháng</em>
    </div>
    <?php if(date('d')*1 == 1 || date('d')*1 == date('t')): ?>
    <?php // if(1): ?>
        <div class="row buttons" style="padding-left: 141px;">
            <?php $this->widget('bootstrap.widgets.TbButton', array(
                'buttonType'=>'submit',
                'label' => 'Save',
                'type'=>'null', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
                'size'=>'small', // null, 'large', 'small' or 'mini'
                //'htmlOptions' => array('style' => 'margin-bottom: 10px; float: right;'),
                )); 
            ?>	
        </div>
    <?php endif; ?>

<?php $this->endWidget(); ?>

</div><!-- form -->

<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/autoNumeric-master/autoNumeric.js"></script>
<script>
    $(document).ready(function(){
        $('.form').find('button:submit').click(function(){
            $.blockUI({ overlayCSS: { backgroundColor: '#fff' } }); 
        });
        fnInitInputCurrency();
    });
</script>