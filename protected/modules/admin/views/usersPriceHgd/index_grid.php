
<?php echo CHtml::link('Tìm Kiếm Nâng Cao','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'users-price-hgd-grid',
	'dataProvider'=>$model->search(),
        'afterAjaxUpdate'=>'function(id, data){ fnUpdateColorbox();}',
        'template'=>'{pager}{summary}{items}{pager}{summary}',
//    'itemsCssClass' => 'items custom_here',
//    'htmlOptions'=> array('class'=>'grid-view custom_here'),
        'pager' => array(
            'maxButtonCount'=>  CmsFormatter::$PAGE_MAX_BUTTON,
        ),
	'enableSorting' => false,
	//'filter'=>$model,
	'columns'=>array(
        array(
            'header' => 'S/N',
            'type' => 'raw',
            'value' => '$this->grid->dataProvider->pagination->currentPage * $this->grid->dataProvider->pagination->pageSize + ($row+1)',
            'headerHtmlOptions' => array('width' => '10px','style' => 'text-align:center;'),
            'htmlOptions' => array('style' => 'text-align:center;')
        ),
            array(
                'name' => 'zone_no',
                'value' => '$data->geZoneNo()',
            ),
            array(
                'header' => 'Tháng',
                'type' => 'raw',
                'value' => '$data->getMonthYear().$data->getPriceDefaultText()',
            ),
            array(
                'header' => 'Đại Lý',
                'type' => 'raw',
                'value' => '$data->getListAgentView(true)',
            ),
//            array(
//                'name' => 'created_date',
//                'value' => '$data->getCreatedDate()',
//            ),
            array(
                'name' => 'json',
                'type' => 'raw',
                'value' => '$data->getDetail()',
            ),
            
            array(
                'header' => 'Actions',
                'class'=>'CButtonColumn',
                'template'=> ControllerActionsName::createIndexButtonRoles($actions),
                'buttons'=>array(
                    'update' => array(
                        'visible' => '$data->canUpdate()',
                    ),
                    'delete'=>array(
                        'visible'=> 'GasCheck::canDeleteData($data)',
                    ),
                ),
            ),
	),
)); ?>

<script>
$(document).ready(function() {
    fnUpdateColorbox();
});

function fnUpdateColorbox(){   
    fixTargetBlank();
}
</script>