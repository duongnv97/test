<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=> GasCheck::getCurl(),
	'method'=>'get',
)); ?>

	<div class="row">
            <?php echo $form->label($model,'zone_no',array()); ?>
            <?php echo $form->textField($model,'zone_no',array('class'=>'w-200','maxlength'=>20)); ?>
	</div>

	<div class="row">
            <?php echo $form->labelEx($model,'c_month',array('style' => ''));?>
            <?php echo $form->dropDownList($model,'c_month', ActiveRecord::getMonthVn(),array('class' => 'w-200', 'empty'=>'Select')); ?>
        </div>
        <div class="row">
            <?php echo $form->labelEx($model,'c_year'); ?>
            <?php echo $form->dropDownList($model,'c_year', ActiveRecord::getRangeYear(2016, date('Y')+35),array('class' => 'w-200', 'empty'=>'Select')); ?>
        </div>

	<div class="row buttons" style="padding-left: 159px;">
            <?php $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType'=>'submit',
            'label'=>'Search',
            'type'=>'null', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
            'size'=>'small', // null, 'large', 'small' or 'mini'
            //'htmlOptions' => array('style' => 'margin-bottom: 10px; float: right;'),
        )); ?>	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->