<?php 
//$index=1; 
//$listdataMaterial = AppCache::getListdata('GasMaterials', AppCache::LISTDATA_MATERIAL);
$model->getJsonOther();

?>
<div class="clr"></div>
<div class="row">
    <table class="view_materials_table f_size_15 WrapTypeGas w-600" style="width:100%">
        <thead>
            <tr>
                <th class="item_c"><?php echo $model->getAttributeLabel('json_other');?></th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>
                <div class="WrapBoxMaterial" NameInput="UsersPriceHgd[json_other][]">
                    <div class="row">
                        <?php echo $form->hiddenField($model, "temp_material_6"); ?>
                        <?php 
                            // widget auto complete search material
                            $aData = array(
                                'model'=>$model,
                                'MaterialsJson'=> BuVo::model()->getListdataJsonOtherMaterial(),
                                'field_material_id' => "temp_material_6",
                                'name_relation_material'=>'rFake',
                                'field_autocomplete_name'=> "autocomplete_material_6",
                                'doSomethingOnCloseMaterial'=>'resetOnCloseMaterial',
                                'width'=>'200px',
                            );
                            $this->widget('ext.GasAutocompleteMaterial.GasAutocompleteMaterial',
                                array('data'=>$aData));                                        
                        ?>            
                    </div>

                    <div class="clr"></div>
                    <table class="tb materials_table w-percent100 hm_table FixHeader">
                        <thead>
                            <tr>
                                <th class="item_c w-20">#</th>
                                <th class="item_c w-80">Mã</th>
                                <th class="item_c">Vật tư</th>
                                <th class="item_c">Giá bán</th>
                                <th class="item_c">Tiền công giao nhận</th>
                                <th class="last item_c">Xóa</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $index = 1; 
                                $model->json_other          = is_array($model->json_other) ? $model->json_other : [];
                                $model->list_pay_employee   = is_array($model->list_pay_employee) ? $model->list_pay_employee : []; 
                            ?>
                            <?php foreach($model->json_other as $materials_id => $price): ?>
                            <?php $name = isset($listdataMaterial[$materials_id]) ? $listdataMaterial[$materials_id] : ''; 
                                if(empty($materials_id)){
                                    continue;
                                }
                                $code       = isset($listModelMaterial[$materials_id]) ? $listModelMaterial[$materials_id]['materials_no'] : '';
                                $amountPay   = isset($model->list_pay_employee[$materials_id]) ? $model->list_pay_employee[$materials_id] : '';
                            ?>
                            <tr>                        
                                <td class="item_c order_no"><?php echo $index++ ?></td>
                                <td class="item_c"><?php echo $code;?></td>
                                <td class=""><?php echo $name;?></td>
                                <td class="uid_<?php echo $materials_id;?> w-100">
                                    <input class="f_size_16 ad_fix_currency item_r w-100" name="UsersPriceHgd[json_other][<?php echo $materials_id;?>]" value="<?php echo $price;?>" type="text">
                                </td>
                                <td class="w-100">
                                    <input class="f_size_16 ad_fix_currency item_r w-100" name="UsersPriceHgd[list_pay_employee][<?php echo $materials_id;?>]" value="<?php echo $amountPay;?>" type="text">
                                </td>
                                <td class="item_c last"><span class="remove_icon_only"></span></td>
                            </tr>
                            <?php endforeach;?>
                        </tbody>
                    </table>
                </div><!--end <div class="WrapBoxMaterial">-->
            </td>
            </tr>
        </tbody>
    </table>
</div>
