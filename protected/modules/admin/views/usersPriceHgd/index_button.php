<div class="form">
    <?php
        $LinkNormal = Yii::app()->createAbsoluteUrl('admin/usersPriceHgd/index');
        $LinkDetail = Yii::app()->createAbsoluteUrl('admin/usersPriceHgd/index', array( 'setup'=> 1));
    ?>
    <h1><?php echo $this->singleTitle;?>
        <a class='btn_cancel f_size_14 <?php echo (!isset($_GET['setup'])) ? "active":"";?>' href="<?php echo $LinkNormal;?>">Giá Khu Vực</a>
        <?php if($model->canSetAdjustAmountInMonth()): ?>
            <a class='btn_cancel f_size_14 <?php echo isset($_GET['setup']) && $_GET['setup']==1 ? "active":"";?>' href="<?php echo $LinkDetail;?>">Set Tăng Giảm</a>
        <?php endif; ?>
    </h1> 
</div>