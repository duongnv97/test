<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'users-price-hgd-form',
	'enableAjaxValidation'=>false,
        'htmlOptions' => array('enctype' => 'multipart/form-data'),
)); ?>

    <p class="note">Dòng có dấu <span class="required">*</span> là bắt buộc.</p>
    <?php echo MyFormat::BindNotifyMsg(); ?>
    <div class="row f_size_16">
        <?php echo $form->labelEx($model,'c_month',array('style' => ''));?>
        <?php echo $model->getMonthYear(); ?>
    </div>
    <div class="row f_size_16">
        <?php echo $form->labelEx($model,'zone_no'); ?>
        <?php echo $model->zone_no; ?>
    </div>
    
    <div class="row">
        <?php echo $form->labelEx($model,'price_default'); ?>
        <?php echo $form->textField($model,'price_default',array('class'=>'w-200 ad_fix_currency','maxlength'=>20)); ?>
        <?php echo $form->error($model,'price_default'); ?>
    </div>
    <div class="row">
        <?php echo $form->labelEx($model,'price_cut_1'); ?>
        <?php echo $form->textField($model,'price_cut_1',array('class'=>'w-200 ad_fix_currency','maxlength'=>20)); ?>
        <?php echo $form->error($model,'price_cut_1'); ?>
    </div>
    <div class="row">
        <?php echo $form->labelEx($model,'price_plus_1'); ?>
        <?php echo $form->textField($model,'price_plus_1',array('class'=>'w-200 ad_fix_currency','maxlength'=>20)); ?>
        <?php echo $form->error($model,'price_plus_1'); ?>
    </div>
    <div class="row">
        <?php echo $form->labelEx($model,'price_plus_2'); ?>
        <?php echo $form->textField($model,'price_plus_2',array('class'=>'w-200 ad_fix_currency','maxlength'=>20)); ?>
        <?php echo $form->error($model,'price_plus_2'); ?>
    </div>
    
    <div class="row">
        <?php echo $form->labelEx($model,'price_custom'); ?>
        <?php echo $form->textField($model,'price_custom',array('class'=>'w-200 ad_fix_currency','maxlength'=>20)); ?>
        <?php echo $form->error($model,'price_custom'); ?>
    </div>
    
    <div class="row">
        <?php echo $form->labelEx($model,'agent_id'); ?>
        <?php echo $form->hiddenField($model,'temp_agent', array('class'=>'')); ?>
        <?php
            // 1. limit search kh của sale
            $url = Yii::app()->createAbsoluteUrl('admin/ajax/search_by_role', array('role'=> ROLE_AGENT));
            // widget auto complete search user customer and supplier
            $aData = array(
                'model'=>$model,
                'field_customer_id'=>'temp_agent',
                'url'=> $url,
                'name_relation_user'=>'rAgent',
                'ClassAdd' => 'w-400',
//                'ShowTableInfo' => 0,
                'fnSelectCustomerV2' => "fnDeloyBySelect",
                'doSomethingOnClose' => "doSomethingOnClose",
            );
            $this->widget('ext.SpaAutocompleteCustomer.SpaAutocompleteCustomer',
                array('data'=>$aData));                                        
        ?>
    </div>
    <?php include "_form_agent.php"; ?>
    <br><br>
    <?php include "_form_gas.php"; ?>
    <br><br>
    <?php include "_form_other.php"; ?>
    
    <div class="row buttons" style="padding-left: 141px;">
        <?php $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType'=>'submit',
            'label'=>$model->isNewRecord ? 'Create' :'Save',
            'type'=>'null', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
            'size'=>'small', // null, 'large', 'small' or 'mini'
            //'htmlOptions' => array('style' => 'margin-bottom: 10px; float: right;'),
            )); 
        ?>	
    </div>

<?php $this->endWidget(); ?>

</div><!-- form -->
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/jquery.floatThead.min.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/underscore-min.js"></script>    
<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/print-store-card.css" />
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/autoNumeric-master/autoNumeric.js"></script>
<script>
    $(document).ready(function(){
        $('.form').find('button:submit').click(function(){
            $.blockUI({ overlayCSS: { backgroundColor: '#fff' } }); 
        });
        fnBindRemoveIcon();
        $('.FixHeader').floatThead();
    });
    
    /**
    * @Author: ANH DUNG Jun 23, 2015
    * @Todo: function này dc gọi từ ext của autocomplete
    * @Param: $model
    */
    function fnDeloyBySelect(ui, idField, idFieldCustomer){
        var row = $(idField).closest('.row');
        var ClassCheck = "uid_"+ui.item.id;
        var td_name = '<td class="'+ClassCheck+'">'+ui.item.name_role+'</td>';
        var td_remove = '<td class="item_c last"><span class="remove_icon_only"></span></td>';
        var input = '<input name="UsersPriceHgd[agent_id][]"  value="'+ui.item.id+'" type="hidden">';
        var tr = '<tr><td class="item_c order_no"></td>'+td_name+td_remove+input+'</tr>';
        if($('.tb_deloy_by tbody').find('.'+ClassCheck).size() < 1 ) {
            $('.tb_deloy_by tbody').append(tr);
        }
        fnRefreshOrderNumber();
    }
    
    /**
    * @Author: ANH DUNG Dec 28, 2016
    * @Todo: function này dc gọi từ ext của autocomplete, action close auto
    */
    function doSomethingOnClose(ui, idField, idFieldCustomer){
        var row = $(idField).closest('.row');
        row.find('.remove_row_item').trigger('click');
    }
    
</script>

<script>
    $(document).ready(function(){
        fnInitInputCurrency();
    });
    
    /**
    * @Author: ANH DUNG Apr 29, 2017
    * @Todo: chú ý với các hàm js mà overide từ widget thì phải để ở cuối file, vì nếu để ở giữa, khi mà bên dưới còn nữa thì sẽ bị override
    */
    function afterMaterialSelect(idField, ui){
        var WrapBoxMaterial = $(idField).closest('.WrapBoxMaterial');
        var WrapTypeGas     = $(idField).closest('.WrapTypeGas');
        var nameInput       = WrapBoxMaterial.attr('NameInput');
        var nameInputPrice  = 'UsersPriceHgd[json_other]['+ui.item.id+']';
        var nameInputPayEmployee  = 'UsersPriceHgd[list_pay_employee]['+ui.item.id+']';
        var ClassCheck      = "uid_"+ui.item.id;
        var input           = '<input type="hidden" name="'+nameInput+'" value="'+ui.item.id+'">';
        var inputPrice      = '<input type="text" name="'+nameInputPrice+'" value="" class="f_size_16 ad_fix_currency item_r w-100">';
        var inputPayEmployee = '<input type="text" name="'+nameInputPayEmployee+'" value="" class="f_size_16 ad_fix_currency item_r w-100">';
        
        var td_name         = '<td class="'+ClassCheck+'">'+ui.item.name + input + '</td>';
        var td_name_only    = '<td class="'+ClassCheck+'">'+ui.item.name+'</td>';
        var td_code         = '<td class="item_c">'+ui.item.materials_no+'</td>';
        var td_price        = '<td class="item_c w-100">'+inputPrice+'</td>';
        var td_PayEmployee  = '<td class="item_c w-100">'+inputPayEmployee+'</td>';
        var td_remove       = '<td class="item_c last"><span class="remove_icon_only"></span></td>';
        
        if(idField == '#UsersPriceHgd_autocomplete_material_6'){// van dây bếp
            var tr = '<tr><td class="item_c order_no"></td>'+td_code+td_name_only+td_price+td_PayEmployee+td_remove+'</tr>';
        }else{
            var tr = '<tr><td class="item_c order_no"></td>'+td_name+td_remove+'</tr>';
        }
        
//        if( WrapBoxMaterial.find('tbody').find('.'+ClassCheck).size() < 1 ) {
//            WrapBoxMaterial.find('tbody').append(tr);
//        }
        if( WrapTypeGas.find('.'+ClassCheck).size() < 1 ) {
            WrapBoxMaterial.find('tbody').append(tr);
            fnInitInputCurrency();
        }else{
            alert('Mã gas đã được thiết lập giá, vui lòng chọn lại');
        }
        
        fnRefreshOrderNumber();
    }
    
    function resetOnCloseMaterial(idField, ui){
        $(idField).attr('readonly', false).val('');
    }
    
</script>