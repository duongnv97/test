<?php
$this->breadcrumbs=array(
    $this->pluralTitle => array('index'),
    ' ' . $this->singleTitle . ' : ',
);

$menus = array(
	array('label'=>"$this->pluralTitle Management", 'url'=>array('index')),
	array('label'=>"Tạo Mới $this->singleTitle", 'url'=>array('create')),
	array('label'=>"Cập Nhật $this->singleTitle", 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>"Xóa $this->singleTitle", 'url'=>array('delete'), 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Bạn chắc chắn muốn xóa ?')),
);
$this->menu= ControllerActionsName::createMenusRoles($menus, $actions);
$index=1; 
$mAppCache          = new AppCache();
$listdataMaterial   = $mAppCache->getListdata('GasMaterials', AppCache::LISTDATA_MATERIAL);
$listModelMaterial  = $mAppCache->getMasterModel('GasMaterials', AppCache::ARR_MODEL_MATERIAL);
Yii::app()->clientScript->registerCoreScript('jquery.ui');
?>

<h1>Xem: <?php echo $this->singleTitle; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'zone_no',
		array(
                    'label' => 'Tháng',
                    'type' => 'raw',
                    'value' => $model->getMonthYear().$model->getPriceDefaultText(),
                ),
                'created_date:datetime',
                array(
                    'label' => 'Đại Lý',
                    'type' => 'raw',
                    'value' => $model->getListAgentView(true),
                ),
                array(
                    'name' => 'json',
                    'type' => 'raw',
                    'value' => $model->getDetail(),
                ),
	),
)); ?>
<br><br>
<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'users-price-hgd-form',
	'enableAjaxValidation'=>false,
        'htmlOptions' => array('enctype' => 'multipart/form-data'),
)); ?>
<?php // $model->getDataUpdate(); $model->formatDataGet(); 
include "_form_other.php"; ?>

<?php $this->endWidget(); ?>
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/jquery.floatThead.min.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/underscore-min.js"></script>    
<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/print-store-card.css" />
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/autoNumeric-master/autoNumeric.js"></script>
<script>
    $(document).ready(function(){
        $('.FixHeader').floatThead();
        fnInitInputCurrency();
    });
</script>    
    