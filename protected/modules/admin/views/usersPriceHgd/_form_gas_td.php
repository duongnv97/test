<td>
    <div class="WrapBoxMaterial" NameInput="UsersPriceHgd[<?php echo $fieldName;?>][]">
        <div class="row">
            <?php echo $form->hiddenField($model, "temp_material_$index"); ?>
            <?php 
                // widget auto complete search material
                $aData = array(
                    'model'=>$model,
                    'MaterialsJson'=> BuVo::model()->getListdataGasJson(),
                    'field_material_id' => "temp_material_$index",
                    'name_relation_material'=>'rFake',
                    'field_autocomplete_name'=> "autocomplete_material_$index",
                    'doSomethingOnCloseMaterial'=>'resetOnCloseMaterial',
                    'width'=>'200px',
                );
                $this->widget('ext.GasAutocompleteMaterial.GasAutocompleteMaterial',
                    array('data'=>$aData));                                        
            ?>            
        </div>

        <div class="clr"></div>
        <table class="materials_table w-percent100">
            <thead>
                <tr>
                    <th class="item_c w-20">#</th>
                    <th class=" item_c">Vật tư</th>
                    <th class=" last item_c">Xóa</th>
                </tr>
            </thead>
            <tbody>
                <?php $index = 1;?>
                <?php foreach($model->$fieldName as $materials_id): ?>
                <?php $name = isset($listdataMaterial[$materials_id]) ? $listdataMaterial[$materials_id] : ''; 
                    if(empty($materials_id)){
                        continue;
                    }
                ?>
                <tr>                        
                    <td class="item_c order_no"><?php echo $index++ ?></td>
                    <td class="uid_<?php echo $materials_id;?>">
                        <input name="UsersPriceHgd[<?php echo $fieldName;?>][]" value="<?php echo $materials_id;?>" type="hidden">
                        <?php echo $name;?>
                    </td>
                    <td class="item_c last"><span class="remove_icon_only"></span></td>
                </tr>
                <?php endforeach;?>
            </tbody>
        </table>
    </div><!--end <div class="WrapBoxMaterial">-->
</td>