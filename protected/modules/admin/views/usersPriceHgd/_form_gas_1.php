<?php $index=1; 
$mAppCache          = new AppCache();
$listdataMaterial   = $mAppCache->getListdata('GasMaterials', AppCache::LISTDATA_MATERIAL);
$model->formatDataGet();
?>
<div class="clr"></div>
<div class="row">
    <table class="view_materials_table f_size_15" style="width:100%">
        <thead>
            <tr>
                <th class="item_c">Gas cỏ</th>
                <th class="item_c">Gas thường</th>
                <th class="item_c td_last_r">Gas VIP</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>
                    <div class="WrapBoxMaterial" NameInput="UsersPriceHgd[list_material_co][]">
                        <div class="row">
                            <?php echo $form->hiddenField($model,'material_1'); ?>		
                            <?php 
                                // widget auto complete search material
                                $aData = array(
                                    'model'=>$model,
                                    'MaterialsJson'=> BuVo::model()->getListdataGasJson(),
                                    'field_material_id'=>'material_1',
                                    'name_relation_material'=>'rFake',
                                    'field_autocomplete_name'=>'autocomplete_material_1',
                                    'doSomethingOnCloseMaterial'=>'doSomethingOnCloseMaterial',
                                    'width'=>'200px',
                                );
                                $this->widget('ext.GasAutocompleteMaterial.GasAutocompleteMaterial',
                                    array('data'=>$aData));                                        
                            ?>            
                        </div>

                        <div class="clr"></div>
                        <table class="materials_table w-percent100">
                            <thead>
                                <tr>
                                    <th class="item_c w-20">#</th>
                                    <th class="item_code item_c">Vật tư</th>
                                    <th class="item_unit last item_c">Xóa</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $index = 1;?>
                                <?php foreach($model->list_material_co as $materials_id): ?>
                                <?php $name = isset($listdataMaterial[$materials_id]) ? $listdataMaterial[$materials_id] : ''; 
                                    if(empty($materials_id)){
                                        continue;
                                    }
                                ?>
                                <tr>                        
                                    <td class="item_c order_no"><?php echo $index++ ?></td>
                                    <td class="uid_<?php echo $materials_id;?>">
                                        <input name="UsersPriceHgd[list_material_co][]" value="<?php echo $materials_id;?>" type="hidden">
                                        <?php echo $name;?>
                                    </td>
                                    <td class="item_c last"><span class="remove_icon_only"></span></td>
                                </tr>
                                <?php endforeach;?>
                            </tbody>
                        </table>
                    </div><!--end <div class="WrapBoxMaterial">-->
                </td>
                <td>
                    <div class="WrapBoxMaterial" NameInput="UsersPriceHgd[list_material_normal][]">
                        <div class="row">
                            <?php echo $form->hiddenField($model,'material_2'); ?>		
                            <?php 
                                // widget auto complete search material
                                $aData = array(
                                    'model'=>$model,
                                    'MaterialsJson'=> BuVo::model()->getListdataGasJson(),
                                    'field_material_id'=>'material_2',
                                    'name_relation_material'=>'rFake',
                                    'field_autocomplete_name'=>'autocomplete_material_2',
                                    'doSomethingOnCloseMaterial'=>'doSomethingOnCloseMaterial',
                                    'width'=>'200px',
                                );
                                $this->widget('ext.GasAutocompleteMaterial.GasAutocompleteMaterial',
                                    array('data'=>$aData));                                        
                            ?>            
                        </div>

                        <div class="clr"></div>
                        <table class="materials_table w-percent100">
                            <thead>
                                <tr>
                                    <th class="item_c w-20">#</th>
                                    <th class="item_code item_c">Vật tư</th>
                                    <th class="item_unit last item_c">Xóa</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $index = 1;?>
                                <?php foreach($model->list_material_normal as $materials_id): ?>
                                <?php $name = isset($listdataMaterial[$materials_id]) ? $listdataMaterial[$materials_id] : ''; 
                                    if(empty($materials_id)){
                                        continue;
                                    }
                                ?>
                                <tr>                        
                                    <td class="item_c order_no"><?php echo $index++ ?></td>
                                    <td class="uid_<?php echo $materials_id;?>">
                                        <input name="UsersPriceHgd[list_material_normal][]" value="<?php echo $materials_id;?>" type="hidden">
                                        <?php echo $name;?>
                                    </td>
                                    <td class="item_c last"><span class="remove_icon_only"></span></td>
                                </tr>
                                <?php endforeach;?>
                            </tbody>
                        </table>
                    </div><!--end <div class="WrapBoxMaterial">-->
                    
                </td>
                <td>
                    <div class="WrapBoxMaterial" NameInput="UsersPriceHgd[list_material_vip][]">
                        <div class="row">
                            <?php echo $form->hiddenField($model,'material_3'); ?>		
                            <?php 
                                // widget auto complete search material
                                $aData = array(
                                    'model'=>$model,
                                    'MaterialsJson'=> BuVo::model()->getListdataGasJson(),
                                    'field_material_id'=>'material_3',
                                    'name_relation_material'=>'rFake',
                                    'field_autocomplete_name'=>'autocomplete_material_3',
                                    'doSomethingOnCloseMaterial'=>'doSomethingOnCloseMaterial',
                                    'width'=>'200px',
                                );
                                $this->widget('ext.GasAutocompleteMaterial.GasAutocompleteMaterial',
                                    array('data'=>$aData));                                        
                            ?>            
                        </div>

                        <div class="clr"></div>
                        <table class="materials_table w-percent100">
                            <thead>
                                <tr>
                                    <th class="item_c w-20">#</th>
                                    <th class="item_code item_c">Vật tư</th>
                                    <th class="item_unit last item_c">Xóa</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $index = 1;?>
                                <?php foreach($model->list_material_vip as $materials_id): ?>
                                <?php $name = isset($listdataMaterial[$materials_id]) ? $listdataMaterial[$materials_id] : ''; 
                                    if(empty($materials_id)){
                                        continue;
                                    }
                                ?>
                                <tr>                        
                                    <td class="item_c order_no"><?php echo $index++ ?></td>
                                    <td class="uid_<?php echo $materials_id;?>">
                                        <input name="UsersPriceHgd[list_material_vip][]" value="<?php echo $materials_id;?>" type="hidden">
                                        <?php echo $name;?>
                                    </td>
                                    <td class="item_c last"><span class="remove_icon_only"></span></td>
                                </tr>
                                <?php endforeach;?>
                            </tbody>
                        </table>
                    </div><!--end <div class="WrapBoxMaterial">-->
                </td>
                <td>
                    <div class="WrapBoxMaterial" NameInput="UsersPriceHgd[list_material_special][]">
                        <div class="row">
                            <?php echo $form->hiddenField($model,'material_4'); ?>		
                            <?php 
                                // widget auto complete search material
                                $aData = array(
                                    'model'=>$model,
                                    'MaterialsJson'=> BuVo::model()->getListdataGasJson(),
                                    'field_material_id'=>'material_4',
                                    'name_relation_material'=>'rFake',
                                    'field_autocomplete_name'=>'autocomplete_material_4',
                                    'doSomethingOnCloseMaterial'=>'doSomethingOnCloseMaterial',
                                    'width'=>'200px',
                                );
                                $this->widget('ext.GasAutocompleteMaterial.GasAutocompleteMaterial',
                                    array('data'=>$aData));                                        
                            ?>            
                        </div>

                        <div class="clr"></div>
                        <table class="materials_table w-percent100">
                            <thead>
                                <tr>
                                    <th class="item_c w-20">#</th>
                                    <th class="item_code item_c">Vật tư</th>
                                    <th class="item_unit last item_c">Xóa</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $index = 1;?>
                                <?php foreach($model->list_material_special as $materials_id): ?>
                                <?php $name = isset($listdataMaterial[$materials_id]) ? $listdataMaterial[$materials_id] : ''; 
                                    if(empty($materials_id)){
                                        continue;
                                    }
                                ?>
                                <tr>                        
                                    <td class="item_c order_no"><?php echo $index++ ?></td>
                                    <td class="uid_<?php echo $materials_id;?>">
                                        <input name="UsersPriceHgd[list_material_special][]" value="<?php echo $materials_id;?>" type="hidden">
                                        <?php echo $name;?>
                                    </td>
                                    <td class="item_c last"><span class="remove_icon_only"></span></td>
                                </tr>
                                <?php endforeach;?>
                            </tbody>
                        </table>
                    </div><!--end <div class="WrapBoxMaterial">-->
                </td>
            
            </tr>
        </tbody>
    </table>
</div>

<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/autoNumeric-master/autoNumeric.js"></script>
<script>
    $(document).ready(function(){
        fnInitInputCurrency();
    });
    
    function afterMaterialSelect(idField, ui){
        var WrapBoxMaterial = $(idField).closest('.WrapBoxMaterial');
        var nameInput   =   WrapBoxMaterial.attr('NameInput');
        var ClassCheck = "uid_"+ui.item.id;
        var input = '<input type="hidden" name="'+nameInput+'" value="'+ui.item.id+'">';
        
        var td_name = '<td class="'+ClassCheck+'">'+ui.item.name + input + '</td>';
        var td_remove = '<td class="item_c last"><span class="remove_icon_only"></span></td>';
        var tr = '<tr><td class="item_c order_no"></td>'+td_name+td_remove+'</tr>';
        if( WrapBoxMaterial.find('tbody').find('.'+ClassCheck).size() < 1 ) {
            WrapBoxMaterial.find('tbody').append(tr);
        }
        fnRefreshOrderNumber();
    }
    
    function doSomethingOnCloseMaterial(idField, ui){
        $(idField).attr('readonly', false).val('');
    }
    
</script>