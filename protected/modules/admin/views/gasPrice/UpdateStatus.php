<?php if(GasCheck::isAllowAccess('gasPrice', 'UpdateStatus')):?>
<div class="form">
<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'gas-gasSupportCustomer',
)); ?>

    <p class="note item_b">Duyệt Đề Xuất:</p>
    <?php echo MyFormat::BindNotifyMsg(); ?>
    <?php echo $form->errorSummary($model); ?>
    <div class="row">
        <?php echo $form->labelEx($model,'status'); ?>
        <?php echo $form->dropDownList($model,'status', $model->getArrayStatus(), array('class'=>'w-200')); ?>
        <?php echo $form->error($model,'status'); ?>
    </div>
    
    <div class="row text_area_16">
        <?php echo $form->labelEx($model,'note'); ?>
        <?php echo $form->textArea($model, 'note', array("placeholder"=>"Nhập ghi chú",)); ?>
        <?php echo $form->error($model, 'note'); ?>
    </div>
    <div class="row buttons" style="padding-left: 141px;">
        <?php $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType'=>'submit',
            'label'=> "Save",
            'type'=>'null', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
            'size'=>'small', // null, 'large', 'small' or 'mini'
            //'htmlOptions' => array('style' => 'margin-bottom: 10px; float: right;'),
        )); ?>
        <input class='cancel_iframe' type='button' value='Close'>
    </div>

<?php $this->endWidget(); ?>

</div><!-- form -->
<?php endif;?>