<div class="form f_size_15">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'gas-price-form',
	'enableAjaxValidation'=>false,
        'htmlOptions' => array('enctype' => 'multipart/form-data'),
));
$model->c_month = $model->c_month*1;
?>

    <p class="note">Dòng có dấu <span class="required">*</span> là bắt buộc.</p>
    <?php echo MyFormat::BindNotifyMsg(); ?>
    <?php echo $form->errorSummary($model); ?>
    <div class="row">
        <?php echo $form->labelEx($model,'type_customer'); ?>
        <?php echo $form->dropDownList($model,'type_customer', $model->getArrTypeCustomer(),array('class'=>'type_customer')); ?>
        <?php echo $form->error($model,'type_customer'); ?>
    </div>
    <div class="row">
        <?php echo $form->labelEx($model,'c_month'); ?>
        <?php echo $form->dropDownList($model,'c_month', ActiveRecord::getMonthVn(),array('class'=>'c_month')); ?>
        <?php echo $form->error($model,'c_month'); ?>
    </div>
    <div class="row">
        <?php echo $form->labelEx($model,'c_year'); ?>
        <?php echo $form->dropDownList($model,'c_year', ActiveRecord::getRangeYear(2016, (date("Y")+100)),array('class'=>'c_year')); ?>
        <?php echo $form->error($model,'c_year'); ?>
    </div>
    
    <div class="ShowHide zone<?php echo STORE_CARD_KH_BINH_BO;?>">
        <div class="row">
            <?php echo $form->labelEx($model,'exchange'); ?>
            <?php echo $form->textField($model,'exchange',array('class'=>"w-100 number_only_v1",'maxlength'=>6)); ?>
            <?php echo $form->error($model,'exchange'); ?>
        </div>

        <div class="row">
            <?php echo $form->labelEx($model,'c3'); ?>
            <?php echo $form->textField($model,'c3',array('class'=>"w-100 price_c3  number_only_v1",'maxlength'=>16)); ?>
            <?php echo $form->error($model,'c3'); ?>
        </div>
        <div class="row">
            <?php echo $form->labelEx($model,'c4'); ?>
            <?php echo $form->textField($model,'c4',array('class'=>"w-100 price_c4 number_only_v1",'maxlength'=>16)); ?>
            <?php echo $form->error($model,'c4'); ?>
        </div>

        <div class="row">
            <?php echo $form->labelEx($model,'cp'); ?>
            <?php echo $form->textField($model,'cp',array('readonly'=>1, 'class'=>"w-100 price_cp number_only_v1",'maxlength'=>6)); ?>
            <?php echo $form->error($model,'cp'); ?>
        </div>
        <div class="row">
            <?php echo $form->labelEx($model,'premium'); ?>
            <?php echo $form->textField($model,'premium',array('class'=>"w-100 price_premium number_only_v1",'maxlength'=>6)); ?>
            <?php echo $form->error($model,'premium'); ?>
        </div>
        <div class="row">
            <?php echo $form->labelEx($model,'tax_import'); ?>
            <?php echo $form->textField($model,'tax_import',array('readonly'=>1, 'class'=>"w-100 price_tax_import number_only_v1",'maxlength'=>6)); ?>
            <?php echo $form->error($model,'tax_import'); ?>
        </div>
        <div class="row">
            <?php echo $form->labelEx($model,'cost_production'); ?>
            <?php echo $form->textField($model,'cost_production',array('class'=>"w-100 number_only_v1",'maxlength'=>6)); ?>
            <?php echo $form->error($model,'cost_production'); ?>
        </div>
        <div class="row">
            <?php echo $form->labelEx($model,'cost_manage'); ?>
            <?php echo $form->textField($model,'cost_manage',array('class'=>"w-100 number_only_v1",'maxlength'=>6)); ?>
            <?php echo $form->error($model,'cost_manage'); ?>
        </div>
        <div class="row">
            <?php echo $form->labelEx($model,'cost_interest'); ?>
            <?php echo $form->textField($model,'cost_interest',array('class'=>"w-100 number_only_v1",'maxlength'=>6)); ?>
            <?php echo $form->error($model,'cost_interest'); ?>
        </div>
        <div class="row">
            <?php echo $form->labelEx($model,'cost_logistic'); ?>
            <?php echo $form->textField($model,'cost_logistic',array('class'=>"w-100 number_only_v1",'maxlength'=>6)); ?>
            <?php echo $form->error($model,'cost_logistic'); ?>
        </div>
    </div>
    
    <div class="ShowHide zone<?php echo STORE_CARD_KH_MOI;?>">
        <div class="row">
            <label>&nbsp;</label>
            <button class="btn_cancel btnGetG10" type="button" next="<?php echo Yii::app()->createAbsoluteUrl("admin/gasPrice/create", array('price_g10'=>1));?>">Lấy giá G10 tháng trước</button>
        </div>
        <div class="row">
            <?php echo $form->labelEx($model,'g10_hcm'); ?>
            <?php echo $form->textField($model,'g10_hcm',array('class'=>"w-100 number_only_v1 g10_hcm ", 'maxlength'=>10)); ?>
            <input type="text" class="w-100 number_only_v1 root_g10_hcm display_none" style="margin-left:20px;">
            <?php echo $form->error($model,'g10_hcm'); ?>
        </div>
        <div class="row">
            <?php echo $form->labelEx($model,'g10_tinh'); ?>
            <?php echo $form->textField($model,'g10_tinh',array('class'=>"w-100 number_only_v1 g10_tinh ", 'maxlength'=>10)); ?>
            <input type="text" class="w-100 number_only_v1 root_g10_tinh display_none" style="margin-left:20px;">
            <?php echo $form->error($model,'g10_tinh'); ?>
        </div>
        <div class="row">
            <label>&nbsp</label>
            <em class="hight_light item_b">Ví dụ định dạng khoảng tăng giảm: 5000 (tăng), -5000 (giảm)</em>
        </div>
        <div class="row ">
            <?php echo $form->labelEx($model,'change_g10_hcm'); ?>
            <?php echo $form->textField($model,'change_g10_hcm',array('class'=>"w-100 phone_number_only change_g10_hcm item_r",'maxlength'=>10)); ?>
            <?php echo $form->error($model,'change_g10_hcm'); ?>
        </div>
        
        <div class="row display_none">
            <?php echo $form->labelEx($model,'change_g10_tinh'); ?>
            <?php echo $form->textField($model,'change_g10_tinh',array('class'=>"w-100 phone_number_only change_g10_tinh item_r",'maxlength'=>10)); ?>
            <?php echo $form->error($model,'change_g10_tinh'); ?>
        </div>        
        
    </div>
    <div class="row buttons" style="padding-left: 541px;">
        <?php $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType'=>'submit',
            'label'=>$model->isNewRecord ? 'Create' :'Save',
            'type'=>'null', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
            'size'=>'small', // null, 'large', 'small' or 'mini'
            //'htmlOptions' => array('style' => 'margin-bottom: 10px; float: right;'),
        )); ?>	
    </div>

<?php $this->endWidget(); ?>
</div><!-- form -->
<?php if(!$model->isNewRecord): ?>
    <h3 class='title-info'>Xem lại</h3>
    <div class="tb f_size_18">
        <?php echo $model->getHtmlView(); ?>
    </div>
    <link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/print-store-card.css" />
<?php endif; ?>
<script>
    $(document).ready(function(){
        $('.form').find('button:submit').click(function(){
            $.blockUI({ overlayCSS: { backgroundColor: '#fff' } }); 
        });
        fnChangec3();
        fnChangeTypeCustomer();
        fnBindBtnGetG10();
        fnBindBtnChangeG10();
        $('.type_customer').trigger('change');
    });
    
    function fnChangec3(){
        $('.price_c3, .price_c4, .price_premium').keyup(function(){
            var price_c3 = $('.price_c3').val()*1;
            var price_c4 = $('.price_c4').val()*1;
            var cp = (price_c3+price_c4)/2;
            $('.price_cp').val(cp);
            
            var price_premium = $('.price_premium').val();
            if($.trim(price_premium) != ''){
                console.log(price_premium);
                price_premium = price_premium*1;
            }else{
                price_premium = 0;
            }
            
            var sum = cp+price_premium;
            tax_import = (sum*5)/100;
            tax_import = parseFloat(tax_import);
            tax_import = Math.round(tax_import * 100) / 100;
            $('.price_tax_import').val(tax_import); 
            
        });
    }
    
    function fnChangeTypeCustomer(){
        $('.type_customer').change(function(){
            $('.ShowHide').hide();
            $('.zone'+$(this).val()).show();
        });
    }
    
    // lấy giá g10 của tháng trước với giá mối
    function fnBindBtnGetG10(){
        $('.btnGetG10').click(function(){
            $.blockUI({ overlayCSS: { backgroundColor: '#fff' } });
            var c_month = $(".c_month").val();
            var c_year = $(".c_year").val();
            $.ajax({
                url: $(this).attr('next'),
                dataType: 'json',
                data: {c_month:c_month, c_year:c_year},
                success: function(data){
                    $('.g10_hcm').val(data['g10_hcm']);
                    $('.g10_tinh').val(data['g10_tinh']);
                    
                    $('.root_g10_hcm').val(data['g10_hcm']);
                    $('.root_g10_tinh').val(data['g10_tinh']);
                    $.unblockUI();
                }
            });
        });
    }
    
    function fnBindBtnChangeG10(){
        $('.change_g10_hcm').change(function(){
            var g10_hcm = $('.root_g10_hcm').val()*1;
            var g10_tinh = $('.root_g10_tinh').val()*1;
            var change = $(this).val()*1;
            g10_hcm += change;
            g10_tinh += change;
            g10_hcm = parseFloat(g10_hcm);
            g10_tinh = Math.round(g10_tinh * 100) / 100;
            
            $('.g10_hcm').val(g10_hcm);
            $('.g10_tinh').val(g10_tinh);
        });
    }
    
    
    
    
</script>