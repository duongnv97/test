<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=> GasCheck::getCurl(),
	'method'=>'get',
)); ?>

	<div class="row">
            <?php echo $form->label($model,'type_customer',array()); ?>
            <?php echo $form->dropDownList($model,'type_customer', $model->getArrTypeCustomer(),array('class'=>'type_customer','empty'=>'Select')); ?>
	</div>
	<div class="row">
            <?php echo $form->label($model,'c_month',array()); ?>
            <?php echo $form->dropDownList($model,'c_month', ActiveRecord::getMonthVn(),array('class'=>'c_month','empty'=>'Select')); ?>
	</div>

	<div class="row">
            <?php echo $form->label($model,'c_year',array()); ?>
            <?php echo $form->dropDownList($model,'c_year', ActiveRecord::getRangeYear(2016, (date("Y")+100)),array('class'=>'c_year','empty'=>'Select')); ?>
	</div>
        <div class="row">
            <?php echo $form->label($model,'status',array()); ?>
            <?php echo $form->dropDownList($model,'status', $model->getArrayStatus(),array('class'=>'','empty'=>'Select')); ?>
	</div>

	<div class="row buttons" style="padding-left: 159px;">
		        <?php $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType'=>'submit',
            'label'=>'Search',
            'type'=>'null', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
            'size'=>'small', // null, 'large', 'small' or 'mini'
            //'htmlOptions' => array('style' => 'margin-bottom: 10px; float: right;'),
        )); ?>	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->