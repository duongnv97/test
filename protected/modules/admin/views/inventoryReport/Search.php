<?php
/** @Author: HOANG NAM 31/01/2018
 *  @Todo: search form
 *  @Code: NAM006
 *  @Param: 
 **/
    if($type==InventoryCustomer::VIEW_COUNT):
        include '_date_search.php';
    endif;
    include '_count_search.php';
?>
<div class="row">
    <?php // echo $form->label($model,'delivery_person',array()); ?>
    <?php // echo $form->textField($model,'delivery_person',array('class' => 'w-200','maxlength'=>80)); ?>
    <?php 
    // vì cột is_maintain không dùng trong loại KH của thẻ kho nên ta sẽ dùng cho 1: KH bình bò, 2. KH mối
    echo $form->labelEx($model,'type_customer'); ?>
    <?php echo $form->dropDownList($model,'type_customer', GasStoreCard::$DAILY_INTERNAL_TYPE_CUSTOMER,array('class'=>' w-200','empty'=>'Select')); ?>
    <?php // echo $form->dropDownList($model,'type_customer', GasStoreCard::$DAILY_INTERNAL_TYPE_CUSTOMER, array('class'=>' w-200')); ?>
</div>

<div class="row">
<?php echo $form->labelEx($model,'agent_id'); ?>
<?php echo $form->hiddenField($model,'agent_id', array('class'=>'')); ?>
<?php
        // 1. limit search kh của sale
        $url = Yii::app()->createAbsoluteUrl('admin/ajax/search_by_role', array('role'=> ROLE_AGENT));
        // widget auto complete search user customer and supplier
        $extData = array(
            'model'=>$model,
            'field_customer_id'=>'agent_id',
            'url'=> $url,
            'name_relation_user'=>'rAgent',
            'ClassAdd' => 'w-400',
            'field_autocomplete_name' => 'autocomplete_name_1',
            'placeholder'=>'Nhập mã hoặc tên đại lý',
        );
        $this->widget('ext.SpaAutocompleteCustomer.SpaAutocompleteCustomer',
            array('data'=>$extData));
        ?>
</div>
<div class="row">
    <?php echo $form->labelEx($model,'customer_id'); ?>
    <?php echo $form->hiddenField($model,'customer_id', array('class'=>'customer_id')); ?>
    <?php
    // 1. limit search kh của sale
    $url = Yii::app()->createAbsoluteUrl('admin/ajax/search_user_by_code');
    // widget auto complete search user customer and supplier
    $extData = array(
        'model'=>$model,
        'field_customer_id'=>'customer_id',
        'url'=> $url,
        'name_relation_user'=>'rCustomer',
        'ClassAdd' => 'w-400',
    );
    $this->widget('ext.SpaAutocompleteCustomer.SpaAutocompleteCustomer',
        array('data'=>$extData));
    ?>
    <?php echo $form->error($model,'customer_id'); ?>
</div>
<div class="row">
    <?php echo $form->labelEx($model,'province_id'); ?>
    <div class="fix-label">
        <?php
           $this->widget('ext.multiselect.JMultiSelect',array(
                 'model'=>$model,
                 'attribute'=>'province_id',
                 'data'=> GasProvince::getArrAll(),
                 // additional javascript options for the MultiSelect plugin
                'options'=>array('selectedList' => 30,),
                 // additional style
                 'htmlOptions'=>array('style' => 'width: 800px;'),
           ));    
       ?>
    </div>
</div>