<?php 
/** @Author: HOANG NAM 31/01/2018
 *  @Todo: view list button type
 *  @Code: NAM006
 *  @Param: 
 **/
?>
<div class="form">
    <?php
        $LinkNormal = Yii::app()->createAbsoluteUrl('admin/InventoryReport/ReportVo', ['type'=> InventoryCustomer::VIEW_COUNT]);
        $Link1      = Yii::app()->createAbsoluteUrl('admin/InventoryReport/ReportVo', ['type'=> InventoryCustomer::VIEW_DATE]);
    ?>
    <h1><?php echo $this->pageTitle; ?>
        <a class='btn_cancel f_size_14 <?php echo isset($_GET['type']) && $_GET['type']==InventoryCustomer::VIEW_COUNT ? "active":"";?>' href="<?php echo $LinkNormal;?>">Tồn kho vỏ</a>
        <a class='btn_cancel f_size_14 <?php echo isset($_GET['type']) && $_GET['type']==InventoryCustomer::VIEW_DATE ? "active":"";?>' href="<?php echo $Link1;?>">Không lấy hàng trên 15 ngày</a>
    </h1>
</div>