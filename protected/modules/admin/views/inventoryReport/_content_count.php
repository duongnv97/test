<?php
/** @Author: HOANG NAM 31/01/2018
 *  @Todo: view count > 3
 *  @Code: NAM006
 *  @Param: 
 **/
?>
<?php 
$aCustomer          = isset($aData['CUSTOMER_MODEL']) ? $aData['CUSTOMER_MODEL'] : [];
$aMaterials         = isset($aData['MATERIALS_MODEL']) ? $aData['MATERIALS_MODEL'] : [];
$BEGIN_VO           = isset($aData['BEGIN_VO']) ? $aData['BEGIN_VO'] : [];
$OUTPUT_BEFORE      = isset($aData['OUTPUT_BEFORE']) ? $aData['OUTPUT_BEFORE'] : [];
$OUTPUT_IN_PERIOD   = isset($aData['OUTPUT_IN_PERIOD']) ? $aData['OUTPUT_IN_PERIOD'] : [];
$aIdVo              = isset($aData['ID_VO']) ? $aData['ID_VO'] : [];
$index = 1;
$mAppCache          = new AppCache();
$listdataAgent      = $mAppCache->getAgentListdata();
if(count($aCustomer)>0):
?>
<table class="tb hm_table">
    <thead>
        <tr>
            <th class="item_c">#</th>
            <th class="item_c">Đại lý</th>
            <th class="item_c">Mã kế toán</th>
            <th class="item_c">Mã vỏ</th>
            <th class="item_c">Tên vỏ</th>
            <th class="item_c">Tồn đầu</th>
            <th class="item_c">SL xuất</th>
            <th class="item_c">SL nhập</th>
            <th class="item_c">Tồn cuối</th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($aCustomer as $mCustomer): ?>
        <?php 
        $customer_id = $mCustomer->id;
        $agentName   = isset($listdataAgent[$mCustomer->area_code_id]) ? $listdataAgent[$mCustomer->area_code_id] : '';
//        if(!isset($aIdVo[$mCustomer->id]) || $customer_id != $model->customer_id){
//            continue;
//        }
        $sum_begin = $sum_import = $sum_export = $sum_end = 0;
        ?>
        <tr class="display_none rowShow<?php echo $mCustomer->id;?>">
            <td></td>
        </tr>
            <?php foreach ($aIdVo[$customer_id] as $materials_id => $qty): ?>
            <?php 
                $materials_no = $name  = $materials_id;
                if(isset($aMaterials[$materials_id])){
                    $materials_no = $aMaterials[$materials_id]['materials_no'];
                    $name = $aMaterials[$materials_id]['name'];
                }
                $qty_begin          = isset($BEGIN_VO[$customer_id][$materials_id]) ? $BEGIN_VO[$customer_id][$materials_id] : 0;
                $qty_import_before  = isset($OUTPUT_BEFORE[TYPE_STORE_CARD_IMPORT][$customer_id][$materials_id]) ? $OUTPUT_BEFORE[TYPE_STORE_CARD_IMPORT][$customer_id][$materials_id] : 0;
                $qty_export_before  = isset($OUTPUT_BEFORE[TYPE_STORE_CARD_EXPORT][$customer_id][$materials_id]) ? $OUTPUT_BEFORE[TYPE_STORE_CARD_EXPORT][$customer_id][$materials_id] : 0;
                
                $qty_import_in_period = isset($OUTPUT_IN_PERIOD[TYPE_STORE_CARD_IMPORT][$customer_id][$materials_id]) ? $OUTPUT_IN_PERIOD[TYPE_STORE_CARD_IMPORT][$customer_id][$materials_id] : 0;
                $qty_export_in_period = isset($OUTPUT_IN_PERIOD[TYPE_STORE_CARD_EXPORT][$customer_id][$materials_id]) ? $OUTPUT_IN_PERIOD[TYPE_STORE_CARD_EXPORT][$customer_id][$materials_id] : 0;
                
                $begin      = $qty_begin + $qty_export_before - $qty_import_before;
                $end        = $begin + $qty_export_in_period - $qty_import_in_period;
                $sum_begin      += $begin;
                $sum_import     += $qty_import_in_period;
                $sum_export     += $qty_export_in_period;
                $sum_end        += $end;
            
                if($begin == 0 && $qty_import_in_period == 0 && $qty_export_in_period == 0 && $end == 0){
                    continue;
                }
            ?>
            <tr>
                <td></td><td></td><td></td>
                <td><?php echo $materials_no;?></td>
                <td><?php echo $name;?></td>
                <td class="item_c"><?php echo ActiveRecord::formatCurrencyRound($begin);?></td>
                <td class="item_c"><?php echo ActiveRecord::formatCurrencyRound($qty_export_in_period);?></td>
                <td class="item_c"><?php echo ActiveRecord::formatCurrencyRound($qty_import_in_period);?></td>
                <td class="item_c"><?php echo ActiveRecord::formatCurrencyRound($end);?></td>
            </tr>
            <?php endforeach; ?>
            
            <?php 
                if($sum_begin == 0 && $sum_import == 0 && $sum_export == 0 && $sum_end == 0){
                    continue;
                }
            ?>
            <tr class="rowHide" data-ref="<?php echo $mCustomer->id;?>">
                <td class="item_b"><?php echo $index++;?></td>
                <td class="item_b"><?php echo $agentName;?></td>
                <td class="item_b"><?php echo $mCustomer->code_account;?></td>
                <td></td>
                <td class="item_b"><?php echo $mCustomer->code_bussiness.' - '.$mCustomer->first_name;?></td>
                <td class="item_c item_b"><?php echo ActiveRecord::formatCurrencyRound($sum_begin);?></td>
                <td class="item_c item_b"><?php echo ActiveRecord::formatCurrencyRound($sum_export);?></td>
                <td class="item_c item_b"><?php echo ActiveRecord::formatCurrencyRound($sum_import);?></td>
                <td class="item_c item_b"><?php echo ActiveRecord::formatCurrencyRound($sum_end);?></td>
            </tr>
            
        <?php endforeach; ?>
    </tbody>
</table>
<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/print-store-card.css" />
<?php
    endif;
?>