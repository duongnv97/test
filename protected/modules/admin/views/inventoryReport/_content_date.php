<?php
/** @Author: HOANG NAM 31/01/2018
 *  @Todo: view >=25 date
 *  @Code: NAM006
 *  @Param: 
 **/
?>
<?php if(count($aData)>0): ?>
<table class="tb hm_table">
    <thead>
        <tr>
            <th class="item_c">#</th>
            <th class="item_c">Mã KH</th>
            <!--<th class="item_c">Loại KH</th>-->
            <th class="item_c">KH</th>
            <th class="item_c">Địa chỉ</th>
            <th class="item_c">Đại lý</th>
            <th class="item_c">Nhân viên kinh doanh</th>
            <th class="item_c">Hạn thanh toán</th>
            <th class="item_c">Ngày mua cuối cùng</th>
            <th class="item_c">Tồn kho</th>
        </tr>
    </thead>
    <tbody>
        <?php $curent =1; ?>
        <?php foreach ($aData as $key => $value):?>
        <tr class="rowHide">
            <td class="item_c"><?php echo $curent++; ?></td>
            <td class="item_c"><?php echo $value->getAgentLandline(); ?></td>
            <!--<td class="item_c"><?php echo $value->getTypeCustomerText(); ?></td>-->
            <td class=" "><?php echo $value->getFullName(); ?></td>
            <td class="item_c"><?php echo $value->getAddress(); ?></td>
            <td class="item_c"><?php echo $value->getAgent(); ?></td>
            <td class="item_c"><?php echo $value->getSaleName(); ?></td>
            <td class="item_c" style="width:8%;"><?php echo $value->getPaymentDayCustomer(); ?></td>
            <td class="item_c" style="width:10%;"><?php echo $value->getLastPurchase(); ?></td>
            <td class="item_c"><?php echo $value->getCountVoCustomer(); ?></td>
        </tr>
        <?php endforeach;?>
    </tbody>
</table>
<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/print-store-card.css" />
<?php endif;?>