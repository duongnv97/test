<?php
/** @Author: HOANG NAM 31/01/2018
 *  @Todo: view
 *  @Code: NAM006
 *  @Param: 
 **/
$this->breadcrumbs=array(
    $this->pageTitle,
);
$view       = 'DebitCashForm';
$type       = isset($_GET['type']) ? $_GET['type'] : InventoryCustomer::VIEW_COUNT;
?>
<?php include "index_button.php"; ?>
<div class="search-form" style="">
    <div class="wide form">
    <?php $form=$this->beginWidget('CActiveForm', array(
            'action'=> Yii::app()->createAbsoluteUrl('admin/InventoryReport/ReportVo', ['type'=>(isset($_GET['type']) ? $_GET['type'] :4)]),
            'method'=>'get',
    )); ?>  
        
        <?php include 'Search.php'; ?>
        
        <div class="row buttons" style="padding-left: 159px; margin:20px 0;">
            <?php $this->widget('bootstrap.widgets.TbButton', array(
                'buttonType'=>'submit',
                'label'=>Yii::t('translation','Xem Thống Kê'),
                'type'=>'null', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
                'size'=>'small', // null, 'large', 'small' or 'mini'
                //'htmlOptions' => array('style' => 'margin-bottom: 10px; float: right;'),
            )); ?>	
        </div>
    <?php $this->endWidget(); ?>
      
    </div><!-- wide form -->
</div><!-- search-form -->
<?php 
        if($type ==InventoryCustomer::VIEW_COUNT):
            include '_content_count.php';
        elseif($type ==InventoryCustomer::VIEW_DATE) :   
            include '_content_date.php';
        endif;
?>  