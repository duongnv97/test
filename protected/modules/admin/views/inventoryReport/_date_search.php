<div class="row more_col">
    <div class="col1">
        <?php echo Yii::t('translation', $form->label($model,'date_from')); ?>
        <?php 
            $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                'model'=>$model,        
                'attribute'=>'date_from',
                'options'=>array(
                    'showAnim'=>'fold',
                    'dateFormat'=> MyFormat::$dateFormatSearch,       
                    'maxDate'=> '0',
                    'changeMonth' => true,
                    'changeYear' => true,
                    'showOn' => 'button',
                    'buttonImage'=> Yii::app()->theme->baseUrl.'/admin/images/icon_calendar_r.gif',
                    'buttonImageOnly'=> true,                                
                ),        
                'htmlOptions'=>array(
                    'class'=>'w-16',
                    'size'=>'16',
                    'style'=>'height:20px;float:left;',
                    'readonly'=>'1',
                ),
            ));
        ?>     		
    </div>

    <div class="col2">
        <?php echo Yii::t('translation', $form->label($model,'date_to')); ?>
        <?php 
            $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                'model'=>$model,        
                'attribute'=>'date_to',
                'options'=>array(
                    'showAnim'=>'fold',
                    'dateFormat'=> MyFormat::$dateFormatSearch,       
                    'maxDate'=> '0',
                    'changeMonth' => true,
                    'changeYear' => true,
                    'showOn' => 'button',
                    'buttonImage'=> Yii::app()->theme->baseUrl.'/admin/images/icon_calendar_r.gif',
                    'buttonImageOnly'=> true,                                
                ),        
                'htmlOptions'=>array(
                    'class'=>'w-16',
                    'size'=>'16',
                    'style'=>'height:20px;float:left;',
                    'readonly'=>'1',
                ),
            ));
        ?>    
    </div>
</div>