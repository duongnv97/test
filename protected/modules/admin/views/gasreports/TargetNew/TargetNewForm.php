<?php 
$showXeRao = false;
$aDataChart = $aDataChartHgd = [];
?>
<?php if(isset($data['MODEL_CCS'])) :?>
<?php 
$MODEL_AGENT= isset($data['ROLE_AGENT_MODEL'])?$data['ROLE_AGENT_MODEL']:array();
$MODEL_CCS= isset($data['MODEL_CCS'])?$data['MODEL_CCS']:array();
$OUTPUT_BO= isset($data['OUTPUT_BO'])?$data['OUTPUT_BO']:array();
$OUTPUT_MOI= isset($data['OUTPUT_MOI'])?$data['OUTPUT_MOI']:array();
$OUTPUT_HO_GD= isset($data['OUTPUT_HO_GD'])?$data['OUTPUT_HO_GD']:array();
$GAS_REMAIN= isset($data['GAS_REMAIN'])?$data['GAS_REMAIN']:array();
$aZone = GasOrders::getTargetZone();
$mOneMany = new GasOneMany();
$mOneMany->initSessionAgent([ROLE_MONITORING_MARKET_DEVELOPMENT], GasOneMany::TYPE_AGENT_OF_TARGET);
$session=Yii::app()->session;
$aSaleIdShow = [GasLeave::UID_CHIEF_MONITOR, GasLeave::PHUC_HV];

$aDataChart[]       = array('Task', 'Hours per Day');
//$aDataChartHgd[]    = array('Task', 'Hours per Day');
if($model->agent_id == GasOrders::ZONE_TARGET_MIENTAY || $model->agent_id == GasOrders::ZONE_TARGET_MIENTRUNG){ 
//    $showXeRao = true;
}

    $zoneTargetSumBoKg = 0; // target luon la KG
    $zoneTargetSumMoiKg = 0;
    $zoneTargetSumHoGD = 0;// vi ho GD + XE RAO luon la BINH 12
    $zoneTargetSumXeRao = 0;
    
    $zoneThucTeSumBo50Kg = 0;// Thuc Te luon la KG
    $zoneThucTeSumBo12Kg = 0;// Thuc Te luon la KG
    
    $zoneThucTeSumMoi50Kg = 0;// Thuc Te luon la KG
    $zoneThucTeSumMoi12Kg = 0;// Thuc Te luon la KG
    $zoneThucTeSumHoGD = 0;// Thuc Te luon la BINH
    $zoneThucTeSumXeRao = 0;// Thuc Te luon la BINH
    
    $zoneTargetSum = 0;// Thuc Te luon la KG
    $zoneRealSum12 = 0;// Thuc Te luon la BINH
    $zonePercentSum = 0;
    
    $agentTargetSumBoKg = 0; // target luon la KG
    $agentTargetSumMoiKg = 0;
    $agentTargetSumHoGD = 0;// vi ho GD + XE RAO luon la BINH 12
    $agentTargetSumXeRao = 0;
    
    $agentThucTeSumBo50Kg = 0;// Thuc Te luon la KG
    $agentThucTeSumBo12Kg = 0;// Thuc Te luon la KG
    
    $agentThucTeSumMoi50Kg = 0;// Thuc Te luon la KG
    $agentThucTeSumMoi12Kg = 0;// Thuc Te luon la KG
    $agentThucTeSumHoGD = 0;// Thuc Te luon la BINH
    $agentThucTeSumXeRao = 0;// Thuc Te luon la BINH
    
    $agentTargetSum = 0;// Thuc Te luon la KG
    $agentRealSum12 = 0;// Thuc Te luon la BINH
    $agentPercentSum = 0;
?>

<h1>Target <?php echo $aZone[$model->agent_id];?></h1>
<div class="clearfix box_help_color display_none">
    <div class="color_type_1 color_note"></div>&nbsp;&nbsp; Gas Bình Bò
    <div class="clr"></div>
    <div class="color_type_2 color_note"></div>&nbsp;&nbsp; Gas Bình Mối
    <div class="clr"></div>
    <div class="color_type_3 color_note"></div>&nbsp;&nbsp; Hộ Gia Đình
    <div class="clr"></div>
    <div class="color_type_5 color_note"></div>&nbsp;&nbsp; Xe Rao
    <div class="clr"></div>
</div>

<div class="box_350 box_zone " style="margin-top: 50px; border-right: none;"></div>
<div class="float_l" style="width: 800px">
    <div id="piechart_3d" style="width: 800px; height: 400px;"></div>
</div>
<div class="box_350 box_zone_after clr"></div>
<?php foreach ($MODEL_CCS as $sale_id=>$mUser): ?>
    <?php 
        if(in_array($sale_id, $aSaleIdShow)){
            continue ;
        }
        $display_none=''; $display_row_xe_rao="display_none"; 
        if($sale_id == GasLeave::XE_RAO){
            $display_none="display_none";
            $display_row_xe_rao = "";
        }
    ?>
    <div class="wrap_province clr <?php echo $display_none;?> box_sale_<?php echo $sale_id;?>">
        <div class="box_350 box_ccs"></div>
        
        <div class="float_l" style="width: 720px">
            <?php include "tr_agent.php"; ?>
        </div>
        
        <div class="box_350 box_ccs_hide display_none">
            <table class="hm_table items">
                <tbody>
                    <tr>
                        <td colspan="3" class="item_c item_b color_red"><?php echo $mUser->first_name;?></td>
                    </tr>
                    <tr>
                        <td class="item_c w-140">Target</td>
                        <td class="item_c w-140">Thực Tế</td>
                        <td class="item_c w-70">Tỷ Lệ</td>
                    </tr>
                    <?php include "tr_agent_hide.php"; ?>
                </tbody>
            </table>
        </div>
    </div>
<?php endforeach; ?>

<div class="box_350 box_zone_hide display_none">
    <table class="hm_table items">
        <tbody>
            <tr>
                <td colspan="3" class="item_c item_b color_red"><?php echo $aZone[$model->agent_id];?></td>
            </tr>
            <tr>
                <td class="item_c w-140">Target</td>
                <td class="item_c w-140">Thực Tế</td>
                <td class="item_c w-70">Tỷ Lệ</td>
            </tr>
            <?php include "tr_zone_hide.php"; ?>
        </tbody>
    </table>
</div>

<?php if($showXeRao): ?>
<div class="box_agent_hide display_none">
    <div class="box_350  ">
        <table class="hm_table items">
            <tbody>
                <tr>
                    <td colspan="3" class="item_c item_b color_red">Hệ thống đại lý</td>
                </tr>
                <tr>
                    <td class="item_c w-140">Target</td>
                    <td class="item_c w-140">Thực Tế</td>
                    <td class="item_c w-70">Tỷ Lệ</td>
                </tr>
                <?php include "tr_system_agent.php"; ?>
            </tbody>
        </table>
    </div>
</div>
<?php endif; ?>
<?php endif; ?>
<?php 
arsort($aDataChartHgd['InfoSort']);
$aChartTemp[]    = ['Task', 'Hours per Day'];
foreach($aDataChartHgd['InfoSort'] as $user_id => $value):
    $aChartTemp[]  = [$aDataChartHgd['InfoChart'][$user_id]['label'], $value];
endforeach;
//$js_array = json_encode($aDataChart); 
$js_array = json_encode($aChartTemp); 
//        echo "var javascript_array = ". $js_array . ";\n";
//        die;
// echo '<pre>';
//print_r($js_array);
//echo '</pre>';
//die;
?>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script>
    $(function(){
        $('.box_ccs_hide').each(function(){
            var div_ccs = $(this).closest('div.wrap_province');
            div_ccs.find('.box_ccs').html($(this).html());
        });
        
        $('.box_zone').html($(".box_help_color").html() + $(".box_zone_hide").html());
        <?php if($showXeRao):?>
            var htmlXeRao = '<div class="wrap_province clr zone_xe_rao">'+$(".box_sale_<?php echo GasLeave::XE_RAO;?>").html()+'</div>';
            $('.box_zone_after').after(htmlXeRao);
            
            var htmlSystemAgent = '<div class="wrap_province clr">'+$(".box_agent_hide").html()+'</div>';
            $('.zone_xe_rao').after(htmlSystemAgent);
            $('.zone_xe_rao').find('table').each(function(){
//                $(this).find('tr').eq(2).hide();
//                $(this).find('tr').eq(3).hide();
//                $(this).find('tr').eq(4).hide();
            });
            
        <?php endif;?>
        
    });
</script>

<script type="text/javascript">
    google.charts.load("current", {packages:["corechart"]});
    google.charts.setOnLoadCallback(drawChart);
    function drawChart() {
//      var data = google.visualization.arrayToDataTable([
//        ['Task', 'Hours per Day'],
//        ['Work',     11],
//        ['Eat',      2],
//        ['Commute',  2],
//        ['Watch TV', 202],
//        ['Sleep',    7]
//      ]);
      
      var data = google.visualization.arrayToDataTable(<?php echo $js_array;?>);;
      
      var options = {
        title: 'Biểu đồ target sản lượng hộ GĐ',
        is3D: true,
        chartArea: {left:10, width:"80%"},
        legend: {alignment: 'center'}
      };

      var chart = new google.visualization.PieChart(document.getElementById('piechart_3d'));
      chart.draw(data, options);
    }
  </script>