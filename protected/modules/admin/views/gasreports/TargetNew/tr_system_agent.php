
<tr class="color_type_1">
    <td class="item_r">
        <?php echo ActiveRecord::formatCurrency($agentTargetSumBoKg);?> = 
        <?php echo ActiveRecord::formatCurrency(round($agentTargetSumBoKg/12));?>
    </td>
    <td class="item_r">
        <?php echo ActiveRecord::formatCurrency(round($agentThucTeSumBo50Kg));?> = 
        <?php echo ActiveRecord::formatCurrency(round($agentThucTeSumBo50Kg/12));?><br>
        <?php echo ActiveRecord::formatCurrency($agentThucTeSumBo12Kg);?> = 
        <?php echo ActiveRecord::formatCurrency(round($agentThucTeSumBo12Kg/12));?><br>
        <?php // echo ActiveRecord::formatCurrency($KhBinhBoTo12);?>
    </td>
    <td class="item_c"><?php // echo round($percentBinhBo,1);?></td>
</tr>

<tr class="color_type_2">
    <td class="item_r">
        <?php echo ActiveRecord::formatCurrency($agentTargetSumMoiKg);?> = 
        <?php echo ActiveRecord::formatCurrency(round($agentTargetSumMoiKg/12));?>
    </td>
    <td class="item_r">
        <?php echo ActiveRecord::formatCurrency(round($agentThucTeSumMoi50Kg));?> = 
        <?php echo ActiveRecord::formatCurrency(round($agentThucTeSumMoi50Kg/12));?><br>
        <?php echo ActiveRecord::formatCurrency($agentThucTeSumMoi12Kg);?> = 
        <?php echo ActiveRecord::formatCurrency(round($agentThucTeSumMoi12Kg/12));?><br>
        <?php // echo ActiveRecord::formatCurrency($KhBinhMoi12);?>
    </td>
    <td class="item_c"><?php // echo round($percentMoi,1);?></td>
</tr>

<tr class="color_type_3">
    <td class="item_r">
        <?php echo ActiveRecord::formatCurrency($agentTargetSumHoGD);?> = 
        <?php echo ActiveRecord::formatCurrency(round($agentTargetSumHoGD/12));?>
    </td>
    <td class="item_r">
        <?php echo ActiveRecord::formatCurrency($agentThucTeSumHoGD*12);?> = 
        <?php echo ActiveRecord::formatCurrency($agentThucTeSumHoGD);?>
    </td>
    <td class="item_c"><?php // echo round($percentHoGD,1);?></td>
</tr>

<?php if($showXeRao): ?>
<tr class="color_type_5  <?php echo $display_row_xe_rao;?>">
    <td class="item_r">
        <?php echo ActiveRecord::formatCurrency($agentTargetSumXeRao);?> = 
        <?php echo ActiveRecord::formatCurrency(round($agentTargetSumXeRao/12));?>
    </td>
    <td class="item_r">
        <?php echo ActiveRecord::formatCurrency($agentThucTeSumXeRao * 12);?> = 
        <?php echo ActiveRecord::formatCurrency($agentThucTeSumXeRao);?>
    </td>
    <td class="item_c"><?php // echo round($percentXeRao,1);?></td>
</tr>
<?php endif; ?>

<?php 
$agentPercentSum = $agentRealSum12;
if($agentTargetSum){
    $agentPercentSum = ($agentRealSum12/$agentTargetSum)*100;
//    $percentSum = "$realSum "."/"."$targetSumBinh12 = ".($realSum/$targetSumBinh12)*100;
}
?>

<tr class="color_type_4">
    <td class="item_r">
        <?php echo ActiveRecord::formatCurrency($agentTargetSum);?> = 
        <?php echo ActiveRecord::formatCurrency(round($agentTargetSum/12));?>
    </td>
    <td class="item_r">
        <?php echo ActiveRecord::formatCurrency(round($agentRealSum12));?> = 
        <?php echo ActiveRecord::formatCurrency(round($agentRealSum12/12));?>
    </td>
    <td class="item_c"><?php echo round($agentPercentSum);?> %</td>
</tr>