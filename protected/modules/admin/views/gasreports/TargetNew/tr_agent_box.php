<?php

$targetSum = $realSum = 0;
$agentThucTeSumOnlyKg = 0;// Thuc Te luon la BINH
$type = STORE_CARD_KH_BINH_BO;
$targetBo = isset($data['aTargetEachMonth'][$agent_id][$type])?$data['aTargetEachMonth'][$agent_id][$type]:0;
$targetBoConvertBinh12 = round($targetBo/12);
//$OUTPUT_BO_50=isset($data['OUTPUT_BO'][$agent_id][$sale_id][MATERIAL_TYPE_BINHBO_50])?$data['OUTPUT_BO'][$agent_id][$sale_id][MATERIAL_TYPE_BINHBO_50]:0;
//$OUTPUT_BO_45=isset($data['OUTPUT_BO'][$agent_id][$sale_id][MATERIAL_TYPE_BINHBO_45])?$data['OUTPUT_BO'][$agent_id][$sale_id][MATERIAL_TYPE_BINHBO_45]:0;
//$OUTPUT_BO_12=isset($data['OUTPUT_BO'][$agent_id][$sale_id][MATERIAL_TYPE_BINH_12])?$data['OUTPUT_BO'][$agent_id][$sale_id][MATERIAL_TYPE_BINH_12]:0;
//$REMAIN=isset($data['GAS_REMAIN'][$agent_id][$sale_id][$type])?$data['GAS_REMAIN'][$agent_id][$sale_id][$type]:0;
$OUTPUT_BO_50   = isset($data['OUTPUT_BO'][$agent_id][MATERIAL_TYPE_BINHBO_50])?$data['OUTPUT_BO'][$agent_id][MATERIAL_TYPE_BINHBO_50]:0;
$OUTPUT_BO_45   = isset($data['OUTPUT_BO'][$agent_id][MATERIAL_TYPE_BINHBO_45])?$data['OUTPUT_BO'][$agent_id][MATERIAL_TYPE_BINHBO_45]:0;
$OUTPUT_BO_12   = isset($data['OUTPUT_BO'][$agent_id][MATERIAL_TYPE_BINH_12])?$data['OUTPUT_BO'][$agent_id][MATERIAL_TYPE_BINH_12]:0;
$REMAIN         = isset($data['GAS_REMAIN'][$agent_id][$type])?$data['GAS_REMAIN'][$agent_id][$type]:0;
$BinhBoKg       = ($OUTPUT_BO_50*CmsFormatter::$MATERIAL_VALUE_KG[MATERIAL_TYPE_BINHBO_50] + $OUTPUT_BO_45*CmsFormatter::$MATERIAL_VALUE_KG[MATERIAL_TYPE_BINHBO_45])-$REMAIN;
$BinhBoKg12     = round($BinhBoKg/12);
$BinhBoKg45     = round($BinhBoKg/45); // thay $BinhBoKg12
$KhBinhBoTo12   = round($BinhBoKg/12) + $OUTPUT_BO_12;

$percentBinhBo = $KhBinhBoTo12;
if($targetBoConvertBinh12){
    $percentBinhBo = ($KhBinhBoTo12/$targetBoConvertBinh12)*100;
}

$type = STORE_CARD_KH_MOI;
$targetMoi= isset($data['aTargetEachMonth'][$agent_id][$type])?$data['aTargetEachMonth'][$agent_id][$type]:0;
$targetMoiConvertBinh12 = round($targetMoi/12);
//$OUTPUT_MOI_50=isset($data['OUTPUT_MOI'][$agent_id][$sale_id][MATERIAL_TYPE_BINHBO_50])?$data['OUTPUT_MOI'][$agent_id][$sale_id][MATERIAL_TYPE_BINHBO_50]:0;
//$OUTPUT_MOI_45=isset($data['OUTPUT_MOI'][$agent_id][$sale_id][MATERIAL_TYPE_BINHBO_45])?$data['OUTPUT_MOI'][$agent_id][$sale_id][MATERIAL_TYPE_BINHBO_45]:0;
//$OUTPUT_MOI_12=isset($data['OUTPUT_MOI'][$agent_id][$sale_id][MATERIAL_TYPE_BINH_12])?$data['OUTPUT_MOI'][$agent_id][$sale_id][MATERIAL_TYPE_BINH_12]:0;
//$REMAIN=isset($data['GAS_REMAIN'][$agent_id][$sale_id][$type])?$data['GAS_REMAIN'][$agent_id][$sale_id][$type]:0;
// Jun0219 fix
$OUTPUT_MOI_50  = isset($data['OUTPUT_MOI'][$agent_id][MATERIAL_TYPE_BINHBO_50])?$data['OUTPUT_MOI'][$agent_id][MATERIAL_TYPE_BINHBO_50]:0;
$OUTPUT_MOI_45  = isset($data['OUTPUT_MOI'][$agent_id][MATERIAL_TYPE_BINHBO_45])?$data['OUTPUT_MOI'][$agent_id][MATERIAL_TYPE_BINHBO_45]:0;
$OUTPUT_MOI_12  = isset($data['OUTPUT_MOI'][$agent_id][MATERIAL_TYPE_BINH_12])?$data['OUTPUT_MOI'][$agent_id][MATERIAL_TYPE_BINH_12]:0;
$REMAIN         = isset($data['GAS_REMAIN'][$agent_id][$type])?$data['GAS_REMAIN'][$agent_id][$type]:0;
$BinhMoiKg      = ($OUTPUT_MOI_50*CmsFormatter::$MATERIAL_VALUE_KG[MATERIAL_TYPE_BINHBO_50] + $OUTPUT_MOI_45*CmsFormatter::$MATERIAL_VALUE_KG[MATERIAL_TYPE_BINHBO_45])-$REMAIN;

$BinhMoiKg12 = round($BinhMoiKg/12);
$BinhMoiKg45 = round($BinhMoiKg/45); // Jan1919 thay biến $BinhMoiKg12
$KhBinhMoi12 = round($BinhMoiKg/12) + $OUTPUT_MOI_12;
$percentMoi = $KhBinhMoi12;
if($targetMoiConvertBinh12){
    $percentMoi = ($KhBinhMoi12/$targetMoiConvertBinh12)*100;
}

$type = CUSTOMER_HO_GIA_DINH;
$targetHoGD = isset($data['TARGET_HO_GIA_DINH'][$agent_id])?$data['TARGET_HO_GIA_DINH'][$agent_id]:0;
$targetHoGDConvertBinh12 = round($targetHoGD/12);
if(in_array($agent_id, GasConst::$AGENT_NOT_HGD)){
    $OUTPUT_HO_GD = 0;
}else{
    $OUTPUT_HO_GD=isset($data['OUTPUT_HO_GD'][$agent_id][$type][MATERIAL_TYPE_BINH_12])?$data['OUTPUT_HO_GD'][$agent_id][$type][MATERIAL_TYPE_BINH_12]:0;
}
$percentHoGD = $OUTPUT_HO_GD;
if($targetHoGDConvertBinh12){
    $percentHoGD = ($OUTPUT_HO_GD/$targetHoGDConvertBinh12)*100;
}

$type = STORE_CARD_XE_RAO;
$targetXeRao = isset($data['aTargetEachMonth'][$agent_id][$type])?$data['aTargetEachMonth'][$agent_id][$type]:0;
$targetXeRaoConvertBinh12 = round($targetXeRao/12);
$OUTPUT_XE_RAO_BINH_12 = isset($data['OUTPUT_HO_GD'][$agent_id][$type][MATERIAL_TYPE_BINH_12])?$data['OUTPUT_HO_GD'][$agent_id][$type][MATERIAL_TYPE_BINH_12]:0;
$OUTPUT_XE_RAO_BINH_50=isset($data['OUTPUT_HO_GD'][$agent_id][$type][MATERIAL_TYPE_BINHBO_50])?$data['OUTPUT_HO_GD'][$agent_id][$type][MATERIAL_TYPE_BINHBO_50]:0;
$OUTPUT_XE_RAO_BINH_45=isset($data['OUTPUT_HO_GD'][$agent_id][$type][MATERIAL_TYPE_BINHBO_45])?$data['OUTPUT_HO_GD'][$agent_id][$type][MATERIAL_TYPE_BINHBO_45]:0;

$REMAIN=isset($data['GAS_REMAIN'][$agent_id][GasLeave::XE_RAO][$type])?$data['GAS_REMAIN'][$agent_id][GasLeave::XE_RAO][$type]:0;
$XeRaoBinhBoKg = ($OUTPUT_XE_RAO_BINH_50*CmsFormatter::$MATERIAL_VALUE_KG[MATERIAL_TYPE_BINHBO_50] + $OUTPUT_XE_RAO_BINH_45*CmsFormatter::$MATERIAL_VALUE_KG[MATERIAL_TYPE_BINHBO_45])-$REMAIN;
$XeRaoBinhBoConvert12 = round($XeRaoBinhBoKg/12);
$OUTPUT_XE_RAO = $XeRaoBinhBoConvert12 + $OUTPUT_XE_RAO_BINH_12;

$percentXeRao = $OUTPUT_XE_RAO;
if($targetXeRao){
    $percentXeRao = ($OUTPUT_XE_RAO/$targetXeRao)*100;
}

$targetSum = $targetBo + $targetMoi + $targetHoGD + $targetXeRao;
$targetSumBinh12 = $targetBoConvertBinh12 + $targetMoiConvertBinh12 + $targetHoGDConvertBinh12 + $targetXeRaoConvertBinh12;
$realSum12 = $KhBinhBoTo12 + $KhBinhMoi12 + $OUTPUT_HO_GD + $OUTPUT_XE_RAO;

$agentThucTeSumOnlyKg = $BinhBoKg+ ($OUTPUT_BO_12*12) + $BinhMoiKg + ($OUTPUT_MOI_12*12) + ($OUTPUT_HO_GD*12) + $XeRaoBinhBoKg + ($OUTPUT_XE_RAO_BINH_12*12);

$percentSum = $agentThucTeSumOnlyKg;
if($targetSum){
    $percentSum = ($agentThucTeSumOnlyKg/$targetSum)*100;
//    $percentSum = "$realSum "."/"."$targetSumBinh12 = ".($realSum/$targetSumBinh12)*100;
}

$ccsTargetSumBoKg += $targetBo;
$zoneTargetSumBoKg += $targetBo;

$ccsTargetSumMoiKg += $targetMoi;
$zoneTargetSumMoiKg += $targetMoi;
        
$ccsTargetSumHoGD += $targetHoGD;
$zoneTargetSumHoGD += $targetHoGD;

$ccsTargetSumXeRao += $targetXeRao;
$zoneTargetSumXeRao += $targetXeRao;

$ccsThucTeSumBo50Kg += $BinhBoKg;
$zoneThucTeSumBo50Kg += $BinhBoKg;
$ccsThucTeSumBo12Kg += ($OUTPUT_BO_12 * CmsFormatter::$MATERIAL_VALUE_KG[MATERIAL_TYPE_BINH_12]);
$zoneThucTeSumBo12Kg += ($OUTPUT_BO_12 * CmsFormatter::$MATERIAL_VALUE_KG[MATERIAL_TYPE_BINH_12]);

$ccsThucTeSumMoi50Kg += $BinhMoiKg;
$zoneThucTeSumMoi50Kg += $BinhMoiKg;
$ccsThucTeSumMoi12Kg += ($OUTPUT_MOI_12 * CmsFormatter::$MATERIAL_VALUE_KG[MATERIAL_TYPE_BINH_12]);
$zoneThucTeSumMoi12Kg += ($OUTPUT_MOI_12 * CmsFormatter::$MATERIAL_VALUE_KG[MATERIAL_TYPE_BINH_12]);

$ccsThucTeSumHoGD += $OUTPUT_HO_GD;
$zoneThucTeSumHoGD += $OUTPUT_HO_GD;

$ccsThucTeSumXeRao += $OUTPUT_XE_RAO;
$zoneThucTeSumXeRao += $OUTPUT_XE_RAO;
$ccsThucTeSumXeRao50Kg += $XeRaoBinhBoKg;
$ccsThucTeSumXeRao12Kg += $OUTPUT_XE_RAO_BINH_12;

$ccsTargetSum += $targetSum;
//$zoneTargetSum += $targetSum;
$ccsRealSum12 += $agentThucTeSumOnlyKg;
$zoneRealSum12 += $agentThucTeSumOnlyKg;

if($sale_id != GasLeave::XE_RAO){
    $agentTargetSumBoKg += $targetBo; // target luon la KG
    $agentTargetSumMoiKg += $targetMoi;
    $agentTargetSumHoGD += $targetHoGD;// vi ho GD + XE RAO luon la BINH 12
    $agentTargetSumXeRao += $targetXeRao;
    
    $agentThucTeSumBo50Kg += $BinhBoKg;// Thuc Te luon la KG
    $agentThucTeSumBo12Kg += ($OUTPUT_BO_12 * CmsFormatter::$MATERIAL_VALUE_KG[MATERIAL_TYPE_BINH_12]);// Thuc Te luon la KG
    
    $agentThucTeSumMoi50Kg += $BinhMoiKg;// Thuc Te luon la KG
    $agentThucTeSumMoi12Kg += ($OUTPUT_MOI_12 * CmsFormatter::$MATERIAL_VALUE_KG[MATERIAL_TYPE_BINH_12]);// Thuc Te luon la KG
    $agentThucTeSumHoGD += $OUTPUT_HO_GD;// Thuc Te luon la BINH
    $agentThucTeSumXeRao += $OUTPUT_XE_RAO;// Thuc Te luon la BINH
    
    $agentTargetSum += $targetSum;// Thuc Te luon la KG
    $agentRealSum12 += $agentThucTeSumOnlyKg;// Thuc Te luon la BINH
}

?>

<table class="hm_table items">
    <tbody>
    <tr>
        <td colspan="3" class="item_c item_b"><?php echo $MODEL_AGENT[$agent_id]->first_name;?></td>
    </tr>
    <tr>
        <td class="item_c w-140">Target</td>
        <td class="item_c w-140">Thực Tế</td>
        <td class="item_c w-70">Tỷ Lệ</td>
    </tr>
        
    <tr class="color_type_1">
        <td class="item_r">
            <?php echo ActiveRecord::formatCurrency($targetBo);?>
            <?php // echo ' = '.ActiveRecord::formatCurrency($targetBoConvertBinh12);?>
        </td>
        <td class="item_r">
            <?php echo ActiveRecord::formatCurrency(round($BinhBoKg));?>
            <?php echo ' = '.ActiveRecord::formatCurrency($BinhBoKg45);?><br>
            <?php echo ActiveRecord::formatCurrency($OUTPUT_BO_12 * CmsFormatter::$MATERIAL_VALUE_KG[MATERIAL_TYPE_BINH_12]);?>
            <?php echo ' = '.ActiveRecord::formatCurrency($OUTPUT_BO_12);?><br>
            <?php // echo ActiveRecord::formatCurrency($KhBinhBoTo12);?>
        </td>
        <td class="item_c"><?php echo round($percentBinhBo,1);?> %</td>
    </tr>

    <tr class="color_type_2">
        <td class="item_r">
            <?php echo ActiveRecord::formatCurrency($targetMoi);?>
            <?php // echo ' = '.ActiveRecord::formatCurrency($targetMoiConvertBinh12);?>
        </td>
        <td class="item_r">
            <?php echo ActiveRecord::formatCurrency(round($BinhMoiKg));?>
            <?php echo ' = '.ActiveRecord::formatCurrency($BinhMoiKg45);?><br>
            <?php echo ActiveRecord::formatCurrency($OUTPUT_MOI_12 * CmsFormatter::$MATERIAL_VALUE_KG[MATERIAL_TYPE_BINH_12]);?>
            <?php echo ' = '.ActiveRecord::formatCurrency($OUTPUT_MOI_12);?><br>
            <?php // echo ActiveRecord::formatCurrency($KhBinhMoi12);?>
        </td>
        <td class="item_c"><?php echo round($percentMoi,1);?> %</td>
    </tr>

    <tr class="color_type_3">
        <td class="item_r">
            <?php echo ActiveRecord::formatCurrency($targetHoGD);?>
            <?php // echo ' = '.ActiveRecord::formatCurrency($targetHoGDConvertBinh12);?>
        </td>
        <td class="item_r">
            <?php echo ActiveRecord::formatCurrency($OUTPUT_HO_GD*12);?>
            <?php echo ' = '.ActiveRecord::formatCurrency($OUTPUT_HO_GD);?>
        </td>
        <td class="item_c"><?php echo round($percentHoGD,1);?> %</td>
    </tr>

    <?php if($showXeRao): ?>
    <tr class="color_type_5 <?php echo $display_row_xe_rao;?>">
        <td class="item_r">
            <?php echo ActiveRecord::formatCurrency($targetXeRao);?> = 
            <?php echo ActiveRecord::formatCurrency($targetXeRaoConvertBinh12);?>
        </td>
        <td class="item_r">
            <?php echo ActiveRecord::formatCurrency(round($XeRaoBinhBoKg));?> = 
            <?php echo ActiveRecord::formatCurrency($XeRaoBinhBoConvert12);?><br>
            
            <?php echo ActiveRecord::formatCurrency($OUTPUT_XE_RAO_BINH_12 * 12);?> = 
            <?php echo ActiveRecord::formatCurrency($OUTPUT_XE_RAO_BINH_12);?>
        </td>
        <td class="item_c"><?php // echo round($percentXeRao,1);?></td>
    </tr>
    <?php endif; ?>
    
    
    <tr class="color_type_4">
        <td class="item_r">
            <?php echo ActiveRecord::formatCurrency($targetSum);?>
            <?php // echo ActiveRecord::formatCurrency(round($targetSum/12));?>
        </td>
        <td class="item_r">
            <?php echo ActiveRecord::formatCurrency(round($agentThucTeSumOnlyKg));?>
            <?php echo ' = '.ActiveRecord::formatCurrency(round($agentThucTeSumOnlyKg/12));?>
        </td>
        <td class="item_c"><?php echo round($percentSum);?> %</td>
    </tr>
    </tbody>
</table>