<?php 
$percentBinhBo = $ccsThucTeSumBo50Kg + $ccsThucTeSumBo12Kg;
if($ccsTargetSumBoKg){
    $percentBinhBo = ($percentBinhBo/$ccsTargetSumBoKg)*100;
}

$percentMoi = $ccsThucTeSumMoi50Kg + $ccsThucTeSumMoi12Kg;
if($ccsTargetSumMoiKg){
    $percentMoi = ($percentMoi/$ccsTargetSumMoiKg)*100;
}

$percentHoGD = $ccsThucTeSumHoGD*12;
if($ccsTargetSumHoGD){
    $percentHoGD = ($percentHoGD/$ccsTargetSumHoGD)*100;
}

?>

<tr class="color_type_1">
    <td class="item_r">
        <?php echo ActiveRecord::formatCurrency($ccsTargetSumBoKg);?>
        <?php // echo ' = '.ActiveRecord::formatCurrency(round($ccsTargetSumBoKg/12));?>
    </td>
    <td class="item_r">
        <?php echo ActiveRecord::formatCurrency(round($ccsThucTeSumBo50Kg));?>
        <?php echo ' = '.ActiveRecord::formatCurrency(round($ccsThucTeSumBo50Kg/45));?><br>
        <?php echo ActiveRecord::formatCurrency($ccsThucTeSumBo12Kg);?>
        <?php echo ' = '.ActiveRecord::formatCurrency(round($ccsThucTeSumBo12Kg/12));?><br>
        <?php // echo ActiveRecord::formatCurrency($KhBinhBoTo12);?>
    </td>
    <td class="item_c"><?php echo round($percentBinhBo,1);?> %</td>
</tr>

<tr class="color_type_2">
    <td class="item_r">
        <?php echo ActiveRecord::formatCurrency($ccsTargetSumMoiKg);?>
        <?php // echo ' = '.ActiveRecord::formatCurrency(round($ccsTargetSumMoiKg/12));?>
    </td>
    <td class="item_r">
        <?php echo ActiveRecord::formatCurrency(round($ccsThucTeSumMoi50Kg));?>
        <?php echo ' = '.ActiveRecord::formatCurrency(round($ccsThucTeSumMoi50Kg/45));?><br>
        <?php echo ActiveRecord::formatCurrency($ccsThucTeSumMoi12Kg);?>
        <?php echo ' = '.ActiveRecord::formatCurrency(round($ccsThucTeSumMoi12Kg/12));?><br>
        <?php // echo ActiveRecord::formatCurrency($KhBinhMoi12);?>
    </td>
    <td class="item_c"><?php echo round($percentMoi,1);?> %</td>
</tr>

<tr class="color_type_3">
    <td class="item_r">
        <?php echo ActiveRecord::formatCurrency($ccsTargetSumHoGD);?>
        <?php // echo ' = '.ActiveRecord::formatCurrency(round($ccsTargetSumHoGD/12));?>
    </td>
    <td class="item_r">
        <?php echo ActiveRecord::formatCurrency($ccsThucTeSumHoGD*12);?>
        <?php echo ' = ' . ActiveRecord::formatCurrency($ccsThucTeSumHoGD);?>
    </td>
    <td class="item_c"><?php echo round($percentHoGD,1);?> %</td>
</tr>

<?php if($showXeRao): ?>
<tr class="color_type_5 <?php echo $display_row_xe_rao;?>">
    <td class="item_r">
        <?php echo ActiveRecord::formatCurrency($ccsTargetSumXeRao);?> = 
        <?php echo ActiveRecord::formatCurrency(round($ccsTargetSumXeRao/12));?>
    </td>
    <td class="item_r">
        <?php echo ActiveRecord::formatCurrency($ccsThucTeSumXeRao50Kg);?> = 
        <?php echo ActiveRecord::formatCurrency(round($ccsThucTeSumXeRao50Kg/12));?><br>
        <?php echo ActiveRecord::formatCurrency($ccsThucTeSumXeRao12Kg * 12);?> = 
        <?php echo ActiveRecord::formatCurrency($ccsThucTeSumXeRao12Kg);?>
    </td>
    <td class="item_c"><?php // echo round($percentXeRao,1);?></td>
</tr>
<?php endif; ?>

<?php 
// Aug0718 fix target for GĐ KD khu vực
$targetChuyenVienFix = isset($data['aTargetEachMonth'][$sale_id][STORE_CARD_KH_BINH_BO])?$data['aTargetEachMonth'][$sale_id][STORE_CARD_KH_BINH_BO]:0;
//$ccsTargetSum       = $targetChuyenVienFix;// Jun0219 close - sẽ cộng sum target lại, ko lấy từ setup nữa
$zoneTargetSum      += $ccsTargetSum;
if($ccsTargetSum){
    $ccsPercentSum = ($ccsRealSum12/$ccsTargetSum)*100;
//    $percentSum = "$realSum "."/"."$targetSumBinh12 = ".($realSum/$targetSumBinh12)*100;
}

//$aDataChart[]       = array(round($ccsPercentSum)." % - ".$mUser->first_name, round($ccsRealSum12));
$aDataChart[]       = array(round($percentHoGD)." % - ".$mUser->first_name, round($ccsThucTeSumHoGD));
$aDataChartHgd['InfoChart'][$mUser->id]['qty']   = $ccsThucTeSumHoGD;
$aDataChartHgd['InfoChart'][$mUser->id]['label'] = round($percentHoGD)." % - ".$mUser->first_name;
$aDataChartHgd['InfoSort'][$mUser->id] = $ccsThucTeSumHoGD;
?>

<tr class="color_type_4">
    <td class="item_r">
        <?php echo ActiveRecord::formatCurrency($ccsTargetSum);?>
        <?php // echo ' = '.ActiveRecord::formatCurrency(round($ccsTargetSum/12));?>
    </td>
    <td class="item_r">
        <?php echo ActiveRecord::formatCurrency(round($ccsRealSum12));?>
        <?php // echo ' = '.ActiveRecord::formatCurrency(round($ccsRealSum12/12));?>
    </td>
    <td class="item_c"><?php echo round($ccsPercentSum);?> %</td>
</tr>