<?php 
$percentBinhBo = $zoneThucTeSumBo50Kg + $zoneThucTeSumBo12Kg;
if($zoneTargetSumBoKg){
    $percentBinhBo = ($percentBinhBo/$zoneTargetSumBoKg)*100;
}

$percentMoi = $zoneThucTeSumMoi50Kg + $zoneThucTeSumMoi12Kg;
if($zoneTargetSumMoiKg){
    $percentMoi = ($percentMoi/$zoneTargetSumMoiKg)*100;
}

$percentHoGD = $zoneThucTeSumHoGD*12;
if($zoneTargetSumHoGD){
    $percentHoGD = ($percentHoGD/$zoneTargetSumHoGD)*100;
}

?>

<tr class="color_type_1">
    <td class="item_r">
        <?php echo ActiveRecord::formatCurrency($zoneTargetSumBoKg);?>
        <?php // echo ' = '.ActiveRecord::formatCurrency(round($zoneTargetSumBoKg/12));?>
    </td>
    <td class="item_r">
        <?php echo ActiveRecord::formatCurrency(round($zoneThucTeSumBo50Kg));?>
        <?php echo ' = '.ActiveRecord::formatCurrency(round($zoneThucTeSumBo50Kg/45));?><br>
        <?php echo ActiveRecord::formatCurrency($zoneThucTeSumBo12Kg);?>
        <?php echo ' = '.ActiveRecord::formatCurrency(round($zoneThucTeSumBo12Kg/12));?><br>
        <?php // echo ActiveRecord::formatCurrency($KhBinhBoTo12);?>
    </td>
    <td class="item_c"><?php echo round($percentBinhBo,1);?> %</td>
</tr>

<tr class="color_type_2">
    <td class="item_r">
        <?php echo ActiveRecord::formatCurrency($zoneTargetSumMoiKg);?>
        <?php // echo ' = '.ActiveRecord::formatCurrency(round($zoneTargetSumMoiKg/12));?>
    </td>
    <td class="item_r">
        <?php echo ActiveRecord::formatCurrency(round($zoneThucTeSumMoi50Kg));?>
        <?php echo ' = '.ActiveRecord::formatCurrency(round($zoneThucTeSumMoi50Kg/12));?><br>
        <?php echo ActiveRecord::formatCurrency($zoneThucTeSumMoi12Kg);?>
        <?php echo ' = '.ActiveRecord::formatCurrency(round($zoneThucTeSumMoi12Kg/12));?><br>
        <?php // echo ActiveRecord::formatCurrency($KhBinhMoi12);?>
    </td>
    <td class="item_c"><?php echo round($percentMoi,1);?> %</td>
</tr>

<tr class="color_type_3">
    <td class="item_r">
        <?php echo ActiveRecord::formatCurrency($zoneTargetSumHoGD);?>
        <?php // echo ' = '.ActiveRecord::formatCurrency(round($zoneTargetSumHoGD/12));?>
    </td>
    <td class="item_r">
        <?php echo ActiveRecord::formatCurrency($zoneThucTeSumHoGD*12);?>
        <?php echo ' = '.ActiveRecord::formatCurrency($zoneThucTeSumHoGD);?>
    </td>
    <td class="item_c"><?php echo round($percentHoGD,1);?> %</td>
</tr>

<?php if($showXeRao): ?>
<tr class="color_type_5">
    <td class="item_r">
        <?php echo ActiveRecord::formatCurrency($zoneTargetSumXeRao);?>
        <?php echo ' = '.ActiveRecord::formatCurrency(round($zoneTargetSumXeRao/12));?>
    </td>
    <td class="item_r">
        <?php echo ActiveRecord::formatCurrency($zoneThucTeSumXeRao * 12);?>
        <?php echo ' = '.ActiveRecord::formatCurrency($zoneThucTeSumXeRao);?>
    </td>
    <td class="item_c"><?php // echo round($percentXeRao,1);?></td>
</tr>
<?php endif; ?>

<?php 
$zonePercentSum = $zoneRealSum12;
if($zoneTargetSum){
    $zonePercentSum = ($zoneRealSum12/$zoneTargetSum)*100;
//    $percentSum = "$realSum "."/"."$targetSumBinh12 = ".($realSum/$targetSumBinh12)*100;
}
?>

<tr class="color_type_4">
    <td class="item_r">
        <?php echo ActiveRecord::formatCurrency($zoneTargetSum);?>
        <?php // echo ' = '.ActiveRecord::formatCurrency(round($zoneTargetSum/12));?>
    </td>
    <td class="item_r">
        <?php echo ActiveRecord::formatCurrency(round($zoneRealSum12));?>
        <?php // echo ' = '.ActiveRecord::formatCurrency(round($zoneRealSum12/12));?>
    </td>
    <td class="item_c"><?php echo round($zonePercentSum);?> %</td>
</tr>