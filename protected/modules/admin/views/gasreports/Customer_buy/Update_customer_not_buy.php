<h1>Cập nhật khách hàng: <?php echo $model->first_name; ?></h1>
<p>Đ/C: <?php echo $model->address; ?></p>

<div class="form">

<?php
$form=$this->beginWidget('CActiveForm', array(
	'id'=>'gas-district-form',
	'enableAjaxValidation'=>false,
)); ?>

        <!--<p class="note_focus">Chỉ cập nhật khi số Kg gas dư bị lệch so với lần cân đầu tiên</p>-->
    <div class="row">
        <?php echo $form->labelEx($model->mUsersRef,'reason_leave'); ?>
        <?php echo $form->textArea($model->mUsersRef,'reason_leave',array('class'=>'w-400', 'rows'=>5)); ?>
        <?php echo $form->error($model->mUsersRef,'reason_leave'); ?>
    </div>
    
	<div class="row buttons" style="padding-left: 289px;">
		        <?php $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType'=>'submit',
            'label'=>$model->isNewRecord ? Yii::t('translation', 'Create') : Yii::t('translation', 'Save'),
            'type'=>'null', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
            'size'=>'small', // null, 'large', 'small' or 'mini'
            //'htmlOptions' => array('style' => 'margin-bottom: 10px; float: right;'),
        )); ?>	
            <button name="yt123" type="button" id="yw0" class="btn btn-small btn_cancel">Cancel</button>
        </div>

<?php $this->endWidget(); ?>

</div><!-- form -->
<style>
    div.form .row label { width: 289px; }
</style>


<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/jquery.blockUI.js"></script>  
<script>
    $(document).ready(function(){	
        parent.fnUpdateColorbox();   
        $('.form').find('button:submit').click(function(){
            $.blockUI({ overlayCSS: { backgroundColor: '<?php echo BLOCK_UI_COLOR;?>' } }); 
        });
        
        $('.amount_gas_vo_binh').change(function(){
            var amount_gas_vo_binh = $(this).val();
            var amount_empty = $('.amount_empty').val();
            var  amount = parseFloat(amount_gas_vo_binh-amount_empty);
            amount = Math.round(amount * 100) / 100;
            $('.amount_gas_final').val(amount);            
        });
        
        $('.btn_cancel').click(function(){
            parent.$.fn.colorbox.close();
        });
        
//        parent.$.fn.yiiGridView.update("gas-remain-grid");
    });
</script>