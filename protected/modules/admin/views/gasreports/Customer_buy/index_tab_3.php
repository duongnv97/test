
<div class="search-form3 search-form-only-css is_tab" is_tab="3" style="">
<?php $this->renderPartial('Customer_buy/Customer_buy_search',array(
	'model'=>$model,
        'is_tab'=>3,
)); ?>
</div><!-- search-form -->
<?php
Yii::app()->clientScript->registerScript('search3', "
$('.search-form3 form').submit(function(){
	$.fn.yiiGridView.update('gas-bussiness-contract-grid3', {
                url : $(this).attr('action'),
		data: $(this).serialize()
	});
	return false;
});
");

Yii::app()->clientScript->registerScript('ajaxupdate3', "
$('#gas-bussiness-contract-grid3 a.ajaxupdate').live('click', function() {
    $.fn.yiiGridView.update('gas-bussiness-contract-grid3', {
        type: 'POST',
        url: $(this).attr('href'),
        success: function() {
            $.fn.yiiGridView.update('gas-bussiness-contract-grid3');
        }
    });
    return false;
});
");
?>

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'gas-bussiness-contract-grid3',
	'dataProvider'=>$model->searchBuyAbove45(),
        'afterAjaxUpdate'=>'function(id, data){ fnUpdateColorbox(2);}',
        'template'=>''
        . '<div class="clr hight_light item_b f_size_18">Danh sách khách hàng không lấy hàng trên 45 ngày</div>'
        . '{pager}{summary}{items}{pager}{summary}',  
        'pager' => array(
            'maxButtonCount'=>  CmsFormatter::$PAGE_MAX_BUTTON,
        ),            
	'columns'=>array(
        array(
            'header' => 'S/N',
            'type' => 'raw',
            'value' => '$this->grid->dataProvider->pagination->currentPage * $this->grid->dataProvider->pagination->pageSize + ($row+1)',
            'headerHtmlOptions' => array('width' => '10px','style' => 'text-align:center;'),
            'htmlOptions' => array('style' => 'text-align:center;')
        ),
            array(
                'header' => 'Đại Lý',
                'name' => 'area_code_id',
                'value' => '$data->by_agent?$data->by_agent->first_name:""',
                'visible'=>Yii::app()->user->role_id!=ROLE_SUB_USER_AGENT,
                'htmlOptions' => array('style' => 'text-align:center;width:80px;')
            ),  		
            array(
                'header' => 'Mã KH',
                'name' => 'code_bussiness',
                'htmlOptions' => array('style' => 'width:50px;')
            ),     
            array(
                'header' => 'Loại KH',
                'name' => 'is_maintain',
                'value'=>'$data->is_maintain?CmsFormatter::$CUSTOMER_BO_MOI[$data->is_maintain]:""',
                'htmlOptions' => array('style' => 'width:50px;')
            ),     
            array(                
                'name' => 'first_name',
                'type'=>'NameUser',
                'value'=>'$data',
//                'htmlOptions' => array('style' => 'width:50px;')
            ),     
            array(
                'name' => 'address',
                'type' => 'html',
                //'htmlOptions' => array('style' => 'width:150px;')
            ),
            array(
                'name' => 'phone',
                'value' => '$data->getPhoneShow()',
                'type' => 'html',
            ),
            array(
                'name'=>'sale_id',
                'value' => '$data->sale?$data->sale->first_name:""',
                'htmlOptions' => array('style' => 'text-align:center;width:80px;'),
                'visible'=>in_array(Yii::app()->user->role_id, CmsFormatter::$ROLE_VIEW_KH_BINHBO),
            ), 

            array(
                'name'=>'payment_day',
                'value' => '$data->payment?$data->payment->name:""',
                'htmlOptions' => array('style' => 'text-align:center;width:80px;'),
                'visible'=> in_array(Yii::app()->user->role_id, CmsFormatter::$ROLE_VIEW_KH_BINHBO),
            ), 
            
            array(
                'name' => 'last_purchase',
                'type' => 'Date',
                'htmlOptions' => array('style' => 'width:80px;')
            ),
//            array(
//                'name' => 'created_date',
//                'type' => 'Datetime',
//                'htmlOptions' => array('style' => 'width:50px;')
//            ),               
            array(
                'header' => 'Action',
                'class'=>'CButtonColumn',
                'template'=> ControllerActionsName::createIndexButtonRoles($actions,array('update_customer_not_buy')),
                'buttons'=>array(
                    'update_customer_not_buy'=>array(
                        'label'=>'Không Lấy Hàng',
                        //'imageUrl'=>Yii::app()->theme->baseUrl . '/admin/images/edit.png',
//                        'imageUrl'=>Yii::app()->theme->baseUrl . '/admin/images/icon/update_icon.png',
                        'options'=>array('class'=>'update_customer_not_buy'),
                        'url'=>'Yii::app()->createAbsoluteUrl("admin/gasreports/Update_customer_not_buy",
                            array("id"=>$data->id) )',
//                        'visible'=>  'MyFunctionCustom::agentCanUpdateCustomer($data,array("store_card"=>1))',
                    ),
                ),						
            ),
	),
)); ?>