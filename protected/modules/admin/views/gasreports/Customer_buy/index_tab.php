<?php 
//    $mWeeks = GasWeekSetup::GetWeekNumber($model->year, $model->month);
    $tabActive = 0;
?>
<div id="tabs" class="BindChangeSearchCondition">
    <ul>
        <li>
            <a class="" href="#tabs-1">Từ 15 - 30 Ngày</a>
        </li>
        <li>
            <a class="" href="#tabs-2">Từ 30 - 45 Ngày</a>
        </li>
        <li>
            <a class="" href="#tabs-3">Trên 45 Ngày</a>
        </li>
    </ul>
    
    <div id="tabs-1">
        <?php include 'index_tab_1.php';?>
    </div>
    <div id="tabs-2">
        <?php include 'index_tab_2.php';?>
    </div>
    <div id="tabs-3">
        <?php include 'index_tab_3.php';?>
    </div>
    
</div>    


<?php Yii::app()->clientScript->registerCoreScript('jquery.ui'); ?>
<script>
$(function() {
    $( "#tabs" ).tabs({ active: <?php echo $tabActive;?> });
});
</script>