<?php 
$SubmitButtonClass = '';
$ButtonType = 'submit';
$display_none = 'display_none';
if($is_tab==0){
    $display_none = '';    
    $SubmitButtonClass='SubmitButton btn_cancel';
    $ButtonType = 'button';
}
?>
<div class="wide form  <?php echo $display_none; ?>" style="padding:0;">
<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>
        <div class="row">
            <?php // echo $form->textField($model,'sale_id', array('class'=>'uid_auto_hidden sale_id', 'class_update_val'=>'sale_id')); ?>
            <?php echo $form->hiddenField($model,'sale_id', array('class'=>'uid_auto_hidden sale_id', 'class_update_val'=>'sale_id')); ?>
            <?php if($is_tab==0): ?>
            <?php echo $form->label($model,'sale_id'); ?>
                <?php 
                    // widget auto complete search user customer and supplier
                    $aData = array(
                        'model'=>$model,
                        'field_customer_id'=>'sale_id',
                        'url'=> Yii::app()->createAbsoluteUrl('admin/ajax/search_sale'),
                        'name_relation_user'=>'sale',
                        'placeholder'=>'Nhập Tên Sale',
                        'ClassAdd' => 'w-500',
                        'fnSelectCustomer'=>'fnAfterSelectSaleOrAgent',
                    );
                    $this->widget('ext.SpaAutocompleteCustomer.SpaAutocompleteCustomer',
                        array('data'=>$aData));
                ?>
        <?php endif;?>
        </div>
    
        <div class="row">
            <?php // echo $form->textField($model,'sale_id', array('class'=>'uid_auto_hidden sale_id', 'class_update_val'=>'sale_id')); ?>
            <?php echo $form->hiddenField($model,'customer_id', array('class'=>'uid_auto_hidden customer_id', 'class_update_val'=>'customer_id')); ?>
            <?php if($is_tab==0): ?>
            <?php echo $form->label($model,'customer_id'); ?>
                <?php 
                    // widget auto complete search user customer and supplier
                    $aData = array(
                        'model'=>$model,
                        'field_customer_id'=>'customer_id',
                        'url'=> Yii::app()->createAbsoluteUrl('admin/ajax/search_user_by_code'),
                        'name_relation_user'=>'sale',
                        'placeholder'=>'Nhập Tên KH',
                        'ClassAdd' => 'w-500',
                        'field_autocomplete_name'=>'autocomplete_name_parent',
                        'fnSelectCustomer'=>'fnAfterSelectSaleOrAgent',
                    );
                    $this->widget('ext.SpaAutocompleteCustomer.SpaAutocompleteCustomer',
                        array('data'=>$aData));
                ?>
        <?php endif;?>
        </div>
    
        <div class="row">
            <?php echo Yii::t('translation', $form->labelEx($model,'agent_id_search')); ?>
            <?php echo $form->dropDownList($model,'agent_id_search', Users::getSelectByRole(),array('class'=>' agent_id_search','class_update_val'=>'agent_id_search','empty'=>'Select')); ?>
        </div>

	<div class="row">
            <?php echo $form->label($model,'is_maintain', array('label'=>'Loại KH')); ?>
            <?php echo $form->dropDownList($model,'is_maintain', CmsFormatter::$CUSTOMER_BO_MOI,array('class'=>' is_maintain','class_update_val'=>'is_maintain','empty'=>'Select')); ?>
            <?php echo $form->error($model,'is_maintain'); ?>
	</div>	
    
	<div class="row buttons" style="padding-left: 159px;">
            <?php $this->widget('bootstrap.widgets.TbButton', array(
                'buttonType'=>$ButtonType,
                'label'=>Yii::t('translation','Search'),
                'type'=>'null', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
                'size'=>'small', // null, 'large', 'small' or 'mini'
                'htmlOptions' => array('class' => $SubmitButtonClass),
            )); ?>	
        </div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->