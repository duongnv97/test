<?php
$gridCss = Yii::app()->getAssetManager()->publish(Yii::getPathOfAlias('zii.widgets.assets')).'/gridview/styles.css';
Yii::app()->getClientScript()->registerCssFile($gridCss);
$MODEL_USER             = GasOrders::$ZONE_HCM_FULLNAME;
$TYPE_CUSTOMER_COUNT    = isset($aData['OUTPUT']['TYPE_CUSTOMER_COUNT'])?$aData['OUTPUT']['TYPE_CUSTOMER_COUNT']:array();
$TYPE_CUSTOMER_QTY      = isset($aData['OUTPUT']['TYPE_CUSTOMER_QTY'])?$aData['OUTPUT']['TYPE_CUSTOMER_QTY']:array();
$GAS_REMAIN             = isset($aData['GAS_REMAIN'])?$aData['GAS_REMAIN'] : 0;
$sumRemain      = 0;
$sumAll         = 0;

$index = 1;
?>
<div class="grid-view ">
<h1>Báo cáo sản lượng điều phối từ <?php echo $model->date_from;?> đến <?php echo $model->date_to;?></h1>
<table class="items hm_table f_size_15">
    <thead>
        <tr style="">
            <th class="w-20 ">#</th>
            <th class="w-200 ">Điều Phối</th>
            <th class="w-250 ">Chi Tiết</th>
            <!--<th class="w-100 ">Gas Dư (KG)</th>-->
            <th class="w-100 ">Sản Lượng (KG)</th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($TYPE_CUSTOMER_COUNT as $uid_login => $aData): ?>
            <?php
                $fullName       = isset($MODEL_USER[$uid_login]) ? $MODEL_USER[$uid_login] : "";
                $amountRemain   = 0;
                $sumQtyType     = 0;
            ?>
            <tr>
                <td class="item_c"><?php echo $index++;?></td>
                <td class="item_b"><?php echo $fullName;?></td>
                <td>
                    <table style="width: 100%;">
                        <tbody>
                            <?php foreach ($aData as $type_customer => $qtyCustomer): ?>
                            <?php
                                $typeName       = isset(CmsFormatter::$CUSTOMER_BO_MOI[$type_customer]) ? CmsFormatter::$CUSTOMER_BO_MOI[$type_customer] : "($type_customer) Hộ Gia Đình";
                                $sQtyType       = "";
                                
                                if(isset($TYPE_CUSTOMER_QTY[$uid_login][$type_customer])):
                                    foreach($TYPE_CUSTOMER_QTY[$uid_login][$type_customer] as $materials_type_id => $qtyType):
                                        $value  = ActiveRecord::formatCurrency($qtyType);
                                        $weight = CmsFormatter::$MATERIAL_VALUE_KG[$materials_type_id];
                                        $sQtyType .= "Bình $weight KG:  $value <br>";

                                        $qtyToKg    = $qtyType * $weight;
                                        $sumQtyType += $qtyToKg;

                                    endforeach;
                                endif;
                            ?>
                            <tr>
                                <td class="item_b"><?php echo "KH ".$typeName.": ".ActiveRecord::formatCurrencyRound($qtyCustomer);?></td>
                                <td class="item_b "><?php echo $sQtyType;?></td>
                            </tr>
                            <?php endforeach; // end foreach ($aData ?>
                        </tbody>
                    </table>
                </td>
                <!--<td class="item_b item_r "><?php // echo $amountRemain > 0 ? ActiveRecord::formatCurrencyRound($amountRemain) : "";?></td>-->
                <td class="item_b item_r "><?php echo ActiveRecord::formatCurrencyRound($sumQtyType);?></td>
                <?php 
                    $sumAll += $sumQtyType;
                ?>
            </tr>
        <?php endforeach; // end  foreach ($TYPE_CUSTOMER_COUNT ?>        
        <tr>
            <td colspan="3" class="item_r item_b">Tổng</td>
            <td class="item_r item_b"><?php echo ActiveRecord::formatCurrencyRound($sumAll);?></td>
        </tr>
        <tr>
            <td colspan="3" class="item_r item_b">Tổng Gas dư</td>
            <td class="item_r item_b"><?php echo ActiveRecord::formatCurrencyRound($GAS_REMAIN);?></td>
        </tr>
        <tr>
            <td colspan="3" class="item_r item_b">Tổng sau khi trừ Gas dư</td>
            <td class="item_r item_b"><?php echo ActiveRecord::formatCurrencyRound($sumAll-$GAS_REMAIN);?></td>
        </tr>
    </tbody>
</table>

</div>

<script type="text/javascript">
    fnAddClassOddEven('items');
</script>
