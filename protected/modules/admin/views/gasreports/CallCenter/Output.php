<?php
$this->breadcrumbs=array(
    $this->pageTitle,
);
?>

<h1><?php echo $this->pageTitle;?></h1>
<div class="search-form" style="">
    <div class="wide form">

    <?php $form=$this->beginWidget('CActiveForm', array(
            'action'=>Yii::app()->createUrl($this->route),
            'method'=>'get',
    )); ?>
            <?php if(Yii::app()->user->hasFlash('successUpdate')): ?>
                <div class="success_div"><?php echo Yii::app()->user->getFlash('successUpdate');?></div>
            <?php endif; ?>
            <div class="row">
                <?php // echo Yii::t('translation', $form->label($model,'agent_id')); ?>
                <?php // echo $form->dropDownList($model,'agent_id', Users::getSelectByRoleFinal(ROLE_AGENT, array()),array('empty'=>'Select')); ?>
            </div>
                
            <div class="row">
            <?php echo $form->labelEx($model,'agent_id'); ?>
            <?php echo $form->hiddenField($model,'agent_id', array('class'=>'')); ?>
            <?php
                    // 1. limit search kh của sale
                    $url = Yii::app()->createAbsoluteUrl('admin/ajax/search_by_role', array('role'=> ROLE_AGENT));
                    // widget auto complete search user customer and supplier
                    $aDataAuto = array(
                        'model'=>$model,
                        'field_customer_id'=>'agent_id',
                        'url'=> $url,
                        'name_relation_user'=>'agent',
                        'ClassAdd' => 'w-400',
                        'field_autocomplete_name' => 'autocomplete_name',
                        'placeholder'=>'Nhập mã hoặc tên đại lý',
                    );
                    $this->widget('ext.SpaAutocompleteCustomer.SpaAutocompleteCustomer',
                        array('data'=>$aDataAuto));
                    ?>
            </div>
                
                
            <div class="row more_col">
                <div class="col1">
                        <?php echo Yii::t('translation', $form->label($model,'date_from')); ?>
                        <?php 
                            $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                                'model'=>$model,        
                                'attribute'=>'date_from',
                                'options'=>array(
                                    'showAnim'=>'fold',
                                    'dateFormat'=> MyFormat::$dateFormatSearch,
                                    'changeMonth' => true,
                                    'changeYear' => true,
                                    'showOn' => 'button',
                                    'buttonImage'=> Yii::app()->theme->baseUrl.'/admin/images/icon_calendar_r.gif',
                                    'buttonImageOnly'=> true,                                
                                ),        
                                'htmlOptions'=>array(
                                    'class'=>'w-16',
                                    'size'=>'16',
                                    'style'=>'float:left;',                               
                                    'readonly'=>1,
                                ),
                            ));
                        ?>     		
                </div>
                <div class="col2">
                        <?php echo Yii::t('translation', $form->label($model,'date_to')); ?>
                        <?php 
                            $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                                'model'=>$model,        
                                'attribute'=>'date_to',
                                'options'=>array(
                                    'showAnim'=>'fold',
                                    'dateFormat'=> MyFormat::$dateFormatSearch,
                                    'changeMonth' => true,
                                    'changeYear' => true,
                                    'showOn' => 'button',
                                    'buttonImage'=> Yii::app()->theme->baseUrl.'/admin/images/icon_calendar_r.gif',
                                    'buttonImageOnly'=> true,                                
                                ),        
                                'htmlOptions'=>array(
                                    'class'=>'w-16',
                                    'size'=>'16',
                                    'style'=>'float:left;',
                                    'readonly'=>1,
                                ),
                            ));
                        ?>
                </div>
            </div>
            <div class="clr"></div>
            <div class="row buttons" style="padding-left: 159px; margin:20px 0;">
                <?php $this->widget('bootstrap.widgets.TbButton', array(
                    'buttonType'=>'submit',
                    'label'=> 'Xem',
                    'type'=>'null', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
                    'size'=>'small', // null, 'large', 'small' or 'mini'
                    //'htmlOptions' => array('style' => 'margin-bottom: 10px; float: right;'),
                )); ?>
            </div>

    <?php $this->endWidget(); ?>

    </div><!-- wide form -->
</div><!-- search-form -->
<?php if(isset($aData['OUTPUT'])): ?>
    <?php include_once 'OutputCallCenterForm.php';?>
<?php endif; ?>

<style>
    .ui-datepicker-trigger { float: left;}
</style>

<script>
$(document).ready(function(){
    
});
</script>