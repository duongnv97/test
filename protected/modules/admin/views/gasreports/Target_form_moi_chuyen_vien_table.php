<?php foreach($aModelUser as $mUser): ?>
        <div class="box_320">
            <table class="hm_table items">
                <tbody>
                    <tr>
                        <td colspan="3" class="item_c item_b"><?php echo $mUser->first_name;?></td>
                    </tr>
                    <tr>
                        <td class="item_c w-130">Target</td>
                        <td class="item_c w-150">Thực Tế</td>
                        <td class="item_c w-80">Tỷ Lệ</td>
                    </tr>
                    <?php foreach(CmsFormatter::$CUSTOMER_BO_MOI as $type=>$label):?>
                    <?php
                        if($type != STORE_CARD_KH_MOI)  continue;
                        $uid = $mUser->id;
                        //*** for bình bò của KH mối
                        // CHANGE Now 20, 2014 không quy đổi 1 bình bò = 2 bình 12kg nữa, mà nhân ra số kg để tính %
                        $BINHBO_45 = (isset($data['OUTPUT_SALE_MOI_CHUYEN_VIEN_BO'][$uid][$type][MATERIAL_TYPE_BINHBO_45])?$data['OUTPUT_SALE_MOI_CHUYEN_VIEN_BO'][$uid][$type][MATERIAL_TYPE_BINHBO_45]:0)*1;
                        $BINHBO_50 = (isset($data['OUTPUT_SALE_MOI_CHUYEN_VIEN_BO'][$uid][$type][MATERIAL_TYPE_BINHBO_50])?$data['OUTPUT_SALE_MOI_CHUYEN_VIEN_BO'][$uid][$type][MATERIAL_TYPE_BINHBO_50]:0)*1;
                        $TextBBo = $BINHBO_45+$BINHBO_50;
                        $cty_thuc_te_bo_qty += ($BINHBO_45+$BINHBO_50);
                        // $BINHBO_EQUAL_BINH12 = $BINHBO*2;// vì bình bò ở đây ko nhân với kg mà chỉ là qty thuần
                        $BINHBO_45 = $BINHBO_45*CmsFormatter::$MATERIAL_VALUE_KG[MATERIAL_TYPE_BINHBO_45];
                        $BINHBO_50 = $BINHBO_50*CmsFormatter::$MATERIAL_VALUE_KG[MATERIAL_TYPE_BINHBO_50];
                        //*** for bình bò của KH mối                            
                        // CHANGE Now 20, 2014 không quy đổi 1 bình bò = 2 bình 12kg nữa, mà nhân ra số kg để tính %

                        $target=isset($data['aTargetEachMonth'][$uid][$type][$FOR_MONTH])?$data['aTargetEachMonth'][$uid][$type][$FOR_MONTH]:0;
                        $OUTPUT=isset($data['OUTPUT'][$uid][$type])?$data['OUTPUT'][$uid][$type]:0;
                        $REMAIN=isset($data['REMAIN'][$uid][$type])?$data['REMAIN'][$uid][$type]:0;
                        $real = $OUTPUT-$REMAIN;
                        $percent = $real;
    //                            $OUTPUT_BINHBO = ($BINHBO*2)*12;
                        $OUTPUT_BINHBO = $BINHBO_45+$BINHBO_50;//CHANGE Now 20, 2014

                        if($target){
                            $percent = ( ($real+$OUTPUT_BINHBO) / $target) * 100;
                        }
                        $qty = '';
                        if($real){
                            $qty = " = ".ActiveRecord::formatCurrency($real/12);
                        }
                        $target_qty = '';
                        if($target>0){
                            // Close Mar 10, 2015
//                            $target_qty = " = ".ActiveRecord::formatCurrency($target/12);
                        }
                        // for target cty
                        $cty_target_binh_moi_chuyen_vien += $target;
                        $cty_thuc_te_binh_moi_chuyen_vien += $real;
                        $cty_thuc_te_bo += $OUTPUT_BINHBO;
                        // for target cty // nếu có xuất excel thì sẽ dùng con số + cuối cùng đưa vào session của excel
                        // thì đỡ phải tính ở bên dưới
                    ?>

                    <tr class="color_type_<?php echo $type;?>">
                        <td class="item_r"><?php echo ActiveRecord::formatCurrency($target).$target_qty;?></td>
                        <td class="item_r"><?php echo ActiveRecord::formatCurrency($real).$qty;?></td>
                        <td class="item_c"><?php echo round($percent,1);?> %</td>
                    </tr>
                    <tr class="color_type_1">
                        <td class="item_r">&nbsp;</td>
                        <td class="item_r"><?php echo $OUTPUT_BINHBO!=0? (ActiveRecord::formatCurrency($OUTPUT_BINHBO)." = $TextBBo"):'';?></td>
                        <td class="item_r">&nbsp;</td>
                    </tr>
                    <?php endforeach;?>

                </tbody>
            </table>
        </div>                

    <?php endforeach; // end foreach($aModelUser as $mUser): ?>