 <?php if(count($AGENT_MODEL)): ?>
    <div id="tabs-AGENT_MODEL">
        <h1><?php echo $textInfo;?></h1>
        <div class="clearfix">
            <div class="color_type_1 color_note"></div> Gas Bình Bò
            <div class="clr"></div>
            <div class="color_type_2 color_note"></div> Gas Bình Mối
            <div class="clr"></div>
            <div class="color_type_3 color_note"></div> Hộ Gia Đình
        </div>
        <div class="clr"></div>
        <div class="box_350 sum_target_hm"></div>
        <div class="clr"></div>
        
        <?php         
            $hm_target_bo = 0 ;
            $hm_target_binh_moi = 0 ;
            $hm_target_ho_gd = 0 ;
            $hm_thuc_te_bo = 0;
            $hm_thuc_te_binh_moi = 0;
            $hm_thuc_te_ho_gd = 0;
            $key=0;
            $hmSumC1=0;$hmSumC2=0; // Dec 19, 2015 for sum col 
        ?>        
        <?php foreach($AGENT_MODEL as $province_id=>$aModelUser): ?>        
            <div class="wrap_province">
            <h1 class="l_padding_50 <?php echo ($key==0)?" margin_0 padding_0 ":''; $key++;?>">&nbsp;</h1>
            <div class="box_350 one_province"></div>
            <div class="clr"></div>
            <?php 
                $province_target_bo = 0 ;
                $province_target_binh_moi = 0 ;
                $province_target_ho_gd = 0 ;
                $province_thuc_te_bo = 0;
                $province_thuc_te_binh_moi = 0;
                $province_thuc_te_ho_gd = 0;
                $provinceSumC1=0;$provinceSumC2=0; // Dec 19, 2015 for sum col 
            ?>
            <?php foreach($aModelUser as $mUser): ?>
            <?php // nếu là role bị giới hạn đại lý theo dõi thì check  
                if($IS_ROLE_AGENT_LIMIT && !in_array($mUser->id, $ID_AGENT_LIMIT)){
                    continue;
                }
                $sumC1=0;$sumC2=0; // Dec 19, 2015 for sum col 
            ?>
            <div class="box_350">
                <table class="hm_table items">
                    <tbody>
                        <tr>
                            <td colspan="3" class="item_c item_b"><?php echo $mUser->first_name;?></td>
                        </tr>
                        <tr>
                            <td class="item_c w-140">Target</td>
                            <td class="item_c w-140">Thực Tế</td>
                            <td class="item_c w-70">Tỷ Lệ</td>
                        </tr>
                        <?php foreach(CmsFormatter::$CUSTOMER_BO_MOI as $type=>$label):?>
                        <?php
                            if($type == STORE_CARD_XE_RAO){
                                continue;
                            }
                            $uid = $mUser->id;
                            $target=isset($data['aTargetEachMonth'][$uid][$type][$FOR_MONTH])?$data['aTargetEachMonth'][$uid][$type][$FOR_MONTH]:0;
                            $OUTPUT=isset($data['OUTPUT'][$uid][$type])?$data['OUTPUT'][$uid][$type]:0;
                            $REMAIN=isset($data['REMAIN'][$uid][$type])?$data['REMAIN'][$uid][$type]:0;
                            $real = $OUTPUT-$REMAIN;
                            
                            // May 16, 2014 xử lý change cộng sản lượng bình bỏ của KH mối vào sản lương bình bò của dại lý
                            if($type==STORE_CARD_KH_MOI){
                                // chỗ này xử lý trừ đi bình bò của KH mối, sẽ cộng lên cho sản lượng của KH bình bò của đại lý
                                $OUTPUT_BINH_BO=isset($data['BINH_BO_KH_MOI'][$uid][$type])?$data['BINH_BO_KH_MOI'][$uid][$type]:0;
                                $real -=  $OUTPUT_BINH_BO ; // trừ sản lượng bình bò
                                $real +=  $REMAIN; // cộng ngược lại gas dư, vì sẽ không trừ đi gas dư ở đây
                                /* Aug 05, 2014 xử lý trừ sản lượng của sale chuyên viên và sale pttt*/
//                                $OUTPUT_KH_MOI_SALE_CV_PTTT=isset($data['KH_MOI_SALE_CV_PTTT'][$uid][$type])?$data['KH_MOI_SALE_CV_PTTT'][$uid][$type]:0;
//                                $real -=  $OUTPUT_KH_MOI_SALE_CV_PTTT ; // trừ sản lượng bình mối của sale chuyên viên vs pttt
                                // Now 20, 2014 Kiên kêu đóng lại, vì xem thấy nó lệch với bên daily sản lượng
                                /* Aug 05, 2014 xử lý trừ sản lượng của sale chuyên viên và sale pttt*/
                            }
                            if($type==STORE_CARD_KH_BINH_BO){
                                // chỗ này xử lý cộng vào sản lượng bình bò của KH mối, sẽ cộng lên cho sản lượng của KH bình bò của đại lý
                                $OUTPUT_BINH_BO=isset($data['BINH_BO_KH_MOI'][$uid][STORE_CARD_KH_MOI])?$data['BINH_BO_KH_MOI'][$uid][STORE_CARD_KH_MOI]:0;
                                // trừ đi lượng gas dư của KH mối mà bán bình bò
                                $REMAIN_KH_MOI=isset($data['REMAIN'][$uid][STORE_CARD_KH_MOI])?$data['REMAIN'][$uid][STORE_CARD_KH_MOI]:0;
                                $real += ( $OUTPUT_BINH_BO - $REMAIN_KH_MOI) ;
                            }
                            // May 16, 2014 xử lý change cộng sản lượng bình bỏ của KH mối vào sản lương bình bò của dại lý
                            
                            $qty = '';
                            $target_qty = '';
                            if($type==STORE_CARD_KH_MOI && $real){                                    
                                $qty = " = ".ActiveRecord::formatCurrency($real/12);
                            }
                            if($type==STORE_CARD_KH_MOI && $target>0){
                                $target_qty = " = ".ActiveRecord::formatCurrency($target/12);
                            }
                            $percent = $real;
                            if($target > 0){
                                $percent = ($real/$target)*100;
                            }
                            
                            if($type==STORE_CARD_KH_BINH_BO){
                                $province_target_bo += $target;
                                $province_thuc_te_bo += $real;
                                $hm_target_bo += $target;
                                $hm_thuc_te_bo += $real;
                            }else{
                                $province_target_binh_moi += $target;
                                $province_thuc_te_binh_moi += $real;
                                $hm_target_binh_moi += $target;
                                $hm_thuc_te_binh_moi += $real;
                            }
                            $sumC1 += $target;
                            $sumC2 += $real;
                            $provinceSumC1 += $target;
                            $provinceSumC2 += $real;
                        ?>

                        <tr class="color_type_<?php echo $type;?>">
                            <td class="item_r"><?php echo ActiveRecord::formatCurrency($target).$target_qty;?></td>
                            <td class="item_r"><?php echo ActiveRecord::formatCurrency(round($real))."$qty";?></td>
                            <td class="item_c"><?php echo round($percent,1);?> %</td>
                        </tr>
                        <?php endforeach;?>
                        <!-- for hộ gia đình -->
                        <?php
                            $uid = $mUser->id;
                            $target=isset($data['TARGET_HO_GIA_DINH'][$uid][$FOR_MONTH])?$data['TARGET_HO_GIA_DINH'][$uid][$FOR_MONTH]:0;
                            $real=isset($data[GasTargetMonthly::HGD_REAL][$uid])?$data[GasTargetMonthly::HGD_REAL][$uid]:0;
                            $amount = ActiveRecord::formatCurrency($real/12);
                            $percent = $real;
                            $target_qty = '';
                            if($target){
                                $percent = ($real/$target)*100;
                            }
                            if($target>0){
                                $target_qty = " = ".ActiveRecord::formatCurrency($target/12);
                            }
                            $province_target_ho_gd += $target ;
                            $province_thuc_te_ho_gd += $real ;
                            $hm_target_ho_gd += $target ;;
                            $hm_thuc_te_ho_gd += $real ;
                            
                            $sumC1 += $target;
                            $sumC2 += $real;
                            if($sumC1){
                                $percent_row_sum = ($sumC2/$sumC1)*100;
                            }
                            $provinceSumC1 += $target;
                            $provinceSumC2 += $real;
                        ?>
                        <tr class="color_type_3">
                            <td class="item_r"><?php echo ActiveRecord::formatCurrency($target).$target_qty;?></td>
                            <td class="item_r"><?php echo ActiveRecord::formatCurrency($real)." = $amount";?></td>
                            <td class="item_c"><?php echo round($percent,1);?> %</td>
                        </tr>
                        <!-- for hộ gia đình -->
                        <!-- for sum col -->
                        <tr class="color_type_4">
                            <td class="item_r item_b"><?php echo ActiveRecord::formatCurrency($sumC1);?></td>
                            <td class="item_r item_b"><?php echo ActiveRecord::formatCurrency(round($sumC2));?></td>
                            <td class="item_c item_b"><?php echo round($percent_row_sum,1);?> %</td>
                        </tr>
                        
                    </tbody>
                </table>
            </div>
            <?php endforeach; // end foreach($aModelUser as $mUser)?>
            <div class="clr"></div>
            <!-- sum one province -->
            <div class="box_310 one_province_hide display_none">
                <table class="hm_table items">
                    <tbody>
                        <tr>
                            <td colspan="3" class="item_c item_b"><h1 class="margin_0 padding_0"><?php echo isset($PROVINCE_MODEL[$province_id])?$PROVINCE_MODEL[$province_id]->name:"empty- $province_id";?></h1></td>
                        </tr>
                        <tr>
                            <td class="item_c w-140">Target</td>
                            <td class="item_c w-140">Thực Tế</td>
                            <td class="item_c w-70">Tỷ Lệ</td>
                        </tr>
                        <?php
                            $percent_bo = $province_thuc_te_bo;
                            if($province_target_bo){
                                $percent_bo = ($province_thuc_te_bo/$province_target_bo)*100;
                            }
                            $percent_binh_moi = $province_thuc_te_binh_moi;
                            if($province_target_binh_moi){
                                $percent_binh_moi = ($province_thuc_te_binh_moi/$province_target_binh_moi)*100;
                            }
                            $percent_ho_gd = $province_thuc_te_ho_gd;
                            if($province_target_ho_gd){
                                $percent_ho_gd = ($province_thuc_te_ho_gd/$province_target_ho_gd)*100;
                            }
                            
                            $amount_binh_moi = " = ".ActiveRecord::formatCurrency($province_thuc_te_binh_moi/12);
                            $target_qty_binh_moi = '';
                            if($province_target_binh_moi>0){
                                $target_qty_binh_moi = " = ".ActiveRecord::formatCurrency($province_target_binh_moi/12);
                            }
                            
                            $amount_ho_gd = " = ".ActiveRecord::formatCurrency($province_thuc_te_ho_gd/12);
                            $target_qty_ho_gd = '';
                            if($province_target_ho_gd>0){
                                $target_qty_ho_gd = " = ". ActiveRecord::formatCurrency($province_target_ho_gd/12);
                            }
                            if($provinceSumC1){
                                $province_percent_sum = ($provinceSumC2/$provinceSumC1)*100;
                            }
                            $hmSumC1 += $provinceSumC1;
                            $hmSumC2 += $provinceSumC2;
                        ?>

                        <tr class="color_type_1">
                            <td class="item_r"><?php echo ActiveRecord::formatCurrency($province_target_bo);?></td>
                            <td class="item_r"><?php echo ActiveRecord::formatCurrency(round($province_thuc_te_bo));?></td>
                            <td class="item_c"><?php echo round($percent_bo,1);?> %</td>
                        </tr>
                        <tr class="color_type_2">
                            <td class="item_r"><?php echo ActiveRecord::formatCurrency($province_target_binh_moi).$target_qty_binh_moi;?></td>
                            <td class="item_r"><?php echo ActiveRecord::formatCurrency($province_thuc_te_binh_moi).$amount_binh_moi;?></td>
                            <td class="item_c"><?php echo round($percent_binh_moi,1);?> %</td>
                        </tr>
                        <!-- for hộ gia đình -->
                        <tr class="color_type_3">
                            <td class="item_r"><?php echo ActiveRecord::formatCurrency($province_target_ho_gd).$target_qty_ho_gd;?></td>
                            <td class="item_r"><?php echo ActiveRecord::formatCurrency($province_thuc_te_ho_gd).$amount_ho_gd;?></td>
                            <td class="item_c"><?php echo round($percent_ho_gd,1);?> %</td>
                        </tr>
                        <!-- for hộ gia đình -->
                         <!-- for sum col -->
                        <tr class="color_type_4">
                            <td class="item_r item_b"><?php echo ActiveRecord::formatCurrency($provinceSumC1);?></td>
                            <td class="item_r item_b"><?php echo ActiveRecord::formatCurrency(round($provinceSumC2));?></td>
                            <td class="item_c item_b"><?php echo round($province_percent_sum,1);?> %</td>
                        </tr>
                    </tbody>
                </table>
            </div>            
            <div class="clr"></div>
            <!-- sum one province -->
            
            </div> <!-- end <div class="wrap_province"> -->
            
        <?php endforeach; // end <?php foreach($AGENT_MODEL as $province_id=>$aModelUser): ?>
        <div class="clr"></div>
        
        <!-- sum toàn bộ cty -->
        <div class="box_310 sum_target_hm_hide display_none">
            <table class="hm_table items">
                <tbody>
                    <tr>
                        <td colspan="3" class="item_c item_b"><h1 class="margin_0 padding_0">Tổng Hệ Thống Đại Lý</h1></td>
                    </tr>
                    <tr>
                        <td class="item_c w-140">Target</td>
                        <td class="item_c w-140">Thực Tế</td>
                        <td class="item_c w-70">Tỷ Lệ</td>
                    </tr>
                    <?php
                        $percent_bo = $hm_thuc_te_bo;
                        if($hm_target_bo){
                            $percent_bo = ($hm_thuc_te_bo/$hm_target_bo)*100;
                        }
                        $percent_binh_moi = $hm_thuc_te_binh_moi;
                        if($hm_target_binh_moi){
                            $percent_binh_moi = ($hm_thuc_te_binh_moi/$hm_target_binh_moi)*100;
                        }
                        $percent_ho_gd = $hm_thuc_te_ho_gd;
                        if($hm_target_ho_gd){
                            $percent_ho_gd = ($hm_thuc_te_ho_gd/$hm_target_ho_gd)*100;
                        }
                        
                        $amount_binh_moi = " = ".ActiveRecord::formatCurrency($hm_thuc_te_binh_moi/12);
                        $target_qty_binh_moi = '';
                        if($hm_target_binh_moi>0){
                            $target_qty_binh_moi = " = ".ActiveRecord::formatCurrency($hm_target_binh_moi/12);
                        }

                        $amount_ho_gd = " = ".ActiveRecord::formatCurrency($hm_thuc_te_ho_gd/12);
                        $target_qty_ho_gd = '';
                        if($hm_target_ho_gd>0){
                            $target_qty_ho_gd = " = ". ActiveRecord::formatCurrency($hm_target_ho_gd/12);
                        }
                        if($hmSumC1){
                            $hm_percent_sum = ($hmSumC2/$hmSumC1)*100;
                        }
                    ?>

                    <tr class="color_type_1">
                        <td class="item_r"><?php echo ActiveRecord::formatCurrency($hm_target_bo);?></td>
                        <td class="item_r"><?php echo ActiveRecord::formatCurrency(round($hm_thuc_te_bo));?></td>
                        <td class="item_c"><?php echo round($percent_bo,1);?> %</td>
                    </tr>
                    <tr class="color_type_2">
                        <td class="item_r"><?php echo ActiveRecord::formatCurrency($hm_target_binh_moi).$target_qty_binh_moi;?></td>
                        <td class="item_r"><?php echo ActiveRecord::formatCurrency($hm_thuc_te_binh_moi).$amount_binh_moi;?></td>
                        <td class="item_c"><?php echo round($percent_binh_moi,1);?> %</td>
                    </tr>
                    <!-- for hộ gia đình -->
                    <tr class="color_type_3">
                        <td class="item_r"><?php echo ActiveRecord::formatCurrency($hm_target_ho_gd).$target_qty_ho_gd;?></td>
                        <td class="item_r"><?php echo ActiveRecord::formatCurrency($hm_thuc_te_ho_gd).$amount_ho_gd;?></td>
                        <td class="item_c"><?php echo round($percent_ho_gd,1);?> %</td>
                    </tr>
                    <!-- for hộ gia đình -->
                    <!-- for sum col -->
                    <tr class="color_type_4">
                        <td class="item_r item_b"><?php echo ActiveRecord::formatCurrency($hmSumC1);?></td>
                        <td class="item_r item_b"><?php echo ActiveRecord::formatCurrency(round($hmSumC2));?></td>
                        <td class="item_c item_b"><?php echo round($hm_percent_sum,1);?> %</td>
                    </tr>
                </tbody>
            </table>
        </div>
        <!-- sum toàn bộ cty -->
        
    </div>
<?php endif; // end if(count($AGENT_MODEL) ?>