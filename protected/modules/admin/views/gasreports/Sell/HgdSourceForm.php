<?php 
$mAppCache          = new AppCache();
$aAgent             = $mAppCache->getAgentListdata();
$ARR_DAYS           = isset($rData['ARR_DAYS']) ? $rData['ARR_DAYS'] : [];
$aAgentId           = isset($rData['AGENT_ID']) ? $rData['AGENT_ID'] : [];
$CUSTOMERS          = isset($rData['CUSTOMERS']) ? $rData['CUSTOMERS'] : [];
$AGENT_SUM          = isset($rData['AGENT_SUM']) ? $rData['AGENT_SUM'] : [];
$DAY_SUM            = isset($rData['DAY_SUM']) ? $rData['DAY_SUM'] : [];
$aTemp          = ['Loại KH', 'KH Mới', 'KH Cũ'];
$aDataChart[]   = $aTemp;
$tempChart      = []; $index = 1;
$sumAll[Sell::CUSTOMER_NEW] = 0;
$sumAll[Sell::CUSTOMER_OLD] = 0;

foreach ($ARR_DAYS as $days):
    $temp = explode('-', $days);
    $customerNewDay = isset($DAY_SUM[$days][Sell::CUSTOMER_NEW]) ? $DAY_SUM[$days][Sell::CUSTOMER_NEW] : '';
    $customerOldDay = isset($DAY_SUM[$days][Sell::CUSTOMER_OLD]) ? $DAY_SUM[$days][Sell::CUSTOMER_OLD] : '';
    if(!isset($tempChart[$temp[2]])){
        $tempChart[$temp[2]] = array($temp[2]);
    }
    $tempChart[$temp[2]][] = $customerNewDay*1;
    $tempChart[$temp[2]][] = $customerOldDay*1;
    $sumAll[Sell::CUSTOMER_NEW] += $customerNewDay;
    $sumAll[Sell::CUSTOMER_OLD] += $customerOldDay;
    
endforeach;
foreach ($tempChart as $aValue){
    $aDataChart[] = $aValue; 
}
$js_array = json_encode($aDataChart); 
?>
<p class="item_b">Chú thích: KH mới / KH cũ</p>
<div class="grid-view display_none">
<table class="hm_table items freezetablecolumns" id="freezetablecolumns_inventory">
    <thead>
        <tr>
            <th class="item_c w-20" >#</th>
            <th class="item_c w-180" >Đại lý / Ngày</th>
            <th class="item_c w-50" >Tổng</th>
            <?php foreach ($ARR_DAYS as $days): ?>
                <?php $temp = explode('-', $days);?>
                <th class="item_c w-50"><?php echo $temp[2]; ?></th>
            <?php endforeach; ?>
        </tr>
    </thead>
    <tbody>
    <tr>
        <td></td>
        <td></td>
        <td class="item_c item_b">
            <?php 
                echo ActiveRecord::formatCurrency($sumAll[Sell::CUSTOMER_NEW]).'/'.ActiveRecord::formatCurrency($sumAll[Sell::CUSTOMER_OLD]);
            ?>
        </td>
        <?php foreach ($ARR_DAYS as $days): ?>
            <?php
                $customerNewDay = isset($DAY_SUM[$days][Sell::CUSTOMER_NEW]) ? $DAY_SUM[$days][Sell::CUSTOMER_NEW] : '';
                $customerOldDay = isset($DAY_SUM[$days][Sell::CUSTOMER_OLD]) ? $DAY_SUM[$days][Sell::CUSTOMER_OLD] : '';
                $text = $customerNewDay.'/'.$customerOldDay;
            ?>
            <td class="item_c item_b"><?php echo $text; ?></td>
        <?php endforeach; ?>
    </tr>
    <?php foreach ($aAgentId as $agent_id): ?>
    <?php 
        $agentName = isset($aAgent[$agent_id]) ? $aAgent[$agent_id] : $agent_id;
        $customerNew = isset($AGENT_SUM[$agent_id][Sell::CUSTOMER_NEW]) ? $AGENT_SUM[$agent_id][Sell::CUSTOMER_NEW] : '';
        $customerOld = isset($AGENT_SUM[$agent_id][Sell::CUSTOMER_OLD]) ? $AGENT_SUM[$agent_id][Sell::CUSTOMER_OLD] : '';
        $text = $customerNew.'/'.$customerOld;
    ?>
        <tr>
            <td class="item_b item_c"><?php echo $index++;?></td>
            <td class="item_b"><?php echo $agentName;?></td>
            <td class="item_b item_c"><?php echo $text;?></td>
            <?php foreach ($ARR_DAYS as $days): ?>
            <?php 
                $customerNew = isset($CUSTOMERS[$agent_id][$days][Sell::CUSTOMER_NEW]) ? $CUSTOMERS[$agent_id][$days][Sell::CUSTOMER_NEW] : '';
                $customerOld = isset($CUSTOMERS[$agent_id][$days][Sell::CUSTOMER_OLD]) ? $CUSTOMERS[$agent_id][$days][Sell::CUSTOMER_OLD] : '';
                $text = $customerNew.'/'.$customerOld;
            ?>
            <td class="item_c"><?php echo $text; ?></td>
        <?php endforeach; ?>
        </tr>
    <?php endforeach; // end foreach ($aAgentId ?>
    </tbody>
</table>
    
    

</div>
<?php Yii::app()->clientScript->registerCoreScript('jquery.ui'); ?>
<?php
$gridCss = Yii::app()->getAssetManager()->publish(Yii::getPathOfAlias('zii.widgets.assets')).'/gridview/styles.css';
Yii::app()->getClientScript()->registerCssFile($gridCss);
?>
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/scroll/jquery.freezetablecolumns.1.1.js"></script> 
<script>
$(function() {
//    $( "#tabs" ).tabs();
    $(".items > tbody > tr:odd").addClass("odd");
    $(".items > tbody > tr:even").addClass("even");   
    fnTabMonthClick();
});

function fnTabMonthClick(){
     $('.freezetablecolumns').each(function(){
        var id_table = $(this).attr('id');
        $('#'+id_table).freezeTableColumns({
            width:       <?php echo Users::WIDTH_freezetable;?>,   // required - bt 1100
            height:      400,   // required
            numFrozen:   3,     // optional
            frozenWidth: 290,   // optional
            clearWidths: true  // optional
        });   
    });
}
$(window).load(function(){
    var index = 1;
    $('.freezetablecolumns').each(function(){
        if(index==1)
            $(this).closest('div.grid-view').show();
        index++;
    });    
    fnAddClassOddEven('items');
});

</script>


<div class="float_l" style="width: 1200px">
    <div id="curve_chart" style="width: 1100px; height: 500px"></div>
</div>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script type="text/javascript">
    fnAddClassOddEven('items');
    google.charts.load("current", {packages:["corechart"]});
    google.charts.setOnLoadCallback(drawChart);
    function drawChart() {
//      var data = google.visualization.arrayToDataTable([
//        ['Task', 'Hours per Day'],
//        ['Work',     11],
//        ['Eat',      2],
//        ['Commute',  2],
//        ['Watch TV', 202],
//        ['Sleep',    7]
//      ]);
      
      var data = google.visualization.arrayToDataTable(<?php echo $js_array;?>);;
      
      var options = {
          title: '',
          curveType: 'function',
          legend: { position: 'center' }
        };
//      var options = {
//        legend: {alignment: 'center'}
//      };

      var chart = new google.visualization.LineChart(document.getElementById('curve_chart'));
      chart.draw(data, options);
    }
  </script>