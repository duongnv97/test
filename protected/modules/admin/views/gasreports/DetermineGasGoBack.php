<?php
$this->breadcrumbs=array(
	'Xác định bình quay về của PTTT',
);?>

<h1>Xác định bình quay về của PTTT</h1>
<div class="search-form" style="">
    <div class="wide form">

    <?php $form=$this->beginWidget('CActiveForm', array(
            'action'=>Yii::app()->createUrl($this->route),
            'method'=>'post',
    )); ?>
            <?php if(Yii::app()->user->hasFlash('successUpdate')): ?>
                <div class="success_div"><?php echo Yii::app()->user->getFlash('successUpdate');?></div>         
            <?php endif; ?>    
                
            <div class="row more_col">
                <div class="col1">
                    <?php echo Yii::t('translation', $form->label($model,'date_from')); ?>
                    <?php 
                        $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                            'model'=>$model,        
                            'attribute'=>'date_from',
                            'options'=>array(
                                'showAnim'=>'fold',
                                'dateFormat'=> MyFormat::$dateFormatSearch,       
                                'maxDate'=> '0',
                                'changeMonth' => true,
                                'changeYear' => true,
                                'showOn' => 'button',
                                'buttonImage'=> Yii::app()->theme->baseUrl.'/admin/images/icon_calendar_r.gif',
                                'buttonImageOnly'=> true,                                
                            ),        
                            'htmlOptions'=>array(
                                'class'=>'w-16',
                                'size'=>'16',
                                'style'=>'height:20px;float:left;',                               
                            ),
                        ));
                    ?>     		
                </div>
                
                <div class="col2">
                    <?php echo Yii::t('translation', $form->label($model,'date_to')); ?>
                    <?php 
                        $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                            'model'=>$model,        
                            'attribute'=>'date_to',
                            'options'=>array(
                                'showAnim'=>'fold',
                                'dateFormat'=> MyFormat::$dateFormatSearch,       
                                'maxDate'=> '0',
                                'changeMonth' => true,
                                'changeYear' => true,
                                'showOn' => 'button',
                                'buttonImage'=> Yii::app()->theme->baseUrl.'/admin/images/icon_calendar_r.gif',
                                'buttonImageOnly'=> true,                                
                            ),        
                            'htmlOptions'=>array(
                                'class'=>'w-16',
                                'size'=>'16',
                                'style'=>'height:20px;float:left;',                               
                            ),
                        ));
                    ?>    
                </div>
            </div>    

        <div class="row">
            <?php echo $form->labelEx($model,'agent_id'); ?>
            <?php echo $form->dropDownList($model,'agent_id', Users::getArrUserByRoleNotCheck(ROLE_AGENT),array('style'=>'','empty'=>'Select')); ?>
            <?php echo $form->error($model,'agent_id'); ?>
        </div> 
                
            
        <div class="row">
            <?php echo Yii::t('translation', $form->labelEx($model,'maintain_employee_id')); ?>
            <?php echo $form->dropDownList($model,'maintain_employee_id', Users::getSelectByRoleForAgent($model->maintain_employee_id, ONE_MONITORING_MARKET_DEVELOPMENT, '', array('status'=>1, 'get_all'=>1)),array('class'=>'category_ajax', 'style'=>'','empty'=>'Select')); ?>		
        </div>	
    
                
                
            <div class="row buttons" style="padding-left: 159px; margin:20px 0;">
                <?php $this->widget('bootstrap.widgets.TbButton', array(
                'buttonType'=>'submit',
                'label'=>Yii::t('translation','Xem Thống Kê'),
                'type'=>'null', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
                'size'=>'small', // null, 'large', 'small' or 'mini'
                //'htmlOptions' => array('style' => 'margin-bottom: 10px; float: right;'),
            )); ?>	
            <?php // if(ControllerActionsName::isAccessAction('statistic_maintain_export_excel', $actions)):?>
            <input class='statistic_export_excel' next='<?php echo Yii::app()->createAbsoluteUrl('admin/ajax/determineGasGoBack_export_excel');?>' type='button' value='Xuất Thống Kê Hiện Tại Ra Excel'>
            </div>

    <?php $this->endWidget(); ?>

    </div><!-- wide form -->
</div><!-- search-form -->

<style>
    .ui-datepicker-trigger { float: left;}
</style>

<script>
$(document).ready(function(){
    $('.form').find('button:submit').click(function(){        
        $.blockUI({ overlayCSS: { backgroundColor: '#fff' } }); 
    });   
    $('.statistic_export_excel').click(function(){
            window.location = $(this).attr('next');
    });    
});
</script>    
<?php include 'DetermineGasGoBack_form.php'; ?>