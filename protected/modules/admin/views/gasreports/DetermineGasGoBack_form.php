<?php if(isset($data['TOTAL_ALL']) && count($data['TOTAL_ALL'])) :?>
<?php Yii::app()->clientScript->registerCoreScript('jquery.ui'); ?>
<?php
$gridCss = Yii::app()->getAssetManager()->publish(Yii::getPathOfAlias('zii.widgets.assets')).'/gridview/styles.css';
Yii::app()->getClientScript()->registerCssFile($gridCss);
?>
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/jquery.floatThead.min.js"></script>    
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/underscore-min.js"></script>    

<script>
$(function() {
    $( "#tabs" ).tabs();
    $(".items > tbody > tr:odd").addClass("odd");
    $(".items > tbody > tr:even").addClass("even");   
    fnTabMonthClick();
});

function fnTabMonthClick(){
     // floatThead for first tab
     var index=1;
     $('.grid-view').each(function(){
        if(index==1){
            var tableItem = $(this).find('.items').eq(0);
            $(this).find('.items').floatThead();
        }
        index++;
    });    
    // floatThead for first tab
}

</script>

<?php
$TOTAL_ALL = $data['TOTAL_ALL'];
$TOTAL_USING_GAS_HM = isset($data['TOTAL_USING_GAS_HM'])?$data['TOTAL_USING_GAS_HM']:array();
$TOTAL_QTY_BIGGER_2 = isset($data['TOTAL_QTY_BIGGER_2'])?$data['TOTAL_QTY_BIGGER_2']:array();
$TOTAL_QTY_BIGGER_2_COUNT_CUSTOMER = isset($data['TOTAL_QTY_BIGGER_2_COUNT_CUSTOMER'])?$data['TOTAL_QTY_BIGGER_2_COUNT_CUSTOMER']:array();
$ARR_CUSTOMER_ID = isset($data['ARR_CUSTOMER_ID'])?$data['ARR_CUSTOMER_ID']:array();

$MONITOR_EMPLOYEE = Users::getArrIdObjectMonitorAndMarketEmployee(array());
$AGENT_MODEL = Users::getArrObjectUserByRoleHaveOrder (ROLE_AGENT, 'code_account');
$cmsFormater = new CmsFormatter();
$MODEL_CUSTOMER = isset($data['CUSTOMER_ID_BIGGER_2_COUNT'])?Users::getArrayModelByArrayId($data['CUSTOMER_ID_BIGGER_2_COUNT']):array();

$data['MONITOR_EMPLOYEE'] = $MONITOR_EMPLOYEE;
$data['AGENT_MODEL'] = $AGENT_MODEL;
$data['MODEL_CUSTOMER'] = $MODEL_CUSTOMER;
$data['MODEL'] = $model;
$_SESSION['data-excel-gas-go-back'] = $data;	

$sum_total_all = 0;
$sum_using_gas_hm = 0;
$sum_qty_bigger_2 = 0;
$sum_qty_bigger_2_count_customer = 0;
$sum_remain = 0;
?>

<div class="title_table_statistic">Xác định bình quay về của PTTT 
        Từ Ngày <?php echo $model->date_from;?> đến ngày <?php echo $model->date_to;?>
</div>
<div class="grid-view grid-view-scroll">
    <!--<table class="hm_table table_statistic items table_month_1" style="">-->
    <table class="hm_table items " style=" width: 800px;">
        <thead>
            <tr>                               
                <th class="w-100 item_c">NV PTTT</th>
                <th class="w-100">Đại Lý</th>
                <th class="w-50">Tổng BQV</th>               
                <th class="w-50">Dùng Gas HM</th>
                <th class="w-50">Số Bình Mua >= 2</th>
                <th class="w-150">SỐ KH MUA >= 2</th>
                <th class="w-50">SỐ Còn Lại</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach($TOTAL_ALL as $maintain_employee_id=>$item_agent):?>
                <?php foreach($item_agent as $agent_id=>$item_total_all):?>
                    <?php
                        $name_employer = isset($MONITOR_EMPLOYEE[$maintain_employee_id])?$MONITOR_EMPLOYEE[$maintain_employee_id]->first_name:"";
                        $name_agent = isset($AGENT_MODEL[$agent_id])?$AGENT_MODEL[$agent_id]->first_name:"";
                        $total_using_gas_hm = isset($TOTAL_USING_GAS_HM[$maintain_employee_id][$agent_id])?$TOTAL_USING_GAS_HM[$maintain_employee_id][$agent_id]:0;
                        $qty_bigger_2 = isset($TOTAL_QTY_BIGGER_2[$maintain_employee_id][$agent_id])?$TOTAL_QTY_BIGGER_2[$maintain_employee_id][$agent_id]:0;
                        $qty_bigger_2_customer = isset($TOTAL_QTY_BIGGER_2_COUNT_CUSTOMER[$maintain_employee_id][$agent_id])?$TOTAL_QTY_BIGGER_2_COUNT_CUSTOMER[$maintain_employee_id][$agent_id]:0;
                        $remain = $item_total_all-$total_using_gas_hm-$qty_bigger_2;
                        $infoC = '<div class="item_l">';
                        if(isset($ARR_CUSTOMER_ID[$maintain_employee_id][$agent_id])){
                            foreach($ARR_CUSTOMER_ID[$maintain_employee_id][$agent_id] as $customer_id){
                                $infoC .= "<br>".$MODEL_CUSTOMER[$customer_id]->first_name." - ".$MODEL_CUSTOMER[$customer_id]->phone;
                            }
                        }                      
                        $infoC .="</div>";
                        $sum_total_all+=$item_total_all;
                        $sum_using_gas_hm+=$total_using_gas_hm;
                        $sum_qty_bigger_2+=$qty_bigger_2;
                        $sum_qty_bigger_2_count_customer+=$qty_bigger_2_customer;
                        $sum_remain+=$remain;
                    ?>
                    <tr>
                        <td class=""><?php echo $name_employer; ?></td>
                        <td class=""><?php echo $name_agent; ?></td>
                        <td class="item_r"><?php echo ($item_total_all!=0)?ActiveRecord::formatCurrency($item_total_all):""; ?></td>
                        <td class="item_r"><?php echo ($total_using_gas_hm!=0)?ActiveRecord::formatCurrency($total_using_gas_hm):""; ?></td>
                        <td class="item_r"><?php echo ($qty_bigger_2!=0)?ActiveRecord::formatCurrency($qty_bigger_2):""; ?></td>
                        <td class="item_r"><?php echo (($qty_bigger_2_customer!=0)?ActiveRecord::formatCurrency($qty_bigger_2_customer):"").$infoC; ?></td>
                        <td class="item_r"><?php echo ($remain!=0)?ActiveRecord::formatCurrency($remain):""; ?></td>
                    <tr>
                <?php endforeach; // end foreach($item_agent as $agent_id=>$item_ ?>
            <?php endforeach;?>
        </tbody>
        <tfoot>
            <tr>
                <td colspan="2" class="item_r item_b">Tổng Cộng</td>
                <td class="item_r item_b"><?php echo ($sum_total_all!=0)?ActiveRecord::formatCurrency($sum_total_all):""; ?></td>
                <td class="item_r item_b"><?php echo ($sum_using_gas_hm!=0)?ActiveRecord::formatCurrency($sum_using_gas_hm):""; ?></td>
                <td class="item_r item_b"><?php echo ($sum_qty_bigger_2!=0)?ActiveRecord::formatCurrency($sum_qty_bigger_2):""; ?></td>
                <td class="item_r item_b"><?php echo (($sum_qty_bigger_2_count_customer!=0)?ActiveRecord::formatCurrency($sum_qty_bigger_2_count_customer):""); ?></td>
                <td class="item_r item_b"><?php echo ($sum_remain!=0)?ActiveRecord::formatCurrency($sum_remain):""; ?></td>
            <tr>
        </tfoot>

    </table><!-- end <table class="table_statistic items table_mon-->
</div> <!-- end <div class="grid-view-scroll">-->

<?php endif; // end if(count($data)) ?>
