<?php
$this->breadcrumbs=array(
	'Xem báo cáo tồn kho hệ thống',
);
$ListOptionAgent = Users::getSelectByRoleFinal(ROLE_AGENT, array('GetActive'=>1, 'gender'=>$aTypeAgent, 'order'=>'t.gender DESC'));
$ListOptionMaterialType = GasMaterialsType::getAllItem();
$ShortNameAgent = Users::formatShorNameAgent($ListOptionAgent);
?>

<h1>Xem báo cáo tồn kho hệ thống</h1>
<div class="search-form" style="">
    <div class="wide form">

    <?php $form=$this->beginWidget('CActiveForm', array(
            'action'=>Yii::app()->createUrl($this->route),
            'method'=>'post',
    )); ?>
            <?php if(Yii::app()->user->hasFlash('successUpdate')): ?>
                <div class="success_div"><?php echo Yii::app()->user->getFlash('successUpdate');?></div>         
            <?php endif; ?>    
                
                
                
            <div class="row more_col">
                <div class="col1">
                        <?php echo Yii::t('translation', $form->label($model,'date_to')); ?>
                        <?php 
                            $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                                'model'=>$model,        
                                'attribute'=>'date_to',
                                'options'=>array(
                                    'showAnim'=>'fold',
                                    'dateFormat'=> MyFormat::$dateFormatSearch,
        //                            'minDate'=> '0',
                                    'maxDate'=> '0',
                                    'changeMonth' => true,
                                    'changeYear' => true,
                                    'yearRange'=> date('Y').":".date('Y'),
                                    'showOn' => 'button',
                                    'buttonImage'=> Yii::app()->theme->baseUrl.'/admin/images/icon_calendar_r.gif',
                                    'buttonImageOnly'=> true,                                
                                ),        
                                'htmlOptions'=>array(
                                    'class'=>'w-16',
                                    'size'=>'16',
                                    'style'=>'float:left;',                               
                                    'readonly'=>1,
                                ),
                            ));
                        ?>     		
                </div>                
            </div>
                
            <div class="row">
                <?php echo $form->label($model,'ext_materials_type_id'); ?>
                <div class="fix-label">
                    <?php
                       $this->widget('ext.multiselect.JMultiSelect',array(
                             'model'=>$model,
                             'attribute'=>'ext_materials_type_id',
                             'data'=>  GasMaterialsType::getAllItem(),
                             // additional javascript options for the MultiSelect plugin
//                             'options'=>array('header' => false,),
                             'options'=>array('selectedList' => 15,),
                             // additional style
                             'htmlOptions'=>array('style' => 'width: 345px;'),
                       ));    
                   ?>
                </div> 
            </div>                
            
            <?php if(Yii::app()->user->role_id!=ROLE_SUB_USER_AGENT):?>
            <div class="row">
                <?php echo Yii::t('translation', $form->label($model,'agent_id')); ?>
                <?php // echo $form->dropDownList($model,'agent_id', Users::getSelectByRoleFinal(ROLE_AGENT, array('gender'=>Users::IS_AGENT)),array('class'=>'multiselect', 'multiple'=>'multiple')); ?>
                <div class="fix-label">
                    <?php
                       $this->widget('ext.multiselect.JMultiSelect',array(
                             'model'=>$model,
                             'attribute'=>'agent_id',
//                             'data'=>  Users::getSelectByRoleFinal(ROLE_AGENT, array('gender'=>Users::IS_AGENT)),
                             'data'=> $ListOptionAgent,
                             // additional javascript options for the MultiSelect plugin
//                             'options'=>array('header' => false,),
                             'options'=>array(),
                             // additional style
                             'htmlOptions'=>array('style' => 'width: 345px;'),
                       ));    
                   ?>
                </div> 
            </div>
            <?php endif;?>    
                
            <div class="row buttons" style="padding-left: 159px; margin:20px 0;">
                <?php $this->widget('bootstrap.widgets.TbButton', array(
                'buttonType'=>'submit',
                'label'=>Yii::t('translation','Xem Thống Kê'),
                'type'=>'null', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
                'size'=>'small', // null, 'large', 'small' or 'mini'
                //'htmlOptions' => array('style' => 'margin-bottom: 10px; float: right;'),
            )); ?>	
            <?php // if(ControllerActionsName::isAccessAction('statistic_maintain_export_excel', $actions)):?>
            <?php if(0):?>
                    <input class='statistic_maintain_export_excel' next='<?php echo Yii::app()->createAbsoluteUrl('admin/gasmaintain/statistic_maintain_export_excel');?>' type='button' value='Xuất Thống Kê Hiện Tại Ra Excel'>
            <?php endif;?>

            </div>

    <?php $this->endWidget(); ?>

    </div><!-- wide form -->
</div><!-- search-form -->

<style>
    .ui-datepicker-trigger { float: left;}
</style>

<script>
$(document).ready(function(){
    $('.form').find('button:submit').click(function(){        
        $.blockUI({ overlayCSS: { backgroundColor: '#fff' } }); 
    });   
});
</script>    
<?php include 'Inventory_form.php'; ?>