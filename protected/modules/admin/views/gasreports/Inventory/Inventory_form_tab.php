<div class="grid-view display_none">
    <div class="title_table_statistic">TỒN KHO HỆ THỐNG ĐẾN NGÀY: <?php echo $model->date_to;?></div>
    <div class="">
        <table id="freezetablecolumns_inventory" class="freezetablecolumns items">
            <thead>
                <tr  style="height: 60px;">                               
                    <th class="w-20 item_c ">STT</th>
                    <th class="w-40 ">Mã VT</th>
                    <th class="w-150 ">Tên Vật Tư</th>
                    <th class="w-30 ">ĐVT</th>
                    <th class="w-60 ">Tổng Tồn</th>
                    <?php // foreach($ListOptionAgent as $agent_id=>$agent_name): ?>
                    <?php foreach($ShortNameAgent as $agent_id=>$agent_name): ?>
                    <th class="w-60 item_c " style="">
                        <?php // echo $agent_name;?>
                        <?php echo $agent_name; ?>
                    </th>
                    <?php endforeach;?>
                </tr>
            </thead>
            <tbody>
                <?php foreach($ListOptionMaterialType as $materials_type_id=>$materials_type_name):?>
                    <?php if(isset($MODEL_MATERIAL_TYPE[$materials_type_id])):?>
                    <?php $sumType = 0;?>
                    <tr class="h_30">
                        <td class="item_b f_size_15" colspan="4"><?php echo $materials_type_name; ?></td>
                        <td class="f_size_15 sumType<?php echo $materials_type_id;?> item_b item_r"></td>
                        <!--<td class="f_size_15" colspan="<?php echo $countAgent;?>">&nbsp;</td>-->
                        <?php foreach($ListOptionAgent as $agent_id=>$agent_name): ?>
                            <td class="f_size_15 item_c item_b SumColMaterialTypeAgent<?php echo $materials_type_id."_$agent_id";?>"></td>
                        <?php endforeach;?>
                    </tr>
                    <?php foreach($MODEL_MATERIAL_TYPE[$materials_type_id] as $key=>$mMaterial): ?>
                        <?php $sumRow = 0;?>
                        <tr class="h_30">
                            <td class="item_c "><?php echo $key+1; ?></td>
                            <td class="item_c"><?php echo $mMaterial->materials_no; ?></td>
                            <td class=""><?php echo $mMaterial->name; ?></td>
                            <td class="item_c"><?php echo $mMaterial->unit; ?></td>
                            <td class="f_size_15 item_r sum_row item_b">&nbsp;</td>
                            <?php foreach($ListOptionAgent as $agent_id=>$agent_name): ?>
                            <td class="f_size_15 item_c" style="">
                                <?php
                                    $openingBalance = isset($OPENING_BALANCE_YEAR_BEFORE[$agent_id][$mMaterial->id])?$OPENING_BALANCE_YEAR_BEFORE[$agent_id][$mMaterial->id]:0;
                                    $a_import = isset($IMPORT[$agent_id][$mMaterial->id])?$IMPORT[$agent_id][$mMaterial->id]:0;
                                    $a_export = isset($EXPORT[$agent_id][$mMaterial->id])?$EXPORT[$agent_id][$mMaterial->id]:0;
                                    $inventory = $openingBalance+$a_import-$a_export;
                                    $sumRow+=$inventory;
                                    $sumType+=$inventory;
                                    if(isset($SumColMaterialType[$materials_type_id][$agent_id]))
                                        $SumColMaterialType[$materials_type_id][$agent_id] += $inventory;
                                    else
                                        $SumColMaterialType[$materials_type_id][$agent_id] = $inventory;
                                    echo $inventory!=0?ActiveRecord::formatCurrency($inventory):'&nbsp;';
                                ?>
                            </td>
                            <?php endforeach;?>
                            <td style="display: none;" class="sum_row_hide <?php echo $sumRow==0?"class_remove_row":'';?>"><?php echo $sumRow!=0?ActiveRecord::formatCurrency($sumRow):'&nbsp;';?></td>
                        </tr>
                    <?php endforeach; // end foreach($MODEL_MATERIAL_TYPE[$materials_type_id] as $key ?>
                    <span style="display: none;" class="sumTypeHide" link="sumType<?php echo $materials_type_id;?>"><?php echo $sumType!=0?ActiveRecord::formatCurrency($sumType):'&nbsp;';?></span>
                    <?php endif; // end if(isset($MODEL_MATERIAL_TYPE[$m ?>
                <?php endforeach; // end foreach($ListOptionMaterialType as $materials_type_id=>?>
                
            </tbody>
        </table>
    </div>
</div>

<script>
    $(window).load(function(){
        // xử lý cộng tổng loại vật tư theo từng đại lý
        <?php foreach($SumColMaterialType as $materials_type_id=>$aAgentId): ?>
            <?php foreach($aAgentId as $agent_id=>$sumOneColMaterialType): ?>
                $('.SumColMaterialTypeAgent<?php echo $materials_type_id."_$agent_id";?>').html('<?php echo $sumOneColMaterialType!=0?ActiveRecord::formatCurrency($sumOneColMaterialType):'&nbsp;';?>');
            <?php endforeach;?>
        <?php endforeach;?>       
    });
</script>
