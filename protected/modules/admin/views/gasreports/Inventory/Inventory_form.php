<?php if(count($data) && isset($data['OPENING_BALANCE_YEAR_BEFORE'])) :?>
<?php Yii::app()->clientScript->registerCoreScript('jquery.ui'); ?>
<?php
$gridCss = Yii::app()->getAssetManager()->publish(Yii::getPathOfAlias('zii.widgets.assets')).'/gridview/styles.css';
Yii::app()->getClientScript()->registerCssFile($gridCss);
?>
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/scroll/jquery.freezetablecolumns.1.1.js"></script> 
<?php 
$OPENING_BALANCE_YEAR_BEFORE = isset($data['OPENING_BALANCE_YEAR_BEFORE'])?$data['OPENING_BALANCE_YEAR_BEFORE']:array();
$IMPORT = isset($data['IMPORT'])?$data['IMPORT']:array();
$EXPORT = isset($data['EXPORT'])?$data['EXPORT']:array();
$MODEL_MATERIAL_TYPE = isset($data['MODEL_MATERIAL_TYPE'])?$data['MODEL_MATERIAL_TYPE']:array();
$countAgent = count($ListOptionAgent);
$SumColMaterialType = array();
?>

<?php include 'Inventory_form_tab.php';?>

<script>
$(function() {
    $( "#tabs" ).tabs();
    fnUpdateSumRow();
    fnTabMonthClick();
});    
    
function fnTabMonthClick(){
    $('.freezetablecolumns').each(function(){
        var id_table = $(this).attr('id');
        $('#'+id_table).freezeTableColumns({
                width:       <?php echo Users::WIDTH_freezetable;?>,   // required - bt 1100
                height:      400,   // required
                numFrozen:   5,     // optional
                frozenWidth: 355,   // optional
                clearWidths: true  // optional
              });   
    });
}  

function fnUpdateSumRow(){
    $('.class_remove_row').each(function(){
        $(this).closest('tr').remove();
    });
    
    $('.sum_row_hide').each(function(){
        var tr = $(this).closest('tr');
        tr.find('.sum_row').text($(this).text());
    });
    
    $('.sumTypeHide').each(function(){
        var class_update = $(this).attr('link');
        $('.'+class_update).text($(this).text());
    });
    
    
}
    
$(window).load(function(){
    var index = 1;
    $('.freezetablecolumns').each(function(){
        if(index==1)
            $(this).closest('div.grid-view').show();
        index++;
    });    
    fnAddClassOddEven('items');
});
</script>

<?php endif; ?>

