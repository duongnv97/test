<?php 
    $sumOneCarCol = array();
    $sumOneCarAll       = 0;
?>
<table class="items hm_table f_size_14">
    <thead>
        <tr style="">
            <th>#</th>
            <?php foreach($aDateLoop as $date_delivery): ?>
                <?php $tmpDate = explode('-', $date_delivery); ?>
                <th class="item_b"><?php echo $tmpDate[2];?></th>
            <?php endforeach; ?>
            <th class="">Sum 12kg</th>
        </tr>
    </thead>
    <tbody>
        <?php foreach(GasMaterialsType::$ARR_GAS_VO as $materials_type => $typeName): ?>
        <?php $sumOneCarRow = 0; ?>
        <tr>
            <td class="item_b"><?php echo $typeName;?></td>
            <?php foreach($aDateLoop as $date_delivery): ?>
                <?php 
                    $qtyText    = '';
                    if(isset($ONE_CAR[$roadRoute][$date_delivery][$materials_type])):
                        $qtyText = ActiveRecord::formatCurrency($ONE_CAR[$roadRoute][$date_delivery][$materials_type]);
                        $qty     = $ONE_CAR[$roadRoute][$date_delivery][$materials_type];
                        
                        // Dec 30, 2016 sumrow
                        if(in_array($materials_type, GasMaterialsType::$ARR_GAS_VO_45KG)){
                            $qty = ($qty*2);
                        }elseif(in_array($materials_type, GasMaterialsType::$ARR_GAS_VO_6KG)){
                            $qty = round($qty/2);
                        }
                        // Dec 30, 2016 sumrow
                    
                        $sumOneCarRow   += $qty;
                        $sumOneCarAll   += $qty;
                        if(isset($sumOneCarCol[$date_delivery])){
                            $sumOneCarCol[$date_delivery] += $qty;
                        }else{
                            $sumOneCarCol[$date_delivery] = $qty;
                        }
                    endif;
                ?>
                <td class="item_b"><?php echo $qtyText;?></td>
            <?php endforeach; ?>
                <td class="item_b item_r"><?php echo $sumOneCarRow > 0 ? ActiveRecord::formatCurrency($sumOneCarRow) : '';?></td>
        </tr>
        <?php endforeach; ?>
        <tr>
            <td class="item_b">Sum</td>
            <?php foreach($aDateLoop as $date_delivery): ?>
                <?php $qtyText = ''; if(isset($sumOneCarCol[$date_delivery])):
                    $qtyText = ActiveRecord::formatCurrency($sumOneCarCol[$date_delivery]);
                endif; ?>
                <td class="item_b"><?php echo $qtyText;?></td>
            <?php endforeach; ?>
            <td class="item_b item_r"><?php echo ActiveRecord::formatCurrency($sumOneCarAll);?></td>
        </tr>
    </tbody>
</table>