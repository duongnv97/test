<?php
$gridCss = Yii::app()->getAssetManager()->publish(Yii::getPathOfAlias('zii.widgets.assets')).'/gridview/styles.css';
Yii::app()->getClientScript()->registerCssFile($gridCss);
$MODEL_USER     = isset($aData['MODEL_USER'])?$aData['MODEL_USER']:array();
$OUTPUT_DRIVER     = isset($aData['OUTPUT_CAR']['DRIVER'])?$aData['OUTPUT_CAR']['DRIVER']:array();
$OUTPUT_PHU_XE     = isset($aData['OUTPUT_CAR']['PHU_XE'])?$aData['OUTPUT_CAR']['PHU_XE']:array();
$EMPLOYEE_BY_CAR   = isset($aData['OUTPUT_CAR']['EMPLOYEE_BY_CAR'])?$aData['OUTPUT_CAR']['EMPLOYEE_BY_CAR']:array();

$DRIVER_BEFORE_RATIO   = isset($aData['OUTPUT_CAR']['DRIVER_BEFORE_RATIO'])?$aData['OUTPUT_CAR']['DRIVER_BEFORE_RATIO']:array();
$PHU_XE_BEFORE_RATIO   = isset($aData['OUTPUT_CAR']['PHU_XE_BEFORE_RATIO'])?$aData['OUTPUT_CAR']['PHU_XE_BEFORE_RATIO']:array();

$CAR     = isset($aData['CAR'])?$aData['CAR']:array();
arsort($OUTPUT_DRIVER);
arsort($OUTPUT_PHU_XE);
$index = 1;

?>
<div class="grid-view ">
<h1>Sản lượng nhân viên</h1>
<div class="row more_col f_size_15">
    <div class="col1">
    <table class="items hm_table w-700">
        <thead>
            <tr style="">
                <th class="w-10 ">#</th>
                <th class="w-200 item_b">Lái Xe</th>
                <th class="w-200 item_b">Xe</th>
                <th class="w-100 item_b">Loại</th>
                <th class="w-50 item_b">SL thực</th>
                <th class="w-50 item_b">Hệ số</th>
                <th class="w-50 item_b">SL</th>
                <th class="w-50 item_b">Tổng</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($OUTPUT_DRIVER as $employee_id=>$qty): ?>
            <?php
                $employeeName = isset($MODEL_USER[$employee_id]) ? $MODEL_USER[$employee_id]->getFullName() : $employee_id;
                $sCar = $sQtyByCar = $sCarType = $sQtyBeforeRatio = $sRatio = "";
//                is_maintain
                if(isset($CAR[$employee_id])){
                    foreach($CAR[$employee_id] as $car_id){
                        $qtyByCar = isset($EMPLOYEE_BY_CAR[$employee_id][$car_id]) ? ActiveRecord::formatCurrencyRound($EMPLOYEE_BY_CAR[$employee_id][$car_id]) : "";
                        $sCar .= $MODEL_USER[$car_id]->getFullName()."<br>";
                        $sQtyByCar .= $qtyByCar."<br>";
                        $sCarType .= GasConst::getCarWeight($MODEL_USER[$car_id]->is_maintain)."<br>";
                        
                        $qtyBeforeRatio = isset($DRIVER_BEFORE_RATIO[$employee_id][$car_id]) ? ActiveRecord::formatCurrencyRound($DRIVER_BEFORE_RATIO[$employee_id][$car_id]) : "";
                        $sQtyBeforeRatio .= $qtyBeforeRatio."<br>";
                        $sRatio .= $MODEL_USER[$car_id]->last_name."<br>";
                    }
                }
            ?>
            <tr>
                <td class="item_c"><?php echo $index++;?></td>
                <td class="item_b"><?php echo $employeeName;?></td>
                <td class="item_b"><?php echo $sCar;?></td>
                <td class="item_b"><?php echo $sCarType;?></td>
                <td class="item_b item_r"><?php echo $sQtyBeforeRatio;?></td>
                <td class="item_b item_c"><?php echo $sRatio;?></td>
                <td class="item_b item_r"><?php echo $sQtyByCar;?></td>
                <td class="item_b item_r"><?php echo ActiveRecord::formatCurrencyRound($qty);?></td>
            </tr>
            <?php endforeach; ?>        
        </tbody>
    </table>
    </div>
    <?php $index = 1; ?>
    <div class="col2 l_padding_20">
    <table class="items hm_table w-700 ">
        <thead>
            <tr style="">
                <th class="w-10 ">#</th>
                <th class="w-200 item_b">Phụ Xe</th>
                <th class="w-200 item_b">Xe</th>
                <th class="w-100 item_b">Loại</th>
                <th class="w-50 item_b">SL thực</th>
                <th class="w-50 item_b">Hệ số</th>
                <th class="w-50 item_b">SL</th>
                <th class="w-50 item_b">Tổng</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($OUTPUT_PHU_XE as $employee_id=>$qty): ?>
            <?php
                if(!isset($MODEL_USER[$employee_id])){
                    continue;
                }
                $employeeName = isset($MODEL_USER[$employee_id]) ? $MODEL_USER[$employee_id]->getFullName() : $employee_id;
                $sCar = $sQtyByCar = $sCarType = $sQtyBeforeRatio = $sRatio = "";
                if(isset($CAR[$employee_id])){
                    foreach($CAR[$employee_id] as $car_id){
                        $qtyByCar = isset($EMPLOYEE_BY_CAR[$employee_id][$car_id]) ? ActiveRecord::formatCurrencyRound($EMPLOYEE_BY_CAR[$employee_id][$car_id]) : "";
                        $sCar .= $MODEL_USER[$car_id]->getFullName()."<br>";
                        $sQtyByCar .= $qtyByCar."<br>";
                        $sCarType .= GasConst::getCarWeight($MODEL_USER[$car_id]->is_maintain)."<br>";
                        
                        $qtyBeforeRatio = isset($PHU_XE_BEFORE_RATIO[$employee_id][$car_id]) ? ActiveRecord::formatCurrencyRound($PHU_XE_BEFORE_RATIO[$employee_id][$car_id]) : "";
                        $sQtyBeforeRatio .= $qtyBeforeRatio."<br>";
                        $sRatio .= $MODEL_USER[$car_id]->last_name."<br>";
                    }
                }
            ?>
            <tr>
                <td class="item_c"><?php echo $index++;?></td>
                <td class="item_b"><?php echo $employeeName;?></td>
                <td class="item_b"><?php echo $sCar;?></td>
                <td class="item_b"><?php echo $sCarType;?></td>
                <td class="item_b item_r"><?php echo $sQtyBeforeRatio;?></td>
                <td class="item_b item_c"><?php echo $sRatio;?></td>
                <td class="item_b item_r"><?php echo $sQtyByCar;?></td>
                <td class="item_b item_r"><?php echo ActiveRecord::formatCurrencyRound($qty);?></td>
            </tr>
            <?php endforeach; ?>        
        </tbody>
    </table>
    </div>
</div>    
</div>

<script type="text/javascript">
    fnAddClassOddEven('items');
</script>
