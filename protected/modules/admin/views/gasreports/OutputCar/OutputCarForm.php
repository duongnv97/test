<?php
$gridCss = Yii::app()->getAssetManager()->publish(Yii::getPathOfAlias('zii.widgets.assets')).'/gridview/styles.css';
Yii::app()->getClientScript()->registerCssFile($gridCss);
$MODEL_USER     = isset($aData['MODEL_USER'])?$aData['MODEL_USER']:array();
$OUTPUT_CAR     = isset($aData['OUTPUT_CAR'])?$aData['OUTPUT_CAR']:array();
$ONE_CAR        = isset($aData['ONE_CAR'])?$aData['ONE_CAR']:array();
$index = 1; $sumCol = array();
$aDateLoop = MyFormat::getArrayDay($model->date_from_ymd, $model->date_to_ymd);

?>
<div class="grid-view ">
    
<?php foreach($model->getArrRoadRoute() as $roadRoute => $roadRouteText):  $sumCol = array();  $sumAll = 0;?>
    
<h1><?php echo $roadRouteText;?></h1>    
<table class="items hm_table f_size_15">
    <thead>
        <tr style="">
            <th class="w-20 ">#</th>
            <th class="w-200 ">Số Xe</th>
            <?php foreach(GasMaterialsType::$ARR_GAS_VO as $materials_type => $typeName): ?>
                <th class="item_b"><?php echo $typeName;?></th>
            <?php endforeach; ?>
                <th class="">Sum bình 12kg</th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($OUTPUT_CAR[$roadRoute] as $car_id => $aData): ?>
        <?php
            $carName    = $MODEL_USER[$car_id]->getFullName();
            $sumRow     = 0;
        ?>
        <tr>
            <td class="item_c"><?php echo $index++;?></td>
            <td class="item_b"><?php echo $carName;?></td>
                <?php foreach(GasMaterialsType::$ARR_GAS_VO as $materials_type => $typeName): ?>
                <?php 
                    if(isset($aData['qty'][$materials_type])){
                        $value = ActiveRecord::formatCurrency($aData['qty'][$materials_type]);
                        if(isset($sumCol[$materials_type])){
                            $sumCol[$materials_type] += $aData['qty'][$materials_type];
                        }else{
                            $sumCol[$materials_type] = $aData['qty'][$materials_type];
                        }
                        // Dec 30, 2016 sumrow
                        if(in_array($materials_type, GasMaterialsType::$ARR_GAS_VO_45KG)){
                            $sumRow += ($aData['qty'][$materials_type]*2);
                        }elseif(in_array($materials_type, GasMaterialsType::$ARR_GAS_VO_6KG)){
                            $sumRow += round($aData['qty'][$materials_type]/2);
                        }else{
                            $sumRow += $aData['qty'][$materials_type];
                        }
                        // Dec 30, 2016 sumrow
                        
                    }else{
                        $value = '';
                    }
//                    $value = isset($aData['qty'][$materials_type]) ? ActiveRecord::formatCurrency($aData['qty'][$materials_type]) : "";
                ?>
                <td class="item_b item_r "><?php echo $value;?></td>
            <?php endforeach; ?>
                <td class="item_r item_b"><?php $sumAll+= $sumRow; echo ActiveRecord::formatCurrency($sumRow);?></td>
        </tr>
        <?php endforeach; ?>
        <tr>
            <td class="item_b" colspan="2">Tổng cộng</td>
            <?php foreach(GasMaterialsType::$ARR_GAS_VO as $materials_type => $typeName): ?>
                <?php 
                    $value = isset($sumCol[$materials_type]) ? ActiveRecord::formatCurrency($sumCol[$materials_type]) : '';
                ?>
                <td class="item_b item_r "><?php echo $value;?></td>
            <?php endforeach; ?>
            <td class="item_b item_r "><?php echo ActiveRecord::formatCurrency($sumAll);?></td>
        </tr>
    </tbody>
</table>
<?php if(!empty($model->uid_login)): ?>
    <?php include "OutputCarOne.php"; ?>
<?php endif; ?>

<?php endforeach; ?>

</div>

<script type="text/javascript">
    fnAddClassOddEven('items');
</script>
