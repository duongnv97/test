<?php
$this->breadcrumbs=array(
	'Báo cáo sản lượng xe',
);
$aType = GasStoreCard::$DAILY_INTERNAL_TYPE_CUSTOMER;
unset($aType[CUSTOMER_OTHER]);
$cRole = Yii::app()->user->role_id;
$showSubmit = true;
//if($cRole == ROLE_SUB_USER_AGENT && !in_array(MyFormat::getAgentId(), GasCheck::$arrAgentInputCar)){
//    $showSubmit = false;
//}
?>

<h1>Báo cáo sản lượng xe</h1>
<div class="search-form" style="">
    <div class="wide form">

    <?php $form=$this->beginWidget('CActiveForm', array(
            'action'=>Yii::app()->createUrl($this->route),
            'method'=>'get',
    )); ?>
            <?php if(Yii::app()->user->hasFlash('successUpdate')): ?>
                <div class="success_div"><?php echo Yii::app()->user->getFlash('successUpdate');?></div>
            <?php endif; ?>
            <div class="row">
                <?php echo $form->labelEx($model,'agent_id'); ?>
                <?php echo $form->hiddenField($model,'agent_id', array('class'=>'')); ?>
                <?php
                    // 1. limit search kh của sale
                    $url = Yii::app()->createAbsoluteUrl('admin/ajax/search_by_role', array('role'=> ROLE_AGENT));
                    // widget auto complete search user customer and supplier
                    $aDataWidget = array(
                        'model'=>$model,
                        'field_customer_id'=>'agent_id',
                        'url'=> $url,
                        'name_relation_user'=>'agent',
                        'ClassAdd' => 'w-400',
                        'field_autocomplete_name' => 'autocomplete_name',
                        'placeholder'=>'Nhập mã hoặc tên đại lý',
                    );
                    $this->widget('ext.SpaAutocompleteCustomer.SpaAutocompleteCustomer',
                        array('data'=>$aDataWidget));
                ?>
            </div>

            <div class="row">
                <?php echo $form->labelEx($model,'uid_login', array('label'=>'Số Xe')); ?>
                <?php echo $form->hiddenField($model,'uid_login', array('class'=>'')); ?>
                <?php
                    // 1. limit search kh của sale
                    $url = Yii::app()->createAbsoluteUrl('admin/ajax/search_by_role', array('role'=> ROLE_CAR));
                    // widget auto complete search user customer and supplier
                    $aDataWidget = array(
                        'model'=>$model,
                        'field_customer_id'=>'uid_login',
                        'url'=> $url,
                        'name_relation_user'=>'rUidLogin',
                        'ClassAdd' => 'w-400',
                        'field_autocomplete_name' => 'materials_name',
                        'placeholder'=>'Nhập mã hoặc tên xe tải',
                    );
                    $this->widget('ext.SpaAutocompleteCustomer.SpaAutocompleteCustomer',
                        array('data'=>$aDataWidget));
                ?>
            </div>
                
            <div class="row more_col">
                <div class="col1">
                    <?php echo $form->label($model,'date_from'); ?>
                    <?php 
                        $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                            'model'=>$model,        
                            'attribute'=>'date_from',
                            'options'=>array(
                                'showAnim'=>'fold',
                                'dateFormat'=> MyFormat::$dateFormatSearch,
                                'changeMonth' => true,
                                'changeYear' => true,
                                'showOn' => 'button',
                                'buttonImage'=> Yii::app()->theme->baseUrl.'/admin/images/icon_calendar_r.gif',
                                'buttonImageOnly'=> true,                                
                            ),        
                            'htmlOptions'=>array(
                                'class'=>'w-16',
                                'size'=>'16',
                                'style'=>'float:left;',                               
                                'readonly'=>1,
                            ),
                        ));
                    ?>     		
                </div>
                <div class="col2">
                    <?php echo $form->label($model,'date_to'); ?>
                    <?php 
                        $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                            'model'=>$model,        
                            'attribute'=>'date_to',
                            'options'=>array(
                                'showAnim'=>'fold',
                                'dateFormat'=> MyFormat::$dateFormatSearch,
                                'changeMonth' => true,
                                'changeYear' => true,
                                'showOn' => 'button',
                                'buttonImage'=> Yii::app()->theme->baseUrl.'/admin/images/icon_calendar_r.gif',
                                'buttonImageOnly'=> true,                                
                            ),        
                            'htmlOptions'=>array(
                                'class'=>'w-16',
                                'size'=>'16',
                                'style'=>'float:left;',
                                'readonly'=>1,
                            ),
                        ));
                    ?>
                </div>
            </div>
            <div class="clr"></div>
            <input name="IsWebView" class="IsWebView display_none" value="0">
            <?php if($showSubmit): ?>
                <div class="row buttons" style="padding-left: 159px; margin:20px 0;">
                    <input class="ViewWebSumByCar" type="button" value="Xem Theo Xe">
                    <input class="ViewWebSumByEmployee" type="button" value="Xem Theo Nhân Viên">
                    <?php $this->widget('bootstrap.widgets.TbButton', array(
                        'buttonType'=>'submit',
                        'label'=> 'Xuất Excel Thẻ Kho',
                        'type'=>'null', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
                        'size'=>'small', // null, 'large', 'small' or 'mini'
                        //'htmlOptions' => array('style' => 'margin-bottom: 10px; float: right;'),
                    )); ?>
                </div>
            <?php endif; ?>

    <?php $this->endWidget(); ?>

    </div><!-- wide form -->
</div><!-- search-form -->
<?php if($model->MAX_ID == 1): ?>
    <?php include_once 'OutputCarForm.php';?>
<?php elseif($model->MAX_ID == 2): ?>
    <?php include_once 'OutputCarEmployee.php';?>
<?php endif; ?>

<style>
    .ui-datepicker-trigger { float: left;}
</style>

<script>
$(document).ready(function(){
    $('.ViewWebSumByCar').click(function(){
        $('.IsWebView').val(1);
        $(this).closest('form').submit();
    });
    $('.ViewWebSumByEmployee').click(function(){
        $('.IsWebView').val(2);
        $(this).closest('form').submit();
    });
});
</script>