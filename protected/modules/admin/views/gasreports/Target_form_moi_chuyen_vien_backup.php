<?php 
$cty_target_bo = 0;
$cty_target_binh_moi_chuyen_vien = 0;
$cty_thuc_te_bo = 0;
$cty_thuc_te_binh_moi_chuyen_vien = 0;
$cty_thuc_te_bo_qty = 0;
$aIdGiamSatAgent = Users::getArrIdUserByArrayRoleHaveSale( array(ROLE_MONITOR_AGENT), array('order'=>"t.code_bussiness") );
$aIdNvPhucVuKh = Users::getArrIdUserByArrayRoleHaveSale( array(ROLE_EMPLOYEE_MAINTAIN), array('order'=>"t.code_bussiness") );
$aIdChuyenVienMoiReal = array();
// Dec 03, 2014dùng lọc ra những id của sale mối chuyên viên thực sự
foreach($SALE_MODEL_MOI_CHUYEN_VIEN as $province_id=>$aModelUser): 
    $LIST_ID_GIAMSAT = isset($aIdGiamSatAgent[$province_id]['LIST_ID'])?$aIdGiamSatAgent[$province_id]['LIST_ID']:array();
    $LIST_ID_PHUCVU_KH = isset($aIdNvPhucVuKh[$province_id]['LIST_ID'])?$aIdNvPhucVuKh[$province_id]['LIST_ID']:array();
    if( !in_array($aModelUser->id, $LIST_ID_GIAMSAT) && !in_array($aModelUser->id, $LIST_ID_PHUCVU_KH) ){
        $aIdChuyenVienMoiReal[$province_id]['LIST_ID'][] = $aModelUser->id;
        $aIdChuyenVienMoiReal[$province_id]['LIST_MODEL'][] = $aModelUser;
    }    
endforeach;
$FinalArray = array(
    "Sale Mối Chuyên Viên" => $aIdChuyenVienMoiReal,
    "Giám Sát Đại Lý" => $aIdGiamSatAgent,
    "NV Phục Vụ Khách Hàng" => $aIdNvPhucVuKh,
);

// Dec 03, 2014dùng lọc ra những id của sale mối chuyên viên thực sự
?>
<div id="SALE_MODEL_MOI_CHUYEN_VIEN">
    <h1><?php echo $textInfo;?></h1>
    <div class="clr"></div>

    <div class="box_350 ">
        <table class="hm_table items">
            <tbody>
                <tr>
                    <td colspan="3" class="item_c item_b">CÔNG TY</td>
                </tr>
                <tr>
                    <td class="item_c w-120">Target</td>
                    <td class="item_c w-140">Thực Tế</td>
                    <td class="item_c w-100">Tỷ Lệ</td>
                </tr>                       
                <tr class="color_type_2">
                    <td class="item_r cty_target_binh_moi_chuyen_vien"></td>
                    <td class="item_r cty_thuc_te_binh_moi_chuyen_vien"></td>
                    <td class="item_c cty_binh_moi_chuyen_vien_percent"></td>
                </tr>
                <tr class="color_type_1">
                    <td class="item_r"> &nbsp;</td>
                    <td class="item_r cty_thuc_te_binh_bo_chuyen_vien"></td>
                    <td class="item_r"> &nbsp;</td>
                </tr>
            </tbody>
        </table>
    </div>            
    <div class="clr"></div>

    <?php foreach($SALE_MODEL_MOI_CHUYEN_VIEN as $province_id=>$aModelUser): ?>
    <div class="wrap_province">
        <h1 class="l_padding_50 margin_0 padding_0"><?php echo isset($PROVINCE_MODEL[$province_id])?$PROVINCE_MODEL[$province_id]->name:"";?></h1>
        <?php foreach($aModelUser as $mUser): ?>
            <div class="box_310">
                <table class="hm_table items">
                    <tbody>
                        <tr>
                            <td colspan="3" class="item_c item_b"><?php echo $mUser->first_name;?></td>
                        </tr>
                        <tr>
                            <td class="item_c w-120">Target</td>
                            <td class="item_c w-140">Thực Tế</td>
                            <td class="item_c w-100">Tỷ Lệ</td>
                        </tr>
                        <?php foreach(CmsFormatter::$CUSTOMER_BO_MOI as $type=>$label):?>
                        <?php
                            if($type==STORE_CARD_KH_BINH_BO)  continue;
                            $uid = $mUser->id;
                            //*** for bình bò của KH mối
                            // CHANGE Now 20, 2014 không quy đổi 1 bình bò = 2 bình 12kg nữa, mà nhân ra số kg để tính %
                            $BINHBO_45 = (isset($data['OUTPUT_SALE_MOI_CHUYEN_VIEN_BO'][$uid][$type][MATERIAL_TYPE_BINHBO_45])?$data['OUTPUT_SALE_MOI_CHUYEN_VIEN_BO'][$uid][$type][MATERIAL_TYPE_BINHBO_45]:0)*1;
                            $BINHBO_50 = (isset($data['OUTPUT_SALE_MOI_CHUYEN_VIEN_BO'][$uid][$type][MATERIAL_TYPE_BINHBO_50])?$data['OUTPUT_SALE_MOI_CHUYEN_VIEN_BO'][$uid][$type][MATERIAL_TYPE_BINHBO_50]:0)*1;
                            $TextBBo = $BINHBO_45+$BINHBO_50;
                            $cty_thuc_te_bo_qty += ($BINHBO_45+$BINHBO_50);
                            // $BINHBO_EQUAL_BINH12 = $BINHBO*2;// vì bình bò ở đây ko nhân với kg mà chỉ là qty thuần
                            $BINHBO_45 = $BINHBO_45*CmsFormatter::$MATERIAL_VALUE_KG[MATERIAL_TYPE_BINHBO_45];
                            $BINHBO_50 = $BINHBO_50*CmsFormatter::$MATERIAL_VALUE_KG[MATERIAL_TYPE_BINHBO_50];
                            //*** for bình bò của KH mối                            
                            // CHANGE Now 20, 2014 không quy đổi 1 bình bò = 2 bình 12kg nữa, mà nhân ra số kg để tính %
                            
                            $target=isset($data['aTargetEachMonth'][$uid][$type][$FOR_MONTH])?$data['aTargetEachMonth'][$uid][$type][$FOR_MONTH]:0;
                            $OUTPUT=isset($data['OUTPUT'][$uid][$type])?$data['OUTPUT'][$uid][$type]:0;
                            $REMAIN=isset($data['REMAIN'][$uid][$type])?$data['REMAIN'][$uid][$type]:0;
                            $real = $OUTPUT-$REMAIN;
                            $percent = $real;
//                            $OUTPUT_BINHBO = ($BINHBO*2)*12;
                            $OUTPUT_BINHBO = $BINHBO_45+$BINHBO_50;//CHANGE Now 20, 2014
                            
                            if($target){
                                $percent = ( ($real+$OUTPUT_BINHBO) / $target) * 100;
                            }
                            $qty = '';
                            if($real){
                                $qty = " = ".ActiveRecord::formatCurrency($real/12);
                            }
                            $target_qty = '';
                            if($target>0){
                                $target_qty = " = ".ActiveRecord::formatCurrency($target/12);
                            }
                            // for target cty
                            $cty_target_binh_moi_chuyen_vien += $target;
                            $cty_thuc_te_binh_moi_chuyen_vien += $real;
                            $cty_thuc_te_bo += $OUTPUT_BINHBO;
                            // for target cty // nếu có xuất excel thì sẽ dùng con số + cuối cùng đưa vào session của excel
                            // thì đỡ phải tính ở bên dưới
                        ?>

                        <tr class="color_type_<?php echo $type;?>">
                            <td class="item_r"><?php echo ActiveRecord::formatCurrency($target).$target_qty;?></td>
                            <td class="item_r"><?php echo ActiveRecord::formatCurrency($real).$qty;?></td>
                            <td class="item_c"><?php echo round($percent,1);?> %</td>
                        </tr>
                        <tr class="color_type_1">
                            <td class="item_r">&nbsp;</td>
                            <td class="item_r"><?php echo $OUTPUT_BINHBO!=0? (ActiveRecord::formatCurrency($OUTPUT_BINHBO)." = $TextBBo"):'';?></td>
                            <td class="item_r">&nbsp;</td>
                        </tr>
                        <?php endforeach;?>

                    </tbody>
                </table>
            </div>                

        <?php endforeach; // end foreach($aModelUser as $mUser): ?>

    </div> <!-- end  <div class="wrap_province"> -->
    <div class="clr"></div>
    <?php endforeach; // end <?php foreach($SALE_MODEL_BO as $province_id=>$aModelUser): ?>
    <div class="clr"></div>
</div>


<?php 
    $qty_thuc_te_chuyen_vien_cty = '';
    if($cty_thuc_te_binh_moi_chuyen_vien){
        $qty_thuc_te_chuyen_vien_cty = " = ".ActiveRecord::formatCurrency($cty_thuc_te_binh_moi_chuyen_vien/12);
    }
    $target_qty_chuyen_vien_cty = '';
    if($cty_target_binh_moi_chuyen_vien>0){
        $target_qty_chuyen_vien_cty = " = ".ActiveRecord::formatCurrency($cty_target_binh_moi_chuyen_vien/12);
    }
?>
<input class="hide_cty_target_binh_moi_chuyen_vien" value="<?php echo ActiveRecord::formatCurrency($cty_target_binh_moi_chuyen_vien).$target_qty_chuyen_vien_cty;?>" type="hidden">
<input class="hide_cty_thuc_te_binh_moi_chuyen_vien" value="<?php echo ActiveRecord::formatCurrency($cty_thuc_te_binh_moi_chuyen_vien).$qty_thuc_te_chuyen_vien_cty;?>" type="hidden">
<input class="hide_cty_thuc_te_binh_bo_chuyen_vien" value="<?php echo $cty_thuc_te_bo!=0?(ActiveRecord::formatCurrency($cty_thuc_te_bo)." = $cty_thuc_te_bo_qty" ):'';?>" type="hidden">

<input class="hide_cty_binh_moi_chuyen_vien_percent" value="<?php echo round($cty_target_binh_moi_chuyen_vien?(( ($cty_thuc_te_binh_moi_chuyen_vien+ ($cty_thuc_te_bo))/$cty_target_binh_moi_chuyen_vien)*100):$cty_thuc_te_binh_moi_chuyen_vien,1);?> %" type="hidden">
