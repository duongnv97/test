<div class="grid-view grid-view-scroll">
        
    <?php foreach(CmsFormatter::$MATERIAL_TYPE_OUTPUT_STATISTIC as $type_output):?>
        <?php $code = $type_output['code'];             
            $STT = 0;
        ?>
        <div class="title_table_statistic">TỔNG SẢN LƯỢNG GAS BÌNH <?php echo $code;?> NĂM <?php echo $YEARS;?></div>
        <div class="grid-view-scroll">
            <table class="hm_table table_statistic items table_month_<?php echo $YEARS;?>" style="">
                <thead>
                    <tr>                               
                        <th class="w-10 item_c">STT</th>
                        <th class="w-70">Tên đại lý</th>
                        <th class="w-50">Tổng</th>
                        <th class="w-50">BQ</th>
                        <?php foreach($MONTHS as $month=>$tempM): ?>
                        <th class="item_day" style="">
                            <?php echo "T".$month;?>
                        </th>
                        <?php endforeach;?>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach($OUTPUT[$code]['total_year']['sum_row'] as $agent_id=>$total_year_agent):?>
                    <?php $STT++;?>
                    <tr>
                        <td class=""><?php echo $STT; ?></td>
                        <td class=""><?php echo $AGENT_MODEL[$agent_id]->first_name; ?></td>
                        <td class="item_r item_b"><?php echo ActiveRecord::formatCurrency($total_year_agent);?></td>
                        <td class="item_r"><?php 
                            $average = round($total_year_agent/count($MONTHS));
                            echo ActiveRecord::formatCurrency($average); ?>
                        </td>
                        <?php foreach($MONTHS as $month=>$tempM): ?>																	
                        <td class="item_r item_day w-80">
                            <?php 
                                $output_month = isset($OUTPUT[$code]['total_year'][$month][$agent_id])?$OUTPUT[$code]['total_year'][$month][$agent_id]:"";
                                echo $output_month>0?ActiveRecord::formatCurrency($output_month):'';
                            ?>
                        </td>
                        <?php endforeach; // end <?php foreach($days_of_month as $day) ?>
                    </tr>
                    <?php ?>
                    <?php endforeach; // end foreach($OUTPUT[$code]['sum_row_total_month'][$month]  ?>
                    <!-- tổng cộng -->
                        <tr>
                            <td class="">&nbsp;</td>
                            <td class="item_b">Tổng Cộng ( bình )</td>
                            <td class="item_r item_b">
                                <?php $sum_col_total_year_all_agent = $OUTPUT[$code]['sum_col_total_year_all_agent'];
                                echo ActiveRecord::formatCurrency($sum_col_total_year_all_agent);?>
                            </td>
                            <td class="item_r item_b"><?php
                                    $average = round($sum_col_total_year_all_agent/count($MONTHS));
                                    echo ActiveRecord::formatCurrency($average); ?>
                            </td>
                            <?php foreach($MONTHS as $month=>$tempM): ?>	
                            <td class="item_r item_b">
                                <?php 
                                    $sum_month = isset($OUTPUT[$code]['total_year']['sum_col'][$month])?$OUTPUT[$code]['total_year']['sum_col'][$month]:"";
                                    echo $sum_month>0?ActiveRecord::formatCurrency($sum_month):'';
                                ?>
                            </td>
                            <?php endforeach; // end <?php foreach($days_of_month as $day) ?>                        
                        </tr>
                        <tr>
                            <td class="">&nbsp;</td>
                            <td class="item_b">Tổng Cộng ( KG )</td>
                            <td class="item_r item_b">
                                <?php $BINH_KG = KL_BINH_12;
                                    if($code=='45KG')
                                        $BINH_KG = KL_BINH_45;
                                echo ActiveRecord::formatCurrency($sum_col_total_year_all_agent*$BINH_KG);?>
                            </td>
                            <td class="item_r item_b"><?php
                                    echo ActiveRecord::formatCurrency($average*$BINH_KG); ?>
                            </td>
                            <?php foreach($MONTHS as $month=>$tempM): ?>	
                            <td class="item_r item_b">
                                <?php 
                                $sum_month = isset($OUTPUT[$code]['total_year']['sum_col'][$month])?$OUTPUT[$code]['total_year']['sum_col'][$month]:"";
                                echo ActiveRecord::formatCurrency($sum_month*$BINH_KG); ?>
                            </td>
                            <?php endforeach; // end <?php foreach($days_of_month as $day) ?>                        
                        </tr>
                        <!-- tổng cộng -->
                </tbody>
            </table><!-- end <table class="table_statistic items table_mon-->
        </div> <!-- end <div class="grid-view-scroll">-->
    <?php endforeach; // end foreach(CmsFormatter::$MATERIAL_TYPE_OUTPUT_STATISTIC as $type_output)?>    
    <?php include 'Revenue_output_money_bank_year.php'; ?>
    <?php include 'Revenue_output_total_revenue_year.php'; ?>

</div><!-- end <div class="grid-view">-->