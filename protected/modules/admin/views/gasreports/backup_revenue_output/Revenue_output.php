<?php
$this->breadcrumbs=array(
	'Xem báo cáo doanh thu - sản lượng',
);?>

<h1>Xem báo cáo doanh thu - sản lượng</h1>
<div class="search-form" style="">
    <div class="wide form">

    <?php $form=$this->beginWidget('CActiveForm', array(
            'action'=>Yii::app()->createUrl($this->route),
            'method'=>'post',
    )); ?>
            <?php if(Yii::app()->user->hasFlash('successUpdate')): ?>
                <div class="success_div"><?php echo Yii::app()->user->getFlash('successUpdate');?></div>         
            <?php endif; ?>    
                
            <div class="row group_subscriber">
                <?php echo Yii::t('translation', $form->label($model,'statistic_month')); ?>
                <?php echo $form->dropDownList($model,'statistic_month', ActiveRecord::getMonthVnWithZeroFirst(), array('class'=>'', 'empty'=>'Select')); ?>		
            </div>
                
            <div class="row">
                <?php echo Yii::t('translation', $form->labelEx($model,'statistic_year')); ?>
                <?php echo $form->dropDownList($model,'statistic_year', ActiveRecord::getRangeYear(), array('style'=>'')); ?>		
                <?php echo $form->error($model,'statistic_year'); ?>
            </div>     
            
            <div class="row group_subscriber">
                <?php echo Yii::t('translation', $form->label($model,'agent_id')); ?>
                <?php // echo $form->dropDownList($model,'agent_id', Users::getArrUserByRole(ROLE_AGENT),array('style'=>'width:350px;','empty'=>'Select')); ?>

                <div class="fix-label">
                    <?php
                       $this->widget('ext.multiselect.JMultiSelect',array(
                             'model'=>$model,
                             'attribute'=>'agent_id',
                             'data'=>Users::getArrUserByRole(ROLE_AGENT),
                             // additional javascript options for the MultiSelect plugin
                             'options'=>array(),
                             // additional style
                             'htmlOptions'=>array('style' => 'width: 345px;'),
                       ));    
                   ?>
                </div>            
            </div>

            <div class="row buttons" style="padding-left: 159px; margin:20px 0;">
                            <?php $this->widget('bootstrap.widgets.TbButton', array(
                'buttonType'=>'submit',
                'label'=>Yii::t('translation','Xem Thống Kê'),
                'type'=>'null', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
                'size'=>'small', // null, 'large', 'small' or 'mini'
                //'htmlOptions' => array('style' => 'margin-bottom: 10px; float: right;'),
            )); ?>	
            <?php // if(ControllerActionsName::isAccessAction('statistic_maintain_export_excel', $actions)):?>
            <?php if(0):?>
                    <input class='statistic_maintain_export_excel' next='<?php echo Yii::app()->createAbsoluteUrl('admin/gasmaintain/statistic_maintain_export_excel');?>' type='button' value='Xuất Thống Kê Hiện Tại Ra Excel'>
            <?php endif;?>

            </div>

    <?php $this->endWidget(); ?>

    </div><!-- wide form -->
</div><!-- search-form -->

<?php // include_once 'View_store_movement_summary_print.php';?>

<style>
    .ui-datepicker-trigger { float: left;}
</style>

<script>
$(document).ready(function(){
    $('.form').find('button:submit').click(function(){        
        $.blockUI({ overlayCSS: { backgroundColor: '#fff' } }); 
    });   
});

function isValidSubmit(){
    var msg = '';
    if($('#GasStoreCard_date_from').val()=='' || $('#GasStoreCard_date_to').val()==''){
        msg = 'Chưa chọn khoảng ngày thống kê';            
    }
    return msg;
}
</script>    

<?php include 'Revenue_output_form.php'; ?>