<?php if(count($data)) :?>
<?php Yii::app()->clientScript->registerCoreScript('jquery.ui'); ?>
<?php
$gridCss = Yii::app()->getAssetManager()->publish(Yii::getPathOfAlias('zii.widgets.assets')).'/gridview/styles.css';
Yii::app()->getClientScript()->registerCssFile($gridCss);
$OUTPUT = $data['OUTPUT'];
$MONEY_BANK = isset($data['MONEY_BANK'])?$data['MONEY_BANK']:array();
$TOTAL_REVENUE = isset($data['TOTAL_REVENUE'])?$data['TOTAL_REVENUE']:array();
        
$MONTHS = $data['month'];
// tab total year -- sẽ làm include sau

// tab each month
$AGENT_MODEL = Users::getArrObjectUserByRoleHaveOrder (ROLE_AGENT, 'code_account');
$_SESSION['data-excel']['AGENT_MODEL'] = $AGENT_MODEL;
$YEARS = $model->statistic_year;
$_SESSION['data-excel']['YEARS'] = $YEARS;

?>
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/jquery.floatThead.min.js"></script>    
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/underscore-min.js"></script>    

<script>
$(function() {
    $( "#tabs" ).tabs();
    $(".items > tbody > tr:odd").addClass("odd");
    $(".items > tbody > tr:even").addClass("even");   
    fnTabMonthClick();
});

function fnTabMonthClick(){
     // floatThead for first tab
     var index=1;
     $('.grid-view').each(function(){
        if(index==1){
            var tableItem = $(this).find('.items').eq(0);
            $(this).find('.items').floatThead();
        }
        index++;
    });    
    // floatThead for first tab
    
    // floatThead when click switch tab
    $('.tab_month').click(function(){
        var month_current = $(this).attr('month_current');
        var tableItem = $('.table_month_'+month_current);
        tableItem.floatThead();
    });
    // floatThead when click switch tab
}

</script>

<div id="tabs">
<ul>
    <!--<li><a class="tab_month" month_current="<?php echo $YEARS;?>" href="#tabs-<?php echo $YEARS;?>">Năm <?php echo $YEARS;?></a></li>-->
    <?php foreach($MONTHS as $month=>$tempM):?>
        <li><a class="tab_month" month_current="<?php echo $month;?>" href="#tabs-<?php echo $month;?>">Tháng <?php echo $month;?></a></li>
    <?php endforeach; // end foreach($MONTHS as $month) ?>
</ul>

<div id="tabs-<?php echo $YEARS;?>">
    <?php // include_once 'Revenue_output_form_year.php'; ?>
</div>    
<?php foreach($MONTHS as $month=>$tempM):?>
    <div id="tabs-<?php echo $month;?>">
        <div class="grid-view grid-view-scroll">
        
        <?php foreach(CmsFormatter::$MATERIAL_TYPE_OUTPUT_STATISTIC as $type_output):?>
            <?php $code = $type_output['code']; 
                $days_of_month = $OUTPUT[$code]['days'][$month];                
                $STT = 0;
            ?>
            <div class="title_table_statistic">TỔNG SẢN LƯỢNG GAS BÌNH <?php echo $code;?> THÁNG <?php echo $month;?> NĂM <?php echo $YEARS;?></div>
            <div class="grid-view-scroll">
                <table class="hm_table table_statistic items table_month_<?php echo $month;?>" style="">
                    <thead>
                        <tr>                               
                            <th class="w-10 item_c">STT</th>
                            <th class="w-70">Tên đại lý</th>
                            <th class="w-50">Tổng</th>
                            <th class="w-50">BQ</th>
                            <?php foreach($days_of_month as $day): ?>
                            <th class="w-50" style="">
                                <?php echo $day;?>
                            </th>
                            <?php endforeach;?>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach($OUTPUT[$code]['sum_row_total_month'][$month] as $agent_id=>$total_month_agent):?>
                        <?php $STT++;?>
                        <tr>
                            <td class=""><?php echo $STT; ?></td>
                            <td class=""><?php echo $AGENT_MODEL[$agent_id]->first_name; ?></td>
                            <td class="item_r item_b"><?php echo ActiveRecord::formatCurrency($total_month_agent);?></td>
                            <td class="item_r"><?php $average = round($total_month_agent/count($days_of_month)); 
                                echo ActiveRecord::formatCurrency($average); ?>
                            </td>
                            <?php foreach($days_of_month as $day): ?>																	
                            <td class="item_r">
                                <?php 
                                    $output_day = isset($OUTPUT[$code][$month][$day][$agent_id])?$OUTPUT[$code][$month][$day][$agent_id]:"";
                                    echo $output_day>0?ActiveRecord::formatCurrency($output_day):'';
                                ?>
                            </td>
                            <?php endforeach; // end <?php foreach($days_of_month as $day) ?>
                        </tr>
                        <?php ?>
                        <?php endforeach; // end foreach($OUTPUT[$code]['sum_row_total_month'][$month]  ?>
                        <!-- tổng cộng -->
                        <tr>
                            <td class="">&nbsp;</td>
                            <td class="item_b">Tổng Cộng ( bình )</td>
                            <td class="item_r item_b">
                                <?php $sum_col_total_month_all_agent = $OUTPUT[$code]['sum_col_total_month_all_agent'][$month];
                                echo ActiveRecord::formatCurrency($sum_col_total_month_all_agent);?>
                            </td>
                            <td class="item_r item_b">
                                <?php 
                                    $average = round($sum_col_total_month_all_agent/count($days_of_month));
                                echo ActiveRecord::formatCurrency($average); ?>
                            </td>
                            <?php foreach($days_of_month as $day): ?>																	
                            <td class="item_r item_b">
                                <?php 
                                    $sum_day = isset($OUTPUT[$code]['sum_col'][$month][$day])?$OUTPUT[$code]['sum_col'][$month][$day]:"";
                                    echo $sum_day>0?ActiveRecord::formatCurrency($sum_day):'';
                                ?>
                            </td>
                            <?php endforeach; // end <?php foreach($days_of_month as $day) ?>                        
                        </tr>
                        <tr>
                            <td class="">&nbsp;</td>
                            <td class="item_b">Tổng Cộng ( KG )</td>
                            <td class="item_r item_b">
                                <?php 
                                    $BINH_KG = KL_BINH_12;
                                    if($code=='45KG')
                                        $BINH_KG = KL_BINH_45;
                                echo ActiveRecord::formatCurrency($sum_col_total_month_all_agent*$BINH_KG);?>
                            </td>
                            <td class="item_r item_b">
                                <?php echo ActiveRecord::formatCurrency($average*$BINH_KG); ?>
                            </td>
                            <?php foreach($days_of_month as $day): ?>																	
                            <td class="item_r item_b">
                                <?php 
                                $sum_day = isset($OUTPUT[$code]['sum_col'][$month][$day])?$OUTPUT[$code]['sum_col'][$month][$day]:"";
                                echo ActiveRecord::formatCurrency($sum_day*$BINH_KG); ?>
                            </td>
                            <?php endforeach; // end <?php foreach($days_of_month as $day) ?>                        
                        </tr>
                        <!-- tổng cộng -->

                    </tbody>

                </table><!-- end <table class="table_statistic items table_mon-->
            </div> <!-- end <div class="grid-view-scroll">-->
        <?php endforeach; // end foreach(CmsFormatter::$MATERIAL_TYPE_OUTPUT_STATISTIC as $type_output)?>    
            
         <?php include 'Revenue_output_money_bank.php'; ?>
         <?php include 'Revenue_output_total_revenue.php'; ?>
            
        </div><!-- end <div class="grid-view">-->
    </div> <!-- end <div id="tabs-"> -->
<?php endforeach; // end foreach($MONTHS as $month) ?>

<?php endif; // end if(count($data)) ?>
