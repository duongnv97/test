<?php
    $days_of_month = isset($TOTAL_REVENUE['days'][$month])?$TOTAL_REVENUE['days'][$month]:array();                
    $STT = 0;
?>
<div class="title_table_statistic">TỔNG DOANH THU ĐẠI LÝ THÁNG <?php echo $month;?> NĂM <?php echo $YEARS;?></div>
<div class="grid-view-scroll">
    <table class="hm_table table_statistic items table_month_<?php echo $month;?>" style="">
        <thead>
            <tr>                               
                <th class="w-10 item_c">STT</th>
                <th class="w-70">Tên đại lý</th>
                <th class="w-50">Tổng</th>
                <th class="w-50">BQ</th>
                <?php foreach($days_of_month as $day): ?>
                <th class="w-50" style="">
                    <?php echo $day;?>
                </th>
                <?php endforeach;?>
            </tr>
        </thead>
        <tbody>
            <?php if(isset($TOTAL_REVENUE['sum_row_total_month'][$month])):?>
                <?php foreach($TOTAL_REVENUE['sum_row_total_month'][$month] as $agent_id=>$total_month_agent):?>
                <?php $STT++;?>
                <tr>
                    <td class=""><?php echo $STT; ?></td>
                    <td class=""><?php echo $AGENT_MODEL[$agent_id]->first_name; ?></td>
                    <td class="item_r item_b"><?php echo ActiveRecord::formatCurrency($total_month_agent);?></td>
                    <td class="item_r"><?php $average = round($total_month_agent/count($days_of_month)); 
                        echo ActiveRecord::formatCurrency($average); ?>
                    </td>
                    <?php foreach($days_of_month as $day): ?>																	
                    <td class="item_r">
                        <?php 
                            $output_day = isset($TOTAL_REVENUE[$month][$day][$agent_id])?$TOTAL_REVENUE[$month][$day][$agent_id]:"";
                            echo $output_day>0?ActiveRecord::formatCurrency($output_day):'';
                        ?>
                    </td>
                    <?php endforeach; // end <?php foreach($days_of_month as $day) ?>
                </tr>
                <?php ?>
                <?php endforeach; // end foreach($TOTAL_REVENUE['sum_row_total_month'][$month]  ?>
                <!-- tổng cộng -->
                <tr>
                    <td class="">&nbsp;</td>
                    <td class="item_b">Tổng Cộng</td>
                    <td class="item_r item_b">
                        <?php $sum_col_total_month_all_agent = $TOTAL_REVENUE['sum_col_total_month_all_agent'][$month];
                        echo ActiveRecord::formatCurrency($sum_col_total_month_all_agent);
                        ?>
                    </td>
                    <td class="item_r item_b">
                        <?php 
                            $average = round($sum_col_total_month_all_agent/count($days_of_month));
                        echo ActiveRecord::formatCurrency($average); ?>
                    </td>
                    <?php foreach($days_of_month as $day): ?>																	
                    <td class="item_r item_b">
                        <?php 
                            $sum_day = isset($TOTAL_REVENUE['sum_col'][$month][$day])?$TOTAL_REVENUE['sum_col'][$month][$day]:"";
                            echo $sum_day>0?ActiveRecord::formatCurrency($sum_day):'';
                        ?>
                    </td>
                    <?php endforeach; // end <?php foreach($days_of_month as $day) ?>                        
                </tr>
                <!-- tổng cộng -->
            <?php endif;?>    
        </tbody>
    </table><!-- end <table class="table_statistic items table_mon-->
</div> <!-- end <div class="grid-view-scroll">-->
