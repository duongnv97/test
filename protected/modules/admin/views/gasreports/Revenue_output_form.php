<?php if(count($data)) :?>
<?php Yii::app()->clientScript->registerCoreScript('jquery.ui'); ?>
<?php 
if($this->canExportExcelForRevenueOutput()):
    $menus=array(
        array('label'=> 'Xuất excel', 
            'url'=>array('revenue_output', 'ExportExcel'=>1),
            'htmlOptions'=>array('class'=>'export_excel ','label'=>'Xuất Excel'),
            ),
);
endif;
$this->menu= ControllerActionsName::createMenusRoles($menus, $actions);
?>
<?php
$gridCss = Yii::app()->getAssetManager()->publish(Yii::getPathOfAlias('zii.widgets.assets')).'/gridview/styles.css';
Yii::app()->getClientScript()->registerCssFile($gridCss);
$OUTPUT = $data['OUTPUT'];
$DETAIL_12KG = isset($data['DETAIL_12KG'])?$data['DETAIL_12KG']:array();
$MONEY_BANK = isset($data['MONEY_BANK'])?$data['MONEY_BANK']:array();
$TOTAL_REVENUE = isset($data['TOTAL_REVENUE'])?$data['TOTAL_REVENUE']:array();
        
$MONTHS = !empty($data['month']) ? $data['month'] : [];
$month = $model->statistic_month;
// tab total year -- sẽ làm include sau

// tab each month
$AGENT_MODEL = Users::getArrObjectUserByRoleHaveOrder (ROLE_AGENT, 'code_account');
$_SESSION['data-excel']['AGENT_MODEL'] = $AGENT_MODEL;
$YEARS = $model->statistic_year;
$_SESSION['data-excel']['YEARS'] = $YEARS;
$_SESSION['data-excel']['MONTH'] = $month;
?>
<!--<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/jquery.floatThead.min.js"></script>    
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/underscore-min.js"></script>    -->
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/scroll/jquery.freezetablecolumns.1.1.js"></script> 

<script>
$(function() {
    $( "#tabs" ).tabs();
    fnTabMonthClick();
});

$(window).load(function(){
    var index = 1;
    $('.freezetablecolumns').each(function(){
        if(index==1)
            $(this).closest('div.grid-view').show();
        index++;
    });    
    fnAddClassOddEven('items');
});

function fnTabMonthClick(){
    $('.freezetablecolumns').each(function(){
        var id_table = $(this).attr('id');
        $('#'+id_table).freezeTableColumns({
                width:       <?php echo Users::WIDTH_freezetable;?>,   // required - bt 1100
                height:      400,   // required
                numFrozen:   4,     // optional
                frozenWidth: 342,   // optional
                clearWidths: true  // optional
              });   
    });
    $('.freezetablecolumns_money').each(function(){
        var id_table = $(this).attr('id');
        $('#'+id_table).freezeTableColumns({
                width:       <?php echo Users::WIDTH_freezetable;?>,   // required - bt 1100
                height:      400,   // required
                numFrozen:   4,     // optional
                frozenWidth: 400,   // optional
                clearWidths: true  // optional
              });   
    });
}

</script>


<?php if(!empty($model->statistic_month)):?>
<div id="tabs">
    <ul>
        <!--<li><a class="tab_month" month_current="<?php echo $YEARS;?>" href="#tabs-<?php echo $YEARS;?>">Năm <?php echo $YEARS;?></a></li>-->
        <?php foreach(GasStoreCard::$TYPE_CUSTOMER as $key=>$type_customer_code):?>
            <?php 
                if(!isset($DETAIL_12KG[$type_customer_code]['days'][$month]))
                continue;
                $text12 = 'Hộ Gia Đình';
                if(in_array($key, CmsFormatter::$aTypeIdBoMoi)){
                    $text12 = CmsFormatter::$CUSTOMER_BO_MOI[$key];
                }
            ?>
            <li><a class="tab_month" month_current="<?php echo $type_customer_code;?>" href="#tabs-<?php echo $type_customer_code;?>"><?php echo $text12;?> 12KG</a></li>
        <?php endforeach; // end foreach(GasStoreCard::$TYPE_CUSTOMER ?>

        <?php foreach(CmsFormatter::$MATERIAL_TYPE_OUTPUT_STATISTIC as $type_output):?>
            <?php $code = $type_output['code']; 
                $textcode = $code;
                if($code=='45KG'){
                    $textcode = "Bình Bò";
                }
            ?>
            <li><a class="tab_month" month_current="<?php echo $code;?>" href="#tabs-<?php echo $code;?>">Tổng <?php echo $textcode;?></a></li>
        <?php endforeach; // end foreach(CmsFormatter::$MATERIAL_TYPE_OUTPUT_STATISTIC ?>
<!--        <li><a class="tab_month" month_current="MONEY_BANK" href="#tabs-MONEY_BANK">Nộp Công Ty</a></li>
        <li><a class="tab_month" month_current="TOTAL_REVENUE" href="#tabs-TOTAL_REVENUE">Doanh Thu</a></li>-->
    </ul>

    <?php include 'Revenue_output_form_detail_12kg.php'; ?>  

    <?php foreach(CmsFormatter::$MATERIAL_TYPE_OUTPUT_STATISTIC as $type_output):?>
        <?php  // lặp 2 loại bình thống kê là 12 và 45kg        
            $code = $type_output['code']; 
            if(!isset($OUTPUT[$code]['days'][$month]))
                continue;
            $days_of_month = $OUTPUT[$code]['days'][$month];
            $STT = 0;
             $textcode = $code;
            if($code=='45KG'){
                $textcode = "Bình Bò";
            }
        ?>

        <div id="tabs-<?php echo $code;?>">
            <div class="grid-view ">
                <div class="title_table_statistic">TỔNG SẢN LƯỢNG GAS <?php echo $textcode;?> THÁNG <?php echo $month;?> NĂM <?php echo $YEARS;?></div>
                <div class="">
                    <table id="freezetablecolumns<?php echo $code;?>" class="freezetablecolumns items">
                        <thead>
                            <tr>                               
                                <th class="w-30 item_c">STT</th>
                                <th class="w-150">Tên đại lý</th>
                                <th class="w-70">Tổng</th>
                                <th class="w-50">BQ</th>
                                <?php foreach($days_of_month as $day): ?>
                                <th class="w-50" style="">
                                    <?php echo $day;?>
                                </th>
                                <?php endforeach;?>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach($OUTPUT[$code]['sum_row_total_month'][$month] as $agent_id=>$total_month_agent):?>
                            <?php $STT++;?>
                            <tr>
                                <td class=""><?php echo $STT; ?></td>
                                <td class=""><?php echo $AGENT_MODEL[$agent_id]->first_name; ?></td>
                                <td class="item_r item_b"><?php echo ActiveRecord::formatCurrency($total_month_agent);?></td>
                                <td class="item_r item_b"><?php $average = round($total_month_agent/count($days_of_month)); 
                                    echo ActiveRecord::formatCurrency($average); ?>
                                </td>
                                <?php foreach($days_of_month as $day): ?>																	
                                <td class="item_r">
                                    <?php 
                                        $output_day = isset($OUTPUT[$code][$month][$day][$agent_id])?$OUTPUT[$code][$month][$day][$agent_id]:"";
                                        echo $output_day>0?ActiveRecord::formatCurrency($output_day):'';
                                    ?>
                                </td>
                                <?php endforeach; // end <?php foreach($days_of_month as $day) ?>
                            </tr>
                            <?php ?>
                            <?php endforeach; // end foreach($OUTPUT[$code]['sum_row_total_month'][$month]  ?>
                            <!-- tổng cộng -->
                            <tr>
                                <td class="">&nbsp;</td>
                                <td class="item_b">Tổng Cộng ( bình )</td>
                                <td class="item_r item_b">
                                    <?php $sum_col_total_month_all_agent = $OUTPUT[$code]['sum_col_total_month_all_agent'][$month];
                                    echo ActiveRecord::formatCurrency($sum_col_total_month_all_agent);?>
                                </td>
                                <td class="item_r item_b">
                                    <?php 
                                        $average = round($sum_col_total_month_all_agent/count($days_of_month));
                                    echo ActiveRecord::formatCurrency($average); ?>
                                </td>
                                <?php foreach($days_of_month as $day): ?>																	
                                <td class="item_r item_b">
                                    <?php 
                                        $sum_day = isset($OUTPUT[$code]['sum_col'][$month][$day])?$OUTPUT[$code]['sum_col'][$month][$day]:"";
                                        echo $sum_day>0?ActiveRecord::formatCurrency($sum_day):'';
                                    ?>
                                </td>
                                <?php endforeach; // end <?php foreach($days_of_month as $day) ?>                        
                            </tr>
                            <tr>
                                <td class="">&nbsp;</td>
                                <td class="item_b">Tổng Cộng ( KG )</td>
                                <td class="item_r item_b">
                                    <?php 
                                        $BINH_KG = KL_BINH_12;
                                        if($code=='45KG')
                                            $BINH_KG = KL_BINH_45;
                                    echo ActiveRecord::formatCurrency($sum_col_total_month_all_agent*$BINH_KG);?>
                                </td>
                                <td class="item_r item_b">
                                    <?php echo ActiveRecord::formatCurrency($average*$BINH_KG); ?>
                                </td>
                                <?php foreach($days_of_month as $day): ?>																	
                                <td class="item_r item_b">
                                    <?php 
                                    $sum_day = isset($OUTPUT[$code]['sum_col'][$month][$day])?$OUTPUT[$code]['sum_col'][$month][$day]:"";
                                    echo ActiveRecord::formatCurrency($sum_day*$BINH_KG); ?>
                                </td>
                                <?php endforeach; // end <?php foreach($days_of_month as $day) ?>                        
                            </tr>
                            <!-- tổng cộng -->

                        </tbody>

                    </table><!-- end <table class="table_statistic items table_mon-->
                </div> <!-- end <div class="grid-view-scroll">-->

            </div><!-- end <div class="grid-view">-->
        </div> <!-- end <div id="tabs-"> -->
    <?php endforeach; // end foreach(CmsFormatter::$MATERIAL_TYPE_OUTPUT_STATISTIC as $type_output)?>    

    <?php // include 'Revenue_output_money_bank.php'; ?>
    <?php // include 'Revenue_output_total_revenue.php'; ?>
</div> <!-- end <div id="tabs">-->

<?php else: ?>
    <?php include 'Revenue_output_year/form_year.php'; ?>
<?php endif; // end if(!empty($model->statistic_month)): ?>
<?php endif; // end if(count($data)) ?>
