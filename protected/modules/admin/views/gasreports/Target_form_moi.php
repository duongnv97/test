<?php 
    $cDate = date('d');
    if($model->statistic_month!=date('m')){
        $cDate = cal_days_in_month(0, $model->statistic_month, $model->statistic_year);
    }
    $STT=1;
    $SUM_COL_SALE_MOI = 0;
    $ARR_SUM_COL_SALE_MOI = array();
?>

<div id="tabs-SALE_MODEL_MOI">
    <!--<h1><?php echo $textInfo;?></h1>-->
    <div class="clr"></div>
    <div class="grid-view display_none GRID_SALE_MODEL_MOI">
        <table class=" items sale_moi_tb freezetablecolumns" id="freezetablecolumns" style="">
        <thead>
            <tr>                               
                <th class="w-30 item_l">STT</th>
                <th class="w-250 item_l">Tên Sale</th>
                <th class="w-50">Tổng</th>
                <!--<th class="w-50">BQ</th>-->
                <?php for($i=1; $i<=$cDate; $i++): ?>
                <th class="w-30" style="">
                    <?php echo $i<10?"0$i":$i;?>
                </th>
                <?php endfor;?>
            </tr>
        </thead>
        
        <tbody>
                <?php foreach($SALE_MOI_SORT_TOTAL as $uid=>$total): ?>
                <?php if(isset($SALE_MODEL_MOI[$uid])): ?>
                <tr>
                    <td class="item_b"><?php echo $STT++; ?></td>
                    <td class="w-150a item_b"><?php echo $SALE_MODEL_MOI[$uid]->first_name; ?></td>
                    <td class="w-50a sale_moi_total item_c item_b"><?php echo $total>0?ActiveRecord::formatCurrency($total):"";?></td>
                    <?php $SUM_COL_SALE_MOI+= $total; ?>
                    <?php for($i=1; $i<=$cDate; $i++): ?>
                        <?php 
                              $i = $i<10?"0$i":$i;
                              $date = "$model->statistic_year-$model->statistic_month-$i";
                              $outputDay = isset($data['SALE_MOI'][$date][$uid])?$data['SALE_MOI'][$date][$uid]:0; 
                              if(!isset($ARR_SUM_COL_SALE_MOI[$date]))
                                $ARR_SUM_COL_SALE_MOI[$date] = $outputDay;
                              else    
                                $ARR_SUM_COL_SALE_MOI[$date] += $outputDay;
                                          
//                              $total_row += $outputDay;
                        ?>
                        <td class="w-30a item_c item_b">
                            <?php echo $outputDay>0?ActiveRecord::formatCurrency($outputDay):"";?>                            
                        </td>
                    <?php endfor;?>
                </tr>
                <?php endif;?>
                <?php endforeach; // end foreach($aModelUser as $mUser): ?>
                
                <tr>
                    <td>&nbsp;</td>
                    <td class="item_b item_c">Tổng Cộng</td>
                    <td class="w-30 item_c item_b">
                        <?php echo $SUM_COL_SALE_MOI>0?ActiveRecord::formatCurrency($SUM_COL_SALE_MOI):"";?>
                    </td>
                    <?php for($i=1; $i<=$cDate; $i++): ?>
                        <?php 
                              $i = $i<10?"0$i":$i;
                              $date = "$model->statistic_year-$model->statistic_month-$i";
                              $outputDay = isset($ARR_SUM_COL_SALE_MOI[$date])?$ARR_SUM_COL_SALE_MOI[$date]:0; 
                        ?>
                        <td class="w-30 item_c item_b">
                            <?php echo $outputDay>0?ActiveRecord::formatCurrency($outputDay):"";?>                            
                        </td>
                    <?php endfor;?>
                    
                </tr>
        </tbody>
        </table>
    </div><!-- end <div class="grid-view grid-view-scroll">-->    
</div>
