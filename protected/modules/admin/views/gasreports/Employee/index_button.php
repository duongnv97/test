<div class="form">
    <?php
        $LinkNormal = Yii::app()->createAbsoluteUrl('admin/gasreports/employeeMaintain');
        $LinkWait   = Yii::app()->createAbsoluteUrl('admin/gasreports/employeeMaintain', ['detail'=> STATUS_ACTIVE]);
    ?>
    <h1><?php echo $this->pageTitle;?>
        <a class='btn_cancel f_size_14 <?php echo (!isset($_GET['detail'])) ? "active":"";?>' href="<?php echo $LinkNormal;?>">Tổng quát</a>
        <a class='btn_cancel f_size_14 <?php echo isset($_GET['detail']) && $_GET['detail']==1 ? "active":"";?>' href="<?php echo $LinkWait;?>">Chi tiết</a>
    </h1> 
</div>