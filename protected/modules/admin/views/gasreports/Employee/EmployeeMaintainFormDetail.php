<?php
$EMPLOYEE_ID        = $rData['EMPLOYEE_ID']; $index = 1;
$OUTPUT             = $rData['OUTPUT'];
$OUTPUT_GAS         = $rData['OUTPUT_GAS'];
$MATERIALS_ID       = $rData['MATERIALS_ID'];
$OUTPUT_BOBINH      = isset($rData['OUTPUT_BOBINH']) ? $rData['OUTPUT_BOBINH'] : [];
$QTY_BOBINH         = isset($rData['QTY_BOBINH']) ? $rData['QTY_BOBINH'] : [];
$UPHOLD_HGD         = isset($rData['UPHOLD_HGD']) ? $rData['UPHOLD_HGD'] : [];
$mAppCache          = new AppCache();
$aEmployee          = $mAppCache->getListdataUserByRole(ROLE_EMPLOYEE_MAINTAIN);
$aMaterial          = $mAppCache->getListdata('GasMaterials', AppCache::LISTDATA_MATERIAL);
$aModelMaterial     = $mAppCache->getMasterModel('GasMaterials', AppCache::ARR_MODEL_MATERIAL );
$aAgent             = $mAppCache->getAgentListdata();
//$priceBoBinh = 0; $priceUphold = 0; $price = 0;
//Sta2::employeeMaintainCalcPrice($priceBoBinh, Sta2::SALARY_TYPE_BOBINH, 1, 0, 0, 0);
//Sta2::employeeMaintainCalcPrice($priceUphold, Sta2::SALARY_TYPE_UPHOLD, 1, 0, 0, 0);
$aDataCache         = $mAppCache->getPriceHgd(AppCache::PRICE_HGD);
$aUserCredit        = $mAppCache->getUserCredit();
$aCreditPay         = GasConst::getArrCreditMapPay();
$mPriceHgd          = new UsersPriceHgd();
$mPriceHgd->r_type  = Sta2::SALARY_TYPE_BOBINH;
$mPriceHgd->r_qty   = 1;
$mPriceHgd->aDataCache  = $aDataCache;
$priceBoBinh        = $mPriceHgd->calcPayEmployee();

$mPriceHgd          = new UsersPriceHgd();
$mPriceHgd->r_type  = Sta2::SALARY_TYPE_UPHOLD;
$mPriceHgd->r_qty   = 1;
$mPriceHgd->aDataCache  = $aDataCache;
$priceUphold        = $mPriceHgd->calcPayEmployee();
?>

<div class="title_table_statistic"><?php echo $this->pageTitle;?>
        <?php echo " Từ Ngày $model->date_from Đến Ngày $model->date_to"; ?>
</div>
<div class="grid-view display_none">
    <!--<table class="hm_table table_statistic items table_month_1" style="">-->
    <table id="freezetablecolumns_inventory" class="hm_table items freezetablecolumns" style="">
        <thead>
            <tr class="h_50">
                <th class="w-20">#</th>
                <th class="w-110">Nhân viên</th>
                <th class="w-110">Đại lý</th>
                <th class="w-60 ">Thành tiền</th>
                <th class="w-50">Bảo trì<br><?php echo ActiveRecord::formatCurrency($priceUphold); ?></th>
                <th class="w-50">Bộ bình<br><?php echo ActiveRecord::formatCurrency($priceBoBinh); ?></th>
                <?php foreach (CmsFormatter::$MATERIAL_VALUE_KG as $materials_type_id => $kg): ?>
                <?php 
//                    $price = 0; 
//                    Sta2::employeeMaintainCalcPrice($price, 1, 1, 0, $materials_type_id, 0);
                    $mPriceHgd              = new UsersPriceHgd();
                    $mPriceHgd->r_qty       = 1;
                    $mPriceHgd->r_materials_type_id = $materials_type_id;
                    $mPriceHgd->aDataCache  = $aDataCache;
                    $mPriceHgd->calcPayEmployee();
                ?>
                <th class="w-50">B<?php echo $kg;?> Kg<br><?php echo ActiveRecord::formatCurrency($mPriceHgd->r_price); ?></th>
                <?php endforeach; ?>
                <?php foreach ($MATERIALS_ID as $materials_id): ?>
                <?php if(empty($materials_id)) {continue;} ?>
                <?php 
                    $materials_type_id = isset($aModelMaterial[$materials_id]) ? $aModelMaterial[$materials_id]['materials_type_id'] : 0;
//                    $price = 0; 
//                    Sta2::employeeMaintainCalcPrice($price, 1, 1, 0, $materials_type_id, $materials_id);
                    $mPriceHgd          = new UsersPriceHgd();
                    $mPriceHgd->r_qty   = 1;
                    $mPriceHgd->r_materials_type_id = $materials_type_id;
                    $mPriceHgd->r_materials_id      = $materials_id;
                    $mPriceHgd->agent_id            = 100;
                    $mPriceHgd->aDataCache          = $aDataCache;
                    $mPriceHgd->calcPayEmployee();
                ?>
                <th class="w-130" style="white-space: normal;"><?php echo isset($aMaterial[$materials_id]) ? $aMaterial[$materials_id] : $materials_id;?>
                    <br><?php echo ActiveRecord::formatCurrency($mPriceHgd->r_price); ?>
                </th>
                <?php endforeach; ?>
                <th class="w-80 ">Thành tiền</th>
            </tr>
        </thead>
        <tbody>
        <?php $price = 0; foreach($EMPLOYEE_ID as $employee_maintain_id => $aAgentId): ?>
            <?php foreach ($aAgentId as $agent_id): ?>
            <?php
                if(!$model->allowAccessAgent($agent_id) ||empty($employee_maintain_id)){ continue ;}
                $sumRow         = 0;
                $employeeName   = isset($aEmployee[$employee_maintain_id]) ? $aEmployee[$employee_maintain_id] : '';
                if(empty($employeeName)){
                    $employeeName = Users::model()->findByPk($employee_maintain_id)->first_name;
                }
                
                $agentName      = isset($aAgent[$agent_id]) ? $aAgent[$agent_id] : '';
                $qtyUphold         = isset($UPHOLD_HGD[$employee_maintain_id][$agent_id]) ? $UPHOLD_HGD[$employee_maintain_id][$agent_id] : '';
                $qtyBoBinh         = isset($QTY_BOBINH[$employee_maintain_id][$agent_id]) ? $QTY_BOBINH[$employee_maintain_id][$agent_id] : '';
//                $amountBoBinh   = Sta2::employeeMaintainCalcPrice($price, Sta2::SALARY_TYPE_BOBINH, $qtyBoBinh, $employee_maintain_id, 0, 0);
//                $amountUphold   = Sta2::employeeMaintainCalcPrice($price, Sta2::SALARY_TYPE_UPHOLD, $qtyUphold, $employee_maintain_id, 0, 0);
                $mPriceHgd              = new UsersPriceHgd();
                $mPriceHgd->r_type      = Sta2::SALARY_TYPE_BOBINH;
                $mPriceHgd->r_qty       = $qtyBoBinh;
                $mPriceHgd->agent_id    = $agent_id;
                $mPriceHgd->aDataCache  = $aDataCache;
                $amountBoBinh           = $mPriceHgd->calcPayEmployee();

                $mPriceHgd              = new UsersPriceHgd();
                $mPriceHgd->r_type      = Sta2::SALARY_TYPE_UPHOLD;
                $mPriceHgd->r_qty       = $qtyUphold;
                $mPriceHgd->agent_id    = $agent_id;
                $mPriceHgd->aDataCache  = $aDataCache;
                $mPriceHgd->aUserCredit = $aUserCredit;
                $mPriceHgd->r_employee_maintain_id  = $employee_maintain_id;
                $amountUphold           = $mPriceHgd->calcPayEmployee();

                $sumRow += ($amountBoBinh + $amountUphold);
                if($amountBoBinh){
                    $amountBoBinh = '<br>['.ActiveRecord::formatCurrencyRound($amountBoBinh).']';
                }
                if($amountUphold){
                    $amountUphold = '<br>['.ActiveRecord::formatCurrencyRound($amountUphold).']';
                }
            ?>
            <tr>
                <td><?php echo $index++;?><br>.</td>
                <td><?php echo $employeeName.$mPriceHgd->getTextPay();?></td>
                <td><?php echo $agentName;?></td>
                <td class="grandTotalFirst item_r item_b"></td>
                <td class="item_c"><?php echo $qtyUphold > 0 ? $qtyUphold.$amountUphold : '';?></td>
                <td class="item_c"><?php echo $qtyBoBinh >0 ? $qtyBoBinh.$amountBoBinh : '';?></td>
                <?php foreach (CmsFormatter::$MATERIAL_VALUE_KG as $materials_type_id => $kg): ?>
                    <?php 
                        $price = 0;
                        $qty = isset($OUTPUT_GAS[$employee_maintain_id][$agent_id][$materials_type_id]) ? $OUTPUT_GAS[$employee_maintain_id][$agent_id][$materials_type_id] : 0;
//                        $amount     = Sta2::employeeMaintainCalcPrice($price, 1, $qty, $employee_maintain_id, $materials_type_id, 0);
                        $mPriceHgd              = new UsersPriceHgd();
                        $mPriceHgd->r_qty       = $qty;
                        $mPriceHgd->agent_id    = $agent_id;
                        $mPriceHgd->r_materials_type_id = $materials_type_id;
                        $mPriceHgd->aDataCache  = $aDataCache;
                        $mPriceHgd->aUserCredit = $aUserCredit;
                        $mPriceHgd->r_employee_maintain_id  = $employee_maintain_id;
                        $amount                 = $mPriceHgd->calcPayEmployee();
                        $sumRow     += $amount;
                        if($amount){
                            $amount = '<br>['.ActiveRecord::formatCurrencyRound($amount).']';
                        }
                    ?>
                    <td class="item_c">
                        <?php echo $qty > 0 ? ActiveRecord::formatCurrency($qty).$amount : ''; ?>
                    </td>
                <?php endforeach; ?>
                <?php foreach ($MATERIALS_ID as $materials_id): ?>
                    <?php 
                        if(empty($materials_id)) {continue;} 
                        $price = 0;
                        $materials_type_id = isset($aModelMaterial[$materials_id]) ? $aModelMaterial[$materials_id]['materials_type_id'] : 0;
                        $qty        = isset($OUTPUT[$employee_maintain_id][$agent_id][$materials_id]) ? $OUTPUT[$employee_maintain_id][$agent_id][$materials_id] : 0;
                        $qtyBoBinh  = isset($OUTPUT_BOBINH[$employee_maintain_id][$agent_id][$materials_id]) ? $OUTPUT_BOBINH[$employee_maintain_id][$agent_id][$materials_id] : 0;
                        if($materials_type_id != GasMaterialsType::MATERIAL_TYPE_BEP){
    //                        $qty    = $qty - $qtyBoBinh;// Jul2717 tại sao lại phải trừ vật tư bộ bình đi, khi mà đã tính riêng qty val dây cho bộ bình rồi
                        }else{// Jul2617 bếp tính riêng công, không cộng vào bộ bình nên không trừ
                            $qty    = $qty + $qtyBoBinh;
                        }
//                        $amount     = Sta2::employeeMaintainCalcPrice($price, 1, $qty, $employee_maintain_id, $materials_type_id, $materials_id);
                        $mPriceHgd              = new UsersPriceHgd();
                        $mPriceHgd->r_qty       = $qty;
                        $mPriceHgd->agent_id    = $agent_id;
                        $mPriceHgd->r_materials_type_id = $materials_type_id;
                        $mPriceHgd->r_materials_id      = $materials_id;
                        $mPriceHgd->aDataCache          = $aDataCache;
                        $amount             = $mPriceHgd->calcPayEmployee();
                        $sumRow     += $amount;
                        if($amount){
                            $amount = '<br>['.ActiveRecord::formatCurrencyRound($amount).']';
                        }else{
                            $amount = '';
                        }
                    ?>
                    <td class="item_c w-80">
                        <?php echo $qty > 0 ? ActiveRecord::formatCurrency($qty).$amount : ''; ?>
                    </td>
                <?php endforeach; ?>
                <td class="item_r grandTotalLast">
                    <?php echo $sumRow > 0 ? ActiveRecord::formatCurrencyRound($sumRow) : ''; ?>
                </td>
            </tr>

            <?php endforeach; // end foreach ($aAgentId as $agent_id) ?>
        <?php endforeach; // end foreach($EMPLOYEE_ID as $employee_maintain_id => $aAgentId ?>
        </tbody>
    </table><!-- end <table class="table_statistic items table_mon-->
</div> <!-- end <div class="grid-view-scroll">-->

<?php Yii::app()->clientScript->registerCoreScript('jquery.ui'); ?>
<?php
$gridCss = Yii::app()->getAssetManager()->publish(Yii::getPathOfAlias('zii.widgets.assets')).'/gridview/styles.css';
Yii::app()->getClientScript()->registerCssFile($gridCss);
?>
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/scroll/jquery.freezetablecolumns.1.1.js"></script> 

<script>
$(function() {
//    $( "#tabs" ).tabs();
    $(".items > tbody > tr:odd").addClass("odd");
    $(".items > tbody > tr:even").addClass("even");   
    formatGrandTotal();
    fnTabMonthClick();
});

function formatGrandTotal(){
    $('.grandTotalLast').each(function(){
        var tr = $(this).closest('tr');
        tr.find('.grandTotalFirst').text($(this).text());
    });
}

function fnTabMonthClick(){
     $('.freezetablecolumns').each(function(){
        var id_table = $(this).attr('id');
        $('#'+id_table).freezeTableColumns({
            width:       <?php echo Users::WIDTH_freezetable;?>,   // required - bt 1100
            height:      400,   // required
            numFrozen:   4,     // optional
            frozenWidth: 345,   // optional
            clearWidths: true  // optional
        });   
    });
}
$(window).load(function(){
    var index = 1;
    $('.freezetablecolumns').each(function(){
        if(index==1)
            $(this).closest('div.grid-view').show();
        index++;
    });    
    fnAddClassOddEven('items');
});

</script>