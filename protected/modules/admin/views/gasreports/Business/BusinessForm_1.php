<?php Yii::app()->clientScript->registerCoreScript('jquery.ui'); ?>
<?php
$gridCss = Yii::app()->getAssetManager()->publish(Yii::getPathOfAlias('zii.widgets.assets')).'/gridview/styles.css';
Yii::app()->getClientScript()->registerCssFile($gridCss);
?>
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/jquery.floatThead.min.js"></script>    
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/underscore-min.js"></script>    

<script>
$(function() {
    $( "#tabs" ).tabs();
    $(".items > tbody > tr:odd").addClass("odd");
    $(".items > tbody > tr:even").addClass("even");   
    fnTabMonthClick();
});

function fnTabMonthClick(){
     // floatThead for first tab
     var index=1;
     $('.grid-view').each(function(){
        if(index==1){
            var tableItem = $(this).find('.items').eq(0);
            $(this).find('.items').floatThead();
        }
        index++;
    });    
    // floatThead for first tab
}

</script>

<?php
$CUSTOMER_MODEL = $data['CUSTOMER_MODEL'];
$OUTPUT         = $data['OUTPUT'];
$cPrice         = $data['PRICE'];
$sumB50=0;
$sumB45=0;
$sumB12=0;
$sumB6=0;

?>

<div class="title_table_statistic"><?php echo $this->pageTitle;?>
        <?php echo CmsFormatter::$CUSTOMER_BO_MOI[$model->ext_is_maintain]; echo " Từ Ngày $model->date_from Đến Ngày $model->date_to"; ?>
</div>
<div class="grid-view grid-view-scroll">
    <!--<table class="hm_table table_statistic items table_month_1" style="">-->
    <table class="hm_table items " style="">
        <thead>
            <tr>                               
                <th class="">#</th>
                <th class="w-150">Tên KH</th>
                <th class="w-250">Địa Chỉ</th>
                <th class="w-50">Bình 50</th>
                <th class="w-50">Bình 45</th>
                <th class="w-50">Bình 12</th>
                <th class="w-50">Bình 6</th>
                <th class="w-50">Gas Dư</th>
                <th class="w-50">Qty</th>
                <th class="w-50">Sale</th>
                <!--<th class="w-100">Đại Lý</th>-->
            </tr>
        </thead>
        <tbody>
            <?php foreach ($ARR_DAYS as $date_delivery):  ?>
                <?php if(isset($data['data'][$date_delivery])):?>
                    <?php foreach($data['data'][$date_delivery] as $agent_id=>$arr_customer_data):?>
                        <?php foreach($arr_customer_data as $customer_id=>$arr_type_obj):?>
                        <tr>
                            <td class=""><?php echo $cmsFormater->formatDate($date_delivery); ?></td>
                            <!--<td class=""><?php // echo $CUSTOMER_OBJ[$customer_id]->code_bussiness; ?></td>-->
                            <td class=""><?php echo $cmsFormater->formatNameUser($CUSTOMER_OBJ[$customer_id]); ?></td>
                            <?php if(Yii::app()->user->role_id!=ROLE_SUB_USER_AGENT):?>
                            <td class=""><?php echo $CUSTOMER_OBJ[$customer_id]->address; ?></td>
                            <?php endif;?>
                            <?php 
                                $amount_gas_du = 0;
                                $GAS_REMAIN = 0;
                                $BINH_50 = isset($data['data'][$date_delivery][$agent_id][$customer_id][MATERIAL_TYPE_BINHBO_50])?$data['data'][$date_delivery][$agent_id][$customer_id][MATERIAL_TYPE_BINHBO_50]->qty:0;
                                $BINH_45 = isset($data['data'][$date_delivery][$agent_id][$customer_id][MATERIAL_TYPE_BINHBO_45])?$data['data'][$date_delivery][$agent_id][$customer_id][MATERIAL_TYPE_BINHBO_45]->qty:0;
                                $BINH_12 = isset($data['data'][$date_delivery][$agent_id][$customer_id][MATERIAL_TYPE_BINH_12])?$data['data'][$date_delivery][$agent_id][$customer_id][MATERIAL_TYPE_BINH_12]->qty:0;
                                $BINH_6 = isset($data['data'][$date_delivery][$agent_id][$customer_id][MATERIAL_TYPE_BINH_6])?$data['data'][$date_delivery][$agent_id][$customer_id][MATERIAL_TYPE_BINH_6]->qty:0;
                                if(isset($data['GasRemain'][$date_delivery][$agent_id][$customer_id])){
                                    $GAS_REMAIN = $data['GasRemain'][$date_delivery][$agent_id][$customer_id]->amount_gas;
                                    // vì cái phải thu KH receivables_customer đã thực tế trừ đi gas dư rồi. nên không trừ ở đây nữa
                                    // liên quan dòng trên, vì có trường hợp đại lý chỉ cân gas dư cho KH trong ngày hôm đó, nghĩa là hôm đó ko có bán hàng ( xuất thẻ kho )
                                    // nên sẽ hiển thị số tiền gss dư vào đây
                                    if($BINH_50==0&&$BINH_45==0){ // nếu ngày hôm đó chỉ cân gas dư cho KH
                                        $amount_gas_du = ( 0-$data['GasRemain'][$date_delivery][$agent_id][$customer_id]->amount );
                                    }                                    
                                }
                                
                                // ko hiểu chỗ này check để làm gì luôn
//                                if(isset($data['GasRemain'][$date_delivery][$customer_id]) && $data['GasRemain'][$date_delivery][$customer_id]->agent_id!=$data['agent'][$customer_id]){
//                                    $GAS_REMAIN=0;
//                                }
                                $QTY = $BINH_50*KL_BINH_50 + $BINH_45*KL_BINH_45 + $BINH_12*KL_BINH_12 + $BINH_6*KL_BINH_6 - $GAS_REMAIN; 
                                $VO_50 = isset($data['data'][$date_delivery][$agent_id][$customer_id][MATERIAL_TYPE_VO_50])?$data['data'][$date_delivery][$agent_id][$customer_id][MATERIAL_TYPE_VO_50]->qty:0;
                                $VO_45 = isset($data['data'][$date_delivery][$agent_id][$customer_id][MATERIAL_TYPE_VO_45])?$data['data'][$date_delivery][$agent_id][$customer_id][MATERIAL_TYPE_VO_45]->qty:0;
                                $VO_12 = isset($data['data'][$date_delivery][$agent_id][$customer_id][MATERIAL_TYPE_VO_12])?$data['data'][$date_delivery][$agent_id][$customer_id][MATERIAL_TYPE_VO_12]->qty:0;
                                $VO_6 = isset($data['data'][$date_delivery][$agent_id][$customer_id][MATERIAL_TYPE_VO_6])?$data['data'][$date_delivery][$agent_id][$customer_id][MATERIAL_TYPE_VO_6]->qty:0;
                                $sum_B_50+=$BINH_50;
                                $sum_B_45+=$BINH_45;
                                $sum_B_12+=$BINH_12;
                                $sum_B_6+=$BINH_6;
                                $sum_gas_remain+=$GAS_REMAIN;
                                $sum_qty+=$QTY;
                                $sum_VO_50+=$VO_50;
                                $sum_VO_45+=$VO_45;
                                $sum_VO_12+=$VO_12;
                                $sum_VO_6+=$VO_6;
                                $mStoreCard = isset($data['store_card'][$agent_id][$customer_id])?$STORE_CARD_OBJ[$data['store_card'][$agent_id][$customer_id]]:0;
                                $delivery_person = '';
                                $collection_customer_note = '';
                                $collection_customer_date = '';
                                if($mStoreCard){
                                    $delivery_person = $mStoreCard->delivery_person;
                                    $collection_customer_note = $mStoreCard->collection_customer_note;
                                    $collection_customer_date = $cmsFormater->formatDateTime($mStoreCard->collection_customer_date);
                                }
                                
                                $collection_customer = isset($data['collection_customer'][$date_delivery][$agent_id][$customer_id])?$data['collection_customer'][$date_delivery][$agent_id][$customer_id]:0;
                                $sum_collection_customer += $collection_customer;
                                $collection_customer_text = $collection_customer>0?ActiveRecord::formatCurrency($collection_customer):'';
                                
                                $receivables_customer = isset($data['receivables_customer'][$date_delivery][$agent_id][$customer_id])?$data['receivables_customer'][$date_delivery][$agent_id][$customer_id]:0;
                                
                                
                                $receivables_customer_final = ( $receivables_customer + $amount_gas_du );
                                $sum_receivables_customer += $receivables_customer_final;
                                $receivables_customer_text = ActiveRecord::formatCurrency($receivables_customer_final);
                                if($receivables_customer_final<0){
                                    $receivables_customer_text = "(".ltrim ($receivables_customer_text,'-').")";
                                }
                                
                                $QTY_text = ActiveRecord::formatCurrency($QTY);
                                if($QTY<0){
                                    $QTY_text = "(".ltrim ($QTY_text,'-').")";
                                }
                            ?>
                            
                            <?php // if($model->ext_is_maintain==STORE_CARD_KH_BINH_BO):?>    
                            <td class="item_r"><?php echo ($BINH_50!=0)?ActiveRecord::formatCurrency($BINH_50):""; ?></td>
                            <td class="item_r"><?php echo ($BINH_45!=0)?ActiveRecord::formatCurrency($BINH_45):""; ?></td>
                            <?php // endif;?>
                            <td class="item_r"><?php echo ($BINH_12!=0)?ActiveRecord::formatCurrency($BINH_12):""; ?></td>
                            <td class="item_r"><?php echo ($BINH_6!=0)?ActiveRecord::formatCurrency($BINH_6):""; ?></td>
                            
                            <?php // if($model->ext_is_maintain==STORE_CARD_KH_BINH_BO):?>
                            <td class="item_r">
                                <?php echo ($GAS_REMAIN!=0)?ActiveRecord::formatCurrency($GAS_REMAIN):""; ?>
                            </td>
                            <?php // endif;?>
                            <td class="item_r"><?php echo ($QTY!=0)?$QTY_text:""; ?></td>
                            <?php // if($model->ext_is_maintain==STORE_CARD_KH_BINH_BO):?>
                            <td class="item_r"><?php echo ($VO_50!=0)?ActiveRecord::formatCurrency($VO_50):""; ?></td>
                            <td class="item_r"><?php echo ($VO_45!=0)?ActiveRecord::formatCurrency($VO_45):""; ?></td>
                            <?php // endif;?>
                            <td class="item_r"><?php echo ($VO_12!=0)?ActiveRecord::formatCurrency($VO_12):""; ?></td>
                            <td class="item_r"><?php echo ($VO_6!=0)?ActiveRecord::formatCurrency($VO_6):""; ?></td>
                            <?php // if(Yii::app()->user->role_id!=ROLE_SUB_USER_AGENT):?>
                            <td class=""><?php echo $CUSTOMER_OBJ[$customer_id]->sale?$CUSTOMER_OBJ[$customer_id]->sale->first_name:''; ?></td>
                            <?php // endif;?>
                            <td class=""><?php echo $AGENT_MODEL[$agent_id]->first_name; ?></td>
                            <td class=""><?php echo $delivery_person; ?></td>
                            
<!--                            <td class="item_r"><?php // echo $receivables_customer_text; ?></td>
                            <td class="item_r"><?php // echo $collection_customer_text; ?></td>
                            <td class="item_c"><?php // echo $collection_customer_date; ?></td>
                            <td class=""><?php // echo $collection_customer_note; ?></td>-->

                        </tr>            
            
                        <?php endforeach; // end foreach($data['data'][$date_delivery] as $customer_id=>$arr ?>
                    <?php endforeach; // end foreach($data['data'][$date_delivery] as $agent_id=>$arr_customer_data ?>
            
                <?php endif; // end if(isset($data['data'][$date_delivery]?>
            <?php endforeach; // end foreach ($ARR_DAYS as $date_delivery): ?>

        </tbody>
        <tfoot>
            <tr class="f_size_18 odd">
                <td class="item_r item_b" colspan="<?php echo $tfootColSpan;?>">Tổng Cộng</td>
                <?php // if($model->ext_is_maintain==STORE_CARD_KH_BINH_BO):?>
                <td class="item_r item_b"><?php echo ($sum_B_50!=0)?ActiveRecord::formatCurrency($sum_B_50):""; ?></td>
                <td class="item_r item_b"><?php echo ($sum_B_45!=0)?ActiveRecord::formatCurrency($sum_B_45):""; ?></td>
                <?php // endif;?>
                <td class="item_r item_b"><?php echo ($sum_B_12!=0)?ActiveRecord::formatCurrency($sum_B_12):""; ?></td>
                <td class="item_r item_b"><?php echo ($sum_B_6!=0)?ActiveRecord::formatCurrency($sum_B_6):""; ?></td>
                <?php // if($model->ext_is_maintain==STORE_CARD_KH_BINH_BO):?>
                <td class="item_r item_b"><?php echo ($sum_gas_remain!=0)?ActiveRecord::formatCurrency($sum_gas_remain):""; ?></td>
                <?php // endif;?>
                <td class="item_r item_b"><?php echo ($sum_qty!=0)?ActiveRecord::formatCurrency($sum_qty):""; ?></td>
                <?php // if($model->ext_is_maintain==STORE_CARD_KH_BINH_BO):?>
                <td class="item_r item_b"><?php echo ($sum_VO_50!=0)?ActiveRecord::formatCurrency($sum_VO_50):""; ?></td>
                <td class="item_r item_b"><?php echo ($sum_VO_45!=0)?ActiveRecord::formatCurrency($sum_VO_45):""; ?></td>
                <?php // endif;?>
                <td class="item_r item_b"><?php echo ($sum_VO_12!=0)?ActiveRecord::formatCurrency($sum_VO_12):""; ?></td>
                <td class="item_r item_b"><?php echo ($sum_VO_6!=0)?ActiveRecord::formatCurrency($sum_VO_6):""; ?></td>
                <td></td><td></td><td></td>
<!--                <td class="item_r item_b">
                    <?php // echo ($sum_receivables_customer!=0)?ActiveRecord::formatCurrency($sum_receivables_customer):""; ?>
                </td>
                <td class="item_r item_b">
                    <?php // echo ($sum_collection_customer!=0)?ActiveRecord::formatCurrency($sum_collection_customer):""; ?>
                </td>                
                <td></td><td></td>-->
            </tr>
        </tfoot>

    </table><!-- end <table class="table_statistic items table_mon-->
</div> <!-- end <div class="grid-view-scroll">-->
