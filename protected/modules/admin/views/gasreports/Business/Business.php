<?php
$this->breadcrumbs=array(
    $this->pageTitle );
?>

<h1>Xem <?php echo $this->pageTitle;?></h1>
<div class="search-form" style="">
    <div class="wide form">

    <?php $form=$this->beginWidget('CActiveForm', array(
            'action'=>Yii::app()->createUrl($this->route),
            'method'=>'get',
    )); ?>
        <div class="row more_col">
            <div class="col1">
                    <?php echo Yii::t('translation', $form->label($model,'date_from')); ?>
                    <?php 
                        $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                            'model'=>$model,        
                            'attribute'=>'date_from',
                            'options'=>array(
                                'showAnim'=>'fold',
                                'dateFormat'=> MyFormat::$dateFormatSearch,
    //                            'minDate'=> '0',
                                'maxDate'=> '0',
                                'changeMonth' => true,
                                'changeYear' => true,
                                'showOn' => 'button',
                                'buttonImage'=> Yii::app()->theme->baseUrl.'/admin/images/icon_calendar_r.gif',
                                'buttonImageOnly'=> true,                                
                            ),        
                            'htmlOptions'=>array(
                                'class'=>'w-16',
                                'size'=>'16',
                                'style'=>'float:left;',                               
                                'readonly'=>1,
                            ),
                        ));
                    ?>     		
            </div>
            <div class="col2">
                    <?php echo Yii::t('translation', $form->label($model,'date_to')); ?>
                    <?php 
                        $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                            'model'=>$model,        
                            'attribute'=>'date_to',
                            'options'=>array(
                                'showAnim'=>'fold',
                                'dateFormat'=> MyFormat::$dateFormatSearch,
    //                            'minDate'=> '0',
                                'maxDate'=> '0',
                                'changeMonth' => true,
                                'changeYear' => true,
                                'showOn' => 'button',
                                'buttonImage'=> Yii::app()->theme->baseUrl.'/admin/images/icon_calendar_r.gif',
                                'buttonImageOnly'=> true,                                
                            ),        
                            'htmlOptions'=>array(
                                'class'=>'w-16',
                                'size'=>'16',
                                'style'=>'float:left;',
                                'readonly'=>1,
                            ),
                        ));
                    ?>     		
            </div>
        </div>

        <div class="row">
        <?php echo $form->labelEx($model,'agent_id'); ?>
        <?php echo $form->hiddenField($model,'agent_id', array('class'=>'')); ?>
        <?php
                // 1. limit search kh của sale
                $url = Yii::app()->createAbsoluteUrl('admin/ajax/search_by_role', array('role'=> ROLE_AGENT));
                // widget auto complete search user customer and supplier
                $aData = array(
                    'model'=>$model,
                    'field_customer_id'=>'agent_id',
                    'url'=> $url,
                    'name_relation_user'=>'agent',
                    'ClassAdd' => 'w-400',
                    'field_autocomplete_name' => 'autocomplete_name_1',
                    'placeholder'=>'Nhập mã hoặc tên đại lý',
                );
                $this->widget('ext.SpaAutocompleteCustomer.SpaAutocompleteCustomer',
                    array('data'=>$aData));
                ?>
            <?php echo $form->error($model,'agent_id'); ?>
        </div>
        
        <?php $aNotIn = array(ROLE_SUB_USER_AGENT, ROLE_SALE);
        if( !in_array(Yii::app()->user->role_id, $aNotIn )):?>
        <div class="row">
            <?php echo $form->label($model,'ext_sale_id'); ?>
            <?php // echo $form->dropDownList($model,'ext_sale_id', Users::getArrUserByRole(ROLE_SALE, array('order'=>'t.code_bussiness', 'prefix_type_sale'=>1)),array('class'=>'w-250', 'style'=>'width:376px','empty'=>'Select')); ?>

            <?php echo $form->hiddenField($model,'ext_sale_id'); ?>
            <?php 
            // widget auto complete search user customer and supplier
            $aData = array(
                'model'=>$model,
                'field_autocomplete_name'=>'autocomplete_name_sale',
                'field_customer_id'=>'ext_sale_id',
                'url'=> Yii::app()->createAbsoluteUrl('admin/ajax/search_sale'),
                'name_relation_user'=>'rSale',
                'placeholder'=>'Nhập Tên Sale',
                'ClassAdd' => 'w-400',
//                        'fnSelectCustomer'=>'fnAfterSelectSaleOrAgent',
            );
            $this->widget('ext.SpaAutocompleteCustomer.SpaAutocompleteCustomer',
                array('data'=>$aData));                                        
        ?>
        </div>
        <?php endif;?>   

        <div class="row">
        <?php echo $form->labelEx($model,'customer_id'); ?>
        <?php echo $form->hiddenField($model,'customer_id', array('class'=>'')); ?>
        <?php
            // 1. limit search kh của sale
            $url = Yii::app()->createAbsoluteUrl('admin/ajax/search_user_by_code');
            // widget auto complete search user customer and supplier
            $aData = array(
                'model'=>$model,
                'field_customer_id'=>'customer_id',
                'url'=> $url,
                'name_relation_user'=>'rCustomer',
                'ClassAdd' => 'w-400',
                'field_autocomplete_name' => 'autocomplete_name',
            );
            $this->widget('ext.SpaAutocompleteCustomer.SpaAutocompleteCustomer',
                array('data'=>$aData));
            ?>
        </div>
        <div class="row more_col">
            <div class="col1">
                <?php echo $form->labelEx($model,'ext_is_maintain'); ?>
                <?php echo $form->dropDownList($model,'ext_is_maintain', CmsFormatter::$CUSTOMER_BO_MOI,array('class'=>'w-140', 'style'=>'')); ?>
            </div>
        </div>

        <div class="row buttons" style="padding-left: 159px; margin:20px 0;">
            <?php $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType'=>'submit',
            'label'=>Yii::t('translation','Xem Thống Kê'),
            'type'=>'null', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
            'size'=>'small', // null, 'large', 'small' or 'mini'
            //'htmlOptions' => array('style' => 'margin-bottom: 10px; float: right;'),
        )); ?>	
        <?php // if(ControllerActionsName::isAccessAction('statistic_maintain_export_excel', $actions)):?>
        <?php if(0):?>
                <input class='statistic_maintain_export_excel' next='<?php echo Yii::app()->createAbsoluteUrl('admin/gasmaintain/statistic_maintain_export_excel');?>' type='button' value='Xuất Thống Kê Hiện Tại Ra Excel'>
        <?php endif;?>

        </div>

    <?php $this->endWidget(); ?>

    </div><!-- wide form -->
</div><!-- search-form -->

<?php // include_once 'View_store_movement_summary_print.php';?>

<style>
    .ui-datepicker-trigger { float: left;}
</style>

<script>
$(document).ready(function(){
    $('.form').find('button:submit').click(function(){        
        $.blockUI({ overlayCSS: { backgroundColor: '#fff' } }); 
    });   
});
</script>

<?php if(isset($rData['OUTPUT'])) :?>
    <?php include 'BusinessForm.php'; ?>
<?php endif; ?>