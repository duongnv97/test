<?php Yii::app()->clientScript->registerCoreScript('jquery.ui'); ?>
<?php
$gridCss = Yii::app()->getAssetManager()->publish(Yii::getPathOfAlias('zii.widgets.assets')).'/gridview/styles.css';
Yii::app()->getClientScript()->registerCssFile($gridCss);
?>
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/jquery.floatThead.min.js"></script>    
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/underscore-min.js"></script>    

<script>
$(function() {
//    $( "#tabs" ).tabs();
    $(".items > tbody > tr:odd").addClass("odd");
    $(".items > tbody > tr:even").addClass("even");   
    fnTabMonthClick();
});

function fnTabMonthClick(){
     // floatThead for first tab
     var index=1;
     $('.grid-view').each(function(){
        if(index==1){
            var tableItem = $(this).find('.items').eq(0);
            $(this).find('.items').floatThead();
        }
        index++;
    });    
    // floatThead for first tab
}

</script>

<?php
$CUSTOMER_MODEL = $rData['CUSTOMER_MODEL'];
$OUTPUT         = $rData['OUTPUT'];
$REMAIN         = $rData['REMAIN'];
$SALE_OF_CUSTOMER = $rData['SALE_OF_CUSTOMER'];
$cPrice         = $rData['PRICE'];
$sumAmountBB = $sumAmountB12 = $sumAmountAll=0; $index = 1;
$sumB50 = $sumB45= $sumB12 = $sumB6 = $sumAll = $sumGasRemain = $sumOutputBB = 0;

?>

<div class="title_table_statistic"><?php echo $this->pageTitle;?>
        <?php echo CmsFormatter::$CUSTOMER_BO_MOI[$model->ext_is_maintain]; echo " Từ Ngày $model->date_from Đến Ngày $model->date_to"; ?>
</div>
<div class="grid-view grid-view-scroll">
    <!--<table class="hm_table table_statistic items table_month_1" style="">-->
    <table class="hm_table items " style="">
        <thead>
            <tr>
                <th class="w-20">#</th>
                <th class="w-150">Tên KH</th>
                <th class="w-250">Địa Chỉ</th>
                <th class="w-50">B50</th>
                <th class="w-50">B45</th>
                <th class="w-50">B12</th>
                <th class="w-50">B6</th>
                <th class="w-50">Gas Dư</th>
                <th class="w-50">Giá BB</th>
                <th class="w-50">Qty BB</th>
                <th class="w-50">TT BB</th>
                <th class="w-50">Giá B12</th>
                <th class="w-50">TT B12</th>
                <th class="w-50">Doanh Thu</th>
                <th class="w-50">Sale</th>
                <!--<th class="w-100">Đại Lý</th>-->
            </tr>
        </thead>
        <tbody>
        <?php foreach($OUTPUT as $customer_id => $aInfo): ?>
        <?php
            if(empty($customer_id)){
                continue ;
            }
            $sale_id = $SALE_OF_CUSTOMER[$customer_id];
            $priceBB = $priceB12 = $amountBB = $amountB12 = $sumRow = 0; $saleName = $customerName = $customerAdd = '';
            $b50    = isset($OUTPUT[$customer_id][MATERIAL_TYPE_BINHBO_50]) ? $OUTPUT[$customer_id][MATERIAL_TYPE_BINHBO_50] : 0;
            $b45    = isset($OUTPUT[$customer_id][MATERIAL_TYPE_BINHBO_45]) ? $OUTPUT[$customer_id][MATERIAL_TYPE_BINHBO_45] : 0;
            $b12    = isset($OUTPUT[$customer_id][MATERIAL_TYPE_BINH_12]) ? $OUTPUT[$customer_id][MATERIAL_TYPE_BINH_12] : 0;
            $b6     = isset($OUTPUT[$customer_id][MATERIAL_TYPE_BINH_6]) ? $OUTPUT[$customer_id][MATERIAL_TYPE_BINH_6] : 0;
            $gasRemain     = isset($REMAIN[$customer_id]) ? $REMAIN[$customer_id] : 0;
            
            $outputBB = $b50*KL_BINH_50 + $b45*KL_BINH_45 + $b6*KL_BINH_6 - $gasRemain;
            if(isset($cPrice[$customer_id])){
                $priceBB    = $cPrice[$customer_id][UsersPrice::PRICE_B_50];
                $priceB12   = $cPrice[$customer_id][UsersPrice::PRICE_B_12];
                $amountBB = $outputBB * $priceBB;
                
                if($priceB12  < UsersPrice::PRICE_MAX_BINH_BO ){// tức là đang set giá kg cho B12
                    $amountB12 = $b12 * KL_BINH_12 * $priceBB;
                }else{// tức là đang set giá bán 1 bình 12kg
                    $amountB12 = $b12 * $priceB12;
                }
            }
            $amountRow = $amountBB + $amountB12;
            $sumRow += $amountRow;
            $sumAmountBB += $amountBB;
            $sumAmountB12 += $amountB12;
            $sumAmountAll += $amountRow;
            
            $sumB50 += $b50;
            $sumB45 += $b45;
            $sumB12 += $b12;
            $sumB6  += $b6;
            $sumGasRemain       += $gasRemain;
            $sumOutputBB        += $outputBB;
                    
            if(isset($CUSTOMER_MODEL[$customer_id])){
                $customerName   = $CUSTOMER_MODEL[$customer_id]->code_bussiness.'-'.$CUSTOMER_MODEL[$customer_id]->first_name;
                $customerAdd    = $CUSTOMER_MODEL[$customer_id]->address;
            }
            if(isset($CUSTOMER_MODEL[$sale_id])){
                $saleName   = $CUSTOMER_MODEL[$sale_id]->first_name;
            }
        
        ?>
        <tr>
            <td><?php echo $index++;?></td>
            <td><?php echo $customerName;?></td>
            <td><?php echo $customerAdd;?></td>
            <td class="item_c"><?php echo $b50 > 0 ? ActiveRecord::formatCurrencyRound($b50) : '';?></td>
            <td class="item_c"><?php echo $b45 > 0 ? ActiveRecord::formatCurrencyRound($b45) : '';?></td>
            <td class="item_c"><?php echo $b12 > 0 ? ActiveRecord::formatCurrencyRound($b12) : '';?></td>
            <td class="item_c"><?php echo $b6 > 0 ? ActiveRecord::formatCurrencyRound($b6) : '';?></td>
            <td class="item_c"><?php echo $gasRemain > 0 ? ActiveRecord::formatCurrency($gasRemain*1) : '';?></td>
            <td class="item_r"><?php echo $priceBB > 0 ? ActiveRecord::formatCurrencyRound($priceBB) : '';?></td>
            <td class="item_r"><?php echo $outputBB > 0 ? ActiveRecord::formatCurrencyRound($outputBB) : '';?></td>
            <td class="item_r item_b"><?php echo $amountBB > 0 ? ActiveRecord::formatCurrencyRound($amountBB) : '';?></td>
            <td class="item_r"><?php echo $priceB12 > 0 ? ActiveRecord::formatCurrencyRound($priceB12) : '';?></td>
            <td class="item_r item_b"><?php echo $amountB12 > 0 ? ActiveRecord::formatCurrencyRound($amountB12) : '';?></td>
            <td class="item_r item_b"><?php echo $sumRow > 0 ? ActiveRecord::formatCurrencyRound($sumRow) : '';?></td>
            <td><?php echo $saleName;?></td>
        </tr>
            
        <?php endforeach; ?>
        </tbody>
        <tfoot>
            <tr class="f_size_16">
                <td class="item_r item_b" colspan="3">Tổng cộng</td>
                <td class="item_b item_c"><?php echo $sumB50>0?ActiveRecord::formatCurrencyRound($sumB50):''; ?></td>
                <td class="item_b item_c"><?php echo $sumB45>0?ActiveRecord::formatCurrencyRound($sumB45):''; ?></td>
                <td class="item_b item_c"><?php echo $sumB12>0?ActiveRecord::formatCurrencyRound($sumB12):''; ?></td>
                <td class="item_b item_c"><?php echo $sumB6>0?ActiveRecord::formatCurrencyRound($sumB6):''; ?></td>
                <td class="item_b item_c"><?php echo $sumGasRemain>0?ActiveRecord::formatCurrencyRound($sumGasRemain):''; ?></td>
                <td></td>
                <td class="item_b item_r"><?php echo $sumOutputBB>0?ActiveRecord::formatCurrencyRound($sumOutputBB):''; ?></td>
                <td class="item_b item_r"><?php echo $sumAmountBB>0?ActiveRecord::formatCurrencyRound($sumAmountBB):''; ?></td>
                <td></td>
                <td class="item_b item_r"><?php echo $sumAmountB12>0?ActiveRecord::formatCurrencyRound($sumAmountB12):''; ?></td>
                <td class="item_b item_r"><?php echo $sumAmountAll>0?ActiveRecord::formatCurrencyRound($sumAmountAll):''; ?></td>
            </tr>
        </tfoot>

    </table><!-- end <table class="table_statistic items table_mon-->
</div> <!-- end <div class="grid-view-scroll">-->
