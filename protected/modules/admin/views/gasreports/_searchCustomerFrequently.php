<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

            <div class="row">
		<?php echo Yii::t('translation', $form->label($model,'customer_id')); ?>
                <?php echo $form->hiddenField($model,'customer_id'); ?>
                <?php 
                        $this->widget('zii.widgets.jui.CJuiAutoComplete', array(
                                'attribute'=>'autocomplete_name',
                                'model'=>$model,
                                'sourceUrl'=>Yii::app()->createAbsoluteUrl('admin/site/autocompleteSellCustomerMaintainAndPTTT'),
                //                                'name'=>'my_input_name',
                                'options'=>array(
                                        'minLength'=>MIN_LENGTH_AUTOCOMPLETE,
                                        'multiple'=> true,
                                'search'=>"js:function( event, ui ) {
                                        $('#GasMaintainSell_autocomplete_name').addClass('grid-view-loading-gas');
                                        } ",
                                'response'=>"js:function( event, ui ) {
                                        var json = $.map(ui, function (value, key) { return value; });
                                        if(json.length<1){
                                            var error = '<div class=\'errorMessage clr autocomplete_name_text\'>Không tìm thấy dữ liệu.</div>';
                                            if($('.autocomplete_name_text').size()<1)
                                                $('.autocomplete_name_error').after(error);
                                            else
                                                $('.autocomplete_name_error').parent('div').find('.autocomplete_name_text').show();
                                            $('.autocomplete_name_error').parent('div').find('.remove_row_item').hide();
                                        }                                    
                                        
                                        $('#GasMaintainSell_autocomplete_name').removeClass('grid-view-loading-gas');
                                        } ",
                                    
                                    'select'=>"js:function(event, ui) {
                                            $('#GasMaintainSell_customer_id').val(ui.item.id);
                                            var remove_div = '<span class=\'remove_row_item\' onclick=\'fnRemoveName(this)\'></span>';
                                            $('#GasMaintainSell_autocomplete_name').parent('div').find('.remove_row_item').remove();
                                            $('#GasMaintainSell_autocomplete_name').attr('readonly',true).after(remove_div);
                                            fnBuildTableInfo(ui.item);
                                            $('.autocomplete_customer_info').show();
                                            $('.autocomplete_name_error').parent('div').find('.autocomplete_name_text').hide();
                                    }",
                                ),
                                'htmlOptions'=>array(
                                    'class'=>'autocomplete_name_error',
                                    'size'=>45,
                                    'maxlength'=>45,
                                    'style'=>'float:left;',
                                    'placeholder'=>'Nhập tên, số ĐT hoặc mã KH',
                                ),
                        )); 
                        ?>        
                        <script>
                            function fnRemoveName(this_){
                                $(this_).prev().attr("readonly",false); 
                                $("#GasMaintainSell_autocomplete_name").val("");
                                $("#GasMaintainSell_customer_id").val("");
                                $('.autocomplete_customer_info').hide();
                            }
                        function fnBuildTableInfo(item){
                            $(".info_name").text(item.name_customer);
                            $(".info_name_agent").text(item.name_agent);
                            $(".info_code_account").text(item.code_account);
                            $(".info_code_bussiness").text(item.code_bussiness);
                            $(".info_address").text(item.address);
                            $(".info_phone").text(item.phone);
                        }
                            
                        </script>        
                        <div class="clr"></div>		
		<?php echo $form->error($model,'customer_id'); ?>
                <?php $display='display:inline;';
                    $info_name ='';
                    $info_name_agent ='';
                    $info_address ='';
                    $info_code_account ='';
                    $info_code_bussiness ='';
                    $info_phone ='';
                        if(empty($model->customer_id)) $display='display: none;';
                        else{
                            $info_name = $model->customer->first_name;
                            $info_name_agent = $model->customer->name_agent;
                            $info_code_account = $model->customer->code_account;
                            $info_code_bussiness = $model->customer->code_bussiness;
                            $info_address = $model->customer->address;
                            $info_phone = $model->customer->phone;
                        }
                ?>                        
                <div class="autocomplete_customer_info" style="margin-left: 160px;<?php echo $display;?>">
                <table>
                    <tr>
                        <td class="_l">Mã khách hàng:</td>
                        <td class="_r info_code_bussiness"><?php echo $info_code_bussiness;?></td>
                    </tr>

                    <tr>
                        <td class="_l">Tên khách hàng:</td>
                        <td class="_r info_name"><?php echo $info_name;?></td>
                    </tr>                    
                    <tr>
                        <td class="_l">Địa chỉ:</td>
                        <td class="_r info_address"><?php echo $info_address;?></td>
                    </tr>
                    <tr>
                        <td class="_l">Điện Thoại:</td>
                        <td class="_r info_phone"><?php echo $info_phone;?></td>
                    </tr>
                    
                    
                </table>
            </div>
            <div class="clr"></div>    		
            <?php // echo $form->error($model,'customer_id'); ?>
	</div>    
    
        <?php
        $tmp_ = array();
        for($i=1;$i<50;$i++)
            $tmp_[$i]=$i;
        ?>
	<div class="row">
		<?php echo $form->label($model,'qty_buy_from'); ?>
                <?php echo $form->dropDownList($model,'qty_buy_from',$tmp_, array('empty'=>'Select')); ?>
		<?php echo $form->error($model,'qty_buy_from'); ?>
	</div>    
    
        <div class="row">
                <?php echo Yii::t('translation', $form->labelEx($model,'monitoring_id')); ?>
                <?php  echo $form->dropDownList($model,'monitoring_id', Users::getArrUserByRoleNotCheck(ROLE_MONITORING_MARKET_DEVELOPMENT),array('style'=>'width:350px;','empty'=>'Select')); ?>
        </div>

        <div class="row">
                <?php echo Yii::t('translation', $form->labelEx($model,'maintain_employee_id')); ?>
                <?php echo $form->dropDownList($model,'maintain_employee_id', Users::getSelectByRoleForAgent($model->monitoring_id, ONE_MONITORING_MARKET_DEVELOPMENT, $model->agent_id, array('status'=>1)),array('class'=>'category_ajax', 'style'=>'width:350px;','empty'=>'Select')); ?>		
        </div>    

        <?php // if(Yii::app()->user->role_id!=ROLE_SUB_USER_AGENT):?>
            <div class="row">
                <?php echo Yii::t('translation', $form->labelEx($model,'agent_id')); ?>
                <?php echo $form->dropDownList($model,'agent_id', Users::getArrUserByRoleNotCheck(ROLE_AGENT),array('style'=>'width:350px;','empty'=>'Select')); ?>
                <?php echo $form->error($model,'agent_id'); ?>
            </div>    
        <?php // endif;?>  
    
	<div class="row">
		<?php echo $form->label($model,'is_same_seri_maintain', array('label'=>'Lọc KH trùng thông tin')); ?>
                <?php echo $form->dropDownList($model,'is_same_seri_maintain', CmsFormatter::$yesNoFormat, array('empty'=>'Select')); ?>
		<?php echo $form->error($model,'is_same_seri_maintain'); ?>
	</div>      
    
	<div class="row buttons" style="padding-left: 159px;">
		        <?php $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType'=>'submit',
            'label'=>Yii::t('translation','Search'),
            'type'=>'null', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
            'size'=>'small', // null, 'large', 'small' or 'mini'
            //'htmlOptions' => array('style' => 'margin-bottom: 10px; float: right;'),
        )); ?>	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->