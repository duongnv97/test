<?php
$this->breadcrumbs=array(
	'Thống Kê Khách Hàng Thường Xuyên',
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('gas-maintain-sell-grid', {
                url : $(this).attr('action'),
		data: $(this).serialize()
	});
	return false;
});
");

Yii::app()->clientScript->registerScript('ajaxupdate', "
$('#gas-maintain-sell-grid a.ajaxupdate').live('click', function() {
    $.fn.yiiGridView.update('gas-maintain-sell-grid', {
        type: 'POST',
        url: $(this).attr('href'),
        success: function() {
            $.fn.yiiGridView.update('gas-maintain-sell-grid');
        }
    });
    return false;
});
");
?>

<h1><?php echo Yii::t('translation', 'Thống Kê Khách Hàng Thường Xuyên (Sử dụng gas Hướng Minh)'); ?></h1>

<?php // echo CHtml::link(Yii::t('translation','Tìm Kiếm Nâng Cao'),'#',array('class'=>'search-button')); ?>
<div class="search-form" style="">
<?php $this->renderPartial('_searchCustomerFrequently',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'gas-maintain-sell-grid',
	'dataProvider'=>$model->searchCustomerFrequently(),
        'afterAjaxUpdate'=>'function(id, data){ fnUpdateColorbox();}',
        'template'=>'{pager}{summary}{items}{pager}{summary}', 
        'pager' => array(
            'maxButtonCount'=>  CmsFormatter::$PAGE_MAX_BUTTON,
        ),    
    
	'columns'=>array(
        array(
            'header' => 'S/N',
            'type' => 'raw',
            'value' => '$this->grid->dataProvider->pagination->currentPage * $this->grid->dataProvider->pagination->pageSize + ($row+1)',
            'headerHtmlOptions' => array('width' => '30px','style' => 'text-align:center;'),
            'htmlOptions' => array('style' => 'text-align:center;')
        ),
        array(
            'name' => 'agent_id',
            'value' => '$data->agent?$data->agent->first_name:""',
            'visible'=>Yii::app()->user->role_id!=ROLE_SUB_USER_AGENT,
            'htmlOptions' => array('style' => 'text-align:center;width:60px;')
        ),             

        array(                
            'name' => 'customer_id',
            'type'=>'NameUser',
            'value'=>'$data->customer?$data->customer:0',
            'htmlOptions' => array('style' => 'text-align:left;width:100px;')
        ),     

        array(
            'header' => 'Địa Chỉ',
            'type'=>'AddressTempUser',
//            'value' => '$data->customer?$data->customer->address:""',
            'value' => '$data->customer?$data->customer:""',
            'htmlOptions' => array('style' => 'text-align:center;width:80px;'),
            'visible'=>Yii::app()->user->role_id!=ROLE_SUB_USER_AGENT,
        ),   				
        array(
            'header' => 'Điện Thoại',
            'value' => '$data->customer?$data->customer->phone:""',
            'htmlOptions' => array('style' => 'text-align:center;width:60px;'),
            'visible'=>Yii::app()->user->role_id!=ROLE_SUB_USER_AGENT,
        ),     
        array(
            'header' => 'Số Lần Mua Hàng',  
            'name' => 'count_buy',
            'htmlOptions' => array('style' => 'text-align:center;width:30px;')
        ),               
        array(
            'header' => 'Số Lượng',  
            'name' => 'quantity_sell',
            'htmlOptions' => array('style' => 'text-align:center;width:30px;')
        ),               
            
        array(
            'name' => 'monitoring_id',
            'value' => '$data->monitoring?$data->monitoring->first_name:""',
            'htmlOptions' => array('style' => 'text-align:center;width:100px;')
        ),  
        array(
            'name' => 'maintain_employee_id',
            'value' => '$data->maintain_employee?$data->maintain_employee->first_name:""',
            'htmlOptions' => array('style' => 'text-align:center;width:100px;')
        ),
		
	),
)); ?>

<script>
$(document).ready(function() {
//    fnUpdateColorbox();
});

function fnUpdateColorbox(){    
    $(".update_status_maintain_sell").colorbox({iframe:true,innerHeight:'450', innerWidth: '850',close: "<span title='close'>close</span>"});
	$(".view").colorbox({iframe:true,innerHeight:'700', innerWidth: '900',close: "<span title='close'>close</span>"});
}
</script>