<?php 
$cty_target_bo = 0;
$cty_target_binh_moi = 0;
$cty_thuc_te_bo = 0;
$cty_thuc_te_binh_moi = 0;
$cty_remain = 0;
?>
<div id="tabs-SALE_MODEL">
    <h1><?php echo $textInfo;?></h1>
    <div class="clearfix">
        <div class="color_type_1 color_note"></div> Gas Bình Bò
        <div class="clr"></div>
        <!--<div class="color_type_2 color_note"></div> Gas Bình Mối-->
    </div>
    <div class="clr"></div>

    <div class="box_310">
        <table class="hm_table items">
            <tbody>
                <tr>
                    <td colspan="3" class="item_c item_b">CÔNG TY</td>
                </tr>
                <tr>
                    <td class="item_c w-120">Target</td>
                    <td class="item_c w-140">Thực Tế</td>
                    <td class="item_c w-100">Tỷ Lệ</td>
                </tr>                       

                <tr class="color_type_1">
                    <td class="item_r cty_target_bo"></td>
                    <td class="item_r cty_thuc_te_bo"></td>
                    <td class="item_c cty_bo_percent"></td>
                </tr>
                <tr class="color_type_2">
                    <td class="item_r ">Gas dư</td>
                    <td class="item_r cty_remain"></td>
                    <td class="item_c "></td>
                </tr>
                <tr class="color_type_2 display_none">
                    <td class="item_r cty_target_binh_moi"></td>
                    <td class="item_r cty_thuc_te_binh_moi"></td>
                    <td class="item_c cty_binh_moi_percent"></td>
                </tr>

            </tbody>
        </table>
    </div>            
    <div class="clr"></div>

    <?php foreach($SALE_MODEL_BO as $province_id=>$aModelUser): ?>
    <?php if($province_id == GasProvince::KV_MIEN_TAY) { continue; } ?>
    <div class="wrap_province">
        <h1 class="l_padding_50 margin_0 padding_0"><?php echo isset($PROVINCE_MODEL[$province_id])?$PROVINCE_MODEL[$province_id]->name:"";?></h1>
        <?php foreach($aModelUser as $mUser): ?>
            <div class="box_310">
                <table class="hm_table items">
                    <tbody>
                        <tr>
                            <td colspan="3" class="item_c item_b"><?php echo $mUser->first_name;?></td>
                        </tr>
                        <tr>
                            <td class="item_c w-120">Target</td>
                            <td class="item_c w-140">Thực Tế</td>
                            <td class="item_c w-100">Tỷ Lệ</td>
                        </tr>
                        <?php foreach(CmsFormatter::$CUSTOMER_BO_MOI as $type=>$label):?>
                        <?php
                            if($type!=STORE_CARD_KH_BINH_BO)  continue;
                            $uid = $mUser->id;
                            $target=isset($data['aTargetEachMonth'][$uid][$type][$FOR_MONTH])?$data['aTargetEachMonth'][$uid][$type][$FOR_MONTH]:0;
                            $OUTPUT=isset($data['OUTPUT'][$uid][$type])?$data['OUTPUT'][$uid][$type]:0;
                            $REMAIN=isset($data['REMAIN'][$uid][$type])?$data['REMAIN'][$uid][$type]:0;
                            $real = $OUTPUT-$REMAIN;
                            $percent = $real;
                            if($target){
                                $percent = ($real/$target)*100;
                            }
                            // for target cty

                            if($type==STORE_CARD_KH_BINH_BO){
                                $cty_target_bo += $target;
                                $cty_thuc_te_bo += $real;
                                $cty_remain += $REMAIN;

                            }else{
                                $cty_target_binh_moi += $target;
                                $cty_thuc_te_binh_moi += $real;
                            }
                            // for target cty // nếu có xuất excel thì sẽ dùng con số + cuối cùng đưa vào session của excel
                            // thì đỡ phải tính ở bên dưới

                        ?>

                        <tr class="color_type_<?php echo $type;?>">
                            <td class="item_r"><?php echo ActiveRecord::formatCurrency($target);?></td>
                            <td class="item_r"><?php echo ActiveRecord::formatCurrency(round($real));?></td>
                            <td class="item_c"><?php echo round($percent,1);?> %</td>
                        </tr>
                        <tr class="color_type_2">
                            <td class="item_r">Gas dư</td>
                            <td class="item_r"><?php echo ActiveRecord::formatCurrency(round($REMAIN));?></td>
                            <td class="item_c"></td>
                        </tr>
                        <?php endforeach;?>

                    </tbody>
                </table>
            </div>                

        <?php endforeach; // end foreach($aModelUser as $mUser): ?>

    </div> <!-- end  <div class="wrap_province"> -->
    <div class="clr"></div>
    <?php endforeach; // end <?php foreach($SALE_MODEL_BO as $province_id=>$aModelUser): ?>
    <div class="clr"></div>
</div>
    
<input class="hide_cty_target_bo" value="<?php echo ActiveRecord::formatCurrency($cty_target_bo);?>" type="hidden">
<input class="hide_cty_target_binh_moi" value="<?php echo ActiveRecord::formatCurrency($cty_target_binh_moi);?>" type="hidden">
<input class="hide_cty_thuc_te_bo" value="<?php echo ActiveRecord::formatCurrency(round($cty_thuc_te_bo));?>" type="hidden">
<input class="hide_cty_thuc_te_binh_moi" value="<?php echo ActiveRecord::formatCurrency($cty_thuc_te_binh_moi);?>" type="hidden">
<input class="hide_cty_remain" value="<?php echo ActiveRecord::formatCurrency(round($cty_remain));?>" type="hidden">

<input class="hide_cty_bo_percent" value="<?php echo round($cty_target_bo?(($cty_thuc_te_bo/$cty_target_bo)*100):$cty_thuc_te_bo,1) ;?> %" type="hidden">
<input class="hide_cty_binh_moi_percent" value="<?php echo round($cty_target_binh_moi?(($cty_thuc_te_binh_moi/$cty_target_binh_moi)*100):$cty_thuc_te_binh_moi,1);?> %" type="hidden">
