<?php if(count($data) && isset($data['data'])) :?>
<?php Yii::app()->clientScript->registerCoreScript('jquery.ui'); ?>
<?php
$gridCss = Yii::app()->getAssetManager()->publish(Yii::getPathOfAlias('zii.widgets.assets')).'/gridview/styles.css';
Yii::app()->getClientScript()->registerCssFile($gridCss);
?>
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/scroll/jquery.freezetablecolumns.1.1.js"></script> 
<?php 
$session=Yii::app()->session;
$arr_customer_id    = isset($data['customer_id'])?$data['customer_id']:array();
$customer_id_only   = isset($data['customer_id_only'])?$data['customer_id_only']:array();
$SESSION_MODEL_USER = Users::getListdataCustomer( array('arr_customer_id'=> $customer_id_only, 'arr_role_id' => array(ROLE_AGENT, ROLE_SALE)));// khởi tạo session model của search kh bò mối + đại lý + sale;
$DATA_STORECARD     = isset($data['data'])?$data['data']:array();
$OUTPUT             = isset($data['OUTPUT'])?$data['OUTPUT']:array();
$OUTPUT_MOI_12      = isset($data['OUTPUT_MOI_12'])?$data['OUTPUT_MOI_12']:array();
$GAS_REMAIN         = isset($data['GAS_REMAIN'])?$data['GAS_REMAIN']:array();
$ARR_DAYS           = $data['ARR_DAYS'];
$index              = 1;
$SumCol             = array();
$SumColAll          = array();
$SumColAll['BO']    = 0;
$SumColAll['MOI12'] = 0;
?>

<?php include 'Output_customer_form_tab.php';?>

<script>
$(function() {
    $( "#tabs" ).tabs();
//    fnUpdateSumRow();
    fnTabMonthClick();
});    
    
function fnTabMonthClick(){
    $('.freezetablecolumns').each(function(){
        var id_table = $(this).attr('id');
        $('#'+id_table).freezeTableColumns({
                width:       <?php echo Users::WIDTH_freezetable;?>,   // required - bt 1100
                height:      400,   // required
                numFrozen:   6,     // optional
                frozenWidth: 385,   // optional
                clearWidths: true  // optional
              });   
    });
}  

function fnUpdateSumRow(){
    $('.class_remove_row').each(function(){
        $(this).closest('tr').remove();
    });
    
    $('.sum_row_hide').each(function(){
        var tr = $(this).closest('tr');
        tr.find('.sum_row').text($(this).text());
    });
    
    $('.sumTypeHide').each(function(){
        var class_update = $(this).attr('link');
        $('.'+class_update).text($(this).text());
    });
    
    
}
    
$(window).load(function(){
    var index = 1;
    $('.freezetablecolumns').each(function(){
        if(index==1)
            $(this).closest('div.grid-view').show();
        index++;
    });    
    fnAddClassOddEven('items');
});
</script>

<?php endif; ?>

