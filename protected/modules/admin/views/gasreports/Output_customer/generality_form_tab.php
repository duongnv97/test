<div class="grid-view display_none">
    <div class="title_table_statistic">BÁO CÁO SẢN LƯỢNG KHÁCH HÀNG TỪ NGÀY: <?php echo $model->date_from;?> ĐẾN NGÀY: <?php echo $model->date_to;?></div>
    <div class="">
        <table id="freezetablecolumns_inventory" class="freezetablecolumns items">
            <thead>
                <tr class="h_50">
                    <th class="w-20 item_c ">STT</th>
                    <th class="w-130 ">Khách Hàng</th>
                    <th class="w-50 ">Tổng Cộng</th>
                    <th class="w-150 ">Địa chỉ</th>
                    <?php // foreach($ListOptionAgent as $agent_id=>$agent_name): ?>
                    <?php foreach($ARR_DAYS as $date_delivery): ?>
                    <th class="w-40 item_c" style="">
                        <?php echo substr($date_delivery, -2); ?>
                    </th>
                    <?php endforeach;?>
                </tr>
            </thead>
            <tbody>
                <?php foreach($arr_customer_id as $customer_id=>$arr_agent):?>
                    <?php 
                        $SumRowBo = $SumRowMoi = 0;
                        $customerName = $customerCode = $customerAdd = '';
                        if(isset($SESSION_MODEL_USER[$customer_id])){
                            $customerCode   = $SESSION_MODEL_USER[$customer_id]['code_bussiness'];
                            $customerName   = $customerCode.' - '.$SESSION_MODEL_USER[$customer_id]['first_name'];
                            $customerAdd    = $SESSION_MODEL_USER[$customer_id]['address'];
                        }
                    ?>
                    <?php foreach($arr_agent as $agent_id):?>
                        <?php foreach($ARR_DAYS as $date_delivery): ?>
                        <?php 
                            $REMAIN = isset($GAS_REMAIN[$customer_id][$date_delivery][$agent_id])?$GAS_REMAIN[$customer_id][$date_delivery][$agent_id]:0;
                            if(isset($BO_REMAIN[$customer_id][$date_delivery]))
                                $BO_REMAIN[$customer_id][$date_delivery] += $REMAIN;
                            else{
                                $BO_REMAIN[$customer_id][$date_delivery] = $REMAIN;
                            }
                        ?>
                        <?php endforeach; // end foreach($ARR_DAYS as $date_delivery): ?>
                    <?php endforeach; // end foreach($arr_customer_id as $customer_id=>$arr_agent) ?>
                    <tr class="h_70">
                        <td class=""><?php echo $index++; ?></td>
                        <td class="td_break_line "><?php echo $customerName; ?></td>
                        <td class="sum_row_show item_r item_b"></td>
                        <td class="td_break_line "><?php echo $customerAdd; ?></td>
                        <?php foreach($ARR_DAYS as $date_delivery): ?>
                        <td class="item_r ">
                            <?php                                
                                $BO_OUTPUT = isset($OUTPUT[$customer_id][$date_delivery])?$OUTPUT[$customer_id][$date_delivery]:0;
                                $BO_REAL = $BO_OUTPUT - $BO_REMAIN[$customer_id][$date_delivery];
                                /* sum row and col */
                                $SumRowBo+=$BO_REAL;
                                if(isset($SumCol[$date_delivery]['BO']))
                                    $SumCol[$date_delivery]['BO']+=$BO_REAL;
                                else
                                    $SumCol[$date_delivery]['BO']=$BO_REAL;
                                $SumColAll['BO']+=$BO_REAL;
                                /* sum row and col */
                                $MOI_12 = '';
                                $BO_REAL = ($BO_REAL!=0?ActiveRecord::formatCurrencyRound($BO_REAL):'');
                                if($model->ext_is_maintain==STORE_CARD_KH_MOI){
                                    $MOI_12 = isset($OUTPUT_MOI_12[$customer_id][$date_delivery][$agent_id])?$OUTPUT_MOI_12[$customer_id][$date_delivery][$agent_id]:'';
                                    if($MOI_12!=''){
                                        /* sum row and col */
                                        $SumRowMoi+=$MOI_12;
                                        if(isset($SumCol[$date_delivery]['MOI12']))
                                            $SumCol[$date_delivery]['MOI12']+=$MOI_12;
                                        else
                                            $SumCol[$date_delivery]['MOI12']=$MOI_12;
                                        $SumColAll['MOI12']+=$MOI_12;
                                        /* sum row and col */
                                        $MOI_12 = ActiveRecord::formatCurrencyRound($MOI_12);
                                    }
                                    if(!empty($BO_REAL)){
                                        $BO_REAL = "<span class='hight_light'>$BO_REAL</span><br />$MOI_12";
                                    }else{
                                        $BO_REAL = $MOI_12;
                                    }
                                }
                            ?>
                            <?php echo  "$BO_REAL"; ?>
                        </td>
                        <?php endforeach; // end foreach($ARR_DAYS as $date_delivery): ?>
                        <td class="sum_row_hide" style="display: none;">
                            <?php
                                $SumRowMoi = $SumRowMoi!=0?ActiveRecord::formatCurrencyRound($SumRowMoi):'';
                                $SumRowBo = ($SumRowBo!=0?ActiveRecord::formatCurrencyRound($SumRowBo):'');
                                if($model->ext_is_maintain==STORE_CARD_KH_MOI){
                                    if(!empty($SumRowBo)){
                                        $SumRowBo = "<span class='hight_light'>$SumRowBo</span><br />$SumRowMoi";
                                    }else{
                                        $SumRowBo = $SumRowMoi;
                                    }
                                }
                                echo $SumRowBo;
                            ?>
                        </td>                         
                    </tr>
                    
                <?php endforeach; // end foreach($arr_customer_id as $customer_id=>$arr_agent) ?>
                    <?php // die; ?>
                <tr class="h_50">
                    <td></td>
                    <td class="item_r item_b" >Tổng Cộng</td>
                    <td class="item_r item_b">
                    <?php 
//                        $SumColAllBo = $SumColAll['BO'];
//                        $SumColAllMoi12 = $SumColAll['MOI12'];
                        $SumColAll['BO'] = ActiveRecord::formatCurrencyRound($SumColAll['BO']);
                        if($model->ext_is_maintain==STORE_CARD_KH_MOI){
                            if(!empty($SumColAll['BO'])){
                                $SumColAll['BO'] = "<span class='hight_light'>".$SumColAll['BO']."</span><br />".ActiveRecord::formatCurrencyRound($SumColAll['MOI12']);
                            }else{
                                $SumColAll['BO'] = ActiveRecord::formatCurrencyRound($SumColAll['MOI12']);
                            }
                        }
                        echo $SumColAll['BO'];
                    ?>
                    </td>
                    <td></td>
                    
                    <?php foreach($ARR_DAYS as $date_delivery): ?>
                    <td class="item_r item_b">
                        <?php
                            $SumColBo = isset($SumCol[$date_delivery]['BO'])?ActiveRecord::formatCurrencyRound($SumCol[$date_delivery]['BO']):'';
                            $SumColMoi12 = isset($SumCol[$date_delivery]['MOI12'])?ActiveRecord::formatCurrencyRound($SumCol[$date_delivery]['MOI12']):'';
                            if($model->ext_is_maintain==STORE_CARD_KH_MOI){
                                if(!empty($SumColBo)){
                                    $SumColBo = "<span class='hight_light'>$SumColBo</span><br />$SumColMoi12";
                                }else{
                                    $SumColBo = $SumColMoi12;
                                }
                            }
                            echo $SumColBo;
                        ?>
                    </td>
                    <?php endforeach; // end foreach($arr_customer_id as $customer_id=>$arr_agent) ?>
                </tr>
                    
            </tbody>
        </table>
    </div>
</div>

<script>
    $(function(){
        $('.sum_row_hide').each(function(){
            var tr = $(this).closest('tr');
            tr.find('.sum_row_show').html($(this).html());
        });
    });
</script>