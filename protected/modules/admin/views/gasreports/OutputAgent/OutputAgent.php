<?php
$this->breadcrumbs=array(
	'Báo cáo sản lượng đại lý',
);
$aType = GasStoreCard::$DAILY_INTERNAL_TYPE_CUSTOMER;
unset($aType[CUSTOMER_OTHER]);
?>

<h1>Báo cáo sản lượng đại lý</h1>
<div class="search-form" style="">
    <div class="wide form">

    <?php $form=$this->beginWidget('CActiveForm', array(
            'action'=>Yii::app()->createUrl($this->route),
            'method'=>'post',
    )); ?>
            <?php if(Yii::app()->user->hasFlash('successUpdate')): ?>
                <div class="success_div"><?php echo Yii::app()->user->getFlash('successUpdate');?></div>
            <?php endif; ?>
            <div class="row">
                <?php // vì cột is_maintain không dùng trong loại KH của thẻ kho nên ta sẽ dùng cho 1: KH bình bò, 2. KH mối
                    echo $form->labelEx($model,'ext_is_maintain'); ?>
                    <?php // echo $form->dropDownList($model,'ext_is_maintain', CmsFormatter::$CUSTOMER_BO_MOI,array('style'=>'width:159px','empty'=>'Select')); ?>
                    <?php echo $form->dropDownList($model,'ext_is_maintain', $aType, array('class'=>' w-220')); ?>
            </div>
                
            <div class="row more_col">
                <div class="col1">
                        <?php echo Yii::t('translation', $form->label($model,'date_from')); ?>
                        <?php 
                            $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                                'model'=>$model,        
                                'attribute'=>'date_from',
                                'options'=>array(
                                    'showAnim'=>'fold',
                                    'dateFormat'=> MyFormat::$dateFormatSearch,
                                    'changeMonth' => true,
                                    'changeYear' => true,
                                    'showOn' => 'button',
                                    'buttonImage'=> Yii::app()->theme->baseUrl.'/admin/images/icon_calendar_r.gif',
                                    'buttonImageOnly'=> true,                                
                                ),        
                                'htmlOptions'=>array(
                                    'class'=>'w-16',
                                    'size'=>'16',
                                    'style'=>'float:left;',                               
                                    'readonly'=>1,
                                ),
                            ));
                        ?>     		
                </div>
                <div class="col2">
                        <?php echo Yii::t('translation', $form->label($model,'date_to')); ?>
                        <?php 
                            $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                                'model'=>$model,        
                                'attribute'=>'date_to',
                                'options'=>array(
                                    'showAnim'=>'fold',
                                    'dateFormat'=> MyFormat::$dateFormatSearch,
                                    'changeMonth' => true,
                                    'changeYear' => true,
                                    'showOn' => 'button',
                                    'buttonImage'=> Yii::app()->theme->baseUrl.'/admin/images/icon_calendar_r.gif',
                                    'buttonImageOnly'=> true,                                
                                ),        
                                'htmlOptions'=>array(
                                    'class'=>'w-16',
                                    'size'=>'16',
                                    'style'=>'float:left;',
                                    'readonly'=>1,
                                ),
                            ));
                        ?>     		
                </div>
            </div>

            <div class="row buttons" style="padding-left: 159px; margin:20px 0;">
                <?php $this->widget('bootstrap.widgets.TbButton', array(
                    'buttonType'=>'submit',
                    'label'=>Yii::t('translation','Xuất Excel'),
                    'type'=>'null', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
                    'size'=>'small', // null, 'large', 'small' or 'mini'
                    //'htmlOptions' => array('style' => 'margin-bottom: 10px; float: right;'),
                )); ?>	
            </div>

    <?php $this->endWidget(); ?>

    </div><!-- wide form -->
</div><!-- search-form -->

<?php // include_once 'View_store_movement_summary_print.php';?>

<style>
    .ui-datepicker-trigger { float: left;}
</style>

<script>
$(document).ready(function(){

});
</script>