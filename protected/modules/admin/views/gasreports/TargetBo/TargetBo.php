<?php
$this->breadcrumbs=array(
	'Xem báo cáo target',
);?>

<h1><?php echo "Hôm Nay: ".MyFormat::$TheDaysOfTheWeek[date('l')].' '.date('d-m-Y'); ?> - Xem báo cáo target</h1>
<div class="search-form" style="">
    <div class="wide form">
    <?php $form=$this->beginWidget('CActiveForm', array(
            'action'=>Yii::app()->createUrl($this->route),
            'method'=>'post',
    )); ?>
            <?php if(Yii::app()->user->hasFlash('successUpdate')): ?>
                <div class="success_div"><?php echo Yii::app()->user->getFlash('successUpdate');?></div>         
            <?php endif; ?>    
                
            <div class="row more_col">
                <div class="col1">
                        <?php echo Yii::t('translation', $form->label($model,'date_delivery')); ?>
                        <?php 
                            $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                                'model'=>$model,        
                                'attribute'=>'date_delivery',
                                'options'=>array(
                                    'showAnim'=>'fold',
                                    'dateFormat'=> MyFormat::$dateFormatSearch,
        //                            'minDate'=> '0',
                                    'maxDate'=> '0',
                                    'changeMonth' => true,
                                    'changeYear' => true,
                                    'showOn' => 'button',
                                    'buttonImage'=> Yii::app()->theme->baseUrl.'/admin/images/icon_calendar_r.gif',
                                    'buttonImageOnly'=> true,                                
                                ),        
                                'htmlOptions'=>array(
                                    'class'=>'w-16',
                                    'size'=>'16',
                                    'style'=>'height:20px;float:left;',                               
                                ),
                            ));
                        ?>     		
                </div>
                <div class="col2">
                        <?php echo Yii::t('translation', $form->label($model,'statistic_month')); ?>
                        <?php echo $form->dropDownList($model,'statistic_month', ActiveRecord::getMonthVnWithZeroFirst(), array('class'=>'w-150', 'empty'=>'Select')); ?>		
                </div>

                <div class="col3">
                    <?php echo Yii::t('translation', $form->labelEx($model,'statistic_year')); ?>
                    <?php echo $form->dropDownList($model,'statistic_year', ActiveRecord::getRangeYear(), array('class'=>'w-150')); ?>		
                </div>
            </div>
            <div class="row more_col">
                <div class="col1 display_none">
                    <?php 
                    // vì cột is_maintain không dùng trong loại KH của thẻ kho nên ta sẽ dùng cho 1: KH bình bò, 2. KH mối
                    echo $form->labelEx($model,'ext_is_maintain'); ?>
                    <?php // echo $form->dropDownList($model,'ext_is_maintain', CmsFormatter::$CUSTOMER_BO_MOI,array('class'=>' w-200','empty'=>'Select')); ?>
                    <?php echo $form->dropDownList($model,'ext_is_maintain', GasStoreCard::$DAILY_INTERNAL_TYPE_CUSTOMER, array('class'=>' w-200')); ?>
                </div>
            </div>

            <div class="row buttons" style="padding-left: 159px; margin:20px 0;">
                            <?php $this->widget('bootstrap.widgets.TbButton', array(
                'buttonType'=>'submit',
                'label'=>Yii::t('translation','Xem Thống Kê'),
                'type'=>'null', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
                'size'=>'small', // null, 'large', 'small' or 'mini'
                //'htmlOptions' => array('style' => 'margin-bottom: 10px; float: right;'),
            )); ?>	
            <?php // if(ControllerActionsName::isAccessAction('statistic_maintain_export_excel', $actions)):?>
            <?php if(0):?>
                    <input class='statistic_maintain_export_excel' next='<?php echo Yii::app()->createAbsoluteUrl('admin/gasmaintain/statistic_maintain_export_excel');?>' type='button' value='Xuất Thống Kê Hiện Tại Ra Excel'>
            <?php endif;?>

            </div>

    <?php $this->endWidget(); ?>

    </div><!-- wide form -->
</div><!-- search-form -->

<?php // include_once 'View_store_movement_summary_print.php';?>

<style>
    .ui-datepicker-trigger { float: left;}
</style>

<script>
$(document).ready(function(){
    $('.form').find('button:submit').click(function(){        
        $.blockUI({ overlayCSS: { backgroundColor: '#fff' } }); 
    });   
});
</script>    
<?php include 'TargetBoForm.php'; ?>