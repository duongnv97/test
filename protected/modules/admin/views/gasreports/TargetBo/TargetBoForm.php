<?php 
$cty_target_bo          = 0;
$cty_target_binh_moi    = 0;
$cty_thuc_te_bo         = 0;
$cty_thuc_te_binh_moi   = 0;
$cty_remain             = 0;
$PROVINCE_MODEL     = GasProvince::getArrModel();
$FOR_MONTH          = $model->statistic_month;
$FOR_YEAR           = $model->statistic_year;
$SALE_MODEL_BO      = isset($data['SALE_MODEL'])?$data['SALE_MODEL']:array();
$aOutput             = isset($data['OUTPUT'])?$data['OUTPUT']:array();

$textInfo = "Thống kê target ";
if(!empty($model->date_delivery)){
    $tmpDate = explode('-', $model->date_delivery);
    if(count($tmpDate)!=3){
        $tmpDate[0] = date('Y');
        $tmpDate[1] = date('m');
    }    
    $textInfo.="từ ngày 01-$tmpDate[1] đến ngày $model->date_delivery";
}else{
    $textInfo.="tháng $model->statistic_month năm $model->statistic_year";
}

?>
<div id="tabs-SALE_MODEL">
    <h1><?php echo $textInfo;?></h1>
    <div class="clearfix">
        <div class="color_type_1 color_note"></div> Gas Bình Bò
        <div class="clr"></div>
        <!--<div class="color_type_2 color_note"></div> Gas Bình Mối-->
    </div>
    <div class="clr"></div>

    <div class="box_310">
        <table class="hm_table items">
            <tbody>
                <tr>
                    <td colspan="3" class="item_c item_b">CÔNG TY</td>
                </tr>
                <tr>
                    <td class="item_c w-120">Target</td>
                    <td class="item_c w-140">Thực Tế</td>
                    <td class="item_c w-100">Tỷ Lệ</td>
                </tr>                       

                <tr class="color_type_1">
                    <td class="item_r cty_target_bo"></td>
                    <td class="item_r cty_thuc_te_bo"></td>
                    <td class="item_c cty_bo_percent"></td>
                </tr>
                <tr class="color_type_2">
                    <td class="item_r ">Gas dư</td>
                    <td class="item_r cty_remain"></td>
                    <td class="item_c "></td>
                </tr>
                <tr class="color_type_2 display_none">
                    <td class="item_r cty_target_binh_moi"></td>
                    <td class="item_r cty_thuc_te_binh_moi"></td>
                    <td class="item_c cty_binh_moi_percent"></td>
                </tr>

            </tbody>
        </table>
    </div>            
    <div class="clr"></div>

    <?php foreach($SALE_MODEL_BO as $province_id=>$aModelUser): ?>
    <?php if($province_id == GasProvince::KV_MIEN_TAY) { continue; } ?>
    <div class="wrap_province">
        <h1 class="l_padding_50 margin_0 padding_0"><?php echo isset($PROVINCE_MODEL[$province_id])?$PROVINCE_MODEL[$province_id]->name:"";?></h1>
        <div class="box_310 sum_province_top"></div><br/>
        <div class="clr"></div>
        <?php 
            $sumTaget           = 0;
            $sumReal            = 0;
            $sumRemain          = 0;
            $provincePercent    = 0;
        ?>
        <?php foreach($aModelUser as $mUser): ?>
            <div class="box_310">
                <table class="hm_table items">
                    <tbody>
                        <tr>
                            <td colspan="3" class="item_c item_b"><?php echo $mUser->first_name;?></td>
                        </tr>
                        <tr>
                            <td class="item_c w-120">Target</td>
                            <td class="item_c w-140">Thực Tế</td>
                            <td class="item_c w-100">Tỷ Lệ</td>
                        </tr>
                        <?php foreach(CmsFormatter::$CUSTOMER_BO_MOI as $type=>$label):?>
                        <?php
                            if($type!=STORE_CARD_KH_BINH_BO)  continue;
                            $uid = $mUser->id;
                            $qty50  = isset($aOutput[$uid][MATERIAL_TYPE_BINHBO_50]) ? $aOutput[$uid][MATERIAL_TYPE_BINHBO_50]:0;
                            $qty45  = isset($aOutput[$uid][MATERIAL_TYPE_BINHBO_45]) ? $aOutput[$uid][MATERIAL_TYPE_BINHBO_45]:0;
                            $qty12  = isset($aOutput[$uid][MATERIAL_TYPE_BINH_12]) ? $aOutput[$uid][MATERIAL_TYPE_BINH_12]:0;
                            $qty6   = isset($aOutput[$uid][MATERIAL_TYPE_BINH_6]) ? $aOutput[$uid][MATERIAL_TYPE_BINH_6]:0;
                            $qty4   = isset($aOutput[$uid][GasMaterialsType::MATERIAL_BINH_4KG]) ? $aOutput[$uid][GasMaterialsType::MATERIAL_BINH_4KG]:0;
                            $OUTPUT = $qty50*CmsFormatter::$MATERIAL_VALUE_KG[MATERIAL_TYPE_BINHBO_50] + $qty45*CmsFormatter::$MATERIAL_VALUE_KG[MATERIAL_TYPE_BINHBO_45] + 
                                    $qty12*CmsFormatter::$MATERIAL_VALUE_KG[MATERIAL_TYPE_BINH_12] + $qty6*CmsFormatter::$MATERIAL_VALUE_KG[MATERIAL_TYPE_BINH_6] + $qty4*CmsFormatter::$MATERIAL_VALUE_KG[GasMaterialsType::MATERIAL_BINH_4KG];
                            $target = isset($data['aTargetEachMonth'][$uid][$type][$FOR_MONTH])?$data['aTargetEachMonth'][$uid][$type][$FOR_MONTH]:0;
                            $REMAIN = isset($data['REMAIN'][$uid]) ? $data['REMAIN'][$uid]:0;
                            $real   = $OUTPUT-$REMAIN;
                            $percent = $real;
                            if($target){
                                $percent = ($real/$target)*100;
                            }
                            // for target cty

                            if($type==STORE_CARD_KH_BINH_BO){
                                $cty_target_bo += $target;
                                $cty_thuc_te_bo += $real;
                                $cty_remain += $REMAIN;

                            }else{
                                $cty_target_binh_moi += $target;
                                $cty_thuc_te_binh_moi += $real;
                            }
                            // for target cty // nếu có xuất excel thì sẽ dùng con số + cuối cùng đưa vào session của excel
                            // thì đỡ phải tính ở bên dưới
                            $sumTaget   += $target;
                            $sumReal    += $real;
                            $sumRemain  += $REMAIN;
                        ?>

                        <tr class="color_type_<?php echo $type;?>">
                            <td class="item_r"><?php echo ActiveRecord::formatCurrency($target);?></td>
                            <td class="item_r"><?php echo ActiveRecord::formatCurrencyRound($real);?></td>
                            <td class="item_c"><?php echo round($percent,1);?> %</td>
                        </tr>
                        <tr class="color_type_2">
                            <td class="item_r">Gas dư</td>
                            <td class="item_r"><?php echo ActiveRecord::formatCurrencyRound($REMAIN);?></td>
                            <td class="item_c"></td>
                        </tr>
                        <?php endforeach;?>

                    </tbody>
                </table>
            </div>                

        <?php endforeach; // end foreach($aModelUser as $mUser): ?>
        <?php
            if($sumTaget){
                $provincePercent = ($sumReal/$sumTaget)*100;
            }
        ?>
        <div class="sum_province_bot" style="display: none">
                <table class="hm_table items">
                    <tbody>
                        <tr>
                            <td colspan="3" class="item_c item_b">TỔNG CỘNG</td>
                        </tr>
                        <tr>
                            <td class="item_c w-120">Target</td>
                            <td class="item_c w-140">Thực Tế</td>
                            <td class="item_c w-100">Tỷ Lệ</td>
                        </tr>
                        <tr class="color_type_1">
                            <td class="item_r"><?php echo ActiveRecord::formatCurrency($sumTaget);?></td>
                            <td class="item_r"><?php echo ActiveRecord::formatCurrencyRound($sumReal);?></td>
                            <td class="item_c"><?php echo round($provincePercent, 1);?> %</td>
                        </tr>
                        <tr class="color_type_2">
                            <td class="item_r">Gas dư</td>
                            <td class="item_r"><?php echo ActiveRecord::formatCurrencyRound($sumRemain);?></td>
                            <td class="item_c"></td>
                        </tr>
                    </tbody>
                </table>
        </div>
    </div> <!-- end  <div class="wrap_province"> -->
    <div class="clr"></div>
    <?php endforeach; // end <?php foreach($SALE_MODEL_BO as $province_id=>$aModelUser): ?>
    <div class="clr"></div>
</div>
    
<input class="hide_cty_target_bo" value="<?php echo ActiveRecord::formatCurrency($cty_target_bo);?>" type="hidden">
<input class="hide_cty_target_binh_moi" value="<?php echo ActiveRecord::formatCurrency($cty_target_binh_moi);?>" type="hidden">
<input class="hide_cty_thuc_te_bo" value="<?php echo ActiveRecord::formatCurrencyRound($cty_thuc_te_bo);?>" type="hidden">
<input class="hide_cty_thuc_te_binh_moi" value="<?php echo ActiveRecord::formatCurrency($cty_thuc_te_binh_moi);?>" type="hidden">
<input class="hide_cty_remain" value="<?php echo ActiveRecord::formatCurrencyRound($cty_remain);?>" type="hidden">

<input class="hide_cty_bo_percent" value="<?php echo round($cty_target_bo?(($cty_thuc_te_bo/$cty_target_bo)*100):$cty_thuc_te_bo,1) ;?> %" type="hidden">
<input class="hide_cty_binh_moi_percent" value="<?php echo round($cty_target_binh_moi?(($cty_thuc_te_binh_moi/$cty_target_binh_moi)*100):$cty_thuc_te_binh_moi,1);?> %" type="hidden">

<script>
    $(function(){
        // for tab 1
        $('.cty_target_bo').text($('.hide_cty_target_bo').val());
        $('.cty_thuc_te_bo').text($('.hide_cty_thuc_te_bo').val());
        $('.cty_bo_percent').text($('.hide_cty_bo_percent').val());
        $('.cty_remain').text($('.hide_cty_remain').val());
        // for tab 1
        
        
        // for one province
        $('.one_province_hide').each(function(){
           var parent_div = $(this).closest('div.wrap_province');
           parent_div.find('.one_province').html($(this).html());
        });
        // for total cty
        
        //for total province
        $('.sum_province_bot').each(function(){
           var parent_div = $(this).closest('div.wrap_province');
           parent_div.find('.sum_province_top').html($(this).html());
        });
    });
</script>
