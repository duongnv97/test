<?php
$this->breadcrumbs=array(
	'Xem báo cáo daily nội bộ',
);?>

<h1><?php echo "Hôm Nay: ".MyFormat::$TheDaysOfTheWeek[date('l')].' '.date('d-m-Y'); ?> - Xem báo cáo daily nội bộ</h1>
<div class="search-form" style="">
    <div class="wide form">
    <?php $form=$this->beginWidget('CActiveForm', array(
            'action'=>Yii::app()->createUrl($this->route),
            'method'=>'post',
    )); ?>
            <?php if(Yii::app()->user->hasFlash('successUpdate')): ?>
                <div class="success_div"><?php echo Yii::app()->user->getFlash('successUpdate');?></div>         
            <?php endif; ?>    
                
            <div class="row more_col">
                <div class="col1">
                        <?php echo Yii::t('translation', $form->label($model,'date_from')); ?>
                        <?php 
                            $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                                'model'=>$model,        
                                'attribute'=>'date_from',
                                'options'=>array(
                                    'showAnim'=>'fold',
                                    'dateFormat'=> MyFormat::$dateFormatSearch,
        //                            'minDate'=> '0',
                                    'maxDate'=> '0',
                                    'changeMonth' => true,
                                    'changeYear' => true,
                                    'showOn' => 'button',
                                    'buttonImage'=> Yii::app()->theme->baseUrl.'/admin/images/icon_calendar_r.gif',
                                    'buttonImageOnly'=> true,                                
                                ),        
                                'htmlOptions'=>array(
                                    'class'=>'w-16',
                                    'size'=>'16',
                                    'style'=>'float:left;',                               
                                    'readonly'=>1,
                                ),
                            ));
                        ?>     		
                </div>
                <div class="col2">
                        <?php echo Yii::t('translation', $form->label($model,'date_to')); ?>
                        <?php 
                            $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                                'model'=>$model,        
                                'attribute'=>'date_to',
                                'options'=>array(
                                    'showAnim'=>'fold',
                                    'dateFormat'=> MyFormat::$dateFormatSearch,
        //                            'minDate'=> '0',
                                    'maxDate'=> '0',
                                    'changeMonth' => true,
                                    'changeYear' => true,
                                    'showOn' => 'button',
                                    'buttonImage'=> Yii::app()->theme->baseUrl.'/admin/images/icon_calendar_r.gif',
                                    'buttonImageOnly'=> true,                                
                                ),        
                                'htmlOptions'=>array(
                                    'class'=>'w-16',
                                    'size'=>'16',
                                    'style'=>'float:left;',
                                    'readonly'=>1,
                                ),
                            ));
                        ?>     		
                </div>
            </div>
                
            <div class="row more_col">
                <div class="col1">
                    <?php echo $form->label($model,'ext_type_customer',array()); ?>
                    <?php echo $form->dropDownList($model,'ext_type_customer', GasStoreCard::$DAILY_INTERNAL_TYPE_CUSTOMER,array('style'=>'width:153px;', 'empty'=>'Select')); ?>
                </div>
                <div class="col2">
                    <?php echo $form->labelEx($model,'ext_type_gas_vo'); ?>
                    <?php echo $form->dropDownList($model,'ext_type_gas_vo', GasStoreCard::$ARR_TYPE_GAS_VO, array('class'=>'w-150', 'empty'=>'Select')); ?>
                </div>
            </div>
                
            <div class="row more_col">
                <div class="col1 ">
                    <?php echo $form->label($model,'type_in_out',array()); ?>
                    <?php // echo $form->dropDownList($model,'type_in_out', CmsFormatter::$STORE_CARD_ALL_TYPE,array('style'=>'', 'class'=>'multiselect', 'multiple'=>'multiple')); ?>
                    <div class="fix-label float_l">
                        <?php
                           $this->widget('ext.multiselect.JMultiSelect',array(
                                 'model'=>$model,
                                 'attribute'=>'type_in_out',
                                 'data'=>CmsFormatter::$STORE_CARD_ALL_TYPE,
                                 // additional javascript options for the MultiSelect plugin
                                 'options'=>array('selectedList' => 15,),
                                 // additional style
//                                 'htmlOptions'=>array('class' => 'w-150'),
                           ));    
                       ?>
                    </div>                     
                </div>
                <?php if(Yii::app()->user->role_id!=ROLE_SUB_USER_AGENT):?>
                <div class="col2">
                    <?php echo $form->label($model,'agent_id'); ?>
                    <?php echo $form->dropDownList($model,'agent_id', Users::getSelectByRole(),array('style'=>'')); ?>
                </div>
                <?php endif; ?>
            </div>

            <div class="row more_col">
                <div class="col1">
                    <?php echo $form->labelEx($model,'car_id'); ?>
                    <?php echo $form->dropDownList($model,'car_id', Users::getArrDropdown(ROLE_CAR),array('class'=>'w-200', 'empty'=>'Select')); ?>
                </div>
                <div class="col2">
                    <?php echo $form->labelEx($model,'road_route'); ?>
                    <?php echo $form->dropDownList($model,'road_route', $model->getArrRoadRoute(),array('class'=>' w-200','empty'=>'Select')); ?>
                </div>
                <div class="col3">
                    <?php echo $form->labelEx($model,'ext_materials_type_id'); ?>
                    <?php echo $form->dropDownList($model,'ext_materials_type_id', GasMaterialsType::getAllItem(),array('class'=>'w-200','empty'=>'Select')); ?>
                </div>
            </div>
            
            <div class="row">
                <?php echo $form->labelEx($model,'materials_name', array('class'=>'label_materials_name')); ?>                
                <?php echo $form->hiddenField($model,'ext_materials_id'); ?>
                <?php 
                    // widget auto complete search material
                    $aData = array(
                        'model'=>$model,
                        'name_relation_material'=>'materials',
                        'field_autocomplete_name'=>'materials_name',
                        'field_material_id'=>'ext_materials_id',
                    );
                    $this->widget('ext.GasAutocompleteMaterial.GasAutocompleteMaterial',
                        array('data'=>$aData));                                        
                ?>                
            </div>
                
            <div class="row">
                <?php echo $form->label($model,'customer_id'); ?>
                <?php echo $form->hiddenField($model,'customer_id'); ?>
                <?php 
                    // widget auto complete search user customer and supplier
                    $url = Yii::app()->createAbsoluteUrl('admin/ajax/search_user_by_code');
                    $aData = array(
                        'model'=>$model,
                        'name_relation_user'=>'customer',
                        'url'=>$url,
                    );
                    $this->widget('ext.SpaAutocompleteCustomer.SpaAutocompleteCustomer',
                        array('data'=>$aData));                                        
                ?>
            </div>
                

            <div class="row buttons" style="padding-left: 159px; margin:20px 0;">
                            <?php $this->widget('bootstrap.widgets.TbButton', array(
                'buttonType'=>'submit',
                'label'=>Yii::t('translation','Xem Thống Kê'),
                'type'=>'null', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
                'size'=>'small', // null, 'large', 'small' or 'mini'
                //'htmlOptions' => array('style' => 'margin-bottom: 10px; float: right;'),
            )); ?>	
            <?php // if(ControllerActionsName::isAccessAction('statistic_maintain_export_excel', $actions)):?>
            <?php if(0):?>
                    <input class='statistic_maintain_export_excel' next='<?php echo Yii::app()->createAbsoluteUrl('admin/gasmaintain/statistic_maintain_export_excel');?>' type='button' value='Xuất Thống Kê Hiện Tại Ra Excel'>
            <?php endif;?>

            </div>

    <?php $this->endWidget(); ?>

    </div><!-- wide form -->
</div><!-- search-form -->

<?php // include_once 'View_store_movement_summary_print.php';?>

<style>
    .ui-datepicker-trigger { float: left;}
</style>

<script>
$(document).ready(function(){
    $('.form').find('button:submit').click(function(){        
        $.blockUI({ overlayCSS: { backgroundColor: '#fff' } }); 
    });
    
    $('.portlet-content').removeClass('portlet-content');
    
});
</script>    
<?php include 'DailyInternal_form.php'; ?>
