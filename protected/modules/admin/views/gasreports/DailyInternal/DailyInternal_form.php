<?php if(isset($data['DATA'])) :?>
<?php Yii::app()->clientScript->registerCoreScript('jquery.ui'); ?>
<?php
$gridCss = Yii::app()->getAssetManager()->publish(Yii::getPathOfAlias('zii.widgets.assets')).'/gridview/styles.css';
Yii::app()->getClientScript()->registerCssFile($gridCss);
?>
<script>
$(function() {
    $( "#tabs" ).tabs();
    $('.items').floatThead();
});
$(window).load(function(){
    fnAddClassOddEven('items');
});

</script>
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/jquery.floatThead.min.js"></script>    
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/underscore-min.js"></script>    


<?php
$DATA_RES= $data['DATA'];
$MATERIAL_MODEL= $data['MATERIAL_MODEL'];
$CUSTOMER_MODEL= isset($data['CUSTOMER_MODEL'])?$data['CUSTOMER_MODEL']:array();
$cmsFormater = new CmsFormatter();
$sum_import = 0;
$sum_export = 0;
?>

<div class="title_table_statistic">DAILY NỘI BỘ 
        <?php echo " Từ Ngày $model->date_from Đến Ngày $model->date_to" ?>
</div>
<div class="grid-view grid-view-scroll">
    <table class="hm_table items " style="">
        <thead>
            <tr>                               
                <th class="w-10 item_c">Ngày</th>
                <th class="w-150 item_c">Tên KH</th>
                <th class="w-150 item_c">Địa Chỉ</th>
                <th class="w-150 item_c">Thương Hiệu</th>
                <th class="w-100 item_c">Nhập</th>
                <th class="w-100 item_c">Xuất</th>
                <th class="w-70 item_c">Người Giao</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach($DATA_RES as $date_delivery=>$aCustomer): ?>
                    <?php
                        $date_delivery_text = $cmsFormater->formatDate($date_delivery);
                    ?>
                <?php foreach($aCustomer as $customer_id=>$aMaterials): ?>
                    <?php 
                        $customer_name = 'Hộ Gia Đình';
                        $customer_add = '';
                        if(isset($CUSTOMER_MODEL[$customer_id])){
                            $customer_name = $cmsFormater->formatNameUser($CUSTOMER_MODEL[$customer_id]);
                            $customer_add = $CUSTOMER_MODEL[$customer_id]->address;
                        }
                    ?>
                    <?php foreach($aMaterials as $materials_id=>$aType_store_card): ?>
                    <?php 
                        $import = '';
                        $export = '';
                        $delivery_person = '';
                        $import_text = '';
                        $export_text = '';
                        if(isset($DATA_RES[$date_delivery][$customer_id][$materials_id][TYPE_STORE_CARD_IMPORT])){
                            $import = $DATA_RES[$date_delivery][$customer_id][$materials_id][TYPE_STORE_CARD_IMPORT]->qty;
                            $sum_import+=$import;
                            $import_text = ActiveRecord::formatCurrency($import);
                            $delivery_person = $DATA_RES[$date_delivery][$customer_id][$materials_id][TYPE_STORE_CARD_IMPORT]->delivery_person;
                        }
                        if(isset($DATA_RES[$date_delivery][$customer_id][$materials_id][TYPE_STORE_CARD_EXPORT])){
                            $export = $DATA_RES[$date_delivery][$customer_id][$materials_id][TYPE_STORE_CARD_EXPORT]->qty;
                            $sum_export+=$export;
                            $export_text = ActiveRecord::formatCurrency($export);
                            if(trim($delivery_person)!='')
                                $delivery_person .= " - ". $DATA_RES[$date_delivery][$customer_id][$materials_id][TYPE_STORE_CARD_EXPORT]->delivery_person;
                            else
                                $delivery_person = $DATA_RES[$date_delivery][$customer_id][$materials_id][TYPE_STORE_CARD_EXPORT]->delivery_person;
                        }
                    ?>
                    <tr>
                        <td class="item_c"><?php echo $date_delivery_text;?></td>
                        <td class=""><?php echo $customer_name;?></td>
                        <td class=""><?php echo $customer_add;?></td>
                        <td class=""><?php echo $MATERIAL_MODEL[$materials_id]->name;?></td>
                        <td class="item_r"><?php echo $import_text;?></td>
                        <td class="item_r"><?php echo $export_text;?></td>
                        <td class=""><?php echo $delivery_person;?></td>
                    </tr>
            
                    <?php endforeach;// end foreach($aCustomer as $customer_id=>$aMaterials ?>
                <?php endforeach;// end foreach($aCustomer as $customer_id=>$aMaterials ?>
            <?php endforeach; // end foreach($DATA_RES as $date_delivery ?>
        </tbody>
        <tfoot>
            <tr>
                <td class="item_r item_b" colspan="4">Tổng Cộng</td>
                <td class="item_r item_b"><?php echo ActiveRecord::formatCurrency($sum_import);?></td>
                <td class="item_r item_b"><?php echo ActiveRecord::formatCurrency($sum_export);?></td>
                <td></td>
            </tr>
        </tfoot>
    </table>
        
</div>


<?php endif; ?>
