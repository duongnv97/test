<div id="tabs">
    <ul>        
        <?php foreach(GasStoreCard::$TYPE_CUSTOMER as $key=>$type_customer_code):?>
            <?php 
                if(!isset($DETAIL_12KG[$type_customer_code]['total_year']))
                continue;
                $text12 = 'Hộ Gia Đình';
                if(in_array($key, CmsFormatter::$aTypeIdBoMoi)){
                    $text12 = CmsFormatter::$CUSTOMER_BO_MOI[$key];
                }
            ?>
            <li><a class="tab_month" month_current="<?php echo $type_customer_code;?>" href="#tabs-<?php echo $type_customer_code;?>"><?php echo $text12;?> 12KG</a></li>
        <?php endforeach; // end foreach(GasStoreCard::$TYPE_CUSTOMER ?>

        <?php foreach(CmsFormatter::$MATERIAL_TYPE_OUTPUT_STATISTIC as $type_output):?>
            <?php $code = $type_output['code']; ?>
            <li><a class="tab_month" month_current="<?php echo $code;?>" href="#tabs-<?php echo $code;?>">Tổng <?php echo $code;?></a></li>
        <?php endforeach; // end foreach(CmsFormatter::$MATERIAL_TYPE_OUTPUT_STATISTIC ?>
<!--        <li><a class="tab_month" month_current="MONEY_BANK" href="#tabs-MONEY_BANK">Nộp Công Ty</a></li>
        <li><a class="tab_month" month_current="TOTAL_REVENUE" href="#tabs-TOTAL_REVENUE">Doanh Thu</a></li>-->
    </ul>

    <?php include 'form_detail_12kg.php'; ?>  

    <?php foreach(CmsFormatter::$MATERIAL_TYPE_OUTPUT_STATISTIC as $type_output):?>
        <?php  // lặp 2 loại bình thống kê là 12 và 45kg        
            $code = $type_output['code']; 
            $STT = 0;
        ?>
        <div id="tabs-<?php echo $code;?>">
            <div class="grid-view grid-view-scroll">
                <div class="title_table_statistic">TỔNG SẢN LƯỢNG GAS BÌNH <?php echo $code;?> NĂM <?php echo $YEARS;?></div>
                <div class="grid-view-scroll">
                    <table class="hm_table table_statistic items table_month_<?php echo $code;?>" style="">
                        <thead>
                            <tr>                               
                                <th class="w-10 item_c">STT</th>
                                <th class="w-70">Tên đại lý</th>
                                <th class="w-50">Tổng</th>
                                <th class="w-50">BQ</th>
                                <?php foreach($MONTHS as $month=>$tempM): ?>
                                <th class="item_day" style="">
                                    <?php echo "T".$month;?>
                                </th>
                                <?php endforeach;?>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach($OUTPUT[$code]['total_year']['sum_row'] as $agent_id=>$total_year_agent):?>
                            <?php $STT++;?>
                            <tr>
                                <td class=""><?php echo $STT; ?></td>
                                <td class=""><?php echo $AGENT_MODEL[$agent_id]->first_name; ?></td>
                                <td class="item_r item_b"><?php echo ActiveRecord::formatCurrency($total_year_agent);?></td>
                                <td class="item_r item_b"><?php 
                                    $average = round($total_year_agent/count($MONTHS));
                                    echo ActiveRecord::formatCurrency($average); ?>
                                </td>
                                <?php foreach($MONTHS as $month=>$tempM): ?>																	
                                <td class="item_r item_day w-80">
                                    <?php 
                                        $output_month = isset($OUTPUT[$code]['total_year'][$month][$agent_id])?$OUTPUT[$code]['total_year'][$month][$agent_id]:"";
                                        echo $output_month>0?ActiveRecord::formatCurrency($output_month):'';
                                    ?>
                                </td>
                                <?php endforeach; // end <?php foreach($days_of_month as $day) ?>
                            </tr>
                            <?php ?>
                            <?php endforeach; // end foreach($OUTPUT[$code]['sum_row_total_month'][$month]  ?>
                            <!-- tổng cộng -->
                                <tr>
                                    <td class="">&nbsp;</td>
                                    <td class="item_b">Tổng Cộng ( bình )</td>
                                    <td class="item_r item_b">
                                        <?php $sum_col_total_year_all_agent = $OUTPUT[$code]['sum_col_total_year_all_agent'];
                                        echo ActiveRecord::formatCurrency($sum_col_total_year_all_agent);?>
                                    </td>
                                    <td class="item_r item_b"><?php
                                            $average = round($sum_col_total_year_all_agent/count($MONTHS));
                                            echo ActiveRecord::formatCurrency($average); ?>
                                    </td>
                                    <?php foreach($MONTHS as $month=>$tempM): ?>	
                                    <td class="item_r item_b">
                                        <?php 
                                            $sum_month = isset($OUTPUT[$code]['total_year']['sum_col'][$month])?$OUTPUT[$code]['total_year']['sum_col'][$month]:"";
                                            echo $sum_month>0?ActiveRecord::formatCurrency($sum_month):'';
                                        ?>
                                    </td>
                                    <?php endforeach; // end <?php foreach($days_of_month as $day) ?>                        
                                </tr>
                                <tr>
                                    <td class="">&nbsp;</td>
                                    <td class="item_b">Tổng Cộng ( KG )</td>
                                    <td class="item_r item_b">
                                        <?php $BINH_KG = KL_BINH_12;
                                            if($code=='45KG')
                                                $BINH_KG = KL_BINH_45;
                                        echo ActiveRecord::formatCurrency($sum_col_total_year_all_agent*$BINH_KG);?>
                                    </td>
                                    <td class="item_r item_b"><?php
                                            echo ActiveRecord::formatCurrency($average*$BINH_KG); ?>
                                    </td>
                                    <?php foreach($MONTHS as $month=>$tempM): ?>	
                                    <td class="item_r item_b">
                                        <?php 
                                        $sum_month = isset($OUTPUT[$code]['total_year']['sum_col'][$month])?$OUTPUT[$code]['total_year']['sum_col'][$month]:"";
                                        echo ActiveRecord::formatCurrency($sum_month*$BINH_KG); ?>
                                    </td>
                                    <?php endforeach; // end <?php foreach($days_of_month as $day) ?>                        
                                </tr>
                                <!-- tổng cộng -->
                        </tbody>
                    </table><!-- end <table class="table_statistic items table_mon-->
                </div> <!-- end <div class="grid-view-scroll">-->

            </div><!-- end <div class="grid-view">-->
        </div> <!-- end <div id="tabs-"> -->
    <?php endforeach; // end foreach(CmsFormatter::$MATERIAL_TYPE_OUTPUT_STATISTIC as $type_output)?>    

    <?php // include 'money_bank_year.php'; ?>
    <?php // include 'Revenue_output_total_revenue.php'; ?>
</div> <!-- end <div id="tabs">-->