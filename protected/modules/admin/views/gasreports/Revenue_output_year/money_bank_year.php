<?php    
// hiện tại chưa làm phần này cho tab năm Apr 13, 2014
    $STT = 0;
?>
<div class="title_table_statistic">TỔNG ĐẠI LÝ NỘP CÔNG TY NĂM <?php echo $YEARS;?></div>
<div class="grid-view-scroll">
    <table class="hm_table table_statistic items table_month_<?php echo $YEARS;?>" style="">
        <thead>
            <tr>                               
                <th class="w-10 item_c">STT</th>
                <th class="w-70">Tên đại lý</th>
                <th class="w-50">Tổng</th>
                <th class="w-50">BQ</th>
                <?php foreach($MONTHS as $month=>$tempM): ?>
                <th class="item_day" style="">
                    <?php echo "T".$month;?>
                </th>
                <?php endforeach;?>
            </tr>
        </thead>
        <tbody>
            <?php if(isset($MONEY_BANK['total_year']['sum_row'])):?>
                <?php foreach($MONEY_BANK['total_year']['sum_row'] as $agent_id=>$total_year_agent):?>
                <?php $STT++;?>
                <tr>
                    <td class=""><?php echo $STT; ?></td>
                    <td class=""><?php echo $AGENT_MODEL[$agent_id]->first_name; ?></td>
                    <td class="item_r item_b"><?php echo ActiveRecord::formatCurrency($total_year_agent);?></td>
                    <td class="item_r item_b"><?php 
                        $average = round($total_year_agent/count($MONTHS));
                        echo ActiveRecord::formatCurrency($average); ?>
                    </td>
                    <?php foreach($MONTHS as $month=>$tempM): ?>																	
                    <td class="item_r item_day w-80">
                        <?php 
                            $output_month = isset($MONEY_BANK['total_year'][$month][$agent_id])?$MONEY_BANK['total_year'][$month][$agent_id]:"";
                            echo $output_month>0?ActiveRecord::formatCurrency($output_month):'';
                        ?>
                    </td>
                    <?php endforeach; // end <?php foreach($days_of_month as $day) ?>
                </tr>
                <?php ?>
                <?php endforeach; // end foreach($MONEY_BANK['sum_row_total_month'][$month]  ?>
                <!-- tổng cộng -->
                    <tr>
                        <td class="">&nbsp;</td>
                        <td class="item_b">Tổng Cộng</td>
                        <td class="item_r item_b">
                            <?php $sum_col_total_year_all_agent = $MONEY_BANK['sum_col_total_year_all_agent'];
                            echo ActiveRecord::formatCurrency($sum_col_total_year_all_agent);?>
                        </td>
                        <td class="item_r item_b"><?php
                                $average = round($sum_col_total_year_all_agent/count($MONTHS));
                                echo ActiveRecord::formatCurrency($average); ?>
                        </td>
                        <?php foreach($MONTHS as $month=>$tempM): ?>	
                        <td class="item_r item_b">
                            <?php 
                                $sum_month = isset($MONEY_BANK['total_year']['sum_col'][$month])?$MONEY_BANK['total_year']['sum_col'][$month]:"";
                                echo $sum_month>0?ActiveRecord::formatCurrency($sum_month):'';
                            ?>
                        </td>
                        <?php endforeach; // end <?php foreach($days_of_month as $day) ?>                        
                    </tr>
                    <!-- tổng cộng -->
               <?php endif;?> 
        </tbody>
    </table><!-- end <table class="table_statistic items table_mon-->
</div> <!-- end <div class="grid-view-scroll">-->         