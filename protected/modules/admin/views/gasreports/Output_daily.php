<?php
$this->breadcrumbs=array(
	'Xem báo cáo daily sản lượng',
);
$menus=array(
    array('label'=> 'Xuất Excel Danh Sách Hiện Tại',
       'url'=>array('ExcelOutputDaily'), 
       'htmlOptions'=>array('class'=>'export_excel ','label'=>'Xuất Excel')),
);
$this->menu= ControllerActionsName::createMenusRoles($menus, $actions);
?>

<h1>Xem báo cáo daily sản lượng</h1>
<div class="search-form" style="">
    <div class="wide form">

    <?php $form=$this->beginWidget('CActiveForm', array(
            'action'=>Yii::app()->createUrl($this->route),
            'method'=>'post',
    )); ?>
    <?php if(Yii::app()->user->hasFlash('successUpdate')): ?>
        <div class="success_div"><?php echo Yii::app()->user->getFlash('successUpdate');?></div>         
    <?php endif; ?>    
    <div class="row more_col">
        <div class="col1">
            <?php echo Yii::t('translation', $form->label($model,'date_from')); ?>
            <?php 
                $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                    'model'=>$model,        
                    'attribute'=>'date_from',
                    'options'=>array(
                        'showAnim'=>'fold',
                        'dateFormat'=> MyFormat::$dateFormatSearch,
//                            'minDate'=> '0',
                        'maxDate'=> '0',
                        'changeMonth' => true,
                        'changeYear' => true,
                        'showOn' => 'button',
                        'buttonImage'=> Yii::app()->theme->baseUrl.'/admin/images/icon_calendar_r.gif',
                        'buttonImageOnly'=> true,                                
                    ),        
                    'htmlOptions'=>array(
                        'class'=>'w-16',
                        'size'=>'16',
                        'style'=>'float:left;',                               
                        'readonly'=>1,
                    ),
                ));
            ?>     		
        </div>
        <div class="col2">
            <?php echo Yii::t('translation', $form->label($model,'date_to')); ?>
            <?php 
                $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                    'model'=>$model,        
                    'attribute'=>'date_to',
                    'options'=>array(
                        'showAnim'=>'fold',
                        'dateFormat'=> MyFormat::$dateFormatSearch,
//                            'minDate'=> '0',
                        'maxDate'=> '0',
                        'changeMonth' => true,
                        'changeYear' => true,
                        'showOn' => 'button',
                        'buttonImage'=> Yii::app()->theme->baseUrl.'/admin/images/icon_calendar_r.gif',
                        'buttonImageOnly'=> true,                                
                    ),        
                    'htmlOptions'=>array(
                        'class'=>'w-16',
                        'size'=>'16',
                        'style'=>'float:left;',
                        'readonly'=>1,
                    ),
                ));
            ?>     		
        </div>
    </div>
    <div class="row">
        <?php echo $form->labelEx($model,'ext_is_maintain'); ?>
        <div class="fix-label">
            <?php
               $this->widget('ext.multiselect.JMultiSelect',array(
                     'model'=>$model,
                     'attribute'=>'ext_is_maintain',
                     'data'=> CmsFormatter::$CUSTOMER_BO_MOI_ONLY,
                     // additional javascript options for the MultiSelect plugin
                    'options'=>array('selectedList' => 9,),
                     // additional style
                     'htmlOptions'=>array('style' => 'width: 400px;'),
               ));    
           ?>
        </div>
    </div>
        
    <div class="row">
        <?php echo $form->labelEx($model,'province_id_agent'); ?>
        <div class="fix-label">
            <?php
               $this->widget('ext.multiselect.JMultiSelect',array(
                     'model'=>$model,
                     'attribute'=>'province_id_agent',
                     'data'=> GasProvince::getArrAll(),
                     // additional javascript options for the MultiSelect plugin
                    'options'=>array('selectedList' => 30,),
                     // additional style
                     'htmlOptions'=>array('style' => 'width: 400px;'),
               ));    
           ?>
        </div>
    </div>
        
    <?php $aNotIn = array(ROLE_SUB_USER_AGENT, ROLE_SALE);
    if( !in_array(Yii::app()->user->role_id, $aNotIn )):?>
        <div class="row">
            <?php echo $form->label($model,'ext_sale_id'); ?>
            <?php // echo $form->dropDownList($model,'ext_sale_id', Users::getArrUserByRole(ROLE_SALE, array('order'=>'t.code_bussiness', 'prefix_type_sale'=>1)),array('class'=>'w-250', 'style'=>'width:376px','empty'=>'Select')); ?>

            <?php echo $form->hiddenField($model,'ext_sale_id'); ?>
            <?php 
            // widget auto complete search user customer and supplier
            $aData = array(
                'model'=>$model,
                'field_autocomplete_name'=>'autocomplete_name_sale',
                'field_customer_id'=>'ext_sale_id',
                'url'=> Yii::app()->createAbsoluteUrl('admin/ajax/search_sale'),
                'name_relation_user'=>'rSale',
                'placeholder'=>'Nhập Tên Sale',
                'ClassAdd' => 'w-400',
//                        'fnSelectCustomer'=>'fnAfterSelectSaleOrAgent',
            );
            $this->widget('ext.SpaAutocompleteCustomer.SpaAutocompleteCustomer',
                array('data'=>$aData));                                        
        ?>
        </div>
    <?php endif;?>
    
    <div class="row">
    <?php echo $form->labelEx($model,'user_id_create'); ?>
    <?php echo $form->hiddenField($model,'user_id_create', array('class'=>'')); ?>
    <?php
            // 1. limit search kh của sale
            $url = Yii::app()->createAbsoluteUrl('admin/ajax/search_by_role', array('role'=> ROLE_AGENT));
            // widget auto complete search user customer and supplier
            $aData = array(
                'model'=>$model,
                'field_customer_id'=>'user_id_create',
                'url'=> $url,
                'name_relation_user'=>'user_create',
                'ClassAdd' => 'w-400',
                'field_autocomplete_name' => 'autocomplete_name_1',
                'placeholder'=>'Nhập mã hoặc tên đại lý',
            );
            $this->widget('ext.SpaAutocompleteCustomer.SpaAutocompleteCustomer',
                array('data'=>$aData));
            ?>
        <?php echo $form->error($model,'user_id_create'); ?>
    </div>

    <?php if(Yii::app()->user->role_id!=ROLE_SUB_USER_AGENT):?>
    <div class="row group_subscriber">
        <?php echo Yii::t('translation', $form->label($model,'agent_id', ['label' => 'Chọn nhiều đại lý'])); ?>
        <?php // echo $form->dropDownList($model,'agent_id', Users::getSelectByRoleFinal(ROLE_AGENT, array('gender'=>Users::IS_AGENT)),array('class'=>'multiselect', 'multiple'=>'multiple')); ?>
        <div class="fix-label">
            <?php
               $this->widget('ext.multiselect.JMultiSelect',array(
                     'model'=>$model,
                     'attribute'=>'agent_id',
//                             'data'=>  Users::getSelectByRoleFinal(ROLE_AGENT, array('gender'=>Users::IS_AGENT)),
                     'data'=>  Users::getSelectByRoleFinal(ROLE_AGENT, array()),
                     // additional javascript options for the MultiSelect plugin
//                             'options'=>array('header' => false,),
                     'options'=>array(),
                     // additional style
                     'htmlOptions'=>array('style' => 'width: 400px;'),
               ));    
           ?>
        </div> 
    </div>
    <?php endif;?>

    <div class="row more_col">
        <div class="col1">
            <?php echo $form->labelEx($model,'car_id'); ?>
            <?php echo $form->dropDownList($model,'car_id', Users::getArrDropdown(ROLE_CAR),array('class'=>'w-200 float_l', 'empty'=>'Select')); ?>
        </div>
        <div class="col2">
            <?php echo $form->labelEx($model,'road_route'); ?>
            <?php echo $form->dropDownList($model,'road_route', $model->getArrRoadRoute(),array('class'=>' w-200','empty'=>'Select')); ?>
        </div>
        <div class="col3">
            <?php echo $form->labelEx($model,'uid_login'); ?>
            <?php echo $form->dropDownList($model,'uid_login', GasTickets::$DIEU_PHOI_FULLNAME,array('class'=>' w-200','empty'=>'Select')); ?>
        </div>
    </div>

    <div class="row">
    <?php echo $form->labelEx($model,'customer_id'); ?>
    <?php echo $form->hiddenField($model,'customer_id', array('class'=>'')); ?>
    <?php
            // 1. limit search kh của sale
            $url = Yii::app()->createAbsoluteUrl('admin/ajax/search_user_by_code');
            // widget auto complete search user customer and supplier
            $aData = array(
                'model'=>$model,
                'field_customer_id'=>'customer_id',
                'url'=> $url,
                'name_relation_user'=>'rCustomer',
                'ClassAdd' => 'w-400',
                'field_autocomplete_name' => 'autocomplete_name',
            );
            $this->widget('ext.SpaAutocompleteCustomer.SpaAutocompleteCustomer',
                array('data'=>$aData));
            ?>
    </div>

    <div class="row buttons" style="padding-left: 159px; margin:20px 0;">
        <?php $this->widget('bootstrap.widgets.TbButton', array(
        'buttonType'=>'submit',
        'label'=>Yii::t('translation','Xem Thống Kê'),
        'type'=>'null', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
        'size'=>'small', // null, 'large', 'small' or 'mini'
        //'htmlOptions' => array('style' => 'margin-bottom: 10px; float: right;'),
    )); ?>	
    <?php // if(ControllerActionsName::isAccessAction('statistic_maintain_export_excel', $actions)):?>
    <?php if(0):?>
            <input class='statistic_maintain_export_excel' next='<?php echo Yii::app()->createAbsoluteUrl('admin/gasmaintain/statistic_maintain_export_excel');?>' type='button' value='Xuất Thống Kê Hiện Tại Ra Excel'>
    <?php endif;?>

    </div>

    <?php $this->endWidget(); ?>

    </div><!-- wide form -->
</div><!-- search-form -->

<?php // include_once 'View_store_movement_summary_print.php';?>

<style>
    .ui-datepicker-trigger { float: left;}
</style>

<script>
$(document).ready(function(){
    $('.form').find('button:submit').click(function(){        
        $.blockUI({ overlayCSS: { backgroundColor: '#fff' } }); 
    });   
});
</script>    
<?php include 'Output_daily_form.php'; ?>