<table class="tb display_none" style="width: 100%;">
    <tbody>
    <?php foreach($ListOptionMaterialType as $materials_type_id=>$materials_type_name):?>
        <?php $sumType = 0;
            $RemoveRowType = "RemoveRowType";
            if(!isset($MODEL_MATERIAL_TYPE[$materials_type_id])){
                continue;
            }
        ?>
        <tr>
            <td colspan="2">
                <table style="width: 100%;" class="myTable" id="sortTb<?php echo $agent_id."$materials_type_id";?>">
                    <thead>
                        <tr class="h_30">
                            <th class="item_b"><?php echo $materials_type_name; ?></th>
                            <th class="td_last_r sumType<?php echo $materials_type_id;?> item_b item_r r_padding_10"></th>
                        </tr>
                    </thead>
                    <tbody>
                    <?php foreach($MODEL_MATERIAL_TYPE[$materials_type_id] as $key=>$mMaterial): ?>
                        <?php
                            if(!isset($MODEL_MATERIAL_TYPE[$materials_type_id])){
                                continue;
                            }
                            $openingBalance = isset($OPENING_BALANCE_YEAR_BEFORE[$agent_id][$mMaterial->id])?$OPENING_BALANCE_YEAR_BEFORE[$agent_id][$mMaterial->id]:0;
                            $a_import = isset($IMPORT[$agent_id][$mMaterial->id])?$IMPORT[$agent_id][$mMaterial->id]:0;
                            $a_export = isset($EXPORT[$agent_id][$mMaterial->id])?$EXPORT[$agent_id][$mMaterial->id]:0;
                            $inventory = $openingBalance+$a_import-$a_export;                    
        //                    if(isset($SumColMaterialType[$materials_type_id][$agent_id]))
        //                        $SumColMaterialType[$materials_type_id][$agent_id] += $inventory;
        //                    else
        //                        $SumColMaterialType[$materials_type_id][$agent_id] = $inventory;
                            if($inventory == 0){
                                continue;
                            }
                            $sumType+=$inventory;
                            $SumAgent += $inventory;
                            $InventoryAll += $inventory;
                            $RemoveRowType = "";
                        ?>
                    <tr>
                        <td class=" "><?php echo $mMaterial->name; ?></td>
                        <td class="item_r td_last_r r_padding_10">
                            <?php echo ActiveRecord::formatNumberInput($inventory); ?>
                        </td>
                    </tr>
                    <?php endforeach; ?>
                    </tbody>
                </table>
            </td>
        </tr>
            <tr class="display_none">
                <td colspan="2">
                    <span style="display: none;" class="sumTypeHide <?php echo $RemoveRowType;?>" link="sumType<?php echo $materials_type_id;?>"><?php echo $sumType!=0?ActiveRecord::formatCurrency($sumType):'&nbsp;';?></span>
                </td>
            </tr>
    <?php endforeach; ?>
        <tr class="display_none">
            <td colspan="2">
                <span style="display: none;" class="sumAgentHide " link="SumAgent<?php echo $agent_id;?>"><?php echo $SumAgent!=0?ActiveRecord::formatCurrency($SumAgent):'&nbsp;';?></span>
                <?php if($SumAgent!=0) $RemoveBigRow=""; ?>
            </td>
        </tr>
    </tbody>
</table>