<?php if(isset($data['OPENING_BALANCE_YEAR_BEFORE'])) :?>
<?php Yii::app()->clientScript->registerCoreScript('jquery.ui'); ?>
<?php
$gridCss = Yii::app()->getAssetManager()->publish(Yii::getPathOfAlias('zii.widgets.assets')).'/gridview/styles.css';
Yii::app()->getClientScript()->registerCssFile($gridCss);
?>
<?php 
$OPENING_BALANCE_YEAR_BEFORE = isset($data['OPENING_BALANCE_YEAR_BEFORE'])?$data['OPENING_BALANCE_YEAR_BEFORE']:array();
$IMPORT = isset($data['IMPORT'])?$data['IMPORT']:array();
$EXPORT = isset($data['EXPORT'])?$data['EXPORT']:array();
$IMPORT_60 = isset($data['IMPORT_60'])?$data['IMPORT_60']:array();
$MODEL_MATERIAL_TYPE = isset($data['MODEL_MATERIAL_TYPE'])?$data['MODEL_MATERIAL_TYPE']:array();
$countAgent = count($ListOptionAgent);
$SumColMaterialType = array();
$InventoryAll = 0;
$InventoryAll60 = 0;
$date_to = MyFormat::dateDmyToYmdForAllIndexSearch($model->date_to);

?>

<table class="tb materials_table">
    <thead>
        <tr>
            <th class="item_c item_b w-120">Kho</th>
            <th class="item_c item_b w-300">Tổng Tồn</th>
            <th class="item_c item_b w-300 td_last_r">Tồn Quá 60 Ngày</th>
        </tr>
    </thead>
    <tbody>
    <?php foreach($ListOptionAgent as $agent_id=>$agent_name): ?>
        <tr>
            <td class="item_b l_padding_10"><?php echo $agent_name;?></td>
            <td class="">
                <?php $SumAgent=0; $RemoveBigRow = "RemoveBigRow"; ?>
                <p class="item_r item_b r_padding_10 f_size_20">
                    <a class="ClickSumAgent SumAgent<?php echo $agent_id;?>" style="text-decoration: none;" href="javascript:void(0);"></a>
                </p>
                <?php include "InventoryVoF1.php"; ?>
                <span style="display: none;" class="<?php echo $RemoveBigRow;?>"></span>
            </td>
            <td class=" td_last_r">
                <?php $SumAgent=0; ?>
                <p class="item_r item_b r_padding_10 f_size_20">
                    <a  class="ClickSumAgent SumAgent60<?php echo $agent_id;?>" style="text-decoration: none;" href="javascript:void(0);"></a>
                </p>
                <?php include "InventoryVoF2.php"; ?>
            </td>
        </tr>
    <?php endforeach; ?>
    <tr>
        <td class="item_r padding_10 item_b f_size_20">Tổng</td>
        <td class="item_r td_last_r r_padding_10 item_b f_size_20">
            <?php echo ActiveRecord::formatCurrency($InventoryAll); ?>
        </td>
        <td class="item_r td_last_r r_padding_10 item_b f_size_20">
            <?php echo ActiveRecord::formatCurrency($InventoryAll60); ?>
        </td>
    </tr>
    </tbody>
</table>

<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/__jquery.tablesorter/jquery.tablesorter.min.js"></script>
<script>
$(function() {
    fnUpdateSumRow();
    
});    

function fnUpdateSumRow(){
    $('.RemoveRowType').each(function(){
        var class_update = $(this).attr('link');
        var table = $(this).closest('table');
        table.find('.'+class_update).closest('tr').remove();
    });
    
    $('.RemoveBigRow').each(function(){
         $(this).closest('tr').remove();
    });
    
    $('.sumAgentHide').each(function(){
        var class_update = $(this).attr('link');
        var td = $(this).closest('table').closest('td');
        td.find('.'+class_update).text($(this).text());
    });
    
    $('.sumAgent60Hide').each(function(){
        var class_update = $(this).attr('link');
        var td = $(this).closest('table').closest('td');
        td.find('.'+class_update).text($(this).text());
    });
    
    $('.sumTypeHide').each(function(){
        var class_update = $(this).attr('link');
        var table = $(this).closest('table');
        table.find('.'+class_update).text($(this).text());
    });
    
//    $(this).click(function(){
//        });
    
    $('.ClickSumAgent').click(function(){
        var td = $(this).closest('td');
        td.find('table:first').toggle();
    });
}
    
$(window).load(function(){
    fnAddClassOddEven('items');
    //http://stackoverflow.com/questions/8878945/jquery-tablesorter-plugin-error-on-init-cannot-read-property-0-of-undefined
    $(".myTable, .myTable60").each(function(){
        var id = $(this).attr('id');
        if ($('#'+id+" tbody tr").length > 0){
//            $('#'+id).tablesorter(); 
            $('#'+id).tablesorter( {sortList: [[1,1]]} ); 
        }
    });
});
</script>

<?php endif; ?>

