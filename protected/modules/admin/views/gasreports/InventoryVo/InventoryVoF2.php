<?php 
$firstDate = array_search($agent_id, Sta2::$AGENT_VO);
$ok = true;
if($firstDate){
    $firstDatePlus60 = MyFormat::modifyDays($firstDate, Sta2::InventoryVo60);
    $ok = MyFormat::compareTwoDate($date_to, $firstDatePlus60);
}
?>
<?php if($ok): ?>
<table class="tb display_none" style="width: 100%;">
    <tbody>
    <?php foreach($ListOptionMaterialType as $materials_type_id=>$materials_type_name):?>
        <?php $sumType = 0;
            $RemoveRowType = "RemoveRowType";
            if(!isset($MODEL_MATERIAL_TYPE[$materials_type_id])){
                continue;
            }
        ?>
        <tr>
            <td colspan="2">
                <table style="width: 100%;" class="myTable60" id="sortTb60<?php echo $agent_id."$materials_type_id";?>">
                    <thead>
                        <tr class="h_30">
                            <th class="item_b"><?php echo $materials_type_name; ?></th>
                            <th class="td_last_r sumType<?php echo $materials_type_id;?> item_b item_r r_padding_10"></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach($MODEL_MATERIAL_TYPE[$materials_type_id] as $key=>$mMaterial): ?>
                            <?php
                            if(!isset($MODEL_MATERIAL_TYPE[$materials_type_id])){
                                continue;
                            }
                            $openingBalance = isset($OPENING_BALANCE_YEAR_BEFORE[$agent_id][$mMaterial->id])?$OPENING_BALANCE_YEAR_BEFORE[$agent_id][$mMaterial->id]:0;
                            $a_import_60 = isset($IMPORT_60[$agent_id][$mMaterial->id])?$IMPORT_60[$agent_id][$mMaterial->id]:0;
                            $a_export = isset($EXPORT[$agent_id][$mMaterial->id])?$EXPORT[$agent_id][$mMaterial->id]:0;
                            $inventory = $openingBalance+$a_import_60-$a_export;
        //                    if(isset($SumColMaterialType[$materials_type_id][$agent_id]))
        //                        $SumColMaterialType[$materials_type_id][$agent_id] += $inventory;
        //                    else
        //                        $SumColMaterialType[$materials_type_id][$agent_id] = $inventory;
                            if($inventory < 1){
                                continue;
                            }
                            $sumType+=$inventory;
                            $SumAgent += $inventory;
                            $InventoryAll60 += $inventory;
                            $RemoveRowType = "";
                        ?>
                        <tr>
                            <td class=" "><?php echo $mMaterial->name; ?></td>
                            <td class="item_r td_last_r r_padding_10">
                                <?php echo ActiveRecord::formatCurrency($inventory); ?>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                    </tbody>
                </table>
            </td>
        </tr>
            <tr class="display_none">
                <td colspan="2">
                    <span style="display: none;" class="sumTypeHide <?php echo $RemoveRowType;?>" link="sumType<?php echo $materials_type_id;?>"><?php echo $sumType!=0?ActiveRecord::formatCurrency($sumType):'&nbsp;';?></span>
                </td>
            </tr>
    <?php endforeach; ?>
        <tr class="display_none">
            <td colspan="2">
                <span style="display: none;" class="sumAgent60Hide " link="SumAgent60<?php echo $agent_id;?>"><?php echo $SumAgent!=0?ActiveRecord::formatCurrency($SumAgent):'&nbsp;';?></span>
            </td>
        </tr>
    </tbody>
</table>
<?php endif; ?>