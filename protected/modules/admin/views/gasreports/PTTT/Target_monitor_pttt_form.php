<?php if(isset($data[GasTargetMonthly::HGD_REAL])) :?>
<?php Yii::app()->clientScript->registerCoreScript('jquery.ui'); ?>
<?php
$gridCss = Yii::app()->getAssetManager()->publish(Yii::getPathOfAlias('zii.widgets.assets')).'/gridview/styles.css';
Yii::app()->getClientScript()->registerCssFile($gridCss);
?>

<?php
$OUTPUT_HGD = $data[GasTargetMonthly::HGD_REAL];
$MONITOR_MODEL= isset($data['MONITOR_MODEL'])?$data['MONITOR_MODEL']:array();
$AGENT_OF_MONITOR= isset($data['AGENT_OF_MONITOR'])?$data['AGENT_OF_MONITOR']:array();
$FOR_MONTH = $model->statistic_month;
$FOR_YEAR = $model->statistic_year;
$cmsFormater = new CmsFormatter();
?>

<div id="tabs" class="grid-view">
    <ul>
        <li>
            <a class="tab_month" href="#tabs_MONITOR_PTTT">Giám Sát PTTT</a>
        </li>
    </ul>
    
    <?php include 'Target_monitor_pttt_form1.php';?>
</div>

<script>
$(function() {
//    $( "#tabs" ).tabs({ active: 2 });
    $( "#tabs" ).tabs();
});

</script>

<?php endif; // end if(count($data)) ?>

