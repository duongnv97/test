<?php if(isset($data['EMPLOYEE'])) :?>
<?php Yii::app()->clientScript->registerCoreScript('jquery.ui'); ?>
<?php
$gridCss = Yii::app()->getAssetManager()->publish(Yii::getPathOfAlias('zii.widgets.assets')).'/gridview/styles.css';
Yii::app()->getClientScript()->registerCssFile($gridCss);
?>
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/scroll/jquery.freezetablecolumns.1.1.js"></script> 
<?php 
//$EMPLOYEE = isset($data['EMPLOYEE_SORT'])?$data['EMPLOYEE_SORT']:array();
$EMPLOYEE = isset($data['EMPLOYEE'])?$data['EMPLOYEE']:array();
$SUM_MONITOR = isset($data['SUM_MONITOR'])?$data['SUM_MONITOR']:array();
$SUM_EMPLOYEE = isset($data['SUM_EMPLOYEE'])?$data['SUM_EMPLOYEE']:array();
$USER_MODEL = isset($data['USER_MODEL'])?$data['USER_MODEL']:array();
$count_days = count($data['ARR_DAYS']);
$cUid = Yii::app()->user->id;
$cRole = Yii::app()->user->role_id;


?>

<?php include 'Pttt_daily_goback_form2.php';?>

<script>
$(function() {
//    fnUpdateSumRow();
    fnTabMonthClick();
});    
    
function fnTabMonthClick(){
    $('.freezetablecolumns').each(function(){
        var id_table = $(this).attr('id');
        $('#'+id_table).freezeTableColumns({
                width:       <?php echo Users::WIDTH_freezetable;?>,   // required - bt 1100
                height:      400,   // required
                numFrozen:   3,     // optional
                frozenWidth: 260,   // optional
                clearWidths: true  // optional
              });   
    });
}  

$(window).load(function(){
    var index = 1;
    $('.freezetablecolumns').each(function(){
        if(index==1)
            $(this).closest('div.grid-view').show();
        index++;
    });    
    fnAddClassOddEven('items');
});
</script>

<?php endif; ?>

