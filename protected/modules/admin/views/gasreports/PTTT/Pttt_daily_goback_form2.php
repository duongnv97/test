<div class="grid-view display_none">
    <div class="title_table_statistic">Báo cáo bình quay về từ ngày: <?php echo $model->date_from;?> đến ngày <?php echo $model->date_to;?></div>
    <div class="">
        <table id="freezetablecolumns_inventory" class="freezetablecolumns items">
            <thead>
                <tr  style="height: 60px;">                               
                    <th class="w-20 item_c ">STT</th>
                    <th class="w-150 ">PTTT - ngày</th>
                    <th class="w-60 ">Tổng cộng</th>
                    <?php foreach($data['ARR_DAYS'] as $date): ?>
                    <th class="w-30 item_c " style="">
                        <?php $aDate = explode('-', $date);
//                            echo $aDate[2]."-".$aDate[1];
                            echo $aDate[2];
                        ?>
                    </th>
                    <?php endforeach;?>
                </tr>
            </thead>
            <tbody>
                <?php foreach($SUM_EMPLOYEE as $monitoring_id=>$aEm):?>
                    <?php
//                    if( $cRole == ROLE_MONITORING_MARKET_DEVELOPMENT && $cUid != $monitoring_id){
//                        continue;// Mar 26, 2015 chỉ cho giám sát xem bc của chính họ,còn lại xem hết
//                    } // May 27, 2015 cho giám sát xem hết của giám sát khác
                    arsort($SUM_EMPLOYEE[$monitoring_id]);// hight to low. asort => low to high
                    if($SUM_MONITOR[$monitoring_id]==0) continue;?>
                    <tr>
                        <td>&nbsp;</td>
                        <td class="item_b"><?php echo $USER_MODEL[$monitoring_id];?></td>
                        <td class="item_r item_b"><?php echo ActiveRecord::formatCurrency($SUM_MONITOR[$monitoring_id]);?></td>
                        <td colspan="<?php echo $count_days;?>">&nbsp;</td>
                    </tr>
                <?php $index=1;?>
                <?php foreach($SUM_EMPLOYEE[$monitoring_id] as $maintain_employee_id=> $aDateQty):?>
                    <tr>
                        <td class="item_c"><?php echo $index++;?></td>
                        <td><?php echo $USER_MODEL[$maintain_employee_id];?></td>
                        <td class="item_r"><?php echo ActiveRecord::formatCurrency($SUM_EMPLOYEE[$monitoring_id][$maintain_employee_id]);?></td>
                        <?php foreach($data['ARR_DAYS'] as $date): ?>
                        <td class="item_c " style="">
                            <?php 
                                $qty_val = isset($EMPLOYEE[$monitoring_id][$maintain_employee_id][$date])?$EMPLOYEE[$monitoring_id][$maintain_employee_id][$date]:"";
                                echo $qty_val;
                            ?>
                        </td>
                        <?php endforeach;?>
                    </tr>
                <?php endforeach; // end fforeach($EMPLOYEE as $monitoring_id=>$aEm) ?>
                    
                <?php endforeach; // end fforeach($EMPLOYEE as $monitoring_id=>$aEm) ?>
            </tbody>
        </table>
    </div>
</div>