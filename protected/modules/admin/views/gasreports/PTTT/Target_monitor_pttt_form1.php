<div id="tabs_MONITOR_PTTT">
    <div class="clearfix">
        <div class="clr"></div>
        <div class="color_type_3 color_note"></div> Hộ Gia Đình
    </div>
    <div class="clr"></div>
    
        <?php foreach($AGENT_OF_MONITOR as $monitor_id => $aAgentId): ?>
        <?php
            $monitor_output = 0;
            foreach($aAgentId as $agent_id){
                $one = isset($OUTPUT_HGD[$agent_id])?$OUTPUT_HGD[$agent_id]:0;
                $monitor_output += $one;
            }
        ?>
        <div class="box_350">
            <table class="hm_table items">
                <tbody>
                    <tr>
                        <td colspan="3" class="item_c item_b"><?php echo $MONITOR_MODEL[$monitor_id];?></td>
                    </tr>
                    <tr>
                        <td class="item_c w-140">Target</td>
                        <td class="item_c w-140">Thực Tế</td>
                        <td class="item_c w-70">Tỷ Lệ</td>
                    </tr>
                    <!-- for hộ gia đình -->
                    <?php
                        $target=isset($data['aTargetEachMonth'][$monitor_id][STORE_CARD_KH_BINH_BO][$FOR_MONTH])?$data['aTargetEachMonth'][$monitor_id][STORE_CARD_KH_BINH_BO][$FOR_MONTH]:0;
                        $real = $monitor_output;
                        $amount = ActiveRecord::formatCurrency($real/12);
                        $percent = $real;
                        $target_qty = '';
                        if($target){
                            $percent = ($real/$target)*100;
                        }
                        if($target>0){
//                            $target_qty = " = ".ActiveRecord::formatCurrency($target/12);
                        }
                    ?>
                    <tr class="color_type_3">
                        <td class="item_r"><?php echo ActiveRecord::formatCurrency($target).$target_qty;?></td>
                        <!--<td class="item_r"><?php // echo ActiveRecord::formatCurrency($real)." = $amount";?></td>-->
                        <td class="item_r"><?php echo ActiveRecord::formatCurrency($real);?></td>
                        <td class="item_c"><?php echo round($percent,1);?> %</td>
                    </tr>
                    <!-- for hộ gia đình -->
                </tbody>
            </table>
        </div>
        <?php endforeach; // end foreach($aModelUser as $mUser)?>
        <div class="clr"></div>

    </div> <!-- END <div id="tabs_MONITOR_PTTT">-->

