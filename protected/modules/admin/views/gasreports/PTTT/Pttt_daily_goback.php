<?php
$this->breadcrumbs=array(
    'PTTT Bình Quay Về Hàng Ngày ',
);
?>

<h1>PTTT Bình Quay Về Hàng Ngày</h1>
<div class="search-form" style="">
    <div class="wide form">

    <?php $form=$this->beginWidget('CActiveForm', array(
            'action'=>Yii::app()->createUrl($this->route),
            'method'=>'post',
    )); ?>
        <div class="row more_col">
            <div class="col1">
                    <?php echo Yii::t('translation', $form->label($model,'date_from')); ?>
                    <?php 
                        $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                            'model'=>$model,        
                            'attribute'=>'date_from',
                            'options'=>array(
                                'showAnim'=>'fold',
                                'dateFormat'=> MyFormat::$dateFormatSearch,
    //                            'minDate'=> '0',
                                'maxDate'=> '0',
                                'changeMonth' => true,
                                'changeYear' => true,
                                'showOn' => 'button',
                                'buttonImage'=> Yii::app()->theme->baseUrl.'/admin/images/icon_calendar_r.gif',
                                'buttonImageOnly'=> true,                                
                            ),        
                            'htmlOptions'=>array(
                                'class'=>'w-16',
                                'size'=>'16',
                                'style'=>'float:left;',                               
                                'readonly'=>'1',
                            ),
                        ));
                    ?>     		
            </div>
            <div class="col2">
                    <?php echo Yii::t('translation', $form->label($model,'date_to')); ?>
                    <?php 
                        $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                            'model'=>$model,        
                            'attribute'=>'date_to',
                            'options'=>array(
                                'showAnim'=>'fold',
                                'dateFormat'=> MyFormat::$dateFormatSearch,
    //                            'minDate'=> '0',
                                'maxDate'=> '0',
                                'changeMonth' => true,
                                'changeYear' => true,
                                'showOn' => 'button',
                                'buttonImage'=> Yii::app()->theme->baseUrl.'/admin/images/icon_calendar_r.gif',
                                'buttonImageOnly'=> true,                                
                            ),        
                            'htmlOptions'=>array(
                                'class'=>'w-16',
                                'size'=>'16',
                                'style'=>'float:left;',
                                'readonly'=>'1',
                            ),
                        ));
                    ?>     		
            </div>
        </div>
    
        <div class="row">
                <?php echo Yii::t('translation', $form->label($model,'monitoring_id')); ?>
                <?php  echo $form->dropDownList($model,'monitoring_id', Users::getSelectByRoleNotRoleAgent(ROLE_MONITORING_MARKET_DEVELOPMENT),array('style'=>'width:350px;','empty'=>'Select')); ?>
        </div>

        <div class="row">
                <?php echo Yii::t('translation', $form->label($model,'ext_maintain_employee_id')); ?>
                <?php echo $form->dropDownList($model,'ext_maintain_employee_id', Users::getSelectByRoleForAgent($model->monitoring_id, ONE_MONITORING_MARKET_DEVELOPMENT, $model->agent_id, array('status'=>1)),array('class'=>'category_ajax', 'style'=>'width:350px;','empty'=>'Select')); ?>		
        </div>
        
        <div class="row">
            <?php echo Yii::t('translation', $form->label($model,'agent_id')); ?>
            <div class="fix-label">
                <?php
                   $this->widget('ext.multiselect.JMultiSelect',array(
                         'model'=>$model,
                         'attribute'=>'agent_id',
//                             'data'=>  Users::getSelectByRoleFinal(ROLE_AGENT, array('gender'=>Users::IS_AGENT)),
                         'data'=>  Users::getSelectByRoleFinal(ROLE_AGENT, array()),
                         // additional javascript options for the MultiSelect plugin
                        'options'=>array('selectedList' => 30,),
                         // additional style
                         'htmlOptions'=>array('style' => 'width: 345px;'),
                   ));    
               ?>
            </div>
            <?php echo $form->error($model,'agent_id'); ?>
        </div>

        <div class="row buttons" style="padding-left: 159px; margin:20px 0;">
            <?php $this->widget('bootstrap.widgets.TbButton', array(
                'buttonType'=>'submit',
                'label'=>Yii::t('translation','Xem Thống Kê'),
                'type'=>'null', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
                'size'=>'small', // null, 'large', 'small' or 'mini'
                //'htmlOptions' => array('style' => 'margin-bottom: 10px; float: right;'),
            )); ?>
        </div>

    <?php $this->endWidget(); ?>

    </div><!-- wide form -->
</div><!-- search-form -->

<?php // include_once 'View_store_movement_summary_print.php';?>

<style>
    .ui-datepicker-trigger { float: left;}
</style>

<script>
$(document).ready(function(){
    $('.form').find('button:submit').click(function(){        
        $.blockUI({ overlayCSS: { backgroundColor: '#fff' } }); 
    });   
});
</script>    
<?php include 'Pttt_daily_goback_form1.php'; ?>