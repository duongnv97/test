<div id="tabs-MONEY_BANK">
    <div class="grid-view ">

<?php
    $days_of_month = isset($MONEY_BANK['days'][$month])?$MONEY_BANK['days'][$month]:array();                
    $STT = 0;
?>
        <div class="title_table_statistic">TỔNG ĐẠI LÝ NỘP CÔNG TY THÁNG <?php echo $month;?> NĂM <?php echo $YEARS;?></div>
        <div class="">
            <table id="freezetablecolumns_MONEY_BANK" class="freezetablecolumns_money items freezetablecolumns_only_tr" style="">
                <thead>
                    <tr>                               
                        <th class="w-30 item_c">STT</th>
                        <th class="w-150">Tên đại lý</th>
                        <th class="w-100">Tổng</th>
                        <th class="w-80">BQ</th>
                        <?php foreach($days_of_month as $day): ?>
                        <th class="w-90" style="">
                            <?php echo $day;?>
                        </th>
                        <?php endforeach;?>
                    </tr>
                </thead>
                <tbody>
                    <?php if(isset($MONEY_BANK['sum_row_total_month'][$month])):?>
                        <?php foreach($MONEY_BANK['sum_row_total_month'][$month] as $agent_id=>$total_month_agent):?>
                        <?php $STT++;?>
                        <tr>
                            <td class=""><?php echo $STT; ?></td>
                            <td class=""><?php echo $AGENT_MODEL[$agent_id]->first_name; ?></td>
                            <td class="item_r item_b"><?php echo ActiveRecord::formatCurrency($total_month_agent);?></td>
                            <td class="item_r item_b"><?php $average = round($total_month_agent/count($days_of_month)); 
                                echo ActiveRecord::formatCurrency($average); ?>
                            </td>
                            <?php foreach($days_of_month as $day): ?>																	
                            <td class="item_r">
                                <?php 
                                    $output_day = isset($MONEY_BANK[$month][$day][$agent_id])?$MONEY_BANK[$month][$day][$agent_id]:"";
                                    echo $output_day>0?ActiveRecord::formatCurrency($output_day):'';
                                ?>
                            </td>
                            <?php endforeach; // end <?php foreach($days_of_month as $day) ?>
                        </tr>
                        <?php ?>
                        <?php endforeach; // end foreach($MONEY_BANK['sum_row_total_month'][$month]  ?>
                        <!-- tổng cộng -->
                        <tr>
                            <td class="">&nbsp;</td>
                            <td class="item_b">Tổng Cộng</td>
                            <td class="item_r item_b">
                                <?php $sum_col_total_month_all_agent = $MONEY_BANK['sum_col_total_month_all_agent'][$month];
                                echo ActiveRecord::formatCurrency($sum_col_total_month_all_agent);
                                ?>
                            </td>
                            <td class="item_r item_b">
                                <?php 
                                    $average = round($sum_col_total_month_all_agent/count($days_of_month));
                                echo ActiveRecord::formatCurrency($average); ?>
                            </td>
                            <?php foreach($days_of_month as $day): ?>																	
                            <td class="item_r item_b">
                                <?php 
                                    $sum_day = isset($MONEY_BANK['sum_col'][$month][$day])?$MONEY_BANK['sum_col'][$month][$day]:"";
                                    echo $sum_day>0?ActiveRecord::formatCurrency($sum_day):'';
                                ?>
                            </td>
                            <?php endforeach; // end <?php foreach($days_of_month as $day) ?>                        
                        </tr>
                        <!-- tổng cộng -->
                    <?php endif;?>
                </tbody>
            </table><!-- end <table class="table_statistic items table_mon-->
        </div> <!-- end <div class="grid-view-scroll">-->

</div> <!-- end <div class="grid-view grid-view-scroll">-->
</div> <!-- end <div id="tabs-MONEY_BANK">-->
            
         