<?php 
$percent = $monitor_real_sum_kg;
if($monitor_target){
    $percent = ($monitor_real_sum_kg/$monitor_target)*100;
}

?>
<div class="box_310 one_province_hide display_none">
<!--<div class="box_310 one_province_hide ">-->
    <table class="hm_table items">
        <tbody>
            <tr class="color_type_2">
                <td colspan="3" class="item_c item_b"><?php echo $USER_FULL_NAME[$monitor_id];?></td>
            </tr>
            <tr>
                <td class="item_c w-140">Target</td>
                <td class="item_c w-140">Thực Tế</td>
                <td class="item_c w-70">Tỷ Lệ</td>
            </tr>
            <tr>
                <td class="item_c"><?php echo ActiveRecord::formatCurrency($monitor_target);?></td>
                <td class="">
                    Bò: <?php echo ActiveRecord::formatCurrencyRound($monitor_real_bo);?><br>
                    <!--Mối: <?php // echo ActiveRecord::formatCurrency($monitor_output_moi)." = $monitor_output_moi_qty";?><br>-->
                    Mối: <?php echo ActiveRecord::formatCurrency($monitor_output_moi);?><br>
                    Dân Dụng: <?php echo ActiveRecord::formatCurrencyRound($monitor_pttt_all_kg)." = $monitor_pttt_all";?><br>
                    <b>Tổng: <?php echo ActiveRecord::formatCurrencyRound($sumAllOne);?></b>
                    
                </td>
                <td class="item_c"><?php echo ActiveRecord::formatCurrency($percent);?> %</td>
            </tr>
        </tbody>
    </table>
</div>
<div class="clr"></div>