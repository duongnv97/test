<div class="display_none">
<?php
    $cmsFormater = new CmsFormatter();
    $dataTarget=array();
    $FOR_MONTH = $model->statistic_month;
    $FOR_YEAR = $model->statistic_year;
    $needMore=array('target_array_agent' => $aAgentId);
    
    $dataTarget = Statistic::Target($model, $needMore);

    $AGENT_MODEL= isset($dataTarget['ROLE_AGENT_MODEL'])?$dataTarget['ROLE_AGENT_MODEL']:array();
//    $mOneAgent = Users::model()->findByPk($agent_id);
?>
    <div class="clearfix">
        <div class="color_type_1 color_note"></div> &nbsp;&nbsp;&nbsp;Gas Bình Bò
        <div class="clr"></div>
        <div class="color_type_2 color_note"></div> &nbsp;&nbsp;&nbsp;Gas Bình Mối
        <div class="clr"></div>
        <div class="color_type_3 color_note"></div> &nbsp;&nbsp;&nbsp; Hộ Gia Đình
    </div>
    <div class="clr"></div>
    
    <?php foreach($AGENT_MODEL as $province_id=>$aModelUser): ?>
        <?php foreach($aModelUser as $mOneAgent): ?>
            <?php if( in_array($mOneAgent->id, $aAgentId)): ?>
            <div class="box_310 target_agent_hide_<?php echo $mOneAgent->id;?>">
                <table class="hm_table items">
                    <tbody>
                        <tr>
                            <td colspan="3" class="item_c item_b"><?php echo $mOneAgent->first_name;?></td>
                        </tr>
                        <tr>
                            <td class="item_c w-100">Target</td>
                            <td class="item_c w-100">Thực Tế</td>
                            <td class="item_c w-100">Tỷ Lệ</td>
                        </tr>
                        <?php $sumC1=0;$sumC2=0; // Dec 19, 2015 for sum col ?>
                        <?php foreach(CmsFormatter::$CUSTOMER_BO_MOI as $type=>$label):?>
                        <?php
                            if($type == STORE_CARD_XE_RAO){
                                continue;
                            }
                            $uid = $mOneAgent->id;
                            $target=isset($dataTarget['aTargetEachMonth'][$uid][$type][$FOR_MONTH])?$dataTarget['aTargetEachMonth'][$uid][$type][$FOR_MONTH]:0;
                            $OUTPUT=isset($dataTarget['OUTPUT'][$uid][$type])?$dataTarget['OUTPUT'][$uid][$type]:0;
                            $REMAIN=isset($dataTarget['REMAIN'][$uid][$type])?$dataTarget['REMAIN'][$uid][$type]:0;
                            $real = $OUTPUT-$REMAIN;
                            
                            // May 16, 2014 xử lý change cộng sản lượng bình bỏ của KH mối vào sản lương bình bò của dại lý
                            if($type==STORE_CARD_KH_MOI){
                                // chỗ này xử lý trừ đi bình bò của KH mối, sẽ cộng lên cho sản lượng của KH bình bò của đại lý
                                $OUTPUT_BINH_BO=isset($dataTarget['BINH_BO_KH_MOI'][$uid][$type])?$dataTarget['BINH_BO_KH_MOI'][$uid][$type]:0;
                                $real -=  $OUTPUT_BINH_BO ; // trừ sản lượng bình bò
                                $real +=  $REMAIN; // cộng ngược lại gas dư, vì sẽ không trừ đi gas dư ở đây
                                /* Aug 05, 2014 xử lý trừ sản lượng của sale chuyên viên và sale pttt*/
//                                $OUTPUT_KH_MOI_SALE_CV_PTTT=isset($dataTarget['KH_MOI_SALE_CV_PTTT'][$uid][$type])?$dataTarget['KH_MOI_SALE_CV_PTTT'][$uid][$type]:0;
//                                $real -=  $OUTPUT_KH_MOI_SALE_CV_PTTT ; // trừ sản lượng bình mối của sale chuyên viên vs pttt
                                // Now 20, 2014 Kiên kêu đóng lại, vì xem thấy nó lệch với bên daily sản lượng
                                /* Aug 05, 2014 xử lý trừ sản lượng của sale chuyên viên và sale pttt*/
                            }
                            if($type==STORE_CARD_KH_BINH_BO){
                                // chỗ này xử lý cộng vào sản lượng bình bò của KH mối, sẽ cộng lên cho sản lượng của KH bình bò của đại lý
                                $OUTPUT_BINH_BO=isset($dataTarget['BINH_BO_KH_MOI'][$uid][STORE_CARD_KH_MOI])?$dataTarget['BINH_BO_KH_MOI'][$uid][STORE_CARD_KH_MOI]:0;
                                // trừ đi lượng gas dư của KH mối mà bán bình bò
                                $REMAIN_KH_MOI=isset($dataTarget['REMAIN'][$uid][STORE_CARD_KH_MOI])?$dataTarget['REMAIN'][$uid][STORE_CARD_KH_MOI]:0;
                                $real += ( $OUTPUT_BINH_BO - $REMAIN_KH_MOI) ;
                            }
                            // May 16, 2014 xử lý change cộng sản lượng bình bỏ của KH mối vào sản lương bình bò của dại lý
                            
                            $qty = '';
                            $target_qty = '';
                            if($type==STORE_CARD_KH_MOI && $real){                                    
                                $qty = " = ".$real/12;
                            }
                            if($type==STORE_CARD_KH_MOI && $target>0){
                                $target_qty = " = ".$target/12;
                            }

                            $percent = $real;
                            if($target){
                                $percent = ($real/$target)*100;
                            }
                            $sumC1 += $target;
                            $sumC2 += $real;
                        ?>

                        <tr class="color_type_<?php echo $type;?>">
                            <td class="item_r"><?php echo ActiveRecord::formatCurrency($target).$target_qty;?></td>
                            <td class="item_r"><?php echo ActiveRecord::formatCurrency(round($real))."$qty";?></td>
                            <td class="item_c"><?php echo round($percent,1);?> %</td>
                        </tr>
                        <?php endforeach;?>
                        <!-- for hộ gia đình -->
                        <?php
                            $uid = $mOneAgent->id;
                            $target=isset($dataTarget['TARGET_HO_GIA_DINH'][$uid][$FOR_MONTH])?$dataTarget['TARGET_HO_GIA_DINH'][$uid][$FOR_MONTH]:0;
                            $real=isset($dataTarget[GasTargetMonthly::HGD_REAL][$uid])?$dataTarget[GasTargetMonthly::HGD_REAL][$uid]:0;
                            $amount = $real/12;
                            $percent = $real;
                            $target_qty = '';
                            if($target){
                                $percent = ($real/$target)*100;
                            }
                            if($target>0){
                                $target_qty = " = ".$target/12;
                            }
                            $sumC1 += $target;
                            $sumC2 += $real;
                            $percent_row_sum = $sumC2;
                            if($sumC1){
                                $percent_row_sum = ($sumC2/$sumC1)*100;
                            }
                            
                        ?>
                        <tr class="color_type_3">
                            <td class="item_r"><?php echo ActiveRecord::formatCurrency($target).$target_qty;?></td>
                            <td class="item_r"><?php echo ActiveRecord::formatCurrency($real)." = $amount";?></td>
                            <td class="item_c"><?php echo round($percent,1);?> %</td>
                        </tr>
                        <!-- for hộ gia đình -->
                        <!-- for sum col -->
                        <tr class="color_type_4">
                            <td class="item_r item_b"><?php echo ActiveRecord::formatCurrency($sumC1);?></td>
                            <td class="item_r item_b"><?php echo ActiveRecord::formatCurrency(round($sumC2));?></td>
                            <td class="item_c item_b"><?php echo round($percent_row_sum,1);?> %</td>
                        </tr>
                        
                    </tbody>
                </table>
            </div>
        <?php endif; // end if($mOneAgent->id == $agent_id ?>
        <?php endforeach; // end foreach($aModelUser as $mUser)?>
    <div class="clr"></div>
    <?php endforeach; // end <?php foreach($AGENT_MODEL as $province_id=>$aModelUser): ?>

</div>    
    