<?php if(isset($data['ROLE_SALE_MODEL']) ||  isset($data['ROLE_AGENT_MODEL'])) :?>
<?php Yii::app()->clientScript->registerCoreScript('jquery.ui'); ?>
<?php
$gridCss = Yii::app()->getAssetManager()->publish(Yii::getPathOfAlias('zii.widgets.assets')).'/gridview/styles.css';
Yii::app()->getClientScript()->registerCssFile($gridCss);
?>
<script>
$(function() {
//    $( "#tabs" ).tabs({ active: 2 });
    $( "#tabs" ).tabs();
});

$(window).load(function(){
    fnAddClassOddEven('items');
    $('.GRID_SALE_MODEL_MOI').show();
//    $(".items > tbody > tr:odd").addClass("odd");
//    $(".items > tbody > tr:even").addClass("even");   
});

</script>

<?php
$SALE_MODEL_BO= isset($data['ROLE_SALE_MODEL'])?$data['ROLE_SALE_MODEL']:array();
$SALE_MODEL_MOI= isset($data['ROLE_SALE_MODEL_MOI'])?$data['ROLE_SALE_MODEL_MOI']:array();
$SALE_MODEL_MOI_CHUYEN_VIEN= isset($data['ROLE_SALE_MODEL_MOI_CHUYEN_VIEN'])?$data['ROLE_SALE_MODEL_MOI_CHUYEN_VIEN']:array();
$SALE_MOI_SORT_TOTAL= isset($data['SALE_MOI_SORT_TOTAL'])?$data['SALE_MOI_SORT_TOTAL']:array();
$AGENT_MODEL= isset($data['ROLE_AGENT_MODEL'])?$data['ROLE_AGENT_MODEL']:array();
$PROVINCE_MODEL = GasProvince::getArrModel();
$cmsFormater = new CmsFormatter();
$FOR_MONTH = $model->statistic_month;
$FOR_YEAR = $model->statistic_year;
$textInfo = "Thống kê target ";
if(!empty($model->date_delivery)){
    $tmpDate = explode('-', $model->date_delivery);
    if(count($tmpDate)!=3){
        $tmpDate[0] = date('Y');
        $tmpDate[1] = date('m');
    }    
    $textInfo.="từ ngày 01-$tmpDate[1] đến ngày $model->date_delivery";
}else{
    $textInfo.="tháng $model->statistic_month năm $model->statistic_year";
}

$can_view_sale_bo = true;
$can_view_agent = true;
$can_view_sale_moi = true;
$can_view_sale_moi_chuyen_vien = true;
$ID_AGENT_LIMIT = array();
$IS_ROLE_AGENT_LIMIT = false;
$cRole = Yii::app()->user->role_id;
$cUid = Yii::app()->user->id;

if($cRole==ROLE_SUB_USER_AGENT){
    $can_view_sale_bo = false;
    $can_view_agent = false;
    $can_view_sale_moi_chuyen_vien = false;
}

if(in_array($cRole, GasCheck::$ARR_ROLE_LIMIT_AGENT)){
    // nếu là role bị giới hạn đại lý theo dõi thì check , hiện tại là role kế toàn khu vực + kế toán đại lý bị giới hạn
    $can_view_sale_bo = false;
    $IS_ROLE_AGENT_LIMIT = true;
    GasAgentCustomer::initSessionAgent();// khởi tạo session cho role limit agent 
}


if ($cRole==ROLE_SALE) { // Apr 14, 2015 - fix sale bo thay target bo, sale moi thay target moi
    $can_view_sale_bo = false;
    $can_view_agent = false;
    $can_view_sale_moi_chuyen_vien = false;
    $TypeSale = Yii::app()->user->gender;
    if( $TypeSale== Users::SALE_BO ){
        $can_view_sale_bo = true;
        $can_view_agent = false;
        $can_view_sale_moi_chuyen_vien = false;
    }elseif($TypeSale== Users::SALE_MOI_CHUYEN_VIEN){
        $can_view_sale_bo = false;
        $can_view_agent = false;
        $can_view_sale_moi_chuyen_vien = true;
    }
}

$session=Yii::app()->session;
if(isset($session['LIST_AGENT_OF_USER']) && count($session['LIST_AGENT_OF_USER'])){
    $ID_AGENT_LIMIT = $session['LIST_AGENT_OF_USER'];
}
if(is_array($ID_AGENT_LIMIT) && count($ID_AGENT_LIMIT) == 0){
    $IS_ROLE_AGENT_LIMIT = false;
}

// Now 21, 2014 tạm ẩn đi tab Sale Mối
$can_view_sale_moi = false;
// Now 21, 2014 tạm ẩn đi tab Sale Mối

?>

<div id="tabs" class="grid-view">
    <ul>
        <?php if($can_view_sale_bo): ?>
        <li>
            <a class="tab_month" href="#tabs-SALE_MODEL">Sale Bò</a>
        </li>
        <?php endif;?>
        <?php if(count($AGENT_MODEL) && $can_view_agent): ?>
        <li>
            <a class="tab_month" href="#tabs-AGENT_MODEL">Đại Lý</a>
        </li>
        <?php endif;?>
        
        <?php if($can_view_sale_moi): ?>
        <li>
            <a class="tab_month" month_current="sale_moi_tb" href="#tabs-SALE_MODEL_MOI">Sale Mối</a>
        </li>
        <?php endif;?>
        <?php if($can_view_sale_moi_chuyen_vien): ?>
        <li>
            <a class="tab_month" href="#SALE_MODEL_MOI_CHUYEN_VIEN">Sale Mối Chuyên Viên</a>
        </li>
        <?php endif;?>
    </ul>
    
    <?php if(count($SALE_MODEL_BO) && $can_view_sale_bo): ?>
        <?php include 'Target_form_bo.php';?>
    <?php endif; // end if(count($SALE_MODEL)):  ?>
    
    <?php if(count($AGENT_MODEL) && $can_view_agent): ?>
        <?php include 'Target_form_agent.php';?>
    <?php endif;?>
    
    <?php if($can_view_sale_moi): ?>
        <?php include 'Target_form_moi.php';?>
    <?php endif;?>
    
    <?php if($can_view_sale_moi_chuyen_vien): ?>
        <?php include 'Target_form_moi_chuyen_vien.php';?>
    <?php endif;?>
    
    
</div>

<?php endif; // end if(count($data)) ?>

<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/scroll/jquery.freezetablecolumns.1.1.js"></script> 

<script>
    $(function(){
        // for tab 1
        $('.cty_target_bo').text($('.hide_cty_target_bo').val());
        $('.cty_thuc_te_bo').text($('.hide_cty_thuc_te_bo').val());
        $('.cty_bo_percent').text($('.hide_cty_bo_percent').val());
        $('.cty_remain').text($('.hide_cty_remain').val());
        // for tab 1
        
        // for tab 4
        $('.cty_target_binh_moi_chuyen_vien').text($('.hide_cty_target_binh_moi_chuyen_vien').val());
        $('.cty_thuc_te_binh_moi_chuyen_vien').text($('.hide_cty_thuc_te_binh_moi_chuyen_vien').val());
        $('.cty_binh_moi_chuyen_vien_percent').text($('.hide_cty_binh_moi_chuyen_vien_percent').val());
        $('.cty_thuc_te_binh_bo_chuyen_vien').text($('.hide_cty_thuc_te_binh_bo_chuyen_vien').val());
        // for tab 4
        
        // for one province
        $('.one_province_hide').each(function(){
           var parent_div = $(this).closest('div.wrap_province');
           parent_div.find('.one_province').html($(this).html());
        });
        // for total cty
        $('.sum_target_hm').html($('.sum_target_hm_hide').html());
        $('#freezetablecolumns').freezeTableColumns({
                width:       <?php echo Users::WIDTH_freezetable;?>,   // required - bt 1100
                height:      400,   // required
                numFrozen:   2,     // optional
                frozenWidth: 210,   // optional
                clearWidths: true  // optional
              });   
        });
    
</script>
