<?php if(isset($data['OUTPUT'])) :?>
<?php Yii::app()->clientScript->registerCoreScript('jquery.ui'); ?>
<?php
$gridCss = Yii::app()->getAssetManager()->publish(Yii::getPathOfAlias('zii.widgets.assets')).'/gridview/styles.css';
Yii::app()->getClientScript()->registerCssFile($gridCss);
?>
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/scroll/jquery.freezetablecolumns.1.1.js"></script> 
<?php 
$OUTPUT             = isset($data['OUTPUT'])?$data['OUTPUT'] : [];
$SUM_MONTH          = isset($data['SUM_MONTH'])?$data['SUM_MONTH'] : [];
$SUM_ALL            = isset($data['SUM_ALL'])?$data['SUM_ALL'] : 0;
$OUTPUT_SUM         = $data['OUTPUT_SUM'];
$OUTPUT_MONTH       = $data['OUTPUT_MONTH'];
$AGENT_MODEL        = $data['ROLE_AGENT_MODEL'];
$PROVINCE_ID_FOR    = $data['PROVINCE_ID_FOR'];
$PROVINCE_MODEL     = GasProvince::getArrModel();
$month_first        = reset($OUTPUT_MONTH);
$month_last         = end($OUTPUT_MONTH);
$aSumMonthChange    = [];
$sumMonthChange = 0;

?>

<?php include 'Output_change_form_tab.php';?>

<script>
$(function() {
    $( "#tabs" ).tabs();
//    fnUpdateSumRow();
    fnTabMonthClick();
});    
    
function fnTabMonthClick(){
    $('.freezetablecolumns').each(function(){
        var id_table = $(this).attr('id');
        $('#'+id_table).freezeTableColumns({
                width:       <?php echo Users::WIDTH_freezetable;?>,   // required - bt 1100
                height:      400,   // required
                numFrozen:   5,     // optional
                frozenWidth: 433,   // optional
                clearWidths: true  // optional
              });   
    });
}  

$(window).load(function(){
    var index = 1;
    $('.freezetablecolumns').each(function(){
        if(index==1)
            $(this).closest('div.grid-view').show();
        index++;
    });    
    fnAddClassOddEven('items');
});
</script>

<?php endif; ?>

