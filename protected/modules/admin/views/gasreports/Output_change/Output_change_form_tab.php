<div class="grid-view display_none">
    <div class="title_table_statistic">BÁO CÁO SẢN LƯỢNG <?php echo GasStoreCard::$TYPE_OUTPUT_CHANGE[$model->type_user];?> TĂNG GIẢM THEO THÁNG: <?php echo $model->statistic_year;?></div>
    <div class="">
        <table id="freezetablecolumns_inventory" class="freezetablecolumns items">
            <thead>
                <tr class="h_50">
                    <th class="w-20 item_c ">STT</th>
                    <th class="w-150 ">Đại Lý</th>
                    <th class="w-70 ">Tổng Cộng</th>
                    <th class="w-70 ">Bình Quân</th>
                    <th class="w-70 ">Chênh Lệch</th>
                    <?php $index=1; $index_month=0; $CountMonth = count($OUTPUT_MONTH); ?>
                    <?php foreach($OUTPUT_MONTH as $month): ?>
                    <th class="w-40 item_c" style="">
                        <?php echo $month; $index_month++;?>
                    </th>
                    <?php if($index_month != $CountMonth): ?>
                    <th class="w-40 item_c" ></th>
                    <?php endif;?>
                    <?php endforeach;?>
                </tr>
            </thead>
            <tbody>
                <?php foreach($PROVINCE_ID_FOR as $key_province_id=>$province_id): ?>
                    <?php // foreach($aModelUser as $mUser): ?>
                    <?php foreach($OUTPUT_SUM[$province_id] as $agent_id => $sum_qty): ?>
                    <?php 
                        $sum_agent = $sum_qty;
                    ?>
                    <tr>
                        <td><?php echo $index++;?></td>
                        <td><?php echo $AGENT_MODEL[$agent_id]->first_name;?></td>
                        <td class="item_r item_b"><?php echo $sum_agent!=0?ActiveRecord::formatCurrency($sum_agent):'';?></td>
                        <td class="item_r item_b"><?php echo $sum_agent!=0?ActiveRecord::formatCurrency( round($sum_agent/$CountMonth)):'';?></td>
                        <?php
                            $month_first_value      = isset($OUTPUT[$agent_id][$model->statistic_year][$month_first])?$OUTPUT[$agent_id][$model->statistic_year][$month_first]:0;
                            $month_last_value       = isset($OUTPUT[$agent_id][$model->statistic_year][$month_last])?$OUTPUT[$agent_id][$model->statistic_year][$month_last]:0;
                            $diff_value             = $month_last_value - $month_first_value;
                            $sumMonthChange += $diff_value;
                        ?>
                        <td class="item_r item_b"><?php echo $diff_value != 0 ? ActiveRecord::formatCurrency($diff_value):'&nbsp;';?></td>
                        
                        <?php $index_month=0;?>
                        <?php foreach($OUTPUT_MONTH as $month): ?>
                        <?php 
                            $monthValue = isset($OUTPUT[$agent_id][$model->statistic_year][$month])?$OUTPUT[$agent_id][$model->statistic_year][$month]:0;
                        ?>
                        <td class="item_c" style="">
                            <?php echo $monthValue !=0 ? ActiveRecord::formatCurrency($monthValue):'&nbsp;'; $index_month++;?>
                        </td>
                        <?php if($index_month != $CountMonth): ?>
                        <?php 
                            $change_value = 0;
                            if(isset($OUTPUT_MONTH[$month+1])){
                                $next_month = $OUTPUT_MONTH[$month+1];
                                $next_month_value = isset($OUTPUT[$agent_id][$model->statistic_year][$next_month])?$OUTPUT[$agent_id][$model->statistic_year][$next_month]:0;
                                $change_value = $next_month_value - $monthValue;
                            }
                            if(!isset($aSumMonthChange[$month])){
                                $aSumMonthChange[$month] = $change_value;
                            }else{
                                $aSumMonthChange[$month] += $change_value;
                            }
                        ?>
                        <td class="item_c">
                            <?php echo $change_value!=0?ActiveRecord::formatCurrency($change_value):'&nbsp;';?>
                        </td>
                        <?php endif;?>
                        <?php endforeach;?>
                    </tr>
                    <?php endforeach; // end foreach($aModelUser as $mUser)?>
                <?php endforeach; // end <?php foreach($AGENT_MODEL as $province_id=>$aModelUser): ?>
            
            <tr>
                <td></td>
                <td class="item_r item_b">Tổng cộng</td>
                <td class="item_r item_b"><?php echo ActiveRecord::formatCurrency($SUM_ALL); ?></td>
                <td></td>
                <td class="item_r item_b"><?php echo ActiveRecord::formatCurrency($sumMonthChange); ?></td>
                <?php foreach($OUTPUT_MONTH as $month): ?>
                    <?php 
                        $monthValue = isset($SUM_MONTH[$month]) ? $SUM_MONTH[$month] : 0;
                        $monthChange = isset($aSumMonthChange[$month]) ? $aSumMonthChange[$month] : 0;
                    ?>
                    <td class="item_c item_b"><?php echo ActiveRecord::formatCurrency($monthValue); ?></td>
                    <td class="item_c item_b"><?php echo ActiveRecord::formatCurrency($monthChange); ?></td>
                <?php endforeach;?>
            </tr>
            </tbody>
        </table>
    </div>
</div>

<script>
    $(function(){
    });
</script>