<?php
$title = "Báo cáo sản lượng tăng giảm theo tháng";
$this->breadcrumbs=array(
	$title,
);?>

<h1><?php echo "Hôm Nay: ".MyFormat::$TheDaysOfTheWeek[date('l')].' '.date('d-m-Y'). " - $title"; ?></h1>
<div class="search-form" style="">
    <div class="wide form">
    <?php $form=$this->beginWidget('CActiveForm', array(
            'action'=>Yii::app()->createUrl($this->route),
            'method'=>'post',
    )); ?>
            <?php if(Yii::app()->user->hasFlash('successUpdate')): ?>
                <div class="success_div"><?php echo Yii::app()->user->getFlash('successUpdate');?></div>         
            <?php endif; ?>    
    <div class="row">
        <?php echo $form->labelEx($model,'province_id_agent'); ?>
        <div class="fix-label">
            <?php
               $this->widget('ext.multiselect.JMultiSelect',array(
                     'model'=>$model,
                     'attribute'=>'province_id_agent',
                     'data'=> GasProvince::getArrAll(),
                     // additional javascript options for the MultiSelect plugin
                    'options'=>array('selectedList' => 30,),
                     // additional style
                     'htmlOptions'=>array('style' => 'width: 800px;'),
               ));    
           ?>
        </div>
    </div>

    <div class="row more_col">
        <div class="col2">
            <?php echo  $form->label($model,'type_user', array('label'=>'Loại')); ?>
            <?php echo $form->dropDownList($model,'type_user', GasStoreCard::$TYPE_OUTPUT_CHANGE, array('class'=>'w-150')); ?>
        </div>

        <div class="col3">
            <?php echo Yii::t('translation', $form->labelEx($model,'statistic_year')); ?>
            <?php echo $form->dropDownList($model,'statistic_year', ActiveRecord::getRangeYear(), array('class'=>'w-150')); ?>
        </div>
    </div>

    <div class="row buttons" style="padding-left: 159px; margin:20px 0;">
        <?php $this->widget('bootstrap.widgets.TbButton', array(
        'buttonType'=>'submit',
        'label'=>Yii::t('translation','Xem Thống Kê'),
        'type'=>'null', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
        'size'=>'small', // null, 'large', 'small' or 'mini'
        //'htmlOptions' => array('style' => 'margin-bottom: 10px; float: right;'),
    )); ?>	
    <?php // if(ControllerActionsName::isAccessAction('statistic_maintain_export_excel', $actions)):?>
    <?php if(0):?>
            <input class='statistic_maintain_export_excel' next='<?php echo Yii::app()->createAbsoluteUrl('admin/gasmaintain/statistic_maintain_export_excel');?>' type='button' value='Xuất Thống Kê Hiện Tại Ra Excel'>
    <?php endif;?>

    </div>

    <?php $this->endWidget(); ?>

    </div><!-- wide form -->
</div><!-- search-form -->


<style>
    .ui-datepicker-trigger { float: left;}
</style>

<script>
$(document).ready(function(){
    $('.form').find('button:submit').click(function(){        
        $.blockUI({ overlayCSS: { backgroundColor: '#fff' } }); 
    });   
});
</script>    
<?php include 'Output_change_form.php'; ?>