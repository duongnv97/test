<?php
/** @var GasSettle $model */
$canUpdate  = false;
$cRole      = MyFormat::getCurrentRoleId();
$cUid       = MyFormat::getCurrentUid();

$currentStatus  = $model->status;
$step           = 0;
$scenario       = 'view';
$fieldComment   = "comment_leader";
if ( ($model->uid_director == $cUid || $cUid == GasLeave::UID_DIRECTOR)
        && in_array($currentStatus, GasSettle::$LIST_STATUS_CAN_APPROVE_BY_DIRECTOR)
        && $model->cashier_confirm != GasSettle::CASHIER_CONFIRM_YES
) {
    // Is step of director
    $aStatusList = GasSettle::$STATUS_UPDATE_BY_DIRECTOR;
    $step = 3;
    $canUpdate = true;
    $scenario = 'update_from_manager';
    $fieldComment = "comment_director";
} elseif ($model->uid_chief_accountant == $cUid && in_array($currentStatus, GasSettle::$LIST_STATUS_CAN_APPROVE_BY_CHIEF_ACCOUNTANT)
) {
    // Is step of director
    $aStatusList = GasSettle::$STATUS_UPDATE_BY_CHIEF_ACCOUNTANT;
    $step = 2;
    $canUpdate = true;
    $scenario = 'update_from_manager';
    $fieldComment = "comment_chief_accountant";
} elseif ($model->uid_leader == $cUid && in_array($currentStatus, GasSettle::$LIST_STATUS_CAN_APPROVE_BY_MANAGER)
) {
    // Is step of manager
    $canUpdate = true;
    $aStatusList = GasSettle::$STATUS_UPDATE_BY_MANAGER;
    $step = 1;
    $scenario = 'update_from_manager';
    $fieldComment = "comment_leader";
}
$model->scenario = $scenario;
?>

<?php if ($canUpdate): ?>

    <div class="form container">
        <?php $form = $this->beginWidget('CActiveForm', array(
            'id' => 'gas-settle-form',
            'action' => Yii::app()->createAbsoluteUrl('admin/gasSettle/view', array('id' => $model->id)),
            'enableClientValidation' => true,
            'clientOptions' => array(
                'validateOnSubmit' => true,
            ),
        ));
        ?>
        <?php if (Yii::app()->user->hasFlash('successUpdate')): ?>
            <div class='success_div'><?php echo Yii::app()->user->getFlash('successUpdate'); ?></div>
        <?php endif; ?>
        <div class="row">
            <?php echo $form->labelEx($model, 'step_status'); ?>
            <?php echo $form->dropDownList($model, 'step_status', $aStatusList, array('style' => 'width:376px;', 'empty' => 'Select',
                'onChange' => 'onStepStatusChange(this)')); ?>
            <?php echo $form->error($model, 'step_status'); ?>
        </div>

        <div class="row">
            <?php echo $form->labelEx($model, $fieldComment); ?>
            <?php echo $form->textArea($model, $fieldComment, array('rows' => 5, 'class' => 'w-600', "placeholder" => "")); ?>
            <?php echo $form->error($model, $fieldComment); ?>
        </div>
            
        <?php if ($step == 1) { ?>
            <div class="row superior">
                <?php echo $form->labelEx($model, 'uid_chief_accountant'); ?>
                <?php echo $form->dropDownList($model, 'uid_chief_accountant', $model->getListChiefAccountant(), array('class' => 'w-400', 'empty' => 'Select')); ?>
                <?php echo $form->error($model, 'uid_chief_accountant'); ?>
            </div>
        <?php } elseif ($step == 2) { ?>
            <div class="row superior">
                <?php echo $form->labelEx($model, 'uid_director'); ?>
                <?php echo $form->dropDownList($model, 'uid_director', $model->getListDirector(), array('class' => 'w-400', 'empty' => 'Select')); ?>
                <?php echo $form->error($model, 'uid_director'); ?>
            </div>
            
            <div class="row superior">
                <?php echo $form->labelEx($model, 'uid_cashier_confirm'); ?>
                <?php echo $form->dropDownList($model, 'uid_cashier_confirm', $model->getCashierConfirm(), array('class' => 'w-400', 'empty' => 'Select')); ?>
                <?php echo $form->error($model, 'uid_cashier_confirm'); ?>
            </div>
            
            <div class="row superior">
                <?php echo $form->labelEx($model, 'uid_accounting'); ?>
                <?php echo $form->dropDownList($model, 'uid_accounting', $model->getAccounting(), array('class' => 'w-400', 'empty' => 'Select')); ?>
                <?php echo $form->error($model, 'uid_accounting'); ?>
            </div>
        <?php } elseif ($step == 3) { ?>
        <?php } elseif ($step == 4) { ?>
        <?php } ?>

        <div class="row buttons" style="padding-left: 141px;">
            <?php $this->widget('bootstrap.widgets.TbButton', array(
                'buttonType' => 'submit',
                'label' => $model->isNewRecord ? 'Create' : 'Save',
                'type' => 'null', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
                'size' => 'small', // null, 'large', 'small' or 'mini'
                //'htmlOptions' => array('style' => 'margin-bottom: 10px; float: right;'),
            )); ?>
            <input class='cancel_iframe' type='button' value='Cancel'>
        </div>

        <?php $this->endWidget(); ?>
    </div>
    <script>
        function onStepStatusChange($object) {
            $object = $($object);
            $value = $object.val();
            $superior = $object.closest('.form').find('.superior');
            $dropdown = $superior.find('select');
            if ($value == <?php echo GasSettle::UPDATE_TYPE_REJECT; ?> || $value == <?php echo GasSettle::UPDATE_TYPE_NEED_UPDATE; ?>) {
                $superior.hide();
                $dropdown.val('');
            } else {
                $superior.show();
            }
        }
    </script>
<?php endif; ?>