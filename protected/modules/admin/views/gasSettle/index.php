<?php
$this->breadcrumbs = array(
    $this->singleTitle,
);

$menus = array(
    array('label' => "Create $this->singleTitle", 'url' => array('create')),
);
$aUidAllow = [GasConst::UID_ADMIN, GasConst::UID_NHAN_NTT, GasLeave::PHUONG_PTK];
$cUid = MyFormat::getCurrentUid();
if(in_array($cUid, $aUidAllow)):
    $menus[] = ['label'=> 'Loại : Đề nghị chuyển khoản hợp đồng thuê nhà',
                'url'=>array('index', 'ExportExcel'=>1), 
                'htmlOptions'=>array('class'=>'export_excel ','label'=>'Xuất Excel'),
    ];
endif;
$this->menu = ControllerActionsName::createMenusRoles($menus, $actions);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('gas-settle-grid', {
                url : $(this).attr('action'),
		data: $(this).serialize()
	});
	return false;
});
");

Yii::app()->clientScript->registerScript('ajaxupdate', "
$('#gas-settle-grid a.ajaxupdate').live('click', function() {
    $.fn.yiiGridView.update('gas-settle-grid', {
        type: 'POST',
        url: $(this).attr('href'),
        success: function() {
            $.fn.yiiGridView.update('gas-settle-grid');
        }
    });
    return false;
});
");
?>

<h1><?php echo $headTitle; ?></h1>
<?php include "index_button.php"; ?>
<?php echo MyFormat::BindNotifyMsg(); ?>

<?php echo CHtml::link('Tìm Kiếm Nâng Cao', '#', array('class' => 'search-button')); ?>
<div class="search-form display_none" style="display:none">
    <?php $this->renderPartial('_search', array(
        'model' => $model,
    )); ?>
</div><!-- search-form -->

<?php
//if ($data->type == GasSettle::TYPE_PRE_SETTLE && $data->status == GasSettle::STA_WAIT_SETTLE) {echo 'create_settle';}else {echo 'update';}
$this->widget('zii.widgets.grid.CGridView', array(
    'id' => 'gas-settle-grid',
    'dataProvider' => $model->search(),
    'afterAjaxUpdate' => 'function(id, data){ fnUpdateColorbox();}',
    'template' => '{pager}{summary}{items}{pager}{summary}',
//    'itemsCssClass' => 'items custom_here',
//    'htmlOptions'=> array('class'=>'grid-view custom_here'),
    'pager' => array(
        'maxButtonCount' => CmsFormatter::$PAGE_MAX_BUTTON,
    ),
    'enableSorting' => false,
    //'filter'=>$model,
    'columns' => array(
        array(
            'header' => 'S/N',
            'type' => 'raw',
            'value' => '$this->grid->dataProvider->pagination->currentPage * $this->grid->dataProvider->pagination->pageSize + ($row+1)',
            'headerHtmlOptions' => array('width' => '10px', 'style' => 'text-align:center;'),
            'htmlOptions' => array('style' => 'text-align:center;')
        ),
        'code_no',
        array(
            'name' => 'company_id',
            'type' => 'html',
            'value' => '$data->getCompany().$data->getFileOnly()'
        ),
        array(
            'name' => 'Chi tiết',
            'type' => 'html',
            'value' => '$data->getDetailHtml()',
            'htmlOptions' => array('class' => 'w-200')
        ),
        array(
            'type' => 'html',
            'name' => 'reason',
            'value' => '$data->getReasonText()',
        ),
        array(
            'name' => 'type',
            'type' => 'raw',
            'value' => '$data->getTypeText()',
            'htmlOptions' => array('style' => 'text-align:center;')
        ),
        array(
            'name' => 'amount',
            'type' => 'raw',
            'value' => '$data->getAmount().$data->getTotalPayText("<br>Đã chi: ")',
            'htmlOptions' => array('style' => 'text-align:right;')
        ),
        array(
            'name' => 'status',
            'type' => 'html',
            'value' => '$data->getStatusTextViewOnWeb()',
            'htmlOptions' => array('style' => 'text-align:center;')
        ),
        array(
            'name' => 'Ghi chú thủ quỹ',
            'type' => 'html',
            'value' => '$data->getCommentThuQuy()',
        ),
        array(
            'name' => 'Ghi chú kế toán',
            'type' => 'html',
            'value' => '$data->getCommentKeToan()',
        ),
        array(
            'name' => 'uid_login',
            'value' => '$data->getNameUserCreate()'
        ),
        array(
            'name' => 'created_date',
            'type' => 'Datetime',
            'htmlOptions' => array('style' => 'width:50px;'),
        ),
        array(
            'name' => 'cashier_confirm_date',
            'type' => 'Datetime',
            'htmlOptions' => array('style' => 'width:50px;'),
            'visible'=> isset($_GET['cashier_confirm']),
        ),
        array(
            'header' => 'Actions',
            'class' => 'CButtonColumn',
//            'template' => ControllerActionsName::createIndexButtonRoles($actions),
            'template' => ControllerActionsName::createIndexButtonRoles($actions, array('view','update','delete','noteTreasurerAccountant')),
            'buttons' => array(
                'delete' => array(
                    'visible' => 'GasCheck::canDeleteData($data)',
                ),
                'update' => array(
                    'visible' => '$data->canUpdate()',
                    'url' => '$data->getUpdateLink()',
                ),
                'noteTreasurerAccountant' => array(
                    'label'     => 'Ghi chú',     //Text label of the button.
                    'url'       => '["noteTreasurerAccountant", "id" => $data->id]',
                    'imageUrl'  => Yii::app()->theme->baseUrl . '/admin/images/edit.png',
                    'options'   => array('class' => 'note_treasurer_accountant'), //HTML options for the button tag..
                    'visible'   => '$data->canUpdateComment()',
                ),
            ),
        ),
    ),
)); ?>

<script>
    $(document).ready(function () {
        fnUpdateColorbox();
//        fnBindChangeSearchCondition();
    });

    function fnUpdateColorbox() {
        fnShowhighLightTr();
        fixTargetBlank();
        fnnoteTreasurerAccountant();
        $(".view").colorbox({
            iframe: true,
            innerHeight: '1000',
            innerWidth: '1100', escKey: false, close: "<span title='close'>close</span>"
        });
//        fnBindSendMail();
        $(".gallery").colorbox({iframe:true,innerHeight:'1100', innerWidth: '1000',close: "<span title='close'>close</span>"});
        $('.AddIconGrid').each(function(){
            var tr = $(this).closest('tr');
            tr.find('.button-column').append($(this).html());
        });
    }

    /**
     * @author LocNV Sep 20, 2019
     * @todo modal save comment
     */
    function fnnoteTreasurerAccountant(){
        fixTargetBlank();
        $(".note_treasurer_accountant").colorbox({
            iframe:true,
            innerHeight:'400',
            innerWidth: '1100',
            escKey:false,
            close: "<span title='close'>close</span>"
        });
    }
</script>