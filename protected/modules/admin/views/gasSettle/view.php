<?php
/** @var GasSettle $model */
$cmsFormater    = new CmsFormatter();
$showForm       = false;
$ObjCreatedDate = new DateTime($model->created_date);
echo $model->getBtnCashierConfirm();
echo $model->getBtnStatusUnlock($showForm);
if($showForm){
    include 'view_form_renew.php';
}

?>
<?php include 'view_update_status.php'; ?>

<div class="sprint" style=" padding-right: 235px;">
    <a class="button_print r_padding_20" href="javascript:;" title="Quyết toán">
        <img src="<?php echo Yii::app()->theme->baseUrl; ?>/images/print.png"/>
    </a>

    <?php if($model->canExportExcel()): ?>
    <a href="<?php echo Yii::app()->createAbsoluteUrl("admin/gasSettle/view", array('id'=>$model->id, 'to_excel'=>1));?>">
        <img src="<?php echo Yii::app()->theme->baseUrl; ?>/admin/images/icon/excel.png">
    </a>
    <?php endif; ?>

    <?php if(in_array($model->type,$model->getListTypeSettleHomeContract()) && $model->canExportExcelHomeContract()): ?>
    <a  href="<?php echo Yii::app()->createAbsoluteUrl("admin/gasSettle/view",array('id'=>$model->id,'ToExcelHomeContact'=>1)); ?>">
        <img src="<?php echo Yii::app()->theme->baseUrl; ?>/admin/images/icon/excel.png"/>
    </a>
    <?php endif; ?>
</div>
<div class="clr"></div>
<script src="<?php echo Yii::app()->theme->baseUrl; ?>/js/jquery.printElement.min.js"></script>
<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/print-store-card.css"/>
<script type="text/javascript">
    $(document).ready(function () {
        $(".button_print").click(function () {
            $('#printElement').printElement({overrideElementCSS: ['<?php echo Yii::app()->theme->baseUrl;?>/css/print-store-card.css']});
        });
    });
</script>

<div class="container" id="printElement">
    <div style="margin: 0 auto;" class="f_size_14">
        <?php
        // Thêm từng mẫu form phù hợp với kiểu của nó
        if ($model->type == GasSettle::TYPE_AFTER_SETTLE) {
            include('view_settle.php');
        } elseif ($model->type == GasSettle::TYPE_SETTLE_KHONG_TAM_UNG) {
            include('view_settle_khong_tam_ung.php');
        } elseif (in_array($model->type,$model->getListTypeSettleHomeContract())) {
            include('view_settle_home_contract.php');
        }else {
            include 'view_normal.php';
        } ?>

        <table cellpadding="0" cellspacing="0" class="tb hm_table fix_noborder">
            <tbody>
            <tr>
                <td class="item_c" style="width: 25%;">
                    <p class="item_b">Giám đốc
                        <br/>
                        <?php echo $model->isApprovedByDirector() ? 'Đã duyệt' : '&nbsp;'; ?>
                    </p>
                    <br/><br/><br/><br/>

                    <p><?php echo $model->getDirectorName(); ?>&nbsp;</p>
                </td>
                <td class="item_c" style="width: 25%;">
                    <p class="item_b">PT. Kế toán
                        <br/>
                        <?php echo $model->isApprovedByChiefAccountant() ? 'Đã duyệt' : '&nbsp;'; ?>
                    </p>
                    <br/><br/><br/><br/>

                    <p><?php echo $model->getChiefAccountantName(); ?>&nbsp;</p>
                </td>
                <td class="item_c" style="width: 25%;">
                    <p class="item_b">PT. Bộ phận
                        <br/>
                        <?php echo $model->isApprovedByManager() ? 'Đã duyệt' : '&nbsp;'; ?>
                    </p>
                    <br/><br/><br/><br/>

                    <p><?php echo $model->getLeaderName(); ?>&nbsp;</p>
                </td>
                <td class="item_c">
                    <p class="item_b">Người lập</p>
                    <br/><br/><br/><br/><br/>

                    <p><?php echo $model->getUidLoginName(); ?>&nbsp;</p>
                </td>
            </tr>
            </tbody>
        </table>
    </div>
</div>

<?php echo $cmsFormater->formatBreakTaskDailyFile($model);?>
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/jquery.colorbox-min.js"></script>    
<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/admin/css/colorbox.css" />
<script>
    $(window).load(function () { // không dùng dc cho popup
        fnResizeColorbox();
        parent.$('.SubmitButton').trigger('click');
        $(".gallery").colorbox({iframe:true,innerHeight:'1100', innerWidth: '1000',close: "<span title='close'>close</span>"});
    });

    function fnResizeColorbox() {
//        var y = $('body').height()+100;
        var y = $('#main_box').height() + 100;
        parent.$.colorbox.resize({innerHeight: y});
    }
</script>
