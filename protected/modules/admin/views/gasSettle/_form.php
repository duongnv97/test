<?php $typeList = GasSettle::$LIST_TYPE_TEXT;
    unset($typeList[GasSettle::TYPE_AFTER_SETTLE]);
    unset($typeList[GasSettle::TYPE_REQUEST_MONEY]);//NamNH Sep2719 remove
    unset($typeList[GasSettle::TYPE_REQUEST_MONEY_MULTI]);//NamNH Sep2719 remove
    unset($typeList[GasSettle::TYPE_BANK_TRANSFER_MULTI]);//NamNH Sep2719 remove
?>

<div class="form">
    <?php  $form = $this->beginWidget('CActiveForm', array(
        'id' => 'gas-settle-form',
        'enableAjaxValidation' => false,
        'htmlOptions' => array('enctype' => 'multipart/form-data'),
    )); ?>
    <?php echo $form->errorSummary($model); ?>
    <p class="note">Dòng có dấu <span class="required">*</span> là bắt buộc.</p>
    <?php echo MyFormat::BindNotifyMsg(); ?>
    <div class="row">
        <?php echo $form->labelEx($model, 'uid_login'); ?>
        <?php echo $model->getNameUserCreate(); ?>
        <?php echo $form->error($model, 'uid_login'); ?>
    </div>
    <div class="row">
        <?php echo $form->labelEx($model, 'status'); ?>
        <?php echo GasSettle::$LIST_STATUS_TEXT[GasSettle::STA_NEW]; ?>
        <?php echo $form->error($model, 'status'); ?>
    </div>
    
    <div class="row">
        <?php echo $form->labelEx($model, 'company_id'); ?>
        <?php echo $form->dropDownList($model, 'company_id', $model->getListDataCompany(), array('empty'=>'Select','class' => 'w-400')); ?>
        <?php echo $form->error($model, 'company_id'); ?>
    </div>
    
    <div class="row">
        <?php echo $form->label($model, 'distributor_id', array()); ?>
        <?php echo $form->hiddenField($model, 'distributor_id'); ?>
        <?php
        $url = Yii::app()->createAbsoluteUrl('admin/ajax/search_user_by_code', ['is_maintain' => UsersExtend::STORE_CARD_NCC]);
        // widget auto complete search user customer and supplier
        $aData = array(
            'model' => $model,
            'field_customer_id' => 'distributor_id',
            'url' => $url,
            'name_relation_user' => 'rDistributor',
            'field_autocomplete_name'=>'autocomplete_name1',
            'show_role_name' => 1,
            'ClassAdd' => 'w-400',
        );
        $this->widget('ext.SpaAutocompleteCustomer.SpaAutocompleteCustomer',
            array('data' => $aData));
        ?>
        <?php echo $form->error($model, 'distributor_id'); ?>
    </div>

    <div class="row">
        <em class="hight_light item_b" style="margin-left: 140px;">Chú ý: Đề nghị thanh toán không phải là quyết toán của tạm ứng, bạn sẽ tạo được quyết toán của tạm ứng (Sau khi tạm ứng duyệt lần 3) bằng cách bấm vào biểu tượng bút chì ở tạm ứng đó.</em>
        <div class="clr"></div>
        <?php echo $form->labelEx($model, 'type'); ?>
        <?php // if ($model->scenario != 'update') { ?>
        <?php if (1) { ?>
            <?php echo $form->dropDownList($model, 'type', $typeList, array('class' => 'w-400', 'empty'=>'Select',
                'onChange' => 'javascript:typeChanged(this.value)')); ?>
        <?php } else {
            echo GasSettle::$LIST_TYPE_TEXT[$model->type];
        } ?>
        <?php echo $form->error($model, 'type'); ?>
    </div>
    
    <?php include "_form_settle_khong_tam_ung.php"; ?>

    <div class="row">
        <?php echo $form->labelEx($model, 'uid_leader'); ?>
        <?php echo $form->dropDownList($model, 'uid_leader', $model->getListManager(), array('class' => 'w-400', 'empty'=>"Select")); ?>
        <?php echo $form->error($model, 'uid_leader'); ?>
    </div>
    <div class="row">
        <?php echo $form->labelEx($model, 'reason'); ?>
        <?php echo $form->textArea($model, 'reason', array('rows' => 6, 'class' => 'w-800')); ?>
        <?php echo $form->error($model, 'reason'); ?>
    </div>
    <div class="row more_col">
        <div class="typeOnly2 col1 fix_custom_label_required" style="padding-right: 20px;">
            <?php echo $form->label($model, 'number_order'); ?>
            <?php echo $form->textField($model, 'number_order', array("class"=>"",'size' => 20, 'maxlength' => 12)); ?>
            <?php echo $form->error($model, 'number_order'); ?>
        </div>
        <div class="col2 fix_custom_label_required">
            <?php echo $form->label($model, 'contract_id'); ?>
            <?php 
                $mContractListManageMent = new ContractListManagement();
            ?>
            <?php echo $form->dropDownList($model, 'contract_id', $mContractListManageMent->getListContract(), array('class' => 'w-200', 'empty'=>"Select")); ?>
            <?php echo $form->error($model, 'contract_id'); ?>
        </div>
    </div>
    <!-- Loại tạm ứng-->
    <div class="row fix_custom_label_required type3">
        <!--<em class="hight_light item_b" style="margin-left: 140px;">Chú ý: Tạm ứng trừ lương  đề nghị chọn đúng số tháng hoàn trả</em>-->
        <div class="clr"></div>
        <?php echo $form->labelEx($model,'type_pre_settle'); ?>
        <?php echo $form->dropDownList($model,'type_pre_settle', $model->getArrayTypePreSettle(), array('class'=>'w-400', 'empty'=>'Select')); ?>
        <?php echo $form->error($model,'type_pre_settle'); ?>
    </div>
    <!--Số tháng tạm ứng-->
    <div class="row type3">
        <em class="hight_light item_b display_none" style="margin-left: 140px;">Chú ý: Tạm ứng trừ lương  đề nghị chọn đúng số tháng hoàn trả</em>
        <div class="clr"></div>
        <?php // echo $form->labelEx($model,'monthPayDebit'); ?>
        <?php // echo $form->dropDownList($model,'monthPayDebit', MyFormat::BuildNumberOrder(5), array('class'=>'w-400', 'empty'=>'Select')); ?>
        <?php // echo $form->error($model,'monthPayDebit'); ?>
    </div>
    
    <div class="clr"></div>
    <div class="row">
        <?php echo $form->labelEx($model, 'amount'); ?>
        <div class="fix_number_help">
            <?php echo $form->textField($model, 'amount', array("class"=>"number_only ad_fix_currency SettleAmountFinal",'size' => 20, 'maxlength' => 12)); ?>
            <!--<div class="help_number"></div>-->
        </div>
        <?php echo $form->error($model, 'amount'); ?>
    </div>
    <div class="clr"></div>
    
    <div class="row type2 fix_custom_label_required">
        <em class="hight_light item_b" style="margin-left: 140px;">Chú ý: thông tin chuyển khoản không bắt buộc nhập với loại Quyết toán - Không có tạm ứng</em>
        <div class="clr"></div>
        <?php echo $form->labelEx($model, 'bank_user'); ?>
        <?php echo $form->textField($model, 'bank_user', array('class' => 'w-600', 'maxlength' => 255)); ?>
        <?php echo $form->error($model, 'bank_user'); ?>
    </div>
    <div class="row type2 fix_custom_label_required">
        <?php echo $form->labelEx($model, 'bank_number'); ?>
        <?php echo $form->textField($model, 'bank_number', array('class' => 'w-600', 'maxlength' => 255)); ?>
        <?php echo $form->error($model, 'bank_number'); ?>
    </div>
    <div class="row type2 fix_custom_label_required">
        <?php echo $form->labelEx($model, 'bank_name'); ?>
        <?php echo $form->textField($model, 'bank_name', array('class' => 'w-600', 'maxlength' => 255)); ?>
        <?php echo $form->error($model, 'bank_name'); ?>
    </div>
    <div class="row type2 fix_custom_label_required">
        <?php echo $form->labelEx($model, 'bank_branch'); ?>
        <?php echo $form->textField($model, 'bank_branch', array('class' => 'w-600', 'maxlength' => 255)); ?>
        <?php echo $form->error($model, 'bank_branch'); ?>
    </div>
    <div class="row type2 fix_custom_label_required">
            <?php echo $form->labelEx($model,'bank_province_id'); ?>
            <?php echo $form->dropDownList($model,'bank_province_id', $model->getArrAllProvince(),array('class'=>'','empty'=>'Select')); ?>
            <?php echo $form->error($model,'bank_province_id'); ?>
    </div>
    <div class="row type0">
        <?php echo $form->labelEx($model, 'note'); ?>
        <?php echo $form->textArea($model, 'note', array('rows' => 6, 'cols' => 50)); ?>
        <?php echo $form->error($model, 'note'); ?>
    </div>
    <div class="row type4">
        <?php echo $form->labelEx($model, 'parent_id'); ?>
        <?php echo $form->textField($model, 'parent_id', array('size' => 20, 'maxlength' => 20)); ?>
        <?php echo $form->error($model, 'parent_id'); ?>
    </div>
    
    <?php $aModelFile = GasFile::getAllFile($model, GasFile::TYPE_6_SETTLE_CREATE); ?>
    <?php include "_upload_multi.php"; ?>
    
    <div class="row buttons" style="padding-left: 141px;">
        <?php $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType' => 'submit',
            'label' => $model->isNewRecord ? 'Create' : 'Save',
            'type' => 'null', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
            'size' => 'small', // null, 'large', 'small' or 'mini'
            //'htmlOptions' => array('style' => 'margin-bottom: 10px; float: right;'),
        )); ?>
    </div>

    <?php $this->endWidget(); ?>

</div><!-- form -->

<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/autoNumeric-master/autoNumeric.js"></script>
<script>
    function typeChanged($value) {
        $('.type0, .type1, .type2, .type3, .type4, .type5').hide();
        $('.type' + $value).show();
        $('.typeOnly0, .typeOnly1, .typeOnly2, .typeOnly3, .typeOnly4, .typeOnly5').hide();
        $('.typeOnly' + $value).show();
        if($value == <?php echo GasSettle::TYPE_SETTLE_KHONG_TAM_UNG;?>){
            $('.type2').show();
        }
        if($value == <?php echo GasSettle::TYPE_PRE_SETTLE;?>){
            $('.type1').show();
        }
    }
    
    $('#GasSettle_type').on('change',function(){
        refreshBankInfo();
    });
    
    $('#GasSettle_contract_id').on('change',function(){
        $.blockUI({ overlayCSS: { backgroundColor: '#fff' } }); 
        $value = this.value;
        $.ajax({
            url:'<?php echo Yii::app()->createAbsoluteUrl('admin/gasSettle/index',['AJAX_GET_INFO'=>1]) ?>',
            type: 'get',
            data: {idContract:$value},
            success: function(data){
                var jsonReturn = JSON.parse(data);
                $('#GasSettle_bank_user').val(jsonReturn['customer_name']);
                $('#GasSettle_bank_number').val(jsonReturn['bank_account_number']);
                $('#GasSettle_bank_name').val(jsonReturn['bank']);
                $('#GasSettle_bank_branch').val(jsonReturn['bank_branch']);
                $('#GasSettle_bank_province_id').val(jsonReturn['province_id']);
                $.unblockUI();
            }
        });
    });
    
    function refreshBankInfo(){
        $('#GasSettle_contract_id').val('');
        $('#GasSettle_bank_user').val('');
        $('#GasSettle_bank_number').val('');
        $('#GasSettle_bank_name').val('');
        $('#GasSettle_bank_branch').val('');
        $('#GasSettle_bank_province_id').val('');
    }
    
    $(document).ready(function () {
        typeChanged(<?php echo $model->type; ?>);
        $('.form').find('button:submit').click(function () {
            $.blockUI({overlayCSS: {backgroundColor: '#fff'}});
        });
        fnBindFixLabelRequired();
        fnBindRemoveIcon();
        fnInitInputCurrency();
    });
</script>