<?php
/** @var GasSettle $parent */
/** @var GasSettle $model */
$parent = $model->rParent;
?>
<strong class="item_l" style="text-transform: uppercase"><?php echo $parent->getCompany() ?> <br><?php echo $model->code_no;?></strong>
<?php
if ($parent) { ?>
    <strong class="item_r" style="float: right">Số tiền tạm ứng: <?php echo $parent->getAmount(true) ?></strong>
    <br>
    <strong class="item_r" style="float: right">Ngày ứng: <?php echo $parent->getDirectorApproveDate() ?></strong>
<?php } ?>


<h2 class="item_c" style="text-transform: uppercase"> Giấy <?php echo $model->getTypeText(); ?>
    <i class="f_size_12">
        </br>
        </br>
        Ngày <?php echo $ObjCreatedDate->format('d'); ?>
        tháng <?php echo $ObjCreatedDate->format('m'); ?>
        năm <?php echo $ObjCreatedDate->format('Y'); ?>
    </i>
</h2>

<p class="item_b"><span class="item_u item_i">Kính gửi:</span> Ban Giám Đốc</p>
<p class=""><span class="item_b item_i">Nội dung tạm ứng: </span> <?php echo $parent->getReason();?></p>
<p class=""><span class="item_b item_i">Chi tiết quyết toán: </span> </p>

<?php
$parentAmount = 0;
if ($model->rParent) {
    $recordList = $model->rParent->getSettleChilds();
    $parentAmount = $model->rParent->amount;
}
$fakeSettleRecord = new GasSettleRecord();
?>
<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/print-store-card.css"/>
<table cellpadding="0" cellspacing="0" class="tb hm_table">
    <thead>

    <tr>
        <th>#</th>
        <th><?php echo $fakeSettleRecord->getAttributeLabel('item_name'); ?></th>
        <th><?php echo $fakeSettleRecord->getAttributeLabel('payment_no'); ?></th>
        <th><?php echo $fakeSettleRecord->getAttributeLabel('unit'); ?></th>
        <th><?php echo $fakeSettleRecord->getAttributeLabel('qty'); ?></th>
        <th><?php echo $fakeSettleRecord->getAttributeLabel('price'); ?></th>
        <th><?php echo $fakeSettleRecord->getAttributeLabel('total'); ?></th>
        <th><?php echo $fakeSettleRecord->getAttributeLabel('note'); ?></th>
    </tr>
    </thead>
    <tbody>
    <?php
    // Add existed items
    $current = 0;
    $total = 0;
    if (!empty($recordList))
        foreach ($recordList as $record) {
            $subTotal = $record->price * $record->qty;
            $total += $subTotal;
            ?>
            <tr>
                <td class="order_no item_c">
                    <?php echo $current + 1; ?>
                </td>
                <td class="item_c">
                    <?php echo $record->item_name; ?>
                </td>
                <td class="item_c">
                    <?php echo $record->payment_no; ?>
                </td>
                <td class="item_c">
                    <?php echo $record->unit; ?>
                </td>
                <td class="item_c">
                    <?php echo ActiveRecord::formatCurrency($record->qty); ?>
                </td>
                <td class="item_r">
                    <?php echo ActiveRecord::formatCurrency($record->price); ?>
                </td>
                <td class="item_r">
                    <?php echo ActiveRecord::formatCurrency($subTotal); ?>
                </td>
                <td class="item_c">
                    <?php 
                        $distributor = $record->getDistributor();
                        if(!empty($distributor)){
                            echo '<b>'.$distributor.'</b><br>';
                        }
                    ?>
                    <?php echo nl2br($record->note); ?>
                </td>
            </tr>
            <?php
            $current++;
        }
    ?>
    </tbody>
    <tfoot>
    <tr>
        <td class="item_l" colspan="6">TỔNG CỘNG</td>
        <td class="item_r"><?php echo ActiveRecord::formatCurrency($total); ?></td>
        <td class="item_c">&nbsp;</td>
    </tr>
    <?php $deltaAmount = $total - $parentAmount; ?>
    <tr>
        <td class="item_l" colspan="6">1. Số tạm ứng chi không hết</td>
        <td class="item_r"><?php echo ActiveRecord::formatCurrency(($deltaAmount > 0) ? 0 : -$deltaAmount); ?> </td>
        <td class="item_c">&nbsp;</td>
    </tr>
    <tr>
        <td class="item_l" colspan="6">2. Chi quá số tạm ứng</td>
        <td class="item_r"><?php echo ActiveRecord::formatCurrency(($deltaAmount > 0) ? $deltaAmount : 0); ?> </td>
        <td class="item_c">&nbsp;</td>
    </tr>
    <tr>
        <td colspan="8">
            <?php include 'view_viettel_pay.php'; ?>
        </td>
    </tr>
    </tfoot>
</table>
