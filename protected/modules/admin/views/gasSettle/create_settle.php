<?php
$this->breadcrumbs = array(
    $this->pluralTitle => array('index'),
    'Làm Quyết Toán',
);

$menus = array(
    array('label' => "$this->singleTitle Management", 'url' => array('index')),
);
$this->menu = ControllerActionsName::createMenusRoles($menus, $actions);
?>

    <h1>Làm <?php echo $this->singleTitle; ?></h1>

<?php echo $this->renderPartial('_form_settle', array('model' => $model, 'recordList' => $recordList, 'parentId' => $parentId)); ?>