<div class="wide form">

    <?php $form = $this->beginWidget('CActiveForm', array(
        'action' => GasCheck::getCurl(),
        'method' => 'get',
    )); ?>

    <div class="row">
        <?php echo $form->label($model, 'uid_login', array()); ?>
        <?php echo $form->hiddenField($model, 'uid_login'); ?>
        <?php
        $url = Yii::app()->createAbsoluteUrl('admin/ajax/search_for_user_login');
        // widget auto complete search user customer and supplier
        $aData = array(
            'model' => $model,
            'field_customer_id' => 'uid_login',
            'url' => $url,
            'name_relation_user' => 'rUidLogin',
            'show_role_name' => 1,
            'ClassAdd' => 'w-400',
        );
        $this->widget('ext.SpaAutocompleteCustomer.SpaAutocompleteCustomer',
            array('data' => $aData));
        ?>
    </div>
    
    <div class="row more_col">
        <div class="col1">
            <?php echo $form->label($model, 'code_no', array()); ?>
            <?php echo $form->textField($model, 'code_no', array('class' => 'w-300', 'maxlength' => 20)); ?>
        </div>

        <div class="col2">
            <?php echo $form->labelEx($model, 'amount'); ?>
            <?php echo $form->textField($model, 'amount', array('class' => 'w-300', 'maxlength' => 20)); ?>
        </div>
    </div>
    
    <div class="row more_col">
        <div class="col1">
            <?php echo $form->label($model, 'uid_director', array()); ?>
            <?php echo $form->dropDownList($model, 'uid_director', $model->getListDirector(), array('empty' => 'Select', 'class' => 'w-300')); ?>
        </div>
        <div class="col2">
            <?php echo $form->label($model, 'uid_chief_accountant', array()); ?>
            <?php echo $form->dropDownList($model, 'uid_chief_accountant', $model->getListChiefAccountant(), array('empty' => 'Select', 'class' => 'w-300')); ?>
        </div>
    </div>
    <div class="row more_col">
        <div class="col1">
            <?php echo $form->label($model, 'uid_leader', array()); ?>
            <?php echo $form->dropDownList($model, 'uid_leader', $model->getListManager(), array('empty' => 'Select', 'class' => 'w-300')); ?>
        </div>
        <div class="col2">
            <?php echo $form->label($model, 'uid_accounting', array()); ?>
            <?php echo $form->dropDownList($model, 'uid_accounting', $model->getAccounting(), array('empty' => 'Select', 'class' => 'w-300')); ?>
        </div>
    </div>
    
    <div class="row more_col">
        <div class="col1">
            <?php echo $form->label($model, 'uid_cashier_confirm', array()); ?>
            <?php echo $form->dropDownList($model, 'uid_cashier_confirm', $model->getCashierConfirm(), array('empty' => 'Select', 'class' => 'w-300')); ?>
        </div>
        <div class="col2">
            <?php echo $form->label($model, 'type', array()); ?>
            <?php echo $form->dropDownList($model, 'type', GasSettle::$LIST_TYPE_TEXT, array('empty' => 'Select', 'class' => 'w-300')); ?>
        </div>
    </div>
    
    <div class="row more_col">
        <div class="col1">
            <?php echo $form->label($model, 'status', array()); ?>
            <?php echo $form->dropDownList($model, 'status', GasSettle::$LIST_STATUS_TEXT, array('empty' => 'Select', 'class' => 'w-300')); ?>
        </div>
        <div class="col2">
            <?php echo $form->labelEx($model, 'company_id'); ?>
            <?php echo $form->dropDownList($model, 'company_id', $model->getListDataCompany(), array('empty'=>'Select','class' => 'w-300')); ?>
        </div>
    </div>
    
    
    <div class="row">
        <?php echo $form->label($model, 'distributor_id', array()); ?>
        <?php echo $form->hiddenField($model, 'distributor_id'); ?>
        <?php
        $url = Yii::app()->createAbsoluteUrl('admin/ajax/search_user_by_code', ['is_maintain' => UsersExtend::STORE_CARD_NCC]);
        // widget auto complete search user customer and supplier
        $aData = array(
            'model' => $model,
            'field_customer_id' => 'distributor_id',
            'url' => $url,
            'name_relation_user' => 'rDistributor',
            'field_autocomplete_name'=>'autocomplete_name1',
            'show_role_name' => 1,
            'ClassAdd' => 'w-400',
        );
        $this->widget('ext.SpaAutocompleteCustomer.SpaAutocompleteCustomer',
            array('data' => $aData));
        ?>
        <?php echo $form->error($model, 'distributor_id'); ?>
    </div>
    
    <div class="row more_col">
        <div class="col1">
                <?php echo Yii::t('translation', $form->label($model,'date_from')); ?>
                <?php 
                    $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                        'model'=>$model,        
                        'attribute'=>'date_from',
                        'options'=>array(
                            'showAnim'=>'fold',
                            'dateFormat'=> MyFormat::$dateFormatSearch,
//                            'minDate'=> '0',
                            'maxDate'=> '0',
                            'changeMonth' => true,
                            'changeYear' => true,
                            'showOn' => 'button',
                            'buttonImage'=> Yii::app()->theme->baseUrl.'/admin/images/icon_calendar_r.gif',
                            'buttonImageOnly'=> true,                                
                        ),        
                        'htmlOptions'=>array(
                            'class'=>'w-16',
                            'size'=>'16',
                            'style'=>'float:left;',                               
                        ),
                    ));
                ?>     		
        </div>
        <div class="col2">
                <?php echo Yii::t('translation', $form->label($model,'date_to')); ?>
                <?php 
                    $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                        'model'=>$model,        
                        'attribute'=>'date_to',
                        'options'=>array(
                            'showAnim'=>'fold',
                            'dateFormat'=> MyFormat::$dateFormatSearch,
//                            'minDate'=> '0',
                            'maxDate'=> '0',
                            'changeMonth' => true,
                            'changeYear' => true,
                            'showOn' => 'button',
                            'buttonImage'=> Yii::app()->theme->baseUrl.'/admin/images/icon_calendar_r.gif',
                            'buttonImageOnly'=> true,                                
                        ),        
                        'htmlOptions'=>array(
                            'class'=>'w-16',
                            'size'=>'16',
                            'style'=>'float:left;',
                        ),
                    ));
                ?>     		
        </div>
    </div>
    <div class="row">
        <?php echo $form->label($model, 'contract_id'); ?>
        <?php 
            $mContractListManageMent = new ContractListManagement();
        ?>
        <?php echo $form->dropDownList($model, 'contract_id', $mContractListManageMent->getListContract(), array('class' => 'w-300', 'empty'=>"Select")); ?>
    </div>

    <div class="row buttons" style="padding-left: 159px;">
        <?php $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType' => 'submit',
            'label' => 'Search',
            'type' => 'null', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
            'size' => 'small', // null, 'large', 'small' or 'mini'
            //'htmlOptions' => array('style' => 'margin-bottom: 10px; float: right;'),
        )); ?>    </div>

    <?php $this->endWidget(); ?>

</div><!-- search-form -->