<?php
/**
 * @var \GasSettle $model
 */
$listDataDistributor = $model->getListDistributor();
?>
<div class="form">

    <?php $form = $this->beginWidget('CActiveForm', array(
        'id' => 'gas-settle-form',
        'enableAjaxValidation' => false,
        'htmlOptions' => array('enctype' => 'multipart/form-data'),
    )); ?>

    <p class="note">Dòng có dấu <span class="required">*</span> là bắt buộc.</p>
    <?php echo $form->errorSummary($model); ?>
    <?php echo MyFormat::BindNotifyMsg(); ?>
    <div class="row">
        <?php echo $form->labelEx($model, 'uid_login'); ?>
        <?php echo $model->getNameUserCreate(); ?>
        <?php echo $form->error($model, 'uid_login'); ?>
    </div>
    <div class="row">
        <?php echo $form->labelEx($model, 'type'); ?>
        <?php echo GasSettle::$LIST_TYPE_TEXT[GasSettle::TYPE_AFTER_SETTLE]; ?>
        <?php echo $form->error($model, 'type'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model, 'status'); ?>
        <?php echo GasSettle::$LIST_STATUS_TEXT[GasSettle::STA_NEW]; ?>
        <?php echo $form->error($model, 'status'); ?>
    </div>
    <div class="row">
        <?php echo $form->labelEx($model, 'uid_leader'); ?>
        <?php echo $form->dropDownList($model, 'uid_leader', $model->getListManager(), array('class' => 'w-400')); ?>
        <?php echo $form->error($model, 'uid_leader'); ?>
    </div>
    <div class="row">
        <?php echo $form->labelEx($model, 'amount'); ?>
        <span id="sum"><?php echo $model->amount; ?></span>
        <?php echo $form->error($model, 'amount'); ?>
    </div>
    <div class="row">
        <?php echo $form->error($model, 'parent_id'); ?>
    </div>

    <div class="clr"></div>
    <div class="row">
        <label>&nbsp</label>

        <div>
            <a href="javascript:void(0);" class="text_under_none item_b" style="line-height:25px"
               onclick="fnBuildRow();">
                <img style="float: left;margin-right:8px;"
                     src="<?php echo Yii::app()->theme->baseUrl; ?>/images/add.png">
                Thêm Dòng ( Phím tắt F8 )
            </a>
        </div>
    </div>
    <div class="clr"></div>

    <?php $fakeSettleRecord = new GasSettleRecord(); ?>
    <div class="row">
        <label>&nbsp</label>
        <table class="materials_table hm_table TbItem">
            <thead>
            <tr>
                <th class="item_c">#</th>
                <th class="item_c w-250"><?php echo $fakeSettleRecord->getAttributeLabel('item_name'); ?></th>
                <th class="item_c w-100"><?php echo $fakeSettleRecord->getAttributeLabel('payment_no'); ?></th>
                <th class="item_c w-100"><?php echo $fakeSettleRecord->getAttributeLabel('unit'); ?></th>
                <th class="item_c w-100"><?php echo $fakeSettleRecord->getAttributeLabel('qty'); ?></th>
                <th class="item_c w-100"><?php echo $fakeSettleRecord->getAttributeLabel('price'); ?></th>
                <th class="item_c w-100"><?php echo $fakeSettleRecord->getAttributeLabel('total'); ?></th>
                <th class="item_c w-100"><?php echo $fakeSettleRecord->getAttributeLabel('note'); ?></th>
                <th class="last item_c">Xóa</th>
            </tr>
            </thead>
            <tbody>
            <?php
            // Add existed items
            $current = 0;
            if (is_array($recordList)):
                foreach ($recordList as $record) {
                    ?>
                    <tr class="table_tr_row">
                        <td class="order_no item_c">
                            <?php echo $current + 1; ?>
                            <input class="materials_id_hide" value="" type="hidden"
                                   name="SettleRecord[<?php echo $current; ?>][id]">
                        </td>
                        <td class="item_c">
                            <input name="SettleRecord[<?php echo $current; ?>][item_name]" class="seri" type="text"
                                   size="25"
                                   value="<?php echo $record->item_name; ?>">
                        </td>
                        <td class="item_c">
                            <input name="SettleRecord[<?php echo $current; ?>][payment_no]" class="w-50" type="text"
                                   value="<?php echo $record->payment_no; ?>">
                        </td>
                        <td class="item_c">
                            <input name="SettleRecord[<?php echo $current; ?>][unit]" class="w-50" type="text"
                                   value="<?php echo $record->unit; ?>">
                        </td>
                        <td class="item_c">
                            <input name="SettleRecord[<?php echo $current; ?>][qty]" class="w-50 change_amount_row qty"
                                   value="<?php echo ActiveRecord::formatNumberInput($record->qty); ?>">
                        </td>
                        <td class="item_c">
                            <input name="SettleRecord[<?php echo $current; ?>][price]"
                                   class="w-80 change_amount_row price"
                                   value="<?php echo $record->price; ?>" >
                        </td>
                        <td class="item_c item_b" >
                            <input
                                class="total"
                                value=" <?php echo $record->price * $record->qty; ?>"
                                type="hidden">
                        <span class="total_text">
                            <?php echo ActiveRecord::formatCurrency($record->price * $record->qty); ?>
                        </span>
                        </td>
                        <td class="item_c">
                            <?php echo CHtml::dropDownList("SettleRecord[$current][distributor_id]", '',
                                        $listDataDistributor, array('empty'=>'Chọn NPP', 'class' => 'w-200 gSelectSmall display_none', 'options' => array($record->distributor_id => array('selected'=>true))));?>
                        <textarea rows="4" cols="50"
                                  name="SettleRecord[<?php echo $current; ?>][note]"><?php echo trim($record->note); ?></textarea>
                        </td>
                        <td class="item_c last"><span class="remove_icon_only"></span></td>
                    </tr>
                    <?php
                    $current++;
                }
            endif;
            ?>
            </tbody>
        </table>
    </div>
    <?php $aModelFile = GasFile::getAllFile($model, GasFile::TYPE_5_SETTLE); ?>
    <?php include "_upload_multi.php"; ?>

    <div class="row buttons" style="padding-left: 141px;">
        <?php $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType' => 'submit',
            'label' => $model->isNewRecord ? 'Create' : 'Save',
            'type' => 'null', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
            'size' => 'small', // null, 'large', 'small' or 'mini'
            //'htmlOptions' => array('style' => 'margin-bottom: 10px; float: right;'),
        )); ?>    
    </div>

    <?php $this->endWidget(); ?>

</div><!-- form -->
<style>
    .hidden {
        display: none !important;
    }
</style>
<script>
    /**
     * Number.prototype.format(n, x, s, c)
     *
     * @param integer n: length of decimal
     * @param integer x: length of whole part
     * @param mixed   s: sections delimiter
     * @param mixed   c: decimal delimiter
     */
    Number.prototype.format = function (n, x, s, c) {
        var re = '\\d(?=(\\d{' + (x || 3) + '})+' + (n > 0 ? '\\D' : '$') + ')',
            num = this.toFixed(Math.max(0, ~~n));

        return (c ? num.replace('.', c) : num).replace(new RegExp(re, 'g'), '$&' + (s || ','));
    };
    var unformat = function (value, decimal) {
        // Recursively unformat arrays:
        if (isArray(value)) {
            return map(value, function (val) {
                return unformat(val, decimal);
            });
        }

        // Fails silently (need decent errors):
        value = value || 0;

        // Return the value as-is if it's already a number:
        if (typeof value === "number") return value;

        // Build regex to strip out everything except digits, decimal point and minus sign:
        var regex = new RegExp("[^0-9-" + decimal + "]", ["g"]),
            unformatted = parseFloat(
                ("" + value)
                    .replace(/\((.*)\)/, "-$1") // replace bracketed values with negatives
                    .replace(regex, '')         // strip out any cruft
                    .replace(decimal, '.')      // make sure decimal point is standard
            );

        // This will fail silently which may cause trouble, let's wait and see:
        return !isNaN(unformatted) ? unformatted : 0;
    };

    function typeChanged($value) {
        $('.type0, .type1, .type2, .type3, .type4').hide();
        $('.type' + $value).show();
    }

    $(document).ready(function () {
        typeChanged(<?php echo $model->type; ?>);
        $('.form').find('button:submit').click(function () {
            $.blockUI({overlayCSS: {backgroundColor: '#fff'}});
        });
        fnBindFixLabelRequired();

        $current = <?php echo $current;?>;
        fnBuildRow();

        $('.change_amount_row').live('change', updateItemPrice);
        $('.change_amount_row').live('keyup', updateItemPrice);
        updateTotal();
        fnBindRemoveIcon();
    });

    function updateItemPrice() {
        var tr = $(this).closest('tr');
        var change_amount_1 = tr.find('.qty').eq(0).val();
        var change_amount_2 = tr.find('.price').eq(0).val();
        var amount = parseInt(change_amount_2 * change_amount_1);
        tr.find('.total_text').text(commaSeparateNumber(amount));
        tr.find('.total').val(amount);

        updateTotal();
    }

    // Update total
    function updateTotal() {
        var sum = 0;
        $('.total').each(function () {
            sum += Number($(this).val());
        });
        $("#sum").html(sum.format(0, 3, ',', '.'));
    }

    // Trung June 09 2016, For hotkey
    $(document).keydown(function (e) {
        if (e.which == 119) {
            fnBuildRow();
        }
    });

    function fnBuildRow() {
        if($('.table_tr_row').size() > <?php echo GasSettle::MAX_SETTLE_ITEMS;?>){
            return ;
        }
        $rowHtml = '<tr class="table_tr_row">\
        <td class="order_no item_c">\
            ' + ($current + 1) + '\
            <input class="materials_id_hide" value="" type="hidden" name="SettleRecord[' + $current + '][id]">\
            </td>\
            <td class="item_c">\
            <input name="SettleRecord[' + $current + '][item_name]" class="seri" type="text" size="25">\
            </td>\
            <td class="item_c">\
            <input name="SettleRecord[' + $current + '][payment_no]" class="w-50" type="text">\
            </td>\
            <td class="item_c">\
            <input name="SettleRecord[' + $current + '][unit]" class="w-50" type="text">\
            </td>\
            <td class="item_c">\
            <input name="SettleRecord[' + $current + '][qty]" class="w-50 change_amount_row qty"  value="1">\
            </td>\
            <td class="item_c">\
            <input name="SettleRecord[' + $current + '][price]" class="w-80 change_amount_row price">\
            </td>\
            <td class="item_c item_b">\
            <input class="total" type="hidden">\
            <span class="total_text">0</span>\
            </td>\
            <td class="item_c">\
            <textarea rows="4" cols="50" name="SettleRecord[' + $current + '][note]"></textarea>\
            </td>\
            <td class="item_c last"><span class="remove_icon_only"></span></td>\
        </tr>';
        $('.TbItem tbody').append($rowHtml);
        $current++;
//        fnRefreshOrderNumber();
//        fnBindAllAutocomplete();
        fnRefreshOrderNumber();        
    }
</script>