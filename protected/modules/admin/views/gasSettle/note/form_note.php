<?php $this->breadcrumbs = array($this->pageTitle); ?>
<?php echo MyFormat::BindNotifyMsg(); ?>

<h1>Ghi chú <?php echo $this->singleTitle; ?></h1>

<div class="form">
    <?php  $form = $this->beginWidget('CActiveForm', array(
        'id' => 'gas-settle-form',
        'enableAjaxValidation' => false,
        'htmlOptions' => array('enctype' => 'multipart/form-data'),
    )); ?>

    <?php if ($model->canThuQuyUpdateComment()) : ?>
        <div class="row">
            <?php echo $form->labelEx($model, 'comment_thu_quy', array('label' => 'Ghi chú của thủ quỹ')); ?>
            <?php echo $form->textArea($model, 'comment_thu_quy', array('rows' => 6, 'class' => 'w-800')); ?>
            <?php echo $form->error($model, 'comment_thu_quy'); ?>
        </div>
    <?php endif; ?>

    <?php if ($model->canKeToanUpdateComment()) : ?>
        <div class="row">
            <?php echo $form->labelEx($model, 'comment_ke_toan', array('label' => 'Ghi chú của kế toán')); ?>
            <?php echo $form->textArea($model, 'comment_ke_toan', array('rows' => 6, 'class' => 'w-800')); ?>
            <?php echo $form->error($model, 'comment_ke_toan'); ?>
        </div>
    <?php endif; ?>

    <div class="row buttons" style="padding-left: 141px;">
        <?php $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType' => 'submit',
            'label' => 'Save',
            'type' => 'null', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
            'size' => 'small', // null, 'large', 'small' or 'mini'
            //'htmlOptions' => array('style' => 'margin-bottom: 10px; float: right;'),
        )); ?>
    </div>

    <?php $this->endWidget(); ?>
</div>
