<?php foreach($aModelFile as $mFile):?>
    <tr class="materials_row">
        <td class="item_c order_no"></td>
        <td class="item_l w-400">
            <img>
            <?php echo $mFile->getViewImg(); ?>
        </td>
        <td class="item_c last">
            <input type="checkbox" name="delete_file[]" value="<?php echo $mFile->id;?>">
        </td>
    </tr>
<?php endforeach;?>

<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/jquery.colorbox-min.js"></script>    
<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/admin/css/colorbox.css" />
<script>
    $(window).load(function () { // không dùng dc cho popup
        $(".gallery").colorbox({iframe:true,innerHeight:'1100', innerWidth: '1000',close: "<span title='close'>close</span>"});
    });

</script>