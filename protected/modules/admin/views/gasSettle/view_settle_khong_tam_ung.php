<?php
/** @var GasSettle $parent */
/** @var GasSettle $model */
$parent = $model->rParent;
?>
<strong class="item_l" style="text-transform: uppercase"><?php echo $model->getCompany(); ?> <br><?php echo $model->code_no;?> </strong>
<h2 class="item_c" style="text-transform: uppercase"> Giấy <?php echo $model->getTypeText(true); ?>
    <i class="f_size_12">
        </br>
        </br>
        Ngày <?php echo $ObjCreatedDate->format('d'); ?>
        tháng <?php echo $ObjCreatedDate->format('m'); ?>
        năm <?php echo $ObjCreatedDate->format('Y'); ?>
    </i>
</h2>

<p class="item_b"><span class="item_u item_i">Kính gửi:</span> Ban Giám Đốc</p>
<?php
$recordList = $model->rDetail;
$fakeSettleRecord = new GasSettleRecord();
?>
<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/print-store-card.css"/>
<table cellpadding="0" cellspacing="0" class="tb hm_table">
    <thead>
        <tr>
            <th class="item_c" rowspan="2">#</th>
            <th class="item_c " rowspan="2"><?php echo $fakeSettleRecord->getAttributeLabel('item_name'); ?></th>
            <th class="item_c item_border display_none" colspan="4">Dự Toán</th>
            <th class="item_c item_border" colspan="5">Quyết Toán</th>
            <th class="item_c " rowspan="2"><?php echo $fakeSettleRecord->getAttributeLabel('note'); ?></th>
        </tr>
        <tr>
            <th class="item_c display_none"><?php echo $fakeSettleRecord->getAttributeLabel('unit'); ?></th>
            <th class="item_c display_none"><?php echo $fakeSettleRecord->getAttributeLabel('qty'); ?></th>
            <th class="item_c display_none"><?php echo $fakeSettleRecord->getAttributeLabel('price'); ?></th>
            <th class="item_c display_none"><?php echo $fakeSettleRecord->getAttributeLabel('total'); ?></th>

            <th class="item_c "><?php echo $fakeSettleRecord->getAttributeLabel('payment_no'); ?></th>
            <th class="item_c "><?php echo $fakeSettleRecord->getAttributeLabel('unit'); ?></th>
            <th class="item_c "><?php echo $fakeSettleRecord->getAttributeLabel('qty'); ?></th>
            <th class="item_c "><?php echo $fakeSettleRecord->getAttributeLabel('price'); ?></th>
            <th class="item_c "><?php echo $fakeSettleRecord->getAttributeLabel('total'); ?></th>
        </tr>
    </thead>
    <tbody>
    <?php
    // Add existed items
    $current = 0;
    $total = 0;
    $totalPlan = 0;
    if (!empty($recordList))
        foreach ($recordList as $record) {
            $subTotal   = $record->price * $record->qty;
            $total      += $subTotal;
            $totalPlan  += $record->getPlanTotal();
            ?>
            <tr>
                <td class="order_no item_c">
                    <?php echo $current + 1; ?>
                </td>
                <td class="">
                    <?php echo $record->getItemName(); ?>
                </td>
                <td class="item_c display_none">
                    <?php echo $record->getPlanUnit(); ?>
                </td>
                <td class="item_c display_none">
                    <?php echo $record->getPlanQty(true); ?>
                </td>
                <td class="item_r display_none">
                    <?php echo $record->getPlanPrice(true); ?>
                </td>
                <td class="item_r display_none">
                    <?php echo $record->getPlanTotal(true); ?>
                </td>
                
                <td class="item_c">
                    <?php echo $record->payment_no; ?>
                </td>
                <td class="item_c">
                    <?php echo $record->unit; ?>
                </td>
                <td class="item_c">
                    <?php echo ActiveRecord::formatCurrency($record->qty); ?>
                </td>
                <td class="item_r">
                    <?php echo ActiveRecord::formatCurrency($record->price); ?>
                </td>
                <td class="item_r">
                    <?php echo ActiveRecord::formatCurrency($subTotal); ?>
                </td>
                <td class="item_c">
                    <?php 
                        $distributor = $record->getDistributor();
                        if(!empty($distributor)){
                            echo '<b>'.$distributor.'</b><br>';
                        }
                    ?>
                    <?php echo nl2br($record->note); ?>
                </td>
            </tr>
            <?php
            $current++;
        }
    ?>
    </tbody>
    <tfoot>
        <tr>
            <td class="item_l" colspan="5">TỔNG CỘNG</td>
            <td class="item_r display_none"><?php echo ActiveRecord::formatCurrency($totalPlan); ?></td>
            <td class="item_r" colspan="5"><?php echo ActiveRecord::formatCurrency($total); ?></td>
            <td class="item_c display_none">&nbsp;</td>
        </tr>
        <?php $deltaAmount = $total - $totalPlan; ?>
        <tr>
            <td class="item_l display_none" colspan="10">1. Số tạm ứng chi không hết</td>
            <td class="item_r display_none"><?php echo ActiveRecord::formatCurrency(($deltaAmount > 0) ? 0 : -$deltaAmount); ?> </td>
            <td class="item_c display_none">&nbsp;</td>
        </tr>
        <tr>
            <td class="item_l display_none" colspan="10">2. Chi quá số tạm ứng</td>
            <td class="item_r display_none"><?php echo ActiveRecord::formatCurrency(($deltaAmount > 0) ? $deltaAmount : 0); ?> </td>
            <td class="item_c display_none">&nbsp;</td>
        </tr>
        <?php if(!empty($model->getBankNumber())): ?>
        <tr>
            <td colspan="12">
                <p class="display_none"><span class="item_b">Đề nghị chuyển khoản số tiền:</span> <?php echo $model->getAmount(); ?></p>
                <p><span class="item_b">Viết bằng chữ: <?php echo $model->getAmountString(); ?></span></p>
                <p><span class="item_b">Lý do thanh toán: </span><?php echo $model->getReason(); ?> </p>
                <p><span class="item_b">Đơn vị thụ hưởng: </span><?php echo $model->getBankUser(); ?> </p>
                <p><span class="item_b">Số tài khoản: </span><?php echo $model->getBankNumber(); ?> </p>
                <p><span class="item_b">Tại ngân hàng: </span><?php echo $model->getBankName().' - Chi nhánh: '.$model->getBankBranch().' - Tỉnh: '.$model->getProvinceName(); ?> </p>
            </td>
        </tr>
        <?php endif; ?>
        <tr>
            <td colspan="12">
                <?php include 'view_viettel_pay.php'; ?>
            </td>
        </tr>
    </tfoot>
    
</table>
