<?php
/** @var GasSettle $model */
$cmsFormater = new CmsFormatter();
$ObjCreatedDate = new DateTime($model->created_date);
$model->loadInfoDebit();
?>
<strong class="item_l" style="text-transform: uppercase"><?php echo $model->getCompany() ?>  <br><?php echo $model->code_no;?></strong>
<br>

<h2 class="item_c" style="text-transform: uppercase"> Giấy <?php echo $model->getTypeText(); ?>
    <i class="f_size_12">
        </br>
        </br>
        Ngày <?php echo $ObjCreatedDate->format('d'); ?>
        tháng <?php echo $ObjCreatedDate->format('m'); ?>
        năm <?php echo $ObjCreatedDate->format('Y'); ?>
    </i>
</h2>

<p class="item_b"><span class="item_u item_i">Kính gửi:</span> Ban Giám Đốc</p>
<table cellpadding="0" cellspacing="0" class="tb hm_table fix_noborder">
    <tbody>
    <tr>
        <td><p><span class="item_b">Tôi tên: </span> <?php echo $model->getUidLoginName(); ?></p></td>
        <td><span class="item_b">Bộ phận công tác: </span><?php echo $model->getUidLoginRole(); ?></td>
    </tr>
    <tr>
        <td colspan="2">
            <?php if ($model->type == GasSettle::TYPE_PRE_SETTLE): ?>
                <p><span class="item_b">Đề nghị tạm ứng số tiền:</span> <?php echo $model->getAmount().'&nbsp;&nbsp;&nbsp;'.$model->getTypePreSettle(); ?></p>
                <p><span class="item_b">Viết bằng chữ: </span><?php echo $model->getAmountString(); ?></p>
                <p><span class="item_b">Lý do tạm ứng: </span><?php echo $model->getReason(); ?></p>
                <!--<p><span class="item_b"><?php echo $model->getAttributeLabel('monthPayDebit');?>: </span><?php echo $model->monthPayDebit;?> tháng</p>-->
            <?php elseif ($model->type == GasSettle::TYPE_REQUEST_MONEY): ?>
                <p><span class="item_b">Thanh toán số tiền: </span> <?php echo $model->getAmount(); ?></p>
                <p><span class="item_b">Viết bằng chữ: </span><?php echo $model->getAmountString(); ?></p>
                <p><span class="item_b">Nội dung thanh toán: </span><?php echo $model->getReason(); ?> </p>
            <?php elseif ($model->type == GasSettle::TYPE_BANK_TRANSFER) : ?>
                <p><span class="item_b">Đề nghị chuyển khoản số tiền:</span> <?php echo $model->getAmount(); ?>
                </p>
                <p><span class="item_b">Viết bằng chữ: <?php echo $model->getAmountString(); ?></span></p>
                <p><span class="item_b">Lý do chuyển khoản: </span><?php echo $model->getReason(); ?> </p>
                <p><span class="item_b">Đơn vị thụ hưởng: </span><?php echo $model->getBankUser(); ?> </p>
                <p><span class="item_b">Số tài khoản: </span><?php echo $model->getBankNumber(); ?> </p>
                <p><span class="item_b">Tại ngân hàng: </span><?php echo $model->getBankName().' - Chi nhánh: '.$model->getBankBranch().' - Tỉnh: '.$model->getProvinceName(); ?> </p>
            <?php endif; ?>
            <?php include 'view_viettel_pay.php'; ?>
        </td>
    </tr>
    </tbody>
</table>