<div class="form">
    <?php
        $LinkNormal         = Yii::app()->createAbsoluteUrl('admin/gasSettle/index');
        $LinkCashierConfirm = Yii::app()->createAbsoluteUrl('admin/gasSettle/index', array( 'cashier_confirm'=> 1));
        $LinkExpire         = Yii::app()->createAbsoluteUrl('admin/gasSettle/index', array( 'cashier_confirm'=> GasSettle::SETTLE_EXPIRE));
        $LinkRenew          = Yii::app()->createAbsoluteUrl('admin/gasSettle/index', array( 'cashier_confirm'=> GasSettle::SETTLE_RENEW));
    ?>
    <h1><?php echo $this->adminPageTitle;?>
        <a class='btn_cancel f_size_14 <?php echo (!isset($_GET['cashier_confirm'])) ? "active":"";?>' href="<?php echo $LinkNormal;?>">Mới & Chờ Duyệt</a>
        <a class='btn_cancel f_size_14 <?php echo isset($_GET['cashier_confirm']) && $_GET['cashier_confirm']==1 ? "active":"";?>' href="<?php echo $LinkCashierConfirm;?>">Thủ Quỹ Đã Chi</a>
        <a class='btn_cancel f_size_14 <?php echo isset($_GET['cashier_confirm']) && $_GET['cashier_confirm']==GasSettle::SETTLE_EXPIRE ? "active":"";?>' href="<?php echo $LinkExpire;?>">Tạm ứng <?php echo $model->getDayExpirySettle();?> ngày chưa quyết toán</a>
        <a class='btn_cancel f_size_14 <?php echo isset($_GET['cashier_confirm']) && $_GET['cashier_confirm']==GasSettle::SETTLE_RENEW ? "active":"";?>' href="<?php echo $LinkRenew;?>">Tạm ứng gia hạn</a>
    </h1> 
</div>