<?php 
$display_none = "display_none";
if($model->type == GasSettle::TYPE_SETTLE_KHONG_TAM_UNG){
    $display_none = "";
}
?>
<div class="type5 <?php echo $display_none;?>">
    <em class="hight_light item_b" style="margin-left: 140px;">Chú ý: Chỉ làm quyết toán này khi bạn không có tạm ứng</em>
    <div class="clr"></div>
    <div class="row">
        <label>&nbsp</label>
        <div>
            <a href="javascript:void(0);" class="text_under_none item_b" style="line-height:25px"
               onclick="fnBuildRow();">
                <img style="float: left;margin-right:8px;"
                     src="<?php echo Yii::app()->theme->baseUrl; ?>/images/add.png">
                Thêm Dòng ( Phím tắt F8 )
            </a>
        </div>
    </div>
    <div class="clr"></div>

<?php $fakeSettleRecord = new GasSettleRecord(); 
    $rowAdd = GasSettle::MAX_SETTLE_ITEMS; 
    $sumPlan    = 0;
    $sumSettle  = 0;
    $listDataDistributor = $model->getListDistributor();
?>
    <div class="row">
        <table class="materials_table hm_table TbItem SettleWrapTable" cellpadding="0" cellspacing="0">
            <thead>
                <tr>
                    <th class="item_c" rowspan="2">#</th>
                    <th class="item_c w-250" rowspan="2"><?php echo $fakeSettleRecord->getAttributeLabel('item_name'); ?></th>
                    <th class="item_c item_border display_none" colspan="4">Dự Toán</th>
                    <th class="item_c item_border" colspan="5">Quyết Toán</th>
                    <th class="item_c w-100" rowspan="2"><?php echo $fakeSettleRecord->getAttributeLabel('note'); ?></th>
                    <th class="last item_c" rowspan="2">Xóa</th>
                </tr>
                <tr>
                    <th class="item_c w-100 display_none"><?php echo $fakeSettleRecord->getAttributeLabel('unit'); ?></th>
                    <th class="item_c w-100 display_none"><?php echo $fakeSettleRecord->getAttributeLabel('qty'); ?></th>
                    <th class="item_c w-100 display_none"><?php echo $fakeSettleRecord->getAttributeLabel('price'); ?></th>
                    <th class="item_c w-100 display_none"><?php echo $fakeSettleRecord->getAttributeLabel('total'); ?></th>

                    <th class="item_c w-100"><?php echo $fakeSettleRecord->getAttributeLabel('payment_no'); ?></th>
                    <th class="item_c w-100"><?php echo $fakeSettleRecord->getAttributeLabel('unit'); ?></th>
                    <th class="item_c w-100"><?php echo $fakeSettleRecord->getAttributeLabel('qty'); ?></th>
                    <th class="item_c w-100"><?php echo $fakeSettleRecord->getAttributeLabel('price'); ?></th>
                    <th class="item_c w-100"><?php echo $fakeSettleRecord->getAttributeLabel('total'); ?></th>
                </tr>
            </thead>
            <tbody>
                <?php include "_form_settle_item_update.php"; ?>
                <?php include "_form_settle_item_default.php"; ?>
            </tbody>
            <tfoot>
                <tr>
                    <td class="item_b item_r f_size_18 h_30" colspan="2">Tổng cộng</td>
                    <td class="plan_grand_total item_b item_r f_size_18 display_none" style="padding-right: 5px;" colspan="4"><?php echo ActiveRecord::formatCurrency($sumPlan); ?></td>
                    <td class="items_grand_total item_b item_r f_size_18" style="padding-right: 5px;" colspan="5"><?php echo ActiveRecord::formatCurrency($sumSettle); ?></td>
                    <td></td><td></td>
                </tr>
            </tfoot>
        </table>
    </div>
</div>

<script>
$(function(){
    handleQtyPriceAmount('plan_qty', 'plan_price', 'plan_amount', "plan_grand_total", "plan_amount_calc", "PlanRowWrapQtyPriceAmount", 'SettleWrapTable', 'SettleAmountFinal');
    handleQtyPriceAmount('items_qty', 'items_price', 'items_amount', "items_grand_total", "plan_amount_calc", "PlanRowWrapQtyPriceAmount", 'SettleWrapTable', 'SettleAmountFinal');
});
    
$(document).keydown(function(e) {
    if(e.which == 119) {
        fnBuildRow();
    }
});


function fnBuildRow(){
    if($('.table_tr_row').size() > <?php echo GasSettle::MAX_SETTLE_ITEMS;?>){
        return ;
    }
    var tr_new = $('.table_tr_row:first').clone();
    tr_new.find('.remove_icon_only').css({'display':'block'});
    tr_new.find('input').val('');
    tr_new.find('textarea').val('');
    tr_new.find('span').text('');

    tr_new.removeClass('selected');
    $('.TbItem tbody').append(tr_new);        
    fnRefreshOrderNumber();
}
    
</script>