<strong class="item_l" style="text-transform: uppercase"><?php echo $model->code_no;?> </strong>
<h2 class="item_c" style="text-transform: uppercase"> Giấy <?php echo $model->getTypeText(); ?>
    <i class="f_size_12">
        </br>
        </br>
        Ngày <?php echo $ObjCreatedDate->format('d'); ?>
        tháng <?php echo $ObjCreatedDate->format('m'); ?>
        năm <?php echo $ObjCreatedDate->format('Y'); ?>
    </i>
</h2>

<p class="item_b"><span class="item_u item_i">Kính gửi:</span> Ban Giám Đốc</p>
<?php
$recordList = $model->rHomeContractDetail;
$fakeHomeContractDetail = new HomeContractDetail();
$fakeHomeContract = new HomeContract();

?>

<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/print-store-card.css"/>
<table cellpadding="0" cellspacing="0" class="tb hm_table">
    <thead>
        <tr>
            <th class="item_c">#</th>
            <th class="item_c "><?php echo $fakeHomeContractDetail->getAttributeLabel('agent_id'); ?></th>
            <th class="item_c "><?php echo $fakeHomeContractDetail->getAttributeLabel('amount'); ?></th>
            <th class="item_c "><?php echo $fakeHomeContractDetail->getAttributeLabel('date_pay'); ?></th>
            <th class="item_c ">Kỳ thanh toán</th>
            <th class="item_c "><?php echo $fakeHomeContract->getAttributeLabel('pay_beneficiary'); ?></th>
            <th class="item_c "><?php echo $fakeHomeContract->getAttributeLabel('pay_numberPhone'); ?></th>
            <?php if ($model->type!=GasSettle::TYPE_REQUEST_MONEY_MULTI ) {?>
                <th class="item_c "><?php echo $fakeHomeContract->getAttributeLabel('pay_bank'); ?></th>
                <th class="item_c "><?php echo $fakeHomeContract->getAttributeLabel('pay_bankNumber'); ?></th>
            <?php } ?>
        </tr>
    </thead>
    <tbody>
    <?php
    // Add existed items
    $current = 0;
    foreach ($recordList as $record) {
        $mHomeContract = $record->rHomeContract;
        $mHomeContract->mapJsonFieldOneDecode('JSON_FIELD', 'type_pay_value', 'baseArrayJsonDecode');
        $dateEndPay = MyFormat::modifyDays($record->date_pay, $record->amount_month_pay, '+', 'month', 'd/m/Y');
        ?>
            <tr>
                <td class="order_no item_c">
                    <?php echo $current + 1; ?>
                </td>
                <td class="">
                    <?php echo $record->getAgent(); ?>
                </td>
                <td class="item_r">
                    <?php echo $record->getAmount(); ?>
                </td>
                <td class="item_c">
                    <?php echo $record->getDatePay(); ?>
                </td>
                <td class="item_c">
                    <?php echo $record->getDatePay()."<br>$dateEndPay"; ?>
                </td>
                <td class="">
                    <?php echo $mHomeContract->getBeneficiary(); ?>
                </td>
                <td class="item_c">
                    <?php echo $mHomeContract->getNumberPhone(); ?>
                </td>
                <?php if ($model->type!=GasSettle::TYPE_REQUEST_MONEY_MULTI ) {?>
                <td class="">
                    <?php echo $mHomeContract->getBank(); ?>
                </td>
                <td class="item_c">
                    <?php echo $mHomeContract->getBankNumber(); ?>
                </td>
                <?php } ?>
                
                <?php if(!$record->isStatusDone()): ?>
                <td class="item_c itemStatus">
                    <a href="<?php echo Yii::app()->createAbsoluteUrl("admin/gasSettle/view", array("id"=>$model->id,"updateHomeContractDetail"=>$record->id) ) ?>">
                        <img src="<?php echo Yii::app()->theme->baseUrl . '/images/dollar.png'; ?>">
                    </a>
                </td>
                <?php else: ?>
                <td class="item_c itemStatus">
                    <img src="<?php echo Yii::app()->theme->baseUrl . '/images/Checked-icon.png'; ?>">
                </td>
                <?php endif; ?>
            </tr>
        <?php
        $current++;
    }
    ?>
    </tbody>
    <tfoot>
        <tr>
            <td class="item_l" colspan="2">TỔNG CỘNG</td>
            <td class="item_r" colspan="1"><?php echo ActiveRecord::formatCurrency($model->amount); ?></td>
            <td></td>
            <td class="item_r" colspan="<?php echo $model->type==GasSettle::TYPE_REQUEST_MONEY_MULTI ? 3:5; ?>"></td>
        </tr>
        <tr>
            <td class="item_l" colspan="2">ĐÃ CHI</td>
            <td class="item_r" colspan="1"><?php echo $model->getTotalPayText('',true); ?></td>
            <td></td>
            <td class="item_r" colspan="<?php echo $model->type==GasSettle::TYPE_REQUEST_MONEY_MULTI ? 3:5; ?>"></td>
        </tr>
    </tfoot>
    
</table>
<script>
    $('.button_print').on('click',function(){
       $('.itemStatus').hide();
    });
</script>
