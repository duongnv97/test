<?php foreach ($model->aDetail as $mDetail): $rowAdd--; ?>
<?php 
    $sumPlan    += $mDetail->getPlanTotal();
    $sumSettle  += $mDetail->getTotal();

?>
<tr class="table_tr_row PlanRowWrapQtyPriceAmount">
    <td class="order_no item_c"></td>
    <td class="item_c">
        <input name="item_name[]" class="seri" type="text" size="25" value="<?php echo $mDetail->getItemName();?>">
    </td>
    <td class="item_c display_none">
        <input name="plan_unit[]" class="w-50" type="text" value="<?php echo $mDetail->getPlanUnit();?>">
    </td>
    <td class="item_c display_none">
        <input name="plan_qty[]" class="w-50 plan_qty " value="<?php echo ActiveRecord::formatNumberInput($mDetail->getPlanQty());?>" maxlength="11">
    </td>
    <td class="item_c display_none">
        <input name="plan_price[]" class="w-80 plan_price item_r " value="<?php echo ActiveRecord::formatNumberInput($mDetail->getPlanPrice());?>" type="number" maxlength="12">
    </td>
    <td class="item_r item_b display_none" >
        <!--<input class=""  name="plan_total[]" value="" type="hidden">-->
        <span class="plan_amount" style="padding-right: 5px;"><?php echo $mDetail->getPlanTotal(true);?></span>
    </td>

    <td class="item_c">
        <input name="payment_no[]" class="w-70" type="text" value="<?php echo $mDetail->getPaymentNo();?>">
    </td>
    <td class="item_c">
        <input name="unit[]" class="w-50" type="text" value="<?php echo $mDetail->getUnit();?>">
    </td>
    <td class="item_c">
        <input name="qty[]" class="w-50 items_qty " value="<?php echo ActiveRecord::formatNumberInput($mDetail->getQty());?>" maxlength="11">
    </td>
    <td class="item_c">
        <input name="price[]" class="w-80 items_price item_r" value="<?php echo ActiveRecord::formatNumberInput($mDetail->getPrice());?>" maxlength="12">
    </td>

    <td class="item_r item_b" >
        <!--<input class="total" value="" type="hidden">-->
        <span class="items_amount" style="padding-right: 5px;"><?php echo $mDetail->getTotal(true);?></span>
    </td>
    <td class="item_c">
        <?php echo CHtml::dropDownList('distributor_id[]', '',
                $listDataDistributor, array('empty'=>'Chọn NPP', 'class' => 'w-200 gSelectSmall display_none', 'options' => array($mDetail->distributor_id => array('selected'=>true))));?>
        <textarea name="note[]" class="w-150" rows="3" ><?php echo $mDetail->note;?></textarea>
    </td>
    <td class="item_c last"><span class="remove_icon_only"></span></td>
</tr>

<?php endforeach; ?>