<?php
$this->breadcrumbs=array(
	$this->singleTitle,
);
$cRole = MyFormat::getCurrentRoleId();
$cUid = MyFormat::getCurrentUid();
$menus=array(
        ['label'=>"Create $this->singleTitle", 'url'=>array('create')],
);
$mCronExcel = new CronExcel();
//$aUidAllow  = [GasConst::UID_DUYEN_NT, GasConst::UID_CHAU_LNM, GasConst::UID_NHAN_NTT, GasConst::UID_NHI_DT, GasConst::UID_THUAN_NH, GasLeave::THUC_NH, GasConst::UID_ADMIN, GasLeave::PHUONG_PTK, GasConst::UID_NGUYEN_DTM, 1162981];
if($mCronExcel->canDownload()):
    $menus[] = ['label'=> 'Xuất Excel Danh Sách Hiện Tại',
                'url'=>array('index', 'ExportExcel'=>1), 
                'htmlOptions'=>array('class'=>'export_excel ','label'=>'Xuất Excel'),
    ];
endif;
$this->menu= ControllerActionsName::createMenusRoles($menus, $actions);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('employee-cashbook-grid', {
                url : $(this).attr('action'),
		data: $(this).serialize()
	});
	return false;
});
");

Yii::app()->clientScript->registerScript('ajaxupdate', "
$('#employee-cashbook-grid a.ajaxupdate').live('click', function() {
    $.fn.yiiGridView.update('employee-cashbook-grid', {
        type: 'POST',
        url: $(this).attr('href'),
        success: function() {
            $.fn.yiiGridView.update('employee-cashbook-grid');
        }
    });
    return false;
});
");
?>
<?php include "index_button.php"; ?>
<h1>Danh Sách <?php echo $this->pluralTitle; ?></h1>
<?php $this->widget('ListCronExcelWidget', array('model'=>$model)); ?>
<?php echo CHtml::link('Tìm Kiếm Nâng Cao','#',array('class'=>'search-button')); ?>
<div class="search-form ">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'employee-cashbook-grid',
	'dataProvider'=>$model->search(),
        'afterAjaxUpdate'=>'function(id, data){ fnUpdateColorbox();}',
        'template'=>'{pager}{summary}{items}{pager}{summary}',
//    'itemsCssClass' => 'items custom_here',
//    'htmlOptions'=> array('class'=>'grid-view custom_here'),
        'pager' => array(
            'maxButtonCount'=>  CmsFormatter::$PAGE_MAX_BUTTON,
        ),
	'enableSorting' => false,
	//'filter'=>$model,
	'columns'=>array(
        array(
            'header' => 'S/N',
            'type' => 'raw',
            'value' => '$this->grid->dataProvider->pagination->currentPage * $this->grid->dataProvider->pagination->pageSize + ($row+1)',
            'headerHtmlOptions' => array('width' => '10px','style' => 'text-align:center;'),
            'htmlOptions' => array('style' => 'text-align:center;')
        ),
            array(
                'name'=>'code_no',
                'type'=>'raw',
                'value'=>'$data->code_no . "<br><br>".$data->getUrlViewOrderApp()',
            ),
            array(
                'name'=>'date_input',
                'type'=>'raw',
                'value'=>'$data->getDateInput()',
            ),
            array(
                'header'=>'Mã NV',
                'type'=>'raw',
                'value'=>'$data->getEmployeeCodeAccount()',
            ),
            array(
                'name'=>'employee_id',
                'type'=>'raw',
                'value'=>'$data->getEmployeeGrid()',
            ),
            array(
                'name'=>'customer_id',
                'type'=>'raw',
                'value'=>'$data->getCustomer("code_bussiness")." - ".$data->getCustomer()',
            ),
            array(
                'name'=>'master_lookup_id',
                'type'=>'raw',
                'value'=>'$data->getMasterLookupText().$data->getFileOnly()',
            ),
//            'lookup_type',
            array(
                'name'=>'amount_schedule',
                'type'=>'raw',
                'value'=>'$data->getAmountSchedule(true)',
                'htmlOptions' => array('style' => 'text-align:right;')
            ),
            array(
                'header'=>'Thu',
                'type'=>'raw',
                'value'=>'$data->getAmountThu()',
                'htmlOptions' => array('style' => 'text-align:right;')
            ),
            array(
                'header'=>'Chi',
                'type'=>'raw',
                'value'=>'$data->getAmountChi()',
                'htmlOptions' => array('style' => 'text-align:right;')
            ),
            array(
                'name'=>'note',
                'type'=>'raw',
                'value'=>'$data->getNoteWebShow(true)',
            ),
            array(
                'name'=>'uid_login',
                'type'=>'raw',
                'value'=>'$data->getUidLogin()',
            ),
            array(
                'name'=>'agent_id',
                'type'=>'raw',
                'value'=>'$data->getAgent()',
            ),
            array(
                'name'=>'created_date',
                'type'=>'raw',
                'value'=>'$data->getCreatedDate()."<br>".$data->getType()',
                'htmlOptions' => array('style' => 'width:100px;')
            ),
            array(
                'name'=>'last_update_time',
                'type'=>'raw',
                'value'=>'$data->getLastUpdateTime()',
                'htmlOptions' => array('style' => 'width:100px;')
            ),
            array(
                'header' => 'Actions',
                'class'=>'CButtonColumn',
                'template'=> ControllerActionsName::createIndexButtonRoles($actions),
                'buttons'=>array(
                    'update' => array(
                        'visible' => '$data->canUpdate()',
                    ),
                    'delete'=>array(
                        'visible'=> 'GasCheck::canDeleteData($data)',
                    ),
                ),
            ),
	),
)); ?>

<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/jquery.floatThead.min.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/underscore-min.js"></script>    
<script>
$(document).ready(function() {
    fnUpdateColorbox();
});

function fnUpdateColorbox(){   
    fixTargetBlank();
    fnAddSumTr();
    $('.items').floatThead();
    $(".gallery").colorbox({iframe:true,innerHeight:'1100', innerWidth: '1000',close: "<span title='close'>close</span>"});
//    $('body').on('click', '.export_excel', function(){
//        $(this).remove();
//    });// Close Jul1018
}


function fnAddSumTr(){
    var tr='';
    var $sumAmountSchedule = $sumAmountThu = $sumAmountChi = 0;
    
    $('.sumAmountSchedule').each(function(){
        if($.trim($(this).text())!='')
            $sumAmountSchedule += parseFloat(''+$(this).text());
    });
    $('.sumAmountThu').each(function(){
        if($.trim($(this).text())!='')
            $sumAmountThu+= parseFloat(''+$(this).text());
    });
    $('.sumAmountChi').each(function(){
        if($.trim($(this).text())!='')
            $sumAmountChi+= parseFloat(''+$(this).text());
    });

    $sumAmountSchedule  = Math.round($sumAmountSchedule * 100) / 100;
    $sumAmountThu       = Math.round($sumAmountThu * 100) / 100;
    $sumAmountChi       = Math.round($sumAmountChi * 100) / 100;
    
    tr +='<tr class="f_size_18 odd sum_page_current">';
        tr +='<td class="item_r item_b" colspan="6">Tổng Cộng</td>';
        tr +='<td class="item_r item_b">'+commaSeparateNumber($sumAmountSchedule)+'</td>';
        tr +='<td class="item_r item_b">'+commaSeparateNumber($sumAmountThu)+'</td>';
        tr +='<td class="item_r item_b">'+commaSeparateNumber($sumAmountChi)+'</td>';
        tr +='<td colspan="6"></td>';
    tr +='</tr>';
    
    if($('.sum_page_current').size()<1){
        $('.items tbody').prepend(tr);
        $('.items tbody').append(tr);
    }
}



</script>