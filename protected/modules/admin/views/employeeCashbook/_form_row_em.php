<?php if($model->showFieldEmployee()): ?>
    <?php if($model->showSelectEmployee()): ?>
        <div class="row">
            <?php echo $form->labelEx($model,'employee_id'); ?>
            <?php echo $form->dropDownList($model,'employee_id', $model->getListdataEmployee(),array('class'=>'w-400', 'empty'=>'Select')); ?>
            <?php echo $form->error($model,'employee_id'); ?>
        </div>

    <?php else: ?>
    <div class="row">
        <?php echo $form->labelEx($model, 'employee_id', array()); ?>
        <?php echo $form->hiddenField($model, 'employee_id'); ?>
        <?php
        $url = Yii::app()->createAbsoluteUrl('admin/ajax/search_user_login', ['PaySalary' => 1]);
        // widget auto complete search user customer and supplier
        $aData = array(
            'model' => $model,
            'field_customer_id' => 'employee_id',
            'url' => $url,
            'name_relation_user' => 'rEmployee',
            'show_role_name' => 1,
            'ClassAdd' => 'w-400',
            'field_autocomplete_name' => 'autocomplete_name2',
        );
        $this->widget('ext.SpaAutocompleteCustomer.SpaAutocompleteCustomer',
            array('data' => $aData));
        ?>
        <?php echo $form->error($model,'employee_id'); ?>
    </div>
    <?php endif; ?>
<?php endif; ?>