<?php
$this->breadcrumbs=array(
    $this->pluralTitle => array('index'),
    ' ' . $this->singleTitle . ' : ',
);

$menus = array(
	array('label'=>"$this->pluralTitle Management", 'url'=>array('index')),
	array('label'=>"Tạo Mới $this->singleTitle", 'url'=>array('create')),
	array('label'=>"Cập Nhật $this->singleTitle", 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>"Xóa $this->singleTitle", 'url'=>array('delete'), 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Bạn chắc chắn muốn xóa ?')),
);
$this->menu= ControllerActionsName::createMenusRoles($menus, $actions);
?>

<h1>Xem: <?php echo $this->singleTitle; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
            'code_no',
            array(
                'name'=>'date_input',
                'type'=>'raw',
                'value'=>$model->getDateInput(),
            ),
            array(
                'name'=>'employee_id',
                'type'=>'raw',
                'value'=>$model->getEmployeeGrid(),
            ),
            array(
                'name'=>'customer_id',
                'type'=>'raw',
                'value'=>$model->getCustomer("code_bussiness")." - ".$model->getCustomer(),
            ),
            array(
                'name'=>'master_lookup_id',
                'type'=>'raw',
                'value'=>$model->getMasterLookupText().$model->getFileOnly(),
            ),
//            'lookup_type',
            array(
                'name'=>'amount_schedule',
                'type'=>'raw',
                'value'=>$model->getAmountSchedule(true),
                
            ),
            array(
                'label'=>'Thu',
                'type'=>'raw',
                'value'=>$model->getAmountThu(),
                
            ),
            array(
                'label'=>'Chi',
                'type'=>'raw',
                'value'=>$model->getAmountChi(),
                'htmlOptions' => array('style' => 'text-align:right;')
            ),
            array(
                'name'=>'note',
                'type'=>'raw',
                'value'=>$model->getNoteWebShow(true),
            ),
            array(
                'name'=>'uid_login',
                'type'=>'raw',
                'value'=>$model->getUidLogin(),
            ),
            array(
                'name'=>'agent_id',
                'type'=>'raw',
                'value'=>$model->getAgent(),
            ),
            array(
                'name'=>'created_date',
                'type'=>'raw',
                'value'=>$model->getCreatedDate('d/m/Y H:i:s')."<br>".$model->getType(),
                
            ),
            array(
                'name'=>'last_update_time',
                'type'=>'raw',
                'value'=>$model->getLastUpdateTime(),
                
            ),
            array(
                'name'=>'last_update_by',
                'type'=>'raw',
                'value'=>$model->getLastUpdateBy(),
                
            ),
	),
)); ?>
