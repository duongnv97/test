<div class="row">
<?php echo $form->labelEx($model,'customer_id'); ?>
<?php echo $form->hiddenField($model,'customer_id', array('class'=>'')); ?>
<?php
    // 1. limit search kh của sale
    $url = Yii::app()->createAbsoluteUrl('admin/ajax/search_user_by_code');
    // widget auto complete search user customer and supplier
    $aData = array(
        'model'=>$model,
        'field_customer_id'=>'customer_id',
        'url'=> $url,
        'name_relation_user'=>'rCustomer',
        'ClassAdd' => 'w-400',
        'field_autocomplete_name' => 'autocomplete_name1',
    );
    $this->widget('ext.SpaAutocompleteCustomer.SpaAutocompleteCustomer',
        array('data'=>$aData));
    ?>
    <?php echo $form->error($model,'customer_id'); ?>
</div>

<?php if($model->showFieldAmount()): ?>
<div class="row f_size_18 ">
    <?php echo $form->labelEx($model,'amount'); ?>
    <?php echo $form->textField($model,'amount',array('class'=>'w-400 ad_fix_currency f_size_18','maxlength'=>12)); ?>
    <?php echo $form->error($model,'amount'); ?>
</div>
<?php endif; ?>
<?php if($model->showFieldAmountSchedule()): ?>
<div class="row f_size_18">
        <?php echo $form->labelEx($model,'amount_schedule'); ?>
    <?php echo $form->textField($model,'amount_schedule',array('class'=>'w-400 ad_fix_currency f_size_18','maxlength'=>12)); ?>
    <?php echo $form->error($model,'amount_schedule'); ?>
</div>
<?php endif; ?>