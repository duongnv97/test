<?php 
    $cRole = MyFormat::getCurrentRoleId();
    $aRoleCheck = [ROLE_ADMIN];
    $display_none = 'display_none';
    $display_none = '';
    if(in_array($cRole, $aRoleCheck)){
//    if($model->isNewRecord){
        $display_none = '';
    }

?>
<div class="form ">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'employee-cashbook-form',
	'enableAjaxValidation'=>false,
        'htmlOptions' => array('enctype' => 'multipart/form-data'),
)); ?>
    <?php echo $form->errorSummary($model); ?>
    <p class="note">Dòng có dấu <span class="required">*</span> là bắt buộc.</p>
    <?php echo MyFormat::BindNotifyMsg(); ?>
    <div class="row buttons" style="padding-left: 141px;">
        <?php $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType'=>'submit',
            'label'=>$model->isNewRecord ? 'Create' :'Save',
            'type'=>'null', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
            'size'=>'small', // null, 'large', 'small' or 'mini'
            //'htmlOptions' => array('style' => 'margin-bottom: 10px; float: right;'),
        )); ?>	
    </div>
    
    <?php if($model->showFieldType()): ?>
    <div class="row GasCheckboxList" style=''>
        <?php echo $form->labelEx($model,'type'); ?>
        <?php echo $form->radioButtonList($model,'type', $model->getArrayTypeCreate(), 
                    array(
                        'separator'=>"",
                        'template'=>'<li>{input}{label}</li>',
                        'container'=>'ul',
                        'class'=>'RadioOrderType' 
                    )); 
        ?>
        <?php echo $form->error($model,'type'); ?>
    </div>
    <div class='clr'></div>
    <?php endif; ?>
    
    <div class="row">
        <?php echo $form->labelEx($model,'date_input'); ?>
        <?php 
            $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                'model'=>$model,        
                'attribute'=>'date_input',
                'options'=>array(
                    'showAnim'=>'fold',
                    'dateFormat'=> ActiveRecord::getDateFormatJquery(),
                    'minDate'=> $model->getDayLimit(),
//                    'maxDate'=> '0',
                    'changeMonth' => true,
                    'changeYear' => true,
                    'showOn' => 'button',
                    'buttonImage'=> Yii::app()->theme->baseUrl.'/admin/images/icon_calendar_r.gif',
                    'buttonImageOnly'=> true,                                
                ),        
                'htmlOptions'=>array(
                    'class'=>'w-16',
                    'style'=>'height:20px;',
                        'readonly'=>'readonly',
                ),
            ));
        ?>     		
        <?php echo $form->error($model,'date_input'); ?>
    </div>
    
    <?php if($model->showFieldBank()): ?>
    <div class="row">
        <?php echo $form->labelEx($model,'bank_id'); ?>
        <?php echo $form->dropDownList($model,'bank_id', $model->getListdataBank(),array('class'=>'w-400', 'empty'=>'Select')); ?>
        <?php echo $form->error($model,'bank_id'); ?>
    </div>
    <?php endif; ?>
    
    <?php if($model->showFieldAgent()): ?>
    <div class="row f_size_18">
    <?php echo $form->labelEx($model,'agent_id'); ?>
    <?php echo $form->hiddenField($model,'agent_id', array('class'=>'')); ?>
    <?php
            // 1. limit search kh của sale
//            $url = Yii::app()->createAbsoluteUrl('admin/ajax/searchAgentUserLogin');
            $url = Yii::app()->createAbsoluteUrl('admin/ajax/search_by_role', array('role'=> ROLE_AGENT));
            // widget auto complete search user customer and supplier
            $aData = array(
                'model'=>$model,
                'field_customer_id'=>'agent_id',
                'url'=> $url,
                'name_relation_user'=>'rAgent',
                'ClassAdd' => 'w-400',
                'field_autocomplete_name' => 'autocomplete_name',
                'placeholder'=>'Nhập mã hoặc tên đại lý',
            );
            $this->widget('ext.SpaAutocompleteCustomer.SpaAutocompleteCustomer',
                array('data'=>$aData));
            ?>
        <?php echo $form->error($model,'agent_id'); ?>
    </div>
    <?php endif; ?>
    
    <div class="row f_size_18 <?php echo $display_none;?>">
        <?php echo $form->labelEx($model,'master_lookup_id'); ?>
        <?php echo $form->dropDownList($model,'master_lookup_id', $model->getListdataLookupType(),array('class'=>'w-400', 'empty'=>'Select')); ?>
        <?php echo $form->error($model,'master_lookup_id'); ?>
    </div>
    
    <?php if(!$model->isNewRecord): ?>
        <?php include '_form_customer.php'; ?>
    <?php endif; ?>

    <?php include '_form_row_em.php'; ?>
    <?php // if($model->showMultiInput()): ?>
    <?php if($model->isNewRecord): ?>
        <?php include '_form_customer_multi.php'; ?>
    <?php endif; ?>

    <?php if(!$model->isNewRecord): ?>
    <div class="row">
        <?php echo $form->labelEx($model,'note'); ?>
        <?php echo $form->textArea($model,'note',array('rows'=>5, 'cols'=>100)); ?>
        <?php echo $form->error($model,'note'); ?>
    </div>
    <?php endif; ?>
    <?php $aModelFile = GasFile::getAllFile($model, GasFile::TYPE_8_CASHBOOK); ?>
    <?php include "_upload_multi.php"; ?>
    
    <div class="row buttons" style="padding-left: 141px;">
        <?php $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType'=>'submit',
            'label'=>$model->isNewRecord ? 'Create' :'Save',
            'type'=>'null', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
            'size'=>'small', // null, 'large', 'small' or 'mini'
            //'htmlOptions' => array('style' => 'margin-bottom: 10px; float: right;'),
        )); ?>	
    </div>

<?php $this->endWidget(); ?>

</div><!-- form -->
<script>
    $(document).ready(function(){
        $('.form').find('button:submit').click(function(){
            $.blockUI({ overlayCSS: { backgroundColor: '#fff' } }); 
        });
        
    });
</script>

<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/autoNumeric-master/autoNumeric.js"></script>
<script>
    $(document).ready(function(){
        fnInitInputCurrency();
        bindRadioOrderType();
        fnBindRemoveIcon();
    });
   
   function bindRadioOrderType(){
        $('.RadioOrderType').click(function(){
            $.blockUI({ overlayCSS: { backgroundColor: '#fff' } }); 
            $(this).closest('form').submit();
        });
    }
    
</script>