<?php 
    $lb = 'Phải thu số tiền';
    if($model->type != EmployeeCashbook::TYPE_SCHEDULE){
        $lb = 'Nhập tiền đã thu';
    }
?>
<?php if($model->canCreateMulti()): ?>
<div class="clr"></div>
    <div class="row">
        <label>&nbsp</label>
        <div class="float_l">
            <a href="javascript:void(0);" style="line-height:25px"  class="text_under_none item_b" onclick="fnBuildRow();">
                <img style="float: left;margin-right:8px;" src="<?php echo Yii::app()->theme->baseUrl;?>/images/add.png"> 
                Thêm Dòng ( Phím tắt F8 )
            </a>
            <!--<br><em class="hight_light item_b">Chú Ý: Giá khác bình bò phải là giá KG, giá khác bình 12 có thể là giá KG hoặc giá bình </em>-->
        </div>
    </div>
<div class="clr"></div>
<?php endif; ?>

<div class="row">
    <label>&nbsp</label>
    <table class="materials_table materials_table_root materials_table_th hm_table f_size_15" style="">
        <thead>
            <tr>
                <th class="item_c w-20">#</th>
                <th class="w-350 item_c">Khách Hàng</th>
                <th class="w-30 item_c"><?php echo $lb;?></th>
                <th class="w-100 item_c">Ghi chú</th>
                <th class="item_c last">Xóa</th>
            </tr>
        </thead>
        <tbody>
            <tr class="cash_book_row row_input">
                <td class="item_c order_no">1</td>
                <td class="col_customer">
                    <div class="row row_customer_autocomplete">
                        <div class="float_l">
                            <input class="customer_id_hide" value="" type="hidden" name="customer_id[]">
                            <input class="float_l customer_autocomplete w-250"  placeholder="Nhập tên số đt, từ <?php echo MIN_LENGTH_AUTOCOMPLETE; ?> ký tự" maxlength="100" value="" type="text" >
                            <span class="remove_row_item" onclick="fnRemoveNameHand(this);"></span>
                        </div>
                    </div>
                </td>
                <td><input name="list_amount[]" class="w-120 f_size_15 list_amount ad_fix_currency item_r" maxlength="14" value="" type="text" placeholder="<?php echo $lb;?>"></td>
                <td><input name="list_note[]" class="w-300 list_note" type="text" placeholder="Ghi chú"></td>
                <td class="item_c last"><span remove="" class="remove_icon_only"></span></td>
            </tr>
        </tbody>
    </table>
</div>

<div class="clr"></div>

<?php Yii::app()->clientScript->registerCoreScript('jquery.ui'); ?>
<script>
    $(document).keydown(function(e) {
        if(e.which == 119) {
            fnBuildRow();
        }
    });
    
    // Jul 21, 2016 Begin for table multi customer
    $(document).ready(function(){
        $('.form').find('button:submit').live('click', function(){
            $.blockUI({ overlayCSS: { backgroundColor: '#fff' } }); 
        });
        fnBindAllAutocomplete();
        fnBindRemoveIcon();
    });

    function fnBindAllAutocomplete(){
        $('.customer_autocomplete').each(function(){
            fnBindAutocomplete($(this));
        });
    }
    
    function fnBuildRow(){
        if($('.cash_book_row').size() > 100){
            return ;
        }
        var trCopy = $('.cash_book_row:first').clone();
        trCopy.removeClass('selected');
        trCopy.find('.list_amount').val("");
        trCopy.find('.list_note').val("");
        var spanRemoveCustomer = trCopy.find('.remove_row_item');
        fnRemoveNameHand(spanRemoveCustomer);
        
        $('.materials_table_root').append(trCopy);
        fnRefreshOrderNumber();
        fnBindAllAutocomplete();
        fnInitInputCurrency();
    }
    
    // to do bind autocompelte for input
    // @param objInput : is obj input ex  $('.customer_autocomplete')
    function fnBindAutocomplete(objInput){
        var parent_div = objInput.closest('td.col_customer');
        objInput.autocomplete({
            source: '<?php echo Yii::app()->createAbsoluteUrl('admin/ajax/search_user_by_code');?>',
            minLength: '<?php echo MIN_LENGTH_AUTOCOMPLETE;?>',
            close: function( event, ui ) { 
                //$( "#GasStoreCard_materials_name" ).val(''); 
            },
            search: function( event, ui ) { 
                    objInput.addClass('grid-view-loading-gas');
            },
            response: function( event, ui ) { 
                objInput.removeClass('grid-view-loading-gas');
                var json = $.map(ui, function (value, key) { return value; });
                if(json.length<1){
                    var error = '<div class=\'errorMessage clr autocomplete_name_text\'>Không tìm thấy dữ liệu.</div>';
                    if(parent_div.find('.autocomplete_name_text').size()<1){
                        parent_div.find('.remove_row_item').after(error);
                    }
                    else
                        parent_div.find('.autocomplete_name_text').show();
                }                    
                    
            },
            select: function( event, ui ) {
                objInput.attr('readonly',true);
                parent_div.find('.autocomplete_name_text').hide();   
                parent_div.find('.customer_id_hide').val(ui.item.id);   
                parent_div.find('.autocomplete_customer_info').remove();   
                var tableInfo = fnBuildTableCustomerInfo(ui.item.code_bussiness, ui.item.name_customer, ui.item.address, ui.item.phone);
                parent_div.append(tableInfo);
                parent_div.find('.autocomplete_customer_info').show();
            }

        });
    }
    
    function fnBuildTableCustomerInfo(info_code_bussiness, info_name, info_address, info_phone){
        var table = '';
        table += '<div class="autocomplete_customer_info table_small" style="">';
            table += '<table>';
                table += '<tr>';
                    table += '<td class="_l td_first_t">Mã KH:</td>';
                    table += '<td class="_r info_code_bussiness td_last_r td_first_t">'+info_code_bussiness+'</td>';
                table += '</tr>';

                table += '<tr>';
                    table += '<td class="_l">Tên KH:</td>';
                    table += '<td class="_r info_name td_last_r">'+info_name+'</td>';
                table += '</tr>';
                table += '<tr>';
                    table += '<td class="_l">Địa chỉ:</td>';
                    table += '<td class="_r info_address td_last_r">'+info_address+'</td>';
                table += '</tr>';
                table += '<tr>';
                    table += '<td class="_l">Điện Thoại:</td>';
                    table += '<td class="_r info_phone td_last_r">'+info_phone+'</td>';
                table += '</tr>';
            table += '</table>';
        table += '</div>';
        table += '<div class="clr"></div';
        return table;
    }
    
    function fnRemoveNameHand(this_){
        var tr = $(this_).closest('tr.cash_book_row');
        tr.find('.customer_autocomplete').attr("readonly",false);
        tr.find('.customer_autocomplete').val("");
        tr.find('.customer_id_hide').val("");
        tr.find('.autocomplete_customer_info').hide();
    }
    
    // Jul 21, 2016 end for table multi customer

    
</script>
    
    