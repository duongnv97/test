<div class="form">
    <?php
        $LinkNormal     = Yii::app()->createAbsoluteUrl('admin/employeeCashbook/index');
        $LinkSchedule   = Yii::app()->createAbsoluteUrl('admin/employeeCashbook/index', array('type'=> EmployeeCashbook::TYPE_SCHEDULE));
        $LinkDaily      = Yii::app()->createAbsoluteUrl('admin/employeeCashbook/index', array('type'=> EmployeeCashbook::TYPE_DAILY));
        $LinkBankTransfer       = Yii::app()->createAbsoluteUrl('admin/employeeCashbook/index', array('type'=> EmployeeCashbook::TYPE_BANK_TRANSFER));
        $LinkAll        = Yii::app()->createAbsoluteUrl('admin/employeeCashbook/index', array('type'=> EmployeeCashbook::TYPE_ALL));
    ?>
    <h1><?php echo $this->adminPageTitle;?>
        <a class='btn_cancel f_size_14 <?php echo (!isset($_GET['type'])) ? "active":"";?>' href="<?php echo $LinkNormal;?>">Lịch Thu</a>
        <a class='btn_cancel f_size_14 <?php echo isset($_GET['type']) && $_GET['type']== EmployeeCashbook::TYPE_SCHEDULE ? "active":"";?>' href="<?php echo $LinkSchedule;?>">Thu Theo Lịch</a>
        <a class='btn_cancel f_size_14 <?php echo isset($_GET['type']) && $_GET['type']== EmployeeCashbook::TYPE_BANK_TRANSFER ? "active":"";?>' href="<?php echo $LinkBankTransfer;?>">Chuyển khoản</a>
        <a class='btn_cancel f_size_14 <?php echo isset($_GET['type']) && $_GET['type']== EmployeeCashbook::TYPE_DAILY ? "active":"";?>' href="<?php echo $LinkDaily;?>">Thu Hàng Ngày</a>
        <a class='btn_cancel f_size_14 <?php echo isset($_GET['type']) && $_GET['type']== EmployeeCashbook::TYPE_ALL ? "active":"";?>' href="<?php echo $LinkAll;?>">Toàn Bộ</a>
    </h1> 
</div>