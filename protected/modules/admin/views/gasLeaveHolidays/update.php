<?php
$this->breadcrumbs=array(
	'Quản Lý Ngày Nghỉ Lễ'=>array('index'),
	$model->name=>array('view','id'=>$model->id),
	'Cập Nhật',
);

$menus = array(	
        array('label'=>'Quản Lý Ngày Nghỉ Lễ', 'url'=>array('index')),
	array('label'=>'Xem Ngày Nghỉ Lễ', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Tạo Mới Ngày Nghỉ Lễ', 'url'=>array('create')),	
);
$this->menu= ControllerActionsName::createMenusRoles($menus, $actions);
?>

<?php if($this->layout == "ajax"): ?>
<h1 style='display:inline-block'>Cập nhật ngày nghỉ lễ: <?php echo $model->name.' - '.$model->date; ?></h1><br>
    <button style='background: #ff0000;color: white;border: none;padding: 5px 10px;border-radius: 5px;font-weight: bold;cursor: pointer;' id='btnDelete'>Xóa</button>
<?php endif; ?>
    
<h1>Cập nhật ngày nghỉ lễ</h1>
    
<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>
<script>
    $('#btnDelete').on('click',function(){
        if(confirm('Bạn chắc chắn muốn xóa ?')) {
            $.ajax({ 
                url: '<?php echo Yii::app()->createUrl('/admin/gasLeaveHolidays/delete',array('id'=>$model->id))?>',
                type: "POST",
                success: function(){
                    parent.$.fn.colorbox.close();
                },
            });
        }
    });
</script>