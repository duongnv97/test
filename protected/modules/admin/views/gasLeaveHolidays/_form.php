<?php 
$fakeLeaveHolidaysType = new HrHolidayTypes();
?>
<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'gas-leave-holidays-form',
	'enableAjaxValidation'=>false,
)); 
$maxDate = '';
if($model->scenario == 'update'){ // Update
    if(!empty($_GET['id'])){
        $m = GasLeaveHolidays::model()->findByPk($_GET['id']);
        if(!empty($m)){
            $maxDate = MyFormat::dateConverYmdToDmy($m->date);
        }
    }
} else { // Create
    if(!empty($_GET['date'])){
        $maxDate = MyFormat::dateConverYmdToDmy($_GET['date']);
    }
}


?>

	<p class="note">Dòng có dấu <span class="required">*</span> là bắt buộc.</p>
        
        <?php  if(Yii::app()->user->hasFlash('successUpdate')): ?>
            <div class='success_div'><?php echo Yii::app()->user->getFlash('successUpdate');?></div>         
        <?php endif; ?>  	
            	
	<div class="row">
		<?php echo $form->labelEx($model,'name'); ?>
		<?php echo $form->textField($model,'name',array('size'=>30,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'name'); ?>
	</div>
            
        <?php if($this->layout != 'ajax'): ?>
            <div class="row">
                <?php echo $form->labelEx($model,'date'); ?>
                <?php 
                    $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                        'model'=>$model,        
                        'attribute'=>'date',
                        'options'=>array(
                            'showAnim'=>'fold',
                            'dateFormat'=> ActiveRecord::getDateFormatJquery(),
                            'changeMonth' => true,
                            'changeYear' => true,
                            'showOn' => 'button',
                            'buttonImage'=> Yii::app()->theme->baseUrl.'/admin/images/icon_calendar_r.gif',
                            'buttonImageOnly'=> true,                                
                        ),        
                        'htmlOptions'=>array(
                            'class'=>'w-16',
                            'style'=>'height:20px;',
                                'readonly'=>'readonly',
                        ),
                    ));
                ?>  
                <?php echo $form->error($model,'date'); ?>
            </div>
        <?php endif; ?>
            
        <?php if(empty($_GET['date']) && empty($_GET['id']) && $this->layout == 'ajax'){ ?>
            <div class="row col1">
                <?php echo $form->labelEx($model,'date_create_from'); ?>
                <?php 
                    $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                        'model'=>$model,        
                        'attribute'=>'date_create_from',
                        'options'=>array(
                            'showAnim'=>'fold',
                            'dateFormat'=> ActiveRecord::getDateFormatJquery(),
                            'minDate'=> empty($_GET['GasLeaveHolidays']['year']) ? date('01/01/Y', strtotime('+1 year')) : '01/01/'.$_GET['GasLeaveHolidays']['year'],
//                            'maxDate'=> '0',
                            'changeMonth' => true,
                            'changeYear' => true,
                            'showOn' => 'button',
                            'buttonImage'=> Yii::app()->theme->baseUrl.'/admin/images/icon_calendar_r.gif',
                            'buttonImageOnly'=> true,                                
                        ),        
                        'htmlOptions'=>array(
                            'class'=>'w-16',
                            'style'=>'height:20px;',
                                'readonly'=>'readonly',
                        ),
                    ));
                ?>  
                <?php echo $form->error($model,'date_create_from'); ?>
            </div>
            <div class="row col2">
                <?php echo $form->labelEx($model,'date_create_to'); ?>
                <?php 
                    $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                        'model'=>$model,        
                        'attribute'=>'date_create_to',
                        'options'=>array(
                            'showAnim'=>'fold',
                            'dateFormat'=> ActiveRecord::getDateFormatJquery(),
                            'minDate'=> empty($_GET['GasLeaveHolidays']['year']) ? date('01/01/Y', strtotime('+1 year')) : '01/01/'.$_GET['GasLeaveHolidays']['year'],
//                            'maxDate'=> '0',
                            'changeMonth' => true,
                            'changeYear' => true,
                            'showOn' => 'button',
                            'buttonImage'=> Yii::app()->theme->baseUrl.'/admin/images/icon_calendar_r.gif',
                            'buttonImageOnly'=> true,                                
                        ),        
                        'htmlOptions'=>array(
                            'class'=>'w-16',
                            'style'=>'height:20px;',
                                'readonly'=>'readonly',
                        ),
                    ));
                ?>  
                <?php echo $form->error($model,'date_create_to'); ?>
            </div>
        <?php } ?>
            
        <div class="row">
            <?php echo $form->labelEx($model,'type_id'); ?>
            <?php echo $form->dropDownList($model,'type_id', $fakeLeaveHolidaysType->getArrayType(),array('empty'=>'Select','class'=>'w-250')); ?>
            <?php echo $form->error($model,'type_id'); ?>
        </div>
            
            <div class="row compensatory_date_picker" style="display: none">
            <?php echo $form->labelEx($model,'compensatory_leave_date'); ?>
            <?php 
                $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                    'model'=>$model,        
                    'attribute'=>'compensatory_leave_date',
                    'options'=>array(
                        'showAnim'=>'fold',
                        'dateFormat'=> ActiveRecord::getDateFormatJquery(),
//                        'maxDate'=> $maxDate,
                        'changeMonth' => true,
                        'changeYear' => true,
                        'showOn' => 'button',
                        'buttonImage'=> Yii::app()->theme->baseUrl.'/admin/images/icon_calendar_r.gif',
                        'buttonImageOnly'=> true,                                
                    ),        
                    'htmlOptions'=>array(
                        'class'=>'w-16',
                        'style'=>'height:20px;',
                        'readonly'=>'readonly',
                    ),
                ));
            ?>  
            <?php echo $form->error($model,'compensatory_leave_date'); ?>
        </div>
            
<!--        <div class="row">
            <?php // echo $form->labelEx($model,'description'); ?>
            <?php // echo $form->textArea($model,'description',array('style'=>'width:250px;height:40px;')); ?>
            <?php // echo $form->error($model,'description'); ?>
        </div>-->
            
	<div class="row buttons" style="padding-left: 141px;">
                    <?php $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType'=>'submit',
            'label'=>$model->isNewRecord ? 'Create' :'Save',
            'type'=>'null', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
            'size'=>'small', // null, 'large', 'small' or 'mini'
            //'htmlOptions' => array('style' => 'margin-bottom: 10px; float: right;'),
        )); ?>	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->
<script>
    $(document).ready(function(){
        $('.form').find('button:submit').click(function(){
            $.blockUI({ overlayCSS: { backgroundColor: '#fff' } }); 
        });
        //handleSelectType();
        
        // Nếu là ngày nghỉ bù thì display input nghỉ bù của ngày nào
        <?php 
        $mHolidayTypes = HrHolidayTypes::model()->findAll();
        foreach ($mHolidayTypes as $holiday_type) {
            if(strtolower($holiday_type->name) == 'nghỉ bù'){
                echo "compensatory_type_id = '{$holiday_type->id}';"; break;
            }
        }
        ?>
        toggleCompensatoryDatePicker($('#GasLeaveHolidays_type_id'));
    });
    function handleSelectType(){
        $('#GasLeaveHolidays_type_id').on('change',function(){
           $('#GasLeaveHolidays_name').val($('#GasLeaveHolidays_type_id').find("option:selected").text()); 
        });
    }
    function toggleCompensatoryDatePicker(selectBoxElm){
        if(selectBoxElm.val() == compensatory_type_id){
            $('.compensatory_date_picker').show();
        } else {
            $('.compensatory_date_picker').hide();
            $('.compensatory_date_picker input[name="GasLeaveHolidays[compensatory_leave_date]"]').val('');
        }
    }

    // Nếu là ngày nghỉ bù thì display input nghỉ bù của ngày nào, event on change
    $('#GasLeaveHolidays_type_id').on('change', function(){
        toggleCompensatoryDatePicker($(this));
    });
</script>