
<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>
	<div class="row">
            <?php echo $form->labelEx($model,'name'); ?>
            <?php echo $form->textField($model,'name', array('class'=>'w-300')); ?>
        </div>
        
    <div class="row more_col">
        <div class="col1">
            <?php echo $form->labelEx($model, 'date_from'); ?>
            <?php 
            $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                'model' => $model,
                'attribute' => 'date_from',
                'options' => array(
                    'showAnim' => 'fold',
                    'dateFormat' => MyFormat::$dateFormatSearch,
                    'changeMonth' => true,
                    'changeYear' => true,
                    'showOn' => 'button',
                    'buttonImage' => Yii::app()->theme->baseUrl . '/admin/images/icon_calendar_r.gif',
                    'buttonImageOnly' => true,
                ),
                'htmlOptions' => array(
                    'class' => 'w-16',
                    'style' => 'height:20px;',
                ),
            ));
            ?> 
        </div>
        <div class="col2">
            <?php echo $form->labelEx($model, 'date_to'); ?>
            <?php 
            $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                'model' => $model,
                'attribute' => 'date_to',
                'options' => array(
                    'showAnim' => 'fold',
                    'dateFormat' => MyFormat::$dateFormatSearch,
                    'changeMonth' => true,
                    'changeYear' => true,
                    'showOn' => 'button',
                    'buttonImage' => Yii::app()->theme->baseUrl . '/admin/images/icon_calendar_r.gif',
                    'buttonImageOnly' => true,
                ),
                'htmlOptions' => array(
                    'class' => 'w-16',
                    'style' => 'height:20px;',
                ),
            ));
            ?> 
        </div>
    </div>
        
        <div class="row">
            <?php echo $form->labelEx($model,'type_id'); ?>
            <?php echo $form->dropDownList($model,'type_id', $model->getArrayType(),array('empty'=>'Select','class'=>'w-300')); ?>
        </div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->