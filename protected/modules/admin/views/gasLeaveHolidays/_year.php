<div class="wide form">
<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl('admin/GasLeaveHolidays/index/type') . "/" .GasLeaveHolidays::TAB_UI,
	'method'=>'get',
        'id' => 'form-search',
)); ?>
        <div class="row" style="text-align: center;">
            <?php echo $form->dropDownList($model,'year', $model->getArrayYear(),array('class'=>'w-250')); ?>
        </div>
<?php $this->endWidget(); ?>
</div><!-- search-form -->
<script>
$('#GasLeaveHolidays_year').on('change',function(){
    $('#form-search').submit();
});
</script>