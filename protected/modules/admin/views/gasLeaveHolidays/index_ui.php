<?php
$this->breadcrumbs=array(
	'Quản Lý Ngày Nghỉ Lễ',
);
Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('gas-leave-holidays-grid', {
                url : $(this).attr('action'),
		data: $(this).serialize()
	});
	return false;
});
");

Yii::app()->clientScript->registerScript('ajaxupdate', "
$('#gas-leave-holidays-grid a.ajaxupdate').live('click', function() {
    $.fn.yiiGridView.update('gas-leave-holidays-grid', {
        type: 'POST',
        url: $(this).attr('href'),
        success: function() {
            $.fn.yiiGridView.update('gas-leave-holidays-grid');
        }
    });
    return false;
});
");

?>

<?php $this->renderPartial('leaveHolidaysPlan/_form_plan', array('model' => $modelPlan));
?>
<?php 
    $this->renderPartial('_year', array(
            'model' => $model,
            'modelPlan' => $modelPlan,
    )); 
?>
<?php 
    $this->renderPartial('_print', array(
            'model' => $model,
            'modelPlan' => $modelPlan,
    )); 
?>
<div id="calendar">

<?php 
/** @Author: HOANG NAM 30/month/2018
*  @Todo: build calender
*  @Param: 
**/
//echo $model->buildHtmlMonthCalendar($model->year,array(1,2,3,4,5,6,7,8,9,10,11,12));
?>
</div>
<script>
    var year = $('#GasLeaveHolidays_year').val();
    $('#GasLeaveHolidays_year').on('change',function(){
        year = $('#GasLeaveHolidays_year').val();
    });
    $('head').append('<link href="<?php echo Yii::app()->theme->baseUrl ?>/admin/css/calendar.css" type="text/css" rel="stylesheet">');
    $('body').on('click', '.date', function(){
        $(" .date").colorbox({
            iframe: true,
            overlayClose: false, escKey: false,
            innerHeight: '400',
            innerWidth: '500', close: "<span title='close'>close</span>",
            onClosed: function () { // update view when close colorbox
               getDataAjax(year);
            }
        });
    });
    $('body').on('click', '.create a', function(){
        $(" .create a").colorbox({
            iframe: true,
            overlayClose: false, escKey: false,
            innerHeight: '400',
            innerWidth: '500', close: "<span title='close'>close</span>",
            onClosed: function () { // update view when close colorbox
               getDataAjax(year);
            }
        });
    });
    
$(function(e){
    getDataAjax(year);
    jQuery('body').on('click','.tabLink',function(e){
        getDataAjax(year);
        //return false;
    });
})
function getDataAjax(year){
    jQuery.ajax({
        'url':'<?php echo Yii::app()->createAbsoluteUrl('/admin/GasLeaveHolidays/UpdateAjax') ?>'+'/year/'+year,
        'cache':false,
        'success':function(html){
            jQuery("#calendar").html(html);
        }
    });
}
</script>