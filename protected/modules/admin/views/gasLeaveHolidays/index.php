<?php
$this->breadcrumbs=array(
	"Lịch nghỉ lễ",
);

$menus=array(
    array('label'=>"Tạo lịch nghỉ lễ", 'url'=>array('create')),
);

$this->menu= ControllerActionsName::createMenusRoles($menus, $actions);
Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('GasLeaveHolidays-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>
<?php echo MyFormat::BindNotifyMsg(); ?>

<?php echo CHtml::link('Tìm Kiếm Nâng Cao','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
       'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'GasLeaveHolidays-grid',
	'dataProvider'=>$model->search(),
        'template'=>'{pager}{summary}{items}{pager}{summary}',
        'pager' => array(
            'maxButtonCount'=>  CmsFormatter::$PAGE_MAX_BUTTON,
        ),
	'columns'=>array(
            array(
                'header' => 'S/N',
                'type' => 'raw',
                'value' => '$this->grid->dataProvider->pagination->currentPage * $this->grid->dataProvider->pagination->pageSize + ($row+1)',
                'headerHtmlOptions' => array('width' => '10px','style' => 'text-align:center;'),
                'htmlOptions' => array('style' => 'text-align:center;')
            ),
            array(
                'name'=>'name',
                'type'=>'raw',
                'value'=>'$data->getName()',
            ),
            array(
                'name'=>'date',
                'type'=>'raw',
                'value'=>'$data->getDate()',
            ),
            array(
                'name'=>'type_id',
                'type'=>'raw',
                'value'=>'$data->getType()',
            ),
            array(
                'name'=>'compensatory_leave_date',
                'type'=>'raw',
                'value'=>'$data->getCompensatoryDate()',
            ),
            
            array(
            'header' => 'Actions',
            'class' => 'CButtonColumn',
            'template' => ControllerActionsName::createIndexButtonRoles($actions),
            'buttons' => array(
                
                ),
            ),
	),
)); ?>