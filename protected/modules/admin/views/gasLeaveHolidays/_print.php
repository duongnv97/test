<div style="text-align: right;">
    
    <!--only display create button when year > current year-->
    <?php 
    $year = isset($_GET['GasLeaveHolidays']['year'])?$_GET['GasLeaveHolidays']['year']:date('Y');
    $style = '';
    if($year <= date('Y')) $style = 'display: none;';
    ?>
    <ul class="operations" id="yw2" style="<?php echo $style; ?>">
        <li class="create" title="Tạo mới ngày nghỉ lễ">
            <a href="<?php echo yii::app()->createAbsoluteUrl('admin/GasLeaveHolidays/CreateMultiHolidays') ?>" class="cboxElement">
                <span style="margin-left: -32px">Tạo Mới</span>
            </a>
        </li>
    </ul>

    <a class="button_print r_padding_20" target="_blank" href="<?php echo yii::app()->createAbsoluteUrl('admin/GasLeaveHolidays/ToPdf',array('id'=>$modelPlan->id)) ?>" title="Xuất Pdf">
        <img src="<?php echo Yii::app()->theme->baseUrl; ?>/images/print.png">
    </a>
</div>


