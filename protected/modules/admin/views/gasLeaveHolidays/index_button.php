<div class="form">
    <?php
        $LinkGeneral   = Yii::app()->createAbsoluteUrl('admin/gasLeaveHolidays/', ['type'=> GasLeaveHolidays::TAB_GENERAL]);
        $LinkUI        = Yii::app()->createAbsoluteUrl('admin/gasLeaveHolidays/', ['type'=> GasLeaveHolidays::TAB_UI]);
        $type          = isset($_GET['type']) ? $_GET['type'] : GasLeaveHolidays::TAB_GENERAL;
    ?>
    <h1>Quản lý ngày nghỉ lễ
        <a class='btn_cancel f_size_14 <?php echo $type == GasLeaveHolidays::TAB_GENERAL ? "active":"";?>' href="<?php echo $LinkGeneral;?>">Tổng quan</a>
        <a class='btn_cancel f_size_14 <?php echo $type == GasLeaveHolidays::TAB_UI ? "active":"";?>' href="<?php echo $LinkUI;?>">Giao diện</a>
    </h1>
</div>

<?php 
    if($type == GasLeaveHolidays::TAB_GENERAL){
        $this->renderPartial('index', array(
            'model' => $model,
            'actions' => $actions
        )); 
    } else {
        include 'index_ui.php';
    }
?>