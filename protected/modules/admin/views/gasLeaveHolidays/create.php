<?php
$this->breadcrumbs=array(
	'Quản Lý Ngày Nghỉ Lễ'=>array('index'),
	'Tạo Mới',
);

$menus = array(		
        array('label'=>'Quản Lý Ngày Nghỉ Lễ', 'url'=>array('index')),
);
$this->menu= ControllerActionsName::createMenusRoles($menus, $actions);
$date = isset($_GET['date']) ? MyFormat::dateConverYmdToDmy($_GET['date']) : '';
?>

<h1>Tạo mới ngày nghỉ lễ <?php echo $date ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>