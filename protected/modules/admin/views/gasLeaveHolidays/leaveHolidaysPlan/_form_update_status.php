<?php
    $form = $this->beginWidget('CActiveForm', array(
        'id' => 'sell-form',
        'enableAjaxValidation' => false,
        'htmlOptions' => array('enctype' => 'multipart/form-data'),
        'action' => Yii::app()->createUrl('admin/GasLeaveHolidays/UpdateStatusPlan', array('id' => $model->id)),
    ));
    ?>
    <div class="row">
        <?php echo $form->labelEx($model, 'status'); ?>
        <?php echo $form->dropDownList($model, 'status', $model->getArrayStatus(), array('empty' => 'Select', 'class' => 'w-300')); ?>
        <?php echo $form->error($model, 'status'); ?>
    </div>
    <div class="row">
        <?php echo $form->labelEx($model, 'notify'); ?>
        <?php echo $form->textArea($model, 'notify', array('rows' => 5, 'cols' => 35)); ?>
        <?php echo $form->error($model, 'notify'); ?>
    </div>
    <div class="row buttons">
        <?php
        $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType' => 'submit',
            'label' => $model->isNewRecord ? 'Create' : 'Save',
            'type' => 'null', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
            'size' => 'small', // null, 'large', 'small' or 'mini'
        ));
        ?>	
    </div>

<?php $this->endWidget(); ?>



