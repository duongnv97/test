<?php echo $model->getHtmlNotify();?>
<?php
    $form = $this->beginWidget('CActiveForm', array(
        'id' => 'sell-form',
        'enableAjaxValidation' => false,
        'htmlOptions' => array('enctype' => 'multipart/form-data'),
        'action' => $model->scenario == 'update' ? Yii::app()->createUrl('admin/GasLeaveHolidays/UpdatePlan', array('id' => $model->id)) : Yii::app()->createUrl('admin/GasLeaveHolidays/CreatePlan', array('id' => $model->id)),
    ));
    ?>
    <p class="note">Dòng có dấu <span class="required">*</span> là bắt buộc.</p>
        <?php echo MyFormat::BindNotifyMsg(); ?>
    <div class="row display_none">
        <?php echo $form->labelEx($model, 'id'); ?>
        <?php echo $form->textField($model, 'id', array('class' => 'number_only_v1')); ?>
        <?php echo $form->error($model, 'id'); ?>
    </div>
    <div class="row">
        <?php echo $form->labelEx($model, 'approved'); ?>
        <?php echo $form->dropDownList($model, 'approved', GasLeave::ListoptionApprove(), array('empty' => 'Select', 'class' => 'w-300')); ?>
        <?php echo $form->error($model, 'approved'); ?>
    </div>
    <div class="row">
        <?php echo $form->labelEx($model, 'status'); ?>
        <?php echo $model->getStatus();?>
        <?php echo $form->error($model, 'status'); ?>
    </div>
    
    <div class="row">
        <?php echo $form->labelEx($model, 'is_sendmail',['label'=>'Gửi mail cho người duyệt']); ?>
        <?php echo $form->checkBox($model, 'is_sendmail');?>
        <?php echo $form->error($model, 'is_sendmail'); ?>
    </div>
    <?php if($model->status == $model->STATUS_REQUIRED_UPDATE){ ?>
    <div class="row">
        <?php echo $form->labelEx($model, 'notify'); ?>
        <?php echo($model->getNotify()); ?>
        <?php echo $form->error($model, 'notify'); ?>
    </div>
    <?php } ?>
    <div class="row buttons">
        <?php
        $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType' => 'submit',
            'label' => $model->isNewRecord ? 'Create' : 'Save',
            'type' => 'null', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
            'size' => 'small', // null, 'large', 'small' or 'mini'
        ));
        ?>	
    </div>

<?php $this->endWidget(); ?>

