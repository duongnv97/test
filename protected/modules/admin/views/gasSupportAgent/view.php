<?php
$this->breadcrumbs=array(
    $this->pluralTitle => array('index'),
    ' ' . $this->singleTitle . ' : ',
);

$menus = array(
	array('label'=>"$this->pluralTitle Management", 'url'=>array('index')),
	array('label'=>"Tạo Mới $this->singleTitle", 'url'=>array('create')),
	array('label'=>"Cập Nhật $this->singleTitle", 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>"Xóa $this->singleTitle", 'url'=>array('delete'), 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Bạn chắc chắn muốn xóa ?')),
);
$this->menu= ControllerActionsName::createMenusRoles($menus, $actions);
$cRole = Yii::app()->user->role_id;
?>

<h1>Xem: <?php echo $this->singleTitle. " - $model->code_no"; ?></h1>

<?php include 'view_xdetail.php';?>
<?php if($cRole != ROLE_SUB_USER_AGENT): ?>
    <?php include 'view_reply.php';?>
<?php endif;?>
