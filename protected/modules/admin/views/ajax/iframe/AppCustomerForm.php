<?php 
$canCreateAdd = true;
$cRole  = MyFormat::getCurrentRoleId();
$cUid   = MyFormat::getCurrentUid();

if($cUid == GasConst::UID_NGUYET_PT || in_array($cUid, GasConst::getArrUidUpdateAll()) || $cRole == ROLE_ADMIN){
    $canCreateAdd = true;
}
    $transaction_id    = isset($_GET['transaction_id']) ? $_GET['transaction_id'] : 0;
    $mTransactionHistory      = TransactionHistory::model()->findByPk($transaction_id);
    $googleAdd  = $mapLatitude = $mapLongitude = '';
    if($mTransactionHistory && $mTransactionHistory->source == Sell::SOURCE_APP):
        $googleAdd              = $mTransactionHistory->address;
        if(empty($model->area_code_id)){
            $model->area_code_id    = $mTransactionHistory->agent_id;
        }
    endif;
//    include 'AppCustomerFormMap.php';
?>
<?php $this->widget('MapCustomerBookWidget', ['mTransactionHistory' => $mTransactionHistory]); ?>

<div class="form">
<?php
$form=$this->beginWidget('CActiveForm', array(
	'id'=>'gas-district-form',
	'enableAjaxValidation'=>false,
)); ?>
    <p class="note">Dòng có dấu <span class="required">*</span> là bắt buộc.</p>
    <?php echo MyFormat::BindNotifyMsg(); ?>
    
    <?php if(!empty($googleAdd)): ?>
    <div class="row add_new_item"  style="padding-left:0;">
        <?php echo $form->labelEx($model,'address', ['label'=>'Địa chỉ google map']); ?>
        <a href='javascript:;' class="f_size_15" style="padding-left:0;"><?php echo $googleAdd; ?></a>
    </div>
    <?php endif; ?>
    <br><div class="clr"></div>
    <div class="row">
    <?php echo $form->labelEx($model,'area_code_id', array('label'=>'Đại Lý (Nơi giao)')); ?>
    <?php echo $form->hiddenField($model,'area_code_id', array('class'=>'')); ?>
    <?php
            // 1. limit search kh của sale
            $url = Yii::app()->createAbsoluteUrl('admin/ajax/search_by_role', array('role'=> ROLE_AGENT));
            // widget auto complete search user customer and supplier
            $aData = array(
                'model'=>$model,
                'field_customer_id'=>'area_code_id',
                'url'=> $url,
                'name_relation_user'=>'by_agent',
                'ClassAdd' => 'w-400',
                'field_autocomplete_name' => 'autocomplete_name',
                'placeholder'=>'Nhập mã hoặc tên đại lý',
            );
            $this->widget('ext.SpaAutocompleteCustomer.SpaAutocompleteCustomer',
                array('data'=>$aData));
            ?>
        <?php echo $form->error($model,'area_code_id'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model,'first_name'); ?>
        <?php echo $form->textField($model,'first_name', array('class'=>'w-400')); ?>
        <?php echo $form->error($model,'first_name'); ?>
    </div>
    <div class="row">
        <div style="width: 100%;float: left;padding-bottom: 5px;padding-left: 140px;">
            <label style="color: red;width: auto; ">Mỗi số phone cách nhau bởi dấu - vd: 0123456789-0988180386</label>
            <!--<label style="color: red;width: auto; ">Chỉ các tỉnh: TPHCM, Đồng Nai, Bình Dương, Vũng Tàu yêu cầu nhập số di động báo giá</label>-->
        </div>
        <?php echo $form->labelEx($model,'phone'); ?>
        <?php echo $form->textField($model,'phone', array('class'=>'w-400')); ?>
        <?php echo $form->error($model,'phone'); ?>
    </div>
    <div class="row">
        <?php echo $form->labelEx($model,'province_id'); ?>
        <?php echo $form->dropDownList($model,'province_id', GasProvince::getArrAll(),array('class'=>'w-400','empty'=>'Select')); ?>
        <?php echo $form->error($model,'province_id'); ?>
    </div>
    
    <?php if($canCreateAdd): ?>
    <div class="add_new_item float_l f_size_12" style="padding-left: 118px;"><a class="iframe_create_district" href="<?php echo Yii::app()->createAbsoluteUrl('admin/ajax/iframe_create_district') ;?>">Tạo Mới Quận Huyện</a><em> (Nếu trong danh sách không có)</em></div>
    <div class="clr"></div>
    <?php endif; ?>
    
    <div class="row">
        <?php echo $form->labelEx($model,'district_id'); ?>
        <?php echo $form->dropDownList($model,'district_id', GasDistrict::getArrAll($model->province_id),array('class'=>'w-400','empty'=>'Select')); ?>
        <?php echo $form->error($model,'district_id'); ?>
    </div>
    <?php if($canCreateAdd): ?>
        <div class="add_new_item float_l f_size_12" style="padding-left: 118px;"><a class="iframe_create_ward" href="<?php echo Yii::app()->createAbsoluteUrl('admin/ajax/iframe_create_ward') ;?>">Tạo Phường Xã</a><em></em></div>
    <div class="clr"></div>
    <?php endif; ?>
    <div class="row">
        <?php echo $form->labelEx($model,'ward_id'); ?>
        <?php echo $form->dropDownList($model,'ward_id', GasWard::getArrAll($model->province_id, $model->district_id),array('class'=>'w-400','empty'=>'Select')); ?>
        <?php echo $form->error($model,'ward_id'); ?>
    </div>
    <?php if($canCreateAdd): ?>
    <div class="add_new_item float_l f_size_12" style="padding-left: 118px;"><a class="iframe_create_street" href="<?php echo Yii::app()->createAbsoluteUrl('admin/ajax/iframe_create_street') ;?>">Tạo Mới Đường</a><em></em></div>
    <div class="clr"></div>
    <?php endif; ?>
    <div class="row">
        <?php echo $form->labelEx($model,'street_id'); ?>
        <?php echo $form->hiddenField($model,'street_id', array()); ?>
        <?php 
            // widget auto complete search user customer and supplier
            $aData = array(
                'model'=>$model,
                'field_street_id'=>'street_id',
                'field_autocomplete_name'=>'autocomplete_name_street',
                'url'=> Yii::app()->createAbsoluteUrl('admin/ajax/autocomplete_data_streets'),
                'NameRelation'=>'street',
                'ClassAdd' => 'w-400',
            );
            $this->widget('ext.GasAutoStreet.GasAutoStreet',
                array('data'=>$aData));                                        
        ?>
        <?php echo $form->error($model,'street_id'); ?>
    </div>
    <div class="row">
        <?php echo $form->labelEx($model,'house_numbers'); ?>
        <?php echo $form->textField($model,'house_numbers', array('class'=>'w-400')); ?>
        <?php echo $form->error($model,'house_numbers'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model->mUsersRef,'contact_note') ?>
        <?php echo $form->textArea($model->mUsersRef,'contact_note',array('class'=>'w-400','row'=>5)); ?>
        <?php echo $form->error($model->mUsersRef,'contact_note'); ?>
    </div>

    <?php // if($model->isNewRecord) $model->name='Phường '; ?>

    <div class="row buttons" style="padding-left: 141px;">
        <?php $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType'=>'submit',
            'label'=>$model->isNewRecord ? 'Create' :'Save',
            'type'=>'null', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
            'size'=>'small', // null, 'large', 'small' or 'mini'
            //'htmlOptions' => array('style' => 'margin-bottom: 10px; float: right;'),
            )); 
        ?>	
        <input class='cancel_iframe' type='button' value='Cancel'>
    </div>

<?php $this->endWidget(); ?>
    
    <div class="grp-cot clearfix">
        <div class="sk-col-md-12" style="padding-left: 10px;">
            <?php // $this->widget('MapOnePointWidget', ['title' => $googleAdd, 'latitude' => $mapLatitude, 'longitude' => $mapLongitude]); ?>
            <?php // include 'AppCustomerFormMap.php'; ?>
        </div>
    </div>
    
    <?php $this->widget('CustomerChangeInfoWidget', array('customer_id' => $model->id)); ?>
</div><!-- form -->
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/jquery.blockUI.js"></script>  
<script>
$(document).ready(function() {
    $('.form').find('button:submit').click(function(){
        $.blockUI({ overlayCSS: { backgroundColor: '<?php echo BLOCK_UI_COLOR;?>' } }); 
    });
        
    $('#Users_province_id').live('change',function(){
        var province_id = $(this).val();        
        var url_ = "<?php echo Yii::app()->createAbsoluteUrl('admin/ajax/get_slt_district');?>";
        $.blockUI({ overlayCSS: { backgroundColor: '<?php echo BLOCK_UI_COLOR;?>' } }); 
        $.ajax({
            url: url_,
            data: {ajax:1,province_id:province_id},
            type: "get",
            dataType:'json',
            success: function(data){
                $('#Users_district_id').html(data['html_district']);                
                $.unblockUI();
            }
        });

    });   		

    $('#Users_district_id').live('change',function(){
        var province_id = $('#Users_province_id').val();   
        var district_id = $(this).val();        
        var url_ = "<?php echo Yii::app()->createAbsoluteUrl('admin/ajax/get_slt_ward');?>";
        $.blockUI({ overlayCSS: { backgroundColor: '<?php echo BLOCK_UI_COLOR;?>' } }); 
        $.ajax({
                url: url_,
                data: {ajax:1,district_id:district_id,province_id:province_id},
                type: "get",
                dataType:'json',
                success: function(data){
                        $('#Users_ward_id').html(data['html_district']);                
                        $.unblockUI();
                }
        });
    });
    
    $(".iframe_create_district").colorbox({top:100, iframe:true,innerHeight:'300', innerWidth: '700',close: "<span title='close'>close</span>"});
    $(".iframe_create_ward").colorbox({top:100, iframe:true,innerHeight:'400', innerWidth: '700',close: "<span title='close'>close</span>"});
    $(".iframe_create_street").colorbox({top:100, iframe:true,innerHeight:'300', innerWidth: '700',close: "<span title='close'>close</span>"});
    
});

</script>
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/jquery.colorbox-min.js"></script>
<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/admin/css/colorbox.css"/>