<h1>Tạo Mới Khách Hàng Hộ Gia Đình</h1>
<div class="form">
<?php
$form=$this->beginWidget('CActiveForm', array(
	'id'=>'gas-district-form',
	'enableAjaxValidation'=>false,
)); ?>
	<p class="note">Dòng có dấu <span class="required">*</span> là bắt buộc.</p>
        <?php if(Yii::app()->user->hasFlash('successUpdate')): ?>
            <div class="success_div"><?php echo Yii::app()->user->getFlash('successUpdate');?></div>         
        <?php endif; ?>        	
	
	<div class="row">
            <?php echo $form->labelEx($model,'first_name'); ?>
            <?php echo $form->textField($model,'first_name', array('class'=>'w-400')); ?>
            <?php echo $form->error($model,'first_name'); ?>
	</div>
	<div class="row">
            <?php echo $form->labelEx($model,'phone'); ?>
            <?php echo $form->textField($model,'phone', array('class'=>'w-400')); ?>
            <?php echo $form->error($model,'phone'); ?>
	</div>
	<div class="row">
            <?php echo $form->labelEx($model,'province_id'); ?>
            <?php echo $form->dropDownList($model,'province_id', GasProvince::getArrAll(),array('class'=>'w-400','empty'=>'Select')); ?>
            <?php echo $form->error($model,'province_id'); ?>
	</div>

	<div class="row">
            <?php echo $form->labelEx($model,'district_id'); ?>
            <?php echo $form->dropDownList($model,'district_id', GasDistrict::getArrAll($model->province_id),array('class'=>'w-400','empty'=>'Select')); ?>
            <?php echo $form->error($model,'district_id'); ?>
	</div>
        <div class="row">
            <?php echo $form->labelEx($model,'ward_id'); ?>
            <?php echo $form->dropDownList($model,'ward_id', GasWard::getArrAll($model->province_id, $model->district_id),array('class'=>'w-400','empty'=>'Select')); ?>
            <?php echo $form->error($model,'ward_id'); ?>
        </div>
        <div class="row">
            <?php echo $form->labelEx($model,'street_id'); ?>
            <?php echo $form->hiddenField($model,'street_id', array()); ?>
            <?php 
                // widget auto complete search user customer and supplier
                $aData = array(
                    'model'=>$model,
                    'field_street_id'=>'street_id',
                    'field_autocomplete_name'=>'autocomplete_name_street',
                    'url'=> Yii::app()->createAbsoluteUrl('admin/ajax/autocomplete_data_streets'),
                    'NameRelation'=>'street',
                    'ClassAdd' => 'w-400',
                );
                $this->widget('ext.GasAutoStreet.GasAutoStreet',
                    array('data'=>$aData));                                        
            ?>
            <?php echo $form->error($model,'street_id'); ?>
	</div>
        <div class="row">
            <?php echo $form->labelEx($model,'house_numbers'); ?>
            <?php echo $form->textField($model,'house_numbers', array('class'=>'w-400')); ?>
            <?php echo $form->error($model,'house_numbers'); ?>
	</div>

	<?php // if($model->isNewRecord) $model->name='Phường '; ?>

	<div class="row buttons" style="padding-left: 141px;">
            <?php $this->widget('bootstrap.widgets.TbButton', array(
                'buttonType'=>'submit',
                'label'=> 'Create',
                'type'=>'null', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
                'size'=>'small', // null, 'large', 'small' or 'mini'
                //'htmlOptions' => array('style' => 'margin-bottom: 10px; float: right;'),
                )); 
            ?>	
            <!--<input class='cancel_iframe' type='button' value='Cancel'>-->
        </div>

<?php $this->endWidget(); ?>

</div><!-- form -->
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/jquery.blockUI.js"></script>  
<script>
$(document).ready(function() {
    $('.form').find('button:submit').click(function(){
        $.blockUI({ overlayCSS: { backgroundColor: '<?php echo BLOCK_UI_COLOR;?>' } }); 
    });
        
    $('#Users_province_id').live('change',function(){
            var province_id = $(this).val();        
            var url_ = "<?php echo Yii::app()->createAbsoluteUrl('admin/ajax/get_slt_district');?>";
            $.blockUI({ overlayCSS: { backgroundColor: '<?php echo BLOCK_UI_COLOR;?>' } }); 
            $.ajax({
                url: url_,
                data: {ajax:1,province_id:province_id},
                type: "get",
                dataType:'json',
                success: function(data){
                    $('#Users_district_id').html(data['html_district']);                
                    $.unblockUI();
                }
            });

        });   		
	
        $('#Users_district_id').live('change',function(){
            var province_id = $('#Users_province_id').val();   
            var district_id = $(this).val();        
            var url_ = "<?php echo Yii::app()->createAbsoluteUrl('admin/ajax/get_slt_ward');?>";
            $.blockUI({ overlayCSS: { backgroundColor: '<?php echo BLOCK_UI_COLOR;?>' } }); 
            $.ajax({
                    url: url_,
                    data: {ajax:1,district_id:district_id,province_id:province_id},
                    type: "get",
                    dataType:'json',
                    success: function(data){
                            $('#Users_ward_id').html(data['html_district']);                
                            $.unblockUI();
                    }
            });
        });
    
});

</script>
