
<?php if(empty($aData['CUT_SALARY_DETAIL'])) return; ?>
<div id="gas-debts-report-grid" class="grid-view">
    <table class="tb hm_table tb_sticky" style="width: 100%;">
        <thead>
            <tr class="item_c">
                <th>#</th>
                <th>Người thực hiện</th>
                <th>Số tiền</th>
                <th>Lý do/Diễn giải</th>
                <th>Tháng thực hiện</th>
                <th>Loại</th>
                <th>ID liên quan</th>
                <th>Trạng thái</th>
                <th>Ngày tạo</th>
                <th>Người tạo</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td colspan="10" class="item_b item_c">Chi tiết các loại trừ lương</td>
            </tr>
            <?php $no=1; $sum = 0; ?>
            <?php foreach ($aData['CUT_SALARY_DETAIL'] as $item): ?>
                <?php $sum += (empty($item->amount) ? 0 : $item->amount); ?>
                <tr>
                    <td class="item_c"><?php echo $no++; ?></td>
                    <td class="item_l"><?php echo $item->getUserName(); ?></td>
                    <td class="item_r"><?php echo $item->getAmount(); ?></td>
                    <td class="item_l"><?php echo $item->reason; ?></td>
                    <td class="item_c"><?php echo $item->getMonth(); ?></td>
                    <td class="item_l"><?php echo $item->getTypesText(); ?></td>
                    <td class="item_l"><?php echo $item->getRelationInfo(); ?></td>
                    <td class="item_c"><?php echo $item->getStatusText(); ?></td>
                    <td class="item_c"><?php echo $item->getCreatedDate(); ?></td>
                    <td class="item_l"><?php echo $item->getCreatedBy(); ?></td>
                </tr>
            <?php endforeach; ?>
                <tr class="item_b">
                    <td colspan="2" class="item_r">Tổng cộng</td>
                    <td class="item_r"><?php echo CmsFormatter::formatCurrency($sum); ?></td>
                    <td colspan="8"></td>
                </tr>
        </tbody>
    </table>
</div>

<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/print-store-card.css" />
<script>
    $(function(){
        fnAddClassOddEven('items');
    });
</script>