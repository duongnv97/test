<?php
$this->breadcrumbs=array(
	'Xem thông tin lương, công nợ',
);

$cs = Yii::app()->getClientScript();

$css = Yii::app()->theme->baseUrl . '/admin/css/hr.css';
$cs->registerCssFile($css);

$scriptUrl = Yii::app()->theme->baseUrl . '/admin/js/hr.js';
$cs->registerScriptFile($scriptUrl);
?>
<h1>Xem thông tin lương, công nợ</h1>

<h3 class="title-info">Thông tin lương</h3>
<div class="search-form" style="">
<?php $this->renderPartial('hr/_search',array(
	'mHrSalary'=>$mHrSalary,
)); ?>
</div><!-- search-form -->

<?php if( empty($aData['SALARY']) ): ?>
    <p>Chưa có bảng lương trong tháng này!</p>
<?php else: ?>
    <p><b>Trạng thái: </b><?php echo $mHrSalary->getStatusDetail(); ?></p>
    <?php $this->widget('HrTableViewWidget', ['data'=>$aData['SALARY'], 'fixedHeader'=>true, 'sumRowBottom'=>false]); ?>
        <?php include 'cut_salary_detail.php'; ?>
<?php endif; ?>
    
<br>
<br>
<br>
<h3 class="title-info">Công nợ cá nhân</h3>
<?php
$this->widget('DebtsWidget', [
    'model'=>$mDebts,
    'aData'=> empty($aData['DEBTS']) ? [] : $aData['DEBTS'],
    'uid' => $mDebts->user_id
]);