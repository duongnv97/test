<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=> strtok(GasCheck::getCurl(), '?'),
	'method'=>'get',
)); ?>

    <?php 
    $aMonth = array_combine(range(1,12), range(1,12));
    $cYear  = intval(date('Y'));
    $aYear  = array_combine(range($cYear-10,$cYear), range($cYear-10,$cYear));
    ?>
    <div class="row more_col">
        <div class="col1">
            <div class="row">
                <?php echo $form->labelEx($mHrSalary, 'onlyMonth', ['label'=>'Chọn tháng']); ?>
                <?php echo $form->dropDownList($mHrSalary,'onlyMonth', $aMonth,array('class'=>' w-150')); ?>  
            </div>
        </div>
        <div class="col2">
            <div class="row">
                <?php echo $form->labelEx($mHrSalary, 'onlyYear', ['label'=>'Chọn năm']); ?>
                <?php echo $form->dropDownList($mHrSalary,'onlyYear', $aYear, array('class'=>' w-150 number_only_v1')); ?>  
            </div>
        </div>
    </div>

    <div class="row buttons" style="padding-left: 159px;">
        <?php $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType'=>'submit',
            'label'=>'Xem thông tin',
            'type'=>'null', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
            'size'=>'small', // null, 'large', 'small' or 'mini'
            //'htmlOptions' => array('style' => 'margin-bottom: 10px; float: right;'),
        )); ?>	
    </div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->
<script>
    $(function(){
        validateNumber();
    });
</script>