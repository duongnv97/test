<h1>Nhập thông tin nhân viên không liên lạc được </h1>
<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'setup-team-form',
	'enableAjaxValidation'=>false,
        'htmlOptions' => array('enctype' => 'multipart/form-data'),
        'action'=> Yii::app()->createAbsoluteUrl("admin/Ajax/CreateNotContact"),
)); ?>
    <p>Khi không liên lạc được với nhân viên kinh doanh, giám đốc khu vực, các bạn cập nhật thông tin ở đây</p>
    <?php echo MyFormat::BindNotifyMsg(); ?>
    
    <!--Search--> 
    <?php 
        echo $form->hiddenField($model,'type');
        echo $form->hiddenField($model,'user_id');
        echo $form->hiddenField($model,'date_apply');
    ?>
    <div class="row">
        <?php echo $form->labelEx($model,'note') ?>
        <?php echo $form->textArea($model,'note',array('class'=>'w-500','rows'=>5)); ?>
        <?php echo $form->error($model,'note'); ?>
    </div>
    
    <div class="row">
        <?php echo $form->labelEx($model,'user_create', ['label' => 'Nhân viên']); ?>
        <?php echo $form->hiddenField($model,'user_create', array('class'=>'')); ?>
        <?php
            // widget auto complete search user customer and supplier
            $aData = array(
                'model'=>$model,
                'field_customer_id'=>'user_create',
                'url'=> Yii::app()->createAbsoluteUrl('admin/ajax/search_user_login', ['PaySalary' => 1, 'FromNotContact' => 1]),
                'name_relation_user'=>'rUser',
                'ClassAdd' => 'w-500',
                'autocomplete_name' => 'autocomplete_name',
                'ShowTableInfo' => 0,
                'placeholder' => "Nhập mã hoặc tên NV",
                'fnSelectCustomerV2' => 'fnCallSomeFunctionAfterSelectV2',
                'doSomethingOnClose' => "doSomethingOnClose",
            );
            $this->widget('ext.SpaAutocompleteCustomer.SpaAutocompleteCustomer',
                array('data'=>$aData));                                        
        ?>
    </div>
    
    <table class="materials_table hm_table w-500 tbl-user">
        <thead>
            <tr>
                <th class="item_c w-20">#</th>
                <th class="item_code item_c w-300">Nhân viên không liên lạc được</th>
                <th class="item_unit last item_c">Xóa</th>
            </tr>
        </thead>
        <tbody>
        
        </tbody>
        
    </table>
    
    <div class="row buttons" style="padding-left: 141px;">
        <?php $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType'=>'submit',
            'label'=>'Save',
            'type'=>'null', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
            'size'=>'small', // null, 'large', 'small' or 'mini'
            'htmlOptions' => array(
                    'name' => 'save',
                    'value' => 1
                ),
        )); ?>
    </div>

<?php $this->endWidget(); ?>
    
    

</div><!-- form -->
<script>
    $(document).ready(function(){
        $('.form').find('button:submit').click(function(){
            $.blockUI({ overlayCSS: { backgroundColor: '#fff' } }); 
        });
        fnBindRemoveIcon();
    });
    
    function fnCallSomeFunctionAfterSelectV2(ui, idField, idFieldCustomerID){
        var no = ($('.tbl-user tbody tr')).length+1;
        var tr = '<tr>'
                + '<input name="SetupTeam[user_create]['+ui.item.id+']" value="'+ui.item.label+'" type="hidden">'
                + '<td class="item_c order_no">'+no+'</td>'
                + '<td class="uid_114943">'+ui.item.label+'</td>'
                + '<td class="item_c last"><span class="remove_icon_only"></span></td>'
                + '</tr>';
        $('.tbl-user').append(tr);
        return false;
    }
    
    function doSomethingOnClose(ui, idField, idFieldCustomer){
        var row = $(idField).closest('.row');
        row.find('.remove_row_item').trigger('click');
    }
</script>