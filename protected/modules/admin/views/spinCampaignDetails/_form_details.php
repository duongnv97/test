<h3 class='title-info'>Chi Tiết Phần Thưởng</h3>
<div class="clr"></div>
<div class="row">
    <?php echo $form->labelEx($model,'Phần thưởng'); ?>
    <?php echo $form->textField($model,'material_id',array('size'=>32,'placeholder'=>'Nhập mã vật tư hoặc tên vật tư')); ?>
    <!--<em class="hight_light item_b">Chú Ý: Có thể chọn nhiều vật tư trong cùng 1 lần tạo </em>-->
    <?php echo $form->error($model,'material_id'); ?>
    <div class="clr"></div>    		
</div>

<div class="row">
    <table class="materials_table hm_table">
        <thead>
            <tr>
                <th class="item_c">STT</th>
                <th class="item_name item_c">Tên</th>
                <th class="w-30 item_c">Số SP</th>
                <th class="item_unit last item_c">Xóa</th>
            </tr>
        </thead>
        <tbody>
        <?php $aMaterials = json_decode($model->award_json, true);?>
        <?php foreach($aMaterials as $key=>$material    ):?>
        <tr class="details_row details_row_<?php echo $material['id'] ?>">
            <td class="item_c order_no"></td>
            <td class="item_l material_name">
                <label><?php echo $material['name']; ?></label>
                <input class="json" name="SpinCampaignDetails[award_json][]" value='{"id":<?php echo $material["id"];?>, "name":"<?php echo $material["name"];?>", "qty": <?php echo $material["qty"] ?>}' class="materials_id" type="hidden"> 
            </td>
            <td class="item_c">
                <input name="" value="<?php echo $material['qty']; ?>" type="text" class="item_c item_qty w-30 materials_qty number_only_v1"  size="3" maxlength="3"/>
            </td>
            <td class="item_c last"><span class="remove_icon_only"></span></td>
        </tr>
        <?php endforeach;?>
        </tbody>
        <tfoot>
            <tr class="materials_row">
                <td class="item_b item_r f_size_16" colspan="2">Tổng cộng</td>
                <td class="item_b item_c total_qty f_size_16"></td>
                <td class="last" colspan="2"></td>
            </tr>
        </tfoot>
    </table>        
</div>

<script>
    //  Jun 25, 2016 begin for form detail
    $(document).ready(function(){

        fnInitAutocomplete();    // load auto materials  '
        fnRefreshOrderNumber();  // 
        fnBindRemoveIcon();     // action for button-delete: detele row (table)  
        fnCalcTotalPay();       
        fnBindSomeLiveChange(); // loading data when quatity or price change
        fnChangeQtyMaterial();
    });

    function fnInitAutocomplete(){
        var availableMaterials = <?php echo MyFunctionCustom::getMaterialsJson(array('material_id'=>$model->material_id)); ?>;
        $( "#SpinCampaignDetails_material_id" ).autocomplete({
            source: availableMaterials,
            close: function( event, ui ) { $( "#SpinCampaignDetails_material_id" ).val(''); },
            select: function( event, ui ) {
                var class_item = 'details_row_'+ui.item.id;
                if($('.'+class_item).size()<1)
                {
                    fnBuildRowMaterial(event, ui); 
                } else { }
                    
            }
        });

    }

    function fnBindSomeLiveChange(){
        $('.item_qty, .item_price, .OrderTypeAmount').live('change', function(){
            fnCalcTotalPay();
        });
    }
    
    function fnBuildRowMaterial(event, ui){
        
        var PriceBinhBoByMonth = "";
        var class_item = 'materials_row_'+ui.item.id;
        var _tr = '';
            _tr += '<tr class="details_row details_row_' + ui.item.id + ' ">';
            _tr +=      '<td class="item_c order_no"></td>';
            _tr +=      '<td class="item_l"><label>'+ui.item.name+'</label>';
            _tr +=          "<input class= 'json' name='SpinCampaignDetails[award_json][]' type='hidden' value='{"+'\"id\":'+ui.item.id+',\"name\":\"'+ui.item.value+"\",\"qty\":1}'/>";
            _tr +=      '</td>';
            _tr +=      '<td class="item_c ">';
            _tr +=          '<input name="materials_qty[]" value="1" class="item_c item_qty w-30 materials_qty number_only_v1 '+class_item+'" type="text" size="6" maxlength="9">';
            _tr +=      '</td>';
            _tr +=      '<td class="item_c last"><span class="remove_icon_only"></span></td>';
            _tr += '</tr>';
        $('.materials_table tbody').append(_tr);
        fnRefreshOrderNumber();
        bindEventForHelpNumber();
        fnCalcTotalPay();
        fnChangeQtyMaterial();
    }
    
    /**
    * @Author: NamLA Aug 24, 2019
    * @Todo: change json when change qty_material
    */
    function fnChangeQtyMaterial(){
        $('.details_row').change(function(){
            var json = $(this).find(' td .json').val();
            json = (json.slice(0, json.search('qty') +5));
            json += $(this).find(' td .materials_qty').val() + "}";
            $(this).find(' td .json').val(json)
    //        json = json.replace(json.indexOf('qty'))
        });
    }

    function fnCalcTotalPay(){
        var total_qty       = 0;
      
        $('.materials_table tbody .details_row').each(function(){
           var item_qty = $(this).find('.item_qty').val()*1; 
           total_qty+=item_qty;
        });
        total_qty = parseFloat(total_qty);
        total_qty = Math.round(total_qty * 100) / 100;
        $('.total_qty').text(commaSeparateNumber(total_qty));
    }

    function fnAfterRemoveIcon(){
        fnCalcTotalPay();
    }
    

</script>