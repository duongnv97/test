<?php
$this->breadcrumbs = array(
	'Quản Lý '.$this->singleTitle => array('index'),
	'Cập Nhật ' . $this->singleTitle,
);

$menus = array(	
    array('label' => 'Quản Lý '.$this->singleTitle, 'url' => array('index')),
    array('label' => 'Xem ' . $this->singleTitle, 'url' => array('view', 'id' => $model->id)),
);
$this->menu= ControllerActionsName::createMenusRoles($menus, $actions);

?>

<h1>Cập Nhật: <?php echo $this->singleTitle; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>