<?php
$this->breadcrumbs=array(
	
);

$menus=array(
            array('label'=>"Tạo mới", 'url'=>array('create')),
);
$this->menu= ControllerActionsName::createMenusRoles($menus, $actions); //create url-buttons  

// register script and css, add libery for view   

Yii::app()->clientScript->registerScript('search', "  
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});

$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('search-grid', {
                url : $(this).attr('action'),
		data: $(this).serialize()
	});
	return false;
});
");
$cRole = MyFormat::getCurrentRoleId();
if($cRole==ROLE_ADMIN){
    $actions[] = 'testNotify';
}
?>



<h1>Quản lý giải thưởng</h1>
    <?php echo CHtml::link('Tìm Kiếm Nâng Cao', '#', array('class' => 'search-button')); ?>
&nbsp;&nbsp;&nbsp;&nbsp;
    <div class="search-form" style="display: none" >
        <?php
        $this->renderPartial('_search', array(
            'model' => $model,
        ));
        ?>
    </div><!-- search-form -->
    
<?php $this->widget('zii.widgets.grid.CGridView', array(
    'id'=>'search-grid',
    'dataProvider'=>$model->search(),
//        'afterAjaxUpdate'=>'function(id, data){ fnUpdateColorbox();}',
    'template'=>'{pager}{summary}{items}{pager}{summary}',
//    'itemsCssClass' => 'items custom_here',
//    'htmlOptions'=> array('class'=>'grid-view custom_here'),
    'pager' => array(
        'maxButtonCount'=>  CmsFormatter::$PAGE_MAX_BUTTON,
    ),
    'enableSorting' => false,
    //'filter'=>$model,
    'columns'        => array(
        array(
            'header'            => 'S/N',
            'type'              => 'raw',
            'value'             => '$this->grid->dataProvider->pagination->currentPage * $this->grid->dataProvider->pagination->pageSize + ($row+1)',
            'headerHtmlOptions' => array('width' => '10px','style' => 'text-align:center;'),
            'htmlOptions'       => array('style' => 'text-align:center;')
        ),
        array(
            'name'      => 'Tên chiến dịch' ,
            'type'      => 'html',
            'value'     => '$data->getCampaignName()'
        ),
        array(
            'name'      => 'Tên khách hàng',
            'type'      => 'html',
            'value'     => '$data->getUser()'
        ),
        array(
            'name'      => 'number',
            'value'     => '$data->getNumber()'
        ),
        array(
            'name'      => 'rank',
            'value'     => '$data->getRank()'  
        ),
        array(
            'name'      => 'status',
            'value'     => '$data->getStatus()',
            'type'      => 'html'
        ),
        array(
            'header' => 'Actions',
            'class'=>'CButtonColumn',
            'template'=> ControllerActionsName::createIndexButtonRoles($actions,['view','update','delete', 'testNotify']),
            'buttons'=>array(
                'testNotify'=>array(
                    'visible'=> 'MyFormat::getCurrentRoleId()==ROLE_ADMIN',
                    'label'=>'Test send notify winning',     //Text label of the button.
                    'url'=>'Yii::app()->createAbsoluteUrl("admin/spinNumbers/testNotify",array("user_id"=>$data->user_id,"type"=>GasScheduleNotify::GAS24H_NUMBER_WINNING) )',
                    'imageUrl'=>Yii::app()->theme->baseUrl . '/admin/images/Upload.png',
                    'options'=>array('class' => 'update'), //HTML options for the button tag..
                ),
            ),
        ),
    )
));

?>

        
<script>
    $(document).ready(function() {
        fnSetExpired();
    });
    function fnSetExpired(){
        $(document).on('click', '.cancelAward', function(){
            if(confirm('Bạn muốn hủy trao quà cho Khách hàng này ?')) {
                $.fn.yiiGridView.update('search-grid', { 
                    type:'POST',
                    url:$(this).attr('href'),
                    success:function(data) {
                          $.fn.yiiGridView.update('search-grid');
                    }
                });
            }
            return false;
        });
    }
</script>