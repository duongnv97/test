<div class="form">
<?php Yii::app()->getClientScript()->registerCoreScript('jquery.ui'); ?>
<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'spin-campaigns-form',
	'enableAjaxValidation'=>false,
        'htmlOptions' => array('enctype' => 'multipart/form-data'),
)); ?>

    <p class="note">Dòng có dấu <span class="required">*</span> là bắt buộc.</p>
    <?php echo MyFormat::BindNotifyMsg(); ?>
    
        		
            
    <div class="row">
        <?php echo $form->labelEx($model,'campaign_id'); ?>
        <?php echo '<p>'.$model->getCampaignName().'</p>' ?>
        <?php echo $form->hiddenField($model, 'campaign_id'); ?>
        <?php echo $form->error($model,'campaign_id'); ?>
    </div>
    <div class="row">
        <?php echo $form->labelEx($model,'user_id'); ?>
        <?php echo '<p>'.$model->getUserName().'</p>'; ?>
        <?php echo $form->hiddenField($model, 'user_id'); ?>
        <?php echo $form->error($model,'user_id'); ?>
    </div>
    <div class="row">
        <?php echo $form->labelEx($model,'number'); ?>
        <?php echo '<p>'.$model->getNumber().'</p>'; ?>
        <?php echo $form->hiddenField($model, 'number'); ?>
        <?php echo $form->error($model,'number'); ?>
    </div>
    <div class="row">
        <?php echo $form->label($model, 'rank') ?>
        <?php echo '<p>'.$model->getRank().'</p>' ?>
        <?php echo $form->hiddenField($model, 'rank') ?>
    </div>
    <div class="row">
        <?php echo $form->labelEx($model,'status'); ?>
        <?php echo $form->dropdownList($model,'status',SpinCampaignDetails::getArrayStatus(),array('class' => 'w-300')); ?>
        <?php echo $form->error($model,'status'); ?>
    </div>
   <?php include '_form_details.php'?>
    <div class="row buttons" style="padding-left: 141px;">
        <?php $this->widget('bootstrap.widgets.TbButton', array(
        'buttonType'=>'submit',
        'label'=>$model->isNewRecord ? 'Create' :'Save',
        'type'=>'null', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
        'size'=>'small', // null, 'large', 'small' or 'mini'
        //'htmlOptions' => array('style' => 'margin-bottom: 10px; float: right;'),
    )); ?>	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->
<script>
    $(function(){
        $('.form').find('button:submit').click(function(){
            $.blockUI({ overlayCSS: { backgroundColor: '#fff' } }); 
        });
        
    });
</script>