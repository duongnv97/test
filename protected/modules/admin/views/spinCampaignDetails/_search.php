<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=> GasCheck::getCurl(),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'Tên chiến dịch',array()); ?>
		<?php echo $form->textField($model,'campaign_name',['class'=>'w-300']); ?>
	</div>
	<div class="row">
        <?php echo $form->labelEx($model,'Tên khách hàng'); ?>
        <?php echo $form->hiddenField($model,'user_id', array('class'=>'w-300')); ?>
        <?php
                // 1. limit search kh của sale
                $url = Yii::app()->createAbsoluteUrl('admin/ajax/search_by_role', array(
                            'role'      => ROLE_CUSTOMER,
//                            'PaySalary' => UsersProfile::SALARY_YES
                        )
                    );
                // widget auto complete search user customer and supplier
                $aData = array(
                    'model'                     =>$model,
                    'field_customer_id'         =>'user_id',
                    'url'                       => $url,
                    'name_relation_user'        =>'rUsers',
                    'ClassAdd'                  => 'w-300',
                    'field_autocomplete_name'   => 'autocomplete_name',
                    'placeholder'               =>'Nhập mã hoặc tên khách hàng',
                );
                $this->widget('ext.SpaAutocompleteCustomer.SpaAutocompleteCustomer',
                    array('data'=>$aData));
                ?>
        </div>
	<div class="row">
		<?php echo $form->label($model,'number',array()); ?>
		<?php echo $form->textField($model,'number',['class' => 'number_only_v1 w-300', 'maxlength' => 10,]); ?>
	</div>
        <div class="row">
            <?php echo $form->label($model, 'rank') ?>
            <?php echo $form->dropdownList($model, 'rank', SpinCampaignDetails::getArrayRank(),['empty'=>'Select','class'=>'w-300']) ?>
        </div>

        <div class="row">
		<?php echo $form->label($model,'status',array()); ?>
		<?php echo $form->dropdownList($model,'status', SpinCampaignDetails::getArrayStatus(),['empty'=>'Select','class'=>'w-300']); ?>
	</div>
	<div class="row buttons" style="padding-left: 159px;">
            <?php $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType'    =>'submit',
            'label'         =>'Search',
            'type'          =>'null', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
            'size'          =>'small', // null, 'large', 'small' or 'mini'
            //'htmlOptions' => array('style' => 'margin-bottom: 10px; float: right;'),
        )); ?>	
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a class='btn_cancel btnReset'>Reset</a>
        </div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->
<script>
    $(function(){
        $('.btnReset').click(function(){
            var div = $(this).closest('form');
            div.find('input').val('');
            div.find('Select').val('');
            div.find('.remove_row_item').trigger('click');
        });
    });
</script>