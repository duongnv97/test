<?php
$this->breadcrumbs=array(
	Yii::t('translation','Đặt Hàng')=>array('index'),
	Yii::t('translation','Thêm Mới'),
);

$menus = array(		
        array('label'=> Yii::t('translation', 'Đặt Hàng') , 'url'=>array('index')),
);
$this->menu= ControllerActionsName::createMenusRoles($menus, $actions);
?>

<h1><?php echo Yii::t('translation', 'Thêm Đặt Hàng'); ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>