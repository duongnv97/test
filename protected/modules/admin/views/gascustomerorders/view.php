<?php
$this->breadcrumbs=array(
	'Đặt Hàng'=>array('index'),
	$model->customer?$model->customer->first_name:'',
);

$menus = array(
	array('label'=>'Đặt Hàng', 'url'=>array('index')),
	array('label'=>'Thêm Đặt Hàng', 'url'=>array('create')),
	array('label'=>'Cập Nhật Đặt Hàng', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Xóa Đặt Hàng', 'url'=>array('delete'), 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
);
$this->menu= ControllerActionsName::createMenusRoles($menus, $actions);
?>

<h1>Xem Đặt Hàng: <?php echo $model->customer?$model->customer->first_name:''; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'orders_no',
		array(
		'name'=>'customer_id',
		'value'=>$model->customer?$model->customer->first_name:''
		),
		'date_delivery:Date',
		'quantity_50',
		'quantity_45',
		'quantity_12',
		array(
		'name'=>'agent_id',
		'value'=>$model->agent?$model->agent->first_name:''
		),			
		'time_delivery',
		'note:html',
		'note_extra:html',
		'other_material:html',
		
		array(
		'name'=>'user_id_create',
		'value'=>$model->user_create?$model->user_create->first_name:''
		),		
		'created_date:Datetime',
	),
)); ?>
