<?php
$this->breadcrumbs=array(
	'Đặt Hàng',
);

$menus=array(
	array('label'=> Yii::t('translation','Thêm Đặt Hàng'), 'url'=>array('create')),
);
$this->menu= ControllerActionsName::createMenusRoles($menus, $actions);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('gas-customer-orders-grid', {
                url : $(this).attr('action'),
		data: $(this).serialize()
	});
	return false;
});
");

Yii::app()->clientScript->registerScript('ajaxupdate', "
$('#gas-customer-orders-grid a.ajaxupdate').live('click', function() {
    $.fn.yiiGridView.update('gas-customer-orders-grid', {
        type: 'POST',
        url: $(this).attr('href'),
        success: function() {
            $.fn.yiiGridView.update('gas-customer-orders-grid');
        }
    });
    return false;
});
");
?>

<h1><?php echo Yii::t('translation', 'Đặt Hàng'); ?></h1>

<h2>
    <a href="<?php echo Yii::app()->createAbsoluteUrl('admin/gascustomerorders/export_customer_order');?>">
        Xuất file Excel
    </a>
</h2>

<?php echo CHtml::link(Yii::t('translation','Advanced Search'),'#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'gas-customer-orders-grid',
	'dataProvider'=>$model->search(),
        'template'=>'{pager}{summary}{items}{pager}{summary}',     
	'enableSorting' => false,
	//'filter'=>$model,
	'columns'=>array(
        array(
            'header' => 'S/N',
            'type' => 'raw',
            'value' => '$this->grid->dataProvider->pagination->currentPage * $this->grid->dataProvider->pagination->pageSize + ($row+1)',
            'headerHtmlOptions' => array('width' => '30px','style' => 'text-align:center;'),
            'htmlOptions' => array('style' => 'text-align:center;')
        ),
        array(
            'name' => 'customer_id',
            'value' => '$data->customer?$data->customer->first_name:""',
            'htmlOptions' => array('style' => 'text-align:left;')
        ),   
        array(
            'name' => 'date_delivery',
            'type' => 'Date',
            'htmlOptions' => array('style' => 'text-align:center;')
        ), 	        
		array(
            'name' => 'time_delivery',
            'htmlOptions' => array('style' => 'text-align:center;')
        ), 		
        array(
            'name' => 'agent_id',
            'value' => '$data->agent?$data->agent->first_name:""',
            'htmlOptions' => array('style' => 'text-align:left;')
        ),   		
		array(
            'name' => 'quantity_50',
            'htmlOptions' => array('style' => 'text-align:center;')
        ),		
		array(
            'name' => 'quantity_45',
            'htmlOptions' => array('style' => 'text-align:center;')
        ),
		array(
            'name' => 'quantity_12',
            'htmlOptions' => array('style' => 'text-align:center;')
        ), 
 		
		array(
            'name' => 'note',
			'type' => 'html',
            'htmlOptions' => array('style' => 'text-align:left;')
        ), 
        array(
            'name' => 'created_date',
            'type' => 'Datetime',
            'htmlOptions' => array('style' => 'text-align:center;')
        ),		
		/*
		 Yii::t('translation','user_id_create'),
		 Yii::t('translation','note'),
		 Yii::t('translation','created_date'),
		*/
		array(
			'header' => 'Action',
			'class'=>'CButtonColumn',
                        'template'=> ControllerActionsName::createIndexButtonRoles($actions),
		),
	),
)); ?>
