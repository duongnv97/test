<div class="wide form">

    <?php
    $form = $this->beginWidget('CActiveForm', array(
        'action' => GasCheck::getCurl(), //Yii::app()->createUrl($this->route),
        'method' => 'get',
    ));
    ?>   


    <div class="row">    
        <?php echo $form->label($model, 'code_account', array('label' => Yii::t('translation', 'Mã chi nhánh/ địa điểm'))); ?>
        <?php echo $form->textField($model, 'code_account', array('class' => 'w-200', 'maxlength' => 200)); ?>
    </div>  
    <div class="row">
        
        <?php echo $form->labelEx($model, 'created_by'); ?>
        <?php echo $form->hiddenField($model, 'created_by', array('class' => '')); ?>
        <?php
        // 1. limit search kh của sale
        $url = Yii::app()->createAbsoluteUrl('admin/ajax/search_user_login');
        // widget auto complete search user customer and supplier        $url = Yii::app()->createAbsoluteUrl('admin/ajax/search_by_role', array('role' => implode(',', $model->getRoleAllowCreate())));
        
        $aData = array(
            'model' => $model,
            'field_customer_id' => 'created_by',
            'url' => $url,
            'name_relation_user' => 'rCreatedBy',
            'ClassAdd' => 'w-200',
            'field_autocomplete_name' => 'amount_discount',
            'placeholder' => 'Nhập mã NV hoặc tên',
        );
        $this->widget('ext.SpaAutocompleteCustomer.SpaAutocompleteCustomer', array('data' => $aData));
        ?>
        <?php echo $form->error($model, 'created_by'); ?>
    </div>
    <div class="row more_col">
        <div class="col1">
            <?php echo $form->labelEx($model, 'parent_id', array('label' => 'Công ty')); ?>
            <?php echo $form->dropDownList($model, 'parent_id', Borrow::model()->getListdataCompany(), array('class' => 'w-200', 'empty' => 'Select')); ?>
        </div>

        <div class="col2">
            <?php echo Yii::t('translation', $form->labelEx($model, 'province_id')); ?>
            <?php echo $form->dropDownList($model, 'province_id', GasProvince::getArrAllFix(), array('class' => 'w-200', 'empty' => 'Select')); ?>
        </div>    
    </div>
    <div class="row more_col">
        <div class="col1">
            <?php echo Yii::t('translation', $form->labelEx($model, 'district_id')); ?>
            <?php echo $form->dropDownList($model, 'district_id', GasDistrict::getArrAll($model->province_id), array('class' => 'w-200', 'empty' => 'Select')); ?>
        </div>
        <div class="col2">
            <?php echo $form->labelEx($model, 'gender', array('label' => 'Loại')); ?>
            <?php echo $form->dropDownList($model, 'gender', $model->getArrayManageBranch(), array('class' => 'w-200', 'empty' => 'Select')); ?>
        </div>
<!--        <div class="col3">
            <?php echo Yii::t('translation', $form->labelEx($model, 'status')); ?>
            <?php echo $form->dropDownList($model, 'status', ActiveRecord::getUserStatus(), array('class' => 'w-200', 'empty' => 'Select')); ?>
        </div>         -->
    </div>
    
    <div class="row ">
        <?php echo $form->labelEx($model, 'Đại lý'); ?>
        <?php echo $form->hiddenField($model, 'agent_id'); ?>
        <?php
        // 1. limit search kh của sale
        $url = Yii::app()->createAbsoluteUrl('admin/ajax/search_by_role', array('role' => ROLE_AGENT));
        // widget auto complete search user customer and supplier
        $aData = array(
            'model' => $model,
            'field_customer_id' => 'agent_id',
            'url' => $url,
            'name_relation_user' => 'rAgent',
            'ClassAdd' => 'w-300',
            'placeholder' => 'Nhập mã hoặc tên đại lý',
        );
        $this->widget('ext.SpaAutocompleteCustomer.SpaAutocompleteCustomer',             array('data' => $aData));
        ?>
    </div>
    
    <div class="row">
        <?php echo $form->labelEx($model, 'Thực tế'); ?>
        <?php echo $form->dropDownList($model, 'real_status_agent', $model->getArrayRealStatusAgent(), array('class' => 'w-200', 'empty' => 'Select')); ?>
    </div> 


    <div class="row buttons" style="padding-left: 159px;">
        <?php
        $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType' => 'submit',
            'label' => 'Search',
            'type' => 'null', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
            'size' => 'small', // null, 'large', 'small' or 'mini'
                //'htmlOptions' => array('style' => 'margin-bottom: 10px; float: right;'),
        ));
        ?>
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a class='btn_cancel' style="text-decoration-line: none;" href="<?php echo Yii::app()->createAbsoluteUrl('admin/branchCompany/index', ['to_excel' => 1]); ?>">Xuất Excel</a>
    </div>

    <?php $this->endWidget(); ?>

</div><!-- search-form -->
<script>

    $(document).ready(function () {
        $('#BranchCompany_province_id').change(function () {
            var province_id = $(this).val();

            var url_ = "<?php echo Yii::app()->createAbsoluteUrl('admin/ajax/get_slt_district'); ?>";
            $.blockUI({overlayCSS: {backgroundColor: '<?php echo BLOCK_UI_COLOR; ?>'}});
            $.ajax({
                url: url_,
                data: {ajax: 1, province_id: province_id},
                type: "get",
                dataType: 'json',
                success: function (data) {
                    $('#BranchCompany_district_id').html(data['html_district']);
                    $.unblockUI();
                }
            });

        });
        $('.btnReset').click(function () {
            var div = $(this).closest('form');
            div.find('input,select').val('');
        });
    });

</script>