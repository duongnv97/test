<?php

$this->breadcrumbs=array(
	'QL Chi Nhánh'=>array('index'),
	$model->first_name,
);

$menus = array(
	array('label'=>'BranchCompany Management', 'url'=>array('index')),
	array('label'=>'Create GasStorehouse', 'url'=>array('create')),
	array('label'=>'Update GasStorehouse', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete GasStorehouse', 'url'=>array('delete'), 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
);
$this->menu= ControllerActionsName::createMenusRoles($menus, $actions);
?>

<h1>View #<?php echo $model->first_name; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
            array(
                'name'=>'Mã số',
                'value'=>$model->code_account?$model->code_account:''
            ),  
                     
            array(
                'name'=>'Loại',
                'value'=>$model->getArrayManageBranch()[$model->gender]
            ),   
            array(
                'name'=>'Công ty',
                'value'=>$model->getParent()
            ),  
            array(
                'name'=>'Tên chi nhánh',
                'value'=>$model->first_name?$model->getFullname():''
            ),     
            array(
                'name'=>'TỈnh',
                'value'=>$model->province?$model->getProvince():''
            ),            
            array(
                'name'=>'Quận huyện',
                'value'=>$model->district?$model->getDistrict():''
            ),            
            array(
                'name'=>'Trạng thái',
                'value'=>$model->getStatusText()
            ),            
            array(
                'name'=>'Ngày tạo',
                'value'=>$model->getCreatedDate()
            ),            
            array(
                'name'=>'Nguời tạo',
                'value'=>$model->getCreatedBy()
            ),            
                
                    
	
	),
)); ?>
