<?php
$this->breadcrumbs = array(
    'QL Chi Nhánh',
);
$type = isset($_GET['type'])?$_GET['type']:'';
$menus = array(
    array('label' => Yii::t('translation', 'Create GasStorehouse'), 'url' => array('create')),
);
$this->menu = ControllerActionsName::createMenusRoles($menus, $actions);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('gas-agent-grid', {
                url : $(this).attr('action'),
		data: $(this).serialize()
	});
	return false;
});
");

Yii::app()->clientScript->registerScript('ajaxupdate', "
$('#gas-storehouse-grid a.ajaxupdate').live('click', function() {
    $.fn.yiiGridView.update('gas-agent-grid', {
        type: 'POST',
        url: $(this).attr('href'),
        success: function() {
            $.fn.yiiGridView.update('gas-storehouse-grid');
        }
    });
    return false;
});
");
?>
<div class="form">
    <h1><?php echo Yii::t('translation', 'QL Chi Nhánh'); ?>
        <?php
        $LinkGeneral = Yii::app()->createAbsoluteUrl('admin/branchCompany/index');
        $LinkVersionOs = Yii::app()->createAbsoluteUrl('admin/branchCompany/index', ['type' => BranchCompany::IS_ADDRESS]);
        ?>
        <a class='btn_cancel f_size_14 <?php echo!isset($_GET['type']) ? "active" : ""; ?>' href="<?php echo $LinkGeneral; ?>">Chi nhánh / Địa điểm chính</a>
        <a class='btn_cancel f_size_14 <?php echo isset($_GET['type']) && $_GET['type'] == BranchCompany::IS_ADDRESS ? "active" : ""; ?>' href="<?php echo $LinkVersionOs; ?>">Địa điểm con</a>

    </h1>
</div>

<?php echo CHtml::link(Yii::t('translation', 'Advanced Search'), '#', array('class' => 'search-button')); ?>
<div class="search-form">
    <?php
    $this->renderPartial('_search', array(
        'model' => $model,
    ));
    ?>
</div><!-- search-form -->

<?php
$this->widget('zii.widgets.grid.CGridView', array(
    'id' => 'gas-agent-grid',
    'dataProvider' => $model->searchBranchCompany($type),
    'afterAjaxUpdate' => 'function(id, data){ fnUpdateColorbox();}',
    'enableSorting' => false,
//	'filter'=>$model,
    'columns' => array(
        array(
            'header' => 'S/N',
            'type' => 'raw',
            'value' => '$this->grid->dataProvider->pagination->currentPage * $this->grid->dataProvider->pagination->pageSize + ($row+1)',
            'headerHtmlOptions' => array('width' => '30px', 'style' => 'text-align:center;'),
            'htmlOptions' => array('style' => 'text-align:center;')
        ),
        array(
            'name' => 'Loại',
            'value' => '$data->getArrayManageBranch()[$data->gender]',
            'htmlOptions' => array('class' => 'item_c')
        ),
        array(
            'name' => 'Tên Chi Nhánh/ Địa Điểm',
            'type' => 'raw',
            'value' => '$data->getInfoBranch()',
            'htmlOptions' => array('class' => 'item_c')
        ),
        array(
            'name' => 'Mã',
            'value' => '$data->getCodeAccountBranch()',
            'htmlOptions' => array('class' => 'item_c')
        ),
        array(
            'name' => 'Công ty',
            'value' => '$data->getParent()',
            'htmlOptions' => array('class' => 'item_c')
        ),
        array(
            'name' => 'province_id',
            'value' => '$data->getProvince()',
        ),
        array(
            'name' => 'district_id',
            'value' => '$data->getDistrict()',
        ),
        array(
            'name' => 'created_date',
            'value' => '$data->getCreatedDate()',
            'htmlOptions' => array('class' => 'item_c')
        ),
        array(
            'name' => 'created_by',
            'value' => '$data->getCreatedBy()',
            'htmlOptions' => array('class' => 'item_c')
        ),
        array(
            'name' => 'Đại lý',
            'type' => 'raw',
            'value' => '$data->getAgentList()',
            'htmlOptions' => array('class' => 'item_c')
        ),
        array(
            'name' => 'status',
            'type' => 'raw',
            'value' => '$data->getStatusAgent()',
            'htmlOptions' => array('class' => 'item_c')
        ),
        array(
            'name' => 'Thực tế',
            'type' => 'raw',
            'value' => '$data->getRealStatusAgentOfBranch()',
            'htmlOptions' => array('class' => 'item_c')
        ),
        array(
            'header' => 'Giấy phép kinh doanh',
            'value' => '$data->getProfileView()',
            'type' => 'raw',
        ),
        array(
            'header' => 'Action',
            'class' => 'CButtonColumn',
            'template' => ControllerActionsName::createIndexButtonRoles($actions),
            'buttons' => array(
                'update' => array(
                    'visible' => '$data->canUpdateBranch()',
                ),
                'delete' => array(
                    'visible' => 'GasCheck::canDeleteData($data)',
                )
            ),
        ),
    ),
));
?>

<script>
    $(document).ready(function () {
        fnUpdateColorbox();
    });

    function fnUpdateColorbox() {
        fixTargetBlank();
//    jQuery('a.gallery').colorbox({ opacity:0.5 , rel:'group1' });
        $(".gallery").colorbox({iframe: true, innerHeight: '1500', innerWidth: '1050', close: "<span title='close'>close</span>"});
    }
</script>
