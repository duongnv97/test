<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>
<?php
    $aQty = array();
    for ($i = 0; $i <=100; $i=$i+5) {
        $aQty[$i] = $i;
    }
?>
	<div class="row more_col">
            <div class="col1">
                <?php echo $form->label($model,'qtykg_from',array('label'=> 'Sản lượng từ')); ?>
                <?php echo $form->textField($model,'qtykg_from',array('class' => 'number_only_v1')); ?>
                <?php // echo $form->dropDownList($model,'qtykg_from', $aQty ,array('class'=>'w-300', 'options' => array('0'=> array('selected'=>true)))); ?>
            </div>
            <div class="col2">
                <?php echo $form->label($model,'qtykg_to',array('label'=> 'Sản lượng đến')); ?>
		<?php echo $form->textField($model,'qtykg_to', array('class' => 'number_only_v1')); ?>
                <?php // echo $form->dropDownList($model,'qtykg_to', $aQty ,array('class'=>'w-300', 'options' => array('100'=> array('selected'=>true)))); ?>
            </div>
	</div>
    <div class="row">
    <?php echo $form->labelEx($model,'customer_id'); ?>
    <?php echo $form->hiddenField($model,'customer_id', array('class'=>'')); ?>
    <?php
            // 1. limit search kh của sale
            $url = Yii::app()->createAbsoluteUrl('admin/ajax/search_user_by_code');
            // widget auto complete search user customer and supplier
            $aData = array(
                'model'=>$model,
                'field_customer_id'=>'customer_id',
                'url'=> $url,
                'name_relation_user'=>'customer',
                'ClassAdd' => 'w-400',
                'field_autocomplete_name' => 'autocomplete_name',
            );
            $this->widget('ext.SpaAutocompleteCustomer.SpaAutocompleteCustomer',
                array('data'=>$aData));
            ?>
    </div>
    
    <?php // if(MyFormat::getCurrentRoleId() != ROLE_SALE):?>
    <div class="row">
        <?php echo $form->label($model,'ext_sale_id', array('label'=>'Nhân viên KD')); ?>
        <?php echo $form->hiddenField($model,'ext_sale_id'); ?>
        <?php
                    // widget auto complete search user customer and supplier
            $aData = array(
                'model'=>$model,
                'field_customer_id'=>'ext_sale_id',
                'field_autocomplete_name'=>'autocomplete_name_1',
                'url'=> Yii::app()->createAbsoluteUrl('admin/ajax/search_sale'),
                'name_relation_user'=>'rSale',
                'placeholder'=>'Nhập Tên Sale',
                'ClassAdd' => 'w-500',
//                        'fnSelectCustomer'=>'fnAfterSelectSaleOrAgent',
            );
            $this->widget('ext.SpaAutocompleteCustomer.SpaAutocompleteCustomer',
                array('data'=>$aData));                                        
        ?>
    </div>
    <?php // endif;?>
    <div class="row more_col">
            <div class="col1">
                <?php echo $form->labelEx($model,'date_from'); ?>
                 <?php 
                    $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                        'model'=>$model,        
                        'attribute'=>'date_from',
                        'options'=>array(
                            'showAnim'=>'fold',
                            'dateFormat'=> MyFormat::$dateFormatSearch,
                            'changeMonth' => true,
                            'changeYear' => true,
                            'showOn' => 'button',
                            'buttonImage'=> Yii::app()->theme->baseUrl.'/admin/images/icon_calendar_r.gif',
                            'buttonImageOnly'=> true,                                
                        ),        
                        'htmlOptions'=>array(
                            'class'=>'w-16',
                            'style'=>'height:20px;',
                                'readonly'=>'readonly',
                        ),
                    ));
                    ?>  
                <?php echo $form->error($model,'date_from'); ?>
            </div>
            <div class="col2">
                <?php echo $form->labelEx($model,'date_to'); ?>
                 <?php 
                    $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                        'model'=>$model,        
                        'attribute'=>'date_to',
                        'options'=>array(
                            'showAnim'=>'fold',
                            'dateFormat'=> MyFormat::$dateFormatSearch,
                            'changeMonth' => true,
                            'changeYear' => true,
                            'showOn' => 'button',
                            'buttonImage'=> Yii::app()->theme->baseUrl.'/admin/images/icon_calendar_r.gif',
                            'buttonImageOnly'=> true,                                
                        ),        
                        'htmlOptions'=>array(
                            'class'=>'w-16',
                            'style'=>'height:20px;',
                                'readonly'=>'readonly',
                        ),
                    ));
                    ?>  
                <?php echo $form->error($model,'date_to'); ?>
            </div>
        </div>
    
    <div class="row">
            <?php echo $form->label($model,'channel_id', array('label' => 'Trạng thái')); ?>
            <?php echo $form->dropDownList($model,'channel_id', Users::$STATUS_LAY_HANG,array('class'=>'w-200', 'empty'=>'Select')); ?>
            <?php echo $form->error($model,'channel_id'); ?>
	</div>
        
<!--	<div class="row buttons">
		<?php // echo CHtml::submitButton('Search'); ?>
	</div>-->
    <div class="row buttons" style="padding-left: 159px;">
            <?php $this->widget('bootstrap.widgets.TbButton', array(
                'buttonType'=>'submit',
                'label'=>'Xem thống kê',
                'type'=>'null', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
                'size'=>'small', // null, 'large', 'small' or 'mini'
                //'htmlOptions' => array('style' => 'margin-bottom: 10px; float: right;'),
            )); ?>
<!--            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a class='btn_cancel btnReset'>Reset</a>-->
        </div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->