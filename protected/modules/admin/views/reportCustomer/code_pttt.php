<?php

$this->breadcrumbs=array(
	$this->pageTitle,
);

$menus=array(
            array('label'=>"Create $this->singleTitle", 'url'=>array('create')),
);
$this->menu= ControllerActionsName::createMenusRoles($menus, $actions);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('code-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1><?php echo $this->pageTitle; ?></h1>


<?php echo CHtml::link('Tìm Kiếm Nâng Cao','#',array('class'=>'search-button')); ?>
<div class="search-form">
<?php $this->renderPartial('_search_code_pttt',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->
<?php 
$gridCss = Yii::app()->getAssetManager()->publish(Yii::getPathOfAlias('zii.widgets.assets')) . '/gridview/styles.css';
Yii::app()->getClientScript()->registerCssFile($gridCss);
?>
<?php if(!empty($aData)): ?>
<div class="grid-view">
    <table class="items sortable">
        <thead>
            <tr>
                <th>#</th>
                <th>Code</th>
                <th>Nhân viên</th>
                <th>Khách hàng</th>
                <th>Điện thoại</th>
                <th>Ngày tạo</th>
                <th>Ngày mua</th>
                <th class="ClosingBalance">Số phút chênh lệch</th>
            </tr>
        </thead>
        <tbody>
        <?php $i = 1; ?>
        <?php foreach ($aData as $value) : ?>
            <tr>
                <td><?php echo $i++; ?></td>
                <td class="item_c">
                    <?php echo $value['model']->getCode(); ?>
                </td>
                <td><?php echo $value['model']->getUser(); ?></td>
                <td><?php echo $value['model']->getCustomer(); ?></td>
                <td><?php echo $value['model']->getPhone(); ?></td>
                <td class="item_c">
                    <?php echo $value['model']->getCreatedDate(); ?>
                </td>
                <td class="item_c">
                    <?php echo $value['order_date']; ?>
                </td>
                <td class="item_c">
                    <?php echo $value['minute_diff']; ?>
                </td>
            </tr>
        <?php endforeach; ?>
        </tbody>
    </table>
</div>
<?php endif; ?>

<?php 
//$this->widget('zii.widgets.grid.CGridView', array(
//	'id'=>'code-grid',
//	'dataProvider'=>$model->getReportCodePttt(),
////        'afterAjaxUpdate'=>'function(id, data){ fnUpdateColorbox();}',
//        'template'=>'{pager}{summary}{items}{pager}{summary}',
////    'itemsCssClass' => 'items custom_here',
////    'htmlOptions'=> array('class'=>'grid-view custom_here'),
//        'pager' => array(
//            'maxButtonCount'=>  CmsFormatter::$PAGE_MAX_BUTTON,
//        ),
////	'enableSorting' => false,
//	//'filter'=>$model,
//	'columns'=>array(
//            array(
//                'header' => 'S/N',
//                'type' => 'raw',
//                'value' => '$this->grid->dataProvider->pagination->currentPage * $this->grid->dataProvider->pagination->pageSize + ($row+1)',
//                'headerHtmlOptions' => array('width' => '10px','style' => 'text-align:center;'),
//                'htmlOptions' => array('style' => 'text-align:center;')
//            ),
//            array(
//                'name'=>'code',
//                'type'=>'raw',
//                'value'=>'$data->getCode()',
//                'htmlOptions'=>array(
//                    'class'=>'item_c'
//                )
//            ),
//            array(
//                'name'=>'user_id',
//                'type'=>'raw',
//                'value'=>'$data->getUser()',
//            ),
//            array(
//                'name'=>'pttt_customer_id',
//                'type'=>'raw',
//                'value'=>'$data->getCustomer()',
//            ),
//            array(
//                'name'=>'Điện thoại',
//                'type'=>'raw',
//                'value'=>'$data->getPhone()',
//            ),
//            array(
//                'name'=>'created_date',
//                'type'=>'raw',
//                'value'=>'$data->getCreatedDate("d/m/Y")',
//                'htmlOptions'=>array(
//                    'class'=>'item_c'
//                )
//            ),
//            array(
//                'name'=>'date_delivery',
//                'type'=>'raw',
//                'value'=>'$data->getDateDelivery()',
//                'htmlOptions'=>array(
//                    'class'=>'item_c'
//                )
//            ),
//            
////            array(
////            'header' => 'Actions',
////            'class' => 'CButtonColumn',
////            'template' => ControllerActionsName::createIndexButtonRoles($actions),
////            'buttons' => array(
////                
////                ),
////            ),
//	),
//));
?>
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/sortElements/jquery.sortElements.js"></script>
<script>
$(function(){
var table = $('.sortable');
$('.ClosingBalance')
    .wrapInner('<span title="sort this column"/>')
    .each(function(){

        var th = $(this),
            thIndex = th.index(),
            inverse = false;

        th.click(function(){

            table.find('tbody td').filter(function(){

                return $(this).index() === thIndex;

            }).sortElements(function(a, b){
                var aText = $.text([a]);
                var bText = $.text([b]);
                    aText = parseFloat(aText.replace(/,/g, ''));
                    bText = parseFloat(bText.replace(/,/g, ''));
                    aText = aText *1;
                    bText = bText *1;

                if( aText == bText )
                    return 0;

//                return aText > bText ? // asc 
                return aText < bText ? // DESC
                    inverse ? 1 : -1
                    : inverse ? -1 : 1;
                    
            }, function(){
                // parentNode is the element we want to move
                return this.parentNode; 
            });

            inverse = !inverse;
            fnRefreshOrderNumber();
        });
    });
    
//    $('.ClosingBalance').trigger('click');
    
});
</script>
<script>
$(document).ready(function() {
    fnAddClassOddEven('items');
    fnUpdateColorbox();
});

function fnUpdateColorbox(){   
    fixTargetBlank();
}
</script>
<style>
    .sortable thead th:hover {cursor: pointer;}
</style>