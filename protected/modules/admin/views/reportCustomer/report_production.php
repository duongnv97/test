<?php

$this->breadcrumbs=array(
	$this->singleTitle,
);

$menus=array(
            array('label'=>"Create $this->singleTitle", 'url'=>array('create')),
);
?>
<?php 
$gridCss = Yii::app()->getAssetManager()->publish(Yii::getPathOfAlias('zii.widgets.assets')) . '/gridview/styles.css';
Yii::app()->getClientScript()->registerCssFile($gridCss);
?>
<h1>BÁO CÁO PHÂN LOẠI SẢN LƯỢNG KHÁCH HÀNG</h1>
<?php echo CHtml::link('Tìm Kiếm Nâng Cao','#',array('class'=>'search-button')); ?>
<div class="search-form">
<?php $this->renderPartial('_search_production',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->
<?php if (isset($data['aGasStoreCardDetail'])): ?>
<?php
    $aCustomer              = $data['aModelCustomer'];
    $aSale                  = $data['aModelSale'];
    $aGasStoreCardDetail    = $data['aGasStoreCardDetail'];
    $aCongNoVo              = $data['aCongNoVo'];
    $aCongNoTien            = $data['aCongNoTien'];
    $aSanLuongGanNhat       = $data['aSanLuongGanNhat'];
    $i                      = 0;
    $qtykg_from = !empty($_GET['GasStoreCard']['qtykg_from'])?$_GET['GasStoreCard']['qtykg_from']:0;
    $qtykg_to   = !empty($_GET['GasStoreCard']['qtykg_to'])?$_GET['GasStoreCard']['qtykg_to']:0;
    ?>

<div class="grid-view">
    <h2 class="item_b">Báo cáo phân loại sản lượng khách hàng <?php echo date("d/m/Y", strtotime($model->date_from)); ?> - <?php echo date("d/m/Y", strtotime($model->date_to)); ?></h2>
    <table class="items">
        <thead>
            <tr>
                <th>STT</th>
                <th>Mã KH</th>
                <th style="width: 200px;">Tên KH</th>
                <th style="width: 250px;">Địa chỉ</th>
                <th>Sale</th>
                <th>Sản lượng</th>
                <th class="xemchitietslgn" onclick="xemChiTietTatCa(1)">Sản lượng gần nhất</th>
                <th>Ngày lấy hàng gần nhất</th>
                <th>Công nợ vỏ</th>
                <th>Công nợ tiền</th>
                <th>Trạng thái</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($aGasStoreCardDetail as $customer_id => $qtyKg): ?>
            <?php if ($qtyKg >= $qtykg_from && $qtyKg <= $qtykg_to) :?>
            <?php 
                $mCustomer = $aCustomer[$customer_id];
                $mCustomer->last_purchase;
                $mCustomer->sale_id;
                $saleName = isset($aSale[$mCustomer->sale_id]) ? $aSale[$mCustomer->sale_id]->first_name : $mCustomer->sale_id;
                if (!empty($aCongNoVo))
                    $aInfoCongNoVo = $aCongNoVo[$customer_id];
                $aInfoCongNoTien = isset($aCongNoTien[$customer_id]) ? $aCongNoTien[$customer_id] : [];
                $sanLuongGanNhat = isset($aSanLuongGanNhat[$customer_id]) ? $aSanLuongGanNhat[$customer_id] : [];
                $sumSanLuongGanNhat = 0;
            ?>
            <tr>
                <td><?php echo ++$i;  ?></td>
                <td><?php echo $mCustomer->code_bussiness ?></td>
                <td><?php echo $mCustomer->first_name ?></td>
                <td><?php echo $mCustomer->address ?></td>
                <td><?php echo $saleName;?></td>
                <td class="item_r">
                    <?php echo ActiveRecord::formatCurrency($qtyKg); ?>
                </td>
                <td>
                    <?php if (!empty($sanLuongGanNhat)): ?>
                    <div class="theothang" style="display: none;">
                    <?php
                    foreach ($sanLuongGanNhat as $month => $qtyKg2):
                        $sumSanLuongGanNhat += $qtyKg2;
                        echo '<b>'.$month . ': </b>' . ActiveRecord::formatCurrency($qtyKg2) . '<br/>';
                    endforeach;
                    ?>
                    </div>
                    <div onclick="xemTheoThang(this)">
                    <?php echo '<b>Tổng: </b>' . ActiveRecord::formatCurrency($sumSanLuongGanNhat); ?>
                    </div>
                    <?php endif;?>
                </td>
                <td class="item_r">
                    <?php echo $mCustomer->last_purchase; ?>
                </td>
                <!--xem lai phan nay-->
                <?php if (!empty($aCongNoVo)): ?>
                <td>
                    <?php echo "<b>Vỏ nhỏ: </b>" . ActiveRecord::formatCurrency($aInfoCongNoVo['vo_nho']) . "<br/>"; ?>
                    <?php echo "<b>Vỏ lớn: </b>" . ActiveRecord::formatCurrency($aInfoCongNoVo['vo_lon']) . "<br/>"; ?>
                    <?php echo "<b>Tổng: </b>" . ActiveRecord::formatCurrency($aInfoCongNoVo['tong']); ?>
                </td>
                <?php endif; ?>
                <td>
                    <?php echo ActiveRecord::formatCurrency($aInfoCongNoTien ? : 0); ?>
                </td>
                <td><?php echo Users::$STATUS_LAY_HANG[$mCustomer->channel_id]; ?></td>
            </tr>
            <?php endif; ?>
            <?php endforeach; ?>
        </tbody>
    </table>
</div>
<?php endif; ?>
<script>
    $(document).ready(function() {
    fnAddClassOddEven('items');
        /**
        * @Author: LOCNV MAY 3, 2019
        * @Todo: something
        * @Param:  
        */
    });
    function xemTheoThang(obj){
        $(obj).parent().find(".theothang").css("display", "block");
    }
    function xemChiTietTatCa(state){
        if (state === 1) {
            $(".theothang").css("display", "block");
            $(".xemchitietslgn").attr('onclick','xemChiTietTatCa(0)');
        } else {
            $(".theothang").css("display", "none");
            $(".xemchitietslgn").attr('onclick','xemChiTietTatCa(1)');
        }
    }
</script>