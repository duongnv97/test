<?php

$this->breadcrumbs=array(
	$this->singleTitle,
);

$menus=array(
            array('label'=>"Create $this->singleTitle", 'url'=>array('create')),
            array(
                    'label'=>"Export Excel", 
                    'url'=>array('ReportHardDelivery', 'excel'=>1),
                    'linkOptions'=>array(
                        'style'=>'background:url('.Yii::app()->theme->baseUrl.'/admin/images/icon/excel.png)',
                    ),
                    'htmlOptions'=>array(
                        'label'=>'Xuất Excel',
                        'class'=>'update',
                        ),
                ),
);
$this->menu= ControllerActionsName::createMenusRoles($menus, $actions);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('follow-customer-grid', {
                url : $(this).attr('action'),
		data: $(this).serialize()
	});
	return false;
});
");

Yii::app()->clientScript->registerScript('ajaxupdate', "
$('#follow-customer-grid a.ajaxupdate').live('click', function() {
    $.fn.yiiGridView.update('follow-customer-grid', {
        type: 'POST',
        url: $(this).attr('href'),
        success: function() {
            $.fn.yiiGridView.update('follow-customer-grid');
        }
    });
    return false;
});
");
$gridCss = Yii::app()->getAssetManager()->publish(Yii::getPathOfAlias('zii.widgets.assets')) . '/gridview/styles.css';
Yii::app()->getClientScript()->registerCssFile($gridCss);
?>

<h1>Danh Sách <?php echo $this->pluralTitle; ?></h1>


<?php echo CHtml::link('Tìm Kiếm Nâng Cao','#',array('class'=>'search-button')); ?>
<div class="search-form">
<?php $this->renderPartial('_search_hard_delivery',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php if(!empty($data['output'])): ?>
<div class="grid-view">
    <h2 class="item_b">Bảng tính phụ cấp giao hàng bình bò tháng <?php echo date("m/Y", strtotime($model->date_from)); ?></h2>
    <table class="items">
        <thead>
            <tr>
                <th>Mã khách hàng</th>
                <th>Khách hàng</th>
                <th>Sale</th>
                <th>giá B12</th>
                <th>giá B45</th>
                <?php foreach ($data['binh'] as $value) : ?>
                <th><?php echo $value; ?></th>
                <?php endforeach; ?>
                <th>Thành tiền</th>
            </tr>
        </thead>
        <tbody>
            <?php
            $aCustomer = $data['customer'];
            $aSale     = $data['sale'];
            $aEmployee = $data['employee'];
            foreach ($data['output'] as $customer_id => $value):
                foreach ($value as $sale_id => $detail):
            ?>
            <tr>
                <td><?php echo isset($aCustomer[$customer_id])   ? $aCustomer[$customer_id]->code_bussiness : ""; ?></td>
                <td><?php echo isset($aCustomer[$customer_id])   ? $aCustomer[$customer_id]->first_name : ""; ?></td>
                <td><?php echo isset($aSale[$sale_id]) ? $aSale[$sale_id]->first_name : ""; ?></td>
                <td class="item_r">
                    <?php echo isset($data['price'][$customer_id]['price12']) ? ActiveRecord::formatCurrency($data['price'][$customer_id]['price12']) : 0; ?>
                </td>
                <td class="item_r">
                    <?php echo isset($data['price'][$customer_id]['price45']) ? ActiveRecord::formatCurrency($data['price'][$customer_id]['price45']) : 0; ?>
                </td>
                <?php foreach ($data['binh'] as $id_binh => $value) : ?>
                <td class="item_r"><?php echo isset($detail[$id_binh]) ? ActiveRecord::formatNumberInput($detail[$id_binh]) : 0; ?></td>
                <?php endforeach; ?>
                <td class="item_r"><?php echo ActiveRecord::formatCurrency($detail['amount']); ?></td>
            </tr>
            <?php
                endforeach;
            endforeach;
            ?>
            <?php if(isset($data['sum'])): ?>
            <tr>
                <td colspan="6" class="item_r item_b">Tổng cộng</td>
                <td class="item_r item_b"><?php echo ActiveRecord::formatCurrency($data['sum']); ?></td>
            </tr>
            <?php endif; ?>
        </tbody>
    </table>
    
    <h2 class="item_b">Danh sách nhân viên giao hàng khó tháng <?php echo date("m/Y", strtotime($model->date_from)); ?></h2>
    <table class="items">
        <thead>
            <tr>
                <th>STT</th>
                <th>Nhân viên</th>
                <th>Chức vụ</th>
                <th>Bộ phận</th>
                <th>Số tiền</th>
            </tr>
        </thead>
        <tbody>
            <?php
            $i = 1;
            $aRoles = Roles::loadItems();
            foreach ($data['employee'] as $id => $mUser):
            ?>
            <tr>
                <td><?php echo $i++; ?></td>
                <td><?php echo $mUser->first_name; ?></td>
                <td><?php echo isset($aRoles[$mUser->role_id]) ? $aRoles[$mUser->role_id] : ""; ?></td>
                <td><?php echo isset($data['part'][$mUser->parent_id]) ? $data['part'][$mUser->parent_id]->first_name : ""; ?></td>
                <td class="item_r"><?php echo isset($data['emp_money'][$mUser->id]) ? ActiveRecord::formatCurrency($data['emp_money'][$mUser->id]) : 0; ?></td>
            </tr>
            <?php endforeach; ?>
            <?php if(isset($data['sum'])): ?>
            <tr>
                <td colspan="4" class="item_r item_b">Tổng cộng</td>
                <td class="item_r item_b"><?php echo ActiveRecord::formatCurrency($data['sum']); ?></td>
            </tr>
            <?php endif; ?>
        </tbody>
    </table>
</div>
<?php endif; ?>


<script>
$(document).ready(function() {
    fnAddClassOddEven('items');
    fnUpdateColorbox();
});

function fnUpdateColorbox(){   
    fixTargetBlank();
}
</script>