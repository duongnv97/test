<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=> GasCheck::getCurl(),
	'method'=>'get',
)); ?>

	<div class="row">
            <?php echo $form->label($model,'agent_id'); ?>
            <?php echo $form->hiddenField($model,'agent_id', array('class'=>'')); ?>
            <?php
                // 1. limit search kh của sale
                $url = Yii::app()->createAbsoluteUrl('admin/ajax/search_by_role', array('role'=> ROLE_AGENT));
                // widget auto complete search user customer and supplier
                $aData = array(
                    'model'=>$model,
                    'field_customer_id'=>'agent_id',
                    'url'=> $url,
                    'name_relation_user'=>'rAgent',
                    'ClassAdd' => 'w-400',
                    'field_autocomplete_name' => 'autocomplete_name_1',
                    'placeholder'=>'Nhập mã hoặc tên đại lý',
                );
                $this->widget('ext.SpaAutocompleteCustomer.SpaAutocompleteCustomer',
                    array('data'=>$aData));
                ?>
            <?php echo $form->error($model,'agent_id'); ?>
	</div>

        <div class="row ">
        <?php echo $form->label($model,'uid_login'); ?>
        <?php echo $form->hiddenField($model,'uid_login', array('class'=>'')); ?>
        <?php
                // 1. limit search kh của sale
                $url = Yii::app()->createAbsoluteUrl('admin/ajax/search_user_login', ['PaySalary' => 1]);
                // widget auto complete search user customer and supplier
                $aData = array(
                    'model'=>$model,
                    'field_customer_id'=>'uid_login',
                    'url'=> $url,
                    'name_relation_user'=>'rUidLogin',
                    'ClassAdd' => 'w-400',
                    'field_autocomplete_name' => 'autocomplete_name',
                    'placeholder'=>'Nhập mã NV hoặc tên',
                );
                $this->widget('ext.SpaAutocompleteCustomer.SpaAutocompleteCustomer',
                    array('data'=>$aData));
                ?>
            <?php echo $form->error($model,'uid_login'); ?>
        </div>

    <div class="row">
        <?php echo $form->labelEx($model,'province_id'); ?>
        <div class="fix-label">
            <?php
            $this->widget('ext.multiselect.JMultiSelect',array(
                'model'=>$model,
                'attribute'=>'province_id',
                'data'=> GasProvince::getArrAll(),
                // additional javascript options for the MultiSelect plugin
                'options'=>array('selectedList' => 30,),
                // additional style
                'htmlOptions'=>array('style' => 'width: 800px;'),
            ));
            ?>
        </div>
    </div>

	<div class="row more_col">
            <div class="col1">
                <?php echo $form->label($model,'date_from'); ?>
                <?php 
                    $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                        'model'=>$model,        
                        'attribute'=>'date_from',
                        'options'=>array(
                            'showAnim'=>'fold',
                            'dateFormat'=> MyFormat::$dateFormatSearch,
    //                            'minDate'=> '0',
    //                        'maxDate'=> '0',
                            'changeMonth' => true,
                            'changeYear' => true,
                            'showOn' => 'button',
                            'buttonImage'=> Yii::app()->theme->baseUrl.'/admin/images/icon_calendar_r.gif',
                            'buttonImageOnly'=> true,                                
                        ),        
                        'htmlOptions'=>array(
                            'class'=>'w-150',
                            'style'=>'float:left;',                               
                        ),
                    ));
                ?>
            </div>
            <div class="col2">
                <?php echo $form->label($model,'date_to'); ?>
                <?php 
                    $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                        'model'=>$model,        
                        'attribute'=>'date_to',
                        'options'=>array(
                            'showAnim'=>'fold',
                            'dateFormat'=> MyFormat::$dateFormatSearch,
    //                            'minDate'=> '0',
    //                        'maxDate'=> '0',
                            'changeMonth' => true,
                            'changeYear' => true,
                            'showOn' => 'button',
                            'buttonImage'=> Yii::app()->theme->baseUrl.'/admin/images/icon_calendar_r.gif',
                            'buttonImageOnly'=> true,                                
                        ),        
                        'htmlOptions'=>array(
                            'class'=>'w-150',
                            'style'=>'float:left;',
                        ),
                    ));
                ?>
            </div>
        </div>

	<div class="row buttons" style="padding-left: 159px;">
		        <?php $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType'=>'submit',
            'label'=>'Search',
            'type'=>'null', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
            'size'=>'small', // null, 'large', 'small' or 'mini'
            //'htmlOptions' => array('style' => 'margin-bottom: 10px; float: right;'),
        )); ?>
    </div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->