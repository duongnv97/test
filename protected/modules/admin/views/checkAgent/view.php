<?php
$this->breadcrumbs=array(
    $this->pluralTitle => array('index'),
    ' ' . $this->singleTitle . ' : ',
);

$menus = array(
    array('label' => 'Xuất Excel', 'url' => array('view', 'id' => $model->id, 'to_excel' => 1), 'htmlOptions'=>array('class'=>'export_excel','label'=>'Xuất Excel')),
	array('label'=>"$this->pluralTitle Management", 'url'=>array('index')),
	array('label'=>"Tạo Mới $this->singleTitle", 'url'=>array('create')),
	array('label'=>"Cập Nhật $this->singleTitle", 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>"Xóa $this->singleTitle", 'url'=>array('delete'), 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Bạn chắc chắn muốn xóa ?')),
);
$this->menu= ControllerActionsName::createMenusRoles($menus, $actions);
?>

<h1>Xem: <?php echo $this->singleTitle; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
                [
                    'name' => 'agent_id',
                    'value' => $model->getAgent(),
                ],
                [
                    'name' => 'uid_login',
                    'value' => $model->getUidLogin(),
                ],
                [
                    'name' => 'created_date',
                    'value' => $model->getCreatedDate(),
                ],
                [
                    'label' => 'Hình',
                    'type' => 'raw',
                    'value' => $model->getImages()
                ],
		'note',
	),
)); 
$aStore = $aData['STORE'];
$aCashBook = $aData['CASHBOOK'];
$aMaterialId = [];
foreach ($aStore as $key => $value) {
    if($value['material_id']){
        $aMaterialId[] =  $value['material_id'];
    }
}
$aGasMaterials = GasMaterials::getArrayModel($aMaterialId);
?>
<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/print-store-card.css" />
<center class="">
    <table cellpadding="0" cellspacing="0" class="tb hm_table">
        <thead>
            <tr>
                <th colspan="9" >Phần 1: Kho<br></th>
            </tr>
        </thead>
        <thead>
            <tr>
                <th>Loại</th>
                <th class="w-100">Mã Vật tư</th>
                <th>Tên Vật tư</th>
                <th>Tồn đầu</th>
                <th>Nhập kho</th>
                <th>Xuất kho</th>
                <th>Tồn cuối</th>
                <th>Số thực tế</th>
                <th class="w-80">Lệch</th>
            </tr>
        </thead>
        <?php if(!empty($aStore)) : // check empty?>
        <tbody>
            <?php foreach ($aStore as $key => $value): 
                $mGasMaterials = isset($aGasMaterials[$value['material_id']]) ? $aGasMaterials[$value['material_id']] : '';
            ?>
            <tr>

                <td class="item_l"><?= $value['type'] ?></td>
                <td class="item_l"><?= ($mGasMaterials) ? $mGasMaterials->materials_no : '' ?></td>
                <td class="item_l"><?= ($mGasMaterials) ? $mGasMaterials->name : ''  ?></td>
                <td class="item_r"><?= ActiveRecord::formatCurrency($value['open']) ?></td> 
                <td class="item_r"><?= ActiveRecord::formatCurrency($value['import']) ?></td>
                <td class="item_r"><?= ActiveRecord::formatCurrency($value['export']) ?></td>
                <td class="item_r end" ><?= ActiveRecord::formatCurrency($value['end']) ?></td>
                <td class="item_r current"><?= ActiveRecord::formatCurrency($value['current']) ?></td>
                <td class="item_r difference_txt"><?= ActiveRecord::formatCurrency($value['difference']) ?></td>
            </tr>
            <?php endforeach; ?>                             
        </tbody>  
        <?php  endif; ?>
        <thead>
            <tr>
                <th colspan="9" >Phần 2: Quỹ<br></th>
            </tr>
        </thead>
        <thead>
            <tr>
                <th colspan="3" >Quỹ đại lý</th>
                <th>Tồn đầu</th>
                <th>Thu</th>
                <th>Chi</th>
                <th>Tồn cuối</th>
                <th>Số thực tế</th>
                <th>Lệch</th>
            </tr>
        </thead>
        <?php if(!empty($aCashBook)) :?>
        <tbody>
            <input name="CheckAgent[CASHBOOK][open]" value="<?= $aCashBook['open'] ?>" style="display:none">
            <input name="CheckAgent[CASHBOOK][import]" value="<?= $aCashBook['import'] ?>" style="display:none">
            <input name="CheckAgent[CASHBOOK][export]" value="<?= $aCashBook['export'] ?>" style="display:none">
            <input name="CheckAgent[CASHBOOK][end]" value="<?= $aCashBook['end'] ?>" style="display:none">
            <tr>
                <td class="item_l" colspan="3"></td>
                <td class="item_r"><?= ActiveRecord::formatCurrency($aCashBook['open']) ?></td>
                <td class="item_r"><?= ActiveRecord::formatCurrency($aCashBook['import']) ?></td>
                <td class="item_r"><?= ActiveRecord::formatCurrency($aCashBook['export']) ?></td>
                <td class="item_r end"><?= ActiveRecord::formatCurrency($aCashBook['end']) ?></td>
                <td class="item_r current"><?= ActiveRecord::formatCurrency($aCashBook['current']) ?></td>
                <td class="item_r difference_txt"><?= ActiveRecord::formatCurrency($aCashBook['difference']) ?></td>
            </tr>
        </tbody> 
        <?php  endif; ?>
    </table>
    <br>
</center>
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/jquery.floatThead.min.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/underscore-min.js"></script> 
<script type="text/javascript">
String.prototype.replaceAll = function(search, replacement) {
    var target = this;
    return target.replace(new RegExp(search, 'g'), replacement);
};
$( ".current" ).keyup(function() {
    let parent = $(this).parent().parent();
    var end = parseFloat(parent.find('.end').text().replaceAll(/,/,""));
    let result = end - parseFloat($(this).val());
    parent.find('.difference_txt').text(result);
    parent.find(' input.difference').val(result);
});
$(window).load(function () { // không dùng dc cho popup
        $(".gallery").colorbox({iframe:true,innerHeight:'1100', innerWidth: '1000',close: "<span title='close'>close</span>"});
    });
</script>
