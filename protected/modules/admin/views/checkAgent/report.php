<?php

$this->breadcrumbs=array(
	$this->singleTitle,
);

$menus=array(
            array('label'=>"Create $this->singleTitle", 'url'=>array('create')),
);
$aUidLogin = isset($aData['aUID_LOGIN']) ? $aData['aUID_LOGIN'] : [];
$aData = isset($aData['aDATA']) ? $aData['aDATA'] : [];

?>
<?php 
$gridCss = Yii::app()->getAssetManager()->publish(Yii::getPathOfAlias('zii.widgets.assets')) . '/gridview/styles.css';
Yii::app()->getClientScript()->registerCssFile($gridCss);
?>
<h1>Báo cáo kiểm kho đại lý</h1>
<?php echo CHtml::link('Tìm Kiếm Nâng Cao','#',array('class'=>'search-button')); ?>
<div class="search-form">
<?php $this->renderPartial('_search_report',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->
<?php // if (isset($data['aGasStoreCardDetail'])): ?>
<?php if( empty($aData) ) return; ?>

<div class="grid-view">
    <!--<h2 class="item_b">Báo cáo kiểm kho</h2>-->
    <table class="hm_table items">
        <thead>
            <tr>
                <th>STT</th>
                <th>KTKV</th>
                <th style="width: 200px;">Số đại lý KTKV quản lý</th>
                <th style="width: 250px;">Số đại lý đã kiểm tra</th>
                <th style="width: 250px;">Số lần đã kiểm tra</th>
            </tr>
        </thead>
        <tbody>
            <?php $i = 1; ?>
            <?php foreach ($aUidLogin as $uid_login => $ktkvql): ?>
            <tr>
                <td style="text-align: center;" ><?php echo $i++; ?></td>
                <td><?php echo $aData[$uid_login]['ktkv_name'];  ?></td>
                <td style="text-align: center;"><?php echo $ktkvql;  ?></td>
                <td style="text-align: center;">
                    <a target="_blank" 
                       href="<?php echo Yii::app()->createAbsoluteUrl('admin/checkAgent', 
                               [
                                   'CheckAgent[uid_login]'=> $uid_login, 
                                   'CheckAgent[date_from]' => $model->date_from, 
                                   'CheckAgent[date_to]' => $model->date_to
                               ]) ?>">
                           <?php echo $aData[$uid_login]['ktkvkt'];  ?>
                    </a>
                </td>
                <td style="text-align: center;"><?php echo $aData[$uid_login]['sl_ktkvkt']; ?></td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
</div>
<?php // endif; ?>