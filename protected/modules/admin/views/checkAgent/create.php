<?php
$this->breadcrumbs=array(
	$this->pluralTitle=>array('index'),
	'Tạo Mới',
);

$menus = array(		
        array('label'=>"$this->singleTitle Management", 'url'=>array('index')),
);
$this->menu= ControllerActionsName::createMenusRoles($menus, $actions);

Yii::app()->clientScript->registerScript('search', "
$('.init-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('check-agent-grid', {
                url : $(this).attr('action'),
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Tạo Mới <?php echo $this->singleTitle; ?></h1>

<div class="search-form" style="<?php empty($aData) ? '' : 'display:none' ?>">
<?php $this->renderPartial('_init',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->
<?php
if(!empty($aData)){
    echo $this->renderPartial('_form', array('model'=>$model,'aData'=>$aData));
}
?>