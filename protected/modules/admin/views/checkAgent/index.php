<?php
$this->breadcrumbs=array(
	$this->singleTitle,
);

$menus=array(
    array('label' => 'Xuất Excel', 'url' => array('index', 'to_excel' => 1), 'htmlOptions'=>array('class'=>'export_excel','label'=>'Xuất Excel')),
            array('label'=>"Create $this->singleTitle", 'url'=>array('create')),
);
$this->menu= ControllerActionsName::createMenusRoles($menus, $actions);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('check-agent-grid', {
                url : $(this).attr('action'),
		data: $(this).serialize()
	});
	return false;
});
");

Yii::app()->clientScript->registerScript('ajaxupdate', "
$('#check-agent-grid a.ajaxupdate').live('click', function() {
    $.fn.yiiGridView.update('check-agent-grid', {
        type: 'POST',
        url: $(this).attr('href'),
        success: function() {
            $.fn.yiiGridView.update('check-agent-grid');
        }
    });
    return false;
});
");
?>

<h1>Lịch kiểm kho đại lý</h1>


<?php echo CHtml::link('Tìm Kiếm Nâng Cao','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'check-agent-grid',
	'dataProvider'=>$model->search(),
        'afterAjaxUpdate'=>'function(id, data){ fnUpdateColorbox();}',
        'template'=>'{pager}{summary}{items}{pager}{summary}',
//    'itemsCssClass' => 'items custom_here',
//    'htmlOptions'=> array('class'=>'grid-view custom_here'),
        'pager' => array(
            'maxButtonCount'=>  CmsFormatter::$PAGE_MAX_BUTTON,
        ),
	'enableSorting' => false,
	//'filter'=>$model,
	'columns'=>array(
        array(
            'header' => 'S/N',
            'type' => 'raw',
            'value' => '$this->grid->dataProvider->pagination->currentPage * $this->grid->dataProvider->pagination->pageSize + ($row+1)',
            'headerHtmlOptions' => array('width' => '10px','style' => 'text-align:center;'),
            'htmlOptions' => array('style' => 'text-align:center;')
        ),
            array(
                'name' => 'agent_id',
                'type' => 'text',
                'value' => '$data->getAgent()',
            ),
            array(
                'header' => 'KTKV',
                'name' => 'uid_login',
                'value' => '$data->getUidLogin()',
            ),
            array(
                'name' => 'created_date',
                'value' => '$data->getCreatedDate()',
            ),
            array(
                'header' => 'Hình',
                'type' => 'raw',
                'value' => '$data->getImages()',
            ),
            'note',
		array(
                    'header' => 'Actions',
                    'class'=>'CButtonColumn',
                    'template'=> ControllerActionsName::createIndexButtonRoles($actions),
                    'buttons'=>array(
                        'update' => array(
                            'visible' => '$data->canUpdate()',
                        ),
                        'delete'=>array(
                            'visible'=> 'GasCheck::canDeleteData($data)',
                        ),
                    ),
		),
	),
)); ?>

<script>
$(document).ready(function() {
    fnUpdateColorbox();
});
$(window).load(function () { // không dùng dc cho popup
        $(".gallery").colorbox({iframe:true,innerHeight:'1100', innerWidth: '1000',close: "<span title='close'>close</span>"});
    });
function fnUpdateColorbox(){   
    fixTargetBlank();
}
</script>