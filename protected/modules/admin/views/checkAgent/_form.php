<?php
$LinkAction     = Yii::app()->createAbsoluteUrl("admin/checkAgent/create");
$aStore         = $aData['STORE'];
$aCashBook      = $aData['CASHBOOK'];
$aMaterialId    = [];
foreach ($aStore as $key => $value) {
    if($value['material_id']){
        $aMaterialId[] =  $value['material_id'];
    }
}
$aGasMaterials = GasMaterials::getArrayModel($aMaterialId);

?>
<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/print-store-card.css" />
<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/admin/css/form.css" />
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/jquery.floatThead.min.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/underscore-min.js"></script> 
<div class="form">
<!--<form method="post" action="<?php // $LinkAction ?>">-->
<?php echo MyFormat::BindNotifyMsg(); ?>
    
    
<?php 
$form=$this->beginWidget('CActiveForm', array(
	'id'=>'call-review-form',
	'enableAjaxValidation'=>false,
        'htmlOptions' => array('enctype' => 'multipart/form-data'),
)); ?>

<input name="CheckAgent[agent_id]" value="<?= $model->agent_id ?>" style="display:none">
    
    <div class="row buttons" style="padding-left: 750px;">
        <?php $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType'=>'submit',
            'label'=>'Lưu kết quả',
            'type'=>'null', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
            'size'=>'small', // null, 'large', 'small' or 'mini'
            //'htmlOptions' => array('style' => 'margin-bottom: 10px; float: right;'),
        )); ?>
    </div>
    <table cellpadding="0" cellspacing="0" class="tb hm_table tb_sticky">
        <thead>
            <tr>
                <th colspan="14" >Phần 1: Kho<br></th>
            </tr>
        </thead>
        <thead>
            <tr>
                <th>Loại</th>
                <th class="w-100">Mã Vật tư</th>
                <th>Tên Vật tư</th>
                <th>Tồn đầu</th>
                <th>Nhập kho</th>
                <th>Xuất kho</th>
                <th>Tồn cuối</th>
                <th class="w-100">Số thực tế</th>
                <th>Lệch</th>
<!--                <th>Nhập XL</th>
                <th>Xuất XL</th>
                <th>Tiền nhập XL</th>
                <th>Tiền xuất XL</th>
                <th>Điều chỉnh</th>-->
            </tr>
        </thead>
        <?php if(!empty($aStore)) : // tbody ?>
        <tbody>
            <?php foreach ($aStore as $key => $value): 
                $mGasMaterials  = isset($aGasMaterials[$value['material_id']]) ? $aGasMaterials[$value['material_id']] : '';
                $open           = empty($value['open']) ? 0 : $value['open'];
                $import         = empty($value['import']) ? 0 : $value['import'];
                $export         = empty($value['export']) ? 0 : $value['export'];
                $end            = empty($value['end']) ? 0 : $value['end'];
                $current        = empty($value['current']) ? 0 : $value['current'];
                if($model->scenario == 'create'){
                    $current = $end;
                }
                $difference     = empty($value['difference']) ? 0 : $value['difference'];
            ?>
            <tr>
                <td class="item_l">
                    <input name="CheckAgent[STORE][<?= $key ?>][type]" value="<?= $value['type'] ?>" style="display:none">
                    <input name="CheckAgent[STORE][<?= $key ?>][material_id]" value="<?= $value['material_id'] ?>" style="display:none">
                    <?php if(isset($value['materials_type_id'])){ ?>
                        <input name="CheckAgent[STORE][<?= $key ?>][materials_type_id]" value="<?= $value['materials_type_id'] ?>" style="display:none">
                    <?php } ?>
                    <input name="CheckAgent[STORE][<?= $key ?>][open]" value="<?= $open ?>" style="display:none">
                    <input name="CheckAgent[STORE][<?= $key ?>][import]" value="<?= $import ?>" style="display:none">
                    <input name="CheckAgent[STORE][<?= $key ?>][export]" value="<?= $export ?>" style="display:none">
                    <input name="CheckAgent[STORE][<?= $key ?>][end]" value="<?= $end ?>" style="display:none">
                    <?= $value['type'] ?>
                </td>
                <td class="item_l"><?= ($mGasMaterials) ? $mGasMaterials->materials_no : '' ?></td>
                <td class="item_l"><?= ($mGasMaterials) ? $mGasMaterials->name : ''  ?></td>
                <td class="item_r"><?= ActiveRecord::formatCurrency($open) ?></td> 
                <td class="item_r"><?= ActiveRecord::formatCurrency($import) ?></td>
                <td class="item_r"><?= ActiveRecord::formatCurrency($export) ?></td>
                <td class="item_r end" ><?= ActiveRecord::formatCurrency($end) ?></td>
                <td class="item_l"><input style="font-size: initial;" class="current number_only_v1 w-100" name="CheckAgent[STORE][<?= $key ?>][current]" value="<?= $current ?>"  ></td>
                <td class="item_r difference_txt"><?= ActiveRecord::formatCurrency($difference) ?></td>
                <td style="display:none"><input class="difference" name="CheckAgent[STORE][<?= $key ?>][difference]" value="<?= $difference ?>" style="display:none"></td>
<!--                <td class="item_r input_number_txt">
                    Cột nhập xử lý (lệch >0)
                    <?php // echo $difference > 0 ? ActiveRecord::formatCurrency($difference) : '' ?>
                </td>
                <td class="item_r output_number_txt">
                    Cột xuất xử lý (lệch <0)
                    <?php // echo $difference < 0 ? ActiveRecord::formatCurrency(abs($difference)) : '' ?>
                </td>
                <td class="item_r">
                    Tiền Nhập XL 
                </td>
                <td class="item_r">
                    Tiền Xuất XL 
                </td>
                <td class="item_r">
                    <?php 
//                    $cssClass = 'dropdown-store-card';
//                    $disabled = '';
//                    if( $difference == 0 ){
//                        $cssClass .= ' display_none';
//                        $disabled  = 'disabled';
//                    }
                    ?>
                    <?php // echo $form->dropdownList($model,"store_card_type[$key]", CheckAgent::$aStoreCardModify, array('class'=>$cssClass, $disabled=>$disabled)); ?>
                </td>-->
            </tr>
            <?php endforeach; ?>                             
        </tbody>  
        <?php  endif; ?>
        
        <thead>
            <tr>
                <th colspan="14" >Phần 2: Quỹ<br></th>
            </tr>
        </thead>
        <thead>
            <tr>
                <th colspan="3" >Quỹ đại lý</th>
                <th>Tồn đầu</th>
                <th>Thu</th>
                <th>Chi</th>
                <th>Tồn cuối</th>
                <th>Số thực tế</th>
                <th>Lệch</th>
            </tr>
        </thead>
        <?php if(!empty($aCashBook)) :?>
        <?php 
            $cashbookOpen       = empty($aCashBook['open']) ? 0 : $aCashBook['open'];
            $cashbookImport     = empty($aCashBook['import']) ? 0 : $aCashBook['import'];
            $cashbookExport     = empty($aCashBook['export']) ? 0 : $aCashBook['export'];
            $cashbookEnd        = empty($aCashBook['end']) ? 0 : $aCashBook['end'];
            $cashbookCurrent    = empty($aCashBook['current']) ? 0 : $aCashBook['current'];
            if($model->scenario == 'create'){
                $cashbookCurrent = $cashbookEnd;
            }
            $cashbookDifference = empty($aCashBook['difference']) ? 0 : $aCashBook['difference'];
        ?>
        <tbody>
            <input name="CheckAgent[CASHBOOK][open]" value="<?= $aCashBook['open'] ?>" style="display:none">
            <input name="CheckAgent[CASHBOOK][import]" value="<?= $aCashBook['import'] ?>" style="display:none">
            <input name="CheckAgent[CASHBOOK][export]" value="<?= $aCashBook['export'] ?>" style="display:none">
            <input name="CheckAgent[CASHBOOK][end]" value="<?= $aCashBook['end'] ?>" style="display:none">
            <tr>
                <td class="item_l" colspan="3"></td>
                <td class="item_r"><?= ActiveRecord::formatCurrency($cashbookOpen) ?></td>
                <td class="item_r"><?= ActiveRecord::formatCurrency($cashbookImport) ?></td>
                <td class="item_r"><?= ActiveRecord::formatCurrency($cashbookExport) ?></td>
                <td class="item_r end"><?= ActiveRecord::formatCurrency($cashbookEnd) ?></td>
                <td class="item_l "><input style="font-size: initial;" class="current number_only_v1 w-100" name="CheckAgent[CASHBOOK][current]" value="<?= $cashbookCurrent ?>"></td>
                <td class="item_r difference_txt"><?= ActiveRecord::formatCurrency($cashbookDifference) ?></td>
                <td style="display:none"><input class="difference" name="CheckAgent[CASHBOOK][difference]" value="<?= $cashbookDifference ?>" style="display:none"></td>
            </tr>
        </tbody>   
        <?php  endif; ?>
    </table>
    <br>
    <div class="row">
        <label class="item_b">Ghi Chú : </label>            
        <textarea style="width: 786px; height: 117px; margin: 0px;" name="CheckAgent[note]" id="GasStoreCard_note"><?= $model->note ?></textarea>            	
    </div>
    <?php $this->widget('UploadMultiFileWidget',
            array(
                'model' => $model,
                'relations' => 'rFile',
                'title_col_file' => "Upload file chứng từ",
                'field_name' => 'file_name')); ?>
    
    <div class="row buttons" style="padding-left: 750px;">
        <?php $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType'=>'submit',
            'label'=>'Lưu kết quả',
            'type'=>'null', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
            'size'=>'small', // null, 'large', 'small' or 'mini'
            //'htmlOptions' => array('style' => 'margin-bottom: 10px; float: right;'),
        )); ?>
    </div>
    <?php $this->endWidget(); ?>
</div>

<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/autoNumeric-master/autoNumeric.js"></script>
<script type="text/javascript">
    $(function(){
        fnInitInputCurrency();
    });
String.prototype.replaceAll = function(search, replacement) {
    var target = this;
    return target.replace(new RegExp(search, 'g'), replacement);
};
$( ".current" ).keyup(function() {
    let parent  = $(this).parent().parent();
    var end     = parseFloat(parent.find('.end').text().replaceAll(/,/,""));
    let result  = end - parseFloat($(this).val());
    if( isNaN(result) ){
        result  = 0;
    }
    parent.find('.difference_txt').text(result.toLocaleString());// số hàng ngàn 1200 => 1,200
    parent.find(' input.difference').val(result);
    
    var input_number  = result;
    var output_number = Math.abs(result);
    var show_input    = true;
    var dropdown      = parent.find('td .dropdown-store-card');
    
    if(result > 0){
        output_number = '';
    } else if (result < 0){
        input_number  = '';
    } else {
        input_number  = '';
        output_number = '';
        show_input    = false;
    }
    
    parent.find('.input_number_txt').text(input_number);
    parent.find('.output_number_txt').text(output_number);
    if(show_input){
        dropdown.show();
        dropdown.removeAttr('disabled');
    } else {
        dropdown.hide();
        dropdown.attr('disabled','disabled');
    }
});
</script>