<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=> GasCheck::getCurl(),
	'method'=>'get',
)); ?>

	<div class="row">
            <?php echo $form->label($model,'agent_id'); ?>
            <?php echo $form->hiddenField($model,'agent_id', array('class'=>'')); ?>
            <?php
                // 1. limit search kh của sale
                $url = Yii::app()->createAbsoluteUrl('admin/ajax/search_by_role', array('role'=> ROLE_AGENT));
                // widget auto complete search user customer and supplier
                $aData = array(
                    'model'=>$model,
                    'field_customer_id'=>'agent_id',
                    'url'=> $url,
                    'name_relation_user'=>'rAgent',
                    'ClassAdd' => 'w-400',
                    'field_autocomplete_name' => 'autocomplete_name',
                    'placeholder'=>'Nhập mã hoặc tên đại lý',
                    'fnSelectCustomerV2' => "fnDeloyBySelect",
                );
                $this->widget('ext.SpaAutocompleteCustomer.SpaAutocompleteCustomer',
                    array('data'=>$aData));
            ?>
        </div>

	

<?php $this->endWidget(); ?>

</div><!-- search-form -->
<script>

    function exeUrl(id){
        $.blockUI({ overlayCSS: { backgroundColor: '<?php echo BLOCK_UI_COLOR;?>' } }); 
        var sParams = 'CheckAgent[agent_id]='+id;
        var requestUri = '<?php echo Yii::app()->createAbsoluteUrl("admin/checkAgent/create");?>?'+sParams;
        window.location= requestUri;
    };
    function fnDeloyBySelect(ui, idField, idFieldCustomer){
         exeUrl(ui.item.id);
    }
</script>