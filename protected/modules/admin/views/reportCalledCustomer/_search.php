<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=> GasCheck::getCurl(),
	'method'=>'get',
)); ?>

        <div class="row">
            <?php  echo $form->labelEx($model,'employee_id', ['label'=>'Nhân viên']); ?>
            <?php  echo $form->dropDownList($model,'employee_id', $model->getArrayEmployee(), ['empty'=>'Select']); ?>
        </div>
    
        <div class="row more_col">
            <div class="col1">
                <?php echo $form->labelEx($model,'date_from'); ?>
                 <?php 
                    $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                        'model'=>$model,        
                        'attribute'=>'date_from',
                        'options'=>array(
                            'showAnim'=>'fold',
                            'dateFormat'=> MyFormat::$dateFormatSearch,
                            'changeMonth' => true,
                            'changeYear' => true,
                            'showOn' => 'button',
                            'buttonImage'=> Yii::app()->theme->baseUrl.'/admin/images/icon_calendar_r.gif',
                            'buttonImageOnly'=> true,                                
                        ),        
                        'htmlOptions'=>array(
                            'class'=>'w-16',
                            'style'=>'height:20px;',
                                'readonly'=>'readonly',
                        ),
                    ));
                    ?>  
                <?php echo $form->error($model,'date_from'); ?>
            </div>
            <div class="col2">
                <?php echo $form->labelEx($model,'date_to'); ?>
                 <?php 
                    $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                        'model'=>$model,        
                        'attribute'=>'date_to',
                        'options'=>array(
                            'showAnim'=>'fold',
                            'dateFormat'=> MyFormat::$dateFormatSearch,
                            'changeMonth' => true,
                            'changeYear' => true,
                            'showOn' => 'button',
                            'buttonImage'=> Yii::app()->theme->baseUrl.'/admin/images/icon_calendar_r.gif',
                            'buttonImageOnly'=> true,                                
                        ),        
                        'htmlOptions'=>array(
                            'class'=>'w-16',
                            'style'=>'height:20px;',
                                'readonly'=>'readonly',
                        ),
                    ));
                    ?>  
                <?php echo $form->error($model,'date_to'); ?>
            </div>
        </div>

        <div class="row">
            <?php echo $form->labelEx($model,'is_buy_complete'); ?>
            <?php echo $form->dropDownList($model,'is_buy_complete', $model->getArrayStatusBuy(),array('empty'=>'Select','class'=>'w-300')); ?>
            <?php echo $form->error($model,'is_buy_complete'); ?>
        </div>
    
	<div class="row buttons" style="padding-left: 159px;">
        <?php $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType'=>'submit',
            'label'=>'Search',
            'type'=>'null', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
            'size'=>'small', // null, 'large', 'small' or 'mini'
            //'htmlOptions' => array('style' => 'margin-bottom: 10px; float: right;'),
        )); ?>	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->