<?php
$cmsFormater = new CmsFormatter();
?>

<h1>Cập nhật thu tiền khách hàng: Mã Thẻ Kho <?php echo $model->store_card_no;?><br> <?php echo $cmsFormater->formatNameCustomerStoreCard($model);?>
    Ngày <?php echo $cmsFormater->formatDate($model->date_delivery);?>
</h1>
<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'gas-storehouse-form',
	'enableAjaxValidation'=>false,
)); ?> 
    
    <div class="row">
        <p class="hight_light item_b">Chỉ cập nhật khi đã thực tế thu tiền khách hàng, khách hàng nợ sẽ không nhập ở đây.</p>
    </div>
    
    <div class="row">
        <?php echo $form->label($model,'receivables_customer', array('label'=>'Phải Thu')); ?>
        <label style="text-align:right;width:135px;"><?php echo ActiveRecord::formatCurrency($model->receivables_customer); ?></label>
    </div>    
    <div class="clr"></div>
    
    <div class="row">
        <?php echo $form->label($model,'receivables_customer', array('label'=>'Đã Thu Đủ')); ?>
        <input class="receivables_full" type="checkbox" style="margin-left: 119px;"> 
        <span class="receivables_customer display_none"><?php echo $model->receivables_customer; ?></span>
    </div>
    
    <div class="clr"></div>
    <div class="row">
        <?php echo Yii::t('translation', $form->labelEx($model,'collection_customer')); ?>
        <div class="fix_number_help">
            <?php echo $form->textField($model,'collection_customer',array('style'=>'width:136px;','maxlength'=>16, 'class'=>'collection_customer number_only number_only_v1')); ?>            
            <div class="help_number"></div>
        </div>
        <?php echo $form->error($model,'collection_customer'); ?>
    </div>
    <div class="clr"></div>

    <div class="row">
        <?php echo Yii::t('translation', $form->labelEx($model,'collection_customer_note')); ?>
        <?php echo $form->textArea($model,'collection_customer_note',array('style'=>'width:350px')); ?>
        <?php echo $form->error($model,'collection_customer_note'); ?>
    </div>
    
    <?php if(!is_null($model->collection_customer_date) && $model->collection_customer_date!='0000-00-00 00:00:00'):?>
    <div class="row">
        <?php echo Yii::t('translation', $form->labelEx($model,'collection_customer_date')); ?>
        <?php echo $cmsFormater->formatDateTime($model->collection_customer_date); ?>
    </div>
    <?php endif;?>
    
    <?php if(!is_null($model->collection_customer_date_update) && $model->collection_customer_date_update!='0000-00-00 00:00:00'):?>
    <div class="row">
        <?php echo Yii::t('translation', $form->labelEx($model,'collection_customer_date_update')); ?>
        <?php echo $cmsFormater->formatDateTime($model->collection_customer_date_update); ?>
    </div>
    <?php endif;?>

    <div class="row buttons" style="padding-left: 141px;">
        <?php $this->widget('bootstrap.widgets.TbButton', array(
        'buttonType'=>'submit',
        'label'=>$model->isNewRecord ? Yii::t('translation', 'Create') : Yii::t('translation', 'Save'),
        'type'=>'null', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
        'size'=>'small', // null, 'large', 'small' or 'mini'
        //'htmlOptions' => array('style' => 'margin-bottom: 10px; float: right;'),
    )); ?>	
        <input class='cancel_iframe' type='button' value='Cancel'>
    </div>

<?php $this->endWidget(); ?>

</div><!-- form -->
<script>
    $(function(){
        $('#GasStoreCard_collection_customer').trigger('change');
        $('.receivables_full').click(function(){
            if($(this).is(':checked')){
                $('.collection_customer').val($('.receivables_customer').text());
            }else{
                $('.collection_customer').val('');
            }
            $('#GasStoreCard_collection_customer').trigger('change');
        });
    });
</script>