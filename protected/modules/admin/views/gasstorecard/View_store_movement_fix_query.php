<div class="container" id="printElement">
<?php if(isset($data['aMaterials'])): ?>
    <table cellpadding="0" cellspacing="0">
        <tr>
            <td>
                <div class="logo">
                    <img src="<?php echo Yii::app()->theme->baseUrl; ?>/images/logo80x80.png">
                </div>            
                <p style="margin: 5px 0 20px 0;">CÔNG TY TNHH HƯỚNG MINH</p>
            </td>
            <td valign="top">
                <h2 style="margin: 35px 0 0">Báo Cáo Nhập Xuất Tồn</h2>
            </td>
        </tr>
        <tr>
            <td class="agent_info"><b>Tên đại Lý</b>: <?php echo $model->agent?$model->agent->first_name:''?></td>
            <td style="text-align: right;"><b>Từ Ngày</b>: <?php echo $model->date_from." <b>Đến Ngày</b>: $model->date_to";?></td>
        </tr>

    </table>
    <table cellpadding="0" cellspacing="0" class="tb hm_table">
        <thead>
            <tr>
                <th>Vật Tư Nhóm Cha</th>
                <th>Tồn đầu</th>
                <?php foreach(CmsFormatter::$STORE_CARD_TYPE as $id_type=>$name):?>
                    <th><?php echo $name;?></th>
                <?php endforeach;?>
                <!-- <th>Xuất bán</th>-->
                <th>Tồn Cuối</th>
            </tr>
        </thead>
        <tbody>
                <?php
                    $aMaterials = $data['aMaterials'];
                    $sum12OpeningStock = 0;
                    $sum12Import = 0;
                    $sum12Export = 0;
                    $sum12Balance = 0;
                    $sum12Show = true;
                    
                    $OPENING_BALANCE_YEAR_BEFORE = $data['OPENING_BALANCE_YEAR_BEFORE'];
                    $OPENING_IMPORT = isset($data['OPENING_IMPORT']) ? $data['OPENING_IMPORT'] : [];
                    $OPENING_EXPORT = isset($data['OPENING_EXPORT']) ? $data['OPENING_EXPORT'] : [];
                    $IMPORT = isset($data['IMPORT']) ? $data['IMPORT'] : [];
                    $EXPORT = isset($data['EXPORT']) ? $data['EXPORT'] : [];
                ?>
            
                <?php foreach($aMaterials as $key=>$obj): ?>
                <?php 
                    $objParent = $obj['parent_obj'];
                    $objSubModel = $obj['sub_arr_model']; // is array $key=>$mMaterial sub
                    $parentBegin = 0;
                    $parentImport = 0;
                    $parentExport = 0;
                    $parentEnd = 0;
                ?>
            
                <?php if($obj['parent_obj']->materials_type_id==MATERIAL_TYPE_BINH_12 && $sum12Show): $sum12Show=false;?>
                <tr class=" hight_light">
                    <td class="item_b">Tổng Gas 12</td>
                    <td class="item_c item_b sum12OpeningStock"></td>
                    <td class="item_c item_b sum12Import"></td>
                    <td class="item_c item_b sum12Export"></td>
                    <td class="item_c item_b sum12Balance"></td>
                </tr>
                <?php endif;?>
                
                <tr>
                    <td class="item_b"><?php echo $obj['parent_obj']->name;?></td>
                    <td class="item_c item_b parentBegin<?php echo $obj['parent_obj']->id;?>"></td>
                    <td class="item_c item_b parentImport<?php echo $obj['parent_obj']->id;?>"></td>
                    <td class="item_c item_b parentExport<?php echo $obj['parent_obj']->id;?>"></td>
                    <td class="item_c item_b parentEnd<?php echo $obj['parent_obj']->id;?>"></td>
                </tr>
                
                    <?php foreach($objSubModel as $key=>$mMaterial):?>
                        <?php 
                            // tồn đầu
                            $OpeningYear = isset($OPENING_BALANCE_YEAR_BEFORE[$mMaterial->id])?$OPENING_BALANCE_YEAR_BEFORE[$mMaterial->id]:0;
                            $OpeningYearImport = isset($OPENING_IMPORT[$mMaterial->id])?$OPENING_IMPORT[$mMaterial->id]:0;
                            $OpeningYearExport = isset($OPENING_EXPORT[$mMaterial->id])?$OPENING_EXPORT[$mMaterial->id]:0;
                            $OpeningStock = $OpeningYear+$OpeningYearImport-$OpeningYearExport;
                            // Nhập xuất kho
                            $totalImport = isset($IMPORT[$mMaterial->id])?$IMPORT[$mMaterial->id]:0;
                            $totalExport = isset($EXPORT[$mMaterial->id])?$EXPORT[$mMaterial->id]:0;
                            if($OpeningStock==0 && $totalImport==0 && $totalExport==0)
                                continue;
                            // Tồn cuối $remain
                            $remain = $OpeningStock+$totalImport-$totalExport;
                            
                            $parentBegin += $OpeningStock;
                            $parentImport += $totalImport;
                            $parentExport += $totalExport;
                            $parentEnd += $remain;

                            if($obj['parent_obj']->materials_type_id==MATERIAL_TYPE_BINH_12){
                                $sum12OpeningStock += $OpeningStock;
                                $sum12Import += $totalImport;
                                $sum12Export += $totalExport;
                                $sum12Balance += $remain;
                            }
                        ?>
                        <tr>
                            <td class="item_sub_20"><?php echo $mMaterial->materials_no.' - '.$mMaterial->name;?></td>
                            <td class="item_c"><?php echo ActiveRecord::formatCurrency($OpeningStock);?></td>
                            <td class="item_c"><?php echo ActiveRecord::formatCurrency($totalImport);?></td>
                            <td class="item_c"><?php echo ActiveRecord::formatCurrency($totalExport);?></td>
                            <td class="item_c"><?php echo ActiveRecord::formatCurrency($remain);?></td>
                            <!--<td></td>-->
                        </tr>
                    <?php endforeach; // end foreach($objSubModel as $key=>$mMateri ?>
                        
                    <tr class="display_none">
                        <?php 
                            $parentRemove = '';
                            if($parentBegin==0&&$parentImport==0&&$parentExport==0){
                                $parentRemove = 'class_remove_row';
                            }
                        ?>
                        <td colspan="5" class="<?php echo $parentRemove;?>" link="parentBegin<?php echo $obj['parent_obj']->id;?>">
                            <span class="parentBegin" link="parentBegin<?php echo $obj['parent_obj']->id;?>">
                                <?php echo ActiveRecord::formatCurrency($parentBegin);?>
                            </span>
                            <span class="parentImport" link="parentImport<?php echo $obj['parent_obj']->id;?>">
                                <?php echo ActiveRecord::formatCurrency($parentImport);?></span>
                            <span class="parentExport" link="parentExport<?php echo $obj['parent_obj']->id;?>">
                                <?php echo ActiveRecord::formatCurrency($parentExport);?></span>
                            <span class="parentEnd" link="parentEnd<?php echo $obj['parent_obj']->id;?>">
                                <?php echo ActiveRecord::formatCurrency($parentEnd);?></span>
                        </td>
                    </tr>                        
                                
                <?php endforeach; // end parent foreach($aMaterials as $key=>$obj)?>
                <tr class="display_none">
                    <td class="item_sub_20">Tổng Gas 12</td>
                    <td class="item_c sum12OpeningStockHide"><?php echo ActiveRecord::formatCurrency($sum12OpeningStock);?></td>
                    <td class="item_c sum12ImportHide"><?php echo ActiveRecord::formatCurrency($sum12Import);?></td>
                    <td class="item_c sum12ExportHide"><?php echo ActiveRecord::formatCurrency($sum12Export);?></td>
                    <td class="item_c sum12BalanceHide"><?php echo ActiveRecord::formatCurrency($sum12Balance);?></td>
                </tr>                       
        </tbody>        
    </table>
</div>

<?php else: ?>
    <h2>Không có dữ liệu</h2>
<?php endif; // end  if(isset($data['aSumEachType'] ?>
</div>

<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/jquery.floatThead.min.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/underscore-min.js"></script>    
<script>
    $(function(){
        $('.sum12OpeningStock').text($('.sum12OpeningStockHide').text());
        $('.sum12Import').text($('.sum12ImportHide').text());
        $('.sum12Export').text($('.sum12ExportHide').text());
        $('.sum12Balance').text($('.sum12BalanceHide').text());
        
        fnUpdateParent('parentBegin');
        fnUpdateParent('parentImport');
        fnUpdateParent('parentExport');
        fnUpdateParent('parentEnd');
        $('.hm_table').floatThead();
        
        $('.class_remove_row').each(function(){
            var RefClass = $(this).attr('link');
            $('.'+RefClass).closest('tr').remove();
        });        
        
    });
    
    function fnUpdateParent(parentClass){
        $('.'+parentClass).each(function(){
            var RefClass = $(this).attr('link');
            $('.'+RefClass).text($(this).text());
        });
    }
</script>