<?php
$this->breadcrumbs=array(
	'Thẻ Kho'=>array('index'),
	'Tạo Mới',
);

$menus = array(		
        array('label'=>'Thẻ Kho', 'url'=>array('index')),
);
$this->menu= ControllerActionsName::createMenusRoles($menus, $actions);
$msg = 'Tạo mới xuất kho';
$color = '';
if($model->type_store_card == TYPE_STORE_CARD_IMPORT){
    $msg = 'Tạo mới nhập kho';
    $color = 'color: blue;';
}


?>

<h1 style="<?php echo $color;?>"><?php echo $msg;?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>