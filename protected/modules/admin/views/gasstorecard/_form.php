<div class="form">
<?php 
    $cRole = Yii::app()->user->role_id;
    $cUid = MyFormat::getCurrentUid();
    $DieuPhoiCreate = false;
    $DieuPhoiHide   = "";
    // kiểm tra ẩn 1 số thông tin mà kho không cần thiết phải xem
    $display_none_warehouse_new='';
    $l_padding_50='l_padding_50';
    if(MyFormat::getAgentGender()==Users::IS_WAREHOUSE_IN_OUT):
        $l_padding_50 = '';
        $display_none_warehouse_new='display_none';
        $model->type_in_out = STORE_CARD_TYPE_4; // 'Xuất Nội Bộ',
        if($model->type_store_card==TYPE_STORE_CARD_IMPORT): 
            $model->type_in_out = STORE_CARD_TYPE_1; // 'Nhập Nội Bộ',
        endif;
    endif;
    
    if($model->canRemoveChangeCustomer()):
        $DieuPhoiCreate = true;
        $DieuPhoiHide   = "display_none";
    endif;
?>
<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'gas-store-card-form',
	'enableAjaxValidation'=>false,
)); ?>
    <div class="row buttons" style="padding-left: 141px;">
            <?php $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType'=>'submit',
            'label'=>$model->isNewRecord ? 'Create' :'Save',
            'type'=>'null', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
            'size'=>'small', // null, 'large', 'small' or 'mini'
            //'htmlOptions' => array('style' => 'margin-bottom: 10px; float: right;'),
        )); ?>	</div>

	<p class="note">Dòng có dấu <span class="required">*</span> là bắt buộc.</p>
        <em class="hight_light item_b display_none <?php echo $display_none_warehouse_new;?>">Chú Ý: Với khách hàng bình bò phải tạo phiếu cân gas dư ( nếu có ) cho khách hàng trước khi lập phiếu xuất kho</em>
        <?php if(Yii::app()->user->hasFlash('successUpdate')): ?>
            <div class="success_div"><?php echo Yii::app()->user->getFlash('successUpdate');?>: 
                <?php if(isset($_GET['view_id'])):?>
                    <?php $mStoreNew = GasStoreCard::model()->findByPk($_GET['view_id']); ?>
                    &nbsp;&nbsp;&nbsp;<a href="<?php echo Yii::app()->createAbsoluteUrl('admin/gasstorecard/view',array('id'=>$_GET['view_id'])); ?>" target="_blank" title="Xem lại thẻ kho"><?php echo $mStoreNew->store_card_no;?></a>
                <?php endif;?>
            </div>         
        <?php endif; ?>
        
        <?php include 'view_model_err.php';?>
        
        <?php include '_form_customer.php';?>
        
        <h3 class='title-info'>Thông Tin Thẻ Kho</h3>

	<?php if(!$model->isNewRecord ): ?>
            <?php if(!empty($model->store_card_no) ): ?>
            <div class="row">
                <?php echo $form->labelEx($model,'store_card_no'); ?>
                <?php // echo $form->textField($model,'store_card_no',array('size'=>20,'readonly'=>1)); ?>
                <?php echo $model->store_card_no; ?>
                <?php echo $form->error($model,'store_card_no'); ?>
                <div class="clr"></div>
            </div>
            <?php endif;?>
        <div class="row">
            <?php echo $form->labelEx($model,'reason_false'); ?>
            <?php echo $form->dropDownList($model,'reason_false', $model->getStatusFalse(),array('class' =>"w-200 NotMakeOrder", 'empty'=>'Select')); ?>
            <em class="hight_light item_b">( Chú Ý: Những đơn hàng không giao gas hoặc thu vỏ thì đại lý sẽ chọn ở đây và nhập vào ô [Ghi Chú] lý do KH không lấy gas)</em>
            <div class="clr"></div>
	</div>
        <?php endif;?>

	<div class="row more_col NotMakeOrderHide">
            <div class="col1 <?php echo $display_none_warehouse_new;?>">
		<?php echo $form->labelEx($model,'type_in_out'); ?>
                <?php
                    $aTypeImportExport = CmsFormatter::$STORE_CARD_TYPE_IMPORT;
                    if($model->type_store_card==TYPE_STORE_CARD_EXPORT): 
                        $aTypeImportExport = CmsFormatter::$STORE_CARD_TYPE_EXPORT;
                    endif;
                ?>
		<?php echo $form->dropDownList($model,'type_in_out', $aTypeImportExport,array('class' =>"w-200 float_l $DieuPhoiHide", 'empty'=>'Select')); ?>
                <?php if(!empty($DieuPhoiHide)): ?>
                    <label class="w-200"><?php echo $model->getTypeInOutText();?></label>
                <?php endif; ?>
		<?php echo $form->error($model,'type_in_out'); ?>
                
            </div>
            <div class="col2 <?php echo $l_padding_50;?>">
		<?php echo $form->labelEx($model,'store_card_real'); ?>
		<?php echo $form->textField($model,'store_card_real',array('size'=>30,'maxlength'=>20)); ?>
		<?php echo $form->error($model,'store_card_real'); ?>
            </div>
	</div>

	
        <div class="row more_col NotMakeOrderHide">
            <div class="col1">
		<?php echo Yii::t('translation', $form->labelEx($model,'date_delivery')); ?>
                <?php 
                    $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                        'model'=>$model,        
                        'attribute'=>'date_delivery',
                        'options'=>array(
                            'showAnim'=>'fold',
                            'dateFormat'=> ActiveRecord::getDateFormatJquery(),
                            'minDate'=> '-'.GasCashBook::getAgentDaysAllowUpdate(),
                            'maxDate'=> '0',
                            'yearRange'=> (date('Y')-1).":".date('Y'),
                            'changeMonth' => true,
                            'changeYear' => true,
                            'showOn' => 'button',
                            'buttonImage'=> Yii::app()->theme->baseUrl.'/admin/images/icon_calendar_r.gif',
                            'buttonImageOnly'=> true,                                
                        ),        
                        'htmlOptions'=>array(
                            'class'=>'w-16',
                            'style'=>'height:20px;width:166px;',
                                'readonly'=>'readonly',
                        ),
                    ));
                ?>     		
		<?php echo $form->error($model,'date_delivery'); ?>
            </div>
            <div class="col2 l_padding_50">
		<?php echo $form->labelEx($model,'delivery_person'); ?>
		<?php echo $form->textField($model,'delivery_person',array('size'=>30,'maxlength'=>80)); ?>
		<?php echo $form->error($model,'delivery_person'); ?>
            </div>
	</div>
        <?php include "_form_car.php"; ?>
        
        <?php if(!$model->isNewRecord): ?>
	<div class=" display_none row <?php echo $display_none_warehouse_new;?> NotMakeOrderHide">
            <?php echo $form->labelEx($model,'receivables_customer'); ?>
            <span class="item_b float_l"><?php echo ActiveRecord::formatCurrency($model->receivables_customer); ?></span>
	</div>
        <?php endif;?>
        
        <div class="clr"></div>
	<div class=" display_none row <?php echo $display_none_warehouse_new;?>">
            <?php // echo $form->labelEx($model,'pay_now');// Oct biến này sử dụng cho đơn hàng điều phối tạo từ window nên sẽ không sử dụng dưới view bừa bãi thế này được ?>
            <?php // echo $form->checkBox($model,'pay_now',array('class'=>'')); ?><em>( Chỉ check vào khi KH đã thanh toán đủ tiền cho đơn hàng rồi.)</em>
	</div>
        
        <div class="clr"></div>    		
	<div class="row NotMakeOrderHide">
            <?php echo $form->labelEx($model,'materials_name'); ?>
            <?php echo $form->textField($model,'materials_name',array('size'=>32,'placeholder'=>'Nhập mã vật tư hoặc tên vật tư')); ?>
            <em class="hight_light item_b">Chú Ý: Có thể chọn nhiều vật tư trong cùng 1 lần tạo (<a class="hight_light" href="<?php echo Yii::app()->createAbsoluteUrl('admin/site/news',array('id'=>12));?>" target="_blank"> Xem hướng dẫn </a>)</em>
            <?php echo $form->error($model,'materials_name'); ?>
            <div class="clr"></div>    		
	</div>
        
        <?php if( $model->scenario == 'create_customer_pay_now' ): ?>
        <div class="row buttons" style="padding-left: 141px;">
            <?php $this->widget('bootstrap.widgets.TbButton', array(
                'buttonType'=>'submit',
                'label'=>'Load template',
                'htmlOptions' => array(
                    'name' => 'loadTemplate',
                    'value' => 1
                ),
            )); ?>	
        </div>
        <?php endif; ?>
        
        <div class="row NotMakeOrderHide">
            <label>&nbsp</label>
            <table class="materials_table hm_table sortable">
                <thead>
                    <tr>
                        <th class="item_c">#</th>
                        <th class="item_code item_c">Mã</th>
                        <th class="item_name item_c">Tên</th>
                        <th class="item_unit item_c">ĐVT</th>
                        <th class="w-50 item_c">SL</th>
                        <th class=" item_c display_none">Đơn Giá</th>
                        <th class=" item_c w-100 display_none">Thành Tiền</th>
                        <th class="item_unit last item_c">Xóa</th>
                    </tr>
                </thead>
                <tbody>
                    <?php if(count($model->aModelStoreCardDetail)): ?>
                        <?php foreach($model->aModelStoreCardDetail as $key=>$item):?>
                        <?php $mMaterial = $item->materials; ?>
                        <?php if($mMaterial): ?>
                        <?php $unit_use = 1;
                            if(array_key_exists($mMaterial->materials_type_id, CmsFormatter::$MATERIAL_TYPE_BINH_BO_VALUE_KG))
                                $unit_use = CmsFormatter::$MATERIAL_TYPE_BINH_BO_VALUE_KG[$mMaterial->materials_type_id];
                        ?>
                    
                        <tr class="materials_row">
                            <td class="item_c order_no"><?php echo ($key+1);?></td>
                            <td><?php echo $mMaterial->materials_no;?></td>
                            <td class="item_l"><?php echo $mMaterial->name;?></td>
                            <td class="item_c"><?php echo $mMaterial->unit;?></td>
                            <td class="item_c ">
                                <input name="materials_qty[]" value="<?php echo ActiveRecord::formatNumberInput($item->qty);?>" class="item_qty materials_qty number_only_v1  w-50 " type="text" size="6" maxlength="9">
                                <input name="materials_id[]" value="<?php echo $mMaterial->id;?>" class="materials_id" type="hidden">
                                <input name="unit_use[]" value="<?php echo $unit_use;?>" class="unit_use" type="hidden">
                            </td>
                            <td class="item_c display_none">
                                <input name="price[]" value="<?php echo ActiveRecord::formatNumberInput($item->price);?>" class="item_price number_only number_only_v1 w-80" type="text" maxlength="9">
                                <div class="help_number"></div>
                            </td>
                            <td class="item_r item_amount display_none">
                                <?php echo ActiveRecord::formatCurrency($item->amount);?>
                            </td>
                            <td class="item_c last"><span class="remove_icon_only"></span></td>
                        </tr>
                        <?php endif;?>
                        <?php endforeach;?>
                    <?php endif;?>
                    
                </tbody>
                
                <tfoot>
                    <tr class="materials_row">
                        <td class="item_b item_r f_size_16" colspan="4">Tổng Cộng</td>
                        <td class="item_b item_r total_qty f_size_16"></td>
                        <td class=" display_none"></td>
                        <td class="item_b item_r total_amount f_size_16 display_none"><?php echo ActiveRecord::formatCurrency($model->receivables_customer);?></td>
                        <td class="last"></td>
                    </tr>
                </tfoot>
            </table>
        </div>
        
	<div class="row">
            <?php echo $form->labelEx($model,'note'); ?>
            <?php echo $form->textArea($model,'note',array('style'=>'width:680px;height:40px;')); ?>
            <?php echo $form->error($model,'note'); ?>
	</div>
        
        <?php if(!$model->isNewRecord && !empty($model->update_note)): ?>
	<div class="row ">
            <?php echo $form->labelEx($model,'update_note', array('label'=>'Ghi chú của quản lý kho')); ?>
            <span><?php  echo $model->update_note; ?></span>
	</div>
        <?php endif;?>
        
	<div class="row buttons" style="padding-left: 141px;">
            <?php $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType'=>'submit',
            'label'=>$model->isNewRecord ? 'Create' :'Save',
            'type'=>'null', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
            'size'=>'small', // null, 'large', 'small' or 'mini'
            //'htmlOptions' => array('style' => 'margin-bottom: 10px; float: right;'),
        )); ?>	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->
<input type="text" value="1" class="PriceBinhBoByMonth display_none">
<div class="display_none">
    <div id="alert_input_gas_remain" class="">
        <h1>Chú Ý</h1>
        <p class="hight_light item_b f_size_18">Đây là khách hàng bình bò, bạn cần phải nhập gas dư trước khi tạo thẻ kho cho khách hàng này. 
            Nếu khách hàng không cân thì bỏ qua nhập gas dư.</p>
        <a style="font-size:15px;" class="gas_link" target="_blank" href="<?php echo Yii::app()->createAbsoluteUrl('admin/gasremain/create');?>">
            Nhập Gas Dư
        </a>
    </div>
</div>


<script>
$(document).ready(function(){
    
    $('.form').find('button:submit').click(function(){        
        var msg = isValidSubmit();
        if( msg!='' )
        {
            showMsgWarning(msg); return false;
        }
        $.blockUI({ overlayCSS: { backgroundColor: '#fff' } }); 
    });
        
//    var availableMaterials = <?php // echo MyFunctionCustom::getMaterialsJson(array('GetAll'=>1)); ?>;
    var availableMaterials = <?php echo CacheSession::getListMaterialJson(); ?>;

    $( "#GasStoreCard_materials_name" ).autocomplete({
        source: availableMaterials,
        close: function( event, ui ) { $( "#GasStoreCard_materials_name" ).val(''); },
        select: function( event, ui ) {
            var class_item = 'materials_row_'+ui.item.id;
            if($('.'+class_item).size()<1)
                fnBuildRowMaterial(event, ui); 
        }
    });
    
    fnRefreshOrderNumber();    
    fnBindRemoveIcon();
    fnCalcTotalPay();
    fnBindSomeLiveChange();
    notMakeOrder();
    <?php if(!empty($model->reason_false)): ?>
        $('.NotMakeOrder').trigger('change');
    <?php endif; ?>

});

    function notMakeOrder(){
        $('.NotMakeOrder').change(function(){
            if($(this).val() != ''){
                $('.NotMakeOrderHide').hide();
            }else{
                $('.NotMakeOrderHide').show();
            }
        });
    }

    function fnShowAlertGasRemain(){
        return;
        $.colorbox({inline:true, href:"#alert_input_gas_remain",
            closeButton:false,
            overlayClose :false, escKey:false,
            innerHeight:'200', 
            innerWidth:'500',                                 
         });        
    }

    function fnBindSomeLiveChange(){
        $('.item_qty, .item_price').live('change', function(){
            fnCalcTotalPay();
        });
    }
    
    function isValidSubmit(){
        var msg = '';
        if($('.materials_row').size()<1){            
            msg = 'Chưa chọn vật tư';
            return msg;
        }
        $('.materials_qty').each(function(){
            if( parseFloat($(this).val())*1==0){
                msg = 'Số lượng vật tư phải lớn hơn 0';
                $(this).addClass('error');
                return false;
            }else{
                $(this).removeClass('error');
            }
        });
        return msg;
    }

    
    function fnBuildRowMaterial(event, ui){
        var PriceBinhBoByMonth = "";
        if($('.PriceBinhBoByMonth').val()>1 && ( ui.item.materials_type_id==<?php echo MATERIAL_TYPE_BINHBO_50;?> || ui.item.materials_type_id==<?php echo MATERIAL_TYPE_BINHBO_45;?> )) {
            PriceBinhBoByMonth = $('.PriceBinhBoByMonth').val();
        }
        var class_item = 'materials_row_'+ui.item.id;
        var _tr = '';
            _tr += '<tr class="materials_row">';
                _tr += '<td class="item_c order_no"></td>';
                _tr += '<td>'+ui.item.materials_no+'</td>';
                _tr += '<td class="item_l">'+ui.item.name+'</td>';
                _tr += '<td class="item_c">'+ui.item.unit+'</td>';
                _tr += '<td class="item_c">';
                    _tr += '<input name="materials_qty[]" value="1" class="item_qty w-50 materials_qty number_only_v1 '+class_item+'" type="text" size="6" maxlength="9">';
                    _tr += '<input name="materials_id[]" value="'+ui.item.id+'" class="materials_id" type="hidden">';
                    _tr += '<input name="unit_use[]" value="'+ui.item.unit_use+'" class="unit_use" type="hidden">';
                _tr += '</td>';
                _tr += '<td class="item_c display_none">';
                    _tr += '<input name="price[]" value="'+PriceBinhBoByMonth+'" class="item_price number_only number_only_v1 w-80" type="text" maxlength="9">';
                    _tr += '<div class="help_number"></div>';
                _tr += '</td>';
                _tr += '<td class="item_r item_amount display_none">';
                _tr += '</td>';
                _tr += '<td class="item_c last"><span class="remove_icon_only"></span></td>';
            _tr += '</tr>';
        $('.materials_table tbody').append(_tr);
        fnRefreshOrderNumber();
        bindEventForHelpNumber();
        fnCalcTotalPay();
    }
    
    function fnCalcTotalPay(){
        var total_amount = 0;
        var total_qty = 0;
        $('.materials_table tbody .materials_row').each(function(){
           var item_qty = $(this).find('.item_qty').val()*1; 
           var unit_use = $(this).find('.unit_use').val()*1; 
           total_qty+=item_qty;
           var item_price = $(this).find('.item_price').val()*1; 
           var  amount = parseFloat(item_qty*item_price*unit_use);
           amount = Math.round(amount * 100) / 100;
           total_amount += amount;
           $(this).find('.item_amount').text(commaSeparateNumber(amount)); 
        });
        
        total_amount = parseFloat(total_amount);
        total_amount = Math.round(total_amount * 100) / 100;
        $('.receivables_customer').val(total_amount);
        $('.total_amount').text(commaSeparateNumber(total_amount));
        
        total_qty = parseFloat(total_qty);
        total_qty = Math.round(total_qty * 100) / 100;
        $('.total_qty').text(commaSeparateNumber(total_qty));
    }
    
    function fnAfterRemoveIcon(){
        fnCalcTotalPay();
    }
    
</script>


<script>
    $(document).ready(function(){
        
        <?php if($model->modelUser->phone==NO_PHONE_NUMBER):?>
            $('.no_phone_number').trigger('click');
            if($('.no_phone_number').is(':checked'))
                $('.no_phone_number').closest('div.row').find('.no_phone_number_text').attr('readonly', true);
        <?php endif;?>
        
        $('#Users_province_id').live('change',function(){
            var province_id = $(this).val();        
            var url_ = "<?php echo Yii::app()->createAbsoluteUrl('admin/ajax/get_slt_district');?>";
            $.blockUI({ overlayCSS: { backgroundColor: '<?php echo BLOCK_UI_COLOR;?>' } }); 
            $.ajax({
                url: url_,
                data: {ajax:1,province_id:province_id},
                type: "get",
                dataType:'json',
                success: function(data){
                    $('#Users_district_id').html(data['html_district']);                
                    $.unblockUI();
                }
            });
        });   		

        $('#Users_district_id').live('change',function(){
                var province_id = $('#Users_province_id').val();   
                var district_id = $(this).val();        
                var url_ = "<?php echo Yii::app()->createAbsoluteUrl('admin/ajax/get_slt_ward');?>";
                $.blockUI({ overlayCSS: { backgroundColor: '<?php echo BLOCK_UI_COLOR;?>' } }); 
                $.ajax({
                        url: url_,
                        data: {ajax:1,district_id:district_id,province_id:province_id},
                        type: "get",
                        dataType:'json',
                        success: function(data){
                                $('#Users_ward_id').html(data['html_district']);                
                                $.unblockUI();
                        }
                });

        });   		
		
            fnUpdateColorbox();
            fnAddReadonlyStreet();
            fnSelectCustomer();
            
            <?php // if(!in_array(Yii::app()->user->province_id, CmsFormatter::$PROVINCE_ALLOW)): ?>
                fnDisableAddNewCustomer();
            <?php // endif;?>
                fnShowHideCustomer();
                
        $( ".sortable tbody" ).sortable({
            refreshPositions: true, 
            tolerance: 'pointer',
            cursor: 'pointer',
            stop: function( event, ui ) {
                fnRefreshOrderNumber();
            }
        });
    });
    
    function fnUpdateColorbox(){  
        $(".iframe_create_district").colorbox({iframe:true,innerHeight:'400', innerWidth: '700',close: "<span title='close'>close</span>"});
        $(".iframe_create_ward").colorbox({iframe:true,innerHeight:'400', innerWidth: '700',close: "<span title='close'>close</span>"});
        $(".iframe_create_street").colorbox({iframe:true,innerHeight:'400', innerWidth: '700',close: "<span title='close'>close</span>"});
        
    }
	
    function fnSelectCustomer(){
        $('.select_customer').click(function(){
            var type = $(this).val();
            $('.customer').hide();
            $('.customer_'+type).show();
        });
    }
	
    function fnShowHideCustomer(){
        <?php if($model->type_store_card == TYPE_STORE_CARD_IMPORT):?>			
            $('#GasStoreCard_ext_is_new_customer_1').next('label').text('NCC Mới');
            $('#GasStoreCard_ext_is_new_customer_2').next('label').text('NCC Cũ');
        <?php endif;?>	
        <?php if($model->isNewRecord):?>			
                $('.customer_<?php echo $model->ext_is_new_customer;?>').show();
        <?php else:?>	
//                $('.customer_2').show();
//                $('.radio-list-2').remove();
//                $('#GasStoreCard_autocomplete_name').remove();
        
        <?php endif;?>	
            
        $('.select_customer').each(function(){
            if($(this).is(':checked')){
                $(this).trigger('click');
                return false;
            }
        });
    }
    
    function fnDisableAddNewCustomer(){
        $('#GasStoreCard_ext_is_new_customer_1').hide();
        $('#GasStoreCard_ext_is_new_customer_1').next('label').hide();
        <?php if($model->isNewRecord):?>
            $('.customer').hide();
//            $('#GasStoreCard_ext_is_new_customer_2').trigger('click');                    
        <?php endif;?>	
    }
    
    function fnGetPriceBinhBoByMonth(customer_id){
            var url_ = "<?php echo Yii::app()->createAbsoluteUrl('admin/ajax/getPriceBinhBoByMonth');?>";
            $.ajax({
                url: url_,
                data: {ajax:1,customer_id:customer_id},
                type: "get",
                dataType:'json',
                success: function(data){
                    if(data['PriceBinhBoByMonth']>1)
                        $('.PriceBinhBoByMonth').val(data['PriceBinhBoByMonth']);
                }
            });
    }
    
    
</script>

