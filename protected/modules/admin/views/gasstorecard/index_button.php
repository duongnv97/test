<div class="form">
    <?php
        $LinkNormal     = Yii::app()->createAbsoluteUrl('admin/gasstorecard/index');
        $LinkDieuPhoi   = Yii::app()->createAbsoluteUrl('admin/gasstorecard/index', array('call_center'=> 1));
        $LinkXeTai      = Yii::app()->createAbsoluteUrl('admin/gasstorecard/index', array('call_center'=> 2));
    ?>
    <h1><?php echo $this->adminPageTitle;?>
        <a class='btn_cancel f_size_14 <?php echo (!isset($_GET['call_center'])) ? "active":"";?>' href="<?php echo $LinkNormal;?>">Normal</a>
        <a class='btn_cancel f_size_14 <?php echo isset($_GET['call_center']) && $_GET['call_center']==1 ? "active":"";?>' href="<?php echo $LinkDieuPhoi;?>">Điều Phối Tạo</a>
        <a class='btn_cancel f_size_14 <?php echo isset($_GET['call_center']) && $_GET['call_center']==2 ? "active":"";?>' href="<?php echo $LinkXeTai;?>">Xe Tải Giao</a>
    </h1> 
</div>