<div class="container" id="printElement">
<?php if(isset($data['aMaterials'])): ?>
    <table cellpadding="0" cellspacing="0">
        <tr>
            <td>
                <div class="logo">
                    <img src="<?php echo Yii::app()->theme->baseUrl; ?>/images/logo80x80.png">
                </div>            
                <p style="margin: 5px 0 20px 0;">CÔNG TY TNHH HƯỚNG MINH</p>
            </td>
            <td valign="top">
                <h2 style="margin: 35px 0 0">Báo Cáo Nhập Xuất Tồn</h2>
            </td>
        </tr>
        <tr>
            <td class="agent_info"><b>Tên đại Lý</b>: <?php echo $model->agent?$model->agent->first_name:''?></td>
            <td style="text-align: right;"><b>Từ Ngày</b>: <?php echo $model->date_from." <b>Đến Ngày</b>: $model->date_to";?></td>
        </tr>

    </table>
    <table cellpadding="0" cellspacing="0" class="tb hm_table">
        <thead>
<!--            <tr>
                <th colspan="5">Thẻ Kho Tổng Hợp</th>
            </tr>-->
            <tr>
                <th rowspan="2">Vật Tư Nhóm Cha</th>
                <th colspan="4">Số lượng</th>
                <!--<th rowspan="2">Người kiểm (ký-tên)</th>-->
            </tr>
            <tr>
                <th>Tồn Đầu</th>
                <?php foreach(CmsFormatter::$STORE_CARD_TYPE as $id_type=>$name):?>
                    <th><?php echo $name;?></th>
                <?php endforeach;?>
                <!-- <th>Xuất bán</th>-->
                <th>Tồn Cuối</th>
            </tr>
            
        </thead>
        <tbody>
                <?php
                    $aMaterials = $data['aMaterials'];
                    $sum12OpeningStock = 0;
                    $sum12Import = 0;
                    $sum12Export = 0;
                    $sum12Balance = 0;
                    $sum12Show = true;
                ?>
            
                <?php foreach($aMaterials as $key=>$obj): ?>
                <?php 
                    $objParent = $obj['parent_obj'];
                    $objSubModel = $obj['sub_arr_model']; // is array $key=>$mMaterial sub                    
                    $detailSubMaterial = $data[$objParent->id]['detailSubMaterial'];
                    $parentOpeningBalance = $data[$objParent->id]['parentOpeningBalance'];
                    $parentTotalImport = $data[$objParent->id]['parentTotalImport'];
                    $parentTotalExport = $data[$objParent->id]['parentTotalExport'];
                    if($parentOpeningBalance==0 && $parentTotalImport==0 && $parentTotalExport==0)
                        continue;
                    $parentRemain = $parentOpeningBalance+$parentTotalImport-$parentTotalExport;
                    
                    
                ?>
            
                <?php if($obj['parent_obj']->materials_type_id==MATERIAL_TYPE_BINH_12 && $sum12Show): $sum12Show=false;?>
                <tr class=" hight_light">
                    <td class="item_b">Tổng Gas 12</td>
                    <td class="item_c item_b sum12OpeningStock"></td>
                    <td class="item_c item_b sum12Import"></td>
                    <td class="item_c item_b sum12Export"></td>
                    <td class="item_c item_b sum12Balance"></td>
                </tr>
                <?php endif;?>
                
                <tr>
                    <td class="item_b"><?php echo $obj['parent_obj']->name;?></td>
                    <td class="item_c item_b"><?php echo ActiveRecord::formatCurrency($parentOpeningBalance);?></td>
                    <td class="item_c item_b"><?php echo ActiveRecord::formatCurrency($parentTotalImport);?></td>
                    <td class="item_c item_b"><?php echo ActiveRecord::formatCurrency($parentTotalExport);?></td>
                    <td class="item_c item_b"><?php echo ActiveRecord::formatCurrency($parentRemain);?></td>
                </tr>
                
                    <?php foreach($objSubModel as $key=>$mMaterial):?>
                        <?php 
                            // tồn đầu
                            $OpeningStock = isset($detailSubMaterial['OpeningStock'][$mMaterial->id])?$detailSubMaterial['OpeningStock'][$mMaterial->id]:0;
                            // Nhập xuất kho
                            $totalImport = isset($detailSubMaterial['Import'][$mMaterial->id])?$detailSubMaterial['Import'][$mMaterial->id]:0;
                            $totalExport = isset($detailSubMaterial['Export'][$mMaterial->id])?$detailSubMaterial['Export'][$mMaterial->id]:0;
                            if($OpeningStock==0 && $totalImport==0 && $totalExport==0)
                                continue;
                            $remain = $OpeningStock+$totalImport-$totalExport;
                            
                            if($obj['parent_obj']->materials_type_id==MATERIAL_TYPE_BINH_12){
                                $sum12OpeningStock += $OpeningStock;
                                $sum12Import += $totalImport;
                                $sum12Export += $totalExport;
                                $sum12Balance += $remain;
                            }
                        ?>
                        <tr>
                            <td class="item_sub_20"><?php echo $mMaterial->materials_no.' - '.$mMaterial->name;?></td>
                            <td class="item_c"><?php echo ActiveRecord::formatCurrency($OpeningStock);?></td>
                            <td class="item_c"><?php echo ActiveRecord::formatCurrency($totalImport);?></td>
                            <td class="item_c"><?php echo ActiveRecord::formatCurrency($totalExport);?></td>
                            <td class="item_c"><?php echo ActiveRecord::formatCurrency($remain);?></td>
                            <!--<td></td>-->
                        </tr>
                    <?php endforeach;?>
                                
                <?php endforeach;?>
                <tr class="display_none">
                    <td class="item_sub_20">Tổng Gas 12</td>
                    <td class="item_c sum12OpeningStockHide"><?php echo ActiveRecord::formatCurrency($sum12OpeningStock);?></td>
                    <td class="item_c sum12ImportHide"><?php echo ActiveRecord::formatCurrency($sum12Import);?></td>
                    <td class="item_c sum12ExportHide"><?php echo ActiveRecord::formatCurrency($sum12Export);?></td>
                    <td class="item_c sum12BalanceHide"><?php echo ActiveRecord::formatCurrency($sum12Balance);?></td>
                </tr>       
        </tbody>
        
    </table>
</div>

<?php else: ?>
    <h2>Không có dữ liệu</h2>
<?php endif; // end  if(isset($data['aSumEachType'] ?>
</div>

<script>
    $(function(){
        $('.sum12OpeningStock').text($('.sum12OpeningStockHide').text());
        $('.sum12Import').text($('.sum12ImportHide').text());
        $('.sum12Export').text($('.sum12ExportHide').text());
        $('.sum12Balance').text($('.sum12BalanceHide').text());
    });
</script>