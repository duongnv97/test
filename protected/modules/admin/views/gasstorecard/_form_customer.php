<div class="<?php echo $display_none_warehouse_new;?>">
    <h3 class='title-info'>Thông Tin Khách Hàng</h3>
    <div class="row radio-list-2" style=''>    
        <label>&nbsp;</label>
        <?php echo $form->radioButtonList($model,'ext_is_new_customer', CmsFormatter::$CUSTOMER_NEW_OLD_STORE_CARD, 
                    array('separator'=>"",'class'=>'select_customer' )); ?>            
    </div>
    <div class='clr'></div>

    <div class='customer customer_old customer_2 customer_hide_div'>
        <div class="row">
                <?php echo Yii::t('translation', $form->labelEx($model,'customer_id')); ?>
                <?php echo $form->hiddenField($model,'customer_id'); ?>
                <?php 
                        $this->widget('zii.widgets.jui.CJuiAutoComplete', array(
                                'attribute'=>'autocomplete_name',
                                'model'=>$model,
                                'sourceUrl'=>Yii::app()->createAbsoluteUrl('admin/ajax/search_user_by_code'),
                //                                'name'=>'my_input_name',
                                'options'=>array(
                                        'minLength'=>MIN_LENGTH_AUTOCOMPLETE,
                                        'multiple'=> true,
                                'search'=>"js:function( event, ui ) {
                                        $('#GasStoreCard_autocomplete_name').addClass('grid-view-loading-gas');
                                        } ",
                                'response'=>"js:function( event, ui ) {
                                        var json = $.map(ui, function (value, key) { return value; });
                                        if(json.length<1){
                                            var error = '<div class=\'errorMessage clr autocomplete_name_text\'>Không tìm thấy dữ liệu.</div>';
                                            if($('.autocomplete_name_text').size()<1)
                                                $('.autocomplete_name_error').after(error);
                                            else
                                                $('.autocomplete_name_error').parent('div').find('.autocomplete_name_text').show();                                                    
                                            $('.autocomplete_name_error').parent('div').find('.remove_row_item').hide();                                                    
                                        }
                                        $('#GasStoreCard_autocomplete_name').removeClass('grid-view-loading-gas');
                                        } ",

                                    'select'=>"js:function(event, ui) {
                                            $('#GasStoreCard_customer_id').val(ui.item.id);
                                            var remove_div = '<span class=\'remove_row_item\' onclick=\'fnRemoveName(this)\'></span>';
                                            $('#GasStoreCard_autocomplete_name').parent('div').find('.remove_row_item').remove();
                                            $('#GasStoreCard_autocomplete_name').attr('readonly',true).after(remove_div);
                                            fnBuildTableInfo(ui.item);
                                            $('.autocomplete_customer_info').show();
                                            $('.autocomplete_name_error').parent('div').find('.autocomplete_name_text').hide();
                                            if(ui.item.is_maintain==".STORE_CARD_KH_BINH_BO." && ".(isset($_GET['type_store_card'])?$_GET['type_store_card']:1)."==2){
                                                fnShowAlertGasRemain();
                                                fnGetPriceBinhBoByMonth(ui.item.id);
                                            }                                                
                                    }",
                                ),
                                'htmlOptions'=>array(
                                    'class'=>'autocomplete_name_error',
                                    'size'=>30,
                                    'maxlength'=>45,
                                    'style'=>'float:left;',
                                    'placeholder'=>'Nhập mã KH/NCC, Số ĐT. Tối thiểu '.MIN_LENGTH_AUTOCOMPLETE." ký tự",
                                ),
                        )); 
                        ?>        
                        <script>
                            function fnRemoveName(this_){
                                $(this_).prev().attr("readonly",false);                                     
                                $("#GasStoreCard_autocomplete_name").val("");
                                $("#GasStoreCard_customer_id").val("");
                                $('.autocomplete_customer_info').hide();
                                fnAfterRemoveName(this_);
                            }

                            function fnAfterRemoveName(this_){
                                $('.PriceBinhBoByMonth').val(1);
                            }

                            function fnBuildTableInfo(item){
                                $(".info_name").text(item.name_customer);
                                $(".info_name_agent").text(item.name_agent);
                                $(".info_code_account").text(item.code_account);
                                $(".info_code_bussiness").text(item.code_bussiness);
                                $(".info_address").text(item.address);
                                $(".info_phone").text(item.phone);
                            }
                        </script>        
                <?php /*if(!$model->isNewRecord &&  empty($model->customer_id)): ?>
                        <label style="font-weight: normal;"><?php echo CmsFormatter::$CUSTOMER_NEW_OLD_STORE_CARD[CUSTOMER_PAY_NOW]; ?></label>
                <?php endif; */?>

                <div class="clr"></div>		
                <?php echo $form->error($model,'customer_id'); ?>
                <?php $display='display:inline;';
                    $info_name ='';
                    $info_name_agent ='';
                    $info_address ='';
                    $info_code_account ='';
                    $info_code_bussiness ='';
                    $info_phone ='';

                        if(empty($model->customer_id)) $display='display: none;';
                        elseif($model->customer){       
                            $TypeCustomer = '';
                            if($model->customer->is_maintain){
                                $TypeCustomer = isset(CmsFormatter::$CUSTOMER_BO_MOI[$model->customer->is_maintain])?CmsFormatter::$CUSTOMER_BO_MOI[$model->customer->is_maintain]:"";
                            }

                            $info_name = $model->customer->first_name." - KH $TypeCustomer";
                            $info_name_agent = $model->customer->name_agent;
                            $info_code_account = $model->customer->code_account;
                            $info_code_bussiness = $model->customer->code_bussiness;
                            $info_address = $model->customer->address;
                            $info_phone = $model->customer->phone;
                        }                            
                ?>                        
                <div class="autocomplete_customer_info" style="<?php echo $display;?>">
                <table>
                    <tr>
                        <td class="_l">Mã:</td>
                        <td class="_r info_code_bussiness"><?php echo $info_code_bussiness;?></td>
                    </tr>

                    <tr>
                        <td class="_l">Tên:</td>
                        <td class="_r info_name"><?php echo $info_name;?></td>
                    </tr>                    
                    <tr>
                        <td class="_l">Địa chỉ:</td>
                        <td class="_r info_address"><?php echo $info_address;?></td>
                    </tr>
                    <tr>
                        <td class="_l">Điện Thoại:</td>
                        <td class="_r info_phone"><?php echo $info_phone;?></td>
                    </tr>

                </table>
            </div>
            <div class="clr"></div>    		
            <?php // echo $form->error($model,'customer_id'); ?>

        </div>
    </div>
        
    <div class='customer customer_new customer_1 customer_hide_div'>
        <div class="row">
            <?php echo $form->labelEx($model->modelUser,'first_name',array('label'=>'Tên Khách Hàng'));?>
            <?php // echo $form->dropDownList($model->modelUser,'last_name', CmsFormatter::$SALUTATION,array('style'=>'width:120px;','empty'=>'Chọn Xưng Hô')); ?>
            <?php echo $form->textField($model->modelUser,'first_name',array('style'=>'width:245px','maxlength'=>80)); ?>
            <?php echo $form->error($model->modelUser,'last_name'); ?>
            <?php echo $form->error($model->modelUser,'first_name'); ?>

        </div>
        <div class="row">
            <?php echo $form->labelEx($model->modelUser,'phone') ?>
            <?php echo $form->textField($model->modelUser,'phone',array('class'=>'phone_number_only no_phone_number_text ', 'style'=>'width:198px','maxlength'=>20)); ?>
            <div class="add_new_item">
                    <input type="checkbox" class="no_phone_number" ><em>Không có số điện thoại</em>
            </div>
            <?php echo $form->error($model->modelUser,'phone'); ?>
        </div> 

        <div class="row">
                <?php 
                    // vì cột is_maintain không dùng trong loại KH của thẻ kho nên ta sẽ dùng cho 1: KH bình bò, 2. KH mối
                    echo $form->labelEx($model->modelUser,'is_maintain'); ?>
                <?php echo $form->dropDownList($model->modelUser,'is_maintain', CmsFormatter::$CUSTOMER_BO_MOI,array('style'=>'width:376px','empty'=>'Select')); ?>
                <?php echo $form->error($model->modelUser,'is_maintain'); ?>
        </div>	

        <div class="row">
                <?php echo Yii::t('translation', $form->labelEx($model->modelUser,'province_id')); ?>
                <?php echo $form->dropDownList($model->modelUser,'province_id', GasProvince::getArrAll(),array('style'=>'width:376px','class'=>'','empty'=>'Select')); ?>
                <?php echo $form->error($model->modelUser,'province_id'); ?>
        </div> 	

        <div class="row">
                <?php echo Yii::t('translation', $form->labelEx($model->modelUser,'district_id')); ?>
                <?php echo $form->dropDownList($model->modelUser,'district_id', GasDistrict::getArrAll($model->modelUser->province_id, $model->modelUser->id),array('style'=>'width:376px','empty'=>'Select')); ?>
                                <div class="add_new_item"><a class="iframe_create_district" href="<?php echo Yii::app()->createAbsoluteUrl('admin/ajax/iframe_create_district') ;?>">Tạo Mới Quận Huyện</a><em> (Nếu trong danh sách không có)</em></div>
                <?php echo $form->error($model->modelUser,'district_id'); ?>
        </div>

        <div class="row">                        
                <?php echo Yii::t('translation', $form->labelEx($model->modelUser,'ward_id')); ?>
                <?php echo $form->dropDownList($model->modelUser,'ward_id', GasWard::getArrAll($model->modelUser->province_id, $model->modelUser->district_id),array('style'=>'width:376px','empty'=>'Select')); ?>
                <div class="add_new_item"><a class="iframe_create_ward" href="<?php echo Yii::app()->createAbsoluteUrl('admin/ajax/iframe_create_ward') ;?>">Tạo Mới Phường Xã</a><em> (Nếu trong danh sách không có)</em></div>
                <?php echo $form->error($model->modelUser,'ward_id'); ?>
        </div>	

        <div class="row">
                <?php echo $form->labelEx($model->modelUser,'house_numbers') ?>
                <?php echo $form->textField($model->modelUser,'house_numbers',array('style'=>'width:369px','maxlength'=>100)); ?>
                <?php echo $form->error($model->modelUser,'house_numbers'); ?>
        </div> 	

        <div class="row">
            <?php echo $form->labelEx($model->modelUser,'street_id') ?>
            <?php echo $form->hiddenField($model->modelUser,'street_id',array('style'=>'width:369px','maxlength'=>100)); ?>
            <?php 
            $this->widget('zii.widgets.jui.CJuiAutoComplete', array(
                    'attribute'=>'autocomplete_name_street',
                    'model'=>$model,
                    'sourceUrl'=>Yii::app()->createAbsoluteUrl('admin/ajax/autocomplete_data_streets'),
                    'options'=>array(
                        'minLength'=>MIN_LENGTH_AUTOCOMPLETE,
                        'multiple'=> true,
                        'search'=>"js:function( event, ui ) {
                                        $('#GasStoreCard_autocomplete_name_street').addClass('grid-view-loading-gas');
                                        } ",
                        'response'=>"js:function( event, ui ) {
                                var json = $.map(ui, function (value, key) { return value; });
                                if(json.length<1){
                                        var error = '<div class=\'errorMessage clr autocomplete_name_text_street\'>Không tìm thấy dữ liệu.</div>';
                                        if($('.autocomplete_name_text_street').size()<1)
                                            $('.autocomplete_name_error_street').parent('div').find('.add_new_item').after(error);
                                        else
                                            $('.autocomplete_name_error_street').parent('div').find('.autocomplete_name_text_street').show();
//                                                $('.autocomplete_name_error_street').parent('div').find('.autocomplete_name_text_street').hide();
                                }
                                $('#GasStoreCard_autocomplete_name_street').removeClass('grid-view-loading-gas');
                                } ",
                        'select'=>"js:function(event, ui) {
                                        $('#Users_street_id').val(ui.item.id);
                                        var remove_div = '<span class=\'remove_row_item\' onclick=\'fnRemoveNameStreet(this)\'></span>';
                                        $('#GasStoreCard_autocomplete_name_street').parent('div').find('.remove_row_item').remove();
                                        $('#GasStoreCard_autocomplete_name_street').attr('readonly',true).after(remove_div);
                                        $('.autocomplete_name_error_street').parent('div').find('.autocomplete_name_text_street').hide();
                        }",
                    ),
                    'htmlOptions'=>array(
                            'class'=>'autocomplete_name_error_street',
                            'size'=>45,
                            'maxlength'=>45,
                            'style'=>'float:left;width:369px;',
                            'placeholder'=>'Nhập tên đường tiếng việt không dấu',                            
                    ),
            )); 
            ?> 
                <script>
                        function fnRemoveNameStreet(this_){                                    
                                //$(this_).prev().attr("readonly",false); 
                                $(this_).parent('div').find('.autocomplete_name_error_street').attr("readonly",false); 
                                $("#GasStoreCard_autocomplete_name_street").val("");
                                $("#Users_street_id").val("");
                        }
                        function fnAddReadonlyStreet(){
                            return;
                                <?php if(!$model->isNewRecord):?>
                                var remove_div = '<span class=\'remove_row_item\' onclick=\'fnRemoveNameStreet(this)\'></span>';
                                $('#GasStoreCard_autocomplete_name_street').parent('div').find('.remove_row_item').remove();
                                $('#GasStoreCard_autocomplete_name_street').attr('readonly',true).after(remove_div);
                                <?php endif;?>
                        }	

                </script>             
                <div class="add_new_item"><a class="iframe_create_street" href="<?php echo Yii::app()->createAbsoluteUrl('admin/ajax/iframe_create_street') ;?>">Tạo Mới Đường</a><em> (Nếu trong danh sách không có)</em></div>
            <div class="clr"></div>
            <?php echo $form->error($model->modelUser,'street_id'); ?>
        </div> 	
    </div> <!-- end <div class='customer_new'> -->
</div>


<script>
    $(function(){
        <?php if($DieuPhoiCreate): ?>
            $('#GasStoreCard_autocomplete_name').remove();
            $('.radio-list-2').hide();
        <?php endif; ?>
    });

</script>