<?php 
$url = Yii::app()->createAbsoluteUrl('admin/gasstorecard/view_store_movement_summary/excel/1');
?>
<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/print-store-card.css" />
<div class="search-form" style="">
    <div class="wide form">

    <?php $form=$this->beginWidget('CActiveForm', array(
            'action'=> $url,
            'method'=> 'post',
    )); ?>
<div class="row">
    <div class="row more_col">
        <div class="col1">
            <?php echo $form->label($model,'date_from') ?>		
            <?php 
                $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                    'model'=>$model,        
                    'attribute'=>'date_from',
                    'options'=>array(
                        'showAnim'=>'fold',
                        'dateFormat'=> 'dd-mm-yy',
                        'maxDate'=> '0',
                        'changeMonth' => true,
                        'changeYear' => true,
                        'showOn' => 'button',
                        'buttonImage'=> Yii::app()->theme->baseUrl.'/admin/images/icon_calendar_r.gif',
                        'buttonImageOnly'=> true,                                
                    ),        
                    'htmlOptions'=>array(
                        'class'=>'w-16',
                        'style'=>'height:20px;float:left;',
                            'readonly'=>'readonly',
                    ),
                ));
            ?>
        </div>
        <div class="col2">
            <?php echo $form->label($model,'date_to') ?>		
            <?php 
                $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                    'model'=>$model,        
                    'attribute'=>'date_to',
                    'options'=>array(
                        'showAnim'=>'fold',
                        'dateFormat'=> 'dd-mm-yy',
                        'maxDate'=> '0',
                        'changeMonth' => true,
                        'changeYear' => true,
                        'showOn' => 'button',
                        'buttonImage'=> Yii::app()->theme->baseUrl.'/admin/images/icon_calendar_r.gif',
                        'buttonImageOnly'=> true,                                
                    ),        
                    'htmlOptions'=>array(
                        'class'=>'w-16',
                        'style'=>'height:20px;float:left;',
                            'readonly'=>'readonly',
                    ),
                ));
            ?>    
        </div>
    </div>
</div>
<div class="row more_col">
    <div class="col1 w-600">
        <?php echo $form->label($model,'province_id_agent', ['label'=>'Tỉnh']); ?>
        <div class="fix-label">
            <?php
               $this->widget('ext.multiselect.JMultiSelect',array(
                     'model'=>$model,
                     'attribute'=>'province_id_agent',
                     'data'=> GasProvince::getArrAll(),
                     // additional javascript options for the MultiSelect plugin
                    'options'=>array('selectedList' => 30,),
                     // additional style
                     'htmlOptions'=>array('class' => 'w-400'),
               ));    
           ?>
        </div>
    </div>

    <div class="col2">
        <?php echo $form->labelEx($model,'update_note',array('label'=>'Không xem kho', 'class'=>'checkbox_one_label', 'style'=>'padding-top:3px;')); ?>
        <?php echo $form->checkBox($model,'update_note',array('class'=>'float_l')); ?>
    </div>
</div>

        <div class="row buttons" style="padding-left: 159px; margin:20px 0;">
            <?php $this->widget('bootstrap.widgets.TbButton', array(
                'buttonType'=>'submit',
                'label'=>Yii::t('translation','Xuất Excel'),
                'type'=>'null', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
                'size'=>'small', // null, 'large', 'small' or 'mini'
                //'htmlOptions' => array('style' => 'margin-bottom: 10px; float: right;'),
            )); ?>	
           
            
        </div>

    <?php $this->endWidget(); ?>

    </div><!-- wide form -->
</div><!-- search-form -->
