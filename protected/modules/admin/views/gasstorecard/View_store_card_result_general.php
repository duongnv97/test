
<?php if(isset($data['aMaterials'])): ?>


<div class="container" id="printElement">
    <table cellpadding="0" cellspacing="0">
        <tr>
            <td>
                <div class="logo">
                    <img src="<?php echo Yii::app()->theme->baseUrl; ?>/images/logo80x80.png">
                </div>            
                <p style="margin: 5px 0 20px 0;">CÔNG TY TNHH HƯỚNG MINH</p>
            </td>
            <td valign="top">
                <h2 style="margin: 35px 0 0">Thẻ Kho Tổng Hợp</h2>
            </td>
        </tr>
        <tr>
            <td class="agent_info"><b>Tên đại Lý</b>: <?php echo $model->agent?$model->agent->first_name:''?></td>
            <td style="text-align: right;"><b>Ngày lập thẻ</b>: <?php echo str_replace('-', '/', $model->date_delivery) ?></td>
        </tr>

    </table>
    <table cellpadding="0" cellspacing="0" class="tb hm_table">
        <thead>
            <tr>
                <th colspan="6">Thẻ Kho Tổng Hợp</th>
            </tr>
            <tr>
                <th rowspan="2">Vật Tư Nhóm Cha</th>
                <th colspan="4">Số lượng</th>
                <th rowspan="2">Người kiểm (ký-tên)</th>
            </tr>
            <tr>
                <th>Tồn Đầu</th>
                <?php foreach(CmsFormatter::$STORE_CARD_TYPE as $id_type=>$name):?>
                    <th><?php echo $name;?></th>
                <?php endforeach;?>
                <!-- <th>Xuất bán</th>-->
                <th>Tồn</th>
            </tr>
            
        </thead>
        <tbody>
            <?php
                $aMaterials = $data['aMaterials'];
                $parent_obj = $data['parent_obj'];
                $array_materials_type_id = $data['array_materials_type_id']; // all result at here
                $mMaterialType = GasMaterialsType::getAllItem();            
            ?>           
            <?php foreach($array_materials_type_id as $materials_type_id=>$arr_parent_obj_id): ?>
                    <?php 
                        // for row of material type
                        $opening_balance = $data[$materials_type_id]['opening_balance'];
                        $totalImport = $data[$materials_type_id]['totalImport'];
                        $totalExport = $data[$materials_type_id]['totalExport'];
                        $remain = $opening_balance+$totalImport-$totalExport;
                        // for row of material type
                    ?>            
                
                    <tr>
                        <td class="item_b"><?php echo isset($mMaterialType[$materials_type_id])?$mMaterialType[$materials_type_id]:'';?></td>
                        <td class="item_c item_b"><?php echo ActiveRecord::formatCurrency($opening_balance);?></td>
                        <td class="item_c item_b"><?php echo ActiveRecord::formatCurrency($totalImport);?></td>
                        <td class="item_c item_b"><?php echo ActiveRecord::formatCurrency($totalExport);?></td>
                        <td class="item_c item_b"><?php echo ActiveRecord::formatCurrency($remain);?></td>
                        <td></td>
                    </tr>
                <?php  foreach($arr_parent_obj_id as $parent_id=>$arr_obj): ?>
                    <?php 
                        $opening_balance = $arr_obj['opening_balance'];
                        $totalImport = $arr_obj['totalImport'];
                        $totalExport = $arr_obj['totalExport'];
                        $remain = $opening_balance+$totalImport-$totalExport;
                    ?>
                    <tr>
                        <td class="item_sub_20"><?php echo $parent_obj[$parent_id]->name;?></td>
                        <td class="item_c"><?php echo ActiveRecord::formatCurrency($opening_balance);?></td>
                        <td class="item_c"><?php echo ActiveRecord::formatCurrency($totalImport);?></td>
                        <td class="item_c"><?php echo ActiveRecord::formatCurrency($totalExport);?></td>
                        <td class="item_c"><?php echo ActiveRecord::formatCurrency($remain);?></td>
                        <td></td>
                    </tr>            
                <?php endforeach;   // end foreach($arr_parent_obj_id ?>
            <?php endforeach; // end foreach($array_materials_type_id as ?>
        </tbody>
        
    </table>
</div>

<?php else: ?>
<div class="container" id="printElement">
    <h2>Không có dữ liệu</h2>
</div>
<?php endif; // end  if(isset($data['aSumEachType'] ?>

