<?php 
    $cRole = MyFormat::getCurrentRoleId();
    if($cRole == ROLE_SUB_USER_AGENT){
        $aCar = Users::getSelectByRoleForAgent(MyFormat::getAgentId(), ONE_AGENT_CAR, '', array('status'=>1));
    }else{
        $aCar = Users::getArrDropdown(ROLE_CAR);
    }
?>
<div class="wide form ">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action' => GasCheck::getCurl(),
	'method'=>'get',
)); ?>
    
    <div class="row">
    <?php echo $form->labelEx($model,'customer_id'); ?>
    <?php echo $form->hiddenField($model,'customer_id', array('class'=>'')); ?>
    <?php
            // 1. limit search kh của sale
            $url = Yii::app()->createAbsoluteUrl('admin/ajax/search_user_by_code');
            // widget auto complete search user customer and supplier
            $aData = array(
                'model'=>$model,
                'field_customer_id'=>'customer_id',
                'url'=> $url,
                'name_relation_user'=>'rCustomer',
                'ClassAdd' => 'w-400',
                'field_autocomplete_name' => 'autocomplete_name',
            );
            $this->widget('ext.SpaAutocompleteCustomer.SpaAutocompleteCustomer',
                array('data'=>$aData));
            ?>
    </div>
    
    <?php if(MyFormat::getCurrentRoleId() != ROLE_SALE):?>
    <div class="row">
        <?php echo $form->label($model,'ext_sale_id', array('ext_sale_id'=>'Nhân viên KD')); ?>
        <?php echo $form->hiddenField($model,'ext_sale_id'); ?>
        <?php 
            // widget auto complete search user customer and supplier
            $aData = array(
                'model'=>$model,
                'field_customer_id'=>'ext_sale_id',
                'field_autocomplete_name'=>'autocomplete_name_1',
                'url'=> Yii::app()->createAbsoluteUrl('admin/ajax/search_sale'),
                'name_relation_user'=>'rSale',
                'placeholder'=>'Nhập Tên Sale',
                'ClassAdd' => 'w-500',
//                        'fnSelectCustomer'=>'fnAfterSelectSaleOrAgent',
            );
            $this->widget('ext.SpaAutocompleteCustomer.SpaAutocompleteCustomer',
                array('data'=>$aData));                                        
        ?>
    </div>
    <?php endif;?>

    <div class="row more_col">
        <div class="col1">
            <?php echo $form->label($model,'statistic_month'); ?>
            <?php echo $form->dropDownList($model,'statistic_month', ActiveRecord::getMonthVnWithZeroFirst(), array('class'=>' w-200', 'empty'=>'Select')); ?>		
        </div>

        <div class="col2">
            <?php echo $form->labelEx($model,'statistic_year'); ?>
            <?php echo $form->dropDownList($model,'statistic_year', ActiveRecord::getRangeYear(), array('class'=>' w-200', 'empty'=>'Select')); ?>		
        </div>
        <div class="col3">
        </div>
    </div>

    <div class="row more_col">
        <div class="col1">
            <?php echo $form->label($model,'created_date', array('label'=>'Ngày tạo thẻ kho')); ?>
            <?php 
                $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                    'model'=>$model,        
                    'attribute'=>'created_date',
                    'options'=>array(
                        'showAnim'=>'fold',
                        'dateFormat'=> MyFormat::$dateFormatSearch,
//                            'minDate'=> '0',
                        'maxDate'=> '0',
                        'changeMonth' => true,
                        'changeYear' => true,
                        'showOn' => 'button',
                        'buttonImage'=> Yii::app()->theme->baseUrl.'/admin/images/icon_calendar_r.gif',
                        'buttonImageOnly'=> true,                                
                    ),        
                    'htmlOptions'=>array(
                        'class'=>'w-170',
                        'size'=>'16',
                        'style'=>'height:20px;',                                
                    ),
                ));
            ?>     		
            <?php echo $form->error($model,'created_date'); ?>
        </div>

        <div class="col2">
            <?php echo $form->labelEx($model,'car_id'); ?>
            <?php echo $form->dropDownList($model,'car_id', $aCar,array('class'=>'w-200 float_l', 'empty'=>'Select')); ?>
        </div>
        <div class="col3">
            <?php echo $form->labelEx($model,'road_route'); ?>
            <?php echo $form->dropDownList($model,'road_route', $model->getArrRoadRoute(),array('class'=>' w-200','empty'=>'Select')); ?>
        </div>
    </div>	

    <div class="row more_col">
        <div class="col1">
            <?php echo $form->labelEx($model,'uid_login'); ?>
            <?php echo $form->dropDownList($model,'uid_login', GasTickets::$DIEU_PHOI_FULLNAME,array('class'=>' w-200','empty'=>'Select')); ?>
        </div>
        <?php if(Yii::app()->user->role_id!=ROLE_SUB_USER_AGENT):?>
        <div class="col3">
            <?php // echo $form->label($model,'user_id_create'); ?>
            <?php // echo $form->dropDownList($model,'user_id_create', Users::getSelectByRole(),array('style'=>'width:200px;','empty'=>'Select')); ?>
            <?php echo $form->labelEx($model,'reason_false'); ?>
            <?php echo $form->dropDownList($model,'reason_false', $model->getStatusFalse(true),array('class' =>"w-200 ", 'empty'=>'Select')); ?>
        </div>
        <?php endif;?>
    </div>

    <div class="row more_col">
        <div class="col1">
            <?php echo $form->label($model,'date_delivery', array('label' => 'Ngày nhập xuất')); ?>
            <?php 
                $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                    'model'=>$model,        
                    'attribute'=>'date_delivery',
                    'options'=>array(
                        'showAnim'=>'fold',
                        'dateFormat'=> MyFormat::$dateFormatSearch,
//                            'minDate'=> '0',
                        'maxDate'=> '0',
                        'changeMonth' => true,
                        'changeYear' => true,
                        'showOn' => 'button',
                        'buttonImage'=> Yii::app()->theme->baseUrl.'/admin/images/icon_calendar_r.gif',
                        'buttonImageOnly'=> true,                                
                    ),        
                    'htmlOptions'=>array(
                        'class'=>'w-170',
                        'size'=>'16',
                        'style'=>'height:20px;',                               
                    ),
                ));
            ?>     		
            <?php echo $form->error($model,'date_delivery'); ?>
        </div>
        <div class="col2">
            <?php // echo $form->label($model,'delivery_person',array()); ?>
            <?php // echo $form->textField($model,'delivery_person',array('class' => 'w-200','maxlength'=>80)); ?>
            <?php 
            // vì cột is_maintain không dùng trong loại KH của thẻ kho nên ta sẽ dùng cho 1: KH bình bò, 2. KH mối
            echo $form->labelEx($model,'ext_is_maintain'); ?>
            <?php echo $form->dropDownList($model,'ext_is_maintain', CmsFormatter::$CUSTOMER_BO_MOI,array('class'=>' w-200','empty'=>'Select')); ?>
            <?php // echo $form->dropDownList($model,'ext_is_maintain', GasStoreCard::$DAILY_INTERNAL_TYPE_CUSTOMER, array('class'=>' w-200','empty'=>'Select')); ?>
        </div>
        <div class="col3">
            <?php echo $form->labelEx($model,'ext_materials_type_id'); ?>
            <?php echo $form->dropDownList($model,'ext_materials_type_id', GasMaterialsType::getAllItem(),array('class'=>' w-200','empty'=>'Select')); ?>
        </div>
    </div>

    <div class="row more_col">
        <div class="col1">
            <?php echo $form->label($model,'store_card_no',array()); ?>
            <?php echo $form->textField($model,'store_card_no',array('class'=>' w-190','maxlength'=>20)); ?>
        </div>
        <div class="col2">
            <?php echo $form->label($model,'store_card_real', ['label'=>'Mã App bò mối']); ?>
            <?php echo $form->textField($model,'store_card_real',array('class'=>' w-200','maxlength'=>30)); ?>
        </div>
        <div class="col3">
            <?php echo $form->label($model,'type_store_card',array()); ?>
            <?php echo $form->dropDownList($model,'type_store_card', CmsFormatter::$STORE_CARD_TYPE,array('class'=>'w-200', 'empty'=>'Select')); ?>		
        </div>
    </div>

    <div class="row more_col">
        <div class="col1 materials_name_search ">
            <?php echo $form->labelEx($model,'materials_name'); ?>
            <?php echo $form->hiddenField($model,'ext_materials_id', array('class'=>'ext_materials_id')); ?>
            <?php echo $form->textField($model,'materials_name',array('class'=>'materials_name w-200', 'placeholder'=>'Nhập mã vật tư hoặc tên vật tư', 'style'=>'float:left;')); ?>
            <span class="remove_row_item" onclick="fnRemoveNameMaterial(this);"></span>
        </div>
        
        <div class="col2">
            <?php echo $form->labelEx($model,'notViewWarehouse',array('class'=>'checkbox_one_label', 'style'=>'padding-top:3px;')); ?>
            <?php echo $form->checkBox($model,'notViewWarehouse',array('class'=>'float_l')); ?>
        </div>
    </div>

    <?php if(MyFormat::getCurrentRoleId() != ROLE_SUB_USER_AGENT):?>
    <div class="row">
    <?php echo $form->labelEx($model,'user_id_create'); ?>
    <?php echo $form->hiddenField($model,'user_id_create', array('class'=>'')); ?>
    <?php
            // 1. limit search kh của sale
            $url = Yii::app()->createAbsoluteUrl('admin/ajax/search_by_role', array('role'=> ROLE_AGENT));
            // widget auto complete search user customer and supplier
            $aData = array(
                'model'=>$model,
                'field_customer_id'=>'user_id_create',
                'url'=> $url,
                'name_relation_user'=>'rAgent',
                'ClassAdd' => 'w-400',
                'field_autocomplete_name' => 'autocomplete_name_sale',
                'placeholder'=>'Nhập mã hoặc tên đại lý',
            );
            $this->widget('ext.SpaAutocompleteCustomer.SpaAutocompleteCustomer',
                array('data'=>$aData));
            ?>
    </div>
    <?php endif; ?>
    <div class="row more_col">
        <div class="col1">
            <?php echo $form->label($model,'date_from') ?>		
            <?php 
                $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                    'model'=>$model,        
                    'attribute'=>'date_from',
                    'options'=>array(
                        'showAnim'=>'fold',
                        'dateFormat'=> 'dd-mm-yy',
                        'maxDate'=> '0',
                        'changeMonth' => true,
                        'changeYear' => true,
                        'showOn' => 'button',
                        'buttonImage'=> Yii::app()->theme->baseUrl.'/admin/images/icon_calendar_r.gif',
                        'buttonImageOnly'=> true,                                
                    ),        
                    'htmlOptions'=>array(
                        'class'=>'w-16',
                        'style'=>'height:20px;float:left;',
                    ),
                ));
            ?>
        </div>

        <div class="col2">
            <?php echo $form->label($model,'date_to') ?>		
            <?php 
                $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                    'model'=>$model,        
                    'attribute'=>'date_to',
                    'options'=>array(
                        'showAnim'=>'fold',
                        'dateFormat'=> 'dd-mm-yy',
                        'maxDate'=> '0',
                        'changeMonth' => true,
                        'changeYear' => true,
                        'showOn' => 'button',
                        'buttonImage'=> Yii::app()->theme->baseUrl.'/admin/images/icon_calendar_r.gif',
                        'buttonImageOnly'=> true,                                
                    ),        
                    'htmlOptions'=>array(
                        'class'=>'w-16',
                        'style'=>'height:20px;float:left;',
                    ),
                ));
            ?>
        </div>
        <div class="col3"></div>
    </div>
    
    <div class="row">
        <?php echo $form->label($model,'type_in_out',[]); ?>
        <?php // echo $form->dropDownList($model,'master_lookup_id', $model->getListdataLookupType(),array('class'=>'w-200', 'empty'=>'Select')); ?>
        <div class="fix-label">
            <?php
               $this->widget('ext.multiselect.JMultiSelect',array(
                     'model'=>$model,
                     'attribute'=>'type_in_out',
                     'data'=> CmsFormatter::$STORE_CARD_ALL_TYPE,
                     // additional javascript options for the MultiSelect plugin
                    'options'=>array('selectedList' => 30,),
                     // additional style
                     'htmlOptions'=>array('style' => 'width: 800px;'),
               ));    
           ?>
        </div>
    </div>
    <div class="row">
        <?php echo $form->labelEx($model,'province_id_agent'); ?>
        <div class="fix-label">
            <?php
               $this->widget('ext.multiselect.JMultiSelect',array(
                     'model'=>$model,
                     'attribute'=>'province_id_agent',
                     'data'=> GasProvince::getArrAll(),
                     // additional javascript options for the MultiSelect plugin
                    'options'=>array('selectedList' => 30,),
                     // additional style
                     'htmlOptions'=>array('style' => 'width: 800px;'),
               ));    
           ?>
        </div>
    </div>
    
    <div class="row buttons" style="padding-left: 159px;">
        <?php $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType'=>'submit',
            'label'=>'Search',
            'type'=>'null', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
            'size'=>'small', // null, 'large', 'small' or 'mini'
            //'htmlOptions' => array('style' => 'margin-bottom: 10px; float: right;'),
        )); ?>	
    </div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->

<script>
    $(function(){
        var availableMaterials = <?php echo CacheSession::getListMaterialJson(); ?>;
        var parent_div = $('.materials_name_search');
        $( "#GasStoreCard_materials_name" ).autocomplete({
            source: availableMaterials,
            close: function( event, ui ) {  },
            select: function( event, ui ) {
                parent_div.find('.materials_name').attr("readonly",true); 
                parent_div.find('.ext_materials_id').val(ui.item.id);
            }

        });
    });
    
    function fnRemoveNameMaterial(this_){
        var parent_div = $(this_).closest('div.materials_name_search');
        parent_div.find('.materials_name').attr("readonly",false);
        parent_div.find('.materials_name').val("");
        parent_div.find('.ext_materials_id').val("");
    }
    
    
</script>