<?php $session=Yii::app()->session; if($model->getError('errCheckValidQtyKhoAndAgent') && isset($session['STORE_CARD_ID_AGENT_NOT_VALID']) && !empty($session['STORE_CARD_ID_AGENT_NOT_VALID'])): ?>
    <div class="errorSummary"><p>Có Lỗi Xảy Ra:<?php echo $model->getError('errCheckValidQtyKhoAndAgent');?>. Chi Tiết Nhập Hàng Của Đại Lý Như Sau:</p>
        <ul>
            <?php             
            $STORE_CARD_ID_AGENT_NOT_VALID = $session['STORE_CARD_ID_AGENT_NOT_VALID'];
            unset($session['STORE_CARD_ID_AGENT_NOT_VALID']);
            $mStorecardErr = GasStoreCard::model()->findByPk($STORE_CARD_ID_AGENT_NOT_VALID);                    
            $cmsFormater = new CmsFormatter();
            $sum_qty=0;
            ?>
            <?php if($mStorecardErr):?>
            <li><span class="item_b">Ngày:</span> <?php echo MyFormat::dateConverYmdToDmy($mStorecardErr->date_delivery);?></li>
            <li><span class="item_b">Mã Thẻ Kho:</span> <?php echo $mStorecardErr->store_card_no;?></li>
            <li><span class="item_b">Khách Hàng:</span> <?php echo $cmsFormater->formatNameCustomerStoreCard($mStorecardErr);?></li>
            <li><span class="item_b">Loại Nhập Xuất:</span> <?php echo $mStorecardErr->getStoreCardTypeInOut();?></li>
            <li>
            <?php if(count($mStorecardErr->rStoreCardDetail)): ?>
                <table class="view_materials_table hm_table">
                <thead>
                    <tr>
                        <th class="w-30 item_c">#</th>
                        <th class="w-80 item_c">Mã</th>
                        <th class="w-250 item_c ">Tên</th>
                        <th class="w-50 item_c">ĐVT</th>
                        <th class="w-50 item_c ">SL</th>
                        <th class=" item_r ">Đơn Giá</th>
                        <th class=" item_r last">Thành Tiền</th>
                    </tr>
                </thead>
                <tbody>
                        <?php foreach($mStorecardErr->rStoreCardDetail as $key=>$item):?>
                        <?php $mMaterial = GasMaterials::model()->findBypk($item->materials_id); ?>
                        <?php if($mMaterial): $sum_qty+=$item->qty; ?>
                        <tr class="">
                            <td class=" item_c"><?php echo ($key+1);?></td>
                            <td><?php echo $mMaterial->materials_no;?></td>
                            <td class="item_l"><?php echo $mMaterial->name;?></td>
                            <td class="item_c"><?php echo $mMaterial->unit;?></td>
                            <td class=" item_c"><?php echo ActiveRecord::formatCurrency($item->qty);?></td>
                            <td class=" item_r"><?php echo ActiveRecord::formatCurrency($item->price);?></td>
                            <td class="last item_b item_r"><?php echo ActiveRecord::formatCurrency($item->amount);?></td>
                        </tr>                    
                        <?php endif;?>
                        <?php endforeach;?>
                </tbody>
                <tfoot>
                    <tr class="">
                        <td class="item_b item_r f_size_16" colspan="4">Tổng Cộng</td>
                        <td class="item_b item_c f_size_16"><?php echo ActiveRecord::formatCurrency($sum_qty);?></td>
                        <td></td>
                        <td class="item_b item_r f_size_16"><?php echo ActiveRecord::formatCurrency($mStorecardErr->receivables_customer);?></td>
                        <td class="last"></td>
                    </tr>
                </tfoot>
            </table>


            <?php endif;?>
            </li>
            <?php endif;?>
        </ul>
    </div>
<?php endif; ?>