<?php
$this->breadcrumbs=array(
	'Thẻ Kho'=>array('index'),
	$model->store_card_no=>array('view','id'=>$model->id),
	'Cập Nhật',
);

$menus = array(	
        array('label'=>'Thẻ Kho', 'url'=>array('index')),
	array('label'=>'Xem Thẻ Kho', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Tạo Mới Thẻ Kho', 'url'=>array('create', 'type_store_card'=>$model->type_store_card)),	
);
$this->menu= ControllerActionsName::createMenusRoles($menus, $actions);
$msg = 'Cập nhật xuất kho: ';
$color = '';
if($model->type_store_card == TYPE_STORE_CARD_IMPORT){
    $msg = 'Cập nhật nhập kho: ';
    $color = 'color: blue;';
}
        
?>

<h1 style="<?php echo $color;?>"><?php echo $msg. $model->store_card_no; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>