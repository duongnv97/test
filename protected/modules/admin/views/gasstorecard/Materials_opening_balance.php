<?php
$this->breadcrumbs=array(
	'Nhập Dư Đầu Kỳ Vật Tư',
);

$readonly = "readonly='1'";
$text='Bạn Đang Xem Dư Đầu Kỳ Vật Tư. Nếu Muốn Chỉnh Sửa Liên Hệ Với Người Quản Lý';
if(in_array(Yii::app()->user->parent_id, CmsFormatter::$PROVINCE_ALLOW)):
    $readonly='';
    $text='Nhập Dư Đầu Kỳ Vật Tư';
endif;

?>

<?php // if(in_array(Yii::app()->user->province_id, CmsFormatter::$PROVINCE_ALLOW)): // định cho phép theo vùng?>
<?php // if(in_array(Yii::app()->user->parent_id, CmsFormatter::$PROVINCE_ALLOW)): // fix lại cho phép theo đại lý cho nhanh?>
<?php if(1): // fix lại cho phép theo đại lý cho nhanh?>
<h1><?php echo $text;?></h1>
<div class="form opening_balance">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'gas-materials-form',
	'enableAjaxValidation'=>false,
)); ?>
    
        <?php if(empty($readonly)): ?>
	<div class="row buttons" style="padding-left: 154px;">
            <?php $this->widget('bootstrap.widgets.TbButton', array(
                'buttonType'=>'submit',
                'label'=>"Save",
                'type'=>'null', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
                'size'=>'small', // null, 'large', 'small' or 'mini'
                //'htmlOptions' => array('style' => 'margin-bottom: 10px; float: right;'),
            )); ?>	
        
        </div>
        <?php endif; ?>
    
        <?php if(Yii::app()->user->hasFlash('successUpdate')): ?>
            <div class="success_div"><?php echo Yii::app()->user->getFlash('successUpdate');?></div>         
        <?php endif; ?>  	
            <div class="clr"></div>
    
    <?php
        $aModelMaterials = GasMaterials::getArrayModelMaterials();
        $aOpeningBalanceOfAgent = GasMaterialsOpeningBalance::getOpeningBalanceByAgent(MyFormat::getAgentId());
    ?>
    <?php foreach ($aModelMaterials as $arrObj):?>
    <?php 
        $mParent = $arrObj['parent_obj'];
        $mSub = $arrObj['sub_arr_model'];
    ?>
        <fieldset>
            <legend><?php echo $mParent->name;?></legend>
            <div class="row more_col">
                <?php foreach($mSub as $material):?>
                    <?php $val = isset($aOpeningBalanceOfAgent[$material->id])?$aOpeningBalanceOfAgent[$material->id]:''; ?>
                <div class="row_4">
                    <label><?php echo $material->name.(empty($material->unit)?'-': " ($material->unit)-").$material->materials_no;?></label>
                    <input <?php echo $readonly;?> value="<?php echo $val>0?ActiveRecord::formatNumberInput($val):'';?>" class="number_only_v1" name="GasMaterialsOpeningBalance[materials_id][<?php echo $material->id;?>]" type="text" size="6" maxlength="6">                    
                </div>
                <?php endforeach;?>
            </div>
        </fieldset>
    <?php    endforeach;?>

            
        <?php if(empty($readonly)): ?>
	<div class="row buttons" style="padding-left: 154px;">
            <?php $this->widget('bootstrap.widgets.TbButton', array(
                    'buttonType'=>'submit',
                    'label'=>"Save",
                    'type'=>'null', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
                    'size'=>'small', // null, 'large', 'small' or 'mini'
                    //'htmlOptions' => array('style' => 'margin-bottom: 10px; float: right;'),
            )); ?>	
        
        </div>
        <?php endif; ?>
<?php $this->endWidget(); ?>

</div><!-- form -->

<?php else:?>
    <h1>Chức năng hiện đã bị khóa, vui lòng liên hệ với người quản lý</h1>
<?php endif;?>