<?php
$this->breadcrumbs=array(
	'Xem và in ấn thẻ kho',
);?>

<h1>Xem và in ấn thẻ kho</h1>
<div class="search-form" style="">
    <div class="wide form">

    <?php $form=$this->beginWidget('CActiveForm', array(
            'action'=>Yii::app()->createUrl($this->route),
            'method'=>'post',
    )); ?>
            <?php if(Yii::app()->user->hasFlash('successUpdate')): ?>
                <div class="success_div"><?php echo Yii::app()->user->getFlash('successUpdate');?></div>         
            <?php endif; ?>    
                
            <div class="row">
                    <?php echo Yii::t('translation', $form->label($model,'date_delivery')); ?>		
                    <?php 
                        $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                            'model'=>$model,        
                            'attribute'=>'date_delivery',
                            'options'=>array(
                                'showAnim'=>'fold',
                                'dateFormat'=> 'dd-mm-yy',
                                'maxDate'=> '0',
                                'changeMonth' => true,
                                'changeYear' => true,
                                'showOn' => 'button',
                                'buttonImage'=> Yii::app()->theme->baseUrl.'/admin/images/icon_calendar_r.gif',
                                'buttonImageOnly'=> true,                                
                            ),        
                            'htmlOptions'=>array(
                                'class'=>'w-16',
                                'style'=>'height:20px;',
                                'readonly'=>true,
                            ),
                        ));
                    ?>            
                    <?php echo $form->error($model,'date_delivery'); ?>
            </div>
            
            <?php if(Yii::app()->user->role_id!=ROLE_SUB_USER_AGENT):?>    
            <div class="row">
                <?php // echo Yii::t('translation', $form->labelEx($model,'agent_id')); ?>
                <?php // echo $form->dropDownList($model,'agent_id', Users::getSelectByRole(),array('style'=>'','empty'=>'Select')); ?>
            </div>    
            <div class="row">
            <?php echo $form->labelEx($model,'agent_id'); ?>
            <?php echo $form->hiddenField($model,'agent_id', array('class'=>'')); ?>
            <?php
                    // 1. limit search kh của sale
                    $url = Yii::app()->createAbsoluteUrl('admin/ajax/search_by_role', array('role'=> ROLE_AGENT));
                    // widget auto complete search user customer and supplier
                    $aData = array(
                        'model'=>$model,
                        'field_customer_id'=>'agent_id',
                        'url'=> $url,
                        'name_relation_user'=>'agent',
                        'ClassAdd' => 'w-400',
                        'field_autocomplete_name' => 'autocomplete_name',
                        'placeholder'=>'Nhập mã hoặc tên đại lý',
                    );
                    $this->widget('ext.SpaAutocompleteCustomer.SpaAutocompleteCustomer',
                        array('data'=>$aData));
                    ?>
                <?php echo $form->error($model,'agent_id'); ?>
            </div>
            <?php endif;?>    
                
            <div class="row display_none">
                <?php echo $form->labelEx($model,'checkbox_view_general'); ?>
                <?php echo $form->checkBox($model,'checkbox_view_general',
                    array('class'=>'checkbox_view_general','onclick'=>'fnCheckbox_view_general();')); ?>
            </div>    
                
            <?php // if(!$model->checkbox_view_general): ?>    
            <div class="row row_materials_name">
                <div>
                <?php echo $form->labelEx($model,'materials_name', array('class'=>'label_materials_name')); ?>
                <?php // echo $form->textField($model,'materials_name',array('size'=>54,'placeholder'=>'Nhập mã vật tư hoặc tên vật tư')); ?>
                <?php echo $form->hiddenField($model,'ext_materials_id'); ?>
                    
                <?php 
                    // widget auto complete search material
                    $aData = array(
                        'model'=>$model,
                        'field_material_id'=>'ext_materials_id',
                        'name_relation_material'=>'materials',
                        'field_autocomplete_name'=>'materials_name',
                    );
                    $this->widget('ext.GasAutocompleteMaterial.GasAutocompleteMaterial',
                        array('data'=>$aData));                                        
                ?>                     
                    
                </div>
            </div>
            <?php // endif;?>
                
            <div class="row">
                <div class="sprint">
                    <a class="button_print" href="javascript:void(0);">
                        <img src="<?php echo Yii::app()->theme->baseUrl; ?>/images/print.png">
                    </a>
                </div>
            </div>

            <div class="row buttons" style="padding-left: 159px;margin:20px 0;">
                            <?php $this->widget('bootstrap.widgets.TbButton', array(
                'buttonType'=>'submit',
                'label'=>Yii::t('translation','Xem Thống Kê'),
                'type'=>'null', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
                'size'=>'small', // null, 'large', 'small' or 'mini'
                //'htmlOptions' => array('style' => 'margin-bottom: 10px; float: right;'),
            )); ?>	
            <?php if(ControllerActionsName::isAccessAction('statistic_maintain_export_excel', $actions)):?>
                    <input class='statistic_maintain_export_excel' next='<?php echo Yii::app()->createAbsoluteUrl('admin/gasmaintain/statistic_maintain_export_excel');?>' type='button' value='Xuất Thống Kê Hiện Tại Ra Excel'>
            <?php endif;?>

            </div>

    <?php $this->endWidget(); ?>

    </div><!-- wide form -->
</div><!-- search-form -->

<?php if($model->checkbox_view_general): ?>
    <?php include_once 'View_store_card_result_general.php';?>
<?php else:?>
    <?php include_once 'View_store_card_result.php';?>
<?php endif;?>

<script  src="<?php echo Yii::app()->theme->baseUrl; ?>/js/jquery.printElement.min.js"></script>
<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/print-store-card.css" />
<script type="text/javascript">
	$(document).ready(function(){
            $(".button_print").click(function(){
                    $('#printElement').printElement({ overrideElementCSS: ['<?php echo Yii::app()->theme->baseUrl;?>/css/print-store-card.css'] });
            });
	});
</script>


<script>
$(document).ready(function(){
//    var availableMaterials = <?php echo MyFunctionCustom::getMaterialsJson(); ?>;
//    $( "#GasStoreCard_materials_name" ).autocomplete({
//        source: availableMaterials,
//        close: function( event, ui ) { },
//        select: function( event, ui ) {
//            $('#GasStoreCard_ext_materials_id').val(ui.item.id);
//            $('#GasStoreCard_materials_name').val(ui.item.name);
//        }
//    });
    
    
    fnCheckbox_view_general();
    fnBindEventSubmitClick('<?php echo BLOCK_UI_COLOR;?>');
});


function fnCheckbox_view_general(){
//    $('.checkbox_view_general').live('click',function(){
        if($('.checkbox_view_general').is(':checked')){
            $('#GasStoreCard_ext_materials_id').val('');
            $('#GasStoreCard_materials_name').val('');
            $('#GasStoreCard_materials_name').hide();
            $('.row_materials_name').hide();
        }else{
            $('#GasStoreCard_materials_name').show();
            $('.row_materials_name').show();
        }
//    });
    
}

</script>    
