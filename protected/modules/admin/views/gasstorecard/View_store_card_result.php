
    <?php if(isset($data['aSumEachType']) && count($data['aSumEachType'])>0): ?>
    <?php
        $OpeningStock = $data['OpeningStock'];
        $aSumEachType = $data['aSumEachType'];
        $mMaterial = $data['mMaterial'];
        $mMaterialParent = $data['mMaterialParent'];
        $material_name_store_card = $mMaterial->name_store_card;
        if(empty($material_name_store_card)){
            $material_name_store_card = $mMaterialParent->name_store_card;
        }            
        $totalImport = 0;
        $totalExport = 0;
    ?>
    

<div class="container" id="printElement">
    <table cellpadding="0" cellspacing="0">
        <tr>
            <td>
                <div class="logo">
                    <img src="<?php echo Yii::app()->theme->baseUrl; ?>/images/logo80x80.png">
                </div>            
                <p style="margin: 5px 0 20px 0;">CÔNG TY TNHH HƯỚNG MINH</p>
            </td>
            <td valign="top">
                <h2 style="margin: 35px 0 0">THẺ KHO <?php echo $material_name_store_card;?></h2>
            </td>
        </tr>
        <tr>
            <td class="agent_info"><b>Tên đại Lý</b>: <?php echo $model->agent?$model->agent->first_name:''?></td>
            <td style="text-align: right;"><b>Ngày lập thẻ</b>: <?php echo str_replace('-', '/', $model->date_delivery) ?></td>
        </tr>
        <tr>
            <td class="agent_info" colspan="2"><b>Tên Vật Tư</b>:  <?php echo $mMaterial->name;?>   -  Tồn đầu kỳ: <?php echo $OpeningStock;?></td>
        </tr>
    </table>
    <table cellpadding="0" cellspacing="0" class="tb hm_table">
        <thead>
            <tr>
                <th colspan="5"><?php echo $material_name_store_card;?></th>
            </tr>
            <tr>
                <th rowspan="2">Lí do nhập - xuất</th>
                <th colspan="3">Số lượng</th>
                <th rowspan="2">Người kiểm (ký-tên)</th>
            </tr>
            <tr>
                <?php foreach(CmsFormatter::$STORE_CARD_TYPE as $id_type=>$name):?>
                    <th><?php echo $name;?></th>
                <?php endforeach;?>
                <!-- <th>Xuất bán</th>-->
                <th>Tồn</th>
            </tr>
            
        </thead>
        <tbody>
            <?php foreach(CmsFormatter::$STORE_CARD_TYPE as $id_type=>$name1):?>
                <?php foreach(CmsFormatter::$STORE_CARD_ALL_TYPE as $id_all_type=>$name):?>
                    <?php if(isset($aSumEachType[$id_all_type][$id_type])): ?>
                    <tr>
                        <td><?php echo $name;?></td>
                        <?php foreach(CmsFormatter::$STORE_CARD_TYPE as $id_type2=>$name2):?>
                            <?php $qty = isset($aSumEachType[$id_all_type][$id_type2])?$aSumEachType[$id_all_type][$id_type2]:''; 
                                if(!empty($qty)){
                                    if($id_type2==TYPE_STORE_CARD_IMPORT){ // nhập kho
                                        $OpeningStock +=$qty;
                                        $totalImport+=$qty;
                                    }
                                    else{ // xuất kho
                                        $OpeningStock -=$qty;
                                        $totalExport +=$qty;
                                    }
                                }
                            ?>
                            <td class="item_c"><?php echo ActiveRecord::formatCurrency($qty);?></td>
                        <?php endforeach;?>
                        <td class="item_c"><?php echo ActiveRecord::formatCurrency($OpeningStock);?></td>
                        <td></td>
                    </tr>
                    <?php endif;?>
                <?php endforeach;?>
            <?php endforeach;?>
        </tbody>
        <tfoot>
            <tr>
            	<td>Tổng cộng:</td>
                <td class="item_c"><?php echo ActiveRecord::formatCurrency($totalImport);?></td>
                <td class="item_c"><?php echo ActiveRecord::formatCurrency($totalExport);?></td>
                <td class="item_c"><?php echo ActiveRecord::formatCurrency($OpeningStock);?></td>
            	<td>&nbsp;</td>
            </tr>
        </tfoot>
        
        
    </table>
</div>

<?php else: ?>
<div class="container" id="printElement">
    <h2>Không có dữ liệu</h2>
</div>
<?php endif; // end  if(isset($data['aSumEachType'] ?>

