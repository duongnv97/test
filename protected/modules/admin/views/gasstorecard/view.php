<?php
$this->breadcrumbs=array(
	'Thẻ Kho'=>array('index'),
	$model->store_card_no,
);

$menus = array(
	array('label'=>'GasStoreCard Management', 'url'=>array('index')),
//	array('label'=>'Tạo Mới GasStoreCard', 'url'=>array('create')),
	array('label'=>'Cập Nhật GasStoreCard', 'url'=>array('update', 'id'=>$model->id)),
//	array('label'=>'Xóa GasStoreCard', 'url'=>array('delete'), 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Bạn chắc chắn muốn xóa ?')),
);
$this->menu= ControllerActionsName::createMenusRoles($menus, $actions);
$msg = 'xuất kho: ';
if($model->type_store_card == TYPE_STORE_CARD_IMPORT)
        $msg = 'nhập kho: ';
?>

<h1>Xem <?php echo $msg;?>: <?php echo $model->store_card_no; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'store_card_no',
                'store_card_real',
                array(
                    'name'=>'customer_id',
                    'type'=>'NameCustomerStoreCard',
                    'value'=>$model,
                    'visible'=>MyFormat::getAgentGender()!=Users::IS_WAREHOUSE_IN_OUT,
                ),  
                array(
                    'name'=>'type_store_card',
                    'value'=>CmsFormatter::$STORE_CARD_TYPE[$model->type_store_card],
                ),  
                array(
                    'name'=>'type_in_out',
                    'value'=>  $model->getStoreCardTypeInOut(),
                ),  
		array(                
                    'name' => 'delivery_person',
                    'type'=>'html',
                    'value'=>  $model->getDeliveryPerson(),
                ),
		'date_delivery:date',
            
//            Aug 26, 2014 close
//                array(
//                    'name'=>'receivables_customer',
//                    'type'=>'Currency',
//                    'visible'=>MyFormat::getAgentGender()!=Users::IS_WAREHOUSE_IN_OUT,
//                ),            
//                array(
//                    'name'=>'collection_customer',
//                    'type'=>'Currency',
//                    'visible'=>MyFormat::getAgentGender()!=Users::IS_WAREHOUSE_IN_OUT,
//                ),            
//                array(
//                    'name'=>'collection_customer_date',
//                    'type'=>'datetime',
//                    'visible'=>MyFormat::getAgentGender()!=Users::IS_WAREHOUSE_IN_OUT,
//                ),
//                array(
//                    'name'=>'collection_customer_date_update',
//                    'type'=>'datetime',
//                    'visible'=>MyFormat::getAgentGender()!=Users::IS_WAREHOUSE_IN_OUT,
//                ),            
//                array(
//                    'name'=>'collection_customer_note',                    
//                ),
            
                array(
                    'name' => 'user_id_create',
                    'value' => $model->user_create?$model->user_create->first_name:"",
                    'visible'=>Yii::app()->user->role_id!=ROLE_SUB_USER_AGENT,                    
                ),
                array(
                    'name' => 'uid_login',
                    'value'=> $model->getUidLogin(),
                ),
                array(
                    'name' => 'last_update_by',
                    'value'=> $model->getLastUpdate(),
                ),
                array(
                    'name' => 'last_update_time',
                    'type'=>'datetime',
                ),
                array(
                    'name' => 'value_before_update',
                    'type'=>'html',
                    'visible'=>Yii::app()->user->role_id==ROLE_ADMIN,
                ),             
                array(
                    'name' => 'note',
                    'type'=>'StorecardNote',
                    'value'=> $model,
                ),             
		'created_date:datetime',
	),
)); ?>

<h1>Chi Tiết <?php echo $msg;?></h1>

<?php 
$mGasDebts  = GasDebts::model()->findByAttributes(['relate_id'=>$model->id]);
$aDetail    = isset($mGasDebts->json) ? json_decode($mGasDebts->json, true) : '';
$no         = 1;
$sum_qty    = 0;
$sum_amount = 0;
?>
<!--DuongNV Jul0519 table co don gia, thanh tien-->
<?php if(!empty($aDetail)): ?>
<div class="row">
    <label>&nbsp</label>
    <table class="materials_table hm_table">
        <thead>
            <tr>
                <th class="order_no item_c">#</th>
                <th class="item_code item_c">Mã</th>
                <th class="item_name item_c ">Tên</th>
                <th class="item_qty item_c ">SL</th>
                <th class=" item_r">Đơn Giá</th>
                <th class=" item_r last">Thành Tiền</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach($aDetail as $item):?>
            <?php 
                $sum_qty    += $item['qty'];
                $sum_amount += $item['amount'];
            ?>
            <tr class="materials_row">
                <td class=" item_c"><?php echo ($no++);?></td>
                <td><?php echo $item['materials_no']; ?></td>
                <td class="item_l"><?php echo $item['materials_name']; ?></td>
                <td class="item_c"><?php echo ActiveRecord::formatCurrency($item['qty']); ?></td>
                <td class="item_r"><?php echo ActiveRecord::formatCurrency($item['price']); ?></td>
                <td class="last item_b item_r"><?php echo ActiveRecord::formatCurrency($item['amount']);?></td>
            </tr>                    
            <?php endforeach;?>
        </tbody>
        <tfoot>
            <tr class="materials_row">
                <td class="item_b item_r f_size_16" colspan="3">Tổng Cộng</td>
                <td class="item_b item_c total_qty f_size_16"><?php echo ActiveRecord::formatCurrency($sum_qty);?></td>
                <td></td>
                <td class="item_b item_r total_amount f_size_16"><?php echo ActiveRecord::formatCurrency($sum_amount);?></td>
                <td class="last"></td>
            </tr>
        </tfoot>
    </table>
</div>
<?php else: ?>
    <?php 
    $sum_qty = $key = 0;
    $model->aModelStoreCardDetail = GasStoreCardDetail::getByStoreCardId($model->id);
    $aMaterial = CacheSession::getArrayModelMaterial();
    $str = ''; $sum_sl  = 0; $aSum = [];
    $aDetail = $model->aModelStoreCardDetail;
    foreach($aDetail as $item){
        if(!isset($aSum[$item->materials_id])){
            $aSum[$item->materials_id] = $item->qty;
        }else{
            $aSum[$item->materials_id] += $item->qty;
        }
    }

    ?>

    <!--DuongNV Jul0519 table chua co don gia, thanh tien-->
    <div class="row">
        <label>&nbsp</label>
        <table class="materials_table hm_table">
            <thead>
                <tr>
                    <th class="order_no item_c">#</th>
                    <th class="item_code item_c">Mã</th>
                    <th class="item_name item_c ">Tên</th>
                    <th class="item_unit item_c">ĐVT</th>
                    <th class="item_qty item_c ">SL</th>
                    <th class=" item_r  display_none">Đơn Giá</th>
                    <th class=" item_r last display_none">Thành Tiền</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach($aSum as $materials_id => $qty):?>
                <?php 
                    $nameMaterial = $materials_no = $unit = '';
                    if(isset($aMaterial[$materials_id])){
                        $nameMaterial   = $aMaterial[$materials_id]['name'];
                        $materials_no   = $aMaterial[$materials_id]['materials_no'];
                        $unit           = $aMaterial[$materials_id]['unit'];
                    }
                    $sum_qty+=$qty;

                ?>
                <tr class="materials_row">
                    <td class=" item_c"><?php echo ($key+1);?></td>
                    <td><?php echo $materials_no;?></td>
                    <td class="item_l"><?php echo $nameMaterial;?></td>
                    <td class="item_c"><?php echo $unit;?></td>
                    <td class=" item_c"><?php echo ActiveRecord::formatCurrency($qty);?></td>
                    <td class=" item_r display_none"><?php echo ActiveRecord::formatCurrency($item->price);?></td>
                    <td class="last item_b item_r display_none"><?php echo ActiveRecord::formatCurrency($item->amount);?></td>
                </tr>                    
                <?php endforeach;?>
            </tbody>
            <tfoot>
                <tr class="materials_row">
                    <td class="item_b item_r f_size_16" colspan="4">Tổng Cộng</td>
                    <td class="item_b item_c total_qty f_size_16"><?php echo ActiveRecord::formatCurrency($sum_qty);?></td>
                    <td class=" display_none"></td>
                    <td class="item_b item_r total_amount f_size_16 display_none"><?php echo ActiveRecord::formatCurrency($model->receivables_customer);?></td>
                    <td class="last"></td>
                </tr>
            </tfoot>
        </table>
    </div>
<?php endif; ?>