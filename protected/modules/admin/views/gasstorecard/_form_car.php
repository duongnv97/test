<div class="NotMakeOrderHide">
<?php // if(1): ?>
<?php if( in_array(MyFormat::getAgentId(), GasCheck::$arrAgentInputCar) || $cRole == ROLE_ADMIN || in_array($cUid, GasConst::getArrUidUpdateAll()) ): ?>
<?php 
    $needMore = array('status'=>1);
    if($cRole == ROLE_ADMIN || in_array($cUid, GasConst::getArrUidUpdateAll())){
        $needMore = array('status'=>1, 'get_all'=>1);
    }
    $aCar       = Users::getSelectByRoleForAgent(MyFormat::getAgentId(), ONE_AGENT_CAR, '', $needMore);
    $aDriver    = Users::getSelectByRoleForAgent(MyFormat::getAgentId(), ONE_AGENT_DRIVER, '', $needMore);
    $aPhuXe     = Users::getSelectByRoleForAgent(MyFormat::getAgentId(), ONE_AGENT_PHU_XE, '', $needMore);
?>
<div class="row">
    <div class="row radio-list-2" style=''>    
        <?php echo $form->labelEx($model,'road_route'); ?>
        <?php echo $form->radioButtonList($model,'road_route', $model->getArrRoadRoute(), 
                    array('separator'=>"",'class'=>'' )); ?>            
    </div>
</div>
<div class="clr"><br></div>

<div class="row more_col">
    <div class="col1 ">
        <?php echo $form->labelEx($model,'car_id'); ?>
        <?php echo $form->dropDownList($model,'car_id', $aCar,array('class'=>'w-300 float_l','empty'=>'Select')); ?>
        <?php echo $form->error($model,'car_id'); ?>
    </div>
</div>

<div class="row more_col">
    <div class="col1 ">
        <?php echo $form->labelEx($model,'driver_id_1'); ?>
        <?php echo $form->dropDownList($model,'driver_id_1', $aDriver,array('class'=>'w-300 float_l','empty'=>'Select')); ?>
        <?php echo $form->error($model,'driver_id_1'); ?>
    </div>
    <div class="col3 l_padding_50">
        <?php echo $form->labelEx($model,'driver_id_2'); ?>
        <?php echo $form->dropDownList($model,'driver_id_2', $aDriver,array('class'=>'w-300 float_l','empty'=>'Select')); ?>
        <?php echo $form->error($model,'driver_id_2'); ?>
    </div>
</div>

<div class="row more_col">
    <div class="col1 ">
        <?php echo $form->labelEx($model,'phu_xe_1'); ?>
        <?php echo $form->dropDownList($model,'phu_xe_1', $aPhuXe,array('class'=>'w-300 float_l','empty'=>'Select')); ?>
        <?php echo $form->error($model,'phu_xe_1'); ?>
    </div>
    <div class="col3 l_padding_50">
        <?php echo $form->labelEx($model,'phu_xe_2'); ?>
        <?php echo $form->dropDownList($model,'phu_xe_2', $aPhuXe,array('class'=>'w-300 float_l','empty'=>'Select')); ?>
        <?php echo $form->error($model,'phu_xe_2'); ?>
    </div>
</div>

<?php endif; ?>
</div>