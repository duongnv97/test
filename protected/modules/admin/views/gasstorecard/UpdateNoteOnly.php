<?php
$cmsFormater = new CmsFormatter();
?>

<h1>Cập nhật ghi chú: Mã Thẻ Kho <?php echo $model->store_card_no;?><br> <?php echo $cmsFormater->formatNameCustomerStoreCard($model);?>
    Ngày <?php echo $cmsFormater->formatDate($model->date_delivery);?>
</h1>
<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'gas-storehouse-form',
	'enableAjaxValidation'=>false,
)); ?> 
    
    <div class="row">
        <!--<p class="hight_light item_b">Chỉ cập nhật khi đã thực tế thu tiền khách hàng, khách hàng nợ sẽ không nhập ở đây.</p>-->
    </div>
    
    <div class="clr"></div>
    <div class="row">
        <?php echo Yii::t('translation', $form->labelEx($model,'update_note')); ?>
        <?php echo $form->textArea($model,'update_note',array('style'=>'width:500px','rows'=>'10')); ?>
        <?php echo $form->error($model,'update_note'); ?>
    </div>

    <div class="row buttons" style="padding-left: 141px;">
        <?php $this->widget('bootstrap.widgets.TbButton', array(
        'buttonType'=>'submit',
        'label'=>$model->isNewRecord ? Yii::t('translation', 'Create') : Yii::t('translation', 'Save'),
        'type'=>'null', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
        'size'=>'small', // null, 'large', 'small' or 'mini'
        //'htmlOptions' => array('style' => 'margin-bottom: 10px; float: right;'),
    )); ?>	
        <input class='cancel_iframe' type='button' value='Cancel'>
    </div>

<?php $this->endWidget(); ?>

</div><!-- form -->
<script>
    $(function(){
        $('#GasStoreCard_collection_customer').trigger('change');
        $('.receivables_full').click(function(){
            if($(this).is(':checked')){
                $('.collection_customer').val($('.receivables_customer').text());
            }else{
                $('.collection_customer').val('');
            }
            $('#GasStoreCard_collection_customer').trigger('change');
        });
    });
</script>