<div class="form">
    <?php
        $LinkNormal = Yii::app()->createAbsoluteUrl('admin/gasstorecard/view_store_movement_summary');
        $LinkNew    = Yii::app()->createAbsoluteUrl('admin/gasstorecard/view_store_movement_summary', array('version'=> 1));
        $LinkExcel    = Yii::app()->createAbsoluteUrl('admin/gasstorecard/view_store_movement_summary', array('excel'=> 1));
        $cUid = MyFormat::getCurrentUid();
        $aUidAllow = [GasConst::UID_CHIEN_BQ, GasConst::UID_ADMIN, GasConst::UID_NGAN_DTM, GasLeave::PHUONG_PTK];
    ?>
    <h1>Xem và in ấn nhập xuất tồn
        <a class='btn_cancel f_size_14 <?php echo (!isset($_GET['version'])) && (!isset($_GET['excel'])) ? "active":"";?>' href="<?php echo $LinkNormal;?>">Normal</a>
        <a class='btn_cancel f_size_14 <?php echo isset($_GET['version']) && $_GET['version']==1 ? "active":"";?>' href="<?php echo $LinkNew;?>">Loại nhập xuất</a>
        <?php if(in_array($cUid, $aUidAllow)): ?>
            <a class='btn_cancel f_size_14 <?php echo isset($_GET['excel']) && $_GET['excel']==1 ? "active":"";?>' href="<?php echo $LinkExcel;?>">Xuất Excel hệ thống</a>
        <?php endif; ?>
    </h1> 
</div>