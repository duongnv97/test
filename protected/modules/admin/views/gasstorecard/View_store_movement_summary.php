<?php
$this->breadcrumbs=array(
	'Xem và in ấn nhập xuất tồn',
);
$url = Yii::app()->createAbsoluteUrl('admin/gasstorecard/view_store_movement_summary');
if(isset($_GET['version'])){
    $url = Yii::app()->createAbsoluteUrl('admin/gasstorecard/view_store_movement_summary', ['version'=>1]);
}
$urlExcel = Yii::app()->createAbsoluteUrl('admin/gasstorecard/view_store_movement_summary', ['ExportExcel'=>1]);
$urlExcelAll = Yii::app()->createAbsoluteUrl('admin/gasstorecard/view_store_movement_summary', ['ToExcelAll'=>1]);
$aUidExcelAll = [GasConst::UID_ADMIN, GasConst::UID_NGAN_DTM, GasLeave::PHUONG_PTK];
$cUid = MyFormat::getCurrentUid();
?>

<?php include 'index_button_inventory.php'; ?>

<?php 
if(isset($_GET['excel']) && $_GET['excel']==1): // to export excel system 
    include 'export_excel_system.php';
else:
?>
<div class="search-form" style="">
    <div class="wide form">

    <?php $form=$this->beginWidget('CActiveForm', array(
            'action'=> $url,
            'method'=> 'post',
    )); ?>
        <?php if(Yii::app()->user->hasFlash('successUpdate')): ?>
            <div class="success_div"><?php echo Yii::app()->user->getFlash('successUpdate');?></div>         
        <?php endif; ?>    

        <div class="row">
                <?php echo $form->label($model,'date_from') ?>		
                <?php 
                    $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                        'model'=>$model,        
                        'attribute'=>'date_from',
                        'options'=>array(
                            'showAnim'=>'fold',
                            'dateFormat'=> 'dd-mm-yy',
                            'maxDate'=> '0',
                            'changeMonth' => true,
                            'changeYear' => true,
                            'showOn' => 'button',
                            'buttonImage'=> Yii::app()->theme->baseUrl.'/admin/images/icon_calendar_r.gif',
                            'buttonImageOnly'=> true,                                
                        ),        
                        'htmlOptions'=>array(
                            'class'=>'w-16',
                            'style'=>'height:20px;float:left;',
                                'readonly'=>'readonly',
                        ),
                    ));
                ?>            

                <?php echo $form->label($model,'date_to') ?>		
                <?php 
                    $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                        'model'=>$model,        
                        'attribute'=>'date_to',
                        'options'=>array(
                            'showAnim'=>'fold',
                            'dateFormat'=> 'dd-mm-yy',
                            'maxDate'=> '0',
                            'changeMonth' => true,
                            'changeYear' => true,
                            'showOn' => 'button',
                            'buttonImage'=> Yii::app()->theme->baseUrl.'/admin/images/icon_calendar_r.gif',
                            'buttonImageOnly'=> true,                                
                        ),        
                        'htmlOptions'=>array(
                            'class'=>'w-16',
                            'style'=>'height:20px;float:left;',
                                'readonly'=>'readonly',
                        ),
                    ));
                ?>    
        </div>

        <?php if(isset($_GET['version'])): ?>
        <div class="row more_col">
            <div class="col1 ">
                <?php echo $form->label($model,'type_in_out',array()); ?>
                <?php // echo $form->dropDownList($model,'type_in_out', CmsFormatter::$STORE_CARD_ALL_TYPE,array('style'=>'', 'class'=>'multiselect', 'multiple'=>'multiple')); ?>
                <div class="fix-label float_l">
                    <?php
                       $this->widget('ext.multiselect.JMultiSelect',array(
                             'model'=>$model,
                             'attribute'=>'type_in_out',
                             'data' => CmsFormatter::$STORE_CARD_ALL_TYPE,
                             // additional javascript options for the MultiSelect plugin
                             'options'=>array('selectedList' => 15,),
                             // additional style
                            'htmlOptions'=>array('class' => 'w-400'),
                       ));    
                   ?>
                </div>                     
            </div>
        </div>
        <div class="clr"></div>
        <div class="row">
            <?php echo $form->label($model,'province_id_agent', ['label'=>'Tỉnh']); ?>
            <div class="fix-label">
                <?php
                   $this->widget('ext.multiselect.JMultiSelect',array(
                         'model'=>$model,
                         'attribute'=>'province_id_agent',
                         'data'=> GasProvince::getArrAll(),
                         // additional javascript options for the MultiSelect plugin
                        'options'=>array('selectedList' => 30,),
                         // additional style
                         'htmlOptions'=>array('class' => 'w-400'),
                   ));    
               ?>
            </div>
        </div>
        <?php endif; ?>

        <div class="row">
            <?php if(Yii::app()->user->role_id!=ROLE_SUB_USER_AGENT):?>    
            <?php // echo Yii::t('translation', $form->labelEx($model,'agent_id')); ?>
            <?php // echo $form->dropDownList($model,'agent_id', Users::getSelectByRole(),array('style'=>'','empty'=>'Select')); ?>

            <div class="row">
            <?php echo $form->labelEx($model,'agent_id'); ?>
            <?php echo $form->hiddenField($model,'agent_id', array('class'=>'')); ?>
            <?php
                    // 1. limit search kh của sale
//                    $url = Yii::app()->createAbsoluteUrl('admin/ajax/search_by_role', array('role'=> ROLE_AGENT));
                    $url = Yii::app()->createAbsoluteUrl('admin/ajax/search_dropdown_agent');
                    // widget auto complete search user customer and supplier
                    $aData = array(
                        'model'=>$model,
                        'field_customer_id'=>'agent_id',
                        'url'=> $url,
                        'name_relation_user'=>'agent',
                        'ClassAdd' => 'w-400',
                        'field_autocomplete_name' => 'autocomplete_name',
                        'placeholder'=>'Nhập mã hoặc tên đại lý',
                    );
                    $this->widget('ext.SpaAutocompleteCustomer.SpaAutocompleteCustomer',
                        array('data'=>$aData));
                    ?>
                <?php echo $form->error($model,'agent_id'); ?>
            </div>

            <?php endif;?>    
            <div class="sprint">
                <a class="button_print" href="javascript:void(0);">
                    <img src="<?php echo Yii::app()->theme->baseUrl; ?>/images/print.png">
                </a>
            </div>
        </div>

        <div class="row buttons" style="padding-left: 159px; margin:20px 0;">
            <?php $this->widget('bootstrap.widgets.TbButton', array(
                'buttonType'=>'submit',
                'label'=>Yii::t('translation','Xem Thống Kê'),
                'type'=>'null', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
                'size'=>'small', // null, 'large', 'small' or 'mini'
                //'htmlOptions' => array('style' => 'margin-bottom: 10px; float: right;'),
            )); ?>	
            &nbsp;&nbsp;&nbsp;&nbsp;<a class='btn_cancel' href='<?php echo $urlExcel;?>'>Xuất Excel</a>
            <?php if(in_array($cUid, $aUidExcelAll)): ?>
                &nbsp;&nbsp;&nbsp;&nbsp;<a class='btn_cancel' target="_blank" href='<?php echo $urlExcelAll;?>'>Xuất Excel hệ thống</a>
            <?php endif; ?>
        </div>

    <?php $this->endWidget(); ?>

    </div><!-- wide form -->
</div><!-- search-form -->

<?php // include_once 'View_store_movement_summary_print.php';?>
<?php include_once 'View_store_movement_fix_query.php';?>

<script  src="<?php echo Yii::app()->theme->baseUrl; ?>/js/jquery.printElement.min.js"></script>
<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/print-store-card.css" />
<script type="text/javascript">
	$(document).ready(function(){
            $(".button_print").click(function(){
                    $('#printElement').printElement({ overrideElementCSS: ['<?php echo Yii::app()->theme->baseUrl;?>/css/print-store-card.css'] });
            });
	});
</script>

<style>
    .ui-datepicker-trigger { float: left;}
</style>

<script>
$(document).ready(function(){
    $('.form').find('button:submit').click(function(){        
        var msg = isValidSubmit();
        if( msg!='' )
        {
            showMsgWarning(msg); return false;
        }
        $.blockUI({ overlayCSS: { backgroundColor: '#fff' } }); 
    });   

});

    function isValidSubmit(){
        var msg = '';
        if($('#GasStoreCard_date_from').val()=='' || $('#GasStoreCard_date_to').val()==''){
            msg = 'Chưa chọn khoảng ngày thống kê';            
        }
        return msg;
    }
</script>    
<?php
endif; // end excel to export excel system 
?>