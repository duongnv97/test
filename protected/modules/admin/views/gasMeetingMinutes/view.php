<?php
$this->breadcrumbs=array(
	'Biên Bản Họp'=>array('index'),
	$model->date_published,
);

$menus = array(
	array('label'=>'Biên Bản Họp', 'url'=>array('index')),
	array('label'=>'Tạo Mới Biên Bản Họp', 'url'=>array('create')),
	array('label'=>'Cập Nhật Biên Bản Họp', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Xóa Biên Bản Họp', 'url'=>array('delete'), 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Bạn chắc chắn muốn xóa ?')),
);
$this->menu= ControllerActionsName::createMenusRoles($menus, $actions);
?>

<h1>Xem Biên Bản Họp Ngày: <?php echo $model->date_published; ?></h1>

