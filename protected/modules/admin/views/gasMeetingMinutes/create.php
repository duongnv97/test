<?php
$this->breadcrumbs=array(
	'Biên Bản Họp'=>array('index'),
	'Tạo Mới',
);

$menus = array(		
        array('label'=>'Biên Bản Họp', 'url'=>array('index')),
);
$this->menu= ControllerActionsName::createMenusRoles($menus, $actions);
?>

<h1>Tạo Mới Biên Bản Họp</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>