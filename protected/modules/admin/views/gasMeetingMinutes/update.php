<?php
$this->breadcrumbs=array(
	'Biên Bản Họp'=>array('index'),
	$model->date_published=>array('view','id'=>$model->id),
	'Cập Nhật',
);

$menus = array(	
        array('label'=>'Biên Bản Họp', 'url'=>array('index')),
	array('label'=>'Xem Biên Bản Họp', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Tạo Mới Biên Bản Họp', 'url'=>array('create')),	
);
$this->menu= ControllerActionsName::createMenusRoles($menus, $actions);

?>

<h1>Cập Nhật Biên Bản Họp Ngày: <?php echo $model->date_published; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>