<?php
$this->breadcrumbs=array(
    $this->pluralTitle => array('index'),
    ' ' . $this->singleTitle . ' : ',
);

$menus = array(
	array('label'=>"$this->pluralTitle Management", 'url'=>array('index')),
	array('label'=>"Tạo Mới $this->singleTitle", 'url'=>array('create')),
	array('label'=>"Cập Nhật $this->singleTitle", 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>"Xóa $this->singleTitle", 'url'=>array('delete'), 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Bạn chắc chắn muốn xóa ?')),
);
$this->menu= ControllerActionsName::createMenusRoles($menus, $actions);
?>

<h1>Xem Danh Sách Email</h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		array(
                    'name'=>'type',
                    'type'=>'raw',
                    'value'=>$model->getType(),
                ),
                array(
                    'name'=>'status',
                    'type'=>'raw',
                    'value'=>$model->getStatus(),
                ),
                array(
                        'name'=>'user_id',
                        'type'=>'raw',
                        'value'=>$model->getNameUser(),
                    ),
                array(
                        'name'=>'name_file',
                        'type'=>'raw',
                        'value'=>$model->getNameFile(),
                    ),
                array(
                        'name'=>'list_email',
                        'type'=>'raw',
                        'value'=>$model->getTableListEmail(),
                    ),
                array(
                        'name'=>'created_date',
                        'type'=>'raw',
                        'value'=>MyFormat::dateConverYmdToDmy($model->created_date),
                       ),
	),
)); ?>
