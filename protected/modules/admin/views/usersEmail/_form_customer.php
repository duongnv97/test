<div class="row">
<?php echo $form->labelEx($model,'user_id'); ?>
<?php echo $form->hiddenField($model,'user_id', array('class'=>'')); ?>
<?php
    // 1. limit search kh của sale
    $url = Yii::app()->createAbsoluteUrl('admin/ajax/search_user_by_code');
    // widget auto complete search user customer and supplier
    $aData = array(
        'model'=>$model,
        'field_customer_id'=>'user_id',
        'url'=> $url,
        'name_relation_user'=>'rUser',
        'ClassAdd' => 'w-400',
        'field_autocomplete_name' => 'autocomplete_name1',
    );
    $this->widget('ext.SpaAutocompleteCustomer.SpaAutocompleteCustomer',
        array('data'=>$aData));
    ?>
    <?php echo $form->error($model,'user_id'); ?>
</div>

