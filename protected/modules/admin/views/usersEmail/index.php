<?php
$this->breadcrumbs=array(
    $this->pageTitle,
);

$menus=array(
            array('label'=>"Create $this->singleTitle", 'url'=>array('create')),
);
$this->menu= ControllerActionsName::createMenusRoles($menus, $actions);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('users-email-grid', {
                url : $(this).attr('action'),
		data: $(this).serialize()
	});
	return false;
});
");

Yii::app()->clientScript->registerScript('ajaxupdate', "
$('#users-email-grid a.ajaxupdate').live('click', function() {
    $.fn.yiiGridView.update('users-email-grid', {
        type: 'POST',
        url: $(this).attr('href'),
        success: function() {
            $.fn.yiiGridView.update('users-email-grid');
        }
    });
    return false;
});
");
?>

<h1><?php echo $this->pageTitle; ?></h1>


<?php echo CHtml::link('Tìm Kiếm Nâng Cao','#',array('class'=>'search-button')); ?>
<div class="search-form" >
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'users-email-grid',
	'dataProvider'=>$model->search(),
        'afterAjaxUpdate'=>'function(id, data){ fnUpdateColorbox();}',
        'template'=>'{pager}{summary}{items}{pager}{summary}',
//    'itemsCssClass' => 'items custom_here',
//    'htmlOptions'=> array('class'=>'grid-view custom_here'),
        'pager' => array(
            'maxButtonCount'=>  CmsFormatter::$PAGE_MAX_BUTTON,
        ),
	'enableSorting' => false,
	//'filter'=>$model,
	'columns'=>array(
        array(
            'header' => 'S/N',
            'type' => 'raw',
            'value' => '$this->grid->dataProvider->pagination->currentPage * $this->grid->dataProvider->pagination->pageSize + ($row+1)',
            'headerHtmlOptions' => array('width' => '10px','style' => 'text-align:center;'),
            'htmlOptions' => array('style' => 'text-align:center;')
        ),
		array(
                    'name' => 'type',
                    'type' => 'html',
                    'value' => '$data->getType()',
                    'htmlOptions' => array('style' => 'text-align:center;'),
                    ),
		
		array(
                    'name' => 'user_id',
                    'type' => 'html',
                    'value' => '$data->getCustomer()."<br>".$data->getCustomer("address")',
                    ),
		array(
                    'name' => 'list_email',
                    'type' => 'html',
                    'value' => '$data->getTableListEmail()',
                    'htmlOptions' => array('style' => 'text-align:center;'),
                    ),
		array(
                    'name' => 'name_file',
                    'type' => 'html',
                    'value' => '$data->getNameFile()',
                    ),
                array(
                    'name' => 'status',
                    'type' => 'html',
                    'value' => '$data->getStatus()',
                    'htmlOptions' => array('style' => 'text-align:center;'),
                    ),
		array(
                    'name' => 'created_date',
                    'type' => 'html',
                    'value' => '$data->getCreatedDate()',
                    'htmlOptions' => array('style' => 'text-align:center;'),
                ),
		array(
                    'header' => 'Actions',
                    'class'=>'CButtonColumn',
                    'template'=> ControllerActionsName::createIndexButtonRoles($actions),
                    'buttons'=>array(
                        'update' => array(
//                            'visible' => '$data->canUpdate()',
                        ),
                        'delete'=>array(
                            'visible'=> 'GasCheck::canDeleteData($data)',
                        ),
                    ),
		),
	),
)); ?>

<script>
$(document).ready(function() {
    fnUpdateColorbox();
});

function fnUpdateColorbox(){   
    fixTargetBlank();
}
</script>