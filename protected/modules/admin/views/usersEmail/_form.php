<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'users-email-form',
	'enableAjaxValidation'=>false,
        'htmlOptions' => array('enctype' => 'multipart/form-data'),
)); ?>
    <?php echo $form->errorSummary($model); ?>
    <p class="note">Dòng có dấu <span class="required">*</span> là bắt buộc.</p>
    <?php echo MyFormat::BindNotifyMsg(); ?>
    <div class="row buttons" style="padding-left: 141px;">
        <?php $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType'=>'submit',
            'label'=>$model->isNewRecord ? 'Create' :'Save',
            'type'=>'null', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
            'size'=>'small', // null, 'large', 'small' or 'mini'
            //'htmlOptions' => array('style' => 'margin-bottom: 10px; float: right;'),
        )); ?>	
    </div>

    <div class="row">
        <?php echo $form->labelEx($model,'type'); ?>
        <?php echo $form->dropDownList($model,'type', $model->getArrayType()); ?>
        <?php echo $form->error($model,'type'); ?>
    </div>
    <div class="row display_none">
        <?php echo $form->labelEx($model,'status'); ?>
        <?php echo $form->dropDownList($model,'status', $model->getArrayStatus()); ?>
        <?php echo $form->error($model,'status'); ?>
    </div>
    
    <?php if(!$model->isNewRecord): ?>
        <?php include '_form_customer.php'; ?>
    <?php endif; ?>
    <?php if($model->isNewRecord): ?>
        <?php include '_form_customer_multi.php'; ?>
    <?php endif; ?>
    
    
    <?php if(!$model->isNewRecord): ?>
    <label class="color_red">Mỗi email cách nhau bởi dấu ; ví dụ: email1@gmail.com;email2@gmail.com;email3@gmail.com</label>
    <div class="row">
        <?php echo $form->labelEx($model,'list_email'); ?>
        <?php echo $form->textField($model,'list_email',array('class'=>'w-800')); ?>
        <?php echo $form->error($model,'list_email'); ?>
    </div>
    
    <label class="color_red">Tên file là tiếng việt không dấu, viết liền ; ví dụ: KhachSanSheraton, NhaHangHoangTy .... </label>
    <div class="row">
        <?php echo $form->labelEx($model,'name_file'); ?>
        <?php echo $form->textField($model,'name_file',array('class'=>'w-400', 'maxlength'=>30)); ?>
        <?php echo $form->error($model,'name_file'); ?>
    </div>
    <?php endif; ?>
   
    <div class="row buttons" style="padding-left: 141px;">
        <?php $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType'=>'submit',
            'label'=>$model->isNewRecord ? 'Create' :'Save',
            'type'=>'null', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
            'size'=>'small', // null, 'large', 'small' or 'mini'
            //'htmlOptions' => array('style' => 'margin-bottom: 10px; float: right;'),
        )); ?>	
    </div>

<?php $this->endWidget(); ?>

</div><!-- form -->
<script>
    $(document).ready(function(){
        $('.form').find('button:submit').click(function(){
            $.blockUI({ overlayCSS: { backgroundColor: '#fff' } }); 
        });
        
    });
</script>