<div class="form">
    <?php 
    $model->cronNotifyBirthday();
    ?>
<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'users-email-form',
	'enableAjaxValidation'=>false,
        'htmlOptions' => array('enctype' => 'multipart/form-data'),
)); ?>

    <p class="note">Dòng có dấu <span class="required">*</span> là bắt buộc.</p>
    <?php echo MyFormat::BindNotifyMsg(); ?>
    <div class="row buttons" style="padding-left: 141px;">
        <?php $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType'=>'submit',
            'label'=>$model->isNewRecord ? 'Create' :'Save',
            'type'=>'null', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
            'size'=>'small', // null, 'large', 'small' or 'mini'
            //'htmlOptions' => array('style' => 'margin-bottom: 10px; float: right;'),
        )); ?>	
    </div>
    
  
    
    <div class="row">
        <?php echo $form->labelEx($model,'user_id'); ?>
        <?php echo $form->hiddenField($model,'user_id', array('class'=>'')); ?>
        <?php
            // 1. limit search kh của sale
            $url = Yii::app()->createAbsoluteUrl('admin/ajax/search_user_by_code');
            // widget auto complete search user customer and supplier
            $aData = array(
                'model'=>$model,
                'field_customer_id'=>'user_id',
                'url'=> $url,
                'name_relation_user'=>'rUser',
                'ClassAdd' => 'w-400',
                'field_autocomplete_name' => 'autocomplete_name1',
            );
            $this->widget('ext.SpaAutocompleteCustomer.SpaAutocompleteCustomer',
                array('data'=>$aData));
            ?>
            <?php echo $form->error($model,'user_id'); ?>
    </div>

    
    <div class="row display_none">
        <?php echo $form->labelEx($model,'status'); ?>
        <?php echo $form->dropDownList($model,'status', $model->getArrayStatus()); ?>
        <?php echo $form->error($model,'status'); ?>
    </div>
    
    <div class="row">
        <?php echo $form->labelEx($model,'list_email'); ?>
        <?php echo $form->textField($model,'list_email',array('class'=>'w-800')); ?>
        <?php echo $form->error($model,'list_email'); ?>
        <div class="color_red" style="margin-left: 141px;margin-bottom: 10px;">
            Mỗi email cách nhau bởi dấu ; ví dụ: email1@gmail.com;email2@gmail.com;email3@gmail.com
        </div>
    </div>
    
    <div class="row">
        <?php echo $form->labelEx($model,'name_file', array('label'=>'Số điện thoại')); ?>
        <?php echo $form->textField($model,'name_file',array('class'=>'w-800', 'maxlength'=>30)); ?>
        <?php echo $form->error($model,'name_file'); ?>
        <div class="color_red" style="margin-left: 141px;margin-bottom: 10px;">
            Mỗi số phone cách nhau bởi dấu - vd: 0123456789-0988180386
        </div>
    </div>
    
    <div class="row more_col">
        <div class="col1">
                <?php echo $form->labelEx($model,'dob1'); ?>
                <?php 
                    $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                        'model'=>$model,        
                        'attribute'=>'dob1',
                        'options'=>array(
                            'showAnim'=>'slide',
                            'dateFormat'=> ActiveRecord::getDateFormatJquery(),
                            'yearRange'=>'1950:2019',
//                            'minDate'=> '0',
//                            'maxDate'=> '1',
                            'changeMonth' => true,
                            'changeYear' => true,
                            'showOn' => 'button',
                            'buttonImage'=> Yii::app()->theme->baseUrl.'/admin/images/icon_calendar_r.gif',
                            'buttonImageOnly'=> true,                                
                        ),        
                        'htmlOptions'=>array(
                            'class'=>'w-16',
                            'style'=>'height:20px;',
//                                'readonly'=>'readonly',
                            
                        ),
                    ));
                ?>     		
                <?php echo $form->error($model,'dob1'); ?>
        </div>
            <!--<label>&nbsp;</label>-->
        <div class="col2">
                <?php echo $form->labelEx($model,'dob2'); ?>
                <?php 
                    $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                        'model'=>$model,        
                        'attribute'=>'dob2',
                        'options'=>array(
                            'showAnim'=>'slide',
                            'dateFormat'=> ActiveRecord::getDateFormatJquery(),
                            'yearRange'=>'1950:2019',
//                            'minDate'=> '0',
//                            'maxDate'=> '0',
                            'changeMonth' => true,
                            'changeYear' => true,
                            'showOn' => 'button',
                            'buttonImage'=> Yii::app()->theme->baseUrl.'/admin/images/icon_calendar_r.gif',
                            'buttonImageOnly'=> true,                                
                        ),        
                         'htmlOptions'=>array(
                            'class'=>'w-16',
                            'style'=>'height:20px;',
//                                'readonly'=>'readonly',
                        ),
                    ));
                ?>     
                 <?php echo $form->error($model,'dob2'); ?>
        </div>
    </div>
    
    <div class="row">
        <?php echo $form->labelEx($model,'note'); ?>
        <?php echo $form->textArea($model,'note',array('maxlength' => 300, 'rows' => 5, 'cols' => 50)); ?>
        <?php echo $form->error($model,'note'); ?>
    </div>
    
    <div class="row buttons" style="padding-left: 141px;">
        <?php $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType'=>'submit',
            'label'=>$model->isNewRecord ? 'Create' :'Save',
            'type'=>'null', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
            'size'=>'small', // null, 'large', 'small' or 'mini'
            //'htmlOptions' => array('style' => 'margin-bottom: 10px; float: right;'),
        )); ?>	
    </div>
</div>    

<?php $this->endWidget(); ?>

</div><!-- form -->
<script>
    $(document).ready(function(){
        $('.form').find('button:submit').click(function(){
            $.blockUI({ overlayCSS: { backgroundColor: '#fff' } }); 
        });
        
    });
</script>