<?php
$this->breadcrumbs=array(
	$this->pluralTitle=>array('dobList'),
	'Tạo Mới',
);

$menus = array(
    array('label'=>'Manage DobList', 
                'url'=>array('dobList'),
                'htmlOptions' => array('class' => 'index', 'label' => 'Quản lý')),
);
$this->menu= ControllerActionsName::createMenusRoles($menus, $actions);
?>

<h1>Tạo Mới Email</h1>

<?php echo $this->renderPartial('dob/_form', array('model'=>$model)); ?>