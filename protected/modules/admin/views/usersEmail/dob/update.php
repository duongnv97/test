<?php
$this->breadcrumbs = array(
	$this->pluralTitle => array('dobList'),
        $model->id=>array('view','id'=>$model->id),
	'Cập Nhật ' . $this->singleTitle,
);

$menus = array(	
                array('label'=>'Create Dob', 
                    'url'=>array('dobAdd'), 
                    'action' => 'create',
                    'htmlOptions' => array('class' => 'create', 'label' => 'Tạo mới')
                    ),
                array('label'=>'Manage DobList', 
                    'url'=>array('dobList'), 
                    'action' => 'index',
                    'htmlOptions' => array('class' => 'index', 'label' => 'Quản lý')
                    ),
);
$this->menu= ControllerActionsName::createMenusRoles($menus, $actions);

?>

<h1>Cập Nhật: <?php echo $this->singleTitle; ?></h1>

<?php echo $this->renderPartial('dob/_form', array('model'=>$model)); ?>