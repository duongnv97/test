<?php
$this->breadcrumbs=array(
	$this->singleTitleFileSend,
);

$menus=array(
//            array('label'=>"Create $this->singleTitle", 'url'=>array('create')),
);
$this->menu= ControllerActionsName::createMenusRoles($menus, $actions);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('file-send-grid', {
                url : $(this).attr('action'),
		data: $(this).serialize()
	});
	return false;
});
");

Yii::app()->clientScript->registerScript('ajaxupdate', "
$('#file-send-grid a.ajaxupdate').live('click', function() {
    $.fn.yiiGridView.update('file-send-grid', {
        type: 'POST',
        url: $(this).attr('href'),
        success: function() {
            $.fn.yiiGridView.update('file-send-grid');
        }
    });
    return false;
});
");
?>

<h1>Danh Sách <?php echo $this->pluralTitleFileSend; ?></h1>


<?php echo CHtml::link('Tìm Kiếm Nâng Cao','#',array('class'=>'search-button')); ?>
<div class="search-form" >
<?php $this->renderPartial('fileSend/_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'file-send-grid',
	'dataProvider'=>$model->search(),
        'afterAjaxUpdate'=>'function(id, data){ fnUpdateColorbox();}',
        'template'=>'{pager}{summary}{items}{pager}{summary}',
//    'itemsCssClass' => 'items custom_here',
//    'htmlOptions'=> array('class'=>'grid-view custom_here'),
        'pager' => array(
            'maxButtonCount'=>  CmsFormatter::$PAGE_MAX_BUTTON,
        ),
	'enableSorting' => false,
	//'filter'=>$model,
	'columns'=>array(
        array(
            'header' => 'S/N',
            'type' => 'raw',
            'value' => '$this->grid->dataProvider->pagination->currentPage * $this->grid->dataProvider->pagination->pageSize + ($row+1)',
            'headerHtmlOptions' => array('width' => '10px','style' => 'text-align:center;'),
            'htmlOptions' => array('style' => 'text-align:center;')
        ),
		array(
                    'name' => 'user_id',
                    'type' => 'html',
                    'value' => '$data->gerUserInfo()',
//                    'htmlOptions' => array('style' => 'text-align:center;'),
                    ),
		array(
                    'name' => 'type',
                    'type' => 'html',
                    'value' => '$data->getType()',
//                    'htmlOptions' => array('style' => 'text-align:center;'),
                    ),
                array(
                    'name' => 'list_email',
                    'type' => 'html',
                    'value' => '$data->getTableListEmail()',
//                    'htmlOptions' => array('style' => 'text-align:center;'),
                    ),
		array(
                    'name' => 'name_file',
                    'type' => 'raw',
                    'value' => '$data->getNameFile()',
                    'htmlOptions' => array('style' => 'text-align:center;'),
                    ),
		array(
                    'name' => 'created_date',
                    'type' => 'html',
                    'value' => '$data->getCreatedDate()',
                    'htmlOptions' => array('style' => 'text-align:center;'),
                    ),
		array(
                    'header' => 'Actions',
                    'class'=>'CButtonColumn',
                    'template'=> ControllerActionsName::createIndexButtonRoles($actions, array('FileSendView', 'FileSendDelete')).'&nbsp;&nbsp;{ExportPdf}',
                    'buttons'=>array(
                        'FileSendDelete'=>array(
                            'label'=>'Xoa',
                            'imageUrl'=>Yii::app()->theme->baseUrl . '/admin/images/icon/delete_index.png',
                            'options'=>array('class'=>'delete'),
                            'url'=>'Yii::app()->createAbsoluteUrl("admin/usersEmail/FileSendDelete",
                                array("id"=>$data->id) )',
                            'visible' => '1',

                        ),
                        'FileSendView'=>array(
                            'label'=>'Xem',
                            'imageUrl'=>Yii::app()->theme->baseUrl . '/admin/images/icon/view_icon.png',
                            'options'=>array('class'=>'view'),
                            'url'=>'Yii::app()->createAbsoluteUrl("admin/usersEmail/FileSendView",
                                array("id"=>$data->id) )',
                            'visible' => '1',
                        ),
                        'ExportPdf'=>array(
                            'label'=>'Xem pdf',
                            'imageUrl'=>Yii::app()->theme->baseUrl . '/new-icon/pdf.png',
                            'options'=>array('class'=>''),
                            'url'=>'$data->getUrl()',
                            'visible' => '1',
                        ),
                    ),
		),
	),
)); ?>

<script>
$(document).ready(function() {
    fnUpdateColorbox();
});

function fnUpdateColorbox(){   
    fixTargetBlank();
}
</script>