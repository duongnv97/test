<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=> GasCheck::getCurl(),
	'method'=>'get',
)); ?>

        <div class="row">
            <?php echo $form->labelEx($model, 'agent_id',['label'=>'Tỉnh']); ?>
            <div class="fix-label">
                <?php
                $this->widget('ext.multiselect.JMultiSelect', array(
                    'model' => $model,
                    'attribute' => 'agent_id',
                    'data' => GasProvince::getArrAll(),
                    // additional javascript options for the MultiSelect plugin
                    'options' => array('selectedList' => 30),
                    // additional style
                    'htmlOptions' => array('class' => 'w-400'),
                )); 
                ?>
            </div>
        </div>
    
	<div class="row ">
            <?php echo $form->labelEx($model,'employee_id'); ?>
            <?php echo $form->hiddenField($model,'employee_id_static'); ?>
            <?php
                // 1. limit search kh của sale
                $url = Yii::app()->createAbsoluteUrl('admin/ajax/search_user_login_not_agent');
                // widget auto complete search user customer and supplier
                $aData = array(
                    'model'=>$model,
                    'field_customer_id'=>'employee_id_static',
                    'url'=> $url,
                    'name_relation_user'=>'rEmployee',
                    'ClassAdd' => 'w-300',
                    'field_autocomplete_name' => 'autocomplete_employee_static', // tên biến truyền vào
                    'placeholder'=>'Nhập mã hoặc tên nhân viên',
                );
                $this->widget('ext.SpaAutocompleteCustomer.SpaAutocompleteCustomer',
                    array('data'=>$aData));
                ?>
            <?php echo $form->error($model,'agent_id'); ?>
        </div>

	<div class="row">
		<?php echo $form->labelEx($model,'object_type'); ?>
                <?php echo $form->dropDownList($model,'object_type', $model->getArrayType(),array('class'=>'w-300','empty'=>'Select')); ?>
	</div>
    
	<div class="row more_col">
        <div class="col1">
            <?php echo Yii::t('translation', $form->label($model,'date_from')); ?>
            <?php 
                $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                    'model'=>$model,        
                    'attribute'=>'date_from',
                    'options'=>array(
                        'showAnim'=>'fold',
                        'dateFormat'=> MyFormat::$dateFormatSearch,
//                            'minDate'=> '0',
//                        'maxDate'=> '0',
                        'changeMonth' => true,
                        'changeYear' => true,
                        'showOn' => 'button',
                        'buttonImage'=> Yii::app()->theme->baseUrl.'/admin/images/icon_calendar_r.gif',
                        'buttonImageOnly'=> true,                                
                    ),
                    'htmlOptions'=>array(
                        'class'=>'w-16 date_from',
                        'size'=>'16',
                        'class_update_val'=>'date_from',
                        'style'=>'float:left;', 
                        'id' =>'date_from_statistic'
                    ),
                ));
            ?>
        </div>
        <div class="col2">
            <?php echo Yii::t('translation', $form->label($model,'date_to')); ?>
            <?php 
                $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                    'model'=>$model,        
                    'attribute'=>'date_to',
                    'options'=>array(
                        'showAnim'=>'fold',
                        'dateFormat'=> MyFormat::$dateFormatSearch,
//                            'minDate'=> '0',
//                        'maxDate'=> '0',
                        'changeMonth' => true,
                        'changeYear' => true,
                        'showOn' => 'button',
                        'buttonImage'=> Yii::app()->theme->baseUrl.'/admin/images/icon_calendar_r.gif',
                        'buttonImageOnly'=> true,                                
                    ),
                    'htmlOptions'=>array(
                        'class'=>'w-16 date_to',
                        'size'=>'16',
                        'class_update_val'=>'date_to',
                        'style'=>'float:left;',
                        'id' => 'date_to_statistic'
                    ),
                ));
            ?>
        </div>
    </div>

    <div class="row buttons" style="padding-left: 159px;">
        <?php $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType'=>'submit',
            'label'=>'Search',
            'type'=>'null', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
            'size'=>'small', // null, 'large', 'small' or 'mini'
            //'htmlOptions' => array('style' => 'margin-bottom: 10px; float: right;'),
        )); ?>	
        <?php $this->widget('bootstrap.widgets.TbButton', array(
            'url'=> Yii::app()->createAbsoluteUrl("admin/EmployeeProblems/index/excel/1"),
            'label'=>'Xuất Excel',
            'type'=>'null', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
            'size'=>'small', // null, 'large', 'small' or 'mini'
            'visible'=>$model->canExportExcel(),
            'htmlOptions' => array('class' => 'btn_cancel'),
        )); ?>
    </div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->