<?php
$stt                = 1;
$aUid               = $data['aUid'];
$aResult            = $data['result'];
$aType              = $data['type'];
$aList              = $data['aList'];
$needMore['GetAll'] = 1;
$totalSUM           = 0;
$aModelUser         = Users::getArrObjectUserByRole('', $aUid, $needMore);
?>
<div class="grid-view">
    <table class="tb hm_table">
        <thead>
        <tr style="">
            <th class="w-50 ">STT</th>
            <th class="w-300 ">Nhân viên</th>
            <th class="w-150 ">Tổng tiền phạt</th>
            <?php foreach ($model->getArrayType() as $key => $value): ?>
                <th class="w-150 "><?php echo $value; ?></th>
            <?php endforeach; ?>
        </tr>
        </thead>
        <tbody>
            <?php foreach ($aList as $key => $value) : 
                $totalSUM+=$value;
                ?>
                <tr>
                    <td class="item_c"><?= $stt++ ?></td>
                    <td class="item_l">
                        <?php if(isset($aModelUser[$key])){
                                $mUser = $aModelUser[$key];
                                echo $mUser->first_name;
                            }else{
                                echo $key;
                            } 
                        ?>
                    </td>
                    <td class="item_b item_r"><?php echo ActiveRecord::formatCurrency($value);?></td>
                    <?php foreach ($model->getArrayType() as $id_type => $name): ?>
                    <td class="item_r ">
                        <?php if(isset($aResult[$key][$id_type])){
                                echo $aResult[$key][$id_type]['COUNT']." <br>";
                                echo "<b>". ActiveRecord::formatCurrency($aResult[$key][$id_type]['MONEY'])."</b>";
                        }
                        ?>
                    </td>
                    <?php endforeach; ?>
                </tr>
            <?php endforeach; ?>
            <tr>
                <td class="item_c"></td>
                <td class="item_l"></td>
                <td class="item_b item_r"><?php echo ActiveRecord::formatCurrency($totalSUM); ?></td>
                <?php foreach ($model->getArrayType() as $key => $value): ?>
                    <td class="item_r">
                        <?php 
                        if(isset($aType[$key])){
                            echo $aType[$key]['COUNT']." <br>";
                            echo "<b>". ActiveRecord::formatCurrency($aType[$key]['MONEY'])."</b>";
                        }
                        ?>
                    </td>
                <?php endforeach; ?>
            </tr>
        </tbody>
    </table>
</div>
