<?php
Yii::app()->clientScript->registerScript('search-statistic', "
$('.search-statistic-button').click(function(){
	$('.search-statistic-form').toggle();
	return false;
});
$('.search-statistic-form form').submit(function(){
	$.fn.yiiGridView.update('employee-problems-statistic-grid', {
                url : $(this).attr('action'),
		data: $(this).serialize()
	});
	return false;
});
");

Yii::app()->clientScript->registerScript('ajaxupdate-statistic', "
$('#employee-problems-statistic-grid a.ajaxupdate').live('click', function() {
    $.fn.yiiGridView.update('employee-problems-statistic-grid', {
        type: 'POST',
        url: $(this).attr('href'),
        success: function() {
            $.fn.yiiGridView.update('employee-problems-statistic-grid');
        }
    });
    return false;
});
");
?>
<?php // echo CHtml::link('Tìm Kiếm Nâng Cao','#',array('class'=>'search-statistic-button')); ?>
<div class="search-statistic-form" style="padding:10px;margin:10px 0;background:#eee;">
<?php $this->renderPartial('_search_statistic',array(
	'model'=>$model,
)); ?>
</div><!-- search-statistic-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'employee-problems-statistic-grid',
	'dataProvider'=>$model->searchStatistic(),
        'afterAjaxUpdate'=>'function(id, data){ fnUpdateColorbox();}',
        'template'=>'{pager}{summary}{items}{pager}{summary}',
//    'itemsCssClass' => 'items custom_here',
//    'htmlOptions'=> array('class'=>'grid-view custom_here'),
        'pager' => array(
            'maxButtonCount'=>  CmsFormatter::$PAGE_MAX_BUTTON,
        ),
	'enableSorting' => false,
	//'filter'=>$model,
	'columns'=>array(
        array(
            'header' => 'S/N',
            'type' => 'raw',
            'value' => '$this->grid->dataProvider->pagination->currentPage * $this->grid->dataProvider->pagination->pageSize + ($row+1)',
            'headerHtmlOptions' => array('width' => '10px','style' => 'text-align:center;'),
            'htmlOptions' => array('style' => 'text-align:center;')
        ),
		array(
                    'name'  => 'employee_id',
                    'value' => '$data->getEmployee()',
                ),
		array(
                    'header' => 'Chức Vụ',
                    'type'=>'RoleNameUser',                
                    'value'=>'$data->getRoleByUserId()',   
                ),
		array(
                    'name'  => 'money',
                    'value' => '$data->getMoney()',
                    'htmlOptions' => array('style' => 'text-align:right;')
                ),
	),
)); ?>