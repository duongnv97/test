<?php 

?>
<h1>Cập nhật hủy lỗi</h1>
<div class="form">
<?php
$form=$this->beginWidget('CActiveForm', array(
	'id'=>'gas-district-form',
	'enableAjaxValidation'=>false,
)); ?>
    <p class="note">Dòng có dấu <span class="required">*</span> là bắt buộc.</p>
    <?php echo MyFormat::BindNotifyMsg(); ?>
    <?php echo $form->errorSummary($model); ?>
    <div class="row">
        <label><?php echo $model->getAttributeLabel('employee_id');?></label>
        <p><?php echo "{$model->getEmployee()} - {$model->getObjectType()} - {$model->getCreatedDate("d/m/Y H:i:s")}"; ?></p>
    </div>
    <div class="row">
        <label><?php echo $model->getAttributeLabel('title');?></label>
        <p><?php echo "{$model->getField("title")}"; ?></p>
    </div>
    
    <div class="row">
        <?php echo $form->labelEx($model,'status'); ?>
        <?php echo $form->dropDownList($model,'status', $model->getArrayStatus(),array('class'=>'w-400')); ?>
        <?php echo $form->error($model,'status'); ?>
    </div>
    
    <div class="row">
        <?php echo $form->labelEx($model,'change_note') ?>
        <?php echo $form->textArea($model,'change_note',array('class'=>'w-400','rows'=>6)); ?>
        <?php echo $form->error($model,'change_note'); ?>
    </div>

    <?php // if($model->isNewRecord) $model->name='Phường '; ?>

    <div class="row buttons" style="padding-left: 141px;">
        <?php $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType'=>'submit',
            'label'=>$model->isNewRecord ? 'Create' :'Save',
            'type'=>'null', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
            'size'=>'small', // null, 'large', 'small' or 'mini'
            //'htmlOptions' => array('style' => 'margin-bottom: 10px; float: right;'),
            )); 
        ?>	
        <input class='cancel_iframe' type='button' value='Cancel'>
    </div>

<?php $this->endWidget(); ?>
    
</div><!-- form -->
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/jquery.blockUI.js"></script>  
<script>
$(document).ready(function() {
    $('.form').find('button:submit').click(function(){
        $.blockUI({ overlayCSS: { backgroundColor: '<?php echo BLOCK_UI_COLOR;?>' } }); 
    });
    
});

</script>
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/jquery.colorbox-min.js"></script>
<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/admin/css/colorbox.css"/>