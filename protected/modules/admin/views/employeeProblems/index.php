<?php
$this->breadcrumbs=array(
	$this->singleTitle,
);

$menus=array(
            array('label'=>"Create $this->singleTitle", 'url'=>array('create')),
);
$this->menu= ControllerActionsName::createMenusRoles($menus, $actions);
?>
<h1>Danh Sách <?php echo $this->pluralTitle; ?></h1>

<div id="tabs" class="BindChangeSearchCondition">
    <ul>
        <li>
            <a class="" href="#tabs-1">Tổng quát</a>
        </li>
        <li>
            <a class="" href="#tabs-2">Chi tiết</a>
        </li>
    </ul>
    
    <div id="tabs-1">
        <?php include 'statistic.php';?>
    </div>
    <div id="tabs-2">
        <?php include 'detail.php';?>
    </div>
    
</div>    


<?php Yii::app()->clientScript->registerCoreScript('jquery.ui'); ?>
<script>
$(function() {
    $( "#tabs" ).tabs({ active: 0 });
});
$(document).ready(function() {
    fnUpdateColorbox();
});

function fnUpdateColorbox(){   
    fixTargetBlank();
    $(".updateStatus").colorbox({
        iframe: true,
        innerHeight: '600',
        innerWidth: '800', close: "<span title='close'>close</span>"
    });
}
</script>