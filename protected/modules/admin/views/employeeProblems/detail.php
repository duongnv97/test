<?php
Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('employee-problems-grid', {
                url : $(this).attr('action'),
		data: $(this).serialize()
	});
	return false;
});
");

Yii::app()->clientScript->registerScript('ajaxupdate', "
$('#employee-problems-grid a.ajaxupdate').live('click', function() {
    $.fn.yiiGridView.update('employee-problems-grid', {
        type: 'POST',
        url: $(this).attr('href'),
        success: function() {
            $.fn.yiiGridView.update('employee-problems-grid');
        }
    });
    return false;
});
");
?>

<?php // echo CHtml::link('Tìm Kiếm Nâng Cao','#',array('class'=>'search-button')); ?>
<div class="search-form" style="">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'employee-problems-grid',
	'dataProvider'=>$model->search(),
        'afterAjaxUpdate'=>'function(id, data){ fnUpdateColorbox();}',
        'template'=>'{pager}{summary}{items}{pager}{summary}',
//    'itemsCssClass' => 'items custom_here',
//    'htmlOptions'=> array('class'=>'grid-view custom_here'),
        'pager' => array(
            'maxButtonCount'=>  CmsFormatter::$PAGE_MAX_BUTTON,
        ),
	'enableSorting' => false,
	//'filter'=>$model,
	'columns'=>array(
        array(
            'header' => 'S/N',
            'type' => 'raw',
            'value' => '$this->grid->dataProvider->pagination->currentPage * $this->grid->dataProvider->pagination->pageSize + ($row+1)',
            'headerHtmlOptions' => array('width' => '10px','style' => 'text-align:center;'),
            'htmlOptions' => array('style' => 'text-align:center;')
        ),
		array(
                    'name'  => 'employee_id',
                    'type'=>'raw',
                    'value' => '$data->getEmployee()',
                ),
                array(
                    'header' => 'Chức Vụ',
                    'type'=>'RoleNameUser',                
                    'value'=>'$data->getRoleByUserId()',   
                ),
		array(
                    'name'  => 'object_type',
                    'type'=>'raw',
                    'value' => '$data->getObjectType()',
                ),
		array(
                    'name'  => 'money',
                    'type'=>'raw',
                    'value' => '$data->getMoney()',
                    'htmlOptions' => array('style' => 'text-align:right;')
                ),
		array(
                    'name'  => 'object_id',
                    'type'=>'raw',
                    'value' => '$data->getObjectId()',
                ),
		array(
                    'name'  => 'title',
                    'type'=>'raw',
                    'value' => '$data->getUrlUpdateStatus()."".$data->getField("title")',
                ),
		array(
                    'name'  => 'status',
                    'type'=>'raw',
                    'value'=>'$data->getStatusGrid()',
                ),
		array(
                    'name'  => 'change_by_user_id',
                    'type'=>'raw',
                    'value'=>'$data->getUpdateStatusInfo()',
                ),
		array(
                    'name'  => 'created_date',
                    'value' => '$data->getCreatedDate("d/m/Y H:i:s")',
                ),
		array(
                    'header' => 'Actions',
                    'class'=>'CButtonColumn',
                    'template'=> ControllerActionsName::createIndexButtonRoles($actions),
                    'buttons'=>array(
                        'delete' => array(
                            'visible' => '$data->canDelete()',
                        ),
                    ),
		),
	),
)); ?>