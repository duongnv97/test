<?php
$this->breadcrumbs=array(
    $this->pageTitle,
);
Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
");
?>

<?php echo CHtml::link('Tìm Kiếm Nâng Cao','#',array('class'=>'search-button')); ?>
<div class="search-form" style="">
<?php 
$this->renderPartial('report/_search_report_fault_type',array(
	'model'=>$model,
)); 
?>
</div><!-- search-form -->
<?php 
$aType = $model->getArrayTypeReport();
$employee_header_html = '';
$col_span = 2;
if(isset($_GET['is_employee'])){
//    array_unshift($aType, "Nhân viên");
    $col_span = 3;
    $employee_header_html = "<th>Nhân viên</th>";
}
$aSumHeader = [];
foreach ($aType as $type_id => $item){
    $aSumHeader[$type_id] = 0;
    foreach ($data as $agent_id => $count){
        if(isset($count[$type_id])){
            $aSumHeader[$type_id] += $count[$type_id];
        }
    }
}
?>
<i>*Click vào tiêu đề mỗi cột để sắp xếp</i>
<div id="report-grid" class="grid-view">
    <table class="items materials_table sortable">
        <thead>
            <tr>
                <th>#</th>
                <th>Đại lý</th>
                <?php echo $employee_header_html; ?>
                <?php foreach ($aType as $value) : ?>
                <th class="ClosingBalance"><?php echo $value; ?></th>
                <?php endforeach; ?>
            </tr>
            <?php if(!empty($data)): ?>
            <tr>
                <td class="item_b item_r" colspan="<?php echo $col_span ?>">Tổng</td>
                <?php foreach ($aSumHeader as $sum) : ?>
                <td class="item_r"><b><?php echo empty($sum) ? '0' : ActiveRecord::formatCurrency($sum); ?></b></td>
                <?php endforeach; ?>
            </tr>
            <?php endif; ?>
        </thead>
        <tbody>
        <?php $i = 1; ?>
        <?php foreach ($data as $key => $item) : ?>
        <?php 
            $agent_id = $key;$employee_id = 0;$employee_cell_html = '';
            if(isset($_GET['is_employee'])){
                $aId = explode("-", $key);
                $agent_id = $aId[0]; $employee_id = $aId[1];
                $employee_cell_html = "<td>{$model->getNameUserById($employee_id)}</td>";
            }
        ?>
            <tr class="odd">
                <td class="order_no"><?php echo $i++; ?></td>
                <td><?php echo $model->getAgentById($agent_id); ?></td>
                <?php echo $employee_cell_html; ?>
                <?php foreach ($aType as $key => $value) : ?>
                <td class="item_r"><?php echo isset($item[$key]) ? $item[$key] : '0'; ?></td>
                <?php endforeach; ?>
            </tr>
        <?php endforeach; ?>
        </tbody>
    </table>
</div>



<?php 
$gridCss = Yii::app()->getAssetManager()->publish(Yii::getPathOfAlias('zii.widgets.assets')) . '/gridview/styles.css';
Yii::app()->getClientScript()->registerCssFile($gridCss);
?>
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/sortElements/jquery.sortElements.js"></script>
<script>
$(function(){
var table = $('.sortable');
$('.ClosingBalance')
    .wrapInner('<span title="sort this column"/>')
    .each(function(){

        var th = $(this),
            thIndex = th.index(),
            inverse = false;

        th.click(function(){

            table.find('tbody td').filter(function(){

                return $(this).index() === thIndex;

            }).sortElements(function(a, b){
                var aText = $.text([a]);
                var bText = $.text([b]);
                    aText = parseFloat(aText.replace(/,/g, ''));
                    bText = parseFloat(bText.replace(/,/g, ''));
                    aText = aText *1;
                    bText = bText *1;

                if( aText == bText )
                    return 0;

//                return aText > bText ? // asc 
                return aText < bText ? // DESC
                    inverse ? 1 : -1
                    : inverse ? -1 : 1;
                    
            }, function(){
                // parentNode is the element we want to move
                return this.parentNode; 
            });

            inverse = !inverse;
            fnRefreshOrderNumber();
        });
    });
    
//    $('.ClosingBalance').trigger('click');
    
});
</script>
<script>
$(document).ready(function() {
//    fnAddClassOddEven('items');
    fnUpdateColorbox();
});

function fnUpdateColorbox(){   
    fixTargetBlank();
}
</script>
<style>
    .sortable thead th:hover {cursor: pointer;}
</style>