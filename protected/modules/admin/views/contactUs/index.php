<?php
$this->breadcrumbs=array(
	'Contact Us',
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('block-grid', {
		data: $(this).serialize()
	});
	return false;
});
");

Yii::app()->clientScript->registerScript('ajaxupdate', "
$('#block-grid a.ajaxupdate').live('click', function() {
    $.fn.yiiGridView.update('block-grid', {
        type: 'POST',
        url: $(this).attr('href'),
        success: function() {
            $.fn.yiiGridView.update('block-grid');
        }
    });
    return false;
});
");
?>

<h1>List Contacts</h1>

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'contact-us-grid',
	'dataProvider'=>$model->search(),
	//'filter'=>$model,
	'columns'=>array(
		array(
            'header' => 'S/N',
            'type' => 'raw',
            'value' => '$this->grid->dataProvider->pagination->currentPage * $this->grid->dataProvider->pagination->pageSize + ($row+1)',
            'headerHtmlOptions' => array('width' => '30px','style' => 'text-align:center;'),
            'htmlOptions' => array('style' => 'text-align:center;')
        ),
		array(
            'header'=>'Name',
            'value'=>'$data->title . " " . $data->first_name . " " . $data->last_name',
        ),
        'company_name',
        'phone',
        'email',
        array(
            'name'=>'country.name',
            'header'=>'Country',
        ),
        'website',
        array(
            'name'=>'created_time',
            'header'=>'Send Time',
            'type'=>'datetime',
        ),
		array(
			'class'=>'CButtonColumn',
            'template'=>'{view}{delete}'
		),
	),
)); ?>
