<?php
$this->breadcrumbs=array(
	'Contact us'=>array('contactUs/'),
	$model->title . ' ' . $model->first_name . ' ' . $model->last_name,
);
?>

<h1>View Contact</h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
//		'id',
		array(
            'label'=>'Name',
            'value'=>$model->title . ' ' . $model->first_name . ' ' . $model->last_name,
        ),
        'company_name',
        'phone',
        'email',
        array(
            'name'=>'country.name',
            'label'=>'Country',
        ),
        'website',
        'comment',
        array(
            'name'=>'created_time',
			'type'=>'datetime',
            'label'=>'Send Time',
        ),
	),
)); ?>
