<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'users-price-form',
	'enableAjaxValidation'=>false,
        'htmlOptions' => array('enctype' => 'multipart/form-data', 'target'=>'_blank'),
        'action'=>Yii::app()->createUrl('admin/ajax/quotesPrice'),
        
)); ?>
    
    <?php echo MyFormat::BindNotifyMsg(); ?>
        
    <div class="row fix_custom_label_required">
        <?php echo $form->label($model,'Ngày báo giá'); ?>
        <?php 
            $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                'model'         =>$model,        
                'attribute'     =>'created_date',
                'options'       =>array(
                    'showAnim'          =>'fold',
                    'dateFormat'        => MyFormat::$dateFormatSearch,
                    'minDate'           => '0',
                    'changeMonth'       => true,
                    'changeYear'        => true,
                    'showOn'            => 'button',
                    'buttonImage'       => Yii::app()->theme->baseUrl.'/admin/images/icon_calendar_r.gif',
                    'buttonImageOnly'   => true,                                
                ),        
                'htmlOptions'   =>array(
                    'class'     =>'w-300 created_date',
                    'name'      =>'ngay_bao',
                    'size'      =>'w-16',
                    'value'     => date('d-m-Y'),
                    'readOnly'  =>1
                ),
            ));
        ?>     		
    </div>
    <div class="row fix_custom_label_required">
        <?php echo $form->labelEx($model,'Kính gửi'); ?>
        <?php echo $form->textField($model,'customer_parent_id', array('class'=>'w-300', 'name'=>'kinh_gui')); ?>
    </div>
    <div class="row fix_custom_label_required">
        <?php echo $form->labelEx($model,'Người nhận'); ?>
        <?php echo $form->textField($model,'customer_id', array('class'=>'w-300', 'name'=>'nguoi_nhan')); ?>
    </div>
    
    <div class="row fix_custom_label_required">
        <?php echo $form->labelEx($model, 'Giá 12kg') ?>
        <?php echo $form->textField($model, 'price_small',array('class'=>'w-300 ad_fix_currency amount_discount', 'placeHolder'=>'0','name'=>'gia_12')) ?>
    </div>

    <div class="row fix_custom_label_required">
        <?php echo $form->labelEx($model,'Giá 45kg & 50kg') ?>
        <?php echo $form->textField($model, 'price',array('class'=>'w-300 ad_fix_currency amount_discount','placeHolder'=>'0','name'=>'gia_45')) ?>
    </div>
    
    <div class="row buttons" style="padding-left: 141px;">
        <?php $this->widget('bootstrap.widgets.TbButton', array(
        'buttonType'=>'submit',
        'label' => 'Xuất PDF',
        'type'=>'null', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
        'size'=>'small', // null, 'large', 'small' or 'mini'
        'htmlOptions' => array('name' => 'form_quote_price'),
    )); ?>	
    </div>

<?php $this->endWidget(); ?>

</div><!-- form -->

<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/autoNumeric-master/autoNumeric.js"></script>
<script>
    jQuery(document).ready(function(){
        fnBindFixLabelRequired();
        fnInitInputCurrency();
});
</script>