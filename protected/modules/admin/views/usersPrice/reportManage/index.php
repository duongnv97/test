<?php
$sta2 = new Sta2();
$mGasSupportCustomer = new GasSupportCustomer();
$this->breadcrumbs = array(
    $this->pageTitle,
);
$stt = 0;
$aCustomerInvest = $mGasSupportCustomer->getCacheArraytCustomerInvert(); // cache
$menus=array();

if($model->canExportExcelReportManage()):
    $menus[] = [
        'label'=> 'Xuất Excel Danh Sách Hiện Tại',
        'url'=>array('reportManage', 'toExcel'=>1), 
        'htmlOptions'=>array('class'=>'export_excel ','label'=>'Xuất Excel'),
    ];
endif;
    
$this->menu= ControllerActionsName::createMenusRoles($menus, $actions);
?>
<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/print-store-card.css" />
<h1><?php echo $this->pageTitle;?></h1>
<div class="search-form" style="">
    <?php
    $this->renderPartial('reportManage/_search', array(
        'model' => $model,
    ));
    ?>
</div>

<div class="grid-view">
    <table class="tb hm_table sortable materials_table">
        <thead>
        <tr style="">
            <th class="w-20">#</th>
            <th class=" ">Tên KH </th>
            <th class=" ">Địa chỉ</th>
            <th class="w-80 ">Sale</th>
            <th class="w-80 ">Giá G</th>
            <th class="w-80 ClosingBalance">Giá *</th>
            <th class="w-80 ClosingBalance">Tiền đầu tư *</th>
            <th class="w-100 " onclick="document.getElementById('chooseButton').click()">SL bình quân tháng *</th>
            <th class="w-100 ClosingBalance" >SL tháng gần nhất *</th>
            <th class="w-100 ClosingBalance display_none" id="chooseButton" >SL </th>
            <th class="w-80 ">Lấy hàng mới nhất</th>
        </tr>
        </thead>
        <tbody>
            <?php 
            if(!empty($aData) ): 
                $aUsersPrice = $aData['aUsersPrice'];
                $aUsers = $aData['aUsers'];
                $aCustomerMonth = $aData['aCustomerMonth']; // SL bình quân
                $aOneMonth = $aData['aOneMonth'];
                foreach ($aUsersPrice as $item): 
                $stt++;
                if(!isset($aCustomerMonth[$item->customer_id]) || !isset($aOneMonth[$item->customer_id])) continue;
                ?>
                <tr>
                    <td class="item_c order_no"><?php echo $stt; ?></td>
                    <td class="item_l"><?php echo isset($aUsers[$item->customer_id]) ? $aUsers[$item->customer_id]->code_bussiness.' - '.$aUsers[$item->customer_id]->first_name : ''; ?></td>
                    <td class="item_l"><?php echo isset($aUsers[$item->customer_id]) ? $aUsers[$item->customer_id]->address : '';?></td>
                    <td class="item_l"><?php echo isset($aUsers[$item->sale_id]) ? $aUsers[$item->sale_id]->first_name : ''; ?></td>
                    <td class="item_c"><?php echo  ($item->price_code != -1) ? $item->price_code : ' '; ?></td>
                    <td class="item_r"><?php echo ActiveRecord::formatCurrencyRound($item->price); ?></td>
                    <td class="item_r"><?php echo ActiveRecord::formatCurrencyRound($aCustomerInvest[$item->customer_id]); ?></td>
                    <td class="item_b item_c" ><?php echo isset($aCustomerMonth[$item->customer_id]) ? $sta2->convertOutputCustomerForView($aCustomerMonth[$item->customer_id]) : ''; ?></td>
                    <td class="item_c"><?php echo isset($aUsers[$item->customer_id]) ? ActiveRecord::formatCurrencyRound($aOneMonth[$item->customer_id]) : ''; ?></td>
                    <td class="item_b item_c display_none" ><?php echo isset($aCustomerMonth[$item->customer_id]) ? $sta2->sumOutputCustomerForView($aCustomerMonth[$item->customer_id]) : ''; ?></td>
                    <td class="item_c"><?php echo isset($aUsers[$item->customer_id]) ? MyFormat::dateDmyToYmdForAllIndexSearch($aUsers[$item->customer_id]->last_purchase) : ''; ?></td>
                </tr>
                <?php 
                endforeach;
            endif; 
            ?>
        </tbody>
    </table>
</div>

<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/sortElements/jquery.sortElements.js"></script>
<script type="text/javascript">
$(function(){
var table = $('.sortable');
$('.ClosingBalance')
    .wrapInner('<span title="sort this column"/>')
    .each(function(){

        var th = $(this),
            thIndex = th.index(),
            inverse = false;

        th.click(function(){

            table.find('td').filter(function(){

                return $(this).index() === thIndex;

            }).sortElements(function(a, b){
                var aText = $.text([a]);
                var bText = $.text([b]);
                    aText = parseFloat(aText.replace(/,/g, ''));
                    bText = parseFloat(bText.replace(/,/g, ''));
                    aText = aText *1;
                    bText = bText *1;

                if( aText == bText )
                    return 0;

//                return aText > bText ? // asc 
                return aText < bText ? // DESC
                    inverse ? -1 : 1
                    : inverse ? 1 : -1;

            }, function(){

                // parentNode is the element we want to move
                return this.parentNode; 

            });

            inverse = !inverse;
            fnRefreshOrderNumber();
        });
    });
    
    $('.ClosingBalance').trigger('click');
    
});
    
</script>

