<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=> GasCheck::getCurl(),
	'method'=>'get',
)); 
$aMoney = $model->getArrayMoneyInvest();
?>
    <div class="row more_col">
        <div class="col1 ">
            <?php echo $form->label($model,'money_invest',array()); ?>
            <?php echo $form->dropDownList($model,'money_invest', $aMoney,array('class'=>'w-200'));// ,'empty'=>'Select' ?> 
        </div>
        
        <div class="col2 ">
            <?php echo $form->label($model,'money_invest_max',array()); ?>
            <?php echo $form->dropDownList($model,'money_invest_max', $aMoney,array('class'=>'w-200'));// ,'empty'=>'Select' ?> 
        </div>
        <div class="col3 ">
            <?php echo $form->label($model,'price_code',array()); ?>
            <?php echo $form->dropDownList($model,'price_code', $model->getCodePriceBo(),array('class'=>'w-200')); ?>
        </div>
        
    </div>
    <div class="row more_col">
        <div class="col1">
            <?php echo $form->labelEx($model,'payment_day'); ?>
            <?php echo $form->dropDownList($model,'payment_day', GasTypePay::getArrAll(), array('style'=>'width:376px','empty'=>'Select')); ?>
            <?php echo $form->error($model,'payment_day'); ?>
        </div>
        <div class="col2 ">
            <?php echo $form->label($model,'qty',array()); ?>
            <?php echo $form->dropDownList($model,'qty', $model->getArrayQuantity(),array('class'=>'w-200'));// ,'empty'=>'Select' ?> 
        </div>
        <div class="col3">
            <?php echo $form->label($model,'time'); ?>
            <?php 
                $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                    'model'=>$model,        
                    'attribute'=>'time',
                    'options'=>array(
                        'showAnim'=>'fold',
                        'dateFormat'=> MyFormat::$dateFormatSearch,
//                            'minDate'=> '0',
//                        'maxDate'=> '0',
                        'changeMonth' => true,
                        'changeYear' => true,
                        'showOn' => 'button',
                        'buttonImage'=> Yii::app()->theme->baseUrl.'/admin/images/icon_calendar_r.gif',
                        'buttonImageOnly'=> true,                                
                    ),        
                    'htmlOptions'=>array(
                        'class'=>'w-16',
                        'size'=>'16',
                        'style'=>'float:left;', 
                        'readonly'=>'1',
                    ),
                ));
            ?>
        </div>
        
    </div>
    <div class="row buttons" style="padding-left: 159px;">
        <?php $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType'=>'submit',
            'label'=>'Search',
            'type'=>'null', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
            'size'=>'small', // null, 'large', 'small' or 'mini'
            //'htmlOptions' => array('style' => 'margin-bottom: 10px; float: right;'),
        )); ?>	
    </div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->