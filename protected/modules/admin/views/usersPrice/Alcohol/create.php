<?php
$this->breadcrumbs=array(
	'Giá Cồn'=>array('indexAlcohol'),
	'Tạo Mới',
);
$menus=array(
    array(
        'label'=>"Quản Lý Giá Cồn", 
        'url'=>array('indexAlcohol'), 
        'htmlOptions'=>array('class'=>'index','label'=>'Quản Lý')
        ),
);
$this->menu= ControllerActionsName::createMenusRoles($menus, $actions);
?>

<h1>Tạo Mới Giá Cồn</h1>

<?php echo $this->renderPartial('Alcohol/_form_create', array('model'=>$model)); ?>