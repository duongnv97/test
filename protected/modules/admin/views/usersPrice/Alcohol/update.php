<?php
$this->breadcrumbs = array(
	'Giá Cồn' => array('indexAlcohol'),
	'Cập Nhật Giá Cồn',
);
$menus=array(
    array(
        'label'=>"Quản Lý Giá Cồn", 
        'url'=>array('indexAlcohol'), 
        'htmlOptions'=>array('class'=>'index','label'=>'Quản Lý')
        ),
    array(
        'label'=>"Tạo Mới Giá Cồn", 
        'url'=>array('CreateAlcohol'), 
        'htmlOptions'=>array('class'=>'create','label'=>'Tạo mới')
        ),
);
$this->menu= ControllerActionsName::createMenusRoles($menus, $actions);

?>

<h1>Cập Nhật: Giá Cồn</h1>

<?php echo $this->renderPartial('Alcohol/_form_update', array('model'=>$model)); ?>