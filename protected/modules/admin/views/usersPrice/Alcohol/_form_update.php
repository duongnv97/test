<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'users-price-form',
	'enableAjaxValidation'=>false,
        'htmlOptions' => array('enctype' => 'multipart/form-data'),
)); ?>
    <p class="note">Dòng có dấu <span class="required">*</span> là bắt buộc.</p>
    <?php echo MyFormat::BindNotifyMsg(); ?>
    <table class="materials_table materials_table_root materials_table_th hm_table" style="">
        <thead>
            <tr>
                <th class="item_c w-20">#</th>
                <th class="w-350 item_c">Khách Hàng</th>
                <th class="w-80 item_c">Giá Cồn</th>
            </tr>
        </thead>
        <tbody>
        <?php foreach ($model->aModelUpdate as $key=>$mUpdate): ?>
            <tr class="cash_book_row row_input">
                <td class="item_c order_no"><?php echo $key+1;?></td>
                <td class="col_customer">
                    <div class="row row_customer_autocomplete">
                        <div class="float_l display_none">
                            <input class="customer_id_hide" value="<?php echo $mUpdate->customer_id;?>" type="hidden" name="customer_id[]">
                            <input value="<?php echo $mUpdate->id;?>" type="hidden" name="id[]">
                            <input value="<?php echo $mUpdate->c_month;?>" type="hidden" name="c_month[]">
                            <input value="<?php echo $mUpdate->c_year;?>" type="hidden" name="c_year[]">
                            <input class="float_l customer_autocomplete w-250"  placeholder="Nhập tên số đt, từ <?php echo MIN_LENGTH_AUTOCOMPLETE; ?> ký tự" maxlength="100" value="" type="text" >
                            <span class="remove_row_item" onclick="fnRemoveName(this);"></span>
                        </div>
                        <?php 
                            $mCustomer = $mUpdate->rCustomer;
                            $aData = array(
                                'class_custom'=>'table_small',
                                'info_code_bussiness'=>$mCustomer->code_bussiness,
                                'info_name'=>$mCustomer->first_name,
                                'info_address'=>$mCustomer->address,
                                'info_phone'=>$mCustomer->phone,
                            );
                            $this->widget('ext.GasTableCustomerInfo.GasTableCustomerInfo',
                                array('data'=>$aData));                                        
                        ?>
                    </div>
                </td>
                <td class="item_c">
                    <input name="price_alcohol_value[]" class="w-80 gPriceAlcohol number_only ad_fix_currency" maxlength="9" value="<?php echo ActiveRecord::formatNumberInput($mUpdate->price_alcohol_value);?>" type="text" placeholder="Giá cồn">
                </td>
                
                <!--<td class="item_c last"><span remove="" class="remove_icon_only"></span></td>-->
            </tr>
        <?php endforeach; ?>
        </tbody>
    </table>
    <div class="row buttons" style="padding-left: 141px;">
        <?php $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType'=>'submit',
            'label' => 'Save',
            'type'=>'null', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
            'size'=>'small', // null, 'large', 'small' or 'mini'
            //'htmlOptions' => array('style' => 'margin-bottom: 10px; float: right;'),
        )); ?>	
    </div>

<?php $this->endWidget(); ?>

</div><!-- form -->
<script>
    $(document).ready(function(){
        $('.form').find('button:submit').click(function(){
            $.blockUI({ overlayCSS: { backgroundColor: '#fff' } }); 
        });
    });
</script>

<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/autoNumeric-master/autoNumeric.js"></script>
<script>
    jQuery(document).ready(function(){
        fnBindRemoveIcon();
        gSelectChange(<?php echo UsersRef::PRICE_OTHER;?>);
        fnInitInputCurrency();
});
</script>