<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'users-price-form',
	'enableAjaxValidation'=>false,
        'htmlOptions' => array('enctype' => 'multipart/form-data'),
)); ?>

    <p class="note">Dòng có dấu <span class="required">*</span> là bắt buộc.</p>
    <?php echo MyFormat::BindNotifyMsg(); ?>
    <div class="row display_none">
            <?php echo $form->labelEx($model,'c_month',array('style' => ''));?>
            <?php echo $form->dropDownList($model,'c_month', ActiveRecord::getMonthVn(),array('class' => 'w-150', 'empty'=>"Select")); ?>
    </div>
    <div class="row display_none">
            <?php echo $form->labelEx($model,'c_year'); ?>
            <?php echo $form->dropDownList($model,'c_year', ActiveRecord::getRangeYear(2016, date('Y')+35),array('class' => 'w-150', 'empty'=>"Select")); ?>
    </div>
    <?php include "_form_multi.php"; ?>
    
    <div class="row buttons" style="padding-left: 141px;">
        <?php $this->widget('bootstrap.widgets.TbButton', array(
        'buttonType'=>'submit',
        'label' => 'Save',
        'type'=>'null', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
        'size'=>'small', // null, 'large', 'small' or 'mini'
        //'htmlOptions' => array('style' => 'margin-bottom: 10px; float: right;'),
    )); ?>	
    </div>

<?php $this->endWidget(); ?>

</div><!-- form -->
<script>
    $(document).ready(function(){
        $('.form').find('button:submit').click(function(){
            $.blockUI({ overlayCSS: { backgroundColor: '#fff' } }); 
        });
        gSelectAll();
    });
    
    function gSelectAll(){
        $('.gSelectAll').change(function(){
            $('.gSelect').val($(this).val());
            $('.gSelect').trigger('change');
        });
    }
    
    
</script>

<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/autoNumeric-master/autoNumeric.js"></script>
<script>
    jQuery(document).ready(function(){
        fnInitInputCurrency();
});
</script>