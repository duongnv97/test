<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'users-price-form',
	'enableAjaxValidation'=>false,
        'htmlOptions' => array('enctype' => 'multipart/form-data'),
)); ?>

    <p class="note">Dòng có dấu <span class="required">*</span> là bắt buộc.</p>
    <?php echo MyFormat::BindNotifyMsg(); ?>
        
    <?php /*
    <div class="row">
        <?php echo $form->labelEx($model,'customer_id'); ?>
        <?php echo $form->hiddenField($model,'customer_id', array('class'=>'')); ?>
        <?php 
            // widget auto complete search user customer and supplier
            $aData = array(
                'model'=>$model,
                'field_customer_id'=>'customer_id',
                'name_relation_user'=>'rCustomer',
                'url' => Yii::app()->createAbsoluteUrl('admin/ajax/search_user_by_code'),
                'ClassAdd' => 'w-500',
            );
            $this->widget('ext.SpaAutocompleteCustomer.SpaAutocompleteCustomer',
                array('data'=>$aData));                                        
        ?>
        <?php echo $form->error($model,'customer_id'); ?>
    </div> */ ?>
    
    <div class="row display_none">
            <?php echo $form->labelEx($model,'c_month',array('style' => ''));?>
            <?php echo $form->dropDownList($model,'c_month', ActiveRecord::getMonthVn(),array('class' => 'w-150', 'empty'=>"Select")); ?>
    </div>
    <div class="row display_none">
            <?php echo $form->labelEx($model,'c_year'); ?>
            <?php echo $form->dropDownList($model,'c_year', ActiveRecord::getRangeYear(2016, date('Y')+35),array('class' => 'w-150', 'empty'=>"Select")); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model,'price_code') ?>
        <?php echo $form->dropDownList($model,'price_code', $model->listDataPriceCode(), array('empty'=>'Select', 'class'=>'w-150 gSelectAll')); ?>
        <?php echo $form->error($model,'price_code'); ?>
    </div>
    
    <?php include "_form_multi.php"; ?>

<!--    <div class="row">
        <?php echo $form->labelEx($model,'price'); ?>
        <?php echo $form->textField($model,'price', array('class'=>'item_r f_size_18 ad_fix_currency')); ?>
        <?php echo $form->error($model,'price'); ?>
    </div>-->
    
    <div class="row buttons" style="padding-left: 141px;">
        <?php $this->widget('bootstrap.widgets.TbButton', array(
        'buttonType'=>'submit',
        'label' => 'Save',
        'type'=>'null', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
        'size'=>'small', // null, 'large', 'small' or 'mini'
        //'htmlOptions' => array('style' => 'margin-bottom: 10px; float: right;'),
    )); ?>	
    </div>

<?php $this->endWidget(); ?>

</div><!-- form -->
<script>
    $(document).ready(function(){
        $('.form').find('button:submit').click(function(){
            $.blockUI({ overlayCSS: { backgroundColor: '#fff' } }); 
        });
        gSelectAll();
    });
    
    function gSelectAll(){
        $('.gSelectAll').change(function(){
            $('.gSelect').val($(this).val());
            $('.gSelect').trigger('change');
        });
    }
    
    
</script>

<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/autoNumeric-master/autoNumeric.js"></script>
<script>
    jQuery(document).ready(function(){
        fnInitInputCurrency();
});
</script>