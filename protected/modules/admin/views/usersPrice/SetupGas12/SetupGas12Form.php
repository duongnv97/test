<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/print-store-card.css" />
<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'gas-target-monthly-form',
)); ?>
<?php echo MyFormat::BindNotifyMsg(); ?>
<div class="form">
    <h3 class='title-info'>Loại Gas</h3>
    <div class="" style="">
        <input type="submit" value="Save">
    </div>

    <?php if($loadDefault): ?>
        <?php include "SetupGas12FormDefault.php"; ?>
    <?php else: ?>
        <?php include "SetupGas12FormData.php"; ?>
    <?php endif; ?>
    <div class="" style="">
        <input type="submit" value="Save">
    </div>
</div>

<?php $this->endWidget(); ?>
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/autoNumeric-master/autoNumeric.js"></script>
<script>
    jQuery(document).ready(function(){
        fnInitInputCurrency();
});
</script>