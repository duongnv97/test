<table cellpadding="0" cellspacing="0" class="tb hm_table f_size_18">
    <thead>
        <tr>
            <th>#</th>
            <th>Diễn giải</th>
            <th>ĐVT</th>
            <?php foreach ($aZone as $zone_id => $zone_name): ?>
                <th><?php echo $zone_name;?></th>
            <?php endforeach; ?>
        </tr>
    </thead>
    <tbody>
    <?php foreach ($aGas12 as $key => $mMaterial): ?>
        <tr>
            <td class="item_c"><?php echo $index++; ?></td>
            <td><?php echo $mMaterial->materials_no." - ". $mMaterial->getName();?></td>
            <td class="item_c"><?php echo $mMaterial->getUnit();?></td>
            <?php foreach ($aZone as $zone_id => $zone_name): ?>
            <td class="item_r">
                <input class="item_r f_size_18 ad_fix_currency w-100" name="price[<?php echo $mMaterial->id;?>][<?php echo $zone_id;?>]" type="text" maxlength="15" placeholder="Giá bán">
                <input class="item_r f_size_18 ad_fix_currency w-100" name="price_root[<?php echo $mMaterial->id;?>][<?php echo $zone_id;?>]" type="text" maxlength="15" placeholder="Giá vốn">
                <input name="materials_type_id[<?php echo $mMaterial->id;?>]" value="<?php echo $mMaterial->materials_type_id;?>" type="hidden">
            </td>
            <?php endforeach; ?>
        </tr>
    <?php endforeach; ?>
    </tbody>
</table>
<?php $index = 1; ?>
<h3 class='title-info'>Vật Tư</h3>
<div class="" style="">
    <input type="submit" value="Save">
</div>
<table cellpadding="0" cellspacing="0" class="tb hm_table f_size_18">
    <tbody>
    <?php foreach ($aVatTu as $key => $mMaterial): ?>
    <tr>
        <td class="item_c"><?php echo $index++; ?></td>
        <td><?php echo $mMaterial->materials_no." - ". $mMaterial->getName();?></td>
        <td class="item_c"><?php echo $mMaterial->getUnit();?></td>
        <?php foreach ($aZone as $zone_id => $zone_name): ?>
        <td class="item_r">
            <input class="item_r f_size_18 ad_fix_currency w-100" name="price[<?php echo $mMaterial->id;?>][<?php echo $zone_id;?>]" type="text" maxlength="15" placeholder="Giá bán">
            <input class="item_r f_size_18 ad_fix_currency w-100" name="price_root[<?php echo $mMaterial->id;?>][<?php echo $zone_id;?>]" type="text" maxlength="15" placeholder="Giá vốn">
            <input name="materials_type_id[<?php echo $mMaterial->id;?>]" value="<?php echo $mMaterial->materials_type_id;?>" type="hidden">
        </td>
        <?php endforeach; ?>
    </tr>
    <?php endforeach; ?>
    </tbody>
</table>