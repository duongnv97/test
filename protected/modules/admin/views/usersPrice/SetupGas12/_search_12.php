<div class="wide form">
    <?php $form=$this->beginWidget('CActiveForm', array(
            'action'=> Yii::app()->createAbsoluteUrl("admin/usersPrice/setupGas12"),
            'method'=>'get',
    )); ?>
	<div class="row">
                <?php echo $form->label($model,'c_month',array('style' => ''));?>
		<?php echo $form->dropDownList($model,'c_month', ActiveRecord::getMonthVn(),array('class' => 'w-150', 'empty'=>"Select")); ?>
	</div>
	<div class="row">
		<?php echo $form->label($model,'c_year'); ?>
		<?php echo $form->dropDownList($model,'c_year', ActiveRecord::getRangeYear(2016, date('Y')+35),array('class' => 'w-150', 'empty'=>"Select")); ?>
	</div>
        <input type="hidden" name="search_button_click" value="" class="search_button_click">
	<div class="row buttons" style="padding-left: 159px;">
            <?php $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType'=>'submit',
            'label'=>'Tìm Kiếm',
            'type'=>'null', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
            'size'=>'small', // null, 'large', 'small' or 'mini'
            'htmlOptions' => array('class' => 'search_button'),
        )); ?>	</div>
    <?php $this->endWidget(); ?>
</div><!-- search-form -->
<script>
    $(document).ready(function(){
        $('.search_button').click(function(){
            $('.search_button_click').val(1);
//            $('.form').find('select').attr('disabled',true);
        });
    })
</script>