<table cellpadding="0" cellspacing="0" class="tb hm_table f_size_18">
    <thead>
        <tr>
            <th>#</th>
            <th>Diễn giải</th>
            <th>ĐVT</th>
            <?php foreach ($aZone as $zone_id => $zone_name): ?>
                <th class="item_r">
                    <?php echo $zone_name;?>
                    <input zone='<?php echo $zone_id;?>' class="ajust_amount w-100 f_size_18 item_r" placeholder="Số tăng giảm">
                </th>
            <?php endforeach; ?>
        </tr>
    </thead>
    <tbody>
    <?php foreach ($aGas12 as $key => $mUserPrice): ?>
        <?php 
            $mMaterial = $aModelMaterials[$mUserPrice->customer_id];
//            $aPriceZone = json_decode($mUserPrice->price_json_hgd, true);
            
            // Begin fix Jun 26, 2016
            $tmp = json_decode($mUserPrice->price_json_hgd, true);
            $aPriceZone = $tmp['price_sell'];
            $aPriceRoot = $tmp['price_root'];
            // End fix Jun 26, 2016
            
            //fix Aug 24, 2016
            unset($aModelMaterialsNew[$mUserPrice->customer_id]);
            //fix Aug 24, 2016

        ?>
        <tr>
            <td class="item_c"><?php echo $index++; ?></td>
            <td><?php echo $mMaterial->materials_no." - ". $mMaterial->getName();?></td>
            <td class="item_c"><?php echo $mMaterial->getUnit();?></td>
            <?php foreach ($aZone as $zone_id => $zone_name): ?>
            <td class="item_r">
                <input value="<?php echo isset($aPriceZone[$zone_id]) ? $aPriceZone[$zone_id] : "";?>" class="price_show item_r f_size_18 ad_fix_currency w-100" name="price[<?php echo $mMaterial->id;?>][<?php echo $zone_id;?>]" type="text" maxlength="15" placeholder="Giá bán">
                <br>
                <input value="<?php echo isset($aPriceRoot[$zone_id]) ? $aPriceRoot[$zone_id] : "";?>" class="item_r f_size_18 ad_fix_currency w-100" name="price_root[<?php echo $mMaterial->id;?>][<?php echo $zone_id;?>]" type="text" maxlength="15" placeholder="Giá vốn">
                <input name="materials_type_id[<?php echo $mMaterial->id;?>]" value="<?php echo $mMaterial->materials_type_id;?>" type="hidden">
                <span class="price_hide display_none zone_<?php echo $zone_id;?>"><?php echo isset($aPriceZone[$zone_id]) ? $aPriceZone[$zone_id] : "";?></span>
            </td>
            <?php endforeach; ?>
        </tr>
    <?php endforeach; ?>

    <tr><td colspan="6">For new item<hr/></td></tr>
    <?php foreach ($aModelMaterialsNew as $materials_id): ?>
        <?php $mMaterialNew = $aModelMaterials[$materials_id];
            if( $mMaterialNew->materials_no != 'GAS0146'){
                continue;// Now 17, 2016 xử lý setup gía cho 1 số loại gas ko load lên ben tren
                // 1. Now 17, 2016 xử lý cho GAS0146 Gas Vgas đỏ 12
            }
        ?>
        <tr>
            <td class="item_c">1</td>
            <td><?php echo $mMaterialNew->materials_no." - ". $mMaterialNew->getName();?></td>
            <td class="item_c"><?php echo $mMaterialNew->getUnit();?></td>
            <?php foreach ($aZone as $zone_id => $zone_name): ?>
            <td class="item_r">
                <input value="" class="price_show item_r f_size_18 ad_fix_currency w-100" name="price[<?php echo $mMaterialNew->id;?>][<?php echo $zone_id;?>]" type="text" maxlength="15" placeholder="Giá bán">
                <br>
                <input value="" class="item_r f_size_18 ad_fix_currency w-100" name="price_root[<?php echo $mMaterialNew->id;?>][<?php echo $zone_id;?>]" type="text" maxlength="15" placeholder="Giá vốn">
                <input name="materials_type_id[<?php echo $mMaterialNew->id;?>]" value="<?php echo $mMaterialNew->materials_type_id;?>" type="hidden">
                <span class="price_hide display_none zone_<?php echo $zone_id;?>"><?php echo isset($aPriceZone[$zone_id]) ? $aPriceZone[$zone_id] : "";?></span>
            </td>
            <?php endforeach; ?>
        </tr>
    <?php endforeach; ?>
    
    </tbody>
</table>
<?php $index = 1; ?>
<h3 class='title-info'>Vật Tư</h3>
<div class="" style="">
    <input type="submit" value="Save">
</div>
<table cellpadding="0" cellspacing="0" class="tb hm_table f_size_18">
    <thead>
        <tr>
            <th>#</th>
            <th>Diễn giải</th>
            <th>ĐVT</th>
            <?php foreach ($aZone as $zone_id => $zone_name): ?>
                <th class="item_r">
                    <?php echo $zone_name;?>
                    <!--<input zone='<?php echo $zone_id;?>' class="ajust_amount w-100 f_size_18 item_r" placeholder="Số tăng giảm">-->
                </th>
            <?php endforeach; ?>
        </tr>
    </thead>
    <tbody>
    <?php foreach ($aVatTu as $key => $mUserPrice): ?>
    <?php 
        $mMaterial = $aModelMaterials[$mUserPrice->customer_id];
//        $aPriceZone = json_decode($mUserPrice->price_json_hgd, true);
        
        // Begin fix Jun 26, 2016
        $tmp = json_decode($mUserPrice->price_json_hgd, true);
        $aPriceZone = $tmp['price_sell'];
        $aPriceRoot = $tmp['price_root'];
        // End fix Jun 26, 2016
    ?>
    <tr>
        <td class="item_c"><?php echo $index++; ?></td>
        <td><?php echo $mMaterial->materials_no." - ". $mMaterial->getName();?></td>
        <td class="item_c"><?php echo $mMaterial->getUnit();?></td>
        <?php foreach ($aZone as $zone_id => $zone_name): ?>
        <td class="item_r">
            <input value="<?php echo isset($aPriceZone[$zone_id]) ? $aPriceZone[$zone_id] : "";?>" class="price_show item_r f_size_18 ad_fix_currency w-100" name="price[<?php echo $mMaterial->id;?>][<?php echo $zone_id;?>]" type="text" maxlength="15" placeholder="Giá bán">
            <br>
            <input value="<?php echo isset($aPriceRoot[$zone_id]) ? $aPriceRoot[$zone_id] : "";?>" class="item_r f_size_18 ad_fix_currency w-100" name="price_root[<?php echo $mMaterial->id;?>][<?php echo $zone_id;?>]" type="text" maxlength="15" placeholder="Giá vốn">
            <input name="materials_type_id[<?php echo $mMaterial->id;?>]" value="<?php echo $mMaterial->materials_type_id;?>" type="hidden">
            <span class="price_hide display_none"><?php echo isset($aPriceZone[$zone_id]) ? $aPriceZone[$zone_id] : "";?></span>
        </td>
        <?php endforeach; ?>
    </tr>
    <?php endforeach; ?>
    </tbody>
</table>

<script>
    $(function(){
        $('.ajust_amount').change(function(){
            var zone_id = $(this).attr('zone');
            var ajust_amount = $(this).val()*1;
            $('.zone_'+zone_id).each(function(){
                var td = $(this).closest('td');
                var price_hide = td.find('.price_hide').text()*1;
                var price_new = price_hide + ajust_amount;
                var price_show = td.find('.price_show');
                if(price_show.val() !="" ){
                    price_show.val(price_new);
                }
            });
            
        });
    });
</script>