<?php
$this->breadcrumbs=array(
    'Thiết lập giá gas hộ gia đình',
);
$aModelMaterials = GasMaterials::getArrayModel("");
$aModelMaterialsNew = GasMaterials::getArrayIdByMaterialTypeId(GasMaterialsType::$SETUP_PRICE_GAS_12);
$aGas12 = $model->getByMonthYear(UsersPrice::TYPE_HO_GD, GasMaterialsType::$SETUP_PRICE_GAS_12);
$aVatTu = $model->getByMonthYear(UsersPrice::TYPE_HO_GD, GasMaterialsType::$SETUP_PRICE_VATTU);
$loadDefault = false;
if(count($aGas12) < 1 ):// Nếu tháng hiện tại không có dữ liệu thì sẽ lấy của tháng trước
    $mUserPriceNew = new UsersPrice();
    $model->calcMonthYear($mUserPriceNew);
    $aGas12 = $mUserPriceNew->getByMonthYear(UsersPrice::TYPE_HO_GD, GasMaterialsType::$SETUP_PRICE_GAS_12);
    $aVatTu = $mUserPriceNew->getByMonthYear(UsersPrice::TYPE_HO_GD, GasMaterialsType::$SETUP_PRICE_VATTU);
    // Nếu tháng trước không có dữ liệu nữa thì lấy của mặc định
    if(count($aGas12) < 1 ):
        $loadDefault = true;
        $aGas12 = GasMaterials::getByMaterialType(GasMaterialsType::$SETUP_PRICE_GAS_12);
        $aVatTu = GasMaterials::getByMaterialType(GasMaterialsType::$SETUP_PRICE_VATTU);
    endif;
endif;
$aZone = GasOrders::getTargetZone();
unset($aZone[GasOrders::ZONE_TARGET_MIENTRUNG]);
unset($aZone[GasOrders::ZONE_TARGET_TAY_NGUYEN]);
$index = 1;

?>

<h1>Thiết lập giá gas hộ gia đình</h1>


<div class="search-form" style="">
    <?php include "_search_12.php"; ?>
</div><!-- search-form -->
<?php include "SetupGas12Form.php"; ?>