<div class="form">
    <?php
        $LinkNormal     = Yii::app()->createAbsoluteUrl('admin/usersPrice/index');
        $LinkPrice     = Yii::app()->createAbsoluteUrl('admin/usersPrice/index', ['type'=> UsersPrice::TYPE_BELLOW_PRICE]);
        $LinkQuotePrice = Yii::app()->createAbsoluteUrl('admin/usersPrice/index', ['type'=> UsersPrice::TYPE_QUOTE_PRICE]);
    ?>
    <h1>Danh Sách <?php echo $this->pluralTitle; ?>
        <a class='btn_cancel f_size_14 <?php echo (!isset($_GET['type'])) ? "active":"";?>' href="<?php echo $LinkNormal;?>">Normal</a>
        <a class='btn_cancel f_size_14 <?php echo isset($_GET['type']) && $_GET['type']==UsersPrice::TYPE_BELLOW_PRICE ? "active":"";?>' href="<?php echo $LinkPrice;?>">Dưới NPP</a>
        <a class='btn_cancel f_size_14 <?php echo isset($_GET['type']) && $_GET['type']==UsersPrice::TYPE_QUOTE_PRICE ? "active":"";?>' href="<?php echo $LinkQuotePrice;?>">Tạo báo giá</a>
    </h1>
</div>