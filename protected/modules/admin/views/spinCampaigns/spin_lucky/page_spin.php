<?php
$mNum = new SpinNumbers();
$iRan = $mNum->randomLuckyNumber();
$urlSpinningAudio = Yii::app()->theme->baseUrl . '/admin/audio/spinning.mp3';

$aCamDetail = array();
if ($model->id) {
    $aCamDetail = SpinCampaignDetails::getArrayModelByArrayId($model->id);
}
$index = 1;
$aNumber = json_encode($aNumber = $mNum->arrNumberSpin());

$baseurl = Yii::app()->theme->baseUrl;
Yii::app()->clientScript->registerScriptFile($baseurl . '/admin/js/slick.min.js', CClientScript::POS_HEAD);
Yii::app()->clientScript->registerScriptFile($baseurl . '/admin/js/counter.js', CClientScript::POS_HEAD);
Yii::app()->clientScript->registerCssFile($baseurl . '/admin/css/counter.css');
Yii::app()->clientScript->registerCssFile($baseurl . '/admin/css/spin_normalize.css');
Yii::app()->clientScript->registerCssFile($baseurl . '/admin/css/spin_slick.css');
Yii::app()->clientScript->registerCssFile($baseurl . '/admin/css/spin_style.css');
Yii::app()->clientScript->registerCssFile($baseurl . '/admin/css/spin_style.css');
Yii::app()->clientScript->registerCssFile($baseurl . '/admin/css/popup.css');
//Yii::app()->clientScript->registerCssFile($baseurl . '/admin/sweetalert/sweetalert.css');
Yii::app()->clientScript->registerScriptFile($baseurl . '/admin/js/sweetalert.min.js', CClientScript::POS_HEAD);
?>
<!DOCTYPE html>
<html lang="vi">

    <head>
        <title>CÔNG TY CỔ PHẦN DẦU KHÍ MIỀN NAM</title>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!--<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no">-->

        <link href="https://fonts.googleapis.com/css?family=Roboto+Condensed:400,400i,700,700i&display=swap"
              rel="stylesheet">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.7.2/animate.min.css">
    </head>

    <body>
        <!--<audio id="audio-spinning" src="<?php // echo $urlSpinningAudio;                         ?>" preload="auto"></audio>-->
        <!--        <div class="container-notice">
                    <p >Chúc Mừng Bạn</p>
                    <p >Đã Trúng Thưởng</p>
        
                </div>       -->

        <div class="main">
            <div class="inner">
                <div class="container">
                    <div class="top">
                        <div class="ttl">CHƯƠNG TRÌNH XỔ SỐ<span class="logo"><img src="<?php echo Yii::app()->theme->baseUrl . '/admin/images/spin_logo.svg' ?>" alt=""></span></div>
                        <div class="box box-date">
                            <div class="wrap">
                                <div class="date">
                                    <span class="txt-1"><b><i>Giờ</i></b></span>
                                    <span class="txt-2"><b class="timer"><?php echo date("H:i") ?></b></span>
                                </div>
                            </div>
                            <div class="wrap">
                                <div class="date">
                                    <span class="txt-1"><b><i>Ngày</i></b></span>
                                    <span class="txt-2"><b class="date-timer"><?php echo date("d/m/Y") ?></b></span>
                                </div>
                            </div>
                            <!--                        <div class="wrap">
                                                        <div class="date hour">
                                                            <span class="txt-1"><b><i>Giờ</i></b></span>
                                                            <span class="txt-2">15:00</span>
                                                        </div>
                                                    </div>-->

                        </div>
                    </div>

                    <div class="content">
                        <div class="ttl-2">
                            <span class="js-spinning-title">Đang quay giải: </span> 
                            <span class="rank"></span>
                        </div>
                        <div class="number counters"> 
                            <!--<div class="counters">-->
                            <?php for ($i = 0; $i < 8; $i++) { ?>
                                <div><span class="number<?php echo $i ?> counter">0 </span></div>
                            <?php } ?>
                            <input type="hidden" value="0" class="numberWinning" />
                            <?php // echo $form->hiddenField($model,'number',['class'=>'numberWinning','name'=>'SpinLucky']);  ?>
                            <div class="btn btn2" 
                                 data-numberWin="0" 
                                 data-id="0" 
                                 data-rank="0"
                                 data-oldnum= "0"
                                 > 
                                CHỜ<br>QUAY
                            </div>
                            <!--<div class="btn btn2">ĐANG<br>QUAY</div>-->
                            <!--<div class="btn btn3">KẾT<br>THÚC</div>-->
                        </div>
                        <div class="sodaquay" id="spin-form-1">

                            <span>Số người tham gia: <strong><?php echo $model->getLiveUser() ?></strong></span>
                            <span>Số lần đã quay: <span><b><?php echo count($aCamDetail); ?></b> / <?php echo $model->getQtySpin(); ?></span></span>
                            <!--                            <div class="wrap">
                                                            <div class="date hour">
                                                                <span class="txt-1"><b><i>Số người tham gia</i></b></span>
                                                                <span class="txt-2"><?php echo $model->getUserTotal() ?></span>
                                                            </div>
                                                        </div>
                                                        <div id="spin-form-1" class="date active">
                                                            <span class="txt-1 txt-3"><b><i>Số lần đã quay</i></b></span>
                                                            <span class="txt-4"><?php echo count($aCamDetail) . '/' . $model->getQtySpin(); ?></span>
                                                        </div>-->

                        </div>
                        <div id="spin-form-2" class="sodaquay2">
                            <div class="date">
                                <span class="txt-1 txt-3 txt-5"><b><i>Các số đã quay</i></b></span>
                                <div class="itemslider">
                                    <div class="itemslideritem active" >
                                        <div class="sroll-item" >
                                            <?php foreach ($aCamDetail as $key => $mDetail): ?>
                                                <span>
                                                    <strong class="index-spined_number"><?php echo $index++; ?></strong> <b><?php echo $mDetail->getNumber(); ?></b>
                                                </span>
                                            <?php endforeach; ?>


                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div id="spin-form-3" class="boxresufl">
                            <?php
                            $numberAward = 0;
                            foreach ($model->rCampaignDetail as $key => $value) {
                                $numberAward++;
                                $style = empty($value->getNumber()) ? 'style="color:white;"' : '';
                                ?>
                                <div    class="js-award-item item<?php echo (!empty($value->number)) ? ' havebg' : '' ?>"
                                        id="item_rank<?php echo $value->rank ?>_id<?php echo $value->id ?>" 
                                        data-id ="<?php echo $value->id ?>"
                                        data-rank ="<?php echo $value->rank ?>">
                                    <div>
                                        <div class="wrapbox" <?php echo $style; ?>>
                                            <p><span class="item-rank"><?php echo $value->getRank() ?></span>: <span class="item-number"><?php echo $value->getNumber() ?></span></p>
                                            <p class="user-detail"><?php echo $value->getUser() ?></p>
                                        </div>
                                    </div>

                                </div>
                                <?php
                            }
                            ?>

                        </div>
                    </div>

                </div>
                <div class="footer">
                    <div class="container">
                        <img src="<?php echo Yii::app()->theme->baseUrl . '/admin/images/spin_logo2.svg' ?>" alt="" />
                    </div>
                </div>
            </div>
            <div class="modal">
                <div class="modal-overlay modal-toggle"></div>
                <div class="modal-wrapper modal-transition">
                    <div class="modal-body">
                        <div class="pyro"><div class="before"></div><div class="after"></div></div>
                        <div class="modal-content">
                            <h3>Chúc Mừng!!!</h3>
                            <h4 class="m-award-user"></h4>
                            <p class="m-award-rank"></p>
                            <span class="icon"><img src="<?php echo Yii::app()->theme->baseUrl . '/admin/images/check.png'?>" alt=""></span>
                        </div>
                    </div>
                </div>
            </div>

            <script>
                 $('.modal-toggle').on('click', function(e) {
                    e.preventDefault();
                    $('.modal').toggleClass('is-visible');
                    $('body').toggleClass('modalbd');
                 });
                myTimer();
                var aNumber             = <?php echo $aNumber; ?>;
                var elmSpinnig          = ''; // element giải đang quay
                var secondAnimation     = 30;
//                var secondAnimation     = 1;
                var numberAward = <?php echo $numberAward; ?>;
                var currentWinningUser = '';
                const STATUS_BEGIN_SPIN = 1; // bat dau quay
                const STATUS_SPINNING = 2; // dang quay
                const STATUS_END_SPIN = 3; // quay xong
                const CLASS_ANIMATION = 'animated infinite tada slow';
                InitScrollBox();

                //    Event click vào giải để quay
                onClickAward();

                //    Click vao nut quay so
                $(document).on('click', '.btn', function () {
                    if ($(this).hasClass('btn2')) { // return neu dang quay so
                        if ($(this).text().trim() !== 'ĐANGQUAY') {
                            notice('Oops!', 'Xin vui lòng chọn giải để quay!', 'info');
                        }
                        return false;
                    }
                    fnClickSpin($(this));
                });

                //    Click vao nut quay so
                function fnClickSpin(elm) {
//                    var award = $('.rank').text();                    
//                    if (award == '') { // alert neu chua chon giai
//                        alert('Xin vui lòng chọn giải để quay!');
//                        return false;
//                    }
                    offClickAward();
                    var numberWinning = aNumber[Math.floor(Math.random() * aNumber.length)];
                    if (aNumber.length == 0) {
                        $('.numberWinning').val(0);
                    } else {
                        for (var i = 0; i < 8; i++) {
                            $('.number' + i).text(numberWinning.charAt(i));
                        }

                        $('.numberWinning').val(numberWinning);
                        $('.btn').data('numberWin', numberWinning);
                        var index = aNumber.indexOf(numberWinning);
                        if (index != -1) {
                            aNumber.splice(index, 1);
                            if ($('.btn').data('oldnum') != '0' && aNumber.indexOf($('.btn').data('oldnum')) == -1) {
                                aNumber.push($('.btn').data('oldnum'));
                            }
                        }
                    }
                    var cRank = $('.btn').data('rank');
                    var cID = $('.btn').data('id');
                    var cNumber = $('.btn').data('numberWin');
                    beginAnimation(cRank, cID, cNumber); // run animation -> save data and reload ui


                }

                // save data and load ajax
                function fnOnSpin(rank, id, number) {
                    let campaignId = <?php echo $model->id ?>;
                    let url = '<?php echo Yii::app()->createAbsoluteUrl('admin/spinCampaigns/SpinLucky', ['id' => $model->id, 'onSpin' => 1]); ?>';
                        
                    $.post(url, {number: number, campaign_id: campaignId, rank: rank, id: id}, function (data) {
                        fnLoadAjax();
                        if (data != '') {
                            aNumber = JSON.parse(data);
                        }
                    });
                    
                }

                // Load ajax so lan da quay, cac so da quay, ds giai
                function fnLoadAjax() {
                    $.get(location.href, function (data) {
                        var award_user = '';
                        var award_rank = '';
                        for (i = 1; i < 4; i++)
                        {
                            var elmContainer = $('#spin-form-' + i);
                            var newUI = $(data).find('#spin-form-' + i).children();
                            var indexItemActive = -1;
                            if (elmContainer.hasClass('boxresufl')) { // get award is spinning
                                var indexItemActive = elmContainer.find('.js-award-item.animated').index();
                            }

                            elmContainer.empty().append(newUI); // reload new UI

                            if (indexItemActive >= 0) { // assign class active to spinning award
//                                elmContainer.find('.js-award-item').eq(indexItemActive).addClass(CLASS_ANIMATION);
                                var elm_award = elmContainer.find('.js-award-item').eq(indexItemActive);
                                award_user = elm_award.find('.user-detail .award-kh').text();
                                award_rank = elm_award.find('.wrapbox .item-rank').text();

                                if (typeof (Storage) !== "undefined") {

                                    sessionStorage.removeItem('userAward');
                                    sessionStorage.setItem('userAward', award_user);
                                } else {
                                    alert('Trình duyệt của bạn đã quá cũ. Hãy nâng cấp trình duyệt ngay!');
                                }
                            }

                        }                       
                            showEnd(award_user, award_rank);
                            $('.boxresufl').slick('slickGoTo', parseInt(indexItemActive));                      
                        $(".boxresufl").removeClass('slick-initialized');     //reset slick                  
                        InitScrollBox();
                    });

                }
                function sleep(ms) {
                    return new Promise(resolve => setTimeout(resolve, ms));
                }

                // run animation -> save data and reload ui
                async function beginAnimation(rank, id, number) {
                    changeUIButton(STATUS_SPINNING);// change ui button when spinnung

                    runCounterAnimation(secondAnimation); // run animation
                    $("span[data-value]").last().on("webkitTransitionEnd otransitionend oTransitionEnd msTransitionEnd transitionend",
                            function () {                                
                                fnOnSpin(rank, id, number);       // save and reload ui 
                                changeUIButton(STATUS_END_SPIN);    // change UI button when spin is end
                                onClickAward();                     // bật lại event chọn giải 
                            });
                    
//                    var temp = 1;
//                    var timeSleep = ((secondAnimation - temp) <= 0) ? 0 : (secondAnimation - temp);
//                    await sleep(timeSleep * 1000); // wait until the end of animation                     
//                    fnOnSpin(rank, id, number); // save and reload ui                    
//                    await sleep((secondAnimation - timeSleep) * 1000);
//                    changeUIButton(STATUS_END_SPIN);    // change UI button when spin is end
//                    onClickAward();                     // bật lại event chọn giải 


                }

                // change text and cursor button when spin begin or end
                function changeUIButton(status) {
                    switch (status) {
                        case STATUS_SPINNING:
                            $('.btn').addClass('btn2'); // dang quay
                            $('.btn').html('ĐANG<br>QUAY');
                            break;
                        case STATUS_END_SPIN:
//                            $('.btn').removeClass('btn2'); // quay xong
                            $('.btn').html('CHỜ<br>QUAY');
                            break;

                        default:
                            break;
                    }
                }

                // change text and cursor button when spin begin or end
                function playAudio(status) {
                    var elmAudio = document.getElementById('audio-spinning');
                    switch (status) {
                        case STATUS_SPINNING:
                            elmAudio.play();
                            break;
                        case STATUS_END_SPIN:
                            elmAudio.pause();
                            break;

                        default:
                            break;
                    }
                }

                // handle scroll dragable
                function InitScrollBox() {
                    var itemSlide = (numberAward < 5) ? numberAward : 5;

                    $('.sroll-item').slick({
//                        slidesToShow: 4,
                        arrows: false,
                        infinite: false,
                        slidesToScroll: 7,
                        variableWidth: true
                    });
                    $('.boxresufl').slick({
                        slidesToShow: itemSlide,
                        arrows: false,
                        infinite: false,
                        slidesToScroll: itemSlide,

                    });

                }
                function showEnd(award_user, award_rank) {
                    $('.modal').toggleClass('is-visible');
                    $('body').toggleClass('modalbd');
                   
                    $('.modal-content .m-award-user').html(award_user);
                    $('.modal-content .m-award-rank').html(`Là người may mắn nhận thưởng <b>${award_rank}<b>!`);
//                    var firework = '<div class="pyro"><div class="before"></div><div class="after"></div></div>';
//                    notice('Chúc Mừng!', firework + `<b>${award_user}</b><br/> đã là người may mắn trúng <b>${award_rank}<b>!`, 'success');
                    addAudio(SRC_AUDIO_AWARD, 'audio-award');
                }

                function notice(title, content, status) {
                    let wrapper = document.createElement('div');
                    wrapper.setAttribute('class', 'notice-margin');
                    wrapper.innerHTML = content;
                    swal({
                        title: title,
                        content: wrapper,
                        icon: status,
                        button: false,
                    });
                }
                function onClickAward() {
                    $(document).on('click', '.js-award-item', function () {
                        // change text 
                        var currentWinner = $(this).find('.user-detail').text();
                        var spinningTitle = '';
                        if (currentWinner != '') {
//                            return false; // ko cho quay lai nua
                            spinningTitle = 'Đang quay lại giải:';
                        } else {
                            spinningTitle = 'Đang quay giải:';
                        }
                        $('.js-spinning-title').text(spinningTitle);

                        // effect award item
                        resetNumber();
                        $('.js-award-item').removeClass(CLASS_ANIMATION);
                        $(this).addClass(CLASS_ANIMATION);
                        $('.btn').removeClass('btn2');
                        $('.btn').html('QUAY<br>SỐ');

                        var str = $(this).attr('id');
                        $('.rank').text($('#' + str).find('.item-rank').text());
                        $('.btn').data('rank', $(this).data('rank'));
                        $('.btn').data('id', $(this).data('id'));
                        $('.btn').data('oldnum', $('#' + str).find('.item-number').text());
                        elmSpinnig = $(this);
                    });
                }
                function offClickAward() {
                    $(document).off('click', '.js-award-item');
                }
                //reset 00000
                function resetNumber() {
                    $(".counters .counter").children().remove();
                    $(".counters .counter").text('0');
                }
                // timer
                function myTimer() {

                    setInterval(function () {
                        var date = new Date();
                        var hours = date.getHours();
                        var minutes = date.getMinutes();
                        var day = date.getDate();
                        var month = date.getMonth() + 1;
                        var year = date.getFullYear();
                        $(".timer").text(`${hours >= 10 ? hours : "0" + hours}:${minutes >= 10 ? minutes : "0" + minutes}`);
                        $(".date-timer").text(`${day >= 10 ? day : "0" + day}/${month >= 10 ? month : "0" + month}/${year}`);
                    }, 1000);
                }

            </script>
    </body>

</html>