<?php
$this->breadcrumbs = array(
    $this->pluralTitle => array('index'),
    ' ' . $this->singleTitle . ' : ',
);

$menus = array(
    array('label' => "$this->pluralTitle Management", 'url' => array('index')),
);
$this->menu = ControllerActionsName::createMenusRoles($menus, $actions);
Yii::app()->clientScript->registerScript('search', "
    $('#spin-form-form').submit(function(){
            $.fn.yiiGridView.update('spin-grid', {
                    url : $(this).attr('action'),
                    data: $(this).serialize(),
                    type:'POST'
                   
            });
            return false;
    });
");
?>
<h1 style='color: red; text-align: center;'>QUAY SỐ MAY MẮN <b>CHƯƠNG TRÌNH KHUYẾN MÃI <?php echo $model->getName() ?> </b></h1>

<div class = 'row'>
    <div class='spin-form float_l' style="width:65%">   
        <?php
        echo $this->renderPartial('spin_lucky/_form_spin', array(
            'model' => $model,
        ));
        ?>
    </div>
    <div class='float_l winning-form' style="width:35%">
        <h2 style='text-align: center;'>THÔNG TIN GIẢI THƯỞNG</h2>
        <h3 class='title-info'>Chi tiết thông tin giải thưởng</h3>  
        <?php echo MyFormat::BindNotifyMsg(); ?>    
        <?php
        $this->widget('zii.widgets.grid.CGridView', array(
            'id'                => 'spin-grid',
            'dataProvider'      => SpinCampaignDetails::model()->searchWinning($model->id),
            'afterAjaxUpdate'   =>'function(id, data){fnLoadAjax();}',
            'template'          => '{pager}{items}{pager}',
            'pager'             => array(
               'maxButtonCount' => CmsFormatter::$PAGE_MAX_BUTTON,
            ),
            'enableSorting'     => false,
            //'filter'=>$model,
            'columns'           => array(
                array(
                    'header'            => 'STT Giải',
                    'type'              => 'raw',
                    'value'             => '$this->grid->dataProvider->pagination->currentPage * $this->grid->dataProvider->pagination->pageSize + ($row+1)',
                    'headerHtmlOptions' => array('width' => '10px', 'style' => 'text-align:center;'),
                    'htmlOptions'       => array('style' => 'text-align:center;')
                ),
                array(
                    'name'  => 'Tên khách hàng',
                    'type'  => 'html',
                    'value' => '$data->getUserName()'
                ),
                array(
                    'name'  => 'Số trúng thưởng',
                    'value' => '$data->getNumber()',
                    'type'  => 'raw'
                ),
                array(
                    'name'  => 'SĐT',
                    'value' => 'empty($data->rUsers)?"":$data->rUsers->getPhone()'
                ),
                array(
                    'name'  => 'Giải thưởng',
                    'value' => '$data->getRank()'
                ),
//                array(
//                    'name'  => 'Cơ Cấu Giải Thưởng',
//                    'type'  => 'html',
//                    'value' => '$data->getSummaryTableDetails()'
//                ),
            )
        ));
        ?>
    </div> 
    </br>
</div>
<script>
    //ajax update status after create success award
    function fnLoadAjax() {
        $.get(location.href, function(data){
            for (i=1; i<=4; i++)
            {
                $('#spin-form-'+i).empty().append( $(data).find('#spin-form-'+i).children());
            }
        });x
    }
    
</script>   