<?php 
    $mNum = new SpinNumbers();
    $iRan = $mNum->randomLuckyNumber();
    
    $aCamDetail = array();
    if($model->id){
        $aCamDetail = SpinCampaignDetails::getArrayModelByArrayId($model->id);
    }
    $index=1;
?>
<div class="wide form">
    <div id='spin-form-1'>
        <h2 style='text-align: center;'>VÒNG QUAY MAY MẮN</h2>
        <div class = 'row'>
            <div class='float_l' style="font-size:16px; width:20%">
                SỐ THAM GIA: <b><?php echo $model->getUserTotal()?></b>
            </div>
            <div class='float_l' style="font-size:16px; width:60%">
                SỐ ĐÃ QUAY: <b><?php echo count($aCamDetail).'/'.$model->getQtySpin();?></b>
            </div>
            <div class='float_l' style="font-size:16px; width:20%">
                NGÀY: <b><?php echo $model->getCampaignDate()?></b>
            </div>
        </div>
    </div>
        </br>
    <?php $form=$this->beginWidget('CActiveForm', array(
            'id'                    =>'spin-form-form',
            'enableAjaxValidation'  =>false,
            'htmlOptions'           => array('enctype' => 'multipart/form-data'),
    )); ?>
    <div class="row" id='spin-form-2' >
        <h2>Đang quay giải: <?php echo $form->dropdownList($model, 'rank', SpinCampaignDetails::getArrayCurrentRankByCampaignId($model->id), ['class'=>'w-200', 'name'=>'Rank', 'onchange'=>'fnOnChange(this)', 'data-model_id'=>"$model->id"]) ?></h2>
        <h3 id='spin-form-2_1'>Lần quay thứ: <span></span></h3>
    </div>
    <center>
        <div class="row">
            <?php for($i = 0; $i< 8; $i++){ ?>
            <input class="number number<?php echo $i ?>" disabled="disabled" value ="0"/>
            <?php } ?>
            <?php echo $form->hiddenField($model,'number',['class'=>'numberWinning','name'=>'SpinLucky']); ?>
        </div>
        <div class="row" id='spin-form-3'>
            <?php if (count($aCamDetail) >= $model->getQtySpin()): ?>
                <p style="color: red; font-size:15px;"><b> VÒNG QUAY KẾT THÚC</b></p>
            <?php else: ?>
                <div class="row" style="text-align: center">
                <a class='btn_cancel btnSpin'>Quay số</a> 
                <?php $this->widget('bootstrap.widgets.TbButton', array(
                'buttonType'    =>'submit',
                'label'         =>'Quay số', // if has 2 button:  'label' => 'Lấy số'; delete row  'htmlOptions'
                'type'          =>'null', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
                'size'          =>'small', // null, 'large', 'small' or 'mini'
                'htmlOptions'   => array('class'=>'btnSubmit'),

                )); ?>
                </div>
            <?php endif;?>
        </div>
    </center>

    <h3>Các số đã quay: </h3>
        <div id='spin-form-4'>
            <?php foreach($aCamDetail as $key => $mDetail): ?>
                <label style="font-size:15px;" > <?php echo $index++ . '. <b>'.$mDetail->getUserName() . '-'.$mDetail->getNumber().'</b>'?></label>
            <?php endforeach;?>
        </div>
    <?php $this->endWidget(); ?>
</div><!-- spin-form -->
<style>
    .number{
        font-size: 50px;
        height: 70px; 
        margin: 0px 0px 10px 0px !important; 
        width:40px; 
        text-align:center;
        border-radius:10px;
        border:none;
        background-color: #81BEF7 !important;
        color: white !important;
    }
    .btnSpin{
        background-color: #0080FF !important;
        color: white;
        border: none;
        padding: 5px 10px;
        border-radius: 5px;
        font-weight: bold;
        cursor: pointer;
    }
</style>
<script>
    var aNumber = <?php echo json_encode($aNumber = SpinNumbers::arrNumberSpin());?>;
    $(document).on('click', '.btnSpin', function(event){
        var numberWinning = aNumber[Math.floor(Math.random()*aNumber.length)];
        console.log('abc' + numberWinning);
        if(aNumber.length == 0){
            $('.numberWinning').val(0);
        }else{
        // $('.number0').val(numberWinning.charAt(0));
            for(var i = 0; i < 8; i++){
            $('.number'+i).val(numberWinning.charAt(i));
            $('.numberWinning').val(numberWinning);
        }
            var index = aNumber.indexOf(numberWinning);
            if(index != -1){
                aNumber.splice(index,1);
            } 
          }
    });
    function fnOnChange(obj) {
        let rank = obj.value;
        let modelId = $(obj).data('model_id');
        let url ='<?php echo  Yii::app()->createAbsoluteUrl('admin/ajax/OnChangeSpin');?>';
        $.post(url, {rank: rank, model_id: modelId}, function (data) {
           console.log(data);
           $("#spin-form-2_1").find('span').html(data);
        });
    }
</script>   