<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'                    =>'spin-campaigns-form',
	'enableAjaxValidation'  =>false,
        'htmlOptions'           => array('enctype' => 'multipart/form-data'),
)); ?>

    <p class="note">Dòng có dấu <span class="required">*</span> là bắt buộc.</p>
    <?php echo MyFormat::BindNotifyMsg(); ?>
    
        		
        
    <div class="row">
        <?php echo $form->labelEx($model,'name'); ?>
        <?php echo $form->textField($model,'name',['class'=>'w-300']); ?>
        <?php echo $form->error($model,'name'); ?>
    </div>
    <div class="row">
        <?php echo $form->label($model,'campaign_date'); ?>
        <?php 
            $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                'model'         =>$model,        
                'attribute'     =>'campaign_date',
                'options'       =>array(
                    'showAnim'          =>'fold',
                    'dateFormat'        => MyFormat::$dateFormatSearch,
                    'minDate'           => '0',
                    'changeMonth'       => true,
                    'changeYear'        => true,
                    'showOn'            => 'button',
                    'buttonImage'       => Yii::app()->theme->baseUrl.'/admin/images/icon_calendar_r.gif',
                    'buttonImageOnly'   => true,                                
                ),        
                'htmlOptions'   =>array(
                    'class'     =>'w-300 campaign_date',
                    'size'      =>'w-16',
                    'readOnly'  =>1
                ),
            ));
        ?>     		
    </div>
    <div class="row">
        <?php echo $form->label($model, 'type') ?>
        <?php echo $form->dropdownList($model, 'type', SpinCampaigns::getArrayType(), ['class'=>'w-300']) ?>
    </div>
    <div class="row">
        <?php echo $form->label($model, 'qty_spin') ?>
        <?php echo $form->textField($model, 'qty_spin', ['disabled' => 'true', 'class'=>'qty_spin']) ?>
    </div>
    <div class="row">
        <?php echo $form->labelEx($model,'description'); ?>
        <?php echo $form->textArea($model,'description',array('rows'=>6, 'cols'=>50, 'class'=>'w-300')); ?>
        <?php echo $form->error($model,'description'); ?>
    </div>
    <?php include "_form_detail.php";?>
    <div class="row buttons" style="padding-left: 141px;">
                    <?php $this->widget('bootstrap.widgets.TbButton', array(
        'buttonType'    =>'submit',
        'label'         =>$model->isNewRecord ? 'Create' :'Save',
        'type'          =>'null', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
        'size'          =>'small', // null, 'large', 'small' or 'mini'
    )); ?>	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->
<script>
    $(document).ready(function(){
        $('.form').find('button:submit').click(function(){
            $.blockUI({ overlayCSS: { backgroundColor: '#fff' } }); 
        });
        
    });
</script>