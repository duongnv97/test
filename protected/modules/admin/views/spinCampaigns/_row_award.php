<?php 

    
?>

<h3 class='title-info'>Chi tiết phần thưởng</h3>
<div class="clr"></div>
<div class="row">
    <?php echo $form->labelEx($model,'Tên phần thưởng'); ?>
    <?php echo $form->textField($model,'materials_name',array('size'=>32,'placeholder'=>'Nhập mã vật tư hoặc tên vật tư')); ?>
    <!--<em class="hight_light item_b">Chú Ý: Có thể chọn nhiều vật tư trong cùng 1 lần tạo </em>-->
    <?php echo $form->error($model,'materials_name'); ?>
    <div class="clr"></div>    		
</div>

<div class="row">
    <table class="materials_table hm_table">
        <thead>
            <tr>
                <th class="item_c">STT</th>
                <th class="item_c">Mã</th>
                <th class="item_name item_c">Tên</th>
                    <th class="w-30 item_c">SLSP/giải</th>
                <th class=" item_c">Tên giải</th>
                <th class="item_unit last item_c">Xóa</th>
            </tr>
        </thead>
        <tbody>
            
        <?php
        foreach($model->aDetail as $key=>$item):
        ?>
            
            <?php   
            $mMaterial = $item->rMaterial;
            ?>
            
            <?php if($mMaterial): ?>
            <tr class="materials_row">
                <input name="materials_id[]" value="<?php echo $mMaterial->id;?>" class="materials_id" type="hidden">
                <td class="item_c order_no"><?php echo ($key+1);?></td>
                <td><?php echo $mMaterial->materials_no;?></td>
                <td class="item_l"><?php echo $mMaterial->name;?></td>
                <td class="item_c "><input name="materials_qty[]" value="<?php echo $item->qty_material;?>" class="item_c item_qty materials_qty number_only_v1 w-30 " type="text" size="6" maxlength="9"></td>
                <td class="item_c">
                    <select name="rank[]">
                        <?php foreach(SpinCampaignDetails::getArrayRank() as $key=>$rank){ echo $rank?>
                        <option <?php echo ($key == $item->attributes['rank']) ? 'selected' : '' ?> value="<?php echo $key ?>"><?php echo $rank ?>
                        </option>
                        <?php } ?>
                    </select>
                </td>
                <td class="item_c last"><span class="remove_icon_only"></span></td>
            </tr>
            <?php endif;?>
        <?php endforeach;?>
        </tbody>
        <tfoot>
            <tr class="materials_row">
                <td class="item_b item_r f_size_16" colspan="3">Tổng cộng</td>
                <td class="item_b item_c total_qty f_size_16"></td>
                <td class=""></td>
                <!--<td class="item_b item_r total_amount f_size_16"></td>-->
                <td class="last" colspan="6"></td>
            </tr>
        </tfoot>
    </table>        
</div>

<script>
    $(document).ready(function(){
        fnInitAutocomplete();    // load auto materials  
        fnRefreshOrderNumber();  // 
        fnBindRemoveIcon();     // action for button-delete: detele row (table)  
//        fnAfterRemoveIcon();
        fnCalcTotalPay();       
        fnBindSomeLiveChange(); // loading data when quatity or price change
    });

    /**
    * @Author: ANH DUNG Jun 25, 2016
    * @Todo: init autocomplete
    */
    function fnInitAutocomplete(){
        var availableMaterials = <?php echo MyFunctionCustom::getMaterialsJson(array('created_by'=>$model->created_by)); ?>;
        $( "#SpinCampaigns_materials_name" ).autocomplete({
            source: availableMaterials,
            close: function( event, ui ) { $( "#SpinCampaigns_materials_name" ).val(''); },
            select: function( event, ui ) {
                var class_item = 'materials_row_'+ui.item.id;
                if($('.'+class_item).size()<1)
                    fnBuildRowMaterial(event, ui); 
            }
        });

    }         

    function fnBindSomeLiveChange(){
        $('.item_qty, .item_price, .OrderTypeAmount').live('change', function(){
            fnCalcTotalPay();
        });
    }
    
    function fnBuildRowMaterial(event, ui){
        
        var PriceBinhBoByMonth = "";
        var class_item = 'materials_row_'+ui.item.id;
        var _tr = '';
            _tr += '<tr class="materials_row">';
                _tr += '<td class="item_c order_no"></td>';
                _tr += '<td>';
                _tr += ui.item.materials_no;
                _tr += '<input name="materials_id[]" value="'+ui.item.id+'" class="materials_id" type="hidden">';
                _tr += '</td>';
                
                _tr += '<td class="item_l">'+ui.item.name+'</td>';
                _tr += '<td class="item_c ">';
                 _tr += '<input name="materials_qty[]" value="1" class="item_c item_qty w-30 materials_qty number_only_v1 '+class_item+'" type="text" size="6" maxlength="9">';
                _tr += '</td>';
//                _tr += '<td class="item_c ">';                
//                _tr += '<input <?php // echo $readonlyPrice;?> name="price[]" value="'+ui.item.price+'" class="item_price number_only number_only_v1 w-60" type="text" maxlength="9">';
//                _tr += '<input name="materials_id[]" value="'+ui.item.id+'" class="materials_id" type="hidden">';             
//                _tr += '<div class="help_number"></div>';
//                _tr += '</td>';

                _tr += '<td class="item_c">'
                _tr += '<select name="rank[]">'
                <?php foreach(SpinCampaignDetails::getArrayRank() as $key=> $rank){ ?>
                    _tr += '<option value = <?php echo $key; ?> ><?php echo $rank; ?></option>'
                <?php } ?>
                _tr += '</select>'
//                _tr += '<td class="item_r item_amount  f_size_15 item_b"></td>';
                _tr += '<td class="item_c last"><span class="remove_icon_only"></span></td>';
            _tr += '</tr>';
        $('.materials_table tbody').append(_tr);
        fnRefreshOrderNumber();
        bindEventForHelpNumber();
        fnCalcTotalPay();
    }
    

    function fnCalcTotalPay(){
        var total_amount    = 0;
        var total_qty       = 0;
//        var orderType       = getOrderType();
      
        $('.materials_table tbody .materials_row').each(function(){
           var item_qty = $(this).find('.item_qty').val()*1; 
           total_qty+=item_qty;
           var item_price = $(this).find('.item_price').val()*1; 
           var  amount = parseFloat(item_qty*item_price);
           amount = Math.round(amount * 100) / 100;
           total_amount += amount;
                      console.log(amount);
           $(this).find('.item_amount').text(commaSeparateNumber(amount)); 
        });
//        
        total_amount = parseFloat(total_amount);
        total_amount = Math.round(total_amount * 100) / 100;
        $('.total_hide').val(total_amount);
        $('.total_amount').text(commaSeparateNumber(total_amount));
        total_qty = parseFloat(total_qty);
        total_qty = Math.round(total_qty * 100) / 100;
        $('.total_qty').text(commaSeparateNumber(total_qty));
        $('.qty_spin').val(total_qty);
        $('.qty_spin').text(commaSeparateNumber(total_qty));
        //fnGrandTotal();
    }

    function fnAfterRemoveIcon(){
        fnCalcTotalPay();
    }
    

</script>