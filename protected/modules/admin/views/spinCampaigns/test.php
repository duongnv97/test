<!DOCTYPE html>
<html lang="vi">

<head>
    <title>CÔNG TY CỔ PHẦN DẦU KHÍ MIỀN NAM</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no">

    <link href="css/normalize.css" rel="stylesheet">
    <link href="css/slick.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Roboto+Condensed:400,400i,700,700i&display=swap"
        rel="stylesheet">
</head>

<body>
    <div class="main">
        <div class="inner">
            <div class="container">
                <div class="top">
                    <div class="ttl">CHƯƠNG TRÌNH XỔ SỐ<span class="logo"><img src="img/logo.svg" alt=""></span></div>
                    <div class="box">
                        <div class="date">
                            <span class="txt-1">Ngày</span>
                            <span class="txt-2">01/09/2019</span>
                        </div>
                        <div class="date hour">
                            <span class="txt-1">Giờ</span>
                            <span class="txt-2">15:00</span>
                        </div>
                    </div>
                </div>
                <div class="content">
                    <div class="ttl-2">Đang quay số giải: xxx xxxx xxx xxx</div>
                    <div class="number">
                        <div><span>0</span></div>
                        <div><span>0</span></div>
                        <div><span>0</span></div>
                        <div><span>0</span></div>
                        <div><span>0</span></div>
                        <div><span>0</span></div>
                        <div><span>0</span></div>
                        <div><span>0</span></div>
                        <div class="btn">QUAY<br>SỐ</div>
                        <!-- <div class="btn btn2">ĐANG<br>QUAY</div> -->
                        <!-- <div class="btn btn3">KẾT<br>THÚC</div> -->
                    </div>
                    <div class="sodaquay">
                        <div class="date active">
                            <span class="txt-1 txt-3">Số lần đã quay</span>
                            <span class="txt-4">3/10</span></div>
                    </div>
                    <div class="sodaquay2">
                        <div class="date">
                            <span class="txt-1 txt-3">Các số đã quay</span>
                            <div class="itemslider">
                                <div class="itemslideritem active">
                                    <div>
                                        <span>33676532</span>
                                        <span>33676532</span>
                                        <span>33676532</span>
                                        <span>33676532</span>
                                        <span>33676532</span>
                                        <span>33676532</span>
                                        <span>33676532</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="boxresufl">
                        <div class="item havebg">
                            <div>
                                <p>Giải 1: 45767945</p>
                                <p>Nguyễn Thế Châu</p>
                                <p>ĐT: 0909.123.xxx</p>
                            </div>

                        </div>
                        <div class="item">
                            <div>
                                <p>Giải 1: 45767945</p>
                                <p>Nguyễn Thế Châu</p>
                                <p>ĐT: 0909.123.xxx</p>
                            </div>
                        </div>
                        <div class="item">
                            <div></div>
                        </div>
                        <div class="item">
                            <div> </div>
                        </div>
                        <div class="item">
                            <div></div>
                        </div>
                    </div>
                </div>

            </div>
            <div class="footer">
                <div class="container">
                    <img src="img/logo2.svg" alt="" />
                </div>
            </div>
        </div>
        <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
        <script src="js/slick.min.js"></script>
        <script>
            $('.itemslider').slick({
                slidesToShow: 1,
                arrows: false,
                infinite: false,
                slidesToScroll: 1
            });
        </script>
</body>

</html>