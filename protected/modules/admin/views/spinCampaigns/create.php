<?php
$this->breadcrumbs=array(
	'Quản Lý '.$this->pluralTitle=>array('index'),
	'Tạo Mới '.$this->singleTitle,
);

$menus = array(		
        array('label'=>"Quản Lý $this->singleTitle", 'url'=>array('index')),
);
$this->menu= ControllerActionsName::createMenusRoles($menus, $actions);
?>
<div class="form">
<h1>Tạo mới <?php echo $this->pluralTitle; ?></h1>
</div>
<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>