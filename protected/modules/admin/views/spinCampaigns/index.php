<?php
$this->breadcrumbs=array(
	
);

$menus=array(
            array('label'=>"Tạo Mới ".$this->pluralTitle, 'url'=>array('create')),
);
$this->menu= ControllerActionsName::createMenusRoles($menus, $actions); //create url-buttons  

// register script and css, add libery for view   

Yii::app()->clientScript->registerScript('search', "  
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('search-grid', {
                url : $(this).attr('action'),
		data: $(this).serialize()
	});
	return false;
});
");
?>



<h1>Quản lý <?php echo $this->pluralTitle; ?></h1>
    <?php echo CHtml::link('Tìm Kiếm Nâng Cao', '#', array('class' => 'search-button')); ?>
&nbsp;&nbsp;&nbsp;&nbsp;
    <?php //    echo CHtml::link('Test', Yii::app()->createAbsoluteUrl('admin/NhanOrder/test'), array('class' => '',  'target'=>'_blank')); ?>
    <div class="search-form" style="display: none" >
        <?php
        $this->renderPartial('_search', array(
            'model' => $model,
        ));
        ?>
    </div><!-- search-form -->
<?php $this->widget('zii.widgets.grid.CGridView', array(
    'id'            =>'search-grid',
    'dataProvider'  =>$model->search(),
//        'afterAjaxUpdate'=>'function(id, data){ fnUpdateColorbox();}',
    'template'      =>'{pager}{summary}{items}{pager}{summary}',
//    'itemsCssClass' => 'items custom_here',
//    'htmlOptions'=> array('class'=>'grid-view custom_here'),
    'pager'         => array(
        'maxButtonCount'=>  CmsFormatter::$PAGE_MAX_BUTTON,
    ),
    'enableSorting' => false,
    //'filter'=>$model,
    'columns'       => array(
        array(
            'header'            => 'S/N',
            'type'              => 'raw',
            'value'             => '$this->grid->dataProvider->pagination->currentPage * $this->grid->dataProvider->pagination->pageSize + ($row+1)',
            'headerHtmlOptions' => array('width' => '10px','style' => 'text-align:center;'),
            'htmlOptions'       => array('style' => 'text-align:center;')
        ),
        array(
            'name'      => 'Tên chiến dịch' ,
            'type'      => 'html',
            'value'     => '$data->getName()'
        ),
        array(
            'name'      => 'Cơ cấu giải thưởng',
            'value'     => '$data->getSummaryTable()',
            'type'      => 'raw'
        ),
        array(
            'name'      => 'Ngày thực hiện chiến dịch',
            'value'     => '$data->getCampaignDate()'
        ),
        array(
            'name'      => 'Số lượt quay',
            'value'     => '$data->getQtySpin()'
        ),
        array(
            'name'      => 'Loại',
            'value'     => '$data->getType()'  
        ),
        array(
            'name'      => 'Số người tham gia',
            'value'     => '$data->getUserTotal()'
        ),
        array(
            'name'      => 'Mô tả',
            'value'     => '$data->getDescription()'
        ),
        array(
            'name'      => 'Ngày tạo',
            'value'     => '$data->getCreatedDate()',
        ),
        array(
            'name'      => 'Người tạo',
            'value'     => '$data->getCreatedBy()',
        ),
        array(
            'header'    => 'actions',
            'class'     => 'CButtonColumn',
            'template'  => ControllerActionsName::createIndexButtonRoles($actions,array('view','update','delete','spinLucky','NotifyWinning')),
            'buttons'   =>[
                'spinLucky'  => [
                    'label'=>'Quay số trúng thưởng',     //Text label of the button.
                    'url'=>'["SpinLucky", "id"=>$data->id]',       //A PHP expression for generating the URL of the button.   
                    'imageUrl'=>Yii::app()->theme->baseUrl . '/admin/images/icon/selling.png',
                    'visible'=>'$data->checkCanSpin()',
                    'options'=>array('class' => 'spin' ,'target' => '_blank'), //HTML options for the button tag..
                ],
                'NotifyWinning'=> [
//                    'visible'=> 'MyFormat::getCurrentRoleId()==ROLE_ADMIN',
                    'label'=>'Notify trúng giải cho KH',     //Text label of the button.
                    'url'=>'Yii::app()->createAbsoluteUrl("admin/SpinCampaigns/NotifyWinning",array("id"=>$data->id) )',
                    'imageUrl'=>Yii::app()->theme->baseUrl . '/admin/images/Upload.png',
                    'options'=>array('class' => 'update','onclick'=>'return confirm("Bạn có chắc muốn gửi notify app cho KH trúng giải trong chiến dịch này?");'), //HTML options for the button tag..
                ],
            ]
        )
    )
));
?>
<script>
    $(document).ready(function(){
        //fnSpinColorbox();
    });
//    function fnSpinColorbox(){    
//        $(".spin").colorbox({iframe:true,innerHeight:'500', innerWidth: '1050',close: "<span title='close'>close</span>"});
//    }
</script>