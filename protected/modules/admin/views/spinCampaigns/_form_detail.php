<h3 class='title-info'>Chi tiết phần thưởng</h3>
<div class="clr"></div>
<div class="row">
    <?php echo $form->labelEx($model,'Giải'); ?>
    <?php echo $form->dropdownList($model,'rank', SpinCampaignDetails::getArrayRank(),['empty'=>'Select', 'class'=>'item-l rank']); ?>
    <?php echo $form->error($model,'rank'); ?>
    <div class="clr"></div>    		
</div>
<!--<div class="row">
    <?php echo $form->labelEx($model,'Tên phần thưởng'); ?>
    <?php echo $form->textField($model,'materials_name',array('size'=>32,'placeholder'=>'Nhập mã vật tư hoặc tên vật tư')); ?>
    <em class="hight_light item_b">Chú Ý: Có thể chọn nhiều vật tư trong cùng 1 lần tạo </em>
    <?php echo $form->error($model,'materials_name'); ?>
    <div class="clr"></div>    		
</div>-->


<div class="row">
    <table  id="materials_table" class=" materials_table add_detail_table hm_table">
        <thead>
            <tr>
                <th class="item_c">STT</th>
                <th class="item_name item_c">Giải</th>
                <th class=" item_c">Số lượng giải</th>
                <th class="w-500 item_c">Chi tiết phần thưởng</th>
                <th class="item_unit last item_c">Xóa</th>
            </tr>
        </thead>
        <tbody class="body_add_detail">
        <?php foreach($model->aDetail as $key=>$item):?>
            <tr class="type_rank materials_row_<?php echo $item->rank ?>">
                <td class="item_c order_no_campaign">
                    <?php echo ($key+1);?>
                </td>
                <td class="item_c rank">
                    <?php echo $item->getRank()?>
                    <input name="SpinCampaignDetails[rank][]" value="<?php echo $item->rank ?>" type="hidden"/>
                </td>
                <td class="item_c">
                    <input name="SpinCampaignDetails[qty_award][]" value="<?php echo $item->qty_award; ?>" type="text" class="w-30 qty_award item_c item_qty qty_award number_only_v1"  size="3" maxlength="3"/>
                </td>
                <td style="padding: 10px !important; " class="item_l material_name">
                        <?php echo $form->textField($model,"materials_name",array("class"=>"m_name","style"=>"", "size"=>32,"placeholder"=>"Nhập mã vật tư hoặc tên vật tư")); ?>
                        <?php echo $form->error($model,'materials_name'); ?>
                        <?php $aMaterials = empty(json_decode($item->award_json, true))?array():json_decode($item->award_json, true);?>
                        <table style="width: 90%; margin:0 auto !important; margin-bottom: 10px !important;" class="materials_table details_table hm_table">
                            <thead>
                                <tr>
                                    <th class="item_name item_c">Tên</th>
                                    <th class="w-30 item_c">Số SP</th>
                                    <th class="item_unit last item_c">Xóa</th>
                                </tr>
                            </thead>
                            <tbody>
                        <?php foreach($aMaterials as $keyMaterial=>$material):?>
                        <tr class="details_row details_row_<?php echo $item->rank; ?> ">
                            <td class="item_l material_name">
                                <label><?php echo $material['name']; ?></label>
                                <input class="json" name="SpinCampaignDetails[<?php echo $item->rank ?>][award_json][]" value='{"id":<?php echo $material["id"];?>, "name":"<?php echo $material["name"];?>", "qty": <?php echo $material["qty"] ?>}' class="materials_id" type="hidden"> 
                            </td>
                            <td class="item_c">
                                <input name="SpinCampaignDetails[<?php echo $item->rank; ?>][materials_qty][]" value="<?php echo $material['qty']; ?>" type="text" class="item_c item_qty w-30 materials_qty number_only_v1"  size="3" maxlength="3"/>
                            </td>
                            <td class="item_c last"><span class="remove_icon_only"></span></td>
                        </tr>
                        <?php endforeach;?>
                            </tbody>
                        </table>
                </td>
                <td class="item_c last"><span class="remove_icon_only"></span></td>
            </tr>
        <?php endforeach;?>
            
        </tbody>
        <tfoot>
            <tr class="materials_foot">
                <td class="item_b item_r f_size_16" colspan="2">Tổng số giải</td>
                <td class="item_b item_c total_qty f_size_16"></td>
                <td class=""></td>
            </tr>
        </tfoot>
</table> 
</div>
<script>
$(document).ready(function(){
    fnAutoAddRow();
    fnRefreshOrderNumberCampaign(); 
    fnBindRemoveIcon();
    fnCalcTotalPay(); 
    fnBindSomeLiveChange(); // loading data when quatity or price change
    fnInitAutocomplete();
    fnChangeQtyMaterial();
});
$(document).on('input', '.qty_award', function(){
    fnCalcTotalPay();
});

/**
* @Author: NamLA Aug 24, 2019
* @Todo: add row when change rank
*/
function fnAutoAddRow(){
    $('#SpinCampaigns_rank').change(function(event, ui){
        var value_rank = $(".rank").children("option:selected").val();
        if($('.materials_row_'+$('#SpinCampaigns_rank').val()).size()<1 && value_rank != 0){ //chỉ xét trong giải
            fnBuildRowAward(event, ui, value_rank);
            fnInitAutocomplete(); // load auto materials 
        }
    });
}

/**
* @Author: NamLA Aug 24, 2019
* @Todo: init autocomplete material
*/
function fnInitAutocomplete(){
    var availableMaterials = <?php echo MyFunctionCustom::getMaterialsJson(array('created_by'=>$model->created_by)); ?>;
    $( ".m_name" ).autocomplete({
        source: availableMaterials,
        close: function( event, ui ) { $( ".m_name" ).val(''); },
        select: function( event, ui ) {
            var classname   = ($(this).parent().parent().attr('class'));
            var value_rank  = classname.replace('type_rank materials_row_', '');
            value_rank      = value_rank.replace(' selected', '');
            var class_item  = 'details_row_'+ui.item.id;
            if($('.materials_row_'+value_rank+' .'+class_item).size()<1){ //chỉ xét trong giải
                fnSetValueMaterial( event, ui, value_rank);
            }
        }
    });
} 

function fnBuildRowAward(event,ui,value_rank){
    var _tr = '';
    _tr += '<tr class="type_rank materials_row_'+ value_rank +'">';
    _tr +=      '<td class="item_c order_no_campaign"></td>';
    _tr +=      '<td class="item_c rank">';
    _tr +=          $('.rank').children('option:selected').text() + '<input name="SpinCampaignDetails[rank][]" value='+value_rank+' type="hidden"/>';
    _tr +=      '</td>';
    _tr +=      '<td class="item_c qty_award_col">';
    _tr +=          '<input name="SpinCampaignDetails[qty_award][]" value="1" class="w-30 item_c item_qty qty_award number_only_v1 " type="text" size="3" maxlength="3">';
    _tr +=      '</td>';
    _tr +=      '<td style="padding: 10px !important; " class="item_l material_name">';
    _tr +=          '<?php echo $form->textField($model,"materials_name",array("class"=>"m_name","style"=>"margin-top: 10px !important;", "size"=>32,"placeholder"=>"Nhập mã vật tư hoặc tên vật tư")); ?>';
    _tr +=          "<?php echo $form->error($model,'materials_name'); ?>";
    _tr +=      '<table style="width: 90%; margin:0 auto !important; margin-bottom: 10px !important;" class="materials_table details_table hm_table">';
    _tr +=          '<thead>';
    _tr +=              '<th class="item_name item_c">Tên</th>';
    _tr +=              '<th class="w-30 item_c">Số SP</th>';
    _tr +=              '<th class="item_unit last item_c">Xóa</th>';
    _tr +=          '</thead>';
    _tr +=          '<tbody>';
    _tr +=          '</tbody>';
    _tr +=      '</table> ';
    _tr +=      '</td>';
    _tr +=      '<td class="item_c last"><span class="remove_icon_only remove_rank remove_rank_'+value_rank+'"></span></td>';
    _tr += '</tr>';
    $('#materials_table .body_add_detail').append(_tr);
    bindEventForHelpNumber();
    fnRefreshOrderNumberCampaign();
    fnCalcTotalPay();
};

/**
* @Author: NamLA Aug 24, 2019
* @Todo: add details row autocomplete
*/
function fnSetValueMaterial(event, ui, value_rank){
    var _tr = '';
    _tr += '<tr class="details_row details_row_'+ui.item.id+'">';
    _tr +=      '<td class="item_l material_name">';
    _tr +=          '<label>'+ui.item.value+'</label>';
    _tr +=          "<input class= 'json' name='SpinCampaignDetails["+value_rank+"][award_json][]' type='hidden' value='{"+'\"id\":'+ui.item.id+',\"name\":\"'+ui.item.value+"\",\"qty\":1}'/>";
    _tr +=      '</td>';
    _tr +=      '<td class="item_c">';
    _tr +=          '<input name="SpinCampaignDetails['+value_rank+'][materials_qty][]" value="1" class="item_c item_qty w-30 materials_qty number_only_v1 " type="text" size="6" maxlength="9">';
    _tr +=      '</td>'; 
    _tr +=      '<td class="item_c last"><span class="remove_icon_only"></span></td>';
    _tr += '</tr>';
    $('.materials_row_'+value_rank+' .material_name .details_table tbody').append(_tr);
    fnChangeQtyMaterial();
}   
/**
* @Author: NamLA Aug 24, 2019
* @Todo: change json when change qty_material
*/
function fnChangeQtyMaterial(){
    $('.details_row').change(function(){
        var json = $(this).find(' td .json').val();
        json = (json.slice(0, json.search('qty') +5));
        json += $(this).find(' td .materials_qty').val() + "}";
        $(this).find(' td .json').val(json)
//        json = json.replace(json.indexOf('qty'))
    });
}
/**
* @Author: NamLA Aug 24, 2019
* @Todo: refresh order number
*/
function fnRefreshOrderNumberCampaign(){
    validateNumber();
    $('#materials_table').each(function(){
        var index = 1;
        $(this).find('.order_no_campaign').each(function(){
            $(this).text(index++);
        });
    });
}    
function fnBindSomeLiveChange(){
    $('.item_qty, .item_price, .qty_award .OrderTypeAmount').live('change', function(){
        fnCalcTotalPay();
    });
}

function fnCalcTotalPay(){
    var total_qty       = 0;
    $('#materials_table tbody .type_rank').each(function(){
        var qty_award = $(this).find('.qty_award').val()*1; 
        total_qty    += qty_award;
    });
    total_qty = parseFloat(total_qty);
    total_qty = Math.round(total_qty * 100) / 100;
    $('.total_qty').text(commaSeparateNumber(total_qty));
    $('.qty_spin').val(total_qty);
}       

function fnAfterRemoveIcon(){
    fnCalcTotalPay();
}


</script>