<?php
$this->breadcrumbs=array(
    'Quản Lý '.$this->pluralTitle => array('index'),
    'Thông Tin ' . $this->singleTitle . ' : ',
);

$menus = array(
	array('label'=>"Quản Lý $this->pluralTitle", 'url'=>array('index')),
	array('label'=>"Tạo Mới $this->singleTitle", 'url'=>array('create')),
	array('label'=>"Cập Nhật $this->singleTitle", 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>"Xóa $this->singleTitle", 'url'=>array('delete'), 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Bạn chắc chắn muốn xóa ?')),
);
$this->menu= ControllerActionsName::createMenusRoles($menus, $actions);
?>

<h1>Xem: <?php echo $this->singleTitle; ?></h1>
<?php echo MyFormat::BindNotifyMsg(); ?>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
            array(
                'name'  => 'name',                       
                'value' => $model->getName(),
            ),
            array(
                'name'  => 'campaign_date',
                'value' => $model->getCampaignDate(),
            ),
            array(
                'name'  => 'qty_spin',
                'value' => $model->getQtySpin()
            ),
            array(
                'name'  => 'type',
                'value' => $model->getType()
            ),
            array(
                'label' => 'Cơ cấu giải thưởng',
                'value' => $model->getSummaryTable(),
                'type'  => 'html'
            ),
            array(
                'name'  => 'description',
                'value' => $model->getDescription(),
            ),
            array(
                'name'  => 'created_date',
                'value' => $model->getCreatedDate(),
            ),
            array(
                'name'  => 'created_by',
                'value' => $model->getCreatedBy()
            )
	),
)); ?>
