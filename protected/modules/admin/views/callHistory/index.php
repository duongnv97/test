<?php
$this->breadcrumbs=array(
	$this->singleTitle,
);

$menus=array(
            array('label'=>"Create $this->singleTitle", 'url'=>array('create')),
);
$this->menu= ControllerActionsName::createMenusRoles($menus, $actions);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('call-history-grid', {
                url : $(this).attr('action'),
		data: $(this).serialize()
	});
	return false;
});
");

Yii::app()->clientScript->registerScript('ajaxupdate', "
$('#call-history-grid a.ajaxupdate').live('click', function() {
    $.fn.yiiGridView.update('call-history-grid', {
        type: 'POST',
        url: $(this).attr('href'),
        success: function() {
            $.fn.yiiGridView.update('call-history-grid');
        }
    });
    return false;
});
");
?>

<h1>Danh Sách <?php echo $this->pluralTitle; ?></h1>


<?php // echo CHtml::link('Tìm Kiếm Nâng Cao','#',array('class'=>'search-button')); ?>
<div class="search-form" style="">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'call-history-grid',
	'dataProvider'=>$model->search(),
        'afterAjaxUpdate'=>'function(id, data){ fnUpdateColorbox();}',
        'template'=>'{pager}{summary}{items}{pager}{summary}',
//    'itemsCssClass' => 'items custom_here',
//    'htmlOptions'=> array('class'=>'grid-view custom_here'),
        'pager' => array(
            'maxButtonCount'=>  CmsFormatter::$PAGE_MAX_BUTTON,
        ),
	'enableSorting' => false,
	//'filter'=>$model,
	'columns'=>array(
        array(
            'header' => 'S/N',
            'type' => 'raw',
            'value' => '$this->grid->dataProvider->pagination->currentPage * $this->grid->dataProvider->pagination->pageSize + ($row+1)',
            'headerHtmlOptions' => array('width' => '10px','style' => 'text-align:center;'),
            'htmlOptions' => array('style' => 'text-align:center;')
        ),
            array(
                'name' => 'agent_id',
                'value' => '$data->getAgentName()',
            ),
            array(
                'name' => 'user_id',
                'value' => '$data->getUserCreated()',
            ),
            array(
                'name' => 'sell_id',
                'type' => 'raw',
                'value' => '$data->getSellLink()',
            ),
            'phone',
            array(
                'name' => 'status',
                'value' => '$data->getStatus()',
            ),
            array(
                'name' => 'type_call',
                'type' => 'raw',
                'value' => '$data->getTypeCall()',
            ),
            array(
                'header' => 'Nghe lại',
                'type' => 'raw',
                'value' => '$data->getHtmlTestCall()',
            ),
            'file_record_name',
            array(
                'name' => 'created_date_client',
                'value' => '$data->getCreatedDateClient()',
            ),
            array(
                'name' => 'created_date',
                'value' => '$data->getCreatedDate()',
            ),
            array(
                'header' => 'Chi tiết lỗi',
                'type' => 'html',
                'value' => '$data->getViewReasonCallReview()',
            ),
            array(
                'header' => 'Điểm',
                'type' => 'html',
                'value' => '$data->getScore()',
            ),
            array(
                'header' => 'Actions',
                'class'=>'CButtonColumn',
                'template'=> ControllerActionsName::createIndexButtonRoles($actions),
                'buttons'=>array(
                ),
            ),
	),
)); ?>

<script>
$(document).ready(function() {
    fnUpdateColorbox();
    bindChangeAgent();
});

function fnUpdateColorbox(){   
    fixTargetBlank();
}


function bindChangeAgent(){
    $('body').on('click', '.ShowChangeAgent', function(){
        $(this).hide();
        var td = $(this).closest('td');
        td.find('.WrapChangeAgentDropdown').show();
    });
    $('body').on('click', '.CancelChangeAgent', function(){
        var td = $(this).closest('td');
        td.find('.WrapChangeAgentDropdown').hide();
        td.find('.ShowChangeAgent').show();
    });
    $('body').on('click', '.SaveChangeAgent', function(){
        var td = $(this).closest('td');
        var status = td.find('.ChangeAgentValue').val();
        if($.trim(status) == ''){
            alert("Chưa chọn phân loại");
            return false;
        }
        var url_ = $(this).attr('next');
        var typeText = td.find('.ChangeAgentValue').find('option:selected').text();
        $.ajax({
            url:url_,
            type: 'post',
//            dataType: 'json',
            data: {type_call_test:status},
            success: function(data){
                td.find('.TypeCallTestText').text(typeText);
                td.find('.WrapChangeAgentDropdown').hide();
                td.find('.ShowChangeAgent').show();
            }
        });
    });
}
</script>