<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'call-history-form',
	'enableAjaxValidation'=>false,
        'htmlOptions' => array('enctype' => 'multipart/form-data'),
)); ?>

    <p class="note">Dòng có dấu <span class="required">*</span> là bắt buộc.</p>
    <?php echo MyFormat::BindNotifyMsg(); ?>
    
        		
        
    <div class="row">
        <?php echo $form->labelEx($model,'call_id'); ?>
        <?php echo $form->textField($model,'call_id',array('size'=>11,'maxlength'=>11)); ?>
        <?php echo $form->error($model,'call_id'); ?>
    </div>
    <div class="row">
        <?php echo $form->labelEx($model,'agent_id'); ?>
        <?php echo $form->textField($model,'agent_id',array('size'=>11,'maxlength'=>11)); ?>
        <?php echo $form->error($model,'agent_id'); ?>
    </div>
    <div class="row">
        <?php echo $form->labelEx($model,'province_id'); ?>
        <?php echo $form->textField($model,'province_id'); ?>
        <?php echo $form->error($model,'province_id'); ?>
    </div>
    <div class="row">
        <?php echo $form->labelEx($model,'user_id'); ?>
        <?php echo $form->textField($model,'user_id',array('size'=>12,'maxlength'=>12)); ?>
        <?php echo $form->error($model,'user_id'); ?>
    </div>
    <div class="row">
        <?php echo $form->labelEx($model,'phone'); ?>
        <?php echo $form->textField($model,'phone',array('size'=>12,'maxlength'=>12)); ?>
        <?php echo $form->error($model,'phone'); ?>
    </div>
    <div class="row">
        <?php echo $form->labelEx($model,'status'); ?>
        <?php echo $form->textField($model,'status'); ?>
        <?php echo $form->error($model,'status'); ?>
    </div>
    <div class="row">
        <?php echo $form->labelEx($model,'sell_id'); ?>
        <?php echo $form->textField($model,'sell_id',array('size'=>12,'maxlength'=>12)); ?>
        <?php echo $form->error($model,'sell_id'); ?>
    </div>
    <div class="row">
        <?php echo $form->labelEx($model,'customer_id'); ?>
        <?php echo $form->textField($model,'customer_id',array('size'=>12,'maxlength'=>12)); ?>
        <?php echo $form->error($model,'customer_id'); ?>
    </div>
    <div class="row">
        <?php echo $form->labelEx($model,'type_call'); ?>
        <?php echo $form->textArea($model,'type_call',array('rows'=>6, 'cols'=>50)); ?>
        <?php echo $form->error($model,'type_call'); ?>
    </div>
    <div class="row">
        <?php echo $form->labelEx($model,'file_record_name'); ?>
        <?php echo $form->textField($model,'file_record_name',array('size'=>50,'maxlength'=>50)); ?>
        <?php echo $form->error($model,'file_record_name'); ?>
    </div>
    <div class="row">
        <?php echo $form->labelEx($model,'created_date_client'); ?>
        <?php echo $form->textField($model,'created_date_client'); ?>
        <?php echo $form->error($model,'created_date_client'); ?>
    </div>
    <div class="row">
        <?php echo $form->labelEx($model,'created_date'); ?>
        <?php echo $form->textField($model,'created_date'); ?>
        <?php echo $form->error($model,'created_date'); ?>
    </div>
	<div class="row buttons" style="padding-left: 141px;">
		        <?php $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType'=>'submit',
            'label'=>$model->isNewRecord ? 'Create' :'Save',
            'type'=>'null', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
            'size'=>'small', // null, 'large', 'small' or 'mini'
            //'htmlOptions' => array('style' => 'margin-bottom: 10px; float: right;'),
        )); ?>	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->
<script>
    $(document).ready(function(){
        $('.form').find('button:submit').click(function(){
            $.blockUI({ overlayCSS: { backgroundColor: '#fff' } }); 
        });
        
    });
</script>