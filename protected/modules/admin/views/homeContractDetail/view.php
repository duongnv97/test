<?php

$menus = array(
	array('label'=>"$this->pluralTitle Management", 'url'=>array('index')),
	array('label'=>"Tạo Mới $this->singleTitle", 'url'=>array('create')),
	array('label'=>"Cập Nhật $this->singleTitle", 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>"Xóa $this->singleTitle", 'url'=>array('delete'), 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Bạn chắc chắn muốn xóa ?')),
);
$this->menu= ControllerActionsName::createMenusRoles($menus, $actions);
?>

<h1><?php echo $this->singleTitle.": ".$model->id .'-'.$model->getAgent(); ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
             array(
                    'name'  =>'created_date',
                    'type'  =>'raw',
                    'value' => $model->getCreatedDate(),
                ),
            array(
                    'name'  =>'agent_id',
                    'type'  =>'raw',
                    'value' => $model->getAgent(),
                ),
            array(
                    'name'  =>'amount',
                    'type'  =>'raw',
                    'value' => $model->getAmount(),
                ),
            array(
                    'name'  =>'date_pay',
                    'type'  =>'raw',
                    'value' => $model->getDatePay(),
                ),
            array(
                    'name'  =>'province_id',
                    'type'  =>'raw',
                    'value' => $model->getProvince(),
                ),
            array(
                    'name'  =>'settle_id',
                    'type'  =>'raw',
                    'value' => $model->getSettle(),
                ),
    ),
));
?>
