<div class="form">
    <?php $form=$this->beginWidget('CActiveForm', array(
        'id'=>'homecontract-form',
        'enableAjaxValidation'=>false,
               'htmlOptions' => array('enctype' => 'multipart/form-data'),
        )); ?>
    <p class="note">Dòng có dấu <span class="required">*</span> là bắt buộc.</p>
    
    <?php echo MyFormat::BindNotifyMsg(); ?>
    
    <div class="rowDetailStatus row">
        <?php echo $form->labelEx($model,'date_pay'); ?>
            <?php

            $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                'model'=>$model,        
                'attribute'=>'date_pay',
                'options'=>array(
                    'showAnim'=>'fold',
                    'dateFormat'=> MyFormat::$dateFormatSearch,
                    'changeMonth' => true,
                    'changeYear' => true,
                    'showOn' => 'button',
                    'buttonImage'=> Yii::app()->theme->baseUrl.'/admin/images/icon_calendar_r.gif',
                    'buttonImageOnly'=> true,                                
                ),        
                'htmlOptions'=>array(
                    'class'=>'w-16',
                    'style'=>'height:20px;',
                ),
            ));
            ?>  
        <?php echo $form->error($model,'date_pay'); ?>
    </div>
    
    <div class="row">
    <?php echo $form->labelEx($model,'amount'); ?>
    <?php echo $form->textField($model,'amount',array('class'=>'w-300 number_only ad_fix_currency',)); ?>
    <?php echo $form->error($model,'amount'); ?>
    </div>
    
    <div class="row buttons" style="padding-left: 141px;">
        <?php $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType'=>'submit',
            'label'=>$model->isNewRecord ? 'Create' :'Save',
            'type'=>'null', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
            'size'=>'small', // null, 'large', 'small' or 'mini'
            //'htmlOptions' => array('style' => 'margin-bottom: 10px; float: right;'),
            )); ?>
    </div>
    <?php $this->endWidget(); ?>
</div>

<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/autoNumeric-master/autoNumeric.js"></script>
<script>
    $(document).ready(function () {
        fnInitInputCurrency(); 
    });
</script>

