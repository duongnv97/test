<?php
$this->breadcrumbs=array(
	$this->singleTitle,
);

$menus=array(
            array('label'=>"Create $this->singleTitle", 'url'=>array('create')),
);
$this->menu= ControllerActionsName::createMenusRoles($menus, $actions);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('app-login-grid', {
                url : $(this).attr('action'),
		data: $(this).serialize()
	});
	return false;
});
");

Yii::app()->clientScript->registerScript('ajaxupdate', "
$('#app-login-grid a.ajaxupdate').live('click', function() {
    $.fn.yiiGridView.update('app-login-grid', {
        type: 'POST',
        url: $(this).attr('href'),
        success: function() {
            $.fn.yiiGridView.update('app-login-grid');
        }
    });
    return false;
});
");
?>

<h1>Danh Sách <?php echo $this->pluralTitle; ?></h1>


<?php echo CHtml::link('Tìm Kiếm Nâng Cao','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'app-login-grid',
	'dataProvider'=>$model->search(),
        'afterAjaxUpdate'=>'function(id, data){ fnUpdateColorbox();}',
        'template'=>'{pager}{summary}{items}{pager}{summary}',
//    'itemsCssClass' => 'items custom_here',
//    'htmlOptions'=> array('class'=>'grid-view custom_here'),
        'pager' => array(
            'maxButtonCount'=>  CmsFormatter::$PAGE_MAX_BUTTON,
        ),
	'enableSorting' => false,
	//'filter'=>$model,
	'columns'=>array(
        array(
            'header' => 'S/N',
            'type' => 'raw',
            'value' => '$this->grid->dataProvider->pagination->currentPage * $this->grid->dataProvider->pagination->pageSize + ($row+1)',
            'headerHtmlOptions' => array('width' => '10px','style' => 'text-align:center;'),
            'htmlOptions' => array('style' => 'text-align:center;')
        ),
        array(
            'name' => 'user_id',
            'value'  => '$data->getNameById()'
        ),
        'username',
        array(
            'name' => 'platform',
            'value'  => '$data->getPlatform()'
        ),
        array(
            'name' => 'app_type',
            'value'  => '$data->getAppType()'
        ),
        'device_name',
        'device_imei',
        'device_os_version',
        'version_code',
        array(
            'name' => 'created_date',
            'value'  => '$data->getCreatedDate()'
        ),
        array(
            'header'  => 'DeviceToken',
            'value' => '$data->getDeviceToken()'
        ),
//        'gcm_device_token',
//        'apns_device_token',
//		array(
//                    'header' => 'Actions',
//                    'class'=>'CButtonColumn',
//                    'template'=> ControllerActionsName::createIndexButtonRoles($actions),
//                    'buttons'=>array(
//                        'update' => array(
////                            'visible' => '$data->can  Update()',
//                        ),
//                        'delete'=>array(
////                            'visible'=> 'GasCheck::canDeleteData($data)',
//                        ),
//                    ),
//		),
	),
)); ?>

<script>
$(document).ready(function() {
    fnUpdateColorbox();
});

function fnUpdateColorbox(){   
    fixTargetBlank();
}
</script>