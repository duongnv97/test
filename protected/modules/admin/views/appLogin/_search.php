<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=> GasCheck::getCurl(),
	'method'=>'get',
)); ?>

    <div class="row">
        <?php echo $form->labelEx($model,'user_id'); ?>
        <?php echo $form->hiddenField($model,'user_id', array('class'=>'')); ?>
        <?php
            // 1. limit search kh của sale
            $url = Yii::app()->createAbsoluteUrl('admin/ajax/search_by_role', array('role' => ROLE_EMPLOYEE_MAINTAIN));
            // widget auto complete search user customer and supplier
            $aData = array(
                'model'=>$model,
                'field_customer_id'=>'user_id',
                'url'=> $url,
                'name_relation_user'=>'rUser',
                'ClassAdd' => 'w-400',
                'field_autocomplete_name' => 'autocomplete_user',
                'placeholder'=>'Nhập mã NV hoặc tên',
            );
            $this->widget('ext.SpaAutocompleteCustomer.SpaAutocompleteCustomer',
                array('data'=>$aData));
        ?>
    </div>

	<div class="row">
		<?php echo $form->label($model,'username',array()); ?>
		<?php echo $form->textField($model,'username',array('maxlength'=>50, 'class'=>'w-300')); ?>
	</div>

	<div class="row more_col">
            <div class="col1">
		<?php echo $form->label($model,'platform',array()); ?>
		<?php echo $form->dropdownList($model,'platform', AppLogin::model()->getArrayPlatform(), array('empty'=>'Select', 'class'=>'w-200')); ?>
            </div>

            <div class="col2">
		<?php echo $form->label($model,'app_type',array()); ?>
		<?php echo $form->dropdownList($model,'app_type', AppLogin::model()->getArrayAppType(), array('empty'=>'Select', 'class'=>'w-200')); ?>
            </div>
	</div>

	<div class="row more_col">
            <div class="col1">
		<?php echo $form->label($model,'device_name',array()); ?>
		<?php echo $form->textField($model,'device_name',array('maxlength'=>100, 'class'=>'w-200')); ?>
            </div>

            <div class="col2">
		<?php echo $form->label($model,'device_imei',array()); ?>
		<?php echo $form->textField($model,'device_imei',array('maxlength'=>100, 'class'=>'w-200')); ?>
            </div>

            <div class="col3">
		<?php echo $form->label($model,'device_os_version',array()); ?>
		<?php echo $form->textField($model,'device_os_version',array('maxlength'=>100, 'class'=>'w-200')); ?>
            </div>
	</div>

	<div class="row more_col">
            <div class="col1">
		<?php echo $form->label($model,'version_code',array()); ?>
		<?php echo $form->textField($model,'version_code',array('maxlength'=>20, 'class'=>'w-200')); ?>
            </div>

            <div class="col2">
            <?php echo Yii::t('translation', $form->label($model,'created_date')); ?>
            <?php 
                $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                    'model'=>$model,        
                    'attribute'=>'created_date',
                    'options'=>array(
                        'showAnim'=>'fold',
                        'dateFormat'=> MyFormat::$dateFormatSearch,
                        'changeMonth' => true,
                        'changeYear' => true,
                        'showOn' => 'button',
                        'buttonImage'=> Yii::app()->theme->baseUrl.'/admin/images/icon_calendar_r.gif',
                        'buttonImageOnly'=> true,                                
                    )
                ));
            ?>     		
            </div>
        </div>


	<div class="row buttons" style="padding-left: 159px;">
        <?php $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType'=>'submit',
            'label'=>'Search',
            'type'=>'null', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
            'size'=>'small', // null, 'large', 'small' or 'mini'
            //'htmlOptions' => array('style' => 'margin-bottom: 10px; float: right;'),
        )); ?>	
        </div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->