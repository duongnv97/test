<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=> GasCheck::getCurl(),
	'method'=>'post',
        'enableAjaxValidation'=>false,
        'htmlOptions' => array('enctype' => 'multipart/form-data'),
)); ?>
    
    <div class="row">
        <?php echo $form->labelEx($model,'file_excel'); ?>
        <div style="padding-left: 140px;">
            <?php echo $form->fileField($model,'file_excel'); ?>
            <em class="hight_light item_b"><a href="<?php echo Yii::app()->createAbsoluteUrl('/').'/upload/excel/check_virtual_phone.xlsx';?>">Tải Biểu Mẫu Excel </a></em>
            <br>
        </div>
        <?php echo $form->error($model,'file_excel'); ?>
    </div>
    
    <div class="row more_col">
        <div class="col1">
            <?php echo $form->labelEx($model, 'date_from'); ?>
            <?php 
            $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                'model' => $model,

                'attribute' => 'date_from',
                'options' => array(
                    'showAnim' => 'fold',
                    'dateFormat' => MyFormat::$dateFormatSearch,
                    'changeMonth' => true,
                    'changeYear' => true,
                    'showOn' => 'button',
                    'buttonImage' => Yii::app()->theme->baseUrl . '/admin/images/icon_calendar_r.gif',
                    'buttonImageOnly' => true,

                ),

                'htmlOptions' => array(
                    'class' => 'w-16',
                    'style' => 'height:20px;',
                    'readonly' => 'readonly',
                ),
            ));
            ?>  
        </div>
        <div class="col2">
            <?php echo $form->labelEx($model, 'date_to'); ?>
            <?php 
            $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                'model' => $model,

                'attribute' => 'date_to',
                'options' => array(
                    'showAnim' => 'fold',
                    'dateFormat' => MyFormat::$dateFormatSearch,
                    'changeMonth' => true,
                    'changeYear' => true,
                    'showOn' => 'button',
                    'buttonImage' => Yii::app()->theme->baseUrl . '/admin/images/icon_calendar_r.gif',
                    'buttonImageOnly' => true,

                ),

                'htmlOptions' => array(
                    'class' => 'w-16',
                    'style' => 'height:20px;',
                    'readonly' => 'readonly',
                ),
            ));
            ?>  
        </div>
    </div>
    
<div class="row buttons" style="padding-left: 159px;">
    <?php $this->widget('bootstrap.widgets.TbButton', array(
        'buttonType'=>'submit',
        'label'=>'Search',
        'type'=>'null', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
        'size'=>'small', // null, 'large', 'small' or 'mini'
        //'htmlOptions' => array('style' => 'margin-bottom: 10px; float: right;'),
    )); ?>
<?php $this->endWidget(); ?>
</div>
</div><!-- search-form -->



