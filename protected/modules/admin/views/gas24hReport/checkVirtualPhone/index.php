<?php
$this->breadcrumbs=array(
	$this->singleTitle,
);

$menus=array(
//    array('label'=>"Create $this->singleTitle", 'url'=>array('create')),
);
$this->menu= ControllerActionsName::createMenusRoles($menus, $actions);
$gridCss = Yii::app()->getAssetManager()->publish(Yii::getPathOfAlias('zii.widgets.assets')) . '/gridview/styles.css';
Yii::app()->getClientScript()->registerCssFile($gridCss);
?>

<h1>Danh Sách <?php echo $this->pluralTitle; ?></h1>

<div class="search-form">
<?php $this->renderPartial('checkVirtualPhone/_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->
<div class="grid-view">
    <?php if(!empty($aData1)): ?>
        <h2 class="item_b"></h2>
        <table class="items" style="width: 40%; float: left; margin-right: 20px;">
            <thead>
                <tr>
                    <th>STT</th>
                    <th>Đại lý</th>
                    <th>Số tài khoản app</th>
                </tr>
            </thead>
            <tbody>
                <?php
                $i = 1; $sum = 0;
                foreach ($aData1 as $value):
                    $sum += $value['count'];
                ?>
                <tr>
                    <td class="item_c"><?php echo $i++; ?></td>
                    <td class="item_l"><?php echo isset($value['agent']) ? $value['agent'] : ""; ?></td>
                    <td class="item_r"><?php echo $value['count']; ?></td>
                </tr>
                <?php endforeach; ?>
                <tr>
                    <td colspan="2" class="item_r item_b">Tổng cộng</td>
                    <td class="item_r item_b"><?php echo ActiveRecord::formatCurrency($sum); ?></td>
                </tr>
            </tbody>
        </table>
    <?php endif; ?>

    <?php if(!empty($aData2)): ?>
        <h2 class="item_b"></h2>
        <table class="items" style="width: 57%;">
            <thead>
                <tr>
                    <th>STT</th>
                    <th>Nhân viên</th>
                    <th>Mã code</th>
                    <th>Số tài khoản app</th>
                </tr>
            </thead>
            <tbody>
                <?php
                $i = 1; $sum = 0;
                foreach ($aData2 as $value):
                    $sum += $value['count'];
                ?>
                <tr>
                    <td class="item_c"><?php echo $i++; ?></td>
                    <td class="item_l"><?php echo $value['name']; ?></td>
                    <td class="item_l"><?php echo $value['code_no']; ?></td>
                    <td class="item_r"><?php echo $value['count']; ?></td>
                </tr>
                <?php endforeach; ?>
                <tr>
                    <td colspan="3" class="item_r item_b">Tổng cộng</td>
                    <td class="item_r item_b"><?php echo ActiveRecord::formatCurrency($sum); ?></td>
                </tr>
            </tbody>
        </table>
    <?php endif; ?>
</div>
<div class="clearfix"></div>

<script>
$(document).ready(function() {
    fnAddClassOddEven('items');
    fnUpdateColorbox();
});

function fnUpdateColorbox(){   
    fixTargetBlank();
}
</script>