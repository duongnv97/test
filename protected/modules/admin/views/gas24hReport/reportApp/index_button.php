<div class="form">
    <?php
        $LinkNormal         = Yii::app()->createAbsoluteUrl('admin/Gas24hReport/ReportApp');
        $LinkAll            = Yii::app()->createAbsoluteUrl('admin/Gas24hReport/ReportApp', array('type'=> SellReport::TYPE_ALL));
        $LinkDetail         = Yii::app()->createAbsoluteUrl('admin/Gas24hReport/ReportApp', array('type'=> SellReport::TYPE_DETAIL));
    ?>
    <h1>Danh sách 
        <a class='btn_cancel f_size_14 <?php echo (!isset($_GET['type'])) ? "active":"";?>' href="<?php echo $LinkNormal;?>">Thống kê năm</a>
        <a class='btn_cancel f_size_14 <?php echo isset($_GET['type']) && $_GET['type']==SellReport::TYPE_ALL ? 'active':'';?>' href="<?php echo $LinkAll;?>">Thống kê tổng quát</a>
        <a class='btn_cancel f_size_14 <?php echo isset($_GET['type']) && $_GET['type']==SellReport::TYPE_DETAIL ? 'active':'';?>' href="<?php echo $LinkDetail;?>">Thống kê SL KH đặt app</a>
    </h1> 
</div>

