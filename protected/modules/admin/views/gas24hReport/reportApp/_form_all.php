<?php 
$aData = $model->getReportAppAll();
?>
<?php echo CHtml::link('Tìm Kiếm Nâng Cao','#',array('class'=>'search-button')); ?>
<div class="search-form">
    <?php $this->renderPartial('reportApp/_search_all',array(
            'model'=>$model,
    )); ?>
</div><!-- search-form -->
<div class="form">
    <?php if(!empty($aData['AGENTS'])): ?>
    <table class="materials_table hm_table" style='width:100%;'>
        <thead>
            <tr>
                <th class='item_c'>#</th>
                <th class='item_c'>KH cài app</th>
                <th class='item_c'>Số bình app</th>
                <th class='item_c'>KH đặt app</th>
                <?php foreach ($aData['SELL_TIMES'] as $key => $sell_time) : ?>
                <th class='item_c'>Lần <?php echo $sell_time; ?></th>
                <?php endforeach; ?>
            </tr>
        </thead>
        <tbody>
            <tr id="header-tr" class="item_b"></tr>
            <?php
            $totalAppInstall = 0;
            $totalAppCountGas = 0;
            $totalAppCustomer = 0;
            $aTimes = [];
            ?>
            <?php foreach ($aData['AGENTS'] as $agent_id => $agent) :
                $totalAppInstall    += !empty($aData['APP_INSTALL'][$agent_id]) ? $aData['APP_INSTALL'][$agent_id] : 0;
                $totalAppCountGas   += !empty($aData['APP_SELL'][$agent_id]['COUNT_GAS']) ? $aData['APP_SELL'][$agent_id]['COUNT_GAS'] : 0;
                $totalAppCustomer   += !empty($aData['APP_SELL'][$agent_id]['CUSTOMER']) ? count($aData['APP_SELL'][$agent_id]['CUSTOMER']) : 0;
            ?>
            <tr>
                <td class="item_b"><?php echo $agent; ?></td>
                <td class="item_r item_b"><?php echo !empty($aData['APP_INSTALL'][$agent_id]) ? ActiveRecord::formatCurrency($aData['APP_INSTALL'][$agent_id]) : ''; ?></td>
                <td class="item_r item_b"><?php echo !empty($aData['APP_SELL'][$agent_id]['COUNT_GAS']) ? ActiveRecord::formatCurrency($aData['APP_SELL'][$agent_id]['COUNT_GAS']) : ''; ?></td>
                <td class="item_r item_b"><?php echo !empty($aData['APP_SELL'][$agent_id]['CUSTOMER']) ? ActiveRecord::formatCurrency(count($aData['APP_SELL'][$agent_id]['CUSTOMER'])) : ''; ?></td>
                <?php foreach ($aData['SELL_TIMES'] as $key => $sell_time) : ?>
                <?php $aTime            = !empty($aData['SELL_TIMES_CUSTOMER'][$agent_id][$sell_time]) ? $aData['SELL_TIMES_CUSTOMER'][$agent_id][$sell_time] : []; ?>
                <td class="item_c">
                    <?php
                    $aDataAgent         = !empty($aData['APP_SELL'][$agent_id]['CUSTOMER']) ? $aData['APP_SELL'][$agent_id]['CUSTOMER'] : [];
                    $aCus       = array_intersect($aDataAgent,$aTime);
                    $count      = count($aCus);
                    if(!empty($aTimes[$sell_time])){
                        $aTimes[$sell_time] += $count;
                    }else{
                        $aTimes[$sell_time] = $count;
                    }
                    $link = Yii::app()->createAbsoluteUrl('admin/sell/index', ['sCustomer' => implode(',', $aCus)]);
                    $value = "<a href='$link' target='_blank'>$count</a>";
                    echo $count > 0 ?  $value : '';
                    ?>
                </td>
                <?php endforeach; ?>
            </tr>
            <?php endforeach; ?>
            <tr class="item_b" id="footer-tr">
                <td class="item_c item_b">Tổng</td>
                <td class="item_r item_b"><?php echo ActiveRecord::formatCurrency($totalAppInstall); ?></td>
                <td class="item_r item_b"><?php echo ActiveRecord::formatCurrency($totalAppCountGas); ?></td>
                <td class="item_r item_b"><?php echo ActiveRecord::formatCurrency($totalAppCustomer); ?></td>
                <?php foreach ($aData['SELL_TIMES'] as $key => $sell_time) : ?>
                <td class="item_c"><?php echo !empty($aTimes[$sell_time]) ? $aTimes[$sell_time] : 0; ?></td>
                <?php endforeach; ?>
            </tr>
        </tbody>
    </table>
    <?php endif; ?>
</div>
    