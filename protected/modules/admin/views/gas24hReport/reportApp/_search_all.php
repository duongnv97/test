<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=> GasCheck::getCurl(),
	'method'=>'post',
)); ?>
<!-- ++content-->
    <div class="row">
        <?php echo $form->labelEx($model,'province_id_agent'); ?>
        <div class="fix-label">
            <?php
               $this->widget('ext.multiselect.JMultiSelect',array(
                     'model'=>$model,
                     'attribute'=>'province_id_agent',
                     'data'=> GasProvince::getArrAll(),
                     // additional javascript options for the MultiSelect plugin
                    'options'=>array(),
                     // additional style
                     'htmlOptions'=>array('style' => 'width: 800px;'),
               ));    
           ?>
        </div>
    </div>
    <div class="row">
        <?php echo $form->labelEx($model,'sort_type_status'); ?>
        <?php echo $form->dropDownList($model,'sort_type_status', $model->getArraySort(), []); ?>		
    </div> 
<!-- --content-->

<div class="row buttons" style="padding-left: 159px;">
    <?php $this->widget('bootstrap.widgets.TbButton', array(
        'buttonType'=>'submit',
        'label'=>'Search',
        'type'=>'null', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
        'size'=>'small', // null, 'large', 'small' or 'mini'
        //'htmlOptions' => array('style' => 'margin-bottom: 10px; float: right;'),
    )); ?>
<?php $this->endWidget(); ?>
</div>
</div><!-- search-form -->