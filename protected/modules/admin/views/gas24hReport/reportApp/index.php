<?php
$this->breadcrumbs=array(
	$this->pageTitle,
);
Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
");
?>
<?php echo MyFormat::BindNotifyMsg(); ?>
<h1><?php echo $this->pageTitle; ?></h1>
<?php include 'index_button.php'; ?>
<?php 

if(!isset($_GET['type'])){
    include '_form_default.php';
}elseif ($_GET['type']==SellReport::TYPE_ALL) {
    include '_form_all.php';
}elseif ($_GET['type']==SellReport::TYPE_DETAIL) {
    include '_form_detail.php';
}

?>
<script>
    $(document).ready(function(){
        $('#header-tr').html($('#footer-tr').html());    
    });
</script>