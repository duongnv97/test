<?php 
$aData = $model->getReportAppDefault();
?>

<?php echo CHtml::link('Tìm Kiếm Nâng Cao','#',array('class'=>'search-button')); ?>
<div class="search-form">
    <?php $this->renderPartial('reportApp/_search',array(
            'model'=>$model,
    )); ?>
</div><!-- search-form -->

<div class="form">
    <?php if(!empty($aData['AGENTS'])):
        foreach ($aData['AGENTS'] as $agent_id => $agent) : ?>
    <h1><?php echo $agent; ?></h1>
    <table class="materials_table hm_table" style='width:100%;'>
        <thead>
            <tr>
                <th class='item_c'>#</th>
                <?php if(!empty($aData['MONTH'][$agent_id])): foreach ($aData['MONTH'][$agent_id] as $key => $month) : ?>
                <th colspan="2" class='item_c'>Tháng <?php echo $month ?></th>
                <?php    endforeach; endif;  ?>
            </tr>
        </thead>
        <tbody>
            <?php if(!empty($aData['MONTH'][$agent_id])): ?>
            <tr>
                <td class='item_c'>Khách hàng cài app</td>
                <?php foreach ($aData['MONTH'][$agent_id] as $key => $month) : ?>
                <td colspan="2" class='item_c'><?php echo !empty($aData['APP_INSTALL'][$agent_id][$month]) ? $aData['APP_INSTALL'][$agent_id][$month].' KH': ''; ?></td>
                <?php    endforeach; ?>
            </tr>
            <tr>
                <td class='item_c'>SL Đơn hàng</td>
                <?php foreach ($aData['MONTH'][$agent_id] as $key => $month) : ?>
                <td colspan="2" class='item_c'><?php echo !empty($aData['APP_SELL'][$agent_id][$month]['COUNT_SELL']) ? $aData['APP_SELL'][$agent_id][$month]['COUNT_SELL']: ''; ?></td>
                <?php    endforeach; ?>
            </tr>
            <tr>
                <td class='item_c'>SL Khách hàng</td>
                <?php foreach ($aData['MONTH'][$agent_id] as $key => $month) : ?>
                <td colspan="2" class='item_c'><?php echo !empty($aData['APP_SELL'][$agent_id][$month]['CUSTOMER']) ? count($aData['APP_SELL'][$agent_id][$month]['CUSTOMER']): ''; ?></td>
                <?php     endforeach; ?>
            </tr>
            
            <!--thống kê số bình gas--> 
            <tr>
                <td class='item_c'></td>
                <?php foreach ($aData['MONTH'][$agent_id] as $key => $month) : ?>
                <td class='item_c item_b'>Tháng trước</td>
                <td class='item_c item_b'>Trong tháng</td>
                <?php endforeach; ?>
            </tr>
            <tr>
                <td class='item_c item_b'>Số bình App</td>
                <?php foreach ($aData['MONTH'][$agent_id] as $key => $month) : ?>
                <td class='item_c item_b'><?php echo !empty($aData['APP_SELL'][$agent_id][$month]['COUNT_GAS_OLD']) ? $aData['APP_SELL'][$agent_id][$month]['COUNT_GAS_OLD'] : '' ?></td>
                <td class='item_c item_b'><?php echo !empty($aData['APP_SELL'][$agent_id][$month]['COUNT_GAS_NEW']) ? $aData['APP_SELL'][$agent_id][$month]['COUNT_GAS_NEW'] : '' ?></td>
                <?php endforeach; ?>
            </tr>
            
            <!--Thống kê theo số lần-->
            
            <?php if(!empty($aData['SELL_TIMES'][$agent_id])): ?>
            <tr>
                <td class='item_c item_b'>Tổng KH</td>
                <?php foreach ($aData['MONTH'][$agent_id] as $key => $month) : ?>
                <td class='item_c item_b'>
                    <?php
                    $aOld       = !empty($aData['APP_SELL'][$agent_id][$month]['CUSTOMER_OLD']) ? $aData['APP_SELL'][$agent_id][$month]['CUSTOMER_OLD'] : [];
                    echo count($aOld);
                    ?>
                </td>
                <td class='item_c item_b'>
                    <?php
                    $aNew       = !empty($aData['APP_SELL'][$agent_id][$month]['CUSTOMER_NEW']) ? $aData['APP_SELL'][$agent_id][$month]['CUSTOMER_NEW'] : [];
                    echo count($aNew);
                    ?>
                </td>
                <?php endforeach; ?>
            </tr>
            <?php foreach ($aData['SELL_TIMES'][$agent_id] as $key => $sell_time) : ?>
            <tr>
                <?php $aTime      = !empty($aData['SELL_TIMES_CUSTOMER'][$agent_id][$sell_time]) ? $aData['SELL_TIMES_CUSTOMER'][$agent_id][$sell_time] : []; ?>
                <td class='item_c'>Lần <?php echo $sell_time; ?></td>
                <?php foreach ($aData['MONTH'][$agent_id] as $key => $month) : ?>
                <td class='item_c'>
                    <?php
                    $aOld       = !empty($aData['APP_SELL'][$agent_id][$month]['CUSTOMER_OLD']) ? $aData['APP_SELL'][$agent_id][$month]['CUSTOMER_OLD'] : [];
                    $aCus       = array_intersect($aOld,$aTime);
                    $count      = count($aCus);
                    $link = Yii::app()->createAbsoluteUrl('admin/sell/index', ['sCustomer' => implode(',', $aCus)]);
                    $value = "<a href='$link' target='_blank'>$count</a>";
                    echo $count > 0 ?  $value : '';
                    ?> 
                </td>
                <td class='item_c'>
                    <?php
                    $aOld       = !empty($aData['APP_SELL'][$agent_id][$month]['CUSTOMER_NEW']) ? $aData['APP_SELL'][$agent_id][$month]['CUSTOMER_NEW'] : [];
                    $aCus       = array_intersect($aOld,$aTime);
                    $count      = count(array_intersect($aOld,$aTime));
                    $link = Yii::app()->createAbsoluteUrl('admin/sell/index', ['sCustomer' => implode(',', $aCus)]);
                    $value = "<a href='$link' target='_blank'>$count</a>";
                    echo $count > 0 ?  $value : '';
                    ?> 
                </td>
                <?php endforeach; ?>
            </tr>
            <?php endforeach; endif; ?>
            <?php endif; ?>
        </tbody>
    </table>
    <?php    endforeach; endif; ?>
</div>