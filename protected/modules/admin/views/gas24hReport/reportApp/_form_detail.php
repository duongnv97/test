<?php echo CHtml::link('Tìm Kiếm Nâng Cao','#',array('class'=>'search-button')); ?>
<div class="search-form">
    <?php $this->renderPartial('reportApp/_search_detail',array(
            'model'=>$model,
    )); ?>
</div><!-- search-form -->
<?php 
if(!empty($model->search_time)): 
    $aData = $model->getReportDetail();
?>
<div class="form">
    <?php if(!empty($aData['AGENTS'])): ?>
    <table class="materials_table hm_table" style='width:100%;'>
        <thead>
            <tr>
                <th>#</th>
                <th class='item_c item_b'>Tổng</th>
                <?php foreach ($aData['MONTH'] as $key => $month): ?>
                <th class="item_c">Tháng <?php echo $month; ?></th>
                <th class='item_c item_b' style='display: none;'>Tổng</th>
                <?php endforeach; ?>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($aData['AGENTS'] as $agent_id => $agent) : ?>
            <?php $aTime            = !empty($aData['SELL_TIMES_CUSTOMER'][$agent_id][$model->search_time]) ? $aData['SELL_TIMES_CUSTOMER'][$agent_id][$model->search_time] : []; ?>
            <?php $sum = 0; ?>
            <tr>
                <td><?php echo $agent; ?></td>
                <td class='item_target_<?php echo $agent_id; ?> item_b item_c'></td>
                <?php foreach ($aData['MONTH'] as $key => $month): ?>
                <td class="item_c">
                    <?php
                    $aDataAgent         = !empty($aData['APP_SELL'][$agent_id][$month]['CUSTOMER']) ? $aData['APP_SELL'][$agent_id][$month]['CUSTOMER'] : [];
                    $aCus = array_intersect($aDataAgent,$aTime);
                    $count              = count($aCus);
                    $sum                += $count;
                    $link = Yii::app()->createAbsoluteUrl('admin/sell/index', ['sCustomer' => implode(',', $aCus)]);
                    $value = "<a href='$link' target='_blank'>$count</a>";
                    echo $count > 0 ?  $value : '';
                    ?>
                </td>
                <?php endforeach; ?>
                <td data-target='<?php echo $agent_id; ?>' class='item_from item_b item_c' style='display: none;'><?php echo $sum; ?></td>
            </tr>
            <?php endforeach;?>
            </tbody>
        </table>
    <?php endif; ?>
</div>
<?php endif;?>
<script>
    $(document).ready(function(){
        $('.item_from').each(function(){
            $data_target  = $(this).data('target');
            $class_target = '.item_target_'+$data_target;
            $($class_target).html($(this).html());
        });
    });
</script>