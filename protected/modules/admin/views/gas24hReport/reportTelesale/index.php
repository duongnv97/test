<?php
$this->breadcrumbs=array(
    $this->pageTitle,
);
if($this->canExcelReportTelesale()):
    $menus[] = ['label'=> 'Xuất Excel Danh Sách Hiện Tại',
                'url'=>array('ReportTeleSale', 'ExportExcel'=>1), 
                'htmlOptions'=>array('class'=>'export_excel ','label'=>'Xuất Excel'),
    ];
endif;
$this->menu= ControllerActionsName::createMenusRoles($menus, $actions);

?>

<h1><?php echo $this->pageTitle;?></h1>
<div class="search-form" style="">
<?php $this->renderPartial('reportTelesale/_search',array(
        'model'=>$model,
)); ?>
</div><!-- search-form -->

<style>
    .ui-datepicker-trigger { float: left;}
</style>

<script>
$(document).ready(function(){
    $('.form').find('button:submit').click(function(){        
        $.blockUI({ overlayCSS: { backgroundColor: '#fff' } }); 
    });   
 
});
</script>
<?php if(isset($data['OUT_SELL_SUM_EMPLOYEE'])): ?>
<div class="title_table_statistic">BÁO CÁO CODE/BQV  
        <?php
        echo " Từ Ngày $model->date_from Đến Ngày $model->date_to"; 
        ?>
</div>
<?php
$gridCss = Yii::app()->getAssetManager()->publish(Yii::getPathOfAlias('zii.widgets.assets')) . '/gridview/styles.css';
Yii::app()->getClientScript()->registerCssFile($gridCss);
$LIST_EMPLOYEE          = $data['EMPLOYEE'];
$AGENT                  = $data['AGENT'];
$OUT_SELL_EMPLOYEE      = $data['OUT_SELL_EMPLOYEE'];
//$OUT_SELL_SUM_AGENT     = $data['OUT_SELL_SUM_AGENT'];       //tổng sell từng đại lí
$OUT_SELL_SUM_EMPLOYEE  = $data['OUT_SELL_SUM_EMPLOYEE'];    //tổng sell từng NV
$OUT_SELL               = $data['OUT_SELL'];                 //sell theo từng đại lí của từng NV
//$OUT_TRACKING_AGENT     = $data['OUT_TRACKING_AGENT'];       //tổng code từng đại lí
$OUT_TRACKING_EMPLOYEE  = $data['OUT_TRACKING_EMPLOYEE'];    //tổng code từng NV
$SUM_OUT_TRACKING       = $data['SUM_OUT_TRACKING'];         //code theo từng đại lí của từng NV
$OUTPUT_AGENT           = $data['OUTPUT_AGENT'];                 //code theo từng đại lí của từng NV
$EMPLOYEE_FULLNAME      = isset($data['EMPLOYEE_FULLNAME']) ? $data['EMPLOYEE_FULLNAME'] : [];// full name 

$mAppCache = new AppCache();
$aTelesale = $mAppCache->getArrayIdRole(ROLE_TELESALE);
$index = 1;
// Get table height
$freeHeightTable    = (sizeof($LIST_EMPLOYEE) + 1) * 19;
$freeHeightTable    = $freeHeightTable > 400 ? 400 : $freeHeightTable;
$mAppPromotion = new AppPromotion();

?>
<script type="text/javascript"
        src="<?php echo Yii::app()->theme->baseUrl; ?>/js/scroll/jquery.freezetablecolumns.1.1.js"></script>
<div class="grid-view display_none">
    <table id="freezetablecolumns_report_call" class="items hm_table freezetablecolumns">
        <thead>
        <tr class="h_50">
            <th class="w-20 ">#</th>
            <th class="w-150 ">Đại lý</th>
            <th class="w-120 ">Tổng</th>
            <?php foreach ($OUT_SELL_EMPLOYEE as $user_id => $arrayValue): ?>
            <?php
//                if(empty($model->fromPttt) && !in_array($user_id, $aTelesale)){
//                    continue ;
//                }elseif($model->fromPttt && in_array($user_id, $aTelesale)){
//                    continue ;
//                }
                $trackingValueSumEmployee       = isset($SUM_OUT_TRACKING['EMPLOYEE'][$user_id]) ? $SUM_OUT_TRACKING['EMPLOYEE'][$user_id] : '0';
                $saleValueSumEmployee   = isset($arrayValue['value']) ? $arrayValue['value'] : '0';
                $textSum                = $trackingValueSumEmployee.'/'.$saleValueSumEmployee;
                if ($textSum == '0/0'){
                     continue ;
                }
                $employeeName   = isset($arrayValue['name']) ? $arrayValue['name']: '';
                $temp           = explode(' ', $employeeName);
                $employeeName   = end($temp);
                $employeeName   .= isset($EMPLOYEE_FULLNAME[$user_id]) ? '<br>'.$EMPLOYEE_FULLNAME[$user_id] : '';
                ?>
                <th class="item_b w-80" style="white-space: normal;"><?php echo $employeeName; ?></th>
            <?php endforeach; ?>
        </tr>
        </thead>
        <tbody>
            <tr class="h_20">
                <td></td>
                <td></td>
                <?php 
                    $trackingValueSumAll   = isset($SUM_OUT_TRACKING['SUM']) ? $SUM_OUT_TRACKING['SUM'] : '0';
                    $saleValueSumAll       = isset($OUT_SELL_SUM_EMPLOYEE['SUM']) ? $OUT_SELL_SUM_EMPLOYEE['SUM'] : '0';
                    $sumAll = $trackingValueSumAll.'/'.$saleValueSumAll;
                ?>
                <td class="item_b item_c"><?php echo 'Tổng: '. $sumAll ?></td>
                <?php foreach ($OUT_SELL_EMPLOYEE as $user_id => $arrayValue): ?>
                <?php
//                    if(empty($model->fromPttt) && !in_array($user_id, $aTelesale)){
//                        continue ;
//                    }elseif($model->fromPttt && in_array($user_id, $aTelesale)){
//                        continue ;
//                    }
                    $trackingValueSumEmployee       = isset($SUM_OUT_TRACKING['EMPLOYEE'][$user_id]) ? $SUM_OUT_TRACKING['EMPLOYEE'][$user_id] : '0';
                    $saleValueSumEmployee   = isset($arrayValue['value']) ? $arrayValue['value'] : '0';
                    $textSum                = $trackingValueSumEmployee.'/'.$saleValueSumEmployee;
                    if ($textSum == '0/0'){
                         continue ;
                    }
                ?>
                    <td class="item_b item_c w-60"><?php echo $textSum; ?></td>
                <?php endforeach; ?>
            </tr>
            
            <?php foreach ($OUTPUT_AGENT as $agent_id => $arrayValue): ?>
                <?php
//                    if(empty($agent_id)){
//                        continue ;
//                    }
                    $name           = isset($AGENT[$agent_id]) ? $AGENT[$agent_id] : '';
                    $nRef           = isset($arrayValue['tracking']) ? (int)$arrayValue['tracking'] : 0;
                    $nSale          = isset($arrayValue['sell']) ? $arrayValue['sell'] : '0';
                    $textSumRow     = $nRef. '/' . $nSale;
                    if ($textSumRow == '0/0'):
                        continue ;
                    endif;
                ?>
                <tr class="h_20">
                    <td class="item_c"><?php echo $index++; ?></td>
                    <td class="item_b f_size_10"><?php echo $name; ?></td>
                    <td class="item_r item_b"><?php echo $textSumRow; ?></td>
                    <?php foreach ($OUT_SELL_EMPLOYEE as $user_id => $arrayValue): ?>
                        <?php
//                        if(empty($model->fromPttt) && !in_array($user_id, $aTelesale)){
//                            continue ;
//                        }elseif($model->fromPttt && in_array($user_id, $aTelesale)){
//                            continue ;
//                        }
                        $trackingValueSumEmployee       = isset($SUM_OUT_TRACKING['EMPLOYEE'][$user_id]) ? $SUM_OUT_TRACKING['EMPLOYEE'][$user_id] : '0';
                        $saleValueSumEmployee   = isset($arrayValue['value']) ? $arrayValue['value'] : '0';
                        $textSum                = $trackingValueSumEmployee.'/'.$saleValueSumEmployee;
                        if ($textSum == '0/0'){
                             continue ;
                        }
                        $trackingvalue  = isset($OUT_TRACKING_EMPLOYEE[$agent_id][$user_id]) ? $OUT_TRACKING_EMPLOYEE[$agent_id][$user_id] : '0';
                        $saleValue      = isset($OUT_SELL[$agent_id][$user_id]) ? $OUT_SELL[$agent_id][$user_id] : '0';
                        $text = $trackingvalue. '/' .$saleValue;
                        if ($text == '0/0'):
                            $text = '';
                        endif;
                        ?>
                    <td class="item_c "><?php echo $text; ?></td>
                    <?php endforeach; ?>
                </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
</div>

<script type="text/javascript">
    fnAddClassOddEven('items');
    $('.freezetablecolumns').each(function () {
        var id_table = $(this).attr('id');
        $('#' + id_table).freezeTableColumns({
            width:       <?php echo Users::WIDTH_freezetable;?>,   // required - bt 1100
            height:      <?php echo $freeHeightTable;?>,   // required
            numFrozen: 3,     // optional
            frozenWidth: 330,   // optional
            clearWidths: true  // optional
        });
    });
    $(window).load(function () {
        var index = 1;
        $('.freezetablecolumns').each(function () {
            if(index == 1)
                $(this).closest('div.grid-view').show();
            index++;
        });
        fnAddClassOddEven('items');
    });
</script>
<?php endif; ?>