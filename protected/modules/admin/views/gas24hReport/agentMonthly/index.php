<?php
$this->breadcrumbs=array(
    $this->pageTitle,
);

$menus=array(
        array('label'=>'Create Văn Bản', 'url'=>array('create')),
);
$this->menu= ControllerActionsName::createMenusRoles($menus, $actions);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('gas-text-grid', {
                url : $(this).attr('action'),
		data: $(this).serialize()
	});
	return false;
});
");

Yii::app()->clientScript->registerScript('ajaxupdate', "
$('#gas-text-grid a.ajaxupdate').live('click', function() {
    $.fn.yiiGridView.update('gas-text-grid', {
        type: 'POST',
        url: $(this).attr('href'),
        success: function() {
            $.fn.yiiGridView.update('gas-text-grid');
        }
    });
    return false;
});
");
?>

<h1><?php echo $this->pageTitle;?></h1>

<?php echo CHtml::link('Tìm Kiếm Nâng Cao','#',array('class'=>'search-button')); ?>
<div class="search-form" style="">
<?php $this->renderPartial('agentMonthly/_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php
$sellReport = new SellReport();
$mAppCache  = new AppCache();
$aAgent     = $mAppCache->getAgent();
if(isset($data)){
    $aMonth = isset($data['aMonth']) ? $data['aMonth'] : array();
    $aAgentId = isset($data['aAgentId']) ? $data['aAgentId'] : array();
    $stt = 1;
    $maxMonth = 1;
    if(!empty($aMonth)){
        $maxMonth =  max($aMonth);
    }
}
?>

<div class="grid-view">
    <table class="tb hm_table">
        <thead>
        <tr style="">
            <th class="w-50 ">STT</th>
            <th class="w-300 ">Đại lý</th>
            <th class="w-250 ">Bình quân tháng <br> Cuộc gọi/App</th>
            <?php  if(count($aMonth)): 
                    for($i = 1 ; $i<= $maxMonth ; $i++ ): ?>
                        <th class="w-120 ">Tháng <?php echo $i; ?></th>
            <?php  endfor; 
            endif; ?>
        </tr>
        </thead>
        <tbody>
            <tr>
                <td class="item_c"> </td>
                <td class="item_c"> </td>
                <td class="item_b item_c">
                    <?php  // đợi anh dũng review xong mới đưa vào
//                        echo isset($data['TOTAL'][Sell::SOURCE_WEB]) ? number_format(array_sum($data['TOTAL'][Sell::SOURCE_WEB])/$maxMonth) : '0';
//                        echo "/";
//                        echo isset($data['TOTAL'][Sell::SOURCE_APP]) ? number_format(array_sum($data['TOTAL'][Sell::SOURCE_APP])/$maxMonth) : '0';
                    ?>
                </td>
            
                <?php  for($i = 1 ; $i<= $maxMonth ; $i++ ): ?>
                            <td class="item_b item_c ">
                                <?php 
                                    echo isset($data['TOTAL'][Sell::SOURCE_WEB][$i]) ? $data['TOTAL'][Sell::SOURCE_WEB][$i] : '0';
                                    echo "/";
                                    echo isset($data['TOTAL'][Sell::SOURCE_APP][$i]) ? $data['TOTAL'][Sell::SOURCE_APP][$i] : '0';
                                ?>
                            </td>
                <?php  endfor; ?>
                
            </tr>
            <?php
            if(isset($data)):
                foreach ($aAgentId as $agent_id) :
            ?>
                    <tr>
                        <td class="item_c"><?php echo $stt; ?></td>
                        <td class="item_b ">
                            <?php echo isset($aAgent[$agent_id]) ? $aAgent[$agent_id]['first_name'] : ''; ?>
                        </td>
                        <td class=" item_c item_b">
                            <?php 
                            echo isset($data['AVERAGE'][$agent_id][Sell::SOURCE_WEB]) ? number_format($data['AVERAGE'][$agent_id][Sell::SOURCE_WEB]/$maxMonth) : '0';
                            echo "/";
                            echo isset($data['AVERAGE'][$agent_id][Sell::SOURCE_APP]) ? number_format($data['AVERAGE'][$agent_id][Sell::SOURCE_APP]/$maxMonth) : '0';
                            ?>
                        </td>
                        <?php   for($i = 1; $i<= max($aMonth); $i++) : ?>
                                <td class="item_c"> 
                                    <?php  
                                        echo isset($data['MONTHLY'][$agent_id][$i][Sell::SOURCE_WEB]) ? $data['MONTHLY'][$agent_id][$i][Sell::SOURCE_WEB] : '0';
                                        echo "/";
                                        echo isset($data['MONTHLY'][$agent_id][$i][Sell::SOURCE_APP]) ? $data['MONTHLY'][$agent_id][$i][Sell::SOURCE_APP] : '0'; 
                                    ?>
                                </td>
                        <?php endfor;  $stt++; ?>
                    </tr>
                    
            <?php endforeach;
            endif;
            ?>
        
        </tbody>
    </table>
</div>
<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/print-store-card.css" />

<script>
$(document).ready(function() {
    fnUpdateColorbox();
});

function fnUpdateColorbox(){   
    fixTargetBlank();
}
</script>