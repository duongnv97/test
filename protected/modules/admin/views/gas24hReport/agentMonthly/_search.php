<?php 
$aSwitch = '';
if($model->canExportExcel()):
    $aSwitch = array('ExportExcel'=>1);
endif;
$urlExcel = Yii::app()->createAbsoluteUrl('admin/Gas24hReport/AgentMonthly',$aSwitch);
?>
<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=> Yii::app()->createAbsoluteUrl('admin/Gas24hReport/AgentMonthly'),
	'method'=>'get',
)); ?>

<div class="row more_col">
    <div class="col1">
        <?php echo Yii::t('translation', $form->labelEx($model,'statistic_year')); ?>
        <?php echo $form->dropDownList($model,'statistic_year', ActiveRecord::getRangeYear(), array('class'=>'w-200')); ?>		
        <?php echo $form->error($model,'statistic_year'); ?>
    </div>
    <div class="col2">
        <?php echo $form->labelEx($model,'sort_type_status'); ?>
        <?php echo $form->dropDownList($model,'sort_type_status', $model->getArrayStatusForSortType(),array('class'=>'w-200','empty'=>'Select')); ?>
    </div>
</div>
    
<div class="row">
    <?php echo $form->labelEx($model,'province_id_agent'); ?>
    <div class="fix-label">
        <?php
           $this->widget('ext.multiselect.JMultiSelect',array(
                 'model'=>$model,
                 'attribute'=>'province_id_agent',
                 'data'=> GasProvince::getArrAll(),
                 // additional javascript options for the MultiSelect plugin
                'options'=>array('selectedList' => 30,),
                 // additional style
                 'htmlOptions'=>array('style' => 'width: 800px;'),
           ));    
       ?>
    </div>
</div>



<div class="row buttons" style="padding-left: 159px;">
    <?php $this->widget('bootstrap.widgets.TbButton', array(
        'buttonType'=>'submit',
        'label'=>'Search',
        'type'=>'null', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
        'size'=>'small', // null, 'large', 'small' or 'mini'
        //'htmlOptions' => array('style' => 'margin-bottom: 10px; float: right;'),
    )); ?>
    &nbsp;&nbsp;&nbsp;&nbsp;<a class='btn_cancel' href='<?php echo $urlExcel;?>'>Xuất Excel</a>
<?php $this->endWidget(); ?>
</div>
    

</div><!-- search-form -->



