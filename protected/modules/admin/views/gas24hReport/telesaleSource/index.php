<?php
$this->breadcrumbs=array(
    $this->pageTitle,
);
if($this->canExcelReportTelesale()):
    $menus[] = ['label'=> 'Xuất Excel Danh Sách Hiện Tại',
                'url'=>array('TelesaleSource', 'ExportExcel'=>1), 
                'htmlOptions'=>array('class'=>'export_excel ','label'=>'Xuất Excel'),
    ];
endif;
$this->menu= ControllerActionsName::createMenusRoles($menus, $actions);

?>

<h1><?php echo $this->pageTitle;?></h1>
<div class="search-form" style="">
<?php $this->renderPartial('telesaleSource/_search',array(
        'model'=>$model,
)); ?>
</div><!-- search-form -->

<style>
    .ui-datepicker-trigger { float: left;}
</style>

<script>
$(document).ready(function(){
    $('.form').find('button:submit').click(function(){        
        $.blockUI({ overlayCSS: { backgroundColor: '#fff' } }); 
    });   
 
});
</script>
<?php if(isset($data['SUM_ALL'])): ?>
<div class="title_table_statistic">BÁO CÁO SỐ ĐƠN HÀNG/ APP  
        <?php
        echo " Từ Ngày $model->date_from Đến Ngày $model->date_to"; 
        ?>
</div>
<?php
$gridCss = Yii::app()->getAssetManager()->publish(Yii::getPathOfAlias('zii.widgets.assets')) . '/gridview/styles.css';
Yii::app()->getClientScript()->registerCssFile($gridCss);
$OUT_PUT = $data['OUT_PUT'];
$SUM_TELESALE          = $data['SUM_TELESALE'];
$SUM_AGENT             = $data['SUM_AGENT'];
$SUM_ALL                = $data['SUM_ALL'];
$mAppCache              = new AppCache();
$aAgentName             = $mAppCache->getAgentListdata();
//echo '<pre>';
//print_r($aAgentName);
//echo '</pre>';
//die;

$LIST_TELESALE           = $data['LIST_TELESALE'];
$LIST_AGENT           = $data['LIST_AGENT'];
$name= Users::getArrObjectUserByRole('', $LIST_TELESALE);
$aUid = CHtml::listData($name, 'id', 'first_name');
$index = 1;
// Get table height
$freeHeightTable    = (sizeof($LIST_TELESALE) + 1) * 19;
$freeHeightTable    = $freeHeightTable > 400 ? 400 : $freeHeightTable;

?>
<script type="text/javascript"
        src="<?php echo Yii::app()->theme->baseUrl; ?>/js/scroll/jquery.freezetablecolumns.1.1.js"></script>
<div class="grid-view display_none">
    <table id="freezetablecolumns_report_call" class="items hm_table freezetablecolumns">
        <thead>
        <tr class="h_50">
            <th class="w-20 ">#</th>
            <th class="w-150 ">Đại lý</th>
            <th class="w-120 ">Tổng</th>
            <?php foreach ($LIST_TELESALE as $key_telesale): ?>
                <?php 
                $sumSell       = isset($SUM_TELESALE[$key_telesale]['NUMBER_SELL']) ? $SUM_TELESALE[$key_telesale]['NUMBER_SELL'] : '0';
                $sumApp       = isset($SUM_TELESALE[$key_telesale]['NUMBER_APP']) ? $SUM_TELESALE[$key_telesale]['NUMBER_APP'] : '0';
                $textSum                = $sumSell.'/'.$sumApp;
                if ($textSum == '0/0'){
                     continue ;
                }
                $telesaleName   = isset($aUid[$key_telesale]) ? $aUid[$key_telesale]: '';
                $temp           = explode(' ', $telesaleName);
                $telesaleName   = end($temp);
                ?>
                <th class="item_b w-80" style="white-space: normal;"><?php echo $telesaleName; ?></th>
            <?php endforeach; ?>
        </tr>
        </thead>
        <tbody>
            <tr class="h_20">
                <td></td>
                <td></td>
                <?php 
                    $numberSellAll   = isset($SUM_ALL['NUMBER_SELL']) ? $SUM_ALL['NUMBER_SELL'] : '0';
                    $numberAppAll   = isset($SUM_ALL['NUMBER_APP']) ? $SUM_ALL['NUMBER_APP'] : '0';
                    $sumAll = $numberSellAll.'/'.$numberAppAll;
                ?>
                <td class="item_b item_c"><?php echo 'Tổng: '. $sumAll ?></td>
                <?php foreach ($LIST_TELESALE as $key_telesale): ?>
                <?php
                    $sumSell       = isset($SUM_TELESALE[$key_telesale]['NUMBER_SELL']) ? $SUM_TELESALE[$key_telesale]['NUMBER_SELL'] : '0';
                    $sumApp       = isset($SUM_TELESALE[$key_telesale]['NUMBER_APP']) ? $SUM_TELESALE[$key_telesale]['NUMBER_APP'] : '0';
                    $textSum                = $sumSell.'/'.$sumApp;
                    if ($textSum == '0/0'){
                         continue ;
                    }
                ?>
                    <td class="item_b item_c w-60"><?php echo $textSum; ?></td>
                <?php endforeach; ?>
            </tr>
            
            <?php foreach ($LIST_AGENT as $key_agent_id ): ?>
                <?php
                    $nameAgent           = isset($aAgentName[$key_agent_id]) ? $aAgentName[$key_agent_id] : '';
                    $numberSellAgent     = isset($SUM_AGENT[$key_agent_id]['NUMBER_SELL']) ? $SUM_AGENT[$key_agent_id]['NUMBER_SELL'] : 0;
                    $numberAppAgent      = isset($SUM_AGENT[$key_agent_id]['NUMBER_APP']) ? $SUM_AGENT[$key_agent_id]['NUMBER_APP'] : 0;
                    $textSumRow     = $numberSellAgent. '/' . $numberAppAgent;
                    if ($textSumRow == '0/0'):
                        continue ;
                    endif;
                ?>
                <tr class="h_20">
                    <td class="item_c"><?php echo $index++; ?></td>
                    <td class="item_b f_size_10"><?php echo $nameAgent; ?></td>
                    <td class="item_r item_b"><?php echo $textSumRow; ?></td>
                    <?php foreach ($LIST_TELESALE as $key_telesale): ?>
                        <?php
                        $numberSell           = isset($OUT_PUT[$key_agent_id][$key_telesale]['NUMBER_SELL']) ? $OUT_PUT[$key_agent_id][$key_telesale]['NUMBER_SELL'] : 0;
                        $numberApp           = isset($OUT_PUT[$key_agent_id][$key_telesale]['NUMBER_APP']) ? $OUT_PUT[$key_agent_id][$key_telesale]['NUMBER_APP'] : 0;
                        $text                = $numberSell.'/'.$numberApp;
                        if ($text == '0/0'){
                            $text = '' ;
                        }
                        ?>
                    <td class="item_c "><?php echo $text; ?></td>
                    <?php endforeach; ?>
                </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
</div>

<script type="text/javascript">
    fnAddClassOddEven('items');
    $('.freezetablecolumns').each(function () {
        var id_table = $(this).attr('id');
        $('#' + id_table).freezeTableColumns({
            width:       <?php echo Users::WIDTH_freezetable;?>,   // required - bt 1100
            height:      <?php echo $freeHeightTable;?>,   // required
            numFrozen: 3,     // optional
            frozenWidth: 330,   // optional
            clearWidths: true  // optional
        });
    });
    $(window).load(function () {
        var index = 1;
        $('.freezetablecolumns').each(function () {
            if(index == 1)
                $(this).closest('div.grid-view').show();
            index++;
        });
        fnAddClassOddEven('items');
    });
</script>
<?php endif; ?>