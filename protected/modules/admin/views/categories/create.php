<?php
$this->breadcrumbs=array(
	'Categories'=>array('index'),
	'Create',
);

$menus = array(		
        array('label'=>'Categories Management', 'url'=>array('index')),
);
$this->menu= ControllerActionsName::createMenusRoles($menus, $actions);
?>

<h1>Create Categories</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>