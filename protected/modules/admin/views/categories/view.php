<?php
$this->breadcrumbs=array(
	'Categories'=>array('index'),
	$model->title,
);

$menus = array(
	array('label'=>'Create Categories', 'url'=>array('create')),
	array('label'=>'Update Categories', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete Categories', 'url'=>array('delete'), 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
);
$this->menu= ControllerActionsName::createMenusRoles($menus, $actions);
?>

<h1>View Categories #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(		
		'title',
		'description',
		array(
                    'name'=>'Parent',                    
                    'value'=> !is_null($model->findByPk($model->parent_id)) ? $model->parent->title : '',  //(!is_null($model->findByPk($model->parent_id))?$model->findByPk($model->parent_id)->title:""),                    
                ),
		array(
                    'name'=>'Layout',                    
                    'value'=> $model->layout->title,                    
                ),
		'slug',
	),
)); ?>
