<div class="wide form">

    <?php
    $form = $this->beginWidget('CActiveForm', array(
        'action' => GasCheck::getCurl(), //Yii::app()->createUrl($this->route),
        'method' => 'get',
    ));
    ?>   
    <div class="row">
        <?php echo $form->label($model, 'name', array('label' => Yii::t('translation', 'Tên văn bản'))); ?>
        <?php echo $form->textField($model, 'name', array('size' => 60, 'maxlength' => 300)); ?>
    </div>
    <div class="row buttons" style="padding-left: 159px;">
        <?php
        $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType' => 'submit',
            'label' => 'Search',
            'type' => 'null', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
            'size' => 'small', // null, 'large', 'small' or 'mini'
                //'htmlOptions' => array('style' => 'margin-bottom: 10px; float: right;'),
        ));
        ?>
        
    </div>

    <?php $this->endWidget(); ?>

</div><!-- search-form -->
