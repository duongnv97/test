<?php

$this->breadcrumbs=array(
	'QL Văn Bản'=>array('index'),
	$model->name,
);

$menus = array(
	array('label'=>'Document Management', 'url'=>array('index')),
	array('label'=>'Create DocMangement', 'url'=>array('create')),
	array('label'=>'Update DocMangement', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete DocMangement', 'url'=>array('delete'), 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
);
$this->menu= ControllerActionsName::createMenusRoles($menus, $actions);
?>

<h1>View #<?php echo $model->name; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
            array(
                'name'=>'Nam',
                'value'=>$model->getName()
            ),            
            array(
                'name'=>'Mô tả',
                'value'=>$model->getDescription()
            ),            
            array(
                'name'=>'File văn bản',
                'type'=>'raw',
                'value'=>$model->getFileShowIndex()
            ),            
            array(
                'name'=>'Ngày tạo',
                'value'=>$model->getCreatedDate()
            ),            
            array(
                'name'=>'Người tạo',
                'value'=>$model->getCreatedBy()
            )            
	
	),
)); ?>
