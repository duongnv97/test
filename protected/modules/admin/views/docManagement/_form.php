<?php echo MyFormat::BindNotifyMsg(); ?>
<div class="form">
    <?php
    $classDisplay = 'display_none';
    $form = $this->beginWidget('CActiveForm', array(
        'id' => 'doc-form',
        'enableAjaxValidation' => false,
        'htmlOptions' => array('enctype' => 'multipart/form-data'),
    ));
    ?>
    <div class="row">
        <?php echo Yii::t('translation', $form->labelEx($model, 'name', array('label' => 'Tên văn bản'))); ?>
        <?php echo $form->textField($model, 'name', array('style' => 'width:500px', 'maxlength' => 150)); ?>
        <?php echo $form->error($model, 'name'); ?>
    </div>
    <div class="row ">
        <?php echo $form->labelEx($model, 'description'); ?>
        <?php echo $form->textArea($model, 'description', array('cols' => 61, 'rows' => 10, "placeholder" => "Mô tả")); ?>
        <?php echo $form->error($model, 'description'); ?>
    </div>

    <div class="clr"></div>
    <?php include '_form_file.php'; ?>

    <div class="row buttons" style="padding-left:141px;">
        <?php
        $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType' => 'submit',
            'label' => $model->isNewRecord ? Yii::t('translation', 'Create') : Yii::t('translation', 'Save'),
            'type' => 'null', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
            'size' => 'small', // null, 'large', 'small' or 'mini'
                //'htmlOptions' => array('style' => 'margin-bottom: 10px; float: right;'),
        ));
        ?>	
    </div>
    <?php $this->endWidget(); ?>
</div><!-- form -->


<script>    
    
    $(document).ready(function(){
        fnRefreshOrderNumber();
        fnBindRemoveIcon();
    });
    function fnBuildRowFile(this_){
        $(this_).closest('.tb_file').find('.materials_table').find('tr:visible:last').next('tr').show();
    }
</script>


