<?php
$this->breadcrumbs=array(
	Yii::t('translation','Văn bản')=>array('index'),
	Yii::t('translation','Create'),
);

$menus = array(		
        array('label'=> Yii::t('translation', 'Văn bản') , 'url'=>array('index')),
);
$this->menu= ControllerActionsName::createMenusRoles($menus, $actions);
?>
<h1><?php echo Yii::t('translation', 'Tạo Mới văn bản'); ?></h1>
<?php

echo $this->renderPartial('_form', array('model'=>$model)); 
?>