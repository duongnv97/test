<?php

$this->breadcrumbs=array(
	'QL Văn bản',
);

$menus=array(
	array('label'=> Yii::t('translation','Create DocManagement'), 'url'=>array('create')),
);
$this->menu= ControllerActionsName::createMenusRoles($menus, $actions);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('gas-doc-grid', {
                url : $(this).attr('action'),
		data: $(this).serialize()
	});
	return false;
});
");

Yii::app()->clientScript->registerScript('ajaxupdate', "
$('#gas-storehouse-grid a.ajaxupdate').live('click', function() {
    $.fn.yiiGridView.update('gas-doc-grid', {
        type: 'POST',
        url: $(this).attr('href'),
        success: function() {
            $.fn.yiiGridView.update('gas-storehouse-grid');
        }
    });
    return false;
});
");
?>

<h1><?php echo Yii::t('translation', 'QL Văn bản'); ?></h1>

<?php echo CHtml::link(Yii::t('translation','Advanced Search'),'#',array('class'=>'search-button')); ?>
<div class="search-form">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'gas-doc-grid',
	'dataProvider'=>$model->search(),
	'enableSorting' => false,
	'columns'=>array(
         array(
            'header' => 'S/N',
            'type' => 'raw',
            'value' => '$this->grid->dataProvider->pagination->currentPage * $this->grid->dataProvider->pagination->pageSize + ($row+1)',
            'headerHtmlOptions' => array('width' => '30px','style' => 'text-align:center;'),
            'htmlOptions' => array('style' => 'text-align:center;')
        ),
            array(
                'name'=>'name',
                'value'=>'$data->getName()',
            ),		 
            array(
                'name'=>'description',
                'value'=>'$data->getDescription()',
            ),	 
            array(
                'name'=>'File văn bản',
                'type'=>'raw',
                'value'=>'$data->getFileShowIndex()',
            ),	 
             
            array(
                'name'=>'created_date',
                'value'=>'$data->getCreatedDate()',
            ),		 
            array(
                'name'=>'created_by',
                'value'=>'$data->getCreatedBy()',
            ),        
		 
		array(
                    'header' => 'Action',
                    'class'=>'CButtonColumn',
                    'template'=> ControllerActionsName::createIndexButtonRoles($actions),
                    'buttons'=>array(
                        'update' => array(
                            'visible' => '$data->canUpdate()',
                        ),
                        'delete'=>array(
                            'visible'=> '$data->canDelete()',
                        ),
                    ),
		),
	),
)); ?>
