<?php
$aData = $model->getStatisticalVersionOs();
$htmlAndroid = '<table class="items" style="width:40%; margin:0 auto;">'
            .       '<thead>'
            .           '<tr><th>Platform</th><th>Phiên bản OS</th><th>Số lượng</th></tr>'
            .       '</thead>'
            .   '<tbody>';
$htmlIos = $htmlAndroid;
$aPlatform = AppLogin::model()->getArrayPlatform();
if(isset($aData[UsersTokens::PLATFORM_ANDROID])){
    foreach ($aData[UsersTokens::PLATFORM_ANDROID] as $value) {
        $htmlAndroid .= "<tr>"
                     .      "<td>{$aPlatform[UsersTokens::PLATFORM_ANDROID]}</td>"
                     .      "<td>{$value['name']}</td>"
                     .      "<td class='item_r'>"
                     .          ActiveRecord::formatCurrency($value['count']) 
                     .      "</td>"
                     .  "</tr>";
    }
}
if(isset($aData[UsersTokens::PLATFORM_IOS])){
    foreach ($aData[UsersTokens::PLATFORM_IOS] as $value) {
        $htmlIos     .= "<tr>"
                     .      "<td>{$aPlatform[UsersTokens::PLATFORM_IOS]}</td>"
                     .      "<td>{$value['name']}</td>"
                     .      "<td class='item_r'>"
                     .          ActiveRecord::formatCurrency($value['count']) 
                     .      "</td>"
                     .  "</tr>";
    }
}
$htmlAndroid .=     '</tbody>'
             .   '</table>';
$htmlIos     .=     '</tbody>'
             .   '</table>';


?>
<div class="grid-view" style="display:flex;">
    <?php 
    echo $htmlAndroid;
    echo $htmlIos;
    ?>
</div>
