<?php 
    $aData     = $model->getStatisticalApp();
    $aPlatform = AppLogin::model()->getArrayPlatform();
    $aAppType  = AppLogin::model()->getArrayAppType();
    $html = '';
    foreach ($aData as $app_type => $detail) {
        $html .= "<p>";
        if(isset($aAppType[$app_type])){
            $html .= "<b>{$aAppType[$app_type]}:</b>";
        }
        foreach ($detail as $platfrom => $num) {
            if(isset($aPlatform[$platfrom])){
                $html .= "<span> {$aPlatform[$platfrom]}: {$num},</span>";
            }
        }
        $html = rtrim($html, ",");
        $html .= "</p>";
    }
    echo $html;
?>

<?php echo CHtml::link('Tìm Kiếm Nâng Cao','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display: none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'app-login-lock-grid',
	'dataProvider'=>$model->search(),
        'afterAjaxUpdate'=>'function(id, data){ fnUpdateColorbox();}',
        'template'=>'{pager}{summary}{items}{pager}{summary}',
//    'itemsCssClass' => 'items custom_here',
//    'htmlOptions'=> array('class'=>'grid-view custom_here'),
        'pager' => array(
            'maxButtonCount'=>  CmsFormatter::$PAGE_MAX_BUTTON,
        ),
	'enableSorting' => false,
	//'filter'=>$model,
	'columns'=>array(
        array(
            'header' => 'S/N',
            'type' => 'raw',
            'value' => '$this->grid->dataProvider->pagination->currentPage * $this->grid->dataProvider->pagination->pageSize + ($row+1)',
            'headerHtmlOptions' => array('width' => '10px','style' => 'text-align:center;'),
            'htmlOptions' => array('style' => 'text-align:center;')
        ),
        array(
            'name' => 'is_lock',
            'value'  => '$data->getLockStatus()'
        ),
        'total_user_id',
        'username_open',
        array(
            'name' => 'platform',
            'value'  => '$data->getPlatform()'
        ),
        array(
            'name' => 'app_type',
            'value'  => '$data->getAppType()'
        ),
        'device_name',
        'device_imei',
        'device_os_version',
        'version_code',
        array(
            'name' => 'last_update',
            'value'  => '$data->getLastUpdate()'
        ),
            array(
                'header' => 'Actions',
                'class'=>'CButtonColumn',
                'template'=> ControllerActionsName::createIndexButtonRoles($actions),
                'buttons'=>array(
                        'update' => array(
//                            'visible' => '$data->canUpdate()',
                        ),
                        'delete'=>array(
//                            'visible'=> 'GasCheck::canDeleteData($data)',
                        ),
                ),
            ),
	),
)); ?>