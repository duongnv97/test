<?php
$this->breadcrumbs=array(
	$this->pageTitle,
);

$menus=array(
            array('label'=>"Create $this->singleTitle", 'url'=>array('create')),
);
$this->menu= ControllerActionsName::createMenusRoles($menus, $actions);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('app-login-lock-grid', {
                url : $(this).attr('action'),
		data: $(this).serialize()
	});
	return false;
});
");

Yii::app()->clientScript->registerScript('ajaxupdate', "
$('#app-login-lock-grid a.ajaxupdate').live('click', function() {
    $.fn.yiiGridView.update('app-login-lock-grid', {
        type: 'POST',
        url: $(this).attr('href'),
        success: function() {
            $.fn.yiiGridView.update('app-login-lock-grid');
        }
    });
    return false;
});
");
?>

<!--<h1>Danh Sách <?php echo $this->pluralTitle; ?></h1>-->
<?php include "index_button.php"; ?>
<?php 
    if($type == AppLoginLock::TAB_TYPE_VERSION_OS) {
        include "statistical_version_os.php";
    } elseif ($type == AppLoginLock::TAB_TYPE_GENERAL) {
        include "statistical_general.php";
    }
?>

<?php 
$gridCss = Yii::app()->getAssetManager()->publish(Yii::getPathOfAlias('zii.widgets.assets')) . '/gridview/styles.css';
Yii::app()->getClientScript()->registerCssFile($gridCss);
?>
<script>
$(document).ready(function() {
    fnUpdateColorbox();
    fnAddClassOddEven('items');
});

function fnUpdateColorbox(){   
    fixTargetBlank();
}
</script>