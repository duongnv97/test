<div class="form">
    <?php
        $LinkGeneral   = Yii::app()->createAbsoluteUrl('admin/appLoginLock', ['type'=> AppLoginLock::TAB_TYPE_GENERAL]);
        $LinkVersionOs = Yii::app()->createAbsoluteUrl('admin/appLoginLock', ['type'=> AppLoginLock::TAB_TYPE_VERSION_OS]);
    ?>
    <h1>Danh Sách <?php echo $this->pageTitle; ?>
        <a class='btn_cancel f_size_14 <?php echo $type == AppLoginLock::TAB_TYPE_GENERAL ? "active":"";?>' href="<?php echo $LinkGeneral;?>">Tổng quát</a>
        <a class='btn_cancel f_size_14 <?php echo $type == AppLoginLock::TAB_TYPE_VERSION_OS ? "active":"";?>' href="<?php echo $LinkVersionOs;?>">Version OS</a>
    </h1>
</div>