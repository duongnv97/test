<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=> GasCheck::getCurl(),
	'method'=>'get',
)); ?>

	<div class="row more_col">
            <div class="col1">
		<?php echo $form->label($model,'is_lock'); ?>
		<?php echo $form->dropdownList($model,'is_lock', $model->getArrayLockStatus(), array('empty'=>'Select','class'=>'w-200')); ?>
            </div>

            <div class="col2">
		<?php echo $form->label($model,'total_user_id',array()); ?>
		<?php echo $form->textField($model,'total_user_id', array('class'=>'w-200 number_only_v1')); ?>
            </div>

            <div class="col3">
		<?php echo $form->label($model,'username_open',array()); ?>
		<?php echo $form->textField($model,'username_open',array('class'=>'w-200','maxlength'=>50)); ?>
            </div>
	</div>

	<div class="row more_col">
            <div class="col1">
		<?php echo $form->label($model,'platform',array()); ?>
		<?php echo $form->dropdownList($model,'platform', AppLogin::model()->getArrayPlatform(), array('empty'=>'Select','class'=>'w-200')); ?>
            </div>

            <div class="col2">
		<?php echo $form->label($model,'app_type',array()); ?>
		<?php echo $form->dropdownList($model,'app_type', AppLogin::model()->getArrayAppType(), array('empty'=>'Select','class'=>'w-200')); ?>
            </div>
	</div>

	<div class="row more_col">
            <div class="col1">
		<?php echo $form->label($model,'device_name',array()); ?>
		<?php echo $form->textField($model,'device_name',array('class'=>'w-200','maxlength'=>100)); ?>
            </div>

            <div class="col2">
		<?php echo $form->label($model,'device_imei',array()); ?>
		<?php echo $form->textField($model,'device_imei',array('class'=>'w-200','maxlength'=>100)); ?>
            </div>
            
            <div class="col3">
                    <?php echo $form->label($model,'device_os_version',array()); ?>
                    <?php echo $form->textField($model,'device_os_version',array('class'=>'w-200','maxlength'=>100)); ?>
            </div>
	</div>

	<div class="row more_col">
            <div class="col1">
		<?php echo $form->label($model,'version_code',array()); ?>
		<?php echo $form->textField($model,'version_code',array('class'=>'w-200','maxlength'=>20)); ?>
            </div>

            <div class="col2">
		<?php echo $form->label($model,'last_update',array()); ?>
		<?php 
                $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                    'model'=>$model,        
                    'attribute'=>'last_update',
                    'options'=>array(
                        'showAnim'=>'fold',
                        'dateFormat'=> MyFormat::$dateFormatSearch,
                        'changeMonth' => true,
                        'changeYear' => true,
                        'showOn' => 'button',
                        'buttonImage'=> Yii::app()->theme->baseUrl.'/admin/images/icon_calendar_r.gif',
                        'buttonImageOnly'=> true,                                
                    )
                ));
            ?>
            </div>
	</div>

	<div class="row buttons" style="padding-left: 159px;">
		        <?php $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType'=>'submit',
            'label'=>'Search',
            'type'=>'null', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
            'size'=>'small', // null, 'large', 'small' or 'mini'
            //'htmlOptions' => array('style' => 'margin-bottom: 10px; float: right;'),
        )); ?>	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->