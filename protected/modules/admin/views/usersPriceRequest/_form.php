<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'users-price-request-form',
	'enableAjaxValidation'=>false,
        'htmlOptions' => array('enctype' => 'multipart/form-data'),
)); ?>

    <p class="note">Dòng có dấu <span class="required">*</span> là bắt buộc.</p>
    <?php echo MyFormat::BindNotifyMsg(); ?>
    
    <div class="row">
        <em class="hight_light item_b" style="margin-left: 140px;">Chú ý: Đề xuất giá sẽ áp dụng cho tháng kế tiếp. Nếu bạn muốn giá áp dụng luôn cho tháng hiện tại thì bạn mail cho Sếp Hoàn và cc cho IT Dũng để cập nhật luôn cho bạn</em>
        <?php echo $form->labelEx($model,'uid_approved_1'); ?>
        <?php echo $form->dropDownList($model, 'uid_approved_1', $model->getListApprovedLevel(GasOneManyBig::TYPE_PRICE_REQUEST_1), array('class' => 'w-400', 'empty'=>'Select')); ?>
        <?php echo $form->error($model,'uid_approved_1'); ?>
    </div>
    <?php include "_form_multi.php"; ?>
    <div class="row buttons" style="padding-left: 141px;">
        <?php $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType'=>'submit',
            'label'=>$model->isNewRecord ? 'Create' :'Save',
            'type'=>'null', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
            'size'=>'small', // null, 'large', 'small' or 'mini'
            //'htmlOptions' => array('style' => 'margin-bottom: 10px; float: right;'),
        )); ?>
    </div>

<?php $this->endWidget(); ?>

</div><!-- form -->
<script>
    $(document).ready(function(){
        $('.form').find('button:submit').click(function(){
            $.blockUI({ overlayCSS: { backgroundColor: '#fff' } }); 
        });
        
    });
</script>