<?php
$this->breadcrumbs=array(
	$this->singleTitle,
);

$menus=array(
        array('label'=>"Create $this->singleTitle", 'url'=>array('create')),
);

if($model->canCreateOrUpdate()){
    $this->menu= ControllerActionsName::createMenusRoles($menus, $actions);
}


Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('users-price-request-grid', {
                url : $(this).attr('action'),
		data: $(this).serialize()
	});
	return false;
});
");

Yii::app()->clientScript->registerScript('ajaxupdate', "
$('#users-price-request-grid a.ajaxupdate').live('click', function() {
    $.fn.yiiGridView.update('users-price-request-grid', {
        type: 'POST',
        url: $(this).attr('href'),
        success: function() {
            $.fn.yiiGridView.update('users-price-request-grid');
        }
    });
    return false;
});
");
?>

<h1>Danh Sách <?php echo $this->pluralTitle; ?></h1>


<?php echo CHtml::link('Tìm Kiếm Nâng Cao','#',array('class'=>'search-button')); ?>
<div class="search-form" style="">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'users-price-request-grid',
	'dataProvider'=>$model->search(),
        'afterAjaxUpdate'=>'function(id, data){ fnUpdateColorbox();}',
        'template'=>'{pager}{summary}{items}{pager}{summary}',
//    'itemsCssClass' => 'items custom_here',
//    'htmlOptions'=> array('class'=>'grid-view custom_here'),
        'pager' => array(
            'maxButtonCount'=>  CmsFormatter::$PAGE_MAX_BUTTON,
        ),
	'enableSorting' => false,
	//'filter'=>$model,
	'columns'=>array(
        array(
            'header' => 'S/N',
            'type' => 'raw',
            'value' => '$this->grid->dataProvider->pagination->currentPage * $this->grid->dataProvider->pagination->pageSize + ($row+1)',
            'headerHtmlOptions' => array('width' => '10px','style' => 'text-align:center;'),
            'htmlOptions' => array('style' => 'text-align:center;')
        ),
            array(
                'header' => 'Đề xuất tháng',
                'value'=>'$data->getMonthYear().$data->getHtmlIconStatus().$data->getTypeText()',
                'type' => 'raw',
                'htmlOptions' => array('style' => 'text-align:center;'),
            ),
            array(
                'header' => 'Chi tiết',
                'value'=>'$data->getDetailInfo()',
                'type' => 'raw',
            ),
            array(
                'name'=>'status',
                'value'=>'$data->getStatusText()',
                'htmlOptions' => array('style' => 'text-align:center;'),
            ),
            array(
                'header'=>'Người duyệt',
                'type'=> 'html',
                'value'=>'$data->getHistoryApproved()',
                'htmlOptions' => array('style' => 'width:150px;'),
            ),
            array(
                'name'=>'uid_login',
                'value'=>'$data->getUidLogin()',
                'htmlOptions' => array('style' => 'text-align:center;'),
            ),
            array(
                'name'=>'created_date',
                'value'=>'$data->getCreatedDate()',
                'htmlOptions' => array('style' => 'text-align:center;'),
            ),
            array(
                'header' => 'Actions',
                'class'=>'CButtonColumn',
                'template'=> ControllerActionsName::createIndexButtonRoles($actions),
                'buttons'=>array(
                    'update' => array(
                        'visible' => '$data->canUpdate()',
                    ),
                    'delete'=>array(
                        'visible'=> 'GasCheck::canDeleteData($data)',
                    ),
                ),
            ),
	),
)); ?>

<script>
$(document).ready(function() {
    fnUpdateColorbox();
});

function fnUpdateColorbox(){   
    fixTargetBlank();
    fnShowhighLightTr();
    $(".view").colorbox({
        iframe: true,
        innerHeight: '1000',
        innerWidth: '1200', close: "<span title='close'>close</span>"
    });
    $('.AddIconGrid').each(function(){
        var tr = $(this).closest('tr');
        tr.find('.button-column').append($(this).html());
    });
}
</script>