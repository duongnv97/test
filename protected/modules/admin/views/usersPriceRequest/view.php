<?php
$this->breadcrumbs=array(
    $this->pluralTitle => array('index'),
    ' ' . $this->singleTitle . ' : ',
);

$menus = array(
	array('label'=>"$this->pluralTitle Management", 'url'=>array('index')),
	array('label'=>"Tạo Mới $this->singleTitle", 'url'=>array('create')),
	array('label'=>"Cập Nhật $this->singleTitle", 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>"Xóa $this->singleTitle", 'url'=>array('delete'), 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Bạn chắc chắn muốn xóa ?')),
);
$this->menu= ControllerActionsName::createMenusRoles($menus, $actions);
$model->showDropdown=true;
?>

<h1>Xem: <?php echo $this->singleTitle; ?></h1>

<?php echo MyFormat::BindNotifyMsg(); ?>
<?php if($model->canViewApprovedForm()):?>
    <?php include "view_update_status.php"; ?>
<?php endif;?>

<?php $model->showDropdown=false; include "view_grid.php"; ?>
<style>
.detail-view tbody tr th { width: 100px;} 
</style>
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/autoNumeric-master/autoNumeric.js"></script>
<script>
$(document).ready(function() {
    fnShowhighLightTr();
    gSelectChange(<?php echo UsersRef::PRICE_OTHER;?>);
    fnInitInputCurrency();
});

</script>