<div class="form">
<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'sell-form',
	'enableAjaxValidation'=>false,
        'htmlOptions' => array('enctype' => 'multipart/form-data'),
)); ?>
<?php
    $fieldApproved      = $model->getFieldApproved();
    $model->formApproved = $form;
    $model->classTableInfo  = 'materials_table hm_table w-800';
?>
    
<div class="row buttons" style="padding-left: 141px;">
    <?php $this->widget('bootstrap.widgets.TbButton', array(
        'buttonType'=>'submit',
        'label'=> "Save",
        'type'=>'null', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
        'size'=>'small', // null, 'large', 'small' or 'mini'
        //'htmlOptions' => array('style' => 'margin-bottom: 10px; float: right;'),
    )); ?>
    <input class='cancel_iframe' type='button' value='Close'>
</div>

<?php if($model->showDropdownApproved()): ?>
<div class="row">
    <?php echo $form->labelEx($model, $fieldApproved); ?>
    <?php echo $form->dropDownList($model, $fieldApproved, $model->listdataApproved,array('class'=>'w-400', 'empty'=>'Select')); ?>
    <?php echo $form->error($model, $fieldApproved); ?>
</div>
<?php endif; ?>

<div class="row">
    <?php echo $form->labelEx($model,'status'); ?>
    <?php // echo $form->dropDownList($model,'status', $model->listdataStatus,array('class'=>'w-400', 'empty'=>'Select')); ?>
    <?php echo $form->dropDownList($model,'status', $model->listdataStatus,array('class'=>'w-400 UpdateStatus')); ?>
    <?php echo $form->error($model,'status'); ?>
</div>
<div class="row">
    <?php echo $form->labelEx($model, $model->fieldNote); ?>
    <?php echo $form->textArea($model, $model->fieldNote,array('class'=>'w-400 h_80')); ?>
    <?php echo $form->error($model, $model->fieldNote); ?>
</div>

<div class="row">
    <!--<label>&nbsp;</label>-->
    <?php echo $model->getDetailInfo(); ?>
</div>
<br>
<div class="row buttons" style="padding-left: 141px;">
    <?php $this->widget('bootstrap.widgets.TbButton', array(
        'buttonType'=>'submit',
        'label'=> "Save",
        'type'=>'null', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
        'size'=>'small', // null, 'large', 'small' or 'mini'
        //'htmlOptions' => array('style' => 'margin-bottom: 10px; float: right;'),
    )); ?>
    <input class='cancel_iframe' type='button' value='Close'>
</div>
<?php $this->endWidget(); ?>
</div>
<script>
    $(window).load(function(){
        updateStatusChange();
        parent.$.fn.yiiGridView.update('users-price-request-grid');
    });
    
    function  updateStatusChange(){
        $('.UpdateStatus').change(function(){
            var valueSelect = $(this).val();
            if(valueSelect == <?php echo UsersPriceRequest::STATUS_REJECT;?>){
                $('.ActionApproved').val(<?php echo UsersPriceRequest::ITEM_REJECT;?>);
            }else{
                $('.ActionApproved').val(<?php echo UsersPriceRequest::ITEM_APPROVED;?>);   
            }
        });
    }
    
</script>