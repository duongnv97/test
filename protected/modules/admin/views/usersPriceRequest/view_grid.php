<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
            array(
                'label' => 'Đề xuất tháng',
                'value'=> $model->getMonthYear(),
                'htmlOptions' => array('style' => 'text-align:center;'),
            ),
            array(
                'label' => 'Chi tiết',
                'value'=>$model->getDetailInfo(),
                'type' => 'raw',
            ),
            array(
                'name'=>'status',
                'value'=>$model->getStatusText(),
                'htmlOptions' => array('style' => 'text-align:center;'),
            ),
            array(
                'header'=>'Người duyệt',
                'type'=> 'html',
                'value'=>$model->getHistoryApproved(),
            ),
            array(
                'name'=>'uid_login',
                'value'=>$model->getUidLogin(),
                'htmlOptions' => array('style' => 'text-align:center;'),
            ),
            array(
                'name'=>'created_date',
                'value'=>$model->getCreatedDate(),
                'htmlOptions' => array('style' => 'text-align:center;'),
            ),
	),
)); ?>
