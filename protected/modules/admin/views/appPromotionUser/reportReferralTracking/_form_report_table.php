<?php
$this->breadcrumbs=array(
    $this->pageTitle,
);
?>
<h1><?php echo $this->pageTitle; ?></h1>

<?php 
$gridCss = Yii::app()->getAssetManager()->publish(Yii::getPathOfAlias('zii.widgets.assets')) . '/gridview/styles.css';
Yii::app()->getClientScript()->registerCssFile($gridCss);
?>
<?php echo CHtml::link('Tìm Kiếm Nâng Cao','#',array('class'=>'search-button')); ?>
<div class="search-form" style="">
<?php $this->renderPartial('reportReferralTracking/_search',array(
        'model'=>$model,
)); ?>
</div><!-- search-form -->

<div class="grid-view">
    <table class="items hm_table tableReport" style='width:100%;'>
        <thead>
            <tr>
                <th class='item_c'>Mã</th>
                <th class='item_c'>Tên</th>
                <th class='item_c'>Số điện thoại</th>
                <?php for ($i =1; $i<= $model->countLevel;$i++){ ?>
                <th class='item_c'>Cấp <?php echo $i; ?></th>
                <?php } ?>
                <th class='item_c'>Tổng</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($aData as $key => $value) { ?>
             <tr>
                <td><?php echo isset($value['code_no']) ? $value['code_no'] : ''; ?></td>
                <td><?php echo isset($value['name']) ? $value['name'] : ''; ?></td>
                <td><?php echo isset($value['phone']) ? $value['phone'] : ''; ?></td>
                <?php for ($i =1; $i<= $model->countLevel;$i++){ ?>
                <td><?php echo  isset($value[$i]) ? $value[$i] : ''; ?></td>
                <?php } ?>
                <td><?php echo isset($value['sum']) ? $value['sum'] : '' ?></td>
            </tr>
            <?php } ?>
        </thead>
    </table>
</div>


