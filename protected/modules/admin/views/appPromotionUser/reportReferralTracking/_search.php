<div class="wide form">
    <?php $form=$this->beginWidget('CActiveForm', array(
            'action'=>Yii::app()->createUrl($this->route),
            'method'=>'get',
    )); 

    Yii::app()->clientScript->registerScript('search', "
    $('.search-button').click(function(){
            $('.search-form').toggle();
            return false;
    });
    $('.search-form form').submit(function(){
            $.fn.yiiGridView.update('referraltracking-grid', {
                    url : $(this).attr('action'),
                    data: $(this).serialize()
            });
            return false;
    });
    ");
    ?>
    <div class="row">
        <?php echo $form->label($model,'ref_id'); ?>
        <?php echo $form->hiddenField($model,'ref_id'); ?>
        <?php 
            // widget auto complete search user customer and supplier
            $aData = array(
                'model'=>$model,
                'field_customer_id'=>'ref_id',
                'name_relation_user'=>'rCustomer',
                'url' => Yii::app()->createAbsoluteUrl('admin/ajax/searchHgd', ['customer_type'=> UsersExtend::STORE_CARD_HGD_APP]),
                'ClassAdd' => 'w-500',
            );
            $this->widget('ext.SpaAutocompleteCustomer.SpaAutocompleteCustomer',
                array('data'=>$aData));                                        
        ?>
    </div>
    <div class="row">
        <?php echo $form->labelEx($model,'code_no'); ?>
        <?php echo $form->textField($model,'code_no',array('class'=>'w-200','maxlength'=>25)); ?>
    </div>
    
    <div class="row more_col">
        <div class="col1">
            <?php echo Yii::t('translation', $form->label($model,'date_from')); ?>
            <?php 
                $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                    'model'=>$model,        
                    'attribute'=>'date_from',
                    'options'=>array(
                        'showAnim'=>'fold',
                        'dateFormat'=> MyFormat::$dateFormatSearch,
//                            'minDate'=> '0',
//                        'maxDate'=> '0',
                        'changeMonth' => true,
                        'changeYear' => true,
                        'showOn' => 'button',
                        'buttonImage'=> Yii::app()->theme->baseUrl.'/admin/images/icon_calendar_r.gif',
                        'buttonImageOnly'=> true,                                
                    ),
                    'htmlOptions'=>array(
                        'class'=>'w-16 date_from',
                        'size'=>'16',
                        'class_update_val'=>'date_from',
                        'style'=>'float:left;',                               
                    ),
                ));
            ?>
        </div>
        <div class="col2">
            <?php echo Yii::t('translation', $form->label($model,'date_to')); ?>
            <?php 
                $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                    'model'=>$model,        
                    'attribute'=>'date_to',
                    'options'=>array(
                        'showAnim'=>'fold',
                        'dateFormat'=> MyFormat::$dateFormatSearch,
//                            'minDate'=> '0',
//                        'maxDate'=> '0',
                        'changeMonth' => true,
                        'changeYear' => true,
                        'showOn' => 'button',
                        'buttonImage'=> Yii::app()->theme->baseUrl.'/admin/images/icon_calendar_r.gif',
                        'buttonImageOnly'=> true,                                
                    ),
                    'htmlOptions'=>array(
                        'class'=>'w-16 date_to',
                        'size'=>'16',
                        'class_update_val'=>'date_to',
                        'style'=>'float:left;',
                    ),
                ));
            ?>
        </div>
    </div>
    <div class="row">
        <?php echo $form->labelEx($model,'Loại ngày'); ?>
        <?php echo $form->dropDownList($model,'type_search', $model->getArraySearchRef(),array('empty'=>'select','class'=>'w-300')); ?>
    </div>
    <div class="row buttons" style="padding-left: 159px;">
        <?php $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType'=>'submit',
            'label'=>'Search',
            'type'=>'null', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
            'size'=>'small', // null, 'large', 'small' or 'mini'
            //'htmlOptions' => array('style' => 'margin-bottom: 10px; float: right;'),
        )); ?>	
    </div>
<?php $this->endWidget(); ?>
</div><!-- search-form -->