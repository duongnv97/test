<?php
$this->breadcrumbs=array(
	$this->singleTitle,
);

$menus=array(
            array('label'=>"Create $this->singleTitle", 'url'=>array('create')),
    //    @Code: NAM010
//            array('label'=> 'Xuất Excel Danh Sách Hiện Tại',
//               'url'=>array('ExportExcel'), 
//               'htmlOptions'=>array('class'=>'export_excel ','label'=>'Xuất Excel')),
);
$cRole = MyFormat::getCurrentRoleId();
$mTelesale = new Telesale();
if($mTelesale->showSetSaleId()){
    $menus[] = array('label'=> 'Xuất Excel Danh Sách Hiện Tại',
               'url'=>array('ExportExcel'), 
               'htmlOptions'=>array('class'=>'export_excel ','label'=>'Xuất Excel'));
}


$this->menu= ControllerActionsName::createMenusRoles($menus, $actions);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('app-promotion-user-grid', {
                url : $(this).attr('action'),
		data: $(this).serialize()
	});
	return false;
});
");

Yii::app()->clientScript->registerScript('ajaxupdate', "
$('#app-promotion-user-grid a.ajaxupdate').live('click', function() {
    $.fn.yiiGridView.update('app-promotion-user-grid', {
        type: 'POST',
        url: $(this).attr('href'),
        success: function() {
            $.fn.yiiGridView.update('app-promotion-user-grid');
        }
    });
    return false;
});
");
?>

<h1>Danh Sách <?php echo $this->pluralTitle; ?></h1>

<?php echo MyFormat::BindNotifyMsg(); ?>

<?php echo CHtml::link('Tìm Kiếm Nâng Cao','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'app-promotion-user-grid',
	'dataProvider'=>$model->search(),
        'afterAjaxUpdate'=>'function(id, data){ fnUpdateColorbox();}',
        'template'=>'{pager}{summary}{items}{pager}{summary}',
//    'itemsCssClass' => 'items custom_here',
//    'htmlOptions'=> array('class'=>'grid-view custom_here'),
        'pager' => array(
            'maxButtonCount'=>  CmsFormatter::$PAGE_MAX_BUTTON,
        ),
	'enableSorting' => false,
	//'filter'=>$model,
	'columns'=>array(
        array(
            'header' => 'S/N',
            'type' => 'raw',
            'value' => '$this->grid->dataProvider->pagination->currentPage * $this->grid->dataProvider->pagination->pageSize + ($row+1)',
            'headerHtmlOptions' => array('width' => '10px','style' => 'text-align:center;'),
            'htmlOptions' => array('style' => 'text-align:center;')
        ),
		array(
                    'name'  => 'user_id',
                    'type'  => 'raw',
                    'value'  => '$data->getUserInfo()',
                ),
		array(
                    'header'  => 'Đại lý',
                    'type'  => 'raw',
                    'value'  => '$data->getAgent()',
                ),
		'promotion_title',
                array(
                    'name'  => 'code_no',
                    'htmlOptions' => array('style' => 'text-align:center;')
                ),
                array(
                    'name'  => 'promotion_amount',
                    'value' => '$data->getPromotionAmount(true)',
                    'htmlOptions' => array('style' => 'text-align:right;')
                ),
                array(
                    'name'  => 'time_use',
                    'htmlOptions' => array('style' => 'text-align:center;')
                ),
                array(
                    'name'  => 'time_use_real',
                    'htmlOptions' => array('style' => 'text-align:center;')
                ),
                array(
                    'name'  => 'expiry_date',
                    'type' => 'raw',
                    'value'  => '$data->getExpiryDate()',
                    'htmlOptions' => array('style' => 'text-align:center;')
                ),
                array(
                    'name'  => 'promotion_type',
                    'type'  => 'raw',
                    'value'  => '$data->getpromotionType()',
                ),
                array(
                    'name'  => 'status_use',
                    'value'  => '$data->getStatusUse()',
                    'type'  => 'raw',
                    'htmlOptions' => array('style' => 'text-align:center;')
                ),
                array(
                    'name'  => 'date_apply',
                    'value'  => '$data->getDateApply()',
                    'htmlOptions' => array('style' => 'text-align:center;')
                ),
                array(
                    'name'  => 'created_date',
                    'type'  => 'raw',
                    'value'  => '$data->getCreatedDate()."<br>".$data->id',
                ),
                array(
                    'name'  => 'uid_login',
                    'value'  => '$data->getUidLogin()',
                ),
		array(
                    'header' => 'Actions',
                    'class'=>'CButtonColumn',
                    'template'=> ControllerActionsName::createIndexButtonRoles($actions),
                    'buttons'=>array(
                        'update' => array(
                            'visible' => '$data->canUpdate()',
                        ),
                        'delete'=>array(
                            'visible'=> 'GasCheck::canDeleteData($data)',
                        ),
                    ),
		),
	),
)); ?>

<script>
$(document).ready(function() {
    fnUpdateColorbox();
});

function fnUpdateColorbox(){
    fixTargetBlank();
    fnShowhighLightTr();
}
</script>