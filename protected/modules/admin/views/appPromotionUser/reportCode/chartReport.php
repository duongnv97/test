<?php
if (isset($totalDataChart)) {
    /** @var array $totalDataChart list value for lines */
    $jsArrayTotalCall = json_encode($totalDataChart);
    $jsArrayTotalSell = json_encode($totalDataChartSell);
    $jsArrayTotalLocation = json_encode($totalDataChartLocation);
    ?>
    <div class="float_l" style="width: 1100px">
        <div id="success_location_chart" style="width: 1100px; height: 500px"></div>
    </div>
    <div class="float_l" style="width: 1100px">
        <div id="success_call_chart" style="width: 1100px; height: 500px"></div>
    </div>
    <div class="float_l" style="width: 1100px">
        <div id="success_sell_chart" style="width: 1100px; height: 500px"></div>
    </div>
    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script type="text/javascript">
        //        fnAddClassOddEven('items');
        google.charts.load("current", {packages: ["corechart"]});
        google.charts.setOnLoadCallback(drawChart);
        function drawChart() {
            // Making a 3D Pie Chart https://developers.google.com/chart/interactive/docs/gallery/piechart?hl=vi
            var data = google.visualization.arrayToDataTable(<?php echo $jsArrayTotalCall; ?>);

            var options = {
                title: 'Khách hàng đã nhập mã CODE Telesale',
                is3D: true,
            };
            var chart = new google.visualization.PieChart(document.getElementById('success_call_chart'));
            chart.draw(data, options);
            
            // Making a 3D Pie Chart https://developers.google.com/chart/interactive/docs/gallery/piechart?hl=vi
            var data2 = google.visualization.arrayToDataTable(<?php echo $jsArrayTotalSell; ?>);
            var options2 = {
                title: 'Tổng số bình quay về',
                is3D: true,
            };
            var chart2 = new google.visualization.PieChart(document.getElementById('success_sell_chart'));
            chart2.draw(data2, options2);
            
            // Making a 3D Pie Chart https://developers.google.com/chart/interactive/docs/gallery/piechart?hl=vi
            var data3 = google.visualization.arrayToDataTable(<?php echo $jsArrayTotalLocation; ?>);
            var options3 = {
                title: 'Location NULL',
                is3D: true,
            };
            var chart3 = new google.visualization.PieChart(document.getElementById('success_location_chart'));
            chart3.draw(data3, options3);
        }
    </script>
<?php } ?>