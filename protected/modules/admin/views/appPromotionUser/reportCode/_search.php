<?php 
$urlExcel = Yii::app()->createAbsoluteUrl('admin/appPromotionUser/CodePtttCheckLocation', ['ToExcel'=>1]);
?>
<!-- Used for agent call report search area -->
<div class="wide form">

<?php 
$mFakeGasReferral = new GasReferralTracking();
$form=$this->beginWidget('CActiveForm', array(
//	'action'=> GasCheck::getCurl(),
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>
    
    <div class="row more_col">
        <div class="col1">
            <?php $mOneMany = new GasOneMany; ?>
            <?php echo $form->labelEx($model,'monitoring_id',array('label'=>'Xem CV PTTT','class'=>'checkbox_one_label', 'style'=>'padding-top:3px;')); ?>
            <?php echo $form->dropDownList($model,'monitoring_id', $mOneMany->getListdataByType(GasOneMany::TYPE_MONITOR_GDKV), array('style'=>'','class'=>'w-200','empty'=>'Select')); ?>
        </div>
        <div class="col2">
            <?php echo $form->labelEx($model,'status_use'); ?>
            <?php echo $form->dropDownList($model,'status_use', $mFakeGasReferral->getArrayStatus(), array('class'=>'w-200', 'empty'=>'Select')); ?>
        </div>
    </div>

    <div class="row more_col">
        <div class="col1">
                <?php echo $form->label($model,'date_from'); ?>
                <?php 
                    $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                        'model'=>$model,        
                        'attribute'=>'date_from',
                        'options'=>array(
                            'showAnim'=>'fold',
                            'dateFormat'=> MyFormat::$dateFormatSearch,
//                            'minDate'=> '0',
                            'maxDate'=> '0',
                            'changeMonth' => true,
                            'changeYear' => true,
                            'showOn' => 'button',
                            'buttonImage'=> Yii::app()->theme->baseUrl.'/admin/images/icon_calendar_r.gif',
                            'buttonImageOnly'=> true,                                
                        ),        
                        'htmlOptions'=>array(
                            'class'=>'w-150 float_l',
                            'readonly' => 1,
                        ),
                    ));
                ?>     		
        </div>
        <div class="col2">
                <?php echo $form->label($model,'date_to'); ?>
                <?php 
                    $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                        'model'=>$model,        
                        'attribute'=>'date_to',
                        'options'=>array(
                            'showAnim'=>'fold',
                            'dateFormat'=> MyFormat::$dateFormatSearch,
//                            'minDate'=> '0',
                            'maxDate'=> '0',
                            'changeMonth' => true,
                            'changeYear' => true,
                            'showOn' => 'button',
                            'buttonImage'=> Yii::app()->theme->baseUrl.'/admin/images/icon_calendar_r.gif',
                            'buttonImageOnly'=> true,                                
                        ),        
                        'htmlOptions'=>array(
                            'class'=>'w-150 float_l',
                            'readonly' => 1,
                        ),
                    ));
                ?>     		
        </div>
    </div>
    
    <div class="row">
        <?php echo $form->labelEx($model,'Nhân viên'); ?>
        <?php echo $form->hiddenField($model,'user_id', array('class'=>'')); ?>
        <?php
            // 1. limit search kh của sale
            $url = Yii::app()->createAbsoluteUrl('admin/ajax/search_by_role', array('role'=> implode(',', $model->getRoleSaleIdHgd())));
            // widget auto complete search user customer and supplier
            $aData = array(
                'model'=>$model,
                'field_customer_id'=>'user_id',
                'url'=> $url,
                'name_relation_user'=>'rUser',
                'ClassAdd' => 'w-400',
                'field_autocomplete_name' => 'autocomplete_name_1',
                'placeholder'=>'Nhập mã NV hoặc tên',
            );
            $this->widget('ext.SpaAutocompleteCustomer.SpaAutocompleteCustomer',
                array('data'=>$aData));
        ?>
    </div>
    
    <div class="row buttons" style="padding-left: 159px;">
        <?php $this->widget('bootstrap.widgets.TbButton', array(
        'buttonType'=>'submit',
        'label'=>'Xem',
        'type'=>'null', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
        'size'=>'small', // null, 'large', 'small' or 'mini'
        //'htmlOptions' => array('style' => 'margin-bottom: 10px; float: right;'),
    )); ?>
    &nbsp;&nbsp;&nbsp;&nbsp;<a class='btn_cancel' target="_blank" href='<?php echo $urlExcel;?>'>Xuất Excel</a>
    </div>
    

<?php $this->endWidget(); ?>

</div><!-- search-form -->