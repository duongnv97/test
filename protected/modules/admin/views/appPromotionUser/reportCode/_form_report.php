<?php
$this->breadcrumbs=array(
    $this->pageTitle,
);
$menus=array(
);
$this->menu= ControllerActionsName::createMenusRoles($menus, $actions);
?>

<h1><?php echo $this->pageTitle;?></h1>
<style>
    .ui-datepicker-trigger { float: left;}
</style>
<div class="search-form" style="">
<?php $this->renderPartial('reportCode/_search',array(
        'model'=>$model,
)); ?>
</div><!-- search-form -->
<script>
$(document).ready(function(){
    $('.form').find('button:submit').click(function(){        
        $.blockUI({ overlayCSS: { backgroundColor: '#fff' } }); 
    });   
 
});
</script>
<?php if(isset($data['OUT_TRACKING'])): ?>

<?php
$gridCss = Yii::app()->getAssetManager()->publish(Yii::getPathOfAlias('zii.widgets.assets')) . '/gridview/styles.css';
Yii::app()->getClientScript()->registerCssFile($gridCss);
$LIST_EMPLOYEE              = isset($data['EMPLOYEE']) ? $data['EMPLOYEE'] : [];
$OUT_TRACKING               = isset($data['OUT_TRACKING']) ? $data['OUT_TRACKING']: [];
$OUT_TRACKING_DATE          = isset($data['OUT_TRACKING_DATE']) ? $data['OUT_TRACKING_DATE']: [];
$DATE_ARRAY                 = isset($data['ARRAY_DATE']) ? $data['ARRAY_DATE']: [];
$OUT_SELL                   = isset($data['OUT_SELL']) ? $data['OUT_SELL']: [];
$OUT_SELL_CHART             = isset($data['OUT_SELL_CHART']) ? $data['OUT_SELL_CHART']: [];
$SUM_OUT_TRACKING           = isset($data['SUM_OUT_TRACKING']) ? $data['SUM_OUT_TRACKING']: [];
$SUM_OUT_SELL               = isset($data['SUM_OUT_SELL']) ? $data['SUM_OUT_SELL']: [];
$LOCATION_NULL              = isset($data['LOCATION_NULL']) ? $data['LOCATION_NULL']: [];
$SUM_LOCATION_NULL          = isset($data['SUM_LOCATION_NULL']) ? $data['SUM_LOCATION_NULL']: [];
$index = 1;
// Get table height
$freeHeightTable = (sizeof($LIST_EMPLOYEE) + 1) * 19;
$freeHeightTable = $freeHeightTable > 400 ? 400 : $freeHeightTable;

// For piechart all total call
$totalDataChart[] = ['Nhân viên', 'Cuộc gọi'];
$totalDataChartSell[] = ['Nhân viên', 'Bán hàng'];
$totalDataChartLocation[] = ['Nhân viên', 'Location NULL'];
?>
<script type="text/javascript"
        src="<?php echo Yii::app()->theme->baseUrl; ?>/js/scroll/jquery.freezetablecolumns.1.1.js"></script>
<div class="grid-view display_none">
    <table id="freezetablecolumns_report_call" class="items hm_table freezetablecolumns">
        <thead>
        <tr  class="h_30">
            <th class="w-20 ">#</th>
            <th class="w-150 ">Nhân viên</th>
            <th class="w-150" style="white-space:normal;">Tọa độ rỗng/KH nhập mã/BQV</th>
            <?php foreach ($DATE_ARRAY as $days): ?>
                <th class="item_b"><?php echo substr($days, -2); ?></th>
            <?php endforeach; ?>
        </tr>
        </thead>
        <tbody>
        <?php
        //@Code: NAM013
        ?>
            <tr class="h_20">
                <td class="item_b item_c" colspan="2">Tổng</td>
                <?php
                $valueSum          = isset($SUM_OUT_TRACKING['SUM']) ? $SUM_OUT_TRACKING['SUM'] : '0';
                $saleValueSum      = isset($SUM_OUT_SELL['SUM']) ? $SUM_OUT_SELL['SUM'] : '0';
                $locationValueSum  = isset($SUM_LOCATION_NULL) ? array_sum($SUM_LOCATION_NULL) : '0';
                $textSum = $locationValueSum.'/'.$valueSum.'/'.$saleValueSum;
                if ($textSum == '0/0/0'):
                    $textSum = '';
                endif;
                ?>
                <td class="item_c"><?php echo $textSum; ?></td>
            <?php foreach ($DATE_ARRAY as $days): ?>
                <?php
                $valueDate          = isset($SUM_OUT_TRACKING['DATE'][$days]) ? $SUM_OUT_TRACKING['DATE'][$days] : '0';
                $salevalueDate      = isset($SUM_OUT_SELL['DATE'][$days]) ? $SUM_OUT_SELL['DATE'][$days] : '0';
                $locationvalueDate  = isset($SUM_LOCATION_NULL[$days]) ? $SUM_LOCATION_NULL[$days] : '0';
                $textDate = $locationvalueDate.'/'.$valueDate.'/'.$salevalueDate;
                if ($textDate == '0/0/0'):
                    $textDate = '';
                endif;
                ?>
                <td class="w-60 item_c"><?php echo $textDate; ?></td>
            <?php endforeach; ?>
            </tr>
            
        <?php foreach ($OUT_TRACKING as $user_id => $value): 
                if(!empty($model->user_id && $model->user_id != $user_id)){
                    continue;
                }
                $name                       = isset($LIST_EMPLOYEE[$user_id]) ? $LIST_EMPLOYEE[$user_id] : '';
                $nRef                       = isset($OUT_TRACKING[$user_id]) ? (int)$OUT_TRACKING[$user_id] : 0;
                $nSale                      = isset($OUT_SELL_CHART[$user_id]['value']) ? $OUT_SELL_CHART[$user_id]['value'] : '0';
                $nLocation                  = isset($LOCATION_NULL[$user_id]) ? array_sum($LOCATION_NULL[$user_id]) : '0';
                $totalDataChart[]           = [$name, $nRef];
                $totalDataChartSell[]       = [$name, $nSale];
                $totalDataChartLocation[]   = [$name, (int)$nLocation];
                $textSum = $nLocation.'/'.$nRef.'/'.$nSale;
                if ($textSum == '0/0/0'):
                    continue ;
                endif;
            ?>
            <tr class="h_20">
                <td class="item_c"><?php echo $index++; ?></td>
                <td class="item_b"><?php echo $name; ?></td>
                <td class="item_c item_b"><?php echo $textSum; ?></td>
                <?php foreach ($DATE_ARRAY as $days): ?>
                    <?php
                    $value              = isset($OUT_TRACKING_DATE[$user_id][$days]) ? $OUT_TRACKING_DATE[$user_id][$days] : '0';
                    $saleValue          = isset($OUT_SELL[$user_id][$days]) ? $OUT_SELL[$user_id][$days] : '0';
                    $locationValue      = isset($LOCATION_NULL[$user_id][$days]) ? $LOCATION_NULL[$user_id][$days] : '0';
                    $text = $locationValue.'/'.$value.'/'.$saleValue;
                    if ($text == '0/0/0'):
                        $text = '';
                    endif;
                    ?>
                    <td class="w-60 item_c"><?php echo $text; ?></td>
                <?php endforeach; ?>
            </tr>
        <?php endforeach; ?>
        </tbody>
    </table>
</div>


<script type="text/javascript">
    fnAddClassOddEven('items');
    $('.freezetablecolumns').each(function () {
        var id_table = $(this).attr('id');
        $('#' + id_table).freezeTableColumns({
            width:       <?php echo Users::WIDTH_freezetable;?>,   // required - bt 1100
            height:      <?php echo $freeHeightTable;?>,   // required
            numFrozen: 3,     // optional
            frozenWidth: 370,   // optional
            clearWidths: true  // optional
        });
    });
    $(window).load(function () {
        var index = 1;
        $('.freezetablecolumns').each(function () {
            if (index == 1)
                $(this).closest('div.grid-view').show();
            index++;
        });
        fnAddClassOddEven('items');
    });
</script>
<?php include('chartReport.php'); ?>
<?php endif; ?>