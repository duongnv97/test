<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'app-promotion-user-form',
	'enableAjaxValidation'=>false,
        'htmlOptions' => array('enctype' => 'multipart/form-data'),
)); ?>

    <p class="note">Dòng có dấu <span class="required">*</span> là bắt buộc.</p>
    <?php echo MyFormat::BindNotifyMsg(); ?>

    <div class="row">
        <?php echo $form->labelEx($model,'status_use'); ?>
        <?php echo $model->getStatusUse(); ?>
    </div>
    <div class="row">
        <?php echo $form->labelEx($model,'user_id'); ?>
        <?php echo $form->hiddenField($model,'user_id'); ?>
        <?php 
            // widget auto complete search user customer and supplier
            $aData = array(
                'model'=>$model,
                'field_customer_id'=>'user_id',
                'name_relation_user'=>'rUser',
                'url' => Yii::app()->createAbsoluteUrl('admin/ajax/searchHgd', ['customer_type'=> UsersExtend::STORE_CARD_HGD_APP]),
                'ClassAdd' => 'w-400',
            );
            $this->widget('ext.SpaAutocompleteCustomer.SpaAutocompleteCustomer',
                array('data'=>$aData));                                        
        ?>
        <?php echo $form->error($model,'user_id'); ?>
    </div>
    
    <div class="row">
        <?php echo $form->labelEx($model,'promotion_id'); ?>
        <?php echo $form->dropDownList($model,'promotion_id', $model->getArrayPromotion(), array('class'=>'w-400', 'empty'=>'Select')); ?>
        <?php echo $form->error($model,'promotion_id'); ?>
    </div>
    <div class="row buttons" style="padding-left: 141px;">
        <?php $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType'=>'submit',
            'label'=>$model->isNewRecord ? 'Create' :'Save',
            'type'=>'null', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
            'size'=>'small', // null, 'large', 'small' or 'mini'
            //'htmlOptions' => array('style' => 'margin-bottom: 10px; float: right;'),
        )); ?>	
    </div>

<?php $this->endWidget(); ?>

</div><!-- form -->
<script>
    $(document).ready(function(){
        $('.form').find('button:submit').click(function(){
            $.blockUI({ overlayCSS: { backgroundColor: '#fff' } }); 
        });
        
    });
</script>