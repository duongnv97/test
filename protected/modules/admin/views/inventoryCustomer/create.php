<?php
$this->breadcrumbs=array(
	$this->pageTitle=>array('index'),
	'Tạo Mới',
);

$aParams = ['index'];
if(isset($_GET['type'])){
    $aParams = ['index', 'type'=>$_GET['type']];
}

$menus = array(		
        array('label'=>"$this->singleTitle Management", 'url'=> $aParams),
);
$this->menu= ControllerActionsName::createMenusRoles($menus, $actions);
?>

<h1>Nhập <?php echo $this->pageTitle;?></h1>

<div class="form">
    <?php $form=$this->beginWidget('CActiveForm', array(
            'id'=>'inventory-customer-form',
            'enableAjaxValidation'=>false,
            'htmlOptions' => array('enctype' => 'multipart/form-data'),
    )); ?>
    <?php echo $form->hiddenField($model,'id', array('class'=>'')); ?>
    <?php echo $this->renderPartial('_form_multi', array('model'=>$model)); ?>
    <?php $this->endWidget(); ?>
</div>