<?php
    $aModelUser = $model->getCacheArraytCustomerDebit();
    $index=1;
?>

<div class="clr"></div>
<div class="row">
    <label>&nbsp</label>
    <table class="materials_table hm_table tb_deloy_by w-500">
        <thead>
            <tr>
                <th class="item_c w-20">#</th>
                <th class="item_code item_c w-300">Khách hàng</th>
                <th class="item_unit last item_c">Xóa</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach($aModelUser as $key => $value): ?>
            <?php $mUser = Users::model()->findByPk($value); ?>
            <tr>
                <input name="InventoryCustomer[customer_id][<?php echo $value?>]"  value="<?php echo $value ?>" type="hidden">
                <td class="item_c order_no"><?php echo $index++ ?></td>
                <td class="uid_<?php echo $value?>"><?php echo ($mUser) ? $mUser->code_bussiness ." - ".$mUser->first_name : '' ;?></td>
                <td class="item_c last"><span class="remove_icon_only"></span></td>
            </tr>
            <?php endforeach;?>
        </tbody>
        
    </table>
</div>
