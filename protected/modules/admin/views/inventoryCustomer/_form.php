<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'inventory-customer-form',
	'enableAjaxValidation'=>false,
        'htmlOptions' => array('enctype' => 'multipart/form-data'),
)); ?>

    <p class="note">Dòng có dấu <span class="required">*</span> là bắt buộc.</p>
    <?php echo MyFormat::BindNotifyMsg(); ?>

    <div class="row">
    <?php echo $form->labelEx($model,'customer_id'); ?>
    <?php echo $form->hiddenField($model,'customer_id', array('class'=>'')); ?>
    <?php
            // 1. limit search kh của sale
            $url = Yii::app()->createAbsoluteUrl('admin/ajax/search_user_by_code');
            // widget auto complete search user customer and supplier
            $aData = array(
                'model'=>$model,
                'field_customer_id'=>'customer_id',
                'url'=> $url,
                'name_relation_user'=>'rCustomer',
                'ClassAdd' => 'w-400',
            );
            $this->widget('ext.SpaAutocompleteCustomer.SpaAutocompleteCustomer',
                array('data'=>$aData));
            ?>
        <?php echo $form->error($model,'customer_id'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model,'debit'); ?>
        <?php echo $form->textField($model,'debit',array('class'=>' w-150 f_size_15 ad_fix_currency item_r','maxlength'=>15)); ?>
        <?php echo $form->error($model,'debit'); ?>
    </div>
    <div class="row">
        <?php echo $form->labelEx($model,'credit'); ?>
        <?php echo $form->textField($model,'credit',array('class'=>' w-150 f_size_15 ad_fix_currency item_r','maxlength'=>15)); ?>
        <?php echo $form->error($model,'credit'); ?>
    </div>
    <div class="row ClassTdVo">
        <?php echo $form->labelEx($model,'json_vo'); ?>
        <div class="float_l">
            <?php $aInventoryVo = $model->getJsonUpdate(); ?>
            <?php include '_form_material.php'; ?>
        </div>
    </div>
    <div class="clr"></div>
    <div class="row buttons" style="padding-left: 141px;">
        <?php $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType'=>'submit',
            'label'=>$model->isNewRecord ? 'Create' :'Save',
            'type'=>'null', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
            'size'=>'small', // null, 'large', 'small' or 'mini'
            //'htmlOptions' => array('style' => 'margin-bottom: 10px; float: right;'),
        )); ?>	
    </div>

<?php $this->endWidget(); ?>

</div><!-- form -->
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/autoNumeric-master/autoNumeric.js"></script>
<script>
    $(document).ready(function(){
        $('.form').find('button:submit').click(function(){
            $.blockUI({ overlayCSS: { backgroundColor: '#fff' } }); 
        });
        fnInitInputCurrency();
        bindEventAddVo();
        fnBindAllAutocomplete();
    });
    
    function fnBindAllAutocomplete(){
        // material
        $('.material_autocomplete').each(function(){
            bindMaterialAutocomplete($(this));
        });
    }
</script>