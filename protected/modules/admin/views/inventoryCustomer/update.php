<?php
$this->breadcrumbs = array(
	$this->pluralTitle => array('index'),
        $model->id=>array('view','id'=>$model->id),
	'Cập Nhật ' . $this->pageTitle,
);

$aParamsIndex   = ['index', 'type'=> $model->type];
$aParamsCreate  = ['create', 'type'=> $model->type];

$menus = array(	
    array('label' => $this->pluralTitle, 'url' => $aParamsIndex),
    array('label' => 'Xem ' . $this->singleTitle, 'url' => array('view', 'id' => $model->id)),	
    array('label' => 'Tạo Mới ' . $this->singleTitle, 'url' => $aParamsCreate),
);
$this->menu= ControllerActionsName::createMenusRoles($menus, $actions);

?>

<h1>Cập Nhật: <?php echo $this->pageTitle; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>