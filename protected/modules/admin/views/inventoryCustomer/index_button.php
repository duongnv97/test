<div class="form">
    <?php
        $LinkNormal     = Yii::app()->createAbsoluteUrl('admin/inventoryCustomer/index');
        $LinkPrice     = Yii::app()->createAbsoluteUrl('admin/inventoryCustomer/index', ['type'=> InventoryCustomer::TYPE_CLEAN_INVENTORY]);
    ?>
    <h1><?php // echo $this->pluralTitle; ?>
        <a class='btn_cancel f_size_14 <?php echo (!isset($_GET['type'])) ? "active":"";?>' href="<?php echo $LinkNormal;?>">Tồn đầu kỳ KH</a>
        <a class='btn_cancel f_size_14 <?php echo isset($_GET['type']) && $_GET['type']==InventoryCustomer::TYPE_CLEAN_INVENTORY ? "active":"";?>' href="<?php echo $LinkPrice;?>">Xóa công nợ</a>
    </h1>
</div>