<?php
$this->breadcrumbs=array(
	$this->pageTitle,
);
$aParams = ['create'];
if(isset($_GET['type'])){
    $aParams = ['create', 'type'=>$_GET['type']];
}

$menus=array(
    array('label'=>"Create $this->pageTitle", 'url'=>$aParams),
);
$this->menu= ControllerActionsName::createMenusRoles($menus, $actions);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('inventory-customer-grid', {
                url : $(this).attr('action'),
		data: $(this).serialize()
	});
	return false;
});
");

Yii::app()->clientScript->registerScript('ajaxupdate', "
$('#inventory-customer-grid a.ajaxupdate').live('click', function() {
    $.fn.yiiGridView.update('inventory-customer-grid', {
        type: 'POST',
        url: $(this).attr('href'),
        success: function() {
            $.fn.yiiGridView.update('inventory-customer-grid');
        }
    });
    return false;
});
");
?>

<?php include "index_button.php"; ?>
<?php echo CHtml::link('Tìm Kiếm Nâng Cao','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'inventory-customer-grid',
	'dataProvider'=>$model->search(),
        'afterAjaxUpdate'=>'function(id, data){ fnUpdateColorbox();}',
        'template'=>'{pager}{summary}{items}{pager}{summary}',
//    'itemsCssClass' => 'items custom_here',
//    'htmlOptions'=> array('class'=>'grid-view custom_here'),
        'pager' => array(
            'maxButtonCount'=>  CmsFormatter::$PAGE_MAX_BUTTON,
        ),
	'enableSorting' => false,
	//'filter'=>$model,
	'columns'=>array(
        array(
            'header' => 'S/N',
            'type' => 'raw',
            'value' => '$this->grid->dataProvider->pagination->currentPage * $this->grid->dataProvider->pagination->pageSize + ($row+1)',
            'headerHtmlOptions' => array('width' => '10px','style' => 'text-align:center;'),
            'htmlOptions' => array('style' => 'text-align:center;')
        ),
            array(
                'header'=>'Mã kế toán',
                'type'=>'html',
                'value'=>'$data->getCustomer("code_account")',
            ),
            array(
                'name'=>'customer_id',
                'type'=>'html',
                'value'=>'$data->getCustomer("code_bussiness"). " - " .$data->getCustomer()',
            ),
            array(
                'name'=>'type_customer',
                'value'=>'$data->getCustomer("type_customer")',
            ),
            array(
                'name'=>'agent_id',
                'value'=>'$data->getAgent()',
            ),
            array(
                'name'=>'sale_id',
                'value'=>'$data->getSale()',
            ),
            array(
                'name'=>'debit',
                'value'=>'$data->getDebit(true)',
                'htmlOptions' => array('style' => 'text-align:right;'),
            ),
            array(
                'name'=>'credit',
                'value'=>'$data->getCredit(true)',
                'htmlOptions' => array('style' => 'text-align:right;'),
            ),
            array(
                'name'=>'json_vo',
                'type'=>'raw',
                'value'=>'$data->getJsonVoView()',
//                'htmlOptions' => array('style' => 'text-align:right;'),
            ),
            array(
                'name' => 'uid_login',
                'value' => '$data->getUidLogin()',
            ),
            array(
                'name' => 'created_date',
                'value' => '$data->getCreatedDate()',
                'htmlOptions' => array('style' => 'width:50px;'),
            ),
            array(
                'header' => 'Actions',
                'class'=>'CButtonColumn',
                'template'=> ControllerActionsName::createIndexButtonRoles($actions),
                'buttons'=>array(
                    'update' => array(
                        'visible' => '$data->canUpdate()',
                    ),
                    'delete'=>array(
                        'visible'=> 'GasCheck::canDeleteData($data)',
                    ),
                ),
            ),
	),
)); ?>

<script>
$(document).ready(function() {
    fnUpdateColorbox();
});

function fnUpdateColorbox(){   
    fixTargetBlank();
}
</script>