<?php
$this->breadcrumbs = array(
	$this->pluralTitle => array('index'),
        $model->id=>array('view','id'=>$model->id),
	'Cập Nhật ' . $this->pageTitle,
);

$aParamsIndex   = ['index', 'type'=> $model->type];
$aParamsCreate  = ['create', 'type'=> $model->type];

$menus = array(	
    array('label' => $this->pluralTitle, 'url' => $aParamsIndex),
    array('label' => 'Xem ' . $this->singleTitle, 'url' => array('view', 'id' => $model->id)),	
    array('label' => 'Tạo Mới ' . $this->singleTitle, 'url' => $aParamsCreate),
);

$aParamsIndex = ['limitDebitIndex'];
$aParamsCreate  = ['limitDebitCreate'];

$menus=array(
    array('label'=>"Quản lý", 'url'=>$aParamsIndex, 
        'htmlOptions'=>array('class'=>'index ','label'=>'Quản lý')),
    array('label'=>"Tạo mới", 'url'=>$aParamsCreate, 
        'htmlOptions'=>array('class'=>'create ','label'=>'Tạo mới')),
);

$this->menu= ControllerActionsName::createMenusRoles($menus, $actions);

?>

<h1><?php echo $this->pageTitle; ?></h1>

<?php echo $this->renderPartial('limitDebit/_form', array('model'=>$model)); ?>