<?php
$this->breadcrumbs=array(
	$this->pageTitle=>array('index'),
	'Tạo Mới',
);

$aParams = ['limitDebitIndex'];

$menus=array(
    array('label'=>"Quản lý", 'url'=>$aParams, 
        'htmlOptions'=>array('class'=>'index ','label'=>'Quản lý')),
);

$this->menu= ControllerActionsName::createMenusRoles($menus, $actions);
?>

<h1>Nhập <?php echo $this->pageTitle;?></h1>

<div class="form">
    <?php $form=$this->beginWidget('CActiveForm', array(
            'id'=>'inventory-customer-form',
            'enableAjaxValidation'=>false,
            'htmlOptions' => array('enctype' => 'multipart/form-data'),
    )); ?>
    <?php echo $form->hiddenField($model,'id', array('class'=>'')); ?>
    <?php echo $this->renderPartial('limitDebit/_form_multi', array('model'=>$model)); ?>
    <?php $this->endWidget(); ?>
</div>