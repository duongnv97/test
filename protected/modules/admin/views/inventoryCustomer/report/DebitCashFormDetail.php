<?php 
$mCustomer          = isset($aData['mCustomer']) ? $aData['mCustomer'] : 0;
$closingDebit1       = isset($aData['ClosingDebit']) ? $aData['ClosingDebit'] : 0;
$closingCredit1      = isset($aData['ClosingCredit']) ? $aData['ClosingCredit'] : 0;
$ORDERS             = isset($aData['ORDERS']) ? $aData['ORDERS'] : [];
$PAY                = isset($aData['PAY']) ? $aData['PAY'] : [];
$ARR_DAYS           = isset($aData['ARR_DAYS']) ? $aData['ARR_DAYS'] : [];
$mAppCache          = new AppCache();
$aMasterLookup      = $mAppCache->getModelTypeLookup(GasMasterLookup::TYPE_CASHBOOK);

$index = 1; $incurredDebit1 = $incurredCredit1 = 0;
$closingDebit2 = $closingCredit2 = 0;

if(!$model->allowAccessAgent($mCustomer->area_code_id)){
   $ARR_DAYS = [];// Dec2518 hide info if user not allow access
}
?>
<table class="tb hm_table f_size_15 w-600">
    <thead>
        <tr>
            <th class="item_c">#</th>
            <th class="item_c">Ngày</th>
            <th class="item_c">Mã chứng từ</th>
            <th class="item_c">Diễn giải</th>
            <th class="item_c">Nợ</th>
            <th class="item_c">Có</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td></td><td></td><td></td>
            <td class="item_b">Số dư đầu kỳ</td>
            <td class="item_r item_b"><?php echo !empty($closingDebit1) ? ActiveRecord::formatCurrencyRound($closingDebit1) : ''; ?></td>
            <td class="item_r item_b"><?php echo !empty($closingCredit1) ? ActiveRecord::formatCurrencyRound($closingCredit1) : ''; ?></td>
        </tr>
        <tr>
            <td></td><td></td><td></td>
            <td class="item_b">Phát sinh trong kỳ</td>
            <td class="item_r item_b incurredDebit"></td>
            <td class="item_r item_b incurredCredit"></td>
        </tr>
        <tr>
            <td></td><td></td><td></td>
            <td class="item_b">Số dư cuối kỳ</td>
            <td class="item_r item_b closingDebit"></td>
            <td class="item_r item_b closingCredit"></td>
        </tr>
        <?php foreach ($ARR_DAYS as $date): ?>
            <?php $amountRemain = 0; ?>
            <?php if(isset($ORDERS[$date])):?>
                <?php foreach ($ORDERS[$date] as $info): ?>
                    <?php 
                        $code_no    = $info['code_no'];
                        $debit      = $info['total_gas'];
                        $credit     = 0;
                        $note       = 'Xuất bán gas';
                        $incurredDebit1 += $debit;
                        if($info['total_gas'] > 0){
                            include 'DebitCashFormTr.php';
                        }
                    ?>
                    <?php if($info['gas_remain'] > 0): 
                            $code_no    = $info['code_no'];
                            $debit      = 0;
                            $credit     = $info['gas_remain'];
                            $amountRemain = $credit;
                            $incurredCredit1 += $credit;
                            $note       = 'Gas dư';
                            include 'DebitCashFormTr.php';
                     endif; ?>
                    <?php if($info['pay_back_amount'] > 0): 
                            $code_no    = $info['code_no'];
                            $debit      = 0;
                            $credit     = $info['pay_back_amount'];
                            $amountRemain = $credit;
                            $incurredCredit1 += $credit;
                            $note       = 'Cân thiếu đầu vào';
                            include 'DebitCashFormTr.php';
                     endif; ?>
                    <?php if($info['discount'] > 0): 
                            $code_no    = $info['code_no'];
                            $debit      = 0;
                            $credit     = $info['discount'];
                            $amountRemain = $credit;
                            $incurredCredit1 += $credit;
                            $note       = 'Giảm giá';
                            include 'DebitCashFormTr.php';
                     endif; ?>
        
                <?php endforeach; ?>
            <?php endif; ?>

            <?php if(isset($PAY[$date][GasMasterLookup::MASTER_TYPE_CHI])):?>
                <?php 
                    foreach($PAY[$date][GasMasterLookup::MASTER_TYPE_CHI] as $typeLookup => $info):
                        $nameType = isset($aMasterLookup[$typeLookup]) ? $aMasterLookup[$typeLookup]['name'] : '';
//                        if($typeLookup == GasMasterLookup::ID_CHI_THE_CHAN || (!isset($PAY[$date][GasMasterLookup::MASTER_TYPE_THU]) && $typeLookup == GasMasterLookup::ID_CHI_GAS_DU)):
                        if(1):// Aug1717 nếu có chi gas dư hoặc thế chân thì luôn đưa sang bên nợ
                            // Aug1717 cứ có chi là đưa sang bên nợ - fix row bên trên
                            $code_no    = $info['code_no'];
                            $debit      = $info['amount'];
                            $credit     = 0;
                            $incurredDebit1 += $debit;
                            $note       = $nameType.': '.$info['note'];
                            include 'DebitCashFormTr.php';
                        endif;

                        // Aug1717 bỏ đoạn này, luôn đưa gas dư sang cột có.
//                        if($typeLookup != GasMasterLookup::ID_CHI_THE_CHAN):
//                            $code_no    = $info['code_no'];
//                            $debit      = 0;
//                            $credit     = $info['amount'];
//                            $amountRemain = $credit;
//                            $incurredCredit1 += $credit;
//                            $note       = $nameType.': '.$info['note'];
//                            include 'DebitCashFormTr.php';
//                        endif;
                    endforeach;
                ?>

            <?php endif; ?>

            <?php if(isset($PAY[$date][GasMasterLookup::MASTER_TYPE_THU])):?>
                <?php 
                foreach($PAY[$date][GasMasterLookup::MASTER_TYPE_THU] as $typeLookup => $info):
                    $nameType = isset($aMasterLookup[$typeLookup]) ? $aMasterLookup[$typeLookup]['name'] : '';
                    $code_no    = $info['code_no'];
                    $debit      = 0;
//                    $credit     = $info['amount'] - $amountRemain;
                    $credit     = $info['amount'];
                    $incurredCredit1 += $credit;
                    $note       = $nameType.': '.$info['note'];
                    include 'DebitCashFormTr.php';
                endforeach;
                ?>
            <?php endif; ?>
        
        <?php endforeach; // end foreach ($ARR_DAYS ?>
    </tbody>
</table>
<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/print-store-card.css" />
<?php
$model->calcClosingDebitCredit($closingDebit1, $closingCredit1, $incurredDebit1, $incurredCredit1, $closingDebit2, $closingCredit2);
?>
<span class="incurredDebit1Hide display_none"><?php echo $incurredDebit1!=0 ? ActiveRecord::formatCurrencyRound($incurredDebit1) : '';?></span>
<span class="incurredCredit1Hide display_none"><?php echo $incurredCredit1!=0 ? ActiveRecord::formatCurrencyRound($incurredCredit1) : '';?></span>
<span class="closingDebit2Hide display_none"><?php echo $closingDebit2!=0 ? ActiveRecord::formatCurrencyRound($closingDebit2) : '';?></span>
<span class="closingCredit2Hide display_none"><?php echo $closingCredit2!=0 ? ActiveRecord::formatCurrencyRound($closingCredit2) : '';?></span>
<?php
$_SESSION['incurredDebit1']     = ($incurredDebit1!=0) ? $incurredDebit1 : '';
$_SESSION['incurredCredit1']    = ($incurredCredit1!=0) ? $incurredCredit1 : '';
$_SESSION['closingDebit2']      = ($closingDebit2!=0) ? $closingDebit2 : '';
$_SESSION['closingCredit2']     = ($closingCredit2!=0) ? $closingCredit2 : '';
?>
<script>
    $(function(){
        $('.incurredDebit').html($('.incurredDebit1Hide').html());
        $('.incurredCredit').html($('.incurredCredit1Hide').html());
        $('.closingDebit').html($('.closingDebit2Hide').html());
        $('.closingCredit').html($('.closingCredit2Hide').html());
    });
</script>