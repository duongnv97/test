<div class="form">
    <?php
        $LinkNormal = Yii::app()->createAbsoluteUrl('admin/inventoryCustomer/reportDebitVo', ['type'=> InventoryCustomer::VIEW_GENERAL]);
        $Link1      = Yii::app()->createAbsoluteUrl('admin/inventoryCustomer/reportDebitVo', ['type'=> InventoryCustomer::VIEW_DETAIL]);
        $typeBtn    = isset($_GET['type']) ?  $_GET['type'] : InventoryCustomer::VIEW_GENERAL;
    ?>
    <h1><?php echo $this->pageTitle; ?>
        <a class='btn_cancel f_size_14 <?php echo $typeBtn==InventoryCustomer::VIEW_GENERAL ? "active":"";?>' href="<?php echo $LinkNormal;?>">Tổng quát</a>
        <a class='btn_cancel f_size_14 <?php echo $typeBtn==InventoryCustomer::VIEW_DETAIL ? "active":"";?>' href="<?php echo $Link1;?>">Chi tiết</a>
    </h1>
</div>