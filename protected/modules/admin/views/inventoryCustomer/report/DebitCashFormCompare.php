<?php 
/** @Note: AnhDung Aug2318 có 3 chỗ sử dụng view này
 * 1. this page, view on web
 * 2. Export excel
 * 3. Export pdf to send email
 * 
 */
$mCustomer          = $aData['mCustomer'] ? $aData['mCustomer'] : 0;
$mCompany           = $aData['mCompany'] ? $aData['mCompany'] : 0;
$OUTPUT             = isset($aData['OUTPUT']) ? $aData['OUTPUT'] : [];
$ARR_DAYS           = isset($aData['ARR_DAYS']) ? $aData['ARR_DAYS'] : [];
$index = 1; $companyName = $companyAdd = '';
if($mCompany){
    $companyName    = $mCompany->first_name;
    $companyAdd     = $mCompany->address;
}
$mAppCache = new AppCache();
//$aMaterials     = $mAppCache->getMasterModel('GasMaterials', AppCache::ARR_MODEL_MATERIAL);
$aMaterialsType = $mAppCache->getMasterModel('GasMaterialsType', AppCache::ARR_MODEL_MATERIAL_TYPE);
$b12ToKg = false; $sumAll = 0; $sessionExcel['DataMaterialType'] = []; $sessionExcel['DataSumAll'] = 0;
$mInventory = new InventoryCustomer();
$mInventory->customer_id = $mCustomer->id;
if(in_array($mCustomer->id, $mInventory->getCustomerPriceB12Kg())){
    $b12ToKg = true; 
}

if(!$model->allowAccessAgent($mCustomer->area_code_id)){
   $OUTPUT = [];// Dec2518 hide info if user not allow access
}
// DuongNV 18 Jun,2019 Vị trí cột tổng cho những KH khó tính
$sumColPosition = in_array($model->customer_id, $model->getArrCustomerSumColBottom()) ? 'bottom' : 'top';
?>

<div class="container" id="printElement">
<table cellpadding="0" cellspacing="0">
    <tr>
        <td class="item_b"><?php echo $companyName?></td>
    </tr>
    <tr>
        <td><?php echo $companyAdd?></td>
    </tr>
</table>
<br>

<table cellpadding="0" cellspacing="0">
    <tr>
        <td class="item_b item_c">
            <h1 class="f_size_18">
                Bảng kê hóa đơn: <?php echo $mCustomer->first_name;?><br>
            </h1>
            Từ <?php echo $model->date_from;?> đến <?php echo $model->date_to;?>
        </td>
    </tr>
</table>

<table class="tb hm_table f_size_15">
    <thead>
        <tr>
            <th class="item_c">#</th>
            <th class="item_c">Ngày</th>
            <th class="item_c">Diễn giải</th>
            <th class="item_c">Số lượng</th>
            <th class="item_c">Gas dư</th>
            <th class="item_c">Cân thiếu đầu vào</th>
            <th class="item_c">Tổng số lượng</th>
            <th class="item_c">Giá bán</th>
            <th class="item_c">Giảm giá</th>
            <th class="item_c">Tiền bán</th>
        </tr>
    </thead>
    <tbody>
        <?php if($sumColPosition == 'top'): ?>
        <!--<tr class="<?php // echo $sumColPosition=='top' ? '' : 'display_none' ?>">-->
        <tr>
            <td></td><td></td>
            <td class="item_b" colspan="7">Tổng cộng</td>
            <td class="item_r item_b SumAll"></td>
        </tr>
        <?php endif; ?>
        <?php foreach ($OUTPUT as $materials_type_id => $aInfo): ?>
        <?php
            if(in_array($materials_type_id, $mInventory->getMaterialsTypeHideInReportDebitCashCompare())){
                continue ;
            }
            $index = 1; $sumBlock = $sumQty = $sumQtyFinal = $sumRemain = $sumPayback = 0;
            $materials_type_name = isset($aMaterialsType[$materials_type_id]) ? $aMaterialsType[$materials_type_id]['name'] : '';
        ?>
        <tr class="SumBlock<?php echo $materials_type_id;?>">
            <td></td><td></td>
        </tr>
        <?php foreach ($ARR_DAYS as $date): ?>
        <?php if(!isset($aInfo[$date])) continue; ?>
            <?php // foreach ($aInfo[$date] as $aInfo1): ?>
            <?php 
                $aInfo1 = $aInfo[$date];
                $qty = $qtyFinal = $aInfo1['qty'];
                $aInfo1['discount'] = isset($aInfo1['discount']) ? $aInfo1['discount'] : 0;
                $aInfo1['pay_back'] = isset($aInfo1['pay_back']) ? $aInfo1['pay_back'] : 0;
//                if(in_array($materials_type_id, GasMaterialsType::getArrGasKg()) || ($materials_type_id == GasMaterialsType::MATERIAL_BINH_12KG && $aInfo1['price'] < UsersPrice::PRICE_MAX_BINH_BO) 
//                || $b12ToKg){
                if($mInventory->isPriceKg($materials_type_id, $aInfo1['price'])):
                    $qty = $aInfo1['qty'] * CmsFormatter::$MATERIAL_VALUE_KG[$materials_type_id];
                    $qtyFinal =  $qty - ($aInfo1['remain_kg'] + $aInfo1['pay_back']);
                    $mInventory->isPriceKgB12($materials_type_id, $b12ToKg);
                    if($b12ToKg && $materials_type_id == GasMaterialsType::MATERIAL_BINH_12KG && $aInfo1['price'] > UsersPrice::PRICE_MAX_BINH_BO){
                        // Dec1817 xử lý thêm cho HỶ Lâm Môn nếu đơn hàng chỉ có thu vỏ B12 và có gas dư =>>> $aInfo1['price'] > UsersPrice::PRICE_MAX_BINH_BO
                        $aInfo1['price'] = round($aInfo1['price'] / 12);
                    }
                    if($materials_type_id == GasMaterialsType::MATERIAL_BINH_6KG && $aInfo1['price'] > UsersPrice::PRICE_MAX_BINH_BO){
                        $aInfo1['price'] = round($aInfo1['price'] / 6);
                    }// Oct2517
                    
                endif;
                
                $grandTotal = ($qtyFinal * $aInfo1['price']) - $aInfo1['discount'];
                if($materials_type_id == GasMaterialsType::MATERIAL_BINH_6KG){
                    $grandTotal = round($grandTotal, -3);// Feb2818 for Sheraton Sài Gòn
                }
                $sumBlock   += $grandTotal;
                $sumAll     += $grandTotal;
                $sessionExcel['DataSumAll']     += $grandTotal;
                
                $sumQty         += $qty;
                $sumQtyFinal    += $qtyFinal ;
                $sumRemain      += $aInfo1['remain_kg'];
                $sumPayback     += $aInfo1['pay_back'];
            ?>
            <tr>
                <td class="item_c"><?php echo $index++;?></td>
                <td class="item_c"><?php echo MyFormat::dateConverYmdToDmy($date);?></td>
                <td class="">Xuất bán</td>
                <td class="item_r"><?php echo $qty != 0 ? ActiveRecord::formatCurrencyRound($qty) : ''; ?></td>
                <td class="item_r"><?php echo $aInfo1['remain_kg'] != 0 ? ActiveRecord::formatCurrency($aInfo1['remain_kg']) : ''; ?></td>
                <td class="item_r"><?php echo $aInfo1['pay_back'] != 0 ? ActiveRecord::formatCurrency($aInfo1['pay_back']) : ''; ?></td>
                <td class="item_r"><?php echo ActiveRecord::formatCurrency($qtyFinal);?></td>
                <td class="item_r"><?php echo ActiveRecord::formatCurrency($aInfo1['price']); ?></td>
                <td class="item_r"><?php echo $aInfo1['discount'] != 0 ? ActiveRecord::formatCurrencyRound($aInfo1['discount']) : ''; ?></td>
                <td class="item_r"><?php echo ActiveRecord::formatCurrencyRound($grandTotal);?></td>
            </tr>
            <?php // endforeach;// en foreach ($ARR_DAYS ?>
        <?php endforeach;// en foreach ($ARR_DAYS ?>
            <tr class="display_none SumBlockHide" next="<?php echo $materials_type_id;?>">
                <td class="item_b" colspan="3"><?php echo $materials_type_name;?></td>
                <?php if($sumColPosition=='top'): ?>
                <td class="item_r item_b"><?php echo ActiveRecord::formatCurrencyRound($sumQty);?></td>
                <td class="item_r item_b"><?php echo $sumRemain != 0 ? ActiveRecord::formatCurrency($sumRemain) : '';?></td>
                <td class="item_r item_b"><?php echo $sumPayback != 0 ? ActiveRecord::formatCurrency($sumPayback) : '';?></td>
                <td class="item_r item_b"><?php echo ActiveRecord::formatCurrency($sumQtyFinal);?></td>
                <td></td><td></td>
                <td class="item_r item_b"><?php echo ActiveRecord::formatCurrencyRound($sumBlock);?></td>
                <?php else: ?>
                    <?php echo str_repeat('<td></td>', 7); ?>
                <?php endif; ?>
            </tr>
            <?php
                $temp = [];
                $temp['name']       = $materials_type_name;
                $temp['qty']        = $sumQty;
                $temp['remain']     = $sumRemain;
                $temp['payback']    = $sumPayback;
                $temp['totalQty']   = $sumQtyFinal;
                $temp['amount']     = $sumBlock;
                $sessionExcel['DataMaterialType'][$materials_type_id] = $temp;
            ?>
            
        <?php endforeach;// en foreach ($OUTPUT ?>
        <?php if($sumColPosition != 'top'): ?>
        <!--<tr class="<?php // echo $sumColPosition=='top' ? 'display_none' : '' ?>">-->
        <tr>
            <td class="item_b" colspan="3">Tổng cộng</td>
            <td class="item_r item_b"><?php echo ActiveRecord::formatCurrencyRound($sumQty);?></td>
            <td class="item_r item_b"><?php echo $sumRemain != 0 ? ActiveRecord::formatCurrency($sumRemain) : '';?></td>
            <td></td>
            <td class="item_r item_b"><?php echo ActiveRecord::formatCurrency($sumQtyFinal);?></td>
            <td></td>
            <td></td>
            <td class="item_r item_b"><?php echo ActiveRecord::formatCurrencyRound($sumAll);?></td>
        </tr>
        <?php endif; ?>
        <tr class="display_none"><td class="SumAllHide"><?php echo ActiveRecord::formatCurrencyRound($sumAll);?></td></tr>
    </tbody>
</table>
<?php include '_print_footer.php'; ?>
</div> <!--end div id="printElement" -->
<?php 
    $_SESSION['DataSumRow'] = $sessionExcel;
?>

<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/print-store-card.css" />
<script  src="<?php echo Yii::app()->theme->baseUrl; ?>/js/jquery.printElement.min.js"></script>
<script>
    $(function(){
//        $('.incurredDebit').html($('.incurredDebit1Hide').html());
//        $('.incurredCredit').html($('.incurredCredit1Hide').html());
//        $('.closingDebit').html($('.closingDebit2Hide').html());
//        $('.closingCredit').html($('.closingCredit2Hide').html());
        $('.SumBlockHide').each(function(){
            var id = $(this).attr('next');
            $('.SumBlock'+id).html($(this).html());
            $(this).remove();
        });
        $('.SumAll').html($('.SumAllHide').html());
        $('.SumAllHide').remove();
        $(".button_print").click(function(){
            $('#printElement').printElement({ overrideElementCSS: ['<?php echo Yii::app()->theme->baseUrl;?>/css/print-store-card.css'] });
        });

    });
</script>