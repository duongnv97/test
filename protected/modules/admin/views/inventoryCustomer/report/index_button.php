<div class="form">
    <?php
        $LinkNormal = Yii::app()->createAbsoluteUrl('admin/inventoryCustomer/reportDebitCash', ['type'=> InventoryCustomer::VIEW_GENERAL]);
        $Link1      = Yii::app()->createAbsoluteUrl('admin/inventoryCustomer/reportDebitCash', ['type'=> InventoryCustomer::VIEW_DETAIL]);
        $Link2      = Yii::app()->createAbsoluteUrl('admin/inventoryCustomer/reportDebitCash', ['type'=> InventoryCustomer::VIEW_COMPARE]);
    ?>
    <h1><?php echo $this->pageTitle; ?>
        <a class='btn_cancel f_size_14 <?php echo isset($_GET['type']) && $_GET['type']==InventoryCustomer::VIEW_GENERAL ? "active":"";?>' href="<?php echo $LinkNormal;?>">Tổng quát</a>
        <a class='btn_cancel f_size_14 <?php echo isset($_GET['type']) && $_GET['type']==InventoryCustomer::VIEW_DETAIL ? "active":"";?>' href="<?php echo $Link1;?>">Chi tiết</a>
        <a class='btn_cancel f_size_14 <?php echo isset($_GET['type']) && $_GET['type']==InventoryCustomer::VIEW_COMPARE ? "active":"";?>' href="<?php echo $Link2;?>">Đối chiếu</a>
    </h1>
</div>