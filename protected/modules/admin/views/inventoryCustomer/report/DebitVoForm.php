 <?php
$aCustomer          = isset($aData['CUSTOMER_MODEL']) ? $aData['CUSTOMER_MODEL'] : [];
$aMaterials         = isset($aData['MATERIALS_MODEL']) ? $aData['MATERIALS_MODEL'] : [];
$BEGIN_VO           = isset($aData['BEGIN_VO']) ? $aData['BEGIN_VO'] : [];
$OUTPUT_BEFORE      = isset($aData['OUTPUT_BEFORE']) ? $aData['OUTPUT_BEFORE'] : [];
$OUTPUT_IN_PERIOD   = isset($aData['OUTPUT_IN_PERIOD']) ? $aData['OUTPUT_IN_PERIOD'] : [];
$aIdVo              = isset($aData['ID_VO']) ? $aData['ID_VO'] : [];
$index = 1;
$mAppCache          = new AppCache();
$listdataAgent      = $mAppCache->getAgentListdata();
$session            = Yii::app()->session;
$sum_vo_lon = $sum_vo_nho = 0;
?>
<table class="tb hm_table">
    <thead>
        <tr>
            <th class="item_c">#</th>
            <th class="item_c">Đại lý</th>
            <th class="item_c">Mã kế toán</th>
            <th class="item_c">Mã vỏ</th>
            <th class="item_c">Tên vỏ</th>
            <th class="item_c">Tồn đầu</th>
            <th class="item_c">SL xuất</th>
            <th class="item_c">SL nhập</th>
            <th class="item_c">Tồn cuối</th>
        </tr>
    </thead>
    <tbody>
    <tr style="text-align: center;">
        <td colspan="8" class="item_b">Tổng tồn cuối vỏ nhỏ</td>
        <td class="item_b sum_vo_nho_top"></td>
    </tr>
    <tr style="text-align: center;">
        <td colspan="8" class="item_b">Tổng tồn cuối vỏ lớn</td>
        <td class="item_b sum_vo_lon_top"></td>
    </tr>
    <tr style="text-align: center;">
        <td colspan="8" class="item_b">Tổng tồn cuối</td>
        <td class="item_b sum_vo_top"></td>
    </tr>
        <?php foreach ($aCustomer as $mCustomer): ?>
        <?php
        if(!$mCustomer->allowAccessAgent($mCustomer->area_code_id)){
            continue;// Add check Dec2518
        }
        
        $customer_id = $mCustomer->id;
        $agentName   = isset($listdataAgent[$mCustomer->area_code_id]) ? $listdataAgent[$mCustomer->area_code_id] : '';
//        if(!isset($aIdVo[$mCustomer->id]) || $customer_id != $model->customer_id){
//            continue;
//        }
        $sum_begin = $sum_import = $sum_export = $sum_end = 0;
        $sum_begin_nho = $sum_import_nho = $sum_export_nho = $sum_end_nho = 0;
        $sum_begin_lon = $sum_import_lon = $sum_export_lon = $sum_end_lon = 0;
        ?>
        <tr class="display_none rowShow<?php echo $mCustomer->id;?>">
            <td></td>
        </tr>
        <!--Vỏ Nhỏ-->
            <tr class="display_none rowShow<?php echo $mCustomer->id;?>Nho">
                <td></td>
            </tr>
            <?php foreach ($aIdVo[$customer_id]['MATERIALS_VO_NHO'] as $materials_id => $qty): ?>
            <?php 
                $materials_no = $name  = $materials_id;
                if(isset($aMaterials[$materials_id])){
                    $materials_no = $aMaterials[$materials_id]['materials_no'];
                    $name = $aMaterials[$materials_id]['name'];
                }
                $qty_begin          = isset($BEGIN_VO[$customer_id][$materials_id]) ? $BEGIN_VO[$customer_id][$materials_id] : 0;
                $qty_import_before  = isset($OUTPUT_BEFORE[TYPE_STORE_CARD_IMPORT][$customer_id][$materials_id]) ? $OUTPUT_BEFORE[TYPE_STORE_CARD_IMPORT][$customer_id][$materials_id] : 0;
                $qty_export_before  = isset($OUTPUT_BEFORE[TYPE_STORE_CARD_EXPORT][$customer_id][$materials_id]) ? $OUTPUT_BEFORE[TYPE_STORE_CARD_EXPORT][$customer_id][$materials_id] : 0;
                
                $qty_import_in_period = isset($OUTPUT_IN_PERIOD[TYPE_STORE_CARD_IMPORT][$customer_id][$materials_id]) ? $OUTPUT_IN_PERIOD[TYPE_STORE_CARD_IMPORT][$customer_id][$materials_id] : 0;
                $qty_export_in_period = isset($OUTPUT_IN_PERIOD[TYPE_STORE_CARD_EXPORT][$customer_id][$materials_id]) ? $OUTPUT_IN_PERIOD[TYPE_STORE_CARD_EXPORT][$customer_id][$materials_id] : 0;
                
                $begin      = $qty_begin + $qty_export_before - $qty_import_before;
                $end        = $begin + $qty_export_in_period - $qty_import_in_period;
                $sum_begin      += $begin;
                $sum_import     += $qty_import_in_period;
                $sum_export     += $qty_export_in_period;
                $sum_end        += $end;
//                tính toán vỏ nhỏ
                $sum_begin_nho  += $begin;
                $sum_import_nho += $qty_import_in_period;
                $sum_export_nho += $qty_export_in_period;
                $sum_end_nho    += $end;
            
                if($begin == 0 && $qty_import_in_period == 0 && $qty_export_in_period == 0 && $end == 0){
                    continue;
                }
            ?>
            <tr class="display_none viewShow<?php echo $mCustomer->id;?>Nho">
                <td></td><td></td><td></td>
                <td><?php echo $materials_no;?></td>
                <td><?php echo $name;?></td>
                <td class="item_c"><?php echo ActiveRecord::formatCurrencyRound($begin);?></td>
                <td class="item_c"><?php echo ActiveRecord::formatCurrencyRound($qty_export_in_period);?></td>
                <td class="item_c"><?php echo ActiveRecord::formatCurrencyRound($qty_import_in_period);?></td>
                <td class="item_c"><?php echo ActiveRecord::formatCurrencyRound($end);?></td>
            </tr>
            <?php endforeach; ?>
            
            
        <!--Vỏ lớn-->
            <tr class="display_none rowShow<?php echo $mCustomer->id;?>Lon">
                <td></td>
            </tr>
            <?php foreach ($aIdVo[$customer_id]['MATERIALS_VO_LON'] as $materials_id => $qty): ?>
            <?php 
                $materials_no = $name  = $materials_id;
                if(isset($aMaterials[$materials_id])){
                    $materials_no = $aMaterials[$materials_id]['materials_no'];
                    $name = $aMaterials[$materials_id]['name'];
                }
                $qty_begin          = isset($BEGIN_VO[$customer_id][$materials_id]) ? $BEGIN_VO[$customer_id][$materials_id] : 0;
                $qty_import_before  = isset($OUTPUT_BEFORE[TYPE_STORE_CARD_IMPORT][$customer_id][$materials_id]) ? $OUTPUT_BEFORE[TYPE_STORE_CARD_IMPORT][$customer_id][$materials_id] : 0;
                $qty_export_before  = isset($OUTPUT_BEFORE[TYPE_STORE_CARD_EXPORT][$customer_id][$materials_id]) ? $OUTPUT_BEFORE[TYPE_STORE_CARD_EXPORT][$customer_id][$materials_id] : 0;
                
                $qty_import_in_period = isset($OUTPUT_IN_PERIOD[TYPE_STORE_CARD_IMPORT][$customer_id][$materials_id]) ? $OUTPUT_IN_PERIOD[TYPE_STORE_CARD_IMPORT][$customer_id][$materials_id] : 0;
                $qty_export_in_period = isset($OUTPUT_IN_PERIOD[TYPE_STORE_CARD_EXPORT][$customer_id][$materials_id]) ? $OUTPUT_IN_PERIOD[TYPE_STORE_CARD_EXPORT][$customer_id][$materials_id] : 0;
                
                $begin      = $qty_begin + $qty_export_before - $qty_import_before;
                $end        = $begin + $qty_export_in_period - $qty_import_in_period;
                $sum_begin      += $begin;
                $sum_import     += $qty_import_in_period;
                $sum_export     += $qty_export_in_period;
                $sum_end        += $end;
//                Tính toán vỏ lớn
                $sum_begin_lon  += $begin;
                $sum_import_lon += $qty_import_in_period;
                $sum_export_lon += $qty_export_in_period;
                $sum_end_lon    += $end;
            
                if($begin == 0 && $qty_import_in_period == 0 && $qty_export_in_period == 0 && $end == 0){
                    continue;
                }
            ?>
            <tr class="display_none viewShow<?php echo $mCustomer->id;?>Lon">
                <td></td><td></td><td></td>
                <td><?php echo $materials_no;?></td>
                <td><?php echo $name;?></td>
                <td class="item_c"><?php echo ActiveRecord::formatCurrencyRound($begin);?></td>
                <td class="item_c"><?php echo ActiveRecord::formatCurrencyRound($qty_export_in_period);?></td>
                <td class="item_c"><?php echo ActiveRecord::formatCurrencyRound($qty_import_in_period);?></td>
                <td class="item_c"><?php echo ActiveRecord::formatCurrencyRound($end);?></td>
            </tr>
            <?php endforeach; ?>
            
            <?php if(!($sum_begin_nho == 0 && $sum_import_nho == 0 && $sum_export_nho == 0 && $sum_end_nho == 0)){ ?>
            <tr class="display_none rowHide" data-ref="<?php echo $mCustomer->id;?>Nho">
                <td></td>
                <td colspan="4" class="item_b" style="text-align: center;">Vỏ Nhỏ</td>
                <td class="item_c item_b"><?php echo ActiveRecord::formatCurrencyRound($sum_begin_nho);?></td>
                <td class="item_c item_b"><?php echo ActiveRecord::formatCurrencyRound($sum_export_nho);?></td>
                <td class="item_c item_b"><?php echo ActiveRecord::formatCurrencyRound($sum_import_nho);?></td>
                <td class="item_c item_b"><?php echo ActiveRecord::formatCurrencyRound($sum_end_nho);?></td>
            </tr>
            <?php }?>
            
            <?php if(!($sum_begin_lon == 0 && $sum_import_lon == 0 && $sum_export_lon == 0 && $sum_end_lon == 0)){ ?>
            <tr class="display_none rowHide" data-ref="<?php echo $mCustomer->id;?>Lon">
                <td></td>
                <td colspan="4" class="item_b" style="text-align: center;">Vỏ Lớn</td>
                <td class="item_c item_b"><?php echo ActiveRecord::formatCurrencyRound($sum_begin_lon);?></td>
                <td class="item_c item_b"><?php echo ActiveRecord::formatCurrencyRound($sum_export_lon);?></td>
                <td class="item_c item_b"><?php echo ActiveRecord::formatCurrencyRound($sum_import_lon);?></td>
                <td class="item_c item_b"><?php echo ActiveRecord::formatCurrencyRound($sum_end_lon);?></td>
            </tr>
            <?php }?>
            
            <?php if(!($sum_begin == 0 && $sum_import == 0 && $sum_export == 0 && $sum_end == 0)){?>
            <tr class="display_none rowHide" data-ref="<?php echo $mCustomer->id;?>">
                <td class="item_b"><?php echo $index++;?></td>
                <td class="item_b"><?php echo $agentName;?></td>
                <td class="item_b"><?php // echo $mCustomer->code_bussiness;?></td>
                <td></td>
                <td class="item_b">
<!--                    LOC005-->
                    <a href="<?php echo Yii::app()->createAbsoluteUrl("admin/inventoryCustomer/reportDebitVo", array('type'=>2, 'InventoryCustomer[customer_id]'=>$mCustomer->id)) ?>" target="_blank">
                        <?php echo $mCustomer->code_bussiness.' - '.$mCustomer->first_name;?>
                    </a>
                </td>
                <td class="item_c item_b"><?php echo ActiveRecord::formatCurrencyRound($sum_begin);?></td>
                <td class="item_c item_b"><?php echo ActiveRecord::formatCurrencyRound($sum_export);?></td>
                <td class="item_c item_b"><?php echo ActiveRecord::formatCurrencyRound($sum_import);?></td>
                <td class="item_c item_b"><?php echo ActiveRecord::formatCurrencyRound($sum_end);?></td>
            </tr>
            <?php } ?>

            <?php   $sum_vo_lon += $sum_end_lon;
                    $sum_vo_nho += $sum_end_nho;
            ?>
        <?php endforeach; ?>
        <?php if ($sum_vo_nho != 0): ?>
            <tr style="text-align: center;">
                <td colspan="8" class="item_b">Tổng tồn cuối vỏ nhỏ</td>
                <td class="item_b sum_vo_nho_bot"><?php echo ActiveRecord::formatCurrencyRound($sum_vo_nho);  ?></td>
            </tr>
        <?php endif; ?>
        <?php if ($sum_vo_lon != 0): ?>
            <tr style="text-align: center;">
                <td colspan="8" class="item_b">Tổng tồn cuối vỏ lớn</td>
                <td class="item_b sum_vo_lon_bot"><?php echo ActiveRecord::formatCurrencyRound($sum_vo_lon); ?></td>
            </tr>
        <?php endif;?>
        <?php if ($sum_vo_lon != 0 || $sum_vo_nho != 0): ?>
            <tr style="text-align: center;">
                <td colspan="8" class="item_b">Tổng tồn cuối</td>
                <td class="item_b sum_vo_bot"><?php echo ActiveRecord::formatCurrencyRound($sum_vo_lon + $sum_vo_nho); ?></td>
            </tr>
        <?php endif; ?>
    </tbody>
</table>
<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/print-store-card.css" />
 <script>
     jQuery(document).ready(function () {
         $(".sum_vo_nho_top").html($(".sum_vo_nho_bot").html());
         $(".sum_vo_lon_top").html($(".sum_vo_lon_bot").html());
         $(".sum_vo_top").html($(".sum_vo_bot").html());
     });
 </script>
