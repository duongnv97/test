<?php 
$gridCss = Yii::app()->getAssetManager()->publish(Yii::getPathOfAlias('zii.widgets.assets')) . '/gridview/styles.css';
Yii::app()->getClientScript()->registerCssFile($gridCss);
?>
<?php // 
    $aGasAppOrder   = isset($aData) ? $aData : [];
    $i              = 0;
?>
<div class="grid-view">
    <h2 class="item_b">Báo cáo sản lượng khách hàng <?php echo date("d/m/Y", strtotime($model->date_from)); ?> - <?php echo date("d/m/Y", strtotime($model->date_to)); ?></h2>
    <table class="items">
        <thead>
            <tr>
                <th class="item_c">S/N</th>
                <th class="item_c">Ngày giao</th>
                <th class="item_c">Mã số</th>
                <th class="item_c">Đại lý/người thực hiện</th>
                <th class="item_c">Khách hàng</th>
                <th class="item_c">Chi tiết</th>
                <th class="item_c">Trạng thái</th>
                <th class="item_c">Ngày tạo</th>
                <th class="item_c">Action</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($aGasAppOrder as $gas_app_order_id => $gasAppOrder): ?>
            <tr>
                <td style="text-align: center;"><?php echo ++$i; ?></td>
                <td style="text-align: center;"><?php echo $gasAppOrder->getDateDeliveryWeb(); ?></td>
                <td style="text-align: center;">
                    <?php
                    echo $gasAppOrder->code_no . '<br/>';
                    echo $gasAppOrder->getArrayDeliveryText();
                    ?>
                </td>
                <td>
                    <?php
                    echo $gasAppOrder->mapJsonOnly();
                    echo $gasAppOrder->getAgentDisplayGrid() . '<br/>';
                    echo $gasAppOrder->getDeliveryTimerGrid();
                    ?>
                </td>
                <td>
                    <?php 
                    echo $gasAppOrder->getCustomerInfoV1() .'<br/><b>';
                    echo $gasAppOrder->getCustomerPhoneOrderGrid() .'</b> - ';
                    echo $gasAppOrder->getNoteCustomer(). '<br/>';
                    echo $gasAppOrder->getNoteEmployeeWebShow();
                    echo $gasAppOrder->getFileOnly();
                    ?>
                </td>
                <td>
                    <?php
                    echo $gasAppOrder->setListdataPhuXe();
                    echo $gasAppOrder->getViewDetail(). '<br/>';
                    echo $gasAppOrder->getPhuXeWeb();
                    ?>
                </td>
                <td style="text-align: center;">
                    <?php
                    echo $gasAppOrder->getStatusText().'<br><b>';
                    echo $gasAppOrder->getReasonFalse().'</b><br>';
                    echo $gasAppOrder->getTimeOverWebView();
                    ?>
                </td>
                <td style="text-align: center;">
                    <?php
                    echo $gasAppOrder->getCreatedDate("d/m/Y H:i:s") . '<br/>';
                    echo $gasAppOrder->getInfoCreatedByOnGrid().'<br>';
                    echo $gasAppOrder->getNameCar();
                    ?>
                </td>
                <td>
                    <a class="view cboxElement" title="View" href="<?php echo Yii::app()->createAbsoluteUrl("admin/gasAppOrder/view", array("id"=>$gas_app_order_id)) ?>" target="_blank">
                        <img src="<?php echo Yii::app()->createAbsoluteUrl('/assets/890491de/gridview/view.png')?>" alt="View">
                    </a>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
</div>
<script>
$(document).ready(function() {
    fnAddClassOddEven('items');
    });
</script>