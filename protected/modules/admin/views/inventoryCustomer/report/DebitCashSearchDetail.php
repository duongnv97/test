<div class="row more_col">
    <div class="col1">
        <?php echo Yii::t('translation', $form->label($model,'date_from')); ?>
        <?php 
            $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                'model'=>$model,        
                'attribute'=>'date_from',
                'options'=>array(
                    'showAnim'=>'fold',
                    'dateFormat'=> MyFormat::$dateFormatSearch,
                    'maxDate'=> '0',
                    'changeMonth' => true,
                    'changeYear' => true,
                    'showOn' => 'button',
                    'buttonImage'=> Yii::app()->theme->baseUrl.'/admin/images/icon_calendar_r.gif',
                    'buttonImageOnly'=> true,                                
                ),        
                'htmlOptions'=>array(
                    'class'=>'w-16',
                    'size'=>'16',
                    'style'=>'height:20px;float:left;',
                    'readonly'=>'1',
                ),
            ));
        ?>     		
    </div>

    <div class="col2">
        <?php echo Yii::t('translation', $form->label($model,'date_to')); ?>
        <?php 
            $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                'model'=>$model,        
                'attribute'=>'date_to',
                'options'=>array(
                    'showAnim'=>'fold',
                    'dateFormat'=> MyFormat::$dateFormatSearch,       
                    'maxDate'=> '0',
                    'changeMonth' => true,
                    'changeYear' => true,
                    'showOn' => 'button',
                    'buttonImage'=> Yii::app()->theme->baseUrl.'/admin/images/icon_calendar_r.gif',
                    'buttonImageOnly'=> true,                                
                ),        
                'htmlOptions'=>array(
                    'class'=>'w-16',
                    'size'=>'16',
                    'style'=>'height:20px;float:left;',
                    'readonly'=>'1',
                ),
            ));
        ?>    
    </div>
</div>

<div class="row">
<?php echo $form->labelEx($model,'customer_id'); ?>
<?php echo $form->hiddenField($model,'customer_id', array('class'=>'customer_id')); ?>
<?php
        // 1. limit search kh của sale
        $url = Yii::app()->createAbsoluteUrl('admin/ajax/search_user_by_code');
        // widget auto complete search user customer and supplier
        $extData = array(
            'model'=>$model,
            'field_customer_id'=>'customer_id',
            'url'=> $url,
            'name_relation_user'=>'rCustomer',
            'ClassAdd' => 'w-400',
        );
        $this->widget('ext.SpaAutocompleteCustomer.SpaAutocompleteCustomer',
            array('data'=>$extData));
        ?>
    <?php echo $form->error($model,'customer_id'); ?>
</div>
<div class="row f_size_15">
    <label>&nbsp;</label>
    <?php echo $form->checkBox($model,'viewTheChan',array('class'=>'float_l')); ?>
    <?php echo $form->labelEx($model,'viewTheChan',array('class'=>'checkbox_one_label w-170', 'style'=>'padding-top:3px;')); ?>
</div>          
<div class="clr"></div>