<?php
$this->breadcrumbs=array(
    $this->pageTitle,
);
$view       = 'DebitCashForm';
$type       = isset($_GET['type']) ? $_GET['type'] : 1;
$aParamsEmail = ['type' => $type, 'to_send_email'=>1, 'date_from' => $model->date_from, 'date_to' => $model->date_to];

$urlExcel   = Yii::app()->createAbsoluteUrl('admin/inventoryCustomer/reportDebitCash', ['type' => $type, 'to_excel'=>1]);
$urlEmail   = Yii::app()->createAbsoluteUrl('admin/inventoryCustomer/reportDebitCash', $aParamsEmail);
$aEmail     = $model->getListEmailSendCompareDebit();

?>
<?php include "index_button.php"; ?>
<div class="search-form" style="">
    <div class="wide form">
    <?php $form=$this->beginWidget('CActiveForm', array(
            'action'=> Yii::app()->createAbsoluteUrl('admin/inventoryCustomer/reportDebitCash', ['type'=>(isset($_GET['type']) ? $_GET['type'] :1)]),
            'method'=>'get',
    )); ?>
        <?php if(Yii::app()->user->hasFlash('successUpdate')): ?>
            <div class="success_div"><?php echo Yii::app()->user->getFlash('successUpdate');?></div>         
        <?php endif; ?>    

        <?php if(isset($_GET['type']) && $_GET['type']==InventoryCustomer::VIEW_GENERAL): ?>
            <?php include 'DebitCashSearchGeneral.php'; ?>
        <?php elseif(isset($_GET['type']) && $_GET['type']==InventoryCustomer::VIEW_DETAIL): ?>
            <?php $view = 'DebitCashFormDetail'; include 'DebitCashSearchDetail.php'; ?>
        <?php elseif(isset($_GET['type']) && $_GET['type']==InventoryCustomer::VIEW_COMPARE): ?>
            <?php $view = 'DebitCashFormCompare'; include 'DebitCashSearchCompare.php'; ?>
        <?php endif; ?>
            

        <div class="row buttons" style="padding-left: 159px; margin:20px 0;">
            <?php $this->widget('bootstrap.widgets.TbButton', array(
                'buttonType'=>'submit',
                'label'=>Yii::t('translation','Xem Thống Kê'),
                'type'=>'null', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
                'size'=>'small', // null, 'large', 'small' or 'mini'
                //'htmlOptions' => array('style' => 'margin-bottom: 10px; float: right;'),
            )); ?>	
            <?php if(isset($_GET['type']) && ($_GET['type']==InventoryCustomer::VIEW_GENERAL || $_GET['type']==InventoryCustomer::VIEW_DETAIL ||$_GET['type']==InventoryCustomer::VIEW_COMPARE)): ?>
                &nbsp;&nbsp;&nbsp;&nbsp;<a class='btn_cancel' type='submit' href='<?php echo $urlExcel;?>'>Xuất Excel</a>
            <?php endif; ?>
            <?php if(isset($_GET['type']) && ($_GET['type']==InventoryCustomer::VIEW_COMPARE) && $model->canSendEmailCompareDebit() && count($aEmail)): ?>
                &nbsp;&nbsp;&nbsp;&nbsp;<a class='btn_cancel BtnSendEmailCompare' href='<?php echo $urlEmail;?>'>Gửi Email đối chiếu</a>
                <br><br><p class="f_size_15">Email: <?php echo implode(';', $aEmail); ?></p>
            <?php endif; ?>
        </div>
         
    <?php $this->endWidget(); ?>

    </div><!-- wide form -->
</div><!-- search-form -->

<style>
    .ui-datepicker-trigger { float: left;}
</style>

<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/jquery.floatThead.min.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/underscore-min.js"></script>    
<script>
$(document).ready(function(){
    $('.form').find('button:submit').click(function(){        
//        $.blockUI({ overlayCSS: { backgroundColor: '#fff' } }); 
        var wrapDiv = $(this).closest('.search-form');
        var customer_id = wrapDiv.find('.customer_id').val();
        console.log(customer_id);
        if(customer_id == ''){
//            wrapDiv.find('button:submit').hide();
        }
    });
    
    $('.rowHide').each(function(){
        var customer_id = $(this).attr('data-ref');
        $('.rowShow'+customer_id).show().html($(this).html());
    });
//    $('.hm_table').floatThead();// Gây chậm khi xem nhiều
    $('.BtnSendEmailCompare').click(function(){
       if(confirm('Bạn chắc chắn gửi email đối chiếu công nợ?')){
           $.blockUI({ overlayCSS: { backgroundColor: '#fff' } }); 
           return true;
       }
       return false;
    });
});
</script>

<?php if(isset($aData['CUSTOMER_ID']) || isset($aData['ClosingDebit']) || isset($aData['mCustomer']) ): ?>
    <?php include $view.'.php'; ?>
<?php endif; ?>