    <div class="row">
            <?php echo $form->labelEx($model,'type_vo', array('label'=>'Loại vỏ')); ?>
            <?php echo $form->dropDownList($model,'type_vo', $model->getArrayTypeDebitVo(),array('class'=>' w-200','empty'=>'Select')); ?>
    </div>
    <div class="row">
        <?php echo $form->labelEx($model,'customer_id'); ?>
        <?php echo $form->hiddenField($model,'customer_id', array('class'=>'customer_id')); ?>
        <?php
                // 1. limit search kh của sale
                $url = Yii::app()->createAbsoluteUrl('admin/ajax/search_user_by_code');
                // widget auto complete search user customer and supplier
                $extData = array(
                    'model'=>$model,
                    'field_customer_id'=>'customer_id',
                    'url'=> $url,
                    'name_relation_user'=>'rCustomer',
                    'ClassAdd' => 'w-400',
                );
                $this->widget('ext.SpaAutocompleteCustomer.SpaAutocompleteCustomer',
                    array('data'=>$extData));
                ?>
            <?php echo $form->error($model,'customer_id'); ?>
    </div>