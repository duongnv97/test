<tr>
    <td class="item_c"><?php echo $index++;?></td>
    <td class="item_c"><?php echo MyFormat::dateConverYmdToDmy($date);?></td>
    <td><?php echo $code_no;?></td>
    <td><?php echo $note;?></td>
    <td class="item_r "><?php echo $debit !=0 ? ActiveRecord::formatCurrencyRound($debit) : '';?></td>
    <td class="item_r "><?php echo $credit !=0 ? ActiveRecord::formatCurrencyRound($credit) : '';?></td>
</tr>