<?php 
$aCustomer              = isset($aData['CUSTOMER_MODEL']) ? $aData['CUSTOMER_MODEL'] : [];
$BEGIN_CASH             = isset($aData['BEGIN_CASH']) ? $aData['BEGIN_CASH'] : [];
$DATA_OPENING_DEBIT     = isset($aData['DATA_OPENING_DEBIT']) ? $aData['DATA_OPENING_DEBIT'] : [];
$DATA_IN_PERIOD_DEBIT   = isset($aData['DATA_IN_PERIOD_DEBIT']) ? $aData['DATA_IN_PERIOD_DEBIT'] : [];
$DATA_OPENING_CREDIT    = isset($aData['DATA_OPENING_CREDIT']) ? $aData['DATA_OPENING_CREDIT'] : [];
$DATA_IN_PERIOD_CREDIT  = isset($aData['DATA_IN_PERIOD_CREDIT']) ? $aData['DATA_IN_PERIOD_CREDIT'] : [];
$mAppCache              = new AppCache();
$listdataAgent          = $mAppCache->getAgentListdata(); $i = 1;
$listdataSale           = $mAppCache->getListdataUserByRole(ROLE_SALE);
$listdataSale1          = $mAppCache->getListdataUserByRole(ROLE_MONITORING_MARKET_DEVELOPMENT);
$listdataSale2          = $mAppCache->getListdataUserByRole(ROLE_EMPLOYEE_MAINTAIN);
$listdataSale           = $listdataSale + $listdataSale1 + $listdataSale2;
$index = 1;
$sumOpeningDebit = $sumOpeningCredit = $sumIncurredDebit = $sumIncurredCredit = $sumClosingDebit = $sumClosingCredit = 0;
$session=Yii::app()->session;
?>
<table class="tb hm_table">
    <thead>
        <tr>
            <th class="item_c">#</th>
            <th class="item_c">Mã kế toán</th>
            <th class="item_c">Khách hàng</th>
            <th class="item_c">Địa chỉ</th>
            <th class="item_c">Nợ đầu kỳ</th>
            <th class="item_c">Có đầu kỳ</th>
            <th class="item_c">Phát sinh nợ</th>
            <th class="item_c">Phát sinh có</th>
            <th class="item_c">Nợ cuối kỳ</th>
            <th class="item_c">Có cuối kỳ</th>
            <th class="item_c">Hạn TT</th>
            <th class="item_c">Sale</th>
            <th class="item_c">Nơi giao</th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($aCustomer as $mCustomer): ?>
        <?php
            if(!$mCustomer->allowAccessAgent($mCustomer->area_code_id)){
                continue;// Add check Dec2518
            }
            $agentName  = isset($listdataAgent[$mCustomer->area_code_id]) ? $listdataAgent[$mCustomer->area_code_id] : '';
            $saleName   = isset($listdataSale[$mCustomer->sale_id]) ? $listdataSale[$mCustomer->sale_id] : $mCustomer->sale_id;
            $customer_id = $mCustomer->id;
            $closingDebit1 = $closingCredit1 = 0;
            $closingDebit2 = $closingCredit2 = 0;
            
            $openingDebit1          = isset($BEGIN_CASH[$customer_id]['debit']) ? $BEGIN_CASH[$customer_id]['debit'] : 0;
            $openingCredit1         = isset($BEGIN_CASH[$customer_id]['credit']) ? $BEGIN_CASH[$customer_id]['credit'] : 0;
            
            // vì tiền của KH có thu tiền mặt và chi gas dư, nên ở đây phải trừ đi gas dư
            $incurredDebit1     = isset($DATA_OPENING_DEBIT[$customer_id]) ? $DATA_OPENING_DEBIT[$customer_id] : 0;// phải thu KH
//            $incurredCredit1    = $model->calcDebitCustomer($DATA_OPENING_CREDIT, $customer_id);// tính thu trong kỳ không được trừ gas dư
            $incurredCredit1    = isset($DATA_OPENING_CREDIT[$customer_id][GasMasterLookup::MASTER_TYPE_THU]) ? $DATA_OPENING_CREDIT[$customer_id][GasMasterLookup::MASTER_TYPE_THU] : 0;// Jul0317 đã thu - không trừ gas dư
            // 1. nợ và có đầu kỳ
            $model->calcClosingDebitCredit($openingDebit1, $openingCredit1, $incurredDebit1, $incurredCredit1, $closingDebit1, $closingCredit1);
            // 2. phát sinh nợ và có trong kỳ
            $incurredDebit2     = isset($DATA_IN_PERIOD_DEBIT[$customer_id]) ? $DATA_IN_PERIOD_DEBIT[$customer_id] : 0;
//            $incurredCredit2    = $model->calcDebitCustomer($DATA_IN_PERIOD_CREDIT, $customer_id);
            $incurredCredit2    = isset($DATA_IN_PERIOD_CREDIT[$customer_id][GasMasterLookup::MASTER_TYPE_THU]) ? $DATA_IN_PERIOD_CREDIT[$customer_id][GasMasterLookup::MASTER_TYPE_THU] : 0;
            /* 3. nợ và có cuối kỳ
             * công thức: nợ + nợ - có - có = A
             * Nếu A > 0 thì A nằm bên nợ cuối kỳ
             * Nếu A < 0 thì A nằm bên có cuối kỳ
             */
            $model->calcClosingDebitCredit($closingDebit1, $closingCredit1, $incurredDebit2, $incurredCredit2, $closingDebit2, $closingCredit2);
            // Jul1218 nếu nhỏ hơn 1k thì round = 0 - Fix Thúc 
            $closingDebit1          = $closingDebit1 > InventoryCustomer::AMOUNT_ROUND_DOWN ? $closingDebit1 : 0;
            $closingCredit1         = $closingCredit1 > InventoryCustomer::AMOUNT_ROUND_DOWN ? $closingCredit1 : 0;
            $closingDebit2          = $closingDebit2 > InventoryCustomer::AMOUNT_ROUND_DOWN ? $closingDebit2 : 0;
            $closingCredit2         = $closingCredit2 > InventoryCustomer::AMOUNT_ROUND_DOWN ? $closingCredit2 : 0;
            // Jul1218 nếu nhỏ hơn 1k thì round = 0 - Fix Thúc 
            
            if( $model->view_closing_debit && 
                ( $closingCredit2 > 0 || ((empty($closingCredit2) && empty($closingDebit2))))
            ){
                continue;
            }
            if($model->view_closing_credit && 
                ($closingDebit2 > 0 || ((empty($closingCredit2) && empty($closingDebit2)))) ){
                continue;
            }
            
            $sumOpeningDebit    += $closingDebit1;
            $sumOpeningCredit   += $closingCredit1;
            $sumIncurredDebit   += $incurredDebit2;
            $sumIncurredCredit  += $incurredCredit2;
            $sumClosingDebit    += $closingDebit2;
            $sumClosingCredit   += $closingCredit2;
            
            $url_ = Yii::app()->createAbsoluteUrl('admin/gasAppOrder/index', ['customer_id'=>$mCustomer->id]);
            $linkViewOrder = "<a href='$url_' target='_blank'>$mCustomer->first_name</a>";
            $mInventoryTemp = new InventoryCustomer();
            $mInventoryTemp->customer_id = $mCustomer->id;
            
        ?>
        <tr>
            <td class="item_c"><?php echo $index++;?></td>
            <td class="item_c"><?php echo $mCustomer->code_account;?></td>
            <!--<td><?php echo $mCustomer->code_bussiness.' - '.$linkViewOrder;?></td>-->
            <td><?php echo $mCustomer->code_bussiness.' - '.$linkViewOrder.$mInventoryTemp->getUrlUpdateCustomerType().$mInventoryTemp->getUrlLockCustomer($mCustomer);?></td>
            <td><?php echo $mCustomer->address;?></td>
            <td class="item_r"><?php echo $closingDebit1 !=0 ? ActiveRecord::formatCurrencyRound($closingDebit1) : '';?></td>
            <td class="item_r"><?php echo $closingCredit1 !=0 ? ActiveRecord::formatCurrencyRound($closingCredit1) : '';?></td>
            <td class="item_r"><?php echo $incurredDebit2 !=0 ? ActiveRecord::formatCurrencyRound($incurredDebit2) : '';?></td>
            <td class="item_r"><?php echo $incurredCredit2 !=0 ? ActiveRecord::formatCurrencyRound($incurredCredit2) : '';?></td>
            <td class="item_r item_b"><?php echo $closingDebit2 !=0 ? ActiveRecord::formatCurrencyRound($closingDebit2) : '';?></td>
            <td class="item_r item_b"><?php echo $closingCredit2 !=0 ? ActiveRecord::formatCurrencyRound($closingCredit2) : '';?></td>
            <td><?php echo $mCustomer->getMasterLookupName('payment');?></td>
            <td><?php echo $saleName;?></td>
            <td><?php echo $agentName;?></td>
        </tr>
        <?php endforeach; ?>
        <tr class="SumCol">
            <td class="item_r item_b" colspan="4">Sum</td>
            <td class="item_r item_b"><?php echo ActiveRecord::formatCurrencyRound($sumOpeningDebit);?></td>
            <td class="item_r item_b"><?php echo ActiveRecord::formatCurrencyRound($sumOpeningCredit);?></td>
            <td class="item_r item_b"><?php echo ActiveRecord::formatCurrencyRound($sumIncurredDebit);?></td>
            <td class="item_r item_b"><?php echo ActiveRecord::formatCurrencyRound($sumIncurredCredit);?></td>
            <td class="item_r item_b"><?php echo ActiveRecord::formatCurrencyRound($sumClosingDebit);?></td>
            <td class="item_r item_b"><?php echo ActiveRecord::formatCurrencyRound($sumClosingCredit);?></td>
            <td></td><td></td><td></td>
        </tr>
    </tbody>
</table>
<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/print-store-card.css" />
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/jquery.floatThead.min.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/underscore-min.js"></script>    
<script>
    $(function(){
       $('.hm_table tbody').prepend($('.SumCol').clone());
       $('.hm_table').floatThead();
    });
</script>