<?php 
$aCustomer              = isset($aData['CUSTOMER_MODEL']) ? $aData['CUSTOMER_MODEL'] : [];
$OUT_PUT_INVENTORY_VO   = isset($aData['OUT_PUT_INVENTORY_VO']) ? $aData['OUT_PUT_INVENTORY_VO'] : [];
$OUT_PUT_STORE_RESULT   = isset($aData['OUT_PUT_STORE_RESULT']) ? $aData['OUT_PUT_STORE_RESULT'] : [];
$index = 1;
$sumInventory = $sumPre = 0;
?>
<table class="tb hm_table sortable">
    <thead>
        <tr>
            <th class="item_c">#</th>
            <th class="item_c">Khách hàng</th>
            <th class="item_c">Địa chỉ</th>
            <th class="item_c">Vỏ tồn</th>
            <th class="item_c ClosingBalance">Vỏ ước tính</th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($aCustomer as $mCustomer): ?>
        <?php 
            $customer_id        = $mCustomer->id;
            $inventoryVo        = isset($OUT_PUT_INVENTORY_VO[$customer_id]) ? $OUT_PUT_INVENTORY_VO[$customer_id] : 0;
            $preVo              = isset($OUT_PUT_STORE_RESULT[$customer_id]) ? $OUT_PUT_STORE_RESULT[$customer_id] : 0;
            $url_ = Yii::app()->createAbsoluteUrl('admin/gasAppOrder/index', ['customer_id'=>$mCustomer->id]);
            $linkViewOrder = "<a href='$url_' target='_blank'>$mCustomer->first_name</a>";
            $mInventoryTemp = new InventoryCustomer();
            $mInventoryTemp->customer_id = $mCustomer->id;
            if($inventoryVo <= $preVo  || ($preVo == 0 && $inventoryVo == 0)){
                continue;
            }
            $sumInventory += $inventoryVo;
            $sumPre       += $preVo;
        ?>
        <tr>
            <td class="item_c"><?php echo $index++;?></td>
            <td><?php echo $mCustomer->code_bussiness.' - '.$linkViewOrder;?></td>
            <td><?php echo $mCustomer->address;?></td>
            <td class="item_r"><?php echo $inventoryVo;?></td>
            <td class="item_r"><?php echo $preVo;?></td>
        </tr>
        <?php endforeach; ?>
        <?php if($index > 1): ?>
        <tr class="SumCol">
            <td class="item_r item_b" colspan="3">Sum</td>
            <td class="item_r item_b"><?php echo $sumInventory; ?></td>
            <td class="item_r item_b"><?php echo $sumPre; ?></td>
        </tr>
        <?php endif; ?>
    </tbody>
</table>
<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/print-store-card.css" />
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/jquery.floatThead.min.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/underscore-min.js"></script>    
<script>
    $(function(){
       $('.hm_table tbody').prepend($('.SumCol').clone());
       $('.hm_table').floatThead();
    });
</script>

<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/sortElements/jquery.sortElements.js"></script>
<!--https://stackoverflow.com/questions/3160277/jquery-table-sort-->
<script>
$(function(){
var table = $('.sortable');
$('.ClosingBalance')
    .wrapInner('<span title="sort this column"/>')
    .each(function(){

        var th = $(this),
            thIndex = th.index(),
            inverse = false;

        th.click(function(){

            table.find('td').filter(function(){

                return $(this).index() === thIndex;

            }).sortElements(function(a, b){
                var aText = $.text([a]);
                var bText = $.text([b]);
                    aText = parseFloat(aText.replace(/,/g, ''));
                    bText = parseFloat(bText.replace(/,/g, ''));
                    aText = aText *1;
                    bText = bText *1;

                if( aText == bText )
                    return 0;

//                return aText > bText ? // asc 
                return aText < bText ? // DESC
                    inverse ? -1 : 1
                    : inverse ? 1 : -1;

            }, function(){

                // parentNode is the element we want to move
                return this.parentNode; 

            });

            inverse = !inverse;
            fnRefreshOrderNumber();
        });
    });
    
    $('.ClosingBalance').trigger('click');
    
});
</script>