<?php 
$urlExcel   = Yii::app()->createAbsoluteUrl('admin/inventoryCustomer/reportWarningVo', ['to_excel'=>1]);
?>
<div class="search-form" style="">
    <div class="wide form">
    <?php $form=$this->beginWidget('CActiveForm', array(
            'action'=>Yii::app()->createUrl($this->route),
            'method'=>'get',
    )); ?>
        <?php if(Yii::app()->user->hasFlash('successUpdate')): ?>
            <div class="success_div"><?php echo Yii::app()->user->getFlash('successUpdate');?></div>         
        <?php endif; ?>
        <div class="row more_col">
            <div class="col1">
                <?php echo Yii::t('translation', $form->label($model,'date_to')); ?>
                <?php 
                    $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                        'model'=>$model,        
                        'attribute'=>'date_to',
                        'options'=>array(
                            'showAnim'=>'fold',
                            'dateFormat'=> MyFormat::$dateFormatSearch,       
                            'maxDate'=> '0',
                            'changeMonth' => true,
                            'changeYear' => true,
                            'showOn' => 'button',
                            'buttonImage'=> Yii::app()->theme->baseUrl.'/admin/images/icon_calendar_r.gif',
                            'buttonImageOnly'=> true,                                
                        ),        
                        'htmlOptions'=>array(
                            'class'=>'w-16',
                            'size'=>'16',
                            'style'=>'height:20px;float:left;',
                            'readonly'=>'1',
                        ),
                    ));
                ?>
            </div>
            
            <div class="col2">
                <?php 
                // vì cột is_maintain không dùng trong loại KH của thẻ kho nên ta sẽ dùng cho 1: KH bình bò, 2. KH mối
                echo $form->labelEx($model,'type_customer'); ?>
                <?php echo $form->dropDownList($model,'type_customer', GasStoreCard::$DAILY_INTERNAL_TYPE_CUSTOMER,array('class'=>' w-200','empty'=>'Select')); ?>
            </div>
        </div>
            
        <div class="row">
        <?php echo $form->labelEx($model,'agent_id'); ?>
        <?php echo $form->hiddenField($model,'agent_id', array('class'=>'')); ?>
        <?php
                // 1. limit search kh của sale
                $url = Yii::app()->createAbsoluteUrl('admin/ajax/search_by_role', array('role'=> ROLE_AGENT));
                // widget auto complete search user customer and supplier
                $extData = array(
                    'model'=>$model,
                    'field_customer_id'=>'agent_id',
                    'url'=> $url,
                    'name_relation_user'=>'rAgent',
                    'ClassAdd' => 'w-400',
                    'field_autocomplete_name' => 'autocomplete_name_1',
                    'placeholder'=>'Nhập mã hoặc tên đại lý',
                );
                $this->widget('ext.SpaAutocompleteCustomer.SpaAutocompleteCustomer',
                    array('data'=>$extData));
                ?>
        </div>
        <div class="row">
        <?php echo $form->labelEx($model,'sale_id'); ?>
        <?php echo $form->hiddenField($model,'sale_id', array('class'=>'')); ?>
        <?php
                // 1. limit search kh của sale
                $url = Yii::app()->createAbsoluteUrl('admin/ajax/search_sale');
                // widget auto complete search user customer and supplier
                $extData = array(
                    'model'=>$model,
                    'field_customer_id'=>'sale_id',
                    'url'=> $url,
                    'name_relation_user'=>'rSale',
                    'ClassAdd' => 'w-400',
                    'field_autocomplete_name' => 'autocomplete_name_2',
                    'placeholder'=>'Nhập mã hoặc tên nhân viên',
                );
                $this->widget('ext.SpaAutocompleteCustomer.SpaAutocompleteCustomer',
                    array('data'=>$extData));
                ?>
        </div>
            
        <div class="row">
        <?php echo $form->labelEx($model,'customer_id'); ?>
        <?php echo $form->hiddenField($model,'customer_id', array('class'=>'customer_id')); ?>
        <?php
                // 1. limit search kh của sale
                $url = Yii::app()->createAbsoluteUrl('admin/ajax/search_user_by_code');
                // widget auto complete search user customer and supplier
                $extData = array(
                    'model'=>$model,
                    'field_customer_id'=>'customer_id',
                    'url'=> $url,
                    'name_relation_user'=>'rCustomer',
                    'ClassAdd' => 'w-400',
                );
                $this->widget('ext.SpaAutocompleteCustomer.SpaAutocompleteCustomer',
                    array('data'=>$extData));
                ?>
            <?php echo $form->error($model,'customer_id'); ?>
        </div>
            
        <div class="row">
            <?php echo $form->labelEx($model,'province_id'); ?>
            <div class="fix-label">
                <?php
                   $this->widget('ext.multiselect.JMultiSelect',array(
                         'model'=>$model,
                         'attribute'=>'province_id',
                         'data'=> GasProvince::getArrAll(),
                         // additional javascript options for the MultiSelect plugin
                        'options'=>array('selectedList' => 30,),
                         // additional style
                         'htmlOptions'=>array('style' => 'width: 800px;'),
                   ));    
               ?>
            </div>
        </div>

        <div class="row buttons" style="padding-left: 159px; margin:20px 0;">
            <?php $this->widget('bootstrap.widgets.TbButton', array(
                'buttonType'=>'submit',
                'label'=>Yii::t('translation','Xem Thống Kê'),
                'type'=>'null', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
                'size'=>'small', // null, 'large', 'small' or 'mini'
                //'htmlOptions' => array('style' => 'margin-bottom: 10px; float: right;'),
            )); ?>
            &nbsp;&nbsp;&nbsp;&nbsp;<a class='btn_cancel' href='<?php echo $urlExcel;?>'>Xuất Excel</a>
        </div>

    <?php $this->endWidget(); ?>

    </div><!-- wide form -->
</div><!-- search-form -->

<script>
    $(document).ready(function(){
        $('.form').find('button:submit').click(function(){
            $.blockUI({ overlayCSS: { backgroundColor: '#fff' } }); 
        });
    });
    
</script>