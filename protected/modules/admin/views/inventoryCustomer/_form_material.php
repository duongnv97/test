<?php 
//$index=1; 
$mAppCache = new AppCache();
$listdataMaterial = $mAppCache->getListdata('GasMaterials', AppCache::LISTDATA_MATERIAL);
//$model->getJsonVo();
$display_none = 'display_none';
?>
<div class="clr"></div>
<a href="javascript:;" class="f_size_15 AddVo">Thêm vỏ</a>
<table class="view_materials_table f_size_15 w-400 TableVo" style="width:100%">
    <tbody>
        <tr>
            <td>
                <?php foreach ($model->aInventoryVo as $materials_id_vo => $qty): ?>
                    <?php 
                        $name = isset($listdataMaterial[$materials_id_vo]) ? $listdataMaterial[$materials_id_vo] : '';
                        $display_none = '';
                    ?>
                    <div class="BlockVo float_l" style="margin-bottom: 8px;">
                    <?php // echo CHtml::dropDownList('materials_id_vo[]', $materials_id_vo,
//                        GasMaterials::getListVoInventory(), array('empty'=>'Chọn vỏ', 'class' => "w-300 gSelect h_30"));?>
                        <input class="materials_id_vo" value="<?php echo $materials_id_vo;?>" type="hidden" name="materials_id_vo[]">
                        <input class="float_l material_autocomplete w-250"  placeholder="Nhập mã hoặc tên vật tư" maxlength="100" value="<?php echo $name;?>" type="text" readonly="1">
                        <span onclick="" class="RemoveMaterialJs remove_item_material"></span>
                        
                        &nbsp;&nbsp;<input name="qty[]" class="w-30 h_20 f_size_15 input_qty item_r" maxlength="5" value="<?php echo $qty;?>" type="text" placeholder="SL">
                        &nbsp;&nbsp;&nbsp;<a href="javascript:;" class="BlockVoRemove"> Xóa</a>
                    </div>
                <?php endforeach; ?>
                
                <div class="BlockVo float_l" style="margin-bottom: 8px;">
                    <?php // echo CHtml::dropDownList('materials_id_vo[]', "",
//                    GasMaterials::getListVoInventory(), array('empty'=>'Chọn vỏ', 'class' => "w-300 gSelect h_30"));?>
                    
                    <input class="materials_id_vo" value="" type="hidden" name="materials_id_vo[]">
                    <input class="float_l material_autocomplete w-250"  placeholder="Nhập mã hoặc tên vật tư" maxlength="100" value="" type="text">
                    <span onclick="" class="RemoveMaterialJs remove_item_material"></span>
                    &nbsp;&nbsp;<input name="qty[]" class="w-30  h_20 f_size_15 input_qty item_r" maxlength="5" value="" type="text" placeholder="SL">
                    &nbsp;&nbsp;<a href="javascript:;" class="BlockVoRemove <?php echo $display_none;?>"> Xóa</a>
                </div>
            </td>
        </tr>
    </tbody>
</table>

<script>
    // to do bind autocompelte for input material
    // @param objInput : is obj input ex  $('.customer_autocomplete')    
    function bindMaterialAutocomplete(objInput){
        var availableMaterials = <?php echo MyFunctionCustom::getMaterialsJsonByType(GasMaterialsType::$ARR_WINDOW_VO); ?>;
        var parent_div = objInput.closest('.BlockVo');
        objInput.autocomplete({
            source: availableMaterials,
//            close: function( event, ui ) { $( "#GasStoreCard_materials_name" ).val(''); },
            select: function( event, ui ) {
                parent_div.find('.materials_id_vo').val(ui.item.id);
                parent_div.find('.material_autocomplete').attr('readonly',true);
            }
        });
    }
</script>