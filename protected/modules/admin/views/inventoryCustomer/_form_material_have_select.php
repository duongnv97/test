<?php 
//$index=1; 
//$listdataMaterial = AppCache::getListdata('GasMaterials', AppCache::LISTDATA_MATERIAL);
//$model->getJsonVo();

?>
<div class="clr"></div>
<a href="javascript:;" class="f_size_15 AddVo">Thêm vỏ</a>
<table class="view_materials_table f_size_15 w-400 TableVo" style="width:100%">
    <tbody>
        <tr>
            <td>
                <?php foreach ($model->aInventoryVo as $materials_id_vo => $qty): ?>
                    <div class="BlockVo float_l" style="margin-bottom: 8px;">
                    <?php echo CHtml::dropDownList('materials_id_vo[]', $materials_id_vo,
                        GasMaterials::getListVoInventory(), array('empty'=>'Chọn vỏ', 'class' => "w-300 gSelect h_30"));?>
                        <input name="qty[]" class="w-50 number_only_v1 h_20 f_size_15 input_qty" maxlength="2" value="<?php echo $qty;?>" type="text" placeholder="SL">
                        &nbsp;&nbsp;&nbsp;<a href="javascript:;" class="BlockVoRemove display_none"> Xóa</a>
                    </div>
                <?php endforeach; ?>
                
                <div class="BlockVo float_l" style="margin-bottom: 8px;">
                <?php echo CHtml::dropDownList('materials_id_vo[]', "",
                    GasMaterials::getListVoInventory(), array('empty'=>'Chọn vỏ', 'class' => "w-300 gSelect h_30"));?>
                    <input name="qty[]" class="w-50 number_only_v1 h_20 f_size_15 input_qty" maxlength="2" value="" type="text" placeholder="SL">
                    &nbsp;&nbsp;&nbsp;<a href="javascript:;" class="BlockVoRemove display_none"> Xóa</a>
                </div>
            </td>
        </tr>
    </tbody>
</table>
