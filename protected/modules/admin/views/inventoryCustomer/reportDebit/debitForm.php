<?php 
$aCustomer              = isset($aData['CUSTOMER_MODEL']) ? $aData['CUSTOMER_MODEL'] : [];
$sumZone                = [];
$DATA_DEBIT             = isset($aData['DATA_DEBIT']) ? $aData['DATA_DEBIT'] : [];
$DATA_ZONE              = isset($aData['DATA_ZONE']) ? $aData['DATA_ZONE'] : [];
$aZone                  = isset($aData['ZONE']) ? $aData['ZONE'] : [];
$index = 1;
$sumAll = $sumOpeningDebit = $sumIncurredDebit = $sumIncurredCredit = $sumClosingDebit = $sumClosingCredit = 0
?>
<table class="tb hm_table">
    <thead>
        <tr>
            <th class="item_c">#</th>
            <th class="item_c">Mã kế toán</th>
            <th class="item_c">Khách hàng</th>
            <th class="item_c">Địa chỉ</th>
            <th class="item_c">Hạn TT</th>
            <th class="item_c">Sale</th>
            <th class="item_c">Nơi giao</th>
            <th class="item_c">Nợ</th>
            <?php foreach ($aZone as $keyZone   => $zone): ?>
            <th class="item_c"><?php echo !empty($zone['title']) ? $zone['title'] : '' ?></th>
            <?php endforeach; ?>
            <th class="item_c">Tổng nợ phải thu</th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($aCustomer as $mCustomer): ?>
        <?php 
            $sumCurrent         = 0;
            $customer_id        = $mCustomer->id;
            $closingDebit1      = isset($DATA_DEBIT[$customer_id]) ? $DATA_DEBIT[$customer_id] : 0;
            if($closingDebit1 <= 0){
                continue;
            }
            $agentName          = isset($listdataAgent[$mCustomer->area_code_id]) ? $listdataAgent[$mCustomer->area_code_id] : '';
            $saleName           = isset($listdataSale[$mCustomer->sale_id]) ? $listdataSale[$mCustomer->sale_id] : $mCustomer->sale_id;
            $sumOpeningDebit    += $closingDebit1;
            
            $url_ = Yii::app()->createAbsoluteUrl('admin/gasAppOrder/index', ['customer_id'=>$mCustomer->id]);
            $linkViewOrder = "<a href='$url_' target='_blank'>$mCustomer->first_name</a>";
            $mInventoryTemp = new InventoryCustomer();
            $mInventoryTemp->customer_id = $mCustomer->id;
            
        ?>
        <tr>
            <td class="item_c"><?php echo $index++;?></td>
            <td class="item_c"><?php echo $mCustomer->code_account;?></td>
            <td><?php echo $mCustomer->code_bussiness.' - '.$linkViewOrder.$mInventoryTemp->getUrlUpdateCustomerType().$mInventoryTemp->getUrlLockCustomer();?></td>
            <td><?php echo $mCustomer->address;?></td>
            <td><?php echo $mCustomer->getMasterLookupName('payment');?></td>
            <td><?php echo $saleName;?></td>
            <td><?php echo $agentName;?></td>
            <td class="item_r"><?php echo $closingDebit1 !=0 ? ActiveRecord::formatCurrencyRound($closingDebit1) : '';?></td>
            <?php foreach ($aZone as $keyZone   => $zone): ?>
            <?php
            $curentZone         = isset($DATA_ZONE[$customer_id][$keyZone]) ? $DATA_ZONE[$customer_id][$keyZone]: 0;
            $sumAll             += $curentZone;
            $sumCurrent         += $curentZone;
            $sumZone[$keyZone]  = isset($sumZone[$keyZone]) ? $sumZone[$keyZone]+$curentZone : $curentZone;
            ?>
            <td class="item_r"><?php echo isset($DATA_ZONE[$customer_id][$keyZone]) ? ActiveRecord::formatCurrencyRound($DATA_ZONE[$customer_id][$keyZone]) : ''; ?></td>
            <?php endforeach; ?>
            <td class="item_r"><?php echo ActiveRecord::formatCurrencyRound($sumCurrent);?></td>
            
        </tr>
        <?php endforeach; ?>
        <tr class="SumCol">
            <td class="item_r item_b" colspan="7">Sum</td>
            <td class="item_r item_b"><?php echo ActiveRecord::formatCurrencyRound($sumOpeningDebit);?></td>
            <?php foreach ($aZone as $keyZone   => $zone): ?>
            <th class="item_r item_b"><?php echo isset($sumZone[$keyZone]) ? ActiveRecord::formatCurrencyRound($sumZone[$keyZone]) : ''; ?></th>
            <?php endforeach; ?>
            <td class="item_r item_b"><?php echo ActiveRecord::formatCurrencyRound($sumAll);?></td>
        </tr>
    </tbody>
</table>
<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/print-store-card.css" />
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/jquery.floatThead.min.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/underscore-min.js"></script>    
<script>
    $(function(){
       $('.hm_table tbody').prepend($('.SumCol').clone());
       $('.hm_table').floatThead();
    });
</script>