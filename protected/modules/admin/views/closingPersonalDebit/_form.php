<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'closing-personal-debit-form',
	'enableAjaxValidation'=>false,
        'htmlOptions' => array('enctype' => 'multipart/form-data'),
)); ?>

    <p class="note">Dòng có dấu <span class="required">*</span> là bắt buộc.</p>
    <?php echo MyFormat::BindNotifyMsg(); ?>
    
        		
        
    <div class="row">
        <?php echo $form->labelEx($model,'sale_id'); ?>
        <?php echo $form->textField($model,'sale_id'); ?>
        <?php echo $form->error($model,'sale_id'); ?>
    </div>
    <div class="row">
        <?php echo $form->labelEx($model,'status_send_mail'); ?>
        <?php echo $form->textField($model,'status_send_mail'); ?>
        <?php echo $form->error($model,'status_send_mail'); ?>
    </div>
    <div class="row">
        <?php echo $form->labelEx($model,'status_debit'); ?>
        <?php echo $form->textField($model,'status_debit'); ?>
        <?php echo $form->error($model,'status_debit'); ?>
    </div>
    <div class="row">
        <?php echo $form->labelEx($model,'created_date'); ?>
        <?php echo $form->textField($model,'created_date'); ?>
        <?php echo $form->error($model,'created_date'); ?>
    </div>
    <div class="row">
        <?php echo $form->labelEx($model,'customer_id'); ?>
        <?php echo $form->textField($model,'customer_id'); ?>
        <?php echo $form->error($model,'customer_id'); ?>
    </div>
    
    <div class="row">
        <?php echo $form->labelEx($model,'created_date_bigint'); ?>
        <?php echo $form->textField($model,'created_date_bigint',array('size'=>20,'maxlength'=>20)); ?>
        <?php echo $form->error($model,'created_date_bigint'); ?>
    </div>
	<div class="row buttons" style="padding-left: 141px;">
		        <?php $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType'=>'submit',
            'label'=>$model->isNewRecord ? 'Create' :'Save',
            'type'=>'null', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
            'size'=>'small', // null, 'large', 'small' or 'mini'
            //'htmlOptions' => array('style' => 'margin-bottom: 10px; float: right;'),
        )); ?>	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->
<script>
    $(document).ready(function(){
        $('.form').find('button:submit').click(function(){
            $.blockUI({ overlayCSS: { backgroundColor: '#fff' } }); 
        });
        
    });
</script>