<?php
$this->breadcrumbs=array(
	$this->pageTitle,
);

//$menus=array(
//            array('label'=>"Create $this->pageTitle", 'url'=>array('create')),
//);
//$this->menu= ControllerActionsName::createMenusRoles($menus, $actions);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('closing-personal-debit-grid', {
                url : $(this).attr('action'),
		data: $(this).serialize()
	});
	return false;
});
");

Yii::app()->clientScript->registerScript('ajaxupdate', "
$('#closing-personal-debit-grid a.ajaxupdate').live('click', function() {
    $.fn.yiiGridView.update('closing-personal-debit-grid', {
        type: 'POST',
        url: $(this).attr('href'),
        success: function() {
            $.fn.yiiGridView.update('closing-personal-debit-grid');
        }
    });
    return false;
});
");
?>

<h1>Danh Sách <?php echo $this->pageTitle; ?></h1>


<?php echo CHtml::link('Tìm Kiếm Nâng Cao','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'closing-personal-debit-grid',
	'dataProvider'=>$model->search(),
        'afterAjaxUpdate'=>'function(id, data){ fnUpdateColorbox();}',
        'template'=>'{pager}{summary}{items}{pager}{summary}',
//    'itemsCssClass' => 'items custom_here',
//    'htmlOptions'=> array('class'=>'grid-view custom_here'),
        'pager' => array(
            'maxButtonCount'=>  CmsFormatter::$PAGE_MAX_BUTTON,
        ),
	'enableSorting' => false,
	//'filter'=>$model,
	'columns'=>array(
        array(
            'header' => 'S/N',
            'type' => 'raw',
            'value' => '$this->grid->dataProvider->pagination->currentPage * $this->grid->dataProvider->pagination->pageSize + ($row+1)',
            'headerHtmlOptions' => array('width' => '10px','style' => 'text-align:center;'),
            'htmlOptions' => array('style' => 'text-align:center;')
        ),
            array(
                'name' => 'sale_id',
                'type' => 'text',
                'value' => '$data->getSale()',
            ),
            array(
                'name' => 'customer_id',
                'type' => 'html',
                'value' => '$data->getCustomer()',
            ),
            array(
                'name' => 'status_send_mail',
                'type' => 'text',
                'value' => '$data->getStatusSendMail()',
            ),
            array(
                'name' => 'status_process',
                'type' => 'text',
                'value' => '$data->getStatusProcess()',
            ),
            array(
                'name' => 'status_debit',
                'type' => 'text',
                'value' => '$data->getStatusDebit()',
            ),
            array(
                'name' => 'created_date',
                'type' => 'text',
                'value' => '$data->getCreatedDate()',
            ),
		array(
                    'header' => 'Actions',
                    'class'=>'CButtonColumn',
                    'template'=> ControllerActionsName::createIndexButtonRoles($actions),
                    'buttons'=>array(
                        'update' => array(
                            'visible' => 'false',
                        ),
                        'delete'=>array(
                            'visible' => 'false'
                        ),
                    ),
		),
	),
)); ?>

<script>
$(document).ready(function() {
    fnUpdateColorbox();
});

function fnUpdateColorbox(){   
    fixTargetBlank();
}
</script>