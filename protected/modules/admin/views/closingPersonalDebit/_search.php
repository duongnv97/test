<div class="search-form" style="">
        <div class="wide form">
            <?php $form=$this->beginWidget('CActiveForm', array(
//            'action'=>Yii::app()->createUrl($this->route),
                'action' => Yii::app()->createAbsoluteUrl('admin/closingPersonalDebit/reportVo'),
                'method'=>'get',
            )); ?>
        <div class="row">
        <?php echo $form->labelEx($model,'agent_id'); ?>
        <?php echo $form->hiddenField($model,'agent_id', array('class'=>'')); ?>
        <?php
                // 1. limit search kh của sale
                $url = Yii::app()->createAbsoluteUrl('admin/ajax/search_by_role', array('role'=> ROLE_AGENT));
                // widget auto complete search user customer and supplier
                $extData = array(
                    'model'=>$model,
                    'field_customer_id'=>'agent_id',
                    'url'=> $url,
                    'name_relation_user'=>'rAgent',
                    'ClassAdd' => 'w-400',
                    'field_autocomplete_name' => 'autocomplete_name_1',
                    'placeholder'=>'Nhập mã hoặc tên đại lý',
                );
                $this->widget('ext.SpaAutocompleteCustomer.SpaAutocompleteCustomer',
                    array('data'=>$extData));
                ?>
        </div>
        <?php 
        if(!$model->onlySearchCustomer):?>
        <div class="row">
        <?php echo $form->labelEx($model,'sale_id'); ?>
        <?php echo $form->hiddenField($model,'sale_id', array('class'=>'')); ?>
        <?php
                // 1. limit search kh của sale
                $url = Yii::app()->createAbsoluteUrl('admin/ajax/search_sale');
                // widget auto complete search user customer and supplier
                $extData = array(
                    'model'=>$model,
                    'field_customer_id'=>'sale_id',
                    'url'=> $url,
                    'name_relation_user'=>'rSale',
                    'ClassAdd' => 'w-400',
                    'field_autocomplete_name' => 'autocomplete_name_2',
                    'placeholder'=>'Nhập mã hoặc tên nhân viên',
                );
                $this->widget('ext.SpaAutocompleteCustomer.SpaAutocompleteCustomer',
                    array('data'=>$extData));
                ?>
        </div>
        <?php endif;?>
        <div class="row">
            <?php 
                echo $form->labelEx($model,'type_customer'); ?>
                <?php echo $form->dropDownList($model,'type_customer', GasStoreCard::$DAILY_INTERNAL_TYPE_CUSTOMER,array('class'=>' w-200', 'empty'=>'Select')); ?>
        </div>
        <div class="row">
        <?php echo $form->labelEx($model,'customer_id'); ?>
        <?php echo $form->hiddenField($model,'customer_id', array('class'=>'customer_id')); ?>
        <?php
                // 1. limit search kh của sale
                $url = Yii::app()->createAbsoluteUrl('admin/ajax/search_user_by_code');
                // widget auto complete search user customer and supplier
                $extData = array(
                    'model'=>$model,
                    'field_customer_id'=>'customer_id',
                    'url'=> $url,
                    'name_relation_user'=>'rCustomer',
                    'ClassAdd' => 'w-400',
                );
                $this->widget('ext.SpaAutocompleteCustomer.SpaAutocompleteCustomer',
                    array('data'=>$extData));
                ?>
            <?php echo $form->error($model,'customer_id'); ?>
        </div>
            <div class="row buttons" style="padding-left: 159px; margin:20px 0;">
                <?php $this->widget('bootstrap.widgets.TbButton', array(
                    'buttonType'=>'submit',
                    'label'=>Yii::t('translation','Xem Thống Kê'),
                    'type'=>'null', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
                    'size'=>'small', // null, 'large', 'small' or 'mini'
                    //'htmlOptions' => array('style' => 'margin-bottom: 10px; float: right;'),
                )); ?>
            </div>

            <?php $this->endWidget(); ?>

        </div><!-- wide form -->
    </div><!-- search-form -->