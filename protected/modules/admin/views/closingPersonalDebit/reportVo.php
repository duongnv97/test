<?php
$this->breadcrumbs=array(
    $this->pageTitle,
);
$menus=array();
$this->menu= ControllerActionsName::createMenusRoles($menus, $actions);
Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
");
$gridCss = Yii::app()->getAssetManager()->publish(Yii::getPathOfAlias('zii.widgets.assets')) . '/gridview/styles.css';
Yii::app()->getClientScript()->registerCssFile($gridCss);

?>
<h1><?php echo $this->pageTitle; ?></h1>
<div class="search-form" >
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->
<?php if(!empty($aData)) : 
    $index = 1;
?>   
<div class="grid-view">
    <table class="items hm_table">
        <thead>
            <tr>
                <th class="item_c" rowspan="2">#</th>
                <th class="item_c" rowspan="2">Khách hàng</th>
                <th class="item_c" rowspan="2">Địa chỉ</th>
                <th class="item_c" colspan="2">Nợ (vỏ)</th>
                <th class="item_c" colspan="2">Sản lượng (kg)</th>
                <th class="item_c" colspan="2">Hạn mức (vỏ)</th>
                <th class="item_c" colspan="2">Vượt mức (vỏ)</th>
            </tr>
            <tr>
                <th class="item_c">Vỏ nhỏ</th>
                <th class="item_c">Vỏ lớn</th>
                <th class="item_c">Vỏ nhỏ</th>
                <th class="item_c">Vỏ lớn</th>
                <th class="item_c">Vỏ nhỏ</th>
                <th class="item_c">Vỏ lớn</th>
                <th class="item_c">Vỏ nhỏ</th>
                <th class="item_c">Vỏ lớn</th>
            </tr>
        </thead>
        <tbody>
            <tr class = 'needSum' style="font-weight: bold; font-size: 1.2em"></tr>
            <?php 
                $aSum = [
                    'vo_no_nho' => 0,
                    'vo_no_lon' => 0,
                    'san_luong_nho' => 0,
                    'san_luong_lon' => 0,
                    'han_muc_nho' => 0,
                    'han_muc_lon' => 0,
                    'vuot_muc_nho' => 0,
                    'vuot_muc_lon' => 0,
                ];
            ?>
            <?php foreach ($aData as $customer_id => $detail): ?>
            <tr>
                <td class="item_c"><?php echo $index++; ?></td>
                <td class="item_l"><?php echo !empty($detail['customer_name']) ? $detail['customer_name'] : ''; ?></td>
                <td class="item_l"><?php echo !empty($detail['address']) ? $detail['address'] : ''; ?></td>
                <?php 
                    $vo_nho = !empty($detail['vo_nho']) && $detail['vo_nho'] > 0 ? $detail['vo_nho'] : 0;
                    $vo_lon = !empty($detail['vo_lon']) && $detail['vo_lon'] > 0 ? $detail['vo_lon'] : 0;
                    $aSum['vo_no_nho'] += $vo_nho;
                    $aSum['vo_no_lon'] += $vo_lon;
                ?>
                <td class="item_c"><?php echo $vo_nho > 0 ? $vo_nho : ''; ?></td>
                <td class="item_c"><?php echo $vo_lon > 0 ? $vo_lon : ''; ?></td>
                <?php 
                    $san_luong_vo_nho = !empty($detail['san_luong_vo_nho']) && $detail['san_luong_vo_nho'] > 0 ? $detail['san_luong_vo_nho'] : 0;
                    $san_luong_vo_lon = !empty($detail['san_luong_vo_lon']) && $detail['san_luong_vo_lon'] > 0? $detail['san_luong_vo_lon'] : 0;
                    $aSum['san_luong_nho'] += $san_luong_vo_nho;
                    $aSum['san_luong_lon'] += $san_luong_vo_lon;
                ?>
                <td class="item_c"><?php echo $san_luong_vo_nho > 0 ? $san_luong_vo_nho : ''; ?></td>
                <td class="item_c"><?php echo $san_luong_vo_lon > 0 ? $san_luong_vo_lon : ''; ?></td>
                <?php 
                    $so_luong_vo_nho = !empty($detail['han_muc_vo_nho']) && $detail['han_muc_vo_nho'] > 0 ? $detail['han_muc_vo_nho'] : 0;
                    $so_luong_vo_lon = !empty($detail['han_muc_vo_lon']) && $detail['han_muc_vo_lon'] > 0 ? $detail['han_muc_vo_lon'] : 0;
                    $aSum['han_muc_vo_nho'] += $so_luong_vo_nho;
                    $aSum['han_muc_vo_lon'] += $so_luong_vo_lon;
                ?>
                <td class="item_c"><?php echo $so_luong_vo_nho > 0 ? $so_luong_vo_nho : ''; ?></td>
                <td class="item_c"><?php echo $so_luong_vo_lon > 0 ? $so_luong_vo_lon : ''; ?></td>
                <?php 
                    $vuot_muc_vo_nho = !empty($detail['vuot_muc_vo_nho']) && $detail['vuot_muc_vo_nho'] > 0 ? $detail['vuot_muc_vo_nho'] : 0;
                    $vuot_muc_vo_lon = !empty($detail['vuot_muc_vo_lon']) && $detail['vuot_muc_vo_lon'] > 0 ? $detail['vuot_muc_vo_lon'] : 0;
                    $aSum['vuot_muc_vo_nho'] += $vuot_muc_vo_nho;
                    $aSum['vuot_muc_vo_lon'] += $vuot_muc_vo_lon;
                    $textVuot = '';
                    if($vuot_muc_vo_lon > 0 || $vuot_muc_vo_nho > 0){
                        $textVuot .= '<span class="high_light_tr"><span>';
                    }
                ?>
                <td class="item_c"><?php echo $textVuot; echo $vuot_muc_vo_nho > 0 ? $vuot_muc_vo_nho : ''; ?></td>
                <td class="item_c"><?php echo $vuot_muc_vo_lon > 0 ? $vuot_muc_vo_lon : ''; ?></td>
            </tr>
            <?php endforeach; ?>
            <tr class ='showSum' style="font-weight: bold; font-size: 1.2em">
                <td class="item_c" colspan="3">Tổng cộng</td>
                <td class="item_c"><?php echo $aSum['vo_no_nho']; ?></td>
                <td class="item_c"><?php echo $aSum['vo_no_lon']; ?></td>
                <td class="item_c"><?php echo $aSum['san_luong_nho']; ?></td>
                <td class="item_c"><?php echo $aSum['san_luong_lon']; ?></td>
                <td class="item_c"><?php echo $aSum['han_muc_vo_nho']; ?></td>
                <td class="item_c"><?php echo $aSum['han_muc_vo_lon']; ?></td>
                <td class="item_c"><?php echo $aSum['vuot_muc_vo_nho']; ?></td>
                <td class="item_c"><?php echo $aSum['vuot_muc_vo_lon']; ?></td>
            </tr>
        </tbody>
    </table>
</div>
<?php endif;?>

<script type="text/javascript">
    fnShowhighLightTr();
    fnAddClassOddEven('items');
    $('.needSum').html($('.showSum').html());
</script>
