<?php
$this->breadcrumbs=array(
	$this->singleTitle,
);

$menus=array(
        array('label'=>"Create $this->singleTitle", 'url'=>array('create')),
);
if($model->agentCanCreate()){
    $this->menu= ControllerActionsName::createMenusRoles($menus, $actions);
}


Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('gas-order-promotion-grid', {
                url : $(this).attr('action'),
		data: $(this).serialize()
	});
	return false;
});
");

Yii::app()->clientScript->registerScript('ajaxupdate', "
$('#gas-order-promotion-grid a.ajaxupdate').live('click', function() {
    $.fn.yiiGridView.update('gas-order-promotion-grid', {
        type: 'POST',
        url: $(this).attr('href'),
        success: function() {
            $.fn.yiiGridView.update('gas-order-promotion-grid');
        }
    });
    return false;
});
");
?>

<?php include "index_button.php";?>
<h1>Danh Sách <?php echo $this->pluralTitle; ?></h1>

<?php echo CHtml::link('Tìm Kiếm Nâng Cao','#',array('class'=>'search-button')); ?>
<!--<div class="search-form FixDatePicker" style="">-->
<div class="search-form FixDatePicker" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'gas-order-promotion-grid',
	'dataProvider'=>$model->search(),
        'afterAjaxUpdate'=>'function(id, data){ fnUpdateColorbox();}',
        'template'=>'{pager}{summary}{items}{pager}{summary}',
//    'itemsCssClass' => 'items custom_here',
//    'htmlOptions'=> array('class'=>'grid-view custom_here'),
        'pager' => array(
            'maxButtonCount'=>  CmsFormatter::$PAGE_MAX_BUTTON,
        ),
	'enableSorting' => false,
	//'filter'=>$model,
	'columns'=>array(
        array(
            'header' => 'S/N',
            'type' => 'raw',
            'value' => '$this->grid->dataProvider->pagination->currentPage * $this->grid->dataProvider->pagination->pageSize + ($row+1)',
            'headerHtmlOptions' => array('width' => '10px','style' => 'text-align:center;'),
            'htmlOptions' => array('style' => 'text-align:center;')
        ),
//		'code_no',
                array(
                    'header' => 'Đơn vị',
                    'value'=>'$data->getAgent()',
                ),
                array(
                    'header'=>'Chi Tiết',
                    'type' => 'html',
                    'value'=>'$data->getSomeDetail()',
//                    'htmlOptions' => array('class' => 'w-300')
                ),
//                array(
//                    'name' => 'note',
//                    'type' => 'html',
//                    'value' => '$data->getNote()',
//                    'htmlOptions' => array('style' => 'max-width:300px;')
//                ),
                array(
                    'header'=>'Tồn kho hiện tại',
                    'type' => 'html',
                    'value'=>'$data->formatInventory()',
                ),
                array(
                    'header'=>'Chi tiết đặt hàng',
                    'type' => 'raw',
                    'value'=>'"<b>Người tạo: ".$data->getUidLogin()."</b><br><br>".$data->formatDetail()',
//                    'htmlOptions' => array('class' => 'w-300')
                ),
		array(
                    'name' => 'status',
                    'value' => '$data->getStatus()',
                    'htmlOptions' => array('style' => 'text-align:center;')
                ),
                array(
                    'name' => 'created_date',
                    'type' => 'Datetime',
                    'htmlOptions' => array('style' => 'width:90px;')
                ),
		array(
                    'header' => 'Actions',
                    'class'=>'CButtonColumn',
                    'template'=> ControllerActionsName::createIndexButtonRoles($actions, array('view','update','delete','print')),
                    'buttons'=>array(
                        'update'=>array(
                            'visible'=> '$data->CanUpdateOrderPromotion()',
                        ),
                        'delete'=>array(
                            'visible'=> 'GasCheck::canDeleteData($data)',
                        ),
                        'print'=>array(
                            'label'=>'Print đặt hàng',
                            'imageUrl'=>Yii::app()->theme->baseUrl . '/admin/images/print-icon.png',
                            'options'=>array('class'=>''),
                            'url'=>'Yii::app()->createAbsoluteUrl("admin/gasOrderPromotion/print",
                                array("id"=>$data->id) )',                            
                        ),
                    ),
		),
	),
)); ?>

<script>
$(document).ready(function() {
    fnUpdateColorbox();
});

function fnUpdateColorbox(){   
    fixTargetBlank();
    fnShowhighLightTr();
}
</script>