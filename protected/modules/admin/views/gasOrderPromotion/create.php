<?php
$this->breadcrumbs=array(
	$this->pluralTitle=>array('index'),
	'Tạo Mới',
);

$menus = array(		
        array('label'=>"$this->singleTitle Management", 'url'=>array('index')),
);
$this->menu= ControllerActionsName::createMenusRoles($menus, $actions);
$delivery_user_update = 0;
?>

<h1>Tạo Mới <?php echo $this->singleTitle; ?></h1>

<?php include '_form.php'; ?>