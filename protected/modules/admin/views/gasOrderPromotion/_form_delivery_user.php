<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'gas-order-promotion-form',
	'enableAjaxValidation'=>false,
        'htmlOptions' => array('enctype' => 'multipart/form-data'),
)); 
    $model->InitMaterial();
?>
    <p class="note">Dòng có dấu <span class="required">*</span> là bắt buộc.</p>
    <?php echo MyFormat::BindNotifyMsg(); ?>
        
    <div class="row ">
        <?php echo $form->labelEx($model,'uid_login'); ?>
        <div class="item_b"><?php echo $model->getUidLogin(); ?></div>
    </div>
    <div class="clr"><br/></div>
    <div class="row ">
        <?php echo $form->labelEx($model,'user_id_delivery'); ?>
        <div class="item_b"><?php echo $model->getUserDelivery(); ?></div>
    </div>
    <div class="clr"><br/></div>
    <?php include '_form_detail.php';?>
    <div class="row">
        <?php echo $form->labelEx($model,'note'); ?>
        <div class="float_l item_b"><?php echo $model->getNote(); ?></div>
    </div>
    <?php echo $form->textArea($model,'note',array('class'=>'w-400 display_none','rows'=>6, 'cols'=>50)); ?>
    <div class="clr "></div>
    
    <?php if($model->showConfirmButton()): ?>
        <div class="row buttons" style="padding-left: 141px;">
            <?php 
//            $this->widget('bootstrap.widgets.TbButton', array(
//                'buttonType'=>'submit',
//                'label'=>$model->isNewRecord ? 'Create' :'Save',
//                'type'=>'null', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
//                'size'=>'small', // null, 'large', 'small' or 'mini'
//                //'htmlOptions' => array('style' => 'margin-bottom: 10px; float: right;'),
//            )); 
            ?>
            <button type="button" class="btn_cancel p_confirm_order">Xác nhận đơn đặt hàng</button>
        </div>
    <?php endif; ?>
<?php $this->endWidget(); ?>

</div><!-- form -->
<script>
    $(document).ready(function(){
        $('.form').find('button:submit').click(function(){
            $.blockUI({ overlayCSS: { backgroundColor: '#fff' } }); 
        });
        
        $('.p_confirm_order').click(function(){
            if(confirm("Bạn chắc chắn xác nhận đơn hàng hợp lệ? Sau khi xác nhận bạn sẽ không sửa được đơn hàng.")){
                var p_confirm_order = '<input type="hidden" name="p_confirm_order" value="1">';
                var form = $(this).closest('form');
                form.append(p_confirm_order);
                form.submit();
            }
        });
    });
</script>