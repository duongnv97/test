<?php
$this->breadcrumbs=array(
    $this->pluralTitle => array('index'),
    ' ' . $this->singleTitle . ' : ',
);

$menus = array(
    array('label'=>"$this->pluralTitle Management", 'url'=>array('index')),
    array('label'=>"Cập Nhật $this->singleTitle", 'url'=>array('update', 'id'=>$model->id)),
);
$this->menu= ControllerActionsName::createMenusRoles($menus, $actions);
$aOrderMiss = $model->getMissOfAgent();
$missSumQty = 0;
$missSumQtyReal = 0;
$missSumQtyMiss = 0;
?>

<h1>Xem và In: <?php echo $this->singleTitle; ?></h1>

<script  src="<?php echo Yii::app()->theme->baseUrl; ?>/js/jquery.printElement.min.js"></script>
<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/print-store-card.css" />
<script type="text/javascript">
	$(document).ready(function(){
            $(".button_print").click(function(){
                $('#printElement').printElement({ overrideElementCSS: ['<?php echo Yii::app()->theme->baseUrl;?>/css/print-store-card.css'] });
            });
            fnShowhighLightTr();
	});
</script>


<div class="sprint" style=" padding-right: 800px;">
    <a class="button_print" href="javascript:void(0);" title="In Đặt Hàng">
        <img src="<?php echo Yii::app()->theme->baseUrl; ?>/images/print.png">
    </a>    
</div>
<div class="clr"></div>

<div class="container BoxPrint2" id="printElement">
    <table cellpadding="0" cellspacing="0">
        <tr>
            <td style="width: 60%; border-bottom: 2px solid #000; height: 157px;" class="">
                <div class="logo">
                    <img src="<?php echo Yii::app()->theme->baseUrl; ?>/images/gas_logo80.jpg">
                </div>            
            </td>
            <td class="item_r" style="border-bottom: 2px solid #000; vertical-align: bottom">
                <p>
                    <span class="item_b f_size_14">CÔNG TY CỔ PHẦN DẦU KHÍ MIỀN NAM </span>
                </p>
                <p>
                    Đ.C: 86 Nguyễn Cửu Vân, F17, Bình Thạnh, HCM 
                </p>
                <p>
                    Tel: 08.38 409 409 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Fax: 08.6294 7078
                </p>
            </td>
        </tr>
        <tr><td class="item_r f_size_14 r_padding_20 t_padding_5" colspan="2"></td></tr>
    </table>
    
    <table cellpadding="0" cellspacing="0">
        <tr>
            <td class="item_c">
                <h2 style="margin: 35px 0 0">ĐẶT HÀNG VẬT TƯ - KHUYẾN MÃI</h2>
            </td>
        </tr>
        <tr>
            <td class="agent_info">
                <span class="item_b">Đơn vị đặt:</span> <?php echo $model->getUidLogin();?> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <span class="item_b">Ngày đặt:</span> <?php echo $model->getCreatedDate();?>
            </td>
        </tr>
        <tr>
            <td class="agent_info"><span class="item_b">Đại lý:</span> <?php echo $model->getAgent();?></td>
        </tr>
        <tr>
            <td class="agent_info"><span class="item_b">Xuất tại kho:</span> </td>
        </tr>
    </table>
    
    <?php echo $model->formatDetailPrint(); ?>
    <?php if(count($aOrderMiss)): ?>
    <p class="item_b">Đơn Hàng Giao Thiếu, Cần Bổ Sung</p>
    <table class="tb hm_table summary_promotion ">
    <thead>
        <tr>
            <th class="item_b item_c">#</th>
            <th class="item_b item_c">Vật Tư</th>
            <th class="item_b item_c">ĐV</th>
            <th class="item_b item_c">Slg đặt</th>
            <th class="item_b item_c">Đã Nhận</th>
            <th class="item_b item_c">SL Thiếu <br> (chênh lệch)</th>
            <th class="item_b item_c">Slg xuất thực tế</th>
            <th class="item_b item_c w-300">Ghi Chú</th>
        </tr>
    </thead>
    <tbody>
    <?php foreach ($aOrderMiss as $key=>$mDetail): ?>
        <?php  
            $qtyMiss = $mDetail->qty - $mDetail->qty_real;
            if($qtyMiss == 0){
                continue;
            }
            $missSumQty += $mDetail->qty;
            $missSumQtyReal += $mDetail->qty_real;
            $missSumQtyMiss += $qtyMiss;
        ?>
        <tr>
            <td><?php echo $key+1;?></td>
            <td><?php echo $mDetail->getMaterialName();?></td>
            <td><?php echo $mDetail->unit;?></td>
            <td class="item_c"><?php echo ActiveRecord::formatCurrency($mDetail->qty);?></td>
            <td class="item_c"><?php echo ActiveRecord::formatCurrency($mDetail->qty_real);?></td>
            <td class="item_c"><span class="high_light_td_red"></span><?php echo ActiveRecord::formatCurrency($qtyMiss);?></td>
            <td></td><td></td>
        </tr>
    <?php endforeach; ?>
        <tr>
            <td class="item_b item_r" colspan="3">Tổng cộng</td>
            <td class="item_b item_c"><?php echo ActiveRecord::formatCurrency($missSumQty);?></td>
            <td class="item_b item_c"><?php echo ActiveRecord::formatCurrency($missSumQtyReal);?></td>
            <td class="item_b item_c"><?php echo ActiveRecord::formatCurrency($missSumQtyMiss);?></td>
            <td></td><td></td>
        </tr>
    </tbody>
    </table>
    <?php endif; ?>
    
    <table cellpadding="0" cellspacing="0">
        <tr>
            <td class="" style="width: 70%;"></td>
            <td class="">HCM, ngày <?php echo date('d');?> tháng <?php echo date('m');?> năm <?php echo date('Y');?></td>
        </tr>
    </table>
    
    <br>
    <table cellpadding="0" cellspacing="0">
        <tr>
            <td class="item_c" style="vertical-align: top;">
                <span class="item_b">Người Giao</span>
                <br><i>( Ký, ghi rõ họ tên)</i>
            </td>
            <td class="item_c" style="vertical-align: top;">
                <span class="item_b">Người Nhận</span>
                <br><i>( Ký, ghi rõ họ tên)</i>
            </td>
        </tr>
    </table>
    
</div>