<?php
$this->breadcrumbs=array(
	$this->singleTitle,
);

$menus=array(
            array('label'=>"Create $this->singleTitle", 'url'=>array('create')),
);
$this->menu= ControllerActionsName::createMenusRoles($menus, $actions);
$cUid = Yii::app()->user->id;
$cRole = Yii::app()->user->role_id;
$aUserDelivery = $model->getArrayUserDelivery();
$cAgent = MyFormat::getAgentId();
$accountingCanEdit = $model->accountingCanEdit();

$UserDeliveryView = false;
$aDataAccounting = array();
if(isset($aUserDelivery[$cUid]) ){
    $UserDeliveryView = true;
}
$aDataAccounting = $model->getAccountingQty('', $aStartAndEndDate['week_start'], $aStartAndEndDate['week_end']);
$aProvince = GasProvince::getArrAll();

//$allOrderPromotion = GasOrderPromotion::model()->findAll();
//foreach($allOrderPromotion as $mOrderPromotion){
////    GasOrderPromotionDetail::UpdateStatusAndUserDelivery($mOrderPromotion);
//    $mOrderPromotion->update(array('province_id'));
//}
//die;
?>
<?php include "index_button.php";?>
<?php include "summary_search.php";?>
<?php echo MyFormat::BindNotifyMsg(); ?>

<div class="sprint" style=" padding-right: 743px;">
    <a class="button_print" href="javascript:void(0);" title="In Đặt Hàng">
        <img src="<?php echo Yii::app()->theme->baseUrl; ?>/images/print.png">
    </a>
</div>
<div class="clr"></div>
<script  src="<?php echo Yii::app()->theme->baseUrl; ?>/js/jquery.printElement.min.js"></script>
<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/print-store-card.css" />
<script type="text/javascript">
    $(document).ready(function(){
        $(".button_print").click(function(){
                $('#printElement').printElement({ overrideElementCSS: ['<?php echo Yii::app()->theme->baseUrl;?>/css/print-store-card.css'] });
        });
    });
</script>

<form method="post" class="">
    <div class="container" id="printElement">
    <h1>Tổng hợp đặt hàng từ <?php echo MyFormat::dateConverYmdToDmy($aStartAndEndDate['week_start']); ?> đến <?php echo MyFormat::dateConverYmdToDmy($aStartAndEndDate['week_end']); ?></h1>

    <?php if($cRole != ROLE_SUB_USER_AGENT || in_array($cAgent, $model->aAgentAllowSummary)): ?>
        <table class="tb hm_table summary_promotion ">
        <?php foreach ($aUserDelivery as $uid=>$name): ?>
            <?php if($UserDeliveryView && $cUid != $uid) { continue;}?>
            <?php if($cRole == ROLE_SUB_USER_AGENT && $uid != GasLeave::DUNG_NTT):
                continue;
                endif; ?>
            <tr>
                <td class="item_b w-200"><?php echo $name; ?></td>
                <td>
                    <?php if(isset($data[$uid])): ?>
                        <?php if($uid == GasLeave::DUNG_NTT || $uid == GasLeave::PHUONG_PTK): ?>                    
                            <?php foreach ($data[$uid] as $province_id => $aRes): ?>
                                <?php if(in_array($cAgent, $model->aAgentAllowSummary) && $province_id != GasProvince::TINH_BINH_DUONG): 
                                continue;
                                endif; ?>
                                <p class="f_size_15 item_b"><?php echo $aProvince[$province_id]; ?></p>
                                <?php //  echo isset($data[$uid]) ? $model->getSummaryTable($aRes, $uid, $aDataAccounting): ""; ?>
                                <?php  echo $model->getSummaryTable($aRes, $uid, $aDataAccounting); ?>
                            <?php endforeach; ?>
                        <?php else: ?>
                                <?php if($cRole != ROLE_SUB_USER_AGENT): ?>
                                    <?php include "summary_table.php"; ?>
                                <?php endif; ?>
                        <?php endif; ?>
                    <?php endif; ?>
                </td>
            </tr>
        <?php endforeach; ?>
        <?php if($accountingCanEdit): ?>
            <tr>
                <td colspan="2">
                    <div class="form float_r " >
                        <input type="submit" class="btn_cancel" value="Save">
                    </div>
                </td>
            </tr>
        <?php endif; ?>
        </table>
    <?php endif; ?>
    </div>
</form>

<script>
    $(function(){
        $('.accounting_qty').change(function(){
            var table = $(this).closest('table');
            var sum = 0;
            table.find('.accounting_qty').each(function(){
                sum += ($(this).val()*1);
            });
            table.find('.SumOneAccounting').html(commaSeparateNumber(sum));
        });
        
        $('.CopySL').click(function(){
            $('.InputQtyAccounting').each(function(){
                $(this).val($(this).closest('tr').find('.HideQty').text());
            });
        });
        
        
    });
</script>


<style>
    .summary_promotion td {padding: 10px;}
</style>

