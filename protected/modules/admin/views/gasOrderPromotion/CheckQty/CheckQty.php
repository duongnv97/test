<?php
$this->breadcrumbs=array(
    $this->singleTitle,
);

$menus=array(
    array('label'=>"Create $this->singleTitle", 'url'=>array('create')),
);
$this->menu= ControllerActionsName::createMenusRoles($menus, $actions);

?>
<?php $this->renderPartial('application.modules.admin.views.gasOrderPromotion.index_button', array('model'=>$model));?>

<?php include "_searchQty.php";?>
<?php echo MyFormat::BindNotifyMsg(); ?>

<div class="sprint" style=" padding-right: 743px;">
    <a class="button_print" href="javascript:void(0);" title="In Đặt Hàng">
        <img src="<?php echo Yii::app()->theme->baseUrl; ?>/images/print.png">
    </a>
</div>
<div class="clr"></div>
<script  src="<?php echo Yii::app()->theme->baseUrl; ?>/js/jquery.printElement.min.js"></script>
<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/print-store-card.css" />
<script type="text/javascript">
    $(document).ready(function(){
        $(".button_print").click(function(){
                $('#printElement').printElement({ overrideElementCSS: ['<?php echo Yii::app()->theme->baseUrl;?>/css/print-store-card.css'] });
        });
    });
</script>

<div class="container" id="printElement">
<h1>Tổng hợp đặt hàng từ <?php echo $model->date_from; ?> đến <?php echo $model->date_to; ?></h1>
<?php if(isset($data['ITEMS'])): ?>
<table class="tb hm_table summary_promotion ">
    <thead>
        <tr>
            <th>#</th>
            <th>Vật tư</th>
            <th>Kế toán đặt</th>
            <th>SL thực nhập từ xưởng/trạm</th>
            <th>Chênh lệch</th>
        </tr>
    </thead>
    <tbody>
    <?php $index = 1; $session=Yii::app()->session; ?>
    <?php foreach ($data['ITEMS'] as $materials_type_id => $aInfo): ?>
    <?php foreach ($aInfo as $materials_type_id => $materials_id): ?>
        <?php 
            $name = isset($session['MATERIAL_MODEL'][$materials_id]['name']) ? $session['MATERIAL_MODEL'][$materials_id]['name'] : "";
            $unit = isset($session['MATERIAL_MODEL'][$materials_id]['unit']) ? $session['MATERIAL_MODEL'][$materials_id]['unit'] : "";
            $accountingQty  = isset($data['ACCOUNTING_QTY'][$model->user_id_delivery][$materials_id]) ? ActiveRecord::formatCurrency($data['ACCOUNTING_QTY'][$model->user_id_delivery][$materials_id]) : "";
            $agentQty       = isset($data['AGENT_QTY'][$model->user_id_delivery][$materials_id]) ? ActiveRecord::formatCurrency($data['AGENT_QTY'][$model->user_id_delivery][$materials_id]) : "";
            if(empty($accountingQty) && empty($agentQty)){
                continue;
            }
            $classFocus = "";
            if($accountingQty == $agentQty){
                $classFocus = "high_light_tr";
            }
            $qtyChange = $accountingQty - $agentQty;
        ?>
        <tr>
            <td class="item_c <?php echo $classFocus;?>"><?php echo $index++; ?></td>
            <td><?php echo $name; ?></td>
            <td class="item_c"><?php echo $accountingQty > 0 ? ActiveRecord::formatCurrency($accountingQty) : ""; ?></td>
            <td class="item_c"><?php echo $agentQty > 0 ? ActiveRecord::formatCurrency($agentQty) : ''; ?></td>
            <td class="item_c"><?php echo ActiveRecord::formatCurrency($qtyChange); ?></td>
        </tr>
    <?php endforeach; ?>
    <?php endforeach; ?>
    </tbody>
</table>
<?php endif; ?>

</div>

<style>
    .summary_promotion td {padding: 10px;}
</style>

<script>
$(document).ready(function() {
    fnShowhighLightTr();
});
</script>