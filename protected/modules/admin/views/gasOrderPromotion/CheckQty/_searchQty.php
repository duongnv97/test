<div class="search-form">
    <div class="wide form">
    <?php $form=$this->beginWidget('CActiveForm', array(
            'action'=> Yii::app()->createAbsoluteUrl("admin/gasOrderPromotion/index", array('CheckQty'=>1)),
            'method'=>'get',
    )); ?>

    <div class="row">
        <?php echo $form->label($model,'date_from',array()); ?>
        <?php 
            $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                'model'=>$model,        
                'attribute'=>'date_from',
                'options'=>array(
                    'showAnim'=>'fold',
                    'dateFormat'=> 'dd-mm-yy',
    //                            'minDate'=> '0',
                    'changeMonth' => true,
                    'changeYear' => true,
                    'showOn' => 'button',
                    'buttonImage'=> Yii::app()->theme->baseUrl.'/admin/images/icon_calendar_r.gif',
                    'buttonImageOnly'=> true,                                
                ),        
                'htmlOptions'=>array(
                    'class'=>'w-16',
                    'style'=>'height:20px;float:left;',
                    'readonly'=>1,
                ),
            ));
        ?>            

        <?php echo $form->label($model,'date_to',array()); ?>
        <?php 
            $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                'model'=>$model,        
                'attribute'=>'date_to',
                'options'=>array(
                    'showAnim'=>'fold',
                    'dateFormat'=> 'dd-mm-yy',
    //                            'minDate'=> '0',
                    'changeMonth' => true,
                    'changeYear' => true,
                    'showOn' => 'button',
                    'buttonImage'=> Yii::app()->theme->baseUrl.'/admin/images/icon_calendar_r.gif',
                    'buttonImageOnly'=> true,                                
                ),        
                'htmlOptions'=>array(
                    'class'=>'w-16',
                    'style'=>'height:20px;float:left;',
                    'readonly'=>1,
                ),
            ));
        ?>
    </div>
    <div class="row ">
        <?php echo $form->label($model,'user_id_delivery'); ?>
        <?php echo $form->dropDownList($model,'user_id_delivery', $model->getArrayUserDelivery(),array('class'=>'w-400')); ?>
        <?php echo $form->error($model,'user_id_delivery'); ?>
    </div>

    <div class="row buttons" style="padding-left: 159px;">
        <?php $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType'=>'submit',
            'label'=>'Search',
            'type'=>'null', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
            'size'=>'small', // null, 'large', 'small' or 'mini'
            //'htmlOptions' => array('style' => 'margin-bottom: 10px; float: right;'),
        )); ?>	
    </div>

    <?php $this->endWidget(); ?>
    </div>
</div>    

<style>
    .ui-datepicker-trigger { float: left;}
</style>
