<div class="form">
    <?php
        $LinkNormal     = Yii::app()->createAbsoluteUrl('admin/gasOrderPromotion/index');
        $LinkClosed     = Yii::app()->createAbsoluteUrl('admin/gasOrderPromotion/index', array( 'status'=> GasOrderPromotion::STATUS_COMPLETE));
        $LinkClosedMissing = Yii::app()->createAbsoluteUrl('admin/gasOrderPromotion/index', array( 'status'=> GasOrderPromotion::STATUS_COMPLETE_MISSING));
        $LinkSummary    = Yii::app()->createAbsoluteUrl('admin/gasOrderPromotion/summary', array( 'summary'=> 1));
        $LinkMiss       = Yii::app()->createAbsoluteUrl('admin/gasOrderPromotion/index', array( 'missing'=> 1));
        $LinkCheckQty   = Yii::app()->createAbsoluteUrl('admin/gasOrderPromotion/index', array( 'CheckQty'=> 1));
        
    ?>
    <h1><?php echo $this->adminPageTitle;?>
        <a class='btn_cancel f_size_14 <?php echo (!isset($_GET['CheckQty']) && !isset($_GET['status']) && !isset($_GET['summary']) && !isset($_GET['missing']) ) ? "active":"";?>' href="<?php echo $LinkNormal;?>">Mới Tạo</a>
        <a class='btn_cancel f_size_14 <?php echo ( isset($_GET['status']) && $_GET['status'] == GasOrderPromotion::STATUS_COMPLETE ) ? "active":"";?>' href="<?php echo $LinkClosed;?>">Đã Giao Đủ</a>
        <a class='btn_cancel f_size_14 <?php echo ( isset($_GET['status']) && $_GET['status'] == GasOrderPromotion::STATUS_COMPLETE_MISSING ) ? "active":"";?>' href="<?php echo $LinkClosedMissing;?>">Đã Giao Thiếu</a>
        <?php if(GasCheck::isAllowAccess("GasOrderPromotion", "summary")): ?>
            <a class='btn_cancel f_size_14 <?php echo isset($_GET['summary']) ? "active":"";?>' href="<?php echo $LinkSummary;?>">Tổng hợp đặt hàng</a>
        <?php endif; ?>
        <a class='btn_cancel f_size_14 <?php echo ( isset($_GET['missing'])) ? "active":"";?>' href="<?php echo $LinkMiss;?>">Tổng Hợp Đơn Hàng Thiếu</a>
        <?php if($model->canViewCheckQty()): ?>
            <a class='btn_cancel f_size_14 <?php echo isset($_GET['CheckQty']) ? "active":"";?>' href="<?php echo $LinkCheckQty;?>">Đối chiếu SL</a>
        <?php endif; ?>
    </h1> 
</div>