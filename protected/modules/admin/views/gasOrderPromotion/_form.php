<div class="form">
<?php
    $cRole = MyFormat::getCurrentRoleId();
    $hideClass='';
    if($cRole==ROLE_SUB_USER_AGENT){
        $hideClass='display_none';
    }
?>
    
<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'gas-order-promotion-form',
	'enableAjaxValidation'=>false,
        'htmlOptions' => array('enctype' => 'multipart/form-data'),
)); ?>

    <p class="note">Dòng có dấu <span class="required">*</span> là bắt buộc.</p>
    <?php echo MyFormat::BindNotifyMsg(); ?>
    <?php if(!$model->isNewRecord): ?>
    <div class="row ">
        <?php echo $form->labelEx($model,'created_date'); ?>
        <label><?php echo MyFormat::dateConverYmdToDmy($model->created_date);?></label>
    </div>
    <div class="clr"></div>
    <?php endif; ?>
    
    <div class="row <?php echo $hideClass;?>">
    <?php echo $form->labelEx($model,'agent_id'); ?>
    <?php echo $form->hiddenField($model,'agent_id', array('class'=>'')); ?>
    <?php
            // 1. limit search kh của sale
            $url = Yii::app()->createAbsoluteUrl('admin/ajax/search_by_role', array('role'=> ROLE_AGENT));
            // widget auto complete search user customer and supplier
            $aData = array(
                'model'=>$model,
                'field_customer_id'=>'agent_id',
                'url'=> $url,
                'name_relation_user'=>'rAgent',
                'ClassAdd' => 'w-400',
                'field_autocomplete_name' => 'autocomplete_name_1',
                'placeholder'=>'Nhập mã hoặc tên đại lý',
                'fnSelectCustomer'=>'fnSelectCustomer',
            );
            $this->widget('ext.SpaAutocompleteCustomer.SpaAutocompleteCustomer',
                array('data'=>$aData));
            ?>
        <?php echo $form->error($model,'agent_id'); ?>
    </div>
    
    <div class="row ">
        <?php echo $form->labelEx($model,'user_id_delivery'); ?>
        <?php echo $form->dropDownList($model,'user_id_delivery', $model->getArrayUserDelivery(),array('class'=>'w-400','empty'=>'Select')); ?>
        <?php echo $form->error($model,'user_id_delivery'); ?>
    </div>
    <?php include '_form_detail.php';?>
    <div class="row">
        <?php echo $form->labelEx($model,'note'); ?>
        <?php echo $form->textArea($model,'note',array('class'=>'w-900','rows'=>6)); ?>
        <?php echo $form->error($model,'note'); ?>
    </div>
    <div class="row">
        <?php echo $form->labelEx($model,'note_after_receive'); ?>
        <?php echo $form->textArea($model,'note_after_receive',array('class'=>'w-900','rows'=>6)); ?>
        <?php echo $form->error($model,'note_after_receive'); ?>
    </div>
    
    <div class="row buttons" style="padding-left: 141px;">
            <?php $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType'=>'submit',
            'label'=>$model->isNewRecord ? 'Create' :'Save',
            'type'=>'null', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
            'size'=>'small', // null, 'large', 'small' or 'mini'
            //'htmlOptions' => array('style' => 'margin-bottom: 10px; float: right;'),
        )); ?>	
    </div>

<?php $this->endWidget(); ?>

</div><!-- form -->
<script>
    $(document).ready(function(){
        $('.form').find('button:submit').click(function(){
            $.blockUI({ overlayCSS: { backgroundColor: '#fff' } }); 
        });
    });
    
    function fnSelectCustomer(user_id, idField, idFieldCustomer){
        var agent_id = $(idFieldCustomer).val();
        console.log(agent_id);
        var next = '<?php echo GasCheck::getCurl();?>?agent_id='+agent_id;
        next = updateQueryStringParameter(next, 'agent_id', agent_id);
        $.blockUI({ overlayCSS: { backgroundColor: '#fff' } });
        window.location = next;
    }
</script>