<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=> GasCheck::getCurl(),
	'method'=>'get',
)); 
$needMore = array('get_only_material'=>1);
$sTableInventory = "";
$aJsonMaterial = MyFunctionCustom::getMaterialsJsonByTypeWithInventory($model, GasMaterialsType::$PROMOTION, $sTableInventory, $needMore);
$listDataMaterial = GasMaterials::sessionGetListData(GasMaterials::NAME_MATERIAL_JSON);
?>
        <div class="row">
		<?php echo Yii::t('translation', $form->labelEx($model,'ext_materials_id')); ?>
                <?php echo $form->hiddenField($model,'ext_materials_id'); ?>		
                <?php 
                    // widget auto complete search material
                    $aData = array(
                        'model'=>$model,
                        'field_material_id'=>'ext_materials_id',
                        'name_relation_material'=>'rMaterial',
                        'field_autocomplete_name'=>'autocomplete_material',
                        'MaterialsJson'=> $aJsonMaterial,
                    );
                    $this->widget('ext.GasAutocompleteMaterial.GasAutocompleteMaterial',
                        array('data'=>$aData));                                        
                ?>             
		<?php echo $form->error($model,'ext_materials_id'); ?>
	</div>
        <div class="row">
            <?php echo $form->label($model,'material_by_agent',array()); ?>
            <?php echo $form->dropDownList($model,'material_by_agent', $model->getArrayGroupMaterial(),array('class'=>'w-370 material_by_agent','empty'=>'Select')); ?>
	</div>
    
        <div class="row group_subscriber w-3">
            <?php echo Yii::t('translation', $form->label($model,'ext_materials_multi')); ?>
            <div class="fix-label">
                <?php
                   $this->widget('ext.multiselect.JMultiSelect',array(
                         'model'=>$model,
                         'attribute'=>'ext_materials_multi',
                         'data'=> $listDataMaterial,
                         // additional javascript options for the MultiSelect plugin
                         'options'=>array('selectedList' => 30,),
                         // additional style
                         'htmlOptions'=>array('style' => 'width: 380px; height:600px;'),
                   ));    
               ?>
            </div>
        </div>
    
        <div class="row">
            <?php echo $form->label($model,'date_from',array()); ?>
            <?php 
                $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                    'model'=>$model,        
                    'attribute'=>'date_from',
                    'options'=>array(
                        'showAnim'=>'fold',
                        'dateFormat'=> MyFormat::$dateFormatSearch,
        //                            'minDate'=> '0',
                        'changeMonth' => true,
                        'changeYear' => true,
                        'showOn' => 'button',
                        'buttonImage'=> Yii::app()->theme->baseUrl.'/admin/images/icon_calendar_r.gif',
                        'buttonImageOnly'=> true,                                
                    ),        
                    'htmlOptions'=>array(
                        'class'=>'w-16',
                        'style'=>'height:20px;float:left;',
                    ),
                ));
            ?>            

            <?php echo $form->label($model,'date_to',array()); ?>
            <?php 
                $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                    'model'=>$model,        
                    'attribute'=>'date_to',
                    'options'=>array(
                        'showAnim'=>'fold',
                        'dateFormat'=> MyFormat::$dateFormatSearch,
        //                            'minDate'=> '0',
                        'changeMonth' => true,
                        'changeYear' => true,
                        'showOn' => 'button',
                        'buttonImage'=> Yii::app()->theme->baseUrl.'/admin/images/icon_calendar_r.gif',
                        'buttonImageOnly'=> true,                                
                    ),        
                    'htmlOptions'=>array(
                        'class'=>'w-16',
                        'style'=>'height:20px;float:left;',
                    ),
                ));
            ?>              

        </div>
    
        <div class="row">
            <?php echo $form->labelEx($model,'uid_login'); ?>
            <?php echo $form->hiddenField($model,'uid_login', array('class'=>'')); ?>
            <?php
                // 1. limit search kh của sale
                $url = Yii::app()->createAbsoluteUrl('admin/ajax/search_for_user_login');
                // widget auto complete search user customer and supplier
                $aData = array(
                    'model'=>$model,
                    'field_customer_id'=>'uid_login',
                    'url'=> $url,
                    'name_relation_user'=>'rUidLogin',
                    'field_autocomplete_name'=>'autocomplete_name',
                    'ClassAdd' => 'w-400',
    //                'ShowTableInfo' => 0,
                );
                $this->widget('ext.SpaAutocompleteCustomer.SpaAutocompleteCustomer',
                    array('data'=>$aData));                                        
            ?>

            <?php echo $form->error($model,'uid_login'); ?>
        </div>
    
        <div class="row">
            <?php echo $form->label($model,'code_no',array()); ?>
            <?php echo $form->textField($model,'code_no',array('class'=>'w-400','maxlength'=>50)); ?>
	</div>

	<div class="row buttons" style="padding-left: 159px;">
		        <?php $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType'=>'submit',
            'label'=>'Search',
            'type'=>'null', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
            'size'=>'small', // null, 'large', 'small' or 'mini'
            //'htmlOptions' => array('style' => 'margin-bottom: 10px; float: right;'),
        )); ?>	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->
<script>
    $(function(){
        $('.material_by_agent').change(function(){
            var select = $(this).val();
            //Do it simple http://stackoverflow.com/questions/6966260/jquery-multiselect-set-a-value-as-selected-in-the-multiselect-dropdown
            if(select == <?php echo GasOrderPromotion::GROUP_AGENT;?>){
                $('#GasOrderPromotion_ext_materials_multi option').prop('selected', false);
                var data="<?php echo $model->getIdMaterialAgent();?>";
                //Make an array
                var dataarray=data.split(",");
                // Set the value
                $("#GasOrderPromotion_ext_materials_multi").val(dataarray);
                // Then refresh
                $("#GasOrderPromotion_ext_materials_multi").multiselect("refresh");
            }else if(select == <?php echo GasOrderPromotion::GROUP_KHO;?>){
                $('#GasOrderPromotion_ext_materials_multi option').prop('selected', false);
                var data="<?php echo $model->getIdMaterialKho();?>";
                //Make an array
                var dataarray=data.split(",");
                // Set the value
                $("#GasOrderPromotion_ext_materials_multi").val(dataarray);
                // Then refresh
                $("#GasOrderPromotion_ext_materials_multi").multiselect("refresh");
            }else{
                $('#GasOrderPromotion_ext_materials_multi option').prop('selected', false);
                // Then refresh
                $("#GasOrderPromotion_ext_materials_multi").multiselect("refresh");
            }
        });
        
    });
</script>