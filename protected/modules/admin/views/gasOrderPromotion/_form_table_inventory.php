<?php 
$sTableInventory = "";
$aJsonMaterial = MyFunctionCustom::getMaterialsJsonByTypeWithInventory($model, GasMaterialsType::$PROMOTION, $sTableInventory);

if(in_array($model->status, $model->aStatusCanUpdate)){
    $dayAllow = date('Y-m-d');
    $dayAllow = MyFormat::modifyDays($dayAllow, $model->getDayAllowUpdate(), '-');
    $canEdit = MyFormat::compareTwoDate($model->created_date, $dayAllow);    
    if(!$canEdit){
        $readonly = "1";
    }
}elseif(in_array($model->status, $model->ARR_STATUS_CLOSE)){
    $readonly = "1";
}

?>
<div class="row">
    <label>Tồn Hiện Tại</label>
    <table class="materials_table hm_table">
        <thead>
            <tr>
                <th class="item_c">#</th>
                <th class="item_code item_c">Tên Vật Tư</th>
                <th class="item_c">Đơn Vị</th>
                <th class="item_c">Tồn kho</th>
            </tr>
        </thead>
        <tbody>
            <?php echo $sTableInventory;?>
        </tbody>
    </table>
</div>