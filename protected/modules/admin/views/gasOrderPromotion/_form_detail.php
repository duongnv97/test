<?php 
$readonly = "";
$aJsonMaterial = "";
?>

<?php // if(!$delivery_user_update): ?>
    <?php include "_form_table_inventory.php"; ?>
    <?php include "_form_detail_add_icon.php"; ?>
<?php /* else: ?>
    <div class="row">
        <label>Tồn Hiện Tại</label>
        <?php echo $model->formatInventory(); ?>
    </div>
    <br>
<script>
    $(function(){
        $('.delivery_user_update').addClass('materials_table hm_table');
        $('.delivery_user_update td').addClass('padding_5');
    });
    
</script>

<?php endif; */ ?>

<div class="row">
    <label>&nbsp</label>
    <table class="materials_table hm_table g_tb1 WrapRowQtySum ">
        <thead>
            <tr>
                <th class="item_c">#</th>
                <th class="item_code item_c">Tên Vật Tư</th>
                <th class="item_c">Đơn Vị</th>
                <th class="item_c">Tồn kho</th>
                <th class="item_c">Slg</th>
                <th class="item_c">Slg Thực Nhận</th>
                <th class="item_c">Ghi Chú</th>
                <th class="item_unit last item_c">Xóa</th>
            </tr>
        </thead>
        <tbody>
            <?php 
                $max_upload = GasSupportCustomer::MAX_ITEM;
                $max_upload_show = 5;
                $items_qty_sum = 0;
                $items_inventory_sum = 0;
                $cRole = Yii::app()->user->role_id;
            ?>
            <?php if(count($model->aModelDetail)):?>
            <?php foreach($model->aModelDetail as $key=>$item):?>
            <tr class="materials_row RowQtySum">
                <td class="item_c order_no"></td>
                <td class="w-400 col_material">
                    <?php echo $form->hiddenField($item,'materials_id[]', array('class'=>'materials_id_hide','value'=>$item->materials_id)); ?>
                    <input class="float_l material_autocomplete w-350"  placeholder="Nhập mã hoặc tên vật tư" maxlength="100" value="<?php echo $item->getMaterialName();?>" type="text" readonly="true">
                    <span onclick="" class="remove_material_js remove_item_material"></span>
                    
                    <?php echo $form->hiddenField($item,'id[]', array('class'=>'','value'=>$item->id)); ?>
                </td>
                <td class="item_c w-50">
                    <span class=" items_unit"><?php echo $item->unit;?></span>
                </td>
                <td class="item_c w-50">
                    <span class=" items_inventory_text"><?php echo ActiveRecord::formatNumberInput($item->inventory);?></span>
                    <?php echo $form->hiddenField($item,'inventory[]', array('class'=>'items_inventory', 'value'=> ActiveRecord::formatNumberInput($item->inventory))); ?>
                </td>
                <td class="item_c w-50">
                    <?php echo $form->textField($item,'qty[]', array('class'=>'w-50 item_r items_qty number_only_v1 items_calc', 'maxlength'=>'8', 'value'=> ActiveRecord::formatNumberInput($item->qty), 'readonly'=>$readonly)); ?>
                </td>
                <td class="item_c">
                    <?php echo $form->textField($model->mDetail,'qty_real[]', array('class'=>'w-50 item_r items_qty_real number_only_v1 ', 'maxlength'=>'8', 'value'=> ActiveRecord::formatNumberInput($item->qty_real))); ?>
                </td>
                <td class="item_c">
                    <?php echo $form->textField($model->mDetail,'note[]', array('class'=>'w-150 ', 'value'=> $item->getNote())); ?>
                </td>
                <td class="item_c last"><span class="remove_icon_only"></span></td>
            </tr>
            <?php $max_upload_show--; $items_qty_sum += $item->qty; $items_inventory_sum+=$item->inventory ?>
            <?php endforeach;?>
            <?php endif;?>

            <?php for($i=1; $i<=($max_upload-count($model->aModelDetail)); $i++): ?>
            <?php $display = "";
                if($i>$max_upload_show || !empty($readonly) )
                    $display = "display_none";
            ?>
            <tr class="materials_row RowQtySum <?php echo $display;?>">
                <td class="item_c order_no"></td>
                <td class=" col_material w-400">
                    <?php echo $form->hiddenField($model->mDetail,'materials_id[]', array('class'=>'materials_id_hide')); ?>
                    <input class="float_l material_autocomplete w-350"  placeholder="Nhập mã hoặc tên vật tư" maxlength="100" value="" type="text">
                    <span onclick="" class="remove_material_js remove_item_material"></span>
                </td>
                <td class="item_c ">
                    <span class=" items_unit"></span>
                </td>
                <td class="item_c ">
                    <span class=" items_inventory_text"></span>
                    <?php echo $form->hiddenField($model->mDetail,'inventory[]', array('class'=>'items_inventory')); ?>
                </td>
                <td class="item_c">
                    <?php echo $form->textField($model->mDetail,'qty[]', array('class'=>'w-50 item_r items_qty number_only_v1 items_calc', 'maxlength'=>'8')); ?>
                </td>
                <td class="item_c">
                    <?php echo $form->textField($model->mDetail,'qty_real[]', array('class'=>'w-50 item_r items_qty_real number_only_v1 ', 'maxlength'=>'8')); ?>
                </td>
                <td class="item_c">
                    <?php echo $form->textField($model->mDetail,'note[]', array('class'=>'w-150  ')); ?>
                </td>
                <td class="item_c last"><span class="remove_icon_only"></span></td>
            </tr>
            <?php endfor;?>
        </tbody>
        <tfoot>
            <tr>
                <td class="item_b item_r f_size_16" colspan="3">Tổng Cộng</td>
                <td class="item_b item_r f_size_16 items_inventory_sum r_padding_10" ><?php echo ActiveRecord::formatCurrency($items_inventory_sum);?></td>
                <td class="item_b item_r f_size_16 items_qty_sum r_padding_10" ><?php echo ActiveRecord::formatCurrency($items_qty_sum);?></td>
                <td></td>
            </tr>
        </tfoot>
    </table>
</div>

<?php Yii::app()->clientScript->registerCoreScript('jquery.ui');?>
<script>
    
    <?php // if($cRole == ROLE_SUB_USER_AGENT):?>
    $(document).keydown(function(e) {
        if(e.which == 119) {
            <?php if(empty($readonly)): // Apr 11, 2016 cho phép kế toán thêm mới vật tư ?>
                fnBuildRow();
            <?php endif;?>
        }
    });
    function fnBuildRow(){
        $('.g_tb1 tbody').find('tr:visible:last').next('tr').show();
    }
    
    <?php // endif;?>
    
    $(document).ready(function(){
        fnRefreshOrderNumber();
        fnBindRemoveIcon();
        fnBindAllAutocomplete();
        fnBindRemoveMaterialFix('materials_id_hide', 'material_autocomplete');
        fnRowSumAnyField(".items_inventory", ".items_inventory_sum");
    });
    
    function fnAfterRemoveIcon(){
        fnRowSumAnyField(".items_inventory", ".items_inventory_sum");
    }
    

    function fnBindAllAutocomplete(){
        // material
        $('.material_autocomplete').each(function(){
            bindMaterialAutocomplete($(this));
        });
    }
    
    // to do bind autocompelte for input material
    // @param objInput : is obj input ex  $('.customer_autocomplete')    
    function bindMaterialAutocomplete(objInput){
        var availableMaterials = <?php echo $aJsonMaterial;?>;
        var parent_div = objInput.closest('.col_material');
        objInput.autocomplete({
            source: availableMaterials,
//            close: function( event, ui ) { $( "#GasStoreCard_materials_name" ).val(''); },
            select: function( event, ui ) {
                parent_div.find('.materials_id_hide').val(ui.item.id);
                parent_div.find('.material_autocomplete').attr('readonly',true);
                fnSetValue(objInput, ui);
                fnRowSumAnyField(".items_inventory", ".items_inventory_sum");
            }
        });
    }
        
    function fnSetValue(objInput, ui){
        var tr = objInput.closest('tr');
        tr.find('.items_unit').text(ui.item.unit);
        tr.find('.items_inventory').val(ui.item.inventory);
        tr.find('.items_inventory_text').text(commaSeparateNumber(ui.item.inventory));
    }
    
</script>