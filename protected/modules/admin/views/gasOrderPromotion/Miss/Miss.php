<?php
$this->breadcrumbs=array(
	$this->singleTitle,
);
$menus=array(
        array('label'=>"Create $this->singleTitle", 'url'=>array('create')),
);
if($model->agentCanCreate()){
    $this->menu= ControllerActionsName::createMenusRoles($menus, $actions);
}
$cRole = Yii::app()->user->role_id;
$session=Yii::app()->session;
$data = $model->Miss();
$aAgent = Users::getArrDropdown(ROLE_AGENT);
$prevAgent  = 0;
$cAgent     = 0;
if($cRole == ROLE_SUB_USER_AGENT){
    $cAgent = MyFormat::getAgentId();
}


?>
<?php $this->renderPartial('application.modules.admin.views.gasOrderPromotion.index_button', array('model'=>$model)); ?>
<h1>Tổng Hợp Đơn Hàng Còn Thiếu từ ngày <?php echo GasOrderPromotionDetail::$DateMiss; ?></h1>
<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/print-store-card.css" />
<table class="tb hm_table summary_promotion ">
    <thead>
        <tr>
            <th class="item_b item_c">Đại Lý</th>
            <th class="item_b item_c">Vật Tư, Hàng Khuyến Mãi</th>
            <th class="item_b item_c">Yêu Cầu</th>
            <th class="item_b item_c">Thực Nhận</th>
            <th class="item_b item_c">SL Thiếu (chênh lệch)</th>
        </tr>
    </thead>
    <tbody>
    <?php foreach ($data as $mDetail): ?>
        <?php 
            $agentName = $aAgent[$mDetail->agent_id];
            $materialName = isset($session['MATERIAL_MODEL'][$mDetail->materials_id]['name']) ? $session['MATERIAL_MODEL'][$mDetail->materials_id]['name'] : "";
            $missQty = $mDetail->qty_real - $mDetail->qty;
            if($missQty == 0 || ( $cRole == ROLE_SUB_USER_AGENT && $cAgent != $mDetail->agent_id) ){
                continue;
            }
        ?>
        <?php if($prevAgent != 0 && $prevAgent != $mDetail->agent_id): ?>
        <tr><td colspan="5" class="high_light_tr"></td></tr>
        <?php endif; ?>
        <tr>
            <td class=""><?php echo $agentName;?></td>
            <td class=""><?php echo $materialName;?></td>
            <td class="item_c"><?php echo ActiveRecord::formatCurrency($mDetail->qty);?></td>
            <td class="item_c"><?php echo ActiveRecord::formatCurrency($mDetail->qty_real);?></td>
            <td class="item_c item_b "><span class="high_light_td_red"></span><?php echo ActiveRecord::formatCurrency($missQty);?></td>
        </tr>
        <?php $prevAgent = $mDetail->agent_id; ?>
    <?php endforeach; ?>
    </tbody>
</table>

<script>
$(document).ready(function() {
    fnUpdateColorbox();
});

function fnUpdateColorbox(){
    fnShowhighLightTr();
}
</script>