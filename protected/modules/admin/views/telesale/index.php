<?php
$this->breadcrumbs=array(
	$this->singleTitle,
);

$menus=array(
            array('label'=>"Create $this->singleTitle", 'url'=>array('create')),
);
$this->menu= ControllerActionsName::createMenusRoles($menus, $actions);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('telesale-grid', {
                url : $(this).attr('action'),
		data: $(this).serialize()
	});
	return false;
});
");

Yii::app()->clientScript->registerScript('ajaxupdate', "
$('#telesale-grid a.ajaxupdate').live('click', function() {
    $.fn.yiiGridView.update('telesale-grid', {
        type: 'POST',
        url: $(this).attr('href'),
        success: function() {
            $.fn.yiiGridView.update('telesale-grid');
        }
    });
    return false;
});
");
?>
<?php echo MyFormat::BindNotifyMsg(); ?>

<h1>Danh Sách <?php echo $this->pluralTitle; ?></h1>
<?php include 'index_button.php'; ?>

<?php echo CHtml::link('Tìm Kiếm Nâng Cao','#',array('class'=>'search-button')); ?>
<div class="search-form">

<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $mPromotionUser = new AppPromotionUser(); $cRole = MyFormat::getCurrentRoleId(); $cUid = MyFormat::getCurrentUid();?>
<?php if($model->showSetSaleId()): ?>
    <div class="form">
        <a class="btn_cancel f_size_14 IframeCreateCustomer text_under_none" href="<?php echo $mPromotionUser->getUrlCreateCustomerTelesale();?>">Tạo KH Telesale</a>
    </div>
<?php endif; ?>

<div class="form">
    <?php $form=$this->beginWidget('CActiveForm', array(
            'id'=>'telesale-form',
            'enableAjaxValidation'=>false,
            'htmlOptions' => array('enctype' => 'multipart/form-data'),
    )); ?>

    <?php $this->widget('zii.widgets.grid.CGridView', array(
        'id'=>'telesale-grid',
        'dataProvider'=>$model->getCustomer(),
        'afterAjaxUpdate'=>'function(id, data){ fnUpdateColorbox();}',
        'template'=>'{pager}{summary}{items}{pager}{summary}',
        'pager' => array(
            'maxButtonCount'=>  CmsFormatter::$PAGE_MAX_BUTTON,
        ),
        'enableSorting' => false,
        //'filter'=>$model,
        'columns'=>array(
        array(
            'header' => 'S/N',
            'type' => 'raw',
            'value' => '$this->grid->dataProvider->pagination->currentPage * $this->grid->dataProvider->pagination->pageSize + ($row+1)',
            'headerHtmlOptions' => array('width' => '10px','style' => 'text-align:center;'),
            'htmlOptions' => array('style' => 'text-align:center;')
        ),
        array(
            'class'=>'CCheckBoxColumn',
            'selectableRows'=>2,
            'id'=>'Telesale[customer]',
            'htmlOptions'=>array(
                'class'=>'$data->canModify()',
            ),
        ),
        array(
            'name'=>'isDisplay',
            'type'=>'raw',
            'value'=>'"<span class=\'isDisplay ".$data->canModify()."\' ></span>"',
            'headerHtmlOptions'=>array('style'=>'display:none'),
            'htmlOptions' => array(
                'style'=>'display:none',
            )
        ),
        array(
            'name'=>'agent_id',
            'type'=>'raw',
//            'value'=>'$data->getAgentOfCustomer()',
            'value'=>'$data->getAgentWithLink()', //DuongNV add link Update Search Sep 20, 2018
            //function of tbl Users
        ),   
        array(
            'header' => 'Loại KH',
            'name' => 'is_maintain',
            'value'=>'$data->getTypeCustomerText()',
            'htmlOptions' => array('style' => 'width:50px;')
        ),
        array(
            'name'=>'first_name',
            'type'=>'raw',
            'value'=>'$data->getFirstName().$data->getLinkOrder()',
            //'value'=>'$data->getFirstName()."-".$data->getLinkOrder()',
        ),
        array(
            'name'=>'address',
            'type'=>'raw',
            'value'=>'$data->getAddress()',
        ),
        array(
            'name'=>'phone',
            'type'=>'raw',
            'value'=>'$data->getPhone()',
        ),
        array(
            'header'=>'ĐH mới nhất',
            'name'=>'last_purchase',
            'type'=>'raw',
            'value'=>'$data->getLastPurchase()',
        ),
        array(
            'header'=>'Ngày tạo KH',
            'name'=>'created_date',
            'type'=>'raw',
            'value'=>'$data->getCreatedBy()."<br>".$data->getCreatedDate()',
        ),
        array(
            'name'=>'note',
            'type'=>'raw',
            'value'=>'$data->getNewestNote()',
        ),    
        array(
            'header'=>'Nhân viên',
            'type'=>'raw',
            'value'=>'$data->getEmployee()',
        ),
        array(
            'header'=>'Hẹn gọi lại',
            'type'=>'raw',
            'value'=>'$data->getAppointmentDate()',
        ),
        array(
            'name'=>'Actions',
            'type'=>'raw',
            'value'=>'$data->getActionsForNote()',
            //'visible'=>$model->canCreateNote()
        ),
                
//            array(
//                'header' => 'Actions',
//                'class'=>'CButtonColumn',
//                'template'=> ControllerActionsName::createIndexButtonRoles($actions),
//                'buttons'=>array(
//
//                ),
//            ),
        ),
    )); ?>
  
    <?php $model->setSaleIdLogin(); ?>
    
    <div class="BoxApply row <?php echo $model->showSetSaleId();?>">
        <?php  echo $form->labelEx($model,'sale_id', ['label'=>'Nhân viên']); ?>
        <?php  echo $form->dropDownList($model,'sale_id', $model->getArrayEmployee()); ?>
    </div>
    
    <?php // if(1): ?>
    <?php // if(!isset($_GET['type']) || $model->showSetSaleId()): ?>
    <?php if($model->showSetSaleId() || $model->showButtonApply()): ?>
    <div class="row buttons" style="float: left; margin: 0 20px 0 0;">
        <?php $this->widget('bootstrap.widgets.TbButton', array(
        'buttonType'=>'submit',
        'label'=> 'Apply',
        'type'=>'null', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
        'size'=>'small', // null, 'large', 'small' or 'mini'
        //'htmlOptions' => array('style' => 'margin-bottom: 10px; float: right;'),
        )); ?>	
    </div>
    <?php endif; ?>
    
    <?php if($model->canDelete() || $cUid == GasConst::UID_VEN_NTB): ?>
        <div class="row buttons">
            <?php $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType'=>'button',
            'label'=> 'Hủy KH đã chọn',
            'type'=>'null', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
            'size'=>'small', // null, 'large', 'small' or 'mini'
            'htmlOptions' => array(
    //            'visible' => $model->canDelete(),
                'class'=>'enableRmSaleIdBtnXXX btn_cancel display_none'
            ),
            )); ?>	
        </div>
    <?php endif; ?>

    <?php $this->endWidget(); ?>
</div>

<script>
$(document).ready(function() {
    fnUpdateColorbox();
    $(".IframeCreateCustomer").colorbox({iframe:true,innerHeight:'1000', innerWidth: '700',close: "<span title='close'>close</span>"});
    
    <?php if($model->showSetSaleId()):?>
        $('.BoxApply').find('#Telesale_sale_id').prepend('<option id="delSaleIdOption" value="" selected><b>Xóa nhân viên Telesale hiện tại<b></option>');
    <?php endif;?>
    
});

function fnUpdateColorbox(){
    fixTargetBlank();
    $(".createNote").colorbox({
        iframe: true,
        overlayClose: false, escKey: true,
        innerHeight: '1000',
        innerWidth: '1100', close: "<span title='close'>close</span>",
        onClosed: function () { // update view when close colorbox
    //       location.reload();
        }
    });
    
    mark();
    $('.viewNote[style="display:block"]').closest('tr').css('background','#dcdcdc');
    $('.viewNote[style="display:block"]').closest('tr').find('input').attr('disabled',true);
}

</script>
<script>
    //click tr to check the checkbox
    $(document).ready(function() {
        $('#telesale-grid table.items tr').not('table.items thead tr').on('click',function(event) {
            if(event.target.className.indexOf('createNote') != -1){
                return;
            }
            if (event.target.type !== 'checkbox') {
                $(':checkbox', this).trigger('click');
            }
        });
    });
    
    $('body').on('click', '.TelesaleUpdateSolr', function(){
        $.blockUI({ overlayCSS: { backgroundColor: 'fff' } });
        var url_ = $(this).attr('next');
        $.ajax({
            url: url_,
            type: 'post',
            dataType: 'json',
            success: function(data){
                $.unblockUI();
            }
        });
    });
    
</script>
<script>
    function mark(){
        $('.isDisplay.visible').parents('tr').find('input').attr("data-display",'visible');
        $('.isDisplay.hidden').parents('tr').find('input').attr("data-display",'hidden');
        $('.enableRmSaleIdBtn').css({"background": "#0080FF",'color': 'white','border': 'none','padding': '5px 10px','border-radius': '5px','font-weight': 'bold','cursor': 'pointer'});
        //check all
        $("#Telesale_customer_all").click(function(){
            $('input:checkbox:enabled[name="Telesale[customer][]"]').not(this).prop('checked', this.checked);
        });
        //click tr to check
        $('#telesale-grid table.items tr').not('table.items thead tr').on('click',function(event) {
            if(event.target.className.indexOf('createNote') != -1){
                return;
            }
            if (event.target.type !== 'checkbox') {
                $(':checkbox', this).trigger('click');
            }
        });
    }
    //handle event when click enable Rm SaleId Btn
    $('.enableRmSaleIdBtn').on('click',function(){
            //enable
            if($('table.items').find('input[data-display="hidden"]').attr('disabled')){
                $('table.items').find('input[data-display="hidden"]').removeAttr('disabled');
                $(this).text("Disable Remove Sale Employee");
                $('#Telesale_sale_id').prepend('<option id="delSaleIdOption" value="" selected><b>Xóa nhân viên Telesale hiện tại<b></option>')
            }
            else{
            //disable
                if($('table.items').find('input[data-display="hidden"]').is(':checked')){
                    $('table.items').find('input[data-display="hidden"]').prop('checked', false);
                }
                $('table.items').find('input[data-display="hidden"]').attr('disabled',true);
                $(this).text("Enable Remove Sale Employee");
                $('#delSaleIdOption').remove();
            }
        });
    
    
</script>
