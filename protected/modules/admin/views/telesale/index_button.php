<div class="form">
    <?php
        $LinkNormal         = Yii::app()->createAbsoluteUrl('admin/telesale/index');
        $LinkCallAgain         = Yii::app()->createAbsoluteUrl('admin/telesale/index', array( 'type'=> $model->call_again   ));
        $LinkCalled         = Yii::app()->createAbsoluteUrl('admin/telesale/index', array( 'type'=> $model->called));
        $LinkNotCall        = Yii::app()->createAbsoluteUrl('admin/telesale/index', array( 'type'=> $model->not_call));
    ?>
    <h1><?php echo $this->adminPageTitle;?>
        <a class='btn_cancel f_size_14 <?php echo (!isset($_GET['type'])) ? "active":"";?>' href="<?php echo $LinkNormal;?>">Chưa nhận</a>
        <a class='btn_cancel f_size_14 <?php echo isset($_GET['type']) && $_GET['type']==$model->call_again ? "active":"";?>' href="<?php echo $LinkCallAgain;?>">Gọi lại hôm nay</a>
        <a class='btn_cancel f_size_14 <?php echo isset($_GET['type']) && $_GET['type']==$model->not_call ? "active":"";?>' href="<?php echo $LinkNotCall;?>">Chưa gọi</a>
        <a class='btn_cancel f_size_14 <?php echo isset($_GET['type']) && $_GET['type']==$model->called? "active":"";?>' href="<?php echo $LinkCalled;?>">Đã gọi</a>
    </h1> 
</div>