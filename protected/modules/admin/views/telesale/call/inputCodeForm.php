<?php 
$ARR_CUSTOMER = isset($rData['ARR_CUSTOMER']) ? $rData['ARR_CUSTOMER'] : [];
$index = 1; $totalCall = $totalSell = 0; $aCustomer = [];
$aSellNotCustomerApp = $rData['ARR_SELL'];
$mAppCache = new AppCache();
$aAgent = $mAppCache->getAgentListdata();
?>
<table class="tb hm_table f_size_15 materials_table">
    <thead>
        <tr>
            <th class="item_b">#</th>
            <th class="item_b item_c">Loại KH</th>
            <th class="item_b item_c">Đại lý</th>
            <th class="item_b item_c">Khách hàng</th>
            <th class="item_b item_c">Điện thoại</th>
            <th class="item_b item_c">Ngày tạo</th>
            <th class="item_b item_c">Cuộc gọi</th>
            <th class="item_b item_c">Bán hàng</th>
            <th class="item_b item_c">Ngày bán hàng</th>
        </tr>
    </thead>
    <tbody>
    <?php foreach ($ARR_CUSTOMER as $k => $mUser): ?>
        <?php 
            if($model->is_buy_complete && !isset($rData['ARR_SELL'][$mUser->id]) ){
                continue;
            }
        ?>
        <?php include 'inputCodeTr.php'; ?>
    <?php endforeach; ?>
    <?php 
//        if(count($aSellNotCustomerApp)){
//            $aUserId = array_keys($aSellNotCustomerApp);
//            $aCustomer = Users::getArrObjectUserByRole(ROLE_CUSTOMER, $aUserId);
//        }
    ?>
    <?php foreach ($aCustomer as $k => $mUser): ?>
        <?php include 'inputCodeTr.php'; ?>
    <?php endforeach; ?>    
        
    </tbody>
    <tfoot>
        <tr>
            <td class="item_r item_b" colspan="6">Sum</td>
            <td class="item_c item_b"><?php echo $totalCall;?></td>
            <td class="item_c item_b"><?php echo $totalSell;?></td>
            <td></td><td></td>
        </tr>
    </tfoot>
</table>