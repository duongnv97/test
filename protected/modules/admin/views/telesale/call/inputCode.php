<?php
$this->breadcrumbs=array(
    $this->pageTitle,
);
$menus=array(
//            array('label'=> 'Xuất Excel Danh Sách Hiện Tại',
//               'url'=>array('ExportExcelReportGasBack'), 
//               'htmlOptions'=>array('class'=>'export_excel ','label'=>'Xuất Excel')),
);
$this->menu= ControllerActionsName::createMenusRoles($menus, $actions);
?>

<h1><?php echo $this->pageTitle;?></h1>
<div class="search-form" style="">
<?php $this->renderPartial('call/inputCodeSearch',array(
        'model'=>$model,
)); ?>
</div><!-- search-form -->

<style>
     .ui-datepicker-trigger { float: left;} 
</style>
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/jquery.floatThead.min.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/underscore-min.js"></script>    
<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/print-store-card.css" />

<script>
$(document).ready(function(){
    $('.form').find('button:submit').click(function(){        
        $.blockUI({ overlayCSS: { backgroundColor: '#fff' } }); 
    });
 
    $('.materials_table').find('tbody').prepend($('.materials_table').find('tfoot').html());
    $('.hm_table').floatThead();
});
</script>
<?php if(isset($rData['ARR_CUSTOMER_PHONE']) && count($rData['ARR_CUSTOMER_PHONE'])): ?>
    <?php include 'inputCodeForm.php'; ?>
<?php endif; ?>



