<?php 
    $hasCall = $hasSell = '';$dateSell = '';
    
    $agentName = isset($aAgent[$mUser->area_code_id]) ? $aAgent[$mUser->area_code_id] : '';
    
    
    if(isset($rData['ARR_SELL'][$mUser->id])){
        $hasSell = 'Có';
        $totalSell++;
        unset($aSellNotCustomerApp[$mUser->id]);
        $dateSell = MyFormat::dateConverYmdToDmy($rData['ARR_SELL'][$mUser->id]->created_date, 'd/m/Y H:i');
        $agentName = isset($aAgent[$rData['ARR_SELL'][$mUser->id]->agent_id]) ? $aAgent[$rData['ARR_SELL'][$mUser->id]->agent_id] : '';
    }
    
    $tempPhone = explode('-', $mUser->phone);
    foreach($tempPhone as $phone){
        if(isset($rData['ARR_CALL_RECORD'][$phone*1])){
            $hasCall = 'Có';
            $totalCall++;
        }
    }

?>
<tr>
    <td class="item_c"><?php echo $index++;?></td>
    <td class="item_c"><?php echo $mUser->getTypeCustomerText();?></td>
    <td class=""><?php echo $agentName?></td>
    <td class=""><?php echo $mUser->first_name."<br> $mUser->address";?></td>
    <td class="item_c"><?php echo $mUser->getPhoneShow();?></td>
    <td class="item_c"><?php echo MyFormat::dateConverYmdToDmy($mUser->created_date, 'd/m/Y H:i');?></td>
    <td class="item_c"><?php echo $hasCall;?></td>
    <td class="item_c"><?php echo $hasSell;?></td>
    <td class="item_c"><?php echo $dateSell;?></td>
</tr>