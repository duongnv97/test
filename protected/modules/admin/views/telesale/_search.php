
<div class="wide form">

    <?php $form=$this->beginWidget('CActiveForm', array(
            'action'=> GasCheck::getCurl(),
            'method'=>'get',
    )); ?>

    <div class="row">
        <?php echo $form->labelEx($model,'agent_id'); ?>
        <?php echo $form->hiddenField($model,'agent_id'); ?>
        <?php
            // 1. limit search kh của sale
            $url = Yii::app()->createAbsoluteUrl('admin/ajax/search_by_role', array('role'=> ROLE_AGENT));
            // widget auto complete search user customer and supplier
            $aData = array(
                'model'=>$model,
                'field_customer_id'=>'agent_id',
                'url'=> $url,
                'name_relation_user'=>'rAgent',
                'ClassAdd' => 'w-400',
                'field_autocomplete_name' => 'autocomplete_agent',
                'placeholder'=>'Nhập mã hoặc tên đại lý',
            );
            $this->widget('ext.SpaAutocompleteCustomer.SpaAutocompleteCustomer',
                array('data'=>$aData));
            ?>
        <?php echo $form->error($model,'agent_id'); ?>
    </div>
    
    <?php if(isset($_GET['type'])){ ?>
    <div class="row more_col">
        <div class="col1">
            <?php echo $form->labelEx($model, 'date_call_from', []); ?>
             <?php 
                $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                    'model'=>$model,        
                    'attribute'=>'date_call_from',
                    'options'=>array(
                        'showAnim'=>'fold',
                        'dateFormat'=> MyFormat::$dateFormatSearch,
                //                            'minDate'=> '0',
                //                            'maxDate'=> '0',
                        'changeMonth' => true,
                        'changeYear' => true,
                        'showOn' => 'button',
                        'buttonImage'=> Yii::app()->theme->baseUrl.'/admin/images/icon_calendar_r.gif',
                        'buttonImageOnly'=> true,                                
                    ),        
                    'htmlOptions'=>array(
                        'class'=>'w-16',
                        'style'=>'height:20px;',
//                            'readonly'=>'readonly',
                    ),
                ));
                ?>  
            <?php echo $form->error($model,'date_call_from'); ?>
        </div>
        <div class="col1">
            <?php echo $form->labelEx($model, 'date_call_to', []); ?>
             <?php 
                $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                    'model'=>$model,        
                    'attribute'=>'date_call_to',
                    'options'=>array(
                        'showAnim'=>'fold',
                        'dateFormat'=> MyFormat::$dateFormatSearch,
                //                            'minDate'=> '0',
                //                            'maxDate'=> '0',
                        'changeMonth' => true,
                        'changeYear' => true,
                        'showOn' => 'button',
                        'buttonImage'=> Yii::app()->theme->baseUrl.'/admin/images/icon_calendar_r.gif',
                        'buttonImageOnly'=> true,                                
                    ),        
                    'htmlOptions'=>array(
                        'class'=>'w-16',
                        'style'=>'height:20px;',
//                            'readonly'=>'readonly',
                    ),
                ));
                ?>  
            <?php echo $form->error($model,'date_call_to'); ?>
        </div>
    </div>
    <?php } ?>
    
    <div class="row more_col">
        <div class="col1">
            <?php  echo $form->labelEx($model,'sale_id', ['label'=>'Nhân viên']); ?>
            <?php  echo $form->dropDownList($model,'sale_id', $model->getArrayEmployee(), ['empty'=>'Select','class'=>'w-200']); ?>
        </div>
        <div class="col2">
            <?php echo $form->labelEx($model,'status_customer',['label'=>'Trạng thái']); ?>
            <?php echo $form->dropdownList($model,'status_customer', $model->getArrayCallStatus(true), array('empty'=>'Select','class'=>'w-200')); 
            ?>
        </div>
        <div class="col3">
            <?php echo $form->labelEx($model,'Trạng thái đơn hàng'); ?>
            <?php echo $form->dropDownList($model,'is_buy_complete', $model->getArrayStatusBuy(),array('empty'=>'Select','class'=>'w-200')); ?>
        </div>
    </div>
    
    <div class="row more_col">
        <div class="col1">
            <?php echo $form->labelEx($model,'province_id'); ?>
            <?php echo $form->dropDownList($model,'province_id', GasProvince::getArrAll(),array('style'=>'','class'=>'w-200','empty'=>'Select')); ?>
        </div> 	

        <div class="col2">
            <?php echo $form->labelEx($model,'district_id'); ?>
            <?php echo $form->dropDownList($model,'district_id', GasDistrict::getArrAll($model->province_id),array('class'=>'w-200', 'empty'=>"Select")); ?>
        </div>
        <div class="col3">
            <?php echo $form->label($model,'is_maintain', array('label'=>'Loại KH')); ?>
            <?php echo $form->dropDownList($model,'is_maintain', CmsFormatter::$CUSTOMER_BO_MOI,array('empty'=>'Select', 'class' => 'w-200')); ?>
            <?php echo $form->error($model,'is_maintain'); ?>
        </div>
    </div>
    
    <div class="row more_col">
        <div class="col1">
            <?php echo $form->labelEx($model, 'date_created_from', ['label'=>'Ngày tạo KH từ']); ?>
             <?php 
                $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                    'model'=>$model,        
                    'attribute'=>'date_created_from',
                    'options'=>array(
                        'showAnim'=>'fold',
                        'dateFormat'=> MyFormat::$dateFormatSearch,
                //                            'minDate'=> '0',
                //                            'maxDate'=> '0',
                        'changeMonth' => true,
                        'changeYear' => true,
                        'showOn' => 'button',
                        'buttonImage'=> Yii::app()->theme->baseUrl.'/admin/images/icon_calendar_r.gif',
                        'buttonImageOnly'=> true,                                
                    ),        
                    'htmlOptions'=>array(
                        'class'=>'w-16',
                        'style'=>'height:20px;',
//                            'readonly'=>'readonly',
                    ),
                ));
                ?>  
            <?php echo $form->error($model,'date_created_from'); ?>
        </div>
        <div class="col1">
            <?php echo $form->labelEx($model, 'date_created_to', ['label'=>'Đến']); ?>
             <?php 
                $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                    'model'=>$model,        
                    'attribute'=>'date_created_to',
                    'options'=>array(
                        'showAnim'=>'fold',
                        'dateFormat'=> MyFormat::$dateFormatSearch,
                //                            'minDate'=> '0',
                //                            'maxDate'=> '0',
                        'changeMonth' => true,
                        'changeYear' => true,
                        'showOn' => 'button',
                        'buttonImage'=> Yii::app()->theme->baseUrl.'/admin/images/icon_calendar_r.gif',
                        'buttonImageOnly'=> true,                                
                    ),        
                    'htmlOptions'=>array(
                        'class'=>'w-16',
                        'style'=>'height:20px;',
//                            'readonly'=>'readonly',
                    ),
                ));
                ?>  
            <?php echo $form->error($model,'date_created_to'); ?>
        </div>
    </div>
    <div class="row more_col">
        <?php  echo $form->labelEx($model,'count_sell', ['label'=>'Số hóa đơn đã mua']); ?>
        <?php  echo $form->dropDownList($model,'count_sell', $model->getArrayCountSell(), ['empty'=>'Select','class'=>'w-200']); ?>
    </div>
    <div class="row more_col">
        <div class="col1">
            <?php echo $form->labelEx($model, 'last_purchase_from', ['label'=>'ĐH mới nhất từ']); ?>
             <?php 
                $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                    'model'=>$model,        
                    'attribute'=>'last_purchase_from',
                    'options'=>array(
                        'showAnim'=>'fold',
                        'dateFormat'=> MyFormat::$dateFormatSearch,
                //                            'minDate'=> '0',
                //                            'maxDate'=> '0',
                        'changeMonth' => true,
                        'changeYear' => true,
                        'showOn' => 'button',
                        'buttonImage'=> Yii::app()->theme->baseUrl.'/admin/images/icon_calendar_r.gif',
                        'buttonImageOnly'=> true,                                
                    ),        
                    'htmlOptions'=>array(
                        'class'=>'w-16',
                        'style'=>'height:20px;',
//                            'readonly'=>'readonly',
                    ),
                ));
                ?>  
            <?php echo $form->error($model,'last_purchase_from'); ?>
        </div>
        <div class="col1">
            <?php echo $form->labelEx($model, 'last_purchase_to', ['label'=>'Đến']); ?>
             <?php 
                $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                    'model'=>$model,        
                    'attribute'=>'last_purchase_to',
                    'options'=>array(
                        'showAnim'=>'fold',
                        'dateFormat'=> MyFormat::$dateFormatSearch,
                //                            'minDate'=> '0',
                //                            'maxDate'=> '0',
                        'changeMonth' => true,
                        'changeYear' => true,
                        'showOn' => 'button',
                        'buttonImage'=> Yii::app()->theme->baseUrl.'/admin/images/icon_calendar_r.gif',
                        'buttonImageOnly'=> true,                                
                    ),        
                    'htmlOptions'=>array(
                        'class'=>'w-16',
                        'style'=>'height:20px;',
//                            'readonly'=>'readonly',
                    ),
                ));
                ?>  
            <?php echo $form->error($model,'last_purchase_to'); ?>
        </div>
    </div>
    
    <div class="row more_col">
        <div class="col1">
            <?php echo $form->labelEx($model, 'callFrom', []); ?>
             <?php 
                $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                    'model'=>$model,        
                    'attribute'=>'callFrom',
                    'options'=>array(
                        'showAnim'=>'fold',
                        'dateFormat'=> MyFormat::$dateFormatSearch,
                //                            'minDate'=> '0',
                //                            'maxDate'=> '0',
                        'changeMonth' => true,
                        'changeYear' => true,
                        'showOn' => 'button',
                        'buttonImage'=> Yii::app()->theme->baseUrl.'/admin/images/icon_calendar_r.gif',
                        'buttonImageOnly'=> true,                                
                    ),        
                    'htmlOptions'=>array(
                        'class'=>'w-16',
                        'style'=>'height:20px;',
//                            'readonly'=>'readonly',
                    ),
                ));
                ?>  
            <?php echo $form->error($model,'callFrom'); ?>
        </div>
        <div class="col1">
            <?php echo $form->labelEx($model, 'callTo', ['label'=>'Đến']); ?>
             <?php 
                $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                    'model'=>$model,        
                    'attribute'=>'callTo',
                    'options'=>array(
                        'showAnim'=>'fold',
                        'dateFormat'=> MyFormat::$dateFormatSearch,
                //                            'minDate'=> '0',
                //                            'maxDate'=> '0',
                        'changeMonth' => true,
                        'changeYear' => true,
                        'showOn' => 'button',
                        'buttonImage'=> Yii::app()->theme->baseUrl.'/admin/images/icon_calendar_r.gif',
                        'buttonImageOnly'=> true,                                
                    ),        
                    'htmlOptions'=>array(
                        'class'=>'w-16',
                        'style'=>'height:20px;',
//                            'readonly'=>'readonly',
                    ),
                ));
                ?>  
            <?php echo $form->error($model,'callTo'); ?>
        </div>
    </div>

    <div class="row more_col">
        <div class="col1">
            <?php echo $form->labelEx($model,'status_customer_cancel',['label'=>'Lý do từ chối']); ?>
            <?php echo $form->dropdownList($model,'status_customer_cancel', $model->getArrayStatusCancel(), array('empty'=>'Select','class'=>'w-200')); 
            ?>
        </div>
        <div class="col2">
            <?php echo $form->labelEx($model,'fromPttt',array('class'=>'checkbox_one_label', 'style'=>'padding-top:3px;')); ?>
            <?php echo $form->checkBox($model,'fromPttt',array('class'=>'float_l')); ?>
        </div>
        <div class="col3">
            <?php $aPageSize = $model->getArrayPageSize(); 
                unset($aPageSize[250]);
                unset($aPageSize[150]);
            ?>
            <?php echo $form->labelEx($model,'Số dòng hiển thị'); ?>
            <?php echo $form->dropDownList($model,'pageSize', $aPageSize,array('class'=>'w-200')); ?>
        </div>
    </div>
    
    <div class="row">
    <?php echo $form->labelEx($model,'customer_id'); ?>
    <?php echo $form->hiddenField($model,'customer_id', array('class'=>'')); ?>
    <?php
            // 1. limit search kh của sale
            $url = Yii::app()->createAbsoluteUrl('admin/ajax/searchHgd');
            // widget auto complete search user customer and supplier
            $aData = array(
                'model'=>$model,
                'field_customer_id'=>'customer_id',
                'url'=> $url,
                'name_relation_user'=>'rCustomer',
                'ClassAdd' => 'w-400',
//                'fnSelectCustomer'=>'fnSelectCustomer',
            );
            $this->widget('ext.SpaAutocompleteCustomer.SpaAutocompleteCustomer',
                array('data'=>$aData));
            ?>
        <?php echo $form->error($model,'customer_id'); ?>
    </div>
    
    <div class="row more_col">
        <div class="col1">
            <?php  echo $form->labelEx($model,'storehouse_id', ['label'=>'Mã KM']); ?>
            <?php //  echo $form->dropDownList($model,'storehouse_id', $model->getArrayPttt(), ['empty'=>'Select','class'=>'w-200']); ?>
            <?php echo $form->textField($model,'storehouse_id', array('class'=>'w-200', 'placeholder' => 'vd: 1001')); ?>
        </div>
        <div class="col2">
            <?php  echo $form->labelEx($model,'created_by', ['label'=>'Người tạo KH']); ?>
            <?php  echo $form->dropDownList($model,'created_by', $model->getArrayEmployee(), ['empty'=>'Select','class'=>'w-200']); ?>
        </div>
        <div class="col3">
        </div>
    </div>
    

    <div class="row buttons" style="padding-left: 159px;">
        <?php $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType'=>'submit',
            'label'=>'Search',
            'type'=>'null', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
            'size'=>'small', // null, 'large', 'small' or 'mini'
            //'htmlOptions' => array('style' => 'margin-bottom: 10px; float: right;'),
        )); ?>	
    </div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->


<script>
    $(document).ready(function(){
        fnBindChangeProvince();
    });
    
    /**
    * @Author: ANH DUNG Apr 20, 2016
    */
    function fnBindChangeProvince(){
        $('body').on('change', '#Telesale_province_id', function(){
            var province_id = $(this).val();        
            var url_ = "<?php echo Yii::app()->createAbsoluteUrl('admin/ajax/get_slt_district');?>";
            $.blockUI({ overlayCSS: { backgroundColor: '<?php echo BLOCK_UI_COLOR;?>' } }); 
            $.ajax({
                url: url_,
                data: {ajax:1,province_id:province_id}, 
                type: "get",
                dataType:'json',
                success: function(data){
                    $('#Telesale_district_id').html(data['html_district']);                
                    $.unblockUI();
                }
            });
        });

//        $('body').on('change', '#Users_district_id', function(){
//            var province_id = $('#Users_province_id').val();   
//            var district_id = $(this).val();        
//            var url_ = "<?php echo Yii::app()->createAbsoluteUrl('admin/ajax/get_slt_ward');?>";
//            $.blockUI({ overlayCSS: { backgroundColor: '<?php echo BLOCK_UI_COLOR;?>' } }); 
//            $.ajax({
//                url: url_,
//                data: {ajax:1,district_id:district_id,province_id:province_id},
//                type: "get",
//                dataType:'json',
//                success: function(data){
//                    $('#Users_ward_id').html(data['html_district']);                
//                    $.unblockUI();
//                }
//            });
//        });
    }
    //if check "KH tu choi" will display dropdownList status cancel
    $('select[name="Telesale[status_customer]"]').change(
        function(){
            if ($(this).val() == '3') {
                $('.stt_cancelDL').show();
                $('#Telesale_status_cancel option:first-child').attr("selected", "selected");
            } else {
                $('.stt_cancelDL').hide();
            }
        }
    );
</script>