<?php
$this->breadcrumbs=array(
    $this->pageTitle,
);
$menus=array(
//            array('label'=> 'Xuất Excel Danh Sách Hiện Tại',
//               'url'=>array('ExportExcelReportGasBack'), 
//               'htmlOptions'=>array('class'=>'export_excel ','label'=>'Xuất Excel')),
);
$this->menu= ControllerActionsName::createMenusRoles($menus, $actions);
?>

<h1><?php echo $this->pageTitle;?></h1>
<div class="search-form" style="">
<?php $this->renderPartial('report/_search',array(
        'model'=>$model,
)); ?>
</div><!-- search-form -->

<style>
    .ui-datepicker-trigger { float: left;}
</style>

<script>
$(document).ready(function(){
    $('.form').find('button:submit').click(function(){        
        $.blockUI({ overlayCSS: { backgroundColor: '#fff' } }); 
    });   
 
});
</script>

<?php
$gridCss = Yii::app()->getAssetManager()->publish(Yii::getPathOfAlias('zii.widgets.assets')) . '/gridview/styles.css';
Yii::app()->getClientScript()->registerCssFile($gridCss);
$index = 1;
// Get table height
$freeHeightTable = isset($data['employee']) ? (sizeof($data['employee']) + 2) * 19 : 400;
$freeHeightTable = $freeHeightTable > 400 ? 400 : $freeHeightTable;

// For piechart all total call
$totalDataChart[] = ['Loại hủy', 'SL'];
?>
<script type="text/javascript"
        src="<?php echo Yii::app()->theme->baseUrl; ?>/js/scroll/jquery.freezetablecolumns.1.1.js"></script>
<div class="grid-view display_none">
    <table id="freezetablecolumns_report_call" class="items hm_table freezetablecolumns">
        <thead>
        <tr  class="h_50">
            <th class="w-20 ">#</th>
            <th class="w-150 ">Nhân viên</th>
            <th class="w-100 ">Tổng</th>
            <?php foreach ($data['status'] as $status): ?>
                <th class="w-80 item_b"  style="white-space:normal;" ><?php echo $status; ?></th>
            <?php endforeach; ?>
        </tr>
        </thead>
        <tbody>
            <tr class="h_20">
                <td class="item_b item_c" colspan="2">Tổng</td>
                <?php
                $valueSum          = isset($data['sum']) ? array_sum($data['sum']) : '0';
                $textSum = $valueSum;
                
                if ($textSum == '0'):
                    $textSum = '';
                endif;
                ?>
                <td class="item_c item_b"><?php echo $textSum; ?></td>
            <?php foreach ($data['status'] as $key => $status): ?>
                <?php
                $valueTotal          = isset($data['sum'][$key]) ? $data['sum'][$key] : '0';
                $text = $valueTotal;
                $totalDataChart[] = [$status, (int)$valueTotal];
                if ($text == '0'):
                    $text = '';
                endif;
                ?>
                <td class="item_c item_b"><?php echo $text; ?></td>
            <?php endforeach; ?>
            </tr>
            
        <?php 
        if(isset($data['employee']))
        foreach ($data['employee'] as $user_id => $userName): 
            ?>
            <tr class="h_20">
                <td class="item_c"><?php echo $index++; ?></td>
                <td class="item_b"><?php echo $userName; ?></td>
                <?php
                    $nRef = isset($data['View'][$user_id]['Sum']) ? $data['View'][$user_id]['Sum'] : 0;
                ?>
                <td class="item_c item_b"><?php echo $nRef; ?></td>
                <?php foreach ($data['status'] as $key => $status): ?>
                <?php
                $valueEmployee         = isset($data['View'][$user_id]['Detail'][$key]) ? $data['View'][$user_id]['Detail'][$key] : '0';
                $textEmployee = $valueEmployee;
                if ($textEmployee == '0'):
                    $textEmployee = '';
                endif;
                ?>
                <td class="item_c"><?php echo $textEmployee; ?></td>
            <?php endforeach; ?>
            </tr>
        <?php endforeach; ?>
        </tbody>
    </table>
</div>


<script type="text/javascript">
    fnAddClassOddEven('items');
    $('.freezetablecolumns').each(function () {
        var id_table = $(this).attr('id');
        $('#' + id_table).freezeTableColumns({
            width:       <?php echo Users::WIDTH_freezetable;?>,   // required - bt 1100
            height:      <?php echo $freeHeightTable;?>,   // required
            numFrozen: 3,     // optional
            frozenWidth: 315,   // optional
            clearWidths: true  // optional
        });
    });
    $(window).load(function () {
        var index = 1;
        $('.freezetablecolumns').each(function () {
            if (index == 1)
                $(this).closest('div.grid-view').show();
            index++;
        });
        fnAddClassOddEven('items');
    });
</script>
<?php include('chartCancel.php'); ?>
