<?php 
$EMPLOYEE       =  isset($aData['EMPLOYEE']) ? $aData['EMPLOYEE'] : [];
$OUT_PUT        =  isset($aData['OUT_PUT']) ? $aData['OUT_PUT'] : [];
$AGENT          =  isset($aData['AGENT']) ? $aData['AGENT'] : [];
$SUM            =  isset($aData['SUM']) ? $aData['SUM'] : [];
?>

<?php
$this->breadcrumbs=array(
    $this->pageTitle,
);
$menus=array(
);
$this->menu= ControllerActionsName::createMenusRoles($menus, $actions);
?>

<h1><?php echo $this->pageTitle;?></h1>
<div class="search-form" style="">
<?php $this->renderPartial('report/reportAgent/_search',array(
        'model'=>$model,
)); ?>
</div><!-- search-form -->

<script>
$(document).ready(function(){
    $('.form').find('button:submit').click(function(){        
        $.blockUI({ overlayCSS: { backgroundColor: '#fff' } }); 
    });   
 
});
</script>
<?php
$gridCss = Yii::app()->getAssetManager()->publish(Yii::getPathOfAlias('zii.widgets.assets')) . '/gridview/styles.css';
Yii::app()->getClientScript()->registerCssFile($gridCss);
$index = 1;
?>
<script type="text/javascript"
        src="<?php echo Yii::app()->theme->baseUrl; ?>/js/scroll/jquery.freezetablecolumns.1.1.js"></script>
<?php if(!empty($OUT_PUT)): ?>
    <div class="grid-view display_none">
        <table id="freezetablecolumns_report_call" class="items hm_table freezetablecolumns">
            <thead>
            <tr  class="h_50">
                <th class="w-20 item_b">#</th>
                <th class="w-150 item_b">Đại lý / Nhân viên</th>
                <th class="w-100 item_b">Tổng</th>
                <?php foreach ($EMPLOYEE as $employee_id => $nameEmployee): ?>
                    <th class="w-150 item_b"><?php echo $nameEmployee; ?></th>
                <?php endforeach; ?>
            </tr>
            </thead>
            <tbody>
                <tr  class="h_50">
                    <td class="item_b" colspan="2"></td>
                    <td class="item_c item_b"><?php echo array_sum($SUM); ?></td>
                    <?php foreach ($EMPLOYEE as $employee_id => $nameEmployee): ?>
                    <td class="item_c item_b"><?php echo isset($SUM[$employee_id]) ? $SUM[$employee_id] : 0; ?></td>
                    <?php endforeach; ?>
                </tr>
                <?php foreach ($OUT_PUT as $agent_id => $aReport): ?>
                <tr>
                    <td class="item_c"><?php echo $index++;?></td>
                    <td class="item_b item_l"><?php echo isset($AGENT[$agent_id]) ? $AGENT[$agent_id]['first_name'] : $agent_id;?></td>
                    <td class="item_b item_c"><?php echo isset($OUT_PUT[$agent_id]) && array_sum($OUT_PUT[$agent_id]) > 0 ? array_sum($OUT_PUT[$agent_id]) : '';?></td>
                    <?php foreach ($EMPLOYEE as $employee_id => $nameEmployee): ?>
                    <td class="item_c"><?php echo isset($OUT_PUT[$agent_id][$employee_id]) ? $OUT_PUT[$agent_id][$employee_id] : ''; ?></td>
                    <?php endforeach; ?>
                </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
<script>
    $('#freezetablecolumns_report_call').freezeTableColumns({
        width:       <?php echo Users::WIDTH_freezetable;?>,   // required - bt 1100
        height:      400,   // required
        numFrozen: 3,     // optional
        frozenWidth: 330,   // optional
        clearWidths: true  // optional
    });
    $(window).load(function () {
        var index = 1;
        $('.freezetablecolumns').each(function () {
            if (index == 1)
                $(this).closest('div.grid-view').show();
            index++;
        });
        fnAddClassOddEven('items');
    });
</script>
<?php endif; ?>
