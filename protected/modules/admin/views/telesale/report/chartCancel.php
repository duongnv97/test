<?php
/** @Author: HOANG NAM 02/04/2018
 *  @Todo: chart
 **/

if (isset($totalDataChart)) {
    /** @var array $totalDataChart list value for lines */
    $jsArrayTotalCall = json_encode($totalDataChart);
    ?>
    <div class="float_l" style="width: 1100px">
        <div id="success_chart" style="width: 1100px; height: 500px"></div>
    </div>
    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script type="text/javascript">
        //        fnAddClassOddEven('items');
        google.charts.load("current", {packages: ["corechart"]});
        google.charts.setOnLoadCallback(drawChart);
        function drawChart() {
            // Making a 3D Pie Chart https://developers.google.com/chart/interactive/docs/gallery/piechart?hl=vi
            var data = google.visualization.arrayToDataTable(<?php echo $jsArrayTotalCall; ?>);

            var options = {
                title: 'KH từ chối',
                is3D: true,
            };
            var chart = new google.visualization.PieChart(document.getElementById('success_chart'));
            chart.draw(data, options);

        }
    </script>
<?php } ?>