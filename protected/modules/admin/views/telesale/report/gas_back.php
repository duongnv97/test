<?php
$this->breadcrumbs=array(
    $this->pageTitle,
);
$menus=array(
            array('label'=> 'Xuất Excel Danh Sách Hiện Tại',
               'url'=>array('ExportExcelReportGasBack'), 
               'htmlOptions'=>array('class'=>'export_excel ','label'=>'Xuất Excel')),
);
$this->menu= ControllerActionsName::createMenusRoles($menus, $actions);
?>

<h1><?php echo $this->pageTitle;?></h1>
<div class="search-form" style="">
<?php $this->renderPartial('report/_search',array(
        'model'=>$model,
)); ?>
</div><!-- search-form -->

<style>
    .ui-datepicker-trigger { float: left;}
</style>

<script>
$(document).ready(function(){
    $('.form').find('button:submit').click(function(){        
        $.blockUI({ overlayCSS: { backgroundColor: '#fff' } }); 
    });   
 
});
</script>
<p>
    CHC: Mã code có gọi ra(Code have call) <br>
    AC: Tất cả code của telesale (All telesale code)
</p>
<p>
    ACO: Bình QV app có gọi ra (App call out)<br>
    AA: Tất cả BQV App của telesale (All telesale BQV app)
</p>
<?php if(isset($data['SUM_REVENUE_ALL'])): ?>

<?php
$gridCss = Yii::app()->getAssetManager()->publish(Yii::getPathOfAlias('zii.widgets.assets')) . '/gridview/styles.css';
Yii::app()->getClientScript()->registerCssFile($gridCss);
$LIST_EMPLOYEE      = $data['EMPLOYEE'];
$DATE_ARRAY         = $data['ARRAY_DATE'];
$OUT_FOLLOW         = $data['OUT_FOLLOW'];
$OUT_SELL           = $data['OUT_SELL'];
$OUT_FOLLOW_CHART   = $data['OUT_FOLLOW_CHART'];
$OUT_SELL_CHART     = $data['OUT_SELL_CHART'];
$SUM_OUT_FOLLOW     = $data['SUM_OUT_FOLLOW'];
$SUM_OUT_SELL       = $data['SUM_OUT_SELL'];
$OUT_TRACKING       = $data['OUT_TRACKING'];
$OUT_TRACKING_DATE  = $data['OUT_TRACKING_DATE'];
$SUM_OUT_TRACKING   = $data['SUM_OUT_TRACKING'];
$SUM_REVENUE        = $data['SUM_REVENUE'];
$OUT_SELL_APP_SUM   = $data['OUT_SELL_APP_SUM'];
$OUT_SELL_APP_ALL   = $data['OUT_SELL_APP_ALL'];
$OUT_SELL_FOLLOW        = !empty($data['OUT_SELL_FOLLOW']) ? $data['OUT_SELL_FOLLOW'] : [];
$APP_WITHOUT_CALL_SUM   = !empty($data['APP_WITHOUT_CALL_SUM']) ? $data['APP_WITHOUT_CALL_SUM'] : [];
$TELESALE_CODE_CALLOUT  = !empty($data['TELESALE_CODE_CALLOUT']) ? $data['TELESALE_CODE_CALLOUT'] : [];

$SUM_REVENUE_HAVE_CALLOUT       = isset($data['SUM_REVENUE_HAVE_CALLOUT']) ? $data['SUM_REVENUE_HAVE_CALLOUT'] : [];
$SUM_ALL_REVENUE_HAVE_CALLOUT   = $data['SUM_ALL_REVENUE_HAVE_CALLOUT'];
$SUM_REVENUE_ALL                = $data['SUM_REVENUE_ALL'];


$index = 1;
// Get table height
$freeHeightTable    = (sizeof($LIST_EMPLOYEE) + 1) * 19;
$freeHeightTable    = $freeHeightTable > 400 ? 400 : $freeHeightTable;

// For piechart all total call
$totalDataChartTracking[] = ['Nhân viên', 'KH nhập mã'];
$totalDataChart[] = ['Nhân viên', 'Cuộc gọi'];
$totalDataChartSell[] = ['Nhân viên', 'Bán hàng'];
$mAppPromotion = new AppPromotion();

$mAppCache = new AppCache();
$aTelesale = $mAppCache->getArrayIdRole(ROLE_TELESALE);

?>
<script type="text/javascript"
        src="<?php echo Yii::app()->theme->baseUrl; ?>/js/scroll/jquery.freezetablecolumns.1.1.js"></script>
<div class="grid-view display_none">
    <table id="freezetablecolumns_report_call" class="items hm_table freezetablecolumns">
        <thead>
        <tr class="h_20">
            <th class="w-20 ">#</th>
            <th class="w-120 ">Nhân viên</th>
            <th class="w-70 ">DT Call</th>
            <th class="w-70 ">DT No Call</th>
            <th class="w-60 ">CHC/AC</th>
            <th class="w-60 ">ACO/AA</th>
            <th class="w-60 ">Call/NoCall</th>
            <th class="w-60 ">BQV Call/App</th>
            <th class="w-100 ">CG/Code/BQV</th>
            <?php foreach ($DATE_ARRAY as $days): ?>
                <th class="item_b w-60"><?php echo substr($days, -2); ?></th>
            <?php endforeach; ?>
        </tr>
        </thead>
        <tbody>
            <?php
        //@Code: NAM013
        ?>
            <tr class="h_20">
                <td></td>
                <td class="item_b item_c">Tổng</td>
                <?php
                $valueSum               = isset($SUM_OUT_FOLLOW['SUM']) ? $SUM_OUT_FOLLOW['SUM'] : '0';
                $trackingValueSum       = isset($SUM_OUT_TRACKING['SUM']) ? $SUM_OUT_TRACKING['SUM'] : '0';
                $saleValueSum           = isset($SUM_OUT_SELL['SUM']) ? $SUM_OUT_SELL['SUM'] : '0';
                $countCus               = isset($OUT_SELL_FOLLOW) ? array_sum($OUT_SELL_FOLLOW) : '0';
                $appWithoutCall         = isset($APP_WITHOUT_CALL_SUM) ? array_sum($APP_WITHOUT_CALL_SUM) : '0';
                $codeHaveCall           = isset($TELESALE_CODE_CALLOUT) ? array_sum($TELESALE_CODE_CALLOUT) : '0';
                
                $textSum                = $valueSum.'/'.$trackingValueSum.'/'.$saleValueSum;
                $textCallNoCall         = $countCus.'/'.($saleValueSum-$countCus); // Call/NoCall
                $textSumApp             = ($saleValueSum - $OUT_SELL_APP_ALL). '/' . $OUT_SELL_APP_ALL;
                $textAppHaveCall        = ($OUT_SELL_APP_ALL - $appWithoutCall). '/' . $OUT_SELL_APP_ALL;// ACO/AA
                $textCodeHaveCall       = $codeHaveCall. '/' . $trackingValueSum;// CHC/AC
                if ($textSum == '0/0/0'):
                    $textSum = '';
                endif;
                $nSumRevenueNotCall     = $SUM_REVENUE_ALL - $SUM_ALL_REVENUE_HAVE_CALLOUT;
                
                ?>
                <td class="item_r item_b"><?php // Sep2018 tạm close lại vì sai khi check fromPttt  echo ActiveRecord::formatCurrency($SUM_ALL_REVENUE_HAVE_CALLOUT); ?></td>
                <td class="item_r item_b"><?php // echo ActiveRecord::formatCurrency($nSumRevenueNotCall); ?></td>
                <td class="item_c item_b"><?php echo $textCodeHaveCall; ?></td>
                <td class="item_c item_b"><?php echo $textAppHaveCall; ?></td>
                <td class="item_c item_b"><?php echo $textCallNoCall; ?></td>
                <td class="item_c item_b"><?php echo $textSumApp;?></td>
                <td class="item_c item_b"><?php echo $textSum;?></td>
            <?php foreach ($DATE_ARRAY as $days): ?>
                <?php
                $valueDate          = isset($SUM_OUT_FOLLOW['DATE'][$days]) ? $SUM_OUT_FOLLOW['DATE'][$days] : '0';
                $trackingvalueDate  = isset($SUM_OUT_TRACKING['DATE'][$days]) ? $SUM_OUT_TRACKING['DATE'][$days] : '0';
                $salevalueDate      = isset($SUM_OUT_SELL['DATE'][$days]) ? $SUM_OUT_SELL['DATE'][$days] : '0';
                $textDate = $valueDate.'/'.$trackingvalueDate.'/'.$salevalueDate;
                if ($textDate == '0/0/0'):
                    $textDate = '';
                endif;
                ?>
                <td class="w-60 item_c item_b"><?php echo $textDate; ?></td>
            <?php endforeach; ?>
            </tr>
            
        <?php foreach ($OUT_SELL_CHART as $user_id => $arrayValue): ?>
            <?php
//                if(empty($model->fromPttt) && !in_array($user_id, $aTelesale)){
//                    continue ;
//                }elseif($model->fromPttt && in_array($user_id, $aTelesale)){
//                    continue ;
//                }
                
                
                $name = isset($arrayValue['name']) ? $arrayValue['name']: '';
                $nRef           = isset($OUT_TRACKING[$user_id]) ? (int)$OUT_TRACKING[$user_id] : 0;
                $nFollow        = isset($OUT_FOLLOW_CHART[$user_id]['value']) ? $OUT_FOLLOW_CHART[$user_id]['value'] : '0';
                $nSale          = isset($OUT_SELL_CHART[$user_id]['value']) ? $OUT_SELL_CHART[$user_id]['value'] : '0';
                $nSumRevenue    = isset($SUM_REVENUE[$user_id]) ? $SUM_REVENUE[$user_id] : '0';
                $nSumOrderApp   = isset($OUT_SELL_APP_SUM[$user_id]['value']) ? $OUT_SELL_APP_SUM[$user_id]['value'] : '0';
                $nCusFollow     = isset($OUT_SELL_FOLLOW[$user_id]) ? $OUT_SELL_FOLLOW[$user_id] : '0';
                $appWithoutCall = isset($APP_WITHOUT_CALL_SUM[$user_id]) ? $APP_WITHOUT_CALL_SUM[$user_id] : '0';
                $codeHaveCall   = isset($TELESALE_CODE_CALLOUT[$user_id]) ? $TELESALE_CODE_CALLOUT[$user_id] : '0';
                
                $nSumRevenueCallOut     = isset($SUM_REVENUE_HAVE_CALLOUT[$user_id]) ? $SUM_REVENUE_HAVE_CALLOUT[$user_id] : 0;
                $nSumRevenueNotCall     = $nSumRevenue - $nSumRevenueCallOut;
                
                $textCusFollow  = $nCusFollow .'/'.($nSale-$nCusFollow);
                if ($textCusFollow == '0/0'):
                    $textCusFollow = '';
                endif;
//                    $textCusFollow = '';
                $textSumRow     = $nFollow .'/'.$nRef. '/' . $nSale;
                if ($textSumRow == '0/0/0'):
                    continue ;
                endif;
                $textAppHaveCall = '';
                if($nSumOrderApp > 0){
                    $textAppHaveCall = ($nSumOrderApp - $appWithoutCall).'/'.$nSumOrderApp;
                }
                $textCodeHaveCall = '';
                if($nRef > 0){
                    $textCodeHaveCall = $codeHaveCall.'/'.$nRef;
                }
                
            ?>
            <?php
                $totalDataChart[]           = [$name, $nFollow];
                $totalDataChartTracking[]   = [$name, $nRef];
                $totalDataChartSell[]       = [$name, $nSale];
            ?>
            <tr class="h_20">
                <td class="item_c"><?php echo $index++; ?></td>
                <td class="item_b"><?php echo $name; ?></td>
                
                <td class="item_r item_b"><?php echo ActiveRecord::formatCurrency($nSumRevenueCallOut); ?></td>
                <td class="item_r item_b"><?php echo ActiveRecord::formatCurrency($nSumRevenueNotCall); ?></td>
                <td class="item_c item_b"><?php echo $textCodeHaveCall; ?></td>
                <td class="item_c item_b"><?php echo $textAppHaveCall; ?></td>
                <td class="item_c item_b"><?php echo $textCusFollow; ?></td>
                <td class="item_c item_b"><?php echo ($nSale - $nSumOrderApp). '/' . $nSumOrderApp; ?></td>
                <td class="item_c item_b"><?php echo $textSumRow; ?></td>
                <?php foreach ($DATE_ARRAY as $days): ?>
                    <?php
                    $color = '';
                    $dayOfWeek = MyFormat::dateConverYmdToDmy($days, 'l');
                    if($dayOfWeek == 'Sunday'){
                        $color = 'ColorSunday';
                    }

                    $trackingvalue  = isset($OUT_TRACKING_DATE[$user_id][$days]) ? $OUT_TRACKING_DATE[$user_id][$days] : '0';
                    $value          = isset($OUT_FOLLOW[$user_id][$days]) ? $OUT_FOLLOW[$user_id][$days] : '0';
                    $saleValue      = isset($OUT_SELL[$user_id][$days]) ? $OUT_SELL[$user_id][$days] : '0';
                    $text = $value .'/'.$trackingvalue. '/' . $saleValue;
                    if ($text == '0/0/0'):
                        $text = '';
                    endif;
                    ?>
                <td class="item_c <?php echo $color;?>"><?php echo $text; ?></td>
                <?php endforeach; ?>
            </tr>
        <?php endforeach; ?>
        </tbody>
    </table>
</div>


<script type="text/javascript">
    fnAddClassOddEven('items');
    $('.freezetablecolumns').each(function () {
        var id_table = $(this).attr('id');
        $('#' + id_table).freezeTableColumns({
            width:       <?php echo Users::WIDTH_freezetable;?>,   // required - bt 1100
            height:      <?php echo $freeHeightTable;?>,   // required
            numFrozen: 9,     // optional
            frozenWidth: 700,   // optional
            clearWidths: true  // optional
        });
    });
    $(window).load(function () {
        var index = 1;
        $('.freezetablecolumns').each(function () {
            if (index == 1)
                $(this).closest('div.grid-view').show();
            index++;
        });
        fnAddClassOddEven('items');
    });
</script>
<?php include('chartReport.php'); ?>
<?php endif; ?>