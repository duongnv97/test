<?php
/** @Author: HOANG NAM 02/04/2018
 *  @Todo: chart
 **/

if (isset($totalDataChart)) {
    /** @var array $totalDataChart list value for lines */
    $jsArrayTotalCall = json_encode($totalDataChart);
    $jsArrayTotalSell = json_encode($totalDataChartSell);
    $jsArrayTotalTrachking = json_encode($totalDataChartTracking);
    ?>
    <div class="float_l" style="width: 1100px">
        <div id="success_call_chart" style="width: 1100px; height: 500px"></div>
    </div>
    <div class="float_l" style="width: 1100px">
        <div id="success_tracking_chart" style="width: 1100px; height: 500px"></div>
    </div>
    <div class="float_l" style="width: 1100px">
        <div id="success_sell_chart" style="width: 1100px; height: 500px"></div>
    </div>
    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script type="text/javascript">
        //        fnAddClassOddEven('items');
        google.charts.load("current", {packages: ["corechart"]});
        google.charts.setOnLoadCallback(drawChart);
        function drawChart() {
            // Making a 3D Pie Chart https://developers.google.com/chart/interactive/docs/gallery/piechart?hl=vi
            var data = google.visualization.arrayToDataTable(<?php echo $jsArrayTotalCall; ?>);

            var options = {
                title: 'Tổng số cuộc gọi đi',
                is3D: true,
            };
            var chart = new google.visualization.PieChart(document.getElementById('success_call_chart'));
            chart.draw(data, options);

            // Making a 3D Pie Chart https://developers.google.com/chart/interactive/docs/gallery/piechart?hl=vi
            var data2 = google.visualization.arrayToDataTable(<?php echo $jsArrayTotalTrachking; ?>);
            var options2 = {
                title: 'KH nhập mã',
                is3D: true,
            };
            var chart2 = new google.visualization.PieChart(document.getElementById('success_tracking_chart'));
            chart2.draw(data2, options2);
            
            // Making a 3D Pie Chart https://developers.google.com/chart/interactive/docs/gallery/piechart?hl=vi
            var data2 = google.visualization.arrayToDataTable(<?php echo $jsArrayTotalSell; ?>);
            var options2 = {
                title: 'Tổng số bình quay về',
                is3D: true,
            };
            var chart2 = new google.visualization.PieChart(document.getElementById('success_sell_chart'));
            chart2.draw(data2, options2);
            
            
        }
    </script>
<?php } ?>