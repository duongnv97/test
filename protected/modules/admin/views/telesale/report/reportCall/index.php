<?php
$this->breadcrumbs=array(
    $this->pageTitle,
);
$menus=array(
//            array('label'=> 'Xuất Excel Danh Sách Hiện Tại',
//               'url'=>array('ExportExcelReportGasBack'), 
//               'htmlOptions'=>array('class'=>'export_excel ','label'=>'Xuất Excel')),
);
$this->menu= ControllerActionsName::createMenusRoles($menus, $actions);
?>

<h1><?php echo $this->pageTitle;?></h1>
<div class="search-form" style="">
<?php $this->renderPartial('report/reportCall/search',array(
        'model'=>$model,
)); ?>
</div><!-- search-form -->

<style>
    .ui-datepicker-trigger { float: left;}
</style>

<script>
$(document).ready(function(){
    $('.form').find('button:submit').click(function(){        
        $.blockUI({ overlayCSS: { backgroundColor: '#fff' } }); 
    });   
 
});
</script>
<?php if(isset($aData['EMPLOYEE_VIEW'])): ?>
<?php
$gridCss = Yii::app()->getAssetManager()->publish(Yii::getPathOfAlias('zii.widgets.assets')) . '/gridview/styles.css';
Yii::app()->getClientScript()->registerCssFile($gridCss);
$index = 1;
// Get table height
$freeHeightTable = isset($aData['EMPLOYEE_VIEW']) ? (sizeof($aData['EMPLOYEE_VIEW']) + 2) * 19 : 400;
$freeHeightTable = $freeHeightTable > 400 ? 400 : $freeHeightTable;

// For piechart all total call
$totalDataChart[] = ['Nhân viên', 'Tổng nhận'];
$totalCallChart[] = ['Nhân viên', 'Tổng gọi'];
?>
<script type="text/javascript"
        src="<?php echo Yii::app()->theme->baseUrl; ?>/js/scroll/jquery.freezetablecolumns.1.1.js"></script>
<div class="grid-view">
    <table id="freezetablecolumns_report_call" class="items hm_table freezetablecolumns">
        <thead>
        <tr  class="h_50">
            <th class="w-20 ">#</th>
            <th class="w-150 ">Nhân viên</th>
            <th class="w-100 ">Nhận/Gọi</th>
        </tr>
        </thead>
        <tbody>
            <tr class="h_20">
                <td colspan="2" class="item_c item_b">Tổng</td>
                <?php 
                    $numGet             = isset($aData['Get']) ? array_sum($aData['Get']) : 0;
                    $numCall            = isset($aData['Called']) ? array_sum($aData['Called']) : 0;
                    $text               = $numGet.'/'.$numCall;
                    if($text == '0/0'){
                        $text       = '';
                    }
                ?>
                 <td class="item_b item_c"><?php echo $text; ?></td>
            </tr>
            
            <?php foreach ($aData['EMPLOYEE_VIEW'] as $user_id => $userName): ?>
            <?php
                $numGet             = isset($aData['Get'][$user_id]) ? (int)$aData['Get'][$user_id] : 0;
                $numCall             = isset($aData['Called'][$user_id]) ? (int)$aData['Called'][$user_id] : 0;
                $totalDataChart[]   = [$userName, (int)$numGet];
                $totalCallChart[]   = [$userName, (int)$numCall];
            ?>
                <tr class="h_20">
                    <td class="item_c"><?php echo $index++; ?></td>
                    <td class=""><?php echo $userName; ?></td>
                    <?php 
                    $numGetCurrent          = isset($aData['Get'][$user_id]) ? $aData['Get'][$user_id] : 0;
                    $numCallCurrent         = isset($aData['Called'][$user_id]) ? $aData['Called'][$user_id] : 0;
                    $textEmployee           = $numGetCurrent.'/'.$numCallCurrent;
                    if($textEmployee == '0/0'){
                        $textEmployee       = '';
                    }
                    ?>
                    <td class="item_c"><?php echo $textEmployee; ?></td>
                </tr>
                <?php endforeach; ?>
        </tbody>
    </table>
</div>
<?php include('chart.php'); ?>
<?php endif; ?>
