<?php
if (isset($totalDataChart)) {
    /** @var array $totalDataChart list value for lines */
    $jsArrayTotalGet = json_encode($totalDataChart);
    $jsArrayTotalCalled = json_encode($totalCallChart);
    ?>
    <div class="float_l" style="width: 1100px">
        <div id="success_chart" style="width: 1100px; height: 500px"></div>
    </div>
    <div class="float_l" style="width: 1100px">
        <div id="call_chart" style="width: 1100px; height: 500px"></div>
    </div>
    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script type="text/javascript">
        //        fnAddClassOddEven('items');
        google.charts.load("current", {packages: ["corechart"]});
        google.charts.setOnLoadCallback(drawChart);
        function drawChart() {
            // Making a 3D Pie Chart https://developers.google.com/chart/interactive/docs/gallery/piechart?hl=vi
            var data = google.visualization.arrayToDataTable(<?php echo $jsArrayTotalGet; ?>);

            var options = {
                title: 'Nhận',
                is3D: true,
            };
            var chart = new google.visualization.PieChart(document.getElementById('success_chart'));
            chart.draw(data, options);
            // Making a 3D Pie Chart https://developers.google.com/chart/interactive/docs/gallery/piechart?hl=vi
            var data = google.visualization.arrayToDataTable(<?php echo $jsArrayTotalCalled; ?>);

            var optionsCall = {
                title: 'Gọi',
                is3D: true,
            };
            var chart = new google.visualization.PieChart(document.getElementById('call_chart'));
            chart.draw(data, optionsCall);

        }
    </script>
<?php } ?>