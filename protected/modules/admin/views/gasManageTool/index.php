<?php
$this->breadcrumbs=array(
	'Quản Lý Cấp Phát Công Cụ-Dụng Cụ',
);

$menus=array(
            array('label'=>'Create Cấp Phát Công Cụ-Dụng cụ', 'url'=>array('create')),
);
$this->menu= ControllerActionsName::createMenusRoles($menus, $actions);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('gas-manage-tool-grid', {
                url : $(this).attr('action'),
		data: $(this).serialize()
	});
	return false;
});
");

Yii::app()->clientScript->registerScript('ajaxupdate', "
$('#gas-manage-tool-grid a.ajaxupdate').live('click', function() {
    $.fn.yiiGridView.update('gas-manage-tool-grid', {
        type: 'POST',
        url: $(this).attr('href'),
        success: function() {
            $.fn.yiiGridView.update('gas-manage-tool-grid');
        }
    });
    return false;
});
");
?>

<h1>Quản Lý Cấp Phát Công Cụ-Dụng Cụ</h1>

<?php echo CHtml::link('Tìm Kiếm Nâng Cao','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'gas-manage-tool-grid',
	'dataProvider'=>$model->search(),
        'afterAjaxUpdate'=>'function(id, data){ fnUpdateColorbox();}',
        'template'=>'{pager}{summary}{items}{pager}{summary}', 
        'pager' => array(
            'maxButtonCount'=>  CmsFormatter::$PAGE_MAX_BUTTON,
        ),            
	'enableSorting' => false,
	//'filter'=>$model,
	'columns'=>array(
        array(
            'header' => 'S/N',
            'type' => 'raw',
            'value' => '$this->grid->dataProvider->pagination->currentPage * $this->grid->dataProvider->pagination->pageSize + ($row+1)',
            'headerHtmlOptions' => array('width' => '10px','style' => 'text-align:center;'),
            'htmlOptions' => array('style' => 'text-align:center;')
        ),
		'code_no',
                array(
                    'name'=>'date_allocation',
                    'type'=>'date',
                    'htmlOptions' => array('style' => 'width:100px;text-align:center;')
                ),
                array(
                    'name'=>'employee_id',
                    'type'=>'NameAndRole',
                    'value'=>'$data->rEmployee',
//                    'htmlOptions' => array('style' => 'width:80px;'),
                ),
                array(
                    'header' => 'Chi Tiết',
                    'type'=>'GasManageToolDetail',
                    'value'=>'$data',
                ),
//                array(
//                    'name'  => 'amount',
//                    'value' => '$data->getAmount()',
//                ),
                array(
                    'name'=>'uid_login',
                    'value'=>'$data->rUidLogin?$data->rUidLogin->first_name:""',
//                    'htmlOptions' => array('style' => 'width:80px;'),
                ),
		'created_date:datetime',
		array(
                    'header' => 'Actions',
                    'class'=>'CButtonColumn',
                    'template'=> ControllerActionsName::createIndexButtonRoles($actions),
                    'buttons'=>array(
                        'update'=>array(
                            'visible'=> '$data->canUpdate()',
                        ),
                        'delete'=>array(
                            'visible'=> 'GasCheck::canDeleteData($data)',
                        ),
                    ), 
		),
	),
)); ?>

<script>
$(document).ready(function() {
    fnUpdateColorbox();
});

function fnUpdateColorbox(){   
    fixTargetBlank();
}
</script>