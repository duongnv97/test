<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'gas-manage-tool-form',
	'enableAjaxValidation'=>false,
)); ?>
<?php $mDetail = new GasManageToolDetail(); $index=1;?>

	<p class="note">Dòng có dấu <span class="required">*</span> là bắt buộc.</p>
        <?php echo $form->errorSummary($model); ?>
        
        <?php  if(Yii::app()->user->hasFlash('successUpdate')): ?>
            <div class='success_div'><?php echo Yii::app()->user->getFlash('successUpdate');?></div>         
        <?php endif; ?>  	
	<div class="row">
            <?php echo $form->labelEx($model,'date_allocation'); ?>
            <?php 
                $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                    'model'=>$model,        
                    'attribute'=>'date_allocation',
                    'options'=>array(
                        'showAnim'=>'fold',
                        'dateFormat'=> ActiveRecord::getDateFormatJquery(),
//                            'minDate'=>0,
                        'changeMonth' => true,
                        'changeYear' => true,
                        'showOn' => 'button',
                        'buttonImage'=> Yii::app()->theme->baseUrl.'/admin/images/icon_calendar_r.gif',
                        'buttonImageOnly'=> true,                                
                    ),        
                    'htmlOptions'=>array(
                        'class'=>'w-16',
                        'readonly'=>'readonly',
                    ),
                ));
                ?>
            <?php echo $form->error($model,'date_allocation'); ?>
	</div>
	<div class="row">
            <?php echo $form->labelEx($model,'employee_id'); ?>
            <?php echo $form->hiddenField($model,'employee_id', array('class'=>'')); ?>
            <?php // widget auto complete search user customer and supplier
            $url = Yii::app()->createAbsoluteUrl('admin/ajax/search_user_login', ['PaySalary' => 1]);
            $aData = array(
                'model'=>$model,
                'field_customer_id'=>'employee_id',
                'url'=> $url,
                'name_relation_user'=>'rEmployee',
            );
            $this->widget('ext.SpaAutocompleteCustomer.SpaAutocompleteCustomer',
                array('data'=>$aData));                                        
            ?>
            <?php echo $form->error($model,'employee_id'); ?>
	</div>
            
        <div class="row">
            <label>&nbsp</label>
            <div>
                <a href="javascript:void(0);" class="text_under_none item_b" style="line-height:25px" onclick="fnBuildRow();">
                    <img style="float: left;margin-right:8px;" src="<?php echo Yii::app()->theme->baseUrl;?>/images/add.png"> 
                    Thêm Dòng ( Phím tắt F8 )
                </a>
            </div>
        </div>
        <div class="clr"></div>
        
        <div class="row">
            <label>&nbsp</label>    
            <table class="materials_table hm_table">
                <thead>
                    <tr>
                        <th class="item_c">#</th>
                        <th class="item_c w-250">Vật Tư</th>
                        <th class="item_c w-100">Số Lượng</th>
                        <th class="item_c w-100">Đơn giá / 1 cái</th>
                        <th class="item_c w-80">Số tháng hoàn trả</th>
                        <th class="last item_c">Xóa</th>
                    </tr>
                </thead>
                <tbody>
                    <?php if(count($model->rManageToolDetail)): ?>
                        <?php foreach($model->rManageToolDetail as $item): ?>
                        <tr class="table_tr_row">
                            <td class="order_no item_c"><?php echo $index++;?></td>
                            <td class="item_c col_material">
                                <input class="materials_id_hide" value="<?php echo $item->materials_id;?>" type="hidden" name="GasManageToolDetail[materials_id][]">
                                <input class="float_l material_autocomplete w-220"  placeholder="Nhập mã hoặc tên vật tư" maxlength="100" value="<?php echo $item->rMaterial->name;?>" type="text" readonly="1">
                                <span class="remove_material_js remove_item_material"></span>
                            </td>
                            <td class="">
                                <input name="GasManageToolDetail[qty][]" class="qty number_only_v1" type="text" size="10" maxlength="8" value="<?php echo ActiveRecord::formatNumberInput($item->qty);?>">
                            </td>
                            <td class="">
                                <input name="GasManageToolDetail[price][]" class="qty number_only_v1 ad_fix_currency" type="text" size="10" maxlength="8" value="<?php echo ActiveRecord::formatNumberInput($item->price);?>">
                            </td>
                            <td class="">
                                <?php echo $form->dropDownList($mDetail,'month_pay[]', MyFormat::BuildNumberOrder(360), array('class'=>'w-80', 'options' => array( $item->month_pay => array('selected'=>true)) )); ?>
                            </td>
                            <td class="item_c last"><span class="remove_icon_only"></span></td>
                        </tr>
                        <?php endforeach;?>
                    
                    <?php else:?>
                        <tr class="table_tr_row">
                            <td class="order_no item_c">1</td>
                            <td class="item_c col_material">
                                <input class="materials_id_hide" value="" type="hidden" name="GasManageToolDetail[materials_id][]">
                                <input class="float_l material_autocomplete w-220"  placeholder="Nhập mã hoặc tên vật tư" maxlength="100" value="" type="text">
                                <span class="remove_material_js remove_item_material"></span>
                            </td>
                            <td class="">
                                <input name="GasManageToolDetail[qty][]" class="qty number_only_v1" type="text" size="10" maxlength="8">
                            </td>
                            <td class="">
                                <input name="GasManageToolDetail[price][]" class="qty number_only_v1 ad_fix_currency" type="text" size="10" maxlength="8">
                            </td>
                            <td class="">
                                <?php echo $form->dropDownList($mDetail,'month_pay[]', MyFormat::BuildNumberOrder(360), array('class'=>'w-80')); ?>
                            </td>
                            <td class="item_c last"><span class="remove_icon_only"></span></td>
                        </tr>
                    <?php endif;?>                    
                    
                </tbody>
            </table>            
        </div>

	<div class="row buttons" style="padding-left: 141px;">
            <?php $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType'=>'submit',
            'label'=>$model->isNewRecord ? 'Create' :'Save',
            'type'=>'null', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
            'size'=>'small', // null, 'large', 'small' or 'mini'
            //'htmlOptions' => array('style' => 'margin-bottom: 10px; float: right;'),
        )); ?>	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/autoNumeric-master/autoNumeric.js"></script>
<script>
    $(document).keydown(function(e) {
        if(e.which == 119) {
            fnBuildRow();
        }
    });    
    
    $(document).ready(function(){
        $('.form').find('button:submit').click(function(){
            $.blockUI({ overlayCSS: { backgroundColor: '#fff' } }); 
        });
        $('.table_tr_row:first').find('.remove_icon_only').hide();
        fnBindRemoveIcon();
        fnBindAllAutocomplete();
        fnBindRemoveMaterialFix('materials_id_hide', 'material_autocomplete');
        
        fnInitInputCurrency();
    });
    
    function fnBuildRow(){
        var tr_new = $('.table_tr_row:first').clone();
        tr_new.find('.remove_icon_only').css({'display':'block'});
        tr_new.find('.qty').val('');

        // for autocompete hide
        tr_new.find('.materials_id_hide').val('');
        tr_new.find('.material_autocomplete').attr('readonly',false).val('');
        // for autocompete hide
        
        tr_new.removeClass('selected');
        tr_new.find('.error').removeClass('error');
        $('.materials_table tbody').append(tr_new);        
        fnRefreshOrderNumber();
        fnBindAllAutocomplete();
        fnInitInputCurrency();
    }
    
    // to do bind autocompelte for input material
    // @param objInput : is obj input ex  $('.customer_autocomplete')    
    function bindMaterialAutocomplete(objInput){
        var availableMaterials = <?php echo MyFunctionCustom::getMaterialsJsonByType(GasMaterialsType::getMaterialTypeCCDC()); ?>;
        var parent_div = objInput.closest('.col_material');
        objInput.autocomplete({
            source: availableMaterials,
//            close: function( event, ui ) { $( "#GasStoreCard_materials_name" ).val(''); },
            select: function( event, ui ) {
                parent_div.find('.materials_id_hide').val(ui.item.id);
                parent_div.find('.material_autocomplete').attr('readonly',true);
            }
        });
    }
    
    function fnBindAllAutocomplete(){
        // material
        $('.material_autocomplete').each(function(){
            bindMaterialAutocomplete($(this));
        });
    }    
    
    
</script>