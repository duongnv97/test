<?php
$this->breadcrumbs=array(
	'Quản Lý Cấp Phát Công Cụ-Dụng Cụ'=>array('index'),
	'Tạo Mới',
);

$menus = array(		
        array('label'=>'Quản Lý Cấp Phát Công Cụ-Dụng Cụ', 'url'=>array('index')),
);
$this->menu= ControllerActionsName::createMenusRoles($menus, $actions);
?>

<h1>Tạo Mới Cấp Phát Công Cụ-Dụng Cụ</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>