<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'code_no',array()); ?>
		<?php echo $form->textField($model,'code_no',array('size'=>30,'maxlength'=>30)); ?>
	</div>

	<div class="row more_col">
            <div class="col1">
                    <?php echo Yii::t('translation', $form->label($model,'date_from')); ?>
                    <?php 
                        $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                            'model'=>$model,        
                            'attribute'=>'date_from',
                            'options'=>array(
                                'showAnim'=>'fold',
                                'dateFormat'=> MyFormat::$dateFormatSearch,
    //                            'minDate'=> '0',
                                'maxDate'=> '0',
                                'changeMonth' => true,
                                'changeYear' => true,
                                'showOn' => 'button',
                                'buttonImage'=> Yii::app()->theme->baseUrl.'/admin/images/icon_calendar_r.gif',
                                'buttonImageOnly'=> true,                                
                            ),        
                            'htmlOptions'=>array(
                                'class'=>'w-16',
                                'size'=>'16',
                                'style'=>'float:left;',                               
                            ),
                        ));
                    ?>     		
            </div>
            <div class="col2">
                    <?php echo Yii::t('translation', $form->label($model,'date_to')); ?>
                    <?php 
                        $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                            'model'=>$model,        
                            'attribute'=>'date_to',
                            'options'=>array(
                                'showAnim'=>'fold',
                                'dateFormat'=> MyFormat::$dateFormatSearch,
    //                            'minDate'=> '0',
                                'maxDate'=> '0',
                                'changeMonth' => true,
                                'changeYear' => true,
                                'showOn' => 'button',
                                'buttonImage'=> Yii::app()->theme->baseUrl.'/admin/images/icon_calendar_r.gif',
                                'buttonImageOnly'=> true,                                
                            ),        
                            'htmlOptions'=>array(
                                'class'=>'w-16',
                                'size'=>'16',
                                'style'=>'float:left;',
                            ),
                        ));
                    ?>     		
            </div>
        </div> 
    

	<div class="row">
		<?php echo $form->label($model,'employee_id',array()); ?>
		<?php echo $form->hiddenField($model,'employee_id', array('class'=>'')); ?>
                <?php // widget auto complete search user customer and supplier
                $url = Yii::app()->createAbsoluteUrl('admin/ajax/search_user_login', ['PaySalary' => 1]);
                $aData = array(
                    'model'=>$model,
                    'field_customer_id'=>'employee_id',
                    'url'=> $url,
                    'name_relation_user'=>'rEmployee',
                );
                $this->widget('ext.SpaAutocompleteCustomer.SpaAutocompleteCustomer',
                    array('data'=>$aData));                                        
                ?>
	</div>
    
        <div class="row">
            <?php echo Yii::t('translation', $form->labelEx($model,'ext_materials_id')); ?>
            <?php echo $form->hiddenField($model,'ext_materials_id'); ?>		
            <?php 
                // widget auto complete search material
                $aData = array(
                    'model'=>$model,
                    'field_material_id'=>'ext_materials_id',
                    'name_relation_material'=>'rMaterial',
                );
                $this->widget('ext.GasAutocompleteMaterial.GasAutocompleteMaterial',
                    array('data'=>$aData));                                        
            ?>				
        </div>

	<div class="row buttons" style="padding-left: 159px;">
                <?php $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType'=>'submit',
            'label'=>'Search',
            'type'=>'null', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
            'size'=>'small', // null, 'large', 'small' or 'mini'
            //'htmlOptions' => array('style' => 'margin-bottom: 10px; float: right;'),
        )); ?>	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->