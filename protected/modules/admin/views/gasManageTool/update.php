<?php
$this->breadcrumbs=array(
	'Quản Lý Cấp Phát Công Cụ-Dụng Cụ'=>array('index'),
	$model->code_no=>array('view','id'=>$model->id),
	'Cập Nhật',
);

$menus = array(	
        array('label'=>'Quản Lý Cấp Phát Công Cụ-Dụng Cụ', 'url'=>array('index')),
	array('label'=>'Xem Cấp Phát Công Cụ-Dụng Cụ', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Tạo Mới Cấp Phát Công Cụ-Dụng Cụ', 'url'=>array('create')),	
);
$this->menu= ControllerActionsName::createMenusRoles($menus, $actions);

?>

<h1>Cập Nhật Cấp Phát Công Cụ-Dụng Cụ: <?php echo $model->code_no; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>