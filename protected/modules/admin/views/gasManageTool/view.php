<?php
$this->breadcrumbs=array(
	'Quản Lý Cấp Phát Công Cụ-Dụng Cụ'=>array('index'),
	$model->code_no,
);

$menus = array(
	array('label'=>'Quản Lý Cấp Phát Công Cụ-Dụng Cụ', 'url'=>array('index')),
	array('label'=>'Tạo Mới  Cấp Phát Công Cụ-Dụng Cụ', 'url'=>array('create')),
	array('label'=>'Cập Nhật Cấp Phát Công Cụ-Dụng Cụ', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Xóa Cấp Phát Công Cụ-Dụng Cụ', 'url'=>array('delete'), 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Bạn chắc chắn muốn xóa ?')),
);
$this->menu= ControllerActionsName::createMenusRoles($menus, $actions);
$sum = 0;
?>

<h1>Xem Cấp Phát Công Cụ-Dụng Cụ: <?php echo $model->code_no; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'code_no',
		'date_allocation:date',
                array(
                    'name'=>'employee_id',
                    'type'=>'NameAndRole',
                    'value'=>$model->rEmployee,
                ),  
//                array(
//                    'name'  => 'amount',
//                    'value' => $model->getAmount(),
//                ),
                array(
                    'name'=>'uid_login',
                    'value'=>$model->rUidLogin ? $model->rUidLogin->first_name : '',
                ),  
		'created_date:datetime',
	),
)); ?>

<div class="row">
    <label>&nbsp</label>    
    <table class="materials_table hm_table">
        <thead>
            <tr>
                <th class="item_c">#</th>
                <th class="item_c w-250">Vật Tư</th>
                <th class="item_c w-100">Số Lượng</th>
                <th class="item_c w-100">Đơn giá / 1 cái</th>
                <th class="item_c w-100">Thành tiền</th>
                <th class="item_c w-80">Số tháng hoàn trả</th>
            </tr>
        </thead>
        <tbody>
            <?php if(count($model->rManageToolDetail)): ?>
                <?php foreach($model->rManageToolDetail as $key=>$item): ?>
                <?php $sum += $item->getAmount(); ?>
                <tr class="table_tr_row">
                    <td class="order_no item_c"><?php echo $key+1;?></td>
                    <td class="item_l col_material">
                        <?php echo $item->rMaterial->name;?>
                    </td>
                    <td class="item_c">
                        <?php echo ActiveRecord::formatCurrency($item->qty);?>
                    </td>
                    <td class="item_r">
                        <?php echo ActiveRecord::formatCurrency($item->price);?>
                    </td>
                    <td class="item_r">
                        <?php echo $item->getAmount(true);?>
                    </td>
                    <td class="item_c">
                        <?php echo $item->month_pay;?> tháng
                    </td>
                </tr>
                <?php endforeach;?>
            <?php endif;?>
        </tbody>
        <tfoot>
            <tr>
                <td colspan="4" class="item_r item_b">Tổng</td>
                <td class="item_r item_b"><?php echo ActiveRecord::formatCurrency($sum);?></td>
                <td></td>
            </tr>
        </tfoot>
    </table>
</div>