<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=> Yii::app()->createAbsoluteUrl("admin/gasWorkSchedule/index"),
	'method'=>'get',
)); ?>
	<div class="row">
            <?php echo $form->label($model,'year',array()); ?>
            <?php echo $form->dropDownList($model,'year', ActiveRecord::getRangeYear(2016, 2150),array('class'=>'w-400')); ?>
	</div>
	<div class="row buttons" style="padding-left: 159px;">
            <?php $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType'=>'submit',
            'label'=>'Search',
            'type'=>'null', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
            'size'=>'small', // null, 'large', 'small' or 'mini'
            //'htmlOptions' => array('style' => 'margin-bottom: 10px; float: right;'),
        )); ?>	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->