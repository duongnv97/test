<?php
$this->breadcrumbs=array(
	$this->singleTitle,
);

$menus=array(
            array('label'=>"Create $this->singleTitle", 'url'=>array('create')),
);
$this->menu= ControllerActionsName::createMenusRoles($menus, $actions);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('gas-work-schedule-grid', {
                url : $(this).attr('action'),
		data: $(this).serialize()
	});
	return false;
});
");

Yii::app()->clientScript->registerScript('ajaxupdate', "
$('#gas-work-schedule-grid a.ajaxupdate').live('click', function() {
    $.fn.yiiGridView.update('gas-work-schedule-grid', {
        type: 'POST',
        url: $(this).attr('href'),
        success: function() {
            $.fn.yiiGridView.update('gas-work-schedule-grid');
        }
    });
    return false;
});
");
$date = new DateTime(date("Y-m-d"));
$cWeek = $date->format("W");
?>

<?php include "index_c_week.php"; ?>

<?php
$numberWeekOfYear = MyFormat::getIsoWeeksInYear($model->year);
$aWeekDays = array();
$begin = 1;
for($i = $begin; $i<= $numberWeekOfYear; $i++): 
    $aDate = MyFormat::dateByWeekNumber($model->year, $i);
    $aDayOfWeek = MyFormat::getArrayDay($aDate['date_from'], $aDate['date_to']);
    $temp = array(
        'aDateBeginEnd'=> $aDate,
        'aDayOfWeek'=> $aDayOfWeek,
    );
    $aWeekDays[$i] = $temp;
endfor;
$aSlot = $model->getArrEmployeeAssignToSlot();
$aSlotText = $model->getArrSlotText();
$model->date_from = $aWeekDays[$begin]['aDateBeginEnd']['date_from'];
$model->date_to = $aWeekDays[$numberWeekOfYear]['aDateBeginEnd']['date_to'];
$allOfYear = $model->getAll();
$aRes = $model->countBySlot(GasWorkSchedule::CA_OFF);
//echo '<pre>';
//print_r($aRes);
//echo '</pre>';

?>
<h1><a class="click_schedule_year" href="javascript:void(0)">Xem Lịch Năm <?php echo $model->year;?></a></h1>
<div class="wrap_schedule_year display_none">
<?php foreach ($aWeekDays as $weekNo=>$aDayOfWeek): ?>
<table class=" materials_table t_padding_20 week_no_<?php echo $weekNo;?>">
    <thead>
        <?php if($weekNo == $cWeek): ?>
        <tr>
            <th class="item_c td_last_r f_size_15" colspan="8">
                Tuần Hiện Tại
            </th>
        </tr>
        <?php endif; ?>
    </thead>
    <tbody>
        <tr>
            <td class="item_b"><?php echo "Week $weekNo ".MyFormat::dateConverYmdToDmy($aDayOfWeek['aDateBeginEnd']['date_from'])." - ".MyFormat::dateConverYmdToDmy($aDayOfWeek['aDateBeginEnd']['date_to']); ?></td>
            <?php foreach ($aDayOfWeek['aDayOfWeek'] as $day): ?>
                <td class="item_b item_c"><?php echo MyFormat::$TheDaysOfTheWeek[MyFormat::dateConverYmdToDmy($day, "l")] ?> <br><?php echo MyFormat::dateConverYmdToDmy($day, "d-m"); ?></td>
            <?php endforeach; ?>
        </tr>
        <?php foreach ($model->getArrEmployee() as $employee_id => $nameEmployee): ?>
        <tr>
            <td class="item_b"><?php echo $nameEmployee;?></td>
            <?php foreach ($aDayOfWeek['aDayOfWeek'] as $day): ?>
                <?php foreach ($aSlotText as $slot_id => $slotText): ?>
                    <?php if(isset($allOfYear[$day][$employee_id][$slot_id])): ?>
                    <td class="w-60 item_c slot_color_<?php echo $slot_id; ?>"><?php echo $slotText; ?></td>
                    <?php endif; ?>
                <?php endforeach; ?>
            <?php endforeach; ?>
        </tr>            
        <?php endforeach; // end foreach ($model->getArrEmployee() ?>
    </tbody>
</table>

<?php endforeach; ?>
</div>

<script>
$(document).ready(function() {
    $('.c_week').html($(".week_no_<?php echo $cWeek;?>").html());
    $('.c_week').next('table').html($(".week_no_<?php echo $cWeek+1;?>").html());
    $('.c_week').next('table').next('table').html($(".week_no_<?php echo $cWeek+2;?>").html());
    $('.click_schedule_year').click(function(){
       $('.wrap_schedule_year').toggle();
    });
    
    $(".gallery").colorbox({iframe:true,innerHeight:'1500', innerWidth: '1050',close: "<span title='close'>close</span>"});
});
</script>

<style>
.slot_color_1 {background: #F48FB1;}
.slot_color_2 {background: #FFD54F;}
.slot_color_3 {background: #4CAF50;}
.slot_color_4 {background: #C8E6C9;}
.slot_color_5 {background: #9E9E9E;}
</style>