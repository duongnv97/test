<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'gas-work-schedule-form',
	'enableAjaxValidation'=>false,
        'htmlOptions' => array('enctype' => 'multipart/form-data'),
)); ?>

    <p class="note">Dòng có dấu <span class="required">*</span> là bắt buộc.</p>
    <?php echo MyFormat::BindNotifyMsg(); ?>

    <div class="row">
        <?php echo $form->labelEx($model,'weekNo'); ?>
        <?php echo $form->dropDownList($model,'weekNo', MyFormat::BuildNumberOrder(53),array('class'=>'w-400')); ?>
        <?php echo $form->error($model,'weekNo'); ?>
    </div>
    <div class="row">
        <?php echo $form->labelEx($model,'year'); ?>
        <?php echo $form->dropDownList($model,'year', ActiveRecord::getRangeYear(2016, 2150),array('class'=>'w-400')); ?>
        <?php echo $form->error($model,'year'); ?>
    </div>

    <div class="row buttons" style="padding-left: 141px;">
        <?php $this->widget('bootstrap.widgets.TbButton', array(
        'buttonType'=>'submit',
        'label'=>$model->isNewRecord ? 'Create' :'Save',
        'type'=>'null', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
        'size'=>'small', // null, 'large', 'small' or 'mini'
        //'htmlOptions' => array('style' => 'margin-bottom: 10px; float: right;'),
    )); ?>	
    </div>

<?php $this->endWidget(); ?>

</div><!-- form -->
<script>
    $(document).ready(function(){
        $('.form').find('button:submit').click(function(){
            $.blockUI({ overlayCSS: { backgroundColor: '#fff' } }); 
        });
        
    });
</script>