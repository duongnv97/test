<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'price-support-form',
	'enableAjaxValidation'=>false,
        'htmlOptions' => array('enctype' => 'multipart/form-data'),
)); ?>

    <p class="note">Dòng có dấu <span class="required">*</span> là bắt buộc.</p>
    <?php echo MyFormat::BindNotifyMsg(); ?>
    
    <div class="row display_none">
        <?php echo $form->labelEx($model,'type'); ?>
        <?php echo $form->dropDownList($model,'type', $model->getArrayType(), array('empty'=>'select','class'=>'w-300')); ?>
        <?php echo $form->error($model,'type'); ?>
    </div>
    <div class="row display_none">
        <?php echo $form->labelEx($model,'status'); ?>
        <?php echo $form->dropDownList($model,'status', $model->getArrayStatus(), array('class'=>'w-300')); ?>
        <?php echo $form->error($model,'status'); ?>
    </div>
    <div class="row">
        <?php echo $form->labelEx($model,'date_apply'); ?>
        <?php 
            $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                'model'=>$model,        
                'attribute'=>'date_apply',
                'options'=>array(
                    'showAnim'=>'fold',
                    'dateFormat'=> MyFormat::$dateFormatSearch,
                    'changeMonth' => true,
                    'changeYear' => true,
                    'showOn' => 'button',
                    'buttonImage'=> Yii::app()->theme->baseUrl.'/admin/images/icon_calendar_r.gif',
                    'buttonImageOnly'=> true,                                
                ),        
                'htmlOptions'=>array(
                    'class'=>'w-16',
                    'style'=>'height:20px;',
                        'readonly'=>'readonly',
                ),
            ));
        ?>  
        <?php echo $form->error($model,'date_apply'); ?>
    </div>

    <div class="clr"></div>
    <div class="row">
        <label>&nbsp</label>
        <div class="float_l">
            <a href="javascript:void(0);" style="line-height:25px"  class="text_under_none item_b" onclick="fnBuildRow();">
                <img style="float: left;margin-right:8px;" src="<?php echo Yii::app()->theme->baseUrl;?>/images/add.png"> 
                Thêm Dòng ( Phím tắt F8 )
            </a>
            <!--<br><em class="hight_light item_b">Chú Ý: Giá khác bình bò phải là giá KG, giá khác bình 12 có thể là giá KG hoặc giá bình </em>-->
        </div>
    </div>
    <div class="clr"></div>
    
    <div class="row">
        <table class="materials_table materials_table_root materials_table_th hm_table f_size_15">
            <thead>
                <tr>
                    <th class="item_c w-20">#</th>
                    <th class="w-350 item_c">Tên khách hàng</th>
                    <th class=" item_c">Giá bình 12</th>
                    <th class=" item_c">Giá bình 45</th>
                    <th class=" item_c">Ghi chú</th>
                    <th class="item_c last">Xóa</th>
                </tr>
            </thead>
            <tbody>
                <!--tr copy-->
                <tr class="tr-copy display_none">
                    <td class="item_c"></td>
                    <td class="col_customer w-400">
                        <div class="row row_customer_autocomplete">
                            <div class="float_l">
                                <input class="customer_id_hide" name="PriceSupport[customer_id][]" value="" type="hidden">
                                <input class="float_l customer_autocomplete w-250"  placeholder="Nhập tên số đt, từ <?php echo MIN_LENGTH_AUTOCOMPLETE; ?> ký tự" maxlength="100" value="" type="text" >
                                <span class="remove_row_item" onclick="fnRemoveNameHand(this);"></span>
                            </div>
                        </div>
                    </td>
                    <td>
                        <input class="w-120 ad_fix_currency number_only_v1 item_r price_12" name="PriceSupport[price_12][]" maxlength="14" type="text" placeholder="Bình 12">
                    </td>
                    <td>
                        <input class="w-120 ad_fix_currency number_only_v1 item_r price_45" name="PriceSupport[price_45][]" maxlength="14" type="text"  placeholder="Bình 45 + 50">
                    </td>
                    <td>
                        <!--<input class="w-300" name="PriceSupport[note][]" placeholder="Ghi chú" type="text">-->
                        <textarea name="PriceSupport[note][]" class="w-200" rows="3"></textarea>
                    </td>
                    <td class="item_c last w-20"><span remove="" class="remove_icon_only"></span></td>
                </tr>
                <!--end tr copy-->
            </tbody>
        </table>
    </div>
    
    <div class="row buttons" style="padding-left: 141px;">
        <?php $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType'=>'submit',
            'label'=>$model->isNewRecord ? 'Create' :'Save',
            'type'=>'null', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
            'size'=>'small', // null, 'large', 'small' or 'mini'
            //'htmlOptions' => array('style' => 'margin-bottom: 10px; float: right;'),
        )); ?>	
    </div>

<?php $this->endWidget(); ?>

</div><!-- form -->

<?php Yii::app()->clientScript->registerCoreScript('jquery.ui'); ?>
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/autoNumeric-master/autoNumeric.js"></script>
<script>
    $(document).ready(function(){
        $('.form').find('button:submit').click(function(){
            $.blockUI({ overlayCSS: { backgroundColor: '#fff' } }); 
        });
        fnBuildRow();
        fnBindAllAutocomplete();
        fnBindRemoveIcon();
    });
    
    function fnBindAllAutocomplete(){
        $('.customer_autocomplete').each(function(){
            fnBindAutocomplete($(this));
        });
    }
    
    // to do bind autocompelte for input
    // @param objInput : is obj input ex  $('.customer_autocomplete')
    function fnBindAutocomplete(objInput){
        var parent_div = objInput.closest('td.col_customer');
        objInput.autocomplete({
            source: '<?php echo Yii::app()->createAbsoluteUrl('admin/ajax/search_by_role', ['role'=>ROLE_CUSTOMER]);?>',
            minLength: '<?php echo MIN_LENGTH_AUTOCOMPLETE;?>',
            close: function( event, ui ) { 
                //$( "#GasStoreCard_materials_name" ).val(''); 
            },
            search: function( event, ui ) { 
                    objInput.addClass('grid-view-loading-gas');
            },
            response: function( event, ui ) { 
                objInput.removeClass('grid-view-loading-gas');
                var json = $.map(ui, function (value, key) { return value; });
                if(json.length<1){
                    var error = '<div class=\'errorMessage clr autocomplete_name_text\'>Không tìm thấy dữ liệu.</div>';
                    if(parent_div.find('.autocomplete_name_text').size()<1){
                        parent_div.find('.remove_row_item').after(error);
                    }
                    else
                        parent_div.find('.autocomplete_name_text').show();
                }                    
                    
            },
            select: function( event, ui ) {
                objInput.attr('readonly',true);
                parent_div.find('.autocomplete_name_text').hide();   
                parent_div.find('.customer_id_hide').val(ui.item.id);   
                parent_div.find('.autocomplete_customer_info').remove();   
                var tableInfo = fnBuildTableCustomerInfo(ui.item.code_bussiness, ui.item.name_customer, ui.item.address, ui.item.phone);
                parent_div.append(tableInfo);
                parent_div.find('.autocomplete_customer_info').show();
            }

        });
    }
    
    function fnBuildTableCustomerInfo(info_code_bussiness, info_name, info_address, info_phone){
        var table = '';
        table += '<div class="autocomplete_customer_info table_small" style="">';
            table += '<table>';
                table += '<tr>';
                    table += '<td class="_l td_first_t">Mã KH:</td>';
                    table += '<td class="_r info_code_bussiness td_last_r td_first_t">'+info_code_bussiness+'</td>';
                table += '</tr>';
                table += '<tr>';
                    table += '<td class="_l">Tên KH:</td>';
                    table += '<td class="_r info_name td_last_r">'+info_name+'</td>';
                table += '</tr>';
                table += '<tr>';
                    table += '<td class="_l">Địa chỉ:</td>';
                    table += '<td class="_r info_address td_last_r">'+info_address+'</td>';
                table += '</tr>';
                table += '<tr>';
                    table += '<td class="_l">Điện Thoại:</td>';
                    table += '<td class="_r info_phone td_last_r">'+info_phone+'</td>';
                table += '</tr>';
            table += '</table>';
        table += '</div>';
        table += '<div class="clr"></div';
        return table;
    }
    
    function fnBuildRow(){
        var trCopy = $('.tr-copy').clone();
        trCopy.removeClass('tr-copy display_none');
        trCopy.find('td').eq(0).addClass('order_no');
//        trCopy.find('.customer_id_hide').attr('name', 'PriceSupport[customer_id][]');
//        trCopy.find('.price_12').attr('name', 'PriceSupport[price_12][]');
//        trCopy.find('.price_45').attr('name', 'PriceSupport[price_45][]');
        $('.materials_table_root').append(trCopy);
        fnRefreshOrderNumber();
        fnBindAllAutocomplete();
        fnInitInputCurrency();
    }
</script>