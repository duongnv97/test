<?php
$this->breadcrumbs=array(
    $this->pluralTitle => array('index'),
    ' ' . $this->singleTitle . ' : ',
);

$menus = array(
	array('label'=>"$this->pluralTitle Management", 'url'=>array('index')),
	array('label'=>"Tạo Mới $this->singleTitle", 'url'=>array('create')),
	array('label'=>"Cập Nhật $this->singleTitle", 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>"Xóa $this->singleTitle", 'url'=>array('delete'), 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Bạn chắc chắn muốn xóa ?')),
);
$this->menu= ControllerActionsName::createMenusRoles($menus, $actions);
?>

<h1>Xem: <?php echo $this->singleTitle; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		array(
                'name'=>'customer_id',
                'type'=>"raw",
                'value'=> $model->getCustomer(),
            ),
            array(
                'name'=>'type_customer',
                'type'=>"raw",
                'value'=> $model->getTypeCustomer(),
            ),
            array(
                'name'=>'type',
                'type'=>"raw",
                'value'=> $model->getType(),
            ),
            array(
                'name'=>'price_12',
                'type'=>"raw",
                'value'=> $model->getPrice12(),
            ),
            array(
                'name'=>'price_45',
                'type'=>"raw",
                'value'=> $model->getPrice45(),
            ),
            array(
                'name'=>'date_apply',
                'type'=>"raw",
                'value'=> $model->getDateApply(),
            ),
            array(
                'name'=>'status',
                'type'=>"raw",
                'value'=> $model->getStatus(),
            ),
	),
)); ?>
