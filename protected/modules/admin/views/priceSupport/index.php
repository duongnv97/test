<?php
$this->breadcrumbs=array(
	$this->singleTitle,
);

$menus=array(
            array('label'=>"Create $this->singleTitle", 'url'=>array('create')),
);
$this->menu= ControllerActionsName::createMenusRoles($menus, $actions);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('price-support-grid', {
                url : $(this).attr('action'),
		data: $(this).serialize()
	});
	return false;
});
");

Yii::app()->clientScript->registerScript('ajaxupdate', "
$('#price-support-grid a.ajaxupdate').live('click', function() {
    $.fn.yiiGridView.update('price-support-grid', {
        type: 'POST',
        url: $(this).attr('href'),
        success: function() {
            $.fn.yiiGridView.update('price-support-grid');
        }
    });
    return false;
});
");
?>

<h1>Danh Sách <?php echo $this->pluralTitle; ?></h1>


<?php echo CHtml::link('Tìm Kiếm Nâng Cao','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display: none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'price-support-grid',
	'dataProvider'=>$model->search(),
        'afterAjaxUpdate'=>'function(id, data){ fnUpdateColorbox();}',
        'template'=>'{pager}{summary}{items}{pager}{summary}',
//    'itemsCssClass' => 'items custom_here',
//    'htmlOptions'=> array('class'=>'grid-view custom_here'),
        'pager' => array(
            'maxButtonCount'=>  CmsFormatter::$PAGE_MAX_BUTTON,
        ),
	'enableSorting' => false,
	//'filter'=>$model,
	'columns'=>array(
        array(
            'header' => 'S/N',
            'type' => 'raw',
            'value' => '$this->grid->dataProvider->pagination->currentPage * $this->grid->dataProvider->pagination->pageSize + ($row+1)',
            'headerHtmlOptions' => array('width' => '10px','style' => 'text-align:center;'),
            'htmlOptions' => array('style' => 'text-align:center;')
        ),
            array(
                'name'=>'type',
                'type'=>"raw",
                'value'=> '$data->getType()',
                'visible'=>Yii::app()->user->role_id == ROLE_ADMIN,
            ),
            array(
                'name'=>'customer_id',
                'type'=>"raw",
                'value'=> '$data->getCustomer()',
            ),
            array(
                'header'=>'Địa chỉ',
                'type'=>"raw",
                'value'=> '$data->getCustomer("address")',
            ),
            array(
                'name'=>'type_customer',
                'type'=>"raw",
                'value'=> '$data->getTypeCustomer()',
            ),
            
            array(
                'name'=>'sale_id',
                'type'=>"raw",
                'value'=> '$data->getSale()',
            ),
            
            array(
                'name'=>'price_12',
                'type'=>"raw",
                'value'=> '$data->getPrice12()',
                'htmlOptions' => array('style' => 'text-align:right;')
            ),
            array(
                'name'=>'price_45',
                'type'=>"raw",
                'value'=> '$data->getPrice45()',
                'htmlOptions' => array('style' => 'text-align:right;')
            ),
            array(
                'name'=>'date_apply',
                'type'=>"raw",
                'value'=> '$data->getDateApply()',
            ),
            array(
                'name'=>'status',
                'type'=>"raw",
                'value'=> '$data->getStatus()',
                'htmlOptions' => array('style' => 'text-align:center;')
            ),
            array(
                'name'=>'note',
                'type'=>"raw",
                'value'=> '$data->getNote()',
            ),
            array(
                'name'=>'created_date',
                'type'=>"raw",
                'htmlOptions' => array('style' => 'text-align:center;'),
                'value'=> '$data->getCreatedDate()',
            ),
		array(
                    'header' => 'Actions',
                    'class'=>'CButtonColumn',
                    'template'=> ControllerActionsName::createIndexButtonRoles($actions),
                    'buttons'=>array(
                        'update' => [
                            'visible' => '$data->canUpdate()',
                        ],
                        'delete' => [
                            'visible' => '$data->canDelete()',
                        ]
                    ),
		),
	),
)); ?>

<script>
$(document).ready(function() {
    fnUpdateColorbox();
});

function fnUpdateColorbox(){   
    fixTargetBlank();
}
</script>