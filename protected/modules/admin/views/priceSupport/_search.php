<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=> GasCheck::getCurl(),
	'method'=>'get',
)); ?>

	<div class="row">
            <?php echo $form->label($model,'type',array()); ?>
            <?php echo $form->dropDownList($model,'type', $model->getArrayType(), array('empty'=>'select')); ?>
	</div>
    
        <div class="row ">
            <?php echo $form->labelEx($model, 'customer_id'); ?>
            <?php echo $form->hiddenField($model, 'customer_id'); ?>
            <?php
            // 1. limit search kh của sale
            $url =  Yii::app()->createAbsoluteUrl('admin/ajax/search_user_by_code');
            // widget auto complete search user customer and supplier
            $aData = array(
                'model' => $model,
                'field_customer_id' => 'customer_id',
                'url' => $url,
                'name_relation_user' => 'rCustomer',
                'ClassAdd' => 'w-300',
                'placeholder' => 'Nhập mã hoặc tên khách hàng',
            );
            $this->widget('ext.SpaAutocompleteCustomer.SpaAutocompleteCustomer',             array('data' => $aData));
            ?>
            <?php echo $form->error($model, 'customer_id'); ?>
        </div>
    
        <div class="row">
            <?php echo $form->label($model,'sale_id'); ?>
            <?php echo $form->hiddenField($model,'sale_id'); ?>
            <?php 
                // widget auto complete search user customer and supplier
                $aData = array(
                    'model'=>$model,
                    'field_customer_id'=>'sale_id',
                    'field_autocomplete_name'=>'created_date',
                    'url'=> Yii::app()->createAbsoluteUrl('admin/ajax/search_sale'),
                    'name_relation_user'=>'rSale',
                    'placeholder'=>'Nhập Tên Sale',
                    'ClassAdd' => 'w-400',
    //                        'fnSelectCustomer'=>'fnAfterSelectSaleOrAgent',
                );
                $this->widget('ext.SpaAutocompleteCustomer.SpaAutocompleteCustomer',
                    array('data'=>$aData));                                        
            ?>
        </div>

	<div class="row">
		<?php echo $form->label($model,'type_customer',array()); ?>
		<?php echo $form->dropDownList($model,'type_customer',CmsFormatter::$CUSTOMER_BO_MOI,array('empty'=>'Select')); ?>
	</div>

        <div class="row more_col">
            <div class="col1">
                <?php echo $form->label($model,'price_12',array()); ?>
                <?php echo $form->textField($model,'price_12',array('class'=>'number_only_v1')); ?>
            </div>
            <div class="col2">
                <?php echo $form->label($model,'price_45',array()); ?>
                <?php echo $form->textField($model,'price_45',array('class'=>'number_only_v1')); ?>
            </div>
        </div>
    
        <div class="row more_col">
            <div class="col1">
                <?php echo $form->labelEx($model,'date_from', ['label'=>'Ngày áp dụng từ']); ?>
                 <?php 
                    $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                        'model'=>$model,        
                        'attribute'=>'date_from',
                        'options'=>array(
                            'showAnim'=>'fold',
                            'dateFormat'=> MyFormat::$dateFormatSearch,
                            'changeMonth' => true,
                            'changeYear' => true,
                            'showOn' => 'button',
                            'buttonImage'=> Yii::app()->theme->baseUrl.'/admin/images/icon_calendar_r.gif',
                            'buttonImageOnly'=> true,                                
                        ),        
                        'htmlOptions'=>array(
                            'class'=>'w-16',
                            'style'=>'height:20px;',
                        ),
                    ));
                    ?>  
                <?php echo $form->error($model,'date_from'); ?>
            </div>
            <div class="col2">
                <?php echo $form->labelEx($model,'date_to', ['label'=>'đến']); ?>
                 <?php 
                    $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                        'model'=>$model,        
                        'attribute'=>'date_to',
                        'options'=>array(
                            'showAnim'=>'fold',
                            'dateFormat'=> MyFormat::$dateFormatSearch,
                            'changeMonth' => true,
                            'changeYear' => true,
                            'showOn' => 'button',
                            'buttonImage'=> Yii::app()->theme->baseUrl.'/admin/images/icon_calendar_r.gif',
                            'buttonImageOnly'=> true,                                
                        ),        
                        'htmlOptions'=>array(
                            'class'=>'w-16',
                            'style'=>'height:20px;',
                        ),
                    ));
                    ?>  
                <?php echo $form->error($model,'date_to'); ?>
            </div>
        </div>

	<div class="row buttons" style="padding-left: 159px;">
		        <?php $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType'=>'submit',
            'label'=>'Search',
            'type'=>'null', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
            'size'=>'small', // null, 'large', 'small' or 'mini'
            //'htmlOptions' => array('style' => 'margin-bottom: 10px; float: right;'),
        )); ?>	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->