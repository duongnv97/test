<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'price-support-form',
	'enableAjaxValidation'=>false,
        'htmlOptions' => array('enctype' => 'multipart/form-data'),
)); ?>

    <p class="note">Dòng có dấu <span class="required">*</span> là bắt buộc.</p>
    <?php echo MyFormat::BindNotifyMsg(); ?>
    
<!--    <div class="row">
        <?php // echo $form->labelEx($model,'type'); ?>
        <?php // echo $form->dropDownList($model,'type', $model->getArrayType(), array('empty'=>'select','class'=>'w-300')); ?>
        <?php // echo $form->error($model,'type'); ?>
    </div>-->
    <div class="row">
        <?php echo $form->labelEx($model,'date_apply'); ?>
        <?php echo $model->getDateApply(); ?>
        <?php echo $form->error($model,'date_apply'); ?>
    </div>
    <div class="row">
        <?php echo $form->labelEx($model,'status'); ?>
        <?php echo $form->dropDownList($model,'status', $model->getArrayStatus(), array('class'=>'w-300')); ?>
        <?php echo $form->error($model,'status'); ?>
    </div>
<!--    <div class="row">
        <?php // echo $form->labelEx($model,'date_apply'); ?>
         <?php 
//            $this->widget('zii.widgets.jui.CJuiDatePicker', array(
//                'model'=>$model,        
//                'attribute'=>'date_apply',
//                'options'=>array(
//                    'showAnim'=>'fold',
//                    'dateFormat'=> MyFormat::$dateFormatSearch,
//                    'changeMonth' => true,
//                    'changeYear' => true,
//                    'showOn' => 'button',
//                    'buttonImage'=> Yii::app()->theme->baseUrl.'/admin/images/icon_calendar_r.gif',
//                    'buttonImageOnly'=> true,                                
//                ),        
//                'htmlOptions'=>array(
//                    'class'=>'w-16',
//                    'style'=>'height:20px;',
//                        'readonly'=>'readonly',
//                ),
//            ));
            ?>  
        <?php // echo $form->error($model,'date_apply'); ?>
    </div>-->
    
    <div class="row ">
        <?php echo $form->labelEx($model,'customer_id'); ?>
        <?php echo $form->hiddenField($model,'customer_id'); ?>
        <?php
            // 1. limit search kh của sale
            $url = Yii::app()->createAbsoluteUrl('admin/ajax/search_by_role', array('role'=> ROLE_CUSTOMER));
            // widget auto complete search user customer and supplier
            $aData = array(
                'model'=>$model,
                'field_customer_id'=>'customer_id',
                'url'=> $url,
                'name_relation_user'=>'rCustomer',
                'ClassAdd' => 'w-300',
                'placeholder'=>'Nhập mã hoặc tên khách hàng',
            );
            $this->widget('ext.SpaAutocompleteCustomer.SpaAutocompleteCustomer',
                array('data'=>$aData));
            ?>
        <?php echo $form->error($model,'customer_id'); ?>
    </div>
    <div class="row">
        <?php echo $form->labelEx($model,'price_12'); ?>
        <?php echo $form->textField($model,'price_12',array('class'=>'w-200 ad_fix_currency', 'maxlength' => 10)); ?>
        <?php echo $form->error($model,'price_12'); ?>
    </div>
    <div class="row">
        <?php echo $form->labelEx($model,'price_45'); ?>
        <?php echo $form->textField($model,'price_45',array('class'=>'w-200 ad_fix_currency', 'maxlength' => 10)); ?>
        <?php echo $form->error($model,'price_45'); ?>
    </div>
    <div class="row">
        <?php echo $form->labelEx($model,'note'); ?>
        <?php echo $form->textArea($model,'note',array('class'=>'w-500', 'rows'=>5)); ?>
        <?php echo $form->error($model,'note'); ?>
    </div>
    
    <div class="row buttons" style="padding-left: 141px;">
            <?php $this->widget('bootstrap.widgets.TbButton', array(
                'buttonType'=>'submit',
                'label'=>$model->isNewRecord ? 'Create' :'Save',
                'type'=>'null', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
                'size'=>'small', // null, 'large', 'small' or 'mini'
                //'htmlOptions' => array('style' => 'margin-bottom: 10px; float: right;'),
            )); ?>	
    </div>

<?php $this->endWidget(); ?>

</div><!-- form -->

<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/autoNumeric-master/autoNumeric.js"></script>
<script>
    $(document).ready(function(){
        $('.form').find('button:submit').click(function(){
            $.blockUI({ overlayCSS: { backgroundColor: '#fff' } }); 
        });
        fnBindRemoveIcon();
        $('.remove_row_item').hide();
        fnInitInputCurrency();
    });
</script>