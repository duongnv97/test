<?php
$this->breadcrumbs=array(
	'QL Sale'=>array('index'),
	$model->first_name,
);

$menus = array(
	array('label'=>'Users Management', 'url'=>array('index')),
	array('label'=>'Create Users', 'url'=>array('create')),
	array('label'=>'Update Users', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete Users', 'url'=>array('delete'), 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
);
$this->menu= ControllerActionsName::createMenusRoles($menus, $actions);
?>

<h1>View Sale: <?php echo $model->first_name; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
        array(
			'label' => 'Họ Tên',
            'name' => 'first_name',                       
            //'htmlOptions' => array('style' => 'text-align:center;')
        ),	
		'address',
            array(
                'name'=>'province_id',
                'value'=>$model->province?$model->province->name:'',
            ),		
            array(
                'name'=>'district_id',
                'value'=>$model->district?$model->district->name:'',
            ),            
           
		'login_attemp',
		'created_date:datetime',
		'last_logged_in:datetime',
		'gender',
		'phone',
	),
)); ?>
