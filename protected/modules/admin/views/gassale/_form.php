<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/jquery.blockUI.js"></script>    
<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'users-form',
	'enableAjaxValidation'=>false,
)); ?>

	<?php echo Yii::t('translation', '<p class="note">Fields with <span class="required">*</span> are required.</p>'); ?>
	
	<div class="row">
		<?php echo Yii::t('translation', $form->labelEx($model,'first_name',array('label'=>'Họ Tên'))); ?>
		<?php echo $form->textField($model,'first_name',array('size'=>80,'maxlength'=>150)); ?>
		<?php echo $form->error($model,'first_name'); ?>
	</div>

	<div class="row">
		<?php echo Yii::t('translation', $form->labelEx($model,'address')); ?>
		<?php echo $form->textArea($model,'address',array('style'=>'width:450px')); ?>
		<?php echo $form->error($model,'address'); ?>
	</div>

    
	<div class="row">
		<?php echo Yii::t('translation', $form->labelEx($model,'province_id')); ?>
		<?php echo $form->dropDownList($model,'province_id', GasProvince::getArrAll(),array('class'=>'')); ?>
		<?php echo $form->error($model,'province_id'); ?>
	</div>    
    
	<div class="row">
		<?php echo Yii::t('translation', $form->labelEx($model,'district_id')); ?>
		<?php echo $form->dropDownList($model,'district_id', GasDistrict::getArrAll(),array('class'=>'')); ?>
		<?php echo $form->error($model,'district_id'); ?>
	</div>

	<div class="row">
		<?php echo Yii::t('translation', $form->labelEx($model,'status')); ?>
		<?php echo $form->dropDownList($model,'status', ActiveRecord::getUserStatus()); ?>
		<?php echo $form->error($model,'status'); ?>
	</div>

	<div class="row">
		<?php echo Yii::t('translation', $form->labelEx($model,'gender')); ?>
		<?php echo $form->dropDownList($model,'gender', Users::$gender); ?>
		<?php echo $form->error($model,'gender'); ?>
	</div>

	<div class="row">
		<?php echo Yii::t('translation', $form->labelEx($model,'phone')); ?>
		<?php echo $form->textField($model,'phone',array('size'=>20,'maxlength'=>20)); ?>
		<?php echo $form->error($model,'phone'); ?>
	</div>

	<div class="row buttons" style="padding-left: 115px;">
		        <?php $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType'=>'submit',
            'label'=>$model->isNewRecord ? Yii::t('translation', 'Create') : Yii::t('translation', 'Save'),
            'type'=>'null', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
            'size'=>'small', // null, 'large', 'small' or 'mini'
            //'htmlOptions' => array('style' => 'margin-bottom: 10px; float: right;'),
        )); ?>	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->

<script>
$(document).ready(function() {
    $('#Users_province_id').change(function(){
        var province_id = $(this).val();        
//        if($.trim(gender)==''){
//            $('#Products_category_id').html('<option value="">Select category</option>');            
//            return;
//        }                    
        var url_ = "<?php echo Yii::app()->createAbsoluteUrl('admin/gascustomer/create');?>";
//		$('#Products_category_id').html('<option value="">Select category</option>');            
    $.blockUI({ overlayCSS: { backgroundColor: '<?php echo BLOCK_UI_COLOR;?>' } }); 
        $.ajax({
            url: url_,
            data: {ajax:1,province_id:province_id},
            type: "get",
            dataType:'json',
            success: function(data){
                $('#Users_district_id').html(data['html_district']);
                $('#Users_storehouse_id').html(data['html_store']);
                $.unblockUI();
            }
        });
        
    });    
})

</script>
