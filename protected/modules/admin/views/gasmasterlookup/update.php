<?php
$this->breadcrumbs=array(
	'Gas Master Lookups'=>array('index'),
	$model->name=>array('view','id'=>$model->id),
	'Cập Nhật',
);

$menus = array(	
        array('label'=>'GasMasterLookup Management', 'url'=>array('index')),
	array('label'=>'Xem GasMasterLookup', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Tạo Mới GasMasterLookup', 'url'=>array('create')),	
);
$this->menu= ControllerActionsName::createMenusRoles($menus, $actions);

?>

<h1>Cập Nhật Master Lookup: <?php echo $model->name; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>