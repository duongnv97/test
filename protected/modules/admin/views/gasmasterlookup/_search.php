<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

    <div class="row">
        <?php echo $form->label($model,'name',array()); ?>
        <?php echo $form->textField($model,'name',array('size'=>60,'maxlength'=>200)); ?>
    </div>
    <div class="row">
        <?php echo $form->label($model,'type',array()); ?>
        <?php echo $form->dropDownList($model,'type', CmsFormatter::$MASTER_TYPE, array('empty'=>'Select')); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model,'type_lookup', []); ?>
        <?php echo $form->dropDownList($model,'type_lookup', $model->getArrayMasterType(), array('empty'=>'Select')); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model,'status',array()); ?>
        <?php echo $form->dropDownList($model,'status', ActiveRecord::getUserStatus(), array('empty'=>'Select')); ?>
    </div>

    <div class="row buttons" style="padding-left: 159px;">
                    <?php $this->widget('bootstrap.widgets.TbButton', array(
        'buttonType'=>'submit',
        'label'=>'Search',
        'type'=>'null', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
        'size'=>'small', // null, 'large', 'small' or 'mini'
        //'htmlOptions' => array('style' => 'margin-bottom: 10px; float: right;'),
    )); ?>	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->