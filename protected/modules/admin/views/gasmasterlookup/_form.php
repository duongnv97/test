<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'gas-master-lookup-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Dòng có dấu <span class="required">*</span> là bắt buộc.</p>
          	
        <?php if(Yii::app()->user->hasFlash('successUpdate')): ?>
            <div class="success_div"><?php echo Yii::app()->user->getFlash('successUpdate');?></div>         
        <?php endif; ?>  
	<div class="row">
            <?php echo $form->labelEx($model,'type_lookup'); ?>
            <?php echo $form->dropDownList($model,'type_lookup', $model->getArrayMasterType()); ?>
            <?php echo $form->error($model,'type_lookup'); ?>
	</div>
	<div class="row">
            <?php echo $form->labelEx($model,'name'); ?>
            <?php echo $form->textField($model,'name',array('size'=>90,'maxlength'=>200)); ?>
            <?php echo $form->error($model,'name'); ?>
	</div>
	<div class="row">
            <?php echo $form->labelEx($model,'type'); ?>
            <?php echo $form->dropDownList($model,'type', CmsFormatter::$MASTER_TYPE); ?>
            <?php echo $form->error($model,'type'); ?>
	</div>

        <?php
        $tmp_ = array();
        for($i=1;$i<100;$i++)
            $tmp_[$i]=$i;
        ?>
	<div class="row">
		<?php echo $form->labelEx($model,'display_order'); ?>
                <?php echo $form->dropDownList($model,'display_order',$tmp_); ?>
		<?php echo $form->error($model,'display_order'); ?>
	</div>        
        
	<div class="row">
		<?php echo Yii::t('translation', $form->labelEx($model,'status')); ?>
		<?php echo $form->dropDownList($model,'status', ActiveRecord::getUserStatus()); ?>
		<?php echo $form->error($model,'status'); ?>
	</div>
	<div class="row buttons" style="padding-left: 141px;">
		        <?php $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType'=>'submit',
            'label'=>$model->isNewRecord ? 'Create' :'Save',
            'type'=>'null', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
            'size'=>'small', // null, 'large', 'small' or 'mini'
            //'htmlOptions' => array('style' => 'margin-bottom: 10px; float: right;'),
        )); ?>	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->
<script>
    $(document).ready(function(){
        $('.form').find('button:submit').click(function(){
            $.blockUI({ overlayCSS: { backgroundColor: '#fff' } }); 
        });
        
    });
</script>