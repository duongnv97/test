<?php
$this->breadcrumbs=array(
	'Gas Master Lookups'=>array('index'),
	'Tạo Mới',
);

$menus = array(		
        array('label'=>'GasMasterLookup Management', 'url'=>array('index')),
);
$this->menu= ControllerActionsName::createMenusRoles($menus, $actions);
?>

<h1>Tạo Mới GasMasterLookup</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>