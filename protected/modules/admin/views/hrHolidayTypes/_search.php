
<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>
	 <div class="row">
        <?php echo $form->labelEx($model,'name'); ?>
        <?php echo $form->textField($model,'name',array('class'=>'')); ?>
        <?php echo $form->error($model,'name'); ?>
        </div>
        <div class="row">
            <?php echo $form->labelEx($model,'factor'); ?>
            <?php echo $form->textField($model,'factor',array('class'=>'number_only_v1')); ?>
            <?php echo $form->error($model,'factor'); ?>
        </div>
        <div class="row">
            <?php echo $form->labelEx($model,'status'); ?>
            <?php echo $form->dropDownList($model,'status', $model->getArrayStatus(),array('empty'=>'Select','class'=>'w-300')); ?>
            <?php echo $form->error($model,'status'); ?>
        </div>
	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->