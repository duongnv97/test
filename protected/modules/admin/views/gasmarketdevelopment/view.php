<?php
$model->modelUser = Users::model()->findByPk($model->customer_id);
$this->breadcrumbs=array(
	'Phát Triển Thị Trường'=>array('index'),
	($model->modelUser->first_name),
);

$menus = array(
	array('label'=>'Phát Triển Thị Trường', 'url'=>array('index')),
	array('label'=>'Tạo Mới Phát Triển Thị Trường', 'url'=>array('create')),
	array('label'=>'Cập Nhật Phát Triển Thị Trường', 'url'=>array('update', 'id'=>$model->id)),
//	array('label'=>'Xóa Phát Triển Thị Trường', 'url'=>array('delete'), 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
);
$this->menu= ControllerActionsName::createMenusRoles($menus, $actions);
?>
<?php $cmsFormat = new CmsFormatter(); ?>
<h1>Xem Phát Triển Thị Trường: <?php echo $cmsFormat->formatNameUser($model->modelUser); ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
            'code_no',
            array(
                'name' => 'agent_id',
                'value'=>$model->agent?$model->agent->first_name:'',
            ), 	
            array(
                'label'=>'Mã Khách Hàng',
                'name'=>'customer_name',
                'value'=>$model->customer?$model->customer->code_bussiness:'',
            ),               
             array(
                'label'=>'Mã Hệ Thống',
                'name'=>'customer_name',
                'value'=>$model->customer?$model->customer->code_account:'',
            ),                
            array(
                'name'=>'customer_name',
                'type'=>'NameUser',
                'value'=>$model->customer?$model->customer:0,
            ),               
            array(
                'name'=>'customer_address',
                'type'=>'AddressTempUser',
                'value'=>$model->customer?$model->customer:'',
				'visible'=>Yii::app()->user->role_id==ROLE_ADMIN,
            ),               
            array(
                'name'=>'customer_phone',
                'value'=>$model->customer?$model->customer->phone:'',
				'visible'=>Yii::app()->user->role_id==ROLE_ADMIN,
            ),               
          
		'maintain_date:date',
        array(
            'name' => 'monitoring_id',
			'value'=>$model->monitoring?$model->monitoring->first_name:'',
        ), 		
        array(
            'name' => 'maintain_employee_id',
			'value'=>$model->maintain_employee?$model->maintain_employee->first_name:'',
        ), 		
            array(
                'name'=>'materials_id',
                'value'=>$model->materials?$model->materials->name:'',
            ),               
		'seri_no',
                             
		'note',
            array(
                'name'=>'status',
                'value'=>  CmsFormatter::$STATUS_MAINTAIN[$model->status],
            ),   
            'note_update_status:html',			
		'created_date:Datetime',
	),
)); ?>
