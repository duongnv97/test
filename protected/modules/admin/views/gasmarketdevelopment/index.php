<?php
$this->breadcrumbs=array(
	'Phát Triển Thị Trường',
);

$menus=array(
	array('label'=> Yii::t('translation','Tạo Mới Phát Triển Thị Trường'), 'url'=>array('create')),
);
$this->menu= ControllerActionsName::createMenusRoles($menus, $actions);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){

	$.fn.yiiGridView.update('gas-maintain-grid', {
                url : $(this).attr('action'),
		data: $(this).serialize()
	});
	return false;
});
");

//    $('#gas-maintain-grid').find('.keys').attr('title', '" . Yii::app()->request->url . "');

Yii::app()->clientScript->registerScript('ajaxupdate', "
$('#gas-maintain-grid a.ajaxupdate').live('click', function() {
    $.fn.yiiGridView.update('gas-maintain-grid', {
        type: 'POST',
        url: $(this).attr('href'),
        success: function() {
            $.fn.yiiGridView.update('gas-maintain-grid');
        }
    });
    return false;
});
");
?>

<h1><?php echo Yii::t('translation', 'Phát Triển Thị Trường'); ?></h1>

<?php echo CHtml::link(Yii::t('translation','Tìm Kiếm Nâng Cao'),'#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'gas-maintain-grid',
	'dataProvider'=>$model->search(),
        'afterAjaxUpdate'=>'function(id, data){ fnUpdateColorbox();}',
//	'enableSorting' => false,
        'template'=>'{pager}{summary}{items}{pager}{summary}', 
        'pager' => array(
            'maxButtonCount'=>  CmsFormatter::$PAGE_MAX_BUTTON,
        ),    
    
	//'filter'=>$model,
	'columns'=>array(
        array(
            'header' => 'S/N',
            'type' => 'raw',
            'value' => '$this->grid->dataProvider->pagination->currentPage * $this->grid->dataProvider->pagination->pageSize + ($row+1)',
            'headerHtmlOptions' => array('width' => '10px','style' => 'text-align:center;'),
            'htmlOptions' => array('style' => 'text-align:center;')
        ),
        array(
            'name' => 'code_no',
            'htmlOptions' => array('style' => 'text-align:center;width:50px;')
        ),               
        array(
            'name' => 'agent_id',
            'value' => '$data->agent?$data->agent->first_name:""',
            'visible'=>Yii::app()->user->role_id!=ROLE_SUB_USER_AGENT,
            'htmlOptions' => array('style' => 'text-align:center;width:50px;')
        ),               
        array(
            'name' => 'maintain_date',
            'type'=>'date',
            'htmlOptions' => array('style' => 'text-align:center;width:50px;')
        ),             
        array(
            'header' => 'Mã KH',
            'value' => '$data->customer?$data->customer->code_bussiness:""',
//            'htmlOptions' => array('style' => 'text-align:left;width:350px;')
        ),     
            array(                
                'name' => 'customer_id',
                'type'=>'NameUser',
                'value'=>'$data->customer?$data->customer:0',
//                'htmlOptions' => array('style' => 'width:50px;')
            ),     
            
        array(
            'name' => 'customer_address',
            'type'=>'AddressTempUser',// kiểu này lấy address build sẵn, sẽ nhanh hơn kiểu dưới rất nhiều, nhưng nếu change tên phường hoặc quận thì ko thay đổi
//            'type'=>'PrimaryAddress', // kiểu này là mỗi lần phải find lại address cập nhật trực tiếp ở db
            'value' => '$data->customer?$data->customer:""',
//            'visible'=>Yii::app()->user->role_id==ROLE_ADMIN,
            'htmlOptions' => array('style' => 'text-align:left;width:80px;')
        ),             
        array(
            'name' => 'customer_phone',
            'value' => '$data->customer?$data->customer->phone:""',
			//'visible'=>Yii::app()->user->role_id!=ROLE_AGENT,
			'visible'=>Yii::app()->user->role_id==ROLE_ADMIN,
            'htmlOptions' => array('style' => 'text-align:center;width:60px;')
        ),           
		
        array(
            'name' => 'monitoring_id',
            'value' => '$data->monitoring?$data->monitoring->first_name:""',
            'htmlOptions' => array('style' => 'text-align:center;width:100px;')
        ),             
        array(
            'name' => 'maintain_employee_id',
            'value' => '$data->maintain_employee?$data->maintain_employee->first_name:""',
            'htmlOptions' => array('style' => 'text-align:center;width:100px;')
        ),             
	 
        array(
            'name' => 'materials_id',
            'value' => '$data->materials?$data->materials->name:$data->materials_name',
//            'htmlOptions' => array('style' => 'text-align:left;width:350px;')
        ),             
		 Yii::t('translation','seri_no'),
//        array(
//            'name' => 'note',
//            'type'=>'html',
//            'htmlOptions' => array('style' => 'width:130px;')
//        ),             
            
//        array(
//            'name' => 'status',
//            'type'=>'StatusMaintain',
////            'value'=>'CmsFormatter::$STATUS_MAINTAIN[$data->status]',
//            'htmlOptions' => array('style' => 'text-align:center;width:70px;font-weight:bold;')
//        ),              
//        array(
//            'name' => 'status',
//            'type'=>'HadSelling',
//            'value'=>'$data',
//            'htmlOptions' => array('style' => 'display:none;'),
//            'headerHtmlOptions' => array('style' => 'display: none;'),
//        ),  		
//        array(
//            'name' => 'note_update_status',
//            'type'=>'html',
//            'htmlOptions' => array('style' => 'width:80px;')
//        ), 		
        array(
            'name' => 'created_date',
            'type' => 'Datetime',
            'htmlOptions' => array('style' => 'width:50px;')
        ),     
            
        array(
            'header' => 'Actions',
            'class'=>'CButtonColumn',
//            'template'=> ControllerActionsName::createIndexButtonRoles($actions, array('update_status_market_development','view','update','delete')),
            'template'=> ControllerActionsName::createIndexButtonRoles($actions, array('view','update','delete')),
            'buttons'=>array(
            'update_status_market_development'=>array(
                'label'=>'Cập nhật trạng thái phát triển thị trường',
                'imageUrl'=>Yii::app()->theme->baseUrl . '/admin/images/edit.png',
                'options'=>array('class'=>'update_status_market_development'),
                'url'=>'Yii::app()->createAbsoluteUrl("admin/gasmarketdevelopment/update_status_market_development",
                    array("maintain_id"=>$data->id,
                    "order_id"=>$data->id) )',
                 'visible'=>'( Yii::app()->user->role_id == ROLE_ADMIN ||  (int)$data->update_num < (int)Yii::app()->params["limit_update_maintain"] ||  $data->status==STATUS_NOT_YET_CALL )', 
                ),
                'update'=>array(
//                    'visible'=>'( Yii::app()->user->role_id == ROLE_ADMIN || (int)$data->update_num < (int)Yii::app()->params["limit_update_maintain"] )',
                    'visible'=>  'MyFunctionCustom::agentCanUpdateMarketDevelopment($data)',
                ),
                'delete'=>array(
                    'visible'=> 'GasCheck::canDeleteData($data)',
                ),
            ),            
        ),
    ),
)); ?>

<style>   
</style>

<script>
$(document).ready(function() {
    fnUpdateColorbox();
    fnFixScrollHeaderTable();
});

function fnUpdateColorbox(){    
    $(".update_status_market_development").colorbox({iframe:true,innerHeight:'450', innerWidth: '850',close: "<span title='close'>close</span>"});
    $(".view").colorbox({iframe:true,innerHeight:'550', innerWidth: '900',close: "<span title='close'>close</span>"});
    $('.HadSelling').each(function(){
        var tr_parent = $(this).parent('td').parent('tr');    
        var html_a_view = tr_parent.find('.HadSelling').html();
        tr_parent.css({'background':'#5FB404'});
        tr_parent.find('td:last').append(html_a_view);
    });
    fixTargetBlank();
    $(".view_selling").colorbox({iframe:true,innerHeight:'700', innerWidth: '900',close: "<span title='close'>close</span>"});    
}

function fnFixScrollHeaderTable(){
    return;// chỉ sử dụng cho table có số cột cố định
    $(window).scroll(function(){
      var tableItem = $('.grid-view').eq(0).find('.items').eq(0);
      var tHead = $('.grid-view').eq(0).find('.items').eq(0).find('thead');
      var h = tableItem.offset().top;
      var top = $(window).scrollTop();
      if( top > h ) // height of float header
       tHead.addClass("stick");
      else
       tHead.removeClass("stick");
     });    
}

</script>



<script>
    $(document).ready(function(){
//		$('#GasMarketDevelopment_monitoring_id').live('change',function(){			
//			var monitoring_id = $(this).val();        
//			var url_ = "<?php echo Yii::app()->createAbsoluteUrl('admin/ajax/get_slt_monitoring_employee');?>";
//			$.blockUI({ overlayCSS: { backgroundColor: '<?php echo BLOCK_UI_COLOR;?>' } }); 
//			$.ajax({
//				url: url_,
//				data: {ajax:1,monitoring_id:monitoring_id},
//				type: "get",
//				dataType:'json',
//				success: function(data){
//					$('#GasMarketDevelopment_maintain_employee_id').html(data['html']);                
//					$.unblockUI();
//				}
//			});
//			
//		});   	

	});
    
  
</script>