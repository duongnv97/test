<?php
$this->breadcrumbs=array(
	Yii::t('translation','Phát Triển Thị Trường')=>array('index'),
	Yii::t('translation','Create'),
);

$menus = array(		
        array('label'=> Yii::t('translation', 'Phát Triển Thị Trường') , 'url'=>array('index')),
);
$this->menu= ControllerActionsName::createMenusRoles($menus, $actions);
?>

<h1><?php echo Yii::t('translation', 'Tạo Mới Phát Triển Thị Trường'); ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>