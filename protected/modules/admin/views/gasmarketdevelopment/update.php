<?php
$this->breadcrumbs=array(
	Yii::t('translation','Phát Triển Thị Trường')=>array('index'),
	($model->modelUser->first_name)=>array('view','id'=>$model->id),
	Yii::t('translation','Cập Nhật'),
);

$menus = array(	
        array('label'=> Yii::t('translation', 'Phát Triển Thị Trường'), 'url'=>array('index')),
	//array('label'=> Yii::t('translation', 'Xem Phát Triển Thị Trường'), 'url'=>array('view', 'id'=>$model->id)),
	array('label'=> Yii::t('translation', 'Tạo Mới Phát Triển Thị Trường'), 'url'=>array('create')),	
);
$this->menu= ControllerActionsName::createMenusRoles($menus, $actions);

?>
<?php $cmsFormat = new CmsFormatter(); ?>
<h1><?php echo Yii::t('translation', 'Cập Nhật Phát Triển Thị Trường:['.$model->modelUser->code_bussiness.'] '.$cmsFormat->formatNameUser($model->modelUser)); ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>