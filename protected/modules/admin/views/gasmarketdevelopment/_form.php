<div class="form huongminh">
<?php
//$gridCss = Yii::app()->getAssetManager()->publish(Yii::getPathOfAlias('zii.widgets.assets')).'/gridview/styles.css';
//Yii::app()->getClientScript()->registerCssFile($gridCss);
?>

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'gas-maintain-form',
	'enableAjaxValidation'=>false,
)); ?>

<?php //echo $form->errorSummary($model); ?>
<?php //echo $form->errorSummary($model->modelUser); ?>
	<p class="note">Dòng có dấu <span class="required">*</span> là bắt buộc.</p>
        <?php if(Yii::app()->user->hasFlash('successUpdate')): ?>
            <div class="success_div"><?php echo Yii::app()->user->getFlash('successUpdate');?></div>         
        <?php endif; ?>     
	<h3 class='title-info'>Thông Tin Khách Hàng</h3>

        <div class="row radio-list-2" style=''>    
			<label>&nbsp;</label>
            <?php echo $form->radioButtonList($model,'is_new_customer', CmsFormatter::$CUSTOMER_NEW_OLD, 
				array('separator'=>"",'class'=>'select_customer' )); ?>            
        </div>
	<div class='clr'></div>
	<div class='customer customer_old customer_2'>
		<div class="row">
                    <?php echo Yii::t('translation', $form->labelEx($model,'customer_id')); ?>
                    <?php echo $form->hiddenField($model,'customer_id'); ?>
                    <?php 
                            $this->widget('zii.widgets.jui.CJuiAutoComplete', array(
                                    'attribute'=>'autocomplete_name',
                                    'model'=>$model,
                                    'sourceUrl'=>Yii::app()->createAbsoluteUrl('admin/site/AutocompleteCustomerPTTT'),
                    //                                'name'=>'my_input_name',
                                    'options'=>array(
                                            'minLength'=>MIN_LENGTH_AUTOCOMPLETE,
                                            'multiple'=> true,
                                'search'=>"js:function( event, ui ) {
                                        $('#GasMarketDevelopment_autocomplete_name').addClass('grid-view-loading-gas');
                                        } ",
                                'response'=>"js:function( event, ui ) {
                                        var json = $.map(ui, function (value, key) { return value; });
                                        if(json.length<1){
                                            var error = '<div class=\'errorMessage clr autocomplete_name_text_customer\' style=\'padding-left: 145px; \'>Không tìm thấy dữ liệu.</div>';
                                            if($('.autocomplete_name_text_customer').size()<1)
                                                    $('.autocomplete_name_error_customer').after(error);
                                            else
                                                $('.autocomplete_name_error_customer').parent('div').find('.autocomplete_name_text_customer').show();
                                            $('.autocomplete_name_error_customer').parent('div').find('.remove_row_item').hide();                                            
                                        }
                                        $('#GasMarketDevelopment_autocomplete_name').removeClass('grid-view-loading-gas');
                                        } ",
                                        
                                        'select'=>"js:function(event, ui) {
                                                $('#GasMarketDevelopment_customer_id').val(ui.item.id);
                                                var remove_div = '<span class=\'remove_row_item\' onclick=\'fnRemoveNameCustomer(this)\'></span>';
                                                $('#GasMarketDevelopment_autocomplete_name').parent('div').find('.remove_row_item').remove();
                                                $('#GasMarketDevelopment_autocomplete_name').attr('readonly',true).after(remove_div);
                                                fnBuildTableInfoCustomer(ui.item);
                                                $('.autocomplete_customer_info').show();
                                                $('.autocomplete_name_error_customer').parent('div').find('.autocomplete_name_text_customer').hide();
                                        }",
                                    ),
                                    'htmlOptions'=>array(
                                        'class'=>'autocomplete_name_error_customer',
                                        'size'=>45,
                                        'maxlength'=>45,
                                        'style'=>'float:left;',
                                        'placeholder'=>'Nhập tên, số ĐT, Địa chỉ hoặc mã KH',
                                    ),
                            )); 
                            ?>        
                            <script>
                                function fnRemoveNameCustomer(this_){
                                    $(this_).prev().attr("readonly",false); 									
                                    $("#GasMarketDevelopment_autocomplete_name").val("");
                                    $("#GasMarketDevelopment_customer_id").val("");
                                    $('.autocomplete_customer_info').hide();
                                }
                            function fnBuildTableInfoCustomer(item){
                                $(".info_name").text(item.name_customer);
                                $(".info_name_agent").text(item.name_agent);
                                $(".info_code_account").text(item.code_account);
                                $(".info_code_bussiness").text(item.code_bussiness);
                                $(".info_address").text(item.address);
                                $(".info_phone").text(item.phone);
                            }

                            </script>        
                            <div class="clr"></div>		
                    <?php echo $form->error($model,'customer_id'); ?>
                    <?php $display='display:inline;';
                        $info_name ='';
                        $info_name_agent ='';
                        $info_address ='';
                        $info_code_account ='';
                        $info_code_bussiness ='';
                        $info_phone ='';
                            if(empty($model->customer_id)) $display='display: none;';
                            else{
                                $info_name = $model->customer->first_name;
                                $info_name_agent = $model->customer->name_agent;
                                $info_code_account = $model->customer->code_account;
                                $info_code_bussiness = $model->customer->code_bussiness;
                                $info_address = $model->customer->address;
                                $info_phone = $model->customer->phone;
                            }
                    ?>                        
                    <div class="autocomplete_customer_info" style="<?php echo $display;?>">
                    <table>
                        <tr>
                            <td class="_l">Mã khách hàng:</td>
                            <td class="_r info_code_bussiness"><?php echo $info_code_bussiness;?></td>
                        </tr>

                        <tr>
                            <td class="_l">Tên khách hàng:</td>
                            <td class="_r info_name"><?php echo $info_name;?></td>
                        </tr>                    
                        <tr>
                            <td class="_l">Địa chỉ:</td>
                            <td class="_r info_address"><?php echo $info_address;?></td>
                        </tr>
                        <tr>
                            <td class="_l">Điện Thoại:</td>
                            <td class="_r info_phone"><?php echo $info_phone;?></td>
                        </tr>


                    </table>
                </div>
                <div class="clr"></div>    		
                <?php // echo $form->error($model,'customer_id'); ?>
            </div>	
	
	</div> <!-- end <div class='customer_old'> -->
	
	
	<div class='customer customer_new customer_1'>
	
		<div class="row">
                        <?php echo $form->labelEx($model->modelUser,'first_name',array('label'=>'Tên Khách Hàng')); ?>
                        <?php echo $form->dropDownList($model->modelUser,'last_name', CmsFormatter::$SALUTATION,array('style'=>'width:120px;','empty'=>'Chọn Xưng Hô')); ?>
                        <?php echo $form->textField($model->modelUser,'first_name',array('style'=>'width:245px','maxlength'=>80)); ?>
                        <em>Tên Khách Hàng Không Được Bao Gồm Số</em>
                        <?php echo $form->error($model->modelUser,'last_name'); ?>
                        <?php echo $form->error($model->modelUser,'first_name'); ?>					
		</div>
		<div class="row">
                        <?php echo $form->labelEx($model->modelUser,'phone') ?>
                        <?php echo $form->textField($model->modelUser,'phone',array('class'=>'phone_number_only no_phone_number_text ', 'style'=>'width:198px','maxlength'=>20)); ?>
                        <div class="add_new_item">
                                <input type="checkbox" class="no_phone_number" ><em>Không có số điện thoại</em>
                        </div>
                        <?php echo $form->error($model->modelUser,'phone'); ?>
		</div> 
		<div class="row">
			<?php echo Yii::t('translation', $form->labelEx($model->modelUser,'province_id')); ?>
			<?php echo $form->dropDownList($model->modelUser,'province_id', GasProvince::getArrAll(),array('style'=>'width:376px','class'=>'','empty'=>'Select')); ?>
			<?php echo $form->error($model->modelUser,'province_id'); ?>
		</div> 	

		<div class="row">
			<?php echo Yii::t('translation', $form->labelEx($model->modelUser,'district_id')); ?>
			<?php echo $form->dropDownList($model->modelUser,'district_id', GasDistrict::getArrAll($model->modelUser->province_id, $model->modelUser->id),array('style'=>'width:376px','empty'=>'Select')); ?>
                        <div class="add_new_item"><a class="iframe_create_district" href="<?php echo Yii::app()->createAbsoluteUrl('admin/ajax/iframe_create_district') ;?>">Tạo Mới Quận Huyện</a><em> (Nếu trong danh sách không có)</em></div>
			<?php echo $form->error($model->modelUser,'district_id'); ?>
		</div>

		<div class="row">                        
			<?php echo Yii::t('translation', $form->labelEx($model->modelUser,'ward_id')); ?>
			<?php echo $form->dropDownList($model->modelUser,'ward_id', GasWard::getArrAll($model->modelUser->province_id, $model->modelUser->district_id),array('style'=>'width:376px','empty'=>'Select')); ?>
                        <div class="add_new_item"><a class="iframe_create_ward" href="<?php echo Yii::app()->createAbsoluteUrl('admin/ajax/iframe_create_ward') ;?>">Tạo Mới Phường Xã</a><em> (Nếu trong danh sách không có)</em></div>
			<?php echo $form->error($model->modelUser,'ward_id'); ?>
		</div>	
		
		<div class="row">
                        <?php echo $form->labelEx($model->modelUser,'house_numbers') ?>
                        <?php echo $form->textField($model->modelUser,'house_numbers',array('style'=>'width:369px','maxlength'=>100)); ?>
                        <?php echo $form->error($model->modelUser,'house_numbers'); ?>
		</div> 	
		
		<div class="row">
                    <?php echo $form->labelEx($model->modelUser,'street_id') ?>
                    <?php echo $form->hiddenField($model->modelUser,'street_id',array('style'=>'width:369px','maxlength'=>100)); ?>
                    <?php 
                            $this->widget('zii.widgets.jui.CJuiAutoComplete', array(
                                            'attribute'=>'autocomplete_name_street',
                                            'model'=>$model,
                                            'sourceUrl'=>Yii::app()->createAbsoluteUrl('admin/ajax/autocomplete_data_streets'),
                                            'options'=>array(
                                                            'minLength'=>MIN_LENGTH_AUTOCOMPLETE,
                                                            'multiple'=> true,
                                                            'search'=>"js:function( event, ui ) {
                                                                    $('#GasMarketDevelopment_autocomplete_name_street').addClass('grid-view-loading-gas');
                                                                    } ",
                                                            'response'=>"js:function( event, ui ) {
                                                                    var json = $.map(ui, function (value, key) { return value; });
                                                                            if(json.length<1){
                                                                                var error = '<div class=\'errorMessage clr autocomplete_name_text\'>Không tìm thấy dữ liệu.</div>';
                                                                                if($('.autocomplete_name_text').size()<1)
                                                                                        $('.autocomplete_name_error').parent('div').find('.add_new_item').after(error);
                                                                                else
                                                                                        $('.autocomplete_name_error').parent('div').find('.autocomplete_name_text').show();
                                                                                $('.autocomplete_name_error').parent('div').find('.remove_row_item').hide();
                                                                            }
                                                                            $('#GasMarketDevelopment_autocomplete_name_street').removeClass('grid-view-loading-gas');
                                                                            } ",
                                                            'select'=>"js:function(event, ui) {
                                                                            $('#Users_street_id').val(ui.item.id);
                                                                            var remove_div = '<span class=\'remove_row_item\' onclick=\'fnRemoveName(this)\'></span>';
                                                                            $('#GasMarketDevelopment_autocomplete_name_street').parent('div').find('.remove_row_item').remove();
                                                                            $('#GasMarketDevelopment_autocomplete_name_street').attr('readonly',true).after(remove_div);
                                                                            $('.autocomplete_name_error').parent('div').find('.autocomplete_name_text').hide();
                                                            }",
                                            ),
                                            'htmlOptions'=>array(
                                                    'class'=>'autocomplete_name_error',
                                                    'size'=>45,
                                                    'maxlength'=>45,
                                                    'style'=>'float:left;width:369px;',
                                                    'placeholder'=>'Nhập tên đường tiếng việt không dấu',                            
                                            ),
                            )); 
                            ?> 
                            <script>
                                    function fnRemoveName(this_){
                                            //$(this_).prev().attr("readonly",false); 
                                            $(this_).parent('div').find('input').attr("readonly",false); 
                                            $("#GasMarketDevelopment_autocomplete_name_street").val("");
                                            $("#Users_street_id").val("");
                                    }
                                    function fnAddReadonlyStreet(){
                                            <?php if(!$model->isNewRecord):?>
                                            var remove_div = '<span class=\'remove_row_item\' onclick=\'fnRemoveName(this)\'></span>';
                                            $('#GasMarketDevelopment_autocomplete_name_street').parent('div').find('.remove_row_item').remove();
                                            $('#GasMarketDevelopment_autocomplete_name_street').attr('readonly',true).after(remove_div);
                                            <?php endif;?>
                                    }					
                            </script>             
                            <div class="add_new_item"><a class="iframe_create_street" href="<?php echo Yii::app()->createAbsoluteUrl('admin/ajax/iframe_create_street') ;?>">Tạo Mới Đường</a><em> (Nếu trong danh sách không có)</em></div>
                    <div class="clr"></div>
                    <?php echo $form->error($model->modelUser,'street_id'); ?>
		</div> 	
	</div> <!-- end <div class='customer_new'> -->
	
	<h3 class='title-info'>Thông Tin Phát Triển Thị Trường</h3>
	<div class="row">
		<?php echo Yii::t('translation', $form->labelEx($model,'maintain_date')); ?>
                <?php 
                    $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                        'model'=>$model,        
                        'attribute'=>'maintain_date',
                        'options'=>array(
                            'showAnim'=>'fold',
                            'dateFormat'=> ActiveRecord::getDateFormatJquery(),
//                            'minDate'=> '0',
							'maxDate'=> '0',
                            'changeMonth' => true,
                            'changeYear' => true,
                            'showOn' => 'button',
                            'buttonImage'=> Yii::app()->theme->baseUrl.'/admin/images/icon_calendar_r.gif',
                            'buttonImageOnly'=> true,                                
                        ),        
                        'htmlOptions'=>array(
                            'class'=>'w-16',
                            'style'=>'height:20px;',
                                'readonly'=>'readonly',
                        ),
                    ));
                ?>     		
		<?php echo $form->error($model,'maintain_date'); ?>
	</div>

	<div class="row">
		<?php echo Yii::t('translation', $form->labelEx($model,'monitoring_id')); ?>
		<?php  echo $form->dropDownList($model,'monitoring_id', Users::getSelectByRole(ROLE_MONITORING_MARKET_DEVELOPMENT),array('style'=>'width:376px;','empty'=>'Select')); ?>
		<?php echo $form->error($model,'monitoring_id'); ?>
	</div>
	
	<div class="row">
		<?php echo Yii::t('translation', $form->labelEx($model,'maintain_employee_id')); ?>
		<?php echo $form->dropDownList($model,'maintain_employee_id', Users::getSelectByRoleForAgent($model->monitoring_id, ONE_MONITORING_MARKET_DEVELOPMENT, $model->agent_id, array('status'=>1,'form_create'=>1)),array('class'=>'category_ajax', 'style'=>'width:376px;','empty'=>'Select')); ?>		
		<?php echo $form->error($model,'maintain_employee_id'); ?>
	</div>

	<div class="row">
		<?php echo Yii::t('translation', $form->labelEx($model,'accounting_employee_id')); ?>
		<?php echo $form->dropDownList($model,'accounting_employee_id', Users::getSelectByRoleForAgent(Yii::app()->user->parent_id, ONE_AGENT_ACCOUNTING, $model->agent_id, array('status'=>1)),array('class'=>'category_ajax', 'style'=>'width:376px;','empty'=>'Select')); ?>		
		<?php echo $form->error($model,'accounting_employee_id'); ?>
	</div>

	<div class="row">
		<?php echo Yii::t('translation', $form->labelEx($model,'materials_id')); ?>
		<?php // $dataCat = CHtml::listData(GasMaterials::getCatLevel2(GasMaterials::$MATERIAL_GAS),'id','name','group'); ?>
		<?php echo $form->hiddenField($model,'materials_id'); ?>		
                <?php 
                    // widget auto complete search material
                    $aData = array(
                        'model'=>$model,
                        'name_relation_material'=>'materials',
                    );
                    $this->widget('ext.GasAutocompleteMaterial.GasAutocompleteMaterial',
                        array('data'=>$aData));                                        
                ?>		
		<?php echo $form->error($model,'materials_id'); ?>
	</div>

	<div class="row">
		<?php echo Yii::t('translation', $form->labelEx($model,'seri_no')); ?>
		<?php echo $form->textField($model,'seri_no',array('style'=>'width:366px','maxlength'=>20)); ?>
		<?php echo $form->error($model,'seri_no'); ?>
	</div>

	<div class="row">
		<?php echo Yii::t('translation', $form->labelEx($model,'note')); ?>
		<?php echo $form->textArea($model,'note',array('rows'=>3,'style'=>'width:370px','maxlength'=>500)); ?>
		<?php echo $form->error($model,'note'); ?>
	</div>

	<div class="row buttons" style="padding-left: 141px;">
		        <?php $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType'=>'submit',
            'label'=>$model->isNewRecord ? Yii::t('translation', 'Create') : Yii::t('translation', 'Save'),
            'type'=>'null', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
            'size'=>'small', // null, 'large', 'small' or 'mini'
            //'htmlOptions' => array('style' => 'margin-bottom: 10px; float: right;'),
        )); ?>	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->

<script>
    $(document).ready(function(){
	
        $('.form').find('button:submit').click(function(){
            $.blockUI({ overlayCSS: { backgroundColor: '<?php echo BLOCK_UI_COLOR;?>' } }); 
        });
        
        <?php if($model->modelUser->phone==NO_PHONE_NUMBER):?>
            $('.no_phone_number').trigger('click');
            if($('.no_phone_number').is(':checked'))
                $('.no_phone_number').closest('div.row').find('.no_phone_number_text').attr('readonly', true);
        <?php endif;?>
        
    $('#Users_province_id').live('change',function(){
        var province_id = $(this).val();        
        var url_ = "<?php echo Yii::app()->createAbsoluteUrl('admin/ajax/get_slt_district');?>";
        $.blockUI({ overlayCSS: { backgroundColor: '<?php echo BLOCK_UI_COLOR;?>' } }); 
        $.ajax({
            url: url_,
            data: {ajax:1,province_id:province_id},
            type: "get",
            dataType:'json',
            success: function(data){
                $('#Users_district_id').html(data['html_district']);                
                $.unblockUI();
            }
        });
        
    });   		
	
		$('#Users_district_id').live('change',function(){
			var province_id = $('#Users_province_id').val();   
			var district_id = $(this).val();        
			var url_ = "<?php echo Yii::app()->createAbsoluteUrl('admin/ajax/get_slt_ward');?>";
			$.blockUI({ overlayCSS: { backgroundColor: '<?php echo BLOCK_UI_COLOR;?>' } }); 
			$.ajax({
				url: url_,
				data: {ajax:1,district_id:district_id,province_id:province_id},
				type: "get",
				dataType:'json',
				success: function(data){
					$('#Users_ward_id').html(data['html_district']);                
					$.unblockUI();
				}
			});
			
		});   		
		
		$('#GasMarketDevelopment_monitoring_id').live('change',function(){			
			var monitoring_id = $(this).val();        
			var url_ = "<?php echo Yii::app()->createAbsoluteUrl('admin/ajax/get_slt_monitoring_employee');?>";
			$.blockUI({ overlayCSS: { backgroundColor: '<?php echo BLOCK_UI_COLOR;?>' } }); 
			$.ajax({
				url: url_,
				data: {ajax:1,monitoring_id:monitoring_id},
				type: "get",
				dataType:'json',
				success: function(data){
					$('#GasMarketDevelopment_maintain_employee_id').html(data['html']);                
					$.unblockUI();
				}
			});
			
		});   	
		
		fnUpdateColorbox();
		fnAddReadonlyStreet();
		fnSelectCustomer();
		
		fnShowHideCustomer();
	});
    
    function fnUpdateColorbox(){  
        $(".iframe_create_district").colorbox({iframe:true,innerHeight:'400', innerWidth: '700',close: "<span title='close'>close</span>"});
        $(".iframe_create_ward").colorbox({iframe:true,innerHeight:'400', innerWidth: '700',close: "<span title='close'>close</span>"});
        $(".iframe_create_street").colorbox({iframe:true,innerHeight:'400', innerWidth: '700',close: "<span title='close'>close</span>"});
        
    }
	
	function fnSelectCustomer(){
		$('.select_customer').click(function(){
			var type = $(this).val();
			$('.customer').hide();
			$('.customer_'+type).show();
		});
	}
	
	function fnShowHideCustomer(){
	//return; // need open khi phát triển thị trường đi lượt 2
		$('.customer').hide();
		<?php if($model->isNewRecord):?>			
			$('.customer_<?php echo $model->is_new_customer;?>').show();
		<?php else:?>	
			$('.customer_1').show();
			$('.radio-list-2').remove();
		<?php endif;?>	
	}
    
</script>
