<center><h1>Cập nhật lý do hủy đơn</h1></center>
<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'borrow-form',
	'enableAjaxValidation'=>false,
        'htmlOptions' => array('enctype' => 'multipart/form-data'),
)); ?>
    <?php echo MyFormat::BindNotifyMsg(); ?>
    <div class="row">
        <?php echo $form->label($model,'code_no'); ?>
        <?php echo $model->getCodeNo(). " - {$model->getDateOnly()}"  ; ?>
    </div>
    <div class="row">
        <label><?php echo "Khách hàng"; ?></label>
        <?php echo $model->getCustomer('first_name'). " - ĐT: {$model->getPhone()}"; ?>
    </div>
    <div class="row">
        <label><?php echo "Địa chỉ"; ?></label>
        <?php echo $model->getCustomer('address'); ?>
    </div>
    
    <div class="row">
        <?php echo $form->label($model,'comment_cancel', ['label' => 'Tổng đài cập nhật']); ?>
        <?php //echo $form->textArea($model,'note',array('rows'=>6,'cols'=>80,"placeholder"=>"Ghi chú", 'hidden'=>'hidden'));?>
        <textarea rows="6" cols="70" placeholder="Lí do hủy" name="UpholdHgd[comment_cancel]" id="UpholdHgd_comment_cancel"><?php echo $model->getCommentCancel(); ?></textarea>
    </div>
    
    <div class="row buttons" style="padding-left: 141px;">
        <?php $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType'=>'submit',
            'label'=>$model->isNewRecord ? 'Create' :'Save',
            'type'=>'null', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
            'size'=>'small', // null, 'large', 'small' or 'mini'
            //'htmlOptions' => array('style' => 'margin-bottom: 10px; float: right;'),
        )); ?>	
        <input class='cancel_iframe' type='button' value='Cancel'>
    </div>

    <br>
    
<?php $this->endWidget(); ?>

</div><!-- form -->



<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/autoNumeric-master/autoNumeric.js"></script>
<script>
    $(document).ready(function(){
        $('.form').find('button:submit').click(function(){
            $.blockUI({ overlayCSS: { backgroundColor: '#fff' } }); 
        });
        fnInitInputCurrency();
        parent.$.fn.yiiGridView.update("borrow-grid");
    });
</script>
