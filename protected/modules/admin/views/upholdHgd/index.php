<?php
$model->ScanAutomaticeCallBack();

$this->breadcrumbs=array(
	$this->singleTitle,
);

$menus=array(
            array('label'=>"Create $this->singleTitle", 'url'=>array('create')),
);
if($model->canExportExcelIndex()):
        $menus[] = [
            'label'=> 'Xuất Excel Danh Sách Hiện Tại',
            'url'=>array('index', 'ExportExcel'=>1), 
            'htmlOptions'=>array('class'=>'export_excel ','label'=>'Xuất Excel'),
        ];
    
endif;
$this->menu= ControllerActionsName::createMenusRoles($menus, $actions);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('uphold-hgd-grid', {
                url : $(this).attr('action'),
		data: $(this).serialize()
	});
	return false;
});
");

Yii::app()->clientScript->registerScript('ajaxupdate', "
$('#uphold-hgd-grid a.ajaxupdate').live('click', function() {
    $.fn.yiiGridView.update('uphold-hgd-grid', {
        type: 'POST',
        url: $(this).attr('href'),
        success: function() {
            $.fn.yiiGridView.update('uphold-hgd-grid');
        }
    });
    return false;
});
");
?>

<h1>Danh Sách <?php echo $this->pluralTitle; ?></h1>


<?php echo CHtml::link('Tìm Kiếm Nâng Cao','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'uphold-hgd-grid',
	'dataProvider'=>$model->search(),
        'afterAjaxUpdate'=>'function(id, data){ fnUpdateColorbox();}',
        'template'=>'{pager}{summary}{items}{pager}{summary}',
//    'itemsCssClass' => 'items custom_here',
//    'htmlOptions'=> array('class'=>'grid-view custom_here'),
        'pager' => array(
            'maxButtonCount'=>  CmsFormatter::$PAGE_MAX_BUTTON,
        ),
	'enableSorting' => false,
	//'filter'=>$model,
	'columns'=>array(
            array(
                'header' => 'S/N',
                'type' => 'raw',
                'value' => '$this->grid->dataProvider->pagination->currentPage * $this->grid->dataProvider->pagination->pageSize + ($row+1)',
                'headerHtmlOptions' => array('width' => '10px','style' => 'text-align:center;'),
                'htmlOptions' => array('style' => 'text-align:center;')
            ),
            array(
                'header'=>'Ngày',
                'value'=>'$data->getDateOnly()',
            ),
            'code_no',
            array(
                'name'=>'customer_id',
                'type'=>'html',
                'value'=>'"<b>".$data->getCustomer()."</b> - ".$data->getCustomer("phone")."</b><br>".$data->getCustomer("address")',
//                'htmlOptions' => array('style' => 'width:200px;'),
            ),
            array(
                'name'=>'phone',
                'type'=>'raw',
                'value'=>'$data->getPhone()',
            ),
            array(
                'name'=>'agent_id',
                'type'=>'raw',
                'value'=>'$data->getAgent()',
            ),
            
            array(
                'name'=>'employee_id',
                'value'=>'$data->getEmployee()',
            ),
            
            array(
                'header'=>'Ghi chú',
                'type'=>'Raw',
                'value'=>'$data->getNote()',
            ),
            array(
                'name'=>'uid_login',
                'value'=>'$data->getUidLogin()',
            ),
            
            array(
                'name'=>'status',
                'value'=>'$data->getStatus()',
                'htmlOptions' => array('style' => 'text-align:center;'),
            ),
            array(
                'name' => 'created_date',
                'value' => '$data->getCreatedDate()',
                'htmlOptions' => array('style' => 'width:50px;'),
            ),
            array(
                'header' => 'Actions',
                'class'=>'CButtonColumn',
                'template'=> ControllerActionsName::createIndexButtonRoles($actions,array('view','update','delete','commentCancel')),
                'buttons'=>array(
                    'update' => array(
                        'visible' => '$data->canUpdateWeb()',
                    ),
                    'delete'=>array(
                        'visible'=> 'GasCheck::canDeleteData($data)',
                    ),
                   'commentCancel'=>array(
                        'label'=>'hủy đơn hàng',
                        'imageUrl'=>Yii::app()->theme->baseUrl . '/images/ico-validate-error.png',
                        'options'=>array('class'=>'commentCancel'),
                        'url'=>'Yii::app()->createAbsoluteUrl("admin/upholdHgd/commentCancel", array("id"=>$data->id) )',
                        'visible' => '$data->canCommentCancel()',
                    ),
                ),
            ),
	),
)); ?>
<?php 

?>
<script>
$(document).ready(function() {
    fnUpdateColorbox();
    
});

function fnUpdateColorbox(){   
    fixTargetBlank();
     $(".commentCancel").colorbox({
        iframe:true,
        innerHeight:'350', 
        innerWidth: '850',
        close: "<span title='close'>close</span>",
        onClosed: function () { // update view when close colorbox
                $.fn.yiiGridView.update('uphold-hgd-grid');
//                location.reload();
            }
        });
}
</script>