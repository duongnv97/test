<?php $this->widget('application.components.widgets.XDetailView', array(
    'data' => $model,
    'attributes' => array(
        'group11'=>array(
            'ItemColumns' => 3,
            'attributes' => array(
		array(
                    'name'=>'customer_id',
                    'type'=>'raw',
                    'value'=>"<b>".$model->getCustomer()."</b> - ".$model->getCustomer("phone")."</b><br>".$model->getCustomer("address"),
                ),
		array(
                    'name'=>'status',
                    'value'=>$model->getStatus(),
                ),
                array(
                    'name'=>'phone',
                    'value'=>$model->getPhone(),
                ),
                array(
                    'name'=>'agent_id',
                    'value'=>$model->getAgent(),
                ),
                array(
                    'name'=>'employee_id',
                    'value'=>$model->getEmployee(),
                ),
                array(
                    'label'=>'Ghi chú',
                    'type'=>'raw',
                    'value'=>$model->getNote(),
                ),
                array(
                    'name'=>'uid_login',
                    'type'=>'raw',
                    'value'=>$model->getUidLogin(),
                ),
                array(
                    'name' => 'created_date',
                    'value' => $model->getCreatedDate(),
                ), 
                'code_no',
            ),
        ),
    ),
)); ?>


<style>
    .detail-view .detail-view th, .detail-view .detail-view td { font-size: 1.1em;}
</style>
<script>
    $(document).ready(function(){
//        $('.xdetail_c2').each(function(){
//            $(this).find('td:first').addClass("w-300");
//            $(this).find('td').eq(1).addClass("w-100");
//            $(this).find('th').addClass("w-120");
//        });
    });
</script>