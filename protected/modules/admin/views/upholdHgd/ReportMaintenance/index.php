<?php $this->breadcrumbs=array(
    $this->pageTitle,
); ?>

<h1><?php echo $this->pageTitle;?></h1>
<div class="search-form" style="">
<?php $this->renderPartial('ReportMaintenance/_search',array(
        'model'=>$model,
)); 
?>
</div><!-- search-form -->
<?php if(!empty($data['aUphold'])): ?>
<?php 
$gridCss = Yii::app()->getAssetManager()->publish(Yii::getPathOfAlias('zii.widgets.assets')) . '/gridview/styles.css';
Yii::app()->getClientScript()->registerCssFile($gridCss);
$session=Yii::app()->session;
//Uphold
$OUTPUT_UPHOLD              = $data['aUphold']['OUTPUT']; 
$SUMALl_UPHOLD              = $data['aUphold']['SUMALL'];           //tổng đơn mối
$SUM_COMPLETE_UPHOLD        = $data['aUphold']['SUM_COMPLETE'];     //tổng đơn mối hoàn thành
//Uphold HGD
$OUTPUT_UPHOLD_HGD          = $data['aUpholdHGD']['OUTPUT'];            
$SUMALl_UPHOLD_HGD          = $data['aUpholdHGD']['SUMALL'];        //tổng đơn hộ GD
$SUM_COMPLETE_UPHOLD_HGD    = $data['aUpholdHGD']['SUM_COMPLETE'];  //tổng đơn hộ GD hoàn thành

$aUid = [];
foreach ($OUTPUT_UPHOLD as $key => $value) {
    $aUid[$key] = $key;
}
foreach ($OUTPUT_UPHOLD_HGD as $key => $value) {
    $aUid[$key] = $key;
}
?>
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/scroll/jquery.freezetablecolumns.1.1.js"></script>
<div class="grid-view " id="report-grid">
<table class="items materials_table sortable" >
    <thead>
        <tr>
            <th class="w-20">STT</th>
            <th class="w-250">Họ tên NV</th>
            <th colspan="2">Tổng đơn </th>
            <th colspan="2">Đơn hoàn thành</th>
        </tr>
        <tr>
            <th class="w-20"></th>
            <th class="w-250"></th>
            <th class="item_c"><?php echo 'Mối';?></th>
            <th class="item_c"><?php echo 'Hộ';?></th>
            <th class="item_c"><?php echo 'Mối';?></th>
            <th class="item_c"><?php echo 'Hộ';?></th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <?php $sumAllUphold             = isset($SUMALl_UPHOLD) ? $SUMALl_UPHOLD : "0"; ?>
            <?php $sumAllUpholdHGD          = isset($SUMALl_UPHOLD_HGD) ? $SUMALl_UPHOLD_HGD : "0"; ?>
            <?php $sumCompleteUphold        = isset($SUM_COMPLETE_UPHOLD) ? $SUM_COMPLETE_UPHOLD : "0"; ?>
            <?php $sumCompleteUpholdHGD     = isset($SUM_COMPLETE_UPHOLD_HGD) ? $SUM_COMPLETE_UPHOLD_HGD : "0"; ?>
            <td class="w-20"></td>
            <td class="w-250">Tổng</td>
            <td class="item_c"><?php echo $sumAllUphold;?></td>
            <td class="item_c"><?php echo $sumAllUpholdHGD;?></td>
            <td class="item_c"><?php echo $sumCompleteUphold;?></td>
            <td class="item_c"><?php echo $sumCompleteUpholdHGD;?></td>
        </tr>
     <?php $stt = 1;?>
        <?php foreach ($aUid as $key_uid_login => $uid_login): ?>
        <tr class="odd">
            <?php $model = Users::model()->findByPk($key_uid_login);?>
            <?php $name = isset($model) ? $model->first_name :''; ?>
            <?php $sumMoi           = isset($OUTPUT_UPHOLD[$key_uid_login]['SUM']) ? $OUTPUT_UPHOLD[$key_uid_login]['SUM'] : "0"; ?>
            <?php $sumHGD           = isset($OUTPUT_UPHOLD_HGD[$key_uid_login]['SUM']) ? $OUTPUT_UPHOLD_HGD[$key_uid_login]['SUM'] : "0"; ?>
            <?php $sumMoiComplete   = isset($OUTPUT_UPHOLD[$key_uid_login]['COMPLETE']) ? $OUTPUT_UPHOLD[$key_uid_login]['COMPLETE'] : "0"; ?>
            <?php $sumHGDComplete   = isset($OUTPUT_UPHOLD_HGD[$key_uid_login]['COMPLETE']) ? $OUTPUT_UPHOLD_HGD[$key_uid_login]['COMPLETE'] : "0"; ?>
            <td class="w-20 item_c order_no"><?php echo $stt++;?></td>
            <td class="w-250 "><?php echo $name;?></td>
            <td class="item_c"><?php echo $sumMoi?></td>
            <td class="item_c"><?php echo $sumHGD?></td>
            <td class="item_c"><?php echo $sumMoiComplete?></td>
            <td class="item_c"><?php echo $sumHGDComplete?></td>
        </tr>
        <?php endforeach; ?>
    </tbody>
</table>
</div>        
<?php endif; ?>

<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/print-store-card.css" />
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/jquery.floatThead.min.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/underscore-min.js"></script>    

<?php 
$gridCss = Yii::app()->getAssetManager()->publish(Yii::getPathOfAlias('zii.widgets.assets')) . '/gridview/styles.css';
Yii::app()->getClientScript()->registerCssFile($gridCss);
?>
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/sortElements/jquery.sortElements.js"></script>
<script>
$(document).ready(function() {
//    fnAddClassOddEven('items');
    fnUpdateColorbox();
});

function fnUpdateColorbox(){   
    fixTargetBlank();
}
</script>
<style>
    .sortable thead th:hover {cursor: pointer;}
</style>