<?php
$this->breadcrumbs=array(
    $this->pluralTitle => array('index'),
    ' ' . $this->singleTitle . ' : ' .$model->getOneName(). ' - ' . $model->many_id,
);

$menus = array(
	array('label'=>"$this->pluralTitle Management", 'url'=>array('index')),
	array('label'=>"Tạo Mới $this->singleTitle", 'url'=>array('create')),
	array('label'=>"Cập Nhật $this->singleTitle", 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>"Xóa $this->singleTitle", 'url'=>array('delete'), 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Bạn chắc chắn muốn xóa ?')),
);
$this->menu= ControllerActionsName::createMenusRoles($menus, $actions);
?>
<?php echo MyFormat::BindNotifyMsg(); ?>   
<h1>Xem: <?php echo $this->singleTitle; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
            array(
                'name'  => 'one_id',                       
                'value' => $model->getOneName(),
            ),
            array(
                'name'  => 'many_id',                       
//                'value' => $model->many_id,
                'value' => $model->getManyName(),
            ),
            array(
                'name'  => 'type',                       
                'value' => $model->getType(),
            ),
            array(
                'name'  => 'date_apply',                       
                'value' => $model->getDateApply(),
            ),
	),
)); ?>
