<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=> GasCheck::getCurl(),
	'method'=>'get',
)); ?>

	<div class="row">
        <?php echo $form->labelEx($model,'one_id'); ?>
        <?php echo $form->hiddenField($model,'one_id', array('class'=>'')); ?>
        <?php
                // 1. limit search kh của sale
                $url = Yii::app()->createAbsoluteUrl('admin/ajax/search_by_role', array(
//                            'role'      => implode(',', [ROLE_DIRECTOR_BUSSINESS, ROLE_CUSTOMER, ROLE_MONITOR_AGENT,ROLE_HEAD_OF_BUSINESS,ROLE_MONITORING_MARKET_DEVELOPMENT,ROLE_AGENT]),
                            'role'      => implode(',', [ROLE_ALL]),
//                            'PaySalary' => UsersProfile::SALARY_YES
                        )
                    );
                // widget auto complete search user customer and supplier
                $aData = array(
                    'model'=>$model,
                    'field_customer_id'=>'one_id',
                    'url'=> $url,
                    'name_relation_user'=>'one',
                    'ClassAdd' => 'w-400',
                    'field_autocomplete_name' => 'autocomplete_name',
                    'placeholder'=>'Nhập mã NV hoặc tên',
                );
                $this->widget('ext.SpaAutocompleteCustomer.SpaAutocompleteCustomer',
                    array('data'=>$aData));
                ?>
        </div>
	<div class="row" id="tab_id">
            <?php echo $form->label($model, 'Many_id <br> (Tìm bằng ID)', array()); ?>
            <?php echo $form->textField($model,'many_id',array('class'=>'w-400', 'size'=>11,'maxlength'=>11)); ?>
            <?php echo $form->error($model,'many_id'); ?>
	</div>
        <div class="row" id="tab_autocomplete">
            <?php echo $form->label($model, 'many_id <br> (Tìm bằng Autocomplete)', array()); ?>
            <?php // echo $form->hiddenField($model, 'many_id', array('class' => '')); ?>
            <?php
            // 1. limit search kh của sale
            $url = Yii::app()->createAbsoluteUrl('admin/ajax/search_by_role', array(
                'role' => implode(',', [ROLE_DIRECTOR_BUSSINESS, ROLE_CUSTOMER, ROLE_MONITOR_AGENT, ROLE_HEAD_OF_BUSINESS, ROLE_MONITORING_MARKET_DEVELOPMENT, ROLE_AGENT]),
    //                            'PaySalary' => UsersProfile::SALARY_YES
                    )
            );
            // widget auto complete search user customer and supplier
            $aData = array(
                'model' => $model,
                'field_customer_id' => 'many_id',
                'url' => $url,
                'name_relation_user' => 'many',
                'ClassAdd' => 'w-400',
                'field_autocomplete_name' => 'autocomplete_name_1',
                'placeholder' => 'Nhập mã NV hoặc tên',
            );
            $this->widget('ext.SpaAutocompleteCustomer.SpaAutocompleteCustomer', array('data' => $aData));
            ?>
        </div>
	<div class="row">
            <?php echo $form->label($model,'type',array()); ?>
            <?php echo $form->dropdownList($model, 'type', $model->getArrayTypeCreateUI(),array('empty'=>'Select', 'class' => 'w-400')) ?>
	</div>

	<div class="row">
                <?php echo $form->label($model,'date_apply'); ?>
                <?php 
                    $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                        'model'=>$model,        
                        'attribute'=>'date_apply',
                        'options'=>array(
                            'showAnim'=>'fold',
                            'dateFormat'=> MyFormat::$dateFormatSearch,
//                            'minDate'=> '0',
                            'maxDate'=> '0',
                            'changeMonth' => true,
                            'changeYear' => true,
                            'showOn' => 'button',
                            'buttonImage'=> Yii::app()->theme->baseUrl.'/admin/images/icon_calendar_r.gif',
                            'buttonImageOnly'=> true,                                
                        ),        
                        'htmlOptions'=>array(
                            'class'=>'w-200 date_apply',
                            'size'=>'16',
                            'style'=>'float:left;',                               
                        ),
                    ));
                ?>     		
        </div>

	<div class="row buttons" style="padding-left: 159px;">
            <?php $this->widget('bootstrap.widgets.TbButton', array(
                'buttonType'=>'submit',
                'label'=>'Search',
                'type'=>'null', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
                'size'=>'small', // null, 'large', 'small' or 'mini'
                //'htmlOptions' => array('style' => 'margin-bottom: 10px; float: right;'),
            )); ?>	
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a class='btn_cancel btnReset'>Reset</a>
        </div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->
<script>
    $(function(){
        $('.btnReset').click(function(){
            var div = $(this).closest('form');
            div.find('input').val('');
            div.find('Select').val('');
            div.find('.remove_row_item').trigger('click');
        });
    });
    
</script>