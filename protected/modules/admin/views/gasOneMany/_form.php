<?php 
$urlReload = $model->isNewRecord ? Yii::app()->createAbsoluteUrl("/admin/GasOneMany/create") : Yii::app()->createAbsoluteUrl("/admin/GasOneMany/update",['id'=>$model->id]);
$type = empty($_GET['type']) ? GasOneMany::TAB_ID : $_GET['type'];    
?>

<div class="form">
    

    <?php
    $form = $this->beginWidget('CActiveForm', array(
        'id' => 'gas-one-many-form',
        'enableAjaxValidation' => false,
        'htmlOptions' => array('enctype' => 'multipart/form-data'),
    ));
    ?>

    <p class="note">Dòng có dấu <span class="required">*</span> là bắt buộc.</p>
<?php echo MyFormat::BindNotifyMsg(); ?>

    <div class="row GasCheckboxList">
        <?php echo $form->labelEx($model,'Loại nhập Many'); ?>
        <?php echo $form->radioButtonList($model,'type_create_many', $model->getArrayTypeCreateManyID(), 
                    array(
                        'separator'=>"",
                        'template'=>'<li>{input}{label}</li>',
                        'container'=>'ul',
                        'class'=>'RadioOrderType' 
                    )); 
        ?>
        <div class="clearfix"></div>
    </div>
    <div class="row">
        <?php echo $form->labelEx($model, 'one_id'); ?>
        <?php echo $form->hiddenField($model, 'one_id', array('class' => '')); ?>
        <?php
        // 1. limit search kh của sale
        $url = Yii::app()->createAbsoluteUrl('admin/ajax/search_by_role', array(
//            'role' => implode(',', [ROLE_DIRECTOR_BUSSINESS, ROLE_CUSTOMER, ROLE_MONITOR_AGENT, ROLE_HEAD_OF_BUSINESS, ROLE_MONITORING_MARKET_DEVELOPMENT, ROLE_AGENT]),
            'role' => implode(',', [ROLE_ALL]),
//                            'PaySalary' => UsersProfile::SALARY_YES
                )
        );
        // widget auto complete search user customer and supplier
        $aData = array(
            'model' => $model,
            'field_customer_id' => 'one_id',
            'url' => $url,
            'name_relation_user' => 'one',
            'ClassAdd' => 'w-400',
            'field_autocomplete_name' => 'autocomplete_name',
            'placeholder' => 'Nhập mã NV hoặc tên',
        );
        $this->widget('ext.SpaAutocompleteCustomer.SpaAutocompleteCustomer', array('data' => $aData));
        ?>
    </div>
    <div class="row">
        <?php echo $form->labelEx($model, 'many_id', array()); ?>
        <?php if($type == GasOneMany::TAB_ID){ ?>
        <?php echo $form->textField($model, 'many_id', array('class' => 'w-400' ,'placeHolder'=>'Nhập ID')); ?>
        <?php echo $form->error($model,'many_id'); ?>
        <?php }else{ ?>
        <?php echo $form->hiddenField($model, 'many_id', array('class' => '')); ?>
        <?php
        // 1. limit search kh của sale
        $url = Yii::app()->createAbsoluteUrl('admin/ajax/search_by_role', array(
//            'role' => implode(',', [ROLE_DIRECTOR_BUSSINESS, ROLE_CUSTOMER, ROLE_MONITOR_AGENT, ROLE_HEAD_OF_BUSINESS, ROLE_MONITORING_MARKET_DEVELOPMENT, ROLE_AGENT]),
            'role' => implode(',', [ROLE_ALL]),
//                            'PaySalary' => UsersProfile::SALARY_YES
                )
        );
        // widget auto complete search user customer and supplier
        $aData = array(
            'model'                     => $model,
            'field_customer_id'         => 'many_id',
            'url'                       => $url,
            'name_relation_user'        => 'many',
            'ClassAdd'                  => 'w-400',
            'field_autocomplete_name'   => 'autocomplete_name_1',
            'placeholder'               => 'Nhập mã NV hoặc tên',
        );
        $this->widget('ext.SpaAutocompleteCustomer.SpaAutocompleteCustomer', array('data' => $aData));
        ?>
        <?php } ?>
    </div>

    <div class="row">
        <?php echo $form->label($model, 'type', array()); ?>
        <?php echo $form->dropdownList($model, 'type', $model->getArrayTypeCreateUI(), array('empty' => 'Select', 'class' => 'w-400')) ?>
        <?php echo $form->error($model,'type'); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model, 'date_apply'); ?>
        <?php
        $this->widget('zii.widgets.jui.CJuiDatePicker', array(
            'model' => $model,
            'attribute' => 'date_apply',
            'options' => array(
                'showAnim'      => 'fold',
                'dateFormat'    => MyFormat::$dateFormatSearch,
//                            'minDate'=> '0',
//                'maxDate'       => '0',
                'changeMonth'   => true,
                'changeYear'    => true,
                'showOn'        => 'button',
                'buttonImage'   => Yii::app()->theme->baseUrl . '/admin/images/icon_calendar_r.gif',
                'buttonImageOnly' => true,
            ),
            'htmlOptions' => array(
                'class'     => 'w-200 date_apply',
                'size'      => '16',
                'style'     => 'float:left;',
                'readOnly'  => 1
            ),
        ));
        ?>    
        <?php echo $form->error($model,'date_apply'); ?>
    </div>
    <div class="row buttons" style="padding-left: 141px;">
        <?php
        $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType' => 'submit',
            'label'      => $model->isNewRecord ? 'Create' : 'Save',
            'type'       => 'null', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
            'size'       => 'small', // null, 'large', 'small' or 'mini'
                //'htmlOptions' => array('style' => 'margin-bottom: 10px; float: right;'),
        ));
        ?>	
    </div>

<?php $this->endWidget(); ?>

</div>
<!-- form -->
<script>
    $(document).ready(function () {
        $('.form').find('button:submit').click(function () {
            $.blockUI({overlayCSS: {backgroundColor: '#fff'}});
        });
        bindRadioCreateFor();
    });
    function bindRadioCreateFor(){
        $('#GasOneMany_type_create_many li').each(function(){
            if($(this).find('input').val() == <?php echo $type ?>){
                $(this).find('input').prop('checked', true);
                $(this).css('background-color', 'rgba(76, 175, 80)');
            }
        });
        $('#GasOneMany_type_create_many li').on('click', function(){
            $('#GasOneMany_type_create_many li').css('background-color', 'rgba(0, 0, 0, 0)');
            var radio = $(this).find('input');
            if( radio.is(':checked') ){
                $(this).css('background-color', 'rgba(76, 175, 80)');
            }
            var url = '<?php echo $urlReload; ?>/type/'+radio.val();
            window.location.replace(url);
        });
    }
    function getOrderType(){
        var res = 0;
        $('.RadioOrderType').each(function(){
            if($(this).is(':checked')){
                res = $(this).val();
                return false;
            }
        });
        return res*1;
    }
</script>