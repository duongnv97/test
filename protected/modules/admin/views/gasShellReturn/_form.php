<?php
/**
 * @var GasShellReturn $model
 * @var CActiveForm $form
 */
?>
<div class="form">

    <?php $form = $this->beginWidget('CActiveForm', array(
        'id' => 'gas-shell-return-form',
        'enableAjaxValidation' => false,
        'htmlOptions' => array('enctype' => 'multipart/form-data'),
    )); ?>

    <p class="note">Dòng có dấu <span class="required">*</span> là bắt buộc.</p>
    <?php echo MyFormat::BindNotifyMsg(); ?>


    <div class="row">
        <?php echo $form->labelEx($model, 'customer_id'); ?>
        <?php echo $model->getCustomerName(); ?>&nbsp;
    </div>
    <div class="row">
        <?php echo $form->labelEx($model, 'type_customer'); ?>
        <?php echo $model->getTypeCustomerText(); ?>&nbsp;
    </div>
    <div class="row">
        <?php echo $form->labelEx($model, 'sale_id'); ?>
        <?php echo $model->getSaleName(); ?>&nbsp;
    </div>
    <div class="row">
        <?php echo $form->labelEx($model, 'status'); ?>
        <?php echo $model->getStatusText(); ?>&nbsp;
    </div>
    <div class="row">
        <?php echo $form->labelEx($model, 'week'); ?>
        <?php echo $model->getWeekText(); ?>&nbsp;
    </div>
    <div class="row">
        <?php echo $form->labelEx($model, 'total'); ?>
        <?php echo $model->getTotalText(); ?>&nbsp;
    </div>
    <div class="row">
        <?php echo $form->labelEx($model, 'forecast'); ?>
        <?php
        echo $model->getForecastText();
        echo "&nbsp";
        ?>
    </div>
    <div class="row">
        <?php echo $form->labelEx($model, 'received'); ?>
        <?php
        if (MyRoleChecking::isRoleDieuPhoi() || MyRoleChecking::isRoleAdmin() || $model->isCustomerSaler()) {
            echo $form->textField($model, 'received');
            echo $form->error($model, 'received');
        } else {
            echo $model->getReceivedText();
            echo "&nbsp";
        }
        ?>
    </div>
    <div class="row">
        <?php echo $form->labelEx($model, 'note'); ?>
        <?php echo $form->textArea($model, 'note', array('rows' => 6, 'cols' => 50)); ?>
        <?php echo $form->error($model, 'note'); ?>
    </div>
    <div class="row buttons" style="padding-left: 141px;">
        <?php $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType' => 'submit',
            'label' => $model->isNewRecord ? 'Create' : 'Save',
            'type' => 'null',
            'size' => 'small',
        )); ?>
    </div>

    <?php $this->endWidget(); ?>

</div><!-- form -->
<script>
    $(document).ready(function () {
        $('.form').find('button:submit').click(function () {
            $.blockUI({overlayCSS: {backgroundColor: '#fff'}});
        });
    });
</script>