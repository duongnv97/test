<?php
$this->breadcrumbs = array(
    $this->pluralTitle => array('index'),
    $model->id => array('view', 'id' => $model->id),
    'Cập Nhật ' . $this->singleTitle,
);

$menus = array(
    array('label' => $this->pluralTitle, 'url' => array('index')),
);
$this->menu = ControllerActionsName::createMenusRoles($menus, $actions);

?>

    <h1>Cập Nhật: <?php echo $this->singleTitle; ?></h1>

<?php echo $this->renderPartial('_form', array('model' => $model)); ?>