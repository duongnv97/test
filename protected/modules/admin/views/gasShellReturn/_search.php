<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=> GasCheck::getCurl(),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'customer_id',array()); ?>
		<?php echo $form->textField($model,'customer_id',array('size'=>10,'maxlength'=>10)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'type_customer',array()); ?>
		<?php echo $form->textField($model,'type_customer'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'sale_id',array()); ?>
		<?php echo $form->textField($model,'sale_id',array('size'=>10,'maxlength'=>10)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'status',array()); ?>
		<?php echo $form->textField($model,'status'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'week',array()); ?>
		<?php echo $form->textField($model,'week'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'total',array()); ?>
		<?php echo $form->textField($model,'total'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'forecast',array()); ?>
		<?php echo $form->textField($model,'forecast'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'received',array()); ?>
		<?php echo $form->textField($model,'received'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'note',array()); ?>
		<?php echo $form->textArea($model,'note',array('rows'=>6, 'cols'=>50)); ?>
	</div>

	<div class="row buttons" style="padding-left: 159px;">
		        <?php $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType'=>'submit',
            'label'=>'Search',
            'type'=>'null', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
            'size'=>'small', // null, 'large', 'small' or 'mini'
            //'htmlOptions' => array('style' => 'margin-bottom: 10px; float: right;'),
        )); ?>	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->