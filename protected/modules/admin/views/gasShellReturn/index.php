<?php
$this->breadcrumbs = array(
    $this->singleTitle,
);

$menus = array(
    array('label' => "Lập Lịch $this->singleTitle", 'url' => array('planning')),
);
$this->menu = ControllerActionsName::createMenusRoles($menus, $actions);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('gas-shell-return-grid', {
                url : $(this).attr('action'),
		data: $(this).serialize()
	});
	return false;
});
");

Yii::app()->clientScript->registerScript('ajaxupdate', "
$('#gas-shell-return-grid a.ajaxupdate').live('click', function() {
    $.fn.yiiGridView.update('gas-shell-return-grid', {
        type: 'POST',
        url: $(this).attr('href'),
        success: function() {
            $.fn.yiiGridView.update('gas-shell-return-grid');
        }
    });
    return false;
});
");

// Trung June 21 2016
$link = Yii::app()->createAbsoluteUrl('admin/gasShellReturn/ajaxUpdate');
?>

<h1><?php echo $this->pageTitle . " " . $this->pluralTitle; ?></h1>


<?php echo CHtml::link('Tìm Kiếm Nâng Cao', '#', array('class' => 'search-button')); ?>
<div class="search-form" style="display:none">
    <?php $this->renderPartial('_search', array(
        'model' => $model,
    )); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
    'id' => 'gas-shell-return-grid',
    'dataProvider' => $model->search(),
    'afterAjaxUpdate' => 'function(id, data){ fnUpdateColorbox();}',
    'template' => '{pager}{summary}{items}{pager}{summary}',
//    'itemsCssClass' => 'items custom_here',
//    'htmlOptions'=> array('class'=>'grid-view custom_here'),
    'pager' => array(
        'maxButtonCount' => CmsFormatter::$PAGE_MAX_BUTTON,
    ),
    'enableSorting' => false,
    //'filter'=>$model,
    'columns' => array(
        array(
            'header' => 'S/N',
            'type' => 'raw',
            'value' => '$this->grid->dataProvider->pagination->currentPage * $this->grid->dataProvider->pagination->pageSize + ($row+1)',
            'headerHtmlOptions' => array('width' => '10px', 'style' => 'text-align:center;'),
            'htmlOptions' => array('style' => 'text-align:center;')
        ),
        array(
            'name' => 'customer_id',
            'value' => '$data->getCustomerName()'
        ),
        array(
            'type' => 'html',
            'name' => 'type_customer',
            'value' => '$data->getTypeCustomerText(). $data->getSpanCodeForHighLight()'
        ),
        array(
            'name' => 'sale_id',
            'value' => '$data->getSaleName()'
        ),
        array(
            'name' => 'status',
            'value' => '$data->getStatusText()'
        ),
        'week',
        'total',
        'forecast',
        'received',
        array(
            'name' => 'note',
            'type' => 'raw',
            'value' => '$data->getDetailHtml()',
        ),
        array(
            'header' => 'Actions',
            'class' => 'CButtonColumn',
            'template' => ControllerActionsName::createIndexButtonRoles($actions),
            'buttons' => array(
                'view' => array(
                    'visible' => 'false',
                ),
                'update' => array(
                    'visible' => '$data->isUpdatable()',
                ),
                'delete' => array(
                    'visible' => 'GasCheck::canDeleteData($data)',
                ),
            ),
        ),
    ),
)); ?>
<script>
    $(document).ready(function () {
        fnUpdateColorbox();
        fnShowhighLightTr();
    });

    function fnUpdateColorbox() {
        fixTargetBlank();
    }
    function reloadGrid(data) {
        $.fn.yiiGridView.update('menu-grid');
    }
</script>