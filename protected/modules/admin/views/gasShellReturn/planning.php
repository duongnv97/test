<?php
/**
 *
 * @var GasShellReturnController $this
 * @var DateTime $firstWeekDay
 * @var array $listModel
 * @var mixed $actions
 */

$this->breadcrumbs = array(
    $this->singleTitle,
);

Yii::app()->clientScript->registerScript('ajaxupdate', "
$('#gas-shell-return-grid a.ajaxupdate').live('click', function() {
    $.fn.yiiGridView.update('gas-shell-return-grid', {
        type: 'POST',
        url: $(this).attr('href'),
        success: function() {
            $.fn.yiiGridView.update('gas-shell-return-grid');
        }
    });
    return false;
});
");

$menus = array(
    array('label' => "$this->singleTitle", 'url' => array('index')),
);
$this->menu = ControllerActionsName::createMenusRoles($menus, $actions);

// Trung June 28 2016
$link = Yii::app()->createAbsoluteUrl('admin/gasShellReturn/ajaxUpdate');
$cUid = Yii::app()->user->id;
$fakeItem = new GasShellReturn();

// Trung Jul 06 2016
$currentCounter = 0;
?>

<h1><?php echo $this->pageTitle . " " . $this->pluralTitle; ?> tuần <?php echo $firstWeekDay; ?></h1>
<?php echo MyFormat::BindNotifyMsg(); ?>


<div class="form">

    <?php $form = $this->beginWidget('CActiveForm', array(
        'id' => 'gas-orders-form',
        'enableAjaxValidation' => false,
    )); ?>
    <input value="" type="hidden" id="delete_ids"
           name="DeleteIds">

    <?php if (Yii::app()->user->hasFlash('successUpdate')): ?>
        <div class='success_div'><?php echo Yii::app()->user->getFlash('successUpdate'); ?></div>
    <?php endif; ?>

    <div class="row">
        <label>&nbsp</label>

        <div>
            <a href="javascript:void(0);" style="line-height:25px;" class="text_under_none item_b"
               onclick="fnBuildRow();">
                <img style="float: left;margin-right:8px;"
                     src="<?php echo Yii::app()->theme->baseUrl; ?>/images/add.png">
                Thêm Dòng ( Phím tắt F8 )
            </a>
        </div>
    </div>
    <div class="row">
        <table class="materials_table materials_table_root materials_table_th" style="">
            <thead>
            <tr>
                <th class="item_c w-10">#
                    <input value="<?php echo $firstWeekDay; ?>" type="hidden"
                           name="CurrentWeek"></th>
                <th class="w-400 item_c"><?php echo $fakeItem->getAttributeLabel('customer_id') ?></th>
                <th class="w-150 item_c"><?php echo $fakeItem->getAttributeLabel('sale_id') ?></th>
                <th class="w-70 item_c"><?php echo $fakeItem->getAttributeLabel('freq') ?></th>
                <th class="w-70 item_c"><?php echo $fakeItem->getAttributeLabel('old_remain') ?></th>
                <th class="w-70 item_c"><?php echo $fakeItem->getAttributeLabel('new_remain') ?></th>
                <th class="w-70 item_c">Tổng cộng</th>
                <th class="w-70 item_c"><?php echo $fakeItem->getAttributeLabel('forecast') ?></th>
                <th class="w-220 item_c"><?php echo $fakeItem->getAttributeLabel('note') ?></th>
                <th class="item_unit item_c last">Xóa</th>
            </tr>
            </thead>
            <tbody>
            <?php if (count($listModel)): ?>
                <?php /** @var GasShellReturn $item */
                foreach ($listModel as $item): ?>
                    <tr class="calc_row row_input">
                        <td class="order_no item_c w-10"></td>
                        <td class="col_customer">
                            <?php echo $item->getCustomerName() ?>
                            <input value="<?php echo $item->id; ?>" type="hidden" class="shell_return_id"
                                   name="GasShellReturn[<?php echo $currentCounter ?>][id]">
                            <input class="customer_id_hide" value="<?php echo $item->customer_id ?>" type="hidden"
                                   name="GasShellReturn[<?php echo $currentCounter ?>][customer_id]">
                        </td>
                        <td>
                            <span class="sale_name"><?php echo $item->getSaleName(); ?></span>
                        </td>
                        <td>
                            <input name="GasShellReturn[<?php echo $currentCounter ?>][freq]" class="w-50"
                                   value="<?php echo $item->freq ?>"
                                   maxlength="9" type="number">
                        </td>
                        <td><input name="GasShellReturn[<?php echo $currentCounter ?>][old_remain]"
                                   class="w-50 old_remain"
                                   maxlength="5" value="<?php echo $item->old_remain; ?>"
                                   type="hidden"><?php echo $item->old_remain; ?></td>
                        <td><input name="GasShellReturn[<?php echo $currentCounter ?>][new_remain]"
                                   class="w-50 new_remain"
                                   maxlength="5" value="<?php echo $item->new_remain; ?>" type="number"></td>
                        <td><span class="total"><?php echo $item->getTotal(); ?></span></td>
                        <td><input name="GasShellReturn[<?php echo $currentCounter ?>][forecast]" class="w-50"
                                   maxlength="5"
                                   value="<?php echo $item->forecast; ?>" type="number"></td>
                        <td class="">
                            <textarea name="GasShellReturn[<?php echo $currentCounter ?>][note]" class=""
                                      maxlength="300"><?php echo $item->note; ?></textarea>
                        </td>
                        <td class="item_c last"><span remove="" class="remove_icon_only"></span></td>
                    </tr>
                    <?php $currentCounter++ ?>
                <?php endforeach; ?>
            <?php endif; ?>
            </tbody>
        </table>
    </div>

    <div class="row buttons" style="padding-left: 141px;">
        <?php $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType' => 'submit',
            'label' => 'Save',
            'type' => 'null', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
            'size' => 'small', // null, 'large', 'small' or 'mini'
            //'htmlOptions' => array('style' => 'margin-bottom: 10px; float: right;'),
        )); ?>    </div>

    <?php $this->endWidget(); ?>

</div><!-- form -->
<?php Yii::app()->clientScript->registerCoreScript('jquery.ui'); ?>
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/jquery.floatThead.min.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/underscore-min.js"></script>
<script>
    var currentCounter = <?php echo $currentCounter; ?>;

    $(document).ready(function () {
        $('.form').find('button:submit').click(function () {
            $.blockUI({overlayCSS: {backgroundColor: '#fff'}});
        });

        fnBindAllAutocomplete();
        fnRefreshOrderNumber();
        fnBindRemoveIcon();
        fnChangeReleaseDate();

        $('.old_remain').live('change', calculateTotal).live('keyup', calculateTotal);
        $('.new_remain').live('change', calculateTotal).live('keyup', calculateTotal);
    });

    function fnBeforeRemoveIcon($this) {
        var parentTr = $this.closest('tr');
        var idObj = parentTr.find('.shell_return_id');
        var deleteId = idObj.val();
        if (deleteId > 0) {
            if ($('#delete_ids').val() == '') {
                $('#delete_ids').val(deleteId);
            } else {
                $('#delete_ids').val($('#delete_ids').val() + ',' + deleteId);
            }
        }
    }

    $(document).keydown(function (e) {
        if (e.which == 119) {
            fnBuildRow();
            $('.old_remain').live('change', calculateTotal).live('keyup', calculateTotal);
            $('.new_remain').live('change', calculateTotal).live('keyup', calculateTotal);
        }
    });

    function fnChangeReleaseDate() {
        $('#GasOrders_date_delivery, .user_id_executive').change(function () {
            var url_ = '<?php echo Yii::app()->createAbsoluteUrl('admin/gasOrders/create')?>';
            var date_delivery = $('#GasOrders_date_delivery').val();
            var user_id_executive = $('.user_id_executive').val();
            if ($.trim(date_delivery) != '' && $.trim(user_id_executive) != '') {
                $.blockUI({overlayCSS: {backgroundColor: '#fff'}});
                $.ajax({
                    url: url_,
                    data: {date_delivery: date_delivery, user_id_executive: user_id_executive, 'from_ajax': 1},
                    dataType: 'json',
                    type: 'post',
                    success: function (data) {
                        if (data['success'])
                            window.location = data['next'];
                        $.unblockUI();
                    }
                });
            }

        });
    }

    function fnBuildRow() {
        var _tr = '';
        _tr += '<tr class="calc_row row_input">';
        _tr += '<td class="order_no item_c w-10"></td>';
        _tr += '<td class="col_customer">';
        _tr += '<div class="clr"></div>';
        _tr += '<div class="row row_customer_autocomplete">';
        _tr += '<div class="float_l">';
        _tr += '<input class="customer_id_hide" value="" type="hidden" name="GasShellReturn[' + currentCounter + '][customer_id]">';
        _tr += '<input class="float_l customer_autocomplete w-340"  placeholder="Nhập mã, tên hoặc số đt" maxlength="100" value="" type="text">';
        _tr += '<span class="remove_row_item" onclick="fnRemoveName(this);"></span>';
        _tr += '</div>';
        _tr += '</div>';
        _tr += '</td>';
        _tr += '<td>';
        _tr += '<span class="sale_name"></span>';
        _tr += '</td>';
        _tr += '<td>';
        _tr += '<input name="GasShellReturn[' + currentCounter + '][freq]" class="w-50" value="30" maxlength="9" type="number">';
        _tr += '</td>';
        _tr += '<td><input name="GasShellReturn[' + currentCounter + '][old_remain]" class="w-50 old_remain" maxlength="5" value="0" type="hidden"></td>';
        _tr += '<td><input name="GasShellReturn[' + currentCounter + '][new_remain]" class="w-50 new_remain" maxlength="5" value="0" type="number"></td>';
        _tr += '<td><span class="total">0</span></td>';
        _tr += '<td><input name="GasShellReturn[' + currentCounter + '][forecast]" class="w-50" maxlength="5" value="0" type="number"></td>';
        _tr += '<td class="">';
        _tr += '<textarea name="GasShellReturn[' + currentCounter + '][note]" maxlength="300"></textarea>';
        _tr += '</td>';
        _tr += '<td class="item_c last"><span remove="" class="remove_icon_only"></span></td>';
        _tr += '</tr>';

        // Increase to next counter
        currentCounter++;

        $('.materials_table_root tbody:first').append(_tr);
        fnRefreshOrderNumber();
        bindEventForHelpNumber();
        fnBindAllAutocomplete();
        $('.materials_table').floatThead();
    }

    function fnBindAllAutocomplete() {
        // customer
        $('.customer_autocomplete').each(function () {
            fnAutocompleteCustomer($(this));
        });
    }

    // @param objInput : is obj input ex  $('.customer_autocomplete')
    function fnAutocompleteCustomer(objInput) {
        var parent_div = objInput.closest('td.col_customer');
        var parentTr = parent_div.closest('tr');
        objInput.autocomplete({
            source: '<?php echo Yii::app()->createAbsoluteUrl('admin/ajax/search_user_by_code');?>',
            minLength: '<?php echo MIN_LENGTH_AUTOCOMPLETE;?>',
            close: function (event, ui) {
            },
            search: function (event, ui) {
                objInput.addClass('grid-view-loading-gas');
            },
            response: function (event, ui) {
                objInput.removeClass('grid-view-loading-gas');
                var json = $.map(ui, function (value, key) {
                    return value;
                });
                if (json.length < 1) {
                    var error = '<div class=\'errorMessage clr autocomplete_name_text\'>Không tìm thấy dữ liệu.</div>';
                    if (parent_div.find('.autocomplete_name_text').size() < 1) {
                        parent_div.find('.remove_row_item').after(error);
                    }
                    else
                        parent_div.find('.autocomplete_name_text').show();
                }

            },
            select: function (event, ui) {
                objInput.attr('readonly', true);
                parent_div.find('.autocomplete_name_text').hide();
                parent_div.find('.customer_id_hide').val(ui.item.id);
                parent_div.find('.autocomplete_customer_info').remove();
                var tableInfo = fnBuildTableCustomerInfo(ui.item.code_bussiness, ui.item.name_customer, ui.item.address, ui.item.phone);
                parent_div.append(tableInfo);
                parent_div.find('.autocomplete_customer_info').show();
                fnAjaxGetLastOrder(parentTr, ui);
            }
        });
    }

    function fnRemoveName(this_) {
        var parent_div = $(this_).closest('td.col_customer');
        parent_div.find('.customer_autocomplete').attr("readonly", false);
        parent_div.find('.customer_autocomplete').val("");
        parent_div.find('.customer_id_hide').val("");
        parent_div.find('.autocomplete_customer_info').hide();
    }

    function fnBuildTableCustomerInfo(info_code_bussiness, info_name, info_address, info_phone) {
        var table = '';
        table += '<div class="autocomplete_customer_info table_small" style="">';
        table += '<table>';
        table += '<tr>';
        table += '<td class="_l td_first_t">Mã KH:</td>';
        table += '<td class="_r info_code_bussiness td_last_r td_first_t">' + info_code_bussiness + '</td>';
        table += '</tr>';

        table += '<tr>';
        table += '<td class="_l">Tên KH:</td>';
        table += '<td class="_r info_name td_last_r">' + info_name + '</td>';
        table += '</tr>';
        table += '<tr>';
        table += '<td class="_l">Địa chỉ:</td>';
        table += '<td class="_r info_address td_last_r">' + info_address + '</td>';
        table += '</tr>';
        table += '<tr>';
        table += '<td class="_l">Điện Thoại:</td>';
        table += '<td class="_r info_phone td_last_r">' + info_phone + '</td>';
        table += '</tr>';
        table += '</table>';
        table += '</div>';
        table += '<div class="clr"></div>';
        return table;
    }

    function calculateTotal() {
        var parentTr = $(this).closest('tr.calc_row');
        var oldRemain = parentTr.find('input.old_remain');
        var new_remain = parentTr.find('input.new_remain');
        var total = parentTr.find('span.total');
        total.html((oldRemain.val() * 1 + new_remain.val() * 1));
    }

    /**
     * @Author: ANH DUNG May 14, 2016
     * @Todo: get info last order of this customer
     * @param: parentTr is object tr
     * @param: ui  object json info customer
     */
    function fnAjaxGetLastOrder(parentTr, ui) {
        var url_ = "<?php echo Yii::app()->createAbsoluteUrl("admin/gasShellReturn/getShellReturnInfo");?>";
        $.blockUI({overlayCSS: {backgroundColor: '#fff'}});
        $.ajax({
            url: url_,
            type: 'post',
            dataType: "json",
            data: {
                customer_id: ui.item.id,
                week: <?php echo $firstWeekDay ?>
            },
            success: function (data) {
                if (data['success']) {
                    parentTr.find('.sale_name').html(data['model']['sale_name']);
                    parentTr.find('.old_remain').val(data['model']['old_remain']);
                    parentTr.find('.freq').val(data['model']['freq']);
                    parentTr.find('.note').val(data['model']['note']);
                }
                $.unblockUI();
            }
        });
    }
</script>