<?php
$gridCss = Yii::app()->getAssetManager()->publish(Yii::getPathOfAlias('zii.widgets.assets')) . '/gridview/styles.css';
Yii::app()->getClientScript()->registerCssFile($gridCss);
$this->breadcrumbs = array(
    $this->pageTitle,
);
?>

<h1><?php echo $this->pageTitle; ?></h1>
<div class="search-form" style="">
    <?php $this->renderPartial('failedStatus/_search', array(
        'model' => $model, 'data' => $data,
    )); ?>
</div><!-- search-form -->

<style>
    .ui-datepicker-trigger {
        float: left;
    }
</style>

<script>
    $(document).ready(function () {
        $('.form').find('button:submit').click(function () {
            $.blockUI({overlayCSS: {backgroundColor: '#fff'}});
        });
    });
</script>

<?php
if (isset($data['OUTPUT_SUM'])) :
    $LIST_CALL_CENTER_USER = $data['LIST_CALL_CENTER_USER'];
    $FAILED_STATUS_LIST = $data['FAILED_STATUS_LIST'];
    $OUTPUT_SUM = isset($data['OUTPUT_SUM']) ? $data['OUTPUT_SUM'] : array();
    $OUTPUT_SUM_TYPE = isset($data['OUTPUT_SUM_TYPE']) ? $data['OUTPUT_SUM_TYPE'] : array();
    $OUTPUT = isset($data['OUTPUT']) ? $data['OUTPUT'] : array();
    $index = 1;
    // Get table height
    $freeHeightTable = 1;

    // For chart
    $totalDataChart[] = ['Loại', 'Số cuộc gọi'];
    // Render for chart
    foreach ($FAILED_STATUS_LIST as $failed_status => $status_name):
        if (isset($OUTPUT_SUM_TYPE[$failed_status])) {
            $totalDataChart[] = [$status_name, $OUTPUT_SUM_TYPE[$failed_status]];
        }
    endforeach;
    ?>
    <script type="text/javascript"
            src="<?php echo Yii::app()->theme->baseUrl; ?>/js/scroll/jquery.freezetablecolumns.1.1.js"></script>
    <div class="grid-view display_none">
        <table id="freezetablecolumns_report_failed" class="items hm_table freezetablecolumns">
            <thead>
            <tr style="">
                <th class="w-20">#</th>
                <th class="w-140">Tên nhân viên</th>
                <th class="w-30">Tổng</th>
                <?php foreach ($FAILED_STATUS_LIST as $status_name): ?>
                    <th class="item_c w-100"><?php echo $status_name; ?></th>
                <?php endforeach; ?>
            </tr>
            </thead>
            <tbody>
                <tr>
                    <td colspan="2" class="item_c item_b">Tổng</td>
                    <td class="item_c item_b"><?php echo array_sum($OUTPUT_SUM); ?></td>
                    <?php foreach ($FAILED_STATUS_LIST as $failed_status => $status_name): ?>
                        <td class="item_c item_b"><?php echo isset($OUTPUT[$failed_status]) ? array_sum($OUTPUT[$failed_status]) : ''; ?></td>
                    <?php endforeach; ?>
                </tr>
            <?php foreach ($OUTPUT_SUM as $user_id => $sumCall): ?>
                <?php
                if ($sumCall > 0):
                    $nameEmployee = $user_id;
                    $nameEmployee = Sta3::getCallUserNameFromID($LIST_CALL_CENTER_USER, $user_id);
                    $freeHeightTable++;
                    ?>
                    <tr>
                        <td class="item_c"><?php echo $index++; ?></td>
                        <td class="item_b"><?php echo $nameEmployee; ?></td>
                        <td class="item_c item_b"><?php echo $sumCall; ?></td>
                        <?php foreach ($FAILED_STATUS_LIST as $failed_status => $status_name): ?>
                            <td class="item_c"><?php echo isset($OUTPUT[$failed_status][$user_id]) ? $OUTPUT[$failed_status][$user_id] : ''; ?></td>
                        <?php endforeach; ?>
                    </tr>
                    <?php
                endif;
            endforeach;
            // Get table height
            $freeHeightTable = $freeHeightTable * 19;
            $freeHeightTable = $freeHeightTable > 400 ? 400 : $freeHeightTable;
            ?>
            </tbody>
        </table>
    </div>


    <script type="text/javascript">
        $('.freezetablecolumns').each(function () {
            var id_table = $(this).attr('id');
            $('#' + id_table).freezeTableColumns({
                width:       <?php echo Users::WIDTH_freezetable;?>,   // required - bt 1100
                height:      <?php echo $freeHeightTable;?>,   // required
                numFrozen: 3,     // optional
                frozenWidth: 240,   // optional
                clearWidths: true  // optional
            });
        });
        $(window).load(function () {
            var index = 1;
            $('.freezetablecolumns').each(function () {
                if (index == 1)
                    $(this).closest('div.grid-view').show();
                index++;
            });
            fnAddClassOddEven('items');
        });
    </script>
    <?php
endif;
?>
<?php
if (isset($totalDataChart)):
    include 'chartPercentFailed.php';
endif;
?>
