<?php
$this->breadcrumbs = array(
    $this->pageTitle,
);

$totalIncoming = 0;
$totalSaleItem = 0;
$totalMisscall = 0;
?>

<h1><?php echo $this->pageTitle; ?></h1>
<div class="search-form" style="">
    <?php $this->renderPartial('report/_search', array(
        'model' => $model,
    )); ?>
</div><!-- search-form -->

<style>
    .ui-datepicker-trigger {
        float: left;
    }
</style>

<script>
    $(document).ready(function () {
        $('.form').find('button:submit').click(function () {
            $.blockUI({overlayCSS: {backgroundColor: '#fff'}});
        });
    });
</script>

<?php
if (isset($data['SUMMARIZE'])) {
    $DATE_ARRAY = $data['DATE_ARRAY'];
    $SUMMARIZE = $data['SUMMARIZE'];
    $tempChart = array();
    $listUserId = array();
    $totalDataChart = array();
    $listTitleChart = array('Nghe gọi đến', 'Tạo đơn hàng', 'Gọi nhỡ');

    foreach ($DATE_ARRAY as $date) {
        for ($i = 0; $i < 24; $i++) {
            $tempChart[$date . '_' . $i] = array("{$date}:{$i}h");
            $value = array();
            $value[] = "{$date}:{$i}h";
            $currentId = $date . '_' . $i;
            $incoming = isset($SUMMARIZE[Call::STATUS_HANGUP][$currentId]) ? $SUMMARIZE[Call::STATUS_HANGUP][$currentId] * 1 : 0;
            $sale = isset($SUMMARIZE[0][$currentId]) ? $SUMMARIZE[0][$currentId] * 1 : 0;
            $missCall = isset($SUMMARIZE[Call::STATUS_MISS_CALL][$currentId]) ? $SUMMARIZE[Call::STATUS_MISS_CALL][$currentId] * 1 : 0;
            $totalIncoming += $incoming;
            $totalMisscall += $missCall;
            $totalSaleItem += $sale;
            $totalDataChart[] = array(
//                "{$date}:{$i}h",
                "{$i}h",
                $incoming,
                $sale, // Total sale
                $missCall);
        }
    }
}
?>
<div class="">
    <table class="tb hm_table">
        <thead>
        <tr style="">
            <th class="w-150 ">Loại</th>
            <th class="w-150 ">Số lượng</th>
        </tr>
        </thead>
        <tbody>
            <tr>
                <td class="item_c">Nghe gọi đến</td>
                <td class="item_b item_c"><?php echo $totalIncoming; ?></td>
            </tr>
            <tr>
                <td class="item_c">Tạo đơn hàng</td>
                <td class="item_b item_c"><?php echo $totalSaleItem; ?></td>
            </tr>
            <tr>
                <td class="item_c">Gọi nhỡ</td>
                <td class="item_b item_c"><?php echo $totalMisscall; ?></td>
            </tr>
        </tbody>
    </table>
</div>
<?php
if (isset($data['SUMMARIZE'])) {
    include 'chartTotalCall.php';
}
?>
<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/print-store-card.css" />