<?php
$gridCss = Yii::app()->getAssetManager()->publish(Yii::getPathOfAlias('zii.widgets.assets')) . '/gridview/styles.css';
Yii::app()->getClientScript()->registerCssFile($gridCss);
$this->breadcrumbs = array(
    $this->pageTitle,
);
?>

<h1><?php echo $this->pageTitle; ?></h1>
<div class="search-form" style="">
    <?php $this->renderPartial('sellStatus/_search', array(
        'model' => $model, 'data' => $data,
    )); ?>
</div><!-- search-form -->

<style>
    .ui-datepicker-trigger {
        float: left;
    }
</style>

<script>
    $(document).ready(function () {
        $('.form').find('button:submit').click(function () {
            $.blockUI({overlayCSS: {backgroundColor: '#fff'}});
        });
    });
</script>

<?php
if (isset($data['OUTPUT_SUM_SALE'])) :
    $LIST_CALL_CENTER_USER = $data['LIST_CALL_CENTER_USER'];
    $CANCEL_STATUS_LIST = $data['CANCEL_STATUS_LIST'];
    $OUTPUT_SALE = isset($data['OUTPUT_SALE']) ? $data['OUTPUT_SALE'] : array();
    $OUTPUT_SUM_SALE = isset($data['OUTPUT_SUM_SALE']) ? $data['OUTPUT_SUM_SALE'] : array();
    $index = 1;
    // Get table height
    $freeHeightTable = 1;

    // For chart
    $totalDataChart[] = ['Loại', 'Số cuộc gọi'];
    // Render for chart
    if (isset($OUTPUT_SUM_SALE[Sell::STATUS_PAID])):
        $totalDataChart[] = ["Thành công", $OUTPUT_SUM_SALE[Sell::STATUS_PAID] * 1];
    endif;
    foreach ($CANCEL_STATUS_LIST as $failed_status => $status_name):
        if (isset($OUTPUT_SUM_SALE[Sell::STATUS_CANCEL . '_' . $failed_status])):
            $totalDataChart[] = [$status_name, $OUTPUT_SUM_SALE[Sell::STATUS_CANCEL . '_' . $failed_status] * 1];
        endif;
    endforeach;
    if (isset($OUTPUT_SUM_SALE[Sell::STATUS_CANCEL . '_'])):
        $totalDataChart[] = ['Khác', $OUTPUT_SUM_SALE[Sell::STATUS_CANCEL . '_'] * 1];
    endif;
    ?>
    <script type="text/javascript"
            src="<?php echo Yii::app()->theme->baseUrl; ?>/js/scroll/jquery.freezetablecolumns.1.1.js"></script>
    <div class="grid-view display_none">
        <table id="freezetablecolumns_report_failed" class="items hm_table freezetablecolumns">
            <thead>
            <tr>
                <th class="w-20">#</th>
                <th class="w-140">Tên nhân viên</th>
                <th class="w-30">Fail</th>
                <th class="w-60">Tổng</th>
                <th class="w-30">%</th>
                <?php foreach ($CANCEL_STATUS_LIST as $status_name): ?>
                    <th class="item_c w-100"><?php echo $status_name; ?></th>
                <?php endforeach; ?>
                <th class="item_c w-100">Khác</th>
            </tr>
            </thead>
            <tbody>
            <?php if (isset($OUTPUT_SALE[0])) foreach ($OUTPUT_SALE[0] as $user_id => $sumSell): ?>
                <?php
                if ($sumSell > 0):
                    $nameEmployee = $user_id;
                    $nameEmployee = Sta3::getCallUserNameFromID($LIST_CALL_CENTER_USER, $user_id);
                    $successSell = isset($OUTPUT_SALE[Sell::STATUS_PAID][$user_id]) ? $OUTPUT_SALE[Sell::STATUS_PAID][$user_id] * 1 : 0;
                    $freeHeightTable++;
                    ?>
                    <tr class="h_20">
                        <td class="item_c"><?php echo $index++; ?></td>
                        <td class="item_b"><?php echo $nameEmployee; ?></td>
                        <td class="item_c item_b"><?php echo '' . ($sumSell - $successSell); ?></td>
                        <td class="item_c item_b"><?php echo $successSell . '/' . $sumSell; ?></td>
                        <td class="item_c item_b"><?php echo '' . round(($successSell / (($sumSell) ? $sumSell : 1)) * 100) . '%'; ?></td>
                        <?php foreach ($CANCEL_STATUS_LIST as $failed_status => $status_name): ?>
                            <td class="item_c item_b"><?php echo isset($OUTPUT_SALE[Sell::STATUS_CANCEL . '_' . $failed_status][$user_id]) ? $OUTPUT_SALE[Sell::STATUS_CANCEL . '_' . $failed_status][$user_id] : ''; ?></td>
                        <?php endforeach; ?>
                        <td class="item_c item_b"><?php echo isset($OUTPUT_SALE[Sell::STATUS_CANCEL . '_'][$user_id]) ? $OUTPUT_SALE[Sell::STATUS_CANCEL . '_'][$user_id] : ''; ?></td>
                    </tr>
                    <?php
                endif;
            endforeach;
            // Get table height
            $freeHeightTable = $freeHeightTable * 19;
            $freeHeightTable = $freeHeightTable > 400 ? 400 : $freeHeightTable;
            ?>
            </tbody>
        </table>
    </div>


    <script type="text/javascript">
        $('.freezetablecolumns').each(function () {
            var id_table = $(this).attr('id');
            $('#' + id_table).freezeTableColumns({
                width:       <?php echo Users::WIDTH_freezetable;?>,   // required - bt 1100
                height:      <?php echo $freeHeightTable;?>,   // required
                numFrozen: 5,     // optional
                frozenWidth: 330,   // optional
                clearWidths: true  // optional
            });
        });
        $(window).load(function () {
            var index = 1;
            $('.freezetablecolumns').each(function () {
                if (index == 1)
                    $(this).closest('div.grid-view').show();
                index++;
            });
            fnAddClassOddEven('items');
        });
    </script>
    <?php
endif;
?>
<?php
if (isset($totalDataChart)):
    include 'chartPercentFailed.php';
endif;
?>
