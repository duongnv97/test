<!-- Used for agent call report search area -->
<div class="wide form">

    <?php $form = $this->beginWidget('CActiveForm', array(
//	'action'=> GasCheck::getCurl(),
        'action' => Yii::app()->createUrl($this->route),
        'method' => 'get',
    )); ?>
    <div class="row">
        <?php echo $form->labelEx($model, 'user_id'); ?>
        <?php echo $form->hiddenField($model, 'user_id', array('class' => '')); ?>
        <?php
        // 1. limit search kh của sale
        $url = Yii::app()->createAbsoluteUrl('admin/ajax/search_by_role', array('role' => ROLE_CALL_CENTER));
        // widget auto complete search user customer and supplier
        $aData = array(
            'model' => $model,
            'field_customer_id' => 'user_id',
            'url' => $url,
            'name_relation_user' => 'rUser',
            'ClassAdd' => 'w-400',
            'field_autocomplete_name' => 'autocomplete_name_1',
            'placeholder' => 'Nhập mã NV hoặc tên',
        );
        $this->widget('ext.SpaAutocompleteCustomer.SpaAutocompleteCustomer',
            array('data' => $aData));
        ?>
    </div>

    <div class="row more_col">
        <div class="col1">
            <?php echo $form->label($model, 'date_from'); ?>
            <?php
            $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                'model' => $model,
                'attribute' => 'date_from',
                'options' => array(
                    'showAnim' => 'fold',
                    'dateFormat' => MyFormat::$dateFormatSearch,
//                            'minDate'=> '0',
                    'maxDate' => '0',
                    'changeMonth' => true,
                    'changeYear' => true,
                    'showOn' => 'button',
                    'buttonImage' => Yii::app()->theme->baseUrl . '/admin/images/icon_calendar_r.gif',
                    'buttonImageOnly' => true,
                ),
                'htmlOptions' => array(
                    'class' => 'w-150 float_l',
                    'readonly' => 1,
                ),
            ));
            ?>
        </div>
        <div class="col2">
            <?php echo $form->label($model, 'date_to'); ?>
            <?php
            $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                'model' => $model,
                'attribute' => 'date_to',
                'options' => array(
                    'showAnim' => 'fold',
                    'dateFormat' => MyFormat::$dateFormatSearch,
//                            'minDate'=> '0',
                    'maxDate' => '0',
                    'changeMonth' => true,
                    'changeYear' => true,
                    'showOn' => 'button',
                    'buttonImage' => Yii::app()->theme->baseUrl . '/admin/images/icon_calendar_r.gif',
                    'buttonImageOnly' => true,
                ),
                'htmlOptions' => array(
                    'class' => 'w-150 float_l',
                    'readonly' => 1,
                ),
            ));
            ?>
        </div>
        <div class="col3">
            <?php echo $form->label($model, 'type'); ?>
            <?php echo $form->dropDownList($model, 'type', $model->getArrayTypeCall(), array('class' => 'w-200', 'empty' => 'Select')); ?>
        </div>
        <div class="col3">
            <?php echo $form->label($model, 'failed_status', ['label' => 'Loại']);
            echo $form->dropDownList($model, 'failed_status', $data['FAILED_STATUS_LIST'], array('class' => 'w-200', 'empty' => 'Select')); ?>
        </div>
    </div>

    <div class="row buttons" style="padding-left: 159px;">
        <?php $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType' => 'submit',
            'label' => 'Xem',
            'type' => 'null', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
            'size' => 'small', // null, 'large', 'small' or 'mini'
            //'htmlOptions' => array('style' => 'margin-bottom: 10px; float: right;'),
        )); ?>    </div>

    <?php $this->endWidget(); ?>

</div><!-- search-form -->