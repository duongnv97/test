<?php
/** Crete c
 * Created by PhpStorm.
 * User: Trung Nguyen
 * Date: 6/22/2017
 */
if (isset($totalDataChart)) {
    /** @var array $totalDataChart list value for lines */
    $jsArrayTotalCall = json_encode($totalDataChart);
    ?>
    <div class="float_l" style="width: 1100px">
        <div id="success_call_failed_chart" style="width: 1100px; height: 500px"></div>
    </div>
    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script type="text/javascript">
        //        fnAddClassOddEven('items');
        google.charts.load("current", {packages: ["corechart"]});
        google.charts.setOnLoadCallback(drawChart);
        function drawChart() {
            // Making a 3D Pie Chart https://developers.google.com/chart/interactive/docs/gallery/piechart?hl=vi
            var data = google.visualization.arrayToDataTable(<?php echo $jsArrayTotalCall; ?>);

            var options = {
                title: 'Tổng trạng thái đơn hàng',
                is3D: true,
            };
            var chart = new google.visualization.PieChart(document.getElementById('success_call_failed_chart'));
            chart.draw(data, options);
        }
    </script>
<?php } ?>