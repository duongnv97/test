<?php
/** Crete c
 * Created by PhpStorm.
 * User: Trung Nguyen
 * Date: 6/22/2017
 */
if (isset($listTitleChart)) {
    /** @var array $listTitleChart list title for lines */
    /** @var array $totalDataChart list value for lines */
    $jsArrayTotalCall = json_encode($totalDataChart);
    ?>
    <div class="float_l" style="width: 1100px">
        <div id="total_call_chart" style="width: 1100px; height: 500px"></div>
    </div>
    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script type="text/javascript">
        //        fnAddClassOddEven('items');
        google.charts.load("current", {packages: ["line"]});
        google.charts.setOnLoadCallback(drawChart);
        function drawChart() {
            var data = new google.visualization.DataTable();
            data.addColumn('string', 'Thời gian');
            <?php
            foreach ($listTitleChart as $value) {
                echo "data.addColumn('number', '$value');\n";
            }
            echo "data.addRows($jsArrayTotalCall);\n";
            ?>
//            data.addColumn('number', 'Guardians of the Galaxy');
//            data.addColumn('number', 'The Avengers');
//
//            data.addRows([
//                [1, 37.8, 80.8],
//                [2, 30.9, 69.5],
//            ]);
            var options = {
                title: 'Tổng số cuộc gọi theo giờ',
                isStacked: false,
                focusTarget: 'category',
            };
            var chart = new google.charts.Line(document.getElementById('total_call_chart'));
            chart.draw(data, google.charts.Line.convertOptions(options));
        }
    </script>
<?php } ?>