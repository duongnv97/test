<?php
$this->breadcrumbs = array(
    $this->pageTitle,
);

$totalIncoming = 0;
$totalSaleItem = 0;
$totalMisscall = 0;
?>

<h1><?php echo $this->pageTitle; ?></h1>
<div class="search-form" style="">
    <?php $this->renderPartial('report/_search', array(
        'model' => $model,
    )); ?>
</div><!-- search-form -->

<style>
    .ui-datepicker-trigger {
        float: left;
    }
</style>

<script>
    $(document).ready(function () {
        $('.form').find('button:submit').click(function () {
            $.blockUI({overlayCSS: {backgroundColor: '#fff'}});
        });
    });
</script>

<?php
if (isset($data['SUMMARIZE'])) {
    $SUMMARIZE = $data['SUMMARIZE'];
    $tempChart = array();
    $listUserId = array();
    $totalDataChart = array();
    $listTitleChart = array('Cuộc gọi đến', 'Đơn hàng', 'Gọi nhỡ');

    for ($i = 0; $i < 24; $i++) {
        $tempChart[$i] = array("{$i}h");
        $value = array();
        $value[] = "{$i}h";
        $currentId = $i;
        $incoming = isset($SUMMARIZE[Call::STATUS_HANGUP][$currentId]) ? $SUMMARIZE[Call::STATUS_HANGUP][$currentId] * 1 : 0;
        $sale = isset($SUMMARIZE[0][$currentId]) ? $SUMMARIZE[0][$currentId] * 1 : 0;
        $missCall = isset($SUMMARIZE[Call::STATUS_MISS_CALL][$currentId]) ? $SUMMARIZE[Call::STATUS_MISS_CALL][$currentId] * 1 : 0;
        $totalIncoming += $incoming;
        $totalMisscall += $missCall;
        $totalSaleItem += $sale;
        $totalDataChart[] = array(
            "{$i}h",
            $incoming,
            $sale, // Total sale
            $missCall);
    }
}
?>
<div class="grid-view">
    <table class="tb hm_table">
        <thead>
        <tr style="">
            <th class="w-150 ">Loại</th>
            <th class="w-150 ">Số lượng</th>
        </tr>
        </thead>
        <tbody>
        <tr>
            <td class="item_c">Cuộc gọi đến</td>
            <td class="item_b item_c"><?php echo $totalIncoming; ?></td>
        </tr>
        <tr>
            <td class="item_c">Đơn hàng</td>
            <td class="item_b item_c"><?php echo $totalSaleItem; ?></td>
        </tr>
        <tr>
            <td class="item_c">Gọi nhỡ</td>
            <td class="item_b item_c"><?php echo $totalMisscall; ?></td>
        </tr>
        </tbody>
    </table>
</div>
<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/print-store-card.css" />
<?php
if (isset($data['SUMMARIZE'])) {
    include 'chartTotalCall.php';
}
?>
