

<?php
$this->breadcrumbs=array(
    $this->singleTitle,
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('app-text-grid', {
                url : $(this).attr('action'),
		data: $(this).serialize()
	});
	return false;
});
");

Yii::app()->clientScript->registerScript('ajaxupdate', "
$('#app-text-grid a.ajaxupdate').live('click', function() {
    $.fn.yiiGridView.update('app-text-grid', {
        type: 'POST',
        url: $(this).attr('href'),
        success: function() {
            $.fn.yiiGridView.update('app-text-grid');
        }
    });
    return false;
});
");
?>

<?php echo CHtml::link('Tìm Kiếm Nâng Cao','#',array('class'=>'search-button')); ?>
<div class="search-form" style="">
<?php $this->renderPartial('OrderCancel/reportAgent/_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->
<?php
$mSellReport    = new SellReport();
$aList          = $model->searchOrderCancelAgent(); // lấy biến
$rData          = $aList['rData'];                       // [agent_id] => array([status_cancel]=> count)
$rSumStatusCancel = $aList['rSumStatusCancel']; // [status_cancel] => sum
$rSumAgent      = $aList['rSumAgent'];               // [agent_id] => sum
$aStatusCancel  = $mSellReport->getArrayStatusCancelForAgent();
$mAppCache      = new AppCache();
$aAgent         = $mAppCache->getAgentListdata();
$stt = 0;
?>

<div class="grid-view">
    <table class="tb hm_table">
        <thead>
        <tr style="">
            <th class="w-30 ">STT</th>
            <th class="w-350 ">Tên đại lý</th>
            <th class="w-100 "></th>
            <?php foreach ($aStatusCancel as $value) { ?>
                <th class="w-150 "><?php echo $value; ?></th>
            <?php }?>
            
        </tr>
        </thead>
        <tbody>
        <tr>
            <td class="item_c"></td>
            <td class="item_c"></td>
            <td class="item_c">Tổng</td>
             <?php foreach ($rSumStatusCancel as $value) { ?>
                <td class="item_b item_c "><?php echo $value; ?></td>
            <?php }?>
            
        </tr>
        
        <?php foreach ($rSumAgent as $key => $value) { $stt++; 
             if(isset($rData[$key])){
                 $Status = $rData[$key];
                  ?>
                <tr>
                    <td class="item_c"><?php echo $stt; ?></td>
                    <td class=""><?php echo $aAgent[$key]; ?></td>
                    <td class="item_b item_c"><?php echo $value; ?></td>
                    <?php foreach ($aStatusCancel as $id => $item) { ?> 
                            <td class=" item_c">
                                <?php if(! empty($Status[$id])){
                                    echo $Status[$id];
                                }else{
                                    echo '0'; 
                                }
                                ?></td>
                    <?php }?>
                    
                </tr>
        <?php }} ?>
             
        
       
                
        
       
        </tbody>
    </table>
</div>
<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/print-store-card.css" />



<script>
$(document).ready(function() {
    fnUpdateColorbox();
});

function fnUpdateColorbox(){   
    fixTargetBlank();
}
</script>