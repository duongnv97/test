<div class="form">
    <?php
        $LinkNormal         = Yii::app()->createAbsoluteUrl('admin/reportCall/orderCancel');
        $LinkPVKH           = Yii::app()->createAbsoluteUrl('admin/reportCall/orderCancel', array( 'type_cancel'=> 1));
        $LinkCENTER         = Yii::app()->createAbsoluteUrl('admin/reportCall/orderCancel', array( 'type_cancel'=> GasSettle::SETTLE_EXPIRE));
    ?>
    <h1>Danh sách 
        <a class='btn_cancel f_size_14 <?php echo (!isset($_GET['type_cancel'])) ? "active":"";?>' href="<?php echo $LinkNormal;?>">Phát sinh theo đại lý</a>
        
        <a class='btn_cancel f_size_14 <?php echo isset($_GET['type_cancel']) && $_GET['type_cancel']==1 ? "active":"";?>' href="<?php echo $LinkPVKH;?>">Phát sinh theo PVKH</a>
        
        <a class='btn_cancel f_size_14 <?php echo isset($_GET['type_cancel']) && $_GET['type_cancel']== 2 ? "active":"";?>' href="<?php echo $LinkCENTER;?>">Phát sinh theo NV tổng đài</a>
        
    </h1> 
    
    
</div>
