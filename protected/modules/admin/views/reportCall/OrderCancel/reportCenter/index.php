

<?php
$this->breadcrumbs=array(
    $this->singleTitle,
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('app-text-grid', {
                url : $(this).attr('action'),
		data: $(this).serialize()
	});
	return false;
});
");

Yii::app()->clientScript->registerScript('ajaxupdate', "
$('#app-text-grid a.ajaxupdate').live('click', function() {
    $.fn.yiiGridView.update('app-text-grid', {
        type: 'POST',
        url: $(this).attr('href'),
        success: function() {
            $.fn.yiiGridView.update('app-text-grid');
        }
    });
    return false;
});
");
?>

<?php echo CHtml::link('Tìm Kiếm Nâng Cao','#',array('class'=>'search-button')); ?>
<div class="search-form" style="">
<?php $this->renderPartial('OrderCancel/reportCenter/_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->
<?php 
$mSellReport = new SellReport();
$aList = $model->searchOrderCancelCENTER();  // lấy biến
$rData = $aList['rData'];                       // [uid_login] => array([status_cancel]=> count)
$rSumStatusCancel = $aList['rSumStatusCancel']; // [status_cancel] => sum
$rSumUid = $aList['rSumUid'];               // [uid_login] => sum
$aStatusCancel      = $mSellReport->getArrayStatusCancelForCenter();
$mAppCache          = new AppCache();
$aCallCenter       = $mAppCache->getListdataUserByRole(ROLE_CALL_CENTER);
$stt = 0;
?>
<div class="grid-view">
    <table class="tb hm_table">
        <thead>
        <tr style="">
            <th class="w-40 ">STT</th>
            <th class="w-350 ">Tên NV tổng đài</th>
            <th class="w-140 "></th>
            <?php foreach ($aStatusCancel as $value) { ?>
                <th class="w-150 "><?php echo $value; ?></th>
            <?php }?>
            
        </tr>
        </thead>
        <tbody>
        <tr>
            <td class="item_c"></td>
            <td class="item_c"></td>
            <td class="item_c">Tổng</td>
             <?php foreach ($rSumStatusCancel as $value): ?>
                <td class="item_b item_c "><?php echo $value; ?></td>
            <?php endforeach; ?>
            
        </tr>
        
        <?php foreach ($rSumUid as $key => $value) : 
            $stt++; 
            if(isset($aCallCenter[$key])){
                $nameCallCenter = $aCallCenter[$key];
            }else{
                $mUser = Users::model()->findByPk($key);
                $nameCallCenter = ($mUser) ? $mUser->first_name : $key;
            }
                ?>
                <tr>
                    <td class="item_c"><?php echo $stt; ?></td>
                    <td class=""><?php echo $nameCallCenter; ?></td>
                    <td class="item_b item_c"><?php echo $value; ?></td>
                    <?php foreach ($aStatusCancel as $keyStatus => $value) : ?> 
                            <td class=" item_c">
                                <?php 
                                if(!empty($rData[$key][$keyStatus])){
                                    echo $rData[$key][$keyStatus];
                                }else{
                                    echo '0'; 
                                }
                                ?></td>
                    <?php endforeach; ?>
                    
                </tr>
        <?php endforeach; ?>
        
       
        </tbody>
    </table>
</div>
<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/print-store-card.css" />


<script>
$(document).ready(function() {
    fnUpdateColorbox();
});

function fnUpdateColorbox(){   
    fixTargetBlank();
}
</script>