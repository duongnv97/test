<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>GasCheck::getCurl(),//Yii::app()->createUrl($this->route),
	'method'=>'post',
)); ?>
        <div class="row">
        <?php echo $form->labelEx($model,'employee_maintain_id'); ?>
        <?php echo $form->hiddenField($model,'employee_maintain_id', array('class'=>'')); ?>
        <?php
                // 1. limit search kh của sale
                $url = Yii::app()->createAbsoluteUrl('admin/ajax/search_by_role', array('role'=> implode(',', Sell::$ROLE_EMPLOYEE_SEARCH_AUTOCOMPLETE)));
                // widget auto complete search user customer and supplier
                $aData = array(
                    'model'=>$model,
                    'field_customer_id'=>'employee_maintain_id',
                    'url'=> $url,
                    'name_relation_user'=>'rEmployeeMaintain',
                    'ClassAdd' => 'w-400',
                    'field_autocomplete_name' => 'autocomplete_name_2',
                    'placeholder'=>'Nhập mã NV hoặc tên',
                );
                $this->widget('ext.SpaAutocompleteCustomer.SpaAutocompleteCustomer',
                    array('data'=>$aData));
                ?>
            <?php echo $form->error($model,'employee_maintain_id'); ?>
        </div>

        <div class="row more_col">
            <div class="col1">
                <?php echo $form->labelEx($model,'type_customer'); ?>
                <?php echo $form->dropDownList($model,'type_customer', CmsFormatter::$CUSTOMER_BO_MOI,array('class'=>'w-200','empty'=>'Select')); ?>
            </div>
            <div class="col2">
                <?php echo $form->label($model, 'promotion_id', ['label'=>'Mã khuyến mãi']); ?>
                <?php echo $form->textField($model, 'promotion_id', array('class' => 'w-190', 'placeholder' => 'vd: 24H750')); ?>
            </div>
            <div class="col3">
                <?php echo $form->labelEx($model,'pttt_code'); ?>
                <?php echo $form->dropDownList($model,'pttt_code', ActiveRecord::getYesNo(),array('class'=>'w-200','empty'=>'Select')); ?>
            </div>  
        </div>
    
        <div class="row more_col">
            <div class="col1 w-400">
                <?php echo $form->labelEx($model,'province_id'); ?>
                <div class="fix-label">
                    <?php
                       $this->widget('ext.multiselect.JMultiSelect',array(
                             'model'=>$model,
                             'attribute'=>'province_id',
                             'data'=> GasProvince::getArrAll(),
                             // additional javascript options for the MultiSelect plugin
                            'options'=>array('selectedList' => 30,),
                             // additional style
                             'htmlOptions'=>array('class' => 'w-200'),
                       ));    
                   ?>
                </div>
            </div>
            <div class="col2">
                <?php echo $form->labelEx($model,'Bình quay về KM'); ?>
                <?php echo $form->dropDownList($model,'app_promotion_user_id', ActiveRecord::getYesNo(),array('class'=>'w-200','empty'=>'Select')); ?>
            </div>
        </div>
        <div class="row">
        <?php echo $form->labelEx($model,'agent_id'); ?>
        <?php echo $form->hiddenField($model,'agent_id', array('class'=>'')); ?>
        <?php
                // 1. limit search kh của sale
                $url = Yii::app()->createAbsoluteUrl('admin/ajax/search_by_role', array('role'=> ROLE_AGENT));
                // widget auto complete search user customer and supplier
                $aData = array(
                    'model'=>$model,
                    'field_customer_id'=>'agent_id',
                    'url'=> $url,
                    'name_relation_user'=>'rAgent',
                    'ClassAdd' => 'w-400',
                    'field_autocomplete_name' => 'autocomplete_name_3',
                    'placeholder'=>'Nhập mã hoặc tên đại lý',
                );
                $this->widget('ext.SpaAutocompleteCustomer.SpaAutocompleteCustomer',
                    array('data'=>$aData));
                ?>
            <?php echo $form->error($model,'agent_id'); ?>
        </div>
    
    <div class="row more_col">
        <div class="col1">
                <?php echo $form->label($model,'date_from'); ?>
                <?php 
                    $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                        'model'=>$model,        
                        'attribute'=>'date_from',
                        'options'=>array(
                            'showAnim'=>'fold',
                            'dateFormat'=> MyFormat::$dateFormatSearch,
//                            'minDate'=> '0',
                            'maxDate'=> '0',
                            'changeMonth' => true,
                            'changeYear' => true,
                            'showOn' => 'button',
                            'buttonImage'=> Yii::app()->theme->baseUrl.'/admin/images/icon_calendar_r.gif',
                            'buttonImageOnly'=> true,                                
                        ),        
                        'htmlOptions'=>array(
                            'class'=>'w-200 date_from',
                            'size'=>'16',
                            'style'=>'float:left;',                               
                        ),
                    ));
                ?>     		
        </div>
        <div class="col2">
                <?php echo $form->label($model,'date_to'); ?>
                <?php 
                    $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                        'model'=>$model,        
                        'attribute'=>'date_to',
                        'options'=>array(
                            'showAnim'=>'fold',
                            'dateFormat'=> MyFormat::$dateFormatSearch,
//                            'minDate'=> '0',
                            'maxDate'=> '0',
                            'changeMonth' => true,
                            'changeYear' => true,
                            'showOn' => 'button',
                            'buttonImage'=> Yii::app()->theme->baseUrl.'/admin/images/icon_calendar_r.gif',
                            'buttonImageOnly'=> true,                                
                        ),        
                        'htmlOptions'=>array(
                            'class'=>'w-150 DateTo',
                            'size'=>'16',
                            'style'=>'float:left;',
                        ),
                    ));
                ?>     		
        </div>
    </div>
    <div class="row more_col">
        <div class="col1">
                <?php echo $form->label($model,'Ngày gọi từ'); ?>
                <?php 
                    $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                        'model'=>$model,        
                        'attribute'=>'date_comment_from',
                        'options'=>array(
                            'showAnim'=>'fold',
                            'dateFormat'=> MyFormat::$dateFormatSearch,
//                            'minDate'=> '0',
                            'maxDate'=> '0',
                            'changeMonth' => true,
                            'changeYear' => true,
                            'showOn' => 'button',
                            'buttonImage'=> Yii::app()->theme->baseUrl.'/admin/images/icon_calendar_r.gif',
                            'buttonImageOnly'=> true,                                
                        ),        
                        'htmlOptions'=>array(
                            'class'=>'w-200 date_comment_from',
                            'size'=>'16',
                            'style'=>'float:left;',                               
                        ),
                    ));
                ?>     		
        </div>
        <div class="col2">
                <?php echo $form->label($model,'Đến ngày'); ?>
                <?php 
                    $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                        'model'=>$model,        
                        'attribute'=>'date_comment_to',
                        'options'=>array(
                            'showAnim'=>'fold',
                            'dateFormat'=> MyFormat::$dateFormatSearch,
//                            'minDate'=> '0',
                            'maxDate'=> '0',
                            'changeMonth' => true,
                            'changeYear' => true,
                            'showOn' => 'button',
                            'buttonImage'=> Yii::app()->theme->baseUrl.'/admin/images/icon_calendar_r.gif',
                            'buttonImageOnly'=> true,                                
                        ),        
                        'htmlOptions'=>array(
                            'class'=>'w-150 date_comment_to',
                            'size'=>'16',
                            'style'=>'float:left;',
                        ),
                    ));
                ?>     		
        </div>
    </div>
    <div class="row">
        <?php echo $form->labelEx($model,'uid_login'); ?>
        <?php echo $form->hiddenField($model,'uid_login', array('class'=>'')); ?>
        <?php
            // 1. limit search kh của sale
            $url = Yii::app()->createAbsoluteUrl('admin/ajax/search_by_role', array('role'=> implode(',', $model->getRoleAllowCreate())));
            // widget auto complete search user customer and supplier
            $aData = array(
                'model'=>$model,
                'field_customer_id'=>'uid_login',
                'url'=> $url,
                'name_relation_user'=>'rUidLogin',
                'ClassAdd' => 'w-400',
                'field_autocomplete_name' => 'amount_discount',
                'placeholder'=>'Nhập mã NV hoặc tên',
            );
            $this->widget('ext.SpaAutocompleteCustomer.SpaAutocompleteCustomer',
                array('data'=>$aData));
            ?>
        <?php echo $form->error($model,'uid_login'); ?>
    </div>
    <div class="row">
        <?php echo $form->labelEx($model,'customer_id'); ?>
        <?php echo $form->hiddenField($model,'customer_id', array('class'=>'')); ?>
        <?php
                // 1. limit search kh của sale
                $url = Yii::app()->createAbsoluteUrl('admin/ajax/searchHgd');
                // widget auto complete search user customer and supplier
                $aData = array(
                    'model'=>$model,
                    'field_customer_id'=>'customer_id',
                    'url'=> $url,
                    'name_relation_user'=>'rCustomer',
                    'ClassAdd' => 'w-400',
    //                'fnSelectCustomer'=>'fnSelectCustomer',
                );
                $this->widget('ext.SpaAutocompleteCustomer.SpaAutocompleteCustomer',
                    array('data'=>$aData));
                ?>
            <?php echo $form->error($model,'customer_id'); ?>
    </div>
    
    <div class="row more_col">
        <div class="col1">
            <?php $aPageSize = $model->getArrayPageSize(); ?>
            <?php echo $form->labelEx($model,'Số dòng hiển thị'); ?>
            <?php echo $form->dropDownList($model,'pageSize', $aPageSize,array('class'=>'w-200')); ?>
        </div>
        <div class="col2">
            <?php echo $form->labelEx($model,'Lỗi NV'); ?>
            <?php echo $form->dropDownList($model,'order_type_status', $model->getArrayStatusOrder(),array('class'=>'w-200','empty'=>'Select')); ?>
        </div>
        <div class="col3">
            <?php echo $form->labelEx($model,'Đơn phạt'); ?>
            <?php echo $form->dropDownList($model,'punish', ActiveRecord::getYesNo(),array('class'=>'w-200','empty'=>'Select')); ?>
        </div>  
    </div>
    
    <div class="row">
        <?php echo $form->labelEx($model,'Nhân viên'); ?>
        <?php echo $form->hiddenField($model,'sale_id', array('class'=>'')); ?>
        <?php
            $url = Yii::app()->createAbsoluteUrl('admin/ajax/search_user_login');
            $aData = array(
                'model'=>$model,
                'field_customer_id'=>'sale_id',
                'url'=> $url,
                'name_relation_user'=>'rSale',
                'ClassAdd' => 'w-400',
                'field_autocomplete_name' => 'autocomplete_name_4',
                'placeholder'=>'Nhập mã hoặc tên NV',
            );
            $this->widget('ext.SpaAutocompleteCustomer.SpaAutocompleteCustomer',
                array('data'=>$aData));
            ?>
        <?php echo $form->error($model,'ptttUid'); ?>
    </div>
    
    <div class="row buttons" style="padding-left: 159px;">
        <?php $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType'=>'submit',
            'label'=>'Search',
            'type'=>'null', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
            'size'=>'small', // null, 'large', 'small' or 'mini'
            //'htmlOptions' => array('style' => 'margin-bottom: 10px; float: right;'),
        )); ?>
        <?php $urlExcel = Yii::app()->createAbsoluteUrl('admin/reportCall/reportComplete/to_excel/1'); ?>
        <?php if (isset($_GET['type']) && $_GET['type'] == SellReport::TYPE_INDEX_CHECKED): ?>
        <?php $urlExcel = Yii::app()->createAbsoluteUrl('admin/reportCall/callComplete/type/' . SellReport::TYPE_INDEX_CHECKED . '/ExportExcel/1'); ?>
        <a class="btn btn_cancel" href="<?php echo $urlExcel ?>" style="text-decoration: none;">Xuất Excel</a>
        <?php endif; ?>
    </div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->

<script>
    $(function(){
        $('.date_from').change(function(){
            var div = $(this).closest('.row');
            div.find('.DateTo').val($(this).val());
        });
    });
</script>