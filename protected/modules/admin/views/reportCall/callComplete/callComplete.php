<?php
$this->breadcrumbs=array(
	$this->singleTitle,
);

$allowCommentCancel = true;

$menus=array(
);
$this->menu= ControllerActionsName::createMenusRoles($menus, $actions);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('sell-grid', {
                url : $(this).attr('action'),
		data: $(this).serialize()
	});
	return false;
});
");

Yii::app()->clientScript->registerScript('ajaxupdate', "
$('#sell-grid a.ajaxupdate').live('click', function() {
    $.fn.yiiGridView.update('sell-grid', {
        type: 'POST',
        url: $(this).attr('href'),
        success: function() {
            $.fn.yiiGridView.update('sell-grid');
        }
    });
    return false;
});
");

?>
<?php echo MyFormat::BindNotifyMsg(); ?>
<h1><?php echo $this->pageTitle; ?></h1>
<?php include 'index_button.php'; ?>
<?php echo MyFormat::BindNotifyMsg(); ?>
<?php if($model->canViewAction()): ?>
<?php echo CHtml::link('Tìm Kiếm Nâng Cao','#',array('class'=>'search-button')); ?>
<div class="search-form" style="">
<?php $this->renderPartial('callComplete/_search',array(
        'model'=>$model,
)); ?>
</div><!-- search-form -->
<div class="form">
    <?php $type = isset($_GET['type']) ? $_GET['type'] : SellReport::TYPE_INDEX_DEFAULT;?>
    <?php $form=$this->beginWidget('CActiveForm', array(
            'id'=>'sell-form',
            'enableAjaxValidation'=>false,
            'htmlOptions' => array('enctype' => 'multipart/form-data'),
    )); ?>
    <?php $this->widget('zii.widgets.grid.CGridView', array(
            'id'=>'sell-grid',
            'dataProvider'=>$model->searchAllSellComplete($type),
            'afterAjaxUpdate'=>'function(id, data){ fnUpdateColorbox();}',
            'template'=>'{pager}{summary}{items}{pager}{summary}',
            'pager' => array(
                'maxButtonCount'=>  CmsFormatter::$PAGE_MAX_BUTTON,
            ),
    //	'enableSorting' => false,
            //'filter'=>$model,
            'columns'=>array(
            array(
                'header' => 'S/N',
                'type' => 'raw',
                'value' => '$this->grid->dataProvider->pagination->currentPage * $this->grid->dataProvider->pagination->pageSize + ($row+1)',
                'headerHtmlOptions' => array('width' => '10px','style' => 'text-align:center;'),
                'htmlOptions' => array('style' => 'text-align:center;')
            ),
            array(
                    'class'=>'CCheckBoxColumn',
                    'selectableRows'=>2,
                    'id'=>'callCompleteElement',
                    'cssClassExpression'=>'$data->canApplyForUsers();',
                    'checkBoxHtmlOptions'=>array('name'=>'SellReport[callCompleteElement][]')
                ),
            array(
                'name'=>'created_date_only',
                'value' => '$data->getDateSellGrid()."<br><br><b>".$data->getPtttCode()."</b>"."<br>".$data->getCustomerNewOld()',
                'type'=>'html',
                'htmlOptions' => array('style' => 'width:50px;'),
            ),
            array(
                'name'=>'code_no',
                'type'=>'html',
                'value'=>'$data->code_no.$data->getHighPriceText()."<br><br>Ngày tạo: ".$data->getCreatedDate()',
                'htmlOptions' => array('style' => 'width:50px;'),
            ),
            array(
                'name'=>'agent_id',
                'type'=>'raw',
                'value'=>'"<b>Tổng: ".$data->getGrandTotal(true)."</b><br>".$data->getAgent()',
            ),

            array(
                'name'=>'customer_id',
                'type'=>'html',
                'value'=>'$data->getCustomer()',
                'htmlOptions' => array('style' => 'width:100px;'),
            ),
            array(
                'name'=>'type_customer',
                'type'=>'raw',
                'value'=>'$data->getTypeCustomer()."<br>".$data->getArrayPlatformText()',
                'htmlOptions' => array('style' => 'width:30px;'),
            ),
            array(
                'header'=>'Phone',
                'type'=>'raw',
                'value'=>'$data->getInfoAccounting()."<br>".$data->getCallCenter()."<br>".$data->getUrlViewCall()',
            ),
            array(
                'header'=>'Gas + VT Bán',
                'type'=>'raw',
                'value'=>'$data->getInfoGas()."".$data->getImageViewGrid()."<br>".$data->getDeliveryTimerGrid()."<br>".$data->getStatusCancelText()."<br>".$data->getReasonCanCel()',
            ),
            array(
                'header'=>'KM',
                'type'=>'html',
                'value'=>'$data->viewKmName.$data->getPromotionCodeNo()',
            ),
            array(
                'header'=>'Audit',
                'type'=>'raw',
                'value'=>'$data->getEmployeeCheck()',
            ),
            array(
                'header'=>'Ngày gọi',
                'type'=>'raw',
                'value'=>'$data->getTimeCheck()',
            ),
            array(
                'name' => 'order_type_status',
                'type' => 'raw',
                'value' => '$data->getOrderTypeStatus()',
                'htmlOptions' => array('style' => 'width:100px;')
            ),
            array(
                'name' => 'Ghi chú',
                'type' => 'raw',
                'value' => '$data->getNoteCheck()',
                'htmlOptions' => array('style' => 'width:70px;')
            ),
//            array(
//                'name' => 'Lịch sử Audit',
//                'type' => 'raw',
//                'value' => '$data->getOrderTypeStatusAll()',
//                'htmlOptions' => array('style' => 'width:250px;')
//            ),
            array(
                'header' => 'Actions',
                'class'=>'CButtonColumn',
                'template'=> ControllerActionsName::createIndexButtonRoles($actions, array('commentComplete')),
                'buttons'=>array(
                    'commentComplete'=>array(
                        'visible' => '$data->canActionComplete()',
                        'label'=>'Kiểm tra đơn hàng',
                        'imageUrl'=>Yii::app()->theme->baseUrl . '/images/add2.png',
                        'options'=>array('class'=>'commentComplete'),
                        'url'=>'Yii::app()->createAbsoluteUrl("admin/ReportCall/commentComplete", array("id"=>$data->id) )',
                    ),
                ),
            ),
            ),
    )); ?>
    <?php if($model->canSetEmployee()):?>
    <div class="BoxApply row">
        <?php  echo $form->labelEx($model,'sale_id', ['label'=>'Nhân viên']); ?>
        <?php  echo $form->dropDownList($model,'sale_id', $model->getListEmployeeTest(),array('class'=>'w-200','empty'=>'Xóa nhân viên Telesale hiện tại')); ?>
    </div>
    <div class="row buttons" style="float: left; margin: 0 20px 0 0;">
        <?php $this->widget('bootstrap.widgets.TbButton', array(
        'buttonType'=>'submit',
        'label'=> 'Apply',
        'type'=>'null', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
        'size'=>'small', // null, 'large', 'small' or 'mini'
        )); ?>	
    </div>
    <?php endif;?>
    <?php $this->endWidget(); ?>
</div>
<?php endif; ?>
<script>
$(document).ready(function() {
    fnUpdateColorbox();
});

function fnUpdateColorbox(){   
    fixTargetBlank();
    $(".commentComplete").colorbox({iframe:true,innerHeight:'600', innerWidth: '1000',close: "<span title='close'>close</span>"});
    $('.isDisplayNone').find('input').attr('disabled',true);
}
</script>
