
<center><h1> Kiểm Tra Đơn Hàng Hoàn Thành</h1></center>
<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'borrow-form',
	'enableAjaxValidation'=>false,
        'htmlOptions' => array('enctype' => 'multipart/form-data'),
)); ?>

    <?php echo MyFormat::BindNotifyMsg(); ?>
    
            <div class="row">
                <?php echo $form->label($model,'code_no'); ?>
                <div style="display: inline-block">:
                <?php echo $model->getCodeNo(); ?>
                </div>
            </div>
            <div class="row">
                <?php echo $form->label($model,'customer_id'); ?>
                <div style="display: inline-block">:
                <?php echo $model->getCustomer(); ?>
                </div>
            </div>
    
            <div class="row">
                <?php echo $form->label($model,'phone'); ?>
                <div style="display: inline-block">:
                <?php echo $model->getCustomerPhone(); ?>
                </div>
            </div>
            <div class="row">
                <?php echo $form->label($model,'order_type_status'); ?>
                <div style="display: inline-block">:
                <?php echo $model->getOrderTypeStatusAll(); ?>
                </div>
            </div>
       
            <div class="row">
                <?php echo $form->label($model,'Phản hồi từ KH'); ?>
                <?php echo $form->dropDownList($model,'order_type_status', $model->getArrayStatusOrder(),array('class'=>'w-200','empty'=>'Select')); ?>
            </div>
            <div class="row">
                <?php echo $form->label($model,'comment'); ?>
                <?php echo $form->textArea($model,'comment',array('rows'=>6,'cols'=>80,"placeholder"=>""));?>
            </div>
            <div class="row float_l f_size_19">
                <label>&nbsp;</label>
                <?php echo $form->checkBox($model,'isCancelWrong',array('class'=>'float_l')); ?>
                <?php echo $form->labelEx($model,'Phạt hoàn thành sai quy trình',array('class'=>'checkbox_one_label w-300 l_padding_10', 'style'=>'padding-top:3px;')); ?>
            </div>       
            <div class="clr"></div>
    
    

    <div class="row buttons" style="padding-left: 141px;">
        <?php $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType'=>'submit',
            'label'=>$model->isNewRecord ? 'Create' :'Save',
            'type'=>'null', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
            'size'=>'small', // null, 'large', 'small' or 'mini'
            //'htmlOptions' => array('style' => 'margin-bottom: 10px; float: right;'),
        )); ?>	
        <input class='cancel_iframe' type='button' value='Cancel'>
    </div>

    <br>
    
<?php $this->endWidget(); ?>

</div><!-- form -->



<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/autoNumeric-master/autoNumeric.js"></script>
<script>
    $(document).ready(function(){
        $('.form').find('button:submit').click(function(){
            $.blockUI({ overlayCSS: { backgroundColor: '#fff' } }); 
        });
        fnInitInputCurrency();
        parent.$.fn.yiiGridView.update("borrow-grid");
    });
</script>
