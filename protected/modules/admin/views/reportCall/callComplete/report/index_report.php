<?php
$type = !isset($_GET['type']) ? SellReport::TYPE_REPORT_AGENT : $_GET['type'];
$this->breadcrumbs=array(
    $this->singleTitle,
);
Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('app-text-grid', {
                url : $(this).attr('action'),
		data: $(this).serialize()
	});
	return false;
});
");
?>
<?php include 'index_report_button.php'; ?>
<?php echo CHtml::link('Tìm Kiếm Nâng Cao','#',array('class'=>'search-button')); ?>
<div class="search-form" style="">
<?php $this->renderPartial('callComplete/report/_search_report',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->
<?php
$mSellReport        = new SellReport();
$aList              = $type == SellReport::TYPE_REPORT_AGENT ? $model->searchOrderComplete() : $model->searchOrderComplete(SellReport::TYPE_REPORT_PVKH); // lấy biến
$rData              = $aList['rData'];
$rAgent             = $aList['rAgent'];
$rSumStatusComplete = $aList['rSumStatusComplete'];
$rSumAgent          = $aList['rSumAgent'];
$aStatusComplete      = $aList['statusComplete'];
$mAppCache          = new AppCache();
if($type == 1){
    $aAgent = $mAppCache->getAgentListdata();
}else{
    $aPVKH      = $mAppCache->getListdataUserByRole(ROLE_EMPLOYEE_MAINTAIN);
    $aPTTT      = $mAppCache->getListdataUserByRole(ROLE_EMPLOYEE_MARKET_DEVELOPMENT);
    $aAgent     = $aPTTT + $aPVKH;
    ksort($aAgent);
}
$stt = 0;
?>
<div class="grid-view">
    <table class="tb hm_table">
        <thead>
        <tr style="">
            <th class="w-30 ">STT</th>
            <th class="w-350 ">Tên đại lý</th>
            <th class="w-100 "></th>
            <?php foreach ($aStatusComplete as $statusComplete) : ?>
                <th class="w-150 "><?php echo $statusComplete; ?></th>
            <?php endforeach;?>
        </tr>
        </thead>
        <tbody>
        <tr>
            <td class="item_r item_b" colspan="2">Tổng</td>
            <td class="item_c item_b"><?php echo isset($rSumStatusComplete) ? array_sum($rSumStatusComplete) : 0; ?></td>
            <?php foreach ($aStatusComplete as $key_status => $nameStatus) : ?>
            <td class="item_b item_c "><?php echo isset($rSumStatusComplete[$key_status]) ? $rSumStatusComplete[$key_status] : 0; ?></td>
            <?php endforeach;?>
            
        </tr>
        
        <?php foreach ($rSumAgent as $agent_id => $sumAgent) : 
            $stt++; 
             if(isset($rData[$agent_id])):
                $Status = $rData[$agent_id];
                ?>
                <tr>
                    <td class="item_c"><?php echo $stt; ?></td>
                    <td class=""><?php echo !empty($aAgent[$agent_id]) ? $aAgent[$agent_id] : ''; ?></td>
                    <td class="item_b item_c"><?php echo $sumAgent; ?></td>
                    <?php foreach ($aStatusComplete as $key_status => $item) : ?> 
                    <td class=" item_c"><?php echo !empty($Status[$key_status])? $Status[$key_status] : 0; ?></td>
                    <?php endforeach;?>
                </tr>
        <?php endif; 
            endforeach;
        ?> 
        </tbody>
    </table>
</div>
<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/print-store-card.css" />
<script>
$(document).ready(function() {
    fnUpdateColorbox();
});

function fnUpdateColorbox(){   
    fixTargetBlank();
}
</script>