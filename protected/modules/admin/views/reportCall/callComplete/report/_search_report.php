<?php 
$type = !isset($_GET['type']) ? SellReport::TYPE_REPORT_AGENT : $_GET['type'];
$role = ($type == SellReport::TYPE_REPORT_AGENT) ? ROLE_AGENT : ROLE_EMPLOYEE_MAINTAIN;
$field = ($type == SellReport::TYPE_REPORT_AGENT) ? 'agent_id' : 'employee_maintain_id';
$relation = ($type == SellReport::TYPE_REPORT_AGENT) ? 'rAgent' : 'rEmployeeMaintain';
?>
<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=> GasCheck::getCurl(),
	'method'=>'get',
)); ?>
    
    <div class="row">
        <?php echo $form->labelEx($model,$field); ?>
        <?php echo $form->hiddenField($model,$field, array('class'=>'')); ?>
        <?php
                // 1. limit search kh của sale
                $url = Yii::app()->createAbsoluteUrl('admin/ajax/search_by_role', array('role'=> $role));
                // widget auto complete search user customer and supplier
                $aData = array(
                    'model'=>$model,
                    'field_customer_id'=>$field,
                    'url'=> $url,
                    'name_relation_user'=>$relation,
                    'ClassAdd' => 'w-400',
                    'field_autocomplete_name' => 'autocomplete_name_3',
                    'placeholder'=>$type == SellReport::TYPE_REPORT_AGENT? 'Nhập mã hoặc tên đại lý' : 'Nhập mã hoặc tên NV PVKH',
                );
                $this->widget('ext.SpaAutocompleteCustomer.SpaAutocompleteCustomer',
                    array('data'=>$aData));
                ?>
            <?php echo $form->error($model,'agent_id'); ?>
        </div>
    
    <div class="row more_col">
        <div class="col1">
                <?php echo $form->label($model,'date_from'); ?>
                <?php 
                    $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                        'model'=>$model,        
                        'attribute'=>'date_from',
                        'options'=>array(
                            'showAnim'=>'fold',
                            'dateFormat'=> MyFormat::$dateFormatSearch,
//                            'minDate'=> '0',
                            'maxDate'=> '0',
                            'changeMonth' => true,
                            'changeYear' => true,
                            'showOn' => 'button',
                            'buttonImage'=> Yii::app()->theme->baseUrl.'/admin/images/icon_calendar_r.gif',
                            'buttonImageOnly'=> true,                                
                        ),        
                        'htmlOptions'=>array(
                            'class'=>'w-16 date_from',
                            'size'=>'16',
                            'style'=>'float:left;',                               
                        ),
                    ));
                ?>     		
        </div>
        <div class="col2">
                <?php echo $form->label($model,'date_to'); ?>
                <?php 
                    $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                        'model'=>$model,        
                        'attribute'=>'date_to',
                        'options'=>array(
                            'showAnim'=>'fold',
                            'dateFormat'=> MyFormat::$dateFormatSearch,
//                            'minDate'=> '0',
                            'maxDate'=> '0',
                            'changeMonth' => true,
                            'changeYear' => true,
                            'showOn' => 'button',
                            'buttonImage'=> Yii::app()->theme->baseUrl.'/admin/images/icon_calendar_r.gif',
                            'buttonImageOnly'=> true,                                
                        ),        
                        'htmlOptions'=>array(
                            'class'=>'w-16',
                            'size'=>'16',
                            'style'=>'float:left;',
                        ),
                    ));
                ?>     		
        </div>
    </div>
    
    <div class="row buttons" style="padding-left: 159px;">
        <?php $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType'=>'submit',
            'label'=>'Thống kê',
            'type'=>'null', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
            'size'=>'small', // null, 'large', 'small' or 'mini'
            //'htmlOptions' => array('style' => 'margin-bottom: 10px; float: right;'),
        )); ?>
        
        <a class="btn_cancel" href="<?php   echo Yii::app()->createUrl('admin/reportCall/reportCallComplete/to_excel/'.$type);?>">Xuất Excel</a>
    </div>
    

<?php $this->endWidget(); ?>

</div><!-- search-form -->