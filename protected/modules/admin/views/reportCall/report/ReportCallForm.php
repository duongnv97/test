<?php
$gridCss = Yii::app()->getAssetManager()->publish(Yii::getPathOfAlias('zii.widgets.assets')) . '/gridview/styles.css';
Yii::app()->getClientScript()->registerCssFile($gridCss);
$LIST_CALL_CENTER_USER = $data['LIST_CALL_CENTER_USER'];
$index = 1;
$DATE_ARRAY = isset($data['DATE_ARRAY']) ? $data['DATE_ARRAY'] : array();
$OUTPUT_SUM = isset($data['OUTPUT_SUM']) ? $data['OUTPUT_SUM'] : array();
$OUTPUT = isset($data['OUTPUT']) ? $data['OUTPUT'] : array();
$OUTPUT_SUM_SALE = isset($data['OUTPUT_SUM_SALE']) ? $data['OUTPUT_SUM_SALE'] : array();
$OUTPUT_SALE = isset($data['OUTPUT_SALE']) ? $data['OUTPUT_SALE'] : array();
// Get table height
$freeHeightTable = (sizeof($OUTPUT_SUM) + 1) * 19;
$freeHeightTable = $freeHeightTable > 400 ? 400 : $freeHeightTable;

// For piechart all total call
$totalDataChart[] = ['Nhân viên', 'Cuộc gọi'];
$totalDataChartSell[] = ['Nhân viên', 'Bán hàng'];
?>
<script type="text/javascript"
        src="<?php echo Yii::app()->theme->baseUrl; ?>/js/scroll/jquery.freezetablecolumns.1.1.js"></script>
<div class="grid-view display_none">
    <table id="freezetablecolumns_report_call" class="items hm_table freezetablecolumns">
        <thead>
        <tr  class="h_20">
            <th class="w-20 ">#</th>
            <th class="w-180 ">Nhân viên tổng đài</th>
            <th class="w-80 ">Tổng</th>
            <th class="w-30 ">%</th>
            <?php foreach ($DATE_ARRAY as $days): ?>
                <th class="item_b"><?php echo substr($days, -2); ?></th>
            <?php endforeach; ?>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($OUTPUT_SUM as $user_id => $sumCall): ?>
            <?php
            $nameEmployee = $user_id;
            $saleSum = $saleSuccess = $sumRow = 0;
            $nameEmployee = Sta3::getCallUserNameFromID($LIST_CALL_CENTER_USER, $user_id);
            if (isset ($OUTPUT_SUM_SALE[$user_id][0])) {
                $saleSum = $OUTPUT_SUM_SALE[$user_id][0] * 1;
            }
            if (isset ($OUTPUT_SUM_SALE[$user_id][Sell::STATUS_PAID])) {
                $saleSuccess = $OUTPUT_SUM_SALE[$user_id][Sell::STATUS_PAID] * 1;
            }

            // Add to piechart data
            $totalDataChart[] = [$nameEmployee, $sumCall];
            $totalDataChartSell[] = [$nameEmployee, $saleSum * 1];

            ?>
            <tr class="h_20">
                <td class="item_c"><?php echo $index++; ?></td>
                <td class="item_b"><?php echo $nameEmployee; ?></td>
                <td class="item_c item_b"><?php echo '' . $saleSuccess . '/' . $saleSum . '/' . $sumCall; ?></td>
                <td class="item_c item_b"><?php echo '' . round(($saleSuccess / (($saleSum) ? $saleSum : 1)) * 100) . '%'; ?></td>
                <?php foreach ($DATE_ARRAY as $days): ?>
                    <?php
                    $value          = isset($OUTPUT[$user_id][$days]) ? $OUTPUT[$user_id][$days] : '0';
                    $saleValue      = isset($OUTPUT_SALE[$user_id][$days][0]) ? $OUTPUT_SALE[$user_id][$days][0] : '0';
                    $successValue   = isset($OUTPUT_SALE[$user_id][$days][Sell::STATUS_PAID]) ? $OUTPUT_SALE[$user_id][$days][Sell::STATUS_PAID] : '0';
                    $text = $successValue . '/' . $saleValue . '/' . $value;
                    if ($text == '0/0/0'):
                        $text = '';
                    endif;
                    ?>
                    <td class="w-60 item_c"><?php echo $text; ?></td>
                <?php endforeach; ?>
            </tr>
        <?php endforeach; ?>
        </tbody>
    </table>
</div>


<script type="text/javascript">
    fnAddClassOddEven('items');
    $('.freezetablecolumns').each(function () {
        var id_table = $(this).attr('id');
        $('#' + id_table).freezeTableColumns({
            width:       <?php echo Users::WIDTH_freezetable;?>,   // required - bt 1100
            height:      <?php echo $freeHeightTable;?>,   // required
            numFrozen: 4,     // optional
            frozenWidth: 355,   // optional
            clearWidths: true  // optional
        });
    });
    $(window).load(function () {
        var index = 1;
        $('.freezetablecolumns').each(function () {
            if (index == 1)
                $(this).closest('div.grid-view').show();
            index++;
        });
        fnAddClassOddEven('items');
    });
</script>
<?php include('chartPercentSuccess.php'); ?>
