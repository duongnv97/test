<?php
$this->breadcrumbs=array(
    $this->pageTitle,
);
?>

<h1><?php echo $this->pageTitle;?></h1>
<div class="search-form" style="">
<?php $this->renderPartial('report/_search',array(
        'model'=>$model,
)); ?>
</div><!-- search-form -->

<style>
    .ui-datepicker-trigger { float: left;}
</style>

<script>
$(document).ready(function(){
    $('.form').find('button:submit').click(function(){        
        $.blockUI({ overlayCSS: { backgroundColor: '#fff' } }); 
    });   
 
});
</script>

<?php if(isset($data['OUTPUT'])): ?>
    <?php include 'ReportCallForm.php'; ?>
<?php endif; ?>