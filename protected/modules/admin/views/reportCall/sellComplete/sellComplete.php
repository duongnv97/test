<?php
$this->breadcrumbs=array(
	$this->singleTitle,
);

$allowCommentCancel = true;

$menus=array(
);
$this->menu= ControllerActionsName::createMenusRoles($menus, $actions);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
");
?>
<h1><?php echo $this->pageTitle; ?></h1>
<?php echo MyFormat::BindNotifyMsg(); ?>
<?php echo CHtml::link('Tìm Kiếm Nâng Cao','#',array('class'=>'search-button')); ?>
<div class="search-form" style="">
<?php $this->renderPartial('sellComplete/_search',array(
        'model'=>$model,
)); ?>
</div><!-- search-form -->
<?php if(!empty($aData['REPORT'])): ?>    
<?php
$i = 1;
$gridCss = Yii::app()->getAssetManager()->publish(Yii::getPathOfAlias('zii.widgets.assets')) . '/gridview/styles.css';
Yii::app()->getClientScript()->registerCssFile($gridCss);
?>
<div class="grid-view">
    <table class="items hm_table">
        <thead>
        <tr>
            <th rowspan="2" class="w-20 item_b">#</th>
            <th rowspan="2" class="w-150 item_b">Đại lý</th>
            <?php $aZone = $model->getArrayZone();?>
            <?php foreach ($aZone as $key => $zone): ?>
            <th colspan="<?php echo isset($aData['SOURCE_ID']) ? count($aData['SOURCE_ID']) : '1' ?>" class="w-50 item_b"><?php echo !empty($zone['title']) ? $zone['title'] : ''; ?></th>
            <?php endforeach; ?>
            <th rowspan="2" class="w-150 item_b">Tổng</th>
        </tr>
        <tr>
            <?php $aZone = $model->getArrayZone();?>
            <?php foreach ($aZone as $key => $zone): ?>
                <?php foreach ($aData['SOURCE_ID'] as $keySource): ?>
                    <th ><?php echo isset($aData['SOURCE'][$keySource]) ? $aData['SOURCE'][$keySource] : ''; ?></th>
                <?php endforeach; ?>
            <?php endforeach; ?>
        </tr>
        </thead>
        <tbody>
            <tr class="item_b item_c needCoppy"></tr>
            <?php 
                $aSum =[];
                $sumFinal = 0;
            ?>
            <?php foreach ($aData['REPORT'] as $agent_id => $aReport): ?>
            <?php $sumCurrent = 0; ?>
            <tr>
                <td class="item_c"><?php echo $i++; ?></td>
                <td><?php echo !empty($aData['AGENT_MODEL'][$agent_id]) ? $aData['AGENT_MODEL'][$agent_id]->first_name : ''; ?></td>
                <?php foreach ($aZone as $key => $zone): ?>
                    <?php foreach ($aData['SOURCE_ID'] as $keySource): ?>
                        <?php
                            if(isset($aSum[$keySource][$key])){
                                $aSum[$keySource][$key] += isset($aReport[$keySource][$key]) ? $aReport[$keySource][$key] : 0;
                            }else{
                                $aSum[$keySource][$key] = isset($aReport[$keySource][$key]) ? $aReport[$keySource][$key] : 0;
                            }
                            $sumCurrent += isset($aReport[$keySource][$key]) ? $aReport[$keySource][$key] : 0;
                            $sumFinal   +=isset($aReport[$keySource][$key]) ? $aReport[$keySource][$key] : 0;
                        ?>
                        <td class="item_c"><?php echo isset($aReport[$keySource][$key]) ? $aReport[$keySource][$key] : ''; ?></td>
                    <?php endforeach; ?>
                <?php endforeach; ?>
                <td class="item_c item_b"><?php echo $sumCurrent; ?></td>
            </tr>
            <?php endforeach; ?>
            <tr class="item_b item_c targetCoppy">
                <td colspan="2">Tổng</td>
                <?php foreach ($aZone as $key => $zone): ?>
                    <?php foreach ($aData['SOURCE_ID'] as $keySource): ?>
                        <td ><?php echo isset($aSum[$keySource][$key]) ? $aSum[$keySource][$key] : 0; ?></td>
                    <?php endforeach; ?>
                <?php endforeach; ?>
                <td><?php echo $sumFinal; ?></td>
            </tr>
        </tbody>
    </table>
</div>
<script>
$(window).load(function () {
    fnAddClassOddEven('items');
    $('.needCoppy').html($('.targetCoppy').html());
});
</script>
<?php endif; ?>
