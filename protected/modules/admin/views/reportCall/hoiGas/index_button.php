<div class="form">
    <?php
        $LinkNormal         = Yii::app()->createAbsoluteUrl('admin/reportCall/HoiGas');
        $LinkRefCode        = Yii::app()->createAbsoluteUrl('admin/reportCall/HoiGas', array('type'=> CallCenter::TYPE_HOI_GAS_GS_DETAIL));
        $LinkCodeEmployee   = Yii::app()->createAbsoluteUrl('admin/reportCall/HoiGas', array('type'=> CallCenter::TYPE_HOI_GAS_AGENT));
    ?>
    <h1>Danh sách 
        <a class='btn_cancel f_size_14 <?php echo (!isset($_GET['type'])) ? "active":"";?>' href="<?php echo $LinkNormal;?>">Hối gas giám sát</a>
        <a class='btn_cancel f_size_14 <?php echo isset($_GET['type']) && $_GET['type']==CallCenter::TYPE_HOI_GAS_GS_DETAIL ? 'active':'';?>' href="<?php echo $LinkRefCode;?>">Hối gas giám sát chi tiết</a>
        <a class='btn_cancel f_size_14 <?php echo isset($_GET['type']) && $_GET['type']==CallCenter::TYPE_HOI_GAS_AGENT ? 'active':'';?>' href="<?php echo $LinkCodeEmployee;?>">Hối gas đại lý</a>
    </h1> 
</div>