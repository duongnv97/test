<?php 
    $aData = $model->getHoiGasDetail();
    $index = 1;
    $totalDataChart = array();
    $listTitleChart = array('Tổng số hối gas');
?>
<?php echo CHtml::link('Tìm Kiếm Nâng Cao','#',array('class'=>'search-button')); ?>
<div class="search-form" style="">
<?php $this->renderPartial('hoiGas/_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->
<div class="grid-view">
    <table class="items hm_table tableReport" style='width:100%;'>
        <thead>
            <tr>
                <th class='item_c'>#</th>
                <th class='item_c'>Đại lý</th>
                <th class='item_c'>Tổng hối Gas</th>
            </tr>
        </thead>
        <tbody>
            <?php 
            if(isset($aData['Sum']) && is_array($aData['Sum'])){
                foreach ($aData['Sum'] as $key => $value) { ?>
            <tr>
                <td><?php echo $index++;?></td>
                <td>
                    <?php 
                        $name = isset($aData['GS'][$key]) ? $aData['GS'][$key]['first_name'] : "$key";
                        $totalDataChart[] = array(
                                            $name,
                                            $value*1
                                        );
                        echo $name;
                    ?>
                </td>
                <td><?php echo $value;?></td>
            </tr>
                <?php }
            } ?>
        </tbody>
    </table>
</div>
<?php if(count($totalDataChart) >0){
        include 'chartHoiGas.php';
        }
?>