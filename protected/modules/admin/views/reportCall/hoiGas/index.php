<?php 
$gridCss = Yii::app()->getAssetManager()->publish(Yii::getPathOfAlias('zii.widgets.assets')) . '/gridview/styles.css';
Yii::app()->getClientScript()->registerCssFile($gridCss);
?>
<?php
$this->breadcrumbs=array(
    $this->singleTitle,
);

$menus=array(
    array('label'=>"Create $this->singleTitle", 'url'=>array('create')),
);
$this->menu= ControllerActionsName::createMenusRoles($menus, $actions);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('app-promotion-grid', {
                url : $(this).attr('action'),
		data: $(this).serialize()
	});
	return false;
});
");

Yii::app()->clientScript->registerScript('ajaxupdate', "
$('#app-promotion-grid a.ajaxupdate').live('click', function() {
    $.fn.yiiGridView.update('app-promotion-grid', {
        type: 'POST',
        url: $(this).attr('href'),
        success: function() {
            $.fn.yiiGridView.update('app-promotion-grid');
        }
    });
    return false;
});
");
?>

<?php include "index_button.php"; ?>
<?php 
    if(isset($_GET['type']) && $_GET['type']==CallCenter::TYPE_HOI_GAS_GS_DETAIL){
        include '_form_detail.php';
    }elseif(isset($_GET['type']) && $_GET['type']==CallCenter::TYPE_HOI_GAS_AGENT) {
        include '_form_agent.php';
    }else{
        include '_form_default.php';
    }
?>
<script>
$(document).ready(function() {
    fnUpdateColorbox();
});

function fnUpdateColorbox(){   
    fixTargetBlank();
}
</script>