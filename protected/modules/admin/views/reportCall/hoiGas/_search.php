<?php 
$aSwitch = !isset($_GET['type']) ? array('to_excel' => 1) : array('to_excel' => 1,'type' => $_GET['type']);
$urlExcel = Yii::app()->createAbsoluteUrl('admin/reportCall/HoiGas',$aSwitch);
?>
<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=> GasCheck::getCurl(),
	'method'=>'get',
)); ?>
    <div class="row">
        <?php echo $form->labelEx($model,'user_id'); ?>
        <?php echo $form->hiddenField($model,'user_id'); ?>
        <?php
            // 1. limit search kh của sale
            $url = Yii::app()->createAbsoluteUrl('admin/ajax/search_for_user_login');
            // widget auto complete search user customer and supplier
            $aData = array(
                'model'=>$model,
                'field_customer_id'=>'user_id',
                'url'=> $url,
                'name_relation_user'=>'rUser',
                'ClassAdd' => 'w-400',
                'field_autocomplete_name' => 'autocomplete_user',
                'placeholder'=>'Nhập mã hoặc tên nhân viên',
            );
            $this->widget('ext.SpaAutocompleteCustomer.SpaAutocompleteCustomer',
                array('data'=>$aData));
            ?>
        <?php echo $form->error($model,'user_id'); ?>
    </div>
    <div class="row more_col">
        <div class="col1">
            <?php echo $form->labelEx($model, 'date_from', []); ?>
             <?php 
                $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                    'model'=>$model,        
                    'attribute'=>'date_from',
                    'options'=>array(
                        'showAnim'=>'fold',
                        'dateFormat'=> MyFormat::$dateFormatSearch,
                //                            'minDate'=> '0',
                //                            'maxDate'=> '0',
                        'changeMonth' => true,
                        'changeYear' => true,
                        'showOn' => 'button',
                        'buttonImage'=> Yii::app()->theme->baseUrl.'/admin/images/icon_calendar_r.gif',
                        'buttonImageOnly'=> true,                                
                    ),        
                    'htmlOptions'=>array(
                        'class'=>'w-16',
                        'style'=>'height:20px;',
//                            'readonly'=>'readonly',
                    ),
                ));
                ?>  
            <?php echo $form->error($model,'date_from'); ?>
        </div>
        <div class="col1">
            <?php echo $form->labelEx($model, 'date_to', []); ?>
             <?php 
                $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                    'model'=>$model,        
                    'attribute'=>'date_to',
                    'options'=>array(
                        'showAnim'=>'fold',
                        'dateFormat'=> MyFormat::$dateFormatSearch,
                //                            'minDate'=> '0',
                //                            'maxDate'=> '0',
                        'changeMonth' => true,
                        'changeYear' => true,
                        'showOn' => 'button',
                        'buttonImage'=> Yii::app()->theme->baseUrl.'/admin/images/icon_calendar_r.gif',
                        'buttonImageOnly'=> true,                                
                    ),        
                    'htmlOptions'=>array(
                        'class'=>'w-16',
                        'style'=>'height:20px;',
//                            'readonly'=>'readonly',
                    ),
                ));
                ?>  
            <?php echo $form->error($model,'date_to'); ?>
        </div>
    </div>
    <div class="row">
        <?php echo $form->labelEx($model, 'type', []); ?>
        <?php echo $form->dropDownList($model,'type', $model->getArrayTypeCall(),array('class'=>'w-200', 'empty'=>'Select')); ?>
    </div>
    <div class="row buttons" style="padding-left: 159px;">
        <?php $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType'=>'submit',
            'label'=>'Search',
            'type'=>'null', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
            'size'=>'small', // null, 'large', 'small' or 'mini'
            //'htmlOptions' => array('style' => 'margin-bottom: 10px; float: right;'),
        )); ?>
        &nbsp;&nbsp;&nbsp;&nbsp;<a class='btn_cancel' href='<?php echo $urlExcel;?>'>Xuất Excel</a>
<?php $this->endWidget(); ?>
    </div>
    

</div><!-- search-form -->