<?php
$this->breadcrumbs = array(
	$this->pluralTitle => array('index'),
        $model->id=>array('view','id'=>$model->id),
	'Cập Nhật ' . $this->singleTitle,
);

$menus = array(	
    array('label' => $this->pluralTitle, 'url' => array('index')),
);
$this->menu= ControllerActionsName::createMenusRoles($menus, $actions);

?>

<h1><?php echo $this->singleTitle; ?></h1>

<?php echo MyFormat::BindNotifyMsg(); ?>

<?php 
//$this->widget('HrReviewWidget',
//            array('data' => $data));
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'hr-approved-reports-form',
	'enableAjaxValidation'=>false,
        'htmlOptions' => array('enctype' => 'multipart/form-data'),
)); ?>

    <p class="note">Dòng có dấu <span class="required">*</span> là bắt buộc.</p>
    
    <div class="row">
        <?php echo $form->labelEx($model,'note'); ?>
        <?php echo $form->textArea($model,'note',array('rows'=>6, 'cols'=>50)); ?>
        <?php echo $form->error($model,'note'); ?>
    </div>

	<div class="row buttons" style="padding-left: 141px;">
            <?php $this->widget('bootstrap.widgets.TbButton', array(
                'buttonType'=>'submit',
                'label'=> $model->isTreasurer() ? 'Xác nhận chi lương' : 'Duyệt',
                'type'=>'null', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
                'size'=>'small', // null, 'large', 'small' or 'mini'
                'htmlOptions' => array('name' => 'approve'),
            )); ?>	
            <?php $this->widget('bootstrap.widgets.TbButton', array(
                'buttonType'=>'submit',
                'label'=>'Yêu cầu cập nhật',
                'type'=>'null', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
                'size'=>'small', // null, 'large', 'small' or 'mini'
                'htmlOptions' => array('name' => 'requestUpdate'),
            )); ?>	
        </div>

<?php $this->endWidget(); ?>

</div><!-- form -->
<script>
    $(document).ready(function(){
        $('.form').find('button:submit').click(function(){
            $.blockUI({ overlayCSS: { backgroundColor: '#fff' } }); 
        });
        $('.panel li.list-group-item').click(function() {
            $('.panel').removeClass('panel-active');
            if(!$(this).siblings('.collapse').hasClass('in'))
                $(this).parents('.panel').addClass('panel-active');
        });
    });
</script>