<div class="form" style="margin: 20px 0">
    <?php
        $LinkDefault    = Yii::app()->createAbsoluteUrl('admin/hrApprovedReports/index', ['type'=> HrApprovedReports::TYPE_GENERAL]);
        $linkDetail     = Yii::app()->createAbsoluteUrl('admin/hrApprovedReports/index', ['type'=> HrApprovedReports::TYPE_DETAIL]);
        $type           = isset($_GET['type']) ? $_GET['type'] : HrApprovedReports::TYPE_DETAIL;
        $cs             = Yii::app()->getClientScript();
        $css            = Yii::app()->theme->baseUrl . '/admin/css/hr.css';
        $cs->registerCssFile($css);
    ?>
    <a class='btn_cancel f_size_14 <?php echo $type == HrApprovedReports::TYPE_DETAIL  ? "active":"";  ?>' href="<?php echo $linkDetail; ?>">Chi tiết</a>
    <?php if( !$model->isTreasurer()): ?>
    <a class='btn_cancel f_size_14 <?php echo $type == HrApprovedReports::TYPE_GENERAL ? "active":""; ?>' href="<?php echo $LinkDefault; ?>">Tổng hợp</a>
    <?php endif; ?>
</div>

<?php 
if($type == HrApprovedReports::TYPE_DETAIL){
    $this->renderPartial('index_detail',array(
        'model'=>$model, 'actions' => $this->listActionsCanAccess,
    ));
} else {
    $this->renderPartial('index_general',array(
        'model'=>$model, 'actions' => $this->listActionsCanAccess,
    ));
}
?>