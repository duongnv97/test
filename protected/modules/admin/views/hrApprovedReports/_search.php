<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=> GasCheck::getCurl(),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'code_no',array()); ?>
		<?php echo $form->textField($model,'code_no',array('class'=>'w-300')); ?>
	</div>
    
	<div class="row">
		<?php echo $form->label($model,'status',array()); ?>
		<?php echo $form->dropdownList($model,'status', HrApprovedReports::$aStatus,array('class'=>'w-300', 'empty'=>'Select')); ?>
	</div>


	<div class="row buttons" style="padding-left: 159px;">
		        <?php $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType'=>'submit',
            'label'=>'Search',
            'type'=>'null', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
            'size'=>'small', // null, 'large', 'small' or 'mini'
            //'htmlOptions' => array('style' => 'margin-bottom: 10px; float: right;'),
        )); ?>	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->