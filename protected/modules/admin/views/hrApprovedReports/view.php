<?php
$this->breadcrumbs=array(
    $this->pluralTitle => array('index'),
    ' ' . $this->singleTitle . ' : ',
);

$type  = isset($_GET['type']) ? $_GET['type'] : HrApprovedReports::TYPE_DETAIL;
$menus = array(
	array(
            'label'=>"Export Excel", 
            'url'=>array('exportExcel', 'type'=>$type),
            'linkOptions'=>array(
                'style'=>'background:url('.Yii::app()->theme->baseUrl.'/admin/images/icon/excel.png)',
            ),
            'visible'=> $type == HrApprovedReports::TYPE_DETAIL ? $model->canApprove() : true,
            'htmlOptions'=>array(
                'label'=>'Xuất Excel',
                'class'=>'update',
                ),
        ),
	array('label'=>"$this->pluralTitle Management", 'url'=>array('index')),
        $type == HrApprovedReports::TYPE_GENERAL ?: // Chỉ duyệt ở tab detail
	array(
            'label'=>$model->isTreasurer() ? 'Xác nhận chi lương' : 'Duyệt',
            'url'=>array('approve', 'id'=>$model->id), 
            'linkOptions'=>array(
                'style'=>'background:url('.Yii::app()->theme->baseUrl.'/admin/images/icon/icon-32-active.png)',
                ),
            'visible'=>$model->canApprove(),
            'htmlOptions'=>array(
                'label'=> $model->isTreasurer() ? 'Xác nhận chi' : 'Duyệt',
                'class'=>'update',
                ),
            ),
);
$this->menu= ControllerActionsName::createMenusRoles($menus, $actions);
?>

<h1>Xem và <?php echo $this->singleTitle; ?></h1>

<h3 class='title-info'><?php echo 'Bảng tổng hợp lương T'.date('m/Y', strtotime($modelReview->start_date)); ?></h3>
<?php 
//$this->widget('application.components.widgets.XDetailView', array(
//    'data' => $model,
//    'attributes' => array(
//        'group11'=>array(
//            'ItemColumns' => 3,
//            'attributes' => array(
//                    'code_no',
//                [
//                    'name'=>'name',
//                    'value'=>$model->getName()
//                ],
//                [
//                    'name'=>'id_approved_lv1',
//                    'value'=>$model->getApprovedLv1()
//                ],
//                [
//                    'name'=>'date_approved_lv1',
//                    'value'=>$model->getApprovedDate($model->date_approved_lv1)
//                ],
//                [
//                    'name'=>'id_approved_lv2',
//                    'value'=>$model->getApprovedLv2()
//                ],
//                [
//                    'name'=>'date_approved_lv2',
//                    'value'=>$model->getApprovedDate($model->date_approved_lv2)
//                ],
//                [
//                    'name'=>'id_treasurer',
//                    'value'=>$model->getTreasurer()
//                ],
//                [
//                    'name'=>'date_treasurer',
//                    'value'=>$model->getApprovedDate($model->date_treasurer)
//                ],
//		'note',
//		[
//                    'name'=>'status',
//                    'value'=>$model->getStatus()
//                ],
//		[
//                    'name'=>'created_date',
//                    'value'=>$model->getCreatedDate()
//                ],
//                    
//            ),
//        ),
//    ),
//)); 

$cs     = Yii::app()->getClientScript();
$css    = Yii::app()->theme->baseUrl . '/admin/css/hr.css';
$cs->registerCssFile($css);
if( $model->isTreasurer() ){
    $this->widget('HrReviewWidget',
            array('model'=>$modelReview));
} else {
    if( $type == HrApprovedReports::TYPE_GENERAL ){ // Bảng lương tổng hợp
        $this->widget('HrReviewWidget',
            array('model'=>$modelReview));
    } elseif( $type == HrApprovedReports::TYPE_DETAIL ){ // Bảng lương chi tiết
        $mReport = new HrSalaryReports();
        $mReport = $mReport->getReportSameCodeNo($model->code_no);
        $data    = json_decode($mReport->data, true);
        $this->widget('HrTableViewWidget', 
                ['data'=>$data, 'fixedHeader'=>true, 'enableScript'=>true]);
    }
}

?>
<div class="row" style="margin: 20px 0">
    <?php $url = Yii::app()->createAbsoluteUrl("admin/hrApprovedReports/approve", ['id'=>$model->id]); ?>
    <a href="<?php echo $url; ?>" 
       style="background:rgb(0, 128, 255); font-weight:600; border-radius:5px; padding:10px; color:white; visibility: <?php echo $model->canApprove() ? 'visible':'hidden'; ?>">
        <?php echo $model->isTreasurer() ? 'Xác nhận chi':'Duyệt'; ?>
    </a>
</div>