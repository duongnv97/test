<?php
$this->breadcrumbs=array(
	$this->singleTitle,
);

$menus=array(
//            array('label'=>"Create $this->singleTitle", 'url'=>array('create')),
);
$this->menu= ControllerActionsName::createMenusRoles($menus, $actions);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('hr-approved-reports-grid', {
                url : $(this).attr('action'),
		data: $(this).serialize()
	});
	return false;
});
");

Yii::app()->clientScript->registerScript('ajaxupdate', "
$('#hr-approved-reports-grid a.ajaxupdate').live('click', function() {
    $.fn.yiiGridView.update('hr-approved-reports-grid', {
        type: 'POST',
        url: $(this).attr('href'),
        success: function() {
            $.fn.yiiGridView.update('hr-approved-reports-grid');
        }
    });
    return false;
});
");
?>

<h1>Danh Sách <?php echo $this->pluralTitle; ?></h1>


<?php echo CHtml::link('Tìm Kiếm Nâng Cao','#',array('class'=>'search-button')); ?>
<div class="search-form" >
<?php $this->renderPartial('_search_general',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'hr-approved-reports-grid',
	'dataProvider'=>$model->searchGeneralReport(),
        'afterAjaxUpdate'=>'function(id, data){ fnUpdateColorbox();}',
        'template'=>'{pager}{summary}{items}{pager}{summary}',
//    'itemsCssClass' => 'items custom_here',
//    'htmlOptions'=> array('class'=>'grid-view custom_here'),
        'pager' => array(
            'maxButtonCount'=>  CmsFormatter::$PAGE_MAX_BUTTON,
        ),
	'enableSorting' => false,
	//'filter'=>$model,
	'columns'=>array(
        array(
            'header' => 'S/N',
            'type' => 'raw',
            'value' => '$this->grid->dataProvider->pagination->currentPage * $this->grid->dataProvider->pagination->pageSize + ($row+1)',
            'headerHtmlOptions' => array('width' => '10px','style' => 'text-align:center;'),
            'htmlOptions' => array('style' => 'text-align:center;')
        ),
		'code_no',
            [
                'name'=>'name',
                'value'=>'$data->getNameForApprove()'
            ],
            [
                'name'=>'start_date',
                'value'=>'$data->getStartDate()'
            ],
            [
                'name'=>'end_date',
                'value'=>'$data->getEndDate()'
            ],
            [
                'name'=>'created_date',
                'value'=>'$data->getCreatedDate()'
            ],
            
		array(
                    'header' => 'Actions',
                    'class'=>'CButtonColumn',
                    'template'=> ControllerActionsName::createIndexButtonRoles($actions),
                    'buttons'=>array(
                        'update' => array(
                            'visible' => 'false',
                        ),
                        'delete' => array(
                            'visible' => 'false',
                        ),
                        'view' => array(
                            'url' => 'Yii::app()->createUrl("admin/hrApprovedReports/view", ["id"=>$data->id,"type"=>HrApprovedReports::TYPE_GENERAL])'
                        )
                    ),
		),
	),
)); ?>

<script>
$(document).ready(function() {
    fnUpdateColorbox();
});

function fnUpdateColorbox(){   
    fixTargetBlank();
}
</script>