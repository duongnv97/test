<?php
$this->breadcrumbs = array(
    $this->pluralTitle => array('index'),
    ' ' . $this->singleTitle . ' : ',
);

$menus = array(
    array('label' => "$this->pluralTitle Management", 'url' => array('index')),
    array('label' => "Tạo Mới $this->singleTitle", 'url' => array('create')),
    array('label' => "Cập Nhật $this->singleTitle", 'url' => array('update', 'id' => $model->id)),
    array('label' => "Xóa $this->singleTitle", 'url' => array('delete'), 'linkOptions' => array('submit' => array('delete', 'id' => $model->id), 'confirm' => 'Bạn chắc chắn muốn xóa ?')),
);
$this->menu = ControllerActionsName::createMenusRoles($menus, $actions);
?>

<h1>Xem: <?php echo $this->singleTitle; ?></h1>

<?php
$this->widget('zii.widgets.CDetailView', array(
    'data' => $model,
    'attributes' => array(
        array(
            'name' => 'user_id',
            'value' => $model->getUserName(),
        ),
        array(
            'name' => 'amount',
            'value' => $model->getAmount(),
        ),
        'reason',
        array(
            'name'  => 'month',
            'value' => $model->getMonth(),
        ),
        array(
            'name' => 'type',
            'value' => $model->getTypesText(),
        ),
        array(
            'name' => 'relate_id',
            'value' => $model->getRelationInfo(),
        ),
        array(
            'name' => 'status',
            'value' => $model->getStatusText(),
        ),
        array(
            'name' => 'created_by',
            'value' => $model->getCreatedBy(),
        ),
        array(
            'name' => 'created_date',
            'value' => $model->getCreatedDate(),
        ),
    ),
));
?>
