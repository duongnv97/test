<div class="form">
    <?php
        $LinkGeneral = Yii::app()->createAbsoluteUrl('admin/gasDebts/reportDebtsGeneral');
        $linkDetail  = Yii::app()->createAbsoluteUrl('admin/gasDebts/reportDebtsDetail');
    ?>
    <h1>Báo cáo công nợ nhân viên
        <?php if(GasCheck::isAllowAccess('gasDebts', 'reportDebtsGeneral') && $model->canViewReportDebts()): ?>
        <a class='btn_cancel f_size_14 <?php echo $this->isGeneralTab() ? 'active' : '' ?>' href="<?php echo $LinkGeneral;?>">Tổng quát</a>
        <?php endif; ?>
        
        <?php if(GasCheck::isAllowAccess('gasDebts', 'reportDebtsDetail')): ?>
        <a class='btn_cancel f_size_14 <?php echo $this->isGeneralTab() ? '' : 'active' ?>' href="<?php echo $linkDetail;?>">Chi tiết</a>
        <?php endif; ?>
    </h1>
</div>