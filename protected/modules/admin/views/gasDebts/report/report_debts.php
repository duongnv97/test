<?php 
$this->breadcrumbs = array(
    $this->singleTitle,
);
?>
<?php echo MyFormat::BindNotifyMsg(); ?>
<div class="form">
<?php include 'index_button.php';?>
</div>


<?php if(!$this->isGeneralTab()) include 'report_debts_detail.php'; ?>

<?php 
if( $this->isGeneralTab() ):
    echo CHtml::link('Tìm Kiếm Nâng Cao', '#', array('class' => 'search-button')); ?>
    <div class="search-form">
        <?php
        $this->renderPartial('report/_search', array(
            'model' => $model,
        ));
        ?>
    </div><!-- search-form -->
<?php endif; ?>

<?php if( empty($aData['MODEL']) || !$this->isGeneralTab() ) return;
$no            = 1; 
$aPositionWork = UsersProfile::model()->getArrWorkRoom();
$aProvince     = GasProvince::getArrAll();
$sumOpeningDebit  = 0;
$sumOpeningCredit = 0;
$sumClosingDebit  = 0;
$sumClosingCredit = 0;
$sumIncurredDebit = 0;
$sumIncurredCredit= 0;
?>

<?php 
// Form nhập trừ lương
if($model->canCutSalary()): ?>
<div class="wide form">
<?php
    $form=$this->beginWidget('CActiveForm', array(
            'id'=>'inventory-customer-form',
            'enableAjaxValidation'=>false,
//            'action' => ['/admin/gasDebts/reportDebtsGeneral'],
            'htmlOptions' => array('enctype' => 'multipart/form-data'),
    )); 
endif;
?>
<div id="gas-debts-report-grid" class="grid-view">
    <table class="tb hm_table tb_sticky">
        <thead>
            <tr>
                <th>#</th>
                <th>Mã NV</th>
                <th>Tên nhân viên</th>
                <th>Bộ phận</th>
                <th>Tỉnh</th>
                <th>Ngày vào làm</th>
                <th>Ngày nghỉ việc</th>
                <th>Nợ đầu kỳ</th>
                <th>Có đầu kỳ</th>
                <th>Phát sinh nợ</th>
                <th>Phát sinh có</th>
                <th>Nợ cuối kỳ</th>
                <th>Có cuối kỳ</th>
                <?php if($model->canCutSalary()): ?>
                <th>Trừ lương</th>
                <?php endif; ?>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($aData['MODEL'] as $uid => $item): ?>
                <?php 
                $urlDetail      = Yii::app()->createAbsoluteUrl("admin/gasDebts/reportDebtsDetail", 
                                    [
                                        'GasDebtsReport'=>[
                                            'user_id'=>$uid, 
                                            'date_from'=>$model->date_from,
                                            'date_to'=>$model->date_to,
                                        ]
                                    ]);
                $beginJob       = empty($item->date_begin_job) ? '' : MyFormat::dateConverYmdToDmy($item->date_begin_job);
                $endJob         = empty($item->leave_date) || $item->leave_date == '0000-00-00' ? '' : MyFormat::dateConverYmdToDmy($item->leave_date);
                
                $openingDebit   = empty($aData['OPENING_DEBIT'][$uid]) ? 0 : $aData['OPENING_DEBIT'][$uid]; // Nợ đầu kỳ
                $openingCredit  = empty($aData['OPENING_CREDIT'][$uid]) ? 0 : $aData['OPENING_CREDIT'][$uid]; // Có đầu kỳ
                $incurredDebit  = empty($aData['INCURRED_DEBIT'][$uid]) ? 0 : $aData['INCURRED_DEBIT'][$uid]; // Phát sinh nợ
                $incurredCredit = empty($aData['INCURRED_CREDIT'][$uid]) ? 0 : $aData['INCURRED_CREDIT'][$uid]; // Phát sinh có
                $closingDebit   = empty($aData['CLOSING_DEBIT'][$uid]) ? 0 : $aData['CLOSING_DEBIT'][$uid]; // Nợ cuối kỳ
                $closingCredit  = empty($aData['CLOSING_CREDIT'][$uid]) ? 0 : $aData['CLOSING_CREDIT'][$uid]; // Có cuối kỳ
                $sumOpeningDebit    += $openingDebit;
                $sumOpeningCredit   += $openingCredit;
                $sumClosingDebit    += $closingDebit;
                $sumClosingCredit   += $closingCredit;
                $sumIncurredDebit   += $incurredDebit;
                $sumIncurredCredit  += $incurredCredit;
                ?>
                <tr>
                    <td class="item_c"><?php echo $no++; ?></td>
                    <td class="item_c"><?php echo $item->code_account; ?></td>
                    <td class="item_l">
                        <a href="<?php echo $urlDetail; ?>" target="_blank">
                            <?php echo $item->first_name; ?>
                        </a>
                    </td>
                    <td class="item_l"><?php echo isset($aPositionWork[$item->position_work]) ? $aPositionWork[$item->position_work] : ''; ?></td>
                    <td class="item_l"><?php echo isset($aProvince[$item->province_id]) ? $aProvince[$item->province_id] : ''; ?></td>
                    <td class="item_l"><?php echo $beginJob; ?></td>
                    <td class="item_l"><?php echo $endJob; ?></td>
                    <td class="item_r"><?php echo empty($openingDebit) ? '' : ActiveRecord::formatCurrency($openingDebit); ?></td>
                    <td class="item_r"><?php echo empty($openingCredit) ? '' : ActiveRecord::formatCurrency($openingCredit); ?></td>
                    <td class="item_r"><?php echo empty($incurredDebit) ? '' : ActiveRecord::formatCurrency($incurredDebit); ?></td>
                    <td class="item_r"><?php echo empty($incurredCredit) ? '' : ActiveRecord::formatCurrency($incurredCredit); ?></td>
                    <td class="item_r"><?php echo empty($closingDebit) ? '' : ActiveRecord::formatCurrency($closingDebit); ?></td>
                    <td class="item_r"><?php echo empty($closingCredit) ? '' : ActiveRecord::formatCurrency($closingCredit); ?></td>
                    <?php if($model->canCutSalary()): ?>
                    <td>
                        <input type="text" name="GasDebtsReport[cut_salary][amount][]" class="w-150 f_size_15 ad_fix_currency item_r" maxlength="15">
                        <input type="hidden" name="GasDebtsReport[cut_salary][uid][]" value="<?php echo $uid; ?>">
                    </td>
                    <?php endif; ?>
                </tr>
            <?php endforeach; ?>
        </tbody>
        <tfoot class="item_r item_b">
            <td colspan="7">Tổng cộng</td>
            <td><?php echo empty($sumOpeningDebit) ? '' : ActiveRecord::formatCurrency($sumOpeningDebit); ?></td>
            <td><?php echo empty($sumOpeningCredit) ? '' : ActiveRecord::formatCurrency($sumOpeningCredit); ?></td>
            <td><?php echo empty($sumIncurredDebit) ? '' : ActiveRecord::formatCurrency($sumIncurredDebit); ?></td>
            <td><?php echo empty($sumIncurredCredit) ? '' : ActiveRecord::formatCurrency($sumIncurredCredit); ?></td>
            <td><?php echo empty($sumClosingDebit) ? '' : ActiveRecord::formatCurrency($sumClosingDebit); ?></td>
            <td><?php echo empty($sumClosingCredit) ? '' : ActiveRecord::formatCurrency($sumClosingCredit); ?></td>
            <?php if($model->canCutSalary()): ?>
            <td></td>
            <?php endif; ?>
        </tfoot>
    </table>
</div>

<?php 
if($model->canCutSalary()): ?>

    <div class="row">
        <?php echo $form->labelEx($model, 'month'); ?>
        <?php 
        $this->widget('zii.widgets.jui.CJuiDatePicker', array(
            'model' => $model,
            'attribute' => 'month',
            'options' => array(
                'showAnim' => 'fold',
                'dateFormat' => MyFormat::$dateFormatSearch,
                'changeMonth' => true,
                'changeYear' => true,
                'showOn' => 'button',
                'buttonImage' => Yii::app()->theme->baseUrl . '/admin/images/icon_calendar_r.gif',
                'buttonImageOnly' => true,
            ),

            'htmlOptions' => array(
                'class' => 'w-16',
                'style' => 'height:20px;',
                'readonly' => 'readonly',
                'value'=>date('d-m-Y'),
                'name' => "GasDebtsReport[cut_salary][month]"
            ),
        ));
        ?>  
    </div>

    <div class="row buttons" style="padding-left: 141px;">
        <?php $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType'=>'submit',
            'label'=>'Xác nhận trừ lương',
            'type'=>'null', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
            'size'=>'small', // null, 'large', 'small' or 'mini'
            //'htmlOptions' => array('style' => 'margin-bottom: 10px; float: right;'),
        )); ?>	
    </div>
<?php $this->endWidget(); ?>
</div>
<?php endif; ?>

<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/print-store-card.css" />
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/autoNumeric-master/autoNumeric.js"></script>
<script>
    $(function(){
        fnInitInputCurrency();
        fnAddClassOddEven('items');
    });
</script>