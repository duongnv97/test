<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=> GasCheck::getCurl(),
	'method'=>'get',
)); ?>

	<div class="row">
            <?php echo $form->hiddenField($model, 'user_id',
                    array(
                        'class' => 'uid_auto_hidden uid_leave_hidden',
                        'class_update_val'  => 'uid_leave_hidden',
                    )); ?>
            <?php echo $form->labelEx($model, 'user_id'); ?>
            <?php
            // Widget auto complete search user
            $aData = array(
                'model'             => $model,
                'field_customer_id' => 'user_id',
                'url'               => Yii::app()->createAbsoluteUrl('admin/ajax/search_user_login', ['PaySalary' => 1]),
                'name_relation_user'        => 'rUser',
                'field_autocomplete_name'   => 'autocomplete_user',
                'ClassAdd'                  => 'w-400',
            );
            $this->widget('ext.SpaAutocompleteCustomer.SpaAutocompleteCustomer',
                    array('data'    => $aData)
                    );
            ?>
            <?php echo $form->error($model, 'user_id'); ?>
        </div>
        <div class="row more_col">
            <div class="col1">
                    <?php echo $form->label($model,'date_from'); ?>
                    <?php 
                        $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                            'model'=>$model,        
                            'attribute'=>'date_from',
                            'options'=>array(
                                'showAnim'=>'fold',
                                'dateFormat'=> MyFormat::$dateFormatSearch,
    //                            'minDate'=> '0',
//                                'maxDate'=> '0',
                                'changeMonth' => true,
                                'changeYear' => true,
                                'showOn' => 'button',
                                'buttonImage'=> Yii::app()->theme->baseUrl.'/admin/images/icon_calendar_r.gif',
                                'buttonImageOnly'=> true,                                
                            ),        
                            'htmlOptions'=>array(
                                'class'=>'w-180 date_from',
                                'style'=>'float:left;',                               
                            ),
                        ));
                    ?>     		
            </div>
            <div class="col2">
                    <?php echo $form->label($model,'date_to'); ?>
                    <?php 
                        $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                            'model'=>$model,        
                            'attribute'=>'date_to',
                            'options'=>array(
                                'showAnim'=>'fold',
                                'dateFormat'=> MyFormat::$dateFormatSearch,
    //                            'minDate'=> '0',
//                                'maxDate'=> '0',
                                'changeMonth' => true,
                                'changeYear' => true,
                                'showOn' => 'button',
                                'buttonImage'=> Yii::app()->theme->baseUrl.'/admin/images/icon_calendar_r.gif',
                                'buttonImageOnly'=> true,                                
                            ),        
                            'htmlOptions'=>array(
                                'class'=>'w-180',
                                'style'=>'float:left;',
                            ),
                        ));
                    ?>     		
            </div>
        </div>
    
    <!--Search created date-->
        <div class="row more_col">
            <div class="col1">
                    <?php echo $form->label($model,'Ngày tạo từ'); ?>
                    <?php 
                        $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                            'model'=>$model,        
                            'attribute'=>'date_create_from',
                            'options'=>array(
                                'showAnim'=>'fold',
                                'dateFormat'=> MyFormat::$dateFormatSearch,
    //                            'minDate'=> '0',
//                                'maxDate'=> '0',
                                'changeMonth' => true,
                                'changeYear' => true,
                                'showOn' => 'button',
                                'buttonImage'=> Yii::app()->theme->baseUrl.'/admin/images/icon_calendar_r.gif',
                                'buttonImageOnly'=> true,                                
                            ),        
                            'htmlOptions'=>array(
                                'class'=>'w-180 date_from',
                                'style'=>'float:left;',                               
                            ),
                        ));
                    ?>     		
            </div>
            <div class="col2">
                    <?php echo $form->label($model,'đến'); ?>
                    <?php 
                        $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                            'model'=>$model,        
                            'attribute'=>'date_create_to',
                            'options'=>array(
                                'showAnim'=>'fold',
                                'dateFormat'=> MyFormat::$dateFormatSearch,
    //                            'minDate'=> '0',
//                                'maxDate'=> '0',
                                'changeMonth' => true,
                                'changeYear' => true,
                                'showOn' => 'button',
                                'buttonImage'=> Yii::app()->theme->baseUrl.'/admin/images/icon_calendar_r.gif',
                                'buttonImageOnly'=> true,                                
                            ),        
                            'htmlOptions'=>array(
                                'class'=>'w-180',
                                'style'=>'float:left;',
                            ),
                        ));
                    ?>     		
            </div>
        </div>
    
        <div class="row">
            <?php 
            // Nếu là tab trừ lương thì chỉ search những loại trừ lương thôi
            $type  = isset($_GET['type']) ? $_GET['type'] : GasDebts::TAB_DETAIL;
            $aType = ($type == GasDebts::TAB_DETAIL) ? $model->getTypes() : $model->getArrTypeCutSalaryForView();
            ?>
            <?php echo $form->labelEx($model, 'type'); ?>
            <?php echo $form->dropDownList($model, 'type', $aType, ['empty' => 'Select', 'class'=>'w-400']); ?>
            <?php echo $form->error($model, 'type'); ?>
        </div>
	
	<div class="row">
            <?php echo $form->labelEx($model, 'status'); ?>
            <?php echo $form->dropDownList($model, 'status', $model->getStatus(), ['empty' => 'Select', 'class'=>'w-400',]); ?>
            <?php echo $form->error($model, 'status'); ?>
        </div>
    
    <div class="row ">
        <?php echo $form->labelEx($model, 'created_by'); ?>
        <?php echo $form->hiddenField($model, 'created_by'); ?>
        <?php
        // 1. limit search kh của sale
        $url = Yii::app()->createAbsoluteUrl('admin/ajax/search_user_login', ['PaySalary' => 1]);
        // widget auto complete search user customer and supplier
        $aData2 = array(
            'model' => $model,
            'field_customer_id' => 'created_by',
            'url' => $url,
            'name_relation_user' => 'rCreatedBy',
            'ClassAdd' => 'w-400',
            'placeholder' => 'Nhập mã NV',
            'field_autocomplete_name'   => 'autocomplete_name',
        );
        $this->widget('ext.SpaAutocompleteCustomer.SpaAutocompleteCustomer', array(
            'data' => $aData2
        ));
        ?>
    </div>

	<div class="row buttons" style="padding-left: 159px;">
            <?php $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType'=>'submit',
            'label'=>'Search',
            'type'=>'null', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
            'size'=>'small', // null, 'large', 'small' or 'mini'
            //'htmlOptions' => array('style' => 'margin-bottom: 10px; float: right;'),
        )); ?>	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->