<?php
$this->breadcrumbs = array(
    $this->singleTitle,
);

$menus = array(
    array(
        'label'=>"Export Excel", 
        'url'=>array('index', 'toExcel'=>1),
        'linkOptions'=>array(
            'style'=>'background:url('.Yii::app()->theme->baseUrl.'/admin/images/icon/excel.png)',
        ),
        'htmlOptions'=>array(
            'label'=>'Xuất Excel',
            'class'=>'update',
        ),
    ),
    array('label' => "Create $this->singleTitle", 'url' => array('create')),
);
$this->menu = ControllerActionsName::createMenusRoles($menus, $actions);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('gas-debts-grid', {
                url : $(this).attr('action'),
		data: $(this).serialize()
	});
	return false;
});
");

Yii::app()->clientScript->registerScript('ajaxupdate', "
$('#gas-debts-grid a.ajaxupdate').live('click', function() {
    $.fn.yiiGridView.update('gas-debts-grid', {
        type: 'POST',
        url: $(this).attr('href'),
        success: function() {
            $.fn.yiiGridView.update('gas-debts-grid');
        }
    });
    return false;
});
");
?>

<!--<h1>Danh Sách <?php // echo $this->pluralTitle; ?></h1>-->
<?php 
include 'index_button.php'; 
$type   = isset($_GET['type']) ? $_GET['type'] : GasDebts::TAB_DETAIL;
if($type == GasDebts::TAB_CUT_SALARY){
    $model->view_cut_salary = true;
}
?>


<?php echo CHtml::link('Tìm Kiếm Nâng Cao', '#', array('class' => 'search-button')); ?>
<div class="search-form" style="display:none">
    <?php
    $this->renderPartial('_search', array(
        'model' => $model,
    ));
    ?>
</div><!-- search-form -->

<?php
$this->widget('zii.widgets.grid.CGridView', array(
    'id' => 'gas-debts-grid',
    'dataProvider' => $model->search(),
    'afterAjaxUpdate' => 'function(id, data){ fnUpdateColorbox();}',
    'template' => '{pager}{summary}{items}{pager}{summary}',
//    'itemsCssClass' => 'items custom_here',
//    'htmlOptions'=> array('class'=>'grid-view custom_here'),
    'pager' => array(
        'maxButtonCount' => CmsFormatter::$PAGE_MAX_BUTTON,
    ),
    'enableSorting' => false,
    //'filter'=>$model,
    'columns' => array(
        array(
            'header' => 'S/N',
            'type' => 'raw',
            'value' => '$this->grid->dataProvider->pagination->currentPage * $this->grid->dataProvider->pagination->pageSize + ($row+1)',
            'headerHtmlOptions' => array('width' => '10px', 'style' => 'text-align:center;'),
            'htmlOptions' => array('style' => 'text-align:center;')
        ),
        array(
            'name'  => 'user_id',
            'value' => '$data->getUserName()',
            'footer' => 'Tổng',
            'footerHtmlOptions' => array(
                'style' => 'text-align:left; font-weight:bold'),
        ),
        array(
            'name'  => 'amount',
            'value' => '$data->getAmount()',
            'htmlOptions' => array('style' => 'text-align:right;'),
            'footer' => CmsFormatter::formatCurrency($model->getTotal($model->search()->getData(), 'amount')),
            'footerHtmlOptions' => array(
                'style' => 'text-align:right; font-weight:bold'),
        ),
        'reason',
        array(
            'name'  => 'month',
            'value' => '$data->getMonth()',
            'htmlOptions' => array('style' => 'text-align:center;')
        ),
        array(
            'name'  => 'type',
            'value' => '$data->getTypesText()',
        ),
        array(
            'name'  => 'relate_id',
            'type'  => 'raw',
            'value' => '$data->getRelationInfo()',
        ),
        array(
            'name' => 'status',
            'value' => '$data->getStatusText()',
            'htmlOptions' => array('style' => 'text-align:center;')
        ),

        array(
            'name'  => 'created_date',
            'value' => '$data->getCreatedDate()',
        ),
        array(
            'name'  => 'created_by',
            'value' => '$data->getCreatedBy()',
        ),
        array(
            'header' => 'Actions',
            'class' => 'CButtonColumn',
            'template' => ControllerActionsName::createIndexButtonRoles($actions),
            'buttons' => array(
                'update' => array(
                    'visible' => '$data->canUpdate()',
                ),
                'delete' => array(
                    'visible' => 'GasCheck::canDeleteData($data)',
                ),
            ),
        ),
    ),
));
?>

<script>
    $(document).ready(function () {
        fnUpdateColorbox();
//        $('.createMulti').removeClass('employees_create').addClass('create').find('span').text('Tạo Mới');
    });

    function fnUpdateColorbox() {
        fixTargetBlank();
    }
</script>