<div class="form">

    <?php
    $form = $this->beginWidget('CActiveForm', array(
        'id' => 'gas-debts-form',
        'enableAjaxValidation' => false,
        'htmlOptions' => array('enctype' => 'multipart/form-data'),
    ));
    ?>
    <p class="note">Dòng có dấu <span class="required">*</span> là bắt buộc.</p>
    <?php echo MyFormat::BindNotifyMsg(); ?>
    <div class="row">
        <?php echo $form->labelEx($model, 'type'); ?>
        <?php echo $form->dropDownList($model, 'type', $model->getTypesOnWebCreate(), ['class'=>'w-400']); ?>
        <?php echo $form->error($model, 'type'); ?>
    </div>
    <div class="row">
        <?php echo $form->hiddenField($model, 'user_id',
                array(
                    'class' => 'uid_auto_hidden uid_leave_hidden',
                    'class_update_val'  => 'uid_leave_hidden',
                )); ?>
        <?php echo $form->labelEx($model, 'user_id'); ?>
        <?php
        // Widget auto complete search user
        $aData = array(
            'model'             => $model,
            'field_customer_id' => 'user_id',
            'url'               => Yii::app()->createAbsoluteUrl('admin/ajax/search_user_login', ['PaySalary' => 1]),
            'name_relation_user'        => 'rUser',
            'field_autocomplete_name'   => 'autocomplete_user',
            'ClassAdd'                  => 'w-400',
        );
        $this->widget('ext.SpaAutocompleteCustomer.SpaAutocompleteCustomer',
                array('data'    => $aData)
                );
        ?>
        <?php echo $form->error($model, 'user_id'); ?>
    </div>
    <div class="row">
        <?php echo $form->labelEx($model, 'amount'); ?>
        <?php echo $form->textField($model, 'amount',
                array(
                    "class" =>  "number_only ad_fix_currency",
                    'size'  => 20,
                    'maxlength' => 12)); ?>
        <?php echo $form->error($model, 'amount'); ?>
    </div>
    
    <div class="row">
        <?php echo $form->labelEx($model, 'month'); ?>
        <?php $this->widget('zii.widgets.jui.CJuiDatePicker', array(
            'attribute' => 'month',
            'model' => $model,
            'options' => array(
                'showAnim' => 'fold',
                'dateFormat'=> ActiveRecord::getDateFormatJquery(),
                'changeMonth' => true,
                'changeYear' => true,
                'showOn' => 'button',
                'buttonImage' => Yii::app()->theme->baseUrl . '/admin/images/icon_calendar_r.gif',
                'buttonImageOnly' => true,
                'minDate'=> $model->getDayLimit(),
            ),
            'htmlOptions' => array(
                'class' => 'w-16',
                'readonly' => true,
            ),
        ));
        ?>
        <?php echo $form->error($model, 'month'); ?>
    </div>
    <div class="row">
        <?php echo $form->labelEx($model, 'reason'); ?>
        <?php echo $form->textArea($model, 'reason', array('rows' => 6, 'cols' => 50)); ?>
        <?php echo $form->error($model, 'reason'); ?>
    </div>
    <div class="row display_none">
        <?php echo $form->labelEx($model, 'relate_id'); ?>
        <?php echo $form->textField($model, 'relate_id', array('size' => 11, 'maxlength' => 11)); ?>
        <?php echo $form->error($model, 'relate_id'); ?>
    </div>
    <div class="row type3 display_none">
        <?php echo $form->labelEx($model,'monthCount'); ?>
        <?php echo $form->dropDownList($model,'monthCount', MyFormat::BuildNumberOrder(360), array('class'=>'w-400', 'empty'=>'Select')); ?>
        <?php echo $form->error($model,'monthCount'); ?>
    </div>
    
    <div class="row">
        <?php echo $form->labelEx($model, 'status'); ?>
        <?php echo $form->dropDownList($model, 'status', $model->getStatus(), ['class'=>'w-400',]); ?>
        <?php echo $form->error($model, 'status'); ?>
    </div>
    <div class="row buttons" style="padding-left: 141px;">
        <?php
        $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType' => 'submit',
            'label' => $model->isNewRecord ? 'Create' : 'Save',
            'type' => 'null', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
            'size' => 'small', // null, 'large', 'small' or 'mini'
                //'htmlOptions' => array('style' => 'margin-bottom: 10px; float: right;'),
        ));
        ?>	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/autoNumeric-master/autoNumeric.js"></script>
<script>
    $(document).ready(function () {
        $('.form').find('button:submit').click(function () {
            $.blockUI({overlayCSS: {backgroundColor: '#fff'}});
        });
        
        fnInitInputCurrency();
    });
</script>