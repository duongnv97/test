<div class="form">

    <?php
    $form = $this->beginWidget('CActiveForm', array(
        'id' => 'gas-debts-form',
        'enableAjaxValidation' => false,
        'htmlOptions' => array('enctype' => 'multipart/form-data'),
    ));
    ?>
    <p class="note">Dòng có dấu <span class="required">*</span> là bắt buộc.</p>
    <?php echo MyFormat::BindNotifyMsg(); ?>
    <div class="row">
        <?php $mDebt = new GasDebts('create'); ?>
        <?php echo $form->labelEx($mDebt, 'user_id'); ?>
        <?php echo $form->hiddenField($mDebt, "user_id", array()); ?>
        <?php
        // Widget auto complete search user
        $aData = array(
            'model'                     => $mDebt,
            'field_customer_id'         => 'user_id',
            'url'                       => Yii::app()->createAbsoluteUrl('admin/ajax/search_user_login', ['PaySalary' => 1]),
            'name_relation_user'        => 'rUser',
            'field_autocomplete_name'   => 'autocomplete_user',
            'ShowTableInfo'             => 0,
            'ClassAdd'                  => 'w-400',
            'fnSelectCustomerV2'        => 'fnDoAfterSelect',
            'doSomethingOnClose'        => "doSomethingOnClose",
        );
        $this->widget('ext.SpaAutocompleteCustomer.SpaAutocompleteCustomer', array('data' => $aData)
        );
        ?>
    </div>
    
            
    <div class="row" style="margin-bottom:30px;">
        <table id="person_response" class="tb  hm_table">
            <thead>
                <tr>
                    <th class="item_c w-10">#</th>
                    <th class="item_c w-100">Loại công nợ</th>
                    <th class="item_c w-150">Người thực hiện</th>
                    <th class="item_c w-150">Số tiền</th>
                    <th class="item_c w-250">Lý do</th>
                    <th class="item_c w-80">Số tháng hoàn trả</th>
                    <th class="item_c w-100">Tháng bắt đầu</th>
                    <th class="item_unit last item_c w-30">Xóa</th>
                </tr>
            </thead>
            <tbody>
                <tr class="materials_row tmp_tr display_none" style="">
                    <td class="item_c order padding_5"></td>
                    <td class="padding_5">
                        <?php echo $form->dropDownList($mDebt, 'type[]', $mDebt->getTypesOnWebCreate(), ['class'=>'w-100']); ?>
                    </td>
                    <td class="user_info_td padding_5">
                        <p class="user_name_txt"></p>
                        <input class="user_id_value"
                               name="GasDebts[user_id][]"
                               type="hidden" value="">
                    </td>
                    <td class="padding_5">
                        <input class="number_only ad_fix_currency w-100"
                               name="GasDebts[amount][]"
                               type="text" value="0">
                    </td>
                    <td class="padding_5">
                        <?php echo $form->textArea($mDebt, 'reason[]', array('rows' => 1, 'class' => 'w-250')); ?>
                    </td>
                    <td class="padding_5">
                        <?php echo $form->dropDownList($model,'monthCount[]', $mDebt->getArrayMonthOnWebCreate(), array('class'=>'w-80')); ?>
                    </td>
                    <td class="padding_5">
                        <input name="GasDebts[month][]" class="DateMulti w-60 f_size_14" value="<?php echo date('m/Y');?>" readonly="1">
                    </td>
                    <td class="item_c last padding_5"><span class="remove_icon_only"></span></td>
                </tr>
            </tbody>
        </table>
    </div>
    <div class="row buttons" style="padding-left: 141px;">
        <?php
        $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType' => 'submit',
            'label' => $model->isNewRecord ? 'Create' : 'Save',
            'type' => 'null', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
            'size' => 'small', // null, 'large', 'small' or 'mini'
                //'htmlOptions' => array('style' => 'margin-bottom: 10px; float: right;'),
        ));
        ?>	</div>

    <?php $this->endWidget(); ?>

</div><!-- form -->
<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/print-store-card.css" />
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/autoNumeric-master/autoNumeric.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/jquery.floatThead.min.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/underscore-min.js"></script>    
<?php Yii::app()->clientScript->registerCoreScript('jquery.ui');?>
<script>
    $(document).ready(function () {
        $('.form').find('button:submit').click(function () {
            $.blockUI({overlayCSS: {backgroundColor: '#fff'}});
        });

        fnInitInputCurrency();
        fnBindRemoveIcon();
        $('.hm_table').floatThead(); // lỗi mất border
    });
    // Rewrite action after select autocomple user (getElementById work but $(#..) not working)
    function fnDoAfterSelect(ui, idField, idFieldCustomerID){
        var d = new Date();
        var sMilisecond = d.getTime();
        var tbl = $('table#person_response tbody');
        var tr = tbl.find('tr.tmp_tr').clone();
        tr.removeClass('display_none');
        tr.removeClass('tmp_tr');
        tr.find('td.order').addClass('order_no');
        tr.find('td.user_info_td p.user_name_txt').text(ui.item.label);
        tr.find('td.user_info_td input.user_id_value').val(ui.item.id);
        tr.attr('style','');
        tr.find('.DateMulti').attr('id', sMilisecond);
        tr.find('.DateMulti').datepicker({
            dateFormat      : '<?php echo DomainConst::DATE_FORMAT_2_1;?>',
            showAnim        : 'fold',
            showOn          : 'button',
            changeMonth     : true,
            changeYear      : true,
            minDate         : <?php echo $model->getDayLimit();?>,
            buttonImage     : '<?php echo Yii::app()->theme->baseUrl;?>/admin/images/icon_calendar_r.gif',
            buttonImageOnly : true
        });
        
        tbl.append(tr);
        bindEventForHelpNumber();
        fnInitInputCurrency();
        fnRefreshOrderNumber();
        return false;
    }
    
/**
* @Author: ANH DUNG Dec 28, 2016
* @Todo: function này dc gọi từ ext của autocomplete, action close auto
*/
function doSomethingOnClose(ui, idField, idFieldCustomer){
    var row = $(idField).closest('.row');
    row.find('.remove_row_item').trigger('click');
}

</script>