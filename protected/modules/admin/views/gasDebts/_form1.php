<div class="form">

    <?php
    $form = $this->beginWidget('CActiveForm', array(
        'id' => 'gas-debts-form',
        'enableAjaxValidation' => false,
        'htmlOptions' => array('enctype' => 'multipart/form-data'),
    ));
    ?>

    <p class="note">Dòng có dấu <span class="required">*</span> là bắt buộc.</p>
    <?php echo MyFormat::BindNotifyMsg(); ?>
    <div class="grp-cot">
        <div class="sk-col-md-12 row">
            <div class="sk-col-md-6">
                <div class="sk-col-md-3 lable-text">
                    <?php echo $form->hiddenField($model, 'user_id',
                            array(
                                'class' => 'uid_auto_hidden uid_leave_hidden',
                                'class_update_val'  => 'uid_leave_hidden',
                            )); ?>
                    <?php echo $form->labelEx($model, 'user_id'); ?>
                </div>
                <div class="sk-col-md-6 input-content">
                    <?php
                    // Widget auto complete search user
                    $aData = array(
                        'model'             => $model,
                        'field_customer_id' => 'user_id',
                        'url'               => Yii::app()->createAbsoluteUrl('admin/ajax/search_user_leave'),
                        'name_relation_user'        => 'rUser',
                        'field_autocomplete_name'   => 'autocomplete_user',
                    );
                    $this->widget('ext.SpaAutocompleteCustomer.SpaAutocompleteCustomer',
                            array('data'    => $aData)
                            );
                    ?>
                    <?php echo $form->error($model, 'user_id'); ?>
                </div>
            </div>
            <div class="sk-col-md-6">
                <div class="sk-col-md-3 lable-text">
                    <?php echo $form->labelEx($model, 'amount'); ?>
                </div>
                <div class="sk-col-md-6 input-content fix_number_help">
                    <?php echo $form->textField($model, 'amount',
                            array(
                                "class" =>  "number_only ad_fix_currency",
                                'size'  => 20,
                                'maxlength' => 12)); ?>
                    <?php echo $form->error($model, 'amount'); ?>
                </div>
            </div>
        </div>
        <div class="sk-col-md-12">
            <div class="sk-col-md-6">
                <div class="sk-col-md-3 lable-text">
                    <?php echo $form->labelEx($model, 'reason'); ?>
                </div>
                <div class="sk-col-md-6 input-content">
                    <?php echo $form->textArea($model, 'reason', array('rows' => 6, 'cols' => 50)); ?>
                    <?php echo $form->error($model, 'reason'); ?>
                </div>
            </div>
            <div class="sk-col-md-6">
                <div class="sk-col-md-3 lable-text">
                    <?php echo $form->labelEx($model, 'month'); ?>
                </div>
                <div class="sk-col-md-6 input-content">
                    <?php
                    $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                        'attribute' => 'month',
                        'model' => $model,
                        'options' => array(
                            'showAnim' => 'fold',
                            'dateFormat' => DomainConst::DATE_FORMAT_2_1,
                            'changeMonth' => true,
                            'changeYear' => true,
                            'showOn' => 'button',
                            'buttonImage' => Yii::app()->theme->baseUrl . '/admin/images/icon_calendar_r.gif',
                            'buttonImageOnly' => true,
                        ),
                        'htmlOptions' => array(
                            'class' => 'w-16',
                            'readonly' => true,
                        ),
                    ));
//                    $this->widget('zii.widgets.jui.CJuiDatePicker', array(
//                        'model' => $model,
//                        'attribute' => 'month',
//                        'options' => array(
//                            'dateFormat' => DomainConst::DATE_FORMAT_2_1,
//                            'showAnim' => 'slide', //'slide','fold','slideDown','fadeIn','blind','bounce','clip','drop'
//                            'showOtherMonths' => true, // Show Other month in jquery
//                            'selectOtherMonths' => true, // Select Other month in jquery
//                            'changeMonth' => true,
//                            'changeYear' => true,
////                            'showOn' => 'button',
//                        ),
//                        'htmlOptions' => array(
//                            'style' => ''
//                        ),
//                    ));
                    ?>
                    <?php echo $form->error($model, 'month'); ?>
                </div>
            </div>
        </div>
        <div class="sk-col-md-12">
            <div class="sk-col-md-6">
                <div class="sk-col-md-3 lable-text">
                    <?php echo $form->labelEx($model, 'type'); ?>
                </div>
                <div class="sk-col-md-6 input-content">
                    <?php echo $form->textField($model, 'type'); ?>
                    <?php echo $form->error($model, 'type'); ?>
                </div>
            </div>
            <div class="sk-col-md-6">
                <div class="sk-col-md-3 lable-text">
                    <?php echo $form->labelEx($model, 'relate_id', array('class' => 'lable-text')); ?>
                </div>
                <div class="sk-col-md-6 input-content">
                    <?php echo $form->textField($model, 'relate_id', array('size' => 11, 'maxlength' => 11)); ?>
                    <?php echo $form->error($model, 'relate_id'); ?>
                </div>
            </div>
        </div>
    </div>
    <div class="row buttons" style="padding-left: 141px;">
        <?php
        $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType' => 'submit',
            'label' => $model->isNewRecord ? 'Create' : 'Save',
            'type' => 'null', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
            'size' => 'small', // null, 'large', 'small' or 'mini'
                //'htmlOptions' => array('style' => 'margin-bottom: 10px; float: right;'),
        ));
        ?>	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->
<div>
    <h1>Yii CJuiDatePicker: Default</h1>
    <script>
     $(function() {    
       $( "#anim" ).change(function() {
         $( "#datepicker" ).datepicker( "option", "showAnim", $( this ).val() );
       });
     });
     </script>
     <p>Animations:<br>
           <select id="anim">
                   <option value="show">Show (default) => show</option>
                   <option value="slideDown">Slide down => slideDown</option>
                   <option value="fadeIn">Fade in => fadeIn</option>
                   <option value="blind">Blind (UI Effect) => blind</option>
                   <option value="bounce">Bounce (UI Effect) => bounce</option>
                   <option value="clip">Clip (UI Effect) => clip</option>
                   <option value="drop">Drop (UI Effect) => drop</option>
                   <option value="fold">Fold (UI Effect) => fold</option>
                   <option value="slide">Slide (UI Effect) => slide</option>
                   <option value="">None</option>
           </select>
   </p>
   <?php
   $this->widget('zii.widgets.jui.CJuiDatePicker',array(
       'name'=>'datepicker',
       // additional javascript options for the date picker plugin
       'options'=>array(
           'showAnim'=>'slide',//'slide','fold','slideDown','fadeIn','blind','bounce','clip','drop'
       ),
       'htmlOptions'=>array(
           'style'=>'height:20px;background-color:green;color:white;',
       ),
   ));
   ?>
    <h1>Yii CJuiDatePicker: Inline</h1>
    <?php
    $this->widget('zii.widgets.jui.CJuiDatePicker',array(
        'name'=>'datepicker-Inline',
        'options'=>array(
            'showAnim'=>'slide',//'slide','fold','slideDown','fadeIn','blind','bounce','clip','drop'
        ),
        'htmlOptions'=>array(
            'style'=>''
        ),
    ));
    ?>
    <h1>Yii CJuiDatePicker: Show/Select Other Month Dates</h1>
    <?php
    $this->widget('zii.widgets.jui.CJuiDatePicker',array(
        'name'=>'datepicker-other-month',
        'options'=>array(
            'showAnim'=>'slide',//'slide','fold','slideDown','fadeIn','blind','bounce','clip','drop'
            'showOtherMonths'=>true,// Show Other month in jquery
            'selectOtherMonths'=>true,// Select Other month in jquery
        ),
        'htmlOptions'=>array(
            'style'=>''
        ),
    ));
    ?>
    <?php
    $this->widget('zii.widgets.jui.CJuiDatePicker',array(
        'name'=>'datepicker-showButtonPanel',
        'value'=>date('d-m-Y'),    
        'options'=>array(
            'showAnim'=>'slide',//'slide','fold','slideDown','fadeIn','blind','bounce','clip','drop'
            'showButtonPanel'=>true,
        ),
        'htmlOptions'=>array(
            'style'=>''
        ),
    ));
?>
    <h1>Yii CJuiDatePicker: Display Month & Year Menus</h1>
    <?php
    $this->widget('zii.widgets.jui.CJuiDatePicker',array(
        'name'=>'datepicker-month-year-menu',
//        'flat'=>true,//remove to hide the datepicker
        'options'=>array(
                'dateFormat' => 'yy-mm-dd',
            'showAnim'=>'slide',//'slide','fold','slideDown','fadeIn','blind','bounce','clip','drop'
            'changeMonth'=>true,
            'changeYear'=>true,
            'yearRange'=>'2000:2099',
            'minDate' => '2000-01-01',      // minimum date
            'maxDate' => '2099-12-31',      // maximum date
        ),
        'htmlOptions'=>array(
            'style'=>''
        ),
    ));
    ?>
</div>

<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/autoNumeric-master/autoNumeric.js"></script>
<script>
    $(document).ready(function () {
        $('.form').find('button:submit').click(function () {
            $.blockUI({overlayCSS: {backgroundColor: '#fff'}});
        });
        
        fnInitInputCurrency();
    });
</script>