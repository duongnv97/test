<div class="form">
    <?php
        $linkDetail    = Yii::app()->createAbsoluteUrl('admin/gasDebts', ['type'=> GasDebts::TAB_DETAIL]);
        $LinkCutSalary = Yii::app()->createAbsoluteUrl('admin/gasDebts', ['type'=> GasDebts::TAB_CUT_SALARY]);
        $type          = isset($_GET['type']) ? $_GET['type'] : GasDebts::TAB_DETAIL;
        $aRoleViewAll  = GasDebts::getArrRoleViewAllIndex();
        $aRoleViewSome = GasDebts::getArrRoleViewSomeIndex();
        $aAllow        = array_merge($aRoleViewAll, $aRoleViewSome);
        $cRole         = MyFormat::getCurrentRoleId();
    ?>
    <h1>Danh Sách <?php echo $this->pluralTitle; ?>
        <?php if(in_array($cRole, $aAllow)): ?>
        <a class='btn_cancel f_size_14 <?php echo $type == GasDebts::TAB_DETAIL ? 'active' : '' ?>' href="<?php echo $linkDetail;?>">Chi tiết</a>
        <?php endif; ?>
        <a class='btn_cancel f_size_14 <?php echo $type == GasDebts::TAB_DETAIL ? '' : 'active' ?>' href="<?php echo $LinkCutSalary;?>">Trừ lương</a>
    </h1>
</div>