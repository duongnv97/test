<?php $this->widget('application.components.widgets.XDetailView', array(
    'data' => $model,
    'attributes' => array(
        'group11'=>array(
            'ItemColumns' => 3,
            'itemCssClass' => array('odd xdetail_c2'),
            'attributes' => array(
               
                array(
                    'name'=>'type',
                    'value'=>$model->getTypeText(),
                ),
		array(
                    'name'=>'customer_id',
                    'type'=>'html',
                    'value'=>$model->getCustomer(),
                ),
                array(
                    'name'=>'type_customer',
                    'value'=>$model->getTypeCustomer(),
                ),
                array(
                    'name'=>'sale_id',
                    'value'=>$model->getSale(),
                ),
                
                array(
                    'name'=>'reason',
                    'value'=>$model->getReason(),
                    'type' => 'html',
                ),
                'code_no',
                array(
                    'name'=>'status',
                    'value'=>$model->getStatusText(),
                ),
                array(
                    'name'=>'uid_login',
                    'value'=>$model->getUidLogin(),
                ),
                array(
                    'name' => 'created_date',
                    'type' => 'Datetime',
                ),
                array(
                    'label'=>'Ghi chú duyệt lần 1',
                    'name'=>'note_level_1',
                    'value'=>$model->getNoteLevel1(),
                    'type' => 'html',
                ),
                array(
                    'label'=>'Ghi chú duyệt lần 2',
                    'name'=>'note_level_2',
                    'value'=>$model->getNoteLevel2(),
                    'type' => 'html',
                ),
                array(
                    'label'=>'Ghi chú duyệt lần 3',
                    'name'=>'note_level_3',
                    'value'=>$model->getNoteLevel3(),
                    'type' => 'html',
                ),
            ),
        ),
    ),
)); ?>

<?php $this->widget('application.components.widgets.XDetailView', array(
    'data' => $model,
    'attributes' => array(
        'group11'=>array(
            'ItemColumns' => 3,
            'itemCssClass' => array('odd xdetail_c2'),
            'attributes' => array(
                array(
                    'name' => 'approved_level_1',
                    'value'=>$model->getApprovedLevel1(),
                ),
                array(
                    'name' => 'approved_level_2',
                    'value'=>$model->getApprovedLevel2(),
                ),
                array(
                    'name' => 'approved_level_3',
                    'value'=> $model->getApprovedLevel3(),
                ),
                array(
                    'label'=>'Ghi chú duyệt lần 1',
                    'name'=>'note_level_1',
                    'value'=>$model->getNoteLevel1(),
                    'type' => 'html',
                ),
                array(
                    'label'=>'Ghi chú duyệt lần 2',
                    'name'=>'note_level_2',
                    'value'=>$model->getNoteLevel2(),
                    'type' => 'html',
                ),
                array(
                    'label'=>'Ghi chú duyệt lần 3',
                    'name'=>'note_level_3',
                    'value'=>$model->getNoteLevel3(),
                    'type' => 'html',
                ),
            ),
        ),
    ),
)); ?>


<style>
    .detail-view .detail-view th, .detail-view .detail-view td { font-size: 1.1em;}
</style>
<script>
    $(document).ready(function(){
        $('.xdetail_c2').each(function(){
//            $(this).find('td:first').addClass("w-300");
//            $(this).find('td').eq(1).addClass("w-100");
            $(this).find('th').addClass("w-100");
        });
    });
</script>