<?php
$this->breadcrumbs=array(
	$this->singleTitle,
);

$menus=array(
            array('label'=>"Create $this->singleTitle", 'url'=>array('create')),
);
$this->menu= ControllerActionsName::createMenusRoles($menus, $actions);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('customer-special-grid', {
                url : $(this).attr('action'),
		data: $(this).serialize()
	});
	return false;
});
");

Yii::app()->clientScript->registerScript('ajaxupdate', "
$('#customer-special-grid a.ajaxupdate').live('click', function() {
    $.fn.yiiGridView.update('customer-special-grid', {
        type: 'POST',
        url: $(this).attr('href'),
        success: function() {
            $.fn.yiiGridView.update('customer-special-grid');
        }
    });
    return false;
});
");
?>

<h1>Danh Sách <?php echo $this->pluralTitle; ?></h1>
<?php echo MyFormat::BindNotifyMsg(); ?>
<?php include "index_button.php"; ?>

<?php echo CHtml::link('Tìm Kiếm Nâng Cao','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'customer-special-grid',
	'dataProvider'=>$model->search(),
        'afterAjaxUpdate'=>'function(id, data){ fnUpdateColorbox();}',
        'template'=>'{pager}{summary}{items}{pager}{summary}',
//    'itemsCssClass' => 'items custom_here',
//    'htmlOptions'=> array('class'=>'grid-view custom_here'),
        'pager' => array(
            'maxButtonCount'=>  CmsFormatter::$PAGE_MAX_BUTTON,
        ),
	'enableSorting' => false,
	//'filter'=>$model,
	'columns'=>array(
        array(
            'header' => 'S/N',
            'type' => 'raw',
            'value' => '$this->grid->dataProvider->pagination->currentPage * $this->grid->dataProvider->pagination->pageSize + ($row+1)',
            'headerHtmlOptions' => array('width' => '10px','style' => 'text-align:center;'),
            'htmlOptions' => array('style' => 'text-align:center;')
        ),
            array(
                'name'=>'customer_id',
                'type'=>'html',
                'value'=>'$data->getCustomer().$data->getNewestApproved()',
            ),
            array(
                'name'=>'type_customer',
                'value'=>'$data->getTypeCustomer()',
                'htmlOptions' => array('style' => 'width:30px;'),
            ),
            array(
                'name'=>'sale_id',
                'type' => 'html',
                'value'=>'$data->getSale()."<br><br><b>CODE: ".$data->code_no."</b>"',
                'htmlOptions' => array('style' => 'width:100px;'),
            ),
            array(
                'name'=>'type',
                'value'=>'$data->getTypeText()',
                'htmlOptions' => array('style' => 'width:50px;'),
            ),
            array(
                'name'=>'reason',
                'value'=>'$data->getReason()',
                'type' => 'html',
//                'htmlOptions' => array('style' => 'width:50px;'),
            ),
            array(
                'name'=>'status',
                'type' => 'html',
                'value'=>'$data->getStatusText().$data->getLockCustomerText()',
                'htmlOptions' => array('style' => 'text-align:center; width:50px;'),
            ),
            array(
                'name'=>'uid_login',
                'value'=>'$data->getUidLogin()',
            ),
            array(
                'name' => 'created_date',
                'type' => 'raw',
                'value' => '$data->getCreatedDate()."<br>".$data->getUrlRemoveSpecial()',
                'htmlOptions' => array('style' => 'width:50px;')
            ),
            array(
                'header' => 'Actions',
                'class'=>'CButtonColumn',
                'template'=> ControllerActionsName::createIndexButtonRoles($actions),
                'buttons'=>array(
                    'update' => array(
                        'visible' => '$data->canUpdate()',
                    ),
                    'delete'=>array(
                        'visible'=> 'GasCheck::canDeleteData($data)',
                    ),
                ),
            ),
	),
)); ?>

<script>
$(document).ready(function() {
    fnUpdateColorbox();
});

function fnUpdateColorbox(){
    fixTargetBlank();
    $(".view").colorbox({
        iframe: true,
        innerHeight: '1000',
        innerWidth: '1100', escKey: false, close: "<span title='close'>close</span>"
    });
}
</script>