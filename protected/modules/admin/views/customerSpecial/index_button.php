<div class="form">
    <?php
        $LinkNormal         = Yii::app()->createAbsoluteUrl('admin/customerSpecial/index');
        $LinkIsSpecial      = Yii::app()->createAbsoluteUrl('admin/customerSpecial/index', array( 'StatusSpecial'=> CustomerSpecial::TYPE_ADD_NEW_SPECIAL));
        $LinkRemoveSpecial  = Yii::app()->createAbsoluteUrl('admin/customerSpecial/index', array( 'StatusSpecial'=> CustomerSpecial::TYPE_CANCEL_SPECIAL));
        $LinkNotApproved    = Yii::app()->createAbsoluteUrl('admin/customerSpecial/index', array( 'StatusSpecial'=> CustomerSpecial::TYPE_CANCEL_NOT_APPROVED));
    ?>
    <h1><?php echo $this->adminPageTitle;?>
        <a class='btn_cancel f_size_14 <?php echo (!isset($_GET['StatusSpecial'])) ? "active":"";?>' href="<?php echo $LinkNormal;?>">Đề Xuất & Chờ Duyệt</a>
        <a class='btn_cancel f_size_14 <?php echo isset($_GET['StatusSpecial']) && $_GET['StatusSpecial']==1 ? "active":"";?>' href="<?php echo $LinkIsSpecial;?>">Là KH Đặc Biệt</a>
        <a class='btn_cancel f_size_14 <?php echo isset($_GET['StatusSpecial']) && $_GET['StatusSpecial']==CustomerSpecial::TYPE_CANCEL_NOT_APPROVED ? "active":"";?>' href="<?php echo $LinkNotApproved;?>">Hủy Bỏ</a>
        <a class='btn_cancel f_size_14 <?php echo isset($_GET['StatusSpecial']) && $_GET['StatusSpecial']==CustomerSpecial::TYPE_CANCEL_SPECIAL ? "active":"";?>' href="<?php echo $LinkRemoveSpecial;?>">Đã Gỡ Bỏ KH Đặc Biệt</a>
    </h1> 
</div>