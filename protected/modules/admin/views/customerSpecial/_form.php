<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'customer-special-form',
	'enableAjaxValidation'=>false,
        'htmlOptions' => array('enctype' => 'multipart/form-data'),
)); ?>

    <p class="note">Dòng có dấu <span class="required">*</span> là bắt buộc.</p>
    <?php echo MyFormat::BindNotifyMsg(); ?>
    <div class="row">
        <?php echo $form->labelEx($model,'status'); ?>
        <?php echo $model->getStatusText(); ?>
    </div>
    <div class="row">
        <?php echo $form->labelEx($model,'type'); ?>
        <?php echo $model->getTypeText(); ?>
    </div>
    <div class="row">
    <?php echo $form->labelEx($model,'customer_id'); ?>
    <?php echo $form->hiddenField($model,'customer_id', array('class'=>'')); ?>
    <?php
            // 1. limit search kh của sale
            $url = Yii::app()->createAbsoluteUrl('admin/ajax/search_user_by_code');
            // widget auto complete search user customer and supplier
            $aData = array(
                'model'=>$model,
                'field_customer_id'=>'customer_id',
                'url'=> $url,
                'name_relation_user'=>'rCustomer',
                'ClassAdd' => 'w-400',
                'fnSelectCustomer'=>'fnSelectCustomer',
            );
            $this->widget('ext.SpaAutocompleteCustomer.SpaAutocompleteCustomer',
                array('data'=>$aData));
            ?>
        <?php echo $form->error($model,'customer_id'); ?>
    </div>
    
    <div class="row">
        <?php echo $form->labelEx($model, 'approved_level_1'); ?>
        <?php echo $form->dropDownList($model, 'approved_level_1', $model->getListApprovedLevel(GasOneManyBig::TYPE_CUSTOMER_SPECIAL_1), array('class' => 'w-400', 'empty'=>'Select')); ?>
        <?php echo $form->error($model, 'approved_level_1'); ?>
    </div>
    
    <?php if($model->type == CustomerSpecial::TYPE_ADD_NEW_SPECIAL): ?>
    <div class="row">
        <?php echo $form->labelEx($model,'reason'); ?>
        <div class="fix-label">
            <?php
               $this->widget('ext.multiselect.JMultiSelect',array(
                     'model'=>$model,
                     'attribute'=>'reason',
                     'data'=> $model->getArrayReason(),
                     // additional javascript options for the MultiSelect plugin
                    'options'=>array('selectedList' => 30,),
                     // additional style
                     'htmlOptions'=>array('style' => 'width: 800px;'),
               ));    
           ?>
        </div>
        <?php echo $form->error($model,'reason'); ?>
    </div>
    <?php endif; ?>

    <div class="row">
        <?php echo $form->labelEx($model,'note_create'); ?>
        <?php echo $form->textArea($model,'note_create',array('rows'=>6, 'class'=>'w-600')); ?>
        <?php echo $form->error($model,'note_create'); ?>
    </div>
    <div class="row buttons" style="padding-left: 141px;">
        <?php $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType'=>'submit',
            'label'=>$model->isNewRecord ? 'Create' :'Save',
            'type'=>'null', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
            'size'=>'small', // null, 'large', 'small' or 'mini'
            //'htmlOptions' => array('style' => 'margin-bottom: 10px; float: right;'),
        )); ?>	
    </div>

<?php $this->endWidget(); ?>
</div><!-- form -->
    
<?php if(!empty($model->customer_id)): ?>
    <?php // $this->widget('CustomerSpecialWidget', array('customer_id' => $model->customer_id)); ?>
<?php endif; ?>

<script>
    $(document).ready(function(){
        $('.form').find('button:submit').click(function(){
            $.blockUI({ overlayCSS: { backgroundColor: '#fff' } }); 
        });
        
    });
    
    function fnSelectCustomer(user_id, idField, idFieldCustomer){
        if(idFieldCustomer == '#CustomerSpecial_customer_id' ){
            var customer_id = $('#CustomerSpecial_customer_id').val();
            var next = '<?php echo GasCheck::getCurl();?>?customer_id='+customer_id;
            next = updateQueryStringParameter(next, 'customer_id', customer_id);
            $.blockUI({ overlayCSS: { backgroundColor: '#fff' } });
            window.location = next;
        }
    }
</script>