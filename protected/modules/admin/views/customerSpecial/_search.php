<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=> GasCheck::getCurl(),
	'method'=>'get',
)); ?>

	<div class="row">
        <?php echo $form->labelEx($model,'customer_id'); ?>
        <?php echo $form->hiddenField($model,'customer_id', array('class'=>'')); ?>
        <?php
                // 1. limit search kh của sale
                $url = Yii::app()->createAbsoluteUrl('admin/ajax/search_user_by_code');
                // widget auto complete search user customer and supplier
                $aData = array(
                    'model'=>$model,
                    'field_customer_id'=>'customer_id',
                    'url'=> $url,
                    'name_relation_user'=>'rCustomer',
                    'ClassAdd' => 'w-400',
    //                'fnSelectCustomer'=>'fnSelectCustomer',
                );
                $this->widget('ext.SpaAutocompleteCustomer.SpaAutocompleteCustomer',
                    array('data'=>$aData));
                ?>
            <?php echo $form->error($model,'customer_id'); ?>
        </div>

        <div class="row">
        <?php echo $form->labelEx($model,'sale_id'); ?>
        <?php echo $form->hiddenField($model,'sale_id', array('class'=>'')); ?>
        <?php
                // 1. limit search kh của sale
                $url = Yii::app()->createAbsoluteUrl('admin/ajax/search_by_role', array('role'=> implode(',', $model->getRoleSale())));
                // widget auto complete search user customer and supplier
                $aData = array(
                    'model'=>$model,
                    'field_customer_id'=>'sale_id',
                    'url'=> $url,
                    'name_relation_user'=>'rSale',
                    'ClassAdd' => 'w-400',
                    'field_autocomplete_name' => 'autocomplete_name_2',
                    'placeholder'=>'Nhập mã NV kinh doanh hoặc tên ',
                );
                $this->widget('ext.SpaAutocompleteCustomer.SpaAutocompleteCustomer',
                    array('data'=>$aData));
                ?>
        </div>
        <div class="row">
        <?php echo $form->labelEx($model,'uid_login'); ?>
        <?php echo $form->hiddenField($model,'uid_login', array('class'=>'')); ?>
        <?php
                // 1. limit search kh của sale
                $url = Yii::app()->createAbsoluteUrl('admin/ajax/search_for_user_login');
                // widget auto complete search user customer and supplier
                $aData = array(
                    'model'=>$model,
                    'field_customer_id'=>'uid_login',
                    'url'=> $url,
                    'name_relation_user'=>'rUidLogin',
                    'ClassAdd' => 'w-400',
                    'field_autocomplete_name' => 'autocomplete_name_3',
                    'placeholder'=>'Nhập mã NV hoặc tên ',
                );
                $this->widget('ext.SpaAutocompleteCustomer.SpaAutocompleteCustomer',
                    array('data'=>$aData));
                ?>
        </div>
    
	<div class="row more_col">
            <div class="col1">
                <?php echo $form->label($model,'type_customer',array()); ?>
                <?php echo $form->dropDownList($model,'type_customer', CmsFormatter::$CUSTOMER_BO_MOI,array('class'=>'w-200','empty'=>'Select')); ?>
            </div>

            <div class="col2">
                <?php echo $form->label($model,'type',array()); ?>
                <?php echo $form->dropDownList($model,'type', $model->getArrayType(),array('class'=>'w-200','empty'=>'Select')); ?>
            </div>

            <div class="col3">
                <?php echo $form->label($model,'status',array()); ?>
                <?php echo $form->dropDownList($model,'status', $model->getArrayStatus(),array('class'=>'w-200','empty'=>'Select')); ?>
            </div>
        </div>
        <div class="row">
            <?php echo $form->labelEx($model,'reason'); ?>
            <div class="fix-label">
                <?php
                   $this->widget('ext.multiselect.JMultiSelect',array(
                         'model'=>$model,
                         'attribute'=>'reason',
                         'data'=> $model->getArrayReason(),
                         // additional javascript options for the MultiSelect plugin
                        'options'=>array('selectedList' => 30,),
                         // additional style
                         'htmlOptions'=>array('style' => 'width: 550px;'),
                   ));    
               ?>
            </div>
            <?php echo $form->error($model,'reason'); ?>
        </div>

	<div class="row">
            <?php echo $form->label($model,'code_no',array()); ?>
            <?php echo $form->textField($model,'code_no'); ?>
	</div>

	<div class="row buttons" style="padding-left: 159px;">
            <?php $this->widget('bootstrap.widgets.TbButton', array(
                'buttonType'=>'submit',
                'label'=>'Search',
                'type'=>'null', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
                'size'=>'small', // null, 'large', 'small' or 'mini'
                //'htmlOptions' => array('style' => 'margin-bottom: 10px; float: right;'),
            )); ?>	
        </div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->