<!--<h1>Cập nhật thông tin KH</h1>-->
<div class="form">
<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'customer-draft-detail-form',
	'enableAjaxValidation'=>false,
        'htmlOptions' => array('enctype' => 'multipart/form-data'),
)); ?>
    <?php 
        $phone = $mCustomerDraft->getPhone();
    ?>
     
    <div class="row">
        <input type="hidden" name="cUrl" value="<?php echo GasCheck::getCurl();?>">
        <?php echo $form->hiddenField($mCustomerDraft, 'id'); ?>
        <?php echo $form->labelEx($mCustomerDraft,'name'); ?>
        <?php echo $form->textField($mCustomerDraft,'name', array('class'=>'w-400')); ?>
        <?php echo $form->error($mCustomerDraft,'name'); ?>
    </div>
    <div class="row">
        <div style="width: 100%;float: left;padding-bottom: 5px;padding-left: 140px;">
            <label style="color: red;width: auto; ">Mỗi số phone cách nhau bởi dấu - vd: 0123456789-0988180386</label>
            <!--<label style="color: red;width: auto; ">Chỉ các tỉnh: TPHCM, Đồng Nai, Bình Dương, Vũng Tàu yêu cầu nhập số di động báo giá</label>-->
        </div>
        <?php echo $form->labelEx($mCustomerDraft,'phone'); ?>
        <?php echo $form->textField($mCustomerDraft,'phone', array('value'=>$phone, 'class'=>'w-400 phone_number_only')); ?>
        <?php echo $form->error($mCustomerDraft,'phone'); ?>
    </div>
    <div class="row">
        <?php echo $form->labelEx($mCustomerDraft,'address'); ?>
        <?php echo $form->textField($mCustomerDraft,'address', array('class'=>'w-600')); ?>
        <?php echo $form->error($mCustomerDraft,'address'); ?>
    </div>
    
    <div class="row more_col">
        <div class="col1">
            <?php echo $form->labelEx($mCustomerDraft,'province_id'); ?>
            <?php echo $form->dropDownList($mCustomerDraft,'province_id', GasProvince::getArrAll(),array('style'=>'','class'=>'w-200','empty'=>'Select')); ?>
            <?php echo $form->error($mCustomerDraft,'province_id'); ?>
        </div> 	

        <div class="col2">
            <?php echo $form->labelEx($mCustomerDraft,'district_id'); ?>
            <?php echo $form->dropDownList($mCustomerDraft,'district_id', GasDistrict::getArrAll($mCustomerDraft->province_id),array('class'=>'w-200', 'empty'=>"Select")); ?>
            <?php echo $form->error($mCustomerDraft,'district_id'); ?>
        </div>
    </div>
    
    <div class="row buttons" style="padding-left: 141px;">
            <?php $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType'=>'submit',
            'label'=>$mCustomerDraft->isNewRecord ? 'Create' :'Save',
            'type'=>'null', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
            'size'=>'small', // null, 'large', 'small' or 'mini'
            //'htmlOptions' => array('style' => 'margin-bottom: 10px; float: right;'),
        )); ?>	
    </div>
    <hr>
<?php $this->endWidget(); ?>
</div><!-- form -->

<script>
</script>