<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=> GasCheck::getCurl(),
	'method'=>'get',
)); 

$mCustomerDraft = new CustomerDraft();
?>

    <div class="row more_col">
        <div class="col1">
            <?php  echo $form->labelEx($model,'employee_id', ['label'=>'Nhân viên']); ?>
            <?php  echo $form->dropDownList($model,'employee_id', $mCustomerDraft->getArrayEmployee(), ['empty'=>'Select','class'=>'w-200']); ?>
        </div>
        <div class="col2">
        </div>
        <div class="col3">
        </div>
    </div>
    
    
    <div class="row more_col">
        <div class="col1">
            <?php echo $form->labelEx($model, 'date_call_from', []); ?>
             <?php 
                $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                    'model'=>$model,        
                    'attribute'=>'date_call_from',
                    'options'=>array(
                        'showAnim'=>'fold',
                        'dateFormat'=> MyFormat::$dateFormatSearch,
                //                            'minDate'=> '0',
                //                            'maxDate'=> '0',
                        'changeMonth' => true,
                        'changeYear' => true,
                        'showOn' => 'button',
                        'buttonImage'=> Yii::app()->theme->baseUrl.'/admin/images/icon_calendar_r.gif',
                        'buttonImageOnly'=> true,                                
                    ),        
                    'htmlOptions'=>array(
                        'class'=>'w-16',
                        'style'=>'height:20px;',
//                            'readonly'=>'readonly',
                    ),
                ));
                ?>  
        </div>
        <div class="col1">
            <?php echo $form->labelEx($model, 'date_call_to', []); ?>
             <?php 
                $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                    'model'=>$model,        
                    'attribute'=>'date_call_to',
                    'options'=>array(
                        'showAnim'=>'fold',
                        'dateFormat'=> MyFormat::$dateFormatSearch,
                //                            'minDate'=> '0',
                //                            'maxDate'=> '0',
                        'changeMonth' => true,
                        'changeYear' => true,
                        'showOn' => 'button',
                        'buttonImage'=> Yii::app()->theme->baseUrl.'/admin/images/icon_calendar_r.gif',
                        'buttonImageOnly'=> true,                                
                    ),        
                    'htmlOptions'=>array(
                        'class'=>'w-16',
                        'style'=>'height:20px;',
//                            'readonly'=>'readonly',
                    ),
                ));
                ?>  
        </div>
    </div>

    <div class="row more_col">
        <div class="col1">
           <?php echo $form->labelEx($model,'status',['label'=>'Trạng thái']); ?>
           <?php echo $form->dropdownList($model,'status', $model->getArrayCallStatus(true), array('empty'=>'Select','class'=>'w-200')); 
           ?>
       </div>
       <div class="col1">
           <?php echo $form->labelEx($model,'status_cancel',['label'=>'Lý do từ chối']); ?>
           <?php echo $form->dropdownList($model,'status_cancel', $model->getArrayStatusCancel(), array('empty'=>'Select','class'=>'w-200')); 
           ?>
       </div>
    </div>

    <div class="row buttons" style="padding-left: 159px;">
        <?php $this->widget('bootstrap.widgets.TbButton', array(
           'buttonType'=>'submit',
           'label'=>'Search',
           'type'=>'null', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
           'size'=>'small', // null, 'large', 'small' or 'mini'
           //'htmlOptions' => array('style' => 'margin-bottom: 10px; float: right;'),
        )); ?>	
    </div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->