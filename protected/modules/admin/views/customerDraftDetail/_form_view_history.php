<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'customer-draft-detail-grid',
	'dataProvider'=>$model->searchByCustomerDrafty(),
        'afterAjaxUpdate'=>'function(id, data){ fnUpdateColorbox();}',
        'template'=>'{pager}{summary}{items}{pager}{summary}',
//    'itemsCssClass' => 'items custom_here',
//    'htmlOptions'=> array('class'=>'grid-view custom_here'),
        'pager' => array(
            'maxButtonCount'=>  CmsFormatter::$PAGE_MAX_BUTTON,
        ),
	'enableSorting' => false,
	//'filter'=>$model,
	'columns'=>array(
        array(
            'header' => 'S/N',
            'type' => 'raw',
            'value' => '$this->grid->dataProvider->pagination->currentPage * $this->grid->dataProvider->pagination->pageSize + ($row+1)',
            'headerHtmlOptions' => array('width' => '10px','style' => 'text-align:center;'),
            'htmlOptions' => array('style' => 'text-align:center;')
        ),
                array(
                    'name'  => 'appointment_date',
                    'type'  => 'raw',
                    'value' => '$data->getAppointment()'
                ),
                array(
                    'name'  => 'note',
                    'type'  => 'raw',
                    'value' => '$data->getNote()'
                ),
                array(
                    'name'  => 'status',
                    'type'  => 'raw',
                    'value' => '$data->getStatus()'
                ),
                array(
                    'name'  => 'status_cancel',
                    'type'  => 'raw',
                    'value' => '$data->getStatusCancel()'
                ),
                array(
                    'header' => 'Actions',
                    'class'=>'CButtonColumn',
                    'template'=> ControllerActionsName::createIndexButtonRoles($actions),
                    'buttons'=>array(
                        'update' => array(
                            'visible' => '$data->canUpdate()',
                        ),
                        'delete'=>array(
                            'visible' => '$data->canDelete()',
                        ),
                    ),
                ),
	),
)); ?>
