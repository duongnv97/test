<?php
$this->breadcrumbs=array(
    $this->pluralTitle => array('index'),
    ' ' . $this->singleTitle . ' : ',
);

$menus = array(
	array('label'=>"$this->pluralTitle Management", 'url'=>array('index')),
	array('label'=>"Tạo Mới $this->singleTitle", 'url'=>array('create')),
	array('label'=>"Cập Nhật $this->singleTitle", 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>"Xóa $this->singleTitle", 'url'=>array('delete'), 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Bạn chắc chắn muốn xóa ?')),
);
$this->menu= ControllerActionsName::createMenusRoles($menus, $actions);
?>

<h1>Xem: <?php echo $this->singleTitle; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		array(
                    'name'  => 'Khách hàng',
                    'type'  => 'raw',
                    'value' => $model->getInfoCustomerDraft(),
                ),
                array(
                    'name'  => 'appointment_date',
                    'type'  => 'raw',
                    'value' => $model->getAppointment()
                ),
                array(
                    'name'  => 'note',
                    'type'  => 'raw',
                    'value' => $model->getNote()
                ),
                array(
                    'name'  => 'status',
                    'type'  => 'raw',
                    'value' => $model->getStatus()
                ),
                array(
                    'name'  => 'status_cancel',
                    'type'  => 'raw',
                    'value' => $model->getStatusCancel()
                ),
                array(
                    'name'=>'created_date',
                    'type'=>'raw',
                    'value'=>$model->getCreatedDate()
                ),
	),
)); ?>
