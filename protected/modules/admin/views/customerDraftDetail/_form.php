<?php if(!$model->isOnlyView()){ ?>

<?php 
    $mCustomerDraft = $model->rCustomerDraft;
    $display_none   = $mCustomerDraft->hasErrors() ? '' : 'display_none';
    $infoCustomer   = $model->getInfoCustomerDraft();
?>

<a href="javascript:;" class="customer_toggle" nextAction='BoxUpdatePhone'>Cập nhật thông tin KH</a>
<div class="BoxUpdatePhone <?php echo $display_none;?>">
    <?php include '_formUpdatePhone.php'; ?>
</div>

<div class="form">
<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'customer-draft-detail-form',
	'enableAjaxValidation'=>false,
        'htmlOptions' => array('enctype' => 'multipart/form-data'),
)); ?>

    <p class="note">Dòng có dấu <span class="required">*</span> là bắt buộc.</p>
    <?php echo MyFormat::BindNotifyMsg(); ?>
    <?php echo $infoCustomer; ?>

    <div class="row">
        <?php echo $form->labelEx($model, 'appointment_date', []); ?>
        <?php 
            $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                'model'=>$model,        
                'attribute'=>'appointment_date',
                'options'=>array(
                    'showAnim'=>'fold',
                    'dateFormat'=> ActiveRecord::getDateFormatJquery(),
//                  'minDate'=> '0',
//                  'maxDate'=> '0',
                    'changeMonth' => true,
                    'changeYear' => true,
                    'showOn' => 'button',
                    'buttonImage'=> Yii::app()->theme->baseUrl.'/admin/images/icon_calendar_r.gif',
                    'buttonImageOnly'=> true,                                
                ),        
                'htmlOptions'=>array(
                    'class'=>'w-16',
                    'style'=>'height:30px;',
                    'readonly'=>'readonly',
                ),
            ));
        ?>  
        <?php echo $form->error($model,'appointment_date'); ?>
    </div>
    
    <div class="row GasCheckboxList float_l"  style="">
        <?php echo $form->labelEx($model,'status', ['class'=>'']); ?>
        <div style="margin-left: 100px">
            <?php echo $form->radioButtonList($model,'status', $model->getArrayCallStatus(), 
                        array(
                            'separator'=>"",
                            'template'=>'<li>{input}{label}</li>',
                            'container'=>'ul',
                            'class'=>'RadioOrderType',
//                            'headerHtmlOptions'=>'height: auto',
                        )); 
            ?>
        </div>
        <div class="clr"></div><br>
        <?php echo $form->error($model,'status'); ?>
    </div>
    <div class="clr"></div><br>
    
    <div class="row">
        <?php echo $form->labelEx($model,'note'); ?>
        <?php echo $form->textArea($model,'note',array('rows'=>5, 'cols'=>100)); ?>
        <?php echo $form->error($model,'note'); ?>
    </div>
    <div class="row display_none">
        <?php echo $form->labelEx($model,'status_cancel',['label'=>'Lý do từ chối']); ?>
        <?php echo $form->dropdownList($model,'status_cancel', $model->getArrayStatusCancel(), array('empty'=>'Select','class'=>'w-200')); ?>
        <?php echo $form->error($model,'status_cancel'); ?>
    </div>
    <div class="row buttons" style="padding-left: 141px;">
            <?php $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType'=>'submit',
            'label'=>$model->isNewRecord ? 'Tạo ghi chú' :'Save',
            'type'=>'null', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
            'size'=>'small', // null, 'large', 'small' or 'mini'
            //'htmlOptions' => array('style' => 'margin-bottom: 10px; float: right;'),
        )); ?>	
        <input class='cancel_iframe' type='button' value='Cancel'>
    </div>

<?php $this->endWidget(); ?>
</div><!-- form -->
<div class="form">
    <a href="javascript:;" class="customer_toggle" nextAction='create_customer'>Tạo KH</a>
    <div class="create_customer">
        <?php $mCustomerDraft = $model->getCustomerDraft(); if(!empty($mCustomerDraft)){?>
            <iframe id="iframe" style="width: 100%;" src="<?php echo $mCustomerDraft->getUrlCreateCustomer(); ?>" scrolling="no" frameborder="0"></iframe>
        <?php }?>
    </div>
</div>

<?php } ?>

<?php include '_form_view_history.php';?>
<script>
    $load_number = 0;
    $(document).ready(function(){
        $('.form').find('button:submit').click(function(){
            $.blockUI({ overlayCSS: { backgroundColor: '#fff' } });
        });
        $('.customer_toggle').click(function(){
            var nextAction = $(this).attr('nextAction');
            $('.' + nextAction).toggle();
            return false;
        });
        validatePhoneNumber();
        fnSelectProvinceDistrict('#CustomerDraft_province_id', '#CustomerDraft_district_id' , "<?php echo Yii::app()->createAbsoluteUrl('admin/ajax/get_slt_district');?>");
    });
    $('#iframe').load(function(){
        this.style.height = this.contentWindow.document.body.scrollHeight + 'px';
        if($load_number <= 0){
            $('.create_customer').toggle();
        }
        $load_number++;
    });
</script>