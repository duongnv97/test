<?php
$this->breadcrumbs=array(
	'Mediases'=>array('index'),
	'Create',
);

$menus = array(		
        array('label'=>'Medias Management', 'url'=>array('index')),
);
$this->menu= ControllerActionsName::createMenusRoles($menus, $actions);
?>

<h1>Create Medias</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model, 'mes' => $mes)); ?>