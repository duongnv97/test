<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'medias-form',
	'enableAjaxValidation'=>false,
        'htmlOptions' => array(
            'enctype' => 'multipart/form-data',
        ),
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'title'); ?>
		<?php echo $form->textField($model,'title',array('size'=>45,'maxlength'=>45)); ?>
		<?php echo $form->error($model,'title'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'description'); ?>
		<?php echo $form->textArea($model,'description',array('rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'description'); ?>
	</div>
        
        <div class="row" style="padding-left: 115px;">
            <?php if(!$model->isNewRecord) 
            { 
                if($model->type == 'image-upload') {
                    echo CHtml::image(Yii::app()->baseUrl."/upload/uploaded_$model->user_id/adminshow/".$model->url, "image");  
                }                
                elseif($model->type == 'image-link') {
                    echo CHtml::image($model->url, "image");  
                }
                else
                    echo $model->url;             
            }            
        ?>
        </div>

	<div class="row">
		<?php echo $form->labelEx($model,'type'); ?>		
                <div style="display: block; margin-left: 115px;">
                    <?php echo $form->radioButtonList($model,'type',array('image-upload'=>'Image - Upload', 'image-link' => 'Image - Link'), array('style' => 'display:inline','float' => 'left')); ?>
                </div>
		<?php echo $form->error($model,'type'); ?>
        <?php if($mes != "") { ?> <div class="errorMessage"><?php echo $mes; ?></div><?php } ?>
	</div>
        
        <div id="tabs-image-upload"  style="width: 847px;display: none;">
            <div class="row">
                <label for="UsersActions_user_id"><?php echo  Yii::t('translation','Image')?></label>
                <?php echo CHtml::activeFileField($model,'url'); ?>                 
            </div>            
        </div>

        <div id="tabs-image-link" style="width: 847px;display: none;">
            <div class="row">
                <label for="UsersActions_user_id"><?php echo  Yii::t('translation','URL')?></label>
                <?php echo CHtml::textField('image_url',$model->url && $model->type == 'image-link' ? $model->url : '', array('size' => 45 )); ?> 
            </div>                        
        </div>

        <div id="tabs-youtube" style="width: 847px; display: none;" >
            <div class="row">
                <label for="UsersActions_user_id"><?php echo  Yii::t('translation','Embed')?></label>
                <?php echo CHtml::textArea('youtube_embed', $model->url && $model->type == 'video-youtube' ? $model->url : '', array('size' => 45, 'style' => 'width:286px; height: 78px;')); ?>    
            </div>
        </div>
        

	<div class="row buttons" style="padding-left: 115px;">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->


<script type="text/javaScript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/jquery-ui-1.8.21.custom/js/jquery-ui-1.8.21.custom.min.js"></script>
<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->theme->baseUrl; ?>/js/jquery-ui-1.8.21.custom/css/smoothness/jquery-ui-1.8.21.custom.css" />

<script type="text/javascript">
    var block1 = document.getElementById("tabs-image-upload");
    var block2 = document.getElementById("tabs-image-link");
    var block3 = document.getElementById("tabs-youtube");    
    
    $("#Medias_type_0").click(function(){
        block1.style.display = "block";
        block2.style.display = "none";
        //block3.style.display = "none";
    });
    
    $("#Medias_type_1").click(function(){
        block1.style.display = "none";
        block2.style.display = "block";
        //block3.style.display = "none";	
    });
    
    $("#Medias_type_2").click(function(){
        block1.style.display = "none";
        block2.style.display = "none";
       // block3.style.display = "block";	
    });   
    
    <?php if(!$model->isNewRecord) { ?>
        <?php if($model->type == 'image-upload') { ?>
            block1.style.display = "block";
        <?php } ?>
            
        <?php if($model->type == 'image-link') { ?>
            block2.style.display = "block";
        <?php } ?>
            
        <?php if($model->type == 'video-youtube') { ?>
            block3.style.display = "block";
        <?php } ?>
    <?php } ?>

</script>

<?php ?>