<?php
$this->breadcrumbs=array(
	'Medias',
);

$menus=array(
	array('label'=>'Create Medias', 'url'=>array('create')),
);
$this->menu= ControllerActionsName::createMenusRoles($menus, $actions);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('medias-grid', {
                url : $(this).attr('action'),
		data: $(this).serialize()
	});
	return false;
});
");

Yii::app()->clientScript->registerScript('ajaxupdate', "
$('#medias-grid a.ajaxupdate').live('click', function() {
    $.fn.yiiGridView.update('medias-grid', {
        type: 'POST',
        url: $(this).attr('href'),
        success: function() {
            $.fn.yiiGridView.update('medias-grid');
        }
    });
    return false;
});
");
?>

<h1>List Mediases</h1>

<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'medias-grid',
	'dataProvider'=>$model->search(),
	//'filter'=>$model,
	'columns'=>array(
        array(
            'header' => 'S/N',
            'type' => 'raw',
            'value' => '$this->grid->dataProvider->pagination->currentPage * $this->grid->dataProvider->pagination->pageSize + ($row+1)',
            'headerHtmlOptions' => array('width' => '30px','style' => 'text-align:center;'),
            'htmlOptions' => array('style' => 'text-align:center;')
        ),		
		'title',
		array(
                    'name'=>'image',
                    'type' => 'html',
                    'htmlOptions' => array('style' => 'text-align:center;'),
                    'value'=> '$data->type == "image-upload" ? CHtml::image(Yii::app()->baseUrl."/upload/uploaded_$data->user_id/adminthumb/".$data->url, "image"):
                        ($data->type == "image-link" ? CHtml::image($data->url, "image") : "");'
                ),
		'user.username',
		/*
		'order',
		'created',
		'modified',
		*/
		array(
			'class'=>'CButtonColumn',
                        'template'=> ControllerActionsName::createIndexButtonRoles($actions),
		),
	),
)); ?>
