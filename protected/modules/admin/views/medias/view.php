<?php
$this->breadcrumbs=array(
	'Mediases'=>array('index'),
	$model->title,
);

$menus = array(
	array('label'=>'Create Medias', 'url'=>array('create')),
	array('label'=>'Update Medias', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete Medias', 'url'=>array('delete'), 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
);
$this->menu= ControllerActionsName::createMenusRoles($menus, $actions);
?>

<h1>View Medias #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(	
		'title',
        array(
        'name' => 'link',
        'value' => $model->type == "image-upload" ? Yii::app()->request->getBaseUrl(true)."/upload/uploaded_$model->user_id/".$model->url : "",
        ),
		'description',
		'type',
		array(
                    'name'=>'media',
                    'type' => 'raw',                    
                    'value'=> $model->type == "image-upload" ? CHtml::image(Yii::app()->baseUrl."/upload/uploaded_$model->user_id/adminshow/".$model->url, "image"):
                        ($model->type == "image-link" ? CHtml::image($model->url, "image") : $model->url)
                ),
		'user.username',		
		'created',
		'modified',
	),
)); ?>
