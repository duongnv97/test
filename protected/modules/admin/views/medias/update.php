<?php
$this->breadcrumbs=array(
	'Mediases'=>array('index'),
	$model->title=>array('view','id'=>$model->id),
	'Update',
);

$menus = array(	
        array('label'=>'Medias Management', 'url'=>array('index')),
	array('label'=>'View Medias', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Create Medias', 'url'=>array('create')),	
);
$this->menu= ControllerActionsName::createMenusRoles($menus, $actions);

?>

<h1>Update Medias <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model, 'mes' => $mes)); ?>