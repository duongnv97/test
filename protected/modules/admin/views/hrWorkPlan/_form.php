<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'work-plan-form',
	'enableAjaxValidation'=>false,
        'htmlOptions' => array('enctype' => 'multipart/form-data'),
)); ?>

    <p class="note">Dòng có dấu <span class="required">*</span> là bắt buộc.</p>
    <?php echo MyFormat::BindNotifyMsg(); ?>
    
        		
        
    <div class="row">
        <?php echo $form->labelEx($model,'status'); ?>
        <?php echo $form->dropDownList($model,'status', $model->getArrayStatus()); ?>
        <?php echo $form->error($model,'status'); ?>
    </div>
    <div class="row">
        <?php echo $form->labelEx($model,'approved'); ?>
        <?php echo $form->dropDownList($model,'approved', GasLeave::ListoptionApprove()); ?>
        <?php echo $form->error($model,'approved'); ?>
    </div>
    <div class="row">
        <?php // echo $form->labelEx($model,'approved_date'); ?>
        <?php //echo $form->textField($model,'approved_date'); ?>
        <?php //echo $form->error($model,'approved_date'); ?>
    </div>
    <div class="row">
        <?php //echo $form->labelEx($model,'notify'); ?>
        <?php //echo $form->textArea($model,'notify',array('rows'=>6, 'cols'=>50)); ?>
        <?php //echo $form->error($model,'notify'); ?>
    </div>
    <div class="row">
        <?php //echo $form->labelEx($model,'created_by'); ?>
        <?php //echo $form->textField($model,'created_by',array('size'=>11,'maxlength'=>11)); ?>
        <?php //echo $form->error($model,'created_by'); ?>
    </div>
    <div class="row">
        <?php //echo $form->labelEx($model,'created_date'); ?>
        <?php //echo $form->textField($model,'created_date'); ?>
        <?php //echo $form->error($model,'created_date'); ?>
    </div>
    <div class="row">
        <?php echo $form->labelEx($model,'role_id'); ?>
        <?php echo $form->dropDownList($model,'role_id',$model->getArrayRoles()); ?>
        <?php echo $form->error($model,'role_id'); ?>
    </div>
    <div class="row">
        <?php echo $form->labelEx($model,'date_from'); ?>
         <?php 
            $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                'model'=>$model,        
                'attribute'=>'date_from',
                'options'=>array(
                    'showAnim'=>'fold',
                    'dateFormat'=> "dd/mm/yy",
            //                            'minDate'=> '0',
            //                            'maxDate'=> '0',
                    'changeMonth' => true,
                    'changeYear' => true,
                    'showOn' => 'button',
                    'buttonImage'=> Yii::app()->theme->baseUrl.'/admin/images/icon_calendar_r.gif',
                    'buttonImageOnly'=> true,                                
                ),        
                'htmlOptions'=>array(
                    'class'=>'w-16',
                    'style'=>'height:20px;',
                        'readonly'=>'readonly',
                ),
            ));
            ?>  
        <?php echo $form->error($model,'date_from'); ?>
    </div>
    <div class="row">
        <?php echo $form->labelEx($model,'date_to'); ?>
         <?php 
            $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                'model'=>$model,        
                'attribute'=>'date_to',
                'options'=>array(
                    'showAnim'=>'fold',
                    'dateFormat'=> 'dd/mm/yy',
            //                            'minDate'=> '0',
            //                            'maxDate'=> '0',
                    'changeMonth' => true,
                    'changeYear' => true,
                    'showOn' => 'button',
                    'buttonImage'=> Yii::app()->theme->baseUrl.'/admin/images/icon_calendar_r.gif',
                    'buttonImageOnly'=> true,                                
                ),        
                'htmlOptions'=>array(
                    'class'=>'w-16',
                    'style'=>'height:20px;',
                        'readonly'=>'readonly',
                ),
            ));
            ?>  
        <?php echo $form->error($model,'date_to'); ?>
    </div>
	<div class="row buttons" style="padding-left: 141px;">
		        <?php $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType'=>'submit',
            'label'=>$model->isNewRecord ? 'Create' :'Save',
            'type'=>'null', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
            'size'=>'small', // null, 'large', 'small' or 'mini'
            //'htmlOptions' => array('style' => 'margin-bottom: 10px; float: right;'),
        )); ?>	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->
<script>
    $(document).ready(function(){
        $('.form').find('button:submit').click(function(){
            $.blockUI({ overlayCSS: { backgroundColor: '#fff' } }); 
        });
        
    });
</script>