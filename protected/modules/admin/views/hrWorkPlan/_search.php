<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=> GasCheck::getCurl(),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'status',array()); ?>
		<?php echo $form->dropDownList($model,'status', $model->getArrayStatus(), array('empty' => 'Select')); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'approved',array()); ?>
		<?php echo $form->dropDownList($model,'approved',GasLeave::ListoptionApprove(), array('empty' => 'Select')); ?>
	</div>

	<div class="row">
            <?php echo $form->labelEx($model,'approved_date'); ?>
             <?php 
                $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                    'model'=>$model,        
                    'attribute'=>'approved_date',
                    'options'=>array(
                        'showAnim'=>'fold',
                        'dateFormat'=> "dd/mm/yy",
                //                            'minDate'=> '0',
                //                            'maxDate'=> '0',
                        'changeMonth' => true,
                        'changeYear' => true,
                        'showOn' => 'button',
                        'buttonImage'=> Yii::app()->theme->baseUrl.'/admin/images/icon_calendar_r.gif',
                        'buttonImageOnly'=> true,                                
                    ),        
                    'htmlOptions'=>array(
                        'class'=>'w-16',
                        'style'=>'height:20px;',
//                            'readonly'=>'readonly',
                    ),
                ));
                ?>  
        </div>
    
	<div class="row">
            <?php echo $form->labelEx($model,'created_date'); ?>
             <?php 
                $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                    'model'=>$model,        
                    'attribute'=>'created_date',
                    'options'=>array(
                        'showAnim'=>'fold',
                        'dateFormat'=> "dd/mm/yy",
                //                            'minDate'=> '0',
                //                            'maxDate'=> '0',
                        'changeMonth' => true,
                        'changeYear' => true,
                        'showOn' => 'button',
                        'buttonImage'=> Yii::app()->theme->baseUrl.'/admin/images/icon_calendar_r.gif',
                        'buttonImageOnly'=> true,                                
                    ),        
                    'htmlOptions'=>array(
                        'class'=>'w-16',
                        'style'=>'height:20px;',
//                            'readonly'=>'readonly',
                    ),
                ));
                ?>  
        </div>

	<div class="row">
            <?php echo $form->labelEx($model,'role_id'); ?>
            <?php echo $form->dropDownList($model,'role_id',$model->getArrayRoles(), array('empty' => 'Select')); ?>
        </div>

	<div class="row">
            <?php echo $form->labelEx($model,'date_from'); ?>
             <?php 
                $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                    'model'=>$model,        
                    'attribute'=>'date_from',
                    'options'=>array(
                        'showAnim'=>'fold',
                        'dateFormat'=> "dd/mm/yy",
                //                            'minDate'=> '0',
                //                            'maxDate'=> '0',
                        'changeMonth' => true,
                        'changeYear' => true,
                        'showOn' => 'button',
                        'buttonImage'=> Yii::app()->theme->baseUrl.'/admin/images/icon_calendar_r.gif',
                        'buttonImageOnly'=> true,                                
                    ),        
                    'htmlOptions'=>array(
                        'class'=>'w-16',
                        'style'=>'height:20px;',
//                            'readonly'=>'readonly',
                    ),
                ));
                ?>  
        </div>
    
        <div class="row">
            <?php echo $form->labelEx($model,'date_to'); ?>
             <?php 
                $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                    'model'=>$model,        
                    'attribute'=>'date_to',
                    'options'=>array(
                        'showAnim'=>'fold',
                        'dateFormat'=> 'dd/mm/yy',
                //                            'minDate'=> '0',
                //                            'maxDate'=> '0',
                        'changeMonth' => true,
                        'changeYear' => true,
                        'showOn' => 'button',
                        'buttonImage'=> Yii::app()->theme->baseUrl.'/admin/images/icon_calendar_r.gif',
                        'buttonImageOnly'=> true,                                
                    ),        
                    'htmlOptions'=>array(
                        'class'=>'w-16',
                        'style'=>'height:20px;',
//                            'readonly'=>'readonly',
                    ),
                ));
                ?>  
        </div>

	<div class="row buttons" style="padding-left: 159px;">
		        <?php $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType'=>'submit',
            'label'=>'Search',
            'type'=>'null', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
            'size'=>'small', // null, 'large', 'small' or 'mini'
            //'htmlOptions' => array('style' => 'margin-bottom: 10px; float: right;'),
        )); ?>	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->