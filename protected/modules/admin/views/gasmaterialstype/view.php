<?php
$this->breadcrumbs=array(
	'Loại Vật Tư'=>array('index'),
	$model->name,
);

$menus = array(
	array('label'=>'Loại Vật Tư', 'url'=>array('index')),
	array('label'=>'Create Vật Tư', 'url'=>array('create')),
	array('label'=>'Update Vật Tư', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete Vật Tư', 'url'=>array('delete'), 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
);
$this->menu= ControllerActionsName::createMenusRoles($menus, $actions);
?>

<h1>View Loại Vật Tư: <?php echo $model->name; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'name',
		'status:status',
	),
)); ?>
