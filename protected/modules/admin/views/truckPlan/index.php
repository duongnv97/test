<?php
$this->breadcrumbs=array(
	$this->singleTitle,
);

$menus=array(
    array('label' => 'Xuất Excel', 'url' => array('index', 'to_excel' => 1), 'htmlOptions'=>array('class'=>'export_excel','label'=>'Xuất Excel')),
            array('label'=>"Create $this->singleTitle", 'url'=>array('create')),
);
$this->menu= ControllerActionsName::createMenusRoles($menus, $actions);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('truck-plan-grid', {
                url : $(this).attr('action'),
		data: $(this).serialize()
	});
	return false;
});
");

Yii::app()->clientScript->registerScript('ajaxupdate', "
$('#truck-plan-grid a.ajaxupdate').live('click', function() {
    $.fn.yiiGridView.update('truck-plan-grid', {
        type: 'POST',
        url: $(this).attr('href'),
        success: function() {
            $.fn.yiiGridView.update('truck-plan-grid');
        }
    });
    return false;
});
");
?>

<h1>Danh Sách <?php echo $this->pluralTitle; ?></h1>


<?php echo CHtml::link('Tìm Kiếm Nâng Cao','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none;">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'truck-plan-grid',
	'dataProvider'=>$model->search(),
        'afterAjaxUpdate'=>'function(id, data){ fnUpdateColorbox();}',
        'template'=>'{pager}{summary}{items}{pager}{summary}',
//    'itemsCssClass' => 'items custom_here',
//    'htmlOptions'=> array('class'=>'grid-view custom_here'),
        'pager' => array(
            'maxButtonCount'=>  CmsFormatter::$PAGE_MAX_BUTTON,
        ),
	'enableSorting' => false,
	//'filter'=>$model,
	'columns'=>array(
        array(
            'header' => 'S/N',
            'type' => 'raw',
            'value' => '$this->grid->dataProvider->pagination->currentPage * $this->grid->dataProvider->pagination->pageSize + ($row+1)',
            'headerHtmlOptions' => array('width' => '10px','style' => 'text-align:center;'),
            'htmlOptions' => array('style' => 'text-align:center;')
        ),
                array(
                    'name'=>'plan_date',
                    'type'=>'raw',
                    'value'=>'$data->getPlanDate()."<br>".$data->getAllFile()',
                ),
                array(
                    'header' => 'Tài xế/Xe',
                    'name'=>'car_id',
                    'type'=>'raw',
                    'value'=>'$data->getDriver()."<br>".$data->getCar()',
                ),
                array(
                    'name'=>'supplier_id',
                    'type'=>'raw',
                    'value'=>'$data->getSupplier()',
                ),
                array(
                    'name'=>'supplier_warehouse_id',
                    'type'=>'raw',
                    'value'=>'$data->getSupplierWarehouse()',
                ),
                array(
                    'header' => 'SL Nhận',
                    'type'=>'raw',
                    'value'=>'$data->getQtyReal(true)',
                    'htmlOptions' => array('style' => 'text-align:right;'),
                ),
//                array(
//                    'name'=>'driver_id',
//                    'type'=>'raw',
//                    'value'=>'$data->getDriver()',
//                ),
                array(
                    'header'=>'Chi tiết giao hàng',
                    'type'=>'raw',
                    'value'=>'$data->getHtmlShortTableDetail()',
                ),
                array(
                    'name'=>'type',
                    'type'=>'raw',
                    'value'=>'$data->getType()',
                ),
//                array(
//                    'name'=>'created_by',
//                    'type'=>'raw',
//                    'value'=>'$data->getCreatedBy()',
//                ),
                array(
                    'name'=>'status',
                    'type'=>'raw',
                    'value'=>'$data->getStatus()',
                    'htmlOptions' => array('style' => 'text-align:center;'),
                ),
                array(
                    'name'=>'created_date',
                    'type'=>'raw',
                    'value'=>'$data->getCreatedDate()',
                ),
		array(
                    'header' => 'Actions',
                    'class'=>'CButtonColumn',
                    'template'=> ControllerActionsName::createIndexButtonRoles($actions),
                    'buttons'=>array(
                        'update'=>array(
                            'visible'=> '$data->canUpdate()',
                        ),
                        'delete'=>array(
                            'visible'=> 'GasCheck::canDeleteData($data)',
                        ),
                    ),
		),
	),
)); ?>

<script>
$(document).ready(function() {
    fnUpdateColorbox();
});

function fnUpdateColorbox(){   
    fixTargetBlank();
    $(".gallery").colorbox({iframe:true,innerHeight:'1100', innerWidth: '1000',close: "<span title='close'>close</span>"});
}
</script>