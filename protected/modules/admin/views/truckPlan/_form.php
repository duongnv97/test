<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'truck-plan-form',
	'enableAjaxValidation'=>false,
        'htmlOptions' => array('enctype' => 'multipart/form-data'),
)); ?>

    <?php echo $form->errorSummary($model); ?>
    <p class="note">Dòng có dấu <span class="required">*</span> là bắt buộc.</p>
    <?php echo MyFormat::BindNotifyMsg(); ?>
    
    <div class="row buttons" style="padding-left: 141px;">
        <?php
        $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType' => 'submit',
            'label' => $model->isNewRecord ? 'Create' : 'Save',
            'type' => 'null', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
            'size' => 'small', // null, 'large', 'small' or 'mini'
                //'htmlOptions' => array('style' => 'margin-bottom: 10px; float: right;'),
        ));
        ?>
    </div>
    <?php if(!$model->showAutoDone()): ?>
        <div class="row GasCheckboxList">
            <label>Loại hoàn thành</label>
            <label><?php echo $model->getAutoDone();?></label>
        </div>
    <?php else: ?>
    <div class="row GasCheckboxList">
        <label>&nbsp;</label>
        <?php echo $form->radioButtonList($model,'auto_done', $model->getArrayAutoDone(),
                array(
                    'separator'=>"",
                    'template'=>'<li class="RadioItem">{input}{label}</li>',
                    'container'=>'ul',
                    'class'=>'RadioType' 
                )); 
    ?>
        <?php echo $form->error($model,'auto_done'); ?>
    </div>
    <?php endif; ?>
    <div class='clr'></div>
    
    <div class="row GasCheckboxList">
        <?php echo $form->labelEx($model,'status'); ?>
        <?php echo $form->radioButtonList($model,'status', TruckPlan::$aStatus,
                array(
                    'separator'=>"",
                    'template'=>'<li class="RadioItem">{input}{label}</li>',
                    'container'=>'ul',
                    'class'=>'RadioType' 
                )); 
    ?>
        <?php echo $form->error($model,'status'); ?>
    </div>
    <div class='clr'></div>
    
    <div class="row GasCheckboxList">
        <?php echo $form->labelEx($model,'type'); ?>
        <?php echo $form->radioButtonList($model,'type', TruckPlan::$aType, 
                array(
                    'separator'=>"",
                    'template'=>'<li class="RadioItem">{input}{label}</li>',
                    'container'=>'ul',
                    'class'=>'RadioType' 
                )); 
    ?>
        <?php echo $form->error($model,'type'); ?>
    </div>
    <div class='clr'></div>
    
    <div class="row">
        <?php echo $form->labelEx($model, 'plan_date'); ?>
        <?php 
        $this->widget('zii.widgets.jui.CJuiDatePicker', array(
            'model' => $model,

            'attribute' => 'plan_date',
            'options' => array(
                'showAnim' => 'fold',
                'dateFormat'=> ActiveRecord::getDateFormatJquery(),
                'changeMonth' => true,
                'changeYear' => true,
                'showOn' => 'button',
                'buttonImage' => Yii::app()->theme->baseUrl . '/admin/images/icon_calendar_r.gif',
                'buttonImageOnly' => true,

            ),

            'htmlOptions' => array(
                'class' => 'w-16',
                'style' => 'height:20px;',
                'readonly' => 'readonly',
            ),
        ));
        ?>  
        <?php echo $form->error($model, 'plan_date'); ?>
    </div>
    <div class="row ">
        <?php echo $form->labelEx($model, 'driver_id'); ?>
        <?php echo $form->hiddenField($model, 'driver_id'); ?>
        <?php
        // 1. limit search kh của sale
        $url = Yii::app()->createAbsoluteUrl('admin/ajax/search_by_role', array('role' => ROLE_DRIVER));
        // widget auto complete search user customer and supplier
        $aData = array(
            'model' => $model,
            'field_customer_id' => 'driver_id',
            'url' => $url,
            'name_relation_user' => 'rDriver',
            'ClassAdd' => 'w-300',
            'placeholder' => 'Nhập mã hoặc tên tài xế',
        );
        $this->widget('ext.SpaAutocompleteCustomer.SpaAutocompleteCustomer', array('data' => $aData));
        ?>
        <?php echo $form->error($model, 'driver_id'); ?>
    </div>
    
    <div class="row ">
        <?php echo $form->labelEx($model, 'car_id'); ?>
        <?php echo $form->hiddenField($model, 'car_id'); ?>
        <?php
        // 1. limit search kh của sale
        $url = Yii::app()->createAbsoluteUrl('admin/ajax/search_by_role', array('role' => ROLE_CAR));
        // widget auto complete search user customer and supplier
        $aData = array(
            'model' => $model,
            'field_customer_id' => 'car_id',
            'url' => $url,
            'name_relation_user' => 'rCar',
            'ClassAdd' => 'w-300',
            'placeholder' => 'Nhập mã hoặc bs xe',
            'field_autocomplete_name'=>'autocomplete_name2'
        );
        $this->widget('ext.SpaAutocompleteCustomer.SpaAutocompleteCustomer', array('data' => $aData));
        ?>
        <?php echo $form->error($model, 'car_id'); ?>
    </div>
    
    <div class="row">
        <?php echo $form->labelEx($model,'created_by_note'); ?>
        <?php echo $form->textArea($model,'created_by_note',array('class'=>'w-500', 'rows'=>5)); ?>
        <?php echo $form->error($model,'created_by_note'); ?>
    </div>
    <?php include '_form_hire_transport.php'; ?>
    <?php include '_form_sj_shipping.php'; ?>
    <h3 class='title-info'>Chi tiết hàng nhận</h3>
    <div class="row">
        <?php echo $form->labelEx($model,'supplier_id'); ?>
        <?php echo $form->dropdownList($model,'supplier_id', $model->getArraySupplier(), array('class'=>'w-300', 'empty' => 'Select')); ?>
        <?php echo $form->error($model,'supplier_id'); ?>
    </div>
    
    <div class="row ">
        <?php echo $form->labelEx($model,'supplier_warehouse_id'); ?>
        <?php echo $form->dropdownList($model,'supplier_warehouse_id', $model->getArraySupplierWarehouse(), array('class'=>'w-300', 'empty' => 'Select')); ?>
        <?php echo $form->error($model,'supplier_warehouse_id'); ?>
    </div>
    
    <div class="row">
        <?php echo $form->labelEx($model,'qty_plan'); ?>
        <?php echo $form->textField($model,'qty_plan', ['class'=>'f_size_15 w-300 ad_fix_currency number_only', 'maxlength' => 8]); ?>
        <?php echo $form->error($model,'qty_plan'); ?>
    </div>
    
    <div class="row">
        <?php echo $form->labelEx($model,'qty_real'); ?>
        <?php echo $form->textField($model,'qty_real', ['class'=>'f_size_15 w-300 ad_fix_currency number_only', 'maxlength' => 8]); ?>
        <?php echo $form->error($model,'qty_real'); ?>
    </div>
    
    <div class="row">
        <?php echo $form->labelEx($model,'supplier_price'); ?>
        <?php echo $form->textField($model,'supplier_price', ['class'=>'f_size_15 w-300 ad_fix_currency number_only', 'maxlength' => 8]); ?>
        <?php echo $form->error($model,'supplier_price'); ?>
    </div>
    
    <div class="row">
        <?php echo $form->labelEx($model,'price_usd_ton'); ?>
        <?php echo $form->textField($model,'price_usd_ton', ['class'=>'f_size_15 w-300 ad_fix_currency number_only', 'maxlength' => 9]); ?>
        <?php echo $form->error($model,'price_usd_ton'); ?>
    </div>
    <div class="row">
        <?php echo $form->labelEx($model,'exchange_rate'); ?>
        <?php echo $form->textField($model,'exchange_rate', ['class'=>'f_size_15 w-300 ad_fix_currency number_only', 'maxlength' => 6]); ?>
        <?php echo $form->error($model,'exchange_rate'); ?>
    </div>
    
    <div class="row">
        <label>&nbsp;</label>
        <div class="float_l"><?php echo $model->getImgImport();?></div>
    </div>
    <div class="clr"></div>
    <div class="row f_size_18">
        <?php echo $form->labelEx($model,'gas_remain'); ?>
        <?php echo $model->getGasRemain(true)." kg. Thành tiền {$model->getGasRemainAmount(true)}"; ?>
    </div>
    
    <?php include '_form_detail.php'; ?>
    
    <div class="row buttons" style="padding-left: 141px;">
        <?php
        $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType' => 'submit',
            'label' => $model->isNewRecord ? 'Create' : 'Save',
            'type' => 'null', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
            'size' => 'small', // null, 'large', 'small' or 'mini'
                //'htmlOptions' => array('style' => 'margin-bottom: 10px; float: right;'),
        ));
        ?>	
    </div>

<?php $this->endWidget(); ?>

</div><!-- form -->

<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/autoNumeric-master/autoNumeric.js"></script>
<script>
    
    /* Jul0519 DungNT close RadioType  Choose type effect - đoạn xử lý event cho radio này đc làm ở bindRadioEvent trong huongminh.js
    $('.RadioType').each(function(){
        var isCheck = $(this).attr('checked');
        if(isCheck == 'checked'){
            $(this).closest('li.RadioItem').css('background','rgb(76, 175, 80)');
        }
    });
    
    // Choose type effect
    $('.RadioItem').on('click', function(){
        var radioButton = $(this).children('input.RadioType');
        if(radioButton.is(':checked')){
            $('.RadioItem').css('background','white');
            $(this).css('background','rgb(76, 175, 80)');
        } else {
            $('.RadioItem').css('background','rgb(76, 175, 80)');
            $(this).css('background','white');
        }
    });*/
    
    // Load warehouse khi chon supplier
    /* DungNT close Jun09 - ko su dung
    $('#TruckPlan_supplier_id').on('change', function(){
        var supplier_id = $(this).val();
        var urlWH = '<?php echo Yii::app()->createUrl("admin/TruckPlan/create", ['loadWareHouse'=>1]); ?>';
        $.blockUI({ overlayCSS: { backgroundColor: '#fff' } }); 
        $.ajax({
            url: urlWH,
            data: {supplier_id: supplier_id},
            type: 'get',
//            dataType:'json',
            success: function(data){
                $.unblockUI();
                $('#TruckPlan_supplier_warehouse_id').html(data);
            }
        });
    });
     */
    
    $(document).ready(function(){
        bindRadioEvent();
        $('.form').find('button:submit').click(function(){
            $.blockUI({ overlayCSS: { backgroundColor: '#fff' } }); 
        });
        fnInitInputCurrency();
        $(".gallery").colorbox({iframe:true,innerHeight:'1100', innerWidth: '1000',close: "<span title='close'>close</span>"});
    });
    
</script>