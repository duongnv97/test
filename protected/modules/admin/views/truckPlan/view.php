<?php
$this->breadcrumbs=array(
    $this->pluralTitle => array('index'),
    ' ' . $this->singleTitle . ' : ',
);

$menus = array(
	array('label'=>"$this->pluralTitle Management", 'url'=>array('index')),
	array('label'=>"Tạo Mới $this->singleTitle", 'url'=>array('create')),
	array('label'=>"Cập Nhật $this->singleTitle", 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>"Xóa $this->singleTitle", 'url'=>array('delete'), 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Bạn chắc chắn muốn xóa ?')),
);
$this->menu= ControllerActionsName::createMenusRoles($menus, $actions);
?>

<h1>Xem: <?php echo $this->singleTitle; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
            array(
                'name'=>'type',
                'type'=>'raw',
                'value'=>$model->getType(),
            ),
            array(
                'name'=>'status',
                'type'=>'raw',
                'value'=>$model->getStatus(),
            ),
            array(
                'name'=>'plan_date',
                'type'=>'raw',
                'value'=>$model->getPlanDate(),
            ),
            array(
                'name'=>'supplier_id',
                'type'=>'raw',
                'value'=>$model->getSupplier(),
            ),
            array(
                'name'=>'supplier_warehouse_id',
                'type'=>'raw',
                'value'=>$model->getSupplierWarehouse(),
            ),
            'qty_plan',
            'qty_real',
            array(
                'name'=>'supplier_price',
                'type'=>'raw',
                'value'=> ActiveRecord::formatCurrency($model->supplier_price),
            ),
            array(
                'name'=>'transport_price',
                'type'=>'raw',
                'value'=>ActiveRecord::formatCurrency($model->transport_price),
            ),
            array(
                'name'=>'driver_id',
                'type'=>'raw',
                'value'=>$model->getDriver(),
            ),
            array(
                'name'=>'car_id',
                'type'=>'raw',
                'value'=>$model->getCar(),
            ),
            'driver_note',
            array(
                'name'=>'created_by',
                'type'=>'raw',
                'value'=>$model->getCreatedBy(),
            ),
            array(
                'name'=>'created_date',
                'type'=>'raw',
                'value'=>$model->getCreatedDate(),
            ),
	),
));
?>
<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'truck-plan-form',
	'enableAjaxValidation'=>false,
        'htmlOptions' => array('enctype' => 'multipart/form-data'),
)); ?>
<div class="row">
    <?php include '_form_detail_table.php'; ?>
</div>
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/autoNumeric-master/autoNumeric.js"></script>
<script>
$(document).ready(function(){
    fnInitInputCurrency();
    $(".gallery").colorbox({iframe:true,innerHeight:'1100', innerWidth: '1000',close: "<span title='close'>close</span>"});
});
</script>
<?php $this->endWidget(); ?>