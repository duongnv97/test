<table class="materials_table hm_table">
    <thead>
        <tr>
            <th class="item_c">#</th>
            <th class="item_c w-300">Khách hàng</th>
            <th class="item_c w-100">Kho giao khác</th>
            <th class=" item_c">Giá bán USD/tấn</th>
            <th class=" item_c">Tỷ giá bán</th>
            <th class=" item_c">Giá bán</th>
            <th class=" item_c">SL dự kiến</th>
            <th class="item_c">SL thực tế</th>
            <th class="item_c">Trạng thái</th>
            <th class="item_c ">Ghi chú</th>
            <th class="last item_c">Xóa</th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($model->getArrayDetailExport() as $mDetail) : ?>
            <tr class="materials_row">
                <td class="item_c order_no"></td>
                <td>
                    <?php 
                    $mUser = $mDetail->rCustomer;
                    $label = empty($mUser) ? '' : $mUser->code_bussiness.' - '.$mUser->id.' - '.$mUser->first_name;
                    echo $label.$mDetail->getViewImg();
                    ?>
                    <input name="TruckPlanDetail[customer_id][]" value="<?php echo $mUser->id ?>" type="hidden">
                    <input name="TruckPlanDetail[id][]" value="<?php echo $mDetail->id ?>" type="hidden">
                </td>
                <td>
                    <textarea name="TruckPlanDetail[customer_warehouse_other][]" class="w-150" rows="3" ><?php echo $mDetail->customer_warehouse_other;?></textarea>
                </td>
                <td class="item_c">
                    <input name="TruckPlanDetail[price_usd_ton][]" value="<?php echo $mDetail->price_usd_ton; ?>" class="w-80 number_only ad_fix_currency f_size_15" type="text">
                </td>
                <td class="item_c">
                    <input name="TruckPlanDetail[exchange_rate][]" value="<?php echo $mDetail->exchange_rate; ?>" class="w-80 number_only ad_fix_currency f_size_15" type="text">
                </td>
                <td class="item_c">
                    <input name="TruckPlanDetail[customer_price][]" value="<?php echo $mDetail->customer_price; ?>" class="w-80 number_only ad_fix_currency f_size_15" type="text">
                </td>
                    <td class="item_c">
                    <input name="TruckPlanDetail[qty_plan][]" value="<?php echo $mDetail->qty_plan; ?>" class="w-80 number_only ad_fix_currency f_size_15" type="text" maxlength="9">
                </td>
                <td class="item_c">
                    <input name="TruckPlanDetail[qty_real][]" value="<?php echo $mDetail->qty_real; ?>" class="w-80 number_only ad_fix_currency f_size_15" type="text" maxlength="9">
                </td>
                <td class="item_c">
                <?php 
                echo $form->dropdownList($mDetail,'status', TruckPlan::$aStatusDetail,array('class'=>'w-100', 'name'=>'TruckPlanDetail[status][]')); 
                ?>
                </td>
                <td class="item_c">
                    <!--<textarea name="TruckPlanDetail[created_by_note][]" class="w-150" rows="3" ></textarea>-->
                    <textarea name="TruckPlanDetail[created_by_note][]" class="w-150" rows="3" ><?php echo $mDetail->created_by_note;?></textarea>
                </td>
                <td class="item_c last"><span class="remove_icon_only"></span></td>
            </tr>
        <?php endforeach; ?>
    </tbody>
</table>