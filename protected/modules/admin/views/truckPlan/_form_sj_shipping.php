<h3 class='title-info'>Chi tiết vận chuyển thuê</h3>

<div class="row">
    <?php echo $form->labelEx($model,'sj_shipping_price'); ?>
    <?php echo $form->textField($model,'sj_shipping_price', ['class'=>'f_size_15 w-300 ad_fix_currency number_only', 'maxlength' => 8]); ?>
    <?php echo $form->error($model,'sj_shipping_price'); ?>
</div>

<div class="row">
    <?php echo $form->labelEx($model,'sj_shipping_price_gas_remain'); ?>
    <?php echo $form->textField($model,'sj_shipping_price_gas_remain', ['class'=>'f_size_15 w-300 ad_fix_currency number_only', 'maxlength' => 8]); ?>
    <?php echo $form->error($model,'sj_shipping_price_gas_remain'); ?>
</div>

