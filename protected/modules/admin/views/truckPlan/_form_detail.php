<h3 class='title-info'>Chi tiết hàng giao</h3>

<div class="row ">
    <?php $mDetail = new TruckPlanDetail(); ?>
    <?php echo $form->labelEx($mDetail, 'customer_id'); ?>
    <?php echo $form->hiddenField($mDetail, 'customer_id'); ?>
    <?php
    // 1. limit search kh của sale
//    $url = Yii::app()->createAbsoluteUrl('admin/ajax/search_by_role', array('role' => ROLE_CUSTOMER));
    $url = Yii::app()->createAbsoluteUrl('admin/ajax/search_user_by_code');
    // widget auto complete search user customer and supplier
    $aData = array(
        'model'                 => $mDetail,
        'field_customer_id'     => 'customer_id',
        'url'                   => $url,
        'name_relation_user'    => 'rCustomer',
        'ClassAdd'              => 'w-300',
        'fnSelectCustomerV2'    => true,
        'ShowTableInfo'         => 0,
        'doSomethingOnClose'    => true,
        'placeholder'           => 'Nhập mã hoặc tên khách hàng',
    );
    $this->widget('ext.SpaAutocompleteCustomer.SpaAutocompleteCustomer', array('data' => $aData));
    ?>
    <?php echo $form->error($model,'customer_id'); ?>
</div>
<div class="row">
    <?php include '_form_detail_table.php'; ?>
</div>

<script>
    function fnCallSomeFunctionAfterSelectV2(ui, idField, idFieldCustomerID){
        $('tr.selected').removeClass('selected');
        var existsElm = $('.materials_table tr > td > input[value='+ui.item.id+']');
        if(existsElm.length) {
            var tr = existsElm.closest('tr.materials_row');
            $(tr).addClass('selected');
            $(tr).fadeIn(100).fadeOut(100).fadeIn(100).fadeOut(100).fadeIn(100);
            return;
        }
        fnBuildRowMaterial(ui);
    }
    function doSomethingOnClose(ui, idField, idFieldCustomerID){
        var this_ = $(idField).siblings('.remove_row_item');
        fnRemoveName(this_, idField, idFieldCustomerID);
    }
    function fnBuildRowMaterial(ui){
        var _tr = '';
            _tr += '<tr class="materials_row">';
                _tr += '<td class="item_c order_no"></td>';
                _tr += '<td>';
                _tr +=      ui.item.label;
                _tr +=      '<input name="TruckPlanDetail[customer_id][]" value="'+ui.item.id+'" type="hidden">';
                _tr += '</td>';
                _tr += '<td>';
                _tr +=      '<textarea name="TruckPlanDetail[customer_warehouse_other][]" class="w-150" rows="3" ><?php echo $mDetail->customer_warehouse_other;?></textarea>';
                _tr += '</td>';
                
                _tr += '<td class="item_c">';
                _tr +=      '<input name="TruckPlanDetail[price_usd_ton][]" value="" class="w-80 number_only ad_fix_currency f_size_15" type="text">';
                _tr += '</td>';
                
                _tr += '<td class="item_c">';
                _tr +=      '<input name="TruckPlanDetail[exchange_rate][]" value="" class="w-80 number_only ad_fix_currency f_size_15" type="text">';
                _tr += '</td>';
                
                _tr += '<td class="item_c">';
                _tr +=      '<input name="TruckPlanDetail[customer_price][]" value="" class="w-80 number_only ad_fix_currency f_size_15" type="text">';
//                _tr +=      '<div style="font-weight:bold; color:red;"></div>';
                _tr += '</td>';
                
                _tr += '<td class="item_c">';
                _tr +=      '<input name="TruckPlanDetail[qty_plan][]" value="" class="w-80  number_only ad_fix_currency f_size_15" type="text" maxlength="9">';
                _tr += '</td>';
                _tr += '<td class="item_c">';
                _tr +=      '<input name="TruckPlanDetail[qty_real][]" value="" class="w-80  number_only ad_fix_currency f_size_15" type="text" maxlength="9">';
                _tr += '</td>';
                _tr += '<td class="item_c">';
                _tr += '<?php 
                $dropdownStaus = $form->dropdownList($mDetail,'status[]', TruckPlan::$aStatus,array('class'=>'w-100')); 
                echo preg_replace( "/\r|\n/", "", $dropdownStaus ); // remove all line break to run script
                ?>';
                _tr += '</td>';
                _tr += '<td class="item_c">';
                _tr +=      '<textarea name="TruckPlanDetail[created_by_note][]" class="w-150" rows="3"></textarea>';
                _tr += '</td>';
                _tr += '<td class="item_c last"><span class="remove_icon_only"></span></td>';
            _tr += '</tr>';
        $('.materials_table tbody').append(_tr);
        fnRefreshOrderNumber();
        bindEventForHelpNumber();
        fnInitInputCurrency();
    }
    $(document).ready(function(){
        fnRefreshOrderNumber();
        fnBindRemoveIcon();
    });
</script>