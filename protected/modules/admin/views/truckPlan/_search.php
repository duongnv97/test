<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=> GasCheck::getCurl(),
	'method'=>'get',
)); ?>

	<div class="row display_none">
            <?php echo $form->label($model,'code_no'); ?>
            <?php echo $form->textField($model,'code_no',array('class'=>'w-200')); ?>
	</div>

        <div class="row more_col">
            <div class="col2">
                <?php echo $form->label($model,'supplier_id'); ?>
                <?php echo $form->dropdownList($model,'supplier_id',$model->getArraySupplier(),array('class'=>'w-200', 'empty'=>'Select')); ?>
            </div>
            <div class="col3">
                <?php echo $form->label($model,'supplier_warehouse_id'); ?>
                <?php echo $form->dropdownList($model,'supplier_warehouse_id',$model->getArraySupplierWarehouse(),array('class'=>'w-200', 'empty'=>'Select')); ?>
            </div>
            <div class="col1">
                <?php echo $form->label($model,'type'); ?>
                <?php echo $form->dropdownList($model,'type', TruckPlan::$aType,array('class'=>'w-200', 'empty'=>'Select')); ?>
            </div>
        </div>
	<div class="row">
            <?php echo $form->label($model,'status'); ?>
            <?php echo $form->dropdownList($model,'status', TruckPlan::$aStatus,array('class'=>'w-200', 'empty'=>'Select')); ?>
	</div>
    
        <div class="row more_col">
            <div class="col1">
                <?php echo $form->labelEx($model, 'plan_date_from', ['label'=>'Từ ngày']); ?>
                <?php 
                $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                    'model' => $model,
                    'attribute' => 'plan_date_from',
                    'options' => array(
                        'showAnim' => 'fold',
                        'dateFormat' => MyFormat::$dateFormatSearch,
                        'changeMonth' => true,
                        'changeYear' => true,
                        'showOn' => 'button',
                        'buttonImage' => Yii::app()->theme->baseUrl . '/admin/images/icon_calendar_r.gif',
                        'buttonImageOnly' => true,

                    ),

                    'htmlOptions' => array(
                        'class' => 'w-16',
                        'style' => 'height:20px;',
                    ),
                ));
                ?> 
            </div>
            <div class="col2">
                <?php echo $form->labelEx($model, 'plan_date_to', ['label'=>'Đến ngày']); ?>
                <?php 
                $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                    'model' => $model,
                    'attribute' => 'plan_date_to',
                    'options' => array(
                        'showAnim' => 'fold',
                        'dateFormat' => MyFormat::$dateFormatSearch,
                        'changeMonth' => true,
                        'changeYear' => true,
                        'showOn' => 'button',
                        'buttonImage' => Yii::app()->theme->baseUrl . '/admin/images/icon_calendar_r.gif',
                        'buttonImageOnly' => true,

                    ),

                    'htmlOptions' => array(
                        'class' => 'w-16',
                        'style' => 'height:20px;',
                    ),
                ));
                ?> 
            </div>
        </div>
    
        <div class="row ">
            <?php echo $form->label($model, 'driver_id'); ?>
            <?php echo $form->hiddenField($model, 'driver_id'); ?>
            <?php
            // 1. limit search kh của sale
            $url = Yii::app()->createAbsoluteUrl('admin/ajax/search_by_role', array('role' => ROLE_DRIVER));
            // widget auto complete search user customer and supplier
            $aData = array(
                'model' => $model,
                'field_customer_id' => 'driver_id',
                'url' => $url,
                'name_relation_user' => 'rDriver',
                'ClassAdd' => 'w-500',
                'placeholder' => 'Nhập mã hoặc tên tài xế',
            );
            $this->widget('ext.SpaAutocompleteCustomer.SpaAutocompleteCustomer', array('data' => $aData));
            ?>
        </div>
    
        <div class="row ">
            <?php echo $form->label($model, 'car_id'); ?>
            <?php echo $form->hiddenField($model, 'car_id'); ?>
            <?php
            // 1. limit search kh của sale
            $url = Yii::app()->createAbsoluteUrl('admin/ajax/search_by_role', array('role' => ROLE_CAR));
            // widget auto complete search user customer and supplier
            $aData = array(
                'model' => $model,
                'field_customer_id' => 'car_id',
                'url' => $url,
                'name_relation_user' => 'rCar',
                'ClassAdd' => 'w-500',
                'placeholder' => 'Nhập mã hoặc bs xe',
                'field_autocomplete_name'=>'autocomplete_name2'
            );
            $this->widget('ext.SpaAutocompleteCustomer.SpaAutocompleteCustomer', array('data' => $aData));
            ?>
        </div>

        <div class="row">
            <?php echo $form->label($model,'customer_id'); ?>
            <?php echo $form->hiddenField($model,'customer_id'); ?>
            <?php 
                // widget auto complete search user customer and supplier
                $aData = array(
                    'model'=>$model,
                    'field_customer_id'=>'customer_id',
                    'name_relation_user'=>'rCustomer',
                    'url' => Yii::app()->createAbsoluteUrl('admin/ajax/search_user_by_code'),
                    'ClassAdd' => 'w-500',
                    'field_autocomplete_name'=>'autocomplete_name3'
                );
                $this->widget('ext.SpaAutocompleteCustomer.SpaAutocompleteCustomer',
                    array('data'=>$aData));                                        
            ?>
        </div>

	<div class="row buttons" style="padding-left: 159px;">
		        <?php $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType'=>'submit',
            'label'=>'Search',
            'type'=>'null', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
            'size'=>'small', // null, 'large', 'small' or 'mini'
            //'htmlOptions' => array('style' => 'margin-bottom: 10px; float: right;'),
        )); ?>	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->