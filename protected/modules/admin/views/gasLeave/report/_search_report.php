<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=> Yii::app()->createAbsoluteUrl('admin/gasLeave/report'),
	'method'=>'get',
)); ?>
        <div class="row">
            <?php // echo $form->label($model,'report_year'); ?>
            <?php // echo $form->dropDownList($model,'report_year', ActiveRecord::getRangeYear(2016, date('Y')+10),array('class'=>'w-200')); ?>
        </div>
        <div class="row more_col">
            <div class="col1">
                    <?php echo Yii::t('translation', $form->label($model,'date_from')); ?>
                    <?php 
                        $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                            'model'=>$model,        
                            'attribute'=>'date_from',
                            'options'=>array(
                                'showAnim'=>'fold',
                                'dateFormat'=> MyFormat::$dateFormatSearch,
    //                            'minDate'=> '0',
                                'maxDate'=> '0',
                                'changeMonth' => true,
                                'changeYear' => true,
                                'showOn' => 'button',
                                'buttonImage'=> Yii::app()->theme->baseUrl.'/admin/images/icon_calendar_r.gif',
                                'buttonImageOnly'=> true,                                
                            ),        
                            'htmlOptions'=>array(
                                'class'=>'w-16',
                                'readonly'=>1,
                                'style'=>'float:left;',
                            ),
                        ));
                    ?>     		
            </div>
            <div class="col2">
                    <?php echo Yii::t('translation', $form->label($model,'date_to')); ?>
                    <?php 
                        $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                            'model'=>$model,        
                            'attribute'=>'date_to',
                            'options'=>array(
                                'showAnim'=>'fold',
                                'dateFormat'=> MyFormat::$dateFormatSearch,
    //                            'minDate'=> '0',
                                'maxDate'=> '0',
                                'changeMonth' => true,
                                'changeYear' => true,
                                'showOn' => 'button',
                                'buttonImage'=> Yii::app()->theme->baseUrl.'/admin/images/icon_calendar_r.gif',
                                'buttonImageOnly'=> true,                                
                            ),        
                            'htmlOptions'=>array(
                                'class'=>'w-16',
                                'readonly'=>1,
                                'style'=>'float:left;',
                            ),
                        ));
                    ?>     		
            </div>
        </div>
        <div class="row">      
            <?php // echo $form->textField($model,'uid_leave', array('class'=>'uid_auto_hidden uid_leave_hidden', 'class_update_val'=>'uid_leave_hidden')); ?>
            <?php echo $form->hiddenField($model,'uid_leave', array('class'=>'uid_auto_hidden uid_leave_hidden', 'class_update_val'=>'uid_leave_hidden')); ?>
            <?php echo $form->labelEx($model,'uid_leave'); ?>
            <?php 
                // widget auto complete search user customer and supplier
                $aDataAuto = array(
                    'model'=>$model,
                    'field_customer_id'=>'uid_leave',
                    'url'=> Yii::app()->createAbsoluteUrl('admin/ajax/search_user_leave'),
                    'name_relation_user'=>'rUidLeave',
                    'field_autocomplete_name'=>'autocomplete_name_v1',
//                    'fnSelectCustomer'=>'fnAfterSelectSaleOrAgent',
                );
                $this->widget('ext.SpaAutocompleteCustomer.SpaAutocompleteCustomer',
                    array('data'=>$aDataAuto));
            ?>                        
        </div>
    
	<div class="row buttons" style="padding-left: 159px;">
            <?php $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType'=>'submit',
            'label'=>'Search',
            'type'=>'null', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
            'size'=>'small', // null, 'large', 'small' or 'mini'
            //'htmlOptions' => array('style' => 'margin-bottom: 10px; float: right;'),
        )); ?>	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->