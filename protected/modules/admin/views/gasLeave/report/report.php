<?php
$this->breadcrumbs=array(
	$this->pageTitle,
);
?>

<h1>Báo cáo nghỉ phép</h1>
<?php include '_search_report.php'; ?>
<?php if(isset($aData['UID'])): ?>
<?php 
$OUTPUT     = $aData['OUTPUT'];
$MONTH      = $aData['MONTH'];
$MODEL_USER = $aData['MODEL_USER'];
$key = 1;
$leaveOneYear = 12;
?>
<table class="tb hm_table">
    <thead>
        <tr>
            <th>#</th>
            <th class="item_c item_b">Nhân viên</th>
            <?php foreach ($MONTH as $mth): ?>
                <th class="item_c item_b">TH <?php echo $mth;?></th>
            <?php endforeach; ?>
            <th>Tổng</th>
            <th>Còn lại</th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($aData['UID'] as $uid_leave): ?>
        <?php 
            if(!isset($MODEL_USER[$uid_leave])){
                continue;
            }
            $sumRow = 0;
            $nameEmployee = $MODEL_USER[$uid_leave]->code_bussiness . ' - '. $MODEL_USER[$uid_leave]->first_name;
        ?>
        <tr>
            <td class="item_c"><?php echo $key++;?></td>
            <td><?php echo $nameEmployee;?></td>
            <?php foreach ($MONTH as $mth): ?>
            <?php 
                $off = isset($OUTPUT[$uid_leave][$mth]) ? $OUTPUT[$uid_leave][$mth] : '';
                $sumRow += $off;
            ?>
                <td class="item_c"><?php echo $off;?></td>
            <?php endforeach; ?>
            <td class="item_c item_b"><?php echo $sumRow;?></td>
            <td class="item_c item_b"><?php echo $leaveOneYear - $sumRow;?></td>
        </tr>
        <?php endforeach; ?>
    </tbody>
</table>
<?php endif; ?>
<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/print-store-card.css" />
<script>
    $(function(){
        $('.class_remove_row').each(function(){
            $(this).closest('tr').remove();
        });
    });
</script>