<?php
$this->breadcrumbs=array(
	$this->pageTitle,
);

$menus=array(
            array('label'=>"Create $this->pageTitle", 'url'=>array('create')),
);
$this->menu= ControllerActionsName::createMenusRoles($menus, $actions);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('users-hgd-pttt-grid', {
                url : $(this).attr('action'),
		data: $(this).serialize()
	});
	return false;
});
");

Yii::app()->clientScript->registerScript('ajaxupdate', "
$('#users-hgd-pttt-grid a.ajaxupdate').live('click', function() {
    $.fn.yiiGridView.update('users-hgd-pttt-grid', {
        type: 'POST',
        url: $(this).attr('href'),
        success: function() {
            $.fn.yiiGridView.update('users-hgd-pttt-grid');
        }
    });
    return false;
});
");
?>

<h1>Danh Sách <?php echo $this->pageTitle; ?></h1>


<?php echo CHtml::link('Tìm Kiếm Nâng Cao','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'users-hgd-pttt-grid',
	'dataProvider'=>$model->search(),
        'afterAjaxUpdate'=>'function(id, data){ fnUpdateColorbox();}',
        'template'=>'{pager}{summary}{items}{pager}{summary}',
//    'itemsCssClass' => 'items custom_here',
//    'htmlOptions'=> array('class'=>'grid-view custom_here'),
        'pager' => array(
            'maxButtonCount'=>  CmsFormatter::$PAGE_MAX_BUTTON,
        ),
	'enableSorting' => false,
	//'filter'=>$model,
	'columns'=>array(
        array(
            'header' => 'S/N',
            'type' => 'raw',
            'value' => '$this->grid->dataProvider->pagination->currentPage * $this->grid->dataProvider->pagination->pageSize + ($row+1)',
            'headerHtmlOptions' => array('width' => '10px','style' => 'text-align:center;'),
            'htmlOptions' => array('style' => 'text-align:center;')
        ),
            array(
                'type' => 'raw',
                'name' => 'first_name',
                'value' => '$data->getCustomerInfo()'
            ),
            array(
                'type' => 'raw',
                'name' => 'temp_password',
                'value' => '$data->getPTTTcode()'
            ),
            array(
                'type' => 'raw',
                'name' => 'area_code_id',
                'value' => '$data->getAgent()'
            ),
            array(
                'type' => 'raw',
                'name' => 'address',
                'value' => '$data->getAddress()'
            ),
            array(
                'name' => 'username', // Số seri
                'type' => 'raw',
                'value' => '$data->username'
            ),
            array(
                'name' => 'email', // Thương hiệu vỏ
                'type' => 'raw',
                'value' => '$data->getShellBrand()'
            ),
            array(
                'name' => 'password_hash', // Tên Dịch Vụ Đối Thủ
                'type' => 'raw',
                'value' => '$data->password_hash'
            ),
            array(
                'type' => 'raw',
                'name' => 'created_by',
                'value' => '$data->getCreatedBy()'
            ),
            array(
                'type' => 'raw',
                'name' => 'created_date',
                'value' => '$data->getCreatedDate()'
            ),
//		array(
//                    'header' => 'Actions',
//                    'class'=>'CButtonColumn',
//                    'template'=> ControllerActionsName::createIndexButtonRoles($actions),
//                    'buttons'=>array(
//                        'update' => array(
//                            'visible' => '$data->canUpdate()',
//                        ),
//                    ),
//		),
	),
)); ?>

<script>
$(document).ready(function() {
    fnUpdateColorbox();
});

function fnUpdateColorbox(){   
    fixTargetBlank();
}
</script>