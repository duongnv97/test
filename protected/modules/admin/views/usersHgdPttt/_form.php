<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'users-hgd-pttt-form',
	'enableAjaxValidation'=>false,
        'htmlOptions' => array('enctype' => 'multipart/form-data'),
)); ?>

    <p class="note">Dòng có dấu <span class="required">*</span> là bắt buộc.</p>
    <?php echo MyFormat::BindNotifyMsg(); ?>
    
        		
        
    <div class="row">
        <?php echo $form->labelEx($model,'customer_id'); ?>
        <?php echo $form->textField($model,'customer_id'); ?>
        <?php echo $form->error($model,'customer_id'); ?>
    </div>
    <div class="row">
        <?php echo $form->labelEx($model,'username'); ?>
        <?php echo $form->textField($model,'username',array('size'=>50,'maxlength'=>50)); ?>
        <?php echo $form->error($model,'username'); ?>
    </div>
    <div class="row">
        <?php echo $form->labelEx($model,'email'); ?>
        <?php echo $form->textField($model,'email',array('size'=>60,'maxlength'=>80)); ?>
        <?php echo $form->error($model,'email'); ?>
    </div>
    <div class="row">
        <?php echo $form->labelEx($model,'password_hash'); ?>
        <?php echo $form->textArea($model,'password_hash',array('rows'=>6, 'cols'=>50)); ?>
        <?php echo $form->error($model,'password_hash'); ?>
    </div>
    <div class="row">
        <?php echo $form->labelEx($model,'temp_password'); ?>
        <?php echo $form->textArea($model,'temp_password',array('rows'=>6, 'cols'=>50)); ?>
        <?php echo $form->error($model,'temp_password'); ?>
    </div>
    <div class="row">
        <?php echo $form->labelEx($model,'first_name'); ?>
        <?php echo $form->textArea($model,'first_name',array('rows'=>6, 'cols'=>50)); ?>
        <?php echo $form->error($model,'first_name'); ?>
    </div>
    <div class="row">
        <?php echo $form->labelEx($model,'last_name'); ?>
        <?php echo $form->textArea($model,'last_name',array('rows'=>6, 'cols'=>50)); ?>
        <?php echo $form->error($model,'last_name'); ?>
    </div>
    <div class="row">
        <?php echo $form->labelEx($model,'name_agent'); ?>
        <?php echo $form->textField($model,'name_agent',array('size'=>10,'maxlength'=>10)); ?>
        <?php echo $form->error($model,'name_agent'); ?>
    </div>
    <div class="row">
        <?php echo $form->labelEx($model,'code_account'); ?>
        <?php echo $form->textField($model,'code_account',array('size'=>30,'maxlength'=>30)); ?>
        <?php echo $form->error($model,'code_account'); ?>
    </div>
    <div class="row">
        <?php echo $form->labelEx($model,'code_bussiness'); ?>
        <?php echo $form->textField($model,'code_bussiness',array('size'=>30,'maxlength'=>30)); ?>
        <?php echo $form->error($model,'code_bussiness'); ?>
    </div>
    <div class="row">
        <?php echo $form->labelEx($model,'address'); ?>
        <?php echo $form->textArea($model,'address',array('rows'=>6, 'cols'=>50)); ?>
        <?php echo $form->error($model,'address'); ?>
    </div>
    <div class="row">
        <?php echo $form->labelEx($model,'address_vi'); ?>
        <?php echo $form->textField($model,'address_vi',array('size'=>60,'maxlength'=>255)); ?>
        <?php echo $form->error($model,'address_vi'); ?>
    </div>
    <div class="row">
        <?php echo $form->labelEx($model,'house_numbers'); ?>
        <?php echo $form->textArea($model,'house_numbers',array('rows'=>6, 'cols'=>50)); ?>
        <?php echo $form->error($model,'house_numbers'); ?>
    </div>
    <div class="row">
        <?php echo $form->labelEx($model,'province_id'); ?>
        <?php echo $form->textField($model,'province_id'); ?>
        <?php echo $form->error($model,'province_id'); ?>
    </div>
    <div class="row">
        <?php echo $form->labelEx($model,'channel_id'); ?>
        <?php echo $form->textField($model,'channel_id'); ?>
        <?php echo $form->error($model,'channel_id'); ?>
    </div>
    <div class="row">
        <?php echo $form->labelEx($model,'district_id'); ?>
        <?php echo $form->textField($model,'district_id'); ?>
        <?php echo $form->error($model,'district_id'); ?>
    </div>
    <div class="row">
        <?php echo $form->labelEx($model,'ward_id'); ?>
        <?php echo $form->textField($model,'ward_id'); ?>
        <?php echo $form->error($model,'ward_id'); ?>
    </div>
    <div class="row">
        <?php echo $form->labelEx($model,'street_id'); ?>
        <?php echo $form->textField($model,'street_id'); ?>
        <?php echo $form->error($model,'street_id'); ?>
    </div>
    <div class="row">
        <?php echo $form->labelEx($model,'storehouse_id'); ?>
        <?php echo $form->textField($model,'storehouse_id',array('size'=>10,'maxlength'=>10)); ?>
        <?php echo $form->error($model,'storehouse_id'); ?>
    </div>
    <div class="row">
        <?php echo $form->labelEx($model,'sale_id'); ?>
        <?php echo $form->textField($model,'sale_id',array('size'=>11,'maxlength'=>11)); ?>
        <?php echo $form->error($model,'sale_id'); ?>
    </div>
    <div class="row">
        <?php echo $form->labelEx($model,'payment_day'); ?>
        <?php echo $form->textField($model,'payment_day'); ?>
        <?php echo $form->error($model,'payment_day'); ?>
    </div>
    <div class="row">
        <?php echo $form->labelEx($model,'beginning'); ?>
        <?php echo $form->textField($model,'beginning',array('size'=>14,'maxlength'=>14)); ?>
        <?php echo $form->error($model,'beginning'); ?>
    </div>
    <div class="row">
        <?php echo $form->labelEx($model,'first_char'); ?>
        <?php echo $form->textField($model,'first_char'); ?>
        <?php echo $form->error($model,'first_char'); ?>
    </div>
    <div class="row">
        <?php echo $form->labelEx($model,'login_attemp'); ?>
        <?php echo $form->textField($model,'login_attemp'); ?>
        <?php echo $form->error($model,'login_attemp'); ?>
    </div>
    <div class="row">
        <?php echo $form->labelEx($model,'created_date'); ?>
        <?php echo $form->textField($model,'created_date'); ?>
        <?php echo $form->error($model,'created_date'); ?>
    </div>
    <div class="row">
        <?php echo $form->labelEx($model,'created_date_bigint'); ?>
        <?php echo $form->textField($model,'created_date_bigint',array('size'=>12,'maxlength'=>12)); ?>
        <?php echo $form->error($model,'created_date_bigint'); ?>
    </div>
    <div class="row">
        <?php echo $form->labelEx($model,'last_logged_in'); ?>
        <?php echo $form->textField($model,'last_logged_in'); ?>
        <?php echo $form->error($model,'last_logged_in'); ?>
    </div>
    <div class="row">
        <?php echo $form->labelEx($model,'ip_address'); ?>
        <?php echo $form->textField($model,'ip_address',array('size'=>30,'maxlength'=>30)); ?>
        <?php echo $form->error($model,'ip_address'); ?>
    </div>
    <div class="row">
        <?php echo $form->labelEx($model,'role_id'); ?>
        <?php echo $form->textField($model,'role_id'); ?>
        <?php echo $form->error($model,'role_id'); ?>
    </div>
    <div class="row">
        <?php echo $form->labelEx($model,'application_id'); ?>
        <?php echo $form->textField($model,'application_id'); ?>
        <?php echo $form->error($model,'application_id'); ?>
    </div>
    <div class="row">
        <?php echo $form->labelEx($model,'status'); ?>
        <?php echo $form->textField($model,'status'); ?>
        <?php echo $form->error($model,'status'); ?>
    </div>
    <div class="row">
        <?php echo $form->labelEx($model,'gender'); ?>
        <?php echo $form->textField($model,'gender',array('size'=>6,'maxlength'=>6)); ?>
        <?php echo $form->error($model,'gender'); ?>
    </div>
    <div class="row">
        <?php echo $form->labelEx($model,'phone'); ?>
        <?php echo $form->textArea($model,'phone',array('rows'=>6, 'cols'=>50)); ?>
        <?php echo $form->error($model,'phone'); ?>
    </div>
    <div class="row">
        <?php echo $form->labelEx($model,'verify_code'); ?>
        <?php echo $form->textField($model,'verify_code',array('size'=>60,'maxlength'=>100)); ?>
        <?php echo $form->error($model,'verify_code'); ?>
    </div>
    <div class="row">
        <?php echo $form->labelEx($model,'area_code_id'); ?>
        <?php echo $form->textField($model,'area_code_id',array('size'=>11,'maxlength'=>11)); ?>
        <?php echo $form->error($model,'area_code_id'); ?>
    </div>
    <div class="row">
        <?php echo $form->labelEx($model,'parent_id'); ?>
        <?php echo $form->textField($model,'parent_id',array('size'=>11,'maxlength'=>11)); ?>
        <?php echo $form->error($model,'parent_id'); ?>
    </div>
    <div class="row">
        <?php echo $form->labelEx($model,'slug'); ?>
        <?php echo $form->textArea($model,'slug',array('rows'=>6, 'cols'=>50)); ?>
        <?php echo $form->error($model,'slug'); ?>
    </div>
    <div class="row">
        <?php echo $form->labelEx($model,'is_maintain'); ?>
        <?php echo $form->textField($model,'is_maintain'); ?>
        <?php echo $form->error($model,'is_maintain'); ?>
    </div>
    <div class="row">
        <?php echo $form->labelEx($model,'type'); ?>
        <?php echo $form->textField($model,'type'); ?>
        <?php echo $form->error($model,'type'); ?>
    </div>
    <div class="row">
        <?php echo $form->labelEx($model,'address_temp'); ?>
        <?php echo $form->textArea($model,'address_temp',array('rows'=>6, 'cols'=>50)); ?>
        <?php echo $form->error($model,'address_temp'); ?>
    </div>
    <div class="row">
        <?php echo $form->labelEx($model,'last_purchase'); ?>
        <?php echo $form->textField($model,'last_purchase'); ?>
        <?php echo $form->error($model,'last_purchase'); ?>
    </div>
    <div class="row">
        <?php echo $form->labelEx($model,'created_by'); ?>
        <?php echo $form->textField($model,'created_by',array('size'=>11,'maxlength'=>11)); ?>
        <?php echo $form->error($model,'created_by'); ?>
    </div>
    <div class="row">
        <?php echo $form->labelEx($model,'price'); ?>
        <?php echo $form->textField($model,'price',array('size'=>10,'maxlength'=>10)); ?>
        <?php echo $form->error($model,'price'); ?>
    </div>
    <div class="row">
        <?php echo $form->labelEx($model,'price_other'); ?>
        <?php echo $form->textField($model,'price_other',array('size'=>11,'maxlength'=>11)); ?>
        <?php echo $form->error($model,'price_other'); ?>
    </div>
    <div class="row">
        <?php echo $form->labelEx($model,'flag_fix_update'); ?>
        <?php echo $form->textField($model,'flag_fix_update'); ?>
        <?php echo $form->error($model,'flag_fix_update'); ?>
    </div>
	<div class="row buttons" style="padding-left: 141px;">
		        <?php $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType'=>'submit',
            'label'=>$model->isNewRecord ? 'Create' :'Save',
            'type'=>'null', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
            'size'=>'small', // null, 'large', 'small' or 'mini'
            //'htmlOptions' => array('style' => 'margin-bottom: 10px; float: right;'),
        )); ?>	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->
<script>
    $(document).ready(function(){
        $('.form').find('button:submit').click(function(){
            $.blockUI({ overlayCSS: { backgroundColor: '#fff' } }); 
        });
        
    });
</script>