<?php
$this->breadcrumbs=array(
    $this->pluralTitle => array('index'),
    ' ' . $this->singleTitle . ' : ',
);

$menus = array(
	array('label'=>"$this->pluralTitle Management", 'url'=>array('index')),
	array('label'=>"Tạo Mới $this->singleTitle", 'url'=>array('create')),
	array('label'=>"Cập Nhật $this->singleTitle", 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>"Xóa $this->singleTitle", 'url'=>array('delete'), 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Bạn chắc chắn muốn xóa ?')),
);
$this->menu= ControllerActionsName::createMenusRoles($menus, $actions);
?>

<h1>Xem: <?php echo $this->singleTitle; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'customer_id',
		'username',
		'email',
		'password_hash',
		'temp_password',
		'first_name',
		'last_name',
		'name_agent',
		'code_account',
		'code_bussiness',
		'address',
		'address_vi',
		'house_numbers',
		'province_id',
		'channel_id',
		'district_id',
		'ward_id',
		'street_id',
		'storehouse_id',
		'sale_id',
		'payment_day',
		'beginning',
		'first_char',
		'login_attemp',
		'created_date',
		'created_date_bigint',
		'last_logged_in',
		'ip_address',
		'role_id',
		'application_id',
		'status',
		'gender',
		'phone',
		'verify_code',
		'area_code_id',
		'parent_id',
		'slug',
		'is_maintain',
		'type',
		'address_temp',
		'last_purchase',
		'created_by',
		'price',
		'price_other',
		'flag_fix_update',
	),
)); ?>
