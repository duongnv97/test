<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=> GasCheck::getCurl(),
	'method'=>'get',
)); ?>

	<div class="row">
            <?php echo $form->label($model,'created_by'); ?>
            <?php echo $form->hiddenField($model,'created_by'); ?>
            <?php 
                // widget auto complete search user customer and supplier
                $aRoleCreatedBy = [ROLE_EMPLOYEE_MARKET_DEVELOPMENT, ROLE_MONITORING_MARKET_DEVELOPMENT];
                $aData = array(
                    'model'=>$model,
                    'url' => Yii::app()->createAbsoluteUrl('admin/ajax/search_by_role', ['role' => implode(',', $aRoleCreatedBy)]),
                    'field_customer_id'=>'created_by',
                    'field_autocomplete_name'=>'autocompletename3',
                    'name_relation_user'=>'rCreatedBy',
                    'ClassAdd' => 'w-500',
                    'placeholder' => 'Nhập mã nhân viên',
                );
                $this->widget('ext.SpaAutocompleteCustomer.SpaAutocompleteCustomer',
                    array('data'=>$aData));                                        
            ?>
        </div>
    
	<div class="row">
            <?php echo $form->label($model,'area_code_id'); ?>
            <?php echo $form->hiddenField($model,'area_code_id'); ?>
            <?php 
                // widget auto complete search user customer and supplier
                $aData = array(
                    'model'=>$model,
                    'field_customer_id'=>'area_code_id',
                    'field_autocomplete_name'=>'autocompletename2',
                    'name_relation_user'=>'rAgent',
                    'url' => Yii::app()->createAbsoluteUrl('admin/ajax/search_by_role',['role'=>ROLE_AGENT]),
                    'ClassAdd' => 'w-500',
                    'placeholder' => 'Nhập mã hoặc tên đại lý',
                );
                $this->widget('ext.SpaAutocompleteCustomer.SpaAutocompleteCustomer',
                    array('data'=>$aData));                                        
            ?>
        </div>
    
        <div class="row">
            <?php  echo $form->labelEx($model,'temp_password'); ?>
            <?php  echo $form->textField($model,'temp_password', ['class'=>'w-200']); ?>
        </div>
    
        <div class="row">
            <?php  echo $form->labelEx($model,'username'); ?>
            <?php  echo $form->textField($model,'username', ['class'=>'w-200']); ?>
        </div>
    
    <div class="row more_col">
        <div class="col1">
                <?php echo Yii::t('translation', $form->label($model,'date_from', array('label' => "Ngày tạo từ" ))); ?>
                <?php 
                    $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                        'model'=>$model,        
                        'attribute'=>'date_from',
                        'options'=>array(
                            'showAnim'=>'fold',
                            'dateFormat'=> MyFormat::$dateFormatSearch,
//                            'minDate'=> '0',
//                            'maxDate'=> '0',
                            'changeMonth' => true,
                            'changeYear' => true,
                            'showOn' => 'button',
                            'buttonImage'=> Yii::app()->theme->baseUrl.'/admin/images/icon_calendar_r.gif',
                            'buttonImageOnly'=> true,                                
                        ),        
                        'htmlOptions'=>array(
                            'class'=>'w-16',
                            'size'=>'16',
                            'style'=>'float:left;',
                        ),
                    ));
                ?>     		
        </div>
        <div class="col2">
                <?php echo Yii::t('translation', $form->label($model,'date_to', array('label' => "Đến ngày" ))); ?>
                <?php 
                    $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                        'model'=>$model,        
                        'attribute'=>'date_to',
                        'options'=>array(
                            'showAnim'=>'fold',
                            'dateFormat'=> MyFormat::$dateFormatSearch,
//                            'minDate'=> '0',
//                            'maxDate'=> '0',
                            'changeMonth' => true,
                            'changeYear' => true,
                            'showOn' => 'button',
                            'buttonImage'=> Yii::app()->theme->baseUrl.'/admin/images/icon_calendar_r.gif',
                            'buttonImageOnly'=> true,                                
                        ),        
                        'htmlOptions'=>array(
                            'class'=>'w-16',
                            'size'=>'16',
                            'style'=>'float:left;',
                        ),
                    ));
                ?>     		
        </div>
    </div>
    
        <div class="row">
            <?php  echo $form->labelEx($model, 'first_char', ['label' => 'Trùng điểm làm việc']); ?>
            <?php  echo $form->checkbox($model,'first_char', []); ?>
        </div>

	<div class="row buttons" style="padding-left: 159px;">
        <?php $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType'=>'submit',
            'label'=>'Search',
            'type'=>'null', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
            'size'=>'small', // null, 'large', 'small' or 'mini'
            //'htmlOptions' => array('style' => 'margin-bottom: 10px; float: right;'),
        )); ?>	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->