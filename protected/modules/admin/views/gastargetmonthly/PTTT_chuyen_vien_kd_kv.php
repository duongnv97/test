<div class="clr"></div> 
<h1>PTTT - Chuyên viên kinh doanh khu vực</h1>

<?php 
$aNvPTTT = Users::getArrUserByRoleForSetUpTarget(ROLE_EMPLOYEE_MARKET_DEVELOPMENT, array('target_chuyenvien_kd_khuvuc' =>1, 'order'=>'t.gender ASC, t.code_bussiness ASC'));
?>
<?php foreach ($aNvPTTT as $key=>$item): ?>
    <div class="row setup_target" style="">
        <label><?php echo $item;?></label>
        <div class="fix_number_help">
            <?php if(isset($aSalePercent[$key]['target']) && $aSalePercent[$key]['target']): ?>
                <?php echo $form->textField($model,'target[]',array('value'=>  ActiveRecord::formatNumberInput($aSalePercent[$key]['target']),'class'=>"number_only",'size'=>3,'maxlength'=>16)); ?>
            <?php else: ?>
                <?php echo $form->textField($model,'target[]',array('class'=>"number_only",'size'=>3,'maxlength'=>16)); ?>
            <?php endif; ?>
            <div class="help_number"></div>
            <?php echo $form->hiddenField($model,'sale_id[]',array('value'=>$key)); ?>
        </div>
    </div>
<?php endforeach; ?>    
<div class="clr"></div>