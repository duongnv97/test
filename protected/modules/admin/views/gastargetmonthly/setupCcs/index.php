<?php
$this->breadcrumbs=array(
    $this->pageTitle,
);
?>

<h1><?php echo $this->pageTitle;?></h1>
<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'gas-target-monthly-form',
	'enableAjaxValidation'=>false,
)); ?>
<div class="search-form" style="">
<?php $this->renderPartial('setupCcs/_search',array(
	'model'=>$model,
        'form'=>$form,
)); ?>
</div><!-- search-form --> 
<?php if(!empty($aData)): ?>
<?php 
$MONITOR        = isset($aData['MONITOR']) ? $aData['MONITOR'] : [];
$USER           = isset($aData['USER']) ? $aData['USER'] : [];
$TARGET         = isset($aData['TARGET']) ? $aData['TARGET'] : [];
$mGasOneMany    = new GasOneMany();
$aMonitor       = $mGasOneMany->getArrayMonitorOfGdkv($model->gdkv_id, $model->date_from_ymd);
$totalMonitor = count($aMonitor);
$model->mapLabel();
?>
<div class="form">
    <?php foreach ($MONITOR as $monitor_id=>$aUser): ?>
    <?php
        $display_none = '';
        if($model->type == GasTargetMonthly::TYPE_GDKV && !empty($model->gdkv_id) && $monitor_id != $model->gdkv_id){
            $display_none = 'display_none';
        }elseif($model->type != GasTargetMonthly::TYPE_GDKV && $totalMonitor && !in_array($monitor_id, $aMonitor)){
            $display_none = 'display_none';
        }
    ?>
    <div class="row monitorGroup <?php echo $display_none;?>">
    <h1><?php echo isset($USER[$monitor_id]) ? '<p class = "display_none">'.$monitor_id.'<p>'.$USER[$monitor_id]->first_name : ''; ?></h1>
    <div class="row setup_target" style="">
        <fieldset class="box_310" style="">
            <legend class="item_b">Thiết lập cho tất cả</legend>
            <label><?php echo $model->lb1;?></label>
            <div class="fix_number_help">
                <input value="" data-bind="target_point" class="number_only agent_change setupAll" size="3" maxlength="16" type="text">
                <!--<div class="help_number"></div>-->
            </div>
            <label><?php echo $model->lb2;?></label>
            <div class="fix_number_help">
                <input value="" data-bind="target_app" class="number_only agent_change setupAll" size="3" maxlength="16" type="text">
                <!--<div class="help_number"></div>-->
            </div>
            <?php if($model->showTargetBqv()): ?>
                <label><?php echo $model->lb3;?></label>
                <div class="fix_number_help">
                    <input value="" data-bind="target_bqv" class="number_only agent_change setupAll" size="3" maxlength="16" type="text">
                    <!--<div class="help_number"></div>-->
                </div>
            <?php endif; ?>
            
        </fieldset>
    </div>
    <div class="clr"></div> 
        <?php foreach ($aUser as $user_id => $id): ?>
            <?php
                $labelCcs = $firstName = '';
                if(isset($USER[$user_id])){
                    $firstName =  $USER[$user_id]->first_name;
                    if($USER[$user_id]->gender == Users::SALE_MOI_PTTT){
                        $firstName = "PTTT - $firstName";
                    }elseif($USER[$user_id]->gender == Users::SALE_PTTT_KD_KHU_VUC){
                        $firstName = "CCS - $firstName";
                    }
                }
            ?>
            <div class="row setup_target" style="">
                <fieldset class="box_310" style="">
                    <?php echo $form->hiddenField($model,'sale_id[]',array('value'=>$user_id)); ?>
                    <legend class="item_b"><?php echo $firstName; ?></legend>
                    <label><?php echo $model->lb1;?></label>
                    <div class="fix_number_help">
                        <?php echo $form->textField($model,'target_point[]',array('value'=> isset($TARGET[$user_id]['target_point']) ? $TARGET[$user_id]['target_point'] : '','class'=>"number_only agent_change target_point",'size'=>3,'maxlength'=>16)); ?>
                        <!--<div class="help_number"></div>-->
                    </div>
                    <label><?php echo $model->lb2;?></label>
                    <div class="fix_number_help">
                        <?php echo $form->textField($model,'target_app[]',array('value'=>isset($TARGET[$user_id]['target_app']) ? $TARGET[$user_id]['target_app'] : '','class'=>"number_only agent_change target_app",'size'=>3,'maxlength'=>16)); ?>
                        <!--<div class="help_number"></div>-->
                    </div>
                    
                    <?php if($model->showTargetBqv()): ?>
                    <label><?php echo $model->lb3;?></label>
                    <div class="fix_number_help">
                        <?php echo $form->textField($model,'target_bqv[]',array('value'=>isset($TARGET[$user_id]['target_bqv']) ? $TARGET[$user_id]['target_bqv'] : '','class'=>"number_only agent_change target_bqv",'size'=>3,'maxlength'=>16)); ?>
                        <!--<div class="help_number"></div>-->
                    </div>
                    <?php endif; ?>
                </fieldset>
            </div>
        <?php endforeach; ?>
    </div>
    <div class="clr"></div> 
    <?php endforeach; ?> 
    <div class="clr"></div>
    <div class="row buttons" style="padding-left: 115px;">
    <?php $this->widget('bootstrap.widgets.TbButton', array(
        'buttonType'=>'submit',
        'label'=> 'Lưu lại',
        'type'=>'null', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
        'size'=>'small', // null, 'large', 'small' or 'mini'
        //'htmlOptions' => array('style' => 'margin-bottom: 10px; float: right;'),
    )); ?>	</div>  
</div>
<?php endif; ?>
<?php $this->endWidget(); ?>

<style>
    .setup_target {float:left;width:330px; }
</style>

<script>
$(document).ready(function(){
    $('.form').find('button:submit').click(function(){        
        $.blockUI({ overlayCSS: { backgroundColor: '#fff' } }); 
    });
    onChangeSetUpAll();
});

function onChangeSetUpAll(){
    $('.setupAll').on('input',function(){
       value = $(this).val();
       className = '.' + $(this).data('bind');
       $(this).closest('.monitorGroup').find(className).val(value);
    });
}
</script>    
