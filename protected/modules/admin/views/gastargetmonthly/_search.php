<div class="wide form">
	<div class="row">
                <?php echo $form->label($model,'monthly',array('style' => ''));?>
                <?php // $model->monthly = 13; // luôn setup cho target cho 1 năm, nên là tháng sẽ là 13?>
                <?php // echo $form->textField($model,'monthly'); ?>
		<?php echo $form->dropDownList($model,'monthly', ActiveRecord::getMonthVn(),array('style' => 'width:150px;')); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'type'); ?>
		<?php echo $form->dropDownList($model,'type', CmsFormatter::$CUSTOMER_BO_MOI,array('style' => 'width:150px;')); ?>
	</div>
    
	<div class="row">
		<?php echo $form->label($model,'year'); ?>
		<?php echo $form->dropDownList($model,'year', ActiveRecord::getRangeYear(),array('style' => 'width:150px;')); ?>
	</div>
        <input type="hidden" name="search_button_click" value="" class="search_button_click">
	<div class="row buttons" style="padding-left: 159px;">
            <?php $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType'=>'submit',
            'label'=>'Tìm Kiếm',
            'type'=>'null', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
            'size'=>'small', // null, 'large', 'small' or 'mini'
            'htmlOptions' => array('class' => 'search_button'),
        )); ?>	</div>
</div><!-- search-form -->
<script>
    $(document).ready(function(){
        $('.search_button').click(function(){
            $('.search_button_click').val(1);
//            $('.form').find('select').attr('disabled',true);
        });
    })
</script>