<?php
$this->breadcrumbs=array(
	Yii::t('translation','Gas Target Monthlies')=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	Yii::t('translation','Update'),
);

$menus = array(	
        array('label'=> Yii::t('translation', 'GasTargetMonthly Management'), 'url'=>array('index')),
	array('label'=> Yii::t('translation', 'View GasTargetMonthly'), 'url'=>array('view', 'id'=>$model->id)),
	array('label'=> Yii::t('translation', 'Create GasTargetMonthly'), 'url'=>array('create')),	
);
$this->menu= ControllerActionsName::createMenusRoles($menus, $actions);

?>

<h1><?php echo Yii::t('translation', 'Update GasTargetMonthly '.$model->id); ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>