<?php
$MONITOR            = isset($aData['MONITOR']) ? $aData['MONITOR'] : [];
$MONITOR_GDKV       = isset($aData['MONITOR_GDKV']) ? $aData['MONITOR_GDKV'] : [];
$USER               = isset($aData['USER']) ? $aData['USER'] : [];
$TARGET_DAILY       = isset($aData['TARGET_DAILY']) ? $aData['TARGET_DAILY'] : [];
$TARGET_BQV         = isset($aData['TARGET_BQV']) ? $aData['TARGET_BQV'] : [];
$TARGET_BQV_SHARE       = isset($aData['TARGET_SHARE']) ? $aData['TARGET_SHARE'] : [];
$TARGET_BQV_V2      = isset($aData['TARGET_BQV_V2']) ? $aData['TARGET_BQV_V2'] : [];
$TARGET_SETUP       = isset($aData['TARGET_SETUP']) ? $aData['TARGET_SETUP'] : [];
$TARGET_SETUP_GSKV  = isset($aData['TARGET_SETUP_GSKV']) ? $aData['TARGET_SETUP_GSKV'] : [];
$IS_APPLY_GSKV       = isset($aData['IS_APPLY_GSKV']) ? $aData['IS_APPLY_GSKV'] : [];
$mGasTargetMonthlyCal = new GasTargetMonthly();
$mGasTargetMonthlyCal->date_from = $model->date_from;
?>
<div id="tabs-2">
<br>
<?php if(!empty($MONITOR_GDKV)): ?>
<h1>Thống Kê - Giám Sát</h1>
<table class="tb hm_table statistic_gskv_target" style="border-collapse: collapse; table-layout: fixed;font-size: 10px;">
    <thead style="background: #f1f1f1;font-size: 10px;">
        <tr>
        <th class="item_b item_c w-50" rowspan="2">STT</th>
            <th class="item_b item_c w-55" rowspan="2">Tên nhân viên</th>
            <th class="item_b item_c w-40" rowspan="2">Loại NV</th>
            <th class="item_b item_c" colspan="2">Target</th>
            <th class="item_b item_c" colspan="4">Thực tế</th>
            <th class="item_b item_c" colspan="2">Tỷ lệ</th>
            <th class="item_b item_c w-50" rowspan="2">Tỷ Lệ Thưởng</th>
            <th class="item_b item_c w-100" rowspan="2">Doanh thu 1</th>
            <th class="item_b item_c w-100" rowspan="2">Doanh thu 2</th>
            <th class="item_b item_c w-100" rowspan="2">Tiền vốn 1</th>
            <th class="item_b item_c w-100" rowspan="2">Tiền vốn 2</th>
            <th class="item_b item_c w-100" rowspan="2">Lợi nhuận 1</th>
            <th class="item_b item_c w-100" rowspan="2">Lợi nhuận 2</th>
            <th class="item_b item_c w-100" rowspan="2">Tổng lợi nhuận</th>
            <th class="item_b item_c w-100" rowspan="2">Thưởng</th>
        </tr>
        <tr>
            <th class="item_b item_c w-60">DLV/ App</th>
            <th class="item_b item_c w-60">BQV (kg)</th>
            <th class="item_b item_c w-60">DLV/ App</th>
            <th class="item_b item_c w-60">BQV Lần 1 (kg)</th>
            <th class="item_b item_c w-60">BQV Lần 2 (kg)</th>
            <th class="item_b item_c w-60">BQV Chia Sẽ (kg)</th>
            <th class="item_b item_c w-60">DLV/ App</th>
            <th class="item_b item_c w-60">BQV</th>
        </tr>
    </thead>
    <tbody>
        
    </tbody>
</table>
<?php foreach ($MONITOR_GDKV as $monitor_gdkv_id => $aCcsId): ?>
<?php 
    $point_sum_gdkv = 0;
    $point_current_sum_gdkv =  0;
    $app_sum_gdkv = 0;
    $app_current_sum_gdkv = 0;
    $bqv_sum_gdkv = 0;
    $bqv_current_sum_gdkv = 0;
    $bqv_current_sum_gdkv_v2 = 0;
    $sum_gdkv_total = 0;
    $sum_gdkv_total_share = 0;
    $sum_gdkv_total_v2 = 0;
    $sum_gdkv_fund = 0;
    $sum_gdkv_fund_v2 = 0;
    $sum_gdkv_revenue = 0;
    $sum_gdkv_revenue_v2 = 0;
    $sum_gdkv_revenue_final = 0;
    $sum_gdkv_remain = 0;
    $sum_gdkv_remain_v2 = 0;
    $bqv_current_sum_gdkv_share = 0;
    $sum_gdkv_remain_share = 0;
?>
<div class="gdkvGroup">
<h1><?php echo isset($USER[$monitor_gdkv_id]) ? $USER[$monitor_gdkv_id]->first_name : ''; ?> </h1>
<table class="tb hm_table float_tb" style="border-collapse: collapse; table-layout: fixed;font-size: 10px;">
    <thead style="background: #f1f1f1;font-size: 10px;">
        <tr>
        <th class="item_b item_c w-50" rowspan="2">STT</th>
            <th class="item_b item_c w-55" rowspan="2">Tên nhân viên</th>
            <th class="item_b item_c w-40" rowspan="2">Loại NV</th>
            <th class="item_b item_c" colspan="2">Target</th>
            <th class="item_b item_c" colspan="4">Thực tế</th>
            <th class="item_b item_c" colspan="2">Tỷ lệ</th>
            <th class="item_b item_c w-50" rowspan="2">Tỷ Lệ Thưởng</th>
            <th class="item_b item_c w-100" rowspan="2">Doanh thu 1</th>
            <th class="item_b item_c w-100" rowspan="2">Doanh thu 2</th>
            <th class="item_b item_c w-100" rowspan="2">Tiền vốn 1</th>
            <th class="item_b item_c w-100" rowspan="2">Tiền vốn 2</th>
            <th class="item_b item_c w-100" rowspan="2">Lợi nhuận 1</th>
            <th class="item_b item_c w-100" rowspan="2">Lợi nhuận 2</th>
            <th class="item_b item_c w-100" rowspan="2">Tổng lợi nhuận</th>
            <th class="item_b item_c w-100" rowspan="2">Thưởng</th>
        </tr>
        <tr>
            <th class="item_b item_c w-60">DLV/ App</th>
            <th class="item_b item_c w-60">BQV (kg)</th>
            <th class="item_b item_c w-60">DLV/ App</th>
            <th class="item_b item_c w-60">BQV Lần 1 (kg)</th>
            <th class="item_b item_c w-60">BQV Lần 2 (kg)</th>
            <th class="item_b item_c w-60">BQV Chia Sẽ (kg)</th>
            <th class="item_b item_c w-60">DLV/ App</th>
            <th class="item_b item_c w-60">BQV</th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($aCcsId as $monitor_id => $monitor_id_v2): ?>
        <?php 
            if(empty($MONITOR[$monitor_id])){
                continue;
            }
            $aEmployeeId = $MONITOR[$monitor_id];
            $point_sum = 0;
            $point_current_sum =  0;
            $app_sum = 0;
            $app_current_sum = 0;
            $bqv_sum = 0;
            $bqv_current_sum = 0;
            $bqv_current_sum_v2 = 0;
            $sum_total  = 0;
            $sum_total_share  = 0;
            $sum_total_v2  = 0;
            $sum_fund  = 0;
            $sum_fund_v2  = 0;
            $sum_revenue  = 0;
            $sum_revenue_v2  = 0;
            $sum_revenue_final  = 0;
            $sum_remain = 0;
            $sum_remain_v2 = 0;
            $bqv_current_sum_share = 0;
            $sum_remain_share = 0;
            $bqv_gskv = $bqv_gskv_v2 = $bqv_gskv_share = $gskv_total
                    = $gskv_total_share = $gskv_total_v2 = $gskv_fund_v2 
                    = $gskv_fund = $gskv_revenue_v2 = $gskv_revenue = 0;
        ?>
        <tr>
            <td class="item_c item_b" colspan="20"><?php echo isset($USER[$monitor_id]) ? $USER[$monitor_id]->first_name : ''; ?></td>
        </tr>
            <?php $index = 1;?>
            <?php foreach ($aEmployeeId as $user_id => $id): ?>
            <?php
//            gas remain
            $current_remain  = !empty($TARGET_BQV['GAS_REMAIN'][$user_id]) ? $TARGET_BQV['GAS_REMAIN'][$user_id] : 0;
            $sum_remain      +=   $current_remain;
            $sum_gdkv_remain +=  $current_remain;
//            gas remain
            $current_remain_v2  = !empty($TARGET_BQV_V2['GAS_REMAIN'][$user_id]) ? $TARGET_BQV_V2['GAS_REMAIN'][$user_id] : 0;
            $sum_remain_v2      +=   $current_remain_v2;
            $sum_gdkv_remain_v2 +=  $current_remain_v2;
//            gas remain
            $current_remain_share  = !empty($TARGET_BQV_SHARE['GAS_REMAIN'][$user_id]) ? $TARGET_BQV_SHARE['GAS_REMAIN'][$user_id] : 0;
            $sum_remain_share      +=   $current_remain_share;
            $sum_gdkv_remain_share +=  $current_remain_share;
            
            $point = isset($TARGET_SETUP[$user_id]['point']) ? $TARGET_SETUP[$user_id]['point'] : 0;
            $point_current = isset($TARGET_DAILY[$user_id]['point']) ? $TARGET_DAILY[$user_id]['point'] : 0;
            $app = isset($TARGET_SETUP[$user_id]['app']) ? $TARGET_SETUP[$user_id]['app'] : 0;
            $app_current = isset($TARGET_DAILY[$user_id]['app']) ? $TARGET_DAILY[$user_id]['app'] : 0;
            $bqv = isset($TARGET_SETUP[$user_id]['bqv']) ? $TARGET_SETUP[$user_id]['bqv'] : 0;
            $bqv_current = isset($TARGET_BQV[$user_id]['bqv']) ? $TARGET_BQV[$user_id]['bqv'] : 0;
            $bqv_current = $mGasTargetMonthlyCal->getKgBQV($bqv_current,$current_remain);
            $bqv_current_v2 = isset($TARGET_BQV_V2[$user_id]['bqv']) ? $TARGET_BQV_V2[$user_id]['bqv'] : 0;
            $bqv_current_v2 = $mGasTargetMonthlyCal->getKgBQV($bqv_current_v2,$current_remain_v2);
            $bqv_current_share = isset($TARGET_BQV_SHARE[$user_id]['bqv']) ? $TARGET_BQV_SHARE[$user_id]['bqv'] : 0;
            $bqv_current_share = $mGasTargetMonthlyCal->getKgBQV($bqv_current_share,$current_remain_share);
        //    sum
            $point_sum += $point;
            $point_current_sum +=  $point_current;
            $app_sum += $app;
            $app_current_sum += $app_current;
            $bqv_sum += $bqv;
            $bqv_current_sum += $bqv_current;
            $bqv_current_sum_v2 += $bqv_current_v2;
            $bqv_current_sum_share += $bqv_current_share;
        //    sum gdkv
            $point_sum_gdkv += $point;
            $point_current_sum_gdkv +=  $point_current;
            $app_sum_gdkv += $app;
            $app_current_sum_gdkv += $app_current;
            $bqv_sum_gdkv += $bqv;
            $bqv_current_sum_gdkv += $bqv_current;
            $bqv_current_sum_gdkv_v2 += $bqv_current_v2;
            $bqv_current_sum_gdkv_share += $bqv_current_share;
//            doanh thu
            $current_total  = !empty($TARGET_BQV['GRAND_TOTAL'][$user_id]) ? $TARGET_BQV['GRAND_TOTAL'][$user_id] : 0;
            $sum_total      +=   $current_total;
            $sum_gdkv_total +=  $current_total;
            
            $current_total_share  = !empty($TARGET_BQV_SHARE['GRAND_TOTAL'][$user_id]) ? $TARGET_BQV_SHARE['GRAND_TOTAL'][$user_id] : 0;
            $sum_gdkv_total_share +=  $current_total_share;
            $sum_total_share      +=   $current_total_share;
            
            $current_total_v2  = !empty($TARGET_BQV_V2['GRAND_TOTAL'][$user_id]) ? $TARGET_BQV_V2['GRAND_TOTAL'][$user_id] : 0;
            $sum_gdkv_total_v2 +=  $current_total_v2;
            $sum_total_v2      +=   $current_total_v2;
            
            $current_fund  = !empty($TARGET_BQV['GAS_FUNDS'][$user_id]) ? (int)$TARGET_BQV['GAS_FUNDS'][$user_id] : 0;
            $current_fund_share  = !empty($TARGET_BQV_SHARE['GAS_FUNDS'][$user_id]) ? (int)$TARGET_BQV_SHARE['GAS_FUNDS'][$user_id] : 0;
            $sum_gdkv_fund +=  $current_fund + $current_fund_share;
            $sum_fund      +=   $current_fund + $current_fund_share;
            
            $current_fund_v2  = !empty($TARGET_BQV_V2['GAS_FUNDS'][$user_id]) ? (int)$TARGET_BQV_V2['GAS_FUNDS'][$user_id] : 0;
            $sum_gdkv_fund_v2 +=  $current_fund_v2;
            $sum_fund_v2      +=   $current_fund_v2;
            
            $current_revenue  = $current_total + $current_total_share - $current_fund - $current_fund_share;
            $sum_gdkv_revenue +=  $current_revenue;
            $sum_revenue      +=   $current_revenue;
            
            $current_revenue_v2  = $current_total_v2 - $current_fund_v2;
            $current_revenue_v2  = $current_revenue_v2 / 2;// Apr 23, 2019 bỏ 50% lợi nhuận BQV 2
            $sum_gdkv_revenue_v2 +=  $current_revenue_v2;
            $sum_revenue_v2      +=   $current_revenue_v2;            
            
            $style = 'style = "background: none repeat scroll 0 0 #D8E2F3;"';
            if(in_array($user_id, $IS_APPLY_GSKV)){
                $style = '';
            }
            ?>
            <tr <?php echo $style;?>>
                <td class="item_c w-50"><?php echo $index++;?></td>
                <td class="item_l w-55"><?php echo isset($USER[$user_id]) ? $USER[$user_id]->first_name : ''; ?></td>
                <td class="item_l w-40">
                    <?php 
                    $firstName = 'PTTT';
                    if(isset($USER[$user_id])){
                        if($USER[$user_id]->gender == Users::SALE_MOI_PTTT){
                            $firstName = 'PTTT';
                        }elseif($USER[$user_id]->gender == Users::SALE_PTTT_KD_KHU_VUC){
                            $firstName = 'CCS';
                        }
                    }
                    echo $firstName;
                    ?>
                </td>
                <td class="item_c w-60"><?php echo ActiveRecord::formatCurrency($point + $app);?></td>
                <td class="item_c w-60"><?php echo ActiveRecord::formatCurrency($bqv);?></td>
                <td class="item_c w-60"><?php echo ActiveRecord::formatCurrency($point_current + $app_current);?></td>
                <td class="item_c w-60"><?php echo ActiveRecord::formatCurrency($bqv_current);?></td>
                <td class="item_c w-60"><?php echo ActiveRecord::formatCurrency($bqv_current_v2);?></td>
                <td class="item_c w-60"><?php echo ActiveRecord::formatCurrency($bqv_current_share);?></td>
                <td class="item_c w-60"><?php echo $mGasTargetMonthlyCal->getRateBQV([$point_current,$app_current],[$point,$app]);?> %</td>
                <?php 
                    $moreCurrent = 0;
                    if($mGasTargetMonthlyCal->isApplyShare()){
                        $moreCurrent = $bqv_current_share;
                    }
                ?>
                <td class="item_c w-60"><?php echo $mGasTargetMonthlyCal->getRateBQV([$moreCurrent,$bqv_current,$bqv_current_v2],[$bqv]);?>  %</td>
                <?php 
                $sApp   = $mGasTargetMonthlyCal->getRateBQV([$point_current,$app_current],[$point,$app]);
                $sBQV   = $mGasTargetMonthlyCal->getRateBQV([$moreCurrent,$bqv_current,$bqv_current_v2],[$bqv]);
                $sBonus = $mGasTargetMonthlyCal->getRateBonus($sApp,$sBQV);
                
                $revenue                = (($sBonus >= GasTargetMonthly::PRTCENT_BONUS_PTTT) &&($sBonus/100) * ($current_revenue + $current_revenue_v2) > 0) ? (int)(($sBonus/100) * ($current_revenue + $current_revenue_v2)) : 0 ;
                $sum_gdkv_revenue_final +=  $revenue;
                $sum_revenue_final      +=  $revenue;
                
                ?>
                <td class="item_c w-50"><?php echo $sBonus;?>  %</td>
                <td class="item_r w-100"><?php echo ActiveRecord::formatCurrency($current_total + $current_total_share);?></td>
                <td class="item_r w-100"><?php echo ActiveRecord::formatCurrency($current_total_v2);?></td>
                <td class="item_r w-100"><?php echo ActiveRecord::formatCurrency($current_fund + $current_fund_share);?></td>
                <td class="item_r w-100"><?php echo ActiveRecord::formatCurrency($current_fund_v2);?></td>
                <td class="item_r w-100"><?php echo ActiveRecord::formatCurrency($current_revenue);?></td>
                <td class="item_r w-100"><?php echo ActiveRecord::formatCurrency($current_revenue_v2);?></td>
                <td class="item_r w-100"><?php echo ActiveRecord::formatCurrency($current_revenue + $current_revenue_v2);?></td>
                <td class="item_r w-100"><?php echo ActiveRecord::formatCurrency($revenue);?></td>
            </tr>
            <?php 
            if(in_array($user_id, $IS_APPLY_GSKV) || $sBonus >= GasTargetMonthly::PRTCENT_BONUS_PTTT){
                $bqv_gskv       += $bqv_current;
                $bqv_gskv_v2    += $bqv_current_v2;
                $bqv_gskv_share += $bqv_current_share;
		$gskv_total     += $current_total;
                $gskv_total_share += $current_total_share;
                $gskv_total_v2   +=  $current_total_v2;
                $gskv_fund_v2+= $current_fund_v2;
                $gskv_fund   += $current_fund + $current_fund_share;
                $gskv_revenue_v2+= $current_revenue_v2;            
                $gskv_revenue   += $current_revenue;
            }
            ?>
            <?php endforeach;?>
            <tr>
                <td class="item_c item_b w-140" colspan="3">Tổng</td>
                <td class="item_c item_b w-60"><?php echo ActiveRecord::formatCurrency($point_sum+$app_sum);?></td>
                <td class="item_c item_b w-60"><?php echo ActiveRecord::formatCurrency($bqv_sum);?></td>
                <td class="item_c item_b w-60"><?php echo ActiveRecord::formatCurrency($point_current_sum+$app_current_sum);?></td>
                <td class="item_c item_b w-60"><?php echo ActiveRecord::formatCurrency($bqv_current_sum);?></td>
                <td class="item_c item_b w-60"><?php echo ActiveRecord::formatCurrency($bqv_current_sum_v2);?></td>
                <td class="item_c item_b w-60"><?php echo ActiveRecord::formatCurrency($bqv_current_sum_share);?></td>
                <td class="item_c item_b w-60"><?php echo $mGasTargetMonthlyCal->getRateBQV([$point_current_sum,$app_current_sum],[$point_sum,$app_sum]);?> %</td>
                <?php 
                    $moreCurrent = 0;
                    if($mGasTargetMonthlyCal->isApplyShare()){
                        $moreCurrent = $bqv_current_sum_share;
                    }
                ?>
                <td class="item_c item_b w-60"><?php echo $mGasTargetMonthlyCal->getRateBQV([$moreCurrent,$bqv_current_sum,$bqv_current_sum_v2],[$bqv_sum]);?>  %</td>
                <?php 
                $sApp   = $mGasTargetMonthlyCal->getRateBQV([$point_current_sum,$app_current_sum],[$point_sum,$app_sum]);
                $sBQV   = $mGasTargetMonthlyCal->getRateBQV([$moreCurrent,$bqv_current_sum,$bqv_current_sum_v2],[$bqv_sum]);
                $sBonus = $mGasTargetMonthlyCal->getRateBonus($sApp,$sBQV);
                ?>
                <td class="item_c item_b w-50"><?php echo $sBonus;?>  %</td>
                <td class="item_r item_b w-100"><?php echo ActiveRecord::formatCurrency($sum_total + $sum_total_share);?></td>
                <td class="item_r item_b w-100"><?php echo ActiveRecord::formatCurrency($sum_total_v2);?></td>
                <td class="item_r item_b w-100"><?php echo ActiveRecord::formatCurrency($sum_fund);?></td>
                <td class="item_r item_b w-100"><?php echo ActiveRecord::formatCurrency($sum_fund_v2);?></td>
                <td class="item_r item_b w-100"><?php echo ActiveRecord::formatCurrency($sum_revenue);?></td>
                <td class="item_r item_b w-100"><?php echo ActiveRecord::formatCurrency($sum_revenue_v2);?></td>
                <td class="item_r item_b w-100"><?php echo ActiveRecord::formatCurrency($sum_revenue+$sum_revenue_v2);?></td>
                <td class="item_r item_b w-100"><?php echo ActiveRecord::formatCurrency($sum_revenue_final);?></td>
            </tr>
            <tr class="statistic_gskv">
                <?php 
                $target_gskv    = isset($TARGET_SETUP_GSKV[$monitor_id]['app']) ? $TARGET_SETUP_GSKV[$monitor_id]['app'] : 0;
//                $target_gskv    += isset($TARGET_SETUP[$monitor_id]['bqv']) ? $TARGET_SETUP[$monitor_id]['bqv'] : 0;
                $moreCurrent = 0;
                if($mGasTargetMonthlyCal->isApplyShare()){
                    $moreCurrent = $bqv_gskv_share;
                }
                $sBonus         = $mGasTargetMonthlyCal->getRateBQV([$moreCurrent,$bqv_gskv,$bqv_gskv_v2],[$target_gskv]);
                $gskv_allTotal  = $gskv_revenue + $gskv_revenue_v2;
                $revenue_gskv   = ($sBonus >= GasTargetMonthly::PERCENT_BONUS_GS ) ? 0.2 * $gskv_allTotal * ($sBonus / 100) : 0;
                ?>
                <td class="item_c item_b w-140" colspan="3"><?php echo isset($USER[$monitor_id]) ? $USER[$monitor_id]->first_name : ''; ?></td>
                <td class="item_c item_b w-60"></td>
                <td class="item_c item_b w-60"><?php echo ActiveRecord::formatCurrency($target_gskv);?></td>
                <td class="item_c item_b w-60"></td>
                <td class="item_c item_b w-60"><?php echo ActiveRecord::formatCurrency($bqv_gskv);?></td>
                <td class="item_c item_b w-60"><?php echo ActiveRecord::formatCurrency($bqv_gskv_v2);?></td>
                <td class="item_c item_b w-60"><?php echo ActiveRecord::formatCurrency($bqv_gskv_share);?></td>
                <td class="item_c item_b w-50" colspan="3"><?php echo $sBonus;?>  %</td>
                <td class="item_r item_b w-100"><?php echo ActiveRecord::formatCurrency((int)$gskv_total + (int)$gskv_total_share);?></td>
                <td class="item_r item_b w-100"><?php echo ActiveRecord::formatCurrency((int)$gskv_total_v2);?></td>
                <td class="item_r item_b w-100"><?php echo ActiveRecord::formatCurrency((int)$gskv_fund);?></td>
                <td class="item_r item_b w-100"><?php echo ActiveRecord::formatCurrency((int)$gskv_fund_v2);?></td>
                <td class="item_r item_b w-100"><?php echo ActiveRecord::formatCurrency((int)$gskv_revenue);?></td>
                <td class="item_r item_b w-100"><?php echo ActiveRecord::formatCurrency((int)$gskv_revenue_v2);?></td>
                <td class="item_r item_b w-100"><?php echo ActiveRecord::formatCurrency((int)$gskv_allTotal);?></td>
                <td class="item_r item_b w-100"><?php echo ActiveRecord::formatCurrency((int)$revenue_gskv);?></td>
            </tr>
        <?php endforeach;?>
        <tr>
            <td class="item_c item_b w-130" colspan="3" style="color:#ad0000">Tổng - <?php echo isset($USER[$monitor_gdkv_id]) ? $USER[$monitor_gdkv_id]->first_name : ''; ?></td>
            <td class="item_c item_b w-60" style="color:#ad0000"><?php echo ActiveRecord::formatCurrency($point_sum_gdkv+$app_sum_gdkv);?></td>
            <td class="item_c item_b w-60" style="color:#ad0000"><?php echo ActiveRecord::formatCurrency($bqv_sum_gdkv);?></td>
            <td class="item_c item_b w-60" style="color:#ad0000"><?php echo ActiveRecord::formatCurrency($point_current_sum_gdkv+$app_current_sum_gdkv);?></td>
            <td class="item_c item_b w-60" style="color:#ad0000"><?php echo ActiveRecord::formatCurrency($bqv_current_sum_gdkv);?></td>
            <td class="item_c item_b w-60" style="color:#ad0000"><?php echo ActiveRecord::formatCurrency($bqv_current_sum_gdkv_v2);?></td>
            <td class="item_c item_b w-60" style="color:#ad0000"><?php echo ActiveRecord::formatCurrency($bqv_current_sum_gdkv_share);?></td>
            <td class="item_c item_b w-60" style="color:#ad0000"><?php echo $mGasTargetMonthlyCal->getRateBQV([$point_current_sum_gdkv,$app_current_sum_gdkv],[$point_sum_gdkv,$app_sum_gdkv]);?> %</td>
            <?php 
                $moreCurrent = 0;
                if($mGasTargetMonthlyCal->isApplyShare()){
                    $moreCurrent = $bqv_current_sum_gdkv_share;
                }
            ?>
            <td class="h_50 item_c item_b w-60" style="color:#ad0000"><?php echo $mGasTargetMonthlyCal->getRateBQV([$moreCurrent,$bqv_current_sum_gdkv,$bqv_current_sum_gdkv_v2],[$bqv_sum_gdkv]);?>  %</td>
            <?php 
                $sApp   = $mGasTargetMonthlyCal->getRateBQV([$point_current_sum_gdkv,$app_current_sum_gdkv],[$point_sum_gdkv,$app_sum_gdkv]);
                $sBQV   = $mGasTargetMonthlyCal->getRateBQV([$moreCurrent,$bqv_current_sum_gdkv,$bqv_current_sum_gdkv_v2],[$bqv_sum_gdkv]);
                $sBonus = $mGasTargetMonthlyCal->getRateBonus($sApp,$sBQV);
            ?>
                <td class="item_c item_b w-50" style="color:#ad0000"><?php echo $sBonus;?>  %</td>
                <td class="item_r item_b w-100" style="color:#ad0000"><?php echo ActiveRecord::formatCurrency($sum_gdkv_total + $sum_gdkv_total_share);?></td>
                <td class="item_r item_b w-100" style="color:#ad0000"><?php echo ActiveRecord::formatCurrency($sum_gdkv_total_v2);?></td>
                <td class="item_r item_b w-100" style="color:#ad0000"><?php echo ActiveRecord::formatCurrency($sum_gdkv_fund);?></td>
                <td class="item_r item_b w-100" style="color:#ad0000"><?php echo ActiveRecord::formatCurrency($sum_gdkv_fund_v2);?></td>
                <td class="item_r item_b w-100" style="color:#ad0000"><?php echo ActiveRecord::formatCurrency($sum_gdkv_revenue);?></td>
                <td class="item_r item_b w-100" style="color:#ad0000"><?php echo ActiveRecord::formatCurrency($sum_gdkv_revenue_v2);?></td>
                <td class="item_r item_b w-100" style="color:#ad0000"><?php echo ActiveRecord::formatCurrency($sum_gdkv_revenue+$sum_gdkv_revenue_v2);?></td>
                <td class="item_r item_b w-100" style="color:#ad0000"><?php echo ActiveRecord::formatCurrency($sum_gdkv_revenue_final);?></td>
        </tr>
    </tbody>
</table>
</div>
<?php endforeach; ?>
<?php include 'targetTeam.php'; ?>
<?php endif; ?>
</div>
<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/print-store-card.css" />
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/jquery.floatThead.min.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/underscore-min.js"></script> 
<script>
    $(function(){
       $('.float_tb').floatThead({top:50});
    });
    $('tr.statistic_gskv').each(function(){
        $('table.statistic_gskv_target tbody').append($(this).clone());
    });
</script>