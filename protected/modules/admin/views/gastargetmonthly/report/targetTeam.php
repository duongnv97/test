<hr>
<br>
<div class="row">
<?php foreach ($MONITOR_GDKV as $monitor_gdkv_id => $aCcsId): ?>
    <h1><?php echo isset($USER[$monitor_gdkv_id]) ? $USER[$monitor_gdkv_id]->first_name : ''; ?> </h1>
    <?php foreach ($aCcsId as $monitor_id => $monitor_id_v2): ?>
    <?php 
    $aEmployeeId = !empty($MONITOR[$monitor_id]) ? $MONITOR[$monitor_id] : [];
    $index = 1;
    ?>
        <table class="materials_table hm_table w-400" style="display: inline-table">
            <thead>
                <tr>
                    <th class="item_c w-20">#</th>
                    <th class="item_code item_c w-380"><?php echo isset($USER[$monitor_id]) ? $USER[$monitor_id]->first_name : '';?></th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($aEmployeeId as $user_id => $id): ?>

                <tr>
                    <td class="item_c order_no"><?php echo $index++; ?></td>
                    <?php if(isset($USER[$user_id])): ?>
                    <?php 
                        $mUser = isset($USER[$user_id]) ? $USER[$user_id] : null;
                    ?>
                    <td><?php echo '<b>'.AppPromotion::getRefCodeUser($mUser). '</b> -- ' . $mUser->getNameWithRole();?></td>
                    <?php else : ?>
                    <td></td>
                    <?php endif; ?>
                </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    <?php endforeach; ?>
    <div class="clr"></div>
<?php endforeach; ?>
</div>