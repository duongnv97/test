<?php
    $LinkGeneral    = Yii::app()->createAbsoluteUrl('admin/gastargetmonthly/ccs');
    $LinkTelesale   = Yii::app()->createAbsoluteUrl('admin/gastargetmonthly/ccs', ['type'=> GasTargetMonthly::TYPE_TELESALE]);
    $LinkStatistic  = Yii::app()->createAbsoluteUrl('admin/gastargetmonthly/CcsStatistic', ['type'=> GasTargetMonthly::TYPE_STATISTIC]);
?>
<a class='btn_cancel f_size_14 <?php echo empty($model->type_index) ? "active":"";?>' href="<?php echo $LinkGeneral;?>">CCS</a>
<a class='btn_cancel f_size_14 <?php echo $model->type_index ==  GasTargetMonthly::TYPE_TELESALE ? "active":"";?>' href="<?php echo $LinkTelesale;?>">TELESALE</a>
<?php if(GasCheck::isAllowAccess('gastargetmonthly', 'CcsStatistic')): ?>
    <a class='btn_cancel f_size_14 <?php echo $model->type_index ==  GasTargetMonthly::TYPE_STATISTIC ? "active":"";?>' href="<?php echo $LinkStatistic;?>">BC CHỈ TIÊU</a>
<?php endif;?>