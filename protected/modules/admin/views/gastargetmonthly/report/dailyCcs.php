<?php
$urlExcel = Yii::app()->createAbsoluteUrl('admin/gastargetmonthly/dailyCcs', ['ToExcel'=>1]);
$this->breadcrumbs=array(
    $this->pageTitle,
);
?>
<h1><?php echo $this->pageTitle; ?></h1>
<div class="search-form" style="">
    <div class="wide form">

    <?php $form=$this->beginWidget('CActiveForm', array(
            'action'=>Yii::app()->createUrl($this->route),
            'method'=>'post',
    )); ?>
        <div class="row more_col">
            <div class="col1">
                <?php $mOneMany = new GasOneMany; ?>
                <?php echo $form->labelEx($model,'monitoring_id',array('label'=>'Xem CV PTTT','class'=>'checkbox_one_label', 'style'=>'padding-top:3px;')); ?>
                <?php echo $form->dropDownList($model,'monitoring_id', $mOneMany->getListdataByType(GasOneMany::TYPE_MONITOR_GDKV), array('style'=>'','class'=>'w-200','empty'=>'Select')); ?>
            </div>
            <div class="col2">
                <?php echo Yii::t('translation', $form->label($model,'date_from')); ?>
                <?php 
                    $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                        'model'=>$model,        
                        'attribute'=>'date_from',
                        'options'=>array(
                            'showAnim'=>'fold',
                            'dateFormat'=> MyFormat::$dateFormatSearch,       
                            'maxDate'=> '0',
                            'changeMonth' => true,
                            'changeYear' => true,
                            'showOn' => 'button',
                            'buttonImage'=> Yii::app()->theme->baseUrl.'/admin/images/icon_calendar_r.gif',
                            'buttonImageOnly'=> true,                                
                        ),        
                        'htmlOptions'=>array(
                            'class'=>'w-180',
                            'style'=>'height:20px;float:left;',
                            'readonly'=>'1',
                        ),
                    ));
                ?>     		
            </div>

            <div class="col3">
                <?php echo Yii::t('translation', $form->label($model,'date_to')); ?>
                <?php 
                    $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                        'model'=>$model,        
                        'attribute'=>'date_to',
                        'options'=>array(
                            'showAnim'=>'fold',
                            'dateFormat'=> MyFormat::$dateFormatSearch,       
                            'maxDate'=> '0',
                            'changeMonth' => true,
                            'changeYear' => true,
                            'showOn' => 'button',
                            'buttonImage'=> Yii::app()->theme->baseUrl.'/admin/images/icon_calendar_r.gif',
                            'buttonImageOnly'=> true,                                
                        ),        
                        'htmlOptions'=>array(
                            'class'=>'w-180',
                            'style'=>'height:20px;float:left;',
                            'readonly'=>'1',
                        ),
                    ));
                ?>    
            </div>
            
        </div>
        <div class="row">
            <?php echo $form->label($model,'province_id',array()); ?>
            <?php echo $form->dropDownList($model,'province_id', GasProvince::getArrAll(),array('class'=>'w-200','empty'=>'Select')); ?>
        </div>
        <div class="row">
        <?php echo $form->labelEx($model,'agent_id'); ?>
        <?php echo $form->hiddenField($model,'agent_id', array('class'=>'')); ?>
        <?php
                // 1. limit search kh của sale
                $url = Yii::app()->createAbsoluteUrl('admin/ajax/search_by_role', array('role'=> ROLE_AGENT));
                // widget auto complete search user customer and supplier
                $aDataSearch = array(
                    'model'=>$model,
                    'field_customer_id'=>'agent_id',
                    'url'=> $url,
                    'name_relation_user'=>'rAgent',
                    'ClassAdd' => 'w-200',
                    'field_autocomplete_name' => 'autocomplete_name',
                    'placeholder'=>'Nhập mã hoặc tên đại lý',
                );
                $this->widget('ext.SpaAutocompleteCustomer.SpaAutocompleteCustomer',
                    array('data'=>$aDataSearch));
                ?>
        </div>
        
        <div class="row buttons" style="padding-left: 159px; margin:20px 0;">
                <?php $this->widget('bootstrap.widgets.TbButton', array(
                'buttonType'=>'submit',
                'label'=>Yii::t('translation','Xem Thống Kê'),
                'type'=>'null', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
                'size'=>'small', // null, 'large', 'small' or 'mini'
                //'htmlOptions' => array('style' => 'margin-bottom: 10px; float: right;'),
            )); ?>
            &nbsp;&nbsp;&nbsp;&nbsp;<a class='btn_cancel' target="_blank" href='<?php echo $urlExcel;?>'>Xuất Excel</a>
        </div>

    <?php $this->endWidget(); ?>

    </div><!-- wide form -->
</div><!-- search-form -->
<?php include 'dailyForm.php'; ?>