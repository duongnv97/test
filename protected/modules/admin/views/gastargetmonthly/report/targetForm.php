<?php
$MONITOR            = isset($aData['MONITOR']) ? $aData['MONITOR'] : [];
$MONITOR_GDKV       = isset($aData['MONITOR_GDKV']) ? $aData['MONITOR_GDKV'] : [];
$USER               = isset($aData['USER']) ? $aData['USER'] : [];
$TARGET_DAILY       = isset($aData['TARGET_DAILY']) ? $aData['TARGET_DAILY'] : [];
$TARGET_BQV         = isset($aData['TARGET_BQV']) ? $aData['TARGET_BQV'] : [];
$TARGET_SETUP       = isset($aData['TARGET_SETUP']) ? $aData['TARGET_SETUP'] : [];
?>
<div id="tabs-1">
<br>
<?php if(!empty($MONITOR_GDKV)): ?>
<div class="clearfix box_help_color">
    <div class="color_type_1 color_note"></div>&nbsp;&nbsp; Điểm LV
    <div class="clr"></div>
    <div class="color_type_2 color_note"></div>&nbsp;&nbsp; Số App
    <div class="clr"></div>
    <div class="color_type_3 color_note"></div>&nbsp;&nbsp; BQV
    <div class="clr"></div>
</div>
<?php foreach ($MONITOR_GDKV as $monitor_gdkv_id => $aCcsId): ?>
<?php 
    $point_sum_gdkv = 0;
    $point_current_sum_gdkv =  0;
    $app_sum_gdkv = 0;
    $app_current_sum_gdkv = 0;
    $bqv_sum_gdkv = 0;
    $bqv_current_sum_gdkv = 0;
?>
<div class="gdkvGroup">
<h1><?php echo isset($USER[$monitor_gdkv_id]) ? $USER[$monitor_gdkv_id]->first_name : ''; ?> </h1>
<div class="box_350 box_ccs_view_gdkv"></div>
<div class="clr"></div>
<br><br><br>
<?php foreach ($aCcsId as $monitor_id => $monitor_id_v2): ?>
<?php 
    if(empty($MONITOR[$monitor_id])){
        continue;
    }
    $aEmployeeId = $MONITOR[$monitor_id];
    $point_sum = 0;
    $point_current_sum =  0;
    $app_sum = 0;
    $app_current_sum = 0;
    $bqv_sum = 0;
    $bqv_current_sum = 0;
?>
<div class="CcsGroup">
    <div class="box_350 box_ccs_view"></div>
    <div class="clr"></div>
    <?php foreach ($aEmployeeId as $user_id => $id): ?>
    <?php
    $point = isset($TARGET_SETUP[$user_id]['point']) ? $TARGET_SETUP[$user_id]['point'] : 0;
    $point_current = isset($TARGET_DAILY[$user_id]['point']) ? $TARGET_DAILY[$user_id]['point'] : 0;
    $app = isset($TARGET_SETUP[$user_id]['app']) ? $TARGET_SETUP[$user_id]['app'] : 0;
    $app_current = isset($TARGET_DAILY[$user_id]['app']) ? $TARGET_DAILY[$user_id]['app'] : 0;
    $bqv = isset($TARGET_SETUP[$user_id]['bqv']) ? $TARGET_SETUP[$user_id]['bqv'] : 0;
    $bqv_current = isset($TARGET_BQV[$user_id]['bqv']) ? $TARGET_BQV[$user_id]['bqv'] : 0;
//    sum
    $point_sum += $point;
    $point_current_sum +=  $point_current;
    $app_sum += $app;
    $app_current_sum += $app_current;
    $bqv_sum += $bqv;
    $bqv_current_sum += $bqv_current;
//    sum gdkv
    $point_sum_gdkv += $point;
    $point_current_sum_gdkv +=  $point_current;
    $app_sum_gdkv += $app;
    $app_current_sum_gdkv += $app_current;
    $bqv_sum_gdkv += $bqv;
    $bqv_current_sum_gdkv += $bqv_current;
    ?>
    <div class="box_350 box_ccs_hide">
        <table class="hm_table items">
            <tbody>
                <tr>
                    <td colspan="3" class="item_c item_b"><?php echo isset($USER[$user_id]) ? $USER[$user_id]->first_name : ''; ?></td>
                </tr>
                <tr>
                    <td class="item_c w-140">Target</td>
                    <td class="item_c w-140">Thực Tế</td>
                    <td class="item_c w-70">Tỷ Lệ</td>
                </tr>
                <tr class="color_type_1">
                    <td class="item_r">
                        <?php echo ActiveRecord::formatCurrency($point);?>
                    </td>
                    <td class="item_r">
                        <?php echo ActiveRecord::formatCurrency($point_current);?>
                    </td>
                    <td class="item_r">
                        <?php echo $point > 0 ? (int)($point_current*100/$point) : 100;?> %
                    </td>
                </tr>

                <tr class="color_type_2">
                    <td class="item_r">
                        <?php echo ActiveRecord::formatCurrency($app);?>
                    </td>
                    <td class="item_r">
                        <?php echo ActiveRecord::formatCurrency($app_current);?>
                    </td>
                    <td class="item_r">
                        <?php echo $app > 0 ? (int)($app_current*100/$app) : 100;?> %
                    </td>
                </tr>

                <tr class="color_type_3">
                    <td class="item_r">
                        <?php echo ActiveRecord::formatCurrency($bqv);?>
                    </td>
                    <td class="item_r">
                        <?php echo ActiveRecord::formatCurrency($bqv_current);?>
                    </td>
                    <td class="item_r">
                        <?php echo $bqv > 0 ? (int)($bqv_current*100/$bqv) : 100;?>  %
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
    <?php endforeach; ?>
    <div class="clr"></div>
    <div class="box_350 box_ccs_hide display_none">
        <table class="hm_table items ">
            <tbody>
                <tr>
                    <td colspan="3" class="item_c item_b color_red"><?php echo isset($USER[$monitor_id]) ? $USER[$monitor_id]->first_name : ''; ?></td>
                </tr>
                <tr>
                    <td class="item_c w-140">Target</td>
                    <td class="item_c w-140">Thực Tế</td>
                    <td class="item_c w-70">Tỷ Lệ</td>
                </tr>
                <tr class="color_type_1">
                    <td class="item_r">
                        <?php echo ActiveRecord::formatCurrency($point_sum);?>
                    </td>
                    <td class="item_r">
                        <?php echo ActiveRecord::formatCurrency($point_current_sum);?>
                    </td>
                    <td class="item_r">
                        <?php echo $point_sum > 0 ? (int)($point_current_sum*100/$point_sum) : 100;?> %
                    </td>
                </tr>

                <tr class="color_type_2">
                    <td class="item_r">
                        <?php echo ActiveRecord::formatCurrency($app_sum);?>
                    </td>
                    <td class="item_r">
                        <?php echo ActiveRecord::formatCurrency($app_current_sum);?>
                    </td>
                    <td class="item_r">
                        <?php echo $app_sum > 0 ? (int)($app_current_sum*100/$app_sum) : 100;?> %
                    </td>
                </tr>

                <tr class="color_type_3">
                    <td class="item_r">
                        <?php echo ActiveRecord::formatCurrency($bqv_sum);?>
                    </td>
                    <td class="item_r">
                        <?php echo ActiveRecord::formatCurrency($bqv_current_sum);?>
                    </td>
                    <td class="item_r">
                        <?php echo $bqv_sum > 0 ? (int)($bqv_current_sum*100/$bqv_sum) : 100;?>  %
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
    <div class="clr"></div>
    <br><br><br>
</div>
<?php endforeach; ?>
    <div class="box_350 box_ccs_hide_gdkv display_none">
        <table class="hm_table items ">
            <tbody>
                <tr>
                    <td class="item_c w-140">Target</td>
                    <td class="item_c w-140">Thực Tế</td>
                    <td class="item_c w-70">Tỷ Lệ</td>
                </tr>
                <tr class="color_type_1">
                    <td class="item_r">
                        <?php echo ActiveRecord::formatCurrency($point_sum_gdkv);?>
                    </td>
                    <td class="item_r">
                        <?php echo ActiveRecord::formatCurrency($point_current_sum_gdkv);?>
                    </td>
                    <td class="item_r">
                        <?php echo $point_sum_gdkv > 0 ? (int)($point_current_sum_gdkv*100/$point_sum_gdkv) : 100;?> %
                    </td>
                </tr>

                <tr class="color_type_2">
                    <td class="item_r">
                        <?php echo ActiveRecord::formatCurrency($app_sum_gdkv);?>
                    </td>
                    <td class="item_r">
                        <?php echo ActiveRecord::formatCurrency($app_current_sum_gdkv);?>
                    </td>
                    <td class="item_r">
                        <?php echo $app_sum_gdkv > 0 ? (int)($app_current_sum_gdkv*100/$app_sum_gdkv) : 100;?> %
                    </td>
                </tr>

                <tr class="color_type_3">
                    <td class="item_r">
                        <?php echo ActiveRecord::formatCurrency($bqv_sum_gdkv);?>
                    </td>
                    <td class="item_r">
                        <?php echo ActiveRecord::formatCurrency($bqv_current_sum_gdkv);?>
                    </td>
                    <td class="item_r">
                        <?php echo $bqv_sum_gdkv > 0 ? (int)($bqv_current_sum_gdkv*100/$bqv_sum_gdkv) : 100;?>  %
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
</div>
<?php endforeach; ?>
<?php include 'targetTeam.php'; ?>
<?php endif; ?>
<script>
    $(document).ready(function(){
        $('.box_ccs_hide').each(function(){
            $(this).closest('.CcsGroup').find('.box_ccs_view').html($(this).html());
        });
    });
    $(document).ready(function(){
        $('.box_ccs_hide_gdkv').each(function(){
            $(this).closest('.gdkvGroup').find('.box_ccs_view_gdkv').html($(this).html());
        });
    });
</script>
</div>