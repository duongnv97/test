<?php
$MONITOR            = isset($aData['MONITOR']) ? $aData['MONITOR'] : [];
$MONITOR_GDKV       = isset($aData['MONITOR_GDKV']) ? $aData['MONITOR_GDKV'] : [];
$USER               = isset($aData['USER']) ? $aData['USER'] : [];
$TARGET_DAILY       = isset($aData['TARGET_DAILY']) ? $aData['TARGET_DAILY'] : [];
$TARGET_BQV         = isset($aData['TARGET_BQV']) ? $aData['TARGET_BQV'] : [];
$TARGET_BQV_SHARE   = isset($aData['TARGET_SHARE']) ? $aData['TARGET_SHARE'] : [];
$TARGET_BQV_V2      = isset($aData['TARGET_BQV_V2']) ? $aData['TARGET_BQV_V2'] : [];
$TARGET_SETUP       = isset($aData['TARGET_SETUP']) ? $aData['TARGET_SETUP'] : [];
$OUT_FOLLOW_CHART   = isset($aData['TARGET_TELESALE']['OUT_FOLLOW_CHART']) ? $aData['TARGET_TELESALE']['OUT_FOLLOW_CHART'] : [];
$OUT_FOLLOW         = isset($aData['TARGET_TELESALE']['OUT_FOLLOW']) ? $aData['TARGET_TELESALE']['OUT_FOLLOW'] : [];
$OUT_TRACKING       = isset($aData['TARGET_TELESALE']['OUT_TRACKING']) ? $aData['TARGET_TELESALE']['OUT_TRACKING'] : [];
$TELESALE_CODE_CALLOUT   = isset($aData['TARGET_TELESALE']['TELESALE_CODE_CALLOUT']) ? $aData['TARGET_TELESALE']['TELESALE_CODE_CALLOUT'] : [];
$TARGET_TELESALE_BQV   = isset($aData['TARGET_TELESALE_BQV']) ? $aData['TARGET_TELESALE_BQV'] : [];
$HOLIDAY_TELESALE   = isset($aData['HOLIDAY_TELESALE']) ? $aData['HOLIDAY_TELESALE'] : [];
$mGasTargetMonthlyCal = new GasTargetMonthly();
$mGasTargetMonthlyCal->date_from = $model->date_from;
?>
<div id="tabs-2">
<br>
<?php if(!empty($MONITOR_GDKV)): ?>
<?php foreach ($MONITOR_GDKV as $monitor_gdkv_id => $aCcsId): ?>
<?php 
    $point_sum_gdkv = 0;
    $point_current_sum_gdkv =  0;
    $app_sum_gdkv = 0;
    $bqv_sum_gdkv = 0;
    $bqv_current_sum_gdkv = 0;
    $bqv_current_sum_gdkv_v2 = 0;
    $sum_gdkv_total = 0;
    $sum_gdkv_total_share = 0;
    $sum_gdkv_total_v2 = 0;
    $sum_gdkv_fund = 0;
    $sum_gdkv_fund_v2 = 0;
    $sum_gdkv_revenue = 0;
    $sum_gdkv_revenue_v2 = 0;
    $sum_gdkv_revenue_final = 0;
    $sum_gdkv_remain = 0;
    $sum_gdkv_remain_v2 = 0;
    $bqv_current_sum_gdkv_share = 0;
    $sum_gdkv_remain_share = 0;
    $app_current_in_sum_gdkv = 0;
    $app_current_out_sum_gdkv = 0;
    $bqv_current_support_sum_gdkv = 0;
?>
<div class="gdkvGroup">
<h1><?php echo isset($USER[$monitor_gdkv_id]) ? $USER[$monitor_gdkv_id]->first_name : ''; ?> </h1>
<table class="materials_table hm_table float_tb" style="border-collapse: collapse;">
    <thead style="background: #f1f1f1;">
        <tr>
        <th class="item_b item_c" rowspan="2">STT</th>
            <th class="item_b item_c" rowspan="2">Tên nhân viên</th>
            <th class="item_b item_c" colspan="3">Target</th>
            <th class="item_b item_c" colspan="6">Thực tế</th>
            <th class="item_b item_c" colspan="3">Tỷ lệ</th>
            <th class="item_b item_c" rowspan="2">Tỷ Lệ Thưởng</th>
            <th class="item_b item_c" rowspan="2">Doanh Thu 1</th>
            <th class="item_b item_c" rowspan="2">Doanh Thu 2</th>
            <th class="item_b item_c" rowspan="2">Tiền vốn 1</th>
            <th class="item_b item_c" rowspan="2">Tiền vốn 2</th>
            <th class="item_b item_c" rowspan="2">Lợi nhuận 1</th>
            <th class="item_b item_c" rowspan="2">Lợi nhuận 2</th>
            <th class="item_b item_c" rowspan="2">Tổng lợi nhuận</th>
            <th class="item_b item_c" rowspan="2">Thưởng</th>
        </tr>
        <tr>
            <th class="item_b item_c">CG/ ngày</th>
            <th class="item_b item_c">App/ Tháng</th>
            <th class="item_b item_c">BQV (kg)</th>
            <th class="item_b item_c">CG/ ngày</th>
            <th class="item_b item_c">App Trong</th>
            <th class="item_b item_c">App Ngoài</th>
            <th class="item_b item_c">BQV Lần 1 (kg)</th>
            <th class="item_b item_c">BQV Lần 2 (kg)</th>
            <th class="item_b item_c">BQV Chia Sẽ (kg)</th>
            <th class="item_b item_c">CG/ ngày</th>
            <th class="item_b item_c">App</th>
            <th class="item_b item_c">BQV</th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($aCcsId as $monitor_id => $monitor_id_v2): ?>
        <?php 
            if(empty($MONITOR[$monitor_id])){
                continue;
            }
            $aEmployeeId = $MONITOR[$monitor_id];
            $point_sum = 0;
            $point_current_sum =  0;
            $app_sum = 0;
            $bqv_sum = 0;
            $bqv_current_sum = 0;
            $bqv_current_sum_v2 = 0;
            $sum_total  = 0;
            $sum_total_share  = 0;
            $sum_total_v2  = 0;
            $sum_fund  = 0;
            $sum_fund_v2  = 0;
            $sum_revenue  = 0;
            $sum_revenue_v2  = 0;
            $sum_revenue_final  = 0;
            $sum_remain = 0;
            $sum_remain_v2 = 0;
            $bqv_current_sum_share = 0;
            $sum_remain_share = 0;
            $app_current_in_sum = 0;
            $app_current_out_sum = 0;
            $bqv_current_support_sum = 0;
        ?>
        <tr>
            <td class="item_c item_b" colspan="23"><?php echo isset($USER[$monitor_id]) ? $USER[$monitor_id]->first_name : ''; ?></td>
        </tr>
            <?php $index = 1;?>
            <?php foreach ($aEmployeeId as $user_id => $id): ?>
            <?php
//            gas remain
            $current_remain  = !empty($TARGET_BQV['GAS_REMAIN'][$user_id]) ? $TARGET_BQV['GAS_REMAIN'][$user_id] : 0;
            $current_remain  += !empty($TARGET_TELESALE_BQV['GAS_REMAIN'][$user_id]) ? $TARGET_TELESALE_BQV['GAS_REMAIN'][$user_id] : 0;
            $sum_remain      +=   $current_remain;
            $sum_gdkv_remain +=  $current_remain;
//            gas remain
            $current_remain_v2  = !empty($TARGET_BQV_V2['GAS_REMAIN'][$user_id]) ? $TARGET_BQV_V2['GAS_REMAIN'][$user_id] : 0;
            $sum_remain_v2      +=   $current_remain_v2;
            $sum_gdkv_remain_v2 +=  $current_remain_v2;
//            gas remain
            $current_remain_share  = !empty($TARGET_BQV_SHARE['GAS_REMAIN'][$user_id]) ? $TARGET_BQV_SHARE['GAS_REMAIN'][$user_id] : 0;
            $sum_remain_share      +=   $current_remain_share;
            $sum_gdkv_remain_share +=  $current_remain_share;
            
            $point = isset($TARGET_SETUP[$user_id]['point']) ? $TARGET_SETUP[$user_id]['point'] : 0;
//            setup count date in here
            $countDate     = isset($OUT_FOLLOW[$user_id]) ? $mGasTargetMonthlyCal->getCountDate($user_id,$OUT_FOLLOW[$user_id],$HOLIDAY_TELESALE) : 0;
            $countDate     = $countDate <= 0 ? 1 : $countDate; // nếu 0 thì tính cho 1 công để hiển thị số cuộc gọi
            $point_current = isset($OUT_FOLLOW_CHART[$user_id]['value']) ? (int)((int)$OUT_FOLLOW_CHART[$user_id]['value'] / $countDate) : '0';
            $app = isset($TARGET_SETUP[$user_id]['app']) ? $TARGET_SETUP[$user_id]['app'] : 0;
            $app_current_in = isset($TELESALE_CODE_CALLOUT[$user_id]) ? (int)$TELESALE_CODE_CALLOUT[$user_id] : 0;
            $app_current_in_sum += $app_current_in;
            $app_current_in_sum_gdkv += $app_current_in;
            $nRef               = isset($OUT_TRACKING[$user_id]) ? (int)$OUT_TRACKING[$user_id] : 0;
            $app_current_out = $nRef - $app_current_in;
            $app_current_out_sum += $app_current_out;
            $app_current_out_sum_gdkv += $app_current_out;
            $bqv = isset($TARGET_SETUP[$user_id]['bqv']) ? $TARGET_SETUP[$user_id]['bqv'] : 0;
            $bqv_telesale  = isset($TARGET_TELESALE_BQV[$user_id]['bqv']) ? $TARGET_TELESALE_BQV[$user_id]['bqv'] : 0;
            $bqv_current = isset($TARGET_BQV[$user_id]['bqv']) ? $TARGET_BQV[$user_id]['bqv'] : 0;
            $bqv_current = $mGasTargetMonthlyCal->getKgBQV($bqv_current+$bqv_telesale,$current_remain);
            $bqv_current_support    = isset($TARGET_BQV['GAS_SUPPORT'][$user_id]['bqv']) ? $TARGET_BQV['GAS_SUPPORT'][$user_id]['bqv'] : 0;
            $bqv_current_support    += isset($TARGET_TELESALE_BQV['GAS_SUPPORT'][$user_id]['bqv']) ? $TARGET_TELESALE_BQV['GAS_SUPPORT'][$user_id]['bqv'] : 0;
            $remain_current_support    = isset($TARGET_BQV['GAS_SUPPORT'][$user_id]['GAS_REMAIN']) ? $TARGET_BQV['GAS_SUPPORT'][$user_id]['GAS_REMAIN'] : 0;
            $remain_current_support    += isset($TARGET_TELESALE_BQV['GAS_SUPPORT'][$user_id]['GAS_REMAIN']) ? $TARGET_TELESALE_BQV['GAS_SUPPORT'][$user_id]['GAS_REMAIN'] : 0;
            $bqv_current_support    = $mGasTargetMonthlyCal->getKgBQV($bqv_current_support,$remain_current_support);
            $bqv_current_support_sum    += $bqv_current_support;
            $bqv_current_support_sum_gdkv    += $bqv_current_support;
            $bqv_current_v2 = isset($TARGET_BQV_V2[$user_id]['bqv']) ? $TARGET_BQV_V2[$user_id]['bqv'] : 0;
            $bqv_current_v2 = $mGasTargetMonthlyCal->getKgBQV($bqv_current_v2,$current_remain_v2);
            $bqv_current_share = isset($TARGET_BQV_SHARE[$user_id]['bqv']) ? $TARGET_BQV_SHARE[$user_id]['bqv'] : 0;
            $bqv_current_share = $mGasTargetMonthlyCal->getKgBQV($bqv_current_share,$current_remain_share);
        //    sum
            $point_sum += $point;
            $point_current_sum +=  $point_current;
            $app_sum += $app;
            $bqv_sum += $bqv;
            $bqv_current_sum += $bqv_current;
            $bqv_current_sum_v2 += $bqv_current_v2;
            $bqv_current_sum_share += $bqv_current_share;
        //    sum gdkv
            $point_sum_gdkv += $point;
            $point_current_sum_gdkv +=  $point_current;
            $app_sum_gdkv += $app;
            $bqv_sum_gdkv += $bqv;
            $bqv_current_sum_gdkv += $bqv_current;
            $bqv_current_sum_gdkv_v2 += $bqv_current_v2;
            $bqv_current_sum_gdkv_share += $bqv_current_share;
//            doanh thu
            $current_total  = !empty($TARGET_BQV['GRAND_TOTAL'][$user_id]) ? $TARGET_BQV['GRAND_TOTAL'][$user_id] : 0;
            $current_total  += !empty($TARGET_TELESALE_BQV['GRAND_TOTAL'][$user_id]) ? $TARGET_TELESALE_BQV['GRAND_TOTAL'][$user_id] : 0;
            $sum_total      +=   $current_total;
            $sum_gdkv_total +=  $current_total;
            
            $current_total_share  = !empty($TARGET_BQV_SHARE['GRAND_TOTAL'][$user_id]) ? $TARGET_BQV_SHARE['GRAND_TOTAL'][$user_id] : 0;
            $sum_gdkv_total_share +=  $current_total_share;
            $sum_total_share      +=   $current_total_share;
            
            $current_total_v2  = !empty($TARGET_BQV_V2['GRAND_TOTAL'][$user_id]) ? $TARGET_BQV_V2['GRAND_TOTAL'][$user_id] : 0;
            $current_total_v2  += !empty($TARGET_BQV_SHARE['GRAND_TOTAL'][$user_id]) ? $TARGET_BQV_SHARE['GRAND_TOTAL'][$user_id] : 0;
            $sum_gdkv_total_v2 +=  $current_total_v2;
            $sum_total_v2      +=   $current_total_v2;
            
            $current_fund  = !empty($TARGET_BQV['GAS_FUNDS'][$user_id]) ? (int)$TARGET_BQV['GAS_FUNDS'][$user_id] : 0;
            $current_fund  += !empty($TARGET_BQV_SHARE['GAS_FUNDS'][$user_id]) ? (int)$TARGET_BQV_SHARE['GAS_FUNDS'][$user_id] : 0;
            $current_fund  += !empty($TARGET_TELESALE_BQV['GAS_FUNDS'][$user_id]) ? (int)$TARGET_TELESALE_BQV['GAS_FUNDS'][$user_id] : 0;
            $sum_gdkv_fund +=  $current_fund;
            $sum_fund      +=   $current_fund;
            
            $current_fund_v2  = !empty($TARGET_BQV_V2['GAS_FUNDS'][$user_id]) ? (int)$TARGET_BQV_V2['GAS_FUNDS'][$user_id] : 0;
            $current_fund_v2  += !empty($TARGET_BQV_SHARE['GAS_FUNDS'][$user_id]) ? (int)$TARGET_BQV_SHARE['GAS_FUNDS'][$user_id] : 0;
            $sum_gdkv_fund_v2 +=  $current_fund_v2;
            $sum_fund_v2      +=   $current_fund_v2;
            
            $current_revenue_support_remove    = isset($TARGET_BQV['GAS_SUPPORT'][$user_id]['remove_revenue']) ? $TARGET_BQV['GAS_SUPPORT'][$user_id]['remove_revenue'] : 0;
            $current_revenue_support_remove    += isset($TARGET_TELESALE_BQV['GAS_SUPPORT'][$user_id]['remove_revenue']) ? $TARGET_TELESALE_BQV['GAS_SUPPORT'][$user_id]['remove_revenue'] : 0;
            $current_revenue  = round($current_total + $current_total_share - $current_fund - $current_revenue_support_remove);
            $sum_gdkv_revenue +=  $current_revenue;
            $sum_revenue      +=   $current_revenue;
            
            $current_revenue_v2  = $current_total_v2 - $current_fund_v2;
            $current_revenue_v2  = $current_revenue_v2 / 2; // Apr 23, 2019 bỏ 50% lợi nhuận BQV 2
            $sum_gdkv_revenue_v2 +=  $current_revenue_v2;
            $sum_revenue_v2      +=   $current_revenue_v2;            
            
            ?>
            <tr>
                <td class="item_c w-50"><?php echo $index++;?></td>
                <td class="display_none"><?php echo $countDate;?></td>
                <td class="item_l w-55"><?php echo isset($USER[$user_id]) ? $USER[$user_id]->first_name : ''; ?></td>
                <td class="item_c w-60"><?php echo ActiveRecord::formatCurrency($point);?></td>
                <td class="item_c w-60"><?php echo ActiveRecord::formatCurrency($app);?></td>
                <td class="item_c w-60"><?php echo ActiveRecord::formatCurrency($bqv);?></td>
                <td class="item_c w-60"><?php echo ActiveRecord::formatCurrency($point_current);?></td>
                <td class="item_c w-60"><?php echo ActiveRecord::formatCurrency($app_current_in);?></td>
                <td class="item_c w-60"><?php echo ActiveRecord::formatCurrency($app_current_out);?></td>
                <td class="item_c w-60"><?php echo ActiveRecord::formatCurrency($bqv_current_support).'/ '.ActiveRecord::formatCurrency($bqv_current);?></td>
                <td class="item_c w-60"><?php echo ActiveRecord::formatCurrency($bqv_current_v2);?></td>
                <td class="item_c w-60"><?php echo ActiveRecord::formatCurrency($bqv_current_share);?></td>
                <td class="item_c w-60"><?php echo $mGasTargetMonthlyCal->getRateBQV([$point_current],[$point]);?> %</td>
                <td class="item_c w-60"><?php echo $mGasTargetMonthlyCal->getRateBQV([($app_current_in * GasTargetMonthly::PERCENT_TELESALE_IN),($app_current_out * GasTargetMonthly::PERCENT_TELESALE_OUT) ],[$app]);?> %</td>
                <td class="item_c w-60"><?php echo $mGasTargetMonthlyCal->getRateBQV([$bqv_current_support,$bqv_current_v2],[$bqv]);?>  %</td>
                <?php 
                $sPoint = $mGasTargetMonthlyCal->getRateBQV([$point_current],[$point]);
                $sApp   = $mGasTargetMonthlyCal->getRateBQV([($app_current_in * GasTargetMonthly::PERCENT_TELESALE_IN),($app_current_out * GasTargetMonthly::PERCENT_TELESALE_OUT) ],[$app]);
                $sBQV   = $mGasTargetMonthlyCal->getRateBQV([$bqv_current_support,$bqv_current_v2],[$bqv]);
                $sBonus = $mGasTargetMonthlyCal->getRateBonusTelesale($sPoint,$sApp,$sBQV);
                $revenue                = ($mGasTargetMonthlyCal->isBonus($user_id,$countDate,$model->date_from_ymd,$sBonus) &&($sBonus/100) * ($current_revenue + $current_revenue_v2) > 0) ? (int)((($sBonus/100) * ($current_revenue)) + $current_revenue_v2) : 0 ;
                $sum_gdkv_revenue_final +=  $revenue;
                $sum_revenue_final      +=  $revenue;
                
                ?>
                <td class="item_c w-50"><?php echo $sBonus;?>  %</td>
                <td class="item_r w-100"><?php echo ActiveRecord::formatCurrency($current_total + $current_total_share);?></td>
                <td class="item_r w-100"><?php echo ActiveRecord::formatCurrency($current_total_v2);?></td>
                <td class="item_r w-100"><?php echo ActiveRecord::formatCurrency($current_fund);?></td>
                <td class="item_r w-100"><?php echo ActiveRecord::formatCurrency($current_fund_v2);?></td>
                <td class="item_r w-100"><?php echo ActiveRecord::formatCurrency($current_revenue);?></td>
                <td class="item_r w-100"><?php echo ActiveRecord::formatCurrency($current_revenue_v2);?></td>
                <td class="item_r w-100"><?php echo ActiveRecord::formatCurrency($current_revenue + $current_revenue_v2);?></td>
                <td class="item_r w-100"><?php echo ActiveRecord::formatCurrency($revenue);?></td>
            </tr>
            <?php endforeach;?>
            <tr>
                <td class="item_c item_b w-140" colspan="2">Tổng</td>
                <td class="item_c item_b w-60"><?php echo ActiveRecord::formatCurrency($point_sum);?></td>
                <td class="item_c item_b w-60"><?php echo ActiveRecord::formatCurrency($app_sum);?></td>
                <td class="item_c item_b w-60"><?php echo ActiveRecord::formatCurrency($bqv_sum);?></td>
                <td class="item_c item_b w-60"><?php echo ActiveRecord::formatCurrency($point_current_sum);?></td>
                <td class="item_c item_b w-60"><?php echo ActiveRecord::formatCurrency($app_current_in_sum);?></td>
                <td class="item_c item_b w-60"><?php echo  ActiveRecord::formatCurrency($app_current_out_sum);?></td>
                <td class="item_c item_b w-60"><?php echo ActiveRecord::formatCurrency($bqv_current_support_sum).'/ '.ActiveRecord::formatCurrency($bqv_current_sum);?></td>
                <td class="item_c item_b w-60"><?php echo ActiveRecord::formatCurrency($bqv_current_sum_v2);?></td>
                <td class="item_c item_b w-60"><?php echo ActiveRecord::formatCurrency($bqv_current_sum_share);?></td>
                <td class="item_c item_b w-60"><?php echo $mGasTargetMonthlyCal->getRateBQV([$point_current_sum],[$point_sum]);?> %</td>
                <td class="item_c item_b w-60"><?php echo $mGasTargetMonthlyCal->getRateBQV([($app_current_in_sum * GasTargetMonthly::PERCENT_TELESALE_IN),($app_current_out_sum * GasTargetMonthly::PERCENT_TELESALE_OUT) ],[$app_sum]);?> %</td>
                <td class="item_c item_b w-60"><?php echo $mGasTargetMonthlyCal->getRateBQV([$bqv_current_support_sum,$bqv_current_sum_v2],[$bqv_sum]);?>  %</td>
                <?php 
                $sPoint = $mGasTargetMonthlyCal->getRateBQV([$point_current_sum],[$point_sum]);
                $sApp   = $mGasTargetMonthlyCal->getRateBQV([($app_current_in_sum * GasTargetMonthly::PERCENT_TELESALE_IN),($app_current_out_sum * GasTargetMonthly::PERCENT_TELESALE_OUT) ],[$app_sum]);
                $sBQV   = $mGasTargetMonthlyCal->getRateBQV([$bqv_current_support_sum,$bqv_current_sum_v2],[$bqv_sum]);
                $sBonus = $mGasTargetMonthlyCal->getRateBonusTelesale($sPoint,$sApp,$sBQV);
                ?>
                <td class="item_c item_b w-50"><?php echo $sBonus;?>  %</td>
                <td class="item_r item_b w-100"><?php echo ActiveRecord::formatCurrency($sum_total + $sum_total_share);?></td>
                <td class="item_r item_b w-100"><?php echo ActiveRecord::formatCurrency($sum_total_v2);?></td>
                <td class="item_r item_b w-100"><?php echo ActiveRecord::formatCurrency($sum_fund);?></td>
                <td class="item_r item_b w-100"><?php echo ActiveRecord::formatCurrency($sum_fund_v2);?></td>
                <td class="item_r item_b w-100"><?php echo ActiveRecord::formatCurrency($sum_revenue);?></td>
                <td class="item_r item_b w-100"><?php echo ActiveRecord::formatCurrency($sum_revenue_v2);?></td>
                <td class="item_r item_b w-100"><?php echo ActiveRecord::formatCurrency($sum_revenue+$sum_revenue_v2);?></td>
                <td class="item_r item_b w-100"><?php echo ActiveRecord::formatCurrency($sum_revenue_final);?></td>
            </tr>
        <?php endforeach;?>
        <tr>
            <td class="item_c item_b w-130" colspan="2" style="color:#ad0000">Tổng - <?php echo isset($USER[$monitor_gdkv_id]) ? $USER[$monitor_gdkv_id]->first_name : ''; ?></td>
            <td class="item_c item_b w-60" style="color:#ad0000"><?php echo ActiveRecord::formatCurrency($point_sum_gdkv);?></td>
            <td class="item_c item_b w-60" style="color:#ad0000"><?php echo ActiveRecord::formatCurrency($app_sum_gdkv);?></td>
            <td class="item_c item_b w-60" style="color:#ad0000"><?php echo ActiveRecord::formatCurrency($bqv_sum_gdkv);?></td>
            <td class="item_c item_b w-60" style="color:#ad0000"><?php echo ActiveRecord::formatCurrency($point_current_sum_gdkv);?></td>
            <td class="item_c item_b w-60" style="color:#ad0000"><?php echo ActiveRecord::formatCurrency($app_current_in_sum_gdkv);?></td>
            <td class="item_c item_b w-60" style="color:#ad0000"><?php echo  ActiveRecord::formatCurrency($app_current_out_sum);?></td>
            <td class="item_c item_b w-60" style="color:#ad0000"><?php echo ActiveRecord::formatCurrency($bqv_current_support_sum_gdkv).'/ '.ActiveRecord::formatCurrency($bqv_current_sum_gdkv);?></td>
            <td class="item_c item_b w-60" style="color:#ad0000"><?php echo ActiveRecord::formatCurrency($bqv_current_sum_gdkv_v2);?></td>
            <td class="item_c item_b w-60" style="color:#ad0000"><?php echo ActiveRecord::formatCurrency($bqv_current_sum_gdkv_share);?></td>
            <td class="item_c item_b w-60"><?php echo $mGasTargetMonthlyCal->getRateBQV([$point_current_sum_gdkv],[$point_sum_gdkv]);?> %</td>
            <td class="item_c item_b w-60"><?php echo $mGasTargetMonthlyCal->getRateBQV([($app_current_in_sum_gdkv * GasTargetMonthly::PERCENT_TELESALE_IN),($app_current_out_sum_gdkv * GasTargetMonthly::PERCENT_TELESALE_OUT)],[$app_sum_gdkv]);?> %</td>
            <td class="h_50 item_c item_b w-60" style="color:#ad0000"><?php echo $mGasTargetMonthlyCal->getRateBQV([$bqv_current_support_sum_gdkv,$bqv_current_sum_gdkv_v2],[$bqv_sum_gdkv]);?>  %</td>
            <?php 
                $sPoint = $mGasTargetMonthlyCal->getRateBQV([$point_current_sum_gdkv],[$point_sum_gdkv]);
                $sApp   = $mGasTargetMonthlyCal->getRateBQV([($app_current_in_sum_gdkv * GasTargetMonthly::PERCENT_TELESALE_IN),($app_current_out_sum_gdkv * GasTargetMonthly::PERCENT_TELESALE_OUT) ],[$app_sum_gdkv]);
                $sBQV   = $mGasTargetMonthlyCal->getRateBQV([$bqv_current_support_sum_gdkv,$bqv_current_sum_gdkv_v2],[$bqv_sum_gdkv]);
                $sBonus = $mGasTargetMonthlyCal->getRateBonusTelesale($sPoint,$sApp,$sBQV);
            ?>
                <td class="item_c item_b w-50" style="color:#ad0000"><?php echo $sBonus;?>  %</td>
                <td class="item_r item_b w-100" style="color:#ad0000"><?php echo ActiveRecord::formatCurrency($sum_gdkv_total + $sum_gdkv_total_share);?></td>
                <td class="item_r item_b w-100" style="color:#ad0000"><?php echo ActiveRecord::formatCurrency($sum_gdkv_total_v2);?></td>
                <td class="item_r item_b w-100" style="color:#ad0000"><?php echo ActiveRecord::formatCurrency($sum_gdkv_fund);?></td>
                <td class="item_r item_b w-100" style="color:#ad0000"><?php echo ActiveRecord::formatCurrency($sum_gdkv_fund_v2);?></td>
                <td class="item_r item_b w-100" style="color:#ad0000"><?php echo ActiveRecord::formatCurrency($sum_gdkv_revenue);?></td>
                <td class="item_r item_b w-100" style="color:#ad0000"><?php echo ActiveRecord::formatCurrency($sum_gdkv_revenue_v2);?></td>
                <td class="item_r item_b w-100" style="color:#ad0000"><?php echo ActiveRecord::formatCurrency($sum_gdkv_revenue+$sum_gdkv_revenue_v2);?></td>
                <td class="item_r item_b w-100" style="color:#ad0000"><?php echo ActiveRecord::formatCurrency($sum_gdkv_revenue_final);?></td>
        </tr>
    </tbody>
</table>
</div>
<?php endforeach; ?>
<?php include 'targetTeam.php'; ?>
<?php endif; ?>
</div>
<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/print-store-card.css" />
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/jquery.floatThead.min.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/underscore-min.js"></script> 
<script>
    $(function(){
       $('.float_tb').floatThead({top:50});
    });
</script>