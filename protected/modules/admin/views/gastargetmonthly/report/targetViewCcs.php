<div class="search-form" style="">
    <div class="wide form">

    <?php $form=$this->beginWidget('CActiveForm', array(
            'action'=>GasCheck::getCurl(),
            'method'=>'post',
    )); ?>
        <div class="row more_col">
            <div class="col1">
                <?php echo Yii::t('translation', $form->label($model,'date_from')); ?>
                <?php 
                    $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                        'model'=>$model,        
                        'attribute'=>'date_from',
                        'options'=>array(
                            'showAnim'=>'fold',
                            'dateFormat'=> MyFormat::$dateFormatSearch,       
                            'maxDate'=> '0',
                            'changeMonth' => true,
                            'changeYear' => true,
                            'showOn' => 'button',
                            'buttonImage'=> Yii::app()->theme->baseUrl.'/admin/images/icon_calendar_r.gif',
                            'buttonImageOnly'=> true,                                
                        ),        
                        'htmlOptions'=>array(
                            'class'=>'w-16',
                            'size'=>'16',
                            'style'=>'height:20px;float:left;',
                            'readonly'=>'1',
                        ),
                    ));
                ?>     		
            </div>

            <div class="col2">
                <?php echo Yii::t('translation', $form->label($model,'date_to')); ?>
                <?php 
                    $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                        'model'=>$model,        
                        'attribute'=>'date_to',
                        'options'=>array(
                            'showAnim'=>'fold',
                            'dateFormat'=> MyFormat::$dateFormatSearch,       
                            'maxDate'=> '0',
                            'changeMonth' => true,
                            'changeYear' => true,
                            'showOn' => 'button',
                            'buttonImage'=> Yii::app()->theme->baseUrl.'/admin/images/icon_calendar_r.gif',
                            'buttonImageOnly'=> true,                                
                        ),        
                        'htmlOptions'=>array(
                            'class'=>'w-16',
                            'size'=>'16',
                            'style'=>'height:20px;float:left;',
                            'readonly'=>'1',
                        ),
                    ));
                ?>    
            </div>
            
            <div class="col3">
                <?php echo $form->label($model,'type'); ?>
                <?php echo $form->dropDownList($model,'type', $model->getTypeSearch(),array('style' => 'width:150px;')); ?>
            </div>
        </div>
        <div class="row more_col">
            <div class="col1">
		<?php echo $form->labelEx($model,'sale_id', array('label'=>'GĐKV')); ?>
		<?php echo $form->dropDownList($model,'sale_id', $model->getArrayMonitor(),array('empty'=>'select','style' => 'width:150px;')); ?>
            </div>
            <div class="col2">
                <?php $mOneMany = new GasOneMany; ?>
                <?php echo $form->labelEx($model,'monitoring_id',array('label'=>'Xem CV PTTT','class'=>'checkbox_one_label', 'style'=>'padding-top:3px;')); ?>
                <?php echo $form->dropDownList($model,'monitoring_id', $mOneMany->getListdataByType(GasOneMany::TYPE_MONITOR_GDKV), array('style'=>'','class'=>'w-200','empty'=>'Select')); ?>
            </div>
	</div>
        <div class="row buttons" style="padding-left: 159px; margin:20px 0;">
                <?php $this->widget('bootstrap.widgets.TbButton', array(
                'buttonType'=>'submit',
                'label'=>Yii::t('translation','Xem Thống Kê'),
                'type'=>'null', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
                'size'=>'small', // null, 'large', 'small' or 'mini'
                //'htmlOptions' => array('style' => 'margin-bottom: 10px; float: right;'),
            )); ?>
            <?php if($model->type != GasTargetMonthly::TYPE_GDKV):?>
            &nbsp;&nbsp;&nbsp;&nbsp;<a class='btn_cancel' target="_blank" href='<?php echo $urlExcel;?>'>Xuất Excel</a>
            <?php endif; ?>
        </div>

    <?php $this->endWidget(); ?>

    </div><!-- wide form -->
</div><!-- search-form -->
<?php if($model->type == GasTargetMonthly::TYPE_CCS):?>
<div id="tabs" class="grid-view">
    <ul>
        <li>
            <a class="tab_2" href="#tabs-2">Giao diện 1</a>
        </li>
        <li>
            <a class="tab_1" href="#tabs-1">Giao diện 2</a>
        </li>
    </ul>
    <?php include 'targetForm.php'; ?>
    <?php include 'targetFormV2.php'; ?>
</div>
<?php elseif($model->type == GasTargetMonthly::TYPE_GDKV):?>
<div id="tabs" class="grid-view">
    <ul>
<!--        <li>
            <a class="tab_2" href="#tabs-2">Giao diện 1</a>
        </li>-->
        <li>
            <a class="tab_1" href="#tabs-1">Giao diện 2</a>
        </li>
    </ul>
    <?php include 'targetCvGs.php'; ?>
    <?php // include 'targetCvGsV2.php'; ?>
</div>
<?php endif; ?>
<script>
$(document).ready(function() {
    $( "#tabs" ).tabs();
//    $( "#tabs" ).tabs({ active: 1 });
});
</script>