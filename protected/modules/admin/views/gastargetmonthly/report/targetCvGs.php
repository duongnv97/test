<?php
$MONITOR_GDKV       = isset($aData['MONITOR_GDKV']) ? $aData['MONITOR_GDKV'] : [];
$USER               = isset($aData['USER']) ? $aData['USER'] : [];
$PROVINCE           = isset($aData['PROVINCE']) ? $aData['PROVINCE'] : [];
$TARGET_DAILY       = isset($aData['TARGET_DAILY']) ? $aData['TARGET_DAILY'] : [];
$TARGET_BO          = isset($aData['BQV_BO']) ? $aData['BQV_BO'] : [];
$TARGET_MOI         = isset($aData['BQV_MOI']) ? $aData['BQV_MOI'] : [];
$TARGET_SETUP       = isset($aData['TARGET_SETUP']) ? $aData['TARGET_SETUP'] : [];
$aTelesale          = $model->getArrayTelesaleMonitor();
?>
<div id="tabs-1">
<br>
<?php if(!empty($MONITOR_GDKV)): ?>
<div class="clearfix box_help_color">
    <div class="color_type_1 color_note"></div>&nbsp;&nbsp; Hộ (kg)
    <div class="clr"></div>
    <div class="color_type_2 color_note"></div>&nbsp;&nbsp; Bò (kg)
    <div class="clr"></div>
    <div class="color_type_3 color_note"></div>&nbsp;&nbsp; Mối (kg)
    <div class="clr"></div>
    <div class="box_350 box_ccs_view_gdkv_all "></div>
</div>
<?php 
    $bqv_sum_gdkv_all   = 0;
    $bqv_current_sum_gdkv_all = 0;
    $bo_gdkv_all = 0;
    $bo_current_gdkv_all = 0;
    $moi_gdkv_all = 0;
    $moi_current_gdkv_all = 0;
?>
<?php foreach ($MONITOR_GDKV as $monitor_gdkv_id => $aCcsId): ?>
<?php 
    if(in_array($monitor_gdkv_id, $aTelesale)){
        continue;
    }
    $bqv_sum_gdkv   = 0;
    $bqv_current_sum_gdkv = 0;
    $bo_gdkv = 0;
    $bo_current_gdkv = 0;
    $moi_gdkv = 0;
    $moi_current_gdkv = 0;
?>

<div class="gdkvGroup">
    <h1><?php echo isset($USER[$monitor_gdkv_id]) ? $USER[$monitor_gdkv_id]->first_name : ''; ?> </h1>
    <div class="box_350 box_ccs_view_gdkv"></div>
    <div class="clr"></div>
    <br><br><br>
    <?php foreach ($aCcsId as $user_id => $id): ?>
    <?php
    $bqv            = isset($TARGET_SETUP[$user_id]['app']) ? $TARGET_SETUP[$user_id]['app'] : 0;
    $bqv_current    = isset($TARGET_DAILY[$user_id]['bqv']) ? $TARGET_DAILY[$user_id]['bqv']: 0;
    $bo          = isset($TARGET_SETUP[$user_id]['bqv']) ? $TARGET_SETUP[$user_id]['bqv'] : 0;
    $bo_current  = isset($TARGET_BO[$user_id]) ? $TARGET_BO[$user_id] : 0;
    $moi          = isset($TARGET_SETUP[$user_id]['point']) ? $TARGET_SETUP[$user_id]['point'] : 0;
    $moi_current  = isset($TARGET_MOI[$user_id]) ? $TARGET_MOI[$user_id] : 0;
//    sum gdkv
    $bqv_sum_gdkv += $bqv;
    $bqv_current_sum_gdkv += $bqv_current;
    $bo_gdkv += $bo;
    $bo_current_gdkv += $bo_current;
    $moi_gdkv += $moi;
    $moi_current_gdkv += $moi_current;
    
    $bqv_current_sum_gdkv_all += $bqv_current;
    $bo_current_gdkv_all += $bo_current;
    $moi_current_gdkv_all += $moi_current;
    ?>
    <div class="box_350 box_ccs_hide">
        <table class="hm_table items">
            <tbody>
                <tr>
                    <td colspan="3" class="item_c item_b"><?php echo isset($PROVINCE[$user_id]) ? $PROVINCE[$user_id]->name : ''; ?></td>
                </tr>
                <tr>
                    <td class="item_c w-140">Target</td>
                    <td class="item_c w-140">Thực Tế</td>
                    <td class="item_c w-70">Tỷ Lệ</td>
                </tr>
                <tr class="color_type_1">
                    <td class="item_r">
                        <?php echo ActiveRecord::formatCurrency($bqv);?>
                    </td>
                    <td class="item_r">
                        <?php echo ActiveRecord::formatCurrency($bqv_current);?>
                    </td>
                    <td class="item_r">
                        <?php echo $bqv > 0 ? (int)($bqv_current*100/$bqv) : 100;?> %
                    </td>
                </tr>
                <tr class="color_type_2">
                    <td class="item_r">
                        <?php echo ActiveRecord::formatCurrency($bo);?>
                    </td>
                    <td class="item_r">
                        <?php echo ActiveRecord::formatCurrency($bo_current);?>
                    </td>
                    <td class="item_r">
                        <?php echo $bo > 0 ? (int)($bo_current*100/$bo) : 100;?> %
                    </td>
                </tr>
                <tr class="color_type_3">
                    <td class="item_r">
                        <?php echo ActiveRecord::formatCurrency($moi);?>
                    </td>
                    <td class="item_r">
                        <?php echo ActiveRecord::formatCurrency($moi_current);?>
                    </td>
                    <td class="item_r">
                        <?php echo $moi > 0 ? (int)($moi_current*100/$moi) : 100;?> %
                    </td>
                </tr>
                <tr class="color_type_0">
                    <td class="item_r">
                        <?php echo ActiveRecord::formatCurrency($bqv + $bo + $moi);?>
                    </td>
                    <td class="item_r">
                        <?php echo ActiveRecord::formatCurrency($bqv_current + $bo_current + $moi_current);?>
                    </td>
                    <td class="item_r">
                        <?php echo ($bqv + $bo + $moi) > 0 ? (int)(($bqv_current + $bo_current + $moi_current)*100/($bqv + $bo + $moi)) : 100;?> %
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
    <?php endforeach; ?>
    <?php 
        $s_bqv_gdkv            = $bqv_sum_gdkv;
        $s_bo_gdkv             = $bo_gdkv;
        $s_moi_gdkv            = $moi_gdkv;
        $s_bqv_gdkv_all            += $bqv_sum_gdkv;
        $s_bo_gdkv_all             += $bo_gdkv;
        $s_moi_gdkv_all            += $moi_gdkv;
    ?>
    <div class="clr"></div>
    <div class="box_350 box_ccs_hide_gdkv display_none">
        <table class="hm_table items ">
            <tbody>
                <tr>
                    <td class="item_c w-140">Target</td>
                    <td class="item_c w-140">Thực Tế</td>
                    <td class="item_c w-70">Tỷ Lệ</td>
                </tr>
                <tr class="color_type_1">
                    <td class="item_r">
                        <?php echo ActiveRecord::formatCurrency($s_bqv_gdkv);?>
                    </td>
                    <td class="item_r">
                        <?php echo ActiveRecord::formatCurrency($bqv_current_sum_gdkv);?>
                    </td>
                    <td class="item_r">
                        <?php echo $s_bqv_gdkv > 0 ? (int)($bqv_current_sum_gdkv*100/$s_bqv_gdkv) : 100;?> %
                    </td>
                </tr>
                <tr class="color_type_2">
                    <td class="item_r">
                        <?php echo ActiveRecord::formatCurrency($s_bo_gdkv);?>
                    </td>
                    <td class="item_r">
                        <?php echo ActiveRecord::formatCurrency($bo_current_gdkv);?>
                    </td>
                    <td class="item_r">
                        <?php echo $s_bo_gdkv > 0 ? (int)($bo_current_gdkv*100/$s_bo_gdkv) : 100;?> %
                    </td>
                </tr>
                <tr class="color_type_3">
                    <td class="item_r">
                        <?php echo ActiveRecord::formatCurrency($s_moi_gdkv);?>
                    </td>
                    <td class="item_r">
                        <?php echo ActiveRecord::formatCurrency($moi_current_gdkv);?>
                    </td>
                    <td class="item_r">
                        <?php echo $s_moi_gdkv > 0 ? (int)($moi_current_gdkv*100/$s_moi_gdkv) : 100;?> %
                    </td>
                </tr>
                <tr class="color_type_0">
                    <td class="item_r">
                        <?php echo ActiveRecord::formatCurrency($s_bqv_gdkv + $s_bo_gdkv + $s_moi_gdkv);?>
                    </td>
                    <td class="item_r">
                        <?php echo ActiveRecord::formatCurrency($bqv_current_sum_gdkv + $bo_current_gdkv + $moi_current_gdkv);?>
                    </td>
                    <td class="item_r">
                        <?php echo ($s_bqv_gdkv + $s_bo_gdkv + $s_moi_gdkv) > 0 ? (int)(($bqv_current_sum_gdkv + $bo_current_gdkv + $moi_current_gdkv)*100/($s_bqv_gdkv + $s_bo_gdkv + $s_moi_gdkv)) : 100;?> %
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
</div>
<?php endforeach; ?>
<div class="clr"></div>
<div class="box_350 box_ccs_hide_gdkv_all display_none">
    <table class="hm_table items ">
        <tbody>
            <tr>
                <td class="item_c w-140">Target</td>
                <td class="item_c w-140">Thực Tế</td>
                <td class="item_c w-70">Tỷ Lệ</td>
            </tr>
            <tr class="color_type_1">
                <td class="item_r">
                    <?php echo ActiveRecord::formatCurrency($s_bqv_gdkv_all);?>
                </td>
                <td class="item_r">
                    <?php echo ActiveRecord::formatCurrency($bqv_current_sum_gdkv_all);?>
                </td>
                <td class="item_r">
                    <?php echo $s_bqv_gdkv_all > 0 ? (int)($bqv_current_sum_gdkv_all*100/$s_bqv_gdkv_all) : 100;?> %
                </td>
            </tr>
            <tr class="color_type_2">
                <td class="item_r">
                    <?php echo ActiveRecord::formatCurrency($s_bo_gdkv_all);?>
                </td>
                <td class="item_r">
                    <?php echo ActiveRecord::formatCurrency($bo_current_gdkv_all);?>
                </td>
                <td class="item_r">
                    <?php echo $s_bo_gdkv_all > 0 ? (int)($bo_current_gdkv_all*100/$s_bo_gdkv_all) : 100;?> %
                </td>
            </tr>
            <tr class="color_type_3">
                <td class="item_r">
                    <?php echo ActiveRecord::formatCurrency($s_moi_gdkv_all);?>
                </td>
                <td class="item_r">
                    <?php echo ActiveRecord::formatCurrency($moi_current_gdkv_all);?>
                </td>
                <td class="item_r">
                    <?php echo $s_moi_gdkv_all > 0 ? (int)($moi_current_gdkv_all*100/$s_moi_gdkv_all) : 100;?> %
                </td>
            </tr>
            <tr class="color_type_0">
                <td class="item_r">
                    <?php echo ActiveRecord::formatCurrency($s_bqv_gdkv_all + $s_bo_gdkv_all + $s_moi_gdkv_all);?>
                </td>
                <td class="item_r">
                    <?php echo ActiveRecord::formatCurrency($bqv_current_sum_gdkv_all + $bo_current_gdkv_all + $moi_current_gdkv_all);?>
                </td>
                <td class="item_r">
                    <?php echo ($s_bqv_gdkv_all + $s_bo_gdkv_all + $s_moi_gdkv_all) > 0 ? (int)(($bqv_current_sum_gdkv_all + $bo_current_gdkv_all + $moi_current_gdkv_all)*100/($s_bqv_gdkv_all + $s_bo_gdkv_all + $s_moi_gdkv_all)) : 100;?> %
                </td>
            </tr>
        </tbody>
    </table>
</div>
<?php endif; ?>
<script>
    $(document).ready(function(){
        $('.box_ccs_hide_gdkv').each(function(){
            $(this).closest('.gdkvGroup').find('.box_ccs_view_gdkv').html($(this).html());
        });
        $('.box_ccs_hide_gdkv_all').each(function(){
            $(this).closest('#tabs-1').find('.box_ccs_view_gdkv_all').html($(this).html());
        });
    });
</script>
</div>