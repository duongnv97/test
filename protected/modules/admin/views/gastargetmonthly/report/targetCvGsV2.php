<?php
$MONITOR            = isset($aData['MONITOR']) ? $aData['MONITOR'] : [];
$MONITOR_GDKV       = isset($aData['MONITOR_GDKV']) ? $aData['MONITOR_GDKV'] : [];
$USER               = isset($aData['USER']) ? $aData['USER'] : [];
$TARGET_DAILY       = isset($aData['TARGET_DAILY']) ? $aData['TARGET_DAILY'] : [];
$TARGET_BOMOI       = isset($aData['TARGET_BOMOI']) ? $aData['TARGET_BOMOI'] : [];
$TARGET_SETUP       = isset($aData['TARGET_SETUP']) ? $aData['TARGET_SETUP'] : [];
?>
<div id="tabs-2">
<br>
<?php if(!empty($MONITOR_GDKV)): ?>
<?php foreach ($MONITOR_GDKV as $monitor_gdkv_id => $aCcsId): ?>
<?php 
    $index          = 1;
    $bqv_sum_gdkv = 0;
    $bqv_current_sum_gdkv = 0;
    $boMoi_gdkv = 0;
    $boMoi_current_gdkv = 0;
?>

<div class="gdkvGroup">
    <h1><?php echo isset($USER[$monitor_gdkv_id]) ? $USER[$monitor_gdkv_id]->first_name : ''; ?> </h1>
    
    <table class="materials_table hm_table">
        <thead>
            <tr>
            <th class="item_b item_c" rowspan="2">STT</th>
                <th class="item_b item_c" rowspan="2">Tên nhân viên</th>
                <th class="item_b item_c" rowspan="2">Loại NV</th>
                <th class="item_b item_c" colspan="2">Target</th>
                <th class="item_b item_c" colspan="2">Thực tế</th>
                <th class="item_b item_c" colspan="2">tỷ lệ</th>
            </tr>
            <tr>
                <th class="item_b item_c">Hộ (kg)</th>
                <th class="item_b item_c">Mối+Bò (kg)</th>
                <th class="item_b item_c">Hộ (kg)</th>
                <th class="item_b item_c">Mối+Bò (kg)</th>
                <th class="item_b item_c">Hộ (kg)</th>
                <th class="item_b item_c">Mối+Bò (kg)</th>
            </tr>
        <tbody>
            <?php foreach ($aCcsId as $user_id => $id): ?>
            <?php
            $bqv            = isset($TARGET_SETUP[$user_id]['app']) ? $TARGET_SETUP[$user_id]['app'] : 0;
            $bqv_current    = isset($TARGET_DAILY[$user_id]['bqv']) ? $TARGET_DAILY[$user_id]['bqv'] * 12 : 0;
            $namePosition   = isset($USER[$user_id]) && $USER[$user_id]->gender == Users::SALE_PTTT_KD_KHU_VUC ? 'GSKV ' : 'CV PTTT ';
            $boMoi          = isset($TARGET_SETUP[$user_id]['bqv']) ? $TARGET_SETUP[$user_id]['bqv'] : 0;
            $boMoi_current  = isset($TARGET_BOMOI[$user_id]) ? $TARGET_BOMOI[$user_id] : 0;
        //    sum gdkv
            $bqv_sum_gdkv += $bqv;
            $bqv_current_sum_gdkv += $bqv_current;

            if(isset($USER[$user_id]) && $USER[$user_id]->gender == Users::SALE_PTTT_KD_KHU_VUC ){
                $boMoi_gdkv += $boMoi;
                $boMoi_current_gdkv += $boMoi_current;
            }
            ?>
            <tr>
                <td class="item_c"><?php echo $index++; ?></td>
                <td class="item_l"><?php echo $namePosition;echo isset($USER[$user_id]) ? $USER[$user_id]->first_name : ''; ?></td>
                <td class="item_l">
                    <?php 
                    $firstName = '';
                    if(isset($USER[$user_id])){
                        if($USER[$user_id]->gender == Users::SALE_MOI_PTTT){
                            $firstName = 'PTTT';
                        }elseif($USER[$user_id]->gender == Users::SALE_PTTT_KD_KHU_VUC){
                            $firstName = 'CCS';
                        }
                    }
                    echo $firstName;
                    ?>
                </td>
                <td class="item_c"><?php echo ActiveRecord::formatCurrency($bqv);?></td>
                <td class="item_c">
                    <?php if(isset($USER[$user_id]) && $USER[$user_id]->gender == Users::SALE_PTTT_KD_KHU_VUC ): ?>
                    <?php echo ActiveRecord::formatCurrency($boMoi);?>
                    <?php endif; ?>
                </td>
                <td class="item_c"><?php echo ActiveRecord::formatCurrency($bqv_current);?></td>
                <td class="item_c">
                    <?php if(isset($USER[$user_id]) && $USER[$user_id]->gender == Users::SALE_PTTT_KD_KHU_VUC ): ?>
                    <?php echo ActiveRecord::formatCurrency($boMoi_current);?>
                    <?php endif; ?>
                </td>
                <td class="item_c"><?php echo $bqv > 0 ? (int)($bqv_current*100/$bqv) : 100;?> %</td>
                <td class="item_c">
                    <?php if(isset($USER[$user_id]) && $USER[$user_id]->gender == Users::SALE_PTTT_KD_KHU_VUC ): ?>
                    <?php echo $boMoi > 0 ? (int)($boMoi_current*100/$boMoi) : 100;?> %
                    <?php endif; ?>
                </td>
            </tr>
            <?php endforeach; ?>
            <tr>
                <td class="item_b item_c" colspan="3">Tổng</td>
                <td class="item_b item_c"><?php echo ActiveRecord::formatCurrency($bqv_sum_gdkv);?></td>
                <td class="item_b item_c">
                    <?php echo ActiveRecord::formatCurrency($boMoi_gdkv);?>
                </td>
                <td class="item_b item_c"><?php echo ActiveRecord::formatCurrency($bqv_current_sum_gdkv);?></td>
                <td class="item_b item_c">
                    <?php echo ActiveRecord::formatCurrency($boMoi_current_gdkv);?>
                </td>
                <td class="item_b item_c"><?php echo $bqv_sum_gdkv > 0 ? (int)($bqv_current_sum_gdkv*100/$bqv_sum_gdkv) : 100;?> %</td>
                <td class="item_b item_c">
                    <?php echo $boMoi_gdkv > 0 ? (int)($boMoi_current_gdkv*100/$boMoi_gdkv) : 100;?> %
                </td>
            </tr>
        </tbody>
        </thead>
    </table>

</div>
<?php endforeach; ?>
<?php endif; ?>
<script>
    $(document).ready(function(){
        $('.box_ccs_hide_gdkv').each(function(){
            $(this).closest('.gdkvGroup').find('.box_ccs_view_gdkv').html($(this).html());
        });
    });
</script>
</div>