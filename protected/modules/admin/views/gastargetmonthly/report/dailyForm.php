<?php
$POINT_APP          = isset($aData['POINT_APP']) ? $aData['POINT_APP'] : [];
$BQV                = isset($aData['BQV']) ? $aData['BQV'] : [];
$DATE               = isset($aData['DATE']) ? $aData['DATE'] : [];
$EMPLOYEE           = isset($aData['EMPLOYEE']) ? $aData['EMPLOYEE'] : [];
$EMPLOYEE_MODEL     = isset($aData['EMPLOYEE_MODEL']) ? $aData['EMPLOYEE_MODEL'] : [];
$EMPLOYEE_MONITOR   = isset($aData['EMPLOYEE_MONITOR']) ? $aData['EMPLOYEE_MONITOR'] : [];

$gridCss = Yii::app()->getAssetManager()->publish(Yii::getPathOfAlias('zii.widgets.assets')) . '/gridview/styles.css';
Yii::app()->getClientScript()->registerCssFile($gridCss);
$index = 1;
?>
<script type="text/javascript"
        src="<?php echo Yii::app()->theme->baseUrl; ?>/js/scroll/jquery.freezetablecolumns.1.1.js"></script>
        
<?php if(!empty($POINT_APP) ||  !empty($BQV)): ?>
<?php 
$point_sum  = 0;
$app_sum    = 0;
$bqv_sum    = 0;
$sum_date   = [];
?>
<p>
    Chú thích: Số điểm PTTT / App / Bqv
</p>
<div class="grid-view display_none">
    <table id="freezetablecolumns_report" class="items hm_table freezetablecolumns">
        <thead>
        <tr  class="h_50">
            <th class="w-20 item_b">#</th>
            <th class="w-150 item_b">Nhân viên</th>
            <th class="w-50 item_b">Loại NV</th>
            <th class="w-150 item_b">Chuyên viên</th>
            <th class="w-100 item_b">Tổng</th>
            <th class="w-100 item_b">Doanh thu</th>
            <?php foreach ($DATE as $days): ?>
                <th class="item_b w-50"><?php echo substr($days, -2);?></th>
            <?php endforeach; ?>
        </tr>
        </thead>
        <tbody>
            <tr  class="h_50 showHeader">
            </tr>
            <?php foreach ($EMPLOYEE as $employee_id): ?>
            <?php
            if(empty($employee_id)){ continue ;}
            $point_sum_current  = 0;
            $app_sum_current    = 0;
            $bqv_sum_current    = 0;
            $monitor_id         = isset($EMPLOYEE_MONITOR[$employee_id]) ? $EMPLOYEE_MONITOR[$employee_id] : 0;
            if(empty($monitor_id)){ continue ;}
            if(empty($POINT_APP[$employee_id]) && empty($BQV[$employee_id])){
                continue;
            }
            ?>
            <tr class="tr_item_show">
                <td class="item_c"><?php echo $index++;?></td>
                <td class="item_b item_l">
                    <?php echo !empty($EMPLOYEE_MODEL[$employee_id]) ? $EMPLOYEE_MODEL[$employee_id]->first_name : $employee_id ;?>
                </td>
                <td class="item_b item_l">
                    <?php 
                    $firstName = '';
                    if(isset($EMPLOYEE_MODEL[$employee_id])){
                        if($EMPLOYEE_MODEL[$employee_id]->gender == Users::SALE_MOI_PTTT){
                            $firstName = 'PTTT';
                        }elseif($EMPLOYEE_MODEL[$employee_id]->gender == Users::SALE_PTTT_KD_KHU_VUC){
                            $firstName = 'CCS';
                        }
                    }
                    echo $firstName;
                    ?>
                </td>
                <td class="item_b item_l"><?php echo !empty($EMPLOYEE_MODEL[$monitor_id]) ? $EMPLOYEE_MODEL[$monitor_id]->first_name : $monitor_id ;?></td>
                <td class="item_b item_c item_show"></td>
                <td class="item_b item_r"><?php echo !empty($BQV['GRAND_TOTAL'][$employee_id]) ? ActiveRecord::formatCurrency($BQV['GRAND_TOTAL'][$employee_id]) : '' ?></td>
                <?php foreach ($DATE as $days): ?>
                <?php
                    $point              = isset($POINT_APP[$employee_id][$days]['point']) ? $POINT_APP[$employee_id][$days]['point'] : 0;
                    $app                = isset($POINT_APP[$employee_id][$days]['app']) ? $POINT_APP[$employee_id][$days]['app'] : 0;
                    $bqv                = isset($BQV[$employee_id][$days]['bqv']) ? $BQV[$employee_id][$days]['bqv'] : 0;
                    $point_sum          += $point;
                    $app_sum            += $app;
                    $bqv_sum            += $bqv;
                    $point_sum_current  += $point;
                    $app_sum_current    += $app;
                    $bqv_sum_current    += $bqv;
                    if(isset($sum_date[$days])){
                        $sum_date[$days]['point']       += $point;
                        $sum_date[$days]['app']         += $app;
                        $sum_date[$days]['bqv']         += $bqv;
                    }else{
                        $sum_date[$days]['point']       = $point;
                        $sum_date[$days]['app']         = $app;
                        $sum_date[$days]['bqv']         = $bqv;
                    }
                    $textShow   = $point . '/' . $app . '/' . $bqv;
                    if($textShow == '0/0/0'){
                        $textShow = '-';
                    }
                ?>
                <td class="item_c"><?php echo $textShow;?></td>
                <?php endforeach; ?>
                <td class="item_b item_c item_hidden display_none">
                    <?php 
                    $textShow   = $point_sum_current . '/' . $app_sum_current . '/' . $bqv_sum_current;
                    if($textShow == '0/0/0'){
                        $textShow = '';
                    }
                    echo $textShow;
                    ?>
                </td>
            </tr>
            <?php endforeach; ?>
            <tr  class="h_50 hiddenHeader display_none">
                <td></td>
                <td></td>
                <td></td>
                <td class="item_c item_b">Tổng</td>
                <td class="item_c item_b">
                    <?php 
                    $textShow   = $point_sum . '/' . $app_sum . '/' . $bqv_sum;
                    if($textShow == '0/0/0'){
                        $textShow = '';
                    }
                    echo $textShow;
                    ?>
                </td>
                <td class="item_r item_b">
                <?php echo is_array($BQV['GRAND_TOTAL']) ? ActiveRecord::formatCurrency(array_sum($BQV['GRAND_TOTAL'])) : '' ?>
                </td>
                <?php foreach ($DATE as $days): ?>
                <td class="item_b item_c">
                <?php 
                    if(isset($sum_date[$days])){
                        $textShow   = $sum_date[$days]['point'] . '/' . $sum_date[$days]['app'] . '/' . $sum_date[$days]['bqv'];
                        if($textShow == '0/0/0'){
                            $textShow = '';
                        }
                        echo $textShow;
                    }
                ?>
                </td>
                <?php endforeach; ?>
            </tr>
        </tbody>
    </table>
</div>
<script>
    $(document).ready(function (){
        $('.showHeader').each(function(){
            $(this).html($(this).closest('.hm_table').find('.hiddenHeader').html());
        });
        $('.item_show').each(function(){
            $(this).html($(this).closest('.tr_item_show').find('.item_hidden').html());
        });
        $('#freezetablecolumns_report').freezeTableColumns({
            width:       <?php echo Users::WIDTH_freezetable;?>,   // required - bt 1100
            height:      400,   // required
            numFrozen:  6,     // optional
            frozenWidth: 630,   // optional
            clearWidths: true  // optional
        });
        $(window).load(function () {
            var index = 1;
            $('.freezetablecolumns').each(function () {
                if (index == 1)
                    $(this).closest('div.grid-view').show();
                index++;
            });
            fnAddClassOddEven('items');
        });
    });
</script>
<?php endif; ?>
        