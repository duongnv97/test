
<!--              SEARCH FORM              -->
<div class="search-form" style="">
    <div class="wide form">

    <?php $form=$this->beginWidget('CActiveForm', array(
            'action'=>GasCheck::getCurl(),
            'method'=>'post',
    )); ?>
        
        <div class="row more_col">
            <div class="col1">
		<?php echo $form->labelEx($model,'sale_id', array('label'=>'GĐKV')); ?>
		<?php echo $form->dropDownList($model,'sale_id', $model->getArrayMonitor(),array('empty'=>'select','style' => 'width:150px;')); ?>
            </div>
	</div>
        <div class="row buttons" style="padding-left: 159px; margin:20px 0;">
                <?php $this->widget('bootstrap.widgets.TbButton', array(
                'buttonType'=>'submit',
                'label'=>Yii::t('translation','Xem Thống Kê'),
                'type'=>'null', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
                'size'=>'small', // null, 'large', 'small' or 'mini'
                //'htmlOptions' => array('style' => 'margin-bottom: 10px; float: right;'),
            )); ?>
        </div>

    <?php $this->endWidget(); ?>

    </div><!-- wide form -->
</div>
<!--              END SEARCH FORM              -->

<?php
if(empty($aData)) return;
$aMonth                 = array_keys($aData);
$prevMonth              = $aMonth[0];
$currMonth              = $aMonth[1];
$DISPLAY                = [];
$colorBonus             = '#9affde';
$colorNoBonus           = '#eca1a1';
$colorHighest           = '#fbff00';

// MONTH 1
$MONITOR                = isset($aData[$prevMonth]['MONITOR']) ? $aData[$prevMonth]['MONITOR'] : [];
$MONITOR_GDKV           = isset($aData[$prevMonth]['MONITOR_GDKV']) ? $aData[$prevMonth]['MONITOR_GDKV'] : [];
$USER                   = isset($aData[$prevMonth]['USER']) ? $aData[$prevMonth]['USER'] : [];
$TARGET_DAILY           = isset($aData[$prevMonth]['TARGET_DAILY']) ? $aData[$prevMonth]['TARGET_DAILY'] : [];
$TARGET_BQV             = isset($aData[$prevMonth]['TARGET_BQV']) ? $aData[$prevMonth]['TARGET_BQV'] : [];
$TARGET_BQV_SHARE       = isset($aData[$prevMonth]['TARGET_SHARE']) ? $aData[$prevMonth]['TARGET_SHARE'] : [];
$TARGET_BQV_V2          = isset($aData[$prevMonth]['TARGET_BQV_V2']) ? $aData[$prevMonth]['TARGET_BQV_V2'] : [];
$TARGET_SETUP           = isset($aData[$prevMonth]['TARGET_SETUP']) ? $aData[$prevMonth]['TARGET_SETUP'] : [];
$TARGET_SETUP_GSKV      = isset($aData[$prevMonth]['TARGET_SETUP_GSKV']) ? $aData[$prevMonth]['TARGET_SETUP_GSKV'] : [];
$IS_APPLY_GSKV          = isset($aData[$prevMonth]['IS_APPLY_GSKV']) ? $aData[$prevMonth]['IS_APPLY_GSKV'] : [];

// MONTH 2
$__MONITOR              = isset($aData[$currMonth]['MONITOR']) ? $aData[$currMonth]['MONITOR'] : [];
$__MONITOR_GDKV         = isset($aData[$currMonth]['MONITOR_GDKV']) ? $aData[$currMonth]['MONITOR_GDKV'] : [];
$__USER                 = isset($aData[$currMonth]['USER']) ? $aData[$currMonth]['USER'] : [];
$__TARGET_DAILY         = isset($aData[$currMonth]['TARGET_DAILY']) ? $aData[$currMonth]['TARGET_DAILY'] : [];
$__TARGET_BQV           = isset($aData[$currMonth]['TARGET_BQV']) ? $aData[$currMonth]['TARGET_BQV'] : [];
$__TARGET_BQV_SHARE     = isset($aData[$currMonth]['TARGET_SHARE']) ? $aData[$currMonth]['TARGET_SHARE'] : [];
$__TARGET_BQV_V2        = isset($aData[$currMonth]['TARGET_BQV_V2']) ? $aData[$currMonth]['TARGET_BQV_V2'] : [];
$__TARGET_SETUP         = isset($aData[$currMonth]['TARGET_SETUP']) ? $aData[$currMonth]['TARGET_SETUP'] : [];
$__TARGET_SETUP_GSKV    = isset($aData[$currMonth]['TARGET_SETUP_GSKV']) ? $aData[$currMonth]['TARGET_SETUP_GSKV'] : [];
$__IS_APPLY_GSKV        = isset($aData[$currMonth]['IS_APPLY_GSKV']) ? $aData[$currMonth]['IS_APPLY_GSKV'] : [];
$mGasTargetMonthlyCal   = new GasTargetMonthly();
$mGasTargetMonthlyCal->date_from = $model->date_from;
$USER_ALL               = $USER + $__USER;
//// merge 2d array $MONITOR_GDKV keep key
//$MONITOR_GDKV       = [];
//foreach ($_MONITOR_GDKV as $monitor_gdkv_id => $aCcsId) {
//    if( isset($__MONITOR_GDKV[$monitor_gdkv_id]) ){
//        $MONITOR_GDKV[$monitor_gdkv_id] = $__MONITOR_GDKV[$monitor_gdkv_id] + $_MONITOR_GDKV[$monitor_gdkv_id];
//    }
//}
//$aDiffMonitorGdkv = array_diff(array_keys($__MONITOR_GDKV), array_keys($_MONITOR_GDKV));
//foreach ($aDiffMonitorGdkv as $monitor_gdkv_id) {
//    $MONITOR_GDKV[$monitor_gdkv_id] = $__MONITOR_GDKV[$monitor_gdkv_id];
//}
?>


<?php 
//--------------------------------------PREV MONTH
if(!empty($MONITOR_GDKV)):
    foreach ($MONITOR_GDKV as $monitor_gdkv_id => $aCcsId):
        //prev month
        $point_sum_gdkv         = 0;
        $point_current_sum_gdkv = 0;
        $app_sum_gdkv           = 0;
        $app_current_sum_gdkv   = 0;
        $bqv_sum_gdkv           = 0;
        $bqv_current_sum_gdkv   = 0;
        $bqv_current_sum_gdkv_v2= 0;
        foreach ($aCcsId as $monitor_id => $monitor_id_v2):
            if(empty($MONITOR[$monitor_id])) continue;
            $aEmployeeId        = $MONITOR[$monitor_id];
            // prev month
            $point_sum          = 0;
            $point_current_sum  = 0;
            $app_sum            = 0;
            $app_current_sum    = 0;
            $bqv_sum            = 0;
            $bqv_current_sum    = 0;
            $bqv_current_sum_v2 = 0;
            $bqv_gskv           = 0;
            $bqv_gskv_v2        = 0;
            foreach ($aEmployeeId as $user_id => $id):
    //            gas remain
                $current_remain     = !empty($TARGET_BQV['GAS_REMAIN'][$user_id]) ? $TARGET_BQV['GAS_REMAIN'][$user_id] : 0;
                $current_remain_v2  = !empty($TARGET_BQV_V2['GAS_REMAIN'][$user_id]) ? $TARGET_BQV_V2['GAS_REMAIN'][$user_id] : 0;
                $point              = isset($TARGET_SETUP[$user_id]['point']) ? $TARGET_SETUP[$user_id]['point'] : 0;
                $point_current      = isset($TARGET_DAILY[$user_id]['point']) ? $TARGET_DAILY[$user_id]['point'] : 0;
                $app                = isset($TARGET_SETUP[$user_id]['app']) ? $TARGET_SETUP[$user_id]['app'] : 0;
                $app_current        = isset($TARGET_DAILY[$user_id]['app']) ? $TARGET_DAILY[$user_id]['app'] : 0;
                $bqv                = isset($TARGET_SETUP[$user_id]['bqv']) ? $TARGET_SETUP[$user_id]['bqv'] : 0;
                $bqv_current        = isset($TARGET_BQV[$user_id]['bqv']) ? $TARGET_BQV[$user_id]['bqv'] : 0;
                $bqv_current        = $mGasTargetMonthlyCal->getKgBQV($bqv_current,$current_remain);
                $bqv_current_v2     = isset($TARGET_BQV_V2[$user_id]['bqv']) ? $TARGET_BQV_V2[$user_id]['bqv'] : 0;
                $bqv_current_v2     = $mGasTargetMonthlyCal->getKgBQV($bqv_current_v2,$current_remain_v2);
    //            sum
                $point_sum              += $point;
                $point_current_sum      += $point_current;
                $app_sum                += $app;
                $app_current_sum        += $app_current;
                $bqv_sum                += $bqv;
                $bqv_current_sum        += $bqv_current;
                $bqv_current_sum_v2     += $bqv_current_v2;
    //            sum gdkv
                $point_sum_gdkv         += $point;
                $point_current_sum_gdkv += $point_current;
                $app_sum_gdkv           += $app;
                $app_current_sum_gdkv   += $app_current;
                $bqv_sum_gdkv           += $bqv;
                $bqv_current_sum_gdkv   += $bqv_current;
                $bqv_current_sum_gdkv_v2+= $bqv_current_v2;
    //            row ccs employee
                $sApp   = $mGasTargetMonthlyCal->getRateBQV([$point_current,$app_current],[$point,$app]);
                $moreCurrent = 0;
                if($mGasTargetMonthlyCal->isApplyShare()){
                    $moreCurrent = $bqv_current_share;
                }
                $sBQV   = $mGasTargetMonthlyCal->getRateBQV([$moreCurrent,$bqv_current,$bqv_current_v2],[$bqv]);
                $sBonus = $mGasTargetMonthlyCal->getRateBonus($sApp,$sBQV);
                $DISPLAY['CCS'][$user_id][$prevMonth] = $sBonus;
                $DISPLAY['CCS_BY_MONTH'][$monitor_id][$prevMonth][$user_id] = $sBonus;
    //            end row ccs employee
                if(in_array($user_id, $IS_APPLY_GSKV) || $sBonus >= GasTargetMonthly::PRTCENT_BONUS_PTTT){
                    $bqv_gskv           += $bqv_current;
                    $bqv_gskv_v2        += $bqv_current_v2;
                }
            endforeach;
//            <!--ROW SUM CCS-->
            $sApp   = $mGasTargetMonthlyCal->getRateBQV([$point_current_sum,$app_current_sum],[$point_sum,$app_sum]);
            $moreCurrent = 0;
            if($mGasTargetMonthlyCal->isApplyShare()){
                $moreCurrent = $bqv_current_sum_share;
            }
            $sBQV   = $mGasTargetMonthlyCal->getRateBQV([$moreCurrent,$bqv_current_sum,$bqv_current_sum_v2],[$bqv_sum]);
            $sBonus = $mGasTargetMonthlyCal->getRateBonus($sApp,$sBQV);
            $DISPLAY['SUM_CCS'][$monitor_id][$prevMonth] = $sBonus;
//            <!--END ROW SUM CCS-->
//            <!--ROW SUM GS-->
            $target_gskv    = isset($TARGET_SETUP_GSKV[$monitor_id]['app']) ? $TARGET_SETUP_GSKV[$monitor_id]['app'] : 0;
            $moreCurrent = 0;
            if($mGasTargetMonthlyCal->isApplyShare()){
                $moreCurrent = $bqv_gskv_share;
            }
            $sBonus         = $mGasTargetMonthlyCal->getRateBQV([$moreCurrent,$bqv_gskv,$bqv_gskv_v2],[$target_gskv]);
            $DISPLAY['SUM_GS'][$monitor_id][$prevMonth] = $sBonus;
//            <!--END ROW SUM GS-->
        endforeach;
//        <!--ROW SUM GDKV-->
        $sApp   = $mGasTargetMonthlyCal->getRateBQV([$point_current_sum_gdkv,$app_current_sum_gdkv],[$point_sum_gdkv,$app_sum_gdkv]);
        $moreCurrent = 0;
        if($mGasTargetMonthlyCal->isApplyShare()){
            $moreCurrent = $bqv_current_sum_gdkv_share;
        }
        $sBQV   = $mGasTargetMonthlyCal->getRateBQV([$moreCurrent,$bqv_current_sum_gdkv,$bqv_current_sum_gdkv_v2],[$bqv_sum_gdkv]);
        $sBonus = $mGasTargetMonthlyCal->getRateBonus($sApp,$sBQV);
        $DISPLAY['SUM_GDKV'][$monitor_gdkv_id][$prevMonth] = $sBonus;
//        <!--END ROW SUM GDKV-->
    endforeach;
endif;
?>




<?php 
//--------------------------------------CURRENT MONTH
if(!empty($__MONITOR_GDKV)):
    foreach ($__MONITOR_GDKV as $__monitor_gdkv_id => $__aCcsId):
        //prev month
        $__point_sum_gdkv         = 0;
        $__point_current_sum_gdkv = 0;
        $__app_sum_gdkv           = 0;
        $__app_current_sum_gdkv   = 0;
        $__bqv_sum_gdkv           = 0;
        $__bqv_current_sum_gdkv   = 0;
        $__bqv_current_sum_gdkv_v2= 0;
        foreach ($__aCcsId as $__monitor_id => $__monitor_id_v2):
            if(empty($__MONITOR[$__monitor_id])) continue;
            $__aEmployeeId        = $__MONITOR[$__monitor_id];
            // prev month
            $__point_sum          = 0;
            $__point_current_sum  = 0;
            $__app_sum            = 0;
            $__app_current_sum    = 0;
            $__bqv_sum            = 0;
            $__bqv_current_sum    = 0;
            $__bqv_current_sum_v2 = 0;
            $__bqv_gskv           = 0;
            $__bqv_gskv_v2        = 0;
            foreach ($__aEmployeeId as $__user_id => $__id):
    //            gas remain
                $__current_remain     = !empty($__TARGET_BQV['GAS_REMAIN'][$__user_id]) ? $__TARGET_BQV['GAS_REMAIN'][$__user_id] : 0;
                $__current_remain_v2  = !empty($__TARGET_BQV_V2['GAS_REMAIN'][$__user_id]) ? $__TARGET_BQV_V2['GAS_REMAIN'][$__user_id] : 0;
                $__point              = isset($__TARGET_SETUP[$__user_id]['point']) ? $__TARGET_SETUP[$__user_id]['point'] : 0;
                $__point_current      = isset($__TARGET_DAILY[$__user_id]['point']) ? $__TARGET_DAILY[$__user_id]['point'] : 0;
                $__app                = isset($__TARGET_SETUP[$__user_id]['app']) ? $__TARGET_SETUP[$__user_id]['app'] : 0;
                $__app_current        = isset($__TARGET_DAILY[$__user_id]['app']) ? $__TARGET_DAILY[$__user_id]['app'] : 0;
                $__bqv                = isset($__TARGET_SETUP[$__user_id]['bqv']) ? $__TARGET_SETUP[$__user_id]['bqv'] : 0;
                $__bqv_current        = isset($__TARGET_BQV[$__user_id]['bqv']) ? $__TARGET_BQV[$__user_id]['bqv'] : 0;
                $__bqv_current        = $mGasTargetMonthlyCal->getKgBQV($__bqv_current,$__current_remain);
                $__bqv_current_v2     = isset($__TARGET_BQV_V2[$__user_id]['bqv']) ? $__TARGET_BQV_V2[$__user_id]['bqv'] : 0;
                $__bqv_current_v2     = $mGasTargetMonthlyCal->getKgBQV($__bqv_current_v2,$__current_remain_v2);
    //            sum
                $__point_sum              += $__point;
                $__point_current_sum      += $__point_current;
                $__app_sum                += $__app;
                $__app_current_sum        += $__app_current;
                $__bqv_sum                += $__bqv;
                $__bqv_current_sum        += $__bqv_current;
                $__bqv_current_sum_v2     += $__bqv_current_v2;
    //            sum gdkv
                $__point_sum_gdkv         += $__point;
                $__point_current_sum_gdkv += $__point_current;
                $__app_sum_gdkv           += $__app;
                $__app_current_sum_gdkv   += $__app_current;
                $__bqv_sum_gdkv           += $__bqv;
                $__bqv_current_sum_gdkv   += $__bqv_current;
                $__bqv_current_sum_gdkv_v2+= $__bqv_current_v2;
    //            row ccs employee
                $__sApp   = $mGasTargetMonthlyCal->getRateBQV([$__point_current,$__app_current],[$__point,$__app]);
                $__sBQV   = $mGasTargetMonthlyCal->getRateBQV([$__bqv_current,$__bqv_current_v2],[$__bqv]);
                $__sBonus = $mGasTargetMonthlyCal->getRateBonus($__sApp,$__sBQV);
                $DISPLAY['CCS'][$__user_id][$currMonth] = $__sBonus;
                $DISPLAY['CCS_BY_MONTH'][$__monitor_id][$currMonth][$__user_id] = $__sBonus;
    //            end row ccs employee
                if(in_array($__user_id, $__IS_APPLY_GSKV) || $__sBonus >= GasTargetMonthly::PRTCENT_BONUS_PTTT){
                    $__bqv_gskv           += $__bqv_current;
                    $__bqv_gskv_v2        += $__bqv_current_v2;
                }
            endforeach;
//            <!--ROW SUM CCS-->
            $__sApp   = $mGasTargetMonthlyCal->getRateBQV([$__point_current_sum,$__app_current_sum],[$__point_sum,$__app_sum]);
            $__sBQV   = $mGasTargetMonthlyCal->getRateBQV([$__bqv_current_sum,$__bqv_current_sum_v2],[$__bqv_sum]);
            $__sBonus = $mGasTargetMonthlyCal->getRateBonus($__sApp,$__sBQV);
            $DISPLAY['SUM_CCS'][$__monitor_id][$currMonth] = $__sBonus;
//            <!--END ROW SUM CCS-->
//            <!--ROW SUM GS-->
            $__target_gskv    = isset($__TARGET_SETUP_GSKV[$__monitor_id]['app']) ? $__TARGET_SETUP_GSKV[$__monitor_id]['app'] : 0;
            $__sBonus         = $mGasTargetMonthlyCal->getRateBQV([$__bqv_gskv,$__bqv_gskv_v2],[$__target_gskv]);
            $DISPLAY['SUM_GS'][$__monitor_id][$currMonth] = $__sBonus;
//            <!--END ROW SUM GS-->
        endforeach;
//        <!--ROW SUM GDKV-->
        $__sApp   = $mGasTargetMonthlyCal->getRateBQV([$__point_current_sum_gdkv,$__app_current_sum_gdkv],[$__point_sum_gdkv,$__app_sum_gdkv]);
        $__sBQV   = $mGasTargetMonthlyCal->getRateBQV([$__bqv_current_sum_gdkv,$__bqv_current_sum_gdkv_v2],[$__bqv_sum_gdkv]);
        $__sBonus = $mGasTargetMonthlyCal->getRateBonus($__sApp,$__sBQV);
        $DISPLAY['SUM_GDKV'][$__monitor_gdkv_id][$currMonth] = $__sBonus;
//        <!--END ROW SUM GDKV-->
    endforeach;
endif;
?>







<!--                      TABLE CURRENT MONTH               -->
<div id="tabs-2">
<br>
<?php if(!empty($__MONITOR_GDKV)): ?>
<p><b>Chú thích:</b></p>
<ul>
    <li>
        <input value="6" data-id ="_highest" class="check_show" type="checkbox" checked >
        <div style="border:1px solid black; width:20px; display:inline-block; position:relative; top:5px; height:20px; background:<?php echo $colorHighest; ?>"></div>
        <span>Cao nhất đội 2 tháng liên tục</span>
    </li>
    <li>
        <input value="6" data-id ="_bonus" class="check_show" type="checkbox" checked>
        <div style="border:1px solid black; width:20px; display:inline-block; position:relative; top:5px; height:20px; background:<?php echo $colorBonus; ?>"></div>
        <span>Đạt 2 tháng</span>
    </li>
    <li>
        <input value="6" data-id ="_default" class="check_show" type="checkbox" checked>
        <div style="border:1px solid black; width:20px; display:inline-block; position:relative; top:5px; height:20px; background:white;"></div>
        <span>Đạt 1 tháng/ Thời gian làm chính thức nhỏ hơn 2 tháng</span>
    </li>
    <li>
        <input value="6" data-id ="_no_bonus" class="check_show" type="checkbox" checked>
        <div style="border:1px solid black; width:20px; display:inline-block; position:relative; top:5px; height:20px; background:<?php echo $colorNoBonus; ?>"></div>
        <span>Không đạt</span>
    </li>
</ul>

<h1>Thống Kê - Giám Sát</h1>
<table class="tb hm_table statistic_gskv_target" style="border-collapse: collapse; table-layout: fixed;font-size: 10px; width: 100%;">
    <thead style="background: #f1f1f1;font-size: 10px;">
        <tr>
            <th class="item_b item_c w-10">STT</th>
            <th class="item_b item_c w-55">Tên nhân viên</th>
            <th class="item_b item_c w-50">Tỷ Lệ Thưởng <?php echo date('m/Y', strtotime($prevMonth)) ?></th>
            <th class="item_b item_c w-50">Tỷ Lệ Thưởng <?php echo date('m/Y', strtotime($currMonth)) ?></th>
        </tr>
    </thead>
    <tbody>
        <?php 
        $i = 0;
        foreach ($DISPLAY['SUM_GS'] as $monitor_id => $data): 
            $sumGsPrev = empty($data[$prevMonth]) ? 0 : $data[$prevMonth];
            $sumGsCurr = empty($data[$currMonth]) ? 0 : $data[$currMonth];
            $classDisplayBonus     = '_default';
            $style = '';
            if($sumGsPrev >= GasTargetMonthly::PERCENT_BONUS_GS && $sumGsCurr >= GasTargetMonthly::PERCENT_BONUS_GS){
                $style = "style='background: $colorBonus;'";
                $classDisplayBonus = '_bonus';
            }
            if($sumGsPrev < GasTargetMonthly::PERCENT_BONUS_GS && $sumGsCurr < GasTargetMonthly::PERCENT_BONUS_GS){
                $style = "style='background: $colorNoBonus;'";
                $classDisplayBonus = '_no_bonus';
            }
        ?>
        <tr class="item_b <?php echo $classDisplayBonus; ?>" <?php echo $style; ?>>
                <td class="item_c"><?php echo $i++; ?></td>
                <td>
                    <?php echo empty($USER_ALL[$monitor_id]) ? '' : $USER_ALL[$monitor_id]->first_name; ?>
                </td>
                <td class="item_c"><?php echo $sumGsPrev; ?> %</td>
                <td class="item_c"><?php echo $sumGsCurr; ?> %</td>
            </tr>
        <?php endforeach;?>
    </tbody>
</table>
<?php foreach ($__MONITOR_GDKV as $monitor_gdkv_id => $aCcsId): ?>
<div class="gdkvGroup">
<h1><?php echo isset($__USER[$monitor_gdkv_id]) ? $__USER[$monitor_gdkv_id]->first_name : ''; ?> </h1>
<table class="tb hm_table float_tb" style="border-collapse: collapse; table-layout: fixed;font-size: 10px; width: 100%;">
    <thead style="background: #f1f1f1;font-size: 10px;">
        <tr>
            <th class="item_b item_c w-5">STT</th>
            <th class="item_b item_c w-55">Tên nhân viên</th>
            <th class="item_b item_c w-40">Loại NV</th>
            <th class="item_b item_c w-50">Tỷ Lệ Thưởng <?php echo date('m/Y', strtotime($prevMonth)) ?></th>
            <th class="item_b item_c w-50">Tỷ Lệ Thưởng <?php echo date('m/Y', strtotime($currMonth)) ?></th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($aCcsId as $monitor_id => $monitor_id_v2): ?>
        <?php 
            if(empty($__MONITOR[$monitor_id])) continue;
            $aEmployeeId = $__MONITOR[$monitor_id];
            $aHighest    = $mGasTargetMonthlyCal->getHighestCcs($DISPLAY['CCS_BY_MONTH'][$monitor_id]);
        ?>
        <tr>
            <td class="item_c item_b f_size_15" colspan="5" style="background: #f1f1f1;">
                <?php echo isset($__USER[$monitor_id]) ? $__USER[$monitor_id]->first_name : ''; ?>
            </td>
        </tr>
            <?php $index = 1;?>
            <?php foreach ($aEmployeeId as $user_id => $id): ?>
            <!--row css employee-->
            <?php 
                $sBonusPrev = empty($DISPLAY['CCS'][$user_id][$prevMonth]) ? 0 : $DISPLAY['CCS'][$user_id][$prevMonth];
                $sBonusCurr = empty($DISPLAY['CCS'][$user_id][$currMonth]) ? 0 : $DISPLAY['CCS'][$user_id][$currMonth];
                $style      = '';
                $classDisplayBonus     = '_default';
                if($sBonusPrev >= GasTargetMonthly::PRTCENT_BONUS_PTTT && $sBonusCurr >= GasTargetMonthly::PRTCENT_BONUS_PTTT){
                    $style  = 'style="background: '.$colorBonus.';"'; // đạt chỉ tiêu
                    $classDisplayBonus = '_bonus';
                }
                if($sBonusPrev < GasTargetMonthly::PRTCENT_BONUS_PTTT && $sBonusCurr < GasTargetMonthly::PRTCENT_BONUS_PTTT && in_array($user_id, $__IS_APPLY_GSKV) && in_array($user_id, $IS_APPLY_GSKV)){
                    $style  = 'style="background: '.$colorNoBonus.';"'; // ko đạt chỉ tiêu
                    $classDisplayBonus = '_no_bonus';
                }
                if(in_array($user_id, $aHighest)){
                    $style  = 'style="background: '.$colorHighest.';"'; // đạt chỉ tiêu cao nhất
                    $classDisplayBonus = '_highest';
                }
            ?>
            <tr <?php echo $style; ?> class="<?php echo $classDisplayBonus; ?>">
                <td class="item_c w-5"><?php echo $index++;?></td>
                <td class="item_l w-55"><?php echo isset($__USER[$user_id]) ? $__USER[$user_id]->first_name : ''; ?></td>
                <td class="item_l w-40">
                    <?php 
                    $firstName = 'PTTT';
                    if(isset($__USER[$user_id])){
                        if($__USER[$user_id]->gender == Users::SALE_MOI_PTTT){
                            $firstName = 'PTTT';
                        }elseif($__USER[$user_id]->gender == Users::SALE_PTTT_KD_KHU_VUC){
                            $firstName = 'CCS';
                        }
                    }
                    echo $firstName;
                    ?>
                </td>
                <td class="item_c w-50"><?php echo $sBonusPrev; // prev month ?>  %</td>
                <td class="item_c w-50"><?php echo $sBonusCurr; // current month ?>  %</td>
            </tr>
            <!--end row css employee-->
            <?php endforeach;?>
            <!--ROW SUM CCS-->
<!--            <tr>
                <td class="item_c item_b w-140" colspan="3">Tổng</td>
                <?php 
//                $sBonusPrev = empty($DISPLAY['SUM_CCS'][$monitor_gdkv_id][$monitor_id][$prevMonth]) ? 0 : $DISPLAY['SUM_CCS'][$monitor_gdkv_id][$monitor_id][$prevMonth];
//                $sBonusCurr = empty($DISPLAY['SUM_CCS'][$monitor_gdkv_id][$monitor_id][$currMonth]) ? 0 : $DISPLAY['SUM_CCS'][$monitor_gdkv_id][$monitor_id][$currMonth];
                ?>
                <td class="item_c item_b w-50"><?php // echo $sBonusPrev; // prev month ?>  %</td>
                <td class="item_c item_b w-50"><?php // echo $sBonusCurr; // current month ?>  %</td>
            </tr>-->
            <!--END ROW SUM CCS-->
            <!--ROW SUM GS-->
<!--            <tr class="statistic_gskv">
                <td class="item_c item_b w-140" colspan="3"><?php // echo isset($__USER[$monitor_id]) ? $__USER[$monitor_id]->first_name : ''; ?></td>
                <?php 
//                $sBonusPrev = empty($DISPLAY['SUM_GS'][$monitor_gdkv_id][$monitor_id][$prevMonth]) ? 0 : $DISPLAY['SUM_GS'][$monitor_gdkv_id][$monitor_id][$prevMonth];
//                $sBonusCurr = empty($DISPLAY['SUM_GS'][$monitor_gdkv_id][$monitor_id][$currMonth]) ? 0 : $DISPLAY['SUM_GS'][$monitor_gdkv_id][$monitor_id][$currMonth];
                ?>
                <td class="item_c item_b w-50 js-bonus-gs-prev"><?php // echo $sBonusPrev; // prev month ?>  %</td>
                <td class="item_c item_b w-50 js-bonus-gs-curr"><?php // echo $sBonusCurr; // current month ?>  %</td>
            </tr>-->
            <!--END ROW SUM GS-->
        <?php endforeach;?>
        <!--ROW SUM GDKV-->
<!--        <tr>
            <td class="item_c item_b w-130" colspan="3" style="color:#ad0000">Tổng - <?php // echo isset($USER[$monitor_gdkv_id]) ? $USER[$monitor_gdkv_id]->first_name : ''; ?></td>
            <?php 
//            $sBonusPrev = empty($DISPLAY['SUM_GDKV'][$monitor_gdkv_id][$prevMonth]) ? 0 : $DISPLAY['SUM_GDKV'][$monitor_gdkv_id][$prevMonth];
//            $sBonusCurr = empty($DISPLAY['SUM_GDKV'][$monitor_gdkv_id][$currMonth]) ? 0 : $DISPLAY['SUM_GDKV'][$monitor_gdkv_id][$currMonth];
            ?>
            <td class="item_c item_b w-50" style="color:#ad0000"><?php // echo $sBonusPrev; // prev month ?>  %</td>
            <td class="item_c item_b w-50" style="color:#ad0000"><?php // echo $sBonusCurr; // current month ?>  %</td>
        </tr>-->
        <!--END ROW SUM GDKV-->
    </tbody>
</table>
</div>
<?php endforeach; ?>
<?php endif; ?>
</div>



<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/print-store-card.css" />
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/jquery.floatThead.min.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/underscore-min.js"></script> 
<script>
    $(function(){
        $('.float_tb').floatThead({top:50});
        $('.check_show').click(function(){
            $dataClass = $(this).data('id');
            $classHiddend = '.'+$dataClass;
            if(this.checked == true){
                $($classHiddend).removeClass('display_none');
            }else{
                $($classHiddend).addClass('display_none');
            }
        });
    });
</script>
