<?php
$urlExcel = Yii::app()->createAbsoluteUrl('admin/gastargetmonthly/ccs', ['ToExcel'=>1]);
$this->breadcrumbs=array(
    $this->pageTitle,
);
?>
<div class="form">
<h1><?php echo $this->pageTitle; ?><?php include 'index_button.php';?></h1>
</div>
<?php 
if($model->type_index == GasTargetMonthly::TYPE_TELESALE){
    include 'targetViewTelesale.php';
}elseif($model->type_index == GasTargetMonthly::TYPE_STATISTIC){
    include 'targetViewStatistic.php';
}else{
     include 'targetViewCcs.php';
}
?>