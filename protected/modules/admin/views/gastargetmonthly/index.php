<?php
$this->breadcrumbs=array(
	'Thiết lập phần trăm target',
);
?>

<h1>Thiết lập target cho đại lý và nhân viên sale</h1>
<div class="flash notice">
    Chú ý: Với Đại Lý (ô target) và Sale thì nhập Bình Bò và Mối<br>
    Với Đại Lý (ô Target Hộ Gia Đình) và Giám Sát PTTT thì chỉ nhập Bình Bò
</div>    

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'gas-target-monthly-form',
	'enableAjaxValidation'=>false,
)); ?>
<div class="search-form" style="">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
        'form'=>$form,
)); ?>
</div><!-- search-form -->

<div class="form">
        <!--<h1>ĐẠI LÝ</h1>-->
        
        <?php $aSale = Users::getArrUserByRole(ROLE_AGENT);
            $help_kien_coi_b12='help_kien_coi_b12';
        ?>
        <?php foreach ($aSale as $key=>$item): ?>
            <div class="row setup_target" style="">
                <fieldset class="box_310" style="min-height: 100px;">
                    <legend class="item_b"><?php echo $item;?></legend>
                    <label>Target</label>
                    <div class="fix_number_help">
                        <?php if(isset($aSalePercent[$key]['target']) && $aSalePercent[$key]['target']): ?>
                            <?php echo $form->textField($model,'target[]',array('value'=> ActiveRecord::formatNumberInput($aSalePercent[$key]['target']),'class'=>"number_only agent_change ",'size'=>3,'maxlength'=>16)); ?>
                        <?php else: ?>
                            <?php echo $form->textField($model,'target[]',array('class'=>"number_only agent_change ",'size'=>3,'maxlength'=>16)); ?>
                        <?php endif; ?>
                        <div class="help_number"></div>
                        <?php echo $form->hiddenField($model,'sale_id[]',array('value'=>$key)); ?>
                    </div>
                    <label>Target Hộ Gia Đình</label>
                    <div class="fix_number_help">
                        <?php if(isset($aSalePercent[$key]['target_ho_gd']) && $aSalePercent[$key]['target_ho_gd']): ?>
                            <?php echo $form->textField($model,'target_ho_gd[]',array('value'=> ActiveRecord::formatNumberInput($aSalePercent[$key]['target_ho_gd']),'class'=>"number_only target_ho_gd $help_kien_coi_b12",'size'=>3,'maxlength'=>16)); ?>
                        <?php else: ?>
                            <?php echo $form->textField($model,'target_ho_gd[]',array('class'=>"number_only target_ho_gd $help_kien_coi_b12",'size'=>3,'maxlength'=>16)); ?>
                        <?php endif; ?>
                        <div class="help_number"></div>
                    </div>
                    <div class="clr"></div>
                    <p><?php echo MyFunctionCustom::remove_vietnamese_accents($item);?></p>
                </fieldset>
            </div>
        <?php endforeach; ?>    
        <div class="clr"></div> 
        <h1>NHÂN VIÊN SALE</h1>
                
        <?php 
        $aTypeSaleNotIn = array(Users::SALE_MOI);
        $aSale = Users::getArrUserByRoleForSetUpTarget(ROLE_SALE, array('aTypeSaleNotIn'=>$aTypeSaleNotIn, 'order'=>'t.gender ASC, t.code_bussiness ASC'));
        $SaleBo = Users::getArrIdUserByArrayRole(array(ROLE_SALE), 
                    array('gender'=> Users::SALE_BO, 'order'=>'t.gender ASC, t.code_bussiness ASC'));
        ?>
        <?php foreach ($aSale as $key=>$item): ?>
            <?php 
                $help_kien_coi_b12='help_kien_coi_b12';
                if(in_array($key, $SaleBo)){
                    $help_kien_coi_b12='';
                }
            ?>
            <div class="row setup_target" style="">
                <label><?php echo $item;?></label>
                <div class="fix_number_help">
                    <?php if(isset($aSalePercent[$key]['target']) && $aSalePercent[$key]['target']): ?>
                        <?php echo $form->textField($model,'target[]',array('value'=>  ActiveRecord::formatNumberInput($aSalePercent[$key]['target']),'class'=>"number_only $help_kien_coi_b12",'size'=>3,'maxlength'=>16)); ?>
                    <?php else: ?>
                        <?php echo $form->textField($model,'target[]',array('class'=>"number_only $help_kien_coi_b12",'size'=>3,'maxlength'=>16)); ?>
                    <?php endif; ?>
                    <div class="help_number"></div>
                    <?php echo $form->hiddenField($model,'sale_id[]',array('value'=>$key)); ?>
                </div>
            </div>
        <?php endforeach; ?>    
        <div class="clr"></div>
        <?php include 'index_giam_sat_pttt.php'; ?>
        <?php // include 'PTTT_chuyen_vien_kd_kv.php'; ?>
	<div class="row buttons" style="padding-left: 115px;">
        <?php $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType'=>'submit',
            'label'=> 'Lưu lại',
            'type'=>'null', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
            'size'=>'small', // null, 'large', 'small' or 'mini'
            //'htmlOptions' => array('style' => 'margin-bottom: 10px; float: right;'),
        )); ?>	</div>    
<?php $this->endWidget(); ?>
</div>

<style>
    .setup_target {float:left;width:330px; }
    
</style>

<script>
$(document).ready(function(){
    $('.form').find('button:submit').click(function(){        
        $.blockUI({ overlayCSS: { backgroundColor: '#fff' } }); 
    });   
    
//    $('.agent_change').eq(1).change(function(){
//        $('.agent_change').val($(this).val());
//    });
//    $('.target_ho_gd').eq(1).change(function(){
//        $('.target_ho_gd').val($(this).val());
//    });
    
//    $('.help_kien_coi_b12').change(function(){
//        var val = $(this).val();
//        $(this).val(val*12);
//    });
    
});
</script>    