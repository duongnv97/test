<?php
$this->breadcrumbs=array(
	'Gas Target Monthlies'=>array('index'),
	$model->id,
);

$menus = array(
	array('label'=>'GasTargetMonthly Management', 'url'=>array('index')),
	array('label'=>'Create GasTargetMonthly', 'url'=>array('create')),
	array('label'=>'Update GasTargetMonthly', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete GasTargetMonthly', 'url'=>array('delete'), 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
);
$this->menu= ControllerActionsName::createMenusRoles($menus, $actions);
?>

<h1>View GasTargetMonthly #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'sale_id',
		'monthly',
		'year',
		'percent_target',
		'created_date',
	),
)); ?>
