<link rel="stylesheet" type="text/css" media="screen" href="<?php echo Yii::app()->theme->baseUrl; ?>/admin/css/form.css" />
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/jquery.blockUI.js"></script>    
<div class="form">
    <?php if($msg): ?>
        <div class="success_div"><?php echo $msg;?></div>
        <div class="success_close"><input type="button" value="Close"></div>        
        <style>
            .success_close {text-align: center; padding: 20px;margin: 0;}
            .success_div {margin-top: 85px;}
        </style>
    <?php else: ?>
        <h1 class="h1-in-form">Cập nhật tình trạng bảo trì khách hàng: <?php echo $model->customer?$model->customer->first_name:'';?></h1>
        <?php $form=$this->beginWidget('CActiveForm', array(
                'id'=>'maintain-form',
                'enableAjaxValidation'=>false,
        )); ?>
                <?php // echo $form->errorSummary($model); ?>

                <div class="row">
                        <?php echo $form->labelEx($model,'status_seri_diff', array('style'=>'')); ?>
                        <?php echo $form->dropDownList($model,'status_seri_diff', CmsFormatter::$STATUS_MAINTAIN_BACK,array('style'=>'width:390px;','class'=>'cat_month')); ?>
                        <?php echo $form->error($model,'status_seri_diff'); ?>
                </div>

<!--                <div class="row">
                        <?php // echo Yii::t('translation', $form->labelEx($model,'note_seri_diff')); ?>
                        <?php // echo $form->textArea($model,'note_seri_diff',array('style'=>'width:390px;height:130px','maxlength'=>500)); ?>
                        <?php // echo $form->error($model,'note_seri_diff'); ?>
                </div>-->
        
                <div class="row">
                            <?php echo Yii::t('translation', $form->labelEx($model,'note_seri_diff')); ?>
                                    <div style="padding-left:141px;">
                                            <?php
                                            $this->widget('ext.niceditor.nicEditorWidget', array(
                                                    "model" => $model, // Data-Model
                                                    "attribute" => 'note_seri_diff', // Attribute in the Data-Model        
                                                    "config" => array(
            //                                "maxHeight" => "200px",   
                                                                    "buttonList"=>Yii::app()->params['niceditor'],
                                                    ),
                                                    "width" => EDITOR_WIDTH, // Optional default to 100%
                                                    "height" => EDITOR_HEIGHT, // Optional default to 150px
                                            ));
                                            ?>                                
                                    </div>		
                            <?php echo $form->error($model,'note_seri_diff'); ?>
                    </div>        

                <div class="row buttons" style="padding-left: 85px;">
                    <?php $this->widget('bootstrap.widgets.TbButton', array(
                    'buttonType'=>'submit',
                    'label'=>$model->isNewRecord ? Yii::t('translation', 'Create') : Yii::t('translation', 'Save'),
                    'type'=>'null', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
                    'size'=>'small', // null, 'large', 'small' or 'mini'
                    'htmlOptions' => array('class' => 'submit-blockui'),
                )); ?>	</div>

        <?php $this->endWidget(); ?>
        <?php endif; ?>
</div><!-- form -->

<style>
    .h1-in-form {font-size: 16px;text-align: center; margin-bottom: 30px;}
    #yw0{
        margin-left: 50px;
    }
</style>

<script>
    $(document).ready(function(){
        parent.$.fn.yiiGridView.update("gas-maintain-grid");   
        
        $('.success_close').click(function(){
            parent.$.colorbox.close();
        });
        
        $('.submit-blockui').live('click',function(){
            $('.form').block({
                message: '', 
                overlayCSS:  { backgroundColor: '#fff' }
           }); 
//           $('.form').unblock(); 
        });
    });
</script>