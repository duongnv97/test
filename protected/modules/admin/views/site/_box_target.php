<?php if( ( Yii::app()->user->role_id==ROLE_SUB_USER_AGENT && MyFormat::getAgentGender()==Users::IS_AGENT) 
//        || Yii::app()->user->role_id==ROLE_SALE
        ):?>

<?php 
    $cmsFormater = new CmsFormatter();
    $data=array();
    $model = new GasStoreCard();
    $model->statistic_year = date('Y');
    $model->statistic_month = date('m');
    $FOR_MONTH = $model->statistic_month;
    $FOR_YEAR = $model->statistic_year;    
    $data = Statistic::Target($model);

    $AGENT_MODEL= isset($data['ROLE_AGENT_MODEL'])?$data['ROLE_AGENT_MODEL']:array();
?>
    <h1>Thống kê target tháng <?php echo date('m');?> năm <?php echo date('Y');?></h1>
    <div class="clearfix">
        <div class="color_type_1 color_note"></div> &nbsp;&nbsp;&nbsp;Gas Bình Bò
        <div class="clr"></div>
        <div class="color_type_2 color_note"></div> &nbsp;&nbsp;&nbsp;Gas Bình Mối
        <div class="clr"></div>
        <div class="color_type_3 color_note"></div> &nbsp;&nbsp;&nbsp; Hộ Gia Đình
    </div>
    <div class="clr"></div>     
    
    <?php foreach($AGENT_MODEL as $province_id=>$aModelUser): ?>
        <?php foreach($aModelUser as $mUser): ?>
        <?php if($mUser->id== MyFormat::getAgentId()): ?>
            <div class="box_310">
                <table class="hm_table items">
                    <tbody>
                        <tr>
                            <td colspan="3" class="item_c item_b"><?php echo $mUser->first_name;?></td>
                        </tr>
                        <tr>
                            <td class="item_c w-100">Target</td>
                            <td class="item_c w-100">Thực Tế</td>
                            <td class="item_c w-100">Tỷ Lệ</td>
                        </tr>
                        <?php foreach(CmsFormatter::$CUSTOMER_BO_MOI as $type=>$label):?>
                        <?php
                            if($type == STORE_CARD_XE_RAO){
                                continue;
                            }
                            $uid = $mUser->id;
                            $target=isset($data['aTargetEachMonth'][$uid][$type][$FOR_MONTH])?$data['aTargetEachMonth'][$uid][$type][$FOR_MONTH]:0;
                            $OUTPUT=isset($data['OUTPUT'][$uid][$type])?$data['OUTPUT'][$uid][$type]:0;
                            $REMAIN=isset($data['REMAIN'][$uid][$type])?$data['REMAIN'][$uid][$type]:0;
                            $real = $OUTPUT-$REMAIN;
                            
                            // May 16, 2014 xử lý change cộng sản lượng bình bỏ của KH mối vào sản lương bình bò của dại lý
                            if($type==STORE_CARD_KH_MOI){
                                // chỗ này xử lý trừ đi bình bò của KH mối, sẽ cộng lên cho sản lượng của KH bình bò của đại lý
                                $OUTPUT_BINH_BO=isset($data['BINH_BO_KH_MOI'][$uid][$type])?$data['BINH_BO_KH_MOI'][$uid][$type]:0;
                                $real -=  $OUTPUT_BINH_BO ; // trừ sản lượng bình bò
                                $real +=  $REMAIN; // cộng ngược lại gas dư, vì sẽ không trừ đi gas dư ở đây
                                /* Aug 05, 2014 xử lý trừ sản lượng của sale chuyên viên và sale pttt*/
//                                $OUTPUT_KH_MOI_SALE_CV_PTTT=isset($data['KH_MOI_SALE_CV_PTTT'][$uid][$type])?$data['KH_MOI_SALE_CV_PTTT'][$uid][$type]:0;
//                                $real -=  $OUTPUT_KH_MOI_SALE_CV_PTTT ; // trừ sản lượng bình mối của sale chuyên viên vs pttt
                                // Now 20, 2014 Kiên kêu đóng lại, vì xem thấy nó lệch với bên daily sản lượng
                                /* Aug 05, 2014 xử lý trừ sản lượng của sale chuyên viên và sale pttt*/
                            }
                            if($type==STORE_CARD_KH_BINH_BO){
                                // chỗ này xử lý cộng vào sản lượng bình bò của KH mối, sẽ cộng lên cho sản lượng của KH bình bò của đại lý
                                $OUTPUT_BINH_BO=isset($data['BINH_BO_KH_MOI'][$uid][STORE_CARD_KH_MOI])?$data['BINH_BO_KH_MOI'][$uid][STORE_CARD_KH_MOI]:0;
                                // trừ đi lượng gas dư của KH mối mà bán bình bò
                                $REMAIN_KH_MOI=isset($data['REMAIN'][$uid][STORE_CARD_KH_MOI])?$data['REMAIN'][$uid][STORE_CARD_KH_MOI]:0;
                                $real += ( $OUTPUT_BINH_BO - $REMAIN_KH_MOI) ;
                            }
                            // May 16, 2014 xử lý change cộng sản lượng bình bỏ của KH mối vào sản lương bình bò của dại lý
                            
                            $qty = '';
                            $target_qty = '';
                            if($type==STORE_CARD_KH_MOI && $real){                                    
                                $qty = " = ".$real/12;
                            }
                            if($type==STORE_CARD_KH_MOI && $target>0){
                                $target_qty = " = ".$target/12;
                            }

                            $percent = $real;
                            if($target > 0){
                                $percent = ($real/$target)*100;
                            }
                        ?>

                        <tr class="color_type_<?php echo $type;?>">
                            <td class="item_r"><?php echo ActiveRecord::formatCurrency($target).$target_qty;?></td>
                            <td class="item_r"><?php echo ActiveRecord::formatCurrency($real)."$qty";?></td>
                            <td class="item_c"><?php echo round($percent,1);?> %</td>
                        </tr>
                        <?php endforeach;?>
                        <!-- for hộ gia đình -->
                        <?php
                            $uid = $mUser->id;
                            $target=isset($data['TARGET_HO_GIA_DINH'][$uid][$FOR_MONTH])?$data['TARGET_HO_GIA_DINH'][$uid][$FOR_MONTH]:0;
                            $real=isset($data[GasTargetMonthly::HGD_REAL][$uid])?$data[GasTargetMonthly::HGD_REAL][$uid]:0;
                            $amount = $real/12;
                            $percent = $real;
                            $target_qty = '';
                            if($target){
                                $percent = ($real/$target)*100;
                            }
                            if($target>0){
                                $target_qty = " = ".$target/12;
                            }
                        ?>
                        <tr class="color_type_3">
                            <td class="item_r"><?php echo ActiveRecord::formatCurrency($target).$target_qty;?></td>
                            <td class="item_r"><?php echo ActiveRecord::formatCurrency($real)." = $amount";?></td>
                            <td class="item_c"><?php echo round($percent,1);?> %</td>
                        </tr>
                        <!-- for hộ gia đình -->
                    </tbody>
                </table>
            </div>
        <?php endif;?>
        <?php endforeach; // end foreach($aModelUser as $mUser)?>
    <div class="clr"></div>
    <?php endforeach; // end <?php foreach($AGENT_MODEL as $province_id=>$aModelUser): ?>    

<?php endif;?>