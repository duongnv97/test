<?php 
$mAgent = Users::model()->findByPk(MyFormat::getAgentId());
$date_last_gas_remain = Users::GetKeyInfo($mAgent, 'date_last_gas_remain');
$date_last_storecard = Users::GetKeyInfo($mAgent, 'date_last_storecard');
$today = date('Y-m-d');
$delay_gas_remain = MyFormat::getNumberOfDayBetweenTwoDate($today, $date_last_gas_remain);
$delay_storecard = MyFormat::getNumberOfDayBetweenTwoDate($today, $date_last_storecard);

?>
<div class="display_none">
    <div id="alert_box_date_input" class="">
        <h1>Thống kê ngày nhập web</h1>
        <p class="f_size_15">Thẻ Kho: <?php echo MyFormat::dateConverYmdToDmy($date_last_storecard);?> Trễ <?php echo $delay_storecard;?> ngày</p>
        <p class="f_size_15">Gas Dư: <?php echo MyFormat::dateConverYmdToDmy($date_last_gas_remain);?> Trễ <?php echo $delay_gas_remain;?> ngày</p>
    </div>
</div>

<script>
$(function(){
//Close on Sep 23, 2016
//    $.colorbox({inline:true, href:"#alert_box_date_input",
//        closeButton:false,
//            overlayClose :false, escKey:false,
//            innerHeight:'250',
//            innerWidth:'500'
//     });
});
</script>
