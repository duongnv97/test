<?php if($FlagIsAgent): ?>
<?php // if(0):?>
<?php include '_box_date_input.php'; // Apr 10, 2015 ?>

<div class="box_nxt">
    <div class="search-form" style="">
        <div class="wide form">
        <?php $form=$this->beginWidget('CActiveForm', array(
                'action'=>Yii::app()->createUrl($this->route),
                'method'=>'post',
        )); ?>
            <?php $mStoreCardV1 = new GasStoreCard; $mStoreCardV1->date_delivery= date('d-m-Y'); ?>
                <div class="row">
                    <?php echo $form->label($mStoreCardV1,'date_delivery', array('label'=>'Thống Kê Nhập Xuất Tồn Ngày', 'style'=>'width:auto;')); ?>
                    <?php 
                        $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                            'model'=>$mStoreCardV1,        
                            'attribute'=>'date_delivery',
                            'options'=>array(
                                'showAnim'=>'fold',
                                'dateFormat'=> MyFormat::$dateFormatSearch,
                                'maxDate'=> '0',
                                'changeMonth' => true,
                                'changeYear' => true,
                                'showOn' => 'button',
                                'buttonImage'=> Yii::app()->theme->baseUrl.'/admin/images/icon_calendar_r.gif',
                                'buttonImageOnly'=> true,    
                                'yearRange'=> date('Y').":".date('Y'),
                            ),        
                            'htmlOptions'=>array(
                                'class'=>'w-16',
                                'size'=>'16',
                                'style'=>'height:20px;float:left;',
                                'readonly'=>1,
                            ),
                        ));
                    ?>     		
                </div>                

        <?php $this->endWidget(); ?>

        </div><!-- wide form -->
    </div><!-- search-form -->
    
    <?php 
        // May 16, 2015, xử lý khi đại lý login vào bị chậm, cho ajax chạy load cái thống kê này sau
    ?>
    <div class="nxt_result_form" style="box-shadow: 0 0 5px #CCCCCC; margin-bottom: 10px; padding:0 5px;">
        <?php if(isset($_GET['ajax_nxt'])):?>
            <?php include '_box_nxt_form.php';?>
        <?php else:?>
            <?php include '_box_nxt_loading.php';?>
        <?php endif;?>
    </div>
    
</div>

<style>
    #ui-datepicker-div { z-index: 20 !important; }
</style>
<script>
$(function(){
    $('#GasStoreCard_date_delivery').change(function(){
//        $.blockUI({ overlayCSS: { backgroundColor: '#fff' } }); 
        $('div.box_nxt').block({
            overlayCSS:  { backgroundColor: '#fff' }
        });

        var date_nxt = $(this).val();
        var requestUri = '<?php echo Yii::app()->createAbsoluteUrl('admin/site/index', array('ajax_nxt'=>1 ));?>';
        $.ajax({
            url: requestUri,
            data:{date_nxt:date_nxt},
            success : function(data) {
                $('.nxt_result_form').html($(data).find('.nxt_result_form').html());
                fnRemoveTrNxt();
//                $.unblockUI();
                $('div.box_nxt').unblock();
            }
        });
        
    });
    fnRemoveTrNxt();
    
});

function fnRemoveTrNxt(){
    $('.class_remove_row').closest('tr').remove();
    $('.class_focus_nxt').closest('tr').addClass('class_focus_nxt_odd');
}

// May 16, 2015 Fix for agent login bi cham
$(window).load(function(){
//    setTimeout(fnTriggerChangeDateNxt, 1000);// 1000 ms = 1s
    fnTriggerChangeDateNxt();
//    fnForTargetOnly();// Close on Sep 25, 2016 ko cần sử dụng nữa
});

function fnTriggerChangeDateNxt(){
    $('#GasStoreCard_date_delivery').trigger('change');
}

// May 16,2 015, chỉ load tính target trong onload, không load khi change date
function fnForTargetOnly(){
    var requestUri = '<?php echo Yii::app()->createAbsoluteUrl('admin/site/index', array('fnForTargetOnly'=>1 ));?>';
    $.ajax({
        url: requestUri,
        success : function(data) {
            $('.box_target').html($(data).find('.box_target').html());
        }
    });
}

</script>

<?php endif;?>