<?php include '_box_issue.php';?>
<?php 
$FlagIsAgent = false;
$cRole = MyFormat::getCurrentRoleId();
$cUid = MyFormat::getCurrentUid();
if($cRole==ROLE_SUB_USER_AGENT 
   && ( MyFormat::getAgentGender()==Users::IS_AGENT)
   ):
    $FlagIsAgent = true;
endif;

$mPromotionUser = new AppPromotionUser();
if(in_array($cRole, $mPromotionUser->getRoleSaleIdHgd())){
    include '_box_code_gas24h.php';
}
?>

<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->theme->baseUrl; ?>/admin/css/ad_gas.css" />
    <div class="box_target">
        <?php if(isset($_GET['fnForTargetOnly'])):?>
            <?php // include '_box_target.php';?>
        <?php else:?>
            <?php if($FlagIsAgent): ?>
                <?php // include '_box_nxt_loading.php';?>
            <?php endif;?>
        <?php endif;?>
    </div>
<?php if(!isset($_GET['date_nxt'])): // giảm tải ?>
    <div class="clr"></div>
    <?php include '_box_revenue_output.php';?>
<?php endif;?>
<div class="clr"></div>
<?php include '_box_nxt.php';?>
<div class="clr"></div>

<?php 
    $aRoleLimit = [ROLE_EMPLOYEE_MAINTAIN, ROLE_PARTNER];
if(in_array($cRole, $aRoleLimit)): ?>
    <?php include '_box_ref_tree.php';?>
<?php endif; ?>

<?php if(!$this->isGuest()): ?>
    <?php include '_box_news.php';?>
<?php endif; ?>

<script>
$(function(){
});
</script>

