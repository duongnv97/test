<?php // if(Yii::app()->user->role_id==ROLE_SUB_USER_AGENT && MyFormat::getAgentGender()==Users::IS_AGENT):?>
<?php if($FlagIsAgent): ?>
<div class="box_revenue_output_index">
<?php 
    $cmsFormater = new CmsFormatter();
    $data=array();
    $model = new GasStoreCard();
    $model->statistic_year = date('Y');
    $model->statistic_month = date('m');
    $FOR_MONTH = $model->statistic_month;
    $FOR_YEAR = $model->statistic_year;    
    $model->agent_id = array(MyFormat::getAgentId());
    $data = Statistic::RevenueOutput($model);
?>
<?php if(isset($data['OUTPUT'])):?>
    <?php include 'AgentStatisticRevenueOutput/Revenue_output_form.php';?>
<?php endif;?>    
    
</div>
<?php endif;?>