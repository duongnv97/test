<div class="ad_read_news">
    <?php
    $mCms = Cms::getAllShowIndexByStatus(STATUS_ACTIVE);
    if(count($mCms)>0): ?>
        <?php foreach($mCms as $cms): ?>
            <section>
                <h1 class="title"><strong><?php echo $cms->title;?></strong></h1>
                <div class="content document">
                    <?php echo $cms->cms_content;?>
                </div>
                <div class='clr'></div>
            </section>
        <?php endforeach;?>
    <?php endif;?>
</div>

<div class="ad_read_news">
    <section>
         <h1 class="title"><strong>Thông Báo Khác</strong></h1>
        <div class="content document">
        <?php
        $mCms = Cms::getAllActive();
        if(count($mCms)>0): ?>
            <ul>
            <?php foreach($mCms as $cms): ?>
                <li>
                    <a class="gas_link" href="<?php echo Yii::app()->createAbsoluteUrl('admin/site/news',array('id'=>$cms->id));?>">
                       <?php echo $cms->title;?>
                   </a>
                </li>
            <?php endforeach;?>
            </ul>
        <?php endif;?>
        </div>
    </section>
</div>