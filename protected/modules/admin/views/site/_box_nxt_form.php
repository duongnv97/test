<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/print-store-card.css" />

    <?php
        $AGENT_NXT = Statistic::AgentNXT();
        $WAREHOUSE_MODEL = $AGENT_NXT['WAREHOUSE_MODEL'];
        $MATERIAL_MODEL = isset($AGENT_NXT['MATERIAL_MODEL'])?$AGENT_NXT['MATERIAL_MODEL']:array();
        $OPENING_BALANCE_YEAR_BEFORE = isset($AGENT_NXT['OPENING_BALANCE_YEAR_BEFORE'])?$AGENT_NXT['OPENING_BALANCE_YEAR_BEFORE']:array();
        $IMPORT_IN_YEAR = isset($AGENT_NXT['IMPORT_IN_YEAR'])?$AGENT_NXT['IMPORT_IN_YEAR']:array();
        $EXPORT_IN_YEAR = isset($AGENT_NXT['EXPORT_IN_YEAR'])?$AGENT_NXT['EXPORT_IN_YEAR']:array();
        $GAS_INPUT = isset($AGENT_NXT['GAS_INPUT'])?$AGENT_NXT['GAS_INPUT']:array();
        $GAS_OUTPUT = isset($AGENT_NXT['GAS_OUTPUT'])?$AGENT_NXT['GAS_OUTPUT']:array();
        $VO_INPUT = isset($AGENT_NXT['VO_INPUT'])?$AGENT_NXT['VO_INPUT']:array();
        $VO_OUTPUT = isset($AGENT_NXT['VO_OUTPUT'])?$AGENT_NXT['VO_OUTPUT']:array();
        $VT_GIFT_INPUT = isset($AGENT_NXT['VT_GIFT_INPUT'])?$AGENT_NXT['VT_GIFT_INPUT']:array();
        $VT_GIFT_OUTPUT = isset($AGENT_NXT['VT_GIFT_OUTPUT'])?$AGENT_NXT['VT_GIFT_OUTPUT']:array();
        $NXT_OUTPUT_TYPE_CUSTOMER = CmsFormatter::$NXT_OUTPUT_TYPE_CUSTOMER;       
        $MATERIAL_GAS_FOREACH = isset($AGENT_NXT['MATERIAL_GAS_FOREACH'])?$AGENT_NXT['MATERIAL_GAS_FOREACH']:array();
        $MATERIAL_VO_FOREACH = isset($AGENT_NXT['MATERIAL_VO_FOREACH'])?$AGENT_NXT['MATERIAL_VO_FOREACH']:array();
        $MATERIAL_VT_GIFT_FOREACH = isset($AGENT_NXT['MATERIAL_VT_GIFT_FOREACH'])?$AGENT_NXT['MATERIAL_VT_GIFT_FOREACH']:array();
        $sumColumn = array();
        $key=1;
    ?>
    
    <h1>Bình Gas</h1>
    <table cellpadding="0" cellspacing="0" class="tb hm_table" style="width: 100%;">
        <thead>
            <tr>
                <th rowspan="2">STT</th>
                <th rowspan="2">Tên Vật Tư</th>
                <th rowspan="2">ĐVT</th>
                <th rowspan="2">Tồn Gas Đầu Kỳ</th>
                <th colspan="<?php echo count(CmsFormatter::$LIST_WAREHOUSE_ID)+2;?>">Nhập Gas</th>
                <th colspan="5">Xuất Gas</th>
                <th rowspan="2">Tồn Gas Cuối Kỳ</th>
            </tr>
            <tr>
                <?php foreach(CmsFormatter::$LIST_WAREHOUSE_ID as $warehouse_id):?>                    
                    <th><?php echo isset($WAREHOUSE_MODEL[$warehouse_id]) ? $WAREHOUSE_MODEL[$warehouse_id]->first_name : "";?></th>
                <?php endforeach;?>
                <th>Nhập Đại lý khác</th>
                <th>Khác</th>
                <th>Bán KH Hộ GD</th>
                <th>Bán KH mối</th>
                <th>Bán KH bình bò</th>
                <th>Xuất Đại lý khác</th>
                <th>Khác</th>
            </tr>
        </thead>
        <tbody>
                <?php foreach($MATERIAL_GAS_FOREACH as $materials_id):?>
                <?php
                    $MaterialName = $MATERIAL_MODEL[$materials_id]->materials_no.'-'.$MATERIAL_MODEL[$materials_id]->name;
                    $MaterialUnit = $MATERIAL_MODEL[$materials_id]->unit;
                    $OPENING = isset($OPENING_BALANCE_YEAR_BEFORE[$materials_id])?$OPENING_BALANCE_YEAR_BEFORE[$materials_id]:0;
                    $IMPORT = isset($IMPORT_IN_YEAR[$materials_id])?$IMPORT_IN_YEAR[$materials_id]:0;
                    $EXPORT = isset($EXPORT_IN_YEAR[$materials_id])?$EXPORT_IN_YEAR[$materials_id]:0;
                    $OPENING_BALANCE = ($OPENING+$IMPORT-$EXPORT);
                    $IMPORT_ROW = 0;
                    $EXPORT_ROW = 0;

                    $other_agent= isset($GAS_INPUT['OTHER_AGENT'][$materials_id])?$GAS_INPUT['OTHER_AGENT'][$materials_id]:0;
                    $other_obj= isset($GAS_INPUT['OTHER_OBJ'][$materials_id])?$GAS_INPUT['OTHER_OBJ'][$materials_id]:0;
                    $IMPORT_ROW+=($other_agent+$other_obj);
                    $class_remove_row='';
                    $class_focus_nxt='';
                    
                ?>
                <tr>
                    <td class="item_c"><?php echo $key;?></td>
                    <td class=""><?php echo $MaterialName;?></td>
                    <td class="item_c"><?php echo $MaterialUnit;?></td>
                    <td class="item_c"><?php echo ActiveRecord::formatCurrency($OPENING_BALANCE);?></td>
                    <?php foreach(CmsFormatter::$LIST_WAREHOUSE_ID as $warehouse_id):?>
                        <?php
                            $one_warehouse = isset($GAS_INPUT[$warehouse_id][$materials_id])?$GAS_INPUT[$warehouse_id][$materials_id]:0;
                            $IMPORT_ROW+=$one_warehouse;
                            // for sum col   
                            if(!isset($sumColumn[$warehouse_id]))
                                $sumColumn[$warehouse_id] = $one_warehouse;
                            else 
                                $sumColumn[$warehouse_id] += $one_warehouse;
                            // for sum col
                        
                        ?>
                        <td class="item_c"><?php echo $one_warehouse>0?ActiveRecord::formatCurrency($one_warehouse):'';?></td>
                    <?php endforeach;?>
                        
                    <td class="item_c"><?php echo $other_agent>0?ActiveRecord::formatCurrency($other_agent):'';?></td>
                    <td class="item_c"><?php echo $other_obj>0?ActiveRecord::formatCurrency($other_obj):'';?></td>
                    <!-- begin xuất gas -->
                    <?php foreach($NXT_OUTPUT_TYPE_CUSTOMER as $customer_type):?>
                        <?php
                            $one_customer_type = isset($GAS_OUTPUT[$customer_type][$materials_id])?$GAS_OUTPUT[$customer_type][$materials_id]:0;
                            $EXPORT_ROW+=$one_customer_type;
                            // for sum col   
                            if(!isset($sumColumn[$customer_type]))
                                $sumColumn[$customer_type] = $one_customer_type;
                            else 
                                $sumColumn[$customer_type] += $one_customer_type;
                            // for sum col
                            
                        ?>
                        <td class="item_c"><?php echo $one_customer_type>0?ActiveRecord::formatCurrency($one_customer_type):'';?></td>
                    <?php endforeach;?>
                    <?php
                        $CLOSING_BALANCE = $OPENING_BALANCE+$IMPORT_ROW-$EXPORT_ROW;
                        if($OPENING_BALANCE==0&&$CLOSING_BALANCE==0){
                            $class_remove_row='class_remove_row';
                        }else{
                            $key++;
                        }

                        if($IMPORT_ROW!=0 || $EXPORT_ROW!=0 ){
                            $class_focus_nxt = 'class_focus_nxt';
                        }
                        
                        // for sum col
                        if(!isset($sumColumn['OPENING_BALANCE']))
                            $sumColumn['OPENING_BALANCE'] = $OPENING_BALANCE;
                        else 
                            $sumColumn['OPENING_BALANCE'] += $OPENING_BALANCE;
                        if(!isset($sumColumn['CLOSING_BALANCE']))
                            $sumColumn['CLOSING_BALANCE'] = $CLOSING_BALANCE;
                        else 
                            $sumColumn['CLOSING_BALANCE'] += $CLOSING_BALANCE;                        
                        if(!isset($sumColumn['other_agent']))
                            $sumColumn['other_agent'] = $other_agent;
                        else 
                            $sumColumn['other_agent'] += $other_agent;                        
                        if(!isset($sumColumn['other_obj']))
                            $sumColumn['other_obj'] = $other_obj;
                        else 
                            $sumColumn['other_obj'] += $other_obj;                        
                        
                    ?>                        
                    <td class="item_c <?php echo $class_remove_row." ".$class_focus_nxt;?>"><?php echo ActiveRecord::formatCurrency($CLOSING_BALANCE);?></td>
                </tr>
            
                <?php endforeach; // end foreach($MATERIAL_GAS_FOREACH as $materials_id ?>
        </tbody>
        <tfoot>
            <tr>
                <td class="item_r item_b" colspan="3">Tổng Cộng</td>
                <td class="item_c item_b"><?php echo isset($sumColumn['OPENING_BALANCE'])?ActiveRecord::formatCurrency($sumColumn['OPENING_BALANCE']):'';?></td>
                <?php foreach(CmsFormatter::$LIST_WAREHOUSE_ID as $warehouse_id):?>
                <td class="item_c item_b"><?php echo isset($sumColumn[$warehouse_id])?ActiveRecord::formatCurrency($sumColumn[$warehouse_id]):'';?></td>
                <?php endforeach; // end foreach ?>
                <td class="item_c item_b"><?php echo isset($sumColumn['other_agent'])?ActiveRecord::formatCurrency($sumColumn['other_agent']):'';?></td>
                <td class="item_c item_b"><?php echo isset($sumColumn['other_obj'])?ActiveRecord::formatCurrency($sumColumn['other_obj']):'';?></td>
                <?php foreach($NXT_OUTPUT_TYPE_CUSTOMER as $customer_type):?>
                <td class="item_c item_b"><?php echo isset($sumColumn[$customer_type])?ActiveRecord::formatCurrency($sumColumn[$customer_type]):'';?></td>
                <?php endforeach; // end foreach ?>
                <td class="item_c item_b"><?php echo isset($sumColumn['CLOSING_BALANCE'])?ActiveRecord::formatCurrency($sumColumn['CLOSING_BALANCE']):'';?></td>
            </tr>
        </tfoot>
    </table>
    
    
    <h1>Vỏ bình</h1>
    <?php $key=1;$sumColumn=array();?>
    <table cellpadding="0" cellspacing="0" class="tb hm_table">
        <thead>
            <tr>
                <th rowspan="2">STT</th>
                <th rowspan="2">Tên Vật Tư</th>
                <th rowspan="2">ĐVT</th>
                <th rowspan="2">Tồn Vỏ Đầu Kỳ</th>
                <th colspan="4">Nhập Vỏ</th>
                <th colspan="<?php echo count(CmsFormatter::$LIST_WAREHOUSE_ID)+1;?>">Xuất Vỏ</th>
                <th rowspan="2">Tồn Vỏ Cuối Kỳ</th>
            </tr>
            <tr>
                <th>Thu Vỏ KH Hộ GD</th>
                <th>Thu Vỏ KH mối</th>
                <th>Thu Vỏ KH bình bò</th>
                <th>Khác</th>
                <?php foreach(CmsFormatter::$LIST_WAREHOUSE_ID as $warehouse_id):?>
                    <th><?php echo isset($WAREHOUSE_MODEL[$warehouse_id]) ? $WAREHOUSE_MODEL[$warehouse_id]->first_name : "";?></th>
                <?php endforeach;?>
                <th>Khác</th>
            </tr>
        </thead>
        <tbody>
                <?php foreach($MATERIAL_VO_FOREACH as $materials_id):?>
                <?php
                    $MaterialName = $MATERIAL_MODEL[$materials_id]->materials_no.'-'.$MATERIAL_MODEL[$materials_id]->name;
                    $MaterialUnit = $MATERIAL_MODEL[$materials_id]->unit;
                    $OPENING = isset($OPENING_BALANCE_YEAR_BEFORE[$materials_id])?$OPENING_BALANCE_YEAR_BEFORE[$materials_id]:0;
                    $IMPORT = isset($IMPORT_IN_YEAR[$materials_id])?$IMPORT_IN_YEAR[$materials_id]:0;
                    $EXPORT = isset($EXPORT_IN_YEAR[$materials_id])?$EXPORT_IN_YEAR[$materials_id]:0;
                    $OPENING_BALANCE = ($OPENING+$IMPORT-$EXPORT);
                    $IMPORT_ROW = 0;
                    $EXPORT_ROW = 0;
                    $other_obj= isset($VO_OUTPUT['OTHER_OBJ'][$materials_id])?$VO_OUTPUT['OTHER_OBJ'][$materials_id]:0;
                    $EXPORT_ROW+=($other_obj);
                    $class_remove_row='';
                    $class_focus_nxt='';
                ?>
            
                <tr>
                    <td class="item_c"><?php echo $key;?></td>
                    <td class=""><?php echo $MaterialName;?></td>
                    <td class="item_c"><?php echo $MaterialUnit;?></td>
                    <td class="item_c"><?php echo ActiveRecord::formatCurrency($OPENING_BALANCE);?></td>
                    <?php foreach($NXT_OUTPUT_TYPE_CUSTOMER as $customer_type):?>
                        <?php if($customer_type != CUSTOMER_IS_AGENT):?>
                        <?php
                            $one_customer_type = isset($VO_INPUT[$customer_type][$materials_id])?$VO_INPUT[$customer_type][$materials_id]:0;
                            $IMPORT_ROW+=$one_customer_type;
                            // for sum col   
                            if(!isset($sumColumn[$customer_type]))
                                $sumColumn[$customer_type] = $one_customer_type;
                            else 
                                $sumColumn[$customer_type] += $one_customer_type;
                            // for sum col
                        ?>
                        <td class="item_c"><?php echo $one_customer_type>0?ActiveRecord::formatCurrency($one_customer_type):'';?></td>
                        <?php endif;?>
                    <?php endforeach;?>
                    <!-- begin xuất vỏ -->
                    <?php foreach(CmsFormatter::$LIST_WAREHOUSE_ID as $warehouse_id):?>
                        <?php
                            $one_warehouse = isset($VO_OUTPUT[$warehouse_id][$materials_id])?$VO_OUTPUT[$warehouse_id][$materials_id]:0;
                            $EXPORT_ROW+=$one_warehouse;
                            // for sum col   
                            if(!isset($sumColumn[$warehouse_id]))
                                $sumColumn[$warehouse_id] = $one_warehouse;
                            else 
                                $sumColumn[$warehouse_id] += $one_warehouse;
                            // for sum col
                        ?>
                        <td class="item_c"><?php echo $one_warehouse>0?ActiveRecord::formatCurrency($one_warehouse):'';?></td>
                    <?php endforeach;?>
                    <td class="item_c"><?php echo $other_obj>0?ActiveRecord::formatCurrency($other_obj):'';?></td>
                    <?php
                        $CLOSING_BALANCE = $OPENING_BALANCE+$IMPORT_ROW-$EXPORT_ROW;
                        if($OPENING_BALANCE==0&&$CLOSING_BALANCE==0){
                            $class_remove_row='class_remove_row';
                        }else{
                            $key++;
                        }
                        if($IMPORT_ROW!=0 || $EXPORT_ROW!=0 ){
                            $class_focus_nxt = 'class_focus_nxt';
                        }
                        // for sum col
                        if(!isset($sumColumn['OPENING_BALANCE']))
                            $sumColumn['OPENING_BALANCE'] = $OPENING_BALANCE;
                        else 
                            $sumColumn['OPENING_BALANCE'] += $OPENING_BALANCE;
                        if(!isset($sumColumn['CLOSING_BALANCE']))
                            $sumColumn['CLOSING_BALANCE'] = $CLOSING_BALANCE;
                        else 
                            $sumColumn['CLOSING_BALANCE'] += $CLOSING_BALANCE;                        
                        if(!isset($sumColumn['other_obj']))
                            $sumColumn['other_obj'] = $other_obj;
                        else 
                            $sumColumn['other_obj'] += $other_obj;  
                    ?>    
                    
                    <td class="item_c <?php echo $class_remove_row." ".$class_focus_nxt;?>"><?php echo ActiveRecord::formatCurrency($CLOSING_BALANCE);?></td>                            
                </tr>
                <?php endforeach; // end foreach($MATERIAL_VO_FOREACH as $materials_id ?>
        </tbody>
        <tfoot>
            <tr>
                <td class="item_r item_b" colspan="3">Tổng Cộng</td>
                <td class="item_c item_b"><?php echo isset($sumColumn['OPENING_BALANCE'])?ActiveRecord::formatCurrency($sumColumn['OPENING_BALANCE']):'';?></td>
                <?php foreach($NXT_OUTPUT_TYPE_CUSTOMER as $customer_type):?>
                <?php if($customer_type != CUSTOMER_IS_AGENT):?>
                    <td class="item_c item_b"><?php echo isset($sumColumn[$customer_type])?ActiveRecord::formatCurrency($sumColumn[$customer_type]):'';?></td>
                <?php endif;?>
                <?php endforeach; // end foreach ?>
                <?php foreach(CmsFormatter::$LIST_WAREHOUSE_ID as $warehouse_id):?>
                <td class="item_c item_b"><?php echo isset($sumColumn[$warehouse_id])?ActiveRecord::formatCurrency($sumColumn[$warehouse_id]):'';?></td>
                <?php endforeach; // end foreach ?>
                <td class="item_c item_b"><?php echo isset($sumColumn['other_obj'])?ActiveRecord::formatCurrency($sumColumn['other_obj']):'';?></td>
                <td class="item_c item_b"><?php echo isset($sumColumn['CLOSING_BALANCE'])?ActiveRecord::formatCurrency($sumColumn['CLOSING_BALANCE']):'';?></td>
            </tr>
        </tfoot>        
    </table>
    
    
    <h1>Vật tư & quà tặng</h1>
    <?php $key=1;$sumColumn=array();?>
    <table cellpadding="0" cellspacing="0" class="tb hm_table">
        <thead>
            <tr>
                <th rowspan="2">STT</th>
                <th rowspan="2">Tên Vật Tư</th>
                <th rowspan="2">ĐVT</th>
                <th rowspan="2">Tồn VT đầu kỳ</th>
                <th colspan="<?php echo count(CmsFormatter::$NXT_VT_GIFT_INPUT);?>">Nhập vật tư</th>
                <th colspan="<?php echo count(CmsFormatter::$NXT_VT_GIFT_OUTPUT);?>">Xuất vật tư</th>
                <th rowspan="2">Tồn VT cuối kỳ</th>
            </tr>
            <tr>
                <?php foreach(CmsFormatter::$NXT_VT_GIFT_INPUT as $gift_id):?>
                    <th><?php echo CmsFormatter::$STORE_CARD_ALL_TYPE[$gift_id];?></th>
                <?php endforeach;?>
                <?php foreach(CmsFormatter::$NXT_VT_GIFT_OUTPUT as $gift_id):?>
                    <th><?php echo CmsFormatter::$STORE_CARD_ALL_TYPE[$gift_id];?></th>
                <?php endforeach;?>
            </tr>
        </thead>
        <tbody>
                <?php foreach($MATERIAL_VT_GIFT_FOREACH as $materials_id):?>
                <?php
                    $MaterialName = $MATERIAL_MODEL[$materials_id]->materials_no.'-'.$MATERIAL_MODEL[$materials_id]->name;
                    $MaterialUnit = $MATERIAL_MODEL[$materials_id]->unit;
                    $OPENING = isset($OPENING_BALANCE_YEAR_BEFORE[$materials_id])?$OPENING_BALANCE_YEAR_BEFORE[$materials_id]:0;
                    $IMPORT = isset($IMPORT_IN_YEAR[$materials_id])?$IMPORT_IN_YEAR[$materials_id]:0;
                    $EXPORT = isset($EXPORT_IN_YEAR[$materials_id])?$EXPORT_IN_YEAR[$materials_id]:0;
                    $OPENING_BALANCE = ($OPENING+$IMPORT-$EXPORT);
                    $IMPORT_ROW = 0;
                    $EXPORT_ROW = 0;
                    $class_remove_row='';
                    $class_focus_nxt='';
                ?>
            
                <tr>
                    <td class="item_c"><?php echo $key;?></td>
                    <td class=""><?php echo $MaterialName;?></td>
                    <td class="item_c"><?php echo $MaterialUnit;?></td>
                    <td class="item_c"><?php echo ActiveRecord::formatCurrency($OPENING_BALANCE);?></td>
                    <?php foreach(CmsFormatter::$NXT_VT_GIFT_INPUT as $gift_id):?>
                    <?php
                        $one_warehouse = isset($VT_GIFT_INPUT[$gift_id][$materials_id])?$VT_GIFT_INPUT[$gift_id][$materials_id]:0;
                        $IMPORT_ROW+=$one_warehouse;
                       
                        ?>
                        <td class="item_c"><?php echo $one_warehouse>0?ActiveRecord::formatCurrency($one_warehouse):'';?></td>
                    <?php endforeach;?>
                    <?php foreach(CmsFormatter::$NXT_VT_GIFT_OUTPUT as $gift_id):?>
                    <?php
                        $one_warehouse = isset($VT_GIFT_OUTPUT[$gift_id][$materials_id])?$VT_GIFT_OUTPUT[$gift_id][$materials_id]:0;
                        $EXPORT_ROW+=$one_warehouse;
                        
                        ?>
                        <td class="item_c"><?php echo $one_warehouse>0?ActiveRecord::formatCurrency($one_warehouse):'';?></td>
                    <?php endforeach;?>
                    <?php
                        $CLOSING_BALANCE = $OPENING_BALANCE+$IMPORT_ROW-$EXPORT_ROW;
                        if($OPENING_BALANCE==0&&$CLOSING_BALANCE==0){
                            $class_remove_row='class_remove_row';
                        }else{
                            $key++;
                        }
                        if($IMPORT_ROW!=0 || $EXPORT_ROW!=0 ){
                            $class_focus_nxt = 'class_focus_nxt';
                        }
                       
                    ?>                        
                    <td class="item_c <?php echo $class_remove_row." ".$class_focus_nxt;?>"><?php echo ActiveRecord::formatCurrency($CLOSING_BALANCE);?></td>    

                </tr>                
                <?php endforeach; // end foreach($MATERIAL_VT_GIFT_FOREACH as $materials_id ?>
        </tbody>
                
    </table>
    
