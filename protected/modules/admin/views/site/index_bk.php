<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->theme->baseUrl; ?>/admin/css/ad_gas.css" />
<?php include '_box_target.php';?>
<div class="clr"></div>
<?php include '_box_revenue_output.php';?>
<div class="clr"></div>
<div class="ad_read_news">
<?php
$mCms = Cms::getAllShowIndexByStatus(STATUS_ACTIVE);
if(count($mCms)>0): ?>
    <?php foreach($mCms as $cms): ?>
        <section>
            <h1 class="title"><strong><?php echo $cms->title;?></strong></h1>
            <div class="content document">
                <?php echo $cms->cms_content;?>
            </div>
            <div class='clr'></div>
        </section>
    <?php endforeach;?>
<?php endif;?>
</div>

<div class="ad_read_news">
    <section>
         <h1 class="title"><strong>Thông Báo Khác</strong></h1>
        <div class="content document">
        <?php
        $mCms = Cms::getAllActive();
        if(count($mCms)>0): ?>
            <ul>
            <?php foreach($mCms as $cms): ?>
                <li>
                    <a class="gas_link" href="<?php echo Yii::app()->createAbsoluteUrl('admin/site/news',array('id'=>$cms->id));?>">
                       <?php echo $cms->title;?>
                   </a>
                </li>
            <?php endforeach;?>
            </ul>
        <?php endif;?>
        </div>
    </section>
</div>

<script>
	$(function(){
	});
	
	function startWatching() {
        var file;

        if (typeof window.FileReader !== 'function') {
            display("The file API isn't supported on this browser yet.");
            return;
        }

        input = document.getElementById('filename');
        if (!input) {
            display("Um, couldn't find the filename element.");
        }
        else if (!input.files) {
            display("This browser doesn't seem to support the `files` property of file inputs.");
        }
        else if (!input.files[0]) {
            display("Please select a file before clicking 'Show Size'");
        }
        else {
            file = input.files[0];
            lastMod = file.lastModifiedDate;
            display("Last modified date: " + lastMod);
            display("Change the file");
            setInterval(tick, 250);
        }
    }

    function tick() {
        var file = input.files && input.files[0];
        if (file && lastMod && file.lastModifiedDate.getTime() !== lastMod.getTime()) {
            lastMod = file.lastModifiedDate;
            display("File changed: " + lastMod);
        }
    }

    function display(msg) {
        var p = document.createElement('p');
        p.innerHTML = msg;
        document.body.appendChild(p);
    }
	
</script>

