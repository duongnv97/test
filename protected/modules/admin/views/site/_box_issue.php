<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/admin/css/huongminh_ticket.css" />
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/huongminh_ticket.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/huongminh_issue.js"></script>
<?php
    $salaryText         = '[Salary: No]';
    $btnViewSalary      = '';
    if(Yii::app()->user->payment_day == UsersProfile::SALARY_YES){
//        $salaryText     = '[Salary: Yes]';
        $salaryText     = '';
        $btnViewSalary  = "<a class='btn_cancel f_size_14' href='".Yii::app()->createAbsoluteUrl('admin/ajax/SalaryInfo')."'>Xem lương, CNCN</a>";
    }

    $textHead = $btnViewSalary.$salaryText.' Công việc cần xử lý';
    $cUid = MyFormat::getCurrentUid();
    $cRole = MyFormat::getCurrentRoleId();
    $mIssue = new GasIssueTickets();
    $mIssue->unsetAttributes();  // clear any default values
    if($cUid == GasLeave::UID_DIRECTOR){
        $_GET['view_type']          = GasIssueTickets::VIEW_TYPE_GIAM_DOC;
    }else{
        $mIssue->chief_monitor_id   = MyFormat::getCurrentUid();
        $_GET['view_type']          = GasIssueTickets::VIEW_TYPE_TONG_GIAM_SAT;
        if($cUid == GasConst::UID_ADMIN){
            $mIssue->chief_monitor_id   = GasConst::UID_DUNG_NT;
        }
    }
    // all user login
    $mIssue->pageSize   = 10;
    if(in_array($cUid, $mIssue->getListUidBigPageSize())){
        $mIssue->pageSize   = 50;
    }
    $dataProvider       = $mIssue->searchOpen();
    
    $mIssueKien = new GasIssueTickets();
    $mIssueKien->unsetAttributes();  // clear any default values
    $mIssueKien->chief_monitor_id   = GasLeave::UID_HEAD_OF_LEGAL;

?>
<?php if($cUid == GasConst::UID_ADMIN): ?>
<?php $textHead .= ' - Nguyễn Tiến Dũng'; ?>
<!--Oct 12, 2018-->
<?php // $this->widget('EmployeeProjectWidget', array('mIssue'=>$mIssueKien, 'dataProvider'=> $mIssueKien->searchOpen(), 'needMore' => ['idCListView'=>GasTickets::STATUS_OPEN])); ?>
<div class="digital_ticket">
    <div class="form">
        <h1 class="title">
            <a class='btn_cancel f_size_14' href="<?php echo Yii::app()->createAbsoluteUrl('admin/gasIssueTickets/index');?>">Go To Issue</a>
            <strong>Công việc cần xử lý - Nguyễn Ngọc Kiên</strong> 
        </h1>
    </div>
    <?php $this->widget('IssueWidget', array('mIssue'=>$mIssueKien, 'dataProvider'=> $mIssueKien->searchOpen(), 'needMore' => ['idCListView'=>GasTickets::STATUS_OPEN])); ?>
</div>
<?php endif; ?>

<?php if(!in_array($cUid, $mIssueKien->getListUidNotViewCalendar()) && count($dataProvider->data) > 0): ?>
    <!--Oct 12, 2018-->
    <?php $this->widget('EmployeeProjectWidget', array('mIssue'=>$mIssue, 'dataProvider'=> $dataProvider, 'needMore' => ['idCListView'=>GasTickets::STATUS_OPEN])); ?>
<?php endif; ?>

<div class="digital_ticket">
    <div class="form">
        <h1 class="title">
            <!--<a class='btn_cancel f_size_14' href="<?php echo Yii::app()->createAbsoluteUrl('admin/gasIssueTickets/index');?>">Issue</a>-->
            <strong><?php echo $textHead;?></strong>
        </h1>
    </div>
    <?php $this->widget('IssueWidget', array('mIssue'=>$mIssue, 'dataProvider'=> $dataProvider, 'needMore' => ['idCListView'=>GasTickets::STATUS_OPEN])); ?>
</div>

<script>
    $(document).ready(function(){
        var BLOCK_UI_COLOR  = '<?php echo BLOCK_UI_COLOR;?>';
        var urlAutocomplete = '<?php echo $mIssue->getUrlSearchEmployee();?>';
        var urlAjaxUpdate   = '<?php echo Yii::app()->createAbsoluteUrl('admin/gasIssueTickets/reply'); ?>';
        fnInitIssueEvent(BLOCK_UI_COLOR, urlAutocomplete, urlAjaxUpdate);
    });
</script>