<?php Yii::app()->clientScript->registerCoreScript('jquery.ui'); ?>
<?php
$gridCss = Yii::app()->getAssetManager()->publish(Yii::getPathOfAlias('zii.widgets.assets')).'/gridview/styles.css';
Yii::app()->getClientScript()->registerCssFile($gridCss);
$OUTPUT = $data['OUTPUT'];
$DETAIL_12KG = isset($data['DETAIL_12KG'])?$data['DETAIL_12KG']:array();
$MONEY_BANK = isset($data['MONEY_BANK'])?$data['MONEY_BANK']:array();
$TOTAL_REVENUE = isset($data['TOTAL_REVENUE'])?$data['TOTAL_REVENUE']:array();
        
$MONTHS = $data['month'];
$month = $model->statistic_month;
// tab total year -- sẽ làm include sau

// tab each month
$AGENT_MODEL = Users::getArrObjectUserByRoleHaveOrder (ROLE_AGENT, 'code_account');
$_SESSION['data-excel']['AGENT_MODEL'] = $AGENT_MODEL;
$YEARS = $model->statistic_year;
$_SESSION['data-excel']['YEARS'] = $YEARS;

?>
<!--<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/jquery.floatThead.min.js"></script>    
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/underscore-min.js"></script>    -->

<script>
$(function() {
    $( "#tabs" ).tabs();
    $(".items > tbody > tr:odd").addClass("odd");
    $(".items > tbody > tr:even").addClass("even");   
//    fnTabMonthClick();
});

function fnTabMonthClick(){
     // floatThead for first tab
     var index=1;
     $('.grid-view').each(function(){
        if(index==1){
            var tableItem = $(this).find('.items').eq(0);
            $(this).find('.items').floatThead();
        }
        index++;
    });    
    // floatThead for first tab
    
    // floatThead when click switch tab
    $('.tab_month').click(function(){
        var month_current = $(this).attr('month_current');
        var tableItem = $('.table_month_'+month_current);
        tableItem.floatThead();
    });
    // floatThead when click switch tab
}

</script>


<div id="tabs">
    <ul>
        
        <?php foreach(GasStoreCard::$TYPE_CUSTOMER as $key=>$type_customer_code):?>
            <?php 
                if(!isset($DETAIL_12KG[$type_customer_code]['days'][$month]))
                continue;
                $text12 = 'Hộ Gia Đình';
                if(in_array($key, CmsFormatter::$aTypeIdBoMoi)){
                    $text12 = CmsFormatter::$CUSTOMER_BO_MOI[$key];
                }
            ?>
            <li><a class="tab_month" month_current="<?php echo $type_customer_code;?>" href="#tabs-<?php echo $type_customer_code;?>"><?php echo $text12;?> 12KG</a></li>
        <?php endforeach; // end foreach(GasStoreCard::$TYPE_CUSTOMER ?>

        <?php foreach(CmsFormatter::$MATERIAL_TYPE_OUTPUT_STATISTIC as $type_output):?>
            <?php $code = $type_output['code']; 
                $textShow = $code;
                if($code == '45KG'){
                    $textShow = 'Bình Bò';
                }
            ?>
            <li><a class="tab_month" month_current="<?php echo $code;?>" href="#tabs-<?php echo $code;?>">Tổng <?php echo $textShow;?></a></li>
        <?php endforeach; // end foreach(CmsFormatter::$MATERIAL_TYPE_OUTPUT_STATISTIC ?>
<!--        <li><a class="tab_month" month_current="MONEY_BANK" href="#tabs-MONEY_BANK">Nộp Công Ty</a></li>
        <li><a class="tab_month" month_current="TOTAL_REVENUE" href="#tabs-TOTAL_REVENUE">Doanh Thu</a></li>-->
       
<!--        <li><a class="tab_month" month_current="" href="#tabs_one">
            Tổng sản lượng gas bán tháng <?php echo date('m');?> năm <?php echo date('Y');?>
        </a></li>-->
    </ul>

    <?php include 'Revenue_output_form_detail_12kg.php'; ?>  

    <?php foreach(CmsFormatter::$MATERIAL_TYPE_OUTPUT_STATISTIC as $type_output):?>
        <?php  // lặp 2 loại bình thống kê là 12 và 45kg        
            $code = $type_output['code']; 
            if(!isset($OUTPUT[$code]['days'][$month]))
                continue;
            $days_of_month = $OUTPUT[$code]['days'][$month];
            $STT = 0;
            $textShow = $code;
            if($code == '45KG'){
                $textShow = 'Bình Bò';
            }
            
        ?>

        <div id="tabs-<?php echo $code;?>">
            <div class="grid-view grid-view-scroll">
                <div class="title_table_index">TỔNG SẢN LƯỢNG GAS BÌNH <?php echo $textShow;?> THÁNG <?php echo $month;?> NĂM <?php echo $YEARS;?></div>
                <div class="grid-view-scroll">
                    <table class="hm_table table_statistic items table_month_<?php echo $code;?>" style="">
                        <thead>
                            <tr>                               
                                <th class="w-50">Tổng</th>
                                <th class="w-50">BQ</th>
                                <?php foreach($days_of_month as $day): ?>
                                <th class="w-50" style="">
                                    <?php echo $day;?>
                                </th>
                                <?php endforeach;?>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach($OUTPUT[$code]['sum_row_total_month'][$month] as $agent_id=>$total_month_agent):?>
                            <?php $STT++;?>
                            <tr>
                                <td class="item_r item_b"><?php echo ActiveRecord::formatCurrency($total_month_agent);?></td>
                                <td class="item_r"><?php $average = round($total_month_agent/count($days_of_month)); 
                                    echo ActiveRecord::formatCurrency($average); ?>
                                </td>
                                <?php foreach($days_of_month as $day): ?>																	
                                <td class="item_r">
                                    <?php 
                                        $output_day = isset($OUTPUT[$code][$month][$day][$agent_id])?$OUTPUT[$code][$month][$day][$agent_id]:"";
                                        echo $output_day>0?ActiveRecord::formatCurrency($output_day):'';
                                    ?>
                                </td>
                                <?php endforeach; // end <?php foreach($days_of_month as $day) ?>
                            </tr>
                            <?php ?>
                            <?php endforeach; // end foreach($OUTPUT[$code]['sum_row_total_month'][$month]  ?>

                        </tbody>

                    </table><!-- end <table class="table_statistic items table_mon-->
                </div> <!-- end <div class="grid-view-scroll">-->

            </div><!-- end <div class="grid-view">-->
        </div>  <!-- end <div id="tabs --> 
    <?php endforeach; // end foreach(CmsFormatter::$MATERIAL_TYPE_OUTPUT_STATISTIC as $type_output)?>    

    <?php // include 'Revenue_output_money_bank.php'; ?>
    <?php // include 'Revenue_output_total_revenue.php'; ?>
        
</div> <!-- end <div id="tabs">-->

