<?php foreach(GasStoreCard::$TYPE_CUSTOMER as $key=>$type_customer_code):?>
    <?php 
        $code = $type_customer_code; 
        if(!isset($DETAIL_12KG[$type_customer_code]['days'][$month]))
            continue;
            $text12 = 'Hộ Gia Đình';
            if(in_array($key, CmsFormatter::$aTypeIdBoMoi)){
                $text12 = CmsFormatter::$CUSTOMER_BO_MOI[$key];
            }
        
        $days_of_month = $DETAIL_12KG[$code]['days'][$month];
        $STT = 0;                        
        ?>
    
    <div id="tabs-<?php echo $type_customer_code;?>">
        <div class="grid-view grid-view-scroll">
            <div class="title_table_index">TỔNG SẢN LƯỢNG GAS BÌNH 12KG KH <?php echo $text12;?> THÁNG <?php echo $month;?> NĂM <?php echo $YEARS;?></div>
            <div class="grid-view-scroll">
                <table class="hm_table table_statistic items table_month_<?php echo $type_customer_code;?>" style="">
                    <thead>
                        <tr>                               
                            <th class="w-50">Tổng</th>
                            <th class="w-50">BQ</th>
                            <?php foreach($days_of_month as $day): ?>
                            <th class="w-50" style="">
                                <?php echo $day;?>
                            </th>
                            <?php endforeach;?>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach($DETAIL_12KG[$code]['sum_row_total_month'][$month] as $agent_id=>$total_month_agent):?>
                        <?php $STT++;?>
                        <tr>
                            <td class="item_r item_b"><?php echo ActiveRecord::formatCurrency($total_month_agent);?></td>
                            <td class="item_r"><?php $average = round($total_month_agent/count($days_of_month)); 
                                echo ActiveRecord::formatCurrency($average); ?>
                            </td>
                            <?php foreach($days_of_month as $day): ?>																	
                            <td class="item_r">
                                <?php 
                                    $output_day = isset($DETAIL_12KG[$code][$month][$day][$agent_id])?$DETAIL_12KG[$code][$month][$day][$agent_id]:"";
                                    echo $output_day>0?ActiveRecord::formatCurrency($output_day):'';
                                ?>
                            </td>
                            <?php endforeach; // end <?php foreach($days_of_month as $day) ?>
                        </tr>
                        <?php ?>
                        <?php endforeach; // end foreach($DETAIL_12KG[$code]['sum_row_total_month'][$month]  ?>
                        <!-- tổng cộng -->
                        <?php $sum_col_total_month_all_agent = $DETAIL_12KG[$code]['sum_col_total_month_all_agent'][$month];
                        $average = round($sum_col_total_month_all_agent/count($days_of_month)); ?>
                        
                    </tbody>

                </table><!-- end <table class="table_statistic items table_mon-->
            </div> <!-- end <div class="grid-view-scroll">-->
            
        </div><!-- end <div class="grid-view">-->
    </div> <!-- end <div id="tabs --> 
<?php endforeach; // end foreach(GasStoreCard::$TYPE_CUSTOMER as $key=>$type_customer_code): ?>  