<?php
$this->breadcrumbs=array(
    $this->pluralTitle => array('index'),
    ' ' . $this->singleTitle . ' : ',
);

$menus = array(
	array('label'=>"$this->pluralTitle Management", 'url'=>array('index')),
	array('label'=>"Xóa $this->singleTitle", 'url'=>array('delete'), 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Bạn chắc chắn muốn xóa ?')),
);
$this->menu= ControllerActionsName::createMenusRoles($menus, $actions);
?>

<h1>Xem: <?php echo $this->singleTitle; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
            array(
                'name' => 'type',
                'type' => 'html',
                'value' => $model->getTypeError(),
            ),
            array(
                'name' => 'agent_id',
                'type' => 'html',
                'value' => $model->getAgent(),
            ),
            array(
                'name' => 'employee_id',
                'type' => 'html',
                'value' => $model->getEmployee(),
            ),
            array(
                'name' => 'created_date',
                'type' => 'html',
                'value' => $model->getCreatedDate(),
            ),
	),
)); ?>
