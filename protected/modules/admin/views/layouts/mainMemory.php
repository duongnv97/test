<?php if(isset(Yii::app()->user->id) && Yii::app()->user->role_id == ROLE_ADMIN): ?>
    Execution Time: <?php $log = new CLogger();
    echo round($log->getExecutionTime(),3);?> sec Memory Usage: <?php echo round(memory_get_usage(false)/1048576,2);?> MB
<?php endif; ?>