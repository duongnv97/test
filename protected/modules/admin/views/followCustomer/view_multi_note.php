<h1>Xem ghi chú: <?php echo $model->rCustomer->first_name; ?></h1>
<p>Đ/C: <?php echo $model->rCustomer->address; ?></p>
<?php
    $data = FollowCustomer::model()->getNoteById($_GET['id']);
    $this->widget('zii.widgets.grid.CGridView', array(
        'id'=>'view-multi-note-grid',
        'dataProvider'=>$data,
        'afterAjaxUpdate'=>'function(id, data){ fnUpdateColorbox();}',
        'template'=>'{pager}{summary}{items}{pager}{summary}',
//    'itemsCssClass' => 'items custom_here',
//    'htmlOptions'=> array('class'=>'grid-view custom_here'),
        'pager' => array(
            'maxButtonCount'=>  CmsFormatter::$PAGE_MAX_BUTTON,
        ),
        'enableSorting' => false,
        //'filter'=>$model,
        'columns'=>array(
        array(
            'header' => 'S/N',
            'type' => 'raw',
            'value' => '$this->grid->dataProvider->pagination->currentPage * $this->grid->dataProvider->pagination->pageSize + ($row+1)',
            'headerHtmlOptions' => array('width' => '10px','style' => 'text-align:center;'),
            'htmlOptions' => array('style' => 'text-align:center;')
        ),
        array(
            'name'=>'customer_id',
            'type'=>'raw',
            'value'=>'$data->getCustomerInfo()'
        ),
        array(
            'name'=>'employee_id',
            'type'=>'raw',
            'value'=>'$data->getEmployeeInfo()'
        ),
        array(
            'header'=>'EXT',
            'value'=>'$data->getJsonDataField("employee_ext")'
        ),
        array(
            'name'=>'status',
            'type'=>'raw',
            'value'=>'$data->getStatus()'
        ),
        array(
            'name'=>'status_cancel',
            'type'=>'raw',
            'value'=>'$data->getStatusCancel()'
        ),
        array(
            'name'=>'send_sms',
            'type'=>'raw',
            'value'=>'$data->sms_phone'
        ),
        array(
            'name'=>'appointment_date',
            'type'=>'raw',
            'value'=>'$data->getAppointmentDate()'
        ),
        array(
            'name'=>'note',
            'type'=>'raw',
            'value'=>'$data->getNote()'
        ),
        array(
            'name'=>'created_date',
            'type'=>'raw',
            'value'=>'$data->getCreatedDate()'
        ),
        array(
            'header' => 'Actions',
            'class'=>'CButtonColumn',
            'template'=> ControllerActionsName::createIndexButtonRoles($actions),
            'buttons'=>array(
                'update'=>array(
                    'visible'=> '$data->canUpdateNote()',
                ),
                'delete'=>array(
                    'visible'=> '$data->canUpdateNote()',
                ),
            )
        )
    )));
        
?>
