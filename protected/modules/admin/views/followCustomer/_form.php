<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'follow-customer-form',
	'enableAjaxValidation'=>false,
        'htmlOptions' => array('enctype' => 'multipart/form-data'),
)); ?>

    <p class="note">Dòng có dấu <span class="required">*</span> là bắt buộc.</p>
    <?php echo MyFormat::BindNotifyMsg(); ?>
    

    <div class="row more_col">
        <div class="row">
            <?php echo $form->labelEx($model,'appointment_date'); ?>
             <?php 
                $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                    'model'=>$model,        
                    'attribute'=>'appointment_date',
                    'options'=>array(
                        'showAnim'=>'fold',
                        'dateFormat'=> ActiveRecord::getDateFormatJquery(),
                        'changeMonth' => true,
                        'changeYear' => true,
                        'showOn' => 'button',
                        'buttonImage'=> Yii::app()->theme->baseUrl.'/admin/images/icon_calendar_r.gif',
                        'buttonImageOnly'=> true,                                
                    ),        
                    'htmlOptions'=>array(
//                        'class'=>'w-16',
//                        'style'=>'height:20px;',
                        'readonly'=>'readonly',
                    ),
                ));
                ?>  
            <?php echo $form->error($model,'appointment_date'); ?>
        </div>
    </div>
    
    <div class="row">
        <?php echo $form->labelEx($model,'note'); ?>
        <?php echo $form->textArea($model,'note',array('rows'=>5, 'cols'=>100)); ?>
        <?php echo $form->error($model,'note'); ?>
    </div>
    <div class="clr"></div>
    <div class="row GasCheckboxList float_l"  style="">
        <?php echo $form->labelEx($model,'status', ['class'=>'']); ?>
        <div style="margin-left: 140px">
            <?php echo $form->radioButtonList($model,'status', $model->getArrayCallStatus(), 
                        array(
                            'separator'=>"",
                            'template'=>'<li>{input}{label}</li>',
                            'container'=>'ul',
                            'class'=>'RadioOrderType',
//                            'headerHtmlOptions'=>'height: auto',
                        )); 
            ?>
        </div>
        <?php echo $form->error($model,'status'); ?>
    </div>
    <div class="clr"></div><br>
    <div class="row stt_cancelDL">
        <?php echo $form->labelEx($model,'status_cancel'); ?>
        <?php echo $form->dropdownList($model,'status_cancel', $model->getArrayStatusCancel(), array('empty'=>'Select')); ?>
    </div>
    
    <br><hr>
    <?php if($model->rCustomer): ?>
        <?php include '_formSms.php'; ?>
    <?php endif; ?>
    <div class="row buttons" style="padding-left: 141px;">
        <?php $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType'=>'submit',
            'label'=>$model->isNewRecord ? 'Create' :'Save',
            'type'=>'null', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
            'size'=>'small', // null, 'large', 'small' or 'mini'
            //'htmlOptions' => array('style' => 'margin-bottom: 10px; float: right;'),
        )); ?>
    </div>

<?php $this->endWidget(); ?>
    
<?php include 'view_multi_note.php' ?>

</div><!-- form -->
<style>
    .GasCheckboxList ul { padding: 0;}
</style>

<script>
    $(document).ready(function(){
        $('.form').find('button:submit').click(function(){
            $.blockUI({ overlayCSS: { backgroundColor: '#fff' } }); 
        });
        
    });
    //for update view
    $('input:radio[name="FollowCustomer[status]"]').each(
        function(){
            if ($(this).is(':checked') && $(this).val() == '3') {
                $('.stt_cancelDL').show();
            }
        }
    );
//    //if check "KH tu choi" will display dropdownList status cancel
//    $('input:radio[name="FollowCustomer[status]"]').change(
//        function(){
//            if ($(this).is(':checked') && $(this).val() == '3') {
//                $('.stt_cancelDL').show();
//                $('#FollowCustomer_status_cancel option:first-child').attr("selected", "selected");
//            } else {
//                $('.stt_cancelDL').hide();
//            }
//        }
//    );
    
</script>