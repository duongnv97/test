<?php
$this->breadcrumbs=array(
	$this->pluralTitle=>array('index'),
	'Tạo Mới',
);

$menus = array(		
        array('label'=>"$this->singleTitle Management", 'url'=>array('index')),
);
$this->menu= ControllerActionsName::createMenusRoles($menus, $actions);

?>

<h1>EXT: <?php echo CacheSession::getCookie(CacheSession::CURRENT_USER_EXT);?> - Tạo Mới <?php echo $this->singleTitle. " - KH: {$model->rCustomer->first_name} - {$model->getPhoneClickCall()}"; ?></h1>
<p>Đ/C: <?php echo $model->rCustomer->address; ?></p>

<?php echo $this->renderPartial('_form', array('model'=>$model, 'actions'=>$actions)); ?>