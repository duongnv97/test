<div class="form">
    <br><hr>
<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'follow-customer-form',
	'enableAjaxValidation'=>false,
        'htmlOptions' => array('enctype' => 'multipart/form-data'),
)); ?>
    <?php echo MyFormat::BindNotifyMsg(); ?>
    
    <div class="row ">
            <?php echo $form->labelEx($model,'bill_duration',array('label'=>'Giây gọi')); ?>
            <?php echo $form->textField($model,'bill_duration', array('class'=>'number_only_v1 align_l', 'maxlength'=>5)); ?>
            <?php echo $form->error($model,'bill_duration'); ?>
    </div>
    
    <div class="row buttons" style="padding-left: 141px;">
        <?php $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType'=>'submit',
            'label'=>$model->isNewRecord ? 'Create' :'Save',
            'type'=>'null', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
            'size'=>'small', // null, 'large', 'small' or 'mini'
            //'htmlOptions' => array('style' => 'margin-bottom: 10px; float: right;'),
        )); ?>
    </div>

<?php $this->endWidget(); ?>
    
</div><!-- form -->

