<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=> GasCheck::getCurl(),
	'method'=>'get',
)); ?>

	<?php if(Yii::app()->user->role_id!=ROLE_SUB_USER_AGENT):?>
            <div class="row">
            <?php echo $form->labelEx($model,'customer_id'); ?>
            <?php echo $form->hiddenField($model,'customer_id', array('class'=>'')); ?>
            <?php
                    // 1. limit search kh của sale
                    $url = Yii::app()->createAbsoluteUrl('admin/ajax/searchHgd');
                    // widget auto complete search user customer and supplier
                    $aData = array(
                        'model'=>$model,
                        'field_customer_id'=>'customer_id',
                        'url'=> $url,
                        'name_relation_user'=>'rCustomer',
                        'ClassAdd' => 'w-400',
                        'field_autocomplete_name' => 'autocomplete_customer',
                        'placeholder'=>'Nhập mã hoặc tên khách hàng',
                    );
                    $this->widget('ext.SpaAutocompleteCustomer.SpaAutocompleteCustomer',
                        array('data'=>$aData));
                    ?>
                <?php echo $form->error($model,'customer_id'); ?>
            </div>
        <?php endif; ?>
        <?php if(Yii::app()->user->role_id!=ROLE_SUB_USER_AGENT):?>
            <div class="row">
            <?php echo $form->labelEx($model,'employee_id'); ?>
            <?php echo $form->hiddenField($model,'employee_id', array('class'=>'')); ?>
            <?php
                    // 1. limit search kh của sale
                    $url = Yii::app()->createAbsoluteUrl('admin/ajax/search_by_role', array('role'=> implode(',', [ROLE_TELESALE, ROLE_CALL_CENTER, ROLE_DIEU_PHOI])));
                    // widget auto complete search user customer and supplier
                    $aData = array(
                        'model'=>$model,
                        'field_customer_id'=>'employee_id',
                        'url'=> $url,
                        'name_relation_user'=>'rEmployee',
                        'ClassAdd' => 'w-400',
                        'field_autocomplete_name' => 'autocomplete_employee',
                        'placeholder'=>'Nhập mã hoặc tên nhân viên',
                    );
                    $this->widget('ext.SpaAutocompleteCustomer.SpaAutocompleteCustomer',
                        array('data'=>$aData));
                    ?>
                <?php echo $form->error($model,'employee_id'); ?>
            </div>
        <?php endif; ?>
        <div class="row more_col">
            <div class="col1">
                <?php echo $form->labelEx($model,'date_appointment_from'); ?>
                 <?php 
                    $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                        'model'=>$model,        
                        'attribute'=>'date_appointment_from',
                        'options'=>array(
                            'showAnim'=>'fold',
                            'dateFormat'=> MyFormat::$dateFormatSearch,
                            'changeMonth' => true,
                            'changeYear' => true,
                            'showOn' => 'button',
                            'buttonImage'=> Yii::app()->theme->baseUrl.'/admin/images/icon_calendar_r.gif',
                            'buttonImageOnly'=> true,                                
                        ),        
                        'htmlOptions'=>array(
                            'class'=>'w-16',
                            'style'=>'height:20px;',
                                
                        ),
                    ));
                    ?>  
                <?php echo $form->error($model,'date_appointment_from'); ?>
            </div>
            <div class="col2">
                <?php echo $form->labelEx($model,'date_appointment_to'); ?>
                 <?php 
                    $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                        'model'=>$model,        
                        'attribute'=>'date_appointment_to',
                        'options'=>array(
                            'showAnim'=>'fold',
                            'dateFormat'=> MyFormat::$dateFormatSearch,
                            'changeMonth' => true,
                            'changeYear' => true,
                            'showOn' => 'button',
                            'buttonImage'=> Yii::app()->theme->baseUrl.'/admin/images/icon_calendar_r.gif',
                            'buttonImageOnly'=> true,                                
                        ),        
                        'htmlOptions'=>array(
                            'class'=>'w-16',
                            'style'=>'height:20px;',
                                
                        ),
                    ));
                    ?>  
                <?php echo $form->error($model,'date_appointment_to'); ?>
            </div>
        </div>
    
        <div class="row">
            <?php echo $form->labelEx($model,'status'); ?>
            <?php echo $form->dropdownList($model,'status', $model->getArrayCallStatus(), array('empty'=>'Select')); ?>
            <?php echo $form->error($model,'status'); ?>
        </div>
    
        <div class="row">
            <?php echo $form->labelEx($model,'status_cancel'); ?>
            <?php echo $form->dropdownList($model,'status_cancel', $model->getArrayStatusCancel(), array('empty'=>'Select')); ?>
            <?php echo $form->error($model,'status_cancel'); ?>
        </div>
    
        <div class="row more_col">
            <div class="col1">
                <?php echo $form->labelEx($model,'search_second_from', ['label'=>'Giây gọi từ']); ?>
                <?php echo $form->textField($model,'search_second_from'); ?>
                <?php echo $form->error($model,'search_second_from'); ?>
            </div>
            
            <div class="col2">
                <?php echo $form->labelEx($model,'search_second_to',['label'=>'Đến']); ?>
                <?php echo $form->textField($model,'search_second_to'); ?>
                <?php echo $form->error($model,'search_second_to'); ?>
            </div>
        </div>

	<div class="row buttons" style="padding-left: 159px;">
		        <?php $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType'=>'submit',
            'label'=>'Search',
            'type'=>'null', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
            'size'=>'small', // null, 'large', 'small' or 'mini'
            //'htmlOptions' => array('style' => 'margin-bottom: 10px; float: right;'),
        )); ?>	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->