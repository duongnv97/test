<?php
$this->breadcrumbs=array(
    $this->pluralTitle => array('index'),
    ' ' . $this->singleTitle . ' : ',
);

$menus = array(
	array('label'=>"$this->pluralTitle Management", 'url'=>array('index')),
	array('label'=>"Tạo Mới $this->singleTitle", 'url'=>array('create')),
	array('label'=>"Cập Nhật $this->singleTitle", 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>"Xóa $this->singleTitle", 'url'=>array('delete'), 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Bạn chắc chắn muốn xóa ?')),
);
$this->menu= ControllerActionsName::createMenusRoles($menus, $actions);
$model->bill_duration = !empty($model->bill_duration) ? $model->bill_duration : '';
?>
<center><h1>Xem: <?php echo $model->getCustomerInfo(); ?></h1></center>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		//'id',
                array(
                    'name' => 'employee_id',
                    'value' => $model->getEmployeeInfo(),
                ),
                array(
                    'label'=>'Giây gọi',
                    'type'=>'html',
                    'value' => $model->getCallViewOnGrid(),
                ),
                array(
                    'name' => 'customer_id',
                    'type'=>'html',
                    'value' => $model->getCustomerInfo()."<br>".$model->getCustomerInfo("address"),
                ),
                array(
                    'label' => 'SĐT',
                    'value' => $model->getCustomerInfo("phone"),
                ),
                array(
                    'label' => 'Loại KH',
                    'value' => $model->getTypeCustomer(),
                ),
                array(
                    'label' => 'Đại lý',
                    'value' => $model->getAgentOfCustomer(),
                ),
                
                array(
                    'name' => 'appointment_date',
                    'value' => $model->getAppointmentDate(),
                ),
                array(
                    'name' => 'created_date',
                    'value' => $model->getCreatedDate(),
                ), 
                array(
                    'name' => 'note',
                    'value' => $model->getNote(),
                ), 
	),
)); 

if($model->canEditCall()){
    include "_from_edit_call.php";
}
?>
