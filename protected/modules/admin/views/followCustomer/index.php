<?php
$this->breadcrumbs=array(
	$this->singleTitle,
);

$menus=array(
            array('label'=>"Create $this->singleTitle", 'url'=>array('create')),
);
//$this->menu= ControllerActionsName::createMenusRoles($menus, $actions);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('follow-customer-grid', {
                url : $(this).attr('action'),
		data: $(this).serialize()
	});
	return false;
});
");

Yii::app()->clientScript->registerScript('ajaxupdate', "
$('#follow-customer-grid a.ajaxupdate').live('click', function() {
    $.fn.yiiGridView.update('follow-customer-grid', {
        type: 'POST',
        url: $(this).attr('href'),
        success: function() {
            $.fn.yiiGridView.update('follow-customer-grid');
        }
    });
    return false;
});
");
?>

<h1>Danh Sách <?php echo $this->pluralTitle; ?></h1>


<?php echo CHtml::link('Tìm Kiếm Nâng Cao','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'follow-customer-grid',
	'dataProvider'=>$model->search(),
        'afterAjaxUpdate'=>'function(id, data){ fnUpdateColorbox();}',
        'template'=>'{pager}{summary}{items}{pager}{summary}',
//    'itemsCssClass' => 'items custom_here',
//    'htmlOptions'=> array('class'=>'grid-view custom_here'),
        'pager' => array(
            'maxButtonCount'=>  CmsFormatter::$PAGE_MAX_BUTTON,
        ),
	'enableSorting' => false,
	//'filter'=>$model,
	'columns'=>array(
        array(
            'header' => 'S/N',
            'type' => 'raw',
            'value' => '$this->grid->dataProvider->pagination->currentPage * $this->grid->dataProvider->pagination->pageSize + ($row+1)',
            'headerHtmlOptions' => array('width' => '10px','style' => 'text-align:center;'),
            'htmlOptions' => array('style' => 'text-align:center;')
        ),
            array(
                'header' => 'Đại lý',
                'type'=>'raw',
                'value'=>'$data->getAgentOfCustomer()',
                //function of tbl Users
            ), 
            array(
                'name'=>'customer_id',
                'type'=>'raw',
                'value'=>'$data->getCustomerInfo()."<br>".$data->getCustomerInfo("address")'
            ),
            array(
                'header' => 'Loại KH',
                'value'=>'$data->getTypeCustomer()',
                'htmlOptions' => array('style' => 'width:50px;')
            ),
            array(
                'header'=>'SĐT',
                'type'=>'raw',
                'value'=>'$data->getCustomerInfo("phone")'
            ),
            array(
                'name'=>'employee_id',
                'type'=>'raw',
                'value'=>'$data->getEmployeeInfo()'
            ),
            array(
                'name'=>'appointment_date',
                'type'=>'raw',
                'value'=>'$data->getAppointmentDate()'
            ),
            array(
                'name'=>'note',
                'type'=>'raw',
                'value'=>'$data->getNote()'
            ),
            array(
                'header'=>'Giây gọi',
                'type'=>'raw',
                'value'=>'$data->getCallViewOnGrid()',
                'htmlOptions' => array('style' => 'text-align:center;')
            ),
            array(
                'name'=>'status',
                'type'=>'raw',
                'value'=>'$data->getStatus()'
            ),
            array(
                'name'=>'status_cancel',
                'type'=>'raw',
                'value'=>'$data->getStatusCancel()'
            ),
            array(
                'name'=>'created_date',
                'type'=>'raw',
                'value'=>'$data->getCreatedDate()'
            ),
//		array(
//                    'name'=>'created_date_only',
//                    'type'=>'raw',
//                    'value'=>'$data->getCreatedDateOnly()'
//                ),
		array(
                    'header' => 'Actions',
                    'class'=>'CButtonColumn',
                    'template'=> ControllerActionsName::createIndexButtonRoles($actions),
                    'buttons'=>array(
//                        'update' => array(
//                            'visible' => '$data->canUpdate()',
//                        ),
//                        'delete'=>array(
//                            'visible'=> 'GasCheck::canDeleteData($data)',
//                        ),
                    ),
		),
	),
)); ?>

<script>
$(document).ready(function() {
    fnUpdateColorbox();
});

function fnUpdateColorbox(){   
    fixTargetBlank();
    $(".view").colorbox({iframe:true,innerHeight:'1500', innerWidth: '800',close: "<span title='close'>close</span>"});
    $(".update").colorbox({iframe:true,innerHeight:'1500', innerWidth: '1000',close: "<span title='close'>close</span>"});
}
</script>