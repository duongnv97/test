<?php 
$mCustomer  = $model->rCustomer;
$temp       = explode('-', $mCustomer->phone);
$aPhone     = [];
$aPhone[-1] = 'Không gửi';
foreach($temp as $v){ 
    $aPhone[$v] = $v;
}
?>
<?php if(count($aPhone)): ?>
<div class="clr"></div>
<div class="row GasCheckboxList float_l"  style="">
    <?php echo $form->labelEx($model,'send_sms', ['class'=>'']); ?>
    <div style="margin-left: 141px">
        <?php echo $form->radioButtonList($model,'send_sms', $aPhone,
                    array(
                        'separator'=>"",
                        'template'=>'<li>{input}{label}</li>',
                        'container'=>'ul',
                        'class'=>'RadioOrderType',
                    )); 
        ?>
    </div>
    <?php echo $form->error($model,'send_sms'); ?>
</div>
<div class="clr"></div>
<!--<br><br><br>-->
<?php endif; ?>
