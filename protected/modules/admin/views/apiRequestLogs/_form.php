<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'api-request-logs-form',
	'enableAjaxValidation'=>false,
        'htmlOptions' => array('enctype' => 'multipart/form-data'),
)); ?>

    <p class="note">Dòng có dấu <span class="required">*</span> là bắt buộc.</p>
    <?php echo MyFormat::BindNotifyMsg(); ?>
    
        		
        
    <div class="row">
        <?php echo $form->labelEx($model,'ip_address'); ?>
        <?php echo $form->textField($model,'ip_address',array('size'=>50,'maxlength'=>50)); ?>
        <?php echo $form->error($model,'ip_address'); ?>
    </div>
    <div class="row">
        <?php echo $form->labelEx($model,'country'); ?>
        <?php echo $form->textField($model,'country',array('size'=>60,'maxlength'=>200)); ?>
        <?php echo $form->error($model,'country'); ?>
    </div>
    <div class="row">
        <?php echo $form->labelEx($model,'user_id'); ?>
        <?php echo $form->textField($model,'user_id',array('size'=>11,'maxlength'=>11)); ?>
        <?php echo $form->error($model,'user_id'); ?>
    </div>
    <div class="row">
        <?php echo $form->labelEx($model,'method'); ?>
        <?php echo $form->textArea($model,'method',array('rows'=>6, 'cols'=>50)); ?>
        <?php echo $form->error($model,'method'); ?>
    </div>
    <div class="row">
        <?php echo $form->labelEx($model,'content'); ?>
        <?php echo $form->textArea($model,'content',array('rows'=>6, 'cols'=>50)); ?>
        <?php echo $form->error($model,'content'); ?>
    </div>
    <div class="row">
        <?php echo $form->labelEx($model,'response'); ?>
        <?php echo $form->textArea($model,'response',array('rows'=>6, 'cols'=>50)); ?>
        <?php echo $form->error($model,'response'); ?>
    </div>
    <div class="row">
        <?php echo $form->labelEx($model,'created_date'); ?>
        <?php echo $form->textField($model,'created_date'); ?>
        <?php echo $form->error($model,'created_date'); ?>
    </div>
    <div class="row">
        <?php echo $form->labelEx($model,'responsed_date'); ?>
        <?php echo $form->textField($model,'responsed_date'); ?>
        <?php echo $form->error($model,'responsed_date'); ?>
    </div>
	<div class="row buttons" style="padding-left: 141px;">
		        <?php $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType'=>'submit',
            'label'=>$model->isNewRecord ? 'Create' :'Save',
            'type'=>'null', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
            'size'=>'small', // null, 'large', 'small' or 'mini'
            //'htmlOptions' => array('style' => 'margin-bottom: 10px; float: right;'),
        )); ?>	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->
<script>
    $(document).ready(function(){
        $('.form').find('button:submit').click(function(){
            $.blockUI({ overlayCSS: { backgroundColor: '#fff' } }); 
        });
        
    });
</script>