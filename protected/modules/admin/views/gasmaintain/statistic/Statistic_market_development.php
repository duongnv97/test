<h1><?php echo Yii::t('translation', 'Thống Kê Phát Triển Thị Trường'); ?></h1>

<?php //echo CHtml::link(Yii::t('translation','Advanced Search'),'#',array('class'=>'search-button')); ?>
<div class="search-form" style="">
    <div class="wide form">

    <?php $form=$this->beginWidget('CActiveForm', array(
            'action'=>Yii::app()->createUrl($this->route),
            'method'=>'post',
    )); ?>
            <?php if(Yii::app()->user->hasFlash('successUpdate')): ?>
                <div class="success_div"><?php echo Yii::app()->user->getFlash('successUpdate');?></div>         
            <?php endif; ?>    
            <div class="row">
                    <?php echo Yii::t('translation', $form->labelEx($model,'province_id')); ?>
                    <?php echo $form->dropDownList($model,'province_id', GasProvince::getArrAll(),array('style'=>'width:169px', 'empty'=>'Select')); ?>
            </div> 	
                
            <div class="row">
                    <?php echo Yii::t('translation', $form->label($model,'date_from')); ?>		
                    <?php 
                        $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                            'model'=>$model,        
                            'attribute'=>'date_from',
                            'options'=>array(
                                'showAnim'=>'fold',
                                'dateFormat'=> 'dd-mm-yy',
    //                            'minDate'=> '0',
                                'changeMonth' => true,
                                'changeYear' => true,
                                'showOn' => 'button',
                                'buttonImage'=> Yii::app()->theme->baseUrl.'/admin/images/icon_calendar_r.gif',
                                'buttonImageOnly'=> true,                                
                            ),        
                            'htmlOptions'=>array(
                                'class'=>'w-16',
                                'style'=>'height:20px;',
    //                                'readonly'=>'readonly',
                            ),
                        ));
                    ?>            
                    <?php echo $form->error($model,'date_from'); ?>
            </div>

            <div class="row">
                    <?php echo Yii::t('translation', $form->label($model,'date_to')); ?>
                    <?php 
                        $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                            'model'=>$model,        
                            'attribute'=>'date_to',
                            'options'=>array(
                                'showAnim'=>'fold',
                                'dateFormat'=> 'dd-mm-yy',
    //                            'minDate'=> '0',
                                'changeMonth' => true,
                                'changeYear' => true,
                                'showOn' => 'button',
                                'buttonImage'=> Yii::app()->theme->baseUrl.'/admin/images/icon_calendar_r.gif',
                                'buttonImageOnly'=> true,                                
                            ),        
                            'htmlOptions'=>array(
                                'class'=>'w-16',
                                'style'=>'height:20px;',
    //                                'readonly'=>'readonly',
                            ),
                        ));
                    ?>  		
                    <?php echo $form->error($model,'date_to'); ?>
            </div>



            <div class="row buttons" style="padding-left: 159px;">
                            <?php $this->widget('bootstrap.widgets.TbButton', array(
                'buttonType'=>'submit',
                'label'=>Yii::t('translation','Xem Thống Kê'),
                'type'=>'null', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
                'size'=>'small', // null, 'large', 'small' or 'mini'
                //'htmlOptions' => array('style' => 'margin-bottom: 10px; float: right;'),
            )); ?>	
                    <?php if(ControllerActionsName::isAccessAction('statistic_maintain_export_excel', $actions)):?>
                            <input class='statistic_maintain_export_excel' next='<?php echo Yii::app()->createAbsoluteUrl('admin/gasmaintain/statistic_maintain_export_excel');?>' type='button' value='Xuất Thống Kê Hiện Tại Ra Excel'>
                    <?php endif;?>

                    </div>

    <?php $this->endWidget(); ?>

    </div><!-- search-form -->
</div><!-- search-form -->

 <script>
$(function() {
	$('.statistic_maintain_export_excel').click(function(){
		window.location = $(this).attr('next');
	});
	
	$('.grid-view table tbody tr').click(function()
	{
		$('.grid-view table tbody tr').removeClass('selected');
		$(this).addClass('selected');
	});
        $('.form').find('button:submit').click(function(){
            $.blockUI({ overlayCSS: { backgroundColor: '<?php echo BLOCK_UI_COLOR;?>' } }); 
        });         

});
</script>

<?php if(count($resultSta)>0): ?>
<?php Yii::app()->clientScript->registerCoreScript('jquery.ui'); ?>
 <script>
$(function() {

	$( "#tabs" ).tabs();
    $(".items > tbody > tr:odd").addClass("odd");
    $(".items > tbody > tr:even").addClass("even");   
	
	$('.statistic_maintain_export_excel').click(function(){
		window.location = $(this).attr('next');
	});
	
});
</script>

<?php
$gridCss = Yii::app()->getAssetManager()->publish(Yii::getPathOfAlias('zii.widgets.assets')).'/gridview/styles.css';
Yii::app()->getClientScript()->registerCssFile($gridCss);
?>

<div id="tabs">
<ul>
	<li><a href="#tabs-1">Nhân Viên Phát Triển Thị Trường</a></li>
</ul>
        <?php 
            $AGENT_DATA = $resultSta['AGENT'];
            $MONITOR_MODEL = Users::getArrObjectUserByRoleHaveOrder (ROLE_MONITORING_MARKET_DEVELOPMENT, 'code_account');
            $_SESSION['data-excel']['MONITOR_MODEL'] = $MONITOR_MODEL;
            $MAINTAIN_EMPLOYEE_DATA = $resultSta['MAINTAIN_EMPLOYEE']; // Nhân viên bảo trì,(role nv chăm sóc kh)
            $MAINTAIN_EMPLOYEE_ACCOUNTING_DATA = $resultSta['MAINTAIN_EMPLOYEE_ACCOUNTING'];// nhân viên kế toán bán hàng			
            $MAINTAIN_EMPLOYEE_MODEL = Users::getSelectEmployeeForAgent(ONE_MONITORING_MARKET_DEVELOPMENT, $MONITOR_MODEL);
            $MAINTAIN_EMPLOYEE_ACCOUNTING_MODEL = Users::getSelectEmployeeForAgent(ONE_AGENT_ACCOUNTING, $MONITOR_MODEL);	
            $_SESSION['data-excel']['MAINTAIN_EMPLOYEE_MODEL'] = $MAINTAIN_EMPLOYEE_MODEL;			
            $_SESSION['data-excel']['MAINTAIN_EMPLOYEE_ACCOUNTING_MODEL'] = $MAINTAIN_EMPLOYEE_ACCOUNTING_MODEL;			
        ?>    
	
	<div id="tabs-1">
		<div class="grid-view">
			<table class="items">
				<thead>
				<tr>
					<!--<th>Đại Lý</th>-->
					<th>Nhân Viên PTTT</th>
					<th>Điểm Đã Đi</th>
					<th>Cuộc Gọi Tốt</th>
					<th>Cuộc Gọi Xấu</th>
				</tr>
				</thead>

				<tbody>	
					<tr class="">
						<td class="item_b">Tổng Cộng:</td>
						<!--<td class="item_b"></td>-->
						<td class="item_c item_b"><?php echo $MAINTAIN_EMPLOYEE_DATA['CountIsMaintain']['total_count'];?></td>
						<td class="item_c item_b"><?php echo $MAINTAIN_EMPLOYEE_DATA['CountIsMaintainCallGood']['total_count'];?></td>
						<td class="item_c item_b"><?php echo $MAINTAIN_EMPLOYEE_DATA['CountIsMaintainCallBad']['total_count'];?></td>
					</tr>				
				<?php foreach($MAINTAIN_EMPLOYEE_MODEL as $agent_id=>$arrModel): ?>				
					<?php
						$mAgent = $arrModel['mAgent'];
						$mEmployee = $arrModel['mEmployee'];						
					?>
					<?php if(count($mEmployee)>0) foreach($mEmployee as $mUser): ?>
						<?php 
							if(isset($MAINTAIN_EMPLOYEE_DATA['CountIsMaintain'][$agent_id][$mUser->id])):
						?>
						<tr class="">
							<!--<td><?php echo $mAgent->first_name; ?></td>-->
							<td><?php echo $mUser->first_name; ?></td>
							<td class="item_c">
								<?php echo isset($MAINTAIN_EMPLOYEE_DATA['CountIsMaintain'][$agent_id][$mUser->id])?$MAINTAIN_EMPLOYEE_DATA['CountIsMaintain'][$agent_id][$mUser->id]:''; ?>
							</td>							
							<td class="item_c">
								<?php echo isset($MAINTAIN_EMPLOYEE_DATA['CountIsMaintainCallGood'][$agent_id][$mUser->id])?$MAINTAIN_EMPLOYEE_DATA['CountIsMaintainCallGood'][$agent_id][$mUser->id]:''; ?>
							</td>
							<td class="item_c">
								<?php echo isset($MAINTAIN_EMPLOYEE_DATA['CountIsMaintainCallBad'][$agent_id][$mUser->id])?$MAINTAIN_EMPLOYEE_DATA['CountIsMaintainCallBad'][$agent_id][$mUser->id]:''; ?>
							</td>
						</tr>			
						<?php endif;?>
					<?php endforeach; // foreach($mEmployee ?>					
				<?php endforeach; // foreach($MAINTAIN_EMPLOYEE_MODEL ?>
				</tbody>
				
			</table>		
		</div> <!-- end grid-view -->
	</div> <!-- end tab2 -->
</div>

<?php endif; ?>

<style>
	.items tr td.item_c {text-align:center;}
	.items tr td.item_b { font-weight:bold;}
</style>

