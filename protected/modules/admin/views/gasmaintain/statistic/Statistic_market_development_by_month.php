<h1><?php echo Yii::t('translation', 'Thống Kê Phát Triển Thị Trường'); ?></h1>
<?php //echo CHtml::link(Yii::t('translation','Advanced Search'),'#',array('class'=>'search-button')); ?>
<div class="search-form" style="">
    <div class="wide form">

    <?php $form=$this->beginWidget('CActiveForm', array(
            'action'=>Yii::app()->createUrl($this->route),
            'method'=>'post',
    )); ?>
            <?php if(Yii::app()->user->hasFlash('successUpdate')): ?>
                <div class="success_div"><?php echo Yii::app()->user->getFlash('successUpdate');?></div>         
            <?php endif; ?>    

            <div class="row display_none">
                    <?php echo Yii::t('translation', $form->labelEx($model,'province_id')); ?>
                    <?php echo $form->dropDownList($model,'province_id', GasProvince::getArrAll(),array('style'=>'width:376px','empty'=>'Select')); ?>
            </div> 
            <div class="row">
                <?php echo Yii::t('translation', $form->labelEx($model,'agent_id')); ?>
                <?php echo $form->dropDownList($model,'agent_id', Users::getSelectByRole(),array('style'=>'width:376px;','empty'=>'Select')); ?>
                <?php echo $form->error($model,'agent_id'); ?>
            </div>                  
                
            <div class="row group_subscriber">
            <?php echo Yii::t('translation', $form->label($model,'monitoring_id')); ?>
                <div class="fix-label">
                        <?php
                           $this->widget('ext.multiselect.JMultiSelect',array(
                                         'model'=>$model,
                                         'attribute'=>'monitoring_id',
                                         'data'=>Users::getArrUserByRole(ROLE_MONITORING_MARKET_DEVELOPMENT),
                                         // additional javascript options for the MultiSelect plugin
                                         'options'=>array('header' => false,),
                                         // additional style
                                         'htmlOptions'=>array('style' => 'width: 370px;'),
                           ));    
                   ?>
                </div>            
            </div>				
				
            <div class="row group_subscriber">
                    <?php echo Yii::t('translation', $form->label($model,'statistic_month')); ?>
                    <?php // echo $form->dropDownList($model,'costs_month', ActiveRecord::getMonthVn(),array('style'=>'width:350px;','empty'=>'Select')); ?>
                    <div class="fix-label">
                        <?php
                           $this->widget('ext.multiselect.JMultiSelect',array(
                                 'model'=>$model,
                                 'attribute'=>'statistic_month',
                                 'data'=>ActiveRecord::getMonthVnWithZeroFirst(),
                                 // additional javascript options for the MultiSelect plugin
                                 'options'=>array('header' => false,),
                                 // additional style
                                 'htmlOptions'=>array('style' => 'width: 370px;'),
                           ));    
                       ?>
                    </div>               
            </div>

            <div class="row">
                    <?php echo Yii::t('translation', $form->labelEx($model,'statistic_year')); ?>
                    <?php echo $form->dropDownList($model,'statistic_year', ActiveRecord::getRangeYear(), array('style'=>'width:376px;')); ?>		
                    <?php echo $form->error($model,'statistic_year'); ?>
            </div>	
			
            <div class="row">
                    <?php echo Yii::t('translation', $form->labelEx($model,'sort_by')); ?>
                    <?php echo $form->dropDownList($model,'sort_by', CmsFormatter::$SortByFormat,array('style'=>'width:376px')); ?>
            </div> 				

            <div class="row buttons" style="padding-left: 159px;">
                            <?php $this->widget('bootstrap.widgets.TbButton', array(
                'buttonType'=>'submit',
                'label'=>Yii::t('translation','Xem Thống Kê'),
                'type'=>'null', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
                'size'=>'small', // null, 'large', 'small' or 'mini'
                //'htmlOptions' => array('style' => 'margin-bottom: 10px; float: right;'),
            )); ?>	
            <?php //if(ControllerActionsName::isAccessAction('statistic_market_development_by_month_export_excel', $actions)):?>
                    <input class='Statistic_maintain_by_month_export_excel' next='<?php echo Yii::app()->createAbsoluteUrl('admin/gasmaintain/statistic_market_development_by_month_export_excel');?>' type='button' value='Xuất Thống Kê Hiện Tại Ra Excel'>
            <?php //endif;?>

            </div>

    <?php $this->endWidget(); ?>

    </div><!-- search-form -->
</div><!-- search-form -->
 <script>
$(function() {
	$('.Statistic_maintain_by_month_export_excel').click(function(){
		window.location = $(this).attr('next');
	});
	
	$('.grid-view table tbody tr').click(function()
	{
		$('.grid-view table tbody tr').removeClass('selected');
		$(this).addClass('selected');
	});
        
        fnBindEventSubmitClick('<?php echo BLOCK_UI_COLOR;?>');
});
</script>

<?php if(count($resultSta)>0): ?>
<?php Yii::app()->clientScript->registerCoreScript('jquery.ui'); ?>

<!--<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/jquery.floatThead.min.js"></script>    
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/underscore-min.js"></script>    -->

<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/scroll/jquery.freezetablecolumns.1.1.js"></script> 
 <script>
$(function() {
    $( "#tabs" ).tabs();    
//    fnFixScrollHeaderTable();
    fnTabMonthClick();
});


$(window).load(function(){
    var index = 1;
    $('.freezetablecolumns').each(function(){
        if(index==1)
            $(this).closest('div.grid-view').show();
        index++;
    });
    fnAddClassOddEven('items'); 
});

function fnTabMonthClick(){
    $('.freezetablecolumns').each(function(){
        var id_table = $(this).attr('id');
        $('#'+id_table).freezeTableColumns({
                width:       <?php echo Users::WIDTH_freezetable;?>,   // required - bt 1100
                height:      400,   // required
                numFrozen:   2,     // optional
                frozenWidth: 242,   // optional
                clearWidths: true  // optional
              });   
    });
    
//    $('.tab_month').click(function(){
//        var month_current = $(this).attr('month_current');
//        var tableItem = $('.table_month_'+month_current);
//        tableItem.floatThead();
////        calcWidthTable(tableItem);
//        
//    });
}

function calcWidthTable(tableItem){
    return;
    // calculate width để xuất hiện scroll cho thanh bar ngang
        var trWidth = 0;
        var tHeadTrFirst = tableItem.find('thead tr').eq(0);
        tHeadTrFirst.find('th').each(function(){
//            alert($(this).width());
//            alert($(this).outerWidth( true ) );
            trWidth += $(this).outerWidth( true );
        });
        tableItem.css({'width':trWidth+'px'});          
        // calculate width để xuất hiện scroll cho thanh bar ngang
}


function fnFixScrollHeaderTable(){
    return;
    var index=1;
     $('.grid-view').each(function(){
        if(index==1){
            var tableItem = $(this).find('.items').eq(0);
            $(this).find('.items').floatThead();
//            calcWidthTable(tableItem);
        }
        index++;
    });
    return;
    
//    $('.grid-view').each(function(){
//        var tableItem = $(this).find('.items').eq(0);
//        var trWidth = 0;
//        var tBodyTrFirst = tableItem.find('tbody tr').eq(2);
//        tBodyTrFirst.find('td').each(function(){
//            alert($(this).width());
//            trWidth += $(this).width();
//        });
//        tableItem.css({'width':trWidth+'px'});        
//    });
//    return;
    
    
    
//    $(window).scroll(function(){
//        var browsers = detecting_browsers_by_ducktyping();
//        $('.grid-view').each(function(){
//            var tableItem = $(this).find('.items').eq(0);            
//            var tHead = $(this).find('.items').eq(0).find('thead');
//            var tBody = $(this).find('.items').eq(0).find('tbody');
//            var h = tableItem.offset().top;
//            var top = $(window).scrollTop();
//            if( top > h ) // height of float header
//            {
//                tHead.addClass("stick");
//                var tHeadTrFirst = tHead.find('tr').eq(0);
//                var tBodyTrFirst = tBody.find('tr').eq(1);
//                var key=0;
//                tHead.find('th').each(function(){
//                    var tdWidth = tBodyTrFirst.find('td').eq(key++).width();
//                    if(browsers=='chrome')
//                        tdWidth = parseFloat(tdWidth).toFixed(2);
//                    if(browsers=='firefox')
//                        tdWidth = parseFloat(tdWidth).toFixed(1);
//                    $(this).width(tdWidth);
//                });             
//            }
//            else
//             tHead.removeClass("stick");
//           });    
//        });
}

function roundToTwo(num) {    
    return +(Math.round(num + "e+2")  + "e-2");
//roundToTwo(1.005)
//1.01
//roundToTwo(10)
//10
//roundToTwo(1.7777777)
//1.78
}

</script>

<?php

$gridCss = Yii::app()->getAssetManager()->publish(Yii::getPathOfAlias('zii.widgets.assets')).'/gridview/styles.css';
Yii::app()->getClientScript()->registerCssFile($gridCss);
$MONTHS = $resultSta['month'];
$MONITOR_EMPLOYEE = Users::getArrIdObjectMonitorAndMarketEmployee($model->monitoring_id, 'first_name');
$YEARS = $model->statistic_year;
$_SESSION['data-excel']['MONITOR_EMPLOYEE'] = $MONITOR_EMPLOYEE;
$_SESSION['data-excel']['YEARS'] = $YEARS;
$key=1;
?>

<div id="tabs">
<ul>
    <?php foreach($MONTHS as $month=>$tempM):?>
        <li><a class="tab_month" month_current="<?php echo $month;?>" href="#tabs-<?php echo $month;?>">Tháng <?php echo $month;?></a></li>
    <?php endforeach; // end foreach($MONTHS as $month) ?>
</ul>
    <?php foreach($MONTHS as $month=>$tempM):?>
    <?php $days_of_month = $resultSta['days'][$month];?>
    <div id="tabs-<?php echo $month;?>">
            <div class="clr"></div>
            <div class="grid-view <?php echo ($key++)==1?"display_none":"";?> ">
                <table id="freezetablecolumns<?php echo $month;?>" class="freezetablecolumns items">
                    <thead>
                        <tr>                               
                            <th class="w-150">PTTT</th>
                            <th class="w-70">Tổng Cộng</th>
                            <?php foreach($days_of_month as $day): ?>
                            <th class=" w-30" style="">
                                <?php echo $day;?></th>
                            <?php endforeach;?>
                        </tr>
                    </thead>

                    <tbody>	
                    <?php if(isset($resultSta['total_monitor'][$YEARS][$month])): ?>
                    <?php foreach($resultSta['total_monitor'][$YEARS][$month] as $monitor_id=>$total_monitor):?>
                            <?php $arrIdAndTotalOfEmployee = $resultSta['total_month'][$YEARS][$month][$monitor_id];?>
                            <?php if(   isset($MONITOR_EMPLOYEE[$monitor_id]) &&
                                        (isset($resultSta['total_monitor'][$YEARS][$month][$monitor_id]) ||
                                        isset($resultSta['total_monitor_sell'][$YEARS][$month][$monitor_id]))
                                ):?>
                                <tr>
                                    <td class="w-150a item_b"><?php echo $MONITOR_EMPLOYEE[$monitor_id]->first_name; ?></td>
                                    <?php
                                        $text_total_monitor='';
                                        $market_dev = isset($resultSta['total_monitor'][$YEARS][$month][$monitor_id])?$resultSta['total_monitor'][$YEARS][$month][$monitor_id]:0;
                                        $market_dev_sell = isset($resultSta['total_monitor_sell'][$YEARS][$month][$monitor_id])?$resultSta['total_monitor_sell'][$YEARS][$month][$monitor_id]:0;

                                        if($market_dev!=0 || $market_dev_sell!=0){
                                        if($market_dev_sell!=0)
                                                $text_total_monitor= $market_dev.'/'.$market_dev_sell;
                                        elseif($market_dev!=0)
                                                $text_total_monitor= $market_dev;
                                        elseif($market_dev!=0 && $market_dev_sell!=0)
                                                $text_total_monitor= $market_dev.'/'.$market_dev_sell.' = '.(($market_dev_sell/$market_dev)*100).' %';
                                        elseif($market_dev==0 && $market_dev_sell!=0)
                                                $text_total_monitor= $market_dev.'/'.$market_dev_sell.': BH '.($market_dev_sell).' %';
                                        }
                                    ?>									
                                    <td class="item_c w-70a item_b">
                                        <?php echo $text_total_monitor;?>
                                    </td> 
                                    <?php foreach($days_of_month as $day): ?>
                                    <td class="w-50a">&nbsp;</td>
                                    <?php endforeach; // end <?php foreach($days_of_month as $day) ?>
                                </tr>
                            <?php endif;?>
                            <?php foreach($arrIdAndTotalOfEmployee as $employee_id=>$total):?>		
                                    <?php
                                            $text_total_month='';
                                            //$market_dev = isset($resultSta['total_month'][$YEARS][$month][$employee->id])?$resultSta['total_month'][$YEARS][$month][$employee->id]:0;
                                            $market_dev = $total;
                                            $market_dev_sell = isset($resultSta['total_month_sell'][$YEARS][$month][$monitor_id][$employee_id])?$resultSta['total_month_sell'][$YEARS][$month][$monitor_id][$employee_id]:0;
                                            if($market_dev!=0 || $market_dev_sell!=0){
                                                if($market_dev_sell!=0)
                                                    $text_total_month = $market_dev.'/'.$market_dev_sell;
                                                elseif($market_dev!=0)
                                                        $text_total_month = $market_dev;
                                                elseif($market_dev!=0 && $market_dev_sell!=0)
                                                        $text_total_month = $market_dev.'/'.$market_dev_sell.' = '.(($market_dev_sell/$market_dev)*100).' %';
                                                elseif($market_dev==0 && $market_dev_sell!=0)
                                                        $text_total_month = $market_dev.'/'.$market_dev_sell.': BH '.($market_dev_sell).' %';
                                            }
                                    ?>
                                    <?php if(isset($MONITOR_EMPLOYEE[$employee_id]) && ($market_dev!=0 || $market_dev_sell!=0)):?>
                                    <tr class="">
                                        <td class="w-150a" ><?php echo $MONITOR_EMPLOYEE[$employee_id]->first_name; ?></td>
                                        <td class="item_c w-70a"><?php echo $text_total_month;?></td>     														
                                        <?php foreach($days_of_month as $day): ?>																	
                                        <td class="w-50a item_c ">
                                                <?php 
                                                        $market_dev = isset($resultSta['market_dev'][$YEARS][$month][$day][$monitor_id][$employee_id])?$resultSta['market_dev'][$YEARS][$month][$day][$monitor_id][$employee_id]:0;
                                                        $market_dev_sell = isset($resultSta['market_dev_sell'][$YEARS][$month][$day][$monitor_id][$employee_id])?$resultSta['market_dev_sell'][$YEARS][$month][$day][$monitor_id][$employee_id]:0;
                                                        if($market_dev!=0 || $market_dev_sell!=0){
                                                                if($market_dev_sell!=0)
                                                                        echo $market_dev.'/'.$market_dev_sell;
                                                                elseif($market_dev!=0)
                                                                        echo $market_dev;
                                                        }
                                                ?>		
                                        </td>
                                        <?php endforeach; // end <?php foreach($days_of_month as $day) ?>
                                    </tr>	
                                <?php endif;?>								
                        <?php endforeach; // end foreach($AGENT_MODEL as $mAgent) ?>
                    <?php endforeach; // end foreach($AGENT_MODEL as $mAgent) ?>
                    <?php endif; // end if(isset($resultSta['total_monitor'][$YEARS][$month]) ?>	
                    </tbody>

                </table>		
            </div> <!-- end grid-view -->
    </div> <!-- end tab1 -->
    <?php endforeach; // end foreach($MONTHS as $month) ?>
</div>

<?php endif; ?>

<style>
    /*.item_first { width: 150px;}*/    
    /*.grid-view-scroll { width: 100%; overflow-x: auto;}*/
</style>

