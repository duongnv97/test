<h1><?php echo Yii::t('translation', 'Thống Kê Tiến Độ Bảo Trì'); ?></h1>

<?php //echo CHtml::link(Yii::t('translation','Advanced Search'),'#',array('class'=>'search-button')); ?>
<div class="search-form" style="">
    <div class="wide form">

    <?php $form=$this->beginWidget('CActiveForm', array(
            'action'=>Yii::app()->createUrl($this->route),
            'method'=>'post',
    )); ?>
            <?php if(Yii::app()->user->hasFlash('successUpdate')): ?>
                <div class="success_div"><?php echo Yii::app()->user->getFlash('successUpdate');?></div>         
            <?php endif; ?>    

            <div class="row">
                    <?php echo Yii::t('translation', $form->labelEx($model,'province_id')); ?>
                    <?php echo $form->dropDownList($model,'province_id', GasProvince::getArrAll(),array('style'=>'width:376px','empty'=>'Select','class'=>'')); ?>
            </div> 	
                
            <div class="row group_subscriber">
                    <?php echo Yii::t('translation', $form->label($model,'statistic_month')); ?>
                    <?php // echo $form->dropDownList($model,'costs_month', ActiveRecord::getMonthVn(),array('style'=>'width:350px;','empty'=>'Select')); ?>
                    <div class="fix-label">
                        <?php
                           $this->widget('ext.multiselect.JMultiSelect',array(
                                 'model'=>$model,
                                 'attribute'=>'statistic_month',
                                 'data'=>ActiveRecord::getMonthVnWithZeroFirst(),
                                 // additional javascript options for the MultiSelect plugin
                                 'options'=>array('header' => false,),
                                 // additional style
                                 'htmlOptions'=>array('style' => 'width: 370px;'),
                           ));    
                       ?>
                    </div>               
            </div>

            <div class="row">
                    <?php echo Yii::t('translation', $form->labelEx($model,'statistic_year')); ?>
                    <?php echo $form->dropDownList($model,'statistic_year', ActiveRecord::getRangeYear(), array('style'=>'width:376px;')); ?>		
                    <?php echo $form->error($model,'statistic_year'); ?>
            </div>	
            <div class="row">
                    <?php echo Yii::t('translation', $form->labelEx($model,'sort_by')); ?>
                    <?php echo $form->dropDownList($model,'sort_by', CmsFormatter::$SortByFormat,array('style'=>'width:376px')); ?>
            </div> 	
            <div class="row buttons" style="padding-left: 159px;">
                            <?php $this->widget('bootstrap.widgets.TbButton', array(
                'buttonType'=>'submit',
                'label'=>Yii::t('translation','Xem Thống Kê'),
                'type'=>'null', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
                'size'=>'small', // null, 'large', 'small' or 'mini'
                //'htmlOptions' => array('style' => 'margin-bottom: 10px; float: right;'),
            )); ?>	
            <?php if(ControllerActionsName::isAccessAction('statistic_maintain_by_month_export_excel', $actions)):?>
                    <input class='Statistic_maintain_by_month_export_excel' next='<?php echo Yii::app()->createAbsoluteUrl('admin/gasmaintain/statistic_maintain_by_month_export_excel');?>' type='button' value='Xuất Thống Kê Hiện Tại Ra Excel'>
            <?php endif;?>

            </div>

    <?php $this->endWidget(); ?>

    </div><!-- search-form -->
</div><!-- search-form -->

 <script>
$(function() {
	$('.Statistic_maintain_by_month_export_excel').click(function(){
		window.location = $(this).attr('next');
	});
	
	$('.grid-view table tbody tr').click(function()
	{
		$('.grid-view table tbody tr').removeClass('selected');
		$(this).addClass('selected');
	});
        $('.form').find('button:submit').click(function(){
            $.blockUI({ overlayCSS: { backgroundColor: '<?php echo BLOCK_UI_COLOR;?>' } }); 
        });         

});
</script>

<?php if(count($resultSta)>0): ?>
<?php Yii::app()->clientScript->registerCoreScript('jquery.ui'); ?>
 <script>
$(function() {

    $( "#tabs" ).tabs();
    $(".items > tbody > tr:odd").addClass("odd");
    $(".items > tbody > tr:even").addClass("even");   

});
</script>

<?php
$gridCss = Yii::app()->getAssetManager()->publish(Yii::getPathOfAlias('zii.widgets.assets')).'/gridview/styles.css';
Yii::app()->getClientScript()->registerCssFile($gridCss);
$MONTHS = $resultSta['month'];
$AGENT_MODEL = Users::getArrObjectUserByRoleHaveOrder (ROLE_AGENT, 'code_account');
$YEARS = $model->statistic_year;
$_SESSION['data-excel']['AGENT_MODEL'] = $AGENT_MODEL;
$_SESSION['data-excel']['YEARS'] = $YEARS;

?>

<div id="tabs">
<ul>
    <?php foreach($MONTHS as $month=>$tempM):?>
	<li><a href="#tabs-<?php echo $month;?>">Tháng <?php echo $month;?></a></li>
    <?php endforeach; // end foreach($MONTHS as $month) ?>
</ul>
    <?php foreach($MONTHS as $month=>$tempM):?>
    <?php $days_of_month = $resultSta['days'][$month];?>
    <div id="tabs-<?php echo $month;?>">
            <div class="grid-view">
                    <table class="items">
                        <thead>
                            <tr>
                                <th class="item_first">Đại Lý / Ngày</th>
                                <th class="item_first">Tổng Cộng</th>
                                <?php foreach($days_of_month as $day): ?>
                                    <th class="item_day"><?php echo $day;?></th>
                                <?php endforeach;?>
                            </tr>
                        </thead>

                        <tbody>	
                        <?php //foreach($AGENT_MODEL as $mAgent):?>			
                        <?php foreach($resultSta['total_month'][$YEARS][$month] as $agent_id=>$total):?>			
                                <tr class="">
                                    <td><?php echo $AGENT_MODEL[$agent_id]->first_name; ?></td>
                                    <td class="item_c">
                                        <?php //echo isset($resultSta['total_month'][$YEARS][$month][$agent_id])?$resultSta['total_month'][$YEARS][$month][$agent_id]:''; ?>
                                        <?php echo $total; ?>
                                    </td>                                    
                                    <?php foreach($days_of_month as $day): ?>
                                    <td class="item_c item_day">
                                        <?php echo isset($resultSta[$YEARS][$month][$day][$agent_id])?$resultSta[$YEARS][$month][$day][$agent_id]:''; ?>
                                    </td>
                                    <?php endforeach; // end <?php foreach($days_of_month as $day) ?>
                                </tr>							
                        <?php endforeach; // end foreach($AGENT_MODEL as $mAgent) ?>
                        </tbody>

                    </table>		
            </div> <!-- end grid-view -->
    </div> <!-- end tab1 -->
    <?php endforeach; // end foreach($MONTHS as $month) ?>
</div>

<?php endif; ?>

<style>
    .items tr td.item_c {text-align:center;}
    .items tr td.item_b { font-weight:bold;}
    .item_first { width: 150px;}
    .grid-view { width: 100%; overflow-x: auto;}
</style>

