<?php
/* $this->breadcrumbs=array(
	'Bảo Trì'=>array('index'),
	$model->customer?$model->customer->first_name:'',
);

$menus = array(
	array('label'=>'Bảo Trì', 'url'=>array('index')),
	array('label'=>'Thêm Bảo Trì', 'url'=>array('create')),
	array('label'=>'Cập Nhật Bảo Trì', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Xóa Bảo Trì', 'url'=>array('delete'), 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
);



if(!(Yii::app()->user->role_id == ROLE_ADMIN || ( Yii::app()->user->role_id == ROLE_SUB_USER_AGENT && (int)$model->update_num < (int)Yii::app()->params["limit_update_maintain"]) )){
    $pos = array_search('Update', $actions);
    unset($actions[$pos]);
}
    
$this->menu= ControllerActionsName::createMenusRoles($menus, $actions); */

?>
<?php $cmsFormat = new CmsFormatter(); ?>
<h1>Xem Bảo Trì Khách Hàng: <?php echo $model->customer?$cmsFormat->formatNameUser($model->customer):''; ?></h1>
<?php $cmsFormat = new CmsFormatter(); ?>
<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
            'code_no',
            array(
                'name' => 'agent_id',
                'value'=>$model->agent?$model->agent->first_name:'',
            ), 	
            array(
                'label'=>'Mã Khách Hàng',
                'name'=>'customer_name',
                'value'=>$model->customer?$model->customer->code_bussiness:'',
            ),               
          /*   array(
                'label'=>'Mã Hệ Thống',
                'name'=>'customer_name',
                'value'=>$model->customer?$model->customer->code_account:'',
            ),    */            
            array(
                'name'=>'customer_name',
                'type'=>'NameUser',
                'value'=>$model->customer?$model->customer:0,
            ),               
            array(
                'name'=>'customer_address',
                'type'=>'AddressTempUser',
                'value'=>$model->customer?$model->customer:'',
            ),               
            array(
                'name'=>'customer_phone',
                'value'=>$model->customer?$model->customer->phone:'',
            ),               
          
		'maintain_date:date',
        array(
            'name' => 'maintain_employee_id',
			'value'=>$model->maintain_employee?$model->maintain_employee->first_name:'',
        ), 			
            array(
                'name'=>'materials_id',
                'value'=>$model->materials?$model->materials->name:'',
            ),               
		'seri_no',
            array(
                'name'=>'is_need_maintain_back',
                'value'=>  CmsFormatter::$yesNoFormat[$model->is_need_maintain_back],
            ),                 
		'note',
            array(
                'name'=>'status',
                'value'=>  CmsFormatter::$STATUS_MAINTAIN[$model->status],
            ),   
            array(
                'name'=>'using_gas_huongminh',
                'value'=>  CmsFormatter::$yesNoFormat[$model->using_gas_huongminh],
            ),   
		'note_update_status:html',			
		'created_date:Datetime',
	),
)); ?>
