<?php
$this->breadcrumbs=array(
	Yii::t('translation','Bảo Trì')=>array('index'),
	Yii::t('translation','Thêm Bảo Trì'),
);

$menus = array(		
        array('label'=> Yii::t('translation', 'Bảo Trì') , 'url'=>array('index')),
);
$this->menu= ControllerActionsName::createMenusRoles($menus, $actions);
?>

<h1><?php echo Yii::t('translation', 'Thêm Bảo Trì'); ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>