<?php
$this->breadcrumbs=array(
	'Bán Hàng Có Cuộc Gọi Xấu',
);

$menus=array(
	array('label'=> Yii::t('translation','Xuất Excel Danh Sách Hiện Tại'), 
            'url'=>array('Export_supervision_list_maintain_sell_call_bad'), 
            'htmlOptions'=>array('class'=>'export_excel','label'=>'Xuất Excel')),    
);
$this->menu= ControllerActionsName::createMenusRoles($menus, $actions);
Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('gas-maintain-grid', {
                url : $(this).attr('action'),
		data: $(this).serialize()
	});
	return false;
});
");

Yii::app()->clientScript->registerScript('ajaxupdate', "
$('#gas-maintain-grid a.ajaxupdate').live('click', function() {
    $.fn.yiiGridView.update('gas-maintain-grid', {
        type: 'POST',
        url: $(this).attr('href'),
        success: function() {
            $.fn.yiiGridView.update('gas-maintain-grid');
        }
    });
    return false;
});
");
?>

<h1>Bán Hàng Có Cuộc Gọi Xấu</h1>

<?php echo CHtml::link(Yii::t('translation','Tìm Kiếm Nâng Cao'),'#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search_sell',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->
 

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'gas-maintain-grid',
	'dataProvider'=>$model->search(),
        'afterAjaxUpdate'=>'function(id, data){ fnUpdateColorbox();}',
//	'enableSorting' => false,
        'template'=>'{pager}{summary}{items}{pager}{summary}', 
	'columns'=>array(
        array(
            'header' => 'S/N',
            'type' => 'raw',
            'value' => '$this->grid->dataProvider->pagination->currentPage * $this->grid->dataProvider->pagination->pageSize + ($row+1)',
            'headerHtmlOptions' => array('width' => '30px','style' => 'text-align:center;'),
            'htmlOptions' => array('style' => 'text-align:center;')
        ),
        array(
            'name' => 'agent_id',
            'value' => '$data->agent?$data->agent->first_name:""',
            'visible'=>Yii::app()->user->role_id!=ROLE_SUB_USER_AGENT,
            'htmlOptions' => array('style' => 'text-align:center;width:80px;')
        ),               
        array(
            'name' => 'date_sell',
            'type'=>'date',
            'htmlOptions' => array('style' => 'text-align:center;width:50px;')
        ),  
        array(
            'header' => 'Mã KH',
            'value' => '$data->customer?$data->customer->code_bussiness:""',
//            'htmlOptions' => array('style' => 'text-align:left;width:350px;')
        ),               
        array(
                    'name' => 'customer_id',
				'type'=>'NameUser',
                'value'=>'$data->customer?$data->customer:0',			

        //            'htmlOptions' => array('style' => 'text-align:left;width:350px;')
                ),  		
                array(
                    'header' => 'Địa Chỉ',
                    'type'=>'AddressTempUser',
                    'value' => '$data->customer?$data->customer:""',                   
                    'htmlOptions' => array('style' => 'text-align:center;width:80px;'),
					'visible'=>Yii::app()->user->role_id!=ROLE_SUB_USER_AGENT,
                ),   				
                array(
                    'header' => 'Điện Thoại',
                    'value' => '$data->customer?$data->customer->phone:""',
                    'htmlOptions' => array('style' => 'text-align:center;width:60px;'),
					'visible'=>Yii::app()->user->role_id!=ROLE_SUB_USER_AGENT,
                ),               
                array(
                    'header' => 'Bảo Trì',
                    'type' => 'MaintainHistory',  
                    'value' => '$data',  
                    'htmlOptions' => array('style' => 'width:100px;')
                    ),  
        array(
            'name' => 'status',
            'type'=>'StatusMaintain',
//            'value'=>'CmsFormatter::$STATUS_MAINTAIN[$data->status]',
            'htmlOptions' => array('style' => 'text-align:center;width:70px;font-weight:bold;')
        ),      
        array(
            'name' => 'note_update_status',
            'type'=>'html',
            'htmlOptions' => array('style' => 'width:80px;')
        ),             
            
        array(
            'name' => 'status_call_bad',
            'type'=>'StatusSupervision',
//            'value'=>'CmsFormatter::$STATUS_MAINTAIN_BACK[$data->status_maintain_back]',
            'htmlOptions' => array('style' => 'text-align:center;width:70px;font-weight:bold;')
        ),       
        array(
            'name' => 'note_call_bad',
			'type'=>'html',
            'htmlOptions' => array('style' => 'width:130px;')
        ),             
		array(
                        'htmlOptions' => array('style' => 'text-align:center;width:70px;'),
			'header' => 'Action',
			'class'=>'CButtonColumn',
                        'template'=> ControllerActionsName::createIndexButtonRoles($actions, array('Supervision_list_maintain_sell_call_bad')),
                        'buttons'=>array(
                            'Supervision_list_maintain_sell_call_bad'=>array(
                                'label'=>'Cập nhật trạng thái bảo trì',
                                'imageUrl'=>Yii::app()->theme->baseUrl . '/admin/images/edit.png',
                                'options'=>array('class'=>'list_for_monitoring_maintain'),
                                'url'=>'Yii::app()->createAbsoluteUrl("admin/gasmaintain/update_supervision_list_maintain_sell_call_bad",
                                    array("maintain_sell_id"=>$data->id,
                                            "order_id"=>$data->id) )',
//                                 'visible'=>'( Yii::app()->user->role_id == ROLE_ADMIN || (int)$data->update_num < (int)Yii::app()->params["limit_update_maintain"] )',                                
                                
                            ),
			),                          
		),
	),
)); ?>


<script>
$(document).ready(function() {
    fnUpdateColorbox();
});

function fnUpdateColorbox(){    
    $(".list_for_monitoring_maintain").colorbox({iframe:true,innerHeight:'450', innerWidth: '850',close: "<span title='close'>close</span>"});
}
</script>