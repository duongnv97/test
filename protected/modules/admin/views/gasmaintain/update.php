<?php
$this->breadcrumbs=array(
	Yii::t('translation','Bảo Trì')=>array('index'),
	($model->customer?$model->customer->first_name:'')=>array('view','id'=>$model->id),
	Yii::t('translation','Cập Nhật'),
);

$menus = array(	
        array('label'=> Yii::t('translation', 'Bảo Trì'), 'url'=>array('index')),
	//array('label'=> Yii::t('translation', 'Xem Bảo Trì'), 'url'=>array('view', 'id'=>$model->id)),
	array('label'=> Yii::t('translation', 'Thêm Bảo Trì'), 'url'=>array('create')),	
);
$this->menu= ControllerActionsName::createMenusRoles($menus, $actions);

?>
<?php $cmsFormat = new CmsFormatter(); ?>
<h1><?php echo Yii::t('translation', 'Cập Nhật Bảo Trì Khách Hàng: '.($model->customer?$cmsFormat->formatNameUser($model->customer):'')); ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>