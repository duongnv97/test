<?php
$this->breadcrumbs=array(
	'Bảo Trì',
);

$menus=array(
	array('label'=> Yii::t('translation','Thêm Bảo Trì'), 'url'=>array('create')),
	array('label'=> Yii::t('translation','Xuất Excel Danh Sách Hiện Tại'), 
            'url'=>array('Export_list_maintain'), 
            'htmlOptions'=>array('class'=>'export_excel','label'=>'Xuất Excel')),
);
$this->menu= ControllerActionsName::createMenusRoles($menus, $actions);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('gas-maintain-grid', {
                url : $(this).attr('action'),
		data: $(this).serialize()
	});
	return false;
});
");

Yii::app()->clientScript->registerScript('ajaxupdate', "
$('#gas-maintain-grid a.ajaxupdate').live('click', function() {
    $.fn.yiiGridView.update('gas-maintain-grid', {
        type: 'POST',
        url: $(this).attr('href'),
        success: function() {
            $.fn.yiiGridView.update('gas-maintain-grid');
        }
    });
    return false;
});
");
?>

<h1><?php echo Yii::t('translation', 'Bảo Trì'); ?></h1>

<?php echo CHtml::link(Yii::t('translation','Tìm Kiếm Nâng Cao'),'#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->
 

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'gas-maintain-grid',
	'dataProvider'=>$model->search(),
        'afterAjaxUpdate'=>'function(id, data){ fnUpdateColorbox();}',
//	'enableSorting' => false,
        'template'=>'{pager}{summary}{items}{pager}{summary}', 
        'pager' => array(
            'maxButtonCount'=>  CmsFormatter::$PAGE_MAX_BUTTON,
        ),    
    
	'columns'=>array(
        array(
            'header' => 'S/N',
            'type' => 'raw',
            'value' => '$this->grid->dataProvider->pagination->currentPage * $this->grid->dataProvider->pagination->pageSize + ($row+1)',
            'headerHtmlOptions' => array('width' => '30px','style' => 'text-align:center;'),
            'htmlOptions' => array('style' => 'text-align:center;')
        ),
        array(
            'name' => 'code_no',
            'htmlOptions' => array('style' => 'text-align:center;width:50px;')
        ),               
        array(
            'name' => 'agent_id',
            'value' => '$data->agent?$data->agent->first_name:""',
            'visible'=>Yii::app()->user->role_id!=ROLE_SUB_USER_AGENT,
            'htmlOptions' => array('style' => 'text-align:center;width:80px;')
        ),               
        array(
            'name' => 'maintain_date',
            'type'=>'date',
            'htmlOptions' => array('style' => 'text-align:center;width:50px;')
        ),             
        array(
            'header' => 'Mã KH',
            'value' => '$data->customer?$data->customer->code_bussiness:""',
//            'htmlOptions' => array('style' => 'text-align:left;width:350px;')
        ),     
            array(                
                'name' => 'customer_id',
                'type'=>'NameUser',
                'value'=>'$data->customer?$data->customer:0',
//                'htmlOptions' => array('style' => 'width:50px;')
            ),     
            
        array(
            'name' => 'customer_address',
            'type'=>'AddressTempUser',
//            'value' => '$data->customer?$data->customer->address:""',
            'value' => '$data->customer?$data->customer:""',
            'visible'=>Yii::app()->user->role_id!=ROLE_SUB_USER_AGENT,
            'htmlOptions' => array('style' => 'text-align:left;width:100px;')
        ),             
        array(
            'name' => 'customer_phone',
            'value' => '$data->customer?$data->customer->phone:""',
			'visible'=>Yii::app()->user->role_id!=ROLE_SUB_USER_AGENT,
            'htmlOptions' => array('style' => 'text-align:center;width:60px;')
        ),           
		
		array(
            'name' => 'maintain_employee_id',
            'value' => '$data->maintain_employee?$data->maintain_employee->first_name:""',
            'htmlOptions' => array('style' => 'text-align:center;width:50px;')
        ),             
	 
        array(
            'name' => 'materials_id',
            'value' => '$data->materials?$data->materials->name:""',
//            'htmlOptions' => array('style' => 'text-align:left;width:350px;')
        ),             
		 Yii::t('translation','seri_no'),
        array(
            'name' => 'note',
            'type'=>'html',
            'htmlOptions' => array('style' => 'width:80px;')
        ),             
        array(
            'name' => 'note_update_status',
            'type'=>'html',
            'htmlOptions' => array('style' => 'width:80px;')
        ),             
        array(
            'name' => 'status',
            'type'=>'StatusMaintain',
//            'value'=>'CmsFormatter::$STATUS_MAINTAIN[$data->status]',
            'htmlOptions' => array('style' => 'text-align:center;width:70px;font-weight:bold;')
        ),              
        array(
            'name' => 'status',
            'type'=>'HadSelling',
            'value'=>'$data',
            'htmlOptions' => array('style' => 'display:none;'),
            'headerHtmlOptions' => array('style' => 'display: none;'),
        ),       
            array(
            'name' => 'created_date',
            'type' => 'Datetime',
            'htmlOptions' => array('style' => 'width:50px;')
        ),
            
        array(
                'htmlOptions' => array('style' => 'text-align:center;width:70px;'),
                'header' => 'Action',
                'class'=>'CButtonColumn',
                'template'=> ControllerActionsName::createIndexButtonRoles($actions, array('update_status_maintain','view','update','delete')),
                'buttons'=>array(
                    'update_status_maintain'=>array(
                        'label'=>'Cập nhật trạng thái bảo trì',
                        'imageUrl'=>Yii::app()->theme->baseUrl . '/admin/images/edit.png',
                        'options'=>array('class'=>'update_status_maintain'),
                        'url'=>'Yii::app()->createAbsoluteUrl("admin/gasmaintain/update_status_maintain",
                            array("maintain_id"=>$data->id,
                                    "order_id"=>$data->id) )',
//                                 'visible'=>'( Yii::app()->user->role_id == ROLE_ADMIN || (int)$data->update_num < (int)Yii::app()->params["limit_update_maintain"] )',
//                                 'visible'=>'( Yii::app()->user->role_id == ROLE_ADMIN || ( (int)$data->update_num < (int)Yii::app()->params["limit_update_maintain"] &&  Yii::app()->user->role_id == ROLE_CHECK_MAINTAIN ) )', 
                         'visible'=>'( Yii::app()->user->role_id == ROLE_ADMIN ||  (int)$data->update_num < (int)Yii::app()->params["limit_update_maintain"] ||  $data->status==STATUS_NOT_YET_CALL )', 

                    ),
                    'update'=>array(
//                                'visible'=>'( Yii::app()->user->role_id == ROLE_ADMIN || ( Yii::app()->user->role_id == ROLE_SUB_USER_AGENT && (int)$data->update_num < (int)Yii::app()->params["limit_update_maintain"]) )',
                        'visible'=>'( Yii::app()->user->role_id == ROLE_ADMIN || (int)$data->update_num < (int)Yii::app()->params["limit_update_maintain"] )',
                    ),
                    'delete'=>array(
                        'visible'=> 'GasCheck::canDeleteData($data)',
                    ),

                ),                          
        ),
	),
)); ?>

<style>
    
    
</style>

<script>
$(document).ready(function() {
    fnUpdateColorbox();
});

function fnUpdateColorbox(){    
    $(".update_status_maintain").colorbox({iframe:true,innerHeight:'450', innerWidth: '850',close: "<span title='close'>close</span>"});
    $(".view").colorbox({iframe:true,innerHeight:'550', innerWidth: '900',close: "<span title='close'>close</span>"});
    $('.HadSelling').each(function(){
        var tr_parent = $(this).parent('td').parent('tr');    
        var html_a_view = tr_parent.find('.HadSelling').html();
        tr_parent.css({'background':'#5FB404'});
        tr_parent.find('td:last').append(html_a_view);
    });
    
    $(".view_selling").colorbox({iframe:true,innerHeight:'700', innerWidth: '900',close: "<span title='close'>close</span>"});    
    fixTargetBlank();
}
</script>