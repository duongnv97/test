<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

        <div class="row">
                    <?php echo Yii::t('translation', $form->label($model,'customer_id')); ?>
                    <?php echo $form->hiddenField($model,'customer_id'); ?>
                    <?php 
                            $this->widget('zii.widgets.jui.CJuiAutoComplete', array(
                                    'attribute'=>'autocomplete_name',
                                    'model'=>$model,
                                    'sourceUrl'=>Yii::app()->createAbsoluteUrl('admin/site/AutocompleteCustomerMaintain'),
                    //                                'name'=>'my_input_name',
                                    'options'=>array(
                                            'minLength'=>MIN_LENGTH_AUTOCOMPLETE,
                                            'multiple'=> true,
                                        'search'=>"js:function( event, ui ) {
                                                $('#GasMaintain_autocomplete_name').addClass('grid-view-loading-gas');
                                                } ",
                                        'response'=>"js:function( event, ui ) {
                                                var json = $.map(ui, function (value, key) { return value; });
                                                if(json.length<1){
                                                    var error = '<div class=\'errorMessage clr autocomplete_name_text\'>Không tìm thấy dữ liệu.</div>';
                                                    $('.autocomplete_name_error').after(error);
                                                }
                                                $('#GasMaintain_autocomplete_name').removeClass('grid-view-loading-gas');
                                                } ",
                                        
                                        'select'=>"js:function(event, ui) {
                                                $('#GasMaintain_customer_id').val(ui.item.id);
                                                var remove_div = '<span class=\'remove_row_item\' onclick=\'fnRemoveName(this)\'></span>';
                                                $('#GasMaintain_autocomplete_name').parent('div').find('.remove_row_item').remove();
                                                $('#GasMaintain_autocomplete_name').attr('readonly',true).after(remove_div);
                                                fnBuildTableInfo(ui.item);
                                                $('.autocomplete_customer_info').show();
                                                $('.autocomplete_name_error').parent('div').find('.autocomplete_name_text').hide();
                                        }",
                                    ),
                                    'htmlOptions'=>array(
                                        'class'=>'autocomplete_name_error',
                                        'size'=>45,
                                        'maxlength'=>45,
                                        'style'=>'float:left;',
                                        'placeholder'=>'Nhập tên, số ĐT hoặc mã KH',
                                    ),
                            )); 
                            ?>        
                            <script>
                                function fnRemoveName(this_){
                                    $(this_).prev().attr("readonly",false); 
                                    $("#GasMaintain_autocomplete_name").val("");
                                    $("#GasMaintain_customer_id").val("");
                                    $('.autocomplete_customer_info').hide();
                                }
                            function fnBuildTableInfo(item){
                                $(".info_name").text(item.name_customer);
                                $(".info_name_agent").text(item.name_agent);
                                $(".info_code_account").text(item.code_account);
                                $(".info_code_bussiness").text(item.code_bussiness);
                                $(".info_address").text(item.address);
                                $(".info_phone").text(item.phone);
                            }

                            </script>        
                            <div class="clr"></div>		
                    <?php echo $form->error($model,'customer_id'); ?>
                    <?php $display='display:inline;';
                        $info_name ='';
                        $info_name_agent ='';
                        $info_address ='';
                        $info_code_account ='';
                        $info_code_bussiness ='';
                        $info_phone ='';
                            if(empty($model->customer_id)) $display='display: none;';
                            else{
                                $info_name = $model->customer->first_name;
                                $info_name_agent = $model->customer->name_agent;
                                $info_code_account = $model->customer->code_account;
                                $info_code_bussiness = $model->customer->code_bussiness;
                                $info_address = $model->customer->address;
                                $info_phone = $model->customer->phone;
                            }
                    ?>                        
                    <div class="autocomplete_customer_info" style="margin-left: 160px; <?php echo $display;?>">
                    <table>
                        <tr>
                            <td class="_l">Mã khách hàng:</td>
                            <td class="_r info_code_bussiness"><?php echo $info_code_bussiness;?></td>
                        </tr>

                        <tr>
                            <td class="_l">Tên khách hàng:</td>
                            <td class="_r info_name"><?php echo $info_name;?></td>
                        </tr>                    
                        <tr>
                            <td class="_l">Địa chỉ:</td>
                            <td class="_r info_address"><?php echo $info_address;?></td>
                        </tr>
                        <tr>
                            <td class="_l">Điện Thoại:</td>
                            <td class="_r info_phone"><?php echo $info_phone;?></td>
                        </tr>


                    </table>
                </div>
                <div class="clr"></div>    		
            </div>    
    
        <div class="row">
		<?php echo Yii::t('translation', $form->label($model,'maintain_date')); ?>
                <?php 
                    $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                        'model'=>$model,        
                        'attribute'=>'maintain_date',
                        'options'=>array(
                            'showAnim'=>'fold',
                            //'dateFormat'=> ActiveRecord::getDateFormatJquery(),
                            'dateFormat'=> 'dd-mm-yy',
//                            'minDate'=> '0',
                            'changeMonth' => true,
                            'changeYear' => true,
                            'showOn' => 'button',
                            'buttonImage'=> Yii::app()->theme->baseUrl.'/admin/images/icon_calendar_r.gif',
                            'buttonImageOnly'=> true,                                
                        ),        
                        'htmlOptions'=>array(
                            'class'=>'w-16',
                            'style'=>'height:20px;float:left',
//                                'readonly'=>'readonly',
                        ),
                    ));
                ?>     		
            <?php echo Yii::t('translation', $form->label($model,'seri_no')); ?>
            <?php echo $form->textField($model,'seri_no',array('style'=>'')); ?>
		
	</div>	

        
			<div class="row">
                    <?php echo Yii::t('translation', $form->label($model,'date_from',array('label'=>'Bảo Trì Từ Ngày'))); ?>		
                    <?php 
                        $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                            'model'=>$model,        
                            'attribute'=>'date_from',
                            'options'=>array(
                                'showAnim'=>'fold',
                                'dateFormat'=> 'dd-mm-yy',
    //                            'minDate'=> '0',
                                'changeMonth' => true,
                                'changeYear' => true,
                                'showOn' => 'button',
                                'buttonImage'=> Yii::app()->theme->baseUrl.'/admin/images/icon_calendar_r.gif',
                                'buttonImageOnly'=> true,                                
                            ),        
                            'htmlOptions'=>array(
                                'class'=>'w-16',
                                'style'=>'height:20px;float:left',
    //                                'readonly'=>'readonly',
                            ),
                        ));
                    ?>            
                    <?php echo $form->error($model,'date_from'); ?>
            </div>

            <div class="row">
                    <?php echo Yii::t('translation', $form->label($model,'date_to',array('label'=>'Bảo Trì Đến Ngày'))); ?>
                    <?php 
                        $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                            'model'=>$model,        
                            'attribute'=>'date_to',
                            'options'=>array(
                                'showAnim'=>'fold',
                                'dateFormat'=> 'dd-mm-yy',
    //                            'minDate'=> '0',
                                'changeMonth' => true,
                                'changeYear' => true,
                                'showOn' => 'button',
                                'buttonImage'=> Yii::app()->theme->baseUrl.'/admin/images/icon_calendar_r.gif',
                                'buttonImageOnly'=> true,                                
                            ),        
                            'htmlOptions'=>array(
                                'class'=>'w-16',
                                'style'=>'height:20px;float:left',
    //                                'readonly'=>'readonly',
                            ),
                        ));
                    ?>  		
                    <?php echo $form->error($model,'date_to'); ?>
            </div>
		
		
		<?php if(Yii::app()->user->role_id!=ROLE_SUB_USER_AGENT):?>
            <div class="row">
                <?php echo Yii::t('translation', $form->labelEx($model,'agent_id')); ?>
                <?php echo $form->dropDownList($model,'agent_id', Users::getSelectByRole(),array('style'=>'width:350px;','empty'=>'Select')); ?>
            </div>   

		<div class="row">
			<?php echo Yii::t('translation', $form->labelEx($model,'maintain_employee_id')); ?>
			<?php echo $form->dropDownList($model,'maintain_employee_id', Users::getSelectByRoleForAgent(Yii::app()->user->id, ONE_AGENT_MAINTAIN, $model->agent_id, array('status'=>1)),array('class'=>'category_ajax', 'style'=>'width:350px;','empty'=>'Select')); ?>		
			<?php echo $form->error($model,'maintain_employee_id'); ?>
		</div>			
        <?php endif;?>
    
	<div class="row">
		<?php echo Yii::t('translation', $form->label($model,'customer_address')); ?>
		<?php echo $form->textField($model,'customer_address',array('style'=>'width:342px;')); ?>
	</div>	
	
	<div class="row">
		<?php echo Yii::t('translation', $form->labelEx($model,'status')); ?>
		<?php echo $form->dropDownList($model,'status', CmsFormatter::$STATUS_MAINTAIN,array('empty'=>'Select')); ?>
	</div>

	<div class="row">
		<?php echo Yii::t('translation', $form->labelEx($model,'had_selling')); ?>
		<?php echo $form->dropDownList($model,'had_selling', CmsFormatter::$yesNoFormat,array('empty'=>'Select')); ?>
	</div>
    
        <div class="row">
                <?php echo $form->labelEx($model,'using_gas_huongminh', array('style'=>'')); ?>
                <?php echo $form->dropDownList($model,'using_gas_huongminh', CmsFormatter::$yesNoFormat,array('style'=>'width:350px;','empty'=>'Select')); ?>
                <?php echo $form->error($model,'using_gas_huongminhs'); ?>
        </div>
    
    
	<div class="row buttons" style="padding-left: 159px;">
		        <?php $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType'=>'submit',
            'label'=>Yii::t('translation','Search'),
            'type'=>'null', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
            'size'=>'small', // null, 'large', 'small' or 'mini'
            //'htmlOptions' => array('style' => 'margin-bottom: 10px; float: right;'),
        )); ?>	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->
<style>
    .ui-datepicker-trigger { float: left;}
</style>