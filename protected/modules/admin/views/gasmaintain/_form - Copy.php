<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'gas-maintain-form',
	'enableAjaxValidation'=>false,
)); ?>

	<?php echo Yii::t('translation', '<p class="note">Fields with <span class="required">*</span> are required.</p>'); ?>
        <?php if(Yii::app()->user->hasFlash('successUpdate')): ?>
            <div class="success_div"><?php echo Yii::app()->user->getFlash('successUpdate');?></div>         
        <?php endif; ?>        	
            
			<?php /*if($model->isNewRecord): ?>
        <div class="radio-list-2">            
            <?php echo $form->radioButtonList($model,'is_new_customer', array(CUSTOMER_NEW=>'Khách Hàng Mới', CUSTOMER_OLD=>'Khách Hàng Cũ'), array('separator'=>"" )); ?>            
        </div>
            <?php endif; */?>        	
        <div class="customer_old">
            <div class="row">
                    <?php echo Yii::t('translation', $form->labelEx($model,'customer_id')); ?>
                    <?php echo $form->hiddenField($model,'customer_id'); ?>
                    <?php 
                            $this->widget('zii.widgets.jui.CJuiAutoComplete', array(
                                    'attribute'=>'autocomplete_name',
                                    'model'=>$model,
                                    'sourceUrl'=>Yii::app()->createAbsoluteUrl('admin/site/AutocompleteCustomerMaintain'),
                    //                                'name'=>'my_input_name',
                                    'options'=>array(
                                            'minLength'=>MIN_LENGTH_AUTOCOMPLETE,
                                            'multiple'=> true,
                                        'select'=>"js:function(event, ui) {
                                                $('#GasMaintain_customer_id').val(ui.item.id);
                                                var remove_div = '<span class=\'remove_row_item\' onclick=\'fnRemoveName(this)\'></span>';
                                                $('#GasMaintain_autocomplete_name').parent('div').find('.remove_row_item').remove();
                                                $('#GasMaintain_autocomplete_name').attr('readonly',true).after(remove_div);
                                                fnBuildTableInfo(ui.item);
                                                $('.autocomplete_customer_info').show();
                                        }",
                                    ),
                                    'htmlOptions'=>array(
                                        'size'=>45,
                                        'maxlength'=>45,
                                        'style'=>'float:left;',
                                        'placeholder'=>'Nhập tên, số ĐT hoặc mã KH',
                                    ),
                            )); 
                            ?>        
                            <script>
                                function fnRemoveName(this_){
                                    $(this_).prev().attr("readonly",false); 
                                    $("#GasMaintain_autocomplete_name").val("");
                                    $("#GasMaintain_customer_id").val("");
                                    $('.autocomplete_customer_info').hide();
                                }
                            function fnBuildTableInfo(item){
                                $(".info_name").text(item.value);
                                $(".info_name_agent").text(item.name_agent);
                                $(".info_code_account").text(item.code_account);
                                $(".info_code_bussiness").text(item.code_bussiness);
                                $(".info_address").text(item.address);
                                $(".info_phone").text(item.phone);
                            }

                            </script>        
                            <div class="clr"></div>		
                    <?php echo $form->error($model,'customer_id'); ?>
                    <?php $display='display:inline;';
                        $info_name ='';
                        $info_name_agent ='';
                        $info_address ='';
                        $info_code_account ='';
                        $info_code_bussiness ='';
                        $info_phone ='';
                            if(empty($model->customer_id)) $display='display: none;';
                            else{
                                $info_name = $model->customer->first_name;
                                $info_name_agent = $model->customer->name_agent;
                                $info_code_account = $model->customer->code_account;
                                $info_code_bussiness = $model->customer->code_bussiness;
                                $info_address = $model->customer->address;
                                $info_phone = $model->customer->phone;
                            }
                    ?>                        
                    <div class="autocomplete_customer_info" style="<?php echo $display;?>">
                    <table>
                        <tr>
                            <td class="_l">Mã khách hàng:</td>
                            <td class="_r info_code_bussiness"><?php echo $info_code_bussiness;?></td>
                        </tr>

                        <tr>
                            <td class="_l">Tên khách hàng:</td>
                            <td class="_r info_name"><?php echo $info_name;?></td>
                        </tr>                    
                        <tr>
                            <td class="_l">Địa chỉ:</td>
                            <td class="_r info_address"><?php echo $info_address;?></td>
                        </tr>
                        <tr>
                            <td class="_l">Điện Thoại:</td>
                            <td class="_r info_phone"><?php echo $info_phone;?></td>
                        </tr>


                    </table>
                </div>
                <div class="clr"></div>    		
                <?php // echo $form->error($model,'customer_id'); ?>
            </div>
            
        </div>
                
            
        <div class="customer_new">
            <div class="row">
                    <?php echo Yii::t('translation', $form->labelEx($model,'customer_code_bussiness')); ?>
                    <?php echo $form->textField($model,'customer_code_bussiness',array('size'=>60,'maxlength'=>20)); ?>
                    <?php echo $form->error($model,'customer_code_bussiness'); ?>
            </div>
            <div class="row">
                    <?php echo Yii::t('translation', $form->labelEx($model,'customer_name')); ?>
                    <?php echo $form->textField($model,'customer_name',array('size'=>60,'maxlength'=>100)); ?>
                    <?php echo $form->error($model,'customer_name'); ?>
            </div>
            <div class="row">
                    <?php echo Yii::t('translation', $form->labelEx($model,'customer_address')); ?>
                    <?php echo $form->textArea($model,'customer_address',array('style'=>'width:376px','maxlength'=>500)); ?>
                    <?php echo $form->error($model,'customer_address'); ?>
            </div>

            <div class="row">
                    <?php echo Yii::t('translation', $form->labelEx($model,'customer_phone')); ?>
                    <?php echo $form->textField($model,'customer_phone',array('size'=>60,'maxlength'=>20)); ?>
                    <?php echo $form->error($model,'customer_phone'); ?>
            </div>        
        </div>
    
	<div class="row">
		<?php echo Yii::t('translation', $form->labelEx($model,'maintain_date')); ?>
                <?php 
                    $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                        'model'=>$model,        
                        'attribute'=>'maintain_date',
                        'options'=>array(
                            'showAnim'=>'fold',
                            'dateFormat'=> ActiveRecord::getDateFormatJquery(),
//                            'minDate'=> '0',
                            'changeMonth' => true,
                            'changeYear' => true,
                            'showOn' => 'button',
                            'buttonImage'=> Yii::app()->theme->baseUrl.'/admin/images/icon_calendar_r.gif',
                            'buttonImageOnly'=> true,                                
                        ),        
                        'htmlOptions'=>array(
                            'class'=>'w-16',
                            'style'=>'height:20px;',
                                'readonly'=>'readonly',
                        ),
                    ));
                ?>     		
		<?php echo $form->error($model,'maintain_date'); ?>
	</div>

	<div class="row">
		<?php echo Yii::t('translation', $form->labelEx($model,'maintain_employee')); ?>
		<?php echo $form->textField($model,'maintain_employee',array('size'=>60,'maxlength'=>100)); ?>
		<?php echo $form->error($model,'maintain_employee'); ?>
	</div>

	<div class="row">
		<?php echo Yii::t('translation', $form->labelEx($model,'materials_id')); ?>
		<?php $dataCat = CHtml::listData(GasMaterials::getCatLevel2(),'id','name','group'); ?>
		<?php echo $form->dropDownList($model,'materials_id', $dataCat,array('class'=>'category_ajax', 'style'=>'width:376px;','empty'=>'Select')); ?>		
		<?php echo $form->error($model,'materials_id'); ?>
	</div>

	<div class="row">
		<?php echo Yii::t('translation', $form->labelEx($model,'seri_no')); ?>
		<?php echo $form->textField($model,'seri_no',array('style'=>'width:376px','maxlength'=>20)); ?>
		<?php echo $form->error($model,'seri_no'); ?>
	</div>

	<div class="row">
		<?php echo Yii::t('translation', $form->labelEx($model,'note')); ?>
		<?php echo $form->textArea($model,'note',array('style'=>'width:376px','maxlength'=>500)); ?>
		<?php echo $form->error($model,'note'); ?>
	</div>


	<div class="row buttons" style="padding-left: 141px;">
		        <?php $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType'=>'submit',
            'label'=>$model->isNewRecord ? Yii::t('translation', 'Create') : Yii::t('translation', 'Save'),
            'type'=>'null', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
            'size'=>'small', // null, 'large', 'small' or 'mini'
            //'htmlOptions' => array('style' => 'margin-bottom: 10px; float: right;'),
        )); ?>	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->

<style>
    .radio-list-2 {padding-left: 138px; padding-bottom: 5px;}
    .radio-list-2 label {font-weight: bold; padding-right: 25px;}
    
</style>

<script>
    $(document).ready(function(){
        $('.radio-list-2').find('input:radio').click(function(){
            var val_radio = $(this).val();
            if(val_radio==<?php echo CUSTOMER_NEW;?>){ // new customer
                $('.customer_old').hide();
                $('.customer_new').show();
                $('.remove_row_item').click();
                
            }else{ // old customer
                $('.customer_old').show();
                $('.customer_new').hide();
            }
                
            
        });
        
        
    });
    
    $(window).load(function(){
        $('#GasMaintain_is_new_customer_<?php echo $model->is_new_customer-1;?>').trigger('click');
    });
    
</script>