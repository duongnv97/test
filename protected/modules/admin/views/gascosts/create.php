<?php
$this->breadcrumbs=array(
	Yii::t('translation','Chi Phí')=>array('index'),
	Yii::t('translation','Create'),
);

$menus = array(		
        array('label'=> Yii::t('translation', 'Chi Phí') , 'url'=>array('index')),
);
$this->menu= ControllerActionsName::createMenusRoles($menus, $actions);
?>

<h1><?php echo Yii::t('translation', 'Create Chi Phí'); ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>