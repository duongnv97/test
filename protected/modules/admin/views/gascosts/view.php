<?php
$this->breadcrumbs=array(
	'Chi Phí'=>array('index'),
	$model->name,
);

$menus = array(
	array('label'=>'Chi Phí', 'url'=>array('index')),
	array('label'=>'Create Chi Phí', 'url'=>array('create')),
	array('label'=>'Update Chi Phí', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete Chi Phí', 'url'=>array('delete'), 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
);
$this->menu= ControllerActionsName::createMenusRoles($menus, $actions);
?>

<h1>View Chi Phí: <?php echo $model->name; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'name',
		'order_no',
		
		'status:status',
	),
)); ?>
