<div class="form">

    <?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'gas-one-many-form',
	'enableAjaxValidation'=>false,
        'htmlOptions' => array('enctype' => 'multipart/form-data'),
)); ?>

    <p class="note">Dòng có dấu <span class="required">*</span> là bắt buộc.</p>
    <?php echo MyFormat::BindNotifyMsg(); ?>
    
    <div class="row ">
        <?php echo $form->labelEx($model, 'one_id', ['label'=>'Đại lý']); ?>
        <?php echo $form->hiddenField($model, 'one_id'); ?>
        <?php
        // 1. limit search kh của sale
        $url = Yii::app()->createAbsoluteUrl('admin/ajax/search_by_role', array('role' => ROLE_AGENT));
        // widget auto complete search user customer and supplier
        $aData = array(
            'model' => $model,
            'field_customer_id' => 'one_id',
            'url' => $url,
            'name_relation_user' => 'one',
            'ClassAdd' => 'w-300',
            'placeholder' => 'Nhập mã hoặc tên đại lý',
        );
        $this->widget('ext.SpaAutocompleteCustomer.SpaAutocompleteCustomer', array('data' => $aData));
        ?>
        <?php echo $form->error($model, 'one_id'); ?>
    </div>
    
    <div class="row">
        <?php echo $form->labelEx($model,'many_id', ['label'=>'Giá tăng / giảm']); ?>
        <?php echo $form->textField($model,'many_id',array('class'=>'w-300 number_only ad_fix_currency')); ?>
        <?php echo $form->error($model,'many_id'); ?>
    </div>
    
    <div class="row">
        <?php echo $form->labelEx($model, 'date_apply'); ?>
        <?php 
        $this->widget('zii.widgets.jui.CJuiDatePicker', array(
            'model' => $model,
            'attribute' => 'date_apply',
            'options' => array(
                'showAnim' => 'fold',
                'dateFormat' => MyFormat::$dateFormatSearch,
                'changeMonth' => true,
                'changeYear' => true,
                'showOn' => 'button',
                'buttonImage' => Yii::app()->theme->baseUrl . '/admin/images/icon_calendar_r.gif',
                'buttonImageOnly' => true,
            ),
            'htmlOptions' => array(
                'class' => 'w-16',
                'style' => 'height:20px;',
                'readonly' => 'readonly',
            ),
        ));
        ?>  
    <?php echo $form->error($model, 'date_apply'); ?>
    </div>
    
    <div class="row buttons" style="padding-left: 141px;">
        <?php $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType'=>'submit',
            'label'=>$model->isNewRecord ? 'Create' :'Save',
            'type'=>'null', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
            'size'=>'small', // null, 'large', 'small' or 'mini'
            //'htmlOptions' => array('style' => 'margin-bottom: 10px; float: right;'),
        )); ?>	
    </div>

<?php $this->endWidget(); ?>

</div><!-- form -->
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/autoNumeric-master/autoNumeric.js"></script>
<script>
    $(document).ready(function(){
        fnInitInputCurrency();
        $('.form').find('button:submit').click(function(){
            $.blockUI({ overlayCSS: { backgroundColor: '#fff' } }); 
        });
        
    });
</script>