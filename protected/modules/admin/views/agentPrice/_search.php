<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=> GasCheck::getCurl(),
	'method'=>'get',
)); ?>

	<div class="row ">
            <?php echo $form->labelEx($model, 'one_id', ['label'=>'Đại lý']); ?>
            <?php echo $form->hiddenField($model, 'one_id'); ?>
            <?php
            // 1. limit search kh của sale
            $url = Yii::app()->createAbsoluteUrl('admin/ajax/search_by_role', array('role' => ROLE_AGENT));
            // widget auto complete search user customer and supplier
            $aData = array(
                'model' => $model,
                'field_customer_id' => 'one_id',
                'url' => $url,
                'name_relation_user' => 'one',
                'ClassAdd' => 'w-300',
                'placeholder' => 'Nhập mã hoặc tên đại lý',
            );
            $this->widget('ext.SpaAutocompleteCustomer.SpaAutocompleteCustomer', array('data' => $aData));
            ?>
            <?php echo $form->error($model, 'one_id'); ?>
        </div>

	<div class="row more_col">
            <div class="col1">
                <?php echo $form->labelEx($model,'searchFrom', ['label'=>'Giá tăng / giảm từ']); ?>
                <?php echo $form->textField($model,'searchFrom',array('class'=>'w-150 number_only ad_fix_currency')); ?>
            </div>
            <div class="col2">
                <?php echo $form->labelEx($model,'searchTo', ['label'=>'Đến']); ?>
                <?php echo $form->textField($model,'searchTo',array('class'=>'w-150 number_only ad_fix_currency')); ?>
            </div>
        </div>
    
    <div class="row more_col">
        <div class="col1">
            <?php echo $form->labelEx($model, 'dateFrom', ['label'=>'Ngày áp dụng từ']); ?>
            <?php 
            $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                'model' => $model,
                'attribute' => 'dateFrom',
                'options' => array(
                    'showAnim' => 'fold',
                    'dateFormat' => MyFormat::$dateFormatSearch,
                    'changeMonth' => true,
                    'changeYear' => true,
                    'showOn' => 'button',
                    'buttonImage' => Yii::app()->theme->baseUrl . '/admin/images/icon_calendar_r.gif',
                    'buttonImageOnly' => true,

                ),

                'htmlOptions' => array(
                    'class' => 'w-150',
                    'style' => 'height:20px;',
                ),
            ));
            ?>  
        </div>
        <div class="col2">
            <?php echo $form->labelEx($model, 'dateTo', ['label'=>'Đến']); ?>
            <?php 
            $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                'model' => $model,
                'attribute' => 'dateTo',
                'options' => array(
                    'showAnim' => 'fold',
                    'dateFormat' => MyFormat::$dateFormatSearch,
                    'changeMonth' => true,
                    'changeYear' => true,
                    'showOn' => 'button',
                    'buttonImage' => Yii::app()->theme->baseUrl . '/admin/images/icon_calendar_r.gif',
                    'buttonImageOnly' => true,
                ),
                'htmlOptions' => array(
                    'class' => 'w-150',
                    'style' => 'height:20px;',
                ),
            ));
            ?>  
        </div>
    </div>

	<div class="row buttons" style="padding-left: 159px;">
		        <?php $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType'=>'submit',
            'label'=>'Search',
            'type'=>'null', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
            'size'=>'small', // null, 'large', 'small' or 'mini'
            //'htmlOptions' => array('style' => 'margin-bottom: 10px; float: right;'),
        )); ?>	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->