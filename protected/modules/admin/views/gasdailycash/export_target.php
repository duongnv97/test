<h1><?php echo Yii::t('translation', 'Xuất Dữ liệu target hàng ngày'); ?></h1>

<?php //echo CHtml::link(Yii::t('translation','Advanced Search'),'#',array('class'=>'search-button')); ?>
<div class="search-form" style="">
<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'post',
)); ?>

        <div class="row">
		<?php echo Yii::t('translation', $form->label($model,'sell_date_from')); ?>
		<?php // echo $form->textField($model,'sell_date_from'); ?>
                <?php 
                    $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                        'model'=>$model,        
                        'attribute'=>'sell_date_from',
                        'options'=>array(
                            'showAnim'=>'fold',
                            'dateFormat'=> ActiveRecord::getDateFormatJquery(),
//                            'minDate'=> '0',
                            'changeMonth' => true,
                            'changeYear' => true,
                            'showOn' => 'button',
                            'buttonImage'=> Yii::app()->theme->baseUrl.'/admin/images/icon_calendar_r.gif',
                            'buttonImageOnly'=> true,                                
                        ),        
                        'htmlOptions'=>array(
                            'class'=>'w-16',
                            'style'=>'height:20px;',
//                                'readonly'=>'readonly',
                        ),
                    ));
                ?>            
		<?php echo $form->error($model,'sell_date_from'); ?>
	</div>

	<div class="row buttons" style="padding-left: 159px;padding-top: 28px;">
		        <?php $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType'=>'submit',
            'label'=>Yii::t('translation','Xuất target hàng ngày ra Excel '),
            'type'=>'null', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
            'size'=>'small', // null, 'large', 'small' or 'mini'
            //'htmlOptions' => array('style' => 'margin-bottom: 10px; float: right;'),
        )); ?>	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->
</div><!-- search-form -->
<style>
    div.form .group_subscriber .fix-label label {width: auto;} 
</style>
<script>
    $(window).load(function(){
        $('.group_subscriber').find('.ui-multiselect').eq(0).click(function(){
            if($(".ui-multiselect-menu").css('display')=='block')
                $('.ui-multiselect-menu').hide();
            else
                $('.ui-multiselect-menu').show();
        });
    });
</script>
