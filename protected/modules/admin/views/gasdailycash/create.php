<?php
$this->breadcrumbs=array(
	Yii::t('translation','QL Thu Tiền')=>array('index'),
	Yii::t('translation','Thêm Mới'),
);

$menus = array(		
        array('label'=> Yii::t('translation', 'QL Thu Tiền') , 'url'=>array('index')),
);
$this->menu= ControllerActionsName::createMenusRoles($menus, $actions);
?>

<h1><?php echo Yii::t('translation', 'Thêm Mới Thu Tiền'); ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>