<?php
$this->breadcrumbs=array(
	Yii::t('translation','Báo cáo'),
);
?>

<h1><?php echo Yii::t('translation', 'Các loại báo cáo'); ?></h1>

<div class="search-form" style="">
    <div class="wide form report-form">
        <div class="col1">
            <ul>
                <li>
                    <a href="<?php echo Yii::app()->createAbsoluteUrl('admin/gasdailyselling/export_dailyselling');?>">
                        Export Bán Hàng
                    </a>         
                </li>

                <li>
                    <a href="<?php echo Yii::app()->createAbsoluteUrl('admin/gasdailycash/export_dailycash');?>">
                        Export Thu Tiền
                    </a> 	
                </li>

            </ul>
        </div>
        <div class="col2">
            <ul>
                <li>
                    <a href="<?php echo Yii::app()->createAbsoluteUrl('admin/gasdailycash/export_target');?>">
                        Export Export_target
                    </a>         
                </li>

                <li>
                    <a href="<?php echo Yii::app()->createAbsoluteUrl('admin/gasdailycash/export_dailycash');?>">
                       xxxxx
                    </a> 	
                </li>

            </ul>
        </div>
        <div class="clr"></div>
    </div><!-- search-form -->
</div><!-- search-form -->

<style>
    .report-form .col1 {width: 50%; float: left;}
    .report-form .col2 {width: 50%; float: left;}
    .report-form ul li {width: 300px;}
    
</style>