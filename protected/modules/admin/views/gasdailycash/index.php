<?php
$this->breadcrumbs=array(
	'QL Thu Tiền',
);

$menus=array(
	array('label'=> Yii::t('translation','Thêm Mới Thu Tiền'), 'url'=>array('create')),
);
$this->menu= ControllerActionsName::createMenusRoles($menus, $actions);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('gas-daily-cash-grid', {
                url : $(this).attr('action'),
		data: $(this).serialize()
	});
	return false;
});
");

Yii::app()->clientScript->registerScript('ajaxupdate', "
$('#gas-daily-cash-grid a.ajaxupdate').live('click', function() {
    $.fn.yiiGridView.update('gas-daily-cash-grid', {
        type: 'POST',
        url: $(this).attr('href'),
        success: function() {
            $.fn.yiiGridView.update('gas-daily-cash-grid');
        }
    });
    return false;
});
");
?>

<h1><?php echo Yii::t('translation', 'QL Thu Tiền'); ?></h1>
<?php if(Yii::app()->user->role_id==ROLE_ADMIN):?>
<h2>
    <a href="<?php echo Yii::app()->createAbsoluteUrl('admin/gasdailycash/import_dailycash');?>">
        Nhập từ file Excel
    </a>
	<!--
	<br />
    <a href="<?php echo Yii::app()->createAbsoluteUrl('admin/gasdailycash/export_dailycash');?>">
        Xuất Thu Tiền Ra file Excel
    </a> 	
	<br />
    <a href="<?php echo Yii::app()->createAbsoluteUrl('admin/gasdailycash/export_dailyplan');?>">
        Xuất Kế Hoạch Hàng Ngày Ra file Excel
    </a> 	
    -->
</h2>
<?php endif;?>

<?php echo CHtml::link(Yii::t('translation','Advanced Search'),'#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'gas-daily-cash-grid',
	'dataProvider'=>$model->search(),
        'template'=>'{pager}{summary}{items}{pager}{summary}', 
	'enableSorting' => false,
	//'filter'=>$model,
        'pager' => array(
            'maxButtonCount'=>  CmsFormatter::$PAGE_MAX_BUTTON,
        ),        
	'columns'=>array(
        array(
            'header' => 'S/N',
            'type' => 'raw',
            'value' => '$this->grid->dataProvider->pagination->currentPage * $this->grid->dataProvider->pagination->pageSize + ($row+1)',
            'headerHtmlOptions' => array('width' => '30px','style' => 'text-align:center;'),
            'htmlOptions' => array('style' => 'text-align:center;')
        ),
array(
            'header' => 'Mã KH',
            'value' => '$data->customer?$data->customer->code_account:""',
        ), 		
        array(
            'name' => 'customer_id',
            'value' => '$data->customer?$data->customer->first_name:""',
            'htmlOptions' => array('style' => 'text-align:left;width:350px;')
        ),   	
		array(
            'header' => 'Tên Phụ',
            'value' => '$data->customer?$data->customer->name_agent:""',
        ),
        array(
            'name' => 'date_collection',
            'type' => 'Date',
            'htmlOptions' => array('style' => 'text-align:center;')
        ), 		
            
        array(
            'name' => 'total',
            'type' => 'Currency',
            'htmlOptions' => array('style' => 'text-align:right;')
        ),            
      
            
        /* array(
            'header' => 'Tỉnh',
            'value' => '$data->customer?($data->customer->province?$data->customer->province->name:""):""',
        ),            
		 
        array(
            'header' => 'Kênh',
            'value' => '$data->customer?$data->customer->channel->name:""',
        ),            
        array(
            'header' => 'Khu Vực',
            'value' => '$data->customer?($data->customer->district?$data->customer->district->name:""):""',
        ),            
        array(
            'header' => 'Sale',
            'value' => '$data->customer?$data->customer->sale->first_name:""',
        ),       */      
		            
            
                    /* 'created_date:Datetime', */
		array(
			'header' => 'Action',
			'class'=>'CButtonColumn',
                        'template'=> ControllerActionsName::createIndexButtonRoles($actions),
		),
	),
)); ?>
