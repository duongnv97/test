<?php
$this->breadcrumbs=array(
	Yii::t('translation','QL Thu Tiền')=>array('index'),
	($model->customer?$model->customer->first_name:'')=>array('view','id'=>$model->id),
	Yii::t('translation','Update'),
);

$menus = array(	
        array('label'=> Yii::t('translation', 'Thu Tiền'), 'url'=>array('index')),
	array('label'=> Yii::t('translation', 'Xem Thu Tiền'), 'url'=>array('view', 'id'=>$model->id)),
	array('label'=> Yii::t('translation', 'Thêm Mới Thu Tiền'), 'url'=>array('create')),	
);
$this->menu= ControllerActionsName::createMenusRoles($menus, $actions);

?>

<h1><?php echo Yii::t('translation', 'Cập Nhật Thu Tiền: '.($model->customer?$model->customer->first_name:'')); ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>