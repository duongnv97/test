<?php
$this->breadcrumbs=array(
	'QL Thu Tiền'=>array('index'),
	$model->customer?$model->customer->first_name:'',
);

$menus = array(
	array('label'=>'Thu Tiền', 'url'=>array('index')),
	array('label'=>'Thêm Mới Thu Tiền', 'url'=>array('create')),
	array('label'=>'Cập Nhật Thu Tiền', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Xóa Thu Tiền', 'url'=>array('delete'), 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
);
$this->menu= ControllerActionsName::createMenusRoles($menus, $actions);
?>

<h1>View Thu tiền: <?php echo $model->customer?$model->customer->first_name:''; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
            array(
                'name'=>'customer_id',
                'value'=>$model->customer?$model->customer->first_name:'',
            ),    		
		'date_collection:Date',
		'total:Currency',
		
         array(
            'name'=>'agent_id',
            'value' => $model->agent?$model->agent->first_name:'',
        ),    		
		
		/*
		array(
            'label' => 'Tên Đại Lý',
            'value' => $model->customer?$model->customer->name_agent:"",
        ),      
            
        array(
            'label' => 'Tỉnh',
            'value' => $model->customer?$model->customer->province->name:"",
        ),            
		 
        array(
            'label' => 'Kênh',
            'value' => $model->customer?$model->customer->channel->name:"",
        ),            
        array(
            'label' => 'Khu Vực',
            'value' => $model->customer?($model->customer->district?$model->customer->district->name:""):"",
        ),            
        array(
            'label' => 'Sale',
            'value' => $model->customer?$model->customer->sale->first_name:"",
        ),           */   
		'created_date:Datetime',
	),
)); ?>
