<div class="form">

<?php 
    $form=$this->beginWidget('CActiveForm', array(
            'id'=>'coefficients-form',
            'enableAjaxValidation'=>false,
            'htmlOptions' => array('enctype' => 'multipart/form-data'),
    )); ?>

    <p class="note">Dòng có dấu <span class="required">*</span> là bắt buộc.</p>
    <?php echo MyFormat::BindNotifyMsg(); ?>
    <div class="row">
        <?php echo $form->labelEx($model,'name'); ?>
        <?php echo $form->textField($model,'name', array('class'=>'w-400')); ?>
        <?php echo $form->error($model,'name'); ?>
    </div>
    
    <div class="row">
        <?php echo $form->labelEx($model,'value'); ?>
        <?php echo $form->textField($model,'value',array('class'=>'number_only_v1 w-400 item_l')); ?>
        <?php echo $form->error($model,'value'); ?>
    </div>
    
    <div class="row">
        <?php echo $form->labelEx($model,'position_work'); ?>
        <?php echo $form->dropdownList($model,'position_work', HrSalaryReports::model()->getArrayWorkPosition(), array('class'=>'number_only_v1 w-400','empty'=>'Select')); ?>
        <?php echo $form->error($model,'position_work'); ?>
    </div>
    
    <div class="row">
        <?php echo $form->labelEx($model,'position_work_list'); ?>
        <div class="fix-label">
            <?php
               $this->widget('ext.multiselect.JMultiSelect',array(
                     'model'=>$model,
                     'attribute'=>'position_work_list',
                     'data'=> UsersProfile::model()->getArrWorkRoom(),
                     // additional javascript options for the MultiSelect plugin
                    'options'=>array('selectedList' => 30),
                     // additional style
                     'htmlOptions'=>array('class' => 'w-400'),
               ));    
           ?>
        </div>
        <?php echo $form->error($model,'position_work_list'); ?>
    </div>
    
    <div class="row buttons">
        <?php $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType'=>'submit',
            'label'=>$model->isNewRecord ? 'Create' :'Save',
            'type'=>'null', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
            'size'=>'small', // null, 'large', 'small' or 'mini'
            //'htmlOptions' => array('style' => 'margin-bottom: 10px; float: right;'),
        )); ?>	
    </div>

<?php $this->endWidget(); ?>
    
</div><!-- form -->
