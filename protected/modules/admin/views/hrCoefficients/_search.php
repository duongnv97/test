
<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>
	<div class="row">
        <?php echo $form->labelEx($model,'name'); ?>
        <?php echo $form->textField($model,'name',array('class'=>'w-300',)); ?>
        <?php echo $form->error($model,'name'); ?>
        </div>

        <div class="row">
            <?php echo $form->labelEx($model,'value'); ?>
            <?php echo $form->textField($model,'value',array('class'=>'number_only_v1 w-300',)); ?>
            <?php echo $form->error($model,'value'); ?>
        </div>
    
        <div class="row">
            <?php echo $form->labelEx($model,'position_work'); ?>
            <div class="fix-label">
                <?php
                   $this->widget('ext.multiselect.JMultiSelect',array(
                         'model'=>$model,
                         'attribute'=>'position_work',
                         'data'=> HrSalaryReports::model()->getArrayWorkPosition(),
                         // additional javascript options for the MultiSelect plugin
                        'options'=>array('selectedList' => 30),
                         // additional style
                         'htmlOptions'=>array('class' => 'w-400'),
                   ));    
               ?>
            </div>
        </div>

        <div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->