<?php
$this->breadcrumbs=array(
	$this->singleTitle,
);

$menus=array(
    array('label'=>"Create $this->singleTitle", 'url'=>array('create')),
);
$this->menu= ControllerActionsName::createMenusRoles($menus, $actions);
Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('Coefficients-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>
<?php echo MyFormat::BindNotifyMsg(); ?>
<h1>Danh Sách <?php echo $this->pluralTitle; ?></h1>

<?php echo CHtml::link('Tìm Kiếm Nâng Cao','#',array('class'=>'search-button')); ?>
    <div class="search-form" style="display:none">
    <?php $this->renderPartial('_search',array(
            'model'=>$model,
    )); ?>
    </div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'Coefficients-grid',
	'dataProvider'=>$model->search(),
        'pager' => array(
            'maxButtonCount'=>  CmsFormatter::$PAGE_MAX_BUTTON,
        ),
	'columns'=>array(
            array(
                'header' => 'S/N',
                'type' => 'raw',
                'value' => '$this->grid->dataProvider->pagination->currentPage * $this->grid->dataProvider->pagination->pageSize + ($row+1)',
                'headerHtmlOptions' => array('width' => '10px','style' => 'text-align:center;'),
                'htmlOptions' => array('style' => 'text-align:center;')
            ),
            array(
                'name'=>'name',
                'type'=>'raw',
                'value'=>'$data->getName()',
            ),
            array(
                'name'=>'value',
                'type'=>'raw',
                'value'=>'$data->getValue()',
            ),
            array(
                'name'=>'position_work',
                'type'=>'raw',
                'value'=>'$data->getPositionWork()',
            ),
            array(
                'name'=>'position_work_list',
                'type'=>'raw',
                'value'=>'$data->getPositionWork(true)',
            ),
            
//            array(
//                'name'=>'status',
//                'type'=>'raw',
//                'value'=>'$data->getStatus()',
//                'visible'=> $model->canView(),
//            ),
           
            array(
            'header' => 'Actions',
            'class' => 'CButtonColumn',
            'template' => ControllerActionsName::createIndexButtonRoles($actions),
            'buttons' => array(
                
                ),
            ),
	),
)); ?>