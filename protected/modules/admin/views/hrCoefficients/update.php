<?php
$this->breadcrumbs = array(
	'Danh sách hệ số' => array('index'),
);

$menus = array(	
    array('label' => $this->pluralTitle, 'url' => array('index')),
    array('label' => 'Xem ' . $this->singleTitle, 'url' => array('view', 'id' => $model->id)),	
);
$this->menu= ControllerActionsName::createMenusRoles($menus, $actions);
$mRole = new Roles();
$role_name = isset($model->role_id) ? $mRole->GetRoleNameById($model->role_id) : '';
?>

<h1>Cập Nhật <?php echo $this->pluralTitle." ".$role_name; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>