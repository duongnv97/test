<?php $role = isset($_GET['role']) ? $_GET['role'] : ''; ?>
<span id="current_role_id" data-value="<?php echo $role; ?>"></span>

<?php
$data = $model->getCoefficientsByRole($role);
$dataHTML = "";
$createUrl = Yii::app()->createAbsoluteUrl('/admin/hrCoefficients/create', array('ajax'=>1));

$actions = array('Create','Update','Delete', 'View');
Yii::app()->clientScript->registerScript('updateGridView', "
$('.search-form form').submit(function(){
        $.fn.yiiGridView.update('Coefficients-grid".$role."');
});
");
/*DuongNV 06/08/18 17:17 Một vài trường hợp bị lỗi*/
//Yii::app()->clientScript->registerScript("create", "
//    $('.create').click(function(e){
//        var url = '".$createUrl."' +'/role/".$role."';
//        $('.create a').attr('href',url);
//    })
//");
$this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'Coefficients-grid'.$role,
	'dataProvider'=>$data,
        'pager' => array(
            'maxButtonCount'=>  CmsFormatter::$PAGE_MAX_BUTTON,
        ),
	'columns'=>array(
            array(
                'header' => 'S/N',
                'type' => 'raw',
                'value' => '$this->grid->dataProvider->pagination->currentPage * $this->grid->dataProvider->pagination->pageSize + ($row+1)',
                'headerHtmlOptions' => array('width' => '10px','style' => 'text-align:center;'),
                'htmlOptions' => array('style' => 'text-align:center;')
            ),
            array(
                'name'=>'name',
                'type'=>'raw',
                'value'=>'$data->getName()',
            ),
            array(
                'name'=>'value',
                'type'=>'raw',
                'value'=>'$data->getValue()',
            ),
            
            array(
                'name'=>'status',
                'type'=>'raw',
                'value'=>'$data->getStatus()',
                'visible'=> $model->canView(),
            ),
           
            array(
            'header' => 'Actions',
            'class' => 'CButtonColumn',
            'template' => ControllerActionsName::createIndexButtonRoles($actions),
            'buttons' => array(
                
                ),
            ),
	),
)); ?>

