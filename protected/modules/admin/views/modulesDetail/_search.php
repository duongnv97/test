<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=> GasCheck::getCurl(),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'modules_id',array()); ?>
		<?php echo $form->textField($model,'modules_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'type',array()); ?>
		<?php echo $form->textField($model,'type'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'materials_type_id',array()); ?>
		<?php echo $form->textField($model,'materials_type_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'materials_id',array()); ?>
		<?php echo $form->textField($model,'materials_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'qty',array()); ?>
		<?php echo $form->textField($model,'qty'); ?>
	</div>

	<div class="row buttons" style="padding-left: 159px;">
		        <?php $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType'=>'submit',
            'label'=>'Search',
            'type'=>'null', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
            'size'=>'small', // null, 'large', 'small' or 'mini'
            //'htmlOptions' => array('style' => 'margin-bottom: 10px; float: right;'),
        )); ?>	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->