<div class="form">
<?php 
Yii::app()->clientScript->registerCoreScript('jquery.ui');
?>
<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'modules-detail-form',
	'enableAjaxValidation'=>false,
        'htmlOptions' => array('enctype' => 'multipart/form-data'),
)); ?>

    <p class="note">Dòng có dấu <span class="required">*</span> là bắt buộc.</p>
    <?php echo MyFormat::BindNotifyMsg(); ?>
    
        		
        
    <div class="row">
        <?php echo $form->labelEx($model,'modules_id'); ?>
        <?php echo $form->dropdownList($model,'modules_id', Modules::model()->getArrAllModule('name'), array('class'=>'w-370')); ?>
        <?php echo $form->error($model,'modules_id'); ?>
    </div>
    <div class="row">
        <?php echo $form->labelEx($model,'type'); ?>
        <?php echo $form->dropdownList($model,'type', Modules::model()->getArrayTypeNameModule(), array('class'=>'w-370')); ?>
        <?php echo $form->error($model,'type'); ?>
    </div>
    
    <div class="row">
        <?php echo $form->labelEx($model,'materials_id'); ?>
        <?php echo $form->hiddenField($model,'materials_id'); ?>		
        <?php 
            // widget auto complete search material
//            $mCustomerRequest = new GasSupportCustomerRequest();
            $aData = array(
                'model'=>$model,
//                'MaterialsJson'=> $mCustomerRequest->formatToJsonMaterialForWeb(),
                'field_material_id'=>'materials_id',
                'name_relation_material'=>'rMaterials',
                'field_autocomplete_name'=>'autocomplete_material',
                'htmlOptions' => array(
                    'class' => 'w-300'
                )
            );
            $this->widget('ext.GasAutocompleteMaterial.GasAutocompleteMaterial',
                array('data'=>$aData));                                        
        ?> 
        <?php echo $form->error($model,'materials_id'); ?>
    </div>
<!--    
    <div class="row">
        <?php echo $form->labelEx($model,'materials_type_id'); ?>
        <?php echo $form->dropdownList($model,'materials_type_id', GasMaterialsType::getAllItem() , array('class' => 'w-370')); ?>
        <?php echo $form->error($model,'materials_type_id'); ?>
    </div>-->

    <div class="row">
        <?php echo $form->labelEx($model,'qty'); ?>
        <?php echo $form->textField($model,'qty', array('class' => 'w-370')); ?>
        <?php echo $form->error($model,'qty'); ?>
    </div>
	<div class="row buttons" style="padding-left: 141px;">
        <?php $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType'=>'submit',
            'label'=>$model->isNewRecord ? 'Create' :'Save',
            'type'=>'null', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
            'size'=>'small', // null, 'large', 'small' or 'mini'
            //'htmlOptions' => array('style' => 'margin-bottom: 10px; float: right;'),
        )); ?>	
        </div>

<?php $this->endWidget(); ?>

</div><!-- form -->
<script>
    $(document).ready(function(){
        $('.form').find('button:submit').click(function(){
            $.blockUI({ overlayCSS: { backgroundColor: '#fff' } }); 
        });
    });
</script>