<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'gas-file-scan-form',
	'enableAjaxValidation'=>false,
    'htmlOptions' => array('enctype' => 'multipart/form-data'),
)); ?>

	<p class="note">Dòng có dấu <span class="required">*</span> là bắt buộc.</p>
        <em class="hight_light item_b">Chú Ý: Bạn phải xoay hình để             
            hiển thị bình thường trước khi upload lên. <a target="_blank" href="<?php echo Yii::app()->createAbsoluteUrl('admin/site/news',array('id'=>28));?>">Xem Hướng Dẫn</a></em>
        <?php  if(Yii::app()->user->hasFlash('successUpdate')): ?>
            <div class='success_div'><?php echo Yii::app()->user->getFlash('successUpdate');?>
                <?php if(isset($_GET['view_id'])):?>
                    <?php $mNew = GasSalesFileScan::model()->findByPk($_GET['view_id']); ?>
                    &nbsp;&nbsp;&nbsp;<a href="<?php echo Yii::app()->createAbsoluteUrl('admin/gasSalesFileScan/view',array('id'=>$_GET['view_id'])); ?>" target="_blank" title="Xem lại file upload bán hàng"><?php echo $mNew->code_no;?></a>
                <?php endif;?>
            </div>         
        <?php endif; ?>
        
        <?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo Yii::t('translation', $form->labelEx($model,'date_sales')); ?>
                <?php 
                    $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                        'model'=>$model,        
                        'attribute'=>'date_sales',
                        'options'=>array(
                            'showAnim'=>'fold',
                            'dateFormat'=> ActiveRecord::getDateFormatJquery(),
//                            'minDate'=> '0',
                            'maxDate'=> '0',
                            'changeMonth' => true,
                            'changeYear' => true,
                            'showOn' => 'button',
                            'buttonImage'=> Yii::app()->theme->baseUrl.'/admin/images/icon_calendar_r.gif',
                            'buttonImageOnly'=> true,                                
                        ),        
                        'htmlOptions'=>array(
                            'class'=>'w-16',
                            'style'=>'height:20px;',
                                'readonly'=>'readonly',
                        ),
                    ));
                ?>     		
		<?php echo $form->error($model,'date_sales'); ?>
	</div>
	<div class="row">
		<?php echo $form->labelEx($model,'accounting_employee_id'); ?>
                <?php echo $form->dropDownList($model,'accounting_employee_id', Users::getSelectByRoleForAgent(Yii::app()->user->parent_id, ONE_AGENT_ACCOUNTING, $model->agent_id, array('status'=>1)),array('class'=>'','empty'=>'Select')); ?>
		<?php echo $form->error($model,'accounting_employee_id'); ?>
	</div>
            
        <div class="row">
            <label>&nbsp</label>
            <table class="materials_table hm_table">
                <thead>
                    <tr>
                        <th class="item_c">#</th>
                        <th class="item_code item_c">File Scan ( cho phép định dạng <?php echo GasSalesFileScanDetai::$AllowFile;?>)</th>
                        <th class="item_unit last item_c">Xóa</th>
                    </tr>
                </thead>
                <tbody>
                    <?php 
                        $max_upload = 5;
                    ?>
                    <?php if(count($model->aModelDetail)):?>
                    <?php foreach($model->aModelDetail as $key=>$item):?>
                    <tr class="materials_row">
                        <td class="item_c order_no"></td>
                        <td class="item_c w-400">
                            <?php // echo $form->fileField($item,'file_name[]'); ?>
                            <?php if(!empty($item->file_name) && !empty($item->date_sales)): ?>
                            <p>
                                <a rel="group1" class="gallery" href="<?php echo ImageProcessing::bindImageByModel($item);?>"> 
                                    <img width="100" height="70" src="<?php echo ImageProcessing::bindImageByModel($item);?>">
                                </a>
                            </p>
                            <?php endif;?>
                            <?php echo $form->error($item,'file_name'); ?>
                            <?php echo $form->hiddenField($item,'aIdNotIn[]', array('value'=>$item->id)); ?>
                        </td>
                        <td class="item_c last"><span class="remove_icon_only"></span></td>
                    </tr> 
                    <?php $max_upload--;?>
                    <?php endforeach;?>
                    <?php endif;?>
                    
                    <?php for($i=1; $i<=10; $i++): ?>
                    <?php $display = "";
                        if($i>$max_upload)
                            $display = "display_none";
                    ?>
                    <tr class="materials_row <?php echo $display;?>">
                        <td class="item_c order_no"></td>
                        <td class="item_l w-400">
                            <?php echo $form->fileField($model->mDetail,'file_name[]', array('accept'=>'image/*')); ?>
                            <?php // echo $form->error($model->mDetail,'file_name'); ?>                        
                        </td>
                        <td class="item_c last"><span class="remove_icon_only"></span></td>
                    </tr>                    
                    <?php endfor;?>
                </tbody>
            </table>
        </div>            
            
	<div class="row">
            <?php echo $form->labelEx($model,'note'); ?>
            <?php echo $form->textArea($model,'note',array('rows'=>5,'cols'=>80,"placeholder"=>"")); ?>
            <?php echo $form->error($model,'note'); ?>
	</div>        
        
	<div class="row buttons" style="padding-left: 141px;">
		        <?php $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType'=>'submit',
            'label'=>$model->isNewRecord ? 'Create' :'Save',
            'type'=>'null', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
            'size'=>'small', // null, 'large', 'small' or 'mini'
            //'htmlOptions' => array('style' => 'margin-bottom: 10px; float: right;'),
        )); ?>	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->
<script>
    $(document).ready(function(){
        $('.form').find('button:submit').click(function(){
            $.blockUI({ overlayCSS: { backgroundColor: '#fff' } }); 
        });
        fnRefreshOrderNumber();
        fnBindRemoveIcon();
        jQuery('a.gallery').colorbox({ opacity:0.5 , rel:'group1' });
    });
    
    function fnAfterRemoveIcon(){
    // tại sao lại chỉ xử lý khi update, khi tạo mới lại ko dc???
        <?php if(!$model->isNewRecord): ?>
                $('.materials_table tbody').find('.display_none').eq(0).removeClass('display_none');
        <?php endif;?>
    }
</script>
