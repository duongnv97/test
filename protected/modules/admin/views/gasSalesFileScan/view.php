<?php
$this->breadcrumbs=array(
	'Bán Hàng File Scan'=>array('index'),
	$model->code_no,
);

$menus = array(
	array('label'=>'Bán Hàng File Scan', 'url'=>array('index')),
	array('label'=>'Tạo Mới Bán Hàng File Scan', 'url'=>array('create')),
	array('label'=>'Cập Nhật Bán Hàng File Scan', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Xóa Bán Hàng File Scan', 'url'=>array('delete'), 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Bạn chắc chắn muốn xóa ?')),
);
$this->menu= ControllerActionsName::createMenusRoles($menus, $actions);
$model->aModelDetail = count($model->rFileScanDetail)?$model->rFileScanDetail:array();
?>

<h1>Xem Bán Hàng File Scan: <?php echo $model->code_no; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'code_no',
                array(
                    'name'=>'agent_id',
                    'value'=>$model->rAgent?$model->rAgent->first_name:'',
                ),            
//		'uid_login',
		'date_sales:date',
                array(
                    'name'=>'accounting_employee_id',
                    'value'=>$model->rAccountingEmployee?$model->rAccountingEmployee->first_name:'',
                ),            
		'created_date:datetime',
                array(
                    'name'=>'last_update_by',
                    'value'=>$model->rLastUpdateBy?$model->rLastUpdateBy->first_name:'',
                ),                   
		'last_update_time:datetime',
                array(
                    'name'=>'note',
                    'type'=>'html',
                    'value'=>nl2br($model->note),
                ),            
		
	),
)); ?>

<div class="row">
        <label>&nbsp</label>
        <table class="materials_table hm_table">
            <thead>
                <tr>
                    <th class="item_c">#</th>
                    <th class="item_code item_c">File Scan</th>
                </tr>
            </thead>
            <tbody>
                <?php if(count($model->aModelDetail)):?>
                <?php foreach($model->aModelDetail as $key=>$item):?>
                <tr class="materials_row">
                    <td class="item_c order_no"><?php echo $key+1;?></td>
                    <td class="item_c w-400 last">
                        <?php // echo $form->fileField($item,'file_name[]'); ?>
                        <p>
                            <a rel="group1" class="gallery" href="<?php echo ImageProcessing::bindImageByModel($item);?>"> 
                                <img width="100" height="70" src="<?php echo ImageProcessing::bindImageByModel($item);?>">
                            </a>
                        </p>
                    </td>
                </tr> 
                <?php endforeach;?>
                <?php endif;?>

            </tbody>
        </table>
    </div>

<script>
    $(document).ready(function(){
        jQuery('a.gallery').colorbox({ opacity:0.5 , rel:'group1' });
    });
</script>