<?php
$this->breadcrumbs=array(
	'Bán Hàng File Scan'=>array('index'),
	$model->code_no=>array('view','id'=>$model->id),
	'Cập Nhật',
);

$menus = array(	
        array('label'=>'Bán Hàng File Scan', 'url'=>array('index')),
	array('label'=>'Xem Bán Hàng File Scan', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Tạo Mới Bán Hàng File Scan', 'url'=>array('create')),	
);
$this->menu= ControllerActionsName::createMenusRoles($menus, $actions);

?>

<h1>Cập Nhật Bán Hàng File Scan: <?php echo $model->code_no; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>