<?php
$this->breadcrumbs=array(
	'Bán Hàng File Scan'=>array('index'),
	'Tạo Mới',
);

$menus = array(		
        array('label'=>'Bán Hàng File Scan', 'url'=>array('index')),
);
$this->menu= ControllerActionsName::createMenusRoles($menus, $actions);
?>

<h1>Tạo Mới Bán Hàng File Scan</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>