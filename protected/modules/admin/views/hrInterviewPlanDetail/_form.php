
<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'hr-interview-plan-detail-form',
	'enableAjaxValidation'=>false,
        'htmlOptions' => array('enctype' => 'multipart/form-data'),
)); ?>

    <!--<p class="note">Dòng có dấu <span class="required">*</span> là bắt buộc.</p>-->
    <?php echo MyFormat::BindNotifyMsg(); ?>    
    
    <div class="row" style="display: none;">
        <?php echo $form->labelEx($interview,'interview_date'); ?>
        <?php
            if ($model->isNewRecord) {
                $date = CommonProcess::getCurrentDateTime(DomainConst::DATE_FORMAT_3);
            } else {
                $date = CommonProcess::convertDateTime($interview->interview_date,
                        DomainConst::DATE_FORMAT_4,
                        DomainConst::DATE_FORMAT_3);
            }
            $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                'model'     => $interview,
                'attribute' => 'interview_date',
                'options'   => array(
                    'showAnim'      => 'fold',
                    'dateFormat'    => 'dd/mm/yy',
//                        'maxDate'       => '0',
                    'changeMonth'   => true,
                    'changeYear'    => true,
                    'showOn'        => 'button',
                    'buttonImage'   => Yii::app()->theme->baseUrl . '/admin/images/icon_calendar_r.gif',
                    'buttonImageOnly' => true,
                ),
                'htmlOptions'=>array(
                            'class'=>'w-16',
//                                'style'=>'height:20px;width:166px;',
                            'readonly'=>'readonly',
                            'value' => $date,
                        ),
            ));
            ?>
        <?php echo $form->error($interview,'interview_date'); ?>
    </div>
    <div class="row">
        <?php echo $form->labelEx($interview,'score'); ?>
        <?php echo $form->dropDownList($interview, 'score', CommonProcess::getInterviewScore()); ?>
        <?php echo $form->error($interview,'score'); ?>
    </div>
    <div class="row">
        <?php echo $form->labelEx($interview,'status'); ?>
        <?php echo $form->dropDownList($interview, 'status', HrInterviewPlan::getInterviewResultStatus()); ?>
        <?php echo $form->error($interview,'status'); ?>
    </div>
    <div class="row">
        <?php echo $form->labelEx($interview,'interview_date_next'); ?>
        <?php        
            if ($model->isNewRecord) {
                $date = CommonProcess::getCurrentDateTime(DomainConst::DATE_FORMAT_3);
            } else {
                $date = CommonProcess::convertDateTime($interview->interview_date,
                        DomainConst::DATE_FORMAT_4,
                        DomainConst::DATE_FORMAT_3);
            }
            $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                'model'     => $interview,
                'attribute' => 'interview_date_next',
                'options'   => array(
                    'showAnim'      => 'fold',
                    'dateFormat'    => 'dd/mm/yy',
//                        'maxDate'       => '0',
                    'changeMonth'   => true,
                    'changeYear'    => true,
                    'showOn'        => 'button',
                    'buttonImage'   => Yii::app()->theme->baseUrl . '/admin/images/icon_calendar_r.gif',
                    'buttonImageOnly' => true,
                ),
                'htmlOptions'=>array(
                            'class'=>'w-16',
//                                'style'=>'height:20px;width:166px;',
                            'readonly'=>'readonly',
                            'value' => $date,
                        ),
            ));
            ?>
        <?php echo $form->error($interview,'interview_date_next'); ?>
    </div>
        
    <!--    <div class="row">
            <?php // echo $form->labelEx($model,'interview_plan_id'); ?>
            <?php // echo $form->dropDownList($model,'interview_plan_id', HrInterviewPlan::loadItems()); ?>
            <?php // echo $form->error($model,'interview_plan_id'); ?>
        </div>-->
    <div class="row">
        <?php echo $form->labelEx($model,'note'); ?>
        <?php echo $form->textArea($model,'note',array('rows'=>6, 'cols'=>50)); ?>
        <?php echo $form->error($model,'note'); ?>
    </div>
<!--    <div class="row">
        <?php echo $form->labelEx($model,'created_by'); ?>
        <?php echo $form->textField($model,'created_by',array('size'=>11,'maxlength'=>11)); ?>
        <?php echo $form->error($model,'created_by'); ?>
    </div>
    <div class="row">
        <?php echo $form->labelEx($model,'created_date'); ?>
        <?php echo $form->textField($model,'created_date'); ?>
        <?php echo $form->error($model,'created_date'); ?>
    </div>-->
	<div class="row buttons" style="padding-left: 141px;">
		        <?php $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType'=>'submit',
            'label'=>$model->isNewRecord ? 'Create' :'Save',
            'type'=>'null', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
            'size'=>'small', // null, 'large', 'small' or 'mini'
            //'htmlOptions' => array('style' => 'margin-bottom: 10px; float: right;'),
        )); ?>	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->
<script>
    $(document).ready(function(){
        $('.form').find('button:submit').click(function(){
            $.blockUI({ overlayCSS: { backgroundColor: '#fff' } }); 
        });
        
    });
</script>