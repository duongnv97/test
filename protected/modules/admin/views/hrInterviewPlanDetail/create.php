<?php
$this->breadcrumbs=array(
	$this->pluralTitle=>array('index'),
	'Tạo Mới',
);

$menus = array(		
        array('label'=>"$this->singleTitle Management", 'url'=>array('index')),
);
$this->menu= ControllerActionsName::createMenusRoles($menus, $actions);
?>

<h1>Tạo Mới <?php echo $this->singleTitle; ?></h1>
<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$interview,
	'attributes'=>array(
//		'id',
		'code_no',
		array(
                   'name'=>'plan_id',
                   'value'=> isset($interview->rPlan) ? $interview->rPlan->code_no : '',
                ),
		array(
                   'name'=>'user_id',
                   'value'=> isset($interview->rUser) ? $interview->rUser->first_name : '',
                ),
		array(
                   'name'=>'candidate_id',
                   'value'=> isset($interview->rCandidate) ? $interview->rCandidate->first_name : '',
                ),
                array(
                    'label' => 'Vị trí ứng tuyển',
                    'type'=>'RoleNameUser',
                    'value' => $interview->role_id,
                    //'htmlOptions' => array('style' => 'text-align:center;')
                ),
//		'interview_date',
//		'score',
//		'interview_date_next',
//                array(
//                    'name' => 'interview_date_next',
//                    'value' => isset($model->interview_date_next) ? $model->interview_date_next: '',
//                    'visible' => isset($model->interview_date_next),
//                ),
		'note',
//		array(
//                   'name'=>'created_by',
//                   'value'=> isset($model->rCreatedBy) ? $model->rCreatedBy->first_name : '',
//                ),
//		'created_date',
//		array(
//                   'name'=>'status',
//                   'type'=>'Status',
////                    'visible' => CommonProcess::isUserAdmin(),
//                ),
	),
)); ?>
<!--<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'hr-interview-plan-form',
	'enableAjaxValidation'=>false,
        'htmlOptions' => array('enctype' => 'multipart/form-data'),
)); ?>

<?php $this->endWidget(); ?>
</div>-->
<?php echo $this->renderPartial('_form', array(
        'model'=>$model,
        'interview' => $interview,
    )); ?>