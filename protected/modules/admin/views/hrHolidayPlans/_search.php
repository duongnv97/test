
<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>
	<div class="row">
        <?php echo $form->labelEx($model,'id'); ?>
        <?php echo $form->textField($model,'id',array('class'=>'number_only_v1')); ?>
        <?php echo $form->error($model,'id'); ?>
        </div>
        <div class="row">
            <?php echo $form->labelEx($model,'approved'); ?>
            <?php echo $form->dropDownList($model,'approved', $model->getApprovePersons(),array('empty'=>'Select','class'=>'w-300')); ?>
            <?php echo $form->error($model,'approved'); ?>
        </div>
        <div class="row">
            <?php echo $form->labelEx($model,'status'); ?>
            <?php echo $form->dropDownList($model,'status', $model->getArrayStatus(),array('empty'=>'Select','class'=>'w-300')); ?>
            <?php echo $form->error($model,'status'); ?>
        </div>
	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->