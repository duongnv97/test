<div class="form">

<?php 

    
    $form=$this->beginWidget('CActiveForm', array(
            'id'=>'sell-form',
            'enableAjaxValidation'=>false,
            'htmlOptions' => array('enctype' => 'multipart/form-data'),
    )); ?>

    <p class="note">Dòng có dấu <span class="required">*</span> là bắt buộc.</p>
    <?php echo MyFormat::BindNotifyMsg(); ?>
    <div class="row">
        <?php echo $form->labelEx($model,'id'); ?>
        <?php echo $form->textField($model,'id',array('class'=>'number_only_v1')); ?>
        <?php echo $form->error($model,'id'); ?>
    </div>
    <div class="row">
        <?php echo $form->labelEx($model,'approved'); ?>
        <?php echo $form->dropDownList($model,'approved', $model->getApprovePersons(),array('empty'=>'Select','class'=>'w-300')); ?>
        <?php echo $form->error($model,'approved'); ?>
    </div>
    
    <div class="row buttons">
        <?php $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType'=>'submit',
            'label'=>$model->isNewRecord ? 'Create' :'Save',
            'type'=>'null', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
            'size'=>'small', // null, 'large', 'small' or 'mini'
            //'htmlOptions' => array('style' => 'margin-bottom: 10px; float: right;'),
        )); ?>	
    </div>

<?php $this->endWidget(); ?>
    
</div><!-- form -->
