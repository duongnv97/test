<?php
$this->breadcrumbs=array(
    $this->pluralTitle => array('index'),
    ' ' . $this->singleTitle . ' : ',
);

$menus = array(
	array('label'=>"$this->pluralTitle Management", 'url'=>array('index')),
	array('label'=>"Tạo Mới $this->singleTitle", 'url'=>array('create')),
	array('label'=>"Cập Nhật $this->singleTitle", 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>"Xóa $this->singleTitle", 'url'=>array('delete'), 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Bạn chắc chắn muốn xóa ?')),
);
$this->menu= ControllerActionsName::createMenusRoles($menus, $actions);
?>

<h1>Xem: <?php echo $this->singleTitle; ?></h1>
<?php include 'view_detail.php'; ?>
<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
            array(
                'name'=>'id',
                'type'=>'raw',
                'value'=>$model->getYear(),
            ),
            
            array(
                'name'=>'approved',
                'type'=>'raw',
                'value'=>$model->getApproved(),
            ),
            array(
                'name'=>'approved_date',
                'type'=>'raw',
                'value'=>$model->getApprovedDate(),
            ),
            array(
                'name'=>'notify',
                'type'=>'raw',
                'value'=>$model->getNotify(),
            ),
            array(
                'name'=>'status',
                'type'=>'raw',
                'value'=>$model->getStatus(),
            ),
            array(
                'name'=>'created_by',
                'type'=>'raw',
                'value'=>$model->getCreatedBy(),
            ),
            array(
                'name'=>'created_date',
                'type'=>'raw',
                'value'=>$model->getCreatedDate(),
            ),
	),
)); ?>
