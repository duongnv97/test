<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/jquery.colorbox-min.js"></script>
<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/admin/css/colorbox.css"/>
<script src="<?php echo Yii::app()->theme->baseUrl; ?>/js/huongminh_socket.js"></script>
<input type="hidden" class="TodayText" value="<?php echo date('d-m-Y');?>">
<input type="hidden" class="UrlBoMoiSubmit" value="<?php echo Yii::app()->createAbsoluteUrl('admin/ajaxForUpdate/makeOrderBoMoi');?>">

<?php 
$cRole      = MyFormat::getCurrentRoleId();
$customer_id = $agent_id = $user_id_executive = 0;
$b50 = $b45 = $b12 = $b6 = 0; 
$note = $agent_name = $customer_name = $customer_address = '';
if($mCustomer){
    $customer_id    = $mCustomer->id;
    $customer_name  = $mCustomer->code_bussiness. ' - '. $mCustomer->first_name;
}

?>
<?php echo MyFormat::BindNotifyMsg(); ?>

<div class="w-600">
    <?php if(in_array($cRole, GasSocketNotify::getRoleViewCallCenter())): ?>
        <?php include "index_callcenter.php"; ?>
    <?php endif; ?>
</div>

<style>
#cboxClose { position: relative !important;} 
</style>
<script>
$(document).ready(function() {
//    bindChangeAgent();
    WrapOneTab = $('.BoxSell');
    setInfoCustomer(WrapOneTab, <?php echo $customer_id;?>, '<?php echo $customer_name;?>', '<?php echo $phone_number;?>', '<?php echo $mCustomer->address;?>');
});
</script>

<!-- May 21, 2017 for Bo Moi action-->
<script>
$(document).ready(function() {
    fnBindAllAutocompleteAgent();
    bindEventBoMoi();
    fnOrderBoMoiType();
    fnOrderBoMoiSubmit();
    $('.OrderBoMoiCustomerView').click(function(){
        $('.OrderBoMoiCustomerInfo').toggle();
    });
});


function fnBindAllAutocompleteAgent(){
    var urlSource = '<?php echo Yii::app()->createAbsoluteUrl('admin/ajax/search_by_role', array('role'=> ROLE_AGENT));?>';
    $('.OrderBoMoiAgentName').each(function(){
        fnBindAutocompleteAgent($(this), urlSource);
    });
}
</script>

<!-- May 21, 2017 for Bo Moi action-->