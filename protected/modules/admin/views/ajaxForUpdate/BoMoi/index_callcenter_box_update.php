<?php 
    $aStatusFailed = Call::model()->getArrayFailedStatus();
    unset($aStatusFailed[GasConst::SELL_COMPLETE]);
?>
<a href="javascript:;" class="call-focus item_b ControlCallBoxUpdateFailed">Cuộc gọi không có bán hàng</a>
<div class="CallBoxUpdateFailed display_none">
    <div class="call-grp-client">
        <div class="call-info-client">
            <?php echo CHtml::dropDownList('CallFailedStatus', "",
              $aStatusFailed, array('class'=>' call-ipt-slc CallFailedStatus','empty'=>"Select"));?>
        </div>
     </div>
     <div class="call-grp-client">
        <div class="call-info-client">
            <textarea class="call-txt-area CallFailedNote" name="CallFailedNote" placeholder="Ghi chú"></textarea>
        </div>
     </div>
      <div class="call-grp-client">
            <label class="call-lbl2"></label>
        <div class="call-info-client">
            <button class="call-btn-update CallFailedBtnUpdate" next="<?php echo Yii::app()->createAbsoluteUrl('admin/ajaxForUpdate/callFailedUpdate');?>">Lưu</button>
        </div>
     </div>
</div>