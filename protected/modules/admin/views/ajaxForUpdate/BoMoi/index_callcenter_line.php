<?php 
    $cRole = MyFormat::getCurrentRoleId();
    $showFunctionHgd = '';
    $linkCreateSell     = Yii::app()->createAbsoluteUrl('admin/sell/create');
    $linkCreateUphold   = Yii::app()->createAbsoluteUrl('admin/upholdHgd/create');
    $ajaxSearchCustomer = Yii::app()->createAbsoluteUrl('admin/ajax/searchHgd');
    if($cRole == ROLE_DIEU_PHOI) {
        $ajaxSearchCustomer = Yii::app()->createAbsoluteUrl('admin/ajax/search_user_by_code');
        $showFunctionHgd = 'display_none';
    }
    $i = 1;
?>

<div class="call-group-call clearfix WrapOneTab " id="tabs-<?php echo $i;?>" tab_index="<?php echo $i-1;?>">
    <div class="call-form-call">
        <input class="CallGetCustomerByPhoneUrl" value="<?php echo Yii::app()->createAbsoluteUrl('admin/ajaxForUpdate/callGetCustomerByPhone');?>" type="hidden">
        <input class="CallGetCustomerHistoryUrl" value="<?php echo Yii::app()->createAbsoluteUrl('admin/ajaxForUpdate/callGetCustomerHistory');?>" type="hidden">
        <input class="CallUpdateCustomerIdUrl" value="<?php echo Yii::app()->createAbsoluteUrl('admin/ajaxForUpdate/callUpdateCustomerId');?>" type="hidden">
        <input class="CallCurrentTab CallCurrentTab<?php echo $i-1;?>" type="hidden" name="CallCurrentTab" value="<?php echo $i-1;?>">
        <input class="CallCurrentPhone" type="hidden" name="CallCurrentPhone" value="<?php echo $phone_number;?>">
        <input class="CallStatusLine" value="0" type="hidden">
        <input class="call_uuid" value="0" type="hidden">
        <input class="parent_call_uuid" value="0" type="hidden">
        <input class="child_call_uuid" value="0" type="hidden">
        <input class="customer_id" value="<?php echo $mCustomer->id;?>" type="hidden">

        <div class="grp-bar" style="margin-bottom: 0;">
            <div class="right-from">
                <div class="call-tab-bar-fone CallPhoneNumber"></div>
            </div>
        </div>
        <div class="grp-bar">
            <?php include 'call_bomoi_action.php'; ?>
        </div>
        
        <div class="call-form-control">
            <h3 class="call-name-client CustomerInfoName"></h3>
            <div class="call-grp-client">
               <div class="call-info-client ">
                   <span class="CustomerInfoPhone"></span><br>
                   <span class="CustomerInfoAddress"></span>
               </div>
            </div>
            <?php include 'index_callcenter_line_info_bomoi.php'; ?>
            <?php // include 'index_callcenter_box_update.php'; ?>
        </div>
    </div>

    <div class="BoxCustomerHistory">
        <div class="call-t-header display_none">Lịch sử lấy hàng</div>
        <table class="call-tbl-list display_none">
            <tr>
                <th>Ngày</th>
                <th>Chi tiết</th>
            </tr>
        </table>
    </div>
    
</div>