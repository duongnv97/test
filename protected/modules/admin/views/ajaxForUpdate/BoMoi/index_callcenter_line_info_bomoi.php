<p><a href="javascript:;" class="call-focus item_b OrderBoMoiCustomerView">Xem thông tin khách hàng</a></p>
<div class="OrderBoMoiCustomerInfo display_none1">
    <table>
        <tr>
            <td class="w-130 item_b">Đại lý</td>
            <td class="float_l customer_agent_name"></td>
        </tr>
        <tr>
            <td class=" item_b">Đại lý gần nhất</td>
            <td class="float_l customer_agent_name_delivery"></td>
        </tr>
        <tr>
            <td class=" item_b">Người liên hệ</td>
            <td class="float_l customer_contact"></td>
        </tr>
        <tr>
            <td class=" item_b">Loại KH</td>
            <td class="float_l customer_type"></td>
        </tr>
        <tr>
            <td class="item_b">Ghi chú</td>
            <td class="float_l customer_note"></td>
        </tr>
        <tr>
            <td class=" item_b">Sale</td>
            <td class="float_l customer_sale"></td>
        </tr>
    </table>
</div>