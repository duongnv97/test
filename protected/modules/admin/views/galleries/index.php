<?php
$this->breadcrumbs=array(
	'Galleries',
);

$menus=array(
	array('label'=> Yii::t('translation','Create Gallery'), 'url'=>array('create')),
);
$this->menu= ControllerActionsName::createMenusRoles($menus, $actions);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('galleries-grid', {
                url : $(this).attr('action'),
		data: $(this).serialize()
	});
	return false;
});
");

Yii::app()->clientScript->registerScript('ajaxupdate', "
$('#galleries-grid a.ajaxupdate').live('click', function() {
    $.fn.yiiGridView.update('galleries-grid', {
        type: 'POST',
        url: $(this).attr('href'),
        success: function() {
            $.fn.yiiGridView.update('galleries-grid');
        }
    });
    return false;
});
");
?>

<h1><?php echo Yii::t('translation', 'Gallery Management'); ?></h1>

<?php echo CHtml::link(Yii::t('translation','Advanced Search'),'#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'galleries-grid',
	'dataProvider'=>$model->search(),
	'enableSorting' => false,
	//'filter'=>$model,
	'columns'=>array(
        array(
            'header' => 'S/N',
            'type' => 'raw',
            'value' => '$this->grid->dataProvider->pagination->currentPage * $this->grid->dataProvider->pagination->pageSize + ($row+1)',
            'headerHtmlOptions' => array('width' => '30px','style' => 'text-align:center;'),
            'htmlOptions' => array('style' => 'text-align:center;')
        ),
        Yii::t('translation','title'),
        array(
            'name'=>'thumb_image',
            'type' => 'html',
            'htmlOptions' => array('style' => 'text-align:center;'),
            'value'=> '!empty($data->thumb_image) ? CHtml::image(Yii::app()->baseUrl."/upload/admin/galleries/thumbs/".$data->thumb_image, "gallery", array()):"";'
        ),
        array(
            'header' => 'URL',
            'type' => 'html',
            'value' => 'CHtml::link(
                                        Yii::app()->createAbsoluteUrl("/galleries/view/slug/".$data->slug),
                                        Yii::app()->createAbsoluteUrl("/galleries/view/slug/".$data->slug),
                                        array("target"=>"_blank")
                                    )',
        ),
        'code',
        array(
            'name'=>'status',
            'type'=>'status',
            'htmlOptions' => array('style' => 'text-align:center;'),
            'value'=>'array("status"=>$data->status,"id"=>$data->id)',
        ),
        array(
            'name'=>'created_date',
            'type'=>'datetime',
            'htmlOptions' => array('style' => 'text-align:center;'),
            'value'=>'$data->created_date',
        ),
        array(
            'header' => 'View Uploaded Pictures',
            'class'=>'CButtonColumn',
            'template'=>'{viewPhoto}',
            'buttons'=>array(
                'viewPhoto'=>array(
                    'label'=>'View uploaded pictures of gallery',
                    'imageUrl'=>Yii::app()->theme->baseUrl . '/admin/images/view_gallery.png',
                    'url'=>'Yii::app()->createAbsoluteUrl("admin/photos/",array("gallery_id"=>$data->id))',
                ),
            ),
        ),
		array(
			'header' => 'Action',
			'class'=>'CButtonColumn',
            'template'=> ControllerActionsName::createIndexButtonRoles($actions),
		),
	),
)); ?>
