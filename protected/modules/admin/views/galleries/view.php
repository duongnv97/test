<?php
$this->breadcrumbs=array(
	'Galleries'=>array('index'),
	$model->title,
);

$menus = array(
	array('label'=>'Galleries Management', 'url'=>array('index')),
	array('label'=>'Create Galleries', 'url'=>array('create')),
	array('label'=>'Update Galleries', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete Galleries', 'url'=>array('delete'), 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
);
$this->menu= ControllerActionsName::createMenusRoles($menus, $actions);
?>

<h1>View Gallery <?php echo $model->title; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'title',
		'header_text:html',
		'footer_text:html',
        array(
            'name'=>'thumb_image',
            'type' => 'html',
            'htmlOptions' => array('style' => 'text-align:center;'),
            'value'=> !empty($model->thumb_image) ? CHtml::image(Yii::app()->baseUrl."/upload/admin/galleries/thumbs/".$model->thumb_image, "gallery", array()):"",
        ),
        array(
            'name'=>'status',
            'type'=>'raw',
            'value'=>$model->status == 0?"Inactive":"Active",
        ),
        array(
            'name' => 'URL',
            'type' => 'html',
            'value' => CHtml::link(
                Yii::app()->createAbsoluteUrl("galleries/view/slug/".$model->slug),
                Yii::app()->createUrl("galleries/view/slug/".$model->slug),
                array("target"=>"_blank")
            ),
        ),
		'title_tag',
		'meta_tag',
		'meta_keyword',
		'code',
		'created_date:datetime',
		'updated_date:datetime',
	),
)); ?>
