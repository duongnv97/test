<?php
$this->breadcrumbs=array(
	Yii::t('translation','Galleries')=>array('index'),
	Yii::t('translation','Create'),
);

$menus = array(		
        array('label'=> Yii::t('translation', 'Galleries Management') , 'url'=>array('index')),
);
$this->menu= ControllerActionsName::createMenusRoles($menus, $actions);
?>

<h1><?php echo Yii::t('translation', 'Create Galleries'); ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>