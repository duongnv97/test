<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'galleries-form',
	'enableAjaxValidation'=>false,
    'htmlOptions' => array('class'=>'', 'enctype' => 'multipart/form-data'),
)); ?>

	<?php echo Yii::t('translation', '<p class="note">Fields with <span class="required">*</span> are required.</p>'); ?>

    <?php echo $form->errorSummary($model); ?>
	
	<div class="row">
		<?php echo Yii::t('translation', $form->labelEx($model,'title')); ?>
		<?php echo $form->textField($model,'title',array('size'=>60,'maxlength'=>250)); ?>
		<?php echo $form->error($model,'title'); ?>
	</div>

    <?php if(Yii::app()->controller->action->id == 'update'){?>
    <div class="row">
		<?php echo Yii::t('translation', $form->labelEx($model,'slug')); ?>
		<?php echo $form->textField($model,'slug',array('size'=>60,'maxlength'=>250)); ?>
		<?php echo $form->error($model,'slug'); ?>
	</div>
    <?php } ?>

    <div class="row">
        <?php echo Yii::t('translation', $form->labelEx($model,'thumb_image')); ?>
        <?php echo $form->fileField($model,'thumb_image',array('id'=>'thumb_image')); ?>
        <?php echo $form->error($model,'thumb_image'); ?>
    </div>
    <?php if(Yii::app()->controller->action->id == 'update'){?>
    <div class="row">
        <label>&nbsp;</label>
        <div>
            <?php echo CHtml::image(Yii::app()->baseUrl."/upload/admin/galleries/thumbs/".$model->thumb_image, "gallery", array());?>
        </div>
    </div>
    <?php } ?>

	<div class="row">
		<?php echo Yii::t('translation', $form->labelEx($model,'header_text')); ?>
        <div style="float:left;">
            <?php
            $this->widget('ext.ckeditor.CKEditorWidget',array(
                "model"=>$model,
                "attribute"=>'header_text',
                "config" => array(
                    "height"=>"150px",
                    "width"=>"500px",
                    "toolbar"=>Yii::app()->params['ckeditor_simple']
                )
            ));
            ?>

        </div>
		<?php echo $form->error($model,'header_text'); ?>
	</div>
    <div class='clr'></div>

	<div class="row">
		<?php echo Yii::t('translation', $form->labelEx($model,'footer_text')); ?>
        <div style="float:left;">
            <?php
            $this->widget('ext.ckeditor.CKEditorWidget',array(
                "model"=>$model,
                "attribute"=>'footer_text',
                "config" => array(
                    "height"=>"150px",
                    "width"=>"500px",
                    "toolbar"=>Yii::app()->params['ckeditor_simple']
                )
            ));
            ?>
        </div>
		<?php echo $form->error($model,'footer_text'); ?>
	</div>
    <div class='clr'></div>

    <fieldset>
        <legend><b>SEO Fields</b></legend>
        <div class="row">
            <?php echo $form->labelEx($model,'title_tag'); ?>
            <?php echo $form->textField($model,'title_tag',array('size'=>74,'maxlength'=>250)); ?>
            <?php echo $form->error($model,'title_tag'); ?>
        </div>
        <div class="clr"></div>
        <div class="row">
            <?php echo $form->labelEx($model,'meta_keyword'); ?>
            <?php echo $form->textArea($model,'meta_keyword',array('rows'=>5, 'cols'=>57)); ?>
            <?php echo $form->error($model,'meta_keyword'); ?>
        </div>
        <div class="clr"></div>
        <div class="row">
            <?php echo $form->labelEx($model,'meta_tag'); ?>
            <?php echo $form->textArea($model,'meta_tag',array('rows'=>5, 'cols'=>57)); ?>
            <?php echo $form->error($model,'meta_tag'); ?>
        </div>
    </fieldset>

	<div class="row buttons" style="padding-left: 115px;">
		        <?php $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType'=>'submit',
            'label'=>$model->isNewRecord ? Yii::t('translation', 'Create') : Yii::t('translation', 'Save'),
            'type'=>'null', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
            'size'=>'small', // null, 'large', 'small' or 'mini'
            //'htmlOptions' => array('style' => 'margin-bottom: 10px; float: right;'),
        )); ?>	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->
<?php
/*
 <script type="text/javaScript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/jquery.stringToSlug.js"></script>
<script type="text/javaScript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/jquery.stringToSlug.min.js"></script>

<script type="text/javascript">

    jQuery(document).ready(function(){
        validateNumber();
        $("#Galleries_title").stringToSlug({
            setEvents: 'keyup keydown blur',
            getPut: '#Galleries_slug',
            space: '-'
        });
    });

    function validateNumber(){
        $(".number").each(function(){
            $(this).unbind("keydown");
            $(this).bind("keydown",function(event){
                if( !(event.keyCode == 8                                // backspace
                    || event.keyCode == 46                              // delete
                    || event.keyCode == 9							// tab
                    || event.keyCode == 190							// dáº¥u cháº¥m (point)
                    || (event.keyCode >= 35 && event.keyCode <= 40)     // arrow keys/home/end
                    || (event.keyCode >= 48 && event.keyCode <= 57)     // numbers on keyboard
                    || (event.keyCode >= 96 && event.keyCode <= 105))   // number on keypad
                    ) {
                    event.preventDefault();     // Prevent character input
                }
            });
        });
    }

</script>
 */
?>
