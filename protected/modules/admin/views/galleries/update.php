<?php
$this->breadcrumbs=array(
	Yii::t('translation','Galleries')=>array('index'),
	$model->title=>array('view','id'=>$model->id),
	Yii::t('translation','Update'),
);

$menus = array(	
        array('label'=> Yii::t('translation', 'Galleries Management'), 'url'=>array('index')),
	array('label'=> Yii::t('translation', 'View Galleries'), 'url'=>array('view', 'id'=>$model->id)),
	array('label'=> Yii::t('translation', 'Create Galleries'), 'url'=>array('create')),	
);
$this->menu= ControllerActionsName::createMenusRoles($menus, $actions);

?>

<h1><?php echo Yii::t('translation', 'Update Galleries '.$model->id); ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>