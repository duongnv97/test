<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'title',array('label' => Yii::t('translation','Title'))); ?>
		<?php echo $form->textField($model,'title',array('size'=>60,'maxlength'=>250)); ?>
	</div>

    <div class="row">
        <?php echo $form->label($model,'code',array('label' => Yii::t('translation','Code'))); ?>
        <?php echo $form->textField($model,'code',array('size'=>10,'maxlength'=>10)); ?>
    </div>

	<div class="row">
		<?php echo $form->label($model,'header_text',array('label' => Yii::t('translation','Header text'))); ?>
		<?php echo $form->textArea($model,'header_text',array('rows'=>6, 'cols'=>50)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'footer_text',array('label' => Yii::t('translation','Footer text'))); ?>
		<?php echo $form->textArea($model,'footer_text',array('rows'=>6, 'cols'=>50)); ?>
	</div>

	<div class="row buttons" style="padding-left: 159px;">
        <?php $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType'=>'submit',
            'label'=>Yii::t('translation','Search'),
            'type'=>'null', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
            'size'=>'small', // null, 'large', 'small' or 'mini'
            //'htmlOptions' => array('style' => 'margin-bottom: 10px; float: right;'),
        )); ?>
    </div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->