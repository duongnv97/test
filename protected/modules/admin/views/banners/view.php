<?php
$this->breadcrumbs=array(
	 Yii::t('translation','Banner Management')=>array('index'),
	 Yii::t('translation','View Banner'),
//  	$model->banner_title,
);

$menus = array(
	array('label'=> Yii::t('translation','Banner Management'), 'url'=>array('index')),
	array('label'=> Yii::t('translation','Create Banner'), 'url'=>array('create')),
	array('label'=> Yii::t('translation','Update Banner'), 'url'=>array('update', 'id'=>$model->id)),
	array('label'=> Yii::t('translation','Delete Banner'), 'url'=>'delete', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=> Yii::t('translation','Are you sure you want to delete this item?'))),
);
$this->menu= ControllerActionsName::createMenusRoles($menus, $actions);
?>

<h1>View Banner [<?php echo $model->banner_title; ?>]</h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
//		'id',
		 Yii::t('translation','banner_title'),
		array(
				'name'=> Yii::t('translation','banner_description'),
				'type' => 'html',
			),
//		'thumb_image',
		 Yii::t('translation','large_image'),
//		'link',
        array(
            'name'=> Yii::t('translation','place_holder_id'),
            'type' => 'raw',
            'htmlOptions' => array('style' => 'text-align:center;'),
            'value'=> $model->placeholder->position
        ),
	),
)); ?>

<div class="column" style="width: 98%; padding:15px 0 15px 0;">
    <img src="<?php echo Yii::app()->createAbsoluteUrl('/upload/banner/'.$model->large_image);?>"  style="max-width:800px;" height="300"/>
</div>
