<?php
$this->breadcrumbs=array(
	'Banner Management',
);

$menus = array(
	array('label'=>'Create Banner', 'url'=>array('create')),
);
$this->menu= ControllerActionsName::createMenusRoles($menus, $actions);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('banners-grid', {
                url : $(this).attr('action'),
		data: $(this).serialize()
	});
	return false;
});
");

Yii::app()->clientScript->registerScript('ajaxupdate', "
$('#banners-grid a.ajaxupdate').live('click', function() {
    $.fn.yiiGridView.update('banners-grid', {
        type: 'POST',
        url: $(this).attr('href'),
        success: function() {
            $.fn.yiiGridView.update('banners-grid');
        }
    });
    return false;
});
");
?>

<h1><?php echo Yii::t('translation','Banner Management')?></h1>

<?php echo CHtml::link( Yii::t('translation','Advanced Search'),'#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'banners-grid',
	'dataProvider'=>$model->search(),
	//'filter'=>$model,
	'columns'=>array(
        array(
            'header' => 'S/N',
            'type' => 'raw',
            'value' => '$this->grid->dataProvider->pagination->currentPage * $this->grid->dataProvider->pagination->pageSize + ($row+1)',
            'headerHtmlOptions' => array('width' => '30px','style' => 'text-align:center;'),
            'htmlOptions' => array('style' => 'text-align:center;')
        ),
        array(
            'name'=>'large_image',
            'type' => 'html',
            'htmlOptions' => array('style' => 'text-align:center;'),
            'value'=> 'file_exists(Yii::getPathOfAlias("webroot")."/upload/banner/".$data->large_image) ? CHtml::image(Yii::app()->baseUrl."/upload/banner/".$data->large_image, "image", array("width"=>"230","height"=>"69")):"";'
        ),
		'banner_title',
		array(
				'name' =>'banner_description',
				'type' =>'html' ,
			),	
//		'thumb_image',

//		'link',
//		'place_holder_id',
        array(
            'name'=>'place_holder_id',
            'type' => 'raw',
            'htmlOptions' => array('style' => 'text-align:center;'),
            'value'=> '$data->placeholder->position'
        ),
		array(
			'class'=>'CButtonColumn',
                        'template'=> ControllerActionsName::createIndexButtonRoles($actions),
		),
	),
)); ?>
