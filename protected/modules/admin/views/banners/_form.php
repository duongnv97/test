<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'banners-form',
	'enableAjaxValidation'=>false,
    'method'=>'post',
    'htmlOptions' => array('enctype' => 'multipart/form-data'),
)); ?>

	<p class="note"><?php echo Yii::t('translation','Fields with <span class="required">*</span> are required.'); ?></p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo Yii::t('translation',$form->labelEx($model,'banner_title')); ?>
        <?php echo $form->textField($model,'banner_title',array('size'=>70,'maxlength'=>250)); ?>
		<?php echo Yii::t('translation',$form->error($model,'banner_title')); ?>
	</div>

    <div class="row">
        <?php echo Yii::t('translation',$form->labelEx($model,'banner_description')); ?>
        <div style="float:left;">
            <?php
            $this->widget('ext.ckeditor.CKEditorWidget',array(
                "model"=>$model,
                "attribute"=>'banner_description',
                "config" => array(
                    "height"=>"150px",
                    "width"=>"500px",
                    "toolbar"=>Yii::app()->params['ckeditor_simple']
                )
            ));
            ?>
        </div>
        <?php echo Yii::t('translation',$form->error($model,'banner_description')); ?>
    </div>
    <div class='clr'></div>	

	<div class="row">
		 <?php echo Yii::t('translation',$form->labelEx($model,'imageFile')); ?>
        <?php echo $form->fileField($model, 'imageFile'); ?>
        <?php echo Yii::t('translation',$form->error($model,'imageFile')); ?>
	</div>

    <div class="row">
        <?php echo Yii::t('translation',$form->labelEx($model,'place_holder_id')); ?>
        <?php echo $form->dropDownList($model,'place_holder_id',PlaceHolders::loadItems()); ?>
        <?php echo Yii::t('translation',$form->error($model,'place_holder_id')); ?>
    </div>

	<div class="row buttons">
		<span class="btn-submit"><?php echo CHtml::submitButton($model->isNewRecord ? Yii::t('translation','Create') : Yii::t('translation','Save') ); ?></span>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->