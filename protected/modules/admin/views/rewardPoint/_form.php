<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'reward-point-form',
	'enableAjaxValidation'=>false,
        'htmlOptions' => array('enctype' => 'multipart/form-data'),
)); ?>

    <p class="note">Dòng có dấu <span class="required">*</span> là bắt buộc.</p>
    <?php echo MyFormat::BindNotifyMsg(); ?>
    
        		
        
    <div class="row">
        <?php echo $form->labelEx($model,'customer_id'); ?>
        <?php echo $form->textField($model,'customer_id',array('size'=>10,'maxlength'=>10)); ?>
        <?php echo $form->error($model,'customer_id'); ?>
    </div>
    <div class="row">
        <?php echo $form->labelEx($model,'default_point'); ?>
        <?php echo $form->textField($model,'default_point',array('size'=>10,'maxlength'=>10)); ?>
        <?php echo $form->error($model,'default_point'); ?>
    </div>
    <div class="row">
        <?php echo $form->labelEx($model,'reward_setup_id'); ?>
        <?php echo $form->textField($model,'reward_setup_id',array('size'=>10,'maxlength'=>10)); ?>
        <?php echo $form->error($model,'reward_setup_id'); ?>
    </div>
    <div class="row">
        <?php echo $form->labelEx($model,'bonus_point'); ?>
        <?php echo $form->textField($model,'bonus_point',array('size'=>10,'maxlength'=>10)); ?>
        <?php echo $form->error($model,'bonus_point'); ?>
    </div>
    <div class="row">
        <?php echo $form->labelEx($model,'total_point'); ?>
        <?php echo $form->textField($model,'total_point',array('size'=>10,'maxlength'=>10)); ?>
        <?php echo $form->error($model,'total_point'); ?>
    </div>
    <div class="row">
        <?php echo $form->labelEx($model,'created_date'); ?>
        <?php echo $form->textField($model,'created_date'); ?>
        <?php echo $form->error($model,'created_date'); ?>
    </div>
	<div class="row buttons" style="padding-left: 141px;">
		        <?php $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType'=>'submit',
            'label'=>$model->isNewRecord ? 'Create' :'Save',
            'type'=>'null', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
            'size'=>'small', // null, 'large', 'small' or 'mini'
            //'htmlOptions' => array('style' => 'margin-bottom: 10px; float: right;'),
        )); ?>	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->
<script>
    $(document).ready(function(){
        $('.form').find('button:submit').click(function(){
            $.blockUI({ overlayCSS: { backgroundColor: '#fff' } }); 
        });
        
    });
</script>