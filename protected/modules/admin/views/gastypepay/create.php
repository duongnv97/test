<?php
$this->breadcrumbs=array(
	Yii::t('translation','Quy định ngày thanh toán')=>array('index'),
	Yii::t('translation','Create'),
);

$menus = array(		
        array('label'=> Yii::t('translation', 'GasTypePay Management') , 'url'=>array('index')),
);
$this->menu= ControllerActionsName::createMenusRoles($menus, $actions);
?>

<h1><?php echo Yii::t('translation', 'Create ngày thanh toán:'); ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>