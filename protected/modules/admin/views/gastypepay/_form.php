<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'gas-type-pay-form',
	'enableAjaxValidation'=>false,
)); ?>

	<?php echo Yii::t('translation', '<p class="note">Fields with <span class="required">*</span> are required.</p>'); ?>

	<div class="row">
		<?php echo Yii::t('translation', $form->labelEx($model,'name')); ?>
		<?php // echo $form->textField($model,'name',array('style'=>'width:550px;')); ?>
                <?php echo $form->textArea($model,'name',array('style'=>'width:700px;','rows'=>3, 'cols'=>50)); ?>
		<?php echo $form->error($model,'name'); ?>
	</div>    
    
	<div class="row">
		<?php echo Yii::t('translation', $form->labelEx($model,'type')); ?>
		<?php echo $form->dropDownList($model,'type', GasTypeMaster::getArrAll(),array('style'=>'width:550px;')); ?>
		<?php echo $form->error($model,'type'); ?>
	</div>
        <div class="row">
            <label>&nbsp;</label>
            <div style="float: left;">
                <?php echo Yii::app()->params['note_type_pay']; ?>
                </div>
        </div>
    <div class="clr"></div>
	<div class="row">
		<?php echo Yii::t('translation', $form->labelEx($model,'pay_day')); ?>
            
		<?php echo $form->textArea($model,'pay_day',array('style'=>'width:700px;','rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'pay_day'); ?>
	</div>

	<div class="row buttons" style="padding-left: 115px;">
		        <?php $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType'=>'submit',
            'label'=>$model->isNewRecord ? Yii::t('translation', 'Create') : Yii::t('translation', 'Save'),
            'type'=>'null', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
            'size'=>'small', // null, 'large', 'small' or 'mini'
            //'htmlOptions' => array('style' => 'margin-bottom: 10px; float: right;'),
        )); ?>	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->