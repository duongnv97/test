<?php
/* echo '<pre>';
echo print_r(json_decode($model->json_type_3, true));
echo '</pre>'; */
$this->breadcrumbs=array(
	'Quy định ngày thanh toán'=>array('index'),
	$model->name,
);

$menus = array(
	array('label'=>'GasTypePay Management', 'url'=>array('index')),
	array('label'=>'Create GasTypePay', 'url'=>array('create')),
	array('label'=>'Update GasTypePay', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete GasTypePay', 'url'=>array('delete'), 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
);
$this->menu= ControllerActionsName::createMenusRoles($menus, $actions);
?>

<h1>View ngày thanh toán: <?php echo $model->name; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'name',
            array(
                'name'=>'type',
                'value'=>$model->TypeMaster?$model->TypeMaster->name:'',
            ),
		'pay_day',

	),
)); ?>
