<?php
$this->breadcrumbs=array(
	'GasTypeMaster',
);

$menus=array(
	array('label'=> Yii::t('translation','Create GasTypeMaster'), 'url'=>array('create')),
);
$this->menu= ControllerActionsName::createMenusRoles($menus, $actions);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('gas-type-master-grid', {
                url : $(this).attr('action'),
		data: $(this).serialize()
	});
	return false;
});
");

Yii::app()->clientScript->registerScript('ajaxupdate', "
$('#gas-type-master-grid a.ajaxupdate').live('click', function() {
    $.fn.yiiGridView.update('gas-type-master-grid', {
        type: 'POST',
        url: $(this).attr('href'),
        success: function() {
            $.fn.yiiGridView.update('gas-type-master-grid');
        }
    });
    return false;
});
");
?>

<h1><?php echo Yii::t('translation', 'Gas Type Masters Management'); ?></h1>

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'gas-type-master-grid',
	'dataProvider'=>$model->search(),
	'enableSorting' => false,
	//'filter'=>$model,
	'columns'=>array(
//        array(
//            'header' => 'S/N',
//            'type' => 'raw',
//            'value' => '$this->grid->dataProvider->pagination->currentPage * $this->grid->dataProvider->pagination->pageSize + ($row+1)',
//            'headerHtmlOptions' => array('width' => '30px','style' => 'text-align:center;'),
//            'htmlOptions' => array('style' => 'text-align:center;')
//        ),
	'id',
            Yii::t('translation','name'),
		 Yii::t('translation','short_name'),
		 Yii::t('translation','note'),
		array(
			'header' => 'Action',
			'class'=>'CButtonColumn',
                        'template'=> ControllerActionsName::createIndexButtonRoles($actions),
		),
	),
)); ?>
