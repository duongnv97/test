<?php
$this->breadcrumbs=array(
	Yii::t('translation','Gas Type Masters')=>array('index'),
	Yii::t('translation','Create'),
);

$menus = array(		
        array('label'=> Yii::t('translation', 'GasTypeMaster Management') , 'url'=>array('index')),
);
$this->menu= ControllerActionsName::createMenusRoles($menus, $actions);
?>

<h1><?php echo Yii::t('translation', 'Create GasTypeMaster'); ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>