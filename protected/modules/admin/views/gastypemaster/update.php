<?php
$this->breadcrumbs=array(
	Yii::t('translation','Gas Type Masters')=>array('index'),
	$model->name=>array('view','id'=>$model->id),
	Yii::t('translation','Update'),
);

$menus = array(	
        array('label'=> Yii::t('translation', 'GasTypeMaster Management'), 'url'=>array('index')),
	array('label'=> Yii::t('translation', 'View GasTypeMaster'), 'url'=>array('view', 'id'=>$model->id)),
	array('label'=> Yii::t('translation', 'Create GasTypeMaster'), 'url'=>array('create')),	
);
$this->menu= ControllerActionsName::createMenusRoles($menus, $actions);

?>

<h1><?php echo Yii::t('translation', 'Update GasTypeMaster '.$model->id); ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>