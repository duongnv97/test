<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=> GasCheck::getCurl(),
	'method'=>'get',
)); ?>
        
        <div class="row more_col">
            <div class="col1">
            </div>
            <div class="col2">
                <?php echo $form->label($model,'phone',array()); ?>
                <?php echo $form->textField($model,'phone',array('maxlength'=>15,'style'=>'width: 192px!important;')); ?>
            </div>
            <div class="col3">
                <?php echo $form->labelEx($model,'type'); ?>
                <?php  echo $form->dropDownList($model,'type', $model->getArrayType(), ['empty'=>'Select','class'=>'w-200']); ?>
            </div>
        </div>
        <?php if(isset($_GET['type'])){ ?>
        <div class="row more_col">
            <div class="col1">
                <?php echo $form->labelEx($model, 'date_call_from', []); ?>
                 <?php 
                    $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                        'model'=>$model,        
                        'attribute'=>'date_call_from',
                        'options'=>array(
                            'showAnim'=>'fold',
                            'dateFormat'=> MyFormat::$dateFormatSearch,
                    //                            'minDate'=> '0',
                    //                            'maxDate'=> '0',
                            'changeMonth' => true,
                            'changeYear' => true,
                            'showOn' => 'button',
                            'buttonImage'=> Yii::app()->theme->baseUrl.'/admin/images/icon_calendar_r.gif',
                            'buttonImageOnly'=> true,                                
                        ),        
                        'htmlOptions'=>array(
                            'class'=>'w-16',
                            'style'=>'height:20px;',
    //                            'readonly'=>'readonly',
                        ),
                    ));
                    ?>  
            </div>
            <div class="col2">
                <?php echo $form->labelEx($model, 'date_call_to', []); ?>
                 <?php 
                    $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                        'model'=>$model,        
                        'attribute'=>'date_call_to',
                        'options'=>array(
                            'showAnim'=>'fold',
                            'dateFormat'=> MyFormat::$dateFormatSearch,
                    //                            'minDate'=> '0',
                    //                            'maxDate'=> '0',
                            'changeMonth' => true,
                            'changeYear' => true,
                            'showOn' => 'button',
                            'buttonImage'=> Yii::app()->theme->baseUrl.'/admin/images/icon_calendar_r.gif',
                            'buttonImageOnly'=> true,                                
                        ),        
                        'htmlOptions'=>array(
                            'class'=>'w-16',
                            'style'=>'height:20px;',
    //                            'readonly'=>'readonly',
                        ),
                    ));
                    ?>  
            </div>
        </div>
        <?php } ?>
        
        <div class="row more_col">
            <div class="col1">
                <?php  echo $form->labelEx($model,'employee_id', ['label'=>'Nhân viên']); ?>
                <?php  echo $form->dropDownList($model,'employee_id', $model->getArrayEmployee(), ['empty'=>'Select','class'=>'w-200']); ?>
            </div>
            <div class="col2">
                <?php echo $form->labelEx($model,'status',['label'=>'Trạng thái']); ?>
                <?php echo $form->dropdownList($model,'status', $model->getArrayCallStatus(true), array('empty'=>'Select','class'=>'w-200')); 
                ?>
            </div>
            <div class="col3 display_none">
                <?php echo $form->labelEx($model,'Trạng thái đơn hàng'); ?>
                <?php echo $form->dropDownList($model,'is_buy_complete', $model->getArrayStatusBuy(),array('empty'=>'Select','class'=>'w-200')); ?>
            </div>
            <div class="col3">
                <?php echo $form->labelEx($model,'Số dòng hiển thị'); ?>
                <?php echo $form->dropDownList($model,'pageSize', $model->getArrayPageSize(),array('empty'=>'Select','class'=>'w-200')); ?>
            </div>
        </div>
        <div class="row more_col">
            <div class="col1 display_none">
                <?php echo $form->labelEx($model,'status_cancel',['label'=>'Lý do từ chối']); ?>
                <?php echo $form->dropdownList($model,'status_cancel', $model->getArrayStatusCancel(), array('empty'=>'Select','class'=>'w-200')); 
                ?>
            </div>
        </div>
    
        <div class="row more_col">
            <div class="col1">
                <?php echo $form->labelEx($model,'province_id'); ?>
                <?php echo $form->dropDownList($model,'province_id', GasProvince::getArrAll(),array('style'=>'','class'=>'w-200','empty'=>'Select')); ?>
            </div> 	

            <div class="col2">
                <?php echo $form->labelEx($model,'district_id'); ?>
                <?php echo $form->dropDownList($model,'district_id', GasDistrict::getArrAll($model->province_id),array('class'=>'w-200', 'empty'=>"Select")); ?>
            </div>
        </div>
    
	<div class="row more_col display_none">
            <div class="col1">
                <?php echo $form->labelEx($model, 'sell_created_date_from',array()); ?>
                 <?php 
                    $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                        'model'=>$model,        
                        'attribute'=>'sell_created_date_from',
                        'options'=>array(
                            'showAnim'=>'fold',
                            'dateFormat'=> MyFormat::$dateFormatSearch,
                    //                            'minDate'=> '0',
                    //                            'maxDate'=> '0',
                            'changeMonth' => true,
                            'changeYear' => true,
                            'showOn' => 'button',
                            'buttonImage'=> Yii::app()->theme->baseUrl.'/admin/images/icon_calendar_r.gif',
                            'buttonImageOnly'=> true,                                
                        ),        
                        'htmlOptions'=>array(
                            'class'=>'w-16',
                            'style'=>'height:20px;',
    //                            'readonly'=>'readonly',
                        ),
                    ));
                    ?>  
                <?php echo $form->error($model,'sell_created_date_from'); ?>
            </div>
            <div class="col2">
                <?php echo $form->labelEx($model, 'sell_created_date_to', array()); ?>
                 <?php 
                    $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                        'model'=>$model,        
                        'attribute'=>'sell_created_date_to',
                        'options'=>array(
                            'showAnim'=>'fold',
                            'dateFormat'=> MyFormat::$dateFormatSearch,
                    //                            'minDate'=> '0',
                    //                            'maxDate'=> '0',
                            'changeMonth' => true,
                            'changeYear' => true,
                            'showOn' => 'button',
                            'buttonImage'=> Yii::app()->theme->baseUrl.'/admin/images/icon_calendar_r.gif',
                            'buttonImageOnly'=> true,                                
                        ),        
                        'htmlOptions'=>array(
                            'class'=>'w-16',
                            'style'=>'height:20px;',
    //                            'readonly'=>'readonly',
                        ),
                    ));
                    ?>  
                <?php echo $form->error($model,'sell_created_date_to'); ?>
            </div>
        </div>
        <div class="row">
            <?php echo $form->labelEx($model,'owner_id'); ?>
            <?php echo $form->hiddenField($model,'owner_id'); ?>
            <?php
                // 1. limit search kh của sale
                $url = Yii::app()->createAbsoluteUrl('admin/ajax/search_for_user_login');
                // widget auto complete search user customer and supplier
                $aData = array(
                    'model'=>$model,
                    'field_customer_id'=>'owner_id',
                    'url'=> $url,
                    'name_relation_user'=>'rOwner',
                    'ClassAdd' => 'w-200',
                    'field_autocomplete_name' => 'autocomplete_owner',
                    'placeholder'=>'Nhập mã hoặc tên nhân viên',
                );
                $this->widget('ext.SpaAutocompleteCustomer.SpaAutocompleteCustomer',
                    array('data'=>$aData));
                ?>
        </div>
	<div class="row buttons" style="padding-left: 159px;">
		        <?php $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType'=>'submit',
            'label'=>'Search',
            'type'=>'null', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
            'size'=>'small', // null, 'large', 'small' or 'mini'
            //'htmlOptions' => array('style' => 'margin-bottom: 10px; float: right;'),
        )); ?>	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->
    
<script>
    fnSelectProvinceDistrict('#CustomerDraft_province_id', '#CustomerDraft_district_id' , "<?php echo Yii::app()->createAbsoluteUrl('admin/ajax/get_slt_district');?>");
</script>
