<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'customer-draft-form',
	'enableAjaxValidation'=>false,
        'htmlOptions' => array('enctype' => 'multipart/form-data'),
)); ?>

    <p class="note">Dòng có dấu <span class="required">*</span> là bắt buộc.</p>
    <?php echo MyFormat::BindNotifyMsg(); ?>
    <div class="row">
        <?php echo $form->labelEx($model,'type'); ?>
        <?php  echo $form->dropDownList($model,'type', $model->getArrayType(), ['empty'=>'Select','class'=>'w-200']); ?>
        <?php echo $form->error($model,'type'); ?>
    </div>
    
    <div class="row more_col">
        <div class="col1">
            <?php echo $form->labelEx($model,'province_id'); ?>
            <?php echo $form->dropDownList($model,'province_id', GasProvince::getArrAll(),array('style'=>'','class'=>'w-200','empty'=>'Select')); ?>
            <?php echo $form->error($model,'province_id'); ?>
        </div> 	

        <div class="col2">
            <?php echo $form->labelEx($model,'district_id'); ?>
            <?php echo $form->dropDownList($model,'district_id', GasDistrict::getArrAll($model->province_id),array('class'=>'w-200', 'empty'=>"Select")); ?>
            <?php echo $form->error($model,'district_id'); ?>
        </div>
    </div>
    
    <div class="row">
        <?php echo $form->labelEx($model,'owner_id'); ?>
        <?php echo $form->hiddenField($model,'owner_id'); ?>
        <?php
            // 1. limit search kh của sale
            $url = Yii::app()->createAbsoluteUrl('admin/ajax/search_for_user_login');
            // widget auto complete search user customer and supplier
            $aData = array(
                'model'=>$model,
                'field_customer_id'=>'owner_id',
                'url'=> $url,
                'name_relation_user'=>'rOwner',
                'ClassAdd' => 'w-400',
                'field_autocomplete_name' => 'autocomplete_owner',
                'placeholder'=>'Nhập mã hoặc tên nhân viên',
            );
            $this->widget('ext.SpaAutocompleteCustomer.SpaAutocompleteCustomer',
                array('data'=>$aData));
            ?>
        <?php echo $form->error($model,'owner_id'); ?>
    </div>
    
    
    
    
    <div class="row">
        <?php echo $form->labelEx($model,'file_excel'); ?>
        <div style="padding-left: 140px;">
            <?php echo $form->fileField($model,'file_excel'); ?>
            <em class="hight_light item_b">Hệ thống chuyển sang nhập từ file excel. Bạn tải biểu mẫu về để thao tác</em>
            <em class="hight_light item_b"><a href="<?php echo Yii::app()->createAbsoluteUrl('/').'/upload/excel/customer_draft.xlsx';?>">Tải Biểu Mẫu Excel </a></em>
            <br>
            <em class="hight_light item_b">Mỗi lần thao tác nhập file, bạn hãy luôn tải file excel ở link trên về để nhập, tránh bị lỗi.</em>
            <br>
            <!--<em class="hight_light item_b">Quy Định Bắt Buộc Khi Nhập File Excel: Bảng Mã Unicode, kiểu gõ Telex. Nếu bạn gõ kiểu VNI hệ thống sẽ không hỗ trợ tìm kiếm. </em>-->
        </div>
        <?php echo $form->error($model,'file_excel'); ?>
    </div>
    <div class="row buttons" style="padding-left: 141px;">
                    <?php $this->widget('bootstrap.widgets.TbButton', array(
        'buttonType'=>'submit',
        'label'=>$model->isNewRecord ? 'Create' :'Save',
        'type'=>'null', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
        'size'=>'small', // null, 'large', 'small' or 'mini'
        //'htmlOptions' => array('style' => 'margin-bottom: 10px; float: right;'),
    )); ?>	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->
<script>
    $(document).ready(function(){
        $('.form').find('button:submit').click(function(){
            $.blockUI({ overlayCSS: { backgroundColor: '#fff' } }); 
        });

        fnSelectProvinceDistrict('#CustomerDraft_province_id', '#CustomerDraft_district_id' , "<?php echo Yii::app()->createAbsoluteUrl('admin/ajax/get_slt_district');?>");
    });
</script>