<?php
$this->breadcrumbs=array(
	$this->singleTitle,
);

$menus=array(
            array('label'=>"Create $this->singleTitle", 'url'=>array('create')),
);
$this->menu= ControllerActionsName::createMenusRoles($menus, $actions);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('customer-draft-grid', {
                url : $(this).attr('action'),
		data: $(this).serialize()
	});
	return false;
});
");

Yii::app()->clientScript->registerScript('ajaxupdate', "
$('#customer-draft-grid a.ajaxupdate').live('click', function() {
    $.fn.yiiGridView.update('customer-draft-grid', {
        type: 'POST',
        url: $(this).attr('href'),
        success: function() {
            $.fn.yiiGridView.update('customer-draft-grid');
        }
    });
    return false;
});
");
?>
<?php echo MyFormat::BindNotifyMsg(); ?>
<h1>Danh Sách <?php echo $this->pluralTitle; ?></h1>
<?php include 'index_button.php'; ?>

<?php echo CHtml::link('Tìm Kiếm Nâng Cao','#',array('class'=>'search-button')); ?>
<div class="search-form">
    <?php $this->renderPartial('_search',array(
            'model'=>$model,
    )); ?>
</div><!-- search-form -->
<div class="wide form">
    <?php $form=$this->beginWidget('CActiveForm', array(
            'id'=>'customer-form',
            'enableAjaxValidation'=>false,
            'htmlOptions' => array('enctype' => 'multipart/form-data'),
    )); ?>
        <?php $this->widget('zii.widgets.grid.CGridView', array(
                'id'=>'customer-draft-grid',
                'dataProvider'=>$model->search(),
                'afterAjaxUpdate'=>'function(id, data){ fnUpdateColorbox();}',
                'template'=>'{pager}{summary}{items}{pager}{summary}',
        //    'itemsCssClass' => 'items custom_here',
        //    'htmlOptions'=> array('class'=>'grid-view custom_here'),
                'pager' => array(
                    'maxButtonCount'=>  CmsFormatter::$PAGE_MAX_BUTTON,
                ),
                'enableSorting' => false,
                //'filter'=>$model,
                'columns'=>array(
                array(
                    'header' => 'S/N',
                    'type' => 'raw',
                    'value' => '$this->grid->dataProvider->pagination->currentPage * $this->grid->dataProvider->pagination->pageSize + ($row+1)',
                    'headerHtmlOptions' => array('width' => '10px','style' => 'text-align:center;'),
                    'htmlOptions' => array('style' => 'text-align:center;')
                ),
                        array(
                            'class'=>'CCheckBoxColumn',
                            'selectableRows'=>2,
                            'id'=>'CustomerDraft[customer]',
//                            'disabled'=>'$data->canModify()',
                        ),
                        array(
                            'name'    =>'name',
                            'type'      => 'raw',
                            'value'     => '$data->getName()'
                        ),
                        array(
                            'name'    =>'phone',
                            'type'      => 'raw',
                            'value'     => '$data->getPhoneClickCall()'
                        ),
                        array(
                            'header'    =>'Địa chỉ Excel',
                            'type'      => 'raw',
                            'value'     => '$data->getDistrictView()'
                        ),
                        array(
                            'name'    =>'address',
                            'type'      => 'raw',
                            'value'     => '$data->getAddress()'
                        ),
                        array(
                            'name'    =>'employee_id',
                            'type'      => 'raw',
                            'value'     => '$data->getEmployee()'
                        ),
                        array(
                            'name'    =>'owner_id',
                            'type'      => 'raw',
                            'value'     => '$data->getOwner()',
                            'visible'=> MyFormat::getCurrentRoleId()==ROLE_ADMIN,
                        ),
                        array(
                            'name'    =>'status',
                            'type'      => 'raw',
                            'value'     => '$data->getStatus()'
                        ),
//                        array(
//                            'name'    =>'status_cancel',
//                            'type'      => 'raw',
//                            'value'     => '$data->getStatusCancel()'
//                        ),
//                        array(
//                            'name'    =>'sell_created_date',
//                            'type'      => 'raw',
//                            'value'     => '$data->getSellCreated()'
//                        ),
                        array(
                            'name'    =>'appointment_date',
                            'type'      => 'raw',
                            'value'     => '$data->getAppointment()'
                        ),
                        array(
                            'name'    =>'created_date',
                            'type'      => 'raw',
                            'value'     => '$data->getCreatedDate()'
                        ),
                        array(
                            'name'=>'Actions',
                            'type'=>'raw',
                            'value'=>'$data->getActions()',
                        ),
                ),
        )); ?>
    <?php 
        $mTelesale = new Telesale();
    ?>
    <?php // if(!isset($_GET['type']) || $model->canUpdateApply()): ?>
    <?php if(!isset($_GET['type']) || ($mTelesale->showSetSaleId() &&  $_GET['type'] != $model->called && $_GET['type'] != $model->call_again) ): ?>
        <div class="row more_col BoxApply">
            <div class="col1">
                <?php  echo $form->labelEx($model,'employee_id', ['label'=>'Nhân viên']); ?>
                <?php  echo $form->dropDownList($model,'employee_id', $model->getArrayEmployee(), ['empty'=>'Xóa nhân viên Telesale hiện tại','class'=>'w-200']); ?>
            </div>
            <?php // if($model->canDelete() || $mTelesale->showSetSaleId()): ?>
                <div class="col2">
                    <?php echo $form->labelEx($model,'generate',array('class'=>'checkbox_one_label', 'style'=>'padding-top:3px;', 'label'=> "Gắn $model->countGenerate KH cho NV telesale chọn ")); ?>
                    <?php echo $form->checkBox($model,'generate',array('class'=>'float_l')); ?>
                </div>
            <?php // endif; ?>
        </div>
        <div class="row more_col">
            <div class="col1" style="float: left; margin: 0 20px 0 0;">
                <?php $this->widget('bootstrap.widgets.TbButton', array(
                'buttonType'=>'submit',
                'label'=> 'Apply',
                'type'=>'null', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
                'size'=>'small', // null, 'large', 'small' or 'mini'
                'htmlOptions' => array('style' => 'margin-bottom: 10px; float: right;'),
                )); ?>	
            </div>
            <?php // if($model->canDelete()): ?>
            <?php // if($mTelesale->canDelete() || $cUid == GasConst::UID_VEN_NTB): ?>
                <!--<div class="col2">-->
                    <?php // $this->widget('bootstrap.widgets.TbButton', array(
//                    'buttonType'=>'button',
//                    'label'=> 'Enable Remove Sale Employee',
//                    'type'=>'null', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
//                    'size'=>'small', // null, 'large', 'small' or 'mini'
//                    'htmlOptions' => array(
//                        'visible' => $model->canDelete(),
//                        'class'=>'enableRmSaleIdBtn'
//                    ),
//                    )); ?>	
                <!--</div>-->
            <?php // endif; ?>
        </div>
    <?php endif; ?>
    <?php $this->endWidget(); ?>
</div>
<script>
$(document).ready(function() {
    fnUpdateColorbox();
});

function fnUpdateColorbox(){   
    fixTargetBlank();
//    $('[id^="CustomerDraft[customer]_"][disabled=disabled]').attr("data-display",'hidden');
//    $('.enableRmSaleIdBtn').css({"background": "#0080FF",'color': 'white','border': 'none','padding': '5px 10px','border-radius': '5px','font-weight': 'bold','cursor': 'pointer'});
    //check all
    $("#CustomerDraft_customer_all").click(function(){
        $('input:checkbox:enabled[name="CustomerDraft[customer][]"]').not(this).prop('checked', this.checked);
    });
    // Thực hiện dc quyền check danh sách khác
//    $('.enableRmSaleIdBtn').off('click').on('click',function(){
//        //enable
//        if($('[id^="CustomerDraft[customer]_"][disabled]').length > 0){
//            $('[id^="CustomerDraft[customer]_"][disabled]').removeAttr('disabled');
//            $(this).text("Disable Remove Sale Employee");
//        }
//        else{
//            $('[id^="CustomerDraft[customer]_"][data-display=hidden]').attr('disabled','disabled');
//            $(this).text("Enable Remove Sale Employee");
//        }
//    });
    // color box
    $(".targetHtml").colorbox({
        iframe: true,
        overlayClose: false, escKey: true,
        innerHeight: '1000',
        innerWidth: '1100', close: "<span title='close'>close</span>",
        onClosed: function () { // update view when close colorbox
            $.fn.yiiGridView.update('customer-draft-grid');
        }
    });
}
</script>