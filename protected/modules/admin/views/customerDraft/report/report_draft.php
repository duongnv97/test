<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/print-store-card.css" />
<?php
$this->breadcrumbs=array(
	$this->pageTitle,
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
");
?>
<?php echo MyFormat::BindNotifyMsg(); ?>
<h1><?php echo $this->pageTitle; ?></h1>
<?php echo CHtml::link('Tìm Kiếm Nâng Cao','#',array('class'=>'search-button')); ?>
<div class="search-form">
    <?php $this->renderPartial('report/_search',array(
            'model'=>$model,
    )); ?>
</div><!-- search-form -->

<table class="tb hm_table">
<thead>
    <tr>
        <th class="item_c">#</th>
        <th class="item_c">Nhân viên</th>
        <th class="item_c">Tổng cuộc gọi</th>
        <?php foreach ($aData['LIST_STATUS'] as $key => $strTh) { ?>
        <th class="item_c"><?php echo $strTh; ?></th>
        <?php } ?>
    </tr>
</thead>
<tbody>
    <?php $i = 1; ?>
    <?php foreach ($aData['REPORT'] as $employee_id => $aReport) { ?>
    <tr>
        <td class="item_c"><?php echo $i++; ?></td>
        <td class="item_b"><?php echo !empty($aData['MODEL_EMPLOYEES'][$employee_id]) ? $aData['MODEL_EMPLOYEES'][$employee_id]->first_name : ''  ?></td>
        <td class="item_c item_b"><?php echo !empty($aData['REPORT'][$employee_id]) ? array_sum($aData['REPORT'][$employee_id]) : ''  ?></td>
        <?php foreach ($aData['LIST_STATUS'] as $statusCall => $strTh) { ?>
        <td class="item_c"><?php echo !empty($aReport[$statusCall]) ? $aReport[$statusCall] : ''; ?></td>
        <?php } ?>
    </tr>
    <?php } ?>
    
</tbody>
</table>