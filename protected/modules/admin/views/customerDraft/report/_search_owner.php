<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=> GasCheck::getCurl(),
	'method'=>'get',
)); ?>
        <div class="row more_col">
            <div class="col1">
                <?php echo $form->labelEx($model, 'Từ', []); ?>
                 <?php 
                    $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                        'model'=>$model,        
                        'attribute'=>'date_call_from',
                        'options'=>array(
                            'showAnim'=>'fold',
                            'dateFormat'=> MyFormat::$dateFormatSearch,
                    //                            'minDate'=> '0',
                    //                            'maxDate'=> '0',
                            'changeMonth' => true,
                            'changeYear' => true,
                            'showOn' => 'button',
                            'buttonImage'=> Yii::app()->theme->baseUrl.'/admin/images/icon_calendar_r.gif',
                            'buttonImageOnly'=> true,                                
                        ),        
                        'htmlOptions'=>array(
                            'class'=>'w-16',
                            'style'=>'height:20px;',
    //                            'readonly'=>'readonly',
                        ),
                    ));
                    ?>  
            </div>
            <div class="col2">
                <?php echo $form->labelEx($model, 'Đến', []); ?>
                 <?php 
                    $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                        'model'=>$model,        
                        'attribute'=>'date_call_to',
                        'options'=>array(
                            'showAnim'=>'fold',
                            'dateFormat'=> MyFormat::$dateFormatSearch,
                    //                            'minDate'=> '0',
                    //                            'maxDate'=> '0',
                            'changeMonth' => true,
                            'changeYear' => true,
                            'showOn' => 'button',
                            'buttonImage'=> Yii::app()->theme->baseUrl.'/admin/images/icon_calendar_r.gif',
                            'buttonImageOnly'=> true,                                
                        ),        
                        'htmlOptions'=>array(
                            'class'=>'w-16',
                            'style'=>'height:20px;',
    //                            'readonly'=>'readonly',
                        ),
                    ));
                    ?>  
            </div>
        </div>
        <div class="row">
            <?php echo $form->labelEx($model,'owner_id'); ?>
            <?php echo $form->hiddenField($model,'owner_id'); ?>
            <?php
                // 1. limit search kh của sale
                $url = Yii::app()->createAbsoluteUrl('admin/ajax/search_for_user_login');
                // widget auto complete search user customer and supplier
                $aData = array(
                    'model'=>$model,
                    'field_customer_id'=>'owner_id',
                    'url'=> $url,
                    'name_relation_user'=>'rOwner',
                    'ClassAdd' => 'w-400',
                    'field_autocomplete_name' => 'autocomplete_owner',
                    'placeholder'=>'Nhập mã hoặc tên nhân viên',
                );
                $this->widget('ext.SpaAutocompleteCustomer.SpaAutocompleteCustomer',
                    array('data'=>$aData));
                ?>
        </div>
        <div class="row">
            <?php echo $form->labelEx($model,'status',['label'=>'Trạng thái']); ?>
            <?php echo $form->dropdownList($model,'status', $model->getArrayCallStatus(true), array('empty'=>'Select','class'=>'w-200')); 
            ?>
        </div>
	<div class="row buttons" style="padding-left: 159px;">
		        <?php $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType'=>'submit',
            'label'=>'Search',
            'type'=>'null', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
            'size'=>'small', // null, 'large', 'small' or 'mini'
            //'htmlOptions' => array('style' => 'margin-bottom: 10px; float: right;'),
        )); ?>	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->