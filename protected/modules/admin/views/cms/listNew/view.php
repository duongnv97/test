<?php
/**
 * VerzDesignCMS
 * 
 * LICENSE
 *
 * @copyright	Copyright (c) 2012 Verz Design (http://www.verzdesign.com)
 * @version 	$Id: view.php 2012-06-01 09:09:18 nguyendung $
 * @since		1.0.0
 */
?>
<?php
$this->breadcrumbs=array(
	'Cms'=>array('index'),
	$model->title,
);

$menus = array(
                array('label'=>'Create Cms', 
                    'url'=>array('listNewCreate'), 
                    'action' => 'create',
                    'htmlOptions' => array('class' => 'create', 'label' => 'Tạo mới')
                    ),
                array('label'=>'Update Cms', 
                    'url'=>array('listNewUpdate', 'id'=>$model->id), 
                    'action' => 'update',
                    'htmlOptions' => array('class' => 'update', 'label' => 'Cập nhật')
                    ),
                array('label'=>'Delete Cms', 
                    'url'=>array('listNewDelete'), 
                    'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),
                    'confirm'=>'Are you sure you want to delete this item?'),
                    'htmlOptions' => array('class' => 'delete', 'label' => 'Xóa')
                    ),
                array('label'=>'Manage Cms', 
                    'url'=>array('listNewIndex'), 
                    'action' => 'index',
                    'htmlOptions' => array('class' => 'index', 'label' => 'Quản lý')
                    ),
        );

$this->menu= ControllerActionsName::createMenusRoles($menus, $actions);
?>

<h1>View Cms: <?php echo $model->title; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'title',
		'display_order',
		array(
			'name'=>'status',
			'type'=>'Status',
		),
		array(
                        'type' => 'datetime',
			'name'=>'created_date',
                        'value'=>$model->created_date,
			//'value'=>date(ActiveRecord::getDateFormatPhp().' H:i' , strtotime($model->created_date)),
		),
                array(
                    'name'=>'cms_content',
                    'type'=>'raw',
		),
		'short_content:html',
	),
)); ?>
