<?php
/**
 * VerzDesignCMS
 * 
 * LICENSE
 *
 * @copyright	Copyright (c) 2012 Verz Design (http://www.verzdesign.com)
 * @version 	$Id: _form.php 2012-06-01 09:09:18 nguyendung $
 * @since		1.0.0
 */
?>
<!-- <script src="//ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script> -->

<div class="form">
<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'cms-form',
	'enableAjaxValidation'=>false,
    'htmlOptions'=>array('enctype' => 'multipart/form-data')
)); ?>

    <p class="note">Fields with <span class="required">*</span> are required.</p>
    <?php echo MyFormat::BindNotifyMsg(); ?>
    <?php echo $form->errorSummary($model); ?>

    <div class="row buttons" style="padding-left: 141px;">
        <?php
        $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType' => 'submit',
            'label' => $model->isNewRecord ? 'Create' : 'Save',
            'type' => 'null', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
            'size' => 'small', // null, 'large', 'small' or 'mini'
                //'htmlOptions' => array('style' => 'margin-bottom: 10px; float: right;'),
        ));
        ?>	
    </div>
    <div class="clr"></div>

    <div class="row" style="display:none">
        <?php echo $form->labelEx($model, 'role_id'); ?>
        <?php echo $form->dropDownList($model, 'role_id', $model->getArrayRole(), array('empty'=>'Select','class' => 'w-400')); ?>
        <?php echo $form->error($model, 'role_id'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model, 'category_id'); ?>
        <?php echo $form->dropDownList($model, 'category_id', $model->getArrayCategory(), array('empty'=>'Select','class' => 'w-400')); ?>
        <?php echo $form->error($model, 'category_id'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model,'title'); ?>
        <?php echo $form->textField($model,'title',array('class'=>'w-800','maxlength'=>450)); ?>
        <?php echo $form->error($model,'title'); ?>
    </div>

    <div class="row hide_div">
        <?php echo $form->labelEx($model,'slug'); ?>
        <?php echo $form->textField($model,'slug',array('class' => 'w-400','maxlength'=>250)); ?>
        <?php echo $form->error($model,'slug'); ?>
    </div>
    <div class='clr'></div>

    <div class="row" >
        <?php echo $form->labelEx($model,'status'); ?>
        <?php echo $form->dropDownList($model,'status',ActiveRecord::getUserStatus(), array('class' => 'w-400'));?>
        <?php echo $form->error($model,'status'); ?>
    </div>

    <?php
    $tmp_ = array();
    for($i=1;$i<50;$i++)
        $tmp_[$i]=$i;
    ?>
    <div class="row">
            <?php echo $form->labelEx($model,'display_order'); ?>
            <?php echo $form->dropDownList($model,'display_order',$tmp_, array('class' => 'w-400')); ?>
            <?php echo $form->error($model,'display_order'); ?>
    </div>
    <br>
    <?php include '_formNotify.php'; ?>
    <br>
    <div class='clr'></div>
    <div class='clr'></div>
    <?php if(!$model->isNewRecord): ?>
        <?php include "_form_upload_multi.php"; ?>
    <?php endif; ?>
    <div class='clr'></div>
    <br><br>
    <div class="row">
        <label>&nbsp;</label>
        <label class="color_red" style="width: auto">
            Chú ý:
            <br>1. Kích thước hình ảnh trong bài viết: Hình chữ nhật đứng thì chọn 200 x 356 (width x height)
        </label>
    </div>
    <div class='clr'></div>
    <div class="">
        <?php echo $form->labelEx($model,'cms_content', ['class'=>'item_b f_size_15']); ?><br><br>
        <div class="">
            <?php echo $form->textArea($model,'cms_content',array('class'=>'ckeditor')); ?>
            <div class="clr"></div>
            <?php echo $form->error($model,'cms_content'); ?>
        </div>
    </div>

    <div class='clr'></div>
    <div class="row display_none">
        <?php echo $form->labelEx($model,'short_content'); ?>
        <?php echo $form->textArea($model,'short_content',array('class'=>'w-800', 'rows'=>10)); ?>
        <?php echo $form->error($model,'short_content'); ?>
    </div>
    <div class='clr'></div>

    <div class="row buttons" style="padding-left: 141px;">
        <?php
        $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType' => 'submit',
            'label' => $model->isNewRecord ? 'Create' : 'Save',
            'type' => 'null', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
            'size' => 'small', // null, 'large', 'small' or 'mini'
                //'htmlOptions' => array('style' => 'margin-bottom: 10px; float: right;'),
        ));
        ?>
    </div>

<?php $this->endWidget(); ?>

</div><!-- form -->
<?php Yii::app()->clientScript->registerCoreScript('jquery'); ?>
<script type="text/javaScript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/jquery.stringToSlug.js"></script>
<script type="text/javaScript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/jquery.stringToSlug.min.js"></script>

<script type="text/javascript">
    jQuery(document).ready(function(){
        $('.form').find('button:submit').click(function () {
            $.blockUI({overlayCSS: {backgroundColor: '#fff'}});
        });

        validateNumber();
        $("#Cms_title").stringToSlug({
                setEvents: 'keyup keydown blur',
                getPut: '#Cms_slug',
                space: '-'
        });
        $(".gallery").colorbox({iframe:true,innerHeight:'1100', innerWidth: '1000',close: "<span title='close'>close</span>"});
        
    });

    function validateNumber(){
        $(".number").each(function(){
                $(this).unbind("keydown");
                $(this).bind("keydown",function(event){
                    if( !(event.keyCode == 8                                // backspace
                        || event.keyCode == 46                              // delete
                        || event.keyCode == 9							// tab
                        || event.keyCode == 190							// dáº¥u cháº¥m (point) 
                        || (event.keyCode >= 35 && event.keyCode <= 40)     // arrow keys/home/end
                        || (event.keyCode >= 48 && event.keyCode <= 57)     // numbers on keyboard
                        || (event.keyCode >= 96 && event.keyCode <= 105))   // number on keypad
                        ) {
                            event.preventDefault();     // Prevent character input
                        }
                });
        });
    }
    
</script>

<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/gasckeditor/ckeditor.js"></script>
<!--<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/gasckeditor/samples/js/sample.js"></script>-->
<!--<link href="<?php echo Yii::app()->theme->baseUrl; ?>/gasckeditor/samples/css/samples.css" rel="stylesheet" type="text/css" media="screen" />-->
<!--<link href="<?php echo Yii::app()->theme->baseUrl; ?>/gasckeditor/samples/toolbarconfigurator/lib/codemirror/neo.css" rel="stylesheet" type="text/css" media="screen" />-->
<script>
    // http://stackoverflow.com/questions/15659390/ckeditor-automatically-strips-classes-from-div
    $(function(){
        $(".ckeditor").each(function(){
           var id = $(this).attr('id');
            CKEDITOR.replace( id, {
//                removePlugins: 'about, iframe, flash',
                removePlugins: 'about, flash',
                height: 600,
                allowedContent: true,// Aug 13, 2016 chắc chắn phải open dòng này, không thì sẽ bị lỗi khi có tab và accordiontab // http://docs.ckeditor.com/#!/guide/dev_allowed_content_rules  == http://ckeditor.com/forums/CKEditor-3.x/ckeditor-remove-tag-attributes
                extraAllowedContent : '*(*)'
            });
        });
        
    });
</script>