<?php
/**
 * VerzDesignCMS
 * 
 * LICENSE
 *
 * @copyright	Copyright (c) 2012 Verz Design (http://www.verzdesign.com)
 * @version 	$Id: admin.php 2012-06-01 09:09:18 nguyendung $
 * @since		1.0.0
 */
?>
<?php
$this->breadcrumbs=array(
	$this->pageTitle
);

$menus = array(
	array('label'=>'Create Cms', 
            'url'=>array('listNewCreate'),
            'htmlOptions' => array('class' => 'create', 'label' => 'Tạo mới')),
);
$this->menu= ControllerActionsName::createMenusRoles($menus, $actions);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('cms-grid', {
                url : $(this).attr('action'),
		data: $(this).serialize()
	});
	return false;
});
");

Yii::app()->clientScript->registerScript('ajaxupdate', "
$('#cms-grid a.ajaxupdate').live('click', function() {
    $.fn.yiiGridView.update('cms-grid', {
        type: 'POST',
        url: $(this).attr('href'),
        success: function() {
            $.fn.yiiGridView.update('cms-grid');
        }
    });
    return false;
});
");
?>
<?php echo MyFormat::BindNotifyMsg(); ?>
<h1><?php echo $this->pageTitle;?></h1>

<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
<div class="search-form">
<?php $this->renderPartial('listNew/_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'cms-grid',
	'dataProvider'=>$model->searchListNew(),
    'template'=>'{pager}{summary}{items}{pager}{summary}', 
    'afterAjaxUpdate'=>'function(id, data){ fnUpdateColorbox();}',
        //'afterAjaxUpdate'=>'function(id, data){alert(1)}',
        //'beforeAjaxUpdate'=>'function(id, data){ $(".search-form form").serialize(); }',
    
    
	//'filter'=>$model,
	'columns'=>array(
            array(
                'header' => 'S/N',
                'type' => 'raw',
                'value' => '$this->grid->dataProvider->pagination->currentPage * $this->grid->dataProvider->pagination->pageSize + ($row+1)',
                'headerHtmlOptions' => array('width' => '30px','style' => 'text-align:center;'),
                'htmlOptions' => array('style' => 'text-align:center;')
            ),
            
            array(
                'name' => 'title',
                'type' => 'raw',
                'value' => '$data->getTitle()'
            ),
            array(
                'name'=>'category_id',
                'type' => 'raw',
                'value'=>'$data->getCategoryId()',
            ),
            array(
                'name'=>'display_order',
                'htmlOptions' => array('style' => 'text-align:center;width:80px;')
            ),
            array(
                'name'=>'creator_id',
                'type' => 'raw',
                'value'=>'$data->getCreatedBy()',
            ),
            array(
                'type' => 'datetime',
                'name'=>'created_date',
                'value'=>'$data->created_date',
                'htmlOptions' => array('style' => 'text-align:center;')
            ),
            array(
                'header'  => 'Lượt xem',
                'value'  => '$data->getUserViewOnApp()',
                'htmlOptions' => array('style' => 'text-align:center;'),
            ),
            array(
                'name'=>'status',
                'type'=>'status',
                'value'=>'array("status"=>$data->status,"id"=>$data->id)',
                'htmlOptions' => array('style' => 'text-align:center;'),
//                'cssClassExpression'=>'GasCheck::isAllowAccess("cms", $data->status==0?"ajaxActivate":"ajaxDeactivate")?"":"remove_html_only"'
            ),

            array(
                    'header' => 'Actions',
                    'class'=>'CButtonColumn',
                    'template'=> ControllerActionsName::createIndexButtonRoles($actions, array('ListNewView', 'ListNewUpdate', 'delete')),
                    'buttons'=>array(
                        'ListNewView'=>array(
                            'label'=>'Xem',
                            'imageUrl'=>Yii::app()->theme->baseUrl . '/admin/images/icon/view_icon.png',
                            'options'=>array('class'=>'view'),
                            'url'=>'Yii::app()->createAbsoluteUrl("admin/cms/listNewView",
                                array("id"=>$data->id) )',
//                            'visible' => '1', => Oct118 Toàn code sai
                        ),
                        'ListNewUpdate'=>array(
                            'label'=>'Cập nhật',
                            'imageUrl'=>Yii::app()->theme->baseUrl . '/admin/images/icon/update_icon.png',
                            'options'=>array('class'=>''),
                            'url'=>'Yii::app()->createAbsoluteUrl("admin/cms/listNewUpdate",
                                array("id"=>$data->id) )',
                            'visible' => '$data->canUpdate()',
                        ),
//                        'ListNewDelete'=>array(
//                            'label'=>'Xóa',
//                            'imageUrl'=>Yii::app()->theme->baseUrl . '/admin/images/icon/delete_index.png',
//                            'options'=>array('class'=>'delete'),
//                            'url'=>'Yii::app()->createAbsoluteUrl("admin/cms/listNewDelete",
//                                array("id"=>$data->id) )',
//                            'visible' => '1',
//                        ),
                    ),
		),
	),
)); ?>

<script>
$(document).ready(function() {
    fnUpdateColorbox();
});

function fnUpdateColorbox(){   
    fixTargetBlank();
}
</script>