<?php
/**
 * VerzDesignCMS
 * 
 * LICENSE
 *
 * @copyright	Copyright (c) 2012 Verz Design (http://www.verzdesign.com)
 * @version 	$Id: _search.php 2012-06-01 09:09:18 nguyendung $
 * @since		1.0.0
 */
?>
<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>
    <div class="row more_col">
        <div class="col1">
            <?php echo $form->label($model, 'category_id'); ?>
            <?php echo $form->dropDownList($model, 'category_id', $model->getArrayCategory(), array('empty'=>'Select', 'class' => 'w-200')); ?>
        </div>
	<div class="col2">
            <?php echo $form->label($model,'status'); ?>
            <?php echo $form->dropDownList($model,'status', ActiveRecord::getUserStatus(), ['empty'=>'Select', 'class' => 'w-200']); ?>
	</div>
    </div>
    <div class="row buttons">
        <span class="btn-submit"><?php echo CHtml::submitButton('Search'); ?></span>
    </div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->