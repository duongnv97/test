<?php
/**
 * VerzDesignCMS
 * 
 * LICENSE
 *
 * @copyright	Copyright (c) 2012 Verz Design (http://www.verzdesign.com)
 * @version 	$Id: create.php 2012-06-01 09:09:18 nguyendung $
 * @since		1.0.0
 */
?>
<?php
$this->breadcrumbs=array(
	'QL thông báo'=>array('index'),
	'Tạo mới',
);

$menus = array(	
            array('label'=>'Manage Cms', 
                'url'=>array('listNewIndex'),
                'htmlOptions' => array('class' => 'index', 'label' => 'Quản lý')),
         );

$this->menu= ControllerActionsName::createMenusRoles($menus, $actions);
?>

<h1>Tạo mới thông báo App</h1>

<?php echo $this->renderPartial('listNew/_form', array('model'=>$model)); ?>

