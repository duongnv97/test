<?php
/**
 * VerzDesignCMS
 * 
 * LICENSE
 *
 * @copyright	Copyright (c) 2012 Verz Design (http://www.verzdesign.com)
 * @version 	$Id: update.php 2012-06-01 09:09:18 nguyendung $
 * @since		1.0.0
 */
?>
<?php
$this->breadcrumbs=array(
	'QL thông báo'=>array('index'),
	$model->title=>array('view','id'=>$model->id),
	'Cập nhật',
);

$menus = array(
                array('label'=>'Create Cms', 
                    'url'=>array('listNewCreate'), 
                    'action' => 'create',
                    'htmlOptions' => array('class' => 'create', 'label' => 'Tạo mới')
                    ),
                array('label'=>'View Cms', 
                    'url'=>array('listNewView', 'id'=>$model->id), 
                    'action' => 'view',
                    'htmlOptions' => array('class' => 'view', 'label' => 'Xem')
                    ),
                array('label'=>'Manage Cms', 
                    'url'=>array('listNewIndex'), 
                    'action' => 'index',
                    'htmlOptions' => array('class' => 'index', 'label' => 'Quản lý')
                    ),
        );

$this->menu= ControllerActionsName::createMenusRoles($menus, $actions);

?>

<h1><?php echo "Total Notify: {$model->countNotifyInsert()} - "; ?> Cập nhật: <?php echo $model->title; ?></h1>

<?php echo $this->renderPartial('listNew/_form', array('model'=>$model)); ?>