<?php
/**
 * VerzDesignCMS
 * 
 * LICENSE
 *
 * @copyright	Copyright (c) 2012 Verz Design (http://www.verzdesign.com)
 * @version 	$Id: _search.php 2012-06-01 09:09:18 nguyendung $
 * @since		1.0.0
 */
?>
<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>
        <div class="row more_col">
            <div class="col1">
                <?php echo $form->label($model, 'role_id'); ?>
                <?php echo $form->dropDownList($model, 'role_id', $model->getArrayRole(), array('empty'=>'Select','class' => 'w-200')); ?>
                <?php echo $form->error($model, 'role_id'); ?>
            </div>
        </div>
<?php /*
	<div class="row">
		<?php echo $form->label($model,'display_order'); ?>
		<?php echo $form->textField($model,'display_order'); ?>
	</div>
*/ ?>
	<div class="row">
		<?php echo $form->label($model,'show_in_menu'); ?>
		<?php echo $form->dropDownList($model,'show_in_menu',array(''=>'','0'=>'No','1'=>'Yes')); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'status'); ?>
		<?php echo $form->dropDownList($model,'status', ActiveRecord::getUserStatus(true)); ?>
	</div>

	
<?php /*
	<div class="row">
		<?php echo $form->label($model,'meta_keywords'); ?>
		<?php echo $form->textField($model,'meta_keywords',array('size'=>60,'maxlength'=>250)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'meta_desc'); ?>
		<?php echo $form->textField($model,'meta_desc',array('size'=>60,'maxlength'=>250)); ?>
	</div>
*/ ?>
	<div class="row buttons">
		<span class="btn-submit"><?php echo CHtml::submitButton('Search'); ?></span>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->