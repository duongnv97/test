<?php
$this->breadcrumbs=array(
	'QL Kênh'=>array('index'),
	$model->name,
);

$menus = array(
	array('label'=>'GasChannel Management', 'url'=>array('index')),
	array('label'=>'Create GasChannel', 'url'=>array('create')),
	array('label'=>'Update GasChannel', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete GasChannel', 'url'=>array('delete'), 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
);
$this->menu= ControllerActionsName::createMenusRoles($menus, $actions);
?>

<h1>View Kênh #<?php echo $model->name; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		
		'name',
		'short_name',
		'status:status',
	),
)); ?>
