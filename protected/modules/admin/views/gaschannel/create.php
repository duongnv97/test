<?php
$this->breadcrumbs=array(
	Yii::t('translation','QL Kênh')=>array('index'),
	Yii::t('translation','Create'),
);

$menus = array(		
        array('label'=> Yii::t('translation', 'GasChannel Management') , 'url'=>array('index')),
);
$this->menu= ControllerActionsName::createMenusRoles($menus, $actions);
?>

<h1><?php echo Yii::t('translation', 'Tạo Kênh'); ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>