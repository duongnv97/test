<?php
$this->breadcrumbs=array(
	Yii::t('translation','QL Kênh')=>array('index'),
	$model->name=>array('view','id'=>$model->id),
	Yii::t('translation','Update'),
);

$menus = array(	
        array('label'=> Yii::t('translation', 'GasChannel Management'), 'url'=>array('index')),
	array('label'=> Yii::t('translation', 'View GasChannel'), 'url'=>array('view', 'id'=>$model->id)),
	array('label'=> Yii::t('translation', 'Create GasChannel'), 'url'=>array('create')),	
);
$this->menu= ControllerActionsName::createMenusRoles($menus, $actions);

?>

<h1><?php echo Yii::t('translation', 'Cập Nhật Kênh'.$model->name); ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>