<?php
$this->breadcrumbs=array(
	'Đại Lý',
);

$menus=array(
	array('label'=> Yii::t('translation','Create Đại Lý'), 'url'=>array('create')),
);
$this->menu= ControllerActionsName::createMenusRoles($menus, $actions);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
/*
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('users-grid', {
                url : $(this).attr('action'),
		data: $(this).serialize()
	});
	return false;
});
*/
");

Yii::app()->clientScript->registerScript('ajaxupdate', "
$('#users-grid a.ajaxupdate').live('click', function() {
    $.fn.yiiGridView.update('users-grid', {
        type: 'POST',
        url: $(this).attr('href'),
        success: function() {
            $.fn.yiiGridView.update('users-grid');
        }
    });
    return false;
});
");
$cRole          = Yii::app()->user->role_id;
$aRoleViewMap   = array(ROLE_DIEU_PHOI);
$dataProvider   = $model->search();
$needMore       = array("listAgent"=>$dataProvider->data);
$actions[]      = 'updateDetail'; // DuongNV Aug1419 thêm action để tạo nút cập nhật tài xế ở column actions
?>

<?php if(!in_array($cRole, $aRoleViewMap) || ( $cRole == ROLE_ADMIN && !GasCheck::isLocalhost())): ?>
    <?php $this->widget('AgentMapWidget', array('needMore' => $needMore)); ?>
<?php endif; ?>

<h1><?php echo Yii::t('translation', 'Đại Lý'); ?></h1>

<div class="search-form" >
<?php include "_search_index.php"; ?>
</div>    

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'users-grid',
	'dataProvider'=>$dataProvider,
        'template'=>'{pager}{summary}{items}{pager}{summary}',     
	'afterAjaxUpdate'=>'function(id, data){fnUpdateColorbox();}',
	'columns'=>array(
        array(
            'header' => 'S/N',
            'type' => 'raw',
            'value' => '$this->grid->dataProvider->pagination->currentPage * $this->grid->dataProvider->pagination->pageSize + ($row+1)',
            'headerHtmlOptions' => array('width' => '30px','style' => 'text-align:center;'),
            'htmlOptions' => array('style' => 'text-align:center;')
        ),
            array(
                'header' => 'Mã Đại Lý',
                'name' => 'code_account',
                'htmlOptions' => array('style' => 'width:50px;')
            ), 	
            array(
                'header' => 'Mã Mới',
                'value' => '$data->username . "<br>"',
                'type' => 'raw',
                'htmlOptions' => array('style' => 'text-align:center;')
            ),
//            array(
//                'header' => 'Mật Khẩu',
//                'value'=>'$data->temp_password',
//                'visible'=>Yii::app()->user->role_id==ROLE_ADMIN,
//            ),  
            array(
                'name'=>'id',
                'visible'=>Yii::app()->user->role_id==ROLE_ADMIN,                
            ),
            array(
                'header'=>'HS Pháp Lý',
                'value'=>'$data',
                'type'=>'AgentProfileCheck',
                'htmlOptions' => array('class' => 'item_c'),
                'visible'=>Yii::app()->user->role_id==ROLE_ADMIN,
            ),
            
            array(
                'header' => 'Loại',
                'name' => 'gender',
                'value' => '$data->getTypeAgent()',
            ),
            array(
                'name'=>'phone',
                'type'=>'html',
                'value'=>'$data->getAgentInfo()',
                'htmlOptions' => array('style' => 'width:70px;'),
            ),
            array(
                'header' => 'Tên Đại Lý',
                'name'=>'first_name',
            ),
            
            array(
                'header' => 'Ngày Cập Nhật Sổ Quỹ',
                'name'=>'payment_day',
                'htmlOptions' => array('style' => 'text-align:center;width:80px;'),
                'visible'=>Yii::app()->user->role_id==ROLE_ADMIN,                
            ),
            
            array(
                'name'=>'address',
                'type'=>'html',
                'value'=>'$data->getInfoAgentOnly()',
            ),		
            array(
                'name'=>'province_id',
                'value'=>'$data->getProvince()',
            ),
            array(
                'name'=>'district_id',
                'type'=>'raw',
                'value'=>'$data->getDistrict()."<br>".$data->price_other." m"',
            ),
            array(
                'name'=>'status',
                'type'=>'status',
                'htmlOptions' => array('style' => 'text-align:center;')
            ),
            array(
                'name'=>'Trạng thái thực tế',
                'type'=>'raw',
                'value'=>'$data->getRealStatusAgent()',
                'htmlOptions' => array('style' => 'text-align:center;')
            ),
//            array(
//                'name'=>'beginning',
//                'type'=>'Currency',
//                'htmlOptions' => array('style' => 'text-align:right;'),
//                'visible'=>Yii::app()->user->role_id==ROLE_ADMIN,
//            ), 
//            array(
//                'header' => 'Login Mới Nhất',
//                'name'=>'last_logged_in',
//                'type'=>'datetime',
//            ),			
            
            array(
            'header' => 'Action',
            'class'=>'CButtonColumn',
//            'template'=> ControllerActionsName::createIndexButtonRoles($actions, array('view')),
//                'template'=> ControllerActionsName::createIndexButtonRoles($actions, array('view', 'update')),
                'template'=> ControllerActionsName::createIndexButtonRoles($actions,array('view','update','updateDetail')),
                'buttons'=>[
                    'updateDetail' => array
                    (
                        'label'=>'Cập nhật xe, tài xế...',     //Text label of the button.
                        'url'=> 'Yii::app()->createAbsoluteUrl("/admin/gasmember/updateDetail", ["id"=>$data->id])',  
                        'imageUrl'=>Yii::app()->theme->baseUrl . '/admin/images/icon_new_task.gif',
                        'options'=>array('class' => 'update_agent'), //HTML options for the button tag..
                        'visible' => 'GasCheck::isAllowAccess("gasmember", "updateDetail")'
                    )
                ]
            ),
	),
)); ?>

<script>
$(document).ready(function() {
    fnUpdateColorbox();
    fnUpdateAgent();
});

function fnUpdateColorbox(){
    fixTargetBlank();
//    $('.button-column').find('.view').attr('target','_blank');
}
function fnUpdateAgent(){    
    $(document).on('click', '.update_agent', function(event){
        event.preventDefault();
        $.colorbox({
            href: $(this).attr("href"),
            iframe: true,
            innerHeight: '500',
            innerWidth: '1050',
            close: "<span title='close'>close</span>"
        });
    });
}
</script>