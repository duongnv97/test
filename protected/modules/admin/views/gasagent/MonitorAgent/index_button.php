<div class="form">
    <?php
        $LinkNormal     = Yii::app()->createAbsoluteUrl('admin/gasagent/monitorAgent');
        $LinkReport     = Yii::app()->createAbsoluteUrl('admin/gasagent/monitorAgent', array( 'report'=> 1));
    ?>
    <h1><?php echo $this->adminPageTitle;?>
        <a class='btn_cancel f_size_14 <?php echo (!isset($_GET['report'])) ? "active":"";?>' href="<?php echo $LinkNormal;?>">Theo dõi chi tiết</a>
        <a class='btn_cancel f_size_14 <?php echo isset($_GET['report']) && $_GET['report']==1 ? "active":"";?>' href="<?php echo $LinkReport;?>">Báo Cáo</a>
    </h1> 
</div>