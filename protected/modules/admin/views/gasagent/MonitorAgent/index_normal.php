<?php $allAgent = $model->getByDate(); ?>
<table class="tb materials_table  hm_table">
    <thead>
        <tr>
            <th class="w-50 item_c">#</th>
            <th class="w-100 item_c">Mã ĐL</th>
            <th class="w-250 item_c" >Đại Lý</th>
            <th class="td_last_r  item_c w-120">Ngày Giờ Online</th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($allAgent as $key=>$item): ?>
        <tr>
            <td class="item_c"><?php echo $key+1; ?></td>
            <td class="item_c padding_5"><?php echo $item->code_account; ?></td>
            <td><?php echo $item->getAgentName(); ?></td>
            <td class="td_last_r item_c"><?php echo MyFormat::dateConverYmdToDmy($item->last_active, "d/m/Y H:i"); ?></td>
        </tr>
        <?php endforeach; ?>
    </tbody>
</table>