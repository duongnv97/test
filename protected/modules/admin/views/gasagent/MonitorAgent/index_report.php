<?php $aData = $model->reportByDate(); 
$aDays      = MyFormat::getArrayDay($model->date_from_ymd, $model->date_to_ymd);
$aAgent     = Users::getArrObjectUserByRole(ROLE_AGENT, array(), array());
$OUTPUT     = isset($aData['OUTPUT']) ? $aData['OUTPUT'] : array();
$SUM_ALL    = isset($aData['SUM_ALL']) ? $aData['SUM_ALL'] : array();
$key        = 1;
?>
<div class="grid-view">
<table class="tb hm_table freezetablecolumns" id="freezetablecolumns">
    <thead>
        <tr>
            <th class="w-20 item_c">#</th>
            <th class="w-50 item_c">Mã ĐL</th>
            <th class="w-150 item_c" >Đại Lý</th>
            <th class="w-20">Tổng cộng</th>
            <?php foreach ($aDays as $day): ?>
            <?php $tmp = explode('-', $day); ?>
                <th class="w-20"><?php echo $tmp[2];?></th>
            <?php endforeach; ?>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($SUM_ALL as $agent_id=>$sum): ?>
        <?php 
            $agentName  = '';
            $agentCode  = '';            
            $mAgent = isset($aAgent[$agent_id]) ? $aAgent[$agent_id] : false;
            if($mAgent){
                $agentName  = $mAgent->getFullName();
                $agentCode  = $mAgent->code_account;
            }
        ?>
        <tr>
            <td class="item_c w-20"><?php echo $key++; ?></td>
            <td class="item_c w-50"><?php echo $agentCode; ?></td>
            <td class="w-150"><?php echo $agentName; ?></td>
            <td class="item_c w-20 item_b"><?php echo $sum; ?></td>
            <?php foreach ($aDays as $day): ?>
            <?php 
                $qty = isset($OUTPUT[$agent_id][$day]) ? $OUTPUT[$agent_id][$day] : '';
            ?>
                <td class="w-20"><?php echo $qty;?></td>
            <?php endforeach; ?>
        </tr>
        <?php endforeach; ?>
    </tbody>
</table>
</div>    

<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/scroll/jquery.freezetablecolumns.1.1.js"></script> 
<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/print-store-card.css" />
<script>
$(document).ready(function() {
    initFreezetable();
});

function initFreezetable(){
    $('#freezetablecolumns').freezeTableColumns({
        width:       <?php echo Users::WIDTH_freezetable;?>,   // required - bt 1100
        height:      1000,   // required
        numFrozen:   4,     // optional
        frozenWidth: 330,   // optional
        clearWidths: true  // optional
    });   
}  
</script>