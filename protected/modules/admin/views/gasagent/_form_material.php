<?php 
    $canUpdate = isset($canUpdate) ? $canUpdate : 0;
?>

<h3 class='title-info'>Cài đặt Gas + Hàng khuyến mãi được bán</h3>
<div class="clr"></div>
<?php if($canUpdate): ?>
    <div class="row">
        <div style="width: 100%;float: left;padding-bottom: 5px;padding-left: 140px;">
            <label style="color: red;width: auto; ">Danh mục Gas + Hàng khuyến mãi được bán của đại lý</label>
        </div>
        <label>Vật tư</label>
        <input type="text" class="AutoMaterialName" placeholder="Nhập mã vật tư hoặc tên vật tư">
        <div class="clr"></div>    		
    </div>
<?php endif; ?>

<div class="row">
    <label>&nbsp</label>
    <table class="materials_table hm_table">
        <thead>
            <tr>
                <th class="item_c">#</th>
                <th class="item_code item_c">Mã</th>
                <th class="item_name item_c">Tên</th>
                <th class="item_unit item_c">ĐVT</th>
                <?php if($canUpdate): ?>
                <th class="item_unit last item_c">Xóa</th>
                <?php endif; ?>
            </tr>
        </thead>
        <tbody>
        <?php foreach($model->aMaterial as $key=>$materials_id):?>
            <?php 
                $mSell = new SellDetail();
                $mSell->materials_id = $materials_id;
            $mMaterial = $mSell->rMaterial; ?>
            <?php if($mMaterial): ?>
            <tr class="materials_row">
                <td class="item_c order_no"><?php echo ($key+1);?></td>
                <td><?php echo $mMaterial->materials_no;?></td>
                <td class="item_l"><?php echo $mMaterial->name;?>
                    <input name="materials_id_agent[]" value="<?php echo $mMaterial->id;?>" class="materials_id_agent" type="hidden">
                </td>
                <td class="item_c"><?php echo $mMaterial->unit;?></td>
                <?php if($canUpdate): ?>
                <td class="item_c last"><span class="remove_icon_only"></span></td>
                <?php endif; ?>
            </tr>
            <?php endif;?>
        <?php endforeach;?>
        </tbody>
    </table>
</div>

<script>
    //  Jun 25, 2016 begin for form detail
    $(document).ready(function(){
        fnInitAutocomplete();
        fnRefreshOrderNumber();    
        fnBindRemoveIcon();
    });
    
    /**
    * @Author: ANH DUNG Jun 25, 2016
    * @Todo: init autocomplete
    */
    function fnInitAutocomplete(){
        var availableMaterials = <?php echo MyFunctionCustom::getMaterialsJson(); ?>;
        $('.AutoMaterialName').each(function(){
            var this_ = $(this);
            $(this).autocomplete({
                source: availableMaterials,
                close: function( event, ui ) { this_.val(''); },
                select: function( event, ui ) {
                    var class_item = 'materials_row_'+ui.item.id;
                    if($('.'+class_item).size()<1)
                        fnBuildRowMaterial(event, ui); 
                }
            });
        });
    }
    
    function fnBuildRowMaterial(event, ui){
        var PriceBinhBoByMonth = "";
        var class_item = 'materials_row_'+ui.item.id;
        var _tr = '';
            _tr += '<tr class="materials_row '+class_item+'">';
                _tr += '<td class="item_c order_no"></td>';
                _tr += '<td>'+ui.item.materials_no+'</td>';
                _tr += '<td class="">'+ui.item.name;
                    _tr += '<input name="materials_id_agent[]" value="'+ui.item.id+'" class="materials_id_agent" type="hidden">';
                _tr += '</td>';
                _tr += '<td class="item_c">'+ui.item.unit+'</td>';
                _tr += '<td class="item_c last"><span class="remove_icon_only"></span></td>';
            _tr += '</tr>';
        $('.materials_table tbody').append(_tr);
        fnRefreshOrderNumber();
    }
    
//    Jun 25, 2016 end for form detail
    
</script>


