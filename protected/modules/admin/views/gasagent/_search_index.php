<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=> Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>
    <div class="row">
        <?php echo $form->labelEx($model,'gender', array('label'=>'Loại')); ?>
        <?php echo $form->dropDownList($model,'gender', $model->getArrayTypeAgent(),array('class'=>'w-300', 'empty'=>'Select')); ?>
        <?php echo $form->error($model,'gender'); ?>
    </div>
    
    <div class="row">
        <?php echo $form->label($model,'address_vi', array('label'=>"Tên đại lý")); ?>
        <?php echo $form->textField($model,'address_vi',array('class'=>'w-300','maxlength'=>250)); ?>
    </div>
    
    <div class="row more_col">
        <div class="col1">
            <?php echo $form->labelEx($model,'parent_id', array('label'=>'Công ty')); ?>
            <?php echo $form->dropDownList($model,'parent_id', Borrow::model()->getListdataCompany(),array('class'=>'w-300', 'empty'=>'Select')); ?>
        </div>
        <div class="col2">
            <?php echo $form->labelEx($model,'created_by', array('label'=>'Chủ cơ sở')); ?>
            <?php echo $form->dropDownList($model,'created_by', GasConst::getBossAgent(),array('class'=>'w-300', 'empty'=>'Select')); ?>
        </div>
    </div>
    
    <div class="row more_col">
        <div class="col1">
            <?php echo $form->labelEx($model,'province_id'); ?>
            <?php echo $form->dropDownList($model,'province_id', GasProvince::getArrAll(),array('style'=>'','class'=>'w-300','empty'=>'Select')); ?>
        </div> 	

        <div class="col2">
            <?php echo $form->labelEx($model,'district_id'); ?>
            <?php echo $form->dropDownList($model,'district_id', GasDistrict::getArrAll($model->province_id),array('class'=>'w-300', 'empty'=>"Select")); ?>
        </div>
    </div>
    
    <div class="row more_col">
        <div class="col1">
            <?php echo $form->labelEx($model,'first_char', ['label'=>'Trạng thái thực tế']); ?>
            <?php echo $form->dropDownList($model,'first_char', $model->getArrayRealStatusAgent(),array('class'=>'w-300', 'empty'=>"Select")); ?>
        </div>
    </div>

    <div class="row buttons" style="padding-left: 159px;">
        <?php $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType'=>'submit',
            'label'=>Yii::t('translation','Search'),
            'type'=>'null', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
            'size'=>'small', // null, 'large', 'small' or 'mini'
            //'htmlOptions' => array('style' => 'margin-bottom: 10px; float: right;'),
        )); ?>	
    </div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->


<script>
    $(document).ready(function(){
        fnBindChangeProvince();
    });
    
    /**
    * @Author: ANH DUNG Apr 20, 2016
    */
    function fnBindChangeProvince(){
        $('body').on('change', '#Users_province_id', function(){
            var province_id = $(this).val();        
            var url_ = "<?php echo Yii::app()->createAbsoluteUrl('admin/ajax/get_slt_district');?>";
            $.blockUI({ overlayCSS: { backgroundColor: '<?php echo BLOCK_UI_COLOR;?>' } }); 
            $.ajax({
                url: url_,
                data: {ajax:1,province_id:province_id}, 
                type: "get",
                dataType:'json',
                success: function(data){
                    $('#Users_district_id').html(data['html_district']);                
                    $.unblockUI();
                }
            });
        });

//        $('body').on('change', '#Users_district_id', function(){
//            var province_id = $('#Users_province_id').val();   
//            var district_id = $(this).val();        
//            var url_ = "<?php echo Yii::app()->createAbsoluteUrl('admin/ajax/get_slt_ward');?>";
//            $.blockUI({ overlayCSS: { backgroundColor: '<?php echo BLOCK_UI_COLOR;?>' } }); 
//            $.ajax({
//                url: url_,
//                data: {ajax:1,district_id:district_id,province_id:province_id},
//                type: "get",
//                dataType:'json',
//                success: function(data){
//                    $('#Users_ward_id').html(data['html_district']);                
//                    $.unblockUI();
//                }
//            });
//        });
    }
</script>