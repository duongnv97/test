<?php 
    if(empty($model->price_other)){
        $model->price_other = 3000;
    }
    $mOM    = new GasOneMany();
    $checkCity   = $mOM->isAgentOfCity($model->province_id, $model->id, GasOneMany::TYPE_AGENT_CITY);
?>
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/jquery.blockUI.js"></script>    
<div class="form">

    <?php echo MyFormat::BindNotifyMsg(); ?>
<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'users-form',
	'enableAjaxValidation'=>false,
)); ?>
	<?php echo Yii::t('translation', '<p class="note">Fields with <span class="required">*</span> are required.</p>'); ?>
        <div class="row GasCheckboxList" style=''>
            <?php echo $form->labelEx($model,'gender', array('label'=>'Loại')); ?>
            <?php echo $form->radioButtonList($model,'gender', $model->getArrayTypeAgent(), 
                        array(
                            'separator'=>"",
                            'template'=>'<li>{input}{label}</li>',
                            'container'=>'ul',
                            'class'=>'RadioOrderType' 
                        )); 
            ?>
        </div>
        <div class='clr'></div>
        <div class="row GasCheckboxList" style=''>
            <?php echo $form->labelEx($model,'first_char', array('label'=>'Trạng thái thực tế')); ?>
            <?php echo $form->radioButtonList($model,'first_char', $model->getArrayRealStatusAgent(), 
                        array(
                            'separator'=>"",
                            'template'=>'<li>{input}{label}</li>',
                            'container'=>'ul',
                            'class'=>'RadioOrderType' 
                        )); 
            ?>
        </div>
        <div class='clr'></div>
    
        <div class="row">
            <?php echo $form->labelEx($model,'parent_id', array('label'=>'Công ty')); ?>
            <?php echo $form->dropDownList($model,'parent_id', Borrow::model()->getListdataCompany(),array('class'=>'w-400', 'empty'=>'Select')); ?>
            <?php echo $form->error($model,'parent_id'); ?>
        </div>
         
        <div class="row">
            <?php echo $form->labelEx($model, 'sale_id', array('label' => 'Chi nhánh/ Địa điểm')); ?>
            <?php $mBranchCompany = new BranchCompany(); ?>
            <?php echo $form->dropDownList($model, 'sale_id',$model->isNewRecord?[]:$mBranchCompany->getArrBranch($model->parent_id), array('class' => 'w-400', 'empty' => 'Select')); ?>
            <div class="add_new_item"><a class="iframe_create_branch_company" href="<?php echo Yii::app()->createAbsoluteUrl('admin/branchCompany/create',['ajax'=>1]) ;?>">Tạo Mới Chi Nhánh / Địa Điểm</a><em> (Nếu trong danh sách không có)</em></div>
            <?php echo $form->error($model, 'sale_id'); ?>
        </div>
        <div class="row">
            <?php echo $form->labelEx($model, 'storehouse_id', array('label' => 'Địa điểm con')); ?>
            <?php $mBranchCompany = new BranchCompany(); ?>
            <?php echo $form->dropDownList($model, 'storehouse_id',$model->isNewRecord?[]:$mBranchCompany->getArrBranch('',$model->sale_id), array('class' => 'w-400', 'empty' => 'Select')); ?>
            <?php echo $form->error($model, 'storehouse_id'); ?>
        </div>
        <div class="row">
            <?php echo $form->labelEx($model,'created_by', array('label'=>'Chủ cơ sở')); ?>
            <?php echo $form->dropDownList($model,'created_by', GasConst::getBossAgent(),array('class'=>'w-400', 'empty'=>'Select')); ?>
            <?php echo $form->error($model,'created_by'); ?>
        </div>
       
    
        <div class="row">
            <?php echo $form->labelEx($model,'username', array('label' => "Mã mới của đại lý")); ?>
            <?php echo $form->textField($model,'username',array('class'=>'w-400')); ?>
            <?php echo $form->error($model,'username'); ?>
	</div>		
    
        <div class="row">
            <?php if (!$model->isNewRecord):?>
                <div style="display: none; width: 100%;float: left;padding-bottom: 5px;padding-left: 140px;">
                    <label style="color: red;width: auto; ">Bỏ trống bên dưới nếu bạn không muốn đổi mật khẩu hiện tại</label>
                </div>
            <?php endif?>
            <?php // echo $form->labelEx($model,'password_hash'); ?>
            <?php // echo $form->passwordField($model,'password_hash',array('style'=>'width:500px','maxlength'=>50,'value'=>'')); ?>
            <?php // echo $form->error($model,'password_hash'); ?>
	</div>

	<div class="row">
            <?php echo $form->labelEx($model,'password_confirm'); ?>
            <?php echo $form->passwordField($model,'password_confirm',array('class'=>'w-400','maxlength'=>50,'value'=>'')); ?>
            <?php echo $form->error($model,'password_confirm'); ?>
	</div>

        <div class="row">
            <?php echo $form->labelEx($model,'phone', array('label'=>"Số di động nhận SMS tạo đơn hàng")) ?>
            <?php echo $form->textField($model,'phone',array('class'=>'w-400', 'style'=>'','maxlength'=>100)); ?>
            <?php echo $form->error($model,'phone'); ?>
        </div>
    
        <div class="row">
            <?php echo Yii::t('translation', $form->labelEx($model,'code_account',array('label'=>'Mã Đại Lý'))); ?>
            <?php echo $form->textField($model,'code_account',array('class'=>'w-400','maxlength'=>20)); ?>
            <?php echo $form->error($model,'code_account'); ?>
	</div>	
	
	<div class="row">
            <?php echo Yii::t('translation', $form->labelEx($model,'first_name',array('label'=>'Tên Đại Lý'))); ?>
            <?php echo $form->textField($model,'first_name',array('class'=>'w-400','maxlength'=>150)); ?>
            <?php echo $form->error($model,'first_name'); ?>
	</div>
        <div class="row">
            <?php echo $form->labelEx($model,'agent_name_real', array('label' => 'Tên giấy phép kinh doanh')); ?>
            <?php echo $form->textField($model,'agent_name_real',array('class'=>'w-400')); ?>
            <?php echo $form->error($model,'agent_name_real'); ?>
	</div>	

        <div class="row">
            <?php echo $form->labelEx($model,'code_bussiness', array('label'=>"Phone Máy Bàn")) ?>
            <?php echo $form->textField($model,'code_bussiness',array('class'=>'w-400', 'maxlength'=>100)); ?>
            <?php echo $form->error($model,'code_bussiness'); ?>
        </div>
    
	<div class="row">
            <?php echo Yii::t('translation', $form->labelEx($model,'payment_day',array('label'=>'Ngày Cập Nhật Sổ Quỹ'))); ?>
            <?php echo $form->textField($model,'payment_day',array('maxlength'=>150,'class'=>'number_only w-400')); ?>
            <?php echo $form->error($model,'payment_day'); ?>
	</div>

	<div class="row">
            <?php echo Yii::t('translation', $form->labelEx($model,'address')); ?>
            <?php echo $form->textArea($model,'address',array('readonly'=>1 ,'style'=>'width:400px')); ?>
            <?php echo $form->error($model,'address'); ?>
	</div>
    
	<div class="row">
            <?php echo Yii::t('translation', $form->labelEx($model,'beginning')); ?>
            <div class="fix_number_help">
            <?php echo $form->textField($model,'beginning',array('size'=>30,'maxlength'=>12,'class'=>'number_only')); ?>
            <div class="help_number"></div>
            </div>
            <?php echo $form->error($model,'beginning'); ?>
	</div>
	<div class="clr"></div>
    
    
        <?php if(!$model->isNewRecord):?>
	<div class="row" style="padding-left: 141px;">
            <a class='update_agent' href='<?php echo Yii::app()->createAbsoluteUrl("admin/site/update_agent_employee_maintain",array("agent_id"=>$model->id) );?>'>
                    Cập Nhật Nhân Viên Bảo Trì
            </a><br/>
            <fieldset class='info_fieldset'>
                <legend>Danh Sách Nhân Viên Bảo Trì - NV Phục Vụ Khách Hàng</legend>
                <?php $listMany = GasOneMany::getArrModelOfManyId($model->id, ONE_AGENT_MAINTAIN);?>
                <div class='for_update_maintain fix_inner_filedset'>
                        <ul>
                        <?php if(count($listMany)>0) foreach ($listMany as $key=>$item): ?>
                                <li><?php echo ($key+1).'.  '.($item->many?$item->many->code_bussiness.' - '.$item->many->first_name:'');?></li>
                        <?php endforeach;?>
                        </ul>
                </div>
             </fieldset>
	</div>

	<div class="row" style="padding-left: 141px;">
            <a class='update_agent' href='<?php echo Yii::app()->createAbsoluteUrl("admin/site/update_agent_employee_accounting",array("agent_id"=>$model->id) );?>'>
                    Cập Nhật Nhân Viên Kế Toán
            </a>
            <fieldset class='info_fieldset'>
                <legend>Danh Sách Nhân Viên Kế Toán</legend>
                <?php $listMany = GasOneMany::getArrModelOfManyId($model->id, ONE_AGENT_ACCOUNTING);?>
                <div class='for_update_accounting fix_inner_filedset'>
                    <ul>
                    <?php if(count($listMany)>0) foreach ($listMany as $key=>$item): ?>
                            <li><?php echo ($key+1).'.  '.($item->many?$item->many->code_bussiness.' - '.$item->many->first_name:'');?></li>
                    <?php endforeach;?>
                    </ul>
                </div>			
             </fieldset>		
	</div>
        
	<div class="row" style="padding-left: 141px;">
            <a class='update_agent' href='<?php echo Yii::app()->createAbsoluteUrl("admin/site/update_agent_ccs",array("agent_id"=>$model->id) );?>'>
                    Cập Nhật NV CCS + Chuyên Viên CCS
            </a>
            <fieldset class='info_fieldset'>
                <legend>Danh Sách Nhân Viên CCS</legend>
                <?php $listMany = GasOneMany::getArrModelOfManyId($model->id, ONE_AGENT_CCS);?>
                <div class='for_update_ccs fix_inner_filedset'>
                    <ul>
                    <?php if(count($listMany)>0) foreach ($listMany as $key=>$item): ?>
                            <li><?php echo ($key+1).'.  '.($item->many?$item->many->code_bussiness.' - '.$item->many->first_name:'');?></li>
                    <?php endforeach;?>
                    </ul>
                </div>			
             </fieldset>		
	</div>
        
	<div class="row" style="padding-left: 141px;">
            <a class='update_agent' href='<?php echo Yii::app()->createAbsoluteUrl("admin/site/updateAgentCar",array("agent_id"=>$model->id) );?>'>
                    Cập Nhật Xe Tải
            </a>
            <fieldset class='info_fieldset'>
                <legend>Danh Sách Xe Tải</legend>
                <?php $listMany = GasOneMany::getArrModelOfManyId($model->id, ONE_AGENT_CAR);?>
                <div class='UpdateAgentCar fix_inner_filedset'>
                    <ul>
                    <?php if(count($listMany)>0) foreach ($listMany as $key=>$item): ?>
                            <li><?php echo ($key+1).'.  '.($item->many?$item->many->code_bussiness.' - '.$item->many->first_name:'');?></li>
                    <?php endforeach;?>
                    </ul>
                </div>
             </fieldset>
	</div>
        
	<div class="row" style="padding-left: 141px;">
            <a class='update_agent' href='<?php echo Yii::app()->createAbsoluteUrl("admin/site/updateAgentDriver",array("agent_id"=>$model->id) );?>'>
                    Cập Nhật Tài Xế
            </a>
            <fieldset class='info_fieldset'>
                <legend>Danh Sách Tài Xế</legend>
                <?php $listMany = GasOneMany::getArrModelOfManyId($model->id, ONE_AGENT_DRIVER);?>
                <div class='UpdateAgentDriver fix_inner_filedset'>
                    <ul>
                    <?php if(count($listMany)>0) foreach ($listMany as $key=>$item): ?>
                            <li><?php echo ($key+1).'.  '.($item->many?$item->many->code_bussiness.' - '.$item->many->first_name:'');?></li>
                    <?php endforeach;?>
                    </ul>
                </div>
             </fieldset>
	</div>
        
	<div class="row" style="padding-left: 141px;">
            <a class='update_agent' href='<?php echo Yii::app()->createAbsoluteUrl("admin/site/updateAgentPhuXe",array("agent_id"=>$model->id) );?>'>
                    Cập Nhật Phụ Xe
            </a>
            <fieldset class='info_fieldset'>
                <legend>Danh Sách Phụ Xe</legend>
                <?php $listMany = GasOneMany::getArrModelOfManyId($model->id, ONE_AGENT_PHU_XE);?>
                <div class='UpdateAgentPhuXe fix_inner_filedset'>
                    <ul>
                    <?php if(count($listMany)>0) foreach ($listMany as $key=>$item): ?>
                            <li><?php echo ($key+1).'.  '.($item->many?$item->many->code_bussiness.' - '.$item->many->first_name:'');?></li>
                    <?php endforeach;?>
                    </ul>
                </div>
             </fieldset>
	</div>
        
        <?php endif;?>
    
    
	<div class="more_col">
            <div class="row col1">
                <?php echo Yii::t('translation', $form->labelEx($model,'province_id')); ?>
                <?php echo $form->dropDownList($model,'province_id', GasProvince::getArrAllFix(),array('class'=>'w-400', 'empty'=>'Select')); ?>
                <?php echo $form->error($model,'province_id'); ?>
            </div>
            <div class="row col2 add_new_item float_l">
                <label for="checkCity" >Thuộc Thành Phố</label>
                <input id="checkCity" type="checkbox" name="Users[checkCity]" <?php echo $checkCity ? 'checked' : ''; ?>>
            </div>
	</div>    
        
        <div class="row">
            <?php echo Yii::t('translation', $form->labelEx($model,'district_id')); ?>
            <?php echo $form->dropDownList($model,'district_id', GasDistrict::getArrAll($model->province_id),array('class'=>"float_l w-400")); ?>
            <div class="add_new_item float_l">
                    <a class="iframe_create_district" href="<?php echo Yii::app()->createAbsoluteUrl('admin/ajax/iframe_create_district') ;?>">Tạo Mới Quận Huyện</a><em> (Nếu trong danh sách không có)</em>
            </div>
            <div class="clr"></div>
            <?php echo $form->error($model,'district_id'); ?>
        </div>
        <div class="clr"></div>
        	
        <div class="row">
            <?php echo Yii::t('translation', $form->labelEx($model,'ward_id')); ?>
            <?php echo $form->dropDownList($model,'ward_id', GasWard::getArrAll($model->province_id, $model->district_id),array('class'=>'w-400','empty'=>'Select')); ?>
            <div class="add_new_item"><a class="iframe_create_ward" href="<?php echo Yii::app()->createAbsoluteUrl('admin/ajax/iframe_create_ward') ;?>">Tạo Mới Phường Xã</a><em> (Nếu trong danh sách không có)</em></div>
            <?php echo $form->error($model,'ward_id'); ?>
        </div>

	<div class="row">
            <?php echo $form->labelEx($model,'house_numbers') ?>
            <?php echo $form->textField($model,'house_numbers',array('class'=>'w-400','maxlength'=>100)); ?>
            <?php echo $form->error($model,'house_numbers'); ?>
	</div>
        
        <div class="row">
            <?php echo $form->labelEx($model,'street_id') ?>
            <?php echo $form->hiddenField($model,'street_id',array('style'=>'width:369px','maxlength'=>100)); ?>
            <?php 
                $this->widget('zii.widgets.jui.CJuiAutoComplete', array(
                        'attribute'=>'autocomplete_name_street',
                        'model'=>$model,
                        'sourceUrl'=>Yii::app()->createAbsoluteUrl('admin/ajax/autocomplete_data_streets'),
                        'options'=>array(
                                'minLength'=>MIN_LENGTH_AUTOCOMPLETE,
                                'multiple'=> true,
                                'search'=>"js:function( event, ui ) {
                                        $('#Users_autocomplete_name_street').addClass('grid-view-loading-gas');
                                        } ",
                                'response'=>"js:function( event, ui ) {
										var json = $.map(ui, function (value, key) { return value; });
										if(json.length<1){
											var error = '<div class=\'errorMessage clr autocomplete_name_text\'>Không tìm thấy dữ liệu.</div>';
											if($('.autocomplete_name_text').size()<1)
												$('.autocomplete_name_error').parent('div').find('.add_new_item').after(error);
											else
												$('.autocomplete_name_error').parent('div').find('.autocomplete_name_text').show();
											$('.remove_row_item').hide();
										}
								
                                        $('#Users_autocomplete_name_street').removeClass('grid-view-loading-gas');
                                        } ",
                                'select'=>"js:function(event, ui) {
                                        $('#Users_street_id').val(ui.item.id);
                                        var remove_div = '<span class=\'remove_row_item\' onclick=\'fnRemoveName(this)\'></span>';
                                        $('#Users_autocomplete_name_street').parent('div').find('.remove_row_item').remove();
                                        $('#Users_autocomplete_name_street').attr('readonly',true).after(remove_div);
										$('.autocomplete_name_error').parent('div').find('.autocomplete_name_text').hide();
                                }",
                        ),
                        'htmlOptions'=>array(
                            'class'=>'autocomplete_name_error w-400',
                            'size'=>45,
                            'maxlength'=>45,
                            'style'=>'float:left;',
                            'placeholder'=>'Nhập tên đường tiếng việt không dấu',                            
                        ),
                )); 
                ?> 
                <script>
                    function fnRemoveName(this_){
                        $(this_).parent('div').find('input').attr("readonly",false); 
                        $("#Users_autocomplete_name_street").val("");
                        $("#Users_street_id").val("");
                    }
                </script>             
                <div class="add_new_item"><a class="iframe_create_street" href="<?php echo Yii::app()->createAbsoluteUrl('admin/ajax/iframe_create_street') ;?>">Tạo Mới Đường</a><em> (Nếu trong danh sách không có)</em></div>
            <div class="clr"></div>
            <?php echo $form->error($model,'street_id'); ?>
	</div>
        
        <?php $canUpdate = 1; include "_form_material.php"; ?>

	<div class="row">
            <?php echo Yii::t('translation', $form->labelEx($model,'status')); ?>
            <?php echo $form->dropDownList($model,'status', ActiveRecord::getUserStatus(),array('style'=>'width:500px')); ?>
            <?php echo $form->error($model,'status'); ?>
	</div>

        <div class="row display_none">
		<?php echo Yii::t('translation', $form->labelEx($model,'last_logged_in', array('label'=>'Ngày Đánh PTTT'))); ?>
		<?php // Yii::import('application.extensions.CJuiDateTimePicker.CJuiDateTimePicker');?>
                <?php 
                $this->widget('zii.widgets.jui.CJuiDatePicker', array(
//                $this->widget('CJuiDateTimePicker',array(
                    'model'=>$model,        
                    'attribute'=>'last_logged_in',
                    'options'=>array(
                        'showAnim'=>'fold',
//                        'dateFormat'=> ActiveRecord::getDateFormatJquery(),
                        'dateFormat'=> 'yy-mm-dd',
                        'changeMonth' => true,
                        'changeYear' => true,
                        'showOn' => 'button',
                        'buttonImage'=> Yii::app()->theme->baseUrl.'/admin/images/icon_calendar_r.gif',
                        'buttonImageOnly'=> true,                                
                    ),        
                    'htmlOptions'=>array(
                        'class'=>'',
                        'style'=>'width: 200px;margin-right:10px;',                        
                    ),
                ));
            ?>
            <?php echo $form->error($model,'last_logged_in'); ?>
	</div>
        
        <div class="row">
            <?php echo $form->labelEx($model,'slug', array()) ?>
            <?php echo $form->textField($model,'slug',array('style'=>'width:500px','maxlength'=>100, "placeholder"=>"lat,lng => 10.785964,106.764464")); ?>
            <?php echo $form->error($model,'slug'); ?>
	</div>
        <div class="row">
            <?php echo $form->labelEx($model,'price_other', ['label' => 'Bán kính app Gas24h']) ?>
            <?php echo $form->textField($model,'price_other',array('class'=>'w-200','maxlength'=>100, "placeholder"=> 'bán kính đại lý đv: mét')); ?> m
            <?php echo $form->error($model,'price_other'); ?>
	</div>

	<div class="row buttons" style="padding-left: 141px;">
            <?php $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType'=>'submit',
            'label'=>$model->isNewRecord ? Yii::t('translation', 'Create') : Yii::t('translation', 'Save'),
            'type'=>'null', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
            'size'=>'small', // null, 'large', 'small' or 'mini'
            //'htmlOptions' => array('style' => 'margin-bottom: 10px; float: right;'),
            )); ?>	
        </div>

<?php $this->endWidget(); ?>

</div><!-- form -->

<style>
 .row .info_fieldset { margin-top:15px;width:486px;}
 .row .info_fieldset legend { font-weight:bold;font-size:15px;}
 .row .info_fieldset ul{ list-style:none;}
 .row .info_fieldset ul li{ font-size:15px;}
 .fix_inner_filedset {margin:-17px;}
</style>

<script>
function fnUpdateColorbox(){
    $(".update_agent").colorbox({iframe:true,innerHeight:'500', innerWidth: '1050',close: "<span title='close'>close</span>"});
    $(".iframe_create_district").colorbox({iframe:true,innerHeight:'400', innerWidth: '700',close: "<span title='close'>close</span>"});
    $(".iframe_create_ward").colorbox({iframe:true,innerHeight:'400', innerWidth: '700',close: "<span title='close'>close</span>"});
    $(".iframe_create_street").colorbox({iframe:true,innerHeight:'400', innerWidth: '700',close: "<span title='close'>close</span>"});
    $(".iframe_create_branch_company").colorbox({iframe:true,innerHeight:'500', innerWidth: '900',close: "<span title='close'>close</span>"});
}	

$(document).ready(function() {
fnUpdateColorbox();

    $('#Users_province_id').change(function(){
        var province_id = $(this).val();        
//        if($.trim(gender)==''){
//            $('#Products_category_id').html('<option value="">Select category</option>');            
//            return;
//        }                    
//        var url_ = "<?php echo Yii::app()->createAbsoluteUrl('admin/gascustomer/create');?>";
        var url_ = "<?php echo Yii::app()->createAbsoluteUrl('admin/ajax/get_slt_district');?>";
//		$('#Products_category_id').html('<option value="">Select category</option>');            
    $.blockUI({ overlayCSS: { backgroundColor: '<?php echo BLOCK_UI_COLOR;?>' } }); 
        $.ajax({
            url: url_,
            data: {ajax:1,province_id:province_id},
            type: "get",
            dataType:'json',
            success: function(data){
                $('#Users_district_id').html(data['html_district']);
//                $('#Users_storehouse_id').html(data['html_store']);// DungNT Close Aug0819
                $.unblockUI();
            }
        });
        
    });  
    
    $('#Users_district_id').live('change',function(){
        var province_id = $('#Users_province_id').val();   
        var district_id = $(this).val();        
        var url_ = "<?php echo Yii::app()->createAbsoluteUrl('admin/ajax/get_slt_ward');?>";
        $.blockUI({ overlayCSS: { backgroundColor: '<?php echo BLOCK_UI_COLOR;?>' } }); 
        $.ajax({
                url: url_,
                data: {ajax:1,district_id:district_id,province_id:province_id},
                type: "get",
                dataType:'json',
                success: function(data){
                        $('#Users_ward_id').html(data['html_district']);                
                        $.unblockUI();
                }
        });
    });       
    $('#Users_parent_id').live('change',function(){
        var parent_id = $(this).val();        
        var url_ = "<?php echo Yii::app()->createAbsoluteUrl('admin/ajax/get_slt_branch');?>";
        $.blockUI({ overlayCSS: { backgroundColor: '<?php echo BLOCK_UI_COLOR;?>' } }); 
        $.ajax({
                url: url_,
                data: {ajax:1,parent_id:parent_id},
                type: "get",
                dataType:'json',
                success: function(data){
                        $('#Users_sale_id').html(data['html_branch']); 
                        $('#Users_sale_id').change();
                        $.unblockUI();
                }
        });
        
    });       
    $('#Users_sale_id').live('change',function(){
        var storehouse_id = $(this).val(); 
        console.log(storehouse_id);
        var url_ = "<?php echo Yii::app()->createAbsoluteUrl('admin/ajax/get_slt_address_by_branch');?>";
        $.blockUI({ overlayCSS: { backgroundColor: '<?php echo BLOCK_UI_COLOR;?>' } }); 
        $.ajax({
                url: url_,
                data: {ajax:1,storehouse_id:storehouse_id},
                type: "get",
                dataType:'json',
                success: function(data){
                        $('#Users_storehouse_id').html(data['html_branch']);                
                        $.unblockUI();
                }
        });
    });       
    
})

</script>
