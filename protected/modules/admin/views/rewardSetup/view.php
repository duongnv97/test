<?php
$this->breadcrumbs=array(
    $this->pluralTitle => array('index'),
    ' ' . $this->singleTitle . ' : ',
);

$menus = array(
	array('label'=>"$this->pluralTitle Management", 'url'=>array('index')),
	array('label'=>"Tạo Mới $this->singleTitle", 'url'=>array('create')),
	array('label'=>"Cập Nhật $this->singleTitle", 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>"Xóa $this->singleTitle", 'url'=>array('delete'), 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Bạn chắc chắn muốn xóa ?')),
);
$this->menu= ControllerActionsName::createMenusRoles($menus, $actions);
?>

<h1>Xem: <?php echo $this->singleTitle; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
                array(
                    'name'=>'agent_id',
                    'type'=>'raw',
                    'value'=>$model->getAgent(),
                ),
                array(
                    'name'=>'date_from',
                    'type'=>'raw',
                    'value'=>MyFormat::dateConverYmdToDmy($model->date_from),
                ),
                array(
                    'name'=>'date_to',
                    'type'=>'raw',
                    'value'=>MyFormat::dateConverYmdToDmy($model->date_to),
                ),
                array(
                    'name'=>'status',
                    'type'=>'raw',  
                    'value'=>$model->getStatus(),
                ),
                array(
                    'name'=>'point_reward',
                    'type'=>'raw',  
                    'value'=>$model->getPointReward(),
                ),
                array(
                    'name'=>'count_sell_apply',
                    'type'=>'raw',  
                    'value'=>$model->getCountSellApply(),
                ),
               array(
                    'name'=>'created_date',
                    'type'=>'DateTime',
                    ),
//		'created_by',
	),
)); ?>
