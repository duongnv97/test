<?php
$this->breadcrumbs=array(
	$this->singleTitle,
);

$menus=array(
            array('label'=>"Create $this->singleTitle", 'url'=>array('create')),
);
$this->menu= ControllerActionsName::createMenusRoles($menus, $actions);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('reward-setup-grid', {
                url : $(this).attr('action'),
		data: $(this).serialize()
	});
	return false;
});
");

Yii::app()->clientScript->registerScript('ajaxupdate', "
$('#reward-setup-grid a.ajaxupdate').live('click', function() {
    $.fn.yiiGridView.update('reward-setup-grid', {
        type: 'POST',
        url: $(this).attr('href'),
        success: function() {
            $.fn.yiiGridView.update('reward-setup-grid');
        }
    });
    return false;
});
");
?>

<h1>Danh Sách <?php echo $this->pluralTitle; ?></h1>


<?php echo CHtml::link('Tìm Kiếm Nâng Cao','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'reward-setup-grid',
	'dataProvider'=>$model->search(),
        'afterAjaxUpdate'=>'function(id, data){ fnUpdateColorbox();}',
        'template'=>'{pager}{summary}{items}{pager}{summary}',
//    'itemsCssClass' => 'items custom_here',
//    'htmlOptions'=> array('class'=>'grid-view custom_here'),
        'pager' => array(
            'maxButtonCount'=>  CmsFormatter::$PAGE_MAX_BUTTON,
        ),
	'enableSorting' => false,
	//'filter'=>$model,
	'columns'=>array(
        array(
            'header' => 'S/N',
            'type' => 'raw',
            'value' => '$this->grid->dataProvider->pagination->currentPage * $this->grid->dataProvider->pagination->pageSize + ($row+1)',
            'headerHtmlOptions' => array('width' => '10px','style' => 'text-align:center;'),
            'htmlOptions' => array('style' => 'text-align:center;')
        ),
		array(
                    'name' => 'agent_id',
                    'type' => 'html',
                    'value' => '$data->getAgent()',
                    ),
                array(
                    'name'=> 'date_from',
                    'type' => 'html',
                    'value' => 'MyFormat::dateConverYmdToDmy($data->date_from)',
                    'htmlOptions' => array('style' => 'text-align:center;'),
                    ),
                array(
                    'name' => 'date_to',
                    'type' => 'html',
                    'value' => 'MyFormat::dateConverYmdToDmy($data->date_to)',
                    'htmlOptions' => array('style' => 'text-align:center;'),
                    ),  
                array(
                    'name' => 'status',
                    'type' => 'html',
                    'value' => '$data->getStatus()',
                    'htmlOptions' => array('style' => 'text-align:center;'),
                   ),
                array(
                    'name' => 'point_reward',
                    'type' => 'html',
                    'value' => '$data->getPointReward()',
                    'htmlOptions' => array('style' => 'text-align:center;'),
                   ),
                array(
                    'name' => 'count_sell_apply',
                    'type' => 'html',
                    'value' => '$data->getCountSellApply()',
                    'htmlOptions' => array('style' => 'text-align:center;'),
                   ),
                 array(
                        'name' => 'created_date',
                        'type' => 'Datetime',
                        'htmlOptions' => array('style' => 'text-align:center;'),
                ),
		/*
		'created_by',
		*/
		array(
                    'header' => 'Actions',
                    'class'=>'CButtonColumn',
                    'template'=> ControllerActionsName::createIndexButtonRoles($actions),
                    'buttons'=>array(
                        'update' => array(
                            'visible' => '$data->canUpdate()',
                        ),
                        'delete'=>array(
                            'visible'=> 'GasCheck::canDeleteData($data)',
                        ),
                    ),
		),
	),
)); ?>

<script>
$(document).ready(function() {
    fnUpdateColorbox();
});

function fnUpdateColorbox(){   
    fixTargetBlank();
}
</script>