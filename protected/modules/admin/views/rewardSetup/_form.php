<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'reward-setup-form',
	'enableAjaxValidation'=>false,
        'htmlOptions' => array('enctype' => 'multipart/form-data'),
)); ?>

    <p class="note">Dòng có dấu <span class="required">*</span> là bắt buộc.</p>
    <?php echo MyFormat::BindNotifyMsg(); ?>
    
        		
        
    <div class ='row'>  
        <?php echo $form->labelEx($model,'agent_id'); ?>
        <?php
                       $this->widget('ext.multiselect.JMultiSelect',array(
                             'model'=>$model,
                             'attribute'=>'agent_id',
                             'data'=>Users::getArrUserByRole(ROLE_AGENT),
                             // additional javascript options for the MultiSelect plugin
                             'options'=>array(),
                             // additional style
                             'htmlOptions'=>array('style' => 'width: 600px;'),
                       ));    
                   ?>
        <?php echo $form->error($model,'agent_id'); ?>
    </div>  
    
    <div class="row more_col">
        <div class="col1">
                <?php echo $form->labelEx($model,'date_from'); ?>
                <?php 
                    $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                        'model'=>$model,        
                        'attribute'=>'date_from',
                        'options'=>array(
                            'showAnim'=>'fold',
                            'dateFormat'=> ActiveRecord::getDateFormatJquery(),
            //                            'minDate'=> '0',
//                            'maxDate'=> '1',
                            'changeMonth' => true,
                            'changeYear' => true,
                            'showOn' => 'button',
                            'buttonImage'=> Yii::app()->theme->baseUrl.'/admin/images/icon_calendar_r.gif',
                            'buttonImageOnly'=> true,                                
                        ),        
                        'htmlOptions'=>array(
                            'class'=>'w-16',
                            'style'=>'height:20px;',
                                'readonly'=>'readonly',
                            
                        ),
                    ));
                ?>     		
                <?php echo $form->error($model,'date_from'); ?>
        </div>
            <!--<label>&nbsp;</label>-->
        <div class="col2">
                <?php echo $form->labelEx($model,'date_to'); ?>
                <?php 
                    $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                        'model'=>$model,        
                        'attribute'=>'date_to',
                        'options'=>array(
                            'showAnim'=>'fold',
                            'dateFormat'=> ActiveRecord::getDateFormatJquery(),
//                            'minDate'=> '0',
//                            'maxDate'=> '0',
                            'changeMonth' => true,
                            'changeYear' => true,
                            'showOn' => 'button',
                            'buttonImage'=> Yii::app()->theme->baseUrl.'/admin/images/icon_calendar_r.gif',
                            'buttonImageOnly'=> true,                                
                        ),        
                         'htmlOptions'=>array(
                            'class'=>'w-16',
                            'style'=>'height:20px;',
                                'readonly'=>'readonly',
                        ),
                    ));
                ?>     
                 <?php echo $form->error($model,'date_to'); ?>
        </div>
    </div>

    <div class="row">
            <?php echo $form->labelEx($model, 'status'); ?>
            <?php echo $form->dropDownList($model, 'status', $model->getArrayStatus()); ?>
            <?php echo $form->error($model, 'status'); ?>
    </div>
    
    <div class="row">
        <?php echo $form->labelEx($model,'point_reward'); ?>
        <?php echo $form->textField($model,'point_reward',array('size'=>10,'maxlength'=>10)); ?>
        <?php echo $form->error($model,'point_reward'); ?>
    </div>
    <div class="row">
        <?php echo $form->labelEx($model,'count_sell_apply'); ?>
        <?php echo $form->textField($model,'count_sell_apply'); ?>
        <?php echo $form->error($model,'count_sell_apply'); ?>
    </div>
   
	<div class="row buttons" style="padding-left: 141px;">
		        <?php $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType'=>'submit',
            'label'=>$model->isNewRecord ? 'Create' :'Save',
            'type'=>'null', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
            'size'=>'small', // null, 'large', 'small' or 'mini'
            //'htmlOptions' => array('style' => 'margin-bottom: 10px; float: right;'),
        )); ?>	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->
<script>
    $(document).ready(function(){
        $('.form').find('button:submit').click(function(){
            $.blockUI({ overlayCSS: { backgroundColor: '#fff' } }); 
        });
        
    });
</script>