<?php $aFile =  $model->getFileOnly(); ?>
<h3 class='title-info'>Hình ảnh liên quan</h3>
<div class="row">
    <label>&nbsp</label>
    <div>
        <a href="javascript:void(0);" class="text_under_none item_b" style="line-height:25px" onclick="fnBuildRowImg();">
            <img style="float: left;margin-right:8px;" src="<?php echo Yii::app()->theme->baseUrl;?>/images/add.png"> 
            Thêm Dòng ( Phím tắt F9 )
        </a>
    </div>
</div>
<div class="clr"></div>
<div class='row'>
    <label>&nbsp;</label>
    <table id='tblImg' class="materials_table hm_table">
            <thead>
                <tr>
                    <th class="item_c">#</th>
                    <th class="item_c">Hình ảnh</th>
                    <th class="item_c">Xóa</th>
                </tr>
            </thead>
            <tbody>
                <?php $index = 1; ?>
                <?php 
                if(!empty($aFile)):
                foreach ($aFile as $key => $mFile) : ?>
                    <tr>
                        <td class='item_c order_no'><?php echo $index++; ?></td>
                        <td class='item_c w-300'><?php echo $mFile->getViewImg(); ?></td>
                        <td class='item_c'><span data-id ='<?php echo $mFile->id; ?>' class='remove_icon_only'></span></td>
                    </tr>
                <?php endforeach; 
                endif;?>
                <tr>
                    <td class="item_c order_no"><?php echo $index; ?></td>
                    <td class="item_c">
                        <input name="file_name[]" id="GasSupportCustomerRequest_file_name" type="file" accept="image/*">
                    <td class="item_c last"><span class="remove_icon_only"></span></td>
                </tr>
            </tbody>
    </table>
</div>