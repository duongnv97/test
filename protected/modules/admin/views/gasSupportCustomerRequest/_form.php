<div class="form">
<?php
$mCustomerRequest = new GasSupportCustomerRequest();

$form=$this->beginWidget('CActiveForm', array(
	'id'=>'gas-support-customer-request-form',
	'enableAjaxValidation'=>false,
        'htmlOptions' => array('enctype' => 'multipart/form-data'),
)); ?>
    <p class="note">Dòng có dấu <span class="required">*</span> là bắt buộc.</p>
    <?php echo MyFormat::BindNotifyMsg(); ?>
    
    <div class="row buttons" style="padding-left: 141px;">
		        <?php $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType'=>'submit',
            'label'=>$model->isNewRecord ? 'Create' :'Save',
            'type'=>'null', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
            'size'=>'small', // null, 'large', 'small' or 'mini'
        )); ?>	</div>
    
    <?php if(empty($model->isNewRecord)) : ?>
    <div class="row">
        <?php echo $form->label($model,'code_no')." : "; ?>
        <?php echo $model->code_no  ; ?>
    </div>
    <div class="row">
        <?php echo $form->label($model,'agent_id')." : "; ?>
        <?php echo $model->getAgent()." - ".$model->getAgent('address') ; ?>
    </div>
    <div class="row">
        <?php echo $form->label($model,'sale_id')." : "; ?>
        <?php echo $model->getSale(); ?>
    </div>
    <div class="row">
        <?php echo $form->label($model,'created_date')." : "; ?>
        <?php echo $model->getCreatedDate(); ?>
    </div>
    <div class="row">
        <?php echo $form->label($model,'last_update_by')." : "; ?>
        <?php echo $model->getUserUpdate(); ?>
    </div>
    <div class="row">
        <?php echo $form->label($model,'last_update_time')." : "; ?>
        <?php echo $model->getUpdateDate(); ?>
    </div>
    <div class="row">
        <?php echo $form->labelEx($model,'status'); ?>
        <?php echo $form->dropDownList($model,'status', $model->getArrayStatusForm(),array('class'=>'w-200')); ?>
    </div>
    <?php else: ?>
        <?php echo $form->dropDownList($model,'status', $model->getArrayStatusForm(),array('style'=>'display: none')); ?>
    <?php endif; ?> 
    
    <div class="row">
        <?php echo $form->labelEx($model,'customer_id'); ?>
        <?php echo $form->hiddenField($model,'customer_id', array('class'=>'')); ?>
        <?php
            // 1. limit search kh của sale
            $url = Yii::app()->createAbsoluteUrl('admin/ajax/search_user_by_code');
            // widget auto complete search user customer and supplier
            $aData = array(
                'model'=>$model,
                'field_customer_id'=>'customer_id',
                'url'=> $url,
                'name_relation_user'=>'rCustomer',
                'ClassAdd' => 'w-400',
                'field_autocomplete_name' => 'autocomplete_name_3',
                'placeholder'=>'Nhập mã NV hoặc tên',
            );
            $this->widget('ext.SpaAutocompleteCustomer.SpaAutocompleteCustomer',
                array('data'=>$aData));
            ?>
        <?php echo $form->error($model,'customer_id'); ?>
    </div>
    
    <div class="row">
        <?php echo $form->labelEx($model,'created_by'); ?>
        <?php echo $form->hiddenField($model,'created_by', array('class'=>'')); ?>
        <?php
            // 1. limit search kh của sale
            $url = Yii::app()->createAbsoluteUrl('admin/ajax/search_by_role', array('role'=> implode(',', GasUphold::$ROLE_EMPLOYEE_SEARCH_AUTOCOMPLETE)));
            // widget auto complete search user customer and supplier
            $aData = array(
                'model'=>$model,
                'field_customer_id'=>'created_by',
                'url'=> $url,
                'name_relation_user'=>'rUserCreate',
                'ClassAdd' => 'w-400',
                'field_autocomplete_name' => 'autocomplete_name_2',
                'placeholder'=>'Nhập mã NV hoặc tên',
            );
            $this->widget('ext.SpaAutocompleteCustomer.SpaAutocompleteCustomer',
                array('data'=>$aData));
            ?>
        <?php echo $form->error($model,'created_by'); ?>
    </div>
    
    <div class="row">
        <?php echo $form->label($model,'note').' : '; ?>
        <?php echo $model->getNote(); ?>
    </div>
    
    <div class="row">
        <?php echo $form->labelEx($model,'note_manager'); ?>
        <?php echo $form->textArea($model,'note_manager',array('rows'=>6, 'cols'=>100)); ?>
        <?php echo $form->error($model,'note_manager'); ?>
    </div>
    
    <div class="row">
        <?php echo $form->labelEx($model,'action_invest'); ?>
        <?php echo $form->dropDownList($model,'action_invest', $model->getArrayActionInvest(),array('empty'=>'select','class'=>'w-200')); ?>
        <?php echo $form->error($model,'action_invest'); ?>
    </div>
    <?php 
    include "_form_module.php";
    include "_form_item_image.php";
    ?>

    
    <div class="row buttons" style="padding-left: 141px;">
                    <?php $this->widget('bootstrap.widgets.TbButton', array(
        'buttonType'=>'submit',
        'label'=>$model->isNewRecord ? 'Create' :'Save',
        'type'=>'null', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
        'size'=>'small', // null, 'large', 'small' or 'mini'
    )); ?>	</div>
    
<?php $this->endWidget(); ?>
</div><!-- form -->
<script>
    <?php Yii::app()->clientScript->registerCoreScript('jquery.ui'); ?>
    
    var jsonDataMaterials = <?php echo $mCustomerRequest->formatToJsonMaterialForWeb(); ?>;
    
    $(document).ready(function(){
        $('.form').find('button:submit').click(function(){
            $.blockUI({ overlayCSS: { backgroundColor: '#fff' } }); 
        });
        fnBindRemoveIcon();
        //fnBindRemoveMaterial();
//        bindEventForHelpNumber();
        fnBindAllAutocomplete();
        fnBindRemoveMaterialFix('materials_id_hide', 'material_autocomplete');
        eventChangeValue();
        $(".gallery").colorbox({iframe:true,innerHeight:'1100', innerWidth: '1000',close: "<span title='close'>close</span>"});
    });
    function fnBuildRow(this_){
        $(this_).closest('.itemTable').find('tr:visible:last').next('tr').show();
    }
    
    function fnBuildRowModule(){
        $('.itemMudule').find('.moduleDiv:visible:last').next('.moduleDiv').show();
    }
    
    function fnBindAllAutocomplete(){
        // material
        $('.material_autocomplete').each(function(){
            bindMaterialAutocomplete($(this));
        });
    }
    
    // to do bind autocompelte for input material
    // @param objInput : is obj input ex  $('.customer_autocomplete')    
    function bindMaterialAutocomplete(objInput){
        var availableMaterials = jsonDataMaterials;
        var parent_div = objInput.closest('.col_material');
        objInput.autocomplete({
            source: availableMaterials,
//            close: function( event, ui ) { $( "#GasStoreCard_materials_name" ).val(''); },
            select: function( event, ui ) {
                var item = $(':input[value="'+ui.item.id+'"][class="materials_id_hide"]').val();
                if(item == null){
                    parent_div.find('.materials_id_hide').val(ui.item.id);
                    parent_div.find('.material_autocomplete').attr('readonly',true);
                    fnSetValue(objInput, ui);
                }else{
                    var tr = objInput.closest('tr');
                    tr.find('.col_material').closest('.material_autocomplete').text(''); // fix
                }
            }
        });
    }
//    function fnCheckExist(ui.item.id){
//        var item = $(':input[value="'+ui.item.id+'"][class="materials_id_hide"]').val();
//        if(item == null){
//            return false;
//        }
//        return true;
//    }
        
    function fnSetValue(objInput, ui){
        var tr = objInput.closest('tr');
        var MaterialType = <?php echo CJSON::encode(GasMaterialsType::getAllItem()); ?>;
        tr.find('.items_qty').val(1);
        var type_id = ui.item.materials_type_id;
        tr.find('.items_type').text(MaterialType[type_id]); // ui.item.materials_type_id
        tr.find('.items_type_id').val(type_id);
//        tr.find('.items_qty').trigger("change");
    }
    
        
    function fnBuildRowImg(){
        var rowCount = $('#tblImg tr').length;
        $('#tblImg').append('<tr><td class="item_c order_no">'+ rowCount +'</td><td class="item_c"><input name="file_name[]" id="GasSupportCustomerRequest_file_name" type="file" accept="image/*"><td class="item_c last"><span class="remove_icon_only"></span></td></tr>');
    }
    
    function fnBeforeRemoveIcon($this){
        $id = $($this).data('id');
        $('#tblImg').after('<input style="display:none;" name="GasSupportCustomerRequest[list_id_image][]" id="GasSupportCustomerRequest_list_id_image" value="'+$id+'">');
    }
    
    $(document).keydown(function(e) {
        if(e.which == 119) {
            fnBuildRowModule();
        }
        if(e.which == 120) {
            fnBuildRowImg();
        }
    });
    function eventChangeValue(){
        $('.qtyMudule').on('focusin',function(){
            $(this).data('val', $(this).val());
        }).on('change',function(){
            var prev = $(this).data('val');
            var current = $(this).val();
            $(this).closest('.moduleDiv').find('.items_qty').each(function(){
                var value = $(this).val();
                if(value != ''){
                    prev = prev == '' ? 1 : prev;
                    if((parseFloat(prev).toFixed(1)) <= 0 || (parseFloat(value).toFixed(1)) <= 0){
                        $(this).val(current);
                    }else{
                        $(this).val(parseFloat(value * (current/prev)).toFixed(1));
                    }
                }
            });
        });
//        change module
        $('.module_id').on('change',function(){
            $.blockUI({ overlayCSS: { backgroundColor: '#fff' } }); 
            var divModuleSelect = $(this).closest('.moduleDiv');
            var input_type      = $(divModuleSelect).find('.module_type');
            var input_qty       = $(divModuleSelect).find('.qtyMudule');
            $.ajax({
                url:'<?php echo Yii::app()->createAbsoluteUrl('admin/gasSupportCustomerRequest/create',['AJAX_MODULE'=>1]) ?>',
                type: "get",
                data: {'AJAX_ID':$(this).val()},
                success: function(data){
                    $index = $(divModuleSelect).data('index');
                    var jsonReturn = JSON.parse(data);
                    module_type = jsonReturn['module_type'];
//                    change module_type
                    $(input_type).val(module_type);
                    $(input_qty).val('1');
//                    change detail of mudule
                    $(divModuleSelect).find('.materials_row').each(function(){
                        if($(this).find('.materials_id_hide').val() != ''){
                            $(this).remove();
                        }
                    });
                    var detail_material = jsonReturn['detail'];
                    $(detail_material).each(function(key,$item){
                        strAppen    = '';
                        strAppen    += '<tr class="materials_row  ">';
                        strAppen    += '<td class="item_c order_no">1</td>';
                        strAppen    += '<td class=" col_material w-300">';
                        strAppen    += '<input class="materials_id_hide" name="GasSupportCustomerRequest[json]['+$index+'][detail][materials_id][]" id="GasSupportCustomerRequest_json" type="hidden" value="'+ $item['materials_id']+'">';
                        strAppen    += '<input class="float_l material_autocomplete w-250 ui-autocomplete-input" placeholder="Nhập mã hoặc tên vật tư" maxlength="100" value="'+$item['materials_name']+'" type="text" autocomplete="on" readonly="readonly">';
                        strAppen    += '<span role="status" aria-live="polite" class="ui-helper-hidden-accessible"></span>';
                        strAppen    += '<span onclick="" class="remove_material_js remove_item_material"></span>';
                        strAppen    += '</td>';
                        strAppen    += '<td class="item_c ">';
                        strAppen    += '<span class=" items_type">'+$item['materials_type_name']+'</span>';
                        strAppen    += '<input class="items_type_id"  name="GasSupportCustomerRequest[json]['+$index+'][detail][materials_type_id][]" id="GasSupportCustomerItem_price" type="hidden" value="'+$item['materials_type_id']+'">';
                        strAppen    += '</td>';
                        strAppen    += '<td class="item_c">';
                        strAppen    += '<input class="w-80 items_qty items_calc number_only number_only_v1" maxlength="8" name="GasSupportCustomerRequest[json]['+$index+'][detail][qty][]" id="GasSupportCustomerItem_price" type="text" value="'+$item['qty']+'">';
                        strAppen    += '</td>';
                        strAppen    += '<td class="item_c last"><span class="remove_icon_only"></span></td>';
                        strAppen    += '</tr>';
                        $(divModuleSelect).find('.materials_table').find('tbody').prepend(strAppen);
                    });
                    fnBindAllAutocomplete();
                    fnRefreshOrderNumber();
                    $.unblockUI();
                }
            });
        });
        
    }
    
</script>