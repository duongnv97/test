
<h3 class='title-info'>Lý do hủy</h3>

<div class="form">
<?php
$form=$this->beginWidget('CActiveForm', array(
	'id'=>'calcel-request-form',
	'enableAjaxValidation'=>false,
        'htmlOptions' => array('enctype' => 'multipart/form-data'),
)); ?>
    
    <div class="flash_message_3"></div>
    <div class="row" style="text-align: left;">
        <?php echo $form->label($model,'reason_reject'); ?>
        <?php echo $form->textArea($model,'reason_reject',['cols'=>50, 'rows'=>5]); ?>
    </div>
    
    
    <div class="row buttons" style="text-align: left;">
        <?php $this->widget('bootstrap.widgets.TbButton', array(
        'buttonType'=>'submit',
        'label'=>'Save',
        'type'=>'null', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
        'size'=>'small', // null, 'large', 'small' or 'mini'
        'htmlOptions' =>[
            'class' => 'save-btn',
        ]
        )); ?>	
    </div>
    
<?php $this->endWidget(); ?>
</div><!-- form -->
<script>
    $('#calcel-request-form').on('submit', function(e){
        var c = confirm('Bạn chắc chắn muốn hủy?');
        if(c){
            var form = $(this);
            var url = form.attr('action');
            $.ajax({
                   type: "POST",
                   url: url,
                   data: form.serialize(), // serializes the form's elements.
                   success: function(data)
                   {
                        if(data.indexOf("Lý do không được để trống!") == -1){
                            $('#cboxClose').click();
                            $('.flash_message_2').html(data);
                        } else {
                            // nếu lý do empty thì báo lỗi
                            $('.flash_message_3').html(data);
                        }
                   }
                 });
         }
        return false;
    });
</script>