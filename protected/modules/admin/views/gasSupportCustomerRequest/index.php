<?php

$this->breadcrumbs=array(
	$this->singleTitle,
);

$menus=array(
            array('label'=>"Create $this->singleTitle", 'url'=>array('create')),
);
$this->menu= ControllerActionsName::createMenusRoles($menus, $actions);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('gas-support-customer-request-grid', {
                url : $(this).attr('action'),
		data: $(this).serialize()
	});
	return false;
});
");

Yii::app()->clientScript->registerScript('ajaxupdate', "
$('#gas-support-customer-request-grid a.ajaxupdate').live('click', function() {
    $.fn.yiiGridView.update('gas-support-customer-request-grid', {
        type: 'POST',
        url: $(this).attr('href'),
        success: function() {
            $.fn.yiiGridView.update('gas-support-customer-request-grid');
        }
    });
    return false;
});
");
Yii::app()->clientScript->registerScript('cancelRequest', "
$(document).on('click', '.cancel_request_btn', function(){
    $.colorbox({
        href: $(this).attr('href'),
        innerHeight:'300', 
        innerWidth: '700',
        onClosed: function(){
            $.fn.yiiGridView.update('gas-support-customer-request-grid');
        }
    });
    return false;
});
");
?>
<div class="form">
    <div class="flash_message_2"></div>
<h1>Danh Sách <?php echo $this->pluralTitle; ?>
    <?php include 'index_button.php'; ?>
</h1>
</div>
<?php echo CHtml::link('Tìm Kiếm Nâng Cao','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'gas-support-customer-request-grid',
	'dataProvider'=>$model->search(),
        'afterAjaxUpdate'=>'function(id, data){ fnUpdateColorbox();}',
        'template'=>'{pager}{summary}{items}{pager}{summary}',
//    'itemsCssClass' => 'items custom_here',
//    'htmlOptions'=> array('class'=>'grid-view custom_here'),
        'pager' => array(
            'maxButtonCount'=>  CmsFormatter::$PAGE_MAX_BUTTON,
        ),
	'enableSorting' => false,
	//'filter'=>$model,
	'columns'=>array(
        array(
            'header' => 'S/N',
            'type' => 'raw',
            'value' => '$this->grid->dataProvider->pagination->currentPage * $this->grid->dataProvider->pagination->pageSize + ($row+1)',
            'headerHtmlOptions' => array('width' => '10px','style' => 'text-align:center;'),
            'htmlOptions' => array('style' => 'text-align:center;')
        ),
//            'code_no',
                array(
                        'name' => 'code_no',
                        'type' => 'html',
                        'value' => '$data->getCodeNo()',
                    ),
                array(
                    'name' => 'customer_id',
                    'type' => 'html',
                    'value' => '$data->getCustomer() ."<br><br>".$data->getCustomer(\'address\')."<br><br>".$data->getImageViewGrid()',
//                        'htmlOptions' => array('style' => 'width:250px;'),
                ),
                array(
                    'name' => 'type_customer',
                    'type' => 'html',
                    'value' => '$data->getTypeCustomer()',
//                        'htmlOptions' => array('style' => 'width:100px;'),
                ),
		array(
                    'name' => 'sale_id',
                    'type' => 'html',
                    'value' => '$data->getSale()',
                ),
                array(
                    'name' => 'agent_id',
                    'type' => 'html',
                    'value' => '$data->getAgent()',
                ),
                array(
                    'name' => 'json',
                    'type' => 'html',
                    'value' => '$data->formatJsonProduct()',
                        'htmlOptions' => array('style' => 'width:300px;'),
                ),
                array(
                    'name' => 'note',
                    'type' => 'raw',
                    'value' => '$data->getNote().$data->getHtmlIconGreen()',
                ),
                array(
                    'name' => 'created_by',
                    'type' => 'html',
                    'value' => '$data->getUserCreate()',
                ),
                array(
                    'name' => 'status',
                    'type' => 'html',
                    'value' => '$data->getStatus()',
                ),
                array(
                    'name' => 'action_invest',
                    'type' => 'html',
                    'value' => '$data->getActionInvest()',
                ),
                array(
                    'name' => 'created_date',
                    'type' => 'html',
                    'value' => '$data->getCreatedDate()',
                ),
            
		array(
                    'header' => 'Actions',
                    'class'=>'CButtonColumn',
                    'template'=> ControllerActionsName::createIndexButtonRoles($actions,['generate','view','update','delete']),
                    'buttons'=>array(
                        'update' => array(
                            'visible' => '$data->canUpdate()',
                        ),
                        'generate'=>array(
                            'label'=>'Generate',
                            'imageUrl'=>Yii::app()->theme->baseUrl . '/images/add1.png',
                            'options'=>array('class'=>'generate'),
                            'url'=>'Yii::app()->createAbsoluteUrl("admin/gasSupportCustomerRequest/generate", array("id"=>$data->id) )',
                            'visible' => '$data->canCreateGenerate()',
                        ),
                        'delete'=>array(
                            'visible'=> 'GasCheck::canDeleteData($data)',
                        ),
                    ),
		),
	),
)); ?>

<script>
$(document).ready(function() {
    fnUpdateColorbox();
});

function fnUpdateColorbox(){   
    fixTargetBlank();
    $(".gallery").colorbox({iframe:true,innerHeight:'1100', innerWidth: '1000',close: "<span title='close'>close</span>"});
    $('.AddIconGrid').each(function(){
        var tr = $(this).closest('tr');
        tr.find('.button-column').append($(this).html());
    });
}
</script>