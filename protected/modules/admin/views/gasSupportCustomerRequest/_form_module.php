<h3 class='title-info' style="background-color: teal;margin-bottom: 0;">Danh sách module</h3>
<?php echo $form->error($model,'json'); ?>
<div class="itemMudule">
    <div class="row">
        <div>
            <a href="javascript:void(0);" class="text_under_none item_b" style="line-height:25px" onclick="fnBuildRowModule();">
                <img style="float: left;margin-right:8px;" src="<?php echo Yii::app()->theme->baseUrl;?>/images/add.png"> 
                Thêm Dòng ( Phím tắt F8 )
            </a>
        </div>
    </div>
    <div class="clr"></div>
    <?php 
    $fakeModules = new Modules();
    $index = 0;
    $maxRow = GasSupportCustomerRequest::MAX_ITEM_MODULE;
    ?>
    <?php 
    if(!empty($model->json) && is_array($model->json))
    foreach ($model->json as $itemModule):
        $aItem              = !empty($itemModule['detail']) ? $itemModule['detail'] : [];
        $qtyModule          = !empty($itemModule['qty']) ? $itemModule['qty'] : 0;
        $id_module          = !empty($itemModule['module_id']) ? $itemModule['module_id'] : 0;
        $type_module        = !empty($itemModule['module_type']) ? $itemModule['module_type'] : 0;
    ?>
    <div class="moduleDiv" data-index ='<?php echo $index; ?>' style="padding-left: 40px;background: rgba(0, 0, 0, 0.06);padding-top:15px;padding-bottom:15px;margin-bottom:10px; ">
        <div class="row display_none">
            <?php echo $form->labelEx($model,'module type'); ?>
            <?php echo $form->textField($model,'json['.$index.'][module_type]',['value'=>$type_module, 'class' => 'module_type']); ?>
        </div>
        <div class="row">
            <?php echo $form->labelEx($model,'Module'); ?>
            <?php echo $form->dropdownList($model,'json['.$index.'][module_id]',$fakeModules->getArrAllModule('name'),array('class'=>'w-200 module_id','empty'=>'select','options' => array( $id_module=>array('selected'=>true)))); ?>
        </div>
        <div class="row">
            <?php echo $form->labelEx($model,'Số lượng'); ?>
            <?php echo $form->textField($model,'json['.$index.'][qty]',array('maxlength' => 6,'value'=>$qtyModule,'class'=>'qtyMudule')); ?>
        </div>
        <div class="row">
            <?php include '_form_item.php'; ?>
        </div>
    </div>

    <?php
    $index++;
    endforeach;?>
    <?php for ($index; $index < $maxRow; $index++):
        $class_display = 'display_none';
        $aItem = [];
        if($index <= 0){
            $class_display = '';
        }
    ?>
    <div class="<?php echo $class_display; ?> moduleDiv" style="padding-left: 40px;background: rgba(0, 0, 0, 0.06);padding-top:15px;padding-bottom:15px;margin-bottom:10px; ">
        <div class="row display_none">
            <?php echo $form->labelEx($model,'module type'); ?>
            <?php echo $form->textField($model,'json['.$index.'][module_type]',['class' => 'module_type']); ?>
        </div>
        <div class="row">
            <?php echo $form->labelEx($model,'Module'); ?>
            <?php echo $form->dropdownList($model,'json['.$index.'][module_id]',$fakeModules->getArrAllModule('name'),array('class'=>'w-200 module_id','empty'=>'select')); ?>
        </div>
        <div class="row">
            <?php echo $form->labelEx($model,'Số lượng'); ?>
            <?php echo $form->textField($model,'json['.$index.'][qty]',['maxlength' => 6,'class'=>'qtyMudule']); ?>
        </div>
        <div class="row">
            <?php include '_form_item.php'; ?>
        </div>
    </div>
    <?php endfor; ?>
</div>