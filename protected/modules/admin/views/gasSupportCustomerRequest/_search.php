<div class="wide form">
<?php
$mCustomerRequest = new GasSupportCustomerRequest();

$form=$this->beginWidget('CActiveForm', array(
	'action'=> GasCheck::getCurl(),
	'method'=>'get',
)); ?>

    <div class="row more_col">
	<div class="col1">
            <?php echo $form->label($model,'code_no',array()); ?>
            <?php echo $form->textField($model,'code_no',array('class'=>'w-200','size'=>30,'maxlength'=>30)); ?>
	</div>
        <div class="col2">
            <?php echo $form->label($model,'type_customer',array()); ?>
            <?php echo $form->dropDownList($model,'type_customer',CmsFormatter::$CUSTOMER_BO_MOI,array('class'=>'w-200', 'empty'=>'Select')); ?>
	</div>
        <div class="col3">
            <?php echo $form->label($model,'status',array()); ?>
            <?php echo $form->dropDownList($model,'status',$model->getArrayStatus(),array('class'=>'w-200','empty'=>'Select')); ?>
	</div>
    </div>
	<div class="row">
        <?php echo $form->label($model,'customer_id'); ?>
        <?php echo $form->hiddenField($model,'customer_id', array('class'=>'')); ?>
        <?php
                // 1. limit search kh của sale
                $url = Yii::app()->createAbsoluteUrl('admin/ajax/search_user_by_code');
                // widget auto complete search user customer and supplier
                $aData = array(
                    'model'=>$model,
                    'field_customer_id'=>'customer_id',
                    'url'=> $url,
                    'name_relation_user'=>'rCustomer',
                    'ClassAdd' => 'w-400',
    //                'fnSelectCustomer'=>'fnSelectCustomer',
                );
                $this->widget('ext.SpaAutocompleteCustomer.SpaAutocompleteCustomer',
                    array('data'=>$aData));
                ?>
            <?php echo $form->error($model,'customer_id'); ?>
        </div>
        <div class="row">
        <?php echo $form->labelEx($model,'created_by'); ?>
        <?php echo $form->hiddenField($model,'created_by', array('class'=>'')); ?>
        <?php
                // 1. limit search kh của sale
                $url = Yii::app()->createAbsoluteUrl('admin/ajax/search_by_role');
                // widget auto complete search user customer and supplier
                $aData = array(
                    'model'=>$model,
                    'field_customer_id'=>'created_by',
                    'url'=> $url,
                    'name_relation_user'=>'rUserCreate',
                    'ClassAdd' => 'w-400',
                    'field_autocomplete_name' => 'autocomplete_name_3',
                    'placeholder'=>'Nhập mã NV kinh doanh hoặc tên ',
                );
                $this->widget('ext.SpaAutocompleteCustomer.SpaAutocompleteCustomer',
                    array('data'=>$aData));
                ?>
        </div>
	<div class="row">
        <?php echo $form->labelEx($model,'sale_id'); ?>
        <?php echo $form->hiddenField($model,'sale_id', array('class'=>'')); ?>
        <?php
                // 1. limit search kh của sale
                $url = Yii::app()->createAbsoluteUrl('admin/ajax/search_by_role', array('role'=> implode(',', GasSupportCustomerRequest::$ROLE_EMPLOYEE_SEARCH_AUTOCOMPLETE)));
                // widget auto complete search user customer and supplier
                $aData = array(
                    'model'=>$model,
                    'field_customer_id'=>'sale_id',
                    'url'=> $url,
                    'name_relation_user'=>'rSale',
                    'ClassAdd' => 'w-400',
                    'field_autocomplete_name' => 'autocomplete_name_2',
                    'placeholder'=>'Nhập mã NV kinh doanh hoặc tên ',
                );
                $this->widget('ext.SpaAutocompleteCustomer.SpaAutocompleteCustomer',
                    array('data'=>$aData));
                ?>
        </div>
	<div class="row">
        <?php echo $form->labelEx($model,'agent_id'); ?>
        <?php echo $form->hiddenField($model,'agent_id', array('class'=>'')); ?>
        <?php
                // 1. limit search kh của sale
                $url = Yii::app()->createAbsoluteUrl('admin/ajax/search_by_role', array('role'=> ROLE_AGENT));
                // widget auto complete search user customer and supplier
                $aData = array(
                    'model'=>$model,
                    'field_customer_id'=>'agent_id',
                    'url'=> $url,
                    'name_relation_user'=>'rAgent',
                    'ClassAdd' => 'w-400',
                    'field_autocomplete_name' => 'autocomplete_name_4',
                    'placeholder'=>'Nhập mã hoặc tên đại lý',
                );
                $this->widget('ext.SpaAutocompleteCustomer.SpaAutocompleteCustomer',
                    array('data'=>$aData));
                ?>
            <?php echo $form->error($model,'agent_id'); ?>
        </div>
        <div class="row col_material">
            <?php echo $form->label($model,'json',array()); ?>
            <input class="materials_id_hide" name="GasSupportCustomerRequest[json]" id="GasSupportCustomerRequest_json" type="hidden">                    
            <input class="float_l material_autocomplete w-250 " placeholder="Nhập mã hoặc tên vật tư" maxlength="100" value="" type="text" autocomplete="on">
            <span onclick="" class="remove_material_js remove_item_material"></span>
        </div>

	<div class="row buttons" style="padding-left: 159px;">
		        <?php $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType'=>'submit',
            'label'=>'Search',
            'type'=>'null', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
            'size'=>'small', // null, 'large', 'small' or 'mini'
            //'htmlOptions' => array('style' => 'margin-bottom: 10px; float: right;'),
        )); ?>	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->

<script>
    <?php Yii::app()->clientScript->registerCoreScript('jquery.ui'); ?>

    $(document).ready(function(){
//        $('.form').find('button:submit').click(function(){
//            $.blockUI({ overlayCSS: { backgroundColor: '#fff' } }); 
//        });
        fnBindRemoveIcon();
        //fnBindRemoveMaterial();
//        bindEventForHelpNumber();
        fnBindAllAutocomplete();
        fnBindRemoveMaterialFix('materials_id_hide', 'material_autocomplete');
    });
    function fnBuildRow(){
        $('.g_tb1 tbody').find('tr:visible:last').next('tr').show();
    }
    
    function fnBindAllAutocomplete(){
        // material
        $('.material_autocomplete').each(function(){
            bindMaterialAutocomplete($(this));
        });
    }
    
    // to do bind autocompelte for input material
    // @param objInput : is obj input ex  $('.customer_autocomplete')    
    function bindMaterialAutocomplete(objInput){
        var availableMaterials = <?php echo $mCustomerRequest->formatToJsonMaterialForWeb(); //MyFunctionCustom::getMaterialsJsonByType(GasMaterialsType::$THIET_BI); ?>;
        var parent_div = objInput.closest('.col_material');
        objInput.autocomplete({
            source: availableMaterials,
//            close: function( event, ui ) { $( "#GasStoreCard_materials_name" ).val(''); },
            select: function( event, ui ) {
                parent_div.find('.materials_id_hide').val(ui.item.id);
                parent_div.find('.material_autocomplete').attr('readonly',true);
//                fnSetValue(objInput, ui);
            }
        });
    }
    function fnSetValue(objInput, ui){
        var tr = objInput.closest('tr');
        var MaterialType = <?php echo CJSON::encode(GasMaterialsType::getAllItem()); ?>;
        tr.find('.items_qty').val(1);
        var type_id = ui.item.materials_type_id;
        tr.find('.items_type').text(MaterialType[type_id]); // ui.item.materials_type_id
        tr.find('.items_type_id').val(type_id);
//        tr.find('.items_qty').trigger("change");
    }
</script>