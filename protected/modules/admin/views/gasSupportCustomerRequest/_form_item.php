<h4 class='title-info'>Chi tiết vật tư</h4>
<div class="itemTable">
    <div class="row">
        <label>&nbsp</label>
        <div>
            <a href="javascript:void(0);" class="text_under_none item_b" style="line-height:25px" onclick="fnBuildRow(this);">
                <img style="float: left;margin-right:8px;" src="<?php echo Yii::app()->theme->baseUrl;?>/images/add.png"> 
                Thêm vật tư
            </a>
        </div>
    </div>
    <div class="clr"></div>
    <div class="row">
        <label>&nbsp</label>
        <table class="materials_table hm_table g_tb1 WrapRowQtyPriceAmount">
            <thead>
                <tr>
                    <th class="item_c">#</th>
                    <th class="item_code item_c">Tên thiết bị</th>
                    <th class="item_code item_c">Loại thiết bị</th>

                    <th class="item_c">Số lượng</th>
                    <!--<th class="item_c">Loại</th>-->
                    <th class="item_unit last item_c">Xóa</th>
                </tr>
            </thead>
            <tbody>
                <?php 
                    $max_upload = GasSupportCustomerRequest::MAX_ITEM;
                    $max_upload_show = 3;
                    $aMaterialType = GasMaterialsType::getAllItem();
                    $stt = 1;
                ?>
                <?php if(count($aItem)):?>
                <?php foreach($aItem as $key => $item):?>
                <?php
                $mGasMaterials = GasMaterials::model()->findByPk($item['materials_id']);
                ?>
                <tr class="materials_row  ">
                    <td class="item_c order_no"><?php echo $stt; ?></td>
                    <td class=" col_material w-500">
                        <input class="materials_id_hide" name="GasSupportCustomerRequest[json][<?php echo $index; ?>][detail][materials_id][]" id="GasSupportCustomerRequest_json" type="hidden" value="<?php echo $item['materials_id']; ?>">    
                        <input class="float_l material_autocomplete w-450 ui-autocomplete-input" placeholder="Nhập mã hoặc tên vật tư" maxlength="100" value="<?php echo $mGasMaterials->getName(); ?>" type="text" autocomplete="on" readonly="readonly">


                        <span role="status" aria-live="polite" class="ui-helper-hidden-accessible"></span>
                        <span onclick="" class="remove_material_js remove_item_material"></span>
                    </td>
                    <td class="item_c ">
                        <span class=" items_type"><?php echo $aMaterialType[$item['materials_type_id']]; ?></span>
                        <input class="items_type_id"  name="GasSupportCustomerRequest[json][<?php echo $index; ?>][detail][materials_type_id][]" id="GasSupportCustomerItem_price" type="hidden" value="<?php echo $item['materials_type_id']; ?>">                    <!--<div class="help_number"></div>-->

                    </td>
                    <td class="item_c">
                        <input maxlength="6" class="w-50 items_qty items_calc number_only number_only_v1" maxlength="8" name="GasSupportCustomerRequest[json][<?php echo $index; ?>][detail][qty][]" id="GasSupportCustomerItem_price" type="text" value="<?php echo $item['qty']; ?>">                    <!--<div class="help_number"></div>-->
                    </td>

                    <td class="item_c last"><span class="remove_icon_only"></span></td>
                </tr>
                <?php $stt++; endforeach;?>
                <?php endif;?>
                <?php for($i=1; $i<=($max_upload-count($aItem)); $i++): ?>
                <?php $display = ""; 
                    if($i>$max_upload_show)
                        $display = "display_none";
                ?>
                <tr class="materials_row RowQtyPriceAmount <?php echo $display;?>">
                    <td class="item_c order_no"><?php echo $stt; ?></td>
                    <td class=" col_material w-500">
                        <input class="materials_id_hide" name="GasSupportCustomerRequest[json][<?php echo $index; ?>][detail][materials_id][]" id="GasSupportCustomerRequest_json" type="hidden">                    
                        <input class="float_l material_autocomplete w-450 " placeholder="Nhập mã hoặc tên vật tư" maxlength="100" value="" type="text" autocomplete="on">
                        <span role="status" aria-live="polite" class="ui-helper-hidden-accessible"></span>
                        <span onclick="" class="remove_material_js remove_item_material"></span>
                    </td>
                    <td class="item_c ">
                        <span class=" items_type"></span>
                        <input class="items_type_id "  name="GasSupportCustomerRequest[json][<?php echo $index; ?>][detail][materials_type_id][]" id="GasSupportCustomerItem_price" type="hidden">                    <!--<div class="help_number"></div>-->
                    </td>
                    <td class="item_c">
                        <input maxlength="6" class="w-50 items_qty items_calc number_only number_only_v1" maxlength="8" name="GasSupportCustomerRequest[json][<?php echo $index; ?>][detail][qty][]"  type="text">                    <!--<div class="help_number"></div>-->
                    </td>

                    <td class="item_c last"><span class="remove_icon_only"></span></td>
                </tr>
                <?php $stt++; endfor;?> 
            </tbody>
            <tfoot>

            </tfoot>
        </table>
    </div>
</div>

