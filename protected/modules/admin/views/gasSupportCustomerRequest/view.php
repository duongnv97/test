<?php
$this->breadcrumbs=array(
    $this->pluralTitle => array('index'),
    ' ' . $this->singleTitle . ' : ',
);

$menus = array(
	array('label'=>"$this->pluralTitle Management", 'url'=>array('index')),
	array('label'=>"Tạo Mới $this->singleTitle", 'url'=>array('create')),
	array('label'=>"Cập Nhật $this->singleTitle", 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>"Xóa $this->singleTitle", 'url'=>array('delete'), 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Bạn chắc chắn muốn xóa ?')),
);
$this->menu= ControllerActionsName::createMenusRoles($menus, $actions);
?>

<h1>Xem: <?php echo $this->singleTitle; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'code_no',
                array(
                    'name' => 'customer_id',
                    'value' => $model->getCustomer() ."<br><br>".$model->getCustomer('address'),
//                        'htmlOptions' => array('style' => 'width:250px;'),
                ),
                array(
                    'name' => 'type_customer',
                    'value' => $model->getTypeCustomer(),
//                        'htmlOptions' => array('style' => 'width:100px;'),
                ),
		array(
                    'name' => 'sale_id',
                    'value' => $model->getSale(),
                ),
                array(
                    'name' => 'agent_id',
                    'value' => $model->getAgent(),
                ),
                array(
                    'name' => 'json',
                    'type' => 'html',
                    'value' => $model->formatJsonProduct(),
                ),
                array(
                    'name' => 'note',
                    'value' => $model->getNote(),
                ),
                array(
                    'name' => 'created_by',
                    'value' => $model->getUserCreate(),
                ),
                array(
                    'name' => 'status',
                    'value' => $model->getStatus(),
                ),
                array(
                    'name' => 'action_invest',
                    'value' => $model->getActionInvest(),
                ),
                array(
                    'name' => 'created_date',
                    'value' => $model->getCreatedDate(),
                ),
                array(
                    'name' => 'File đính kèm',
                    'type' => 'html',
                    'value' => $model->getViewImg(),
                ),
	),
)); ?>

<script>
    $(".gallery").colorbox({iframe:true,innerHeight:'1500', innerWidth: '1050',close: "<span title='close'>close</span>"});
</script>