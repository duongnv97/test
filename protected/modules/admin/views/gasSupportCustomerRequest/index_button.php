<?php
    $linkNormal     = Yii::app()->createAbsoluteUrl('admin/gasSupportCustomerRequest/index');
    $linkConfirm    = Yii::app()->createAbsoluteUrl('admin/gasSupportCustomerRequest/index', array( 'statusSearch'=> GasSupportCustomerRequest::STATUS_APPROVED));
    $linkCancel     = Yii::app()->createAbsoluteUrl('admin/gasSupportCustomerRequest/index', array( 'statusSearch'=> GasSupportCustomerRequest::STATUS_REJECT));
    $linkCreatePgnv = Yii::app()->createAbsoluteUrl('admin/gasSupportCustomerRequest/index', array( 'statusSearch'=> GasSupportCustomerRequest::STATUS_CREATE_PGNV));
?>
<a class='btn_cancel f_size_14 <?php echo (!isset($_GET['statusSearch'])) ? "active":"";?>' href="<?php echo $linkNormal;?>">Mới</a>
<a class='btn_cancel f_size_14 <?php echo isset($_GET['statusSearch']) && $_GET['statusSearch'] == GasSupportCustomerRequest::STATUS_APPROVED ? "active":"";?>' href="<?php echo $linkConfirm;?>">Đã duyệt</a>
<a class='btn_cancel f_size_14 <?php echo isset($_GET['statusSearch']) && $_GET['statusSearch'] == GasSupportCustomerRequest::STATUS_CREATE_PGNV ? "active":"";?>' href="<?php echo $linkCreatePgnv;?>">Đã tạo PGNV</a>
<a class='btn_cancel f_size_14 <?php echo isset($_GET['statusSearch']) && $_GET['statusSearch'] == GasSupportCustomerRequest::STATUS_REJECT ? "active":"";?>' href="<?php echo $linkCancel;?>">Huỷ bỏ</a>