<?php
$mCallReview = new CallReview();

$this->breadcrumbs=array(
    $this->pageTitle,
);
$menus=[];
$this->menu= ControllerActionsName::createMenusRoles($menus, $actions);

Yii::app()->clientScript->registerScript('search1', "
$('.searchCallHistory').click(function(){
	$('.search-form-call-w').toggle();
	return false;
});
$('.search-form-call-w form').submit(function(){
	$.fn.yiiGridView.update('list-call-grid', {
                url : $(this).attr('action'),
		data: $(this).serialize()
	});
	return false;
});
");
Yii::app()->clientScript->registerScript('ajaxupdate', "
$('#list-call-grid a.ajaxupdate').live('click', function() {
    $.fn.yiiGridView.update('list-call-grid', {
        type: 'POST',
        url: $(this).attr('href'),
        success: function() {
            $.fn.yiiGridView.update('list-call-grid');
        }
    });
    return false;
});
");
?>
<h1><?php echo $this->pageTitle;?></h1>
<?php include 'index_button.php'; ?>
<?php echo MyFormat::BindNotifyMsg(); ?>
<?php echo CHtml::link('Tìm Kiếm Nâng Cao','javascript:;',array('class'=>'searchCallHistory')); ?>
<div class="search-form search-form-call-w " style="">
<?php include '_search.php';?>
</div><!-- search-form -->

<div class="wide form">
    <?php $form=$this->beginWidget('CActiveForm', array(
            'id'=>'customer-form',
            'enableAjaxValidation'=>false,
            'htmlOptions' => array('enctype' => 'multipart/form-data'),
    )); ?>
<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'list-call-grid',
	'dataProvider'=>$mCall->search(),
        'afterAjaxUpdate'=>'function(id, data){ fnUpdateColorbox();}',
        'template'=>'{pager}{summary}{items}{pager}{summary}',
//    'itemsCssClass' => 'items custom_here',
//    'htmlOptions'=> array('class'=>'grid-view custom_here'),
        'pager' => array(
            'maxButtonCount'=>  CmsFormatter::$PAGE_MAX_BUTTON,
        ),
        'enableSorting' => false,
	'columns'=>array(
        array(
            'header' => 'S/N',
            'type' => 'raw',
            'value' => '$this->grid->dataProvider->pagination->currentPage * $this->grid->dataProvider->pagination->pageSize + ($row+1)',
            'headerHtmlOptions' => array('width' => '10px','style' => 'text-align:center;'),
            'htmlOptions' => array('style' => 'text-align:center;'),
        ),
        array(
            'class'=>'CCheckBoxColumn',
            'selectableRows'=>2,
            'id'=>'Call[id]',
//            'htmlOptions' => array(
//                        'visible' => 'true',
//                        'class'=>'hidden',
//                    ),
//                            'disabled'=>'$data->canModify()',
        ),
        array(
            'name'=>'direction',
//            'value'=>'$data->getDirection()."<br>".$data->getStartTime()."<br>".$data->call_uuid',
            'value'=>'$data->getDirection(true)."<br>".$data->getStartTime()',
            'type'=>'raw',
        ),
        array(
            'name'=>'caller_number',
            'type'=>'raw',
            'value'=>'$data->getCallNumberView()',
        ),
        array(
            'visible'=> MyFormat::getCurrentRoleId()!=ROLE_TEST_CALLL,
            'name'=>'destination_number',
            'type'=>'raw',
            'value'=>'$data->getDestinationNumberView()',
            'htmlOptions' => array('style' => 'text-align:center;'),
        ),
        array(
            'header'=>'Giây gọi',
            'type'=>'raw',
            'value'=>'$data->getBillDuration()',
            'htmlOptions' => array('style' => 'text-align:center;'),
        ),
        array(
//            'visible'=> MyFormat::getCurrentRoleId()!=ROLE_TEST_CALLL,
            'name'=>'user_id',
            'type'=>'raw',
            'value'=>'$data->getUser()',
        ),
        array(
            'visible'=> MyFormat::getCurrentRoleId()!=ROLE_TEST_CALLL,
            'name'=>'agent_id',
            'type'=>'raw',
            'value'=>'$data->getAgentGrid()',
        ),
        array(
            'visible'=> MyFormat::getCurrentRoleId()!=ROLE_TEST_CALLL,
            'name'=>'customer_id',
            'type'=>'raw',
            'value'=>'"<b>".$data->getCustomer()."</b><br>".$data->getCustomer("address")',
        ),
        array(
            'visible'=> MyFormat::getCurrentRoleId()!=ROLE_TEST_CALLL,
            'header'=>'Tạo bán hàng',
            'value'=>'$data->getUrlMakeSell()',
            'type'=>'raw',
            'htmlOptions' => array('style' => 'text-align:center;'),
        ),
        array(
            'visible'=> MyFormat::getCurrentRoleId()!=ROLE_TEST_CALLL,
            'header'=>'Status',
            'value'=>'$data->getCallStatus()',
            'htmlOptions' => array('style' => 'text-align:center;'),
        ),
        array(
            'visible'=> MyFormat::getCurrentRoleId()!=ROLE_TEST_CALLL,
            'header'=>'Trạng thái',
            'type'=>'raw',
            'value'=>'$data->getFailedStatus()',
            'htmlOptions' => array('style' => 'text-align:center;'),
        ),
        array(
//            'visible'=> MyFormat::getCurrentRoleId()!=ROLE_TEST_CALLL,
            'header'=>'Ghi chú',
            'type'=>'raw',
            'value'=>'$data->getFailedNote()',
            'htmlOptions' => array('style' => 'text-align:center;'),
        ),
        
        array(
            'header'=>'Nghe Lại',
            'type'=>'raw',
            'htmlOptions' => array('style' => 'text-align:center;'),
            'value'=>'$data->getUrlPlayAudio()',
        ),
        array(
            'name'=> 'created_date',
            'type'=> 'html',
            'value'=> '$data->getCreatedDate()."<br>(".$data->id.")<br>". $data->call_uuid',
            'htmlOptions' => array('style' => 'width:50px;'),
        ),
        array(
            'header'=>'Chấm điểm',
            'type'=>'raw',
            'value'=>'$data->getScore()',
            'htmlOptions' => array('style' => 'text-align:center;'),
        ),
        array(
            'header' => 'Chi tiết lỗi',
            'type' => 'html',
            'value' => '$data->getViewReasonCallReview()',
             'htmlOptions' => array('style' => 'width:110px;'),
        ),
        array(
            'header'=>'NV kiểm tra',
            'type'=>'raw',
            'value'=>'$data->getUserCheckCall()',
            'htmlOptions' => array('style' => 'text-align:center;'),
        ),
        array(
            'name'=>'Actions',
            'type'=>'raw',
            'value'=>'$data->getActions()',
            'htmlOptions' => array('style' => 'width:100px;'),
        ),

	),
)); ?>
    <?php // if(!isset($_GET['type']) || $model->canUpdateApply()): ?>
    <?php if(!isset($_GET['type']) && $mCallReview->canPickCallReview()): ?>
        <div class="row more_col BoxApply">
            <div class="col1">
                <?php  echo $form->labelEx($mCall,'user_check_call', ['label'=>'Nhân viên nhận kiểm tra']); ?>
                <?php  echo $form->dropDownList($mCall,'user_check_call', $mCallReview->getArrayEmployee(), ['empty'=>'Xóa nhân viên Telesale hiện tại','class'=>'w-200']); ?>
            </div>
        </div>
        <div class="row more_col">
            <div class="col1" style="float: left; margin: 0 20px 0 0;">
                <?php $this->widget('bootstrap.widgets.TbButton', array(
                'buttonType'=>'submit',
                'label'=> 'Apply',
                'type'=>'null', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
                'size'=>'small', // null, 'large', 'small' or 'mini'
                'htmlOptions' => array('style' => 'margin-bottom: 10px; float: right;'),
                )); ?>	
            </div>
            <?php // if($model->canDelete()): ?>
            <?php // if($mTelesale->canDelete() || $cUid == GasConst::UID_VEN_NTB): ?>
                <!--<div class="col2">-->
                    <?php // $this->widget('bootstrap.widgets.TbButton', array(
//                    'buttonType'=>'button',
//                    'label'=> 'Enable Remove Sale Employee',
//                    'type'=>'null', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
//                    'size'=>'small', // null, 'large', 'small' or 'mini'
//                    'htmlOptions' => array(
//                        'visible' => $model->canDelete(),
//                        'class'=>'enableRmSaleIdBtn'
//                    ),
//                    )); ?>	
                <!--</div>-->
            <?php // endif; ?>
        </div>
    <?php endif; ?>
    <?php $this->endWidget(); ?>
</div>
<script>
$(document).ready(function() {
    fnUpdateColorbox();
    bindChangeAgent();
});

function fnUpdateColorbox(){
    fnShowhighLightTr();
    fixTargetBlank();
    $("#Call_id_all").click(function(){
        $('input:checkbox:enabled[name="Call[id][]"]').not(this).prop('checked', this.checked);
    });
//    $(".createDetail").colorbox({iframe:true,innerHeight:'550', innerWidth: '850',close: "<span title='close'>close</span>"});
    $('.PlayAudioPbxIcon').click(function(){
        var td = $(this).closest('td');
        td.find('.PlayAudioPbxDiv').toggle();
    });
    $(".targetHtml").colorbox({
        iframe: true,
        overlayClose: false, escKey: true,
        innerHeight: '1000',
        innerWidth: '900', close: "<span title='close'>close</span>",
        onClosed: function () { // update view when close colorbox
            $.fn.yiiGridView.update('list-call-grid');
        }
    });
    $(".targetDeny").colorbox({
        iframe: true,
        overlayClose: false, escKey: true,
        innerHeight: '250',
        innerWidth: '400', close: "<span title='close'>close</span>",
        onClosed: function () { // update view when close colorbox
            $.fn.yiiGridView.update('list-call-grid');
        }
    });
    
}

function bindChangeAgent(){
    $('body').on('click', '.ShowChangeAgent', function(){
        $(this).hide();
        var td = $(this).closest('td');
        td.find('.WrapChangeAgentDropdown').show();
    });
    $('body').on('click', '.CancelChangeAgent', function(){
        var td = $(this).closest('td');
        td.find('.WrapChangeAgentDropdown').hide();
        td.find('.ShowChangeAgent').show();
    });
    $('body').on('click', '.SaveChangeAgent', function(){
        var td = $(this).closest('td');
        var tr = $(this).closest('tr');
        var agent_id    = td.find('.SelectAgentId').val();
        var status      = td.find('.ChangeAgentValue').val();
        var note        = td.find('.ChangeAgentNote').val();
        var score       = td.find('.ChangeValueScore').val();
        
//        if($.trim(status) == ''){
//            alert("Chưa chọn phân loại");
//            return false;
//        }
//        if($.trim(agent_id) == ''){
//            alert("Chưa chọn đại lý");
//            return false;
//        }
        
        var url_        = $(this).attr('next');
        var typeText    = td.find('.ChangeAgentValue').find('option:selected').text();
        var agentName   = td.find('.SelectAgentId').find('option:selected').text();
        console.log(agentName);
        $.blockUI({ overlayCSS: { backgroundColor: '#fff' } }); 
        $.ajax({
            url:url_,
            type: 'post',
//            dataType: 'json',
            data: {type_call_test:status, note:note, agent_id:agent_id, score:score },
            success: function(data){
                td.find('.TypeCallTestText').text(typeText);
                td.find('.WrapChangeAgentDropdown').hide();
                td.find('.ShowChangeAgent').show();
                
                tr.find('.AgentGridName').text(agentName);
                tr.find('.CallNoteText').text(note);
                $.unblockUI();
            }
        });
    });
}

</script>
<!--
<audio controls>
  <source src="horse.ogg" type="audio/ogg">
  <source src="https://apps.worldfone.vn/externalcrm/playback2.php?calluuid=1491879194.2751&secrect=1923d10f88daae995b1f461c04f15165&version=3" type="audio/mpeg">
Your browser does not support the audio element.
</audio>
-->
