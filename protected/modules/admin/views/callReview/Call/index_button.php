<div class="form">
    <?php
        $LinkNormal         = Yii::app()->createAbsoluteUrl('admin/callReview/listCall');
        $LinkRECEIVED       = Yii::app()->createAbsoluteUrl('admin/callReview/listCall', array( 'type'=> CallReview::RECEIVED   ));
        $LinkTESTED         = Yii::app()->createAbsoluteUrl('admin/callReview/listCall', array( 'type'=> CallReview::TESTED ));
    ?>
    <h1><?php echo $this->adminPageTitle;?>
        <a class='btn_cancel f_size_14 <?php echo (!isset($_GET['type'])) ? "active":"";?>' href="<?php echo $LinkNormal;?>">Chưa nhận</a>
        <a class='btn_cancel f_size_14 <?php echo isset($_GET['type']) && $_GET['type']== CallReview::RECEIVED ? "active":"";?>' href="<?php echo $LinkRECEIVED;?>">Đã nhận</a>
        <a class='btn_cancel f_size_14 <?php echo isset($_GET['type']) && $_GET['type']== CallReview::TESTED ? "active":"";?>' href="<?php echo $LinkTESTED;?>">Đã kiểm tra</a>
    </h1> 
</div>