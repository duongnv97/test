<?php
$aRes = $data['data']; 
$toTalSum = $data['toTalSum'];
$aDay = $data['aDay'];
$aSumCol = $data['aSumCol'];
$aSumRow = $data['aSumRow'];
?>
<div class="grid-view display_none">
    <table id="freezetablecolumns_report_call" class="items hm_table freezetablecolumns">
        <thead>
        <tr style="">
            <th class="w-20 ">STT</th>
            <th class="w-160 ">Họ tên người kiểm tra</th>
            <th class="w-100 ">Tổng ghi âm</th>
            <?php foreach ($aDay as $date): ?>
                <th class="w-60 "><?php echo substr($date, -2); ?></th>
            <?php endforeach;?>
        </tr>
        </thead>
        <tbody>
            <tr>
                <td class="item_c"></td>
                <td class="item_c"></td>
                <td class="item_b item_c"><?php echo $toTalSum; ?></td>
                <?php foreach ($aDay as $date): ?>
                    <td class="item_c"><?php echo (isset($aSumCol[$date])) ? $aSumCol[$date] : ' '; ?></td>
                <?php endforeach;?>
            </tr>
            <?php foreach ($aSumRow as $key => $sumRow): 
                $stt++; 
                $model = Users::model()->findByPk($key);
            ?>
            <tr>
                <td class="item_c"><?php echo $stt; ?></td>
                <td class=""><?php echo isset($model) ? $model->first_name :''; ?></td>
                <td class="item_c"><?php echo $sumRow ; ?></td>
                 <?php foreach ($aDay as $date): ?>
                    <td class="item_c">
                        <?php 
                        echo (isset($aRes[$key][$date])) ? $aRes[$key][$date] : ' ';
                        ?>
                    </td>
                <?php endforeach;?>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
</div>