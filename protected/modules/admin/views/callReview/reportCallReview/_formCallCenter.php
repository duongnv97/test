<?php
$list = $data['data']; 
$aSumColReason = $data['aSumColReason'];
$aSumRowReason = $data['aSumRowReason'];
$aSumRowApp    = $data['aSumRowApp'];
$aScore        = $data['aScore'];
$aReason = $model->getArrayReasonForView();
$TotalSumRowApp = array_sum($aSumRowApp);
$TotalSumRowReason = array_sum($aSumRowReason);
?>
<div class="grid-view display_none">
    <b style='color:red'>Ghi chú : Số lỗi / % trên tổng số lỗi / % trên tổng số ghi âm.</b>
    <br><br>
    <table id="freezetablecolumns_report_call_call_center" class="items hm_table freezetablecolumns">
        <thead>
        <tr style="">
            <th class="w-20 ">STT</th>
            <th class="w-150 ">Họ tên NVTĐ</th>
            <th class="w-40 ">Ghi âm</th>
            <th class="w-40 ">Lỗi</th>
            <th class="w-50 ">ĐTB</th>
            <?php foreach ($aReason as $key => $value): ?>
            <th class="w-160"><?php echo $value; ?></th>
            <?php endforeach; ?>
        </tr>
        </thead>
        <tbody>
            <tr>
                <td class="item_c"></td>
                <td class="item_b item_c">Tổng</td>
                <td class="item_b item_c"><?php echo $TotalSumRowApp; ?></td>
                <td class="item_b item_c"><?php echo $TotalSumRowReason; ?></td>
                <td class="item_b item_c"></td>
                <?php foreach ($aReason as $key => $value): ?>
                    <td class="item_b item_c"><?php echo isset($aSumColReason[$key]) ? $aSumColReason[$key] : ''; ?></td>
                <?php endforeach; ?>
            </tr>
            <?php foreach ($list as $key => $value): 
                $stt++; 
                $model = Users::model()->findByPk($key);
            ?>
            <tr>
                <td class="item_c"><?php echo $stt; ?></td>
                <td class=""><?php echo isset($model) ? $model->first_name :''; ?></td>
                <td class="item_c"><?php echo $aSumRowApp[$key]; ?></td>
                <td class="item_c"><?php echo isset($aSumRowReason[$key]) ? $aSumRowReason[$key] : ''; ?></td>
                <td class="item_c"><?php echo isset($aScore[$key]) ? number_format($aScore[$key] , 2) : ''; ?></td>
                <?php foreach ($aReason as $id => $reasonName): ?>
                    <td class="item_c">
                    <?php 
                    if(isset($value[$id])){
                        $pREASON = number_format($value[$id]/$aSumRowReason[$key]*100, 0, '.', '');
                        $pAPP = number_format($value[$id]/$aSumRowApp[$key]*100, 0, '.', '');
                        echo $value[$id].' / '. $pREASON.'% / '.$pAPP.'% '; 
                    }else{
                        echo '&nbsp;';
                    }
                    ?>
                    </td>
                <?php endforeach; ?>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
</div>