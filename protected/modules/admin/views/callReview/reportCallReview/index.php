<?php
$this->breadcrumbs = array(
    $this->pageTitle,
);
$totalIncoming = 0;
$stt = 0;
$menus=array();
if($model->canExportExcel()):
        $menus[] = [
            'label'=> 'Xuất Excel Danh Sách Hiện Tại',
            'url'=>array('reportCallReview', 'ExportExcel'=>1), 
            'htmlOptions'=>array('class'=>'export_excel ','label'=>'Xuất Excel'),
        ];
    
endif;
$this->menu= ControllerActionsName::createMenusRoles($menus, $actions);
$gridCss = Yii::app()->getAssetManager()->publish(Yii::getPathOfAlias('zii.widgets.assets')) . '/gridview/styles.css';
Yii::app()->getClientScript()->registerCssFile($gridCss);
?>

<h1><?php echo $this->pageTitle; ?></h1>
<?php include 'index_button.php'; ?>
<div class="search-form" style="">
    <?php 
    if(isset($_GET['type']) && $_GET['type']== CallReview::BC_TONGDAI ){
        $this->renderPartial('reportCallReview/_searchCallCenter', array(
            'model' => $model,
        )); 
    }elseif(isset($_GET['type']) && $_GET['type']== CallReview::BC_KIEMTRA_TONGDAI ){
        $this->renderPartial('reportCallReview/_searchCallCenterOnDay', array(
            'model' => $model,
        )); 
    }elseif(isset($_GET['type']) && $_GET['type']== CallReview::BC_KIEMTRA ){
        $this->renderPartial('reportCallReview/_searchUserCheck', array(
            'model' => $model,
        )); 
    }
    ?>
</div><!-- search-form -->

<style>
    .ui-datepicker-trigger {
        float: left;
    }
</style>

<script>
    $(document).ready(function () {
        $('.form').find('button:submit').click(function () {
            $.blockUI({overlayCSS: {backgroundColor: '#fff'}});
        });
    });
</script>
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/scroll/jquery.freezetablecolumns.1.1.js"></script>

<?php 

if(isset($_GET['type']) && $_GET['type']== CallReview::BC_TONGDAI ):
    include '_formCallCenter.php';
elseif(isset($_GET['type']) && $_GET['type']== CallReview::BC_KIEMTRA_TONGDAI):
    //include '_formCheckOnCenter.php';
    include '_formCallCenterOnDay.php';
elseif(isset($_GET['type']) && $_GET['type']== CallReview::BC_KIEMTRA):                                     //// báo cáo NV kiểm tra
    include '_formUserCheck.php';
endif;
?>
    
<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/print-store-card.css" />
<script type="text/javascript">
    fnAddClassOddEven('items');
    $('.freezetablecolumns').each(function () {
        var id_table = $(this).attr('id');
        console.log(id_table);
        if(id_table == 'freezetablecolumns_report_call_call_center'){
            $('#' + id_table).freezeTableColumns({
                width:       <?php echo Users::WIDTH_freezetable;?>,   // required - bt 1100
                height:      400,   // required
                numFrozen: 5,     // optional
                frozenWidth: 350,   // optional
                clearWidths: true  // optional
            });
        }else{
            $('#' + id_table).freezeTableColumns({
                width:       <?php echo Users::WIDTH_freezetable;?>,   // required - bt 1100
                height:      400,   // required
                numFrozen: 3,     // optional
                frozenWidth: 315,   // optional
                clearWidths: true  // optional
            });
        }
        
    });
    $(window).load(function () {
        var index = 1;
        $('.freezetablecolumns').each(function () {
            if (index == 1)
                $(this).closest('div.grid-view').show();
            index++;
        });
        fnAddClassOddEven('items');
    });
    
</script>