<div class="form">
    <?php
        $LinkBC_KIEMTRA          = Yii::app()->createAbsoluteUrl('admin/callReview/reportCallReview', array( 'type'=> CallReview::BC_KIEMTRA   ));
        $LinkBC_TONGDAI          = Yii::app()->createAbsoluteUrl('admin/callReview/reportCallReview', array( 'type'=> CallReview::BC_TONGDAI   ));
        $LinkBC_KIEMTRA_TONGDAI  = Yii::app()->createAbsoluteUrl('admin/callReview/reportCallReview', array( 'type'=> CallReview::BC_KIEMTRA_TONGDAI   ));
    ?>
    <h1><?php echo $this->adminPageTitle;?>
        <a class='btn_cancel f_size_14 <?php echo isset($_GET['type']) && $_GET['type']== CallReview::BC_KIEMTRA ? "active":"";?>' href="<?php echo $LinkBC_KIEMTRA;?>">NV kiểm tra</a>
        <a class='btn_cancel f_size_14 <?php echo isset($_GET['type']) && $_GET['type']== CallReview::BC_TONGDAI ? "active":"";?>' href="<?php echo $LinkBC_TONGDAI;?>">NV tổng đài</a>
        <a class='btn_cancel f_size_14 <?php echo isset($_GET['type']) && $_GET['type']== CallReview::BC_KIEMTRA_TONGDAI ? "active":"";?>' href="<?php echo $LinkBC_KIEMTRA_TONGDAI;?>">NV tổng đài theo ngày</a>
    </h1> 
</div>