<!-- Used for agent call report search area -->
<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
//	'action'=> GasCheck::getCurl(),
	'action'=>Yii::app()->createUrl($this->route).'/type/'.CallReview::BC_KIEMTRA,
	'method'=>'get',
)); ?>    
    <div class="row more_col">
        <div class="col1">
            <?php echo $form->label($model,'date_from') ?>		
            <?php 
                $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                    'model'=>$model,        
                    'attribute'=>'date_from',
                    'options'=>array(
                        'showAnim'=>'fold',
                        'dateFormat'=> 'dd-mm-yy',
                        'maxDate'=> '0',
                        'changeMonth' => true,
                        'changeYear' => true,
                        'showOn' => 'button',
                        'buttonImage'=> Yii::app()->theme->baseUrl.'/admin/images/icon_calendar_r.gif',
                        'buttonImageOnly'=> true,                                
                    ),        
                    'htmlOptions'=>array(
                        'class'=>'w-16',
                        'style'=>'height:20px;float:left;',
                    ),
                ));
            ?>
        </div>

        <div class="col2">
            <?php echo $form->label($model,'date_to') ?>		
            <?php 
                $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                    'model'=>$model,        
                    'attribute'=>'date_to',
                    'options'=>array(
                        'showAnim'=>'fold',
                        'dateFormat'=> 'dd-mm-yy',
                        'maxDate'=> '0',
                        'changeMonth' => true,
                        'changeYear' => true,
                        'showOn' => 'button',
                        'buttonImage'=> Yii::app()->theme->baseUrl.'/admin/images/icon_calendar_r.gif',
                        'buttonImageOnly'=> true,                                
                    ),        
                    'htmlOptions'=>array(
                        'class'=>'w-16',
                        'style'=>'height:20px;float:left;',
                    ),
                ));
            ?>
        </div>
        <div class="col3"></div>
    </div>
    
    <div class="row buttons" style="padding-left: 159px;">
        <?php $this->widget('bootstrap.widgets.TbButton', array(
        'buttonType'=>'submit',
        'label'=>'Xem',
        'type'=>'null', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
        'size'=>'small', // null, 'large', 'small' or 'mini'
        //'htmlOptions' => array('style' => 'margin-bottom: 10px; float: right;'),
    )); ?>	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->