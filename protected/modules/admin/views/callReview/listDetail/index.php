<?php
$this->breadcrumbs=array(
	$this->singleTitle,
);

$menus=array();
if($model->canExportExcel()):
        $menus[] = [
            'label'=> 'Xuất Excel Danh Sách Hiện Tại',
            'url'=>array('listDetail', 'ExportExcel'=>1), 
            'htmlOptions'=>array('class'=>'export_excel ','label'=>'Xuất Excel'),
        ];
    
endif;
$this->menu= ControllerActionsName::createMenusRoles($menus, $actions);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('call-review-grid', {
                url : $(this).attr('action'),
		data: $(this).serialize()
	});
	return false;
});
");

Yii::app()->clientScript->registerScript('ajaxupdate', "
$('#call-review-grid a.ajaxupdate').live('click', function() {
    $.fn.yiiGridView.update('call-review-grid', {
        type: 'POST',
        url: $(this).attr('href'),
        success: function() {
            $.fn.yiiGridView.update('call-review-grid');
        }
    });
    return false;
});
");
?>

<h1>Danh Sách <?php echo $this->pluralTitle; ?></h1>


<?php echo CHtml::link('Tìm Kiếm Nâng Cao','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('listDetail/_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'call-review-grid',
	'dataProvider'=>$model->search(),
        'afterAjaxUpdate'=>'function(id, data){ fnUpdateColorbox();}',
        'template'=>'{pager}{summary}{items}{pager}{summary}',
//    'itemsCssClass' => 'items custom_here',
//    'htmlOptions'=> array('class'=>'grid-view custom_here'),
        'pager' => array(
            'maxButtonCount'=>  CmsFormatter::$PAGE_MAX_BUTTON,
        ),
	'enableSorting' => false,
	//'filter'=>$model,
	'columns'=>array(
        array(
            'header' => 'S/N',
            'type' => 'raw',
            'value' => '$this->grid->dataProvider->pagination->currentPage * $this->grid->dataProvider->pagination->pageSize + ($row+1)',
            'headerHtmlOptions' => array('width' => '10px','style' => 'text-align:center;'),
            'htmlOptions' => array('style' => 'text-align:center;')
        ),
                array(
                    'name'=>'call_id',
                    'type'=>'raw',
                    'htmlOptions' => array('style' => 'text-align:center;'),
                    'value'=>'$data->getModelCallForUrlPlayAudio()',
                ),
                array(
                    'visible'=> MyFormat::getCurrentRoleId()!=ROLE_TEST_CALLL,
                    'name'=>'agent_id',
                    'type'=>'raw',
                    'value'=>'$data->getAgent()',
                ),
                array(
                    'name'=>'province_id',
                    'type'=>'raw',
                    'value'=>'$data->getProvince()',
                ),
                array(
                    'visible'=> MyFormat::getCurrentRoleId()!=ROLE_TEST_CALLL,
                    'name'=>'call_center_id',
                    'type'=>'raw',
                    'value'=>'$data->getCallCenter()',
                ),
                array(
                    'name'=>'reason',
                    'type'=>'raw',
                    'value'=>'$data->getReaSon()',
                ),
		array(
                    'name'=>'created_date',
                    'type'=>'raw',
                    'value'=>'$data->getCreatedDate()',
                ),
                array(
                    'name'=>'creates_by',
                    'type'=>'raw',
                    'value'=>'$data->getCreatesBy()',
                ),
                array(
                    'header' => 'Nội dung',
                    'type'=>'raw',
                    'value'=>'$data->getNote()',
                ),
		/*
		'created_date',
		'creates_by',
		*/
		array(
                    'header' => 'Actions',
                    'class'=>'CButtonColumn',
                    'template'=> ControllerActionsName::createIndexButtonRoles($actions),
                    'buttons'=>array(
                        'update' => array(
                            'visible' => '$data->canUpdate()',
                        ),
                        'delete'=>array(
                            'visible'=> 'GasCheck::canDeleteData($data)',
                        ),
                    ),
		),
	),
)); ?>

<script>
$(document).ready(function() {
    fnUpdateColorbox();
});

function fnUpdateColorbox(){
    fnShowhighLightTr();
    fixTargetBlank();
//    $(".createDetail").colorbox({iframe:true,innerHeight:'550', innerWidth: '850',close: "<span title='close'>close</span>"});
    $('.PlayAudioPbxIcon').click(function(){
        var td = $(this).closest('td');
        td.find('.PlayAudioPbxDiv').toggle();
    });
}
</script>