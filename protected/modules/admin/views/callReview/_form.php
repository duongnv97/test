<?php
$infoCall   = $model->getInfoCall();
$mSell = new Sell();

?>
<div class="form">
<?php 
if(!isset($_GET['OnlyView'])): // dành cho chỉnh sửa tạo mới
$form=$this->beginWidget('CActiveForm', array(
	'id'=>'call-review-form',
	'enableAjaxValidation'=>false,
        'htmlOptions' => array('enctype' => 'multipart/form-data'),
)); ?>
<center><h1>Tạo Mới <?php echo $this->singleTitle; ?></h1></center>
<p class="note">Dòng có dấu <span class="required">*</span> là bắt buộc.</p>
<?php echo MyFormat::BindNotifyMsg(); ?>
<?php echo $infoCall; ?>
<div class="row group_subscriber">
    <?php echo $form->label($model,'reason'); ?>
    <?php // echo $form->dropDownList($model,'agent_id', Users::getArrUserByRole(ROLE_AGENT),array('style'=>'width:350px;','empty'=>'Select')); ?>
    <div class="fix-label">
        <?php
           $this->widget('ext.multiselect.JMultiSelect',array(
                 'model'=>$model,
                 'attribute'=>'reason',
                 'data'=>$model->getArrayReason(),
                 // additional javascript options for the MultiSelect plugin
                 'options'=>array('selectedList' => 30,),
                 // additional style
                 'htmlOptions'=>array('style' => 'width: 500px;'),
           ));    
       ?>
    </div>
</div>
        
<div class="row">
    <?php echo $form->label($mCall,'customer_new',['label'=>'Loại KH']); ?>
    <?php  echo $form->dropDownList($mCall,'customer_new', $mSell->getArrayCustomerNewOld(), ['class'=>'w-200']); ?>
</div>
<div class="row">
    <?php echo $form->labelEx($mCall,'note',['label'=>'Ghi chú']); ?>
    <?php echo $form->textArea($mCall,'note',array('class'=>'w-300 h_50 NoteTraThe')); ?>
</div>

<div class="row buttons" style="padding-left: 141px;">
                <?php $this->widget('bootstrap.widgets.TbButton', array(
    'buttonType'=>'submit',
    'label'=> 'Save',
    'type'=>'null', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
    'size'=>'small', // null, 'large', 'small' or 'mini'
    //'htmlOptions' => array('style' => 'margin-bottom: 10px; float: right;'),
)); ?>	
<input class='cancel_iframe' type='button' value='Cancel'>

</div>

<?php $this->endWidget(); ?>
</div><!-- form -->
<?php endif; ?>
    <?php if(!empty($mCall->getNote())): ?>
    <div class="row">
        <label class='item_b'>Ghi chú :</label>
        <?php echo $mCall->getNote(); ?>
    </div>
    <?php endif; ?>
<?php include '_form_view.php'; ?>

<script>
    $(document).ready(function(){
        $('.form').find('button:submit').click(function(){
            $.blockUI({ overlayCSS: { backgroundColor: '#fff' } }); 
        });
    });
</script>