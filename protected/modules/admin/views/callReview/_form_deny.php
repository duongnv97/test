<div class="form">

<?php 
$form=$this->beginWidget('CActiveForm', array(
	'id'=>'call-review-form',
	'enableAjaxValidation'=>false,
        'htmlOptions' => array('enctype' => 'multipart/form-data'),
)); 
?>
    <br><br>
    <center><h1>Xác nhận nhả cuộc gọi</h1></center>
    <?php echo MyFormat::BindNotifyMsg(); ?>
    <input name='isDeny' value='1' hidden='1'>
    <div class="row buttons" style="padding-left: 90px;" >
                <?php $this->widget('bootstrap.widgets.TbButton', array(
    'buttonType'=>'submit',
    'label'=> 'Submit',
    'type'=>'null', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
    'size'=>'small', // null, 'large', 'small' or 'mini'
    //'htmlOptions' => array('style' => 'margin-bottom: 10px; float: right;'),
)); ?>	
    <input class='cancel_iframe' type='button' value='Cancel'>



</div>

<?php $this->endWidget(); ?>

</div><!-- form -->
<script>
    $(document).ready(function(){
        $('.form').find('button:submit').click(function(){
            $.blockUI({ overlayCSS: { backgroundColor: '#fff' } }); 
        });
    });
</script>