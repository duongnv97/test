<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'call-review-grid',
	'dataProvider'=>$model->searchForView(),
        'afterAjaxUpdate'=>'function(id, data){ fnUpdateColorbox();}',
        'template'=>'{pager}{summary}{items}{pager}{summary}',
//    'itemsCssClass' => 'items custom_here',
//    'htmlOptions'=> array('class'=>'grid-view custom_here'),
        'pager' => array(
            'maxButtonCount'=>  CmsFormatter::$PAGE_MAX_BUTTON,
        ),
	'enableSorting' => false,
	//'filter'=>$model,
	'columns'=>array(
        array(
            'header' => 'S/N',
            'type' => 'raw',
            'value' => '$this->grid->dataProvider->pagination->currentPage * $this->grid->dataProvider->pagination->pageSize + ($row+1)',
            'headerHtmlOptions' => array('width' => '10px','style' => 'text-align:center;'),
            'htmlOptions' => array('style' => 'text-align:center;')
        ),
		array(
                    'name'    =>'reason',
                    'type'      => 'raw',
                    'value'     => '$data->getReason()'
                ),
                array(
                    'name'    =>'created_date',
                    'type'      => 'raw',
                    'value'     => '$data->getCreatedDate()'
                ),
//            array(
//                'header' => 'Actions',
//                'class'=>'CButtonColumn',
//                'template'=> ControllerActionsName::createIndexButtonRoles($actions),
//                'buttons'=>array(
////                    'update' => array(
////                        'visible' => '$data->canUpdate()',
////                    ),
//                    'delete'=>array(
//                        'visible'=> '0',
//                    ),
//                ),
//            ),
	),
)); ?>