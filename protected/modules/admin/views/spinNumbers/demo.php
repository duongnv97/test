<h1>Admin test</h1>
<?php echo MyFormat::BindNotifyMsg(); ?>

<div class="autogen-form" style="margin: 10px; padding: 10px;background: #eee;">
<div class="wide form">
    
    <h3>Tự động tạo số và gán cho user</h3>
    
    <?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>['/admin/spinNumbers/demo'],
	'method'=>'post',
)); ?>
    
<!--        <div class="row">
            <?php // echo $form->label($model, 'Chọn chức vụ'); ?>
            <div class="fix-label">
                <?php
//                $this->widget('ext.multiselect.JMultiSelect', array(
//                    'model' => $model,
//                    'attribute' => 'role_id',
//                    'data' => Roles::model()->getDropdownList(),
//                    // additional javascript options for the MultiSelect plugin
//                    'options' => array('selectedList' => 30),
//                    // additional style
//                    'htmlOptions' => array('class' => 'w-400'),
//                )); 
                ?>
            </div>
        </div>-->
    
    <div class="row ">
        <?php echo $form->labelEx($model, 'KH'); ?>
        <?php echo $form->hiddenField($model, 'user_assign'); ?>
        <?php
        // 1. limit search kh của sale
        $url = Yii::app()->createAbsoluteUrl('admin/ajax/searchHgd');
        // widget auto complete search user customer and supplier
        $aData = array(
            'model' => $model,
            'field_customer_id' => 'user_assign',
            'url' => $url,
            'name_relation_user' => 'rEmployee',
            'ClassAdd' => 'w-300',
            'placeholder' => 'Nhập mã hoặc tên KH',
            'field_autocomplete_name' => 'autocomplete_name2',
        );
        $this->widget('ext.SpaAutocompleteCustomer.SpaAutocompleteCustomer', array('data' => $aData));
        ?>
    </div>
    
        <div class="row">
            <?php echo $form->label($model,'Source'); ?>
            <?php echo $form->dropdownList($model,'source',$model->getArraySource(), ['class' => 'w-300']); ?>
        </div>

<!--        <div class="row">
            <?php // echo $form->label($model,'Số người tối đa'); ?>
            <?php // echo $form->textField($model,'limit_assign',['class' => 'number_only_v1 w-300', 'maxlength' => 10]); ?>
        </div>-->
        
	<div class="row buttons" style="padding-left: 159px;">
            <?php $this->widget('bootstrap.widgets.TbButton', array(
                'buttonType'=>'submit',
                'label'     =>'Gán cho nhân viên',
                'type'      =>'null', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
                'size'      =>'small', // null, 'large', 'small' or 'mini'
                'htmlOptions' => array('name' => 'AssignNumber'),
            )); ?>
            
            <a class='btn_cancel active' target="_blank" 
               style="text-decoration: none;"
               onclick="return confirm('are you sure?')"
               href="<?php echo Yii::app()->createAbsoluteUrl("/admin/spinNumbers/demo", ['auto_gen'=>1, 'max'=>100]); ?>">Tạo 100 số</a>
        </div>

<?php $this->endWidget(); ?>

</div>
</div>