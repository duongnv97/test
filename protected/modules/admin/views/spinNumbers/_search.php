<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>GasCheck::getCurl(),//Yii::app()->createUrl($this->route), //clean URL 
	'method'=>'get',
)); ?>
    <div class="row">
    <?php echo $form->labelEx($model,'Tên khách hàng'); ?>
    <?php echo $form->hiddenField($model,'user_id', array('class'=>'')); ?>
    <?php
//            $url = Yii::app()->createAbsoluteUrl('admin/ajax/search_by_role', ['role'=> ROLE_CUSTOMER]);
            $url = Yii::app()->createAbsoluteUrl('admin/ajax/searchHgd');
            $aData = array(
                'model'                     =>$model,
                'field_customer_id'         =>'user_id',
                'url'                       => $url,
                'name_relation_user'        =>'rUsers',
                'ClassAdd'                  => 'w-300',
                'field_autocomplete_name'   => 'autocomplete_name',
                'placeholder'               =>'Nhập mã hoặc tên khách hàng',
            );
            $this->widget('ext.SpaAutocompleteCustomer.SpaAutocompleteCustomer',
                array('data'=>$aData));
    ?>
    </div>
    <div class="row">
        <?php echo $form->label($model,'number'); ?>
        <?php echo $form->textField($model,'number',['class' => 'number_only_v1 w-300', 'maxlength' => 10,]); ?>
    </div>
    <div class='row more_col'>
        <div class="col1">
            <?php echo $form->label($model,'source_detail'); ?>
            <?php echo $form->textField($model,'source_detail',['class' =>' ']); ?>
        </div>
        <div class="col2">
            <?php echo $form->label($model,'source'); ?>
            <?php echo $form->dropdownList($model, 'source', SpinNumbers::getArraySource(),array('empty'=>'Select', 'class' => 'w-200')) ?>
        </div>
    </div>
    <br>
        <div style='clear: both' class='row more_col'>
            <div class="col1">
                <?php echo $form->label($model,'Ngày tạo hạn từ'); ?>
                <?php 
                    $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                        'model'     =>$model,        
                        'attribute' =>'date_from',
                        'options'   =>array(
                            'showAnim'      =>'fold',
                            'dateFormat'    => MyFormat::$dateFormatSearch,
//                            'minDate'=> '0',
                            'maxDate'       => '0',
                            'changeMonth'   => true,
                            'changeYear'    => true,
                            'showOn'        => 'button',
                            'buttonImage'   => Yii::app()->theme->baseUrl.'/admin/images/icon_calendar_r.gif',
                            'buttonImageOnly'=> true,                                
                        ),        
                        'htmlOptions'=>array(
                            'class' =>'w-16 date_from',
                            'size'  =>'16',
                            'style' =>'float:left;',                               
                        ),
                    ));
                ?>     		
            </div>
            <div class="col2">
                    <?php echo $form->label($model,'Đến'); ?>
                    <?php 
                        $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                            'model'             =>$model,        
                            'attribute'         =>'date_to',
                            'options'           =>array(
                                'showAnim'      =>'fold',
                                'dateFormat'    => MyFormat::$dateFormatSearch,
                                'maxDate'       => '0',
                                'changeMonth'   => true,
                                'changeYear'    => true,
                                'showOn'        => 'button',
                                'buttonImage'   => Yii::app()->theme->baseUrl.'/admin/images/icon_calendar_r.gif',
                                'buttonImageOnly'=> true,                                
                            ),        
                            'htmlOptions'=>array(
                                'class' =>'w-16 date_to',
                                'size'  =>'16',
                                'style' =>'float:left;',
                            ),
                        ));
                    ?>     		
            </div>
        </div>
        
        <div class="row">
            <?php echo $form->label($model, 'Trạng thái') ?>
            <?php echo $form->dropdownList($model, 'status', SpinNumbers::getArrayStatus(),array('empty'=>'Select', 'class' => 'w-300')) ?>
        </div>
    
        <?php if($model->is_test): ?>
        <div class="row">
            <?php echo $form->label($model, 'Chỉ user demo') ?>
            <?php echo $form->checkbox($model, 'search_user_demo',array()) ?>
        </div>
        <?php endif; ?>
    
        <div class='row'>
            <br/>
        </div>
	<div class="row buttons" style="padding-left: 159px;">
            <?php $this->widget('bootstrap.widgets.TbButton', array(
                'buttonType'=>'submit',
                'label'     =>'Search',
                'type'      =>'null', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
                'size'      =>'small', // null, 'large', 'small' or 'mini'
                //'htmlOptions' => array('style' => 'margin-bottom: 10px; float: right;'),
            )); ?>
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a class='btn_cancel btnReset'>Reset</a>
        </div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->

<script>
    $(function(){
        $('.date_from').change(function(){
            var div = $(this).closest('.row');
            div.find('.date_to').val($(this).val());
        });
        $('.btnReset').click(function(){
            var div = $(this).closest('form');
            div.find('input').val('');
            div.find('Select').val('');
            div.find('.remove_row_item').trigger('click');
        });
    });
</script>