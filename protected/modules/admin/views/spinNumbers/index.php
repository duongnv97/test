<?php
$this->breadcrumbs=array(
	
);
// register script and css, add libery for view   

Yii::app()->clientScript->registerScript('search', "  
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});

$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('search-grid', {
                url : $(this).attr('action'),
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Báo cáo <?php echo $this->pluralTitle; ?></h1>
<?php echo MyFormat::BindNotifyMsg(); ?>

<?php echo CHtml::link('Tìm Kiếm Nâng Cao', '#', array('class' => 'search-button')); ?>
&nbsp;&nbsp;&nbsp;&nbsp;
<div class="search-form" style="display: none" >
    <?php
    $this->renderPartial('_search', array(
        'model' => $model,
    ));
    ?>
</div><!-- search-form -->
    
<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'                =>'search-grid',
	'dataProvider'      =>$model->search(),
        'afterAjaxUpdate'   =>'function(id, data){ fnUpdateColorbox();}',
        'template'          =>'{pager}{summary}{items}{pager}{summary}',
//    'itemsCssClass' => 'items custom_here',
//    'htmlOptions'=> array('class'=>'grid-view custom_here'),
        'pager'             => array(
            'maxButtonCount'=>  CmsFormatter::$PAGE_MAX_BUTTON,
        ),
	'enableSorting'     => false,
	//'filter'=>$model,
    'columns'               => array(
            array(
                'header'            => 'S/N',
                'type'              => 'raw',
                'value'             => '$this->grid->dataProvider->pagination->currentPage * $this->grid->dataProvider->pagination->pageSize + ($row+1)',
                'headerHtmlOptions' => array('width' => '10px','style' => 'text-align:center;'),
                'htmlOptions'       => array('style' => 'text-align:center;')
            ),
            array(
                'name'      => 'user_id' ,
                'type'      => 'html',
                'value'     => '$data->getUser()'
            ),
            array(
                'name'      => 'number',
                'value'     => '$data->getNumber()'
            ),
            array(
                'name'      => 'source_detail',
                'value'     => '$data->getSourceDetail()'
            ),
            array(
                'name'      => 'source',
                'value'     => '$data->getSource()'
            ),
            array(
                'name'      => 'created_date',
                'value'     => '$data->getCreatedDate()'
            ),
            array(
                'name'      => 'expired_date',
                'value'     => '$data->getExpiredDate()'
            ),
            array(
                'name'      => 'status',
                'type'      => 'html',
                'value'     => '$data->getStatus()'
            ),
            array(
                'header' => 'Actions',
                'class'=>'CButtonColumn',
                'template'=> ControllerActionsName::createIndexButtonRoles($actions,['view','update','delete','testNotify']),
                'buttons'=>array(
                    'testNotify'=>array(
                        'visible'=> 'MyFormat::getCurrentRoleId()==ROLE_ADMIN',
                        'label'=>'Test send notify winning',     //Text label of the button.
                        'url'=>'Yii::app()->createAbsoluteUrl("admin/spinNumbers/testNotify",array("user_id"=>$data->user_id,"type"=>GasScheduleNotify::GAS24H_NEW_LUCKY_NUMBER) )',
                        'imageUrl'=>Yii::app()->theme->baseUrl . '/admin/images/Upload.png',
                        'options'=>array('class' => 'update','onclick'=>'return confirm("Bạn có chắc?");'), //HTML options for the button tag..
                    ),
                ),
            ),
    )
));
?>

<script>
$(document).ready(function() {
    fnUpdateColorbox();
});

function fnUpdateColorbox(){  
     fixTargetBlank();
		$(".report").colorbox({iframe:true,height:'350' ,innerHeight:'500', innerWidth: '1050',close: "<span title='close'>close</span>"});
	}
</script>
