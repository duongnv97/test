<?php
/**
 * VerzDesignCMS
 * 
 * LICENSE
 *
 * @copyright	Copyright (c) 2012 Verz Design (http://www.verzdesign.com)
 * @version 	$Id: _form.php 2012-06-01 09:09:18 nguyendung $
 * @since		1.0.0
 */
?>
<div class="form editor_area_size ">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'email-templates-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'email_subject'); ?>
		<?php echo $form->textField($model,'email_subject',array('class'=>'w-500','maxlength'=>255)); ?>
		<?php echo $form->error($model,'email_subject'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'parameter_description'); ?>
		<?php echo $form->textArea($model,'parameter_description',array('class'=>'w-500','rows'=>10, 'cols'=>50)); ?>
		<?php echo $form->error($model,'parameter_description'); ?>
	</div>
        
        <div class="row">
            <?php echo $form->labelEx($model,'email_body', ['class'=>'item_b f_size_15']); ?><br><br>
            <div style="">
                    <?php
//                    $this->widget('ext.niceditor.nicEditorWidget', array(
//                            "model" => $model, // Data-Model
//                            "attribute" => 'email_body', // Attribute in the Data-Model        
//                            "config" => array(
////                                "maxHeight" => "200px",   
//                            "buttonList"=>Yii::app()->params['niceditor_v_2'],
//                            ),
//                            "width" => GasMeetingMinutes::EDITOR_WIDTH, // Optional default to 100%
//                            "height" => GasMeetingMinutes::EDITOR_HEIGHT, // Optional default to 150px
//                    ));
//                    ?>
            <?php echo $form->textArea($model,'email_body',array('class'=>'ckeditor')); ?>
            <div class="clr"></div>
            <?php echo $form->error($model,'email_body'); ?>
            </div>
	</div>
	<div class='clr'></div>

        <div class="row" style="display: none;">
            <?php echo $form->labelEx($model,'type'); ?>
            <?php echo $form->textField($model,'type',array('size'=>60,'maxlength'=>255)); ?>
            <?php echo $form->error($model,'type'); ?>
	</div>
        
	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->

<script charset="utf-8" type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/gasckeditor/ckeditor.js"></script>
<!--https://ckeditor.com/old/forums/CKEditor-3.x/utf-8-ckeditor-->
<script>
    // http://stackoverflow.com/questions/15659390/ckeditor-automatically-strips-classes-from-div
    $(function(){
        $(".ckeditor").each(function(){
           var id = $(this).attr('id');
            CKEDITOR.replace( id, {
//                removePlugins: 'about, iframe, flash',
                removePlugins: 'about, flash',
                height: 500,
                allowedContent: true,// Aug 13, 2016 chắc chắn phải open dòng này, không thì sẽ bị lỗi khi có tab và accordiontab // http://docs.ckeditor.com/#!/guide/dev_allowed_content_rules  == http://ckeditor.com/forums/CKEditor-3.x/ckeditor-remove-tag-attributes
                entities_latin: false,
                extraAllowedContent : '*(*)'
            });
        });
        
    });
</script>