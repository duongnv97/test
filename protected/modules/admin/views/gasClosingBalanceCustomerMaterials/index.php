<?php
$this->breadcrumbs=array(
	'Nhập Tồn Đầu Kỳ Vỏ',
);

$menus=array(
            array('label'=>'Create Nhập Tồn Đầu Kỳ Vỏ', 'url'=>array('create')),
);
$this->menu= ControllerActionsName::createMenusRoles($menus, $actions);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('gas-closing-balance-customer-materials-grid', {
                url : $(this).attr('action'),
		data: $(this).serialize()
	});
	return false;
});
");

Yii::app()->clientScript->registerScript('ajaxupdate', "
$('#gas-closing-balance-customer-materials-grid a.ajaxupdate').live('click', function() {
    $.fn.yiiGridView.update('gas-closing-balance-customer-materials-grid', {
        type: 'POST',
        url: $(this).attr('href'),
        success: function() {
            $.fn.yiiGridView.update('gas-closing-balance-customer-materials-grid');
        }
    });
    return false;
});
");
?>

<h1>Nhập Tồn Đầu Kỳ Vỏ</h1>

<?php echo CHtml::link('Tìm Kiếm Nâng Cao','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'gas-closing-balance-customer-materials-grid',
	'dataProvider'=>$model->search(),
        'afterAjaxUpdate'=>'function(id, data){ fnUpdateColorbox();}',
        'template'=>'{pager}{summary}{items}{pager}{summary}', 
        'pager' => array(
            'maxButtonCount'=>  CmsFormatter::$PAGE_MAX_BUTTON,
        ),            
	'enableSorting' => false,
	//'filter'=>$model,
	'columns'=>array(
        array(
            'header' => 'S/N',
            'type' => 'raw',
            'value' => '$this->grid->dataProvider->pagination->currentPage * $this->grid->dataProvider->pagination->pageSize + ($row+1)',
            'headerHtmlOptions' => array('width' => '10px','style' => 'text-align:center;'),
            'htmlOptions' => array('style' => 'text-align:center;')
        ),
//                array(
//                    'name' => 'year',
//                    'htmlOptions' => array('style' => 'text-align:center;')
//                ),
                array(                
                    'name' => 'customer_id',
                    'type'=>'NameUser',
                    'value'=>'$data->customer?$data->customer:0',
                    'htmlOptions' => array('style' => 'text-align:left;width:100px;')
                ),                
                array(                
                    'name' => 'type_customer',
                    'value'=>'$data->customer->is_maintain?CmsFormatter::$CUSTOMER_BO_MOI[$data->customer->is_maintain]:""',
                    'htmlOptions' => array('style' => 'text-align:left;width:100px;')
                ),                
		
                array(
                    'name' => 'export_materials_type_id',
                    'value'=>'$data->rExportMaterials?$data->rExportMaterials->name:""',
                ),
                array(
                    'name' => 'balance',
                    'htmlOptions' => array('style' => 'text-align:right;')
                ),
//		'import_materials_type_id',rExportMaterials
		array(
			'header' => 'Actions',
			'class'=>'CButtonColumn',
                        'template'=> ControllerActionsName::createIndexButtonRoles($actions),
		),
	),
)); ?>

<script>
$(document).ready(function() {
    fnUpdateColorbox();
});

function fnUpdateColorbox(){   
    fixTargetBlank();
}
</script>