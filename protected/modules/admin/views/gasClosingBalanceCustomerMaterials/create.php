<?php
$this->breadcrumbs=array(
	'Nhập Tồn Đầu Kỳ Vỏ'=>array('index'),
	'Tạo Mới',
);

$menus = array(		
        array('label'=>'Nhập Tồn Đầu Kỳ Vỏ', 'url'=>array('index')),
);
$this->menu= ControllerActionsName::createMenusRoles($menus, $actions);
?>

<h1>Tạo Mới Nhập Tồn Đầu Kỳ Vỏ</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>