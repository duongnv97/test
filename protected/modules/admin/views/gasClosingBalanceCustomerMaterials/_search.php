<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'year',array()); ?>
		<?php echo $form->textField($model,'year'); ?>
	</div>


	<div class="row">
                <?php echo $form->labelEx($model,'customer_id'); ?>
                <?php echo $form->hiddenField($model,'customer_id'); ?>
                <?php 
                    // widget auto complete search user customer and supplier
                    $aData = array(
                        'model'=>$model,
                        'name_relation_user'=>'customer',
                    );
                    $this->widget('ext.SpaAutocompleteCustomer.SpaAutocompleteCustomer',
                        array('data'=>$aData));                                        
                ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'type_customer',array()); ?>
		<?php echo $form->dropDownList($model,'type_customer', CmsFormatter::$CUSTOMER_BO_MOI,array('class'=>'w-150', 'empty'=>'Select')); ?>
	</div>

	<div class="row">
            <?php echo $form->label($model,'export_materials_type_id'); ?>
            <?php echo $form->dropDownList($model,'export_materials_type_id', GasMaterialsType::getAllItem(CmsFormatter::$MATERIAL_TYPE_CLOSING_BALANCE),array('empty'=>'Select')); ?>
	</div>

	<div class="row">
		<?php // echo $form->label($model,'import_materials_type_id',array()); ?>
		<?php // echo $form->textField($model,'import_materials_type_id'); ?>
	</div>


	<div class="row buttons" style="padding-left: 159px;">
		        <?php $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType'=>'submit',
            'label'=>'Search',
            'type'=>'null', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
            'size'=>'small', // null, 'large', 'small' or 'mini'
            //'htmlOptions' => array('style' => 'margin-bottom: 10px; float: right;'),
        )); ?>	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->