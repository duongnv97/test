<?php
$cmsFormater = new CmsFormatter();
$nameCustomer = $cmsFormater->formatNameUser($model->customer);
$this->breadcrumbs=array(
	'Nhập Tồn Đầu Kỳ Vỏ'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Cập Nhật',
);

$menus = array(	
        array('label'=>'Nhập Tồn Đầu Kỳ Vỏ ', 'url'=>array('index')),
	array('label'=>'Xem Nhập Tồn Đầu Kỳ Vỏ', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Tạo Mới Nhập Tồn Đầu Kỳ Vỏ', 'url'=>array('create')),	
);
$this->menu= ControllerActionsName::createMenusRoles($menus, $actions);

?>

<h1>Cập Nhật Nhập Tồn Đầu Kỳ Vỏ: <?php echo $nameCustomer; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>