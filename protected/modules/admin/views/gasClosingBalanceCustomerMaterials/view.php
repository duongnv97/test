<?php
$cmsFormater = new CmsFormatter();
$nameCustomer = $cmsFormater->formatNameUser($model->customer);

$this->breadcrumbs=array(
	'Nhập Tồn Đầu Kỳ Vỏ'=>array('index'),
	$nameCustomer,
);

$menus = array(
	array('label'=>'Nhập Tồn Đầu Kỳ Vỏ', 'url'=>array('index')),
	array('label'=>'Tạo Mới Nhập Tồn Đầu Kỳ Vỏ', 'url'=>array('create')),
	array('label'=>'Cập Nhật Nhập Tồn Đầu Kỳ Vỏ', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Xóa Nhập Tồn Đầu Kỳ Vỏ', 'url'=>array('delete'), 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Bạn chắc chắn muốn xóa ?')),
);
$this->menu= ControllerActionsName::createMenusRoles($menus, $actions);
?>

<h1>Xem Nhập Tồn Đầu Kỳ Vỏ: <?php echo $nameCustomer; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
//		'year',
                array(                
                    'name' => 'customer_id',
                    'type'=>'NameUser',
                    'value'=>$model->customer?$model->customer:0,
                ),             
                array(                
                    'name' => 'type_customer',
                    'value'=>$model->customer->is_maintain?CmsFormatter::$CUSTOMER_BO_MOI[$model->customer->is_maintain]:"",
                ),  		
                array(
                    'name' => 'export_materials_type_id',
                    'value'=>$model->rExportMaterials?$model->rExportMaterials->name:"",
                ),            
//		'import_materials_type_id',
		'balance',
	),
)); ?>
