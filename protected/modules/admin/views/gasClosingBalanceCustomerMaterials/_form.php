<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'gas-closing-balance-customer-materials-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Dòng có dấu <span class="required">*</span> là bắt buộc.</p>
        
        <?php  if(Yii::app()->user->hasFlash('successUpdate')): ?>
            <div class='success_div'><?php echo Yii::app()->user->getFlash('successUpdate');?></div>         
        <?php endif; ?>  	

	<div class="row">
                <?php echo $form->labelEx($model,'customer_id'); ?>
                <?php echo $form->hiddenField($model,'customer_id'); ?>
                <?php 
                    // widget auto complete search user customer and supplier
                    $aData = array(
                        'model'=>$model,
                        'name_relation_user'=>'customer',
                    );
                    $this->widget('ext.SpaAutocompleteCustomer.SpaAutocompleteCustomer',
                        array('data'=>$aData));                                        
                ?>
	</div>
            
	<div class="row">
            <?php echo $form->labelEx($model,'export_materials_type_id'); ?>
            <?php echo $form->dropDownList($model,'export_materials_type_id', GasMaterialsType::getAllItem(CmsFormatter::$MATERIAL_TYPE_CLOSING_BALANCE),array('style'=>'')); ?>
            <?php echo $form->error($model,'export_materials_type_id'); ?>
	</div>
            
	<div class="row">
            <?php echo $form->labelEx($model,'balance'); ?>
            <?php echo $form->textField($model,'balance', array('maxlength'=>6,'class'=>'number_only number_only_v1')); ?>
            <?php echo $form->error($model,'balance'); ?>
	</div>
	<div class="row buttons" style="padding-left: 141px;">
		        <?php $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType'=>'submit',
            'label'=>$model->isNewRecord ? 'Create' :'Save',
            'type'=>'null', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
            'size'=>'small', // null, 'large', 'small' or 'mini'
            //'htmlOptions' => array('style' => 'margin-bottom: 10px; float: right;'),
        )); ?>	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->
<script>
    $(document).ready(function(){
        $('.form').find('button:submit').click(function(){
            $.blockUI({ overlayCSS: { backgroundColor: '#fff' } }); 
        });
        
    });
</script>