<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'log-payment-form',
	'enableAjaxValidation'=>false,
        'htmlOptions' => array('enctype' => 'multipart/form-data'),
)); ?>

    <p class="note">Dòng có dấu <span class="required">*</span> là bắt buộc.</p>
    <?php echo MyFormat::BindNotifyMsg(); ?>
    
        		
        
    <div class="row">
        <?php echo $form->labelEx($model,'process_time'); ?>
        <?php echo $form->textField($model,'process_time',array('size'=>20,'maxlength'=>20)); ?>
        <?php echo $form->error($model,'process_time'); ?>
    </div>
    <div class="row">
        <?php echo $form->labelEx($model,'ref_id'); ?>
        <?php echo $form->textField($model,'ref_id',array('size'=>10,'maxlength'=>10)); ?>
        <?php echo $form->error($model,'ref_id'); ?>
    </div>
    <div class="row">
        <?php echo $form->labelEx($model,'bank_ref_Id'); ?>
        <?php echo $form->textField($model,'bank_ref_Id',array('size'=>10,'maxlength'=>10)); ?>
        <?php echo $form->error($model,'bank_ref_Id'); ?>
    </div>
    <div class="row">
        <?php echo $form->labelEx($model,'amount'); ?>
        <?php echo $form->textField($model,'amount',array('size'=>10,'maxlength'=>10)); ?>
        <?php echo $form->error($model,'amount'); ?>
    </div>
    <div class="row">
        <?php echo $form->labelEx($model,'json'); ?>
        <?php echo $form->textArea($model,'json',array('rows'=>6, 'cols'=>50)); ?>
        <?php echo $form->error($model,'json'); ?>
    </div>
    <div class="row">
        <?php echo $form->labelEx($model,'created_date_bigint'); ?>
        <?php echo $form->textField($model,'created_date_bigint',array('size'=>20,'maxlength'=>20)); ?>
        <?php echo $form->error($model,'created_date_bigint'); ?>
    </div>
	<div class="row buttons" style="padding-left: 141px;">
		        <?php $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType'=>'submit',
            'label'=>$model->isNewRecord ? 'Create' :'Save',
            'type'=>'null', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
            'size'=>'small', // null, 'large', 'small' or 'mini'
            //'htmlOptions' => array('style' => 'margin-bottom: 10px; float: right;'),
        )); ?>	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->
<script>
    $(document).ready(function(){
        $('.form').find('button:submit').click(function(){
            $.blockUI({ overlayCSS: { backgroundColor: '#fff' } }); 
        });
        
    });
</script>