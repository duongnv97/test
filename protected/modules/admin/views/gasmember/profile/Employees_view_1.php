<?php
$this->breadcrumbs=array(
	'QL nhân sự'=>array('employees'),
	$model->code_account." - ".$model->first_name,
);

$menus = array(
    array('label'=>'QL nhân sự', 'url'=>array('employees')),
    array('label'=>'Tạo mới', 'url'=>array('employees_create')),
    array('label'=>'Cập nhật', 'url'=>array('employees_update', 'id'=>$model->id)),
);
$this->menu= ControllerActionsName::createMenusRoles($menus, $actions);
$mUsersRef = $model->mUsersRef;
?>
<h1>Xem Thông Tin Nhân Viên: <?php echo $model->code_account." - ".$model->first_name; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
        array(
            'label' => 'Mã nhân viên',
            'name' => 'code_account',                       
        ),
        array(
            'name' => 'role_id',
            'type'=>'raw',
            'value'=>"<b>".$model->getFamily(["br"=>"<br>"])."</b>".$model->getRoleName(),
        ),
        array(
            'label' => 'Nơi công tác',
            'value' => $model->getParentCacheSession(),
        ),  
        array(
//            'label' => 'Tên Member',
            'name' => 'first_name',                       
            //'htmlOptions' => array('style' => 'text-align:center;')
        ),	
        'address',
        'phone',
        array(
            'name'=>'province_id',
            'value'=>$model->province?$model->province->name:'',
        ),		
        array(
            'name'=>'district_id',
            'value'=>$model->district?$model->district->name:'',
        ),            

        'created_date:datetime',
        'last_logged_in:datetime',
        'status:status',
	),
)); ?>

<script>
$(document).ready(function() {
    jQuery('a.gallery').colorbox({ opacity:0.5 , rel:'group1' });
    $('.employees').addClass('index').find('span').text('Quản lý');
    $('.employees_create').addClass('create').find('span').text('Tạo mới');
    $('.employees_update').addClass('update').find('span').text('Cập nhật');
});
</script>