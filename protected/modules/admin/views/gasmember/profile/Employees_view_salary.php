<h3 class='title-info'>Thông tin Lương, BHXH</h3>
<?php $this->widget('application.components.widgets.XDetailView', array(
    'data' => $mUsersProfile,
    'attributes' => array(
        'group11'=>array(
            'ItemColumns' => 4,
            'attributes' => array(
                    array(
                        'name' => 'contract_type',
                        'value' => $mUsersProfile->getContractType(),
                    ),
                    array(
                        'name'    => 'date_begin_job',
                        'value'     => $mUsersProfile->getDateBeginJob(),
                    ),
                    array(
                        'name'    => 'contract_begin',
                        'value'     => $mUsersProfile->getContractBegin(),
                    ),
                    array(
                        'name'    => 'contract_end',
                        'value'     => $mUsersProfile->getContractEnd(),
                    ),
            ),
        ),
        'group12'=>array(
            'ItemColumns' => 3,
            'attributes' => array(
                    array(
                        'label' => 'Công ty',
                        'value' => $model->getCompany(),
                    ),
                    array(
                        'name'    => 'base_salary',
                        'value'     => $mUsersProfile->getBaseSalary(),
                    ),
                    array(
                        'name'    => 'salary_insurance',
                        'value'     => $mUsersProfile->getSalaryInsurance(),
                    ),
                    array(
                        'name'    => 'tax_no',
                        'value'     => $mUsersProfile->getTaxNo(),
                    ),
                    array(
                        'name' => 'social_insurance_no',
                        'value' => $mUsersProfile->getSocialInsuranceNo(),
                    ),
                    array(
                        'name'    => 'insurance_start',
                        'value'     => $mUsersProfile->getInsuranceStart(),
                    ),
//                    array(
//                        'name'    => 'insurance_end',
//                        'value'     => $mUsersProfile->getInsurancEnd(),
//                    ),
                    array(
                        'name' => 'bank_no',
                        'value' => $mUsersProfile->getBankNo(),
                    ),
                    array(
                        'name'    => 'bank_id',
                        'value'     => $mUsersProfile->getBankName(),
                    ),
                    array(
                        'name'    => 'bank_province_id',
                        'value'     => $mUsersProfile->getBankProvince(),
                    ),
                    array(
                        'name'    => 'bank_branch',
                        'value'     => $mUsersProfile->getBankBranch(),
                    ),
            ),
        ),
    ),
)); ?>
