<?php
$this->breadcrumbs=array(
    $this->pageTitle => array('employees'),
    'Tạo mới',
);

$menus = array(		
        array('label'=> 'Quản lý nhân sự' , 'url'=>array('employees')),
);
$this->menu= ControllerActionsName::createMenusRoles($menus, $actions);
?>
<h1><?php echo $this->pageTitle;?></h1>
<?php include '_form_employees.php'; ?>
