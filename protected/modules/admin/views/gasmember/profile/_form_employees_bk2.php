<?php 
$display_password   = '';
if($model->isNewRecord){
//    $display_password = 'display_none';
}
$canCreateAdd = true;
?>
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/jquery.blockUI.js"></script>    
<div class="form">
<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'users-form',
	'enableAjaxValidation'=>false,
)); ?>
    <?php echo MyFormat::BindNotifyMsg(); ?>
    <p class="note">Dòng có dấu <span class="required">*</span> là bắt buộc.</p>
    
    <div class="row">
        <?php echo $form->labelEx($model->mProfile,'country_id'); ?>
        <?php echo $form->dropDownList($model->mProfile,'country_id', $model->mProfile->getArrCountry(), array('class'=>'')); ?>
        <?php echo $form->error($model->mProfile,'country_id'); ?>
    </div>
    <div class="row">
        <?php echo $form->labelEx($model->mProfile,'type_nation'); ?>
        <?php echo $form->dropDownList($model->mProfile,'type_nation', $model->mProfile->getArrNation(), array('class'=>'')); ?>
        <?php echo $form->error($model->mProfile,'type_nation'); ?>
    </div>
    <div class="row">
        <?php echo $form->labelEx($model->mProfile,'type_religion'); ?>
        <?php echo $form->dropDownList($model->mProfile,'type_religion', $model->mProfile->getArrReligion(), array('class'=>'')); ?>
        <?php echo $form->error($model->mProfile,'type_religion'); ?>
    </div>
    
    <div class="row">
        <?php echo $form->labelEx($model->mProfile,'type_education'); ?>
        <?php echo $form->dropDownList($model->mProfile,'type_education', $model->mProfile->getArrEducation(), array('class'=>'')); ?>
        <?php echo $form->error($model->mProfile,'type_education'); ?>
    </div>
    <div class="row">
        <?php echo $form->labelEx($model->mProfile,'id_number'); ?>
        <?php echo $form->textField($model->mProfile,'id_number',array('class'=>'w-400', 'maxlength'=>250)); ?>
        <?php echo $form->error($model->mProfile,'id_number'); ?>
    </div>
    <div class="row">
        <?php echo $form->labelEx($model->mProfile,'id_province'); ?>
        <?php echo $form->dropDownList($model->mProfile,'id_province', GasProvince::getArrAll(), array('class'=>'')); ?>
        <?php echo $form->error($model->mProfile,'id_province'); ?>
    </div>
    
    <div class="row">
        <?php echo $form->labelEx($model->mProfile,'id_created_date', []); ?>
        <?php 
            $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                'model'=>$model->mProfile,        
                'attribute'=>'id_created_date',
                'options'=>array(
                    'showAnim'=>'fold',
                    'dateFormat'=> ActiveRecord::getDateFormatJquery(),
    //                            'minDate'=> '0',
                    'maxDate'=> '0',
                    'changeMonth' => true,
                    'changeYear' => true,
                    'showOn' => 'button',
                    'buttonImage'=> Yii::app()->theme->baseUrl.'/admin/images/icon_calendar_r.gif',
                    'buttonImageOnly'=> true,                                
                ),        
                'htmlOptions'=>array(
                    'class'=>'w-16',
                    'style'=>'height:20px;',
                ),
            ));
        ?>     		
        <?php echo $form->error($model->mProfile,'id_created_date'); ?>
    </div>
    <div class="row">
        <?php echo $form->labelEx($model->mProfile,'status_marital'); ?>
        <?php echo $form->dropDownList($model->mProfile,'status_marital', $model->mProfile->getArrStatusMarital(), array('class'=>'')); ?>
        <?php echo $form->error($model->mProfile,'status_marital'); ?>
    </div>
    
    <div class="row">
        <?php echo $form->labelEx($model->mProfile,'date_begin_job', []); ?>
        <?php 
            $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                'model'=>$model->mProfile,        
                'attribute'=>'date_begin_job',
                'options'=>array(
                    'showAnim'=>'fold',
                    'dateFormat'=> ActiveRecord::getDateFormatJquery(),
                    'changeMonth' => true,
                    'changeYear' => true,
                    'showOn' => 'button',
                    'buttonImage'=> Yii::app()->theme->baseUrl.'/admin/images/icon_calendar_r.gif',
                    'buttonImageOnly'=> true,                                
                ),        
                'htmlOptions'=>array(
                    'class'=>'w-16',
                    'style'=>'height:20px;',
                ),
            ));
        ?>     		
        <?php echo $form->error($model->mProfile,'date_begin_job'); ?>
    </div>
    
    <div class="row">
        <?php echo $form->labelEx($model->mProfile,'contract_type'); ?>
        <?php echo $form->dropDownList($model->mProfile,'contract_type', $model->mProfile->getArrContractType(), array('class'=>'')); ?>
        <?php echo $form->error($model->mProfile,'contract_type'); ?>
    </div>
    
    <div class="row">
        <?php echo $form->labelEx($model->mProfile,'contract_begin', []); ?>
        <?php 
            $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                'model'=>$model->mProfile,        
                'attribute'=>'contract_begin',
                'options'=>array(
                    'showAnim'=>'fold',
                    'dateFormat'=> ActiveRecord::getDateFormatJquery(),
                    'changeMonth' => true,
                    'changeYear' => true,
                    'showOn' => 'button',
                    'buttonImage'=> Yii::app()->theme->baseUrl.'/admin/images/icon_calendar_r.gif',
                    'buttonImageOnly'=> true,                                
                ),        
                'htmlOptions'=>array(
                    'class'=>'w-16',
                    'style'=>'height:20px;',
                ),
            ));
        ?>     		
        <?php echo $form->error($model->mProfile,'contract_begin'); ?>
    </div>
    
    <div class="row">
        <?php echo $form->labelEx($model->mProfile,'contract_end', []); ?>
        <?php 
            $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                'model'=>$model->mProfile,        
                'attribute'=>'contract_end',
                'options'=>array(
                    'showAnim'=>'fold',
                    'dateFormat'=> ActiveRecord::getDateFormatJquery(),
                    'changeMonth' => true,
                    'changeYear' => true,
                    'showOn' => 'button',
                    'buttonImage'=> Yii::app()->theme->baseUrl.'/admin/images/icon_calendar_r.gif',
                    'buttonImageOnly'=> true,                                
                ),        
                'htmlOptions'=>array(
                    'class'=>'w-16',
                    'style'=>'height:20px;',
                ),
            ));
        ?>     		
        <?php echo $form->error($model->mProfile,'contract_end'); ?>
    </div>
    
    <div class="row">
        <?php echo $form->labelEx($model->mProfile,'id_number'); ?>
        <?php echo $form->textField($model->mProfile,'id_number',array('class'=>'w-400', 'maxlength'=>250)); ?>
        <?php echo $form->error($model->mProfile,'id_number'); ?>
    </div>
    <div class="row">
        <?php echo $form->labelEx($model->mProfile,'social_insurance_no'); ?>
        <?php echo $form->textField($model->mProfile,'social_insurance_no',array('class'=>'w-400', 'maxlength'=>250)); ?>
        <?php echo $form->error($model->mProfile,'social_insurance_no'); ?>
    </div>
    <div class="row">
        <?php echo $form->labelEx($model->mProfile,'tax_no'); ?>
        <?php echo $form->textField($model->mProfile,'tax_no',array('class'=>'w-400', 'maxlength'=>250)); ?>
        <?php echo $form->error($model->mProfile,'tax_no'); ?>
    </div>
    <div class="row">
        <?php echo $form->labelEx($model->mProfile,'bank_no'); ?>
        <?php echo $form->textField($model->mProfile,'bank_no',array('class'=>'w-400', 'maxlength'=>250)); ?>
        <?php echo $form->error($model->mProfile,'bank_no'); ?>
    </div>
    <div class="row">
        <?php echo $form->labelEx($model->mProfile,'bank_id'); ?>
        <?php echo $form->dropDownList($model->mProfile,'bank_id', $model->mProfile->getListdataBank(), array('class'=>'')); ?>
        <?php echo $form->error($model->mProfile,'bank_id'); ?>
    </div>
    <div class="row">
        <?php echo $form->labelEx($model->mProfile,'address_born'); ?>
        <?php echo $form->textField($model->mProfile,'address_born',array('class'=>'w-400', 'maxlength'=>250)); ?>
        <?php echo $form->error($model->mProfile,'address_born'); ?>
    </div>
    
    <div class="row">
        <?php echo $form->labelEx($model->mProfile,'id_province'); ?>
        <?php echo $form->dropDownList($model->mProfile,'id_province', GasProvince::getArrAll(), array('class'=>'')); ?>
        <?php echo $form->error($model->mProfile,'id_province'); ?>
    </div>
    
    <div class="row">
        <?php echo $form->labelEx($model->mProfile,'address_home'); ?>
        <?php echo $form->textField($model->mProfile,'address_home',array('class'=>'w-400', 'maxlength'=>250)); ?>
        <?php echo $form->error($model->mProfile,'address_home'); ?>
    </div>
    
    <div class="row">
        <?php echo $form->labelEx($model->mProfile,'address_home_province'); ?>
        <?php echo $form->dropDownList($model->mProfile,'address_home_province', GasProvince::getArrAll(), array('class'=>'')); ?>
        <?php echo $form->error($model->mProfile,'address_home_province'); ?>
    </div>
    <div class="row">
        <?php echo $form->labelEx($model->mProfile,'address_live'); ?>
        <?php echo $form->textField($model->mProfile,'address_live',array('class'=>'w-400', 'maxlength'=>250)); ?>
        <?php echo $form->error($model->mProfile,'address_live'); ?>
    </div>
    
    <div class="row">
        <?php echo $form->labelEx($model->mProfile,'address_live_province'); ?>
        <?php echo $form->dropDownList($model->mProfile,'address_live_province', GasProvince::getArrAll(), array('class'=>'')); ?>
        <?php echo $form->error($model->mProfile,'address_live_province'); ?>
    </div>
    
    <div class="row">
        <?php echo $form->labelEx($model->mProfile,'list_email'); ?>
        <?php echo $form->textField($model->mProfile,'list_email',array('class'=>'w-400', 'maxlength'=>250)); ?>
        <?php echo $form->error($model->mProfile,'list_email'); ?>
    </div>
    <div class="row">
        <?php echo $form->labelEx($model->mProfile,'list_phone'); ?>
        <?php echo $form->textField($model->mProfile,'list_phone',array('class'=>'w-400', 'maxlength'=>250)); ?>
        <?php echo $form->error($model->mProfile,'list_phone'); ?>
    </div>
    <div class="row">
        <?php echo $form->labelEx($model->mProfile,'list_phone_family'); ?>
        <?php echo $form->textField($model->mProfile,'list_phone_family',array('class'=>'w-400', 'maxlength'=>250)); ?>
        <?php echo $form->error($model->mProfile,'list_phone_family'); ?>
    </div>
    
    
    <div class="row">
        <?php echo $form->labelEx($model,'role_id'); ?>                
        <?php echo $form->dropDownList($model,'role_id',Roles::getDataSelect(Roles::getRestrict()),array('class'=>'w-400', 'empty'=>'Select')); ?>
        <?php echo $form->error($model,'role_id'); ?>
    </div>
    <div class="row">
        <?php echo $form->labelEx($model,'first_name'); ?>
        <?php echo $form->textField($model,'first_name',array('class'=>'w-400','maxlength'=>150)); ?>
        <?php echo $form->error($model,'first_name'); ?>
    </div>
    
    <div class="row">
        <?php echo $form->labelEx($model,'username'); ?>
        <?php echo $form->textField($model,'username',array('class'=>'w-400', 'maxlength'=>250)); ?>
        <?php echo $form->error($model,'username'); ?>
    </div>		

    <div class="row <?php echo $display_password;?>">
        <?php if (!$model->isNewRecord):?>
            <div style="width: 100%;float: left;padding-bottom: 5px;padding-left: 140px;">
                <label style="color: red;width: auto; ">Bỏ trống bên dưới nếu bạn không muốn đổi mật khẩu hiện tại</label>
            </div>
        <?php endif?>
        <?php echo $form->labelEx($model,'password_hash', ['label'=>'Mật khẩu']); ?>
        <?php echo $form->passwordField($model,'password_hash',array('style'=>'width:450px','maxlength'=>50,'value'=>'')); ?>
        <?php echo $form->error($model,'password_hash'); ?>
    </div>

    <div class="row <?php echo $display_password;?>">
        <?php echo $form->labelEx($model,'password_confirm', ['label'=>'Xác nhận mật khẩu']); ?>
        <?php echo $form->passwordField($model,'password_confirm',array('style'=>'width:450px','maxlength'=>50,'value'=>'')); ?>
        <?php echo $form->error($model,'password_confirm'); ?>
    </div>    
    <div class="row">
        <?php echo $form->labelEx($model,'email'); ?>
        <?php echo $form->textField($model,'email',array('class'=>'w-400','maxlength'=>250)); ?>
        <?php echo $form->error($model,'email'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model,'code_bussiness'); ?>
        <?php echo $form->textField($model,'code_bussiness',array('class'=>'w-400','readonly'=>true)); ?>
        <?php echo $form->error($model,'code_bussiness'); ?>
    </div>	

    <div class="row">
        <?php echo $form->labelEx($model,'address'); ?>
        <?php echo $form->textArea($model,'address',array('class'=>'w-400', 'readonly'=>1)); ?>
        <?php echo $form->error($model,'address'); ?>
    </div>
    <?php include '_form_address.php'; ?>

    <div class="row">
        <?php echo $form->labelEx($model,'status'); ?>
        <?php echo $form->dropDownList($model,'status', ActiveRecord::getUserStatus()); ?>
        <?php echo $form->error($model,'status'); ?>
    </div>
    <div class="row">
        <?php echo $form->labelEx($model,'first_char'); ?>
        <?php echo $form->dropDownList($model,'first_char', Users::$aGender, ['empty'=>'Select']); ?>
        <?php echo $form->error($model,'first_char'); ?>
    </div>
    
    <div class="row">
        <?php echo $form->labelEx($model,'last_purchase', ['label'=>'Ngày Sinh']); ?>
        <?php 
            $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                'model'=>$model,        
                'attribute'=>'last_purchase',
                'options'=>array(
                    'showAnim'=>'fold',
                    'dateFormat'=> ActiveRecord::getDateFormatJquery(),
    //                            'minDate'=> '0',
                    'maxDate'=> '0',
                    'changeMonth' => true,
                    'changeYear' => true,
                    'showOn' => 'button',
                    'buttonImage'=> Yii::app()->theme->baseUrl.'/admin/images/icon_calendar_r.gif',
                    'buttonImageOnly'=> true,                                
                ),        
                'htmlOptions'=>array(
                    'class'=>'w-16',
                    'style'=>'height:20px;',
                ),
            ));
        ?>     		
        <?php echo $form->error($model,'last_purchase'); ?>
    </div>

    <div class="row buttons" style="padding-left: 141px;">
        <?php $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType'=>'submit',
            'label'=>$model->isNewRecord ? Yii::t('translation', 'Create') : Yii::t('translation', 'Save'),
            'type'=>'null', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
            'size'=>'small', // null, 'large', 'small' or 'mini'
            'htmlOptions' => array('class' => 'submit-blockui'),
            //'htmlOptions' => array('style' => 'margin-bottom: 10px; float: right;'),
        )); ?>	
    </div>

<?php $this->endWidget(); ?>

</div><!-- form -->

<script>
$(document).ready(function() {
    $('.employees').addClass('index').find('span').text('Quản lý');
    $('.employees_create').addClass('create').find('span').text('Tạo mới');
    $('.employees_view').addClass('view').find('span').text('Xem');
    
    $('#Users_province_id').change(function(){
        var province_id = $(this).val();        
//        if($.trim(gender)==''){
//            $('#Products_category_id').html('<option value="">Select category</option>');            
//            return;
//        }                    
        var url_ = "<?php echo Yii::app()->createAbsoluteUrl('admin/gascustomer/create');?>";
//		$('#Products_category_id').html('<option value="">Select category</option>');            
    $.blockUI({ overlayCSS: { backgroundColor: '<?php echo BLOCK_UI_COLOR;?>' } }); 
        $.ajax({
            url: url_,
            data: {ajax:1,province_id:province_id},
            type: "get",
            dataType:'json',
            success: function(data){
                $('#Users_district_id').html(data['html_district']);
                $('#Users_storehouse_id').html(data['html_store']);
                $.unblockUI();
            }
        });
        
    });    
    
    $('.submit-blockui').live('click',function(){
       $('.form').block({
           message: '', 
           overlayCSS:  { backgroundColor: '#fff' }
      }); 
//           $('.form').unblock(); 
   });

   $('.form').find('button:submit').click(function(){
        $.blockUI({ overlayCSS: { backgroundColor: '#fff' } }); 
    });

    
});

</script>
