<h3 class='title-info'>Thông tin cá nhân</h3>
<?php $this->widget('application.components.widgets.XDetailView', array(
    'data' => $mUsersProfile,
    'attributes' => array(
        'group11'=>array(
            'ItemColumns' => 4,
            'attributes' => array(
                    array(
                        'name' => 'id_number',
                        'value' => $mUsersProfile->getIdNumber(),
                    ),
                    array(
                        'name'    => 'id_created_date',
                        'value'     => $mUsersProfile->getIdCreatedDate(),
                    ),
                    array(
                        'name'    => 'id_province',
                        'value'     => $mUsersProfile->getIdProvince(),
                    ),
                    array(
                        'name'    => 'status_marital',
                        'value'     => $mUsersProfile->getStatusMarital(),
                    ),
                    array(
                        'name'    => 'country_id',
                        'value'     => $mUsersProfile->getCountry(),
                    ),
                    array(
                        'name'    => 'type_nation',
                        'value'     => $mUsersProfile->getTypeNation(),
                    ),
                    array(
                        'name'    => 'type_religion',
                        'value'     => $mUsersProfile->getTypeReligion(),
                    ),
                    array(
                        'name'    => 'type_education',
                        'value'     => $mUsersProfile->getTypeEducation(),
                    ),
                    array(
                        'name'    => 'address_born',
                        'value'     => $mUsersProfile->address_born,
                    ),
                    array(
                        'label'    => 'Ngày sinh',
                        'name'    => 'last_purchase',
                        'value'     => $model->getLastPurchase(),
                    ),
                    array(
                        'name'    => 'address_home',
                        'value'     => $mUsersProfile->address_home,
                    ),
                    array(
                        'name'    => 'address_home_province',
                        'value'     => $mUsersProfile->getAddressHomeProvince(),
                    ),
                    array(
                        'name'    => 'address_live',
                        'value'     => $mUsersProfile->address_live,
                    ),
                    array(
                        'name'    => 'address_live_province',
                        'value'     => $mUsersProfile->getAddressLiveProvince(),
                    ),
                    array(
                        'name'    => 'list_email',
                        'value'     => $mUsersProfile->list_email,
                    ),
                    array(
                        'name'    => 'list_phone',
                        'value'     => $mUsersProfile->list_phone,
                    ),
                    array(
                        'name'    => 'list_phone_family',
                        'value'     => $mUsersProfile->list_phone_family,
                    ),
                    array(
                        'name'    => 'note',
                        'value'     => $mUsersProfile->getNote(),
                    ),
                    array(
                        'label'    => 'File ảnh đại diện',
                        'type'    => 'raw',
                        'value'     => $mUsersRef->getAvatar(),
                    ),
                    array(
                        'label'    => 'Người tạo',
                        'type'    => 'raw',
                        'value'     => $model->getCreatedBy(),
                    ),
                    array(
                        'label'    => 'Ngày Tạo',
                        'type'    => 'raw',
                        'value'     => $model->getCreatedDate(),
                    ),
            ),
        ),
        
    ),
)); ?>