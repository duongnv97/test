<div class="">
    <div class="row">
        <?php echo $form->labelEx($model,'province_id'); ?>
        <?php echo $form->dropDownList($model,'province_id', GasProvince::getArrAllFix(),array('class'=>'w-400','empty'=>'Select')); ?>
        <?php echo $form->error($model,'province_id'); ?>
    </div>
    <div class="row">
        <?php echo $form->labelEx($model,'district_id'); ?>
        <?php echo $form->dropDownList($model,'district_id', GasDistrict::getArrAll($model->province_id),array('class'=>'w-400 float_l')); ?>
        <div class="add_new_item float_l <?php echo $hideFieldFamily;?>">
            <?php if($canCreateAdd): ?>
                <a class="iframe_create_district" href="<?php echo Yii::app()->createAbsoluteUrl('admin/ajax/iframe_create_district') ;?>">Tạo Mới Quận Huyện</a><em> (Nếu trong danh sách không có)</em>
            <?php else: ?>
                <?php echo $textHelpAddress;?>
            <?php endif; ?>
        </div>
        <?php echo $form->error($model,'district_id'); ?>
    </div>
    <div class="clr"></div>

    <div class="row">
        <?php echo $form->labelEx($model,'ward_id'); ?>
        <?php echo $form->dropDownList($model,'ward_id', GasWard::getArrAll($model->province_id, $model->district_id),array('class'=>'w-400','empty'=>'')); ?>
        <?php if($canCreateAdd): ?>
        <div class="add_new_item <?php echo $hideFieldFamily;?>"><a class="iframe_create_ward" href="<?php echo Yii::app()->createAbsoluteUrl('admin/ajax/iframe_create_ward') ;?>">Tạo Mới Phường Xã</a><em> (Nếu trong danh sách không có)</em></div>
        <?php endif; ?>
        <?php echo $form->error($model,'ward_id'); ?>
    </div>	

    <div class="row">
        <?php echo $form->labelEx($model,'house_numbers') ?>
        <?php echo $form->textField($model,'house_numbers',array('class'=>'w-400','maxlength'=>100)); ?>
        <?php echo $form->error($model,'house_numbers'); ?>
    </div> 	

    
    <div class="grp-cot clearfix">
        <div class="sk-col-md-6">
            <div class="sk-col-md-3 lable-text">
                <?php echo $form->labelEx($model,'street_id'); ?>
            </div>
            <div class="sk-col-md-9 input-content ">
                <?php echo $form->hiddenField($model,'street_id', array()); ?>
                <?php 
                    // widget auto complete search user customer and supplier
                    $aData = array(
                        'model'=>$model,
                        'field_street_id'=>'street_id',
                        'field_autocomplete_name'=>'autocomplete_name_street',
                        'url'=> Yii::app()->createAbsoluteUrl('admin/ajax/autocomplete_data_streets'),
                        'NameRelation'=>'street',
                        'ClassAdd' => 'w-350',
                    );
                    $this->widget('ext.GasAutoStreet.GasAutoStreet',
                        array('data'=>$aData));
                ?>
            </div><?php echo $form->error($model,'street_id'); ?>
        </div>
        <div class="sk-col-md-6 <?php echo $hideFieldFamily;?>">
            <?php // if($canCreateAdd): ?>
            <?php if(0): ?>
                <div class="add_new_item"><a class="iframe_create_street" href="<?php echo Yii::app()->createAbsoluteUrl('admin/ajax/iframe_create_street') ;?>">Tạo Mới Đường</a><em> (Nếu trong danh sách không có)</em></div>
            <?php endif; ?>
        </div>
    </div>
</div>

<script>
    $(document).ready(function(){
        $('#Users_province_id').live('change',function(){
            var province_id = $(this).val();        
            var url_ = "<?php echo Yii::app()->createAbsoluteUrl('admin/ajax/get_slt_district');?>";
            $.blockUI({ overlayCSS: { backgroundColor: '<?php echo BLOCK_UI_COLOR;?>' } }); 
            $.ajax({
                url: url_,
                data: {ajax:1,province_id:province_id},
                type: "get",
                dataType:'json',
                success: function(data){
                    $('#Users_district_id').html(data['html_district']);                
                    $.unblockUI();
                }
            });
        });   		
	
        $('#Users_district_id').live('change',function(){
            var province_id = $('#Users_province_id').val();   
            var district_id = $(this).val();        
            var url_ = "<?php echo Yii::app()->createAbsoluteUrl('admin/ajax/get_slt_ward');?>";
            $.blockUI({ overlayCSS: { backgroundColor: '<?php echo BLOCK_UI_COLOR;?>' } }); 
            $.ajax({
                url: url_,
                data: {ajax:1,district_id:district_id,province_id:province_id},
                type: "get",
                dataType:'json',
                success: function(data){
                        $('#Users_ward_id').html(data['html_district']);                
                        $.unblockUI();
                }
            });
        });

        fnUpdateColorbox();
        fnBindFixLabelRequired();
        fnBindRemoveIcon();
        $('#Users_assign_employee_sales').closest('.unique_wrap_autocomplete ').find('.remove_row_item').trigger('click');
    });
    
    function fnUpdateColorbox(){    
        $(".iframe_create_district").colorbox({iframe:true,innerHeight:'400', innerWidth: '700',close: "<span title='close'>close</span>"});
        $(".iframe_create_ward").colorbox({iframe:true,innerHeight:'400', innerWidth: '700',close: "<span title='close'>close</span>"});
        $(".iframe_create_street").colorbox({iframe:true,innerHeight:'400', innerWidth: '700',close: "<span title='close'>close</span>"});
    }    
	
</script>