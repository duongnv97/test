<?php
$this->breadcrumbs=array(
	'QL nhân sự'=>array('employees'),
	$model->code_account." - ".$model->first_name,
);
$aIndex = array('employees');
if($model->role_id == ROLE_CANDIDATE){
    $aIndex = array('employees', 'tuyendung'=>1);
}

$menus = array(	
    array('label'=> 'Quản lý', 'url'=> $aIndex),
    array('label'=>'Tạo mới', 'url'=>array('employees_create')),
    array('label'=>'Cập nhật', 'url'=>array('employees_update', 'id'=>$model->id)),
);
$this->menu= ControllerActionsName::createMenusRoles($menus, $actions);
$mUsersRef = $model->mUsersRef;
?>
<h1>Xem Thông Tin Nhân Viên: <?php echo $model->code_account." - ".$model->first_name; ?></h1>
<?php include 'Employees_view_xdetail.php'; ?>
<script>
$(document).ready(function() {
    jQuery('a.gallery').colorbox({ opacity:0.5 , rel:'group1' });
    $('.employees').addClass('index').find('span').text('Quản lý');
    $('.employees_create').addClass('create').find('span').text('Tạo mới');
    $('.employees_update').addClass('update').find('span').text('Cập nhật');
});
</script>