<div class="form">
    <?php
        $linkNormal         = Yii::app()->createAbsoluteUrl('admin/gasmember/employees');
        $linkStatusLeave    = Yii::app()->createAbsoluteUrl('admin/gasmember/employees', ['status_leave'=> UsersProfile::STATUS_LEAVE_WAIT]);
    ?>
    <h1><?php echo $this->pageTitle; ?>
        <a class='btn_cancel f_size_14 <?php echo (!isset($_GET['status_leave'])) ? 'active':'';?>' href="<?php echo $linkNormal;?>">Toàn bộ</a>
        <a class='btn_cancel f_size_14 <?php echo isset($_GET['status_leave']) && $_GET['status_leave'] == UsersProfile::STATUS_LEAVE_WAIT ? 'active':'';?>' href="<?php echo $linkStatusLeave;?>">Chờ nghỉ việc</a>
    </h1>
</div>