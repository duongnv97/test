<h3 class='title-info'>Thông tin Lương, BHXH</h3>

<div class="grp-cot clearfix">
    <div class="sk-col-md-6 ">
        <div class="sk-col-md-2 lable-text">
            <?php echo $form->labelEx($model,'payment_day', ['label'=>'Trả lương']); ?>
        </div>
        <div class="sk-col-md-4 input-content radiobox item_l">
            <?php echo $form->radioButtonList($model,'payment_day', CmsFormatter::$yesNoFormat,
                array(
                    'separator'=>"",
                    'template'=>'<li>{input}{label}</li>',
                    'container'=>'ul',
                    'class'=>'radiobox' 
                )); 
            ?>
        </div>
    </div> 
</div>

<div class="grp-cot clearfix">
    <div class="sk-col-md-12">
        <div class="sk-col-md-3">
            <div class="sk-col-md-3 lable-text">
                <?php echo $form->labelEx($model->mProfile,'contract_type'); ?>
            </div>
            <div class="sk-col-md-9 input-content ">
                <?php echo $form->dropDownList($model->mProfile,'contract_type', $model->mProfile->getArrContractType(), ['class'=>'full-control', 'empty'=>'Select']); ?>
                <?php echo $form->error($model->mProfile,'contract_type'); ?>
            </div>
        </div>
        <div class="sk-col-md-3">
            
        </div>
        
        <div class="sk-col-md-3">
            <div class="sk-col-md-3 lable-text">
                <?php echo $form->labelEx($model->mProfile,'contract_begin'); ?>
            </div>
            <div class="sk-col-md-9 input-content ">
                <?php 
                    $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                        'model'=>$model->mProfile,        
                        'attribute'=>'contract_begin',
                        'options'=>array(
                            'showAnim'=>'fold',
                            'dateFormat'=> ActiveRecord::getDateFormatJquery(),
                            'changeMonth' => true,
                            'changeYear' => true,
                            'showOn' => 'button',
                            'buttonImage'=> Yii::app()->theme->baseUrl.'/admin/images/icon_calendar_r.gif',
                            'buttonImageOnly'=> true,                                
                        ),        
                        'htmlOptions'=>array(
                            'class'=>'w-150',
                        ),
                    ));
                ?>     		
                <?php echo $form->error($model->mProfile,'contract_begin'); ?>
            </div>
        </div>
        
        <div class="sk-col-md-3">
            <div class="sk-col-md-3 lable-text">
                <?php echo $form->labelEx($model->mProfile,'contract_end'); ?>
            </div>
            <div class="sk-col-md-9 input-content ">
                <?php 
                    $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                        'model'=>$model->mProfile,        
                        'attribute'=>'contract_end',
                        'options'=>array(
                            'showAnim'=>'fold',
                            'dateFormat'=> ActiveRecord::getDateFormatJquery(),
                            'changeMonth' => true,
                            'changeYear' => true,
                            'showOn' => 'button',
                            'buttonImage'=> Yii::app()->theme->baseUrl.'/admin/images/icon_calendar_r.gif',
                            'buttonImageOnly'=> true,                                
                        ),        
                        'htmlOptions'=>array(
                            'class'=>'w-150',
                        ),
                    ));
                ?>     		
                <?php echo $form->error($model->mProfile,'contract_end'); ?>
            </div>
        </div>
        
        
    </div>
</div>

<div class="grp-cot clearfix">
    <div class="sk-col-md-12">
        <div class="sk-col-md-3">
            <div class="sk-col-md-3 lable-text">
                <?php echo $form->labelEx($model->mProfile,'base_salary'); ?>
            </div>
            <div class="sk-col-md-9 input-content ">
                <?php echo $form->textField($model->mProfile,'base_salary', ['class'=>'full-control ad_fix_currency', 'maxlength'=>250]); ?>
                <?php echo $form->error($model->mProfile,'base_salary'); ?>
            </div>
        </div>
        <div class="sk-col-md-3">
            <div class="sk-col-md-3 lable-text">
                <?php echo $form->labelEx($model->mProfile,'salary_insurance'); ?>
            </div>
            <div class="sk-col-md-9 input-content ">
                <?php echo $form->textField($model->mProfile,'salary_insurance', ['class'=>'full-control ad_fix_currency', 'maxlength'=>250]); ?>
                <?php echo $form->error($model->mProfile,'salary_insurance'); ?>
            </div>
        </div>
        <div class="sk-col-md-3">
            <div class="sk-col-md-3 lable-text">
                <?php echo $form->labelEx($model,'storehouse_id'); ?>
            </div>
            <div class="sk-col-md-9 input-content ">
                <?php echo $form->dropDownList($model,'storehouse_id', Borrow::model()->getListdataCompany(),array('class'=>'full-control', 'empty'=>'Select')); ?>
                <?php echo $form->error($model,'storehouse_id'); ?>
            </div>
        </div>
        
    </div>
</div>

<div class="grp-cot clearfix">
    <div class="sk-col-md-12">
        <div class="sk-col-md-3">
            <div class="sk-col-md-3 lable-text">
                <?php echo $form->labelEx($model->mProfile,'tax_no'); ?>
            </div>
            <div class="sk-col-md-9 input-content ">
                <?php echo $form->textField($model->mProfile,'tax_no', ['class'=>'full-control', 'maxlength'=>250]); ?>
                <?php echo $form->error($model->mProfile,'tax_no'); ?>
            </div>
        </div>
        <div class="sk-col-md-3">
            <div class="sk-col-md-3 lable-text">
                <?php echo $form->labelEx($model->mProfile,'social_insurance_no'); ?>
            </div>
            <div class="sk-col-md-9 input-content ">
                <?php echo $form->textField($model->mProfile,'social_insurance_no',array('class'=>'full-control', 'maxlength'=>250)); ?>
                <?php echo $form->error($model->mProfile,'social_insurance_no'); ?>
            </div>
        </div>
        <div class="sk-col-md-3">
            <div class="sk-col-md-3 lable-text">
                <?php echo $form->labelEx($model->mProfile,'insurance_start'); ?>
            </div>
            <div class="sk-col-md-9 input-content ">
                <?php 
                    $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                        'model'=>$model->mProfile,        
                        'attribute'=>'insurance_start',
                        'options'=>array(
                            'showAnim'=>'fold',
                            'dateFormat'=> ActiveRecord::getDateFormatJquery(),
                            'changeMonth' => true,
                            'changeYear' => true,
                            'showOn' => 'button',
                            'buttonImage'=> Yii::app()->theme->baseUrl.'/admin/images/icon_calendar_r.gif',
                            'buttonImageOnly'=> true,                                
                        ),        
                        'htmlOptions'=>array(
                            'class'=>'w-150',
                        ),
                    ));
                ?>     		
                <?php echo $form->error($model->mProfile,'insurance_start'); ?>
            </div>
        </div>
        <div class="sk-col-md-3 display_none">
            <div class="sk-col-md-3 lable-text">
                <?php echo $form->labelEx($model->mProfile,'insurance_end'); ?>
            </div>
            <div class="sk-col-md-9 input-content ">
                <?php 
                    $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                        'model'=>$model->mProfile,        
                        'attribute'=>'insurance_end',
                        'options'=>array(
                            'showAnim'=>'fold',
                            'dateFormat'=> ActiveRecord::getDateFormatJquery(),
                            'changeMonth' => true,
                            'changeYear' => true,
                            'showOn' => 'button',
                            'buttonImage'=> Yii::app()->theme->baseUrl.'/admin/images/icon_calendar_r.gif',
                            'buttonImageOnly'=> true,                                
                        ),        
                        'htmlOptions'=>array(
                            'class'=>'w-150',
                        ),
                    ));
                ?>     		
                <?php echo $form->error($model->mProfile,'insurance_end'); ?>
            </div>
        </div>
    </div>
</div>

<div class="grp-cot clearfix">
    <div class="sk-col-md-6 ">
        <div class="sk-col-md-2 lable-text"> 
            <?php echo $form->labelEx($model->mProfile,'profile_status', []); ?>
        </div>
        <div class="sk-col-md-8 input-content radiobox item_l">
            <?php echo $form->radioButtonList($model->mProfile,'profile_status', $model->mProfile->getArrProfileStatus(),
                array(
                    'separator'=>"",
                    'template'=>'<li>{input}{label}</li>',
                    'container'=>'ul',
                    'class'=>'radiobox' 
                )); 
            ?>
        </div>
    </div> 
</div>
