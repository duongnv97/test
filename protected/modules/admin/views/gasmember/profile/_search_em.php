<?php 
    $mProfile = new UsersProfile();
    $url = Yii::app()->createAbsoluteUrl('admin/gasmember/employees');
    if(isset($_GET['tuyendung'])){
        $url = Yii::app()->createAbsoluteUrl('admin/gasmember/employees', ['tuyendung'=>1]);
    }

?>
<div class="wide form">
<?php $form=$this->beginWidget('CActiveForm', array(
	'action' => $url,
	'method'=>'get',
)); ?>
    
    <div class="row display_none">
    <?php echo $form->label($model,'id', array('label'=>'Nhân viên')); ?>
    <?php echo $form->hiddenField($model,'id', array('class'=>'')); ?>
    <?php
            // 1. limit search kh của sale
            $url = Yii::app()->createAbsoluteUrl('admin/ajax/search_for_user_login', []);
            // widget auto complete search user customer and supplier
            $aData = array(
                'model'=>$model,
                'field_customer_id'=>'id',
                'url'=> $url,
                'name_relation_user'=>'sale',
                'ClassAdd' => 'w-400',
                'field_autocomplete_name' => 'autocomplete_name',
                'placeholder'=>'Nhập mã hoặc tên nhân viên',
            );
            $this->widget('ext.SpaAutocompleteCustomer.SpaAutocompleteCustomer',
                array('data'=>$aData));
            ?>
    </div>
        
    
    <div class="row">
    <?php echo $form->labelEx($model,'parent_id', array('label'=>'Nơi công tác')); ?>
    <?php echo $form->hiddenField($model,'parent_id', array('class'=>'')); ?>
    <?php
            // 1. limit search kh của sale
            $url = Yii::app()->createAbsoluteUrl('admin/ajax/search_by_role', array('role'=> ROLE_AGENT));
            // widget auto complete search user customer and supplier
            $aData = array(
                'model'=>$model,
                'field_customer_id'=>'parent_id',
                'url'=> $url,
                'name_relation_user'=>'rParent',
                'ClassAdd' => 'w-400',
                'field_autocomplete_name' => 'md5pass',
                'placeholder'=>'Nhập mã hoặc tên đại lý',
            );
            $this->widget('ext.SpaAutocompleteCustomer.SpaAutocompleteCustomer',
                array('data'=>$aData));
            ?>
    </div>

    <div class="row more_col">
        <div class="col1">
            <?php echo $form->label($model->mProfile,'id_number'); ?>
            <?php echo $form->textField($model->mProfile,'id_number',array('class'=>'w-200')); ?>
        </div>
        <div class="col2">
            <?php echo $form->label($model->mProfile,'position_work'); ?>
            <?php echo $form->dropDownList($model->mProfile,'position_work', $model->mProfile->getArrWorkRoom(), ['class'=>'w-200', 'empty'=>'Select']); ?>
        </div>
        <div class="col3">
            <?php echo $form->label($model->mProfile,'position_room'); ?>
            <?php echo $form->dropDownList($model->mProfile,'position_room', $model->mProfile->getArrPositionRoom(), ['class'=>'w-200', 'empty'=>'Select']); ?>
        </div>
    </div>
    <div class="row more_col">
        <div class="col1">
            <?php echo $form->label($model,'role_id'); ?>
            <?php echo $form->dropDownList($model,'role_id', $mProfile->getArrayRole(),array('class'=>'w-200','empty'=>'Select')); ?>
        </div>
        <div class="col2">
            <?php echo $form->label($model,'email',array()); ?>
            <?php echo $form->textField($model,'email',array('class'=>'w-200')); ?>
        </div>
        <div class="col3">
            <?php echo $form->label($model,'username', ['label'=>'Tài khoản đăng nhập web']); ?>
            <?php echo $form->textField($model,'username',array('class'=>'w-200')); ?>
        </div>
    </div>
    <div class="row more_col">
        <div class="col1">
            <?php echo $form->label($model,'storehouse_id'); ?>
            <?php echo $form->dropDownList($model,'storehouse_id', Borrow::model()->getListdataCompany(),array('class'=>'w-200','empty'=>'Select')); ?>
        </div>
        <div class="col2">
            <?php echo $form->label($model,'contract_type', ['label' => 'Loại hợp đồng']); ?>
            <?php echo $form->dropDownList($model,'contract_type', $mProfile->getArrContractType(),array('class'=>'w-200','empty'=>'Select')); ?>
        </div>
        <div class="col3">
            <?php echo $form->label($model,'status_leave', ['label' => $model->mProfile->getAttributeLabel('status_leave')]); ?>
            <?php echo $form->dropDownList($model,'status_leave', $mProfile->getArrStatusLeave(),array('class'=>'w-200','empty'=>'Select')); ?>
        </div>
    </div>
    <div class="row more_col">
        <div class="col1">
            <?php echo $form->labelEx($model,'province_id'); ?>
            <?php echo $form->dropDownList($model,'province_id', GasProvince::getArrAll(),array('class'=>'w-200', 'empty'=>'Select')); ?>
        </div>
        <div class="col2">
            <?php echo $form->labelEx($model,'district_id'); ?>
            <?php echo $form->dropDownList($model,'district_id', GasDistrict::getArrAll($model->province_id),array('class'=>'w-200', 'empty'=>'Select')); ?>
        </div>
        <div class="col3">
            <?php echo $form->label($model->mProfile,'salary_method'); ?>
            <?php echo $form->dropDownList($model->mProfile,'salary_method', $model->mProfile->getArraySalaryMethod(), ['class'=>'w-200', 'empty'=>'Select']); ?>
        </div>
    </div>
    <div class="row more_col">
        <div class="col1">
            <?php echo $form->label($model,'status',array()); ?>
            <?php echo $form->dropDownList($model,'status', $model->getArrayStatusProfile(), ['class'=>'w-200', 'empty'=>'Select']); ?>
        </div>
        <div class="col2">
            <?php echo $form->label($model,'payment_day', ['label'=>'Trả lương']); ?>
            <?php echo $form->dropDownList($model,'payment_day', CmsFormatter::$yesNoFormat, ['class'=>'w-200', 'empty'=>'Select']); ?>
        </div>
        <div class="col3">
            <?php $model->mProfile->id = -1; ?>
            <?php echo $form->label($model,'price', ['label'=>' Ký quỹ']); ?>
            <?php echo $form->dropDownList($model,'price', $model->mProfile->getArrayCredit($model), ['class'=>'w-200', 'empty'=>'Select']); ?>
        </div>
    </div>
    <div class="row more_col">
        <div class="col1">
            <?php echo $form->label($model,'profile_status', ['label'=> $model->mProfile->getAttributeLabel('profile_status')]); ?>
            <?php echo $form->dropDownList($model,'profile_status', $model->mProfile->getArrProfileStatus(), ['class'=>'w-200', 'empty'=>'Select']); ?>
        </div>
        <div class="col2">
        </div>
        <div class="col3">
        </div>
    </div>
    <div class="row more_col">
        <div class="col1">
            <?php echo $form->label($model,'first_name',array('label'=>'Tên nhân viên',)); ?>
            <?php echo $form->textField($model,'first_name',array('class'=>'w-200','placeholder'=>'nhập tiếng việt không dấu')); ?>
        </div>
        <div class="col2">
            <?php echo $form->label($model->mProfile,'date_begin_job_from'); ?>
            <?php 
                $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                    'model'=>$model->mProfile,        
                    'attribute'=>'date_begin_job_from',
                    'options'=>array(
                        'showAnim'=>'fold',
                        'dateFormat'=> MyFormat::$dateFormatSearch,
//                            'minDate'=> '0',
//                        'maxDate'=> '0',
                        'changeMonth' => true,
                        'changeYear' => true,
                        'showOn' => 'button',
                        'buttonImage'=> Yii::app()->theme->baseUrl.'/admin/images/icon_calendar_r.gif',
                        'buttonImageOnly'=> true,                                
                    ),        
                    'htmlOptions'=>array(
                        'class'=>'w-200',
                        'size'=>'16',
                        'style'=>'float:left;',                               
                    ),
                ));
            ?>
        </div>
        <div class="col3">
            <?php echo $form->label($model->mProfile,'date_begin_job_to'); ?>
            <?php 
                $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                    'model'=>$model->mProfile,        
                    'attribute'=>'date_begin_job_to',
                    'options'=>array(
                        'showAnim'=>'fold',
                        'dateFormat'=> MyFormat::$dateFormatSearch,
//                            'minDate'=> '0',
//                        'maxDate'=> '0',
                        'changeMonth' => true,
                        'changeYear' => true,
                        'showOn' => 'button',
                        'buttonImage'=> Yii::app()->theme->baseUrl.'/admin/images/icon_calendar_r.gif',
                        'buttonImageOnly'=> true,                                
                    ),        
                    'htmlOptions'=>array(
                        'class'=>'w-150',
                        'size'=>'16',
                        'style'=>'float:left;',
                    ),
                ));
            ?>     		
        </div>
    </div>

    <div class="row buttons" style="padding-left: 159px;">
                    <?php $this->widget('bootstrap.widgets.TbButton', array(
        'buttonType'=>'submit',
        'label'=>Yii::t('translation','Search'),
        'type'=>'null', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
        'size'=>'small', // null, 'large', 'small' or 'mini'
        //'htmlOptions' => array('style' => 'margin-bottom: 10px; float: right;'),
    )); ?>	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->