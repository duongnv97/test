<?php 
$display_password   = '';
if($model->isNewRecord){
//    $display_password = 'display_none';
}
$canCreateAdd = true;
?>
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/jquery.blockUI.js"></script>    
<div class="form">
<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'users-form',
	'enableAjaxValidation'=>false,
)); ?>
    <?php echo MyFormat::BindNotifyMsg(); ?>
    <p class="note">Dòng có dấu <span class="required">*</span> là bắt buộc.</p>
    <div class="row buttons" style="padding-left: 141px;">
        <?php $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType'=>'submit',
            'label'=>$model->isNewRecord ? Yii::t('translation', 'Create') : Yii::t('translation', 'Save'),
            'type'=>'null', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
            'size'=>'small', // null, 'large', 'small' or 'mini'
            'htmlOptions' => array('class' => 'submit-blockui'),
            //'htmlOptions' => array('style' => 'margin-bottom: 10px; float: right;'),
        )); ?>	
    </div>

<?php $this->endWidget(); ?>

</div><!-- form -->

<script>
$(document).ready(function() {
    $('.employees').addClass('index').find('span').text('Quản lý');
    $('.employees_create').addClass('create').find('span').text('Tạo mới');
    $('.employees_view').addClass('view').find('span').text('Xem');
    
    $('#Users_province_id').change(function(){
        var province_id = $(this).val();        
//        if($.trim(gender)==''){
//            $('#Products_category_id').html('<option value="">Select category</option>');            
//            return;
//        }                    
        var url_ = "<?php echo Yii::app()->createAbsoluteUrl('admin/gascustomer/create');?>";
//		$('#Products_category_id').html('<option value="">Select category</option>');            
    $.blockUI({ overlayCSS: { backgroundColor: '<?php echo BLOCK_UI_COLOR;?>' } }); 
        $.ajax({
            url: url_,
            data: {ajax:1,province_id:province_id},
            type: "get",
            dataType:'json',
            success: function(data){
                $('#Users_district_id').html(data['html_district']);
                $('#Users_storehouse_id').html(data['html_store']);
                $.unblockUI();
            }
        });
        
    });    
    
    $('.submit-blockui').live('click',function(){
       $('.form').block({
           message: '', 
           overlayCSS:  { backgroundColor: '#fff' }
      }); 
//           $('.form').unblock(); 
   });

   $('.form').find('button:submit').click(function(){
        $.blockUI({ overlayCSS: { backgroundColor: '#fff' } }); 
    });

    
});

</script>
