<?php 
$cUid = MyFormat::getCurrentUid();
$display_password   = $model->isNewRecord ? 'display_none' : '';
$canCreateAdd = true;
$classBlockFamily = '';
if($model->role_id != ROLE_FAMILY){
    $classBlockFamily = 'display_none';
}
$display_update   = '';
if(in_array($cUid, GasConst::getArrUidZoneManager())){
    $display_update = 'display_none';
}
$hideFieldFamily   = '';
if($model->role_id == ROLE_FAMILY){
    $hideFieldFamily = 'display_none';
}

?>
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/jquery.blockUI.js"></script>    
<div class="form ">
<?php $form=$this->beginWidget('CActiveForm', array(
    'id'=>'users-form',
    'enableAjaxValidation'=>false,
    'htmlOptions' => array('enctype' => 'multipart/form-data'),
)); ?>
    <?php echo MyFormat::BindNotifyMsg(); ?>
    <?php echo $form->errorSummary($model); ?>
    <?php echo $form->errorSummary($model->mProfile); ?>
    <p class="note">Dòng có dấu <span class="required">*</span> là bắt buộc.</p>
    
    <?php if(!isset($_GET['idRef']) && $model->role_id != ROLE_FAMILY): ?>
        <?php include '_form_employees_default.php';?>
    <?php else: ?>
        <?php include '_tab1.php';?>
    <?php endif; ?>
<?php $this->endWidget(); ?>

</div><!-- form -->

<?php CronUpdate::devPrintDbData($model); ?>

<style>
div.form input[type=text],
div.form input[type="password"],
div.form textarea,
div.form select { font-size: 15px; }
.autocomplete_customer_info {margin-left: 0; margin-top: 5px;}
.grp-cot { font-size: 12px; font-family: Arial,Helvetica,sans-serif;}
</style>

<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/autoNumeric-master/autoNumeric.js"></script>
<script>
$(document).ready(function() {
    jQuery('a.gallery').colorbox({ opacity:0.5 , rel:'group1' });
    $('.employees').addClass('index').find('span').text('Quản lý');
    $('.employees_create').addClass('create').find('span').text('Tạo mới');
    $('.employees_view').addClass('view').find('span').text('Xem');
    $(".createFamily").colorbox({iframe:true,innerHeight:'1200', innerWidth: '1100',close: "<span title='close'>close</span>"});        
    fnInitInputCurrency();
    setChangeRole();
//    $('.ChangeRole').trigger('change');
    $('.UserStatus').trigger('change');
    $('.submit-blockui').live('click',function(){
       $('.form').block({
           message: '', 
           overlayCSS:  { backgroundColor: '#fff' }
      }); 
//           $('.form').unblock(); 
   });

   $('.form').find('button:submit').click(function(){
        $.blockUI({ overlayCSS: { backgroundColor: '#fff' } }); 
    });
});

/** @Author: ANH DUNG Aug 21, 2017
*  @Todo: bind event change role
*/
function setChangeRole(){
    $('.ChangeRole').change(function(){
        $(this).closest('.form').find('button:submit').trigger('click');
        if($(this).val() == <?php echo ROLE_FAMILY;?>){
            $('.BlockFamily').show();
        }else{
            $('.BlockFamily').hide();
        }
    });
    $('.UserStatus').change(function(){
        if($(this).val() == <?php echo STATUS_INACTIVE;?>){
            $('.UserStatusLeave').show();
        }else{
            $('.UserStatusLeave').hide();
        }
    });
    
    $('.SalaryMethodRd').click(function(){
        $('.SalaryAccount').hide();
        $('.SalaryAccountBox' + $(this).val()).show();
    });
    
}


</script>
