<h3 class='title-info'>Thông tin cá nhân</h3>
    
<div class="grp-cot clearfix">
    <div class="sk-col-md-12">
        <div class="sk-col-md-3">
            <div class="sk-col-md-3 lable-text">
                <?php echo $form->labelEx($model->mProfile,'id_number'); ?>
            </div>
            <div class="sk-col-md-9 input-content ">
                <?php echo $form->textField($model->mProfile,'id_number',array('class'=>'full-control', 'maxlength'=>250)); ?>
                <?php echo $form->error($model->mProfile,'id_number'); ?>
            </div>
        </div>
        <div class="sk-col-md-3">
            <div class="sk-col-md-3 lable-text">
                <?php echo $form->labelEx($model->mProfile,'id_created_date'); ?>
            </div>
            <div class="sk-col-md-9 input-content ">
                <?php 
                    $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                        'model'=>$model->mProfile,        
                        'attribute'=>'id_created_date',
                        'options'=>array(
                            'showAnim'=>'fold',
                            'dateFormat'=> ActiveRecord::getDateFormatJquery(),
                            'changeMonth' => true,
                            'changeYear' => true,
                            'showOn' => 'button',
                            'buttonImage'=> Yii::app()->theme->baseUrl.'/admin/images/icon_calendar_r.gif',
                            'buttonImageOnly'=> true,                                
                        ),        
                        'htmlOptions'=>array(
                            'class'=>'w-150',
                        ),
                    ));
                ?>     		
                <?php echo $form->error($model->mProfile,'id_created_date'); ?>
            </div>
        </div>
        <div class="sk-col-md-3">
            <div class="sk-col-md-3 lable-text">
                <?php echo $form->labelEx($model->mProfile,'id_province'); ?>
            </div>
            <div class="sk-col-md-9 input-content ">
                <?php echo $form->dropDownList($model->mProfile,'id_province', GasProvince::getArrAllFix(), array('class'=>'full-control', 'empty'=>'Select')); ?>
                <?php echo $form->error($model->mProfile,'id_province'); ?>
            </div>
        </div>
        <div class="sk-col-md-3">
            <div class="sk-col-md-3 lable-text">
                <?php echo $form->labelEx($model->mProfile,'status_marital'); ?>
            </div>
            <div class="sk-col-md-9 input-content ">
                <?php echo $form->dropDownList($model->mProfile,'status_marital', $model->mProfile->getArrStatusMarital(), array('class'=>'full-control', 'empty'=>'Select')); ?>
                <?php echo $form->error($model->mProfile,'status_marital'); ?>
            </div>
        </div>
    </div>
</div>

<div class="grp-cot clearfix">
    <div class="sk-col-md-12">
        <div class="sk-col-md-3">
            <div class="sk-col-md-3 lable-text">
                <?php echo $form->labelEx($model->mProfile,'country_id'); ?>
            </div>
            <div class="sk-col-md-9 input-content ">
                <?php echo $form->dropDownList($model->mProfile,'country_id', $model->mProfile->getArrCountry(), array('class'=>'full-control', 'empty'=>'Select')); ?>
                <?php echo $form->error($model->mProfile,'country_id'); ?>
            </div>
        </div>
        <div class="sk-col-md-3">
            <div class="sk-col-md-3 lable-text">
                <?php echo $form->labelEx($model->mProfile,'type_nation'); ?>
            </div>
            <div class="sk-col-md-9 input-content ">
                <?php echo $form->dropDownList($model->mProfile,'type_nation', $model->mProfile->getArrNation(), array('class'=>'full-control', 'empty'=>'Select')); ?>
                <?php echo $form->error($model->mProfile,'type_nation'); ?>
            </div>
        </div>
        <div class="sk-col-md-3">
            <div class="sk-col-md-3 lable-text">
                <?php echo $form->labelEx($model->mProfile,'type_religion'); ?>
            </div>
            <div class="sk-col-md-9 input-content ">
                <?php echo $form->dropDownList($model->mProfile,'type_religion', $model->mProfile->getArrReligion(), array('class'=>'full-control', 'empty'=>'Select')); ?>
                <?php echo $form->error($model->mProfile,'type_religion'); ?>
            </div>
        </div>
        <div class="sk-col-md-3">
            <div class="sk-col-md-3 lable-text">
                <?php echo $form->labelEx($model->mProfile,'type_education'); ?>
            </div>
            <div class="sk-col-md-9 input-content ">
                <?php echo $form->dropDownList($model->mProfile,'type_education', $model->mProfile->getArrEducation(), array('class'=>'full-control', 'empty'=>'Select')); ?>
                <?php echo $form->error($model->mProfile,'type_education'); ?>
            </div>
        </div>
        
    </div>
</div>

<div class="grp-cot clearfix">
    <div class="sk-col-md-12">
        <div class="sk-col-md-3">
            <div class="sk-col-md-3 lable-text">
                <?php echo $form->labelEx($model->mProfile,'address_born'); ?>
            </div>
            <div class="sk-col-md-9 input-content ">
                <?php echo $form->textArea($model->mProfile,'address_born', ['class'=>'full-control', 'rows'=>3, 'placeholder'=>'vd: Tp.HCM']); ?>
                <?php echo $form->error($model->mProfile,'address_born'); ?>
            </div>
        </div>
        <div class="sk-col-md-3">
            <div class="sk-col-md-3 lable-text">
                <?php echo $form->labelEx($model,'last_purchase', ['label'=>'Ngày Sinh']); ?>
            </div>
            <div class="sk-col-md-9 input-content ">
                <?php 
                    $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                        'model'=>$model,        
                        'attribute'=>'last_purchase',
                        'options'=>array(
                            'showAnim'=>'fold',
                            'dateFormat'=> ActiveRecord::getDateFormatJquery(),
            //                            'minDate'=> '0',
                            'maxDate'=> '0',
                            'changeMonth' => true,
                            'changeYear' => true,
                            'showOn' => 'button',
                            'buttonImage'=> Yii::app()->theme->baseUrl.'/admin/images/icon_calendar_r.gif',
                            'buttonImageOnly'=> true,                                
                        ),        
                        'htmlOptions'=>array(
                            'class'=>'w-150',
                        ),
                    ));
                ?>
                <?php echo $form->error($model,'last_purchase'); ?>
            </div>
        </div>
    </div>
</div>

<div class="grp-cot clearfix">
    <div class="sk-col-md-12">
        <div class="sk-col-md-3">
            <div class="sk-col-md-3 lable-text">
                <?php echo $form->labelEx($model->mProfile,'address_home'); ?>
            </div>
            <div class="sk-col-md-9 input-content ">
                <?php echo $form->textArea($model->mProfile,'address_home', ['class'=>'full-control', 'rows'=>3, 'placeholder'=>'vd: 86, Nguyễn Cửu Vân, P.17, Q.Bình Thạnh, TP.HCM']); ?>
                <?php echo $form->error($model->mProfile,'address_home'); ?>
            </div>
        </div>
        <div class="sk-col-md-3">
            <div class="sk-col-md-3 lable-text">
                <?php echo $form->labelEx($model->mProfile,'address_home_province'); ?>
            </div>
            <div class="sk-col-md-9 input-content ">
                <?php echo $form->dropDownList($model->mProfile,'address_home_province', GasProvince::getArrAllFix(), array('class'=>'full-control', 'empty'=>'Select')); ?>
                <?php echo $form->error($model->mProfile,'address_home_province'); ?>
            </div>
        </div>
    </div>
</div>

<div class="grp-cot clearfix">
    <div class="sk-col-md-12">
        <div class="sk-col-md-3">
            <div class="sk-col-md-3 lable-text">
                <?php echo $form->labelEx($model->mProfile,'address_live'); ?>
            </div>
            <div class="sk-col-md-9 input-content ">
                <?php echo $form->textArea($model->mProfile,'address_live', ['class'=>'full-control', 'rows'=>3]); ?>
                <?php echo $form->error($model->mProfile,'address_live'); ?>
            </div>
        </div>
        <div class="sk-col-md-3">
            <div class="sk-col-md-3 lable-text">
                <?php echo $form->labelEx($model->mProfile,'address_live_province'); ?>
            </div>
            <div class="sk-col-md-9 input-content ">
                <?php echo $form->dropDownList($model->mProfile,'address_live_province', GasProvince::getArrAllFix(), array('class'=>'full-control', 'empty'=>'Select')); ?>
                <?php echo $form->error($model->mProfile,'address_live_province'); ?>
            </div>
        </div>
    </div>
</div>

<div class="grp-cot clearfix">
    <div class="sk-col-md-12">
        <div class="sk-col-md-1 lable-text">
            <?php echo $form->labelEx($model->mProfile,'list_email'); ?>
        </div>
        <div class="sk-col-md-11 input-content ">
            <label class="color_red">Mỗi email cách nhau bởi dấu ; ví dụ: email1@gmail.com;email2@gmail.com;email3@gmail.com</label>
            <?php echo $form->textField($model->mProfile,'list_email', ['class'=>'full-control', 'rows'=>3]); ?>
            <?php echo $form->error($model->mProfile,'list_email'); ?>
        </div>
    </div>
</div>
<div class="grp-cot clearfix">
    <div class="sk-col-md-12">
        <div class="sk-col-md-1 lable-text">
            <?php echo $form->labelEx($model->mProfile,'list_phone'); ?>
        </div>
        <div class="sk-col-md-11 input-content ">
            <label class="color_red">Mỗi số đt cách nhau bởi dấu - ví dụ: 0988180386-01684331552</label>
            <?php echo $form->textField($model->mProfile,'list_phone', ['class'=>'full-control', 'rows'=>3]); ?>
            <?php echo $form->error($model->mProfile,'list_phone'); ?>
        </div>
    </div>
</div>
<div class="grp-cot clearfix">
    <div class="sk-col-md-12">
        <div class="sk-col-md-1 lable-text">
            <?php echo $form->labelEx($model->mProfile,'list_phone_family'); ?>
        </div>
        <div class="sk-col-md-11 input-content ">
            <label class="color_red">Mỗi số đt cách nhau bởi dấu - ví dụ: 0988180386-01684331552</label>
            <?php echo $form->textField($model->mProfile,'list_phone_family', ['class'=>'full-control', 'rows'=>3]); ?>
            <?php echo $form->error($model->mProfile,'list_phone_family'); ?>
        </div>
    </div>
</div>
<div class="grp-cot clearfix">
    <div class="sk-col-md-12">
        <div class="sk-col-md-1 lable-text">
            <?php echo $form->labelEx($model->mProfile,'note'); ?>
        </div>
        <div class="sk-col-md-11 input-content ">
            <?php echo $form->textArea($model->mProfile,'note', ['class'=>'full-control', 'rows'=>5]); ?>
            <?php echo $form->error($model->mProfile,'note'); ?>
        </div>
    </div>
</div>
<div class="grp-cot clearfix">
    <div class="sk-col-md-12">
        <?php include "_form_avatar.php"; ?>
    </div>
</div>