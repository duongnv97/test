<div id="tabs" class="grid-view">
    <ul>
        <li>
            <a class="tab_1" href="#tabs-1">Thông tin nhân viên</a>
        </li>
        <li>
            <a class="tab_2" href="#tabs-2">Thông tin người thân</a>
        </li>
    </ul>
    <?php include '_tab1.php';?>
    <?php if(!$model->isNewRecord): ?>
        <?php include '_tab2.php';?>        
    <?php endif; ?>
</div>

<script>
$(document).ready(function() {
    $( "#tabs" ).tabs();
//    $( "#tabs" ).tabs({ active: 1 });
});
</script>