<?php
$this->breadcrumbs=array(
    'Quản lý nhân sự'=>array('employees'),
    $model->code_account." - ".$model->first_name=>array('employees_view','id'=>$model->id),
    'Cập nhật',
);
$aIndex = array('employees');
if($model->role_id == ROLE_CANDIDATE){
    $aIndex = array('employees', 'tuyendung'=>1);
}

$menus = array(	
    array('label'=> 'Quản lý', 'url'=> $aIndex),
    array('label'=> 'Xem', 'url'=>array('employees_view', 'id'=>$model->id)),
    array('label'=> 'Tạo mới ', 'url'=>array('employees_create')),	
);
$this->menu= ControllerActionsName::createMenusRoles($menus, $actions);

?>
<h1><?php echo "{$this->pageTitle}: $model->code_account - ".$model->first_name; ?></h1>
<?php // echo $this->renderPartial('_form_employees', array('model'=>$model)); ?>
<?php // echo $this->renderPartial('_form_employees_bk1', array('model'=>$model)); ?>
<?php include '_form_employees.php'; ?>