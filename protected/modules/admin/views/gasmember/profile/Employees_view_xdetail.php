<?php 
    $mUsersProfile = $model->rUsersProfile;
    if(is_null($mUsersProfile)){
        $mUsersProfile = new UsersProfile();
    }
//    $address_born, $address_home, $address_live, $list_email, $list_phone, $list_phone_family, $note;
    $mUsersProfile->mapJsonFieldOneDecode('JSON_FIELD', 'json', 'baseArrayJsonDecode');
    $mUsersRef = $model->rUsersRef;
    if(empty($mUsersRef)){
        $mUsersRef = new UsersRef();
    }
?>
<h3 class='title-info'>Thông tin chung</h3>
<?php $this->widget('application.components.widgets.XDetailView', array(
    'data' => $model,
    'attributes' => array(
        'group11'=>array(
            'ItemColumns' => 3,
            'attributes' => array(
                    'first_name',
                    array(
                        'name' => $mUsersProfile->getAttributeLabel('profile_status'),
                        'value' => $mUsersProfile->getProfileStatus(), 
                    ),
                    array(
                        'name' => 'role_id',
                        'type'=>'raw',
                        'value'=>"<b>".$model->getFamily(["br"=>"<br>"])."</b>".$model->getRoleName(),
                    ),
                    array(
                        'label' => 'Mã nhân viên',
                        'name' => 'code_account',                       
                    ),
                    array(
                        'label' => 'Nơi công tác',
                        'value' => $model->getParentCacheSession(),
                    ),
                    array(
                        'label'    => 'Giới tính',
                        'value'     => $model->getGender(),
                    ),
                    array(
                        'label'    => 'Ký quỹ',
                        'value'     => $model->getCredit(),
                    ),
                    array(
                        'label'    => 'Tài khoản web',
                        'value'     => $model->username,
                    ),
                    array(
                        'label'    => 'Email',
                        'value'     => $model->email,
                    ),
                    array(
                        'label'    => 'Số di động chính',
                        'value'     => $model->phone,
                    ),
                    array(
                        'label'    => 'Trạng Thái',
                        'value'     => $model->getProfileStatus(),
                    ),
                    array(
                        'label'    => 'Ngày nghỉ việc',
                        'value'     => $mUsersProfile->getLeaveDate(),
                    ),
                    array(
                        'label'    => 'Địa chỉ hiện tại',
                        'value'     => $model->address,
                    ),
                    
            ),
        ),
        'group01'=>array(
            'ItemColumns' => 4,
            'attributes' => array(
                array(
                    'label' => 'Trả lương',
                    'value' => $model->getPaySalary(),
                ),
                array(
                    'name' => $mUsersProfile->getAttributeLabel('salary_method'),
                    'value' => $mUsersProfile->getSalaryMethod(),
                ),
                array(
                    'name' => $mUsersProfile->getAttributeLabel('salary_method_phone'),
                    'value' => $mUsersProfile->getSalaryMethodPhone(),
                ),
                array(
                    'name' => $mUsersProfile->getAttributeLabel('salary_method_account'),
                    'value' => $mUsersProfile->getSalaryMethodAccount(),
                ),
            ),
        ),
    ),
)); ?>

<?php if($mUsersProfile->canViewSalary()): ?>
    <?php include 'Employees_view_salary.php'; ?>
<?php endif; ?>

<?php include 'Employees_view_private.php'; ?>
<?php // @Code: NAM005 ?>
<?php include 'Employees_view_pictures.php'; ?>
