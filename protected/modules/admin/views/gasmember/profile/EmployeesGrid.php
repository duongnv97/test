<?php
$this->breadcrumbs=array(
    $this->pageTitle,
);

$menus=array(
    array('label'=> 'Tạo Mới nhân sự', 'url'=>array('employees_create')),
);

if($this->canExportExcelForEmployees()):
    $menus[] = ['label'=> 'Xuất Excel Danh Sách Hiện Tại',
                'url'=>array('employees', 'ExportExcel'=>1), 
                'htmlOptions'=>array('class'=>'export_excel ','label'=>'Xuất Excel'),
    ];
endif;
//$actions[] = 'testNotify';
$this->menu= ControllerActionsName::createMenusRoles($menus, $actions);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('users-grid', {
                url : $(this).attr('action'),
		data: $(this).serialize()
	});
	return false;
});
");

Yii::app()->clientScript->registerScript('ajaxupdate', "
$('#users-grid a.ajaxupdate').live('click', function() {
    $.fn.yiiGridView.update('users-grid', {
        type: 'POST',
        url: $(this).attr('href'),
        success: function() {
            $.fn.yiiGridView.update('users-grid');
        }
    });
    return false;
});
$('#users-grid a.checkOk').live('click', function() {
    if(confirm('Hồ sơ đã được cập nhật đầy đủ ?')){
        $.fn.yiiGridView.update('users-grid', {
            type: 'POST',
            url: $(this).attr('href'),
            success: function() {
                $.fn.yiiGridView.update('users-grid');
            }
        });
    }
    return false;
});
");

?>
<?php // include 'index_button.php'; ?>

<?php echo CHtml::link(Yii::t('translation','Advanced Search'),'#',array('class'=>'search-button')); ?>
<div class="search-form" >
    <?php include '_search_em.php'; ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'users-grid',
	'dataProvider'=>$model->searchGasEmployees(),
        'afterAjaxUpdate'=>'function(id, data){ fnUpdateColorbox();}',
        'template'=>'{pager}{summary}{items}{pager}{summary}',     
        'pager' => array(
            'maxButtonCount'=>  CmsFormatter::$PAGE_MAX_BUTTON,
        ),
	'enableSorting' => false,
	'columns'=>array(
        array(
            'header' => 'S/N',
            'type' => 'raw',
            'value' => '$this->grid->dataProvider->pagination->currentPage * $this->grid->dataProvider->pagination->pageSize + ($row+1)',
            'headerHtmlOptions' => array('width' => '30px','style' => 'text-align:center;'),
            'htmlOptions' => array('style' => 'text-align:center;')
        ),
            array(
                'header' => 'Mã nhân viên',
                'name' => 'code_account',
            ),
            array(
                'header' => 'Tên Nhân Viên',
                'value' => '$data->first_name."<br><b>". $data->username."</b>"',
                'type'=>'raw',
            ),
            array(
                'header' => 'Bộ phận',
                'value' => '$data->getFunctionProfile("getPositionWork")',
                'type'=>'raw',
            ),
            array(
                'name' => 'role_id',
                'type'=>'raw',
                'htmlOptions' => array('style' => 'text-align:center;'),
                'value'=>'"<b>".$data->getFamily(["br"=>"<br>"])."</b>".$data->getRoleName()',
                'visible'=> !isset($_GET['tuyendung'])
            ),
            array(
                'header' => 'Vị trí ứng tuyển',
                'value' => '$data->getFunctionProfile("getPositionApply")',
                'visible'=> isset($_GET['tuyendung'])
            ),
            array(
                'header' => 'Công ty',
                'value' => '$data->getCompany()',
            ),
            array(
                'header' => 'Nơi công tác',
                'value' => '$data->getParentCacheSession()',
            ),
            'address',
            array(
                'header' => 'Vào làm',
                'value'=>'$data->getDateBeginJob()',
            ),
            array(
                'header'    => 'Ký quỹ',
                'value'     => '$data->getCredit()',
                'htmlOptions' => array('style' => 'text-align:center;')
            ),
            array(
                'header' => 'Chi tiết',
                'type' => 'raw',
                'value'=>'$data->getProfileDetail()',
            ),
            array(
                'header'    => 'Người tạo',
                'value'     => '$data->getCreatedBy()',
                'htmlOptions' => array('style' => 'text-align:center;')
            ),
            array(
                'name'=>'status',
                'value'=>'$data->getProfileStatus()',
                'htmlOptions' => array('style' => 'text-align:center;')
            ),
//            array(
//                'name'=>'status',
//                'type'=>'status',
//                'value'=>'array("status"=>$data->status,"id"=>$data->id)',
//                'htmlOptions' => array('style' => 'text-align:center;'),
//                'visible'=>Yii::app()->user->id == GasConst::UID_CHAU_LNM,
//            ),
            array(
                'header' => 'Ngày Tạo',
                'type'=>'raw',
                'value' => '$data->getCreatedDate()."<br>".$data->getFunctionProfile("getStatusLeave")."<br>".Forecast::model()->getLinkTestNotify($data->id)',
                'htmlOptions' => array('style' => 'width:50px;')
            ),
    array(
        'header' => 'Action',
        'class'=>'CButtonColumn',
        'template'=> ControllerActionsName::createIndexButtonRoles($actions,array('Employees_view','Employees_update','delete','CheckFullProfile','updateDetail')),
        'buttons'=>array(
            'Employees_update'=>array(
                'label'=>'Cập Nhật Thông Tin Nhân Viên',
                //'imageUrl'=>Yii::app()->theme->baseUrl . '/admin/images/edit.png',
                'imageUrl'=>Yii::app()->theme->baseUrl . '/admin/images/icon/update_icon.png',
                'options'=>array('class'=>'employees_update'),
                'url'=>'Yii::app()->createAbsoluteUrl("admin/gasmember/employees_update",
                    array("id"=>$data->id) )',
            ),
            'Employees_view'=>array(
                'label'=>'Xem Thông Tin Nhân Viên',
                'imageUrl'=>Yii::app()->theme->baseUrl . '/admin/images/icon/view_icon.png',
                'options'=>array('class'=>'employees_view'),
                'url'=>'Yii::app()->createAbsoluteUrl("admin/gasmember/employees_view",
                    array("id"=>$data->id) )',
            ),
            'CheckFullProfile'=>array(
                'label'=>'Đầy đủ hồ sơ',
                'imageUrl'=>Yii::app()->theme->baseUrl . '/images/Checked-icon.png',
                'options'=>array('class'=>'checkOk'),
                'url'=>'Yii::app()->createAbsoluteUrl("admin/gasmember/checkFullProfile", array("id"=>$data->id) )',
                'visible'=> '!empty($data->rUsersProfile) ? $data->rUsersProfile->canUpdateFull() : 0',
            ),
            'updateDetail' => array(
                'label'=>'Cập nhật xe, tài xế...',     //Text label of the button.
                'url'=>'["updateDetail", "id"=>$data->id]',  
                'imageUrl'=>Yii::app()->theme->baseUrl . '/admin/images/icon_new_task.gif',
                'options'=>array('class' => 'update_agent'), //HTML options for the button tag..
                'visible' => 'GasCheck::isAllowAccess("gasmember", "updateDetail")'
            ),
//            'testNotify' => array( // DuongNV test notify
//                'label'=>'Test send notify',     //Text label of the button.
//                'url'=>'Yii::app()->createAbsoluteUrl("admin/ajax/testNotify",array("id"=>$data->id) )',
//                'imageUrl'=>Yii::app()->theme->baseUrl . '/admin/images/Upload.png',
//                'options'=>array('class' => 'update_agent'), //HTML options for the button tag..
//                'visible' => 'MyFormat::getCurrentRoleId() == ROLE_ADMIN'
//            ),
        ),	                    
    ),
	),
)); ?>

<script>
$(document).ready(function(){
    $('.portlet-content .employees_create').find('a').attr('class','create_new');
    $('.employees_create').removeClass('employees_create').addClass('create').find('span').text('Tạo Mới');
//    $(".create_new").colorbox({iframe:true,innerHeight:'600', innerWidth: '900',close: "<span title='close'>close</span>"});
    fnUpdateColorbox();
    fnBindChangeProvince('<?php echo BLOCK_UI_COLOR;?>', "<?php echo Yii::app()->createAbsoluteUrl('admin/ajax/get_slt_district');?>");
    fnUpdateAgent();
});
	
	
function fnUpdateColorbox(){    
    fixTargetBlank();
//    $(".employees_update").colorbox({iframe:true,innerHeight:'600', innerWidth: '900',close: "<span title='close'>close</span>"});
//    $(".employees_view").colorbox({iframe:true,innerHeight:'500', innerWidth: '900',close: "<span title='close'>close</span>"});
}	
function fnUpdateAgent(){    
    $(document).on('click', '.update_agent', function(event){
        event.preventDefault();
        $.colorbox({
            href: $(this).attr("href"),
            iframe: true,
            innerHeight: '500',
            innerWidth: '1050',
            close: "<span title='close'>close</span>"
        });
    });
}
</script>
