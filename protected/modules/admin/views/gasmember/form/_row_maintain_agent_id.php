<?php
$aMantainAgent = array();
if($model->maintain_agent_id && is_array($model->maintain_agent_id)){
    $aMantainAgent = Users::getArrayModelByArrayId($model->maintain_agent_id);
}
$model->maintain_agent_id = null;
$index=1;
?>
<div class="row ">
    <?php echo $form->label($model,'maintain_agent_id'); ?>
    <?php echo $form->hiddenField($model,'maintain_agent_id', array('class'=>'')); ?>
    <?php
        // 1. limit search kh của sale
        $url = Yii::app()->createAbsoluteUrl('admin/ajax/search_by_role', array('role'=> ROLE_AGENT));
        // widget auto complete search user customer and supplier
        $aData = array(
            'model'=>$model,
            'field_customer_id'=>'maintain_agent_id',
            'url'=> $url,
            'name_relation_user'=>'CustomerMaintain',
            'ClassAdd' => 'w-300',
            'field_autocomplete_name' => 'autocomplete_name_2',
            'placeholder'=>'Nhập mã hoặc tên đại lý',
            'fnSelectCustomerV2' => "fnDeloyBySelectAgent",
            'doSomethingOnClose' => "doSomethingOnCloseMaintain",
        );
        $this->widget('ext.SpaAutocompleteCustomer.SpaAutocompleteCustomer',
            array('data'=>$aData));
        ?>
    <?php echo $form->error($model,'maintain_agent_id'); ?>
</div>
<div class="row">
    <label>&nbsp</label>
    <table class="materials_table hm_table tb_deloy_by w-500">
        <thead>
            <tr>
                <th class="item_c w-20">#</th>
                <th class="item_code item_c w-300">Đại lý</th>
                <th class="item_unit last item_c">Xóa</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach($aMantainAgent as $key => $mUser): ?>
            <?php // if(is_null($mUser)) continue; ?>
            <tr>
                <input name="Users[maintain_agent_id][]"  value="<?php echo $key; ?>" type="hidden">
                <td class="item_c order_no"><?php echo $index++; ?></td>
                <td class="uid_maintain_<?php echo $mUser->id?>"><?php echo $mUser->getNameWithRole(). "  ---  ".$mUser->id;?></td>
                <td class="item_c last"><span class="remove_icon_only"></span></td>
            </tr>
            <?php endforeach;?>
        </tbody>
        
    </table>
</div>
<br>
<script>
//    $(document).ready(function(){ // file kia có rồi
//        fnBindRemoveIcon();
//    });
    /**
    * @Author: ANH DUNG Dec 28, 2016
    * @Todo: function này dc gọi từ ext của autocomplete, action close auto
    */
    function doSomethingOnCloseMaintain(ui, idField, idFieldCustomer){
        var row = $(idField).closest('.row');
        row.find('.remove_row_item').trigger('click');
    }
</script>