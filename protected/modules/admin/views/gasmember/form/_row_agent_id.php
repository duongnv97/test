<?php
$aAgent = array();
if($model->agent_id_search && is_array($model->agent_id_search)){
    $aAgent = Users::getArrayModelByArrayId($model->agent_id_search);
}
$model->agent_id_search = null;
$index=1;
?>
<div class="row ">
    <?php echo $form->label($model,'agent_id_search', ['label'=>'Đại lý giám sát thuộc NV']); ?>
    <?php echo $form->hiddenField($model,'agent_id_search', array('class'=>'')); ?>
    <?php
        // 1. limit search kh của sale
        $url = Yii::app()->createAbsoluteUrl('admin/ajax/search_by_role', array('role'=> ROLE_AGENT));
        // widget auto complete search user customer and supplier
        $aData = array(
            'model'=>$model,
            'field_customer_id'=>'agent_id_search',
            'url'=> $url,
            'name_relation_user'=>'rAgent',
            'ClassAdd' => 'w-300',
            'field_autocomplete_name' => 'autocomplete_name_1',
            'placeholder'=>'Nhập mã hoặc tên đại lý',
            'fnSelectCustomerV2' => "fnDeloyBySelectAgent",
            'doSomethingOnClose' => "doSomethingOnClose",
        );
        $this->widget('ext.SpaAutocompleteCustomer.SpaAutocompleteCustomer',
            array('data'=>$aData));
        ?>
    <?php echo $form->error($model,'agent_id_search'); ?>
</div>
<div class="row">
    <label>&nbsp</label>
    <table class="materials_table hm_table tb_deloy_by_1 w-500">
        <thead>
            <tr>
                <th class="item_c w-20">#</th>
                <th class="item_code item_c w-300">Đại lý</th>
                <th class="item_unit last item_c">Xóa</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach($aAgent as $key => $mUser): ?>
            <?php // if(is_null($mUser)) continue; ?>
            <tr>
                <input name="Users[agent_id_search][]"  value="<?php echo $key; ?>" type="hidden">
                <td class="item_c order_no"><?php echo $index++; ?></td>
                <td class="uid_<?php echo $mUser->id?>"><?php echo $mUser->getNameWithRole(). "  ---  ".$mUser->id;?></td>
                <td class="item_c last"><span class="remove_icon_only"></span></td>
            </tr>
            <?php endforeach;?>
        </tbody>
    </table>
</div>
<br>
<script>
    $(document).ready(function(){
        fnBindRemoveIcon();
    });
    /**
    * @Author: ANH DUNG Jun 23, 2015
    * @Todo: function này dc gọi từ ext của autocomplete
    * @Param: $model
    */
    function fnDeloyBySelectAgent(ui, idField, idFieldCustomer){
        console.log("ok 2232");
        console.log(idField);
        if(idField == '#Users_autocomplete_name_1'){
            var row = $(idField).closest('.row');
            var ClassCheck = "uid_"+ui.item.id;
            var td_name = '<td class="'+ClassCheck+'">'+ui.item.name_role+'</td>';
            var td_remove = '<td class="item_c last"><span class="remove_icon_only"></span></td>';
            var input = '<input name="Users[agent_id_search][]"  value="'+ui.item.id+'" type="hidden">';
            var tr = '<tr><td class="item_c order_no"></td>'+td_name+td_remove+input+'</tr>';
            if($('.tb_deloy_by_1 tbody').find('.'+ClassCheck).size() < 1 ) {
                $('.tb_deloy_by_1 tbody').append(tr);
            }
        }else{
            var row = $(idField).closest('.row');
            var ClassCheck = "uid_maintain_"+ui.item.id;
            var td_name = '<td class="'+ClassCheck+'">'+ui.item.name_role+'</td>';
            var td_remove = '<td class="item_c last"><span class="remove_icon_only"></span></td>';
            var input = '<input name="Users[maintain_agent_id][]"  value="'+ui.item.id+'" type="hidden">';
            var tr = '<tr><td class="item_c order_no"></td>'+td_name+td_remove+input+'</tr>';
            if($('.tb_deloy_by tbody').find('.'+ClassCheck).size() < 1 ) {
                $('.tb_deloy_by tbody').append(tr);
            }
        }
        fnRefreshOrderNumber();
    }
    /**
    * @Author: ANH DUNG Dec 28, 2016
    * @Todo: function này dc gọi từ ext của autocomplete, action close auto
    */
    function doSomethingOnClose(ui, idField, idFieldCustomer){
        var row = $(idField).closest('.row');
        row.find('.remove_row_item').trigger('click');
    }
</script>