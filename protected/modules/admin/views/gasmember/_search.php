<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

    <div class="row">
    <?php echo $form->label($model,'parent_id', array('label'=>'Thuộc Đại Lý')); ?>
    <?php echo $form->hiddenField($model,'parent_id', array('class'=>'')); ?>
    <?php
            // 1. limit search kh của sale
            $url = Yii::app()->createAbsoluteUrl('admin/ajax/search_by_role', array('role'=> ROLE_AGENT));
            // widget auto complete search user customer and supplier
            $aData = array(
                'model'=>$model,
                'field_customer_id'=>'parent_id',
                'url'=> $url,
                'name_relation_user'=>'rParent',
                'ClassAdd' => 'w-400',
                'field_autocomplete_name' => 'autocomplete_name',
                'placeholder'=>'Nhập mã hoặc tên đại lý',
            );
            $this->widget('ext.SpaAutocompleteCustomer.SpaAutocompleteCustomer',
                array('data'=>$aData));
            ?>
    </div>

    <div class="row more_col">
        <div class="col1">
            <?php echo $form->label($model,'email',array()); ?>
            <?php echo $form->textField($model,'email',array('class'=>'w-200')); ?>
        </div>
        <div class="col2">
            <?php echo $form->label($model,'username',array()); ?>
            <?php echo $form->textField($model,'username',array('class'=>'w-200')); ?>
        </div>
    </div>	

    <div class="row more_col">
        <div class="col1">
            <?php echo $form->label($model,'role_id'); ?>
            <?php echo $form->dropDownList($model,'role_id',Roles::loadItems(),array('class'=>'w-200','empty'=>'Select')); ?>
        </div>
        <div class="col2">
            <?php echo $form->label($model,'code_bussiness',array()); ?>
            <?php echo $form->textField($model,'code_bussiness',array('class'=>'w-200')); ?>
        </div>
        <div class="col3">
            <?php echo $form->label($model,'phone',array()); ?>
            <?php echo $form->textField($model,'phone',array('class'=>'w-200')); ?>
        </div>
    </div>

    <div class="row more_col">
        <div class="col1">
            <?php echo $form->label($model,'first_name',array('label'=>'Tên NV',)); ?>
            <?php echo $form->textField($model,'first_name',array('class'=>'w-200','placeholder'=>'nhập tiếng việt không dấu')); ?>
        </div>
        <div class="col2">
            <?php echo $form->label($model,'status',array()); ?>
            <?php echo $form->dropDownList($model,'status', ActiveRecord::getUserStatus(), array('class'=>'w-200','empty'=>'Select')); ?>
        </div>
        <div class="col3">
            <?php echo $form->label($model,'code_account',array()); ?>
            <?php echo $form->textField($model,'code_account',array('class'=>'w-200')); ?>
        </div>
    </div>

    <div class="row buttons" style="padding-left: 159px;">
                    <?php $this->widget('bootstrap.widgets.TbButton', array(
        'buttonType'=>'submit',
        'label'=>Yii::t('translation','Search'),
        'type'=>'null', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
        'size'=>'small', // null, 'large', 'small' or 'mini'
        //'htmlOptions' => array('style' => 'margin-bottom: 10px; float: right;'),
    )); ?>	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->