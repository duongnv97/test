<?php
$this->breadcrumbs=array(
	'Member'=>array('index'),
	$model->code_account." - ".$model->first_name,
);

$menus = array(
	array('label'=>'Member', 'url'=>array('index')),
	array('label'=>'Create Member', 'url'=>array('create')),
	array('label'=>'Update Member', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete Member', 'url'=>array('delete'), 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
);
$this->menu= ControllerActionsName::createMenusRoles($menus, $actions);
$mUsersRef = $model->mUsersRef;
$mAppPromotion = new AppPromotion();
$mAppPromotion->owner_id = $model->id;
?>

<h1>View Member: <?php echo $model->code_account . ' - '. $model->first_name; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
        array(
            'label' => 'Thuộc Đại Lý',
            'name' => 'parent_id',                       
            'value' => $model->parent?$model->parent->first_name:'',
        ),
        array(
            'name' => 'code_account',                       
            //'htmlOptions' => array('style' => 'text-align:center;')
        ),
        array(
            'label' => 'Mã Member',
            'type' => 'raw',
            'name' => 'code_bussiness',
            'value' => $model->code_bussiness . ' - Code app Gas24h: <b>'. $mAppPromotion->getOnlyCodeByUser().'</b>',
            //'htmlOptions' => array('style' => 'text-align:center;')
        ),
        'email',
        array(
            'label' => 'Chức Vụ',
            'type'=>'RoleNameUser',
            'value' => $model->role_id,
            //'htmlOptions' => array('style' => 'text-align:center;')
        ),
        'username',
        array(
            'label' => 'Mật Khẩu',
            'name' => 'temp_password',
            'visible'=> Yii::app()->user->role_id == ROLE_ADMIN,
            //'htmlOptions' => array('style' => 'text-align:center;')
        ),            
        array(
            'label' => 'Tên Member',
            'name' => 'first_name',
            //'htmlOptions' => array('style' => 'text-align:center;')
        ),
        array(
            'label' => 'Hệ Số',
            'name' => 'last_name',
        ),
        array(
            'header' => 'Loại KH',
            'name' => 'is_maintain',
            'value'=> $model->getTypeCustomerText(),
        ),
        array(
            'name' => 'phone',
            //'htmlOptions' => array('style' => 'text-align:center;')
        ),
        array(
            'label' => 'Sale Tương Ứng Trong Hệ Thống',
            'name' => 'sale_id',
            'value' => MyFormat::BuildNameSaleSystem( $model->sale ),
        ),
        array(
            'label' => 'Loại Sale',
            'value'=>$model,
            'type' => 'TypeSaleText',
//            'visible'=>$model->role_id==ROLE_SALE,
            //'htmlOptions' => array('style' => 'text-align:center;')
        ),	
        'address',
        array(
            'name'=>'province_id',
            'value'=>$model->province?$model->province->name:'',
        ),		
        array(
            'name'=>'district_id',
            'value'=>$model->district?$model->district->name:'',
        ),
        array(
            'label'=>'Ghi chú',
            'type'=>'html',
            'value'=>$mUsersRef?$mUsersRef->getNote():'',
        ),

        'created_date:datetime',
        'last_logged_in:datetime',
            'address_vi',
            //'phone',
    ),
)); ?>

<?php $this->widget('TokenWidget', array('user_id' => $model->id)); ?>
File ảnh hiện tại 
<a class="gallery" href="<?php echo ImageProcessing::bindImageByModel($model->mUsersRef,'','',array('size'=>'size2'));?>">
    <img src="<?php echo ImageProcessing::bindImageByModel($model->mUsersRef,'','',array('size'=>'size1'));?>">
</a>

<script>
$(document).ready(function() {
    jQuery('a.gallery').colorbox({ opacity:0.5 , rel:'group1' });
});
</script>