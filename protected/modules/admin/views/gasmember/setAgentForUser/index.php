<?php 
    $aType = $model->getArrayType();
?>
<h1><?php echo $this->pageTitle;?></h1>
<?php echo MyFormat::BindNotifyMsg(); ?>
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/jquery.blockUI.js"></script>    
<div class="form ">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'gas-maintain-sell-form',
	'enableAjaxValidation'=>false,
)); ?>

<div class="row buttons" style="padding-left: 141px;">
<?php $this->widget('bootstrap.widgets.TbButton', array(
    'buttonType'=>'submit',
    'label'=>$model->isNewRecord ? Yii::t('translation', 'Save') : Yii::t('translation', 'Save'),
    'type'=>'null', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
    'size'=>'small', // null, 'large', 'small' or 'mini'
    //'htmlOptions' => array('style' => 'margin-bottom: 10px; float: right;'),
)); ?>
    <div style="display: inline-block;padding: 5px;"></div>
<?php $this->widget('bootstrap.widgets.TbButton', array(
    'buttonType'=>'submit',
    'label'=>'Xem',
    'type'=>'null', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
    'size'=>'small', // null, 'large', 'small' or 'mini'
    'htmlOptions' => array('name' => 'ViewAll'),
)); ?>
</div>
    
<div class="row <?php // echo $display_none;?>">
    <?php echo $form->labelEx($model,'type', array('label'=>'Loại setup')); ?>
    <?php echo $form->dropDownList($model,'type', $aType, array('class'=>'type w-400', 'empty'=>'Select')); ?>
    <?php echo $form->error($model,'type'); ?>
<p>KH mối không bấm nợ: Chọn NV Vu Thai Long</p>
</div>
    
 <div class="row RowDateApply <?php echo $model->getClassShowDateApply();?>">
    <?php echo $form->labelEx($model,'date_apply'); ?>
    <?php 
        $this->widget('zii.widgets.jui.CJuiDatePicker', array(
            'model'=>$model,        
            'attribute'=>'date_apply',
            'options'=>array(
                'showAnim'=>'fold',
                'dateFormat'=> MyFormat::$dateFormatSearch,
//                            'minDate'=> '0',
                'maxDate'=> '0',
                'changeMonth' => true,
                'changeYear' => true,
                'showOn' => 'button',
                'buttonImage'=> Yii::app()->theme->baseUrl.'/admin/images/icon_calendar_r.gif',
                'buttonImageOnly'=> true,                                
            ),        
            'htmlOptions'=>array(
                'class'=>'w-16 ',
                'style'=>'height:20px;',
                'readonly'=>'readonly',
            ),
        ));
    ?>
    <?php echo $form->error($model,'date_apply'); ?>
</div>
    
<?php include '_form_create_one_id.php'; ?>
    
<?php
    if($model->type){
        if($model->voThucTeNo){
            include '_form_view.php';
        }elseif($model->one_id){
            include '_form_create_update_agent.php';
        }
        
    }
?>


<div class="row buttons" style="padding-left: 141px;">
<?php $this->widget('bootstrap.widgets.TbButton', array(
    'buttonType'=>'submit',
    'label'=>$model->isNewRecord ? Yii::t('translation', 'Save') : Yii::t('translation', 'Save'),
    'type'=>'null', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
    'size'=>'small', // null, 'large', 'small' or 'mini'
    //'htmlOptions' => array('style' => 'margin-bottom: 10px; float: right;'),
)); ?>
    <div style="display: inline-block;padding: 5px;"></div>
<?php $this->widget('bootstrap.widgets.TbButton', array(
    'buttonType'=>'submit',
    'label'=>'Xem',
    'type'=>'null', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
    'size'=>'small', // null, 'large', 'small' or 'mini'
    'htmlOptions' => array('name' => 'ViewAll'),
)); ?>
</div>

<?php echo $model->getHtmlTypeDateApply();?>
<?php $this->endWidget(); ?>

</div><!-- form -->
<script>
    $(document).ready(function(){
        $('.form').find('button:submit').click(function(){
            $.blockUI({ overlayCSS: { backgroundColor: '<?php echo BLOCK_UI_COLOR;?>' } }); 
        });
//        var one_id = $('.one_id').val();
//
//        var requestUri = '<?php //echo Yii::app()->createAbsoluteUrl("admin/gasmember/setAgentForUser");?>';
//
//        var attributeAdd = 'one_id=';
//        attributeAdd += $('.one_id').val();
//        attributeAdd += '&type';
//        fnUpdateNextUrl('.type', requestUri, attributeAdd);

        fnBindRemoveIcon();
    });
    $('#GasOneMany_type').change(function(){
        exeUrl(true);
    });
    $('#GasOneMany_date_apply').change(function(){
        exeUrl();
    });

    
    function exeUrl($removeOneId = false){
        var type            = $('#GasOneMany_type').val();
        fnShowDateApply(type);
        var one_id          = $('#GasOneMany_one_id').val();
        var date_apply      = $('#GasOneMany_date_apply').val();
        if($removeOneId){
            one_id = '';
        }
        $.blockUI({ overlayCSS: { backgroundColor: '<?php echo BLOCK_UI_COLOR;?>' } }); 
        var sParams = 'type='+ type + '&one_id='+one_id + '&date_apply='+date_apply;
        var requestUri = '<?php echo Yii::app()->createAbsoluteUrl("admin/gasmember/setAgentForUser");?>?'+sParams;
        window.location= requestUri;
    };
    
    function fnDeloyBySelect(ui, idField, idFieldCustomer){
        if(idFieldCustomer == '#GasOneMany_one_id'){
            exeUrl();
        }else if(idFieldCustomer == '#GasOneMany_many_id'){
            var row = $(idField).closest('.row');
            var ClassCheck = "uid_"+ui.item.id;
            var td_name = '<td class="'+ClassCheck+'">'+ui.item.name_customer+' --- '+ui.item.id+'</td>';
            var td_remove = '<td class="item_c last"><span class="remove_icon_only"></span></td>';
            var input = '<input name="GasOneMany[many_id][]"  value="'+ui.item.id+'" type="hidden">';
            var tr = '<tr><td class="item_c order_no"></td>'+td_name+td_remove+input+'</tr>';
            if($('.tb_deloy_by tbody').find('.'+ClassCheck).size() < 1 ) {
                $('.tb_deloy_by tbody').append(tr);
            }
            fnRefreshOrderNumber();
        }
    }
    
    function fnShowDateApply(type){
        $('.RowDateApply').hide();
        var strDate = $.datepicker.formatDate('dd-mm-yy', new Date());
        if($('.SetupTypeDateApply'+type).size() > 0){
            $('.RowDateApply').show();
            if($('.RowDateApply').find(':input').val() == ''){
                $('.RowDateApply').find(':input').val(strDate);
            }
        }else{
            $('.RowDateApply').find(':input').val('');
        }
    }

</script>
