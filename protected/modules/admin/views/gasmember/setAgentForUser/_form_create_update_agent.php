<?php
$aUser = [];
if($model->type && $model->one_id){
    $aId = GasOneMany::getArrOfManyId($model->one_id, $model->type, $model->getDateApplyYmd());
    if($model->type == GasOneMany::TYPE_GDKV_PROVINE){
        $mGasProvince = new GasProvince();
        $aUser = $mGasProvince->getProvinceByID($aId);
    }else{
        $aUser = Users::getArrayModelByArrayId($aId);
    }
}
$model->many_id = null;
$index=1; $model->getUrlByType();
?>
<div class="row ">
    <?php echo $form->labelEx($model,'many_id', ['label'=> $model->setupLabel]); ?>
    <?php echo $form->hiddenField($model,'many_id', array('class'=>'')); ?>
    <?php
        // 1. limit search kh của sale
//        $url = Yii::app()->createAbsoluteUrl('admin/ajax/search_by_role', array('role'=> ROLE_AGENT));
        // widget auto complete search user customer and supplier
        $aData = array(
            'model'=>$model,
            'field_customer_id'=>'many_id',
            'url'=> $model->setupUrl,
            'name_relation_user'=>'many',
            'ClassAdd' => 'w-400',
            'field_autocomplete_name' => 'autocomplete_name_1',
            'placeholder'=> $model->setupPlaceholder,
            'fnSelectCustomerV2' => 'fnDeloyBySelect',
            'doSomethingOnClose' => 'doSomethingOnClose',
        );
        $this->widget('ext.SpaAutocompleteCustomer.SpaAutocompleteCustomer',
            array('data'=>$aData));
        ?>
    <?php echo $form->error($model,'one_id'); ?>
</div>
<div class="row">
    <label>&nbsp</label>
    <table class="materials_table hm_table tb_deloy_by w-500">
        <thead>
            <tr>
                <th class="item_c w-20">#</th>
                <th class="item_code item_c w-300">Đại lý</th>
                <th class="item_unit last item_c">Xóa</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach($aUser as $key => $mUser): ?>
            <?php // if(is_null($mUser)) continue; ?>
            <tr>
                <input name="GasOneMany[many_id][]"  value="<?php echo $key; ?>" type="hidden">
                <td class="item_c order_no"><?php echo $index++; ?></td>
                <td class="uid_<?php echo $mUser->id?>"><?php
                if($model->type == GasOneMany::TYPE_GDKV_PROVINE){
                    echo $mUser->name. "  ---  ".$mUser->id;
                }else{
                    echo '<b>'.AppPromotion::getRefCodeUser($mUser). '</b> -- ' . $mUser->getNameWithRole(). "  ---  ".$mUser->id;
                }
                ?></td>
                <td class="item_c last"><span class="remove_icon_only"></span></td>
            </tr>
            <?php endforeach;?>
        </tbody>
    </table>
</div>
<script>
/**
* @Author: ANH DUNG Jun 23, 2015
* @Todo: function này dc gọi từ ext của autocomplete
* @Param: $model
*/
function fnDeloyBySelectMany(ui, idField, idFieldCustomer){
    var row = $(idField).closest('.row');
    var ClassCheck = "uid_"+ui.item.id;
    var td_name = '<td class="'+ClassCheck+'">'+ui.item.name_role+'</td>';
    var td_remove = '<td class="item_c last"><span class="remove_icon_only"></span></td>';
    var input = '<input name="GasMaintainSell[one_many_gift][]"  value="'+ui.item.id+'" type="hidden">';
    var tr = '<tr><td class="item_c order_no"></td>'+td_name+td_remove+input+'</tr>';
    if($('.tb_deloy_by tbody').find('.'+ClassCheck).size() < 1 ) {
        $('.tb_deloy_by tbody').append(tr);
    }
    fnRefreshOrderNumber();
}

/**
* @Author: ANH DUNG Dec 28, 2016
* @Todo: function này dc gọi từ ext của autocomplete, action close auto
*/
function doSomethingOnClose(ui, idField, idFieldCustomer){
    var row = $(idField).closest('.row');
    row.find('.remove_row_item').trigger('click');
}
</script>