<?php 
$model->getUrlOneByType();
?>
<div class="row">
    <?php echo $form->labelEx($model,'one_id', array('label'=>$model->setupLabelOne)); ?>
    <?php echo $form->hiddenField($model,'one_id', array('class'=>'one_id')); ?>
    <?php
        // 1. limit search kh của sale
        $url = $model->setupUrlOne;
        // widget auto complete search user customer and supplier
        $aData = array(
            'model'=>$model,
            'field_customer_id'=>'one_id',
            'url'=> $url,
            'name_relation_user'=>$model->relationUrl,
            'ClassAdd' => 'w-400',
            'enableFied' => $model->enableFied,
            'nameView' => $model->nameView,
            'ShowTableInfo' => $model->showTableInfoUrl,
            'placeholder'=> $model->setupPlaceholderOne,
//                'ShowTableInfo' => 0,
            'fnSelectCustomerV2' => "fnDeloyBySelect",
//            'doSomethingOnClose' => "doSomethingOnClose",
        );
        $this->widget('ext.SpaAutocompleteCustomer.SpaAutocompleteCustomer',
            array('data'=>$aData));                                        
    ?>
</div>
