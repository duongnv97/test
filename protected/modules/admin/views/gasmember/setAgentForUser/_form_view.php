<?php
$gridCss = Yii::app()->getAssetManager()->publish(Yii::getPathOfAlias('zii.widgets.assets')) . '/gridview/styles.css';
Yii::app()->getClientScript()->registerCssFile($gridCss);
$aOneId         = [];
$aManyId        = [];
$aModel         = [];
if($model->type){
    $mGasProvince   = new GasProvince();
    $aMaynyIdOneId  = GasOneMany::getManyIdByTypeAndDateApply($model->type, $model->getDateApplyYmd());
    $aTreeView      = $mGasProvince->convertToArrayView($aMaynyIdOneId,$aOneId,$aManyId);
}
$model->getNameModelByType();
$aShowNameOne   = $model->getShowView($aOneId,$aModel,$model->nameModelOne,$model->nameFieldOne);
$aShowNameMany  = $model->getShowView($aManyId,$aModel,$model->nameModelMany,$model->nameFieldMany);
?>
<div class="grid-view">
    <table id="report" class="items hm_table" style="width: 50%">
        <thead>
        <tr class="h_20">
            <th class="w-20 ">#</th>
            <th class="w-120 ">Tên</th>
        </tr>
        </thead>
        <tbody>
            <?php foreach ($aTreeView as $one_id => $aManyId): $i=1;?>
            <tr>
                <td class="item_b" colspan="2"><?php echo isset($aShowNameOne[$one_id]) ? $aShowNameOne[$one_id] : ''; ?></td>
            </tr>
            <?php foreach ($aManyId as $many_id => $_many_id): ?>
            <tr>
                <td class="item_c"><?php echo $i++; ?></td>
                <?php 
                    $codeNAme = '';
                    if(isset($aModel[$many_id])){
                        $codeNAme = '<b>'.AppPromotion::getRefCodeUser($aModel[$many_id]).'</b>' . '-- ';
                    }
                ?>
                <td class="item_l"><?php echo $codeNAme;echo isset($aShowNameMany[$many_id]) ? $aShowNameMany[$many_id] : '' ?></td>
            </tr>
            <?php endforeach; ?>
            <?php endforeach; ?>
        </tbody>
    </table>
</div>
<script>
     $(window).load(function () {
        alert();
        fnAddClassOddEven('items');
     }
</script>
