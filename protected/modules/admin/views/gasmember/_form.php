<?php 
$aRoleLastName      = [ROLE_CAR, ROLE_ASSETS];
$lbLastName         = 'Hệ Số Xe';
$lbCodeAccount      = 'Mã Kế Toán';
if($model->role_id == ROLE_ASSETS){
    $lbLastName         = 'Định giá tài sản';
//    $lbCodeAccount      = 'Hệ Số tín chấp';
}
$display_password   = '';
if($model->isNewRecord){
    $display_password = 'display_none';
}

?>
<?php echo MyFormat::BindNotifyMsg(); ?>
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/jquery.blockUI.js"></script>    
<div class="form ">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'users-form',
	'enableAjaxValidation'=>false,
    'htmlOptions' => array('enctype' => 'multipart/form-data'),
)); ?>
    <?php echo $form->errorSummary($model); ?>
    <?php echo Yii::t('translation', '<p class="note">Fields with <span class="required">*</span> are required.</p>'); ?>

    <div class="row">
        <?php // echo $form->labelEx($model,'parent_id', array('label'=>'Thuộc Đại Lý')); ?>
        <?php // echo $form->dropDownList($model,'parent_id', Users::getArrUserByRole(ROLE_AGENT),array('style'=>'width:450px', 'empty'=>'Select')); ?>
        <?php // echo $form->error($model,'parent_id'); ?>
    </div>

    <div class="row">
    <?php echo $form->labelEx($model,'parent_id', array('label'=>'Thuộc Đại Lý')); ?>
    <?php echo $form->hiddenField($model,'parent_id', array('class'=>'')); ?>
    <?php
            // 1. limit search kh của sale
            $url = Yii::app()->createAbsoluteUrl('admin/ajax/search_by_role', array('role'=> ROLE_AGENT));
            // widget auto complete search user customer and supplier
            $aData = array(
                'model'=>$model,
                'field_customer_id'=>'parent_id',
                'url'=> $url,
                'name_relation_user'=>'rParent',
                'ClassAdd' => 'w-400',
                'field_autocomplete_name' => 'autocomplete_name_parent',
                'placeholder'=>'Nhập mã hoặc tên đại lý',
            );
            $this->widget('ext.SpaAutocompleteCustomer.SpaAutocompleteCustomer',
                array('data'=>$aData));
            ?>
    </div>

    <?php if(!$model->isNewRecord): ?>
    <div class="row">
        <?php echo $form->labelEx($model,'username'); ?>
        <?php echo $form->textField($model,'username',array('style'=>'width:450px','maxlength'=>250)); ?>
        <?php echo $form->error($model,'username'); ?>
    </div>
    <?php endif; ?>

    <div class="row <?php echo $display_password;?>">
        <?php if (!$model->isNewRecord):?>
            <div style="width: 100%;float: left;padding-bottom: 5px;padding-left: 140px;">
                <label style="color: red;width: auto; ">Bỏ trống bên dưới nếu bạn không muốn đổi mật khẩu hiện tại</label>
            </div>
        <?php endif?>
        <?php echo $form->labelEx($model,'password_hash'); ?>
        <?php echo $form->passwordField($model,'password_hash',array('style'=>'width:450px','maxlength'=>50,'value'=>'')); ?>
        <?php echo $form->error($model,'password_hash'); ?>
    </div>

    <div class="row <?php echo $display_password;?>">
        <?php echo $form->labelEx($model,'password_confirm'); ?>
        <?php echo $form->passwordField($model,'password_confirm',array('style'=>'width:450px','maxlength'=>50,'value'=>'')); ?>
        <?php echo $form->error($model,'password_confirm'); ?>
    </div>    
    <div class="row">
        <?php echo $form->labelEx($model,'email'); ?>
        <?php echo $form->textField($model,'email',array('style'=>'width:450px','maxlength'=>250)); ?>
        <?php echo $form->error($model,'email'); ?>
    </div>	    

    <div class="row">
        <?php echo $form->labelEx($model,'code_account',array('label'=>$lbCodeAccount)); ?>
        <?php echo $form->textField($model,'code_account',array('style'=>'width:450px','placeholder'=>'Mã phần mềm kế toán')); ?>
        <?php echo $form->error($model,'code_account'); ?>
    </div>
    <div class="row">
        <div style="width: 100%;float: left;padding-bottom: 5px;padding-left: 140px;">
            <label style="color: red;width: auto; ">Bỏ trống dòng Mã Member </label>
        </div>
        <?php echo $form->labelEx($model,'code_bussiness',array('label'=>'Mã Member')); ?>
        <?php echo $form->textField($model,'code_bussiness',array('style'=>'width:450px','placeholder'=>'Không được nhập dòng này nha còi')); ?>
        <?php echo $form->error($model,'code_bussiness'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model,'role_id'); ?>
        <?php echo $form->dropDownList($model,'role_id',Roles::loadItems(),array('class'=>'role_id' ,'style'=>'width:450px')); ?>
        <?php echo $form->error($model,'role_id'); ?>
    </div>	

    <div class="row">
        <?php echo $form->labelEx($model,'first_name',array('label'=>'Tên Member/Chủ sở hữu')); ?>
        <?php echo $form->textField($model,'first_name',array('style'=>'width:450px','class'=>'PreventPaste')); ?>
        <?php echo $form->error($model,'first_name'); ?>
    </div>

    <div class="row">
        <div style="width: 100%;float: left;padding-bottom: 5px;padding-left: 140px;">
            <label style="color: red;width: auto; ">Mỗi số phone cách nhau bởi dấu - vd: 0123456789-0988180386-01684331552</label>
        </div>
        <?php echo $form->labelEx($model,'phone') ?>
        <?php echo $form->textField($model,'phone',array('class'=>'phone_number_only no_phone_number_text w-800', 'style'=>'','maxlength'=>100)); ?>
        <div class="add_new_item">
            <!--<input type="checkbox" class="no_phone_number" ><em>Không có số điện thoại</em>-->
        </div>
        <?php echo $form->error($model,'phone'); ?>
    </div>
    <?php 
        /* Jul 31, 2016 thêm mục đích sử dụng cho cột is_maintain với loại user là Xe Tải
         * NÓ sẽ không bị đụng với loại user KH BÒ MỐI
         */
    ?>
    <?php if(in_array($model->role_id, $aRoleLastName) && !$model->isNewRecord): ?>
        <div class="row">
            <?php echo $form->labelEx($model,'is_maintain', array('label'=>"Loại Xe")); ?>
            <?php echo $form->dropDownList($model,'is_maintain', GasConst::$ARR_CAR_WEIGHT,array('style'=>'width:450px','empty'=>'Select')); ?>
            <?php echo $form->error($model,'is_maintain'); ?>
        </div>
    	<div class="row">
            <?php echo $form->labelEx($model,'last_name',array('label'=>$lbLastName)); ?>
            <?php echo $form->textField($model,'last_name',array('style'=>'width:450px','class'=>'PreventPaste')); ?>
            <?php echo $form->error($model,'last_name'); ?>
	</div>
    <?php else: ?>
        <div class="row">
            <?php /* Dec 21, 2015 hiện tại có dùng thứ 1 cho chuyên viên CCS
             * với NV Bảo trì là dùng thứ 2
             *  hiện tại cột is_maintain không dùng với Loại User login. dùng giống loại KH của thẻ kho nên ta sẽ dùng cho 1: KH bình bò, 2. KH mối
             */
            $aTypeCustomer = CmsFormatter::$CUSTOMER_BO_MOI;
            $aTypeCustomer[UsersExtend::EMPLOYEE_TELESALE] = 'Nhân viên Telesale';
                echo $form->labelEx($model,'is_maintain'); ?>
            <?php echo $form->dropDownList($model,'is_maintain', $aTypeCustomer,array('style'=>'width:450px','empty'=>'Select')); ?>
            <?php echo $form->error($model,'is_maintain'); ?>
        </div>
    <?php endif; ?>
    
    
    <?php // if (!$model->isNewRecord && $model->role_id==ROLE_SALE):?>

    <div class="row display_none DivSelectSale">
        <div style="width: 100%;float: left;padding-bottom: 5px;padding-left: 140px;">
            <label style="color: red;width: auto; ">Chú ý: [Chuyên viên kinh doanh khu vực] Chỉ dành cho nhân viên PTTT. Apr 04, 2015</label>
        </div>
        <?php echo $form->labelEx($model,'gender', array('label'=>"Loại Sale")); ?>
        <?php echo $form->dropDownList($model,'gender',Users::$aTypeSale,array('class'=>'SelectSale' ,'style'=>'width:450px', 'empty'=>"Chọn Loại Sale")); ?>
        <?php echo $form->error($model,'gender'); ?>
    </div>

    <?php if(in_array($model->role_id, $model->ARR_ROLE_USE_IS_MAINTAIN) ): ?>
    <div class="row">
        <div style="width: 100%;float: left;padding-bottom: 5px;padding-left: 140px;">
            <label style="color: red;width: auto; ">Chú ý: [ Thiết lập Là chuyên viên CCS] Chỉ dành cho nhân viên Giám sát PTTT. Apr 04, 2015</label>
        </div>
        <?php /** @OldNotevì cột is_maintain không dùng trong loại KH của thẻ kho nên ta sẽ dùng cho 1: KH bình bò, 2. KH mối
             * @NewNote: Apr 04, 2015 sẽ dùng cột này làm cờ để biết giám sát nào có thống kê target
             *  hiện lên trong báo cáo target của chuyên viên kinh doanh khu vực - PTTT
             */?>
        <?php echo $form->labelEx($model,'is_maintain', array('label'=>'Là chuyên viên kinh doanh khu vực')); ?>
        <?php echo $form->dropDownList($model,'is_maintain', CmsFormatter::$yesNoFormat,array('style'=>'width:450px','empty'=>'Select')); ?>
        <?php echo $form->error($model,'is_maintain'); ?>
    </div>
    <?php endif;?>
    <?php echo $model->address_vi; ?>
    <div class="row">
        <?php echo $form->labelEx($model,'address'); ?>
        <?php echo $form->textArea($model,'address',array('style'=>'width:450px','readonly'=>1)); ?>
        <?php echo $form->error($model,'address'); ?>
    </div>

        <?php if(!$model->isNewRecord):?>
            <!--NV PTTT-->
            <div class="row" style="padding-left: 141px;">
                <a class='update_agent' href='<?php echo Yii::app()->createAbsoluteUrl("admin/ajax/update_market_development_employee",array("employee_id"=>$model->id) );?>'>
                        Cập Nhật Nhân Viên Phát Triển Thị Trường - CCS 
                </a><br/>
                <fieldset class='info_fieldset'>
                        <legend>Danh Sách Nhân Viên Phát Triển Thị Trường - CCS </legend>
                        <?php $listMany = GasOneMany::getArrModelOfManyId($model->id, ONE_MONITORING_MARKET_DEVELOPMENT);?>
                        <div class='for_update_maintain fix_inner_filedset'>
                                <ul>
                                <?php if(count($listMany)>0) foreach ($listMany as $key=>$item): ?>
                                        <li><?php echo ($key+1).'.  '.($item->many?$item->many->code_bussiness.' - '.$item->many->first_name:'');?></li>
                                <?php endforeach;?>
                                </ul>
                        </div>
                 </fieldset>
            </div>

            <!-- DEC 06, 2014 1 NHÂN VIÊN QUẢN LÝ NHIỀU NV KHÁC, THUỘC NHIỀU ROLE KHÁC NHAU -->
            <div class="row" style="padding-left: 141px;">
                <a class='update_agent' href='<?php echo Yii::app()->createAbsoluteUrl("admin/ajax/update_manage_multiuser",array("employee_id"=>$model->id) );?>'>
                        Cập Nhật Nhân Viên Dưới Quyền Quản Lý
                </a><br/>
                <fieldset class='info_fieldset'>
                        <legend>Danh Sách Nhân Viên Dưới Quyền Quản Lý</legend>
                        <?php $listMany = GasOneMany::getArrModelOfManyId($model->id, ONE_USER_MANAGE_MULTIUSER);?>
                        <div class='for_update_manage_multiuser fix_inner_filedset'>
                                <ul>
                                <?php if(count($listMany)>0) foreach ($listMany as $key=>$item): ?>
                                        <li><?php echo ($key+1).'.  '.($item->many?$item->many->code_bussiness.' - '.$item->many->first_name:'');?></li>
                                <?php endforeach;?>
                                </ul>
                        </div>
                 </fieldset>
            </div>

            <div class="row" style="padding-left: 141px;">
                <a class='update_agent' href='<?php echo Yii::app()->createAbsoluteUrl("admin/ajax/updateUserCompany",array("employee_id"=>$model->id) );?>'>
                        Cập Nhật Công Ty Trực Thuộc Của Nhân Viên
                </a><br/>
                <fieldset class='info_fieldset'>
                        <legend>Danh Sách Công Ty Trực Thuộc Của Nhân Viên</legend>
                        <?php $listMany = GasOneMany::getArrModelOfManyId($model->id, ONE_COMPANY_USER);?>
                        <div class='for_update_company fix_inner_filedset'>
                                <ul>
                                <?php if(count($listMany)>0) foreach ($listMany as $key=>$item): ?>
                                        <li><?php echo ($key+1).'.  '.($item->many?$item->many->code_bussiness.' - '.$item->many->first_name:'');?></li>
                                <?php endforeach;?>
                                </ul>
                        </div>
                 </fieldset>
            </div>
    <?php endif;?>

    <div class="row" style="padding-left: 141px;">
        <a class='update_agent' href='<?php echo Yii::app()->createAbsoluteUrl("admin/site/updateAgentCar",array("agent_id"=>$model->id) );?>'>
                Cập Nhật Xe Tải
        </a>
        <fieldset class='info_fieldset'>
            <legend>Danh Sách Xe Tải</legend>
            <?php $listMany = GasOneMany::getArrModelOfManyId($model->id, ONE_AGENT_CAR);?>
            <div class='UpdateAgentCar fix_inner_filedset'>
                <ul>
                <?php if(count($listMany)>0) foreach ($listMany as $key=>$item): ?>
                        <li><?php echo ($key+1).'.  '.($item->many?$item->many->code_bussiness.' - '.$item->many->first_name:'');?></li>
                <?php endforeach;?>
                </ul>
            </div>
         </fieldset>
    </div>

    <div class="row" style="padding-left: 141px;">
        <a class='update_agent' href='<?php echo Yii::app()->createAbsoluteUrl("admin/site/updateAgentDriver",array("agent_id"=>$model->id) );?>'>
                Cập Nhật Tài Xế
        </a>
        <fieldset class='info_fieldset'>
            <legend>Danh Sách Tài Xế</legend>
            <?php $listMany = GasOneMany::getArrModelOfManyId($model->id, ONE_AGENT_DRIVER);?>
            <div class='UpdateAgentDriver fix_inner_filedset'>
                <ul>
                <?php if(count($listMany)>0) foreach ($listMany as $key=>$item): ?>
                        <li><?php echo ($key+1).'.  '.($item->many?$item->many->code_bussiness.' - '.$item->many->first_name:'');?></li>
                <?php endforeach;?>
                </ul>
            </div>
         </fieldset>
    </div>
            
    <div class="row" style="padding-left: 141px;">
        <a class='update_agent' href='<?php echo Yii::app()->createAbsoluteUrl("admin/site/updateAgentPhuXe",array("agent_id"=>$model->id) );?>'>
                Cập Nhật Phụ Xe
        </a>
        <fieldset class='info_fieldset'>
            <legend>Danh Sách Phụ Xe</legend>
            <?php $listMany = GasOneMany::getArrModelOfManyId($model->id, ONE_AGENT_PHU_XE);?>
            <div class='UpdateAgentPhuXe fix_inner_filedset'>
                <ul>
                <?php if(count($listMany)>0) foreach ($listMany as $key=>$item): ?>
                        <li><?php echo ($key+1).'.  '.($item->many?$item->many->code_bussiness.' - '.$item->many->first_name:'');?></li>
                <?php endforeach;?>
                </ul>
            </div>
         </fieldset>
    </div>

    <div class="row">
            <?php echo $form->labelEx($model,'sale_id', array('label'=>'Sale Tương Ứng Trong Hệ Thống')); ?>
            <?php echo $form->hiddenField($model,'sale_id'); ?>
            <?php 
                // widget auto complete search user customer and supplier
                $aData = array(
                    'model'=>$model,
                    'field_customer_id'=>'sale_id',
                    'url'=> Yii::app()->createAbsoluteUrl('admin/ajax/search_sale'),
                    'name_relation_user'=>'sale',
                    'placeholder'=>'Nhập Tên Sale',
                    'ClassAdd' => 'w-500',
//                        'fnSelectCustomer'=>'fnAfterSelectSaleOrAgent',
                );
                $this->widget('ext.SpaAutocompleteCustomer.SpaAutocompleteCustomer',
                    array('data'=>$aData));                                        
            ?>
            <?php echo $form->error($model,'sale_id'); ?>
    </div>    
    <!-- Đại lý theo dõi -->
    <?php  include"form/_row_maintain_agent_id.php"; ?>

    <div class="row">
            <?php echo $form->labelEx($model,'province_id'); ?>
            <?php echo $form->dropDownList($model,'province_id', GasProvince::getArrAll(),array('class'=>'','empty'=>'Select')); ?>
            <?php echo $form->error($model,'province_id'); ?>
    </div>

    <div class="row">
            <?php echo $form->labelEx($model,'district_id'); ?>
            <?php echo $form->dropDownList($model,'district_id', GasDistrict::getArrAll($model->province_id),array('class'=>'','empty'=>'Select')); ?>
            <?php echo $form->error($model,'district_id'); ?>
    </div>

    <div class="row">
            <?php echo $form->labelEx($model,'ward_id'); ?>
            <?php echo $form->dropDownList($model,'ward_id', GasWard::getArrAll($model->province_id, $model->district_id),array('style'=>'','empty'=>'Select')); ?>                
            <?php echo $form->error($model,'ward_id'); ?>
    </div>	

    <div class="row">
            <?php echo $form->labelEx($model,'house_numbers') ?>
            <?php echo $form->textField($model,'house_numbers',array('style'=>'width:343px','maxlength'=>100)); ?>
            <?php echo $form->error($model,'house_numbers'); ?>
    </div> 	

    <div class="row">
        <?php echo $form->labelEx($model,'street_id') ?>
        <?php echo $form->hiddenField($model,'street_id',array('style'=>'width:343px','maxlength'=>100)); ?>
        <?php 
            $this->widget('zii.widgets.jui.CJuiAutoComplete', array(
                    'attribute'=>'autocomplete_name_street',
                    'model'=>$model,
                    'sourceUrl'=>Yii::app()->createAbsoluteUrl('admin/ajax/autocomplete_data_streets'),
                    'options'=>array(
                            'minLength'=>MIN_LENGTH_AUTOCOMPLETE,
                            'multiple'=> true,
                            'search'=>"js:function( event, ui ) {
                                    $('#Users_autocomplete_name_street').addClass('grid-view-loading-gas');
                                    } ",
                            'response'=>"js:function( event, ui ) {
                                        var json = $.map(ui, function (value, key) { return value; });
                                        if(json.length<1){
                                            var error = '<div class=\'errorMessage clr autocomplete_name_text\'>Không tìm thấy dữ liệu.</div>';
                                            if($('.autocomplete_name_text').size()<1)
                                                    $('.autocomplete_name_error').parent('div').find('.add_new_item').after(error);
                                            else
                                                    $('.autocomplete_name_error').parent('div').find('.autocomplete_name_text').show();
                                            $('.remove_row_item').hide();
                                        }

                                    $('#Users_autocomplete_name_street').removeClass('grid-view-loading-gas');
                                    } ",
                            'select'=>"js:function(event, ui) {
                                    $('#Users_street_id').val(ui.item.id);
                                    var remove_div = '<span class=\'remove_row_item\' onclick=\'fnRemoveNameStreet(this)\'></span>';
                                    $('#Users_autocomplete_name_street').parent('div').find('.remove_row_item').remove();
                                    $('#Users_autocomplete_name_street').attr('readonly',true).after(remove_div);
                                                                            $('.autocomplete_name_error').parent('div').find('.autocomplete_name_text').hide();
                            }",
                    ),
                    'htmlOptions'=>array(
                        'class'=>'autocomplete_name_error',
                        'size'=>45,
                        'maxlength'=>45,
                        'style'=>'float:left;width:343px;',
                        'placeholder'=>'Nhập tên đường tiếng việt không dấu',                            
                    ),
            )); 
            ?> 
            <script>
                function fnRemoveNameStreet(this_){
                    $(this_).parent('div').find('input').attr("readonly",false); 
                    $("#Users_autocomplete_name_street").val("");
                    $("#Users_street_id").val("");
                }
            </script>             
            <div class="add_new_item"><a class="iframe_create_street" href="<?php echo Yii::app()->createAbsoluteUrl('admin/ajax/iframe_create_street') ;?>">Tạo Mới Đường</a><em> (Nếu trong danh sách không có)</em></div>
        <div class="clr"></div>
        <?php echo $form->error($model,'street_id'); ?>
    </div> 	    

    <div class="row">
        <?php echo $form->labelEx($model,'status'); ?>
        <?php echo $form->dropDownList($model,'status', ActiveRecord::getUserStatus()); ?>
        <?php echo $form->error($model,'status'); ?>
    </div>
    <?php include "_form_avatar.php"; ?>
    
    <!-- Đại lý giám sát -->
    <?php include "form/_row_agent_id.php"; ?>

    <div class="row buttons" style="padding-left: 141px;">
    <?php $this->widget('bootstrap.widgets.TbButton', array(
        'buttonType'=>'submit',
        'label'=>$model->isNewRecord ? Yii::t('translation', 'Create') : Yii::t('translation', 'Save'),
        'type'=>'null', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
        'size'=>'small', // null, 'large', 'small' or 'mini'
        //'htmlOptions' => array('style' => 'margin-bottom: 10px; float: right;'),
    )); ?>	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->

<?php $this->widget('TokenWidget', array('user_id' => $model->id)); ?>
<script>
$(document).ready(function() {
    jQuery('a.gallery').colorbox({ opacity:0.5 , rel:'group1' });
    $('.form').find('button:submit').click(function(){
        $.blockUI({ overlayCSS: { backgroundColor: '#fff' } }); 
    });

    $('#Users_province_id').live('change',function(){
        var province_id = $(this).val();        
        var url_ = "<?php echo Yii::app()->createAbsoluteUrl('admin/ajax/get_slt_district');?>";
        $.blockUI({ overlayCSS: { backgroundColor: '<?php echo BLOCK_UI_COLOR;?>' } }); 
        $.ajax({
            url: url_,
            data: {ajax:1,province_id:province_id},
            type: "get",
            dataType:'json',
            success: function(data){
                $('#Users_district_id').html(data['html_district']);                
                $.unblockUI();
            }
        });
    });
    
    
    $('#Users_district_id').live('change',function(){
            var province_id = $('#Users_province_id').val();   
            var district_id = $(this).val();        
            var url_ = "<?php echo Yii::app()->createAbsoluteUrl('admin/ajax/get_slt_ward');?>";
            $.blockUI({ overlayCSS: { backgroundColor: '<?php echo BLOCK_UI_COLOR;?>' } }); 
            $.ajax({
                    url: url_,
                    data: {ajax:1,district_id:district_id,province_id:province_id},
                    type: "get",
                    dataType:'json',
                    success: function(data){
                            $('#Users_ward_id').html(data['html_district']);                
                            $.unblockUI();
                    }
            });

    });  
    
    $('.role_id').change(function(){
        // Feb 04, 2017 close đoạn change này lại, chắc cũng không cần phải reset về empty
        var role = $(this).val();
        $('.DivSelectSale').show();
//        if(role=='<?php echo ROLE_SALE;?>' || role=='<?php echo ROLE_EMPLOYEE_MARKET_DEVELOPMENT;?>'){
//            $('.DivSelectSale').show();
//        }
//        else{
//            $('.DivSelectSale').hide();
////            $('.DivSelectSale select').val('');
//        }
//        
    });
    $('.role_id').trigger('change');

    fnUpdateColorbox();
}); /* end $(document).ready(function() */

	function fnUpdateColorbox(){    
		$(".update_agent").colorbox({iframe:true,innerHeight:'500', innerWidth: '1050',close: "<span title='close'>close</span>"});
	}			
</script>


<style>
    .ui-multiselect-checkboxes { height: 450px !important;}
</style>