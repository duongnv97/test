<?php
$gridCss = Yii::app()->getAssetManager()->publish(Yii::getPathOfAlias('zii.widgets.assets')).'/gridview/styles.css';
Yii::app()->getClientScript()->registerCssFile($gridCss);
$aTemp = array('Month');
foreach($model->getArrayUpholdType() as $typeId=>$typeText) {
    $aTemp[] = $typeText;
}
$aDataChart[] = $aTemp;
//$aDataChart[] = array('Tháng 1', 2, 4, 5, 4, 5);
//$aDataChart[] = array('Tháng 2', 1, 1, 1, 4, 5);
$tempChart = array();
?>
<div class="grid-view ">
<table class="items hm_table">
    <thead>
        <tr style="">
            <th class="w-100 item_c ">Sự cố</th>
            <?php foreach ($data as $month=>$aType): ?>
            <th class="item_c " style="">
                Tháng <?php echo $month; ?>
            </th>
            <?php endforeach;?>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($model->getArrayUpholdType() as $typeId=>$typeText): ?>
        <tr>
            <td class="item_b"><?php echo $typeText;?></td>
            <?php foreach ($data as $month=>$aType): ?>
            <?php 
                $value = isset($data[$month][$typeId]) ? $data[$month][$typeId] : 0;
                if(!isset($tempChart[$month])){
                    $tempChart[$month] = array("Tháng $month");
                }
                $tempChart[$month][] = $value*1;
            ?>
            <td class="item_c item_b" style="">
                <?php echo $value; ?>
            </td>
            <?php endforeach;?>
            
        </tr>
        <?php endforeach; ?>
        
    </tbody>
</table>
</div>
<?php 
//    echo '<pre>';
//print_r($tempChart);
//echo '</pre>';
//die;
?>
<?php 
    foreach ($tempChart as $aValue){
        $aDataChart[] = $aValue; 
    }
?>


<div class="float_l" style="width: 800px">
    <div id="curve_chart" style="width: 900px; height: 500px"></div>
</div>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>

<?php $js_array = json_encode($aDataChart); 
//echo "var javascript_array = ". $js_array . ";\n";
?>
<script type="text/javascript">
    fnAddClassOddEven('items');
    google.charts.load("current", {packages:["corechart"]});
    google.charts.setOnLoadCallback(drawChart);
    function drawChart() {
//      var data = google.visualization.arrayToDataTable([
//        ['Task', 'Hours per Day'],
//        ['Work',     11],
//        ['Eat',      2],
//        ['Commute',  2],
//        ['Watch TV', 202],
//        ['Sleep',    7]
//      ]);
      
      var data = google.visualization.arrayToDataTable(<?php echo $js_array;?>);;
      
      var options = {
          title: '',
          curveType: 'function',
          legend: { position: 'center' }
        };
//      var options = {
//        legend: {alignment: 'center'}
//      };

      var chart = new google.visualization.LineChart(document.getElementById('curve_chart'));
      chart.draw(data, options);
    }
  </script>
