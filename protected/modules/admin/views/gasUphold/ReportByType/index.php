<?php
$this->breadcrumbs=array(
    'Báo cáo phân tích theo sự cố',
);
?>

<h1>Báo cáo phân tích theo sự cố</h1>
<div class="search-form" style="">
    <div class="wide form">

    <?php $form=$this->beginWidget('CActiveForm', array(
            'action'=>Yii::app()->createUrl($this->route),
            'method'=>'get',
    )); ?>
        <?php if(Yii::app()->user->hasFlash('successUpdate')): ?>
            <div class="success_div"><?php echo Yii::app()->user->getFlash('successUpdate');?></div>         
        <?php endif; ?>    

        <div class="row">
            <?php echo $form->labelEx($model,'report_year', array()); ?>
            <?php echo $form->dropDownList($model,'report_year', ActiveRecord::getRangeYear(2016), array('class'=>'')); ?>
        </div>
            
        <div class="row">
            <?php echo $form->labelEx($model,'employee_id'); ?>
            <?php echo $form->hiddenField($model,'employee_id', array('class'=>'')); ?>
            <?php
                // 1. limit search kh của sale
                $url = Yii::app()->createAbsoluteUrl('admin/ajax/search_by_role', array('role'=> implode(',', GasUphold::$ROLE_EMPLOYEE_BAOTRI)));
                // widget auto complete search user customer and supplier
                $aData = array(
                    'model'=>$model,
                    'field_customer_id'=>'employee_id',
                    'url'=> $url,
                    'name_relation_user'=>'rEmployee',
                    'field_autocomplete_name'=>'autocomplete_name_second',
                    'ClassAdd' => 'w-400',
    //                'ShowTableInfo' => 0,
                );
                $this->widget('ext.SpaAutocompleteCustomer.SpaAutocompleteCustomer',
                    array('data'=>$aData));                                        
            ?>

            <?php echo $form->error($model,'employee_id'); ?>
        </div>
            
        <div class="row buttons" style="padding-left: 159px; margin:20px 0;">
                <?php $this->widget('bootstrap.widgets.TbButton', array(
                'buttonType'=>'submit',
                'label'=>Yii::t('translation','Xem Thống Kê'),
                'type'=>'null', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
                'size'=>'small', // null, 'large', 'small' or 'mini'
                //'htmlOptions' => array('style' => 'margin-bottom: 10px; float: right;'),
            )); ?>	
        </div>

    <?php $this->endWidget(); ?>

    </div><!-- wide form -->
</div><!-- search-form -->

<style>
    .ui-datepicker-trigger { float: left;}
</style>

<script>
$(document).ready(function(){
    $('.form').find('button:submit').click(function(){        
        $.blockUI({ overlayCSS: { backgroundColor: '#fff' } }); 
    });   
 
});
</script>

<?php if(count($data)): ?>
    <?php include 'ReportForm.php'; ?>
<?php endif; ?>