<?php
$this->breadcrumbs=array(	
    $this->pageTitle,
);

$menus=array();
$this->menu= ControllerActionsName::createMenusRoles($menus, $actions);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});

");


$dataProvider = $model->searchSchedule();
?>
<h1><?php echo $this->pageTitle;?></h1>
<?php $this->renderPartial('application.modules.admin.views.gasUpholdSchedule.index_button',array());?>
<?php echo CHtml::link('Tìm Kiếm Nâng Cao','#',array('class'=>'search-button')); ?>
<div class="search-form" style="">
<?php include '_search.php'; ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'gas-uphold-grid',
	'dataProvider' => $dataProvider,
//        'afterAjaxUpdate'=>'function(id, data){ fnUpdateColorbox();}',
        'template'=>'{pager}{summary}{items}{pager}{summary}',
//    'itemsCssClass' => 'items custom_here',
//    'htmlOptions'=> array('class'=>'grid-view custom_here'),
        'pager' => array(
            'maxButtonCount'=>  CmsFormatter::$PAGE_MAX_BUTTON,
        ),
	'enableSorting' => false,
	'ajaxUpdate' => false,
	//'filter'=>$model,
	'columns'=>array(
        array(
            'header' => 'S/N',
            'type' => 'raw',
            'value' => '$this->grid->dataProvider->pagination->currentPage * $this->grid->dataProvider->pagination->pageSize + ($row+1)',
            'headerHtmlOptions' => array('width' => '10px','style' => 'text-align:center;'),
            'htmlOptions' => array('style' => 'text-align:center;')
        ),
            array(
                'name'=>'customer_id',
                'type'=>'html',
                'value'=>'$data->getUpholdInfo().$data->getCustomerInfo()',
                'htmlOptions' => array('style' => 'width:100px;')
            ),
//            array(
//                'header' => 'Loại KH',
//                'value'=>'$data->getCustomerType()',
////                'htmlOptions' => array('style' => 'width:50px;')
//            ),
            array(
                'header' => 'Chi tiết',
                'type' => 'raw',
                'value'=>'$data->getReplyContent()',
                'htmlOptions' => array('style' => 'width:150px;')
            ),
//            array(
//                'name'=>'uid_login',
//                'value'=>'$data->getUidLogin()',
//                'htmlOptions' => array('style' => 'width:100px;')
//            ),

            array(
                'header' => 'Map',
                'type' => 'raw',
                'value' => '$data->getHtmlMap()',
                'htmlOptions' => array('style' => 'width:60%;')
            ),     
	),
)); ?>

<script src="http://maps.google.com/maps/api/js?key=AIzaSyCfsbfnHgTh4ODoXLXFfEFhHskDhnRVtjQ" type="text/javascript"></script>
<?php include "index_map.php"; ?>
<script>
$(document).ready(function() {
    fnUpdateColorbox();
});

function fnUpdateColorbox(){   
    fixTargetBlank();
    $(".gallery").colorbox({iframe:true,innerHeight:'1100', innerWidth: '1000',close: "<span title='close'>close</span>"});
}
</script>