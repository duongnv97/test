<?php
$this->breadcrumbs=array(
	$this->singleTitle=>array('index'),
	$model->code_no=>array('view','id'=>$model->id),
	'Cập Nhật',
);

$menus = array(	
        array('label'=>$this->singleTitle, 'url'=>array('index')),
	array('label'=>"Xem $this->singleTitle", 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>"Tạo Mới $this->singleTitle", 'url'=>array('create')),	
);
$this->menu= ControllerActionsName::createMenusRoles($menus, $actions);

?>

<h1>Cập Nhật <?php echo $this->singleTitle;?>: <?php echo $model->code_no; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>