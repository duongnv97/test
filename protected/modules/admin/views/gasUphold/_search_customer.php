<div class="clr"></div>

<div class="row more_col">
    <div class="col1">
    </div>

    <div class="col2">
        <?php echo $form->labelEx($model,'province_id'); ?>
        <?php echo $form->dropDownList($model,'province_id', GasProvince::getArrAll(),array('style'=>'','class'=>'w-200','empty'=>'Select')); ?>
        <?php echo $form->error($model,'province_id'); ?>
    </div> 	

    <div class="col3">
        <?php echo $form->labelEx($model,'district_id'); ?>
        <?php echo $form->dropDownList($model,'district_id', GasDistrict::getArrAll($model->province_id),array('class'=>'w-200', 'empty'=>"Select")); ?>
        <?php echo $form->error($model,'district_id'); ?>
    </div>
</div>

<div class="row">
    <?php echo $form->labelEx($model,'sale_id'); ?>
    <?php echo $form->hiddenField($model,'sale_id'); ?>
    <?php // echo $form->dropDownList($model,'sale_id', GasUphold::getArrUserByRole(ROLE_SALE, array('order'=>'t.code_bussiness', 'prefix_type_sale'=>1)),array('style'=>'width:500px','empty'=>'Select')); ?>
    <?php 
        // widget auto complete search user customer and supplier
        $aData = array(
            'model'=>$model,
            'field_customer_id'=>'sale_id',
            'url'=> Yii::app()->createAbsoluteUrl('admin/ajax/search_sale'),
            'name_relation_user'=>'rSale',
            'field_autocomplete_name'=>'autocomplete_name_sale',
            'placeholder'=>'Nhập Tên Sale',
            'ClassAdd' => 'w-400',
//                        'fnSelectCustomer'=>'fnAfterSelectSaleOrAgent',
        );
        $this->widget('ext.SpaAutocompleteCustomer.SpaAutocompleteCustomer',
            array('data'=>$aData));                                        
    ?>
    <?php echo $form->error($model,'sale_id'); ?>
</div>

<div class="clr"></div>

<script>
    $(document).ready(function(){
        fnBindChangeProvince();
    });
    
    /**
    * @Author: ANH DUNG Apr 20, 2016
    */
    function fnBindChangeProvince(){
        $('body').on('change', '#GasUphold_province_id', function(){
            var province_id = $(this).val();        
            var url_ = "<?php echo Yii::app()->createAbsoluteUrl('admin/ajax/get_slt_district');?>";
            $.blockUI({ overlayCSS: { backgroundColor: '<?php echo BLOCK_UI_COLOR;?>' } }); 
            $.ajax({
                url: url_,
                data: {ajax:1,province_id:province_id}, 
                type: "get",
                dataType:'json',
                success: function(data){
                    $('#GasUphold_district_id').html(data['html_district']);                
                    $.unblockUI();
                }
            });
        });

//        $('body').on('change', '#GasUphold_district_id', function(){
//            var province_id = $('#GasUphold_province_id').val();   
//            var district_id = $(this).val();        
//            var url_ = "<?php echo Yii::app()->createAbsoluteUrl('admin/ajax/get_slt_ward');?>";
//            $.blockUI({ overlayCSS: { backgroundColor: '<?php echo BLOCK_UI_COLOR;?>' } }); 
//            $.ajax({
//                url: url_,
//                data: {ajax:1,district_id:district_id,province_id:province_id},
//                type: "get",
//                dataType:'json',
//                success: function(data){
//                    $('#GasUphold_ward_id').html(data['html_district']);                
//                    $.unblockUI();
//                }
//            });
//        });
    }
</script>