<?php if(!empty($model->customer_id)): ?>
<h3 class='title-info'>Lịch sử các lần bảo trì gần nhất</h3>
<?php 
$aHistory = $model->getHistoryCustomer();
?>
<?php foreach ($aHistory as $key=>$model): ?>
    <?php 
        $linkViewDetail = Yii::app()->createAbsoluteUrl("admin/gasUphold/view", array('id'=>$model->id));
    ?>
    <p class="">Lần <?php echo $key+1;?> : Ngày <?php echo MyFormat::dateConverYmdToDmy($model->created_date); ?> <a class="gas_link" href="<?php echo $linkViewDetail;?>" target="_blank">Click Xem Chi Tiết</a></p>
    <?php include 'view_xdetail.php'; ?>
<?php endforeach; ?>
<?php endif; ?>