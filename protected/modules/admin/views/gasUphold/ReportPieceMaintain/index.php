<?php
$this->breadcrumbs=array(
    'Báo cáo khoán bảo trì',
);
?>

<h1>Báo cáo khoán bảo trì</h1>
<div class="search-form" style="">
    <div class="wide form">

    <?php $form=$this->beginWidget('CActiveForm', array(
            'action'=>Yii::app()->createUrl($this->route),
            'method'=>'get',
    )); ?>  

        <div class="row">
            <?php echo $form->labelEx($model,'aProvinceId'); ?>
            <div class="fix-label">
                <?php
                   $this->widget('ext.multiselect.JMultiSelect',array(
                         'model'=>$model,
                         'attribute'=>'aProvinceId',
                         'data'=> GasProvince::getArrAll(),
                         // additional javascript options for the MultiSelect plugin
                        'options'=>array('selectedList' => 30,),
                         // additional style
                         'htmlOptions'=>array('style' => 'width: 800px;'),
                   ));    
               ?>
            </div>
        </div>
        
        <div class="row">
            <?php echo $form->labelEx($model,'area_id', array('label' => 'Khu Vực')); ?>
            <?php echo $form->dropDownList($model,'area_id', GasOrders::getTargetZone(),array('class' => '', 'empty'=>'Select')); ?>
        </div>
            
        <div class="row buttons" style="padding-left: 159px; margin:20px 0;">
                <?php $this->widget('bootstrap.widgets.TbButton', array(
                'buttonType'=>'submit',
                'label'=>Yii::t('translation','Xem Thống Kê'),
                'type'=>'null', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
                'size'=>'small', // null, 'large', 'small' or 'mini'
            )); ?>	
        </div>

    <?php $this->endWidget(); ?>

    </div><!-- wide form -->
</div><!-- search-form -->

<style>
    .ui-datepicker-trigger { float: left;}
</style>

<script>
$(document).ready(function(){
    $('.form').find('button:submit').click(function(){        
        $.blockUI({ overlayCSS: { backgroundColor: '#fff' } }); 
    });   
 
});
</script>

<?php // if(isset($data['VALUE'])): ?>
    <?php include 'ReportForm.php'; ?>
<?php // endif; ?>