<?php
$gridCss = Yii::app()->getAssetManager()->publish(Yii::getPathOfAlias('zii.widgets.assets')).'/gridview/styles.css';
Yii::app()->getClientScript()->registerCssFile($gridCss);
$SALE_DATA = !empty($data['DATA_SALE']) ? $data['DATA_SALE'] : [];
$SALE_MODEL = !empty($data['SALE_MODEL']) ? $data['SALE_MODEL'] : [];

$EMPLOYEE_DATA = !empty($data['DATA_EMPLOYEE']) ? $data['DATA_EMPLOYEE'] : [];
$EMPLOYEE_MODEL = !empty($data['EMPLOYEE_MODEL']) ? $data['EMPLOYEE_MODEL'] : [];

$PROVINCES = !empty($data['PROVINCES']) ? $data['PROVINCES'] : [];
$ARR_PROVINCE = !empty($data['ARR_PROVINCE']) ? $data['ARR_PROVINCE'] : [];

// Get table height
$freeHeightTable = (sizeof($SALE_DATA) + 1) * 19;
$freeHeightTable = ($freeHeightTable > 400) ? 400 : $freeHeightTable;

$freeHeightTable_employee = (sizeof($SALE_DATA) + 1) * 19;
$freeHeightTable_employee = ($freeHeightTable_employee > 400) ? 400 : $freeHeightTable_employee;

$i = 1;
?>
<script type="text/javascript"
        src="<?php echo Yii::app()->theme->baseUrl; ?>/js/scroll/jquery.freezetablecolumns.1.1.js"></script>
<h1>KHÁCH HÀNG</h1>
<div class="grid-view display_none">
    <table id="freezetablecolumns_sale" class="items hm_table freezetablecolumns">
        <thead>
            <tr class="h_30">
                <th class="w-30">#</th>
                <th class="w-220">Nhân viên kinh doanh</th>
                <th class="w-150">Tổng cộng</th>
                <?php foreach ($ARR_PROVINCE as $province_id): ?>
                    <th class="item_b w-100" colspan="2"><?php echo $PROVINCES[$province_id] ?></th>
                <?php endforeach; ?>
                <th class="item_b w-150" colspan="3">Tổng cộng</td>
            </tr>
            <tr class="item_b h_30 item_c">
                <td></td>
                <td></td>
                <td class="item_c item_b">Bò/mối/tổng</td>
                <?php foreach ($ARR_PROVINCE as $province_id): ?>
                    <td class="w-50">Bò</td>
                    <td class="w-50">Mối</td>
                <?php endforeach; ?>
                <td class="w-50">Bò</td>
                <td class="w-50">Mối</td>
                <td class="w-50">Tổng</td>
            </tr>
        </thead>
        <tbody>
            <?php
                $sumAllBo = 0;
                $sumAllMoi = 0;
            ?>
            <?php foreach ($SALE_MODEL as $sale_id => $mSale): ?>
                <?php
                    $sumBo = !empty($SALE_DATA[$sale_id]['sumBo']) ? $SALE_DATA[$sale_id]['sumBo'] : 0;
                    $sumMoi = !empty($SALE_DATA[$sale_id]['sumMoi']) ? $SALE_DATA[$sale_id]['sumMoi'] : 0;
                    $sumAllBo += $sumBo;
                    $sumAllMoi += $sumMoi;
                ?>
                <tr>
                    <td class="item_c"><?php echo $i++; ?></td>
                    <td class="item_b"><?php echo $mSale->first_name; ?></td>
                    <td class="item_c item_b"><?php echo $sumBo . '/' . $sumMoi . '/' . ($sumBo + $sumMoi) ?></td>
                    <?php foreach ($ARR_PROVINCE as $province_id): ?>
                        <?php
                            $binhBo = !empty($SALE_DATA[$sale_id][$province_id][STORE_CARD_KH_BINH_BO]) ? $SALE_DATA[$sale_id][$province_id][STORE_CARD_KH_BINH_BO] : '';
                            $binhMoi = !empty($SALE_DATA[$sale_id][$province_id][STORE_CARD_KH_MOI]) ? $SALE_DATA[$sale_id][$province_id][STORE_CARD_KH_MOI] : '';
                        ?>
                        <td class="w-50 item_r"><?php echo $binhBo; ?></td>
                        <td class="w-50 item_r"><?php echo $binhMoi; ?></td>
                    <?php endforeach; ?>
                    <td class="w-50 item_r"><?php echo $sumBo; ?></td>
                    <td class="w-50 item_r"><?php echo $sumMoi; ?></td>
                    <td class="w-50 item_r"><?php echo $sumBo + $sumMoi; ?></td>
                </tr>
            <?php endforeach; ?>
            <tr class="h_20">
                <td class="item_b item_c" colspan="3">Tổng cộng</td>
                <?php foreach ($ARR_PROVINCE as $province_id): ?>
                    <?php
                        $sumProvinceBo = !empty($SALE_DATA[$province_id]['sumBo']) ? $SALE_DATA[$province_id]['sumBo'] : 0;
                        $sumProvinceMoi = !empty($SALE_DATA[$province_id]['sumMoi']) ? $SALE_DATA[$province_id]['sumMoi'] : 0;
                    ?>
                    <td class="w-50 item_r item_b"><?php echo $sumProvinceBo; ?></td>
                    <td class="w-50 item_r item_b"><?php echo $sumProvinceMoi; ?></td>
                <?php endforeach; ?>
                    <td class="item_b item_r color_red" style="font-size: 1.1em"><?php echo $sumAllBo; ?></td>
                <td class="item_b item_r color_red" style="font-size: 1.1em"><?php echo $sumAllMoi; ?></td>
                <td class="item_b item_r color_red" style="font-size: 1.1em"><?php echo $sumAllBo + $sumAllMoi; ?></td>
            </tr>
        </tbody>
    </table>
</div>

<?php $i = 1; ?>
<h1>BẢO TRÌ</h1>
<div class="grid-view display_none">
    <table id="freezetablecolumns_employee" class="items hm_table freezetablecolumns_employee">
        <thead>
            <tr class="h_30">
                <th class="w-30">#</th>
                <th class="w-220">Nhân viên bảo trì</th>
                <th class="w-150">Tổng cộng</th>
                <?php foreach ($ARR_PROVINCE as $province_id): ?>
                    <th class="item_b w-100" colspan="2"><?php echo $PROVINCES[$province_id] ?></th>
                <?php endforeach; ?>
                <th colspan="3">Tổng cộng</th>
            </tr>
            <tr class="item_b h_30 item_c">
                <td></td>
                <td></td>
                <td class="item_c item_b">Bò/mối/tổng</td>
                <?php foreach ($ARR_PROVINCE as $province_id): ?>
                    <td class="w-50">Bò</td>
                    <td class="w-50">Mối</td>
                <?php endforeach; ?>
                <td class="w-50">Bò</td>
                <td class="w-50">Mối</td>
                <td class="w-50">Tổng</td>
            </tr>
        </thead>
        <tbody>
            <?php
                $sumAllBo = 0;
                $sumAllMoi = 0;
            ?>
            <?php foreach ($EMPLOYEE_MODEL as $employee_id => $mEmployee): ?>
                <?php
                    $sumBo = !empty($EMPLOYEE_DATA[$employee_id]['sumBo']) ? $EMPLOYEE_DATA[$employee_id]['sumBo'] : 0;
                    $sumMoi = !empty($EMPLOYEE_DATA[$employee_id]['sumMoi']) ? $EMPLOYEE_DATA[$employee_id]['sumMoi'] : 0;
                    $sumAllBo += $sumBo;
                    $sumAllMoi += $sumMoi;
                ?>
                <tr>
                    <td class="item_c"><?php echo $i++; ?></td>
                    <td class="item_b"><?php echo $mEmployee->first_name; ?></td>
                    <td class="item_c item_b"><?php echo $sumBo . '/' . $sumMoi . '/' . ($sumBo + $sumMoi) ?></td>
                    <?php foreach ($ARR_PROVINCE as $province_id): ?>
                        <?php
                            $binhBo = !empty($EMPLOYEE_DATA[$employee_id][$province_id][STORE_CARD_KH_BINH_BO]) ? $EMPLOYEE_DATA[$employee_id][$province_id][STORE_CARD_KH_BINH_BO] : '';
                            $binhMoi = !empty($EMPLOYEE_DATA[$employee_id][$province_id][STORE_CARD_KH_MOI]) ? $EMPLOYEE_DATA[$employee_id][$province_id][STORE_CARD_KH_MOI] : '';
                        ?>
                        <td class="w-50 item_r"><?php echo $binhBo; ?></td>
                        <td class="w-50 item_r"><?php echo $binhMoi; ?></td>
                    <?php endforeach; ?>
                    <td class="w-50 item_r"><?php echo $sumBo; ?></td>
                    <td class="w-50 item_r"><?php echo $sumMoi; ?></td>
                    <td class="w-50 item_r"><?php echo $sumBo + $sumMoi; ?></td>
                </tr>
            <?php endforeach; ?>
            <tr class="h_20">
                <td class="item_b item_c" colspan="3">Tổng cộng</td>
                <?php foreach ($ARR_PROVINCE as $province_id): ?>
                    <?php
                        $sumProvinceBo = !empty($EMPLOYEE_DATA[$province_id]['sumBo']) ? $EMPLOYEE_DATA[$province_id]['sumBo'] : 0;
                        $sumProvinceMoi = !empty($EMPLOYEE_DATA[$province_id]['sumMoi']) ? $EMPLOYEE_DATA[$province_id]['sumMoi'] : 0;
                    ?>
                    <td class="w-50 item_r item_b"><?php echo $sumProvinceBo; ?></td>
                    <td class="w-50 item_r item_b"><?php echo $sumProvinceMoi; ?></td>
                <?php endforeach; ?>
                <td class="item_b item_r color_red" style="font-size: 1.1em"><?php echo $sumAllBo; ?></td>
                <td class="item_b item_r color_red" style="font-size: 1.1em"><?php echo $sumAllMoi; ?></td>
                <td class="item_b item_r color_red" style="font-size: 1.1em"><?php echo $sumAllBo + $sumAllMoi; ?></td>
            </tr>
        </tbody>
    </table>
</div>

<!--sale-->
<script type="text/javascript">
    fnAddClassOddEven('items');
    $('.freezetablecolumns').each(function () {
        var id_table = $(this).attr('id');
        $('#' + id_table).freezeTableColumns({
            width:       <?php echo Users::WIDTH_freezetable;?>,   // required - bt 1100
            height:      <?php echo $freeHeightTable;?>,   // required
            numFrozen: 3,     // optional
            frozenWidth: 450,   // optional
            clearWidths: true  // optional
        });
    });
    $(window).load(function () {
        var index = 1;
        $('.freezetablecolumns').each(function () {
            if (index == 1)
                $(this).closest('div.grid-view').show();
            index++;
        });
        fnAddClassOddEven('items');
    });
</script>

<!--empolyee-->
<script type="text/javascript">
    $('.freezetablecolumns_employee').each(function () {
        var id_table = $(this).attr('id');
        $('#' + id_table).freezeTableColumns({
            width:       <?php echo Users::WIDTH_freezetable;?>,   // required - bt 1100
            height:      <?php echo $freeHeightTable_employee;?>,   // required
            numFrozen: 3,     // optional
            frozenWidth: 450,   // optional
            clearWidths: true  // optional
        });
    });
    $(window).load(function () {
        var index = 1;
        $('.freezetablecolumns_employee').each(function () {
            if (index == 1)
                $(this).closest('div.grid-view').show();
            index++;
        });
        fnAddClassOddEven('items');
    });
</script>