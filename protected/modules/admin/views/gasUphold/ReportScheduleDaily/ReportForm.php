<?php
$gridCss = Yii::app()->getAssetManager()->publish(Yii::getPathOfAlias('zii.widgets.assets')).'/gridview/styles.css';
Yii::app()->getClientScript()->registerCssFile($gridCss);
$MODEL_SALE_BT  = $data['MODEL_SALE_BT'];
$index          = 1;
$DATE_ARRAY     = isset($data['DATE_ARRAY']) ? $data['DATE_ARRAY'] : array();
$OUTPUT_SUM     = isset($data['OUTPUT_SUM']) ? $data['OUTPUT_SUM'] : array();
$OUTPUT         = isset($data['OUTPUT']) ? $data['OUTPUT'] : array();
?>
<div class="grid-view ">
<table class="items hm_table">
    <thead>
        <tr style="">
            <th class="w-20 ">#</th>
            <th class="w-150 ">Nhân viên bảo trì</th>
            <th class="w-50 ">Tổng</th>
            <?php foreach ($DATE_ARRAY as $days): ?>
                <th class="item_b"><?php echo substr($days, -2);?></th>
            <?php endforeach; ?>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($OUTPUT_SUM as $employee_id=> $sumEmployee): ?>
        <?php
            $nameEmployeeMaintain = $employee_id;
            if(isset($MODEL_SALE_BT[$employee_id])){
                $nameEmployeeMaintain = $MODEL_SALE_BT[$employee_id]->getFullName();
            }
            $sumRow = 0;
        ?>
        <tr>
            <td class="item_c"><?php echo $index++;?></td>
            <td class="item_b"><?php echo $nameEmployeeMaintain;?></td>
            <td class="item_c item_b"><?php echo $sumEmployee;?></td>
            <?php foreach ($DATE_ARRAY as $days): ?>
                <?php 
                    $value = isset($OUTPUT[$employee_id][$days]) ? $OUTPUT[$employee_id][$days] : '';
                ?>
                <td class="item_c"><?php echo $value;?></td>
            <?php endforeach; ?>
        </tr>
        <?php endforeach; ?>        
    </tbody>
</table>
</div>


<script type="text/javascript">
    fnAddClassOddEven('items');
</script>
