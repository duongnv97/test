<?php 
$aComment = $model->rUpholdReply;
$cmsFormater = new CmsFormatter();
?>

<?php if(count($aComment)):?>
    <h1>Lịch sử báo cáo</h1>
    <?php foreach($aComment as $iModel): ?>

    <?php $this->widget('application.components.widgets.XDetailView', array(
        'data' => $model,
        'attributes' => array(
            'group1'=>array(
                'ItemColumns' => 2,
                'itemCssClass' => array('odd xdetail_c2'),
                'attributes' => array(
                    array(
                        'label'=>$iModel->getUidLogin()."<br>".$cmsFormater->formatDateTime($iModel->created_date),
                        'type'=>'html',
                        'value'=> $iModel->getReplyContent(),
                    ),
                    array(
                        'label'=> "",
                        'type'=>'html',
                        'value'=> $cmsFormater->formatBreakTaskDailyFile($iModel),
                    ),
                ),
            ),
        ),
    )); ?>

    <?php endforeach; ?>
<div class="clr"></div>
<?php endif;?>