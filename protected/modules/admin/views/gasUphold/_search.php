<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

        <div class="row">
            <?php echo $form->labelEx($model,'customer_id'); ?>
            <?php echo $form->hiddenField($model,'customer_id', array('class'=>'')); ?>
            <?php
                    // 1. limit search kh của sale
                    $url = Yii::app()->createAbsoluteUrl('admin/ajax/search_user_by_code');
                    // widget auto complete search user customer and supplier
                    $aData = array(
                        'model'=>$model,
                        'field_customer_id'=>'customer_id',
                        'url'=> $url,
                        'name_relation_user'=>'rCustomer',
                        'ClassAdd' => 'w-400',
                    );
                    $this->widget('ext.SpaAutocompleteCustomer.SpaAutocompleteCustomer',
                        array('data'=>$aData));                                        
                    ?>
                <?php echo $form->error($model,'customer_id'); ?>
            </div>

            <div class="row">
                <?php echo $form->labelEx($model,'employee_id'); ?>
                <?php echo $form->hiddenField($model,'employee_id', array('class'=>'')); ?>
                <?php
                    // 1. limit search kh của sale
                    $url = Yii::app()->createAbsoluteUrl('admin/ajax/search_by_role', array('role'=>implode(",", GasUphold::$ROLE_EMPLOYEE_SEARCH_AUTOCOMPLETE)));
                    // widget auto complete search user customer and supplier
                    $aData = array(
                        'model'=>$model,
                        'field_customer_id'=>'employee_id',
                        'url'=> $url,
                        'name_relation_user'=>'rEmployee',
                        'field_autocomplete_name'=>'autocomplete_name_second',
                        'ClassAdd' => 'w-400',
        //                'ShowTableInfo' => 0,
                    );
                    $this->widget('ext.SpaAutocompleteCustomer.SpaAutocompleteCustomer',
                        array('data'=>$aData));                                        
                ?>

                <?php echo $form->error($model,'employee_id'); ?>
        </div>
    
        <div class="row">
                <?php echo $form->labelEx($model,'uid_login'); ?>
                <?php echo $form->hiddenField($model,'uid_login', array('class'=>'')); ?>
                <?php
                    // 1. limit search kh của sale
                    $url = Yii::app()->createAbsoluteUrl('admin/ajax/search_user_login');
                    // widget auto complete search user customer and supplier
                    $aData = array(
                        'model'=>$model,
                        'field_customer_id'=>'uid_login',
                        'url'=> $url,
                        'name_relation_user'=>'rUidLogin',
                        'field_autocomplete_name'=>'autocomplete_name_third',
                        'ClassAdd' => 'w-400',
        //                'ShowTableInfo' => 0,
                    );
                    $this->widget('ext.SpaAutocompleteCustomer.SpaAutocompleteCustomer',
                        array('data'=>$aData));                                        
                ?>

                <?php echo $form->error($model,'uid_login'); ?>
        </div>
	
        <div class="row more_col">
            <div class="col1">
                <?php echo $form->label($model,'code_no',array()); ?>
                <?php echo $form->textField($model,'code_no',array('class'=>'w-200','maxlength'=>50)); ?>
            </div>

            <div class="col2">
                <?php echo $form->labelEx($model,'type_uphold'); ?>
                <?php echo $form->dropDownList($model,'type_uphold', $model->getArrayUpholdType(),array('class'=>'w-200','empty'=>'Select')); ?>
                <?php echo $form->error($model,'type_uphold'); ?>
            </div>

            <div class="col3">
                <?php echo $form->labelEx($model,'level_type'); ?>
                <?php echo $form->dropDownList($model,'level_type', $model->getArrayLevel(),array('class'=>'w-200','empty'=>'Select')); ?>
                <?php echo $form->error($model,'level_type'); ?>
            </div>
        </div>

        <div class="row more_col">
            <div class="col1">
                <?php echo $form->label($model,'has_read',array()); ?>
		<?php echo $form->dropDownList($model,'has_read', $model->getArrayHasRead(),array('class'=>'w-200','empty'=>'Select')); ?>
            </div>
            <div class="col2">
                <?php echo $form->label($model,'contact_tel',array()); ?>
                <?php echo $form->textField($model,'contact_tel',array('class'=>'w-200','maxlength'=>100)); ?>
            </div>
            <div class="col3">
                <?php echo $form->label($model,'contact_person',array()); ?>
                <?php echo $form->textField($model,'contact_person',array('class'=>'w-200','maxlength'=>350)); ?>
            </div>
        </div>

        <div class="row more_col">
            <div class="col1">
                <?php echo $form->label($model,'status',array()); ?>
                <?php echo $form->dropDownList($model,'status', $model->getArrayStatus(),array('class'=>'w-200','empty'=>'Select')); ?>
            </div>
            <div class="col2">
                <?php echo $form->labelEx($model,'type_customer'); ?>
                <?php echo $form->dropDownList($model,'type_customer', CmsFormatter::$CUSTOMER_BO_MOI,array('class'=>'w-200', 'empty'=>'Select')); ?>
            </div>
        </div>

	<div class="row more_col">
            <div class="col1">
                <?php echo Yii::t('translation', $form->label($model,'date_from')); ?>
                <?php 
                    $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                        'model'=>$model,        
                        'attribute'=>'date_from',
                        'options'=>array(
                            'showAnim'=>'fold',
                            'dateFormat'=> MyFormat::$dateFormatSearch,       
                            'maxDate'=> '0',
                            'changeMonth' => true,
                            'changeYear' => true,
                            'showOn' => 'button',
                            'buttonImage'=> Yii::app()->theme->baseUrl.'/admin/images/icon_calendar_r.gif',
                            'buttonImageOnly'=> true,                                
                        ),        
                        'htmlOptions'=>array(
                            'class'=>'w-16',
                            'size'=>'16',
                            'style'=>'height:20px;float:left;',
                        ),
                    ));
                ?>     		
            </div>

            <div class="col2">
                <?php echo Yii::t('translation', $form->label($model,'date_to')); ?>
                <?php 
                    $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                        'model'=>$model,        
                        'attribute'=>'date_to',
                        'options'=>array(
                            'showAnim'=>'fold',
                            'dateFormat'=> MyFormat::$dateFormatSearch,       
                            'maxDate'=> '0',
                            'changeMonth' => true,
                            'changeYear' => true,
                            'showOn' => 'button',
                            'buttonImage'=> Yii::app()->theme->baseUrl.'/admin/images/icon_calendar_r.gif',
                            'buttonImageOnly'=> true,                                
                        ),        
                        'htmlOptions'=>array(
                            'class'=>'w-16',
                            'size'=>'16',
                            'style'=>'height:20px;float:left;',
                        ),
                    ));
                ?>    
            </div>
        </div>
    
        <?php include "_search_customer.php" ?>

	<div class="row buttons" style="padding-left: 159px;">
        <?php $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType'=>'submit',
            'label'=>'Search',
            'type'=>'null', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
            'size'=>'small', // null, 'large', 'small' or 'mini'
            //'htmlOptions' => array('style' => 'margin-bottom: 10px; float: right;'),
        )); ?>	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->