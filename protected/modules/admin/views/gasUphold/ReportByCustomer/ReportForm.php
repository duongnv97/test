<?php
$gridCss = Yii::app()->getAssetManager()->publish(Yii::getPathOfAlias('zii.widgets.assets')).'/gridview/styles.css';
Yii::app()->getClientScript()->registerCssFile($gridCss);
$MODEL_CUSTOMER = $data['MODEL_CUSTOMER'];
$MODEL_SALE_BT = $data['MODEL_SALE_BT'];
$ID_CUSTOMER_MAINTAIN = $data['ID_CUSTOMER_MAINTAIN'];
$OUTPUT = $data['OUTPUT']['OUTPUT_ALL'];
$GAS_REMAIN = $data['OUTPUT']['GAS_REMAIN'];
$index = 1;
?>
<div class="grid-view ">
<table class="items hm_table">
    <thead>
        <tr style="">
            <th class="w-20 ">#</th>
            <th class="w-50 ">Mã Kế Toán</th>
            <th class="w-200 ">Tên Khách Hàng</th>
            <th class="w-50">Loại</th>
            <th class="w-50 ">SL sự cố</th>
            <th class="w-50 ">Sản Lượng</th>
            <th class="w-150  ">Sale</th>
            <th class="w-150 ">NV Bảo Trì</th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($data['VALUE'] as $customer_id=>$value): ?>
        <?php
            $nameEmployeeMaintain = "";
            $nameSale = "";
            $mUser = $MODEL_CUSTOMER[$customer_id];
            $mSale = isset($MODEL_SALE_BT[$mUser->sale_id])?$MODEL_SALE_BT[$mUser->sale_id]:"";
            if(isset($ID_CUSTOMER_MAINTAIN[$customer_id]) && isset($MODEL_SALE_BT[$ID_CUSTOMER_MAINTAIN[$customer_id]]) ){
                $mUserBT = $MODEL_SALE_BT[$ID_CUSTOMER_MAINTAIN[$customer_id]];
                $nameEmployeeMaintain = $mUserBT->getFullName();
            }
            $code_account = $mUser->code_account;
            $nameCustomer = $mUser->code_bussiness."-".$mUser->getFullName();
            $typeCustomer = $mUser->getTypeCustomerText();
            
            if($mSale){
                $nameSale = $mSale->getFullName();
            }
            // for output
            $qty_b50 = isset($OUTPUT[$customer_id][MATERIAL_TYPE_BINHBO_50]) ? $OUTPUT[$customer_id][MATERIAL_TYPE_BINHBO_50]:0;
            $qty_b45 = isset($OUTPUT[$customer_id][MATERIAL_TYPE_BINHBO_45]) ? $OUTPUT[$customer_id][MATERIAL_TYPE_BINHBO_45]:0;
            $qty_b12 = isset($OUTPUT[$customer_id][MATERIAL_TYPE_BINH_12]) ? $OUTPUT[$customer_id][MATERIAL_TYPE_BINH_12]:0;
            $amount_50 = $qty_b50*CmsFormatter::$MATERIAL_VALUE_KG[MATERIAL_TYPE_BINHBO_50];
            $amount_45 = $qty_b45*CmsFormatter::$MATERIAL_VALUE_KG[MATERIAL_TYPE_BINHBO_45];
            $amount_12 = $qty_b12*CmsFormatter::$MATERIAL_VALUE_KG[MATERIAL_TYPE_BINH_12];
            $remain = isset($GAS_REMAIN[$customer_id])?$GAS_REMAIN[$customer_id]:0;
//            echo $qty_b50." = $qty_b45 = $qty_b12 = $remain";
            $output_real = $amount_50 + $amount_45 + $amount_12 - $remain;
        ?>
        <tr>
            <td class=""><?php echo $index++;?></td>
            <td class=""><?php echo $code_account;?></td>
            <td class=""><?php echo $nameCustomer;?></td>
            <td class="item_c"><?php echo $typeCustomer;?></td>
            <td class="item_c"><?php echo $value;?></td>
            <td class="item_r"><?php echo ActiveRecord::formatCurrencyRound($output_real);?></td>
            <td class=""><?php echo $nameSale;?></td>
            <td class=""><?php echo $nameEmployeeMaintain;?></td>
        </tr>
        <?php endforeach; ?>        
    </tbody>
</table>
</div>


<script type="text/javascript">
    fnAddClassOddEven('items');
</script>
