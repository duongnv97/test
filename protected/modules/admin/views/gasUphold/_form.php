<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'gas-uphold-form',
	'enableAjaxValidation'=>false,
        'htmlOptions' => array('enctype' => 'multipart/form-data'),
)); ?>

    <p class="note">Dòng có dấu <span class="required">*</span> là bắt buộc.</p>
    <?php echo MyFormat::BindNotifyMsg(); ?>
    <div class="row">
    <?php echo $form->labelEx($model,'customer_id'); ?>
    <?php echo $form->hiddenField($model,'customer_id', array('class'=>'')); ?>
    <?php
            // 1. limit search kh của sale
            $url = Yii::app()->createAbsoluteUrl('admin/ajax/search_user_by_code');
            // widget auto complete search user customer and supplier
            $aData = array(
                'model'=>$model,
                'field_customer_id'=>'customer_id',
                'url'=> $url,
                'name_relation_user'=>'rCustomer',
                'ClassAdd' => 'w-400',
                'fnSelectCustomer'=>'fnSelectCustomer',
            );
            $this->widget('ext.SpaAutocompleteCustomer.SpaAutocompleteCustomer',
                array('data'=>$aData));
            ?>
        <?php echo $form->error($model,'customer_id'); ?>
    </div>
    
    <div class="row">
        <?php echo $form->labelEx($model,'employee_id'); ?>
        <?php echo $form->hiddenField($model,'employee_id', array('class'=>'')); ?>
        <?php
            // 1. limit search kh của sale
            $url = Yii::app()->createAbsoluteUrl('admin/ajax/search_by_role', array('role'=> implode(',', GasUphold::$ROLE_EMPLOYEE_SEARCH_AUTOCOMPLETE)));
            // widget auto complete search user customer and supplier
            $aData = array(
                'model'=>$model,
                'field_customer_id'=>'employee_id',
                'url'=> $url,
                'name_relation_user'=>'rEmployee',
                'field_autocomplete_name'=>'autocomplete_name_second',
                'ClassAdd' => 'w-400',
//                'ShowTableInfo' => 0,
            );
            $this->widget('ext.SpaAutocompleteCustomer.SpaAutocompleteCustomer',
                array('data'=>$aData));                                        
        ?>

        <?php echo $form->error($model,'employee_id'); ?>
    </div>
        
    <div class="row">
        <?php echo $form->labelEx($model,'type_uphold'); ?>
        <?php echo $form->dropDownList($model,'type_uphold', $model->getArrayUpholdType(),array('class'=>'w-400','empty'=>'Select')); ?>
        <?php echo $form->error($model,'type_uphold'); ?>
    </div>
    
    <div class="row display_none">
        <?php echo $form->labelEx($model,'level_type'); ?>
        <?php echo $form->dropDownList($model,'level_type', $model->getArrayLevel(),array('class'=>'w-400','empty'=>'Select')); ?>
        <?php echo $form->error($model,'level_type'); ?>
    </div>
    
    
    <div class="row">
        <?php echo $form->labelEx($model,'content'); ?>
        <?php echo $form->textArea($model,'content',array('class'=>'w-400','rows'=>6, 'cols'=>50)); ?>
        <?php echo $form->error($model,'content'); ?>
    </div>
    <div class="row">
        <?php echo $form->labelEx($model,'contact_person'); ?>
        <?php echo $form->textField($model,'contact_person',array('class'=>'w-400','maxlength'=>350)); ?>
        <?php echo $form->error($model,'contact_person'); ?>
    </div>
    <div class="row">
        <?php echo $form->labelEx($model,'contact_tel'); ?>
        <?php echo $form->textField($model,'contact_tel',array('class'=>'phone_number_only w-400','maxlength'=>12)); ?>
        <?php echo $form->error($model,'contact_tel'); ?>
    </div>
    
    <div class="row">
        <?php // echo $form->labelEx($model,'status'); ?>
        <?php // echo $form->dropDownList($model,'status', $model->getArrayStatus(),array('class'=>'w-400','empty'=>'Select')); ?>
        <?php // echo $form->error($model,'status'); ?>
    </div>

    <div class="row buttons" style="padding-left: 141px;">
        <?php $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType'=>'submit',
            'label'=>$model->isNewRecord ? 'Create' :'Save',
            'type'=>'null', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
            'size'=>'small', // null, 'large', 'small' or 'mini'
            //'htmlOptions' => array('style' => 'margin-bottom: 10px; float: right;'),
        )); ?>	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->

<?php include "view_history.php"; ?>

<script>
    $(document).ready(function(){
        $('.form').find('button:submit').click(function(){
            $.blockUI({ overlayCSS: { backgroundColor: '#fff' } }); 
        });
    });
    
    function fnSelectCustomer(user_id, idField, idFieldCustomer){
        if(idFieldCustomer == '#GasUphold_customer_id' ){
            var customer_id = $('#GasUphold_customer_id').val();
            var next = '<?php echo GasCheck::getCurl();?>?ext_customer_id='+customer_id;
            next = updateQueryStringParameter(next, 'ext_customer_id', customer_id);
            $.blockUI({ overlayCSS: { backgroundColor: '#fff' } });
            window.location = next;
        }
    }
    
</script>