
<center><h1> Kiểm Tra đánh giá</h1></center>
<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'borrow-form',
	'enableAjaxValidation'=>false,
        'htmlOptions' => array('enctype' => 'multipart/form-data'),
));
$model->getGasUpholdById();
?>

    <p class="note">Dòng có dấu <span class="required">*</span> là bắt buộc.</p>
    <?php echo MyFormat::BindNotifyMsg(); ?>
    
            <div class="row">
                <?php echo $form->label($model,'Khách hàng')." :"; ?>
                
                <?php echo $model->getCustomer(); ?>
            </div>
            <div class="row">
                <?php echo $form->label($model,'Code No')." : "; ?>
                <?php echo $model->getWebCodeNo(); ?>
            </div>
    <div class="row">
                <?php echo $form->label($model,'Loại bảo trì')." : "; ?>
                <?php echo $model->getTextUpholdType(); ?>
            </div>
    <div class="row">
                <?php echo $form->label($model,'NV bảo trì')." : "; ?>
                <?php echo $model->getEmployee(); ?>
            </div>
    <div class="row">
                <?php echo $form->label($model,'Người tạo')." : "; ?>
                <?php echo $model->getUidLoginWeb(); ?>
            </div>
    <br>
    <div class="row">
                <?php echo $form->label($model,'Loại đánh giá')." : "; ?>
                <?php echo $model->getRatingType(); ?>
            </div>
    <div class="row">
                <?php echo $form->label($model,'Đánh giá')." : "; ?>
                <?php echo $model->rating_type_value; ?>
            </div>
    
    <br>
    <div class="row">
                <?php echo $form->label($model,'audit_status'); ?>
                <?php echo $form->dropDownList($model,'audit_status', $model->getArrayAuditStatus(),array('class'=>'w-200','empty'=>'Select')); ?>
                <?php echo $form->error($model,'audit_status'); ?>
            </div>
        <div class="row">
                <?php echo $form->label($model,'audit_note'); ?>
                <?php echo $form->textArea($model, 'audit_note', array('maxlength' => 300, 'rows' => 6, 'cols' => 50)); ?>
                <?php echo $form->error($model,'audit_note'); ?>
                </div>

    <div class="row buttons" style="padding-left: 141px;">
        <?php $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType'=>'submit',
            'label'=>$model->isNewRecord ? 'Create' :'Save',
            'type'=>'null', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
            'size'=>'small', // null, 'large', 'small' or 'mini'
            //'htmlOptions' => array('style' => 'margin-bottom: 10px; float: right;'),
        )); ?>	
        <input class='cancel_iframe' type='button' value='Cancel'>
    </div>

    <br>
    
<?php $this->endWidget(); ?>

</div><!-- form -->



<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/autoNumeric-master/autoNumeric.js"></script>
<script>
    $(document).ready(function(){
        $('.form').find('button:submit').click(function(){
            $.blockUI({ overlayCSS: { backgroundColor: '#fff' } }); 
        });
        fnInitInputCurrency();
        parent.$.fn.yiiGridView.update("borrow-grid");
    });
</script>
