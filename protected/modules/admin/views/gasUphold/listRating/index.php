<?php
$this->breadcrumbs=array(
    $this->pageTitle
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('gas-text-grid', {
                url : $(this).attr('action'),
		data: $(this).serialize()
	});
	return false;
});
");

Yii::app()->clientScript->registerScript('ajaxupdate', "
$('#gas-text-grid a.ajaxupdate').live('click', function() {
    $.fn.yiiGridView.update('gas-text-grid', {
        type: 'POST',
        url: $(this).attr('href'),
        success: function() {
            $.fn.yiiGridView.update('gas-text-grid');
        }
    });
    return false;
});
");
?>

<h1><?php echo $this->pageTitle;?></h1>

<?php if(!isset($_GET['TransactionHistoryRating'])): ?>
    <?php echo CHtml::link('Tìm Kiếm Nâng Cao','#',array('class'=>'search-button')); ?>
    <div class="search-form" style="display : none ;">
    <?php $this->renderPartial('listRating/_search',array(
            'model'=>$model,
    )); ?>
    </div><!-- search-form -->
<?php endif; ?>

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'gas-text-grid',
	'dataProvider'=>$model->searchForRating(),
        'afterAjaxUpdate'=>'function(id, data){ fnUpdateColorbox();}',
        'template'=>'{pager}{summary}{items}{pager}{summary}',
//    'itemsCssClass' => 'items custom_here',
//    'htmlOptions'=> array('class'=>'grid-view custom_here'),
        'pager' => array(
            'maxButtonCount'=>  CmsFormatter::$PAGE_MAX_BUTTON,
        ),
	'enableSorting' => false,
	//'filter'=>$model,
	'columns'=>array(
        array(
            'header' => 'S/N',
            'type' => 'raw',
            'value' => '$this->grid->dataProvider->pagination->currentPage * $this->grid->dataProvider->pagination->pageSize + ($row+1)',
            'headerHtmlOptions' => array('width' => '10px','style' => 'text-align:center;'),
            'htmlOptions' => array('style' => 'text-align:center;')
        ),
            array(
                'name'=>'code_no',
                'type'=>'raw',
                'htmlOptions' => array('style' => 'text-align:center;'),
                'value'=>'$data->getGasUpholdById().$data->getWebCodeNo()',
            ),
//		'type',
                array(
                    'header'=>'Khách hàng',
                    'type'=>'html',
                    'value'=>'$data->getCustomer()',
                ),
                
//                array(
//                    'name'=>'level_type',
//                    'value'=>'$data->getTextLevel()',
//                    'htmlOptions' => array('style' => 'text-align:center;')
//                ),
                array(
                    'header'=>'Loại + NV bảo trì',
                    'type'=> 'html',
                    'value'=>'$data->getTextUpholdType()."<br><br>".$data->getEmployee()',
                ),
                
                array(
                    'header'=>'Nội dung',
                    'type'=>'html',
                    'value'=>'$data->getContent()',
                ),
                array(
                    'name'=>'Loại đánh giá',
                    'type'=>'html',
                    'value'=>'$data->getRatingType()',
                ),                 
                array(
                    'header' => 'Đánh giá',
                    'type' => 'html',
                    'value' => '"<center>".$data->rating_type_value . "</center>"',
                    'htmlOptions' => array('style' => 'width:50px;')
                ),
                array(
                    'name'=>'rating_note',
                    'type'=>'raw',
                    'value'=>'$data->getRatingNote()',
                ),
                array(
                    'name'=>'audit_status',
                    'type'=>'html',
                    'value'=>'$data->getAuditStatus()',
                ),
                array(
                    'name'=>'audit_note',
                    'type'=>'html',
                    'value'=>'$data->getAuditNote()',
                ),
                array(
                    'header'=>'Người Tạo',
                    'value'=>'$data->getUidLoginWeb()',
                    'type' => 'raw',
                    'htmlOptions' => array('style' => 'text-align:center;'),
                ),
                array(
                    'header' => 'Ngày Tạo',
                    'type' => 'raw',
                    'value' => '$data->getCreatedInfo()',
                    'htmlOptions' => array('style' => 'width:50px;')
                ), 
		/*
		'content',
		'contact_person',
		'contact_tel',
		'status',
		'uid_login',
		'created_date',
		*/
		array(
                    'header' => 'Actions',
                    'class'=>'CButtonColumn',
                    'template'=> ControllerActionsName::createIndexButtonRoles($actions,array('view','update','delete','createAudit')),
                    'buttons'=>array(
                        'update'=>array(
                            'visible'=> '0',
                        ),
                        'delete'=>array(
                            'visible'=> '0',
                        ),
                        'view' => array(
                            'url'=>'Yii::app()->createAbsoluteUrl("admin/gasUphold/view/id/$data->uphold_id")',
                        ),
                        'createAudit'=>array(
                            'label'=>'Thêm mới kiểm tra',
                            'imageUrl'=>Yii::app()->theme->baseUrl . '/images/add1.png',
                            'options'=>array('class'=>'createAudit'),
                            'url'=>'Yii::app()->createAbsoluteUrl("admin/gasUphold/createAudit", array("id"=>$data->id) )',
                            'visible' => '1',

                        ),
                    ),
		),
	),
)); ?>

<script>
$(document).ready(function() {
    fnUpdateColorbox();
});



function fnUpdateColorbox(){
    $(".createAudit").colorbox({iframe:true,innerHeight:'550', innerWidth: '850',close: "<span title='close'>close</span>"});
}

</script>