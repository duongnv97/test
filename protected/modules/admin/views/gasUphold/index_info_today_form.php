<?php 
//    $date = date('Y-m-d');
//    $CountAll = GasUphold::countByStatusAndDate($date, '', GasUphold::READ_ALL);
//    $CountReadNo = GasUphold::countByStatusAndDate($date, '', GasUphold::READ_NO);
//    $CountStatusNew = GasUphold::countByStatusAndDate($date, GasUphold::STATUS_NEW, GasUphold::READ_ALL);
//    $CountStatusHandle = GasUphold::countByStatusAndDate($date, GasUphold::STATUS_HANLDE, GasUphold::READ_YES);
//    $CountStatusComplete = GasUphold::countByStatusAndDate($date, GasUphold::STATUS_COMPLETE, GasUphold::READ_YES);
//    $CountStatusDelay = GasUphold::countByStatusAndDate($date, GasUphold::STATUS_DELAY, GasUphold::READ_YES);
//    $CountStatusCancel = GasUphold::countByStatusAndDate($date, GasUphold::STATUS_CANCEL, GasUphold::READ_YES);
?>

<div class="grid-view w-300">
    <table class="items f_size_14">
        <tbody>
        <th class="item_c" colspan="4">Tóm tắt ngày <?php echo date('d/m/Y');?></th>
            <tr class="odd">
                <td class="item_b">Tổng cộng</td>
                <td><?php echo $CountAll;?></td>
                <td class="item_b">Chưa xác nhận</td>
                <td><?php echo $CountReadNo;?></td>
            </tr>
            <tr class="odd">
                <td class="item_b">Mới</td>
                <td><?php echo $CountStatusNew;?></td>
                <td class="item_b">Xử lý</td>
                <td><?php echo $CountStatusHandle;?></td>
            </tr>
            <tr class="odd">
                <td class="item_b">Hoàn thành</td>
                <td><?php echo $CountStatusComplete;?></td>
                <td class="item_b">Hoãn</td>
                <td><?php echo $CountStatusDelay;?></td>
            </tr>
            <tr class="odd">
                <td class="item_b">Yêu cầu chuyển</td>
                <td><?php echo $CountStatusCancel;?></td>
                <td class="item_b"></td>
                <td></td>
            </tr>
        </tbody>
    </table>
</div>