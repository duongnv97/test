<?php
$this->breadcrumbs=array(
    'Báo cáo bảo trì định kỳ',
);
?>

<h1>Báo cáo bảo trì định kỳ</h1>
<style>
    .ui-datepicker-trigger { float: left;}
</style>

<script>
$(document).ready(function(){
    $('.form').find('button:submit').click(function(){        
        $.blockUI({ overlayCSS: { backgroundColor: '#fff' } }); 
    });   
    fnAddClassOddEven('items');
});
</script>
<?php if(count($data)): ?>

<?php
$gridCss = Yii::app()->getAssetManager()->publish(Yii::getPathOfAlias('zii.widgets.assets')).'/gridview/styles.css';
Yii::app()->getClientScript()->registerCssFile($gridCss);
$MODEL_SALE_BT = $data['MODEL_SALE_BT'];
$index = 1;

$aTemp = array('Month');
foreach($model->getArrayUpholdType() as $typeId=>$typeText) {
    $aTemp[] = $typeText;
}
$aDataChart[] = $aTemp;
//$aDataChart[] = array('Tháng 1', 2, 4, 5, 4, 5);
//$aDataChart[] = array('Tháng 2', 1, 1, 1, 4, 5);
$tempChart = array();
?>


<div class="grid-view ">
<table class="items hm_table">
    <thead>
        <tr style="">
            <th class="w-20 ">#</th>
            <th class="w-200 ">Nhân viên bảo trì</th>
            <?php foreach ($model->getArrayScheduleType() as $typeId=>$typeText): ?>
                <th class="item_b"><?php echo $typeText;?></th>
            <?php endforeach; ?>
            <th class="">Tổng</th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($data['VALUE'] as $employee_id=> $aData): ?>
        <?php
            $nameEmployeeMaintain = "";
            if(isset($MODEL_SALE_BT[$employee_id])){
                $nameEmployeeMaintain .= $MODEL_SALE_BT[$employee_id]->getFullName();
            }
            $sumRow = 0;
        ?>
        <tr>
            <td class="item_c"><?php echo $index++;?></td>
            <td class="item_b"><?php echo $nameEmployeeMaintain;?></td>
            <?php foreach ($model->getArrayScheduleType() as $typeId=>$typeText): ?>
                <?php 
                    $value = isset($aData[$typeId]) ? $aData[$typeId] : "";
                    $sumRow += $value;
                ?>
                <td class="item_c"><?php echo ActiveRecord::formatCurrency($value);?></td>
            <?php endforeach; ?>
            <td class="item_c item_b"><?php echo ActiveRecord::formatCurrency($sumRow);?></td>
        </tr>
        <?php endforeach; ?>        
        
    </tbody>
</table>
</div>
    
<?php endif; ?>