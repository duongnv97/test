<?php
$gridCss = Yii::app()->getAssetManager()->publish(Yii::getPathOfAlias('zii.widgets.assets')).'/gridview/styles.css';
Yii::app()->getClientScript()->registerCssFile($gridCss);
$MODEL_SALE_BT = $data['MODEL_SALE_BT'];
$index = 1;
?>
<div class="grid-view ">
<table class="items hm_table">
    <thead>
        <tr style="">
            <th class="w-20 ">#</th>
            <th class="w-200 ">Nhân viên bảo trì</th>
            <?php foreach ($model->getArrayUpholdType() as $typeId=>$typeText): ?>
                <th class="item_b"><?php echo $typeText;?></th>
            <?php endforeach; ?>
            <th class="w-50 ">Tổng</th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($data['VALUE'] as $employee_id=> $aData): ?>
        <?php
            $nameEmployeeMaintain = "";
            if(isset($MODEL_SALE_BT[$employee_id])){
                $nameEmployeeMaintain .= $MODEL_SALE_BT[$employee_id]->getFullName();
            }
            $sumRow = 0;
        ?>
        <tr>
            <td class="item_c"><?php echo $index++;?></td>
            <td class="item_b"><?php echo $nameEmployeeMaintain;?></td>
            <?php foreach ($model->getArrayUpholdType() as $typeId=>$typeText): ?>
                <?php 
                    $value = isset($aData[$typeId]) ? $aData[$typeId] : '';
                    $sumRow += $value;
                ?>
                <td class="item_c"><?php echo $value;?></td>
            <?php endforeach; ?>
            <td class="item_c item_b"><?php echo $sumRow;?></td>
        </tr>
        <?php endforeach; ?>        
    </tbody>
</table>
</div>


<script type="text/javascript">
    fnAddClassOddEven('items');
</script>
