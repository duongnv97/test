<?php
$this->breadcrumbs=array(	
    $this->singleTitle,
);

$menus=array(
    array('label'=>"Tạo Mới $this->singleTitle", 'url'=>array('create')),
);

if($model->canExportExcel()):
    $menus[] = ['label'=> 'Xuất Excel Danh Sách Hiện Tại',
                'url'=>array('index', 'ExportExcel'=>1), 
                'htmlOptions'=>array('class'=>'export_excel ','label'=>'Xuất Excel'),
    ];
endif;

$this->menu= ControllerActionsName::createMenusRoles($menus, $actions);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('gas-uphold-grid', {
                url : $(this).attr('action'),
		data: $(this).serialize()
	});
	return false;
});
");

Yii::app()->clientScript->registerScript('ajaxupdate', "
$('#gas-uphold-grid a.ajaxupdate').live('click', function() {
    $.fn.yiiGridView.update('gas-uphold-grid', {
        type: 'POST',
        url: $(this).attr('href'),
        success: function() {
            $.fn.yiiGridView.update('gas-uphold-grid');
        }
    });
    return false;
});
");
?>
<?php echo MyFormat::BindNotifyMsg(); ?>
<h1>Danh Sách <?php echo $this->singleTitle;?></h1>
<?php include 'index_button.php'; ?>
<?php include 'index_info_today.php'; ?>

<?php echo CHtml::link('Tìm Kiếm Nâng Cao','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'gas-uphold-grid',
	'dataProvider'=>$model->search(),
        'afterAjaxUpdate'=>'function(id, data){ fnUpdateColorbox();}',
        'template'=>'{pager}{summary}{items}{pager}{summary}',
//    'itemsCssClass' => 'items custom_here',
//    'htmlOptions'=> array('class'=>'grid-view custom_here'),
        'pager' => array(
            'maxButtonCount'=>  CmsFormatter::$PAGE_MAX_BUTTON,
        ),
	'enableSorting' => false,
	//'filter'=>$model,
	'columns'=>array(
        array(
            'header' => 'S/N',
            'type' => 'raw',
            'value' => '$this->grid->dataProvider->pagination->currentPage * $this->grid->dataProvider->pagination->pageSize + ($row+1)',
            'headerHtmlOptions' => array('width' => '10px','style' => 'text-align:center;'),
            'htmlOptions' => array('style' => 'text-align:center;')
        ),
            array(
                'name'=>'code_no',
                'type'=>'raw',
                'htmlOptions' => array('style' => 'text-align:center;'),
                'value'=>'$data->getWebCodeNo()',
            ),
//		'type',
                array(
                    'name'=>'customer_id',
                    'type'=>'html',
                    'value'=>'"<strong>".$data->getCustomer()."</strong><br>".$data->getCustomer("address")',
                ),
                array(
                    'name'=>'sale_id',
                    'value'=>'$data->getSale()',
                ),
//                array(
//                    'name'=>'level_type',
//                    'value'=>'$data->getTextLevel()',
//                    'htmlOptions' => array('style' => 'text-align:center;')
//                ),
                array(
                    'name'=>'type_uphold',
                    'value'=>'$data->getTextUpholdType()',
                ),
                array(
                    'name'=>'contact_person',
                    'value'=>'$data->getContactPerson()',
                ),
                array(
                    'name'=>'contact_tel',
                    'value'=>'$data->getContactTel()',
                ),
                array(
                    'name'=>'employee_id',
                    'value'=>'$data->getEmployee()',
                ),
                array(
                    'name'=>'content',
                    'type'=>'html',
                    'value'=>'$data->getContent()',
                ),
                array(
                    'name'=>'has_read',
                    'value'=>'$data->getHasReadText()',
                    'htmlOptions' => array('style' => 'text-align:center;')
                ),
            
                array(
                    'name'=>'status',
                    'value'=>'$data->getTextStatus()',
                    'htmlOptions' => array('style' => 'text-align:center;')
                ),
                array(
                    'name'=>'uid_login',
                    'value'=>'$data->getUidLoginWeb()',
                    'type' => 'raw',
                    'htmlOptions' => array('style' => 'text-align:center;'),
                ),
                array(
                    'name' => 'created_date',
                    'type' => 'raw',
                    'value' => '$data->getCreatedInfo()',
                    'htmlOptions' => array('style' => 'width:50px;')
                ),     
		
		/*
		'content',
		'contact_person',
		'contact_tel',
		'status',
		'uid_login',
		'created_date',
		*/
		array(
                    'header' => 'Actions',
                    'class'=>'CButtonColumn',
                    'template'=> ControllerActionsName::createIndexButtonRoles($actions),
                    'buttons'=>array(
                        'update'=>array(
                            'visible'=> 'GasCheck::CanUpdateUphold($data)',
                        ),
                        'delete'=>array(
                            'visible'=> 'GasCheck::canDeleteData($data)',
                        ),
                    ),
		),
	),
)); ?>

<script>
$(document).ready(function() {
    fnUpdateColorbox();
});

function fnUpdateColorbox(){   
    fixTargetBlank();
    fnShowhighLightTr();
}
</script>