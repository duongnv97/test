<div class="search-form">    
    <div class="wide form">

        <?php
        $type = isset($_GET['type']) ? $_GET['type'] : NotifyReport::TAB_TYPE_GENERAL;
        $url  = Yii::app()->createAbsoluteUrl("admin/GasScheduleSmsHistory/report",['type' => $type]);
        $form = $this->beginWidget('CActiveForm', array(
            'action'=>$url,
            'method' => 'get',
        ));
        ?>

        <div class="row more_col">
            <div class="col1">
                <?php echo Yii::t('translation', $form->label($model, 'date_from', ['label' => 'Từ ngày'])); ?>
                <?php
                $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                    'model' => $model,
                    'attribute' => 'date_from',
                    'options' => array(
                        'showAnim' => 'fold',
                        'dateFormat' => MyFormat::$dateFormatSearch,
    //                            'minDate'=> '0',
    //                        'maxDate'=> '0',
                        'changeMonth' => true,
                        'changeYear' => true,
                        'showOn' => 'button',
                        'buttonImage' => Yii::app()->theme->baseUrl . '/admin/images/icon_calendar_r.gif',
                        'buttonImageOnly' => true,
                    ),
                    'htmlOptions' => array(
                        'class' => 'w-16 date_from',
                    ),
                ));
                ?>
            </div>
            <div class="col2">
                <?php echo Yii::t('translation', $form->label($model, 'date_to', ['label' => 'Đến ngày'])); ?>
                <?php
                $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                    'model' => $model,
                    'attribute' => 'date_to',
                    'options' => array(
                        'showAnim' => 'fold',
                        'dateFormat' => MyFormat::$dateFormatSearch,
    //                            'minDate'=> '0',
    //                        'maxDate'=> '0',
                        'changeMonth' => true,
                        'changeYear' => true,
                        'showOn' => 'button',
                        'buttonImage' => Yii::app()->theme->baseUrl . '/admin/images/icon_calendar_r.gif',
                        'buttonImageOnly' => true,
                    ),
                    'htmlOptions' => array(
                        'class' => 'w-16 date_to',
                    ),
                ));
                ?>
            </div>
        </div>

        <?php if($type == NotifyReport::TAB_TYPE_DETAIL): ?>
            <div class="row">
                <?php echo $form->labelEx($model,'role_id', ['label' => 'Chức vụ']); ?>
                <?php echo $form->dropdownList($model,'role_id', Roles::loadItems()); ?>
            </div>
        <?php endif; ?>

        <div class="row buttons" style="padding-left: 159px;">
            <?php
            $this->widget('bootstrap.widgets.TbButton', array(
                'buttonType' => 'submit',
                'label' => 'Search',
                'type' => 'null', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
                'size' => 'small', // null, 'large', 'small' or 'mini'
                    //'htmlOptions' => array('style' => 'margin-bottom: 10px; float: right;'),
            ));
            ?>
            <?php
            $this->widget('bootstrap.widgets.TbButton', array(
                'url' => Yii::app()->createAbsoluteUrl("admin/GasScheduleSmsHistory/report", ['toExcel' => 1, 'type' => $type]),
                'label' => 'Xuất Excel',
                'type' => 'null', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
                'size' => 'small', // null, 'large', 'small' or 'mini'
                'htmlOptions' => array('class' => 'btn_cancel'),
            ));
            ?>
        </div>

    <?php $this->endWidget(); ?>

    </div><!-- search-form -->
</div>