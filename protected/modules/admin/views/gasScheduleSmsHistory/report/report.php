<?php
$this->breadcrumbs=array(
	$this->singleTitle,
);

$menus=array(
            array('label'=>"Create $this->singleTitle", 'url' => array('create')),
);
$this->menu= ControllerActionsName::createMenusRoles($menus, $actions);

?>

<!--<h1>Danh Sách <?php echo $this->pluralTitle; ?></h1>-->
<?php include "index_button.php"; ?>
<?php include "_search.php"; ?>
<?php 
    $type = isset($_GET['type']) ? $_GET['type'] : NotifyReport::TAB_TYPE_GENERAL;
    if($type == NotifyReport::TAB_TYPE_GENERAL) {
        include "_general_tab.php";
    } elseif ($type == NotifyReport::TAB_TYPE_DETAIL) {
        include "_detail_tab.php";
    } elseif ($type == NotifyReport::TAB_TYPE_SMS) {
        include "_tab_type_sms.php";
    }
?>

<?php 
$gridCss = Yii::app()->getAssetManager()->publish(Yii::getPathOfAlias('zii.widgets.assets')) . '/gridview/styles.css';
Yii::app()->getClientScript()->registerCssFile($gridCss);
?>
<script>
$(document).ready(function() {
    fnUpdateColorbox();
    fnAddClassOddEven('items');
});

function fnUpdateColorbox(){   
    fixTargetBlank();
}
</script>
<script type="text/javascript"
        src="<?php echo Yii::app()->theme->baseUrl; ?>/js/scroll/jquery.freezetablecolumns.1.1.js"></script>
<script>
    $('.freezetablecolumns').each(function () {
        var id_table = $(this).attr('id');
        $('#' + id_table).freezeTableColumns({
            width:       <?php echo Users::WIDTH_freezetable;?>,   // required - bt 1100
            height:      500,   // required
            numFrozen: 3,     // optional
            frozenWidth: 310,   // optional
            clearWidths: true  // optional
        });
    });
    $(window).load(function () {
        var index = 1;
        $('.freezetablecolumns').each(function () {
            if (index == 1)
                $(this).closest('div.grid-view').show();
            index++;
        });
        fnAddClassOddEven('items');
    });
</script>