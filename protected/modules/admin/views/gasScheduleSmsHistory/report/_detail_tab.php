<?php
$date_from  = MyFormat::dateConverDmyToYmd($model->date_from, '-');
$date_to    = MyFormat::dateConverDmyToYmd($model->date_to, '-');
$mNotifyReport = new NotifyReport();
$aData      = $mNotifyReport->getDataReport($date_from, $date_to, $model);
?>

<?php if(!empty($aData['data'])): ?>
<?php
    $aUid = array_keys($aData['sum']);
    $aObjUser = Users::getArrObjectUserByRole('', $aUid);
?>
<div class="grid-view display_none">
    <table class="items hm_table freezetablecolumns" id="sms_table">
        <thead>
            <tr>
                <th class="w-20">#</th>
                <th class="w-150 ">Nhân viên</th>
                <th class="w-100">Tổng</th>
                <?php foreach ($aData['date_month'] as $days): ?>
                <th class="w-50"><?php echo substr($days, -2); ?></th>
                <?php endforeach; ?>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td></td><td></td>
                <td class="item_b item_r"><?php echo ActiveRecord::formatCurrency(array_sum($aData['sum'])); ?></td>
                <?php foreach ($aData['date_month'] as $days): ?>
                    <td class="item_b item_r"><?php echo isset($aData['sum_date'][$days]) ? ActiveRecord::formatCurrency($aData['sum_date'][$days]) : ''; ?></td>
                <?php endforeach; ?>
            </tr>
            <?php $i = 1; ?>
            <?php foreach ($aData['data'] as $user_id => $item): ?>
            <tr>
                <td><?php echo $i++; ?></td>
                <td><?php echo isset($aObjUser[$user_id]) ? $aObjUser[$user_id]->first_name : ''; ?></td>
                <td class="item_b item_r"><?php echo isset($aData['sum'][$user_id]) ? ActiveRecord::formatCurrency($aData['sum'][$user_id]) : ''; ?></td>
                <?php foreach ($aData['date_month'] as $d): ?>
                <?php $money = isset($aData['data'][$user_id][$d]) ? ActiveRecord::formatCurrency( $aData['data'][$user_id][$d] ) : ''; ?>
                <td class=" item_r"><?php echo $money; ?></td>
                <?php endforeach; ?>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
</div>
<?php endif; ?>
