<div class="form">
    <?php
        $LinkGeneral    = Yii::app()->createAbsoluteUrl('admin/GasScheduleSmsHistory/report', ['type'=> NotifyReport::TAB_TYPE_GENERAL]);
        $LinkDetail     = Yii::app()->createAbsoluteUrl('admin/GasScheduleSmsHistory/report', ['type'=> NotifyReport::TAB_TYPE_DETAIL]);
        $LinkTypeSms    = Yii::app()->createAbsoluteUrl('admin/GasScheduleSmsHistory/report', ['type'=> NotifyReport::TAB_TYPE_SMS]);
        $type = isset($_GET['type']) ? $_GET['type'] : NotifyReport::TAB_TYPE_GENERAL;
    ?>
    <h1>Thống kê <?php echo $this->pluralTitle; ?>
        <a class='btn_cancel f_size_14 <?php echo $type == NotifyReport::TAB_TYPE_GENERAL ? 'active':'';?>' href="<?php echo $LinkGeneral;?>">Tổng quát</a>
        <a class='btn_cancel f_size_14 <?php echo $type == NotifyReport::TAB_TYPE_DETAIL ? 'active':'';?>' href="<?php echo $LinkDetail;?>">Chi tiết</a>
        <a class='btn_cancel f_size_14 <?php echo $type == NotifyReport::TAB_TYPE_SMS ? 'active':'';?>' href="<?php echo $LinkTypeSms ;?>">Đối tượng gửi</a>
    </h1>
</div>