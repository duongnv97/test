<?php
$date_from = MyFormat::dateConverDmyToYmd($model->date_from, "-");
$date_to = MyFormat::dateConverDmyToYmd($model->date_to, "-");
$mNotifyReport = new NotifyReport();
$aData = $mNotifyReport->getDataReport($date_from, $date_to, $model);
$aType = $model->getArrayType();
?>
<?php if(!empty($aData['data'])): ?>
<div class="grid-view display_none">
    <table class="items hm_table freezetablecolumns" id="sms_table">
        <thead>
            <tr>
                <th class="w-20">#</th>
                <th class="w-150">Loại</th>
                <th class="w-100">Tổng</th>
                <?php foreach ($aData['date_month'] as $month): ?>
                    <th class="w-60">Tháng <?php echo $month; ?></th>
                <?php endforeach; ?>
            </tr>
        </thead>
        <tbody>
            <!--hàng tổng trên-->
            <tr>
                <td colspan="2" class="item_b item_r">Tổng</td>
                <td class="item_b item_r"><?php echo ActiveRecord::formatCurrency(array_sum($aData['sum'])); ?></td>
                <?php foreach ($aData['date_month'] as $month): ?>
                <td class="item_b item_r"><?php echo isset($aData['sum_row'][$month]) ? ActiveRecord::formatCurrency($aData['sum_row'][$month]) : 0 ?></td>
                <?php endforeach; ?>
            </tr>
            <!--End hàng tổng trên-->
            
            <?php $i = 1; ?>
            <?php foreach ($aData['data'] as $type => $item): ?>
            <tr>
                <td><?php echo $i++; ?></td>
                <td><?php echo isset($aType[$type]) ? $aType[$type] : "Type $type"; ?></td>
                <td class="item_b item_r"><?php echo isset($aData['sum'][$type]) ? ActiveRecord::formatCurrency($aData['sum'][$type]) : '&nbsp;'; ?></td>
                <?php foreach ($aData['date_month'] as $month): ?>
                <?php $money = isset($aData['data'][$type][$month]) ? ActiveRecord::formatCurrency( $aData['data'][$type][$month] ) : '&nbsp;'; ?>
                <td class=" item_r"><?php echo $money; ?></td>
                <?php endforeach; ?>
            </tr>
            <?php endforeach; ?>
            
            <!--hàng tổng dưới-->
            <tr>
                <td colspan="2" class="item_b item_r">Tổng</td>
                <td class="item_b item_r"><?php echo ActiveRecord::formatCurrency(array_sum($aData['sum'])); ?></td>
                <?php foreach ($aData['date_month'] as $month): ?>
                <td class="item_b item_r"><?php echo isset($aData['sum_row'][$month]) ? ActiveRecord::formatCurrency($aData['sum_row'][$month]) : 0 ?></td>
                <?php endforeach; ?>
            </tr>
            <!--End hàng tổng dưới-->
        </tbody>
    </table>
</div>
<?php endif; ?>