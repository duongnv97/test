<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'gas-schedule-sms-history-form',
	'enableAjaxValidation'=>false,
        'htmlOptions' => array('enctype' => 'multipart/form-data'),
)); ?>

    <p class="note">Dòng có dấu <span class="required">*</span> là bắt buộc.</p>
    <?php echo MyFormat::BindNotifyMsg(); ?>
    
        		
        
    <div class="row">
        <?php echo $form->labelEx($model,'network'); ?>
        <?php echo $form->textField($model,'network'); ?>
        <?php echo $form->error($model,'network'); ?>
    </div>
    <div class="row">
        <?php echo $form->labelEx($model,'uid_login'); ?>
        <?php echo $form->textField($model,'uid_login',array('size'=>11,'maxlength'=>11)); ?>
        <?php echo $form->error($model,'uid_login'); ?>
    </div>
    <div class="row">
        <?php echo $form->labelEx($model,'phone'); ?>
        <?php echo $form->textField($model,'phone',array('size'=>50,'maxlength'=>50)); ?>
        <?php echo $form->error($model,'phone'); ?>
    </div>
    <div class="row">
        <?php echo $form->labelEx($model,'user_id'); ?>
        <?php echo $form->textField($model,'user_id',array('size'=>11,'maxlength'=>11)); ?>
        <?php echo $form->error($model,'user_id'); ?>
    </div>
    <div class="row">
        <?php echo $form->labelEx($model,'role_id'); ?>
        <?php echo $form->textField($model,'role_id'); ?>
        <?php echo $form->error($model,'role_id'); ?>
    </div>
    <div class="row">
        <?php echo $form->labelEx($model,'username'); ?>
        <?php echo $form->textField($model,'username',array('size'=>60,'maxlength'=>100)); ?>
        <?php echo $form->error($model,'username'); ?>
    </div>
    <div class="row">
        <?php echo $form->labelEx($model,'type'); ?>
        <?php echo $form->textField($model,'type'); ?>
        <?php echo $form->error($model,'type'); ?>
    </div>
    <div class="row">
        <?php echo $form->labelEx($model,'obj_id'); ?>
        <?php echo $form->textField($model,'obj_id',array('size'=>11,'maxlength'=>11)); ?>
        <?php echo $form->error($model,'obj_id'); ?>
    </div>
    <div class="row">
        <?php echo $form->labelEx($model,'title'); ?>
        <?php echo $form->textArea($model,'title',array('rows'=>6, 'cols'=>50)); ?>
        <?php echo $form->error($model,'title'); ?>
    </div>
    <div class="row">
        <?php echo $form->labelEx($model,'json_var'); ?>
        <?php echo $form->textArea($model,'json_var',array('rows'=>6, 'cols'=>50)); ?>
        <?php echo $form->error($model,'json_var'); ?>
    </div>
    <div class="row">
        <?php echo $form->labelEx($model,'count_run'); ?>
        <?php echo $form->textField($model,'count_run',array('size'=>11,'maxlength'=>11)); ?>
        <?php echo $form->error($model,'count_run'); ?>
    </div>
    <div class="row">
        <?php echo $form->labelEx($model,'time_send'); ?>
        <?php echo $form->textField($model,'time_send'); ?>
        <?php echo $form->error($model,'time_send'); ?>
    </div>
    <div class="row">
        <?php echo $form->labelEx($model,'created_date'); ?>
        <?php echo $form->textField($model,'created_date'); ?>
        <?php echo $form->error($model,'created_date'); ?>
    </div>
    <div class="row">
        <?php echo $form->labelEx($model,'created_date_bigint'); ?>
        <?php echo $form->textField($model,'created_date_bigint',array('size'=>12,'maxlength'=>12)); ?>
        <?php echo $form->error($model,'created_date_bigint'); ?>
    </div>
    <div class="row">
        <?php echo $form->labelEx($model,'created_date_on_history'); ?>
        <?php echo $form->textField($model,'created_date_on_history'); ?>
        <?php echo $form->error($model,'created_date_on_history'); ?>
    </div>
    <div class="row">
        <?php echo $form->labelEx($model,'content_type'); ?>
        <?php echo $form->textField($model,'content_type'); ?>
        <?php echo $form->error($model,'content_type'); ?>
    </div>
	<div class="row buttons" style="padding-left: 141px;">
		        <?php $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType'=>'submit',
            'label'=>$model->isNewRecord ? 'Create' :'Save',
            'type'=>'null', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
            'size'=>'small', // null, 'large', 'small' or 'mini'
            //'htmlOptions' => array('style' => 'margin-bottom: 10px; float: right;'),
        )); ?>	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->
<script>
    $(document).ready(function(){
        $('.form').find('button:submit').click(function(){
            $.blockUI({ overlayCSS: { backgroundColor: '#fff' } }); 
        });
        
    });
</script>