<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>GasCheck::getCurl(),//Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>
        <div class="row">
        <?php echo $form->labelEx($model,'employee_maintain_id'); ?>
        <?php echo $form->hiddenField($model,'employee_maintain_id', array('class'=>'')); ?>
        <?php
                // 1. limit search kh của sale
                $url = Yii::app()->createAbsoluteUrl('admin/ajax/search_by_role', array('role'=> implode(',', Sell::$ROLE_EMPLOYEE_SEARCH_AUTOCOMPLETE)));
                // widget auto complete search user customer and supplier
                $aData = array(
                    'model'=>$model,
                    'field_customer_id'=>'employee_maintain_id',
                    'url'=> $url,
                    'name_relation_user'=>'rEmployeeMaintain',
                    'ClassAdd' => 'w-400',
                    'field_autocomplete_name' => 'autocomplete_name_2',
                    'placeholder'=>'Nhập mã NV hoặc tên',
                );
                $this->widget('ext.SpaAutocompleteCustomer.SpaAutocompleteCustomer',
                    array('data'=>$aData));
                ?>
            <?php echo $form->error($model,'employee_maintain_id'); ?>
        </div>
        
        <div class="row more_col">
            <div class="col1" style="width: 720px;">
                <?php echo $form->labelEx($model,'searchGas'); ?>
                <?php // $dataCat = CHtml::listData(GasMaterials::getCatLevel2(GasMaterials::$MATERIAL_GAS),'id','name','group'); ?>
                <?php // echo $form->dropDownList($model,'materials_id_sell', $dataCat,array('class'=>'category_ajax', 'style'=>'width:385px;','empty'=>'Select')); ?>		
                <?php echo $form->hiddenField($model,'searchGas'); ?>		
                <?php 
                    // widget auto complete search material
                    $aData = array(
                        'model'=>$model,
                        'field_material_id'=>'searchGas',
                        'name_relation_material'=>'rSearchGas',
                        'field_autocomplete_name'=>'autocomplete_gas',
                    );
                    $this->widget('ext.GasAutocompleteMaterial.GasAutocompleteMaterial',
                        array('data'=>$aData));                                        
                ?>             
                <?php echo $form->error($model,'searchGas'); ?>
            </div>
            <div class="col2">
                <?php echo $form->labelEx($model,'high_price'); ?>
                <?php echo $form->dropDownList($model,'high_price', ActiveRecord::getYesNo(),array('class'=>'w-150','empty'=>'Select')); ?>
            </div>
        </div>
    
        <div class="row">
        <?php echo $form->labelEx($model,'ptttUid'); ?>
        <?php echo $form->hiddenField($model,'ptttUid', array('class'=>'')); ?>
        <?php
                // 1. limit search kh của sale
                $url = Yii::app()->createAbsoluteUrl('admin/ajax/search_by_role', array('role'=> implode(',', Sell::$ROLE_EMPLOYEE_SEARCH_AUTOCOMPLETE)));
                // widget auto complete search user customer and supplier
                $aData = array(
                    'model'=>$model,
                    'field_customer_id'=>'ptttUid',
                    'url'=> $url,
                    'name_relation_user'=>'rEmployeeMaintain',
                    'ClassAdd' => 'w-400',
                    'field_autocomplete_name' => 'autocomplete_name_5',
                    'placeholder'=>'Nhập mã NV hoặc tên PTTT',
                );
                $this->widget('ext.SpaAutocompleteCustomer.SpaAutocompleteCustomer',
                    array('data'=>$aData));
                ?>
            <?php echo $form->error($model,'ptttUid'); ?>
        </div>
    
        <div class="row more_col">
            <div class="col1">
                <?php echo $form->labelEx($model,'amount_bu_vo'); ?>
                <?php echo $form->dropDownList($model,'amount_bu_vo', ActiveRecord::getYesNo(),array('class'=>'w-200','empty'=>'Select')); ?>
            </div>
            <div class="col2">
                <?php echo $form->labelEx($model,'pttt_code'); ?>
                <?php echo $form->dropDownList($model,'pttt_code', ActiveRecord::getYesNo(),array('class'=>'w-200','empty'=>'Select')); ?>
            </div>
            <div class="col3">
                <?php echo $form->label($model, 'phone', array()); ?>
                <?php echo $form->textField($model, 'phone', array('class' => 'w-200', 'maxlength' => 20)); ?>
            </div>
        </div>
        
        <div class="row more_col">
            <div class="col1">
                <?php 
                    $aDataSale = Users::getArrObjectUserByRole([ROLE_TELESALE, ROLE_CALL_CENTER], []);
                    $aDataSale = CHtml::listData($aDataSale, 'id', 'first_name');
                ?>
                <?php echo $form->labelEx($model,'sale_id', ['label'=>'Telesale']); ?>
                <?php echo $form->dropDownList($model,'sale_id', $aDataSale, array('class'=>'w-200','empty'=>'Select')); ?>
            </div>
            <div class="col2">
                <?php echo $form->labelEx($model,'promotion_amount', ['label'=>'Chuyên viên giảm']); ?>
                <?php echo $form->dropDownList($model,'promotion_amount', ActiveRecord::getYesNo(),array('class'=>'w-200','empty'=>'Select')); ?>
            </div>
            <div class="col3">
                <?php echo $form->labelEx($model,'call_id', ['label' => 'Cuộc gọi']); ?>
                <?php echo $form->dropDownList($model,'call_id', CmsFormatter::$yesNoCharFormat,array('class'=>'w-200','empty'=>'Select')); ?>
            </div>
        </div>
        <div class="row more_col">
            <div class="col1">
                <?php echo $form->labelEx($model,'telesaleCheck', ['label'=>'BQV Telesale']); ?>
                <?php $mTelesale = new Telesale(); echo $form->dropDownList($model,'telesaleCheck', $mTelesale->getArrayTypeBqv(),array('class'=>'w-200','empty'=>'Select')); ?>
            </div>
            <div class="col2"></div>
            <div class="col3"></div>
        </div>

        <div class="row more_col">
            <div class="col1">
                <?php echo $form->labelEx($model,'status'); ?>
                <?php echo $form->dropDownList($model,'status', $model->getArrayStatus(),array('class'=>'w-200','empty'=>'Select')); ?>
            </div>
            <div class="col2">
                <?php echo $form->labelEx($model,'status_cancel'); ?>
                <?php echo $form->dropDownList($model,'status_cancel', $model->getArrayStatusCancel(),array('class'=>'w-200','empty'=>'Select')); ?>
            </div>
            <div class="col3">
                <?php echo $form->labelEx($model,'type_customer'); ?>
                <?php echo $form->dropDownList($model,'type_customer', CmsFormatter::$CUSTOMER_BO_MOI,array('class'=>'w-200','empty'=>'Select')); ?>
            </div>
        </div>
    
        <div class="row more_col">
            <div class="col1 w-400">
                <?php echo $form->labelEx($model,'province_id'); ?>
                <div class="fix-label">
                    <?php
                       $this->widget('ext.multiselect.JMultiSelect',array(
                             'model'=>$model,
                             'attribute'=>'province_id',
                             'data'=> GasProvince::getArrAll(),
                             // additional javascript options for the MultiSelect plugin
                            'options'=>array('selectedList' => 30,),
                             // additional style
                             'htmlOptions'=>array('class' => 'w-200'),
                       ));    
                   ?>
                </div>
            </div>
            <div class="col2">
                <?php echo $form->labelEx($model,'source'); ?>
                <?php echo $form->dropDownList($model,'source', $model->getArraySource(),array('class'=>'w-200','empty'=>'Select')); ?>
            </div>
            <div class="col3">
                <?php echo $form->labelEx($model,'customer_new', ['label'=>'Kh cũ']); ?>
                <?php echo $form->dropDownList($model,'customer_new', $model->getArrayCustomerNewOld(),array('class'=>'w-150','empty'=>'Select')); ?>
            </div>
        </div>
    
        <div class="row">
        <?php echo $form->labelEx($model,'uid_login'); ?>
        <?php echo $form->hiddenField($model,'uid_login', array('class'=>'')); ?>
        <?php
                // 1. limit search kh của sale
                $url = Yii::app()->createAbsoluteUrl('admin/ajax/search_by_role', array('role'=> implode(',', $model->getRoleAllowCreate())));
                // widget auto complete search user customer and supplier
                $aData = array(
                    'model'=>$model,
                    'field_customer_id'=>'uid_login',
                    'url'=> $url,
                    'name_relation_user'=>'rUidLogin',
                    'ClassAdd' => 'w-400',
                    'field_autocomplete_name' => 'amount_discount',
                    'placeholder'=>'Nhập mã NV hoặc tên',
                );
                $this->widget('ext.SpaAutocompleteCustomer.SpaAutocompleteCustomer',
                    array('data'=>$aData));
                ?>
            <?php echo $form->error($model,'uid_login'); ?>
        </div>
        
    <?php if(Yii::app()->user->role_id!=ROLE_SUB_USER_AGENT):?>
        <div class="row">
        <?php echo $form->labelEx($model,'agent_id'); ?>
        <?php echo $form->hiddenField($model,'agent_id', array('class'=>'')); ?>
        <?php
                // 1. limit search kh của sale
                $url = Yii::app()->createAbsoluteUrl('admin/ajax/search_by_role', array('role'=> ROLE_AGENT));
                // widget auto complete search user customer and supplier
                $aData = array(
                    'model'=>$model,
                    'field_customer_id'=>'agent_id',
                    'url'=> $url,
                    'name_relation_user'=>'rAgent',
                    'ClassAdd' => 'w-400',
                    'field_autocomplete_name' => 'autocomplete_name_3',
                    'placeholder'=>'Nhập mã hoặc tên đại lý',
                );
                $this->widget('ext.SpaAutocompleteCustomer.SpaAutocompleteCustomer',
                    array('data'=>$aData));
                ?>
            <?php echo $form->error($model,'agent_id'); ?>
        </div>
    <?php endif; ?>
    <div class="row more_col">
        <div class="col1">
            <?php echo $form->label($model, 'code_no', array()); ?>
            <?php echo $form->textField($model, 'code_no', array('class' => 'w-200', 'maxlength' => 20)); ?>
        </div>
        <div class="col2">
            <?php echo $form->labelEx($model,'order_type'); ?>
            <?php echo $form->dropDownList($model,'order_type', $model->getArrayOrderType(),array('class'=>'w-200','empty'=>'Select')); ?>
        </div>
        <div class="col3">
            <?php $aPageSize = GasConst::getPageSize(); 
                unset($aPageSize[100]);
                unset($aPageSize[200]);
            ?>
            <?php echo $form->labelEx($model,'pageSize'); ?>
            <?php echo $form->dropDownList($model,'pageSize', $aPageSize,array('class'=>'w-200','empty'=>'Select')); ?>
        </div>
        <div class="col3">
            <?php // echo $form->labelEx($model,'uid_login'); ?>
            <?php // echo $form->dropDownList($model,'uid_login', Users::getSelectByRoleForAgent($model->agent_id, ONE_AGENT_ACCOUNTING, "", array('status'=>1)),array('class'=>'w-200','empty'=>'Select')); ?>
        </div>
    </div>
    
    <div class="row more_col">
        <div class="col1">
                <?php echo $form->label($model,'date_from'); ?>
                <?php 
                    $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                        'model'=>$model,        
                        'attribute'=>'date_from',
                        'options'=>array(
                            'showAnim'=>'fold',
                            'dateFormat'=> MyFormat::$dateFormatSearch,
//                            'minDate'=> '0',
                            'maxDate'=> '0',
                            'changeMonth' => true,
                            'changeYear' => true,
                            'showOn' => 'button',
                            'buttonImage'=> Yii::app()->theme->baseUrl.'/admin/images/icon_calendar_r.gif',
                            'buttonImageOnly'=> true,                                
                        ),        
                        'htmlOptions'=>array(
                            'class'=>'w-200 date_from',
                            'size'=>'16',
                            'style'=>'float:left;',                               
                        ),
                    ));
                ?>     		
        </div>
        <div class="col2">
                <?php echo $form->label($model,'date_to'); ?>
                <?php 
                    $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                        'model'=>$model,        
                        'attribute'=>'date_to',
                        'options'=>array(
                            'showAnim'=>'fold',
                            'dateFormat'=> MyFormat::$dateFormatSearch,
//                            'minDate'=> '0',
                            'maxDate'=> '0',
                            'changeMonth' => true,
                            'changeYear' => true,
                            'showOn' => 'button',
                            'buttonImage'=> Yii::app()->theme->baseUrl.'/admin/images/icon_calendar_r.gif',
                            'buttonImageOnly'=> true,                                
                        ),        
                        'htmlOptions'=>array(
                            'class'=>'w-150 DateTo',
                            'size'=>'16',
                            'style'=>'float:left;',
                        ),
                    ));
                ?>     		
        </div>
        <div class="col3">
            <?php echo $form->label($model, 'promotion_id', ['label'=>'Mã khuyến mãi']); ?>
            <?php echo $form->textField($model, 'promotion_id', array('class' => 'w-190', 'placeholder' => 'vd: 24H750')); ?>
        </div>
    </div>
    
    <div class="row">
        <?php echo $form->labelEx($model,'customer_id'); ?>
        <?php echo $form->hiddenField($model,'customer_id', array('class'=>'')); ?>
        <?php
                // 1. limit search kh của sale
                $url = Yii::app()->createAbsoluteUrl('admin/ajax/searchHgd');
                // widget auto complete search user customer and supplier
                $aData = array(
                    'model'=>$model,
                    'field_customer_id'=>'customer_id',
                    'url'=> $url,
                    'name_relation_user'=>'rCustomer',
                    'ClassAdd' => 'w-400',
    //                'fnSelectCustomer'=>'fnSelectCustomer',
                );
                $this->widget('ext.SpaAutocompleteCustomer.SpaAutocompleteCustomer',
                    array('data'=>$aData));
                ?>
            <?php echo $form->error($model,'customer_id'); ?>
    </div>
    
    <div class="row">
        <?php echo $form->labelEx($model,'order_type_status'); ?>
        <?php echo $form->dropDownList($model,'order_type_status', $model->getArrayAudit(),array('class'=>'w-200','empty'=>'Select')); ?>
    </div>
    <div class="row">
        <?php echo $form->labelEx($model,'BQV Chia Sẻ'); ?>
        <?php echo $form->hiddenField($model,'target_v1', array('class'=>'')); ?>
        <?php
            // 1. limit search kh của sale
            $url = Yii::app()->createAbsoluteUrl('admin/ajax/search_by_role', array('role'=> implode(',', Sell::$ROLE_EMPLOYEE_SEARCH_AUTOCOMPLETE)));
            // widget auto complete search user customer and supplier
            $aData = array(
                'model'=>$model,
                'field_customer_id'=>'target_v1',
                'url'=> $url,
                'name_relation_user'=>'rEmployeeMaintain',
                'ClassAdd' => 'w-400',
                'field_autocomplete_name' => 'autocomplete_target_v1',
                'placeholder'=>'Nhập mã NV hoặc tên PTTT',
            );
            $this->widget('ext.SpaAutocompleteCustomer.SpaAutocompleteCustomer',
                array('data'=>$aData));
            ?>
        <?php echo $form->error($model,'target_v1'); ?>
    </div>
    <div class="row">
        <?php echo $form->labelEx($model,'BQV Lần 2'); ?>
        <?php echo $form->hiddenField($model,'target_v2', array('class'=>'')); ?>
        <?php
            // 1. limit search kh của sale
            $url = Yii::app()->createAbsoluteUrl('admin/ajax/search_by_role', array('role'=> implode(',', Sell::$ROLE_EMPLOYEE_SEARCH_AUTOCOMPLETE)));
            // widget auto complete search user customer and supplier
            $aData = array(
                'model'=>$model,
                'field_customer_id'=>'target_v2',
                'url'=> $url,
                'name_relation_user'=>'rEmployeeMaintain',
                'ClassAdd' => 'w-400',
                'field_autocomplete_name' => 'autocomplete_target_v2',
                'placeholder'=>'Nhập mã NV hoặc tên PTTT',
            );
            $this->widget('ext.SpaAutocompleteCustomer.SpaAutocompleteCustomer',
                array('data'=>$aData));
            ?>
        <?php echo $form->error($model,'target_v2'); ?>
    </div>
    <div class="row">
        <?php echo $form->labelEx($model,'Loại BQV'); ?>
        <?php echo $form->dropDownList($model,'type_target', $model->getArrayTypeCcs(),array('class'=>'w-150','empty'=>'Select')); ?>
    </div>
    
	<div class="row buttons" style="padding-left: 159px;">
            <?php $this->widget('bootstrap.widgets.TbButton', array(
                'buttonType'=>'submit',
                'label'=>'Search',
                'type'=>'null', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
                'size'=>'small', // null, 'large', 'small' or 'mini'
                //'htmlOptions' => array('style' => 'margin-bottom: 10px; float: right;'),
            )); ?>
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a class='btn_cancel btnReset'>Reset</a>
        </div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->

<script>
    $(function(){
        $('.date_from').change(function(){
            var div = $(this).closest('.row');
            div.find('.DateTo').val($(this).val());
        });
        $('.btnReset').click(function(){
            var div = $(this).closest('form');
            div.find('input').val('');
            div.find('.remove_row_item').trigger('click');
        });
    });
</script>