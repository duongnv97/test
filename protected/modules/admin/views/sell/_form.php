<div class="form">
<?php
    $allowChangeCustomer = true;
    $cRole  = MyFormat::getCurrentRoleId();
    $cUid   = MyFormat::getCurrentUid();
    
    if (!$model->isNewRecord && in_array($cRole, $model->getRoleAllowCreate()) && $cUid != GasConst::UID_NGOC_PT) {
        $allowChangeCustomer = false;
    }
//    if ($model->isNewRecord && isset($_GET['call_uuid'])){
//        $allowChangeCustomer = false;
//    }
    
$form=$this->beginWidget('CActiveForm', array(
	'id'=>'sell-form',
	'enableAjaxValidation'=>false,
        'htmlOptions' => array('enctype' => 'multipart/form-data'),
)); ?>
    <?php echo $form->errorSummary($model); ?>
    <p class="note">Dòng có dấu <span class="required">*</span> là bắt buộc.</p>
    <?php echo MyFormat::BindNotifyMsg(); ?>

    <div class="row buttons" style="padding-left:250px;">
        <?php $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType'=>'submit',
            'label'=>$model->isNewRecord ? 'Create' :'Save',
            'type'=>'null', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
            'size'=>'small', // null, 'large', 'small' or 'mini'
            //'htmlOptions' => array('style' => 'margin-bottom: 10px; float: right;'),
        )); ?>	
    </div>
    <div class="row">
        <?php echo $form->labelEx($model,'status'); ?>
        <?php echo $model->getStatus(); ?>
    </div>
    
    <?php if($cRole == ROLE_ADMIN): ?>
    <div class="row float_l f_size_15">
        <label>&nbsp;</label>
        <?php echo $form->checkBox($model,'searchKm',array('class'=>'float_l')); ?>
        <?php echo $form->labelEx($model,'searchKm',array('class'=>'checkbox_one_label w-300 l_padding_10', 'style'=>'padding-top:3px;', 'label'=>'Xóa khuyến mãi của đơn hàng')); ?>
    </div>          
    <div class="clr"></div>
    <?php endif; ?>
    <?php include "_form_more.php"; ?>
    
    <div class="clr">
        <div style="width: 60%; float: left;"><?php include '_form_col1.php'; ?></div>
        <div style="width: 40%; float: left;">
            <div id='elementAgent'></div>
        <?php if(!empty($model->agent_id)): ?>
            <?php include '_form_col2.php'; ?>
        <?php endif; ?>   
            </div>
        </div>
    </div>
    <div class="clr"></div>
    <?php // include "_form_detail.php"; ?>

<?php $this->endWidget(); ?>
    <div class="clr"></div>
    
</div><!-- form -->
<?php 
    if($cRole == ROLE_ADMIN){
        echo "SaleName: {$model->getSale()}";
        echo '<pre>';
        print_r($model->getAttributes());
        echo '</pre>';
    }
?>

<script>
    $(document).ready(function(){
        $('.form').find('button:submit').click(function(){
            $.blockUI({ overlayCSS: { backgroundColor: '#fff' } }); 
        });
        $(".IframeCreateCustomer").colorbox({iframe:true,innerHeight:'1000', innerWidth: '700',close: "<span title='close'>close</span>"});
        amountDiscountChange();
        hideCustomerInput();
        <?php if($model->isNewRecord && isset($_GET['transaction_id'])):?>
            var url_ = '<?php echo Yii::app()->createAbsoluteUrl('admin/ajaxForUpdate/removeRowOrderGas24h', ['transaction_history_id'=>$_GET['transaction_id']]);?>';
            putConfirmAppGas24h(url_);
        <?php endif;?>
        $('.RemoveDeliveryTimer').click(function(){
            $(this).closest('.row').find('.InputDeliveryTimer').val('');
        });

    });
    
    function fnSelectCustomer(user_id, idField, idFieldCustomer){
        if(idFieldCustomer == '#Sell_customer_id' ){
            var customer_id = $('#Sell_customer_id').val();
            var next = '<?php echo  Yii::app()->createAbsoluteUrl('admin/ajax/IframeUpdateCustomerHgd');?>';
            next = updateQueryStringParameter(next, 'customer_id', customer_id);
            console.log(next);
            $('.IframeCreateCustomer').attr('href', next);
//            $.blockUI({ overlayCSS: { backgroundColor: '#fff' } });
//            window.location = next;

//          thực hiện hiển thị điểm hiện tại của nhân viên
            $.ajax({
                url:'<?php echo Yii::app()->createAbsoluteUrl('admin/sell/create',['AJAX_MODULE'=>1]) ?>',
                type: 'get',
    //            dataType: 'json',
                data: {idCustomer:user_id},
                success: function(data){
                    var jsonReturn = JSON.parse(data);
                    $('#titleReward').html(jsonReturn['title']);
                }
            });
//            thực hiện load danh sách rewardApp của KH
            $.ajax({
                url:'<?php echo Yii::app()->createAbsoluteUrl('admin/sell/create',['AJAX_REWARD_APP'=>1]) ?>',
                type: 'get',
    //            dataType: 'json',
                data: {idCustomer:user_id,idSell:<?php echo !empty($model->id) ? $model->id : '0'; ?>},
                success: function(data){
                    var jsonReturn = JSON.parse(data);
                    $('#div_reward_app').html(jsonReturn['html']);
                    setOnchangeCheckbox();
                }
            });
        }
    }
    
    function amountDiscountChange(){
        $('.amount_discount').change(function(){
            var amount_discount = $(this).val();
            amount_discount = amount_discount.replace(/,/g , '');// remove comma in string
//            amount_discount = amount_discount.replace(/[^0-9.]/g, "");
            $('.discount_amount_hide').val(amount_discount);
            updateDiscountText();
        });
        
        $('.AutoNoteTraThe').click(function(){
            $('.NoteTraThe').val($(this).text());
        });
        
    }
    
    function updateDiscountText(){
        var discount_amount_hide = $('.discount_amount_hide').val();
        discount_amount_hide = parseFloat(discount_amount_hide);
        discount_amount_hide = Math.round(discount_amount_hide * 100) / 100;
        $('.total_discount').text(commaSeparateNumber(discount_amount_hide));
        fnGrandTotal();
    }
    
    /**
    * @Author: ANH DUNG Apr 12, 2017
    * @Todo: khi chọn lại đại lý thì load lại giá cho đại lý đó 
    */
    function fnSelectAgent(user_id, idField, idFieldCustomer){
        $.blockUI({ overlayCSS: { backgroundColor: '#fff' } }); 
        $('.RowEmployeeMaintain').find('.remove_row_item').click();// Aug2017 xóa nhân viên giao nhận khi đổi đại lý
        $.ajax({
            url:'<?php echo Yii::app()->createAbsoluteUrl('admin/sell/index',['AJAX_AGENT_INVENTORY'=>1]) ?>',
            type: 'get',
            data: {idAgent:user_id},
            success: function(data){
                $('#elementAgent').html(data);
                rebuildMaterial(availableMaterialsCurrent);
                rebuildAnounce();
                $.unblockUI();
            }
        });
//        $(idField).closest('form').find('button:submit').click();
    }
    
    function rebuildAnounce(){
        $( "#anounceDetail" ).html($('#hiddenAnounce').html());
    }
    
    function rebuildMaterial(availableMaterials){
        $( "#Sell_materials_name" ).autocomplete({
            source: availableMaterials,
            close: function( event, ui ) { $( "#Sell_materials_name" ).val(''); },
            select: function( event, ui ) {
                var class_item = 'materials_row_'+ui.item.id;
                if($('.'+class_item).size()<1)
                    fnBuildRowMaterial(event, ui); 
            }
        });
    }
    
    /** @Author: ANH DUNG Sep 15, 2017
     *  @Todo: không cho user change customer khi update
     */
    function hideCustomerInput(){
        <?php if(!$allowChangeCustomer):?>
        $('.BoxCustomer').find('.autocomplete_name_error').remove();
        $('.BoxCustomer').find('.remove_row_item').remove();
        $('.BoxCustomerLinkUpdate').remove();
        <?php endif;?>
    }
    
    /** @Author: ANH DUNG Dec 04, 2017
     *  @Todo: xử lý put event Remove row notify Order App Gas24h when one user has confirm
     */
    function putConfirmAppGas24h(url_){
        $.ajax({
            url:url_,
            dataType: 'json',
            success: function(data){
            }
        });
    }
    
</script>

<style>
.WrapDiscountType ul li label{ width: 180px !important;}
div.form .row label { width: 110px; }
.autocomplete_customer_info { margin-left: 100px;}
</style>