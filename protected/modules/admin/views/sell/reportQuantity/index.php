<?php $this->breadcrumbs=array(
    $this->pageTitle,
); ?>

<h1><?php echo $this->pageTitle;?></h1>
<div class="search-form" style="">
<?php $this->renderPartial('reportQuantity/_search',array(
        'model'=>$model,
)); 
?>

</div><!-- search-form -->
<?php if(!empty($aData['aUid'])): ?>
<?php 
$gridCss = Yii::app()->getAssetManager()->publish(Yii::getPathOfAlias('zii.widgets.assets')) . '/gridview/styles.css';
Yii::app()->getClientScript()->registerCssFile($gridCss);
$session=Yii::app()->session;
$Uid = $aData['aUid'];
$data = $aData['data'];
$sum = $aData['sumAll'];
$aName = array();    
$aName = Users::getArrObjectUserByRole('', $Uid);
$aUid = CHtml::listData($aName, 'id', 'first_name'); //get array name uid login
?>
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/scroll/jquery.freezetablecolumns.1.1.js"></script>
<div class="grid-view " id="report-grid">
<table class="items materials_table sortable" >
    <thead>
        <tr>
            <th class="w-20">STT</th>
            <th class="w-250">Họ tên NV</th>
            <th class="ClosingBalance">Số bình cam bán mới</th>
            <th class="ClosingBalance" >Số bình xanh bán mới</th>
            <th class="ClosingBalance" >Số bình vàng bán mới</th>
        </tr>
         <tr>
            <?php $sumCam      = isset($sum['CAM']) ? $sum['CAM'] : "0"; ?>
            <?php $sumXanh = isset($sum['XANH']) ? $sum['XANH'] : "0"; ?>
            <?php $sumVang     = isset($sum['VANG']) ? $sum['VANG'] : "0"; ?>
            <?php // $sumCall     = isset($sum['CALL']) ? $sum['CALL'] : "0"; ?>
            <td class="w-20 item_c order_no"><?php echo "";?></td>
            <td class="w-250 item_b"><?php echo 'Tổng';?></td>
            <td class="item_c item_b"><?php echo $sumCam?></td>
            <td class="item_c item_b" ><?php echo $sumXanh?></td>
            <td class="item_c item_b" ><?php echo $sumVang?></td>
        </tr>
    </thead>
    <tbody>
        <?php $stt = 1;?>
        <?php foreach ($data as $key_uid_login => $uid_login): ?>
        <tr class="odd">
            <?php $name     = isset($aUid[$key_uid_login]) ? $aUid[$key_uid_login] : ""; ?>
            <?php $cam      = isset($uid_login['CAM']) ? $uid_login['CAM'] : "0"; ?>
            <?php $xanh     = isset($uid_login['XANH']) ? $uid_login['XANH'] : "0"; ?>
            <?php $vang     = isset($uid_login['VANG']) ? $uid_login['VANG'] : "0"; ?>
            <td class="w-20 item_c order_no"><?php echo $stt++;?></td>
            <td class="w-250 "><?php echo $name;?></td>
            <td class="item_c"><?php echo $cam?></td>
            <td class="item_c"><?php echo $xanh?></td>
            <td class="item_c"><?php echo $vang?></td>
        </tr>
        <?php endforeach; ?>
    </tbody>
</table>
</div>        
<?php endif; ?>

<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/print-store-card.css" />
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/jquery.floatThead.min.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/underscore-min.js"></script>    

<?php 
$gridCss = Yii::app()->getAssetManager()->publish(Yii::getPathOfAlias('zii.widgets.assets')) . '/gridview/styles.css';
Yii::app()->getClientScript()->registerCssFile($gridCss);
?>
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/sortElements/jquery.sortElements.js"></script>
<script>
$(function(){
var table = $('.sortable');
$('.ClosingBalance')
    .wrapInner('<span title="sort this column"/>')
    .each(function(){

        var th = $(this),
            thIndex = th.index(),
            inverse = false;

        th.click(function(){

            table.find('tbody td').filter(function(){

                return $(this).index() === thIndex;

            }).sortElements(function(a, b){
                var aText = $.text([a]);
                var bText = $.text([b]);
                    aText = parseFloat(aText.replace(/,/g, ''));
                    bText = parseFloat(bText.replace(/,/g, ''));
                    aText = aText *1;
                    bText = bText *1;

                if( aText == bText )
                    return 0;

//                return aText > bText ? // asc 
                return aText < bText ? // DESC
                    inverse ? 1 : -1
                    : inverse ? -1 : 1;
                    
            }, function(){
                // parentNode is the element we want to move
                return this.parentNode; 
            });

            inverse = !inverse;
            fnRefreshOrderNumber();
        });
    });
    
//    $('.ClosingBalance').trigger('click');
    
});
</script>
<script>
$(document).ready(function() {
//    fnAddClassOddEven('items');
    fnUpdateColorbox();
});

function fnUpdateColorbox(){   
    fixTargetBlank();
}
</script>
<style>
    .sortable thead th:hover {cursor: pointer;}
</style>