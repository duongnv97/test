<div class="search-form" style="">
<?php $this->renderPartial('report/_search',array(
        'model'=>$model,
)); ?>
</div><!-- search-form -->
<?php if(isset($aData['OUTPUT'])): ?>
<?php 
$session=Yii::app()->session;
$sumQty = $sumAmount = $sumDiscount = $sumRevenue = $sumBuVo = $sumPromotionAmount = $sumGasRemainAmount = 0;
$sumGas = $sumVo = $sumKm = $sumByType = 0;
$aMaterialType = GasMaterialsType::getAllItem();

$aOrderType = $model->getArrayOrderType();
unset($aOrderType[Sell::ORDER_TYPE_NORMAL]);
$OUTPUT_OTHER = isset($aData['OUTPUT_OTHER']) ? $aData['OUTPUT_OTHER'] : array();
$aSumOrderType = array();

?>
<table class="tb hm_table f_size_15">
    <thead>
        <tr>
            <th class="item_b">Vật Tư</th>
            <th class="item_b">Số Lượng</th>
            <th class="item_b">Giá</th>
            <th class="item_b">Thành Tiền</th>
            <th class="item_b">Chiết Khấu</th>
            <th class="item_b">Bù Vỏ</th>
            <th class="item_b">Giảm giá</th>
            <th class="item_b">Gas dư</th>
            <th class="item_b">Doanh Thu</th>
            <?php foreach ($aOrderType as $order_type => $order_type_text): ?>
                <!--<th class="item_b"><?php echo $order_type_text;?></th>-->
            <?php endforeach; ?>
        </tr>
    </thead>
    <tbody>
<?php foreach ($aData['OUTPUT'] as $agent_id => $aInfo1): ?>
    <?php foreach ($aInfo1 as $materials_type_id => $aInfo2): ?>
        <?php  
            $sumTypeQty = $sumTypeAmount= $sumTypeDiscount = $sumTypeRevenue = $sumTypeQty = $sumTypeBuVo = $sumTypePromotionAmount = $sumTypeGasRemainAmount = 0;
        ?>
        <?php foreach ($aInfo2 as $materials_id => $aInfo3): ?>
        <?php
            $amountBuVo         = $aInfo3['amount_bu_vo'] > 0 ? ActiveRecord::formatCurrency($aInfo3['amount_bu_vo']) : '';
            $promotionAmount    = $aInfo3['promotion_amount'] > 0 ? ActiveRecord::formatCurrency($aInfo3['promotion_amount']) : '';
            $gasRemainAmount    = $aInfo3['gas_remain_amount'] > 0 ? ActiveRecord::formatCurrency($aInfo3['gas_remain_amount']) : '';
            $qty            = $aInfo3['qty'] > 0 ? ActiveRecord::formatCurrency($aInfo3['qty']) : '';
//            $price          = $aInfo3['price'] > 0 ? ActiveRecord::formatCurrency($aInfo3['price']) : ';
            $price          = (($aInfo3['qty'] != 0 && $aInfo3['amount'] !=0) ? ActiveRecord::formatCurrencyRound($aInfo3['amount']/$aInfo3['qty']) : '');
            $amount         = $aInfo3['amount'] > 0 ? ActiveRecord::formatCurrencyRound($aInfo3['amount']) : '';
            $qty_discount   = $aInfo3['qty_discount'] > 0 ? ActiveRecord::formatCurrency($aInfo3['qty_discount']) : '';
            $discountAmount = $aInfo3['qty_discount'] * $model->getPromotionDiscount();
            $discountAmount += $aInfo3['amount_discount'] > 9 ? $aInfo3['amount_discount'] : 0;// Sep 04, 2016 thêm cho phần sửa CK
            $discountAmount = round($discountAmount, -3);
            
            // http://stackoverflow.com/questions/5150001/how-to-round-to-nearest-thousand
            $revenue                = $aInfo3['amount'] + $aInfo3['amount_bu_vo']- $discountAmount - $aInfo3['promotion_amount'] - $aInfo3['gas_remain_amount'];
            $sumQty                 += $aInfo3['qty'];
            $sumAmount              += $aInfo3['amount'];
            $sumDiscount            += $discountAmount;
            $sumRevenue             += $revenue;
            $sumBuVo                += $aInfo3['amount_bu_vo'];
            $sumPromotionAmount     += $aInfo3['promotion_amount'];
            $sumGasRemainAmount     += $aInfo3['gas_remain_amount'];

            $sumTypeQty             += $aInfo3['qty'];
            $sumTypeAmount          += $aInfo3['amount'];
            $sumTypeDiscount        += $discountAmount;
            $sumTypeRevenue         += $revenue;
            $sumTypeBuVo            += $aInfo3['amount_bu_vo'];
            $sumTypePromotionAmount += $aInfo3['promotion_amount'];
            $sumTypeGasRemainAmount += $aInfo3['gas_remain_amount'];
            
            $revenue        = $revenue > 0 ? ActiveRecord::formatCurrencyRound($revenue) : '' ;
            $discountAmount = $discountAmount > 0 ? ActiveRecord::formatCurrencyRound($discountAmount) : '' ;
            
            if(in_array($materials_type_id, GasMaterialsType::$ARR_WINDOW_VO)){
                $sumVo += $aInfo3['qty'];
            }elseif(in_array($materials_type_id, GasMaterialsType::$ARR_WINDOW_PROMOTION)){
                $sumKm += $aInfo3['qty'];
            }else{ // for Gas and other material val, day, vt khac
                $sumGas += $aInfo3['qty'];
            }
            $debug = '  amount: '.$aInfo3['amount']. ' amount_bu_vo: '. $aInfo3['amount_bu_vo'] .' discount: '. $discountAmount . '  promotionAmount: '. $promotionAmount;
        ?>
        <tr>
            <td><?php echo $session['MATERIAL_MODEL_SELL'][$materials_id]['name'];?></td>
            <td class="item_c"><?php echo $qty;?></td>
            <td class="item_r"><?php echo $price;?></td>
            <td class="item_r"><?php echo $amount;?></td>
            <td class="item_r"><?php echo $discountAmount;?></td>
            <td class="item_r"><?php echo $amountBuVo;?></td>
            <td class="item_r"><?php echo $promotionAmount;?></td>
            <td class="item_r"><?php echo $gasRemainAmount;?></td>
            <td class="item_r"><?php echo $revenue;?></td>
            <?php foreach ($aOrderType as $order_type => $order_type_text): ?>
                <?php 
                    $order_type_amount =  '';
                    if(isset($OUTPUT_OTHER[$agent_id][$materials_id][$order_type])){
                        $order_type_amount =  ActiveRecord::formatCurrencyRound($OUTPUT_OTHER[$agent_id][$materials_id][$order_type]);
                        if(!isset($aSumOrderType[$order_type])){
                            $aSumOrderType[$order_type]     = $OUTPUT_OTHER[$agent_id][$materials_id][$order_type]*1;
                        }else{
                            $aSumOrderType[$order_type]     += $OUTPUT_OTHER[$agent_id][$materials_id][$order_type]*1;
                        }
                    }
                ?>
                <!--<td class="item_r"><?php echo $order_type_amount;?></td>-->
            <?php endforeach; ?>
        </tr>
        <?php endforeach; // end foreach ($aInfo2 ?>
        <tr>
            <th><?php echo $aMaterialType[$materials_type_id];?></th>
            <th><?php echo ActiveRecord::formatCurrency($sumTypeQty);?></th>
            <th></th>
            <th class="item_r"><?php echo $sumTypeAmount != 0 ? ActiveRecord::formatCurrencyRound($sumTypeAmount) : '';?></th>
            <th class="item_r"><?php echo $sumTypeDiscount != 0 ? ActiveRecord::formatCurrency($sumTypeDiscount):'';?></th>
            <th class="item_r"><?php echo $sumTypeBuVo != 0 ? ActiveRecord::formatCurrency($sumTypeBuVo):'';?></th>
            <th class="item_r"><?php echo $sumTypePromotionAmount != 0 ? ActiveRecord::formatCurrency($sumTypePromotionAmount):'';?></th>
            <th class="item_r"><?php echo $sumTypeGasRemainAmount != 0 ? ActiveRecord::formatCurrency($sumTypeGasRemainAmount):'';?></th>
            <th class="item_r"><?php echo $sumTypeRevenue != 0 ? ActiveRecord::formatCurrencyRound($sumTypeRevenue):'';?></th>
            <!--<th></th><th></th><th></th>-->
        </tr>

    <?php endforeach; // end foreach ($aInfo1 ?>
<?php endforeach; // end foreach ($aData['OUTPUT'] ?>
    </tbody>
    <tfoot>
        <tr>
            <td class=''>Sum All<br>
            <!--Gas: <?php echo ActiveRecord::formatCurrency($sumGas);?><br>-->
            <!--Vỏ: <?php echo ActiveRecord::formatCurrency($sumVo);?><br>-->
            <!--KM: <?php echo ActiveRecord::formatCurrency($sumKm);?><br>-->
            </td>
            <td class="item_c"><?php echo ActiveRecord::formatCurrency($sumQty);?></td>
            <td class=""></td>
            <td class="item_r"><?php echo ActiveRecord::formatCurrencyRound($sumAmount);?></td>
            <td class="item_r"><?php echo ActiveRecord::formatCurrencyRound($sumDiscount);?></td>
            <td class="item_r"><?php echo $sumBuVo != 0 ? ActiveRecord::formatCurrencyRound($sumBuVo) : '';?></td>
            <td class="item_r"><?php echo $sumPromotionAmount != 0 ? ActiveRecord::formatCurrencyRound($sumPromotionAmount) : '';?></td>
            <td class="item_r"><?php echo $sumGasRemainAmount != 0 ? ActiveRecord::formatCurrencyRound($sumGasRemainAmount) : '';?></td>
            <td class="item_r"><?php echo ActiveRecord::formatCurrencyRound($sumRevenue);?></td>
            <?php foreach ($aOrderType as $order_type => $order_type_text): ?>
                <!--<td class="item_r"><?php echo isset($aSumOrderType[$order_type]) ? ActiveRecord::formatCurrencyRound($aSumOrderType[$order_type]) : "";?></td>-->
            <?php endforeach; ?>
        </tr>
        
        <?php foreach ($aOrderType as $order_type => $order_type_text): ?>
        <?php 
            $amount = 0;
            if(isset($aSumOrderType[$order_type])){
                $amount = $aSumOrderType[$order_type];
            }
            if(empty($amount)) continue;
            
            if($order_type == Sell::ORDER_TYPE_THU_VO){
                $sumByType -= $amount;
            }else{
                $sumByType += $amount;
            }
        ?>
        <tr>
            <td class="item_r" colspan="8"><?php echo $order_type_text;?></td>
            <td class="item_r"><?php echo ActiveRecord::formatCurrencyRound($aSumOrderType[$order_type]);?></td>
        </tr>
        <?php endforeach; ?>
        <tr>
            <td class="item_r" colspan="8">Tổng Doanh Thu</td>
            <td class="item_r"><?php echo ActiveRecord::formatCurrencyRound($sumRevenue+$sumByType);?></td>
        </tr>
    </tfoot>
</table>
        
<?php endif; ?>

<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/print-store-card.css" />
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/jquery.floatThead.min.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/underscore-min.js"></script>    
<script>
$(document).ready(function() {
    $('.hm_table').floatThead(); // lỗi mất border
});
</script>