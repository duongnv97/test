<?php $this->breadcrumbs=array(
    'Báo cáo bán hàng hộ gia đình',
); ?>
<h1><?php echo $this->pageTitle; ?></h1>

<div class="search-form" style="">
<?php
    include '_search_report_discount.php';
?>
</div><!-- search-form -->

<?php 
    $i = 1;
    $dataSize = sizeof(isset($aData['OUT_PUT']) ? $aData['OUT_PUT'] : '');
    $freeHeightTable    = ($dataSize + 1) * 19;
    $freeHeightTable    = $freeHeightTable > 400 ? 400 : $freeHeightTable;
    $gridCss = Yii::app()->getAssetManager()->publish(Yii::getPathOfAlias('zii.widgets.assets')) . '/gridview/styles.css';
    Yii::app()->getClientScript()->registerCssFile($gridCss);
?>
<?php if(!empty($aData['OUT_PUT'])): ?>
<script type="text/javascript"
        src="<?php echo Yii::app()->theme->baseUrl; ?>/js/scroll/jquery.freezetablecolumns.1.1.js"></script>
<div class="grid-view display_none">
    <table id="freezetablecolumns_report_trathe" class="items hm_table freezetablecolumns">
        <thead>
            <tr class="h_20">
                <th class="item_c w-20">#</th>
                <th class="item_c w-200">Đại lý</th>
                <th class="item_c w-100">Tổng</th>
                <?php foreach($aData['DATE'] as $date => $sumDate): ?>
                <th class="item_c w-60"><?php echo date('d', strtotime($date)); ?></th>
                <?php endforeach; ?>
            </tr>
        </thead>
        <tbody>
            <tr class="h_20">
                <td colspan="2" class="item_b item_c">Tổng</td>
                <td class="item_b item_r"><?php echo isset($aData['DATE']) ? ActiveRecord::formatCurrency(array_sum($aData['DATE'])) : ''; ?></td>
                <?php foreach($aData['DATE'] as $date => $sumDate): ?>
                <td class="item_r item_b w-60"><?php echo ActiveRecord::formatCurrency($sumDate); ?></td>
                <?php endforeach; ?>
            </tr>
            <?php foreach ($aData['OUT_PUT'] as $agent_id => $valueOutput): ?>
            <tr class="h_20">
                <td class="item_c w-20"><?php echo $i++; ?></td>
                <td class="w-200"><?php echo isset($aData['AGENT'][$agent_id]) ? $aData['AGENT'][$agent_id] : ''; ?></td>
                <td class="item_b item_r w-100"><?php echo ActiveRecord::formatCurrency(array_sum($valueOutput)); ?></td>
                <?php foreach($aData['DATE'] as $date => $sumDate): ?>
                <td class="item_r w-60"><?php echo isset($valueOutput[$date]) ? ActiveRecord::formatCurrency($valueOutput[$date]) : ''; ?></td>
                <?php endforeach; ?>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
</div>
<?php endif; ?>

<script type="text/javascript">
    fnAddClassOddEven('items');
    $('.freezetablecolumns').each(function () {
        var id_table = $(this).attr('id');
        $('#' + id_table).freezeTableColumns({
            width:       <?php echo Users::WIDTH_freezetable;?>,   // required - bt 1100
            height:      <?php echo $freeHeightTable;?>,   // required
            numFrozen: 3,     // optional
            frozenWidth: 380,   // optional
            clearWidths: true  // optional
        });
    });
    $(window).load(function () {
        var index = 1;
        $('.freezetablecolumns').each(function () {
            if (index == 1)
                $(this).closest('div.grid-view').show();
            index++;
        });
        fnAddClassOddEven('items');
    });
</script>
