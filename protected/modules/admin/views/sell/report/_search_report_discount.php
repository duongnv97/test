<div class="wide form">

    <?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
    )); ?>
    
    <div class="row">
        <?php echo $form->labelEx($model,'agent_id'); ?>
        <?php echo $form->hiddenField($model,'agent_id'); ?>
        <?php
            // 1. limit search kh của sale
            $url = Yii::app()->createAbsoluteUrl('admin/ajax/search_by_role', array('role'=> ROLE_AGENT));
            // widget auto complete search user customer and supplier
            $aDataAutoComplete = array(
                'model'=>$model,
                'field_customer_id'=>'agent_id',
                'url'=> $url,
                'name_relation_user'=>'rAgent',
                'ClassAdd' => 'w-400',
                'field_autocomplete_name' => 'autocomplete_agent',
                'placeholder'=>'Nhập mã hoặc tên đại lý',
            );
            $this->widget('ext.SpaAutocompleteCustomer.SpaAutocompleteCustomer',
                array('data'=>$aDataAutoComplete));
            ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model,'Tỉnh'); ?>
        <div class="fix-label">
            <?php
               $this->widget('ext.multiselect.JMultiSelect',array(
                     'model'=>$model,
                     'attribute'=>'province_id',
                     'data'=> GasProvince::getArrAll(),
                     // additional javascript options for the MultiSelect plugin
                    'options'=>array(
                        'selectedList' => 30,
                        ),
                     // additional style
                     'htmlOptions'=>array('style' => 'width: 400px;'),
               ));    
           ?>
        </div>
    </div>

    <div class="row more_col">
        <div class="col1">
            <?php echo Yii::t('translation', $form->label($model, 'date_from')); ?>
            <?php
            $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                'model' => $model,
                'attribute' => 'date_from',
                'options' => array(
                    'showAnim' => 'fold',
                    'dateFormat' => MyFormat::$dateFormatSearch,
//                            'minDate'=> '0',
//                        'maxDate'=> '0',
                    'changeMonth' => true,
                    'changeYear' => true,
                    'showOn' => 'button',
                    'buttonImage' => Yii::app()->theme->baseUrl . '/admin/images/icon_calendar_r.gif',
                    'buttonImageOnly' => true,
                ),
                'htmlOptions' => array(
                    'class' => 'w-16 date_from',
                    'size' => '16',
                    'class_update_val' => 'date_from',
                    'style' => 'float:left;',
                    'id' => 'date_from'
                ),
            ));
            ?>
        </div>
        <div class="col2">
            <?php echo Yii::t('translation', $form->label($model, 'date_to')); ?>
            <?php
            $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                'model' => $model,
                'attribute' => 'date_to',
                'options' => array(
                    'showAnim' => 'fold',
                    'dateFormat' => MyFormat::$dateFormatSearch,
//                            'minDate'=> '0',
//                        'maxDate'=> '0',
                    'changeMonth' => true,
                    'changeYear' => true,
                    'showOn' => 'button',
                    'buttonImage' => Yii::app()->theme->baseUrl . '/admin/images/icon_calendar_r.gif',
                    'buttonImageOnly' => true,
                ),
                'htmlOptions' => array(
                    'class' => 'w-16 date_to',
                    'size' => '16',
                    'class_update_val' => 'date_to',
                    'style' => 'float:left;',
                    'id' => 'date_to'
                ),
            ));
            ?>
        </div>
    </div>

    <div class="row buttons" style="padding-left: 159px;">
        <?php
        $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType' => 'submit',
            'label' => 'Search',
            'type' => 'null', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
            'size' => 'small', // null, 'large', 'small' or 'mini'
                //'htmlOptions' => array('style' => 'margin-bottom: 10px; float: right;'),
        ));
        ?>	
        <?php
        $this->widget('bootstrap.widgets.TbButton', array(
            'url' => Yii::app()->createAbsoluteUrl("admin/Sell/reportDiscount",['to_excel'=>1]),
            'label' => 'Xuất Excel',
            'type' => 'null', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
            'size' => 'small', // null, 'large', 'small' or 'mini'
            'htmlOptions' => array('class' => 'btn_cancel'),
        ));
        ?>
    </div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->