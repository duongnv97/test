<?php
$this->breadcrumbs=array(
    $this->pageTitle,
);
?>

<h1><?php echo $this->pageTitle; ?></h1>
<div class="search-form" style="">
<?php $this->renderPartial('report/TargetCcsSearch',array(
        'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php if(isset($aData['OUTPUT'])): ?>
<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/print-store-card.css" />
<?php 
$OUTPUT     = $aData['OUTPUT'];
$InfoPoint  = Sta2::TargetCcs();
$aMonitor   = GasOneMany::getEmployeeOfMonitor(ONE_MONITORING_MARKET_DEVELOPMENT);
$aPoint     = isset($InfoPoint['DATA']) ? $InfoPoint['DATA'] : array();
$aModelUser = isset($InfoPoint['MODEL_USER']) ? $InfoPoint['MODEL_USER'] : array();
$aMonitorNotShow = array(GasConst::UID_HA_VT);
$mAppCache       = new AppCache();
$listdataMonitor = $mAppCache->getListdataUserByRole(ROLE_MONITORING_MARKET_DEVELOPMENT);
?>
<table class="tb hm_table">
    <thead>
        <tr>
            <th class="item_c" rowspan="2">#</th>
            <th class="item_c" rowspan="2">Nhân Viên</th>
            <?php foreach ($OUTPUT as $year=>$aInfo): ?>
                <?php foreach ($aInfo as $month=>$aInfo1): ?>
                <th class="item_c" colspan="3">Tháng <?php echo $month; ?></th>
                <?php endforeach; ?>
            <?php endforeach; ?>
        </tr>
        <tr>
            <th class="item_c">Tiếp Xúc</th>
            <th class="item_c">Bảo Trì</th>
            <th class="item_c">Sản Lượng</th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($aMonitor as $monitoring_id=>$aSaleId): ?>
        <?php // if(in_array($monitoring_id, $aMonitorNotShow)) {continue;} ?>
        <tr>
            <td colspan="5" class="item_b"><?php echo isset($listdataMonitor[$monitoring_id]) ? $listdataMonitor[$monitoring_id] : $monitoring_id;?></td>
        </tr>
        <?php $index = 1; //arsort($aSaleQty);?>
        <?php foreach ($aSaleId as $sale_id=>$sale_id): ?>
            <?php if(!isset($aModelUser[$sale_id])) {continue;} ?>
            <tr>
                <td><?php echo $index++;?></td>
                <td><?php echo isset($aModelUser[$sale_id]) ? $aModelUser[$sale_id] : "";?></td>
                <?php foreach ($OUTPUT as $year=>$aInfo): ?>
                <?php foreach ($aInfo as $month=>$aInfo1): ?>
                    <?php 
                        $tiepXuc = isset($aPoint[$year][$month][$sale_id][UsersExtend::HGD_TIEP_XUC]) ? $aPoint[$year][$month][$sale_id][UsersExtend::HGD_TIEP_XUC] : "";
                        $baoTri  = isset($aPoint[$year][$month][$sale_id][UsersExtend::HGD_BAO_TRI]) ? $aPoint[$year][$month][$sale_id][UsersExtend::HGD_BAO_TRI] : "";
                        $qtyGas = isset($aInfo1[$monitoring_id][$sale_id]) ? $aInfo1[$monitoring_id][$sale_id] : "";
                    ?>
                    <td class="item_c"><?php echo ActiveRecord::formatCurrency($tiepXuc); ?></td>
                    <td class="item_c"><?php echo ActiveRecord::formatCurrency($baoTri); ?></td>
                    <td class="item_c"><?php echo ActiveRecord::formatCurrency($qtyGas); ?></td>
                <?php endforeach; ?>
            <?php endforeach; ?>
            </tr>
        <?php endforeach; // foreach ($aSaleQty ?>
        <?php endforeach; // end foreach ($aData['CCS_ID'] ?>
    </tbody>
</table>

<?php endif; ?>
