<?php
$mAppCache = new AppCache();
$aAgent = $mAppCache->getAgent();
$aMaterialTypeName      = GasMaterialsType::getAllItem();
$session=Yii::app()->session;
$aMaterialName              = $session['MATERIAL_MODEL_SELL']; // $aMaterialName[id]['name'];
$OUTPUT                 = isset($aData['OUTPUT']) ? $aData['OUTPUT'] : [];
$PRICE                  = isset($aData['PRICE']) ? $aData['PRICE'] : [];
?>
<div class="search-form" style="">
<?php $this->renderPartial('report/_search',array(
        'model'=>$model,
)); ?>
</div><!-- search-form -->
Giá niêm yết được xác định dựa vào ngày bắt đầu thống kê.
<?php if(!empty($OUTPUT['qty'])){ ?>
<table class="tb hm_table f_size_15">
    <thead>
        <tr>
            <th class="item_b">Đại lý</th>
            <th class="item_b">Loại vật tư</th>
            <th class="item_b">Vật Tư</th>
            <th class="item_b">Số Lượng</th>
            <th class="item_b">Call</th>
            <th class="item_b">App</th>
            <th class="item_b">Giá</th>
            <th class="item_b">Thành Tiền</th>
            <th class="item_b">Chiết Khấu</th>
            <th class="item_b">Bù Vỏ</th>
            <th class="item_b">Giảm giá CV</th>
            <th class="item_b">Giảm giá KM App</th>
            <th class="item_b">Giảm giá App mặc định</th>
            <!--<th class="item_b">Giảm giá tích luỹ</th>-->
            <th class="item_b">Doanh Thu</th>
        </tr>
    </thead>
    <tbody>
        <?php 
            $qty_sum = $call_sum = $app_sum = $price_total_sum = $discount_amount_sum = $amount_bu_vo_sum = $promotion_cv_sum = $promotion_app_sum = $revenue_sum = 0;
        ?>
        <?php foreach ($OUTPUT['qty'] as $agent_id => $aMaterialType) { ?>
            <?php foreach ($aMaterialType as $material_type_id => $aMaterial) { ?>
                <?php foreach ($aMaterial as $material_id => $qty) { ?>
                <?php 
                $price_current = !empty($PRICE[$agent_id][$material_id]) ? $PRICE[$agent_id][$material_id] : 0;
                $price_total = $price_current * $qty;
                $discount_amount = !empty($OUTPUT['discount_amount'][$agent_id][$material_type_id][$material_id]) ? round($OUTPUT['discount_amount'][$agent_id][$material_type_id][$material_id]) : 0;
                $amount_bu_vo = !empty($OUTPUT['amount_bu_vo'][$agent_id][$material_type_id][$material_id]) ? round($OUTPUT['amount_bu_vo'][$agent_id][$material_type_id][$material_id]) : 0;
                $promotion_cv = !empty($OUTPUT['promotion_cv'][$agent_id][$material_type_id][$material_id]) ? round($OUTPUT['promotion_cv'][$agent_id][$material_type_id][$material_id]) : 0;
                $promotion_app = !empty($OUTPUT['promotion_app'][$agent_id][$material_type_id][$material_id]) ? round($OUTPUT['promotion_app'][$agent_id][$material_type_id][$material_id]) : 0;
                $promotion_default = !empty($OUTPUT['promotion_default'][$agent_id][$material_type_id][$material_id]) ? round($OUTPUT['promotion_default'][$agent_id][$material_type_id][$material_id]) : 0;
                $revenue = round($price_total - $discount_amount - $promotion_cv - $promotion_app - $promotion_default + $amount_bu_vo);
                $call = !empty($OUTPUT['call'][$agent_id][$material_type_id][$material_id]) ? $OUTPUT['call'][$agent_id][$material_type_id][$material_id] : 0;
                $app = !empty($OUTPUT['app'][$agent_id][$material_type_id][$material_id]) ? $OUTPUT['app'][$agent_id][$material_type_id][$material_id] : 0;
                $qty_sum += $qty;
                $call_sum += $call;
                $app_sum += $app;
                $price_total_sum += $price_total;
                $discount_amount_sum += $discount_amount;
                $amount_bu_vo_sum += $amount_bu_vo;
                $promotion_cv_sum += $promotion_cv;
                $promotion_app_sum += $promotion_app;
                $promotion_default_sum += $promotion_default;
                $revenue_sum += $revenue;
                ?>
                    <tr>
                        <td class="item_l"><?php echo !empty($aAgent[$agent_id]['first_name']) ? $aAgent[$agent_id]['first_name'] : ''; ?></td>
                        <td class="item_l"><?php echo !empty($aMaterialTypeName[$material_type_id]) ? $aMaterialTypeName[$material_type_id] : ''; ?></td>
                        <td class="item_l"><?php echo !empty($aMaterialName[$material_id]['name']) ? $aMaterialName[$material_id]['name'] : ''; ?></td>
                        <td class="item_r"><?php echo ActiveRecord::formatCurrency(round($qty));?></td>
                        <td class="item_r"><?php echo !empty($call) ? ActiveRecord::formatCurrency(round($call)) : ''; ?></td>
                        <td class="item_r"><?php echo !empty($app) ? ActiveRecord::formatCurrency(round($app)) : ''; ?></td>
                        <td class="item_r"><?php echo !empty($price_current) ? ActiveRecord::formatCurrency($price_current) : ''; ?></td>
                        <td class="item_r"><?php echo !empty($price_total) ? ActiveRecord::formatCurrency($price_total) : ''; ?></td>
                        <td class="item_r"><?php echo !empty($discount_amount) ? ActiveRecord::formatCurrency($discount_amount) : ''; ?></td>
                        <td class="item_r"><?php echo !empty($amount_bu_vo) ? ActiveRecord::formatCurrency($amount_bu_vo) : ''; ?></td>
                        <td class="item_r"><?php echo !empty($promotion_cv) ? ActiveRecord::formatCurrency($promotion_cv) : ''; ?></td>
                        <td class="item_r"><?php echo !empty($promotion_app) ? ActiveRecord::formatCurrency($promotion_app) : ''; ?></td>
                        <td class="item_r"><?php echo !empty($promotion_default) ? ActiveRecord::formatCurrency($promotion_default) : ''; ?></td>
                        <!--<td class="item_b">Giảm giá tích luỹ</td>-->
                        <td class="item_r"><?php echo !empty($revenue) ? ActiveRecord::formatCurrency($revenue) : ''; ?></td>
                    </tr>
                <?php } ?>
            <?php } ?>
        <?php } ?>
        <tr>
            <td class="item_l item_b" colspan="3">Tổng</td>
            <td class="item_r item_b"><?php echo ActiveRecord::formatCurrency(round($qty_sum));?></td>
            <td class="item_r item_b"><?php echo ActiveRecord::formatCurrency(round($call_sum))?></td>
            <td class="item_r item_b"><?php echo ActiveRecord::formatCurrency(round($app_sum)); ?></td>
            <td class="item_r item_b"></td>
            <td class="item_r item_b"><?php echo ActiveRecord::formatCurrency(round($price_total_sum)); ?></td>
            <td class="item_r item_b"><?php echo ActiveRecord::formatCurrency(round($discount_amount_sum)); ?></td>
            <td class="item_r item_b"><?php echo ActiveRecord::formatCurrency(round($amount_bu_vo_sum)); ?></td>
            <td class="item_r item_b"><?php echo ActiveRecord::formatCurrency(round($promotion_cv_sum)); ?></td>
            <td class="item_r item_b"><?php echo ActiveRecord::formatCurrency(round($promotion_app_sum)); ?></td>
            <td class="item_r item_b"><?php echo ActiveRecord::formatCurrency(round($promotion_default_sum)); ?></td>
            <!--<td class="item_b">Giảm giá tích luỹ</td>-->
            <td class="item_r item_b"><?php echo ActiveRecord::formatCurrency(round($revenue_sum)); ?></td>
        </tr>
    </tbody>
</table>
<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/print-store-card.css" />
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/jquery.floatThead.min.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/underscore-min.js"></script>    
<script>
$(document).ready(function() {
    $('.hm_table').floatThead(); // lỗi mất border
});
</script>
<?php } 
?>