<?php $this->breadcrumbs=array(
    'Báo cáo bán hàng hộ gia đình',
); ?>
<h1>Báo cáo các đơn hàng trả thẻ </h1>

<div class="search-form" style="">
<?php
    include '_search_tra_the.php';
?>
</div><!-- search-form -->

<?php 
    $i = 1;
    $dataSize = sizeof(isset($aData['OUT_PUT']) ? $aData['OUT_PUT'] : '');
    $freeHeightTable    = ($dataSize + 1) * 19;
    $freeHeightTable    = $freeHeightTable > 400 ? 400 : $freeHeightTable;
    $gridCss = Yii::app()->getAssetManager()->publish(Yii::getPathOfAlias('zii.widgets.assets')) . '/gridview/styles.css';
    Yii::app()->getClientScript()->registerCssFile($gridCss);
?>
<?php if(!empty($aData['OUT_PUT'])): ?>
<script type="text/javascript"
        src="<?php echo Yii::app()->theme->baseUrl; ?>/js/scroll/jquery.freezetablecolumns.1.1.js"></script>
<div class="grid-view display_none">
    <table id="freezetablecolumns_report_trathe" class="items hm_table freezetablecolumns">
        <thead>
            <tr class="h_20">
                <th class="item_c w-20">#</th>
                <th class="item_c w-160">Đại lý</th>
                <th class="item_c w-50">Tổng</th>
                <?php foreach($aData['DATE'] as $date): ?>
                <th class="item_c w-30"><?php echo date('d', strtotime($date)); ?></th>
                <?php endforeach; ?>
            </tr>
        </thead>
        <tbody>
            <tr class="h_20">
                <td class="item_c w-20"></td>
                <td class="item_l w-160"><b>Tổng cộng (đơn)<b></td>
                <td class="item_r"><b><?php echo ActiveRecord::formatCurrency($aData['SUM']['total']); ?></b></td>
                <?php foreach($aData['DATE'] as $date): ?>
                    <?php $cell_val = ''; ?>
                    <?php if(isset($aData['SUM'][$date])): ?>
                        <?php $cell_val = $aData['SUM'][$date]; ?>
                    <?php endif; ?>
                <td class="item_c w-30"><b><?php echo $cell_val; ?></b></td>
                <?php endforeach; ?>
            </tr>
            <?php foreach ($aData['OUT_PUT'] as $agent_id => $sell_per_date):  ?>
                <tr class="h_20">
                    <td class="item_c w-20"><?php echo $i++; ?></td>
                    <td class="item_l w-160"><?php echo SellDetail::model()->getAgentById($agent_id); ?></td>
                    <td class="item_r"><b><?php echo array_sum($sell_per_date); ?></b></td>
                    <?php foreach($aData['DATE'] as $date): ?>
                        <?php $cell_val = ''; ?>
                        <?php if(isset($sell_per_date[$date])): ?>
                            <?php $cell_val = $sell_per_date[$date]; ?>
                        <?php endif; ?>
                        <td class="item_c w-30"><?php echo $cell_val; ?></td>
                    <?php endforeach; ?>
                </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
</div>
<?php endif; ?>

<script type="text/javascript">
    fnAddClassOddEven('items');
    $('.freezetablecolumns').each(function () {
        var id_table = $(this).attr('id');
        $('#' + id_table).freezeTableColumns({
            width:       <?php echo Users::WIDTH_freezetable;?>,   // required - bt 1100
            height:      <?php echo $freeHeightTable;?>,   // required
            numFrozen: 3,     // optional
            frozenWidth: 280,   // optional
            clearWidths: true  // optional
        });
    });
    $(window).load(function () {
        var index = 1;
        $('.freezetablecolumns').each(function () {
            if (index == 1)
                $(this).closest('div.grid-view').show();
            index++;
        });
        fnAddClassOddEven('items');
    });
</script>
