<?php
$this->breadcrumbs = array(
    $this->pageTitle,
);
$index = 1;
?>
<h1><?php echo $this->pageTitle;?></h1>

<div class="search-form" style="">
    <?php
    $this->renderPartial('report/allAgentSearch', array(
        'model' => $model,
    ));
    ?>

    
<?php
if (!empty($aData['detail']) && is_array($aData['detail'])):
    $mAppCache = new AppCache();
    $aAgent = $mAppCache->getAgentListdata();
    $sumThuVoFinal = 0;
    if(isset($aData['SUM_ORDER_TYPE']) && is_array($aData['SUM_ORDER_TYPE'])){

        foreach ($aData['SUM_ORDER_TYPE'] as $keySumType => $vSumType) {
            if($keySumType == Sell::ORDER_TYPE_THU_VO){
                $sumThuVoFinal += $vSumType;
            }else{
                $aData['SUM_AMOUNT'] = isset($aData['SUM_AMOUNT']) ? $aData['SUM_AMOUNT'] + $vSumType : 0;
            }
        }
    }
    ?>
    <table class="tb hm_table f_size_15">
        <thead>
            <tr>
                <th class="item_c">#</th>
                <th class="item_b">Đại lý</th>
                <th class="item_b">Doanh thu(tổng bán gas, vật tư, vỏ)</th>
                <th class="item_b">Chiết khấu</th>
                <th class="item_b">Bù vỏ</th>
                <th class="item_b">Giảm giá</th>
                <th class="item_b">Gas dư</th>
                <th class="item_b">Gas dư (kg)</th>
                <th class="item_b">Thu vỏ thế chân</th>
                <th class="item_b">Bình 12</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td class="item_c item_b" colspan="2">Tổng cộng</td>
                <td class="item_b item_r"><?php echo isset($aData['SUM_AMOUNT']) && $aData['SUM_AMOUNT'] > 0 ? ActiveRecord::formatCurrencyRound($aData['SUM_AMOUNT']): ''; ?></td>
                <td class="item_b item_r"><?php echo isset($aData['SUM_DISCOUNT']) && $aData['SUM_DISCOUNT'] > 0 ? ActiveRecord::formatCurrencyRound($aData['SUM_DISCOUNT']): ''; ?></td>
                <td class="item_b item_r"><?php echo isset($aData['SUM_BU_VO']) && $aData['SUM_BU_VO'] > 0? ActiveRecord::formatCurrencyRound($aData['SUM_BU_VO']): ''; ?></td>
                <td class="item_b item_r"><?php echo isset($aData['SUM_PROMOTION_AMOUNT']) && $aData['SUM_PROMOTION_AMOUNT'] > 0? ActiveRecord::formatCurrencyRound($aData['SUM_PROMOTION_AMOUNT']): ''; ?></td>
                <td class="item_b item_r"><?php echo isset($aData['SUM_GAS_REMAIN_AMOUNT']) && $aData['SUM_GAS_REMAIN_AMOUNT'] > 0? ActiveRecord::formatCurrencyRound($aData['SUM_GAS_REMAIN_AMOUNT']): ''; ?></td>
                <td class="item_b item_r"><?php echo isset($aData['SUM_GAS_REMAIN']) && $aData['SUM_GAS_REMAIN'] > 0? ActiveRecord::formatCurrency($aData['SUM_GAS_REMAIN']): ''; ?></td>
                <td class="item_b item_r"><?php echo $sumThuVoFinal > 0 ? ActiveRecord::formatCurrencyRound($sumThuVoFinal) : '' ; ?></td>
                <td class="item_b item_r"><?php echo isset($aData['SUM_BINH_12KG']) && $aData['SUM_BINH_12KG'] > 0? ActiveRecord::formatCurrencyRound($aData['SUM_BINH_12KG']): ''; ?></td>
            </tr>
    <?php foreach ($aData['detail'] as $agent_id => $aSum): 
        $sumThuVo = 0;
        if(isset($aSum['SUM_ORDER_TYPE']) && is_array($aSum['SUM_ORDER_TYPE'])){

            foreach ($aSum['SUM_ORDER_TYPE'] as $keySumType => $vSumType) {
                if($keySumType == Sell::ORDER_TYPE_THU_VO){
                    $sumThuVo += $vSumType;
                }else{
                    $aSum['SUM_AMOUNT'] = isset($aSum['SUM_AMOUNT']) ? $aSum['SUM_AMOUNT'] + $vSumType : 0;
                }
            }
        }
    ?>
            <tr>
                <td class="item_c"><?php echo $index++; ?></td>
                <td class="item_l"><?php echo isset($aAgent[$agent_id]) ? $aAgent[$agent_id]: $agent_id; ?></td>
                <td class="item_r"><?php echo isset($aSum['SUM_AMOUNT']) && $aSum['SUM_AMOUNT'] > 0 ? ActiveRecord::formatCurrencyRound($aSum['SUM_AMOUNT']): ''; ?></td>
                <td class="item_r"><?php echo isset($aSum['SUM_DISCOUNT']) && $aSum['SUM_DISCOUNT'] > 0 ? ActiveRecord::formatCurrencyRound($aSum['SUM_DISCOUNT']): ''; ?></td>
                <td class="item_r"><?php echo isset($aSum['SUM_BU_VO']) && $aSum['SUM_BU_VO'] > 0? ActiveRecord::formatCurrencyRound($aSum['SUM_BU_VO']): ''; ?></td>
                <td class="item_r"><?php echo isset($aSum['SUM_PROMOTION_AMOUNT']) && $aSum['SUM_PROMOTION_AMOUNT'] > 0? ActiveRecord::formatCurrencyRound($aSum['SUM_PROMOTION_AMOUNT']): ''; ?></td>
                <td class="item_r"><?php echo isset($aSum['SUM_GAS_REMAIN_AMOUNT']) && $aSum['SUM_GAS_REMAIN_AMOUNT'] > 0? ActiveRecord::formatCurrencyRound($aSum['SUM_GAS_REMAIN_AMOUNT']): ''; ?></td>
                <td class="item_r"><?php echo isset($aSum['SUM_GAS_REMAIN']) && $aSum['SUM_GAS_REMAIN'] > 0? ActiveRecord::formatCurrency($aSum['SUM_GAS_REMAIN']): ''; ?></td>
                <td class="item_r"><?php echo $sumThuVo > 0 ? ActiveRecord::formatCurrencyRound($sumThuVo) : '' ; ?></td>
                <td class="item_r"><?php echo isset($aSum['SUM_BINH_12KG']) && $aSum['SUM_BINH_12KG'] > 0? ActiveRecord::formatCurrencyRound($aSum['SUM_BINH_12KG']): ''; ?></td>
            </tr>
    <?php endforeach; ?>
            <tr>
                <td class="item_c item_b" colspan="2">Tổng cộng</td>
                <td class="item_b item_r"><?php echo isset($aData['SUM_AMOUNT']) && $aData['SUM_AMOUNT'] > 0 ? ActiveRecord::formatCurrencyRound($aData['SUM_AMOUNT']): ''; ?></td>
                <td class="item_b item_r"><?php echo isset($aData['SUM_DISCOUNT']) && $aData['SUM_DISCOUNT'] > 0 ? ActiveRecord::formatCurrencyRound($aData['SUM_DISCOUNT']): ''; ?></td>
                <td class="item_b item_r"><?php echo isset($aData['SUM_BU_VO']) && $aData['SUM_BU_VO'] > 0? ActiveRecord::formatCurrencyRound($aData['SUM_BU_VO']): ''; ?></td>
                <td class="item_b item_r"><?php echo isset($aData['SUM_PROMOTION_AMOUNT']) && $aData['SUM_PROMOTION_AMOUNT'] > 0? ActiveRecord::formatCurrencyRound($aData['SUM_PROMOTION_AMOUNT']): ''; ?></td>
                <td class="item_b item_r"><?php echo isset($aData['SUM_GAS_REMAIN_AMOUNT']) && $aData['SUM_GAS_REMAIN_AMOUNT'] > 0? ActiveRecord::formatCurrencyRound($aData['SUM_GAS_REMAIN_AMOUNT']): ''; ?></td>
                <td class="item_b item_r"><?php echo isset($aData['SUM_GAS_REMAIN']) && $aData['SUM_GAS_REMAIN'] > 0? ActiveRecord::formatCurrency($aData['SUM_GAS_REMAIN']): ''; ?></td>
                <td class="item_b item_r"><?php echo $sumThuVoFinal > 0 ? ActiveRecord::formatCurrencyRound($sumThuVoFinal) : '' ; ?></td>
                <td class="item_b item_r"><?php echo isset($aData['SUM_BINH_12KG']) && $aData['SUM_BINH_12KG'] > 0? ActiveRecord::formatCurrencyRound($aData['SUM_BINH_12KG']): ''; ?></td>
            </tr>
<?php endif; ?>
        </tbody>
    </table>
</div>

<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/print-store-card.css" />
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/jquery.floatThead.min.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/underscore-min.js"></script>    
<script>
    $(document).ready(function () {
        $('.hm_table').floatThead(); // lỗi mất border
    });
</script>