<?php
// Jun 26, 2016 khởi tạo session cho price hgd put xuống window
UsersPrice::initSessionPriceHgd(date("Y-m-d"));// không nên dùng cache ở chỗ này, nên dùng session => bỏ đã dùng cache Now 07, 2016
// Jun 26, 2016 khởi tạo session cho price hgd put xuống window
if(!is_array($model->aDetail)){
    $model->aDetail = array();
}
//LOC007
$mGasAnnounce = new Announce();
$mGasAnnounce->agent_id = $model->agent_id;
?>

<h3 class='title-info'>Chi tiết</h3>
<div class="clr"></div>
<div id="anounceDetail" class="row">
    <!--<label>&nbsp;</label>-->
    <div class="add_new_item float_l f_size_15 ck_remove_space" style="padding-left: 0;"><?php echo $mGasAnnounce->getAgentPromotion(); //LOC007?></div>
</div>
<div class='clr'></div>

<div class="row">
    <?php echo $form->labelEx($model,'materials_name'); ?>
    <?php echo $form->textField($model,'materials_name',array('size'=>32,'placeholder'=>'Nhập mã vật tư hoặc tên vật tư')); ?>
    <!--<em class="hight_light item_b">Chú Ý: Có thể chọn nhiều vật tư trong cùng 1 lần tạo </em>-->
    <?php echo $form->error($model,'materials_name'); ?>
    <div class="clr"></div>    		
</div>

<div class="row">
    <label>&nbsp;</label>
    <div class="add_new_item float_l f_size_15" style="padding-left: 0;"><a class="" style="padding-left: 0;" href="javascript:;"><?php echo $model->getTextDiscountApp();?></a></div>
</div>
<div class='clr'></div>
<?php if($model->v1_discount_id == Forecast::TYPE_GOLD_TIME): ?>
<div class="row">
    <label>&nbsp;</label>
    <div class="add_new_item float_l f_size_15" style="padding-left: 0;"><a class="" style="padding-left: 0;" href="javascript:;">Giảm giá giờ vàng: <?php echo ActiveRecord::formatCurrency($model->getDiscountGoldTime());?>. Khung giờ vàng: <?php echo Yii::app()->params['goldTimeList'];?></a></div>
</div>
<div class='clr'></div>
<?php endif; ?>

<div class="row">
    <table class="materials_table hm_table">
        <thead>
            <tr>
                <th class="item_c">#</th>
                <th class="item_c">Mã</th>
                <th class="item_name item_c">Tên</th>
                <th class="item_unit item_c">ĐVT</th>
                <th class="w-30 item_c">SL</th>
                <th class=" item_c">Đơn Giá</th>
                <th class=" item_c w-100 ">Thành Tiền</th>
                <th class=" item_c w-50 ">Giá 0 đồng</th>
                <th class=" item_c w-50 ">Seri</th>
                <th class=" item_c w-30 ">KL vỏ</th>
                <th class=" item_c w-30 ">KL Cân</th>
                <th class=" item_c w-30 ">Gas dư</th>
                <th class="item_unit last item_c">Xóa</th>
            </tr>
        </thead>
        <tbody>
        <?php foreach($model->aDetail as $key=>$item):?>
            <?php $mMaterial = $item->rMaterial; ?>
            <?php if($mMaterial): ?>
            <tr class="materials_row">
                <td class="item_c order_no"><?php echo ($key+1);?></td>
                <td><?php echo $mMaterial->materials_no;?></td>
                <td class="item_l"><?php echo $mMaterial->name;?></td>
                <td class="item_c"><?php echo $mMaterial->unit;?></td>
                <td class="item_c ">
                    <input name="materials_qty[]" value="<?php echo ActiveRecord::formatNumberInput($item->qty);?>" class="item_c item_qty materials_qty number_only_v1 w-30 " type="text" size="6" maxlength="9">
                    <input name="materials_id[]" value="<?php echo $mMaterial->id;?>" class="materials_id" type="hidden">
                    <input name="unit_use[]" value="1" class="unit_use" type="hidden">
                </td>
                <td class="item_c ">
                    <input <?php echo $readonlyPrice;?> name="price[]" value="<?php echo ActiveRecord::formatNumberInput($item->price);?>" class="item_price number_only number_only_v1 w-60" type="text" maxlength="9" >
                    <div class="help_number"></div>
                </td>
                <td class="item_r item_amount f_size_15 item_b">
                    <?php echo ActiveRecord::formatCurrency($item->amount);?>
                </td>
                <td class="item_c ">
                    <input class="item_zero_price item_c" type="checkbox">
                </td>
                <td class="item_r ">
                    <input <?php echo $readonlyGasRemain;?> name="seri[]" value="<?php echo $item->seri;?>" class="item_seri w-40 item_c" type="text" maxlength="5">
                </td>
                <td class="item_r ">
                    <input <?php echo $readonlyGasRemain;?> name="kg_empty[]" value="<?php echo $item->kg_empty > 0 ? ActiveRecord::formatNumberInput($item->kg_empty)*1 : '';?>" class=" w-40 item_c" type="text" maxlength="5">
                </td>
                <td class="item_r ">
                    <input <?php echo $readonlyGasRemain;?> name="kg_has_gas[]" value="<?php echo $item->kg_has_gas > 0 ? ActiveRecord::formatNumberInput($item->kg_has_gas)*1 : '';?>" class=" w-40 item_c" type="text" maxlength="5">
                </td>
                <td class="item_c ">
                    <span><?php echo $item->gas_remain > 0 ? ActiveRecord::formatNumberInput($item->gas_remain)*1 : '';?></span>
                </td>
                <td class="item_c last"><span class="remove_icon_only"></span></td>
            </tr>
            <?php endif;?>
        <?php endforeach;?>
        </tbody>
        <tfoot>
            <tr class="materials_row">
                <td class="item_b item_r f_size_16" colspan="4"></td>
                <td class="item_b item_c total_qty f_size_16"></td>
                <td class=""></td>
                <td class="item_b item_r total_amount f_size_16"></td>
                <td class="last" colspan="6"></td>
            </tr>
            <tr class="materials_row">
                <td class="item_b item_r f_size_16 color_red" colspan="4">Chiết khấu</td>
                <td class="item_b item_r f_size_16" colspan="3"><span class="total_discount color_red"><?php echo $model->getDiscountNormal(true);?></span></td>
                <td class="last" colspan="6"></td>
            </tr>
            <tr class="materials_row">
                <td class="item_b item_r f_size_16 color_red" colspan="4">Khuyến mãi</td>
                <td class="item_b item_r f_size_16" colspan="3"><span class="total_promotion_amount color_red"><?php echo $model->getAppPromotion(true);?></span></td>
                <td class="last" colspan="6"></td>
            </tr>
            <tr class="materials_row">
                <td class="item_b item_r f_size_16 color_red" colspan="4">Gas dư</td>
                <td class="item_b item_r f_size_16" colspan="3"><span class="color_red"><?php echo $model->getGasRemainAmount(true);?></span></td>
                <td class="last" colspan="6"></td>
            </tr>
            <tr class="materials_row">
                <td class="item_b item_r f_size_16" colspan="4">Bù vỏ</td>
                <td class="item_b item_r f_size_16" colspan="3"><span class=""><?php echo $model->getAmountBuBo(true);?></span></td>
                <td class="last" colspan="6"></td>
            </tr>
            <tr class="materials_row">
                <td class="item_b item_r f_size_16 " colspan="4">Bán Hàng <span class="type_amount_text"><?php echo $model->getOrderTypeAppView($model->order_type); ?></span></td>
                <td class="item_b item_r f_size_16" colspan="3"><span class="type_amount"><?php echo $model->getTypeAmount(true);?></span></td>
                <td class="last" colspan="6"></td>
            </tr>
            <tr class="materials_row">
                <td class="item_b item_r f_size_16" colspan="4">Thành tiền</td>
                <td class="item_b item_r f_size_16" colspan="3"><span class="grand_total">0</span></td>
                <td class="last" colspan="6"></td>
            </tr>
        </tfoot>
    </table>        
</div>
<input type="hidden" class="total_hide" value="">
<input type="hidden" class="discount_amount_hide" value="<?php echo $model->getDiscountNormal();?>">
<input type="hidden" class="promotion_amount_hide" value="<?php echo $model->promotion_amount;?>">
<input type="hidden" class="discount_default_hide" value="<?php echo $model->getPromotionDiscount();?>">
<input type="hidden" class="type_amount_hide" value="<?php echo $model->getTypeAmount();?>">
<input type="hidden" class="amount_bu_vo_hide" value="<?php echo $model->getAmountBuBo();?>">
<input type="hidden" class="gas_remain_amount_hide" value="<?php echo $model->getGasRemainAmount();?>">

<script>
    //  Jun 25, 2016 begin for form detail
    $(document).ready(function(){
        fnInitAutocomplete();    
        fnRefreshOrderNumber();
        fnBindRemoveIcon();
        fnCalcTotalPay();
        fnBindSomeLiveChange();
        checkZeroPrice();
    });
    
    /**
    * @Author: ANH DUNG Jun 25, 2016
    * @Todo: init autocomplete
    */
    function fnInitAutocomplete(){
        var availableMaterials = <?php echo MyFunctionCustom::getMaterialsJson(array('agent_id'=>$model->agent_id)); ?>;

        $( "#Sell_materials_name" ).autocomplete({
            source: availableMaterials,
            close: function( event, ui ) { $( "#Sell_materials_name" ).val(''); },
            select: function( event, ui ) {
                var class_item = 'materials_row_'+ui.item.id;
                if($('.'+class_item).size()<1)
                    fnBuildRowMaterial(event, ui); 
            }
        });
    }
    
    function fnBindSomeLiveChange(){
        $('.item_qty, .item_price, .OrderTypeAmount').live('change', function(){
            fnCalcTotalPay();
        });
    }
    
    function fnBuildRowMaterial(event, ui){
        var PriceBinhBoByMonth = "";
        var class_item = 'materials_row_'+ui.item.id;
        var _tr = '';
            _tr += '<tr class="materials_row">';
                _tr += '<td class="item_c order_no"></td>';
                _tr += '<td>'+ui.item.materials_no+'</td>';
                _tr += '<td class="item_l">'+ui.item.name+'</td>';
                _tr += '<td class="item_c">'+ui.item.unit+'</td>';
                _tr += '<td class="item_c">';
                    _tr += '<input name="materials_qty[]" value="1" class="item_c item_qty w-30 materials_qty number_only_v1 '+class_item+'" type="text" size="6" maxlength="9">';
                    _tr += '<input name="materials_id[]" value="'+ui.item.id+'" class="materials_id" type="hidden">';
                    _tr += '<input name="unit_use[]" value="'+ui.item.unit_use+'" class="unit_use" type="hidden">';
                _tr += '</td>';
                _tr += '<td class="item_c ">';
                    _tr += '<input <?php echo $readonlyPrice;?> name="price[]" value="'+ui.item.price+'" class="item_price number_only number_only_v1 w-60" type="text" maxlength="9">';
                    _tr += '<div class="help_number"></div>';
                _tr += '</td>';
                _tr += '<td class="item_r item_amount  f_size_15 item_b"></td>';
                _tr += '<td class="item_c ">';
                    _tr += '<input class="item_zero_price item_c" type="checkbox">';
                _tr += '</td>';
                _tr += '<td class="item_c">';
                    _tr += '<input <?php echo $readonlyGasRemain;?> name="seri[]" class="item_seri w-40 item_c" type="text" maxlength="6">';
                _tr += '</td>';
                _tr += '<td class="item_c">';
                    _tr += '<input <?php echo $readonlyGasRemain;?> name="kg_empty[]" class="w-40 item_c" type="text" maxlength="6">';
                _tr += '</td>';
                _tr += '<td class="item_c">';
                    _tr += '<input <?php echo $readonlyGasRemain;?> name="kg_has_gas[]" class="w-40 item_c" type="text" maxlength="6">';
                _tr += '</td>';
                _tr += '<td class="item_c"></td>';
                
                _tr += '<td class="item_c last"><span class="remove_icon_only"></span></td>';
            _tr += '</tr>';
        $('.materials_table tbody').append(_tr);
        fnRefreshOrderNumber();
        bindEventForHelpNumber();
        fnCalcTotalPay();
    }
    
    function fnCalcTotalPay(){
        var total_amount    = 0;
        var total_qty       = 0;
        var orderType       = getOrderType();
        var type_amount     = removeCommaInString($('.OrderTypeAmount').val());
        if(orderType == <?php echo Sell::ORDER_TYPE_THU_VO;?>){
            type_amount *= -1;
            console.log(type_amount);
        }
        $('.type_amount_hide').val(type_amount);
        $('.type_amount').text($('.OrderTypeAmount').val());
        
        $('.materials_table tbody .materials_row').each(function(){
           var item_qty = $(this).find('.item_qty').val()*1; 
           var unit_use = $(this).find('.unit_use').val()*1; 
           total_qty+=item_qty;
           var item_price = $(this).find('.item_price').val()*1; 
           var  amount = parseFloat(item_qty*item_price*unit_use);
           amount = Math.round(amount * 100) / 100;
           total_amount += amount;
           $(this).find('.item_amount').text(commaSeparateNumber(amount)); 
        });
        
        total_amount = parseFloat(total_amount);
        total_amount = Math.round(total_amount * 100) / 100;
        $('.total_hide').val(total_amount);
        $('.total_amount').text(commaSeparateNumber(total_amount));

        total_qty = parseFloat(total_qty);
        total_qty = Math.round(total_qty * 100) / 100;
        $('.total_qty').text(commaSeparateNumber(total_qty));
        fnGrandTotal();
    }
    
    function fnAfterRemoveIcon(){
        fnCalcTotalPay();
    }
    // tính tổng đơn hàng
    function fnGrandTotal(){
        var total = $('.total_hide').val()*1;
        var discount_amount_hide    = $('.discount_amount_hide').val()*1;
        var promotion_amount_hide   = $('.promotion_amount_hide').val()*1;
        var type_amount_hide        = $('.type_amount_hide').val()*1;
        var amount_bu_vo_hide       = $('.amount_bu_vo_hide').val()*1;
        var gas_remain_amount_hide  = $('.gas_remain_amount_hide').val()*1;
        var grand_total             = total + type_amount_hide + amount_bu_vo_hide - discount_amount_hide - promotion_amount_hide - gas_remain_amount_hide;
        grand_total = parseFloat(grand_total);
        grand_total = Math.round(grand_total * 100) / 100;
        $('.grand_total').text(commaSeparateNumber(grand_total));
    }
    
    /** @Author: ANH DUNG Sep 22, 2017
    *  @Todo: event click zero price
    */
    function checkZeroPrice(){
        $('body').on('click', '.item_zero_price', function(){
            if($(this).is(':checked')){
                var tr = $(this).closest('tr');
                tr.find('.item_price').val(0);
                fnCalcTotalPay();
            }
        });
    }
    
//    Jun 25, 2016 end for form detail
    
</script>


