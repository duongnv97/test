<!--<h3 class='title-info'>Tồn kho đại lý</h3>-->
<?php
$mAppCache      = new AppCache();
//$aInventory     = $mAppCache->getCacheDecode(AppCache::APP_AGENT_INVENTORY);
$aInventory     = $mAppCache->getCacheInventoryAllAgent(AppCache::APP_AGENT_INVENTORY);
$aMaterials     = $mAppCache->getMasterModel('GasMaterials', AppCache::ARR_MODEL_MATERIAL);
$aMaterialsType = $mAppCache->getMasterModel('GasMaterialsType', AppCache::ARR_MODEL_MATERIAL_TYPE);
$aAgent         = $mAppCache->getAgent();
$mMonitorUpdate = new MonitorUpdate();
$timeUpdateInventory = $mMonitorUpdate->getTimeUpdateInventory();
$timeUpdateInventory = explode(' ', $timeUpdateInventory);
$isAppGas24         = isset($_GET['transaction_id']) ? true : false;
$mMaterialTmp       =  new GasMaterials();
$aIdMateriasApp     = $mMaterialTmp->getIdApp();// load những item được phép bán của App Gas24h
$aTypeShow = [];

$aMaterialsTypeAllow = array_merge(GasMaterialsType::$ARR_WINDOW_GAS, [GasMaterialsType::MATERIAL_TYPE_PROMOTION]) ;
?>
<table class="tb f_size_13" cellspacing="0" cellpadding="0">
    <thead>
        <tr>
            <th class="item_b" colspan="2">Tồn kho cập nhật lúc: <?php echo $timeUpdateInventory[1];?></th>
            <th class="item_b">Tồn kho</th>
        </tr>
    </thead>
    <tbody>
    <?php foreach ($aInventory as $agent_id => $aInfo1): ?>
    <?php if($model->agent_id != $agent_id) continue; ?>
    <tr>
        <td colspan="3">
            <table cellspacing="0" cellpadding="0">
                <tr>
                    <td colspan="3" style="background: #00BCD4;">
                        <?php echo $aAgent[$agent_id]['code_account'].' - '.$aAgent[$agent_id]['first_name'];?>
                    </td>
                </tr>
                <?php foreach ($aInfo1 as $materials_type_id => $aInfo2): ?>
                <?php if(!isset($aMaterialsType[$materials_type_id]) || !in_array($materials_type_id, $aMaterialsTypeAllow)) continue; ?>
                    <tr>
                        <td colspan="3" style="background: #9d9d9d;">
                            <?php echo $aMaterialsType[$materials_type_id]['name'];?>
                        </td>
                    </tr>
                    <?php foreach ($aInfo2 as $materials_id => $qty): ?>
                    <?php 
//                        if($materials_type_id == GasMaterialsType::MATERIAL_BINH_12KG && $isAppGas24 && !in_array($materials_id, $aIdMateriasApp)){
//                            continue ;
//                        }
                    ?>
                    <tr>
                        <td class="item_c"><?php echo $aMaterials[$materials_id]['materials_no'];?></td>
                        <td class=""><?php echo $aMaterials[$materials_id]['name'];?></td>
                        <td class="item_c"><?php echo ActiveRecord::formatCurrency($qty);?></td>
                    </tr>
                    <?php endforeach; ?>
                <?php endforeach; ?>
            </table>
        </td>
    </tr>
    <?php endforeach; ?>
    </tbody>
</table>
<div id="hiddenAnounce" class="display_none">
    <?php 
        $mGasAnnounce = new Announce();
        $mGasAnnounce->agent_id = $model->agent_id;
    ?>
    <?php echo $mGasAnnounce->getAgentPromotion();?>
</div>
<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/print-store-card.css" />
<script>
    var availableMaterialsCurrent   = <?php echo MyFunctionCustom::getMaterialsJson(array('agent_id'=>$model->agent_id)); ?>;
</script>