<!--<em class="hight_light item_b" style="margin-left: 10px;"><?php echo Sell::getAppTextUpdateHgd();?></em>-->

<!--<br><em class="hight_light item_b" style="margin-left: 10px;">Hệ thống tạm dừng cập nhật thẻ kho hộ gia đình đến 12h cùng ngày</em>-->
<?php 
    $canClickUpdate = $model->canClickUpdate();
    $dateUpdateNext = $model->date_from_ymd;// may14201701 dùng tạm biến date_from_ymd để lưu date
?>
<?php if($model->canActionClickUpdateStorecard() && !in_array(MyFormat::getAgentId(), UsersExtend::getAgentRunAppGN())): ?>
<br>
<em class="hight_light item_b" style="margin-left: 10px;">Lần cập nhật thẻ kho hộ gia đình tiếp theo của bạn là: <?php echo $dateUpdateNext; ?>. Khi bấm cập nhật hệ thống sẽ cập nhật thẻ kho cho bạn ngày <?php echo MyFormat::modifyDays(date('Y-m-d'), 1, '-', 'day', 'd-m-Y');?> và ngày <?php echo date('d-m-Y'); ?></em>
<?php endif; ?>

<?php if($canClickUpdate && in_array(MyFormat::getAgentId(), UsersExtend::getAgentNotRunApp()) ): ?>
<?php // if(0): ?>
<div class="form">
    <?php
        $Link = Yii::app()->createAbsoluteUrl('admin/sell/index', array( 'AgentClickUpdate'=> 1));
    ?>
    <h1><?php echo $this->adminPageTitle;?>
        <a class='btn_cancel f_size_14 btn_closed_tickets' alert_text="Chắc chắng muốn cập nhật thẻ kho hộ gia đình?" href="<?php echo $Link;?>">Click cập nhật thẻ kho hộ gia đình</a>
    </h1> 
</div>
<?php endif; ?>

<div class="form">
    <?php
        $LinkNormal     = Yii::app()->createAbsoluteUrl('admin/sell/create', ['customer_id'=> 0, 'phone_number'=>0, 'auto_note'=>1]);
        $linkCreateTest = Yii::app()->createAbsoluteUrl('admin/sell/index', ['makeTestOrder'=> 1]);
    ?>
    <a class='btn_cancel f_size_14 ' target="_blank" href="<?php echo $LinkNormal;?>">Tạo đơn trả thẻ</a>
    <?php if(!GasCheck::isServerLive()): ?>
        <a class='btn_cancel f_size_14 ' href="<?php echo $linkCreateTest;?>">Tạo đơn Test Bình Thạnh 2</a>
    <?php endif; ?>
</div>