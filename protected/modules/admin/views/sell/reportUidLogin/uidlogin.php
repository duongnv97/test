<?php $this->breadcrumbs=array(
    $this->pageTitle,
); ?>
<h1><?php echo $this->pageTitle;?></h1>
<style>
    td {
        height: 25px;
    }
    .qty {
    width: 50px !important;
    }
    .amount {
    width: 146px !important;
    }
</style>
<div class="search-form" style="">
<?php $this->renderPartial('reportUidLogin/_search',array(
        'model'=>$model,
)); 
?>

</div><!-- search-form -->
<?php if(isset($aData['OUTPUT'])): ?>
<?php 
$session=Yii::app()->session;
$Uid = $aData['Uid'];
$COLUMN = $aData['COLUMN'];
$OUTPUT = $aData['OUTPUT'];
$SUM_AMOUNT = $aData['SUM_AMOUNT'];
$SUMTYPE = $aData['SUM_TYPE'];
$COLUMN_BINH_12 =  $COLUMN['BINH_12'];
$COLUMN_VAT_TU =  $COLUMN['VAT_TU'];
$stt =1;
$gridCss = Yii::app()->getAssetManager()->publish(Yii::getPathOfAlias('zii.widgets.assets')) . '/gridview/styles.css';
Yii::app()->getClientScript()->registerCssFile($gridCss);
$name = array();
$name= Users::getArrObjectUserByRole('', $Uid);
$aUid = CHtml::listData($name, 'id', 'first_name'); //get array name uid login
?>
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/scroll/jquery.freezetablecolumns.1.1.js"></script>
<div class="grid-view display_none">
<table class="tb items hm_table freezetablecolumns f_size_15 " id="freezetablecolumns" >
    <thead>
        <tr>
            <th class="w-20">#</th>
            <th class="w-250">Họ tên</th>
            <?php foreach ($Uid as $key_uid_login => $uid_login): ?>
            <th colspan="2" class="w-210 item_c"><?php echo $aUid[$uid_login];?></th>
            <?php endforeach; ?>
        </tr>
        <tr>
            <td class="item_c" ></td>
            <td class="item_c" >Tổng Doanh Thu Từng NV</td>
            <?php foreach ($Uid as $key_uid_login => $uid_login): ?>
            <?php $sum_uid = isset($SUM_AMOUNT[$uid_login]) ? ActiveRecord::formatCurrency($SUM_AMOUNT[$uid_login]) : "" ;?>
            <td colspan="2" class="item_c w-210"><?php echo $sum_uid;?></td>
            <?php endforeach; ?>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td class="item_c" ></td>
            <td class="item_c">Tổng sản lượng (bình 12kg)</td>
            <?php foreach ($Uid as $key_uid_login => $uid_login): ?>
                        <?php $qty_sum = isset($SUMTYPE[$uid_login]['BINH_12']['QTY']) ? $SUMTYPE[$uid_login]['BINH_12']['QTY'] : '0';?>
                        <?php $amount_sum = isset($SUMTYPE[$uid_login]['BINH_12']['AMOUNT']) ?  $SUMTYPE[$uid_login]['BINH_12']['AMOUNT'] : '0';?>
                        <?php $amount_sum = ActiveRecord::formatCurrency($amount_sum)?>
                    <td class="item_c qty" ><?php echo $qty_sum;?></td>
                    <td class="item_c amount" ><?php echo $amount_sum;?></td>
            <?php endforeach; ?>
        </tr>
        
        <?php  foreach ($COLUMN_BINH_12 as $key_materials_id => $materials_id): ?>
        <tr>
            <td class="item_c"><?php echo $stt;?></td>
            <td><?php echo $session['MATERIAL_MODEL_SELL'][$materials_id]['name'];?></td>
            <?php foreach ($Uid as $key_uid_login => $uid_login): ?>
                        <?php $qty = isset($OUTPUT[$uid_login][$materials_id]) ? ActiveRecord::formatCurrency($OUTPUT[$uid_login][$materials_id]['qty'])  : '0';?>
                        <?php $amount = isset($OUTPUT[$uid_login][$materials_id]) ?  ActiveRecord::formatCurrency($OUTPUT[$uid_login][$materials_id]['amount']) : '0';?>
                    <td class="item_c qty"><?php echo $qty;?></td>
                    <td class="item_c amount"><?php echo $amount;?></td>
            <?php endforeach; $stt++; ?>
        </tr>
        <?php endforeach; ?>
        
        <tr>
            <td class="item_c" ></td>
            <td class="item_c">Vật tư</td>
            <?php foreach ($Uid as $key_uid_login => $uid_login): ?>
                        <?php $qty_sum = isset($SUMTYPE[$uid_login]['VAT_TU']['QTY']) ? $SUMTYPE[$uid_login]['VAT_TU']['QTY'] : '0';?>
                        <?php $amount_sum = isset($SUMTYPE[$uid_login]['VAT_TU']['AMOUNT']) ?  ActiveRecord::formatCurrency($SUMTYPE[$uid_login]['VAT_TU']['AMOUNT']) : '0';?>
            <td class="item_c qty"><?php echo $qty_sum;?></td>
            <td class="item_c amount"><?php echo $amount_sum;?></td>
            <?php endforeach; ?>
        </tr>
        <?php  foreach ($COLUMN_VAT_TU as $key_materials_id => $materials_id): ?>
        <tr>
            <td class="item_c"><?php echo $stt;?></td>
            <td><?php echo isset($session['MATERIAL_MODEL_SELL'][$materials_id]['name']) ? $session['MATERIAL_MODEL_SELL'][$materials_id]['name']: '';?></td>
            <?php foreach ($Uid as $key_uid_login => $uid_login): ?>
                        <?php $qty_vattu = isset($OUTPUT[$uid_login][$materials_id]) ? ActiveRecord::formatCurrency($OUTPUT[$uid_login][$materials_id]['qty']) : '0';?>
                        <?php $amount_vattu = isset($OUTPUT[$uid_login][$materials_id]) ?  ActiveRecord::formatCurrency($OUTPUT[$uid_login][$materials_id]['amount']) : '0';?>
                    <td class="item_c qty"><?php echo $qty_vattu;?></td>
                    <td class="item_c amount"><?php echo $amount_vattu;?></td>
            <?php endforeach; $stt++; ?>
        </tr>
        <?php endforeach; ?>
        
        
         <tr>
            <td class="item_c" ></td>
            <td class="item_c" >Tổng Doanh Thu</td>
            <?php $sum_qty = isset($OUTPUT['SUM_ALL_QTY']) ? $OUTPUT['SUM_ALL_QTY'] : ''; ?>
            <?php $sum_all = isset($OUTPUT['SUM_ALL_AMOUNT']) ? ActiveRecord::formatCurrency($OUTPUT['SUM_ALL_AMOUNT']) : '';?>
            <td class="item_c qty"><?php echo $sum_qty?></td>
            <td class="item_c amount"><?php echo $sum_all;?></td>
        </tr>
    </tbody>
    
</table>
</div>        
<?php endif; ?>

<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/print-store-card.css" />
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/jquery.floatThead.min.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/underscore-min.js"></script>    

<script type="text/javascript">
    var freeHeightTable = 800;

    fnAddClassOddEven('items');
    $('.freezetablecolumns').each(function () {
        var id_table = $(this).attr('id');
        $('#' + id_table).freezeTableColumns({
            width:       <?php echo Users::WIDTH_freezetable;?>,   // required - bt 1100
            height:     500,   // required
            numFrozen: 2,     // optional
            frozenWidth: 270,   // optional
            clearWidths: true  // optional
        });
    });
    $(window).load(function () {
        var index = 1;
        $('.freezetablecolumns').each(function () {
            if(index == 1)
                $(this).closest('div.grid-view').show();
            index++;
        });
        fnAddClassOddEven('items');
    });
    $(document).ready(function () {
        $('.hm_table').floatThead(); // lỗi mất border
    });
</script>