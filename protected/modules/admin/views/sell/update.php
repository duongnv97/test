<?php
$this->breadcrumbs = array(
	$this->pluralTitle => array('index'),
        $model->code_no=>array('view','id'=>$model->id),
	$model->getAgent(),
);

$menus = array(	
    array('label' => $this->pluralTitle, 'url' => array('index')),
    array('label' => 'Xem ' . $this->singleTitle, 'url' => array('view', 'id' => $model->id)),	
    array('label' => 'Tạo Mới ' . $this->singleTitle, 'url' => array('create')),
);
$this->menu= ControllerActionsName::createMenusRoles($menus, $actions);

?>

<h1>Cập Nhật - <?php echo $model->getAgent();?>: <?php echo $model->code_no. ' - Transaction: '.$model->transaction_history_id; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>