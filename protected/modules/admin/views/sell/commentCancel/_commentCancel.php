<center><h1>Cập nhật lý do hủy đơn</h1></center>
<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'borrow-form',
	'enableAjaxValidation'=>false,
        'htmlOptions' => array('enctype' => 'multipart/form-data'),
)); ?>
    <?php echo MyFormat::BindNotifyMsg(); ?>
    <div class="row">
        <?php echo $form->label($mSell,'code_no'); ?>
        <?php echo $mSell->code_no. " - {$mSell->getCreatedDateOnly()}"  ; ?>
    </div>
    <div class="row">
        <label><?php echo "Khách hàng"; ?></label>
        <?php echo $mSell->getCustomer('first_name'). " - ĐT: {$mSell->getPhone()}"; ?>
    </div>
    <div class="row">
        <label><?php echo "Địa chỉ"; ?></label>
        <?php echo $mSell->getCustomer('address'); ?>
    </div>
    <div class="row">
        <label><?php echo "PVKH hủy"; ?></label>
        <?php echo $mSell->getStatusCancelText(); ?>
    </div>
    
    <div class="row">
        <?php echo $form->label($model,'content', ['label' => 'Tổng đài cập nhật']); ?>
        <?php //echo $form->textArea($model,'note',array('rows'=>6,'cols'=>80,"placeholder"=>"Ghi chú", 'hidden'=>'hidden'));?>
        <textarea rows="6" cols="70" placeholder="Lí do hủy" name="GasComment[content]" id="Sell_note"><?php echo $model->getContentSell(); ?></textarea>
    </div>
    
    <div class="row buttons" style="padding-left: 141px;">
        <?php $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType'=>'submit',
            'label'=>$model->isNewRecord ? 'Create' :'Save',
            'type'=>'null', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
            'size'=>'small', // null, 'large', 'small' or 'mini'
            //'htmlOptions' => array('style' => 'margin-bottom: 10px; float: right;'),
        )); ?>	
        <input class='cancel_iframe' type='button' value='Cancel'>
    </div>

    <br>
    
<?php $this->endWidget(); ?>

</div><!-- form -->



<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/autoNumeric-master/autoNumeric.js"></script>
<script>
    $(document).ready(function(){
        $('.form').find('button:submit').click(function(){
            $.blockUI({ overlayCSS: { backgroundColor: '#fff' } }); 
        });
        fnInitInputCurrency();
        parent.$.fn.yiiGridView.update("borrow-grid");
    });
</script>
