<div id="div_reward_app">
<?php 
$mCodeParner = new CodePartner();
$mCodeParner->customer_id   = $model->customer_id;
$mCodeParner->sell_id       = $model->id;
$aCodePartner = $mCodeParner->getCodePartner();
?>
<?php if(!empty($aCodePartner)): ?>
<div class="row">
    <?php echo $form->labelEx($model,'Ưu Đãi App'); ?>
    <div style="display: inline-block;">
        <?php foreach ($aCodePartner as $key => $mCodePartner):?>
            <?php 
            $id                 = $mCodePartner->id;
            $mReward            = !empty($mCodePartner->rReward) ? $mCodePartner->rReward : null;
            $mMaterial          = !empty($mReward->rMaterial) ? $mReward->rMaterial : null;
            if(empty($mReward) || empty($mMaterial)){
                continue;
            }
            $aMaterialFormat    = MyFunctionCustom::formatMaterialJsonItem($mMaterial, 0);
            $materialName       = !empty($mReward) ? $mReward->getTitle() : '';
            $material_id        = !empty($aMaterialFormat['id']) ? $aMaterialFormat['id'] : '';
            $material_no        = !empty($aMaterialFormat['materials_no']) ? $aMaterialFormat['materials_no'] : '';
            $material_name      = !empty($aMaterialFormat['name']) ? $aMaterialFormat['name'] : '';
            $material_unit      = !empty($aMaterialFormat['unit']) ? $aMaterialFormat['unit'] : '';
            $material_qty       = !empty($mReward) ? $mReward->qty : '';
            $material_unit_use  = !empty($aMaterialFormat['unit_use']) ? $aMaterialFormat['unit_use'] : '';
            $material_price     = !empty($aMaterialFormat['price']) ? $aMaterialFormat['price'] : 0;
            ?>
            <label class="reward" for="reward_app_<?php echo $id; ?>"><?php echo $materialName; ?></label>
            <input <?php echo $model->id == $mCodePartner->sell_id || (is_array($model->rewardApp) && in_array($mCodePartner->id,$model->rewardApp)) ? 'checked' : '';?> class="checkbox_reward"
                   data-reward="<?php echo $id;?>"
                   data-material_id ="<?php echo  $material_id;?>"
                   data-material_no ="<?php echo  $material_no;?>"
                   data-material_name ="<?php echo  $material_name;?>"
                   data-material_unit ="<?php echo $material_unit;?>"
                   data-material_qty ="<?php echo $material_qty;?>"
                   data-material_unit_use ="<?php echo $material_unit_use;?>"
                   data-material_price ="<?php echo $material_price;?>"
                   name="Sell[rewardApp][]" id="reward_app_<?php echo $id; ?>" value="<?php echo $id; ?>" type="checkbox">    
            <div class="clr"></div>
        <?php endforeach;?>
    </div>
</div>
<?php endif; ?>
</div>
<script>
    function setOnchangeCheckbox(){
        $(".checkbox_reward").on('change',function(){
            $reward = $(this).data('reward');
            $material_id = $(this).data('material_id');
            $material_no = $(this).data('material_no');
            $material_name = $(this).data('material_name');
            $material_unit = $(this).data('material_unit');
            $material_qty = $(this).data('material_qty');
            $material_unit_use = $(this).data('material_unit_use');
            $material_price = $(this).data('material_price');
            if(this.checked != true){
                if($('input.materials_id[value="'+$material_id+'"]').length > 0){
                    $('input.materials_id[value="'+$material_id+'"]')[0].closest('.materials_row').remove();
                }
            }else{
                var class_item = 'materials_row_'+$material_id;
                var _tr = '';
                    _tr += '<tr class="materials_row reward_'+$reward+'">';
                        _tr += '<td class="item_c order_no"></td>';
                        _tr += '<td>'+$material_no+'</td>';
                        _tr += '<td class="item_l">'+$material_name+'</td>';
                        _tr += '<td class="item_c">'+$material_unit+'</td>';
                        _tr += '<td class="item_c">';
                            _tr += '<input name="materials_qty[]" value="'+$material_qty+'" class="item_c item_qty w-30 materials_qty number_only_v1 '+class_item+'" type="text" size="6" maxlength="9">';
                            _tr += '<input name="materials_id[]" value="'+$material_id+'" class="materials_id" type="hidden">';
                            _tr += '<input name="unit_use[]" value="'+$material_unit_use+'" class="unit_use" type="hidden">';
                        _tr += '</td>';
                        _tr += '<td class="item_c ">';
                            _tr += '<input <?php echo $readonlyPrice;?> name="price[]" value="'+$material_price+'" class="item_price number_only number_only_v1 w-60" type="text" maxlength="9">';
                            _tr += '<div class="help_number"></div>';
                        _tr += '</td>';
                        _tr += '<td class="item_r item_amount  f_size_15 item_b"></td>';
                        _tr += '<td class="item_c ">';
                            _tr += '<input class="item_zero_price item_c" type="checkbox">';
                        _tr += '</td>';
                        _tr += '<td class="item_c">';
                            _tr += '<input <?php echo $readonlyGasRemain;?> name="seri[]" class="item_seri w-40 item_c" type="text" maxlength="6">';
                        _tr += '</td>';
                        _tr += '<td class="item_c">';
                            _tr += '<input <?php echo $readonlyGasRemain;?> name="kg_empty[]" class="w-40 item_c" type="text" maxlength="6">';
                        _tr += '</td>';
                        _tr += '<td class="item_c">';
                            _tr += '<input <?php echo $readonlyGasRemain;?> name="kg_has_gas[]" class="w-40 item_c" type="text" maxlength="6">';
                        _tr += '</td>';
                        _tr += '<td class="item_c"></td>';

                        _tr += '<td class="item_c last"><span class="remove_icon_only"></span></td>';
                    _tr += '</tr>';
                $('.materials_table tbody').append(_tr);
            }
            fnRefreshOrderNumber();
            bindEventForHelpNumber();
            fnCalcTotalPay();
        });
    }
    setOnchangeCheckbox();
</script>