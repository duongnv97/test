<div class="row GasCheckboxList" style=''>
    <?php echo $form->labelEx($model,'order_type'); ?>
    <?php echo $form->radioButtonList($model,'order_type', $model->getArrayOrderType(), 
                array(
                    'separator'=>"",
                    'template'=>'<li>{input}{label}</li>',
                    'container'=>'ul',
                    'class'=>'RadioOrderType' 
                )); 
    ?>
</div>
<div class='clr'></div>
<?php $display_none='display_none'; 
    if($model->order_type != Sell::ORDER_TYPE_NORMAL){
        $display_none = '';
    }
 ?>
<div class=" row <?php echo $display_none;?>">
    <?php echo $form->labelEx($model,'type_amount'); ?>
    <?php echo $form->textField($model,'type_amount',array('class'=>'ad_fix_currency OrderTypeAmount')); ?>
    <?php echo $form->error($model,'type_amount'); ?>
</div>

<div class=" row display_none">
    <label>&nbsp;</label>
    <em class="float_l hight_light item_b">Quy tắc nhập chiết khấu:<br>
        1. Không có quà khuyến mãi, dùng chiết khấu mặc định theo vùng (20,000 hoặc 25,000) thì bỏ trống không nhập<br>
        2. Không có quà khuyến mãi, chỉnh sửa chiết khấu khác mặc định thì nhập số tiền chiết khấu<br>
        2. Có quà khuyến mãi, chỉnh sửa chiết khấu khác mặc định thì nhập số tiền chiết khấu<br>
        3. Chỉnh sửa chiết khấu khác mặc định thì nhập số tiền chiết khấu<br>
    </em>
</div>
<div class="clr"></div>

<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/autoNumeric-master/autoNumeric.js"></script>
<script>
    $(function(){
        bindRadioEvent();
        fnInitInputCurrency();
        bindRadioOrderType();
        bindRadioDiscountType();
    });
    
    $(window).load(function(){
    });
    
    function bindRadioOrderType(){
        $('.RadioOrderType').click(function(){
            var priceVoBinh = <?php echo $model->getPriceVoBinh();?>;
            if($(this).is(':checked')){
                if($(this).val() == <?php echo Sell::ORDER_TYPE_NORMAL;?>){
                    $('.OrderTypeAmount').val(0).trigger('change');
                    $('.OrderTypeAmount').closest('.row').addClass('display_none');
                }else{
                    $('.OrderTypeAmount').val(priceVoBinh).closest('.row').removeClass('display_none');
                    $('.OrderTypeAmount').val(priceVoBinh).trigger('change');
                }
                var label = $(this).closest('li').find('label:first').text();
                $('.type_amount_text').html(label);
            }
        });
    }
    
    function bindRadioDiscountType(){
        $('.RadioDiscountType').click(function(){
            var WrapDiscountType = $(this).closest('.WrapDiscountType');
            if($(this).is(':checked')){
                if($(this).val() == <?php echo Sell::DISCOUNT_DEFAULT;?>){
                    WrapDiscountType.find('.WrapDiscountAmount').addClass('display_none');
                    $('.discount_amount_hide').val($('.discount_default_hide').val());
                    WrapDiscountType.find('.amount_discount').val(0);
                    updateDiscountText();
                }else{
                    WrapDiscountType.find('.WrapDiscountAmount').removeClass('display_none');
                    $('.discount_amount_hide').val(0);
                    updateDiscountText();
                }
            }
        });
    }
    
    function getOrderType(){
        var res = 0;
        $('.RadioOrderType').each(function(){
            if($(this).is(':checked')){
                res = $(this).val();
                return false;
            }
        });
        return res*1;
    }
    
    
</script>    