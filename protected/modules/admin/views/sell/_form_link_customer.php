<?php // if($model->isNewRecord): ?>
<?php if(1): ?>
<div class='clr'></div>
<?php 
    $transaction_id     = isset($_GET['transaction_id']) ? $_GET['transaction_id'] : 0;
    if(!$model->isNewRecord):
        $transaction_id = $model->transaction_history_id;
    endif;
    $phone_number       = isset($_GET['phone_number']) ? $_GET['phone_number'] : '';
    $call_uuid          = isset($_GET['call_uuid']) ? $_GET['call_uuid'] : 0;
    $aParamCreate       = ['transaction_id'=> $transaction_id, 'phone_number'=>$phone_number, 'call_uuid'=>$call_uuid];
    $aParamUpdate       = ['customer_id'=> $model->customer_id, 'transaction_id'=> $transaction_id, 'sell_id' => $model->id];
    $model->note       = isset($_GET['auto_note']) ? 'Trả thẻ' : $model->note;

?>
<?php if(empty($model->customer_id)): ?>
<div class="row">
    <label>&nbsp;</label>
    <div class="add_new_item float_l" style="padding-left: 0;"><a class="IframeCreateCustomer" style="padding-left: 0;" href="<?php echo Yii::app()->createAbsoluteUrl('admin/ajax/iframeCreateCustomerHgd', $aParamCreate) ;?>">Tạo Mới Khách Hàng</a><em> (Nếu không tìm kiếm được khách hàng)</em></div>
</div>
<?php endif; ?>

<?php if(!empty($model->customer_id)): ?>
<div class="row">
    <label>&nbsp;</label>
    <div class="add_new_item float_l" style="padding-left: 0;"><a class="IframeCreateCustomer" style="padding-left: 0;" href="<?php echo Yii::app()->createAbsoluteUrl('admin/ajax/iframeUpdateCustomerHgd', $aParamUpdate) ;?>">Cập Nhật Thông Tin Khách Hàng</a><em></em></div>
</div>
<?php endif; ?>

<div class='clr'></div>
<?php endif; ?>