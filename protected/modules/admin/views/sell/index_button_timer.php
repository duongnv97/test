<?php
    $LinkGeneral   = Yii::app()->createAbsoluteUrl('admin/sell/index');
    $LinkVersionOs = Yii::app()->createAbsoluteUrl('admin/sell/index', ['type'=> Sell::TYPE_TIMER]);
?>
<a class='btn_cancel f_size_14 <?php echo !isset($_GET['type']) ? "active":"";?>' href="<?php echo $LinkGeneral;?>">Toàn hệ thống</a>
<a class='btn_cancel f_size_14 <?php echo isset($_GET['type']) && $_GET['type'] == Sell::TYPE_TIMER ? "active":"";?>' href="<?php echo $LinkVersionOs;?>">Đơn hẹn giờ</a>