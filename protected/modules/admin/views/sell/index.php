<?php
$this->breadcrumbs=array(
	$this->singleTitle,
);

$allowCommentCancel = true;

$menus=array(
    array('label'=>"Create $this->singleTitle", 'url'=>array('create')),
//    array('label'=> 'Xuất Excel Danh Sách Hiện Tại',
//            'url'=>array('ExportExcel', 'type'=>1), 
//            'htmlOptions'=>array('class'=>'export_excel ','label'=>'Xuất Excel Tây Nguyên')),
);

$mCronExcel = new CronExcel();
$session=Yii::app()->session;
if( ($mCronExcel->canDownload() && count($session['LIST_AGENT_OF_USER']) < 1) // check user not limit, chi nhung user dc phep moi cho dowload
    || count($session['LIST_AGENT_OF_USER']) > 1 // allow user limit agent dowload
){
    $menus[] = array('label'=> 'Xuất Excel Danh Sách Hiện Tại',
            'url'=>array('ExportExcel'), 
            'htmlOptions'=>array('class'=>'export_excel ','label'=>'Xuất Excel'));
}

$this->menu= ControllerActionsName::createMenusRoles($menus, $actions);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('sell-grid', {
                url : $(this).attr('action'),
		data: $(this).serialize()
	});
	return false;
});
");

Yii::app()->clientScript->registerScript('ajaxupdate', "
$('#sell-grid a.ajaxupdate').live('click', function() {
    $.fn.yiiGridView.update('sell-grid', {
        type: 'POST',
        url: $(this).attr('href'),
        success: function() {
            $.fn.yiiGridView.update('sell-grid');
        }
    });
    return false;
});
");
$cRole = MyFormat::getCurrentRoleId();
$aRoleShow = [ROLE_CALL_CENTER, ROLE_DIEU_PHOI, ROLE_ADMIN];
?>
<?php echo MyFormat::BindNotifyMsg(); ?>
<?php include "index_button.php"; ?>
<div class="form">
<h1>Danh Sách <?php echo $this->pluralTitle; ?><?php include 'index_button_timer.php'; ?></h1>
</div>
<?php if(in_array($cRole, $aRoleShow)): ?>
    <?php $this->widget('ReportEmployeeUpholdWidget', array('uId' => MyFormat::getCurrentUid(),'needMore' => ['type'=>'table']));  // ?> 
<?php endif; ?>
<?php if($cRole == ROLE_ADMIN): ?>
    <?php // $this->widget('ReportEmployeeUpholdWidget', array('needMore' => ['type'=>'table'])); ?>
<?php endif; ?>

<?php $this->widget('ListCronExcelWidget', array('model'=>$model)); ?>
<?php if(!isset($_GET['Sell'])): ?>
    <?php echo CHtml::link('Tìm Kiếm Nâng Cao','#',array('class'=>'search-button')); ?>
    <div class="search-form" style="">
    <?php $this->renderPartial('_search',array(
            'model'=>$model,
    )); ?>
    </div><!-- search-form -->
<?php endif; ?>

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'sell-grid',
	'dataProvider'=>$model->search(),
        'afterAjaxUpdate'=>'function(id, data){ fnUpdateColorbox();}',
        'template'=>'{pager}{summary}{items}{pager}{summary}',
//    'itemsCssClass' => 'items custom_here',
//    'htmlOptions'=> array('class'=>'grid-view custom_here'),
        'pager' => array(
            'maxButtonCount'=>  CmsFormatter::$PAGE_MAX_BUTTON,
        ),
//	'enableSorting' => false,
	//'filter'=>$model,
	'columns'=>array(
        array(
            'header' => 'S/N',
            'type' => 'raw',
            'value' => '$this->grid->dataProvider->pagination->currentPage * $this->grid->dataProvider->pagination->pageSize + ($row+1)',
            'headerHtmlOptions' => array('width' => '10px','style' => 'text-align:center;'),
            'htmlOptions' => array('style' => 'text-align:center;')
        ),
        array(
            'name'=>'created_date_only',
//            'value' => '$data->getDateSellGrid().$data->getSourceText()',
            'value' => '$data->getDateSellGrid()."<br>".$data->getCallEndTimeWeb()."<br><br><b>".$data->getPtttCode()."</b>".$data->getDisplayErrors()."<br>".$data->getCustomerNewOld()',
            'type'=>'html',
            'htmlOptions' => array('style' => 'width:50px;'),
        ),
        array(
            'name'=>'code_no',
            'type'=>'html',
            'value'=>'$data->code_no.$data->getHighPriceText()."<br><br>Ngày tạo: ".$data->getCreatedDate()',
            'htmlOptions' => array('style' => 'width:50px;'),
        ),
        array(
            'name'=>'agent_id',
            'type'=>'raw',
            'value'=>'"<b>Tổng: ".$data->getGrandTotal(true)."</b><br>".$data->getAgent()',
        ),
        
        array(
            'name'=>'customer_id',
            'type'=>'html',
            'value'=>'$data->getCustomer()',
            'htmlOptions' => array('style' => 'width:100px;'),
        ),
        array(
            'name'=>'type_customer',
            'type'=>'raw',
            'value'=>'$data->getTypeCustomer()."<br>".$data->getArrayPlatformText()',
            'htmlOptions' => array('style' => 'width:30px;'),
        ),
        array(
            'header'=>'Phone',
            'type'=>'raw',
//            'value'=>'$data->getCustomerPhone()',
            'value'=>'$data->getInfoAccounting()."<br>".$data->getCallCenter()."<br>".$data->getUrlViewCall()',
        ),
//        array(
//            'name'=>'uid_login',
//            'value'=>'$data->getUidLogin()',
//        ),
//        array(
//            'name'=>'employee_maintain_id',
//            'value'=>'$data->getEmployeeMaintain()',
//        ),
        
//        array(
//            'header'=>'Phone',
//            'type'=>'html',
//            'value'=>'$data->getCustomer("phone")',
//        ),
        
//        array(
//            'header'=>'Chi Tiết',
//            'type'=>'html',
//            'value'=>'$data->getSummaryTable()',
//        ),
        array(
            'header'=>'Gas + VT Bán',
            'type'=>'raw',
            'value'=>'$data->getInfoGas()."".$data->getImageViewGrid()."<br>".$data->getDeliveryTimerGrid()."<br>".$data->getStatusCancelText()."<br>".$data->getReasonCanCel()',
        ),
        array(
            'header'=>'SL',
            'type'=>'html',
            'value'=>'$data->viewGasQty',
            'htmlOptions' => array('style' => 'text-align:center;'),
        ),
        array(
            'header'=>'Giá',
            'type'=>'html',
            'value'=>'$data->viewGasPrice',
            'htmlOptions' => array('style' => 'text-align:right;'),
        ),
        array(
            'header'=>'TT',
            'type'=>'html',
            'value'=>'$data->viewGasAmount',
            'htmlOptions' => array('style' => 'text-align:right;'),
        ),
        array(
            'header'=>'CK',
            'type'=>'html',
            'value'=>'$data->viewDiscount',
            'htmlOptions' => array('style' => 'text-align:right;'),
        ),
        array(
            'header'=>'Vỏ',
            'type'=>'html',
            'value'=>'$data->viewVoName',
        ),
        array(
            'header'=>'SL',
            'type'=>'html',
            'value'=>'$data->viewVoQty',
            'htmlOptions' => array('style' => 'text-align:center;'),
        ),
        array(
            'header'=>'KM',
            'type'=>'html',
            'value'=>'$data->viewKmName.$data->getPromotionCodeNo()',
        ),
        array(
            'header'=>'SL',
            'type'=>'html',
            'value'=>'$data->viewKmQty',
            'htmlOptions' => array('style' => 'text-align:center;'),
        ),
//        array(
//            'name' => 'created_date',
//            'type' => 'Datetime',
//            'htmlOptions' => array('style' => 'width:50px;'),
//        ),
        array(
            'name' => 'status',
            'type' => 'raw',
            'value' => '$data->getStatus()."<br>".$data->getTimeOverWebView()',
            'htmlOptions' => array('style' => 'width:20px; text-align:center;')
        ),
        array(
//            'htmlOptions' => array('style' => 'width:30px;'),// Sep1717 htmlOptions làm mất biến button-column
            'header' => 'Actions',
            'class'=>'CButtonColumn',
            'template'=> ControllerActionsName::createIndexButtonRoles($actions, array('view','update','delete','audit','commentCancel')),
            'buttons'=>array(
                'update'=>array(
                    'visible'=> '$data->canUpdateWeb()',
                ),
                'delete'=>array(
                    'visible'=> 'GasCheck::canDeleteData($data)',
                ),
                'audit'=>array(
                    'label'=>'Xác nhận tình trạng hủy của đơn hàng',
                    'imageUrl'=>Yii::app()->theme->baseUrl . '/images/add1.png',
                    'options'=>array('class'=>'audit'),
                    'url'=>'Yii::app()->createAbsoluteUrl("admin/sell/audit", array("id"=>$data->id) )',
                     'visible' => '$data->canAuditWeb()',
                ),
                'commentCancel'=>array(
                    'label'=>'lí do hủy của đơn hàng',
                    'imageUrl'=>Yii::app()->theme->baseUrl . '/images/add2.png',
                    'options'=>array('class'=>'commentCancel'),
                    'url'=>'Yii::app()->createAbsoluteUrl("admin/sell/commentCancel", array("belongId"=>$data->id) )',
                    'visible' => '$data->canCommentReasonCancel()',
                ),
            ),
        ),
	),
)); ?>

<script>
$(document).ready(function() {
    fnUpdateColorbox();
});

function fnUpdateColorbox(){   
    fixTargetBlank();
    fnAddSumTr();
    $(".gallery").colorbox({iframe:true,innerHeight:'1100', innerWidth: '1000',close: "<span title='close'>close</span>"});
    fnShowhighLightTr();
    $('body').on('click', '.export_excel', function(){
//        $(this).remove();
    });
    // NGHIA003 audit
    $(".audit").colorbox({iframe:true,innerHeight:'550', innerWidth: '850',close: "<span title='close'>close</span>"});
    $(".commentCancel").colorbox({
        iframe:true,
        innerHeight:'350', 
        innerWidth: '850',
        close: "<span title='close'>close</span>",
        onClosed: function () { // update view when close colorbox
                $.fn.yiiGridView.update('sell-grid');
//                location.reload();
            }
        });
}


function fnAddSumTr(){
    var tr = '';
    var sumGasQty       = 0;
    var sumGasAmount    = 0;
    var sumVoQty        = 0;
    var sumKmQty        = 0;
    var sumDiscountQty  = 0;
    $('.sumGasQty').each(function(){
        if($.trim($(this).text())!='')
            sumGasQty += parseFloat(''+$(this).text());
    });
    $('.sumGasAmount').each(function(){
        if($.trim($(this).text())!='')
            sumGasAmount += parseFloat(''+$(this).text());
    });
    $('.sumVoQty').each(function(){
        if($.trim($(this).text())!='')
            sumVoQty+= parseFloat(''+$(this).text());
    });
    $('.sumKmQty').each(function(){
        if($.trim($(this).text())!='')
            sumKmQty+= parseFloat(''+$(this).text());
    });
    $('.sumDiscountQty').each(function(){
        if($.trim($(this).text())!='')
            sumDiscountQty+= parseFloat(''+$(this).text());
    });
    
    sumGasQty       = Math.round(sumGasQty * 100) / 100;
    sumGasAmount    = Math.round(sumGasAmount * 100) / 100;
    sumVoQty        = Math.round(sumVoQty * 100) / 100;
    sumKmQty        = Math.round(sumKmQty * 100) / 100;
    sumDiscountQty  = Math.round(sumDiscountQty * 100) / 100;
    
    tr +='<tr class="f_size_12 odd sum_page_current">';
        tr +='<td class="item_r item_b" colspan="8">Tổng Cộng</td>';
        tr +='<td class="item_r item_b">'+commaSeparateNumber(sumGasQty)+'</td>';
        tr +='<td></td>';
        tr +='<td class="item_r item_b">'+commaSeparateNumber(sumGasAmount)+'</td>';
        tr +='<td class="item_r item_b">'+commaSeparateNumber(sumDiscountQty)+'</td>';
        tr +='<td></td>';
        tr +='<td class="item_r item_b">'+commaSeparateNumber(sumVoQty)+'</td>';
        tr +='<td></td>';
        tr +='<td class="item_r item_b">'+commaSeparateNumber(sumKmQty)+'</td>';
        tr +='<td></td><td></td>';
    tr +='</tr>';
    
    if($('.sum_page_current').size()<1){
        $('.items tbody').prepend(tr);
        $('.items tbody').append(tr);
    }
}

</script>

</script>