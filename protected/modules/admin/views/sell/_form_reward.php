<h3 id="titleReward" class='title-info'><?php 
$mRewardPoint = new RewardPoint();
$title = $mRewardPoint->getTitleRewardWeb($model->customer_id);
echo $title;
?></h3>
<div class="clr"></div>
<div class="row BoxCustomer">
<?php echo $form->labelEx($model,'Quà tặng'); ?>
<?php echo $form->hiddenField($model,'reward_id', array('class'=>'')); ?>
<?php
    $url = Yii::app()->createAbsoluteUrl('admin/ajax/searchReward');
    // widget auto complete search user customer and supplier
    $aData = array(
            'model'=>$model,
            'field_customer_id'=>'reward_id',
            'url'=> $url,
            'name_relation_user'=>'rReward',
            'placeholder'=> 'Nhập tên quà tặng',
            'ClassAdd' => 'w-400',
            'ShowTableInfo' => '0',
            'field_autocomplete_name' => 'autocomplete_name_reward',
            'fnSelectCustomerV2' => true,
        );
    $this->widget('ext.SpaAutocompleteCustomer.SpaAutocompleteCustomer',
        array('data'=>$aData));
    ?>
    <?php echo $form->error($model,'reward_id'); ?>
</div>
<?php include '_form_reward_app.php'; ?>
<script>
<?php if(!empty($model->rReward)): ?>
    var remove_div = '<span class=\'remove_row_item\' onclick=\'fnRemoveName(this, \"#Sell_autocomplete_name_reward\", \"#Sell_reward_id\")\'></span>';
    $('#Sell_autocomplete_name_reward').val('<?php echo $model->rReward->title . ' - '.$model->rReward->point_apply.' điểm';?>').attr('readonly',true).after(remove_div);
<?php endif;?>
    function fnCallSomeFunctionAfterSelectV2(ui, idField, idFieldCustomerID){
        var class_item = 'materials_row_'+ui.item.material.id;
        var _tr = '';
            _tr += '<tr class="materials_row">';
                _tr += '<td class="item_c order_no"></td>';
                _tr += '<td>'+ui.item.material.materials_no+'</td>';
                _tr += '<td class="item_l">'+ui.item.material.name+'</td>';
                _tr += '<td class="item_c">'+ui.item.material.unit+'</td>';
                _tr += '<td class="item_c">';
                    _tr += '<input name="materials_qty[]" value="'+ui.item.qty+'" class="item_c item_qty w-30 materials_qty number_only_v1 '+class_item+'" type="text" size="6" maxlength="9">';
                    _tr += '<input name="materials_id[]" value="'+ui.item.material.id+'" class="materials_id" type="hidden">';
                    _tr += '<input name="unit_use[]" value="'+ui.item.material.unit_use+'" class="unit_use" type="hidden">';
                _tr += '</td>';
                _tr += '<td class="item_c ">';
                    _tr += '<input <?php echo $readonlyPrice;?> name="price[]" value="'+ui.item.material.price+'" class="item_price number_only number_only_v1 w-60" type="text" maxlength="9">';
                    _tr += '<div class="help_number"></div>';
                _tr += '</td>';
                _tr += '<td class="item_r item_amount  f_size_15 item_b"></td>';
                _tr += '<td class="item_c ">';
                    _tr += '<input class="item_zero_price item_c" type="checkbox">';
                _tr += '</td>';
                _tr += '<td class="item_c">';
                    _tr += '<input <?php echo $readonlyGasRemain;?> name="seri[]" class="item_seri w-40 item_c" type="text" maxlength="6">';
                _tr += '</td>';
                _tr += '<td class="item_c">';
                    _tr += '<input <?php echo $readonlyGasRemain;?> name="kg_empty[]" class="w-40 item_c" type="text" maxlength="6">';
                _tr += '</td>';
                _tr += '<td class="item_c">';
                    _tr += '<input <?php echo $readonlyGasRemain;?> name="kg_has_gas[]" class="w-40 item_c" type="text" maxlength="6">';
                _tr += '</td>';
                _tr += '<td class="item_c"></td>';
                
                _tr += '<td class="item_c last"><span class="remove_icon_only"></span></td>';
            _tr += '</tr>';
        $('.materials_table tbody').append(_tr);
        fnRefreshOrderNumber();
        bindEventForHelpNumber();
        fnCalcTotalPay();
    }
</script>

