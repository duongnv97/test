<?php
$this->breadcrumbs=array(
	$this->pluralTitle=>array('index'),
	'Tạo Mới',
);

$menus = array(		
        array('label'=>"$this->singleTitle Management", 'url'=>array('index')),
);
$this->menu= ControllerActionsName::createMenusRoles($menus, $actions);
?>
<?php if(isset($_GET['transaction_id'])): ?>
<div class="form">
    <?php
        $LinkNormal = Yii::app()->createAbsoluteUrl('admin/sell/create', ['transaction_id'=> $_GET['transaction_id'], 'free_transaction'=>1]);
    ?>
    <a class='btn_cancel f_size_14 float_r' style="margin-right: 100px;" target="_blank" href="<?php echo $LinkNormal;?>">Nhả đơn hàng app cho người khác</a>
</div>
<?php endif; ?>
<h1>Tạo Mới <?php echo $this->singleTitle; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>