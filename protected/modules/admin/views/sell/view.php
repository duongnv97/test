<?php
$this->breadcrumbs=array(
    $this->pluralTitle => array('index'),
    "[ TransId: $model->transaction_history_id ] " . $this->singleTitle . ": ".$model->getCustomer(),
);

$menus = array(
	array('label'=>"$this->pluralTitle Management", 'url'=>array('index')),
	array('label'=>"Tạo Mới $this->singleTitle", 'url'=>array('create')),
	array('label'=>"Cập Nhật $this->singleTitle", 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>"Xóa $this->singleTitle", 'url'=>array('delete'), 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Bạn chắc chắn muốn xóa ?')),
);
$this->menu= ControllerActionsName::createMenusRoles($menus, $actions);
?>

<!--DungNT close Aug1619 <p>Xem: <?php echo $this->singleTitle.": ".$model->getCustomer(); ?></p>-->
<?php echo $model->getHtmlCountNotify(); ?>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
            array(
                'name'=>'status',
                'type'=>'raw',
                'value'=> $model->getStatus().'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;--&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'.$model->getUrlViewCall(),
            ),
            array(
                'label'=>'Loại bán hàng',
                'type' => 'raw',
                'value'=> strip_tags($model->getSourceText()) . " - {$model->getPromotionCodeNo()}  - CompleteTime {$model->complete_time}",
            ),
            array(
                'name'=>'code_no',
                'type'=>'raw',
                'value'=>$model->getCodeNo() . ' - TransactionHistoryId: '.$model->transaction_history_id . " CustomerId: {$model->customer_id} PvkhId: $model->employee_maintain_id",
            ),
            array(
                'name'=>'agent_id',
                'value'=>$model->getAgent(),
            ),
            array(
                'name'=>'created_date_only',
                'type' => 'date',
            ),
            array(
                'name'=>'delivery_timer',
                'value' => $model->getDeliveryTimer(),
            ),
            array(
                'name'=>'uid_login',
                'value'=>$model->getUidLogin(),
            ),
            array(
                'name'=>'employee_maintain_id',
                'value'=>$model->getEmployeeMaintain(),
            ),
            array(
                'name'=>'customer_id',
                'type'=>'raw',
                'value'=>$model->getCustomer().'<br>'.$model->getInfoAccounting(),
            ),
            array(
                'label'=>'Chi Tiết',
                'type'=>'html',
                'value'=>$model->getSummaryTable(),
            ),
            array(
                'name'=>'note',
                'type'=>'html',
            ),
            array(
                'name'=>'pttt_code',
            ),
            array(
                'name' => 'created_date',
                'type' => 'Datetime',
                'htmlOptions' => array('style' => 'width:50px;')
            ),
            array(
                'label' =>'Lịch sử giao nhận',
                'type' =>'raw',
                'value' => $model->getEventLog(),
            ),
	),
)); ?>

<?php
    $mTransactionHistory    = $model->rTransactionHistory;
    $loadJavaScript         = false;
?>
<div class="grp-cot clearfix">
    <div class="sk-col-md-6">
        <h3 class='title-info'>Vị trí đặt hàng</h3>
        <div class="clr"></div>
        <?php $this->widget('MapCustomerBookWidget', ['mTransactionHistory' => $mTransactionHistory]); ?>
    </div>
    <?php if($model->mEventComplete): ?>
    <?php 
        $mTransactionHistoryEventDone = new TransactionHistory();
        $mTransactionHistoryEventDone->google_map   = $model->mEventComplete->google_map;
        $mTransactionHistoryEventDone->source       = Sell::SOURCE_APP;
        $mTransactionHistoryEventDone->address      = '';
        if($model->source != Sell::SOURCE_APP){
            $loadJavaScript         = true;
        }
    ?>
    <div class="sk-col-md-6" style="padding-left: 10px;">
        <h3 class='title-info'>Vị trí hoàn thành đơn hàng</h3>
        <div class="clr"></div>
        <?php $this->widget('MapCustomerBookWidget', ['mTransactionHistory' => $mTransactionHistoryEventDone, 'loadJavaScript' => $loadJavaScript]); ?>
    </div>
    <?php endif; ?>
</div>