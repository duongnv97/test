<?php 
$readonlyPrice              = 'readonly="1"';
$readonlyGasRemain          = 'readonly="1"';
$classDisplay               = 'display_none';
$classDisplayStatus         = 'display_none';
$classDisplayPromotionApp   = $checkUpdateHgd = '';

if(in_array($cUid, GasConst::getArrUidUpdateAll()) || in_array($cUid, $model->getUserAnhHieu()) || $cRole == ROLE_ADMIN): 
    $classDisplay       = '';
    $classDisplayStatus = '';
    $readonlyPrice      = '';
    $readonlyGasRemain = '';
    $today = date('Y-m-d');
    $dateAutoCheck = MyFormat::modifyDays($today, 1, '-');
//    if(MyFormat::compareTwoDate($dateAutoCheck, MyFormat::dateConverDmyToYmd($model->created_date_only))){
    if(1){// luon cap nhat khi Audit sua don hang
        // Jan0818 tự động check update hộ GĐ khi Update những đơn cách quá 2 ngày so với ngày hiện tại
        // nếu khi sửa quên không check là sai kho
        $checkUpdateHgd     = 'checked';
    }
endif;
if($cRole == ROLE_ADMIN):
    $readonlyGasRemain = '';
endif;

if($model->status == Sell::STATUS_CANCEL && 
    ($model->status_cancel == Transaction::CANCEL_AGENT_WRONG ||$model->source == Sell::SOURCE_APP)
){
    $classDisplayStatus = '';
}

if($model->source == Sell::SOURCE_APP){
    $classDisplayPromotionApp   = ' display_none';
}
?>

<!--<div class="row float_l f_size_15 <?php echo $classDisplay;?>">-->
<div class="row float_l f_size_15">
    <label>&nbsp;</label>
    <?php echo $form->checkBox($model,'high_price',array('class'=>'float_l')); ?>
    <?php echo $form->labelEx($model,'high_price',array('class'=>'checkbox_one_label w-300 l_padding_10', 'style'=>'padding-top:3px;')); ?>
</div>          
<div class="clr"></div>

<div class="row">
    <?php echo $form->labelEx($model,'delivery_timer'); ?>
    <?php Yii::import('application.extensions.CJuiDateTimePicker.CJuiDateTimePicker');
    $this->widget('CJuiDateTimePicker',array(
        'model'=>$model, //Model object
        'attribute'=>'delivery_timer', //attribute name
        'mode'=>'datetime', //use "time","date" or "datetime" (default)
        'language'=>'en-GB',
        'options'=>array(
            'minDate'=> '0',
            'maxDate'=> '5',
            'showAnim'=>'fold',
            'showButtonPanel'=>true,
            'autoSize'=>true,
            'dateFormat'=>'dd/mm/yy',
            'timeFormat'=>'hh:mm:ss',
            'width'=>'120',
            'separator'=>' ',
            'showOn' => 'button',
            'buttonImage'=> Yii::app()->theme->baseUrl.'/admin/images/icon_calendar_r.gif',
            'buttonImageOnly'=> true,
            'changeMonth' => true,
            'changeYear' => true,
            //                'regional' => 'en-GB'
        ),
        'htmlOptions' => array(
            'style' => 'width:180px;',
            'readonly'=>'readonly',
            'class'=>'InputDeliveryTimer',
        ),
    ));
    ?>
    <input class="RemoveDeliveryTimer" name="remove_delivery_timer" id="remove_delivery_timer" type="checkbox">
    <span>Xóa hẹn giờ</span>
    <?php echo $form->error($model,'delivery_timer'); ?>
</div>

<div class="row ">
    <?php echo $form->labelEx($model,'agent_id'); ?>
    <?php echo $form->hiddenField($model,'agent_id', array('class'=>'')); ?>
<?php
        // 1. limit search kh của sale
        $url = Yii::app()->createAbsoluteUrl('admin/ajax/search_by_role', array('role'=> ROLE_AGENT));
        // widget auto complete search user customer and supplier
        $aData = array(
            'model'=>$model,
            'field_customer_id'=>'agent_id',
            'url'=> $url,
            'name_relation_user'=>'rAgent',
            'ClassAdd' => 'w-300',
            'field_autocomplete_name' => 'autocomplete_name_4',
            'placeholder'=>'Nhập mã hoặc tên đại lý',
            'fnSelectCustomer'=>'fnSelectAgent',
        );
        $this->widget('ext.SpaAutocompleteCustomer.SpaAutocompleteCustomer',
            array('data'=>$aData));
        ?>
    <?php echo $form->error($model,'agent_id'); ?>
</div>

<div class="row <?php echo $classDisplayStatus;?>">
    <?php echo $form->labelEx($model,'status'); ?>
    <?php echo $form->dropDownList($model,'status', $model->getArrayStatus(),array('class'=>'w-300')); ?>
    <?php echo $form->dropDownList($model,'status_cancel', $model->getArrayStatusCancel(),array('class'=>'w-200','empty'=>'Chọn lý do hủy')); ?>
    <?php echo $form->error($model,'status'); ?>
</div>
<div class="row">
    <?php echo $form->labelEx($model,'created_date_only'); ?>
    <?php 
        $this->widget('zii.widgets.jui.CJuiDatePicker', array(
            'model'=>$model,        
            'attribute'=>'created_date_only',
            'language'=>'en-GB',
            'options'=>array(
                'showAnim'=>'fold',
                'dateFormat'=> ActiveRecord::getDateFormatJquery(),
//                            'minDate'=> '0',
                'maxDate'=> '0',
                'changeMonth' => true,
                'changeYear' => true,
                'showOn' => 'button',
                'buttonImage'=> Yii::app()->theme->baseUrl.'/admin/images/icon_calendar_r.gif',
                'buttonImageOnly'=> true,                                
            ),        
            'htmlOptions'=>array(
                'class'=>'w-16',
                'style'=>'height:20px;',
                'readonly'=>'readonly',
            ),
        ));
    ?>     		
    <?php echo $form->error($model,'created_date_only'); ?>
</div>
<div class="BoxCustomerLinkUpdate">
    <?php include "_form_link_customer.php"; ?>
</div>
<div class="row BoxCustomer">
<?php echo $form->labelEx($model,'customer_id'); ?>
<?php echo $form->hiddenField($model,'customer_id', array('class'=>'')); ?>
<?php
    // 1. limit search kh của sale
    $url = Yii::app()->createAbsoluteUrl('admin/ajax/searchHgd');
    // widget auto complete search user customer and supplier
    $aData = array(
        'model'=>$model,
        'field_customer_id'=>'customer_id',
        'url'=> $url,
        'name_relation_user'=>'rCustomer',
        'ClassAdd' => 'w-300',
        'fnSelectCustomer'=>'fnSelectCustomer',
    );
    $this->widget('ext.SpaAutocompleteCustomer.SpaAutocompleteCustomer',
        array('data'=>$aData));
    ?>
    <?php echo $form->error($model,'customer_id'); ?>
</div>
<div class="row">
    <?php echo $form->labelEx($model,'phone'); ?>
    <?php echo $form->textField($model,'phone',array('class'=>'w-300 number_only_v1 item_l','maxlength'=>12,)); ?>
    <?php echo $form->error($model,'phone'); ?>
</div>
<?php if($model->source == Sell::SOURCE_APP): ?>
<div class="row add_new_item"  style="padding-left:0;">
    <?php echo $form->labelEx($model,'address', ['label'=>'Địa chỉ google map']); ?>
    <a href='javascript:;' class="f_size_15" style="padding-left:0;"><?php echo $model->address; ?></a>
</div>
<?php endif; ?>

<div class="row">
    <label>&nbsp;</label>
    <div class="add_new_item float_l" style="padding-left: 0;">
        <a class="AutoNoteTraThe" style="padding-left: 0;" href="javascript:;">Xác nhận lại địa chỉ và đúng loại gas</a>
        <br><a class="AutoNoteTraThe" style="padding-left: 0;" href="javascript:;">Giao nhanh giúp khách</a>
        <br><a class="AutoNoteTraThe" style="padding-left: 0;" href="javascript:;">Chưa báo giá , báo quà</a>
        <br><a class="AutoNoteTraThe" style="padding-left: 0;" href="javascript:;">Lấy bình mới giúp khách</a>
        <br><a class="AutoNoteTraThe" style="padding-left: 0;" href="javascript:;">KH đã đặt App lần 1</a>
        <br><a class="AutoNoteTraThe" style="padding-left: 0;" href="javascript:;">Trả thẻ</a>
    </div>
</div>
<div class='clr'></div>
<div class="row">
    <?php echo $form->labelEx($model,'note'); ?>
    <?php echo $form->textArea($model,'note',array('class'=>'w-300 h_50 NoteTraThe')); ?>
    <?php echo $form->error($model,'note'); ?>
</div>

<div class="row display_none">
<?php echo $form->labelEx($model,'uid_login'); ?>
<?php echo $form->hiddenField($model,'uid_login', array('class'=>'')); ?>
<?php
        // 1. limit search kh của sale
        $url = Yii::app()->createAbsoluteUrl('admin/ajax/search_by_role', array('role'=> implode(',', Sell::$ROLE_UID_LOGIN)));
        // widget auto complete search user customer and supplier
        $aData = array(
            'model'=>$model,
            'field_customer_id'=>'uid_login',
            'url'=> $url,
            'name_relation_user'=>'rUidLogin',
            'ClassAdd' => 'w-300',
            'field_autocomplete_name' => 'autocomplete_name_3',
            'placeholder'=>'Nhập mã NV hoặc tên',
        );
        $this->widget('ext.SpaAutocompleteCustomer.SpaAutocompleteCustomer',
            array('data'=>$aData));
        ?>
    <?php echo $form->error($model,'uid_login'); ?>
</div>


<div class="RowEmployeeMaintain row <?php // echo $classDisplay;?>">
<?php echo $form->labelEx($model,'employee_maintain_id'); ?>
<?php echo $form->hiddenField($model,'employee_maintain_id', array('class'=>'')); ?>
<?php
        // 1. limit search kh của sale
        $url = Yii::app()->createAbsoluteUrl('admin/ajax/search_by_role', array('role'=> implode(',', Sell::$ROLE_EMPLOYEE_SEARCH_AUTOCOMPLETE)));
        // widget auto complete search user customer and supplier
        $aData = array(
            'model'=>$model,
            'field_customer_id'=>'employee_maintain_id',
            'url'=> $url,
            'name_relation_user'=>'rEmployeeMaintain',
            'ClassAdd' => 'w-300',
            'field_autocomplete_name' => 'autocomplete_name_2',
            'placeholder'=>'Nhập mã NV hoặc tên',
        );
        $this->widget('ext.SpaAutocompleteCustomer.SpaAutocompleteCustomer',
            array('data'=>$aData));
        ?>
    <?php echo $form->error($model,'employee_maintain_id'); ?>
</div>
<div class="clr"></div> 
<div class="row <?php echo $classDisplay;?>">
    <label>&nbsp;</label>
    <?php echo $form->checkBox($model,'isChangePrice',array('class'=>'')); ?>
    <?php echo $form->labelEx($model,'isChangePrice',array('class'=>'checkbox_one_label')); ?>
</div>
<div class="row <?php echo $classDisplay;?>">
    <label>&nbsp;</label>
    <?php echo $form->checkBox($model,'is_bu_vo_zero',array('class'=>'')); ?>
    <?php echo $form->labelEx($model,'is_bu_vo_zero',array('class'=>'checkbox_one_label')); ?>
</div>          
<div class="clr"></div> 
<div class="row <?php echo $classDisplay;?> display_none">
    <label>&nbsp;</label>
    <?php echo $form->checkBox($model,'is_auto_gen_storecard',array('checked' => $checkUpdateHgd)); ?>
    <?php echo $form->labelEx($model,'is_auto_gen_storecard',array('class'=>'checkbox_one_label')); ?>
</div>

<div class="WrapDiscountType">
    <div class="row GasCheckboxList " style=''>
        <?php echo $form->labelEx($model,'discount_type'); ?>
        <?php echo $form->radioButtonList($model,'discount_type', $model->getArrayDiscount(), 
                    array(
                        'separator'=>"",
                        'template'=>'<li>{input}{label}</li>',
                        'container'=>'ul',
                        'class'=>'RadioDiscountType' 
                    )); 
        ?>
    </div>
    <div class='clr'></div>
    <?php $display_none='display_none'; 
        if($model->discount_type != Sell::DISCOUNT_DEFAULT){
            $display_none = '';
        }
     ?>
    <div class=" row WrapDiscountAmount <?php echo $display_none;?>">
        <?php echo $form->labelEx($model,'amount_discount'); ?>
        <?php echo $form->textField($model,'amount_discount',array('class'=>'ad_fix_currency amount_discount')); ?>
        <?php echo $form->error($model,'amount_discount'); ?>
    </div>
</div>

<?php /* 
<div class="">
    <div class="row GasCheckboxList " style=''>
        <?php echo $form->labelEx($model,'amount_discount'); ?>
        <?php echo $form->radioButtonList($model,'amount_discount', $model->getArrayTypeDiscount(), 
                    array(
                        'separator'=>"",
                        'template'=>'<li>{input}{label}</li>',
                        'container'=>'ul',
                        'class'=>'' 
                    )); 
        ?>
    </div>
</div>
*/ ?>

<div class="clr"></div> 
<div class="row <?php echo $classDisplay;?>">
    <?php echo $form->labelEx($model,'pttt_code', ['label'=>'PTTT CODE']); ?>
    <?php if(!empty($model->pttt_code)): ?>
        <?php echo $model->pttt_code; ?>
    <?php else: ?>
        <?php echo $form->textField($model,'pttt_code',array('class'=>'','maxlength'=>5,)); ?>
    <?php endif; ?>
    <?php echo $form->error($model,'pttt_code'); ?>
</div>

<div class="row <?php echo $classDisplayStatus . $classDisplayPromotionApp;?>">
    <?php echo $form->labelEx($model,'promotion_amount'); ?>
    <?php echo $form->textField($model,'promotion_amount',array('class'=>'ad_fix_currency promotion_amount')); ?>
    <?php echo $form->error($model,'promotion_amount'); ?>
</div>
<?php if(!empty($model->promotion_id) || !empty($model->v1_discount_id)): ?>
    <div class="row">
        <label>Khuyến mãi App</label>
        <div class="add_new_item float_l" style="padding-left: 0;">
            <?php echo strip_tags($model->getPromotionCodeNo()); ?>
        </div>
    </div>
<?php endif; ?>
<div class="clr"></div>
<?php include '_form_reward.php'; ?>
<div class="clr"></div>
<?php include '_form_detail.php'; ?>

<div class="row buttons" style="padding-left: 250px;">
    <?php $this->widget('bootstrap.widgets.TbButton', array(
        'buttonType'=>'submit',
        'label'=>$model->isNewRecord ? 'Create' :'Save',
        'type'=>'null', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
        'size'=>'small', // null, 'large', 'small' or 'mini'
        //'htmlOptions' => array('style' => 'margin-bottom: 10px; float: right;'),
    )); ?>	
</div>