<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=> GasCheck::getCurl(),
	'method'=>'get',
)); ?>

	<div class="row ">
            <?php echo $form->labelEx($model, 'agent_id'); ?>
            <?php echo $form->hiddenField($model, 'agent_id'); ?>
            <?php
            // 1. limit search kh của sale
            $url = Yii::app()->createAbsoluteUrl('admin/ajax/search_by_role', array('role' => ROLE_AGENT));
            // widget auto complete search user customer and supplier
            $aData = array(
                'model' => $model,
                'field_customer_id' => 'agent_id',
                'url' => $url,
                'name_relation_user' => 'rAgent',
                'ClassAdd' => 'w-300',
                'placeholder' => 'Nhập mã hoặc tên đại lý',
            );
            $this->widget('ext.SpaAutocompleteCustomer.SpaAutocompleteCustomer',             array('data' => $aData));
            ?>
        </div>

	<div class="row">
            <?php echo $form->labelEx($model,'funds'); ?>
            <?php echo $form->textField($model,'funds', ['class'=>'number_only_v1 ad_fix_currency w-300']); ?>
        </div>
    
        <div class="row">
            <?php echo $form->labelEx($model, 'search_month',['label'=>'Tháng & năm áp dụng']); ?>
            <?php 
            $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                'model' => $model,
                'attribute' => 'search_month',
                'options' => array(
                    'showAnim' => 'fold',
                    'dateFormat' => MyFormat::$dateFormatSearch,
                    'changeMonth' => true,
                    'changeYear' => true,
                    'showOn' => 'button',
                    'buttonImage' => Yii::app()->theme->baseUrl . '/admin/images/icon_calendar_r.gif',
                    'buttonImageOnly' => true,

                ),

                'htmlOptions' => array(
                    'class' => 'w-16',
                    'style' => 'height:20px;',
                ),
            ));
            ?>  
            <?php echo $form->error($model, 'search_month'); ?>
        </div>
    
<!--	<div class="row">
            <?php  ?>
            <?php // echo $form->labelEx($model,'Tháng áp dụng'); ?>
            <?php // echo $form->dropDownList($model,'search_month', array_combine(range(1, 12),range(1, 12)), ['empty'=>'Select', 'class'=>'w-300']); ?>
        </div>
    
	<div class="row">
            <?php // echo $form->labelEx($model,'Năm áp dụng'); ?>
            <?php // echo $form->textField($model,'search_year', ['class'=>'number_only_v1 ad_fix_currency w-300']); ?>
        </div>-->

<!--	<div class="row more_col">
            <div class="col1">
                <?php echo $form->labelEx($model, 'date_from', ['label' => 'Ngày áp dụng từ']); ?>
                <?php 
                $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                    'model' => $model,
                    'attribute' => 'date_from',
                    'options' => array(
                        'showAnim' => 'fold',
                        'dateFormat' => MyFormat::$dateFormatSearch,
                        'changeMonth' => true,
                        'changeYear' => true,
                        'showOn' => 'button',
                        'buttonImage' => Yii::app()->theme->baseUrl . '/admin/images/icon_calendar_r.gif',
                        'buttonImageOnly' => true,

                    ),

                    'htmlOptions' => array(
                        'class' => 'w-16',
                        'style' => 'height:20px;',
                    ),
                ));
                ?>  
            </div>
            <div class="col1">
                <?php echo $form->labelEx($model, 'date_to', ['label' => 'đến']); ?>
                <?php 
                $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                    'model' => $model,
                    'attribute' => 'date_to',
                    'options' => array(
                        'showAnim' => 'fold',
                        'dateFormat' => MyFormat::$dateFormatSearch,
                        'changeMonth' => true,
                        'changeYear' => true,
                        'showOn' => 'button',
                        'buttonImage' => Yii::app()->theme->baseUrl . '/admin/images/icon_calendar_r.gif',
                        'buttonImageOnly' => true,

                    ),

                    'htmlOptions' => array(
                        'class' => 'w-16',
                        'style' => 'height:20px;',
                    ),
                ));
                ?>  
            </div>
        </div>-->


	<div class="row buttons" style="padding-left: 159px;">
		        <?php $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType'=>'submit',
            'label'=>'Search',
            'type'=>'null', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
            'size'=>'small', // null, 'large', 'small' or 'mini'
            //'htmlOptions' => array('style' => 'margin-bottom: 10px; float: right;'),
        )); ?>	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->