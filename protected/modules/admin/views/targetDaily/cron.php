<div class="form">
<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'customer-draft-form',
	'enableAjaxValidation'=>false,
        'htmlOptions' => array('enctype' => 'multipart/form-data'),
)); ?>
    <div class="row">
        <div class="row">
            <?php echo $form->labelEx($model, 'is_purchased'); ?>
            <?php echo $form->textField($model, 'is_purchased',array('style'=>'','class'=>'w-600')); ?>
            <?php echo $form->error($model, 'is_purchased'); ?>
        </div>
        <?php echo $form->labelEx($model,'file_excel'); ?>
        <div style="padding-left: 140px;">
            <?php echo $form->fileField($model,'file_excel'); ?>
        </div>
        <?php echo $form->error($model,'file_excel'); ?>
    </div>
    <div class="row buttons" style="padding-left: 141px;">
        <?php $this->widget('bootstrap.widgets.TbButton', array(
        'buttonType'=>'submit',
        'label'=>'Save',
        'type'=>'null', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
        'size'=>'small', // null, 'large', 'small' or 'mini'
        //'htmlOptions' => array('style' => 'margin-bottom: 10px; float: right;'),
    )); ?>	</div>
<?php $this->endWidget(); ?>
</div>