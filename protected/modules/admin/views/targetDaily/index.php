<?php
$this->breadcrumbs=array(
	$this->singleTitle,
);

$menus=array(
            array('label'=>"Create $this->singleTitle", 'url'=>array('create')),
);
$this->menu= ControllerActionsName::createMenusRoles($menus, $actions);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('target-daily-grid', {
                url : $(this).attr('action'),
		data: $(this).serialize()
	});
	return false;
});
");

Yii::app()->clientScript->registerScript('ajaxupdate', "
$('#target-daily-grid a.removeBQV').live('click', function() {
    if(confirm('Are you sure you want to delete this item?')){
        $.fn.yiiGridView.update('target-daily-grid', {
            type: 'POST',
            url: $(this).attr('href'),
            success: function() {
                $.fn.yiiGridView.update('target-daily-grid');
            }
        });
    }
    return false;
});
");

Yii::app()->clientScript->registerScript('removeBQV', "
$('#target-daily-grid a.ajaxupdate').live('click', function() {
    $.fn.yiiGridView.update('target-daily-grid', {
        type: 'POST',
        url: $(this).attr('href'),
        success: function() {
            $.fn.yiiGridView.update('target-daily-grid');
        }
    });
    return false;
});
");
?>

<h1>Danh Sách <?php echo $this->pluralTitle; ?></h1>


<?php echo CHtml::link('Tìm Kiếm Nâng Cao','#',array('class'=>'search-button')); ?>
<div class="search-form">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'target-daily-grid',
	'dataProvider'=>$model->search(),
        'afterAjaxUpdate'=>'function(id, data){ fnUpdateColorbox();}',
        'template'=>'{pager}{summary}{items}{pager}{summary}',
//    'itemsCssClass' => 'items custom_here',
//    'htmlOptions'=> array('class'=>'grid-view custom_here'),
        'pager' => array(
            'maxButtonCount'=>  CmsFormatter::$PAGE_MAX_BUTTON,
        ),
	'enableSorting' => false,
	//'filter'=>$model,
	'columns'=>array(
        array(
            'header' => 'S/N',
            'type' => 'raw',
            'value' => '$this->grid->dataProvider->pagination->currentPage * $this->grid->dataProvider->pagination->pageSize + ($row+1)',
            'headerHtmlOptions' => array('width' => '10px','style' => 'text-align:center;'),
            'htmlOptions' => array('style' => 'text-align:center;')
        ),
//            array(
//                'name' => 'id',
//                'value' => '$data->id',
//                'htmlOptions' => array('class' => 'display_none')
//            ),
            array(
                'name' => 'customer_id',
                'value' => '$data->getCustomerName()'
            ),
            array(
                'name' => 'type',
                'value' => '$data->getType()'
            ),
            array(
                'name' => 'type_customer',
                'value' => '$data->getTypeCustomer()'
            ),
            array(
                'name' => 'Địa chỉ',
                'value' => '$data->getCustomerAddress()',
                'htmlOptions' => [
                    'style' => 'max-width:300px;'
                ]
            ),
            array(
                'name' => 'Phone',
                'value' => '$data->getCustomerPhone()'
            ),
            array(
                'name' => 'agent_id',
                'value' => '$data->getAgent()'
            ),
            array(
                'name' => 'BQV 1',
                'type' => 'raw', // Duong nv Mar 3 test
                'value' => '$data->getSell(true)'
            ),
            array(
                'name' => 'BQV 2',
                'type' => 'raw', // Duong nv Mar 3 test
                'value' => '$data->getSell()'
            ),
            array(
                'name' => 'Cuộc gọi',
                'type' => 'raw', // Duong nv Mar 3 test
                'value' => '$data->getCall()'
            ),
            array(
                'name' => 'Trùng lặp',
                'type' => 'raw', // Duong nv Mar 3 test
                'value' => '$data->getDub()'
            ),
            array(
                'name' => 'Loại BQV quá',
                'type' => 'raw', // Duong nv Mar 3 test
                'value' => '$data->getTypeRomove()'
            ),
            array(
                'name' => 'is_customer_agent',
                'value' => '$data->getIsCustomerAgent()'
            ),
            array(
                'name' => 'Nhân viên',
                'value' => '$data->getCreatedBy()'
            ),
            array(
                'name' => 'created_date',
                'value' => '$data->getCreatedDate()'
            ),
            array(
                'header' => 'Actions',
                'class'=>'CButtonColumn',
                'template'=> ControllerActionsName::createIndexButtonRoles($actions,['removeV1','removeV2']),
                'buttons'=>array(
                    'removeV1'=>array(
                        'label'=>'Xóa BQV 1',
                        'imageUrl'=>Yii::app()->theme->baseUrl . '/images/delete.png',
                        'options'=>array('class'=>'removeBQV'),
                        'url'=>'Yii::app()->createAbsoluteUrl("admin/targetDaily/RemoveV1", array("id"=>$data->id) )',
                        'visible'=> '$data->canRemove()',
                    ),
                    'removeV2'=>array(
                        'label'=>'Xóa BQV 2',
                        'imageUrl'=>Yii::app()->theme->baseUrl . '/images/ico-validate-error.png',
                        'options'=>array('class'=>'removeBQV'),
                        'url'=>'Yii::app()->createAbsoluteUrl("admin/targetDaily/RemoveV2", array("id"=>$data->id) )',
                        'visible'=> '$data->canRemoveV2()',
                    ),
                ),
            ),
	),
)); ?>

<script>
$(document).ready(function() {
    fnUpdateColorbox();
});

function fnUpdateColorbox(){   
    fixTargetBlank();
}
</script>