<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'target-daily-form',
	'enableAjaxValidation'=>false,
        'htmlOptions' => array('enctype' => 'multipart/form-data'),
)); ?>

    <p class="note">Dòng có dấu <span class="required">*</span> là bắt buộc.</p>
    <?php echo MyFormat::BindNotifyMsg(); ?>
    
        		
        
    <div class="row">
        <?php echo $form->labelEx($model,'type'); ?>
        <?php echo $form->textField($model,'type'); ?>
        <?php echo $form->error($model,'type'); ?>
    </div>
    <div class="row">
        <?php echo $form->labelEx($model,'customer_id'); ?>
        <?php echo $form->textField($model,'customer_id',array('size'=>11,'maxlength'=>11)); ?>
        <?php echo $form->error($model,'customer_id'); ?>
    </div>
    <div class="row">
        <?php echo $form->labelEx($model,'type_customer'); ?>
        <?php echo $form->textField($model,'type_customer'); ?>
        <?php echo $form->error($model,'type_customer'); ?>
    </div>
    <div class="row">
        <?php echo $form->labelEx($model,'sale_id'); ?>
        <?php echo $form->textField($model,'sale_id',array('size'=>11,'maxlength'=>11)); ?>
        <?php echo $form->error($model,'sale_id'); ?>
    </div>
    <div class="row">
        <?php echo $form->labelEx($model,'agent_id'); ?>
        <?php echo $form->textField($model,'agent_id',array('size'=>11,'maxlength'=>11)); ?>
        <?php echo $form->error($model,'agent_id'); ?>
    </div>
    <div class="row">
        <?php echo $form->labelEx($model,'province_id'); ?>
        <?php echo $form->textField($model,'province_id'); ?>
        <?php echo $form->error($model,'province_id'); ?>
    </div>
    <div class="row">
        <?php echo $form->labelEx($model,'created_by'); ?>
        <?php echo $form->textField($model,'created_by',array('size'=>11,'maxlength'=>11)); ?>
        <?php echo $form->error($model,'created_by'); ?>
    </div>
    <div class="row">
        <?php echo $form->labelEx($model,'monitoring_id'); ?>
        <?php echo $form->textField($model,'monitoring_id',array('size'=>11,'maxlength'=>11)); ?>
        <?php echo $form->error($model,'monitoring_id'); ?>
    </div>
    <div class="row">
        <?php echo $form->labelEx($model,'gdkv_id'); ?>
        <?php echo $form->textField($model,'gdkv_id',array('size'=>11,'maxlength'=>11)); ?>
        <?php echo $form->error($model,'gdkv_id'); ?>
    </div>
    <div class="row">
        <?php echo $form->labelEx($model,'materials_id'); ?>
        <?php echo $form->textField($model,'materials_id'); ?>
        <?php echo $form->error($model,'materials_id'); ?>
    </div>
    <div class="row">
        <?php echo $form->labelEx($model,'seri'); ?>
        <?php echo $form->textField($model,'seri'); ?>
        <?php echo $form->error($model,'seri'); ?>
    </div>
    <div class="row">
        <?php echo $form->labelEx($model,'phone'); ?>
        <?php echo $form->textField($model,'phone',array('size'=>11,'maxlength'=>11)); ?>
        <?php echo $form->error($model,'phone'); ?>
    </div>
    <div class="row">
        <?php echo $form->labelEx($model,'sell_id'); ?>
        <?php echo $form->textField($model,'sell_id',array('size'=>11,'maxlength'=>11)); ?>
        <?php echo $form->error($model,'sell_id'); ?>
    </div>
    <div class="row">
        <?php echo $form->labelEx($model,'sell_date'); ?>
        <?php echo $form->textField($model,'sell_date'); ?>
        <?php echo $form->error($model,'sell_date'); ?>
    </div>
    <div class="row">
        <?php echo $form->labelEx($model,'sell_date_bigint'); ?>
        <?php echo $form->textField($model,'sell_date_bigint',array('size'=>11,'maxlength'=>11)); ?>
        <?php echo $form->error($model,'sell_date_bigint'); ?>
    </div>
    <div class="row">
        <?php echo $form->labelEx($model,'sell_total'); ?>
        <?php echo $form->textField($model,'sell_total',array('size'=>11,'maxlength'=>11)); ?>
        <?php echo $form->error($model,'sell_total'); ?>
    </div>
    <div class="row">
        <?php echo $form->labelEx($model,'sell_grand_total'); ?>
        <?php echo $form->textField($model,'sell_grand_total',array('size'=>11,'maxlength'=>11)); ?>
        <?php echo $form->error($model,'sell_grand_total'); ?>
    </div>
    <div class="row">
        <?php echo $form->labelEx($model,'event_market_id'); ?>
        <?php echo $form->textField($model,'event_market_id',array('size'=>10,'maxlength'=>10)); ?>
        <?php echo $form->error($model,'event_market_id'); ?>
    </div>
    <div class="row">
        <?php echo $form->labelEx($model,'created_date'); ?>
        <?php echo $form->textField($model,'created_date'); ?>
        <?php echo $form->error($model,'created_date'); ?>
    </div>
    <div class="row">
        <?php echo $form->labelEx($model,'created_date_only_bigint'); ?>
        <?php echo $form->textField($model,'created_date_only_bigint',array('size'=>12,'maxlength'=>12)); ?>
        <?php echo $form->error($model,'created_date_only_bigint'); ?>
    </div>
	<div class="row buttons" style="padding-left: 141px;">
		        <?php $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType'=>'submit',
            'label'=>$model->isNewRecord ? 'Create' :'Save',
            'type'=>'null', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
            'size'=>'small', // null, 'large', 'small' or 'mini'
            //'htmlOptions' => array('style' => 'margin-bottom: 10px; float: right;'),
        )); ?>	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->
<script>
    $(document).ready(function(){
        $('.form').find('button:submit').click(function(){
            $.blockUI({ overlayCSS: { backgroundColor: '#fff' } }); 
        });
        
    });
</script>