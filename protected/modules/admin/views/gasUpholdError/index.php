<?php
$this->breadcrumbs=array(
	$this->singleTitle,
);

$menus=array(
    array('label'=>"Create $this->singleTitle", 'url'=>array('create')),
);
$this->menu= ControllerActionsName::createMenusRoles($menus, $actions);
Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('Parameters-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>
<?php echo MyFormat::BindNotifyMsg(); ?>
<h1>Danh Sách <?php echo $this->pluralTitle; ?></h1>

<?php if(!isset($_GET['Parameters'])): ?>
    <?php echo CHtml::link('Tìm Kiếm Nâng Cao','#',array('class'=>'search-button')); ?>
    <div class="search-form" style="display:none">
    <?php $this->renderPartial('_search',array(
           'model'=>$model,
    )); ?>
    </div><!-- search-form -->
<?php endif; ?>

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'Parameters-grid',
	'dataProvider'=>$model->search(),
//        'afterAjaxUpdate'=>'function(id, data){ fnUpdateColorbox();}',
        'template'=>'{pager}{summary}{items}{pager}{summary}',
//    'itemsCssClass' => 'items custom_here',
//    'htmlOptions'=> array('class'=>'grid-view custom_here'),
        'pager' => array(
            'maxButtonCount'=>  CmsFormatter::$PAGE_MAX_BUTTON,
        ),
//	'enableSorting' => false,
	//'filter'=>$model,
	'columns'=>array(
            array(
                'header' => 'S/N',
                'type' => 'raw',
                'value' => '$this->grid->dataProvider->pagination->currentPage * $this->grid->dataProvider->pagination->pageSize + ($row+1)',
                'headerHtmlOptions' => array('width' => '10px','style' => 'text-align:center;'),
                'htmlOptions' => array('style' => 'text-align:center;')
            ),
            array(
                'name' =>'employee_id',
                'type' => 'raw',
                'value'  => '$data->getEmployee()'
            ),
            array(
                'name' =>'customer_id',
                'type' => 'raw',
                'value'  => '"<strong>".$data->getCustomer()."</strong><br>".$data->getCustomer("address")',
            ),
            array(
                'header' => 'Lấy Hàng Mới Nhất',
                'value' => '$data->getCustomer("last_purchase")',
                'type' => 'Date',
                'htmlOptions' => array('style' => 'width:50px;')
            ),
            array(
                'header' => 'Loại KH',
                'value'=>'$data->getCustomerType()',
                'htmlOptions' => array('style' => 'width:50px;')
            ),
            array(
                'name'=>'sale_id',
                'value'=>'$data->getSale()',
            ),
            array(
                'header'=>'Lịch bảo trì',
                'value'=>'$data->getScheduleText()',
            ),
            array(
                'name' =>'last_uphold',
                'type' => 'raw',
                'value'  => '$data->getLastUphold()',
                'htmlOptions' => array('style' => 'text-align:center;')
            ),
            
            array(
                'name' =>'created_date',
                'type' => 'raw',
                'value'  => '$data->getCreatedDate()',
                'htmlOptions' => array('style' => 'text-align:center;')
            ),
            
            array(
            'header' => 'Actions',
            'class' => 'CButtonColumn',
            'template' => ControllerActionsName::createIndexButtonRoles($actions),
            'buttons' => array(
                    'delete'=>array(
                            'visible'=> '$data->canDelete()',
                    ),
                ),
            ),
	),
)); ?>

<script>
$(document).ready(function() {
    fnUpdateColorbox();
});

function fnUpdateColorbox(){   
    fixTargetBlank();
    fnShowhighLightTr();
}
</script>