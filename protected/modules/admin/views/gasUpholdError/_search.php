
<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

        <div class="row more_col">
            <div class="col1">
                <?php echo Yii::t('translation', $form->label($model,'date_from')); ?>
                <?php 
                    $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                        'model'=>$model,        
                        'attribute'=>'date_from',
                        'options'=>array(
                            'showAnim'=>'fold',
                            'dateFormat'=> MyFormat::$dateFormatSearch,       
                            'changeMonth' => true,
                            'changeYear' => true,
                            'showOn' => 'button',
                            'buttonImage'=> Yii::app()->theme->baseUrl.'/admin/images/icon_calendar_r.gif',
                            'buttonImageOnly'=> true,                                
                        ),        
                        'htmlOptions'=>array(
                            'class'=>'w-16',
                            'size'=>'16',
                            'style'=>'height:20px;float:left;',
                        ),
                    ));
                ?>     		
            </div>

            <div class="col2">
                <?php echo Yii::t('translation', $form->label($model,'date_to')); ?>
                <?php 
                    $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                        'model'=>$model,        
                        'attribute'=>'date_to',
                        'options'=>array(
                            'showAnim'=>'fold',
                            'dateFormat'=> MyFormat::$dateFormatSearch,       
                            'changeMonth' => true,
                            'changeYear' => true,
                            'showOn' => 'button',
                            'buttonImage'=> Yii::app()->theme->baseUrl.'/admin/images/icon_calendar_r.gif',
                            'buttonImageOnly'=> true,                                
                        ),        
                        'htmlOptions'=>array(
                            'class'=>'w-16',
                            'size'=>'16',
                            'style'=>'height:20px;float:left;',
                        ),
                    ));
                ?>    
            </div>
        </div>
        <div class="row">
            <?php echo $form->labelEx($model,'employee_id'); ?>
            <?php echo $form->hiddenField($model,'employee_id', array('class'=>'')); ?>
            <?php
                // 1. limit search kh của sale
                $url = Yii::app()->createAbsoluteUrl('admin/ajax/search_by_role', array('role'=> implode(',', GasUphold::$ROLE_EMPLOYEE_SEARCH_AUTOCOMPLETE)));
                // widget auto complete search user customer and supplier
                $aData = array(
                    'model'=>$model,
                    'field_customer_id'=>'employee_id',
                    'url'=> $url,
                    'name_relation_user'=>'rEmployee',
                    'field_autocomplete_name'=>'autocomplete_employee',
                    'ClassAdd' => 'w-400',
    //                'ShowTableInfo' => 0,
                );
                $this->widget('ext.SpaAutocompleteCustomer.SpaAutocompleteCustomer',
                    array('data'=>$aData));                                        
            ?>

            <?php echo $form->error($model,'employee_id'); ?>
        </div>
        <div class="row">
        <?php echo $form->labelEx($model,'customer_id'); ?>
        <?php echo $form->hiddenField($model,'customer_id', array('class'=>'')); ?>
        <?php
                // 1. limit search kh của sale
                $url = Yii::app()->createAbsoluteUrl('admin/ajax/search_user_by_code');
                // widget auto complete search user customer and supplier
                $aData = array(
                    'model'=>$model,
                    'field_customer_id'=>'customer_id',
                    'url'=> $url,
                    'name_relation_user'=>'rCustomer',
                    'ClassAdd' => 'w-400',
                );
                $this->widget('ext.SpaAutocompleteCustomer.SpaAutocompleteCustomer',
                    array('data'=>$aData));                                        
                ?>
            <?php echo $form->error($model,'customer_id'); ?>
        </div>
        
        <div class="row">
            <?php echo $form->labelEx($model,'sale_id'); ?>
            <?php echo $form->hiddenField($model,'sale_id'); ?>
            <?php // echo $form->dropDownList($model,'sale_id', GasUphold::getArrUserByRole(ROLE_SALE, array('order'=>'t.code_bussiness', 'prefix_type_sale'=>1)),array('style'=>'width:500px','empty'=>'Select')); ?>
            <?php 
                // widget auto complete search user customer and supplier
                $aData = array(
                    'model'=>$model,
                    'field_customer_id'=>'sale_id',
                    'url'=> Yii::app()->createAbsoluteUrl('admin/ajax/search_sale'),
                    'name_relation_user'=>'rSale',
                    'field_autocomplete_name'=>'autocomplete_name_sale',
                    'placeholder'=>'Nhập Tên Sale',
                    'ClassAdd' => 'w-400',
        //                        'fnSelectCustomer'=>'fnAfterSelectSaleOrAgent',
                );
                $this->widget('ext.SpaAutocompleteCustomer.SpaAutocompleteCustomer',
                    array('data'=>$aData));                                        
            ?>
            <?php echo $form->error($model,'sale_id'); ?>
        </div>
    
        <div class="row more_col">
            <div class="col1">
                <?php echo $form->labelEx($model,'type_customer'); ?>
                <?php echo $form->dropDownList($model,'type_customer', CmsFormatter::$CUSTOMER_BO_MOI,array('class'=>'w-140', 'empty'=>'Select')); ?>
            </div>
            <div class="col2">
                <?php echo $form->label($model,'schedule_type',array()); ?>
                <?php echo $form->dropDownList($model,'schedule_type', $model->getArrayScheduleType(),array('class'=>'w-200','empty'=>'Select')); ?>
            </div>
        </div>
    
    
	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->