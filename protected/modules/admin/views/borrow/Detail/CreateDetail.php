<h1><?php echo $this->pageTitle;?></h1>
<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'borrow-form',
	'enableAjaxValidation'=>false,
        'htmlOptions' => array('enctype' => 'multipart/form-data'),
)); ?>

    <p class="note">Dòng có dấu <span class="required">*</span> là bắt buộc.</p>
    <?php echo MyFormat::BindNotifyMsg(); ?>
    <div class="row">
        <?php echo $form->label($model,'contract_no'); ?>
        <?php echo $model->contract_no; ?>
    </div>
    <div class="row">
        <?php echo $form->label($model,'company_id'); ?>
        <?php echo $model->getCompany(); ?>
    </div>
    <div class="row">
        <?php echo $form->label($model,'bank_id'); ?>
        <?php echo $model->getBank(); ?>
    </div>
    <div class="row">
        <?php echo $form->label($model,'borrow_amount'); ?>
        <?php echo $model->getBorrowAmount(true); ?>
    </div>
    <div class="row">
        <?php echo $form->label($model,'pay_amount'); ?>
        <?php echo $model->getPayAmount(true); ?>
    </div>
    <div class="row">
        <?php echo $form->label($model,'remain_amount'); ?>
        <?php echo $model->getRemainAmount(true); ?>
    </div>
    <div class="row">
        <?php echo $form->labelEx($model,'expiry_date'); ?>
        <?php 
            $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                'model'=>$model->mDetail,
                'attribute'=>'date_pay',
                'options'=>array(
                    'showAnim'=>'fold',
                    'dateFormat'=> ActiveRecord::getDateFormatJquery(),
//                            'minDate'=> '0',
                    'maxDate'=> '0',
                    'changeMonth' => true,
                    'changeYear' => true,
                    'showOn' => 'button',
                    'buttonImage'=> Yii::app()->theme->baseUrl.'/admin/images/icon_calendar_r.gif',
                    'buttonImageOnly'=> true,                                
                ),        
                'htmlOptions'=>array(
                    'class'=>'w-16',
                    'style'=>'height:30px;',
                    'readonly'=>'readonly',
                ),
            ));
        ?>     		
        <?php echo $form->error($model->mDetail,'date_pay'); ?>
    </div>
    <div class="row">
        <?php echo $form->labelEx($model->mDetail,'pay_amount'); ?>
        <?php echo $form->textField($model->mDetail,'pay_amount',array('class'=>'w-150 number_only ad_fix_currency','maxlength'=>20)); ?>
        <?php echo $form->error($model->mDetail,'pay_amount'); ?>
    </div>
    <div class="row buttons" style="padding-left: 141px;">
        <?php $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType'=>'submit',
            'label'=>$model->isNewRecord ? 'Create' :'Save',
            'type'=>'null', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
            'size'=>'small', // null, 'large', 'small' or 'mini'
            //'htmlOptions' => array('style' => 'margin-bottom: 10px; float: right;'),
        )); ?>	
        <input class='cancel_iframe' type='button' value='Cancel'>
    </div>

    <br>
    <div class="row">
        <label>Lịch sử trả tiền</label>
        <div class="float_l"><?php echo $model->getRecordDetail(); ?></div>
    </div>
<?php $this->endWidget(); ?>

</div><!-- form -->



<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/autoNumeric-master/autoNumeric.js"></script>
<script>
    $(document).ready(function(){
        $('.form').find('button:submit').click(function(){
            $.blockUI({ overlayCSS: { backgroundColor: '#fff' } }); 
        });
        fnInitInputCurrency();
        parent.$.fn.yiiGridView.update("borrow-grid");
    });
</script>