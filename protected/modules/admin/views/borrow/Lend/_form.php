<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'borrow-form',
	'enableAjaxValidation'=>false,
        'htmlOptions' => array('enctype' => 'multipart/form-data'),
)); ?>

    <p class="note">Dòng có dấu <span class="required">*</span> là bắt buộc.</p>
    <?php echo MyFormat::BindNotifyMsg(); ?>
    <div class="row">
        <?php echo $form->labelEx($model,'contract_no'); ?>
        <?php echo $form->textField($model,'contract_no',array('class'=> 'w-350','maxlength'=>50)); ?>
        <?php echo $form->error($model,'contract_no'); ?>
    </div>
    <div class="row">
        <?php echo $form->labelEx($model, 'credit_owner_id', array()); ?>
        <?php echo $form->hiddenField($model, 'credit_owner_id'); ?>
        <?php
        $url = Yii::app()->createAbsoluteUrl('admin/ajax/search_user_login', ['GetAll'=>1]);
        // widget auto complete search user customer and supplier
        $aData = array(
            'model' => $model,
            'field_customer_id' => 'credit_owner_id',
            'url' => $url,
            'name_relation_user' => 'rCreditOwner',
            'show_role_name' => 1,
            'ClassAdd' => 'w-350',
        );
        $this->widget('ext.SpaAutocompleteCustomer.SpaAutocompleteCustomer',
            array('data' => $aData));
        ?>
        <?php echo $form->error($model,'credit_owner_id'); ?>
    </div>
    <div class="row">
        <?php echo $form->labelEx($model,'borrow_type'); ?>
        <?php echo $form->dropDownList($model,'borrow_type', $model->getArrayType(),array('class'=>'borrow_type', 'empty'=>'Select')); ?>
        <?php echo $form->error($model,'borrow_type'); ?>
    </div>
    <div class="row">
        <?php echo $form->labelEx($model,'company_id'); ?>
        <?php echo $form->dropDownList($model,'company_id', $model->getListdataCompany(),array('class'=>'', 'empty'=>'Select')); ?>
        <?php echo $form->error($model,'company_id'); ?>
    </div>
    <div class="row">
        <?php echo $form->labelEx($model,'bank_id'); ?>
        <?php echo $form->dropDownList($model,'bank_id', $model->getListdataBank(),array('class'=>'', 'empty'=>'Select')); ?>
        <?php echo $form->error($model,'bank_id'); ?>
    </div>
    <div class="row">
        <?php echo $form->labelEx($model,'borrow_date'); ?>
        <?php 
            $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                'model'=>$model,        
                'attribute'=>'borrow_date',
                'options'=>array(
                    'showAnim'=>'fold',
                    'dateFormat'=> ActiveRecord::getDateFormatJquery(),
//                            'minDate'=> '0',
                    'maxDate'=> '0',
                    'changeMonth' => true,
                    'changeYear' => true,
                    'showOn' => 'button',
                    'buttonImage'=> Yii::app()->theme->baseUrl.'/admin/images/icon_calendar_r.gif',
                    'buttonImageOnly'=> true,                                
                ),        
                'htmlOptions'=>array(
                    'class'=>'w-16',
                    'style'=>'height:20px;',
                    'readonly'=>'readonly',
                ),
            ));
        ?>     		
        <?php echo $form->error($model,'borrow_date'); ?>
    </div>
    <div class="row">
        <?php echo $form->labelEx($model,'expiry_date'); ?>
        <?php 
            $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                'model'=>$model,        
                'attribute'=>'expiry_date',
                'options'=>array(
                    'showAnim'=>'fold',
                    'dateFormat'=> ActiveRecord::getDateFormatJquery(),
                    'minDate'=> '0',
//                    'maxDate'=> '0',
                    'changeMonth' => true,
                    'changeYear' => true,
                    'showOn' => 'button',
                    'buttonImage'=> Yii::app()->theme->baseUrl.'/admin/images/icon_calendar_r.gif',
                    'buttonImageOnly'=> true,                                
                ),        
                'htmlOptions'=>array(
                    'class'=>'w-16',
                    'style'=>'height:20px;',
                ),
            ));
        ?>     		
        <?php echo $form->error($model,'expiry_date'); ?>
    </div>
    <div class="row borrow_month">
        <?php echo $form->labelEx($model,'borrow_month'); ?>
        <?php echo $form->dropDownList($model,'borrow_month', MyFormat::BuildNumberOrder(100),array('class'=>'', 'empty'=>'Select')); ?>
        <?php echo $form->error($model,'borrow_month'); ?>
    </div>
    
    <div class="row">
        <?php echo $form->labelEx($model,'interest_rate'); ?>
        <?php echo $form->textField($model,'interest_rate',array('class'=>'w-150 number_only_v1 item_l','maxlength'=>5)); ?> <span class="f_size_18"> %</span>
        <?php echo $form->error($model,'interest_rate'); ?>
    </div>
    <div class="row">
        <?php echo $form->labelEx($model,'borrow_amount'); ?>
        <?php echo $form->textField($model,'borrow_amount',array('class'=>'w-150 number_only ad_fix_currency','maxlength'=>20)); ?>
        <?php echo $form->error($model,'borrow_amount'); ?>
    </div>
    <div class="row">
        <?php echo $form->labelEx($model,'description'); ?>
        <?php echo $form->textArea($model,'description',array('rows'=>6, 'class'=> "w-600")); ?>
        <?php echo $form->error($model,'description'); ?>
    </div>
    <div class="row">
        <?php echo $form->labelEx($model,'note'); ?>
        <?php echo $form->textArea($model,'note',array('rows'=>6, 'class'=> "w-600")); ?>
        <?php echo $form->error($model,'note'); ?>
    </div>
    <div class="row buttons" style="padding-left: 141px;">
        <?php $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType'=>'submit',
            'label'=>$model->isNewRecord ? 'Create' :'Save',
            'type'=>'null', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
            'size'=>'small', // null, 'large', 'small' or 'mini'
            //'htmlOptions' => array('style' => 'margin-bottom: 10px; float: right;'),
        )); ?>	
    </div>

<?php $this->endWidget(); ?>

</div><!-- form -->
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/autoNumeric-master/autoNumeric.js"></script>
<script>
    $(document).ready(function(){
        $('.form').find('button:submit').click(function(){
            $.blockUI({ overlayCSS: { backgroundColor: '#fff' } }); 
        });
        fnInitInputCurrency();
        BorrowTypeChange();
        $('.borrow_type').trigger('change');
    });
    
    function BorrowTypeChange(){
        $('.borrow_type').change(function(){
            $(".expiry_date, .borrow_month").hide();
            var borrow_type = $(this).val();
            if(borrow_type == <?php echo Borrow::BORROW_BAO_LANH_TT;?>){
                $(".borrow_month").val("").hide();
                $(".borrow_month").find('select').val("");
                $(".expiry_date").show();
            }else{
                $(".expiry_date").hide().find('input').val("");
                $(".borrow_month").show();
            }
        });
    }
    
</script>