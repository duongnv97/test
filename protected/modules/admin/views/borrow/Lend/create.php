<?php
$this->breadcrumbs=array(
	$this->pluralTitle=>array('lendIndex'),
	'Tạo Mới',
);

$menus = array(		
        array('label'=>"Cho Vay Management",'htmlOptions'=>array('class'=>'index','label'=>'Quản lý'),'url'=>array('lendIndex')),
);
$this->menu= ControllerActionsName::createMenusRoles($menus, $actions);
?>

<h1>Tạo Mới Cho Vay</h1>

<?php echo $this->renderPartial('Lend/_form', array('model'=>$model)); ?>