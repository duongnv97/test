<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=> GasCheck::getCurl(),
	'method'=>'get',
)); ?>

    <div class="row">
        <?php echo $form->labelEx($model, 'credit_owner_id', array()); ?>
        <?php echo $form->hiddenField($model, 'credit_owner_id'); ?>
        <?php
        $url = Yii::app()->createAbsoluteUrl('admin/ajax/search_user_login', ['GetAll'=>1]);
        // widget auto complete search user customer and supplier
        $aData = array(
            'model' => $model,
            'field_customer_id' => 'credit_owner_id',
            'url' => $url,
            'name_relation_user' => 'rCreditOwner',
            'show_role_name' => 1,
            'field_autocomplete_name' => 'autocomplete_name1',
            'ClassAdd' => 'w-350',
        );
        $this->widget('ext.SpaAutocompleteCustomer.SpaAutocompleteCustomer',
            array('data' => $aData));
        ?>
        <?php echo $form->error($model,'credit_owner_id'); ?>
    </div>
    <div class="row more_col">
        <div class="col1">
            <?php echo $form->label($model,'contract_no',array()); ?>
            <?php echo $form->textField($model,'contract_no',array('class'=>'w-300','maxlength'=>30)); ?>
        </div>
        <div class="col2">
            <?php echo $form->label($model,'borrow_type',array()); ?>
            <?php echo $form->dropDownList($model,'borrow_type', $model->getArrayType(),array('class'=>'w-300', 'empty'=>'Select')); ?>
        </div>
    </div>
    
    <div class="row more_col">
        <div class="col1">
            <?php echo $form->label($model,'company_id',array()); ?>
            <?php echo $form->dropDownList($model,'company_id', $model->getListdataCompany(),array('class'=>'w-310', 'empty'=>'Select')); ?>
        </div>
        <div class="col2">
            <?php echo $form->label($model,'bank_id',array()); ?>
            <?php echo $form->dropDownList($model,'bank_id', $model->getListdataBank(),array('class'=>'w-300', 'empty'=>'Select')); ?>
        </div>
    </div>
    <div class="row">
        <?php echo $form->label($model,'borrow_month',array()); ?>
        <?php echo $form->dropDownList($model,'borrow_month', MyFormat::BuildNumberOrder(100),array('class'=>'w-310', 'empty'=>'Select')); ?>
    </div>
    
    <div class="row more_col">
        <div class="col1">
            <?php echo $form->label($model,'borrow_date_from'); ?>
            <?php 
                $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                    'model'=>$model,        
                    'attribute'=>'borrow_date_from',
                    'options'=>array(
                        'showAnim'=>'fold',
                        'dateFormat'=> MyFormat::$dateFormatSearch,
//                            'minDate'=> '0',
//                        'maxDate'=> '0',
                        'changeMonth' => true,
                        'changeYear' => true,
                        'showOn' => 'button',
                        'buttonImage'=> Yii::app()->theme->baseUrl.'/admin/images/icon_calendar_r.gif',
                        'buttonImageOnly'=> true,                                
                    ),        
                    'htmlOptions'=>array(
                        'class'=>'w-16',
                        'size'=>'16',
                        'style'=>'float:left;',                               
                    ),
                ));
            ?>
        </div>
        <div class="col2">
            <?php echo $form->label($model,'borrow_date_to'); ?>
            <?php 
                $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                    'model'=>$model,        
                    'attribute'=>'borrow_date_to',
                    'options'=>array(
                        'showAnim'=>'fold',
                        'dateFormat'=> MyFormat::$dateFormatSearch,
//                            'minDate'=> '0',
//                        'maxDate'=> '0',
                        'changeMonth' => true,
                        'changeYear' => true,
                        'showOn' => 'button',
                        'buttonImage'=> Yii::app()->theme->baseUrl.'/admin/images/icon_calendar_r.gif',
                        'buttonImageOnly'=> true,                                
                    ),        
                    'htmlOptions'=>array(
                        'class'=>'w-16',
                        'size'=>'16',
                        'style'=>'float:left;',
                    ),
                ));
            ?>
        </div>
    </div>
    
    <div class="row more_col">
        <div class="col1">
            <?php echo $form->label($model,'expiry_date_from'); ?>
            <?php 
                $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                    'model'=>$model,        
                    'attribute'=>'expiry_date_from',
                    'options'=>array(
                        'showAnim'=>'fold',
                        'dateFormat'=> MyFormat::$dateFormatSearch,
//                            'minDate'=> '0',
//                        'maxDate'=> '0',
                        'changeMonth' => true,
                        'changeYear' => true,
                        'showOn' => 'button',
                        'buttonImage'=> Yii::app()->theme->baseUrl.'/admin/images/icon_calendar_r.gif',
                        'buttonImageOnly'=> true,                                
                    ),        
                    'htmlOptions'=>array(
                        'class'=>'w-16',
                        'size'=>'16',
                        'style'=>'float:left;',                               
                    ),
                ));
            ?>     		
        </div>
        <div class="col2">
            <?php echo $form->label($model,'expiry_date_to'); ?>
            <?php 
                $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                    'model'=>$model,        
                    'attribute'=>'expiry_date_to',
                    'options'=>array(
                        'showAnim'=>'fold',
                        'dateFormat'=> MyFormat::$dateFormatSearch,
//                            'minDate'=> '0',
//                        'maxDate'=> '0',
                        'changeMonth' => true,
                        'changeYear' => true,
                        'showOn' => 'button',
                        'buttonImage'=> Yii::app()->theme->baseUrl.'/admin/images/icon_calendar_r.gif',
                        'buttonImageOnly'=> true,                                
                    ),        
                    'htmlOptions'=>array(
                        'class'=>'w-16',
                        'size'=>'16',
                        'style'=>'float:left;',
                    ),
                ));
            ?>     		
        </div>
    </div>
    
    <div class="row">
        <?php echo $form->label($model, 'uid_login', array()); ?>
        <?php echo $form->hiddenField($model, 'uid_login'); ?>
        <?php
        $url = Yii::app()->createAbsoluteUrl('admin/ajax/search_for_user_login');
        // widget auto complete search user customer and supplier
        $aData = array(
            'model' => $model,
            'field_customer_id' => 'uid_login',
            'url' => $url,
            'name_relation_user' => 'rUidLogin',
            'show_role_name' => 1,
            'ClassAdd' => 'w-310',
        );
        $this->widget('ext.SpaAutocompleteCustomer.SpaAutocompleteCustomer',
            array('data' => $aData));
        ?>
    </div>

    <div class="row buttons" style="padding-left: 159px;">
        <?php $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType'=>'submit',
            'label'=>'Search',
            'type'=>'null', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
            'size'=>'small', // null, 'large', 'small' or 'mini'
            //'htmlOptions' => array('style' => 'margin-bottom: 10px; float: right;'),
        )); ?>	
    </div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->