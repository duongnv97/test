<?php
$this->breadcrumbs = array(
	$this->pluralTitle => array('index'),
        $model->contract_no=>array('view','id'=>$model->id),
	'Cập Nhật ' . $this->singleTitle,
);

$menus = array(	
    array('label'=>"Cho Vay Management",'htmlOptions'=>array('class'=>'index','label'=>'Quản lý'),'url'=>array('lendIndex')),
    array('label' => 'Xem Cho Vay', 'htmlOptions'=>array('class'=>'view','label'=>'View'),'url' => array('lendView', 'id' => $model->id)),	
    array('label'=>"Create cho vay",'url'=>array('lendCreate'),'htmlOptions'=>array('class'=>'create','label'=>'Tạo mới'),),
);
$this->menu= ControllerActionsName::createMenusRoles($menus, $actions);

?>

<h1>Cập Nhật: Cho Vay</h1>

<?php echo $this->renderPartial('Lend/_form', array('model'=>$model)); ?>