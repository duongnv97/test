<?php
$this->breadcrumbs=array(
	$this->pageTitle,
);

$menus=array(
    array('label'=>"Create cho vay",
        'url'=>array('lendCreate'),
        'htmlOptions'=>array('class'=>'create','label'=>'Tạo mới'),
        ),
    array('label'=> 'Xuất Excel Danh Sách Hiện Tại',
            'url'=>array('lendExportExcel'), 
            'htmlOptions'=>array('class'=>'export_excel','label'=>'Xuất Excel')),
);
$this->menu= ControllerActionsName::createMenusRoles($menus, $actions);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('borrow-grid', {
                url : $(this).attr('action'),
		data: $(this).serialize()
	});
	return false;
});
");

Yii::app()->clientScript->registerScript('ajaxupdate', "
$('#borrow-grid a.ajaxupdate').live('click', function() {
    $.fn.yiiGridView.update('borrow-grid', {
        type: 'POST',
        url: $(this).attr('href'),
        success: function() {
            $.fn.yiiGridView.update('borrow-grid');
        }
    });
    return false;
});
");
?>

<?php // include "index_button.php"; ?>
<h1><?php echo $this->pageTitle;?></h1>


<?php echo CHtml::link('Tìm Kiếm Nâng Cao','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('Lend/_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'borrow-grid',
	'dataProvider'=>$model->search(),
        'afterAjaxUpdate'=>'function(id, data){ fnUpdateColorbox();}',
        'template'=>'{pager}{summary}{items}{pager}{summary}',
//    'itemsCssClass' => 'items custom_here',
//    'htmlOptions'=> array('class'=>'grid-view custom_here'),
        'pager' => array(
            'maxButtonCount'=>  CmsFormatter::$PAGE_MAX_BUTTON,
        ),
//	'enableSorting' => false,
	//'filter'=>$model,
	'columns'=>array(
        array(
            'header' => 'S/N',
            'type' => 'raw',
            'value' => '$this->grid->dataProvider->pagination->currentPage * $this->grid->dataProvider->pagination->pageSize + ($row+1)',
            'headerHtmlOptions' => array('width' => '10px','style' => 'text-align:center;'),
            'htmlOptions' => array('style' => 'text-align:center;'),
        ),
            array(
                'header'=> 'Số sổ',
                'name'=>'contract_no',
            ),
            array(
                'name'=>'credit_owner_id',
                'value'=> '$data->getCreditOwner()',
            ),
            array(
                'name'=> 'borrow_type',
                'value'=> '$data->getBorrowType()',
            ),
            array(
                'name'=> 'company_id',
                'value'=> '$data->getCompany()',
            ),
            array(
                'name'=> 'bank_id',
                'value'=> '$data->getBank()',
            ),
//            array(
//                'name'=> 'borrow_date',
//                'value'=> '$data->getBorrowDate()',
//            ),
            array(
                'header'=> 'Chi Tiết',
                'type'=> 'raw',
                'value'=> '$data->getDetail()',
                'htmlOptions' => array('style' => 'width:200px;'),
            ),
            array(
                'header'=> 'Ngày trả',
                'name'=> 'expiry_date',
                'value'=> '$data->getExpiryDate()',
                'htmlOptions' => array('style' => 'width:50px;'),
            ),
//            array(
//                'header'=> 'Ngày chờ đáo hạn',
//                'type'=> 'raw',
//                'value'=> '$data->getDayWaitExpiryDate()',
//                'htmlOptions' => array('style' => 'width:50px;text-align:center;'),
//            ),
//            array(
//                'name'=> 'interest_rate',
//                'value'=> '$data->getInterestRate(true)',
//                'htmlOptions' => array('style' => 'text-align:center;'),
//            ),
            array(
                'header'=> 'Tiền cho vay',
                'name'=> 'borrow_amount',
                'value'=> '$data->getBorrowAmount(true)',
                'type'=> 'html',
                'htmlOptions' => array('style' => 'text-align:right;'),
            ),
            array(
                'header'=> 'Tiền đã trả',
                'name'=> 'pay_amount',
                'value'=> '$data->getPayAmount(true)',
                'type'=> 'html',
                'htmlOptions' => array('style' => 'text-align:right;'),
            ),
            array(
                'name'=> 'remain_amount',
                'value'=> '$data->getRemainAmount(true)',
                'type'=> 'html',
                'htmlOptions' => array('style' => 'text-align:right;'),
            ),
//            array(
//                'name'=> 'description',
//                'type'=> 'html',
//                'value'=> '$data->getDescription()',
//            ),
//            array(
//                'name'=> 'note',
//                'type'=> 'html',
//                'value'=> '$data->getNote()',
//            ),
//            array(
//                'name'=> 'uid_login',
//                'value'=> '$data->getUidLogin()',
//            ),
            array(
                'name'=> 'created_date',
                'type'=> 'html',
                'value'=> '$data->getCreatedInfo()',
                'htmlOptions' => array('style' => 'width:100px;'),
            ),
            
            array(
                'header' => 'Actions',
                'class'=>'CButtonColumn',
                'template'=> ControllerActionsName::createIndexButtonRoles($actions, array('createDetail','lendView','lendUpdate','delete')),
                'buttons'=>array(
                    'createDetail'=>array(
                        'label'=>'Thêm mới đáo hạn',
                        'imageUrl'=>Yii::app()->theme->baseUrl . '/images/add1.png',
                        'options'=>array('class'=>'createDetail'),
                        'url'=>'Yii::app()->createAbsoluteUrl("admin/borrow/createDetail",
                            array("id"=>$data->id) )',
                         'visible' => '$data->canCreateDetail()',

                    ),
                    'lendUpdate' => array(
                        'label'=>'Update',
                        'options'=>array('class'=>'update'),
                        'imageUrl'=>Yii::app()->theme->baseUrl . '/admin/images/icon/update_icon.png',
                        'url'=>'Yii::app()->createAbsoluteUrl("admin/borrow/lendUpdate",
                            array("id"=>$data->id) )',
                        'visible' => '$data->canUpdate()',
                    ),
                    'lendView' => array(
                        'label'=>'View',
                        'imageUrl'=>Yii::app()->theme->baseUrl . '/admin/images/icon/view_icon.png',
                        'url'=>'Yii::app()->createAbsoluteUrl("admin/borrow/LendView",
                            array("id"=>$data->id) )',
                        'options'=>array('class'=>'view')
                    ),
                    'delete'=>array(
                        'visible'=> 'GasCheck::canDeleteData($data)',
                    ),
                ),
            ),
	),
)); ?>

<script>
$(document).ready(function() {
    fnUpdateColorbox();
});

function fnUpdateColorbox(){
    fnShowhighLightTr();
    fixTargetBlank();
    $(".createDetail").colorbox({iframe:true,innerHeight:'550', innerWidth: '850',close: "<span title='close'>close</span>"});
    fnAddSumTr();
}


function fnAddSumTr(){
    var tr='';
    var borrow_amount = 0;
    var remain_amount = 0;
    var pay_amount = 0;
    $('.borrow_amount').each(function(){
        if($.trim($(this).text())!='')
            borrow_amount += parseFloat(''+$(this).text());
    });
    $('.remain_amount').each(function(){
        if($.trim($(this).text())!='')
            remain_amount+= parseFloat(''+$(this).text());
    });
    $('.pay_amount').each(function(){
        if($.trim($(this).text())!='')
            pay_amount+= parseFloat(''+$(this).text());
    });
    
    borrow_amount   = Math.round(borrow_amount * 100) / 100;
    remain_amount   = Math.round(remain_amount * 100) / 100;
    pay_amount      = Math.round(pay_amount * 100) / 100;
    
    tr +='<tr class="f_size_18 odd sum_page_current">';
        tr +='<td class="item_r item_b" colspan="8">Tổng Cộng</td>';
        tr +='<td class="item_r item_b">'+commaSeparateNumber(borrow_amount)+'</td>';
        tr +='<td class="item_r item_b">'+commaSeparateNumber(pay_amount)+'</td>';
        tr +='<td class="item_r item_b">'+commaSeparateNumber(remain_amount)+'</td>';
        tr +='<td></td><td></td>';
    tr +='</tr>';
    
    if($('.sum_page_current').size()<1){
        $('.items tbody').prepend(tr);
        $('.items tbody').append(tr);
    }
}

</script>