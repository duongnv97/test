<?php
$this->breadcrumbs=array(
    $this->pageTitle,
);

$menus = array(		
        array('label'=>"$this->singleTitle Management", 'url'=>array('index')),
        array('label'=> 'Xuất Excel Danh Sách Hiện Tại',
            'url'=>array('ExportExcelReport'), 
            'htmlOptions'=>array('class'=>'export_excel','label'=>'Xuất Excel')),
);
$this->menu= ControllerActionsName::createMenusRoles($menus, $actions);
$aData              = $model->report();
$DETAIL             = $aData['DETAIL'];
$SUM                = $aData['SUM'];
$SUM_BORROW_TYPE    = $aData['SUM_BORROW_TYPE'];
$BORROW_TYPE        = $aData['BORROW_TYPE'];
$index              = 1;
$aCompany           = $model->getListdataCompany();
//$aBank              = $model->getListdataBank();
$aBank              = $aData['BANK_LIST'];
$sumColBig          = 0;
$sumColBank         = [];
?>
<?php echo MyFormat::BindNotifyMsg(); ?>
<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/print-store-card.css" />
<h1><?php echo $this->pageTitle;?> đến ngày <?php echo date('d/m/Y'); ?></h1>
<?php if(isset($aData['SUM'])): ?>
<table class="tb hm_table summary_promotion " id="freezetablecolumns">
    <thead>
        <tr class="h_20">
            <th>#</th>
            <th>Công ty</th>
            <th>Tổng vay</th>
            <?php foreach ($aBank as $bank_id => $bank_name): ?>
            <th><?php echo $bank_name;?></th>
            <?php endforeach; ?>
        </tr>
    </thead>
    
    <tbody>
    <?php foreach ($SUM as $company_id => $sumOne): ?>
    <tr class="h_70">
        <td class="item_c"><?php echo $index++;?></td>
        <td><?php echo isset($aCompany[$company_id]) ? $aCompany[$company_id] : "";?></td>
        <td class="item_r item_b"><?php echo ActiveRecord::formatCurrency($sumOne);?>
            <span style="font-weight: normal !important;">
            <?php foreach ($model->getArrayTypeShortName() as $borrow_type_id => $short_name): ?>
                <?php $borrow_type_amount = isset($SUM_BORROW_TYPE[$company_id][$borrow_type_id]) ? $SUM_BORROW_TYPE[$company_id][$borrow_type_id] : 0; 
                    echo $borrow_type_amount > 0 ? "<br>($short_name) ".ActiveRecord::formatCurrency($borrow_type_amount) : "";
                ?>
            <?php endforeach; ?>
            </span>
        </td>
        <?php foreach ($aBank as $bank_id => $bank_name): ?>
        <?php 
            $amount = isset($DETAIL[$company_id][$bank_id]) ? $DETAIL[$company_id][$bank_id] : 0;
            $sumColBig += $amount;
            if(!isset($sumColBank[$bank_id])){
                $sumColBank[$bank_id] = $amount;
            }else{
                $sumColBank[$bank_id] += $amount;
            }
        ?>
        <td class="item_r">
            <?php if($amount > 0): ?>
                <?php echo ActiveRecord::formatCurrency($amount);?>
                <?php foreach ($model->getArrayTypeShortName() as $borrow_type_id => $short_name): ?>
                    <?php $borrow_type_amount = isset($BORROW_TYPE[$company_id][$bank_id][$borrow_type_id]) ? $BORROW_TYPE[$company_id][$bank_id][$borrow_type_id] : 0; 
                        echo $borrow_type_amount > 0 ? "<br>($short_name) ".ActiveRecord::formatCurrency($borrow_type_amount) : '';
                    ?>
                <?php endforeach; ?>
            <?php endif; ?>
        </td>
        <?php endforeach; ?>
    </tr>
    <?php endforeach; ?>
    <tr class="h_30">
        <td class="item_r item_b" colspan="2">Tổng Cộng</td>
        <td class="item_r item_b"><?php echo ActiveRecord::formatCurrency($sumColBig);?></td>
        <?php foreach ($aBank as $bank_id => $bank_name): ?>
        <?php 
            $amount = isset($sumColBank[$bank_id]) ? $sumColBank[$bank_id] : 0;
        ?>
        <td class="item_r"><?php echo $amount > 0 ? ActiveRecord::formatCurrency($amount):"";?></td>
        <?php endforeach; ?>
    </tr>
    </tbody>
</table>
<?php 
    $session = Yii::app()->session;
    $session['SESS_SUM_COL_BANK'] = $sumColBank;
?>
<?php endif; ?>

<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/scroll/jquery.freezetablecolumns.1.1.js"></script> 
<script>
$(document).ready(function() {
    initFreezetable();
});

function initFreezetable(){
    $('#freezetablecolumns').freezeTableColumns({
        width:       <?php echo Users::WIDTH_freezetable;?>,   // required - bt 1100
        height:      380,   // required
        numFrozen:   3,     // optional
        frozenWidth: 380,   // optional
        clearWidths: true  // optional
    });   
}  
</script>