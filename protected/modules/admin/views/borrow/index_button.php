<div class="form">
    <?php
        $LinkNormal         = Yii::app()->createAbsoluteUrl('admin/borrow/index');
        $LinkCashierConfirm = Yii::app()->createAbsoluteUrl('admin/borrow/index', array( 'expiry'=> 1));
    ?>
    <h1><?php echo $this->adminPageTitle;?>
        <a class='btn_cancel f_size_14 <?php echo (!isset($_GET['expiry'])) ? "active":"";?>' href="<?php echo $LinkNormal;?>">Bình Thường</a>
        <a class='btn_cancel f_size_14 <?php echo isset($_GET['expiry']) ? "active":"";?>' href="<?php echo $LinkCashierConfirm;?>">Sắp Hết Hạn</a>
    </h1> 
</div>