<?php
$this->breadcrumbs=array(
    $this->pluralTitle => array('index'),
    ' ' . $this->singleTitle . ' : ',
);

$menus = array(
	array('label'=>"$this->pluralTitle Management", 'url'=>array('index')),
	array('label'=>"Tạo Mới $this->singleTitle", 'url'=>array('create')),
	array('label'=>"Cập Nhật $this->singleTitle", 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>"Xóa $this->singleTitle", 'url'=>array('delete'), 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Bạn chắc chắn muốn xóa ?')),
);
$this->menu= ControllerActionsName::createMenusRoles($menus, $actions);
?>

<h1>Xem: <?php echo $this->singleTitle; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
            array(
                'name' => 'type',
                'type'=>'html',
                'value'=> $model->getType(),
            ),
            array(
                'name' => 'agent_id',
                'type'=>'html',
                'value'=> $model->getAgentInfo(),
            ),
            array(
                'label' => 'Thời gian hiệu lực',
                'type'=>'html',
                'value'=> $model->getDateFromTo(),
            ),
            array(
                'name' => 'description',
                'type'=>'html',
                'value'=> $model->getDescription(),
            ),
		array(
                'name' => 'status',
                'type'=>'html',
                'value'=>$model->getStatus(),
            ),
	),
)); ?>
