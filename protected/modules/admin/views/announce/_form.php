<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'announce-form',
	'enableAjaxValidation'=>false,
        'htmlOptions' => array('enctype' => 'multipart/form-data'),
)); ?>

    <p class="note">Dòng có dấu <span class="required">*</span> là bắt buộc.</p>
    <?php echo MyFormat::BindNotifyMsg(); ?>
    
        		
        
    <div class="row">
        <?php echo $form->labelEx($model,'type'); ?>
        <?php echo $form->dropDownList($model,'type', $model->getArrayType(),array('class'=>' w-200')); ?>
    </div>
    <div class="row">
        <?php echo $form->labelEx($model,'agent_id'); ?>
        <?php echo $form->hiddenField($model,'agent_id', array('class'=>'')); ?>
        <?php
                // 1. limit search kh của sale
                $url = Yii::app()->createAbsoluteUrl('admin/ajax/search_by_role', array('role'=> ROLE_AGENT));
                // widget auto complete search user customer and supplier
                $extData = array(
                    'model'=>$model,
                    'field_customer_id'=>'agent_id',
                    'url'=> $url,
                    'name_relation_user'=>'rAgent',
                    'ClassAdd' => 'w-400',
                    'field_autocomplete_name' => 'autocomplete_name',
                    'placeholder'=>'Nhập mã hoặc tên đại lý',
                );
                $this->widget('ext.SpaAutocompleteCustomer.SpaAutocompleteCustomer',
                    array('data'=>$extData));
                ?>
        <?php echo $form->error($model,'agent_id'); ?>
    </div>
    <div class="row more_col">
        <div class="col1" style="margin-right: 30px;">
                <?php echo Yii::t('translation', $form->label($model,'date_from')); ?>
                <?php 
                    $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                        'model'=>$model,        
                        'attribute'=>'date_from',
                        'options'=>array(
                            'showAnim'=>'fold',
                            'dateFormat'=> MyFormat::$dateFormatSearch,       
//                            'maxDate'=> '0',
                            'changeMonth' => true,
                            'changeYear' => true,
                            'showOn' => 'button',
                            'buttonImage'=> Yii::app()->theme->baseUrl.'/admin/images/icon_calendar_r.gif',
                            'buttonImageOnly'=> true,                                
                        ),        
                        'htmlOptions'=>array(
                            'class'=>'w-16',
                            'size'=>'16',
                            'style'=>'height:20px;float:left;',
                            'readonly'=>'1',
                        ),
                    ));
                ?>     		
            </div>

            <div class="col2">
                <?php echo Yii::t('translation', $form->label($model,'date_to')); ?>
                <?php 
                    $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                        'model'=>$model,        
                        'attribute'=>'date_to',
                        'options'=>array(
                            'showAnim'=>'fold',
                            'dateFormat'=> MyFormat::$dateFormatSearch,       
//                            'maxDate'=> '0',
                            'changeMonth' => true,
                            'changeYear' => true,
                            'showOn' => 'button',
                            'buttonImage'=> Yii::app()->theme->baseUrl.'/admin/images/icon_calendar_r.gif',
                            'buttonImageOnly'=> true,                                
                        ),        
                        'htmlOptions'=>array(
                            'class'=>'w-16',
                            'size'=>'16',
                            'style'=>'height:20px;float:left;',
                            'readonly'=>'1',
                        ),
                    ));
                ?>    
            </div>
        </div>
    
    <?php if($model->canSetStatus()): ?>
    <div class="row">
        <?php echo $form->labelEx($model,'status'); ?>
        <?php echo $form->dropDownList($model,'status', $model->getArrayStatus(),array('class'=>' w-200')); ?>
    </div>
    <?php endif; ?>
    <div class='clr'></div>
<!--    <div class="row">
        <?php // echo $form->labelEx($model,'description'); ?>
        <?php // echo $form->textArea($model,'description',array('rows'=>6, 'cols'=>50)); ?>
        <?php // echo $form->error($model,'description'); ?>
    </div>-->

    <div class="row">
        <?php echo $form->labelEx($model,'description', ['class'=>'item_b f_size_15']); ?><br><br>
        <div style="width: 80%">
        <?php echo $form->textArea($model,'description',array('class'=>'ckeditor')); ?>
        <div class="clr"></div>
        <?php echo $form->error($model,'description'); ?>
        </div>
    </div>
    <div class='clr'></div>
        
	<div class="row buttons" style="padding-left: 141px;">
		        <?php $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType'=>'submit',
            'label'=>$model->isNewRecord ? 'Create' :'Save',
            'type'=>'null', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
            'size'=>'small', // null, 'large', 'small' or 'mini'
            //'htmlOptions' => array('style' => 'margin-bottom: 10px; float: right;'),
        )); ?>	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->
<script charset="utf-8" type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/gasckeditor/ckeditor.js"></script>
<script>
    $(document).ready(function(){
        $('.form').find('button:submit').click(function(){
            $.blockUI({ overlayCSS: { backgroundColor: '#fff' } }); 
        });
        
        $(".ckeditor").each(function(){
           var id = $(this).attr('id');
            CKEDITOR.replace( id, {
                removePlugins: 'about, flash',
                height: 500,
                allowedContent: true,// Aug 13, 2016 chắc chắn phải open dòng này, không thì sẽ bị lỗi khi có tab và accordiontab // http://docs.ckeditor.com/#!/guide/dev_allowed_content_rules  == http://ckeditor.com/forums/CKEditor-3.x/ckeditor-remove-tag-attributes
                entities_latin: false,
                extraAllowedContent : '*(*)'
            });
        });
        
    });
</script>