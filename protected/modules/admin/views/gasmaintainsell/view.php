<?php /* 
$this->breadcrumbs=array(
	'Bán Hàng'=>array('index'),
	$model->customer?$model->customer->first_name:'',
);

$menus = array(
	array('label'=>'Bán Hàng', 'url'=>array('index')),
	array('label'=>'Thêm Bán Hàng', 'url'=>array('create')),
	array('label'=>'Cập Nhật Bán Hàng', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Xóa Bán Hàng', 'url'=>array('delete'), 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
);
//if(!(( Yii::app()->user->role_id == ROLE_SUB_USER_AGENT && (int)$model->update_num < (int)Yii::app()->params["limit_update_maintain"]) )){
if(!(Yii::app()->user->role_id == ROLE_ADMIN || ( Yii::app()->user->role_id == ROLE_SUB_USER_AGENT && (int)$model->update_num < (int)Yii::app()->params["limit_update_maintain"]) )){
    $pos = array_search('Update', $actions);
    unset($actions[$pos]);
}
$this->menu= ControllerActionsName::createMenusRoles($menus, $actions); */
?>
<?php $cmsFormat = new CmsFormatter(); ?>
<h1>Xem Bán Hàng Khách Hàng: <?php echo $model->customer?$cmsFormat->formatNameUser($model->customer):''; ?></h1>
<?php $cmsFormat = new CmsFormatter(); ?>
<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
            'code_no',
            array(
                'name' => 'agent_id',
                'value'=>$model->agent?$model->agent->first_name:'',
            ), 	
            array(
                'label'=>'Mã Khách Hàng',
                'name'=>'customer_name',
                'value'=>$model->customer?$model->customer->code_bussiness:'',
            ),               
           /*  array(
                'label'=>'Mã Hệ Thống',
                'name'=>'customer_name',
                'value'=>$model->customer?$model->customer->code_account:'',
            ),  */
 			
            array(
                'name'=>'customer_name',
                'type'=>'NameUser',
                'value'=>$model->customer?$model->customer:0,
            ),               
			
			array(
				'label'=>'Địa Chỉ',
                            'type'=>'AddressTempUser',
                'value'=>$model->customer?$model->customer:'',
				//'visible'=>Yii::app()->user->role_id!=ROLE_SUB_USER_AGENT,
            ), 
			array(
				'label'=>'Điện Thoại',
                'value'=>$model->customer?$model->customer->phone:'',
				//'visible'=>Yii::app()->user->role_id!=ROLE_SUB_USER_AGENT,
            ), 
            'date_sell:date',
		'quantity_sell',
            array(
                'name'=>'materials_id_sell',
                'value'=>$model->materials_sell?$model->materials_sell->name:'',
            ),              
		'quantity_vo_back',
            array(
                'name'=>'materials_id_back',
                'value'=>$model->materials_back?$model->materials_back->name:'',
            ),    
		'seri_back',			
		array(
			'label' => 'Trùng Seri Bảo Trì',
			'type' => 'CheckWithSeriMaintain',  
			'value' => $model,  
			//'htmlOptions' => array('style' => 'text-align:center;width:50px;')
		), 		
//		'gifts',
        array(
            'name' => 'status',
            'type'=>'StatusMaintain',
			'value' => $model->status,  
        ), 
            array(
                'name'=>'using_gas_huongminh',
                'value'=>  CmsFormatter::$yesNoFormat[$model->using_gas_huongminh],
            ),  
            array(
                'name'=>'using_promotion',
                'value'=>  CmsFormatter::$yesNoFormat[$model->using_promotion],
            ),              
                array(
                    'label' => 'Bảo Trì',
                    'type' => 'MaintainHistory',  
                    'value' => $model,  
                ), 
			array(
                'name'=>'gifts',
                'type'=>'MaintainSellPromotion',
                'value'=>$model,
            ),    
		'note',
        array(
            'name' => 'note_update_status',
			'type'=>'html',
        ),             
             
            		
		'created_date:datetime',
	),
)); ?>
