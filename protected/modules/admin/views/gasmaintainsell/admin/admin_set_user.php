<h1>Thiết lập User duyệt phép, User đơn vị thực hiện......</h1>

<div class="form">
<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'gas-maintain-sell-form',
	'enableAjaxValidation'=>false,
)); ?>
    <?php echo MyFormat::BindNotifyMsg(); ?>
    <?php echo $form->errorSummary($model); ?>
    
    <div class="row">
        <?php echo $form->labelEx($model,'type'); ?>
        <?php echo $form->dropDownList($model,'type', $model->getTypeAdminSetUser(),array('class'=>'type w-400','empty'=>'Select')); ?>
        <?php echo $form->error($model,'type'); ?>
    </div>
    
    <div class="row">
        <?php echo $form->labelEx($model,'customer_id', array('label'=>'Nhân Viên')); ?>
        <?php echo $form->hiddenField($model,'customer_id', array('class'=>'')); ?>
        <?php
            // 1. limit search kh của sale
            $url = Yii::app()->createAbsoluteUrl('admin/ajax/search_user_login');
            // widget auto complete search user customer and supplier
            $aData = array(
                'model'=>$model,
                'field_customer_id'=>'customer_id',
                'url'=> $url,
                'name_relation_user'=>'customer',
                'ClassAdd' => 'w-400',
//                'ShowTableInfo' => 0,
                'fnSelectCustomerV2' => "fnDeloyBySelect",
                'doSomethingOnClose' => "doSomethingOnClose",
            );
            $this->widget('ext.SpaAutocompleteCustomer.SpaAutocompleteCustomer',
                array('data'=>$aData));                                        
        ?>
    </div>
    
    <?php if($model->type > 5) include '_form_create_update_deloy_by.php';?>
    
	<div class="row buttons" style="padding-left: 141px;">
        <?php $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType'=>'submit',
            'label'=> 'Save',
            'type'=>'null', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
            'size'=>'small', // null, 'large', 'small' or 'mini'
            //'htmlOptions' => array('style' => 'margin-bottom: 10px; float: right;'),
        )); ?>
    </div>

<?php $this->endWidget(); ?>

</div><!-- form -->

<script>
    $(document).ready(function(){
        $('.form').find('button:submit').click(function(){
            $.blockUI({ overlayCSS: { backgroundColor: '<?php echo BLOCK_UI_COLOR;?>' } }); 
        });
        
        var requestUri = '<?php echo Yii::app()->createAbsoluteUrl("admin/gasmaintainsell/admin_set_user");?>';
        fnUpdateNextUrl('.type', requestUri, 'type');
        fnBindRemoveIcon();
    });
    
    /**
    * @Author: ANH DUNG Jun 23, 2015
    * @Todo: function này dc gọi từ ext của autocomplete
    * @Param: $model
    */
    function fnDeloyBySelect(ui, idField, idFieldCustomer){
        var row = $(idField).closest('.row');
        var ClassCheck = "uid_"+ui.item.id;
        var td_name = '<td class="'+ClassCheck+'">'+ui.item.name_role+'</td>';
        var td_remove = '<td class="item_c last"><span class="remove_icon_only"></span></td>';
        var input = '<input name="GasMaintainSell[one_many_gift][]"  value="'+ui.item.id+'" type="hidden">';
        var tr = '<tr><td class="item_c order_no"></td>'+td_name+td_remove+input+'</tr>';
        if($('.tb_deloy_by tbody').find('.'+ClassCheck).size() < 1 ) {
            $('.tb_deloy_by tbody').append(tr);
        }
        fnRefreshOrderNumber();
    }
    
    /**
    * @Author: ANH DUNG Dec 28, 2016
    * @Todo: function này dc gọi từ ext của autocomplete, action close auto
    */
    function doSomethingOnClose(ui, idField, idFieldCustomer){
        var row = $(idField).closest('.row');
        row.find('.remove_row_item').trigger('click');
    }

</script>
