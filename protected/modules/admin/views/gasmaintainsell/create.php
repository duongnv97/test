<?php
$this->breadcrumbs=array(
	Yii::t('translation','Bán Hàng Bảo Trì, Phát Triển Thị Trường')=>array('index'),
	Yii::t('translation','Create'),
);

$menus = array(		
        array('label'=> Yii::t('translation', 'Bán Hàng') , 'url'=>array('index')),
);
$this->menu= ControllerActionsName::createMenusRoles($menus, $actions);
?>

<h1><?php echo Yii::t('translation', 'Tạo Bán Hàng Bảo Trì, Phát Triển Thị Trường'); ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>