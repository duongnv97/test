<?php
$this->breadcrumbs=array(
	'Bán Hàng Bảo Trì, Phát Triển Thị Trường',
);

$menus=array(
	array('label'=> Yii::t('translation','Thêm Bán Hàng'), 'url'=>array('create')),
	array('label'=> Yii::t('translation','Xuất Excel Danh Sách Hiện Tại'), 
            'url'=>array('Export_list_gasmaintainsell'), 
            'htmlOptions'=>array('class'=>'export_excel','label'=>'Xuất Excel')),
    
);
$this->menu= ControllerActionsName::createMenusRoles($menus, $actions);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('gas-maintain-sell-grid', {
                url : $(this).attr('action'),
		data: $(this).serialize()
	});
	return false;
});
");

Yii::app()->clientScript->registerScript('ajaxupdate', "
$('#gas-maintain-sell-grid a.ajaxupdate').live('click', function() {
    $.fn.yiiGridView.update('gas-maintain-sell-grid', {
        type: 'POST',
        url: $(this).attr('href'),
        success: function() {
            $.fn.yiiGridView.update('gas-maintain-sell-grid');
        }
    });
    return false;
});
");
?>

<h1><?php echo Yii::t('translation', 'Bán Hàng Bảo Trì, Phát Triển Thị Trường'); ?></h1>

<?php echo CHtml::link(Yii::t('translation','Tìm Kiếm Nâng Cao'),'#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'gas-maintain-sell-grid',
	'dataProvider'=>$model->search(),
        'afterAjaxUpdate'=>'function(id, data){ fnUpdateColorbox();}',
        'template'=>'{pager}{summary}{items}{pager}{summary}', 
        'pager' => array(
            'maxButtonCount'=>  CmsFormatter::$PAGE_MAX_BUTTON,
        ),    
    
	//'filter'=>$model,
	'columns'=>array(
        array(
            'header' => 'S/N',
            'type' => 'raw',
            'value' => '$this->grid->dataProvider->pagination->currentPage * $this->grid->dataProvider->pagination->pageSize + ($row+1)',
            'headerHtmlOptions' => array('width' => '30px','style' => 'text-align:center;'),
            'htmlOptions' => array('style' => 'text-align:center;')
        ),
        array(
            'name' => 'code_no',
            'htmlOptions' => array('style' => 'text-align:center;width:50px;')
        ),            
        array(
            'name' => 'agent_id',
            'value' => '$data->agent?$data->agent->first_name:""',
            'visible'=>Yii::app()->user->role_id!=ROLE_SUB_USER_AGENT,
            'htmlOptions' => array('style' => 'text-align:center;width:60px;')
        ),             
                array(
                    'name' => 'date_sell',
                    'type'=>'date',
                    'htmlOptions' => array('style' => 'text-align:center;width:50px;')
                ),  
                array(                
                    'name' => 'customer_id',
                    'type'=>'NameUser',
                    'value'=>'$data->customer?$data->customer:0',
                    'htmlOptions' => array('style' => 'text-align:left;width:100px;')
                ),     
                        
                array(
                    'header' => 'Địa Chỉ',
                    'type'=>'AddressTempUser',
        //            'value' => '$data->customer?$data->customer->address:""',
                    'value' => '$data->customer?$data->customer:""',
                    'htmlOptions' => array('style' => 'text-align:center;width:80px;'),
					'visible'=>Yii::app()->user->role_id!=ROLE_SUB_USER_AGENT,
                ),   				
                array(
                    'header' => 'Điện Thoại',
                    'value' => '$data->customer?$data->customer->phone:""',
                    'htmlOptions' => array('style' => 'text-align:center;width:60px;'),
                    'visible'=>Yii::app()->user->role_id!=ROLE_SUB_USER_AGENT,
                ),     
        array(
            'name' => 'monitoring_id',
            'value' => '$data->monitoring?$data->monitoring->first_name:""',
            'htmlOptions' => array('style' => 'text-align:center;width:100px;')
        ),             
            
                array(
                    'name' => 'maintain_employee_id',
                    'value' => '$data->maintain_employee?$data->maintain_employee->first_name:""',
                    'htmlOptions' => array('style' => 'text-align:center;width:100px;')
                ),              
                array(
                    'header' => 'Bảo Trì / PTTT',
                    'type' => 'MaintainHistory',  
                    'value' => '$data',  
                    'htmlOptions' => array('style' => 'width:100px;')
                ),  
//                array(
//                    'header' => 'Bảo Trì Hiệu',  
//                    'value' => '$data->maintain?$data->maintain->materials->name:""',
////                    'htmlOptions' => array('style' => 'text-align:center;width:70px;')
//                ),  
//                array(
//                    'header' => 'Bảo Trì Seri',  
//                    'value' => '$data->maintain?$data->maintain->seri_no:""',
////                    'htmlOptions' => array('style' => 'text-align:center;width:70px;')
//                ),  
//                array(
//                    'header' => 'Bảo Trì Ngày',  
//                    'type'=>'date',
//                    'value' => '$data->maintain?$data->maintain->maintain_date:""',
////                    'htmlOptions' => array('style' => 'text-align:center;width:70px;')
//                ),  
//            
            
               /*  array(
                    'name' => 'quantity_sell',
                    'htmlOptions' => array('style' => 'text-align:center;width:50px;')
                ),  */              
		 
                array(
                    'name' => 'materials_id_sell',
                    'value' => '$data->materials_sell?$data->materials_sell->name:""',
                    'htmlOptions' => array('style' => 'text-align:left;width:60px;')
                ),              
                array(
                    'header' => 'Vỏ Thu Về',  
                    'name' => 'quantity_vo_back',
                    'htmlOptions' => array('style' => 'text-align:center;width:30px;')
                ),               
        array(
            'name' => 'materials_id_back',
            'value' => '$data->materials_back?$data->materials_back->name:""',
            'htmlOptions' => array('style' => 'text-align:left;width:60px;')
        ),  
        array(                    
           'name' => 'seri_back',
           'htmlOptions' => array('style' => 'text-align:center;width:50px;')
       ),  
//            
//       array(
//            'header' => 'Trùng Seri Bảo Trì',
//            'type' => 'CheckWithSeriMaintain',  
//            'value' => '$data',  
//            'htmlOptions' => array('style' => 'text-align:center;width:50px;')
//        ),              
		/*
		 Yii::t('translation','gifts'),
		 Yii::t('translation','note'),
		 Yii::t('translation','agent_id'),
		 Yii::t('translation','user_id_create'),
		 Yii::t('translation','created_date'),
		*/
//        array(
//            'name' => 'note_update_status',
//            'type'=>'html',
//            'htmlOptions' => array('style' => 'width:80px;')
//        ),             
//        array(
//            'name' => 'status',
//            'type'=>'StatusMaintain',
////            'value'=>'CmsFormatter::$STATUS_MAINTAIN[$data->status]',
//            'htmlOptions' => array('style' => 'text-align:center;width:50px;font-weight:bold;')
//        ),              
        array(
            'name' => 'created_date',
            'type' => 'Datetime',
            'htmlOptions' => array('style' => 'width:50px;')
        ),             
            array(
//                        'htmlOptions' => array('style' => 'text-align:center;width:70px;'), KHÔNG DÙNG CÁI NÀY, SẼ BỊ MẤT CLASS button-column
			'header' => 'Action',
			'class'=>'CButtonColumn',
//                        'template'=> ControllerActionsName::createIndexButtonRoles($actions, array('update_status_maintain_sell','view','update','delete')),
                        'template'=> ControllerActionsName::createIndexButtonRoles($actions, array('view','update','delete')),
                        'buttons'=>array(
                            'update_status_maintain_sell'=>array(
                                'label'=>'Cập nhật trạng thái bảo trì',
                                'imageUrl'=>Yii::app()->theme->baseUrl . '/admin/images/edit.png',
                                'options'=>array('class'=>'update_status_maintain_sell'),
                                'url'=>'Yii::app()->createAbsoluteUrl("admin/gasmaintainsell/update_status_maintain_sell",
                                    array("maintain_id"=>$data->id,
                                            "order_id"=>$data->id) )',
                                 'visible'=>'( Yii::app()->user->role_id == ROLE_ADMIN || (int)$data->update_num < (int)Yii::app()->params["limit_update_maintain"] )',                                                                
                            ),
                            'update'=>array(
//                                'visible'=>'( Yii::app()->user->role_id == ROLE_ADMIN || (int)$data->update_num < (int)Yii::app()->params["limit_update_maintain"] )',
                                'visible'=>  'GasCheck::AgentCanUpdateMaintainSell($data)',
                            ),                            
                            'delete'=>array(
                                'visible'=> 'GasCheck::canDeleteData($data)',
                            ),
			),                          
		),
		
	),
)); ?>

<script>
$(document).ready(function() {
    fnUpdateColorbox();
});

function fnUpdateColorbox(){   
    $(".update_status_maintain_sell").colorbox({iframe:true,innerHeight:'450', innerWidth: '850',close: "<span title='close'>close</span>"});
    $(".view").colorbox({iframe:true,innerHeight:'800', innerWidth: '900',close: "<span title='close'>close</span>"});
    fixTargetBlank();
}
</script>