<?php

class ClosingPersonalDebitController extends AdminController 
{
    public $pluralTitle = 'Bảo Trì';
    public $singleTitle = 'Bảo Trì';
    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id)
    {
        $this->pageTitle = 'Xem ';
        try{
        $this->render('view',array(
                'model'=>$this->loadModel($id), 'actions' => $this->listActionsCanAccess,
        ));
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }

    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate()
    {
        $this->pageTitle = 'Tạo Mới ';
        try{
        $model=new ClosingPersonalDebit('create');

        if(isset($_POST['ClosingPersonalDebit']))
        {
            $model->attributes=$_POST['ClosingPersonalDebit'];
            $model->validate();
            if(!$model->hasErrors()){
                $model->save();
                Yii::app()->user->setFlash( MyFormat::SUCCESS_UPDATE, "Thêm mới thành công." );
                $this->redirect(array('create'));
            }				
        }

        $this->render('create',array(
                'model'=>$model, 'actions' => $this->listActionsCanAccess,
        ));
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id)
    {
        $this->pageTitle = 'Cập Nhật ';
        try{
        $model=$this->loadModel($id);
        $model->scenario = 'update';
        if(isset($_POST['ClosingPersonalDebit']))
        {
            $model->attributes=$_POST['ClosingPersonalDebit'];
                    // $this->redirect(array('view','id'=>$model->id));
            $model->validate();
            if(!$model->hasErrors()){
                $model->save();
                Yii::app()->user->setFlash( MyFormat::SUCCESS_UPDATE, "Cập nhật thành công." );
                $this->redirect(array('update','id'=>$model->id));
            }							
        }

        $this->render('update',array(
                'model'=>$model, 'actions' => $this->listActionsCanAccess,
        ));
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id)
    {
        try{
        if(Yii::app()->request->isPostRequest){
            // we only allow deletion via POST request
            if($model = $this->loadModel($id))
            {
                if($model->delete())
                    Yii::log("Uid: " .Yii::app()->user->id. " Delete record ".  print_r($model->attributes, true), 'info');
            }

            // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
            if(!isset($_GET['ajax']))
                    $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('index'));
        }
        else
        {
            Yii::log("Uid: " .Yii::app()->user->id. " Invalid request. Please do not repeat this request again.");
            throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
        }	
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }

    /**
     * Manages all models.
     */
    public function actionIndex()
    {
        $this->pageTitle = 'Danh Sách ';
        try{
            $aData = [];
            $model=new ClosingPersonalDebit();
            $model->unsetAttributes();  // clear any default values
            if(isset($_GET['ClosingPersonalDebit'])){
                $model->attributes=$_GET['ClosingPersonalDebit'];
            }
            $this->render('index',array(
                'model'=>$model, 
                'actions' => $this->listActionsCanAccess,
            ));
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }
    
    /**
     * Manages all models.
     */
    public function actionReportVo()
    {
        error_reporting(1); // var_dump(PHP_INT_SIZE);
        $this->pageTitle = 'Nợ Vỏ';
        try{
            $aData = [];
            $model=new ClosingPersonalDebit();
            $model->current_date = date('Y-m-d');
            $model->unsetAttributes();  // clear any default values
            $cRole = MyFormat::getCurrentRoleId();
            $cUid = MyFormat::getCurrentUid();
            $RoleApply = $model->roleOnlySearchCustomer();
            if(in_array($cRole, $RoleApply)){
                $model->sale_id = $cUid;
                $model->onlySearchCustomer = true;
            }
            if(isset($_GET['ClosingPersonalDebit'])){
                $model->attributes=$_GET['ClosingPersonalDebit'];
                $aData = $model->getDataAll(false);
            }
            $this->render('reportVo',array(
                'model'=>$model, 
                'aData'=>$aData, 
                'actions' => $this->listActionsCanAccess,
            ));
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }
    
    public function actionCron() {
        echo '<pre>';
        print_r('not run');
        echo '</pre>';
        die;
        $model=new ClosingPersonalDebit();
        $date = date('Y-m-d');
        $model->gasAppOrderDaily($date);
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
    * If the data model is not found, an HTTP exception will be raised.
     * @param integer the ID of the model to be loaded
     */
    public function loadModel($id)
    {
        try{
        $model=ClosingPersonalDebit::model()->findByPk($id);
        if($model===null)
        {
            Yii::log("The requested page does not exist.");
            throw new CHttpException(404,'The requested page does not exist.');
        }			
        return $model;
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }

}
