<?php

class AppPromotionUserController extends AdminController 
{
    public $pluralTitle = 'Khuyến mãi của khách hàng';
    public $singleTitle = 'Khuyến mãi của khách hàng';
    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id)
    {
        $this->pageTitle = 'Xem ';
        try{
            $this->render('view',array(
                'model'=>$this->loadModel($id), 'actions' => $this->listActionsCanAccess,
            ));
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate()
    {
        $this->pageTitle = 'Tạo Mới ';
        try{
        $model=new AppPromotionUser('create');

        if(isset($_POST['AppPromotionUser'])){
            $model->attributes=$_POST['AppPromotionUser'];
            $model->getParamsPost();
            $model->validate();
            $model->sourceFrom = BaseSpj::SOURCE_FROM_WINDOW;
            if(!$model->hasErrors()){
                $model->webCreate();
                if(!$model->hasErrors()){
                    $model->notifyToCustomer();
                    Yii::app()->user->setFlash( MyFormat::SUCCESS_UPDATE, "Thêm mới thành công." );
                    $this->redirect(array('create'));
                }
            }				
        }

        $this->render('create',array(
                'model'=>$model, 'actions' => $this->listActionsCanAccess,
        ));
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id)
    {
        $this->pageTitle = 'Cập Nhật ';
        try{
        $model=$this->loadModel($id);
        if(!$model->canUpdate()){
            $this->redirect(['index']);
        }
        $model->scenario = 'update';
        if(isset($_POST['AppPromotionUser']))
        {
            $model->attributes=$_POST['AppPromotionUser'];
                    // $this->redirect(array('view','id'=>$model->id));
            $model->validate();
            if(!$model->hasErrors()){
                $model->webUpdate();
                Yii::app()->user->setFlash( MyFormat::SUCCESS_UPDATE, "Cập nhật thành công." );
                $this->redirect(array('update','id'=>$model->id));
            }							
        }

        $this->render('update',array(
                'model'=>$model, 'actions' => $this->listActionsCanAccess,
        ));
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id)
    {
        try{
        if(Yii::app()->request->isPostRequest){
            // we only allow deletion via POST request
            if($model = $this->loadModel($id))
            {
                if($model->delete())
                    Yii::log("Uid: " .Yii::app()->user->id. " Delete record ".  print_r($model->attributes, true), 'info');
            }

            // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
            if(!isset($_GET['ajax']))
                    $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('index'));
        }
        else
        {
            Yii::log("Uid: " .Yii::app()->user->id. " Invalid request. Please do not repeat this request again.");
            throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
        }	
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }

    /**
     * Manages all models.
     */
    public function actionIndex()
    {
        $this->pageTitle = 'KM khách hàng';
        $this->allowIndex();
        $this->renewPromotion();
        try{
        $model=new AppPromotionUser('search');
        $model->unsetAttributes();  // clear any default values
        if(isset($_GET['AppPromotionUser']))
                $model->attributes=$_GET['AppPromotionUser'];

        $this->render('index',array(
                'model'=>$model, 'actions' => $this->listActionsCanAccess,
        ));
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }

    /** @Author: DungNT Now 16, 2018
     *  @Todo: kiểm tra allow access index
     **/
    public function allowIndex(){
        return ;
        $cUid = MyFormat::getCurrentUid();
        $cRole = MyFormat::getCurrentRoleId();
        $aUidAllow  = [
            GasLeave::UID_CHIEF_MONITOR,
            GasLeave::PHUC_HV,
        ];
        $aRoleAllow = [ROLE_MONITORING_MARKET_DEVELOPMENT];
        if(in_array($cRole, $aRoleAllow) && !in_array($cUid, $aUidAllow)){
            $this->redirect(Yii::app()->createAbsoluteUrl(''));
        }
    }
    
    
    /** @Author: DungNT Sep 26, 2018
     *  @Todo: kiểm tra allow access report reportReferralTracking
     **/
    public function allowReportReferralTracking(){
        return ;
        $cUid = MyFormat::getCurrentUid();
        $cRole = MyFormat::getCurrentRoleId();
        $aUidAllow  = [
            GasLeave::UID_CHIEF_MONITOR,
            GasLeave::PHUC_HV,
        ];
        $aRoleAllow = [ROLE_MONITORING_MARKET_DEVELOPMENT];
        if(in_array($cRole, $aRoleAllow) && !in_array($cUid, $aUidAllow)){
            $this->redirect(Yii::app()->createAbsoluteUrl(''));
        }
    }
    
    /**
     * Returns the data model based on the primary key given in the GET variable.
    * If the data model is not found, an HTTP exception will be raised.
     * @param integer the ID of the model to be loaded
     */
    public function loadModel($id)
    {
        try{
        $model=AppPromotionUser::model()->findByPk($id);
        if($model===null)
        {
            Yii::log("The requested page does not exist.");
            throw new CHttpException(404,'The requested page does not exist.');
        }			
        return $model;
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }
    
    /** @Author: HOANG NAM 19/04/2018
     *  @Todo: export to excel
     *  @Code: NAM010
     **/
   
    public function actionExportExcel(){
        try{
        if(isset($_SESSION['data-excel']) && isset($_SESSION['data-model'])){
            ini_set('memory_limit','1500M');// chưa cần chạy đoạn này, đưa memory_limit lên 500 có thể export dc khoảng 5000 row - chưa test
            ToExcel::summaryAppPromotionUser();
        }
        $this->redirect(array('index'));
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }
    
    /** @Author: HOANG NAM 02/05/2018
     *  @Todo: report Marketing
     *  @Code: NAM012
     **/
    public function actionReportMarketing() {
        $this->pageTitle = 'Mã code Telesale';
        try{
            $cRole = MyFormat::getCurrentRoleId();
            $cUid = MyFormat::getCurrentUid();
            $model = new AppPromotionUser('search');
            $model->unsetAttributes();  // clear any default values
            $model->date_from   = '01-'.date('m-Y');
            $model->date_to     = date('d-m-Y');
            if(isset($_GET['AppPromotionUser'])){
                $model->attributes = $_GET['AppPromotionUser'];
            }
            if(in_array($cRole, $model->getReportMarketingRoleLimit())){
                $model->user_id = $cUid;
            }
            
            $this->render('report/_form_report_marketing',array(
                'model' => $model,
                'data' => $model->getReportMarketing(),
                'actions' => $this->listActionsCanAccess,
            ));
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }
    
    /** @Author: HOANG NAM 02/05/2018
     *  @Todo: report Marketing
     *  @Code: NAM012
     **/
    public function actionExportExcelReportMarketing() {
        try{
        if(isset($_SESSION['data-model-marketing'])){
            ini_set('memory_limit','1500M');// chưa cần chạy đoạn này, đưa memory_limit lên 500 có thể export dc khoảng 5000 row - chưa test
            ToExcel::summaryExportExcelReportMarketing();
        }
        $this->redirect(array('ReportMarketing'));
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }
   
    /** @Author: HOANG NAM 10/05/2018
     *  @Todo: report ReferralTracking  
     **/
    public function actionReportReferralTracking() {
//        $this->redirect(Yii::app()->createAbsoluteUrl(''));
// Close Oct2518 báo cáo này làm cho cpu tăng cao, ko nên làm kiểu bc tree nữa khi cây có trên 2k item thì sẽ truy vấn nhiều
        $this->pageTitle = 'Thống kê nhập mã, mua hàng - hình cây';
        $this->allowReportReferralTracking();
        try{
            $model = new ReportReferralTracking('search');
            $model->unsetAttributes();  // clear any default values
            if(isset($_GET['ReportReferralTracking'])){
                $model->attributes = $_GET['ReportReferralTracking'];
            }
            $model->getDataTree();

            $this->render('reportReferralTracking/_form_report',array(
                'model' => $model,
//                'htmlTree' => $model->getHtmlReportTree($model->getTreeReferralTracking()),
                'actions' => $this->listActionsCanAccess,
            ));
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }
    
    /** @Author: HOANG NAM 10/05/2018
     *  @Todo: report ReferralTracking
     **/
    public function actionReportReferralTrackingTable() {
        $this->pageTitle = 'Thống kê nhập mã giới thiệu dạng bảng Table View';
        try{
            $model = new ReportReferralTracking('search');
            $model->unsetAttributes();  // clear any default values
            if(isset($_GET['ReportReferralTracking'])){
                $model->attributes = $_GET['ReportReferralTracking'];
            }
            $model->ref_date   = true;
            $this->render('reportReferralTracking/_form_report_table',array(
                'model' => $model,
                'aData' => $model->getReportTable(),
                'actions' => $this->listActionsCanAccess,
            ));
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }
    
    /** @Author: DuongNV Oct 15, 18
     *  @Todo: report ReferralTracking
     **/
    public function actionReportCode() {
        $this->pageTitle = 'Mã code GĐKV';
        try{
            $model = new ReportReferralTracking('search');
            $model->unsetAttributes();  // clear any default values
            $model->date_from = date('01-m-Y');
            $model->date_to   = date('d-m-Y');
            if(isset($_GET['ReportReferralTracking'])){
                $model->attributes = $_GET['ReportReferralTracking'];
            }
            $this->exportExcelCode();
            $this->render('reportReferralTracking/_report_code',array(
                'model' => $model,
                'aData' => $model->getReportReferalTracking(),
                'actions' => $this->listActionsCanAccess,
            ));
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }
    
    /** @Author: DuongNV Oct 15, 18
     *  @Todo: excel report code
     **/
    public function exportExcelCode() {
        if(!isset($_GET['excel'])) return;
        try{
            if(isset($_SESSION['data-excel']) && isset($_SESSION['data-model'])){
                ini_set('memory_limit','1500M');// chưa cần chạy đoạn này, đưa memory_limit lên 500 có thể export dc khoảng 5000 row - chưa test
                ToExcel::summaryReportCode();
            }
            $this->redirect(array('index'));
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }
    
    /** @Author: NamNH Jan 30, 2019
     *  @Todo: Report code pttt
     **/
    public function actionCodePtttCheckLocation() {
        $this->pageTitle = 'BC tọa độ code PTTT';
        try{
            $aData = [];
            $this->exportExcelCodeLocation();
            $model = new AppPromotionUser('search');
            $model->unsetAttributes();  // clear any default values
            $model->date_from   = '01-'.date('m-Y');
            $model->date_to     = date('d-m-Y');
            if(isset($_GET['AppPromotionUser'])){
                $model->attributes = $_GET['AppPromotionUser'];
                $aData = $model->getReportCode();
            }
            $this->render('reportCode/_form_report',array(
                'model' => $model,
                'data'  => $aData,
                'actions' => $this->listActionsCanAccess,
            ));
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }
    
    /** @Author: NamNH Jan 30, 2019
     *  @Todo: export excel
     **/
    public function exportExcelCodeLocation(){
        try{
            if(isset($_GET['ToExcel'])){
                if(isset($_SESSION['data-model-code'])){
                    ini_set('memory_limit','1500M');// chưa cần chạy đoạn này, đưa memory_limit lên 500 có thể export dc khoảng 5000 row - chưa test
                    ToExcel::summaryExportExcelCodeLocation();
                }
                $this->redirect(array('ReportCodePTTT'));
            }
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }
    
    /** @Author: DuongNV Mar 5,19
     *  @Todo: gia hạn khuyến mãi
     **/
    public function renewPromotion() {
        if(isset($_GET['renew'])){
            $id     = isset($_GET['id']) ? $_GET['id'] : 0;
            $model  = AppPromotionUser::model()->findByPk($id);
            if($model && $model->canRenewPromotion() && $model->canRenewPromotionCheck2()){
                $model->renewPromotion();
                Yii::app()->user->setFlash(MyFormat::SUCCESS_UPDATE, 'Gia hạn thành công' );
            } else {
                Yii::app()->user->setFlash(MyFormat::ERROR_UPDATE, "Gia hạn không thành công. Khuyến mãi đã gia hạn hoặc ở trạng thái đã sử dụng. {$model->getError('id')}");
            }
            $this->redirect(array('index'));
        }
    }
}
