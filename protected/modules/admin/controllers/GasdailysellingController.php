<?php

class GasdailysellingController extends AdminController
{
//     public function accessRules()
//    {        
//        return array();
//    }  	  
    public function actionImport_dailyselling() {
        date_default_timezone_set('UTC');
        set_time_limit(7200);
//        echo date('Y-m', strtotime('next month',  strtotime('2013-12-07')));
//        die;
        
        
        $model=new GasDailySelling('import_dailyselling');
        if(isset($_POST['GasDailySelling']))
        {
                $model->attributes=$_POST['GasDailySelling'];
                $model->file_excel=$_FILES['GasDailySelling'];
                $model->validate();
                if(!$model->hasErrors()){
                    $this->importExcel($model);
                    $this->redirect(array('index','id'=>$model->id));
                }
                    
        }        
        $this->render('import_dailyselling',array(
                'model'=>$model, 'actions' => $this->listActionsCanAccess,
        ));        
    }
    
    
    public function importExcel($model){
                Yii::import('application.extensions.vendors.PHPExcel',true);
                $objReader = PHPExcel_IOFactory::createReader("Excel2007");
                
                $objPHPExcel = $objReader->load(@$_FILES['GasDailySelling']['tmp_name']['file_excel']);
//                $objWorksheet = $objPHPExcel->getActiveSheet();
                $objWorksheet = $objPHPExcel->getSheet(0);
                $highestRow = $objWorksheet->getHighestRow(); // e.g. 10
                //$highestColumn = $objWorksheet->getHighestColumn(); // e.g 'F'
                $highestColumn = 'O';
                $highestColumnIndex = PHPExcel_Cell::columnIndexFromString($highestColumn);

                $success = 1;
                $aRowInsert=array();
                $aRowInsertCash=array();
                $aCellError = array();
//                $aRowError = array();       
                $created_date=date('Y-m-d H:i:s');
                $uid = Yii::app()->user->id;
                $aCustomer =  Users::getArrCustomerForImport();
				$aSale =  Users::getArrSaleIdForImport();				
                $aCustomerPaymentDay =  Users::getArrPaymentDayOfCustomer();
                $aObjectTypePay = GasTypePay::getArrAllObject();
				$month = trim($objWorksheet->getCellByColumnAndRow(1, 2)->getValue());
				$year = trim($objWorksheet->getCellByColumnAndRow(2, 2)->getValue());				
				$aCustomerPriceInMonth = GasQuotation::getPriceLatestInMonth($month, $year);
				
                for ($row = 2; $row <= $highestRow; ++$row)
                {
                    // file excel cần format column theo dạng text hết để người dùng nhập vào khi đọc không lỗi
                    $d = trim($objWorksheet->getCellByColumnAndRow(0, $row)->getValue());
                    $m = trim($objWorksheet->getCellByColumnAndRow(1, $row)->getValue());
                    $y = trim($objWorksheet->getCellByColumnAndRow(2, $row)->getValue());
                    
                    $temp_customer_id = trim(trim($objWorksheet->getCellByColumnAndRow(3, $row)->getValue()));
					
                    if(isset($aCustomer[$temp_customer_id]))
                        $customer_id = $aCustomer[$temp_customer_id];
                    else{
                        $aCellError[] = ' Dòng '.$row.' Không có Khách Hàng: '.$temp_customer_id;
                        continue;
                    }
					
                    if(isset($aSale[$customer_id]))
                        $sale_id = $aSale[$customer_id];
                    else{
                        $aCellError[] = ' Dòng '.$row.' Không có sale id của Khách Hàng: '.$temp_customer_id;
                        continue;
                    }
					
                    $date_sell = $y.'-'.$m.'-'.$d;
                    $quantity_50 = trim($objWorksheet->getCellByColumnAndRow(4, $row)->getValue());
                    $quantity_45 = trim($objWorksheet->getCellByColumnAndRow(5, $row)->getValue());
                    $quantity_12 = trim($objWorksheet->getCellByColumnAndRow(6, $row)->getValue());
                    $quantity_remain = trim($objWorksheet->getCellByColumnAndRow(7, $row)->getValue());
					$quantity_50 = empty($quantity_50)?0:$quantity_50;
					$quantity_45 = empty($quantity_45)?0:$quantity_45;
					$quantity_12 = empty($quantity_12)?0:$quantity_12;
					$quantity_remain = empty($quantity_remain)?0:$quantity_remain;
                    $quantity_output = trim($objWorksheet->getCellByColumnAndRow(8, $row)->getValue());
                    $price_1_kg = trim($objWorksheet->getCellByColumnAndRow(9, $row)->getValue());
					$price_1_kg = empty($price_1_kg)?0:$price_1_kg;
					$total = round(trim($objWorksheet->getCellByColumnAndRow(10, $row)->getValue()));
					if($price_1_kg<1){
						if(isset($aCustomerPriceInMonth[$customer_id])){							
								$price_1_kg = $aCustomerPriceInMonth[$customer_id]->price_50;
								$price_b_12 = $aCustomerPriceInMonth[$customer_id]->price_12;
								$total=($quantity_50*KL_BINH_50 + $quantity_45*KL_BINH_45 - $quantity_remain) * $price_1_kg + ($quantity_12*$price_b_12) ;																									
						}else{
							$aCellError[] = ' Dòng '.$row.'Khong có báo giá của custome cua: '.$temp_customer_id;
							continue;
						}     					
					}
                    
                    if(isset($aCustomerPaymentDay[$customer_id]) && is_numeric($aCustomerPaymentDay[$customer_id])){
                        $type_pay_id = $aCustomerPaymentDay[$customer_id];
                        if($type_pay_id)
                            $date_of_payment = MyFunctionCustom::getDateOfPayment($date_sell, $aObjectTypePay[$type_pay_id], $d, $m, $y);
                    }
                    else{
                        $aCellError[] = ' Dòng '.$row.'Khong co han thanh toan trong table custome cua: '.$temp_customer_id;
                        continue;
                    }                    
                    
                    $aRowInsert[]="(
                        '$customer_id',
                        '$date_sell',
                        '$quantity_50',
                        '$quantity_45',
                        '$quantity_12',
                        '$quantity_remain',
                        '$quantity_output',
                        '$price_1_kg',
                        '$total',
                        '$date_of_payment',
						'$sale_id',						
                        '$uid',
                        '$created_date'    
                        )";
                    
                    // Nếu thu tiền luôn thì lưu sang bảng thu tiền
                    $cash = round(trim($objWorksheet->getCellByColumnAndRow(11, $row)->getValue()));
                    $d1 = trim($objWorksheet->getCellByColumnAndRow(12, $row)->getValue());
                    if($cash!=''){					
						if($d1 !=''){
							$m1 = trim($objWorksheet->getCellByColumnAndRow(13, $row)->getValue());
							$y1 = trim($objWorksheet->getCellByColumnAndRow(14, $row)->getValue());                    
							$date_collection = $y1.'-'.$m1.'-'.$d1;						
						}
						else
							$date_collection = $date_sell;		
                        $aRowInsertCash[]="(
                        '$customer_id',
                        '$date_collection',
                        '$cash',
						'$sale_id',	
                        '$uid',
                        '$created_date'    
                        )";
                    }
                }
                
            if(count($aCellError)>0){
                $mLog = new GasImportLog();
                $mLog->user_id = $uid;
                $mLog->created_date = $created_date;
                $mLog->json = json_encode($aCellError);
                $mLog->save();
                echo 'Nếu gặp lỗi này hãy giữ nguyên (và chụp ) màn hình và thông báo lại cho Anh Dũng';
                echo '<pre>';
                echo print_r($aCellError);
                echo '</pre >';
                die;
            }				
		
            // save to GasDailySelling
            $tableName = GasDailySelling::model()->tableName();
            $sql = "insert into $tableName (
                            customer_id,
                            date_sell,
                            quantity_50,
                            quantity_45,
                            quantity_12,
                            quantity_remain,
                            quantity_output,
                            price_1_kg,
                            total,
                            date_of_payment,
							sale_id,
                            user_id_create,
                            created_date
                            ) values ".implode(',', $aRowInsert);
            
            Yii::app()->db->createCommand($sql)->execute();
            // end save to GasDailySelling
            // save to GasDailyCash
            if(count($aRowInsertCash)>0){
                // Nếu thu tiền luôn thì lưu sang bảng thu tiền
                $tableName = GasDailyCash::model()->tableName();
                $sql = "insert into $tableName (
                                customer_id,
                                date_collection,
                                total,
								sale_id,
                                user_id_create,
                                created_date
                                ) values ".implode(',', $aRowInsertCash);
                Yii::app()->db->createCommand($sql)->execute();                                
            }
            // end save to GasDailyCash
            
            return $aCellError;
        }    
        
    
    public function actionExport_dailyselling() {
        set_time_limit(72000);       
        
        $model=new GasDailySelling();
		$model->unsetAttributes();  // clear any default values
		if(isset($_POST['GasDailySelling'])){
		
			$model->attributes=$_POST['GasDailySelling'];
			$model->searchExport();	
		}
		
        $this->render('export_dailyselling',array(
                'model'=>$model, 'actions' => $this->listActionsCanAccess,
        ));        
    }    
    
	public function actionView($id)
	{
                try{
                $this->render('view',array(
			'model'=>$this->loadModel($id), 'actions' => $this->listActionsCanAccess,
		));
                }
                catch (Exception $e)
                {
                    Yii::log("Uid: " .Yii::app()->user->id. "Exception ".  $e->getMessage(), 'error');
                    $code = 404;
                    if(isset($e->statusCode))
                        $code=$e->statusCode;
                    if($e->getCode())
                        $code=$e->getCode();
                    throw new CHttpException($code, $e->getMessage());
                }
		
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
                try
                {
		$model=new GasDailySelling('create_DailySelling');

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['GasDailySelling']))
		{
			$model->attributes=$_POST['GasDailySelling'];
                        $model->user_id_create = Yii::app()->user->id;
                        $model->created_date = date('Y-m-d H:i:s');
                        
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('create',array(
			'model'=>$model, 'actions' => $this->listActionsCanAccess,
		));
                }
                catch (Exception $e)
                {
                    Yii::log("Uid: " .Yii::app()->user->id. "Exception ".  $e->getMessage(), 'error');
                    $code = 404;
                    if(isset($e->statusCode))
                        $code=$e->statusCode;
                    if($e->getCode())
                        $code=$e->getCode();
                    throw new CHttpException($code, $e->getMessage());
                }
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
                try
                {
		$model=$this->loadModel($id);
                $model->scenario = 'update_DailySelling';
                $model->date_sell = MyFormat::dateConverYmdToDmy($model->date_sell);
                $model->date_of_payment = MyFormat::dateConverYmdToDmy($model->date_of_payment);
                $model->autocomplete_name = $model->customer?$model->customer->first_name:'';
		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['GasDailySelling']))
		{
			$model->attributes=$_POST['GasDailySelling'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}
                
		$this->render('update',array(
			'model'=>$model, 'actions' => $this->listActionsCanAccess,
		));
                }
                catch (Exception $e)
                {
                    Yii::log("Uid: " .Yii::app()->user->id. "Exception ".  $e->getMessage(), 'error');
                    $code = 404;
                    if(isset($e->statusCode))
                        $code=$e->statusCode;
                    if($e->getCode())
                        $code=$e->getCode();
                    throw new CHttpException($code, $e->getMessage());
                }
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
                try
                {
		if(Yii::app()->request->isPostRequest)
		{
			// we only allow deletion via POST request
			if($model = $this->loadModel($id))
                        {
                            if($model->delete())
                                Yii::log("Delete record ".  print_r($model->attributes, true), 'info');
                        }

			// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
			if(!isset($_GET['ajax']))
				$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('index'));
		}
		else
                {
                    Yii::log("Invalid request. Please do not repeat this request again.");
                    throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
                }	
                }
                catch (Exception $e)
                {
                    Yii::log("Uid: " .Yii::app()->user->id. "Exception ".  $e->getMessage(), 'error');
                    throw new CHttpException(400,$e->getMessage());
                }
	}

	/**
	 * Manages all models.
	 */
	public function actionIndex()
	{
            //GasDailySelling::getPriceLatestInMonth(6, 2013);
	   
                try
                {
		$model=new GasDailySelling('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['GasDailySelling']))
			$model->attributes=$_GET['GasDailySelling'];

		$this->render('index',array(
			'model'=>$model, 'actions' => $this->listActionsCanAccess,
		));
                }
                catch (Exception $e)
                {
                    Yii::log("Uid: " .Yii::app()->user->id. "Exception ".  $e->getMessage(), 'error');
                    $code = 404;
                    if(isset($e->statusCode))
                        $code=$e->statusCode;
                    if($e->getCode())
                        $code=$e->getCode();
                    throw new CHttpException($code, $e->getMessage());
                }
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the ID of the model to be loaded
	 */
	public function loadModel($id)
	{
                try
                {
		$model=GasDailySelling::model()->findByPk($id);
		if($model===null)
                {
                    Yii::log("The requested page does not exist.");
                    throw new CHttpException(404,'The requested page does not exist.');
                }			
		return $model;
                }
                catch (Exception $e)
                {
                    Yii::log("Uid: " .Yii::app()->user->id. "Exception ".  $e->getMessage(), 'error');
                    $code = 404;
                    if(isset($e->statusCode))
                        $code=$e->statusCode;
                    if($e->getCode())
                        $code=$e->getCode();
                    throw new CHttpException($code, $e->getMessage());
                }
	}

	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
                try
                {
		if(isset($_POST['ajax']) && $_POST['ajax']==='gas-daily-selling-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
                }
                catch (Exception $e)
                {
                    Yii::log("Uid: " .Yii::app()->user->id. "Exception ".  $e->getMessage(), 'error');
                    $code = 404;
                    if(isset($e->statusCode))
                        $code=$e->statusCode;
                    if($e->getCode())
                        $code=$e->getCode();
                    throw new CHttpException($code, $e->getMessage());
                }
	}
}
