<?php

class HrInterviewPlanController extends AdminController 
{
    public $pluralTitle = 'Lịch phỏng vấn';
    public $singleTitle = 'Lịch phỏng vấn';
    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id)
    {
        $this->pageTitle = 'Xem ';
        try{
            $detail = $this->loadModel($id)->getDetailInfo();
        $this->render('view',array(
                'model'=>$this->loadModel($id), 'actions' => $this->listActionsCanAccess,
            'detail_info' => $detail,
        ));
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }

    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate()
    {
        $this->pageTitle = 'Tạo Mới ';
        try{
        $model=new HrInterviewPlan('create');
        // Get parameter from url
        $this->validateCreateUrl($model);

        if(isset($_POST['HrInterviewPlan']))
        {
            $model->attributes=$_POST['HrInterviewPlan'];
            $model->validate();
            if(!$model->hasErrors()){
                $model->save();
                GasScheduleEmail::notifyInterviewPlan($model);
                Yii::app()->user->setFlash( MyFormat::SUCCESS_UPDATE, "Thêm mới thành công." );
                $this->redirect(array('view', 'id'=>$model->id));
            }
        }

        $this->render('create',array(
                'model'=>$model, 'actions' => $this->listActionsCanAccess,
        ));
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }
    
    /**
     * Validate create url and get parameter
     * @param Object $model Model
     */
    public function validateCreateUrl(&$model) {
        $model->plan_id         = isset($_GET['plan_id']) ? $_GET['plan_id'] : '';
        $model->candidate_id    = isset($_GET['candidate_id']) ? $_GET['candidate_id'] : '';
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id)
    {
        $this->pageTitle = 'Cập Nhật ';
        try{
        $model=$this->loadModel($id);
        $model->scenario = 'update';
        if(isset($_POST['HrInterviewPlan']))
        {
            $model->attributes=$_POST['HrInterviewPlan'];
                    // $this->redirect(array('view','id'=>$model->id));
            $model->validate();
            if(!$model->hasErrors()){
                $model->save();
                Yii::app()->user->setFlash( MyFormat::SUCCESS_UPDATE, "Cập nhật thành công." );
                $this->redirect(array('update','id'=>$model->id));
            }							
        }

        $this->render('update',array(
                'model'=>$model, 'actions' => $this->listActionsCanAccess,
        ));
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id)
    {
        try{
        if(Yii::app()->request->isPostRequest){
            // we only allow deletion via POST request
            if($model = $this->loadModel($id))
            {
                if($model->delete())
                    Yii::log("Uid: " .Yii::app()->user->id. " Delete record ".  print_r($model->attributes, true), 'info');
            }

            // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
            if(!isset($_GET['ajax']))
                    $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('index'));
        }
        else
        {
            Yii::log("Uid: " .Yii::app()->user->id. " Invalid request. Please do not repeat this request again.");
            throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
        }	
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }

    /**
     * Manages all models.
     */
    public function actionIndex()
    {
        $this->pageTitle = 'Danh Sách ';
        try{
        $model=new HrInterviewPlan('search');
        $model->unsetAttributes();  // clear any default values
//        $model->plan_id = $plan_id;
        if(isset($_GET['HrInterviewPlan']))
                $model->attributes=$_GET['HrInterviewPlan'];

        $this->render('index',array(
                'model'=>$model, 'actions' => $this->listActionsCanAccess,
        ));
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }
    /**
     * Confirm a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionConfirm($id) {
        $this->pageTitle = 'Xác nhận lịch phỏng vấn ';
        try {
            $model = $this->loadModel($id);
            $model->scenario = 'update';
            if (isset($_POST['HrInterviewPlan'])) {
                $model->attributes = $_POST['HrInterviewPlan'];
                // $this->redirect(array('view','id'=>$model->id));
                $model->status = HrInterviewPlan::STATUS_CONFIRMED;
                $model->validate();
                if (!$model->hasErrors()) {
                    $model->save();
                    GasScheduleEmail::notifyInterviewPlanCandidate($model);
                    Yii::app()->user->setFlash(MyFormat::SUCCESS_UPDATE, "Cập nhật thành công.");
                    $this->redirect(array('view', 'id' => $model->id));
                }
            }

            $this->render('confirm', array(
                'model' => $model,
                'actions' => $this->listActionsCanAccess,
            ));
        } catch (Exception $exc) {
            GasCheck::CatchAllExeptiong($exc);
        }
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
    * If the data model is not found, an HTTP exception will be raised.
     * @param integer the ID of the model to be loaded
     */
    public function loadModel($id)
    {
        try{
        $model=HrInterviewPlan::model()->findByPk($id);
        if($model===null)
        {
            Yii::log("The requested page does not exist.");
            throw new CHttpException(404,'The requested page does not exist.');
        }			
        return $model;
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }

}
