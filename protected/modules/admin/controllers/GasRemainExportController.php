<?php

class GasRemainExportController extends AdminController 
{
    public function actionView($id)
    {
            $this->pageTitle = 'Xem ';
            try{
                
                $model = $this->loadModel($id);
                if(Yii::app()->user->id==ROLE_SUB_USER_AGENT && Yii::app()->user->id != $model->uid_login){
                    $this->redirect(array('index'));
                }
                GasRemainExport::getArrModelRemain($model);
                $this->render('view',array(
                        'model'=>$model, 'actions' => $this->listActionsCanAccess,
                ));
            
            }
            catch (Exception $exc)
            {
                Yii::log("Uid: " .Yii::app()->user->id. " Exception ".  $exc->getMessage(), 'error');
                $code = 404;
                if(isset($exc->statusCode))
                    $code=$exc->statusCode;
                if($exc->getCode())
                    $code=$exc->getCode();
                throw new CHttpException($code, $exc->getMessage());
            }

    }

    public function actionCreate()
    {
        $this->pageTitle = 'Tạo Mới ';
            try
            {
            $model=new GasRemainExport('create');
            $this->ajaxGetRemainList($model);
            if(isset($_POST['GasRemainExport']))
            {
                $model->attributes=$_POST['GasRemainExport'];
                $model->validate();
                if(!$model->hasErrors()){
                    $model->save();                    
                    $model = GasRemainExport::model()->findByPk($model->id);
                    GasRemainExport::OverideSomeFieldAfterSave($model);
                    GasRemainExport::SaveDetail($model);
                    GasRemainExport::UpdateStatusHasExport($model, 1);
                    $this->redirect(array('view','id'=>$model->id));
                    Yii::app()->user->setFlash('successUpdate', "Thêm mới thành công.");
                   $this->redirect(array('create'));
                }				
            }

            $this->render('create',array(
                    'model'=>$model, 'actions' => $this->listActionsCanAccess,
            ));
            }
            catch (Exception $exc)
            {
                Yii::log("Uid: " .Yii::app()->user->id. " Exception ".  $exc->getMessage(), 'error');
                $code = 404;
                if(isset($exc->statusCode))
                    $code=$exc->statusCode;
                if($exc->getCode())
                    $code=$exc->getCode();
                throw new CHttpException($code, $exc->getMessage());
            }
    }
    
    /**
     * @Author: ANH DUNG Jun 22, 2014
     * @Todo: khi user khoảng ngày để in hoặc nhập mã cân lần 1, sẽ lấy list những gas dư đã cân 
     * @Param: $model model GasRemainExport
     */
    public function ajaxGetRemainList(&$model){
        if(isset($_GET['date_from']) && isset($_GET['date_to'])){
            $model->aModelRemain = GasRemain::getRemainNotExportByDateRange($_GET['date_from'], $_GET['date_to']);            
        }elseif(isset($_GET['parent_id']) && isset($_GET['ajax_parent'])){
            $model = GasRemainExport::getInfoModelRemainByPk($_GET['parent_id']);            
        }        
    }
    
    public function actionUpdate($id)
    {
            $this->pageTitle = 'Cập Nhật ';
            try
            {
            $model=$this->loadModel($id);
            if(!GasCheck::AgentCanUpdateRemainExport($model)){
                $this->redirect(array('index'));
            }
            $model->scenario = 'update';
            // lấy thêm 1 số biến
            GasRemainExport::getArrModelRemain($model);

            if(isset($_POST['GasRemainExport']))
            {
                $model->attributes=$_POST['GasRemainExport'];
                $model->validate();
                if(!$model->hasErrors()){
                    $model->save();
                    GasRemainExport::SaveDetail($model);
                    $this->redirect(array('view','id'=>$model->id));
                    Yii::app()->user->setFlash('successUpdate', "Cập nhật thành công.");
                    $this->redirect(array('update','id'=>$model->id));
                }							
            }

            $this->render('update',array(
                    'model'=>$model, 'actions' => $this->listActionsCanAccess,
            ));
            }
            catch (Exception $exc)
            {
                Yii::log("Uid: " .Yii::app()->user->id. " Exception ".  $exc->getMessage(), 'error');
                $code = 404;
                if(isset($exc->statusCode))
                    $code=$exc->statusCode;
                if($exc->getCode())
                    $code=$exc->getCode();
                throw new CHttpException($code, $exc->getMessage());
            }
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id)
    {
            try
            {
            if(Yii::app()->request->isPostRequest)
            {
                // we only allow deletion via POST request
                $model = $this->loadModel($id);
                if($model && GasRemainExport::canDeleteRecordFirst($model) && GasCheck::canDeleteData($model))
                {
                    if($model->delete())
                        Yii::log("Uid: " .Yii::app()->user->id. " Delete record ".  print_r($model->attributes, true), 'info');
                }else
                    throw new CHttpException(400,'Đây là lần cân đầu tiên,không thể xóa phiếu này, vì phiếu này có nhiều hơn 1 lần cân, ban phải xóa những lần cân sau');

                // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
                if(!isset($_GET['ajax']))
                        $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('index'));
            }
            else
            {
                Yii::log("Uid: " .Yii::app()->user->id. " Invalid request. Please do not repeat this request again.");
                throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
            }	
            }
            catch (Exception $exc)
            {
                Yii::log("Uid: " .Yii::app()->user->id. " Exception ".  $exc->getMessage(), 'error');
                $code = 404;
                if(isset($exc->statusCode))
                    $code=$exc->statusCode;
                if($exc->getCode())
                    $code=$exc->getCode();
                throw new CHttpException($code, $exc->getMessage());
            }
    }

    /**
     * Manages all models.
     */
    public function actionIndex()
    {
            $this->pageTitle = 'Danh Sách ';
            try
            {
            $model=new GasRemainExport('search');
            $model->unsetAttributes();  // clear any default values
            if(isset($_GET['GasRemainExport']))
                    $model->attributes=$_GET['GasRemainExport'];

            $this->render('index',array(
                    'model'=>$model, 'actions' => $this->listActionsCanAccess,
            ));
            }
            catch (Exception $exc)
            {
                Yii::log("Uid: " .Yii::app()->user->id. " Exception ".  $exc->getMessage(), 'error');
                $code = 404;
                if(isset($exc->statusCode))
                    $code=$exc->statusCode;
                if($exc->getCode())
                    $code=$exc->getCode();
                throw new CHttpException($code, $exc->getMessage());
            }
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
    * If the data model is not found, an HTTP exception will be raised.
     * @param integer the ID of the model to be loaded
     */
    public function loadModel($id)
    {
            try
            {
            $model=GasRemainExport::model()->findByPk($id);
            if($model===null)
            {
                Yii::log("The requested page does not exist.");
                throw new CHttpException(404,'The requested page does not exist.');
            }			
            return $model;
            }
            catch (Exception $exc)
            {
                Yii::log("Uid: " .Yii::app()->user->id. " Exception ".  $exc->getMessage(), 'error');
                $code = 404;
                if(isset($exc->statusCode))
                    $code=$exc->statusCode;
                if($exc->getCode())
                    $code=$exc->getCode();
                throw new CHttpException($code, $exc->getMessage());
            }
    }

    /**
     * Performs the AJAX validation.
     * @param CModel the model to be validated
     */
    protected function performAjaxValidation($model)
    {
            try
            {
            if(isset($_POST['ajax']) && $_POST['ajax']==='gas-remain-export-form')
            {
                    echo CActiveForm::validate($model);
                    Yii::app()->end();
            }
            }
            catch (Exception $exc)
            {
                Yii::log("Uid: " .Yii::app()->user->id. " Exception ".  $exc->getMessage(), 'error');
                $code = 404;
                if(isset($exc->statusCode))
                    $code=$exc->statusCode;
                if($exc->getCode())
                    $code=$exc->getCode();
                throw new CHttpException($code, $exc->getMessage());
            }
    }
}
