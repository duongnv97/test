<?php

class GasstorecardController extends AdminController 
{ 
    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id)
    {
        $this->pageTitle = 'Xem Thẻ Kho';
        try{
            $model = $this->loadModel($id);
        if(MyFormat::getCurrentRoleId()==ROLE_SUB_USER_AGENT && $model->user_id_create != MyFormat::getAgentId() )
            $this->redirect(array('index'));

        $this->render('view',array(
                'model'=>$model, 'actions' => $this->listActionsCanAccess,
        ));
        }catch (Exception $exc){
        GasCheck::CatchAllExeptiong($exc);
        }
    }
    
    /**
     * @Author: ANH DUNG Mar 13, 2017
     * @Todo: checkLockAgentRunApp
     */
    public function checkLockAgentRunApp($model) {
        if($model->isLockAgentRunApp()){
            throw new Exception('Bạn không thể tạo thẻ kho, chức năng này bị tắt với đại lý của bạn');
        }
    }
    
    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate()
    {
//        GasCheck::FunctionMaintenance();
        $this->pageTitle = 'Tạo Mới Thẻ Kho';
        try{
            $model=new GasStoreCard('create_customer_pay_now');
            $this->checkLockAgentRunApp($model);
            $model->modelUser = new Users('create_customer_store_card');
            $model->ext_is_new_customer = CUSTOMER_PAY_NOW;
            $model->date_delivery       = date('d/m/Y');
            $this->validateRequest($model);
            if (isset($_GET['support_customer_id']) && !empty($_GET['support_customer_id'])) {
                $model->loadMaterialsInGasSupportCustomer();
            }
            if(isset($_POST['GasStoreCard']))
            {
                $model->attributes=$_POST['GasStoreCard'];
                $model->modelUser->attributes = $_POST['Users'];
                if($model->ext_is_new_customer==CUSTOMER_NEW){
                    $model->scenario = 'create_customer_new';
                    $model->modelUser->validate();
                }
                elseif($model->ext_is_new_customer==CUSTOMER_OLD){
                    $model->scenario = 'create_customer_old';
                }
                else {
                    $model->scenario = 'create_customer_pay_now';
                    $model->customer_id = 0;
                }

                // Khi click button Load Template chỉ load dữ liệu, không save data DuongNV
                if( isset($_POST['loadTemplate']) ){
                    $model->loadTemplate();
                } else {
                    $model->validate();
                    $model->checkChanHang();
                    if(!$model->hasErrors() && !$model->modelUser->hasErrors()){
                        if($model->ext_is_new_customer==CUSTOMER_NEW){
                            MyFunctionCustom::saveCustomerStoreCard($model->modelUser);
                            $model->customer_id = $model->modelUser->id;
                        }
                        $this->saveStoreCard($model);
                        $model->appOrderUpdateBack();
                        $model->autoMakeRecordForAgent();// Jul2817 chặn tạm trên web lại
        //                        $mUserLogin = Users::model()->findByPk(Yii::app()->user->id);// only for test
        //                        $model->makeSocketNotify($mUserLogin);// only for test

                        GasStoreCardDetail::updateFirstRowCollectionCustomer($model);
                        $msg = 'Thêm mới xuất kho thành công';
                        if($model->type_store_card == TYPE_STORE_CARD_IMPORT)
                                $msg = 'Thêm mới nhập kho thành công';
                            Yii::app()->user->setFlash('successUpdate', $msg);
                       $this->redirect(array('create','type_store_card'=>$model->type_store_card, 'view_id'=>$model->id));
                    }
                }
            }

            $this->render('create',array(
                    'model'=>$model, 'actions' => $this->listActionsCanAccess,
            ));
        }catch (Exception $exc){
        GasCheck::CatchAllExeptiong($exc);
        }
    }

    public function validateRequest($model){
        if(isset($_GET['type_store_card']) && $_GET['type_store_card']== TYPE_STORE_CARD_IMPORT){
            $model->type_store_card = TYPE_STORE_CARD_IMPORT;
        }elseif(isset($_GET['type_store_card']) && $_GET['type_store_card']== TYPE_STORE_CARD_EXPORT){
            $model->type_store_card = TYPE_STORE_CARD_EXPORT;
        } else {
            throw new CHttpException(404, 'Type of store card not valid');
        }
    }

    /** 12-17-2013 to processing save and update StoreCard
     * DL028 5 PN_00103, 
     * CAT001 6 PN_00000540
     * LONG001 7 PN_00005034
     * DL0281500103
     * LONG00115 12345
     */
    public function saveStoreCard($model){
        if(isset($_POST['materials_id']) && count($_POST['materials_id'])>0){
            $aType = array(TYPE_STORE_CARD_IMPORT, TYPE_STORE_CARD_EXPORT);
            if(!in_array($model->type_store_card, $aType)){
                throw new CHttpException(404, 'Type of store card not valid');
            }
            $model->mapInfoUpdate();
            $model->getTypeCustomer();// Apr 14, 2015 cái loại KH này có thể cho đến n cũng dc, chỉ cần đưa vào is_maintain
            $model->mapSomeFieldOnWeb();// Jul 12, 2016
            
//            if(!$model->canMakeSellHgd()){// Sep 07, 2016
//                throw new CHttpException(404, 'Không thể tạo thẻ kho Xuất Bán, Xuất Tặng, Nhập Vỏ hộ gia đình. Hệ thống tự sinh ra những thẻ kho này từ phần mềm bán hàng mới. Mọi thắc mắc vui lòng liên hệ Dũng phòng IT');
//            }// Sep 27, 2016 tạm close lại để các đại lý chạy chuyển đổi, đầu tháng 10 sẽ mở lại
            if(!$model->canMakeStorecardBoMoi()){// Oct 09, 2016
                throw new CHttpException(404, 'Không thể tạo thẻ kho Xuất Bán cho khách hàng bò mối. Bạn phải cập nhật những thẻ kho do điếu phối tạo. Mọi thắc mắc vui lòng liên hệ Dũng phòng IT 01684 331 552, Kiên 0988 180 386');
            }// Sep 27, 2016 tạm close lại để các đại lý chạy chuyển đổi, đầu tháng 10 sẽ mở lại
            
            
            $model->buildStoreCardNo();
            $model->save();
            GasStoreCard::saveStoreCardDetail($model);
        }
    }
    
    /**
     * @Author: ANH DUNG Jan 12, 2015
     * @Todo: tính toán độ dài cho mã thẻ kho
     * CH161500002 
     * CH1615000001
     */
    public function GetLengthCode() {
        $length = MyFormat::MAX_LENGTH_12;
        return MyFormat::GetLengthCode($length);
    }

    public function checkUpdateValid($model){
//        $isCreateToDay = MyFormat::compareTwoDate($model->created_date, date('Y-m-d'));
        if( MyFormat::getCurrentRoleId() == ROLE_SUB_USER_AGENT && $model->user_id_create != MyFormat::getAgentId() || !$model->canUpdate())
            $this->redirect(array('index'));
//            throw new CHttpException(404, 'StoreCard Not Allow. Bạn Không Được Phép Truy Cập Hoặc Trang Bạn Yêu Cầu Không Tồn Tại. ');
    }
        
    public function actionUpdate($id)
    {
//        GasCheck::FunctionMaintenance();
        try{
        $model=$this->loadModel($id);
        $model->modelOld = clone $model;
        $this->checkLockAgentRunApp($model);
        $this->checkUpdateValid($model);
        $this->handleChangeAgent($model);
        $model->scenario = 'update';
        if(empty($model->customer_id)){
            $model->scenario = 'update_customer_pay_now';
            $model->ext_is_new_customer = CUSTOMER_PAY_NOW;
        }else{
            $model->ext_is_new_customer = CUSTOMER_OLD;
        }

        $model->modelUser = new Users('create_customer_store_card');

//            $old_first_name = $model->modelUser?$model->modelUser->first_name:''; // Quan trọng: không cho cập nhật KH khi update thẻ kho
//            // không bao giờ làm kiểu này nữa, sẽ không có edit khách hàng khi update 
//            // chỉ có tạo mới và autocomplete search thôi
//            // bên pttt có làm kiểu cho edit thông tin KH khi update record=>failed => không bao giờ làm update thông tin kh khi update row
//            chỉ cho phép tạo mới
        $model->date_delivery = MyFormat::dateConverYmdToDmy($model->date_delivery);
        $model->aModelStoreCardDetail = GasStoreCardDetail::getByStoreCardId($model->id);
        $this->pageTitle = "$model->store_card_no - {$model->getCustomer()} - {$model->date_delivery} - {$model->getCustomer('address')}";
        
        if(isset($_POST['GasStoreCard'])){
            $model->attributes=$_POST['GasStoreCard'];
            $model->modelUser->attributes = $_POST['Users'];
            $this->handleNotMakeOrder($model);
            
            if(!$model->canUpdateNhapVo()){
                throw new Exception('Không thể cập nhật thẻ kho thu vỏ, phải cập nhật từ app của giao nhận');
            }
            
            if($model->ext_is_new_customer==CUSTOMER_NEW){
                $model->scenario = 'create_customer_new';
                $model->modelUser->validate();
            }
            elseif($model->ext_is_new_customer==CUSTOMER_OLD){
                $model->scenario = 'create_customer_old';
                if($model->customer_id==0)
                    $model->customer_id='';
            }
            else {
                $model->scenario = 'create_customer_pay_now';
                $model->customer_id = 0;
            }

            $model->validate();
            $model->checkChanHang();
//                    $model->modelUser->validate();
            if(!$model->hasErrors() && !$model->modelUser->hasErrors()){
//                if(!$model->hasErrors()){
                    if($model->ext_is_new_customer==CUSTOMER_NEW){
                        MyFunctionCustom::saveCustomerStoreCard($model->modelUser);
                        $model->customer_id = $model->modelUser->id;
                    }                        
                    $model->fromWebUpdate = true;
                    $this->saveStoreCard($model);
                    $model->appOrderUpdateBack();
                    $model->autoMakeRecordForAgent();// Jul2817 chặn tạm trên web lại
                    GasStoreCardDetail::updateFirstRowCollectionCustomer($model);
                    $model->saveLogUpdate(LogUpdate::TYPE_5_GAS_STORECARD);
                    $model->handleAutoCreateDebts(); // DuongNV testing
                    $msg = 'Cập nhật xuất kho thành công';
                    if($model->type_store_card = TYPE_STORE_CARD_IMPORT)
                            $msg = 'Cập nhật nhập kho thành công';

                    Yii::app()->user->setFlash('successUpdate', $msg);
                    $this->redirect(array('update','id'=>$model->id, 'view_id'=>$model->id));
            }
        }

        $this->render('update',array(
                'model'=>$model, 'actions' => $this->listActionsCanAccess,
        ));
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }
    
    /**
     * @Author: ANH DUNG Nov 05, 2016
     * @Todo: xử lý cập nhật ghi chú  và xóa detail cho 1 số đơn hàng ko giao gas
     */
    public function handleNotMakeOrder($model) {
        if(isset($_POST['GasStoreCard']['reason_false']) && !empty($_POST['GasStoreCard']['reason_false'])){
            $model->handleCancel();
            Yii::app()->user->setFlash(MyFormat::SUCCESS_UPDATE, 'Cập nhật lý do không giao gas thành công');
            $this->redirect(array('update','id'=>$model->id, 'view_id'=>$model->id));
        }
    }
    
    /**
     * @Author: ANH DUNG Oct 10, 2016
     * @Todo: chuyển đại lý
     */
    public function handleChangeAgent($model) {
        if(isset($_GET['ChangeAgent']) && isset($_POST['agent_change_id'])){
            $cUid = Myformat::getCurrentUid();
            $model->user_id_create  = $_POST['agent_change_id'];
            $model->last_update_by  = $cUid;
            $model->last_update_time  = date("Y-m-d H:i:s");
            $model->scenario        = "KhoPhuocTanChangeAgent";
            $model->update(array('user_id_create', 'last_update_by', 'last_update_time'));
            die;
        }
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id)
    {
        try
        {
        if(Yii::app()->request->isPostRequest)
        {
            // we only allow deletion via POST request
            if($model = $this->loadModel($id))
            {
                if(GasStoreCard::canDelete($model)){
                    $model->deleteAppOrder();// Jan 21, 2017 - chỉ xử lý deleteAppOrder ở controller này, không được đưa vào before delete. Nó sẽ bị xóa sai khi bên AppOrder delete storecard_id_gas+ vỏ trc khi insert (function handleConfirm() => $this->deleteStorecard())
                    if($model->delete()){
                        Yii::log("Uid: " .Yii::app()->user->id. " Delete record ".  print_r($model->attributes, true), 'info');
                    }
                }
            }

            // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
            if(!isset($_GET['ajax']))
                    $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('index'));
        }
        else
        {
            Yii::log("Uid: " .Yii::app()->user->id. " Invalid request. Please do not repeat this request again.");
            throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
        }	
        }catch (Exception $exc){
        GasCheck::CatchAllExeptiong($exc);
        }
    }

    /**
     * Manages all models.
     */
    public function actionIndex()
    {
        $this->pageTitle = 'StoreCard - Danh Sách Thẻ Kho';
        try{
        $this->toExcelList();
        $this->checkRoleLogin();
        $model=new GasStoreCard('search');
        $model->initSessionAgentSell();
        MyFormat::initSessionModelAgent(MyFormat::getAgentId());
        GasCheck::sessionUserStart(GasCheck::SESSION_USER_STORE_CARD, array(ROLE_CAR, ROLE_DRIVER, ROLE_PHU_XE, ROLE_EMPLOYEE_MAINTAIN));
        $model->unsetAttributes();  // clear any default values
        if(isset($_GET['GasStoreCard']))
            $model->attributes=$_GET['GasStoreCard'];

        $this->render('index',array(
                'model'=>$model, 'actions' => $this->listActionsCanAccess,
        ));
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer the ID of the model to be loaded
     */
    public function loadModel($id)
    {
        try
        {
        $model=GasStoreCard::model()->findByPk($id);
        if($model===null)
        {
            Yii::log("The requested page does not exist.");
            throw new CHttpException(404,'The requested page does not exist.');
        }			
        return $model;
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }

    /**
     * Performs the AJAX validation.
     * @param CModel the model to be validated
     */
    protected function performAjaxValidation($model)
    {
        try
        {
        if(isset($_POST['ajax']) && $_POST['ajax']==='gas-store-card-form')
        {
                echo CActiveForm::validate($model);
                Yii::app()->end();
        }
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }

    /**
     * @Author: ANH DUNG 02-05-2014
     * @Todo: Nhập Dư Đầu Kỳ Vật Tư - nhập xuất tồn
     */    
    public function actionMaterials_opening_balance() {
        $this->pageTitle = 'Cập nhật dư đầu kỳ cho vật tư';
        try {
            $model = new GasMaterialsOpeningBalance();
            if(isset($_POST['GasMaterialsOpeningBalance'])
                    && in_array(Yii::app()->user->parent_id, CmsFormatter::$PROVINCE_ALLOW)
            ){
                GasMaterialsOpeningBalance::deleteByAgent(MyFormat::getAgentId());
                GasMaterialsOpeningBalance::saveOpeningBalance(MyFormat::getAgentId());
                $msg = 'Cập nhật nhập thành công';
                Yii::app()->user->setFlash('successUpdate', $msg);
                $this->redirect(array('materials_opening_balance'));
            }
            $this->render('Materials_opening_balance',array(
                    'model'=>$model, 'actions' => $this->listActionsCanAccess,
            ));
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }

    /**
     * @Author: ANH DUNG 02-05-2014
     * @Todo: Xem và in ấn thẻ kho
     */     
    public function actionView_store_card() {
        MyFunctionCustom::checkChangePassword();
        set_time_limit(7200);
        $this->pageTitle = 'Xem và in ấn thẻ kho';
        try {
            $data   = [];
            $model  = new GasStoreCard();
            $model->date_delivery = date('d-m-Y');
            if(isset($_POST['GasStoreCard'])){
                $model->attributes = $_POST['GasStoreCard'];
                if(!empty($model->ext_materials_id) || $model->checkbox_view_general){
                    $model->agent_id = MyFormat::getAgentId();
                    if(isset($_POST['GasStoreCard']['agent_id'])){
                        $model->agent_id = $_POST['GasStoreCard']['agent_id'];
                    }
                    if(empty($model->date_delivery)){
                        $date_view = date('Y-m-d');
                        $model->date_delivery = date('d-m-Y');
                    }else{
                        $date_view = MyFormat::dateDmyToYmdForAllIndexSearch($model->date_delivery);
                    }
                    $data = MyFunctionCustom::calcOpeningBalance($model->agent_id, $model->ext_materials_id, $date_view, array('model'=>$model));
                }
            }
            if($model->materials)
                $model->materials_name = $model->materials->name;                
            $this->render('View_store_card',array(
                    'model'=>$model, 'actions' => $this->listActionsCanAccess,
                'data'=>$data,
            ));
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }

    /**
     * @Author: ANH DUNG 02-05-2014
     * @Todo: Xem và in ấn nhập xuất tồn
     */        
    public function actionView_store_movement_summary() {
        MyFunctionCustom::checkChangePassword();
        set_time_limit(7200); ini_set('memory_limit','2000M');
        $this->handleExportExcel();
        $this->checkRoleLogin();
        $cRole = MyFormat::getCurrentRoleId();
        $this->pageTitle = 'Xem và in ấn nhập xuất tồn';
        try {
            $data=array();
            $model = new GasStoreCard();
            $model->date_from   = date('d-m-Y');
            $model->date_to     = date('d-m-Y');
            $model->agent_id    = 100;
            if(isset($_GET['version'])){
                $aType = CmsFormatter::$STORE_CARD_ALL_TYPE;
                unset($aType[STORE_CARD_TYPE_1]);
                unset($aType[STORE_CARD_TYPE_4]);
                $model->type_in_out  = array_keys($aType);
            }

            if(isset($_POST['GasStoreCard'])){
                $model->attributes  = $_POST['GasStoreCard'];
                $model->checkAccessAgent($model->agent_id);
                if($cRole == ROLE_SUB_USER_AGENT){
                    $model->agent_id    = MyFormat::getAgentId();
                }
                $model->date_from_ymd  = MyFormat::dateDmyToYmdForAllIndexSearch($model->date_from);
                $model->date_to_ymd    = MyFormat::dateDmyToYmdForAllIndexSearch($model->date_to);
                if(isset($_GET['excel'])){
                    $mToExcel = new ToExcel1();
                    $mToExcel->inventoryByProvince($model);
                }
                $data = Sta2::calcStoreMovement($model);
            }
            if(isset($_GET['excel'])){
                $model->date_from = date('01-m-Y');
                $model->province_id_agent = [1];
            }
            
            $this->render('View_store_movement_summary',array(
                    'model'=>$model, 'actions' => $this->listActionsCanAccess,
                'data'=>$data,
            ));
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }

    public function handleExportExcel() {
        if(isset($_GET['ExportExcel'])){
            ToExcel::InventorySummary();
        }
        if(isset($_GET['ToExcelAll'])){
            $mToExcel = new ToExcel1();
            $mToExcel->inventoryAllAgent();
        }
    }
    
    /**
     * @Author: ANH DUNG May 20, 2014
     * @Todo: cập nhật thu tiền hàng
     * @Param: $id pk model storecard
     */
    public function actionUpdateCollectionCustomer($id) {
        try
        {
           $this->layout = "ajax";
           $model=$this->loadModel($id);
           if(!GasStoreCard::canUpdateCollectionCustomer($model)){
               die('<script type="text/javascript">parent.$.fn.colorbox.close(); parent.$.fn.yiiGridView.update("gas-store-card-grid"); </script>'); 
           }
           if(isset($_POST['GasStoreCard']))
            {
                $attSave = array('collection_customer','collection_customer_note');
                $model->attributes=$_POST['GasStoreCard'];
                $model->collection_customer_note = InputHelper::removeScriptTag($model->collection_customer_note);
                if(is_null($model->collection_customer_date) || $model->collection_customer_date=='0000-00-00 00:00:00'){
                    $model->collection_customer_date = date('Y-m-d H:i:s');
                    $attSave[]='collection_customer_date';
                }else{
                    $model->collection_customer_date_update = date('Y-m-d H:i:s');
                    $attSave[]='collection_customer_date_update';
                }
                
                $model->update($attSave);
                GasStoreCardDetail::updateFirstRowCollectionCustomer($model);
                
                die('<script type="text/javascript">parent.$.fn.colorbox.close(); parent.$.fn.yiiGridView.update("gas-store-card-grid"); </script>'); 
            }
            $model->collection_customer = ActiveRecord::formatNumberInput($model->collection_customer);
           $this->render('UpdateCollectionCustomer',array(
                    'model'=>$model, 'actions' => $this->listActionsCanAccess,
            ));
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }
    
    /**
     * @Author: ANH DUNG Aug 04, 2014
     * @Todo: cập nhật ghi chú của thẻ kho, nv độc lập
     */
    public function actionUpdateNoteOnly($id) {
        try
        {
           $this->layout = "ajax";
           $model=$this->loadModel($id);
           if(isset($_POST['GasStoreCard']))
            {
                $attSave = array('update_note','update_note_by','update_note_time');
                $model->attributes=$_POST['GasStoreCard'];
                $model->update_note_by = Yii::app()->user->id;
                $model->update_note_time = date('Y-m-d H:i:s');
                $model->update_note = trim(InputHelper::removeScriptTag($model->update_note));
                $model->update($attSave);
                die('<script type="text/javascript">parent.$.fn.colorbox.close(); parent.$.fn.yiiGridView.update("gas-store-card-grid"); </script>'); 
            }
           $this->render('UpdateNoteOnly',array(
                    'model'=>$model, 'actions' => $this->listActionsCanAccess,
            ));
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }                    
    }
    
    public function toExcelList() {
//        error_reporting(1);
        if(isset($_GET['ExportExcel']) && isset($_SESSION['data-excel'])){
            ini_set('memory_limit','1500M');
//            ToExcelList::listStorecard();
            $mExportList = new ToExcelList();
            $mExportList->listStorecard($_SESSION['data-excel']->data, new GasStoreCard());
        }
    }
    public function checkRoleLogin() {
        $cRole = MyFormat::getCurrentRoleId();
        $cUid = MyFormat::getCurrentUid();
        if($cRole == ROLE_E_MAINTAIN){
            if($cUid != 908108){
                $this->redirect(Yii::app()->createAbsoluteUrl('/'));
            }
        }
    }
   
}
