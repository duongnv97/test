<?php

class CodePartnerController extends AdminController 
{
    public $pluralTitle = 'Code partner';
    public $singleTitle = 'Code partner';
    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id)
    {
        $this->pageTitle = 'Xem ';
        try{
        $this->render('view',array(
                'model'=>$this->loadModel($id), 'actions' => $this->listActionsCanAccess,
        ));
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id)
    {
        try{
        if(Yii::app()->request->isPostRequest){
            // we only allow deletion via POST request
            if($model = $this->loadModel($id))
            {
                if($model->delete())
                    Yii::log("Uid: " .Yii::app()->user->id. " Delete record ".  print_r($model->attributes, true), 'info');
            }

            // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
            if(!isset($_GET['ajax']))
                    $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('index'));
        }
        else
        {
            Yii::log("Uid: " .Yii::app()->user->id. " Invalid request. Please do not repeat this request again.");
            throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
        }	
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }

    /**
     * Manages all models.
     */
    public function actionIndex()
    {
        $this->pageTitle = 'Code partner';
        $this->indexExcel();
        try{
        $model=new CodePartner('search');
        $model->unsetAttributes();  // clear any default values
        if(isset($_GET['CodePartner']))
                $model->attributes=$_GET['CodePartner'];

        $this->render('index',array(
                'model'=>$model, 'actions' => $this->listActionsCanAccess,
        ));
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
    * If the data model is not found, an HTTP exception will be raised.
     * @param integer the ID of the model to be loaded
     */
    public function loadModel($id)
    {
        try{
        $model=CodePartner::model()->findByPk($id);
        if($model===null)
        {
            Yii::log("The requested page does not exist.");
            throw new CHttpException(404,'The requested page does not exist.');
        }			
        return $model;
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }

    /** @Author: KHANH TOAN Nov 23, 2018
     *  @Todo: số bình mới mà nhân viên tổng đài tư vẫn đượcs trong tháng từ hóa đơn gần nhất
     *  @Param:
     **/
    public function actionReport(){
        $this->pageTitle = 'Báo cáo Code Partner';
        $this->reportCodePartner();
        try {
            $model = new CodePartner();
            $model->unsetAttributes();  // clear any default values
            $aData = array();
            $model->date_from   = date("01-m-Y");
            $model->date_to     = date("d-m-Y");
            if(isset($_GET['CodePartner'])){
                $model->attributes = $_GET['CodePartner'];
            }
            $aData = $model->getReport();   
            $_SESSION['dataReportCodePartner'] = $aData;
            $_SESSION['data-model']         = $model;
            $this->render('report/index',array(
                'aData'=>$aData,
                'model' => $model,
                'actions' => $this->listActionsCanAccess,
            ));
        } catch (Exception $exc) {
            GasCheck::CatchAllExeptiong($exc);
        }
    }
    
    /**
     * @Author: KHANH TOAN Nov 11, 2018
     * @Todo: ExportExcel
     */
    public function reportCodePartner(){
        try{
            if(isset($_GET['to_excel']) && isset($_SESSION['dataReportCodePartner'])){
                ini_set('memory_limit','1500M');// chưa cần chạy đoạn này, đưa memory_limit lên 500 có thể export dc khoảng 5000 row - chưa test
                $mToExcel1 = new ToExcel1();
                $mToExcel1->exportReportCodePartner();
            }
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }
    
    /** @Author: KHANH TOAN Nov 26 2018
     *  @Todo: xuat excel index
     *  @Param:
     **/
    public function indexExcel() {
        try{
            if(isset($_GET['to_excel']) && isset($_SESSION['data-excel'])){
                ini_set('memory_limit','1500M');// chưa cần chạy đoạn này, đưa memory_limit lên 500 có thể export dc khoảng 5000 row - chưa test
                $mToExcel1 = new ToExcel1();
                $mToExcel1->CodePartner();
            }
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }

    
}
