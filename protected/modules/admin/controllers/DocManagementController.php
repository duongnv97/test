<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of DocManagementController
 *
 * @author HaoNH
 */
class DocManagementController extends AdminController {

    public $pluralTitle = "QL Mẫu Văn Bản";
    public $singleTitle = "QL Mẫu Văn Bản";

    public function actionIndex() {
        $this->pageTitle = 'QL Mẫu Văn Bản';

        try {
            $model = new DocManagement();

            $model->unsetAttributes();

            if (isset($_GET['DocManagement'])) {
                $model->attributes = $_GET['DocManagement'];
            }


            $this->render('index', array(
                'model' => $model, 'actions' => $this->listActionsCanAccess,
            ));
        } catch (Exception $exc) {
            GasCheck::CatchAllExeptiong($exc);
        }
    }

    public function actionView($id) {
        try {

            $this->render('view', array(
                'model' => $this->loadModel($id), 'actions' => $this->listActionsCanAccess,
            ));
        } catch (Exception $e) {
            Yii::log("Uid: " . Yii::app()->user->id . "Exception " . $e->getMessage(), 'error');
            $code = 404;
            if (isset($e->statusCode))
                $code = $e->statusCode;
            if ($e->getCode())
                $code = $e->getCode();
            throw new CHttpException($code, $e->getMessage());
        }
    }

    public function actionCreate() {

        try {

            $model = new DocManagement();
            $model->scenario = "doc_create";
            $mGasFile = new GasFile();
            $model->mGasFile = $mGasFile;
            if (isset($_POST['DocManagement'])) {
                $aDocManagement = $_POST['DocManagement'];
                $model->attributes = $aDocManagement;
                $model->validate();
                $mGasFile->ValidateFileDoc($model, $aDocManagement['file_name']);
                if (!$model->hasErrors()) {

                    $model->save();
                    $model->mGasFile->saveRecordFile($model, GasFile::TYPE_21_DOCUMENT);

                    Yii::app()->user->setFlash(MyFormat::SUCCESS_UPDATE, "Thêm mới thành công.");
                    $this->redirect(array('view', 'id' => $model->id));
                }
            }

            $this->render('create', array(
                'model' => $model, 'actions' => $this->listActionsCanAccess,
            ));
        } catch (Exception $exc) {
            GasCheck::CatchAllExeptiong($exc);
        }
    }

    public function actionUpdate($id) {

        try {
            $model = $this->loadModel($id);

            $mGasFile = new GasFile();
            $model->mGasFile = $mGasFile;
            if (isset($_POST['DocManagement'])) {
                $aDocManagement = $_POST['DocManagement'];
                $model->attributes = $aDocManagement;
                $model->validate();
                $mGasFile->ValidateFileDoc($model, $aDocManagement['file_name']);
                if (!$model->hasErrors()) {
                    $model->deleteFile();
                    $model->update();
                    $model->mGasFile->saveRecordFile($model, GasFile::TYPE_21_DOCUMENT);
                    Yii::app()->user->setFlash(MyFormat::SUCCESS_UPDATE, "Thêm mới thành công.");

                    $this->redirect(array('view', 'id' => $model->id));
                }
            }

            $this->render('update', array(
                'model' => $model, 'actions' => $this->listActionsCanAccess,
            ));
        } catch (Exception $exc) {
            GasCheck::CatchAllExeptiong($exc);
        }
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id) {
        try {
            if (Yii::app()->request->isPostRequest) {
                // we only allow deletion via POST request 
                if ($model = $this->loadModel($id)) {
                    if ($model->delete()) {
                        $model->removeAllFile();
                        Yii::log("Uid: " . Yii::app()->user->id . " Delete record " . print_r($model->attributes, true), 'info');
                    }
                }
                // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
                if (!isset($_GET['ajax']))
                    $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('index'));
            }
            else {
                Yii::log("Uid: " . Yii::app()->user->id . " Invalid request. Please do not repeat this request again.");
                throw new CHttpException(400, 'Invalid request. Please do not repeat this request again.');
            }
        } catch (Exception $exc) {
            GasCheck::CatchAllExeptiong($exc);
        }
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer the ID of the model to be loaded
     */
    public function loadModel($id) {
        try {
            $model = DocManagement::model()->findByPk($id);

            if ($model === null) {
                Yii::log("The requested page does not exist.");
                throw new CHttpException(404, 'The requested page does not exist.');
            }
            return $model;
        } catch (Exception $exc) {
            GasCheck::CatchAllExeptiong($exc);
        }
    }

}
