<?php

class GasquotationController extends AdminController
{

    public function actionImport_gasquotation() {        
        set_time_limit(7200);        
        
        $model=new GasQuotation('import_gasquotation');
        if(isset($_POST['GasQuotation']))
        {
                $model->attributes=$_POST['GasQuotation'];
                $model->file_excel=$_FILES['GasQuotation'];
				//var_dump($_FILES['GasQuotation']);die;
                $model->validate();
                if(!$model->hasErrors()){
                    $this->importExcel($model);
                    $this->redirect(array('index','id'=>$model->id));
                }
                   
        }        
        $this->render('import_gasquotation',array(
                'model'=>$model, 'actions' => $this->listActionsCanAccess,
        ));        
    }

		public function importExcel($model){
                Yii::import('application.extensions.vendors.PHPExcel',true);
                $objReader = PHPExcel_IOFactory::createReader("Excel2007");
                
                $objPHPExcel = $objReader->load(@$_FILES['GasQuotation']['tmp_name']['file_excel']);
                $objWorksheet = $objPHPExcel->getActiveSheet();
                $highestRow = $objWorksheet->getHighestRow(); // e.g. 10
                //$highestColumn = $objWorksheet->getHighestColumn(); // e.g 'F'
                $highestColumn = 'F';
                $highestColumnIndex = PHPExcel_Cell::columnIndexFromString($highestColumn);
	
                $success = 1;
                $aRowInsert=array();
                $aCellError = array();
                $created_date=date('Y-m-d H:i:s');
                $uid = Yii::app()->user->id;
                $aCustomer=  Users::getArrCustomerForImport();
				// $aSale =  Users::getArrSaleIdForImport();	
//                $aCustomerPaymentDay=  Users::getArrPaymentDayOfCustomer();
                for ($row = 2; $row <= $highestRow; ++$row)
                {                    
                    // file excel cần format column theo dạng text hết để người dùng nhập vào khi đọc không lỗi

                    
                    $temp_customer_id = trim(trim($objWorksheet->getCellByColumnAndRow(0, $row)->getValue()));
                    if(isset($aCustomer[$temp_customer_id]))
                        $customer_id = $aCustomer[$temp_customer_id];
                    else{
                        $aCellError[] = ' Dòng '.$row.' Khong co Khach Hang: '.$temp_customer_id;
                        continue;
                    }  
					
                    $price_45 = trim($objWorksheet->getCellByColumnAndRow(1, $row)->getValue());
                    $price_50 = trim($objWorksheet->getCellByColumnAndRow(2, $row)->getValue());
                    $price_12 = trim($objWorksheet->getCellByColumnAndRow(3, $row)->getValue());
					$price_month = trim($objWorksheet->getCellByColumnAndRow(4, $row)->getValue());
					if($price_month<10){
						$price_month = '0'.$price_month*1;
					}
					$price_year = trim($objWorksheet->getCellByColumnAndRow(5, $row)->getValue());
					                    
                    $aRowInsert[]="(
                        '$customer_id',
                        '$price_45',
                        '$price_50',
						'$price_12',	
                        '$price_month',
                        '$price_year'    
                        )";
                }
				
            if(count($aCellError)>0){
                $mLog = new GasImportLog();
                $mLog->user_id = $uid;
                $mLog->created_date = $created_date;
                $mLog->json = json_encode($aCellError);
                $mLog->save();
                echo 'Nếu gặp lỗi này hãy giữ nguyên (và chụp ) màn hình và thông báo lại cho Anh Dũng';
                echo '<pre>';
                echo print_r($aCellError);
                echo '</pre >';
                die;
            }				
            $tableName = GasQuotation::model()->tableName();
            $sql = "insert into $tableName (
                            customer_id,
                            price_45,
                            price_50,
							price_12,
                            price_month,
                            price_year
                            ) values ".implode(',', $aRowInsert);
            
			
            Yii::app()->db->createCommand($sql)->execute();

            return $aCellError;
        }    
		
		
	public function actionView($id)
	{
                try{
                $this->render('view',array(
			'model'=>$this->loadModel($id), 'actions' => $this->listActionsCanAccess,
		));
                }
                catch (Exception $e)
                {
                    Yii::log("Uid: " .Yii::app()->user->id. "Exception ".  $e->getMessage(), 'error');
                    $code = 404;
                    if(isset($e->statusCode))
                        $code=$e->statusCode;
                    if($e->getCode())
                        $code=$e->getCode();
                    throw new CHttpException($code, $e->getMessage());
                }
		
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
                try
                {
		$model=new GasQuotation('create_quotation');

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);
                $model->price_year =  date('Y');
                $model->price_month =  date('m');                
                
		if(isset($_POST['GasQuotation']))
		{
			$model->attributes=$_POST['GasQuotation'];
                        $model->user_id_create = Yii::app()->user->id;
                        $model->created_date = date('Y-m-d H:i:s');
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('create',array(
			'model'=>$model, 'actions' => $this->listActionsCanAccess,
		));
                }
                catch (Exception $e)
                {
                    Yii::log("Uid: " .Yii::app()->user->id. "Exception ".  $e->getMessage(), 'error');
                    $code = 404;
                    if(isset($e->statusCode))
                        $code=$e->statusCode;
                    if($e->getCode())
                        $code=$e->getCode();
                    throw new CHttpException($code, $e->getMessage());
                }
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
                try
                {
		$model=$this->loadModel($id);
                $model->scenario='update_quotation';
		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['GasQuotation']))
		{
			$model->attributes=$_POST['GasQuotation'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

                $model->autocomplete_name = $model->customer?$model->customer->first_name:'';
		$this->render('update',array(
			'model'=>$model, 'actions' => $this->listActionsCanAccess,
		));
                }
                catch (Exception $e)
                {
                    Yii::log("Uid: " .Yii::app()->user->id. "Exception ".  $e->getMessage(), 'error');
                    $code = 404;
                    if(isset($e->statusCode))
                        $code=$e->statusCode;
                    if($e->getCode())
                        $code=$e->getCode();
                    throw new CHttpException($code, $e->getMessage());
                }
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
                try
                {
		if(Yii::app()->request->isPostRequest)
		{
			// we only allow deletion via POST request
			if($model = $this->loadModel($id))
                        {
                            if($model->delete())
                                Yii::log("Delete record ".  print_r($model->attributes, true), 'info');
                        }

			// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
			if(!isset($_GET['ajax']))
				$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('index'));
		}
		else
                {
                    Yii::log("Invalid request. Please do not repeat this request again.");
                    throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
                }	
                }
                catch (Exception $e)
                {
                    Yii::log("Uid: " .Yii::app()->user->id. "Exception ".  $e->getMessage(), 'error');
                    throw new CHttpException(400,$e->getMessage());
                }
	}

	/**
	 * Manages all models.
	 */
	public function actionIndex()
	{
                try
                {
		$model=new GasQuotation('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['GasQuotation']))
			$model->attributes=$_GET['GasQuotation'];

		$this->render('index',array(
			'model'=>$model, 'actions' => $this->listActionsCanAccess,
		));
                }
                catch (Exception $e)
                {
                    Yii::log("Uid: " .Yii::app()->user->id. "Exception ".  $e->getMessage(), 'error');
                    $code = 404;
                    if(isset($e->statusCode))
                        $code=$e->statusCode;
                    if($e->getCode())
                        $code=$e->getCode();
                    throw new CHttpException($code, $e->getMessage());
                }
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the ID of the model to be loaded
	 */
	public function loadModel($id)
	{
                try
                {
		$model=GasQuotation::model()->findByPk($id);
		if($model===null)
                {
                    Yii::log("The requested page does not exist.");
                    throw new CHttpException(404,'The requested page does not exist.');
                }			
		return $model;
                }
                catch (Exception $e)
                {
                    Yii::log("Uid: " .Yii::app()->user->id. "Exception ".  $e->getMessage(), 'error');
                    $code = 404;
                    if(isset($e->statusCode))
                        $code=$e->statusCode;
                    if($e->getCode())
                        $code=$e->getCode();
                    throw new CHttpException($code, $e->getMessage());
                }
	}

	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
                try
                {
		if(isset($_POST['ajax']) && $_POST['ajax']==='gas-quotation-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
                }
                catch (Exception $e)
                {
                    Yii::log("Uid: " .Yii::app()->user->id. "Exception ".  $e->getMessage(), 'error');
                    $code = 404;
                    if(isset($e->statusCode))
                        $code=$e->statusCode;
                    if($e->getCode())
                        $code=$e->getCode();
                    throw new CHttpException($code, $e->getMessage());
                }
	}
}
