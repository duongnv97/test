<?php

class UsersInfoController extends AdminController 
{
    public $pluralTitle = 'Bảo Trì';
    public $singleTitle = 'Bảo Trì';
    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id)
    {
        $this->pageTitle = 'Xem ';
        try{
        $this->render('view',array(
                'model'=>$this->loadModel($id), 'actions' => $this->listActionsCanAccess,
        ));
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }

    }

    /**
     * Manages all models.
     */
    public function actionIndex()
    {
        $this->pageTitle = 'Khách Hàng';
//        error_reporting(1); // var_dump(PHP_INT_SIZE);
//        ini_set('memory_limit','1500M');
//        $mUsersInfo = new UsersInfo();
//        $mUsersInfo->getDataFromUsers();
//        die;
        try{
        $model=new UsersInfo('search');
        $model->unsetAttributes();  // clear any default values
        $model->is_maintain = [STORE_CARD_HGD_CCS,STORE_CARD_HGD,STORE_CARD_VIP_HGD,UsersExtend::STORE_CARD_HGD_APP];
        if(isset($_GET['UsersInfo']))
                $model->attributes=$_GET['UsersInfo'];

        $this->render('index',array(
                'model'=>$model, 'actions' => $this->listActionsCanAccess,
        ));
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
    * If the data model is not found, an HTTP exception will be raised.
     * @param integer the ID of the model to be loaded
     */
    public function loadModel($id)
    {
        try{
        $model=UsersInfo::model()->findByPk($id);
        if($model===null)
        {
            Yii::log("The requested page does not exist.");
            throw new CHttpException(404,'The requested page does not exist.');
        }			
        return $model;
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }

}
