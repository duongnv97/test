<?php

class GasLeaveHolidaysController extends AdminController {

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id) {
        $this->pageTitle = 'Xem ';
//        $this->layout = 'ajax';
        try {
            $this->render('view', array(
                'model' => $this->loadModel($id), 'actions' => $this->listActionsCanAccess,
            ));
        } catch (Exception $exc) {
            Yii::log("Uid: " . Yii::app()->user->id . " Exception " . $exc->getMessage(), 'error');
            $code = 404;
            if (isset($exc->statusCode))
                $code = $exc->statusCode;
            if ($exc->getCode())
                $code = $exc->getCode();
            throw new CHttpException($code, $exc->getMessage());
        }
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate($date = null) {
        $this->pageTitle = 'Tạo Mới ';
        if($date != null)
            $this->layout = 'ajax';
        try {
            $model = new GasLeaveHolidays('create');
            if ($date != null) {
                $model->date = MyFormat::dateConverYmdToDmy($date);
            }

            if (isset($_POST['GasLeaveHolidays'])) {
                $model->attributes = $_POST['GasLeaveHolidays'];
                $model->date = MyFormat::dateConverDmyToYmd($model->date);
                if(!empty($_POST['GasLeaveHolidays']['compensatory_leave_date'])){
                    $model->compensatory_leave_date = MyFormat::dateConverDmyToYmd($model->compensatory_leave_date);
                } else {
                    $model->compensatory_leave_date = NULL;
                }
                $model->description = GasLeaveHolidays::slugify($model->name);
                $model->validate();
                if (!$model->hasErrors()) {
                    $model->save();
                    if($date == null){
                        Yii::app()->user->setFlash('successUpdate', "Thêm mới thành công.");
                        $this->redirect(array('create'));
                    }
                    die('<script type="text/javascript">parent.$.fn.colorbox.close();</script>');
                }
            }

            $this->render('create', array(
                'model' => $model, 'actions' => $this->listActionsCanAccess,
            ));
        } catch (Exception $exc) {
            Yii::log("Uid: " . Yii::app()->user->id . " Exception " . $exc->getMessage(), 'error');
            $code = 404;
            if (isset($exc->statusCode))
                $code = $exc->statusCode;
            if ($exc->getCode())
                $code = $exc->getCode();
            throw new CHttpException($code, $exc->getMessage());
        }
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id) {
        $this->pageTitle = 'Cập Nhật ';
        if(isset($_GET['ajax']))
            $this->layout = 'ajax';
        try {
            $model = $this->loadModel($id);
            $model->scenario = 'update';
            $model->date = MyFormat::dateConverYmdToDmy($model->date);
            if(!empty($model->compensatory_leave_date)){
                $model->compensatory_leave_date = MyFormat::dateConverYmdToDmy($model->compensatory_leave_date);
            }
            if (isset($_POST['GasLeaveHolidays'])) {
                $model->attributes = $_POST['GasLeaveHolidays'];
                $model->date = MyFormat::dateConverDmyToYmd($model->date);
                if(!empty($_POST['GasLeaveHolidays']['compensatory_leave_date'])){
                    $model->compensatory_leave_date = MyFormat::dateConverDmyToYmd($model->compensatory_leave_date);
                } else {
                    $model->compensatory_leave_date = NULL;
                }
                if (!$model->hasErrors()) {
                    $model->update();
                    Yii::app()->user->setFlash('successUpdate', "Cập nhật thành công.");
                    //$this->redirect(array('update', 'id' => $model->id));
                }
            }

            $this->render('update', array(
                'model' => $model, 'actions' => $this->listActionsCanAccess,
            ));
        } catch (Exception $exc) {
            Yii::log("Uid: " . Yii::app()->user->id . " Exception " . $exc->getMessage(), 'error');
            $code = 404;
            if (isset($exc->statusCode))
                $code = $exc->statusCode;
            if ($exc->getCode())
                $code = $exc->getCode();
            throw new CHttpException($code, $exc->getMessage());
        }
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id) {
        try {
            if (Yii::app()->request->isPostRequest) {
                // we only allow deletion via POST request
                if ($model = $this->loadModel($id)) {
                    if ($model->delete())
                        Yii::log("Uid: " . Yii::app()->user->id . " Delete record " . print_r($model->attributes, true), 'info');
                }

                // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
                if (!isset($_GET['ajax']))
                    $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('index'));
            }
            else {
                Yii::log("Uid: " . Yii::app()->user->id . " Invalid request. Please do not repeat this request again.");
                throw new CHttpException(400, 'Invalid request. Please do not repeat this request again.');
            }
        } catch (Exception $exc) {
            Yii::log("Uid: " . Yii::app()->user->id . " Exception " . $exc->getMessage(), 'error');
            $code = 404;
            if (isset($exc->statusCode))
                $code = $exc->statusCode;
            if ($exc->getCode())
                $code = $exc->getCode();
            throw new CHttpException($code, $exc->getMessage());
        }
    }

    /**
     * Manages all models.
     */
    public function actionIndex($year = null) {

        $this->pageTitle = 'Danh sách ngày nghỉ lễ ' . $year;
        try {
            $model = new GasLeaveHolidays('search');
            $model->unsetAttributes();  // clear any default values
            if (!empty($year)) {
                $model->year = $year;
            } else {
                $model->year = isset($_GET['GasLeaveHolidays']['year']) ? $_GET['GasLeaveHolidays']['year'] : date('Y');
            }
            if (isset($_GET['GasLeaveHolidays']))
                $model->attributes = $_GET['GasLeaveHolidays'];

            $this->render('index_button', array(
                'model' => $model,
                'modelPlan' => $model->getLeaveHolidaysPlan(),
                'actions' => $this->listActionsCanAccess,
            ));
        } catch (Exception $exc) {
            Yii::log("Uid: " . Yii::app()->user->id . " Exception " . $exc->getMessage(), 'error');
            $code = 404;
            if (isset($exc->statusCode))
                $code = $exc->statusCode;
            if ($exc->getCode())
                $code = $exc->getCode();
            throw new CHttpException($code, $exc->getMessage());
        }
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer the ID of the model to be loaded
     */
    public function loadModel($id) {
        try {
            $model = GasLeaveHolidays::model()->findByPk($id);
            if ($model === null) {
                Yii::log("The requested page does not exist.");
                throw new CHttpException(404, 'The requested page does not exist.');
            }
            return $model;
        } catch (Exception $exc) {
            Yii::log("Uid: " . Yii::app()->user->id . " Exception " . $exc->getMessage(), 'error');
            $code = 404;
            if (isset($exc->statusCode))
                $code = $exc->statusCode;
            if ($exc->getCode())
                $code = $exc->getCode();
            throw new CHttpException($code, $exc->getMessage());
        }
    }

    /**
     * Performs the AJAX validation.
     * @param CModel the model to be validated
     */
    protected function performAjaxValidation($model) {
        try {
            if (isset($_POST['ajax']) && $_POST['ajax'] === 'gas-leave-holidays-form') {
                echo CActiveForm::validate($model);
                Yii::app()->end();
            }
        } catch (Exception $exc) {
            Yii::log("Uid: " . Yii::app()->user->id . " Exception " . $exc->getMessage(), 'error');
            $code = 404;
            if (isset($exc->statusCode))
                $code = $exc->statusCode;
            if ($exc->getCode())
                $code = $exc->getCode();
            throw new CHttpException($code, $exc->getMessage());
        }
    }

    /** @Author: HOANG NAM 02/04/2018
     *  @Todo: update plan
     *  @Param: 
     * */
    public function actionUpdatePlan($id) {
        try {
            $mLeaveHolidaysPlan = HrHolidayPlans::model()->findByPk($id);
            $mLeaveHolidaysPlan->scenario = 'update';
            if (isset($_POST['HrHolidayPlans'])) {
                $mLeaveHolidaysPlan->attributes = $_POST['HrHolidayPlans'];
                $mLeaveHolidaysPlan->status = $mLeaveHolidaysPlan->STATUS_NEW;
                $mLeaveHolidaysPlan->is_sendmail = $_POST['HrHolidayPlans']['is_sendmail'];
                $mLeaveHolidaysPlan->setAttributesBeforeSave();
                $mLeaveHolidaysPlan->validate();

                if (!$mLeaveHolidaysPlan->hasErrors()) {
                    $mLeaveHolidaysPlan->update();
                    if($mLeaveHolidaysPlan->is_sendmail){
                        GasScheduleEmail::notifyGasLeaveHolidays($mLeaveHolidaysPlan);
                    }
                    Yii::app()->user->setFlash(MyFormat::SUCCESS_UPDATE, "Cập nhật thành công.");
                }
            }
            $model = new GasLeaveHolidays('search');
            $model->year = $id;
            $this->render('index', array(
                'model' => $model,
                'modelPlan' => $mLeaveHolidaysPlan,
                'actions' => $this->listActionsCanAccess,
            ));
        } catch (Exception $exc) {
            GasCheck::CatchAllExeptiong($exc);
        }
    }

    /** @Author: HOANG NAM 02/04/2018
     *  @Todo: create plan
     *  @Param: 
     * */
    public function actionCreatePlan($id) {
        $model = new GasLeaveHolidays('search');
        $model->year = $id;
        try {
            $mLeaveHolidaysPlan = new HrHolidayPlans('create');
            $mLeaveHolidaysPlan->status = $mLeaveHolidaysPlan->STATUS_NEW;
            if (isset($_POST['HrHolidayPlans'])) {
                $mLeaveHolidaysPlan->attributes = $_POST['HrHolidayPlans'];
                $mLeaveHolidaysPlan->setAttributesBeforeSave();
                $mLeaveHolidaysPlan->validate();
                if (!$mLeaveHolidaysPlan->hasErrors()) {
                    $mLeaveHolidaysPlan->save();
                    GasScheduleEmail::notifyGasLeaveHolidays($mLeaveHolidaysPlan);
                    Yii::app()->user->setFlash(MyFormat::SUCCESS_UPDATE, "Thêm mới thành công.");
                    $this->render('index', array(
                        'model' => $model,
                        'modelPlan' => $mLeaveHolidaysPlan,
                        'actions' => $this->listActionsCanAccess,
                    ));
                }
            }

            $this->render('index', array(
                'model' => $model,
                'modelPlan' => $mLeaveHolidaysPlan,
                'actions' => $this->listActionsCanAccess,
            ));
        } catch (Exception $exc) {
            GasCheck::CatchAllExeptiong($exc);
        }
    }

    public function actionGenerate($year) {
        $idType = 1;
        $model = new GasLeaveHolidays('search');
        $model->year = $year;
        $mLeaveHolidaysPlan = $model->getLeaveHolidaysPlan();
        try {
            $model->generateLeaveHolidays($model->getAllDay($year, array('Sun')), $idType);
            $this->render('index', array(
                'model' => $model,
                'modelPlan' => $mLeaveHolidaysPlan,
                'actions' => $this->listActionsCanAccess,
            ));
        } catch (Exception $exc) {
            GasCheck::CatchAllExeptiong($exc);
        }
    }
    
    /** @Author: HOANG NAM 04/04/2018
     *  @Todo: update status
     *  @Param: 
     **/
    public function actionUpdateStatusPlan($id){
        try {
            $model = new GasLeaveHolidays('search');
            $model->year = $id;
            $mLeaveHolidaysPlan = HrHolidayPlans::model()->findByPk($id);
            $mLeaveHolidaysPlan->scenario = 'Update';
            if (isset($_POST['HrHolidayPlans'])) {
                $mLeaveHolidaysPlan->attributes = $_POST['HrHolidayPlans'];
                $mLeaveHolidaysPlan->validate();
                if (!$mLeaveHolidaysPlan->hasErrors()) {
                    $mLeaveHolidaysPlan->update();
                    Yii::app()->user->setFlash(MyFormat::SUCCESS_UPDATE, "Cập nhật thành công.");
                }
            }
            $this->render('index', array(
                'model' => $model,
                'modelPlan' => $mLeaveHolidaysPlan,
                'actions' => $this->listActionsCanAccess,
            ));
        } catch (Exception $exc) {
            GasCheck::CatchAllExeptiong($exc);
        }
    }
    /** @Author: HOANG NAM 16/04/2018
     *  @Todo: pdf
     *  @Param: 
     *  const OUTPUT_TO_BROWSER = "I";
     *  const OUTPUT_TO_DOWNLOAD = "D";
     *  const OUTPUT_TO_FILE = "F";
     *  const OUTPUT_TO_STRING = "S";
     **/
    public function actionToPdf($id) {
        try {
            $model = new GasLeaveHolidays();
            $model->pdfOutput = 'I';//Xuất trên trình duyệt
            $model->toLeaveHolidaysPdf($id);
//            Đường dẫn file lưu nếu là OUTPUT_TO_FILE = "F";
//            $model->pdfPathAndName;
        } catch (Exception $exc) {
            GasCheck::CatchAllExeptiong($exc);
        }
    }
    
    //DuongNV comment Dec 20,2018 reason: menu ko hiện
//    all user
//    public function accessRules()
//    {
//        return array(
//            array('allow',  // allow all users to perform  actions
//                'actions'=>array(
//                     'ToPdf'
//                    ),
//                'users'=>array('*'),
//            ),      
//                      
//        );
//    }
    
    public function actionUpdateAjax($year = null)
    {
        $data = [];
        $model = new GasLeaveHolidays('search');
        $model->unsetAttributes();  // clear any default values

        if (!empty($year)) {
            $model->year = $year;
        } else {
            $model->year = isset($_GET['GasLeaveHolidays']['year']) ? $_GET['GasLeaveHolidays']['year'] : date('Y');
        }
        if (isset($_GET['GasLeaveHolidays']))
            $model->attributes = $_GET['GasLeaveHolidays'];

        if(isset($_GET['HrParameters'])) {
            $model->attributes = $_GET['HrParameters'];
        }

        $data['model'] = $model;
//            $data['modelPlan'] = $model->getLeaveHolidaysPlan();
        $this->renderPartial('_calendarHtml', $data, false, true);
    }
    
    /** @Author: DUONG 02/06/2018
     *  @Todo:   $todo
     *  @Code:   DUONG001
     *  @Param: create multi date holidays
     **/
   public function actionCreateMultiHolidays() {
        $this->pageTitle = 'Tạo Mới ';
        $this->layout = 'ajax';
        try {
            // Uncomment the following line if AJAX validation is needed
            // $this->performAjaxValidation($model);
            $model = new GasLeaveHolidays('createMultiHolidays');
            if (isset($_POST['GasLeaveHolidays'])) {
                
                $model->date_create_from = MyFormat::dateConverDmyToYmd($_POST['GasLeaveHolidays']['date_create_from']);
                $model->date_create_to = MyFormat::dateConverDmyToYmd($_POST['GasLeaveHolidays']['date_create_to']);
                
                $date_from = strtotime($model->date_create_from);
                $date_to = strtotime($model->date_create_to);
                $count_date = 0;
                for ($i=$date_from; $i<=$date_to; $i+=86400) {  
                    $model2save = new GasLeaveHolidays('createMultiHolidays');
                    $model2save->date = date("Y-m-d", $i);
                    $model2save->attributes = $_POST['GasLeaveHolidays'];
                    $model2save->description = GasLeaveHolidays::slugify($model2save->name);
                    $model->validate();
                    if (!$model2save->hasErrors()) {
                        if($model2save->save())
                            $count_date++;
                    }
                }
                if($count_date > 0){
                    Yii::app()->user->setFlash('successUpdate', "Thêm mới thành công {$count_date} ngày lễ!");
                    $this->redirect(array('createMultiHolidays'));
                }
            }

            $this->render('create', array(
                'model' => $model, 'actions' => $this->listActionsCanAccess,
            ));
        } catch (Exception $exc) {
            Yii::log("Uid: " . Yii::app()->user->id . " Exception " . $exc->getMessage(), 'error');
            $code = 404;
            if (isset($exc->statusCode))
                $code = $exc->statusCode;
            if ($exc->getCode())
                $code = $exc->getCode();
            throw new CHttpException($code, $exc->getMessage());
        }
   }
}
