<?php

class TicketsController extends AdminController 
{
    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id)
    {
        $this->pageTitle = 'Xem ';
        $gastickets = GasTicketsDetail::model()->findByPk($id);
        $id = $gastickets->ticket_id;
        try{
            $this->render('list/view',array(
                    'model'=>$this->loadModel($id), 'actions' => $this->listActionsCanAccess,
            ));
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate()
    {
       die;
    }

    public function actionUpdate($id)
    {
        die;
    }

    /**
     * @Author: ANH DUNG Oct 14, 2014
     * @Todo: delete comment of ticket, only for admin
     * @Param: $id -- khong lam nua
     */
    public static function actionDelete_comment() {}
    
    public function actionDelete($id)
    {
        try{
        if(Yii::app()->request->isPostRequest)
        {
            // we only allow deletion via POST request
            if($model = $this->loadModel($id))
            {
                if($model->delete())
                    Yii::log("Uid: " .Yii::app()->user->id. " Delete record ".  print_r($model->attributes, true), 'info');
            }

            $msg = "Delete Ticket thành công.";
            $FlashScenario = 'successUpdate';           
            Yii::app()->user->setFlash($FlashScenario, $msg);
            $this->redirect(array('index'));
        }
        else
        {
            Yii::log("Uid: " .Yii::app()->user->id. " Invalid request. Please do not repeat this request again.");
            throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
        }	
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }

    /** Manages all models */
    public function actionIndex()
    {
        $this->pageTitle = 'Hỗ Trợ Sử Dụng Phần Mềm ';
        try{
        $this->viewTicket();
        $this->viewList();
        $ModelCreate=new GasTickets('create');
        $this->ProcessCreate($ModelCreate);

        $model=new GasTickets('search');
        $model->unsetAttributes();  // clear any default values
        if(isset($_GET['GasTickets']))
            $model->attributes=$_GET['GasTickets'];

        $this->render('index',array(
            'model'=>$model, 
            'ModelCreate'=>$ModelCreate, 
            'actions' => $this->listActionsCanAccess,
        ));
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }
    
    public function ProcessCreate($ModelCreate){
        if(isset($_POST['GasTickets'])){
            $ModelCreate->attributes=$_POST['GasTickets'];
            if(MyFormat::getCurrentRoleId() == ROLE_SUB_USER_AGENT ){
                $model->agent_id = MyFormat::getAgentId();
            }
            $ModelCreate->validate();
            if(!$ModelCreate->hasErrors()){               
               $msg = "Tạo mới ticket thành công.";
               $FlashScenario = 'successUpdate';
               if(!GasTickets::UserCanPostTicket()) {
                   $msg = "Có Lỗi không thể tạo ticket(exceed limit), liên hệ với admin quản trị.";
                   $FlashScenario = 'ErrorUpdate';
               }else{
                   $ModelCreate->uid_login = MyFormat::getCurrentUid();
                   $ModelCreate->SaveNewTicket();
               }
               Yii::app()->user->setFlash($FlashScenario, $msg);
//               Yii::app()->user->setFlash('successUpdate', $msg);
               $this->redirect(array('index'));
            }
        }
    }

    /**
     * @Author: ANH DUNG Aug 11, 2014
     * @Todo: kiểm tr có thể reply ko
     * @Param: $model model 
     */
    public function CanReply($model){
        return true;// chỗ này không cần check nữa vì user ko thể vào đây được, chỗ pick ticket đã xử lý cái này rồi
        if($model->process_status == GasTickets::PROCESS_STATUS_PICK){
            $uidLogin = Yii::app()->user->id;
            if($uidLogin != $model->process_user_id || $uidLogin != $model->uid_login )
                return false;
        }
        return true;
    }
    
    /**
     * @Author: ANH DUNG Aug 09, 2014
     * @Todo: xử lý post reply tickeet, không xử lý get
     * @Param: $id pk
     */
    public function actionReply($id)
    {
    try{
        $json = ['success'=>true, 'msg'=>''];
        $model = $this->loadModel($id);
        $model->oldIssueType = $model->issue_type;
        $msg = "Có Lỗi không thể Reply Ticket ( invalid message ).";
        $FlashScenario = 'ErrorUpdate';
        $CanReply = $this->CanReply($model);
        $cRole = MyFormat::getCurrentRoleId();
        if(isset($_POST['GasTickets']) && $model->status == GasTickets::STATUS_OPEN && $CanReply){
            $model->attributes  = $_POST['GasTickets'];
            $model->scenario    = 'reply';
            $model->validate();
            if(!$model->hasErrors()){
                $msg = "$model->code_no Reply Ticket thành công: $model->title";
                $model->uid_post    = MyFormat::getCurrentUid();
                if(isset($_POST['GasTickets']['send_to_id']) && !empty($_POST['GasTickets']['send_to_id'])){
                    $model->isChangeHandleUser = true;
                }
                $model->SaveOneMessageDetail();
                $model->handleFlowPunish();
            }else{
                $msg                = HandleLabel::FortmatErrorsModel($model->getErrors());
                $json['success']    = false;
            }
        }
        }catch (Exception $exc){
            $msg        = $exc->getMessage();
            $json['success']    = false;
        }
        
        if($json['success']){
            $json['msg'] = MyFormat::renderNotifySuccess($msg);
        }else{
            $json['msg'] = MyFormat::renderNotifyError($msg);
        }
        echo CJavaScript::jsonEncode($json);die; 
    }
    
    public function actionClose_ticket($id){
    try{
        $model = $this->loadModel($id);
        if(!Yii::app()->request->isPostRequest || !$model->canClose())
            throw new Exception('Invalid request');
        $model->uid_post    = MyFormat::getCurrentUid();
        $model->CloseTicket();
        $msg = "Close Ticket thành công.";
        $FlashScenario = 'successUpdate';           
        Yii::app()->user->setFlash($FlashScenario, $msg);
        $this->redirect(array('index'));
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }
    
    public function actionPick_ticket($id){
        $model = $this->loadModel($id);
        if($model->process_status == GasTickets::PROCESS_STATUS_PICK)
        { // nếu thời điểm người click sau thì sẽ không pick dc ticket và phải f5 để cập nhật
            die();
        }
        if(!Yii::app()->request->isPostRequest)
            throw new Exception('Invalid request');
        $model->uid_post    = MyFormat::getCurrentUid();
        GasTickets::UpdateStatusTicket($model, GasTickets::PROCESS_STATUS_PICK);
        
        $ModelCreate=new GasTickets('create');
        $model=new GasTickets('search');
        $model->unsetAttributes();  // clear any default values
        $this->render('index',array(
            'model'=>$model, 
            'ModelCreate'=>$ModelCreate,
            'actions' => $this->listActionsCanAccess,
        ));
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
    * If the data model is not found, an HTTP exception will be raised.
     * @param integer the ID of the model to be loaded
     */
    public function loadModel($id)
    {
        try{
            $model=GasTickets::model()->findByPk($id);
            if($model===null)
            {
                Yii::log("The requested page does not exist.");
                throw new CHttpException(404,'The requested page does not exist.');
            }			
            return $model;
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }
    
    /** @Author: ANH DUNG Jan 06, 2018
     *  @Todo: report fix ticket
     **/
    public function actionAuditFix()
    {
        $this->pageTitle = 'Báo cáo sửa ticket';
        try{
            $model=new GasTickets();
            $model->date_from   = "01-".date('m-Y');
            $model->date_to     = date('d-m-Y');
            if(isset($_GET['GasTickets']))
                $model->attributes=$_GET['GasTickets'];
            
            $rData = $model->auditFix();
//          @Code: NAM011
            $_SESSION['data-model'] = $model;
            
            $this->render('report/auditFix',array(
                'model'=>$model, 
                'rData'=>$rData, 
                'actions' => $this->listActionsCanAccess,
            ));
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }
    
    /** @Author: HOANG NAM 23/04/2018
     *  @Todo: export to excel
     *  @Code: NAM011
     **/
    public function actionExportExcel(){
        try{
        if(isset($_SESSION['data-model'])){
            ini_set('memory_limit','1500M');// chưa cần chạy đoạn này, đưa memory_limit lên 500 có thể export dc khoảng 5000 row - chưa test
            ToExcel::summaryAuditFix();
        }
        $this->redirect(array('AuditFix'));
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }
    
    /** @Author: PHAM THANH NGHIA 2018
     *  @Todo: show list tickets
     *  @Code: NGHIA001
     *  @Param: 
     *  
     **/
    public function actionList(){
        $this->pageTitle = 'Danh sách Tickets';
        
        try {
            $model=new GasTicketsDetail('search');
            $model->unsetAttributes();
            if(isset($_GET['GasTicketsDetail'])){
                $model->attributes=$_GET['GasTicketsDetail'];
            }
            
            $this->render('list/list',
                    array(
                    'model'=>$model, 
                    'actions' => $this->listActionsCanAccess,
            ));
        } catch (Exception $exc) {
            GasCheck::CatchAllExeptiong($exc);
        }
             
    }
    
    /** @Author: HOANG NAM 04/06/2018
     *  @Todo: view from index
     **/
    public function viewTicket()
    {
        if(!isset($_GET['ViewTicket'])){
            return ;
        }
        $this->pageTitle = 'Xem ';
        $gastickets = GasTicketsDetail::model()->findByPk($_GET['ViewTicket']);
        $id = $gastickets->ticket_id;
        try{
            $this->render('list/view',array(
                    'model'=>$this->loadModel($id), 'actions' => $this->listActionsCanAccess,
            ));
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }
    
    /** @Author: HOANG NAM 04/06/2018
     *  @Todo: view from index
     **/
    public function viewList()
    {
        if(!isset($_GET['ViewList'])){
            return ;
        }
        $this->pageTitle = 'Danh sách Tickets';
        try {
            $model=new GasTicketsDetail('search');
            $model->unsetAttributes();
            if(isset($_GET['GasTicketsDetail'])){
                $model->attributes=$_GET['GasTicketsDetail'];
            }
            
            $this->render('list/list',
                    array(
                    'model'=>$model, 
                    'actions' => $this->listActionsCanAccess,
            ));
        } catch (Exception $exc) {
            GasCheck::CatchAllExeptiong($exc);
        }
        die;
    }
    
}
