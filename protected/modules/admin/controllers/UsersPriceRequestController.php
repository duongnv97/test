<?php

class UsersPriceRequestController extends AdminController 
{
    public $pluralTitle = "Đề xuất giá";
    public $singleTitle = "Đề xuất giá";
    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id)
    {
        $this->layout = 'ajax';
        try{
            $model = $this->loadModel($id);
            if($model->type != UsersPriceRequest::TYPE_REQUEST_PRICE){
                die;
            }

            $model->getScenarioApproved();
            $this->handleApproved($model);
        $this->render('view',array(
                'model'=>$model, 'actions' => $this->listActionsCanAccess,
        ));
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }
    
    /**
     * @Author: ANH DUNG Dec 22, 2016
     * @Todo: handle approved
     */
    public function handleApproved($model) {
        if(isset($_POST['UsersPriceRequest'])){
            $model->attributes=$_POST['UsersPriceRequest'];
            $model->validate();
            $model->handleApprovedValidate();
            if(!$model->hasErrors()){
                $model->handleApproved();
                Yii::app()->user->setFlash( MyFormat::SUCCESS_UPDATE, "Cập nhật trạng thái duyệt thành công" );
                $this->redirect(array('view','id'=>$model->id));
            }else{
                
                Yii::app()->user->setFlash( MyFormat::ERROR_UPDATE, "Có lỗi xảy ra: <br>".HandleLabel::FortmatErrorsModel($model->getErrors()) );
            }
        }
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate()
    {
        $this->pageTitle = 'Tạo Mới ';
        try{
        $model =new UsersPriceRequest('create');
        $this->checkRequest($model);
        $model->initForCreate();
        if(isset($_POST['UsersPriceRequest']))
        {
            $model->attributes=$_POST['UsersPriceRequest'];
            $model->validate();
            if(!$model->hasErrors()){
                $model->saveCreate();
                $needMore = array('aUidMerge' => array($model->uid_approved_1));
                GasScheduleEmail::buildNotifyPriceRequest($model, $needMore);
                Yii::app()->user->setFlash( MyFormat::SUCCESS_UPDATE, "Thêm mới thành công." );
                $this->redirect(array('create'));
            }else{
                Yii::app()->user->setFlash( MyFormat::ERROR_UPDATE, $model->getError('uid_approved_1'));
            }
        }

        $this->render('create',array(
                'model'=>$model, 'actions' => $this->listActionsCanAccess,
        ));
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }

    /**
     * @Author: ANH DUNG Apr 18, 2017
     * @Todo: check không cho user tạo và cập nhật record sau ngày 28
     */
    public function checkRequest($model) {
        if(!$model->canCreateOrUpdate()){
            $this->redirect(array('index'));
        }
    }
    
    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id)
    {
        $this->pageTitle = 'Cập Nhật ';
        try{
        $model=$this->loadModel($id);
        $this->checkRequest($model);
        $model->scenario = 'update';
        if (!$model->canUpdate()) {
            $this->redirect(array('index'));
        }
        $this->handleScheduleAscending($model);
        $model->buildArrayDetailUpdate();
        if(isset($_POST['UsersPriceRequest'])){
            $model->attributes=$_POST['UsersPriceRequest'];
            $model->validate();
            if(!$model->hasErrors()){
                $model->saveCreate();
                Yii::app()->user->setFlash( MyFormat::SUCCESS_UPDATE, "Cập nhật thành công." );
                $this->redirect(array('update','id'=>$model->id));
            }else{
                Yii::app()->user->setFlash( MyFormat::ERROR_UPDATE, $model->getError('uid_approved_1') );
            }
        }

        $this->render('update',array(
                'model'=>$model, 'actions' => $this->listActionsCanAccess,
        ));
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id)
    {
        try{
        if(Yii::app()->request->isPostRequest){
            // we only allow deletion via POST request
            if($model = $this->loadModel($id))
            {
                if($model->delete())
                    Yii::log("Uid: " .Yii::app()->user->id. " Delete record ".  print_r($model->attributes, true), 'info');
            }

            // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
            if(!isset($_GET['ajax']))
                    $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('index'));
        }
        else
        {
            Yii::log("Uid: " .Yii::app()->user->id. " Invalid request. Please do not repeat this request again.");
            throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
        }	
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }

    /**
     * Manages all models.
     */
    public function actionIndex()
    {
        $this->pageTitle = 'Danh Sách ';
        try{
        $model=new UsersPriceRequest('search');
        $model->unsetAttributes();  // clear any default values
        $model->c_month = (date('m')*1)+1;
        $model->c_year  = date('Y');
        $model->initSessionUserApproved();  // clear any default values
        if(isset($_GET['UsersPriceRequest']))
            $model->attributes=$_GET['UsersPriceRequest'];
        $cRole = MyFormat::getCurrentRoleId();
        if($cRole != ROLE_ADMIN){
//            $model->type  = UsersPriceRequest::TYPE_REQUEST_PRICE;
        }

        $this->render('index',array(
                'model'=>$model, 'actions' => $this->listActionsCanAccess,
        ));
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
    * If the data model is not found, an HTTP exception will be raised.
     * @param integer the ID of the model to be loaded
     */
    public function loadModel($id)
    {
        try{
        $model=UsersPriceRequest::model()->findByPk($id);
        if($model===null)
        {
            Yii::log("The requested page does not exist.");
            throw new CHttpException(404,'The requested page does not exist.');
        }			
        return $model;
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }
    
    /**
     * @Author: ANH DUNG Dec 22, 2016
     * @Todo: update list KH tăng định kỳ
     */
    public function handleScheduleAscending($model)
    {
        if($model->type != UsersPriceRequest::TYPE_PRICE_ASCENDING){
            return ;
        }
        $this->pageTitle = 'Khách hàng tăng định kỳ';
        try{
        if(isset($_POST['customer_id']))
        {
            if($model->saveUpdateSchedule()){
                Yii::app()->user->setFlash( MyFormat::SUCCESS_UPDATE, "Cập nhật thành công thành công." );
                $this->redirect(array('update','id'=>$model->id));
            }else{
                Yii::app()->user->setFlash( MyFormat::ERROR_UPDATE, $model->getError('uid_approved_1'));
            }
        }
        $model->buildArrayDetailUpdate();

        $this->render('update/ScheduleAscending',array(
                'model'=>$model, 'actions' => $this->listActionsCanAccess,
        ));
        die;
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }

}
