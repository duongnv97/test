<?php

class GasSettleController extends AdminController
{
    public $pluralTitle = "Quyết toán";
    public $singleTitle = "Quyết toán";

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id)
    {
        $this->pageTitle = 'Xem Quyết toán';
        $this->layout = 'ajax';
        $this->updateContractDetail();
        try {
            $cUid = MyFormat::getCurrentUid();
            /** @var GasSettle $model */
            $model = $this->loadModel($id);
            $model->modelOld = $model;
            if( !in_array($cUid, $model->getUidViewOnlyAllSystem()) && (!$model->canView() || !$model->canViewCashierConfirm())){
                die('<script type="text/javascript">parent.$.fn.colorbox.close();</script>');
            }
            $this->handleCashierConfirm($model);
            $this->handleExportExcel($model);
            if (isset($_POST['GasSettle'])) {
                // Check data
                $model = $this->loadModel($id);
                $model->scenario = 'update_from_server';
                $model->attributes = $_POST['GasSettle'];
                $model->validate();
                // Save to db
                if (!$model->hasErrors()) {
                    $model->toUpdateStatus();
                    die('<script type="text/javascript">parent.$.fn.colorbox.close(); parent.$.fn.yiiGridView.update("gas-settle-grid"); </script>');
                }
            }
            $model->renew_expiry_date = MyFormat::dateConverYmdToDmy($model->renew_expiry_date);
            $this->render('view', array(
                'model' => $model, 'actions' => $this->listActionsCanAccess,
            ));
        } catch (Exception $exc) {
            GasCheck::CatchAllExeptiong($exc);
        }
    }
    
    /**
     * @Author: DungNT Jul 25, 2016
     * @Todo: to handle CashierConfirm
     */
    public function handleCashierConfirm($model) {
        if(isset($_GET['CashierConfirm']) && isset($_GET['confirm_value'])){
            if($model->canCashierConfirm()){
                $model->handleCashierConfirm($_GET['confirm_value']);
                Yii::app()->user->setFlash(MyFormat::SUCCESS_UPDATE, "Xác Nhận Chi Tiền thành công");
                $this->redirect(array('view', 'id' => $model->id));
            }
        }
    }
    
    /**
     * @Author: DungNT Sep 19, 2016
     */
    public function handleExportExcel($model) {
        if(isset($_GET['to_excel']) && $model->canExportExcel()){
            try{
        //            ini_set('memory_limit','1000M'); chưa cần chạy đoạn này, đưa memory_limit lên 500 có thể export dc khoảng 5000 row - chưa test
                    ExportList::SettleView($model);
            }catch (Exception $exc){
                GasCheck::CatchAllExeptiong($exc);
            }
        }
        // Nghia AUG 2, 2018
        if(isset($_GET['ToExcelHomeContact']) && $model->getListTypeSettleHomeContract()){
            try{
        //            ini_set('memory_limit','1000M'); chưa cần chạy đoạn này, đưa memory_limit lên 500 có thể export dc khoảng 5000 row - chưa test
                    $exportList = new ExportList();
                    $exportList->exportHomeContract($model);
            }catch (Exception $exc){
                GasCheck::CatchAllExeptiong($exc);
            }
        }
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate()
    {
        $this->pageTitle = 'Tạo Mới ';
        try {
            $model              = new GasSettle('create');
            $model->aDetail     = array();
            $mGasFile = new GasFile();
            if (isset($_POST['GasSettle'])) {
                $model->attributes = $_POST['GasSettle'];
                $model->resetFieldByType();
                $model->uid_login = Yii::app()->user->id;
                $model->validate();

                $model->checkCreateMoreSettle();
                $model->checkInfoViettelPay();
                $mGasFile->validateFile($model);
                $model->lockFunctionUserNotGetSalary($model->rUidLogin);
                if (!$model->hasErrors() && $model->save()) {
                    $mGasFile->saveRecordFile($model, GasFile::TYPE_6_SETTLE_CREATE);
                    $model->saveSettleKhongTamUng();
                    $model->saveCreateComment();
                    GasScheduleEmail::BuildListNotifySettle($model, array($model->uid_leader));// Jul 10, 2016
                    Yii::app()->user->setFlash(MyFormat::SUCCESS_UPDATE, "Thêm mới thành công.");
                    $this->redirect(array('index'));
                }
            }

            $this->render('create', array(
                'model' => $model, 'actions' => $this->listActionsCanAccess,
            ));
        } catch (Exception $exc) {
            GasCheck::CatchAllExeptiong($exc);
        }
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id)
    {
        $this->pageTitle = 'Cập Nhật Quyết toán';
        try {
            $model = $this->loadModel($id);
            $model->loadInfoDebit();

            if ($model->type == GasSettle::TYPE_AFTER_SETTLE) {
                $this->handleSettleType($model, $model->parent_id);
            } else {
                // Check update data
                if (!$model->canUpdate()) {
                    $this->redirect(array('index'));
                }
                $model->scenario = 'update';
                $model->aDetail  = $model->rDetail;
                $mGasFile = new GasFile();

                if (isset($_POST['GasSettle'])) {
                    $model->attributes = $_POST['GasSettle'];
                    if ($model->status == GasSettle::STA_REQUEST_EDIT) {
                        $model->status = GasSettle::STA_NEW;
                    }
                    $model->resetFieldByType();
                    $model->validate();
                    $model->checkCreateMoreSettle();
                    $model->checkInfoViettelPay();
                    $mGasFile->validateFile($model);
                    if (!$model->hasErrors()) {
                        $mGasFile->deleteFileInUpdate($model);
                        $model->update();
                        $mGasFile->saveRecordFile($model, GasFile::TYPE_6_SETTLE_CREATE);
                        $model->saveSettleKhongTamUng();
//                        $model->saveToDebit();// Mar1819 move to bam chi tien tren web thi moi tao Debit
                        Yii::app()->user->setFlash(MyFormat::SUCCESS_UPDATE, "Cập nhật thành công.");
                        $this->redirect(array('update', 'id' => $model->id));
                    }
                }

                $this->render('update', array(
                    'model' => $model, 'actions' => $this->listActionsCanAccess,
                ));
            }
        } catch (Exception $exc) {
            GasCheck::CatchAllExeptiong($exc);
        }
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id)
    {
        try {
            if (Yii::app()->request->isPostRequest) {
                // we only allow deletion via POST request
                if ($model = $this->loadModel($id)) {
                    if ($model->delete())
                        Yii::log("Uid: " . Yii::app()->user->id . " Delete record " . print_r($model->attributes, true), 'info');
                }

                // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
                if (!isset($_GET['ajax']))
                    $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('index'));
            } else {
                Yii::log("Uid: " . Yii::app()->user->id . " Invalid request. Please do not repeat this request again.");
                throw new CHttpException(400, 'Invalid request. Please do not repeat this request again.');
            }
        } catch (Exception $exc) {
            GasCheck::CatchAllExeptiong($exc);
        }
    }

    /**
     * Manages all models.
     */
    public function actionIndex()
    {
        $headTitle = 'Quyết toán';
        $this->pageTitle = $headTitle;
        $this->getInfomationContract();
        try {
            $model = new GasSettle('search');
            $model->initSessionUserSubAgent();
            $model->initSessionUserApproved();
            $model->unsetAttributes();  // clear any default values
            if (isset($_GET['GasSettle']))
                $model->attributes = $_GET['GasSettle'];
            $_SESSION['modelGasSettle'] = $model;
            $this->exportExcelForIndex($model);
            $this->render('index', array(
                'model' => $model, 'actions' => $this->listActionsCanAccess,
                'headTitle' => $headTitle
            ));
        } catch (Exception $exc) {
            GasCheck::CatchAllExeptiong($exc);
        }
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer the ID of the model to be loaded
     */
    public function loadModel($id)
    {
        try {
            $model = GasSettle::model()->findByPk($id);
            if ($model === null) {
                Yii::log("The requested page does not exist.");
                throw new CHttpException(404, 'The requested page does not exist.');
            }
            $model->mapFieldComments();
            return $model;
        } catch (Exception $exc) {
            GasCheck::CatchAllExeptiong($exc);
        }
    }

    /**
     * @Author: TRUNG June 09 2016
     * For create new Settle type
     * @param $id parent id
     * @throws CHttpException
     */
    public function actionCreateSettle($id)
    {
        $this->pageTitle = 'Làm Quyết toán';
        try {
            $model = new GasSettle('create');
            $this->handleSettleType($model, $id);
        } catch (Exception $exc) {
            GasCheck::CatchAllExeptiong($exc);
        }
    }

    /**
     * @Author: TRUNG June 09 2016
     * Xữ lý riêng cho data quyết toán
     * @param $model GasSettle đối tượng quyết toán sẽ cập nhật
     * @param $parent_id int id cha (id của đối tượng tạm ứng)
     */
    private function handleSettleType($model, $parent_id)
    {
        $parent = GasSettle::model()->findByPk($parent_id);
        $this->checkCreateMoreQuyetToan($parent);
        $items = null; $mGasFile = new GasFile();
        if ($model != null && isset($_POST['GasSettle'])) {
            $mGasFile->deleteFileInUpdate($model);
            // Lưu thông tin quyết toán
            $items = $model->saveSettleType($parent, $_POST['GasSettle'], $_POST['SettleRecord']);
        }

        // Kiểm tra lưu thành công kết quả không?
        if (!is_null($items)) { // fix by Anh Dung Jul 10, 2016
            GasScheduleEmail::BuildListNotifySettle($model, array($model->uid_leader));// Jul 10, 2016
            // Thông báo thành công
            $msg = "Thêm mới thành công";// AnhDung Jul 14, 2016
            $url = Yii::app()->createAbsoluteUrl("admin/gasSettle/index");
            $cAction = strtolower(Yii::app()->controller->action->id);
            if($cAction == "update"){
                $msg = "Cập nhật thành công";
                $url = Yii::app()->createAbsoluteUrl("admin/gasSettle/update", array('id'=>$model->id));
            }
            Yii::app()->user->setFlash(MyFormat::SUCCESS_UPDATE, $msg);
            $this->redirect($url);
        } else {
            // Hiển thị bình thường cho trường hợp không lưu được hoặc hiển thị view tạo mới
//            if (isset($parent) && is_null($items)) {// Old 
            if (!is_null($parent)) {// fix by Anh Dung Jul 10, 2016
                // Lấy record con mặc định nếu không tồn tại đối tượng sau khi save
                $items = $parent->getSettleChilds();
            }

            $this->render('create_settle', array(
                'model' => $model, 'actions' => $this->listActionsCanAccess,
                'recordList' => $items,
                'parentId' => $parent_id
            ));
        }
    }
    
    /**
     * @Author: DungNT Oct 20, 2016
     * @Todo: check can create MoreQuyetToan
     */
    public function checkCreateMoreQuyetToan($model) {
        $cAction = strtolower(Yii::app()->controller->action->id);
        if(!$model->canCreateMoreQuyetToan() && $cAction == "createsettle"){
            $this->redirect(array('index'));
        }
    }
    
    /**
     * @Author: DungNT Jul 25, 2016
     * @Todo: thủ quỹ xác nhận chi tiền
     */
    public function actionCashierConfirm()
    {
        // nothing to do, sẽ handle trong cùng action view, cái này chỉ để check quyền
    }
    
    /**
     * @Author: DungNT Sep 27, 2016
     * @Todo: gỡ bỏ quá hạn của 1 tạm ứng
     * StatusUnlock
     */
    public function actionStatusUnlock($id)
    {
        $this->layout = 'ajax';
        $model = $this->loadModel($id);
        if(isset($_POST['GasSettle'])){
            $model->attributes = $_POST['GasSettle'];
            if(empty($model->renew_expiry_date) && empty($_POST['InputRemoveLock'])){
                $model->addError('renew_expiry_date', 'Ngày gia hạn không thể trống');
                Yii::app()->user->setFlash(MyFormat::ERROR_UPDATE, 'Có lỗi xảy ra');
                $this->render('view', array(
                    'model' => $model, 'actions' => $this->listActionsCanAccess,
                ));
                die;
            }
            $model->handleStatusUnlock();
            Yii::app()->user->setFlash(MyFormat::SUCCESS_UPDATE, 'Gia hạn tạm ứng thành công');
            $model->sendEmailRenewSuccess();
        }
        
        $this->redirect(array('view', 'id' => $model->id));
    }
    /** @Author: Pham Thanh Nghia Sep 7, 2018
     *  @Todo: Xuất excel, gasSettle/index
     **/
    public function exportExcelForIndex($model){
        try{
            if(!$model->canExportExcelForIndex() || !isset($_GET['ExportExcel'])){
                return ;
            }
            if(isset($_SESSION['dataGasSettleExcel'])){ // $this->type == GasSettle::TYPE_BANK_TRANSFER_MULTI
                ini_set('memory_limit','2000M');// chưa cần chạy đoạn này, đưa memory_limit lên 500 có thể export dc khoảng 5000 row - chưa test
                $mToExcelList = new ToExcelList();
                $mToExcelList->exportGasSettle();
            }
            if(isset($_SESSION['dataGasSettleExcelAll'])){ // $this all 
                ini_set('memory_limit','2000M');
                $mToExcelList = new ToExcelList();
                $mToExcelList->exportGasSettleAll();
            }
            $this->redirect(array('index'));
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }

    public function actionNoteTreasurerAccountant($id)
    {
        $this->pageTitle = 'Ghi chú';
        $model = $this->loadModel($id);
        $this->layout='ajax';
        try {
            if (empty($model->rCommentThuQuy->content)) {
                $model->comment_thu_quy = '';
            } else {
                $model->comment_thu_quy = $model->rCommentThuQuy->content;
            }
            if (empty($model->rCommentKeToan->content)) {
                $model->comment_ke_toan = '';
            } else {
                $model->comment_ke_toan = $model->rCommentKeToan->content;
            }

            if (isset($_POST['GasSettle'])) {
                $model->attributes = $_POST['GasSettle'];
                if ($model->canUpdateComment()) {
                    if ($model->saveUpdateComment()) {
                        Yii::app()->user->setFlash( MyFormat::SUCCESS_UPDATE, "Cập nhật thành công." );
                        $url = Yii::app()->createAbsoluteUrl("admin/gasSettle/noteTreasurerAccountant",
                            array('id' => $id)
                        );
                        $this->redirect($url);
                    } else {
                        Yii::app()->user->setFlash( MyFormat::ERROR_UPDATE, "Cập nhật ghi chú thất bại." );
                    }
                }
            }

            $this->render('note/form_note', array(
                'model' => $model,
                'actions' => $this->listActionsCanAccess,
            ));
        } catch (Exception $exc) {
            GasCheck::CatchAllExeptiong($exc);
        }
    }
    
//    get response info of contract
    public function getInfomationContract(){
        if(isset($_GET['AJAX_GET_INFO'])){
            $result = [];
            $id = isset($_GET['idContract']) ? $_GET['idContract'] : 0;
            $mContractListManagement = ContractListManagement::model()->findByPk($id);
            if(!empty($mContractListManagement)){
                $result['customer_name'] = $mContractListManagement->getCustomerNameText();
                $result['bank_account_number'] = $mContractListManagement->bank_account_number;
                $result['bank'] = $mContractListManagement->bank;
                $result['bank_branch'] = $mContractListManagement->bank_branch;
                $result['province_id'] = $mContractListManagement->province_id;
            }
            echo json_encode($result);
            die;
        }
    }
    
    public function updateContractDetail(){
        if(isset($_GET['updateHomeContractDetail'])){
            $id = $_GET['updateHomeContractDetail'];
            $mHomeContractDetail = HomeContractDetail::model()->findByPk($id);
            if(!empty($mHomeContractDetail)){
                $mHomeContractDetail->status = HomeContractDetail::STATUS_PAY;
                $mHomeContractDetail->update(['status']);
            }
        }
    }
}
