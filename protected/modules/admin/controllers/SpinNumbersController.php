<?php

class SpinNumbersController extends AdminController
{
    public $pluralTitle  = 'Quản lý số quay thưởng';    
    
    public function actionIndex()
    {
        $this->pageTitle = 'Quản lý số quay thưởng';
        try
        {
            $model = new SpinNumbers('search');
            $model->unsetAttributes(); // return value defalt to be null
            if (isset($_GET['SpinNumbers'])){
                $model->attributes = $_GET['SpinNumbers'];
            }
            $this->render('index',array(
                'model'=>$model,
                'actions'=>$this->listActionsCanAccess,
            ));
        }
        catch (Exception $exc)
        {
            GasCheck::CatchAllExeptiong($exc);
        }
    }
    
    /** @Author: DuongNV Sep1219
     *  @Todo: tự động tạo và gán số cho nv, for test
     **/
    public function actionDemo()
    {
        $this->pageTitle = 'Tạo mới dãy số';
        try
        {
            $model  = new SpinNumbers('search');
            if(!$model->canAutogen()){
                $this->redirect('index');
            }
            // Tự động tạo n số
            if(isset($_GET['auto_gen'])){
                $max    = empty($max) ? SpinNumbers::LIMIT_AMOUNT_NUMBER : $max;
                $ret    = $model->autoGenNumber($max);
                if($ret){
                    Yii::app()->user->setFlash(MyFormat::SUCCESS_UPDATE, "Tạo thành công $max số!");
                } else {
                    Yii::app()->user->setFlash(MyFormat::SUCCESS_UPDATE, "Overload");
                }
            }
            
            // assign number to user
            if(isset($_POST['SpinNumbers'])){
                $post       = $_POST['SpinNumbers'];
                $aRole      = empty($post['role_id']) ? [] : $post['role_id'];
                $limitUser  = empty($post['limit_assign']) ? '' : $post['limit_assign'];
                $userAssign = empty($post['user_assign']) ? '' : $post['user_assign'];
                $this->assignNumber($userAssign, $aRole, $limitUser);
            }
            
            $this->render('demo',array(
                'model'=>$model,
                'actions'=>$this->listActionsCanAccess,
            ));
        }
        catch (Exception $exc)
        {
            GasCheck::CatchAllExeptiong($exc);
        }
    }
    
    public function actionDelete($id)
    {
        try {
            $cRole = MyFormat::getCurrentRoleId();
            if($cRole != ROLE_ADMIN){
                $this->redirect(['index']);
            }
            if (Yii::app()->request->isPostRequest) {
                // we only allow deletion via POST request
                if ($model = $this->loadModel($id)) {
                    if ($model->delete())
                        Yii::log("Uid: " . Yii::app()->user->id . " Delete record " . print_r($model->attributes, true), 'info');
                }

                // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
                if (!isset($_GET['ajax']))
                    $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('index'));
            } else {
                Yii::log("Uid: " . Yii::app()->user->id . " Invalid request. Please do not repeat this request again.");
                throw new CHttpException(400, 'Invalid request. Please do not repeat this request again.');
            }
        } catch (Exception $exc) {
            GasCheck::CatchAllExeptiong($exc);
        }
    }
    
    public function loadModel($id)
    {
        try {
            $model = SpinNumbers::model()->findByPk($id);
            if ($model === null) {
                Yii::log("The requested page does not exist.");
                throw new CHttpException(404, 'The requested page does not exist.');
            }
            return $model;
        } catch (Exception $exc) {
            GasCheck::CatchAllExeptiong($exc);
        }
    }
    
    /** @Author: DuongNV Sep1919
     *  @Todo: test notify app
     **/
    public function actionTestNotify($user_id, $type = GasScheduleNotify::GAS24H_NEW_LUCKY_NUMBER){
        $cRole = MyFormat::getCurrentRoleId();
        if(!empty($user_id) && $cRole == ROLE_ADMIN){
            $model = new SpinNumbers();
            $mUser = Users::model()->findByPk($user_id);
            $phone = empty($mUser->username) ? '' : $mUser->username;
            $res   = $model->notifyUser($user_id, $phone, $type);
            echo $res ? "ok - uid: $user_id, phone: $phone" : "fail - phone: $phone - $model->error_message";
            die;
        }
        echo 'empty user id';
    }
    
    /** @Author: DuongNV Sep1919
     *  @Todo: assign number
     **/
    public function assignNumber($userAssign, $aRole, $limitUser){
        if(isset($_POST['AssignNumber'])){
            $model          = new SpinNumbers();
            $res            = 0;
            if(!empty($userAssign)){
                $source     = empty($_POST['SpinNumbers']['source']) ? SpinNumbers::SOURCE_TYPE_TEST : $_POST['SpinNumbers']['source'];
                $mUser      = Users::model()->findByPk($userAssign);
                $model->phone = empty($mUser) ? '' : $mUser->username;
                if(empty($model->phone) && !empty($mUser)){
                    $model->phone = empty(explode('-', $mUser->phone)[0]) ? '' : explode('-', $mUser->phone)[0];
                }
                $model->saveUserNumber($userAssign, $source, '');
                $res        = 1;
            } else {
                $aUid       = Users::getArrIdUserByRole($aRole);
                $mSpin      = new SpinNumbers();
                $mSpin->user_id = $aUid;
                $res        = $mSpin->assignMultiUser($limitUser);
            }
            if($res > 0){
                Yii::app()->user->setFlash(MyFormat::SUCCESS_UPDATE, "Gán thành công cho $res KH!");
            }
        }
    }
    
    
    
}