<?php
 
class ForecastController extends AdminController 
{
    public $pluralTitle = 'Dự báo gas';
    public $singleTitle = 'Dự báo gas';
    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id)
    {
        $this->pageTitle = 'Xem ';
        try{
        $this->render('view',array(
                'model'=>$this->loadModel($id), 'actions' => $this->listActionsCanAccess,
        ));
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }

    }
    /**
     * Manages all models.
     */
    public function actionIndex()
    {
        $this->pageTitle = 'Danh Sách ';
        try{
        $model=new Forecast('search');
        $model->unsetAttributes();  // clear any default values
        if(isset($_GET['Forecast']))
                $model->attributes=$_GET['Forecast'];

        $this->render('index',array(
                'model'=>$model, 'actions' => $this->listActionsCanAccess,
        ));
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
    * If the data model is not found, an HTTP exception will be raised.
     * @param integer the ID of the model to be loaded
     */
    public function loadModel($id)
    {
        try{
        $model=Forecast::model()->findByPk($id);
        if($model===null)
        {
            Yii::log("The requested page does not exist.");
            throw new CHttpException(404,'The requested page does not exist.');
        }			
        return $model;
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }
    
    public function actionSendNotify($id)
    {
        $this->pageTitle = 'Xem ';
        try{
            $model = $this->loadModel($id);
            if(!$model->canCreateNotify()){
                die;
            }
            $mForecastDetail    = new ForecastDetail();
            $date_forecast      = date('Y-m-d');
            $createdDate        = date('Y-m-d H:i:s');
            $date_forecast_search = strtotime($date_forecast);
            $criteria=new CDbCriteria;
            $criteria->compare('t.customer_id', $model->customer_id);
            $criteria->compare('t.date_notify_search',$date_forecast_search);
            $mForecastDetailOld        = ForecastDetail::model()->find($criteria);
            if(empty($mForecastDetailOld)){
                $mForecastDetail->customer_id           = $model->customer_id;
                $mForecastDetail->date_notify           = $date_forecast;
                $mForecastDetail->date_notify_search    = $date_forecast_search;
                $mForecastDetail->created_date          = $createdDate;
                $mForecastDetail->type_customer         = $model->type_customer;
                $mForecastDetail->save();
                $mForecastDetailOld = $mForecastDetail;
            }
            $mForecast = new Forecast();
            if(in_array($mForecastDetailOld->type_customer, CmsFormatter::$aTypeIdBoMoi)){
                $mForecast->cronNotifyApplyActionTest($mForecastDetailOld,GasScheduleNotify::GAS_SERVICE_REMAIN);
            }else{
                $mForecast->cronNotifyApplyActionTest($mForecastDetailOld,GasScheduleNotify::GAS24H_OUT_OF_GAS);
            }
            echo '<pre>';
            print_r('Ok');
            echo '</pre>';
            die;
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }

    }
    
    public function actionSendNotifyRemain($id)
    {
        $this->pageTitle = 'Xem ';
        try{
            $model = $this->loadModel($id);
            if(!$model->canCreateNotify()){
                die;
            }
            $mForecastDetail    = new ForecastDetail();
            $date_forecast      = date('Y-m-d');
            $createdDate        = date('Y-m-d H:i:s');
            $date_forecast_search = strtotime($date_forecast);
            $criteria=new CDbCriteria;
            $criteria->compare('t.customer_id', $model->customer_id);
            $criteria->compare('t.date_notify_search',$date_forecast_search);
            $mForecastDetailOld        = ForecastDetail::model()->find($criteria);
            if(empty($mForecastDetailOld)){
                $mForecastDetail->customer_id           = $model->customer_id;
                $mForecastDetail->date_notify           = $date_forecast;
                $mForecastDetail->date_notify_search    = $date_forecast_search;
                $mForecastDetail->created_date          = $createdDate;
                $mForecastDetail->type_customer         = $model->type_customer;
                $mForecastDetail->save();
                $mForecastDetailOld = $mForecastDetail;
            }
            $mForecast = new Forecast();
            $mForecast->cronNotifyApplyActionTest($mForecastDetailOld,GasScheduleNotify::GAS24H_REMAIN);
            echo '<pre>';
            print_r('Ok');
            echo '</pre>';
            die;
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }

    }
    
    public function actionUpdateForecast($id){
        $this->pageTitle = 'Xem ';
        try{
            $model = $this->loadModel($id);
            if(!empty($model)){
                $mForecast  = new Forecast();
                $mUser      = Users::model()->findByPk($model->customer_id);
                if(!empty($mUser)){
                    $mForecast->createForecast($mUser);
                }
            }
            Yii::app()->end();
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
        
    }
}
