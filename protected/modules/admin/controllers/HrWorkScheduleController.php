<?php

class HrWorkScheduleController extends AdminController 
{
    public $pluralTitle = "lịch làm việc";
    public $singleTitle = "lịch làm việc";
    
    public function actionIndex()
    {
        $this->pageTitle = 'Danh sách lịch làm việc';
        try{
            $model=new HrWorkSchedule('search');
            if(isset($_GET['HrWorkSchedule'])) {
                $model->attributes = $_GET['HrWorkSchedule'];
            }

            $this->render('index',array(
                    'model'=>$model, 'actions' => $this->listActionsCanAccess,
            ));
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }
    public function actionView($id)
    {
        $this->pageTitle = 'Xem lịch làm việc';
        try{
                $this->render('view',array(
                    'model'=>$this->loadModel($id), 'actions' => $this->listActionsCanAccess,
            ));
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }
    public function actionCreate($date = null)
    {
        $this->pageTitle = 'Tạo Mới lịch làm việc';
        try{
            $model=new HrWorkSchedule();
            if(isset($_GET['date'])){
                $this->layout = 'ajax';
            }
            if(isset($_POST['HrWorkSchedule'])){
                $model->attributes = $_POST['HrWorkSchedule'];
                $model->work_day = MyFormat::dateConverDmyToYmd($model->work_day,'-');

                $model->validate();

                if(!$model->hasErrors()){
                    $model->save();
                    Yii::app()->user->setFlash( MyFormat::SUCCESS_UPDATE, "Thêm mới thành công." );
                    if($this->layout == 'ajax') $this->redirect(array('create','date'=>$_GET['date']));
                    else $this->redirect(array('create'));
                }
            }

            $this->render('create',array(
                    'model'=>$model, 'actions' => $this->listActionsCanAccess,
            ));
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }
    public function actionUpdate($id)
    {
        try{
        $model=$this->loadModel($id);

        if(isset($_POST['HrWorkSchedule'])){
            $model->attributes=$_POST['HrWorkSchedule'];
            $model->validate();
            if(!$model->hasErrors()){
                $model->update();
                Yii::app()->user->setFlash( MyFormat::SUCCESS_UPDATE, "Cập nhật thành công." );
            }
        }

        $this->render('update',array(
                'model'=>$model, 'actions' => $this->listActionsCanAccess,
        ));
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }
    public function actionDelete($id)
    {
        try{
        if(Yii::app()->request->isPostRequest)
        {
            // we only allow deletion via POST request
            if($model = $this->loadModel($id))
            {
                if($model->delete()){
                    Yii::log("Uid: " .Yii::app()->user->id. " Delete record ".  print_r($model->attributes, true), 'info');
                }
            }

            // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
            if(!isset($_GET['ajax']))
                $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('index'));
        }
        else
        {
            Yii::log("Uid: " .Yii::app()->user->id. " Invalid request. Please do not repeat this request again.");
            throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
        }
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }
    public function loadModel($id)
    {
        try{
            $model = HrWorkSchedule::model()->findByPk($id);
            if($model===null)
            {
                Yii::log("The requested page does not exist.");
                throw new CHttpException(404,'The requested page does not exist.');
            }			
            return $model;
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }
    
    /** @Author: ANH DUNG Oct 28, 2018
     *  @Todo: khởi tạo 1 cho 1 số role
     **/
    public function initByRole(&$model) {
        $cRole = MyFormat::getCurrentRoleId();
        switch ($cRole) {
            case ROLE_CALL_CENTER:
                $model->search_province = [GasProvince::TP_HCM];
                $model->search_position = [UsersProfile::WORK_ROOM_CALL_CENTER];
                break;
            case ROLE_TELESALE:
                $model->search_province = [GasProvince::TP_HCM];
                $model->search_position = [UsersProfile::WORK_ROOM_TELESALE];
                break;

            default:
                break;
        }
        
        
        
    }
    
    public function actionWorkScheduleUI($datePlanFrom = null, $datePlanTo = null) {
        error_reporting(1); // var_dump(PHP_INT_SIZE);
        ini_set('memory_limit','1500M');
        $this->pageTitle = 'lịch làm việc';
        try{
            $model = new HrWorkSchedule('search');
            $this->initByRole($model);
            if(empty($datePlanFrom)){
                $model->datePlanFrom = isset($_GET['HrWorkSchedule']['datePlanFrom']) ? $_GET['HrWorkSchedule']['datePlanFrom'] : date('01-m-Y');
                $model->datePlanTo   = isset($_GET['HrWorkSchedule']['datePlanTo']) ? $_GET['HrWorkSchedule']['datePlanTo'] : date('t-m-Y'); // last day of month
            } else {
                $model->datePlanFrom = $datePlanFrom;
                $model->datePlanTo   = $datePlanTo;
            }
            $model->attributes = isset($_GET['HrWorkSchedule']) ? $_GET['HrWorkSchedule'] : '';
            if(isset($_GET['HrWorkSchedule']['search_province'])){
                $model->search_province = $_GET['HrWorkSchedule']['search_province'];
            }
            if(isset($_GET['HrWorkSchedule']['search_position'])){
                $model->search_position = $_GET['HrWorkSchedule']['search_position'];
            }
            if(!empty($_GET['HrWorkSchedule']['is_view_sum'])){
                $model->is_view_sum = true;
            }
            
//            $dateFromYmd = MyFormat::dateConverDmyToYmd($model->datePlanFrom,'-');
//            $dateToYmd = MyFormat::dateConverDmyToYmd($model->datePlanTo,'-');
            
            //Send mail
//            if(isset($_POST['HrWorkSchedule']['SendMail'])){
//                $mWPlan = $this->handleWorkPlan($role_id, $dateFromYmd, $dateToYmd);
//                $mWPlan->approved = $_POST['HrWorkSchedule']['approver'];
//                if(!$mWPlan->hasErrors()){
//                    $mWPlan->save();
//                    $this->sendMail($mWPlan);
//                    Yii::app()->user->setFlash( MyFormat::SUCCESS_UPDATE, "Gửi mail thành công!" );
//                }
//            }
//            
            //Auto generate schedule
//            $this->autoGenerateWorkSchedule($role_id, $dateFromYmd, $dateToYmd);
            
            $this->saveSchedule($model);
            
            $this->render('workScheduleUI',array(
                    'model'   => $model, 
                    'aData'   => $model->buildHtmlWorkSchedule(),
                    'actions' => $this->listActionsCanAccess,
            ));
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }
    
    /** @Author: DuongNV Oct 24, 18
     *  @Todo: Save schedule
     *  @params $model contain search_position, search_province
     **/
    public function saveSchedule($model) {
        try{
            $aObjUser   = $model->getArrayUserHr();
            $aUser      = array_keys($aObjUser);
            $from       = isset($model->datePlanFrom) ? $model->datePlanFrom : '';
            $to         = isset($model->datePlanTo) ? $model->datePlanTo : '';
            
            // Tự động tạo lịch
            $url_redirect = isset($_POST['HrWorkSchedule']['url']) ? $_POST['HrWorkSchedule']['url'] : '';
            if( isset($_POST['AutoGenerateSchedule']) ){
                if($model->autoGenerateWorkSchedule($aUser, $from, $to)){
                    Yii::app()->user->setFlash( MyFormat::SUCCESS_UPDATE, "Tạo lịch thành công, nhấn 1 lần nữa để tạo lại." );
                    $this->redirect($url_redirect);
                } else {
                    Yii::app()->user->setFlash( MyFormat::ERROR_UPDATE, "Ca hoặc nhân viên rỗng." );
                    $this->redirect($url_redirect);
                }
            }
            
            if( !empty($_POST['deleteScheduleBtn']) ){
                $mHrWorkSchedule = new HrWorkSchedule();
                $mHrWorkSchedule->deleteOldSchedule($aUser, $from, $to);
            }
            
            // Lưu lịch làm việc
            if( isset($_POST['saveBtn']) ) {
                $work_plan_id = 0;
                $aRowInsert = [];
                
                // Delete all old schedule
                $model->deleteOldSchedule($aUser, $from, $to); 
                
                $aObjectUser = Users::getArrObjectUserByRole("", $aUser);
                if(isset($_POST['HrWorkSchedule']['data'])){
                    foreach ($_POST['HrWorkSchedule']['data'] as $shift) {
                        $aData          = json_decode($shift, true); // 0 - shift_id, 1 - date, 2 - employee_id
                        $work_day       = MyFormat::dateConverDmyToYmd($aData[1],'-');
                        $work_shift_id  = $aData[0];
                        $employee_id    = $aData[2];
                        $status         = HrWorkSchedule::model()->status_new;//new
                        $role_id        = $aObjectUser[$employee_id]->role_id;
                        $aRowInsert[]   =  "(NULL,
                                            '{$work_day}',
                                            '{$work_shift_id}',
                                            '{$work_plan_id}',
                                            '{$role_id}',
                                            '{$employee_id}',
                                            '{$status}')";
                    }
                }
                if(!empty($aRowInsert)){
                    $tableName = HrWorkSchedule::model()->tableName();
                    $sql = 'INSERT INTO ' . $tableName . ' VALUES '. implode(',', $aRowInsert);
                    Yii::app()->db->createCommand($sql)->execute();
                    Yii::app()->user->setFlash( MyFormat::SUCCESS_UPDATE, "Thêm mới thành công." );
                }
                $this->redirect($_POST['HrWorkSchedule']['url']);
            }
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }
    
    /** @Author: DuongNV Oct 4, 2018
     *  @Todo: Check plan exists, if true -> return id, else create new and return id
     *  @params: $date_from, $date_to: work plan from/to (Y-m-d)
     *           $role_id
     **/
    public function handleWorkPlan($role_id, $date_from, $date_to) {
        $criteria = new CDbCriteria;
        $criteria->compare('t.role_id', $role_id);
        $criteria->compare('t.date_from', $date_from);
        $criteria->compare('t.date_to', $date_to);
        $mWPlan = HrWorkPlan::model()->find($criteria);
        if(empty($mWPlan)){
            $mPlanNew = new HrWorkPlan();
            $mPlanNew->date_from = $date_from;
            $mPlanNew->date_to = $date_to;
            $mPlanNew->status = HrWorkSchedule::model()->status_new;//new
            $mPlanNew->role_id = $role_id;
            if(!$mPlanNew->hasErrors()){
//                $mPlanNew->save();
                return $mPlanNew;
            }
        } else {
            return $mWPlan;
        }
    }
    
    public function actionCreateWorkShift() {
        $this->redirect(Yii::app()->createAbsoluteUrl('/admin/HrWorkShift/create'));
    }
    
    public function actionListWorkPlan() {
        $this->redirect(Yii::app()->createAbsoluteUrl('/admin/HrWorkPlan'));
    }
    
    
    public function actionUpdateAjax($datePlanFrom = null, $datePlanTo = null, $role = '')
    {
        if(empty($role)){
            $role = array_keys(HrSalaryReports::getArrayRoleSalary())[0];
        }
//        $aManager = HrWorkSchedule::model()->getArrayManager();
//        $cUid = MyFormat::getCurrentUid();
//        if(in_array($cUid, $aManager)){
//            $aRoleOfManager = array_keys($aManager, $cUid);
//            if(!in_array($role, $aRoleOfManager)){
//                echo "Bạn không có quyền xem thông tin.";
//                die;
//            }
//        }
        if(isset($_GET['updateShift'])){
            $this->getAjaxShiftByRole();
        }
        $data = array();
        $model=new HrWorkSchedule('search');
        if(empty($datePlanFrom)){
            $model->datePlanFrom = isset($_GET['HrWorkSchedule']['datePlanFrom']) ? $_GET['HrWorkSchedule']['datePlanFrom'] : date('01-m-Y');
            $model->datePlanTo = isset($_GET['HrWorkSchedule']['datePlanTo']) ? $_GET['HrWorkSchedule']['datePlanTo'] : date('t-m-Y'); // last day of month
        } else {
            $model->datePlanFrom = $datePlanFrom;
            $model->datePlanTo = $datePlanTo;
        }

        if(isset($_GET['HrWorkSchedule'])) {
            $model->attributes = $_GET['HrWorkSchedule'];
        }
        $data['model'] = $model;
        $data['role'] = $role;
        $this->renderPartial('_tblWorkSchedule', $data, false, true);
    }
    
    /** @Author: DuongNV Oct 04,2018
     *  @Todo: change list shift auto generate schedule
     **/
    public function getAjaxShiftByRole() {
        $role_id = !isset($_GET['role']) ? array_keys(HrSalaryReports::getArrayRoleSalary())[0] : $_GET['role'];
        $criteria = new CDbCriteria;
//        $criteria->addCondition("t.role_id = {$role_id} OR t.role_id = " . ROLE_ALL);
        $aShift = HrWorkShift::model()->findAll($criteria);
        $html = '<div>';
        $aPrevShift = isset($_SESSION['autoSchedule']) ? $_SESSION['autoSchedule'] : [];
        foreach ($aShift as $shift) {
            $shift_value = isset($aPrevShift[$shift->id]) ? $aPrevShift[$shift->id] : 0;
            $html .=  '<div style="padding: 4px 0; width: 170px; display: inline-block; text-align: right;">'
                    .   '<div style="display: inline-block; padding-right:5px;">' . $shift->name . ': </div>'
                    .   '<input type="number" name="ShiftAutoGenerate[list_shift]['.$shift->id.']" value="' . $shift_value . '" id="shift-'.$shift->id.'" style="width:60px;">'
                    . '</div>';
        }
        $html .= '</div>';
        echo $html;die;
    }
    
    /** @Author: NVDuong 13/08/18 21:51
     *  @Todo: Send mail to approve work schedule
     *  @Param: $model model HrWorkPlan
     **/
    public function sendMail($model) {
        try {
            $title =  'Yêu cầu duyệt kế hoạch làm việc từ ngày '
                . MyFormat::dateConverYmdToDmy($model->date_from)
                . ' đến ngày '
                . MyFormat::dateConverYmdToDmy($model->date_to);
            $approveUrl = Yii::app()->createAbsoluteUrl("admin/HrWorkSchedule/Approve", array('id' => $model->id));
            GasScheduleEmail::notifyHr($model, $approveUrl, $title);
        } catch (Exception $exc) {
            GasCheck::CatchAllExeptiong($exc);
        }
    }
    
    /** @Author: NVDuong 13/08/18 21:51
     *  @Todo: Approved work schedule
     *  @Param: $id hrWorkPlan
     **/
    public function actionApprove($id) {
        try {
            $cUid = MyFormat::getCurrentUid();
            $model = HrWorkPlan::model()->findByPk($id);
            if($cUid == $model->approved){
                if(isset($_POST['HrWorkPlan']['approve'])){
                    $model->status = HrWorkPlan::model()->status_approved;
                    $model->approved_date = date('Y-m-d H:i:s');
                    if(!$model->hasErrors()){
                        $model->save();
                        Yii::app()->user->setFlash( MyFormat::SUCCESS_UPDATE, "Duyệt thành công!" );
                    }
                }
                if(isset($_POST['HrWorkPlan']['require_update'])){
                    $model->status = HrWorkPlan::model()->status_require_update;
                    $model->approved_date = null;
                    $model->notify = $_POST['HrWorkPlan']['notify'];
                    if(!$model->hasErrors()){
                        $model->save();
                        Yii::app()->user->setFlash( MyFormat::SUCCESS_UPDATE, "Yêu cầu cập nhật thành công!" );
                    }
                }
                $this->render('approve',array(
                        'model'=>$model, 'actions' => $this->listActionsCanAccess,
                ));
            } else {
                throw new CHttpException(404, 'Bạn không có quyền truy cập trang.');
            }
        } catch (Exception $exc) {
            GasCheck::CatchAllExeptiong($exc);
        }
    }
    
}
