<?php
class GasReceiptsController extends AdminController
{
    
    public $pluralTitle  = 'Số biên bản giao nhận';    
    
    public function actionIndex()
    {
        $this->pageTitle = 'Quản lý số biên bản giao nhận';
        try
        {
            $model = new GasReceipts('search');
            $model->unsetAttributes(); // return value defalt to be null
            if (isset($_GET['GasReceipts'])){
                $model->attributes = $_GET['GasReceipts'];
            }
            $this->render('index',array(
                'model'=>$model,
                'actions'=>$this->listActionsCanAccess,
            ));
        }
        catch (Exception $exc)
        {
            GasCheck::CatchAllExeptiong($exc);
        }
    }
    
    public function actionCreate(){
        $this->pageTitle = 'Tạo mới số biên bản giao nhận';
        try{
        $model=new GasReceipts('create_receipts');
        $model->unsetAttributes();
        $model->create_for = isset($_GET['type']) ? $_GET['type'] : GasReceipts::TYPE_FOR_EMPLOYEE;
        
        if(isset($_POST['GasReceipts'])){
            $model->attributes=$_POST['GasReceipts'];
//            $mDetailObject      = $this->getDetailObject($_POST['GasReceipts']['object_id']);
            $model->handleBeforeSave();
            $model->validate();
            if(!$model->hasErrors()){ 
                $model->save(); // has id gasreceipt
                Yii::app()->user->setFlash( MyFormat::SUCCESS_UPDATE, "Thêm mới thành công." );
                $this->redirect(array('view', 'id'=>$model->id));
            }
        }

        $this->render('create',array(
                'model'=>$model, 'actions' => $this->listActionsCanAccess,
        ));
       
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }
    
    public function actionView($id){
        $this->pageTitle = 'Xem số biên bản giao nhận';
        try{
            $model = $this->loadModel($id);            
            $this->render('view',array(
                    'model'=> $model, 'actions' => $this->listActionsCanAccess,
            ));
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }
    
    public function actionUpdate($id){
        $this->pageTitle = 'Cập nhật số biên bản giao nhận';
        try
        {   
            $model=$this->loadModel($id);
            $model->scenario = 'update_receipts';
            if( !$model->canUpdate() ){
                $this->redirect(['index']);
            }
            $model->create_for = ($model->role_id == ROLE_EMPLOYEE_MAINTAIN) ? GasReceipts::TYPE_FOR_EMPLOYEE : GasReceipts::TYPE_FOR_CAR;
            if( isset($_GET['type']) ){
                $model->create_for = isset($_GET['type']) ? $_GET['type'] : GasReceipts::TYPE_FOR_EMPLOYEE;
            }
            if(isset($_POST['GasReceipts']))
            {
                $model->attributes=$_POST['GasReceipts'];
                $model->handleBeforeSave();
                $model->validate();
                if(!$model->hasErrors()){
                    $model->update();
                    Yii::app()->user->setFlash( MyFormat::SUCCESS_UPDATE, "Cập nhật thành công." );
                    $this->redirect(array('view','id'=>$model->id));
                }
            }

            $this->render('update',array(
                'model'=>$model, 'actions' => $this->listActionsCanAccess,
            ));
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }
    
    public function actionDelete($id){
        try{ 
            if($model = $this->loadModel($id))
            {
                if($model->delete()){
                    Yii::log("Uid: " .Yii::app()->user->id. " Delete record ".  print_r($model->attributes, true), 'info');
                }
            }
            // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
            if(!isset($_GET['ajax']))
                $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('index'));
        
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }
    
    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer the ID of the model to be loaded
     */
    public function loadModel($id){
        try{
            $model= new GasReceipts;
            $model = $model->findByPk($id);
            if($model===null)
            {
                Yii::log("The requested page does not exist.");
                throw new CHttpException(404,'The requested page does not exist.');
            }			
            return $model;
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }
    
    /** @Author: TRONG NHAN date
     *  @Todo: todo
     *  @Param: param
     **/
    public function actionReportNumber() {
        $this->pageTitle = 'Báo cáo số biên bản giao nhận';
        try
        {           
            $model = new GasReceipts('search');
            $model->unsetAttributes(); // return value defalt to be null
            $model->date_from = date('01-m-Y');
            $model->date_to = date('d-m-Y');
            $model->pageSize = 50;
            if (isset($_GET['GasReceipts'])){
                $model->attributes = $_GET['GasReceipts'];
            }

            $this->render('receipts_report/index_report',array(
                'model'=>$model,'actions'=>$this->listActionsCanAccess,
            ));

            }
        catch (Exception $exc)
        {
            GasCheck::CatchAllExeptiong($exc);
        }
    }
    
    /** @Author: TRONG NHAN date
     *  @Todo: todo
     *  @Param: param
     **/
    public function actionViewReport($id) {
        $this->layout = 'ajax';
        try{
            $model = $this->loadModel($id);            
            $this->render('receipts_report/view_report',array(
                    'model'=> $model
            ));
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }

    
    /** @Author: NamLA Aug 7, 2019
     *  @Todo: todo
     *  @Param: param
     **/
    public function actionCheckExpired($id) {
         try{ 
            $model = $this->loadModel($id);
            if( $model->canSetExpired() )
            {
                $model->status = STATUS_INACTIVE;
                $model->update();
            } else {
                $this->redirect(array('index'));
            }
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }

}
