<?php

class UsersPriceController extends AdminController 
{
    public $pluralTitle = "Giá Khách Hàng";
    public $singleTitle = "Giá Khách Hàng";

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate()
    {
        $this->pageTitle = 'Tạo Mới ';
        try{
        $model=new UsersPrice('create');
        $model->c_month = date('m')*1;
        $model->c_year = date('Y');
        if(isset($_POST['UsersPrice']))
        {
            $model->attributes=$_POST['UsersPrice'];
            $model->validate();
            if(!$model->hasErrors()){
                $aCustomerDuplicate = $model->saveCreate();
                if(count($aCustomerDuplicate)){
                    $aModelUser     = Users::getArrayModelByArrayId($aCustomerDuplicate);
                    $sCustomerName  = CHtml::listData($aModelUser, 'id', 'first_name');
                    $sCustomerName  = implode('<br>', $sCustomerName);
                    Yii::app()->user->setFlash( MyFormat::ERROR_UPDATE, 'Thêm mới thành công.<br> Những khách hàng sau bị trùng và không được thêm vào<br>'.$sCustomerName );
                }else{
                    Yii::app()->user->setFlash( MyFormat::SUCCESS_UPDATE, 'Thêm mới thành công.' );
                }
                
               $this->redirect(array('create'));
            }
        }

        $this->render('create',array(
                'model'=>$model, 'actions' => $this->listActionsCanAccess,
        ));
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id)
    {
        $this->pageTitle = 'Cập Nhật Giá Khách Hàng';
        try{
        $model=new UsersPrice('create');
        $model->loadMultiUpdate($id);
        if(isset($_POST['customer_id']))
        {
            $model->saveMultiUpdate($this);
            Yii::app()->user->setFlash( MyFormat::ERROR_UPDATE, "Không có record nào được cập nhật" );
            $this->redirect(array('index'));
        }

        $this->render('update',array(
                'model'=>$model, 'actions' => $this->listActionsCanAccess,
        ));
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id)
    {
        try{
        if(Yii::app()->request->isPostRequest)
        {
            // we only allow deletion via POST request
            if($model = $this->loadModel($id))
            {
                if($model->delete())
                    Yii::log("Uid: " .Yii::app()->user->id. " Delete record ".  print_r($model->attributes, true), 'info');
            }

            // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
            if(!isset($_GET['ajax']))
                    $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('index'));
        }
        else
        {
            Yii::log("Uid: " .Yii::app()->user->id. " Invalid request. Please do not repeat this request again.");
            throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
        }	
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }

    /**
     * Manages all models.
     */
    public function actionIndex()
    {
        $this->pageTitle = 'Giá bò mối';
        try{
        $this->handleAboutCustomer();
        CacheSession::getPriceBoMoi();// init session
        $model=new UsersPrice('search');
        $model->initSessionPriceCodeByMonthYear(date('m')*1, date('Y'));
        $model->unsetAttributes();  // clear any default values
//        $model->c_month = 3;
        $cRole = MyFormat::getCurrentRoleId();
        if($cRole != ROLE_ADMIN){
            $model->c_year = date('Y');
            $model->c_month = date('m')*1;
        }
        if(isset($_GET['UsersPrice']))
            $model->attributes=$_GET['UsersPrice'];

        $this->render('index',array(
                'model'=>$model, 'actions' => $this->listActionsCanAccess,
        ));
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }
    
    /**
     * @Author: ANH DUNG Apr 05, 2017
     * @Todo: handle popup view info about customer
     */
    public function handleAboutCustomer() {
        if(isset($_GET['AboutCustomer'])){
            $this->layout = 'ajax';
            $this->render('Info/AboutCustomer',array(
                    'customer_id'=>$_GET['AboutCustomer'], 'actions' => $this->listActionsCanAccess,
            ));
            die;
        }
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
    * If the data model is not found, an HTTP exception will be raised.
     * @param integer the ID of the model to be loaded
     */
    public function loadModel($id)
    {
        try
        {
        $model=UsersPrice::model()->findByPk($id);
        if($model===null)
        {
            Yii::log("The requested page does not exist.");
            throw new CHttpException(404,'The requested page does not exist.');
        }			
        return $model;
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }
    
    /**
     * @Author: ANH DUNG Jun 02, 2016
     * @Todo: import excel customer price thang 03
     */
    public function actionImport() {
        $model=new UsersPrice();
        $data = array();
        if(isset($_POST['UsersPrice']))
        {
            $model->attributes=$_POST['UsersPrice'];
            $model->file_excel=$_FILES['UsersPrice'];
            $model->validate(array('file_excel'));
            if(!$model->hasErrors()){
                UsersPrice::importExcel($model, $data);
                if(!isset($data['errors'])){
                    Yii::app()->user->setFlash( MyFormat::SUCCESS_UPDATE, "Import thành công: ".$data['count_insert'] );
                    $this->redirect(array('import'));
                }
            }
        }
        $this->render('ImportExcel',array(
            'model'=>$model, 'data'=>$data, 'actions' => $this->listActionsCanAccess,
        ));
    }
    
    /**
     * @Author: ANH DUNG Jun 05, 2016
     * @Todo: setup giá hộ gd
     * admin/usersPrice/setupGas12
     */
    public function actionSetupGas12()
    {
        $this->pageTitle = 'Thiết lập giá gas hộ gia đình ';
        try
        {
        $model=new UsersPrice();
        $model->unsetAttributes();  // clear any default values
        $model->c_year = date("Y");
        if(isset($_GET['UsersPrice']))
            $model->attributes=$_GET['UsersPrice'];
        if(isset($_POST['price']))
        {
            $model->saveByMonthYear();
            $mAppCache = new AppCache();
            $mAppCache->setPriceHgd(AppCache::PRICE_HGD);// add Now 06, 2016
            Yii::app()->user->setFlash( MyFormat::SUCCESS_UPDATE, "Cập nhật thành công." );
            $link = Yii::app()->createAbsoluteUrl("admin/usersPrice/SetupGas12", array("UsersPrice[c_month]"=>$model->c_month, "UsersPrice[c_year]"=>$model->c_year));
            $this->redirect($link);
        }

        $this->render('SetupGas12/SetupGas12',array(
            'model'=>$model, 'actions' => $this->listActionsCanAccess,
        ));
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }
    
    public function actionExportExcel(){
        try{
        if(isset($_SESSION['data-excel'])){
//            ini_set('memory_limit','1000M'); chưa cần chạy đoạn này, đưa memory_limit lên 500 có thể export dc khoảng 5000 row - chưa test
            ini_set('memory_limit','500M');
            ToExcel::CustomerPrice();
        }
        $this->redirect(array('index'));
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }
    /** @Author: Pham Thanh Nghia Nov 16, 2018
     *  @Todo: Báo cáo quản trị khách hàng 
     *  admin/customerSpecial/reportManage
     **/
    public function actionReportManage(){
//        error_reporting(1); // var_dump(PHP_INT_SIZE);
        $this->pageTitle = 'Báo cáo quản trị đầu tư khách hàng';
        
        try{
            $sta2 = new Sta2();
            $model = new UsersPrice();
            $this->exportExcelReportManage($model); // Yii::app()->session['dataManageCustomer']
            $aData = array();
//            $model->time = '1-03-2018'; // test
            $model->time = date('d-m-Y'); 
            if(isset($_GET['UsersPrice'])){
                $model->attributes = $_GET['UsersPrice'];

                $aData = $sta2->getReportManageCustomer($model);
            }
            
            $this->render('reportManage/index',array( 
                'aData'=>$aData,
                'model'=>$model,
                'actions' => $this->listActionsCanAccess,
            ));
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }
    /** @Author: Pham Thanh Nghia Dec 21, 2018
     *  @Todo: admin/usersPrice/reportManage
     **/
    public function exportExcelReportManage($model){
        try{
            if(!$model->canExportExcelReportManage() || !isset($_GET['toExcel'])){
                return ;
            }
            if(isset($_GET['toExcel']) && isset(Yii::app()->session['dataManageCustomer'])){
                ini_set('memory_limit','2000M');// chưa cần chạy đoạn này, đưa memory_limit lên 500 có thể export dc khoảng 5000 row - chưa test
                $mToExcel1 = new ToExcel1();
                $mToExcel1->exportReportManage($model);
            }
            $this->redirect(array('reportManage'));
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }
    
    /** @Author: NhanDT Sep 3, 2019
     *  @Todo: todo
     *  @Param: param
     **/
    public function actionIndexAlcohol() {
        $this->pageTitle = 'Giá Cồn';
        try{
        $model = new UsersPrice('search');
        $model->unsetAttributes();
        $cRole = MyFormat::getCurrentRoleId();
        if($cRole != ROLE_ADMIN){
            $model->c_year = date('Y');
            $model->c_month = date('m')*1;
        }
        if(isset($_GET['UsersPrice']))
            $model->attributes=$_GET['UsersPrice'];

        $this->render('Alcohol/index',array(
                'model'=>$model, 'actions' => $this->listActionsCanAccess,
        ));
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }
    
    /** @Author: NhanDT Sep 3, 2019
     *  @Todo: todo
     *  @Param: param
     **/
    public function actionCreateAlcohol() {
        $this->pageTitle = 'Tạo Mới Giá Cồn';
        try{
        $model=new UsersPrice('create');
        $model->c_month = date('m')*1;
        $model->c_year = date('Y');
        if(isset($_POST['UsersPrice']))
        {
            $model->attributes=$_POST['UsersPrice'];
            $model->validate();
            if(!$model->hasErrors()){
                $aCustomerDuplicate = $model->saveCreateAlcohol();
                if(count($aCustomerDuplicate)){
                    $aModelUser     = Users::getArrayModelByArrayId($aCustomerDuplicate);
                    $sCustomerName  = CHtml::listData($aModelUser, 'id', 'first_name');
                    $sCustomerName  = implode('<br>', $sCustomerName);
                    Yii::app()->user->setFlash( MyFormat::ERROR_UPDATE, 'Thêm mới thành công.<br> Những khách hàng sau bị trùng và không được thêm vào<br>'.$sCustomerName );
                }else{
                    Yii::app()->user->setFlash( MyFormat::SUCCESS_UPDATE, 'Thêm mới thành công.' );
                }
                
               $this->redirect(array('createAlcohol'));
            }
        }
        $this->render('Alcohol/create',array(
                'model'=>$model, 'actions' => $this->listActionsCanAccess,
        ));
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }

    /** @Author: NhanDT Sep 3, 2019
     *  @Todo: todo
     *  @Param: param
     **/
    public function actionUpdateAlcohol($id) {
        $this->pageTitle = 'Cập Nhật Giá Cồn';
        try{
        $model=new UsersPrice('create');
        $model->loadMultiUpdate($id);
        if(isset($_POST['customer_id']))
        {
            if ($model->saveUpdateAlcohol()){
                 Yii::app()->user->setFlash( MyFormat::SUCCESS_UPDATE, "Cập nhật thành công." );
            }
            else {
                Yii::app()->user->setFlash( MyFormat::ERROR_UPDATE, "Không có record nào được cập nhật" );
            }
                $this->redirect(array('updateAlcohol', 'id'=>$id));
        }
        $this->render('Alcohol/update',array(
                'model'=>$model, 'actions' => $this->listActionsCanAccess,
        ));
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }
    
    
}
