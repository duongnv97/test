<?php

class SetupTeamController extends AdminController 
{
    public $pluralTitle = 'Setup đại lý KTKV';
    public $singleTitle = 'Setup đại lý KTKV';
    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id)
    {
        $this->pageTitle = 'Xem ';
        try{
        $this->render('view',array(
                'model'=>$this->loadModel($id), 'actions' => $this->listActionsCanAccess,
        ));
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate()
    {
        $this->pageTitle = 'Tạo Mới ';
        try{
            $model = new SetupTeam('create');
            $model->initDataCreate();
            // get attributes from url after redirect
            if(isset($_GET['SetupTeam'])){
                $model->attributes = $_GET['SetupTeam'];
            }
            // search
            if(isset($_POST['SetupTeam']))
            {
                $model->attributes = $_POST['SetupTeam'];
            }
            $mOld = $model->getExistsTeam(); // search data
            if($mOld){
                $this->redirect(array('update','id'=>$mOld->id));
            }
            // save
            if(isset($_POST['save']))
            {
                $model->validate();
                if(!$model->hasErrors()){
                    $model->formatDataBeforeSave();
                    $model->created_by = MyFormat::getCurrentUid();
                    $model->save();
                    $model->setSomeCache();
                    Yii::app()->user->setFlash( MyFormat::SUCCESS_UPDATE, "Thêm mới thành công." );
                    $this->redirect(array('create'));
                }
            }
            $this->render('create',array(
                    'model'=>$model, 
                    'actions' => $this->listActionsCanAccess,
            ));
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id)
    {
        $this->pageTitle = 'Cập Nhật ';
        try{
        $model=$this->loadModel($id);
        if (!$model->canUpdate()) {
            $this->redirect(array('index'));
        }
        $model->formatDataBeforeUpdate();
        $model->scenario = 'update';
        if(isset($_POST['SetupTeam']))
        {
            $model->attributes=$_POST['SetupTeam'];
            $model->validate();
            if(!$model->hasErrors()){
                $model->formatDataBeforeSave();
                $model->save();
                $model->setSomeCache();
                Yii::app()->user->setFlash( MyFormat::SUCCESS_UPDATE, "Cập nhật thành công." );
                $this->redirect(array('update','id'=>$model->id));
            }							
        }

        $this->render('update',array(
                'model'=>$model, 'actions' => $this->listActionsCanAccess,
        ));
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id)
    {
        try{
        if(Yii::app()->request->isPostRequest){
            // we only allow deletion via POST request
            if($model = $this->loadModel($id))
            {
                if($model->delete())
                    Yii::log("Uid: " .Yii::app()->user->id. " Delete record ".  print_r($model->attributes, true), 'info');
            }

            // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
            if(!isset($_GET['ajax']))
                    $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('index'));
        }
        else
        {
            Yii::log("Uid: " .Yii::app()->user->id. " Invalid request. Please do not repeat this request again.");
            throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
        }	
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }

    /**
     * Manages all models.
     */
    public function actionIndex()
    {
        $this->pageTitle = 'Setup đại lý KTKV';
        try{
        $model=new SetupTeam('search');
        $model->unsetAttributes();  // clear any default values
        $cRole = MyFormat::getCurrentRoleId();
        if($cRole != ROLE_ADMIN){
            $model->type = SetupTeam::TYPE_KTKV_AGENT;
        }
        if(isset($_GET['SetupTeam']))
            $model->attributes=$_GET['SetupTeam'];

        $this->render('index',array(
                'model'=>$model, 'actions' => $this->listActionsCanAccess,
        ));
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
    * If the data model is not found, an HTTP exception will be raised.
     * @param integer the ID of the model to be loaded
     */
    public function loadModel($id)
    {
        try{
        $model=SetupTeam::model()->findByPk($id);
        if($model===null)
        {
            Yii::log("The requested page does not exist.");
            throw new CHttpException(404,'The requested page does not exist.');
        }			
        return $model;
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }

}
