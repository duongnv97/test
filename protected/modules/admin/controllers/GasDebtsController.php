<?php

class GasDebtsController extends AdminController {

    public $pluralTitle = 'Quản lý Công nợ';
    public $singleTitle = 'Quản lý Công nợ';

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id) {
        $this->pageTitle = 'Xem ';
        try {
            $this->render('view', array(
                'model' => $this->loadModel($id), 'actions' => $this->listActionsCanAccess,
            ));
        } catch (Exception $exc) {
            GasCheck::CatchAllExeptiong($exc);
        }
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate() {
        $this->pageTitle = 'Tạo Mới ';
        try {
            $model = new GasDebts('create');
            $model->monthCount = 1;
            if (isset($_POST['GasDebts']['user_id'])) {
                $model->handleCreateMulti();
                Yii::app()->user->setFlash(MyFormat::SUCCESS_UPDATE, "Thêm mới thành công.");
                $this->redirect(array('create'));
            }

            $this->render('create_multi', array(
                'model' => $model, 'actions' => $this->listActionsCanAccess,
            ));
        } catch (Exception $exc) {
            GasCheck::CatchAllExeptiong($exc);
        }
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id) {
        $this->pageTitle = 'Cập Nhật ';
        try {
            $model = $this->loadModel($id);
            if(!$model->canUpdate()){// add Now 07, 2016
                Yii::app()->user->setFlash( MyFormat::ERROR_UPDATE, "Yêu cầu không hợp lệ" );
                $this->redirect(array('index'));
            }
            $model->scenario = 'update';
            $model->formatDataBeforeUpdate();
            if (isset($_POST['GasDebts'])) {
                $model->attributes = $_POST['GasDebts'];
                $model->validate();
                if (!$model->hasErrors()) {
                    $model->formatDataBeforeSave();
                    $model->save();
                    Yii::app()->user->setFlash(MyFormat::SUCCESS_UPDATE, "Cập nhật thành công.");
                    $this->redirect(array('update', 'id' => $model->id));
                }
            }

            $this->render('update', array(
                'model' => $model, 'actions' => $this->listActionsCanAccess,
            ));
        } catch (Exception $exc) {
            GasCheck::CatchAllExeptiong($exc);
        }
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id) {
        try {
            if (Yii::app()->request->isPostRequest) {
                // we only allow deletion via POST request
                if ($model = $this->loadModel($id)) {
                    if ($model->delete())
                        Yii::log("Uid: " . Yii::app()->user->id . " Delete record " . print_r($model->attributes, true), 'info');
                }

                // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
                if (!isset($_GET['ajax']))
                    $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('index'));
            }
            else {
                Yii::log("Uid: " . Yii::app()->user->id . " Invalid request. Please do not repeat this request again.");
                throw new CHttpException(400, 'Invalid request. Please do not repeat this request again.');
            }
        } catch (Exception $exc) {
            GasCheck::CatchAllExeptiong($exc);
        }
    }

    /**
     * Manages all models.
     */
    public function actionIndex() {
        $this->pageTitle = 'Công nợ';
        try {
            $this->exportExcel();
            $model = new GasDebts('search');
            $model->unsetAttributes();  // clear any default values
            if (isset($_GET['GasDebts']))
                $model->attributes = $_GET['GasDebts'];

            $this->render('index', array(
                'model' => $model, 'actions' => $this->listActionsCanAccess,
            ));
        } catch (Exception $exc) {
            GasCheck::CatchAllExeptiong($exc);
        }
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer the ID of the model to be loaded
     */
    public function loadModel($id) {
        try {
            $model = GasDebts::model()->findByPk($id);
            if ($model === null) {
                Yii::log("The requested page does not exist.");
                throw new CHttpException(404, 'The requested page does not exist.');
            }
            return $model;
        } catch (Exception $exc) {
            GasCheck::CatchAllExeptiong($exc);
        }
    }

    /** @Author: DuongNV 10 Jun,2018
     *  @Todo: export excel debit
     **/
    public function exportExcel() {
        try{
            if(isset($_GET['toExcel']) && isset($_SESSION['data-excel-debts'])){
                ini_set('memory_limit','1500M');// chưa cần chạy đoạn này, đưa memory_limit lên 500 có thể export dc khoảng 5000 row - chưa test
                ToExcelList::listGasDebts();
            }
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }
    
    /** @Author: DuongNV Jul1119
     *  @Todo: bc cong no nhan vien (tab tong quat)
     *  Chia ra 2 action để dễ phân quyền trên giao diện
     **/
    public function actionReportDebtsGeneral() {
        $this->pageTitle = 'Báo cáo công nợ nhân viên';
        try {
            $model = new GasDebtsReport();
            $aData = [];
            $model->unsetAttributes();  // clear any default values
            $model->date_from           = date('01-m-Y');
            $model->date_to             = date('t-m-Y');
            if( isset($_POST['GasDebtsReport']['cut_salary']) ){
                $aCutSalaryData = $_POST['GasDebtsReport']['cut_salary'];
                $model->handleCutSalary($aCutSalaryData);
            }
            if( isset($_GET['GasDebtsReport']) ){
                $params                 = $_GET['GasDebtsReport'];
                $model->attributes      = $params;
                $model->province_id     = isset($params['province_id']) ? $params['province_id'] : [];
                $model->position_work   = isset($params['position_work']) ? $params['position_work'] : [];
                $model->date_from       = isset($params['date_from']) ? $params['date_from'] : '';
                $model->date_to         = isset($params['date_to']) ? $params['date_to'] : '';
                $aData = $model->getReportDebts();
            }

            $this->render('report/report_debts', array(
                'model' => $model, 
                'aData' => $aData,
                'actions' => $this->listActionsCanAccess,
            ));
        } catch (Exception $exc) {
            GasCheck::CatchAllExeptiong($exc);
        }
    }
    
    /** @Author: DuongNV Jul1119
     *  @Todo: bc cong no nhan vien (tab chi tiet)
     *  Chia ra 2 action để dễ phân quyền trên giao diện
     **/
    public function actionReportDebtsDetail() {
        $this->pageTitle = 'CÔNG NỢ CHI TIẾT CỦA MỘT CÁN BỘ NHÂN VIÊN';
        try {
            $model = new GasDebtsReport();
            $aData = [];
            $model->unsetAttributes();  // clear any default values
            $model->date_from           = date('01-m-Y');
            $model->date_to             = date('t-m-Y');
            if( isset($_GET['GasDebtsReport']) ){
                $params                 = $_GET['GasDebtsReport'];
                $model->attributes      = $params;
                $model->date_from       = isset($params['date_from']) ? $params['date_from'] : '';
                $model->date_to         = isset($params['date_to']) ? $params['date_to'] : '';
                $aData = $model->getReportDebtsDetail();
            }

            $this->render('report/report_debts', array(
                'model' => $model, 
                'aData' => $aData,
                'actions' => $this->listActionsCanAccess,
            ));
        } catch (Exception $exc) {
            GasCheck::CatchAllExeptiong($exc);
        }
    }
    
    /** @Author: DuongNV Jul1119
     *  @Todo: is general tab, ReportDebts, viet function vi su dung nhieu cho
     **/
    public function isGeneralTab() {
        $action = $this->action->id;
        return strcasecmp($action, 'reportDebtsGeneral') == 0; // compare case-insensitive, return 0 if equal
    }
}
