<?php

class ReportCustomerController extends AdminController
{
    public $pluralTitle = 'Báo cáo khách hàng giao khó';
    public $singleTitle = 'Báo cáo khách hàng giao khó';

    /** @Author: DuongNV Dec 12, 2018
     *  @Todo: Báo cáo khách hàng giao khó
     **/
    public function actionReportHardDelivery()
    {
        try {
            $this->exportExcelHardDelivery();
            $this->pageTitle  = 'Báo cáo khách hàng giao khó';
            $data             = [];
            $model            = new GasStoreCard();
            $model->date_from = date('01-m-Y');
            $model->date_to   = date('d-m-Y');

            if (isset($_GET['GasStoreCard'])) {
                $model->attributes = $_GET['GasStoreCard'];
                $sta3              = new Sta3();
                $data              = $sta3->reportHardDelivery($model);
            }

            $this->render('report_hard_delivery', array(
                'model'   => $model, 
                'actions' => $this->listActionsCanAccess,
                'data'    => $data,
            ));

        } catch (Exception $exc) {
            GasCheck::CatchAllExeptiong($exc);
        }
    }
    
    /** @Author: DUONG 27/05/2019
     *  @Todo: export to excel giao kho
     **/
    public function exportExcelHardDelivery(){
        if( isset($_SESSION['data-excel-hard']) && isset($_GET['excel']) ){
            ini_set('memory_limit','1500M');// chưa cần chạy đoạn này, đưa memory_limit lên 500 có thể export dc khoảng 5000 row - chưa test
            ToExcel::summaryHardDelivery();
            $this->redirect(array('ReportHardDelivery'));
        }
    }

    /** @Author: DuongNV Jan 17,2019
     *  @Todo: List các gas_code có date_delivery = created_date
     **/
    public function actionCodePttt() {
        try {
            $this->excelCodePttt();
            $this->pageTitle  = 'Báo cáo thời gian nhập code PTTT + BQV';
            $model            = new SpjCode();
            $model->date_from = date('01-m-Y');
            $model->date_to   = date('d-m-Y');
            $aData            = [];

            if (isset($_GET['SpjCode'])) {
                $model->attributes = $_GET['SpjCode'];
                $aData             = $model->getReportCodePttt();
            }

            $this->render('code_pttt', array(
                'model'   => $model, 
                'aData'   => $aData,
                'actions' => $this->listActionsCanAccess,
            ));

        } catch (Exception $exc) {
            GasCheck::CatchAllExeptiong($exc);
        }
    }
    
    public function excelCodePttt(){
        try{
            if(isset($_SESSION['data-excel-pttt']) && isset($_GET['excel'])){
                ini_set('memory_limit','2500M');// chưa cần chạy đoạn này, đưa memory_limit lên 500 có thể export dc khoảng 5000 row - chưa test
                $mToExcel = new ToExcel();
                $mToExcel->excelCodePttt();
                $this->redirect(array('codePttt'));
            }
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }
    
    /** @Author: LOCNV May 14, 2019
     *  @Todo: Báo cáo sản lượng khách hàng
     *  @Param:
     * LOC001
     **/
    public function actionReportProduction() {
        try {
            $this->pageTitle  = 'Báo cáo phân loại sản lượng khách hàng';
            $model = new GasStoreCard();
            $model->unsetAttributes();
            $model->channel_id = Users::CON_LAY_HANG;
            $model->date_from = date('01-m-Y');
            $model->date_to   = date('d-m-Y');
            $data = [];
            
            if (isset($_GET['GasStoreCard'])) {
                $model->attributes = $_GET['GasStoreCard'];
                $model->qtykg_from = $_GET['GasStoreCard']['qtykg_from'];
                $model->qtykg_to   = $_GET['GasStoreCard']['qtykg_to'];
                $model->channel_id   = $_GET['GasStoreCard']['channel_id'];
                $careCus = new CareCustomer();
                $data = $careCus->reportCustomerProduction($model);
            }
            
            $this->render("report_production", array(
                'model' => $model,
                'actions'=> $this->listActionsCanAccess,
                'data' => $data,
                ));

        } catch (Exception $exc) {
            GasCheck::CatchAllExeptiong($exc);
        }
    }
}