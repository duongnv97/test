<?php

class HrApprovedReportsController extends AdminController 
{
    public $pluralTitle = 'Duyệt báo cáo lương';
    public $singleTitle = 'Duyệt báo cáo lương';
    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id)
    {
        $this->pageTitle = 'Xem ';
        try{
            $type       = isset($_GET['type']) ? $_GET['type'] : HrApprovedReports::TYPE_DETAIL;
            $model      = new HrApprovedReports();
            
            if( $type == HrApprovedReports::TYPE_DETAIL ){ // Bảng lương chi tiết
                $model  = $this->loadModel($id);
            } else { // Bảng lương tổng hợp
                $mReport        = HrSalaryReports::model()->findByPk($id);
                $model->code_no = $mReport->code_no;
            }
            $mSalary    = new HrSalaryReports();
            $modelReview= '';
            if( $model->isTreasurer() ){ // Nếu là thủ quỹ sẽ có giao diện riêng
                $modelReview = $mSalary->getDataForTreasurer($model->code_no);
            } else {
                $modelReview = $mSalary->getDataForReview($model->code_no);
            }
            $this->render('view',array(
                'model'=>$model, 
                'modelReview'=>$modelReview, 
                'actions' => $this->listActionsCanAccess,
            ));
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }

    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
//    public function actionCreate()
//    {
//        $this->pageTitle = 'Tạo Mới ';
//        try{
//        $model=new HrApprovedReports('create');
//
//        if(isset($_POST['HrApprovedReports']))
//        {
//            $model->attributes=$_POST['HrApprovedReports'];
//            $model->validate();
//            if(!$model->hasErrors()){
//                $model->save();
//                Yii::app()->user->setFlash( MyFormat::SUCCESS_UPDATE, "Thêm mới thành công." );
//                $this->redirect(array('create'));
//            }				
//        }
//
//        $this->render('create',array(
//                'model'=>$model, 'actions' => $this->listActionsCanAccess,
//        ));
//        }catch (Exception $exc){
//            GasCheck::CatchAllExeptiong($exc);
//        }
//    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id)
    {
        $this->redirect(array('index')); // Tạm thời ko cho cập nhật
        $this->pageTitle = 'Cập Nhật ';
        try{
            $model=$this->loadModel($id);
            if(!$model->canApprove()){
                $this->redirect(array('index'));
            }
            $model->scenario = 'update';
            $mSalary    = new HrSalaryReports();
            $data       = $mSalary->getDataForReview($model->code_no);
            if(isset($_POST['HrApprovedReports']))
            {
                $model->attributes=$_POST['HrApprovedReports'];
                $model->validate();
                if(!$model->hasErrors()){
                    if( isset($_POST['approve']) )    $model->approveReport(); // Duyệt bảng lương
                    if( isset($_POST['requestUpdate']) )  $model->cancelReport(); // Yêu cầu cập nhật bảng lương
                    $model->save();
                    Yii::app()->user->setFlash( MyFormat::SUCCESS_UPDATE, "Cập nhật thành công." );
                    $this->redirect(array('update','id'=>$model->id));
                }							
            }

            $this->render('update',array(
                'model'=>$model, 
                'actions' => $this->listActionsCanAccess,
                'data' => $data
            ));
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id)
    {
        try{
        if(Yii::app()->request->isPostRequest){
            // we only allow deletion via POST request
            if($model = $this->loadModel($id))
            {
                if($model->delete())
                    Yii::log("Uid: " .Yii::app()->user->id. " Delete record ".  print_r($model->attributes, true), 'info');
            }

            // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
            if(!isset($_GET['ajax']))
                    $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('index'));
        }
        else
        {
            Yii::log("Uid: " .Yii::app()->user->id. " Invalid request. Please do not repeat this request again.");
            throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
        }	
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }

    /**
     * Manages all models.
     */
    public function actionIndex()
    {
        $this->pageTitle = 'Duyệt lương';
        try{
        $model=new HrApprovedReports('search');
        $model->unsetAttributes();  // clear any default values
        if(isset($_GET['HrApprovedReports']))
                $model->attributes=$_GET['HrApprovedReports'];

        $this->render('index_button',array(
                'model'=>$model, 'actions' => $this->listActionsCanAccess,
        ));
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }
    
    /**
     * dành cho người duyệt
     */
    public function actionApprove($id)
    {
        $this->pageTitle = 'Duyệt ';
        try{
            $model=$this->loadModel($id);
            if(!$model->canApprove()){
                $this->redirect(array('index'));
            }
            $model->scenario = 'update';
            $mSalary    = new HrSalaryReports();
            $data       = $mSalary->getDataForReview($model->code_no);
            if(isset($_POST['HrApprovedReports']))
            {
                $model->attributes=$_POST['HrApprovedReports'];
                $model->validate();
                if(!$model->hasErrors()){
                    $message = '';
                    if( isset($_POST['approve']) ){
                        $model->approveReport(); // Duyệt bảng lương
                        $message = 'Duyệt thành công!';
                    }
                    if( isset($_POST['requestUpdate']) ){
                        $model->cancelReport(); // Yêu cầu cập nhật bảng lương
                        $message = 'Đã yêu cầu cập nhật!';
                    }
                    $model->save();
                    Yii::app()->user->setFlash( MyFormat::SUCCESS_UPDATE, $message );
                    $this->redirect(array('approve','id'=>$model->id));
                }							
            }

            $this->render('approve',array(
                'model'=>$model, 
                'actions' => $this->listActionsCanAccess,
                'data' => $data
            ));
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
    * If the data model is not found, an HTTP exception will be raised.
     * @param integer the ID of the model to be loaded
     */
    public function loadModel($id)
    {
        try{
        $model=HrApprovedReports::model()->findByPk($id);
        if($model===null)
        {
            Yii::log("The requested page does not exist.");
            throw new CHttpException(404,'The requested page does not exist.');
        }			
        return $model;
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }
    
    /** @Author: DuongNV May 6,19
     *  @Todo: export excel
     **/
    public function actionExportExcel() {
        try{
            if(isset($_SESSION['data-excel-approve'])){
                ini_set('memory_limit','1500M');
                $mApprove   = new HrApprovedReports();
                if( $mApprove->isTreasurer() ){ // Nếu là thủ quỹ sẽ có giao diện riêng
                    ToExcel::summaryApproveReportsForTreasurer();
                } else {
                    if( isset($_GET['type']) && $_GET['type'] == HrApprovedReports::TYPE_DETAIL ){
                        ToExcel::summarySalaryReports();
                    } else {
                        ToExcel::summaryApproveReports();
                    }
                }
            }
            $this->redirect(array('index'));
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }

}
