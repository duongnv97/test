<?php

class GasmaterialsController extends AdminController
{
     /*    public function accessRules()
    {
        return array();
    } */
    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id)
    {
        $this->pageTitle = 'Xem Vật Tư';
        try{
            $this->render('view',array(
                    'model'=>$this->loadModel($id), 'actions' => $this->listActionsCanAccess,
            ));
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }

    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate()
    {
        $this->pageTitle = 'Tạo Mới Vật Tư';
        try{
            $model=new GasMaterials('create');
            //$model->materials_no = MyFunctionCustom::getNextId('GasMaterials', 'VT', 6, 'materials_no');                
            // Uncomment the following line if AJAX validation is needed
            // $this->performAjaxValidation($model);
//                $model->parent_id = 294;
//                $model->materials_type_id = 15;
            if(isset($_POST['GasMaterials']))
            {
                $model->attributes=$_POST['GasMaterials'];
                if($model->save()){
                    $this->setCache($model);
                    Yii::app()->user->setFlash('successUpdate', "Thêm mới thành công.");
                    $this->redirect(array('create'));
                }
            }

            $this->render('create',array(
                    'model'=>$model, 'actions' => $this->listActionsCanAccess,
            ));
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }

    /** @Author: ANH DUNG Jan 08, 2018
     *  @Todo: set some cache file
     * Đơn hàng hộ gia đình	Vỏ về hoặc phụ kiện	"material_vo, material_hgd"
        Đơn hàng bò mối	Nhập gas	"material_gas"
                        Nhập vỏ         "material_vo"
        Thẻ kho	Tìm kiếm vật tư	Query site/getDataCache >> "list_all_material"
        List agent	Trong tạo KH của ccs	"list_agent " trong login -> getSomeConfig
     * 
     **/
    public function setCache($model) {
        $mAppCache = new AppCache();
        $mAppCache->setListdataGasVo(AppCache::LISTDATA_MATERIAL_GAS_VO);
        $mAppCache->setMasterModel('GasMaterials', AppCache::ARR_MODEL_MATERIAL);
        $mAppCache->setObjMaterialByType(GasSupportCustomerRequest::getMaterialsTypeAutocomplete(), AppCache::ARR_MODEL_MATERIAL_SUPPORT, ['GetActive'=>1]);
        $mAppCache->setObjMaterialByType(GasMaterialsType::$ARR_WINDOW_VO, AppCache::ARR_MODEL_MATERIAL_VO, ['GetActive'=>1]);
        // June2919 AppCache::ARR_MODEL_MATERIAL_GAS là biến dùng cho loại gas mới, nhập trong đơn hàng bò mối
        $mAppCache->setObjMaterialByType(GasMaterialsType::getTypeBoMoi(), AppCache::ARR_MODEL_MATERIAL_GAS, ['GetActive'=>1]);
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id)
    {
        try{
            $model=$this->loadModel($id);
            $this->pageTitle = $model->name;
            if(isset($_POST['GasMaterials']))
            {
                $model->attributes=$_POST['GasMaterials'];
                if($model->save()){
                    $this->setCache($model);
                    Yii::app()->user->setFlash('successUpdate', "Cập nhật thành công.");
                    $this->redirect(array('view','id'=>$model->id));
                }
            }

            $this->render('update',array(
                    'model'=>$model, 'actions' => $this->listActionsCanAccess,
            ));
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id)
    {
        try{
            if(Yii::app()->request->isPostRequest)
            {
                throw new CHttpException(400,'Chức năng xóa bị tắt bởi admin.');
                    // we only allow deletion via POST request
                    if($model = $this->loadModel($id))
                    {
                        if($model->delete())
                            Yii::log("Delete record ".  print_r($model->attributes, true), 'info');
                    }

                    // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
                    if(!isset($_GET['ajax']))
                            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('index'));
            }
            else
            {
                Yii::log("Invalid request. Please do not repeat this request again.");
                throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
            }	
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }

    /**
     * Manages all models.
     */
    public function actionIndex()
    {
        $this->pageTitle = 'Danh Sách Vật Tư';
        try{
            $this->toExcelList();
            $model=new GasMaterials('search');
            $model->unsetAttributes();  // clear any default values
            $model->status = STATUS_ACTIVE;
            if(isset($_GET['GasMaterials']))
                $model->attributes=$_GET['GasMaterials'];

            $this->render('index',array(
                    'model'=>$model, 'actions' => $this->listActionsCanAccess,
            ));
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer the ID of the model to be loaded
     */
    public function loadModel($id)
    {
        try{
            $model=GasMaterials::model()->findByPk($id);
            if($model===null)
            {
                Yii::log("The requested page does not exist.");
                throw new CHttpException(404,'The requested page does not exist.');
            }			
            return $model;
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }

    /**
     * Handle the ajax request. This process changes the status of member to 1 (mean active)
     * @param type $id the id of member need changed status to 1
     */
    public function actionAjaxActivate($id) {
        if(Yii::app()->request->isPostRequest)
        {
            $model = $this->loadModel($id);
            if(method_exists($model, 'activate'))
            {
                $model->activate();
            }
            Yii::app()->end();
        }
        else
        {
            Yii::log('Invalid request. Please do not repeat this request again.');
            throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
        }
            
    }

    /**
     * Handle the ajax request. This process changes the status of member to 0 (mean deactive)
     * @param type $id the id of member need changed status to 0
     */
    public function actionAjaxDeactivate($id) {
        if(Yii::app()->request->isPostRequest)
        {
            $model = $this->loadModel($id);
            if(method_exists($model, 'deactivate'))
            {
                $model->deactivate();
            }
            Yii::app()->end();
        }
        else
        {
            Yii::log('The requested page does not exist.');
            throw new CHttpException(404,'The requested page does not exist.');
        }
            
    }
    
     /** @Author: KHANH TOAN Nov 26 2018
     *  @Todo: xuat excel index
     *  @Param:
     **/
    public function toExcelList() {
        try{
             if(isset($_GET['ExportExcel']) && isset($_SESSION['data-excel-materials'])){
                ini_set('memory_limit','1500M');
//                echo '<pre>';
//                print_r("die");
//                echo '</pre>';
//                die;

                ToExcelList::listMaterial();
        }
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }
    
}
