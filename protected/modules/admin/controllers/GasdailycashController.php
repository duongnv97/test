<?php

class GasdailycashController extends AdminController
{

   /*  public function accessRules()
    {
        return array();
    }
     */
    public function actionReport(){
        $this->render('report',array(
//                'model'=>$model,
            'actions' => $this->listActionsCanAccess,
        ));         
    }
    
    public function actionImport_dailycash() {
        $model=new GasDailyCash('import_dailycash');
        if(isset($_POST['GasDailyCash']))
        {
            $model->attributes=$_POST['GasDailyCash'];
            $model->file_excel=$_FILES['GasDailyCash'];
            $model->validate();
            if(!$model->hasErrors()){
                $this->importExcel($model);
                $this->redirect(array('index','id'=>$model->id));
            }
                    
        }        
        $this->render('import_dailycash',array(
                'model'=>$model, 'actions' => $this->listActionsCanAccess,
        ));        
    }
    
    
    
    
    public function actionExport_dailycash() {
        set_time_limit(7200);               
        $model=new GasDailyCash();
        $model->unsetAttributes();  // clear any default values
        if(isset($_POST['GasDailyCash'])){
            $model->attributes=$_POST['GasDailyCash'];
            $model->searchExport();	
        }
		
        $this->render('export_dailycash',array(
                'model'=>$model, 'actions' => $this->listActionsCanAccess,
        ));        
    }            
    
    public function actionExport_dailyplan() {
        set_time_limit(7200);               
        $model=new GasDailyCash();
        $model->unsetAttributes();  // clear any default values
        if(isset($_POST['GasDailyCash'])){
            $model->attributes=$_POST['GasDailyCash'];
            $model->exportDailyPlan();	
        }
		
        $this->render('export_dailyplan',array(
                'model'=>$model, 'actions' => $this->listActionsCanAccess,
        ));        
    }        
    
    public function actionExport_target() {
        set_time_limit(7200);               
        $model=new GasDailyCash();
        $model->unsetAttributes();  // clear any default values
        if(isset($_POST['GasDailyCash'])){
            $model->attributes=$_POST['GasDailyCash'];
            GasDailyCash::exportTarget($model->sell_date_from);	
        }
		
        $this->render('export_target',array(
                'model'=>$model, 'actions' => $this->listActionsCanAccess,
        ));        
    }        
    
    
    public function actionExport_account_receivables() {
		// báo cáo công nợ phải thu khách hàng
        set_time_limit(7200);               
        $model=new GasDailyCash();
        $model->unsetAttributes();  // clear any default values
        if(isset($_POST['GasDailyCash'])){
            $model->attributes=$_POST['GasDailyCash'];
            GasDailyCash::exportAccountReceivables($model->sell_date_from,$model->sell_date_to,$model->sale_id);	
        }
		
        $this->render('export_account_receivables',array(
                'model'=>$model, 'actions' => $this->listActionsCanAccess,
        ));        
    }        
    
    public function actionExport_account_receivables_expired() {
		// báo cáo công nợ quá hạn khách hàng 
        set_time_limit(7200);               
        $model=new GasDailyCash();
        $model->unsetAttributes();  // clear any default values
        if(isset($_POST['GasDailyCash'])){
            $model->attributes=$_POST['GasDailyCash'];
            GasDailyCash::exportAccountReceivablesExpired($model->sell_date_to,$model->sale_id);	
        }
		
        $this->render('export_account_receivables_expired',array(
                'model'=>$model, 'actions' => $this->listActionsCanAccess,
        ));        
    }        
    
    
    
    
	public function actionView($id)
	{
                try{
                $this->render('view',array(
			'model'=>$this->loadModel($id), 'actions' => $this->listActionsCanAccess,
		));
                }
                catch (Exception $e)
                {
                    Yii::log("Uid: " .Yii::app()->user->id. "Exception ".  $e->getMessage(), 'error');
                    $code = 404;
                    if(isset($e->statusCode))
                        $code=$e->statusCode;
                    if($e->getCode())
                        $code=$e->getCode();
                    throw new CHttpException($code, $e->getMessage());
                }
		
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
                try
                {
		$model=new GasDailyCash('create_DailyCash');

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['GasDailyCash']))
		{
			$model->attributes=$_POST['GasDailyCash'];
                        $model->user_id_create = Yii::app()->user->id;
                        $model->created_date = date('Y-m-d H:i:s');                        
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('create',array(
			'model'=>$model, 'actions' => $this->listActionsCanAccess,
		));
                }
                catch (Exception $e)
                {
                    Yii::log("Uid: " .Yii::app()->user->id. "Exception ".  $e->getMessage(), 'error');
                    $code = 404;
                    if(isset($e->statusCode))
                        $code=$e->statusCode;
                    if($e->getCode())
                        $code=$e->getCode();
                    throw new CHttpException($code, $e->getMessage());
                }
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
                try
                {
		$model=$this->loadModel($id);
                $model->scenario = 'update_DailyCash';
                $model->date_collection = MyFormat::dateConverYmdToDmy($model->date_collection);
                $model->autocomplete_name = $model->customer?$model->customer->first_name:'';
		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['GasDailyCash']))
		{
			$model->attributes=$_POST['GasDailyCash'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('update',array(
			'model'=>$model, 'actions' => $this->listActionsCanAccess,
		));
                }
                catch (Exception $e)
                {
                    Yii::log("Uid: " .Yii::app()->user->id. "Exception ".  $e->getMessage(), 'error');
                    $code = 404;
                    if(isset($e->statusCode))
                        $code=$e->statusCode;
                    if($e->getCode())
                        $code=$e->getCode();
                    throw new CHttpException($code, $e->getMessage());
                }
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
                try
                {
		if(Yii::app()->request->isPostRequest)
		{
			// we only allow deletion via POST request
			if($model = $this->loadModel($id))
                        {
                            if($model->delete())
                                Yii::log("Delete record ".  print_r($model->attributes, true), 'info');
                        }

			// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
			if(!isset($_GET['ajax']))
				$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('index'));
		}
		else
                {
                    Yii::log("Invalid request. Please do not repeat this request again.");
                    throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
                }	
                }
                catch (Exception $e)
                {
                    Yii::log("Uid: " .Yii::app()->user->id. "Exception ".  $e->getMessage(), 'error');
                    $code = 404;
                    if(isset($e->statusCode))
                        $code=$e->statusCode;
                    if($e->getCode())
                        $code=$e->getCode();
                    throw new CHttpException($code, $e->getMessage());
                }
	}

	/**
	 * Manages all models.
	 */
	public function actionIndex()
	{
                try
                {
		$model=new GasDailyCash('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['GasDailyCash']))
			$model->attributes=$_GET['GasDailyCash'];

		$this->render('index',array(
			'model'=>$model, 'actions' => $this->listActionsCanAccess,
		));
                }
                catch (Exception $e)
                {
                    Yii::log("Uid: " .Yii::app()->user->id. "Exception ".  $e->getMessage(), 'error');
                    $code = 404;
                    if(isset($e->statusCode))
                        $code=$e->statusCode;
                    if($e->getCode())
                        $code=$e->getCode();
                    throw new CHttpException($code, $e->getMessage());
                }
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the ID of the model to be loaded
	 */
	public function loadModel($id)
	{
                try
                {
		$model=GasDailyCash::model()->findByPk($id);
		if($model===null)
                {
                    Yii::log("The requested page does not exist.");
                    throw new CHttpException(404,'The requested page does not exist.');
                }			
		return $model;
                }
                catch (Exception $e)
                {
                    Yii::log("Uid: " .Yii::app()->user->id. "Exception ".  $e->getMessage(), 'error');
                    $code = 404;
                    if(isset($e->statusCode))
                        $code=$e->statusCode;
                    if($e->getCode())
                        $code=$e->getCode();
                    throw new CHttpException($code, $e->getMessage());
                }
	}

	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
                try
                {
		if(isset($_POST['ajax']) && $_POST['ajax']==='gas-daily-cash-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
                }
                catch (Exception $e)
                {
                    Yii::log("Uid: " .Yii::app()->user->id. "Exception ".  $e->getMessage(), 'error');
                    $code = 404;
                    if(isset($e->statusCode))
                        $code=$e->statusCode;
                    if($e->getCode())
                        $code=$e->getCode();
                    throw new CHttpException($code, $e->getMessage());
                }
	}
        
        public function importExcel($model){
                Yii::import('application.extensions.vendors.PHPExcel',true);
                $objReader = PHPExcel_IOFactory::createReader("Excel2007");
                
                $objPHPExcel = $objReader->load(@$_FILES['GasDailyCash']['tmp_name']['file_excel']);
                $objWorksheet = $objPHPExcel->getActiveSheet();
                $highestRow = $objWorksheet->getHighestRow(); // e.g. 10
                //$highestColumn = $objWorksheet->getHighestColumn(); // e.g 'F'
                $highestColumn = 'E';
                $highestColumnIndex = PHPExcel_Cell::columnIndexFromString($highestColumn);

                $success = 1;
                $aRowInsert=array();
//                $countRowSuccess = 0;
//                $countRowError = 0;
                $aCellError = array();
//                $aRowError = array();       
                $created_date=date('Y-m-d H:i:s');
                $uid = Yii::app()->user->id;
                $aCustomer=  Users::getArrCustomerForImport();
				$aSale =  Users::getArrSaleIdForImport();	
//                $aCustomerPaymentDay=  Users::getArrPaymentDayOfCustomer();
                for ($row = 2; $row <= $highestRow; ++$row)
                {                    
                    // file excel cần format column theo dạng text hết để người dùng nhập vào khi đọc không lỗi
                    $d = trim($objWorksheet->getCellByColumnAndRow(0, $row)->getValue());
                    $m = trim($objWorksheet->getCellByColumnAndRow(1, $row)->getValue());
                    $y = trim($objWorksheet->getCellByColumnAndRow(2, $row)->getValue());
                    $date_collection =  $y.'-'.$m.'-'.$d;
                    
                    $temp_customer_id = trim(trim($objWorksheet->getCellByColumnAndRow(3, $row)->getValue()));
                    if(isset($aCustomer[$temp_customer_id]))
                        $customer_id = $aCustomer[$temp_customer_id];
                    else{
                        $aCellError[] = ' Dòng '.$row.' Khong co Khach Hang: '.$temp_customer_id;
                        continue;
                    }  
					
                    if(isset($aSale[$customer_id]))
                        $sale_id = $aSale[$customer_id];
                    else{
                        $aCellError[] = ' Dòng '.$row.' Không có sale id của Khách Hàng: '.$temp_customer_id;
                        continue;
                    }
					
                    $total = round(trim($objWorksheet->getCellByColumnAndRow(4, $row)->getValue()));
                    $aRowInsert[]="(
                        '$customer_id',
                        '$date_collection',
                        '$total',
						'$sale_id',	
                        '$uid',
                        '$created_date'    
                        )";
                }
				
            if(count($aCellError)>0){
                $mLog = new GasImportLog();
                $mLog->user_id = $uid;
                $mLog->created_date = $created_date;
                $mLog->json = json_encode($aCellError);
                $mLog->save();
                echo 'Nếu gặp lỗi này hãy giữ nguyên (và chụp ) màn hình và thông báo lại cho Anh Dũng';
                echo '<pre>';
                echo print_r($aCellError);
                echo '</pre >';
                die;
            }				
            $tableName = GasDailyCash::model()->tableName();
            $sql = "insert into $tableName (
                            customer_id,
                            date_collection,
                            total,
							sale_id,
                            user_id_create,
                            created_date
                            ) values ".implode(',', $aRowInsert);
            
			
            Yii::app()->db->createCommand($sql)->execute();

            return $aCellError;
        }    
                
        
        
}
