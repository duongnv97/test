<?php

class GasUpholdPlanController extends AdminController
{
    public $singleTitle = 'Kế hoạch bảo trì định kỳ';
    public $pluralTitle = 'Kế hoạch bảo trì định kỳ';
    public function actionView($id)
    {
        $this->pageTitle = 'Chi tiết kế hoạch bảo trì định kỳ';
        try{
        $this->render('view',array(
                'model'=>$this->loadModel($id), 'actions' => $this->listActionsCanAccess,
        ));
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }

    public function actionCreate(){
        $this->pageTitle = 'Tạo mới kế hoạch bảo trì định kỳ';
        try{
        $model=new GasUpholdPlan('create');
        $model->unsetAttributes();
        if(isset($_POST['GasUpholdPlan'])){
            $model->attributes = $_POST['GasUpholdPlan'];
            $model->validate();
            $model->validateMulti();
            if(!$model->hasErrors()){
                $model->saveMulti();
                Yii::app()->user->setFlash( MyFormat::SUCCESS_UPDATE, "Thêm mới thành công." );
                $this->redirect(array('create'));
                return;
            }
        }
        $this->render('create',array(
                'model'=>$model, 'actions' => $this->listActionsCanAccess,
        ));
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }
    public function actionUpdate($id){
        $this->pageTitle = 'Cập Nhật ';
        try {
            $model = $this->loadModel($id);
            $model->scenario = 'update';
            if (!$model->canUpdate()) {
                $this->redirect(array('index'));
            }
            $model->formatDataAfterSelect();
            if (isset($_POST['GasUpholdPlan'])) {
                $model->attributes      = $_POST['GasUpholdPlan'];
                $model->validate();
                if (!$model->hasErrors()) {
                    $model->formatDataBeforeSave();
                    $model->update();
                    Yii::app()->user->setFlash(MyFormat::SUCCESS_UPDATE, "Cập nhật thành công.");
                    $this->redirect(array('update', 'id' => $model->id));
                }
            }
            $this->render('update', array(
                'model' => $model, 'actions' => $this->listActionsCanAccess,
            ));
        } catch (Exception $exc) {
            GasCheck::CatchAllExeptiong($exc);
        }
    }
    
        /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id)
    {
       try {
            if (Yii::app()->request->isPostRequest) {
                // we only allow deletion via POST request
                if ($model = $this->loadModel($id)) {
                    if ($model->delete())
                        Yii::log("Uid: " . Yii::app()->user->id . " Delete record " . print_r($model->attributes, true), 'info');
                }

                // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
                if (!isset($_GET['ajax']))
                    $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('index'));
            }
            else {
                Yii::log("Uid: " . Yii::app()->user->id . " Invalid request. Please do not repeat this request again.");
                throw new CHttpException(400, 'Invalid request. Please do not repeat this request again.');
            }
        } catch (Exception $exc) {
            GasCheck::CatchAllExeptiong($exc);
        }
    }

    /**
     * Manages all models.
     */
    public function actionIndex()
    {
        $this->pageTitle = 'Kế hoạch bảo trì định kỳ';
        try{
        $model=new GasUpholdPlan('search');
        $model->unsetAttributes();
        if(isset($_GET['GasUpholdPlan'])){
            $model->attributes = $_GET['GasUpholdPlan'];
        }
        $this->render('index',array(
                'model'=>$model, 'actions' => $this->listActionsCanAccess,
        ));
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }
    
    public function loadModel($id) {
        try {
            $model = GasUpholdPlan::model()->findByPk($id);
            if ($model === null) {
                Yii::log("The requested page does not exist.");
                throw new CHttpException(404, 'The requested page does not exist.');
            }
            return $model;
        } catch (Exception $exc) {
            GasCheck::CatchAllExeptiong($exc);
        }
    }
}