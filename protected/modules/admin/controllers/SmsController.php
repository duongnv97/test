<?php

class SmsController extends AdminController 
{
    public $pluralTitle = "SMS";
    public $singleTitle = "SMS";
    /**
     * Manages all models.
     */
    public function actionIndex()
    {
        $this->pageTitle = 'SMS Send';
        try
        {
        $model=new GasScheduleSmsHistory();
        $model->unsetAttributes();  // clear any default values
        if(isset($_GET['GasScheduleSmsHistory']))
            $model->attributes=$_GET['GasScheduleSmsHistory'];
        $view = 'index';
        if(isset($_GET['report'])){
            $view = 'index_report';
        }elseif(isset($_GET['SmsWait'])){
            $view = 'index_sms';
        }
        
        $this->render($view,array(
                'model'=>$model, 'actions' => $this->listActionsCanAccess,
        ));
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
    * If the data model is not found, an HTTP exception will be raised.
     * @param integer the ID of the model to be loaded
     */
    public function loadModel($id)
    {
        try
        {
        $model=GasScheduleSms::model()->findByPk($id);
        if($model===null)
        {
            Yii::log("The requested page does not exist.");
            throw new CHttpException(404,'The requested page does not exist.');
        }			
        return $model;
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }

   
}
