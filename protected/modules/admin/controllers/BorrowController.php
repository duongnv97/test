<?php

class BorrowController extends AdminController 
{
    public $pluralTitle = "Quản Lý Vay";
    public $singleTitle = "Quản Lý Vay";
    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id)
    {
        $this->pageTitle = 'Xem ';
        try{
        $this->render('view',array(
                'model'=>$this->loadModel($id), 'actions' => $this->listActionsCanAccess,
        ));
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate()
    {
        $this->pageTitle = 'Tạo Mới ';
        try{
        $model=new Borrow('create');
        if(isset($_POST['Borrow']))
        {
            $model->attributes=$_POST['Borrow'];
            $model->validate();
            if(!$model->hasErrors()){
                $model->uid_login = Yii::app()->user->id;
                $model->save();
                Yii::app()->user->setFlash( MyFormat::SUCCESS_UPDATE, "Thêm mới thành công." );
               $this->redirect(array('create'));
            }				
        }

        $this->render('create',array(
                'model'=>$model, 'actions' => $this->listActionsCanAccess,
        ));
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id)
    {
        $this->pageTitle = 'Cập Nhật ';
        try{
        $model=$this->loadModel($id);
        $model->scenario = 'update';
        $model->borrow_date = MyFormat::dateConverYmdToDmy($model->borrow_date);
        $model->expiry_date = MyFormat::dateConverYmdToDmy($model->expiry_date);
        if(!$model->canUpdate()){
                $this->redirect(array('index'));
        }

        if(isset($_POST['Borrow']))
        {
            $model->attributes=$_POST['Borrow'];
            $model->validate();
            if(!$model->hasErrors()){
                $model->save();
                Yii::app()->user->setFlash( MyFormat::SUCCESS_UPDATE, "Cập nhật thành công." );
                $this->redirect(array('update','id'=>$model->id));
            }
        }

        $this->render('update',array(
                'model'=>$model, 'actions' => $this->listActionsCanAccess,
        ));
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id)
    {
        try{
        if(Yii::app()->request->isPostRequest)
        {
            // we only allow deletion via POST request
            if($model = $this->loadModel($id))
            {
                if($model->delete())
                    Yii::log("Uid: " .Yii::app()->user->id. " Delete record ".  print_r($model->attributes, true), 'info');
            }

            // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
            if(!isset($_GET['ajax']))
                $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('index'));
        }
        else
        {
            Yii::log("Uid: " .Yii::app()->user->id. " Invalid request. Please do not repeat this request again.");
            throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
        }	
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }

    /**
     * Manages all models.
     */
    public function actionIndex()
    {
        $this->pageTitle = 'Danh Sách ';
        try{
        $model=new Borrow('search');
        $model->initSessionAccounting();
        $model->unsetAttributes();  // clear any default values
        if(isset($_GET['Borrow']))
            $model->attributes=$_GET['Borrow'];
        $model->credit_type = Borrow::CREDIT_TYPE_BORROW;
        $this->render('index',array(
                'model'=>$model, 'actions' => $this->listActionsCanAccess,
        ));
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
    * If the data model is not found, an HTTP exception will be raised.
     * @param integer the ID of the model to be loaded
     */
    public function loadModel($id)
    {
        try{
        $model=Borrow::model()->findByPk($id);
        if($model===null)
        {
            Yii::log("The requested page does not exist.");
            throw new CHttpException(404,'The requested page does not exist.');
        }			
        return $model;
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }

    /** @Author: ANH DUNG Jul 20, 2018
     *  @Todo: xử lý format một số biến của Lend
     **/
    public function formatForLend($model) {
        if($model->credit_type == Borrow::CREDIT_TYPE_BORROW){
            return ;
        }
        $this->pageTitle = 'Tạo trả tiền';
    }
    
    /**
     * @Author: ANH DUNG Aug 01, 2016
     * @Todo: tạo mới đáo hạn
     */
    public function actionCreateDetail($id)
    {
        $this->pageTitle = 'Tạo mới đáo hạn';
        $this->layout = "ajax";
        try{
        $model=$this->loadModel($id);
        $model->mDetail = new BorrowDetail("CreateDetail");
        $this->formatForLend($model);
        if(isset($_POST['BorrowDetail']))
        {
            $model->mDetail->attributes=$_POST['BorrowDetail'];
            $model->mDetail->borrow_id = $id;
            $model->mDetail->validate();
            if(!$model->mDetail->hasErrors()){
                $model->mDetail->uid_login = Yii::app()->user->id;
                $model->mDetail->save();
                Yii::app()->user->setFlash( MyFormat::SUCCESS_UPDATE, "Thêm mới đáo hạn thành công." );
                $url = Yii::app()->createAbsoluteUrl("admin/borrow/createDetail", array('id'=>$id));
                $this->redirect($url);
            }				
        }

        $this->render('Detail/CreateDetail',array(
                'model'=>$model, 'actions' => $this->listActionsCanAccess,
        ));
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }
    
    /**
     * @Author: ANH DUNG Aug 01, 2016
     * @Todo: delete đáo hạn
     */
    public function actionDeleteDetail($id)
    {
        try{
        $mDetail = MyFormat::loadModelByClass($id, 'BorrowDetail');
        $url = Yii::app()->createAbsoluteUrl("admin/borrow/index");
        if($mDetail)
        {
            $borrow_id = $mDetail->borrow_id;
            $mDetail->delete();
            Yii::app()->user->setFlash( MyFormat::SUCCESS_UPDATE, "Xóa đáo hạn thành công." );
            $url = Yii::app()->createAbsoluteUrl("admin/borrow/createDetail", array('id'=>$borrow_id));
        }
        $this->redirect($url);
        
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }
    
    /**
     * @Author: ANH DUNG Aug 05, 2016
     * @Todo: ExportExcel
     */
    public function actionExportExcel(){
        try{
        if(isset($_SESSION['data-excel'])){
            ExportList::Borrow();
        }
        $this->redirect(array('index'));
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }
    
    /**
     * @Author: ANH DUNG Sep 27, 2016
     * @Todo: bc vay
     */
    public function actionReport()
    {
        $this->pageTitle = 'Báo cáo tổng tiền vay';
        try{
            $this->allowReport();
            $model = new Borrow();
            $this->render('Report/Report',array(
                    'model' => $model, 'actions' => $this->listActionsCanAccess,
            ));
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }
    
    /** @Author: ANH DUNG Jul 26, 2018
     *  @Todo: check user can access to action
     **/
    public function allowReport(){
        $cUid   = MyFormat::getCurrentUid();
        $cRole  = MyFormat::getCurrentRoleId();
        $aUidAllow  = [
            GasLeave::UID_QUYNH_MTN,
        ];
        $aRoleAllow = [ROLE_ACCOUNTING];
        if(in_array($cRole, $aRoleAllow) && !in_array($cUid, $aUidAllow)){
            $this->redirect(Yii::app()->createAbsoluteUrl(''));
        }
    }
    
    /** @Author: HOANG NAM 17/07/2018
     *  @Todo: export excel of action report
     **/
    public function actionExportExcelReport(){
        try{
        if(isset($_SESSION['model-excel-report'])){
            ToExcel::summaryBorrowReport();
        }
        $this->redirect(array('report'));
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }
    
    /***************** START NAM 19/07/2018 ZONE LEND **************/
    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionLendView($id)
    {
        $this->pageTitle = 'Xem ';
        try{
        $this->render('Lend/view',array(
                'model'=>$this->loadModel($id), 'actions' => $this->listActionsCanAccess,
        ));
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionLendCreate()
    {
        $this->pageTitle = 'Tạo Mới ';
        try{
        $model=new Borrow('LendCreate');
        $model->credit_type = Borrow::CREDIT_TYPE_LEND;
        if(isset($_POST['Borrow']))
        {
            $model->attributes=$_POST['Borrow'];
            $model->validate();
            if(!$model->hasErrors()){
                $model->uid_login = Yii::app()->user->id;
                $model->save();
                Yii::app()->user->setFlash( MyFormat::SUCCESS_UPDATE, "Thêm mới thành công." );
               $this->redirect(array('lendCreate'));
            }				
        }

        $this->render('Lend/create',array(
                'model'=>$model, 'actions' => $this->listActionsCanAccess,
        ));
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionLendUpdate($id)
    {
        $this->pageTitle = 'Cập Nhật ';
        try{
        $model=$this->loadModel($id);
        $model->scenario = 'LendUpdate';
        $model->borrow_date = MyFormat::dateConverYmdToDmy($model->borrow_date);
        $model->expiry_date = MyFormat::dateConverYmdToDmy($model->expiry_date);
        if(!$model->canUpdate()){
            $this->redirect(array('LendIndex'));
        }

        if(isset($_POST['Borrow'])) 
        {
            $model->attributes=$_POST['Borrow'];
            $model->validate();
            if(!$model->hasErrors()){
                $model->save();
                Yii::app()->user->setFlash( MyFormat::SUCCESS_UPDATE, "Cập nhật thành công." );
                $this->redirect(array('LendUpdate','id'=>$model->id));
            }
        }

        $this->render('Lend/update',array(
                'model'=>$model, 'actions' => $this->listActionsCanAccess,
        ));
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }
    
    /**
     * Manages all models.
     */
    public function actionLendIndex()
    {
        $this->pageTitle = 'Quản lý cho vay';
        try{
        $model=new Borrow('search');
        $model->initSessionAccounting();
        $model->unsetAttributes();  // clear any default values
        if(isset($_GET['Borrow']))
            $model->attributes=$_GET['Borrow'];
        $model->credit_type = Borrow::CREDIT_TYPE_LEND;

        $this->render('Lend/index',array(
                'model'=>$model, 'actions' => $this->listActionsCanAccess,
        ));
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }
    
    /**
     * 
     */
    public function actionLendExportExcel(){
        try{
        if(isset($_SESSION['data-excel'])){
            ExportList::Borrow();
        }
        $this->redirect(array('LendIndex'));
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }
    
    /**
     * 
     */
    public function actionLendReport()
    {
        $this->pageTitle = 'Báo cáo tổng tiền cho vay';
        $this->allowLendReport();
        try{
            $model = new Borrow();
            $model->credit_type = Borrow::CREDIT_TYPE_LEND;
            $this->render('Report/Report',array(
                    'model' => $model, 'actions' => $this->listActionsCanAccess,
            ));
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }
    
        /** @Author: ANH DUNG Aug 20, 2018
     *  @Todo: kiểm tra allow access report
     **/
    public function allowLendReport(){
        $cUid = MyFormat::getCurrentUid();
        $cRole = MyFormat::getCurrentRoleId();
        $aUidAllow  = [
            GasLeave::UID_QUYNH_MTN,
        ];
        $aRoleAllow = [ROLE_ACCOUNTING];
        if(in_array($cRole, $aRoleAllow) && !in_array($cUid, $aUidAllow)){
            $this->redirect(Yii::app()->createAbsoluteUrl(''));
        }
    }
    
    
    /** @Author: HOANG NAM 17/07/2018
     *  @Todo: export excel of action report
     **/
    public function actionLendExportExcelReport(){
        try{
        if(isset($_SESSION['model-excel-report'])){
            ToExcel::summaryBorrowReport();
        }
        $this->redirect(array('report'));
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }
    /***************** END NAM 19/07/2018 ZONE LEND **************/
}
