<?php

class GasagentController extends AdminController
{ 
    
//         public function accessRules()
//        {
//            return array();
//        } 
    public function actionView($id)
    {
        try{
            $model = $this->loadModel($id);
            $model->aMaterial = explode(",", $model->password_hash);
            $this->pageTitle = $model->getFullName().' - Xem Đại Lý';
            if(!Users::CanAccessProvince($model->province_id)){
                $this->redirect(array('index'));
            }
            $this->render('view',array(
                    'model'=> $model, 'actions' => $this->listActionsCanAccess,
            ));
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate()
    {
        $this->pageTitle = 'Tạo Mới Đại Lý';
        try{
        $model=new Users('create_agent');
        $model->aMaterial = array();
        if(isset($_POST['Users']))
        {
            $model->attributes=$_POST['Users'];
            $model->role_id = ROLE_AGENT;
            $model->application_id = BE;
            $model->code_bussiness = $model->code_account;
            $model->validate();
            if(!$model->hasErrors()){
               $model->scenario = NULL;
                $model->save();
                $mOM = new GasOneMany(); //NhanDT Oct 01,2019
                $mOM->afterSaveAgent($model);
                $this->setCacheAgent($model);
                $this->redirect(array('view','id'=>$model->id));
            }
        }
        $this->render('create',array(
                'model'=>$model, 'actions' => $this->listActionsCanAccess,
        ));
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }
    
    /** @Author: ANH DUNG Sep 10, 2018, check user truy cập
     **/
    public function allowUpdate(){
        $cUid = MyFormat::getCurrentUid();
        $cRole = MyFormat::getCurrentRoleId();
        $aUidAllow  = [
            GasLeave::DUNG_NTT,
        ];
        $aRoleAllow = [ROLE_ACCOUNTING];
        if(in_array($cRole, $aRoleAllow) && !in_array($cUid, $aUidAllow)){
            $this->redirect(Yii::app()->createAbsoluteUrl(''));
        }
    }
    
    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id)
    {
        try{
        $this->allowUpdate();
        $model=$this->loadModel($id);
        $model->modelOld    = clone $model;
        $model->aMaterial   = explode(",", $model->password_hash);
        $this->pageTitle    = $model->getFullName(). ' - Cập Nhật Đại Lý';
        $model->scenario    = 'update_agent';
        $model->autocomplete_name_street    = $model->street?$model->street->name:'';
        $model->agent_name_real             = $model->getKeyInfoV1(UsersExtend::AGENT_NAME_REAL);
        if(isset($_POST['Users']))
        {
            $aAttSave = array('username','code_account','code_bussiness',
                'first_name','address','province_id','district_id','ward_id',
                'house_numbers','street_id','status','phone',
                'beginning','payment_day','gender','last_logged_in','slug','temp_password','password_hash'
            );
            $model->attributes=$_POST['Users'];
            $model->role_id = ROLE_AGENT;
            $model->application_id = BE;
            $model->validate($aAttSave);
            if(!$model->hasErrors()){
//                $model->update($aAttSave); Close on Sep 27, 2016 chắc không cần save kiểu $aAttSave
                $model->setKeyInfo(UsersExtend::AGENT_NAME_REAL, $model->agent_name_real);
                $model->update();
                $mOM = new GasOneMany(); //NhanDT Oct 01,2019
                $mOM->afterSaveAgent($model);
                UsersPhone::savePhone($model);// Jul 18, 2016
                $this->handleAlertWhenChangeTypeAgent($model);
                $this->setCacheAgent($model);
                Yii::app()->user->setFlash( MyFormat::SUCCESS_UPDATE, "Cập nhật thành công." );
                $this->redirect(array('update','id'=>$model->id));
            }
        }

        $this->render('update',array(
                'model'=>$model, 'actions' => $this->listActionsCanAccess,
        ));
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }
    
    public function setCacheAgent($mUser) {
        $mAppCache = new AppCache();
        $mAppCache->setAgent();
        $mAppCache->setCacheAgentExt();
        $mAppCache->setAgentListdata();
        $mUser->solrAdd();
        $mAppCache->setGas24hAgentRadius();
    }
    
    /** @Author: DungNT May 20, 2019 
     *  @Todo: Mở mới: khi chuyển từ trạng thái quy hoạch sang mở mới thì email cho cả phòng kế toán và pháp lý nắm, hoàn thành xong thì chuyển active
     **/
    public function handleAlertWhenChangeTypeAgent($mUser) {
        $aTypeAlert = [Users::IS_AGENT_PLAN, Users::IS_AGENT_NEW];
        $mOldAgent  = $mUser->modelOld;
        if($mOldAgent->gender != $mUser->gender && in_array($mOldAgent->gender, $aTypeAlert)){
            // mail notify
            $mSendEmail = new SendEmail();
            $mSendEmail->alertWhenChangeTypeAgent($mUser);
        }
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id)
    {
        try{
        if(Yii::app()->request->isPostRequest)
        {
            // we only allow deletion via POST request
            if($model = $this->loadModel($id))
            {
                if($model->delete())
                    Yii::log("Uid: " .Yii::app()->user->id. " Delete record ".  print_r($model->attributes, true), 'info');
            }

            // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
            if(!isset($_GET['ajax']))
                $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('index'));
        }
        else
        {
            Yii::log("Uid: " .Yii::app()->user->id. " Invalid request. Please do not repeat this request again.");
            throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
        }	
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }

    /**
     * Manages all models.
     */
    public function actionIndex()
    {
        $this->pageTitle = 'Đại Lý Danh Sách';
        try{
        Users::ResetAllDateUpdateStoreCard();
        Users::InitSessionModelRole(array(ROLE_EMPLOYEE_MAINTAIN, ROLE_ACCOUNTING_AGENT));
        $mSell=new Sell('search');
        $mSell->InitMaterial();
        $model=new Users('search');
        $model->unsetAttributes();  // clear any default values
        if(isset($_GET['Users']))
            $model->attributes=$_GET['Users'];
        $model->role_id = ROLE_AGENT;
        $this->render('index',array(
            'model'=>$model, 'actions' => $this->listActionsCanAccess,
        ));
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }
    
    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer the ID of the model to be loaded
     */
    public function loadModel($id)
    {
        try{
        $model=Users::model()->findByPk($id);
        if($model===null)
        {
            Yii::log("The requested page does not exist.");
            throw new CHttpException(404,'The requested page does not exist.');
        }			
        return $model;
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }

    public function actionAdd_customer_of_agent(){
        $this->layout = 'ajax';
        $model = new GasAgentCustomer();
        $model->agent_id = $_GET['agent_id'];
        $msg= '';
        if(isset($_POST['GasAgentCustomer']) && !empty($_POST['GasAgentCustomer']['customer_id'])){
            $model->attributes=$_POST['GasAgentCustomer'];
            $criteria=new CDbCriteria;
            $criteria->compare('agent_id',$model->agent_id);
            $criteria->compare('customer_id',$model->customer_id);
            $newModel = GasAgentCustomer::model()->find($criteria);
            if(!$newModel){
                $model->save();
                $msg= 'Thêm Thành Công Khách Hàng: '.$model->customer->first_name;
                // cập nhật cờ báo đại lý có KH mới, nên khởi tạo lại session KH của đại lý
                // AjaxControllers public function actionSearch_user_by_code()
//                Users::updateFirstChar($model->agent_id, 1);// Close on Mar 15, 2017 chuyển sang lưu vào cache rồi
                $mAppCache = new AppCache();
                $mAppCache->setCustomerLimitOfAgent($model->agent_id);
            }else
                $msg= 'Khách Hàng: '.$model->customer->first_name.' - Đã Được Thêm Trước Đây';
        }

        $this->render('add_customer_of_agent',array(
                'model'=>$model, 'actions' => $this->listActionsCanAccess,
            'msg'=>$msg,
        ));
    }

    public function actionDelete_customer_agent(){
        $this->layout='';
        if(isset($_GET['id'])){
            GasAgentCustomer::model()->deleteByPk($_GET['id']);
            $model = GasAgentCustomer::model()->findByPk($_GET['id']);
            // cập nhật cờ báo đại lý có KH mới, nên khởi tạo lại session KH của đại lý
            // AjaxControllers public function actionSearch_user_by_code()
            if($model)
                Users::updateFirstChar($model->agent_id, 1);
//                die('<script type="text/javascript">$.fn.yiiGridView.update("users-grid");</script>'); 
        }
    }
    
    /**
     * @Author: ANH DUNG May 04, 2016
     */
    public function actionMonitorAgent() {
        $this->pageTitle = 'Giám sát máy tính đại lý';
        try{
            $model = new GasMonitorAgent();
            $model->date_from = date("d-m-Y");
            $model->date_to = date("d-m-Y");
            if(isset($_GET['report'])){
                $model->date_from = "01-".date("m-Y");
            }
            if(isset($_GET['GasMonitorAgent'])){
                $model->attributes = $_GET['GasMonitorAgent'];
            }
            $this->render('MonitorAgent/index',array(
                    'model'=> $model, 'actions' => $this->listActionsCanAccess,
            ));
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }

}
