<?php

class GasUpholdController extends AdminController 
{
    public $pluralTitle = 'Bảo Trì Sự Cố';
    public $singleTitle = 'Bảo Trì Sự Cố';
    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id)
    {
        try{
            $model = $this->loadModel($id);
            $this->pageTitle = $model->getCustomer().' - Xem Bảo Trì Sự Cố';
            $model->mReply = new GasUpholdReply();
            $model->mReply->scenario = 'create';
            $model->mReply->uphold_id = $id;
            $model->mReply->status = $model->status;
//            $model->mReply->mGasFile = new GasFile();
            $this->HandleReply($model);
            $this->render('view',array(
                    'model' => $model, 'actions' => $this->listActionsCanAccess,
            ));
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }
    
    
    /**
     * @Author: ANH DUNG Oct 21, 2015
     * @Todo: handle post reply
     */
    public function HandleReply($model) {
        if(isset($_POST['GasUpholdReply']))
        {
            $model->mReply->attributes=$_POST['GasUpholdReply'];
            $model->mReply->validate();
            $mGasFile = new GasFile();
            $mGasFile->validateFile($model->mReply);
            if(!$model->mReply->hasErrors()){
                $model->mReply->save();
                $mUpholdReply = GasUpholdReply::model()->findByPk($model->mReply->id);
                $mGasFile->saveRecordFile($mUpholdReply, GasFile::TYPE_3_UPHOLD_REPLY);
                Yii::app()->user->setFlash( MyFormat::SUCCESS_UPDATE, "Thêm mới thành công." );
                $this->redirect(array('view','id'=>$model->id));
            }
        }
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate()
    {
        $this->pageTitle = 'Tạo Mới ';
        try{
        $model=new GasUphold('create');
        $model->type = GasUphold::TYPE_SU_CO;
        $this->HandleGetEmployeeOfCustomer($model);
        if(isset($_GET['phone_number'])){
            $model->contact_tel = $_GET['phone_number'];
        }
        if(isset($_POST['GasUphold'])){
            $model->attributes=$_POST['GasUphold'];
            $model->validate();
            if(!$model->hasErrors()){
                $model->checkUserInactive();
                $model->save();
                Yii::app()->user->setFlash( MyFormat::SUCCESS_UPDATE, "Thêm mới thành công." );
                $this->redirect(array('create'));
            }
        }

        $this->render('create',array(
                'model'=>$model, 'actions' => $this->listActionsCanAccess,
        ));
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }
    
    /**
     * @Author: ANH DUNG Apr 20, 2016
     * @Todo: xử lý gắn employee vào cho customer khi user select KH ở dưới
     */
    public function HandleGetEmployeeOfCustomer($model) {
        if(isset($_GET['ext_customer_id']) || isset($_GET['customer_id'])){
            $model->customer_id = isset($_GET['ext_customer_id']) ? $_GET['ext_customer_id'] : 0;
            if(isset($_GET['customer_id'])){
                $model->customer_id = $_GET['customer_id'];
            }
            $model->employee_id = GasUphold::getEmployeeOfCustomer($model->customer_id);
            $mCustomer = $model->rCustomer;
            if($mCustomer && $mCustomer->channel_id == Users::CHAN_HANG){
                Yii::app()->user->setFlash( MyFormat::ERROR_UPDATE, 'Khách hàng đang bị chặn hàng');
                $this->redirect(array('index'));
            }
        }
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id)
    {
        try
        {
        $model=$this->loadModel($id);
        $this->pageTitle = $model->getCustomer().' - Cập Nhật Bảo Trì Sự Cố';
        if(!GasCheck::CanUpdateUphold($model) || $model->type != GasUphold::TYPE_SU_CO){
            $this->redirect(array('index'));
        }
        $model->scenario = 'update';
        $this->HandleGetEmployeeOfCustomer($model);
        if(isset($_POST['GasUphold']))
        {
            $model->attributes=$_POST['GasUphold'];
            $model->validate();
            if(!$model->hasErrors()){
                $model->checkUserInactive();
                $model->save();
                Yii::app()->user->setFlash( MyFormat::SUCCESS_UPDATE, "Cập nhật thành công." );
                $this->redirect(array('update','id'=>$model->id));
            }
        }

        $this->render('update',array(
                'model'=>$model, 'actions' => $this->listActionsCanAccess,
        ));
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id)
    {
        try
        {
        if(Yii::app()->request->isPostRequest)
        {
                // we only allow deletion via POST request
                if($model = $this->loadModel($id))
                {
                    if($model->delete())
                        Yii::log("Uid: " .Yii::app()->user->id. " Delete record ".  print_r($model->attributes, true), 'info');
                }

                // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
                if(!isset($_GET['ajax']))
                        $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('index'));
        }
        else
        {
            Yii::log("Uid: " .Yii::app()->user->id. " Invalid request. Please do not repeat this request again.");
            throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
        }	
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }

    /**
     * Manages all models.
     */
    public function actionIndex()
    {
        $this->pageTitle = 'BT sự cố';
        $this->handleDetail();
        try{
        $model=new GasUphold('search');
        $model->unsetAttributes();  // clear any default values
        if(isset($_GET['GasUphold']))
            $model->attributes=$_GET['GasUphold'];
        $model->type = GasUphold::TYPE_SU_CO;

//        $_SESSION['data-model'] = $model;
        $this->exportExcel($model);
        $this->render('index',array(
                'model'=>$model, 'actions' => $this->listActionsCanAccess,
        ));
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }

    /**
     * @Author: ANH DUNG Dec 19, 2016
     * @Todo: handle list detail
     */
    public function handleDetail() {
        if(!isset($_GET['detail'])){
            return ;
        }
        $this->pageTitle = 'Xem bản đồ sự cố';
        $model=new GasUpholdReply('search');
        $model->unsetAttributes();  // clear any default values
        if(isset($_GET['GasUpholdReply']))
            $model->attributes=$_GET['GasUpholdReply'];
        $model->type = GasUphold::TYPE_SU_CO;

        $this->render('detail/index',array(
                'model'=>$model, 'actions' => $this->listActionsCanAccess,
        ));
        
        die;
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
    * If the data model is not found, an HTTP exception will be raised.
     * @param integer the ID of the model to be loaded
     */
    public function loadModel($id)
    {
        try
        {
        $model=GasUphold::model()->findByPk($id);
        if($model===null)
        {
            Yii::log("The requested page does not exist.");
            throw new CHttpException(404,'The requested page does not exist.');
        }			
        return $model;
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }
    
    /**
     * @Author: ANH DUNG Apr 03, 2016
     * @Todo: report loai su co
     */
    public function actionReportByType() {
        $this->pageTitle = 'Báo cáo theo loại sự cố';
        try
        {
            $data=array();
            $model = new GasUphold();
            $model->report_year = date('Y');
            if(isset($_GET['GasUphold'])){
                $model->attributes = $_GET['GasUphold'];
                $data = $model->ReportByType();
            }
            $this->render('ReportByType/index',array(
                'model'=>$model, 'actions' => $this->listActionsCanAccess,
                "data"=>$data,
            ));
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }
    
    
    /**
     * @Author: ANH DUNG Apr 03, 2016
     * @Todo: report theo khach hang
     */
    public function actionReportByCustomer() {
        $this->pageTitle = 'Báo cáo theo khách hàng';
        try
        {
            $data=array();
            $model = new GasUphold();
            $model->date_from = "01".date('-m-Y');
            $model->date_to = date('d-m-Y');
            if(isset($_GET['GasUphold'])){
                $model->attributes = $_GET['GasUphold'];
                $data = $model->ReportByCustomer();
            }
            $this->render('ReportByCustomer/index',array(
                'model'=>$model, 'actions' => $this->listActionsCanAccess,
                "data"=>$data,
            ));
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }
    
    /**
     * @Author: ANH DUNG Apr 03, 2016
     * @Todo: report su co bao tri theo nhan vien
     */
    public function actionReportByEmployee() {
        $this->pageTitle = 'Báo cáo sự cố theo nhân viên';
        try{
            $data=array();
            $model = new GasUphold();
            $model->date_from = "01".date('-m-Y');
            $model->date_to = date('d-m-Y');
            if(isset($_GET['GasUphold'])){
                $model->attributes = $_GET['GasUphold'];
                $data = $model->ReportByEmployee();
            }
            $this->render('ReportByEmployee/index',array(
                'model'=>$model, 'actions' => $this->listActionsCanAccess,
                "data"=>$data,
            ));
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }
    
    /**
     * @Author: ANH DUNG Apr 03, 2016
     * @Todo: report su co bao tri theo nhan vien
     */
    public function actionReportUpholdSchedule() {
        $this->pageTitle = 'Báo cáo bảo trì định kỳ';
        try{
            $model = new GasUphold();
            $data = $model->ReportUpholdSchedule();
            $this->render('ReportUpholdSchedule/index',array(
                'model'=>$model, 'actions' => $this->listActionsCanAccess,
                "data"=>$data,
            ));
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }
   
    /**
     * @Author: ANH DUNG Dec 19, 2016
     * @Todo: report  bao tri định kỳ theo nhan vien
     */
    public function actionReportScheduleDaily() {
        $this->pageTitle = 'Báo cáo bảo trì Sự Cố - Định Kỳ hàng ngày';
        try{
            $data=array();
            $model = new GasUphold();
            $model->date_from = "01".date('-m-Y');
            $model->date_to = date('d-m-Y');
            if(isset($_GET['GasUphold'])){
                $model->attributes = $_GET['GasUphold'];
                $data = $model->reportScheduleDaily();
            }
            $this->render('ReportScheduleDaily/index',array(
                'model'=>$model, 'actions' => $this->listActionsCanAccess,
                "data"=>$data,
            ));
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }
    
    
    /** @Author: PHAM THANH NGHIA 2018
     *  @Todo: 
     *  @Code: NGHIA002
     *  @Param: 
     *   Làm trang list comment + rating của table gas_gas_uphold
        admin/gasUphold/index
     * admin/gasUphold/listRating 
     **/
    public function actionListRating(){
        $this->pageTitle = 'KH đánh giá bảo trì';
        try{
            $model=new GasUpholdRating('search');
       
            if(isset($_GET['GasUpholdRating']))
                $model->attributes=$_GET['GasUpholdRating'];

            $this->render('listRating/index',array(
                    'model'=>$model, 'actions' => $this->listActionsCanAccess,
            ));
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }
    
    /** @Author: PHAM THANH NGHIA 2018
     *  @Todo: 
     *  @Code: NGHIA002
     *  @Param: create audit status and audit note
     **/
    public function actionCreateAudit($id){
        $this->layout = "ajax";
        try{
            $model =  GasUpholdRating::model()->findbyPk($id);

            if(empty($model)){
                throw new Exception('Invalid request');
            }
            if($_POST){
                $model->attributes = $_POST['GasUpholdRating'];
                $model->update();
            }

        $this->render('listRating/_audit',array(
                'model'=>$model, 'actions' => $this->listActionsCanAccess,
        ));
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }
    public function exportExcel($model){
        try{
            if(!$model->canExportExcel() || !isset($_GET['ExportExcel'])){
                return ;
            }
        if(isset($_SESSION['data-excel'])){
            ini_set('memory_limit','2000M');// chưa cần chạy đoạn này, đưa memory_limit lên 500 có thể export dc khoảng 5000 row - chưa test
            $mToExcelList = new ToExcelList();
            $mToExcelList->exportGasUphold($model);
        }
//        $this->redirect(array('index'));
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }
    
    /** @Author: LocNV Aug 19, 2019
     *  @Todo: bao cao khoan bao tri
     *  @Param:
     **/
    public function actionReportPieceMaintain() {
        $this->pageTitle = 'Báo cáo khoán bảo trì';
        try
        {
            $data = array();
            $model = new GasUpholdReport();
            if(isset($_GET['GasUpholdReport'])){
                $model->attributes = $_GET['GasUpholdReport'];
                $model->area_id = $_GET['GasUpholdReport']['area_id'];
                $data = $model->reportPieceMaintain();
            }
            $this->render('ReportPieceMaintain/index',array(
                'model' => $model, 'actions' => $this->listActionsCanAccess,
                "data" => $data,
            ));
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }

}