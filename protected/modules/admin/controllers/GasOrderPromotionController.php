<?php

class GasOrderPromotionController extends AdminController 
{
    public $pluralTitle = "Đặt hàng vật tư - khuyến mãi";
    public $singleTitle = "Đặt hàng vật tư - khuyến mãi";
    
    public function actionView($id)
    {
        $this->pageTitle = 'Xem ';
        try{
            $model = $this->loadModel($id);
            if(!$model->CanView()){
                $this->redirect(array('index'));
            }
            $this->render('view',array(
                    'model'=> $model, 'actions' => $this->listActionsCanAccess,
            ));
        }
        catch (Exception $exc)
        {
            GasCheck::CatchAllExeptiong($exc);
        }
    }
    
    public function actionPrint($id)
    {
        $this->pageTitle = 'Print ';
        try{
            $model = $this->loadModel($id);
            $this->render('Print/index',array(
                    'model'=> $model, 'actions' => $this->listActionsCanAccess,
            ));
        }
        catch (Exception $exc)
        {
            GasCheck::CatchAllExeptiong($exc);
        }
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate()
    {
        $this->pageTitle = 'Tạo Mới ';
//        $this->redirect(array('index'));
        try{
        $model=new GasOrderPromotion('create');
        $model->mDetail = new GasOrderPromotionDetail();
        $cRole = MyFormat::getCurrentRoleId();
        if(!$model->agentCanCreate()){
            $this->redirect(array('index'));
        }
        if( $cRole == ROLE_SUB_USER_AGENT ){
            $model->agent_id = MyFormat::getAgentId();
        }elseif(isset ($_GET['agent_id'])){
            $model->agent_id = $_GET['agent_id'];
        }
        if(isset($_POST['GasOrderPromotion']))
        {
            $model->attributes=$_POST['GasOrderPromotion'];
            $model->validate();
            if(!$model->hasErrors()){
                $model->save();
                Yii::app()->user->setFlash( MyFormat::SUCCESS_UPDATE, "Thêm mới thành công." );
               $this->redirect(array('create'));
            }
        }

        $this->render('create',array(
                'model'=>$model, 'actions' => $this->listActionsCanAccess,
        ));
        }
        catch (Exception $exc)
        {
            GasCheck::CatchAllExeptiong($exc);
        }
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id)
    {
        $this->pageTitle = 'Cập Nhật ';
        try{
        $model=$this->loadModel($id);
        if(!$model->CanUpdateOrderPromotion()){
            $this->redirect(array('index'));
        }
        $model->scenario = 'update';
        $model->mDetail = new GasOrderPromotionDetail();
        $model->aModelDetail = $model->rDetail;
        $view = "update";
        $delivery_user_update = 0;
        if($model->isUserDeliveryUpdate()){
            $view = "update_by_delivery_user";
            $delivery_user_update = 1;
            if(isset($_POST['GasOrderPromotion'])){
                $model->status = GasOrderPromotion::STATUS_DELIVERY_UPDATED;   
            }
        }

        if(isset($_POST['GasOrderPromotion']))
        {
            $model->attributes=$_POST['GasOrderPromotion'];
            $model->validate();            
            $model->setStatusUserUpdate();            
            if(!$model->hasErrors()){
                $model->update();
                Yii::app()->user->setFlash( MyFormat::SUCCESS_UPDATE, "Cập nhật thành công." );
                $this->redirect(array('update','id'=>$model->id));
            }
        }

        $this->render($view,array(
            'model'=>$model, 'actions' => $this->listActionsCanAccess,
            'delivery_user_update'=>$delivery_user_update,
        ));
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id)
    {
        try{
        if(Yii::app()->request->isPostRequest)
        {
            // we only allow deletion via POST request
            if($model = $this->loadModel($id))
            {
                if($model->delete())
                    Yii::log("Uid: " .Yii::app()->user->id. " Delete record ".  print_r($model->attributes, true), 'info');
            }

            // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
            if(!isset($_GET['ajax']))
                    $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('index'));
        }
        else
        {
            Yii::log("Uid: " .Yii::app()->user->id. " Invalid request. Please do not repeat this request again.");
            throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
        }	
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }

    /**
     * Manages all models.
     */
    public function actionIndex()
    {
        $this->pageTitle = 'Danh Sách ';
        try{
        $model=new GasOrderPromotion('search');
        $model->InitMaterial();
        $this->CheckQty($model);
        $this->handleMiss($model);
        $model->unsetAttributes();  // clear any default values
        if(isset($_GET['GasOrderPromotion']))
            $model->attributes=$_GET['GasOrderPromotion'];
        $model->status = $model->aStatusViewNew;
        if(isset($_GET['status'])){
            $model->status = $_GET['status'];
        }
        
        $this->render('index',array(
                'model'=>$model, 'actions' => $this->listActionsCanAccess,
        ));
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }
    
    function getStartAndEndDate($week, $year) {
        $dto = new DateTime();
        $ret['week_start'] = $dto->setISODate($year, $week)->format('Y-m-d');
        $ret['week_end'] = $dto->modify('+6 days')->format('Y-m-d');
        return $ret;
    }
    
    /**
     * @Author: ANH DUNG Jul 06, 2016
     * @Todo: xử lý tóm tắt đơn hàng còn thiếu theo thời gian
     */
    public function handleMiss($model)
    {
        if(isset($_GET['missing'])){
            $this->pageTitle = 'Đơn hàng thiếu - Tổng hợp';
            $this->render('Miss/Miss',array(
                    'model'=> $model, 'actions' => $this->listActionsCanAccess,
            ));
            die;
        }
    }
    
    /**
     * @Author: ANH DUNG Feb 24, 2016
     * @Todo: handle report summary 
     */
    public function actionSummary() {
        $model=new GasOrderPromotion('search');
        $model->handleAccountingQty($this);
        $model->InitMaterial();
        $aStartAndEndDate=array();
        if(isset($_GET['GasOrderPromotion']))
            $model->attributes=$_GET['GasOrderPromotion'];
        $data = $model->Summary($aStartAndEndDate);
        if(empty($model->date_from)){
            $model->date_from = MyFormat::dateConverYmdToDmy($aStartAndEndDate['week_start'], "d-m-Y");
        }
        if(empty($model->date_to)){
            $model->date_to = MyFormat::dateConverYmdToDmy($aStartAndEndDate['week_end'], "d-m-Y");
        }
        
        $this->render('summary',array(
            'model'=>$model, 'actions' => $this->listActionsCanAccess,
            'data'=>$data,
            'aStartAndEndDate'=>$aStartAndEndDate,
        ));
    }
    
    /**
     * Returns the data model based on the primary key given in the GET variable.
    * If the data model is not found, an HTTP exception will be raised.
     * @param integer the ID of the model to be loaded
     */
    public function loadModel($id)
    {
        try{
        $model=GasOrderPromotion::model()->findByPk($id);
        if($model===null)
        {
            Yii::log("The requested page does not exist.");
            throw new CHttpException(404,'The requested page does not exist.');
        }			
        return $model;
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }

    /**
     * @Author: ANH DUNG Sep 25 2016
     * @Todo: handle report kiểm tra đối chiếu số lg đặt với slg thực nhận
     */
    public function CheckQty($model) {
        if(!isset($_GET['CheckQty'])){
            return ;
        }
        $data = array();
        $model->date_from   = "01-".date('m-Y');
        $model->date_to     = date("d-m-Y");
        if(isset($_GET['GasOrderPromotion'])){
            $model->attributes = $_GET['GasOrderPromotion'];
            $data = $model->CheckQty();
        }
        $this->render('CheckQty/CheckQty',array(
            'model'=>$model, 'actions' => $this->listActionsCanAccess,
            'data'=>$data,
        ));
        die;
    }
}
