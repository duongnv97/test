<?php

class GastargetmonthlyController extends AdminController
{
    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id)
    {
            try{
            $this->render('view',array(
                    'model'=>$this->loadModel($id), 'actions' => $this->listActionsCanAccess,
            ));
            }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate()
    {
        try{
        $model=new GasTargetMonthly('create');
        if(isset($_POST['GasTargetMonthly']))
        {
            $model->attributes=$_POST['GasTargetMonthly'];
            if($model->save())
                    $this->redirect(array('view','id'=>$model->id));
        }

        $this->render('create',array(
                'model'=>$model, 'actions' => $this->listActionsCanAccess,
        ));
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id)
    {
            try
            {
            $model=$this->loadModel($id);

            // Uncomment the following line if AJAX validation is needed
            // $this->performAjaxValidation($model);

            if(isset($_POST['GasTargetMonthly']))
            {
                    $model->attributes=$_POST['GasTargetMonthly'];
                    if($model->save())
                            $this->redirect(array('view','id'=>$model->id));
            }

            $this->render('update',array(
                    'model'=>$model, 'actions' => $this->listActionsCanAccess,
            ));
            }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id)
    {
            try
            {
            if(Yii::app()->request->isPostRequest)
            {
                    // we only allow deletion via POST request
                    if($model = $this->loadModel($id))
                    {
                        if($model->delete())
                            Yii::log("Delete record ".  print_r($model->attributes, true), 'info');
                    }

                    // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
                    if(!isset($_GET['ajax']))
                            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('index'));
            }
            else
            {
                Yii::log("Invalid request. Please do not repeat this request again.");
                throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
            }	
            }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }

    /**
     * Manages all models.
     */
    public function actionIndex()
    {
        try{
        $model=new GasTargetMonthly();
        $model->monthly = 13;  // clear any default values
        $model->year = date('Y');  // clear any default values
        $model->type = STORE_CARD_KH_BINH_BO;
        $aSalePercent = array();
        if(isset($_POST['GasTargetMonthly'])){
            $model->attributes=$_POST['GasTargetMonthly'];
            if($_POST['search_button_click']==1){
                // only search
            }else{
                GasTargetMonthly::saveTarget($model);
            }
        }

        $aSalePercent = $model->searchByMonthYear();    	

        $this->render('index',array(
            'model'=>$model, 
            'aSalePercent'=>$aSalePercent, 
            'actions' => $this->listActionsCanAccess,
        ));
        }catch (Exception $exc){
        GasCheck::CatchAllExeptiong($exc);
        }
    }

    /** @Author: NamNH Dec 10, 2018
     *  @Todo: setup ccs
     **/
    public function actionSetupCcs(){
        $this->pageTitle = 'Thiết lập target CCS';
        try{
            $model=new GasTargetMonthly();
            $model->unsetAttributes();
            $model->monthly = date('m');  // clear any default values
            $model->year = date('Y');  // clear any default values
            $model->type = GasTargetMonthly::TYPE_CCS;
            if(isset($_POST['GasTargetMonthly'])){
                $model->attributes=$_POST['GasTargetMonthly'];
                if($_POST['search_button_click']==1){
                    // only search
                }else{
                    $model->saveTargetCcs();
                }
            }
            $model->getFullDateSearch();
            $aData = $model->getDataCcs();
            $this->render('setupCcs/index',array(
                'model'=>$model, 
                'aData'=>$aData,
                'actions' => $this->listActionsCanAccess,
            ));
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }
    
    /** @Author: NamNH Dec 10, 2018
     *  @Todo: report ccs
     **/
    public function actionCcs(){
        $this->pageTitle = 'Báo cáo target 2019';
        try
        {
        $this->toExcelCcs();
        $model=new GasTargetMonthly();
        $model->unsetAttributes();
        $model->getPointReportCcs = false;
        $model->date_from = "01".date('-m-Y');
        $model->date_to = date('d-m-Y');
        $model->type = GasTargetMonthly::TYPE_CCS;
        $model->type_index=!empty($_GET['type']) ? $_GET['type'] : '';
        $aData = [];
        if(isset($_POST['GasTargetMonthly'])){
            $model->attributes=$_POST['GasTargetMonthly'];
            if($model->type == GasTargetMonthly::TYPE_CCS){
                $aData = $model->getReportCcs();
            }elseif($model->type == GasTargetMonthly::TYPE_GDKV){
                $aData = $model->getReportCvGs();
            }
        }
        $model->convertToDateDb();
        $this->render('report/targetCcs',array(
            'model'=>$model, 
            'aData'=> $aData,
            'actions' => $this->listActionsCanAccess,
        ));
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }
    
    /** @Author: DuongNV Aug
     *  @Todo: report gs chưa đạt chỉ tiêu
     **/
    public function actionCcsStatistic(){
        $this->pageTitle = 'Báo cáo target 2019';
        try
        {
            $model = new GasTargetMonthly();
            $model->unsetAttributes();
            $model->getPointReportCcs = false;
            $model->type        = GasTargetMonthly::TYPE_CCS;
            $model->type_index  = !empty($_GET['type']) ? $_GET['type'] : '';
            $aData              = [];
            if(isset($_POST['GasTargetMonthly'])){
                $model->attributes = $_POST['GasTargetMonthly'];
                if($model->type == GasTargetMonthly::TYPE_CCS){
                    $now                = date('Y-m-04');
                    $firstDatePrevMonth = MyFormat::modifyDays($now, 2, '-', 'month', '01-m-Y');
                    $lastDatePrevMonth  = MyFormat::modifyDays($now, 2, '-', 'month', 't-m-Y');
                    $firstDateCurrMonth = MyFormat::modifyDays($now, 1, '-', 'month', '01-m-Y');
                    $lastDateCurrMonth  = MyFormat::modifyDays($now, 1, '-', 'month', 't-m-Y');
                    $model->date_from   = $firstDatePrevMonth;
                    $model->date_to     = $lastDatePrevMonth;
                    $aData[$model->date_from] = $model->getReportCcs();
                    $model->date_from   = $firstDateCurrMonth;
                    $model->date_to     = $lastDateCurrMonth;
                    $aData[$model->date_from]  = $model->getReportCcs();
                }
            }
            $model->convertToDateDb();
            $this->render('report/targetCcs',array(
                'model'=>$model, 
                'aData'=> $aData,
                'actions' => $this->listActionsCanAccess,
            ));
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }
    
    /** @Author: NamNH Jan 21, 2018
     *  @Todo: report daily ccs 
     **/
    public function actionDailyCcs(){
        $this->pageTitle = 'Báo cáo daily CCS (Point/App/Bqv)';
        try
        {
        $this->toExcelDaily();
        $model=new TargetDaily();
        $model->unsetAttributes();
        $model->date_from = "01".date('-m-Y');
        $model->date_to = date('d-m-Y');
        $aData = [];
        if(isset($_POST['TargetDaily'])){
            $model->attributes=$_POST['TargetDaily'];
            $aData = $model->getReportDailyCcs();
        }
        $this->render('report/dailyCcs',array(
            'model'=>$model, 
            'aData'=> $aData,
            'actions' => $this->listActionsCanAccess,
        ));
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }
    
    /** @Author: NamNH Feb 13, 2019
     *  @Todo: handle export excel of report daily
     **/
    public function toExcelDaily(){
        try{
            if(isset($_GET['ToExcel'])){
                if(isset($_SESSION['data-model-daily'])){
                    ini_set('memory_limit','1500M');// chưa cần chạy đoạn này, đưa memory_limit lên 500 có thể export dc khoảng 5000 row - chưa test
                    ToExcel::summaryExportExcelDaily();
                }
                $this->redirect(array('DailyCcs'));
            }
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }
    
    /** @Author: NamNH Feb 13, 2019
     *  @Todo: handle export excel of report daily
     **/
    public function toExcelCcs(){
        try{
            if(isset($_GET['ToExcel'])){
                if(isset($_SESSION['data-model-ccs'])){
                    ini_set('memory_limit','1500M');// chưa cần chạy đoạn này, đưa memory_limit lên 500 có thể export dc khoảng 5000 row - chưa test
                    ToExcel::summaryExportExcelCcs();
                }
                $this->redirect(array('ccs'));
            }
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }
    
    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer the ID of the model to be loaded
     */
    public function loadModel($id)
    {
        try
        {
        $model=GasTargetMonthly::model()->findByPk($id);
        if($model===null)
        {
            Yii::log("The requested page does not exist.");
            throw new CHttpException(404,'The requested page does not exist.');
        }			
        return $model;
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }
    
}
