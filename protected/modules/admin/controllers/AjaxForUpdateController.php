<?php
/**  @note this class only for write data - only for Call Center actions
 */
class AjaxForUpdateController extends AdminController{
    public function accessRules()
    {
        return array(
            array('allow',   //allow authenticated user to perform actions
                'users'=>array('@'),
            ),
            array('deny',  // deny all users
                    'users'=>array('*'),
            ),
        );
    }

    /**
     * @Author: ANH DUNG Mar 14, 2017
     * @Todo: get info customer by phone_number or customer_id: name + phone + add
     * @note_fix_May_21_20177: Phân loại xử lý cuộc gọi bò mối và hộ GĐ
     * Khi có sự kiện Dialling thì get info customer by phone_number or customer_id: name + phone + add + history
     */
    public function actionCallGetCustomerByPhone(){
        $cRole = MyFormat::getCurrentRoleId();
        switch ($cRole) {
            case ROLE_CALL_CENTER:
            case ROLE_TELESALE:
                $this->handleCallHgd();
                break;
            case ROLE_DIEU_PHOI:
//                $this->handleCallHgd();// for test call
                $this->handleCallBoMoi();
                break;
            default:
                break;
        }
        echo 'Errors actionCallGetCustomerByPhone Chua xy ly cho role user nghe 1900'; die;
    }
    /**
     * @Author: ANH DUNG May 21, 2017
     * @Todo: tách xử lý cuộc gọi hộ GĐ
     * @note_fix_May_21_20177: Phân loại xử lý cuộc gọi bò mối và hộ GĐ
     */
    public function handleCallHgd(){
        $json = ['success'=>false, 'msg'=>''];
        if(isset($_POST['phone_number'])){
            if($_POST['call_temp_id']){// 1, update user login to call
                CallTemp::updateUserIdLogin($_POST['call_temp_id']);
            }
            /** 1. get array model customer by phone
             *  2. format response by html render for colorbox select customer
             */
            $model      = new UsersPhone();
            $aCustomer  = $model->getByPhone($_POST['phone_number'], UsersPhone::WINDOW_TYPE_HGD);
//            $json['CallCurrentTab']      = $_POST['CallCurrentTab'];
            $json['total_record']       = count($aCustomer);
            if($json['total_record'] > 0){
                $json['msg'] = HtmlFormat::callCenterListCustomer($_POST['phone_number'], $aCustomer, $_POST['CallCurrentTab']);
            }
            $json['success'] = true;
        }
        echo CJavaScript::jsonEncode($json);die; 
    }
    /**
     * @Author: ANH DUNG May 21, 2017
     * @Todo: tách xử lý cuộc gọi Bò Mối
     * @note_fix_May_21_20177: Phân loại xử lý cuộc gọi bò mối và hộ GĐ
     */
    public function handleCallBoMoi(){
        $json = ['success'=>false, 'msg'=>''];
        if(isset($_POST['phone_number'])){
            if($_POST['call_temp_id']){// 1, update user login to call
                CallTemp::updateUserIdLogin($_POST['call_temp_id']);
            }
            /** 1. get array model customer by phone
             *  2. format response by html render for colorbox select customer
             */
            $model      = new UsersPhone();
            $aCustomer  = $model->getByPhone($_POST['phone_number'], UsersPhone::WINDOW_TYPE_BO_MOI);
            $mPhoneLog  = new UsersPhoneLog();// tracking phone number
            $mPhoneLog->doAddNew($_POST['phone_number']);

            $json['total_record']       = count($aCustomer);
            if($json['total_record'] > 0){
                $json['msg'] = HtmlFormat::callCenterListCustomer($_POST['phone_number'], $aCustomer, $_POST['CallCurrentTab']);
            }
            $json['success'] = true;
        }
        echo CJavaScript::jsonEncode($json);die; 
    }
    
    /**
     * @Author: ANH DUNG Mar 14, 2017
     * @Todo: get info customer by phone_number or customer_id: name + phone + add
     */
    public function actionCallGetCustomerHistory(){
        try{
            $cRole = MyFormat::getCurrentRoleId();
            switch ($cRole) {
                case ROLE_CALL_CENTER:
                case ROLE_TELESALE:
                    $this->getCustomerHistoryHgd();
                    break;
                case ROLE_DIEU_PHOI:
                case ROLE_ADMIN:
                    $this->getCustomerHistoryBoMoi();
                    break;
                default:
                    break;
            }
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }
    
    /**
     * @Author: ANH DUNG May 21, 2017
     * @Todo: HGD get info customer by phone_number or customer_id: name + phone + add
     */
    public function getCustomerHistoryHgd(){
        $json = ['success'=>false, 'msg'=>''];
        if(isset($_POST['customer_id'])){
            /** 1. get array model customer by phone
             *  2. format response by html render for colorbox select customer
             */
            $mHtmlFormat                = new HtmlFormat();
            $mHtmlFormat->customer_id   = $_POST['customer_id'];;
            $mCustomer = Users::model()->findByPk($_POST['customer_id']);
            $mSell = new Sell();
            $mSell->customer_id     = $_POST['customer_id'];
            $aModelSell             = $mSell->windowGetHistory(['GetModel'=>1]);
            $json['msg']            = $mHtmlFormat->callCenterCustomerHistory($aModelSell, $_POST['CallCurrentTab']);
            $json['success']        = true;   
            $json['IsBoMoi']        = false;
//            $json['customer_info']  = HandleArrayResponse::handleInfoCustomer($mCustomer, null, null);
            $json['customer_info']['customer_delivery_agent_id']    = $mCustomer->area_code_id;
            $json['customer_info']['customer_delivery_agent']       = CacheSession::getAgentName($mCustomer->area_code_id);
        }
        echo CJavaScript::jsonEncode($json);die; 
    }
    /**
     * @Author: ANH DUNG May 21, 2017
     * @Todo: Bo Moi get info customer by phone_number or customer_id: name + phone + add
     */
    public function getCustomerHistoryBoMoi(){
        $json = ['success'=>false, 'msg'=>''];
        if(isset($_POST['customer_id'])){
            /** 1. get array model customer by phone
             *  2. format response by html render for colorbox select customer
             */
            $mHtmlFormat                = new HtmlFormat();
            $mAppOrder                  = new GasAppOrder();
            $mAppOrder->customer_id     = $_POST['customer_id'];
            $aAppOrder                  = $mAppOrder->windowGetHistory();
//            $modelsStorecard            = $mStorecard->windowGetHistory(['GetModel'=>1]);
//            $json['msg']                = HtmlFormat::callCenterCustomerHistoryBoMoi($modelsStorecard, $_POST['CallCurrentTab']);
            $json['msg']                = $mHtmlFormat->callCenterCustomerHistoryBoMoiV1($aAppOrder, $_POST['CallCurrentTab']);
            $json['success']            = true;   
            $json['IsBoMoi']            = true;   
            // bổ sung luôn thông tin chi tiết cho customer ở request này
            $mCustomer  = $mAppOrder->rCustomer;
            $mUserRef   = $mCustomer->rUsersRef;
            $mSale      = $mCustomer->sale;
            $json['customer_info']      = HandleArrayResponse::handleInfoCustomer($mCustomer, $mUserRef, $mSale);
        }
        echo CJavaScript::jsonEncode($json);die; 
    }
    
    /**
     * @Author: ANH DUNG Mar 15, 2017
     * @Todo: update add phone customer
     */
    public function actionCallUpdateCustomerPhone(){
        $cRole = MyFormat::getCurrentRoleId();
        switch ($cRole) {
            case ROLE_CALL_CENTER:
                $this->callUpdateCustomerPhoneHgd();
                break;
            case ROLE_DIEU_PHOI:
                $this->callUpdateCustomerPhoneBoMoi();
                break;
            default:
                break;
        }
    }
    
    /**
     * @Author: ANH DUNG May 21, 2017
     * @Todo: update add phone customer hgd
     */
    public function callUpdateCustomerPhoneHgd(){
        $json = ['success'=>false, 'msg'=>'Không thể cập nhật, có lỗi xảy ra', 'phone'=>''];
        try{
        if(isset($_POST['customer_id'])){
            /** 1. get array model customer by phone
             *  2. format response by html render for colorbox select customer
             */
            $mCustomer = Users::model()->findByPk($_POST['customer_id']);
            if($mCustomer && in_array($mCustomer->is_maintain, CmsFormatter::$aTypeIdHgd)){
                if(!empty($mCustomer->phone)){
                    $tmpPhone = explode('-', $mCustomer->phone);
                    if(in_array($_POST['phone'], $tmpPhone)){
                        throw new Exception('Số điện thoại đã tồn tại, không thể cập nhật');
                    }
                }
                if($mCustomer->id == GasConst::UID_VANG_LAI){// Aug 
                    throw new Exception('Vui lòng không cập nhật số điện thoại cho KH vãng lai');
                }
                $mCustomer->phone .= '-'.$_POST['phone'];
                $mCustomer->update(['phone', 'address_vi']);
                $mCustomer->solrAdd();
                UsersPhone::savePhone($mCustomer);
                $json['phone']  = $mCustomer->phone;
                $json['msg']    = 'Cập nhật số điện thoại '.$_POST['phone'].' thành công';
                $json['success'] = true;   
            }
        }
        }catch (Exception $exc){
            $json['msg'] = $exc->getMessage();
        }
//        $this->formatNotify($json);// khong su dung dc o ham nay
        echo CJavaScript::jsonEncode($json);die; 
    }
    
    /** @Author: ANH DUNG Jul 11, 2017
     * @Todo: định dạng lại html div notify
     */
    public function formatNotify(&$json) {
        if($json['success']){
            $json['msg'] = MyFormat::renderNotifySuccess($json['msg']);
        }else{
            $json['msg'] = MyFormat::renderNotifyError($json['msg']);
        }
    }
    
    /**
     * @Author: ANH DUNG May 21, 2017
     * @Todo: update add phone customer hgd
     */
    public function callUpdateCustomerPhoneBoMoi(){
        $json = ['success'=>false, 'msg'=>'Không thể cập nhật, có lỗi xảy ra', 'phone'=>''];
        try{
        if(isset($_POST['customer_id'])){
            /** 1. get array model customer by phone
             *  2. format response by html render for colorbox select customer
             */
            $mCustomer = Users::model()->findByPk($_POST['customer_id']);
            if($mCustomer && !in_array($mCustomer->is_maintain, CmsFormatter::$aTypeIdHgd)){
                if(!empty($mCustomer->phone)){
                    $tmpPhone = explode('-', $mCustomer->phone);
                    if(in_array($_POST['phone'], $tmpPhone)){
                        throw new Exception('Số điện thoại đã tồn tại, không thể cập nhật');
                    }
                }
                if($mCustomer->id == GasConst::UID_VANG_LAI){// Aug 
                    throw new Exception('Vui lòng không cập nhật số điện thoại cho KH vãng lai');
                }
                $mCustomer->phone .= '-'.$_POST['phone'];
                $mCustomer->update(['phone', 'address_vi']);
                $mCustomer->solrAdd();
                UsersPhone::savePhone($mCustomer);
                $json['phone']  = $mCustomer->phone;
                $json['msg']    = 'Cập nhật số điện thoại '.$_POST['phone'].' thành công';
                $json['success'] = true;   
            }
        }
        }catch (Exception $exc){
            $json['msg'] = $exc->getMessage();
        }
        echo CJavaScript::jsonEncode($json);die; 
    }
    
    /**
     * @Author: ANH DUNG Mar 15, 2017
     * @Todo: update failed call
     */
    public function actionCallFailedUpdate(){
        $json = ['success'=>false, 'msg'=>'Không có cuộc gọi, không thể cập nhật, có lỗi xảy ra'];
        try{
        if(isset($_POST['call_uuid'])){
            /** 1. get array model customer by phone
             *  2. format response by html render for colorbox select customer
             */
            if(empty($_POST['call_uuid'])){
                throw new Exception('Không có cuộc gọi, dữ liệu không hợp lệ');
            }
            $mCall = new Call();
            $mCall->call_uuid = $_POST['call_uuid'];
            $mCall = $mCall->getByCallUuid();
            if(is_null($mCall)){
                throw new Exception('call_uuid sai, dữ liệu không hợp lệ, không thể cập nhật');
            }
            $mCall->failed_status   = $_POST['status'];
            $mCall->failed_note     = $_POST['note'];
            if(empty($mCall->agent_id)){
                $mCall->agent_id    = isset($_POST['agent_id']) ? $_POST['agent_id'] : 0;
            }
            $mCall->update(['failed_status', 'failed_note', 'agent_id']);
            $json['msg']        = 'Cập nhật thành công';
            $json['success']    = true;   
        }
        }catch (Exception $exc){
            $json['msg'] = $exc->getMessage();
        }
        $this->formatNotify($json);
        echo CJavaScript::jsonEncode($json);die; 
    }
    
    /**
     * @Author: ANH DUNG Mar 16, 2017
     * @Todo: update customer id for record call_uuid HangUp 
     */
    public function actionCallUpdateCustomerId(){
        $json = ['success'=>false, 'msg'=>'failed'];
        try{
        if(isset($_POST['call_uuid'])){
            /** 1. get array model customer by phone
             *  2. format response by html render for colorbox select customer
             */
            $mCallTemp = new CallTemp();
            $mCallTemp->call_uuid   = $_POST['call_uuid'];
            $mCallTemp->customer_id = $_POST['customer_id'];
            $mCallTemp->updateCustomerId();
            
            $json['msg']        = 'ok';
            $json['success']    = true;   
        }
        }catch (Exception $exc){
            $json['msg'] = $exc->getMessage();
        }
        echo CJavaScript::jsonEncode($json);die; 
    }
    
    /**
     * @Author: ANH DUNG May 23, 2017
     * @Todo: render form create đơn hàng bò mối
     */
    public function actionMakeOrderBoMoiFromRecording($customer_id, $phone_number) {
        try{
            $this->layout='ajax';
            $mCustomer = Users::model()->findByPk($customer_id);
            $this->render('BoMoi/OrderBoMoi',array(
                'mCustomer'=>$mCustomer,
                'phone_number'=>$phone_number,
                'actions' => $this->listActionsCanAccess,
            ));
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }
    
    /** * @Author: ANH DUNG May 22, 2017
     * @Todo: Call Center bò mối make order bò mối + đơn hàng xe tải
     */
    public function actionMakeOrderBoMoi() {
        $json = ['success'=>true, 'msg'=>'ok'];
        $cUid = MyFormat::getCurrentUid();
        $mUser = CacheSession::getModelUser($cUid);
//        Logger::WriteLog(json_encode($_POST));
        try{
            $q          = $this->formatParams();
            $mStoreCard = new GasStoreCard();
            $mStoreCard->mAppUserLogin  = $mUser;
            $mStoreCard->customer_id    = $q->customer_id;
            switch ($_POST['order_type']){
                case GasAppOrder::DELIVERY_NOW:
                case GasAppOrder::DELIVERY_THU_VO:
                    $mStoreCard->callCenterCreate($q, $json);
                    break;
                case GasAppOrder::DELIVERY_BY_CAR:
                    $mStoreCard->checkChanHang();
                    $error  = $mStoreCard->getError('customer_id').'';
                    if(!empty($error)){
                        throw new Exception($error);
                    }
                    $mOrderCar = new GasOrders();
                    $mOrderCar->mAppUserLogin   = $mUser;
                    $mOrderCar->callCenterOrderCar($q, $json);
                    break;
                default:
                    break;
            }
            /* 1. check số phone KH đặt app đc lưu vào: appOrder, order detail với KH xe tải
             * 2. check số phone put lên từ CallCenter dc lưu vào appOrder, order detail không?
             * 
             */
            
        }catch (Exception $exc){
            $json['msg'] = $exc->getMessage();
            $json['success'] = false;
        }
        if($json['success']){
            Yii::app()->user->setFlash( MyFormat::SUCCESS_UPDATE, $json['msg']);
            $json['html'] = MyFormat::BindNotifyMsg();
        }else{
            Yii::app()->user->setFlash( MyFormat::ERROR_UPDATE, $json['msg']);
            $json['html'] = MyFormat::BindNotifyMsg();
        }
        
        echo CJavaScript::jsonEncode($json);die; 
    }
    
    /** @Author: ANH DUNG May 19, 2017
     * @Todo: định dạng lại biến post đưa vào biến $q giống như bên api
     */
    public function formatParams() {
        if(!isset($_POST['customer_id'])){
            throw new Exception('Chưa chọn khách hàng');
        }
        $q = new stdClass();
        $q->customer_id         = $_POST['customer_id'];
        $q->agent_id            = $_POST['agent_id'];
        $q->user_id_executive   = $_POST['user_id_executive'];
        $q->note                = $_POST['note'];
        $q->date_delivery       = $_POST['date_delivery'];
        $q->b50                 = $_POST['b50'];
        $q->b45                 = $_POST['b45'];
        $q->b12                 = $_POST['b12'];
        $q->b6                  = $_POST['b6'];
        $q->phone_number        = $_POST['phone_number'];
        $q->type                = $_POST['order_type'];
        $q->pay_direct          = $_POST['pay_direct'];
        $q->delivery_timer      = str_replace('-', '/', $_POST['delivery_timer']);
        $q->bo_moi_other_item   = $_POST['bo_moi_other_item'];// DungNT Sep0219 add for make order: Cồn, bếp, van dây
        return $q;
    }
    
    /**
     * @Author: ANH DUNG Now 01, 2017
     * 1. xử lý ajax để reset, khi xong thì alert và remove icon
     * 2. xử lý reset, ghi log lại user nào reset
     */
    public function actionResetPassCustomer() {
        $json = ['success'=>true, 'msg'=>'Cập nhật lại mật khẩu mặc định cho khách hàng thành công'];
        $cUid = MyFormat::getCurrentUid();
        try{
            $id = isset($_GET['id']) ? $_GET['id'] : 0;
            $mCustomer = Users::model()->findByPk($id);
            if(is_null($mCustomer) || $mCustomer->role_id != ROLE_CUSTOMER || !$mCustomer->canResetPass() || !in_array($mCustomer->is_maintain, CmsFormatter::$aTypeIdMakeUsername)){
                throw new Exception('Bạn không có quyền đặt lại mật khẩu cho khách hàng');
            }
            $mCustomer->passwordType = GasConst::PASS_TYPE_1;
            $mCustomer->doResetPassword();
            $mCustomer->makeRecordChangePass();
            TransactionEvent::resetPassCustomer($mCustomer);
        }catch (Exception $exc){
            $json['msg'] = $exc->getMessage();
            $json['success'] = false;
        }
        
        echo CJavaScript::jsonEncode($json);die; 
    }
    
    /**
     * @Author: ANH DUNG Dec 04, 2017
     * 1. xử lý ajax để put event Remove row notify Order App Gas24h when one user has confirm
     */
    public function actionRemoveRowOrderGas24h($transaction_history_id) {
        if(empty($transaction_history_id)){
            die;
        }
        $mTransactionHistory = new TransactionHistory();
        $mTransactionHistory->id = $transaction_history_id;
        $mTransactionHistory->makeNotifyConfirm(); 
        die; 
    }
    
}