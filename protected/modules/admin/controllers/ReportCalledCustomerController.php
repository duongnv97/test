<?php

class ReportCalledCustomerController extends AdminController 
{
    public $pluralTitle = 'Báo cáo khách hàng đã gọi';
    public $singleTitle = 'Báo cáo khách hàng đã gọi';
    
    public function actionIndex()
    {
        $this->pageTitle = 'Danh Sách ';
        try{
        $model=new FollowCustomer();
        $model->unsetAttributes();  // clear any default values
        if(isset($_GET['FollowCustomer']))
                $model->attributes=$_GET['FollowCustomer'];

        $this->render('index',array(
                'model'=>$model, 'actions' => $this->listActionsCanAccess,
        ));
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
    * If the data model is not found, an HTTP exception will be raised.
     * @param integer the ID of the model to be loaded
     */
    public function loadModel($id)
    {
        try{
        $model=FollowCustomer::model()->findByPk($id);
        if($model===null)
        {
            Yii::log("The requested page does not exist.");
            throw new CHttpException(404,'The requested page does not exist.');
        }			
        return $model;
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }

}
