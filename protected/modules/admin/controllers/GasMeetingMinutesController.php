<?php

class GasMeetingMinutesController extends AdminController 
{
    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id)
    {
        $this->pageTitle = 'Xem ';
        try{
            $model = $this->loadModel($id);
            $model->date_published = MyFormat::dateConverYmdToDmy($model->date_published);
            $mComment = new GasMeetingMinutesComment();
            $mComment->meeting_minutes_id = $model->id;
            if( $model->type == GasMeetingMinutes::TYPE_PHAP_LY ){
                $this->redirect(array('index'));
            }
            $this->render('view_comment/view',array(
                'model'=> $model, 
                'mComment'=> $mComment, 
                'actions' => $this->listActionsCanAccess,
            ));
        }
        catch (Exception $exc)
        {
            GasCheck::CatchAllExeptiong($exc);
        }

    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate()
    {
        $this->pageTitle = 'Tạo Mới ';
        try
        {                
            $model=new GasMeetingMinutes('create');
            $model->date_published = date('d/m/Y');
            if(isset($_POST['GasMeetingMinutes']))
            {
                $model->attributes=$_POST['GasMeetingMinutes'];
                $model->validate();
                if(!$model->hasErrors()){
                    $model->save();
                    Yii::app()->user->setFlash('successUpdate', "Thêm mới thành công.");
                   $this->redirect(array('update','id'=>$model->id));
                }				
            }

            $this->render('create',array(
                    'model'=>$model, 'actions' => $this->listActionsCanAccess,
            ));
            }
            catch (Exception $exc)
            {
                GasCheck::CatchAllExeptiong($exc);
            }
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id)
    {
        $this->pageTitle = 'Cập Nhật ';
        try
        {
            $model=$this->loadModel($id);
            $model->scenario = 'update';
            $model->date_published = MyFormat::dateConverYmdToDmy($model->date_published);
            if(!$model->canUpdate() || $model->type == GasMeetingMinutes::TYPE_PHAP_LY){
                $this->redirect(array('index'));
            }
            
            if(isset($_POST['GasMeetingMinutes']))
            {
                $model->attributes=$_POST['GasMeetingMinutes'];
                        // $this->redirect(array('view','id'=>$model->id));
                $model->validate();
                if(!$model->hasErrors()){
                    $model->save();
                    if(isset($_GET['ajax_auto_save'])){die;}
                    Yii::app()->user->setFlash('successUpdate', "Cập nhật thành công.");
                    $this->redirect(array('update','id'=>$model->id));
                }
            }

            $this->render('update',array(
                    'model'=>$model, 'actions' => $this->listActionsCanAccess,
            ));
            }
            catch (Exception $exc)
            {
                GasCheck::CatchAllExeptiong($exc);
            }
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id)
    {
            try
            {
            if(Yii::app()->request->isPostRequest)
            {
                    // we only allow deletion via POST request
                    if($model = $this->loadModel($id))
                    {
                        if($model->delete())
                            Yii::log("Uid: " .Yii::app()->user->id. " Delete record ".  print_r($model->attributes, true), 'info');
                    }

                    // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
                    if(!isset($_GET['ajax']))
                            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('index'));
            }
            else
            {
                Yii::log("Uid: " .Yii::app()->user->id. " Invalid request. Please do not repeat this request again.");
                throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
            }	
            }
            catch (Exception $exc)
            {
                GasCheck::CatchAllExeptiong($exc);
            }
    }

    /**
     * Manages all models.
     */
    public function actionIndex()
    {
            $this->pageTitle = 'Danh Sách ';
            try
            {
            $model=new GasMeetingMinutes('search');
            $model->unsetAttributes();  // clear any default values            
            if(isset($_GET['GasMeetingMinutes']))
                $model->attributes=$_GET['GasMeetingMinutes'];

            $this->render('index',array(
                    'model'=>$model, 'actions' => $this->listActionsCanAccess,
            ));
            }
            catch (Exception $exc)
            {
                GasCheck::CatchAllExeptiong($exc);
            }
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
    * If the data model is not found, an HTTP exception will be raised.
     * @param integer the ID of the model to be loaded
     */
    public function loadModel($id)
    {
            try
            {
            $model=GasMeetingMinutes::model()->findByPk($id);
            if($model===null)
            {
                Yii::log("The requested page does not exist.");
                throw new CHttpException(404,'The requested page does not exist.');
            }			
            return $model;
            }
            catch (Exception $exc)
            {
                GasCheck::CatchAllExeptiong($exc);
            }
    }

    /**
     * Performs the AJAX validation.
     * @param CModel the model to be validated
     */
    protected function performAjaxValidation($model)
    {
            try
            {
            if(isset($_POST['ajax']) && $_POST['ajax']==='gas-meeting-minutes-form')
            {
                    echo CActiveForm::validate($model);
                    Yii::app()->end();
            }
            }
            catch (Exception $exc)
            {
                GasCheck::CatchAllExeptiong($exc);
            }
    }
    
    /**
     * @Author: ANH DUNG Nov 07, 2014
     * @Todo: something
     * @Param: $model
     */
    public function actionText_post_comment($id)
    {
        $this->pageTitle = 'Post Comment Văn Bản ';
        try
        {
            $model = $this->loadModel($id);
            $msg = "Có Lỗi không thể Gửi ".GasMeetingMinutes::TEXT_HELP." ( invalid message ).";
            $FlashScenario = MyFormat::ERROR_UPDATE;            
//            if(isset($_POST['GasMeetingMinutes']) && $model->status == STATUS_ACTIVE && !$model->stop_comment)
            if( isset($_POST['GasMeetingMinutes']) )
            {
                $model->attributes=$_POST['GasMeetingMinutes'];
                $model->scenario = 'post_comment';
                $model->validate();
                if(!$model->hasErrors()){
                   $msg = "Gửi ".GasMeetingMinutes::TEXT_HELP." thành công.";
                   $FlashScenario = MyFormat::SUCCESS_UPDATE;
                   if(!GasTickets::UserCanPostTicket()) { // dùng chung kiểm tra với ticket (ngại tách, mà thấy cũng ko thể post vượt 200 message 1 ngày dc )
                       $msg = "Có Lỗi không thể Gửi Bình Luận vượt quá giới hạn cho phép (exceed limit), liên hệ với admin quản trị.";
                       $FlashScenario = 'ErrorUpdate';
                   }else{
                       GasMeetingMinutesComment::SaveOneMessageDetail($model);
                   }
                }
            }
            
            Yii::app()->user->setFlash($FlashScenario, $msg);
            $this->redirect(array('view', 'id'=>$id));
        }
        catch (Exception $exc)
        {
            Yii::log("Uid: " .Yii::app()->user->id. " Exception ".  $exc->getMessage(), 'error');
            $code = 404;
            if(isset($exc->statusCode))
                $code=$exc->statusCode;
            if($exc->getCode())
                $code=$exc->getCode();
            throw new CHttpException($code, $exc->getMessage());
        }
    }    
    
    public function actionText_delete_comment($id)
    {
        try
        {
            $model = MyFormat::loadModelByClass($id, 'GasMeetingMinutesComment');
            $text_id = '';
            $msg = "Có Lỗi không thể Xóa Bình Luận.";
            $FlashScenario = 'ErrorUpdate';
            if($model){
                $text_id = $model->meeting_minutes_id;
                $model->delete();
                $msg = "Xóa Bình Luận Thành Công.";
                $FlashScenario = 'successUpdate';
            }
            Yii::app()->user->setFlash($FlashScenario, $msg);
            $this->redirect(array('view', 'id'=>$text_id));
        }
        catch (Exception $exc)
        {
            Yii::log("Uid: " .Yii::app()->user->id. " Exception ".  $exc->getMessage(), 'error');
            $code = 404;
            if(isset($exc->statusCode))
                $code=$exc->statusCode;
            if($exc->getCode())
                $code=$exc->getCode();
            throw new CHttpException($code, $exc->getMessage());
        }
    }
    
}
