<?php

class GasWorkScheduleController extends AdminController 
{
    public $pluralTitle = "Lịch Điều Phối";
    public $singleTitle = "Lịch Điều Phối";
    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id)
    {
        $this->pageTitle = 'Xem ';
        try{
        $this->render('view',array(
                'model'=>$this->loadModel($id), 'actions' => $this->listActionsCanAccess,
        ));
        }
        catch (Exception $exc)
        {
            GasCheck::CatchAllExeptiong($exc);
        }
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate()
    {
        $this->pageTitle = 'Tạo Mới ';
        try
        {
        $cRole = Yii::app()->user->role_id;
        if($cRole != ROLE_ADMIN){
            $this->redirect(array('index'));
        }
        $model=new GasWorkSchedule('create');
        if(isset($_POST['GasWorkSchedule']))
        {
//                $assign = $model->getArrEmployeeAssignToSlot();
//                echo '<pre>';
//                print_r($model->shuffle_assoc($assign));
//                echo '</pre>';
//                die;
                $model->attributes=$_POST['GasWorkSchedule'];
                $numberWeekOfYear = MyFormat::getIsoWeeksInYear($model->year);
                if($model->weekNo > $numberWeekOfYear){
                    $model->weekNo = $numberWeekOfYear;
                }
                $aDateWeekBegin = MyFormat::dateByWeekNumber($model->year, $model->weekNo);
                $aDateWeekEnd = MyFormat::dateByWeekNumber($model->year, $numberWeekOfYear);
                $model->deleteByWeekAndYear($aDateWeekBegin['date_from'], $aDateWeekEnd['date_to']);
                $aRowInsert = array();
                $aUserOffTwoDay = array(GasTickets::DIEU_PHOI_CHUONG, GasTickets::DIEU_PHOI_TRANG);
                for($i = $model->weekNo; $i<=$numberWeekOfYear; $i++ ){
                    $model->buildForOneWeek($i, $aRowInsert, $aUserOffTwoDay);
                }
                $model->saveOneYear($aRowInsert);

//                $model->validate();
//                if(!$model->hasErrors()){
//                    $model->save();
//                    Yii::app()->user->setFlash( MyFormat::SUCCESS_UPDATE, "Thêm mới thành công." );
//                    $this->redirect(array('create'));
//                }
            }

            $this->render('create',array(
                    'model'=>$model, 'actions' => $this->listActionsCanAccess,
            ));
            }
            catch (Exception $exc)
            {
                GasCheck::CatchAllExeptiong($exc);
            }
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id)
    { die;
        $this->pageTitle = 'Cập Nhật ';
        try
        {
        $model=$this->loadModel($id);
        $model->scenario = 'update';
        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if(isset($_POST['GasWorkSchedule']))
        {
            $model->attributes=$_POST['GasWorkSchedule'];
            $model->validate();
            if(!$model->hasErrors()){
                $model->save();
                Yii::app()->user->setFlash( MyFormat::SUCCESS_UPDATE, "Cập nhật thành công." );
                $this->redirect(array('update','id'=>$model->id));
            }
        }

        $this->render('update',array(
                'model'=>$model, 'actions' => $this->listActionsCanAccess,
        ));
        }
        catch (Exception $exc)
        {
            GasCheck::CatchAllExeptiong($exc);
        }
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id)
    { die;
        try
        {
        if(Yii::app()->request->isPostRequest)
        {
            // we only allow deletion via POST request
            if($model = $this->loadModel($id))
            {
                if($model->delete())
                    Yii::log("Uid: " .Yii::app()->user->id. " Delete record ".  print_r($model->attributes, true), 'info');
            }

            // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
            if(!isset($_GET['ajax']))
                    $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('index'));
        }
        else
        {
            Yii::log("Uid: " .Yii::app()->user->id. " Invalid request. Please do not repeat this request again.");
            throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
        }	
        }
        catch (Exception $exc)
        {
            GasCheck::CatchAllExeptiong($exc);
        }
    }

    /**
     * Manages all models.
     */
    public function actionIndex()
    {
        $this->pageTitle = 'Danh Sách ';
        try
        {
        $model=new GasWorkSchedule('search');        
        $model->unsetAttributes();  // clear any default values
        $model->year = date("Y");
        if(isset($_GET['GasWorkSchedule']))
            $model->attributes=$_GET['GasWorkSchedule'];

        $this->render('index',array(
                'model'=>$model, 'actions' => $this->listActionsCanAccess,
        ));
        }
        catch (Exception $exc)
        {
            GasCheck::CatchAllExeptiong($exc);
        }
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
    * If the data model is not found, an HTTP exception will be raised.
     * @param integer the ID of the model to be loaded
     */
    public function loadModel($id)
    {
        try
        {
        $model=GasWorkSchedule::model()->findByPk($id);
        if($model===null)
        {
            Yii::log("The requested page does not exist.");
            throw new CHttpException(404,'The requested page does not exist.');
        }			
        return $model;
        }
        catch (Exception $exc)
        {
            GasCheck::CatchAllExeptiong($exc);
        }
    }

    /**
     * Performs the AJAX validation.
     * @param CModel the model to be validated
     */
    protected function performAjaxValidation($model)
    {
        try
        {
        if(isset($_POST['ajax']) && $_POST['ajax']==='gas-work-schedule-form')
        {
                echo CActiveForm::validate($model);
                Yii::app()->end();
        }
        }
        catch (Exception $exc)
        {
            GasCheck::CatchAllExeptiong($exc);
        }
    }
    
    /**
     * @Author: ANH DUNG May 01, 2016
     * @Todo: handle change schedule slot of employee
     */
    public function actionChangeSlot()
    {
        $this->layout='ajax';
        $this->pageTitle = 'Đổi lịch điều phối';
        try{
            $model=new GasWorkSchedule('ChangeSlot');
            if(isset($_POST['GasWorkSchedule'])){
                $model->attributes=$_POST['GasWorkSchedule'];
                $model->validate();
                if(!$model->hasErrors()){
                    $ok = $model->saveChangeSlot();
                    if($ok){
                        MyFormat::setNotifyMessage(MyFormat::SUCCESS_UPDATE, "Đổi lịch thành công");
                        die('<script type="text/javascript">parent.location.reload();</script>'); 
                    }
                }
            }
            
            $this->render('ChangeSlot/ChangeSlot',array(
                    'model'=>$model, 'actions' => $this->listActionsCanAccess,
            ));
        }
        catch (Exception $exc)
        {
            GasCheck::CatchAllExeptiong($exc);
        }
    }
}
