<?php

class UsersEmailController extends AdminController 
{
    public $pluralTitle = 'Email khách hàng';
    public $singleTitle = 'Email khách hàng';
    public $pluralTitleFileSend = 'Bảng kê đã gửi';
    public $singleTitleFileSend  = 'Bảng kê đã gửi';
    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id)
    {
        $this->pageTitle = 'Xem ';
        try{
        $this->render('view',array(
                'model'=>$this->loadModel($id), 'actions' => $this->listActionsCanAccess,
        ));
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate()
    {
        $this->pageTitle = 'Tạo Mới ';
        try{
        $model=new UsersEmail('create');
        if(isset($_POST['UsersEmail']))
        {
            unset($_SESSION['aListEMail']);
            $model->attributes = $_POST['UsersEmail'];
            $model->validate($model->attributes);
            $model->attributes =  $model->getDataValidate();
            $model->checkBug();
            if(!$model->hasErrors()){
                $model->makeUsersEmail();
                Yii::app()->user->setFlash( MyFormat::SUCCESS_UPDATE, 'Thêm mới thành công.');
                $this->redirect(array('create'));
            } else {
                $model->buildArrayUpdate();
            }
            Yii::app()->user->setFlash( MyFormat::ERROR_UPDATE, HandleLabel::FortmatErrorsModel($model->getErrors()));

        }

        $this->render('create',array(
                'model'=>$model, 'actions' => $this->listActionsCanAccess,
        ));
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id)
    {
        $this->pageTitle = 'Cập Nhật ';
        try{
        $model=$this->loadModel($id);
        $model->scenario = 'update';
        if(isset($_POST['UsersEmail']))
        {
            $model->attributes=$_POST['UsersEmail'];
            $model->validate();
            $model->checkEmail($model->list_email);
            if(!$model->hasErrors()){
                $model->update();
                Yii::app()->user->setFlash( MyFormat::SUCCESS_UPDATE, "Cập nhật thành công." );
                $this->redirect(array('update','id'=>$model->id));
            }							
        }

        $this->render('update',array(
                'model'=>$model, 'actions' => $this->listActionsCanAccess,
        ));
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id)
    {
        try{
        if(Yii::app()->request->isPostRequest){
            // we only allow deletion via POST request
            if($model = $this->loadModel($id))
            {
                if($model->delete())
                    Yii::log("Uid: " .Yii::app()->user->id. " Delete record ".  print_r($model->attributes, true), 'info');
            }

            // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
            if(!isset($_GET['ajax']))
                    $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('index'));
        }
        else
        {
            Yii::log("Uid: " .Yii::app()->user->id. " Invalid request. Please do not repeat this request again.");
            throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
        }	
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }

    /**
     * Manages all models.
     */
    public function actionIndex()
    {
        $this->pageTitle = 'Email khách hàng';
        try{
        $model=new UsersEmail('search');
        $model->unsetAttributes();  // clear any default values
        if(isset($_GET['UsersEmail']))
            $model->attributes = $_GET['UsersEmail'];

        $this->render('index',array(
                'model'=>$model, 'actions' => $this->listActionsCanAccess,
        ));
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
    * If the data model is not found, an HTTP exception will be raised.
     * @param integer the ID of the model to be loaded
     */
    public function loadModel($id)
    {
        try{
        $model=UsersEmail::model()->findByPk($id);
        if($model===null)
        {
            Yii::log("The requested page does not exist.");
            throw new CHttpException(404,'The requested page does not exist.');
        }			
        return $model;
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }
    /** @Author: NGUYEN KHANH TOAN 27 Aug 2018
     *  @Todo: Xoa FileSend
     *  @Param:
     **/
//    public function actionFileSendDelete($id)
//    {
//        try{
//        if($id){
//            // we only allow deletion via POST request
//            if($model = $this->loadModelFileSend($id))
//            {
//                if($model->delete())
//                    Yii::log("Uid: " .Yii::app()->user->id. " Delete record ".  print_r($model->attributes, true), 'info');
//            }
//            // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
//            if(!isset($_GET['ajax']))
//                    $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('FileSendIndex'));
//        }
//        else
//        {
//            Yii::log("Uid: " .Yii::app()->user->id. " Invalid request. Please do not repeat this request again.");
//            throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
//        }	
//        }catch (Exception $exc){
//            GasCheck::CatchAllExeptiong($exc);
//        }
//    }

    /**
     * Manages all models.
     */
    public function actionFileSendIndex()
    {
        $this->pageTitle = 'Bảng kê đã gửi email';
        try{
        $model=new FileSend('search');
        $model->unsetAttributes();  // clear any default values
        if(isset($_GET['FileSend']))
                $model->attributes=$_GET['FileSend'];
        $this->render('fileSend/index',array(
                'model'=>$model, 'actions' => $this->listActionsCanAccess,
        ));
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
    * If the data model is not found, an HTTP exception will be raised.
     * @param integer the ID of the model to be loaded
     */
    public function loadModelFileSend($id)
    {
        try{
        $model=FileSend::model()->findByPk($id);
        if($model===null)
        {
            Yii::log("The requested page does not exist.");
            throw new CHttpException(404,'The requested page does not exist.');
        }			
        return $model;
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }
    
    /**
     * Displays a particular model. 
     * @param integer $id the ID of the model to be displayed
     */
    public function actionFileSendView($id)
    {
        $this->pageTitle = 'Xem ';
        try{
        $this->render('fileSend/view',array(
                'model'=>$this->loadModelFileSend($id), 'actions' => $this->listActionsCanAccess,
        ));
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }
    
    /** @Author: KHANH TOAN  Mar 29 2019
     *  @Todo:list date of birth
     *  @Param:
     **/
    public function actionDobList()
    {
        $this->pageTitle = 'Sinh nhật khách hàng';
        try{
        $model=new UsersEmail('search');
        $model->unsetAttributes();  // clear any default values
        if(isset($_GET['UsersEmail']))
            $model->attributes = $_GET['UsersEmail'];

        $this->render('dob/index',array(
                'model'=>$model, 'actions' => $this->listActionsCanAccess,
        ));
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }

      /** @Author: KHANH TOAN  Mar 29 2019
     *  @Todo:list date of birth
     *  @Param:
     **/
   public function actionDobAdd() {
       $this->pageTitle = 'Tạo Mới ';
        try{
        $model=new UsersEmail('dobAdd');
        if(isset($_POST['UsersEmail']))
        {
            $model->attributes = $_POST['UsersEmail'];
            $model->type = UsersEmail::TYPE_CUSTOMER_DOB;
            $model->setCreatedBy();
            $model->checkPhone();
            $model->validate();
            $model->validateListEmail();
            if(!$model->hasErrors()){
                $model->formatDataForDobAdd();
                $model->save();
                Yii::app()->user->setFlash( MyFormat::SUCCESS_UPDATE, 'Thêm mới thành công.');
                $this->redirect(array('dobAdd'));
            }
        }
        $this->render('dob/create',array(
                'model'=>$model, 'actions' => $this->listActionsCanAccess,
        ));
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
   }
    
    /** @Author: KHANH TOAN  Mar 29 2019
     *  @Todo:list date of birth
     *  @Param:
     **/
   public function actionDobUpdate($id) {
        $this->pageTitle = 'Cập Nhật ';
        try{
        $model=$this->loadModel($id);
        if(!$model->canUpdateDob()){
            $this->redirect(array('dobList'));
        }
        $model->formatDataBeforeUpdate();
        $model->scenario = 'dobUpdate';
        if(isset($_POST['UsersEmail']))
        {
            $model->attributes = $_POST['UsersEmail'];
            $model->checkPhone();
            $model->formatDataForDobAdd();
            $model->validate();
            $model->validateListEmail();
            if(!$model->hasErrors()){
                $model->update();
                Yii::app()->user->setFlash( MyFormat::SUCCESS_UPDATE, 'Cập nhật thành công.');
                $this->redirect(array('dobUpdate','id'=>$model->id));
            }
        }

        $this->render('dob/update',array(
                'model'=>$model, 'actions' => $this->listActionsCanAccess,
        ));
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
   }
   
    
}
