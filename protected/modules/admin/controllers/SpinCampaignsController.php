<?php

class SpinCampaignsController extends AdminController 
{
    public $pluralTitle = 'Chiến Dịch';
    public $singleTitle = 'Chiến Dịch';
    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id)
    {
        $this->pageTitle = 'Thông Tin Chiến Dịch';
        try{
        $this->render('view',array(
                'model'=>$this->loadModel($id), 'actions' => $this->listActionsCanAccess,
        ));
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }

    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate()
    {   
        $this->pageTitle = 'Tạo Mới Chiến Dịch';
        try{
        $model=new SpinCampaigns('create');
        $model->aDetail = [];
        if(isset($_POST['SpinCampaigns']))
        {
            $model->attributes = $_POST['SpinCampaigns'];
            $model->getDetailPost();
            $model->validate();
            if(!$model->hasErrors()){
                $model->handleBeforeSave();
                $model->save();
                $model->saveDetailPost();
                Yii::app()->user->setFlash( MyFormat::SUCCESS_UPDATE, "Thêm mới thành công." );
                $this->redirect(['view', 'id'=>$model->id]);
            }				
        }
        $this->render('create',array(
                'model'=>$model, 'actions' => $this->listActionsCanAccess,
        ));
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id)
    {
        $this->pageTitle = 'Cập Nhật Chiến Dịch';
        
        try{
        $model=$this->loadModel($id);
        $model->campaign_date = MyFormat::dateConverYmdToDmy($model->campaign_date, 'd-m-Y');
        $model->aDetail = $model->getDetail();
        $model->scenario = 'update';
        if(isset($_POST['SpinCampaigns']))
        {   
            $model->attributes=$_POST['SpinCampaigns'];
            $model->handleBeforeSave();
            $model->getDetailPost();
            $model->validate();
            if(!$model->hasErrors()){
                $model->save();
                $model->saveDetailPost();
                Yii::app()->user->setFlash( MyFormat::SUCCESS_UPDATE, "Cập nhật thành công." );
                $this->redirect(array('view','id'=>$model->id));
            }							
        }
        $this->render('update',array(
                'model'=>$model, 'actions' => $this->listActionsCanAccess,
        ));
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id)
    {
        try{
        if(Yii::app()->request->isPostRequest){
            // we only allow deletion via POST request
            if($model = $this->loadModel($id))
            {
                if($model->delete()){
                    Yii::log("Uid: " .Yii::app()->user->id. " Delete record ".  print_r($model->attributes, true), 'info');
                    SpinCampaignDetails::deleteByRootId($id);
                }
            }
            // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
            if(!isset($_GET['ajax']))
                    $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('index'));
        }
        else
        {
            Yii::log("Uid: " .Yii::app()->user->id. " Invalid request. Please do not repeat this request again.");
            throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
        }	
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }

    /**
     * Manages all models.
     */
    public function actionIndex()
    {
        $this->pageTitle = 'Quản Lý Chiến Dịch';
        try{
        $model=new SpinCampaigns('search');
        $model->unsetAttributes();  // clear any default values
        SpinNumbers::model()->checkExpired(); 
        if(isset($_GET['SpinCampaigns']))
                $model->attributes=$_GET['SpinCampaigns'];
        $this->render('index',array(
                'model'=>$model, 'actions' => $this->listActionsCanAccess,
        ));
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
    * If the data model is not found, an HTTP exception will be raised.
     * @param integer the ID of the model to be loaded
     */
    public function loadModel($id)
    {
        try{
        $model=SpinCampaigns::model()->findByPk($id);
        if($model===null)
        {
            Yii::log("The requested page does not exist.");
            throw new CHttpException(404,'The requested page does not exist.');
        }			
        return $model;
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }
    
    /** @Author: NhanDT Aug 20, 2019
     *  @Todo: todo
     *  @Param: param
     **/
    public function actionSpinLucky($id) {
        $this->pageTitle    = 'Vòng quay may mắn';
        $this->layout       = 'ajax';
        try{
            $this->onSpin();
            $model          = $this->loadModel($id);
            $mNumber        = new SpinNumbers();
            $mNumber->createRowsCondition(); // create 100 number if shortage
//            $mNumber->saveUserNumber(2, SpinNumbers::SOURCE_TYPE_CODE , '#dc789');  // example customer has 2 lucky Number 
            $model->aDetail = $model->getDetail();
            if (empty($model->user_total) && $model->checkCanSpin()){
                $model->user_total = SpinNumbers::model()->getQtyUsersUnexpired();
                $model->update();
            }
            if (isset($_POST['SpinLucky'])&&!empty($_POST['SpinLucky'])){ //if has action "save" 
                if (SpinCampaignDetails::checkTwoNumSame($_GET['id'], $_POST['SpinLucky']) > 0){ //check 2 số trùng nhau
                        Yii::app()->user->setFlash(MyFormat::ERROR_UPDATE, 'Số đã trúng. Vui lòng quay số khác!');                  
                   }
                   else {
                    $mNumber->saveUserWinning($_GET['id'], $_POST['SpinLucky'], $_POST['Rank']);                  
                   }
               }
            $this->render('spin_lucky/page_spin',array(
                    'model'=>$model, 'actions' => $this->listActionsCanAccess,
            ));
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }
    /** @Author: NamLA Sep 4,2019
     *  @Todo: save to db when spin a number
     *  @Param: param
     **/
    public function onSpin() {
        if(isset($_GET['onSpin'])){
            $number     = isset($_POST['number']) ? $_POST['number'] : '';
            $campaignId = isset($_POST['campaign_id']) ? $_POST['campaign_id'] : '';
            $rank       = isset($_POST['rank']) ? $_POST['rank'] : '';
            $id         = isset($_POST['id']) ? $_POST['id'] : '';
            $mCampaignDetail    = new SpinCampaignDetails();
            $mCampaignDetail->saveUserWinning($id, $campaignId, $number,$rank); // test, dont save
    //        $mSpinNumbers       = new SpinNumbers();
    //        echo json_encode($mSpinNumbers->arrNumberSpin());
            die;
        }
    }
    
    /** @Author: DuongNV Sep2019
     *  @Todo: notify winning user of campaign
     *  @Param:$id campaign id
     **/
    public function actionNotifyWinning($id)
    {
        try{
            $model = $this->loadModel($id);
            if($model)
            {
                $numberNotify = $model->notifyWinning();
                Yii::app()->user->setFlash( MyFormat::SUCCESS_UPDATE, "Notify thành công cho $numberNotify KH!" );
            }
            $this->redirect(array('index'));
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }
    
}
