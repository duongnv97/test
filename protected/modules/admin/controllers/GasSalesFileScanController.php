<?php

class GasSalesFileScanController extends AdminController 
{
    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id)
    {
            $this->pageTitle = 'Xem ';
            try{
                
                $model = $this->loadModel($id);
                if(Yii::app()->user->id==ROLE_SUB_USER_AGENT && Yii::app()->user->id != $model->uid_login){
                    $this->redirect(array('index'));
                }
                
                $this->render('view',array(
                        'model'=>$model, 'actions' => $this->listActionsCanAccess,
                ));                
            }
            catch (Exception $exc)
            {
                Yii::log("Uid: " .Yii::app()->user->id. " Exception ".  $exc->getMessage(), 'error');
                $code = 404;
                if(isset($exc->statusCode))
                    $code=$exc->statusCode;
                if($exc->getCode())
                    $code=$exc->getCode();
                throw new CHttpException($code, $exc->getMessage());
            }

    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate()
    {
        $this->pageTitle = 'Tạo Mới ';
            try
            {
            $model=new GasSalesFileScan('create');
            $model->mDetail = new GasSalesFileScanDetai();
            
            if(isset($_POST['GasSalesFileScan']))
            {
                    $model->attributes=$_POST['GasSalesFileScan'];
                    $model->mDetail->attributes = $_POST['GasSalesFileScanDetai'];
                    $model->validate();
                    GasSalesFileScanDetai::validateFile($model, array('create'=>1));
                    if(!$model->hasErrors()){
                        $model->save();
                        GasSalesFileScanDetai::saveFileScanDetail($model);
                        Yii::app()->user->setFlash('successUpdate', "Thêm mới thành công.");
                        $this->redirect(array('create','view_id'=>$model->id));
                    }				
            }

            $this->render('create',array(
                    'model'=>$model, 'actions' => $this->listActionsCanAccess,
            ));
            }
            catch (Exception $exc)
            {
                Yii::log("Uid: " .Yii::app()->user->id. " Exception ".  $exc->getMessage(), 'error');
                $code = 404;
                if(isset($exc->statusCode))
                    $code=$exc->statusCode;
                if($exc->getCode())
                    $code=$exc->getCode();
                throw new CHttpException($code, $exc->getMessage());
            }
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id)
    {
            $this->pageTitle = 'Cập Nhật ';
            try
            {
            $model=$this->loadModel($id);
            $model->scenario = 'update';
            if(!GasCheck::AgentCanUpdateSalesFileScan($model)){
                $this->redirect(array('index'));
            }
            $model->aModelDetail = count($model->rFileScanDetail)?$model->rFileScanDetail:array();
            $model->mDetail = new GasSalesFileScanDetai();
            $model->date_sales = MyFormat::dateConverYmdToDmy($model->date_sales);

            if(isset($_POST['GasSalesFileScan']))
            {
                $model->attributes=$_POST['GasSalesFileScan'];
                $model->mDetail->attributes = $_POST['GasSalesFileScanDetai'];
                $model->validate();
                GasSalesFileScanDetai::validateFile($model);
                if(!$model->hasErrors()){
                    $model->save();
                    if(is_array($model->mDetail->aIdNotIn) && count($model->mDetail->aIdNotIn)){
                        GasSalesFileScanDetai::deleteByNotInId($model->id, $model->mDetail->aIdNotIn);
                    }else{
                        GasSalesFileScanDetai::deleteByFileScanId($model->id);
                    }
                    GasSalesFileScanDetai::saveFileScanDetail($model);
                    Yii::app()->user->setFlash('successUpdate', "Cập nhật thành công.");
                    $this->redirect(array('update','id'=>$model->id));
                }							
            }

            $this->render('update',array(
                    'model'=>$model, 'actions' => $this->listActionsCanAccess,
            ));
            }
            catch (Exception $exc)
            {
                Yii::log("Uid: " .Yii::app()->user->id. " Exception ".  $exc->getMessage(), 'error');
                $code = 404;
                if(isset($exc->statusCode))
                    $code=$exc->statusCode;
                if($exc->getCode())
                    $code=$exc->getCode();
                throw new CHttpException($code, $exc->getMessage());
            }
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id)
    {
            try
            {
            if(Yii::app()->request->isPostRequest)
            {
                    // we only allow deletion via POST request
                    if($model = $this->loadModel($id))
                    {
                        if($model->delete())
                            Yii::log("Uid: " .Yii::app()->user->id. " Delete record ".  print_r($model->attributes, true), 'info');
                    }

                    // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
                    if(!isset($_GET['ajax']))
                            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('index'));
            }
            else
            {
                Yii::log("Uid: " .Yii::app()->user->id. " Invalid request. Please do not repeat this request again.");
                throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
            }	
            }
            catch (Exception $exc)
            {
                Yii::log("Uid: " .Yii::app()->user->id. " Exception ".  $exc->getMessage(), 'error');
                $code = 404;
                if(isset($exc->statusCode))
                    $code=$exc->statusCode;
                if($exc->getCode())
                    $code=$exc->getCode();
                throw new CHttpException($code, $exc->getMessage());
            }
    }

    /**
     * Manages all models.
     */
    public function actionIndex()
    {
            $this->pageTitle = 'Danh Sách ';
            try
            {
            $model=new GasSalesFileScan('search');
            $model->unsetAttributes();  // clear any default values
            if(isset($_GET['GasSalesFileScan']))
                    $model->attributes=$_GET['GasSalesFileScan'];

            $this->render('index',array(
                    'model'=>$model, 'actions' => $this->listActionsCanAccess,
            ));
            }
            catch (Exception $exc)
            {
                Yii::log("Uid: " .Yii::app()->user->id. " Exception ".  $exc->getMessage(), 'error');
                $code = 404;
                if(isset($exc->statusCode))
                    $code=$exc->statusCode;
                if($exc->getCode())
                    $code=$exc->getCode();
                throw new CHttpException($code, $exc->getMessage());
            }
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
    * If the data model is not found, an HTTP exception will be raised.
     * @param integer the ID of the model to be loaded
     */
    public function loadModel($id)
    {
            try
            {
            $model=GasSalesFileScan::model()->findByPk($id);
            if($model===null)
            {
                Yii::log("The requested page does not exist.");
                throw new CHttpException(404,'The requested page does not exist.');
            }			
            return $model;
            }
            catch (Exception $exc)
            {
                Yii::log("Uid: " .Yii::app()->user->id. " Exception ".  $exc->getMessage(), 'error');
                $code = 404;
                if(isset($exc->statusCode))
                    $code=$exc->statusCode;
                if($exc->getCode())
                    $code=$exc->getCode();
                throw new CHttpException($code, $exc->getMessage());
            }
    }

}
