<?php

class GasreportsController extends AdminController 
{
    /**
     * @Author: ANH DUNG 02-05-2014 
     * @Todo: xuất báo cáo doanh thu, sản lượng
     */
    public function actionRevenue_output() { 
        MyFunctionCustom::checkChangePassword();
        $cRole = MyFormat::getCurrentRoleId();
        set_time_limit(7200);
        $this->pageTitle = 'Xem báo cáo doanh thu - sản lượng';
        try {
            $data = [];$session=Yii::app()->session;
            $model = new GasStoreCard();
            $model->statistic_year = date('Y');
            $model->statistic_month = date('m');
            if(isset($_POST['GasStoreCard'])){
                $model->attributes = $_POST['GasStoreCard'];
                $aRoleLimit = GasCheck::$ARR_ROLE_LIMIT_AGENT;
                // DuongNV Sep3019 copy get province (nếu move từ trong if ra thì model->agent_id sẽ bị mất giá trị)
                if(!empty($model->province_id_agent)){
                    $model->agent_id = $model->getAgentOfProvince($model->province_id_agent);
                }// DuongNV end
                if(!in_array($cRole, $aRoleLimit)){
                    $model->agent_id = is_array($model->agent_id) ? $model->agent_id : Users::getArrIdAgentNotWarehouse(array('gender'=>Users::IS_AGENT));
                    if(!empty($model->province_id_agent)){
                        $model->agent_id = $model->getAgentOfProvince($model->province_id_agent);
                    }
                }elseif(isset($session['LIST_AGENT_OF_USER']) && count($session['LIST_AGENT_OF_USER'])){
                    $model->agent_id = $session['LIST_AGENT_OF_USER'];
                }
//                $data = Statistic::RevenueOutput($model);
                $data = $model->revenueOutputV2();
            }
            $_SESSION['modelRevenueOutput'] = $model;
            $this->exportExcelRevenueOutput($model);
            $this->render('Revenue_output',array(
                    'model'=>$model, 'actions' => $this->listActionsCanAccess,
                    'data'=>$data,
            ));
        } catch (Exception $exc) {
            GasCheck::CatchAllExeptiong($exc);
        }
    }
    
    public function checkRoleBoMoi($model){
        if(Yii::app()->user->role_id==ROLE_HEAD_GAS_BO){
//            $model->ext_is_maintain = STORE_CARD_KH_BINH_BO; 
        }elseif(Yii::app()->user->role_id==ROLE_HEAD_GAS_MOI){
            $model->ext_is_maintain = STORE_CARD_KH_MOI;
        }
    }
    
    /**
     * @Author: ANH DUNG Feb 22, 2014
     * @Todo: xuất báo cáo daily sản lượng
     */
    public function actionOutput_daily() {
        set_time_limit(7200);
        $this->pageTitle = 'Xem báo cáo daily sản lượng';
        try {
            $data=array();
            $cRole = Yii::app()->user->role_id;
            $cUid = Yii::app()->user->id;            
            $model = new GasStoreCard();
            $model->unsetAttributes();  // clear any default values
            $model->date_from   = date('01-m-Y');
            $model->date_to     = date('d-m-Y');
            $this->mapConditionByRole($model, $cRole, $cUid);
//            $this->checkRoleBoMoi($model);
            
            if(isset($_POST['GasStoreCard'])){
                $model->attributes = $_POST['GasStoreCard'];
                if(!isset($_POST['GasStoreCard']['agent_id']) && $cRole != ROLE_SUB_USER_AGENT
                        && !in_array($cRole, GasCheck::$ARR_ROLE_LIMIT_AGENT)
                ){
//                    $model->agent_id = Users::getArrIdUserByRole(ROLE_AGENT);
//                    $model->agent_id = Users::getArrIdAgentNotWarehouse(array('gender'=>Users::IS_AGENT));// Close on May 11, 2016  anh Đức TP bò Y/C
                    $model->agent_id = Users::getArrIdAgentNotWarehouse(array());
                }
                $this->mapConditionByRole($model, $cRole, $cUid);
//                $this->checkRoleBoMoi($model);
                $data = Statistic::Output_daily($model);
                //export excel by Duong
                $_SESSION['data-model'] = $model;
                //end by Duong
            }
            $this->render('Output_daily',array(
                    'model'=>$model, 'actions' => $this->listActionsCanAccess,
                'data'=>$data,
            ));
        } catch (Exception $exc) {
            GasCheck::CatchAllExeptiong($exc);
        }
    }
    
    /**
     * @Author: ANH DUNG Jul 25, 2016
     * @Todo: giới hạn search của chuyên viên CCS + Sale
     */
    public function mapConditionByRole($model, $cRole, $cUid) {
        $aRoleLikeSale = array(ROLE_SALE, ROLE_MONITORING_MARKET_DEVELOPMENT);
        $cUid = MyFormat::getCurrentUid();
        if($cRole==ROLE_SUB_USER_AGENT || $cUid == GasConst::UID_NHUNG_TTT){
            $model->agent_id = array(MyFormat::getAgentId());
        }elseif(in_array($cRole, $aRoleLikeSale)){
            $model->ext_sale_id = $cUid;
            $TypeSale = Yii::app()->user->gender; // không đưa cái này vào vì sale mối chuyên viên có thể có cả KH bò lẫn mối
            if(in_array($TypeSale, Users::$aIdTypeSaleMoi)){
//                $model->ext_is_maintain = Users::SALE_MOI;
            }
        }
        if(!empty($model->user_id_create)){// DungNT May0619
            $model->agent_id = [$model->user_id_create];
        }
        
    }
    
    /**
     * @Author: ANH DUNG Mar 26, 2014
     * @Todo: xuất báo cáo target doanh số, cho sale vs đại lý
     */
    public function actionTarget() {
//        $this->redirect(Yii::app()->createAbsoluteUrl('admin/site'));
        set_time_limit(7200);
        $this->pageTitle = 'Xem báo cáo target';
        try {
            $data=array();
            $model = new GasStoreCard();
            $model->statistic_year = date('Y');            
            $model->statistic_month = date('m');

            if(isset($_POST['GasStoreCard'])){
                $model->attributes = $_POST['GasStoreCard'];
                $needMore = array('agent_view_sale_moi'=>1);
//                $data = Statistic::Target($model, $needMore);
                $data = Sta2::TargetFix($model);
            }
//            $this->render('Target',array(
            $this->render('TargetBo/TargetBo',array(
                'model'=>$model, 'actions' => $this->listActionsCanAccess,
                'data'=>$data,
            ));
        } catch (Exception $exc) {
            GasCheck::CatchAllExeptiong($exc);
        }
    }
    
    /**
     * @Author: ANH DUNG Apr 02, 2014
     * @Todo: thống kê kh mua hàng thường xuyên số lần mua hàng
     * của KH đang dùng gas Hướng Minh
     */
    public function actionCustomerFrequently() {
        $this->pageTitle = 'Thống Kê Khách Hàng Thường Xuyên';
        try
        {
            $model=new GasMaintainSell('search');
            $model->unsetAttributes();  // clear any default values
            if(isset($_GET['GasMaintainSell']))
                    $model->attributes=$_GET['GasMaintainSell'];

            $this->render('CustomerFrequently',array(
                    'model'=>$model, 'actions' => $this->listActionsCanAccess,
            ));
        } catch (Exception $exc) {
            GasCheck::CatchAllExeptiong($exc);
        }
    }
        
    /**
     * @Author: ANH DUNG Apr 04, 2014
     * @Todo: xác định bình quay về của PTTT
     * của KH đang dùng gas Hướng Minh
     */
    public function actionDetermineGasGoBack() {
        $this->pageTitle = 'Xác định bình quay về của PTTT';
        try
        {
            $model=new GasMaintainSell();            
            $data = array();
            if(isset($_POST['GasMaintainSell'])){
                $model->attributes = $_POST['GasMaintainSell'];
                $data = Statistic::DetermineGasGoBack($model);
            }

            $this->render('DetermineGasGoBack',array(
                    'model'=>$model, 'actions' => $this->listActionsCanAccess,
                "data"=>$data,
            ));
        } catch (Exception $exc) {
            GasCheck::CatchAllExeptiong($exc);
        }
    }
    
    /**
     * @Author: ANH DUNG May 20, 2014
     * @Todo: xuất báo cáo daily nội bộ, cho kho
     */
    public function actionDailyInternal() {
        set_time_limit(7200);
        $this->pageTitle = 'Xem báo cáo daily nội bộ';
        try {
            $data=array();
            $model = new GasStoreCard();
            $model->unsetAttributes();  // clear any default values
            $model->date_from = date('d-m-Y');
            $model->date_to = date('d-m-Y');
            if(Yii::app()->user->role_id!=ROLE_SUB_USER_AGENT){
                $model->agent_id = 26678; // Kho Tân sơn
            }

            if(isset($_POST['GasStoreCard'])){                
                $model->attributes = $_POST['GasStoreCard'];
                if(Yii::app()->user->role_id==ROLE_SUB_USER_AGENT){
                    $model->agent_id = MyFormat::getAgentId();
                }
                $data = Statistic::DailyInternal($model);
            }
            $this->render('DailyInternal/DailyInternal',array(
                'model'=>$model, 'actions' => $this->listActionsCanAccess,
                'data'=>$data,
            ));
        } catch (Exception $exc) {
            GasCheck::CatchAllExeptiong($exc);
        }
    }    
        
    /**
     * @Author: ANH DUNG Jun 28, 2014
     * @Todo: xuất báo cáo tồn kho all đại lý
     */
    public function actionInventory() {
        set_time_limit(7200);
        $this->pageTitle = 'Xem báo cáo tồn kho';
        try {
            $data=array();
            $aTypeAgent = array(Users::IS_AGENT, Users::IS_WAREHOUSE);
            $model = new GasStoreCard();
            $model->date_to = date('d-m-Y');
            if(Yii::app()->user->role_id==ROLE_SUB_USER_AGENT){
                $model->agent_id = array(MyFormat::getAgentId());
            }
            $model->ext_materials_type_id = CmsFormatter::$MATERIAL_TYPE_BINHBO_12;
            
            if(isset($_POST['GasStoreCard'])){
                $model->attributes = $_POST['GasStoreCard'];
                if(!isset($_POST['GasStoreCard']['agent_id']) && Yii::app()->user->role_id != ROLE_SUB_USER_AGENT
                        && !in_array(Yii::app()->user->role_id, GasCheck::$ARR_ROLE_LIMIT_AGENT)
                ){
                    $model->agent_id = Users::getArrIdAgentNotWarehouse(array('gender'=>$aTypeAgent));
                    unset($model->agent_id[GasCheck::KHO_TAN_SON]);
                }
                
                if(!isset($_POST['GasStoreCard']['ext_materials_type_id'])){
                    // Nếu để empty thì lấy hết hơi nhiều
                    $model->ext_materials_type_id = CmsFormatter::$MATERIAL_TYPE_BINHBO_12;
//                    $model->ext_materials_type_id = GasMaterialsType::getAllItem(array(), array('for_in_condition'=>1));
                }
                
                $data = Statistic::Inventory($model);
            }
            $this->render('Inventory/Inventory',array(
                'model'=>$model, 'actions' => $this->listActionsCanAccess,
                'data'=>$data,
                'aTypeAgent'=>$aTypeAgent,
            ));
        } catch (Exception $exc) {
            GasCheck::CatchAllExeptiong($exc);
        }
    }
    
    /**
     * @Author: ANH DUNG Aug 16, 2014
     * @Todo: báo cáo sản lượng khách hàng
     */
    public function actionOutput_customer() {
        $this->reportAgentExcel(); //nam
        set_time_limit(7200);
        $this->pageTitle = 'Xem báo cáo sản lượng khách hàng';
        try {
            $data=array();
            
            $model = new GasStoreCard();
            $model->date_from   = date('01-m-Y');
            $model->date_to     = date('d-m-Y');
            if(Yii::app()->user->role_id==ROLE_SUB_USER_AGENT){
                $model->agent_id = array(MyFormat::getAgentId());
            }elseif(Yii::app()->user->role_id==ROLE_SALE){
                $model->ext_sale_id = Yii::app()->user->id;
                $TypeSale = Yii::app()->user->gender;
                if(in_array($TypeSale, Users::$aIdTypeSaleMoi)){
                    $model->ext_is_maintain = Users::SALE_MOI;
                }
            }
            $this->checkRoleBoMoi($model);
            
            if(isset($_POST['GasStoreCard'])){
                $model->attributes = $_POST['GasStoreCard'];
                $this->checkRoleBoMoi($model);
                if(isset($_GET['type']) && $_GET['type']== GasStoreCard::BC_TONGQUAT){ 
                    $sta2 = new Sta2();
                    $data = $sta2->outputCustomerGenerality($model);
//                    $data = Sta2::Output_customer_generality($model);
                }elseif(isset($_GET['type']) && $_GET['type']== GasStoreCard::BC_CHITIET) {
                    $data = Sta2::Output_customer($model);
                    $_SESSION['data-model'] = $model;
                    $_SESSION['data-excel'] = $data;
                }
            }
            $_GET['type'] = isset($_GET['type']) ? $_GET['type'] : GasStoreCard::BC_CHITIET;
            
            $this->render('Output_customer/index',array(
                'model'=>$model, 'actions' => $this->listActionsCanAccess,
                'data'=>$data,
            ));
        } catch (Exception $exc) {
            GasCheck::CatchAllExeptiong($exc);
        }
    }
    
    /** @Author: NamLA
     *  @Todo: xuat excel tab chi tiet
     **/
    public function reportAgentExcel(){
        if(isset($_GET['to_excel']) && isset($_SESSION['data-excel'])){
            ini_set('memory_limit','1500M');// chưa cần chạy đoạn này, đưa memory_limit lên 500 có thể export dc khoảng 5000 row - chưa test
            ToExcel::summaryOutputCustomer();
            $this->redirect(array('output_customer', 'type'=>GasStoreCard::BC_CHITIET));
        }
    }
    
    /**
     * @Author: ANH DUNG Mar 17, 2015
     * @Todo: xuất báo cáo binh quay ve tung doi, tung nhan vien, tung ngay
     * admin/gasreports/pttt_daily_goback
     */
    public function actionPttt_daily_goback() {
        set_time_limit(7200);
        $this->pageTitle = 'PTTT daily goback';
        try {
            $data=array();
            $model = new GasPtttDailyGoback();
            $model->date_from = "01-".date('m-Y');
            $model->date_to = date('d-m-Y');
            if(isset($_POST['GasPtttDailyGoback'])){
                $model->attributes = $_POST['GasPtttDailyGoback'];
                $data = GasPtttDailyGoback::StatisticPttt($model);
            }
            $this->render('PTTT/Pttt_daily_goback',array(
                'model'=>$model, 'actions' => $this->listActionsCanAccess,
                'data'=>$data,
            ));
        } catch (Exception $exc) {
            GasCheck::CatchAllExeptiong($exc);
        }            
    }
    
    /**
     * @Author: ANH DUNG Mar 18, 2015
     * @Todo: xuất báo cáo target ho gia dinh: gs pttt,
     *  mỗi giám sát quản lý 1 số đại lý, thực tế  sum các đại lý lại
     * admin/gasreports/target_monitor_pttt
     */
    public function actionTarget_monitor_pttt() {
        set_time_limit(7200);
        $this->pageTitle = 'Target hộ gia đình - giám sát PTTT ';
        try {
            $data=array();
            $model = new GasStoreCard();
            $model->statistic_year = date('Y');            
            $model->statistic_month = date('m');

            if(isset($_POST['GasStoreCard'])){
                $model->attributes = $_POST['GasStoreCard'];
                $data = Sta2::Target_monitor_pttt($model);
            }
            $this->render('PTTT/Target_monitor_pttt',array(
                'model'=>$model, 'actions' => $this->listActionsCanAccess,
                'data'=>$data,
            ));
        } catch (Exception $exc) {
            GasCheck::CatchAllExeptiong($exc);
        }
    }
    
    /**
     * @Author: ANH DUNG Apr 03, 2015
     * @Todo:  khach hang khong lay hang trong: 3 tab 15->30, 30->45, tren 45 ngay
     */
    public function actionCustomer_buy() {
        set_time_limit(7200);
        $this->pageTitle = 'Thống kê khách hàng không lấy hàng';
        try
        {
        $model=new Users('search');
        $model->unsetAttributes();  // clear any default values
        if(isset($_GET['Users']))
                $model->attributes=$_GET['Users'];
        $model->role_id = ROLE_CUSTOMER;
        $model->type = CUSTOMER_TYPE_STORE_CARD;

        $this->render('Customer_buy/Customer_buy',array(
                'model'=>$model, 'actions' => $this->listActionsCanAccess,
        ));
        } catch (Exception $exc) {
            GasCheck::CatchAllExeptiong($exc);
        }
    }
    
    /**
     * @Author: ANH DUNG Apr 04, 2015
     * @Todo:  báo cáo target của chuyên viên kinh doanh khu vực - PTTT
     */
    public function actionTarget_sale_cvkv() {
        set_time_limit(7200);
        $this->pageTitle = 'Báo cáo target CCS';
        try
        {
            $data=array();
            $model = new GasStoreCard();
            $model->statistic_year = date('Y');            
            $model->statistic_month = date('m');

            if(isset($_POST['GasStoreCard'])){
                $model->attributes = $_POST['GasStoreCard'];
                $data = Sta2::Target_sale_cvkv($model);
            }
        $this->render('Target_chuyenvien_kinhdoanh_khuvuc/Sale_pttt',array(
                'model'=>$model, 'actions' => $this->listActionsCanAccess,
                'data'=>$data,
        ));
        } catch (Exception $exc) {
            GasCheck::CatchAllExeptiong($exc);
        }
    }
    
    /**
     * @Author: ANH DUNG Apr 20, 2015
     * @Todo: BC san luong tang giam theo thang
     * admin/gasreports/output_change
     */
    public function actionOutput_change() {
        set_time_limit(7200);
        $this->pageTitle = 'Báo cáo sản lượng tăng giảm theo tháng';
        try {
            $data=array();
            $model = new GasStoreCard();
            $model->statistic_year = date('Y');            
            if(isset($_POST['GasStoreCard'])){
                $model->attributes = $_POST['GasStoreCard'];
                $mSta2 = new Sta2();
                $data = $mSta2->outputChange($model);
            }
            $this->render('Output_change/Output_change',array(
                'model'=>$model, 'actions' => $this->listActionsCanAccess,
                'data'=>$data,
            ));
        } catch (Exception $exc) {
            GasCheck::CatchAllExeptiong($exc);
        }
    }
    
    /**
     * @Author: ANH DUNG May 18, 2015
     * @Todo: Cập nhật move khách hàng sang danh sách không lấy hàng
     * admin/gasreports/Customer_buy
     */
    public function actionUpdate_customer_not_buy($id) {
        $this->layout='ajax';
        try {
            $model = MyFormat::loadModelByClass($id, 'Users');
            $model->LoadUsersRef();
            $model->mUsersRef->scenario = 'CustomerLeave';
            
            if(isset($_POST['UsersRef'])){
                $model->mUsersRef->attributes = $_POST['UsersRef'];
                $model->mUsersRef->validate();
                if(!$model->mUsersRef->hasErrors()){
                    $model->SaveUsersRef();// May 27, 2015 
                    $model->UpdateKhongLayHang();
                    die('<script type="text/javascript">parent.$.fn.colorbox.close(); parent.$(".SubmitButton").trigger("click"); </script>'); 
                }
            }
            
            $this->render('Customer_buy/Update_customer_not_buy',array(
                    'model'=>$model, 'actions' => $this->listActionsCanAccess,                    
            ));
            
        } catch (Exception $exc) {
            GasCheck::CatchAllExeptiong($exc);
        }
    }
    
    /**
     * @Author: ANH DUNG Feb 19, 2016
     * @Todo: BC san luong dai ly
     * admin/gasreports/outputAgent
     */
    public function actionOutputAgent() {
        set_time_limit(7200);
        $this->pageTitle = 'Báo cáo sản lượng đại lý';
        try {
            $data=array();
            $model = new GasStoreCard();
            $model->date_from = "01-".date('m-Y');
            $model->date_to = date('d-m-Y');
            if(isset($_POST['GasStoreCard'])){
                $model->attributes = $_POST['GasStoreCard'];
                $data = Sta2::OutputAgent($model);
                ExportExcel::OutputAgent($model, $data);
            }
            $this->render('OutputAgent/OutputAgent',array(
                'model'=>$model, 'actions' => $this->listActionsCanAccess,
            ));
        } catch (Exception $exc) {
            GasCheck::CatchAllExeptiong($exc);
        }
    }
    
    /**
     * @Author: ANH DUNG Mar 02, 2016
     * @Todo: target new
     */
    public function actionTargetNew() {
        $this->pageTitle = 'Target New';
        try
        {
            $data=array();
            $model = new GasStoreCard();
            $model->date_from = "01".date('-m-Y');
            $model->date_to = date('d-m-Y');
            if(isset($_POST['GasStoreCard'])){
                $model->attributes = $_POST['GasStoreCard'];
                $mSta2 = new Sta2();
                $data = $mSta2->targetNew($model);
            }

            $this->render('TargetNew/TargetNew',array(
                'model'=>$model, 'actions' => $this->listActionsCanAccess,
                "data"=>$data,
            ));
        } catch (Exception $exc) {
            GasCheck::CatchAllExeptiong($exc);
        }
    }
    
    /**
     * @Author: ANH DUNG May 05, 2016
     * @Todo: InventoryVo
     */
    public function actionInventoryVo() {
        $this->pageTitle = 'Tồn kho vỏ';
        try
        {
            $data=array();
            $model = new GasStoreCard();
//            $aTypeAgent = array(Users::IS_AGENT, Users::IS_WAREHOUSE);
            $aTypeAgent = array(Users::IS_WAREHOUSE);
            $model->date_to = date('d-m-Y');
            $model->ext_materials_type_id = CmsFormatter::$MATERIAL_TYPE_BINHBO_INPUT;
            unset($model->ext_materials_type_id[3]); // is vỏ MATERIAL_TYPE_VO_6
            $model->agent_id = Users::getArrIdAgentNotWarehouse(array('gender'=>$aTypeAgent));
//            $model->agent_id[MyFormat::DL_TAN_DINH] = MyFormat::DL_TAN_DINH;
            unset($model->agent_id[GasCheck::KHO_TAN_SON]);
            
            if(isset($_POST['GasStoreCard'])){
                $model->attributes = $_POST['GasStoreCard'];
                $data = Sta2::InventoryVo($model);
            }else{
                $data = Sta2::InventoryVo($model);
            }

            $this->render('InventoryVo/InventoryVo',array(
                'model'=>$model, 'actions' => $this->listActionsCanAccess,
                "data"=>$data,
            ));
        } catch (Exception $exc) {
            GasCheck::CatchAllExeptiong($exc);
        }      
    }
    
    /**
     * @Author: ANH DUNG Jul 29, 2016
     * @Todo: BC san luong Xe Phuoc Tan
     * admin/gasreports/outputCar
     */
    public function actionOutputCar() {
        set_time_limit(7200);
        $this->pageTitle = 'Báo cáo sản lượng xe';
        try {
            MyFormat::initSessionModelByRole(array(ROLE_CAR));
            $aData=array();
            $model = new GasStoreCard();
            $model->date_from = "01-".date('m-Y');
            $model->date_to = date('d-m-Y');
            $model->agent_id = MyFormat::getAgentId();
            if(isset($_GET['GasStoreCard'])){
                $model->attributes  = $_GET['GasStoreCard'];
                $model->MAX_ID      = $_GET['IsWebView'];
                $aData       = Sta2::OutputCar($model);
                if(empty($model->MAX_ID)){
                    ExportExcel::OutputCar($model, $aData);
                }
            }
            $this->render('OutputCar/OutputCar',array(
                'model'=>$model,
                'aData'=>$aData,
                'actions' => $this->listActionsCanAccess,
            ));
        } catch (Exception $exc) {
            GasCheck::CatchAllExeptiong($exc);
        }
    }
    
    /**
     * @Author: ANH DUNG Oct 07, 2016
     * @Todo: tính sản lượng của điều phối
     * admin/gasreports/callCenterOutput
     * @Param: tổng sl kh bò, mối, số bình, gas dư, số cuộc gọi
     */
    public function actionCallCenterOutput() {
        $this->pageTitle = 'Bác cáo sản lượng điếu phối';
        try{
            $aData=array();
            $model = new GasStoreCard();
            $model->date_from = "01-".date('m-Y');
            $model->date_to = date('d-m-Y');
            if(isset($_GET['GasStoreCard'])){
                $model->attributes  = $_GET['GasStoreCard'];
                $aData       = Sta2::CallCenterOutput($model);
            }
            $this->render('CallCenter/Output',array(
                'model'=>$model,
                'aData'=>$aData,
                'actions' => $this->listActionsCanAccess,
            ));
        } catch (Exception $exc) {
            GasCheck::CatchAllExeptiong($exc);
        }
    }
    
    /**
     * @Author: ANH DUNG Apr 13, 2017
     * @Todo: BC kết quả kinh doanh
     * admin/gasreports/business
     */
    public function actionBusiness() {
        set_time_limit(7200);
        $this->pageTitle = 'Báo cáo kết quả kinh doanh';
        try {
            $rData = [];
            $model = new GasStoreCard();
            $model->date_from = "01-".date('m-Y');
            $model->date_to = date('d-m-Y');
            $model->agent_id = 100;
            if(isset($_GET['GasStoreCard'])){
                $model->attributes  = $_GET['GasStoreCard'];
                if(!empty($model->agent_id)){
                    $report = new Sta2();
                    $rData  = $report->business($model);
                }else{
                    $model->addError('agent_id', 'Chưa chọn đại lý');
                }
            }
            $this->render('Business/Business',array(
                'model'=>$model,
                'rData'=>$rData,
                'actions' => $this->listActionsCanAccess,
            ));
        } catch (Exception $exc) {
            GasCheck::CatchAllExeptiong($exc);
        }
    }
    
    /**
     * @Author: ANH DUNG Jun 23, 2017
     * @Todo: BC công thực tế NVGN
     * admin/gasreports/employeeMaintain
     */
    public function actionEmployeeMaintain() {
        set_time_limit(7200);
        $this->pageTitle = 'Báo cáo công NV giao nhận';
        try {
            $this->employeeMaintainExcel();
            $rData = [];
            $model = new GasStoreCard();
            $model->date_from = "01-".date('m-Y');
            $model->date_to = date('d-m-Y');
            $model->province_id_agent = [GasProvince::TP_HCM];
            if(isset($_GET['GasStoreCard'])){
                $model->attributes  = $_GET['GasStoreCard'];
                $report = new Sta2();
                if(!isset($_GET['detail'])){
                    $rData  = $report->employeeMaintain($model);
                }else{
                    $rData  = $report->employeeMaintainDetail($model);
                }
            }
            $this->render('Employee/EmployeeMaintain',array(
                'model'=>$model,
                'rData'=>$rData,
                'actions' => $this->listActionsCanAccess,
            ));
        } catch (Exception $exc) {
            GasCheck::CatchAllExeptiong($exc);
        }
    }
    
    /** @Author: ANH DUNG Jun 23, 2017
     * @Todo: to excel report employeeMaintain
     */
    public function employeeMaintainExcel() {
        if(isset($_GET['to_excel'])){
            ini_set('memory_limit','500M');
            ToExcel::EmployeeMaintain();
        }
    }
    /** @Author: ANH DUNG Sep 17, 2017
     *  @Todo: BC KH HGĐ cũ mới
     * admin/gasreports/hgdSource
     */
    public function actionHgdSource() {
        set_time_limit(7200);
        $this->pageTitle = 'Sản lượng khách hàng HGĐ mới - cũ';
        try {
            $rData = [];
            $model = new Sell();
            $model->unsetAttributes();
            $model->date_from   = "01-".date('m-Y');
            $model->date_to     = date('d-m-Y');
            $model->status      = Sell::STATUS_PAID;
//            $model->province_id = [GasProvince::TP_HCM];
            if(isset($_GET['Sell'])){
                $model->attributes  = $_GET['Sell'];
                $report = new Sta2();
                $rData  = $report->hgdSource($model);
            }

            $this->render('Sell/HgdSource',array(
                'model'=>$model,
                'rData'=>$rData,
                'actions' => $this->listActionsCanAccess,
            ));
        } catch (Exception $exc) {
            GasCheck::CatchAllExeptiong($exc);
        }
    }
    
    /** @Author: DUONG 20/04/2018
     *  @Todo: export to excel
     **/
   
    public function actionExcelOutputDaily(){
        try{
            if(isset($_SESSION['data-model'])){
                ini_set('memory_limit','1500M');// chưa cần chạy đoạn này, đưa memory_limit lên 500 có thể export dc khoảng 5000 row - chưa test
                ToExcel::summaryGasReports();
            }
            $this->redirect(array('output_daily'));
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }
    
    /** @Author: NGUYEN KHANH TOAN Sep 12 ,2018
     *  @Todo: xet quyen cho phep xuat excel revenue output
     *  @Param: 
     **/
    public function canExportExcelForRevenueOutput() {
        $cUid   = MyFormat::getCurrentUid();
        $aUidAllow = [GasConst::UID_CHIEN_BQ, GasConst::UID_ADMIN, GasConst::UID_CHAU_LNM];
        return in_array($cUid, $aUidAllow);
    }
    
    public function exportExcelRevenueOutput($model){
        try{
            if(!$this->canExportExcelForRevenueOutput() || !isset($_GET['ExportExcel'])){
                return ;
            }
            if(isset($_SESSION['data-excel'])){
                ini_set('memory_limit','2000M');// chưa cần chạy đoạn này, đưa memory_limit lên 500 có thể export dc khoảng 5000 row - chưa test
                $mToExcelList = new ToExcelList();
                $mToExcelList->exportRevenueOutput();
            }
//        $this->redirect(array('index'));
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }
}
