<?php

class CheckAgentController extends AdminController 
{
    public $pluralTitle = 'Kiểm Kho Đại Lý';
    public $singleTitle = 'Kiểm Kho Đại Lý';
    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id)
    {
        $this->pageTitle = 'Xem ';
        try{
        $model=$this->loadModel($id);
        $aData = $model->getJsonToData();
        $_SESSION['model-excel-check-agent'] = $model;
        $_SESSION['data-excel-check-agent'] = $aData;
        $this->exportExcelView();
        $this->render('view',array(
            'aData' => $aData,
            'model'=>$model, 'actions' => $this->listActionsCanAccess,
        ));
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }

    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate()
    {
        $this->pageTitle = 'Kiểm tra ';
        try{
        $aData =[];
        $model=new CheckAgent('create');
        $staCheckAgent = new StaCheckAgent();
        if(!empty($_GET['CheckAgent']['agent_id'])){
            $aData =$staCheckAgent->getDataCheckAgent($_GET['CheckAgent']['agent_id']); // lấy dữ liệu cho toàn bộ index
            $model->attributes = $_GET['CheckAgent'];
            
            if($model->existData() != NULL){
                $idCheckAgent = $model->existData()->id;
//                $model = $model->updateModel();
//                Yii::app()->user->setFlash( MyFormat::SUCCESS_UPDATE, "Cập nhật thành công." );
                $this->redirect(array('update','id'=>$idCheckAgent));
            }
        }
        if(isset($_POST['CheckAgent']))
        {
            $model->addAttributes($_POST['CheckAgent']);
            $model->attributes = $_POST['CheckAgent'];
            
            $model->validate();
            $model->baseFileValidate();
            
            if($model->existData()){
                $model = $model->updateModel();
//                Yii::app()->user->setFlash( MyFormat::SUCCESS_UPDATE, "Cập nhật thành công." );
                $this->redirect(array('update','id'=>$model->id));
            }
            
            if(!$model->hasErrors()){
                $model->save();
                $model->baseFileSave(GasFile::TYPE_CHECK_AGENT);
                Yii::app()->user->setFlash( MyFormat::SUCCESS_UPDATE, "Thêm mới thành công." );
                $this->redirect(array('update','id'=>$model->id));
            }
            
        }
        
        $this->render('create',array(
            'aData' => $aData,
            'model'=>$model, 
            'actions' => $this->listActionsCanAccess,
        ));
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id)
    {
        $this->pageTitle = 'Cập Nhật ';
        try{
        $model=$this->loadModel($id);

        if( !isset($model) || !$model->canUpdate()){
//        if( !isset($model)){
            $this->redirect(array('index'));
        }
        $model->scenario = 'update';
        if(isset($_POST['CheckAgent'])){
            $model->addAttributes($_POST['CheckAgent']);
            $model->attributes=$_POST['CheckAgent'];
            $model->validate();
            $model->baseFileValidate();
            if(!$model->hasErrors()){
//                if( !empty($model->updateModel()) ){
//                    $model = $model->updateModel();
//                }
                $model->update();
                $model->baseFileSave(GasFile::TYPE_CHECK_AGENT);
                Yii::app()->user->setFlash( MyFormat::SUCCESS_UPDATE, "Cập nhật thành công." );
                $this->redirect(array('update','id'=>$model->id));
            }							
        }
        $aData = $model->getJsonToData();
        $this->render('update',array(
            'aData' => $aData,
            'model'=>$model, 
            'actions' => $this->listActionsCanAccess,
        ));
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id)
    {
        try{
        if(Yii::app()->request->isPostRequest){
            // we only allow deletion via POST request
            if($model = $this->loadModel($id))
            {
                if($model->delete())
                    Yii::log("Uid: " .Yii::app()->user->id. " Delete record ".  print_r($model->attributes, true), 'info');
            }

            // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
            if(!isset($_GET['ajax']))
                    $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('index'));
        }
        else
        {
            Yii::log("Uid: " .Yii::app()->user->id. " Invalid request. Please do not repeat this request again.");
            throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
        }	
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }

    /**
     * Manages all models.
     */
    public function actionIndex()
    {
        $this->pageTitle = 'Danh Sách ';
        try{
        $model=new CheckAgent('search');
        $model->date_from = date('01-m-Y');
        $model->date_to = date('d-m-Y');
        $model->unsetAttributes();  // clear any default values
        $this->exportExcelIndex();
        if(isset($_GET['CheckAgent']))
                $model->attributes=$_GET['CheckAgent'];

        $this->render('index',array(
            'model'=>$model, 
            'actions' => $this->listActionsCanAccess,
        ));
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }
    
    /** @Author: LocNV Jun , 2019
     *  @Todo:
     *  @Param:
     **/
    public function actionReport() {
        $this->pageTitle = 'Báo cáo kiểm kho';
        try{
            $aData              = [];
            $model              = new CheckAgent();
            $model->date_from   = date('01-m-Y');
            $model->date_to     = date('d-m-Y');
            $model->unsetAttributes();  // clear any default values
            $this->exportExcelReport();
            if(isset($_GET['CheckAgent'])) {
                $model->attributes = $_GET['CheckAgent'];
                $aData = $model->reportKiemKho();
            }

            $this->render('report',array(
                'model'=>$model, 
                'actions' => $this->listActionsCanAccess,
                'aData' => $aData,
            ));
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }

        /**
     * Returns the data model based on the primary key given in the GET variable.
    * If the data model is not found, an HTTP exception will be raised.
     * @param integer the ID of the model to be loaded
     */
    public function loadModel($id)
    {
        try{
        $model=CheckAgent::model()->findByPk($id);
        if($model===null)
        {
            Yii::log("The requested page does not exist.");
            throw new CHttpException(404,'The requested page does not exist.');
        }			
        return $model;
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }

    /**
     * @author LocNV Sep 23, 2019
     * @throws CHttpException
     */
    public function exportExcelView()
    {
        try{
            if(isset($_GET['to_excel']) && isset($_SESSION['data-excel-check-agent'])){
                ini_set('memory_limit','1500M');// chưa cần chạy đoạn này, đưa memory_limit lên 500 có thể export dc khoảng 5000 row - chưa test
                $toExcel = new ToExcel();
                $toExcel->summaryCheckAgent();
            }
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }

    /**
     * @author LocNV Sep 30, 2019
     * @throws CHttpException
     */
    public function exportExcelReport()
    {
        try{
            if(isset($_GET['to_excel']) && isset($_SESSION['data-excel-check-agent-report'])){
                ini_set('memory_limit','1500M');// chưa cần chạy đoạn này, đưa memory_limit lên 500 có thể export dc khoảng 5000 row - chưa test
                $toExcel = new ToExcel();
                $toExcel->summaryCheckAgentReport();
            }
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }

    /**
     * @author LocNV Sep 30, 2019
     * @todo export excel check agent index
     * @throws CHttpException
     */
    public function exportExcelIndex()
    {
        try{
            if(isset($_GET['to_excel']) && isset($_SESSION['data-excel-check-agent-index'])){
                ini_set('memory_limit','1500M');// chưa cần chạy đoạn này, đưa memory_limit lên 500 có thể export dc khoảng 5000 row - chưa test
                $toExcel = new ToExcel();
                $toExcel->summaryCheckAgentIndex();
            }
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }
}
