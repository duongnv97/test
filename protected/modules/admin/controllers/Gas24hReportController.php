<?php
class Gas24hReportController extends AdminController 
{
    public $pluralTitle = "Báo cáo theo tháng";
    public $singleTitle = "Báo cáo theo tháng";
    
    /** @Author: Pham Thanh Nghia 2018
     *  @Todo:
     **/
    public function actionAgentMonthly()
    {
        $this->pageTitle = 'App / Call báo cáo';
        try{
            $model = new SellReport();
            $model->statistic_year = date('Y');
//            $model->statistic_year = '2017'; //test
            $model->province_id_agent = array(GasProvince::TP_HCM);
            $model->sort_type_status = Sell::SOURCE_APP; // default sort app
            $data = array();
            if (isset($_GET['SellReport'])) {
                $model->attributes = $_GET['SellReport'];
            }
            $data = $model->searchAgentMonthly();
            
            $_SESSION['data-excel'] = $data;
            $this->exportExcel($model);
            $this->render('agentMonthly/index',array(
                    'model' => $model, 
                    'actions' => $this->listActionsCanAccess,
                    'data' => $data,
                ));
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }
    public function exportExcel($model){
        try{
            if(!$model->canExportExcel() || !isset($_GET['ExportExcel'])){
                return ;
            }
        if(isset($_SESSION['data-excel'])){
            ini_set('memory_limit','2000M');// chưa cần chạy đoạn này, đưa memory_limit lên 500 có thể export dc khoảng 5000 row - chưa test
            $mExportList = new ExportList();
            $mExportList->exportAgentMonthlyGas24h();
        }
//        $this->redirect(array('index'));
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }
    
    /** @Author: HOANG NAM 31/07/2018
     *  @Todo: report app
     **/
    public function actionReportApp(){
        $this->pageTitle = 'Tổng hợp tháng';
        $this->allowReportApp();
        try{
            $model = new SellReport();
            $model->unsetAttributes();
            $model->statistic_year      = date('Y');
            $model->sort_type_status    = SellReport::SORT_GAS;
            $model->search_time    = 1;
            $model->province_id_agent = [GasProvince::TP_HCM];
            if (isset($_POST['SellReport'])) {
                $model->attributes = $_POST['SellReport'];
            }
            $this->render('reportApp/index',array(
                'model' => $model, 
                'actions' => $this->listActionsCanAccess,
            ));
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }
    // check quyền cho 1 số user đc phép view
    public function allowReportApp(){
        $cUid = MyFormat::getCurrentUid();
        $cRole = MyFormat::getCurrentRoleId();
        $aUidAllow  = [
            GasLeave::UID_CHIEF_MONITOR,
        ];
        $aRoleAllow = [ROLE_MONITORING_MARKET_DEVELOPMENT];
        if(in_array($cRole, $aRoleAllow) && !in_array($cUid, $aUidAllow)){
            $this->redirect(Yii::app()->createAbsoluteUrl(''));
        }
    }
    
    /** @Author: KHANH TOAN Sep 18 2018
    *  @Todo: báo cáo telesale 
    *  @Param:
    **/
    public function actionReportTeleSale() 
    {
        $this->pageTitle = 'Telesale Code & bình quay về theo đại lý';
        try{
        $model = new Telesale('search');
        $model->unsetAttributes();  // clear any default values
        $model->date_from   = '01-'.date('m-Y');
        $model->date_to     = date('d-m-Y');
        $model->fromPttt    = Telesale::BQV_TELESALE;
        if(isset($_GET['Telesale'])){
            $model->attributes = $_GET['Telesale'];
        }
        $_SESSION['modelEmployees'] = $model;
        $this->exportExcelReportTelesale($model);
        $this->render('reportTelesale/index',array(
            'model' => $model,
            'data' => $model->getReportTeleSale(),
            'actions' => $this->listActionsCanAccess,
        ));
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }
    
    public function exportExcelReportTelesale($model){
        try{
            if(!$this->canExcelReportTelesale() || !isset($_GET['ExportExcel'])){
                return ;
            }
            if(isset($_SESSION['data-report-telesale'])){
                ini_set('memory_limit','2000M');// chưa cần chạy đoạn này, đưa memory_limit lên 500 có thể export dc khoảng 5000 row - chưa test
                $mToExcelList = new ToExcelList();
                $mToExcelList->exportReportTeleSale();
            }
//        $this->redirect(array('index'));
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }
    
    /** @Author: NGUYEN KHANH TOAN Aug 15 ,2018
     *  @Todo: xet quyen cho phep xuat excel r
     *  @Param:
     **/
    public function canExcelReportTelesale() {
        return true;
    }
    
    /** @Author: KHANH TOAN Dec 08 2018
     *  @Todo: báo cáo KH Excel
     *  @Param:
     **/
    public function actionTelesaleSource() {
        $this->pageTitle = 'BC KH Excel';
        try{
        $model = new CustomerDraft();
//        $model->updateDataOld();
        $model->unsetAttributes();  // clear any default values
        $model->date_from   = '01-'.date('m-Y');
        $model->date_to     = date('d-m-Y');
        if(isset($_GET['CustomerDraft'])){
            $model->attributes = $_GET['CustomerDraft'];
        }
        $_SESSION['model-customer-draft'] = $model;
        $this->exportExcelTelesaleSource();
        $this->render('telesaleSource/index',array(
            'model' => $model,
            'data' => $model->getReportTelesaleSource(),
            'actions' => $this->listActionsCanAccess,
        ));
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }
    
    public function exportExcelTelesaleSource(){
        try{
            if(!$this->canExcelTelesaleSource() || !isset($_GET['ExportExcel'])){
                return ;
            }
            if(isset($_SESSION['data-telesale-source'])){
                ini_set('memory_limit','2000M');// chưa cần chạy đoạn này, đưa memory_limit lên 500 có thể export dc khoảng 5000 row - chưa test
                $mToExcelList = new ToExcelList();
                $mToExcelList->exportTelesaleSource();
            }
//        $this->redirect(array('index'));
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }
    
    /** @Author: NGUYEN KHANH TOAN Dec 12 ,2018
     *  @Todo: xet quyen cho phep xuat excel 
     *  @Param:
     **/
    public function canExcelTelesaleSource() {
        return true;
    }
    
    /** @Author: DuongNV 8 Jun,19
     *  @Todo: Thống kê số ảo
     **/
    public function actionCheckVirtualPhone() {
        try{
            error_reporting(1);
            ini_set('memory_limit','3000M');
            $this->singleTitle  = 'Thống kê số ảo';
            $this->pluralTitle  = 'Thống kê số ảo';
            $model              = new Users();
            $model->date_from   = '01-08-2018';
            $model->date_to     = '31-08-2018';
            $aData1 = $aData2 = [];
            if (isset($_POST['Users'])) {
                $model->date_from   = $_POST['Users']['date_from'];
                $model->date_to     = $_POST['Users']['date_to'];
                $mGas24hReport      = new Gas24hReport();
                $mGas24hReport->getCheckVirtualPhoneReport($model->date_from, $model->date_to, $aData1, $aData2);
            }
            
            $this->render('checkVirtualPhone/index',array(
                'model'     => $model,
                'aData1'    => $aData1,
                'aData2'    => $aData2,
                'actions'   => $this->listActionsCanAccess,
            ));
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }
}
