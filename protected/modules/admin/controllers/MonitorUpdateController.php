<?php

class MonitorUpdateController extends AdminController 
{
    public $pluralTitle = 'Chi tiết lỗi PVKH';
    public $singleTitle = 'Chi tiết lỗi PVKH';

    public function actionIndex()
    {
        $this->pageTitle = $this->pluralTitle;
        $this->indexExcel();
        try{
        $model=new MonitorUpdate('search');
        $model->unsetAttributes();  // clear any default values
        $model->date_from   = '01-'.date("m-Y");
        $model->date_to     = date("d-m-Y");
        $cRole              = MyFormat::getCurrentRoleId();
        $cUid               = MyFormat::getCurrentUid();
        $aRoleLimit         = [ROLE_EMPLOYEE_MAINTAIN, ROLE_EMPLOYEE_MARKET_DEVELOPMENT];
        if(in_array($cRole, $aRoleLimit)){
            $model->user_id     = $cUid;
        }

        if(isset($_GET['MonitorUpdate']))
            $model->attributes=$_GET['MonitorUpdate'];

        $this->render('index',array(
                'model'=>$model, 'actions' => $this->listActionsCanAccess,
        ));
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }
    
        /**
     * Returns the data model based on the primary key given in the GET variable.
    * If the data model is not found, an HTTP exception will be raised.
     * @param integer the ID of the model to be loaded
     */
    public function loadModel($id)
    {
        try{
        $model=MonitorUpdate::model()->findByPk($id);
        if($model===null)
        {
            Yii::log("The requested page does not exist.");
            throw new CHttpException(404,'The requested page does not exist.');
        }			
        return $model;
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }
   
    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id)
    {
        try{
        if(Yii::app()->request->isPostRequest){
            // we only allow deletion via POST request
            if($model = $this->loadModel($id))
            {
                if($model->delete())
                    Yii::log("Uid: " .Yii::app()->user->id. " Delete record ".  print_r($model->attributes, true), 'info');
            }

            // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
            if(!isset($_GET['ajax']))
                    $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('index'));
        }
        else
        {
            Yii::log("Uid: " .Yii::app()->user->id. " Invalid request. Please do not repeat this request again.");
            throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
        }	
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }
    
     /** @Author: KHANH TOAN Nov 26 2018
     *  @Todo: xuat excel index
     *  @Param:
     **/
    public function indexExcel() {
        try{
            if(isset($_GET['to_excel']) && isset($_SESSION['data-excel-monitorUpdate'])){
                ini_set('memory_limit','1500M');// chưa cần chạy đoạn này, đưa memory_limit lên 500 có thể export dc khoảng 5000 row - chưa test
                $mToExcel1 = new ToExcelList();
                $mToExcel1->exportMonitorUpdate();
            }
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }
    
}
