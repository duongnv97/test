<?php

class MediasController extends AdminController
{
    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id)
    {
        try {
            $this->render('view', array(
                'model' => $this->loadModel($id),
                'actions' => $this->listActionsCanAccess,
                ));
        }
        catch (exception $e) {
                    Yii::log("Exception ".  $e->getMessage(), 'error');
                    $code = 404;
                    if(isset($e->statusCode))
                        $code=$e->statusCode;
                    if($e->getCode())
                        $code=$e->getCode();
                    throw new CHttpException($code, $e->getMessage());
        }

    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate()
    {
        try {
            $model = new Medias;
            $mes = "";

            // Uncomment the following line if AJAX validation is needed
            // $this->performAjaxValidation($model);

            if (isset($_POST['Medias'])) {
                $model->attributes = $_POST['Medias'];
                if ($model->type == 'image-upload') {
                    $model->setScenario('create');
                    $uploadedFile = CUploadedFile::getInstance($model, 'url');
                    if ($uploadedFile != null) {
                        $model->real_name = $uploadedFile;
                        $fileName = $model->getImageName($uploadedFile);
                        $model->url = $fileName;
                    }
                } elseif ($model->type == 'image-link') {
                    $model->url = $_POST['image_url'];
                    $ext = substr($_POST['image_url'], -4);
                    if (!in_array($ext, array(
                        '.jpg',
                        '.png',
                        '.gif',
                        '.JPG',
                        '.PNG',
                        '.GIF',
                        ))) {
                        $mes = 'Wrong url!.';
                        $this->render('create', array(
                            'mes' => $mes,
                            'model' => $model,
                            'actions' => $this->listActionsCanAccess,
                        ));
                        return;
                        }
                        
                    $name = end(explode("/", $_POST['image_url']));
                    $sourcecode = Helper::GetImageFromUrl($_POST['image_url']);
                                        
                    $fileName = $model->getImageName($name);
        
                    //check image file
                    $checkImage = $model->saveImageUrl($fileName, $sourcecode);
                    if ($checkImage == null) {
                        $mes = 'Wrong url!.';
                        $this->render('create', array(
                            'mes' => $mes,
                            'model' => $model,
                            'actions' => $this->listActionsCanAccess,
                        ));
                        return;
                    }
                    
                    $model->url = $fileName;
                    $model->type = 'image-upload';                                           
                    
                } else {
                    $model->url = $_POST['youtube_embed'];
                }

                if ($model->save()) {
                    if (isset($uploadedFile) && $uploadedFile != null) {
                        $model->saveImage($fileName, $uploadedFile);
                    }
                    $this->redirect(array('view', 'id' => $model->id));
                }
            }

            $this->render('create', array(
                'mes' => $mes,
                'model' => $model,
                'actions' => $this->listActionsCanAccess,
                ));
        }
        catch (exception $e) {
                    Yii::log("Exception ".  $e->getMessage(), 'error');
                    $code = 404;
                    if(isset($e->statusCode))
                        $code=$e->statusCode;
                    if($e->getCode())
                        $code=$e->getCode();
                    throw new CHttpException($code, $e->getMessage());
        }
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id)
    {
        try {
            $model = $this->loadModel($id);
            $mes = "";

            // Uncomment the following line if AJAX validation is needed
            // $this->performAjaxValidation($model);

            if (isset($_POST['Medias'])) {
                $model->title = $_POST['Medias']['title'];
                $model->description = $_POST['Medias']['description'];

                if ($model->type == 'image-upload') {
                    $file = Yii::app()->basePath . '/../upload/uploaded_' . $model->user_id . '/' .
                        $model->url;
                }

                if ($_POST['Medias']['type'] == 'image-link') {
                    $model->url = $model->url = $_POST['image_url'];
                    $ext = substr($_POST['image_url'], -4);
                    if (!in_array($ext, array(
                        '.jpg',
                        '.png',
                        '.gif',
                        '.JPG',
                        '.PNG',
                        '.GIF',
                        ))) {
                        $mes = 'Wrong url!.';
                        $this->render('create', array(
                            'mes' => $mes,
                            'model' => $model,
                            'actions' => $this->listActionsCanAccess,
                        ));
                        return;
                        }
                    $name = end(explode("/", $_POST['image_url']));
                    $sourcecode = Helper::GetImageFromUrl($_POST['image_url']);
                                        
                    $fileName = $model->getImageName($name);
        
                    //check image file
                    $checkImage = $model->saveImageUrl($fileName, $sourcecode);
                    if ($checkImage == null) {
                        $mes = 'Wrong url!.';
                        $this->render('create', array(
                            'mes' => $mes,
                            'model' => $model,
                            'actions' => $this->listActionsCanAccess,
                        ));
                        return;
                    }
                    
                    $model->url = $fileName;
                    $model->type = 'image-upload'; 
                }
                
                if ($_POST['Medias']['type'] != null && $_POST['Medias']['type'] !=
                    'image-upload' && $model->type == 'image-upload') {
                    $model->deleteImage();
                }
                
                if ($_POST['Medias']['type'] == 'video-youtube') {
                    $model->url = $_POST['youtube_embed'];
                }
                if ($_POST['Medias']['type'] == 'image-upload') {                    
                    $uploadedFile = CUploadedFile::getInstance($model, 'url');
                    $fileName = $model->getImageName($uploadedFile);
                    $model->real_name = $uploadedFile;
                }

                if ($model->type == 'image-upload' && $_POST['Medias']['type'] == 'image-upload') {
                    if ($uploadedFile != null) {                        
                        $model->deleteImage();
                        $model->url = $fileName;
                    }
                }
                if ($model->type != 'image-upload' && $_POST['Medias']['type'] == 'image-upload')
                {
                    $model->setScenario('create');
                    $model->url = $fileName;
                }                    
                    
                //$model->type = $_POST['Medias']['type'];

                if ($model->save()) {
                    if (isset($uploadedFile) && $uploadedFile != null) {
                        $model->saveImage($fileName, $uploadedFile);
                    }
                    $this->redirect(array('view', 'id' => $model->id));
                }
            }

            $this->render('update', array(
                'mes' => $mes,
                'model' => $model,
                'actions' => $this->listActionsCanAccess,
                ));
        }
        catch (exception $e) {
                    Yii::log("Exception ".  $e->getMessage(), 'error');
                    $code = 404;
                    if(isset($e->statusCode))
                        $code=$e->statusCode;
                    if($e->getCode())
                        $code=$e->getCode();
                    throw new CHttpException($code, $e->getMessage());
        }
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id)
    {
        try {
            if (Yii::app()->request->isPostRequest) {
                // we only allow deletion via POST request
                if ($model = $this->loadModel($id)) {
                    if ($model->delete()) {
                        if ($model->type == 'image-upload') {
                            $model->deleteImage();
                        }
                        Yii::log("Delete record " . print_r($model->attributes, true), 'info');
                    }

                }

                // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
                if (!isset($_GET['ajax']))
                    $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('index'));
            } else {
                Yii::log("Invalid request. Please do not repeat this request again.");
                throw new CHttpException(400,
                    'Invalid request. Please do not repeat this request again.');
            }
        }
        catch (exception $e) {
                    Yii::log("Exception ".  $e->getMessage(), 'error');
                    $code = 404;
                    if(isset($e->statusCode))
                        $code=$e->statusCode;
                    if($e->getCode())
                        $code=$e->getCode();
                    throw new CHttpException($code, $e->getMessage());
        }
    }

    /**
     * Manages all models.
     */
    public function actionIndex()
    {
        try {
            $model = new Medias('search');
            $model->unsetAttributes(); // clear any default values
            if (isset($_GET['Medias']))
                $model->attributes = $_GET['Medias'];

            $this->render('index', array(
                'model' => $model,
                'actions' => $this->listActionsCanAccess,
                ));
        }
        catch (exception $e) {
                    Yii::log("Exception ".  $e->getMessage(), 'error');
                    $code = 404;
                    if(isset($e->statusCode))
                        $code=$e->statusCode;
                    if($e->getCode())
                        $code=$e->getCode();
                    throw new CHttpException($code, $e->getMessage());
        }
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer the ID of the model to be loaded
     */
    public function loadModel($id)
    {
        try {
            $model = Medias::model()->findByPk($id);
            if ($model === null) {
                Yii::log("The requested page does not exist.");
                throw new CHttpException(404, 'The requested page does not exist.');
            }
            return $model;
        }
        catch (exception $e) {
                    Yii::log("Exception ".  $e->getMessage(), 'error');
                    $code = 404;
                    if(isset($e->statusCode))
                        $code=$e->statusCode;
                    if($e->getCode())
                        $code=$e->getCode();
                    throw new CHttpException($code, $e->getMessage());
        }
    }

    /**
     * Performs the AJAX validation.
     * @param CModel the model to be validated
     */
    protected function performAjaxValidation($model)
    {
        try {
            if (isset($_POST['ajax']) && $_POST['ajax'] === 'medias-form') {
                echo CActiveForm::validate($model);
                Yii::app()->end();
            }
        }
        catch (exception $e) {
                    Yii::log("Exception ".  $e->getMessage(), 'error');
                    $code = 404;
                    if(isset($e->statusCode))
                        $code=$e->statusCode;
                    if($e->getCode())
                        $code=$e->getCode();
                    throw new CHttpException($code, $e->getMessage());
        }
    }
}
