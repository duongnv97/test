<?php
/** @Author: Pham Thanh Nghia Aug 13, 2018
 *  @Todo: 
 *  @Param:
 **/

class GasScheduleNotifyHistoryController extends AdminController 
{
    public $pluralTitle = 'Notify App';
    public $singleTitle = 'Notify App';
    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id)
    {
        $this->pageTitle = 'Xem ';
        try{
        $this->render('view',array(
                'model'=>$this->loadModel($id), 'actions' => $this->listActionsCanAccess,
        ));
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }

    }
    /**
     * Manages all models.
     */
    public function actionIndex()
    {
        $this->pageTitle = 'Notify app';
        try{
        $model=new GasScheduleNotifyHistory('search');
        $model->unsetAttributes();  // clear any default values
        if(isset($_GET['GasScheduleNotifyHistory']))
                $model->attributes=$_GET['GasScheduleNotifyHistory'];

        $_SESSION['modelScheduleNotifyHistory'] = $model;
        $this->exportExcel($model);
        
        $this->render('index',array(
                'model'=>$model, 'actions' => $this->listActionsCanAccess,
        ));
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
    * If the data model is not found, an HTTP exception will be raised.
     * @param integer the ID of the model to be loaded
     */
    public function loadModel($id)
    {
        try{
        $model=GasScheduleNotifyHistory::model()->findByPk($id);
        if($model===null)
        {
            Yii::log("The requested page does not exist.");
            throw new CHttpException(404,'The requested page does not exist.');
        }			
        return $model;
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }
    
    public function exportExcel($model){
        try{
            if(!$model->canExportExcel() || !isset($_GET['ExportExcel'])){
                return ;
            }
            if(isset($_SESSION['dataScheduleNotifyHistory'])){
                ini_set('memory_limit','2000M');// chưa cần chạy đoạn này, đưa memory_limit lên 500 có thể export dc khoảng 5000 row - chưa test
                $mToExcelList = new ToExcelList();
                $mToExcelList->exportGasScheduleNotifyHistory();
            }
//        $this->redirect(array('index'));
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }

}
