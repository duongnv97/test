<?php

class TransactionController extends AdminController 
{
    public $pluralTitle = "Đặt hàng qua App";
    public $singleTitle = "Đặt hàng qua App";
    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id)
    {
        $this->pageTitle = 'Xem ';
        $this->layout   = 'ajax';
        try{
            $model = $this->loadModel($id);
            $cRole = MyFormat::getCurrentRoleId();
            if($cRole == ROLE_SUB_USER_AGENT):
                if(MyFormat::getAgentId() != $model->agent_id):
                    $this->redirect(array('index'));
                endif;
            endif;

            $this->render('view',array(
                    'model'=>$model, 'actions' => $this->listActionsCanAccess,
            ));
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate()
    {
        $this->pageTitle = 'Tạo Mới ';
        try{
        $model=new TransactionHistory('create');

        if(isset($_POST['Transaction']))
        {
            $model->attributes=$_POST['Transaction'];
            $model->validate();
            if(!$model->hasErrors()){
                $model->save();
                Yii::app()->user->setFlash( MyFormat::SUCCESS_UPDATE, "Thêm mới thành công." );
                $this->redirect(array('create'));
            }
        }
        $this->render('create',array(
                'model'=>$model, 'actions' => $this->listActionsCanAccess,
        ));
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
//    public function actionUpdate($id)
//    {
//        $this->pageTitle = 'Cập Nhật ';
//        try{
//        $model=$this->loadModel($id);
//        $model->scenario = 'update';
//        if(isset($_POST['TransactionHistory'])){
//            $model->attributes=$_POST['TransactionHistory'];
//            $model->validate();
//            if(!$model->hasErrors()){
//                $model->save();
//                Yii::app()->user->setFlash( MyFormat::SUCCESS_UPDATE, "Cập nhật thành công." );
//                $this->redirect(array('update','id'=>$model->id));
//            }							
//        }
//
//        $this->render('update',array(
//                'model'=>$model, 'actions' => $this->listActionsCanAccess,
//        ));
//        }catch (Exception $exc){
//            GasCheck::CatchAllExeptiong($exc);
//        }
//    }
    
    public function loadModel($id){
        try{
        $model=TransactionHistory::model()->findByPk($id);
        if($model===null)
        {
            Yii::log("The requested page does not exist.");
            throw new CHttpException(404,'The requested page does not exist.');
        }			
        return $model;
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }
    public function actionDelete($id)
    {
        try{
        if(Yii::app()->request->isPostRequest){
            // we only allow deletion via POST request
            if($model = $this->loadModel($id))
            {
                if($model->delete())
                    Yii::log("Uid: " .Yii::app()->user->id. " Delete record ".  print_r($model->attributes, true), 'info');
            }

            // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
            if(!isset($_GET['ajax']))
                    $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('index'));
        }
        else
        {
            Yii::log("Uid: " .Yii::app()->user->id. " Invalid request. Please do not repeat this request again.");
            throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
        }	
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }
    
    /**
     * @Author: DungNT Mar 11, 2017
     * @Todo: Cập nhật ext cho current User
     */
    public function handleUpdateExt() {
        if(isset($_GET['UpdateExt'])){
            CacheSession::setCookie(CacheSession::CURRENT_USER_EXT, $_GET['UpdateExt']);
            $mAppCache = new AppCache();
            $mAppCache->setCallCenterExt($_GET['UpdateExt'], MyFormat::getCurrentUid());
        }
    }
    
    public function handleListCall() {
        if(isset($_GET['ListCall'])){
            $aRoleLimit = [ROLE_MONITORING_MARKET_DEVELOPMENT];
            $cRole      = MyFormat::getCurrentRoleId();
//            if(in_array($cRole, $aRoleLimit)){
            if(0){
                $this->redirect(Yii::app()->createAbsoluteUrl('/'));
            }
            $this->pageTitle = 'Call History';
            $mCall = new Call();
            $mCall->unsetAttributes();  // clear any default values
            $mCall->initParamDate();
            
            if(isset($_GET['Call'])){
                $mCall->attributes=$_GET['Call'];
            }
            $this->render('Call/ListCall',array(
                'actions' => $this->listActionsCanAccess,
                'mCall' => $mCall,
            ));
            die;
        }
    }
    /** @Author: DungNT Jul 12, 2017
     * @Todo: xử lý theo dõi đơn hàng toàn hệ thống
     */
    public function handleMonitor() {
        if(isset($_GET['monitor_order'])){
            $this->pageTitle = 'Giám sát đơn hàng';
            $model = new TransactionHistory();
            $model->unsetAttributes();  // clear any default values
            if(isset($_GET['TransactionHistory'])){
                $model->attributes=$_GET['TransactionHistory'];
            }
            $this->render('Monitor/monitor',array(
                'actions' => $this->listActionsCanAccess,
                'model' => $model,
            ));
            die;
        }
    }
    /** @Author: DungNT Aug 05, 2017
     * @Todo: Cập nhật trạng thái cuộc gọi failed
     */
    public function handleTestCall() {
        if(isset($_GET['TestCall']) && $_GET['id']){
            $model = Call::model()->findByPk($_GET['id']);
            if($model){
                $field_update           = 'failed_note';
                $cUid = MyFormat::getCurrentUid();
                if(in_array($cUid, $model->getArrayAudit())){
                    $model->audit_id    = $cUid;
                    $field_update       = 'audit_note';
                }
                if(empty($model->failed_status_old)){
                    $model->failed_status_old   = $model->failed_status;// Now0117 xử lý tách cuộc gọi chưa phân loại đc audit check
                }
                $model->failed_status   = $_POST['type_call_test'];
                $model->$field_update   = $_POST['note'];
                $model->agent_id        = isset($_POST['agent_id']) ? $_POST['agent_id'] : $model->agent_id;
                $model->score           = isset($_POST['score']) ? $_POST['score'] : $model->score;
                $model->setCallFailed();
            }
            die;
        }
    }

    /**
     * Manages all models.
     */
    public function actionIndex()
    {
//        $mAppCache = new AppCache();
//        $mAppCache->setCallCenterExt(1999, MyFormat::getCurrentUid());
//        CacheSession::setCookie(CacheSession::CURRENT_USER_EXT, '0');die;
        $cRole = MyFormat::getCurrentRoleId();
        $aRoleAllow = [ROLE_ADMIN, ROLE_CALL_CENTER];
        if(in_array($cRole, $aRoleAllow) ){
//            $this->layout   = 'ajax';
        }
        
        $this->pageTitle = 'Thông báo đặt hàng APP + Điều Phối';
        $this->handleCallToExcel();
        $this->handleTestCall();
        $this->handleMonitor();
        $this->handleListCall();
        $this->handleUpdateExt();
        $this->handleChangeStatus();
        $this->handleTransConfirmRead();
        $this->handleConfirmReadOrder();
        $this->handleMonitorNote();
        $this->handleHideMonitorNote();
        $this->handleViewCall();
        try{
        $model=new TransactionHistory('search');
        $model->handleChangeRole();
        $this->handleChangeNotPaid($model);
        
        $model->unsetAttributes();  // clear any default values
        if(isset($_GET['TransactionHistory'])){
            $model->attributes=$_GET['TransactionHistory'];
        }

        $this->render('index',array(
            'model'=>$model, 'actions' => $this->listActionsCanAccess,
//            'mCall' => $mCall,
        ));
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }
    
    /**
     * @Author: DungNT Nov 06, 2016
     * @Todo: xử lý change status của app
     */
    public function handleChangeStatus() {
        if(isset($_GET['ChangeStatus']) && isset($_GET['id']) && isset($_POST['status'])){
            $model = $this->loadModel($_GET['id']);
            $model->handleChangeStatus();
            die;
        }
    }
    
    /** @Author: DungNT Nov 30, 2016
     * @Todo: xử lý confirm read transaction new
     */
    public function handleTransConfirmRead() {
        if(isset($_GET['confirm_read']) && isset($_GET['id']) ){
            if(empty($_GET['id'])){
                throw new Exception('handleTransConfirmRead empty id need fix');
            }
            $model = $this->loadModel($_GET['id']);
            $model->handleTransConfirmRead();
            die;
        }
    }
    /** @Author: DungNT Nov 01, 2016
     * @Todo: xử lý confirm read order điều phối new
     */
    public function handleConfirmReadOrder() {
        if(isset($_GET['ConfirmReadOrder']) && isset($_GET['id']) ){
            if(empty($_GET['id'])){
                throw new Exception('handleConfirmReadOrder empty id need fix');
            }
            $model = GasSocketNotify::model()->findByPk($_GET['id']);
            if(is_null($model)){
                die;
            }
//            $model = MyFormat::loadModelByClass($_GET['id'], 'GasSocketNotify');
            $model->handleConfirmReadOrder();
            die;
        }
    }
    
    /** @Author: DungNT Dec 17, 2016
     * xử lý đổi trạng thái của đơn thành thành chưa thu tiền, trong trường hợp giao nhận bấm nhầm thu tiền
     */
    public function handleChangeNotPaid($model) {
        if(isset($_GET['ChangeNotPaid'])){
            if(empty($_GET['id'])){
                throw new Exception('handleChangeNotPaid empty id need fix');
            }
            if($model->allowChangeNotPaid()){
                $model = $this->loadModel($_GET['id']);
                $model->handleChangeNotPaid();
                $msg = 'Đổi trạng thái chưa thu tiền thành công';
                $notifyType = MyFormat::SUCCESS_UPDATE;
            }else{
                $msg = 'Đổi trạng thái chưa thu tiền thất bại';
                $notifyType = MyFormat::ERROR_UPDATE;
            }
            Yii::app()->user->setFlash($notifyType, $msg);
            $url = Yii::app()->createAbsoluteUrl('admin/transaction/index', array('order_status'=>1));
            $this->redirect($url);
        }
    }

    /** @Author: DungNT Apr 24, 2017
     * render Place Autocomplete
     */
    public function actionGooglePlace(){
        $this->pageTitle = 'Tìm địa chỉ KH và đại lý gần khách hàng nhất';
        $this->layout='ajax';
        $this->render('google/gPlaceIndex', []);
    }
    
    /** @Author: DungNT Aug1017
     *  @Todo: Export list call to excel
     */
    public function handleCallToExcel() {
        if(isset($_GET['ExportExcel'])){
            ini_set('memory_limit','1500M');
            $mToExcel = new ToExcel();
            $mToExcel->callList();
        }
    }
    
    /** @Author: DungNT Aug 13, 2017
     * xử lý Cập nhật note cho 1 đơn hàng
     */
    public function handleMonitorNote() {
        if(isset($_GET['MonitorNote']) && isset($_POST['content'])){
            try{
            $json = ['success'=>true, 'msg'=>'ok'];
            $_POST['content'] = trim($_POST['content']);
            if(empty($_GET['transaction_history_id']) || empty($_POST['content'])){
                throw new Exception('Không thể cập nhật trạng thái');
            }
            $mTranHistory       = new TransactionHistory();
            $mTranHistory->id   = $_GET['transaction_history_id'];
            $model = new GasComment();
            $model->content     = $_POST['content'];
            $model->SaveRow($mTranHistory, GasComment::TYPE_3_MONITOR_HGD);
            $json['msg'] = date('H:i');
            } catch (Exception $ex) {
                $json['msg'] = 'Có lỗi xảy ra, không thể tạo ghi chú';
            }
            echo CJavaScript::jsonEncode($json);die; 
        }
    }
    
    /** @Author: DungNT Aug 13, 2017
     * xử lý Cập nhật note cho 1 đơn hàng
     */
    public function handleHideMonitorNote() {
        if(isset($_GET['HideNote'])){
            $session=Yii::app()->session;
            echo '<pre>';
            print_r($_GET);
            echo '</pre>';
            if($_GET['HideNote'] == 1){// is hide note
                if(!isset($session['ARR_MONITOR_HIDE'])){
                    $session['ARR_MONITOR_HIDE']            = [$_GET['transaction_history_id']];
                }else{
                    $aId = $session['ARR_MONITOR_HIDE'];
                    $aId[$_GET['transaction_history_id']]   = $_GET['transaction_history_id'];
                    $session['ARR_MONITOR_HIDE']            = $aId;
                }
            }elseif($_GET['HideNote'] == 2){// remove id hide note
                if(isset($session['ARR_MONITOR_HIDE'])){
                    $aId = $session['ARR_MONITOR_HIDE'];
                    if(($key = array_search($_GET['transaction_history_id'], $aId)) !== false) {
                        unset($aId[$key]);
                    }
                    $session['ARR_MONITOR_HIDE']        = $aId;
                }
            }
            die;
        }
    }
    
    /** @Author: DungNT Aug 19, 2017
     * @Todo: đổi change agent + xem số đt của giao nhận
     */
    public function actionChangeAgent(){
        $this->pageTitle = 'Đổi đại lý của giao nhận + PTTT (CCS)';
        try{
            $model = new TransactionHistory();
            if(isset($_POST['TransactionHistory'])){
                $model->attributes=$_POST['TransactionHistory'];
                $this->checkChangeAgent($model);
                if($model->changeAgentEmployee()){
                    Yii::app()->user->setFlash( MyFormat::SUCCESS_UPDATE, 'Đổi đại lý thành công');
                }else{
                    Yii::app()->user->setFlash( MyFormat::ERROR_UPDATE, 'Đổi đại lý thất bại');
                }
                $this->redirect(['ChangeAgent']);
            }
            $this->render('Agent/ChangeAgent',array(
                'model'=>$model, 'actions' => $this->listActionsCanAccess,
            ));
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }
    
    public function checkChangeAgent($model) {
        if(in_array($model->agent_id, GasCheck::getAgentNotGentAuto())){
            throw new Exception('Nơi công tác của nhân viên không đúng, PVKH và PTTT phải có nơi công tác là đại lý, không phải kho trạm');
        }
    }
    
    public function handleViewCall()
    {
        if(!isset($_GET['ViewCall'])){
            return ;
        }
        $id = $_GET['ViewCall'];
        try{
            $model = MyFormat::loadModelByClass($id, 'Call');
            $this->pageTitle = $model->getCallNumber(). ' - xem thông tin cuộc gọi';
            $this->render('Call/ViewCall',array(
                'model' => $model, 'actions' => $this->listActionsCanAccess,
            ));
            die;
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }
    
     /** @Author: DungNT Jul 09, 2019 
     * @Todo: change agent - Nơi công tác của KTKV+CV
     */
    public function actionChangeAgentKtkv(){
        $this->pageTitle = 'Đổi đại lý của KTKV + CV';
        try{
            $model = new TransactionHistory();
            if(isset($_POST['TransactionHistory'])){
                $model->attributes=$_POST['TransactionHistory'];
//                $this->checkChangeAgent($model);
                if($model->changeAgentKtkv()){
                    Yii::app()->user->setFlash( MyFormat::SUCCESS_UPDATE, 'Đổi đại lý thành công');
                }else{
                    Yii::app()->user->setFlash( MyFormat::ERROR_UPDATE, 'Đổi đại lý thất bại');
                }
                $this->redirect(['ChangeAgentKtkv']);
            }
            $this->render('Agent/ChangeAgentKtkv',array(
                'model'=>$model, 'actions' => $this->listActionsCanAccess,
            ));
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }
    
    
}
