<?php

class GasBussinessContractController extends AdminController 
{
    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id)
    {
        
        $this->pageTitle = 'Xem ';
        $this->layout='ajax';
        try{
            $model = $this->loadModel($id);
            $model->mComment = new GasComment('spancop_comment');
            $aRoleCheck = array(ROLE_SUB_USER_AGENT, ROLE_SALE);
            if(in_array(Yii::app()->user->role_id, $aRoleCheck) && Yii::app()->user->id != $model->uid_login){
                die('<script type="text/javascript">parent.$.fn.colorbox.close();</script>'); 
            }
            $this->HandlePostComment($model);
            $this->render('view',array(
                'model'=>$model, 'actions' => $this->listActionsCanAccess,
            ));
            }
        catch (Exception $exc)
        {
            Yii::log("Uid: " .Yii::app()->user->id. " Exception ".  $exc->getMessage(), 'error');
            $code = 404;
            if(isset($exc->statusCode))
                $code=$exc->statusCode;
            if($exc->getCode())
                $code=$exc->getCode();
            throw new CHttpException($code, $exc->getMessage());
        }
    }
    
    /**
     * @Author: ANH DUNG Sep 27, 2015
     * @Todo: handle save comment from view
     */
    public function HandlePostComment($model) {
        if(isset($_POST['GasComment'])){
            $model->mComment->attributes=$_POST['GasComment'];
            $model->mComment->validate();
            if(!$model->mComment->hasErrors()){
                $model->mComment->SaveRow($model, GasComment::TYPE_1_SPANCOP);
                Yii::app()->user->setFlash( MyFormat::SUCCESS_UPDATE, "Cập nhật báo cáo tình hình thành công." );
                $this->redirect(array('view','id'=>$model->id));
            }else{
                $this->render('view',array(
                    'model'=>$model, 'actions' => $this->listActionsCanAccess,
                ));
                die;
            }
        }
    }
    
    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate()
    {
        $this->pageTitle = 'Tạo Mới ';
        $this->layout='ajax';
        try
        {
        $model=new GasBussinessContract('agent_create');
//            $model->date_plan = date('d/m/Y');
        $this->ResetScenario($model);
        if(isset($_POST['GasBussinessContract']))
        {
            $model->attributes=$_POST['GasBussinessContract'];
            $model->validate();
            if(!$model->hasErrors()){
                $model->save();
                GasBussinessContract::UpdateBelongToId($model, $model->id);
                GasBussinessContract::NewRecordThuongLuong($model);
                Yii::app()->user->setFlash('successUpdate', "Thêm mới thành công.");
               $this->redirect(array('create'));
            }
        }

        $this->render('create',array(
                'model'=>$model, 'actions' => $this->listActionsCanAccess,
        ));
        }
        catch (Exception $exc)
        {
            Yii::log("Uid: " .Yii::app()->user->id. " Exception ".  $exc->getMessage(), 'error');
            $code = 404;
            if(isset($exc->statusCode))
                $code=$exc->statusCode;
            if($exc->getCode())
                $code=$exc->getCode();
            throw new CHttpException($code, $exc->getMessage());
        }
    }

    public function ResetScenario($model){
        $cRole = Yii::app()->user->role_id;
        if(in_array($cRole, $model->SAME_ROLE_SALE)){
            $model->scenario = 'sale_create';
            if($model->id){
                $model->scenario = 'sale_update';
            }
        }elseif(in_array($cRole, $model->SAME_ROLE_MONITOR_AGEN)){
            $model->scenario = 'monitor_agent_create';
            if($model->id){
                $model->scenario = 'monitor_agent_update';
            }
        }
    }
    
    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id)
    {
        $this->pageTitle = 'Cập Nhật ';
        $this->layout='ajax';
        try
        {
        $model=$this->loadModel($id);
        $model->scenario = 'agent_update';
        $this->ResetScenario($model);
        $model->date_plan = MyFormat::dateConverYmdToDmy($model->date_plan);
        if(!GasCheck::AgentCanUpdateBussinessContract($model)){
            die('<script type="text/javascript">parent.$.fn.colorbox.close();</script>'); 
        }

        if(isset($_POST['GasBussinessContract']))
        {
            $model->attributes=$_POST['GasBussinessContract'];
            // $this->redirect(array('view','id'=>$model->id));
            $model->validate();
            if(!$model->hasErrors()){
                $model->last_update_time = date('Y-m-d H:i:s');
                if($model->row_auto==1 && $model->status == GasBussinessContract::STATUS_THUONG_LUONG){
                    $model->still_thuong_luong = 1;
                }
                $model->save();
                GasBussinessContract::NewRecordThuongLuong($model);
                Yii::app()->user->setFlash('successUpdate', "Cập nhật thành công.");
                $this->redirect(array('update','id'=>$model->id));
            }							
        }

        $this->render('update',array(
                'model'=>$model, 'actions' => $this->listActionsCanAccess,
        ));
        }
        catch (Exception $exc)
        {
            Yii::log("Uid: " .Yii::app()->user->id. " Exception ".  $exc->getMessage(), 'error');
            $code = 404;
            if(isset($exc->statusCode))
                $code=$exc->statusCode;
            if($exc->getCode())
                $code=$exc->getCode();
            throw new CHttpException($code, $exc->getMessage());
        }
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id)
    {
            try
            {
            if(Yii::app()->request->isPostRequest)
            {
                    // we only allow deletion via POST request
                    if($model = $this->loadModel($id))
                    {
                        GasBussinessContract::DeleteAllSubModelAutoGenThuongLuong($model);
                        if($model->delete())
                            Yii::log("Uid: " .Yii::app()->user->id. " Delete record ".  print_r($model->attributes, true), 'info');
                    }

                    // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
                    if(!isset($_GET['ajax']))
                            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('index'));
            }
            else
            {
                Yii::log("Uid: " .Yii::app()->user->id. " Invalid request. Please do not repeat this request again.");
                throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
            }	
            }
            catch (Exception $exc)
            {
                Yii::log("Uid: " .Yii::app()->user->id. " Exception ".  $exc->getMessage(), 'error');
                $code = 404;
                if(isset($exc->statusCode))
                    $code=$exc->statusCode;
                if($exc->getCode())
                    $code=$exc->getCode();
                throw new CHttpException($code, $exc->getMessage());
            }
    }

    public function initCurrentWeek(&$model){
        $model->year = date('Y');
        $model->month = date('m');
        $month = $model->month; // chỗ này xử lý không thể lấy tháng hiện tại dc, vì tuần có thể là tuần của tháng trước
        $year = $model->year;
        $model->number_week = GasWeekSetup::GetCurrentWeekNumber($month, $year);
        if($model->number_week == 0){
            echo "Chưa setup tuần của tháng, liên hệ admin";die; // Fix Now 04, 2014 nên báo lỗi để còn biết lối fix, trong TH quên chưa setup
        }
        $model->month = $month;
        $model->year = $year;
        GasWeekSetup::InitSessionWeekNumberByYear($model->year);
    }
    
    /**
     * Manages all models.
     */
    public function actionIndex()
    {
        GasBussinessContract::InitSessionLastIdThuongLuong();
        MyFunctionCustom::checkChangePassword();
        $this->pageTitle = 'SPANCOP Danh Sách ';
        try
        {
        $model=new GasBussinessContract('search');
        $model->unsetAttributes();  // clear any default values
        $this->initCurrentWeek($model);

        if(isset($_GET['GasBussinessContract']))
                $model->attributes=$_GET['GasBussinessContract'];

        $this->render('index',array(
                'model'=>$model, 'actions' => $this->listActionsCanAccess,
        ));
        }
        catch (Exception $exc)
        {
            Yii::log("Uid: " .Yii::app()->user->id. " Exception ".  $exc->getMessage(), 'error');
            $code = 404;
            if(isset($exc->statusCode))
                $code=$exc->statusCode;
            if($exc->getCode())
                $code=$exc->getCode();
            throw new CHttpException($code, $exc->getMessage());
        }
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
    * If the data model is not found, an HTTP exception will be raised.
     * @param integer the ID of the model to be loaded
     */
    public function loadModel($id)
    {
            try
            {
            $model=GasBussinessContract::model()->findByPk($id);
            if($model===null)
            {
                Yii::log("The requested page does not exist.");
                throw new CHttpException(404,'The requested page does not exist.');
            }			
            return $model;
            }
            catch (Exception $exc)
            {
                Yii::log("Uid: " .Yii::app()->user->id. " Exception ".  $exc->getMessage(), 'error');
                $code = 404;
                if(isset($exc->statusCode))
                    $code=$exc->statusCode;
                if($exc->getCode())
                    $code=$exc->getCode();
                throw new CHttpException($code, $exc->getMessage());
            }
    }

    /**
     * Performs the AJAX validation.
     * @param CModel the model to be validated
     */
    protected function performAjaxValidation($model)
    {
        try
        {
        if(isset($_POST['ajax']) && $_POST['ajax']==='gas-bussiness-contract-form')
        {
                echo CActiveForm::validate($model);
                Yii::app()->end();
        }
        }
        catch (Exception $exc)
        {
            Yii::log("Uid: " .Yii::app()->user->id. " Exception ".  $exc->getMessage(), 'error');
            $code = 404;
            if(isset($exc->statusCode))
                $code=$exc->statusCode;
            if($exc->getCode())
                $code=$exc->getCode();
            throw new CHttpException($code, $exc->getMessage());
        }
    }
    
    // list khách hàng đã ký hợp đồng và chưa gắn Customer đã tạo trên hệ thống
    public function actionWas_sign_contract()
    {
//        MyFunctionCustom::checkChangePassword();
        $this->pageTitle = 'Danh Sách SPANCOP đã ký hợp đồng, chưa gắn khách hàng';
        try
        {
        $model=new GasBussinessContract('search');
        $model->unsetAttributes();  // clear any default values

        if(isset($_GET['GasBussinessContract']))
                $model->attributes=$_GET['GasBussinessContract'];

        $this->render('dieuphoi/Was_sign_contract',array(
                'model'=>$model, 'actions' => $this->listActionsCanAccess,
        ));
        }
        catch (Exception $exc)
        {
            Yii::log("Uid: " .Yii::app()->user->id. " Exception ".  $exc->getMessage(), 'error');
            $code = 404;
            if(isset($exc->statusCode))
                $code=$exc->statusCode;
            if($exc->getCode())
                $code=$exc->getCode();
            throw new CHttpException($code, $exc->getMessage());
        }
    }    
    
    // list khách hàng đã ký hợp đồng và được điều phối gắn Customer đã tạo trên hệ thống
    public function actionWas_sign_contract_ready_system()
    {
        $this->pageTitle = 'Danh Sách SPANCOP đã ký hợp đồng, đã gắn khách hàng';
        try
        {
        $model=new GasBussinessContract('search');
        $model->unsetAttributes();  // clear any default values
        if(isset($_GET['GasBussinessContract']))
                $model->attributes=$_GET['GasBussinessContract'];

        $this->render('dieuphoi/Was_sign_contract_ready_system',array(
                'model'=>$model, 'actions' => $this->listActionsCanAccess,
        ));
        }
        catch (Exception $exc)
        {
            Yii::log("Uid: " .Yii::app()->user->id. " Exception ".  $exc->getMessage(), 'error');
            $code = 404;
            if(isset($exc->statusCode))
                $code=$exc->statusCode;
            if($exc->getCode())
                $code=$exc->getCode();
            throw new CHttpException($code, $exc->getMessage());
        }
    }
    
    // ajax update khách hàng cho row spancop đã ký hợp đồng
    public function actionUpdate_customer_sign_contract($id)
    {
        $this->layout='ajax';
        $model = $this->loadModel($id);
        $model->scenario = 'Update_customer_sign_contract';        
        if($model->status!=GasBussinessContract::STATUS_KY_HD || 
            ( $model->customer_id > 0 && Yii::app()->user->role_id != ROLE_ADMIN)
             ){
            die('<script type="text/javascript">parent.$.fn.colorbox.close(); parent.$.fn.yiiGridView.update("gas-bussiness-contract-grid"); </script>'); 
        }
        
        if(isset($_POST['GasBussinessContract']))
        {
            $model->attributes=$_POST['GasBussinessContract'];
            
            $model->date_assign_customer=date('Y-m-d H:i:s');
            if(empty($model->customer_id)){
                $model->date_assign_customer=null;
            }
            $model->update(array('customer_id', 'date_assign_customer'));
            die('<script type="text/javascript">parent.$.fn.colorbox.close(); parent.$.fn.yiiGridView.update("gas-bussiness-contract-grid"); </script>'); 
        }
                    
        $this->render('dieuphoi/Update_customer_sign_contract',array(
                'model'=>$model, 'actions' => $this->listActionsCanAccess,
        ));
    }
    
    // list khách hàng đã ký hợp đồng và được điều phối gắn Customer đã tạo trên hệ thống
    public function actionQuick_report()
    {
        $this->pageTitle = 'Báo Cáo SPANCOP Tóm Tắt';
        try
        {
        $model=new GasBussinessContract('search');
        $model->unsetAttributes();  // clear any default values
        $this->initCurrentWeek($model);
        if(isset($_POST['GasBussinessContract'])){
            $model->attributes=$_POST['GasBussinessContract'];
        }
        $data = GasBussinessContract::QuickReport($model);

        $this->render('Report/Quick_report',array(
            'model'=>$model,
            'data'=>$data,
            'actions' => $this->listActionsCanAccess,
        ));
        }
        catch (Exception $exc)
        {
            Yii::log("Uid: " .Yii::app()->user->id. " Exception ".  $exc->getMessage(), 'error');
            $code = 404;
            if(isset($exc->statusCode))
                $code=$exc->statusCode;
            if($exc->getCode())
                $code=$exc->getCode();
            throw new CHttpException($code, $exc->getMessage());
        }
    }
    
    /**
     * @Author: ANH DUNG Sep 11, 2014
     * @Todo: list kh lớn lấy trên 30 bình 12kg
     */
    public function actionBig_customer()
    {
    $this->pageTitle = 'SPANCOP khách hàng lớn';
    try
    {
        $model=new GasBussinessContract('search');
        $model->unsetAttributes();  // clear any default values
        $model->qty_of_big_customer = GasBussinessContract::QTY_BIG_CUSTOMER;
        if(isset($_GET['GasBussinessContract']))
                $model->attributes=$_GET['GasBussinessContract'];

        $this->render('Big_customer',array(
                'model'=>$model, 'actions' => $this->listActionsCanAccess,
        ));
    }
    catch (Exception $exc)
    {
        Yii::log("Uid: " .Yii::app()->user->id. " Exception ".  $exc->getMessage(), 'error');
        $code = 404;
        if(isset($exc->statusCode))
            $code=$exc->statusCode;
        if($exc->getCode())
            $code=$exc->getCode();
        throw new CHttpException($code, $exc->getMessage());
    }
    }
    
    /**
     * @Author: ANH DUNG Nov 17, 2014
     * @Todo: something
     */
    public function actionUpdate_guide_help($id) {
        try{
            $model = $this->loadModel($id);
            if( !GasCheck::CanUpdateGuideHelpSpancop($model) ){
                die('<script type="text/javascript">parent.$.fn.colorbox.close();</script>'); 
            }
            if(isset($_POST['GasBussinessContract']['guide_help']) && trim($_POST['GasBussinessContract']['guide_help'] != '')){
                $model->attributes=$_POST['GasBussinessContract'];
                GasBussinessContract::Update_guide_help($model);
                Yii::app()->user->setFlash(MyFormat::SUCCESS_UPDATE, "Cập nhật hướng dẫn thực hiện thành công.");
            }
            $this->redirect(array('view','id'=>$model->id));
        }
        catch (Exception $exc)
        {
            GasCheck::CatchAllExeptiong($exc);
        }
    }
    
    
}
