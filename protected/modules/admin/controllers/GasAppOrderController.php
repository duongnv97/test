<?php

class GasAppOrderController extends AdminController 
{
    public $pluralTitle = 'APP Đặt hàng bò mối';
    public $singleTitle = 'APP Đặt hàng bò mối';
    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed  
     */
    public function actionView($id)
    {
//        error_reporting(1); // var_dump(PHP_INT_SIZE);
        $this->layout = 'ajax';
        try{
            $model                  = $this->loadModel($id);
            $this->setLocationCustomer($model);
            $model->agent_id_old    = $model->agent_id;
            $model->status_old      = $model->status;// chưa sử dụng đến
            $model->scenario        = 'WebConfirm';
            $model->mapJsonFieldOneDecode('JSON_FIELD', 'json', 'baseArrayJsonDecode');
            $model->mapJsonFieldOneDecode('JSON_FIELD_INFO', 'json_info', 'baseArrayJsonDecodeV1');
            $model->getCustomerQtyRequest();
            $this->handleConfirm($model);
            $this->render('view',array(
                    'model'=>$model, 'actions' => $this->listActionsCanAccess,
            ));
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }
    
    /**
     * @Author: ANH DUNG Jan 19, 2017
     */
    public function handleConfirm($model) {
        if(isset($_POST['GasAppOrder']) && $model->canConfirm()){
            $model->attributes=$_POST['GasAppOrder'];
            $model->validate();
            if(!$model->hasErrors()){
                $model->formatDbDatetime();
                $model->handleConfirm();
                Yii::app()->user->setFlash( MyFormat::SUCCESS_UPDATE, "Xác nhận thành công" );
                die('<script type="text/javascript">parent.$.fn.colorbox.close(); parent.$.fn.yiiGridView.update("gas-app-order-grid"); </script>'); 
                $this->redirect(array('view','id'=>$model->id));
            }else{
                Yii::app()->user->setFlash( MyFormat::ERROR_UPDATE, HandleLabel::FortmatErrorsModel($model->getErrors()));
            }
        }
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id)
    {
        try{
        $model=$this->loadModel($id);
        $model->modelOld = clone $model;
        $model->mapJsonFieldOneDecode('JSON_FIELD', 'json', 'baseArrayJsonDecode');
        $model->mapJsonFieldOneDecode('JSON_FIELD_INFO', 'json_info', 'baseArrayJsonDecodeV1');
        $model->getStringGasReportDaily();
        $aSeri = $model->getArraySeriVo();
        $this->pageTitle        = $model->code_no.' - '.$model->getCustomer().' - '.$model->getDateDelivery();
        $model->dateDeliveryOld = $model->date_delivery;
        $model->date_delivery   = MyFormat::dateConverYmdToDmy($model->date_delivery, 'd-m-Y');
        $oldDate                = $model->date_delivery;
        $model->scenario        = 'WebUpdate';
        $model->isWebUpdate     = true;
        if(isset($_POST['GasAppOrder'])){
            $model->attributes=$_POST['GasAppOrder'];
            if($model->date_delivery != $oldDate){
                $model->isChangeDate = true;
            }
            if($model->setNotDebit){
                $model->status_debit = GasAppOrder::DEBIT_NO;
            }
            $model->validate();
            if(!$model->hasErrors()){
                $this->webSetDebit($model);
                $model->resetRelation();
                $model->setDbDate();
                $model->removeSomeFormat();
                if($model->setStatusNew){
                    $model->doSetStatusNew();
                    Yii::app()->user->setFlash( MyFormat::SUCCESS_UPDATE, 'Cập nhật trạng thái mới của đơn hàng thành công' );
                    $this->redirect(array('update','id'=>$model->id));
                }
                if($model->setStatusComplete){
                    $model->status       = GasAppOrder::STATUS_COMPPLETE;
                    $model->reason_false = 0;
                }
                $model->handleSaveWebUpdate();
                $model->aDetailVo = $model->info_vo;
                $model->makeGasRemain();
                $model->modelOld->saveEvent(GasAppOrder::WEB_UPDATE, '');
                $model->saveLogUpdate(LogUpdate::TYPE_2_APP_BO_MOI);
                Yii::app()->user->setFlash( MyFormat::SUCCESS_UPDATE, "Cập nhật thành công." );
                $this->redirect(array('update','id'=>$model->id));
            }else{
                Yii::app()->user->setFlash( MyFormat::ERROR_UPDATE, HandleLabel::FortmatErrorsModel($model->getErrors()));
            }
        }

        $this->render('update',array(
                'model'=>$model, 'actions' => $this->listActionsCanAccess,
        ));
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }
    
    public function forFixBug() {
        // for fix bug
//        $model->setJsonNameUser();// Sep1817 for fix change Driver Or CAr OR Phụ Xe
//        // $model->mapInfoCustomer();// Sep1817 for fix change Driver Or CAr OR Phụ Xe
//        $model->setJsonDataField('JSON_FIELD_INFO', 'json_info');// Sep1817 for fix change Driver Or CAr OR Phụ Xe
//        $model->update(['json_info']);die;// Sep1817 for fix change Driver Or CAr OR Phụ Xe
        // for fix bug
    }
    
    /** @Author: ANH DUNG Now 11, 2017
     *  @Todo: web bấm set nợ cho đơn hàng
     */
    public function webSetDebit($model) {
        if($model->webSetDebit){
            $cUid = MyFormat::getCurrentUid();
            $model->mAppUserLogin = Users::model()->findByPk($cUid);
            $model->setDebit();
            Yii::app()->user->setFlash( MyFormat::SUCCESS_UPDATE, "Cập nhật set đơn hàng nợ thành công." );
            $this->redirect(array('update','id'=>$model->id));
        }
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id)
    {
        try{
        if(Yii::app()->request->isPostRequest){
//            throw new CHttpException(400, 'IMPORTANT ! Admin setup không cho xóa, chỉ được hủy đơn hàng');// Feb2118
            // we only allow deletion via POST request
            if($model = $this->loadModel($id))
            {
                if($model->delete())
                    Yii::log("Uid: " .Yii::app()->user->id. " Delete record ".  print_r($model->attributes, true), 'info');
            }

            // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
            if(!isset($_GET['ajax']))
                    $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('index'));
        }
        else
        {
            Yii::log("Uid: " .Yii::app()->user->id. " Invalid request. Please do not repeat this request again.");
            throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
        }	
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }

    /**
     * Manages all models.
     */
    public function actionIndex()
    {
        $this->pageTitle = 'GasAppOrder App đơn hàng bò mối';
        try{
        $this->handleExportExcelList();
        $model=new GasAppOrder('search');
        $model->unsetAttributes();  // clear any default values
        $cRole      = MyFormat::getCurrentRoleId();
        $aRoleKho   = [ROLE_SCHEDULE_CAR, ROLE_MONITORING_STORE_CARD];
        
        if(in_array($cRole, $aRoleKho)){
            $model->date_from   = date('d-m-Y');
            $model->date_to     = $model->date_from;
            $model->type        = GasAppOrder::DELIVERY_BY_CAR;
        }
        if(isset($_GET['customer_id'])){
            $model->customer_id = $_GET['customer_id'];
        }
        
        if(isset($_GET['GasAppOrder']))
            $model->attributes=$_GET['GasAppOrder'];

        $this->render('index',array(
                'model'=>$model, 'actions' => $this->listActionsCanAccess,
        ));
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
    * If the data model is not found, an HTTP exception will be raised.
     * @param integer the ID of the model to be loaded
     */
    public function loadModel($id)
    {
        try{
        $model=GasAppOrder::model()->findByPk($id);
        if($model===null)
        {
            Yii::log("The requested page does not exist.");
            throw new CHttpException(404,'The requested page does not exist.');
        }			
        return $model;
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }

    /** @Author: ANH DUNG May 06, 2017
     * @Todo: view daily bò mối mới admin/gasAppOrder/daily
     */
    public function actionDaily()
    {
        $this->pageTitle = 'BC Daily bò mối';
        try{
        $this->handleDailyExportExcel();
        $model=new GasAppOrder('search');
        $model->unsetAttributes();  // clear any default values
        $model->date_from   = date('d-m-Y');
        $model->date_to     = $model->date_from;
        if(isset($_GET['GasAppOrder']))
            $model->attributes=$_GET['GasAppOrder'];

        $this->render('report/daily',array(
                'model'=>$model, 'actions' => $this->listActionsCanAccess,
        ));
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }
    
    /** @Author: ANH DUNG May 12, 2017
     * @Todo: xử lý xuất excel cho báo cáo daily
     */
    public function handleDailyExportExcel() {
        try {
            if(isset($_GET['ExportExcel'])){
                ini_set('memory_limit','3000M');
                $mExportList = new ExportList();
                $mExportList->appOrderDaily($_SESSION['data-excel']->data, new GasAppOrder());
            }
        } catch (Exception $exc) {
            GasCheck::CatchAllExeptiong($exc);
        }
    }
    
    /** @Author: ANH DUNG Aug 26, 2017
     * @Todo: xử lý xuất excel cho danh sách GasAppOrder
     */
    public function handleExportExcelList() {
        try {
            if(isset($_GET['ExportExcel'])){
                ini_set('memory_limit','2500M');
                $mExportList = new ExportList();
                $mExportList->appOrderList($_SESSION['data-excel']->data, new GasAppOrder());
            }
        } catch (Exception $exc) {
            GasCheck::CatchAllExeptiong($exc);
        }
    }
    
     /** @Author: ANH DUNG May 06, 2017
     *   @Todo: view daily bò mối mới admin/gasAppOrder/appReport
      * Report Customer use app
      * 1. tổng số KH kích hoạt từng tháng theo từng sale
      * 2. Tổng số KH đặt app từng tháng theo từng sale
     */
    public function actionAppReport()
    {
        $this->pageTitle = 'Báo cáo khách hàng dùng app';
        try{
        $model=new GasAppOrder('search');
        $model->unsetAttributes();  // clear any default values
        $rData = [];
        if(isset($_GET['potential'])){ // Khách hàng tiềm năng (Add by DuongNV Oct 10,2018)
            $model->date_from  = isset($_GET['GasAppOrder']['date_from']) ? $_GET['GasAppOrder']['date_from'] : '01-'.date('m-Y');
            $model->date_to    = isset($_GET['GasAppOrder']['date_to']) ? $_GET['GasAppOrder']['date_to'] : date('d-m-Y');
            $model->attributes = isset($_GET['GasAppOrder']) ? $_GET['GasAppOrder'] : '';
            $rData = $model->appReportPotential();
        } else { // Khách hàng bò mối
            $sta2 = new Sta2();
            $rData = $sta2->appReport($model);
        }
//        $this->render('report/AppReport',array(
        $this->render('report/app_report_button',array(
                'model'=>$model, 'actions' => $this->listActionsCanAccess,
                'rData' => $rData
            ));
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }

    /** @Author: ANH DUNG May 06, 2017
    *  @Todo: view daily bò mối mới admin/gasAppOrder/appReport
    * Report Customer use app
    * 1. tổng số KH kích hoạt từng tháng theo từng sale
    * 2. Tổng số KH đặt app từng tháng theo từng sale
     */
    public function actionSoldOut()
    {
        $this->pageTitle = 'Báo cáo xuất bán';
        $this->allowSoldOut();
        try{
        $model=new GasAppOrder('search');
        $model->unsetAttributes();  // clear any default values
        $model->date_from   = '01-'.date('m-Y');
        $model->date_to     = date('d-m-Y');
        $model->obj_id_old  = [GasProvince::TP_HCM];
        $sta2 = new Sta2();
        if(isset($_GET['GasAppOrder']))
            $model->attributes=$_GET['GasAppOrder'];
        $rData = $sta2->soldOut($model);
        $this->exportExcelReportSoldOut($model);
        Yii::app()->session['dataSoldOut'] = $rData;
        $this->render('report/SoldOut',array(
            'model'=>$model, 'actions' => $this->listActionsCanAccess,
            'rData' => $rData
        ));
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }
    
    public function allowSoldOut(){
        $cUid = MyFormat::getCurrentUid();
        $cRole = MyFormat::getCurrentRoleId();
        $aUidAllow  = [GasConst::UID_NGAN_DTM, 
            516154,// Nguyễn Thị Duyên Ben Cat - NV Kế Toán VP
            GasConst::UID_HUE_LT,
            GasLeave::UID_VIEW_ALL_TAY_NGUYEN_GIA_LAI,
            GasLeave::THUC_NH,
            GasLeave::PHUONG_PTK,
            1162981,// Trần Thi Thúy - Bình Định
            GasConst::UID_LONG_PN,
            GasConst::UID_CHIEN_BQ,
        ];
        $aRoleAllow = [ROLE_ACCOUNTING];
//        if(in_array($cRole, $aRoleAllow) && !in_array($cUid, $aUidAllow)){
//            $this->redirect(Yii::app()->createAbsoluteUrl(''));
//        }// Close Mar1519 for all NV kế toán VP view
    }
    
    /** @Author: ANH DUNG Now 08, 2017
     *  @Todo: cập nhật location cho customer
     **/
    public function setLocationCustomer($mAppOrder) {
        if(!isset($_GET['SetLocationCustomer']) || !GasConst::canUpdateLocation() || $mAppOrder->status != GasAppOrder::STATUS_COMPPLETE){
            return ;
        }
        $this->setWrongLocation($mAppOrder);
        $mEventComplete = new TransactionEvent();
        $mEventComplete->transaction_history_id = $mAppOrder->id;
        $mEventComplete->type                   = TransactionEvent::TYPE_BO_MOI;
        $mEventComplete =  $mEventComplete->getOrderComplete();

        if($mEventComplete){
            $mAppOrder->setLocationCustomer($mEventComplete->google_map);
            Yii::app()->user->setFlash( MyFormat::SUCCESS_UPDATE, 'Cập nhật thành công tọa độ vị trí khách hàng');
            $this->redirect(array('view','id'=>$mAppOrder->id));
        }
    }
    
    /** @Author: ANH DUNG Dec 02, 2017
     *  @Todo: cập nhật sai vị trí
     **/
    public function setWrongLocation($mAppOrder) {
        if(!isset($_GET['SetWrongLocation'])){
            return ;
        }
        $mAppOrder->setLocationWrong();
        Yii::app()->user->setFlash( MyFormat::SUCCESS_UPDATE, 'Cập nhật thành công sai vị trí');
        $this->redirect(array('view','id'=>$mAppOrder->id));
    }
    
    /** @Author: ANH DUNG Jan 25, 2018
     *   @Todo: view daily bò mối mới admin/gasAppOrder/reportGas24h
      * Report Customer use app Gas24h
      * bc KH app: tính từ ngày ??? => tổng số kích hoạt, tổng đã đặt thành công, hủy,
        tổng số đặt trên 1 lần, trên 2 lần, 
        Tổng số KM 1 bình,
        Tổng số KH sử dụng mã KM
     */
    public function actionReportGas24h(){
        ini_set('memory_limit','1500M');
        $this->pageTitle = 'App Gas24h Báo cáo';
        $this->allowReportGas24h();
        try{
        $sta2   = new Sta2();
        $rData  = $sta2->reportGas24h();
        $model  = new Users();
        $model->unsetAttributes();  // clear any default values
        $model->date_from   = '01-'.date('m-Y');
        $model->date_to     = date('d-m-Y');
        if(isset($_GET['Users']))
            $model->attributes=$_GET['Users'];
        $rpGas24h = new RpGas24h();
        if(isset($_GET['type'])){
            $rpGas24h->mUser = $model;
            if($_GET['type'] == 1){
                $rData  = $rpGas24h->setupByAgent();
            }
        }
        
        $this->render('report/Gas24h',array(
            'actions' => $this->listActionsCanAccess,
            'rData' => $rData,
            'model' => $model,
            'mRpGas24h' => $rpGas24h
        ));
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }
    
    /** @Author: ANH DUNG Aug 20, 2018
     *  @Todo: kiểm tra allow access report
     **/
    public function allowReportGas24h(){
        $cUid = MyFormat::getCurrentUid();
        $cRole = MyFormat::getCurrentRoleId();
        $aUidAllow  = [
            GasLeave::UID_CHIEF_MONITOR,
            GasLeave::PHUC_HV,
            GasConst::UID_PHUONG_ND,
        ];
        $aRoleAllow = [ROLE_MONITORING_MARKET_DEVELOPMENT];
        if(in_array($cRole, $aRoleAllow) && !in_array($cUid, $aUidAllow)){
            $this->redirect(Yii::app()->createAbsoluteUrl(''));
        }
    }
    
    
    /** @Author: ANH DUNG Mar 29, 2018
     *   @Todo: view sms effore admin/gasAppOrder/reportSmsHgd
      * bc ước lượng số tiền gửi tin nhắn cho KH hộ GĐ hoặc bò mối, dựa trên số đt
     */
    public function actionReportSmsHgd(){
        $this->pageTitle = 'Báo cáo ước lượng tiền gửi tin SMS';
        try{
        $model = new Users();
        $model->date_from   = '01-'.date('m-Y');
        $model->date_to     = date('d-m-Y');
        if(isset($_GET['Users']))
            $model->attributes=$_GET['Users'];
        $sta2 = new Sta2();
        $rData = $sta2->reportSmsHgd($model);
        $this->render('report/EtaSms',array(
            'model'=>$model, 'actions' => $this->listActionsCanAccess,
            'rData' => $rData
        ));
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }
    /****************** ++NGHIA + NAM ZONE 20180724******************/
    /* model/Stock.php
     * IndexStock, ViewStock, UpdateStock ,CreateStock
     */
    public function actionIndexStock(){
        $this->pageTitle = 'Quản Lý Nhập xuất STT';
        try {
        $model=new Stock('search');
        $model->unsetAttributes();
        $model->type_store_card = TYPE_STORE_CARD_EXPORT;
        if(isset($_GET['type'])){
            if($_GET['type'] == Stock::TYPE_EXPIRED){
                $model->date_expired = 10;
            }
        }else{
            $model->date_from   = date('d-m-Y');
            $model->date_to     = $model->date_from;
        }
        if(isset($_GET['Stock'])){
            $model->attributes=$_GET['Stock'];
        }
        $this->render('stock/index',array(
                'model'=>$model, 'actions' => $this->listActionsCanAccess,
        ));
        } catch (Exception $exc) {
            GasCheck::CatchAllExeptiong($exc);
        }
    }
    public function actionViewStock($id){
//        $this->layout = 'ajax';
        try{
            $model                  = $this->loadModelStock($id);
            $this->render('stock/view',array(
                    'model'=>$model, 'actions' => $this->listActionsCanAccess,
            ));
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }
    public function actionUpdateStock($id){
        $this->pageTitle= 'Nhập xuất STT';
        try{
        $model=$this->loadModelStock($id);
        $cUid = MyFormat::getCurrentUid();
        $model->mAppUserLogin = Users::model()->findByPk($cUid);
        if(isset($_POST['Stock'])){
            $model->attributes=$_POST['Stock'];
            $model->formatForSave();
            $model->validate();
            if(!$model->hasErrors()){
                 $model->update();
                Yii::app()->user->setFlash( MyFormat::SUCCESS_UPDATE, "Cập nhật thành công." );
                $this->redirect(array('gasAppOrder/updateStock','id'=>$model->id));
            }else{
                Yii::app()->user->setFlash( MyFormat::ERROR_UPDATE, HandleLabel::FortmatErrorsModel($model->getErrors()));
            }
        }

        $this->render('stock/update',array(
                'model'=>$model, 'actions' => $this->listActionsCanAccess,
        ));
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }
    public function actionCreateStock($id = ''){
        $this->pageTitle= 'Nhập xuất STT';
        try {
        $model=new Stock('search');
        if($id != ''){
            $model->app_order_id = $id;
        }
//        Lấy danh sách app 
        $mAppOrder          = $this->loadModel($id);
        $model->attributes = $mAppOrder->attributes;
        if(isset($_POST['Stock']))
        {
            $model->attributes=$_POST['Stock'];
            $model->formatForSave();
            $model->validate();
            if(!$model->hasErrors()){
//                $model->save();
                $model->handleSaveWeb();
                Yii::app()->user->setFlash('successUpdate', "Thêm mới thành công.");
               $this->redirect(array('gasAppOrder/createStock','id'=>$id));
            }				
        }
        $this->render('stock/create',array(
                'model'=>$model, 'actions' => $this->listActionsCanAccess,
        ));
        } catch (Exception $exc) {
            GasCheck::CatchAllExeptiong($exc);
        }
    }
    
    public function loadModelStock($id)
    {
        try{
        $model=Stock::model()->findByPk($id);
        if($model===null)
        {
            Yii::log("The requested page does not exist.");
            throw new CHttpException(404,'The requested page does not exist.');
        }			
        return $model;
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }
    public function actionDeleteStock($id)
    {
        try{
            if(!empty($id)){
                if($model = $this->loadModelStock($id)){
                    if($model->delete())
                        Yii::log("Uid: " .Yii::app()->user->id. " Delete record ".  print_r($model->attributes, true), 'info');
                }

                if(!isset($_GET['ajax']))
                        $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('indexStock'));
            }else{
                Yii::log("Uid: " .Yii::app()->user->id. " Invalid request. Please do not repeat this request again.");
                throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
            }	
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }
    /** @Author: Pham Thanh Nghia Oct,5 2018
     *  @Todo: export excel for gasAppOrder/soldOut
     **/
    public function exportExcelReportSoldOut($model){
        try{
            if(!$model->canExportExcelReport() || !isset($_GET['toExcel'])){
                return ;
            }
            if(isset($_GET['toExcel']) && !empty(Yii::app()->session['dataSoldOut'])){
//                ini_set('memory_limit','2000M');// chưa cần chạy đoạn này, đưa memory_limit lên 500 có thể export dc khoảng 5000 row - chưa test
                $mToExcel1 = new ToExcel1();
                $mToExcel1->exportReportSoldOut($model);
            }
            $this->redirect(array('soldOut'));
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }
    /** @Author: Pham Thanh Nghia Oct 30, 2018
     *  @Todo: báo cáo doanh thu bồ mối
     **/
    public function actionReportRevenue(){
        $this->pageTitle = 'BC doanh thu Gas bò mối';
        try{
            $sta2 = new Sta2();
            $model = new GasAppOrder();
            $model->date_from   = date("01-m-Y");
            $model->date_to     = date("d-m-Y");
            $model->status = GasAppOrder::STATUS_COMPPLETE;
            $aData = array();
            if(isset($_GET['GasAppOrder'])){
                $model->attributes = $_GET['GasAppOrder'];
                $aData = $sta2->getReportRevenue($model);
                Yii::app()->session['dataRevenue'] = $aData;
            }
            $this->exportExcelRevenue($model);
            $this->render('reportRevenue/index',array( 
                'aData'=>$aData,
                'model'=>$model,
                'actions' => $this->listActionsCanAccess,
            ));
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }
    /** @Author: Pham Thanh Nghia Oct,31 2018
     *  @Todo: export excel for gasAppOrder/reportRevenue
     **/
    public function exportExcelRevenue($model){
        try{
            if(!isset($_GET['toExcel'])){
                return ;
            }
            if(isset($_GET['toExcel']) && !empty(Yii::app()->session['dataRevenue'])){
//                ini_set('memory_limit','2000M');// chưa cần chạy đoạn này, đưa memory_limit lên 500 có thể export dc khoảng 5000 row - chưa test
                $mToExcel1 = new ToExcel1();
                $mToExcel1->exportReportRevenue($model);
            }
            $this->redirect(array('reportRevenue'));
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }
}
