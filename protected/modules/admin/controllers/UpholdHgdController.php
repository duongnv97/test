<?php

class UpholdHgdController extends AdminController 
{
    public $pluralTitle = 'Bảo Trì Hộ Gia Đình';
    public $singleTitle = 'Bảo Trì Hộ Gia Đình';
    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id)
    {
        $this->pageTitle = 'Xem ';
        try{
        $this->render('view',array(
                'model'=>$this->loadModel($id), 'actions' => $this->listActionsCanAccess,
        ));
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }

    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate()
    {
        $this->pageTitle = 'Tạo Mới ';
        try{
        $model=new UpholdHgd('create');
        $model->mapCustomerSelect();
        if(isset($_GET['phone_number'])){
            $model->phone = $_GET['phone_number'];
        }
            
        if(isset($_POST['UpholdHgd']))
        {
            $model->attributes=$_POST['UpholdHgd'];
            $model->uid_login           = MyFormat::getCurrentUid();
            $model->created_date_only   = date('Y-m-d');
            $model->validate();
            if(!$model->hasErrors()){
                $model->save();
                $model->notifyEmployee();
                $model->alertAgentKhoanCreateNew();
                Yii::app()->user->setFlash( MyFormat::SUCCESS_UPDATE, "Thêm mới thành công." );
                $this->redirect(array('create'));
            }				
        }

        $this->render('create',array(
                'model'=>$model, 'actions' => $this->listActionsCanAccess,
        ));
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id)
    {
        $this->pageTitle = 'Cập Nhật ';
        try{
        $model=$this->loadModel($id);
        $model->scenario = 'update';
        $model->phone = '0'.$model->phone;
        if(isset($_POST['UpholdHgd']))
        {
            $model->attributes=$_POST['UpholdHgd'];
                    // $this->redirect(array('view','id'=>$model->id));
            $model->validate();
            if(!$model->hasErrors()){
                $model->save();
                $model->notifyEmployee();
                Yii::app()->user->setFlash( MyFormat::SUCCESS_UPDATE, "Cập nhật thành công." );
                $this->redirect(array('update','id'=>$model->id));
            }							
        }

        $this->render('update',array(
                'model'=>$model, 'actions' => $this->listActionsCanAccess,
        ));
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id)
    {
        try{
        if(Yii::app()->request->isPostRequest){
            // we only allow deletion via POST request
            if($model = $this->loadModel($id))
            {
                if($model->delete())
                    Yii::log("Uid: " .Yii::app()->user->id. " Delete record ".  print_r($model->attributes, true), 'info');
            }

            // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
            if(!isset($_GET['ajax']))
                    $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('index'));
        }
        else
        {
            Yii::log("Uid: " .Yii::app()->user->id. " Invalid request. Please do not repeat this request again.");
            throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
        }	
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }

    /**
     * Manages all models.
     */
    public function actionIndex()
    {
        $this->pageTitle = 'Bảo trì HGĐ';
        try{
        $model=new UpholdHgd('search');
        $model->unsetAttributes();  // clear any default values
        if(isset($_GET['UpholdHgd']))
                $model->attributes=$_GET['UpholdHgd'];
        $this->exportExcelIndex($model);
        $this->render('index',array(
                'model'=>$model, 'actions' => $this->listActionsCanAccess,
        ));
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
    * If the data model is not found, an HTTP exception will be raised.
     * @param integer the ID of the model to be loaded
     */
    public function loadModel($id)
    {
        try{
        $model=UpholdHgd::model()->findByPk($id);
        if($model===null)
        {
            Yii::log("The requested page does not exist.");
            throw new CHttpException(404,'The requested page does not exist.');
        }			
        return $model;
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }
    /** @Author: Pham Thanh Nghia Nov 30, 2018
     *  @Todo: Báo cáo đơn bảo trì bò mối và hộ GĐ
     **/
    public function actionReportSum() {
        $this->pageTitle = 'Báo cáo đơn bảo trì bò mối và hộ GĐ';
        try
        {
            $data=array();
            $sta3 = new Sta3();
            $model = new UpholdHgd();
            $model->date_from   = date('01-m-Y');
            $model->date_to     = date('d-m-Y');
//            $model->date_from = '1-1-2017'; 
//            $model->date_to = '30-01-2017';
            if(isset($_GET['UpholdHgd'])){
                $model->attributes = $_GET['UpholdHgd'];
                $data = $sta3->gasUpholdReportSum($model);
            }
            $this->exportExcelReportSum($model);
            $this->render('ReportSum/index',array(
                'model'=>$model, 'actions' => $this->listActionsCanAccess,
                "data"=>$data,
            ));
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }
    /** @Author: Pham Thanh Nghia Dec, 1 2018
     *  @Todo: export excel for report
     **/
    public function exportExcelReportSum($model){
        try{
            if(!$model->canExportExcel() || !isset($_GET['ExportExcel'])){
                return ;
            }
            if(isset($_GET['ExportExcel']) && isset($_SESSION['dataUpholdReportSum'])){
                ini_set('memory_limit','2000M');// chưa cần chạy đoạn này, đưa memory_limit lên 500 có thể export dc khoảng 5000 row - chưa test
                $mToExcel1 = new ToExcel1();
                $mToExcel1->exportUpholdHgdReportSum($model);
            }
            
            $this->redirect(array('ReportSum'));
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }
    public function exportExcelIndex($model){
        try{
            if(!$model->canExportExcelIndex() || !isset($_GET['ExportExcel'])){
                return ;
            }
            if(isset($_GET['ExportExcel']) && isset($_SESSION['dataUpholdHgdIndex'])){
                ini_set('memory_limit','2000M');// chưa cần chạy đoạn này, đưa memory_limit lên 500 có thể export dc khoảng 5000 row - chưa test
                $mToExcel1 = new ToExcel1();
                $mToExcel1->exportUpholdHgdIndex($model);
            }
            
            $this->redirect(array('ReportSum'));
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }
    
   /** @Author: KHANH TOAN 2018
     *  @Todo: lí do hủy đơn hàng
     *  @Param:
     **/
    public function actionCommentCanCel($id){
        $this->layout = "ajax";
        try{
            $model = UpholdHgd::model()->findbyPk($id);
            if(empty($model)){
                throw new Exception('Invalid request');
            }
            if(isset($_POST['UpholdHgd'])){
                $model->attributes = $_POST['UpholdHgd'];
                $cUid = MyFormat::getCurrentUid();
                $cDate = date("Y-m-d H:i:s");
                $model->status = UpholdHgd::STATUS_CANCEL;
                $model->convert_id =  $cUid;   
                $model->created_date_comment_cancel = $cDate;
                $model->update();
                //send thông báo cho người tạo
                $model->notifyEmployee($model->uid_login);
                Yii::app()->user->setFlash( MyFormat::SUCCESS_UPDATE, "Thêm mới lí do thành công." );
                $this->redirect(array('commentCancel', 'id'=>$id));
            }

            $this->render('statusCancel/_formStatusCancel',array(
                'model'=>$model, 
                'actions' => $this->listActionsCanAccess,
        ));
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }
    
    /** @Author: KHANH TOAN Dec 14 2018
     *  @Todo:
     *  @Param:
     **/
    public function actionReportMaintenance() {
        $this->pageTitle = 'Báo cáo đơn hàng bảo trì';
        $this->reportMaintenanceExcel();
        try
        {
            $data=array();
            $sta3 = new Sta3();
            $model = new UpholdHgd();
            $model->date_from   = date('01-m-Y');
            $model->date_to     = date('d-m-Y');
            if(isset($_GET['UpholdHgd'])){
                $model->attributes = $_GET['UpholdHgd'];
            }
            $data = $sta3->gasUpholdReportMaintenance($model);
            $_SESSION['modelReportMaintenance'] = $model;
            $this->render('ReportMaintenance/index',array(
                'model'=>$model, 
                'actions' => $this->listActionsCanAccess,
                "data"=>$data,
            ));
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }
    
    /** @Author: KHANH TOAN Dec 18 2018
     *  @Todo: xuat excel bao cao bo moi
     *  @Param:
     **/
    public function reportMaintenanceExcel() {
        try{
        if(isset($_GET['to_excel']) && isset($_SESSION['dataReportMaintenance'])){
            ini_set('memory_limit','1500M');// chưa cần chạy đoạn này, đưa memory_limit lên 500 có thể export dc khoảng 5000 row - chưa test
            $mToExcel1 = new ToExcelList();
            $mToExcel1->exportReportMaintenance();
        }
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }


}
