<?php

class GasSupportCustomerRequestController extends AdminController 
{
    public $pluralTitle = 'Yêu cầu vật tư';
    public $singleTitle = 'Yêu cầu vật tư';
    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed 
     */
    public function actionGenerate($id)
    {
        $this->pageTitle = 'Xem ';
        try{
            $model = $this->loadModel($id);
            if($model && $model->customer_id !=null){
                $this->redirect(Yii::app()->createAbsoluteUrl('admin/gasSupportCustomer/create', array('id_request'=> $model->id)));
            }
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id)
    {
        $this->pageTitle = 'Cập Nhật ';
        try{
        $model=$this->loadModel($id);
        $model->modelOld = clone $model;
        $model->scenario = 'update';
        $model->getJsonToArray();
        if(isset($_POST['GasSupportCustomerRequest']))
        {
            $model->attributes = $_POST['GasSupportCustomerRequest'];
            $model->formatJsonToSave();
            $model->validate();
            if(!$model->hasErrors()){
                $model->update();
                $model->notifyRequestUpdate();
                $model->sendEmailAfterApproved();
                Yii::app()->user->setFlash( MyFormat::SUCCESS_UPDATE, "Cập nhật thành công." );
                $this->redirect(array('update','id'=>$model->id));
            }else{
                $model->formatJson();
            }							
        }
        $this->render('update',array(
                'model'=>$model, 'actions' => $this->listActionsCanAccess,
        ));
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }
    /**
     * Manages all models.
     */
    public function actionIndex()
    {
        error_reporting(1); // var_dump(PHP_INT_SIZE);
        $this->cancelRequest();
        $mGasSupportCustomer=new GasSupportCustomer('search');
        $mGasSupportCustomer->InitMaterial();
        $this->pageTitle = 'Yêu cầu vật tư';
        try{ 
        $model=new GasSupportCustomerRequest('search');
        $model->unsetAttributes();  // clear any default values
        if(isset($_GET['statusSearch'])){
            $model->status = $_GET['statusSearch'];
        }else{
            $model->status = GasSupportCustomerRequest::STATUS_NEW;
        }
        if(isset($_GET['GasSupportCustomerRequest'])){
            $model->attributes=$_GET['GasSupportCustomerRequest'];
        }
        $this->render('index',array(
                'model'=>$model, 'actions' => $this->listActionsCanAccess,
        ));
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }
    /**
     * Returns the data model based on the primary key given in the GET variable.
    * If the data model is not found, an HTTP exception will be raised.
     * @param integer the ID of the model to be loaded
     */
    public function loadModel($id)
    {
        try{
        $model=GasSupportCustomerRequest::model()->findByPk($id);
        if($model===null)
        {
            Yii::log("The requested page does not exist.");
            throw new CHttpException(404,'The requested page does not exist.');
        }			
        return $model;
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }
    
    /** @Author: Pham Thanh Nghia Jul 20, 2018
     *  @Todo:
     *  @Param: actionCreate 
     **/
    public function actionCreate(){
        $this->pageTitle = 'Tạo Mới Yêu Cầu ';
        try
        {
            $model=new GasSupportCustomerRequest('create');
            $this->ajaxModule();
            if(isset($_POST['GasSupportCustomerRequest']))
            {
                $model->attributes=$_POST['GasSupportCustomerRequest'];
                $model->validate();
                $model->formatJsonToSave();
                if(!$model->hasErrors()){
                    $model->save();
                    $model->sendEmailAfterCreate();
                    $model= $this->loadModel($model->id);
                    
                    Yii::app()->user->setFlash('successUpdate', "Thêm mới thành công.");
                    $this->redirect(array('create','id'=>$model->id));
                }else{
                    $model->formatJson();
                } 
            }
            $this->render('create',array(
                    'model'=>$model, 'actions' => $this->listActionsCanAccess,
            ));
        }
        catch (Exception $exc)
        {
            GasCheck::CatchAllExeptiong($exc);
        }
    }
    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id)
    {
        try{
        if(Yii::app()->request->isPostRequest)
        {
            if($model = $this->loadModel($id))
            {
                if($model->delete())
                    Yii::log("Uid: " .Yii::app()->user->id. " Delete record ".  print_r($model->attributes, true), 'info');
            }
            // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
            if(!isset($_GET['ajax']))
                    $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('index'));
        }else{
            Yii::log("Uid: " .Yii::app()->user->id. " Invalid request. Please do not repeat this request again.");
            throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
        }
        }catch (Exception $exc){
            Yii::log("Uid: " .Yii::app()->user->id. " Exception ".  $exc->getMessage(), 'error');
            $code = 404;
            if(isset($exc->statusCode))
                $code=$exc->statusCode;
            if($exc->getCode())
                $code=$exc->getCode();
            throw new CHttpException($code, $exc->getMessage());
        }
    }
    
    /**
     * get view of ajax module  change on view create, update
     */
    public function ajaxModule(){
        if(!isset($_GET['AJAX_MODULE'])){
            return;
        }
        $id = isset($_GET['AJAX_ID']) ? $_GET['AJAX_ID'] : 0;
        $mModules = Modules::model()->findByPk($id);
        $aData = [];
        if(!empty($mModules)){
            $aData = $mModules->getArrayDetailForAjax();
        }
        echo json_encode($aData);
        die;
        
    }
    
    /**
     * NamNH 20180904
     * @param int $id
     */
    public function actionView($id)
    {
        $this->pageTitle = 'Chi tiết yêu cầu vật tư';
        try{
        $this->render('view',array(
                'model'=>$this->loadModel($id), 'actions' => $this->listActionsCanAccess,
        ));
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }
    
    /** @Author: DuongNV Feb 20,2019
     *  @Todo: Hủy phiếu đã duyệt
     **/
    public function cancelRequest() {
        if(isset($_GET['cancleRequest'])){
            $id     = isset($_GET['id']) ? $_GET['id'] : "";
            $model  = GasSupportCustomerRequest::model()->findByPk($id);
            if(isset($_POST['GasSupportCustomerRequest']['reason_reject']) && $model->canCancelRequest()){
                if(empty($_POST['GasSupportCustomerRequest']['reason_reject'])){
                    echo MyFormat::renderNotifyError("Lý do không được để trống!");
                } else {
                    $model->cancelRequest();
                    $model->update();
                    echo MyFormat::renderNotifySuccess("Hủy phiếu {$model->code_no} thành công!");
                }
                die();
            }
            if($model && $model->canCancelRequest()){
                $this->layout = 'ajax';
                $this->render('_form_cancel_request',array(
                    'model'=>$model, 
    //                'actions' => $this->listActionsCanAccess,
                ));
            }
            die;
        }
    }
    
}
