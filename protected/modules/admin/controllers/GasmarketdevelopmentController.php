<?php

class GasmarketdevelopmentController extends AdminController 
{
    /** Viết cho NV phát triển thị trường
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id)
    {
        $this->pageTitle = 'Xem Phát Triển Thị Trường';
            try{
                            $this->layout='ajax';
            $this->render('view',array(
                    'model'=>$this->loadModel($id), 'actions' => $this->listActionsCanAccess,
            ));
            }
            catch (Exception $e)
            {
                Yii::log("Uid: " .Yii::app()->user->id. "Exception ".  $e->getMessage(), 'error');
                $code = 404;
                if(isset($e->statusCode))
                    $code=$e->statusCode;
                if($e->getCode())
                    $code=$e->getCode();
                throw new CHttpException($code, $e->getMessage());
            }

    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate()
    {		
        $this->pageTitle = 'Tạo Mới Phát Triển Thị Trường';
            try
            {
            $model=new GasMarketDevelopment('create_marketdevelopment');
            $model->modelUser = new Users('create_marketdevelopment');

            if(isset($_POST['GasMarketDevelopment']))
            {
                    $model->attributes=$_POST['GasMarketDevelopment'];
                    $model->modelUser->attributes = $_POST['Users'];
                    // quan trọng khi làm, nhớ chỗ saveCustomerAgent bên dưới

                    if($model->is_new_customer==CUSTOMER_NEW)
                        $model->modelUser->validate();
                    else
                        $model->scenario = 'create_marketdevelopment_customer_old';
                    $model->validate();
                    $model->type = TYPE_MARKET_DEVELOPMENT;

                    $aAttSave = array('first_name','last_name','province_id','district_id',
                            'ward_id','house_numbers','street_id','phone',                        
                            'address','address_vi','name_agent','area_code_id',
                            );
                    $model->modelUser = MyFunctionCustom::clearHtmlModel($model->modelUser, $aAttSave);                        

                    if(!$model->hasErrors() && !$model->modelUser->hasErrors()){
                        if($model->is_new_customer==CUSTOMER_NEW)
                            $this->saveUser($model);
                        $model->save();
//				MyFunctionCustom::saveCustomerAgent($model->modelUser->id);
                        Yii::app()->user->setFlash('successUpdate', "Thêm mới thành công.");
                       $this->redirect(array('create'));
                    }
            }

            $this->render('create',array(
                    'model'=>$model, 'actions' => $this->listActionsCanAccess,
            ));
            }
            catch (Exception $e)
            {
                Yii::log("Uid: " .Yii::app()->user->id. "Exception ".  $e->getMessage(), 'error');
                $code = 404;
                if(isset($e->statusCode))
                    $code=$e->statusCode;
                if($e->getCode())
                    $code=$e->getCode();
                throw new CHttpException($code, $e->getMessage());
            }
    }

    public function saveUser($model,$needMore=array()){
            $aAttSave = array('first_name','last_name','province_id','district_id',
                    'ward_id','house_numbers','street_id','phone',                        
                    'address','address_vi','name_agent','area_code_id',
                    );
            $model->modelUser = MyFunctionCustom::clearHtmlModel($model->modelUser, $aAttSave);	
            $model->modelUser->role_id = ROLE_CUSTOMER;		
            $model->modelUser->application_id = BE;
            $model->modelUser->type = CUSTOMER_TYPE_MARKET_DEVELOPMENT;
    //		MyFunctionCustom::updateListCustomerOfAgentLogin();
            if(is_null($model->modelUser->id)){
                $model->modelUser->code_account = MyFunctionCustom::getNextIdForUser('Users', Users::getCodeAccount(Yii::app()->user->parent_id).'_', MAX_LENGTH_CODE, 'code_account', ROLE_CUSTOMER);
                $model->modelUser->code_bussiness = MyFunctionCustom::genCodeBusinessCustomerAgent($model->modelUser->first_name);
                $aAttSave[] = 'code_bussiness';
                $aAttSave[] = 'role_id';
                $aAttSave[] = 'code_account';
                $aAttSave[] = 'application_id';			
                $aAttSave[] = 'type';			
                $model->modelUser->save(true, $aAttSave);
                if(is_null($model->modelUser->id))
                    throw new CHttpException(404, 'Lỗi Tạo Mã Khách Hàng. Vui Lòng Thử Lại');
            }else{
                if(isset($needMore['old_first_name'])){
                    $oldName = MyFunctionCustom::getNameRemoveVietnamese($needMore['old_first_name']);
                    $newName = MyFunctionCustom::getNameRemoveVietnamese($model->modelUser->first_name);
                    if(!empty($oldName) && strtolower($oldName) !=  strtolower($newName) && Yii::app()->user->role_id==ROLE_SUB_USER_AGENT ){
                            $aAttSave[] = 'code_bussiness';					
                            $model->modelUser->code_bussiness = MyFunctionCustom::genCodeBusinessCustomerAgent($model->modelUser->first_name);
                    }
                }
                $model->modelUser->update($aAttSave);
            }
            $model->customer_id = $model->modelUser->id;

    }

    public function actionUpdate($id)
    {
        $this->pageTitle = 'Cập Nhật Phát Triển Thị Trường';
        try
        {                    
            $model=$this->loadModel($id);
            if(!MyFunctionCustom::agentCanUpdateMarketDevelopment($model))
                $this->redirect(array('index'));
            $model->scenario = 'update_marketdevelopment';
            $model->modelUser = Users::model()->findByPk($model->customer_id);
            $model->modelUser->scenario = 'update_marketdevelopment';
            $old_first_name = $model->modelUser->first_name;
            $model->autocomplete_name_street = $model->modelUser->street?$model->modelUser->street->name:'';
            $model->maintain_date = MyFormat::dateConverYmdToDmy($model->maintain_date);
            $model->last_update_by = Yii::app()->user->id;
            $model->last_update_time = date('Y-m-d H:i:s');

            if(isset($_POST['GasMarketDevelopment']))
            {
                    $model->attributes=$_POST['GasMarketDevelopment'];
                    $model->modelUser->attributes = $_POST['Users'];
                    $model->validate();
                    $model->modelUser->validate();
                    $model->type = TYPE_MARKET_DEVELOPMENT;
                    if(!$model->hasErrors() && !$model->modelUser->hasErrors()){
                        $this->saveUser($model,array('old_first_name'=>$old_first_name));
                        $model->update();		
                        //MyFunctionCustom::saveCustomerAgent($model->modelUser->id);				
                        Yii::app()->user->setFlash('successUpdate', "Cập Nhật thành công.");
                       $this->redirect(array('update','id'=>$model->id));
                    }
            }

            $this->render('update',array(
                    'model'=>$model, 'actions' => $this->listActionsCanAccess,
            ));
            }
            catch (Exception $e)
            {
                Yii::log("Uid: " .Yii::app()->user->id. "Exception ".  $e->getMessage(), 'error');
                $code = 404;
                if(isset($e->statusCode))
                    $code=$e->statusCode;
                if($e->getCode())
                    $code=$e->getCode();
                throw new CHttpException($code, $e->getMessage());
            }
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id)
    {
            try
            {
            if(Yii::app()->request->isPostRequest)
            {
                    // we only allow deletion via POST request
                    if($model = $this->loadModel($id))
                    {
                        if($model->delete())
                            Yii::log("Uid: " .Yii::app()->user->id. " Delete record ".  print_r($model->attributes, true), 'info');
                    }

                    // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
                    if(!isset($_GET['ajax']))
                            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('index'));
            }
            else
            {
                Yii::log("Uid: " .Yii::app()->user->id. " Invalid request. Please do not repeat this request again.");
                throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
            }	
            }
            catch (Exception $e)
            {
                Yii::log("Uid: " .Yii::app()->user->id. "Exception ".  $e->getMessage(), 'error');
                $code = 404;
                if(isset($e->statusCode))
                    $code=$e->statusCode;
                if($e->getCode())
                    $code=$e->getCode();
                throw new CHttpException($code, $e->getMessage());
            }
    }

    /**
     * Manages all models.
     */
    public function actionIndex()
    {
        $this->pageTitle = 'Danh Sách Phát Triển Thị Trường';
        MyFunctionCustom::checkChangePassword();
            try
            {
            $model=new GasMarketDevelopment('search');
            $model->unsetAttributes();  // clear any default values
            if(isset($_GET['GasMarketDevelopment']))
                    $model->attributes=$_GET['GasMarketDevelopment'];		
            $this->render('index',array(
                    'model'=>$model, 'actions' => $this->listActionsCanAccess,
            ));
            }
            catch (Exception $e)
            {
                Yii::log("Uid: " .Yii::app()->user->id. "Exception ".  $e->getMessage(), 'error');
                $code = 404;
                if(isset($e->statusCode))
                    $code=$e->statusCode;
                if($e->getCode())
                    $code=$e->getCode();
                throw new CHttpException($code, $e->getMessage());
            }
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer the ID of the model to be loaded
     */
    public function loadModel($id)
    {
            try
            {
            $model=GasMarketDevelopment::model()->findByPk($id);
            if($model===null)
            {
                Yii::log("The requested page does not exist.");
                throw new CHttpException(404,'The requested page does not exist.');
            }			
            return $model;
            }
            catch (Exception $e)
            {
                Yii::log("Uid: " .Yii::app()->user->id. "Exception ".  $e->getMessage(), 'error');
                $code = 404;
                if(isset($e->statusCode))
                    $code=$e->statusCode;
                if($e->getCode())
                    $code=$e->getCode();
                throw new CHttpException($code, $e->getMessage());
            }
    }

    /**
     * Performs the AJAX validation.
     * @param CModel the model to be validated
     */
    protected function performAjaxValidation($model)
    {
            try
            {
            if(isset($_POST['ajax']) && $_POST['ajax']==='gas-maintain-form')
            {
                    echo CActiveForm::validate($model);
                    Yii::app()->end();
            }
            }
            catch (Exception $e)
            {
                Yii::log("Uid: " .Yii::app()->user->id. "Exception ".  $e->getMessage(), 'error');
                $code = 404;
                if(isset($e->statusCode))
                    $code=$e->statusCode;
                if($e->getCode())
                    $code=$e->getCode();
                throw new CHttpException($code, $e->getMessage());
            }
    }

    public function actionUpdate_status_market_development()
    {
        $this->layout = 'ajax';
        if(!isset($_GET['maintain_id']))
            die('Không Tìm Thấy Trang Yêu Cầu');
        $model = GasMaintain::model()->findByPk($_GET['maintain_id']);
        if(is_null($model))
            die('Không Tìm Thấy Trang Yêu Cầu');
        $msg= false;
        if(isset($_POST['GasMaintain'])){
            $model->attributes=$_POST['GasMaintain'];
            $update_num = (int)$model->update_num;
            $update_num++;
            $model->update_num = $update_num;
            $model->update(array('status','note_update_status','update_num'));
            $msg= 'Cập Nhật Trạng Thái Thành Công';
        }

        $this->render('Update_status_market_development',array(
                'model'=>$model,
            'msg'=>$msg,
        ));            

    }           
        
}
