<?php

class HrPlanController extends AdminController
{
    public $pluralTitle = 'Kế hoạch tuyển dụng nhân sự';
    public $singleTitle = 'Kế hoạch tuyển dụng nhân sự';

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id)
    {
        $this->pageTitle = 'Xem ';
        $this->layout = 'ajax';
        try {
            $this->render('view', array(
                'model' => $this->loadModel($id), 'actions' => $this->listActionsCanAccess,
            ));
        } catch (Exception $exc) {
            GasCheck::CatchAllExeptiong($exc);
        }
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate()
    {
        $this->pageTitle = 'Tạo Mới ';
        try {
            $model = new HrPlan('create');

            if (isset($_POST['HrPlan'])) {
                $model->attributes = $_POST['HrPlan'];
                $model->validate();
                if (!$model->hasErrors()) {
                    $model->save();
                    GasScheduleEmail::notifyHrPlanApprover($model);
                    Yii::app()->user->setFlash(MyFormat::SUCCESS_UPDATE, "Thêm mới thành công.");
                    $this->redirect(array('create'));
                }
            }

            $this->render('create', array(
                'model' => $model, 'actions' => $this->listActionsCanAccess,
            ));
        } catch (Exception $exc) {
            GasCheck::CatchAllExeptiong($exc);
        }
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id)
    {
        $this->pageTitle = 'Cập Nhật ';
        try {
            $model = $this->loadModel($id);
            $model->scenario = 'update';
            $model->handleUpdate(); 
            if (isset($_POST['HrPlan'])) {
                $model->attributes = $_POST['HrPlan'];
                $model->status = HrPlan::STA_NEW;
                // $this->redirect(array('view','id'=>$model->id));
                $model->validate();
                if (!$model->hasErrors()) {
                    $model->save();
                    Yii::app()->user->setFlash(MyFormat::SUCCESS_UPDATE, "Cập nhật thành công.");
                    $this->redirect(array('update', 'id' => $model->id));
                }
            }

            $this->render('update', array(
                'model' => $model, 'actions' => $this->listActionsCanAccess,
            ));
        } catch (Exception $exc) {
            GasCheck::CatchAllExeptiong($exc);
        }
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id)
    {
        try {
            if (Yii::app()->request->isPostRequest) {
                // we only allow deletion via POST request
                if ($model = $this->loadModel($id)) {
                    if ($model->delete())
                        Yii::log("Uid: " . Yii::app()->user->id . " Delete record " . print_r($model->attributes, true), 'info');
                }

                // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
                if (!isset($_GET['ajax']))
                    $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('index'));
            } else {
                Yii::log("Uid: " . Yii::app()->user->id . " Invalid request. Please do not repeat this request again.");
                throw new CHttpException(400, 'Invalid request. Please do not repeat this request again.');
            }
        } catch (Exception $exc) {
            GasCheck::CatchAllExeptiong($exc);
        }
    }

    /**
     * Manages all models.
     */
    public function actionIndex()
    {
        $this->pageTitle = 'Danh Sách ';
        try {
            $model = new HrPlan('search');
            $model->unsetAttributes();  // clear any default values
            if (isset($_GET['HrPlan']))
                $model->attributes = $_GET['HrPlan'];

            $this->render('index', array(
                'model' => $model, 'actions' => $this->listActionsCanAccess,
            ));
        } catch (Exception $exc) {
            GasCheck::CatchAllExeptiong($exc);
        }
    }

    public function actionUpdateStatus($id)
    {
        try {
            $this->layout = 'ajax';
            if (isset($_POST['HrPlan'])) {
                $model = $this->loadModel($id);
                $model->attributes = $_POST['HrPlan'];
                $model->scenario = 'updateStatus';
                $model->handleBeforeUpdateStatus();
                $model->validate();
                $model->save();
                if (!$model->hasErrors()) {
                    Yii::app()->user->setFlash('successUpdate', "Cập nhật trạng thái thành công.");
                    $this->redirect(Yii::app()->createAbsoluteUrl('admin/hrPlan/view', array('id' => $model->id)));
                }
                $this->render('view', array(
                    'model' => $model, 'actions' => $this->listActionsCanAccess,
                ));
            }
            die;
        } catch (Exception $exc) {
            GasCheck::CatchAllExeptiong($exc);
        }
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer the ID of the model to be loaded
     */
    public function loadModel($id)
    {
        try {
            $model = HrPlan::model()->findByPk($id);
            if ($model === null) {
                Yii::log("The requested page does not exist.");
                throw new CHttpException(404, 'The requested page does not exist.');
            }
            return $model;
        } catch (Exception $exc) {
            GasCheck::CatchAllExeptiong($exc);
        }
    }

}
