<?php

class CustomerDraftDetailController extends AdminController 
{
    public $pluralTitle = 'KH Excel Daily Call Out';
    public $singleTitle = 'KH Excel Daily Call Out';
    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id)
    {
        $this->pageTitle = 'Xem ';
        $this->layout       = 'ajax';
        try{
        $this->render('view',array(
                'model'=>$this->loadModel($id), 'actions' => $this->listActionsCanAccess,
        ));
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }

    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate()
    {
        $this->pageTitle = 'Tạo Mới ';
        try{
        $model=new CustomerDraftDetail('create');
        // set ajax
        if(!empty($_GET['ajax'])){
            $this->layout       = 'ajax';
        }
        if(!empty($_GET['CustomerDraft'])){
            $model->customer_draft_id       = $_GET['CustomerDraft'];
        }
        if(!empty($_GET['OnlyView'])){
            $model->only_view       = $_GET['OnlyView'];
        }else{
            $mCusDraf = CustomerDraft::model()->findByPk($model->customer_draft_id);
            if(empty($mCusDraf) || $mCusDraf->customer_id > 0){
                $this->redirect(Yii::app()->createAbsoluteUrl('admin/customerDraft'));
            }
        }
        $model->appointment_date = date('d/m/Y');
        
        $this->handleUpdatePhone($model);
        if(isset($_POST['CustomerDraftDetail']))
        {
            $model->attributes=$_POST['CustomerDraftDetail'];
            $model->validate();
            if(!$model->hasErrors()){
                $model->handleBeforeSave();
                $model->setCallId();
                $model->save();
                Yii::app()->user->setFlash( MyFormat::SUCCESS_UPDATE, "Thêm mới thành công." );
                $this->redirect(Yii::app()->createAbsoluteUrl('admin/customerDraftDetail/create',
                        array(
                            'ajax'=>1,
                            'CustomerDraft'=>!empty($_GET['CustomerDraft']) ? $_GET['CustomerDraft'] : 0,
                            )));
            }				
        }

        $this->render('create',array(
                'model'=>$model, 'actions' => $this->listActionsCanAccess,
        ));
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }
    
    /** @Author: ANH DUNG Jul 03, 2018
     *  @Todo: xử lý update số đt và tên KH draft
     **/
    public function handleUpdatePhone($model) {
        if(isset($_POST['CustomerDraft'])){
            $mCustomerDraft = $model->rCustomerDraft;
            $mCustomerDraft->attributes = $_POST['CustomerDraft'];
            $mCustomerDraft->scenario = 'UpdatePhone';
            $mCustomerDraft->validate();
            if(!$mCustomerDraft->hasErrors()){
                $mCustomerDraft->formatDataSave();
                $mCustomerDraft->update();
                Yii::app()->user->setFlash( MyFormat::SUCCESS_UPDATE, 'Cập nhật thông tin KH thành công');
                $url = isset($_POST['cUrl']) ? $_POST['cUrl'] : Yii::app()->createAbsoluteUrl('/');
                $this->redirect($url);
            }
        }
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id)
    {
        $this->pageTitle = 'Cập Nhật ';
        try{
        $this->layout       = 'ajax';
        $model=$this->loadModel($id);
        $model->formatDataUpdate();
        $model->scenario = 'update';
        if(isset($_POST['CustomerDraftDetail']))
        {
            $model->attributes=$_POST['CustomerDraftDetail'];
            $model->validate();
            if(!$model->hasErrors()){
                $model->handleBeforeSave();
                $model->save();
                Yii::app()->user->setFlash( MyFormat::SUCCESS_UPDATE, "Cập nhật thành công." );
                $this->redirect(array('update','id'=>$model->id));
            }							
        }

        $this->render('update',array(
                'model'=>$model, 'actions' => $this->listActionsCanAccess,
        ));
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id)
    {
        try{
        if(Yii::app()->request->isPostRequest){
            // we only allow deletion via POST request
            if($model = $this->loadModel($id))
            {
                if($model->delete())
                    Yii::log("Uid: " .Yii::app()->user->id. " Delete record ".  print_r($model->attributes, true), 'info');
            }

            // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
            if(!isset($_GET['ajax']))
                    $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('index'));
        }
        else
        {
            Yii::log("Uid: " .Yii::app()->user->id. " Invalid request. Please do not repeat this request again.");
            throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
        }	
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }

    /**
     * Manages all models.
     */
    public function actionIndex()
    {
        $this->pageTitle = 'Danh Sách ';
        try{
        $model=new CustomerDraftDetail('search');
        $model->unsetAttributes();  // clear any default values
        if(isset($_GET['CustomerDraftDetail']))
                $model->attributes=$_GET['CustomerDraftDetail'];

        $this->render('index',array(
                'model'=>$model, 'actions' => $this->listActionsCanAccess,
        ));
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
    * If the data model is not found, an HTTP exception will be raised.
     * @param integer the ID of the model to be loaded
     */
    public function loadModel($id)
    {
        try{
        $model=CustomerDraftDetail::model()->findByPk($id);
        if($model===null)
        {
            Yii::log("The requested page does not exist.");
            throw new CHttpException(404,'The requested page does not exist.');
        }			
        return $model;
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }

}
