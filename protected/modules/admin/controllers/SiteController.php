<?php

class SiteController extends AdminController
{
    public function accessRules()
    {
        return array(
            array('allow',  // allow all users to perform  actions
                'actions'=>array(
                     'Login', 'Logout', 'Error','captcha',
                    ),
                'users'=>array('*'),
            ),  
            array('allow',   //allow authenticated user to perform actions
                'actions'=>array(
                    'Update_agent_employee_maintain','Update_agent_employee_accounting', 'Update_agent_ccs',
                    'UpdateAgentPhuXe','UpdateAgentDriver','UpdateAgentCar','AutocompleteCustomerMaintain',
                    'Change_my_password','AutocompleteCustomerPTTT','autocompleteSellCustomerMaintainAndPTTT',
                    'index','News','Maintenance','Profile'),
                'users'=>array('@'),
            ),
            array('deny',  // deny all users
                    'users'=>array('*'),
            ),            
                      
        );
    }
    
    
//    public function accessRules()
//    {
//        return array(
//            array('allow',  // allow all users to perform  actions
//                'actions'=>array('Update_agent_employee_maintain','Update_agent_employee_accounting', 'AutocompleteCustomerMaintain',
//                    'ForgotPassword', 'ResetPassword', 'Login', 'Logout', 'Error','captcha',
//                    'Change_my_password',
//                    'AutocompleteCustomerPTTT','autocompleteSellCustomerMaintainAndPTTT'
//                    ),
//                'users'=>array('*'),
//            ),  
//            array('allow',   //allow authenticated user to perform actions
//                'actions'=>array('index','News','Maintenance'),
//                'users'=>array('@'),
//            ),
//            array('deny',  // deny all users
//                    'users'=>array('*'),
//            ),            
//                      
//        );
//    }
    
    public function actions()
    {
        return array(
            // captcha action renders the CAPTCHA image displayed on the contact page
            'captcha'=>array(
                    'class'=>'CCaptchaAction',
                    'backColor'=>0xFFFFFF,
            ),
            // page action renders "static" pages stored under 'protected/views/site/pages'
            // They can be accessed via: index.php?r=site/page&view=FileName
            'page'=>array(
                    'class'=>'CViewAction',
            ),
        );
    }
    
    public function actionUpdate_agent_employee_maintain(){
        $this->layout='ajax';$msg='';
        $model=new GasOneMany();
        $model->one_id = $_GET['agent_id'];
        $mAgent = Users::model()->findByPk($_GET['agent_id']);
        
        if(isset($_POST['GasOneMany']) && $mAgent){
            if(isset($_POST['GasOneMany']['many_id'])){// Jun 01, 2016
                $this->setListAgentOfUser($_POST['GasOneMany']['many_id'], $_GET['agent_id']);
                // Sep 09, 2016 xử lý không xóa nv phục vụ KH, vì có NVPV KH lưu động nữa.
//                if(in_array($mAgent->province_id, GasProvince::model()->getProvinceRunApp())){
                    GasOneMany::deleteManyByType($_POST['GasOneMany']['many_id'], ONE_AGENT_MAINTAIN);
//                }
            }
            GasOneMany::saveArrOfManyId($_GET['agent_id'], ONE_AGENT_MAINTAIN);
            $mAppCache = new AppCache();
            $mAppCache->setUserMaintainOfAgent($_GET['agent_id']);
            $msg='Cập Nhật Thành Công';
        }
        $model->many_id = GasOneMany::getArrOfManyId($_GET['agent_id'], ONE_AGENT_MAINTAIN);
        $this->render('one_many/Update_agent_employee_maintain',array('model'=>$model,'msg'=>$msg));
    }
    
    /**
     * @Author: ANH DUNG Dec 13, 2016
     * @Todo: set array agent của user Giao Nhận hoặc KTBH để khi truy vấn lấy ra cho nhanh
     */
    public function setListAgentOfUser($aUid, $agent_id) {
        foreach($aUid as $uid){
            $mUser = Users::model()->findByPk($uid);
            if(is_null($mUser)){
                continue;
            }
            $setMultiAgent = true;
            if(in_array($mUser->province_id, GasProvince::model()->getProvinceRunApp())){
                $setMultiAgent = false;
            }
            $mUser->setListAgentOfUser($agent_id, $setMultiAgent);
        }
    }

    public function actionUpdate_agent_employee_accounting(){
        $this->layout='ajax';
        $model=new GasOneMany();		
        $msg='';
        $model->one_id = $_GET['agent_id'];
        if(isset($_POST['GasOneMany'])){
            if(isset($_POST['GasOneMany']['many_id'])){
                GasOneMany::deleteManyByType($_POST['GasOneMany']['many_id'], ONE_AGENT_ACCOUNTING);
                $this->setListAgentOfUser($_POST['GasOneMany']['many_id'], $_GET['agent_id']);
            }
            GasOneMany::saveArrOfManyId($_GET['agent_id'], ONE_AGENT_ACCOUNTING);
            $msg='Cập Nhật Thành Công';
        }
        $model->many_id = GasOneMany::getArrOfManyId($_GET['agent_id'], ONE_AGENT_ACCOUNTING);
        $this->render('one_many/Update_agent_employee_accounting',array('model'=>$model,'msg'=>$msg));	
    }
    
    /**
     * @Author: ANH DUNG Jul 09, 2016
     * @Todo: update agent multi CCS for c#
     */
    public function actionUpdate_agent_ccs(){
        $this->layout='ajax';
        $model=new GasOneMany();		
        $msg='';
        $model->one_id = $_GET['agent_id'];
        if(isset($_POST['GasOneMany'])){
//            if(isset($_POST['GasOneMany']['many_id'])){// Jul 10, 2016 với CCS thì không xóa giống NV phục vụ KH, vì 1 ccs có thể thuộc nhiều đại lý
//                GasOneMany::deleteManyByType($_POST['GasOneMany']['many_id'], ONE_AGENT_CCS);
//            }
            GasOneMany::saveArrOfManyId($_GET['agent_id'], ONE_AGENT_CCS);
            $msg='Cập Nhật Thành Công';
        }
        $model->many_id = GasOneMany::getArrOfManyId($_GET['agent_id'], ONE_AGENT_CCS);
        $this->render('one_many/Update_agent_ccs',array('model'=>$model,'msg'=>$msg));	
    }
    /**
     * @Author: ANH DUNG Jul 30, 2016
     * @Todo: update agent multi Car 'UpdateAgentPhuXe','UpdateAgentDriver',
     */
    public function actionUpdateAgentCar(){
        $this->layout='ajax';
        $model=new GasOneMany();		
        $msg='';
        $model->one_id = $_GET['agent_id'];
        if(isset($_POST['GasOneMany']) && !empty($_GET['agent_id'])){
//            if(isset($_POST['GasOneMany']['many_id'])){// Jul 30, 2016 với Car thì không xóa giống NV phục vụ KH, vì 1 Car có thể thuộc nhiều đại lý
//                GasOneMany::deleteManyByType($_POST['GasOneMany']['many_id'], ONE_AGENT_CCS);
//            }
            GasOneMany::saveArrOfManyId($_GET['agent_id'], ONE_AGENT_CAR);
            $msg='Cập Nhật Thành Công';
        }
        $model->many_id = GasOneMany::getArrOfManyId($_GET['agent_id'], ONE_AGENT_CAR);
        $this->render('one_many/UpdateAgentCar',array('model'=>$model,'msg'=>$msg));	
    }
    /**
     * @Author: ANH DUNG Jul 30, 2016
     * @Todo: update agent multi Car
     */
    public function actionUpdateAgentDriver(){
        $this->layout='ajax';
        $model=new GasOneMany();		
        $msg='';
        $model->one_id = $_GET['agent_id'];
        if(isset($_POST['GasOneMany'])){
            GasOneMany::saveArrOfManyId($_GET['agent_id'], ONE_AGENT_DRIVER);
            $msg='Cập Nhật Thành Công';
        }
        $model->many_id = GasOneMany::getArrOfManyId($_GET['agent_id'], ONE_AGENT_DRIVER);
        $this->render('one_many/UpdateAgentDriver',array('model'=>$model,'msg'=>$msg));	
    }
    
    /**
     * @Author: ANH DUNG Jul 30, 2016
     * @Todo: update agent multi Car '','UpdateAgentDriver',
     */
    public function actionUpdateAgentPhuXe(){
        $this->layout='ajax';
        $model=new GasOneMany();		
        $msg='';
        $model->one_id = $_GET['agent_id'];
        if(isset($_POST['GasOneMany'])){
            GasOneMany::saveArrOfManyId($_GET['agent_id'], ONE_AGENT_PHU_XE);
            $msg='Cập Nhật Thành Công';
        }
        $model->many_id = GasOneMany::getArrOfManyId($_GET['agent_id'], ONE_AGENT_PHU_XE);
        $this->render('one_many/UpdateAgentPhuXe',array('model'=>$model,'msg'=>$msg));	
    }

    public function actionError()
    {
        if($error=Yii::app()->errorHandler->error)
        {
            $eMessage = strtolower($error['message']);

            if(strpos($eMessage, "able to resolve the request") || strpos($eMessage, "authorized to perform")
                    || strpos($eMessage, "command failed")
            ){
                $error['message'] = 'Bạn không có quyền truy cập hoặc trang yêu cầu không tồn tại';
                $uid = isset(Yii::app()->user->id)?Yii::app()->user->id:'EMPTY';
                Yii::log("Hack Try By UID: $uid ", 'error');
            }
            if(Yii::app()->request->isAjaxRequest)
                echo $error['message'];
            else
                $this->render('error', $error);
        }
        
        if(isset($_GET['message'])){// Jul2519 DungNT catch error redirect not write log
            $mErrorHandler      = new CErrorHandler();
            $error              = $mErrorHandler->error;
            $error['message']   = $_GET['message'];
            $this->render('error', $error);
        }
        
    }

    public function actionIndex()
    {           
        $this->pageTitle = 'Trang Chủ';
            $this->render('index');
    }

/**
 * Displays the login page
 */
public function actionLogin()
{        
    try {
        if(isset(Yii::app()->user->id))
            $this->redirect(Yii::app()->createAbsoluteUrl('admin'));
        $model=new AdminLoginForm;
        if(isset($_POST['AdminLoginForm']))
        {
            $model->attributes=$_POST['AdminLoginForm'];
            $model->username =  trim($model->username);
            $model->password =  trim($model->password);
            if($model->validate()){
                $cRole = MyFormat::getCurrentRoleId();
                /* Change at yii 1.1.13:
                 * we not use: if (strpos(Yii::app()->user->returnUrl,'/index.php')===false) to check returnUrl
                 */  
                GasCheck::checkSubUserAgent();
                MyFunctionCustom::initNameOfRole();
                GasTrackLogin::SaveTrackLogin(); // Aug 22, 2014
                GasCheck::CheckAllowAdmin();// Now 26, 2014
//                GasCheck::setCallCenterExt($cRole);
                $mGasCheck = new GasCheck();
                $mGasCheck->setCallCenterExt($cRole);
                if (strtolower(Yii::app()->user->returnUrl)!==strtolower(Yii::app()->baseUrl.'/')){
                    $this->redirect(Yii::app()->user->returnUrl);
                }

                switch ($cRole){
                    case ROLE_MANAGER:
                        $this->redirect(Yii::app()->createAbsoluteUrl('admin'));
                    break;
                    case ROLE_ADMIN:
                        $this->redirect(Yii::app()->createAbsoluteUrl('admin'));
                    break;

                    default :$this->redirect(Yii::app()->createAbsoluteUrl('admin'));
                }
            }
        }
        $this->render('login', array('model'=>$model));
    } catch (Exception $e) {
        Yii::log("Bug Login Exception ". $e->getMessage(), 'error');
        $code = 404;
        if(isset($e->statusCode))
            $code=$e->statusCode;
        if($e->getCode())
            $code=$e->getCode();            
        throw new CHttpException($code, "Invalid request login.");
    }

}

/**
* @Author: ANH DUNG Mar 12, 2017
* @Todo: set cache EXT for Employee
*/
public function setCallCenterExt($cRole) {
    throw new Exception('E003 This function not running. Please contact admin');
    $ext = CacheSession::getCookie(CacheSession::CURRENT_USER_EXT);
    if(!empty($ext)){// cập nhật User nào đang ngồi ở EXT nào vào cache
        usleep(rand(100000, 3000000));// 0.5->3s nên test kỹ lại với 3 đến 4 máy cùng 1 user submit 1 lúc random sleep khoảng 0.5s -> 3 second 
        $mAppCache = new AppCache();
        $mAppCache->setCallCenterExt($ext, MyFormat::getCurrentUid());
    }elseif($cRole == ROLE_CALL_CENTER && empty($ext)){
        if(Yii::app()->params['EnableChangeExt'] == 'no'):
            Yii::log(MyFormat::getCurrentUid()." CallCenter cố gắng login ở 1 máy không cho phép", 'error');
            GasCheck::LogoutUser();// will redirect to login page
        endif;
    }
}

/**
 * Logs out the current user and redirect to homepage.
 */
public function actionLogout()
{
    GasTrackLogin::SaveTrackLogin(GasTrackLogin::TYPE_LOGOUT_WEB, ''); // Aug 29, 2017
    GasCheck::LogoutUser();
}

    public function updateUid(){
        $i = 102;
        $criteria = new CDbCriteria;
        $criteria->addCondition('t.id>'.$i); 
        $models = Users::model()->findAll($criteria);
        foreach($models as $item){
                $item->id=$i;
                $item->update(array('id'));
                $i++;
        }					
    }

//*****************************  auto complete of search agent **************************************//
    // -- Nguyen Dung
    public function actionAutocompleteCustomerMaintain(){ 
        $criteria = new CDbCriteria();
        $criteria->addSearchCondition('t.address_vi', MyFunctionCustom::remove_vietnamese_accents($_GET['term']), true, 'OR'); // true ==> LIKE '%...%'
        $criteria->addSearchCondition('t.code_bussiness', $_GET['term'], true, 'OR'); // true ==> LIKE '%...%'

        $criteria->compare('t.role_id', ROLE_CUSTOMER); 
        $criteria->compare('t.type', CUSTOMER_TYPE_MAINTAIN); 
//            $criteria->addNotInCondition('t.type', array(CUSTOMER_TYPE_STORE_CARD)); 
        if(Yii::app()->user->role_id==ROLE_SUB_USER_AGENT){
            $criteria->compare('t.area_code_id',Yii::app()->user->parent_id);
        }    

        $criteria->limit = 30;
        $models = Users::model()->findAll($criteria);
        $returnVal=array();
        $cmsFormat = new CmsFormatter();
        foreach($models as $model)
        {
            $returnVal[] = array(    
                //'label'=>$model->code_account.' - '.$model->code_bussiness.' -- '.$model->first_name,
                'label'=>$model->code_bussiness.' -- '.$cmsFormat->formatNameUser($model).' -- '.$model->phone,
                'value'=>$cmsFormat->formatNameUser($model),
                'name_customer'=> $cmsFormat->formatNameUser($model),
                'id'=>$model->id,
                'name_agent'=>$model->name_agent,
                'code_account'=>$model->code_account,
                'code_bussiness'=>$model->code_bussiness,
                'address'=>$model->address,
                'phone'=>$model->phone,
            );
        }
        echo CJSON::encode($returnVal);
        Yii::app()->end();        
    }    		

    // -- Nguyen Dung Mar 03, 2014
    public function actionAutocompleteCustomerPTTT(){ 
        $criteria = new CDbCriteria();
        $criteria->addSearchCondition('t.address_vi', MyFunctionCustom::remove_vietnamese_accents($_GET['term']), true, 'OR'); // true ==> LIKE '%...%'
        $criteria->addSearchCondition('t.code_bussiness', $_GET['term'], true, 'OR'); // true ==> LIKE '%...%'

        $criteria->compare('t.role_id', ROLE_CUSTOMER); 
        $criteria->compare('t.type', CUSTOMER_TYPE_MARKET_DEVELOPMENT); 
//            $criteria->addNotInCondition('t.type', array(CUSTOMER_TYPE_STORE_CARD)); 
        if(Yii::app()->user->role_id==ROLE_SUB_USER_AGENT){
            $criteria->compare('t.area_code_id',Yii::app()->user->parent_id);
        }    

        $criteria->limit = 30;
        $models = Users::model()->findAll($criteria);
        $returnVal=array();
        $cmsFormat = new CmsFormatter();
        foreach($models as $model)
        {
            $returnVal[] = array(    
                //'label'=>$model->code_account.' - '.$model->code_bussiness.' -- '.$model->first_name,
                'label'=>$model->code_bussiness.' -- '.$cmsFormat->formatNameUser($model).' -- '.$model->phone,
                'value'=>$cmsFormat->formatNameUser($model),
                'name_customer'=> $cmsFormat->formatNameUser($model),
                'id'=>$model->id,
                'name_agent'=>$model->name_agent,
                'code_account'=>$model->code_account,
                'code_bussiness'=>$model->code_bussiness,
                'address'=>$model->address,
                'phone'=>$model->phone,
            );
        }
        echo CJSON::encode($returnVal);
        Yii::app()->end();        
    }    		

    // -- Nguyen Dung
    public function actionAutocompleteSellCustomerMaintainAndPTTT(){ 
        $criteria = new CDbCriteria();
        $criteria->addSearchCondition('t.address_vi', MyFunctionCustom::remove_vietnamese_accents($_GET['term']), true, 'OR'); // true ==> LIKE '%...%'
        $criteria->addSearchCondition('t.code_bussiness', $_GET['term'], true, 'OR'); // true ==> LIKE '%...%'

        $criteria->compare('t.role_id', ROLE_CUSTOMER); 
        $criteria->addNotInCondition('t.type', array(CUSTOMER_TYPE_STORE_CARD)); 
        if(Yii::app()->user->role_id==ROLE_SUB_USER_AGENT){
            $criteria->compare('t.area_code_id',Yii::app()->user->parent_id);
        }

        $criteria->addCondition("t.created_date>='".MyFormat::GetDateLimitSearchCustomerPttt()."'");

        $criteria->limit = 30;
        $criteria->order = 't.id DESC';
        $models = Users::model()->findAll($criteria);
        $returnVal=array();
        $cmsFormat = new CmsFormatter();
        foreach($models as $model)
        {
            $returnVal[] = array(    
                //'label'=>$model->code_account.' - '.$model->code_bussiness.' -- '.$model->first_name,
                'label'=>$model->code_bussiness.' -- '.$cmsFormat->formatNameUser($model).' -- '.$model->phone,
                'value'=>$cmsFormat->formatNameUser($model),
                'name_customer'=> $cmsFormat->formatNameUser($model),
                'id'=>$model->id,
                'name_agent'=>$model->name_agent,
                'code_account'=>$model->code_account,
                'code_bussiness'=>$model->code_bussiness,
                'address'=>$model->address,
                'phone'=>$model->phone,
            );
        }
        echo CJSON::encode($returnVal);
        Yii::app()->end();        
    }          

    public function actionChange_my_password()
    {
        $cRole = MyFormat::getCurrentRoleId();
        $cUid  = MyFormat::getCurrentUid();
        if(empty($cUid)){
            $this->redirect(array('login'));
        }
            
        $model=Users::model()->findByPk($cUid);
        if($model===null){
            Yii::log('The requested page does not exist.');
            throw new CHttpException(404,'The requested page does not exist.');
        }
        $model->scenario    = 'changeMyPassword';
        $model->md5pass     = $model->password_hash;

        if(isset($_POST['Users']))
        {
            $model->currentpassword     = $_POST['Users']['currentpassword'];
            $model->newpassword         = $_POST['Users']['newpassword'];
            $model->password_confirm    = $_POST['Users']['password_confirm'];
            if($model->validate()){
                $model->handleChangePass($_POST['Users']['newpassword']);
                Yii::app()->user->setFlash( MyFormat::SUCCESS_UPDATE, "Đổi Mật Khẩu Thành công" );
                $this->redirect(array('site/change_my_password'));
            }
        }

        $this->render('change_my_password',array(
                'model'=>$model,
        ));
    }        

    public function actionNews()
    {
        $id = isset($_GET['id'])?$_GET['id']:0;
        $model = Cms::model()->findByPk($id);
        if(is_null($model) || $model->status!=STATUS_ACTIVE)
            $model = Cms::getLatestNews();
        if(isset($_GET['HAS_READ_NEWS'])){
            $session=Yii::app()->session;
            $session['HAS_READ_NEWS'] = 1;
            $tempArrId = array();
            if(!isset($session['ARR_ID_NEWS'][$id]))
            {
                if(!isset($session['ARR_ID_NEWS'])){
                    $tempArrId = array($id=>$id);
                }else{
                    $tempArrId = $session['ARR_ID_NEWS'];
                    $tempArrId[$id]=$id;
                }
                $session['ARR_ID_NEWS'] = $tempArrId;
            }
        }        
        
        
        $this->render('News',array(
                'model'=>$model,'actions' => $this->listActionsCanAccess,
        ));
    }
    
    /**
     * @Author: ANH DUNG Jan 02, 2015
     * @Todo: tạm đóng chức năng thêm và sửa của thẻ kho
     */
    public function actionMaintenance() {
        $this->render('Maintenance',array(
                'actions' => $this->listActionsCanAccess,
        ));
    }
    
    /**
     * @Author: ANH DUNG Sep 02, 2015
     * @Todo: user profile
     */
    public function actionProfile() {
        $model = MyFormat::loadModelByClass(Yii::app()->user->id, 'Users');
        $model->LoadUsersRefImageSign();
        $model->mUsersRef->old_image_sign = $model->mUsersRef->image_sign;
        if(isset($_POST['UsersRef'])){
            $model->mUsersRef->attributes = $_POST['UsersRef'];
            $model->mUsersRef->image_sign  = CUploadedFile::getInstance($model->mUsersRef, 'image_sign');
            $model->mUsersRef->validate();
            UsersRef::validateFile($model);
            if(!$model->mUsersRef->hasErrors()){
                $this->handleSaveUpdate($model);
            }
        }
        
        $this->render('user/profile',array(
			'model'=>$model,
		));
    }
    
    public function handleSaveUpdate($model){
        $model->handleSaveUserRef();
        Yii::app()->user->setFlash( MyFormat::SUCCESS_UPDATE, 'Cập nhật thành công' );
        $this->redirect(array('profile'));
    }
    
    /** @Author: ANH DUNG May 28, 2018
     *  @Todo: check user is Guest or not
     **/
    public function isGuest() {
        $cRole = MyFormat::getCurrentRoleId();
        return in_array($cRole, [ROLE_PARTNER]);
    }
    
}