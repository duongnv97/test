<?php

class FollowCustomerController extends AdminController 
{
    public $pluralTitle = 'Daily Telesale';
    public $singleTitle = 'Daily Telesale';
    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id)
    {
        $this->pageTitle = 'Xem ';
        $this->layout='ajax';
        $model = $this->loadModel($id);
        // phần edit giây gọi Nghia Agu 13, 2018 
        if(isset($_POST['FollowCustomer']['bill_duration'])){
            $model->bill_duration = $_POST['FollowCustomer']['bill_duration'];
            $model->validate();
            if(!$model->hasErrors()){
                $model->save();
                Yii::app()->user->setFlash( MyFormat::SUCCESS_UPDATE, 'Cập nhật thành công' );
            }else{
                Yii::app()->user->setFlash( MyFormat::ERROR_UPDATE, 'Cập nhật thất bại' );
            }
            $this->redirect(array('view','id'=>$id));
        }
        try{
        $this->render('view',array(
                'model'=>$model, 'actions' => $this->listActionsCanAccess,
        ));
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }
    
    public function actionViewMultiNote($id)
    {
        $this->pageTitle = 'Xem danh sách ghi chú';
        try{
        $this->layout='ajax';
        $model=new FollowCustomer('create');
        $model->customer_id = $id;
        $this->render('view_multi_note',array(
                'model'=>$model, 'actions' => $this->listActionsCanAccess, //truyền model sẽ bị trùng trong actionCreate  
        ));
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate($id){
        $this->pageTitle    = 'Tạo Mới ';
        $this->layout       = 'ajax';
        $cRole = MyFormat::getCurrentRoleId();
        try{
        $model=new FollowCustomer('create');
        $model->customer_id = $id;
        $model->appointment_date = date('d/m/Y');
        if(!$model->canUpdateNoteInActionCreate()){
            Yii::app()->user->setFlash( MyFormat::ERROR_UPDATE, "Yêu cầu không hợp lệ" );
            $this->redirect(array('index'));
        }
        $ext = CacheSession::getCookie(CacheSession::CURRENT_USER_EXT);
        if(empty($ext) && $cRole != ROLE_ADMIN){
            throw new Exception('Bạn chưa được setup EXT, vui lòng liên hệ admin');
        }

        if(isset($_POST['FollowCustomer'])){
            $model->attributes=$_POST['FollowCustomer'];
            $model->handleSendSms();
            $model->validate();
            if(!$model->hasErrors()){
                $model->setDataJson();
                $model->setCallId();
                $model->save();
                $model->removeOneManyBig();
                die('<script type="text/javascript">parent.$.fn.colorbox.close(); parent.$.fn.yiiGridView.update("telesale-grid"); </script>'); 
                Yii::app()->user->setFlash( MyFormat::SUCCESS_UPDATE, "Thêm mới thành công." );
                $this->redirect(array('create','id'=>$id));
            }				
        }

        $this->render('create',array(
                'model'=>$model, 'actions' => $this->listActionsCanAccess,
            ));
        //$this->actionViewMultiNote($id);
        
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id)
    {
        $this->pageTitle = 'Cập Nhật ';
        $this->layout='ajax';
        try{
        $model=$this->loadModel($id);
        $model->appointment_date = MyFormat::dateConverYmdToDmy($model->appointment_date);

        $model->scenario = 'update';
        if(!$model->canUpdateNote()){
            Yii::app()->user->setFlash( MyFormat::ERROR_UPDATE, "Yêu cầu không hợp lệ" );
            $this->redirect(array('index'));
        }
        if(isset($_POST['FollowCustomer']))
        {
            $model->attributes=$_POST['FollowCustomer'];
            $model->validate();
            if(!$model->hasErrors()){
                $model->handleSendSms();
                $model->setCallId();
                $model->update();
                Yii::app()->user->setFlash( MyFormat::SUCCESS_UPDATE, "Cập nhật thành công." );
                $this->redirect(array('update','id'=>$model->id));
            }							
        }

        $this->render('update',array(
                'model'=>$model, 'actions' => $this->listActionsCanAccess,
        ));
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id)
    {
        try{
        if(Yii::app()->request->isPostRequest){
            // we only allow deletion via POST request
            if($model = $this->loadModel($id))
            {
                if($model->delete())
                    Yii::log("Uid: " .Yii::app()->user->id. " Delete record ".  print_r($model->attributes, true), 'info');
            }

            // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
            if(!isset($_GET['ajax']))
                    $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('index'));
        }
        else
        {
            Yii::log("Uid: " .Yii::app()->user->id. " Invalid request. Please do not repeat this request again.");
            throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
        }	
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }

    /**
     * Manages all models.
     */
    public function actionIndex()
    {
        $this->pageTitle = 'Danh Sách ';
        try{
        $model=new FollowCustomer('search');
        $model->unsetAttributes();  // clear any default values
        if(isset($_GET['FollowCustomer']))
                $model->attributes=$_GET['FollowCustomer'];

        $this->render('index',array(
                'model'=>$model, 'actions' => $this->listActionsCanAccess,
        ));
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
    * If the data model is not found, an HTTP exception will be raised.
     * @param integer the ID of the model to be loaded
     */
    public function loadModel($id)
    {
        try{
        $model=FollowCustomer::model()->findByPk($id);
        if($model===null)
        {
            Yii::log("The requested page does not exist.");
            throw new CHttpException(404,'The requested page does not exist.');
        }			
        return $model;
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }

}
