<?php

/** @Author: NamNH 08/01/2018 Task: NAM002
 *  @Todo: Quản lý thuê nhà
 *  @Param: 
 **/
class HomeContractDetailController extends AdminController
{
    public $pluralTitle = 'Yêu cầu thanh toán';
    public $singleTitle = 'Yêu cầu thanh toán';

    public function actionView($id)
    {
        $this->pageTitle = 'Chi tiết yêu cầu thanh toán hợp đồng thuê nhà';
        try{
        $this->render('view',array(
                'model'=>$this->loadModel($id), 'actions' => $this->listActionsCanAccess,
        ));
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }

    public function actionUpdate($id)
    {
        $this->pageTitle = 'Cập Nhật ';
        try {
            $model = $this->loadModel($id);
            $model->beforeUpdate();
            if (isset($_POST['HomeContractDetail'])) {
                $model->attributes = $_POST['HomeContractDetail'];
                $model->amount = MyFormat::removeComma($model->amount);
                $model->validate();
                if (!$model->hasErrors()) {
                    $model->update();
                    Yii::app()->user->setFlash(MyFormat::SUCCESS_UPDATE, "Cập nhật thành công.");
                    $this->redirect(array('update', 'id' => $model->id));
                }
            }
            $this->render('update', array(
                'model' => $model, 'actions' => $this->listActionsCanAccess,
            ));
        } catch (Exception $exc) {
            GasCheck::CatchAllExeptiong($exc);
        }
    }
   
    /**
     * Manages all models.
     */
    public function actionIndex()
    {
        $this->pageTitle = 'Danh sách yêu cầu thanh toán hợp đồng thuê nhà';
        try{
        $model=new HomeContractDetail('search');
        if(isset($_GET['HomeContractDetail'])){
            $model->attributes = $_GET['HomeContractDetail'];
            $model->agent_id = $_GET['HomeContractDetail']['agent_id'];
            $model->date_pay = $_GET['HomeContractDetail']['date_pay'];
        }
        if(isset($_GET['HomeContractDetail'])){
            $model->attributes = $_GET['HomeContractDetail'];
        }
        $this->render('index',array(
                'model'=>$model, 'actions' => $this->listActionsCanAccess,
        ));
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }
    
    /** @Author: NamNH 12/01/2018
     *  @Todo: load model
     *  @Param: 
     **/
    public function loadModel($id) {
        try {
            $model = HomeContractDetail::model()->findByPk($id);
            if ($model === null) {
                Yii::log("The requested page does not exist.");
                throw new CHttpException(404, 'The requested page does not exist.');
            }
            return $model;
        } catch (Exception $exc) {
            GasCheck::CatchAllExeptiong($exc);
        }
    }
    
    /** @Author: NamNH 12/01/2018 
     *  @Todo: Tự động tạo ra detail  
     *  @Param: admin/homeContractDetail/generate
     **/
    public function actionGenerate(){
        $model=new HomeContractDetail;
        $model->debug = true;
        $model->setUidLeader(GasConst::UID_HIEP_TV);
        $model->windowInsert('2019-09-01', '2019-09-30');
        $this->redirect(array('index',));
    }
    
}
