<?php

class ContractManagementController extends AdminController 
{
    public $pluralTitle = 'Hợp Đồng';
    public $singleTitle = 'Hợp Đồng';
    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id)
    {
        $this->pageTitle = 'Xem ';
        try{
        $this->render('view',array(
                'model'=>$this->loadModel($id), 'actions' => $this->listActionsCanAccess,
        ));
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }

    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate()
    {
        $this->pageTitle = 'Tạo Mới ';
        try{
        $model=new ContractManagement('create');

        if(isset($_GET['contractType'])){
            $this->getAjaxTemplateByType($_GET['contractType']);
        }
        if(isset($_POST['ContractManagement']))
        {
            $model->attributes=$_POST['ContractManagement'];
            $cUid = MyFormat::getCurrentUid();
            $model->created_by = $cUid;
            $model->handleJsonSave();
            $model->validate();
            if(!$model->hasErrors()){
                $model->save();
                Yii::app()->user->setFlash( MyFormat::SUCCESS_UPDATE, "Thêm mới thành công." );
                $this->redirect(array('create'));
            }				
        }

        $this->render('create',array(
                'model'=>$model, 'actions' => $this->listActionsCanAccess,
        ));
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id)
    {
        $this->pageTitle = 'Cập Nhật ';
        try{
        $model=$this->loadModel($id);
        $model->scenario = 'update';
        if(isset($_POST['ContractManagement']))
        {
            $model->attributes=$_POST['ContractManagement'];
            $model->handleJsonSave();
            $model->validate();
            if(!$model->hasErrors()){
                $model->save();
                Yii::app()->user->setFlash( MyFormat::SUCCESS_UPDATE, "Cập nhật thành công." );
                $this->redirect(array('update','id'=>$model->id));
            }							
        }

        $this->render('update',array(
                'model'=>$model, 'actions' => $this->listActionsCanAccess,
        ));
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id)
    {
        try{
        if(Yii::app()->request->isPostRequest){
            // we only allow deletion via POST request
            if($model = $this->loadModel($id))
            {
                if($model->delete())
                    Yii::log("Uid: " .Yii::app()->user->id. " Delete record ".  print_r($model->attributes, true), 'info');
            }

            // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
            if(!isset($_GET['ajax']))
                    $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('index'));
        }
        else
        {
            Yii::log("Uid: " .Yii::app()->user->id. " Invalid request. Please do not repeat this request again.");
            throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
        }	
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }

    /**
     * Manages all models.
     */
    public function actionIndex()
    {
        $this->pageTitle = 'Danh Sách ';
        try{
        $model=new ContractManagement('search');
        $model->unsetAttributes();  // clear any default values
        if(isset($_GET['ContractManagement']))
                $model->attributes=$_GET['ContractManagement'];

        $this->render('index',array(
                'model'=>$model, 'actions' => $this->listActionsCanAccess,
        ));
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
    * If the data model is not found, an HTTP exception will be raised.
     * @param integer the ID of the model to be loaded
     */
    public function loadModel($id)
    {
        try{
        $model=ContractManagement::model()->findByPk($id);
        if($model===null)
        {
            Yii::log("The requested page does not exist.");
            throw new CHttpException(404,'The requested page does not exist.');
        }			
        return $model;
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }
    
    /** @Author: DuongNV Sep 25, 2018
     *  @Todo: Get html ajax khi change loại hợp đồng create, update
     *  @param type contract_type(db)
     **/
    public function getAjaxTemplateByType($type){
        $mTemplate = EmailTemplates::model()->findByPk($type);
        if(empty($mTemplate)) {return '';}
        $html = $mTemplate->email_body;
        $isUpdate = false;
        $mContract = new ContractManagement();
        if(!empty($_GET['id'])){
            $mContract = ContractManagement::model()->findByPk($_GET['id']);
            if($mContract->contract_type == $type){
                $isUpdate = true;
            }
        }
        if($isUpdate){
            $html = $mContract->template;
        }
        $mContract->handleTemplate($html);
        echo $html;die;
    }

    /** @Author: DuongNV Oct 06, 2018
     *  @Todo: export pdf contract
     *  @param id of contract, if download = true -> download pdf instead of view in browser
     **/
    public function actionToPdf($id, $download = false) {
        try {
            $model = new ContractManagement();
            if($download){
                $model->pdfOutput = 'D'; // Download
            } else {
                $model->pdfOutput = 'I'; //Xuất trên trình duyệt
            }
//            https://github.com/spipu/html2pdf/blob/master/doc/output.md
            $model->toPdfContract($id);
        } catch (Exception $exc) {
            GasCheck::CatchAllExeptiong($exc);
        }
    }
}
