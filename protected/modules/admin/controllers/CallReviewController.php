<?php

class CallReviewController extends AdminController 
{
    public $pluralTitle = 'Kiểm tra cuộc gọi';
    public $singleTitle = 'Kiểm tra cuộc gọi';

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page. 
     */
    public function actionCreate()
    {
        error_reporting(1);
        $this->pageTitle = 'Tạo Mới ';
        try{
        $model= new CallReview('create');
        $mCall = new Call();
        // set ajax
         $this->layout       = 'ajax';
        if(!empty($_GET['Call'])){
            $model->call_id       = $_GET['Call'];
            $model->handleForReason();
            $mCall = Call::model()->findByPk($_GET['Call']);
        }
        if(empty($mCall)){
            throw new Exception('Model call not found !');
        }
        // nhả cuộc gọi
        if(isset($_GET['deny']) && isset($_POST['isDeny']) && $_POST['isDeny'] == 1 ){
            $mCall->handleDenyCallToReview();
            Yii::app()->user->setFlash( MyFormat::SUCCESS_UPDATE, "Đã nhả cuộc gọi." );
        }
        if(isset($_POST['CallReview']))
        {
            $model->attributes=$_POST['CallReview'];
            $mCall->attributes=$_POST['Call'];
            $model->handlToSave();
//            $model->validate();
            if(!$model->hasErrors()){
                $model->deleteAllByCallId();
                $model->makeCallReview();
                $mCall->handleSaveWithCallReview($model->call_id,$_POST['Call']['customer_new'],$_POST['Call']['note']);
                Yii::app()->user->setFlash( MyFormat::SUCCESS_UPDATE, "Thêm mới thành công." );
            }
        }
        
        $this->render('create',array(
            'model'=>$model, 
            'mCall' => $mCall,
            'actions' => $this->listActionsCanAccess,
        ));
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }
    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id)
    {
        try{
        if(Yii::app()->request->isPostRequest){
            // we only allow deletion via POST request
            if($model = $this->loadModel($id))
            {
                if($model->delete())
                    Yii::log("Uid: " .Yii::app()->user->id. " Delete record ".  print_r($model->attributes, true), 'info');
            }

            // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
            if(!isset($_GET['ajax']))
                    $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('index'));
        }
        else
        {
            Yii::log("Uid: " .Yii::app()->user->id. " Invalid request. Please do not repeat this request again.");
            throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
        }	
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }
    /**
     * Manages all models.
     */
    public function actionListDetail()
    {
        $this->pluralTitle  = 'Chi tiết kiểm tra cuộc gọi';
        $this->pageTitle    = 'Chi tiết kiểm tra cuộc gọi';
        try{
        $model=new CallReview('search');
        $model->date_from = '01-'.date("m-Y");
        $model->date_to = date("d-m-Y");
        $model->unsetAttributes();  // clear any default values
        if(isset($_GET['CallReview'])){
            $model->attributes=$_GET['CallReview'];
        }
        $this->exportExcelCallReview($model);
        $this->render('listDetail/index',array(
                'model'=>$model, 'actions' => $this->listActionsCanAccess,
        ));
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }
    
    public function actionListCall()
    {
        $this->pageTitle = 'Kiểm tra cuộc gọi';
        try{
        $mCall = new Call();
        $mCall->unsetAttributes();  // clear any default values
        $mCall->direction       = Call::DIRECTION_INBOUND;
        $mCall->type            = Call::QUEUE_HGD;
        $mCall->initParamDate();
        $this->handleVarliableGetType($mCall);
        if(isset($_GET['Call'])){
            $mCall->attributes=$_GET['Call'];
        }
        if(isset($_POST['Call']) && !$this->hasErrorApplyUserCheckCall() ){
            $mCall->saveUserCheckForCall($_POST['Call']['id'],$_POST['Call']['user_check_call']);
            Yii::app()->user->setFlash( MyFormat::SUCCESS_UPDATE, "GÁN THÀNH CÔNG ".count($_POST['Call']['id'])." CUỘC GỌI" );
        }
        
        $this->render('Call/ListCall',array(
            'actions' => $this->listActionsCanAccess,
            'mCall' => $mCall,
        ));
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
    * If the data model is not found, an HTTP exception will be raised.
     * @param integer the ID of the model to be loaded
     */
    public function loadModel($id)
    {
        try{
        $model=CallReview::model()->findByPk($id);
        if($model===null)
        {
            Yii::log("The requested page does not exist.");
            throw new CHttpException(404,'The requested page does not exist.');
        }			
        return $model;
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }
    
    /** @Author: Pham Thanh Nghia Aug 17, 2018
     *  @Todo: nhận biến GET['type'] cho button index vào model
     * Có 3 tab: Chưa nhận user_check_call  = 0, 
     * chưa test: status_check_call = 1; 
     * đã test: status_check_call = 2
     *  @Param:
     **/
    public function handleVarliableGetType(&$model){
        return ; // Sep0218 AnhDung
        if(!isset($_GET['type'])){
            $model->user_check_call = -2; // nếu là -2 sẽ nhận giá trị 0 
        }
        if(isset($_GET['type']) && $_GET['type'] == CallReview::RECEIVED){
            $model->user_check_call = -1 ; // đặt biến bên call sẽ hiểu không nhân giá trị 0
        }
        if(isset($_GET['type']) && $_GET['type'] == CallReview::TESTED){
            $model->status_check_call =  2;
        }
    }
    public function hasErrorApplyUserCheckCall() {
        if(!isset($_POST['Call']['id']) || empty($_POST['Call']['user_check_call']) ){
            Yii::app()->user->setFlash( MyFormat::ERROR_UPDATE, "CHƯA ĐỦ THÔNG TIN" );
            return true;
        }
        return false;
    }
    /** @Author: Pham Thanh Nghia Sep,24 2018
     *  @Todo: báo cáo cho cả 2 loại
     **/
    public function actionReportCallReview(){
        $this->pageTitle = 'Báo cáo kiểm tra cuộc gọi';
        try{
            $model = new CallReview();
            $model->unsetAttributes();  // clear any default values
            $model->created_date = date("d-m-Y");
            $model->date_from = '01-'.date("m-Y");
            $model->date_to = date("d-m-Y");
            $data = '';
            if(isset($_GET['CallReview'])){
                $model->attributes=$_GET['CallReview'];
            }
            if(isset($_GET['type']) && $_GET['type']== CallReview::BC_TONGDAI){ 
                $data = $model->getDataReportForCallCenter();
            }elseif(isset($_GET['type']) && $_GET['type']== CallReview::BC_KIEMTRA_TONGDAI) {
//                $data = $model->getDataReportForCheckOnCenter();
                $data = $model->getDataReportForCallCenterOnDay();
            }elseif(isset($_GET['type']) && $_GET['type']== CallReview::BC_KIEMTRA) {
                $data = $model->getDataReportForUserCheck();
            }
            $this->exportExcelCallReviewForReport($model);// chạy trước empty data
            
            if(empty($data)){
                $this->redirect(array('callReview/ReportCallReview/type/'.CallReview::BC_KIEMTRA));
            }
            $this->render('reportCallReview/index',array(
                'actions' => $this->listActionsCanAccess,
                'data' => $data,
                'model' => $model,
            ));
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }
    /** @Author: Pham Thanh Nghia Oct,17 2018
     *  @Todo: export excel for report 
     **/
    public function exportExcelCallReviewForReport($model){
        try{
            if(!$model->canExportExcel() || !isset($_GET['ExportExcel'])){
                return ;
            }
            if(isset($_GET['ExportExcel']) && isset($_SESSION['dataReportForCallCenter'])){
                ini_set('memory_limit','2000M');// chưa cần chạy đoạn này, đưa memory_limit lên 500 có thể export dc khoảng 5000 row - chưa test
                $mToExcel1 = new ToExcel1();
                $mToExcel1->exportCallReviewForCallCenter($model);
            }
            if(isset($_GET['ExportExcel']) && isset($_SESSION['dataReportForUserCheck'])){
                ini_set('memory_limit','2000M');
                $mToExcel1 = new ToExcel1();
                $mToExcel1->exportCallReviewForUserCheck($model);
            }
            if(isset($_GET['ExportExcel']) && isset($_SESSION['dataReportForCallCenterOnDay'])){
                ini_set('memory_limit','2000M');
                $mToExcel1 = new ToExcel1();
//                $mToExcel1->exportCallReviewForCheckOnCenter($model);
                $mToExcel1->exportCallReviewForCallCenterOnDay($model);
            }
            $this->redirect(array('ReportCallReview'));
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }
    /** @Author: Pham Thanh Nghia Dec 21, 2018
     *  @Todo: admin/usersPrice/reportManage
     **/
    public function exportExcelCallReview($model){
        try{
            if(!$model->canExportExcel() || !isset($_GET['ExportExcel'])){
                return ;
            }
            if(isset($_GET['ExportExcel']) && isset($_SESSION['dataCallReview'])){
                ini_set('memory_limit','2000M');// chưa cần chạy đoạn này, đưa memory_limit lên 500 có thể export dc khoảng 5000 row - chưa test
                $mToExcel1 = new ToExcel1();
                $mToExcel1->exportCallReviewForListDetail($model);
            }
            $this->redirect(array('listDetail'));
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }
    
}
