<?php

class EmployeeProblemsController extends AdminController 
{
    public $pluralTitle = 'Lỗi Phạt Phản Ánh Sự Việc';
    public $singleTitle = 'Lỗi Phạt Phản Ánh Sự Việc';
    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id)
    {
        $this->pageTitle = 'Xem ';
        try{
        $this->render('view',array(
                'model'=>$this->loadModel($id), 'actions' => $this->listActionsCanAccess,
        ));
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }

    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id)
    {
        try{
        if(Yii::app()->request->isPostRequest){
            // we only allow deletion via POST request
            if($model = $this->loadModel($id))
            {
                if($model->delete())
                    Yii::log("Uid: " .Yii::app()->user->id. " Delete record ".  print_r($model->attributes, true), 'info');
            }

            // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
            if(!isset($_GET['ajax']))
                    $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('index'));
        }
        else
        {
            Yii::log("Uid: " .Yii::app()->user->id. " Invalid request. Please do not repeat this request again.");
            throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
        }	
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }

    /**
     * Manages all models.
     */
    public function actionIndex()
    {
        $this->pageTitle = 'Lỗi Phạt Phản Ánh Sự Việc';
        try{
        $model=new EmployeeProblems('search');
        $model->unsetAttributes();  // clear any default values
        $model->date_from   = '01-'.date('m-Y');
        $model->date_to     = date('d-m-Y');
        if(isset($_GET['EmployeeProblems']))
                $model->attributes=$_GET['EmployeeProblems'];

        $this->exportExcel();
        $this->render('index',array(
                'model'=>$model, 'actions' => $this->listActionsCanAccess,
        ));
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
    * If the data model is not found, an HTTP exception will be raised.
     * @param integer the ID of the model to be loaded
     */
    public function loadModel($id)
    {
        try{
        $model=EmployeeProblems::model()->findByPk($id);
        if($model===null)
        {
            Yii::log("The requested page does not exist.");
            throw new CHttpException(404,'The requested page does not exist.');
        }			
        return $model;
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }
    
    public function actionGetExpired()
    {
        $model = new EmployeeProblems();
        $curdate = date('Y-m-d');
        $model->generateExpired($curdate);
        $this->redirect(array('index'));
    }
    
    /** @Author: DuongNV 11/09/2018
     *  @Todo: export excel Employee Problems
     *  @Param: $isDetail: true -> detail view, false -> statistic view
     **/
    public function exportExcel() {
        if(!isset($_GET['excel'])) return;
        $isDetail = isset($_GET['isDetail']) ? $_GET['isDetail'] : false;
        try{
            $model = new EmployeeProblems();
            if($model->canExportExcel())
            {
                if($isDetail){
                    if(isset($_SESSION['data-excel']) && isset($_SESSION['data-model'])){
                        ini_set('memory_limit','1500M');// chưa cần chạy đoạn này, đưa memory_limit lên 500 có thể export dc khoảng 5000 row - chưa test
                        ToExcel::summaryEmployeeProblems($isDetail);
                    }
                }else{
                    if(isset($_SESSION['data-excel-statistic']) && isset($_SESSION['data-model-statistic'])){
                        ini_set('memory_limit','1500M');// chưa cần chạy đoạn này, đưa memory_limit lên 500 có thể export dc khoảng 5000 row - chưa test
                        ToExcel::summaryEmployeeProblems($isDetail);
                    }
                }
                $this->redirect(array('index'));
            } else {
                Yii::log("Bạn không có quyền truy cập trang.");
                throw new CHttpException(404,'Bạn không có quyền truy cập trang.');
            }			
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }
    /** @Author: Pham Thanh Nghia 2018
     *  @Todo:  thống kê số tiền theo từng user trong table EmployeeProblems
     *  @Param: Statistics money
     **/
    public function actionStatisticsMoney(){
        $this->pageTitle = 'Tiền phạt nhân viên';
        try{
        $model=new EmployeeProblems('search');
        $model->unsetAttributes();  // clear any default values
        $model->date_from   = '01-'.date('m-Y');
        $model->date_to     = date('d-m-Y');
        if(isset($_GET['EmployeeProblems']))
            $model->attributes=$_GET['EmployeeProblems'];

        $data = $model->searchStatisticsMoney();
        $this->render('statisticsMoney/index',array(
            'model'=>$model, 
            'actions' => $this->listActionsCanAccess,
            'data' => $data,
        ));
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }

    /** @Author: DuongNV Oct 2, 2018
     *  @Todo: BC loại lỗi phạt
     **/
    public function actionReportFaultType() {
        $this->pageTitle = 'Báo cáo loại lỗi phạt theo đại lý';
        try {
            $this->exportExcelReportFaultType();
            $model = new EmployeeProblems();
            $model->unsetAttributes();  // clear any default values
            $model->date_from   = '01-' . date('m-Y');
            $model->date_to     = date('d-m-Y');
            $model->object_type = EmployeeProblems::PROBLEM_HGD_5_MINUTES;
            if (isset($_GET['EmployeeProblems'])){
                $model->attributes = $_GET['EmployeeProblems'];
            }
            $data = [];
            if(isset($_GET['is_employee'])){
                $this->pageTitle = 'Báo cáo loại lỗi phạt theo nhân viên';
                $data = $model->searchReportFaultType(true);
            } else {
                $data = $model->searchReportFaultType();
            }
//            $this->render('report/report_fault_type', array(
            $this->render('report/index_button', array(
                'model' => $model,
                'actions' => $this->listActionsCanAccess,
                'data' => $data,
            ));
        } catch (Exception $exc) {
            GasCheck::CatchAllExeptiong($exc);
        }
    }
    
    /** @Author: DuongNV Oct 2, 2018
     *  @Todo: export excel BC loại lỗi phạt
     **/
    public function exportExcelReportFaultType() {
        try{
            if(isset($_GET['toExcel'])){
                if(empty($_SESSION['data-ep-excel'])){
                    $_SESSION['data-ep-excel'] = EmployeeProblems::model()->searchReportFaultType();
                    $_SESSION['data-ep-model'] = new EmployeeProblems();
                }
                ini_set('memory_limit','1500M');// chưa cần chạy đoạn này, đưa memory_limit lên 500 có thể export dc khoảng 5000 row - chưa test
                ToExcel::summaryReportFaultType();
            }
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }
    
    /** @Author: DungNT May 05, 2019
     *  @Todo: chuyển trạng thái lỗi sang loại trừ
     **/
    public function actionUpdateStatus($id){
        $this->layout = 'ajax';
        try{
            $model = $this->loadModel($id);
            $model->scenario = 'updateStatus';
            if (isset($_POST['EmployeeProblems'])) {
                $model->attributes = $_POST['EmployeeProblems'];
                $model->setParamsUpdate();
                $model->validate();
                if (!$model->hasErrors()) {
                    if ($model->handleUpdateStatus()) {
                        die('<script type="text/javascript">parent.$.fn.colorbox.close(); parent.$.fn.yiiGridView.update("employee-problems-grid"); </script>');
                    }
                }
            }
            if ($model->hasErrors()) {
                Yii::app()->user->setFlash(MyFormat::ERROR_UPDATE, 'Có lỗi xảy ra, không thể cập nhật trạng thái');
            }
            
            $this->render('updateStatus/exceptError',array(
                    'model' => $model, 'actions' => $this->listActionsCanAccess,
            ));
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }
    
}
