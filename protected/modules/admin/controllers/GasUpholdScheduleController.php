<?php

class GasUpholdScheduleController extends AdminController 
{
    public $pluralTitle = 'Bảo Trì Định Kỳ';
    public $singleTitle = 'Bảo Trì Định Kỳ';
    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id)
    {
        try{
            $model = $this->loadModel($id);
            $this->pageTitle = $model->getCustomer().' - Xem Bảo Trì Định Kỳ';
            $model->mReply = new GasUpholdReply();
            $model->mReply->scenario = 'create';
            $model->mReply->uphold_id = $id;
            $model->mReply->status = $model->status;
//            $model->mReply->mGasFile = new GasFile();
            $this->HandleReply($model);
            $this->render('view',array(
                    'model' => $model, 'actions' => $this->listActionsCanAccess,
            ));
        }
        catch (Exception $exc)
        {
            GasCheck::CatchAllExeptiong($exc);
        }
    }
    
    /**
     * @Author: ANH DUNG Oct 21, 2015
     * @Todo: handle post reply
     */
    public function HandleReply($model) {
        if(isset($_POST['GasUpholdReply']))
        {
            $model->mReply->attributes=$_POST['GasUpholdReply'];
            $model->mReply->validate();
            $mGasFile = new GasFile();
            $mGasFile->validateFile($model->mReply);
            if(!$model->mReply->hasErrors()){
                $model->mReply->save();
                $mUpholdReply = GasUpholdReply::model()->findByPk($model->mReply->id);
                $mGasFile->saveRecordFile($mUpholdReply, GasFile::TYPE_3_UPHOLD_REPLY);
                Yii::app()->user->setFlash( MyFormat::SUCCESS_UPDATE, "Thêm mới thành công." );
                $this->redirect(array('view','id'=>$model->id));
            }
        }
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate()
    {
        $this->pageTitle = 'Tạo Mới Bảo Trì Định Kỳ';
        try
        {
        $model=new GasUphold('createSchedule1');
        $model->type        = GasUphold::TYPE_DINH_KY;
        $model->manage_id   = GasSupportCustomer::UID_QUY_PV;
        if(isset($_GET['customer_id'])){
            $model->customer_id = $_GET['customer_id'];
        }

        if(isset($_POST['GasUphold']))
        {
            $model->attributes=$_POST['GasUphold'];
//            $model->ResetScenario(); Close on Jan 31, 2016
//            $model->validate();Close on Jan 31, 2016
            $model->SaveMultiUpholdSchedule();
//            if(!$model->hasErrors()){
            if(1){// On Jan 31, 2016
//                $model->save();Close on Jan 31, 2016
//                $model->SaveMultiDay();Close on Jan 31, 2016
                
                Yii::app()->user->setFlash( MyFormat::SUCCESS_UPDATE, "Thêm mới thành công." );
                $this->redirect(array('create'));
            }				
        }

        $this->render('create',array(
                'model'=>$model, 'actions' => $this->listActionsCanAccess,
        ));
        }
        catch (Exception $exc)
        {
            GasCheck::CatchAllExeptiong($exc);
        }
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id)
    {
        try{
        $model=$this->loadModel($id);
        $this->pageTitle = $model->getCustomer().' - Cập Nhật Bảo Trì Định Kỳ';
        if(!$model->CanUpdateUpholdSchedule() || $model->type != GasUphold::TYPE_DINH_KY){
            $this->redirect(array('index'));
        }
        $model->GetMultiDay();
        $model->formatDataBeforeUpdate();
        $model->ResetScenario();
        if(isset($_POST['GasUphold']))
        {
            $model->attributes=$_POST['GasUphold'];
            $model->ResetScenario();
            $model->validate();
            if(!$model->hasErrors()){
                $model->setDbDate();
                $model->save();
                $model->SaveMultiDay();
                $model->removePlanNotDone();
                Yii::app()->user->setFlash( MyFormat::SUCCESS_UPDATE, "Cập nhật thành công." );
                $this->redirect(array('update','id'=>$model->id));
            }
        }

        $this->render('update',array(
                'model'=>$model, 'actions' => $this->listActionsCanAccess,
        ));
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id)
    {
        try{
        if(Yii::app()->request->isPostRequest)
        {
            // we only allow deletion via POST request
            if($model = $this->loadModel($id))
            {
                if($model->delete())
                    Yii::log("Uid: " .Yii::app()->user->id. " Delete record ".  print_r($model->attributes, true), 'info');
            }

            // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
            if(!isset($_GET['ajax']))
                    $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('index'));
        }
        else
        {
            Yii::log("Uid: " .Yii::app()->user->id. " Invalid request. Please do not repeat this request again.");
            throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
        }	
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }

    /**
     * Manages all models.
     */
    public function actionIndex()
    {
        $this->pageTitle = 'Bảo trì định kỳ ';
        try{
        $this->handleDetail();
        $model=new GasUphold('search');
        $model->unsetAttributes();  // clear any default values
//        $model->status_uphold_schedule = GasUphold::STATUS_UPHOLD_RUNNING;
        if(isset($_GET['GasUphold']))
            $model->attributes=$_GET['GasUphold'];
        $model->type = GasUphold::TYPE_DINH_KY;

        $this->render('index',array(
                'model'=>$model, 'actions' => $this->listActionsCanAccess,
        ));
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }
    
    /**
     * @Author: ANH DUNG Dec 19, 2016
     * @Todo: handle list detail
     */
    public function handleDetail() {
        if(!isset($_GET['detail'])){
            return ;
        }
        $model=new GasUpholdReply('search');
        $model->unsetAttributes();  // clear any default values
        if(isset($_GET['GasUpholdReply']))
            $model->attributes=$_GET['GasUpholdReply'];
        $model->type = GasUphold::TYPE_DINH_KY;

        $this->render('detail/index',array(
                'model'=>$model, 'actions' => $this->listActionsCanAccess,
        ));
        
        die;
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
    * If the data model is not found, an HTTP exception will be raised.
     * @param integer the ID of the model to be loaded
     */
    public function loadModel($id)
    {
        try{
        $model=GasUphold::model()->findByPk($id);
        if($model===null)
        {
            Yii::log("The requested page does not exist.");
            throw new CHttpException(404,'The requested page does not exist.');
        }			
        return $model;
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }
    
    /**
     * @Author: ANH DUNG Oct 21, 2016
     * @Todo: ExportExcel
     */
    public function actionExportExcel(){
        try{
        if(isset($_SESSION['data-excel'])){
            //            ini_set('memory_limit','1000M'); chưa cần chạy đoạn này, đưa memory_limit lên 500 có thể export dc khoảng 5000 row - chưa test
            ini_set('memory_limit','500M');
            ExportList::UpholdSchedule();
        }
        $this->redirect(array('index'));
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }
    
}
