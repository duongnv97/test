<?php

class GasProfileController extends AdminController 
{
    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id)
    {
        $this->pageTitle = 'Xem ';
        try{
        $this->render('view',array(
                'model'=>$this->loadModel($id), 'actions' => $this->listActionsCanAccess,
        ));
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }

    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate()
    {
        set_time_limit(7200);
        $this->pageTitle = 'Tạo Mới ';
        try{
        $this->getHomeContractId();
        $model=new GasProfile('create');
        $model->mDetail = new GasProfileDetail();
        if(isset($_POST['GasProfile'])){
            $model->attributes=$_POST['GasProfile'];
            $model->mDetail->attributes = $_POST['GasProfileDetail']; 
            $model->validate();
            GasProfileDetail::validateFile($model, array('create'=>1));
            if(!$model->hasErrors()){
                $model->save();
                GasProfileDetail::saveFileScanDetail($model);
                GasScheduleEmail::BuildListNotifyPhapLy($model);
                Yii::app()->user->setFlash('successUpdate', "Thêm mới thành công.");
               $this->redirect(array('create'));
            }
        }

        $this->render('create',array(
                'model'=>$model, 'actions' => $this->listActionsCanAccess,
        ));
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id)
    {
        set_time_limit(7200);
        $this->pageTitle = 'Cập Nhật ';
        try
        {
        $model=$this->loadModel($id);
        $model->scenario = 'update';
        if(!GasCheck::AgentCanUpdateProfileScan($model) || !$model->canUpdate()){
            $this->redirect(array('index'));
        }

        $model->aModelDetail = $model->rProfileDetail;
        $model->mDetail = new GasProfileDetail();
        $model->date_expired = MyFormat::dateConverYmdToDmy($model->date_expired);
        $model->list_materials_id = explode(',', $model->list_materials_id);

        if(isset($_POST['GasProfile']))
        {            
            $model->attributes          = $_POST['GasProfile'];
            $model->mDetail->attributes = $_POST['GasProfileDetail'];
            $model->update_by           = MyFormat::getCurrentUid();
            $model->update_time         = date('Y-m-d H:i:s');
            $model->validate();
            GasTextFile::validateFile($model);
            if(!$model->hasErrors()){
                $model->save();
                if(is_array($model->mDetail->aIdNotIn) && count($model->mDetail->aIdNotIn)){
                    GasProfileDetail::deleteByNotInId($model->id, $model->mDetail->aIdNotIn);
                }else{
                    GasProfileDetail::delete_by_profile_id($model->id);
                }
                GasProfileDetail::saveFileScanDetail($model);
                GasProfileDetail::UpdateOrderNumberFile($model);
                GasScheduleEmail::BuildListNotifyPhapLy($model);
                Yii::app()->user->setFlash('successUpdate', "Cập nhật thành công.");
                $this->redirect(array('update','id'=>$model->id));
            }							
        }

        $this->render('update',array(
                'model'=>$model, 'actions' => $this->listActionsCanAccess,
        ));
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id)
    {
        try
        {
        if(Yii::app()->request->isPostRequest)
        {
                // we only allow deletion via POST request
                if($model = $this->loadModel($id))
                {
                    if($model->delete())
                        Yii::log("Uid: " .Yii::app()->user->id. " Delete record ".  print_r($model->attributes, true), 'info');
                }

                // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
                if(!isset($_GET['ajax']))
                        $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('index'));
        }
        else
        {
            Yii::log("Uid: " .Yii::app()->user->id. " Invalid request. Please do not repeat this request again.");
            throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
        }	
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }

    /**
     * Manages all models.
     */
    public function actionIndex()
    {
        $this->pageTitle = 'Danh Sách ';
        $this->allowIndex();
        GasAgentCustomer::initSessionAgent();// Dec 13, 2016
        try{
        $model=new GasProfile('search');
        $model->unsetAttributes();  // clear any default values
        $model->status = STATUS_ACTIVE;  // clear any default values
        if(isset($_GET['GasProfile']))
            $model->attributes=$_GET['GasProfile'];

        $this->render('index',array(
                'model'=>$model, 'actions' => $this->listActionsCanAccess,
        ));
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
    * If the data model is not found, an HTTP exception will be raised.
     * @param integer the ID of the model to be loaded
     */
    public function loadModel($id)
    {
        try
        {
        $model=GasProfile::model()->findByPk($id);
        if($model===null)
        {
            Yii::log("The requested page does not exist.");
            throw new CHttpException(404,'The requested page does not exist.');
        }			
        return $model;
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }
    
    /**
     * @Author: ANH DUNG Dec 03, 2014
     * @Todo: report agent profile, báo cáo đại lý đã có những loại hs pháp lý nào
     */
    public function actionAgent_report()
    {
        $this->pageTitle = 'Báo cáo hồ sơ pháp lý đại lý ';
        try{
            $model = new GasProfile();
            if(isset($_GET['GasProfile']))
                $model->attributes=$_GET['GasProfile'];
            
            $rData          = $model->report();
            $rModelAgent    = $model->reportGetModelAgent();
            $this->render('Agent_report',array(
                'rData'=> $rData, 'actions' => $this->listActionsCanAccess,
                'model'         => $model,
                'rModelAgent'   => $rModelAgent,
            ));
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }
    
    public function allowIndex(){
        $cUid = MyFormat::getCurrentUid();
        $cRole = MyFormat::getCurrentRoleId();
        $aUidAllow  = [
            GasConst::UID_DAT_LX, 
//            862531, // Trương Văn Lợi -- Close Mar619
        ];
        $aRoleAllow = [ROLE_SALE];
        if(in_array($cRole, $aRoleAllow) && !in_array($cUid, $aUidAllow)){
            $this->redirect(Yii::app()->createAbsoluteUrl(''));
        }
    }
    
    /** @Author: KhueNM SEP 14, 2019
     *  @Todo: get homcontract_id from agent_name , for ajax
     *  @Param:
     * */
    public function getHomeContractId() {
        if (!empty($_GET['getUrlContract'])) {
            $agent_id       = empty($_GET['agent_id']) ? '' : $_GET['agent_id'];
            $mGasProfile    = new GasProfile();
            echo $mGasProfile->getUrlCreateHomeContract($agent_id);
            die;
        }
    }
}
