<?php

class HrHolidayPlansController extends AdminController
{
    public $pluralTitle = "Kế hoạch nghỉ lễ";
    public $singleTitle = "Kế hoạch nghỉ lễ";
    
    public function actionIndex()
    {
        $this->pageTitle = 'Danh sách kế hoạch nghỉ lễ';
        try{
            $model=new HrHolidayPlans('search');
            if(isset($_GET['HrHolidayPlans'])) {
                $model->attributes = $_GET['HrHolidayPlans'];
            }
            $this->render('index',array(
                    'model'=>$model, 'actions' => $this->listActionsCanAccess,
            ));
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }
    public function actionView($id)
    {
        $this->pageTitle = 'Xem kế hoạch nghỉ lễ';
        try{
                $this->render('view',array(
                    'model'=>$this->loadModel($id), 'actions' => $this->listActionsCanAccess,
            ));
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }
    public function actionCreate()
    {
        $this->pageTitle = 'Tạo mới kế hoạch nghỉ lễ';
        try{
            $model=new HrHolidayPlans('create');
            if(isset($_POST['HrHolidayPlans'])){
                $model->attributes = $_POST['HrHolidayPlans'];
                $model->setAttributesBeforeSave();
                $model->validate();
                if(!$model->hasErrors()){
                    $model->save();
                    Yii::app()->user->setFlash( MyFormat::SUCCESS_UPDATE, "Thêm mới thành công." );
                    $this->redirect(array('create'));
                }
            }

            $this->render('create',array(
                    'model'=>$model, 'actions' => $this->listActionsCanAccess,
            ));
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }
    public function actionUpdate($id)
    {
        try{
        $model=$this->loadModel($id);
        $model->scenario = 'update';
        if(isset($_POST['HrHolidayPlans'])){
            $model->attributes=$_POST['HrHolidayPlans'];
            $model->status       = $model->STATUS_NEW;
            $model->setAttributesBeforeSave();
            $model->validate();
            if(!$model->hasErrors()){
                $model->update();
                Yii::app()->user->setFlash( MyFormat::SUCCESS_UPDATE, "Cập nhật thành công." );
            }
        }

        $this->render('update',array(
                'model'=>$model, 'actions' => $this->listActionsCanAccess,
        ));
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }
    public function actionDelete($id)
    {
        try{
        if(Yii::app()->request->isPostRequest)
        {
            // we only allow deletion via POST request
            if($model = $this->loadModel($id))
            {
                if($model->delete()){
                    Yii::log("Uid: " .Yii::app()->user->id. " Delete record ".  print_r($model->attributes, true), 'info');
                }
            }

            // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
            if(!isset($_GET['ajax']))
                $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('index'));
        }
        else
        {
            Yii::log("Uid: " .Yii::app()->user->id. " Invalid request. Please do not repeat this request again.");
            throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
        }
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }
    public function loadModel($id)
    {
        try{
            $model = HrHolidayPlans::model()->findByPk($id);
            if($model===null)
            {
                Yii::log("The requested page does not exist.");
                throw new CHttpException(404,'The requested page does not exist.');
            }			
            return $model;
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }
	
}