<?php

class UserFixAddressController extends AdminController
{
    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id)
    {
        try {
            $model = $this->loadModel($id);

            if (isset($_POST['UsersFixAddress'])) {
                $model->attributes = $_POST['UsersFixAddress'];
                if ($model->save())
                    $this->redirect(array('view', 'id' => $model->id));
            }

            $this->render('update', array(
                'model' => $model, 'actions' => $this->listActionsCanAccess,
            ));
        } catch (Exception $e) {
            GasCheck::CatchAllExeptiong($e);
        }
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id)
    {
        try {
            if (Yii::app()->request->isPostRequest) {
                if ($model = $this->loadModel($id)) {
                    /** @var UsersFixAddress $model */
                    $model->saveReject();
                }

                // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
                if (!isset($_GET['ajax']))
                    $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('index'));
            } else {
                Yii::log("Invalid request. Please do not repeat this request again.");
                throw new CHttpException(400, 'Invalid request. Please do not repeat this request again.');
            }
        } catch (Exception $e) {
            GasCheck::CatchAllExeptiong($e);
        }
    }

    /**
     * Manages all models.
     */
    public function actionIndex()
    {
        try {
            $this->pageTitle = 'Chỉnh sửa địa chỉ khách hàng';
            $model = new UsersFixAddress('search');
            $model->unsetAttributes();  // clear any default values
            if (isset($_GET['UsersFixAddress']))
                $model->attributes = $_GET['UsersFixAddress'];

            $this->render('index', array(
                'model' => $model, 'actions' => $this->listActionsCanAccess,
            ));
        } catch (Exception $e) {
            GasCheck::CatchAllExeptiong($e);
        }
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer the ID of the model to be loaded
     */
    public function loadModel($id)
    {
        try {
            $model = UsersFixAddress::model()->findByPk($id);
            if ($model === null) {
                Yii::log("The requested page does not exist.");
                throw new CHttpException(404, 'The requested page does not exist.');
            }
            return $model;
        } catch (Exception $e) {
            GasCheck::CatchAllExeptiong($e);
        }
    }

    /**
     * Performs the AJAX validation.
     * @param CModel the model to be validated
     */
    protected function performAjaxValidation($model)
    {
        try {
            if (isset($_POST['ajax']) && $_POST['ajax'] === 'user-fix-address-form') {
                echo CActiveForm::validate($model);
                Yii::app()->end();
            }
        } catch (Exception $e) {
            GasCheck::CatchAllExeptiong($e);
        }
    }

    public function actionConfirm($id)
    {
        try {
            if (Yii::app()->request->isPostRequest) {
                // we only allow deletion via POST request
                if ($model = $this->loadModel($id)) {
                    /** @var UsersFixAddress $model */
                    $model->saveConfirm();
                }

                // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
                if (!isset($_GET['ajax']))
                    $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('index'));
            } else {
                Yii::log("Invalid request. Please do not repeat this request again.");
                throw new CHttpException(400, 'Invalid request. Please do not repeat this request again.');
            }
        } catch (Exception $e) {
            GasCheck::CatchAllExeptiong($e);
        }
    }
}