<?php

class GasCustomerCheckController extends AdminController 
{
    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id)
    {
        $this->pageTitle = 'Xem ';
        try{
            $this->layout='ajax';
            $model = $this->loadModel($id);
            $cRole = Yii::app()->user->role_id;
            $cUid = Yii::app()->user->id;
            
            if( $cRole == ROLE_MONITOR_AGENT && 
                $cUid != $model->uid_login
            ){
//                die('<script type="text/javascript">parent.$.fn.colorbox.close();</script>'); 
            }
            
            $this->render('view',array(
                    'model'=>$model, 'actions' => $this->listActionsCanAccess,
            ));
        }
        catch (Exception $exc)
        {
            GasCheck::CatchAllExeptiong($exc);
        }
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate()
    {
        $this->pageTitle = 'Tạo Mới ';
        try
        {
            $model=new GasCustomerCheck('create');
            
            if(isset($_POST['GasCustomerCheck']))
            {
                $model->attributes=$_POST['GasCustomerCheck'];
                $model->file_report  = CUploadedFile::getInstance($model,'file_report');
                $model->validate();
                if(!$model->hasErrors()){
                    GasCustomerCheck::validateFile($model);
                }
                if(!$model->hasErrors()){
                    $model->save();
                    GasCustomerCheck::saveFile($model, 'file_report');
                    Yii::app()->user->setFlash( MyFormat::SUCCESS_UPDATE, "Thêm mới thành công." );
                   $this->redirect(array('create'));
                }				
            }

            $this->render('create',array(
                    'model'=>$model, 'actions' => $this->listActionsCanAccess,
            ));
        }
        catch (Exception $exc)
        {
            GasCheck::CatchAllExeptiong($exc);
        }
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id)
    {
        $this->pageTitle = 'KH cần kiểm tra - Cập Nhật ';
        try
        {
            $model=$this->loadModel($id);
            $this->ResetScenario($model);
            if(!GasCheck::CanUpdateCustomerCheck($model)){
                $this->redirect(array('index'));
            }
            $OldFile = $model->file_report;
            $model->OldFile = $model->file_report;
            $model->report_date = MyFormat::dateConverYmdToDmy($model->report_date);
            if(isset($_POST['GasCustomerCheck']))
            {
                $model->attributes=$_POST['GasCustomerCheck'];
                $model->file_report  = CUploadedFile::getInstance($model,'file_report');
                $model->validate();
                if(!$model->hasErrors()){
                    GasCustomerCheck::validateFile($model);
                }
                if(!$model->hasErrors()){
                    $model->HandleSaveUpdate();
                    Yii::app()->user->setFlash( MyFormat::SUCCESS_UPDATE, "Cập nhật thành công." );
                    $this->redirect(array('update','id'=>$model->id));
                }							
            }
            $attUpdate = array('qty');
//            MyFormat::FormatNumberDecimal($model, $attUpdate);

            $this->render('update',array(
                    'model'=>$model, 'actions' => $this->listActionsCanAccess,
            ));
        }
        catch (Exception $exc)
        {
            GasCheck::CatchAllExeptiong($exc);
        }
    }
    
    /**
     * @Author: ANH DUNG Jun 09, 2015
     * @Todo: reset scenario by role
     * @Param: $model
     */
    public function ResetScenario($model) {
        $model->scenario = 'update_accounting';
        $cRole = Yii::app()->user->role_id;
        if($cRole == ROLE_MONITOR_AGENT){
            $model->scenario = 'update_monitor';
        }
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id)
    {
            try
            {
            if(Yii::app()->request->isPostRequest)
            {
                    // we only allow deletion via POST request
                    if($model = $this->loadModel($id))
                    {
                        if($model->delete())
                            Yii::log("Uid: " .Yii::app()->user->id. " Delete record ".  print_r($model->attributes, true), 'info');
                    }

                    // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
                    if(!isset($_GET['ajax']))
                            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('index'));
            }
            else
            {
                Yii::log("Uid: " .Yii::app()->user->id. " Invalid request. Please do not repeat this request again.");
                throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
            }	
            }
            catch (Exception $exc)
            {
                GasCheck::CatchAllExeptiong($exc);
            }
    }

    /**
     * Manages all models.
     */
    public function actionIndex()
    {
            $this->pageTitle = 'Danh Sách ';
            try
            {
            $model=new GasCustomerCheck('search');
            $model->unsetAttributes();  // clear any default values
            if(isset($_GET['GasCustomerCheck']))
                $model->attributes=$_GET['GasCustomerCheck'];

            $this->render('index',array(
                    'model'=>$model, 'actions' => $this->listActionsCanAccess,
            ));
            }
            catch (Exception $exc)
            {
                GasCheck::CatchAllExeptiong($exc);
            }
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
    * If the data model is not found, an HTTP exception will be raised.
     * @param integer the ID of the model to be loaded
     */
    public function loadModel($id)
    {
        try
        {
        $model=GasCustomerCheck::model()->findByPk($id);
        if($model===null)
        {
            Yii::log("The requested page does not exist.");
            throw new CHttpException(404,'The requested page does not exist.');
        }			
        return $model;
        }
        catch (Exception $exc)
        {
            GasCheck::CatchAllExeptiong($exc);
        }
    }

    /**
     * Performs the AJAX validation.
     * @param CModel the model to be validated
     */
    protected function performAjaxValidation($model)
    {
            try
            {
            if(isset($_POST['ajax']) && $_POST['ajax']==='gas-customer-check-form')
            {
                    echo CActiveForm::validate($model);
                    Yii::app()->end();
            }
            }
            catch (Exception $exc)
            {
                GasCheck::CatchAllExeptiong($exc);
            }
    }
    
    /**
     * @Author: ANH DUNG Nov 23, 2014
     * @Todo: something
     * @Param: $model
     */
    public function actionUpdate_customer($id) {
        try
        {
            $this->layout='ajax';
            $model=$this->loadModel($id);
            $model->scenario = 'Update_customer';
            if(!GasCheck::CanUpdateCustomerCheckReport($model)){
                die('<script type="text/javascript">parent.$.fn.colorbox.close();</script>'); 
            }            
            if(isset($_POST['GasCustomerCheck']))
            {
                $attUpdate = array(
                    'vo_inventory_50','vo_inventory_45','vo_inventory_12',
                    'vo_da_thu_50','vo_da_thu_45','vo_da_thu_12',
                    'vo_de_xuat_thu_50','vo_de_xuat_thu_45','vo_de_xuat_thu_12',
                    'report_handle','note_monitor_agent',
                    );
                $model->attributes=$_POST['GasCustomerCheck'];
                $model->validate();
                if(!$model->hasErrors()){
                    GasCustomerCheck::validateFile($model);
                }
                if(!$model->hasErrors()){
                    $model->update();
                    GasCustomerCheck::saveFile($model, 'file_report');
                    Yii::app()->user->setFlash( MyFormat::SUCCESS_UPDATE, "Cập nhật thành công." );
                    $this->redirect(array('Update_customer','id'=>$model->id));
                }
            }
            
            $attUpdate = array(
                    'vo_inventory_50','vo_inventory_45','vo_inventory_12',
                    'vo_da_thu_50','vo_da_thu_45','vo_da_thu_12',
                    'vo_de_xuat_thu_50','vo_de_xuat_thu_45','vo_de_xuat_thu_12',                    
                    );
            MyFormat::FormatNumberDecimal($model, $attUpdate);            
            $this->render('Update_customer',array(
                    'model'=>$model, 'actions' => $this->listActionsCanAccess,
            ));
            
        }
        catch (Exception $exc)
        {
            GasCheck::CatchAllExeptiong($exc);
        }
    }
}
