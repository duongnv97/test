<?php

class GasTextController extends AdminController 
{
    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id)
    {
        $this->pageTitle = 'Xem ';
        try{
            $this->redirect(array('text_view', 'id'=>$id ));
            $this->render('view',array(
                    'model'=>$this->loadModel($id), 'actions' => $this->listActionsCanAccess,
            ));
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate()
    {
        $this->pageTitle = 'Tạo Mới ';
        try
        {
        $model=new GasText('create');
        $model->mDetail = new GasTextFile();
        $this->ajaxTemplate();

        if(isset($_POST['GasText']))
        {
            $model->attributes=$_POST['GasText'];
            $model->mDetail->attributes = $_POST['GasTextFile']; 
            $model->validate();
            $aNotRequireFile = $model->getArrayTypeNeedApprove();
            if(!in_array($model->type, $aNotRequireFile)){
                GasTextFile::validateFile($model, array('create'=>1));
            }
            if(!$model->hasErrors()){
                $model->handleJsonSave();
                $model->save();
                GasTextFile::saveFileScanDetail($model);
                // Now 21, 2014 add to list send mail to all user
                $model=$this->loadModel($model->id);
                $year = MyFormat::GetYearByDate($model->date_published);
                if($year == date('Y')){// chỉ gửi mail cho văn bản trong năm hiện tại
                    GasScheduleEmail::BuildListTextAlert($model->id);
                    // Mar 14, 2015 tạm đóng lại vì nó gửi quá nhiều, sẽ mở lại sau
                }
//                if($model->handleSaveTPR()){
//                    Yii::app()->user->setFlash('successUpdate', "Thêm mới thành công.");
//                }
                $model->handleSaveGasDebts();
                Yii::app()->user->setFlash(MyFormat::SUCCESS_UPDATE, "Thêm mới thành công.");
                $this->redirect(array('create'));
            }
        }

        $this->render('create',array(
                'model'=>$model, 'actions' => $this->listActionsCanAccess,
        ));
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }

    public function actionUpdate($id)
    {
        $this->pageTitle = 'Cập Nhật ';
        try
        {
        $model=$this->loadModel($id);
        $model->scenario = 'update';
        if(!$model->canUpdate()){
            $this->redirect(array('index'));
        }

        $model->aModelDetail = $model->rTextFile;
        $model->mDetail = new GasTextFile();
        $model->date_published = MyFormat::dateConverYmdToDmy($model->date_published);
        $model->date_expired = MyFormat::dateConverYmdToDmy($model->date_expired);

        if(isset($_POST['GasText']))
        {
            $model->attributes=$_POST['GasText'];
            $model->mDetail->attributes = $_POST['GasTextFile'];
            $model->validate();
            //GasTextFile::validateFile($model);
            $aNotRequireFile = $model->getArrayTypeNeedApprove();
            if(!in_array($model->type, $aNotRequireFile)){
                GasTextFile::validateFile($model);
            }
            if(!$model->hasErrors()){
                $model->handleJsonSave();
                $model->save();
                if(is_array($model->mDetail->aIdNotIn) && count($model->mDetail->aIdNotIn)){
                    GasTextFile::deleteByNotInId($model->id, $model->mDetail->aIdNotIn);
                }else{
                    GasTextFile::deleteByTextId($model->id);
                }
                GasTextFile::saveFileScanDetail($model);
                GasTextFile::UpdateOrderNumberFile($model);
                // Only OPEN FOR TEST - Now 21, 2014 add to list send mail to all user
//                GasScheduleEmail::BuildListTextAlert($model->id); 
//                if($model->handleSaveTPR()){
//                    Yii::app()->user->setFlash('successUpdate', "Cập nhật thành công.");
//                }
                $model->handleSaveGasDebts();
                Yii::app()->user->setFlash(MyFormat::SUCCESS_UPDATE, "Cập nhật thành công.");
                $this->redirect(array('update','id'=>$model->id));
            }
        }

        $this->render('update',array(
                'model'=>$model, 'actions' => $this->listActionsCanAccess,
        ));
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id)
    {
        try
        {
        if(Yii::app()->request->isPostRequest)
        {
                // we only allow deletion via POST request
                if($model = $this->loadModel($id))
                {
                    if($model->delete())
                        Yii::log("Uid: " .Yii::app()->user->id. " Delete record ".  print_r($model->attributes, true), 'info');
                }

                // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
                if(!isset($_GET['ajax']))
                        $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('index'));
        }
        else
        {
            Yii::log("Uid: " .Yii::app()->user->id. " Invalid request. Please do not repeat this request again.");
            throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
        }	
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }

    /**
     * Manages all models.
     */
    public function actionIndex()
    {
        $this->pageTitle = 'Văn bản';
        try{
            $model=new GasText('search');
            $model->unsetAttributes();  // clear any default values
            if(isset($_GET['GasText'])){
                $model->attributes  = $_GET['GasText'];
                $model->is_view_all = $_GET['GasText']['is_view_all'];
            }
            $view = 'index';
            if(isset($_GET['role_id'])){
                $view = 'index_'.$_GET['role_id'];
                $model->role_id     = $_GET['role_id'];
                $this->pageTitle    = 'Văn bản '.$model->getRole();
            }

            $this->render($view, array(
                    'model'=>$model, 'actions' => $this->listActionsCanAccess,
            ));
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
    * If the data model is not found, an HTTP exception will be raised.
     * @param integer the ID of the model to be loaded
     */
    public function loadModel($id)
    {
        try
        {
        $model=GasText::model()->findByPk($id);
        if($model===null)
        {
            Yii::log("The requested page does not exist.");
            throw new CHttpException(404,'The requested page does not exist.');
        }			
        return $model;
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }

    /**
     * Performs the AJAX validation.
     * @param CModel the model to be validated
     */
    protected function performAjaxValidation($model)
    {
        try
        {
        if(isset($_POST['ajax']) && $_POST['ajax']==='gas-text-form')
        {
                echo CActiveForm::validate($model);
                Yii::app()->end();
        }
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }
    
    
    /**
     * Handle the ajax request. This process changes the status of member to 1 (mean active)
     * @param type $id the id of member need changed status to 1
     */
    public function actionAjaxActivate($id) {
        if(Yii::app()->request->isPostRequest)
        {
            $model = $this->loadModel($id);
            if(method_exists($model, 'activate'))
            {
                $model->activate();
            }
            Yii::app()->end();
        }
        else
        {
            Yii::log('Invalid request. Please do not repeat this request again.');
            throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
        }
            
    }

    /**
     * Handle the ajax request. This process changes the status of member to 0 (mean deactive)
     * @param type $id the id of member need changed status to 0
     */
    public function actionAjaxDeactivate($id) {
        if(Yii::app()->request->isPostRequest)
        {
            $model = $this->loadModel($id);
            if(method_exists($model, 'deactivate'))
            {
                $model->deactivate();
            }
            Yii::app()->end();
        }
        else
        {
            Yii::log('The requested page does not exist.');
            throw new CHttpException(404,'The requested page does not exist.');
        }            
    }
    
    public function actionText_view()
    {
        try{
            $mTextView = new GasText('search');
            $mComment = new GasTextComment();
            if(isset($_GET['id'])){
                $mTextView = $this->loadModel( $_GET['id'] );
                $mComment->text_id = $mTextView->id;
            }
            $this->pageTitle = $mTextView->short_content;
            
            $model=new GasText('search');
            $model->unsetAttributes();  // clear any default values
            if(isset($_GET['GasText']))
                $model->attributes=$_GET['GasText'];
            $model->status = STATUS_ACTIVE;
            
            $this->render('Text_view/Text_view',array(
                'model'=>$model,
                'mTextView'=>$mTextView,
                'mComment'=>$mComment,
                'actions' => $this->listActionsCanAccess,
            ));
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }    
    
    public function actionText_post_comment($id)
    {
        $this->pageTitle = 'Post Comment Văn Bản ';
        try{
            $model = $this->loadModel($id);
            $msg = "Có Lỗi không thể Gửi Bình Luận ( invalid message ).";
            $FlashScenario = 'ErrorUpdate';            
            if(isset($_POST['GasText']) && $model->status == STATUS_ACTIVE && !$model->stop_comment)
            {
                $model->attributes=$_POST['GasText'];
                $model->scenario = 'post_comment';
                $model->validate();
                if(!$model->hasErrors()){
                   $msg = "Gửi bình luận thành công.";
                   $FlashScenario = 'successUpdate';
                   if(!GasTickets::UserCanPostTicket()) { // dùng chung kiểm tra với ticket (ngại tách, mà thấy cũng ko thể post vượt 200 message 1 ngày dc )
                       $msg = "Có Lỗi không thể Gửi Bình Luận vượt quá giới hạn cho phép (exceed limit), liên hệ với admin quản trị.";
                       $FlashScenario = 'ErrorUpdate';
                   }else{
                       GasTextComment::SaveOneMessageDetail($model);
                   }
                }
            }
            
            Yii::app()->user->setFlash($FlashScenario, $msg);
            $this->redirect(array('text_view', 'id'=>$id));
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }    
    
    public function actionText_delete_comment($id)
    {
        try{
            $model = MyFormat::loadModelByClass($id, 'GasTextComment');
            $text_id = '';
            $msg = "Có Lỗi không thể Xóa Bình Luận.";
            $FlashScenario = 'ErrorUpdate';
            if($model){
                $text_id = $model->text_id;
                $model->delete();
                $msg = "Xóa Bình Luận Thành Công.";
                $FlashScenario = 'successUpdate';
            }
            Yii::app()->user->setFlash($FlashScenario, $msg);
            $this->redirect(array('text_view', 'id'=>$text_id));
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }    
    
    /** @Author: DuongNV Nov 23, 2018
     *  @Todo: Xem loại hợp đồng render bằng text (not scan)
     **/
    public function actionViewWaitApprove($id)
    {
        $this->pageTitle = 'Xem ';
        try{
            $this->render('view_wait_approve',array(
                    'model'=>$this->loadModel($id), 'actions' => $this->listActionsCanAccess,
            ));
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }
    
    /** @Author: DuongNV Nov 23, 2018
     *  @Todo: Duyệt văn bản
     **/
    public function actionApprove($id)
    {
        $this->pageTitle = 'Duyệt ';
        $this->layout = 'ajax';
        try{
            $model = $this->loadModel($id);
            if($model->canApprove()){
                if(isset($_POST['GasText'])){
                    $model->attributes = $_POST['GasText'];
                    if($model->status == GasText::STT_APPROVED){
                        $model->approved_by   = MyFormat::getCurrentUid();
                        $model->approved_date = date('Y-m-d');
                    }
                    if(!$model->hasErrors()){
                        $model->save();
                        die('<script type="text/javascript">parent.$.fn.colorbox.close();</script>');
                    }
                }
                $this->render('approve',array(
                    'model'=>$model, 'actions' => $this->listActionsCanAccess,
                ));
            }
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }
    
    /** @Author: DuongNV Nov 23, 2018
     *  @Todo: load ajax when type change on create, update
     **/
    public function ajaxTemplate() {
        if(isset($_GET['load_sign'])){
            $company = $_GET['load_sign'];
            $aImage  =  GasText::model()->getArraySignImage();
            echo isset($aImage[$company]) ? $aImage[$company] : '';
            die;
        }
        if(isset($_GET['ajax_template'])){
            $type = $_GET['ajax_template'];
            $idTemplate = GasText::model()->getArrayIdTemplate()[$type];
            $mTemplate = EmailTemplates::model()->findByPk($idTemplate);
            if(empty($mTemplate)) {return '';}
            $html = $mTemplate->email_body;
            $isUpdate  = false;
            $mGasText = new GasText();
            if(!empty($_GET['id'])){
                $mGasText = GasText::model()->findByPk($_GET['id']);
                if($mGasText->type == $type){
                    $isUpdate = true;
                }
            }
            if($isUpdate){
                $html = $mGasText->template;
            }
            $mGasText->handleTemplate($html);
            echo $html;die;
        }
    }
    
}
