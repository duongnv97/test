<?php

class GasFileScanController extends AdminController 
{
    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id)
    {
        $this->pageTitle = 'Xem ';
        try{
                $model = $this->loadModel($id);
                if(Yii::app()->user->id==ROLE_SUB_USER_AGENT && Yii::app()->user->id != $model->uid_login){
                    $this->redirect(array('index'));
                }
                
                $this->render('view',array(
                        'model'=>$model, 'actions' => $this->listActionsCanAccess,
                ));
            }
            catch (Exception $exc)
            {
                Yii::log("Uid: " .Yii::app()->user->id. " Exception ".  $exc->getMessage(), 'error');
                $code = 404;
                if(isset($exc->statusCode))
                    $code=$exc->statusCode;
                if($exc->getCode())
                    $code=$exc->getCode();
                throw new CHttpException($code, $exc->getMessage());
            }

    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate()
    {
//        GasCheck::FunctionMaintenance();
        $this->pageTitle = 'Tạo Mới ';
        try
        {
        $model=new GasFileScan('create');
        $model->mDetail = new GasFileScanDetail();
        $model->aModelFileScanInfo[] = new GasFileScanInfo();            
        $model->agent_id = MyFormat::getAgentId();

        if(isset($_POST['GasFileScan']))
        {
            $model->attributes = $_POST['GasFileScan'];
            $model->mDetail->attributes = $_POST['GasFileScanDetail'];                
            $model->validate();
            GasFileScanDetail::validateFile($model, array('create'=>1));
            if(!$model->hasErrors()){
                $model->save();
                GasFileScanDetail::saveFileScanDetail($model);// save image
                GasFileScanInfo::saveDetail($model);// save record from excel
                GasFileScan::SavePTTTFromExcel($model);

                Yii::app()->user->setFlash('successUpdate', "Thêm mới thành công.");
                $this->redirect(array('create','view_id'=>$model->id));
            }				
        }

        $this->render('create',array(
                'model'=>$model, 'actions' => $this->listActionsCanAccess,
        ));
        }
        catch (Exception $exc)
        {
            Yii::log("Uid: " .Yii::app()->user->id. " Exception ".  $exc->getMessage(), 'error');
            $code = 404;
            if(isset($exc->statusCode))
                $code=$exc->statusCode;
            if($exc->getCode())
                $code=$exc->getCode();
            throw new CHttpException($code, $exc->getMessage());
        }
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id)
    {
//        GasCheck::FunctionMaintenance();
        $this->pageTitle = 'Cập Nhật ';
        try
        {
        $model=$this->loadModel($id);
        $model->scenario = 'update';
        if(!GasCheck::AgentCanUpdateFileScan($model)){
            $this->redirect(array('index'));
        }
        $model->aModelDetail = $model->rFileScanDetail;
        $model->mDetail = new GasFileScanDetail();
        $model->maintain_date = MyFormat::dateConverYmdToDmy($model->maintain_date);
        $model->aModelFileScanInfo = count($model->rDetailInfo)?$model->rDetailInfo: array( (new GasFileScanInfo()) );

        if(isset($_POST['GasFileScan']))
        {
            $model->attributes=$_POST['GasFileScan'];
            $model->mDetail->attributes = $_POST['GasFileScanDetail'];
            $model->validate();
            GasFileScanDetail::validateFile($model);
            if(!$model->hasErrors()){
                $model->save();
                if(GasFileScan::CanRemoveFile($model)){
                    if(is_array($model->mDetail->aIdNotIn) && count($model->mDetail->aIdNotIn)){
                        GasFileScanDetail::deleteByNotInId($model->id, $model->mDetail->aIdNotIn);
                    }else{
                        GasFileScanDetail::deleteByFileScanId($model->id);
                    }
                }

                GasFileScanDetail::saveFileScanDetail($model); // save image
                GasFileScanInfo::saveDetail($model); // save record from excel
                GasFileScan::SavePTTTFromExcel($model);
                Yii::app()->user->setFlash('successUpdate', "Cập nhật thành công.");
                $this->redirect(array('update','id'=>$model->id));
            }							
        }

        $this->render('update',array(
                'model'=>$model, 'actions' => $this->listActionsCanAccess,
        ));
        }
        catch (Exception $exc)
        {
            Yii::log("Uid: " .Yii::app()->user->id. " Exception ".  $exc->getMessage(), 'error');
            $code = 404;
            if(isset($exc->statusCode))
                $code=$exc->statusCode;
            if($exc->getCode())
                $code=$exc->getCode();
            throw new CHttpException($code, $exc->getMessage());
        }
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id)
    {
            try
            {
            if(Yii::app()->request->isPostRequest)
            {
                    // we only allow deletion via POST request
                    if($model = $this->loadModel($id))
                    {
                        if($model->delete())
                            Yii::log("Uid: " .Yii::app()->user->id. " Delete record ".  print_r($model->attributes, true), 'info');
                    }

                    // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
                    if(!isset($_GET['ajax']))
                            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('index'));
            }
            else
            {
                Yii::log("Uid: " .Yii::app()->user->id. " Invalid request. Please do not repeat this request again.");
                throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
            }	
            }
            catch (Exception $exc)
            {
                Yii::log("Uid: " .Yii::app()->user->id. " Exception ".  $exc->getMessage(), 'error');
                $code = 404;
                if(isset($exc->statusCode))
                    $code=$exc->statusCode;
                if($exc->getCode())
                    $code=$exc->getCode();
                throw new CHttpException($code, $exc->getMessage());
            }
    }

    /**
     * Manages all models.
     */
    public function actionIndex()
    {
            $this->pageTitle = 'Danh Sách ';
            try
            {
            $model=new GasFileScan('search');
            $model->unsetAttributes();  // clear any default values
            if(isset($_GET['GasFileScan']))
                    $model->attributes=$_GET['GasFileScan'];

            $this->render('index',array(
                    'model'=>$model, 'actions' => $this->listActionsCanAccess,
            ));
            }
            catch (Exception $exc)
            {
                Yii::log("Uid: " .Yii::app()->user->id. " Exception ".  $exc->getMessage(), 'error');
                $code = 404;
                if(isset($exc->statusCode))
                    $code=$exc->statusCode;
                if($exc->getCode())
                    $code=$exc->getCode();
                throw new CHttpException($code, $exc->getMessage());
            }
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
    * If the data model is not found, an HTTP exception will be raised.
     * @param integer the ID of the model to be loaded
     */
    public function loadModel($id)
    {
            try
            {
            $model=GasFileScan::model()->findByPk($id);
            if($model===null)
            {
                Yii::log("The requested page does not exist.");
                throw new CHttpException(404,'The requested page does not exist.');
            }			
            return $model;
            }
            catch (Exception $exc)
            {
                Yii::log("Uid: " .Yii::app()->user->id. " Exception ".  $exc->getMessage(), 'error');
                $code = 404;
                if(isset($exc->statusCode))
                    $code=$exc->statusCode;
                if($exc->getCode())
                    $code=$exc->getCode();
                throw new CHttpException($code, $exc->getMessage());
            }
    }

        /**
     * @Author: ANH DUNG Mar 25, 2017
     * @Todo: ExportExcel
     */
    public function actionExportExcel(){
        try{
        if(isset($_SESSION['data-excel'])){
//            ini_set('memory_limit','1000M'); chưa cần chạy đoạn này, đưa memory_limit lên 500 có thể export dc khoảng 5000 row - chưa test
            ini_set('memory_limit','500M');
            ExportList::PtttDetail();
        }
        $this->redirect(array('index'));
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }
}
