<?php

class GasEventMarketController extends AdminController 
{
    public $pluralTitle = 'Event chợ, siêu thị, roadshow';
    public $singleTitle = 'Event chợ, siêu thị, roadshow';
    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id)
    {
       
        $this->pageTitle = 'Xem ';
        $this->layout='ajax';
        try{
        $this->render('view',array(
                'model'=>$this->loadModel($id), 'actions' => $this->listActionsCanAccess,
        ));
        }
        catch (Exception $exc)
        {
            GasCheck::CatchAllExeptiong($exc);
        }
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    
     public function actionCreate()
    {
        $this->pageTitle = 'Tạo Mới ';
        try{
            $model = new GasEventMarket('create');
            $model->initCreateEventMarket($model);
            if(isset($_POST['GasEventMarket'])){
                $model->attributes = $_POST['GasEventMarket'];
                $model->attributes =  $model->getDataValidate();
                $model->validate();
                $model->wedGetPost();
                if(!$model->hasErrors()){
                    $model->save();
                    Yii::app()->user->setFlash('successUpdate', 'Thêm mới thành công.');
                    $this->redirect(array('create'));
                }else{
                    $model->formatCanSave();
                    $model->buildArrayEmployeeUpdate();
                }
            }
            $this->render('create',array(
                    'model'=>$model, 'actions' => $this->listActionsCanAccess,
            ));
        }
        catch (Exception $exc)
        {
            GasCheck::CatchAllExeptiong($exc);
        }
    }
    
    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id)
    {
        $this->pageTitle = 'Cập Nhật Ho tro Khach Hang';
        try
        {
        $model=$this->loadModel($id);
        $model->formatDataBeforeUpdate();
        $model->scenario = 'update';
        $model->buildArrayEmployeeUpdate();
        if(isset($_POST['GasEventMarket']))
        {
            $model->attributes=$_POST['GasEventMarket'];
            $model->attributes =  $model->getDataValidate();
            $model->validate();
            $model->wedGetPost();
            if(!$model->hasErrors()){
                $model->update();
                $model->deleteRoadShowDetail();
                Yii::app()->user->setFlash( MyFormat::SUCCESS_UPDATE, 'Cập nhật thành công.' );
                $this->redirect(array('update','id'=>$model->id));
            }else{
                    $model->formatCanSave();
                }							
        }
        $this->render('update',array(
                'model'=>$model, 'actions' => $this->listActionsCanAccess,
        ));
        }
        catch (Exception $exc)
        {
            GasCheck::CatchAllExeptiong($exc);
        }
    }
   
    public function actionIndex()
    {
//        roadShowRow
        $this->pageTitle = 'DANH SÁCH QUẢN LÍ CÁC ĐỢT ĐI CHỢ CỦA NHÂN VIÊN';
        try
        {
        if(!empty($_POST['roadShowRow'])){
            $this->exportExcelReport($_POST['roadShowRow']);
        }
        $model= new GasEventMarket('search');
        $model->unsetAttributes();  // clear any default values
        $model->start_date_from = date('d-m-Y');
        $model->start_date_to = date('d-m-Y');
        if(isset($_GET['GasEventMarket']))
                $model->attributes=$_GET['GasEventMarket'];

        $this->render('index',array(
                'model'=>$model, 'actions' => $this->listActionsCanAccess,
        ));
        }
        catch (Exception $exc)
        {
            GasCheck::CatchAllExeptiong($exc);
        }
    }
    
    public function actionDelete($id)
    {
            try
            {
            if(Yii::app()->request->isPostRequest)
            {
                if($model = $this->loadModel($id))
                {
                    if($model->delete()){
                        $model->deleteRoadShowDetail();
                        Yii::log("Uid: " .Yii::app()->user->id. " Delete record ".  print_r($model->attributes, true), 'info');
                    }
                }
                if(!isset($_GET['ajax']))
                    $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('index'));
            }
            else
            {
                Yii::log("Uid: " .Yii::app()->user->id. " Invalid request. Please do not repeat this request again.");
                throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
            }	
            }
            catch (Exception $exc)
            {
                GasCheck::CatchAllExeptiong($exc);
            }
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
    * If the data model is not found, an HTTP exception will be raised.
     * @param integer the ID of the model to be loaded
     */
    public function loadModel($id)
    {
        try
        {
        $model = GasEventMarket::model()->findByPk($id);
        if($model==null)
        {
            Yii::log("The requested page does not exist.");
            throw new CHttpException(404,'The requested page does not exist.');
        }			
        return $model;
        }
        catch (Exception $exc)
        {
            GasCheck::CatchAllExeptiong($exc);
        }
    }

    public function actionReportMarket()
    {
        $this->pageTitle = 'BÁO CÁO ĐỢT ĐI CHỢ';
        
        try
        {
            $model= new GasEventMarket();
            $aData = array();
            $model->unsetAttributes(); 
            

            if(isset($_GET['GasEventMarket']))
                $model->attributes=$_GET['GasEventMarket'];

            $aData = $model->getReportMarket(); 

            $this->render('reportMarket/index',array(
                    'model'=>$model, 
                    'data'=>$aData,
                    'actions' => $this->listActionsCanAccess,
            ));
        }
        catch (Exception $exc)
        {
            GasCheck::CatchAllExeptiong($exc);
        }
    }
    
    public function actionViewRoadShow($id)
    {
        $this->pageTitle = 'Xem chi tiết';
        $model = $this->loadModel($id);
        $this->layout='ajax';
        try{
            $this->exportExcelReport($id);
            $modelDetail = new GasEventMarketDetail();
            if(isset($_POST['GasEventMarket'])){
                $model->attributes = $_POST['GasEventMarket'];
                $model->setStatusEmployee();
                Yii::app()->user->setFlash( MyFormat::SUCCESS_UPDATE, "Cập nhật thành công." );
            }
            if (isset($_POST['btn_submit']) || isset($_POST['event_market_id']) || isset($_POST['employee_id']) || isset($_POST['status'])) {
                $event_market_id = $_POST['event_market_id'];
                $employee_id = $_POST['employee_id'];
                $status = $_POST['status'];
                if ($modelDetail->setStatusRoadShowEmployee($event_market_id, $employee_id, $status)) {
                    Yii::app()->user->setFlash( MyFormat::SUCCESS_UPDATE, "Cập nhật thành công." );
                    $url = Yii::app()->createAbsoluteUrl("admin/gasEventMarket/viewRoadShow", array('id'=>$id));
                    $this->redirect($url);
                } else {
                    Yii::app()->user->setFlash( MyFormat::ERROR_UPDATE, "Cập nhật trạng thái thất bại." );
                }
            }

            $this->render('roadShow/view_detail',array(
                'model'     => $model,
                'data'      => $model->viewDetailRoadShow($id),
                'id'        => $id,
                'actions'   => $this->listActionsCanAccess,
        ));
        }
        catch (Exception $exc)
        {
            GasCheck::CatchAllExeptiong($exc);
        }        
    }
    /** @Author: LocNV Aug 16, 2019
     *  @Todo: export excel employee run road show
     *  @Param: $event_market_id event market id
     **/
    public function exportExcelReport($event_market_id){
        if(isset($_GET['ToExcel'])){
            try{
                ToExcel::summaryRoadShowEmployee($event_market_id);
            }catch (Exception $exc){
                GasCheck::CatchAllExeptiong($exc);
            }
        }
    }
    
    public function actionCreateDetailMaterial($id){
        $this->pageTitle = 'Vật tư';
        $model = $this->loadModel($id);
        $this->layout='ajax';
        try{
            $modelEventMarketDetail = new GasEventMarketDetail();
            $modelEventMarketDetail->event_market_id = $id;
            $modelEventMarketDetail->type = isset($_GET['type_search']) && $_GET['type_search'] == GasEventMarketDetail::TYPE_MATERIAL_IMPORT ? GasEventMarketDetail::TYPE_MATERIAL_IMPORT : GasEventMarketDetail::TYPE_MATERIAL_EXPORT;
            if(isset($_POST['GasEventMarketDetail'])){
                $modelEventMarketDetail->attributes = $_POST['GasEventMarketDetail'];
                $modelEventMarketDetail->saveEventMarketDetail();
                $modelEventMarketDetail->setDefaultAttributes();
                Yii::app()->user->setFlash( MyFormat::SUCCESS_UPDATE, "Cập nhật thành công." );
            }
            $this->render('eventDetail/_form_create_detail',array(
                'model'     => $model,
                'actions'   => $this->listActionsCanAccess,
                'mDetail'  => $modelEventMarketDetail,
            ));
        }
        catch (Exception $exc)
        {
            GasCheck::CatchAllExeptiong($exc);
        }   
    }
    
    public function actionPayMoney($id){
        try{
            if(Yii::app()->request->isPostRequest){
                // we only allow deletion via POST request
                if($model = $this->loadModel($id))
                {
                    if(!empty($model)){
                        $model->updateStatus(GasEventMarket::STATUS_PAID);
                    }else{
                        throw new Exception('Invalid request member id: '.$model->id);
                    }
                }

                // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
                if(!isset($_GET['ajax']))
                        $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('index'));
            }
            else
            {
                Yii::log("Invalid request. Please do not repeat this request again.");
                throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
            }
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }
    
    public function actionReloadEvent($id){
        try{
            if(Yii::app()->request->isPostRequest){
                // we only allow deletion via POST request
                if($model = $this->loadModel($id))
                {
                    if(!empty($model)){
                        $model->reloadInstallApp();
                    }else{
                        throw new Exception('Invalid request member id: '.$model->id);
                    }
                }

                // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
                if(!isset($_GET['ajax']))
                        $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('index'));
            }
            else
            {
                Yii::log("Invalid request. Please do not repeat this request again.");
                throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
            }
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }
    
    /** @Author: NhanDT Sep 19,2019
     *  @Todo: todo
     *  @Param: param
     **/
    public function actionReportCostEvent() {
        $this->pageTitle = 'BC Activation';
        try
        {
            $model= new GasEventMarket();
            $model->unsetAttributes(); 
            $model->start_date_from = date('01-m-Y');
            $model->start_date_to   = date('d-m-Y');
            if(isset($_GET['GasEventMarket'])){
                $model->attributes=$_GET['GasEventMarket'];
                $_SESSION['data-event'] = $model;
            }
            $this->reportCostEventExcel();
            $this->render('reportCostEvent/index',array(
                    'model'=>$model, 
                    'actions' => $this->listActionsCanAccess,
            ));
        }
        catch (Exception $exc)
        {
            GasCheck::CatchAllExeptiong($exc);
        }
    }

    /** @Author: NhanDT Sep 20,2019
     *  @Todo: todo
     *  @Param: param
     **/
    public function reportCostEventExcel(){
        try{
            if(isset($_GET['to_excel'])){
                $mExcel = new ToExcel();
                $mExcel->costEventMarketExcel();
            }
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }
}
