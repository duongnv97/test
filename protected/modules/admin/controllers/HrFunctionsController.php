<?php

class HrFunctionsController extends AdminController {

    public $pluralTitle = DomainConst::CONTENT10000;
    public $singleTitle = DomainConst::CONTENT10000;

    /**
     * Manages all models.
     */
    public function actionIndex() {
        $this->pageTitle = DomainConst::CONTENT10015;
        try {
            $model = new HrFunctions('search');
            $model->unsetAttributes();
            //for search
            if (isset($_GET['HrFunctions'])) {
                $model->attributes = $_GET['HrFunctions'];
            }

            $this->render('index', array(
                'model' => $model, 'actions' => $this->listActionsCanAccess,
            ));
        } catch (Exception $exc) {
            GasCheck::CatchAllExeptiong($exc);
        }
    }

    /**
     * Setting all functions, in all roles
     * @param String $type Type function id
     */
    public function actionSetting($type = '', $role = '') {
        $this->pageTitle = DomainConst::CONTENT10016;
        try {
            $model = new HrFunctions('search');
            //for search
            if (isset($_GET['HrFunctions'])) {
                $model->attributes = $_GET['HrFunctions'];
            }

            $aTab = HrFunctionTypes::getArrayTypes();
            $this->render('functionsUI', array(
                'model' => $model,
                'type' => $type,
                'role' => $role,
                'aTab' => $aTab,
                'actions' => $this->listActionsCanAccess,
            ));
        } catch (Exception $exc) {
            GasCheck::CatchAllExeptiong($exc);
        }
    }

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id) {
        $this->pageTitle = DomainConst::CONTENT10017;
        try {
            $this->render('view', array(
                'model' => $this->loadModel($id), 'actions' => $this->listActionsCanAccess,
            ));
        } catch (Exception $exc) {
            GasCheck::CatchAllExeptiong($exc);
        }
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate() {
        $this->pageTitle = DomainConst::CONTENT10017;
        try {
            $model = new HrFunctions('create');
            if (isset($_POST['HrFunctions'])) {
                $model->attributes = $_POST['HrFunctions'];
                $model->validate();

                if (!$model->hasErrors()) {

                    $model->saveAll();
                    Yii::app()->user->setFlash(MyFormat::SUCCESS_UPDATE, "Thêm mới thành công.");
                    $this->redirect(array('create'));
                }
            }

            $this->render('create', array(
                'model' => $model, 'actions' => $this->listActionsCanAccess,
            ));
        } catch (Exception $exc) {
            GasCheck::CatchAllExeptiong($exc);
        }
    }
    
    /**
     * Action create function (for front-end)
     */
    public function actionFrontCreate() {
        $this->ajaxReload();
        $this->pageTitle = DomainConst::CONTENT10017;
        try {
            $model = new HrFunctions('create');
            $title = "Thêm mới thành công.";
            $model->unsetAttributes();
            if (isset($_GET['type_id'])){
                $model->attributes         = $_GET;
                $model->position_work_list = explode(",", $_GET['position_work_list']);
                $model->position_room_list = explode(",", $_GET['position_room_list']);
                $model->scenario           = 'update';
                $title                     = "Cập nhật thành công.";
            }
            if (isset($_POST['saveBtn'])) {
                if(isset($_POST['HrFunctions'])){
                    $model->attributes = $_POST['HrFunctions'];
                    $model->old_code_no= isset($_POST['HrFunctions']['old_code_no']) ? $_POST['HrFunctions']['old_code_no'] : null;
                }
                $model->validate();
                if (!$model->hasErrors()) {
                    $model->saveAll();
                    Yii::app()->user->setFlash(MyFormat::SUCCESS_UPDATE, $title);
                    if($model->scenario == 'update'){
                        $this->redirectUpdate($model);
                    } else {
                        $this->redirect(array('frontCreate'));
                    }
                }
            }

            $this->render('frontCreate', array(
                'model' => $model, 'actions' => $this->listActionsCanAccess,
            ));
        } catch (Exception $exc) {
            GasCheck::CatchAllExeptiong($exc);
        }
    }
    
    /** @Author: DuongNV Nov 19,2018
     *  @Todo: redirect when update functions
     **/
    public function redirectUpdate($model) {
        $list_pos  = implode(",", $model->position_work_list);
        $list_pos_formated = str_pad($list_pos, strlen($list_pos)+2, ",", STR_PAD_BOTH); // Thêm dấu phẩy đầu và cuối list id để search cho chính xác
        $list_room = implode(",", $model->position_room_list);
        $list_room_formated= str_pad($list_room, strlen($list_room)+2, ",", STR_PAD_BOTH); // Thêm dấu phẩy đầu và cuối list id để search cho chính xác
        $this->redirect(array('frontCreate',
            'type_id'=>$model->type_id,
            'position_work_list'=>$list_pos_formated,
            'position_room_list'=>$list_room_formated));
    }
    
    /**
     * Validate front create action rul
     * @param String $role_id Id of role
     * @param String $type_id Id of type
     */
    public function validateFrontCreateUrl(&$role_id, &$type_id) {
        $role_id = isset($_GET['role']) ? $_GET['role'] : '';
        $type_id = isset($_GET['type']) ? $_GET['type'] : '';
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id) {
        try {
            $model = $this->loadModel($id);
            $model->scenario = 'update';

            if (isset($_POST['HrFunctions'])) {
                $model->attributes = $_POST['HrFunctions'];
                $model->validate();
                if (!$model->hasErrors()) {
                    $model->update();
                    Yii::app()->user->setFlash(MyFormat::SUCCESS_UPDATE, DomainConst::CONTENT00035);
                }
            }

            $this->render('update', array(
                'model' => $model, 'actions' => $this->listActionsCanAccess,
            ));
        } catch (Exception $exc) {
            GasCheck::CatchAllExeptiong($exc);
        }
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id) {
        try {
            if (Yii::app()->request->isPostRequest) {
                // we only allow deletion via POST request
                if ($model = $this->loadModel($id)) {
                    if ($model->delete()) {
                        Yii::log("Uid: " . Yii::app()->user->id . " Delete record " . print_r($model->attributes, true), 'info');
                    }
                }

                // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
                if (!isset($_GET['ajax']))
                    $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('index'));
            }
            else {
                Yii::log("Uid: " . Yii::app()->user->id . " Invalid request. Please do not repeat this request again.");
                throw new CHttpException(400, 'Invalid request. Please do not repeat this request again.');
            }
        } catch (Exception $exc) {
            GasCheck::CatchAllExeptiong($exc);
        }
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer the ID of the model to be loaded
     */
    public function loadModel($id) {
        try {
            $model = HrFunctions::model()->findByPk($id);
            if ($model === null) {
                Yii::log("The requested page does not exist.");
                throw new CHttpException(404, 'The requested page does not exist.');
            }
            return $model;
        } catch (Exception $exc) {
            GasCheck::CatchAllExeptiong($exc);
        }
    }

    /** @Author: HOANG NAM day/month/2018
     *  @Todo: Get Function form
     *  @Param: 
     * */
    public function actionGetFunctionForm($modelName, $field_name, $relation) {
        if (isset($_GET['role_id']) && isset($_GET['ajax']) && $_GET['ajax'] == 1) {
            $model = new $modelName('search');
            $this->widget('FunctionWidget', array('model' => $model, 'field_name' => $field_name, 'relation' => $relation, 'role_id' => $_GET['role_id']));
        }
    }

    /**
     * Action get list function
     */
    public function actionGetListFunction() {
        if (!empty($_GET['role'])) {
            $this->layout = 'ajax';
            $model = new HrFunctions('search');
            $model->role_id = $_GET['role_id'];
            $this->renderPartial('frontCreate', array('model' => $model));
        }
    }

    /**
     * Action update ajax
     * @param String $type Type function id
     * @param String $role Role id
     */
    public function actionUpdateAjax($type = '', $role = '') {
        $data = array();
        $model = new HrFunctions('search');

        if (isset($_GET['HrFunctions'])) {
            $model->attributes = $_GET['HrFunctions'];
        }
        $data['model'] = $model;
        $data['role'] = $role;
        $data['type'] = $type;

        $this->layout = 'ajax';
        if (!empty($type) && !empty($role)) {
            $this->renderPartial('_ajaxIndex', $data, false, true);
        } else {
            $role_transfer = '';
            if(!empty($_GET['aNeedMore']) && gettype($_GET['aNeedMore']) != 'array'){
                $aNeedMore = json_decode($_GET['aNeedMore'], true);
                $role_transfer = $aNeedMore['role'];
            }
            $this->renderPartial('_function_data', array('type' => $type, 'role'=>$role_transfer), false, true);
        }
    }

    public function ajaxReload() {
        if(isset($_GET['ajax'])){
            $type     = isset($_GET['type']) ? $_GET['type'] : '';
            $pos_list = isset($_GET['pos'])  ? $_GET['pos']  : '';
            $resVal   = ''; $model  = '';
            $class    = 'dragThamSoItem';
            if($type == HrFunctions::KEYWORD_COEFFICIENT){
                $class = 'dragHeSoItem';
                $model = HrCoefficients::model()->getCoefByPosition($pos_list);
            } else {
                $model = HrParameters::model()->getParamsByPosition($pos_list);
            }
            foreach ($model as $item) {
                $resVal .= '<span class="'.$class.' dragItem" id="'.HrFunctions::KEYWORD_COEFFICIENT. $item->id . '" data-id="' . $item->id.'">' . $item->getName() . '</span>';
            }
            echo $resVal;
            die;
        }
    }
}
