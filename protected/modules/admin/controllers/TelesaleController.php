<?php

class TelesaleController extends AdminController 
{
    public $pluralTitle = 'Khách hàng Telesale';
    public $singleTitle = 'Khách hàng Telesale';

    public function actionIndex()
    {
        $this->pageTitle = 'Telesale list ';
        try{
        //$model = Telesale::model()->getCustomer(); 
        $cRole  = MyFormat::getCurrentRoleId();
        $cUid   = MyFormat::getCurrentUid();
        $model = new Telesale('search');
        $model->unsetAttributes();  // clear any default values
        if(isset($_GET['type']) && $_GET['type'] == $model->call_again){
            $model->date_call_from  = date('d-m-Y');
            $model->date_call_to    = $model->date_call_from;
        }
        if(isset($_GET['type']) && in_array($cRole, $model->getArrayRoleLikeTelesale())){
            $model->sale_id    = $cUid;
        }
        $model->province_id    = GasProvince::TP_HCM;
        
        if(isset($_GET['Telesale'])){
            $model->attributes = $_GET['Telesale'];
        }
        $this->checkUpdateSolr();
        if(isset($_POST['Telesale']))
        {
            if(isset($_POST['Telesale']['customer']) && count($_POST['Telesale']['customer']) > 0){
                $aIdCustomer    = $_POST['Telesale']['customer'];
                $sale_id        = $_POST['Telesale']['sale_id'];
                $number_customer = $model->updateSalsId($aIdCustomer,$sale_id);
//                $number_customer = 0;
//                foreach ($_POST['Telesale']['customer'] as $ctm_id) {
//                    $mCustomer = $this->loadModel($ctm_id);
//                    $mCustomer->sale_id = $sale_id;// Open normal để Telesale nhận gọi tính BQV bình thường 
////                    $mCustomer->storehouse_id = $sale_id;// Dec2818 open cho nhận KH App gọi test của PTTT Anh Đợt => ko tính BQV
//                    $mCustomer->save();
//                    $number_customer++;
//                }
                $model->saveOneManyBig($sale_id, $aIdCustomer);
                if($sale_id == ''){
                    Yii::app()->user->setFlash( 'notice', "BỎ GÁN THÀNH CÔNG ".$number_customer." KHÁCH HÀNG" );
                    $this->redirect(array('index'));
                }
                Yii::app()->user->setFlash( 'notice', "GÁN THÀNH CÔNG ".$number_customer." KHÁCH HÀNG" );
                    $this->redirect(array('index'));
            }						
        }
        $model->limitByZone();

        $this->render('index',array(
                'model'=>$model, 'actions' => $this->listActionsCanAccess,
        ));
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }

    public function actionCreateNote() {
        $this->pageTitle = 'Tạo Mới ';
        try{
        $model=new FollowCustomer('create');

        if(isset($_POST['FollowCustomer']))
        {
            $model->attributes=$_POST['FollowCustomer'];
            $model->validate();

            if(!$model->hasErrors()){
                $model->save();
                Yii::app()->user->setFlash( MyFormat::SUCCESS_UPDATE, "Thêm mới thành công." );
                $this->redirect(array('create'));
            }				
        }

        $this->render('create',array(
                'model'=>$model, 'actions' => $this->listActionsCanAccess,
        ));
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }
    
    public function loadModel($id)
    {
        try{
        $model=Users::model()->findByPk($id);
        if($model===null)
        {
            Yii::log("The requested page does not exist.");
            throw new CHttpException(404,'The requested page does not exist.');
        }			
        return $model;
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }
    
    public function actionReportGasBack()
    {
        $this->pageTitle = 'Telesale bình quay về';
        try{
        $model = new Telesale('search');
        $model->unsetAttributes();  // clear any default values
        $model->date_from   = '01-'.date('m-Y');
        $model->date_to     = date('d-m-Y');
        if(isset($_GET['Telesale'])){
            $model->attributes = $_GET['Telesale'];
        }
        $this->render('report/gas_back',array(
            'model' => $model,
            'data' => $model->getGasBack(),
            'actions' => $this->listActionsCanAccess,
        ));
        
        
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }
    
    /** @Author: HOANG NAM 02/05/2018
     *  @Todo: report Marketing
     *  @Code: NAM012
     **/
    public function actionExportExcelReportGasBack() {
        try{
        if(isset($_SESSION['data-model-gas-back'])){
            ini_set('memory_limit','1500M');// chưa cần chạy đoạn này, đưa memory_limit lên 500 có thể export dc khoảng 5000 row - chưa test
            ToExcel::summaryExportExcelReportGasBack();
        }
        $this->redirect(array('ReportGasBack'));
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }
    
    public function actionReportCancel()
    {
        $this->pageTitle = 'Telesale báo cáo khách hàng từ chối';
        try{
        $model = new Telesale('search');
        $model->unsetAttributes();  // clear any default values
        $model->date_from   = '01-'.date('m-Y');
        $model->date_to     = date('d-m-Y');
        if(isset($_GET['Telesale'])){
            $model->attributes = $_GET['Telesale'];
        }
        $this->render('report/index_cancel',array(
            'model' => $model,
            'data' => $model->getCancel(),
            'actions' => $this->listActionsCanAccess,
        ));
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }
    
    /** @Author: ANH DUNG Jun 16, 2018
     *  @Todo: bc check cuộc gọi, input code và BQV của telesale 
     * 1. get all info KH nhập code của NV
     * 2. get all BQV của KH đó
     * 3. get all cuộc gọi ra ứng với list số đt của KH
     **/
    public function actionInputCode()
    {
        error_reporting(1);
        $this->pageTitle = 'Telesale báo cáo cuộc gọi đơn APP';
        $this->inputCodeExcel();
        try{
        $model = new Telesale('search');
        $model->unsetAttributes();  // clear any default values
        $model->date_from   = '01-'.date('m-Y');
        $model->date_to     = date('d-m-Y');
        if(isset($_GET['Telesale'])){
            $model->attributes = $_GET['Telesale'];
        }

        $this->render('call/inputCode',array(
            'model' => $model,
            'rData' => $model->rInputCode(),
            'actions' => $this->listActionsCanAccess,
        ));
        
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }
    
    /** @Author: ANH DUNG Jun 18, 2018
     *  @Todo: export excel telesale 
     **/
    public function inputCodeExcel() {
        try{
        if(!isset($_GET['ToExcel']) || !isset($_SESSION['rData'])){
            return ;
        }
        ini_set('memory_limit','1500M');// chưa cần chạy đoạn này, đưa memory_limit lên 500 có thể export dc khoảng 5000 row - chưa test
        $mToExcel = new ToExcel();
        $mToExcel->telesaleInputCode();
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }
    
    /** @Author: DuongNV Sep 20,2018
     *  @Todo: Check Update Solr
     **/
    public function checkUpdateSolr() {
        if(isset($_GET['UpdateSolr'])){
            $id = isset($_GET['_id']) ? $_GET['_id'] : 0;
            $mUser = Users::model()->findByPk($id);
            if(!empty($mUser)) {
                $mUser->solrAdd();
            }
            echo CJavaScript::jsonEncode([]);die; 
        }
    }
    
    /** @Author: NamNH Sep 26,2018
     *  @Todo: report call / no call
     **/
    public function actionReportCall(){
        $this->pageTitle = 'Telesale list ';
        try{
        $model  = new Telesale('search');
        $model->unsetAttributes();  // clear any default values
        $aData  = [];
        if(isset($_GET['Telesale'])){
            $model->attributes  = $_GET['Telesale'];
            $aData              = $model->getReportCall();
        }
        $this->render('report/reportCall/index',array(
            'model'     =>$model,
            'aData'    =>$aData,
            'actions'   => $this->listActionsCanAccess,
        ));
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }
    
    /** @Author: NamNH Nov 23, 2018
     *  @Todo: report telesale by agent
     **/
    public function actionReportAgent(){
        $this->pageTitle = 'Báo cáo cuộc gọi telesale theo đại lý';
        try{
        $model  = new Telesale('search');
        $model->unsetAttributes();  // clear any default values
        $aData  = [];
        $model->date_from   = '01-'.date('m-Y');
        $model->date_to     = date('d-m-Y');
        if(isset($_GET['Telesale'])){
            $model->attributes  = $_GET['Telesale'];
            $aData              = $model->getReportAgent();
        }
        $this->render('report/reportAgent/index',array(
            'model'     =>$model,
            'aData'    =>$aData,
            'actions'   => $this->listActionsCanAccess,
        ));
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }
}


