<?php

class UsersPriceHgdController extends AdminController 
{
    public $pluralTitle = "Giá KH hộ gia đình";
    public $singleTitle = "Giá KH hộ gia đình";
    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id)
    {
        $this->pageTitle = 'Xem ';
        try{
        $this->render('view',array(
                'model'=>$this->loadModel($id), 'actions' => $this->listActionsCanAccess,
        ));
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate()
    {
        $this->pageTitle = 'Tạo Mới ';
        try{
        $model=new UsersPriceHgd('create');
        $model->c_month = date('m')*1;
        $model->c_year  = date('Y');
        if(isset($_POST['UsersPriceHgd'])){
            $model->attributes=$_POST['UsersPriceHgd'];
            $model->validate();
            if(!$model->hasErrors()){
                $model->save();
                Yii::app()->user->setFlash( MyFormat::SUCCESS_UPDATE, "Thêm mới thành công." );
                $this->redirect(array('create'));
            }				
        }

        $this->render('create',array(
                'model'=>$model, 'actions' => $this->listActionsCanAccess,
        ));
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id)
    {
        try{
        $model=$this->loadModel($id);
        if (!$model->canUpdate()) {
            $this->redirect(array('index'));
        }
        
        $model->scenario = 'update';
        $model->getDataUpdate();
        $this->pageTitle = $model->geZoneNo();
        if(isset($_POST['UsersPriceHgd']))
        {
            $model->attributes=$_POST['UsersPriceHgd'];
            $model->validate();
            if(!$model->hasErrors()){
                $model->update();
                $mAppCache = new AppCache();
                $mAppCache->setPriceHgd(AppCache::PRICE_HGD);
                Yii::app()->user->setFlash( MyFormat::SUCCESS_UPDATE, "Cập nhật thành công." );
                $this->redirect(array('update','id'=>$model->id));
            }else{
                Yii::app()->user->setFlash( MyFormat::ERROR_UPDATE, $model->getError('zone_no') );
            }
        }

        $this->render('update',array(
                'model'=>$model, 'actions' => $this->listActionsCanAccess,
        ));
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id)
    {
        try{
        if(Yii::app()->request->isPostRequest){
            // we only allow deletion via POST request
            if($model = $this->loadModel($id))
            {
                if($model->delete())
                    Yii::log("Uid: " .Yii::app()->user->id. " Delete record ".  print_r($model->attributes, true), 'info');
            }

            // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
            if(!isset($_GET['ajax']))
                    $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('index'));
        }
        else
        {
            Yii::log("Uid: " .Yii::app()->user->id. " Invalid request. Please do not repeat this request again.");
            throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
        }	
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }

    /**
     * Manages all models.
     */
    public function actionIndex()
    {
        $this->pageTitle = 'Danh Sách ';
        try{
        $model=new UsersPriceHgd('search');
        
//        $model->checkDuplicateMaterialInZone();// only for dev check
        $model->unsetAttributes();  // clear any default values
        if(isset($_GET['setup'])){
//            $model->c_month         = date('m')*1;
//            $model->c_year          = date('Y');
//            $model->HgdPriceInit    = Yii::app()->setting->getItem('HgdPriceInit');
//            $model->HgdAmountAdjust = Yii::app()->setting->getItem('HgdAmountAdjust');
//            $model->HgdAmountAdjustGas = Yii::app()->setting->getItem('HgdAmountAdjustGas');
        }
        $this->handleSetup($model);
        if(isset($_GET['UsersPriceHgd']))
            $model->attributes=$_GET['UsersPriceHgd'];

        $this->render('index',array(
                'model'=>$model, 'actions' => $this->listActionsCanAccess,
        ));
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }
    
    /**
     * @Author: ANH DUNG Dec 28, 2016
     * @Todo: handle setup lại giá P cho một tháng
     */
    public function handleSetup($model) {
        if(isset($_GET['setup']) && isset($_POST['UsersPriceHgd'])){
            if(!$model->canSetAdjustAmountInMonth()){
                throw new Exception('Bạn không có quyền hoặc thao tác không hợp lệ');
            }
            $model->attributes=$_POST['UsersPriceHgd'];
            $model->HgdPriceInit        = MyFormat::removeComma($model->HgdPriceInit);
            $model->HgdAmountAdjust     = MyFormat::removeComma($model->HgdAmountAdjust);
            $model->HgdAmountAdjustGas  = MyFormat::removeComma($model->HgdAmountAdjustGas);
            Yii::app()->setting->setDbItem('HgdPriceInit', $model->HgdPriceInit);
            Yii::app()->setting->setDbItem('HgdAmountAdjust', $model->HgdAmountAdjust);
            Yii::app()->setting->setDbItem('HgdAmountAdjustGas', $model->HgdAmountAdjustGas);
            $model->autoSetPrice();
            
            Yii::app()->user->setFlash( MyFormat::SUCCESS_UPDATE, "Cập nhật thành công, vui lòng vào danh sách giá hộ GĐ kiểm tra lại kết quả" );
            $this->render('index',array(
                'model'=>$model, 'actions' => $this->listActionsCanAccess,
            ));
            die;
        }
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
    * If the data model is not found, an HTTP exception will be raised.
     * @param integer the ID of the model to be loaded
     */
    public function loadModel($id)
    {
        try{
        $model=UsersPriceHgd::model()->findByPk($id);
        if($model===null)
        {
            Yii::log("The requested page does not exist.");
            throw new CHttpException(404,'The requested page does not exist.');
        }			
        return $model;
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }

}
