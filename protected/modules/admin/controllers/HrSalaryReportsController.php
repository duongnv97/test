<?php

class HrSalaryReportsController extends AdminController {

    public $pluralTitle = DomainConst::CONTENT10012;
    public $singleTitle = DomainConst::CONTENT10012;

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id) {
        $this->pageTitle = 'Xem ';
        try {
            $this->render('view', array(
                'model' => $this->loadModel($id), 'actions' => $this->listActionsCanAccess,
            ));
        } catch (Exception $exc) {
            GasCheck::CatchAllExeptiong($exc);
        }
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate() {
        $this->pageTitle = 'Tạo Mới ';
        try {
            $model = new HrSalaryReports('create');

            if (isset($_POST['HrSalaryReports'])) {
                $model->attributes = $_POST['HrSalaryReports'];
                $model->validate();
                if (!$model->hasErrors()) {
                    $model->save();
                    Yii::app()->user->setFlash(MyFormat::SUCCESS_UPDATE, "Thêm mới thành công.");
                    $this->redirect(array('create'));
                }
            }

            $this->render('create', array(
                'model' => $model, 'actions' => $this->listActionsCanAccess,
            ));
        } catch (Exception $exc) {
            GasCheck::CatchAllExeptiong($exc);
        }
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id) {
        $this->pageTitle = 'Cập Nhật ';
        try {
            $model = $this->loadModel($id);
            $model->scenario = 'update';
            if (isset($_POST['HrSalaryReports'])) {
                $model->attributes = $_POST['HrSalaryReports'];
                // $this->redirect(array('view','id'=>$model->id));
                $model->validate();
                if (!$model->hasErrors()) {
                    $model->save();
                    Yii::app()->user->setFlash(MyFormat::SUCCESS_UPDATE, "Cập nhật thành công.");
                    $this->redirect(array('update', 'id' => $model->id));
                }
            }

            $this->render('update', array(
                'model' => $model, 'actions' => $this->listActionsCanAccess,
            ));
        } catch (Exception $exc) {
            GasCheck::CatchAllExeptiong($exc);
        }
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id) {
        try {
            if (Yii::app()->request->isPostRequest) {
                // we only allow deletion via POST request
                if ($model = $this->loadModel($id)) {
                    if( !empty($model->code_no && isset($_POST['same_code_no'])) ){
                        $tableName  = HrSalaryReports::model()->tableName();
                        $sql        = "DELETE FROM {$tableName} WHERE code_no = '{$model->code_no}'";
                        Yii::app()->db->createCommand($sql)->execute();
                        Yii::log("Uid: " . Yii::app()->user->id . " Delete all record with code_no " . $model->code_no, 'info');
                    } else {
                        if ($model->delete())
                            Yii::log("Uid: " . Yii::app()->user->id . " Delete record " . print_r($model->attributes, true), 'info');
                    }
                }

                // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
                if (!isset($_GET['ajax']))
                    $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('index'));
            }
            else {
                Yii::log("Uid: " . Yii::app()->user->id . " Invalid request. Please do not repeat this request again.");
                throw new CHttpException(400, 'Invalid request. Please do not repeat this request again.');
            }
        } catch (Exception $exc) {
            GasCheck::CatchAllExeptiong($exc);
        }
    }

    /**
     * Manages all models.
     */
    public function actionIndex() {
        $this->pageTitle = 'Danh Sách ';
        try {
            $model = new HrSalaryReports('search');
            $model->unsetAttributes();  // clear any default values
            if (isset($_GET['HrSalaryReports']))
                $model->attributes = $_GET['HrSalaryReports'];

            $this->render('index', array(
                'model' => $model, 'actions' => $this->listActionsCanAccess,
            ));
        } catch (Exception $exc) {
            GasCheck::CatchAllExeptiong($exc);
        }
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer the ID of the model to be loaded
     */
    public function loadModel($id) {
        try {
            $model = HrSalaryReports::model()->findByPk($id);
            if ($model === null) {
                Yii::log("The requested page does not exist.");
                throw new CHttpException(404, 'The requested page does not exist.');
            }
            return $model;
        } catch (Exception $exc) {
            GasCheck::CatchAllExeptiong($exc);
        }
    }

    /**
     *  @Todo: report index page
     *  @Param:
     **/
    public function actionReport()
    {
        $this->setShowInApprove();
        $this->pageTitle = 'Báo cáo lương, sản lượng và chấm công';
        try{
            $this->render('report',array(
                    'actions' => $this->listActionsCanAccess,
            ));
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }
    
    /**
     *  @Todo: report index page
     *  @Param:
     **/
    public function actionGetTypeReportContent()
    {
        try{
            $this->sortHeaderReport();
            $type = empty($_GET['type']) ? '' : $_GET['type'];
            $this->layout = 'ajax';
            $this->renderPartial('_data_report',array(
                    'type'    => $type,
                    'actions' => $this->listActionsCanAccess,
            ));
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }
    
    /*
     * @Todo: action for ajax
     */
    public function actionUpdateAjax()
    {
        $type       = intval(isset($_GET['type']) ? $_GET['type'] : '');
        $action     = isset($_GET['action']) ? $_GET['action'] : '';
        $id         = intval(isset($_GET['id']) ? $_GET['id'] : '');
        $code_no    = isset($_GET['code_no']) ? $_GET['code_no'] : '';
        $model      = '';
        $isReturn   = false;
        switch ($action) {
            case 'view':
                $model = empty($code_no) 
                        ? HrSalaryReports::model()->findByPk($id)
                        : HrSalaryReports::model()->getReportSameCodeNo($code_no);
                $isReturn = true;
                break;
            case 'loadLastCalculatingReport':
                $criteria = new CDbCriteria;	
                $criteria->order = "id desc";
                $criteria->compare("t.type_id", $type);
                $criteria->compare("t.status", HrSalaryReports::STT_CALCULATING);
                $model = HrSalaryReports::model()->find($criteria);
                if( !empty($model->code_no) ){
                    $model = HrSalaryReports::model()->getReportSameCodeNo($model->code_no);
                }
                break;

            default:
                break;
        }
        if(empty($model)) return '';
        if($isReturn){
            $result['report'] = $this->renderHtmlReportData($model, $isReturn);
            $result['button'] = $model->getListButtonOfReport();
            echo CJSON::encode($result);
        } else {
            $this->renderHtmlReportData($model, $isReturn);
        }
        Yii::app()->end();
    }
    
    /**
     * action for calculating salary
     */
    public function actionCalculate(){
         error_reporting(1);
        $this->pageTitle = 'Calculating...';
        try{
            $this->layout           = 'ajax';
            $model                  = new HrSalaryReports('UserCalculate');
            $model->dateFromDmy     = date('01-m-Y');
            $model->dateToDmy       = date('t-m-Y');
            $mSchedule              = new HrWorkSchedule();
            $aUser                  = [];
            //Search user
            if(isset($_GET['search_user'])){
                $mSchedule->attributes = isset($_GET['HrWorkSchedule']) ? $_GET['HrWorkSchedule'] : '';
                if( isset($_GET['HrWorkSchedule']['search_province']) ){
                    $mSchedule->search_province = $_GET['HrWorkSchedule']['search_province'];
                    $model->province_id         = implode(",", $mSchedule->search_province);
                }
                if( isset($_GET['HrWorkSchedule']['search_position']) ){
                    $mSchedule->search_position = $_GET['HrWorkSchedule']['search_position'];
                    $model->position_work       = implode(",", $mSchedule->search_position);
                }
                $aUser = $mSchedule->getArrayUserHr();
            }
            if( isset($_GET['isCalculateAll']) ){
                $model->isCalculateAll = true;
            }
            //recalculate
            if( !empty($_GET['re']) ){
                $lastRId                = $_GET['rId'];
                $model                  = HrSalaryReports::model()->findByPk($lastRId);
                if( !empty($model->data) ) {
                    $aIdUser            = [];
                    if( !empty($model->code_no) ){
                        $model          = $model->getReportSameCodeNo();
                    }
                    $model->dateFromDmy = MyFormat::dateConverYmdToDmy($model->start_date, 'd-m-Y');
                    $model->dateToDmy   = MyFormat::dateConverYmdToDmy($model->end_date, 'd-m-Y');
                    $aPrevData  = json_decode($model->data, true);
                    foreach ($aPrevData as $key => $value) {
                        if($key != HrSalaryReports::KEY_HEADER) $aIdUser[] = $key;
                    }
                    $aUser              = $model->getArrayUsersById($aIdUser);
                }
            }

            if( isset($_POST['calculate']) ){
                $model->attributes  = $_POST['HrSalaryReports'];
                $model->start_date  =  MyFormat::dateConverDmyToYmd($model->dateFromDmy, '-');
                $model->end_date    =  MyFormat::dateConverDmyToYmd($model->dateToDmy, '-');
                $arrUserHr          = empty($_POST['HrSalaryReports']['user_id']) ? [] : $model->getArrayUsersById($_POST['HrSalaryReports']['user_id']);
                $aIdUser            = empty($_POST['HrSalaryReports']['user_id']) ? [] : $_POST['HrSalaryReports']['user_id'];
                $model->validate();
                if( !$model->hasErrors() ){
                    // Tính lương cho nhiều bộ phận cùng lúc
                    $aPositionWork = explode(',', $model->position_work);
                    $aProvince     = explode(',', $model->province_id);
                    if( count($aPositionWork) > 1 || count($aProvince) > 1 ){
                        $code_no    = $model->generateCodeNo();
                        $pct100     = count($aPositionWork)*count($aProvince);
                        $isInitData = false;
                        $mInitData  = null;
                        foreach ($aPositionWork as $key_pos => $position) {
                            foreach ($aProvince as $key_pro => $province) {
                                //Progress bar
                                $percent    = intval(($key_pos*count($aProvince)+$key_pro+1)*100/$pct100);
                                $model->flushData($percent);
                                //End progress bar
                                $mNewHr                     = new HrSalaryReports('UserCalculate');
                                if($isInitData){
                                    $mNewHr         = $mInitData;
                                    $mNewHr->id     = null;
                                    $mNewHr->setIsNewRecord(true);
                                }
                                $mNewHr->type_id            = $model->type_id;
                                $mNewHr->name               = $model->name;
                                $mNewHr->start_date         = $model->start_date;
                                $mNewHr->end_date           = $model->end_date;
                                $mNewHr->dateFromDmy        = $model->dateFromDmy;
                                $mNewHr->dateToDmy          = $model->dateToDmy;
                                $mNewHr->position_work      = $position;
                                $mNewHr->province_id        = $province;
                                $mNewHr->status             = HrSalaryReports::STT_CALCULATING;
                                $mSchedule                  = new HrWorkSchedule();
                                $mSchedule->search_province = [$province];
                                $mSchedule->search_position = [$position];
                                $mNewHr->code_no            = $code_no;
                                $arrUserHr                  = $mSchedule->getArrayUserHr();
                                if(count($arrUserHr) > 0){
                                    if(!$isInitData){
                                        $mNewHr->initData();
                                        $mNewHr->isInitData = true;
                                        $mInitData          = $mNewHr;
                                        $isInitData         = true;
                                    }
                                    $mNewHr->aIdUser            = array_keys($arrUserHr);
                                    $mNewHr->createReport($arrUserHr);
                                    $mNewHr->save();
                                }
                            }
                        }
                        // Delete old report when recalculate
                        if( !empty($_GET['re']) ){
                            $model->deleteModelSameCodeNo();
                        }
                    } else {
//                        $model->aIdUser = $aIdUser;
                        $model->createReport($arrUserHr);
                        $model->save();
                    }
                    die('<script type="text/javascript">parent.$.fn.colorbox.close();</script>');
                }
            }
            $this->render('_calculating',array(
                'model'     => $model,
                'aUser'     => $aUser,
                'actions'   => $this->listActionsCanAccess,
            ));
            
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }
    
    /**
     * change status between approved and final
     */
    public function actionChangeStatus() {
        try {
            $this->sendReviewLv1(); // Gửi chị Ngân duyệt
            $id         = empty($_GET['id']) ? '' : $_GET['id'];
            $code_no    = empty($_GET['code_no']) ? '' : $_GET['code_no'];
            $status     = empty($_GET['status']) ? '' : $_GET['status'];
            $mHrSalary  = new HrSalaryReports();
            if( !empty($code_no) ){
                $mHrSalary->changeReportStatusByCodeNo($code_no, $status);
                echo HrSalaryReports::model()->aStatus[$status];
                return;
            }
            if (!empty($id) && in_array($status, array_keys(HrSalaryReports::model()->aStatus))){
                $model = HrSalaryReports::model()->findByPk($id);
                if($status == HrSalaryReports::STT_FINAL){
                    $criteria = new CDbCriteria; 
                    $criteria->compare('type_id', $model->type_id);
                    $criteria->compare('position_work', $model->position_work);
                    $criteria->compare('status', HrSalaryReports::STT_FINAL);
                    $criteria->compare('MONTH(start_date)', date('m', strtotime($model->start_date)));
                    $criteria->compare('YEAR(start_date)', date('Y', strtotime($model->start_date)));
                    $mReportIfExist = HrSalaryReports::model()->findAll($criteria);
                    if(!empty($mReportIfExist)){
                        return 0;
                    }
                }
                
                $model->status = $status;
                $model->save();
                echo HrSalaryReports::model()->aStatus[$status];
            }
        } catch (Exception $exc) {
            GasCheck::CatchAllExeptiong($exc);
        }
    }
    
    /** @Author: DuongNV 28 Mar,19
     *  @Todo: Gửi chị Ngân xét duyệt bảng lương của tất cả các bộ phận
     **/
    public function sendReviewLv1() {
        $model = new HrSalaryReports();
        if( isset($_GET['reviewlv1']) && $model->canSendReview()){
            $code_no    = isset($_GET['code_no']) ? $_GET['code_no'] : '';
            if(empty($code_no)) return;
            $model->changeReportStatusByCodeNo($code_no, HrSalaryReports::STT_REQUEST);
            $criteria = new CDbCriteria;
            $criteria->compare('t.code_no', $code_no);
            $model = HrApprovedReports::model()->find($criteria);
            $mApprove = new HrApprovedReports();
            if($model){
                $model->id_approved_lv1   = '';
                $model->date_approved_lv1 = '';
                $model->id_approved_lv2   = '';
                $model->date_approved_lv2 = '';
                $mApprove                 = $model;
                $mApprove->isNewRecord    = false;
            }
            
            $mApprove->code_no = $code_no;
            $mApprove->status  = HrApprovedReports::STT_NEW;
            
            $criteria       = new CDbCriteria;
            $criteria->compare('t.code_no', $code_no);
            $mReport        = HrSalaryReports::model()->find($criteria);
            if($mReport){
                $cDate          = date('d');
                $salaryDate     = date('m/Y', strtotime($mReport->start_date));
                $mApprove->name = ( $cDate >= 15 && $cDate <= 20 ) ? 'Lương kỳ 1 T'.$salaryDate : 'Lương kỳ 2 T'.$salaryDate;
                $mApprove->isNewRecord ? $mApprove->save() : $mApprove->update();
                $mApprove->notifyEmail();
            }
            $this->redirect(array('report'));
        }
    }
    
    /**
     * get left menu report calculating tab
     */
    public function actionGetHtmlListReport() {
        try {
            $type   = empty($_GET['type']) ? '' : $_GET['type'];
            $status = empty($_GET['status']) ? HrSalaryReports::STT_CALCULATING : explode(",", $_GET['status']);
            $model  = new HrSalaryReports('search');
            $aRp    = $model->getArraySalaryReportsByType($type, $status, true);
            $aStatusColor = array(
                            HrSalaryReports::STT_CALCULATING => '#e6f1fd!important',
                            HrSalaryReports::STT_REQUEST     => '#ffa2a2!important',
                            HrSalaryReports::STT_APPROVED    => '#dbcb7f!important',
                            HrSalaryReports::STT_FINAL       => '#a2ffba!important',
                        );
            foreach ($aRp as $key => $value) {
                $st         = MyFormat::dateConverYmdToDmy($value->start_date);
                $en         = MyFormat::dateConverYmdToDmy($value->end_date);
                $cssClass   = 'calculatingBtn ';
                if( array_search($key, array_keys($aRp)) === 0 ){
                    $cssClass = 'active';
                }
                if($value->status == HrSalaryReports::STT_APPROVED || $value->status == HrSalaryReports::STT_FINAL){
                    $cssClass .= ' approvedBtn';
                }
                if($value->status == HrSalaryReports::STT_CALCULATING){
                    $cssClass .= ' calculatingBtn';
                }
                $code_no = empty($value->code_no) ? '' : " data-code_no='{$value->code_no}'";
                echo "<li class='ajaxBtn btn btn-default {$cssClass}'"
                        . ' data-action="view"'
                        . " data-id='{$value->id}'"
                        .   $code_no
                        . " data-status='{$value->status}'" 
                        . " style='background:{$aStatusColor[$value->status]}'>"
                            ."<b>{$value->name}</b><br>{$st} - {$en}"
                      .'</li>';
            }
        } catch (Exception $exc) {
            GasCheck::CatchAllExeptiong($exc);
        }
    }
    
    /**
     * send mail
     */
    public function actionSendMailReport($id = '') {
        try {
            if($id != ''){
                $model = HrSalaryReports::model()->findByPk($id);
            }
            if(isset($_POST['HrSalaryReports']['approved_by'])){
                $model->approved_by = $_POST['HrSalaryReports']['approved_by'];
                $model->status = HrSalaryReports::STT_REQUEST;
                if (!$model->hasErrors()) {
                    $model->save();
                }
                $title = 'Yêu cầu xét duyệt '.$model->name;
                $approveUrl = Yii::app()->createAbsoluteUrl("admin/HrSalaryReports/ApprovedReport", array('id'=>$model->id));
                GasScheduleEmail::notifyHr($model, $approveUrl, $title);
                die('<script type="text/javascript">parent.$.fn.colorbox.close();</script>');
            }
            $this->layout = 'ajax';
            $this->render('listoptionApprove',array(
                'model'=>$model,
            ));
        } catch (Exception $exc) {
            GasCheck::CatchAllExeptiong($exc);
        }
    }
    
    /**
     * Approved Report for approver
     */
    public function actionApprovedReport($id) {
        try {
            $cRole = MyFormat::getCurrentRoleId();
            $cUid = MyFormat::getCurrentUid();
//            $aApprover = HrSalaryReports::model()->getArrayApprover();
            $model = HrSalaryReports::model()->findByPk($id);
            if($cUid == $model->approved_by){
                if(isset($_POST['HrSalaryReports'])){
                    if(isset($_POST['HrSalaryReports']['approved'])){ //Approved
                        $model->status = HrSalaryReports::STT_APPROVED;
                        $model->approved_date = date('Y-m-d H:i:s');
                        Yii::app()->user->setFlash(MyFormat::SUCCESS_UPDATE, "Duyệt bảng lương thành công.");
                    } else { // Require update
                        $model->status = HrSalaryReports::STT_REQUEST;
                        $model->approved_date = null;
                        $model->notify = $_POST['HrSalaryReports']['notify'];
                        Yii::app()->user->setFlash(MyFormat::SUCCESS_UPDATE, "Yêu cầu chỉnh sửa thành công.");
                    }
                }
//                $model->approved_by = $cUid;
                $model->save();
                $this->render('approvedSuccess', array('model' => $model));
            } else {
                throw new CHttpException(400, 'Bạn không có quyền truy cập trang');
            }
        } catch (Exception $exc) {
            GasCheck::CatchAllExeptiong($exc);
        }
    }
    
    /** @Author: DUONG 22/07/2018
     *  @Todo: export to excel
     **/
    public function actionExportExcel(){
        try{
        if(isset($_SESSION['data-excel']) && isset($_SESSION['data-model'])){
            ini_set('memory_limit','1500M');// chưa cần chạy đoạn này, đưa memory_limit lên 500 có thể export dc khoảng 5000 row - chưa test
            ToExcel::summarySalaryReports();
        }
        $this->redirect(array('index'));
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }
    
    /** @Author: NVDuong 30/07/18
     *  @Todo: get list final table to calculate salary, ajax ra để tính ở tab thu nhập
     **/
    public function actionGetListTable() {
        $month = isset($_GET['HrSalaryReports']['search_month']) ? $_GET['HrSalaryReports']['search_month'] : '';
        $year  = isset($_GET['HrSalaryReports']['search_year']) ? $_GET['HrSalaryReports']['search_year'] : '';
        $posit = isset($_GET['HrSalaryReports']['search_position']) ? $_GET['HrSalaryReports']['search_position'] : '';
        $provi = isset($_GET['HrSalaryReports']['search_province']) ? $_GET['HrSalaryReports']['search_province'] : '';
        if(empty($month) || empty($year)){
            die("");
        }
        $mAllType = HrFunctionTypes::model()->findAll();
        $aAllType = [];
        foreach ($mAllType as $t) {
            $aAllType[$t->id] = $t->name;
        }
        
        //Find exists salary table
        $criteria1 = new CDbCriteria; 
        $criteria1->compare('t.type_id', UsersHr::model()->getIdCalcSalary()); // Type là tính lương
        $criteria1->compare('MONTH(t.start_date)', $month);
        $criteria1->compare('YEAR(t.start_date)', $year);
        $mSalaryExists = HrSalaryReports::model()->find($criteria1);
        
        $html               = '';
        $btnExportExcel     = '';
        $btnRequest         = '';
        $btnCalculate       = '';
        $btnShowInApprove   = ''; // hiển trên trang list Tổng hợp lương
        
        if(!empty($mSalaryExists)){
            if(!empty($mSalaryExists->code_no)){
                $needMore = [
                    'position' => $posit,
                    'province' => $provi,
                ];
                $mSalaryExists = $mSalaryExists->getReportSameCodeNo('', $needMore);
                $urlRequest    = Yii::app()->createAbsoluteUrl('admin/HrSalaryReports/changeStatus', ['reviewlv1'=>1, 'code_no'=>$mSalaryExists->code_no]);
                $btnRequest    = '<a class="btn btn-primary" href="'.$urlRequest.'" style="margin-left:15px; visibility:'.($mSalaryExists->canSendReview() ? 'visible':'hidden').'">Gửi xét duyệt</a>';

                $urlSetShow         = Yii::app()->createAbsoluteUrl('admin/HrSalaryReports/report', ['showInApprove'=>1,'code_no'=>$mSalaryExists->code_no, 'hide'=> empty($mSalaryExists->is_show) ? 0 : 1]);
                $text               = empty($mSalaryExists->is_show) ? 'Tạo mới': 'Hủy tạo';
                $btnShowInApprove   = $mSalaryExists->canSetShow() ? '<a class="btn btn-primary" href="'.$urlSetShow.'" style="margin-left:15px;">'.$text.'</a>' : '';
            }
            echo '<div class="error-rp">';
            $this->renderHtmlReportData($mSalaryExists);
            echo '</div>';
            $urlEE          = Yii::app()->createAbsoluteUrl('admin/HrSalaryReports/ExportExcel');
            $btnExportExcel = '<a class="ExportExcelBtn btn btn-primary" data-action="" href="'.$urlEE.'" style="margin-left:15px;">Export Excel</a>';
        }

        $mSalary = new HrSalaryReports();
        $message = '';
        if($mSalary->canCalculateFinalSalary($month, $year, $message)){
            $btnCalculate = '<br><a class="btn btn-primary calTotalSalaryBtn">Tính</a>';
        } else {
            $btnCalculate = '<br><p>'.$message.'</p>';
            $btnRequest   = '';
        }
        echo $html.$btnCalculate.$btnExportExcel.$btnRequest.$btnShowInApprove;
    }
    
    /** @Author: NVDuong 31/07/18
     *  @Todo: load more month of prev year
     *  @Param: 
     **/
    public function actionLoadMoreMonth($crYear, $type) {
        $prevYear = $crYear - 1;
        $html = '<span class="separateYear" data-year="'. $prevYear .'">' . $prevYear . '</span>';
        for($i = 1; $i <= 12; $i++) {
        $html .= '<li class="btn btn-default getListTableBtn" data-month="'. $i .'" data-year="'. $prevYear .'" data-type="'. $type .'">'
                    .'<span>Tháng '. $i .'</span>'
                .'</li>';
        }
        echo $html;
    }
    
    /** @Author: NVDuong 31/07/18
     *  @Todo: Calculate final salary
     *  @Param: 
     **/
    public function actionCalTotalSalary() {
        $month          = isset($_GET['HrSalaryReports']['search_month']) ? $_GET['HrSalaryReports']['search_month'] : '';
        $year           = isset($_GET['HrSalaryReports']['search_year'])  ? $_GET['HrSalaryReports']['search_year'] : '';
        $aPositionWork  = UsersProfile::model()->getArrWorkRoom();
        $aProvince      = GasProvince::getArrAll();
        $date           = new DateTime();
        $date->setDate($year ,$month,1);
        $start_date     = $date->format('Y-m-d');
        $end_date       = $date->format('Y-m-t');
        $mSampleReport                  = new HrSalaryReports();
        $mSampleReport->type_id         = UsersHr::model()->getIdCalcSalary();
        $mSampleReport->start_date      = $start_date;
        $mSampleReport->end_date        = $end_date;
        $oldCodeNo                      = $mSampleReport->deleteSameFinal();
        $mSampleReport->created_by      = MyFormat::getCurrentUid();
        $mSampleReport->status          = HrSalaryReports::STT_CALCULATING;
        $mSampleReport->data            = null;
        // Keep old code no when calculate final report again
        $mSampleReport->code_no         = empty($oldCodeNo) ? $mSampleReport->generateCodeNo() : $oldCodeNo;
//        $mSampleReport->code_no         = $mSampleReport->generateCodeNo();
        // Init data for multiple position
        $mSampleReport->initData();
        $mSampleReport->isInitData      = true;
        $isInitData                     = false;
        $mInitData                      = null;
        $pct100                         = count($aPositionWork)*count($aProvince);
        $aDebtsMonthly                  = [];
        $model                          = '';
        foreach ($aPositionWork as $posId => $posName) {
            foreach ($aProvince as $proId => $proName) {
                $mSchedule = new HrWorkSchedule();
                $mSchedule->search_province = [$proId];
                $mSchedule->search_position = [$posId];
                $mSchedule->datePlanFrom    = MyFormat::dateConverYmdToDmy($start_date, 'd-m-Y');
                $mSchedule->datePlanTo      = MyFormat::dateConverYmdToDmy($end_date, 'd-m-Y');
                $arrUserHr                  = $mSchedule->getArrayUserHr();
                if(!empty($arrUserHr)){
//                  Progress bar
                    $posIndex = array_search($posId, array_keys($aPositionWork));
                    $proIndex = array_search($proId, array_keys($aProvince));
                    $percent    = intval(($posIndex*count($aProvince)+$proIndex+1)*100/$pct100);
                    $mSampleReport->flushData($percent);
//                  End progress bar
                    $model = new HrSalaryReports();
                    if($isInitData){
                        $model = $mInitData;
                        $model->setIsNewRecord(true);
                    }
                    $model->attributes          = $mSampleReport->attributes;
                    $aPosition                  = UsersProfile::model()->getArrWorkRoom();
                    $pos_name                   = isset($aPosition[$posId]) ? $aPosition[$posId] : "";
                    $reportName                 = 'Bảng lương của ' . $pos_name . ' từ ngày ' . MyFormat::dateConverYmdToDmy($model->start_date) . ' đến ngày ' . MyFormat::dateConverYmdToDmy($model->end_date);
                    $model->position_work       = $posId;
                    $model->province_id         = $proId;
                    $model->name                = $reportName;
                    if(!$isInitData){
                        $model->initData();
                        $mInitData              = $model;
                        $isInitData             = true;
                    }
                    $model->isInitData          = true;
                    ini_set('memory_limit', '-1');
                    $model->createReport($arrUserHr);
                    if($model->type_id == UsersHr::model()->getIdCalcSalary()){
                        $aDebtsMonthly += $model->aDebtsMonthly;
                    }
                    $model->save();
                }
            }
        }
        $mGasDebts = new GasDebts();
        $mGasDebts->createFromSalary($aDebtsMonthly, $model);
        $mReportSameCodeNo = $mSampleReport->getReportSameCodeNo();
        $this->renderHtmlReportData($mReportSameCodeNo);
    }
    
    /*
     * @author DuongNV 16/08/2018
     * Render report as html
     * @return html string
     */
    public function renderHtmlReportData($model, $isReturn = false) {
        $mFuncType  = new HrFunctionTypes();
        $aType      = $mFuncType->getArrayOrderedTypes();
        $typeName   = '';
        foreach ($aType as $t) {
            if($t->id == $model->type_id){
                $typeName = $t->name;break;
            }
        }
        $std        = MyFormat::dateConverYmdToDmy($model->start_date);
        $ed         = MyFormat::dateConverYmdToDmy($model->end_date);
        $title      = 'Bảng '.$typeName.' từ ngày '.$std.' đến ngày '.$ed;
        $this->layout = 'ajax';
        $_SESSION['data-excel'] = $model;
        $_SESSION['data-model'] = new HrSalaryReports();
        if($isReturn){
            return $this->renderPartial('_report_content',array(
                'title'     =>$title,
                'model'     =>$model,
            ), $isReturn);
        } else {
            $this->renderPartial('_report_content',array(
                'title'     =>$title,
                'model'     =>$model,
            ));
        }
    }
    
    /** @Author: DuongNV Mar 8,19
     *  @Todo: Sắp xếp thứ tự header bảng lương (tab thu nhập)
     **/
    public function sortHeaderReport(){
        if(isset($_GET['sort-header'])){
            $aPair = isset($_GET['aPair']) ? $_GET['aPair'] : [];
            if(!empty($aPair)){
                $mReport = new HrSalaryReports();
                $template = EmailTemplates::model()->findByPk($mReport->getIdTemplateHeader());
                $template->email_body = json_encode($aPair, JSON_FORCE_OBJECT);
                $template->save();
                echo 'Lưu thành công!';
                die;
            }
            echo 'Lưu thất bại!';
            die;
        }
    }
    
    /** @Author: DuongNV 2 Jul,2019
     *  @Todo: dc hiện trên bảng tổng hợp lương hay ko
     *  @params: $hide <=> is_show = null
     **/
    public function setShowInApprove() {
        $model = new HrSalaryReports();
        if( isset($_GET['showInApprove']) && $model->canSetShow()){
            $code_no    = isset($_GET['code_no']) ? $_GET['code_no'] : '';
            if(empty($code_no)) return;
            
            $criteria = new CDbCriteria;
            $criteria->compare('code_no', $code_no);
            $value    = empty($_GET['hide']) ? 1 : NULL;
            HrSalaryReports::model()->updateAll(array('is_show'=>$value), $criteria);
            $this->redirect(array('report'));
        }
    }
}
