<?php

class GasBreakTaskController extends AdminController 
{
    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id)
    {
        $this->pageTitle = 'Xem ';
        $this->layout='ajax';
        try{
            $model = $this->loadModel($id);
            if( !GasBreakTask::IsOwnerTask($model) ){
                $this->redirect(array('index'));
            }
//            $model->deadline = MyFormat::dateConverYmdToDmy($model->deadline);
            $mTaskDaily = new GasBreakTaskDaily('create');
            $mTaskDaily->break_task_id =  $model->id;
            $mTaskDaily->current_status =  $model->status;

        $this->render('view',array(
            'model' => $model,
            'mTaskDaily' => $mTaskDaily,
            'actions' => $this->listActionsCanAccess,
        ));
        }
        catch (Exception $exc)
        {
            GasCheck::CatchAllExeptiong($exc);
        }
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate()
    {
        $this->pageTitle = 'Tạo Mới ';
        try
        {
        $model=new GasBreakTask('create');

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if(isset($_POST['GasBreakTask']))
        {
            $model->attributes=$_POST['GasBreakTask'];
            $model->validate();            
            if(!$model->hasErrors()){
                    $model->save();
                    Yii::app()->user->setFlash( MyFormat::SUCCESS_UPDATE, "Thêm mới thành công." );
               $this->redirect(array('create'));
            }				
        }

        $this->render('create',array(
                'model'=>$model, 'actions' => $this->listActionsCanAccess,
        ));
        }
        catch (Exception $exc)
        {
            GasCheck::CatchAllExeptiong($exc);
        }
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id)
    {
        $this->pageTitle = 'Cập Nhật ';
        try
        {
        $model=$this->loadModel($id);
        $model->scenario = 'update';
        $model->deadline = MyFormat::dateConverYmdToDmy($model->deadline);
        $model->date_begin = MyFormat::dateConverYmdToDmy($model->date_begin);
        if(!GasCheck::CanUpdateBreakTask($model)){
            $this->redirect(array('index'));
        }

        if(isset($_POST['GasBreakTask']))
        {
            $model->attributes=$_POST['GasBreakTask'];
            $model->validate();
            if(!$model->hasErrors()){
                $model->save();
                Yii::app()->user->setFlash( MyFormat::SUCCESS_UPDATE, "Cập nhật thành công." );
                $this->redirect(array('update','id'=>$model->id));
            }							
        }

        $this->render('update',array(
                'model'=>$model, 'actions' => $this->listActionsCanAccess,
        ));
        }
        catch (Exception $exc)
        {
            GasCheck::CatchAllExeptiong($exc);
        }
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id)
    {
        try
        {
        if(Yii::app()->request->isPostRequest)
        {
                // we only allow deletion via POST request
                if($model = $this->loadModel($id))
                {
                    if($model->delete())
                        Yii::log("Uid: " .Yii::app()->user->id. " Delete record ".  print_r($model->attributes, true), 'info');
                }

                // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
                if(!isset($_GET['ajax']))
                        $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('index'));
        }
        else
        {
            Yii::log("Uid: " .Yii::app()->user->id. " Invalid request. Please do not repeat this request again.");
            throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
        }	
        }
        catch (Exception $exc)
        {
            GasCheck::CatchAllExeptiong($exc);
        }
    }

    /**
     * Manages all models.
     */
    public function actionIndex()
    {
        $this->pageTitle = 'Danh Sách ';
        try
        {
        $model=new GasBreakTask('search');
        $model->unsetAttributes();  // clear any default values
        if(isset($_GET['GasBreakTask']))
            $model->attributes=$_GET['GasBreakTask'];
        if(isset($_GET['status'])){
            $model->status = GasBreakTask::STATUS_CLOSE_BY_LEADER;
        }

        $this->render('index',array(
                'model'=>$model, 'actions' => $this->listActionsCanAccess,
        ));
        }
        catch (Exception $exc)
        {
            GasCheck::CatchAllExeptiong($exc);
        }
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
    * If the data model is not found, an HTTP exception will be raised.
     * @param integer the ID of the model to be loaded
     */
    public function loadModel($id)
    {
        try
        {
        $model=GasBreakTask::model()->findByPk($id);
        if($model===null)
        {
            Yii::log("The requested page does not exist.");
            throw new CHttpException(404,'The requested page does not exist.');
        }			
        return $model;
        }
        catch (Exception $exc)
        {
            GasCheck::CatchAllExeptiong($exc);
        }
    }

    /**
     * Performs the AJAX validation.
     * @param CModel the model to be validated
     */
    protected function performAjaxValidation($model)
    {
        try
        {
        if(isset($_POST['ajax']) && $_POST['ajax']==='gas-break-task-form')
        {
                echo CActiveForm::validate($model);
                Yii::app()->end();
        }
        }
        catch (Exception $exc)
        {
            GasCheck::CatchAllExeptiong($exc);
        }
    }
    
    /**
     * @Author: ANH DUNG Dec 14, 2014
     * @Todo: user update Báo Cáo Công Việc Hàng Ngày
     */
    public function actionUpdate_report_daily($id)
    {
        $this->layout='ajax';
        try{
            $model = $this->loadModel($id);
//            $model->deadline = MyFormat::dateConverYmdToDmy($model->deadline);
            if( !GasBreakTask::IsOwnerTask($model) ){
                $this->redirect(array('index'));
            }
            $mTaskDaily = new GasBreakTaskDaily('create');
            $mTaskDaily->break_task_id =  $model->id;
            $mTaskDaily->current_status =  $model->status;
            $this->handlePostReport($model, $mTaskDaily, "Gửi báo cáo thành công.");
            $this->render('view',array(
                'model' => $model,
                'mTaskDaily' => $mTaskDaily,
                'actions' => $this->listActionsCanAccess,
            ));
        }
        catch (Exception $exc)
        {
            GasCheck::CatchAllExeptiong($exc);
        }
    }
    
    /**
     * @Author: ANH DUNG Dec 14, 2014
     * @Todo: xử lý lưu báo cáo khi gửi
     * @Param: $model model GasBreakTask
     * @Param: $mTaskDaily model GasBreakTaskDaily
     */
    public function handlePostReport($model, $mTaskDaily, $msg) {
        if(isset($_POST['GasBreakTaskDaily'])){
            $mTaskDaily->attributes = $_POST['GasBreakTaskDaily'];            
            $mTaskDaily->validate();
            $mGasFile = new GasFile();
            $mGasFile->validateFile($mTaskDaily);
            if(!$mTaskDaily->hasErrors()){
                if(!GasTickets::UserCanPostTicket()) { // dùng chung kiểm tra với ticket (ngại tách, mà thấy cũng ko thể post vượt 200 message 1 ngày dc )
                    $msg = "Có Lỗi không thể Gửi báo cáo vượt quá giới hạn cho phép (exceed limit), liên hệ với admin quản trị.";
                    throw new Exception($msg);
                }
                $mTaskDaily->save();
                $mTaskDaily = GasBreakTaskDaily::model()->findByPk($mTaskDaily->id);
                $mGasFile->saveRecordFile($mTaskDaily, GasFile::TYPE_1_BREAK_TASK);
                // update back to root model
                $model->status = $mTaskDaily->current_status;
                $model->update(array('status'));
                Yii::app()->user->setFlash( MyFormat::SUCCESS_UPDATE, $msg );
                $this->redirect(array('view','id'=>$model->id));
            }            
        }
    }
    
    /**
     * @Author: ANH DUNG Dec 14, 2014
     * @Todo: user post ý kiến chỉ đạo của cấp trên cho Báo Cáo Công Việc Hàng Ngày
     */
    public function actionPost_leader_help_content($id)
    {
        $this->layout='ajax';
        try{
            $model = $this->loadModel($id);
//            $model->deadline = MyFormat::dateConverYmdToDmy($model->deadline);            
            $mTaskDaily = new GasBreakTaskDaily('create_leader_help');
            $mTaskDaily->break_task_id =  $model->id;
            $mTaskDaily->current_status =  $model->status;
            $this->handlePostReport($model, $mTaskDaily, "Gửi Ý Kiến Chỉ Đạo thành công.");
            
            $this->render('view',array(
                'model' => $model,
                'mTaskDaily' => $mTaskDaily,
                'actions' => $this->listActionsCanAccess,
            ));
        }
        catch (Exception $exc)
        {
            GasCheck::CatchAllExeptiong($exc);
        }
    }
    
    /**
     * @Author: ANH DUNG Dec 14, 2014
     * @Todo: xử lý lưu Ý Kiến Chỉ Đạo
     * @Param: $model model GasBreakTask
     * @Param: $mTaskDaily model GasBreakTaskDaily
     */
    public function HandlePostLeaderHelp($model, $mTaskDaily) {
        return ; // khong can dung den cai nay
        if(isset($_POST['GasBreakTaskDaily'])){
            $mTaskDaily->attributes = $_POST['GasBreakTaskDaily'];            
            $mTaskDaily->validate();
            if(!$mTaskDaily->hasErrors()){
                $mTaskDaily->save();
                Yii::app()->user->setFlash( MyFormat::SUCCESS_UPDATE, "Gửi báo cáo thành công." );
                $this->redirect(array('view','id'=>$model->id));
            }            
        }
    }
    
    
    /**
     * @Author: ANH DUNG Dec 14, 2014
     * @Todo: Delete one row of Báo Cáo Công Việc Hàng Ngày
     */
    public function actionDelete_report_daily($id)
    {        
        $this->layout='ajax';
        try{
            $mTaskDaily = MyFormat::loadModelByClass($id, 'GasBreakTaskDaily');
            $break_task_id =  $mTaskDaily->break_task_id;
            $mTaskDaily->delete();
            Yii::app()->user->setFlash( MyFormat::SUCCESS_UPDATE, "Xóa thành công." );
            $this->redirect(array('view','id' => $break_task_id));
        }
        catch (Exception $exc)
        {
            GasCheck::CatchAllExeptiong($exc);
        }
    }
    
    
}
