<?php

class GasremainController extends AdminController 
{
    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id)
    {
        $this->layout='ajax';
        $this->pageTitle = 'Xem ';
        try{
            $this->render('view',array(
                    'model'=>$this->loadModel($id), 'actions' => $this->listActionsCanAccess,
            ));
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate()
    {
        $this->pageTitle = 'Tạo Mới ';
        try{
            $model=new GasRemain('create');
            if(isset($_POST['GasRemain'])){
                $model->attributes=$_POST['GasRemain'];
                $model->validate();
                if(!$model->hasErrors()){
                    GasRemain::saveRemain($model);
                    Yii::app()->user->setFlash('successUpdate', "Thêm mới thành công.");
                   $this->redirect(array('create'));
                }				
            }

            $this->render('create',array(
                    'model'=>$model, 'actions' => $this->listActionsCanAccess,
            ));
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id)
    {
        try{
            $model=$this->loadModel($id);
            $model->modelOld = clone $model;
            if(!$model->canUpdate()){
                $this->redirect(array('index'));
            }
            $this->pageTitle = "STT $model->seri - cập nhật";
            $model->updateFormatData();
            if(isset($_POST['GasRemain'])){
                $model->attributes=$_POST['GasRemain'];
                $model->validate();
                if(!$model->hasErrors()){
                    $model->updateWeb();
                    $model->updateBackAppOrder();
                    $model->saveLogUpdate(LogUpdate::TYPE_4_GAS_REMAIN);
                    Yii::app()->user->setFlash('successUpdate', 'Cập nhật thành công');
                    $this->redirect(array('update','id'=>$model->id));
                }
            }

            $this->render('update',array(
                    'model'=>$model, 'actions' => $this->listActionsCanAccess,
            ));
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id)
    {
        try{
            if(Yii::app()->request->isPostRequest)
            {
                // we only allow deletion via POST request
                if($model = $this->loadModel($id))
                {
                    if($model->delete())
                        Yii::log("Uid: " .Yii::app()->user->id. " Delete record ".  print_r($model->attributes, true), 'info');
                }

                // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
                if(!isset($_GET['ajax']))
                        $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('index'));
            }
            else
            {
                Yii::log("Uid: " .Yii::app()->user->id. " Invalid request. Please do not repeat this request again.");
                throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
            }	
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }

    /**
     * Manages all models.
     */
    public function actionIndex()
    {
        $this->pageTitle = 'Gas dư';
        try{
            $model=new GasRemain('search');
            $model->unsetAttributes();  // clear any default values
            if(isset($_GET['GasRemain']))
                $model->attributes=$_GET['GasRemain'];

            $this->render('index',array(
                    'model'=>$model, 'actions' => $this->listActionsCanAccess,
            ));
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
    * If the data model is not found, an HTTP exception will be raised.
     * @param integer the ID of the model to be loaded
     */
    public function loadModel($id)
    {
        try
        {
            $model=GasRemain::model()->findByPk($id);
            if($model===null)
            {
                Yii::log("The requested page does not exist.");
                throw new CHttpException(404,'The requested page does not exist.');
            }			
            return $model;
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }

    /**
     * Performs the AJAX validation.
     * @param CModel the model to be validated
     */
    protected function performAjaxValidation($model)
    {
        try{
            if(isset($_POST['ajax']) && $_POST['ajax']==='gas-remain-form'){
                echo CActiveForm::validate($model);
                Yii::app()->end();
            }
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }
    
    /**
     * @Author: ANH DUNG Feb 26, 2014
     * @Todo: cập nhật trạng thái sau khi về kho cân lại
     * @Param: $mUser model user
     * @Return: full name with salution of user
     */    
    public function actionUpdate_status($id)
    {
        try{
            $model=GasRemain::model()->findByPk($id);
            if($model===null)
            {
                Yii::log("The requested page does not exist.");
                throw new CHttpException(404,'The requested page does not exist.');
            }
            return $model;
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }
    
        
    /**
     * @Author: ANH DUNG Aug 05, 2016
     * @Todo: ExportExcel
     */
    public function actionExportExcel(){
        try{
        if(isset($_SESSION['data-excel'])){
            ini_set('memory_limit','1000M');
            $mExportList = new ExportList();
            $mExportList->gasRemain($_SESSION['data-excel']->data, new GasRemain());
//            ExportList::GasRemain();
        }
        $this->redirect(array('index'));
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }
    
}
