<?php

class InventoryReportController extends AdminController 
{
    /** @Author: HOANG NAM 31/01/2018
     *  @Todo:  Báo cáo vỏ tồn kho
     *  @Code: NAM006
     **/
    public function actionReportVo()
    {
        $this->pageTitle = 'Tồn kho vỏ KH bò mối';
        try{
        $aData=[];
        $model;
        $this->getDataModel($aData,$model);
        $this->render('ReportVo',array(
            'model'=>$model, 'actions' => $this->listActionsCanAccess,
            'aData'=>$aData,
        ));
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }
    
    /** @Author: HOANG NAM 31/01/2018
     *  @Todo: Lấy danh sách data cần được in
     *  @Code: NAM006
     **/
   public function getDataModel(&$aData,&$model){
       if(isset($_GET['type']) && $_GET['type']==InventoryCustomer::VIEW_DATE):
           $this->getUsersReport($aData,$model);
       else:
            $this->getInventoryCustomer($aData,$model);
       endif;
   }
   
   /** @Author: HOANG NAM 31/01/2018
    *  @Todo: get model InventoryCustomer
    *  @Code: NAM006
    **/
   public function getInventoryCustomer(&$aData,&$model){
        $_GET['type'] = InventoryCustomer::VIEW_COUNT;
        $model = new InventoryCustomer();
        $model->unsetAttributes();  // clear any default values
        $model->date_from   = '01-'.date('m-Y');
        $model->date_to     = date('d-m-Y');
        if(isset($_GET['InventoryCustomer'])):
            $this->setAttributes($model,'InventoryCustomer');
            $model->date_from   = isset($_GET['InventoryCustomer']['date_from']) ? $_GET['InventoryCustomer']['date_from'] : null;
            $model->date_to   = isset($_GET['InventoryCustomer']['date_to']) ? $_GET['InventoryCustomer']['date_to'] : null;
            $aData  =   $model->inventorySearch();
            if(isset($aData['ID_VO']) && $model->canToExcel()){
                ToExcel::ReportDebitVo($aData, $model);
            }
        endif;
   }
   
   /** @Author: HOANG NAM 31/01/2018
    *  @Todo: get model InventoryCustomer
    *  @Code: NAM006
    **/
    public function getUsersReport(&$aData,&$model){
        $_GET['type'] = InventoryCustomer::VIEW_DATE;
        $model = new Users();
        $model->unsetAttributes();  // clear any default values
        if(isset($_GET['Users'])):
            $this->setAttributes($model,'Users');
            $model->is_maintain     = $model->type_customer;
            $model->agent_id_search = $model->agent_id;
            $model->role_id         = ROLE_CUSTOMER;
            $model->type            = CUSTOMER_TYPE_STORE_CARD;
            $aData  =   $model->searchBuyLarger15();
        endif;
   }
   
   /** @Author: HOANG NAM 01/02/2018
    *  @Todo: set attributes
    *  @Code: NAM006
    **/
    public function setAttributes(&$model,$nameModel){
        $model->countVo         = isset($_GET[$nameModel]['countVo']) ? $_GET[$nameModel]['countVo'] : $model->countVo;
        $model->agent_id        = isset($_GET[$nameModel]['agent_id']) ? $_GET[$nameModel]['agent_id'] : null;
        $model->type_customer   = isset($_GET[$nameModel]['type_customer']) ? $_GET[$nameModel]['type_customer'] : null;
        $model->customer_id     = isset($_GET[$nameModel]['customer_id']) ? $_GET[$nameModel]['customer_id'] : null;
        $model->province_id     = isset($_GET[$nameModel]['province_id']) ? $_GET[$nameModel]['province_id'] : null;
    }
}

