<?php

class GasSupportCustomerController extends AdminController 
{
    protected function beforeAction($action) {
//        $cRole = Yii::app()->user->role_id;
//        if($cRole != ROLE_ADMIN){
//            GasCheck::FunctionMaintenance();
//        }
        $this->adminPageTitle = "Phiếu Giao Nhiệm Vụ";
        return parent::beforeAction($action);
    }
    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id)
    {
        $this->pageTitle = "Xem $this->adminPageTitle";
        $this->layout='ajax';
        try{
             $model = $this->loadModel($id);
             $model->mGasFile = new GasFile();
            if( !GasSupportCustomer::IsOwner($model) ){
                $this->redirect(array('index'));
            }
        $model->scenario = "Update_status";
        
        $model->aModelDetail = $model->rDetail;
        $this->render('view',array(
                'model'=> $model, 'actions' => $this->listActionsCanAccess,
        ));
        }
        catch (Exception $exc)
        {
            GasCheck::CatchAllExeptiong($exc);
        }
    }
    
    /**
     * @Author: ANH DUNG Sep 02, 2015
     */
    public function actionPrint($id)
    {
        $this->pageTitle = "Print $this->adminPageTitle";
        try{
            $model = $this->loadModel($id);
            if( !$model->canPrint() ){
                $this->redirect(array('index'));
            }
        $this->render('print',array(
                'model'=> $model, 'actions' => $this->listActionsCanAccess,
        ));
        }
        catch (Exception $exc)
        {
            GasCheck::CatchAllExeptiong($exc);
        }
    }
    
    /**
     * @Author: ANH DUNG Dec 27, 2014
     * @Todo: for user approve 1, 2
     * @Param: $id
     */
    public function actionUpdate_status($id)
    {
        $this->pageTitle = "Xem $this->adminPageTitle";
        $this->layout='ajax';
        try{
             $model = $this->loadModel($id);
             $model->mGasFile = new GasFile();
             $model->scenario = "Update_status";
            if( !GasSupportCustomer::IsOwner($model) && !GasSupportCustomer::CanUpdateStatus($model)){
                $this->redirect(array('index'));
            }
            $this->HandlePost($model);
            $model->aModelDetail = $model->rDetail;
        $this->render('view',array(
                'model'=> $model, 'actions' => $this->listActionsCanAccess,
        ));
        }
        catch (Exception $exc)
        {
            GasCheck::CatchAllExeptiong($exc);
        }
    }
    
    /**
     * @Author: ANH DUNG Dec 27, 2014
     */
    public function HandlePost($model) {
        if(isset($_POST['GasSupportCustomer'])){
            $model->attributes=$_POST['GasSupportCustomer'];
            $model->validate();
            $mGasFile = new GasFile();
            $mGasFile->validateFile($model);
            if(!$model->hasErrors()){
                GasSupportCustomer::HandleUpdateStatus($model);
                if(isset($_POST['i']) && in_array($_POST['i'], array(2,3,4))){
                    $model->UpdaetPercentDetailItem($_POST['i']);
                }
                $model->deleteFile();
                $mGasFile->saveRecordFile($model, GasFile::TYPE_4_SUPPORT_CUSTOMER_COMPLETE);
                Yii::app()->user->setFlash( MyFormat::SUCCESS_UPDATE, "Cập nhật trạng thái thành công." );
                $this->redirect(array('view','id'=>$model->id));
            }
            $mOld = $this->loadModel($model->id);
            $model->status = $mOld->status;
        }
    }
    
    /** Sep 16, 2015
     *  1. NV kỹ thuật tạo: danh mục thiết bị, file, KH => send mail
     *  2. NV sale vào cập nhật thông tin còn lại => send mail
     *  3. TP bò, mối + GD KD + Tổng GD duyệt - view có thể chỉnh sửa lại % của từng item
     *  Required: lưu lại % cho từng lần duyệt
     * 
     */
    public function actionCreate()
    {
        try{
        $model=new GasSupportCustomer('sub_agent_create');// NV kỹ thuật tạo ban đầu, sau đó sale vào cập nhật
        $this->setScenarioCreate($model);
        $model->mDetail = new GasSupportCustomerItem();
        $model->mGasFile = new GasFile();
        $model->mapInfoRequest();
        if(isset($_POST['GasSupportCustomer']))
        {
            $model->attributes=$_POST['GasSupportCustomer'];
            $model->mDetail->attributes = $_POST['GasSupportCustomerItem'];
            $model->setUserApproveHgd();
            $model->validate();
            $model->checkUploadFile();
            GasFile::ValidateFilePdf($model);
            if(!$model->hasErrors()){
                $model->save();
                $model->updateBackIdToRequest();
                $model->mGasFile->saveRecordFile($model, GasFile::TYPE_2_SUPPORT_CUSTOMER);
                Yii::app()->user->setFlash( MyFormat::SUCCESS_UPDATE, "Thêm mới thành công." );
                $this->redirect(array('create'));
            }
        }
        $this->hgdTitle($model);
        $this->pageTitle = "Tạo Mới $this->adminPageTitle";

        $this->render('create',array(
            'model'=>$model, 'actions' => $this->listActionsCanAccess,
        ));
        }
        catch (Exception $exc)
        {
            GasCheck::CatchAllExeptiong($exc);
        }
    }
    
    public function setScenarioCreate($model){
        $cRole = Yii::app()->user->role_id;
        $cUid = Yii::app()->user->id;
        // Sep 29, 2015 cho phép gần như tất cả các loại user có thể tạo dc GiaoNV, chỉ có tech_create tạo thì có thể khác giao diện
        if(in_array($cRole, GasSupportCustomer::$aRoleNewCreate)){
            $model->scenario = 'tech_create';
        }elseif(in_array($cRole, GasSupportCustomer::$aRoleCreateExt1)){
            $model->scenario = 'ccs_create';
//            $model->date_request = date("Y-m-d");
            $model->type_support = GasSupportCustomer::TYPE_SUPPORT_HGD;
        }
    }
    
    public function setScenarioUpdate($model){
        $cRole = Yii::app()->user->role_id;
        $cUid = Yii::app()->user->id;
        // Sep 29, 2015 cho phép gần như tất cả các loại user có thể tạo dc GiaoNV, chỉ có tech_create tạo thì có thể khác giao diện
        $model->scenario = 'sub_agent_update';
        if(in_array($cRole, GasSupportCustomer::$aRoleNewCreate)){
            $model->scenario = 'tech_update';
        }elseif(in_array($cRole, GasSupportCustomer::$aRoleCreateExt1)){
            $model->scenario = 'ccs_update';
        }elseif($model->isScenarioSaleUpdate()){
            $model->scenario = 'sale_update';
//        }elseif(in_array($cRole, GasSupportCustomer::$aRoleAllowCreate)){
//            $model->scenario = 'sub_agent_update'; // Close Sep 29, 2015 vì sẽ cho tất cả các user có thể tạo và update
        }elseif( $model->CanUpdateTimeDoingReal() ){
            $model->scenario = 'head_update_time_real';// Tổ trưởng bảo trì cập nhật ngày giờ thi công thực tế
        }
//        elseif(!in_array($cRole, GasSupportCustomer::$aRoleNewCreate)){
//            $model->scenario = 'sub_agent_update';
//        }// Oct 06, 2015 cảm thấy có thể closse đoạn if này lại
    }    

    public function actionUpdate($id)
    {
        try{
        $model=$this->loadModel($id);
        $this->setScenarioUpdate($model);
        $model->date_request = MyFormat::dateConverYmdToDmy($model->date_request);
        $model->mDetail = new GasSupportCustomerItem();
        $model->aModelDetail = $model->rDetail;
        $model->mGasFile = new GasFile();
        if(!GasCheck::CanUpdateSupportCustomer($model)){
            $this->redirect(array('index'));
        }
        $this->HandleTechSendMail($model);
        
//        if( $model->canUpdateToPrint()){
//            $model->scenario = 'update_to_print';
//        }
        $model->time_doing = MyFormat::datetimeDbDatetimeUser($model->time_doing);
        $model->time_doing_real = MyFormat::datetimeDbDatetimeUser($model->time_doing_real);
        $mGasFile = new GasFile();
        
        if(isset($_POST['GasSupportCustomer']))
        {
            $model->attributes=$_POST['GasSupportCustomer'];
            $model->validate();
            GasFile::ValidateFilePdf($model);
            if(!$model->hasErrors()){
                if($model->scenario == 'head_update_time_real'){
                    $model->HandleUpdateTimeReal();
                }else{
                    $model->update();
                }
                $model->deleteFile();
                $mGasFile->saveRecordFile($model, GasFile::TYPE_2_SUPPORT_CUSTOMER);
                $this->HandleForSaleUpdate($model);
                Yii::app()->user->setFlash( MyFormat::SUCCESS_UPDATE, "Cập nhật thành công." );
                $this->redirect(array('update','id'=>$model->id));
            }
        }
        $this->hgdTitle($model);
        $this->pageTitle = "Cập Nhật $this->adminPageTitle";
        $this->render('update',array(
                'model'=>$model, 'actions' => $this->listActionsCanAccess,
        ));
        }
        catch (Exception $exc)
        {
            GasCheck::CatchAllExeptiong($exc);
        }
    }
    
    /**
     * @Author: ANH DUNG Oct 14, 2015
     * @Todo: xử lý tech send mail cho Sale
     */
    public function HandleTechSendMail($model) {
        if(isset($_POST['ajax_update_send_mail'])){
            $model->TechSendMail();
            die;
        }
    }
    
    /**
     * @Author: ANH DUNG Sep 18, 2015
     * @Todo: handle for sale update %
     */
    public function HandleForSaleUpdate($model) {
        if($model->scenario == 'sale_update'){ // chỉ có sale update % lần đầu
            $model->UpdaetPercentDetailItem(1);
        }
    }

    public function actionDelete($id)
    {
        try
        {
        if(Yii::app()->request->isPostRequest)
        {
                // we only allow deletion via POST request
                if($model = $this->loadModel($id))
                {
                    if($model->delete())
                        Yii::log("Uid: " .Yii::app()->user->id. " Delete record ".  print_r($model->attributes, true), 'info');
                }

                // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
                if(!isset($_GET['ajax']))
                    $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('index'));
        }
        else
        {
            Yii::log("Uid: " .Yii::app()->user->id. " Invalid request. Please do not repeat this request again.");
            throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
        }	
        }
        catch (Exception $exc)
        {
            GasCheck::CatchAllExeptiong($exc);
        }
    }

    public function actionIndex()
    {
//        $this->redirect(Yii::app()->createAbsoluteUrl('admin/site/index'));
        $this->pageTitle = $this->adminPageTitle;
        try
        {
        $model=new GasSupportCustomer('search');
        $model->unsetAttributes();  // clear any default values
        $model->InitMaterial();
        if(isset($_GET['all']) ){ // xem cho search hết tất cả NGHIA 5/7/2018
            $model->all = 1;
        }
        if(isset($_GET['GasSupportCustomer']))
            $model->attributes=$_GET['GasSupportCustomer'];
        if(isset($_GET['status']) ){
            $model->status = (int)$_GET['status'];
        }
        $model->type_support = GasSupportCustomer::TYPE_SUPPORT_BO_MOI;
        
        if(isset($_GET['schedule'])){
            $model->schedule = (int)$_GET['schedule'];
            $this->render('index_time_doing_real',array(
                'model'=>$model, 'actions' => $this->listActionsCanAccess,
            ));
            die;
        }
        $this->exportExcel($model);
        $this->render('index',array(
                'model'=>$model, 'actions' => $this->listActionsCanAccess,
        ));
        }
        catch (Exception $exc)
        {
            GasCheck::CatchAllExeptiong($exc);
        }
    }

    public function loadModel($id)
    {
        try
        {
        $model=GasSupportCustomer::model()->findByPk($id);
        if($model===null)
        {
            Yii::log("The requested page does not exist.");
            throw new CHttpException(404,'The requested page does not exist.');
        }			
        return $model;
        }
        catch (Exception $exc)
        {
            GasCheck::CatchAllExeptiong($exc);
        }
    }

    /**
     * @Author: ANH DUNG Jul 23, 2016
     */
    public function hgdTitle($model) {
        if($model->type_support == GasSupportCustomer::TYPE_SUPPORT_HGD){
            $this->adminPageTitle = "Đầu tư hộ gia đình";
        }
    }
    
    public function actionHgd()
    {
        try
        {
        $model=new GasSupportCustomer();
        $model->unsetAttributes();  // clear any default values
        $model->InitMaterial();
        if(isset($_GET['GasSupportCustomer'])){
            $model->attributes=$_GET['GasSupportCustomer'];
        }
        if(isset($_GET['status'])){
            $model->status = (int)$_GET['status'];
        }
        $model->type_support = GasSupportCustomer::TYPE_SUPPORT_HGD;
        $this->hgdTitle($model);
        $this->pageTitle = "Hộ gia đình - ".$this->adminPageTitle;
        
        $this->render('hgd/hgd',array(
                'model'=>$model, 'actions' => $this->listActionsCanAccess,
        ));
        }
        catch (Exception $exc)
        {
            GasCheck::CatchAllExeptiong($exc);
        }
    }
    
    /**
     * @Author: PHAM THANH NGHIA 29/06/2018
     * @Todo: ExportExcel
     */
    public function exportExcel($model){
        try{
            if(!$model->canExportExcel() || !isset($_GET['ExportExcel'])){
                return ;
            }
        if(isset($_SESSION['data-excel'])){
            ini_set('memory_limit','2000M');// chưa cần chạy đoạn này, đưa memory_limit lên 500 có thể export dc khoảng 5000 row - chưa test
            $mExportList = new ExportList();
            $mExportList->exportGasSupportCustomer();
        }
        $this->redirect(array('index'));
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }
    
}
