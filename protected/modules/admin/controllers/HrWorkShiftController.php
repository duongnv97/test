<?php

class HrWorkShiftController extends AdminController 
{
    public $pluralTitle = "Ca làm việc";
    public $singleTitle = "Ca làm việc";
    
    public function actionIndex()
    {
        $this->pageTitle = 'Danh sách ca làm việc';
        try{
            $model=new HrWorkShift('search');
            $model->unsetAttributes();
            if(isset($_GET['HrWorkShift'])) {
                $model->attributes = $_GET['HrWorkShift'];
            }
            $this->render('index',array(
                    'model'=>$model, 'actions' => $this->listActionsCanAccess,
            ));
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }
    public function actionView($id)
    {
        $this->pageTitle = 'Xem ca làm việc';
        try{
                $this->render('view',array(
                    'model'=>$this->loadModel($id), 'actions' => $this->listActionsCanAccess,
            ));
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }
    public function actionCreate()
    {
        $this->pageTitle = 'Tạo Mới ca làm việc';
        try{
            $model=new HrWorkShift('create');
            if(isset($_POST['HrWorkShift'])){
                $model->attributes = $_POST['HrWorkShift'];
                $model->status = 1;

                $model->validate();
                if(!$model->hasErrors()){
                    $model->save();
                    Yii::app()->user->setFlash( MyFormat::SUCCESS_UPDATE, "Thêm mới thành công." );
                    //$this->redirect(array('view','id'=>$model->id));
                }
            }

            $this->render('create',array(
                    'model'=>$model, 'actions' => $this->listActionsCanAccess,
            ));
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }
    public function actionUpdate($id)
    {
        try{
        $model=$this->loadModel($id);
        if(!empty($_GET['layout'])){
            $this->layout = 'ajax';
        }
        if(isset($_POST['HrWorkShift'])){
            $model->attributes=$_POST['HrWorkShift'];
            $model->validate();
            if(!$model->hasErrors()){
                $model->update();
                Yii::app()->user->setFlash( MyFormat::SUCCESS_UPDATE, "Cập nhật thành công." );
            }
        }

        $this->render('update',array(
                'model'=>$model, 'actions' => $this->listActionsCanAccess,
        ));
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }
    public function actionDelete($id)
    {
        throw new Exception('Admin lock Oct2618');
        try{
        if(Yii::app()->request->isPostRequest)
        {
            // we only allow deletion via POST request
            if($model = $this->loadModel($id))
            {
                if($model->delete()){
                    Yii::log("Uid: " .Yii::app()->user->id. " Delete record ".  print_r($model->attributes, true), 'info');
                }
            }

            // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
            if(!isset($_GET['ajax']))
                $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('index'));
        }
        else
        {
            Yii::log("Uid: " .Yii::app()->user->id. " Invalid request. Please do not repeat this request again.");
            throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
        }
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }
    public function loadModel($id)
    {
        try{
            $model = HrWorkShift::model()->findByPk($id);
            if($model===null)
            {
                Yii::log("The requested page does not exist.");
                throw new CHttpException(404,'The requested page does not exist.');
            }			
            return $model;
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }
}
