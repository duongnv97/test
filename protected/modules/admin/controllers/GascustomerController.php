<?php

class GascustomerController extends AdminController
{   
    public function actionImport_customer() {

        $model=new Users('import_customer');
        if(isset($_POST['Users']))
        {
            $model->attributes=$_POST['Users'];
            $model->file_excel=$_FILES['Users'];
            $model->validate();
            if(!$model->hasErrors()){
                $this->importExcel($model);
                $this->redirect(array('index','id'=>$model->id));
            }
        }
        $this->render('import_customer',array(
                'model'=>$model, 'actions' => $this->listActionsCanAccess,
        ));
    }

    public function importExcel($model){
            Yii::import('application.extensions.vendors.PHPExcel',true);
            $objReader = PHPExcel_IOFactory::createReader("Excel2007");
            $objPHPExcel = $objReader->load(@$_FILES['Users']['tmp_name']['file_excel']);
//                $objWorksheet = $objPHPExcel->getActiveSheet();
            $objWorksheet = $objPHPExcel->getSheet(0);
            $highestRow = $objWorksheet->getHighestRow(); // e.g. 10
            //$highestColumn = $objWorksheet->getHighestColumn(); // e.g 'F'
            $highestColumn = 'J';
            $highestColumnIndex = PHPExcel_Cell::columnIndexFromString($highestColumn);

            $success = 1;
            $aRowInsert=array();
            $countRowSuccess = 0;
            $countRowError = 0;
            $aCellError = array();
            $aRowError = array();       
            $created_date=date('Y-m-d H:i:s');
            $role_id=ROLE_CUSTOMER;
            $application_id=BE;
            for ($row = 2; $row <= $highestRow; ++$row)
            {
                // file excel cần format column theo dạng text hết để người dùng nhập vào khi đọc không lỗi
//                    $cDate = MyFormat::formatDateDb(trim($objWorksheet->getCellByColumnAndRow(0, $row)->getValue()));
                $code_account = mysql_real_escape_string(trim($objWorksheet->getCellByColumnAndRow(0, $row)->getValue()));
//                    $code_bussiness = mysql_real_escape_string(trim($objWorksheet->getCellByColumnAndRow(2, $row)->getValue()));
                $first_name = mysql_real_escape_string($objWorksheet->getCellByColumnAndRow(1, $row)->getValue());
                $name_agent = mysql_real_escape_string($objWorksheet->getCellByColumnAndRow(2, $row)->getValue());
                $address = mysql_real_escape_string($objWorksheet->getCellByColumnAndRow(3, $row)->getValue());
                $province_id = trim($objWorksheet->getCellByColumnAndRow(4, $row)->getValue());
                $channel_id = trim($objWorksheet->getCellByColumnAndRow(5, $row)->getValue());
                $district_id    = trim($objWorksheet->getCellByColumnAndRow(6, $row)->getValue());
                $payment_day    = trim($objWorksheet->getCellByColumnAndRow(7, $row)->getValue());
                $sale_id    = trim($objWorksheet->getCellByColumnAndRow(8, $row)->getValue());
                                    $beginning    = trim($objWorksheet->getCellByColumnAndRow(9, $row)->getValue());                   

                $aRowInsert[]="('$code_account',
                    '$name_agent',
                    '$first_name',
                    '$address',
                    '$province_id',
                    '$channel_id',
                    '$sale_id',
                    '$district_id',
                    '$payment_day',
                                            '$beginning',						
                    '$role_id',
                    '$application_id',
                    '$created_date'    
                    )";
            }

        $tableName = Users::model()->tableName();
        $sql = "insert into $tableName (code_account,
                        name_agent,
                        first_name,
                        address,
                        province_id,
                        channel_id,
                        sale_id,
                        district_id,
                        payment_day,
                                                    beginning,
                        role_id,
                        application_id,
                        created_date
                        ) values ".implode(',', $aRowInsert);

        Yii::app()->db->createCommand($sql)->execute();                
    }

    public function actionView($id)
    {
        $this->pageTitle = 'Xem Khách Hàng';
        try{
            $this->layout = 'ajax';
            $model = $this->loadModel($id);
            if(!Users::CanAccessProvince($model->province_id)){
                $this->redirect(array('customer_store_card'));
            }
            
            $this->render('view',array(
                    'model'=> $model, 'actions' => $this->listActionsCanAccess,
            ));
            }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate()
    {
        $this->pageTitle = 'Tạo Mới Khách Hàng';
        if(isset($_GET['ajax']) && $_GET['ajax']==1){
            $this->getSltDistrict();
        }

        try{
        $model=new Users('create_customer');
        if(isset($_POST['Users']))
        {
            $model->attributes=$_POST['Users'];
            $model->role_id = ROLE_CUSTOMER;
            if($model->save())
                $this->redirect(array('view','id'=>$model->id));
        }

        $this->render('create',array(
                'model'=>$model, 'actions' => $this->listActionsCanAccess,
        ));
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id)
    {        
        try
            {
            $model=$this->loadModel($id);
            $this->pageTitle = 'Cập Nhật Khách Hàng - '.$model->first_name;
            $model->scenario = 'update_customer';
            if($model->role_id!=ROLE_CUSTOMER)
                $this->redirect(array('index'));

            if(isset($_POST['Users']))
            {
                    $model->attributes=$_POST['Users'];
                    // $model->role_id = ROLE_CUSTOMER;
                    $aUpdate = array('code_bussiness','code_account','name_agent','beginning','first_name','address','province_id','district_id',
                    'sale_id','payment_day','gender','phone','channel_id','status');		
                    $model->validate();	
                    if(!$model->hasErrors()){
                            $model->update($aUpdate);
                            $this->redirect(array('view','id'=>$model->id));
                    }

            }

            $this->render('update',array(
                    'model'=>$model, 'actions' => $this->listActionsCanAccess,
            ));
            }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }

    public function getSltDistrict(){

        $province_id = (int)$_GET['province_id'];
        $aDis = GasDistrict::getArrAll($province_id);
        $html_district='';
        if(count($aDis)>0){
            $html_district.='<option value="">Select Quận </option>';
            foreach($aDis as $key=>$item){
                $selected = '';
//                    if($_GET['catId']==$key)
//                        $selected = 'selected="selected"';
                $html_district.='<option value="'.$key.'" '.$selected.'>'.$item.'</option>';
            }
        }
        else
            $html_district.='<option value="">Select Quận</option>';

        $aStore = GasStorehouse::getArrAll($province_id);
        $html_store='';
        if(count($aStore)>0){
            $html_store.='<option value="">Select Chi Nhánh</option>';
            foreach($aStore as $key=>$item){
                $selected = '';
//                    if($_GET['catId']==$key)
//                        $selected = 'selected="selected"';
                $html_store.='<option value="'.$key.'" '.$selected.'>'.$item.'</option>';
            }
        }
        else
            $html_store.='<option value="">Select Chi Nhánh</option>';
        $json = CJavaScript::jsonEncode(array('html_district'=>$html_district,'html_store'=>$html_store));            
        echo $json;die;
    }        

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id)
    {
            try
            {
            if(Yii::app()->request->isPostRequest)
            {
                    // we only allow deletion via POST request
                    if($model = $this->loadModel($id))
                    {
                        if($model->delete())
                            Yii::log("Uid: " .Yii::app()->user->id. " Delete record ".  print_r($model->attributes, true), 'info');
                    }

                    // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
                    if(!isset($_GET['ajax']))
                            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('index'));
            }
            else
            {
                Yii::log("Uid: " .Yii::app()->user->id. " Invalid request customer. Please do not repeat this request again.");
                throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
            }	
            }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }

    /**
     * Manages all models.
     */
    public function actionIndex()
    {
        $this->pageTitle = 'Quản Lý Khách Hàng';
            try
            {
            $model=new Users('search');
            $model->unsetAttributes();  // clear any default values
            if(isset($_GET['Users']))
                    $model->attributes=$_GET['Users'];

            $model->role_id = ROLE_CUSTOMER;
            $aDuplicate=array();
            if(isset($_GET['check_duplicate'])){

                Users::check_duplicate();
            }

            $this->render('index',array(
                    'model'=>$model, 'actions' => $this->listActionsCanAccess,
            ));
            }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }


    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer the ID of the model to be loaded
     */
    public function loadModel($id)
    {
        try
        {
            $model=Users::model()->findByPk($id);
            if($model===null)
            {
                Yii::log("The requested page does not exist.");
                throw new CHttpException(404,'The requested page does not exist.');
            }			
            return $model;
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }

    /**
     * Performs the AJAX validation.
     * @param CModel the model to be validated
     */
    protected function performAjaxValidation($model)
    {
        try
        {
            if(isset($_POST['ajax']) && $_POST['ajax']==='users-form')
            {
                    echo CActiveForm::validate($model);
                    Yii::app()->end();
            }
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }


    public function actionCustomer_maintain()
    {
        $this->pageTitle = 'Khách Hàng';
        try
        {
            //echo Users::getCodeAccount(Yii::app()->user->id);die;
            MyFunctionCustom::checkChangePassword();
//                MyFunctionCustom::updateListCustomerOfAgentLogin();
            $model=new Users('search');
            $model->unsetAttributes();  // clear any default values
            if(isset($_GET['Users']))
                    $model->attributes=$_GET['Users'];

            $model->role_id = ROLE_CUSTOMER;

            $this->render('Customer_maintain',array(
                    'model'=>$model, 'actions' => $this->listActionsCanAccess,
            ));
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }     

    /**
     * @Author: ANH DUNG 11-12-2013
     * @Todo: dùng tạm cho test bảo trì sửa tên vs địa chỉ KH
     */
    public function actionCustomer_maintain_for_fix_add()
    {
        try
        {
            $this->redirect(Yii::app()->createAbsoluteUrl('admin/site/index'));
                //echo Users::getCodeAccount(Yii::app()->user->id);die;
                MyFunctionCustom::checkChangePassword();
//                    MyFunctionCustom::updateListCustomerOfAgentLogin();
            $model=new Users('search');
            $model->unsetAttributes();  // clear any default values
            if(isset($_GET['Users']))
                    $model->attributes=$_GET['Users'];

            $model->role_id = ROLE_CUSTOMER;

            $this->render('Customer_maintain_for_fix_add',array(
                    'model'=>$model, 'actions' => $this->listActionsCanAccess,
            ));
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }        

    public function actionCreate_customer_maintain(){
        $this->pageTitle = 'Tạo Mới Khách Hàng';
        //$this->layout = 'ajax';
        $model = new Users('create_maintain_user');
        $msg= '';
        if(isset($_POST['Users'])){
            $aAttSave = array('code_bussiness','first_name','last_name',
            'province_id','district_id','ward_id','house_numbers',
            'street_id','phone','address','address_vi','name_agent',
            'type','area_code_id',
            );				
            $model->attributes = $_POST['Users'];
            $model->validate($aAttSave);
            // quan trọng khi làm, nhớ chỗ saveCustomerAgent bên dưới
            if(!$model->hasErrors()){
//                        MyFunctionCustom::updateListCustomerOfAgentLogin();
                    $model = MyFunctionCustom::clearHtmlModel($model, $aAttSave);
                    $model->role_id = ROLE_CUSTOMER;
                    $model->type = CUSTOMER_TYPE_MAINTAIN;
                    $model->application_id = BE;
                    $model->code_account = MyFunctionCustom::getNextIdForUser('Users', Users::getCodeAccount(Yii::app()->user->parent_id).'_', MAX_LENGTH_CODE, 'code_account', ROLE_CUSTOMER);
                    $model->code_bussiness = MyFunctionCustom::genCodeBusinessCustomerAgent($model->first_name);
                    $aAttSave[] = 'role_id';
                    $aAttSave[] = 'code_account';
                    $aAttSave[] = 'application_id';
                    $model->save(true, $aAttSave);
//                        MyFunctionCustom::saveCustomerAgent($model->id);
                    Yii::app()->user->setFlash('successUpdate', 'Thêm Thành Công Khách Hàng: '.$model->first_name);
                $this->redirect(array('Create_customer_maintain'));					
            }                                    
        }
        $this->render('create_customer_maintain',array(
                'model'=>$model, 'actions' => $this->listActionsCanAccess,
            'msg'=>$msg,
        ));     						
    }

    public function actionUpdate_customer_maintain(){
        $this->pageTitle = 'Cập Nhật Khách Hàng';
        //$this->layout = 'ajax';
        $model = $this->loadModel($_GET['id']);
        $oldName = MyFunctionCustom::getNameRemoveVietnamese($model->first_name);
        if($model->role_id!=ROLE_CUSTOMER || !MyFunctionCustom::agentCanUpdateCustomer($model))
                $this->redirect(array('create_customer_maintain'));
        $model->scenario = 'update_customer_maintain';
        $model->autocomplete_name_street = $model->street?$model->street->name:'';
        $msg= '';
        if(isset($_POST['Users'])){
                            $aAttSave = array('first_name','last_name',
                            'province_id','district_id','ward_id','house_numbers',
                            'street_id','phone',
                            //'address_vi','address_temp',
                            'address','address_vi','name_agent',
                            );	
//                                if(Yii::app()->user->role_id==ROLE_ADMIN)
//                                    $aAttSave[] = 'code_bussiness';
                $model->attributes = $_POST['Users'];
                $model->validate($aAttSave);
                if(!$model->hasErrors()){
                    $newName = MyFunctionCustom::getNameRemoveVietnamese($model->first_name);
                    if(strtolower($oldName) !=  strtolower($newName) && Yii::app()->user->role_id==ROLE_SUB_USER_AGENT){
                        $aAttSave[] = 'code_bussiness';
//                           $needMore = array('customer_id'=>$model->id);
                        $model->code_bussiness = MyFunctionCustom::genCodeBusinessCustomerAgent($model->first_name);
                    }

                    $model = MyFunctionCustom::clearHtmlModel($model, $aAttSave);
                    $model->update($aAttSave);
                    //MyFunctionCustom::saveCustomerAgent($model->id);

                    Yii::app()->user->setFlash('successUpdate', 'Cập Nhật Thành Công Khách Hàng: '.$model->first_name);
                    $this->redirect(array('update_customer_maintain','id'=>$model->id));
                }                                    
        }
        $this->render('update_customer_maintain',array(
                'model'=>$model, 'actions' => $this->listActionsCanAccess,
            'msg'=>$msg,
        ));     						
    }


  //*****************************  auto complete of search customer **************************************//
    // -- Nguyen Dung
    public function actionAutocompleteCustomer(){ 
        $criteria = new CDbCriteria();
//            $criteria->compare('t.name', $_GET['term'], true);        
//public CDbCriteria addSearchCondition(string $column, string $keyword, boolean $escape=true, string $operator='AND', string $like='LIKE')            
        $criteria->addSearchCondition('t.first_name', $_GET['term'], true, 'OR'); // true ==> LIKE '%...%'
        $criteria->addSearchCondition('t.code_account', $_GET['term'], true, 'OR'); // true ==> LIKE '%...%'
        $criteria->addSearchCondition('t.code_bussiness', $_GET['term'], true, 'OR'); // true ==> LIKE '%...%'
        $criteria->compare('t.role_id', ROLE_CUSTOMER); 
        $criteria->limit = 30;

        if(Yii::app()->params['enable_limit_customer_of_agent']=='yes'){
            if(Yii::app()->user->role_id!=ROLE_ADMIN){
            }
        }

        $models = Users::model()->findAll($criteria);
        $returnVal=array();
        foreach($models as $model)
        {
            $returnVal[] = array(    
                'label'=>$model->code_account.' - '.$model->code_bussiness.' -- '.$model->first_name,
                'value'=>$model->first_name,
                'id'=>$model->id,
                'name_agent'=>$model->name_agent,
                'code_account'=>$model->code_account,
                'code_bussiness'=>$model->code_bussiness,
                'address'=>$model->address,
                'sale'=>$model->sale?$model->sale->first_name:"",
            );
        }
        echo CJSON::encode($returnVal);
        Yii::app()->end();        
    }        

//*****************************  auto complete of search agent **************************************//
    // -- Nguyen Dung
    public function actionAutocompleteAgent(){ 
        $criteria = new CDbCriteria();
//            $criteria->compare('t.name', $_GET['term'], true);        
//public CDbCriteria addSearchCondition(string $column, string $keyword, boolean $escape=true, string $operator='AND', string $like='LIKE')            
        $criteria->addSearchCondition('t.first_name', $_GET['term'], true, 'OR'); // true ==> LIKE '%...%'
        $criteria->addSearchCondition('t.code_account', $_GET['term'], true, 'OR'); // true ==> LIKE '%...%'
        //$criteria->addSearchCondition('t.code_bussiness', $_GET['term'], true, 'OR'); // true ==> LIKE '%...%'
        $criteria->compare('t.role_id', ROLE_AGENT); 
        $criteria->limit = 30;
        $models = Users::model()->findAll($criteria);
        $returnVal=array();
        foreach($models as $model)
        {
            $returnVal[] = array(    
                'label'=>$model->code_account.' - '.$model->code_bussiness.' -- '.$model->first_name,
                'value'=>$model->first_name,
                'id'=>$model->id,
                'name_agent'=>$model->name_agent,
                'code_account'=>$model->code_account,
                'code_bussiness'=>$model->code_bussiness,
                'address'=>$model->address,
                'sale'=>$model->sale?$model->sale->first_name:"",
            );
        }
        echo CJSON::encode($returnVal);
        Yii::app()->end();        
    }    		

    // 10-24-2013
    public function actionExport_list_customer_maintain(){
        if(isset($_SESSION['data-excel'])){
            set_time_limit(7200);
            ExportList::Export_list_customer_maintain();
        }
        $this->redirect(array('index'));               
    }

    /**
     * @Author: ANH DUNG Aug 24, 2015
     */
    public function actionExportStorecard(){
        if(isset($_SESSION['data-excel'])){
            // DuongNV feb 27,2019 init session array id khách hàng có đặt hàng qua app trong 3 tháng
            if(isset($_GET['app'])){
                $mGasAppOrder               = new GasAppOrder();
                $_SESSION['data-excel-app'] = $mGasAppOrder->getArrayRecentUseApp();
            }
            set_time_limit(7200);
            ini_set('memory_limit','500M');
            ExportList::CustomerStoreCard();
        }
        $this->redirect(array('index'));               
    }
    
    /** @Author: DuongNV feb 27,2019
     *  @Todo: check quyền xuất excel khách hàng ko đặt app trong 3 tháng
     **/
    public function canExportApp() {
        $aUidAllow  = [GasConst::UID_ADMIN];
        $cUid       = MyFormat::getCurrentUid();
        return in_array($cUid, $aUidAllow);
    }
    
    /**
     * @Author: ANH DUNG Apr 05, 2016
     * @Todo: cho sale or cv CCS tạo KH
     */
    public function actionCustomerSaleStoreCard()
    {
        $this->pageTitle = 'Khách Hàng Chờ Duyệt';	
        try{
        MyFunctionCustom::checkChangePassword();
        $model=new Users('search');
        $model->unsetAttributes();  // clear any default values
        $model->status = STATUS_WAIT_ACTIVE;
        if(isset($_GET['Users']))
            $model->attributes=$_GET['Users'];
        $model->role_id = ROLE_CUSTOMER;
        $model->type = CUSTOMER_TYPE_STORE_CARD;

        $this->render('CustomerSaleStoreCard',array(
                'model'=>$model, 'actions' => $this->listActionsCanAccess,
        ));
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }

    public function actionCustomer_store_card()
    {
        $this->pageTitle = 'KH Bò Mối';	
        try{
            MyFunctionCustom::checkChangePassword();
//                    MyFunctionCustom::updateListCustomerOfAgentLogin();
        $model=new Users('search');
        $model->unsetAttributes();  // clear any default values
        $model->channel_id = Users::CON_LAY_HANG;
        if(isset($_GET['Users']))
            $model->attributes=$_GET['Users'];
        $model->role_id = ROLE_CUSTOMER;
        $model->type = CUSTOMER_TYPE_STORE_CARD;
        

        $this->render('Customer_store_card',array(
                'model'=>$model, 'actions' => $this->listActionsCanAccess,
        ));
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }

    public function actionCustomer_store_card_bo()
    {
    $this->pageTitle = 'Khách Hàng Bò';	
    try
    {
        MyFunctionCustom::checkChangePassword();
        $model=new Users('search');
        $model->unsetAttributes();  // clear any default values
        if(isset($_GET['Users']))
                $model->attributes=$_GET['Users'];
        $model->role_id = ROLE_CUSTOMER;
        $model->is_maintain = [STORE_CARD_KH_BINH_BO, UsersExtend::STORE_CARD_HEAD_QUATER];
        $model->channel_id = Users::CON_LAY_HANG;

        $this->render('Customer_store_card_bo',array(
                'model'=>$model, 'actions' => $this->listActionsCanAccess,
        ));
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }
    public function actionCustomer_store_card_moi()
    {
    $this->pageTitle = 'Khách Hàng Mối';	
    try
    {
        MyFunctionCustom::checkChangePassword();
        $model=new Users('search');
        $model->unsetAttributes();  // clear any default values
        $model->channel_id = Users::CON_LAY_HANG;
        if(isset($_GET['Users']))
                $model->attributes=$_GET['Users'];
        $model->role_id = ROLE_CUSTOMER;
        $model->is_maintain = STORE_CARD_KH_MOI;

        $this->render('Customer_store_card_moi',array(
                'model'=>$model, 'actions' => $this->listActionsCanAccess,
        ));
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }

    /** @Author: ANH DUNG Now 23, 2017
     *  @Todo: map sẵn thông tin cho KH app
     **/
    public function mapInfoApp(&$mUser) {
        $cUid = MyFormat::getCurrentUid();
        if(!in_array($cUid, GasTickets::getUidCreateKhApp())){
            return ;
        }
        $mUser->price               = UsersRef::PRICE_OTHER;
        $mUser->is_maintain         = UsersExtend::STORE_CARD_HGD_APP;
        
    }
    
    public function setInfoApp(&$mUser) {
        if($mUser->is_maintain == UsersExtend::STORE_CARD_HGD_APP){
            $mUser->username   = $mUser->phone;
        }
    }
    
    public function actionCreate_customer_store_card(){
    try {
        if(!GasCheck::AgentCreateCustomerStoreCard() || !GasOrders::CanCreateCustomer()){
            $this->redirect('customer_store_card');
        }

        $this->pageTitle = 'Tạo Mới Khách Hàng Bò Mối';
        $model = new Users('create_customer_store_card');
        $model->status = STATUS_WAIT_ACTIVE;
        if(!$model->canCreateMoreCustomer()){
            throw new Exception("Bạn đã tạo đủ ".UsersRef::$LIMIT_240_HGD." khách hàng hộ gia đình chương trình 240");
        }
        $model->mUsersRef = new UsersRef('create_customer_store_card'); // Sep 29, 2015
        $model->getSaleIdCreate();
        $this->mapInfoApp($model);
        $msg= '';
        if(isset($_POST['Users'])){
            $model->attributes = $_POST['Users'];
            $model->mUsersRef->attributes = $_POST['UsersRef'];
            $model->setStatusSaleCreate();
            $this->setInfoApp($model);
            $model->created_by = Yii::app()->user->id; // Apr 11, 2016 lưu người tạo KH - là điều phối các KV hoặc là sale
            $model->mUsersRef->validate();
            $model->validate();
            $model->validateGasBrand();
            $model->validateExtPhone2();
            if(!$model->mUsersRef->hasErrors() && !$model->hasErrors()){
                $model->setUsernameCustomer();
                MyFunctionCustom::saveCustomerStoreCard($model);
                $model->makeRecordChangePass();
            }

            if(!$model->hasErrors() && !$model->mUsersRef->hasErrors()){
                $model->mUsersRef->user_id = $model->id;
                $model->mUsersRef->setGasBrand();
                $model->SaveUsersRef();// Sep 29, 2015
                // May 13, 2016 update Phone KH
                UsersPhone::savePhone($model);
                $model->saveExtPhone2();
                $model->setChainAccountApp();
                $model->solrAdd();
                GasScheduleEmail::BuildListNotifyNewCustomer($model);// Apr 12, 2016
                Yii::app()->user->setFlash('successUpdate', 'Thêm Thành Công Khách Hàng: '.$model->first_name);
                $this->redirect(array('create_customer_store_card'));					
            }

        }
            $this->render('create_customer_store_card',array(
                    'model'=>$model, 'actions' => $this->listActionsCanAccess,
                'msg'=>$msg,
            ));     	

        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }        
    }

    public function actionUpdate_customer_store_card(){
        try{
//        throw new Exception('Hệ thống cập nhật để có thể nhập nhiều email báo giá, bạn vuil lòng quay lại sau 30 phút nữa');
        $model = $this->loadModel($_GET['id']);
        if(!Users::CanAccessProvince($model->province_id)){
            $this->redirect(array('customer_store_card'));
        }
        $this->handleToResendSms($model);
        $this->handleToUpdateReportDebit($model);
        $mOldUser = clone $model;
        $model->modelOld = clone $model;
        
        $this->pageTitle = $model->first_name;
        $oldName = MyFunctionCustom::getNameRemoveVietnamese($model->first_name);
        $oldSale            = $model->sale_id;
        $cRole              = MyFormat::getCurrentRoleId();
        $aRoleUpdateAll     = [ROLE_ADMIN, ROLE_CALL_CENTER];
        if( !MyFunctionCustom::agentCanUpdateCustomer($model, array('store_card'=>1)) || $model->role_id!=ROLE_CUSTOMER 
            || ( !in_array($cRole, $aRoleUpdateAll) && in_array($model->is_maintain, CmsFormatter::$aTypeIdHgd) && !$model->canUpdateHgd() ) ){
            $this->redirect(array('create_customer_store_card'));
        }
        $model->scenario                    = 'update_customer_store_card';
        $model->autocomplete_name_street    = $model->street?$model->street->name:'';
        $model->autocomplete_name           = $model->customer?$model->customer->first_name:'';
        $model->LoadUsersRef();
        $model->loadExtPhone2();
        $msg= '';
        $this->HandleReject($model);

        if(isset($_POST['Users'])){
            $aAttSave = array('first_name','last_name',
            'province_id','district_id','ward_id','house_numbers',
            'street_id','phone','address','address_vi','name_agent',
            'is_maintain','parent_id','beginning',
            'sale_id','payment_day','area_code_id',
//                'created_by', 'last_logged_in', // Close from Apr 11, 2016 Dec 25, 2014 lưu vết người sửa KH cuối cùng
            'status','channel_id', // May 14, 2015 trạng thái lấy hàng của KH
            'code_account','price','price_other', // Mar 15, 2016 đồng bộ Mã KH PM Kế toán
            'storehouse_id','email','price','price_other','first_char', // Mar 15, 2016 đồng bộ Mã KH PM Kế toán
            'username',// Jan0518 cho phép admin sửa username KH app
            'application_id', //Sep1918 cho phép chỉnh sữa line
            );
            $model->attributes = $_POST['Users'];
            $model->mUsersRef->attributes = $_POST['UsersRef'];
            $model->setStatusSaleCreate();
            $model->validate($aAttSave);
            $model->validateGasBrand();
            $model->validateExtPhone2();
            if(!$model->hasErrors() && !$model->mUsersRef->hasErrors()){
//                   Close from Apr 11, 2016  $model->created_by = Yii::app()->user->id; // Uid Dec 25, 2014 lưu vết người sửa KH cuối cùng
//                    $model->last_logged_in = date('Y-m-d H:i:s'); // Datetime Dec 25, 2014 lưu vết người sửa KH cuối cùng
                $newName = MyFunctionCustom::getNameRemoveVietnamese($model->first_name);
                if(strtolower($oldName) !=  strtolower($newName) && !in_array($model->is_maintain, CmsFormatter::$aTypeIdHgd)){
//                    if(1){// May 08, 2017 only dev test change name
                    $aAttSave[] = 'code_bussiness';
                    $needMore=array('type'=>CUSTOMER_TYPE_STORE_CARD, 'customer_all_system'=>1);
                    if(in_array($model->is_maintain, CmsFormatter::$ARR_TYPE_CUSTOMER_STORECARD)){
                        $needMore['BoMoi'] = 1;
                    }
                    $model->code_bussiness = MyFunctionCustom::genCodeBusinessCustomerAgent($model->first_name, $needMore);
                }

                $model = MyFunctionCustom::clearHtmlModel($model, $aAttSave);
                $model->update($aAttSave);
                // Dec 28, 2015 update table if change sale id
                if($oldSale != $model->sale_id){
                    $model->changeSale();
                }
                // Dec 28, 2015 update table if change sale id

                GasClosingBalanceCustomer::updateTonDauKy(GasMaterialsOpeningBalance::YEAR_OPENING, $model->id, 
                        $model->is_maintain, $model->beginning);
                // cập nhật công nợ KH
                $model->mUsersRef->setGasBrand();
                $model->SaveUsersRef();// May 27, 2015 
                // May 13, 2016 update Phone KH
                UsersPhone::savePhone($model);
                $model->saveExtPhone2();
                $model->setChainAccountApp();
                $this->trackingChangeCustomer($model, $mOldUser);
                $this->handleChangeTypeCustomer($model, $mOldUser);
                $this->notifyLockCustomer($model);
                $model->solrAdd();

                Yii::app()->user->setFlash('successUpdate', "Cập Nhật Thành Công Khách Hàng: $model->code_bussiness  - ".$model->first_name);
                $this->redirect(array('update_customer_store_card','id'=>$model->id));
            }
        }
        $this->render('update_customer_store_card',array(
                'model'=>$model, 'actions' => $this->listActionsCanAccess,
            'msg'=>$msg,
        ));
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }
    
    /** @Author: ANH DUNG Apr 13, 2016
     *  @Todo: handle update customer storecard
     */
    public function HandleReject($model) {
        if(isset($_GET['reject']) && $model->canRejectCreateCustomer()){
//        if(isset($_GET['reject']) ){
//            $model->setStatus(STATUS_REJECT);
            $model->delete();// Jul 13, 2017 Chương nói xóa luôn khi hủy bỏ
            Yii::app()->user->setFlash('successUpdate', "Hủy Bỏ Thành Công Khách Hàng: $model->code_bussiness  - ".$model->first_name);
            $this->redirect(array('customerSaleStoreCard'));
        }
    }
    
    public function trackingChangeCustomer($model, $mOldUser) {
        if(in_array($model->is_maintain, CmsFormatter::$aTypeIdMakeUsername)){
            $mMonitorUpdate = new MonitorUpdate();
            $mMonitorUpdate->type = MonitorUpdate::TYPE_CHANGE_INFO_CUSTOMER;
            $mMonitorUpdate->saveChangeInfoCustomer($mOldUser);
        }
    }
    
    /** @Author: ANH DUNG Jun 25, 2018
     *  @Todo: update loại KH bò mối khi change type_customer vào các table liên quan
     * nếu change loại KH thì cập nhật sang các bảng khác luôn
     **/
    public function handleChangeTypeCustomer($model, $mOldUser) {
        if($mOldUser->is_maintain != $model->is_maintain && in_array($model->is_maintain, CmsFormatter::$aTypeIdMakeUsername)){
            CronUpdate::updateTypeCustomerReportDebitCash($model->id);
        }
    }
    
    /** @Author: ANH DUNG Jul 11, 2018
     *  @Todo: xử lý mail notify Kinh doanh + Kế toán khi KH bị chặn hàng
     **/
    public function notifyLockCustomer($mCustomer) {
        $sendEmail = new SendEmail();
        $sendEmail->notifyLockCustomer($mCustomer);
    }
    
    /** @Author: ANH DUNG Mar 02, 2017
     *  @Todo: xử lý gửi lại báo giá SMS
     */
    public function handleToResendSms($model) {
        if(isset($_GET['ResendSms']) && $model->canResendSms()){
            $model->doResendSms();
            Yii::app()->user->setFlash(MyFormat::SUCCESS_UPDATE, "Gửi SMS báo giá thành công");
            $this->redirect(array('update_customer_store_card','id'=>$model->id));
        }
    }
    /** @Author: ANH DUNG Jan 28, 2018
     *  @Todo: xử lý cập nhật lại loại KH cho BC công nợ tổng quát
     */
    public function handleToUpdateReportDebit($model) {
        if(isset($_GET['UpdateReportDebit'])){
            CronUpdate::updateTypeCustomerReportDebitCash($model->id);
            Yii::app()->user->setFlash(MyFormat::SUCCESS_UPDATE, 'Cập nhật lại loại KH cho BC công nợ tổng quát thành công');
            $this->redirect(array('update_customer_store_card','id'=>$model->id));
        }
    }
        
 public function actionCreate_sale(){
    try {
        $this->layout='ajax';
        $model = new Users('dieuphoi_create_sale');
        if(isset($_POST['Users'])){
            $model->attributes = $_POST['Users'];
            $model->validate();
            if(!$model->hasErrors()){
                Users::SaveSale($model);
                Yii::app()->user->setFlash('successUpdate', 'Thêm Thành Công Sale: '.$model->first_name);
                $this->redirect(array('Create_sale'));					
            }
        }
        $this->render('Create_sale/create',array(
            'model'=>$model, 'actions' => $this->listActionsCanAccess,
        ));     	

        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }        
    }        
    
    /**
     * @Author: ANH DUNG May 14, 2015
     * @Todo: danh sach KH khong lay hang
     * @Param: $model
     */
    public function actionCustomer_not_buy()
    {
    $this->pageTitle = 'Khách hàng không lấy hàng';	
    try{
        MyFunctionCustom::checkChangePassword();
        $model=new Users('search');
        $model->unsetAttributes();  // clear any default values
        if(isset($_GET['Users']))
                $model->attributes=$_GET['Users'];
        $model->role_id     = ROLE_CUSTOMER;
        $model->type        = CUSTOMER_TYPE_STORE_CARD;
        $model->channel_id  = Users::KHONG_LAY_HANG;

        $this->render('Customer_not_buy',array(
                'model'=>$model, 'actions' => $this->listActionsCanAccess,
        ));
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }
    
    /**
     * @Author: ANH DUNG Now 30, 2015
     * @Todo: Admin quản lý username của KH
     */
    public function actionList_username()
    {
        $this->pageTitle = 'Username Login Khách Hàng';	
        try{
        $model=new Users('search');
        $model->unsetAttributes();  // clear any default values
        $model->channel_id = Users::CON_LAY_HANG;
        if(isset($_GET['Users']))
            $model->attributes=$_GET['Users'];
        $model->role_id = ROLE_CUSTOMER;
        $model->type = CUSTOMER_TYPE_STORE_CARD;

        $this->render('List_username',array(
                'model'=>$model, 'actions' => $this->listActionsCanAccess,
        ));
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }
    
    /**
     * @Author: ANH DUNG Now 30, 2015
     * @Todo: Admin Update username của KH
     */
    public function actionList_username_update(){
        $model = $this->loadModel($_GET['id']);
        $this->pageTitle = 'Update - '.$model->first_name;        
        $cRole = Yii::app()->user->role_id;
        if($cRole != ROLE_ADMIN)
            $this->redirect(array('create_customer_store_card'));
        $model->scenario = 'update_customer_username';
        if(isset($_POST['Users'])){
            $aAttSave = array('username');
            $model->attributes = $_POST['Users'];
            $model->role_id = ROLE_CUSTOMER;
            $model->validate();
            if(!$model->hasErrors()){
                if(!empty($model->password_hash)){
                    $aAttSave[] = 'temp_password';
                    $aAttSave[] = 'password_hash';
                    $model->temp_password   = $model->password_hash;
                    $model->password_hash   = md5($model->password_hash);
                    $model->password_confirm = $model->password_hash;// only for pass validate not meaning
                }
                $model->update($aAttSave);
                $this->setRequiredChangePass($model);
                Yii::app()->user->setFlash('successUpdate', "Cập Nhật Thành Công Khách Hàng: $model->code_bussiness  - ".$model->first_name);
                $this->redirect(array('list_username_update','id'=>$model->id));
            }
        }
        $this->render('List_username_update',array(
                'model'=>$model, 'actions' => $this->listActionsCanAccess,
        ));
    }
    public function setRequiredChangePass($mUser) {
        if($mUser->password){
            $mMonitorUpdate = new MonitorUpdate();
            $mMonitorUpdate->makeRecordChangePass($mUser);
            UsersTokens::deleteByUser($mUser->id);
        }
    }
    
    /**
     * @Author: ANH DUNG Jun 08, 2016
     * @Todo: CCS danh sách KH STORE_CARD_VIP_HGD hộ GĐ 240 điểm
     */
    public function actionVip240()
    {
        $this->pageTitle = 'Khách Hàng Dân Dụng - 240 VIP';
        try{
        $model=new Users();
        $model->unsetAttributes();  // clear any default values
//        $model->channel_id = Users::CON_LAY_HANG;// Jan0718 tạm close để load nhanh, chưa tìm đc giải pháp cho sql này
        $aHgdAllow = array(STORE_CARD_VIP_HGD, STORE_CARD_HGD_CCS);
        $model->is_maintain = $aHgdAllow;
        $model->date_from   = '01-'.date('m-Y');
        $model->date_to     = date('d-m-Y');
        if(isset($_GET['Users'])){
            $model->attributes=$_GET['Users'];
        }
        $model->role_id     = ROLE_CUSTOMER;
        $model->type        = CUSTOMER_TYPE_STORE_CARD;
        if(!empty($model->is_maintain) && !in_array($model->is_maintain, $aHgdAllow)){
            $model->is_maintain = STORE_CARD_HGD_CCS;
        }
        
        $this->render('Customer_Vip240',array(
                'model'=>$model, 'actions' => $this->listActionsCanAccess,
        ));
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }
    
    /**
     * @Author: ANH DUNG Jun 08, 2016
     * @Todo: CCS danh sách KH STORE_CARD_VIP_HGD hộ GĐ 240 điểm
     */
    public function actionHgd()
    {
        $this->pageTitle = 'Khách hàng hộ gia đình';
        try{
        $model=new Users();
        $model->unsetAttributes();  // clear any default values
        $model->channel_id = Users::CON_LAY_HANG;// mở lại để biết chỗ này chưa fix được, vẫn query chậm trên 1s -- Jan0718 tạm close để load nhanh, chưa tìm đc giải pháp cho sql này
        $aHgdAllow = array(STORE_CARD_HGD, STORE_CARD_VIP_HGD, STORE_CARD_HGD_CCS);
        $model->is_maintain = $aHgdAllow;
        $model->date_from   = '01-'.date('m-Y');
        $model->date_to     = date('d-m-Y');
        if(isset($_GET['Users'])){
            $model->attributes=$_GET['Users'];
        }
        $model->role_id     = ROLE_CUSTOMER;
        $model->type        = CUSTOMER_TYPE_STORE_CARD;
        
        if(!empty($model->is_maintain) && !is_array($model->is_maintain) && !in_array($model->is_maintain, $aHgdAllow)){
            $model->is_maintain = STORE_CARD_HGD;
        }

        $this->render('Customer_Hgd',array(
                'model'=>$model, 'actions' => $this->listActionsCanAccess,
        ));
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }
    
    /**
     * @Author: ANH DUNG Sep 08, 2016
     * @Todo: view map KH dân dụng CCS
     */
    public function actionHgdMap()
    {
        $this->pageTitle = 'Báo cáo vị trí khách hàng hộ gia đình';
        try{
        $model=new Users();
        $model->date_from   = date("d-m-Y");
        $model->date_to     = $model->date_from; 
        $model->unsetAttributes();  // clear any default values
        $model->pageSize    = 100;  // clear any default values
        if(isset($_GET['Users'])){
            $model->attributes=$_GET['Users'];
        }
        $this->render('hgd/HgdMap',array(
                'model'=>$model, 'actions' => $this->listActionsCanAccess,
        ));
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }

    /**
     * @Author: ANH DUNG Sep 19, 2016
     * @Todo: ExportHgd
     */
    public function actionExportHgd(){
        try{
        if(isset($_SESSION['data-excel'])){
            ini_set('memory_limit','1000M');
            $mUser = new Users();
            $mExportList = new ExportList();
            $mExportList->hgd($_SESSION['data-excel']->data, $mUser);
        }
        $this->redirect(array('index'));
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }
    
    /**
     * @Author: ANH DUNG Now 30, 2016
     * @Todo: danh sách KH APP: UsersExtend::STORE_CARD_HGD_APP
     */
    public function actionApp()
    {
        $this->pageTitle = 'Khách hàng APP';
        try{
        $model=new Users();
        $model->unsetAttributes();  // clear any default values
//        $model->channel_id = Users::CON_LAY_HANG;// Jan0718 tạm close để load nhanh, chưa tìm đc giải pháp cho sql này
        $model->date_from   = '01-'.date('m-Y');
        $model->date_to     = date('d-m-Y');
        if(isset($_GET['Users']))
            $model->attributes=$_GET['Users'];
        $model->role_id     = ROLE_CUSTOMER;
        $model->type        = CUSTOMER_TYPE_STORE_CARD;
        $model->is_maintain = UsersExtend::STORE_CARD_HGD_APP;

        $this->render('Customer_App',array(
                'model'=>$model, 'actions' => $this->listActionsCanAccess,
        ));
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }
    
    /**
     * Handle the ajax request. This process changes the status of member to 1 (mean active)
     * @param type $id the id of member need changed status to 1
     */
    public function actionAjaxActivate($id) {
        if(Yii::app()->request->isPostRequest){
            $model = $this->loadModel($id);
            if(method_exists($model, 'activate'))
            {
                $model->activate();
            }
            Yii::app()->end();
        }else{
            Yii::log('Invalid request. Please do not repeat this request again.');
            throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
        }
    }

    /** Handle the ajax request. This process changes the status of member to 0 (mean deactive)
     * @param type $id the id of member need changed status to 0
     */
    public function actionAjaxDeactivate($id) {
        if(Yii::app()->request->isPostRequest){
            $model = $this->loadModel($id);
            if(method_exists($model, 'deactivate'))
            {
                $model->deactivate();
            }
            Yii::app()->end();
        }else{
            Yii::log('The requested page does not exist.');
            throw new CHttpException(404,'The requested page does not exist.');
        }
    }
    /** @Author: Pham Thanh Nghia Aug 22 ,2018
     *  @Todo: hiện colorbox quản lý File
     *  @Param: use for admin/gascustomer/customer_store_card
     **/
    public function actionManageFile($id){
        $this->pageTitle = 'Tạo Mới ';
        try{
        $model= Users::model()->findByPk($id);
        $mGasCustomerImages = new GasCustomerImages();
        $this->layout       = 'ajax';
        if(isset($_POST['Users']))
        {
            $model->attributes=$_POST['Users'];
            $mGasCustomerImages->ValidateFile($model);
            if(!$model->hasErrors()){
                $mGasCustomerImages->handleImage($model);
                Yii::app()->user->setFlash( MyFormat::SUCCESS_UPDATE, 'Cập nhật thành công ');
            }
        }
        $this->render('manage_file',array(
            'model'=>$model, 
            'mGasCustomerImages'=>$mGasCustomerImages, 
            'actions' => $this->listActionsCanAccess,
        ));
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }
    
    
}
