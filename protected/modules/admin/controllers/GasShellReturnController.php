<?php

class GasShellReturnController extends AdminController
{
    public $pluralTitle = "Thu Vỏ";
    public $singleTitle = "Thu Vỏ";

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id)
    {
        $this->pageTitle = 'Xem ';
        try {
            $this->render('view', array(
                'model' => $this->loadModel($id), 'actions' => $this->listActionsCanAccess,
            ));
        } catch (Exception $exc) {
            GasCheck::CatchAllExeptiong($exc);
        }

    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate()
    {
        $this->pageTitle = 'Tạo Mới ';
        try {
            $model = new GasShellReturn('create');

            // Uncomment the following line if AJAX validation is needed
            // $this->performAjaxValidation($model);

            if (isset($_POST['GasShellReturn'])) {
                $model->attributes = $_POST['GasShellReturn'];
                $model->validate();
                if (!$model->hasErrors()) {
                    $model->save();
                    Yii::app()->user->setFlash(MyFormat::SUCCESS_UPDATE, "Thêm mới thành công.");
                    $this->redirect(array('create'));
                }
            }

            $this->render('create', array(
                'model' => $model, 'actions' => $this->listActionsCanAccess,
            ));
        } catch (Exception $exc) {
            GasCheck::CatchAllExeptiong($exc);
        }
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id)
    {
        $this->pageTitle = 'Cập Nhật ';
        try {
            $model = $this->loadModel($id);

            if (isset($_POST['GasShellReturn'])) {
                /** @var GasShellReturn $model */
                if ($model->saveWithRole($_POST['GasShellReturn'])) {
                    Yii::app()->user->setFlash(MyFormat::SUCCESS_UPDATE, "Cập nhật thành công.");
                    $this->redirect(array('update', 'id' => $model->id));
                } else {
                    Yii::app()->user->setFlash(MyFormat::ERROR_UPDATE, "Cập nhật không thành công.");
                }
            }

            $this->render('update', array(
                'model' => $model, 'actions' => $this->listActionsCanAccess,
            ));
        } catch (Exception $exc) {
            GasCheck::CatchAllExeptiong($exc);
        }
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id)
    {
        try {
            if (Yii::app()->request->isPostRequest) {
                // we only allow deletion via POST request
                if ($model = $this->loadModel($id)) {
                    if ($model->delete())
                        Yii::log("Uid: " . Yii::app()->user->id . " Delete record " . print_r($model->attributes, true), 'info');
                }

                // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
                if (!isset($_GET['ajax']))
                    $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('index'));
            } else {
                Yii::log("Uid: " . Yii::app()->user->id . " Invalid request. Please do not repeat this request again.");
                throw new CHttpException(400, 'Invalid request. Please do not repeat this request again.');
            }
        } catch (Exception $exc) {
            GasCheck::CatchAllExeptiong($exc);
        }
    }

    /**
     * Manages all models.
     */
    public function actionIndex()
    {
        $this->pageTitle = 'Danh Sách ';
        try {
            $model = new GasShellReturn('search');
            $model->unsetAttributes();  // clear any default values
            if (isset($_GET['GasShellReturn']))
                $model->attributes = $_GET['GasShellReturn'];
            $this->render('index', array(
                'model' => $model, 'actions' => $this->listActionsCanAccess,
            ));
        } catch (Exception $exc) {
            GasCheck::CatchAllExeptiong($exc);
        }
    }

    /**
     * @Author: TRUNG June 21 2016
     * Make the plan for pending item.
     * @throws CHttpException
     */
    public function actionPlanning()
    {
        $this->pageTitle = 'Lập kế hoạch ';
        try {
            // Check save data first
            if (isset($_POST['DeleteIds']) && isset($_POST['CurrentWeek']) &&
                isset($_POST['GasShellReturn']) && is_array($_POST['GasShellReturn'])
            ) {
                if (GasShellReturn::saveList($_POST['GasShellReturn'], explode(',', $_POST['DeleteIds']), $_POST['CurrentWeek'])) {
                    // Flash success message if done
                    Yii::app()->user->setFlash(MyFormat::SUCCESS_UPDATE, "Cập nhật thành công.");
                } else {
                    Yii::app()->user->setFlash(MyFormat::ERROR_UPDATE, "Cập nhật không thành công.");
                }
            }
            // Lấy tuần này là tuần nào
            $firstWeekDay = GasShellReturn::getFirstDateOfWeek();

            // Lấy data của tuần này
            $listModel = GasShellReturn::getPlanningThisWeekData($firstWeekDay);

            // Lấy data củ của tuần trước
            $listLastModel = GasShellReturn::getPlanningLastWeekData($firstWeekDay, $listModel);

            // Kết hợp 2 cái thành và xóa ngày hiện tại
            /** @var GasShellReturn $value */
            foreach ($listLastModel as $value) {
                $listModel[] = $value;
            }

            $this->render('planning', array(
                'listModel' => $listModel, 'firstWeekDay' => $firstWeekDay, 'actions' => $this->listActionsCanAccess,
            ));
        } catch (Exception $exc) {
            GasCheck::CatchAllExeptiong($exc);
        }
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer the ID of the model to be loaded
     */
    public function loadModel($id)
    {
        try {
            $model = GasShellReturn::model()->findByPk($id);
            if ($model === null) {
                Yii::log("The requested page does not exist.");
                throw new CHttpException(404, 'The requested page does not exist.');
            }
            return $model;
        } catch (Exception $exc) {
            GasCheck::CatchAllExeptiong($exc);
        }
    }

    /**
     * Performs the AJAX validation.
     * @param CModel the model to be validated
     */
    protected function performAjaxValidation($model)
    {
        try {
            if (isset($_POST['ajax']) && $_POST['ajax'] === 'gas-shell-return-form') {
                echo CActiveForm::validate($model);
                Yii::app()->end();
            }
        } catch (Exception $exc) {
            GasCheck::CatchAllExeptiong($exc);
        }
    }

    /**
     * @Author: Trung June 28, 2016
     * @Todo: get last customer shell return info
     */
    public function actionGetShellReturnInfo()
    {
        $json = array('success' => false);
        $aRes = array();
        $week = $_POST['week'];
        if (isset($_POST['customer_id']) && isset($week)) {
            $mOrderDetail = GasOrdersDetail::getLastOrderOfCustomer($_POST['customer_id']);
            // Try to find current data
            $criteria = new CDbCriteria;
            $criteria->compare('customer_id', $_POST['customer_id']);
            $criteria->compare('week', $week);
            $currentItem = GasShellReturn::model()->find($criteria);
            if (empty($currentItem)) {
                $week = date(strtotime($week . ' - 7 days'));
                // Try to file last 7 days
                $criteria = new CDbCriteria;
                $criteria->compare('customer_id', $_POST['customer_id']);
                $criteria->compare('week', $week);
                $currentItem = GasShellReturn::model()->find($criteria);
            }

            if (empty($currentItem)) {
                // Try to find customer info
                /** @var Users $customer */
                $customer = Users::model()->findByPk($_POST['customer_id']);
                if (!empty($customer)) {
                    $currentItem = new GasShellReturn();
                    $currentItem->sale_id = $customer->sale_id;
                    $currentItem->customer_id = $customer->id;
                }
            }

            if (!empty($currentItem)) {
                // Prepare data to send back to clientO
                $result = array(
                    'sale_id' => $currentItem->sale_id,
                    'sale_name' => empty($currentItem->rSale) ? '' : $currentItem->rSale->first_name,
                    'old_remain' => $currentItem->old_remain > 0 ? $currentItem->old_remain : 0,
                    'freg' => $currentItem->freq > 0 ? $currentItem->freq : 0,
                    'note' => !empty($currentItem->note) ? $currentItem->note : 0,
                );
                $json = array('success' => true, "model" => $result);
            } else {
                $json = array('success' => false);
            }
        }
        echo CJavaScript::jsonEncode($json);
        die;
    }
}
