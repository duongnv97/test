<?php

class GascashbookController extends AdminController 
{
    /**
     * Displays a particular model. 
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id)
    {
        $this->pageTitle = 'Xem ';
        try{                
            $model = $this->loadModel($id);
            if(Yii::app()->user->role_id==ROLE_SUB_USER_AGENT && $model->agent_id!=MyFormat::getAgentId())
                $this->redirect(array('index'));
            $this->render('view',array(
                    'model'=>$model, 'actions' => $this->listActionsCanAccess,
            ));

        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }

    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate()
    {
        $this->pageTitle = 'Tạo Mới ';
        try{
        $this->ajaxChangeReleaseDate();
//            $this->checkExistsRecord();
        $model=new GasCashBook('create');
//        if(!$model->canCreate()){
            throw new Exception('Bạn không được tạo sổ quỹ trên web');
//        }
        $model->aModelCashBookDetail = array(new GasCashBookDetail());
        if(isset($_POST['GasCashBook']))
        {
                $model->attributes=$_POST['GasCashBook'];
                $model->validate();
                if(!$model->hasErrors()){
                    if(!$this->checkReleaseDateValid($model))
                        throw new CHttpException(404, " Ngày Không Hợp Lệ.");
                    $model->save();
                    GasCashBook::saveCashBookDetail($model);
                    Yii::app()->user->setFlash('successUpdate', "Thêm mới thành công.");
//                       $this->redirect(array('create'));
                   $this->redirect(array('view','id'=>$model->id));
                }				
        }

        $this->render('create',array(
                'model'=>$model, 'actions' => $this->listActionsCanAccess,
        ));
        }catch (Exception $exc){
        GasCheck::CatchAllExeptiong($exc);
        }
    }
    
    /**
     * @Author: ANH DUNG 12-29-2013
     * @Todo: khi bấm tạo mới sổ quỹ, thì sẽ kiểm tra xem ngày hiện tại có record nào dc tạo chưa
     * nếu có rồi thì redirect qua update record đó
     */    
    
    public function checkExistsRecord(){
        $today = date('Y-m-d');
        $model = GasCashBook::getByReleaseDate($today);
        if($model){
            $this->redirect(array('update','id'=>$model->id));
        }
    }
    
    /**
     * @Author: ANH DUNG 12-29-2013
     * @Todo: khi change release date check xem ngày đó đã có dữ liệu chưa, nếu có thì redirect qua update
     * không thì ko làm gì cả
     *  1. check nếu date đó có hợp lệ để edit không nữa? sẽ check ở action update
     */        
    public function ajaxChangeReleaseDate(){ 
        if(isset($_POST['release_date']) && isset($_POST['from_ajax'])){
            try {
//                $json = array('success'=>false, 'next'=>Yii::app()->createAbsoluteUrl('admin/gascashbook/create'));
                $json = array('success'=>false, 'next'=>Yii::app()->createAbsoluteUrl('admin/gascashbook/create'));
                $release_date = MyFormat::dateConverDmyToYmd($_POST['release_date']);
                $model = GasCashBook::getByReleaseDate($release_date);
                if($model)
                    $json = array('success'=>true,'next'=>Yii::app()->createAbsoluteUrl('admin/gascashbook/update',array('id'=>$model->id) ));               
            } catch (Exception $exc) {
                $json = array('success'=>false, 'next'=>Yii::app()->createAbsoluteUrl('admin/gascashbook/create'));
            }
            echo CJavaScript::jsonEncode($json);die;
        }
    }
    
    /**
     * @Author: ANH DUNG 12-29-2013
     * @Todo: kiểm tra update có hợp lệ không, nếu không thì redirect qua create
     * chỉ cho cập nhật record có release_date ngày hiện tại và 1 ngày trước
     */      
    public function checkUpdateValid($pk){
        try {
            $model = GasCashBook::model()->findByPk($pk);
            if($model){
                $today = date('Y-m-d');
                $prev_date = MyFormat::modifyDays($today, GasCashBook::getAgentDaysAllowUpdate(), '-');
                $isValidDateBetween = MyFormat::compareDateBetween($prev_date, $today, $model->release_date);
                if($model->agent_id != MyFormat::getAgentId() ||
                    !$isValidDateBetween
                )
                    $this->redirect(array('create'));
            }
        } catch (Exception $exc) {
            throw new CHttpException('404', $exc->getMessage());
        }        
    }
    
    public function checkReleaseDateValid($model){
        try {
            if(!MyFormat::validDateInput($model->release_date, "/")){
                throw new Exception("Ngày user nhập: ( $model->release_date ) == không hợp lệ");
            }
            $today = date('Y-m-d');
            $prev_date = MyFormat::modifyDays($today, GasCashBook::getAgentDaysAllowUpdate(), '-');
            $release_date =  MyFormat::dateConverDmyToYmd($model->release_date);
            $isValidDateBetween = MyFormat::compareDateBetween($prev_date, $today, $release_date);
            if( !$isValidDateBetween )
                return false;
            return true;
        } catch (Exception $ex) {
            throw new Exception(" checkReleaseDateValid Ngày Không Hợp Lệ.".$ex->getMessage());
        }                       
    }
    

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id)
    {
        $this->pageTitle = 'Cập Nhật ';
        try{
        $model=$this->loadModel($id);
        $this->checkUpdateValid($id);
//            if(Yii::app()->user->role_id==ROLE_SUB_USER_AGENT && $model->agent_id!=MyFormat::getAgentId())
//                $this->redirect(array('index'));

        $model->scenario = 'update';
        $model->release_date = MyFormat::dateConverYmdToDmy($model->release_date);
        $old_release_date = $model->release_date;
        $aModelCashBookDetail = GasCashBookDetail::getByCashBookId($model->id);
        if(count($aModelCashBookDetail))
            $model->aModelCashBookDetail = $aModelCashBookDetail;
        else
            $model->aModelCashBookDetail = array(new GasCashBookDetail());

        if(isset($_POST['GasCashBook']))
        {
            $model->attributes=$_POST['GasCashBook'];
            $model->validate();
            if(!$model->hasErrors()){
                $model->release_date = $old_release_date;
                $cmsFormater = new CmsFormatter();
                // May 13, 2014 lưu lại giá trị trước cập nhật của sổ quỹ, hiện tại chưa cần nên ko lưu
//                    $model->value_before_update = $cmsFormater->formatCashBookDetail($model);
                $model->save();
                GasCashBook::saveCashBookDetail($model);
                Yii::app()->user->setFlash('successUpdate', "Cập nhật thành công.");
                $this->redirect(array('update','id'=>$model->id));
            }							
        }

        $this->render('update',array(
                'model'=>$model, 'actions' => $this->listActionsCanAccess,
        ));
        }catch (Exception $exc){
        GasCheck::CatchAllExeptiong($exc);
        }
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id)
    {
        try{
        if(Yii::app()->request->isPostRequest)
        {
            // we only allow deletion via POST request
            if($model = $this->loadModel($id))
            {
                if($model->delete())
                    Yii::log("Uid: " .Yii::app()->user->id. " Delete record ".  print_r($model->attributes, true), 'info');
            }

            // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
            if(!isset($_GET['ajax']))
                    $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('index'));
        }
        else
        {
            Yii::log("Uid: " .Yii::app()->user->id. " Invalid request. Please do not repeat this request again.");
            throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
        }
            }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }

    /**
     * Manages all models.
     */
    public function actionIndex()
    {
        MyFunctionCustom::checkChangePassword();
        $this->pageTitle = 'Sổ Quỹ ';
        try{
        $this->handleExportExcel();
        $model=new GasCashBook('search');
        $model->unsetAttributes();  // clear any default values
        $model->date_from   = '01-'.date('m-Y');
        $model->date_to     = date('d-m-Y');
        
        if(Yii::app()->user->role_id==ROLE_SUB_USER_AGENT){
            $model->agent_id = MyFormat::getAgentId();
        }
        if(isset($_GET['GasCashBook']))
                $model->attributes=$_GET['GasCashBook'];
//            if(empty($model->agent_id))
//                $model->agent_id = Yii::app()->user->id;

        $this->render('index',array(
                'model'=>$model, 'actions' => $this->listActionsCanAccess,
        ));
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }
    
    public function handleExportExcel() {
        if(isset($_GET['ExportExcel']) && isset($_SESSION['data-excel'])){
//            ini_set('memory_limit','1000M');
            ExportList::CashbookAgent();
        }
    }
    public function handleExportExcelAll() {
        if(isset($_GET['ExportExcelAll']) && isset($_SESSION['data-excel'])){
//            ini_set('memory_limit','1000M');
            ini_set('memory_limit','500M');
            ExportList::CashbookAgentAll();
        }
    }
    public function handleExportExcelEmployee() {
        $aTarget = [GasCashBookDetail::TARGET_EMPLOYEE, GasCashBookDetail::TARGET_EMPLOYEE_OFF];
        if(isset($_GET['ExportExcelAll']) && isset($_SESSION['data-excel']) && isset($_GET['target']) && in_array($_GET['target'], $aTarget)){
//            ini_set('memory_limit','1000M');
            ini_set('memory_limit','500M');
            ExportList::CashbookEmployee();
        }
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
    * If the data model is not found, an HTTP exception will be raised.
     * @param integer the ID of the model to be loaded
     */
    public function loadModel($id)
    {
        try{
            $model=GasCashBook::model()->findByPk($id);
            if($model===null)
            {
                Yii::log("The requested page does not exist.");
                throw new CHttpException(404,'The requested page does not exist.');
            }			
            return $model;
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }
    
    /** @Author: ANH DUNG Jun 19, 2017
     * @Todo: view all toàn hệ thống tồn hiện tại
     */
    public function actionViewAll()
    {
        $this->pageTitle = 'Sổ Quỹ toàn hệ thống';
        try{
        $cUid = MyFormat::getCurrentUid();
        $this->handleExportExcelEmployee();
        $this->handleExportExcelAll();
        $model=new GasCashBookDetail('search');
        $model->unsetAttributes();  // clear any default values
//        $model->date_from   = '01-'.date('m-Y');
        $model->date_from   = date('d-m-Y');
        $model->date_to     = $model->date_from;
        $view   = 'ViewAll';
        if(isset($_GET['GasCashBookDetail']))
            $model->attributes=$_GET['GasCashBookDetail'];
        $model->province_id = isset($_GET['GasCashBookDetail']['province_id']) ? $_GET['GasCashBookDetail']['province_id'] : [GasProvince::TP_HCM];
        $aViewEmployee = [GasCashBookDetail::TARGET_EMPLOYEE, GasCashBookDetail::TARGET_EMPLOYEE_OFF, GasCashBookDetail::TARGET_EMPLOYEE_DEBIT, GasCashBookDetail::TARGET_SALE];
        $model->typeReport = isset($_GET['target']) ? $_GET['target'] : 0;
        if($model->typeReport == GasCashBookDetail::TARGET_EMPLOYEE_BY_AGENT){
            if (!empty($model->employee_id)) {
                $view   = 'ViewEmployeeByAgent';
                $this->pageTitle    = 'Tồn quỹ nhân viên theo đại lý';
                $model->aData = $model->getClosingByAgent($model->employee_id);
            }
        }elseif(in_array($model->typeReport, $aViewEmployee)){
            if($model->typeReport==GasCashBookDetail::TARGET_EMPLOYEE_DEBIT && !in_array($cUid, $model->getUserViewDebit())):
                $this->redirect(array('ViewAll'));
            endif;
                
            $model->aData       = $model->viewAllEmployee();
            $view               = 'ViewAllEmployee';
            $this->pageTitle    = 'Sổ Quỹ nhân viên';
        }else{
            $model->aData       = $model->viewAll();
        }
        $this->render('report/'.$view,array(
                'model'=>$model, 'actions' => $this->listActionsCanAccess,
        ));
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }

    public function actionCashbookForProvince()
    {
        error_reporting(1); // var_dump(PHP_INT_SIZE);
        MyFunctionCustom::checkChangePassword();
        $this->pageTitle = 'Sổ quỹ khu vực';
        try{
//        $this->handleExportExcel();
        $model=new GasCashBook('search');
        $model->unsetAttributes();  // clear any default values
        $aData = array();
        $model->date_from   = '01-'.date('m-Y');
        $model->date_to     = date('d-m-Y');
        if(isset($_GET['GasCashBook'])){
            $model->attributes = $_GET['GasCashBook'];
            $aData = $model->getReportExcelForProvinceID($model);
        }
        if(!empty($aData)){
            $mToExcel1 = new ToExcel1();
            $mToExcel1->CashbookAgentForProvinceID($aData);
        }

        $this->render('reportExcelForProvinceID/index',array(
                'model'=>$model, 
                'actions' => $this->listActionsCanAccess,
                'aData' => $aData,
        ));
        
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }
}
