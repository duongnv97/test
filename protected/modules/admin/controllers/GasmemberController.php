<?php

class GasmemberController extends AdminController
{
    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id)
    {
        try{
            $model = $this->loadModel($id);
            if($model->id < 10){
                throw new Exception('Invalid request member id: '.$model->id);
            }
            // Dec1418 sinh mã giới thiệu của NV, nếu NV chưa login
            $mApp = new AppPromotion();
            $mApp->owner_id = $model->id;
            $mApp->getMyReferralInfo();

            $model->LoadUsersRefImageSign();
            $this->pageTitle = ''. $model->first_name." - Xem Member";
            $this->render('view',array(
                    'model'=> $model, 'actions' => $this->listActionsCanAccess,
            ));
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }

    /** admin create member */
    public function actionCreate(){
        $this->pageTitle = 'Tạo Mới Member';
        try{
        $model=new Users('create_member');
        if(isset($_POST['Users'])){
            $model->attributes=$_POST['Users'];
            $model->application_id      = BE;
            $model->password_hash       = GasConst::DEFAULT_PASS;
            $model->password_confirm    = $model->password_hash;
            $model->temp_password       = $model->password_hash;
            $this->handleUsername($model);
            $model->validate();
            if(!$model->hasErrors()){
                $model->scenario = NULL;
                $this->genCodeUser($model);
                $model->save();
                $model->password_hash =  md5($model->password_confirm);
                $model->update(array('password_hash'));
                GasAgentCustomer::saveEmployeeMaintainAgent($model->id);
                UsersPhone::savePhone($model);// May 25, 2016
                $mMonitorUpdate = new MonitorUpdate();
                $mMonitorUpdate->makeRecordChangePass($model);
                GasOneMany::changeAgentEmployee($model);
                $this->saveAgentMonitor($model);
                Yii::app()->user->setFlash( MyFormat::SUCCESS_UPDATE, "Thêm mới thành công." );
                $this->redirect(array('update','id'=>$model->id));
            }
        }

        $this->render('create',array(
                'model'=>$model, 'actions' => $this->listActionsCanAccess,
        ));
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id){
        try
        {
        $model=$this->loadModel($id);
        if($model->id < 10){
            throw new Exception('Invalid request member id: '.$model->id);
        }
        $this->pageTitle = ''. $model->first_name." - Cập Nhật Member";
        $model->scenario = 'update_member';
        $model->modelOld = clone $model;
        $model->maintain_agent_id = GasAgentCustomer::getEmployeeMaintainAgent($model->id);
        $oldName = MyFunctionCustom::getNameRemoveVietnamese($model->first_name);
        $model->autocomplete_name_street = $model->street?$model->street->name:'';
        // Dec 04, 2015
        $model->LoadUsersRefImageSign();
        $model->mUsersRef->old_image_sign = $model->mUsersRef->image_sign;
        $model->agent_id_search = GasOneMany::getArrOfManyId($model->id, GasOneMany::TYPE_AGENT_OF_MONITOR);
        // Dec 04, 2015

        if(isset($_POST['Users']))
        {
            $model->attributes=$_POST['Users'];
            //if($model->role_id==ROLE_ADMIN) // BY NGUYEN DUNG
            // $model->role_id = ROLE_MEMBER;
            $model->application_id = BE;
            $model->validate();
            $model->mUsersRef->attributes = $_POST['UsersRef'];
            $model->mUsersRef->image_sign  = CUploadedFile::getInstance($model->mUsersRef, 'image_sign');
            $model->mUsersRef->validate();
            UsersRef::validateFile($model);

            if(!$model->hasErrors() && !$model->mUsersRef->hasErrors()){
                $aUpdate = array('username','code_account','role_id','first_name','address',
                    'province_id','district_id','status','email',
                    'ward_id','house_numbers','street_id','parent_id','address_vi',
                    'sale_id','is_maintain','phone', 'last_name'
                    );

                if(in_array($model->role_id, $model->ARR_ROLE_USE_IS_MAINTAIN)):
                    $aUpdate[] = 'is_maintain'; // Apr 04, 2015 sẽ dùng cột này làm cờ để biết giám sát nào có thống kê target
                endif;

//                if(empty($model->code_account)){
//                    $model->code_account = MyFunctionCustom::getNextIdForEmployee('Users');
//                }// Close on Jun 17, 2016
                $newName = MyFunctionCustom::getNameRemoveVietnamese($model->first_name);
                $aUpdate[] = 'code_bussiness';
                if(strtolower($oldName) !=  strtolower($newName)){                            
                    //$needMore = array('id_not_in'=>$model->id);
                    $model->code_bussiness = MyFunctionCustom::getCodeBusinessEmployee($model->first_name, array());
                }
                if(isset($_POST['Users']['gender'])){
                    $aUpdate[] = 'gender';
                }

                if(!empty($model->password_hash)){
                    $model->temp_password = $model->password_hash;
                    $model->password_hash = md5($model->password_hash);
                    $aUpdate[] = 'temp_password';
                    $aUpdate[] = 'password_hash';
                }
                $model->update($aUpdate);
                $this->handleSaveUpdate($model);// Dec 04, 2015
                $this->handleUpdateRoleAppPromotion($model);// Dec 04, 2015
                GasAgentCustomer::saveEmployeeMaintainAgent($model->id);
                UsersPhone::savePhone($model);// May 25, 2016
                
//                GasOneMany::changeAgentEmployee($model);// Dec1317 không chạy hàm này ở update, dễ dẫn đến cập nhật sai đại lý cho giao nhận
                $mAppCache = new AppCache();
                $mAppCache->setUserCredit(AppCache::USER_CREDIT);
                $this->saveAgentMonitor($model);
                $this->setCache($model);
                if( $model->role_id == ROLE_SALE ){ // Jan 17, 2014 cập nhật loại sale cho table spancop
//                        Users::UpdateSaleType($model);
//                        die;
                }
                Yii::app()->user->setFlash( MyFormat::SUCCESS_UPDATE, "Cập nhật thành công." );
                $this->redirect(array('update','id'=>$model->id));
            }
        }

        $this->render('update',array(
            'model'=>$model, 'actions' => $this->listActionsCanAccess,
        ));
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }
    
    /** @Author: DungNT Sep 30, 2018
     * @Todo: set some cache file
     */
    public function setCache($mUser){
        $aRoleNotSet = [ROLE_CUSTOMER];
        if(!in_array($mUser->role_id, $aRoleNotSet)){
//            $mAppCache = new AppCache();
//            $mAppCache->setListdataUserByRole($mUser->role_id);// tạm close lại, khi nào dùng thì open
        }
        if(!array_key_exists($mUser->id, GasConst::getArrayGdkv())){
            return ;
        }
        $mAppCache = new AppCache();
        $mAppCache->setListdataCvAgent();
    }
    
    /** @Author: DungNT Dec 04, 2015
     * @Todo: xử lý save ảnh đại diện của member
     */
    public function handleSaveUpdate($model){
        $model->handleSaveUserRef();
    }
    
    /** @Author: DungNT Jul 09, 2018
     *  @Todo: update role_id sang model AppPromotion if change
     **/
    public function handleUpdateRoleAppPromotion($model){
        if($model->modelOld->role_id != $model->role_id){
            $mAppPromotion = new AppPromotion();
            $mAppPromotion->owner_id = $model->id;
            $mAppPromotion = $mAppPromotion->getByOwner();
            if(!empty($mAppPromotion)){
                $mAppPromotion->owner_role_id = $model->role_id;
                $mAppPromotion->update(['owner_role_id']);
            }
        }
    }
    
    /**
     * @Author: DungNT Oct 25, 2017
     * @Todo: xử lý save những agent mà nhân viên đang giám sát
     */
    public function saveAgentMonitor($mUser){
        if(isset($_POST['Users'])){
            $_POST['GasOneMany']['many_id'] = isset($_POST['Users']['agent_id_search']) ? $_POST['Users']['agent_id_search'] : [];
            GasOneMany::saveArrOfManyId($mUser->id, GasOneMany::TYPE_AGENT_OF_MONITOR);
        }
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id)
    {
        try{
            if(Yii::app()->request->isPostRequest){
                // we only allow deletion via POST request
                if($model = $this->loadModel($id))
                {
                    if($model->id < 10){
                        throw new Exception('Invalid request member id: '.$model->id);
                    }
                    if($model->delete())
                        Yii::log("Delete record ".  print_r($model->attributes, true), 'info');
                }

                // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
                if(!isset($_GET['ajax']))
                        $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('index'));
            }
            else
            {
                Yii::log("Invalid request. Please do not repeat this request again.");
                throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
            }
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }

    /**
     * Manages all models.
     */
    public function actionIndex(){
        $this->pageTitle = 'Member Login';
        try{
            $model=new Users('search');
            $model->unsetAttributes();  // clear any default values
            $model->status = STATUS_ACTIVE;
            if(isset($_GET['Users']))
                    $model->attributes=$_GET['Users'];

            $this->render('index',array(
                    'model'=>$model, 'actions' => $this->listActionsCanAccess,
            ));
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer the ID of the model to be loaded
     */
    public function loadModel($id){
        try{
            $model=Users::model()->findByPk($id);
            if($model===null)
            {
                Yii::log("The requested page does not exist.");
                throw new CHttpException(404,'The requested page does not exist.');
            }			
            return $model;
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }
    
    public function actionEmployees(){
//            MyFunctionCustom::updateCodeAccountUser();            
        $this->pageTitle = 'Quản lý nhân sự';
        if(isset($_GET['tuyendung'])){
            $this->pageTitle = 'Quản lý ứng viên tuyển dụng';
        }
        try{
            $model=new Users('employees');
            $model->unsetAttributes();  // clear any default values
            $model->mProfile = new UsersProfile();
            $model->mProfile->unsetAttributes();
            if(isset($_GET['Users']))
                $model->attributes=$_GET['Users'];
            if(isset($_GET['UsersProfile']))
                $model->mProfile->attributes=$_GET['UsersProfile'];
            if(isset($_GET['status_leave'])){
                $model->status_leave = $_GET['status_leave'];
            }

            $_SESSION['modelEmployees'] = $model;
            $this->exportExcel($model);
            
            $this->render('profile/Employees',array(
                    'model'=>$model, 'actions' => $this->listActionsCanAccess,
            ));
         }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }
    
    /** @Author: DungNT Aug 19, 2017
     * @note: map test user, đỡ phải nhập form khi dev test
     */
    public function mapEmployeeTest(&$model) {
        $mUser = Users::model()->findByPk(930457);
        $aFieldNotCopy = array('id');
        // handle add promotion of user
        MyFormat::copyFromToTable($mUser, $model, $aFieldNotCopy);
    }
    
    /** @Author: DungNT Now 23, 2018
     *  @Todo: auto gen code of user form 2 chỗ tạo 
     **/
    public function genCodeUser(&$mUser) {
        if(in_array($mUser->role_id, Roles::getRestrict())){
            return ;
        }
        $mUser->code_account    = MyFunctionCustom::getNextIdForEmployee('Users', 'HM', 6);
        $mUser->code_bussiness  = MyFunctionCustom::getCodeBusinessEmployee($mUser->first_name, []);
    }
    
    public function actionEmployees_create(){
        //                @Code: NAM005
        $mEmployeeImages = new EmployeesImages();
        $this->pageTitle = 'Tạo mới nhân sự';
        $model =new Users('employees_create');
        $model->mProfile = new UsersProfile('employees_create');
        $this->checkAccessEmployees($model);
        $this->mapSomeInfo($model);
        $this->resetScenario($model);
//        $this->mapEmployeeTest($model);
        if(isset($_POST['Users'])){
            $model->attributes              = $_POST['Users'];
            $model->mProfile->attributes    = $_POST['UsersProfile'];
            $model->parent_id               = empty($model->parent_id) ? '' : $model->parent_id;
            $this->handleUsername($model);
            $this->checkAgentForSomeUser($model);
            $this->resetScenario($model);
            $model->validate();
            $model->mProfile->validate();
            $model->validateByHand();
//            @Code: NAM005
            $mEmployeeImages->ValidateFile($model);
            $mEmployeeImages->checkRequiredCmnd($model, EmployeesImages::TYPE_CMND);
            if(!$model->hasErrors() && !$model->mProfile->hasErrors() && !$mEmployeeImages->hasErrors()){
                $model->application_id  = BE;
                $this->setSaleType($model);
                $this->genCodeUser($model);
                $model->saveEmployee();
                //                @Code: NAM005
                $mEmployeeImages->handleImage($model);
                
                GasScheduleEmail::employeeNew($model);
                Yii::app()->user->setFlash( MyFormat::SUCCESS_UPDATE, 'Thêm mới thành công nhân viên: '.$model->first_name);
                $this->redirect(['employees_create']);
            }
        }
        $this->render('profile/Employees_create', array(
                'model'=>$model, 'actions' => $this->listActionsCanAccess,
        ));
    }
    
    /** @Author: DungNT Jan 10, 2018
     *  @Todo: set loại sale bò với user là NV kinh doanh (Sale)
     **/
    public function setSaleType(&$mUser) {
        if($mUser->role_id == ROLE_SALE){
            $mUser->gender = Users::SALE_BO;
        }
    }
    public function resetScenario(&$mUser) {
        if($mUser->role_id == ROLE_FAMILY){
            if($mUser->isNewRecord){
                $mUser->scenario            = 'createFamily';
                $mUser->mProfile->scenario  = 'createFamily';
            }else{
                $mUser->scenario            = 'updateFamily';
                $mUser->mProfile->scenario  = 'updateFamily';
                $this->pageTitle            = 'Cập nhật người thân của nhân viên';
            }
        }
    }
    
    // khởi tạo một số thông tin default
    public function mapSomeInfo(&$mUser) {
        $cUid = MyFormat::getCurrentUid();
        $mUser->mProfile->country_id    = 1;
        $mUser->mProfile->type_nation   = 1;
        $mUser->mProfile->type_religion = 1;
        
        if(isset($_GET['idRef'])){
            $mUser->role_id         = ROLE_FAMILY;
            $mUser->area_code_id    = $_GET['idRef'];
            $mEmployee = Users::model()->findByPk($mUser->area_code_id);
            $mUser->parent_id       = $mEmployee->parent_id;
            $mUser->province_id     = $mEmployee->province_id;
            $mUser->district_id     = $mEmployee->district_id;
            $mUser->ward_id         = $mEmployee->ward_id;
            $mUser->house_numbers   = $mEmployee->house_numbers;
            $mUser->street_id       = $mEmployee->street_id;
            
            $this->layout           = 'ajax';
            $this->pageTitle        = 'Tạo mới người thân của nhân viên: '.$mEmployee->first_name;
        }
        
        if(in_array($cUid, $mUser->mProfile->getListUidCreate())){
            $mUser->role_id         = ROLE_CANDIDATE;
            $mUser->parent_id       = 768411;
            $mUser->mProfile->contract_type = UsersProfile::CONTRACT_WAIT_INTERVIEW;
        }
    }
    
    /** @Author: DungNT Sep 19, 2018
     *  @Todo: lock input set wrong agent for PVKH + PTTT
     **/
    public function checkAgentForSomeUser($model) {
        $aRoleCheck = [ROLE_EMPLOYEE_MAINTAIN, ROLE_EMPLOYEE_MARKET_DEVELOPMENT];
        if(in_array($model->role_id, $aRoleCheck) && in_array($model->parent_id, GasCheck::getAgentNotGentAuto())){
            throw new Exception('Nơi công tác của nhân viên không đúng, PVKH và PTTT phải có nơi công tác là đại lý, không phải kho trạm');
        }
    }
    
    public function checkAccessEmployees($model) {
        if(empty($model->mProfile)){
            return ;
        }
        if(!$model->mProfile->canAccess()){
            $this->redirect(array('employees'));
        }
    }

    public function actionEmployees_update($id){
        //                @Code: NAM005
        $mEmployeeImages = new EmployeesImages();
        $model = $this->loadModel($id);
        $model->modelOld    = clone $model;
        $this->pageTitle    = $model->first_name;
        $model->oldStatus   = $model->status;
        $oldName = MyFunctionCustom::getNameRemoveVietnamese($model->first_name);
        $model->scenario = 'employees_update';
        $model->loadEmployeeUpdate();
        if(!$model->canUpdateEmployee()){
            $this->redirect(array('employees'));
        }
        $model->LoadUsersRefImageSign();
        $model->mUsersRef->old_image_sign = $model->mUsersRef->image_sign;
        $this->checkAccessEmployees($model);
        $this->resetScenario($model);
        if(isset($_POST['Users'])){
            $model->attributes              = $_POST['Users'];
            $model->mProfile->attributes    = $_POST['UsersProfile'];
            $model->parent_id               = empty($model->parent_id) ? '' : $model->parent_id;
            $this->checkAgentForSomeUser($model);
            $this->resetScenario($model);
            $model->validate();
            // for validate file
            $model->mProfile->validate();
            $model->mUsersRef->attributes = $_POST['UsersRef'];
            $model->mUsersRef->image_sign  = CUploadedFile::getInstance($model->mUsersRef, 'image_sign');
            $model->mUsersRef->validate();
            UsersRef::validateFile($model);
            // for validate file
            
            $model->validateByHand();
            //                @Code: NAM005
            $mEmployeeImages->ValidateFile($model);
            if(!$model->hasErrors() && !$model->mProfile->hasErrors() && !$model->mUsersRef->hasErrors() && !$mEmployeeImages->hasErrors()){
                $newName    = MyFunctionCustom::getNameRemoveVietnamese($model->first_name);
                if(strtolower($oldName) !=  strtolower($newName)){
                    $model->code_bussiness = MyFunctionCustom::getCodeBusinessEmployee($model->first_name, []);
                }
                $model->saveEmployee();
                //                @Code: NAM005
                $mEmployeeImages->handleImage($model);
                
                $model->handleSaveUserRef();
                $model->mProfile->mUser    = $model;// AD May2118
                $model->mProfile->changeGas24hCode();// AD May2118
                if($model->role_id != ROLE_CANDIDATE){
                    $model->saveLogUpdate(LogUpdate::TYPE_7_USER);
                }
            
                if(!$model->mProfile->hasErrors()){
                    Yii::app()->user->setFlash( MyFormat::SUCCESS_UPDATE, 'Cập Nhật Thành Công Nhân Viên: '.$model->first_name);
                    $this->redirect(array('employees_update','id'=>$model->id));
                }
            }                                    
        }
        $this->render('profile/Employees_update',array(
            'model'=>$model, 'actions' => $this->listActionsCanAccess,
        ));
    }

    public function actionEmployees_view($id){
//        $this->layout='ajax';
        $model = $this->loadModel($id);
        $model->mProfile = $model->rUsersProfile;
//        $this->checkAccessEmployees($model);// Aug2318 tạm close lại cho view
        $this->pageTitle = $model->first_name;
        $this->render('profile/Employees_view',array(
            'model'=>$model, 'actions' => $this->listActionsCanAccess,
        ));

    }

    // 10-24-2013
    public function actionExport_list_employees(){}        
    /**
     * Handle the ajax request. This process changes the status of member to 1 (mean active)
     * @param type $id the id of member need changed status to 1
     */
    public function actionAjaxActivate($id) {
        if(Yii::app()->request->isPostRequest){
            $model = $this->loadModel($id);
            if(method_exists($model, 'activate'))
            {
                $model->activate();
            }
            Yii::app()->end();
        }else{
            Yii::log('Invalid request. Please do not repeat this request again.');
            throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
        }
    }

    /** Handle the ajax request. This process changes the status of member to 0 (mean deactive)
     * @param type $id the id of member need changed status to 0
     */
    public function actionAjaxDeactivate($id) {
        if(Yii::app()->request->isPostRequest){
            $model = $this->loadModel($id);
            if(method_exists($model, 'deactivate'))
            {
                $model->deactivate();
            }
            Yii::app()->end();
        }else{
            Yii::log('The requested page does not exist.');
            throw new CHttpException(404,'The requested page does not exist.');
        }
    }
    
    public function actionMail_reset_password($id){
        try{
            $model = $this->loadModel($id);
            SendEmail::ResetPasswordModelAndSendMail($model);
            Yii::app()->user->setFlash('successUpdate', "Gửi mail reset password thành công.");
            $this->redirect(array('index','id'=>$model->id));  
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }
    
    /** @Author: DungNT Aug 08, 2018 
     *  @Todo: set list agent for one user, any type need manage
     **/
    public function actionSetAgentForUser()
    {
        $this->pageTitle = 'Cập nhật đại lý cho NV';
        try{
            $cRole = MyFormat::getCurrentRoleId();
            $model = new GasOneMany();
            $model->type        = isset($_GET['type']) ? $_GET['type'] : '';
            $model->one_id      = isset($_GET['one_id']) ? $_GET['one_id'] : '';
            $model->date_apply  = isset($_GET['date_apply']) ? $_GET['date_apply'] : '';

            if( isset($_POST['GasOneMany'])){
                $model->attributes = $_POST['GasOneMany'];
                if(isset($_POST['ViewAll'])){
                    $model->voThucTeNo = true;
                }else{
//                GasOneMany::saveArrOfManyId($model->one_id, $model->type);
                    $model->saveArrOfManyIdFixDateApply();
                    $model->updateSomeCache();
                    Yii::app()->user->setFlash('successUpdate', 'Cập nhật thành công');
                    $this->redirect(array('setAgentForUser','type'=> $model->type, 'one_id'=> $model->one_id, 'date_apply'=> $model->getDateApply()));
                }
                
            }
            $this->render('setAgentForUser/index',array(
                    'model'=> $model, 'actions' => $this->listActionsCanAccess,
            ));
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }

    
    public function exportExcel($model){
        try{
            if(!$this->canExportExcelForEmployees() || !isset($_GET['ExportExcel'])){
                return ;
            }

            if(isset($_SESSION['dataEmployees'])){
                ini_set('memory_limit','2000M');
                $mToExcelList = new ToExcelList();
                $mToExcelList->exportGasMember();
            }
//        $this->redirect(array('index'));
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }
    
    /** @Author: NGUYEN KHANH TOAN Aug 15 ,2018
     *  @Todo: xet quyen cho phep xuat excel employees
     *  @Param:
     **/
    public function canExportExcelForEmployees() {
        $cUid       = MyFormat::getCurrentUid();
        $cRole      = MyFormat::getCurrentRoleId();
        $aUidAllow  = [
            GasLeave::UID_CHIEF_ACCOUNTANT, 
            GasConst::UID_HANH_NT, 
            GasConst::UID_ADMIN, 
            GasConst::UID_CHAU_LNM,
            2085473, // hungpt - Phạm Thanh Hùng - NV Kế Toán VP
        ];
        $aRoleAllow = [ROLE_ADMIN];
        return in_array($cUid, $aUidAllow) || in_array($cRole, $aRoleAllow);
    }
    
    /** @Author: KHANH TOAN 2018
     *  @Todo: get usersname từ first_name
     *  @Param:
     **/
    public function handleUsername(&$mUser) {
        if(empty($mUser->first_name) || in_array($mUser->role_id, Roles::getRestrict())){
            return ;
        }
        $user_name_origin = '';
        $first_name = GasLeaveHolidays::slugify($mUser->first_name);
        $first_name = strtolower($first_name);
        $words = explode("-", $first_name);
        $user_name_origin .= end($words);
        $count = count($words);
        foreach ($words as $key => $w) {
            if (--$count <= 0) {
                break;
            }
            $user_name_origin .= $w[0];
        }
        $number = 1;
        $user_name = $user_name_origin;
        while($this->checkUsernameExist($user_name)){
            $user_name = $user_name_origin.$number;
            $number ++;
        }
        $mUser->username = $user_name;
    }
    
    /** @Author: KHANH TOAN 2018
     *  @Todo: kiểm tra có tồn tại username chưa
     *         có => true, không => false
     *  @Param: $user_name
     **/
    public function checkUsernameExist($user_name) {
        $criteria = new CDbCriteria(); 
        $criteria->compare('t.username', $user_name);
        $sParamsIn = implode(',', Roles::getRestrict());
        $criteria->addCondition("t.role_id NOT IN ($sParamsIn)");
        $mUser = Users::model()->find($criteria);
        if(empty($mUser)){
            return false;
        }
        return true;
    }

    public function actionCheckFullProfile($id){
        $model=$this->loadModel($id);
        try{
            if(Yii::app()->request->isPostRequest){
                // we only allow deletion via POST request
                if($model = $this->loadModel($id))
                {
                    if($model->id < 10){
                        throw new Exception('Invalid request member id: '.$model->id);
                    }
                    if(!empty($model->rUsersProfile)){
                        $model->rUsersProfile->updateFullProfile();
                    }else{
                        throw new Exception('Invalid request member id: '.$model->id);
                    }
                }

                // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
                if(!isset($_GET['ajax']))
                        $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('index'));
            }
            else
            {
                Yii::log("Invalid request. Please do not repeat this request again.");
                throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
            }
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }
    
    /** @Author: NamLA Aug1419
     *  @Todo: action để update Lái xe, Xe tải, Phụ xe, Đại lý theo dõi
     *  @Param: $id user id
     **/
    public function actionUpdateDetail($id){
        try{
            $this->layout   = 'ajax';
            $model = $this->loadModel($id);
            $model->maintain_agent_id = GasAgentCustomer::getEmployeeMaintainAgent($model->id);
            $type           = empty($_GET['type']) ? GasOneMany::TAB_TYPE_DRIVER : $_GET['type'];
            if(isset($_POST['Users']['maintain_agent_id'])){ 
                     
                GasAgentCustomer::saveEmployeeMaintainAgent($model->id);
                Yii::app()->user->setFlash( MyFormat::SUCCESS_UPDATE, "Cập nhật thành công." );
                $this->redirect(["updatedetail" ,'id' => $id, 'type' => $type]);   
            } 
            if(isset($_POST['Users']['agent_id'])){
                $_POST['GasOneMany']['many_id'] = isset($_POST['Users']['agent_id']) ? $_POST['Users']['agent_id'] : [];
                switch($type){
                    case GasOneMany::TAB_TYPE_DRIVER:
                        GasOneMany::saveArrOfManyId($model->id, ONE_AGENT_DRIVER);
                        break;
                    case GasOneMany::TAB_TYPE_TRUCK:
                        GasOneMany::saveArrOfManyId($model->id, ONE_AGENT_CAR);
                        break;
                    case GasOneMany::TAB_TYPE_PHU_XE:
                        GasOneMany::saveArrOfManyId($model->id, ONE_AGENT_PHU_XE);
                        break;
                }

                Yii::app()->user->setFlash( MyFormat::SUCCESS_UPDATE, "Cập nhật thành công." );
                $this->redirect(["updatedetail" ,'id' => $id, 'type' => $type]);   
            }
            $this->render('updateType', ['model'=>$model,'type'=> $type]);
        } catch (Exception $ex) {
            GasCheck::CatchAllExeptiong($ex);
        }
    }

}
