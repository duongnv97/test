<?php

class TruckPlanController extends AdminController 
{
    public $pluralTitle = 'Lịch Giao Nhận Xe Bồn';
    public $singleTitle = 'Lịch Giao Nhận Xe Bồn';
    /**
     * Displays a particular model. 
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id)
    {
        $this->pageTitle = 'Xem ';
        try{
        $this->render('view',array(
                'model'=>$this->loadModel($id), 'actions' => $this->listActionsCanAccess,
        ));
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate()
    {
        $this->pageTitle = 'Tạo Mới '.$this->pluralTitle;
        try{
            $this->loadWarehouse();
            $model = new TruckPlan('create');
            $model->initDataCreate();
            if(isset($_POST['TruckPlan']))
            {
                $model->attributes  = $_POST['TruckPlan'];
                $model->validate();
                $model->validateWebCreate();
                if(!$model->hasErrors()){
                    $model->formatDataSave();
                    $model->save();
                    $model->saveDetailExport($_POST['TruckPlanDetail']);
                    Yii::app()->user->setFlash( MyFormat::SUCCESS_UPDATE, "Thêm mới thành công." );
                    $this->redirect(array('create'));
                }				
            }

            $this->render('create',array(
                    'model'=>$model, 'actions' => $this->listActionsCanAccess,
            ));
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id)
    {
        $this->pageTitle = 'Cập Nhật '.$this->pluralTitle;
        try{
        $model=$this->loadModel($id);
        $model->scenario = 'update';
        $model->formatDataBeforeRenderUpdate();
        if(isset($_POST['TruckPlan']))
        {
            $model->attributes=$_POST['TruckPlan'];
            $model->validate();
            $model->validateWebCreate();
            if(!$model->hasErrors()){
                $model->formatDataSave();
                $model->save();
                $model->saveDetailExport($_POST['TruckPlanDetail']);
                Yii::app()->user->setFlash( MyFormat::SUCCESS_UPDATE, "Cập nhật thành công." );
                $this->redirect(array('update','id'=>$model->id));
            }
        }

        $this->render('update',array(
                'model'=>$model, 'actions' => $this->listActionsCanAccess,
        ));
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id)
    {
        try{
        if(Yii::app()->request->isPostRequest){
            // we only allow deletion via POST request
            if($model = $this->loadModel($id))
            {
                if($model->delete()){
                    // Delete detail
                    $criteria = new CDbCriteria;
                    $criteria->compare('truck_plan_id',$id);
                    TruckPlanDetail::model()->deleteAll($criteria);
                    
                    Yii::log("Uid: " .Yii::app()->user->id. " Delete record ".  print_r($model->attributes, true), 'info');
                }
            }

            // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
            if(!isset($_GET['ajax']))
                    $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('index'));
        }
        else
        {
            Yii::log("Uid: " .Yii::app()->user->id. " Invalid request. Please do not repeat this request again.");
            throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
        }
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }

    /**
     * Manages all models.
     */
    public function actionIndex()
    {
        $this->pageTitle = 'Danh Sách ';
        try{
        $model=new TruckPlan('search');
        $model->unsetAttributes();  // clear any default values
        $this->exportExcel(); //LocNV Sep 26, 2019
        if(isset($_GET['TruckPlan']))
                $model->attributes=$_GET['TruckPlan'];

        $this->render('index',array(
                'model'=>$model, 'actions' => $this->listActionsCanAccess,
        ));
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
    * If the data model is not found, an HTTP exception will be raised.
     * @param integer the ID of the model to be loaded
     */
    public function loadModel($id)
    {
        try{
        $model=TruckPlan::model()->findByPk($id);
        if($model===null)
        {
            Yii::log("The requested page does not exist.");
            throw new CHttpException(404,'The requested page does not exist.');
        }			
        return $model;
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }
    
    /** @Author: DuongNV May 28,19
     *  @Todo: load ware house khi select supplier
     **/
    public function loadWarehouse() {
        if( isset($_GET['loadWareHouse']) ){
            $model              = new TruckPlan();
            $model->supplier_id = isset($_GET['supplier_id']) ? $_GET['supplier_id'] : 0;
            $aWarehouse         = $model->getArraySupplierWarehouse();
            $html               = '';
            foreach ($aWarehouse as $id => $value) {
                $html .= "<option value='$id'>$value</option>";
            }
            echo $html;
            die;
        }
    }

    /**
     * @author LocNV Sep 25, 2019
     * @throws CHttpException
     */
    public function exportExcel()
    {
        try{
            if(isset($_GET['to_excel']) && isset($_SESSION['data-excel-truck-plan'])){
                ini_set('memory_limit','1500M');// chưa cần chạy đoạn này, đưa memory_limit lên 500 có thể export dc khoảng 5000 row - chưa test
                $toExcel = new ToExcel();
                $toExcel->summaryTruckPlan();
            }
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }
}
