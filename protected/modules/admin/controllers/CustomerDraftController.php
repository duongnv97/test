<?php

class CustomerDraftController extends AdminController 
{
    public $pluralTitle = 'KH Excel';
    public $singleTitle = 'KH Excel';

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate()
    {
        error_reporting(1);
        ini_set('memory_limit','3000M');
        $this->pageTitle = 'Tạo Mới ';
        try{
        $model=new CustomerDraft('create');
//        $model->type = $model->TYPE_TELESALE_FIND;
        $model->type = $model->TYPE_BUY;
        $model->province_id = 1;
//        $model->district_id = 13;
//        $model->owner_id = 1392015; // Dương Trung Kiên - Marketing

        if(isset($_POST['CustomerDraft']))
        {
            $model->attributes=$_POST['CustomerDraft'];
            $model->validate();
            if(!$model->hasErrors()){
                $aError = $model->saveFromExcel();
                Yii::app()->user->setFlash( MyFormat::SUCCESS_UPDATE, $model->getViewPhoneDuplicate($aError) );
                $this->redirect(array('create'));
            }				
        }

        $this->render('create',array(
                'model'=>$model, 'actions' => $this->listActionsCanAccess,
        ));
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id)
    {
        try{
        if(Yii::app()->request->isPostRequest){
            // we only allow deletion via POST request
            if($model = $this->loadModel($id))
            {
                if($model->delete())
                    Yii::log("Uid: " .Yii::app()->user->id. " Delete record ".  print_r($model->attributes, true), 'info');
            }

            // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
            if(!isset($_GET['ajax']))
                    $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('index'));
        }
        else
        {
            Yii::log("Uid: " .Yii::app()->user->id. " Invalid request. Please do not repeat this request again.");
            throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
        }	
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }

    /**
     * Manages all models.
     */
    public function actionIndex()
    {
        $this->pageTitle = 'Danh Sách ';
        try{
        $cRole = MyFormat::getCurrentRoleId();
        $cUid = MyFormat::getCurrentUid();
        $model=new CustomerDraft('search');
        $model->unsetAttributes();  // clear any default values
        $model->province_id = 1;
        $mTelesale = new Telesale();
        if(!empty($_GET['type'])){
            $model->type_search = $_GET['type'];
        }

        if(isset($_GET['type']) && $_GET['type'] == $model->call_again){
            $model->date_call_from  = date('d-m-Y');
            $model->date_call_to    = $model->date_call_from;
        }
        if(isset($_GET['type']) && $cRole == ROLE_CALL_CENTER){
            $model->employee_id    = $cUid;
        }

        if(isset($_GET['CustomerDraft'])){
            $model->attributes=$_GET['CustomerDraft'];
            if(isset($_GET['type']) && $cRole == ROLE_CALL_CENTER && !$mTelesale->showSetSaleId() ){
                $model->employee_id    = $cUid;
            }
        }
        
        
        
        $this->handleGenerateCustomer();
        $this->handleApplyCustomer();
        $this->render('index',array(
                'model'=>$model, 'actions' => $this->listActionsCanAccess,
        ));
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }
    
    public function handleApplyCustomer(){
        if(isset($_POST['CustomerDraft']['customer'])){
            $sale_id = $_POST['CustomerDraft']['employee_id'];
            $number_customer = 0;
            foreach ($_POST['CustomerDraft']['customer'] as $ctm_id) {
                $mCustomerDraft = $this->loadModel($ctm_id);
                $mCustomerDraft->employee_id = $sale_id;
                $mCustomerDraft->save();
                $number_customer++;
            }
            $type = isset($_GET['type']) ? $_GET['type'] : '';
            if($sale_id == ''){
                Yii::app()->user->setFlash( MyFormat::SUCCESS_UPDATE, "BỎ GÁN THÀNH CÔNG ".$number_customer." KHÁCH HÀNG" );
                $this->redirect(array('index', 'type'=>$type));
            }
            Yii::app()->user->setFlash( MyFormat::SUCCESS_UPDATE, "GÁN THÀNH CÔNG ".$number_customer." KHÁCH HÀNG" );
                $this->redirect(array('index', 'type'=>$type));
        }
    }
    
    public function handleGenerateCustomer(){
        if(!empty($_POST['CustomerDraft']['generate']) ){
            $sale_id = $_POST['CustomerDraft']['employee_id'];
            if(!empty($sale_id)){
                $mCustomerDraft = new CustomerDraft();
                $mCustomerDraft->employee_id = $sale_id;
                $mCustomerDraft->generateCusForEmployee();
                Yii::app()->user->setFlash( MyFormat::SUCCESS_UPDATE, "GÁN THÀNH CÔNG ".$mCustomerDraft->countGenerate." KHÁCH HÀNG" );
                $this->redirect(array('index'));
            }
        }
    }
    
    /** @Author: HOANG NAM 09/07/2018
     *  @Todo: report employees call draft
     **/
    public function actionReportDraft(){
        $this->pageTitle = 'KH Excel báo cáo cuộc gọi';
        $model=new CustomerDraft('search');
        $model->unsetAttributes();  // clear any default values
        $model->date_call_from  = '01-'.date('m-Y');
        $model->date_call_to    = date('d-m-Y');;
        if(isset($_GET['CustomerDraft'])){
            $model->attributes=$_GET['CustomerDraft'];
        }
        $this->render('report/report_draft',array(
            'model'=>$model, 
            'aData'=>$model->searchReport(), 
            'actions' => $this->listActionsCanAccess,
        ));
    }
    
    /** @Author: HOANG NAM 13/07/2018
     *  @Todo: report owner draft
     **/
    public function actionReportOwnerDraft(){
        $this->pageTitle = 'KH Excel báo cáo nguồn data';
        $model=new CustomerDraft('search');
        $model->unsetAttributes();  // clear any default values
        $model->date_call_from  = '01-07-2018';
        $model->date_call_to    = '03-08-2018';
        if(isset($_GET['CustomerDraft'])){
            $model->attributes=$_GET['CustomerDraft'];
        }
        $this->render('report/report_owner',array(
            'model'=>$model, 
            'aData'=>$model->searchOwnerReport(), 
            'actions' => $this->listActionsCanAccess,
        ));
    }
    
    /**
     * Returns the data model based on the primary key given in the GET variable.
    * If the data model is not found, an HTTP exception will be raised.
     * @param integer the ID of the model to be loaded
     */
    public function loadModel($id)
    {
        try{
        $model=CustomerDraft::model()->findByPk($id);
        if($model===null)
        {
            Yii::log("The requested page does not exist.");
            throw new CHttpException(404,'The requested page does not exist.');
        }			
        return $model;
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }

}
