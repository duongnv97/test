<?php

class GasScheduleSmsHistoryController extends AdminController 
{
    public $pluralTitle = 'SMS';
    public $singleTitle = 'SMS';

    public function actionReport()
    {
        $this->pageTitle = 'Thống kê ';
        try{
        $model = new GasScheduleSmsHistory('search');
        $model->unsetAttributes();  // clear any default values
        $this->exportExcel();
        $model->date_from   = date('01-01-Y');
        $model->date_to     = date('d-m-Y');
        if(isset($_GET['type']) && $_GET['type'] == NotifyReport::TAB_TYPE_DETAIL){
            $model->date_from   = date('01-m-Y');
            $model->role_id     = ROLE_TELESALE;
        }
        
        if(isset($_GET['GasScheduleSmsHistory']))
            $model->attributes = $_GET['GasScheduleSmsHistory'];
        
        $this->render('report/report',array(
                'model'=>$model, 'actions' => $this->listActionsCanAccess,
        ));
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }
    
    /** @Author: DuongNV Oct 5, 11
     *  @Todo: excel report
     **/
    public function exportExcel() {
        try{
            if((isset($_SESSION['data-excel-general']) || isset($_SESSION['data-excel-detail'])) && isset($_GET['toExcel'])){
                ini_set('memory_limit','1500M');// chưa cần chạy đoạn này, đưa memory_limit lên 500 có thể export dc khoảng 5000 row - chưa test
                ToExcel::summarySmsHistoryReport($_GET['type']);
                $this->redirect(array('report'));
            }
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }
}
