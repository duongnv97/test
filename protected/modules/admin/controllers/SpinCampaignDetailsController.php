<?php

class SpinCampaignDetailsController extends AdminController
{
    public $pluralTitle = 'Giải Thưởng';
    public $singleTitle = 'Giải Thưởng';
    public function actionIndex()
    {
        $this->pageTitle = 'Quản Lý Giải Thưởng';
        try{
        $model=new SpinCampaignDetails('search');  
        $model->unsetAttributes();  // clear any default values
        if(isset($_GET['SpinCampaignDetails'])){
                $model->attributes=$_GET['SpinCampaignDetails'];
        }
         $this->render('index',array(
                'model'=>$model, 'actions' => $this->listActionsCanAccess,
        ));
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }
    
    public function actionCancelAward($id){
        try{ 
            if($model = $this->loadModel($id))
            {
                $model->status = SpinCampaignDetails::STATUS_CANCEL;
                $model->update();
            }
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }
    
    public function loadModel($id){
        try{
            $model = new SpinCampaignDetails;
            $model = $model->findByPk($id);
            if($model===null)
            {
                Yii::log("The requested page does not exist.");
                throw new CHttpException(404,'The requested page does not exist.');
            }			
            return $model;
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }
    
    public function actionUpdate($id)
    {
        $this->pageTitle = 'Cập Nhật Giải Thưởng';
        
        try{
        $model=$this->loadModel($id);
        if(isset($_POST['SpinCampaignDetails']))
        {
            $model->attributes=$_POST['SpinCampaignDetails'];
            $model->validate();
            if(!$model->hasErrors()){
                $model->getAwardJson();
                $model->save();
                Yii::app()->user->setFlash( MyFormat::SUCCESS_UPDATE, "Cập nhật thành công." );
                $this->redirect(array('update','id'=>$model->id));
            }							
        }
        $this->render('update',array(
                'model'=>$model, 'actions' => $this->listActionsCanAccess,
        ));
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }

}