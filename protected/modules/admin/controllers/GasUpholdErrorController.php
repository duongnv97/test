<?php
/** @Author: HOANG NAM
 *  @Code: NAM009
 **/
class GasUpholdErrorController extends AdminController
{
    public $pluralTitle = "Lỗi bảo trì";
    public $singleTitle = "Lỗi bảo trì";
    
    public function actionIndex()
    {
        $this->pageTitle = 'Danh sách lỗi bảo trì';
        try{
            $model = new GasUpholdError('search');
            $model->unsetAttributes();  // clear any default values
            if(isset($_GET['GasUpholdError'])) {
                $model->attributes = $_GET['GasUpholdError'];
            }
            $this->render('index',array(
                    'model'=>$model, 'actions' => $this->listActionsCanAccess,
            ));
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }
    
    public function actionView($id)
    {
        $this->pageTitle = 'Xem lỗi bảo trì';
        try{
            $model = $this->loadModel($id);
            $url = Yii::app()->createAbsoluteUrl('admin/gasUpholdSchedule/view', ['id'=>$model->uphold_id]);
            $this->redirect($url);
            $this->render('view',array(
                'model'=>$this->loadModel($id), 'actions' => $this->listActionsCanAccess,
            ));
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }
    /** @Author: HOANG NAM 09/04/2018
     *  @Todo: generate model
     *  @Param: gasUpholdError/generateError
     **/
//    public function actionGenerateError()
//    {
////        $model = new GasUpholdError();
////        $model->generateError();
//        Yii::app()->user->setFlash( MyFormat::SUCCESS_UPDATE, 'Test cron check lỗi bảo trì thành công' );
//        $this->redirect(array('index'));
//    }
    
    public function actionDelete($id)
    {
       try {
            if (Yii::app()->request->isPostRequest) {
                // we only allow deletion via POST request
                if ($model = $this->loadModel($id)) {
                    if ($model->delete())
                        Yii::log("Uid: " . Yii::app()->user->id . " Delete record " . print_r($model->attributes, true), 'info');
                }

                // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
                if (!isset($_GET['ajax']))
                    $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('index'));
            }
            else {
                Yii::log("Uid: " . Yii::app()->user->id . " Invalid request. Please do not repeat this request again.");
                throw new CHttpException(400, 'Invalid request. Please do not repeat this request again.');
            }
        } catch (Exception $exc) {
            GasCheck::CatchAllExeptiong($exc);
        }
    }
    
    public function loadModel($id)
    {
        try{
            $model = GasUpholdError::model()->findByPk($id);
            if($model===null)
            {
                Yii::log("The requested page does not exist.");
                throw new CHttpException(404,'The requested page does not exist.');
            }			
            return $model;
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }
    
}