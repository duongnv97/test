<?php
class AjaxSocketController extends AdminController
{
    public function accessRules(){
        return array(
            array('allow',   //allow authenticated user to perform actions
                'users'=>array('@'),
            ),
            array('deny',  // deny all users
                    'users'=>array('*'),
            ),
        );
    }
    /**
     * @Author: ANH DUNG Nov 29, 2016
     * @Todo: all notify will handle here
     */
    public function actionHandleNotify(){
        try{
            if(!isset($_POST['code']) || !isset($_POST['obj_id']) || !isset($_POST['remote_id']) || !isset($_POST['server_io_notify_id'])){
                throw new Exception('missing param code or obj_id or...');
            }
            $aCodeNotCheck = [GasSocketNotify::CODE_APP_ORDER_NEW];
            // Feb 24, 2017 phải xác nhận trc khi chạy hàm dưới, vì hàm dưới có thể bị lỗi thực tế là bị xóa rồi, nên bị die luôn
            $mSocketNotifyOnlyUpdate = new GasSocketNotify();
            if(!in_array($_POST['code'], $aCodeNotCheck)){
                $mSocketNotifyOnlyUpdate->updateClientReceivedNotify($_POST['server_io_notify_id']);// cập nhật client đã nhận dc để server kia không gửi tiếp nữa
                $mSocketNotify = GasSocketNotify::model()->findByPk($_POST['remote_id']);
                if(is_null($mSocketNotify)){
                    throw new Exception('actionHandleNotify null model GasSocketNotify => remote_id: '.$_POST['remote_id'] );
                }
            }
            
            /* 1. update back to server io status = 1(receive )
             * 2. handle build json return
             */
            switch ($_POST['code']) {
                case GasSocketNotify::CODE_STORECARD:
                case GasSocketNotify::CODE_STORECARD_DIEUPHOI:
                    // nothing to do
                    break;
                case GasSocketNotify::CODE_APP_ORDER_NEW:
                    $this->notifyAppTransactionNew($_POST['obj_id']);
                    break;

                default:
                    break;
            }
            
            
            die;
        }catch (Exception $exc){
            GasCheck::CatchAllExceptionJson($exc);
        }
    }

    
    /**
     * @Author: ANH DUNG Nov 29, 2016
     * @Todo: handle build json return
     */
    public function notifyAppTransactionNew($obj_id) {
        try{
        $mTransactionHistory = TransactionHistory::model()->findByPk($obj_id);
        if(is_null($mTransactionHistory)){
            throw new Exception('notifyAppTransactionNew null model TransactionHistory => id: '.$obj_id);
        }
        $html = '<tr class="odd">';
        $html .= '<td>'.$mTransactionHistory->getImageNew().'</td>';
        $html .= '<td>'.$mTransactionHistory->getHtmlDetail().'</td>';
        $html .= '<td class="w-200">'.$mTransactionHistory->getDetailView()."<br>".$mTransactionHistory->getUrlHideOrder()."<br>".$mTransactionHistory->getNote()."<br>".$mTransactionHistory->getEmployeeAccounting().'</td>';
        $html .= '<td class="w-100">'.$mTransactionHistory->getHtmlAction().'</td>';
        
//        $html .= '<td>'.$mTransactionHistory->getPhone().'</td>';
////        $html .= '<td>'.$mTransactionHistory->getEmail().'</td>';
//        $html .= '<td>'.$mTransactionHistory->getAddress().'</td>';
//        $html .= '<td>'.$mTransactionHistory->getNote().'</td>';
//        $html .= '<td>'.$mTransactionHistory->getClassRowSpan().$mTransactionHistory->getDetailView().'</td>';
//        $html .= '<td>'.$mTransactionHistory->getChangeStatus().'</td>';
//        $html .= '<td>'.$mTransactionHistory->getUrlMakeOrder().'</td>';
//        $html .= '<td>'.$mTransactionHistory->getCreatedDate().'</td>';
        $html .= '</tr>';
        
        $json = CJavaScript::jsonEncode(array('success'=>true,'mgs'=>'1','html'=>$html, 'ClassRowSpan'=>$mTransactionHistory->getClassRowName()));
        echo $json;die;
        }catch (Exception $exc){
            throw new Exception($exc->getMessage());
        }
    }

    
}