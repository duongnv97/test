<?php

class PhotosController extends AdminController
{


	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
    /*
	public function actionCreate($gallery_id)
	{
        try
        {
            $model=new Photos('create');
            $gallery = Galleries::model()->findByPk($gallery_id);

            // Uncomment the following line if AJAX validation is needed
            // $this->performAjaxValidation($model);

            if(isset($_POST['Photos']))
            {
                $model->attributes  = $_POST['Photos'];
                $model->gallery_id = $gallery_id;
                $model->description = $_POST['Photos']['description'];
                $model->created_date    = date('Y-m-d H:i:s');

                $uploadFile = CUploadedFile::getInstance($model,'file_name');
                if(!empty($uploadFile)){
                    $fileName = preg_replace('/\.\w+$/', '', $uploadFile->name);
                    $fileName = $fileName . '_' . time() . '.' . $uploadFile->extensionName;
                    $model->file_name = $fileName;
                }else{
                    $uploadFile = "";
                    $model->file_name = "";
                }


                if($model->save()){
                    if(!empty($uploadFile)){
                        if (!file_exists(Yii::getPathOfAlias('webroot'). '/upload/admin/galleries'))
                            mkdir(Yii::getPathOfAlias('webroot'). '/upload/admin/galleries');
                        if (!file_exists(Yii::getPathOfAlias('webroot'). '/upload/admin/galleries/thumbs'))
                            mkdir(Yii::getPathOfAlias('webroot'). '/upload/admin/galleries/thumbs');
                        if (!file_exists(Yii::getPathOfAlias('webroot'). '/upload/admin/galleries/large'))
                            mkdir(Yii::getPathOfAlias('webroot'). '/upload/admin/galleries/large');

                        $uploadFile->saveAs(Yii::getPathOfAlias('webroot'). '/upload/admin/galleries/' . $fileName);

                        $ImageProcessing = new ImageProcessing();
                        $ImageProcessing->folder = '/upload/admin/galleries';

                        $ImageProcessing->file = $fileName;
                        $ImageProcessing->thumbs = array(
                            'thumbs' => array('width' => IMAGE_ADMIN_GALLERY_THUMB_WIDTH, 'height' =>
                            IMAGE_ADMIN_GALLERY_THUMB_HEIGHT),
                            'large' => array('width' => IMAGE_ADMIN_GALLERY_WIDTH, 'height' =>
                            IMAGE_ADMIN_GALLERY_HEIGHT),
                        );
                        $ImageProcessing->create_thumbs();

                    }
                    $this->redirect(array('index','gallery_id'=>$gallery_id));
                }
            }

            $this->render('create',array(
                'model'=>$model,
                'gallery_id'=>$gallery_id,
                'gallery'=>$gallery,
                'actions' => $this->listActionsCanAccess,
            ));
        }
        catch (Exception $e)
        {
                    Yii::log("Exception ".  $e->getMessage(), 'error');
                    $code = 404;
                    if(isset($e->statusCode))
                        $code=$e->statusCode;
                    if($e->getCode())
                        $code=$e->getCode();
                    throw new CHttpException($code, $e->getMessage());
        }
	}
    */

    public function actionUpload($gallery_id)
    {
        if(isset($_FILES['file_name']))
        {
            //upload new files
            foreach($_FILES['file_name']['name'] as $key=>$filename){
                //move_uploaded_file($_FILES['files']['tmp_name'][$key],Yii::app()->params['uploadDir'].$filename);

                $model=new Photos();
                //$model->attributes=$_POST['Photos'];
                $model->gallery_id = $gallery_id;
                $model->created_date    = date('Y-m-d H:i:s');

                $uploadFile = CUploadedFile::getInstanceByName('file_name['.$key.']');
                if($uploadFile->size > 2*1024*1024){
                    Yii::app()->user->setFlash('error', "The ".$filename." was larger than 2MB. Please upload a smaller file.");
                }
                else{
                    if(!empty($uploadFile)){

                        $fileName = preg_replace('/\.\w+$/', '', $uploadFile->name);
                        $fileName = $fileName . '_' . time() . '.' . $uploadFile->extensionName;
                        $model->file_name = $fileName;
                    }else{
                        $uploadFile = "";
                        $model->file_name = "";
                    }
                    if($model->save()){
                        if(!empty($uploadFile)){
                            if (!file_exists(Yii::getPathOfAlias('webroot'). '/upload/admin/galleries'))
                                mkdir(Yii::getPathOfAlias('webroot'). '/upload/admin/galleries');
                            if (!file_exists(Yii::getPathOfAlias('webroot'). '/upload/admin/galleries/thumbs'))
                                mkdir(Yii::getPathOfAlias('webroot'). '/upload/admin/galleries/thumbs');
                            if (!file_exists(Yii::getPathOfAlias('webroot'). '/upload/admin/galleries/large'))
                                mkdir(Yii::getPathOfAlias('webroot'). '/upload/admin/galleries/large');

                            $uploadFile->saveAs(Yii::getPathOfAlias('webroot'). '/upload/admin/galleries/' . $fileName);

                            $ImageProcessing = new ImageProcessing();
                            $ImageProcessing->folder = '/upload/admin/galleries';

                            $ImageProcessing->file = $fileName;
                            $ImageProcessing->thumbs = array(
                                'thumbs' => array('width' => IMAGE_ADMIN_THUMB_WIDTH, 'height' =>
                                IMAGE_ADMIN_THUMB_HEIGHT),
                                'large' => array('width' => IMAGE_ADMIN_WIDTH, 'height' =>
                                IMAGE_ADMIN_HEIGHT),
                            );
                            $ImageProcessing->create_thumbs();

                        }
                    }

                }
            }
            $this->redirect(array('index','gallery_id'=>$gallery_id));

        }else
            throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');

    }


	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
        try
        {
            $model=$this->loadModel($id);
            $oldImage = $model->file_name;
            $model->scenario = 'update';

            $gallery = Galleries::model()->findByPk($model->gallery_id);

            // Uncomment the following line if AJAX validation is needed
            // $this->performAjaxValidation($model);

            if(isset($_POST['Photos']))
            {
                $model->attributes=$_POST['Photos'];
                $uploadFile = CUploadedFile::getInstance($model,'file_name');
                if(!empty($uploadFile)){
                    $fileName = preg_replace('/\.\w+$/', '', $uploadFile->name);
                    $fileName = $fileName . '_' . time() . '.' . $uploadFile->extensionName;
                    $model->file_name = $fileName;
                }else{
                    $uploadFile = "";
                    $model->file_name = $oldImage;
                }
                if($model->save()){
                    if(!empty($uploadFile)){
                        if (!file_exists(Yii::getPathOfAlias('webroot'). '/upload/admin/galleries'))
                            mkdir(Yii::getPathOfAlias('webroot'). '/upload/admin/galleries');
                        if (!file_exists(Yii::getPathOfAlias('webroot'). '/upload/admin/galleries/thumbs'))
                            mkdir(Yii::getPathOfAlias('webroot'). '/upload/admin/galleries/thumbs');
                        if (!file_exists(Yii::getPathOfAlias('webroot'). '/upload/admin/galleries/large'))
                            mkdir(Yii::getPathOfAlias('webroot'). '/upload/admin/galleries/large');

                        $uploadFile->saveAs(Yii::getPathOfAlias('webroot'). '/upload/admin/galleries/' . $fileName);

                        $ImageProcessing = new ImageProcessing();
                        $ImageProcessing->folder = '/upload/admin/galleries';

                        $ImageProcessing->file = $fileName;
                        $ImageProcessing->thumbs = array(
                            'thumbs' => array('width' => IMAGE_ADMIN_THUMB_WIDTH, 'height' =>
                            IMAGE_ADMIN_THUMB_HEIGHT),
                            'large' => array('width' => IMAGE_ADMIN_WIDTH, 'height' =>
                            IMAGE_ADMIN_HEIGHT),
                        );
                        $ImageProcessing->create_thumbs();

                    }
                    $this->redirect(array('index','gallery_id'=>$model->gallery_id));
                }
            }

            $this->render('update',array(
                'model'=>$model,
                'actions' => $this->listActionsCanAccess,
                'gallery'=>$gallery,
            ));
        }
        catch (Exception $e)
        {
                    Yii::log("Exception ".  $e->getMessage(), 'error');
                    $code = 404;
                    if(isset($e->statusCode))
                        $code=$e->statusCode;
                    if($e->getCode())
                        $code=$e->getCode();
                    throw new CHttpException($code, $e->getMessage());
        }
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
        try
        {
            if(Yii::app()->request->isPostRequest)
            {
                // we only allow deletion via POST request
                if($model = $this->loadModel($id))
                {
                    $model = $this->loadModel($id);
                    if (!file_exists(Yii::getPathOfAlias('webroot'). '/upload/admin/galleries/'.$model->file_name))
                        unlink(Yii::getPathOfAlias('webroot'). '/upload/admin/galleries/'.$model->file_name);
                    if (!file_exists(Yii::getPathOfAlias('webroot'). '/upload/admin/galleries/thumbs/'.$model->file_name))
                        unlink(Yii::getPathOfAlias('webroot'). '/upload/admin/galleries/thumbs/'.$model->file_name);
                    if (!file_exists(Yii::getPathOfAlias('webroot'). '/upload/admin/galleries/large/'.$model->file_name))
                        unlink(Yii::getPathOfAlias('webroot'). '/upload/admin/galleries/large/'.$model->file_name);
                    if($model->delete())
                        Yii::log("Delete record ".  print_r($model->attributes, true), 'info');
                }

                // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
                if(!isset($_GET['ajax']))
                    $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('index'));
            }
            else
            {
                Yii::log("Invalid request. Please do not repeat this request again.");
                throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
            }
        }
        catch (Exception $e)
        {
                    Yii::log("Exception ".  $e->getMessage(), 'error');
                    $code = 404;
                    if(isset($e->statusCode))
                        $code=$e->statusCode;
                    if($e->getCode())
                        $code=$e->getCode();
                    throw new CHttpException($code, $e->getMessage());
        }
	}

	/**
	 * Manages all models.
	 */
	public function actionIndex($gallery_id)
	{
        try
        {
            $model=new Photos('search');
            $model->unsetAttributes();  // clear any default values
            $model->gallery_id = (int)$gallery_id;
            $gallery = Galleries::model()->findByPk($gallery_id);
            if(isset($_GET['Photos'])){
                $model->attributes=$_GET['Photos'];
                $model->description = $_GET['Photos']['description'];
            }

            $this->render('index',array(
                'model'=>$model,
                'gallery_id'=>$gallery_id,
                'gallery'=>$gallery,
                'actions' => $this->listActionsCanAccess,
            ));
        }
        catch (Exception $e)
        {
                    Yii::log("Exception ".  $e->getMessage(), 'error');
                    $code = 404;
                    if(isset($e->statusCode))
                        $code=$e->statusCode;
                    if($e->getCode())
                        $code=$e->getCode();
                    throw new CHttpException($code, $e->getMessage());
        }
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the ID of the model to be loaded
	 */
	public function loadModel($id)
	{
        try
        {
            $model=Photos::model()->findByPk($id);
            if($model===null)
            {
                Yii::log("The requested page does not exist.");
                throw new CHttpException(404,'The requested page does not exist.');
            }
            return $model;
        }
        catch (Exception $e)
        {
                    Yii::log("Exception ".  $e->getMessage(), 'error');
                    $code = 404;
                    if(isset($e->statusCode))
                        $code=$e->statusCode;
                    if($e->getCode())
                        $code=$e->getCode();
                    throw new CHttpException($code, $e->getMessage());
        }
	}

	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
        try
        {
            if(isset($_POST['ajax']) && $_POST['ajax']==='photos-form')
            {
                echo CActiveForm::validate($model);
                Yii::app()->end();
            }
        }
        catch (Exception $e)
        {
                    Yii::log("Exception ".  $e->getMessage(), 'error');
                    $code = 404;
                    if(isset($e->statusCode))
                        $code=$e->statusCode;
                    if($e->getCode())
                        $code=$e->getCode();
                    throw new CHttpException($code, $e->getMessage());
        }
	}
}
