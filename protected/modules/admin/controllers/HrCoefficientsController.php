<?php

class HrCoefficientsController extends AdminController {

    public $pluralTitle = "Hệ số";
    public $singleTitle = "Hệ số";

    public function actionIndex() {
        $this->pageTitle = 'Danh sách hệ số';
        try {
            $model = new HrCoefficients('search');
            $model->unsetAttributes();  // clear any default values
            //for search
            if (isset($_GET['HrCoefficients'])) {
                $model->attributes = $_GET['HrCoefficients'];
            }

            $this->render('index', array(
                'model' => $model, 'actions' => $this->listActionsCanAccess,
            ));
        } catch (Exception $exc) {
            GasCheck::CatchAllExeptiong($exc);
        }
    }
    
    public function actionUpdateAjax($role = '')
    {
            $data = array();
            $model=new HrCoefficients('search');
            
            if(isset($_GET['HrCoefficients'])) {
                $model->attributes = $_GET['HrCoefficients'];
            }
            $data['model'] = $model;
            $data['role'] = $role;
            $this->renderPartial('_ajaxIndex', $data, false, true);
    }

    public function actionView($id) {
        $this->pageTitle = 'Xem hệ số';
        try {
            $this->render('view', array(
                'model' => $this->loadModel($id), 'actions' => $this->listActionsCanAccess,
            ));
        } catch (Exception $exc) {
            GasCheck::CatchAllExeptiong($exc);
        }
    }

    public function actionCreate($ajax = false) {
        $this->pageTitle = 'Tạo Mới hệ số';
        if ($ajax) {
            $this->layout = 'ajax';
        }
        try {
            $model = new HrCoefficients('create');
            $model->unsetAttributes();
            if (isset($_POST['HrCoefficients'])) {
                $model->attributes = $_POST['HrCoefficients'];
                $model->status = 1;
                $model->validate();
                if (!$model->hasErrors()) {
                    $model->position_work_list = implode(",", $model->position_work_list);
                    $model->save();
                    Yii::app()->user->setFlash(MyFormat::SUCCESS_UPDATE, "Thêm mới thành công.");
                    $this->redirect(array('create', 'ajax' => $ajax));
                }
            }

            $this->render('create', array(
                'model' => $model ,'actions' => $this->listActionsCanAccess,
            ));
        } catch (Exception $exc) {
            GasCheck::CatchAllExeptiong($exc);
        }
    }

    public function actionUpdate($id, $ajax = false) {
        try {
            $model = $this->loadModel($id);
            if ($ajax) {
            $this->layout = 'ajax';
            }
            $model->scenario = 'update';
            $model->position_work_list = explode(",", $model->position_work_list);
            if (isset($_POST['HrCoefficients'])) {
                $model->attributes = $_POST['HrCoefficients'];
                $model->validate();
                if (!$model->hasErrors()) {
                    $model->position_work_list = implode(",", $model->position_work_list);
                    $model->update();
                    Yii::app()->user->setFlash(MyFormat::SUCCESS_UPDATE, "Cập nhật thành công.");
                    $this->redirect(array('update', 'id'=>$model->id,'ajax' => $ajax));
                }
            }

            $this->render('update', array(
                'model' => $model, 'actions' => $this->listActionsCanAccess,
            ));
        } catch (Exception $exc) {
            GasCheck::CatchAllExeptiong($exc);
        }
    }

    public function actionDelete($id) {
        try {
            if (Yii::app()->request->isPostRequest) {
                // we only allow deletion via POST request
                if ($model = $this->loadModel($id)) {
                    if ($model->delete()) {
                        Yii::log("Uid: " . Yii::app()->user->id . " Delete record " . print_r($model->attributes, true), 'info');
                    }
                }

                // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
                if (!isset($_GET['ajax']))
                    $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('index'));
            }
            else {
                Yii::log("Uid: " . Yii::app()->user->id . " Invalid request. Please do not repeat this request again.");
                throw new CHttpException(400, 'Invalid request. Please do not repeat this request again.');
            }
        } catch (Exception $exc) {
            GasCheck::CatchAllExeptiong($exc);
        }
    }

    public function loadModel($id) {
        try {
            $model = HrCoefficients::model()->findByPk($id);
            if ($model === null) {
                Yii::log("The requested page does not exist.");
                throw new CHttpException(404, 'The requested page does not exist.');
            }
            return $model;
        } catch (Exception $exc) {
            GasCheck::CatchAllExeptiong($exc);
        }
    }

}
