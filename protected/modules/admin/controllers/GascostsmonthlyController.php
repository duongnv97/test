<?php

class GascostsmonthlyController extends AdminController
{
   /*  public function accessRules()
    {
       return array();
    } */  
         
        public function actionExport_cost() {
            set_time_limit(7200);               
            $model=new GasCostsMonthly();
            $model->unsetAttributes();  // clear any default values
            if(isset($_POST['GasCostsMonthly'])){
                $model->attributes=$_POST['GasCostsMonthly'];
                GasCostsMonthly::export_cost($model);
            }
            $this->render('export_cost',array(
                    'model'=>$model, 'actions' => $this->listActionsCanAccess,
            ));        
        }
		
        public function actionExport_cost_revenue() {
            set_time_limit(7200);               
            $model=new GasCostsMonthly();
            $model->unsetAttributes();  // clear any default values
            if(isset($_POST['GasCostsMonthly'])){
                $model->attributes=$_POST['GasCostsMonthly'];
                GasCostsMonthly::export_cost_revenue($model);
            }
            $this->render('export_cost_revenue',array(
                    'model'=>$model, 'actions' => $this->listActionsCanAccess,
            ));        
        }        
    
        public function actionExport_business_results() {
            set_time_limit(7200);               
            $model=new GasCostsMonthly();
            $model->unsetAttributes();  // clear any default values
            if(isset($_POST['GasCostsMonthly'])){
                $model->attributes=$_POST['GasCostsMonthly'];
                GasCostsMonthly::export_business_results($model);
            }
            $this->render('export_business_results',array(
                    'model'=>$model, 'actions' => $this->listActionsCanAccess,
            ));        
        }	
		
        // begin 3 (3x4=12) bao cao latest 
        // 1.1
        public function actionExport_agent_revenue() {
            
            set_time_limit(7200);               
            $model=new GasCostsMonthly();
            $model->unsetAttributes();  // clear any default values
            if(isset($_POST['GasCostsMonthly'])){

                $model->attributes=$_POST['GasCostsMonthly'];
                GasCostsMonthly::export_agent_revenue($model);
            }
            $this->render('export_agent_revenue',array(
                    'model'=>$model, 'actions' => $this->listActionsCanAccess,
            ));        
        }        
	
        // 1.2
        public function actionExport_agent_price_root() {
            
            set_time_limit(7200);               
            $model=new GasCostsMonthly();
            $model->unsetAttributes();  // clear any default values
            if(isset($_POST['GasCostsMonthly'])){

                $model->attributes=$_POST['GasCostsMonthly'];
                GasCostsMonthly::export_agent_price_root($model);
            }
            $this->render('export_agent_price_root',array(
                    'model'=>$model, 'actions' => $this->listActionsCanAccess,
            ));        
        }        		
    
        // 1.3 -- add more at sheet 2
        public function actionExport_agent_cost() {
            
            set_time_limit(7200);               
            $model=new GasCostsMonthly();
            $model->unsetAttributes();  // clear any default values
            if(isset($_POST['GasCostsMonthly'])){

                $model->attributes=$_POST['GasCostsMonthly'];
                GasCostsMonthly::export_agent_cost($model);
            }
            $this->render('export_agent_cost',array(
                    'model'=>$model, 'actions' => $this->listActionsCanAccess,
            ));        
        }        
        
        // 1.4
        public function actionExport_agent_gross_profit() {
            set_time_limit(7200);               
            $model=new GasCostsMonthly();
            $model->unsetAttributes();  // clear any default values
            if(isset($_POST['GasCostsMonthly'])){

                $model->attributes=$_POST['GasCostsMonthly'];
                GasCostsMonthly::export_agent_gross_profit($model);
            }
            $this->render('export_agent_gross_profit',array(
                    'model'=>$model, 'actions' => $this->listActionsCanAccess,
            ));        
        }        		
        // 1.5
		public function actionExport_agent_net_profit() {
            set_time_limit(7200);               
            $model=new GasCostsMonthly();
            $model->unsetAttributes();  // clear any default values
            if(isset($_POST['GasCostsMonthly'])){

                $model->attributes=$_POST['GasCostsMonthly'];
                GasCostsMonthly::export_agent_net_profit($model);
            }
            $this->render('export_agent_net_profit',array(
                    'model'=>$model, 'actions' => $this->listActionsCanAccess,
            ));        
        }        				
		
    
        // 1.6 -- add more at sheet 7
        public function actionExport_cost_sum_total() {            
            set_time_limit(7200);               
            $model=new GasCostsMonthly();
            $model->unsetAttributes();  // clear any default values
            if(isset($_POST['GasCostsMonthly'])){

                $model->attributes=$_POST['GasCostsMonthly'];
                GasCostsMonthly::export_cost_sum_total($model);
            }
            $this->render('export_cost_sum_total',array(
                    'model'=>$model, 'actions' => $this->listActionsCanAccess,
            ));        
        }      		
		
        // 2.1
        public function actionExport_agent_revenue_by_material() {            
            set_time_limit(7200);               
            $model=new GasCostsMonthly();
            $model->unsetAttributes();  // clear any default values
            if(isset($_POST['GasCostsMonthly'])){

                $model->attributes=$_POST['GasCostsMonthly'];
                GasCostsMonthly::export_agent_revenue_by_material($model);
            }
            $this->render('export_agent_revenue_by_material',array(
                    'model'=>$model, 'actions' => $this->listActionsCanAccess,
            ));        
        }
		
        // 2.2
        public function actionExport_agent_price_root_by_material() {
            set_time_limit(7200);               
            $model=new GasCostsMonthly();
            $model->unsetAttributes();  // clear any default values
            if(isset($_POST['GasCostsMonthly'])){

                $model->attributes=$_POST['GasCostsMonthly'];
                GasCostsMonthly::export_agent_price_root_by_material($model);
            }
            $this->render('export_agent_price_root_by_material',array(
                    'model'=>$model, 'actions' => $this->listActionsCanAccess,
            ));        
        }      		
	// 2.3	
        public function actionExport_agent_output_by_material() {
            set_time_limit(7200);               
            $model=new GasCostsMonthly();
            $model->unsetAttributes();  // clear any default values
            if(isset($_POST['GasCostsMonthly'])){

                $model->attributes=$_POST['GasCostsMonthly'];
                GasCostsMonthly::export_agent_output_by_material($model);
            }
            $this->render('export_agent_output_by_material',array(
                    'model'=>$model, 'actions' => $this->listActionsCanAccess,
            ));        
        }      		
		
        // 2.4
        public function actionExport_agent_gross_profit_by_material() {
            set_time_limit(7200);               
            $model=new GasCostsMonthly();
            $model->unsetAttributes();  // clear any default values
            if(isset($_POST['GasCostsMonthly'])){

                $model->attributes=$_POST['GasCostsMonthly'];
                GasCostsMonthly::export_agent_gross_profit_by_material($model);
            }
            $this->render('export_agent_gross_profit_by_material',array(
                    'model'=>$model, 'actions' => $this->listActionsCanAccess,
            ));        
        }      		
		
		
        // 2.5  -- add more at sheet 5
        public function actionExport_agent_by_costs_2() {
            set_time_limit(7200);               
            $model=new GasCostsMonthly();
            $model->unsetAttributes();  // clear any default values
            if(isset($_POST['GasCostsMonthly'])){

                $model->attributes=$_POST['GasCostsMonthly'];
                GasCostsMonthly::export_agent_by_costs_2($model);
            }
            $this->render('export_agent_by_costs_2',array(
                    'model'=>$model, 'actions' => $this->listActionsCanAccess,
            ));        
        }          
        // 2.6  -- add more at sheet 6
        public function actionExport_agent_by_costs_3() {
            set_time_limit(7200);               
            $model=new GasCostsMonthly();
            $model->unsetAttributes();  // clear any default values
            if(isset($_POST['GasCostsMonthly'])){

                $model->attributes=$_POST['GasCostsMonthly'];
                GasCostsMonthly::export_agent_by_costs_3($model);
            }
            $this->render('export_agent_by_costs_3',array(
                    'model'=>$model, 'actions' => $this->listActionsCanAccess,
            ));        
        }          
        
    // 3.1
        public function actionExport_revenue_material_by_agent() {
            set_time_limit(7200);               
            $model=new GasCostsMonthly();
            $model->unsetAttributes();  // clear any default values
            if(isset($_POST['GasCostsMonthly'])){
                $model->attributes=$_POST['GasCostsMonthly'];
                GasCostsMonthly::export_revenue_material_by_agent($model);
            }
            $this->render('export_revenue_material_by_agent',array(
                    'model'=>$model, 'actions' => $this->listActionsCanAccess,
            ));        
        }      		
		
        // 3.2
        public function actionExport_price_root_material_by_agent() {
            set_time_limit(7200);               
            $model=new GasCostsMonthly();
            $model->unsetAttributes();  // clear any default values
            if(isset($_POST['GasCostsMonthly'])){
                $model->attributes=$_POST['GasCostsMonthly'];
                GasCostsMonthly::export_price_root_material_by_agent($model);
            }
            $this->render('export_price_root_material_by_agent',array(
                    'model'=>$model, 'actions' => $this->listActionsCanAccess,
            ));        
        }      		
		
        // 3.3
        public function actionExport_output_material_by_agent() {
            set_time_limit(7200);               
            $model=new GasCostsMonthly();
            $model->unsetAttributes();  // clear any default values
            if(isset($_POST['GasCostsMonthly'])){
                $model->attributes=$_POST['GasCostsMonthly'];
                GasCostsMonthly::export_output_material_by_agent($model);
            }
            $this->render('export_output_material_by_agent',array(
                    'model'=>$model, 'actions' => $this->listActionsCanAccess,
            ));        
        }      		
		
        // 3.4
        public function actionExport_gross_profit_material_by_agent() {
            set_time_limit(7200);               
            $model=new GasCostsMonthly();
            $model->unsetAttributes();  // clear any default values
            if(isset($_POST['GasCostsMonthly'])){
                $model->attributes=$_POST['GasCostsMonthly'];
                GasCostsMonthly::export_gross_profit_material_by_agent($model);
            }
            $this->render('export_gross_profit_material_by_agent',array(
                    'model'=>$model, 'actions' => $this->listActionsCanAccess,
            ));        
        }      		
		
    
	public function actionView($id)
	{
                try{
                $this->render('view',array(
			'model'=>$this->loadModel($id), 'actions' => $this->listActionsCanAccess,
		));
                }
                catch (Exception $e)
                {
                    Yii::log("Uid: " .Yii::app()->user->id. "Exception ".  $e->getMessage(), 'error');
                    $code = 404;
                    if(isset($e->statusCode))
                        $code=$e->statusCode;
                    if($e->getCode())
                        $code=$e->getCode();
                    throw new CHttpException($code, $e->getMessage());
                }
		
	}

	public function actionCreate()
	{
                try
                {
		if(isset($_GET['ajax_costs']))
		{
                    $aRes = array();
                    $aMaterial = GasCostsMonthly::getArrObjMaterialForAjaxMonthly();
                    $dataAgent = Users::getArrObjectUserByRole(ROLE_AGENT);
                    foreach($dataAgent as $agent_id=>$obj_agent){
                        $criteria=new CDbCriteria;
                        if(count($aMaterial[0]['sub_arr_id'])>0)
                                $criteria->addInCondition('t.materials_id',$aMaterial[0]['sub_arr_id']);

                        $criteria->compare('t.agent_id',$agent_id);
                        $criteria->compare('t.sell_month',$_GET['sell_month']*1);
                        $criteria->compare('t.sell_year',$_GET['sell_year']);
                        $criteria->select = 'agent_id, sum(total_root) as total_root ';
                        $criteria->group = 't.agent_id';
                        $res = GasMaterialsSell::model()->findAll($criteria);                    
                        if(count($res)>0)
                        foreach($res as $obj){
                            $aRes[$agent_id]= round($obj->total_root);
                        }         
                    }         
                    
                    $json = CJavaScript::jsonEncode(array('success'=>true,'agent_res'=>$aRes));
                    echo $json;
                    die;
//                    foreach($dataAgent as $agent_id=>$obj_agent){
//                        foreach($aMaterial as $materialObj){
//                                $criteria=new CDbCriteria;
//                                if(count($materialObj['sub_arr_id'])>0)
//                                        $criteria->addInCondition('t.materials_id',$materialObj['sub_arr_id']);
//
//                                $criteria->compare('t.agent_id',$agent_id);
//                                $criteria->compare('t.sell_month',$_GET['sell_month']);
//                                $criteria->compare('t.sell_year',$_GET['sell_year']);
//                                $criteria->select = 'agent_id, sum(qty) as qty, sum(total_root) as total_root ';
//                                $criteria->group = 't.agent_id';
//                                $res = GasMaterialsSell::model()->findAll($criteria);
//
//                                if(count($res)>0)
//                                foreach($res as $obj){
////                                    $aRes[$agent_id][$materialObj['parent_obj']->id]['materials_obj'] = $materialObj;
//                                    $aRes[$agent_id]= $obj->total_root;
//                                }
//                        }
//                    }
                    
                }                    
                    
		$model=new GasCostsMonthly('create_costs');

                $model->costs_month = date('m');
                $model->costs_year = date('Y');

		if(isset($_POST['GasCostsMonthly']))
		{
			$model->attributes=$_POST['GasCostsMonthly'];
                        $model->agent_id = 1;
                        $model->total = 1;
                        $model->note = '';
                        $model->validate();
			if(!$model->hasErrors()){
                            if(count($_POST['GasCostsMonthly']['agent_id'])>0){
                                foreach($_POST['GasCostsMonthly']['agent_id'] as $key=>$item){
                                    if(!empty($_POST['GasCostsMonthly']['total'][$key]) && trim($item)!=''){
                                        $modelAdd=new GasCostsMonthly();
                                        $modelAdd->costs_id = $model->costs_id;
                                        $modelAdd->agent_id = $item;
                                        $modelAdd->total = $_POST['GasCostsMonthly']['total'][$key];
                                        $modelAdd->costs_month = $model->costs_month;
                                        $modelAdd->costs_year = $model->costs_year;
                                        $modelAdd->note = $_POST['GasCostsMonthly']['note'][$key];
                                        $modelAdd->save();
                                    }
                                }
                                
                                $this->redirect(array('index'));
                            }
                            
                        }
		}
                
		$this->render('create',array(
			'model'=>$model, 'actions' => $this->listActionsCanAccess,
		));
                }
                catch (Exception $e)
                {
                    Yii::log("Uid: " .Yii::app()->user->id. "Exception ".  $e->getMessage(), 'error');
                    $code = 404;
                    if(isset($e->statusCode))
                        $code=$e->statusCode;
                    if($e->getCode())
                        $code=$e->getCode();
                    throw new CHttpException($code, $e->getMessage());
                }
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
                try
                {
		$model=$this->loadModel($id);
                $model->scenario ='update_costs';
		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['GasCostsMonthly']))
		{
			$model->attributes=$_POST['GasCostsMonthly'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('update',array(
			'model'=>$model, 'actions' => $this->listActionsCanAccess,
		));
                }
                catch (Exception $e)
                {
                    Yii::log("Uid: " .Yii::app()->user->id. "Exception ".  $e->getMessage(), 'error');
                    $code = 404;
                    if(isset($e->statusCode))
                        $code=$e->statusCode;
                    if($e->getCode())
                        $code=$e->getCode();
                    throw new CHttpException($code, $e->getMessage());
                }
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
                try
                {
		if(Yii::app()->request->isPostRequest)
		{
			// we only allow deletion via POST request
			if($model = $this->loadModel($id))
                        {
                            if($model->delete())
                                Yii::log("Delete record ".  print_r($model->attributes, true), 'info');
                        }

			// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
			if(!isset($_GET['ajax']))
				$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('index'));
		}
		else
                {
                    Yii::log("Invalid request. Please do not repeat this request again.");
                    throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
                }	
                }
                catch (Exception $e)
                {
                    Yii::log("Uid: " .Yii::app()->user->id. "Exception ".  $e->getMessage(), 'error');
                    throw new CHttpException(400,$e->getMessage());
                }
	}

	/**
	 * Manages all models.
	 */
	public function actionIndex()
	{
                try
                {
		$model=new GasCostsMonthly('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['GasCostsMonthly']))
			$model->attributes=$_GET['GasCostsMonthly'];

		$this->render('index',array(
			'model'=>$model, 'actions' => $this->listActionsCanAccess,
		));
                }
                catch (Exception $e)
                {
                    Yii::log("Uid: " .Yii::app()->user->id. "Exception ".  $e->getMessage(), 'error');
                    $code = 404;
                    if(isset($e->statusCode))
                        $code=$e->statusCode;
                    if($e->getCode())
                        $code=$e->getCode();
                    throw new CHttpException($code, $e->getMessage());
                }
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the ID of the model to be loaded
	 */
	public function loadModel($id)
	{
                try
                {
		$model=GasCostsMonthly::model()->findByPk($id);
		if($model===null)
                {
                    Yii::log("The requested page does not exist.");
                    throw new CHttpException(404,'The requested page does not exist.');
                }			
		return $model;
                }
                catch (Exception $e)
                {
                    Yii::log("Uid: " .Yii::app()->user->id. "Exception ".  $e->getMessage(), 'error');
                    $code = 404;
                    if(isset($e->statusCode))
                        $code=$e->statusCode;
                    if($e->getCode())
                        $code=$e->getCode();
                    throw new CHttpException($code, $e->getMessage());
                }
	}

	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
                try
                {
		if(isset($_POST['ajax']) && $_POST['ajax']==='gas-costs-monthly-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
                }
                catch (Exception $e)
                {
                    Yii::log("Uid: " .Yii::app()->user->id. "Exception ".  $e->getMessage(), 'error');
                    $code = 404;
                    if(isset($e->statusCode))
                        $code=$e->statusCode;
                    if($e->getCode())
                        $code=$e->getCode();
                    throw new CHttpException($code, $e->getMessage());
                }
	}
}
