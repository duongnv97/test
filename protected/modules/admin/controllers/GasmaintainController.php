<?php

class GasmaintainController extends AdminController
{

        
    public function actionStatistic_maintain(){
        $this->pageTitle = 'Thống Kê Bảo Trì';
            /* update created date of 
            $criteria = new CDBcriteria();
            $criteria->addCondition('t.customer_id<>""');
            $criteria->addCondition('t.employee_maintain_id IS NULL');
            $mUpdate = GasAgentCustomer::model()->findAll($criteria);
            //echo count($mUpdate);die;
            foreach($mUpdate as $item){
                    $mUser = Users::model()->findByPk($item->customer_id);
                    if($mUser){
                            $item->created_date = $mUser->	created_date;
                            $item->update(array('created_date'));
                    }else
                            $item->delete();
            }
            die;
            /* update created date of */


            $model=new GasMaintain();
            $resultSta = array();
            if(isset($_POST['GasMaintain'])){
                    $resultSta = Statistic::Statistic_maintain();
                    $model->attributes=$_POST['GasMaintain'];
            }else
                    unset($_SESSION['data-excel']);

            $this->render('statistic/Statistic_maintain',array(
                            'model'=>$model, 'actions' => $this->listActionsCanAccess,
                            'resultSta'=>$resultSta,
            ));     						

    }

    public function actionStatistic_market_development(){
        $this->pageTitle = 'Thống Kê Phát Triển Thị Trường';
            $model=new GasMaintain();
            $resultSta = array();
            if(isset($_POST['GasMaintain'])){
                    $resultSta = Statistic::Statistic_market_development();
                    $model->attributes=$_POST['GasMaintain'];
            }else
                    unset($_SESSION['data-excel']);

            $this->render('statistic/Statistic_market_development',array(
                            'model'=>$model, 'actions' => $this->listActionsCanAccess,
                            'resultSta'=>$resultSta,
            ));     								
    }

    public function actionStatistic_market_development_by_month(){
        $this->pageTitle = 'Thống Kê Phát Triển Thị Trường Theo Tháng';
            set_time_limit(7200);
            $model=new GasMaintain();
            $resultSta = array();
            $model->statistic_year = date('Y');
            $model->statistic_month = array(date('m')=>date('m'));
            if(isset($_POST['GasMaintain'])){
                $model->attributes=$_POST['GasMaintain'];
                $this->LimitMonthQuery($model);
                $resultSta = Statistic::Statistic_market_development_by_month($model);
            }else
                    unset($_SESSION['data-excel']);

            $this->render('statistic/Statistic_market_development_by_month',array(
                            'model'=>$model, 'actions' => $this->listActionsCanAccess,
                            'resultSta'=>$resultSta,
            ));  	
    }	

    public function actionStatistic_market_development_by_month_export_excel(){
            if(isset($_SESSION['data-excel'])){
                StatisticExport::Statistic_market_development_by_month_export_excel();
            }	
            Yii::app()->user->setFlash('successUpdate', "Chưa chọn thời gian thống kê.");
            $this->redirect(array('statistic_market_development_by_month'));		
    }        

    public function actionStatistic_maintain_export_excel(){
            if(isset($_SESSION['data-excel'])){
                StatisticExport::Statistic_maintain_export_excel();
            }
            Yii::app()->user->setFlash('successUpdate', "Chưa chọn thời gian thống kê.");
            $this->redirect(array('statistic_maintain'));
    }

    // Mar 24, 2015 xử lý không cho xem nhiều tháng 1 lúc, CPU không chạy nổi
    public function LimitMonthQuery(&$model){
        if(count($model->statistic_month)>3){
            $model->statistic_month = array($model->statistic_month[0], $model->statistic_month[1], $model->statistic_month[2]);
        }
    }

    public function actionStatistic_maintain_by_month(){
        $this->pageTitle = 'Thống Kê Bảo Trì Theo Tháng';
            set_time_limit(7200);
            $model=new GasMaintain();
            $resultSta = array();
            if(isset($_POST['GasMaintain'])){
                $model->attributes=$_POST['GasMaintain'];
                $this->LimitMonthQuery($model);
                $resultSta = Statistic::Statistic_maintain_by_month($model);
            }else
                    unset($_SESSION['data-excel']);

            $this->render('statistic/Statistic_maintain_by_month',array(
                            'model'=>$model, 'actions' => $this->listActionsCanAccess,
                            'resultSta'=>$resultSta,
            ));  	
    }

    public function actionStatistic_maintain_by_month_export_excel(){
            if(isset($_SESSION['data-excel'])){
                StatisticExport::Statistic_maintain_by_month_export_excel();
            }	
            Yii::app()->user->setFlash('successUpdate', "Chưa chọn thời gian thống kê.");
            $this->redirect(array('statistic_maintain_by_month'));		
    }	



    public function actionView($id)
    {
        $this->pageTitle = 'Xem Bảo Trì';
            try{
                            $this->layout='ajax';
            $this->render('view',array(
                    'model'=>$this->loadModel($id), 'actions' => $this->listActionsCanAccess,
            ));
            }
            catch (Exception $e)
            {
                Yii::log("Uid: " .Yii::app()->user->id. "Exception ".  $e->getMessage(), 'error');
                $code = 404;
                if(isset($e->statusCode))
                    $code=$e->statusCode;
                if($e->getCode())
                    $code=$e->getCode();
                throw new CHttpException($code, $e->getMessage());                    
            }

    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate()
    {
        $this->pageTitle = 'Tạo Mới Bảo Trì';
            try
            {
            $model=new GasMaintain('create_maintain');
            $model->is_new_customer=CUSTOMER_OLD;
            if(isset($_POST['GasMaintain']))
            {
                    $aIgnoreAttributes = array('note_update_status','update_num',
                        'status','user_id_create','created_date'
                    );        
                    InputHelper::ignoreAttributesPost($aIgnoreAttributes);
                    $model->attributes=$_POST['GasMaintain'];
                   /*  $model->is_new_customer = (int)$model->is_new_customer;
                    if($model->is_new_customer==CUSTOMER_OLD){
                         $model->customer_name = 'by pass validate';
                         $model->customer_address = 'by pass validate';
                         $model->customer_phone = 'by pass validate';
                    } */
                    $model->validate();
                    /* if($model->is_new_customer==CUSTOMER_OLD){
                        if(empty($model->customer_id))
                            $model->addError ('customer_id', 'Chưa chọn khách hàng');
                    } */

                    if(!$model->hasErrors() && ($model->is_new_customer==CUSTOMER_NEW || $model->is_new_customer==CUSTOMER_OLD)){
                        $aAttSave = array('maintain_date','maintain_employee_id',
                            'materials_id','seri_no','note','is_need_maintain_back'
                            );
                        $model = MyFunctionCustom::clearHtmlModel($model, $aAttSave);
//                            if($model->is_new_customer==CUSTOMER_NEW){
//                                $mUser = new Users('create_customer_maintain');
//                                $mUser->first_name = $model->customer_name;
//                                $mUser->address = $model->customer_address;
//                                $mUser->phone = $model->customer_phone;
//                                $mUser->role_id = ROLE_CUSTOMER;
//                                $mUser->application_id = BE;
//                                $mUser->code_bussiness = $model->customer_code_bussiness;
//                                // Lấy mã kế toán ( code account ) của đại lý xây dựng thành mã KH
//                                $mUser->code_account = MyFunctionCustom::getNextIdForUser('Users', Users::getCodeAccount(Yii::app()->user->id).'_', MAX_LENGTH_CODE, 'code_account', ROLE_CUSTOMER);
//                                $mUser->save();
//                                $model->customer_id = $mUser->id;
//                            }

                        $model->agent_id = Yii::app()->user->parent_id;
                        $model->scenario = null;
                        $model->save();
                        Yii::app()->user->setFlash('successUpdate', "Thêm mới thành công.");
                        $this->redirect(Yii::app()->createAbsoluteUrl('admin/gasmaintain/create'));
                    }

            }

            $this->render('create',array(
                    'model'=>$model, 'actions' => $this->listActionsCanAccess,
            ));
            }
            catch (Exception $e)
            {
                Yii::log("Uid: " .Yii::app()->user->id. "Exception ".  $e->getMessage(), 'error');
                $code = 404;
                if(isset($e->statusCode))
                    $code=$e->statusCode;
                if($e->getCode())
                    $code=$e->getCode();
                throw new CHttpException($code, $e->getMessage());
            }
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id)
    {
        $this->pageTitle = 'Cập Nhật Bảo Trì';
            try
            {
            $model=$this->loadModel($id);
            $model->scenario = 'update_maintain';
            if(!( Yii::app()->user->role_id == ROLE_ADMIN || ( Yii::app()->user->role_id == ROLE_SUB_USER_AGENT && (int)$model->update_num < (int)Yii::app()->params["limit_update_maintain"]) )
                    || (Yii::app()->user->role_id == ROLE_SUB_USER_AGENT && $model->agent_id!=MyFormat::getAgentId())
                )
                $this->redirect(array('index'));
            $mUser = Users::model()->findByPk($model->customer_id);
            if(is_null($mUser))
                $this->redirect(array('index'));
            $model->customer_name = $mUser->first_name;
            $model->customer_address = $mUser->address;
            $model->customer_phone = $mUser->phone;
            $model->customer_code_bussiness = $mUser->code_bussiness;
            $model->maintain_date = MyFormat::dateConverYmdToDmy($model->maintain_date);

            if(isset($_POST['GasMaintain']))
            {
                $aIgnoreAttributes = array('note_update_status','update_num',
                    'status','user_id_create','created_date'
                );                    

                InputHelper::ignoreAttributesPost($aIgnoreAttributes);
                    $model->attributes=$_POST['GasMaintain'];
                    $model->validate();
                    if(!$model->hasErrors()){
                        $aAttSave = array('maintain_date','maintain_employee_id',
                            'materials_id','seri_no','note','is_need_maintain_back','customer_id',
                            'last_update_by','last_update_time',
                            );
                        $mUser = Users::model()->findByPk($model->customer_id);
                        $model = MyFunctionCustom::clearHtmlModel($model, $aAttSave);
                        $model->last_update_by = Yii::app()->user->id;
                        $model->last_update_time = date('Y-m-d H:i:s');
//                            $mUser->phone = $model->customer_phone;
//                            $mUser->code_bussiness = $model->customer_code_bussiness;
//                            $mUser->update(array('first_name','address','phone','code_bussiness'));
//                            $model->customer_id = $mUser->id;
                        $model->update($aAttSave);
                        Yii::app()->user->setFlash('successUpdate', "Cập nhật thành công.");
                        //$this->redirect(Yii::app()->createAbsoluteUrl('admin/gasmaintain/create'));							
                        $this->redirect(array('update','id'=>$model->id));
                    }		}

            $this->render('update',array(
                    'model'=>$model, 'actions' => $this->listActionsCanAccess,
            ));
            }
            catch (Exception $e)
            {
                Yii::log("Uid: " .Yii::app()->user->id. "Exception ".  $e->getMessage(), 'error');
                $code = 404;
                if(isset($e->statusCode))
                    $code=$e->statusCode;
                if($e->getCode())
                    $code=$e->getCode();
                throw new CHttpException($code, $e->getMessage());
            }
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id)
    {
            try
            {
            if(Yii::app()->request->isPostRequest)
            {
                    // we only allow deletion via POST request
                    if($model = $this->loadModel($id))
                    {
                        if($model->delete())
                            Yii::log("Delete record ".  print_r($model->attributes, true), 'info');
                    }

                    // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
                    if(!isset($_GET['ajax']))
                            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('index'));
            }
            else
            {
                Yii::log("Invalid request. Please do not repeat this request again.");
                throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
            }	
            }
            catch (Exception $e)
            {
                Yii::log("Uid: " .Yii::app()->user->id. "Exception ".  $e->getMessage(), 'error');
                throw new CHttpException(400,$e->getMessage());
            }
    }

    /**
     * Manages all models.
     */
    public function actionIndex()
    {                   
        $this->pageTitle = 'Danh Sách Bảo Trì';
            try
            {
                            MyFunctionCustom::checkChangePassword();
                            MyFunctionCustom::updateListAgentOfUserMaintainLogin();
            $model=new GasMaintain('search');
            $model->unsetAttributes();  // clear any default values
            if(isset($_GET['GasMaintain']))
                    $model->attributes=$_GET['GasMaintain'];

            $this->render('index',array(
                    'model'=>$model, 'actions' => $this->listActionsCanAccess,
            ));
            }
            catch (Exception $e)
            {
                Yii::log("Uid: " .Yii::app()->user->id. "Exception ".  $e->getMessage(), 'error');
                $code = 404;
                if(isset($e->statusCode))
                    $code=$e->statusCode;
                if($e->getCode())
                    $code=$e->getCode();
                throw new CHttpException($code, $e->getMessage());
            }
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer the ID of the model to be loaded
     */
    public function loadModel($id)
    {
            try
            {
            $model=GasMaintain::model()->findByPk($id);
            if($model===null)
            {
                Yii::log("The requested page does not exist.");
                throw new CHttpException(404,'The requested page does not exist.');
            }			
            return $model;
            }
            catch (Exception $e)
            {
                Yii::log("Uid: " .Yii::app()->user->id. "Exception ".  $e->getMessage(), 'error');
                $code = 404;
                if(isset($e->statusCode))
                    $code=$e->statusCode;
                if($e->getCode())
                    $code=$e->getCode();
                throw new CHttpException($code, $e->getMessage());
            }
    }

    /**
     * Performs the AJAX validation.
     * @param CModel the model to be validated
     */
    protected function performAjaxValidation($model)
    {
            try
            {
            if(isset($_POST['ajax']) && $_POST['ajax']==='gas-maintain-form')
            {
                    echo CActiveForm::validate($model);
                    Yii::app()->end();
            }
            }
            catch (Exception $e)
            {
                Yii::log("Uid: " .Yii::app()->user->id. "Exception ".  $e->getMessage(), 'error');
                $code = 404;
                if(isset($e->statusCode))
                    $code=$e->statusCode;
                if($e->getCode())
                    $code=$e->getCode();
                throw new CHttpException($code, $e->getMessage());
            }
    }

    public function actionUpdate_status_maintain()
    {
        $this->layout = 'ajax';
        if(!isset($_GET['maintain_id']))
            die('Không Tìm Thấy Trang Yêu Cầu');
        $model = GasMaintain::model()->findByPk($_GET['maintain_id']);
        if(is_null($model))
            die('Không Tìm Thấy Trang Yêu Cầu');
        $msg= false;
        if(isset($_POST['GasMaintain'])){
            $model->attributes=$_POST['GasMaintain'];
            $update_num = (int)$model->update_num;
            $update_num++;
            $model->update_num = $update_num;
            $model->update(array('using_gas_huongminh','status','note_update_status','update_num'));
            $msg= 'Cập Nhật Trạng Thái Thành Công';
        }

        $this->render('update_status_maintain',array(
                'model'=>$model,
            'msg'=>$msg,
        ));            

    }       

    // dành cho giám sát bảo trì danh sách các bảo trì cần nhân viên xuống lần nữa sau khi
    // gặp nhân viên tư vấn, cần thay van hay gì đó
    public function actionList_for_monitoring_maintain()
    {
        $this->pageTitle = 'Giám Sát Bảo Trì';
        try
        {
            MyFunctionCustom::checkChangePassword();
//                MyFunctionCustom::updateListAgentOfUserMaintainLogin();
            $model=new GasMaintain('search');
            $model->unsetAttributes();  // clear any default values
            if(isset($_GET['GasMaintain']))
                    $model->attributes=$_GET['GasMaintain'];
            $model->is_need_maintain_back = 1;
            $this->render('List_for_monitoring_maintain',array(
                    'model'=>$model, 'actions' => $this->listActionsCanAccess,
            ));
            }
            catch (Exception $e)
            {
                Yii::log("Uid: " .Yii::app()->user->id. "Exception ".  $e->getMessage(), 'error');
                $code = 404;
                if(isset($e->statusCode))
                    $code=$e->statusCode;
                if($e->getCode())
                    $code=$e->getCode();
                throw new CHttpException($code, $e->getMessage());
            }             
    }

    // dành cho tổng giám sát danh sách cuộc gọi xấu bên bảo trì
    public function actionSupervision_list_maintain_call_bad()
    {
        $this->pageTitle = 'Bảo Trì Có Cuộc Gọi Xấu';
        try
        {
            MyFunctionCustom::checkChangePassword();
            $model=new GasMaintain('search');
            $model->unsetAttributes();  // clear any default values
            $model->status = CmsFormatter::$STATUS_MAINTAIN_BAD;
            if(isset($_GET['GasMaintain']))
                    $model->attributes=$_GET['GasMaintain'];

            $this->render('Supervision_list_maintain_call_bad',array(
                    'model'=>$model, 'actions' => $this->listActionsCanAccess,
            ));
            }
            catch (Exception $e)
            {
                Yii::log("Uid: " .Yii::app()->user->id. "Exception ".  $e->getMessage(), 'error');
                throw  new CHttpException("Uid: " .Yii::app()->user->id. "Exception ".  $e->getMessage());     
            }                  
    }        

    // dành cho tổng giám sát danh sách cuộc gọi xấu bên bán hàng
    public function actionSupervision_list_maintain_sell_call_bad()
    {
        $this->pageTitle = 'Bán Hàng Có Cuộc Gọi Xấu';
        try
        {
            MyFunctionCustom::checkChangePassword();
            $model=new GasMaintainSell('search');
            $model->unsetAttributes();  // clear any default values
            $model->status =  CmsFormatter::$STATUS_MAINTAIN_BAD;
            if(isset($_GET['GasMaintainSell']))
                    $model->attributes=$_GET['GasMaintainSell'];

            $this->render('Supervision_list_maintain_sell_call_bad',array(
                    'model'=>$model, 'actions' => $this->listActionsCanAccess,
            ));
            }
            catch (Exception $e)
            {
                Yii::log("Uid: " .Yii::app()->user->id. "Exception ".  $e->getMessage(), 'error');
                throw  new CHttpException("Uid: " .Yii::app()->user->id. "Exception ".  $e->getMessage());     
            }       
    }        

    // dành cho tổng giám sát danh sách ko trùng seri bên bán hàng
    public function actionSupervision_list_maintain_sell_seri_diff()
    {
        $this->pageTitle = 'Bán Hàng Có Seri Không Trùng';
        try
        {
            MyFunctionCustom::checkChangePassword();
            $model=new GasMaintainSell('search');
            $model->unsetAttributes();  // clear any default values
            if(isset($_GET['GasMaintainSell']))
                    $model->attributes=$_GET['GasMaintainSell'];
            $model->is_same_seri_maintain = 0;
            $this->render('Supervision_list_maintain_sell_seri_diff',array(
                    'model'=>$model, 'actions' => $this->listActionsCanAccess,
            ));
            }
            catch (Exception $e)
            {
                Yii::log("Uid: " .Yii::app()->user->id. "Exception ".  $e->getMessage(), 'error');
                $code = 404;
                if(isset($e->statusCode))
                    $code=$e->statusCode;
                if($e->getCode())
                    $code=$e->getCode();
                throw new CHttpException($code, $e->getMessage());
            }                 
    }

    public function actionUpdate_list_for_monitoring_maintain(){
        $this->layout = 'ajax';
        if(!isset($_GET['maintain_id']))
            die('Không Tìm Thấy Trang Yêu Cầu');
        $model = GasMaintain::model()->findByPk($_GET['maintain_id']);
        if(is_null($model))
            die('Không Tìm Thấy Trang Yêu Cầu');
        $msg= false;
        if(isset($_POST['GasMaintain'])){
            $model->attributes=$_POST['GasMaintain'];
            $model->update(array('status_maintain_back','note_maintain_back'));
            $msg= 'Cập Nhật Trạng Thái Thành Công';
        }

        $this->render('popup/Update_list_for_monitoring_maintain',array(
            'model'=>$model,
            'msg'=>$msg,
        ));           
    }

    public function actionUpdate_supervision_list_maintain_call_bad(){
        $this->layout = 'ajax';
        if(!isset($_GET['maintain_id']))
            die('Không Tìm Thấy Trang Yêu Cầu');
        $model = GasMaintain::model()->findByPk($_GET['maintain_id']);
        if(is_null($model))
            die('Không Tìm Thấy Trang Yêu Cầu');
        $msg= false;
        if(isset($_POST['GasMaintain'])){
            $model->attributes=$_POST['GasMaintain'];
            $model->update(array('status_call_bad','note_call_bad'));
            $msg= 'Cập Nhật Trạng Thái Thành Công';
        }

        $this->render('popup/Update_supervision_list_maintain_call_bad',array(
            'model'=>$model,
            'msg'=>$msg,
        )); 		
    }        


    public function actionUpdate_supervision_list_maintain_sell_call_bad(){
        $this->layout = 'ajax';
        if(!isset($_GET['maintain_sell_id']))
            die('Không Tìm Thấy Trang Yêu Cầu');
        $model = GasMaintainSell::model()->findByPk($_GET['maintain_sell_id']);
        if(is_null($model))
            die('Không Tìm Thấy Trang Yêu Cầu');
        $msg= false;
        if(isset($_POST['GasMaintainSell'])){
            $model->attributes=$_POST['GasMaintainSell'];
            $model->update(array('status_call_bad','note_call_bad'));
            $msg= 'Cập Nhật Trạng Thái Thành Công';
        }

        $this->render('popup/Update_supervision_list_maintain_sell_call_bad',array(
            'model'=>$model,
            'msg'=>$msg,
        )); 		

    }        

    public function actionUpdate_supervision_list_maintain_sell_seri_diff(){
        $this->layout = 'ajax';
        if(!isset($_GET['maintain_sell_id']))
            die('Không Tìm Thấy Trang Yêu Cầu');
        $model = GasMaintainSell::model()->findByPk($_GET['maintain_sell_id']);
        if(is_null($model))
            die('Không Tìm Thấy Trang Yêu Cầu');
        $msg= false;
        if(isset($_POST['GasMaintainSell'])){
            $model->attributes=$_POST['GasMaintainSell'];
            $model->update(array('status_seri_diff','note_seri_diff'));
            $msg= 'Cập Nhật Trạng Thái Thành Công';
        }

        $this->render('popup/Update_supervision_list_maintain_sell_seri_diff',array(
            'model'=>$model,
            'msg'=>$msg,
        )); 
    }

    // 10-31-2013 ANH DUNG
    public function actionExport_list_maintain(){
        if(isset($_SESSION['data-excel'])){
            set_time_limit(7200);
            ExportList::Export_list_maintain();
        }
        $this->redirect(array('index'));            
    }

    // 10-31-2013 ANH DUNG
    public function actionExport_list_for_monitoring_maintain(){
        if(isset($_SESSION['data-excel'])){
            set_time_limit(7200);
            ExportList::Export_list_for_monitoring_maintain();
        }
        $this->redirect(array('index'));            

    }         

    // 10-31-2013 ANH DUNG
    public function actionExport_supervision_list_maintain_call_bad(){
        if(isset($_SESSION['data-excel'])){
            set_time_limit(7200);
            ExportList::Export_supervision_list_maintain_call_bad();
        }
        $this->redirect(array('index'));            

    }         


    // 10-31-2013 ANH DUNG
    public function actionExport_supervision_list_maintain_sell_call_bad(){
        if(isset($_SESSION['data-excel'])){
            set_time_limit(7200);
            ExportList::Export_supervision_list_maintain_sell_call_bad();
        }
        $this->redirect(array('index'));            

    }         

    // 10-31-2013 ANH DUNG
    public function actionExport_supervision_list_maintain_sell_seri_diff(){
        if(isset($_SESSION['data-excel'])){
            set_time_limit(7200);
            ExportList::Export_supervision_list_maintain_sell_seri_diff();
        }
        $this->redirect(array('index'));                        
    }         

        
        
}
