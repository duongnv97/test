<?php

class SellController extends AdminController 
{
    public $pluralTitle = "Bán hàng hộ gia đình";
    public $singleTitle = "Bán hàng hộ gia đình";
    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id)
    {
        $this->pageTitle = 'Xem Bán hàng hộ gia đình';
        try{
        $this->render('view',array(
                'model'=>$this->loadModel($id), 'actions' => $this->listActionsCanAccess,
        ));
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }
    
    /** @Author: ANH DUNG Dec 22, 2017
     *  @Todo: nhả đơn app cho nhân viên khác nhận
     **/
    public function freeTransaction() {
        if(!isset($_GET['free_transaction'])){
            return ;
        }
        $mTransactionHistory = TransactionHistory::model()->findByPk($_GET['transaction_id']);
        if(empty($mTransactionHistory)){
            return ;
        }
        $mTransactionHistory->setEmptyUserCallCenter();
        Yii::app()->user->setFlash( MyFormat::SUCCESS_UPDATE, 'Nhả đơn hàng App thành công' );
        $this->redirect(array('index'));
    }
    
    /** @Author: DungNT Jun 17, 2019
     *  @Todo: get list agent lock app
     **/
    public function getArrayAgentLockApp() {
        return [
//            2068340, // Đại Lý Lê Chánh - An giang
        ];
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate()
    {
    try{
//        error_reporting(1); // var_dump(PHP_INT_SIZE);
        $this->pageTitle = 'Tạo Mới Bán hàng hộ gia đình';
        $this->ajaxModule();
        $model=new Sell('WebCreate');
        $this->freeTransaction();
        $this->checkRoleCreate($model);
        $this->validateCreateUrl($model);
        $this->initCreateSell($model);
        if(isset($_GET['phone_number'])){
            $model->phone = $_GET['phone_number'];
        }
        if(isset($_POST['Sell'])){
            $mReward = new Reward();
            $model->attributes = $_POST['Sell'];
            $this->checkAgentInactive($model);
            $model->rCustomer = Users::model()->findByPk($model->customer_id);
            $model->validate();
            $model->changePhoneNewPrefix();
            $model->webGetPost();
            $mReward->checkReward($model);
            $mReward->checkRewardApp($model);
            if(!$model->hasErrors()){
                $model->formatDbDatetime();
                $model->save();
                $model->webSaveDetail();// chưa tạo $mTransactionHistory
                $model->makeTransactionHistory();// only run at Window create from PMBH C# bắt đầu tạo $mTransactionHistory
                $model->updateIdToCall();// Jun 29, 2017 tạm return đã, để test lại rồi run sau
                $mReward->saveRewardInSell($model);
                if($model->status == Sell::STATUS_CANCEL){
                    $mCodePartner = new CodePartner();
                    $mCodePartner->updateCancel($model);
                }
                if($model->status == Sell::STATUS_PAID){
                    $mRewardPoint = new RewardPoint();
                    $mRewardPoint->addPoint($model);
                }
                Yii::app()->user->setFlash( MyFormat::SUCCESS_UPDATE, "Thêm mới thành công." );
                $this->redirect(array('view','id'=>$model->id));
            }
        }

        $this->render('create',array(
                'model'=>$model, 'actions' => $this->listActionsCanAccess,
        ));
        }catch (Exception $exc){
            $this->redirectErrors($exc, 'index');// DungNT Jul2419 close, do flash hiển thị nhiều lần khi User không reload page line cuộc gọi 1900
            GasCheck::CatchAllExeptiong($exc);
        }
    }
    
    /** @Author: ANH DUNG Sep 17, 2017
     *  @Todo: 
     */
    public function checkAgentInactive(&$model) {
        $aAgentOff = CacheSession::getListAgentInactive();
        if(in_array($model->agent_id, $aAgentOff)){
            throw new Exception($model->getAgent() . ' không còn hoạt động, không thể tạo đơn hàng');
//            $model->agent_id = '';
        }
        if($model->customer_id == GasConst::HGD_TRA_THE){
            throw new Exception('Không thể tạo đơn hàng cho KH này. Vui lòng tạo mới thông tin KH');
        }
        if($model->source == Sell::SOURCE_APP && in_array($model->agent_id, $this->getArrayAgentLockApp())){
            throw new Exception("Không thể tạo đơn hàng đặt app cho {$model->getAgent()}. Vui lòng liên hệ chuyên viên hoặc GĐKV");
        }

    }
    
    /** @Author: ANH DUNG Mar 15, 2017
     *  @Todo: init some info create Sell
     */
    public function initCreateSell(&$model) {
        $model->uid_login = MyFormat::getCurrentUid();
        $model->created_date_only = date('d/m/Y');
        $model->getDiscountType();
        $this->mapCustomer($model);
        $model->source = Sell::SOURCE_WEB;
        $model->mapTransaction();// agent_id luôn dc lấy bên model TransactionHistory
    }
    
    public function mapCustomer(&$model) {
        if(isset($_GET['customer_id'])){
            $model->customer_id = $_GET['customer_id'];
            if($model->rCustomer && $model->customer_id != GasConst::HGD_TRA_THE){
                $model->agent_id    = $model->rCustomer->area_code_id;
            }
        }
    }
    
    /** @Author: ANH DUNG Dec 19, 2017
     *  @Todo: kiểm tra role KTKV chỉ cho của anh Hiếu được phép tạo BH
     **/
    public function checkRoleCreate($model) {
        $cRole = MyFormat::getCurrentRoleId();
        $cUid = MyFormat::getCurrentUid();
        if($cRole == ROLE_ACCOUNTING_ZONE && !in_array($cUid, $model->getUserAnhHieu())){// Đào Thị Thu Quyên anh Hiếu khoán
            throw new Exception('Bạn không có quyền truy cập');
        }
    }
    
    /** @Author: ANH DUNG Mar 15, 2017
     *  @Todo: get mTransaction
     */
    public function validateCreateUrl(&$model) {
        if(isset($_GET['transaction_id'])){
            $model->mTransaction = TransactionHistory::model()->findByPk($_GET['transaction_id']);
        }
    }
    
    /** @Author: ANH DUNG Nov 06, 2016
     *  @Todo: kiểm tra url tạo có hợp lệ không
     */
    public function validateCreateUrlRemove($model) {
        $ok = true;
        if(!isset($_GET['transaction_id'])){
            throw new Exception('Yêu cầu không hợp lệ 1');
        }
        $mTransaction = TransactionHistory::model()->findByPk($_GET['transaction_id']);
        if(is_null($mTransaction) || !empty($mTransaction->sell_id) ){
            throw new Exception('Transaction yêu cầu không hợp lệ 2');
        }
        
        $cRole = MyFormat::getCurrentRoleId();
        if($cRole != ROLE_ADMIN && $mTransaction->agent_id != MyFormat::getAgentId()){
//        if($mTransaction->agent_id != MyFormat::getAgentId()){// đoạn trên ($cRole != ROLE_ADMIN) có thể bỏ, chỉ để cho admin test
            throw new Exception('Transaction yêu cầu không hợp lệ 3');
        }
        return $mTransaction;
    }

    /**Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated 
     */
    public function actionUpdate($id)
    {
        error_reporting(1); // var_dump(PHP_INT_SIZE);
        try{
        $model=$this->loadModel($id);
        $model->oldCode         = $model->pttt_code;
        $model->mBeforeUpdate   = clone $model;
        if(!$model->canUpdateWeb()){// add Now 07, 2016
            Yii::app()->user->setFlash( MyFormat::ERROR_UPDATE, "Yêu cầu không hợp lệ" );
            $this->redirect(array('index'));
        }
//        $model->checkUpdateGasRemain();
        $model->formatDataBeforeUpdate();
        $mReward = new Reward();
        $mReward->formatSellBeforeUpdate($model);
        $this->pageTitle = "$model->code_no - {$model->getAgent()} - $model->created_date_only - {$model->getCustomer('address')}";
        
        if(isset($_POST['Sell'])){
            $model->attributes=$_POST['Sell'];
            $this->handleRollbackPromotion($model);
            $this->checkAgentInactive($model);
            $model->validatePtttCode();
            $model->checkUnsetTimer();
            $model->validate();
            $model->changePhoneNewPrefix();
            $model->webGetPost();
            $mReward->checkReward($model);
            $mReward->checkRewardApp($model);
//            $model->setFirstOrder();
            if(!$model->hasErrors()){
                $model->formatDbDatetime();
                $model->update();
//                [TASKTIMER]
//                    update forecast Oct 31, 2018
                if($model->status == Sell::STATUS_PAID){
                    $mForecast = new Forecast();
                    $mForecast->handleGenerateForecastSell([$model->customer_id]);
                }
                $model->webSaveDetail();
                $model->updateSpjCode();
                $model->adminUpdateStoreCard();
                $model->checkChangeAgent();
                $model->addGasRemain();
                $model->saveEvent(Transaction::CALLCENTER_CHANGE, MyFormat::getCurrentUid());
                $mReward->saveRewardInSell($model);
                if($model->status == Sell::STATUS_CANCEL){
                    $mCodePartner = new CodePartner();
                    $mCodePartner->updateCancel($model);
                }
                if($model->status == Sell::STATUS_PAID){
                    $mRewardPoint = new RewardPoint();
                    $mRewardPoint->addPoint($model);
                }
                Yii::app()->user->setFlash( MyFormat::SUCCESS_UPDATE, "Cập nhật thành công." );
                $this->redirect(array('update','id'=>$model->id));
            }
        }

        $this->render('update',array(
                'model'=>$model, 'actions' => $this->listActionsCanAccess,
        ));
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }
    
    /** @Author: ANH DUNG Dec 19, 2018
     *  @Todo: admin click hủy Promotion của đơn này
     **/
    public function handleRollbackPromotion($model) {
        $cRole = MyFormat::getCurrentRoleId();
        if(empty($model->searchKm) || $cRole != ROLE_ADMIN){
            return ;
        }
        $model->promotion_id        = 0;
        $model->promotion_amount    = 0;
        $model->promotion_type      = 0;
        $mTransactionHistory = $model->rTransactionHistory;
        $mTransactionHistory->rollbackPromotionByHand();
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id)
    {
        try{
        if(Yii::app()->request->isPostRequest)
        {
            throw new CHttpException(400, 'IMPORTANT ! Admin setup không cho xóa, chỉ được hủy đơn hàng');// Feb2118
            // we only allow deletion via POST request 
            if($model = $this->loadModel($id))
            {
                if($model->delete()){
                    Yii::log("Uid: " .Yii::app()->user->id. " Delete record ".  print_r($model->attributes, true), 'info');
                }
            }

            // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
            if(!isset($_GET['ajax']))
                $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('index'));
        }
        else
        {
            Yii::log("Uid: " .Yii::app()->user->id. " Invalid request. Please do not repeat this request again.");
            throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
        }
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }

    /**
     * Manages all models.
     */
    public function actionIndex()
    {
        $this->renderInventoryAgent();
        $this->pageTitle = 'Sell HGD - Danh sách bán hàng hộ gia đình';
        $this->allowIndex();
        $this->makeTestOrder();
        CacheSession::getListIdTelesale();
        $cRole = MyFormat::getCurrentRoleId();
        try{
        $model=new Sell('search');
        $this->handleAgentClickUpdate($model);
        $model->InitMaterial();
        $model->unsetAttributes();  // clear any default values
//        $model->date_from   = date("d-m-Y");
//        $model->date_to     = date("d-m-Y");
//        [TASKTIMER]
        if(isset($_GET['type']) && $_GET['type'] == Sell::TYPE_TIMER){
            $model->is_timer = Forecast::TYPE_TIMER; 
        }else{
            $model->is_timer = Forecast::TYPE_IMMEDIATE;
        }
        if(isset($_GET['Sell']))
            $model->attributes=$_GET['Sell'];
        $this->applySomeRule($model);

        $this->render('index',array(
                'model'=>$model, 'actions' => $this->listActionsCanAccess,
        ));
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }
    
    public function allowIndex(){
        $cUid = MyFormat::getCurrentUid();
        $cRole = MyFormat::getCurrentRoleId();
        $aUidAllow  = [GasConst::UID_SU_BH, 
        ];
        $aRoleAllow = [ROLE_SALE];
        if(in_array($cRole, $aRoleAllow) && !in_array($cUid, $aUidAllow)){
            $this->redirect(Yii::app()->createAbsoluteUrl(''));
        }
    }
    
    /** @Author: DungNT Jan 11, 2019
     *  @Todo: limit condition for some role
     **/
    public function applySomeRule(&$model) {
        $cRole = MyFormat::getCurrentRoleId();
        $cUid = MyFormat::getCurrentUid();
        if($cRole == ROLE_SUB_USER_AGENT){
            $model->agent_id = MyFormat::getAgentId();
        }
        if($cRole == ROLE_EMPLOYEE_MARKET_DEVELOPMENT){
            $mAppPromotion = new AppPromotion();
            $mAppPromotion->owner_id = $cUid;
            $model->promotion_id = $mAppPromotion->getOnlyCodeByUser();
        }
    }
    
    /**
     * @Author: ANH DUNG Aug 25, 2016
     */
    public function handleAgentClickUpdate($model) {
        if(isset($_GET['AgentClickUpdate'])){
            if($model->handleAgentClickUpdate()){
                $msg = "Cập nhật thẻ kho hộ gia đình thành công";
                $notifyType = MyFormat::SUCCESS_UPDATE;
            }else{
                $msg = "Cập nhật thất bại, mỗi lần cập nhật phải cách nhau $model->minutesAgentUpdate phút";
                $notifyType = MyFormat::ERROR_UPDATE;
            }
            Yii::app()->user->setFlash($notifyType, $msg);
            $this->redirect(array('index'));
        }
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
    * If the data model is not found, an HTTP exception will be raised.
     * @param integer the ID of the model to be loaded
     */
    public function loadModel($id)
    {
        try{
        $model=Sell::model()->findByPk($id);
        if($model===null)
        {
            Yii::log("The requested page does not exist.");
            throw new CHttpException(404,'The requested page does not exist.');
        }			
        return $model;
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }

    /**
     * @Author: ANH DUNG Aug 23, 2016
     * @Todo: report summary agent
     * admin/reportAgent
     */
    public function actionReportAgent()
    {
        $this->pageTitle = 'HGD - báo cáo bán hàng hộ gia đình';
        $this->reportAgentExcel();
        try{
            $model = new Sell();
            $model->InitMaterial();
            $model->date_from   = date("d-m-Y");
            $model->date_to     = date("d-m-Y");
            $cRole = Yii::app()->user->role_id;
            if($cRole == ROLE_SUB_USER_AGENT){
                $model->agent_id = MyFormat::getAgentId();
            }
            $aData = array();
            if(!empty($_GET['type_report_agent'])){
               $model->type_report_agent = $_GET['type_report_agent'];
            }
            if(isset($_GET['Sell'])){
                $model->attributes = $_GET['Sell'];
                $model->typeReport = Sell::REPORT_WEB;
                $model->checkAccessAgent($model->agent_id);
                if(is_array($model->province_id)){
                    $model->isWebSumAll = true;
                }
                $aData = $model->ReportAgent();
                $_SESSION['data-excel'] = $aData;
                $_SESSION['data-model'] = $model;
            }
            $this->render('report/agent',array( "aData"=>$aData,
                    'model'=>$model, 'actions' => $this->listActionsCanAccess,
            ));
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }
    
    /** @Author: ANH DUNG Now 08, 2017
     * @Todo: ExportExcel
     */
    public function reportAgentExcel(){
        try{
        if(isset($_GET['to_excel']) && isset($_SESSION['data-excel'])){
            ini_set('memory_limit','1500M');// chưa cần chạy đoạn này, đưa memory_limit lên 500 có thể export dc khoảng 5000 row - chưa test
            ToExcel::summaryHgd();
        }
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }
    
    /**
     * @Author: ANH DUNG Aug 05, 2016
     * @Todo: ExportExcel
     */
    public function actionExportExcel(){
        try{
        if(isset($_SESSION['data-excel'])){
            set_time_limit(7200);
//            ini_set('memory_limit','1000M');// Apr2319 DungNT Close - only use default mem 300mb
            $mExportList = new ExportList();
            $mExportList->sellDaily($_SESSION['data-excel']->data, new Sell());
        }
        $this->redirect(array('index'));
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }
    
    /**
     * @Author: ANH DUNG Sep 18, 2016
     * @Todo: report target ccs
     * admin/targetCcs
     */
    public function actionTargetCcs()
    {
        $this->pageTitle = 'Báo cáo bình quay về ccs';
        try{
            $model = new GasPtttDailyGoback();
            $model->date_from   = "01-".date('m-Y');
            $model->date_to     = date("d-m-Y");
            $aData = array();
            if(isset($_GET['GasPtttDailyGoback'])){
                $model->attributes=$_GET['GasPtttDailyGoback'];
                $aData = $model->getOutputCCSNew();
            }
            
            $this->render('report/TargetCcs',array( "aData"=>$aData,
                    'model'=>$model, 'actions' => $this->listActionsCanAccess,
            ));
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }
    
    /** @Author: PHAM THANH NGHIA 28/5/2018
     *  @Todo:  
     *  @Code: NGHIA002
     *  @Param: 
     *  Làm trang list comment + rating của table gas_transaction_history
        admin/sell/listRating
     **/
    public function actionListRating(){
        $this->pageTitle = 'KH app Gas24h đánh giá';
        try{
            $model=new TransactionHistoryRating('search');
       
            if(isset($_GET['TransactionHistoryRating']))
                $model->attributes=$_GET['TransactionHistoryRating'];

            $this->render('listRating/index',array(
                    'model'=>$model, 'actions' => $this->listActionsCanAccess,
            ));
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }
    
    /** @Author: PHAM THANH NGHIA 30/5/2018
     *  @Todo: 
     *  @Code: NGHIA003
     *  @Param: Kiểm tra trạng thái hủy đơn hàng của chị Ngọc - để báo cáo
     *  cập nhật vào order_type_status (lý do hủy mới), note
     **/
    public function actionAudit($id){
        try {
            $this->layout = "ajax";
            $model =  Sell::model()->findbyPk($id);
            
            if(empty($model)){
                throw new Exception('Invalid request');
            }
            
            if(!$model->canAuditWeb()){
                Yii::app()->user->setFlash( MyFormat::ERROR_UPDATE, "Yêu cầu không hợp lệ" );
                $this->redirect(array('index'));
            }
            if(isset($_POST['Sell'])){
                $model->attributes = $_POST['Sell'];
                $model->saveAudit();
                Yii::app()->user->setFlash( MyFormat::SUCCESS_UPDATE, "Thêm mới kiểm tra thành công." );
                $this->redirect(array('audit', 'id'=>$id));
            }
            
            $this->render('audit/_audit',array(
                    'model'=>$model, 'actions' => $this->listActionsCanAccess,
            ));
        } catch (Exception $exc) {
            GasCheck::CatchAllExeptiong($exc);
        }
    }
    
    /** @Author: ANH DUNG Jun 05, 2018
     *  @Todo: tạo đơn test cho bình thạnh 2
     **/
    public function makeTestOrder() {
        if(GasCheck::isServerLive() || !isset($_GET['makeTestOrder'])){
            return ;
        }
        for($i=0; $i<10; $i++){
            $mTransactionHistory = new TransactionHistory();
            $mTransactionHistory->agent_id = 658920;// BT2
            $mTransactionHistory->mAppUserLogin = Users::model()->findByPk(138559);
            $mTransactionNew = $mTransactionHistory->autoCreateOrder();
        }
        $this->redirect(array('index'));
    }
    
    /** @Author: NamNH 09/2018
     *  @Todo: report for all agent
     *  @Param:
     **/
    public function actionReportAllAgent(){
        $this->pageTitle = 'BC HGD - toàn hệ thống';
        $this->reportAllAgentExcel();
        try{
            $model = new Sell();
            $model->InitMaterial();
            $model->date_from   = '01-'.date('m-Y');
            $model->date_to     = date('d-m-Y');
            $cRole = MyFormat::getCurrentRoleId();
            $aData = [];
            if(isset($_GET['Sell'])){
                $model->attributes = $_GET['Sell'];
//                $model->typeReport = Sell::REPORT_WEB;
                $mSellReport = new SellReport();
                $mSellReport->mSell = $model;
                $aData = $mSellReport->ReportAllAgent();
                $_SESSION['data-model-all'] = $mSellReport;
            }
            
            $this->render('report/allAgent',array( 
                "aData"=>$aData,
                'model'=>$model,
                'actions' => $this->listActionsCanAccess,
            ));
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }
    
    /** @Author: NamNH 09/2018
     *  @Todo: report agent of report
     **/
    public function reportAllAgentExcel(){
        try{
            if(isset($_GET['to_excel']) && isset($_SESSION['data-model-all'])){
                ini_set('memory_limit','1500M');// chưa cần chạy đoạn này, đưa memory_limit lên 500 có thể export dc khoảng 5000 row - chưa test
                ToExcel::summaryReportAllAgent();
            }
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }
    
    /** @Author: DuongNV Sep 07, 2018
     *  @Todo: report trả thẻ
     **/
    public function actionReportTraThe() {
        $this->pageTitle = 'Báo cáo đơn hàng trả thẻ';
        try{
            $model = new SellReport();
            $model->date_from   = date("01-m-Y");
            $model->date_to     = date("d-m-Y");
            $aData = array();
            if(isset($_GET['SellReport'])){
                $model->attributes = $_GET['SellReport'];
                $model->date_from = MyFormat::dateConverYmdToDmy($model->date_from, 'd-m-Y');
                $model->date_to   = MyFormat::dateConverYmdToDmy($model->date_to, 'd-m-Y');
                $aData = $model->getTraTheReport();
            }
            $this->render('report/reportTraThe',array( 
                "aData"=>$aData,
                'model'=>$model,
                'actions' => $this->listActionsCanAccess,
            ));
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }
    
    /** @Author: NamNH Oct 19, 2018
     *  @Todo: report discount sell
     **/
    public function actionReportDiscount(){
        $this->pageTitle = 'Thống kê giảm giá HGĐ';
        try{
            $model = new SellReport();
            $model->date_from   = date("01-m-Y");
            $model->date_to     = date("d-m-Y");
            $aData = array();
            if(isset($_GET['SellReport'])){
                $model->attributes = $_GET['SellReport'];
                $model->date_from = MyFormat::dateConverYmdToDmy($model->date_from, 'd-m-Y');
                $model->date_to   = MyFormat::dateConverYmdToDmy($model->date_to, 'd-m-Y');
                $aData = $model->getReportDiscount();
            }
            $this->exportExcelDiscount();
            $this->render('report/reportDiscount',array( 
                'aData'=>$aData,
                'model'=>$model,
                'actions' => $this->listActionsCanAccess,
            ));
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }
    
    public function exportExcelDiscount(){
        if(isset($_GET['to_excel']) && isset($_SESSION['data-model-discount'])){
            ini_set('memory_limit','1500M');// chưa cần chạy đoạn này, đưa memory_limit lên 500 có thể export dc khoảng 5000 row - chưa test
            ToExcel::summaryReportDiscount();
        }
    }
    
    /** @Author: KHANH TOAN 2018
     *  @Todo: báo cáo doanh thu của từng NV tổng đài
     **/
    public function actionReportUidLogin()
    {
        $this->pageTitle = 'BC doanh thu NV tổng đài';
        $this->reportUidLoginExcel();
        try{
            $model = new SellReport();
            $model->InitMaterial();
            $model->date_from   = date("d-m-Y");
            $model->date_to     = date("d-m-Y");
            $cRole = Yii::app()->user->role_id;
            if($cRole == ROLE_SUB_USER_AGENT){
                $model->agent_id = MyFormat::getAgentId();
            }
            $aData = array();
            if(isset($_GET['SellReport'])){
                $model->attributes = $_GET['SellReport'];
                if(is_array($model->province_id)){
                    $model->isWebSumAll = true;
                }
                $aData = $model->ReportUidLogin();
                $_SESSION['data-excel'] = $aData;
                $_SESSION['data-model'] = $model;
            }
            $this->render('reportUidLogin/uidlogin',array( "aData"=>$aData,
                    'model'=>$model, 'actions' => $this->listActionsCanAccess,
            ));
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }
    
    /**
     * @Author: KHANH TOAN Nov 02, 2018
     * @Todo: ExportExcel
     */
    public function reportUidLoginExcel(){
        try{
        if(isset($_GET['to_excel']) && isset($_SESSION['data-excel'])){
            ini_set('memory_limit','1500M');// chưa cần chạy đoạn này, đưa memory_limit lên 500 có thể export dc khoảng 5000 row - chưa test
            $mToExcel1 = new ToExcel1();
            $mToExcel1->exportReportUidLogin();
            
        }
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }
    
    /** @Author: KHANH TOAN Nov 02, 2018
     *  @Todo: số bình mới mà nhân viên tổng đài tư vẫn đượcs trong tháng từ hóa đơn gần nhất
     *  @Param:
     **/
    public function actionReportQuantity(){
        $this->pageTitle = 'BC sản lượng bình cam';
        $this->reportQuantityExcel();
        try {
            $model = new SellReportQuantity();
            $model->unsetAttributes();  // clear any default values
            $aData = array();
            $model->date_from   = date("01-m-Y");
            $model->date_to     = date("d-m-Y");
            if(isset($_GET['SellReportQuantity'])){
                $model->attributes = $_GET['SellReportQuantity'];
            }
//            $aData = $model->getReportQuantity();   
            $aData = $model->getReportQuantityV2();   
            $_SESSION['dataReportQuantity'] = $aData;
            $_SESSION['data-model']         = $model;
            $this->render('reportQuantity/index',array(
                'aData'=>$aData,
                'model' => $model,
                'actions' => $this->listActionsCanAccess,
            ));
        } catch (Exception $exc) {
            GasCheck::CatchAllExeptiong($exc);
        }
    }
    
    /**
     * @Author: KHANH TOAN Nov 11, 2018
     * @Todo: ExportExcel
     */
    public function reportQuantityExcel(){
        try{
        if(isset($_GET['to_excel']) && isset($_SESSION['dataReportQuantity'])){
            ini_set('memory_limit','1500M');// chưa cần chạy đoạn này, đưa memory_limit lên 500 có thể export dc khoảng 5000 row - chưa test
            $mToExcel1 = new ToExcel1();
            $mToExcel1->exportReportQuantity();
            
        }
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }
    
    /** @Author: KHANH TOAN 2018
     *  @Todo: lí do hủy đơn hàng
     *  @Param:
     **/
    public function actionCommentCanCel($belongId){
        try {
            $this->layout = 'ajax';
            $mGasComment = new GasComment();
            $mGasComment->belong_id = $belongId;
            $mGasComment->type      = GasComment::TYPE_4_SELL_CANCEL;
            $mGasComment            = $mGasComment->getByBelongId();
            if(empty($mGasComment)){
                $mGasComment = new GasComment();
                $mGasComment->belong_id  = $belongId;
            }
            $mSell = $mGasComment->rSell;
            if(!$mSell->canCommentReasonCancel()){
                die('Invalid request');
            }
            
            if(isset($_POST['GasComment'])){
                $mGasComment->attributes = $_POST['GasComment'];
                $mGasComment->saveCommentCancel();
                Yii::app()->user->setFlash( MyFormat::SUCCESS_UPDATE, "Thêm mới lí do thành công." );
                $this->redirect(array('commentCancel', 'belongId'=>$belongId));
            }
            
            $this->render('commentCancel/_commentCancel',array(
                'model' => $mGasComment, 
                'mSell' => $mSell, 
                'actions' => $this->listActionsCanAccess,
            ));
        } catch (Exception $exc) {
            GasCheck::CatchAllExeptiong($exc);
        }
    }
    
    //NamNH ajax in form created_update sell
    public function ajaxModule(){
        if(isset($_GET['AJAX_MODULE'])){
            $id = isset($_GET['idCustomer']) ? $_GET['idCustomer'] : 0;
            $mRewardPoint = new RewardPoint();
            $title = $mRewardPoint->getTitleRewardWeb($id);
            $aData = [];
            $aData['title'] = $title;
            echo json_encode($aData);
            die;
        }
        if(isset($_GET['AJAX_REWARD_APP'])){
            $id = isset($_GET['idCustomer']) ? $_GET['idCustomer'] : 0;
            $sellId = isset($_GET['idSell']) ? $_GET['idSell'] : 0;
            $mCodePartner = new CodePartner();
            $html = $mCodePartner->getCodePartnerWeb($id,$sellId);
            $aData = [];
            $aData['html'] = $html;
            echo json_encode($aData);
            die;
        }
        
    }
    
    /** @Author: NamNH Sep2519
    *  @Todo: reder by ajax inventory
    **/
    public function renderInventoryAgent(){
        if(isset($_GET['AJAX_AGENT_INVENTORY'])){
            $this->layout = 'removeLayout';
            $model = new Sell();
            $idAgent = isset($_GET['idAgent']) ? $_GET['idAgent'] : 0;
            $model->agent_id = $idAgent;
            $this->render('_form_col2',array(
                'model' => $model,
                'actions' => $this->listActionsCanAccess,
            ));
            die;
        }
    }
}
