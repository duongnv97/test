<?php
/** Author: bb - recode ANH DUNG
 * May 13, 2014
 */
class RolesAuthController extends AdminController
{
    
    // ANH DUNG
    public function actionResetRoleCustomOfUser($id)
    {
        $criteria = new CDbCriteria;
        $criteria->compare('user_id', $id);
        ActionsUsers::model()->deleteAll($criteria);
        $this->redirect(array('user','id'=>$id));
        
        // bb root
//        $aUserId = array();
//        $criteria = new CDbCriteria;
//        $criteria->compare('t.role_id', $id);
//        $mUsers = Users::model()->findAll($criteria);
//        if ($mUsers)
//            $aUserId = CHtml::listData($mUsers, 'id', 'id');
//        
//        $criteria = new CDbCriteria;
//        $criteria->addInCondition('user_id', $aUserId);
//        ActionsUsers::model()->deleteAll($criteria);
//        $this->redirect(array('group','id'=>$id));        
    }
    
    //bb
    public function actionGroup($id)
    {
        try
        {
            $mGroup = Roles::model()->findByPk($id);
            $this->pageTitle = 'Phân Quyền Cho Role Groups - '.$mGroup->role_name;
            if(isset($_POST['submit']))
            {
                foreach ($this->aControllers as $keyController => $aController) 
                {
                    $mController = Controllers::getByName($keyController);
                    if($mController)
                    {
                        $mController->addGroupRoles($this->postArrayCheckBoxToAllowDenyValue($keyController), $id);
                        Yii::app()->user->setFlash('successUpdate', "Cập Nhật Thành Công");
                    }                    
                }
                $this->refresh();
            }
            $this->render('group',array(
                    'id'=>$id,
                    'mGroup'=>$mGroup,
                    'actions' => $this->listActionsCanAccess,
            ));
        }
        catch (Exception $exc)
        {
            Yii::log("Uid: " .Yii::app()->user->id. " Exception ".  $exc->getMessage(), 'error');
            $code = 404;
            if(isset($exc->statusCode))
                $code=$exc->statusCode;
            if($exc->getCode())
                $code=$exc->getCode();
            throw new CHttpException($code, $exc->getMessage());
        }
    }
    /*/bb**
     * thiet lap quyen trong user se uu tien cao nhat. user deny hoac allow thi se k phu thuoc group.
     * not yet test check it
     */
    public function actionUser($id)
    {
        try
        {
            $mUser = Users::model()->findByPk($id);
            $this->pageTitle = 'Phân Quyền Cho Users - '.$mUser->first_name;
            if(is_null($mUser))
                throw new Exception('Phân quyền cho user tồn tại');
           if(isset($_POST['submit']))
            {
                foreach ($this->aControllers as $keyController => $aController) 
                {
                    $mController = Controllers::getByName($keyController);
                    if($mController)
                    {
                        $mController->addUserRoles($this->postArrayCheckBoxToAllowDenyValue($keyController), $id);
                        Yii::app()->user->setFlash('successUpdate', "Cập Nhật Thành Công");
                    }                    
                }
                $this->refresh();
            }
            $this->render('user',array(
                    'id'=>$id,
                    'mUser'=>$mUser,
                    'actions' => $this->listActionsCanAccess,
            ));
        }
        catch (Exception $exc)
        {
            Yii::log("Uid: " .Yii::app()->user->id. " Exception ".  $exc->getMessage(), 'error');
            $code = 404;
            if(isset($exc->statusCode))
                $code=$exc->statusCode;
            if($exc->getCode())
                $code=$exc->getCode();
            throw new CHttpException($code, $exc->getMessage());
        }
    }
    
    //bb
    public function postArrayCheckBoxToAllowDenyValue($keyController)
    {
        $aResult = array();
        $aControllers = $this->aControllers;
        foreach($aControllers[$keyController]['actions'] as $keyAction=>$aAction){
            if(isset($_POST[$keyController][$keyAction]))
            {
                $aResult[$keyAction] = 'allow';
                foreach($aAction['childActions'] as $childAction)
                {
                    $aResult[$childAction] = 'allow';
                }
            }
            else
            {
                $aResult[$keyAction] = 'deny';
                foreach($aAction['childActions'] as $childAction)
                {
                    $aResult[$childAction] = 'deny';
                }
            }
        }
        
        return $aResult;
    }
    
    public $aControllers = array(         
        'UsersHgdPttt'=>array(
            'alias'=>'PTTT điểm làm việc',
            'actions'=>array(
                'Index'=>array('alias'=>'Index',
                                'childActions'=>array()//những actions mà sẽ được cấp quyền hoặc hủy theo action cha. EX 'childActions'=>array('Approve','Reject')
                                ),
                )
        ),/* end controller */
        'ContractListManagement'=>array(
            'alias'=>'QL Hợp Đồng Quyết Toán',
            'actions'=>array(
                'Index'=>array('alias'=>'Index',
                                'childActions'=>array()//những actions mà sẽ được cấp quyền hoặc hủy theo action cha. EX 'childActions'=>array('Approve','Reject')
                                ),
                'Update'=>array('alias'=>'Update',
                                'childActions'=>array()//những actions mà sẽ được cấp quyền hoặc hủy theo action cha. EX 'childActions'=>array('Approve','Reject')
                                ),
                'Create'=>array('alias'=>'Create',
                                'childActions'=>array()//những actions mà sẽ được cấp quyền hoặc hủy theo action cha. EX 'childActions'=>array('Approve','Reject')
                                ),
                'Delete'=>array('alias'=>'Delete',
                                'childActions'=>array()//những actions mà sẽ được cấp quyền hoặc hủy theo action cha. EX 'childActions'=>array('Approve','Reject')
                                ),
                'View'=>array('alias'=>'View',
                                'childActions'=>array()//những actions mà sẽ được cấp quyền hoặc hủy theo action cha. EX 'childActions'=>array('Approve','Reject')
                                ),
                )
        ),/* end controller */
        'SpinNumbers'=>array(
            'alias'=>'Quản lý vé số - SpinNumbers',
            'actions'=>array(
                'Index'=>array('alias'=>'Index',
                                'childActions'=>array()//những actions mà sẽ được cấp quyền hoặc hủy theo action cha. EX 'childActions'=>array('Approve','Reject')
                                ),
                )
        ),/* end controller */
        'SpinCampaigns'=>array(
            'alias'=>'Quản lý chiến dịch quay số - SpinCampaigns',
            'actions'=>array(
                'Index'=>array('alias'=>'Index',
                                'childActions'=>array()//những actions mà sẽ được cấp quyền hoặc hủy theo action cha. EX 'childActions'=>array('Approve','Reject')
                                ),
                'Create'=>array('alias'=>'Tạo mới',
                                'childActions'=>array()
                                ),
                'Update'=>array('alias'=>'Cập nhật',
                                'childActions'=>array()//những actions mà sẽ được cấp quyền hoặc hủy theo action cha (Index)
                                ),
                'View'=>array('alias'=>'Xem',
                                'childActions'=>array()
                                ),
                'Delete'=>array('alias'=>'Xóa',
                                'childActions'=>array()
                                ),
                'SpinLucky'=>array('alias'=>'Vòng quay may mắn',
                                'childActions'=>array()
                                ),
                'NotifyWinning'=>array('alias'=>'Notify KH trúng giải',
                                'childActions'=>array()
                                ),
                )
        ),/* end controller */
        'SpinCampaignDetails'=>array(
            'alias'=>'Quản lý giải thưởng - SpinCampaignDetails',
            'actions'=>array(
                'CancelAward'=>array('alias'=>'Hủy bỏ giải',
                                'childActions'=>array()//những actions mà sẽ được cấp quyền hoặc hủy theo action cha. EX 'childActions'=>array('Approve','Reject')
                                ),
                'Index'=>array('alias'=>'Index',
                                'childActions'=>array()//những actions mà sẽ được cấp quyền hoặc hủy theo action cha. EX 'childActions'=>array('Approve','Reject')
                                ),
                'Update'=>array('alias'=>'Cập nhật',
                                'childActions'=>array()//những actions mà sẽ được cấp quyền hoặc hủy theo action cha (Index)
                                ),
                )
        ),/* end controller */
        'Assets'=>array(
            'alias'=>'Quản lý tài sản - Assets',
            'actions'=>array(
                'Index'=>array('alias'=>'Index',
                                'childActions'=>array()//những actions mà sẽ được cấp quyền hoặc hủy theo action cha. EX 'childActions'=>array('Approve','Reject')
                                ),
                'Create'=>array('alias'=>'Tạo mới',
                                'childActions'=>array()
                                ),
                'Update'=>array('alias'=>'Cập nhật',
                                'childActions'=>array()//những actions mà sẽ được cấp quyền hoặc hủy theo action cha (Index)
                                ),
                'View'=>array('alias'=>'Xem',
                                'childActions'=>array()
                                ),
                'Delete'=>array('alias'=>'Xóa',
                                'childActions'=>array()
                                ),
                )
        ),/* end controller */
        'UsersPriceOther'=>array(
            'alias'=>'Set up giá KH - UsersPriceOther',
            'actions'=>array(
                'Index'=>array('alias'=>'Index',
                                'childActions'=>array()//những actions mà sẽ được cấp quyền hoặc hủy theo action cha. EX 'childActions'=>array('Approve','Reject')
                                ),
                'Create'=>array('alias'=>'Tạo mới',
                                'childActions'=>array()
                                ),
                'Update'=>array('alias'=>'Cập nhật',
                                'childActions'=>array()//những actions mà sẽ được cấp quyền hoặc hủy theo action cha (Index)
                                ),
                'View'=>array('alias'=>'Xem',
                                'childActions'=>array()
                                ),
                'Delete'=>array('alias'=>'Xóa',
                                'childActions'=>array()
                                ),
                )
        ),/* end controller */
        'GasSourceReport'=>array(
            'alias'=>'Báo cáo nguồn hàng - GasSourceReport',
            'actions'=>array(
                'Index'=>array('alias'=>'Index',
                                'childActions'=>array()//những actions mà sẽ được cấp quyền hoặc hủy theo action cha. EX 'childActions'=>array('Approve','Reject')
                                ),
                'Create'=>array('alias'=>'Tạo mới',
                                'childActions'=>array()
                                ),
                'Update'=>array('alias'=>'Cập nhật',
                                'childActions'=>array()//những actions mà sẽ được cấp quyền hoặc hủy theo action cha (Index)
                                ),
                'View'=>array('alias'=>'Xem',
                                'childActions'=>array()
                                ),
                'Delete'=>array('alias'=>'Xóa',
                                'childActions'=>array()
                                ),
                )
        ),/* end controller */
        'DocManagement'=>array(
            'alias'=>'QL Mẫu Văn Bản - DocManagement',
            'actions'=>array(
                'Index'=>array('alias'=>'Index',
                                'childActions'=>array()//những actions mà sẽ được cấp quyền hoặc hủy theo action cha. EX 'childActions'=>array('Approve','Reject')
                                ),
                'Create'=>array('alias'=>'Tạo mới',
                                'childActions'=>array()
                                ),
                'Update'=>array('alias'=>'Cập nhật',
                                'childActions'=>array()//những actions mà sẽ được cấp quyền hoặc hủy theo action cha (Index)
                                ),
                'View'=>array('alias'=>'Xem',
                                'childActions'=>array()
                                ),
                'Delete'=>array('alias'=>'Xóa',
                                'childActions'=>array()
                                ),
                )
        ),/* end controller */
        'BranchCompany'=>array(
            'alias'=>'QL Chi Nhánh Địa Điểm - BranchCompany',
            'actions'=>array(
                'Index'=>array('alias'=>'Index',
                                'childActions'=>array()//những actions mà sẽ được cấp quyền hoặc hủy theo action cha. EX 'childActions'=>array('Approve','Reject')
                                ),
                'Create'=>array('alias'=>'Tạo mới',
                                'childActions'=>array()
                                ),
                'Update'=>array('alias'=>'Cập nhật',
                                'childActions'=>array()//những actions mà sẽ được cấp quyền hoặc hủy theo action cha (Index)
                                ),
                'View'=>array('alias'=>'Xem',
                                'childActions'=>array()
                                ),
                'Delete'=>array('alias'=>'Xóa',
                                'childActions'=>array()
                                ),
                )
        ),/* end controller */
        'GasReceipts'=>array(
            'alias'=>'Số biên bản giao nhận - GasReceipts',
            'actions'=>array(
                'Index'=>array('alias'=>'Index',
                                'childActions'=>array()//những actions mà sẽ được cấp quyền hoặc hủy theo action cha. EX 'childActions'=>array('Approve','Reject')
                                ),
                'Create'=>array('alias'=>'Tạo mới',
                                'childActions'=>array()
                                ),
                'Update'=>array('alias'=>'Cập nhật',
                                'childActions'=>array()//những actions mà sẽ được cấp quyền hoặc hủy theo action cha (Index)
                                ),
                'View'=>array('alias'=>'Xem',
                                'childActions'=>array()
                                ),
                'Delete'=>array('alias'=>'Xóa',
                                'childActions'=>array()
                                ),
                'ReportNumber'=>array('alias'=>'BC số biên bản giao nhận - reportNumber',
                                'childActions'=>array()
                                ),
                )
        ),/* end controller */
        'ClosingPersonalDebit'=>array(
            'alias'=>'BC Hạn Mức Vỏ - ClosingPersonalDebit',
            'actions'=>array(
                'Index'=>array('alias'=>'Index',
                                'childActions'=>array()//những actions mà sẽ được cấp quyền hoặc hủy theo action cha. EX 'childActions'=>array('Approve','Reject')
                                ),
                'Create'=>array('alias'=>'Tạo mới',
                                'childActions'=>array()
                                ),
                'Update'=>array('alias'=>'Cập nhật',
                                'childActions'=>array()//những actions mà sẽ được cấp quyền hoặc hủy theo action cha (Index)
                                ),
                'View'=>array('alias'=>'Xem',
                                'childActions'=>array()
                                ),
                'Delete'=>array('alias'=>'Xóa',
                                'childActions'=>array()
                                ),
                'ReportVo'=>array('alias'=>'BC Hạn Mức Vỏ - ReportVo',
                                'childActions'=>array()
                                ),
                )
        ),/* end controller */
        'CheckAgent'=>array(
            'alias'=>'Báo cáo kiểm kho đại lý - CheckAgent',
            'actions'=>array(
                'Index'=>array('alias'=>'Index',
                                'childActions'=>array()//những actions mà sẽ được cấp quyền hoặc hủy theo action cha. EX 'childActions'=>array('Approve','Reject')
                                ),
                'Create'=>array('alias'=>'Tạo mới',
                                'childActions'=>array()
                                ),
                'Update'=>array('alias'=>'Cập nhật',
                                'childActions'=>array()//những actions mà sẽ được cấp quyền hoặc hủy theo action cha (Index)
                                ),
                'View'=>array('alias'=>'Xem',
                                'childActions'=>array()
                                ),
                'Delete'=>array('alias'=>'Xóa',
                                'childActions'=>array()
                                ),
                'Report'=>array('alias'=>'Báo cáo kiểm kho - Report',
                                'childActions'=>array()
                                ),
                )
        ),/* end controller */
        'UsersInfo'=>array(
            'alias'=>'Thông tin khách hàng',
            'actions'=>array(
                'Index'=>array('alias'=>'Index',
                                'childActions'=>array()//những actions mà sẽ được cấp quyền hoặc hủy theo action cha. EX 'childActions'=>array('Approve','Reject')
                                ),
                'View'=>array('alias'=>'Xem',
                                'childActions'=>array()
                                ),
                )
        ),/* end controller */
        'TruckPlan'=>array(
            'alias'=>'Lịch Giao Nhận Xe Bồn - truckPlan',
            'actions'=>array(
                'Index'=>array('alias'=>'Index',
                                'childActions'=>array()//những actions mà sẽ được cấp quyền hoặc hủy theo action cha. EX 'childActions'=>array('Approve','Reject')
                                ),
                'Create'=>array('alias'=>'Tạo mới',
                                'childActions'=>array()
                                ),
                'Update'=>array('alias'=>'Cập nhật',
                                'childActions'=>array()//những actions mà sẽ được cấp quyền hoặc hủy theo action cha (Index)
                                ),
                'View'=>array('alias'=>'Xem',
                                'childActions'=>array()
                                ),
                'Delete'=>array('alias'=>'Xóa',
                                'childActions'=>array()
                                ),
                )
        ),/* end controller */
        
        'Reward'=>array(
            'alias'=>'Setup Quà Đổi Điểm Thưởng - Reward',
            'actions'=>array(
                'Index'=>array('alias'=>'Index',
                                'childActions'=>array()//những actions mà sẽ được cấp quyền hoặc hủy theo action cha. EX 'childActions'=>array('Approve','Reject')
                                ),
                'Create'=>array('alias'=>'Tạo mới',
                                'childActions'=>array()
                                ),
                'Update'=>array('alias'=>'Cập nhật',
                                'childActions'=>array()//những actions mà sẽ được cấp quyền hoặc hủy theo action cha (Index)
                                ),
                'View'=>array('alias'=>'Xem',
                                'childActions'=>array()
                                ),
                'Delete'=>array('alias'=>'Xóa',
                                'childActions'=>array()
                                ),
                )
        ),/* end controller */
        
        'RewardPoint'=>array(
            'alias'=>'Quản Lý Điểm Khách Hàng - rewardPoint',
            'actions'=>array(
                'Index'=>array('alias'=>'Index',
                                'childActions'=>array()//những actions mà sẽ được cấp quyền hoặc hủy theo action cha. EX 'childActions'=>array('Approve','Reject')
                                ),
                'Update'=>array('alias'=>'Cập nhật',
                                'childActions'=>array()//những actions mà sẽ được cấp quyền hoặc hủy theo action cha (Index)
                                ),
                'View'=>array('alias'=>'Xem',
                                'childActions'=>array()
                                ),
                'Delete'=>array('alias'=>'Xóa',
                                'childActions'=>array()
                                ),
                )
        ),/* end controller */
        
        'RewardSetup'=>array(
            'alias'=>'Setup Điểm Thưởng Cộng Thêm - rewardSetup',
            'actions'=>array(
                'Index'=>array('alias'=>'Index',
                                'childActions'=>array()//những actions mà sẽ được cấp quyền hoặc hủy theo action cha. EX 'childActions'=>array('Approve','Reject')
                                ),
                'Create'=>array('alias'=>'Tạo mới',
                                'childActions'=>array()
                                ),
                'Update'=>array('alias'=>'Cập nhật',
                                'childActions'=>array()//những actions mà sẽ được cấp quyền hoặc hủy theo action cha (Index)
                                ),
                'View'=>array('alias'=>'Xem',
                                'childActions'=>array()
                                ),
                'Delete'=>array('alias'=>'Xóa',
                                'childActions'=>array()
                                ),
                )
        ),/* end controller */
        
        'Announce'=>array(
            'alias'=>'Đại lý đánh chương trình - Announce',
            'actions'=>array(
                'Index'=>array('alias'=>'Index',
                                'childActions'=>array()//những actions mà sẽ được cấp quyền hoặc hủy theo action cha. EX 'childActions'=>array('Approve','Reject')
                                ),
                'Create'=>array('alias'=>'Tạo mới',
                                'childActions'=>array()
                                ),
                'Update'=>array('alias'=>'Cập nhật',
                                'childActions'=>array()//những actions mà sẽ được cấp quyền hoặc hủy theo action cha (Index)
                                ),
                'View'=>array('alias'=>'Xem',
                                'childActions'=>array()
                                ),
                'Delete'=>array('alias'=>'Xóa',
                                'childActions'=>array()
                                ),
                'Approve'=>array('alias'=>'Duyệt',
                                'childActions'=>array()
                                ),
                )
        ),/* end controller */
        
        'DebitVo'=>array(
            'alias'=>'Hạn mức nợ vỏ khách hàng mối - DebitVo',
            'actions'=>array(
                'Index'=>array('alias'=>'Index',
                                'childActions'=>array()//những actions mà sẽ được cấp quyền hoặc hủy theo action cha. EX 'childActions'=>array('Approve','Reject')
                                ),
                'Create'=>array('alias'=>'Tạo mới',
                                'childActions'=>array()
                                ),
                'Update'=>array('alias'=>'Cập nhật',
                                'childActions'=>array()//những actions mà sẽ được cấp quyền hoặc hủy theo action cha (Index)
                                ),
                'View'=>array('alias'=>'Xem',
                                'childActions'=>array()
                                ),
                'Delete'=>array('alias'=>'Xóa',
                                'childActions'=>array()
                                ),
                )
        ),/* end controller */ 
        'GasEventMarket'=>array(
            'alias'=>'Đi chợ - GasEventMarket',
            'actions'=>array(
                'Index'=>array('alias'=>'Index',
                                'childActions'=>array()//những actions mà sẽ được cấp quyền hoặc hủy theo action cha. EX 'childActions'=>array('Approve','Reject')
                                ),
                'Create'=>array('alias'=>'Tạo mới',
                                'childActions'=>array()
                                ),
                'Update'=>array('alias'=>'Cập nhật',
                                'childActions'=>array()//những actions mà sẽ được cấp quyền hoặc hủy theo action cha (Index)
                                ),
                'View'=>array('alias'=>'Xem',
                                'childActions'=>array()
                                ),
                'Delete'=>array('alias'=>'Xóa',
                                'childActions'=>array()
                                ),
                'ReportMarket'=>array('alias'=>'Báo cáo đi chợ - ReportMarket',
                                'childActions'=>array()
                                ),
                'ViewRoadShow'=>array('alias'=>'Xem chi tiết roadshow - ViewRoadShow',
                                'childActions'=>array()
                                ),
                'CreateDetailMaterial'=>array('alias'=>'Nhập/Xuất vật tư - CreateDetailMaterial',
                                'childActions'=>array()
                                ),
                'PayMoney'=>array('alias'=>'Xác nhận đã chi tiền - PayMoney',
                                'childActions'=>array()
                                ),
                'ReloadEvent'=>array('alias'=>'Cập nhật thông tin tải app - ReloadEvent',
                                'childActions'=>array()
                                ),
                'ReportCostEvent'=>array('alias'=>'BC Activation - ReportCostEvent',
                                'childActions'=>array()
                                ),
                )
        ),/* end controller */ 
        'HrApprovedReports'=>array(
            'alias'=>'Duyệt bảng lương - HrApprovedReports',
            'actions'=>array(
                'Index'=>array('alias'=>'Index',
                                'childActions'=>array()//những actions mà sẽ được cấp quyền hoặc hủy theo action cha. EX 'childActions'=>array('Approve','Reject')
                                ),
                'Update'=>array('alias'=>'Cập nhật',
                                'childActions'=>array()//những actions mà sẽ được cấp quyền hoặc hủy theo action cha (Index)
                                ),
                'View'=>array('alias'=>'Xem',
                                'childActions'=>array()
                                ),
                'Delete'=>array('alias'=>'Xóa',
                                'childActions'=>array()
                                ),
                'Approve'=>array('alias'=>'Duyệt',
                                'childActions'=>array()
                                ),
                'ExportExcel'=>array('alias'=>'Xuất Excel',
                                'childActions'=>array()
                                ),
                )
        ),/* end controller */ 
        'Gasreports'=>array(
            'alias'=>'Báo Cáo - gasreports',
            'actions'=>array(
                'Revenue_output'=>array('alias'=>'Xem báo cáo doanh thu - sản lượng - Revenue_output',
                                'childActions'=>array()//những actions mà sẽ được cấp quyền hoặc hủy theo action cha. 
                                //EX 'childActions'=>array('Approve','Reject')
                                ),
                'Output_daily'=>array('alias'=>'Xem báo cáo daily sản lượng - Output_daily',
                                'childActions'=>array()//những actions mà sẽ được cấp quyền hoặc hủy theo action cha. EX 'childActions'=>array('Approve','Reject')
                                ),
                'Target'=>array('alias'=>'Xem báo cáo target - target',
                                'childActions'=>array()
                                ),
                'CustomerFrequently'=>array('alias'=>'Thống Kê Khách Hàng Thường Xuyên',
                                'childActions'=>array()
                                ),
                'DetermineGasGoBack'=>array('alias'=>'Xác định bình quay về của PTTT',
                                'childActions'=>array()
                                ),
                'DailyInternal'=>array('alias'=>'Daily Nội Bộ',
                                'childActions'=>array()
                                ),
                'Inventory'=>array('alias'=>'Tồn Kho Toàn Hệ Thống',
                                'childActions'=>array()
                                ),
                'Output_customer'=>array('alias'=>'Sản lượng khách hàng',
                                'childActions'=>array()
                                ),
                'Pttt_daily_goback'=>array('alias'=>'PTTT Bình Quay Về Hàng Ngày',
                                'childActions'=>array()
                                ),
                'Target_monitor_pttt'=>array('alias'=>'Báo cáo chỉ tiêu của chuyên viên PTTT - gasreports/target_monitor_pttt',
                                'childActions'=>array()
                                ),
                'Customer_buy'=>array('alias'=>'Thống kê khách hàng không lấy hàng - gasreports/Customer_buy',
                                'childActions'=>array()
                                ),
                'Target_sale_cvkv'=>array('alias'=>'Target CCS - gasreports/Target_sale_cvkv',
                                'childActions'=>array()
                                ),
                'Output_change'=>array('alias'=>'Báo cáo sản lượng tăng giảm theo tháng - gasreports/Output_change',
                                'childActions'=>array()
                                ),
                'Update_customer_not_buy'=>array('alias'=>'Ajax update move Khách hàng sang list không lấy hàng',
                                'childActions'=>array()
                                ),
                'OutputAgent'=>array('alias'=>'Xuất Excel OutputAgent Admin Only',
                                'childActions'=>array()
                                ),
                'TargetNew'=>array('alias'=>'Target New - gasreports/targetNew',
                                'childActions'=>array()
                                ),
                'InventoryVo'=>array('alias'=>'Tồn kho vỏ - gasreports/inventoryVo',
                                'childActions'=>array()
                                ),
                'OutputCar'=>array('alias'=>'Báo cáo sản lượng xe - gasreports/outputCar',
                                'childActions'=>array()
                                ),
                'CallCenterOutput'=>array('alias'=>'Bác cáo sản lượng điếu phối - gasreports/callCenterOutput',
                                'childActions'=>array()
                                ),
                'Business'=>array('alias'=>'Báo cáo kết quả kinh doanh - gasreports/business',
                                'childActions'=>array()
                                ),
                'EmployeeMaintain'=>array('alias'=>'Báo cáo công NV giao nhận - gasreports/employeeMaintain',
                                'childActions'=>array()
                                ),
                'HgdSource'=>array('alias'=>'Sản lượng khách hàng HGĐ mới - cũ - gasreports/HgdSource',
                                'childActions'=>array()
                                ),
                'ExcelOutputDaily'=>array('alias'=>'Excel daily sản lượng - gasreports/excelOutputDaily',
                                'childActions'=>array()
                                ),
                )
        ),/* end controller */
        
        'TruckScales'=>array( 
            'alias'=>'Cân xe tải - TruckScales',
            'actions'=>array(
                'Index'=>array('alias'=>'Index',
                                'childActions'=>array()//những actions mà sẽ được cấp quyền hoặc hủy theo action cha. EX 'childActions'=>array('Approve','Reject')
                                ),
                'View'=>array('alias'=>'Xem',
                                'childActions'=>array()
                                ),
                'Delete'=>array('alias'=>'Xóa',
                                'childActions'=>array()
                                ),
                )
        ),/* end controller */ 
        
        'TargetDaily'=>array(
            'alias'=>'List TargetDaily - TargetDaily',
            'actions'=>array(
                'Index'=>array('alias'=>'Daily Bình Quay Về - Index',
                                'childActions'=>array()//những actions mà sẽ được cấp quyền hoặc hủy theo action cha. EX 'childActions'=>array('Approve','Reject')
                                ),
                'View'=>array('alias'=>'Xem',
                                'childActions'=>array()
                                ),
                'RemoveV1'=>array('alias'=>'Xóa BQV 1',
                                'childActions'=>array()
                                ),
                'RemoveV2'=>array('alias'=>'Xóa BQV 2',
                                'childActions'=>array()
                                ),
                )
        ),/* end controller */ 
        
        'Funds'=>array(
            'alias'=>'Setup giá vốn - Funds',
            'actions'=>array(
                'Index'=>array('alias'=>'Index',
                                'childActions'=>array()//những actions mà sẽ được cấp quyền hoặc hủy theo action cha. EX 'childActions'=>array('Approve','Reject')
                                ),
                'Create'=>array('alias'=>'Tạo mới',
                                'childActions'=>array()
                                ),
                'Update'=>array('alias'=>'Cập nhật',
                                'childActions'=>array()//những actions mà sẽ được cấp quyền hoặc hủy theo action cha (Index)
                                ),
                'View'=>array('alias'=>'Xem',
                                'childActions'=>array()
                                ),
                'Delete'=>array('alias'=>'Xóa',
                                'childActions'=>array()
                                ),
                'ExportExcel'=>array('alias'=>'Xuất Excel',
                                'childActions'=>array()
                                ),
                )
        ),/* end controller */ 
        
        'Forecast'=>array(
            'alias'=>'Dự báo lượng gas - Forecast',
            'actions'=>array(
                'UpdateForecast'=>array('alias'=>'UpdateForecast',
                                'childActions'=>array()//những actions mà sẽ được cấp quyền hoặc hủy theo action cha. EX 'childActions'=>array('Approve','Reject')
                                ),
                'Index'=>array('alias'=>'Index',
                                'childActions'=>array()//những actions mà sẽ được cấp quyền hoặc hủy theo action cha. EX 'childActions'=>array('Approve','Reject')
                                ),
                'View'=>array('alias'=>'View',
                                'childActions'=>array()//những actions mà sẽ được cấp quyền hoặc hủy theo action cha. EX 'childActions'=>array('Approve','Reject')
                                ),
                'SendNotify'=>array('alias'=>' - SendNotify',
                                'childActions'=>array()
                                ),
                'SendNotifyRemain'=>array('alias'=>' SendNotifyRemain',
                                'childActions'=>array()//những actions mà sẽ được cấp quyền hoặc hủy theo action cha (Index)
                                ),
                )
        ),/* end controller */ 
        
        'PriceSupport'=>array(
            'alias'=>'Setup Hỗ trợ giao khó - PriceSupport',
            'actions'=>array(
                'Index'=>array('alias'=>'Index',
                                'childActions'=>array()//những actions mà sẽ được cấp quyền hoặc hủy theo action cha. EX 'childActions'=>array('Approve','Reject')
                                ),
                'Create'=>array('alias'=>'Tạo mới',
                                'childActions'=>array()
                                ),
                'Update'=>array('alias'=>'Cập nhật',
                                'childActions'=>array()//những actions mà sẽ được cấp quyền hoặc hủy theo action cha (Index)
                                ),
                'View'=>array('alias'=>'Xem',
                                'childActions'=>array()
                                ),
                'Delete'=>array('alias'=>'Xóa',
                                'childActions'=>array()
                                ),
                )
        ),/* end controller */ 
        
        'ReportCustomer'=>array(
            'alias'=>'ReportCustomer - ReportCustomer',
            'actions'=>array(
                'ReportHardDelivery'=>array('alias'=>'Hỗ trợ giao khó - ReportHardDelivery',
                                'childActions'=>array()
                                ),
                'CodePttt'=>array('alias'=>'BC So sánh time code PTTT + BQV - CodePttt',
                                'childActions'=>array()
                                ),
                'ReportProduction'=>array('alias'=>'BC sản lượng khách hàng',
                                'childActions'=>array()
                                ),
                
                )
        ),/* end controller */
        
        'CodePartner'=>array(
            'alias'=>'codePartner - codePartner',
            'actions'=>array(
                'Index'=>array('alias'=>'Danh sách code - Index',
                                'childActions'=>array()//những actions mà sẽ được cấp quyền hoặc hủy theo action cha. EX 'childActions'=>array('Approve','Reject')
                                ),
                'Report'=>array('alias'=>'Report',
                                'childActions'=>array()//những actions mà sẽ được cấp quyền hoặc hủy theo action cha (Index)
                                ),
                )
        ),/* end controller */
        
        'ApiRequestLogs'=>array(
            'alias'=>'App Api Log request  - ApiRequestLogs',
            'actions'=>array(
                'Index'=>array('alias'=>'Index',
                                'childActions'=>array()//những actions mà sẽ được cấp quyền hoặc hủy theo action cha. EX 'childActions'=>array('Approve','Reject')
                                ),
                'Create'=>array('alias'=>'Clean Log',
                                'childActions'=>array()
                                ),
                )
        ),/* end controller */
        
        'AppLoginLock'=>array(
            'alias'=>'AppLoginLock - AppLoginLock',
            'actions'=>array(
                'Index'=>array('alias'=>'Index',
                                'childActions'=>array()//những actions mà sẽ được cấp quyền hoặc hủy theo action cha. EX 'childActions'=>array('Approve','Reject')
                                ),
                'Update'=>array('alias'=>'Cập nhật',
                                'childActions'=>array()//những actions mà sẽ được cấp quyền hoặc hủy theo action cha (Index)
                                ),
                'Delete'=>array('alias'=>'Xóa',
                                'childActions'=>array()
                                ),
                )
        ),/* end controller */
        
        'SetupTeam'=>array(
            'alias'=>'Setup đại lý KTKV - SetupTeam',
            'actions'=>array(
                'Index'=>array('alias'=>'Index',
                                'childActions'=>array()//những actions mà sẽ được cấp quyền hoặc hủy theo action cha. EX 'childActions'=>array('Approve','Reject')
                                ),
                'Create'=>array('alias'=>'Tạo mới',
                                'childActions'=>array()
                                ),
                'Update'=>array('alias'=>'Cập nhật',
                                'childActions'=>array()//những actions mà sẽ được cấp quyền hoặc hủy theo action cha (Index)
                                ),
                'View'=>array('alias'=>'Xem',
                                'childActions'=>array()
                                ),
                'Delete'=>array('alias'=>'Xóa',
                                'childActions'=>array()
                                ),
                )
        ),/* end controller */
        
        'ContractManagement'=>array(
            'alias'=>'Hợp đồng KH bò mối - ContractManagement',
            'actions'=>array(
                'Index'=>array('alias'=>'Index',
                                'childActions'=>array()//những actions mà sẽ được cấp quyền hoặc hủy theo action cha. EX 'childActions'=>array('Approve','Reject')
                                ),
                'Create'=>array('alias'=>'Tạo mới',
                                'childActions'=>array('ToPdf')
                                ),
                'Update'=>array('alias'=>'Cập nhật',
                                'childActions'=>array()//những actions mà sẽ được cấp quyền hoặc hủy theo action cha (Index)
                                ),
                'View'=>array('alias'=>'Xem',
                                'childActions'=>array()
                                ),
                'Delete'=>array('alias'=>'Xóa',
                                'childActions'=>array()
                                ),
                )
        ),/* end controller */
        
        'Modules'=>array(
            'alias'=>'Modules vật tư - Modules',
            'actions'=>array(
                'Index'=>array('alias'=>'Index',
                                'childActions'=>array('')//những actions mà sẽ được cấp quyền hoặc hủy theo action cha. EX 'childActions'=>array('Approve','Reject')
                                ),
                'Create'=>array('alias'=>'Tạo mới',
                                'childActions'=>array()
                                ),
                'Update'=>array('alias'=>'Cập nhật',
                                'childActions'=>array()//những actions mà sẽ được cấp quyền hoặc hủy theo action cha (Index)
                                ),
                'View'=>array('alias'=>'Xem',
                                'childActions'=>array()
                                ),
                'Delete'=>array('alias'=>'Xóa',
                                'childActions'=>array()
                                ),
                )
        ),/* end controller */

        'GasTextPersonResponsibles'=>array(
            'alias'=>'Quyết định thưởng phạt - GasTextPersonResponsibles',
            'actions'=>array(
                'Index'=>array('alias'=>'Index',
                                'childActions'=>array('')//những actions mà sẽ được cấp quyền hoặc hủy theo action cha. EX 'childActions'=>array('Approve','Reject')
                                ),
                'Create'=>array('alias'=>'Tạo mới',
                                'childActions'=>array()
                                ),
                'Update'=>array('alias'=>'Cập nhật',
                                'childActions'=>array()//những actions mà sẽ được cấp quyền hoặc hủy theo action cha (Index)
                                ),
                'View'=>array('alias'=>'Xem',
                                'childActions'=>array()
                                ),
                'Delete'=>array('alias'=>'Xóa',
                                'childActions'=>array()
                                ),
                )
        ),/* end controller */
        
        'GasDebts'=>array(
            'alias'=>'Quản lý Công nợ - GasDebts',
            'actions'=>array(
                'Index'=>array('alias'=>'Index',
                                'childActions'=>array('')//những actions mà sẽ được cấp quyền hoặc hủy theo action cha. EX 'childActions'=>array('Approve','Reject')
                                ),
                'Create'=>array('alias'=>'Tạo mới',
                                'childActions'=>array()
                                ),
                'Update'=>array('alias'=>'Cập nhật',
                                'childActions'=>array()//những actions mà sẽ được cấp quyền hoặc hủy theo action cha (Index)
                                ),
                'View'=>array('alias'=>'Xem',
                                'childActions'=>array()
                                ),
                'Delete'=>array('alias'=>'Xóa',
                                'childActions'=>array()
                                ),
                'ReportDebtsGeneral'=>array('alias'=>'Báo cáo công nợ nhân viên - ReportDebtsGeneral',
                                'childActions'=>array()
                                ),
                'ReportDebtsDetail'=>array('alias'=>'Báo cáo chi tiết công nợ nhân viên - ReportDebtsDetail',
                                'childActions'=>array()
                                ),
                )
        ),/* end controller */ 
        
        'CallReview'=>array(
            'alias'=>'Test call - CallReview',
            'actions'=>array(
                'ListDetail'=>array('alias'=>'List cuộc gọi đã review - ListDetail',
                                'childActions'=>array('')//những actions mà sẽ được cấp quyền hoặc hủy theo action cha. EX 'childActions'=>array('Approve','Reject')
                                ),
                'ListCall'=>array('alias'=>'Nhận cuộc gọi - ListCall',
                                'childActions'=>array()
                                ),
                'Create'=>array('alias'=>'Create - Tạo mới review',
                                'childActions'=>array()
                                ),
                'ReportCallReview'=>array('alias'=>'ReportCallReview - Báo cáo kiểm tra cuộc gọi',
                                'childActions'=>array()
                                ),
                'Delete'=>array('alias'=>'Xóa',
                                'childActions'=>array()
                                ),
                )
        ),/* end controller */
        
        'EmployeesImages'=>array(
            'alias'=>'File hồ sơ KH - nhân viên - EmployeesImages',
            'actions'=>array(
                'Index'=>array('alias'=>'Index',
                                'childActions'=>array('')//những actions mà sẽ được cấp quyền hoặc hủy theo action cha. EX 'childActions'=>array('Approve','Reject')
                                ),
                'View'=>array('alias'=>'Xem',
                                'childActions'=>array()
                                ),
                'Delete'=>array('alias'=>'Xóa',
                                'childActions'=>array()
                                ),
                )
        ),/* end controller */
        
        'GasSupportCustomerRequest'=>array(
            'alias'=>'KH bò mối Yêu cầu vật tư - GasSupportCustomerRequest',
            'actions'=>array(
                'Index'=>array('alias'=>'Index',
                                'childActions'=>array('Generate')//những actions mà sẽ được cấp quyền hoặc hủy theo action cha. EX 'childActions'=>array('Approve','Reject')
                                ),
                'Create'=>array('alias'=>'Tạo mới',
                                'childActions'=>array()
                                ),
                'Update'=>array('alias'=>'Cập nhật',
                                'childActions'=>array()//những actions mà sẽ được cấp quyền hoặc hủy theo action cha (Index)
                                ),
                'View'=>array('alias'=>'Xem',
                                'childActions'=>array()
                                ),
                'Delete'=>array('alias'=>'Xóa',
                                'childActions'=>array()
                                ),
                )
        ),/* end controller */
        
        'UsersEmail'=>array(
            'alias'=>' Email khách hàng - UsersEmail',
            'actions'=>array(
                'Index'=>array('alias'=>'Index',
                                'childActions'=>array('')//những actions mà sẽ được cấp quyền hoặc hủy theo action cha. EX 'childActions'=>array('Approve','Reject')
                                ),
                'Create'=>array('alias'=>'Tạo mới',
                                'childActions'=>array()
                                ),
                'Update'=>array('alias'=>'Cập nhật',
                                'childActions'=>array()//những actions mà sẽ được cấp quyền hoặc hủy theo action cha (Index)
                                ),
                'View'=>array('alias'=>'Xem',
                                'childActions'=>array()
                                ),
                'FileSendIndex'=>array('alias'=>'Bảng kê đã gửi - FileSendIndex',
                                'childActions'=>array()
                                ),
                'Delete'=>array('alias'=>'Xóa',
                                'childActions'=>array()
                                ),
                'FileSendView'=>array('alias'=>'Xem bảng kê đã gửi - FileSendView',
                                'childActions'=>array()
                                ),
                'DobList'=>array('alias'=>'Sinh nhật khách hàng - DobList',
                                'childActions'=>array()
                                ),
                'DobAdd'=>array('alias'=>'Tạo mới sinh nhật khách hàng - DobAdd',
                                'childActions'=>array()
                                ),
                'DobUpdate'=>array('alias'=>'Cập nhật sinh nhật khách hàng - DobUpdate',
                                'childActions'=>array()
                                ),
                )
        ),/* end controller */
        
        'GasScheduleNotifyHistory'=>array(
            'alias'=>'List notify app - GasScheduleNotifyHistory',
            'actions'=>array(
                'Report'=>array('alias'=>'Báo cáo Thống kê SMS - report',
                                'childActions'=>array()//những actions mà sẽ được cấp quyền hoặc hủy theo action cha. EX 'childActions'=>array('Approve','Reject')
                                ),
                'Index'=>array('alias'=>'Index',
                                'childActions'=>array()//những actions mà sẽ được cấp quyền hoặc hủy theo action cha. EX 'childActions'=>array('Approve','Reject')
                                ),
                'View'=>array('alias'=>'Xem',
                                'childActions'=>array()
                                ),
                )
        ),/* end controller */
        
        'HrCoefficients'=>array(
            'alias'=>'Hệ số - HrCoefficients',
            'actions'=>array(
                'Index'=>array('alias'=>'Index',
                                'childActions'=>array('UpdateAjax')//những actions mà sẽ được cấp quyền hoặc hủy theo action cha. EX 'childActions'=>array('Approve','Reject')
                                ),
                'Create'=>array('alias'=>'Tạo mới',
                                'childActions'=>array()
                                ),
                'Update'=>array('alias'=>'Cập nhật',
                                'childActions'=>array()//những actions mà sẽ được cấp quyền hoặc hủy theo action cha (Index)
                                ),
                'View'=>array('alias'=>'Xem',
                                'childActions'=>array()
                                ),
                'Delete'=>array('alias'=>'Xóa',
                                'childActions'=>array()
                                ),
                )
        ),/* end controller */
        
        'HrParameters'=>array(
            'alias'=>'Tham số - HrParameters',
            'actions'=>array(
                'Index'=>array('alias'=>'Index',
                                'childActions'=>array('UpdateAjax')//những actions mà sẽ được cấp quyền hoặc hủy theo action cha. EX 'childActions'=>array('Approve','Reject')
                                ),
                'Create'=>array('alias'=>'Tạo mới',
                                'childActions'=>array()
                                ),
                'Update'=>array('alias'=>'Cập nhật',
                                'childActions'=>array()//những actions mà sẽ được cấp quyền hoặc hủy theo action cha (Index)
                                ),
                'View'=>array('alias'=>'Xem',
                                'childActions'=>array()
                                ),
                'Delete'=>array('alias'=>'Xóa',
                                'childActions'=>array()
                                ),
                )
        ),/* end controller */
        
        'HrPlan'=>array(
            'alias'=>'Kế hoạch nhân sự - HrPlan',
            'actions'=>array(
                'Index'=>array('alias'=>'Index',
                                'childActions'=>array()//những actions mà sẽ được cấp quyền hoặc hủy theo action cha. EX 'childActions'=>array('Approve','Reject')
                                ),
                'Create'=>array('alias'=>'Tạo mới',
                                'childActions'=>array()
                                ),
                'Update'=>array('alias'=>'Cập nhật',
                                'childActions'=>array()//những actions mà sẽ được cấp quyền hoặc hủy theo action cha (Index)
                                ),
                'View'=>array('alias'=>'Xem',
                                'childActions'=>array()
                                ),
                'UpdateStatus'=>array('alias'=>'Cập nhật trạng thái',
                                'childActions'=>array()
                                ),
                'Delete'=>array('alias'=>'Xóa',
                                'childActions'=>array()
                                ),
                )
        ),/* end controller */
        
        'HrInterviewPlan'=>array(
            'alias'=>'Kế hoạch phỏng vấn - HrInterviewPlan',
            'actions'=>array(
                'Index'=>array('alias'=>'Index',
                                'childActions'=>array()//những actions mà sẽ được cấp quyền hoặc hủy theo action cha. EX 'childActions'=>array('Approve','Reject')
                                ),
                'Create'=>array('alias'=>'Tạo mới',
                                'childActions'=>array()
                                ),
                'Update'=>array('alias'=>'Cập nhật',
                                'childActions'=>array()//những actions mà sẽ được cấp quyền hoặc hủy theo action cha (Index)
                                ),
                'View'=>array('alias'=>'Xem',
                                'childActions'=>array()
                                ),
                'Confirm'=>array('alias'=>'Xác nhận lịch phỏng vấn',
                                'childActions'=>array()
                                ),
                'Delete'=>array('alias'=>'Xóa',
                                'childActions'=>array()
                                ),
                )
        ),/* end controller */
        
        'HrInterviewPlanDetail'=>array(
            'alias'=>'Chi tiết kế hoạch phỏng vấn - HrInterviewPlanDetail',
            'actions'=>array(
                'Index'=>array('alias'=>'Index',
                                'childActions'=>array()//những actions mà sẽ được cấp quyền hoặc hủy theo action cha. EX 'childActions'=>array('Approve','Reject')
                                ),
                'Create'=>array('alias'=>'Tạo mới',
                                'childActions'=>array()
                                ),
                'Update'=>array('alias'=>'Cập nhật',
                                'childActions'=>array()//những actions mà sẽ được cấp quyền hoặc hủy theo action cha (Index)
                                ),
                'View'=>array('alias'=>'Xem',
                                'childActions'=>array()
                                ),
                'Delete'=>array('alias'=>'Xóa',
                                'childActions'=>array()
                                ),
                )
        ),/* end controller */
        
        'HrWorkPlan'=>array(
            'alias'=>'Kế hoạch làm việc - HrWorkPlan',
            'actions'=>array(
                'Index'=>array('alias'=>'Index',
                                'childActions'=>array()//những actions mà sẽ được cấp quyền hoặc hủy theo action cha. EX 'childActions'=>array('Approve','Reject')
                                ),
                'Create'=>array('alias'=>'Tạo mới',
                                'childActions'=>array()
                                ),
                'Update'=>array('alias'=>'Cập nhật',
                                'childActions'=>array()//những actions mà sẽ được cấp quyền hoặc hủy theo action cha (Index)
                                ),
                'View'=>array('alias'=>'Xem',
                                'childActions'=>array()
                                ),
                'Delete'=>array('alias'=>'Xóa',
                                'childActions'=>array()
                                ),
                )
        ),/* end controller */
        
        'HrWorkShift'=>array(
            'alias'=>'Ca làm việc - HrWorkShift',
            'actions'=>array(
                'Index'=>array('alias'=>'Index',
                                'childActions'=>array()//những actions mà sẽ được cấp quyền hoặc hủy theo action cha. EX 'childActions'=>array('Approve','Reject')
                                ),
                'Create'=>array('alias'=>'Tạo mới',
                                'childActions'=>array()
                                ),
                'Update'=>array('alias'=>'Cập nhật',
                                'childActions'=>array()//những actions mà sẽ được cấp quyền hoặc hủy theo action cha (Index)
                                ),
                'View'=>array('alias'=>'Xem',
                                'childActions'=>array()
                                ),
                'Delete'=>array('alias'=>'Xóa',
                                'childActions'=>array()
                                ),
                )
        ),/* end controller */
        
        'HrWorkSchedule'=>array(
            'alias'=>'Lịch làm việc - HrWorkSchedule',
            'actions'=>array(
                'Index'=>array('alias'=>'Index',
                                'childActions'=>array()//những actions mà sẽ được cấp quyền hoặc hủy theo action cha. EX 'childActions'=>array('Approve','Reject')
                                ),
                'Create'=>array('alias'=>'Tạo mới',
                                'childActions'=>array()
                                ),
                'Update'=>array('alias'=>'Cập nhật',
                                'childActions'=>array()//những actions mà sẽ được cấp quyền hoặc hủy theo action cha (Index)
                                ),
                'View'=>array('alias'=>'Xem',
                                'childActions'=>array()
                                ),
                'WorkScheduleUI'=>array('alias'=>'View + Tạo lịch làm việc - WorkScheduleUI',
                                'childActions'=>array('CreateWorkShift', 'ListWorkPlan', 'UpdateAjax')
                                ),
                'Delete'=>array('alias'=>'Xóa',
                                'childActions'=>array()
                                ),
                )
        ),/* end controller */
        
        'HrFunctions'=>array(
            'alias'=>'Công thức - HrFunctions',
            'actions'=>array(
                'Index'=>array('alias'=>'Index',
                                'childActions'=>array()//những actions mà sẽ được cấp quyền hoặc hủy theo action cha. EX 'childActions'=>array('Approve','Reject')
                                ),
                'Create'=>array('alias'=>'Tạo mới',
                                'childActions'=>array()
                                ),
                'Update'=>array('alias'=>'Cập nhật',
                                'childActions'=>array()//những actions mà sẽ được cấp quyền hoặc hủy theo action cha (Index)
                                ),
                'View'=>array('alias'=>'Xem',
                                'childActions'=>array()
                                ),
                'Setting'=>array('alias'=>'Danh sách công thức',
                                'childActions'=>array('UpdateAjax')
                                ),
                'FrontCreate'=>array('alias'=>'Tạo công thức',
                                'childActions'=>array('AjaxReloadCoef')
                                ),
                'Delete'=>array('alias'=>'Xóa',
                                'childActions'=>array()
                                ),
                )
        ),/* end controller */
        
        'HrFunctionTypes'=>array(
            'alias'=>'Loại công thức - HrFunctionTypes',
            'actions'=>array(
                'Index'=>array('alias'=>'Index',
                                'childActions'=>array()//những actions mà sẽ được cấp quyền hoặc hủy theo action cha. EX 'childActions'=>array('Approve','Reject')
                                ),
                'Create'=>array('alias'=>'Tạo mới',
                                'childActions'=>array()
                                ),
                'Update'=>array('alias'=>'Cập nhật',
                                'childActions'=>array()//những actions mà sẽ được cấp quyền hoặc hủy theo action cha (Index)
                                ),
                'View'=>array('alias'=>'Xem',
                                'childActions'=>array()
                                ),
                'Delete'=>array('alias'=>'Xóa',
                                'childActions'=>array()
                                ),
                )
        ),/* end controller */
        
        'HrHolidayPlans'=>array(
            'alias'=>'Kế hoạch nghỉ lễ - HrHolidayPlans',
            'actions'=>array(
                'Index'=>array('alias'=>'Index',
                                'childActions'=>array()//những actions mà sẽ được cấp quyền hoặc hủy theo action cha. EX 'childActions'=>array('Approve','Reject')
                                ),
                'Create'=>array('alias'=>'Tạo mới',
                                'childActions'=>array()
                                ),
                'Update'=>array('alias'=>'Cập nhật',
                                'childActions'=>array()//những actions mà sẽ được cấp quyền hoặc hủy theo action cha (Index)
                                ),
                'View'=>array('alias'=>'Xem',
                                'childActions'=>array()
                                ),
                'Delete'=>array('alias'=>'Xóa',
                                'childActions'=>array()
                                ),
                )
        ),/* end controller */
        
        'HrHolidayTypes'=>array(
            'alias'=>'Loại ngày nghỉ lễ - HrHolidayTypes',
            'actions'=>array(
                'Index'=>array('alias'=>'Index',
                                'childActions'=>array()//những actions mà sẽ được cấp quyền hoặc hủy theo action cha. EX 'childActions'=>array('Approve','Reject')
                                ),
                'Create'=>array('alias'=>'Tạo mới',
                                'childActions'=>array()
                                ),
                'Update'=>array('alias'=>'Cập nhật',
                                'childActions'=>array()//những actions mà sẽ được cấp quyền hoặc hủy theo action cha (Index)
                                ),
                'View'=>array('alias'=>'Xem',
                                'childActions'=>array()
                                ),
                'Delete'=>array('alias'=>'Xóa',
                                'childActions'=>array()
                                ),
                )
        ),/* end controller */
        
        'HrSalaryReports'=>array(
            'alias'=>'Báo cáo lương - HrSalaryReports',
            'actions'=>array(
                'Index'=>array('alias'=>'Index',
                                'childActions'=>array()//những actions mà sẽ được cấp quyền hoặc hủy theo action cha. EX 'childActions'=>array('Approve','Reject')
                                ),
                'Create'=>array('alias'=>'Tạo mới',
                                'childActions'=>array()
                                ),
                'Update'=>array('alias'=>'Cập nhật',
                                'childActions'=>array()//những actions mà sẽ được cấp quyền hoặc hủy theo action cha (Index)
                                ),
                'View'=>array('alias'=>'Xem',
                                'childActions'=>array()
                                ),
                'ExportExcel'=>array('alias'=>'Xuất Excel',
                                'childActions'=>array()
                                ),
                'SendMailReport'=>array('alias'=>'Yêu cầu duyệt bảng lương',
                                'childActions'=>array()
                                ),
                'ApprovedReport'=>array('alias'=>'Duyệt bảng lương',
                                'childActions'=>array()
                                ),
                'Report'=>array('alias'=>'Danh sách các bảng lương',
                                'childActions'=>array(
                                                    'GetTypeReportContent',
                                                    'UpdateAjax',
                                                    'Calculate',
                                                    'ChangeStatus',
                                                    'GetHtmlListReport',
                                                    'GetListTable',
                                                    'LoadMoreMonth',
                                                    'CalTotalSalary',
                                                    )
                                ),
                'Delete'=>array('alias'=>'Xóa',
                                'childActions'=>array()
                                ),
                )
        ),/* end controller */
        
        'Gas24hReport'=>array(
            'alias'=>'Báo cáo Gas24h - Gas24hReport',
            'actions'=>array(
                'CheckVirtualPhone'=>array('alias'=>' Thống kê số ảo - CheckVirtualPhone',
                                'childActions'=>array()//những actions mà sẽ được cấp quyền hoặc hủy theo action cha. EX 'childActions'=>array('Approve','Reject')
                                ),
                'AgentMonthly'=>array('alias'=>'Báo cáo App / Call - AgentMonthly',
                                'childActions'=>array()//những actions mà sẽ được cấp quyền hoặc hủy theo action cha. EX 'childActions'=>array('Approve','Reject')
                                ),
                'ReportApp'=>array('alias'=>'Báo cáo App monthly - ReportApp',
                                'childActions'=>array()//những actions mà sẽ được cấp quyền hoặc hủy theo action cha. EX 'childActions'=>array('Approve','Reject')
                                ),
                'ReportTeleSale'=>array('alias'=>'BQV đại lý - reportTeleSale',
                                'childActions'=>array()//những actions mà sẽ được cấp quyền hoặc hủy theo action cha. EX 'childActions'=>array('Approve','Reject')
                                ),
                )
        ),/* end controller */
        
        'CustomerDraft'=>array(
            'alias'=>'KH Excel - CustomerDraft',
            'actions'=>array(
                'Index'=>array('alias'=>'Index',
                                'childActions'=>array()//những actions mà sẽ được cấp quyền hoặc hủy theo action cha. EX 'childActions'=>array('Approve','Reject')
                                ),
                'Create'=>array('alias'=>'Tạo mới',
                                'childActions'=>array()
                                ),
                'Delete'=>array('alias'=>'Xóa',
                                'childActions'=>array()
                                ),
                'ReportDraft'=>array('alias'=>'KH Excel báo cáo cuộc gọi - reportDraft',
                                'childActions'=>array()
                                ),
                'ReportOwnerDraft'=>array('alias'=>'KH Excel báo cáo nguồn data - ReportOwnerDraft',
                                'childActions'=>array()
                                ),
                )
        ),/* end controller */
        
        'CustomerDraftDetail'=>array(
            'alias'=>'KH Excel Daily Call Out - CustomerDraftDetail',
            'actions'=>array(
                'Index'=>array('alias'=>'Index',
                                'childActions'=>array()//những actions mà sẽ được cấp quyền hoặc hủy theo action cha. EX 'childActions'=>array('Approve','Reject')
                                ),
                'Create'=>array('alias'=>'Tạo mới',
                                'childActions'=>array()
                                ),
                'Update'=>array('alias'=>'Cập nhật',
                                'childActions'=>array()//những actions mà sẽ được cấp quyền hoặc hủy theo action cha (Index)
                                ),
                'View'=>array('alias'=>'Xem',
                                'childActions'=>array('')
                                ),
                'Delete'=>array('alias'=>'Xóa',
                                'childActions'=>array()
                                ),
                )
        ),/* end controller */
        
        'EmployeeProblems'=>array(
            'alias'=>'Lỗi Phạt Phản Ánh Sự Việc - EmployeeProblems Issueticket',
            'actions'=>array(
                'UpdateStatus'=>array('alias'=>'Update status hủy lỗi - UpdateStatus',
                                'childActions'=>array()
                                ),
                'StatisticsMoney'=>array('alias'=>'BC Tiền phạt nhân viên - StatisticsMoney',
                                'childActions'=>array()//những actions mà sẽ được cấp quyền hoặc hủy theo action cha. EX 'childActions'=>array('Approve','Reject')
                                ),
                'Index'=>array('alias'=>'Index',
                                'childActions'=>array()
                                ),
                'View'=>array('alias'=>'Xem',
                                'childActions'=>array('ViewMultiNote')
                                ),
                'ReportFaultType'=>array('alias'=>'Lỗi phạt HGD - reportFaultType',
                                'childActions'=>array('')
                                ),
                'Delete'=>array('alias'=>'Xóa',
                                'childActions'=>array()
                                ),
                )
        ),/* end controller */
        
        'Telesale'=>array(
            'alias'=>'List Khách hàng Telesale- Telesale',
            'actions'=>array(
                'Index'=>array('alias'=>'List Khách hàng Telesale - index',
                                'childActions'=>array()//những actions mà sẽ được cấp quyền hoặc hủy theo action cha. EX 'childActions'=>array('Approve','Reject')
                                ),
                'CreateNote'=>array('alias'=>'Tạo ghi chú - CreateNote',
                                'childActions'=>array()//những actions mà sẽ được cấp quyền hoặc hủy theo action cha (Index)
                                ),
                'ReportGasBack'=>array('alias'=>'Telesale bình quay về - reportGasBack',
                                'childActions'=>array()//những actions mà sẽ được cấp quyền hoặc hủy theo action cha (Index)
                                ),
                'ReportCancel'=>array('alias'=>'BC Khách hàng từ chối - ReportCancel',
                                'childActions'=>array()//những actions mà sẽ được cấp quyền hoặc hủy theo action cha (Index)
                                ),
                'InputCode'=>array('alias'=>'Telesale báo cáo cuộc gọi đơn APP - InputCode',
                                'childActions'=>array()//những actions mà sẽ được cấp quyền hoặc hủy theo action cha (Index)
                                ),
                'ReportAgent'=>array('alias'=>'BC Cuộc gọi telesale theo đại lý - reportAgent',
                                'childActions'=>array()//những actions mà sẽ được cấp quyền hoặc hủy theo action cha (Index)
                                ),
                )
        ),/* end controller */
        
        'FollowCustomer'=>array(
            'alias'=>'Daily Telesale- FollowCustomer',
            'actions'=>array(
                'Index'=>array('alias'=>'Daily Telesale - Index',
                                'childActions'=>array()//những actions mà sẽ được cấp quyền hoặc hủy theo action cha. EX 'childActions'=>array('Approve','Reject')
                                ),
                'Create'=>array('alias'=>'Tạo mới',
                                'childActions'=>array()
                                ),
                'Update'=>array('alias'=>'Cập nhật',
                                'childActions'=>array()//những actions mà sẽ được cấp quyền hoặc hủy theo action cha (Index)
                                ),
                'View'=>array('alias'=>'Xem',
                                'childActions'=>array('ViewMultiNote')
                                ),
                'Delete'=>array('alias'=>'Xóa',
                                'childActions'=>array()
                                ),
                )
        ),/* end controller */
        
        'MonitorUpdate'=>array(
            'alias'=>' Chi tiết lỗi PVKH - MonitorUpdate',
            'actions'=>array(
                'Index'=>array('alias'=>'Chi tiết lỗi PVKH - MonitorUpdate',
                                'childActions'=>array()//những actions mà sẽ được cấp quyền hoặc hủy theo action cha. EX 'childActions'=>array('Approve','Reject')
                                ),
                'Delete'=>array('alias'=>'Xóa',
                                'childActions'=>array()
                                ),
                )
        ),/* end controller */
        
        'ReportCall'=>array(
            'alias'=>'Report Call Center - ReportCall',
            'actions'=>array(
                'ByUser'=>array('alias'=>'Báo cáo cuộc gọi theo nhân viên - ByUser',
                                'childActions'=>array()//những actions mà sẽ được cấp quyền hoặc hủy theo action cha. EX 'childActions'=>array('Approve','Reject')
                                ),
                'InHour'=>array('alias'=>'Biểu đồ tần suất cuộc gọi trong ngày - InHour',
                                'childActions'=>array()//những actions mà sẽ được cấp quyền hoặc hủy theo action cha. EX 'childActions'=>array('Approve','Reject')
                                ),
                'ByHour'=>array('alias'=>'Biểu đồ cuộc gọi theo giờ - ByHour',
                                'childActions'=>array()//những actions mà sẽ được cấp quyền hoặc hủy theo action cha. EX 'childActions'=>array('Approve','Reject')
                                ),
                'FailedStatus'=>array('alias'=>'Báo cáo cuộc gọi không thành công - FailedStatus',
                                'childActions'=>array()//những actions mà sẽ được cấp quyền hoặc hủy theo action cha. EX 'childActions'=>array('Approve','Reject')
                                ),
                'SellStatus'=>array('alias'=>'Báo cáo đơn hàng không thành công - SellStatus',
                                'childActions'=>array()//những actions mà sẽ được cấp quyền hoặc hủy theo action cha. EX 'childActions'=>array('Approve','Reject')
                                ),
                'OrderCancel'=>array('alias'=>'BC đơn hàng hủy - OrderCancel',
                                'childActions'=>array()//những actions mà sẽ được cấp quyền hoặc hủy theo action cha. EX 'childActions'=>array('Approve','Reject')
                                ),
                'HoiGas'=>array('alias'=>'BC hối gas - HoiGas',
                                'childActions'=>array()//những actions mà sẽ được cấp quyền hoặc hủy theo action cha. EX 'childActions'=>array('Approve','Reject')
                                ),
                'ReportComplete'=>array('alias'=>'BC thời gian hoàn thành - ReportComplete',
                                'childActions'=>array()//những actions mà sẽ được cấp quyền hoặc hủy theo action cha. EX 'childActions'=>array('Approve','Reject')
                                ),
                'CallComplete'=>array('alias'=>'Đơn hàng hoàn thành - CallComplete',
                                'childActions'=>array()//những actions mà sẽ được cấp quyền hoặc hủy theo action cha. EX 'childActions'=>array('Approve','Reject')
                                ),
                'CommentComplete'=>array('alias'=>'Kiểm tra đơn hàng hoàn thành - CommentComplete',
                                'childActions'=>array()//những actions mà sẽ được cấp quyền hoặc hủy theo action cha. EX 'childActions'=>array('Approve','Reject')
                                ),
                'ReportCallComplete'=>array('alias'=>'BC kiểm tra đơn hoàn thành - ReportCallComplete',
                                'childActions'=>array()//những actions mà sẽ được cấp quyền hoặc hủy theo action cha. EX 'childActions'=>array('Approve','Reject')
                                ),
                )
        ),/* end controller */
        
        'InventoryReport'=>array(
            'alias'=>'Tồn kho vỏ KH bò mối - InventoryReport',
            'actions'=>array(
                'ReportVo'=>array('alias'=>'Tồn kho vỏ KH Không lấy hàng trên 15 ngày - ReportVo',
                                'childActions'=>array()//những actions mà sẽ được cấp quyền hoặc hủy theo action cha. EX 'childActions'=>array('Approve','Reject')
                                ),
                )
        ),/* end controller */
        
        'HomeContractDetail'=>array(
            'alias'=>'Auto gen monthly Hợp đồng thuê nhà - HomeContractDetail',
            'actions'=>array(
                'Index'=>array('alias'=>'Index list monthly Hợp đồng thuê nhà - Index',
                                'childActions'=>array()//những actions mà sẽ được cấp quyền hoặc hủy theo action cha. EX 'childActions'=>array('Approve','Reject')
                                ),
                'Update'=>array('alias'=>'Cập nhật',
                                'childActions'=>array()//những actions mà sẽ được cấp quyền hoặc hủy theo action cha (Index)
                                ),
                'View'=>array('alias'=>'Xem',
                                'childActions'=>array()
                                ),
                'Generate'=>array('alias'=>'Admin Run Generate',
                                'childActions'=>array()
                                ),
                )
        ),/* end controller */
        
        'HomeContract'=>array(
            'alias'=>'Hợp đồng thuê nhà - HomeContract',
            'actions'=>array(
                'Report'=>array('alias'=>'report- report',
                                'childActions'=>array()//những actions mà sẽ được cấp quyền hoặc hủy theo action cha. EX 'childActions'=>array('Approve','Reject')
                                ),
                'Index'=>array('alias'=>'Hợp đồng thuê nhà - Index',
                                'childActions'=>array()//những actions mà sẽ được cấp quyền hoặc hủy theo action cha. EX 'childActions'=>array('Approve','Reject')
                                ),
                'Create'=>array('alias'=>'Tạo mới',
                                'childActions'=>array()
                                ),
                'Update'=>array('alias'=>'Cập nhật',
                                'childActions'=>array()//những actions mà sẽ được cấp quyền hoặc hủy theo action cha (Index)
                                ),
                'View'=>array('alias'=>'Xem',
                                'childActions'=>array()
                                ),
                'Delete'=>array('alias'=>'Xóa',
                                'childActions'=>array()
                                ),
                )
        ),/* end controller */
        
        'UserFixAddress'=>array(
            'alias'=>'Sửa địa chỉ KH - UserFixAddress',
            'actions'=>array(
                'Index'=>array('alias'=>'Sửa địa chỉ KH - UserFixAddress',
                                'childActions'=>array()//những actions mà sẽ được cấp quyền hoặc hủy theo action cha. EX 'childActions'=>array('Approve','Reject')
                                ),
                'Create'=>array('alias'=>'Tạo mới',
                                'childActions'=>array()
                                ),
                'Update'=>array('alias'=>'Cập nhật',
                                'childActions'=>array()//những actions mà sẽ được cấp quyền hoặc hủy theo action cha (Index)
                                ),
                'View'=>array('alias'=>'Xem',
                                'childActions'=>array()
                                ),
                'Delete'=>array('alias'=>'Reject',
                                'childActions'=>array()
                                ),
                )
        ),/* end controller */
        
        'AppPromotion'=>array(
            'alias'=>'Chương trình Khuyến mãi - AppPromotion',
            'actions'=>array(
                'Index'=>array('alias'=>'Chương trình Khuyến mãi - index',
                                'childActions'=>array()//những actions mà sẽ được cấp quyền hoặc hủy theo action cha. EX 'childActions'=>array('Approve','Reject')
                                ),
                'Create'=>array('alias'=>'Tạo mới',
                                'childActions'=>array()
                                ),
                'Update'=>array('alias'=>'Cập nhật',
                                'childActions'=>array()//những actions mà sẽ được cấp quyền hoặc hủy theo action cha (Index)
                                ),
                'View'=>array('alias'=>'Xem',
                                'childActions'=>array()
                                ),
                'Delete'=>array('alias'=>'Reject',
                                'childActions'=>array()
                                ),
                )
            ),/* end controller */
        
        'AppPromotionUser'=>array(
            'alias'=>'Khuyến mãi của khách hàng - AppPromotionUser',
            'actions'=>array(
                'Index'=>array('alias'=>'Khuyến mãi của khách hàng - index',
                                'childActions'=>array()//những actions mà sẽ được cấp quyền hoặc hủy theo action cha. EX 'childActions'=>array('Approve','Reject')
                                ),
                'Create'=>array('alias'=>'Tạo mới',
                                'childActions'=>array()
                                ),
                'Update'=>array('alias'=>'Cập nhật',
                                'childActions'=>array()//những actions mà sẽ được cấp quyền hoặc hủy theo action cha (Index)
                                ),
                'View'=>array('alias'=>'Xem',
                                'childActions'=>array()
                                ),
                'Delete'=>array('alias'=>'Delete',
                                'childActions'=>array()
                                ),
                'ExportExcel'=>array('alias'=>'ExportExcel',
                                'childActions'=>array()
                                ),
                'ReportMarketing'=>array('alias'=>'BC Mã code Telesale - ReportMarketing',
                                'childActions'=>array()
                                ),
                'ReportReferralTracking'=>array('alias'=>'Tree view - ReportReferralTracking',
                                'childActions'=>array()
                                ),
                'ReportReferralTrackingTable'=>array('alias'=>'Table view - ReportReferralTrackingTable',
                                'childActions'=>array()
                                ),
                'ReportCode'=>array('alias'=>'Mã code GĐKV - reportCode',
                                'childActions'=>array()
                                ),
                )
        ),/* end controller */
        
        'Mortgage'=>array(
            'alias'=>'Tài sản thế chấp - Mortgage',
            'actions'=>array(
                'Index'=>array('alias'=>'Tài sản thế chấp - Mortgage',
                                'childActions'=>array()//những actions mà sẽ được cấp quyền hoặc hủy theo action cha. EX 'childActions'=>array('Approve','Reject')
                                ),
                'Create'=>array('alias'=>'Tạo mới',
                                'childActions'=>array()
                                ),
                'Update'=>array('alias'=>'Cập nhật',
                                'childActions'=>array()//những actions mà sẽ được cấp quyền hoặc hủy theo action cha (Index)
                                ),
                'View'=>array('alias'=>'Xem',
                                'childActions'=>array()
                                ),
                'Delete'=>array('alias'=>'Xóa',
                                'childActions'=>array()
                                ),
                'Report'=>array('alias'=>'Báo cáo',
                                'childActions'=>array()
                                ),
                )
        ),/* end controller */
        
        'InventoryCustomer'=>array(
            'alias'=>'Tồn đầu kỳ KH - InventoryCustomer',
            'actions'=>array(
                'ReportDebit'=>array('alias'=>'BC công nợ quá hạn - ReportDebit',
                                'childActions'=>array()//những actions mà sẽ được cấp quyền hoặc hủy theo action cha. EX 'childActions'=>array('Approve','Reject')
                                ),
                'ReportWarningVo'=>array('alias'=>'BC cảnh báo tồn vỏ - reportWarningVo',
                                'childActions'=>array()//những actions mà sẽ được cấp quyền hoặc hủy theo action cha. EX 'childActions'=>array('Approve','Reject')
                                ),
                'LimitDebitIndex'=>array('alias'=>'List Hạn mức nợ vỏ - LimitDebitIndex',
                                'childActions'=>array()//những actions mà sẽ được cấp quyền hoặc hủy theo action cha. EX 'childActions'=>array('Approve','Reject')
                                ),
                'LimitDebitCreate'=>array('alias'=>'Tạo mới Hạn mức nợ vỏ - LimitDebitCreate',
                                'childActions'=>array()//những actions mà sẽ được cấp quyền hoặc hủy theo action cha. EX 'childActions'=>array('Approve','Reject')
                                ),
                'LimitDebitUpdate'=>array('alias'=>'Cập nhật Hạn mức nợ vỏ - LimitDebitUpdate',
                                'childActions'=>array()//những actions mà sẽ được cấp quyền hoặc hủy theo action cha. EX 'childActions'=>array('Approve','Reject')
                                ),
                'Index'=>array('alias'=>'Tồn đầu kỳ KH - Index',
                                'childActions'=>array()//những actions mà sẽ được cấp quyền hoặc hủy theo action cha. EX 'childActions'=>array('Approve','Reject')
                                ),
                'Create'=>array('alias'=>'Tạo mới',
                                'childActions'=>array()
                                ),
                'Update'=>array('alias'=>'Cập nhật',
                                'childActions'=>array()//những actions mà sẽ được cấp quyền hoặc hủy theo action cha (Index)
                                ),
                'View'=>array('alias'=>'Xem',
                                'childActions'=>array()
                                ),
                'Delete'=>array('alias'=>'Xóa',
                                'childActions'=>array()
                                ),
                'ReportDebitVo'=>array('alias'=>'Báo cáo công nợ vỏ',
                                'childActions'=>array()
                                ),
                'ReportDebitCash'=>array('alias'=>'Báo cáo công nợ tiền',
                                'childActions'=>array()
                                ),
                )
        ),/* end controller */
        
        'UpholdHgd'=>array(
            'alias'=>'Bảo Trì Hộ Gia Đình - UpholdHgd',
            'actions'=>array(
                'ReportMaintenance'=>array('alias'=>'BC tổng đài đơn bò mối - reportMaintenance',
                                'childActions'=>array()//những actions mà sẽ được cấp quyền hoặc hủy theo action cha. EX 'childActions'=>array('Approve','Reject')
                                ),
                'CommentCancel'=>array('alias'=>'Tổng đài hủy đơn - commentCancel',
                                'childActions'=>array()//những actions mà sẽ được cấp quyền hoặc hủy theo action cha. EX 'childActions'=>array('Approve','Reject')
                                ),
                'ReportSum'=>array('alias'=>'Báo cáo bảo trì Bò Mối / HGĐ - ReportSum by Callcenter',
                                'childActions'=>array()//những actions mà sẽ được cấp quyền hoặc hủy theo action cha. EX 'childActions'=>array('Approve','Reject')
                                ),
                'Index'=>array('alias'=>'Bảo Trì Hộ Gia Đình - Index',
                                'childActions'=>array()//những actions mà sẽ được cấp quyền hoặc hủy theo action cha. EX 'childActions'=>array('Approve','Reject')
                                ),
                'Create'=>array('alias'=>'Tạo mới',
                                'childActions'=>array()
                                ),
                'Update'=>array('alias'=>'Cập nhật',
                                'childActions'=>array()//những actions mà sẽ được cấp quyền hoặc hủy theo action cha (Index)
                                ),
                'View'=>array('alias'=>'Xem',
                                'childActions'=>array()
                                ),
                'Delete'=>array('alias'=>'Xóa',
                                'childActions'=>array()
                                ),
                )
            ),/* end controller */
        
        'Sms'=>array(
            'alias'=>'List - Sms',
            'actions'=>array(
                'Index'=>array('alias'=>'Sms - Index',
                                'childActions'=>array()//những actions mà sẽ được cấp quyền hoặc hủy theo action cha. EX 'childActions'=>array('Approve','Reject')
                                ),
                )
            ),/* end controller */
        'GasTrackLogin'=>array(
            'alias'=>'History Login - GasTrackLogin',
            'actions'=>array(
                'Index'=>array('alias'=>'History Login - Index',
                                'childActions'=>array()//những actions mà sẽ được cấp quyền hoặc hủy theo action cha. EX 'childActions'=>array('Approve','Reject')
                                ),
                )
            ),/* end controller */
        
        'EmployeeCashbook'=>array( // Now 22, 2016
            'alias'=>'Thu chi NV giao nhận - EmployeeCashbook',
            'actions'=>array(
                'Index'=>array('alias'=>'Thu chi NV giao nhận - Index',
                                'childActions'=>array()//những actions mà sẽ được cấp quyền hoặc hủy theo action cha. EX 'childActions'=>array('Approve','Reject')
                                ),
                'Create'=>array('alias'=>'Tạo mới - không phân quyền này cho user',
                                'childActions'=>array()
                                ),
                'Update'=>array('alias'=>'Cập nhật - không phân quyền này cho user',
                                'childActions'=>array()//những actions mà sẽ được cấp quyền hoặc hủy theo action cha (Index)
                                ),
                'View'=>array('alias'=>'Xem',
                                'childActions'=>array()
                                ),
                'Delete'=>array('alias'=>'Xóa',
                                'childActions'=>array()
                                ),
                )
            ),/* end controller */
        
        'GasAppOrder'=>array( // Now 22, 2016
            'alias'=>'Đặt hàng bò mối - GasAppOrder',
            'actions'=>array(
                'ReportRevenue'=>array('alias'=>'BC doanh thu Gas bò mối - reportRevenue',
                                'childActions'=>array()//những actions mà sẽ được cấp quyền hoặc hủy theo action cha. EX 'childActions'=>array('Approve','Reject')
                                ),
                'Index'=>array('alias'=>'Đặt hàng bò mối - Index',
                                'childActions'=>array()//những actions mà sẽ được cấp quyền hoặc hủy theo action cha. EX 'childActions'=>array('Approve','Reject')
                                ),
                'View'=>array('alias'=>'Xem',
                                'childActions'=>array()
                                ),
                'Update'=>array('alias'=>'Cập nhật',
                                'childActions'=>array()
                                ),
                'Daily'=>array('alias'=>'BC Daily bò mối - Daily',
                                'childActions'=>array()
                                ),
                'AppReport'=>array('alias'=>'Báo cáo khách hàng dùng app - AppReport',
                                'childActions'=>array()
                                ),
                'SoldOut'=>array('alias'=>'Báo cáo xuất bán - soldOut',
                                'childActions'=>array()
                                ),
                'ReportGas24h'=>array('alias'=>'Báo cáo App Gas24h - reportGas24h',
                                'childActions'=>array()
                                ),
                'IndexStock'=>array('alias'=>'Nhập xuất STT IndexStock',
                                'childActions'=>array()
                                ),
                'ViewStock'=>array('alias'=>'ViewStock',
                                'childActions'=>array()
                                ),
                'UpdateStock'=>array('alias'=>'UpdateStock',
                                'childActions'=>array()
                                ),
                'CreateStock'=>array('alias'=>'CreateStock',
                                'childActions'=>array()
                                ),
                'DeleteStock'=>array('alias'=>'DeleteStock',
                                'childActions'=>array()
                                ),
                
                'Delete'=>array('alias'=>'Xóa',
                                'childActions'=>array()
                                ),
                
                )
            ),/* end controller */
        
        'Transaction'=>array( // Now 22, 2016
            'alias'=>'Thông báo đặt hàng điều phối + APP - Transaction',
            'actions'=>array(
                'ChangeAgentKtkv'=>array('alias'=>'Đổi đại lý của KTKV + CV - ChangeAgentKtkv',
                                'childActions'=>array()//những actions mà sẽ được cấp quyền hoặc hủy theo action cha. EX 'childActions'=>array('Approve','Reject')
                                ),
                'Index'=>array('alias'=>'List Thông báo - Index',
                                'childActions'=>array()//những actions mà sẽ được cấp quyền hoặc hủy theo action cha. EX 'childActions'=>array('Approve','Reject')
                                ),
                'GooglePlace'=>array('alias'=>'Xem Google Map Search Agent - googlePlace',
                                'childActions'=>array()//những actions mà sẽ được cấp quyền hoặc hủy theo action cha. EX 'childActions'=>array('Approve','Reject')
                                ),
                'ChangeAgent'=>array('alias'=>'Đổi đại lý của giao nhận - ChangeAgent',
                                'childActions'=>array()//những actions mà sẽ được cấp quyền hoặc hủy theo action cha. EX 'childActions'=>array('Approve','Reject')
                                ),
                'View'=>array('alias'=>'Xem vị trí KH - view',
                                'childActions'=>array()
                                ),
                )
            ),/* end controller */
        'UsersPriceHgd'=>array( // Now 22, 2016
            'alias'=>'Giá KH hộ gia đình - UsersPriceHgd',
            'actions'=>array(
                'Index'=>array('alias'=>'Giá KH hộ gia đình - Index',
                                'childActions'=>array()//những actions mà sẽ được cấp quyền hoặc hủy theo action cha. EX 'childActions'=>array('Approve','Reject')
                                ),
                'Update'=>array('alias'=>'Cập nhật',
                                'childActions'=>array()//những actions mà sẽ được cấp quyền hoặc hủy theo action cha (Index)
                                ),
                'View'=>array('alias'=>'Xem',
                                'childActions'=>array()
                                ),
                )
            ),/* end controller */
        
        'UsersPriceRequest'=>array( // Now 22, 2016
            'alias'=>'Đề xuất tăng giảm giá Bò Mối - UsersPriceRequest',
            'actions'=>array(
                'Index'=>array('alias'=>'Đề xuất tăng giảm giá Bò Mối- Index',
                                'childActions'=>array()//những actions mà sẽ được cấp quyền hoặc hủy theo action cha. EX 'childActions'=>array('Approve','Reject')
                                ),
                'Create'=>array('alias'=>'Tạo mới',
                                'childActions'=>array()
                                ),
                'Update'=>array('alias'=>'Cập nhật',
                                'childActions'=>array()//những actions mà sẽ được cấp quyền hoặc hủy theo action cha (Index)
                                ),
                'View'=>array('alias'=>'Xem',
                                'childActions'=>array()
                                ),
                'Delete'=>array('alias'=>'Xóa',
                                'childActions'=>array()
                                ),
                )
            ),/* end controller */
        
        'UsersPrice'=>array( // Now 02, 2015
            'alias'=>'Nhập giá G của KH BÒ Mối - UsersPrice',
            'actions'=>array(
                'ReportManage'=>array('alias'=>'BC quản trị đầu tư KH - ReportManage',
                                'childActions'=>array()
                                ),
                'Index'=>array('alias'=>'List Giá Khách Hàng BÒ Mối - Index',
                                'childActions'=>array()//những actions mà sẽ được cấp quyền hoặc hủy theo action cha. EX 'childActions'=>array('Approve','Reject')
                                ),
                'Create'=>array('alias'=>'Tạo mới',
                                'childActions'=>array()//những actions mà sẽ được cấp quyền hoặc hủy theo action cha (Index)
                                ),
                'Update'=>array('alias'=>'Cập nhật',
                                'childActions'=>array()//những actions mà sẽ được cấp quyền hoặc hủy theo action cha (Index)
                                ),
//                'View'=>array('alias'=>'Xem',
//                                'childActions'=>array()
//                                ),
                'Delete'=>array('alias'=>'Xóa',
                                'childActions'=>array()
                                ),
                'ExportExcel'=>array('alias'=>'ExportExcel',
                                'childActions'=>array()
                                ),
                'IndexAlcohol'=>array('alias'=>'List Giá Cồn KH Bò Mối - IndexAlcohol',
                                'childActions'=>array()
                                ),
                'CreateAlcohol'=>array('alias'=>'Tạo Mới Giá Cồn - CreateAlcohol',
                                'childActions'=>array()
                                ),
                'UpdateAlcohol'=>array('alias'=>'Cập nhật Giá Cồn - UpdateAlcohol',
                                'childActions'=>array()
                                ),
                )
            ),/* end controller */
        
        'GasPrice'=>array( // Now 02, 2015
            'alias'=>' Thiết lập giá CP - GasPrice',
            'actions'=>array(
                'Index'=>array('alias'=>'Thiết lập giá CP - Index',
                                'childActions'=>array()//những actions mà sẽ được cấp quyền hoặc hủy theo action cha. EX 'childActions'=>array('Approve','Reject')
                                ),
                'Create'=>array('alias'=>'Tạo mới',
                                'childActions'=>array()//những actions mà sẽ được cấp quyền hoặc hủy theo action cha (Index)
                                ),
                'Update'=>array('alias'=>'Cập nhật',
                                'childActions'=>array()//những actions mà sẽ được cấp quyền hoặc hủy theo action cha (Index)
                                ),
                'View'=>array('alias'=>'Xem',
                                'childActions'=>array()
                                ),
                'Delete'=>array('alias'=>'Xóa',
                                'childActions'=>array()
                                ),
                'UpdateStatus'=>array('alias'=>'Cập nhật trạng thái - UpdateStatus',
                                'childActions'=>array()
                                ),
                )
            ),/* end controller */        
        
        'BuVo'=>array( // Now 22, 2016
            'alias'=>'Setup bù vỏ - buVo',
            'actions'=>array(
                'Index'=>array('alias'=>'Setup bù vỏ- Index',
                                'childActions'=>array()//những actions mà sẽ được cấp quyền hoặc hủy theo action cha. EX 'childActions'=>array('Approve','Reject')
                                ),
                'Create'=>array('alias'=>'Tạo mới',
                                'childActions'=>array()
                                ),
                'Update'=>array('alias'=>'Cập nhật',
                                'childActions'=>array()//những actions mà sẽ được cấp quyền hoặc hủy theo action cha (Index)
                                ),
                'View'=>array('alias'=>'Xem',
                                'childActions'=>array()
                                ),
                'Delete'=>array('alias'=>'Xóa',
                                'childActions'=>array()
                                ),
                )
            ),/* end controller */
        
        'WorkReport'=>array( // Jul 10, 2016
            'alias'=>'Báo cáo công việc - workReport',
            'actions'=>array(
                'Index'=>array('alias'=>'Báo cáo công việc- Index',
                                'childActions'=>array()//những actions mà sẽ được cấp quyền hoặc hủy theo action cha. EX 'childActions'=>array('Approve','Reject')
                                ),
                'View'=>array('alias'=>'Xem',
                                'childActions'=>array()
                                ),
                'Delete'=>array('alias'=>'Xóa',
                                'childActions'=>array()
                                ),
                )
            ),/* end controller */
        
        'CallHistory'=>array( // Jul 10, 2016
            'alias'=>'Cuộc gọi đại lý - CallHistory',
            'actions'=>array(
                'Index'=>array('alias'=>'Cuộc gọi đại lý - Index',
                                'childActions'=>array()//những actions mà sẽ được cấp quyền hoặc hủy theo action cha. EX 'childActions'=>array('Approve','Reject')
                                ),
                'Delete'=>array('alias'=>'Xóa',
                                'childActions'=>array()
                                ),
                )
            ),/* end controller */
        
        'CustomerSpecial'=>array( // Jul 10, 2016
            'alias'=>'Khách hàng đặc biệt - CustomerSpecial',
            'actions'=>array(
                'Index'=>array('alias'=>'Khách hàng đặc biệt - Index',
                                'childActions'=>array()//những actions mà sẽ được cấp quyền hoặc hủy theo action cha. EX 'childActions'=>array('Approve','Reject')
                                ),
                'Create'=>array('alias'=>'Tạo mới',
                                'childActions'=>array()
                                ),
                'Update'=>array('alias'=>'Cập nhật',
                                'childActions'=>array()//những actions mà sẽ được cấp quyền hoặc hủy theo action cha (Index)
                                ),
                'View'=>array('alias'=>'Xem',
                                'childActions'=>array()
                                ),
                'Delete'=>array('alias'=>'Xóa',
                                'childActions'=>array()
                                ),
                )
            ),/* end controller */
        
        'Borrow'=>array( // Jul 10, 2016
            'alias'=>'Quản Lý Vay - Borrow',
            'actions'=>array(
                'Index'=>array('alias'=>'List Quản Lý Vay - Index',
                                'childActions'=>array()//những actions mà sẽ được cấp quyền hoặc hủy theo action cha. EX 'childActions'=>array('Approve','Reject')
                                ),
                'Create'=>array('alias'=>'Tạo mới',
                                'childActions'=>array()
                                ),
                'Update'=>array('alias'=>'Cập nhật',
                                'childActions'=>array()//những actions mà sẽ được cấp quyền hoặc hủy theo action cha (Index)
                                ),
                'View'=>array('alias'=>'Xem',
                                'childActions'=>array()
                                ),
                'Delete'=>array('alias'=>'Xóa',
                                'childActions'=>array()
                                ),
                'CreateDetail'=>array('alias'=>'Thêm mới đáo hạn',
                                'childActions'=>array()
                                ),
                'DeleteDetail'=>array('alias'=>'Xóa đáo hạn - xóa chi tiết',
                                'childActions'=>array()
                                ),
                'ExportExcel'=>array('alias'=>'ExportExcel',
                                'childActions'=>array()
                                ),
                'Report'=>array('alias'=>'Report',
                                'childActions'=>array()
                                ),
                'ExportExcelReport'=>array('alias'=>'Report - ExportExcelReport',
                                'childActions'=>array()
                                ),
                'LendIndex'=>array('alias'=>'List Quản cho Vay - LendIndex',
                                'childActions'=>array()//những actions mà sẽ được cấp quyền hoặc hủy theo action cha. EX 'childActions'=>array('Approve','Reject')
                                ),
                'LendCreate'=>array('alias'=>'Tạo mới cho Vay - LendCreate',
                                'childActions'=>array()
                                ),
                'LendUpdate'=>array('alias'=>'Cập nhật cho Vay - LendUpdate',
                                'childActions'=>array()//những actions mà sẽ được cấp quyền hoặc hủy theo action cha (Index)
                                ),
                'LendView'=>array('alias'=>'Xem cho Vay - LendView',
                                'childActions'=>array()
                                ),
                'LendExportExcel'=>array('alias'=>'Export Excel List cho Vay - LendExportExcel',
                                'childActions'=>array()
                                ),
                'LendReport'=>array('alias'=>'Báo cáo cho Vay - LendReport',
                                'childActions'=>array()
                                ),
                'LendExportExcelReport'=>array('alias'=>'Export Excel Báo cáo cho Vay - LendExportExcelReport',
                                'childActions'=>array()
                                ),
                
                )
            ),/* end controller */
        
        'GasSettle'=>array( // Jul 10, 2016
            'alias'=>'Quyết toán - gasSettle',
            'actions'=>array(
                'Index'=>array('alias'=>'List Quyết toán - Index',
                                'childActions'=>array()//những actions mà sẽ được cấp quyền hoặc hủy theo action cha. EX 'childActions'=>array('Approve','Reject')
                                ),
                'Create'=>array('alias'=>'Tạo mới',
                                'childActions'=>array('CreateSettle')//những actions mà sẽ được cấp quyền hoặc hủy theo action cha (Index)
                                ),
                'Update'=>array('alias'=>'Cập nhật',
                                'childActions'=>array()//những actions mà sẽ được cấp quyền hoặc hủy theo action cha (Index)
                                ),
                'View'=>array('alias'=>'Xem',
                                'childActions'=>array()
                                ),
                'Delete'=>array('alias'=>'Xóa',
                                'childActions'=>array()
                                ),
                'CashierConfirm'=>array('alias'=>'Thủ Quỹ Xác Nhận Chi Tiền',
                                'childActions'=>array()
                                ),
                'StatusUnlock'=>array('alias'=>'Gỡ bỏ tạm ứng quá hạn',
                                'childActions'=>array()
                                ),
                'NoteTreasurerAccountant'=>array('alias'=>'Ghi chú của thủ quỹ/ kế toán',
                                'childActions'=>array()
                                ),
                )
            ),/* end controller */
        
        'Sell'=>array( // Now 02, 2015
            'alias'=>'Bán hàng hộ gia đình- Sell',
            'actions'=>array(
                'CommentCanCel'=>array('alias'=>'Tổng đài note hủy đơn - CommentCanCel',
                                'childActions'=>array()//những actions mà sẽ được cấp quyền hoặc hủy theo action cha. EX 'childActions'=>array('Approve','Reject')
                                ),
                'ReportQuantity'=>array('alias'=>'BC sản lượng bình cam - ReportQuantity',
                                'childActions'=>array()//những actions mà sẽ được cấp quyền hoặc hủy theo action cha. EX 'childActions'=>array('Approve','Reject')
                                ),
                'ReportUidLogin'=>array('alias'=>'BC doanh thu NV tổng đài - reportUidLogin',
                                'childActions'=>array()//những actions mà sẽ được cấp quyền hoặc hủy theo action cha. EX 'childActions'=>array('Approve','Reject')
                                ),
                'ReportDiscount'=>array('alias'=>'Thống kê giảm giá HGĐ - ReportDiscount',
                                'childActions'=>array()//những actions mà sẽ được cấp quyền hoặc hủy theo action cha. EX 'childActions'=>array('Approve','Reject')
                                ),
                'ReportTraThe'=>array('alias'=>'BC trả thẻ - reportTraThe',
                                'childActions'=>array()//những actions mà sẽ được cấp quyền hoặc hủy theo action cha. EX 'childActions'=>array('Approve','Reject')
                                ),
                'ReportAllAgent'=>array('alias'=>'BC toàn hệ thống - ReportAllAgent',
                                'childActions'=>array()//những actions mà sẽ được cấp quyền hoặc hủy theo action cha. EX 'childActions'=>array('Approve','Reject')
                                ),
                'Index'=>array('alias'=>'Bán hàng hộ gia đình - Index',
                                'childActions'=>array()//những actions mà sẽ được cấp quyền hoặc hủy theo action cha. EX 'childActions'=>array('Approve','Reject')
                                ),
                'Create'=>array('alias'=>'Tạo mới',
                                'childActions'=>array()//những actions mà sẽ được cấp quyền hoặc hủy theo action cha (Index)
                                ),
                'Update'=>array('alias'=>'Cập nhật',
                                'childActions'=>array()//những actions mà sẽ được cấp quyền hoặc hủy theo action cha (Index)
                                ),
                'View'=>array('alias'=>'Xem',
                                'childActions'=>array()
                                ),
                'ReportAgent'=>array('alias'=>'Báo cáo bán hàng hộ gia đình - ReportAgent',
                                'childActions'=>array()
                                ),
                'ExportExcel'=>array('alias'=>'ExportExcel Daily bán hàng hộ gia đình',
                                'childActions'=>array()
                                ),
                'TargetCcs'=>array('alias'=>'Báo cáo bình quay về CCS - TargetCcs',
                                'childActions'=>array()
                                ),
                'ListRating'=>array('alias'=>'KH đánh giá App Gas24h - ListRating',
                                'childActions'=>array()
                                ),
                'Audit'=>array('alias'=>'Audit test đơn hủy - Audit',
                                'childActions'=>array()
                                ),
                'Delete'=>array('alias'=>'Xóa',
                                'childActions'=>array()
                                ),
                )
            ),/* end controller */
        
        'GasWorkSchedule'=>array( // Now 02, 2015
            'alias'=>'Lịch Điều Phối - GasWorkSchedule',
            'actions'=>array(
                'Index'=>array('alias'=>'Lịch Điều Phối - Index',
                                'childActions'=>array()//những actions mà sẽ được cấp quyền hoặc hủy theo action cha. EX 'childActions'=>array('Approve','Reject')
                                ),
                'Create'=>array('alias'=>'Tạo mới',
                                'childActions'=>array()//những actions mà sẽ được cấp quyền hoặc hủy theo action cha (Index)
                                ),
                'Update'=>array('alias'=>'Cập nhật',
                                'childActions'=>array()//những actions mà sẽ được cấp quyền hoặc hủy theo action cha (Index)
                                ),
                'View'=>array('alias'=>'Xem',
                                'childActions'=>array()
                                ),
                'ChangeSlot'=>array('alias'=>'Đổi lịch điều phối',
                                'childActions'=>array()
                                ),
                )
            ),/* end controller */
        
        'GasOrderPromotion'=>array( // Now 02, 2015
            'alias'=>'Đặt hàng vật tư - khuyến mãi - GasOrderPromotion',
            'actions'=>array(
                'Index'=>array('alias'=>'Đặt hàng vật tư - khuyến mãi - Index',
                                'childActions'=>array()//những actions mà sẽ được cấp quyền hoặc hủy theo action cha. EX 'childActions'=>array('Approve','Reject')
                                ),
                'Create'=>array('alias'=>'Tạo mới',
                                'childActions'=>array()//những actions mà sẽ được cấp quyền hoặc hủy theo action cha (Index)
                                ),
                'Update'=>array('alias'=>'Cập nhật',
                                'childActions'=>array()//những actions mà sẽ được cấp quyền hoặc hủy theo action cha (Index)
                                ),
                'View'=>array('alias'=>'Xem',
                                'childActions'=>array()
                                ),
                'Delete'=>array('alias'=>'Xóa',
                                'childActions'=>array()
                                ),
                'Summary'=>array('alias'=>'View Report Summary',
                                'childActions'=>array()
                                ),
                'Print'=>array('alias'=>'Print - In',
                                'childActions'=>array()
                                ),
                )
            ),/* end controller */
        
        'GasSupportAgent'=>array( // Now 02, 2015
            'alias'=>'Đề xuất sửa chữa - GasSupportAgent',
            'actions'=>array(
                'Index'=>array('alias'=>'Đề xuất sửa chữa - Index',
                                'childActions'=>array()//những actions mà sẽ được cấp quyền hoặc hủy theo action cha. EX 'childActions'=>array('Approve','Reject')
                                ),
                'Create'=>array('alias'=>'Tạo mới',
                                'childActions'=>array()//những actions mà sẽ được cấp quyền hoặc hủy theo action cha (Index)
                                ),
                'Update'=>array('alias'=>'Cập nhật',
                                'childActions'=>array()//những actions mà sẽ được cấp quyền hoặc hủy theo action cha (Index)
                                ),
                'View'=>array('alias'=>'Xem',
                                'childActions'=>array()
                                ),
                'Delete'=>array('alias'=>'Xóa',
                                'childActions'=>array()
                                ),
                )
            ),/* end controller */
        
        'GasUpholdError'=>array(
            'alias'=>'Danh Sách Lỗi bảo trì- GasUpholdError',
            'actions'=>array(
                'Index'=>array('alias'=>'Danh Sách Lỗi bảo trì - Index',
                                'childActions'=>array()//những actions mà sẽ được cấp quyền hoặc hủy theo action cha. EX 'childActions'=>array('Approve','Reject')
                                ),
                'View'=>array('alias'=>'Xem',
                                'childActions'=>array()
                                ),
                'Delete'=>array('alias'=>'Xóa',
                                'childActions'=>array()
                                ),
                )
        ),/* end controller */
        
        'GasUpholdPlan'=>array(
            'alias'=>'Kế hoạch bảo trì định kỳ - GasUpholdPlan',
            'actions'=>array(
                'Index'=>array('alias'=>'Kế hoạch bảo trì định kỳ - index',
                                'childActions'=>array()//những actions mà sẽ được cấp quyền hoặc hủy theo action cha. EX 'childActions'=>array('Approve','Reject')
                                ),
                'Create'=>array('alias'=>'Tạo mới',
                                'childActions'=>array()
                                ),
                'Update'=>array('alias'=>'Cập nhật',
                                'childActions'=>array()//những actions mà sẽ được cấp quyền hoặc hủy theo action cha (Index)
                                ),
                'View'=>array('alias'=>'Xem',
                                'childActions'=>array()
                                ),
                'Delete'=>array('alias'=>'Xóa',
                                'childActions'=>array()
                                ),
                )
        ),/* end controller */
        
        'GasUpholdSchedule'=>array( // Now 02, 2015
            'alias'=>'Bảo Trì Định Kỳ - GasUpholdSchedule',
            'actions'=>array(
                'Index'=>array('alias'=>'Bảo Trì Định Kỳ - Index',
                                'childActions'=>array()//những actions mà sẽ được cấp quyền hoặc hủy theo action cha. EX 'childActions'=>array('Approve','Reject')
                                ),
                'Create'=>array('alias'=>'Tạo mới',
                                'childActions'=>array()//những actions mà sẽ được cấp quyền hoặc hủy theo action cha (Index)
                                ),
                'Update'=>array('alias'=>'Cập nhật',
                                'childActions'=>array()//những actions mà sẽ được cấp quyền hoặc hủy theo action cha (Index)
                                ),
                'View'=>array('alias'=>'Xem',
                                'childActions'=>array()
                                ),
                'Delete'=>array('alias'=>'Xóa',
                                'childActions'=>array()
                                ),
                'ExportExcel'=>array('alias'=>'ExportExcel',
                                'childActions'=>array()
                                ),
                )
            ),/* end controller */
        
        'GasUphold'=>array( // Now 02, 2015
            'alias'=>'Bảo Trì Sự Cố - GasUphold',
            'actions'=>array(
                'Index'=>array('alias'=>'Bảo Trì Sự Cố - Index',
                                'childActions'=>array()//những actions mà sẽ được cấp quyền hoặc hủy theo action cha. EX 'childActions'=>array('Approve','Reject')
                                ),
                'Create'=>array('alias'=>'Tạo mới',
                                'childActions'=>array()//những actions mà sẽ được cấp quyền hoặc hủy theo action cha (Index)
                                ),
                'Update'=>array('alias'=>'Cập nhật',
                                'childActions'=>array()//những actions mà sẽ được cấp quyền hoặc hủy theo action cha (Index)
                                ),
                'View'=>array('alias'=>'Xem',
                                'childActions'=>array()
                                ),
                'Delete'=>array('alias'=>'Xóa',
                                'childActions'=>array()
                                ),
                'ReportByType'=>array('alias'=>'Report theo loại sự cố - ReportByType',
                                'childActions'=>array()
                                ),
                'ReportByCustomer'=>array('alias'=>'Report theo khách hàng - ReportByCustomer',
                                'childActions'=>array()
                                ),
                'ReportByEmployee'=>array('alias'=>'Report theo nhân viên- ReportByEmployee',
                                'childActions'=>array()
                                ),
                'ReportUpholdSchedule'=>array('alias'=>'Báo cáo bảo trì định kỳ- ReportUpholdSchedule',
                                'childActions'=>array()
                                ),
                'ReportScheduleDaily'=>array('alias'=>'Báo cáo bảo trì định kỳ hàng ngày- ReportScheduleDaily',
                                'childActions'=>array()
                                ),
                'ListRating'=>array('alias'=>'KH đánh giá bảo trì - ListRating',
                                'childActions'=>array()
                                ),
                'ReportPieceMaintain'=>array('alias'=>'Báo cáo khoán bảo trì - ReportPieceMaintain',
                                'childActions'=>array()
                                ),
                )
            ),/* end controller */

        'GasPtttDailyGoback'=>array(
            'alias'=>'Bình Quay Về Hàng Ngày - GasPtttDailyGoback',
            'actions'=>array(
                'Index'=>array('alias'=>'Bình Quay Về Hàng Ngày - Index',
                                'childActions'=>array()//những actions mà sẽ được cấp quyền hoặc hủy theo action cha. EX 'childActions'=>array('Approve','Reject')
                                ),
                'Create'=>array('alias'=>'Tạo mới',
                                'childActions'=>array()//những actions mà sẽ được cấp quyền hoặc hủy theo action cha (Index)
                                ),
                'Update'=>array('alias'=>'Cập nhật',
                                'childActions'=>array()//những actions mà sẽ được cấp quyền hoặc hủy theo action cha (Index)
                                ),
                'View'=>array('alias'=>'Xem',
                                'childActions'=>array()
                                ),
                'Delete'=>array('alias'=>'Xóa',
                                'childActions'=>array()
                                ),
                'ReportCcsDaily'=>array('alias'=>'Báo cáo điểm và bình quay về - reportCcsDaily',
                                'childActions'=>array()
                                ),
                )
            ),/* end controller */
        
        'GasIssueTickets'=>array(
            'alias'=>'Phản Ánh Sự Việc ( Issue ) - GasIssueTickets',
            'actions'=>array(
                // Delete, DeleteOneDetail, Index, , , , Reopen_ticket
                'Index'=>array('alias'=>'Phản Ánh Sự Việc - Index',
                                'childActions'=>array()//những actions mà sẽ được cấp quyền hoặc hủy theo action cha. EX 'childActions'=>array('Approve','Reject')
                                ),
                'Pick_ticket'=>array('alias'=>'Pick Issue',
                                'childActions'=>array()//những actions mà sẽ được cấp quyền hoặc hủy theo action cha (Index)
                                ),
                'Reply'=>array('alias'=>'Reply Issue',
                                'childActions'=>array()//những actions mà sẽ được cấp quyền hoặc hủy theo action cha (Index)
                                ),
                'Close_ticket'=>array('alias'=>'Close Issue',
                                'childActions'=>array()
                                ),
                'Reopen_ticket'=>array('alias'=>'Reopen Issue - Reopen_ticket',
                                'childActions'=>array()
                                ),
                'Delete'=>array('alias'=>'Xóa Root Topic',
                                'childActions'=>array()
                                ),
                'DeleteOneDetail'=>array('alias'=>'Xóa Delete One Detail',
                                'childActions'=>array()
                                ),
                'PunishEmployee'=>array('alias'=>'Phạt NV 50k - PunishEmployee',
                                'childActions'=>array()
                                ),
                )
            ),/* end controller */
        
        'GasSupportCustomer'=>array(
            'alias'=>'PHIẾU GIAO NHIỆM VỤ - Đề xuất hỗ trợ khách hàng lớn - GasSupportCustomer',
            'actions'=>array(
                'Index'=>array('alias'=>'Đề xuất hỗ trợ khách hàng lớn - Index',
                                'childActions'=>array()//những actions mà sẽ được cấp quyền hoặc hủy theo action cha. EX 'childActions'=>array('Approve','Reject')
                                ),
                'Create'=>array('alias'=>'Tạo mới',
                                'childActions'=>array()//những actions mà sẽ được cấp quyền hoặc hủy theo action cha (Index)
                                ),
                'Update'=>array('alias'=>'Cập nhật',
                                'childActions'=>array()//những actions mà sẽ được cấp quyền hoặc hủy theo action cha (Index)
                                ),
                'View'=>array('alias'=>'Xem',
                                'childActions'=>array()
                                ),
                'Update_status'=>array('alias'=>'Cập nhật trạng thái - Approved',
                                'childActions'=>array()
                                ),
                'Delete'=>array('alias'=>'Xóa',
                                'childActions'=>array()
                                ),
                'Print'=>array('alias'=>'Print',
                                'childActions'=>array()
                                ),
                'Hgd'=>array('alias'=>'List Đề Xuất Hộ Gia Đình - Hgd',
                                'childActions'=>array()
                                ),
                )
            ),/* end controller */
        
        'GasBreakTask'=>array(
            'alias'=>'Phân Công Công Việc - GasBreakTask',
            'actions'=>array(
                'Index'=>array('alias'=>'Quản Lý Phân Công Công Việc - Index',
                                'childActions'=>array()//những actions mà sẽ được cấp quyền hoặc hủy theo action cha. EX 'childActions'=>array('Approve','Reject')
                                ),
                'Create'=>array('alias'=>'Tạo mới',
                                'childActions'=>array()//những actions mà sẽ được cấp quyền hoặc hủy theo action cha (Index)
                                ),
                'Update'=>array('alias'=>'Cập nhật',
                                'childActions'=>array()//những actions mà sẽ được cấp quyền hoặc hủy theo action cha (Index)
                                ),
                'View'=>array('alias'=>'Xem',
                                'childActions'=>array()
                                ),                
                'Update_report_daily'=>array('alias'=>'Tạo Báo Cáo Công Việc Hàng Ngày',
                                'childActions'=>array()
                                ),                
                'Post_leader_help_content'=>array('alias'=>'Tạo Ý Kiến Chỉ Đạo',
                                'childActions'=>array()
                                ),                
                'Delete_report_daily'=>array('alias'=>'Xóa BC Công Việc Hàng Ngày + Ý Kiến Chỉ Đạo',
                                'childActions'=>array()
                                ),                
                'Delete'=>array('alias'=>'Xóa',
                                'childActions'=>array()
                                ),
                )
            ),/* end controller */
        
        'GasMeetingLaw'=>array(
            'alias'=>'Biên Bản Họp - Phòng Pháp Lý - GasMeetingLaw',
            'actions'=>array(
                'Index'=>array('alias'=>'Quản Lý Biên Bản Họp - Phòng Pháp Lý  - Index',
                                'childActions'=>array()//những actions mà sẽ được cấp quyền hoặc hủy theo action cha. EX 'childActions'=>array('Approve','Reject')
                                ),
                'Create'=>array('alias'=>'Tạo mới',
                                'childActions'=>array()//những actions mà sẽ được cấp quyền hoặc hủy theo action cha (Index)
                                ),
                'Update'=>array('alias'=>'Cập nhật',
                                'childActions'=>array()//những actions mà sẽ được cấp quyền hoặc hủy theo action cha (Index)
                                ),
                'View'=>array('alias'=>'Xem',
                                'childActions'=>array()
                                ),                
                'Delete'=>array('alias'=>'Xóa',
                                'childActions'=>array()
                                ),
                )
            ),/* end controller */
        
        'GasCustomerCheck'=>array(
            'alias'=>'Quản Lý khách hàng cần kiểm tra - GasCustomerCheck',
            'actions'=>array(
                'Index'=>array('alias'=>'Quản Lý khách hàng cần kiểm tra  - Index',
                                'childActions'=>array()//những actions mà sẽ được cấp quyền hoặc hủy theo action cha. EX 'childActions'=>array('Approve','Reject')
                                ),
                'Create'=>array('alias'=>'Tạo mới',
                                'childActions'=>array()//những actions mà sẽ được cấp quyền hoặc hủy theo action cha (Index)
                                ),
                'Update'=>array('alias'=>'Cập nhật',
                                'childActions'=>array()//những actions mà sẽ được cấp quyền hoặc hủy theo action cha (Index)
                                ),
                'View'=>array('alias'=>'Xem',
                                'childActions'=>array()
                                ),                
                'Update_customer'=>array('alias'=>'Cập nhật báo cáo kiểm tra',
                                'childActions'=>array()
                                ),                
                'Delete'=>array('alias'=>'Xóa',
                                'childActions'=>array()
                                ),
                )
            ),/* end controller */
        
        'GasMeetingMinutes'=>array(
            'alias'=>' Biên Bản Họp - GasMeetingMinutes',
            'actions'=>array(
                'Index'=>array('alias'=>' Quản Lý Biên Bản Họp - Index',
                                'childActions'=>array()//những actions mà sẽ được cấp quyền hoặc hủy theo action cha. EX 'childActions'=>array('Approve','Reject')
                                ),
                'Create'=>array('alias'=>'Tạo mới',
                                'childActions'=>array()//những actions mà sẽ được cấp quyền hoặc hủy theo action cha (Index)
                                ),
                'Update'=>array('alias'=>'Cập nhật',
                                'childActions'=>array()//những actions mà sẽ được cấp quyền hoặc hủy theo action cha (Index)
                                ),
                'View'=>array('alias'=>'Xem',
                                'childActions'=>array()
                                ),                
                'Text_post_comment'=>array('alias'=>'Comment báo cáo kết quả công việc',
                                'childActions'=>array()
                                ),                
                'Text_delete_comment'=>array('alias'=>'Xóa Comment',
                                'childActions'=>array()
                                ),                
                'Delete'=>array('alias'=>'Xóa',
                                'childActions'=>array()
                                ),
                )
            ),/* end controller */
        
        'GasLeave'=>array(
            'alias'=>' Quản Lý Nghỉ Phép - GasLeave',
            'actions'=>array(
                'Report'=>array('alias'=>'Report',
                                'childActions'=>array()//những actions mà sẽ được cấp quyền hoặc hủy theo action cha. EX 'childActions'=>array('Approve','Reject')
                                ),
                'Index'=>array('alias'=>' Quản Lý Nghỉ Phép',
                                'childActions'=>array()//những actions mà sẽ được cấp quyền hoặc hủy theo action cha. EX 'childActions'=>array('Approve','Reject')
                                ),
                'Create'=>array('alias'=>'Tạo mới',
                                'childActions'=>array()//những actions mà sẽ được cấp quyền hoặc hủy theo action cha (Index)
                                ),
                'Update'=>array('alias'=>'Cập nhật',
                                'childActions'=>array()//những actions mà sẽ được cấp quyền hoặc hủy theo action cha (Index)
                                ),
                'View'=>array('alias'=>'Xem',
                                'childActions'=>array('Text_view')
                                ),                
                'Update_status'=>array('alias'=>'Cập nhật trạng thái',
                                'childActions'=>array('Text_view')
                                ),                
                'Delete'=>array('alias'=>'Xóa',
                                'childActions'=>array()
                                ),
                )
            ),/* end controller */
        
        'GasLeaveHolidays'=>array(
            'alias'=>' Quản Lý Ngày Nghỉ Lễ - GasLeaveHolidays',
            'actions'=>array(
                'Index'=>array('alias'=>'Quản Lý Ngày Nghỉ Lễ',
                                'childActions'=>array('ToPdf', 'UpdateAjax')//những actions mà sẽ được cấp quyền hoặc hủy theo action cha. EX 'childActions'=>array('Approve','Reject')
                                ),
                'Create'=>array('alias'=>'Tạo mới',
                                'childActions'=>array()//những actions mà sẽ được cấp quyền hoặc hủy theo action cha (Index)
                                ),
                'Update'=>array('alias'=>'Cập nhật',
                                'childActions'=>array()//những actions mà sẽ được cấp quyền hoặc hủy theo action cha (Index)
                                ),
                'View'=>array('alias'=>'Xem',
                                'childActions'=>array('Text_view')
                                ),                
                'Delete'=>array('alias'=>'Xóa',
                                'childActions'=>array()
                                ),
                )
            ),/* end controller */   
        'GasText'=>array(
            'alias'=>'Văn Bản Công Ty - GasText',
            'actions'=>array(
                'Index'=>array('alias'=>'Quản lý danh sách văn bản',
                                'childActions'=>array()//những actions mà sẽ được cấp quyền hoặc hủy theo action cha. EX 'childActions'=>array('Approve','Reject')
                                ),
                'Create'=>array('alias'=>'Tạo mới',
                                'childActions'=>array()//những actions mà sẽ được cấp quyền hoặc hủy theo action cha (Index)
                                ),
                'Update'=>array('alias'=>'Cập nhật',
                                'childActions'=>array()//những actions mà sẽ được cấp quyền hoặc hủy theo action cha (Index)
                                ),
                'View'=>array('alias'=>'Xem',
                                'childActions'=>array('Text_view')
                                ),                
                'Text_post_comment'=>array('alias'=>'Post comment',
                                'childActions'=>array()
                                ),
                'AjaxActivate'=>array('alias'=>'AjaxActivate văn bản',
                                'childActions'=>array()
                                ),
                'AjaxDeactivate'=>array('alias'=>'AjaxDeactivate văn bản',
                                'childActions'=>array()
                                ),
                'Text_delete_comment'=>array('alias'=>'Xóa comment',
                                'childActions'=>array()
                                ),
                'Delete'=>array('alias'=>'Xóa văn bản',
                                'childActions'=>array()
                                ),
                )
            ),/* end controller */   
        
        'GasManageTool'=>array(
            'alias'=>'Quản Lý Cấp Phát Công Cụ-Dụng Cụ - GasManageTool',
            'actions'=>array(
                'Index'=>array('alias'=>'Quản Lý Cấp Phát Công Cụ-Dụng Cụ',
                                'childActions'=>array()//những actions mà sẽ được cấp quyền hoặc hủy theo action cha. EX 'childActions'=>array('Approve','Reject')
                                ),
                'Create'=>array('alias'=>'Tạo mới',
                                'childActions'=>array()//những actions mà sẽ được cấp quyền hoặc hủy theo action cha (Index)
                                ),
                'Update'=>array('alias'=>'Cập nhật',
                                'childActions'=>array()//những actions mà sẽ được cấp quyền hoặc hủy theo action cha (Index)
                                ),
                'View'=>array('alias'=>'Xem',
                                'childActions'=>array()
                                ),                
                'Delete'=>array('alias'=>'Xóa',
                                'childActions'=>array()
                                ),
                )
            ),/* end controller */   
        'GasProfile'=>array(
            'alias'=>'Quản Lý Hồ Sơ Pháp Lý - GasProfile',
            'actions'=>array(
                'Agent_report'=>array('alias'=>'Báo Cáo Hồ Sơ Pháp Lý',
                                'childActions'=>array()//những actions mà sẽ được cấp quyền hoặc hủy theo action cha. EX 'childActions'=>array('Approve','Reject')
                                ),
                'Index'=>array('alias'=>'Quản Lý Hồ Sơ Pháp Lý',
                                'childActions'=>array()//những actions mà sẽ được cấp quyền hoặc hủy theo action cha. EX 'childActions'=>array('Approve','Reject')
                                ),
                'Create'=>array('alias'=>'Tạo mới',
                                'childActions'=>array()//những actions mà sẽ được cấp quyền hoặc hủy theo action cha (Index)
                                ),
                'Update'=>array('alias'=>'Cập nhật',
                                'childActions'=>array()//những actions mà sẽ được cấp quyền hoặc hủy theo action cha (Index)
                                ),
                'View'=>array('alias'=>'Xem',
                                'childActions'=>array()
                                ),                
                'Delete'=>array('alias'=>'Xóa',
                                'childActions'=>array()
                                ),
                )
            ),/* end controller */   
        
        'Tickets'=>array(
            'alias'=>'Support - tickets',
            'actions'=>array(
                'Index'=>array('alias'=>'Xem danh sách tickets',
                                'childActions'=>array()//những actions mà sẽ được cấp quyền hoặc hủy theo action cha (Index)
                                ),
                'Create'=>array('alias'=>'Tạo mới',
                                'childActions'=>array()//những actions mà sẽ được cấp quyền hoặc hủy theo action cha (Index)
                                ),
//                'View'=>array('alias'=>'Xem',
//                                'childActions'=>array()
//                                ),                
                'Reply'=>array('alias'=>'Trả Lời ticket',
                                'childActions'=>array()
                                ),
                'Close_ticket'=>array('alias'=>'Close ticket',
                                'childActions'=>array()
                                ),
                'Pick_ticket'=>array('alias'=>'Pick ticket',
                                'childActions'=>array()
                                ),
                'Delete'=>array('alias'=>'Xóa',
                                'childActions'=>array()
                                ),
                'AuditFix'=>array('alias'=>'Report Sửa ticket - auditFix',
                                'childActions'=>array()
                                ),
                'ExportExcel'=>array('alias'=>'Export ticket - auditFix- ExportExcel',
                                'childActions'=>array()
                                ),
                )
            ),/* end controller */   
        
        'GasBussinessContract'=>array(
            'alias'=>'Quản Lý SPANCOP KHÁCH HÀNG - GasBussinessContract',
            'actions'=>array(
                'Index'=>array('alias'=>'Xem SPANCOP KHÁCH HÀNG - Index',
                                'childActions'=>array()//những actions mà sẽ được cấp quyền hoặc hủy theo action cha (Index)
                                ),
                'Create'=>array('alias'=>'Tạo mới',
                                'childActions'=>array()//những actions mà sẽ được cấp quyền hoặc hủy theo action cha (Index)
                                ),
                'View'=>array('alias'=>'Xem - View',
                                'childActions'=>array()
                                ),
                'Update'=>array('alias'=>'Cập nhật',
                                'childActions'=>array()
                                ),
                'Was_sign_contract'=>array('alias'=>'Danh Sách SPANCOP chưa (not) gắn khách hàng',
                                'childActions'=>array()
                                ),
                'Was_sign_contract_ready_system'=>array('alias'=>'Danh Sách SPANCOP đã gắn khách hàng',
                                'childActions'=>array()
                                ),
                'Update_customer_sign_contract'=>array('alias'=>'Cập nhật khách hàng cho SPANCOP',
                                'childActions'=>array()
                                ),
                'Quick_report'=>array('alias'=>'Báo Cáo SPANCOP Tóm Tắt',
                                'childActions'=>array()
                                ),
                'Big_customer'=>array('alias'=>'Danh Sách SPANCOP khách hàng lớn',
                                'childActions'=>array()
                                ),
                'Update_guide_help'=>array('alias'=>'Cập nhật hướng dẫn thực hiện',
                                'childActions'=>array()
                                ),
                'Delete'=>array('alias'=>'Xóa',
                                'childActions'=>array()
                                ),
                )
            ),/* end controller */        
        
        
        'Gasmaintainsell'=>array(
            'alias'=>'Quản Lý Bán Hàng Bảo Trì, Phát Triển Thị Trường - Gasmaintainsell',
            'actions'=>array(
                'Index'=>array('alias'=>'Xem Danh sách bán hàng',
                                'childActions'=>array()//những actions mà sẽ được cấp quyền hoặc hủy theo action cha (Index)
                                ),
                'Create'=>array('alias'=>'Tạo mới',
                                'childActions'=>array()//những actions mà sẽ được cấp quyền hoặc hủy theo action cha (Index)
                                ),
                'View'=>array('alias'=>'Xem',
                                'childActions'=>array()
                                ),
                'Update'=>array('alias'=>'Cập nhật',
                                'childActions'=>array()
                                ),
                'Delete'=>array('alias'=>'Xóa',
                                'childActions'=>array()
                                ),
                'Update_status_maintain_sell'=>array('alias'=>'Cập nhật trạng thái bán hàng',
                                'childActions'=>array()
                                ),
                'Export_list_gasmaintainsell'=>array('alias'=>'Export bán hàng',
                                'childActions'=>array()
                                ),
                'Admin_set_user'=>array('alias'=>'Setup duyệt phép',
                                'childActions'=>array()
                                ),
                )
            ),/* end controller */
        
        'Gasmarketdevelopment'=>array(
            'alias'=>'Quản Lý Phát Triển Thị Trường - Gasmarketdevelopment',
            'actions'=>array(
                'Index'=>array('alias'=>'Xem Danh sách Phát Triển Thị Trường',
                                'childActions'=>array()//những actions mà sẽ được cấp quyền hoặc hủy theo action cha (Index)
                                ),
                'Create'=>array('alias'=>'Tạo mới',
                                'childActions'=>array()//những actions mà sẽ được cấp quyền hoặc hủy theo action cha (Index)
                                ),
                'View'=>array('alias'=>'Xem',
                                'childActions'=>array()
                                ),
                'Update'=>array('alias'=>'Cập nhật',
                                'childActions'=>array()
                                ),
                'Delete'=>array('alias'=>'Xóa',
                                'childActions'=>array()
                                ),
                'Update_status_market_development'=>array('alias'=>'Cập nhật trạng thái PTTT',
                                'childActions'=>array()
                                ),
                )
            ),/* end controller */
        
        'Gasstorecard'=>array(
            'alias'=>'Quản Lý Thẻ Kho - Gasstorecard',
            'actions'=>array(
                'Index'=>array('alias'=>'Xem Danh sách Thẻ Kho - Menu Nhập Xuất',
                                'childActions'=>array()//những actions mà sẽ được cấp quyền hoặc hủy theo action cha (Index)
                                ),
                'Create'=>array('alias'=>'Tạo mới',
                                'childActions'=>array()//những actions mà sẽ được cấp quyền hoặc hủy theo action cha (Index)
                                ),
                'View'=>array('alias'=>'Xem Nhập Xuất',
                                'childActions'=>array()
                                ),
                'Update'=>array('alias'=>'Cập nhật Nhập Xuất - Update',
                                'childActions'=>array()
                                ),
                'Delete'=>array('alias'=>'Xóa',
                                'childActions'=>array()
                                ),
                'Materials_opening_balance'=>array('alias'=>'Nhập Dư Đầu Kỳ Vật Tư',
                                'childActions'=>array()
                                ),
                'View_store_card'=>array('alias'=>'Xem và in ấn thẻ kho - View_store_card',
                                'childActions'=>array()
                                ),
                'View_store_movement_summary'=>array('alias'=>'Xem nhập xuất tồn - view_store_movement_summary',
                                'childActions'=>array()
                                ),
                'UpdateCollectionCustomer'=>array('alias'=>'Cập nhật thu tiền khách hàng',
                                'childActions'=>array()
                                ),
                'UpdateNoteOnly'=>array('alias'=>'Cập nhật ghi chú thẻ kho',
                                'childActions'=>array()
                                ),
                )
            ),/* end controller */
        'Gascustomer'=>array(
            'alias'=>'Quản Lý Khách Hàng Bò, Mối - Gascustomer',
            'actions'=>array(
                'ManageFile'=>array('alias'=>'Add File hồ sơ KH - manageFile',
                                'childActions'=>array()
                                ),
                'App'=>array('alias'=>'Danh sách KH APP',
                                'childActions'=>array()
                                ),
                'HgdMap'=>array('alias'=>'Xem bản đồ vị trí khách hàng HGD - HgdMap',
                                'childActions'=>array()
                                ),
                'Hgd'=>array('alias'=>'List Hộ Gia Đình - Hgd',
                                'childActions'=>array()//những actions mà sẽ được cấp quyền hoặc hủy theo action cha (Index)
                                ),
                'Vip240'=>array('alias'=>'List 240 KH VIP Hộ Gia Đình - Vip240',
                                'childActions'=>array()//những actions mà sẽ được cấp quyền hoặc hủy theo action cha (Index)
                                ),
                'CustomerSaleStoreCard'=>array('alias'=>'List KH chờ duyệt do sale tạo CustomerSaleStoreCard',
                                'childActions'=>array()//những actions mà sẽ được cấp quyền hoặc hủy theo action cha (Index)
                                ),
                'List_username'=>array('alias'=>'Username Khách Hàng',
                                'childActions'=>array()//những actions mà sẽ được cấp quyền hoặc hủy theo action cha (Index)
                                ),
                'List_username_update'=>array('alias'=>'UPDATE Username Khách Hàng',
                                'childActions'=>array()//những actions mà sẽ được cấp quyền hoặc hủy theo action cha (Index)
                                ),
                'Create_sale'=>array('alias'=>'Điều Phối Tạo Sale',
                                'childActions'=>array()//những actions mà sẽ được cấp quyền hoặc hủy theo action cha (Index)
                                ),
                'Customer_store_card'=>array('alias'=>'List KH Bò, Mối - customer_store_card',
                                'childActions'=>array()//những actions mà sẽ được cấp quyền hoặc hủy theo action cha (Index)
                                ),
                'Customer_store_card_bo'=>array('alias'=>'List KH Bò',
                                'childActions'=>array()//những actions mà sẽ được cấp quyền hoặc hủy theo action cha (Index)
                                ),
                'Customer_store_card_moi'=>array('alias'=>'List KH Mối',
                                'childActions'=>array()//những actions mà sẽ được cấp quyền hoặc hủy theo action cha (Index)
                                ),
                'Create_customer_store_card'=>array('alias'=>'Tạo mới - Create_customer_store_card',
                                'childActions'=>array()//những actions mà sẽ được cấp quyền hoặc hủy theo action cha (Index)
                                ),
                'View'=>array('alias'=>'Xem KH  Bò, Mối, Bảo Trì - View',
                                'childActions'=>array()
                                ),
                'Create'=>array('alias'=>'Tạo KH cũ, không sử dụng, nhưng có đoạn ajax trong đó ',
                                'childActions'=>array()
                                ),
                'Index'=>array('alias'=>'List KH cũ',
                                'childActions'=>array()
                                ),
                'Update_customer_store_card'=>array('alias'=>'Cập nhật - Update_customer_store_card',
                                'childActions'=>array()
                                ),
                'Delete'=>array('alias'=>'Xóa',
                                'childActions'=>array()
                                ),
                'Customer_maintain'=>array('alias'=>'Xem Danh sách Khách Hàng Bảo Trì',
                                'childActions'=>array()
                                ),
                'Create_customer_maintain'=>array('alias'=>'Tạo Mới Khách Hàng Bảo Trì',
                                'childActions'=>array()
                                ),
                'Update_customer_maintain'=>array('alias'=>'Cập Nhật Khách Hàng Bảo Trì',
                                'childActions'=>array()
                                ),
                'Export_list_customer_maintain'=>array('alias'=>'Export Khách Hàng Bảo Trì',
                                'childActions'=>array()
                                ),
                'Customer_not_buy'=>array('alias'=>'DS Khách Hàng không lấy hàng - gascustomer/Customer_not_buy',
                                'childActions'=>array()
                                ),
                'ExportStorecard'=>array('alias'=>'Export Excel Khách Hàng Bò Mối - ExportStorecard',
                                'childActions'=>array()
                                ),
                'ExportHgd'=>array('alias'=>'Export Excel Khách Hàng Hộ Gia Đình - ExportHgd',
                                'childActions'=>array()
                                ),
                'AjaxActivate'=>array('alias'=>'Open Account KH - AjaxActivate',
                                'childActions'=>array()
                                ),
                'AjaxDeactivate'=>array('alias'=>'Close Account KH - AjaxDeactivate',
                                'childActions'=>array()
                                ),
                )
            ),/* end controller */
        
        'Gascashbook'=>array(
            'alias'=>'Quản Lý Sổ Quỹ - Gascashbook',
            'actions'=>array(
                'Index'=>array('alias'=>'Xem Danh sách Sổ Quỹ',
                                'childActions'=>array()//những actions mà sẽ được cấp quyền hoặc hủy theo action cha (Index)
                                ),
                'Create'=>array('alias'=>'Tạo mới',
                                'childActions'=>array()//những actions mà sẽ được cấp quyền hoặc hủy theo action cha (Index)
                                ),
                'View'=>array('alias'=>'Xem',
                                'childActions'=>array()
                                ),
                'Update'=>array('alias'=>'Cập nhật',
                                'childActions'=>array()
                                ),
                'Delete'=>array('alias'=>'Xóa',
                                'childActions'=>array()
                                ),
                'ViewAll'=>array('alias'=>'Xem BC tổng toàn hệ thống',
                                'childActions'=>array()
                                ),
                )
            ),/* end controller */
        
        'Gasremain'=>array(
            'alias'=>'Quản Lý Gas Dư - Gasremain',
            'actions'=>array(
                'Index'=>array('alias'=>'Xem Quản Lý Gas Dư',
                                'childActions'=>array()//những actions mà sẽ được cấp quyền hoặc hủy theo action cha (Index)
                                ),
                'Create'=>array('alias'=>'Tạo mới',
                                'childActions'=>array()//những actions mà sẽ được cấp quyền hoặc hủy theo action cha (Index)
                                ),
                'View'=>array('alias'=>'Xem',
                                'childActions'=>array()
                                ),
                'Update'=>array('alias'=>'Cập nhật',
                                'childActions'=>array()
                                ),
                'Delete'=>array('alias'=>'Xóa',
                                'childActions'=>array()
                                ),
                'ExportExcel'=>array('alias'=>'Export Excel List',
                                'childActions'=>array()
                                ),
                )
            ),/* end controller */
        
        'GasOrders'=>array(
            'alias'=>'Quản Lý Đặt Hàng - GasOrders',
            'actions'=>array(
                'Index'=>array('alias'=>'Xem Quản Lý Đặt Hàng',
                                'childActions'=>array()//những actions mà sẽ được cấp quyền hoặc hủy theo action cha (Index)
                                ),
                'Create'=>array('alias'=>'Tạo mới',
                                'childActions'=>array()//những actions mà sẽ được cấp quyền hoặc hủy theo action cha (Index)
                                ),
                'View'=>array('alias'=>'Xem',
                                'childActions'=>array()
                                ),
                'Update'=>array('alias'=>'Cập nhật',
                                'childActions'=>array()
                                ),
                'Delete'=>array('alias'=>'Xóa',
                                'childActions'=>array()
                                ),
                'UpdateCarNumber'=>array('alias'=>'Cập Nhật Số Xe Cho Đặt Hàng',
                                'childActions'=>array()
                                ),
                )
            ),/* end controller */
        
        
        'GasClosingBalanceCustomerMaterials'=>array(
            'alias'=>'Quản Lý Nhập Tồn Đầu Kỳ Vỏ Của Khách Hàng - gasClosingBalanceCustomerMaterials',
            'actions'=>array(
                'Index'=>array('alias'=>'Xem Quản Lý Nhập Tồn Đầu Kỳ Vỏ',
                                'childActions'=>array()//những actions mà sẽ được cấp quyền hoặc hủy theo action cha (Index)
                                ),
                'Create'=>array('alias'=>'Tạo mới',
                                'childActions'=>array()//những actions mà sẽ được cấp quyền hoặc hủy theo action cha (Index)
                                ),
                'View'=>array('alias'=>'Xem',
                                'childActions'=>array()
                                ),
                'Update'=>array('alias'=>'Cập nhật',
                                'childActions'=>array()
                                ),
                'Delete'=>array('alias'=>'Xóa',
                                'childActions'=>array()
                                ),
                )
            ),/* end controller */
        
        
        'Gasmember'=>array(
            'alias'=>'Quản Lý Nhân Sự - Gasmember',
            'actions'=>array(
                'SetAgentForUser'=>array('alias'=>'CẬP NHẬT ĐẠI LÝ CHO NV - SetAgentForUser',
                                'childActions'=>array()//những actions mà sẽ được cấp quyền hoặc hủy theo action cha (Index)
                                ),
                'Employees'=>array('alias'=>'Xem Quản Lý Nhân Sự - Employees',
                                'childActions'=>array()//những actions mà sẽ được cấp quyền hoặc hủy theo action cha (Index)
                                ),
                'Employees_create'=>array('alias'=>'Tạo mới Nhân Sự - Employees_create',
                                'childActions'=>array()//những actions mà sẽ được cấp quyền hoặc hủy theo action cha (Index)
                                ),
                'Employees_update'=>array('alias'=>'Cập Nhật Nhân Sự - Employees_update',
                                'childActions'=>array()//những actions mà sẽ được cấp quyền hoặc hủy theo action cha (Index)
                                ),
                'Employees_view'=>array('alias'=>'Xem Nhân Sự - Employees_view',
                                'childActions'=>array()//những actions mà sẽ được cấp quyền hoặc hủy theo action cha (Index)
                                ),
                'Export_list_employees'=>array('alias'=>'Xuất Excel Nhân Viên - chưa làm',
                                'childActions'=>array()//những actions mà sẽ được cấp quyền hoặc hủy theo action cha (Index)
                                ),
                'AjaxActivate'=>array('alias'=>'Active Nhân Viên',
                                'childActions'=>array()
                                ),
                'AjaxDeactivate'=>array('alias'=>'Deactivate Nhân Viên',
                                'childActions'=>array()
                                ),
                'Index'=>array('alias'=>'Quản Lý Member Login',
                                'childActions'=>array()
                                ),
                'Create'=>array('alias'=>'Tạo mới Member Login',
                                'childActions'=>array()
                                ),
                'View'=>array('alias'=>'Xem Member Login',
                                'childActions'=>array()
                                ),
                'Update'=>array('alias'=>'Cập nhật Member Login',
                                'childActions'=>array()
                                ),
                'Mail_reset_password'=>array('alias'=>'Reset Mật Khẩu Member Login - Mail_reset_password',
                                'childActions'=>array()
                                ),
                'Delete'=>array('alias'=>'Xóa Member Login ',
                                'childActions'=>array()
                                ),
                'CheckFullProfile'=>array('alias'=>'Cập nhật đầy đủ hồ sơ',
                                'childActions'=>array()
                                ),
                'UpdateDetail'=>array('alias'=>'Cập nhật điều xe',
                                'childActions'=>array()
                                ),
                )
            ),/* end controller */
        
        'GasFileScan'=>array(
            'alias'=>'Quản Lý PTTT Files Scan - GasFileScan',
            'actions'=>array(
                'Index'=>array('alias'=>'Danh Sách PTTT Files Scan',
                                'childActions'=>array()//những actions mà sẽ được cấp quyền hoặc hủy theo action cha (Index)
                                ),
                'Create'=>array('alias'=>'Tạo mới',
                                'childActions'=>array()//những actions mà sẽ được cấp quyền hoặc hủy theo action cha (Index)
                                ),
                'View'=>array('alias'=>'Xem',
                                'childActions'=>array()
                                ),
                'Update'=>array('alias'=>'Cập nhật',
                                'childActions'=>array()
                                ),
                'Delete'=>array('alias'=>'Xóa',
                                'childActions'=>array()
                                ),                
                )
            ),/* end controller */        
        'GasSalesFileScan'=>array(
            'alias'=>'Quản Lý Bán Hàng File Scan - GasSalesFileScan',
            'actions'=>array(
                'Index'=>array('alias'=>'Bán Hàng File Scan',
                                'childActions'=>array()//những actions mà sẽ được cấp quyền hoặc hủy theo action cha (Index)
                                ),
                'Create'=>array('alias'=>'Tạo mới',
                                'childActions'=>array()//những actions mà sẽ được cấp quyền hoặc hủy theo action cha (Index)
                                ),
                'View'=>array('alias'=>'Xem',
                                'childActions'=>array()
                                ),
                'Update'=>array('alias'=>'Cập nhật',
                                'childActions'=>array()
                                ),
                'Delete'=>array('alias'=>'Xóa',
                                'childActions'=>array()
                                ),                
                )
            ),/* end controller */       
        
        'GasRemainExport'=>array(
            'alias'=>'Quản Lý Phiếu Giao Nhận Gas Dư - GasRemainExport',
            'actions'=>array(
                'Index'=>array('alias'=>'Quản Lý Phiếu Giao Nhận Gas Dư',
                                'childActions'=>array()//những actions mà sẽ được cấp quyền hoặc hủy theo action cha (Index)
                                ),
                'Create'=>array('alias'=>'Tạo mới',
                                'childActions'=>array()//những actions mà sẽ được cấp quyền hoặc hủy theo action cha (Index)
                                ),
                'View'=>array('alias'=>'Xem',
                                'childActions'=>array()
                                ),
                'Update'=>array('alias'=>'Cập nhật',
                                'childActions'=>array()
                                ),
                'Delete'=>array('alias'=>'Xóa',
                                'childActions'=>array()
                                ),                
                )
            ),/* end controller */        
        
        'Gasmaintain'=>array(
            'alias'=>'Quản Lý Bảo Trì - Gasmaintain',
            'actions'=>array(
                'Index'=>array('alias'=>'Xem Danh sách bảo trì',
                                'childActions'=>array()//những actions mà sẽ được cấp quyền hoặc hủy theo action cha (Index)
                                ),
                'Create'=>array('alias'=>'Tạo mới',
                                'childActions'=>array()//những actions mà sẽ được cấp quyền hoặc hủy theo action cha (Index)
                                ),
                'View'=>array('alias'=>'Xem',
                                'childActions'=>array()
                                ),
                'Update'=>array('alias'=>'Cập nhật',
                                'childActions'=>array()
                                ),
                'Delete'=>array('alias'=>'Xóa',
                                'childActions'=>array()
                                ),
                'Statistic_maintain'=>array('alias'=>'Thống Kê Bảo Trì - /statistic_maintain',
                                'childActions'=>array()
                                ),
                'Statistic_market_development_by_month'=>array('alias'=>'Thống Kê Phát Triển Thị Trường statistic_market_development_by_month',
                                'childActions'=>array()
                                ),
                'Statistic_maintain_export_excel'=>array('alias'=>'Xuất Excel Thống Kê Bảo Trì',
                                'childActions'=>array()
                                ),
                'Statistic_maintain_by_month'=>array('alias'=>'Thống Kê Tiến Độ Bảo Trì - statistic_maintain_by_month',
                                'childActions'=>array()
                                ),
                'Statistic_maintain_by_month_export_excel'=>array('alias'=>'Xuất Excel Tiến Độ Bảo Trì',
                                'childActions'=>array()
                                ),
                'Statistic_market_development_by_month_export_excel'=>array('alias'=>'Xuất Excel Thống Kê Phát Triển Thị Trường',
                                'childActions'=>array()
                                ),
                'Update_status_maintain'=>array('alias'=>'Cập nhật trạng thái bảo trì',
                                'childActions'=>array()
                                ),
                'List_for_monitoring_maintain'=>array('alias'=>'Danh Sách Bảo Trì Cần Xuống Lại',
                                'childActions'=>array()
                                ),
                'Supervision_list_maintain_call_bad'=>array('alias'=>'Danh Sách Bảo Trì Có Cuộc Gọi Xấu',
                                'childActions'=>array()
                                ),
                'Supervision_list_maintain_sell_call_bad'=>array('alias'=>'Danh Sách Bán Hàng Có Cuộc Gọi Xấu',
                                'childActions'=>array()
                                ),
                'Supervision_list_maintain_sell_seri_diff'=>array('alias'=>'Danh Sách Bán Hàng Có Seri Bảo Trì Không Trùng',
                                'childActions'=>array()
                                ),
                'Update_list_for_monitoring_maintain'=>array('alias'=>'Cập Nhật Trạng Thái Bảo Trì Cần Xuống Lại',
                                'childActions'=>array()
                                ),
                'Update_supervision_list_maintain_call_bad'=>array('alias'=>'Cập Nhật Trạng Thái Bảo Trì Có Cuộc Gọi Xấu',
                                'childActions'=>array()
                                ),
                'Update_supervision_list_maintain_sell_call_bad'=>array('alias'=>'Cập Nhật Trạng Thái Bán Hàng Có Cuộc Gọi Xấu',
                                'childActions'=>array()
                                ),

                'Update_supervision_list_maintain_sell_seri_diff'=>array('alias'=>'Cập Nhật Trạng Thái Bán Hàng Có Seri Bảo Trì Không Trùng',
                                'childActions'=>array()
                                ),                
                'Export_list_maintain'=>array('alias'=>'Xuất Excel Danh Sách Bảo Trì',
                                'childActions'=>array()
                                ),                
                'Export_list_for_monitoring_maintain'=>array('alias'=>'Xuất Excel Bảo Trì Cần Xuống Lại',
                                'childActions'=>array()
                                ),                
                'Export_supervision_list_maintain_call_bad'=>array('alias'=>'Xuất Excel Bảo Trì Có Cuộc Gọi Xấu',
                                'childActions'=>array()
                                ),                
                'Export_supervision_list_maintain_sell_call_bad'=>array('alias'=>'Xuất Excel Bán Hàng Có Cuộc Gọi Xấu',
                                'childActions'=>array()
                                ),                
                'Export_supervision_list_maintain_sell_seri_diff'=>array('alias'=>'Xuất Excel Bán Hàng Có Seri Bảo Trì Không Trùng',
                                'childActions'=>array()
                                ),                
                )
            ),/* end controller */        
        
        'Cms'=>array(
            'alias'=>'Quản Lý Tin Tức',
            'actions'=>array(
                'News'=>array('alias'=>'Xem Thông Báo',
                                'childActions'=>array()
                                ),
                'Index'=>array('alias'=>'Danh Sách Tin Tức - cms/index',
                                'childActions'=>array()//những actions mà sẽ được cấp quyền hoặc hủy theo action cha (Index)
                                ),
                'Create'=>array('alias'=>'Tạo mới',
                                'childActions'=>array()//những actions mà sẽ được cấp quyền hoặc hủy theo action cha (Index)
                                ),
                'View'=>array('alias'=>'Xem',
                                'childActions'=>array()
                                ),
                'Update'=>array('alias'=>'Cập nhật',
                                'childActions'=>array()
                                ),
                
                'ListNewIndex'=>array('alias'=>'Thông báo App  - cms/ListNewIndex',
                                'childActions'=>array()//những actions mà sẽ được cấp quyền hoặc hủy theo action cha (Index)
                                ),
                'ListNewCreate'=>array('alias'=>'Tạo mới Thông báo App  - ListNewCreate',
                                'childActions'=>array()//những actions mà sẽ được cấp quyền hoặc hủy theo action cha (Index)
                                ),
                'ListNewView'=>array('alias'=>'Xem Thông báo App - ListNewView',
                                'childActions'=>array()
                                ),
                'ListNewUpdate'=>array('alias'=>'Cập nhật Thông báo App - ListNewUpdate',
                                'childActions'=>array()
                                ),
                
                'Delete'=>array('alias'=>'Xóa',
                                'childActions'=>array()
                                ),                
                'AjaxActivateField'=>array('alias'=>'Activate Hiển Thị Trang Chủ',
                                'childActions'=>array()
                                ),
                'AjaxDeactivateField'=>array('alias'=>'Deactivate Hiển Thị Trang Chủ',
                                'childActions'=>array()
                                ),
                'AjaxActivate'=>array('alias'=>'Activate Tin Tức',
                                'childActions'=>array()
                                ),
                'AjaxDeactivate'=>array('alias'=>'Deactivate Tin Tức',
                                'childActions'=>array()
                                ),
                )
            ),/* end controller */        
                
        'Gasmaterialstype'=>array(
            'alias'=>'Quản Lý Loại Vật Tư - Gasmaterialstype',
            'actions'=>array(
                'Index'=>array('alias'=>'Xem Danh Sách Loại Vật Tư',
                                'childActions'=>array()//những actions mà sẽ được cấp quyền hoặc hủy theo action cha (Index)
                                ),
                'Create'=>array('alias'=>'Tạo mới',
                                'childActions'=>array()//những actions mà sẽ được cấp quyền hoặc hủy theo action cha (Index)
                                ),
                'View'=>array('alias'=>'Xem',
                                'childActions'=>array()
                                ),
                'Update'=>array('alias'=>'Cập nhật',
                                'childActions'=>array()
                                ),
//                'Delete'=>array('alias'=>'Xóa',
//                                'childActions'=>array()
//                                ),
                )
            ),/* end controller */    
        
        'Gasmaterials'=>array(
            'alias'=>'Quản Lý Vật Tư - Gasmaterials',
            'actions'=>array(
                'Index'=>array('alias'=>'Xem Danh Sách Vật Tư',
                                'childActions'=>array()//những actions mà sẽ được cấp quyền hoặc hủy theo action cha (Index)
                                ),
                'Create'=>array('alias'=>'Tạo mới',
                                'childActions'=>array()//những actions mà sẽ được cấp quyền hoặc hủy theo action cha (Index)
                                ),
                'View'=>array('alias'=>'Xem',
                                'childActions'=>array()
                                ),
                'Update'=>array('alias'=>'Cập nhật',
                                'childActions'=>array()
                                ),
                'AjaxActivate'=>array('alias'=>'AjaxActivate',
                                'childActions'=>array()
                                ),
                'AjaxDeactivate'=>array('alias'=>'AjaxDeactivate',
                                'childActions'=>array()
                                ),
//                'Delete'=>array('alias'=>'Xóa',
//                                'childActions'=>array()
//                                ),
                )
            ),/* end controller */    
        
        'Gascosts'=>array(
            'alias'=>'Quản Lý Chi Phí - Gascosts',
            'actions'=>array(
                'Index'=>array('alias'=>'Xem Danh Sách Chi Phí',
                                'childActions'=>array()//những actions mà sẽ được cấp quyền hoặc hủy theo action cha (Index)
                                ),
                'Create'=>array('alias'=>'Tạo mới',
                                'childActions'=>array()//những actions mà sẽ được cấp quyền hoặc hủy theo action cha (Index)
                                ),
                'View'=>array('alias'=>'Xem',
                                'childActions'=>array()
                                ),
                'Update'=>array('alias'=>'Cập nhật',
                                'childActions'=>array()
                                ),
//                'Delete'=>array('alias'=>'Xóa',
//                                'childActions'=>array()
//                                ),
                )
            ),/* end controller */
        
        'Gasagent'=>array(
            'alias'=>'Quản Lý Đại Lý - Gasagent',
            'actions'=>array(
                'MonitorAgent'=>array('alias'=>'Giám Sát Máy Tính Đại Lý',
                                'childActions'=>array()//những actions mà sẽ được cấp quyền hoặc hủy theo action cha (Index)
                                ),
                'Index'=>array('alias'=>'Xem Danh Sách Đại Lý',
                                'childActions'=>array()//những actions mà sẽ được cấp quyền hoặc hủy theo action cha (Index)
                                ),
                'Create'=>array('alias'=>'Tạo mới',
                                'childActions'=>array()//những actions mà sẽ được cấp quyền hoặc hủy theo action cha (Index)
                                ),
                'View'=>array('alias'=>'Xem',
                                'childActions'=>array()
                                ),
                'Update'=>array('alias'=>'Cập nhật',
                                'childActions'=>array()
                                ),
                'Add_customer_of_agent'=>array('alias'=>'Thêm Khách Hàng Đại Lý',
                                'childActions'=>array()
                                ),
                'Delete_customer_agent'=>array('alias'=>'Xóa Khách Hàng Đại Lý',
                                'childActions'=>array()
                                ),
//                'Delete'=>array('alias'=>'Xóa',
//                                'childActions'=>array()
//                                ),
                )
            ),/* end controller */    
        
        'Gastypepay'=>array(
            'alias'=>'Quản Lý Loại Thanh Toán - Gastypepay',
            'actions'=>array(
                'Index'=>array('alias'=>'Xem Danh Sách Loại Thanh Toán',
                                'childActions'=>array()//những actions mà sẽ được cấp quyền hoặc hủy theo action cha (Index)
                                ),
                'Create'=>array('alias'=>'Tạo mới',
                                'childActions'=>array()//những actions mà sẽ được cấp quyền hoặc hủy theo action cha (Index)
                                ),
                'View'=>array('alias'=>'Xem',
                                'childActions'=>array()
                                ),
                'Update'=>array('alias'=>'Cập nhật',
                                'childActions'=>array()
                                ),
//                'Delete'=>array('alias'=>'Xóa',
//                                'childActions'=>array()
//                                ),
                )
            ),/* end controller */    
        
        'Gasmasterlookup'=>array(
            'alias'=>'Quản Lý Loại Thu Chi - Gasmasterlookup',
            'actions'=>array(
                'Index'=>array('alias'=>'Xem Danh Sách Loại Thu Chi',
                                'childActions'=>array()//những actions mà sẽ được cấp quyền hoặc hủy theo action cha (Index)
                                ),
                'Create'=>array('alias'=>'Tạo mới',
                                'childActions'=>array()//những actions mà sẽ được cấp quyền hoặc hủy theo action cha (Index)
                                ),
                'View'=>array('alias'=>'Xem',
                                'childActions'=>array()
                                ),
                'Update'=>array('alias'=>'Cập nhật',
                                'childActions'=>array()
                                ),
//                'Delete'=>array('alias'=>'Xóa',
//                                'childActions'=>array()
//                                ),
                )
            ),/* end controller */    
        'Gastargetmonthly'=>array(
            'alias'=>'Thiết lập target cho đại lý và nhân viên sale - Gastargetmonthly',
            'actions'=>array(
                'DailyCcs'=>array('alias'=>'Báo cáo daily CCS - dailyCcs',
                                'childActions'=>array()
                                ),
                'SetupCcs'=>array('alias'=>'Setup target 2019 - setupCcs',
                                'childActions'=>array()
                                ),
                'Ccs'=>array('alias'=>'BC target 2019 - ccs',
                                'childActions'=>array()
                                ),
                'Index'=>array('alias'=>'Thiết lập target cho đại lý và nhân viên sale',
                                'childActions'=>array()//những actions mà sẽ được cấp quyền hoặc hủy theo action cha (Index)
                                ),
                'CcsStatistic'=>array('alias'=>'Báo cáo đạt chỉ tiêu',
                                'childActions'=>array()//những actions mà sẽ được cấp quyền hoặc hủy theo action cha (Index)
                                ),
                )
            ),/* end controller */    
        
        'Gasprovince'=>array(
            'alias'=>'Quản Lý Tỉnh - Gasprovince',
            'actions'=>array(
                'Index'=>array('alias'=>'Danh Sách Tỉnh',
                                'childActions'=>array()//những actions mà sẽ được cấp quyền hoặc hủy theo action cha (Index)
                                ),
                'Create'=>array('alias'=>'Tạo mới',
                                'childActions'=>array()//những actions mà sẽ được cấp quyền hoặc hủy theo action cha (Index)
                                ),
                'View'=>array('alias'=>'Xem',
                                'childActions'=>array()
                                ),
                'Update'=>array('alias'=>'Cập nhật',
                                'childActions'=>array()
                                ),
//                'Delete'=>array('alias'=>'Xóa',
//                                'childActions'=>array()
//                                ),
                )
            ),/* end controller */            
        
        'Gasdistrict'=>array(
            'alias'=>'Quản Lý Quận/Huyện - Gasdistrict',
            'actions'=>array(
                'Index'=>array('alias'=>'Danh Sách Quận/Huyện',
                                'childActions'=>array()//những actions mà sẽ được cấp quyền hoặc hủy theo action cha (Index)
                                ),
                'Create'=>array('alias'=>'Tạo mới',
                                'childActions'=>array()//những actions mà sẽ được cấp quyền hoặc hủy theo action cha (Index)
                                ),
                'View'=>array('alias'=>'Xem',
                                'childActions'=>array()
                                ),
                'Update'=>array('alias'=>'Cập nhật',
                                'childActions'=>array()
                                ),
//                'Delete'=>array('alias'=>'Xóa',
//                                'childActions'=>array()
//                                ),
                )
            ),/* end controller */            
        
        'Gasstreet'=>array(
            'alias'=>'Quản Lý Tên Đường - Gasstreet',
            'actions'=>array(
                'Index'=>array('alias'=>'Danh Sách Tên Đường',
                                'childActions'=>array()//những actions mà sẽ được cấp quyền hoặc hủy theo action cha (Index)
                                ),
                'Create'=>array('alias'=>'Tạo mới',
                                'childActions'=>array()//những actions mà sẽ được cấp quyền hoặc hủy theo action cha (Index)
                                ),
                'View'=>array('alias'=>'Xem',
                                'childActions'=>array()
                                ),
                'Update'=>array('alias'=>'Cập nhật',
                                'childActions'=>array()
                                ),
//                'Delete'=>array('alias'=>'Xóa',
//                                'childActions'=>array()
//                                ),
                )
            ),/* end controller */            
        
        'Gasward'=>array(
            'alias'=>'Quản Lý Phường Xã - Gasward',
            'actions'=>array(
                'Index'=>array('alias'=>'Danh Sách Phường Xã',
                                'childActions'=>array()//những actions mà sẽ được cấp quyền hoặc hủy theo action cha (Index)
                                ),
                'Create'=>array('alias'=>'Tạo mới',
                                'childActions'=>array()//những actions mà sẽ được cấp quyền hoặc hủy theo action cha (Index)
                                ),
                'View'=>array('alias'=>'Xem',
                                'childActions'=>array()
                                ),
                'Update'=>array('alias'=>'Cập nhật',
                                'childActions'=>array()
                                ),
//                'Delete'=>array('alias'=>'Xóa',
//                                'childActions'=>array()
//                                ),
                )
            ),/* end controller */            
        
        
        'Gasdailycash'=>array(
            'alias'=>'Daily Cash',
            'actions'=>array(
                'Export_account_receivables_expired'=>array('alias'=>'Export Nợ Quá Hạn',
                                'childActions'=>array()//những actions mà sẽ được cấp quyền hoặc hủy theo action cha (Index)
                                ),
                'Export_account_receivables'=>array('alias'=>'Export Phải Thu KH',
                                'childActions'=>array()
                                ),
                'Export_target'=>array('alias'=>'Export Target',
                                'childActions'=>array()
                                ),
                'Export_dailyplan'=>array('alias'=>'Export Daily',
                                'childActions'=>array()
                                ),
                'Export_dailycash'=>array('alias'=>'Export thu tiền',
                                'childActions'=>array()
                                ),
                )
            ),/* end controller */ 
        
        
        'GasWeekSetup'=>array(
            'alias'=>'Quản Lý Tuần Của Tháng - GasWeekSetup',
            'actions'=>array(
                'Index'=>array('alias'=>'Xem Quản Lý Tuần Của Tháng',
                                'childActions'=>array()//những actions mà sẽ được cấp quyền hoặc hủy theo action cha (Index)
                                ),
                'Create'=>array('alias'=>'Tạo mới',
                                'childActions'=>array()//những actions mà sẽ được cấp quyền hoặc hủy theo action cha (Index)
                                ),
                'View'=>array('alias'=>'Xem',
                                'childActions'=>array()
                                ),
                'Update'=>array('alias'=>'Cập nhật',
                                'childActions'=>array()
                                ),
                'Delete'=>array('alias'=>'Xóa',
                                'childActions'=>array()
                                ),
                )
        ),/* end controller */        
        
        'Lognb'=>array(
            'alias'=>'View Log - lognb',
            'actions'=>array(
                'Index'=>array('alias'=>'Xem Log',
                                'childActions'=>array()//những actions mà sẽ được cấp quyền hoặc hủy theo action cha (Index)
                                ),
                )
        ),/* end controller */
        
        'Setting'=>array(
            'alias'=>'Setting - setting',
            'actions'=>array(
                'Index'=>array('alias'=>'Index',
                                'childActions'=>array()//những actions mà sẽ được cấp quyền hoặc hủy theo action cha. EX 'childActions'=>array('Approve','Reject')
                                ),
                )
        ),/* end controller */
        
        /******** May 13, 2014 ANH DUNG ***********/
        
        
    );
    
   
    
}
