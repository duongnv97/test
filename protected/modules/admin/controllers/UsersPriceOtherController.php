<?php

class UsersPriceOtherController extends AdminController 
{
    public $pluralTitle = "Giá Khách Hàng";
    public $singleTitle = "Giá Khách Hàng";

    public function actionIndex()
    {
        $this->pageTitle = 'Giá Khách Hàng';
        try{ 
            $model=new UsersPriceOther('search');
            $model->unsetAttributes();  // clear any default values
//            $model->cronCopyRecord('2019-09-30');
            if(isset($_GET['UsersPriceOther']))
            {
                $model->attributes=$_GET['UsersPriceOther'];
            }
            $this->render('index',array(
                    'model'=>$model, 'actions' => $this->listActionsCanAccess,
            ));
            
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }

    public function actionCreate()
    {
        $this->pageTitle = 'Tạo Mới Giá';
        try{
            $model=new UsersPriceOther();
            $model->unsetAttributes();  // clear any default values
            $string = '';
            if(isset($_POST['UsersPriceOther']))
            {
                $model->attributes=$_POST['UsersPriceOther'];
                $model->validate();
                if(!$model->hasErrors()){
                    $string = $model->saveCreate();
                    if($string){
                        Yii::app()->user->setFlash( MyFormat::ERROR_UPDATE, 'Thêm mới thành công.<br>'.$string);
                    }else{
                        Yii::app()->user->setFlash( MyFormat::SUCCESS_UPDATE, 'Thêm mới thành công.' );
                    }
                    $this->redirect(array('create'));
                }
            }

            $this->render('create',array(
                    'model'=>$model, 'actions' => $this->listActionsCanAccess,
            ));
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }

    public function actionUpdate($id)
    {
        $this->pageTitle = 'Cập Nhật Giá';
        try{
            $model=new UsersPriceOther('update');
            
            $model=$this->loadModel($id);
            $model->loadDataUpdate($id);
            if(isset($_POST['UsersPriceOther']))
            {
                $model->validate();
                if ($model->saveUpdate()){
                     Yii::app()->user->setFlash( MyFormat::SUCCESS_UPDATE, "Cập nhật thành công." );
                }
                else {
                    Yii::app()->user->setFlash( MyFormat::ERROR_UPDATE, "Cập nhật thất bại.<br> Thông tin thiếu hoặc trùng, vui lòng kiểm tra lại!" );
                }
                $this->redirect(array('update', 'id'=>$model->id));
            }
            
            $this->render('update',array(
                    'model'=>$model, 'actions' => $this->listActionsCanAccess,
            ));
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }

    public function actionView($id)
    {
        $this->pageTitle = 'Thông Tin Chiến Dịch';
        try{
            $this->render('view',array(
                    'model'=>$this->loadModel($id), 'actions' => $this->listActionsCanAccess,
        ));
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }

    }

    public function actionDelete($id)
    {
        try{
            if(Yii::app()->request->isPostRequest)
            {
                // we only allow deletion via POST request
                if($model = $this->loadModel($id))
                {
                    if($model->delete())
                        Yii::log("Uid: " .Yii::app()->user->id. " Delete record ".  print_r($model->attributes, true), 'info');
                }

                // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
                if(!isset($_GET['ajax']))
                        $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('index'));
            }
            else
                {
                    Yii::log("Uid: " .Yii::app()->user->id. " Invalid request. Please do not repeat this request again.");
                    throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
                }	
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }


    public function loadModel($id){
        try{
            $model = new UsersPriceOther;
            $model = $model->findByPk($id);
            if($model===null)
            {
                Yii::log("The requested page does not exist.");
                throw new CHttpException(404,'The requested page does not exist.');
            }			
            return $model;
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }
}
