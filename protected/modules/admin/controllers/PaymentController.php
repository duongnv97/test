<?php
class PaymentController extends AdminController
{ 
    public $pluralTitle = "Payment";
    public $singleTitle = "Payment";

//    [TASKTIMER]
    /** @Author: NamNH Oct 20, 2018
     *  @Todo: confirm order by payment
     **/
    public function actionConfirm($orderNumber,$price){
        $mSell = Sell::model()->findByPk($orderNumber);
        $mForecast = new Forecast();
        if(GasCheck::isServerLive()){
            echo '<pre>';
            print_r('Không áp dụng');
            echo '</pre>';
            die;
        }
        if(!empty($mSell)){
            $mSell->payment_type    = Forecast::TYPE_BANK;
            foreach($mSell->rDetail as $key => $mDetail){
//                remove gold timer discount
                if($mDetail->v1_discount_id == Forecast::TYPE_GOLD_TIME){
                    if(!empty($mSell->rTransactionHistory)){
                        $mSell->rTransactionHistory->promotion_amount -= $mDetail->v1_discount_amount;
                        $mSell->rTransactionHistory->grand_total      += $mDetail->v1_discount_amount;
                    }
                    $mDetail->v1_discount_amount = 0;
                }
                $mSell->getDiscountApp(Forecast::TYPE_PAYMENT_FIRST_ORDER,$mDetail);
                //        update transactionHistory
                if(!empty($mSell->rTransactionHistory)){
                    if(isset($mSell->rTransactionHistory->promotion_amount)){
                        $mSell->rTransactionHistory->promotion_amount += $mDetail->v1_discount_amount;
                    }else{
                        $mSell->rTransactionHistory->promotion_amount = $mDetail->v1_discount_amount;
                    }
                    $mSell->rTransactionHistory->grand_total -= $mDetail->v1_discount_amount;
                }
                $mDetail->update();
            }
            //        update transactionHistory
            $mSell->rTransactionHistory->payment_type    = Forecast::TYPE_BANK;
            $mSell->rTransactionHistory->update();
            $mSell->update();
            echo '<pre>';
            print_r('ok');
            echo '</pre>';
            die;
        }
    }
    
}
