<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of BranchCompanyController
 *
 * @author nhh
 */
class BranchCompanyController extends AdminController {

    public function actionIndex() {
        $this->pageTitle = 'Chi Nhánh/ Địa Điểm';
        $this->excelBranch();
        try {
            $model = new BranchCompany();
            
            $model->unsetAttributes();  // clear any default values
            if (isset($_GET['BranchCompany'])) {
                $model->attributes = $_GET['BranchCompany'];
                $model->role_id = ROLE_BRANCH_COMPANY;
            }

            $this->render('index', array(
                'model' => $model, 'actions' => $this->listActionsCanAccess,
            ));
        } catch (Exception $exc) {
            GasCheck::CatchAllExeptiong($exc);
        }
    }

    public function actionView($id) {
        if(isset($_GET['ajax']) && $_GET['ajax'] == 1){
            $this->layout = 'ajax';
        }
        try {

            $this->render('view', array(
                'model' => $this->loadModel($id), 'actions' => $this->listActionsCanAccess,
            ));
        } catch (Exception $e) {
            Yii::log("Uid: " . Yii::app()->user->id . "Exception " . $e->getMessage(), 'error');
            $code = 404;
            if (isset($e->statusCode))
                $code = $e->statusCode;
            if ($e->getCode())
                $code = $e->getCode();
            throw new CHttpException($code, $e->getMessage());
        }
    }

    public function actionCreate() {
        $this->pageTitle = 'Tạo Mới Chi Nhánh/ Địa Điểm';
       
        $isAjax=0;
        if(isset($_GET['ajax']) && $_GET['ajax'] == 1){
            $this->layout = 'ajax';
            $isAjax =1;
        }
       
        try {
            $model = new BranchCompany('create_branch_company');
            $model->setDefaultAttributes();
            if (isset($_POST['BranchCompany'])) {
                $this->setBranchParentId();
                $model->attributes = $_POST['BranchCompany'];                
                $model->role_id = ROLE_BRANCH_COMPANY;
                $model->validate();
                if (!$model->hasErrors()) {
                    $model->save();                     
                    $aParamsView = array('view', 'id' => $model->id);
                    if($isAjax) {
                       $aParamsView['ajax']=$isAjax;
                    }
                    $this->redirect($aParamsView);
                }
            }
            $this->render('create', array(
                'model' => $model, 'actions' => $this->listActionsCanAccess,
            ));
        } catch (Exception $exc) {
            GasCheck::CatchAllExeptiong($exc);
        }
    }

    public function actionUpdate($id) {

        try {

            $model = $this->loadModel($id);
            $this->pageTitle = 'Cập Nhật Chi Nhánh/ Địa Điểm - ' . $model->first_name;
            if ($model->role_id != ROLE_BRANCH_COMPANY)
                $this->redirect(array('index'));

            if (isset($_POST['BranchCompany'])) {
                $this->setBranchParentId();
                $model->attributes = $_POST['BranchCompany'];                
                $model->validate();
                if (!$model->hasErrors()) {
                    $model->update();
                    $this->redirect(array('view', 'id' => $model->id));
                }
            }

            $this->render('update', array(
                'model' => $model, 'actions' => $this->listActionsCanAccess,
            ));
        } catch (Exception $exc) {
            GasCheck::CatchAllExeptiong($exc);
        }
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id) {

        try {
            if (Yii::app()->request->isPostRequest) {
                // we only allow deletion via POST request 
                if ($model = $this->loadModel($id)) {
                    $model->status  = STATUS_WAIT_ACTIVE;
                    $model->role_id = ROLE_CUSTOMER;
                    if ($model->delete()) {
                        Yii::log("Uid: " . Yii::app()->user->id . " Delete record " . print_r($model->attributes, true), 'info');
                    }
                }

                // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
                if (!isset($_GET['ajax']))
                    $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('index'));
            }
            else {
                Yii::log("Uid: " . Yii::app()->user->id . " Invalid request. Please do not repeat this request again.");
                throw new CHttpException(400, 'Invalid request. Please do not repeat this request again.');
            }
        } catch (Exception $exc) {
            GasCheck::CatchAllExeptiong($exc);
        }
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer the ID of the model to be loaded
     */
    public function loadModel($id) {
        try {
            $model = BranchCompany::model()->findByPk($id);

            if ($model === null) {
                Yii::log("The requested page does not exist.");
                throw new CHttpException(404, 'The requested page does not exist.');
            }
            return $model;
        } catch (Exception $exc) {
            GasCheck::CatchAllExeptiong($exc);
        }
    }

    /** @Author: HaoNH August 15, 2019
     *  @Todo:export excel branch company
     * */
    public function excelBranch() {
        try {

            if (isset($_GET['to_excel']) && isset($_SESSION['data-excel-branch'])) {
                ini_set('memory_limit', '1500M'); // chưa cần chạy đoạn này, đưa memory_limit lên 500 có thể export dc khoảng 5000 row - chưa test
                $mToExcel1 = new ToExcelList();
                $mToExcel1->exportBranchCompany();
            }
        } catch (Exception $exc) {
            GasCheck::CatchAllExeptiong($exc);
        }
    }
    
    /** @Author: HaoNH Aug 29, 2019
     *  @Todo:set giá trị mặc định branch_parent_id nếu loại là chi nhánh
     *  @Param:
     **/
   public function setBranchParentId(){
       if($_POST['BranchCompany']['gender'] == BranchCompany::IS_BRANCH){
           $_POST['BranchCompany']['area_code_id'] = 0;          
       }
       
   }


}
