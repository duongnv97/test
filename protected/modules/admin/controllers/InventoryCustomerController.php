<?php

class InventoryCustomerController extends AdminController 
{
    public $pluralTitle = 'Tồn đầu kỳ KH ngày 01-09-2017';
    public $singleTitle = 'Tồn đầu kỳ KH ngày 01-09-2017';
    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id)
    {
        $this->pageTitle = 'tồn đầu kỳ khách hàng bò mối ';
        try{
        $model = $this->loadModel($id);
        $this->overridePageTitle($model);
        $this->render('view',array(
                'model'=> $model, 'actions' => $this->listActionsCanAccess,
        ));
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate()
    {
        $this->pageTitle = 'tồn đầu kỳ khách hàng bò mối';
        try{
        $model=new InventoryCustomer('create');
        $model->type  = isset($_GET['type']) ? $_GET['type'] : InventoryCustomer::TYPE_INVENTORY;
        if(isset($_POST['InventoryCustomer'])){
            $model->attributes=$_POST['InventoryCustomer'];
//            $model->validate();
            $model->runSqlInsert();
            if(!$model->hasErrors()){
                Yii::app()->user->setFlash( MyFormat::SUCCESS_UPDATE, "Thêm mới thành công." );
                $this->redirect(array('create', 'type'=>$model->type));
            }else{
                Yii::app()->user->setFlash( MyFormat::ERROR_UPDATE, HandleLabel::FortmatErrorsModel($model->getErrors()));
            }
        }
        $this->overridePageTitle($model);
        $this->render('create',array(
                'model'=>$model, 'actions' => $this->listActionsCanAccess,
        ));
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }
    
    public function overridePageTitle($model) {
        if($model->type == InventoryCustomer::TYPE_CLEAN_INVENTORY){
            $this->pageTitle = 'Xóa công nợ khách hàng';
        }
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id)
    {
        $this->pageTitle = 'tồn đầu kỳ khách hàng bò mối';
        try{
        $model=$this->loadModel($id);
        if(!$model->canUpdate()){// add Now 07, 2016
            Yii::app()->user->setFlash( MyFormat::ERROR_UPDATE, "Yêu cầu không hợp lệ" );
            $this->redirect(array('index'));
        }
        $model->scenario = 'update';
        if(isset($_POST['InventoryCustomer']))
        {
            $model->attributes=$_POST['InventoryCustomer'];
                    // $this->redirect(array('view','id'=>$model->id));
            $model->validate();
            if(!$model->hasErrors()){
                $model->save();
                Yii::app()->user->setFlash( MyFormat::SUCCESS_UPDATE, "Cập nhật thành công." );
                $this->redirect(array('update','id'=>$model->id));
            }else{
                Yii::app()->user->setFlash( MyFormat::ERROR_UPDATE, HandleLabel::FortmatErrorsModel($model->getErrors()));
            }
        }
        $this->overridePageTitle($model);
        $this->render('update',array(
                'model'=>$model, 'actions' => $this->listActionsCanAccess,
        ));
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id)
    {
        try{
        if(Yii::app()->request->isPostRequest){
            // we only allow deletion via POST request
            if($model = $this->loadModel($id))
            {
                if($model->delete())
                    Yii::log("Uid: " .Yii::app()->user->id. " Delete record ".  print_r($model->attributes, true), 'info');
            }

            // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
            if(!isset($_GET['ajax']))
                    $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('index'));
        }
        else
        {
            Yii::log("Uid: " .Yii::app()->user->id. " Invalid request. Please do not repeat this request again.");
            throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
        }	
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }

    /**
     * Manages all models.
     */
    public function actionIndex()
    {
        $this->limitCallCenter();
        $this->pageTitle = 'Danh Sách ';
        try{
        $model=new InventoryCustomer('search');
        $model->unsetAttributes();  // clear any default values
        $model->type = isset($_GET['type']) ? $_GET['type'] : InventoryCustomer::TYPE_INVENTORY;
        
        if(isset($_GET['InventoryCustomer']))
            $model->attributes=$_GET['InventoryCustomer'];

        $this->render('index',array(
                'model'=>$model, 'actions' => $this->listActionsCanAccess,
        ));
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
    * If the data model is not found, an HTTP exception will be raised.
     * @param integer the ID of the model to be loaded
     */
    public function loadModel($id)
    {
        try{
        $model=InventoryCustomer::model()->findByPk($id);
        if($model===null)
        {
            Yii::log("The requested page does not exist.");
            throw new CHttpException(404,'The requested page does not exist.');
        }			
        return $model;
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }
    
    /** @Author: ANH DUNG Jun 04, 2017
     * @Todo: báo cáo công nợ tiền KH
     * inventoryCustomer/reportDebitCash
     */
    public function actionReportDebitCash()
    {
        $this->limitCallCenter();
        $this->totExcel();
        $this->toEmail();
        $this->pageTitle = 'BC công nợ tiền';
        try{
        $aData = [];
        $cProvince      = Yii::app()->user->province_id;
        $cUid           = MyFormat::getCurrentUid();
        $aUidVungTau    = [GasConst::UID_HUE_LT];
        $aProvinceAuto  = [GasProvince::TP_HCM, GasProvince::TINH_BINH_DUONG, GasProvince::TINH_DONG_NAI, GasProvince::TINH_LONG_AN, GasProvince::TINH_TAY_NINH];
        $this->handleToUpdateReportDebit();
        $this->handleToUpdateLockCustomer();
        
        $model=new InventoryCustomer('search');
        $model->unsetAttributes();  // clear any default values
        $model->date_from   = '01-'.date('m-Y');
        $model->date_to     = date('d-m-Y');
        if(isset($_GET['date_from'])){
            $model->date_from = $_GET['date_from'];
        }
        if(isset($_GET['date_to'])){
            $model->date_to = $_GET['date_to'];
        }
        
        if(in_array($cProvince, $aProvinceAuto) && !in_array($cUid, $aUidVungTau)){
//            $model->province_id = [GasProvince::TP_HCM, GasProvince::TINH_BINH_DUONG, GasProvince::TINH_DONG_NAI, GasProvince::TINH_LONG_AN, GasProvince::TINH_TAY_NINH];
        }
        if(isset($_GET['InventoryCustomer'])){
            $model->attributes=$_GET['InventoryCustomer'];
            $type = isset($_GET['type']) ? $_GET['type'] : InventoryCustomer::VIEW_GENERAL;
            switch ($type) {
                case InventoryCustomer::VIEW_GENERAL:
                    $aData = $model->reportDebitCash();
//                    if(isset($aData['CUSTOMER_ID']) && $model->canToExcelDebitCash()){
//                        ToExcel::ReportDebitCash($aData, $model);
//                    }
                    break;
                case InventoryCustomer::VIEW_DETAIL:
                    $aData = $model->reportDebitCashDetail();
                    break;
                case InventoryCustomer::VIEW_COMPARE:
                    $aData = $model->reportDebitCashCompare();
                    break;

                default:
                    throw new Exception('Yêu cầu không hợp lệ');
                    break;
            }
        }

        $this->render('report/DebitCash',array(
            'model'=>$model, 'actions' => $this->listActionsCanAccess,
            'aData' => $aData
        ));
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }
    
    public function totExcel() {
        try{
            if(isset($_GET['to_excel']) && isset($_SESSION['data-excel']) && isset($_GET['type'])){
                $aData = $_SESSION['data-excel'];
                $model = $_SESSION['data-model'];
                if($_GET['type'] == InventoryCustomer::VIEW_GENERAL){
                    ToExcel::ReportDebitCash($aData, $model);
                }elseif($_GET['type'] == InventoryCustomer::VIEW_COMPARE){
                    ToExcel::ReportDebitCashCompare($aData, $model);
                }elseif($_GET['type'] == InventoryCustomer::VIEW_DETAIL){
                    ToExcel::ReportDebitCashDetail($aData, $model);
                }
            }
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }
    
    /** @Author: ANH DUNG Jun 04, 2017
     * @Todo: báo cáo công nợ tiền KH
     * inventoryCustomer/reportDebitVo
     */
    public function actionReportDebitVo()
    {
        error_reporting(1);
        $this->limitCallCenter();
        $this->pageTitle = 'Báo cáo công nợ vỏ';
        try{
        $aData = [];
        $model=new InventoryCustomer('search');
        $model->unsetAttributes();  // clear any default values
        $model->date_from   = '01-'.date('m-Y');
        $model->date_to     = date('d-m-Y');
        //LOC005
        if(isset($_GET['InventoryCustomer'])){
            $model->attributes=$_GET['InventoryCustomer'];
            $type = isset($_GET['type']) ? $_GET['type'] : InventoryCustomer::VIEW_GENERAL;
            switch ($type) {
                case InventoryCustomer::VIEW_GENERAL:
                    $aData = $model->reportDebitVo();
                    break;
                case InventoryCustomer::VIEW_DETAIL:
                    $sellReport = new SellReport();
                    $aData = $sellReport->getReportDebitVoDetail($model);
//                    $aData = $model->getReportDebitVoDetail();
                    break;
                default:
                    throw new Exception('Yêu cầu không hợp lệ');
                    break;
            }
            $_SESSION['DataDebitVo'] = $aData;
        }
        $this->exportExcel($model);

        $this->render('report/DebitVo',array(
            'model'=>$model, 'actions' => $this->listActionsCanAccess,
            'aData' => $aData
        ));
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }
    
    /** @Author: ANH DUNG Sep 15, 2017
     *  @Todo: limit role call center
     */
    public function limitCallCenter() {
        return ; // Feb1318 Open for all
        $cRole = MyFormat::getCurrentRoleId();
        $cUid = MyFormat::getCurrentUid();
        if($cRole == ROLE_CALL_CENTER){
            $aUidAllow = [GasConst::UID_NGOC_PT, GasTickets::DIEU_PHOI_HIEU_TM];
            if( !in_array($cUid, $aUidAllow) ){
                $url = Yii::app()->createAbsoluteUrl('/');
                $this->redirect($url);
            }
        }
    }
    /** @Author: ANH DUNG Jul 12, 2018
     *  @Todo: Thúc update lai loai KH, bị sai công nợ khi change loại KH bò mối
     **/
    public function handleToUpdateReportDebit() {
        if(isset($_GET['UpdateReportDebit'])){
            CronUpdate::updateTypeCustomerReportDebitCash($_GET['customer_id']);
            Yii::app()->user->setFlash(MyFormat::SUCCESS_UPDATE, 'Cập nhật lại loại KH cho BC công nợ tổng quát thành công');
            $this->redirect(array('reportDebitCash','type' => InventoryCustomer::VIEW_DETAIL));
        }
    }
    /** @Author: ANH DUNG Jul 12, 2018
     *  @Todo: Thúc set chặn hàng KH
     **/
    public function handleToUpdateLockCustomer() {
        if(isset($_GET['UpdateLockCustomer'])){
            $status_lock    = $_GET['UpdateLockCustomer'];
            $flash_message  = ($status_lock == InventoryCustomer::UPDATE_LOCK_CUSTOMER) ? 'Chặn hàng thành công: ' : 'Bỏ chặn hàng thành công: ';
            $mCustomer      = Users::model()->findByPk($_GET['customer_id']);
            $mCustomer->modelOld    = clone $mCustomer;
//            $mCustomer->channel_id  = Users::CHAN_HANG;
            $mCustomer->channel_id  = ($status_lock == InventoryCustomer::UPDATE_LOCK_CUSTOMER) ? Users::CHAN_HANG : Users::CON_LAY_HANG;
            $mCustomer->update(['channel_id']);
            $sendEmail = new SendEmail();
            $sendEmail->notifyLockCustomer($mCustomer);
            Yii::app()->user->setFlash(MyFormat::SUCCESS_UPDATE, $flash_message.$mCustomer->getFullName());
            $this->redirect(array('reportDebitCash','type' => InventoryCustomer::VIEW_DETAIL));
        }
    }
    /** @Author: Pham Thanh Nghia 2018 
     **/
    public function exportExcel($model){
        try{
            if(!$model->canToExcel()){
                return ;
            }
            if(isset($_SESSION['DataDebitVo']) && isset($_SESSION['DataDebitVo']['ID_VO']) && isset($_GET['ExportExcel'])){
                ini_set('memory_limit','2000M');// chưa cần chạy đoạn này, đưa memory_limit lên 500 có thể export dc khoảng 5000 row - chưa test
                ToExcel::ReportDebitVo($model);
            }
//        $this->redirect(array('index'));
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }
   
    /** @Author: Nguyen Khanh Toan 2018
     *  @Todo: send email đối chiếu công nợ to KH
     **/
    public function toEmail() {
        try{
            if(isset($_GET['to_send_email']) && isset($_SESSION['data-excel']) && isset($_GET['type'])){
                $aData      = $_SESSION['data-excel'];
                $modelEmail = $_SESSION['data-model'];
                $aData['modelEmail']    = $modelEmail; 
                $mCustomer              = $aData['mCustomer'];
                if($_GET['type'] == InventoryCustomer::VIEW_COMPARE){
                    $mUsersEmail    = new UsersEmail();
                    $mUsersEmail    = $mUsersEmail->getByTypeAndUser($mCustomer->id, UsersEmail::TYPE_ANNOUNCE_DEBIT);
                    if(empty($mUsersEmail)){
                        throw new Exception('Dữ liệu không hợp lệ');
                    }
                    $mUsersEmail->exportPdf($aData);
                    $mUsersEmail->sendEmail($aData);
                    $mFileSend = new FileSend();
                    $mFileSend->createSend($aData, $mUsersEmail);
                    Yii::app()->user->setFlash(MyFormat::SUCCESS_UPDATE, 'Gửi mail thành công: '.$mCustomer->getFullName());
                    $this->redirect(array('reportDebitCash','type' => InventoryCustomer::VIEW_COMPARE, 'date_from'=> $_GET['date_from'], 'date_to'=> $_GET['date_to']));
                }
            }
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }
    
    /** @Author: NamNH 09/2018
     *  @Todo: report debit
     **/
    public function actionReportDebit(){
        $this->pageTitle = 'BC công nợ quá hạn';
        try{
        $aData = [];
        
        $model=new InventoryCustomer('search');
        $model->unsetAttributes();  // clear any default values
        $model->date_to     = date('d-m-Y');
        if(isset($_GET['InventoryCustomer'])){
            $model->attributes=$_GET['InventoryCustomer'];
            $aData = $model->getReportDebit();
        }

        $this->render('reportDebit/debit',array(
            'model'=>$model, 'actions' => $this->listActionsCanAccess,
            'aData' => $aData
        ));
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }
    
    /** @Author: NamNH Nov 20, 2018
     *  @Todo: report vo
     **/
    public function actionReportWarningVo(){
        $this->pageTitle = 'BC cảnh báo tồn vỏ';
        try{
        $this->toExcelWarningVo();
        $aData = [];
        $model=new InventoryCustomer('search');
        $model->unsetAttributes();  // clear any default values
        $model->date_to         = date('d-m-Y');
        $model->type_customer   = STORE_CARD_KH_BINH_BO;
        $model->initDataSearchWarningVo();
        if(isset($_GET['InventoryCustomer'])){
            $model->attributes=$_GET['InventoryCustomer'];
            $model->initDataSearchWarningVo();
            $aData = $model->getWarningVo();
        }

        $this->render('reportWarningVo/warning',array(
            'model'=>$model, 'actions' => $this->listActionsCanAccess,
            'aData' => $aData
        ));
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }
    
    /** @Author: Pham Thanh Nghia 2018 
     *  @Todo: save to cache list data customer debt
     **/
    public function Rm_actionSetUserDebit(){
        die('not use this function, da chuyen qua admin/gasmember/setAgentForUser, Phan Tich Sai => do cache kieu nay bi reset khi het ngay, khi do khong lay dc data');
        $this->pageTitle = 'Cập nhật danh sách khách hàng bò nợ';
        $model = new InventoryCustomer();
        if(isset($_POST['InventoryCustomer']) ){
            $model->setCacheArraytCustomerDebit($_POST['InventoryCustomer']['customer_id']);
            Yii::app()->user->setFlash(MyFormat::SUCCESS_UPDATE, 'Cập nhật thành công');
            $this->redirect(array('setUserDebit'));
        }
        $this->render('setUserDebit/index',array(
            'model'=>$model, 'actions' => $this->listActionsCanAccess,
        ));
    }
    
    /** @Author: NamNH Dec 19, 2018
     *  @Todo: report warning vo
     **/
    public function toExcelWarningVo(){
        try{
            if(isset($_GET['to_excel']) && isset($_SESSION['ModelExcelWarning'])){
                ToExcel::summaryReportWarningVo();
            }
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }
    /**
     * @Author: PHAM THANH NGHIA 2019
     * @Todo: setup hạn mức nợ vỏ, nợ tiền cho KH
     * type = InventoryCustomer::TYPE_LIMIT_DEBIT
     * debit: hạn mức nợ vỏ, credit: hạn mức nợ tiền
     */
    public function actionLimitDebitIndex()
    {
        $this->pageTitle = 'Hạn mức nợ vỏ';
        try{
            $model=new InventoryCustomer();
            $model->unsetAttributes();  
            $model->type = InventoryCustomer::TYPE_LIMIT_DEBIT;

            if(isset($_GET['InventoryCustomer']))
                $model->attributes=$_GET['InventoryCustomer'];

            $this->render('limitDebit/index',array(
                    'model'=>$model, 'actions' => $this->listActionsCanAccess,
            ));
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }
    public function actionLimitDebitCreate()
    {
        $this->pageTitle = 'Hạn mức nợ vỏ cho khách hàng bò mối';
        try{
        $model=new InventoryCustomer('create');
        $model->type  = InventoryCustomer::TYPE_LIMIT_DEBIT;
        if(isset($_POST['InventoryCustomer'])){
            
            $model->attributes=$_POST['InventoryCustomer'];
//            $model->validate();
            $model->runSqlInsertLimitDebit();
            if(!$model->hasErrors()){
                $model->updateDeBitVo();
                Yii::app()->user->setFlash( MyFormat::SUCCESS_UPDATE, 'Thêm mới thành công' );
                $this->redirect(array('limitDebitIndex'));

            }else{
                Yii::app()->user->setFlash( MyFormat::ERROR_UPDATE, HandleLabel::FortmatErrorsModel($model->getErrors()));
            }
        }
        $this->overridePageTitle($model);
        $this->render('limitDebit/create',array(
                'model'=>$model, 'actions' => $this->listActionsCanAccess,
        ));
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }
    public function actionLimitDebitUpdate($id)
    {
        try{
        $model=$this->loadModel($id);
        $this->pageTitle = 'Cật nhật hạn mức nợ vỏ ' . $model->getCustomer();
        if(!$model->canUpdate()){// add Now 07, 2016
            Yii::app()->user->setFlash( MyFormat::ERROR_UPDATE, "Yêu cầu không hợp lệ" );
            $this->redirect(array('index'));
        }
//        $model->scenario = 'update';
        if(isset($_POST['InventoryCustomer']))
        {
            $model->attributes=$_POST['InventoryCustomer'];
            $model->validate();
            if(!$model->hasErrors()){
                $model->save();
                $model->updateDeBitVo();
                Yii::app()->user->setFlash( MyFormat::SUCCESS_UPDATE, "Cập nhật thành công." );
                $this->redirect(array('limitDebitUpdate','id'=>$model->id));
            }else{
                
                Yii::app()->user->setFlash( MyFormat::ERROR_UPDATE, HandleLabel::FortmatErrorsModel($model->getErrors()));
            }
        }
        $this->render('limitDebit/update',array(
                'model'=>$model, 'actions' => $this->listActionsCanAccess,
        ));
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }
    
}
