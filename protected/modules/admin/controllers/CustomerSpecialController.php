<?php

class CustomerSpecialController extends AdminController 
{
    public $pluralTitle = "Khách hàng đặc biệt";
    public $singleTitle = "Khách hàng đặc biệt";
    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id)
    {
        $this->pageTitle = 'Xem ';
        $this->layout = 'ajax';
        $model = $this->loadModel($id);
        $model->mapJsonField();
        try{
            $this->handleApproved($model);
            $this->handleSetLockUnlockCustomer($model);
            $this->render('view',array(
                    'model' => $model, 'actions' => $this->listActionsCanAccess,
            ));
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }
    
    /**
     * @Author: ANH DUNG Oct 21, 2016
     */
    public function handleSetLockUnlockCustomer($model) {
        if(isset($_GET['SetLockCustomer']) && $model->canLockCustomer()){
            $actionText = "";
            $model->setLockUnlockCustomer($_GET['channel_id'], $actionText);
            Yii::app()->user->setFlash( MyFormat::SUCCESS_UPDATE, "Cập nhật trạng thái $actionText thành công." );
            $this->redirect(array('view','id'=>$model->id));
        }
    }
    
    /**
     * @Author: ANH DUNG Sep 04, 2016
     * @Todo: handle approve 
     */
    public function handleApproved($model) {
        if(isset($_POST['CustomerSpecial'])){
            $model->attributes = $_POST['CustomerSpecial'];
            $model->setJsonField();
            $model->handleApproved();
            Yii::app()->user->setFlash( MyFormat::SUCCESS_UPDATE, "Cập nhật trạng thái thành công." );
            $this->redirect(array('view','id'=>$model->id));
        }
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate()
    {
        $this->pageTitle = 'Tạo Mới ';
        try{
        $model          = new CustomerSpecial('create');
        $model->type    = CustomerSpecial::TYPE_ADD_NEW_SPECIAL;
        $this->getCustomer($model);
        $this->handleRemoveSpecial($model);
        if(isset($_POST['CustomerSpecial']))
        {
            $model->attributes = $_POST['CustomerSpecial'];
            $model->setJsonField();
            $model->validate();
            $model->canCreateMore();            
            $model->uid_login = Yii::app()->user->id;
            if(!$model->hasErrors()){
                $model->getInfoRelate();
                $model->save();
                GasOneMany::saveManyId($model->id, GasOneMany::TYPE_CUSTOMER_SPECIAL, 'CustomerSpecial', 'reason');
                
                $needMore = array('aUidMerge' => array($model->approved_level_1));
                GasScheduleEmail::BuildNotifyCustomerSpecial($model, $needMore);
                
                Yii::app()->user->setFlash( MyFormat::SUCCESS_UPDATE, "Thêm mới thành công." );
               $this->redirect(array('create'));
            }
        }

        $this->render('create',array(
                'model'=>$model, 'actions' => $this->listActionsCanAccess,
        ));
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }

    public function getCustomer($model) {
        if(isset($_GET['customer_id']) && $_GET['customer_id'] > 100 ){
            $model->customer_id = $_GET['customer_id'];
        }
    }
    
    /**
     * @Author: ANH DUNG Sep 09, 2016
     * @Todo: 
     */
    public function handleRemoveSpecial($model) {
        if(isset($_GET['RemoveSpecial'])){
            $mParent = $this->loadModel($_GET['RemoveSpecial']);
            if(!$mParent->canRemoveSpecial()){
                $this->redirect(array('index'));
            }
            $model->type        = CustomerSpecial::TYPE_CANCEL_SPECIAL;
            $model->parent_id   = $mParent->id;
            $model->customer_id = $mParent->customer_id;
        }
    }
    
    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id)
    {
        $this->pageTitle = 'Cập Nhật ';
        try{
        $model=$this->loadModel($id);
        if(!$model->canUpdate()){
            $this->redirect(array('index'));
        }
        $model->scenario = 'update';
        $model->mapJsonField();
        $model->loadRelationField();
        $this->getCustomer($model);
        if(isset($_POST['CustomerSpecial']))
        {
            $model->attributes=$_POST['CustomerSpecial'];
            $model->setJsonField();
            $model->validate();
            if(!$model->hasErrors()){
                $model->getInfoRelate();
                $model->update();
                GasOneMany::saveManyId($model->id, GasOneMany::TYPE_CUSTOMER_SPECIAL, 'CustomerSpecial', 'reason');
                Yii::app()->user->setFlash( MyFormat::SUCCESS_UPDATE, "Cập nhật thành công." );
                $this->redirect(array('update','id'=>$model->id));
            }
        }

        $this->render('update',array(
                'model'=>$model, 'actions' => $this->listActionsCanAccess,
        ));
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id)
    {
        try{
        if(Yii::app()->request->isPostRequest){
            // we only allow deletion via POST request
            if($model = $this->loadModel($id))
            {
                $model->deleteParent();
                if($model->delete())
                    Yii::log("Uid: " .Yii::app()->user->id. " Delete record ".  print_r($model->attributes, true), 'info');
            }

            // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
            if(!isset($_GET['ajax']))
                $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('index'));
        }
        else
        {
            Yii::log("Uid: " .Yii::app()->user->id. " Invalid request. Please do not repeat this request again.");
            throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
        }	
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }

    /**
     * Manages all models.
     */
    public function actionIndex()
    {
        $this->pageTitle = 'Danh Sách ';
        try{
        $model=new CustomerSpecial('search');
        $model->unsetAttributes();  // clear any default values
        $model->initSessionUserApproved();  // clear any default values
        if(isset($_GET['CustomerSpecial']))
            $model->attributes=$_GET['CustomerSpecial'];

        $this->render('index',array(
                'model'=>$model, 'actions' => $this->listActionsCanAccess,
        ));
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
    * If the data model is not found, an HTTP exception will be raised.
     * @param integer the ID of the model to be loaded
     */
    public function loadModel($id)
    {
        try{
        $model=CustomerSpecial::model()->findByPk($id);
        if($model===null)
        {
            Yii::log("The requested page does not exist.");
            throw new CHttpException(404,'The requested page does not exist.');
        }			
        return $model;
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }
}
