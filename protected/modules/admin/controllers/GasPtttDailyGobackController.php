<?php

class GasPtttDailyGobackController extends AdminController 
{
    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id)
    {
        $this->pageTitle = 'Xem Bình Quay Về Hàng Ngày';
        try{
        $this->render('view',array(
                'model'=>$this->loadModel($id), 'actions' => $this->listActionsCanAccess,
        ));
        }
        catch (Exception $exc)
        {
            GasCheck::CatchAllExeptiong($exc);
        }

    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate()
    {
        $this->pageTitle = 'Tạo Mới Bình Quay Về Hàng Ngày';
            try
            {
            $model=new GasPtttDailyGoback('create');
            $this->RenderListEmployeePTTT($model);
            $model->agent_id = MyFormat::getAgentId();

            if(isset($_POST['GasPtttDailyGoback']))
            {
                $model->attributes=$_POST['GasPtttDailyGoback'];
                $model->validate();
                if(!$model->hasErrors()){
                    $model->save();
                    GasPtttDailyGobackDetail::SaveDetailQty($model);
                    Yii::app()->user->setFlash( MyFormat::SUCCESS_UPDATE, "Thêm mới thành công." );
                    $this->redirect(array('create'));
                }
            }

            $this->render('create',array(
                    'model'=>$model, 'actions' => $this->listActionsCanAccess,
            ));
            }
            catch (Exception $exc)
            {
                GasCheck::CatchAllExeptiong($exc);
            }
    }
    
    /**
     * @Author: ANH DUNG Mar 15, 2015
     * @Todo: render list nhân viên PTTT của 1 giám sát ở thời điểm hiện tại
     * @Param: $model
     */
    public function RenderListEmployeePTTT($model){
        if(isset($_POST['monitoring_id'])){
            $model->aMaintainEmployee = Users::getSelectByRoleForAgent($_POST['monitoring_id'], ONE_MONITORING_MARKET_DEVELOPMENT,'', array('status'=>STATUS_ACTIVE));
            $this->render('create',array(
                'model'=>$model, 'actions' => $this->listActionsCanAccess,
            ));
            die;
        }
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id)
    {
        $this->pageTitle = 'Cập Nhật Bình Quay Về Hàng Ngày';
        try
        {
        $model=$this->loadModel($id);
        $model->scenario = 'update';

        if(!GasCheck::AgentCanUpdatePTTTDailyGoback($model)){
            $this->redirect(array('index'));
        }
        $model->maintain_date = MyFormat::dateConverYmdToDmy($model->maintain_date);
        $model->aQty = Chtml::listData($model->rDetail, 'maintain_employee_id', 'qty');
        $model->aMaintainEmployee = Users::getSelectByRoleForAgent( $model->monitoring_id, ONE_MONITORING_MARKET_DEVELOPMENT,'', array('status'=>STATUS_ACTIVE));

        if(isset($_POST['GasPtttDailyGoback']))
        {
            $model->attributes=$_POST['GasPtttDailyGoback'];
                    // $this->redirect(array('view','id'=>$model->id));
            $model->validate();
            if(!$model->hasErrors()){
                $model->save();
                GasPtttDailyGobackDetail::SaveDetailQty($model);
                Yii::app()->user->setFlash( MyFormat::SUCCESS_UPDATE, "Cập nhật thành công." );
                $this->redirect(array('update','id'=>$model->id));
            }							
        }

        $this->render('update',array(
                'model'=>$model, 'actions' => $this->listActionsCanAccess,
        ));
        }
        catch (Exception $exc)
        {
            GasCheck::CatchAllExeptiong($exc);
        }
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id)
    {
        try
        {
        if(Yii::app()->request->isPostRequest)
        {
                // we only allow deletion via POST request
                if($model = $this->loadModel($id))
                {
                    if($model->delete())
                        Yii::log("Uid: " .Yii::app()->user->id. " Delete record ".  print_r($model->attributes, true), 'info');
                }

                // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
                if(!isset($_GET['ajax']))
                        $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('index'));
        }
        else
        {
            Yii::log("Uid: " .Yii::app()->user->id. " Invalid request. Please do not repeat this request again.");
            throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
        }	
        }
        catch (Exception $exc)
        {
            GasCheck::CatchAllExeptiong($exc);
        }
    }

    /**
     * Manages all models.
     */
    public function actionIndex()
    {
        $this->pageTitle = 'Bình Quay Về Hàng Ngày ';
        try
        {
        Users::InitSessionNameUserByRole(ROLE_EMPLOYEE_MARKET_DEVELOPMENT);
        $model=new GasPtttDailyGoback('search');            
        $model->unsetAttributes();  // clear any default values
        if(isset($_GET['GasPtttDailyGoback']))
                $model->attributes=$_GET['GasPtttDailyGoback'];

        $this->render('index',array(
                'model'=>$model, 'actions' => $this->listActionsCanAccess,
        ));
        }
        catch (Exception $exc)
        {
            GasCheck::CatchAllExeptiong($exc);
        }
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
    * If the data model is not found, an HTTP exception will be raised.
     * @param integer the ID of the model to be loaded
     */
    public function loadModel($id)
    {
        try
        {
        $model=GasPtttDailyGoback::model()->findByPk($id);
        if($model===null)
        {
            Yii::log("The requested page does not exist.");
            throw new CHttpException(404,'The requested page does not exist.');
        }			
        return $model;
        }
        catch (Exception $exc)
        {
            GasCheck::CatchAllExeptiong($exc);
        }
    }
    
    /** @Author: ANH DUNG Jul 15, 2017
     * @Todo: BC điểm và bình quay về admin/gasPtttDailyGoback/reportCcsDaily
     */
    public function actionReportCcsDaily() {
        $this->pageTitle = 'Báo cáo điểm và bình quay về';
        try{
        $model=new GasPtttDailyGoback();            
        $model->unsetAttributes();  // clear any default values
        $model->date_from   = "01-".date('m-Y');
        $model->date_to     = date("d-m-Y");
        $aData = [];
        $cRole = MyFormat::getCurrentRoleId();
        $cUid = MyFormat::getCurrentUid();
        if($cRole == ROLE_EMPLOYEE_MARKET_DEVELOPMENT){
            $model->uid_login = $cUid;
        }
        
        if(isset($_GET['GasPtttDailyGoback'])){
            $model->attributes=$_GET['GasPtttDailyGoback'];
            $aData = $model->getCcsDaily();
        }

        $this->render('report/CcsDaily',array('aData'=>$aData,
                'model'=>$model, 'actions' => $this->listActionsCanAccess,
        ));
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }

   
}
