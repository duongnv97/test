<?php

class AnnounceController extends AdminController 
{
    public $pluralTitle = 'Đại lý đánh chương trình';
    public $singleTitle = 'Đại lý đánh chương trình';
    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id)
    {
        $this->pageTitle = 'Xem ';
        try{
        $this->render('view',array(
                'model'=>$this->loadModel($id), 'actions' => $this->listActionsCanAccess,
        ));
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }

    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate()
    {
        $this->pageTitle = 'Tạo Mới ';
        try{
        $model              = new Announce('create');
        $model->date_from   = date('01-m-Y');
        $model->date_to     = date('t-m-Y');

        if(isset($_POST['Announce']))
        {
            $model->attributes=$_POST['Announce'];
            $model->validate();
            if(!$model->hasErrors()){
                $model->formatDbDatetime();
                $model->handleBeforeCreate();
                $model->save();
                $model->sendEmailApprove();
                $model->updateCache();
                Yii::app()->user->setFlash( MyFormat::SUCCESS_UPDATE, "Thêm mới thành công." );
                $this->redirect(array('create'));
            }				
        }

        $this->render('create',array(
                'model'=>$model, 'actions' => $this->listActionsCanAccess,
        ));
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id)
    {
        $this->pageTitle = 'Cập Nhật ';
        try{
        $model=$this->loadModel($id);
        if(!$model->canUpdate()){
            $this->redirect(array('index'));
        }
        $model->formatDataBeforeUpdate();
        $model->scenario = 'update';
        if(isset($_POST['Announce']))
        {
            $model->attributes=$_POST['Announce'];
                    // $this->redirect(array('view','id'=>$model->id));

            $model->validate();
            if(!$model->hasErrors()){
                $model->formatDbDatetime();
//                $model->date_from = date('Y-m-01');
//                $model->date_to = date('Y-m-d');
                $model->save();
                $model->updateCache();
                Yii::app()->user->setFlash( MyFormat::SUCCESS_UPDATE, "Cập nhật thành công." );
                $this->redirect(array('update','id'=>$model->id));
            }							
        }

        $this->render('update',array(
                'model'=>$model, 'actions' => $this->listActionsCanAccess,
        ));
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id)
    {
        try{
        if(Yii::app()->request->isPostRequest){
            // we only allow deletion via POST request
            $model = $this->loadModel($id);
            if($model)
            {
                if($model->delete()){
                    $mAnnounce = new AppCache();
                    $mAnnounce->updateCache();
                    Yii::log("Uid: " .Yii::app()->user->id. " Delete record ".  print_r($model->attributes, true), 'info');
                }
            }

            // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
            if(!isset($_GET['ajax']))
                    $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('index'));
        }
        else
        {
            Yii::log("Uid: " .Yii::app()->user->id. " Invalid request. Please do not repeat this request again.");
            throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
        }	
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }

    /**
     * Manages all models.
     */
    public function actionIndex()
    {
        $this->pageTitle = 'Đại lý đánh chương trình';
        try{
        $model=new Announce('search');
        $model->unsetAttributes();  // clear any default values
        if(isset($_GET['Announce']))
                $model->attributes=$_GET['Announce'];

        $this->render('index',array(
                'model'=>$model, 'actions' => $this->listActionsCanAccess,
        ));
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
    * If the data model is not found, an HTTP exception will be raised.
     * @param integer the ID of the model to be loaded
     */
    public function loadModel($id)
    {
        try{
        $model=Announce::model()->findByPk($id);
        if($model===null)
        {
            Yii::log("The requested page does not exist.");
            throw new CHttpException(404,'The requested page does not exist.');
        }			
        return $model;
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }
    
    /** @Author: DuongNV 2 Jul,2019
     *  @Todo: approve promotion
     **/
    public function actionApprove($id) {
        try{
            $model = Announce::model()->findByPk($id);
            if( $model && $model->canApprove())
            {
                $model->status = STATUS_ACTIVE;
                $model->save();
                $model->updateCache();
                Yii::app()->user->setFlash( MyFormat::SUCCESS_UPDATE, "Duyệt thành công." );
            }
            $this->redirect(array('index'));
        } catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }

}
