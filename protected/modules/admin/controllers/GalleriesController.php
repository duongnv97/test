<?php

class GalleriesController extends AdminController
{
	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
        try{
            $this->render('view',array(
                'model'=>$this->loadModel($id), 'actions' => $this->listActionsCanAccess,
            ));
        }
        catch (Exception $e)
        {
                    Yii::log("Exception ".  $e->getMessage(), 'error');
                    $code = 404;
                    if(isset($e->statusCode))
                        $code=$e->statusCode;
                    if($e->getCode())
                        $code=$e->getCode();
                    throw new CHttpException($code, $e->getMessage());
        }
		
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
        try
        {
            $model=new Galleries('create');

            // Uncomment the following line if AJAX validation is needed
            // $this->performAjaxValidation($model);

            if(isset($_POST['Galleries']))
            {
                $model->attributes=$_POST['Galleries'];
                $model->created_date    = date('Y-m-d H:i:s');
                //$model->slug            = Slug::toSlug($_POST['Galleries']['title']);
                $model->code            = MyFunctionCustom::getNextId('Galleries','{G',6,'code');
                $uploadFile = CUploadedFile::getInstance($model,'thumb_image');
                if(!empty($uploadFile)){
                    $fileName = preg_replace('/\.\w+$/', '', $uploadFile->name);
                    $fileName = $fileName . '_' . time() . '.' . $uploadFile->extensionName;
                    $model->thumb_image = $fileName;
                }else{
                    $uploadFile = "";
                    $model->thumb_image = "";
                }

                if($model->save()){
                    if(!empty($uploadFile)){
                        if (!file_exists(Yii::getPathOfAlias('webroot'). '/upload/admin/galleries'))
                            mkdir(Yii::getPathOfAlias('webroot'). '/upload/admin/galleries');
                        if (!file_exists(Yii::getPathOfAlias('webroot'). '/upload/admin/galleries/thumbs'))
                            mkdir(Yii::getPathOfAlias('webroot'). '/upload/admin/galleries/thumbs');

                        $uploadFile->saveAs(Yii::getPathOfAlias('webroot'). '/upload/admin/galleries/' . $fileName);

                        $ImageProcessing = new ImageProcessing();
                        $ImageProcessing->folder = '/upload/admin/galleries';

                        $ImageProcessing->file = $fileName;
                        $ImageProcessing->thumbs = array(
                            'thumbs' => array('width' => IMAGE_ADMIN_GALLERY_THUMB_WIDTH, 'height' =>
                            IMAGE_ADMIN_GALLERY_THUMB_HEIGHT),
                        );
                        $ImageProcessing->create_thumbs();

                    }
                    $this->redirect(array('view','id'=>$model->id));
                }
            }

            $this->render('create',array(
                'model'=>$model, 'actions' => $this->listActionsCanAccess,
            ));
        }
        catch (Exception $e)
        {
                    Yii::log("Exception ".  $e->getMessage(), 'error');
                    $code = 404;
                    if(isset($e->statusCode))
                        $code=$e->statusCode;
                    if($e->getCode())
                        $code=$e->getCode();
                    throw new CHttpException($code, $e->getMessage());
        }
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
        try
        {
            $model=$this->loadModel($id);
            $oldThumbImage = $model->thumb_image;
            $model->scenario = 'update';

            // Uncomment the following line if AJAX validation is needed
            // $this->performAjaxValidation($model);

            if(isset($_POST['Galleries']))
            {
                $model->attributes      = $_POST['Galleries'];
                $model->updated_date    = date('Y-m-d H:i:s');
                //$model->slug            = Slug::toSlug($_POST['Galleries']['title']);
                $uploadFile             = CUploadedFile::getInstance($model,'thumb_image');
                if(!empty($uploadFile)){
                    $fileName = preg_replace('/\.\w+$/', '', $uploadFile->name);
                    $fileName = $fileName . '_' . time() . '.' . $uploadFile->extensionName;
                    $model->thumb_image = $fileName;
                }else{
                    $model->thumb_image = $oldThumbImage;
                }

                if($model->save()){
                    if(!empty($uploadFile)){
                        if (!file_exists(Yii::getPathOfAlias('webroot'). '/upload/admin/galleries'))
                            mkdir(Yii::getPathOfAlias('webroot'). '/upload/admin/galleries');
                        if (!file_exists(Yii::getPathOfAlias('webroot'). '/upload/admin/galleries/thumbs'))
                            mkdir(Yii::getPathOfAlias('webroot'). '/upload/admin/galleries/thumbs');

                        $uploadFile->saveAs(Yii::getPathOfAlias('webroot'). '/upload/admin/galleries/' . $fileName);

                        $ImageProcessing = new ImageProcessing();
                        $ImageProcessing->folder = '/upload/admin/galleries';

                        $ImageProcessing->file = $fileName;
                        $ImageProcessing->thumbs = array(
                            'thumbs' => array('width' => IMAGE_ADMIN_GALLERY_THUMB_WIDTH, 'height' =>
                            IMAGE_ADMIN_GALLERY_THUMB_HEIGHT),
                        );
                        $ImageProcessing->create_thumbs();

                    }
                    $this->redirect(array('view','id'=>$model->id));
                }
            }

            $this->render('update',array(
                'model'=>$model, 'actions' => $this->listActionsCanAccess,
            ));
        }
        catch (Exception $e)
        {
                    Yii::log("Exception ".  $e->getMessage(), 'error');
                    $code = 404;
                    if(isset($e->statusCode))
                        $code=$e->statusCode;
                    if($e->getCode())
                        $code=$e->getCode();
                    throw new CHttpException($code, $e->getMessage());
        }
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
        try
        {
            if(Yii::app()->request->isPostRequest)
            {
                // we only allow deletion via POST request
                if($model = $this->loadModel($id))
                {
                    if (!file_exists(Yii::getPathOfAlias('webroot'). '/upload/admin/galleries/'.$model->thumb_image))
                        unlink(Yii::getPathOfAlias('webroot'). '/upload/admin/galleries/'.$model->thumb_image);
                    if (!file_exists(Yii::getPathOfAlias('webroot'). '/upload/admin/galleries/thumbs/'.$model->thumb_image))
                        unlink(Yii::getPathOfAlias('webroot'). '/upload/admin/galleries/thumbs/'.$model->thumb_image);


                    if($model->delete()){
                        Photos::model()->deleteAll(array('condition'=>'gallery_id ='.(int)$id));
                        Yii::log("Delete record ".  print_r($model->attributes, true), 'info');
                    }
                }

                // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
                if(!isset($_GET['ajax']))
                    $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('index'));
            }
            else
            {
                Yii::log("Invalid request. Please do not repeat this request again.");
                throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
            }
        }
        catch (Exception $e)
        {
            Yii::log("Exception ".  print_r($e, true), 'error');
            throw new CHttpException(400,$e->getMessage());
        }
	}

	/**
	 * Manages all models.
	 */
	public function actionIndex()
	{
        try
        {
            $model=new Galleries('search');
            $model->unsetAttributes();  // clear any default values
            if(isset($_GET['Galleries']))
                $model->attributes=$_GET['Galleries'];

            $this->render('index',array(
                'model'=>$model, 'actions' => $this->listActionsCanAccess,
            ));
        }
        catch (Exception $e)
        {
                    Yii::log("Exception ".  $e->getMessage(), 'error');
                    $code = 404;
                    if(isset($e->statusCode))
                        $code=$e->statusCode;
                    if($e->getCode())
                        $code=$e->getCode();
                    throw new CHttpException($code, $e->getMessage());
        }
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the ID of the model to be loaded
	 */
	public function loadModel($id)
	{
        try
        {
            $model=Galleries::model()->findByPk($id);
            if($model===null)
            {
                Yii::log("The requested page does not exist.");
                throw new CHttpException(404,'The requested page does not exist.');
            }
            return $model;
        }
        catch (Exception $e)
        {
                    Yii::log("Exception ".  $e->getMessage(), 'error');
                    $code = 404;
                    if(isset($e->statusCode))
                        $code=$e->statusCode;
                    if($e->getCode())
                        $code=$e->getCode();
                    throw new CHttpException($code, $e->getMessage());
        }
	}

	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
        try
        {
            if(isset($_POST['ajax']) && $_POST['ajax']==='galleries-form')
            {
                echo CActiveForm::validate($model);
                Yii::app()->end();
            }
        }
        catch (Exception $e)
        {
                    Yii::log("Exception ".  $e->getMessage(), 'error');
                    $code = 404;
                    if(isset($e->statusCode))
                        $code=$e->statusCode;
                    if($e->getCode())
                        $code=$e->getCode();
                    throw new CHttpException($code, $e->getMessage());
        }
	}
}
