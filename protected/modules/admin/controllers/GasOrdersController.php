<?php

class GasOrdersController extends AdminController 
{
    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id)
    {
        $this->pageTitle = 'Xem ';
        try{
            $model = $this->loadModel($id);
            $cRole = MyFormat::getCurrentRoleId();
            if( ( $cRole==ROLE_SUB_USER_AGENT && $model->agent_id != MyFormat::getAgentId() )
                    || ($cRole==ROLE_DIEU_PHOI && $model->type == GasOrders::TYPE_AGENT )
                ){
//                    $this->redirect(array('index'));
            }

        if(isset($_POST['GasOrders']))
            $model->attributes=$_POST['GasOrders'];
            
        $view = 'view';// Jun 19, 2015
        if(isset($_GET['view_create_by'])){
            $view = 'view_create_by';
        }

        $this->render($view, array(
                'model'=>$model, 'actions' => $this->listActionsCanAccess,
        ));
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate()
    {
//        $this->redirect('index');
        $this->pageTitle = 'Tạo Mới ';
        try{
        $this->ajaxChangeReleaseDate();
        $cRole = MyFormat::getCurrentRoleId();
        $model=new GasOrders('create');
        if(isset($_GET['date_delivery'])){
            $model->date_delivery = MyFormat::dateConverYmdToDmy($_GET['date_delivery']);
        }
        if(isset($_GET['user_id_executive'])){
            $model->user_id_executive = $_GET['user_id_executive'];
        }
        if(isset($_GET['agent_id'])){
            $model->agent_id = $_GET['agent_id'];
        }
        if($cRole == ROLE_SUB_USER_AGENT){
            $model->agent_id = MyFormat::getAgentId();
        }
        
        $model->aModelOrderDetail = array(new GasOrdersDetail());
        if(isset($_POST['GasOrders']))
        {
            $model->attributes=$_POST['GasOrders'];
            if($cRole == ROLE_SUB_USER_AGENT){
                $model->agent_id = MyFormat::getAgentId();
            }
            $this->CheckExitRecord($model);
            $model->validate();
            if(!$model->hasErrors()){
                $model->save();
                GasOrders::saveOrderDetail($model);
                $model->handleFlowCreateToAppOrder();// Jan 31, 2017
                Yii::app()->user->setFlash('successUpdate', "Thêm mới thành công.");
                $this->redirect(array('view','id'=>$model->id));
//                       $this->redirect(array('create'));
            }				
        }

        $this->render('create',array(
                'model'=>$model, 'actions' => $this->listActionsCanAccess,
        ));
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }
    
    
    /**
     * @Author: ANH DUNG Jul 07, 2015
     * @Todo: kiểm tra nếu có đơn hàng tạo rồi thì không cho tạo nữa
     * trong trường hợp 2 điều phối bấm tạo mới và để đó, 10 phút hay lâu hơn nữa
     * mới bấm submit lên
     * @Param: $model
     */
    public function CheckExitRecord($model) {
        if(empty($model->date_delivery) || $this->checkAllowCreateMultiOrder()){
            return ;
        }
        $mOrderCheck                    = new GasOrders();
        $mOrderCheck->date_delivery     = MyFormat::dateConverDmyToYmd($model->date_delivery);
        $mOrderCheck->user_id_executive = $model->user_id_executive;
        $mOrderCheck->user_id_create    = MyFormat::getCurrentUid();
        $mOrderCheck->agent_id          = $model->agent_id;
//        $mOrderCheck = $mOrderCheck->getByDateDelivery($date_delivery, $user_id_executive, Yii::app()->user->id);
        $mOrderCheck = $mOrderCheck->getByDateDeliveryFix();
        if($mOrderCheck){
            throw new Exception("Đơn hàng của ngày $model->date_delivery đã được tạo trước đó, bạn vui lòng kiểm tra lại");
        }
    }

    /**
     * @Author: ANH DUNG 12-29-2013
     * @Todo: khi change release date check xem ngày đó đã có dữ liệu chưa, nếu có thì redirect qua update
     * không thì ko làm gì cả
     *  1. check nếu date đó có hợp lệ để edit không nữa? sẽ check ở action update
     */        
    public function ajaxChangeReleaseDate(){
        
        if(!empty($_POST['date_delivery']) && isset($_POST['user_id_executive']) && isset($_POST['from_ajax'])){
            $date_delivery = MyFormat::dateConverDmyToYmd($_POST['date_delivery']);
            $aParam = array('date_delivery' => $date_delivery, 'user_id_executive' => $_POST['user_id_executive'], 'agent_id'=>$_POST['agent_id']);
            $urlCreate = Yii::app()->createAbsoluteUrl('admin/gasOrders/create', $aParam);
            try {
                if($this->checkAllowCreateMultiOrder()){
                    throw new Exception('Allow create multi - dev note');
                }
                $json = array('success'=>false, 'next'=>$urlCreate);
                $mOrderCheck                    = new GasOrders();
                $mOrderCheck->date_delivery     = $date_delivery;
                $mOrderCheck->user_id_executive = $_POST['user_id_executive'];
                $mOrderCheck->user_id_create    = MyFormat::getCurrentUid();
                $mOrderCheck->agent_id          = $_POST['agent_id'];
//                $model = GasOrders::getByDateDelivery($date_delivery, $_POST['user_id_executive'], MyFormat::getCurrentUid());
                $model = $mOrderCheck->getByDateDeliveryFix();
                if($model)
                    $json = array('success'=>true,'next'=>Yii::app()->createAbsoluteUrl('admin/gasOrders/update',array('id'=>$model->id) ));               
            } catch (Exception $exc) {
                $json = array('success'=>false, 'next'=>$urlCreate);
            }
            echo CJavaScript::jsonEncode($json);die;
        }
    }
    
    /** @Author: ANH DUNG Now 05, 2017
     *  @Todo: cho phép user điều xe Phước Tân tạo nhiều đơn thu vỏ đại lý trong cùng 1 ngày
     **/
    public function checkAllowCreateMultiOrder() {
        $cRole = MyFormat::getCurrentRoleId();
        $cUid = MyFormat::getCurrentUid();
        $aAllow = [GasConst::UID_DIEUXE_KHO_PHUOCTAN, GasConst::UID_DIEUXE_GIA_LAI, GasConst::UID_DIEUXE_BINH_DINH, GasConst::UID_DIEUXE_KHO_BENCAT, GasConst::UID_DIEUXE_VINH_LONG];
        if(in_array($cUid, $aAllow)){
            return true;
        }
        return false;
    }
    
    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id)
    {
//        error_reporting(1); // var_dump(PHP_INT_SIZE);
        $this->pageTitle = 'Cập Nhật ';
        try{
        $model=$this->loadModel($id);
        $model->scenario = 'update';
        if(!GasCheck::AgentCanUpdateOrder($model)){
            $this->redirect(array('index'));
        }

//            $this->checkUpdateValid($id);            
        $model->date_delivery = MyFormat::dateConverYmdToDmy($model->date_delivery);
        $OldCountDetail = count($model->rOrderDetail);
        $model->aModelOrderDetail = $OldCountDetail?$model->rOrderDetail:array(new GasOrdersDetail());

        if(isset($_POST['GasOrders']))
        {
            $model->attributes=$_POST['GasOrders'];
                    // $this->redirect(array('view','id'=>$model->id));
            $model->validate();
            if(!$model->hasErrors()){
                $NewCountDetail = count($model->aModelOrderDetail);
                if( $NewCountDetail>$OldCountDetail && !empty($model->user_id_update_car_number) ){
                    $model->setFlagUpdateCarAgain();
                }
                $model->save();
                GasOrdersDetail::updateOrderDetail($model);
//                    GasOrders::saveOrderDetail($model);
                if($model->hasErrors()){
                    Yii::app()->user->setFlash(MyFormat::ERROR_UPDATE, $model->getError('date_delivery'));
                }else{
                    Yii::app()->user->setFlash('successUpdate', "Cập nhật thành công.");
                }
                $this->redirect(array('update','id'=>$model->id));
            }							
        }

        $this->render('update',array(
                'model'=>$model, 'actions' => $this->listActionsCanAccess,
        ));
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }
    
    /**
     * @Author: ANH DUNG 12-29-2013
     * @Todo: kiểm tra update có hợp lệ không, nếu không thì redirect qua index
     * chỉ cho cập nhật record có release_date ngày hiện tại và 1 ngày trước
     */      
    public function checkUpdateValid($pk){
        try {
            $model = GasOrders::model()->findByPk($pk);
            if($model){
                $today = date('Y-m-d');
                $prev_date = MyFormat::modifyDays($today, GasOrders::$days_allow_update, '-');
                $isValidDateBetween = MyFormat::compareDateBetween($prev_date, $today, $model->date_delivery);
                if($model->user_id_create!=Yii::app()->user->id 
                        /*|| !$isValidDateBetween*/
                )
                    $this->redirect(array('index'));
            }
        } catch (Exception $exc) {
            throw new CHttpException('404', $exc->getMessage());
        }        
    }    

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id)
    {
            try
            {
            if(Yii::app()->request->isPostRequest)
            {
                    // we only allow deletion via POST request
                    if($model = $this->loadModel($id))
                    {
                        if($model->delete())
                            Yii::log("Uid: " .Yii::app()->user->id. " Delete record ".  print_r($model->attributes, true), 'info');
                    }

                    // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
                    if(!isset($_GET['ajax']))
                            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('index'));
            }
            else
            {
                Yii::log("Uid: " .Yii::app()->user->id. " Invalid request. Please do not repeat this request again.");
                throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
            }	
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }

    /**
     * Manages all models.
     */
    public function actionIndex()
    {
        $this->pageTitle = 'Đặt Hàng';
        $this->allowIndex();
        try{
        $model=new GasOrders('search');
        $model->unsetAttributes();  // clear any default values
        $this->handleWait($model);
        
        if(isset($_GET['GasOrders']))
            $model->attributes=$_GET['GasOrders'];

        $this->render('index',array(
                'model'=>$model, 'actions' => $this->listActionsCanAccess,
        ));
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }
    
    /**
     * @Author: ANH DUNG Oct 10, 2016
     */
    public function handleWait($model) {
        if(isset($_GET['wait']) && $model->canPrintDieuXe()){
            $model->aModelOrderDetail = array();
            $cUid = Yii::app()->user->id;
            $model->date_delivery       = date("d-m-Y");
            $model->user_id_executive   = $cUid;
            if(isset($_POST['GasOrders'])){
                $model->attributes=$_POST['GasOrders'];
                $model->aModelOrderDetail = $model->getDetailForDriver();
            }
            $this->render('index',array(
                    'model'=>$model, 'actions' => $this->listActionsCanAccess,
            ));
            die;
        }
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
    * If the data model is not found, an HTTP exception will be raised.
     * @param integer the ID of the model to be loaded
     */
    public function loadModel($id)
    {
        try
        {
            $model=GasOrders::model()->findByPk($id);
            if($model===null)
            {
                Yii::log("The requested page does not exist.");
                throw new CHttpException(404,'The requested page does not exist.');
            }			
            return $model;
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }
    
    /**
     * @Author: ANH DUNG Apr 08, 2014
     * @Todo: for user of kho cập nhật số xe giao hàng
     */    
    public function actionUpdateCarNumber($id){
        try{
            $this->layout='ajax';
            $model=$this->loadModel($id);
            $this->checkUpdateCarNumber($model); // run ok nhưng cần check thêm điều kiện khác nữa
            $model->date_delivery = MyFormat::dateConverYmdToDmy($model->date_delivery);
            $model->aModelOrderDetail = count($model->rOrderDetail)?$model->rOrderDetail:array(new GasOrdersDetail());
            if(isset($_POST['GasOrdersDetail'])){
                $aUpdate = ['user_id_update_car_number','date_update', 'update_again_car'];
                if(isset($_POST['GasOrders']) && $model->type == GasOrders::TYPE_AGENT){
                    $model->attributes  = $_POST['GasOrders'];
                    $model->convertDate();
                    $aUpdate[] = 'date_delivery';// Aug1017 cho phép change date với đơn hàng của đại lý
                }

                GasOrdersDetail::updateCarNumber($model);
                if($model->hasErrors()){
                    Yii::app()->user->setFlash(MyFormat::ERROR_UPDATE, $model->getError('id'));
                    if(isset($_POST['GasOrders']) && $model->type == GasOrders::TYPE_AGENT){
                        $model->date_delivery = MyFormat::dateConverYmdToDmy($model->date_delivery);
                    }
                }else{
                    if(isset($_GET['update_again_car'])){
                        $model->update_again_car = 0;
                    }
                    $model->user_id_update_car_number   = Yii::app()->user->id;
                    $model->date_update                 = date('Y-m-d H:i:s');
                    $model->update($aUpdate);
                    Yii::app()->user->setFlash(MyFormat::SUCCESS_UPDATE, "Cập nhật thành công.");
                    $this->redirect(array('updateCarNumber','id'=>$model->id));
                }
            }

            $this->render('UpdateCarNumber',array(
                    'model'=>$model, 'actions' => $this->listActionsCanAccess,
            ));
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }
    
    /**
     * @Author: ANH DUNG Apr 09, 2014
     * @Todo: kiểm tra update có hợp lệ không, nếu không thì redirect qua index
     * chỉ cho cập nhật record có release_date ngày hiện tại và 1 ngày trước
     */      
    public function checkUpdateCarNumber($model){
        try {
            if(!GasCheck::allowUpdateCarNumber($model)){
                die('<script type="text/javascript">parent.$.fn.colorbox.close();</script>'); 
            }
        } catch (Exception $exc) {
            throw new CHttpException('404', $exc->getMessage());
        }        
    }
    public function allowIndex(){
        $cUid = MyFormat::getCurrentUid();
        $cRole = MyFormat::getCurrentRoleId();
        $aUidLock  = [
//            485406, // Bùi Quyết Chiến
            862902, // Nguyễn Văn Tuấn 
            1441420, // Kho PT - Trần Thị Tuyết Nhung
            862901, // Nguyễn Đình Quảng
        ];
        if(in_array($cUid, $aUidLock)){
            $this->redirect(Yii::app()->createAbsoluteUrl(''));
        }
    }
    
}
