<?php

class EmployeeCashbookController extends AdminController 
{
    public $pluralTitle = 'Thu Tiền KH';
    public $singleTitle = 'Thu Tiền KH';
    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id)
    {
        $this->pageTitle = 'Xem ';
        try{
        $this->render('view',array(
                'model'=>$this->loadModel($id), 'actions' => $this->listActionsCanAccess,
        ));
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }

    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate()
    {
        $this->pageTitle = 'Tạo Mới ';
        try{
        $model=new EmployeeCashbook('WebCreate');
        $model->master_lookup_id    = GasMasterLookup::ID_THU_TIEN_KH;
        $model->date_input          = date('d/m/Y');
        $cRole  = MyFormat::getCurrentRoleId();
        $cUid   = MyFormat::getCurrentUid();
        $aRoleDebt = [ROLE_DEBT_COLLECTION];
        $aUidAllow = [GasLeave::THUC_NH, GasConst::UID_HUE_LT];

        if(in_array($cRole, $aRoleDebt) || in_array($cUid, $aUidAllow)){
            $model->agent_id            = GasConst::AGENT_DKMN;
        }
        if($cUid == GasConst::UID_DUYEN_NT){
            $model->agent_id        = MyFormat::KHO_BEN_CAT;
        }
        $model->type                = EmployeeCashbook::TYPE_SCHEDULE;
        $model->resetScenario();
        if(isset($_POST['EmployeeCashbook'])){
            $model->attributes=$_POST['EmployeeCashbook'];
            $model->resetScenario();
            $model->setWebInfo();
            $model->validate();
            $model->validateFileOnCreate();
            if(!$model->hasErrors()){
                $model->saveMulti();
                if(!$model->hasErrors()){
//                    $model->updateCashbookAgent();
                    Yii::app()->user->setFlash( MyFormat::SUCCESS_UPDATE, 'Thêm mới thành công.');
                    $this->redirect(array('create'));
                }
            }
            Yii::app()->user->setFlash( MyFormat::ERROR_UPDATE, HandleLabel::FortmatErrorsModel($model->getErrors()));
        }

        $this->render('create',array(
                'model'=>$model, 'actions' => $this->listActionsCanAccess,
        ));
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id)
    {
        try{
        $model=$this->loadModel($id);
        $model->modelOld = clone $model;
        if(!$model->canUpdate()){
            $this->redirect(array('index'));
        }
        $model->date_input = MyFormat::dateConverYmdToDmy($model->date_input);
        $this->pageTitle = "$model->code_no - $model->date_input {$model->getCustomer()}";
        $model->resetScenario();
        $mGasFile = new GasFile();
        if(isset($_POST['EmployeeCashbook']))
        {
            $model->attributes=$_POST['EmployeeCashbook'];
            $model->resetScenario();
            $model->setWebInfo();
            $model->validate();
            $mGasFile->validateFile($model);
            if(!$model->hasErrors()){
                $mGasFile->deleteFileInUpdate($model);
                $model->save();
                $mGasFile->saveRecordFile($model, GasFile::TYPE_8_CASHBOOK);
                $model->updateCashbookAgent();
                $model->fixCashbookHgdByDate();
                $model->doFlowChiTreoCongNo();
                $model->modelOld->saveEvent(GasAppOrder::WEB_UPDATE_CASHBOOK);
                $model->saveLogUpdate(LogUpdate::TYPE_3_CASHBOOK);
                Yii::app()->user->setFlash( MyFormat::SUCCESS_UPDATE, "Cập nhật thành công." );
                $this->redirect(array('update','id'=>$model->id));
            }
        }

        $this->render('update',array(
                'model'=>$model, 'actions' => $this->listActionsCanAccess,
        ));
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id)
    {
        try{
        if(Yii::app()->request->isPostRequest){
            // we only allow deletion via POST request
            if($model = $this->loadModel($id))
            {
                if($model->delete())
                    Yii::log("Uid: " .Yii::app()->user->id. " Delete record ".  print_r($model->attributes, true), 'info');
            }

            // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
            if(!isset($_GET['ajax']))
                    $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('index'));
        }
        else
        {
            Yii::log("Uid: " .Yii::app()->user->id. " Invalid request. Please do not repeat this request again.");
            throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
        }	
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }

    /**
     * Manages all models.
     */
    public function actionIndex()
    {
//        CacheSession::setCookie(CacheSession::CURRENT_USER_EXT, 1011);
        $this->pageTitle = 'Thu chi giao nhận';
        try{
        $this->handleExportExcel();
        $model=new EmployeeCashbook('search');
        $model->unsetAttributes();  // clear any default values
//        $model->date_from   = '01-'.date('m-Y');
//        $model->date_to     = date('d-m-Y');
        if(isset($_GET['EmployeeCashbook']))
            $model->attributes=$_GET['EmployeeCashbook'];

        $this->render('index',array(
                'model'=>$model, 'actions' => $this->listActionsCanAccess,
        ));
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
    * If the data model is not found, an HTTP exception will be raised.
     * @param integer the ID of the model to be loaded
     */
    public function loadModel($id)
    {
        try{
        $model=EmployeeCashbook::model()->findByPk($id);
        if($model===null)
        {
            Yii::log("The requested page does not exist.");
            throw new CHttpException(404,'The requested page does not exist.');
        }			
        return $model;
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }
    
        
    public function handleExportExcel() {
        if(isset($_GET['ExportExcel']) && isset($_SESSION['data-excel'])){
            error_reporting(1); // var_dump(PHP_INT_SIZE);
            ini_set('memory_limit','2500M');
            $mExportList = new ExportList();
            $mExportList->employeeCashbook($_SESSION['data-excel']->data, new EmployeeCashbook());
        }
    }

}
