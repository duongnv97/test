<?php

class GasPriceController extends AdminController 
{
    public $pluralTitle = "Thiết lập giá";
    public $singleTitle = "Thiết lập giá";
    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id)
    {
        $this->pageTitle = 'Xem ';
        $this->layout='ajax';
        try{
            $model = $this->loadModel($id);
            $model->scenario = "UpdateStatus";
            $this->HandleUpdateStatus($model);
            $this->render('view',array(
                    'model'=> $model, 'actions' => $this->listActionsCanAccess,
            ));
        }
        catch (Exception $exc)
        {
            GasCheck::CatchAllExeptiong($exc);
        }
    }
    
    /**
     * @Author: ANH DUNG May 15, 2016
     * @Todo: for user approve 1, 2
     */
    public function actionUpdateStatus() {}
    
    /**
     * @Author: ANH DUNG May 16, 2016
     * @Todo: handle post update status
     */
    public function HandleUpdateStatus($model) {
        if(isset($_POST['GasPrice']))
        {
            $model->attributes=$_POST['GasPrice'];
            $model->validate();
            if(!$model->hasErrors()){
                $model->update(array('status', 'note'));
                Yii::app()->user->setFlash( MyFormat::SUCCESS_UPDATE, "Cập nhật trạng thái thành công." );
                $this->redirect(array('view','id'=>$model->id));
            }				
        }
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate()
    {
        $this->pageTitle = 'Tạo Mới ';
        try{
        $model=new GasPrice('create1');
        $model->getPriceG10();
        $model->calcMonthDefault();

        if(isset($_POST['GasPrice']))
        {
            $model->attributes=$_POST['GasPrice'];
            $model->resetScenarioCreate();
            $model->validate();
            if(!$model->hasErrors()){
                if(!$model->canCreate()){
                    throw new Exception('Không thể tạo mới, setup giá đã được tạo, vui lòng cập nhật');
                }
                $model->buildJsonBo();
                $model->buildJsonMoi();
                $model->save();
                GasScheduleSms::notifyGasPriceSetup($model);
                Yii::app()->user->setFlash( MyFormat::SUCCESS_UPDATE, "Thêm mới thành công." );
                $this->redirect(array('update','id'=>$model->id));
            }				
        }else{
            $model->loadPrevMonth();
            $model->formatInputNumber();
        }

        $this->render('create',array(
                'model'=>$model, 'actions' => $this->listActionsCanAccess,
        ));
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }

    public function actionUpdate($id)
    {
        $this->pageTitle = 'Cập Nhật ';
        try{
        $model=$this->loadModel($id);
        $model->resetScenarioUpdate();
        $model->formatInputNumber();
        if(isset($_POST['GasPrice']))
        {
            $model->attributes=$_POST['GasPrice'];
            $model->resetScenarioUpdate();
            if($model->status == GasPrice::STATUS_REFIX){
                $model->status = GasPrice::STATUS_REFIX_OK;
            }
            $model->validate();
            if(!$model->hasErrors()){
                $model->buildJsonBo();
                $model->buildJsonMoi();
                $model->update();
                GasScheduleSms::notifyGasPriceSetup($model);
                Yii::app()->user->setFlash( MyFormat::SUCCESS_UPDATE, "Cập nhật thành công." );
                $this->redirect(array('update','id'=>$model->id));
            }
        }

        $this->render('update',array(
                'model'=>$model, 'actions' => $this->listActionsCanAccess,
        ));
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id)
    {
        try{
        if(Yii::app()->request->isPostRequest)
        {
                // we only allow deletion via POST request
                if($model = $this->loadModel($id))
                {
                    if($model->delete())
                        Yii::log("Uid: " .Yii::app()->user->id. " Delete record ".  print_r($model->attributes, true), 'info');
                }

                // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
                if(!isset($_GET['ajax']))
                        $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('index'));
        }
        else
        {
            Yii::log("Uid: " .Yii::app()->user->id. " Invalid request. Please do not repeat this request again.");
            throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
        }	
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }

    /**
     * Manages all models.
     */
    public function actionIndex()
    {
        $this->pageTitle = 'Danh Sách ';
        try{
        $model=new GasPrice('search');
        $model->unsetAttributes();  // clear any default values
        if(isset($_GET['GasPrice']))
            $model->attributes=$_GET['GasPrice'];        
        $this->render('index',array(
                'model'=>$model, 'actions' => $this->listActionsCanAccess,
        ));
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
    * If the data model is not found, an HTTP exception will be raised.
     * @param integer the ID of the model to be loaded
     */
    public function loadModel($id)
    {
        try
        {
        $model=GasPrice::model()->findByPk($id);
        if($model===null)
        {
            Yii::log("The requested page does not exist.");
            throw new CHttpException(404,'The requested page does not exist.');
        }			
        return $model;
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }

    /**
     * Performs the AJAX validation.
     * @param CModel the model to be validated
     */
    protected function performAjaxValidation($model)
    {
        try{
        if(isset($_POST['ajax']) && $_POST['ajax']==='gas-price-form')
        {
                echo CActiveForm::validate($model);
                Yii::app()->end();
        }
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }
}
