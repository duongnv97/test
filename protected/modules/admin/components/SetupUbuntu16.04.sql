/** ANH DUNG ADD JUN 22, 2018
@Todo: some note when Setup code on Ubuntu 16.04
********* Jun2218 fix setup new project Gas on Ubuntu 16.04 *******
1/ fix bị lỗi nhưng ko nhìn thấy 
Try using the command php --ini. Since you're running command line, it won't necessarily show you the config files you may be looking for (apache, nginx, etc), but it will hopefully get you on the right path.

https://stackoverflow.com/questions/5127838/where-does-php-store-the-error-log-php5-apache-fastcgi-cpanel
https://askubuntu.com/questions/921485/php-ini-does-not-exist
php --info | grep error 
2/ do đoạn load SettingForm::applySettings(); => class MyConfig extends CApplicationComponent
không try catch nên ko show đc lỗi 
3/ PHP 7 khi lỗi không có kết nối db với sql lite cũng không báo lỗi ra màn hình, rất khó để debug
sudo apt-get install php-sqlite3 
sau khi cài xong thì lên bình thường
mode re_write đã On
4/ sau khi login bị lỗi: Call to undefined function curl_init()
 - sau khi cài sudo apt-get install curl vẫn bị lỗi. Check: php -i | grep curl   
 - phải cài đúng version php thì mới hết: PHP 7.0: sudo apt-get install php7.0-curl  => https://stackoverflow.com/questions/38800606/how-to-install-php-curl-in-ubuntu-16-04
5/ -- click file font to install done
********* Jun2218 fix setup new project Gas on Ubuntu 16.04 *******


***** Jun2218 Sql fix default value for all live db Gas *****************
1/ ALTER TABLE `gas_gas_track_login` CHANGE `device_name` `device_name` VARCHAR(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL, CHANGE `device_imei` `device_imei` VARCHAR(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL, CHANGE `device_os_version` `device_os_version` VARCHAR(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL;
2/ ALTER TABLE `gas_sell` CHANGE `amount_bu_vo` `amount_bu_vo` INT(11) UNSIGNED NOT NULL DEFAULT '0', CHANGE `status_cancel` `status_cancel` TINYINT(1) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'lý do hủy đơn hàng', CHANGE `customer_id` `customer_id` INT(11) UNSIGNED NULL DEFAULT '0', CHANGE `type_customer` `type_customer` TINYINT(1) UNSIGNED NOT NULL DEFAULT '0', CHANGE `first_name` `first_name` TINYTEXT CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL, CHANGE `agent_id` `agent_id` INT(11) UNSIGNED NOT NULL DEFAULT '0', CHANGE `uid_login` `uid_login` INT(11) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'là nv kế toán BH, chỉ có user ktbh có thể tạo dc order', CHANGE `amount_discount` `amount_discount` INT(11) UNSIGNED NULL DEFAULT '0' COMMENT 'tiền CK có thể đại lý sửa', CHANGE `transaction_history_id` `transaction_history_id` INT(11) UNSIGNED NOT NULL DEFAULT '0', CHANGE `app_promotion_user_id` `app_promotion_user_id` INT(9) UNSIGNED NOT NULL DEFAULT '0', CHANGE `promotion_id` `promotion_id` SMALLINT(5) UNSIGNED NOT NULL DEFAULT '0', CHANGE `promotion_amount` `promotion_amount` MEDIUMINT(5) UNSIGNED NOT NULL DEFAULT '0', CHANGE `total` `total` INT(11) UNSIGNED NOT NULL DEFAULT '0', CHANGE `grand_total` `grand_total` INT(11) NOT NULL DEFAULT '0', CHANGE `support_id` `support_id` TINYINT(1) UNSIGNED NOT NULL DEFAULT '0', CHANGE `pttt_code` `pttt_code` VARCHAR(5) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL, CHANGE `high_price` `high_price` TINYINT(1) UNSIGNED NOT NULL DEFAULT '0', CHANGE `customer_new` `customer_new` TINYINT(1) UNSIGNED NOT NULL DEFAULT '0' COMMENT '1: KH new, 2: old', CHANGE `platform` `platform` TINYINT(1) UNSIGNED NOT NULL DEFAULT '0' COMMENT '0: web: 1: android, 2: ios', CHANGE `gas_remain` `gas_remain` DECIMAL(5,2) NULL DEFAULT NULL, CHANGE `gas_remain_amount` `gas_remain_amount` INT(11) NOT NULL DEFAULT '0';

***** Jun2218 Sql fix default value for all live db Gas *****************

***** Other fix *******
1/ Disable ONLY_FULL_GROUP_BY: https://stackoverflow.com/questions/23921117/disable-only-full-group-by
 - error: which is not functionally dependent on columns in GROUP BY clause; this is incompatible with sql_mode=only_full_group_by
 - sudo nano /etc/mysql/my.cnf
  [mysqld] sql_mode = "NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION"  

2/