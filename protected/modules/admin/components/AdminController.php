<?php
/**
 * Controller is the customized base controller class.
 * All controller classes for this application should extend from this base class.
 */
class AdminController extends MainController
{

    /**
	 * @var string the default layout for the controller view. Defaults to '//layouts/column1',
	 * meaning using a single column layout. See 'protected/views/layouts/column1.php'.
	 */
	public $layout='/layouts/column2';
	/**
	 * @var array context menu items. This property will be assigned to {@link CMenu::items}.
	 */
	public $menu=array();
	/**
	 * @var array the breadcrumbs of the current page. The value of this property will
	 * be assigned to {@link CBreadcrumbs::links}. Please refer to {@link CBreadcrumbs::links}
	 * for more details on how to specify this property.
	 */
	public $breadcrumbs=array();
    
    public $adminPageTitle='';

	/**
     * Handle the ajax request. This process changes the status of member to 1 (mean active)
     * @param type $id the id of member need changed status to 1
     */
//    public function actionAjaxActivate($id) {
//        if(Yii::app()->request->isPostRequest)
//        {
//            $model = $this->loadModel($id);
//            if(method_exists($model, 'activate'))
//            {
//                $model->activate();
//            }
//            Yii::app()->end();
//        }
//        else
//        {
//            Yii::log('Invalid request. Please do not repeat this request again.');
//            throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
//        }
//            
//    }

    /**
     * Handle the ajax request. This process changes the status of member to 0 (mean deactive)
     * @param type $id the id of member need changed status to 0
     */
//    public function actionAjaxDeactivate($id) {
//        if(Yii::app()->request->isPostRequest)
//        {
//            $model = $this->loadModel($id);
//            if(method_exists($model, 'deactivate'))
//            {
//                $model->deactivate();
//            }
//            Yii::app()->end();
//        }
//        else
//        {
//            Yii::log('The requested page does not exist.');
//            throw new CHttpException(404,'The requested page does not exist.');
//        }
//            
//    }

//    public function actionAjaxShow($id) {
//        if(Yii::app()->request->isPostRequest)
//        {
//            $model = $this->loadModel($id);
//            if(method_exists($model, 'activate'))
//            {
//                $model->showInMenu();
//            }
//            Yii::app()->end();
//        }
//        else
//        {
//            Yii::log('Invalid request. Please do not repeat this request again.');
//            throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
//        }            
//    }

//    public function actionAjaxNotShow($id) {
//        if(Yii::app()->request->isPostRequest)
//        {
//            $model = $this->loadModel($id);
//            if(method_exists($model, 'deactivate'))
//            {
//                $model->notShowInMenu();
//            }
//            Yii::app()->end();
//        }
//        else
//        {
//            Yii::log('Invalid request. Please do not repeat this request again.');
//            throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
//        }           
//    }
//	
	/**
     * Handle the ajax request. This process changes the approval to 1 (mean approved)
     * @param type $id the id of member need changed status to 1
     */
//    public function actionAjaxApprove($id) {
//        if(Yii::app()->request->isPostRequest)
//        {
//            $model = $this->loadModel($id);
//            if(method_exists($model, 'approve'))
//            {
//                $model->approve();
//            }
//            Yii::app()->end();
//        }
//        else
//        {
//            Yii::log('Invalid request. Please do not repeat this request again.');
//            throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
//        }            
//    }

    /**
     * Need override
     * @param $id
     * @return null
     */
    public function loadModel($id)
    {
        return null;
    }
    
    public function accessRules()
    {        
        return $this->controllerRules(Yii::app()->controller->id, 'admin');
    }
    
    /** @Author: DungNT Jul 18, 2019
     *  @Todo: handle redirect error, not write log
     **/
    public function redirectErrors($exc) {
        if(in_array($exc->getCode(), SpjError::$aNotWriteLog)){
            $url = Yii::app()->createAbsoluteUrl('admin/site/error', ['message' => $exc->getMessage()]);
//            Yii::app()->user->setFlash( MyFormat::ERROR_UPDATE, $exc->getMessage());// Jul2519 dùng setFlash gây ra lỗi render nhiều lần dưới view (khi tổng đài tạo đơn app) -> ko sử dụng đc
            $this->redirect($url);
        }
    }
}