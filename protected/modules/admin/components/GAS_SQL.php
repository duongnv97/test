<?php
class GAS_NOTE_SYSTEM
{
    /** SQL RUN MORE
     * SAU NÀY MỚI HIỂU ĐƯỢC
     * ############## AUG 18, 2014 ################
     ALTER TABLE `gas_gas_bussiness_contract` ADD `date_assign_customer` DATETIME NULL COMMENT 'ngày gắn khách hàng';
     * 
     * ############## AUG 26, 2014 ################
     * ALTER TABLE `gas_logger` ADD `ip_address` VARCHAR( 50 ) NOT NULL AFTER `id` ,
        ADD `country` VARCHAR( 100 ) NOT NULL AFTER `ip_address` ;
     * ALTER TABLE `gas_logger` ADD `description` VARCHAR( 250 ) NOT NULL ;
     * 
     * ############## DEC 13, 2014 ################
     * 
     * 
   
CREATE TABLE IF NOT EXISTS `gas_gas_break_task` (
  `id` bigint(11) unsigned NOT NULL AUTO_INCREMENT,
  `code_no` varchar(30) NOT NULL,
  `employee_id` bigint(11) unsigned NOT NULL COMMENT 'Nhân viên được giao việciệc',
  `job_description` varchar(450) NOT NULL COMMENT 'mô tả công việc',
  `deadline` date NOT NULL COMMENT 'deadline hết hạn cv',
  `status` tinyint(1) unsigned NOT NULL COMMENT '1: New, 2: Processing , 3: Complete',
  `uid_login` bigint(11) unsigned NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

CREATE TABLE IF NOT EXISTS `gas_gas_break_task_daily` (
  `id` bigint(11) unsigned NOT NULL AUTO_INCREMENT,
  `break_task_id` bigint(11) unsigned NOT NULL,
  `current_status` tinyint(1) unsigned DEFAULT NULL COMMENT 'status hien tai khi update report',
  `report_date` date DEFAULT NULL COMMENT 'ngay report',
  `report_content` text COMMENT 'noi dung report',
  `job_change` text COMMENT 'công việc phát sinh',
  `uid_login` bigint(11) unsigned DEFAULT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;
     * 
     * 
     * 

CREATE TABLE IF NOT EXISTS `gas_gas_support_customer` (
  `id` bigint(11) unsigned NOT NULL AUTO_INCREMENT,
  `code_no` varchar(30) NOT NULL,
  `date_request` date DEFAULT NULL,
  `customer_id` bigint(11) unsigned DEFAULT NULL,
  `type_customer` tinyint(1) unsigned DEFAULT NULL,
  `agent_id` bigint(11) unsigned DEFAULT NULL COMMENT 'khách hàng của đại lý nào',
  `status` tinyint(1) unsigned NOT NULL DEFAULT '1' COMMENT '1: new, 2: approve 1, 3: approve 2, 4: reject',
  `item_name` text COMMENT 'ten thiet bi yeu cau',
  `type` tinyint(1) unsigned DEFAULT NULL COMMENT '1: cho muon, 2: tang, 3: ban',
  `approved_uid_level_1` bigint(11) unsigned DEFAULT NULL,
  `approved_date_level_1` datetime DEFAULT NULL,
  `approved_note_level_1` text,
  `approved_uid_level_2` bigint(11) unsigned DEFAULT NULL,
  `approved_date_level_2` datetime DEFAULT NULL,
  `approved_note_level_2` text,
  `uid_login` bigint(11) unsigned NOT NULL COMMENT 'sale tao',
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `date_request` (`date_request`),
  KEY `customer_id` (`customer_id`),
  KEY `type_customer` (`type_customer`),
  KEY `type` (`type`),
  KEY `approved_uid_level_1` (`approved_uid_level_1`),
  KEY `approved_uid_level_2` (`approved_uid_level_2`),
  KEY `uid_login` (`uid_login`),
  KEY `status` (`status`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

--
     * 
     * Oct 23, 2015
--
-- Table structure for table `gas_api_request_logs`
--

CREATE TABLE IF NOT EXISTS `gas_api_request_logs` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `ip_address` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `country` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `method` text COLLATE utf8_unicode_ci,
  `content` text COLLATE utf8_unicode_ci,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=13 ;

--
-- Dumping data for table `gas_api_request_logs`
--
--
-- Table structure for table `gas_api_users_tokens`
--

CREATE TABLE IF NOT EXISTS `gas_api_users_tokens` (
  `id` bigint(11) unsigned NOT NULL AUTO_INCREMENT COMMENT 'never delete this record, just only exprired because of using APNs device token',
  `user_id` int(11) unsigned NOT NULL COMMENT 'FK',
  `mobile_number` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(50) COLLATE utf8_unicode_ci NOT NULL COMMENT 'as session for the first time login to app via mobile number',
  `apns_device_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'APNs device token to push notification',
  `gcm_device_token` varchar(350) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'GCM device token to push notification',
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=41 ;
     * 
     * 
     * 
CREATE TABLE IF NOT EXISTS `gas_api_check_signup` (
  `id` bigint(11) unsigned NOT NULL AUTO_INCREMENT,
  `code` varchar(100) NOT NULL,
  `ip_address` varchar(100) NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

-- --------------------------------------------------------

--
-- Table structure for table `gas_api_request_logs`
--

CREATE TABLE IF NOT EXISTS `gas_api_request_logs` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `ip_address` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `country` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `user_id` bigint(11) DEFAULT NULL,
  `method` text COLLATE utf8_unicode_ci,
  `content` text COLLATE utf8_unicode_ci,
  `response` text COLLATE utf8_unicode_ci NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `responsed_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=66 ;

-- --------------------------------------------------------

--
-- Table structure for table `gas_api_users_tokens`
--

CREATE TABLE IF NOT EXISTS `gas_api_users_tokens` (
  `id` bigint(11) unsigned NOT NULL AUTO_INCREMENT COMMENT 'never delete this record, just only exprired because of using APNs device token',
  `user_id` int(11) unsigned NOT NULL COMMENT 'FK',
  `mobile_number` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(50) COLLATE utf8_unicode_ci NOT NULL COMMENT 'as session for the first time login to app via mobile number',
  `apns_device_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'APNs device token to push notification',
  `gcm_device_token` varchar(350) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'GCM device token to push notification',
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=87 ;

     * 
     * 
     * 
     * 
     * 
     * @return array
     */
    
    
  
}