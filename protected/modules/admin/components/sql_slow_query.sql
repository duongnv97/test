/**
    ANH DUNG ADD Jun 11, 2018
    write log some sql get slow query and status it
    SHOW FULL PROCESSLIST;
    SHOW PROCESSLIST;
*/
/* 1. status: new
    DESCRIBE SELECT SQL_NO_CACHE * FROM `gas_gas_app_order` `t` WHERE ((((t.type=1) AND ( (t.agent_id IN (122) AND t.status=2) OR t.employee_maintain_id=874498)) AND (t.status IN (1,2,3))) AND (t.date_delivery>='2018-05-11')) AND (t.date_delivery<='2018-06-11') ORDER BY t.id DESC LIMIT 10 */

/* 2. status: new
    Nam và nghĩa thử dùng trên db c1lab1 các câu sql sau
    1/ SELECT SQL_NO_CACHE * FROM `gas_users` `t` WHERE t.role_id = '4' AND t.type =3 AND t.is_maintain IN ( 8, 7, 9 )  AND t.channel_id =1 ORDER BY t.id DESC LIMIT 0 , 30
    2/ SELECT SQL_NO_CACHE * FROM `gas_users` `t` WHERE t.role_id = 4 AND t.type =3 AND t.is_maintain IN ( 1, 2,6, 12,13 ) AND t.channel_id = '1'  AND t.status = 1 ORDER BY t.id DESC LIMIT 10 
    3/ Sau đó index cột channel_id, rồi run lại 2 câu sql trên  tại sao khi index channel_id lại chạy chậm 
    EXPLAIN SELECT * FROM `gas_users` `t` WHERE t.role_id = '4' AND t.type =3 AND t.is_maintain IN ( 8, 7, 9 )  AND t.channel_id =1 ORDER BY t.id DESC LIMIT 0 , 30

/* 3. Jul1318  status: resolve
    show variables like 'long_query_time';
    SELECT SQL_NO_CACHE DISTINCT agent_id FROM `gas_sell` `t` WHERE (t.status IN (2,3)) AND (t.complete_time>='2018-07-13 14:09:01');
    **** xử lý ngay  slow query mysql slow query slow  mysql sleep ****
    SHOW FULL PROCESSLIST;
    SHOW PROCESSLIST;
    select * from INFORMATION_SCHEMA.PROCESSLIST where db = 'c1gas35';
    1/ index field: created_by table user, check all index của field khác + table khác -- done
    2/ Tạo 1 mã tự động cho KH của CCS 
    3/ xóa lỗi xác nhận của PVKH ngày 2018-01-06 -- done 
    4/ 5 Phút resize Droplet: 1Gb testdatabase => 10h11 begin resize => 10h17 done 
    6/ 18 | Sorting result | SELECT * FROM `gas_users` `t` WHERE (((((t.created_by='1144280') AND (t.is_maintain='9')) AND (t.rol |
    7/ làm mail ngày 01 đầu tháng nhắc nhở điều phối gọi  hoặc nạp tiền cho số: 0869 246 920
    8/ test iphone: di icon test cảm ứng màn hình, gọi zalo test mic + camera trước, bấm la bàn kiểm tra lỗi chấm trên màn hình
    9/ fix index mysql https://stackoverflow.com/questions/3695768/table-with-80-million-records-and-adding-an-index-takes-more-than-18-hours-or-f
    https://www.percona.com/blog/2011/11/29/innodb-vs-mysql-index-counts/
    https://www.dreamhost.com/blog/mysql-checking-the-slow-query-log-and-simple-indexing/
    https://dev.mysql.com/doc/refman/5.5/en/explain-output.html
    https://dba.stackexchange.com/questions/129795/mysql-difficulty-with-optimizing-slow-query-with-index-but-without-improvement
    https://www.percona.com/blog/2007/08/18/how-fast-can-you-sort-data-with-mysql/
    03h 34 start import mysql -> 03h45 done


/* 4. Jul1318  status: monitor - đã tìm đc giải pháp, đang theo dõi thêm
    What is Sleep Query in MySQL Database? --
    https://www.znetlive.com/blog/what-is-sleep-query-in-mysql-database/
    https://www.a2hosting.com/kb/developer-corner/mysql/enabling-the-slow-query-log-in-mysql
https://www.experts-exchange.com/questions/27921809/Finding-MySQL-queries-creating-Sleep-processes.html
    many Sleep processes in Mysql Effects of a large number of MYSQL query – sleep :
    Increased consumption of CPU and memory resources (RAM, cache and processor).
    Slowing down of server.
    Increased downtime for websites -hackers try to slow website using sleep SQL injections
    SHOW VARIABLES LIKE 'interactive_timeout'
    1/ SET GLOBAL slow_query_log = 'ON';
    2/ File locate: /var/lib/mysql/daukhimiennam-slow.log
    3/ I found some info about it and have just inserted the following settings in the MySQL my.cnf file:
    - nano /etc/mysql/my.cnf
    interactive_timeout=60 -- run ok - default 28,800 seconds
    wait_timeout=60 -- run ok
    max_connections=250 -- chua set
    SET GLOBAL interactive_timeout = 60;
    SET GLOBAL wait_timeout = 60;
    $vi /etc/my.cnf
    wait_timeout = 60

5/ delete folder runtime when get error on log table
    CDbCommand::execute() failed: SQLSTATE[HY000]: General error: 5 database is locked. The SQL statement executed was: DELETE FROM 'YiiSession' WHERE expire<:expire. in /var/www/clients/client1/web2/web/protected/components/GasCheck.php (662) in /var/www/clients/client1/web2/web/protected/modules/admin/AdminModule.php (14) in /var/www/clients/client1/web2/web/index.php (26)


*/