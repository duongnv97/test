<?php

/**
 * This is the model class for table "{{_gas_quotation}}".
 *
 * The followings are the available columns in table '{{_gas_quotation}}':
 * @property string $id
 * @property integer $customer_id
 * @property string $price_50
 * @property string $price_45
 * @property string $price_12
 * @property string $price_month
 * @property string $price_year
 * @property integer $user_id_create
 * @property string $created_date
 */
class GasQuotation extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return GasQuotation the static model class
	 */
    public $autocomplete_name;
	public $file_excel;
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{_gas_quotation}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('customer_id, price_50, price_45, price_12, price_month,price_year', 'required', 'on'=>'create_quotation,update_quotation'),
			array('customer_id, user_id_create', 'numerical', 'integerOnly'=>true),
			array('price_50, price_45, price_12', 'length', 'max'=>16),
			array('price_month', 'length', 'max'=>2),
			array('price_year', 'length', 'max'=>4),
			array('created_date', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, customer_id, price_50, price_45, price_12, price_month, price_year, user_id_create, created_date,file_excel', 'safe'),
    array('file_excel', 'file', 'on'=>'import_gasquotation',
        'allowEmpty'=>false,
        'types'=> 'xls,xlsx',
        'wrongType'=>'Chỉ cho phép tải file xls,xlsx',
        //'maxSize' => ActiveRecord::getMaxFileSize(), // 5MB
        //'tooLarge' => 'The file was larger than '.(ActiveRecord::getMaxFileSize()/1024).' KB. Please upload a smaller file.',
    ),       			
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
                    'customer' => array(self::BELONGS_TO, 'Users', 'customer_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'customer_id' => 'Khách hàng',
			'price_50' => 'Giá bình 50',
			'price_45' => 'Giá bình 45',
			'price_12' => 'Giá bình 12',
			'price_month' => 'Tháng',
			'price_year' => 'Năm',
			'user_id_create' => 'Người tạo',
			'created_date' => 'Ngày tạo',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('t.id',$this->id,true);
		$criteria->compare('t.customer_id',$this->customer_id);
		$criteria->compare('t.price_50',$this->price_50,true);
		$criteria->compare('t.price_45',$this->price_45,true);
		$criteria->compare('t.price_12',$this->price_12,true);
		$criteria->compare('t.price_month',$this->price_month,true);
		$criteria->compare('t.price_year',$this->price_year,true);
		$criteria->compare('t.user_id_create',$this->user_id_create);
		$criteria->compare('t.created_date',$this->created_date,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
            'pagination'=>array(
                'pageSize'=> Yii::app()->user->getState('pageSize',Yii::app()->params['defaultPageSize']),
            ),
		));
	}

    /*
    public function activate()
    {
        $this->status = 1;
        $this->update();
    }

    public function deactivate()
    {
        $this->status = 0;
        $this->update();
    }
	*/

	public function defaultScope()
	{
		return array(
			//'condition'=>'',
		);
	}
	
	public static function getPriceLatestInMonth($month, $year){
			// 1. get customer id unquine in month
				$criteria=new CDbCriteria();				
				$criteria->compare('t.price_month',$month);		
				$criteria->compare('t.price_year',$year);							
				$res = GasQuotation::model()->findAll($criteria);		
				$aReturn = array();
				if($res){
					foreach($res as $obj){
						$aReturn[$res->customer_id] = $obj;						
					}
				}								
				return $aReturn;				
	}
	
}