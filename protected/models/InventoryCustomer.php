<?php

/**
 * This is the model class for table "{{_inventory_customer}}".
 *
 * The followings are the available columns in table '{{_inventory_customer}}':
 * @property string $id
 * @property integer $c_year
 * @property string $customer_id
 * @property string $customer_parent_id
 * @property integer $type_customer
 * @property string $debit
 * @property string $json_vo
 * @property string $created_date
 */
class InventoryCustomer extends BaseSpj
{
    public $aCustomerIdLimitVo = [], $view_closing_debit, $view_closing_credit, $price_code, $sAgentId='', $sCustomerId='', $province_id, $autocomplete_name, $autocomplete_name_1, $autocomplete_name_2, $temp_material, $aInventoryVo = [], $date_from, $date_to,  $date_from_ymd, $date_to_ymd;
    public $viewTheChanOnlyRevenue = 0, $viewTheChan = 0, $begin_year = 2016, $begin_date = '2017-09-01';// ngày nhập tồn đầu
    const VIEW_GENERAL  = 1;// xem tab tổng quát
    const VIEW_DETAIL   = 2; // tab chi tiết
    const VIEW_COMPARE  = 3; // tab đối chiếu
    
    const TYPE_INVENTORY        = 1; // tồn đầu của KH: vỏ, tiền 
    const TYPE_CLEAN_INVENTORY  = 2; // xóa công nợ KH: vỏ tiền, vì sẽ cấn trừ cho sale
    const TYPE_LIMIT_DEBIT      = 3; // setup giới hạn nợ vỏ, tiền: debit: vỏ, credit: tiền
    
    /** @Author: HOANG NAM 31/01/2018
     *  @Todo: Loại thống kê tồn kho vỏ
     *  @Code: NAM006
     *  @Param: 
     **/
    public $sCustomer;//search theo danh sách customer
    public $countVo     = 3; // số lượng vỏ nợ tối thiểu hiển thị
    const VIEW_COUNT    = 4; // tab khách hàng tồn kho trên 3 vỏ
    const VIEW_DATE     = 5; // tab khách hàng không lấy hàng từ 15 ngày
    
    const UPDATE_LOCK_CUSTOMER      = 1;
    const UPDATE_UNLOCK_CUSTOMER    = 2;
    
    const AMOUNT_ROUND_DOWN = 999;

    const TYPE_DEBIT_VO_12 = 1; // loại vỏ 12
    const TYPE_DEBIT_VO_45 = 2; // loại vỏ 45
    public $type_vo;

    /** @Author: DungNT Sep 11, 2017
     *  @Todo: get type không đưa vào công nợ KH
     */
    public function getTypeNotReport() {
        return [GasMasterLookup::ID_CHI_THE_CHAN, GasMasterLookup::ID_THU_THE_CHAN_BM];
    }
    public function getTypeStorecardNotReport() {
        return [STORE_CARD_TYPE_12, STORE_CARD_TYPE_21];
    }
    public function getMaterialsTypeHideInReportDebitCashCompare() {// array các materials_type_id hide in BC $model->reportDebitCashCompare();
        return [GasMaterialsType::MATERIAL_ALCOHOL];
    }
    
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }
    
    public function getCustomerPriceB12Kg() {
        /** @Note: Dec1817 các KH này chỉ lấy B12 và có cân gas dư, nếu lấy B45 nữa thì ko rõ có đúng hay không */
        return [
            524210,// beign Hy Lam Mon
            506049,
            560588,
            652634,// End hệ thống Hy Lam Mon
            886292,// End VIET00083-Nhà Hàng Việt
            1745626, // CARMELINA00001 Resort carmelina
        ];
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{_inventory_customer}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        return array(
            array('customer_id,', 'required', 'on'=>'create, update'),
            array('uid_login, id, c_year, customer_id, customer_parent_id, type_customer, debit, credit, json_vo, created_date', 'safe'),
            array('type, viewTheChanOnlyRevenue, view_closing_debit, view_closing_credit, viewTheChan, province_id, agent_id, sale_id, date_from, date_to,  date_from_ymd, date_to_ymd', 'safe'),
            array('customer_id', 'checkUniqueRecord', 'on'=>'update'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        return array(
            'rCustomer' => array(self::BELONGS_TO, 'Users', 'customer_id'),
            'rUidLogin' => array(self::BELONGS_TO, 'Users', 'uid_login'),
            'rAgent' => array(self::BELONGS_TO, 'Users', 'agent_id'),
            'rSale' => array(self::BELONGS_TO, 'Users', 'sale_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        $aLabel = array(
//            @Code: NAM006
            'countVo'=>'Vỏ tồn kho tối thiểu',
            
            'id' => 'ID',
            'c_year' => 'C Year',
            'customer_id' => 'Khách hàng',
            'customer_parent_id' => 'Customer Parent',
            'type_customer' => 'Loại KH',
            'debit' => 'Nợ đầu kỳ', // hạn mức nợ vỏ - type = InventoryCustomer::TYPE_LIMIT_DEBIT
            'credit' => 'Có đầu kỳ', // hạn mức nợ tiền - type = InventoryCustomer::TYPE_LIMIT_DEBIT
            'json_vo' => 'Công nợ vỏ',
            'created_date' => 'Ngày tạo',
            'uid_login' => 'Người tạo',
            'date_from' => 'Từ ngày',
            'date_to' => 'Đến',
            'agent_id' => 'Đại lý',
            'sale_id' => 'Nhân viên kinh doanh',
            'province_id' => 'Tỉnh',
            'price_code' => 'Nhóm giá',
            'viewTheChan' => 'Xem công nợ thế chân vỏ',
            'view_closing_debit' => 'Xem nợ cuối kỳ',
            'view_closing_credit' => 'Xem có cuối kỳ',
            'viewTheChanOnlyRevenue' => 'Chỉ xem thu, chi thế chân',
            
            
        );
        if($this->type == InventoryCustomer::TYPE_LIMIT_DEBIT){
            $aLabel['debit'] = 'nợ vỏ bò';
            $aLabel['credit'] = 'nợ vỏ 12';
        }
        return $aLabel;
    }

    public function search(){
        $criteria=new CDbCriteria;
        $criteria->compare('t.c_year',$this->c_year);
        $criteria->compare('t.customer_id',$this->customer_id);
        $criteria->compare('t.type_customer',$this->type_customer);
        $criteria->compare('t.uid_login', $this->uid_login);
        $criteria->compare('t.type', $this->type);
        $criteria->order = 't.id DESC';
        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
            'pagination'=>array(
                'pageSize'=> Yii::app()->user->getState('pageSize',Yii::app()->params['defaultPageSize']),
            ),
        ));
    }
    
    /** @Author: DungNT Oct 28, 2018
     *  @Todo: check unique record KH
     **/
    public function checkUniqueRecord() {
        $criteria = new CDbCriteria();
        $criteria->addCondition("t.customer_id = $this->customer_id AND t.type = $this->type AND t.id <> $this->id");
        $model = self::model()->find($criteria);
        if($model){
            $this->addError('customer_id', 'Khách hàng này đã được setup tồn đầu, bạn vui lòng kiểm tra lại');
        }
    }
    
    public function getCustomer($field_name='first_name') {
        $mUser = $this->rCustomer;
        if($mUser){
            if($field_name == 'type_customer'){
                return isset(CmsFormatter::$CUSTOMER_BO_MOI[$this->type_customer]) ?  CmsFormatter::$CUSTOMER_BO_MOI[$this->type_customer] : '';;
            }
            return $mUser->$field_name;
        }
        return '';
    }
    public function getUserLogin($field_name='') {
        $mUser = $this->rUserLogin;
        if($mUser){
            if($field_name != ''){
                return $mUser->$field_name;
            }
//            return "<b>".$mUser->code_account."-".$mUser->first_name."</b><br>".$mUser->address."<br><b>Phone: </b>".$mUser->phone;
            return $mUser->first_name;
        }
        return '';
    }
    
    public function getJsonVoView() {
        if(empty($this->json_vo)){
            return '';
        }
        $mAppCache = new AppCache();
        $aMaterial = $mAppCache->getListdata('GasMaterials', AppCache::LISTDATA_MATERIAL);
        $json_vo = json_decode($this->json_vo, true);
        $str = '';$key = 0;
        foreach($json_vo as $materials_id_vo => $qty){
            $name = isset($aMaterial[$materials_id_vo]) ? $aMaterial[$materials_id_vo]  : '';
            $br = '';
            if($key++ != 0){
                $br = '<br>';
            }
            $str .= "$br <b>SL: $qty</b> - $name";
        }
        return $str;
    }
    public function getDebit($format = false) {
        if($format){
            return ($this->debit > 0) ? ActiveRecord::formatCurrency($this->debit) : '';
        }
        return $this->debit;
    }
    public function getCredit($format = false) {
        if($format){
            return ($this->credit > 0) ? ActiveRecord::formatCurrency($this->credit) : '';
        }
        return $this->credit;
    }
    
    public function getAgent() {
        $mUser = $this->rAgent;
        if($mUser){
            return $mUser->first_name;
        }
        return '';
    }
    public function getSale() {
        $mUser = $this->rSale;
        if($mUser){
            return $mUser->first_name;
        }
        return '';
    }
        
    public function canUpdate() {
        $cRole  = MyFormat::getCurrentRoleId();
        $cUid   = MyFormat::getCurrentUid();
        if($cRole == ROLE_ADMIN){
            return true;
        }
        $aUidAllow = [GasConst::UID_HA_PT, GasLeave::UID_HEAD_GAS_BO];
        if( in_array($this->uid_login, $aUidAllow) ){
            return true;// Jul2817 cho Nguyên sửa lại hết của hcm dn bd để nhập vỏ
        }
        
        if($cUid != $this->uid_login){
            return false;
        }
        
        $dayAllow = date('Y-m-d');
        $dayAllow = MyFormat::modifyDays($dayAllow, 10, '-');
//        $dayAllow = MyFormat::modifyDays($dayAllow, 1000, '-');
        return MyFormat::compareTwoDate($this->created_date, $dayAllow);
    }
    
    protected function beforeValidate() {
        $this->setInfoCustomer();
        $this->trimData();
        $this->setJsonUpdate();
        return parent::beforeValidate();
    }

    public function setInfoCustomer() {
        $mCustomer = $this->rCustomer;
        if($mCustomer){
            $this->customer_parent_id   = !empty($mCustomer->parent_id) ? $mCustomer->parent_id : 0;
            $this->type_customer        = !empty($mCustomer->is_maintain) ? $mCustomer->is_maintain : 0; 
            $this->agent_id             = !empty($mCustomer->area_code_id) ? $mCustomer->area_code_id : 0;
            $this->sale_id              = !empty($mCustomer->sale_id) ? $mCustomer->sale_id : 0;
        }
    }
    
    public function trimData() {
        $this->debit    = MyFormat::removeComma($this->debit);
        $this->credit   = MyFormat::removeComma($this->credit);
    }
    public function setJsonUpdate() {
        if(!isset($_POST['materials_id_vo'])){
            return ;
        }
        $json = [];
        foreach($_POST['materials_id_vo'] as $k1 => $materials_id_vo){
            if( !empty($materials_id_vo) && isset($_POST['qty'][$k1])){
                $json[$materials_id_vo] = $_POST['qty'][$k1];
            }
        }
        $this->json_vo = json_encode($json);
    }
    public function getJsonUpdate() {
        $this->aInventoryVo = json_decode($this->json_vo, true);
    }
    
    public function runSqlInsert() {
        if(!isset($_POST['customer_id'])){
            return ;
        }
        if(!isset($_POST['materials_id_vo'])){
            return ;
        }
        $created_date = date('Y-m-d H:i:s');
        $aRowInsert = $aCustomerCheck = []; $sErrors = '';
        $cUid = MyFormat::getCurrentUid();
        $aCustomerSetup = $this->getListDataCustomer();
        
        foreach($_POST['customer_id'] as $key => $customer_id){
            $mCustomer = Users::model()->findByPk($customer_id);
            if(in_array($customer_id, $aCustomerCheck) || in_array($customer_id, $aCustomerSetup)){
                $sErrors .= "<br>".$mCustomer->getFullName();
                continue ;
            }

            $debit  = isset($_POST['debit'][$key]) ? MyFormat::removeComma($_POST['debit'][$key]) : 0;
            $credit = isset($_POST['credit'][$key]) ? MyFormat::removeComma($_POST['credit'][$key]) : 0;
            $json = [];
            if(isset($_POST['materials_id_vo'][$customer_id])){
                foreach($_POST['materials_id_vo'][$customer_id] as $k1 => $materials_id_vo){
                    if(!empty($materials_id_vo) && isset($_POST['qty'][$customer_id][$k1])){
                        $json[$materials_id_vo] = $_POST['qty'][$customer_id][$k1];
                    }
                }
            }
            $json_vo = json_encode($json);
            
            $aRowInsert[]="(
                    '$this->type',
                    '$cUid',
                    '$this->begin_year',
                    '$customer_id',
                    '$mCustomer->parent_id',
                    '$mCustomer->is_maintain',
                    '$mCustomer->area_code_id',
                    '$mCustomer->sale_id',
                    '$debit',
                    '$credit',
                    '$json_vo',
                    '$created_date'
                    )";
            $aCustomerCheck[$customer_id] = $customer_id;
        }
        $sql = $this->buildSqlInsert($aRowInsert);
        if(count($aRowInsert)){
            Yii::app()->db->createCommand($sql)->execute();
        }
        
        if(!empty($sErrors)){
            $this->addError('id', "Bạn nhập trùng khách hàng, vui lòng quay lại danh sách để kiểm tra. KH trùng: {$sErrors}");
        }
    }
    
    /** @Author: DungNT Jan 09, 2019
     *  @Todo: build sql insert
     **/
    public function buildSqlInsert($aRowInsert) {
        $tableName = InventoryCustomer::model()->tableName();            
        $sql = "insert into $tableName (
                        type,
                        uid_login,
                        c_year,
                        customer_id,
                        customer_parent_id,
                        type_customer,
                        agent_id,
                        sale_id,
                        debit,
                        credit,
                        json_vo,
                        created_date
                        ) values ".implode(',', $aRowInsert);
        return $sql;
    }

    /**
     * @Author: DungNT Jun 04, 2017
     * @Todo: get all record customer id đã setup 
     */
    public function getListDataCustomer() {
        $criteria = new CDbCriteria();
        if(!empty($this->begin_year)){
            $criteria->addCondition('t.c_year=' . $this->begin_year);
        }
        if(!empty($this->type)){
            $criteria->addCondition('t.type=' . $this->type);
        }
        $models = self::model()->findAll($criteria);
        return CHtml::listData($models, 'customer_id', 'customer_id');
    }
    
    /**
     * @Author: DungNT Jun 04, 2017
     * @Todo: BC tồn tiền KH trong khoảng ngày
     * 1. lấy tồn đầu Tiền trong table InventoryCustomer
     * 2. Tính phải thu KH bên table GasAppOrder
     * 3. Tính đã thu KH trong employee Cashbook
     * @note: Khoản phải thu: Nợ (phải thu), Có (đã thu) 
     * @note: Khoản phải trả: Nợ (đã trả), Có (phải trả) 
     * Fiscal Year năm tài chính.
        Accounting Period kỳ kế toán. Có thể là tháng, quý, năm tùy thuộc yêu cầu báo cáo.
        Opening Debit số dư đầu kỳ bên nợ.
        Opening Credit số dư đầu kỳ bên có.
        Closing Debit số dư cuối kỳ bên nợ.
        Closing Credit số dư đầu kỳ bên có.
     */
    public function reportDebitCash() {
        $this->reportDebitInit();
        $aRes = [];
        $this->reportVoGetBegin($aRes, InventoryCustomer::TYPE_INVENTORY);
        // 1. tính phát sinh DEBIT-NỢ (đầu kỳ) từ ngày setup đến date_from để tính ra DEBIT-NỢ (đã thu) đầu kỳ
        $date_from  = $this->begin_date;
        $date_to    = MyFormat::modifyDays($this->date_from_ymd, 1, '-');
        $begin_date_to = $date_to;
        $nameVar    = 'DATA_OPENING_DEBIT';
        $this->reportCashGetDebit($aRes, $nameVar, $date_from, $date_to);
        
        // 2. tính phát sinh DEBIT-NỢ (trong kỳ ) trong khoảng $date_from, $date_to 
        $date_from  = $this->date_from_ymd;
        $date_to    = $this->date_to_ymd;
        $nameVar    = 'DATA_IN_PERIOD_DEBIT';
        $this->reportCashGetDebit($aRes, $nameVar, $date_from, $date_to);
        
        // 3. tính phát sinh CREDIT-CÓ (đầu kỳ) từ ngày setup đến date_from để tính ra CREDIT-CÓ (phải thu) đầu kỳ
        $date_from  = $this->begin_date;
        $date_to    = $begin_date_to;
        $nameVar    = 'DATA_OPENING_CREDIT';
        $this->reportCashGetCredit($aRes, $nameVar, $date_from, $date_to);
        // 4. tính phát sinh CREDIT-CÓ (trong kỳ ) trong khoảng $date_from, $date_to 
        $date_from  = $this->date_from_ymd;
        $date_to    = $this->date_to_ymd;
        $nameVar    = 'DATA_IN_PERIOD_CREDIT';
        $this->reportCashGetCredit($aRes, $nameVar, $date_from, $date_to);
        
        if(isset($aRes['CUSTOMER_ID'])){
            $aRes['CUSTOMER_MODEL'] = Users::getArrObjectUserByRole(ROLE_CUSTOMER, $aRes['CUSTOMER_ID']);
        }
        $_SESSION['data-excel'] = $aRes;
        $_SESSION['data-model'] = $this;
        return $aRes;
    }
    
    /** @Author: DungNT Jul 29, 2017
     * @Todo:  tính Có (đã thu) KH - trong table gas_employee_cashbook
     */
    public function reportCashGetCredit(&$aRes, $nameVar, $date_from, $date_to) {
        $criteria = new CDBcriteria();
        $this->reportCashSameCriteria($criteria, $date_from, $date_to, 'date_input', 'agent_id');
        $criteria->select   = 'sum(amount) as amount, t.agent_id, t.lookup_type, t.master_lookup_id, t.customer_id, t.sale_id';
        $criteria->group    = 't.customer_id, t.lookup_type, t.master_lookup_id';
        $mRes = EmployeeCashbook::model()->findAll($criteria);
        $nameDebit = 'DATA_OPENING_DEBIT';// Aug1617 xử lý đưa chi tiền thế chân sang bên nợ
        if($nameVar == 'DATA_IN_PERIOD_CREDIT'){
            $nameDebit = 'DATA_IN_PERIOD_DEBIT';
        }
        
        foreach($mRes as $item){
            if(in_array($item->master_lookup_id, $this->getTypeNotReport()) && !$this->viewTheChan && !$this->viewTheChanOnlyRevenue){
                continue;// Sep1117 bỏ 2 loại thu chi thế chân bò mối ra không tính
            }
            
            if($this->viewTheChanOnlyRevenue && !in_array($item->master_lookup_id, $this->getTypeNotReport())){
                continue ;// Jul018 chỉ xem tiền thu, chi thế chân vỏ của KH - ko xem dau ky
            }
            
            if($item->lookup_type == GasMasterLookup::MASTER_TYPE_CHI){// Aug1717 có chi là đưa sang bên nợ
                if(!isset($aRes[$nameDebit][$item->customer_id])){
                    $aRes[$nameDebit][$item->customer_id] = $item->amount;
                }else{
                    $aRes[$nameDebit][$item->customer_id] += $item->amount;
                }
            }else{
                if(!isset($aRes[$nameVar][$item->customer_id][$item->lookup_type])){
                    $aRes[$nameVar][$item->customer_id][$item->lookup_type] = $item->amount;
                }else{
                    $aRes[$nameVar][$item->customer_id][$item->lookup_type] += $item->amount;
                }
            }
            $aRes['CUSTOMER_ID'][$item->customer_id]        = $item->customer_id;
        }
    }
    
    /** @Author: DungNT Jul 29, 2017
     * @Todo: tính Nợ (phải thu) KH - trong table GasAppOrder
     */
    public function reportCashGetDebit(&$aRes, $nameVar, $date_from, $date_to) {
        if($this->viewTheChanOnlyRevenue){
            return ;// Jul018 chỉ xem tiền thu, chi thế chân vỏ của KH - ko xem dau ky
        }
        $criteria = new CDBcriteria();
        $this->reportCashSameCriteria($criteria, $date_from, $date_to, 'date_delivery', 'agent_id');
        $criteria->addCondition('t.status=' . GasAppOrder::STATUS_COMPPLETE);
        $criteria->select   = 'sum(total_gas) as total_gas, sum(total_gas_du) as total_gas_du, sum(pay_back_amount) as pay_back_amount, sum(discount) as discount, t.agent_id, t.customer_id, t.sale_id';
        $criteria->group    = 't.customer_id';
        $mRes = GasAppOrder::model()->findAll($criteria);
        
        $nameCredit = 'DATA_OPENING_CREDIT';// Aug1617 xử lý luôn đưa tiền gas dư sang bên có
        if($nameVar == 'DATA_IN_PERIOD_DEBIT'){
            $nameCredit = 'DATA_IN_PERIOD_CREDIT';
        }

        foreach($mRes as $item){
            $aRes[$nameVar][$item->customer_id]         = $item->total_gas;
            $aRes['CUSTOMER_ID'][$item->customer_id]    = $item->customer_id;
            
            $totalCredit = $item->total_gas_du + $item->pay_back_amount + $item->discount;
            if($totalCredit < 1){// Aug2817 xử lý trả lại gas dư + giảm giá
                continue;
            }
            if(!isset($aRes[$nameCredit][$item->customer_id][GasMasterLookup::MASTER_TYPE_THU])){
                $aRes[$nameCredit][$item->customer_id][GasMasterLookup::MASTER_TYPE_THU] = $totalCredit;
            }else{
                $aRes[$nameCredit][$item->customer_id][GasMasterLookup::MASTER_TYPE_THU] += $totalCredit;
            }
        }
    }
    
    public function reportCashSameCriteria(&$criteria, $date_from, $date_to, $fieldDate, $fieldAgentId) {
        if($fieldDate == 'date_delivery'){
            DateHelper::searchBetween($date_from, $date_to, 'date_delivery_bigint', $criteria, false);
        }elseif($fieldDate == 'date_input'){
            DateHelper::searchBetween($date_from, $date_to, 'date_input_bigint', $criteria, false);
        }else{
            throw new Exception("reportCashSameCriteria $fieldDate chưa được định nghĩa, liên hệ Dũng IT hỗ trợ 0384 331 552");
//            $criteria->addBetweenCondition("t.$fieldDate", $date_from, $date_to);
        }
        if(!empty($this->customer_id)){
            $criteria->addCondition('t.customer_id=' . $this->customer_id);
        }
        if(!empty($this->type_customer)){
            $criteria->addCondition('t.type_customer=' . $this->type_customer);
            // Jan2818 không  thể bỏ điều kiện này vì sẽ không giới hạn được theo search loại KH
            // Close on Jan2818 vì gây lêch công nợ tổng quát 
//            khi xem theo loại KH bò mối =>  do KH chuyển từ KH bò sang mối hoặc ngược lại
        }
        if(!empty($this->agent_id)){
            $criteria->addCondition("t.$fieldAgentId=" . $this->agent_id);
        }
        if(!empty($this->sale_id)){
            $criteria->addCondition('t.sale_id=' . $this->sale_id);
        }
        $this->limitAgent($criteria, $this->sAgentId, $fieldAgentId);
        $this->limitAgent($criteria, $this->sCustomerId, 'customer_id');
    }
    
    /** * @Author: DungNT Jul 29, 2017 - khởi tạo một số biến dùng chung cho 2 report
     */
    public function reportDebitInit() {
        $this->date_from_ymd    = MyFormat::dateDmyToYmdForAllIndexSearch($this->date_from);
        $this->date_to_ymd      = MyFormat::dateDmyToYmdForAllIndexSearch($this->date_to);
        $aAgentId               = $this->getAgentOfProvince($this->province_id);
        $aCustomerId            = $this->getCustomerByPriceCode($this->price_code);
        if(count($this->aCustomerIdLimitVo)){// DungNT Fix Jan1619
            $aCustomerId = array_merge($aCustomerId, $this->aCustomerIdLimitVo);
        }
        $this->sAgentId         = implode(',', $aAgentId);
        $this->sCustomerId      = implode(',', $aCustomerId);
    }
    /** @Author: DungNT Jun 04, 2017
     * @Todo: BC tồn Vỏ KH trong khoảng ngày
     * 1. lấy tồn đầu Vỏ trong table InventoryCustomer
     * 2. Lấy sản lượng bình đã bán + vỏ thu về của KH bên table Gasstorecard (thẻ kho )
     * 3. lấy vỏ tương ứng gas setup
     * 4. Đổi từ gas sang vỏ tương ứng và render báo cáo Excel
     */
    public function reportDebitVo() {
        $this->reportDebitInit();
        $aRes = [];
        $this->reportVoGetBegin($aRes, InventoryCustomer::TYPE_INVENTORY);
        $mAppCache = new AppCache();
        $aGasVo = $mAppCache->getListdataGasVo();
        // 1. tính nhập xuất từ ngày setup đến date_from
        $date_from  = $this->begin_date;
        $date_to    = MyFormat::modifyDays($this->date_from_ymd, 1, '-');
        $nameVar    = 'OUTPUT_BEFORE';
        $this->reportVoGetOutput($aRes, $nameVar, $aGasVo, $date_from, $date_to);
        
        // 2. tính nhập xuất trong khoảng $date_from, $date_to
        $date_from  = $this->date_from_ymd;
        $date_to    = $this->date_to_ymd;
        $nameVar    = 'OUTPUT_IN_PERIOD';
        $this->reportVoGetOutput($aRes, $nameVar, $aGasVo, $date_from, $date_to);
        $this->removeCustomerDebt($aRes); // Xóa công nợ khách hàng

        if(isset($aRes['CUSTOMER_ID'])){
            $aRes['CUSTOMER_MODEL'] = Users::getArrObjectUserByRole(ROLE_CUSTOMER, $aRes['CUSTOMER_ID']);
        }
        $aType = [GasMaterialsType::MATERIAL_VO_50, GasMaterialsType::MATERIAL_VO_45, GasMaterialsType::MATERIAL_VO_12, GasMaterialsType::MATERIAL_VO_6, GasMaterialsType::MATERIAL_VO_4];
        $aRes['MATERIALS_MODEL'] = GasMaterials::getArrayIdByMaterialTypeId($aType, ['ListIdModel'=>1]);
//        sắp xếp theo thứ tự bình nhỏ đến bình lớn
        $this->groupTypeVo($aRes,$aRes['MATERIALS_MODEL']);
        return $aRes;
    }
    
    /** @Author: DuongNV 15 mar 19
     *  @Todo: Xóa công nợ khách hàng
     **/
    public function removeCustomerDebt(&$aRes) {
        $aCustomerDebt = $this->getArrayCustomerDebt();
        foreach ($aCustomerDebt as $customer_id => $detail) {
            foreach ($detail as $materials_id => $qty) {
                if( isset($aRes['OUTPUT_IN_PERIOD'][TYPE_STORE_CARD_IMPORT][$customer_id][$materials_id]) ){
                    $aRes['OUTPUT_IN_PERIOD'][TYPE_STORE_CARD_IMPORT][$customer_id][$materials_id] += $qty;
                } else {
                    $aRes['OUTPUT_IN_PERIOD'][TYPE_STORE_CARD_IMPORT][$customer_id][$materials_id]  = $qty;
                }
                $aRes['ID_VO'][$customer_id][$materials_id] = 0;
            }
        }
    }
    
    /** @Author: DuongNV 15 mar 19
     *  @Todo: get array ds công nợ khách hàng
     **/
    public function getArrayCustomerDebt() {
        $criteria       = new CDbCriteria;
        $criteria->compare('t.type', self::TYPE_CLEAN_INVENTORY);
        $models         = InventoryCustomer::model()->findAll($criteria);
        $aCustomerDebt  = [];
        foreach ($models as $value) {
            $aJsonVo    = json_decode($value->json_vo, true);
            foreach ($aJsonVo as $materials_id => $qty) {
                $aCustomerDebt[$value->customer_id][$materials_id] = $qty;
            }
        }
        return $aCustomerDebt;
    }
    
    /** @Author: DungNT Jun 04, 2017
     * 1. lấy tồn đầu Vỏ trong table InventoryCustomer
     */
    public function reportVoGetBegin(&$aRes, $type) {
        if($this->viewTheChanOnlyRevenue){
            return ;// Jul018 chỉ xem tiền thu, chi thế chân vỏ của KH - ko xem dau ky
        }
        $criteria = new CDbCriteria();
        $criteria->addCondition('t.c_year = ' . $this->begin_year);
        $criteria->addCondition('t.type = ' . $type);
        if(!empty($this->customer_id)){
            if(is_array($this->customer_id)){
                $criteria->addInCondition('t.customer_id', $this->customer_id);
            }else{
                $criteria->addCondition('t.customer_id=' . $this->customer_id);
            }
        }
        if(!empty($this->type_customer)){
            $criteria->addCondition('t.type_customer=' . $this->type_customer);
        }
        if(!empty($this->agent_id)){
            $criteria->addCondition('t.agent_id=' . $this->agent_id);
        }
        if(!empty($this->sale_id)){
            $criteria->addCondition('t.sale_id=' . $this->sale_id);
        }
        $this->limitAgent($criteria, $this->sAgentId, 'agent_id');
         //Toan Jan 12
        $this->limitAgent($criteria, $this->sCustomerId, 'customer_id');
        $models = InventoryCustomer::model()->findAll($criteria);
        foreach($models as $item){
            $json_vo = json_decode($item->json_vo, true);
            if(is_array($json_vo)){
                $aRes['BEGIN_VO'][$item->customer_id]           = $json_vo;// Tồn đầu
                $aRes['ID_VO'][$item->customer_id]              = $json_vo;// array id vỏ để foreach cho mỗi KH
            }
            $aRes['BEGIN_CASH'][$item->customer_id]['debit']    = $item->debit;
            $aRes['BEGIN_CASH'][$item->customer_id]['credit']   = $item->credit;
            $aRes['CUSTOMER_ID'][$item->customer_id]            = $item->customer_id;
        }
        $models = null;
    }
    
    /** @Author: DungNT Jun 05, 2017 - get output in range dtae */
    public function reportVoGetOutput(&$aRes, $nameVar, $aGasVo, $date_from, $date_to) {
        $criteria = new CDBcriteria();
        $this->reportVoGetOutputCriteria($criteria, $date_from, $date_to);
        $mRes = GasStoreCardDetail::model()->findAll($criteria);
        foreach($mRes as $item){
            $materials_id = $item->materials_id;
            if(array_key_exists($materials_id, $aGasVo)){
                $materials_id = $aGasVo[$materials_id];// chuyển id Gas sang id Vỏ
            }
            if(!isset($aRes[$nameVar][$item->type_store_card][$item->customer_id][$materials_id])){
                $aRes[$nameVar][$item->type_store_card][$item->customer_id][$materials_id]    = $item->qty;// phát sinh trong khoảng thời gian
            }else{
                $aRes[$nameVar][$item->type_store_card][$item->customer_id][$materials_id]    += $item->qty;
            }
            $aRes['ID_VO'][$item->customer_id][$materials_id] = 0;
            $aRes['SALE_ID'][$item->sale_id]                = $item->sale_id;
            $aRes['CUSTOMER_ID'][$item->customer_id]        = $item->customer_id;
        }
    }
    
    public function reportVoGetOutputCriteria(&$criteria, $date_from, $date_to) {
        DateHelper::searchBetween($date_from, $date_to, 'date_delivery_bigint', $criteria, false);
        if(!empty($this->customer_id)){
            if(is_array($this->customer_id)){
                $criteria->addInCondition('t.customer_id', $this->customer_id);
            }else{
                $criteria->addCondition('t.customer_id=' . $this->customer_id);
            }
        }
        if(!empty($this->type_customer)){
            $criteria->addCondition('t.type_customer=' . $this->type_customer);
        }
        $criteria->addInCondition('t.type_customer', CmsFormatter::$ARR_TYPE_CUSTOMER_STORECARD);
        if(!empty($this->agent_id)){
            $criteria->addCondition('t.user_id_create=' . $this->agent_id);
        }
        if(!empty($this->sale_id)){
            $criteria->addCondition('t.sale_id=' . $this->sale_id);
        }
        
        $sParamsIn = implode(',', $this->getTypeStorecardNotReport());
        $criteria->addCondition("t.type_in_out NOT IN ($sParamsIn)");
        
        $this->limitAgent($criteria, $this->sAgentId, 'user_id_create');
          //Toan Jan 12
        $this->limitAgent($criteria, $this->sCustomerId, 'customer_id');
//        $this->limitAgent($criteria, $this->sCustomerId, 'customer_id');
        $sParamsIn = implode(',', CmsFormatter::$MATERIAL_TYPE_BINHBO_12);
        $criteria->addCondition("t.materials_type_id IN ($sParamsIn)");
        $criteria->select   = 'sum(qty) as qty, t.type_store_card, t.user_id_create, t.materials_type_id, t.materials_id, t.customer_id, t.sale_id';
        $criteria->group    = 't.type_store_card, t.customer_id, t.materials_id';
    }
    
    /** @Author: DungNT Jul 28, 2017
     * @Todo: check data xem có thể xuất excel không hay là để dưới view
     */
    public function canToExcel() {
        if(isset($_GET['ExportExcel'])){
            return true;
        }
        if(empty($this->customer_id) && empty($this->agent_id) && empty($this->sale_id)){
            return true;
        }
        return false;
    }
    /** @Author: Pham Thanh Nghia Aug 7, 2018
     *  @Todo: Xét quyền những người được xuất file excel
     *  @Param:
     **/
    public function canToExcelForView() {
        return true;
    }
    public function canToExcelDebitCash() {
        return false;// Sep1317 Thúc không muốn xuất excel
        if(!empty($this->customer_id) || !empty($this->agent_id) || !empty($this->sale_id)){
            return false;
        }
        return true;
    }
    
    /** @Author: DungNT Jul 28, 2017
     * @Todo: get criterial limit agent by arrray province_id
     */
    public function limitAgent(&$criteria, $sParamsIn, $fieldName) {
        if(!empty($sParamsIn)){
            $criteria->addCondition("t.$fieldName IN ($sParamsIn)");
        }
    }
    
    /** @Author: DungNT Jul 28, 2017
     * @Todo: get list agent by array province
     */
    public function getAgentOfProvince($aProvince) {
        if(!is_array($aProvince)){
            return [];
        }
        $aRes = [];
        $mAppCache = new AppCache();
        $aAgent = $mAppCache->getAgent();
        foreach($aAgent as $item){
            if(in_array($item['province_id'], $aProvince)){
                $aRes[] = $item['id'];
            }
        }
        return $aRes;
    }
    
    /** @Author: DungNT Jul 30, 2017
     * @Todo: get list customer by array price_code
     */
    public function getCustomerByPriceCode($aPriceCode) {
        if(!is_array($aPriceCode)){
            return [];
        }
        $temp = explode('-', $this->date_from);
        $mUsersPrice = new UsersPrice();
        $mUsersPrice->c_month   = $temp[1];
        $mUsersPrice->c_year    = $temp[2];
        return $mUsersPrice->getCustomerByPriceCode();
    }
    
    /** @Author: DungNT Jul 29, 2017
     * @Todo: tính nợ của KH trên data cashbook
     * vì tiền của KH có thu tiền mặt và chi gas dư, nên ở đây phải trừ đi gas dư
     */
    public function calcDebitCustomer($aData, $customer_id) {
        $in     = isset($aData[$customer_id][GasMasterLookup::MASTER_TYPE_THU]) ? $aData[$customer_id][GasMasterLookup::MASTER_TYPE_THU] : 0;
        $out    = isset($aData[$customer_id][GasMasterLookup::MASTER_TYPE_CHI]) ? $aData[$customer_id][GasMasterLookup::MASTER_TYPE_CHI] : 0;
        return $in - $out;
    }

    /** @Author: DungNT Jul 30, 2017
     * @Todo: tính nợ hoặc có cuối kỳ
     *  công thức: nợ + nợ - có - có = A
    * Nếu A > 0 thì A nằm bên nợ cuối kỳ
    * Nếu A < 0 thì A nằm bên có cuối kỳ
     */
    public function calcClosingDebitCredit($openingDebit, $openingCredit, $incurredDebit, $incurredCredit, &$closingDebit, &$closingCredit) {
        $amount = ($openingDebit + $incurredDebit) - ($openingCredit + $incurredCredit);
        if($amount > 0){
            $closingDebit = $amount;
        }elseif($amount < 0){
            $closingCredit = $amount*-1;
        }
    }
    
    /** @Author: DungNT Jul 31, 2017
     * @Todo: chi tiết công nợ KH
     */
    public function reportDebitCashDetail() {
        /* 1. tính dư đầu kỳ
         * 2. find list phát sinh bán hàng và thu tiền trong kỳ
         * 2.1 tính riêng list bán hàng cho ra array day: model GasAppOrder
         * 2.2 tính riêng list thu tiền KH cho ra array day: model cashbook
         */
        if(empty($this->customer_id)){
            return [];
        }
        $aRes = [];
        $this->calcOpeningBalance($aRes);
        $this->getOrderDaily($aRes);
        $this->getCustomerPay($aRes);
        $aRes['ARR_DAYS'] = MyFormat::getArrayDay($this->date_from_ymd, $this->date_to_ymd);
        $_SESSION['data-excel'] = $aRes;
        $_SESSION['data-model'] = $this;
        return $aRes;
    }
    
    /** @Author: DungNT Jul 19, 2017
     * @Todo: tính dư đầu kỳ của 1 KH
     */
    public function calcOpeningBalance(&$aRes) {
        $this->reportDebitInit();
        $aData = [];
        $this->reportVoGetBegin($aData, InventoryCustomer::TYPE_INVENTORY);
        // 1. tính phát sinh DEBIT-NỢ (đầu kỳ) từ ngày setup đến date_from để tính ra DEBIT-NỢ (đã thu) đầu kỳ
        $date_from  = $this->begin_date;
        $date_to    = MyFormat::modifyDays($this->date_from_ymd, 1, '-');
        $begin_date_to = $date_to;
        $nameVar    = 'DATA_OPENING_DEBIT';
        $this->reportCashGetDebit($aData, $nameVar, $date_from, $date_to);
        // 3. tính phát sinh CREDIT-CÓ (đầu kỳ) từ ngày setup đến date_from để tính ra CREDIT-CÓ (phải thu) đầu kỳ
        $date_from  = $this->begin_date;
        $date_to    = $begin_date_to;
        $nameVar    = 'DATA_OPENING_CREDIT';
        $this->reportCashGetCredit($aData, $nameVar, $date_from, $date_to);
        $customer_id = $this->customer_id;
        $closingDebit1 = $closingCredit1 = 0;
        $BEGIN_CASH             = isset($aData['BEGIN_CASH']) ? $aData['BEGIN_CASH'] : [];
        $DATA_OPENING_DEBIT     = isset($aData['DATA_OPENING_DEBIT']) ? $aData['DATA_OPENING_DEBIT'] : [];
        $DATA_OPENING_CREDIT    = isset($aData['DATA_OPENING_CREDIT']) ? $aData['DATA_OPENING_CREDIT'] : [];

        $openingDebit1          = isset($BEGIN_CASH[$customer_id]['debit']) ? $BEGIN_CASH[$customer_id]['debit'] : 0;
        $openingCredit1         = isset($BEGIN_CASH[$customer_id]['credit']) ? $BEGIN_CASH[$customer_id]['credit'] : 0;
        // vì tiền của KH có thu tiền mặt và chi gas dư, nên ở đây phải trừ đi gas dư
        $incurredDebit1         = isset($DATA_OPENING_DEBIT[$customer_id]) ? $DATA_OPENING_DEBIT[$customer_id] : 0;// phải thu KH
//        $incurredCredit1        = $this->calcDebitCustomer($DATA_OPENING_CREDIT, $customer_id);// tính thu trong kỳ không được trừ gas dư
        $incurredCredit1        = isset($DATA_OPENING_CREDIT[$customer_id][GasMasterLookup::MASTER_TYPE_THU]) ? $DATA_OPENING_CREDIT[$customer_id][GasMasterLookup::MASTER_TYPE_THU] : 0;// Jul0317 đã thu - không trừ gas dư

        // 1. nợ và có đầu kỳ
        $this->calcClosingDebitCredit($openingDebit1, $openingCredit1, $incurredDebit1, $incurredCredit1, $closingDebit1, $closingCredit1);
        $aRes['ClosingDebit']  = $closingDebit1;
        $aRes['ClosingCredit'] = $closingCredit1;
        $aRes['mCustomer']     = Users::model()->findByPk($customer_id);
    }
    
    /** @Author: DungNT Aug 01, 2017
     *  @Todo: get daily order of customer
     */
    public function getOrderDaily(&$aRes, $needMore = []) {
        $criteria = new CDbCriteria();
        DateHelper::searchBetween($this->date_from_ymd, $this->date_to_ymd, 'date_delivery_bigint', $criteria, false);
        if(!empty($this->customer_id)){
            $criteria->addCondition('t.customer_id=' . $this->customer_id);
        }
        $criteria->addCondition('t.status=' . GasAppOrder::STATUS_COMPPLETE);
        $criteria->order = 't.id ASC';
        $models = GasAppOrder::model()->findAll($criteria);
        if(isset($needMore['GetModel'])){
            return $models;
        }
        foreach($models as $item){
            $temp['total_gas']      = $item->total_gas;
            $temp['pay_back_amount']= $item->pay_back_amount;
            $temp['discount']       = $item->discount;
            $temp['gas_remain']     = $item->total_gas_du;
            $temp['code_no']        = $item->code_no;
            $aRes['ORDERS'][$item->date_delivery][] = $temp;
        }
    }
    
    /** @Author: DungNT Aug 01, 2017
     *  @Todo: lấy history thu tiên KH trong khoảng ngày
     */
    public function getCustomerPay(&$aRes) {
        $criteria = new CDbCriteria();
        DateHelper::searchBetween($this->date_from_ymd, $this->date_to_ymd, 'date_input_bigint', $criteria, false);
        if(!empty($this->customer_id)){
            $criteria->addCondition('t.customer_id=' . $this->customer_id);
        }
//        $criteria->addCondition('t.lookup_type=' . GasMasterLookup::MASTER_TYPE_THU);
        $criteria->select   = 'sum(t.amount) as amount,  t.code_no, t.note, t.lookup_type, t.date_input, t.master_lookup_id';
        $criteria->group    = 't.date_input, t.lookup_type, t.master_lookup_id';
        $criteria->order    = 't.id ASC';
        $models = EmployeeCashbook::model()->findAll($criteria);
        foreach($models as $item){
            if(in_array($item->master_lookup_id, $this->getTypeNotReport()) && !$this->viewTheChan){
                continue;// Sep1117 bỏ 2 loại thu chi thế chân bò mối ra không tính
            }
            $temp['amount']         = $item->amount;
            $temp['code_no']        = $item->code_no;
            $temp['note']           = $item->note;
            $aRes['PAY'][$item->date_input][$item->lookup_type][$item->master_lookup_id] = $temp;
        }
    }
    
    /** @Author: DungNT Aug 30, 2017
     * @Todo: bảng đối chiếu công nợ KH
     */
    public function reportDebitCashCompare() {
        /* 1. find all AppOder của KH trong khoảng ngày 
         * 2. format theo loại vật tư 
         */
        if(empty($this->customer_id)){
            return [];
        }
        $aRes = [];
        $this->reportDebitInit();
        $aData = $this->getOrderDaily($aRes, ['GetModel'=>1]);
        $this->reportDebitCashCompareFormatData($aRes, $aData);
        $aRes['ARR_DAYS']   = MyFormat::getArrayDay($this->date_from_ymd, $this->date_to_ymd);
        $aRes['mCustomer']  = Users::model()->findByPk($this->customer_id);
        $aRes['mCompany']   = Users::model()->findByPk($aRes['mCustomer']->storehouse_id);
        $_SESSION['data-excel'] = $aRes;
        $_SESSION['data-model'] = $this;
        return $aRes;
    }
    public function reportDebitCashCompareFormatData(&$aRes, $aData) {
        $aVoGas = GasMaterialsType::getArrVoAndGas();
        $typePutPayBack     = [GasMaterialsType::MATERIAL_BINH_45KG, GasMaterialsType::MATERIAL_BINH_50KG];
        foreach($aData as $mAppOrder){
            $temp = $aRemain = []; $emptyGas = true;
            $materials_type_gas = 0;
            $putDiscount = $putPayBackAmout = $putRemain = true;
            $mAppOrder->mapJsonFieldOneDecode('JSON_FIELD', 'json', 'baseArrayJsonDecode');
            $mAppOrder->mapJsonFieldOneDecode('JSON_FIELD_INFO', 'json_info', 'baseArrayJsonDecodeV1');
            
            $this->handleArrayGas($temp, $mAppOrder, $materials_type_gas, $emptyGas);
            $this->handleArrayVo($aRemain, $temp, $mAppOrder, $materials_type_gas, $aVoGas);
            $this->handleGasRemainBB($aRemain, $temp);
            $this->handleEmptyGas($aRemain, $emptyGas, $temp, $mAppOrder);
            
            // xử lý đưa vào mảng chính
            foreach($temp as $materials_type_id => $aInfo){
                if(!isset($aRes['OUTPUT'][$materials_type_id][$mAppOrder->date_delivery]['qty'])){
                    $aRes['OUTPUT'][$materials_type_id][$mAppOrder->date_delivery]['qty']   = $aInfo['qty'];
                }else{
                    $aRes['OUTPUT'][$materials_type_id][$mAppOrder->date_delivery]['qty']   += $aInfo['qty'];
                }
                
                $aRes['OUTPUT'][$materials_type_id][$mAppOrder->date_delivery]['price'] = $aInfo['price'];
                
                $remain = isset($aRemain[$materials_type_id]) ? $aRemain[$materials_type_id] : 0;
                if(!isset($aRes['OUTPUT'][$materials_type_id][$mAppOrder->date_delivery]['remain_kg'])){
                    $aRes['OUTPUT'][$materials_type_id][$mAppOrder->date_delivery]['remain_kg']  = $remain;
                }else{
                    $aRes['OUTPUT'][$materials_type_id][$mAppOrder->date_delivery]['remain_kg']  += $remain;
                }
                
                if($putDiscount){
                    if(!isset($aRes['OUTPUT'][$materials_type_id][$mAppOrder->date_delivery]['discount'])){
                        $aRes['OUTPUT'][$materials_type_id][$mAppOrder->date_delivery]['discount']  = $mAppOrder->discount;
                    }else{
                        $aRes['OUTPUT'][$materials_type_id][$mAppOrder->date_delivery]['discount']  += $mAppOrder->discount;
                    }
                    $putDiscount = false;
                }
                if($putPayBackAmout && in_array($materials_type_id, $typePutPayBack) ){
                    if(!isset($aRes['OUTPUT'][$materials_type_id][$mAppOrder->date_delivery]['pay_back'])){
                        $aRes['OUTPUT'][$materials_type_id][$mAppOrder->date_delivery]['pay_back']  = $mAppOrder->pay_back;
                    }else{
                        $aRes['OUTPUT'][$materials_type_id][$mAppOrder->date_delivery]['pay_back']  += $mAppOrder->pay_back;
                    }
                    $putPayBackAmout = false;
                }
            }
        }// end foreach($aData as $mAppOrder

    }
    
    /** @Author: DungNT Now 08, 2017
     *  @Todo: cộng gas dư của B45 và 50 vào 1 loại
     **/
    public function handleGasRemainBB(&$aRemain, $temp) {
        $gasRemainB50 = isset($aRemain[GasMaterialsType::MATERIAL_BINH_50KG]) ? $aRemain[GasMaterialsType::MATERIAL_BINH_50KG] : 0;
        $gasRemainB45 = isset($aRemain[GasMaterialsType::MATERIAL_BINH_45KG]) ? $aRemain[GasMaterialsType::MATERIAL_BINH_45KG] : 0;
        if(isset($temp[GasMaterialsType::MATERIAL_BINH_50KG]['qty']) && $temp[GasMaterialsType::MATERIAL_BINH_50KG]['qty'] > 0){
            $aRemain[GasMaterialsType::MATERIAL_BINH_50KG] = $gasRemainB50 + $gasRemainB45;
            unset($aRemain[GasMaterialsType::MATERIAL_BINH_45KG]);
        }elseif(isset($temp[GasMaterialsType::MATERIAL_BINH_45KG]['qty']) && $temp[GasMaterialsType::MATERIAL_BINH_45KG]['qty'] > 0){
            $aRemain[GasMaterialsType::MATERIAL_BINH_45KG] = $gasRemainB50 + $gasRemainB45;
            unset($aRemain[GasMaterialsType::MATERIAL_BINH_50KG]);
        }
    }
    // format param gas
    public function handleArrayGas(&$temp, $mAppOrder, &$materials_type_gas, &$emptyGas) {
        foreach($mAppOrder->info_gas as $materials_id => $detail){
            $materials_type_id  = $detail['materials_type_id'];
            $materials_id       = $detail['materials_id'];
            if(!isset($temp[$materials_type_id]['qty'])){
                $temp[$materials_type_id]['qty'] = $detail['qty_real'];
            }else{
                $temp[$materials_type_id]['qty'] += $detail['qty_real'];
            }
            $temp[$materials_type_id]['price']  = $detail['price'];
            if(in_array($materials_type_id, GasMaterialsType::getArrGasBB())){
                $materials_type_gas = $materials_type_id;
            }
            $emptyGas = false;
        }
    }
    // format param vo
    public function handleArrayVo(&$aRemain, &$temp, $mAppOrder, $materials_type_gas, $aVoGas) {
        foreach($mAppOrder->info_vo as $materials_id => $detail){
            $materials_type_id  = $detail['materials_type_id'];
            $remain_kg = ($detail['kg_has_gas'] - $detail['kg_empty']);
            if($materials_type_id == GasMaterialsType::MATERIAL_VO_6){
                $materials_type_gas = GasMaterialsType::MATERIAL_BINH_6KG;
            }
            if($remain_kg > 0 || empty($materials_type_gas)){
                $materials_type_gas = isset($aVoGas[$materials_type_id]) ? $aVoGas[$materials_type_id] : $materials_type_id;
            } 

//                if($remain_kg > 0 && in_array($materials_type_gas, GasMaterialsType::getArrGasKg())){
            if($remain_kg > 0){// Now0217 thêm đk này để không bị sai nếu ngày đó vừa có đơn bán, vừa có đơn thu vỏ
                if(!isset($aRemain[$materials_type_gas])){
                    $aRemain[$materials_type_gas] = $remain_kg;
                }else{
                    $aRemain[$materials_type_gas] += $remain_kg;
                }

                if(!isset($temp[$materials_type_gas]) && !isset($temp[GasMaterialsType::MATERIAL_BINH_50KG]) && !isset($temp[GasMaterialsType::MATERIAL_BINH_45KG])){//Now0417 Nếu đơn hàng có gas B12 và có gas dư ở B45 thì đưa gas dư vào B45
                    /* Ex: A178697XN
                     */
                    $temp[$materials_type_gas]['qty']    = 0;
                    $temp[$materials_type_gas]['price']  = $mAppOrder->price_bb;
                }
            }
        }
    }
    public function handleEmptyGas(&$aRemain, $emptyGas, &$temp, $mAppOrder) {
        if($emptyGas){// xử lý nếu không có gas - đơn hàng chỉ thu vỏ
            foreach ($aRemain as $materials_type_id => $remain_kg):
                $temp[$materials_type_id]['qty']    = 0;
                $temp[$materials_type_id]['price']  = $mAppOrder->price_bb;
            endforeach;
        }
    }
    
    /** @Author: DungNT Oct 25, 2017
     *  @Todo: check is price kg
     */
    public function isPriceKg($materials_type_id, $price) {
        $isPriceKg = false;
        if(in_array($this->customer_id, $this->getCustomerPriceB12Kg())){
            $isPriceKg = true; 
        }
        
        if(in_array($materials_type_id, GasMaterialsType::getArrGasKg())
            || ($materials_type_id == GasMaterialsType::MATERIAL_BINH_12KG && $price < UsersPrice::PRICE_MAX_BINH_BO)
            || $materials_type_id == GasMaterialsType::MATERIAL_BINH_6KG
            || ($materials_type_id == GasMaterialsType::MATERIAL_BINH_12KG && $this->customer_id == 63262)
                // Jan2618 63262,// Sheraton Sài Gòn -> Mã khách hàng này bình 12kg anh Dũng trong bảng kê quy ra thành kg luôn giúp em, vì bình nào cũng cân gas dư hết đó anh.
        ){
            // Oct2517 Bình 6kg chỉ có Sheraton lấy nên đang tính riêng
            $isPriceKg = true; 
        }
        return $isPriceKg;
    }
    
    /** @Author: DungNT Jan 26, 2018
     *  @Todo: Xác định KH tính giá kg để trừ gas dư bình 12
     */
    public function isPriceKgB12($materials_type_id, &$b12ToKg) {
        if($materials_type_id != GasMaterialsType::MATERIAL_BINH_12KG){
            return ;
        }
        $aCustomerB12  = [
            63262,// Sheraton Sài Gòn
//            852292,// Q900007-(Xe HCM 1) Hàng Dương Q.1// Now2218 ko xử lý đc B12 có gas dư, lấy cả B45
        ];
        if(in_array($this->customer_id, $aCustomerB12)){
            $b12ToKg = true; 
        }
    }
    
    /** @Author: DungNT Jun 15, 2018
     *  @Todo: get url update customer type for report debit
     **/
    public function getUrlUpdateCustomerType() {
        $aUidAllow  = [GasConst::UID_ADMIN, GasLeave::THUC_NH];
        $cUid = MyFormat::getCurrentUid();
        if(in_array($cUid, $aUidAllow)){
            $url = Yii::app()->createAbsoluteUrl("admin/inventoryCustomer/reportDebitCash", array('customer_id'=>$this->customer_id, 'UpdateReportDebit'=>1));
            return "<br><br><a target='_blank' class='OnlyRunLinkAlertText'  alert_text='Chắc chắn muốn cập nhật lại loại KH cho BC công nợ tổng quát?' href='$url'>Cập nhật loại KH</a>";
        }
        return '';
    }
    
    /** @Author: DungNT Jul 12, 2018
     *  @Todo: get url update customer Chan Hang
     **/
    public function getUrlLockCustomer($mCustomer) {
        $aUidAllow          = [GasConst::UID_ADMIN, GasLeave::THUC_NH];
        $cUid               = MyFormat::getCurrentUid();
        if(in_array($cUid, $aUidAllow)){
            $urlTitle       = 'Chặn hàng KH';
            $alertText      = 'Chắc chắn muốn chặn hàng KH này?';
            $statusLock     = InventoryCustomer::UPDATE_LOCK_CUSTOMER;
            if($mCustomer->channel_id == Users::CHAN_HANG){ // Nếu là đang bị chặn
                $urlTitle   = 'Bỏ chặn KH';
                $alertText  = 'Chắc chắn muốn bỏ chặn hàng KH này?';
                $statusLock = InventoryCustomer::UPDATE_UNLOCK_CUSTOMER;
            }
            $url = Yii::app()->createAbsoluteUrl("admin/inventoryCustomer/reportDebitCash", array('customer_id'=>$this->customer_id, 'UpdateLockCustomer'=>$statusLock));
            return "<br><br><a target='_blank' class='OnlyRunLinkAlertText'  alert_text='$alertText' href='$url'>$urlTitle</a>";
        }
        return '';
    }
    
    /*************** FEB0718 ZONE NAM CODE **********************/
    /** @Author: HOANG NAM 31/01/2018
     *  @Todo: get Inventory
     *  @Code: NAM006
     **/
   public function inventorySearch(){
        $aData              = $this->reportDebitVo();
        $aCustomer          = isset($aData['CUSTOMER_MODEL']) ? $aData['CUSTOMER_MODEL'] : [];
        $BEGIN_VO           = isset($aData['BEGIN_VO']) ? $aData['BEGIN_VO'] : [];
        $OUTPUT_BEFORE      = isset($aData['OUTPUT_BEFORE']) ? $aData['OUTPUT_BEFORE'] : [];
        $OUTPUT_IN_PERIOD   = isset($aData['OUTPUT_IN_PERIOD']) ? $aData['OUTPUT_IN_PERIOD'] : [];
        $aIdVo              = isset($aData['ID_VO']) ? $aData['ID_VO'] : [];
        foreach ($aCustomer as $keyCustomer => $mCustomer):
            $sum_end = 0;
            $customer_id = $mCustomer->id;
            foreach ($aIdVo[$customer_id] as $materials_id => $qty):
                $qty_begin              = isset($BEGIN_VO[$customer_id][$materials_id]) ? $BEGIN_VO[$customer_id][$materials_id] : 0;
                $qty_import_before      = isset($OUTPUT_BEFORE[TYPE_STORE_CARD_IMPORT][$customer_id][$materials_id]) ? $OUTPUT_BEFORE[TYPE_STORE_CARD_IMPORT][$customer_id][$materials_id] : 0;
                $qty_export_before      = isset($OUTPUT_BEFORE[TYPE_STORE_CARD_EXPORT][$customer_id][$materials_id]) ? $OUTPUT_BEFORE[TYPE_STORE_CARD_EXPORT][$customer_id][$materials_id] : 0;
                
                $qty_import_in_period   = isset($OUTPUT_IN_PERIOD[TYPE_STORE_CARD_IMPORT][$customer_id][$materials_id]) ? $OUTPUT_IN_PERIOD[TYPE_STORE_CARD_IMPORT][$customer_id][$materials_id] : 0;
                $qty_export_in_period   = isset($OUTPUT_IN_PERIOD[TYPE_STORE_CARD_EXPORT][$customer_id][$materials_id]) ? $OUTPUT_IN_PERIOD[TYPE_STORE_CARD_EXPORT][$customer_id][$materials_id] : 0;
                $begin                  = $qty_begin + $qty_export_before - $qty_import_before;
                $end                    = $begin + $qty_export_in_period - $qty_import_in_period;
                $sum_end                += $end;
            endforeach;
            if($sum_end < $this->countVo):
                unset($aData['CUSTOMER_MODEL'][$keyCustomer]);
                continue;
            endif;
            $aData['CUSTOMER_MODEL'][$keyCustomer]->sum_end_vo = $sum_end;
        endforeach;
//        sort
        if(isset($aData['CUSTOMER_MODEL'])):
            usort($aData['CUSTOMER_MODEL'], function ($item1, $item2) {
                return $item2->sum_end_vo >= $item1->sum_end_vo;
            });
        endif;
        return $aData;
   }
   /** @Author: HOANG NAM 01/02/2018
    *  @Todo: get array select số lượng vỏ
    *  @Code: NAM006
    *  @Param: 
   **/
   public function getListCountSelect(){
        $start  = 3;
        $end    = 50;
        $key    = range($start,$end,1);
        $valueView  = range($start,$end,1);
        array_walk($valueView, function(&$value, $key) { $value .= ' Vỏ'; } );
        return array_combine($key,$valueView);
   }
   /** @Author: HOANG NAM 01/02/2018
    *  @Todo: getArrayCountVo
    *  @Code: NAM006
    *  @Param: array(id => count )
    **/
   public function getListCustomerCountVo($sCustomer){
        $aResult =[];
        $this->sCustomer = $sCustomer;
        $aData              = $this->reportDebitVo();
        $aCustomer          = isset($aData['CUSTOMER_MODEL']) ? $aData['CUSTOMER_MODEL'] : [];
        $BEGIN_VO           = isset($aData['BEGIN_VO']) ? $aData['BEGIN_VO'] : [];
        $OUTPUT_BEFORE      = isset($aData['OUTPUT_BEFORE']) ? $aData['OUTPUT_BEFORE'] : [];
        $OUTPUT_IN_PERIOD   = isset($aData['OUTPUT_IN_PERIOD']) ? $aData['OUTPUT_IN_PERIOD'] : [];
        $aIdVo              = isset($aData['ID_VO']) ? $aData['ID_VO'] : [];
        foreach ($aCustomer as $mCustomer):
            $sum_end = 0;
            $customer_id = $mCustomer->id;
            foreach ($aIdVo[$customer_id] as $materials_id => $qty):
                $qty_begin              = isset($BEGIN_VO[$customer_id][$materials_id]) ? $BEGIN_VO[$customer_id][$materials_id] : 0;
                $qty_import_before      = isset($OUTPUT_BEFORE[TYPE_STORE_CARD_IMPORT][$customer_id][$materials_id]) ? $OUTPUT_BEFORE[TYPE_STORE_CARD_IMPORT][$customer_id][$materials_id] : 0;
                $qty_export_before      = isset($OUTPUT_BEFORE[TYPE_STORE_CARD_EXPORT][$customer_id][$materials_id]) ? $OUTPUT_BEFORE[TYPE_STORE_CARD_EXPORT][$customer_id][$materials_id] : 0;

                $qty_import_in_period   = isset($OUTPUT_IN_PERIOD[TYPE_STORE_CARD_IMPORT][$customer_id][$materials_id]) ? $OUTPUT_IN_PERIOD[TYPE_STORE_CARD_IMPORT][$customer_id][$materials_id] : 0;
                $qty_export_in_period   = isset($OUTPUT_IN_PERIOD[TYPE_STORE_CARD_EXPORT][$customer_id][$materials_id]) ? $OUTPUT_IN_PERIOD[TYPE_STORE_CARD_EXPORT][$customer_id][$materials_id] : 0;
                $begin                  = $qty_begin + $qty_export_before - $qty_import_before;
                $end                    = $begin + $qty_export_in_period - $qty_import_in_period;
                $sum_end                += $end;
            endforeach;
            $aResult[$mCustomer->id] = $sum_end;
        endforeach;
        return $aResult;
   }
   
   /*************** FEB0718 ZONE NAM CODE **********************/
   /** @Author: HOANG NAM 09/07/2018
   *  @Todo: group theo danh sách vỏ nhỏ và vỏ lớn
   **/
   public function groupTypeVo(&$aRes,$aMaterial){
       foreach ($aRes['ID_VO'] as $id_customer => $value) {
           $aRes['ID_VO'][$id_customer]['MATERIALS_VO_NHO']= array();
           $aRes['ID_VO'][$id_customer]['MATERIALS_VO_LON']= array();
           foreach ($value as $materials_id => $qty) {
               $type = isset($aMaterial[$materials_id]) ? $aMaterial[$materials_id]->materials_type_id : 0;
               if(in_array($type, GasMaterialsType::$ARR_GAS_VO_NHO)){
                    $aRes['ID_VO'][$id_customer]['MATERIALS_VO_NHO'][$materials_id]  = $qty;
               }else{
                    $aRes['ID_VO'][$id_customer]['MATERIALS_VO_LON'][$materials_id]  = $qty;
               }
           }
       }
    }
   
    /** @Author: DungNT Aug 23, 2018
     *  @Todo: check User can send email đối chiếu công nợ với KH
     **/
    public function canSendEmailCompareDebit() {
        $cUid   = MyFormat::getCurrentUid();
        $aAllow = [GasLeave::THUC_NH, GasConst::UID_ADMIN];
        return in_array($cUid, $aAllow);
    }
    
    /** @Author: DungNT Aug 23, 2018
     *  @Todo: get list email setup of customer
     **/
    public function getListEmailSendCompareDebit() {
        if(empty($this->customer_id)){
            return [];
        }
        $mUsersEmail = new UsersEmail();
        $mUsersEmail->user_id   = $this->customer_id;
        $mUsersEmail->type      = UsersEmail::TYPE_ANNOUNCE_DEBIT;
        return $mUsersEmail->getByTypeCompareDebit();
    }
    
    /** @Author: NamNH 09/2018
     *  @Todo: get data of report credit
     **/
    public function getReportDebit(){
        $this->reportDebitInit();
        $aData = [];
        $this->reportVoGetBegin($aData, InventoryCustomer::TYPE_INVENTORY);
        $date_from  = $this->begin_date;
        $date_to    = $this->date_to_ymd;
        // 1. tính phát sinh DEBIT-NỢ để tính ra DEBIT-NỢ (đã thu) đầu kỳ
        $nameVar    = 'DATA_OPENING_DEBIT';
        $this->reportCashGetDebit($aData, $nameVar, $date_from, $date_to);
        
        // 3. tính phát sinh CREDIT-CÓ để tính ra CREDIT-CÓ (phải thu) đầu kỳ
        $nameVar    = 'DATA_OPENING_CREDIT';
        $this->reportCashGetCredit($aData, $nameVar, $date_from, $date_to);
        
        if(isset($aData['CUSTOMER_ID'])){
            $aData['CUSTOMER_MODEL'] = Users::getArrObjectUserByRole(ROLE_CUSTOMER, $aData['CUSTOMER_ID']);
        }
        $this->handleDebit($aData,$date_to);

        return $aData;
    }
    
    /** @Author: NamNH 09/2018
     *  @Todo: get detail of report credit
     **/
    public function getDetailCredit($dateSearch,$mCustomer,&$closingDebit,$aZone){
        $aResult        = [];
        $sumDebit       = $closingDebit;
//        bỏ qua các hoá đơn không nằm trong array zone và sau hạn thanh toán
        $dateSearchOrder = '';
        $pay_day        = $this->getDaysPay($mCustomer,$dateSearchOrder);
        $min            = 999;
        foreach ($aZone as $keyZone => $vZone) {
            $minArr     = min($vZone);
            if($min > $minArr && !empty($vZone['end'])){
                $min    = $minArr;
            }
        }
        $min            += $pay_day;
        $from_before    = MyFormat::addDays($dateSearch, $min, '-');
        if(!empty($dateSearchOrder) && $dateSearchOrder < $from_before){
            $from_before = $dateSearchOrder;
        }
        $sumBefore      = $this->getTotalGas($from_before, $dateSearch,$mCustomer);
//        $closingDebit   -= $sumBefore;
        $sumDebit       -= $sumBefore;
//        lấy danh sách tiền nằm trong khoản ngày định nghĩa trước ở hàm $this->getArrayZone();
        foreach ($aZone as $key => $zone) {
            if($sumDebit <= 0){
                break;
            }
            $temp = $sumDebit;
//            set date from and to of zone
            if(!empty($zone['end'])){
                $date_from  = MyFormat::addDays($dateSearch, ($zone['end'] + $pay_day), '-');
            }else{
                $date_from  = MyFormat::addDays($this->begin_date, $pay_day, '-');
               
            }
            if(!empty($zone['start'])){
                $date_to    = MyFormat::addDays($dateSearch, ($zone['start'] + $pay_day), '-');
            }else{
                $date_to    = MyFormat::addDays($dateSearch, $pay_day, '-');
            }
            if(!empty($dateSearchOrder) && $dateSearchOrder < $date_to){
                $date_to = $dateSearchOrder;
            }
            $nDebit             = $this->getTotalGas($date_from, $date_to,$mCustomer);
            if($nDebit <= 0){
                continue;
            }
            $sumDebit           -= $nDebit;
            $aResult[$key]      = $sumDebit <= 0 ? $temp : $nDebit;
        }
        return $aResult;
    }
    
    /** @Author: NamNH 09/2018
     *  @Todo: get total gas
     **/
    public function getTotalGas($date_from,$date_to,$mCustomer){
        $result     = 0;
        $criteria = new CDBcriteria();
        $criteria->compare('t.customer_id', $mCustomer->id);
        $this->reportCashSameCriteria($criteria, $date_from, $date_to, 'date_delivery', 'agent_id');
        $criteria->select   = 'sum(grand_total) as grand_total,sum(total_gas) as total_gas, sum(total_gas_du) as total_gas_du, sum(pay_back_amount) as pay_back_amount, sum(discount) as discount, t.agent_id, t.customer_id, t.sale_id';
        $criteria->addCondition('t.status=' . GasAppOrder::STATUS_COMPPLETE);
        $criteria->addCondition('t.status_debit=' . GasAppOrder::DEBIT_YES);
        $criteria->group    = 't.customer_id';
        $mGasAppOrder = GasAppOrder::model()->find($criteria);
        if(!empty($mGasAppOrder)){
            $result             = $mGasAppOrder->grand_total;
        }
        return $result;
    }
    
    /** @Author: NamNH 09/2018
     *  @Todo: set total
     **/
    public function handleDebit(&$aData,$dateSearch){
        $aZone                  = $this->getArrayZone();
        $aData['ZONE']          = $aZone;
        $mAppCache              = new AppCache();
        $BEGIN_CASH             = isset($aData['BEGIN_CASH']) ? $aData['BEGIN_CASH'] : [];
        $DATA_OPENING_DEBIT     = isset($aData['DATA_OPENING_DEBIT']) ? $aData['DATA_OPENING_DEBIT'] : [];
        $DATA_OPENING_CREDIT    = isset($aData['DATA_OPENING_CREDIT']) ? $aData['DATA_OPENING_CREDIT'] : [];
        $aCustomer              = isset($aData['CUSTOMER_MODEL']) ? $aData['CUSTOMER_MODEL'] : [];
        $listdataAgent          = $mAppCache->getAgentListdata(); $i = 1;
        $listdataSale           = $mAppCache->getListdataUserByRole(ROLE_SALE);
        $listdataSale1          = $mAppCache->getListdataUserByRole(ROLE_MONITORING_MARKET_DEVELOPMENT);
        $listdataSale2          = $mAppCache->getListdataUserByRole(ROLE_EMPLOYEE_MAINTAIN);
        $listdataSale           = $listdataSale + $listdataSale1 + $listdataSale2;
        foreach ($aCustomer as $mCustomer){
            $customer_id = $mCustomer->id;
            $closingDebit1 = $closingCredit1 = 0;

            $openingDebit1          = isset($BEGIN_CASH[$customer_id]['debit']) ? $BEGIN_CASH[$customer_id]['debit'] : 0;
            $openingCredit1         = isset($BEGIN_CASH[$customer_id]['credit']) ? $BEGIN_CASH[$customer_id]['credit'] : 0;

            // vì tiền của KH có thu tiền mặt và chi gas dư, nên ở đây phải trừ đi gas dư
            $incurredDebit1     = isset($DATA_OPENING_DEBIT[$customer_id]) ? $DATA_OPENING_DEBIT[$customer_id] : 0;// phải thu KH
//            $incurredCredit1    = $model->calcDebitCustomer($DATA_OPENING_CREDIT, $customer_id);// tính thu trong kỳ không được trừ gas dư
            $incurredCredit1    = isset($DATA_OPENING_CREDIT[$customer_id][GasMasterLookup::MASTER_TYPE_THU]) ? $DATA_OPENING_CREDIT[$customer_id][GasMasterLookup::MASTER_TYPE_THU] : 0;// Jul0317 đã thu - không trừ gas dư
            // 1. nợ và có đầu kỳ
            $this->calcClosingDebitCredit($openingDebit1, $openingCredit1, $incurredDebit1, $incurredCredit1, $closingDebit1, $closingCredit1);
            // Jul1218 nếu nhỏ hơn 1k thì round = 0 - Fix Thúc 
            $closingDebit1          = $closingDebit1 > InventoryCustomer::AMOUNT_ROUND_DOWN ? $closingDebit1 : 0;
            $closingCredit1         = $closingCredit1 > InventoryCustomer::AMOUNT_ROUND_DOWN ? $closingCredit1 : 0;
            // Jul1218 nếu nhỏ hơn 1k thì round = 0 - Fix Thúc
            if($closingDebit1 > 0){
                $aValueZone         = $this->getDetailCredit($dateSearch,$mCustomer,$closingDebit1,$aZone);
                $aData['DATA_ZONE'][$customer_id]           = $aValueZone;
            }
            if(!isset($aData['DATA_DEBIT'][$customer_id])){
                $aData['DATA_DEBIT'][$customer_id]           = $closingDebit1;
            }else{
                $aData['DATA_DEBIT'][$customer_id]           += $closingDebit1;
            }
        }
        
    }
    
    /** @Author: NamNH Aug 13, 2019
     *  @Todo: get last order
     *  @Param:$mCustomer
     **/
    public function getLastPurchase($mCustomer,$dateMax = '') {
        $criteria = new CDBcriteria();
        $criteria->compare('t.customer_id', $mCustomer->id);
        $criteria->addCondition('t.status=' . GasAppOrder::STATUS_COMPPLETE);
        $criteria->addCondition('t.status_debit=' . GasAppOrder::DEBIT_YES);
        if(!empty($dateMax)){
            DateHelper::searchSmaller($dateMax, 'date_delivery_bigint', $criteria);
        }
        $criteria->order = 't.id DESC';
        $criteria->limit = 1;
        $mGasAppOrder = GasAppOrder::model()->find($criteria);
        return $mGasAppOrder;
    }
    
    /** @Author: NamNH 09/2018
     *  @Todo: get days pay
     **/
    public function getDaysPay($mCustomer,&$dateSearch){
        $result         = 0;
        $mPayMent       = $mCustomer->payment;
        if(!empty($mPayMent)){
            switch ($mPayMent->type){
                case PAY_TYPE_1:
                    if($mPayMent->pay_day == 15){//mua hàng từ ngày 1 - 15 thanh toán vào ngày 25 trong tháng và mua hàng từ 16-cuối tháng thanh toán vào ngày 10 tháng sau
                        $dateNow    = $this->date_to_ymd;
                        $dateSearch = $this->date_to_ymd;
                        $dayNow     = (int)MyFormat::dateConverYmdToDmy($dateNow, 'd');
                        $monthNow  = (int)MyFormat::dateConverYmdToDmy($dateNow, 'm');
                        $yearNow   = (int)MyFormat::dateConverYmdToDmy($dateNow, 'Y');
//                        Thực hiện tính toán ngày tối đa. do có đơn hàng ngày < 10 và ngày hiện tại < 10 thì sẽ tính nhằm đơn hàng ngày 15->30 là đơn hàng đến hạng
                        if($dayNow < 10){
                            $monthNow -= 1;
                            if($monthNow < 1){
                                $monthNow = 12;
                                $yearNow -= 1;
                            }
                            if($monthNow < 10){
                                $monthNow  = '0'.$monthNow;
                            }
                            $dateSearch = $yearNow.'-'.$monthNow.'-15';
                        }elseif($dayNow < 25){ // bình tháng trước
                            $dateSearch = $yearNow.'-'.$monthNow.'-01';
                            $dateSearch = MyFormat::modifyDays($dateSearch, 1, '-');
                        }
                        $mGasAppOrder   = $this->getLastPurchase($mCustomer,$dateSearch);
                        if(!empty($mGasAppOrder->date_delivery)){
                            $dayPay = $monthPay = $yearPay = '';
                            $day    = (int)MyFormat::dateConverYmdToDmy($mGasAppOrder->date_delivery, 'd');
                            $month  = (int)MyFormat::dateConverYmdToDmy($mGasAppOrder->date_delivery, 'm');
                            $year   = (int)MyFormat::dateConverYmdToDmy($mGasAppOrder->date_delivery, 'Y');
                            if($day >= 1 && $day <= 15){
                                $yearPay    = $year;
                                $monthPay   = $month;
                                if($monthPay < 10){
                                    $monthPay = '0'.$monthPay;
                                }
                                $dayPay     = 25;
                            }else{
                                $dayPay = 10;
                                $yearPay = $year;
                                $monthPay = $month + 1;
                                if($monthPay < 10){
                                    $monthPay = '0'.$monthPay;
                                }
                                if($monthPay > 12){
                                    $monthPay = '01';
                                    $yearPay = $year + 1;
                                }
                            }
                            $datePay = $yearPay.'-'.$monthPay.'-'.$dayPay;
                            $result = MyFormat::getNumberOfDayBetweenTwoDate($datePay, $this->date_to_ymd);
                            break;
                        }
                    }
                    $result = $mPayMent->pay_day;
                    break;
                case PAY_TYPE_2:
                    $result = 7;
                    break;
                case PAY_TYPE_3:
                    if($mPayMent->pay_day == '1-30-5-1'){//L3: ngày 5 hàng tháng : mua hàng từ ngày 1 - 30 thanh toán vào ngày 20 tháng sau
                        $dateNow    = $this->date_to_ymd;
                        $dateSearch = $this->date_to_ymd;
                        $dayNow     = (int)MyFormat::dateConverYmdToDmy($dateNow, 'd');
                        $monthNow  = (int)MyFormat::dateConverYmdToDmy($dateNow, 'm');
                        $yearNow   = (int)MyFormat::dateConverYmdToDmy($dateNow, 'Y');
    //                        Thực hiện tính toán ngày tối đa.
                        if($dayNow < 20){
                            $monthNow -= 1;
                            if($monthNow < 1){
                                $monthNow = 12;
                                $yearNow -= 1;
                            }
                            if($monthNow < 10){
                                $monthNow  = '0'.$monthNow;
                            }
                            $dateSearch = $yearNow.'-'.$monthNow.'-01';
                        }elseif($dayNow < 25){ // bình tháng trước
                            $dateSearch = $yearNow.'-'.$monthNow.'-01';
                        }
                        $dateSearch = MyFormat::modifyDays($dateSearch, 1, '-');
                        $mGasAppOrder   = $this->getLastPurchase($mCustomer,$dateSearch);
                        if(!empty($mGasAppOrder->date_delivery)){
                            $day    = (int)MyFormat::dateConverYmdToDmy($mGasAppOrder->date_delivery, 'd');
                            $month  = (int)MyFormat::dateConverYmdToDmy($mGasAppOrder->date_delivery, 'm');
                            $year   = (int)MyFormat::dateConverYmdToDmy($mGasAppOrder->date_delivery, 'Y');
                            $dayPay = 20;
                            $yearPay = $year;
                            $monthPay = $month + 1;
                            if($monthPay < 10){
                                $monthPay = '0'.$monthPay;
                            }elseif($monthPay > 12){
                                $monthPay = '01';
                                $yearPay = $year + 1;
                            }
                            $datePay = $yearPay.'-'.$monthPay.'-'.$dayPay;
                            $result = MyFormat::getNumberOfDayBetweenTwoDate($datePay, $this->date_to_ymd);
                            break;
                        }
                    }
                    $result = 0;
                    break;
                case PAY_TYPE_4:
//                    ngày mua gần nhất
                    $mGasAppOrder = $this->getLastPurchase($mCustomer);
                    if(!empty($mGasAppOrder)){
                        $dateNow = $this->date_to_ymd;
                        $countDate = MyFormat::getNumberOfDayBetweenTwoDate($dateNow, $mGasAppOrder->date_delivery);
                        $result = $countDate;
                    }
                    break;
            }
        }
        return $result;
    }
    /** @Author: NamNH 09/2018
     *  @Todo: get array zone of report detail credit
     **/
    public function getArrayZone(){
        return [
            ['start'=>1,'end'=>6,'title'=>'Đến hạn'],
            ['start'=>7,'end'=>14,'title'=>'7-14'],
            ['start'=>15,'end'=>21,'title'=>'15-21'],
            ['start'=>22,'end'=>null,'title'=>'22-'],
        ];
    }
    
    /** @Author: NamNH Nov 20, 2018
     *  @Todo: report warning vo
     **/
    public function getWarningVo(){
        $_SESSION['ModelExcelWarning'] = $this;
        $aData = [];
        $this->getReportInventoryVo($aData);
        $this->getCycleInventoryVo($aData);
//        sort
        if(is_array($aData['CUSTOMER_MODEL'])){
//            sort ton kho
//            uasort($aData['CUSTOMER_MODEL'], function ($item1, $item2) use ($aData) {
//                $preVo1              = isset($aData['OUT_PUT_INVENTORY_VO'][$item1->id]) ? $aData['OUT_PUT_INVENTORY_VO'][$item1->id] : 0;
//                $preVo2              = isset($aData['OUT_PUT_INVENTORY_VO'][$item2->id]) ? $aData['OUT_PUT_INVENTORY_VO'][$item2->id] : 0;
//                return $preVo2 > $preVo1;
//            });
//            sort SL
            uasort($aData['CUSTOMER_MODEL'], function ($item1, $item2) use ($aData) {
                $preVo1              = isset($aData['OUT_PUT_STORE_RESULT'][$item1->id]) ? $aData['OUT_PUT_STORE_RESULT'][$item1->id] : 0;
                $preVo2              = isset($aData['OUT_PUT_STORE_RESULT'][$item2->id]) ? $aData['OUT_PUT_STORE_RESULT'][$item2->id] : 0;
                return $preVo2 > $preVo1;
            });
        }
        return $aData;
    }
    
    /** @Author: NamNH Nov 20, 2018
     *  @Todo: get report inventory vo
     **/
    public function getReportInventoryVo(&$aData){
        $this->reportDebitInit();
        $this->reportVoGetBegin($aData, InventoryCustomer::TYPE_INVENTORY);
        $mAppCache = new AppCache();
        $aGasVo = $mAppCache->getListdataGasVo();
        // 1. tính nhập xuất từ ngày setup đến date_from
        $date_from  = $this->begin_date;
        $date_to    = MyFormat::modifyDays($this->date_from_ymd, 1, '-');
        $nameVar    = 'OUTPUT_BEFORE';
        $this->reportVoGetOutput($aData, $nameVar, $aGasVo, $date_from, $date_to);
//        Lấy danh sách model customer
        if(isset($aData['CUSTOMER_ID'])){
            $aData['CUSTOMER_MODEL'] = Users::getArrObjectUserByRole(ROLE_CUSTOMER, $aData['CUSTOMER_ID']);
        }
        $BEGIN_VO           = isset($aData['BEGIN_VO']) ? $aData['BEGIN_VO'] : [];
        $OUTPUT_BEFORE      = isset($aData['OUTPUT_BEFORE']) ? $aData['OUTPUT_BEFORE'] : [];
        $aCustomer          = isset($aData['CUSTOMER_MODEL']) ? $aData['CUSTOMER_MODEL'] : [];
//        lấy số lượng vỏ tồn kho
        $aMaterial  = $mAppCache->getMasterModel('GasMaterials', AppCache::ARR_MODEL_MATERIAL);
        foreach ($aCustomer as $mCustomer) {
            $customer_id = $mCustomer->id;
            
            $qty_begin          = isset($BEGIN_VO[$customer_id]) ? $this->sumByTypeMaterial($BEGIN_VO[$customer_id],$aMaterial,[GasMaterialsType::MATERIAL_VO_45,GasMaterialsType::MATERIAL_VO_50]) : 0;
            $qty_import_before  = isset($OUTPUT_BEFORE[TYPE_STORE_CARD_IMPORT][$customer_id]) ? $this->sumByTypeMaterial($OUTPUT_BEFORE[TYPE_STORE_CARD_IMPORT][$customer_id],$aMaterial,[GasMaterialsType::MATERIAL_VO_45,GasMaterialsType::MATERIAL_VO_50]) : 0;
            $qty_export_before  = isset($OUTPUT_BEFORE[TYPE_STORE_CARD_EXPORT][$customer_id]) ? $this->sumByTypeMaterial($OUTPUT_BEFORE[TYPE_STORE_CARD_EXPORT][$customer_id],$aMaterial,[GasMaterialsType::MATERIAL_VO_45,GasMaterialsType::MATERIAL_VO_50]) : 0;
            $begin      = $qty_begin + $qty_export_before - $qty_import_before;
            $aData['OUT_PUT_INVENTORY_VO'][$customer_id] = $begin;
        }
    }
    
    /** @Author: NamNH Nov 20, 2018
     **/
    public function getCycleInventoryVo(&$aData){
        $model = new GasStoreCard();
        $ymd                        = MyFormat::dateConverDmyToYmd($this->date_to, '-');
        $preMonth                   = MyFormat::addDays($ymd, 1, $operator = '-', $amount_of_days = 'month', 'd-m-Y');
        $model->date_from           = MyFormat::dateConverYmdToDmy($preMonth, '01-m-Y');
        $model->date_to             = MyFormat::dateConverYmdToDmy($preMonth, 't-m-Y');
        $model->ext_is_maintain     = $this->type_customer;
        $model->agent_id            = $this->agent_id;
        $model->ext_sale_id         = $this->sale_id;
        $model->customer_id         = $this->customer_id;
        $model->province_id   = $this->province_id;
//        get data
        $aData['OUT_PUT_STORE_CARD'] = Sta2::Output_customer($model);
        $data = isset($aData['OUT_PUT_STORE_CARD']) ? $aData['OUT_PUT_STORE_CARD'] : [];
        $arr_customer_id    = isset($data['customer_id'])?$data['customer_id']:array();
        $ARR_DAYS           = $data['ARR_DAYS'];
        $OUTPUT             = isset($data['OUTPUT'])?$data['OUTPUT']:array();
        $OUTPUT_MOI_12      = isset($data['OUTPUT_MOI_12'])?$data['OUTPUT_MOI_12']:array();
        $GAS_REMAIN         = isset($data['GAS_REMAIN'])?$data['GAS_REMAIN']:array();
        foreach($arr_customer_id as $customer_id=>$arr_agent){
            $SumRowBo       =0;
            $SumRowMoi      =0;
            foreach($arr_agent as $agent_id){
                foreach($ARR_DAYS as $date_delivery){
                    $BO_OUTPUT  = isset($OUTPUT[$customer_id][$date_delivery][$agent_id])?$OUTPUT[$customer_id][$date_delivery][$agent_id]:0;
                    $BO_REMAIN  = isset($GAS_REMAIN[$customer_id][$date_delivery][$agent_id])?$GAS_REMAIN[$customer_id][$date_delivery][$agent_id]:0;
                    $BO_REAL    = $BO_OUTPUT - $BO_REMAIN;
                    $SumRowBo+=$BO_REAL;
                    $MOI_12     = 0;
                    if($model->ext_is_maintain==STORE_CARD_KH_MOI){
                        $MOI_12 = isset($OUTPUT_MOI_12[$customer_id][$date_delivery][$agent_id])?$OUTPUT_MOI_12[$customer_id][$date_delivery][$agent_id]:0;
                        if($MOI_12 > 0){
//                            $SumRowMoi +=$MOI_12;// ko tinh B12
                        }
                    }
                }
            }
            $sumRow = $SumRowMoi+ $SumRowBo;
            $aData['OUT_PUT_STORE_RESULT'][$customer_id] = round(($sumRow * 0.35) / 45, 1);
        }
    }
    
    /** @Author: NamNH Dec 18, 2018
     *  @Todo: sum by array type
     *  @Param: 
     * $aData: array(id_material => count, ) mảng id material và số lượng
     * $aModel: array(id_material => array attribute, ) danh sách model material
     * $aType: array(type_1, type_2) danh sách type accept
     **/
    public function sumByTypeMaterial($aData, $aModel, $aType){
        $result = 0;
        foreach ($aData as $key => $value) {
            if(isset($aModel[$key]) && in_array($aModel[$key]['materials_type_id'], $aType)){
                $result += $value;
            }
        }
        return $result;
    }
    
    /** @Author: DungNT Dec 20, 2018
     *  @Todo: init some default data search
     **/
    public function initDataSearchWarningVo() {
        $cRole = MyFormat::getCurrentRoleId();
        $cUid = MyFormat::getCurrentUid();
        $mUser = new Users();
        if(in_array($cRole, $mUser->ARR_ROLE_SALE_LIMIT)){
            $this->sale_id = $cUid;
        }
    }
    /**
     * @Author: PHAM THANH NGHIA Jan 7,2019
     * @Todo: admin/views/inventoryCustomer/limitDebit/index.php  
     */
    public function searchLimitDebit(){
        $criteria=new CDbCriteria;
        $criteria->compare('t.c_year',$this->c_year);
        $criteria->compare('t.customer_id',$this->customer_id);
        $criteria->compare('t.type_customer',$this->type_customer);
        $criteria->compare('t.uid_login', $this->uid_login);
        $criteria->compare('t.type', InventoryCustomer::TYPE_LIMIT_DEBIT);
        $criteria->order = 't.id DESC';
        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
            'pagination'=>array(
                'pageSize'=> Yii::app()->user->getState('pageSize',Yii::app()->params['defaultPageSize']),
            ),
        ));
    }
    
    public function runSqlInsertLimitDebit() {
        if(!isset($_POST['customer_id'])){
            return ;
        }
        
        $created_date = date('Y-m-d H:i:s');
        $aRowInsert = $aCustomerCheck = []; $sErrors = '';
        $cUid = MyFormat::getCurrentUid();
        $c_year = 0;
        $this->begin_year = 0;
        $aCustomerSetup = $this->getListDataCustomer();
        
        foreach($_POST['customer_id'] as $key => $customer_id){
            
            $mCustomer = Users::model()->findByPk($customer_id);
            if(in_array($customer_id, $aCustomerCheck) || in_array($customer_id, $aCustomerSetup)){
                $sErrors .= "<br>".$mCustomer->getFullName();
                continue ;
            }
            $debit  = isset($_POST['debit'][$key]) ? MyFormat::removeComma($_POST['debit'][$key]) : 0;
            $credit = isset($_POST['credit'][$key]) ? MyFormat::removeComma($_POST['credit'][$key]) : 0;
            
            $parent_id   = !empty($mCustomer->parent_id) ? $mCustomer->parent_id : 0;
            $is_maintain = !empty($mCustomer->is_maintain) ? $mCustomer->is_maintain : 0;
            $agent_id    = !empty($mCustomer->area_code_id) ? $mCustomer->area_code_id : 0;
            $sale_id     = !empty($mCustomer->sale_id) ? $mCustomer->sale_id : 0;
            if(empty($debit) && empty($credit)){
                continue;
            }
            
            $aRowInsert[]="(
                    '$this->type',
                    '$cUid',
                    '$c_year',
                    '$customer_id',
                    '$parent_id',
                    '$is_maintain',
                    '$agent_id',
                    '$sale_id',
                    '$debit',
                    '$credit',
                    '',
                    '$created_date'
                    )";
            $aCustomerCheck[$customer_id] = $customer_id;
        }
        
        $sql = $this->buildSqlInsert($aRowInsert);
        if(count($aRowInsert)){
            Yii::app()->db->createCommand($sql)->execute();
        }

        if(!empty($sErrors)){
            $this->addError('id', "Bạn nhập trùng khách hàng, vui lòng quay lại danh sách để kiểm tra. KH trùng: {$sErrors}");
        }
    }
    
    /** @Author: DungNT Jan 16, 2019
     *  @Todo: get model debit vo cua KH
     **/
    public function getDebitVoOfCustomer() {
        if(empty($this->customer_id)){
            return null;
        }
        $criteria = new CDbCriteria();
        $criteria->addCondition('t.customer_id = ' . $this->customer_id);
        $criteria->compare('t.type', InventoryCustomer::TYPE_LIMIT_DEBIT);
        return InventoryCustomer::model()->find($criteria);
    }
    
    /** @Author: KHANH TOAN Jan 09 2019
     *  @Todo: cập nhật tồn vỏ theo date cho các KH 
     * 1/   findAll model từ table InventoryCustomer theo type = InventoryCustomer::TYPE_LIMIT_DEBIT
     * 2/   lấy dữ liệu tồn đầu, nhập xuất, từ $mInventoryCustomer->reportDebitVo()
     * 3/   tính tồn cuối cho bình 12(vỏ nhỏ), bình 45(vỏ lớn)
     * 4/   update vào field json_vo: debit_vo_45 => 5, debit_vo_12 => 2
     **/
    public function updateDeBitVo() {
        $from = time();
        $criteria = new CDbCriteria();
        $criteria->compare('t.type', InventoryCustomer::TYPE_LIMIT_DEBIT);
        $models = InventoryCustomer::model()->findAll($criteria);
        $aCustomer = $aRes = [];
        foreach ($models as $key => $mInventoryCustomer) {
            $aCustomer[] = $mInventoryCustomer->customer_id;
        }
        $aRes = $this->getDataDebitVo($aCustomer);
        foreach($models as $key => $mInventoryCustomer){
            $debit_vo_12 = isset($aRes[$mInventoryCustomer->customer_id]['VO_12']) ? $aRes[$mInventoryCustomer->customer_id]['VO_12'] : 0;
            $debit_vo_45 = isset($aRes[$mInventoryCustomer->customer_id]['VO_45']) ? $aRes[$mInventoryCustomer->customer_id]['VO_45'] : 0;
            $json = json_decode($mInventoryCustomer->json_vo, true);
            $json['debit_vo_12'] = $debit_vo_12;
            $json['debit_vo_45'] = $debit_vo_45;
            $json_vo = json_encode($json);
            $mInventoryCustomer->json_vo = $json_vo;
            $mInventoryCustomer->update();
        }
        $to = time();
        $second = $to-$from;
        $info = count($models)." record TYPE_LIMIT_DEBIT - Run  mInventoryCustomer->updateDeBitVo done in:+ ".($second).'  Second  <=> '.($second/60).' Minutes '.date('Y-m-d H:i:s');
        Logger::WriteLog($info);
    }
    
    /** @Author: KHANH TOAN 2018
     *  @Todo: get data tinh ton cuoi binh 12 + binh 45
     *  @Param: $aCustomer
     **/
    public function getDataDebitVo($aCustomer) {
        $aData = $aRes = [];
        $mInventoryCustomer =  new InventoryCustomer();
        $mInventoryCustomer->date_from          = date('d-m-Y');
        $mInventoryCustomer->date_to            = $mInventoryCustomer->date_from;
        $mInventoryCustomer->aCustomerIdLimitVo = $aCustomer;
        $aData = $mInventoryCustomer->reportDebitVo();
        $this->formatData($aRes, $aData);
        return $aRes;
    }
    
    /** @Author: KHANH TOAN Jan 12 2018
     *  @Todo:
     **/
    public function formatData(&$aRes, $aData) {
        $aCustomer          = isset($aData['CUSTOMER_MODEL']) ? $aData['CUSTOMER_MODEL'] : [];
        $aMaterials         = isset($aData['MATERIALS_MODEL']) ? $aData['MATERIALS_MODEL'] : [];
        $BEGIN_VO           = isset($aData['BEGIN_VO']) ? $aData['BEGIN_VO'] : [];
        $OUTPUT_BEFORE      = isset($aData['OUTPUT_BEFORE']) ? $aData['OUTPUT_BEFORE'] : [];
        $OUTPUT_IN_PERIOD   = isset($aData['OUTPUT_IN_PERIOD']) ? $aData['OUTPUT_IN_PERIOD'] : [];
        $aIdVo              = isset($aData['ID_VO']) ? $aData['ID_VO'] : [];
        foreach ($aCustomer as $mCustomer){
            $customer_id = $mCustomer->id;
            $sum_end_nho = $sum_end_lon = 0;
            foreach ($aIdVo[$customer_id]['MATERIALS_VO_NHO'] as $materials_id => $qty){
                    $qty_begin          = isset($BEGIN_VO[$customer_id][$materials_id]) ? $BEGIN_VO[$customer_id][$materials_id] : 0;
                    $qty_import_before  = isset($OUTPUT_BEFORE[TYPE_STORE_CARD_IMPORT][$customer_id][$materials_id]) ? $OUTPUT_BEFORE[TYPE_STORE_CARD_IMPORT][$customer_id][$materials_id] : 0;
                    $qty_export_before  = isset($OUTPUT_BEFORE[TYPE_STORE_CARD_EXPORT][$customer_id][$materials_id]) ? $OUTPUT_BEFORE[TYPE_STORE_CARD_EXPORT][$customer_id][$materials_id] : 0;

                    $qty_import_in_period = isset($OUTPUT_IN_PERIOD[TYPE_STORE_CARD_IMPORT][$customer_id][$materials_id]) ? $OUTPUT_IN_PERIOD[TYPE_STORE_CARD_IMPORT][$customer_id][$materials_id] : 0;
                    $qty_export_in_period = isset($OUTPUT_IN_PERIOD[TYPE_STORE_CARD_EXPORT][$customer_id][$materials_id]) ? $OUTPUT_IN_PERIOD[TYPE_STORE_CARD_EXPORT][$customer_id][$materials_id] : 0;
                    $begin      = $qty_begin + $qty_export_before - $qty_import_before;
                    $end        = $begin + $qty_export_in_period - $qty_import_in_period;
    //                tính toán vỏ nhỏ
                    $sum_end_nho    += $end;
            }
            foreach ($aIdVo[$customer_id]['MATERIALS_VO_LON'] as $materials_id => $qty){
                $qty_begin          = isset($BEGIN_VO[$customer_id][$materials_id]) ? $BEGIN_VO[$customer_id][$materials_id] : 0;
                $qty_import_before  = isset($OUTPUT_BEFORE[TYPE_STORE_CARD_IMPORT][$customer_id][$materials_id]) ? $OUTPUT_BEFORE[TYPE_STORE_CARD_IMPORT][$customer_id][$materials_id] : 0;
                $qty_export_before  = isset($OUTPUT_BEFORE[TYPE_STORE_CARD_EXPORT][$customer_id][$materials_id]) ? $OUTPUT_BEFORE[TYPE_STORE_CARD_EXPORT][$customer_id][$materials_id] : 0;
                
                $qty_import_in_period = isset($OUTPUT_IN_PERIOD[TYPE_STORE_CARD_IMPORT][$customer_id][$materials_id]) ? $OUTPUT_IN_PERIOD[TYPE_STORE_CARD_IMPORT][$customer_id][$materials_id] : 0;
                $qty_export_in_period = isset($OUTPUT_IN_PERIOD[TYPE_STORE_CARD_EXPORT][$customer_id][$materials_id]) ? $OUTPUT_IN_PERIOD[TYPE_STORE_CARD_EXPORT][$customer_id][$materials_id] : 0;
                
                $begin      = $qty_begin + $qty_export_before - $qty_import_before;
                $end        = $begin + $qty_export_in_period - $qty_import_in_period;
//                Tính toán vỏ lớn
                $sum_end_lon    += $end;
            }
            $aRes[$customer_id]['VO_12'] = $sum_end_nho;
            $aRes[$customer_id]['VO_45'] = $sum_end_lon;
        }
    }

    /** @Author: LOCNV Jun 4, 2019
     *  @Todo: get array type debit vo
     *  @Param:
     * LOC005
     **/
    public function getArrayTypeDebitVo() {
        return array(
            InventoryCustomer::TYPE_DEBIT_VO_12 => 'Bình 12kg',
            InventoryCustomer::TYPE_DEBIT_VO_45 => 'Bình 45kg'
        );
    }
    
    /** @Author: DuongNV 18 Jun,2019
     *  @Todo: Những khách hàng khó tính, cho cột tổng xuống dưới
     **/
    public function getArrCustomerSumColBottom() {
        return [
            138146
        ];
    }

}