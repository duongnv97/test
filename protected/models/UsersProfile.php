<?php

/**
 * This is the model class for table "{{_users_profile}}".
 *
 * The followings are the available columns in table '{{_users_profile}}':
 * @property string $status_leave
 * @property string $user_id
 * @property integer $type
 * @property string $parent_id
 * @property integer $country_id
 * @property integer $type_nation
 * @property integer $type_religion
 * @property integer $type_education
 * @property string $id_number
 * @property integer $id_province
 * @property string $id_created_date
 * @property integer $status_marital
 * @property string $date_begin_job
 * @property integer $contract_type
 * @property string $contract_begin
 * @property string $contract_end
 * @property string $social_insurance_no
 * @property string $tax_no
 * @property string $bank_no
 * @property string $bank_id
 * @property string $address_home_province
 * @property string $address_live_province
 * @property string $update_by
 * @property string $position_work
 * @property string $json
 */
class UsersProfile extends BaseSpj
{
    /** @note 1/ dob để bên table user lấy colum: last_purchase, gender(giới tính), 
        2/ table gas_users_profile
        id, user_id, type (1:user, 2: vo, 3: con), parent_id, address_born (nơi sinh - text), address_home (đc quê), address_home_province (tỉnh nào)
        address_live (dc/ thường trú), address_live_province (tỉnh nào), country_id, type_nation (dân tộc), type_religion (tôn giáo), type_education (văn hóa), id_number (cmnd), id_province, id_created_date,
        status_marital (hôn nhân), date_begin_job (ngày thử việc), contract_type (loại hđ), contract_begin (date), contract_end, social_insurance_no,
        tax_no, bank_no, bank_id, list_email, list_phone, list_phone_family (json liên hệ khi cần: phone - name)
        - dc/ tạm trú: là nơi ở hiện tại, sẽ save ở address table user
        - Thường trú là đc ở quê 
        json: address_born, address_home, address_live, list_email, list_phone, list_phone_family (json liên hệ khi cần: phone - name)
     */
    public $date_begin_job_from, $date_begin_job_to, $address_born, $address_home, $address_live, $list_email, $list_phone, $list_phone_family, $note;
    public $mUser, $gas24h_code;
    public $JSON_FIELD      = array('address_born', 'address_home', 'address_live', 'list_email', 'list_phone', 'list_phone_family', 'note');
    const COUNTRY_VN        = 1;
    const COUNTRY_CHINA     = 2;
    const COUNTRY_LAO       = 3;
    const COUNTRY_CAMPUCHIA = 4;
    const COUNTRY_SINGAPORE = 5;
    
    const COUNT_MONTH_REAL  = 1;// số tháng kể từ ngày vào làm được lên chính thức => tính thưởng giám sát ccs
    const SALARY_NO                 = 0;
    const SALARY_YES                = 1;
    
    const SALARY_METHOD_VIETTEL     = 1;// 1: ViettelPay, 2: Chuyen khoan: 3: Tien mat
    const SALARY_METHOD_BANK        = 2;
    const SALARY_METHOD_CASH        = 3;
    
    const STATUS_LEAVE_WAIT         = 1;// field status_leave Tình trạng chờ nghỉ
    
    const WORK_ROOM_DIRECTOR        = 1;// 
    const WORK_ROOM_PTTT            = 2;// PTTT
    const WORK_ROOM_IT              = 3;// Phòng công nghệ
    const WORK_ROOM_ACCOUNTING      = 4;// Phòng kế toán
    const WORK_ROOM_BUSINESS        = 5;// Phòng kinh doanh
    const WORK_ROOM_TECHNICAL       = 6;// Phòng kỹ thuật
    const WORK_ROOM_MARKETING       = 7;// Phòng Marketing
    const WORK_ROOM_LAW             = 8;// Phòng pháp chế
    const WORK_ROOM_CALL_CENTER     = 9;// Phòng tổng đài
    const WORK_ROOM_MATERIAL        = 10;// Phòng vật tư
    const WORK_ROOM_TRAM_BENCAT     = 11;// Trạm Bến Cát
    const WORK_ROOM_TRAM_BINHDINH   = 12;// Trạm Bình Định
    const WORK_ROOM_TRAM_PHUOCTAN   = 13;// Trạm Phước Tân
    const WORK_ROOM_TRAM_VINHLONG   = 14;// Trạm Vĩnh Long
    const WORK_ROOM_86_NCV          = 15;// Văn phòng
    const WORK_ROOM_PVKH            = 16;// PVKH
    
    const WORK_DRIVER_XE_BON         = 17;// Tài xế xe bồn
    const WORK_DRIVER_XE_BINH        = 18;//  Tài xế xe bình
    const WORK_PHU_XE_BINH           = 19;// Phụ xe xe bình
    const WORK_QUAN_DOC             = 20;// Quản đốc
    const WORK_CONG_NHAN            = 21;// Công nhân
    const WORK_GIAM_SAT             = 22;// Giám sát
    const WORK_SECURITY             = 23;// Bảo vệ
    const WORK_TAP_VU               = 24;// Tạp vụ
    const WORK_ROOM_TELESALE        = 25;// Phòng telesale
    const WORK_ROOM_CCS             = 26;// CCS
    
    const POSITION_ROOM_EMPLOYEE    = 1;// Nhân viên
    const POSITION_ROOM_MANAGE      = 2;// phụ trách
    const POSITION_ROOM_HEAD        = 3;// trưởng phòng
    const POSITION_ROOM_DIRECTOR    = 4;// Giám đốc
    const POSITION_ROOM_TRY         = 5;// Học Việc
    const POSITION_ROOM_MULTI_TASK  = 6;// Kiêm nhiệm
    
    const PROFILE_STATUS_NEW        = 1;// chưa có hs
    const PROFILE_STATUS_UPDATE     = 2;// đang cập nhật 
    const PROFILE_STATUS_DONE       = 3;// đã đẩy đủ
    
    public function getArrCountry() {
        return [
            UsersProfile::COUNTRY_VN        => 'Việt Nam',
            UsersProfile::COUNTRY_CHINA     => 'Trung Quốc',
            UsersProfile::COUNTRY_LAO       => 'Lào',
            UsersProfile::COUNTRY_CAMPUCHIA => 'Campuchia',
            UsersProfile::COUNTRY_SINGAPORE => 'Singapore',
        ];
    }
    public function getArrProfileStatus() {
        return [
            UsersProfile::PROFILE_STATUS_NEW        => 'Chưa có',
            UsersProfile::PROFILE_STATUS_UPDATE     => 'Đang bổ sung thêm',
            UsersProfile::PROFILE_STATUS_DONE       => 'Đầy đủ',
        ];
    }
    
    public function getArrWorkRoom() {
        return [
            UsersProfile::WORK_ROOM_PTTT                => 'PTTT',
            UsersProfile::WORK_ROOM_CCS                 => 'CCS',
            UsersProfile::WORK_ROOM_CALL_CENTER         => 'Phòng tổng đài',
            UsersProfile::WORK_ROOM_PVKH                => 'PVKH',
            UsersProfile::WORK_ROOM_TELESALE            => 'Phòng telesale',
            UsersProfile::WORK_ROOM_IT                  => 'Phòng công nghệ',
            UsersProfile::WORK_ROOM_ACCOUNTING          => 'Phòng kế toán',
            UsersProfile::WORK_ROOM_BUSINESS            => 'Phòng kinh doanh',
            UsersProfile::WORK_ROOM_TECHNICAL           => 'Phòng kỹ thuật',
            UsersProfile::WORK_ROOM_MARKETING           => 'Phòng Marketing',
            UsersProfile::WORK_ROOM_LAW                 => 'Phòng pháp chế',
            UsersProfile::WORK_ROOM_MATERIAL            => 'Phòng vật tư',
//            UsersProfile::WORK_ROOM_TRAM_BENCAT         => 'Trạm Bến Cát',
//            UsersProfile::WORK_ROOM_TRAM_BINHDINH       => 'Trạm Bình Định',
//            UsersProfile::WORK_ROOM_TRAM_PHUOCTAN       => 'Trạm Phước Tân',
//            UsersProfile::WORK_ROOM_TRAM_VINHLONG       => 'Trạm Vĩnh Long',
//            UsersProfile::WORK_ROOM_86_NCV              => 'Văn phòng',
            UsersProfile::WORK_ROOM_DIRECTOR            => 'Ban giám đốc',
            UsersProfile::WORK_DRIVER_XE_BON            => 'Tài xế xe bồn',
            UsersProfile::WORK_DRIVER_XE_BINH           => 'Tài xế xe bình',
            UsersProfile::WORK_PHU_XE_BINH              => 'Phụ xe xe bình',
            UsersProfile::WORK_QUAN_DOC                 => 'Quản đốc',
            UsersProfile::WORK_CONG_NHAN                => 'Công nhân',
            UsersProfile::WORK_GIAM_SAT                 => 'Giám sát',
            UsersProfile::WORK_SECURITY                 => 'Bảo vệ',
            UsersProfile::WORK_TAP_VU                   => 'Tạp vụ',
            
        ];
    }
    
    public function getArrPositionRoom() {
        return [
            UsersProfile::POSITION_ROOM_EMPLOYEE        => 'Nhân viên',
            UsersProfile::POSITION_ROOM_MANAGE          => 'Phụ trách',
            UsersProfile::POSITION_ROOM_HEAD            => 'Trưởng phòng',
            UsersProfile::POSITION_ROOM_DIRECTOR        => 'Giám đốc',
            UsersProfile::POSITION_ROOM_TRY             => 'Học việc',
            UsersProfile::POSITION_ROOM_MULTI_TASK      => 'Kiêm nhiệm',
        ];
    }
    
    public function getArrStatusLeave() {
        return [
            UsersProfile::STATUS_LEAVE_WAIT => 'Chờ nghỉ việc',
        ];
    }
    
    public function getArrNation() {//thuộc dân tộc nào
        return [
            1 => 'Kinh (Việt)', 2 => 'Tày', 55 => 'Thái', 3 => 'Mường', 4 => 'Khmer', 5 => 'Hoa', 6 => 'Nùng', 7 => "H'Mông", 8 => 'Dao', 9 => 'Gia Rai', 10 => 'Ê Đê', 11 => 'Ba Na',
            12 => 'Sán Chay',13 => 'Chăm',14 => 'Cơ Ho',15 => 'Xơ Đăng',16 => 'Sán Dìu',17 => 'Hrê',18 => 'Ra Glai',19 => 'Mnông',20 => 'Thổ',21 => 'Xtiêng',22 => 'Khơ mú',23 => 'Bru - Vân Kiều',24 => 'Cơ Tu',25 => 'Giáy',26 => 'Tà Ôi',27 => 'Mạ',
            28 => 'Giẻ-Triêng',29 => 'Co',30 => 'Chơ Ro',31 => 'Xinh Mun',32 => 'Hà Nhì',33 => 'Chu Ru',34 => 'Lào',35 => 'La Chí',36 => 'Kháng',37 => 'Phù Lá',38 => 'La Hủ',39 => 'La Ha',40 => 'Pà Thẻn',41 => 'Lự',42 => 'Ngái',43 => 'Chứt',44 => 'Lô Lô',45 => 'Mảng',46 => 'Cơ Lao',47 => 'Bố Y',48 => 'Cống',49 => 'Si La',50 => 'Pu Péo',51 => 'Rơ Măm',52 => 'Brâu',53 => 'Ơ Đu',54 => 'Người nước ngoài',
        ];
    }
    public function getArrReligion() {//thuộc type_religion (tôn giáo) nào
        return [
            1 => 'Phật giáo', 2 => 'Công giáo', 3 => 'Tin Lành', 4 => 'Cao Đài', 5 => 'Phật giáo Hòa Hảo', 6 => 'Hồi giáo', 7 => "Bahá'í", 8 => 'Tịnh độ cư sĩ Phật hội Việt Nam', 9 => 'Đạo Tứ Ân Hiếu Nghĩa', 10 => 'Đạo Bửu Sơn Kỳ Hương', 11 => 'Minh Sư Đạo', 12=>'Minh Lý Đạo', 13=>'Bà-la-môn'
        ];
    }
    public function getArrEducation() {//thuộc type_education (văn hóa) nào
        return [
            1 => '12/12', 2=>'Trung cấp', 3 => 'Cao đẳng', 4 => 'Đại học', 5 => 'Thạc sĩ', 6 => 'Tiến sĩ', 7 => 'Chưa học hết 12'
        ];
    }
    public function getArrStatusMarital() {//status_marital (hôn nhân)
        return [
            1 => 'Độc thân', 2 => 'Đã kết hôn'
        ];
    }
    public function getArrContractType() {//contract_type (loại hđ)
        return [
            UsersProfile::CONTRACT_TRY          => 'Thử việc', 
            UsersProfile::CONTRACT_1_YEAR       => '1 năm', 
            UsersProfile::CONTRACT_3_YEAR       => '3 năm',
            UsersProfile::CONTRACT_FOREVER      => 'Không thời hạn',
            UsersProfile::CONTRACT_PART_TIME    => 'Part time',
            UsersProfile::CONTRACT_WAIT_INTERVIEW => 'Chờ phỏng vấn',
            UsersProfile::CONTRACT_INTERNSHIP   => 'Thực tập sinh',
        ];
    }
    const CONTRACT_TRY          = 1;// Thử việc
    const CONTRACT_1_YEAR       = 2;
    const CONTRACT_3_YEAR       = 3;
    const CONTRACT_FOREVER      = 4;
    const CONTRACT_PART_TIME    = 5;
    const CONTRACT_WAIT_INTERVIEW   = 6;
    const CONTRACT_INTERNSHIP       = 7;// thực tập
    
    public function getArraySalaryMethod() {
        $aRes = [
            UsersProfile::SALARY_METHOD_VIETTEL     => 'Viettel Pay',
            UsersProfile::SALARY_METHOD_BANK        => 'Chuyển khoản',
            UsersProfile::SALARY_METHOD_CASH        => 'Không',
        ];
        $cRole = MyFormat::getCurrentRoleId();
        $aRoleShowAll = [ROLE_ADMIN, ROLE_ACCOUNTING, ROLE_RECEPTION];
        if(!in_array($cRole, $aRoleShowAll)){
            unset($aRes[UsersProfile::SALARY_METHOD_CASH]);
        }
        return $aRes;
    }

    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{_users_profile}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        return array(
            array('date_begin_job, position_work, id_number, contract_type', 'required', 'on'=>'employees_create, employees_update'),
            array('country_id', 'required', 'on'=>'createFamily, updateFamily'),
            array('id_number','unique','message'=>'Số CMND này đã có trên hệ thống, vui lòng kiểm tra lại nhân viên đã được tạo trên hệ thống rồi'),
            array('salary_method_account','unique','message'=>'Số thẻ Viettel Pay này đã có trên hệ thống, vui lòng kiểm tra lại', 'on'=>'employees_create, employees_update'),
            array('first_name_root,date_begin_job_real, leave_date, id, user_id, type, parent_id, country_id, type_nation, type_religion, type_education, id_number, id_province, id_created_date, status_marital, date_begin_job, contract_type, contract_begin, contract_end, social_insurance_no, tax_no, bank_no, bank_id, address_home_province, address_live_province, update_by, update_time, json', 'safe'),
            array('profile_status, salary_method, salary_method_phone, salary_method_account, date_begin_job_from, date_begin_job_to, status_leave, bank_province_id, bank_branch, position_room, position_work, gas24h_code, position_apply, salary_insurance, insurance_start, insurance_end, note, base_salary, address_born, address_home, address_live, list_email, list_phone, list_phone_family', 'safe'),
            array('salary_method_phone', 'checkPhone', 'on'=>'employees_create, employees_update'),
        );
    }//(json liên hệ khi cần: phone - name)

    public function checkPhone($attribute,$params){
        $oldPhoneNumber = $this->salary_method_phone;
        $this->salary_method_phone = UsersPhone::removeLeftZero(trim($this->salary_method_phone));
        if(empty($this->salary_method_phone)){
            return ;
        }
        /* 1. check chiều dài phone
         * 2. check phone phải là di động
         */
        $phoneNumber = $this->salary_method_phone;
        if(!UsersPhone::isValidCellPhone($phoneNumber)){// 1 + 2
            $this->addError('salary_method_phone', "Số điện thoại $oldPhoneNumber không phải là số di động hợp lệ");
        }
    }
    
    /** @return array relational rules. */
    public function relations(){
        return [
            'rUser' => array(self::BELONGS_TO, 'Users', 'user_id'),
        ];
    }
    
    /** @Author: DungNT Jun 13, 2018
     *  @Todo: get list uid View All
     **/
    public function getListUidViewAll() {
        return [
            GasConst::UID_THUY_HT,
        ];
    }
    public function getListUidUpdateAll() {
        $aRes = GasConst::getAdminArray();
        $aRes[] = GasConst::UID_HANH_NT;
        $aRes[] = GasConst::UID_CHAU_LNM;
        $aRes[] = GasLeave::UID_CHIEF_ACCOUNTANT;
        return $aRes;
    }
    // list user nhập liệu lại PM nhân sự
    public function getListUidFixData() {
        return [
//            GasConst::UID_NGOC_PT,
//            GasConst::UID_HUE_LT,
//            GasConst::UID_LONG_PN,
//            GasTickets::DIEU_PHOI_HIEU_TM,
            GasLeave::UID_QUYNH_MTN,
            GasLeave::UID_CHIEF_ACCOUNTANT,
//            GasLeave::DUNG_NTT,
            GasConst::UID_CHAU_LNM,
        ];
    }
    
    /** @Author: DungNT Feb 19, 2018
     *  @Todo: define uid tạo hồ sơ nhân viên thử việc
     *  tạo ở hồ sơ ứng viên trực tiếp, không phải bộ phận nhân sự ở văn phòng
     **/
    public function getListUidCreate() {
        $aRes = [
//            2,
//            GasConst::UID_TINH_P, // Phan Tịnh
//            865752, // Trần Văn Bình
            GasConst::UID_NGOC_PT,
            GasConst::UID_TRUONG_NN,
//            GasLeave::PHUC_HV,
            GasLeave::UID_VIEW_ALL_TAY_NGUYEN_GIA_LAI,
        ];
        $aRole          = $this->getRoleAllowCreate();
        $listdataUser   = [];
        $mAppCache      = new AppCache();
        foreach($aRole as $role_id):
            $aCvPttt        = $mAppCache->getUserByRole($role_id);
            $aTmp           = CHtml::listData($aCvPttt, 'id', 'id');
            $listdataUser   = array_merge($listdataUser, $aTmp);
        endforeach;
//        $listdataUser   = CHtml::listData($aCvPttt, 'id', 'id');

        return array_merge($aRes, $listdataUser);
    }
    
    /** @Author: DungNT May 08, 2018
     *  @Todo: get array role cho phép tạo ứng viên tuyển dụng
     **/
    public function getRoleAllowCreate() {
        return [ROLE_DIEU_PHOI, ROLE_BRANCH_DIRECTOR, ROLE_TELESALE, ROLE_HEAD_OF_MAINTAIN, ROLE_MONITORING_MARKET_DEVELOPMENT, ROLE_MONITOR_AGENT, ROLE_ACCOUNTING_ZONE, ROLE_ACCOUNTING];
//        return [ROLE_MONITORING_MARKET_DEVELOPMENT, ROLE_MONITOR_AGENT, ROLE_ACCOUNTING_ZONE];
    }
    
    /** @Author: DungNT Apr 23, 2019 
     *  @Todo: get array role allow change code 4 so trong update nhan su
     **/
    public function getArrayRoleAllowChangeCode() {
        return [ROLE_PARTNER, ROLE_EMPLOYEE_MARKET_DEVELOPMENT, ROLE_TELESALE];
    }
    /** @Author: DungNT Apr 23, 2019 
     *  @Todo:check User Nhân sự allow update change code 4 so trong update Nhân sự
     **/
    public function canUpdateCodeAppGas24h() {
        $cUid   = MyFormat::getCurrentUid();
        $cRole  = MyFormat::getCurrentRoleId();
        $aUidAllow = [GasConst::UID_CHAU_LNM, GasConst::UID_HANH_NT];
        if($cRole == ROLE_ADMIN || in_array($cUid, $aUidAllow)){
            return true;
        }
        return false;
    }
    
    /** @Author: DungNT May 21, 2018
     *  @Todo: get array credit of PVKH OR type partner
     **/
    public function getArrayCredit($mUser) {
        $aCredit    = GasConst::getArrCredit();
        $aPartner   = GasConst::getArrPartner();
        if($this->id == -1){// flag get data for search
            return $aCredit+$aPartner;
        }
        if($mUser->role_id == ROLE_EMPLOYEE_MAINTAIN){
            return $aCredit;
        }elseif($mUser->role_id == ROLE_PARTNER){
            return $aPartner;
        }
        return [];
    }

    /** bitrix_no: sj0011
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'user_id' => 'User',
            'type' => 'Loại quan hệ',
            'parent_id' => 'Parent',
            'country_id' => 'Quốc gia',
            'type_nation' => 'Dân tộc',
            'type_religion' => 'Tôn giáo',
            'type_education' => 'Học vấn',
            'id_number' => 'Số CMND / Số Căn cước công dân',
            'id_province' => 'Nơi cấp',
            'id_created_date' => 'Ngày cấp',
            'status_marital' => 'Hôn nhân',
            'date_begin_job' => 'Ngày vào làm',
            'contract_type' => 'Loại hợp đồng',
            'contract_begin' => 'Ngày ký hợp đồng',
            'contract_end' => 'Ngày kết thúc HĐ',
            'social_insurance_no' => 'Số BHXH',
            'tax_no' => 'Mã số thuế',
            'bank_no' => 'Số TK ngân hàng',
            'bank_id' => 'Ngân hàng',
            'address_home_province' => 'Tỉnh quê quán',
            'address_live_province' => 'Tỉnh thường trú',
            'update_by' => 'Cập nhật bởi',
            'update_time' => 'Ngày cập nhật',
            'json' => 'Json Address',
            'base_salary' => 'Lương căn bản',
            'salary_insurance' => 'Lương BHXH',
            
            'address_born' => 'Nơi sinh',
            'address_home' => 'Quê quán',
            'address_live' => 'Địa chỉ thường trú',
            'list_email' => 'Email khác',
            'list_phone' => 'Các số đt khác',
            'list_phone_family' => 'Điện thoại liên lạc khi cần',
            'note' => 'Ghi chú',
            'leave_date' => 'Ngày nghỉ việc',
            'insurance_start' => 'Ngày tham gia(BHXH)',
            'insurance_end' => 'Ngày hết hạn(BHXH)',
            'position_apply' => 'Vị trí ứng tuyển (dùng cho tuyển dụng NV )',
            'gas24h_code' => 'Mã code Gas24h',
            'position_work' => 'Bộ phận làm việc',
            'position_room' => 'Chức vụ',
            'bank_province_id' => 'Tỉnh làm thẻ',
            'bank_branch' => 'Chi nhánh ngân hàng',
            'status_leave' => 'Tình trạng nghỉ',
            'date_begin_job_from' => 'Vào làm từ ngày',
            'date_begin_job_to' => 'Đến ngày',
            'salary_method'         => 'Hình thức trả lương',
            'salary_method_phone'   => 'Số đt Viettel Pay',
            'salary_method_account' => 'Số tài khoản Viettel Pay',
            'profile_status'        => 'Tình trạng hồ sơ',
            'date_begin_job_real'   => 'Ngày làm chính thức',
            'first_name_root'       => 'Tên trong giấy khai sinh',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        $criteria=new CDbCriteria;
        $criteria->order = 't.id DESC';
        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
            'pagination'=>array(
                'pageSize'=> Yii::app()->user->getState('pageSize',Yii::app()->params['defaultPageSize']),
            ),
        ));
    }
    
    public function getListdataCompany() {
        return Users::getArrDropdown(ROLE_COMPANY);
    }
    public function getListdataBank() {
        return Users::getArrDropdown(ROLE_BANK);
    }
    /** @Author: DungNT Aug 21, 2017
     * @Todo: định dạng lại data trước khi lưu
     */
    public function formatDataSave() {
        if($this->isNewRecord && empty($this->first_name_root)){
            $mUser = Users::model()->findByPk($this->user_id);
            if(!empty($mUser)){
                $this->first_name_root  = $mUser->first_name;
            }
        }
        if(strpos($this->date_begin_job, '/')){
            $this->date_begin_job = MyFormat::dateConverDmyToYmd($this->date_begin_job);
            MyFormat::isValidDate($this->date_begin_job);
            if(empty($this->date_begin_job_real)){
                $this->date_begin_job_real  = MyFormat::modifyDays($this->date_begin_job, self::COUNT_MONTH_REAL, '+', 'month');
            }else{
                if(strpos($this->date_begin_job_real, '/')){
                     $this->date_begin_job_real = MyFormat::dateConverDmyToYmd($this->date_begin_job_real);
                }
            }
            MyFormat::isValidDate($this->date_begin_job_real);
            
        }
        if(strpos($this->contract_begin, '/')){
            $this->contract_begin = MyFormat::dateConverDmyToYmd($this->contract_begin);
            MyFormat::isValidDate($this->contract_begin);
        }
        if(strpos($this->contract_end, '/')){
            $this->contract_end = MyFormat::dateConverDmyToYmd($this->contract_end);
            MyFormat::isValidDate($this->contract_end);
        }
        if(strpos($this->id_created_date, '/')){
            $this->id_created_date = MyFormat::dateConverDmyToYmd($this->id_created_date);
            MyFormat::isValidDate($this->id_created_date);
        }
        if(strpos($this->leave_date, '/')){
            $this->leave_date = MyFormat::dateConverDmyToYmd($this->leave_date);
            MyFormat::isValidDate($this->leave_date);
        }
        if(strpos($this->insurance_start, '/')){
            $this->insurance_start = MyFormat::dateConverDmyToYmd($this->insurance_start);
            MyFormat::isValidDate($this->insurance_start);
        }
        if(strpos($this->insurance_end, '/')){
            $this->insurance_end = MyFormat::dateConverDmyToYmd($this->insurance_end);
            MyFormat::isValidDate($this->insurance_end);
        }
        $this->setJsonDataField();
    }
    protected function afterValidate() {
        $this->base_salary      = MyFormat::removeComma($this->base_salary);
        $this->salary_insurance = MyFormat::removeComma($this->salary_insurance);
        $cUid = MyFormat::getCurrentUid();
        if(in_array($cUid, $this->getListUidCreate()) && empty($this->position_apply) && !in_array($cUid, $this->getListUidUpdateAll()) ){
            $this->addError('position_apply', 'Chưa chọn vị trí ứng tuyển của nhân viên');
        }
        return parent::afterValidate();
    }
    protected function beforeValidate() {
        $this->cleanDataPost();
        $this->id_number      = $this->id_number > 0 ? $this->id_number : '';
        return parent::beforeValidate();
    }
    public function cleanDataPost() {
        $this->id_created_date      = trim($this->id_created_date);
        $this->date_begin_job       = trim($this->date_begin_job);
        $this->contract_begin       = trim($this->contract_begin);
        $this->contract_end         = trim($this->contract_end);
        $this->leave_date           = trim($this->leave_date);
        $this->insurance_start      = trim($this->insurance_start);
        $this->insurance_end        = trim($this->insurance_end);
        $this->salary_method_phone      = trim($this->salary_method_phone);
        $this->salary_method_account    = MyFormat::removeAllWhiteSpace($this->salary_method_account);
    }
    
    /** @Author: DungNT Aug 21, 2017
     * @Todo: định dạng lại data trước khi view
     */
    public function formatDataView() {
        if(!empty($this->date_begin_job)){
            $this->date_begin_job = MyFormat::dateConverYmdToDmy($this->date_begin_job);
        }
        if(!empty($this->date_begin_job_real)){
            $this->date_begin_job_real = MyFormat::dateConverYmdToDmy($this->date_begin_job_real);
        }
        if(!empty($this->contract_begin)){
            $this->contract_begin = MyFormat::dateConverYmdToDmy($this->contract_begin);
        }
        if(!empty($this->contract_end)){
            $this->contract_end = MyFormat::dateConverYmdToDmy($this->contract_end);
        }
        if(!empty($this->id_created_date)){
            $this->id_created_date = MyFormat::dateConverYmdToDmy($this->id_created_date);
        }
        if(!empty($this->leave_date)){
            $this->leave_date = MyFormat::dateConverYmdToDmy($this->leave_date);
        }
        if(!empty($this->insurance_start)){
            $this->insurance_start = MyFormat::dateConverYmdToDmy($this->insurance_start);
        }
        if(!empty($this->insurance_end)){
            $this->insurance_end = MyFormat::dateConverYmdToDmy($this->insurance_end);
        }
        $this->mapJsonFieldOneDecode('JSON_FIELD', 'json', 'baseArrayJsonDecode');
        $this->gas24h_code = $this->getGas24hCode();
    }

    public function getLeaveDate() {
        return MyFormat::dateConverYmdToDmy($this->leave_date);
    }
    public function getContractType() {
        $aContract = $this->getArrContractType();
        return isset($aContract[$this->contract_type]) ? $aContract[$this->contract_type] : '';
    }
    public function getDateBeginJob() {
        return MyFormat::dateConverYmdToDmy($this->date_begin_job);
    }
    public function getContractBegin() {
        return MyFormat::dateConverYmdToDmy($this->contract_begin);
    }
    public function getContractEnd() {
        return MyFormat::dateConverYmdToDmy($this->contract_end);
    }
    public function getInsuranceStart() {
        return MyFormat::dateConverYmdToDmy($this->insurance_start);
    }
    public function getInsurancEnd() {
        return MyFormat::dateConverYmdToDmy($this->insurance_end);
    }
    public function getIdCreatedDate() {
        return MyFormat::dateConverYmdToDmy($this->id_created_date);
    }
    public function getBaseSalary() {
        return ActiveRecord::formatCurrencyRound($this->base_salary);
    }
    public function getBaseSalaryNormal() {
        return $this->base_salary;
    }
    public function getSalaryInsurance() {
        return ActiveRecord::formatCurrencyRound($this->salary_insurance);
    }
    public function getSalaryInsuranceNormal() {
        return $this->salary_insurance;
    }
    public function getSocialInsuranceNo() {
        return $this->social_insurance_no;
    }
    public function getTaxNo() {
        return $this->tax_no;
    }
    public function getBankNo() {
        return $this->bank_no;
    }
    public function getBankName() {
        if(empty($this->bank_id)){
            return ;
        }
        $aBank = $this->getListdataBank();
        return isset($aBank[$this->bank_id]) ? $aBank[$this->bank_id] : '';
    }
    public function getIdNumber() {
        return $this->id_number;
    }
    public function getIdProvince() {
        if(empty($this->id_province)){
            return '';
        }
        $mAppCache = new AppCache();
        $aProvince = $mAppCache->getListdata('GasProvince', AppCache::LISTDATA_PROVINCE);
        return isset($aProvince[$this->id_province]) ? $aProvince[$this->id_province] : '';
    }
    public function getStatusMarital() {
        $aMaterital = $this->getArrStatusMarital();
        return isset($aMaterital[$this->status_marital]) ? $aMaterital[$this->status_marital] : '';
    }
    public function getCountry() {
        $aCountry = $this->getArrCountry();
        return isset($aCountry[$this->country_id]) ? $aCountry[$this->country_id] : '';
    }
    public function getTypeNation() {
        $aData = $this->getArrNation();
        return isset($aData[$this->type_nation]) ? $aData[$this->type_nation] : '';
    }
    public function getTypeReligion() {
        $aData = $this->getArrReligion();
        return isset($aData[$this->type_religion]) ? $aData[$this->type_religion] : '';
    }
    public function getTypeEducation() {
        $aData = $this->getArrEducation();
        return isset($aData[$this->type_education]) ? $aData[$this->type_education] : '';
    }
    public function getAddressHomeProvince() {
        if(empty($this->address_home_province)){
            return '';
        }
        $mAppCache = new AppCache();
        $aProvince = $mAppCache->getListdata('GasProvince', AppCache::LISTDATA_PROVINCE);
        return isset($aProvince[$this->address_home_province]) ? $aProvince[$this->address_home_province] : '';
    }
    public function getAddressLiveProvince() {
        if(empty($this->address_live_province)){
            return '';
        }
        $mAppCache = new AppCache();
        $aProvince = $mAppCache->getListdata('GasProvince', AppCache::LISTDATA_PROVINCE);
        return isset($aProvince[$this->address_live_province]) ? $aProvince[$this->address_live_province] : '';
    }
    public function getNote() {
        return nl2br($this->note);
    }
    public function getPositionApply() {
        $session=Yii::app()->session;
        if(!isset($session['ROLE_NAME_USER']) || !isset($session['ROLE_NAME_USER'][$this->position_apply]))
                $session['ROLE_NAME_USER'] = Roles::getArrRoleName();
        return isset($session['ROLE_NAME_USER'][$this->position_apply]) ? $session['ROLE_NAME_USER'][$this->position_apply] : '';
    }
    public function getPositionWork() {
        $aData = $this->getArrWorkRoom();
        return isset($aData[$this->position_work]) ? $aData[$this->position_work] : '';
    }
    public function getPositionRoom() {
        $aData = $this->getArrPositionRoom();
        return isset($aData[$this->position_room]) ? $aData[$this->position_room] : '';
    }
    public function getBankProvince() {
        if(empty($this->bank_province_id)){
            return '';
        }
        $mAppCache = new AppCache();
        $aProvince = $mAppCache->getListdata('GasProvince', AppCache::LISTDATA_PROVINCE);
        return isset($aProvince[$this->bank_province_id]) ? $aProvince[$this->bank_province_id] : '';
    }
    public function getBankBranch() {
        return $this->bank_branch;
    }
    public function getStatusLeave() {
        if(empty($this->status_leave)){
            return '';
        }
        $aStatusLeave = $this->getArrStatusLeave();
        return isset($aStatusLeave[$this->status_leave]) ? '<b>'.$aStatusLeave[$this->status_leave].'</b>' : '';
    }
    public function getSalaryMethod() {
        $aData = $this->getArraySalaryMethod();
        return isset($aData[$this->salary_method]) ? $aData[$this->salary_method] : '';
    }
    public function getSalaryMethodPhone() {
        if(empty($this->salary_method_phone)){
            return '';
        }
        return '0'.$this->salary_method_phone;
    }
    public function getSalaryMethodAccount() {
        return $this->salary_method_account;
    }
    public function getProfileStatus() { 
        $aCountry = $this->getArrProfileStatus();
        return isset($aCountry[$this->profile_status]) ? $aCountry[$this->profile_status] : '';
    }
    public function getFirstNameRoot() {
        return $this->first_name_root;
    }

    /** @Author: DungNT Sep 08, 2017
     *  @Todo: check user can access Profile
     */
    public function canAccess() {
        $aUidAllowAll   = $this->getListUidUpdateAll();
        $mUser          =  null;
        $aUidAllow      = GasConst::getArrUidZoneManager();
        $cUid           = MyFormat::getCurrentUid();
        $cRole          = MyFormat::getCurrentRoleId();
        $aRoleNotAllow   = [ROLE_PARTNER, ROLE_CANDIDATE, ROLE_DEPARTMENT, ROLE_SUB_USER_AGENT];
        if(isset($_GET['id'])){
            $this->user_id = $_GET['id'];
        }
        if(!empty($this->user_id)){
            $mUser = Users::model()->findByPk($this->user_id);
        }
        
        if($mUser && in_array($mUser->role_id, $aRoleNotAllow) && !in_array($cUid, $aUidAllowAll)){
            return false;
        }
//        if(in_array($cUid, $aUidAllowAll) || in_array($cUid, $this->getListUidFixData())){
        if(in_array($cUid, $aUidAllowAll)){
            return true;
        }
        if(empty($this->user_id) && ( in_array($cUid, $this->getListUidCreate()) || in_array($cUid, $aUidAllow))){
            return true;
        }
        if($mUser && ( in_array($cUid, $this->getListUidCreate()) || in_array($cUid, $aUidAllow) ) ){
            if($mUser->created_by == $cUid && $mUser->role_id == ROLE_CANDIDATE){// cho phép người tạo đc update và view cho 1 số loại user
                return true;
            }
        }

        return false;
    }

    
    
    /** @Author: DungNT Feb 19, 2018
     *  @Todo: get list role Allow create 
     **/
    public function getArrayRole() {
        $mRole = new Roles();
        $cUid = MyFormat::getCurrentUid();
        $aRes = [];
        if(in_array($cUid, $this->getListUidCreate()) && !in_array($cUid, $this->getListUidUpdateAll())){
            $needMore['InRole'] = [ROLE_CANDIDATE];
            $aRes = $mRole->getDataSelectV1($needMore);
        }else{
            $aRes = Roles::getDataSelect(Roles::getRestrict());
        }
        return $aRes;
    }
    /** @Author: DungNT Feb 25, 2018
     *  @Todo: get list role Position Apply
     **/
    public function getArrayRolePositionApply() {
        $mRole = new Roles();
        $cUid = MyFormat::getCurrentUid();
        $aRes = [];
        $needMore['InRole'] = [ROLE_EMPLOYEE_MARKET_DEVELOPMENT, ROLE_EMPLOYEE_MAINTAIN,
            ROLE_ACCOUNTING_ZONE, ROLE_DRIVER, ROLE_TECHNICAL, ROLE_E_MAINTAIN, ROLE_CALL_CENTER, 
            ROLE_SALE, ROLE_MONITOR_AGENT, ROLE_HEAD_OF_BUSINESS
            ];
        $aRes = $mRole->getDataSelectV1($needMore);
        return $aRes;
    }
    
    /** @Author: DungNT Feb 19, 2018 -- Define show hide some field */
    public function showNoiCongTac() {
        $ok = true;
        $cUid = MyFormat::getCurrentUid();
        if(in_array($cUid, $this->getListUidCreate()) && !in_array($cUid, $this->getListUidUpdateAll())){
            $ok = false;
        }
        return $ok;
    }
    public function showRole() {
        $cUid = MyFormat::getCurrentUid();
        if(in_array($cUid, $this->getListUidFixData())){
            return true;
        }
        return $this->showNoiCongTac();
    }
    public function showUsername() {
        return $this->showNoiCongTac();
    }
    public function showStatus() {
        return $this->showNoiCongTac();
    }
    public function canViewSalary() {
        $cUid = MyFormat::getCurrentUid();
        $canViewSalary      = true;
        $aUidViewSalary     = [GasConst::UID_ADMIN, GasConst::UID_HANH_NT, GasConst::UID_CHAU_LNM];
        if(!in_array($cUid, $aUidViewSalary)){
            $canViewSalary = false;
        }
        return $canViewSalary;
    }
    public function showBlockFamily($mUser) {
        if($mUser->role_id == ROLE_FAMILY){
            return true;
        }
        return false;
    }
    public function showGas24hCode($mUser) {
        $cRole = MyFormat::getCurrentRoleId();
        if(!$this->canUpdateCodeAppGas24h()){
            return false;
        }
        return in_array($mUser->role_id, $this->getArrayRoleAllowChangeCode());
    }
    public function getGas24hCodeOnGrid() {
        if(!in_array($this->mUser->role_id, $this->getArrayRoleAllowChangeCode())){
            return '';
        }
        return "<b>{$this->getGas24hCode()}</b>";
    }
    public function showSalaryViettelPay() {
        if($this->salary_method != UsersProfile::SALARY_METHOD_VIETTEL){
            return 'display_none';
        }
        return '';
    }
    public function showSalaryBank() {
        if($this->salary_method != UsersProfile::SALARY_METHOD_BANK){
            return 'display_none';
        }
        return '';
    }
    public function showPositionRoom() {
        $cUid = MyFormat::getCurrentUid();
        return in_array($cUid, $this->getListUidUpdateAll());
    }
    
    /** @Author: DungNT May 21, 2018
     *  @Todo: get code of partner
     **/
    public function getGas24hCode() {
        if(!in_array($this->mUser->role_id, $this->getArrayRoleAllowChangeCode())){
            return '';
        }
        $mAppPromotion = new AppPromotion();
        $mAppPromotion->owner_id        = $this->mUser->id;
        $code = $mAppPromotion->getOnlyCodeByUser();
        if(!empty($mAppPromotion->mPromotion)){
            $mAppPromotion->mPromotion->setPartnerType(AppPromotion::PARTNER_TYPE_GRAB);
        }
        return $code;
    }
    /** @Author: DungNT May 21, 2018
     *  @Todo: change code of partner
     **/
    public function changeGas24hCode() {
        if(!in_array($this->mUser->role_id, $this->getArrayRoleAllowChangeCode())){
            return ;
        }
        $oldCode    = $this->getGas24hCode();
        $newCode    = $this->gas24h_code;
        if(empty($newCode) || $oldCode == $newCode){
            return ;
        }
        $mAppPromotion              = new AppPromotion();
        $mAppPromotion->owner_id    = $this->mUser->id;
        $mAppPromotion              = $mAppPromotion->getByOwner();
        if(empty($mAppPromotion)){
            return ;
        }
        $mAppPromotion->code_no     = $newCode;
        $mAppPromotion->scenario    = 'updateCode';
        $mAppPromotion->validate();
        if($mAppPromotion->hasErrors()){
            $this->addError('gas24h_code', "Mã $this->gas24h_code đã tồn tại, vui lòng nhập mã khác");
            return ;
        }
        $mAppPromotion->update(['code_no', 'owner_role_id']);
    }
    
    /** @Author: DungNT May 08, 2018
     *  @Todo: map position to manager để lấy email
     **/
    public function getManagerPosition() {
        return [
//            ROLE_EMPLOYEE_MARKET_DEVELOPMENT    => [GasLeave::PHUC_HV, GasLeave::UID_CHIEF_MONITOR],
            ROLE_EMPLOYEE_MARKET_DEVELOPMENT    => [GasLeave::PHUC_HV],
            ROLE_EMPLOYEE_MAINTAIN              => [GasLeave::PHUC_HV],
            ROLE_CALL_CENTER                    => [GasConst::UID_PHUONG_NT],
            ROLE_DRIVER                         => [GasLeave::UID_DAT_NV],
            ROLE_TECHNICAL                      => [GasLeave::UID_HEAD_TECHNICAL, GasSupportCustomer::UID_QUY_PV],
        ];
    }
    
    /** @Author: DungNT Jun 29, 2019
     *  @Todo: get record by user id
     **/
    public function getByUserId($user_id) {
        $criteria = new CDbCriteria();
        $criteria->compare('t.user_id', $user_id);
        return UsersProfile::model()->find($criteria);
    }
    // update profile_status is ok
    public function updateFullProfile(){
        $this->profile_status = self::PROFILE_STATUS_DONE;
        $this->update();
    }
    // check profile_status can update or not 
    public function canUpdateFull(){
        if($this->profile_status == self::PROFILE_STATUS_DONE){
            return false;
        }
        return true;
    }
    
    /** @Author: DungNT Aug 05, 2019
     *  @Todo: khi chuyển trạng thái chờ Nghỉ Việc hoặc khóa tài khoản thì remove user PVKH OR PTTT of agent
     *  để hệ thống không gửi notify và SMS đơn HGĐ cho NV PVKH
     *  @note: NV chờ nghỉ việc, NV ko hoạt động là remove
     **/
    public function removeLockUserPvkhOfAgent() {
        $mUser = $this->mUser;
        if($mUser->status == STATUS_INACTIVE || $this->status_leave == UsersProfile::STATUS_LEAVE_WAIT){
            $mCronUpdate            = new CronUpdate();
            $mCronUpdate->writeLog  = false;
            $mCronUpdate->cronRemoveInactiveUserOfAgent();
        }
    }
    
    
}