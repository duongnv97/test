<?php

/**
 * This is the model class for table "{{_gas_leave_holidays}}".
 *
 * The followings are the available columns in table '{{_gas_leave_holidays}}':
 * @property string $id
 * @property string $name
 * @property string $date 
 */
class GasLeaveHolidays extends BaseSpj
{
    const TAB_GENERAL = 1;
    const TAB_UI      = 2;
    public $date_from, $date_to, $date_create_from, $date_create_to, $year, $year_count = 3;
    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return GasLeaveHolidays the static model class
     */
    public static function model($className=__CLASS__)
    {
            return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
            return '{{_gas_leave_holidays}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        return array(
            array('name', 'length', 'max' => 255),
            array('name, date,type_id', 'required', 'on' => 'create, update'),
            array('date_from,date_to,description,id, name, date,year,type_id, compensatory_leave_date', 'safe'),
            array('date_create_from,date_create_to', 'safe'),
            array('name, type_id, date_create_from,date_create_to', 'required', 'on'=> 'createMultiHolidays'),
        );
        
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        return array(
            'rType' => array(self::BELONGS_TO, 'HrHolidayTypes', 'type_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'name' => 'Tên Ngày Lễ',
            'date' => 'Ngày Dương Lịch',
            'date_from' => 'Từ Ngày',
            'date_create_from' => 'Từ Ngày',
            'date_to' => 'Đến Ngày',
            'date_create_to' => 'Đến Ngày',
            'year' => 'Năm',
            'type_id' => 'Loại ngày nghỉ',
            'description' => 'Mô tả',
            'compensatory_leave_date' => 'Nghỉ bù của ngày',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        $criteria=new CDbCriteria;
        $criteria->compare('t.name', $this->name, true);
        $date_from = $date_to = '';
        if(!empty($this->date_from)){
            $date_from = MyFormat::dateDmyToYmdForAllIndexSearch($this->date_from);
            $criteria->addCondition("t.date >= '" . $date_from . "'");
        }
        if(!empty($this->date_to)){
            $date_to = MyFormat::dateDmyToYmdForAllIndexSearch($this->date_to);
            $criteria->addCondition("t.date <= '" . $date_to . "'");
        }
        $criteria->compare("t.type_id", $this->type_id);
        $criteria->compare("t.name", $this->name, true);
        
        $criteria->order = 't.id DESC';
        return new CActiveDataProvider($this, array(
                'criteria'=>$criteria,
                'pagination'=>array(
                    'pageSize'=> Yii::app()->user->getState('pageSize',Yii::app()->params['defaultPageSize']),
                ),
        ));
    }

    protected function beforeSave() {
        if(strpos($this->date, '/')){
            $this->date = MyFormat::dateConverDmyToYmd($this->date);
            MyFormat::isValidDate($this->date);
        }
        return parent::beforeSave();
    }
    
    /**
     * @Author: ANH DUNG Sep 27, 2014
     * @Todo: get number of days holidays
     * @Param: $date_from: 2014-09-27
     * @Param: $date_to: 2014-09-28
     * @return: number of day
     */
    public static function getNumberOfDayHolidays($date_from, $date_to) {
        $criteria = new CDbCriteria();
        $criteria->addBetweenCondition("t.date", $date_from, $date_to);
        return self::model()->count($criteria);
    }
    
    /** @Author: ANH DUNG Jan 01, 2018
     *  @Todo: check date is holidays OR not
     * @param: $date Y-m-d
     */
    public function isHoliday($date) {
        $criteria = new CDbCriteria();
        $criteria->compare('t.date', $date);
        return self::model()->count($criteria);
    }
    /** @Author: ANH DUNG Dec 27, 2018
     *  @Todo: check date is weeken holidays OR not
     *  @param: $date Y-m-d
     *  @note: Sunday is weeken 
     * @return: true if date is weeken, false if not
     */
    public function isWeeken($date) {
        $day_of_week    = strtolower(MyFormat::dateConverYmdToDmy($date, 'l'));
        $aWeeken        = ['sunday'];
        return in_array($day_of_week, $aWeeken);
    }
    
    /** @Author: HOANG NAM 30/03/2018
     *  @Todo: get calender
     *  @Param: 
     * */
    public function buildHtmlCalendar($year, $month,$link = true) {
        // CSS classes
        $fakePlan = new HrHolidayPlans();
        $leaveHolidaysPlan = HrHolidayPlans::model()->findByPk($year);
        $statusLeaveHolidaysPlan = !empty($leaveHolidaysPlan) ? $leaveHolidaysPlan->status : -1;
        $css_cal = 'month';
        $css_cal_row = 'calendar-row';
        $css_cal_day_head = 'calendar-day-head';
        $css_cal_day = 'calendar-day';
        $css_cal_day_number = 'day-number';
        $css_cal_day_blank = 'calendar-day-np';
        $css_cal_day_event = 'special-holiday'; //nghỉ lễ quốc gia
        $css_cal_day_addition = 'addition-holiday'; //nghỉ bù
        $css_cal_day_weekend = 'weekend'; //cuối tuần
        
        // Table headings
        $headings = ['T2', 'T3', 'T4', 'T5', 'T6', 'T7', 'CN'];
        // Start: draw table
        $calendar = "<table class='{$css_cal}'>" .
                "<tr>".
                "<td colspan='7'><h2>Tháng {$month}</h2></th>" .
                "</tr>".
                "<tr class='{$css_cal_row}'>" .
                "<td class='{$css_cal_day_head}'>" .
                implode("</td><td class='{$css_cal_day_head}'>", $headings) .
                "</td>" .
                "</tr>";
        // Days and weeks
        $running_day = date('N', mktime(0, 0, 0, $month, 1, $year));
        $days_in_month = date('t', mktime(0, 0, 0, $month, 1, $year));
        // Row for week one
        $calendar .= "<tr class='{$css_cal_row}'>";
        // Print "blank" days until the first of the current week
        for ($x = 1; $x < $running_day; $x++) {
            $calendar .= "<td class='{$css_cal_day_blank}'> </td>";
        }
        $aCssColorSlug = $this->getArrayHolidaySlug($year);

        // Keep going with days...
        for ($day = 1; $day <= $days_in_month; $day++) {
//            get event in day
            // Check if there is an event today
            $cur_date = date('Y-m-d', mktime(0, 0, 0, $month, $day, $year));
            $mLeaveHoliday = $this->getLeaveHolidayByDate($cur_date);
            $titleDate = !empty($mLeaveHoliday) ? $mLeaveHoliday->getName() . '-' . $mLeaveHoliday->getType() : '';
            $IdEventDate = !empty($mLeaveHoliday) ? $mLeaveHoliday->attributes['id'] : -1;
            // Day cell
//            highlight
            $cssHighlight = '';
//            set class event highlight
            if ($IdEventDate >= 0) {
                $cssHighlight = 'color_type_'.array_search($mLeaveHoliday->description, array_keys($aCssColorSlug));
            }
            $calendar .= "<td title = '{$titleDate}' class='{$cssHighlight} {$css_cal_day}'>";
            // Add the day number
            //event in here
            $urlEvent = '';
            if($link){
//                if ($statusLeaveHolidaysPlan != $fakePlan->STATUS_APPROVED) { AnhDung Dec2518
                if (1) {
                    if ($IdEventDate >= 0) {
                        $urlEvent = "<a href='" . Yii::app()->createAbsoluteUrl("admin/gasLeaveHolidays/update", array('id' => $IdEventDate, 'ajax'=>1)) . "' class='date'>{$day}</a> ";
                    } else {
                        $urlEvent = "<a href='" . Yii::app()->createAbsoluteUrl("admin/gasLeaveHolidays/create", array('date' => $cur_date)) . "' class='date'>{$day}</a> ";
                    }
                } else {
                    if ($IdEventDate >= 0) {
                        $urlEvent = "<a href='" . Yii::app()->createAbsoluteUrl("admin/gasLeaveHolidays/view", array('id' => $IdEventDate)) . "' class='date'>{$day}</a> ";
                    } else {
                        $urlEvent = "{$day}";
                    }
                }
            }else{
                $urlEvent = "{$day}";
            }
            $calendar .= "<div class='{$css_cal_day_number}'>" . $urlEvent . "</div>";
            // Close day cell
            $calendar .= "</td>";
            // New row
            if ($running_day == 7) {
                $calendar .= "</tr>";
                if (($day + 1) <= $days_in_month) {
                    $calendar .= "<tr class='{$css_cal_row}'>";
                }
                $running_day = 1;
            }
            // Increment the running day
            else {
                $running_day++;
            }
        } // for $day
        // Finish the rest of the days in the week
        if ($running_day != 1) {
            for ($x = $running_day; $x <= 7; $x++) {
                $calendar .= "<td class='{$css_cal_day_blank}'> </td>";
            }
        }
        // Final row
        $calendar .= "</tr>";
        // End the table
        $calendar .= '</table>';
        // All done, return result
        return $calendar;
    }

    public function buildHtmlFooterNoteEvent($year) {
        $aEvent = $this->getArrayHolidaySlug($year);
        $res = '<div class="row note-container">'
                .'<h3>Chú thích:</h3>';
        
        foreach ($aEvent as $key => $ev) {
            $res .= '<div class="note_cell_container"><div  class="color_type_'.array_search($key, array_keys($aEvent)).' note-cell"></div><label class="event-text">'.$ev.'</label></div>';
        }
        $res .= '</div>';
        return $res;
    }

    public function buildHtmlMonthCalendar($year, $aMonth = array(),$link = true,$countInLine= 4) {
        $result = $this->buildHtmlFooterNoteEvent($year);
        $result .= '<table >';
        $i = 0;
        foreach ($aMonth as $month) {
            $i++;
            if($i == 1) {
                $result .= '<tr>';
            }
            $result .= '<td>'.$this->buildHtmlCalendar($year, $month, $link).'</td>';
            if($i == $countInLine) {
                $result .= '</tr>';
                $i = 0;
            }
        }
        $result .= '</table>';
        return $result;
    }

    /** @Author: HOANG NAM 31/03/2018
     *  @Todo: get gasLeaveHoliday by date
     *  @Param: 
     * */
    public function getLeaveHolidayByDate($date) {
        $result = null;
        if (isset($date)) {
            $criteria = new CDbCriteria;
            $criteria->addCondition('t.date = "' . $date . '"');
            $criteria->limit = 1;
            $aModel = GasLeaveHolidays::model()->findAll($criteria);
            $result = isset($aModel[0]) ? $aModel[0] : null;
        }
        return $result;
    }

    /** @Author: HOANG NAM 02/04/2018
     *  @Todo: get leave holidays plan
     *  @Param: 
     * */
    public function getLeaveHolidaysPlan() {
        $idPlan = isset($this->year) ? $this->year : '';
        $mLeaveHolidaysPlan = HrHolidayPlans::model()->findByPk($idPlan);
        if (empty($mLeaveHolidaysPlan)) {
            $mLeaveHolidaysPlan = new HrHolidayPlans('create');
            $mLeaveHolidaysPlan->id = $this->year;
            $mLeaveHolidaysPlan->status = $mLeaveHolidaysPlan->STATUS_NEW;
        }
        return $mLeaveHolidaysPlan;
    }

    /** @Author: HOANG NAM 02/04/2018
     *  @Todo: get
     *  @Param: 
     * */
    public function getArrayYear() {
        $result = array();
        $startYear = $this->year - $this->year_count;
        $endYear = $this->year + $this->year_count;
        for ($startYear; $startYear <= $endYear; $startYear++) {
            $result[$startYear] = 'Năm ' . $startYear;
        }
        return $result;
    }
    
    public function getArrayType(){
        $criteria = new CDbCriteria;
        $criteria->compare('t.status', HrHolidayTypes::model()->status_active);
        $models = HrHolidayTypes::model()->findAll($criteria);
        $res    = [];
        foreach ($models as $value) {
            $res[$value->id] = $value->name;
        }
        return $res;
    }

    public function getName() {
        return $this->name;
    }

    public function getDate() {
        return MyFormat::dateConverYmdToDmy($this->date);
    }
    
    public function getCompensatoryDate() {
        return MyFormat::dateConverYmdToDmy($this->compensatory_leave_date);
    }

    public function getType() {
        return !empty($this->rType) ? $this->rType->getName() : '';
    }

    public function getDescription() {
        return $this->description;
    }

    /** @Author: HOANG NAM 02/04/2018
     *  @Todo: generate leave holiday
     *  @Param: 
     * */
    public function generateLeaveHolidays($aDate, $idType) {
        $aRowInsert = array();
        $des = 'Auto generate';
        $mLeaveHolidaysType = HrHolidayTypes::model()->findByPk($idType);
        foreach ($aDate as $key => $date) {
            if (!empty($this->getLeaveHolidayByDate($date))) {
                continue;
            }

            $aRowInsert[] = "('" . $date . "',
                '" . (!empty($mLeaveHolidaysType) ? $mLeaveHolidaysType->getName() : '') . "',
                '" . $idType . "',
                '" . $des . "'
                )";
            $mGasLeaveHolidays = new GasLeaveHolidays('create');
            $mGasLeaveHolidays->validate();
            if (!$mGasLeaveHolidays->hasErrors()) {
                $mGasLeaveHolidays->save();
            }
        }
        $tableName = GasLeaveHolidays::model()->tableName();
        ;
        $sql = "insert into $tableName (date,name,type_id,description) values ";
        $sql .= implode(',', $aRowInsert);
        if (count($aRowInsert) > 0) {
            Yii::app()->db->createCommand($sql)->execute();
        }
    }

    /** @Author: HOANG NAM 02/04/2018
     *  @Todo: GET ALL DAY IN YEAR BY FORMAT
     *  @Param: $aDay(format) $aDay('Sun','Mon','Tue','Wed','Thu','Fri','Sat')
     * */
    public function getAllDay($year, $aDay = array()) {
        $result = array();
        $begin = new DateTime("{$year}-01-01");
        $end = new DateTime("{$year}-12-31");
        $interval = DateInterval::createFromDateString('1 day');
        $period = new DatePeriod($begin, $interval, $end);
        foreach ($period as $dt) {
            if (in_array($dt->format("D"), $aDay)) {
                $result[] = $dt->format('Y-m-d');
            }
        }
        return $result;
    }

    /** @Author: HOANG NAM 16/04/2018
     *  @Todo: export to pdf
     *  @Param: 
     * */
    public function toLeaveHolidaysPdf($year) {
        $this->exportPdf($year);
    }
    
    public function exportPdf($year) {
        try{
        
        $aErrors=[];
        $this->setPdfName($year);
        if( ( $this->pdfOutput == 'F' && !file_exists($this->pdfPathAndName)) || 
                $this->pdfOutput == 'I'
            ){
            $this->toPdf($year);
        }
        }catch (Exception $exc){
            throw new Exception($exc->getMessage());
        }
    }
    
    public function getPathSaveFile() {
        $pathUpload = 'upload/pdf/GasLeaveHolidays';
        $root = Yii::getPathOfAlias('webroot');
        $currentPath = $root . '/' . $pathUpload;
        if (!is_dir($currentPath)){
            $imageProcessing = new ImageProcessing();// check thư mục trước khi tạo
            $imageProcessing->createDirectoryByPath($pathUpload);
            chmod($currentPath, 0777);
        }
        return $pathUpload;
    }
    
    public function setPdfName($year) {
        $this->pdfPathAndName = Yii::getPathOfAlias('webroot') . '/' . $this->getPathSaveFile().'/'.$year.'.pdf';
    }
    
    public function getPdfContentQuotes($year) {
        $resultHtml = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
                        <html xmlns="http://www.w3.org/1999/xhtml">
                        <head>
                        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
                        <title>'.$year.'</title>
                        <link href="'. Yii::app()->theme->baseUrl .'/admin/css/calendar.css" type="text/css" rel="stylesheet">
                        <style rel="stylesheet" type="text/css" media="print">
                            a{text-decoration: none; color: black;}
                            .calendar-day-head{
                                font-weight: bold;
                            }
                            td{
                                width: 35px;
                                height: 35px;
                                text-align: center;
                                vertical-align: top;
                            }
                            .weekend{background: #d6d6d6;}
                            .special-holiday{background: #93ce91; color: white}
                            .addition-holiday{background: #41afde;}
                            .note-container{border-left: 4px solid black; padding: 0 0 5px 10px; margin-left: 35px;}
                            .note-cell{
                                width:25px;
                                height: 25px;
                                display: inline-block;
                                margin: 10px 5px 0 0; 
                                position: relative; 
                                top: 6px;
                            }
                        </style> 
                        </head>
                        <body style="font-family: Arial;">';
        
        $resultHtml .=  $this->buildHtmlMonthCalendar($year,array(1,2,3,4,5,6,7,8,9,10,11,12),false,6);
        $resultHtml .=  '</body>
                        </html>';
        return $resultHtml;
    }
    
    //create slug from description gasleaveholidays
    public function slugify($str) {
        $str = preg_replace("/(à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ)/", 'a', $str);
            $str = preg_replace("/(è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ)/", 'e', $str);
            $str = preg_replace("/(ì|í|ị|ỉ|ĩ)/", 'i', $str);
            $str = preg_replace("/(ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ)/", 'o', $str);
            $str = preg_replace("/(ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ)/", 'u', $str);
            $str = preg_replace("/(ỳ|ý|ỵ|ỷ|ỹ)/", 'y', $str);
            $str = preg_replace("/(đ)/", 'd', $str);
            $str = preg_replace("/(À|Á|Ạ|Ả|Ã|Â|Ầ|Ấ|Ậ|Ẩ|Ẫ|Ă|Ằ|Ắ|Ặ|Ẳ|Ẵ)/", 'A', $str);
            $str = preg_replace("/(È|É|Ẹ|Ẻ|Ẽ|Ê|Ề|Ế|Ệ|Ể|Ễ)/", 'E', $str);
            $str = preg_replace("/(Ì|Í|Ị|Ỉ|Ĩ)/", 'I', $str);
            $str = preg_replace("/(Ò|Ó|Ọ|Ỏ|Õ|Ô|Ồ|Ố|Ộ|Ổ|Ỗ|Ơ|Ờ|Ớ|Ợ|Ở|Ỡ)/", 'O', $str);
            $str = preg_replace("/(Ù|Ú|Ụ|Ủ|Ũ|Ư|Ừ|Ứ|Ự|Ử|Ữ)/", 'U', $str);
            $str = preg_replace("/(Ỳ|Ý|Ỵ|Ỷ|Ỹ)/", 'Y', $str);
            $str = preg_replace("/(Đ)/", 'D', $str);
            $str = str_replace(" ", "-", str_replace("&*#39;","",$str));
            $str = strtolower($str);
        return $str;
    }
    
    public function getArraySlug() {
        $criteria = new CDbCriteria;

        $criteria->select = 'DISTINCT description';
        $aModelSlug = $this->findAll($criteria);
        $aSlug = array();
        foreach ($aModelSlug as $value) {
            $aSlug[] = $value->description;
        }
        return $aSlug;
    }
    
    public function getArrayHolidaySlug($year) {
        $criteria = new CDbCriteria;

        $criteria->select = 'DISTINCT description, name';
        $criteria->addCondition('YEAR(date) = '.$year);
        $aModelSlug = $this->findAll($criteria);
        $aSlug = array();
        foreach ($aModelSlug as $value) {
            $aSlug[$value->description] = $value->name;
        }
        return $aSlug;
    }
    
    /**
     * Get factor value of holiday date
     * @return Factor value, 1 otherwise
     */
    public function getFactorValue() {
        $retVal = 1;
        if (isset($this->rType)) {
            $retVal = $this->rType->getFactor();
        }
        return $retVal;
    }
    
    //-----------------------------------------------------
    // Static methods
    //-----------------------------------------------------
    /**
     * Check date value is off date
     * @param String $date Date value with format [yyyy-mm-dd]
     */
    public static function isOffDate($date) {
        $retVal = false;
        // Check date is exist in db
        
        $criteria = new CDbCriteria();
        $criteria->compare("t.date", $date);
        $models = self::model()->findAll($criteria);
        if (count($models) > 0) {
            $retVal = true;
        }
        
        return $retVal;
    }
    
    /**
     * Get model by date
     * @param String $date Date value
     * @return Model object if found, null otherwise
     */
    public static function getLeaveHolidayModelByDate($date) {
        $result = null;
        if (isset($date)) {
            $criteria = new CDbCriteria;
            $criteria->addCondition('t.date = "' . $date . '"');
            $criteria->limit = 1;
            $model = GasLeaveHolidays::model()->find($criteria);
//            $result = isset($aModel[0]) ? $aModel[0] : null;
            $result = $model;
        }
        return $result;
    }
    
    /** @Author: ANH DUNG Jul 31, 2018
     *  @Todo: get số ngày nghỉ lễ trong 1 năm
     *  @param: $this->date format Y-m-d
     **/
    public function getHolidaysInYear() {
        $temp = explode('-', $this->date);
        $criteria = new CDbCriteria();
        $criteria->addCondition('YEAR(t.date) = ' . $temp[0]);
        $models = GasLeaveHolidays::model()->findAll($criteria);
        $aRes = [];
        foreach($models as $item){
            // Check if plan was approved
//            $plan = HrHolidayPlans::model()->findByPk($temp[0]);
//            if ($plan && $plan->isApproved()) {
//                $aRes[$item->date] = $item->date;
//            }
            $aRes[$item->date] = $item->date;
        }
        return $aRes;
    }
    
    public function getModelHolidaysInYear() {
        $temp = explode('-', $this->date);
        $criteria = new CDbCriteria();
        $criteria->addCondition('YEAR(t.date) = ' . $temp[0]);
        $models = GasLeaveHolidays::model()->findAll($criteria);
        $aRes = [];
        foreach($models as $item){
            $aRes[$item->date] = $item;
        }
        return $aRes;
    }
    
}