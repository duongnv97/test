<?php

/**
 * This is the model class for table "{{_users_phone_log}}".
 *
 * The followings are the available columns in table '{{_users_phone_log}}':
 * @property string $id
 * @property string $user_id
 * @property string $phone
 * @property integer $role_id
 * @property integer $is_maintain
 * @property string $created_date
 */
class UsersPhoneLog extends BaseSpj
{
    const TYPE_BO_MOI       = 1;
    const TYPE_UPDATE_SOON  = 2;// chưa có - cập nhật sau
    
    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return UsersPhoneLog the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{_users_phone_log}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        return array(
            array('id, user_id, phone, role_id, is_maintain, created_date', 'safe'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        return array(
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
            return array(
                'id' => 'ID',
                'user_id' => 'User',
                'phone' => 'Phone',
                'role_id' => 'Role',
                'is_maintain' => 'Is Maintain',
                'created_date' => 'Created Date',
            );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        $criteria=new CDbCriteria;
        $criteria->compare('t.id',$this->id);
        $criteria->order = 't.id DESC';
        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
            'pagination'=>array(
                'pageSize'=> Yii::app()->user->getState('pageSize',Yii::app()->params['defaultPageSize']),
            ),
        ));
    }
    
    /**
     * @Author: ANH DUNG Oct 18, 2017
     * @Todo: add new record record daily
     */
    public function doAddNew($phone) {
        $this->phone = $phone;
        $this->save();
    }
    
    /**
     * @Author: ANH DUNG Oct 19, 2017
     * @Todo: cập nhật thông tin user cho số phone gọi đến trong ngày
     */
    public static function cronUpdatePhoneUserInfo() {
        $from = time();
        $today = date('Y-m-d');
        $criteria = new CDbCriteria();
        $criteria->addCondition("DATE(t.created_date) = '$today'");
        $models = self::model()->findAll($criteria);
        foreach( $models as $item):
            $criteria = new CDbCriteria();
            $criteria->compare('t.phone', $item->phone);
            $mUserPhone = UsersPhone::model()->findAll($criteria);
            foreach($mUserPhone as $itemPhone):
                $item->user_id      = $itemPhone->user_id;
                $item->role_id      = $itemPhone->role_id;
                $item->is_maintain  = $itemPhone->is_maintain;
                $item->count_customer = count($mUserPhone);
                $item->update();
                break;
            endforeach;
        endforeach;
        
        $to = time();
        $second = $to-$from;
        $info = count($models). " items cronUpdatePhoneUserInfo done in: $second  Second  <=> ".($second/60)." Minutes. item => ";
        Logger::WriteLog($info);
        $models = null;
    }
    
}