<?php

/**
 * This is the model class for table "{{_follow_customer}}".
 *
 * The followings are the available columns in table '{{_follow_customer}}':
 * @property integer $id
 * @property integer $customer_id
 * @property integer $employee_id
 * @property string $appointment_date
 * @property string $created_date
 * @property string $created_date_only
 * @property string $note
 */
class FollowCustomer extends BaseSpj
{
    public $send_sms = 0, $employee_name = '', $employee_ext='', $autocomplete_customer = '', $autocomplete_employee = '';
    public $date_from, $date_to, $number_customer, $date_appointment_from, $date_appointment_to, $is_buy_complete, $search_second_from, $search_second_to;
    public $JSON_FIELD      = array('employee_name', 'employee_ext');


    const STT_BUY_COMPLETE      = 1;
    const STT_BUY_UNCOMPLETE    = 2;
    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return FollowCustomer the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{_follow_customer}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        return array(
            array('appointment_date, status', 'required'),
            array('customer_id, employee_id, appointment_date, created_date, note, status, status_cancel', 'safe'),
            array('send_sms, json, employee_name, employee_ext, date_from, date_to, date_appointment_from, date_appointment_to, is_buy_complete, search_second_from, search_second_to', 'safe'),
            array('bill_duration', 'numerical', 'integerOnly' => true),
            );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        return array(
            'rCustomer' => array(self::BELONGS_TO,'Users','customer_id'),
            'rEmployee' => array(self::BELONGS_TO,'Users','employee_id'),
            'rUsersPhone' => array(self::BELONGS_TO, 'UsersPhone','', 'foreignKey' => array('customer_id'=>'user_id'))
            
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'customer_id' => 'Khách hàng',
            'employee_id' => 'Nhân viên',
            'appointment_date' => 'Ngày hẹn',
            'created_date' => 'Ngày giờ tạo',
            'created_date_only' => 'Ngày tạo',
            'note' => 'Ghi chú',
            'date_appointment_from'=>'Ngày gọi từ',
            'date_appointment_to'=>'đến',
            'date_from'=>'Ngày tạo từ',
            'date_to'=>'đến',
            'is_buy_complete' => 'Trạng thái đơn hàng',
            'status' => 'Trạng thái',
            'status_cancel' => 'Lý do từ chối',
            'send_sms' => 'Gửi sms',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        $criteria=new CDbCriteria;

        $criteria->compare('t.customer_id',$this->customer_id);
        $criteria->compare('t.employee_id',$this->employee_id);
        $criteria->compare('t.status',$this->status);
        $criteria->compare('t.status_cancel',$this->status_cancel);
        //$criteria->compare('t.appointment_date',$this->appointment_date);
        $dateAppointmentFrom = MyFormat::dateConverDmyToYmd($this->date_appointment_from,'-');
        $dateAppointmentTo = MyFormat::dateConverDmyToYmd($this->date_appointment_to,'-');

        if(!empty($dateAppointmentFrom)){
            $criteria->addCondition('t.created_date_only >= "'.$dateAppointmentFrom.'"');
        }
        if(!empty($dateAppointmentTo)){
            $criteria->addCondition('t.created_date_only <= "'.$dateAppointmentTo.'"');
        }
        if(($this->search_second_from != '')){
            $criteria->addCondition('t.bill_duration >= "'.$this->search_second_from.'"');
        }
        if($this->search_second_to != ''){
            $criteria->addCondition('t.bill_duration <= "'.$this->search_second_to.'"');
        }
        $criteria->order = 't.id DESC';
        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
            'pagination'=>array(
                'pageSize'=> Yii::app()->user->getState('pageSize',Yii::app()->params['defaultPageSize']),
            ),
        ));
    }
    public function getCreatedDateOnly(){
        return MyFormat::dateConverYmdToDmy($this->created_date_only,'d/m/Y');
    }
    public function getAppointmentDate(){
        return MyFormat::dateConverYmdToDmy($this->appointment_date,'d/m/Y');
    }
    public function getEmployeeInfo($field_name=''){
        $mUser = $this->rEmployee;
        if($mUser){
            if($field_name != ''){
                return $mUser->$field_name;
            }
            return $mUser->first_name;
        }
        return '';
    }
    public function getCustomerInfo($field_name=''){
        $mUser = $this->rCustomer;
        if($mUser){
            if($field_name != ''){
                return $mUser->$field_name;
            }
            return $mUser->first_name;
        }
        return '';
    }
    public function getTypeCustomer(){
        $mUser = $this->rCustomer;
        if($mUser){
            return $mUser->getTypeCustomerText();
        }
        return '';
    }
    public function getAgentOfCustomer(){
        $mUser = $this->rCustomer;
        if($mUser){
            return $mUser->getAgentOfCustomer();
        }
        return '';
    }
    public function getNote(){
        return nl2br($this->note);
    }
    
    public function getCallDuration() {
        return $this->bill_duration;
    }
    public function getCallLinkView() {
        $mSell = new Sell();
        $mSell->call_id = $this->call_id;
        return $mSell->getUrlViewCall();
    }
    
    public function getCallViewOnGrid() {
        if(empty($this->bill_duration)){
            return '';
        }
        $res = $this->getCallDuration()."<br>{$this->getCallLinkView()}";
        return $res;
    }
    

    protected function beforeValidate() {
        if($this->isNewRecord){
            $cUid = MyFormat::getCurrentUid();
            $this->employee_id = $cUid;
            $mCustomer = $this->rCustomer;
            if($mCustomer){
                $this->agent_id = $mCustomer->area_code_id;
            }
        }
        return parent::beforeValidate();
    }
    
    public function beforeSave() {
        if($this->isNewRecord){
            $this->created_date         = date('Y-m-d H:i:s');
            $this->created_date_only    = date('Y-m-d');
        }
        if(strpos($this->appointment_date, '/')){
            $this->appointment_date = MyFormat::dateConverDmyToYmd($this->appointment_date);
        }

//        if(!empty($this->customer_id)){// Ad Close May0518
//            $user = Users::model()->findByPk($this->customer_id);
//            $this->employee_id = $user->sale_id;
//        }
        return parent::beforeSave();
    }
    public function getListStatisticsEmployee()
    {
        $criteria=new CDbCriteria;

        $criteria->compare('t.employee_id',$this->employee_id);
        //$criteria->compare('t.appointment_date',$this->appointment_date);
        $criteria->select = "t.employee_id, t.created_date_only, COUNT(t.customer_id) as number_customer";
        if(!empty($this->date_from)){
            $dateFrom = MyFormat::dateConverDmyToYmd($this->date_from,'-');
            $criteria->addCondition("t.created_date_only >= '". $dateFrom."'");
        }
        if(!empty($this->date_to)){
            $dateTo   = MyFormat::dateConverDmyToYmd($this->date_to,'-');
            $criteria->addCondition("t.created_date_only <= '". $dateTo."'");
        }

        if(!empty($this->is_buy_complete)){
            if($this->is_buy_complete == self::STT_BUY_COMPLETE){
                $criteria->addCondition('t.sell_id > 0');
            } 
            if($this->is_buy_complete == self::STT_BUY_UNCOMPLETE){
                $criteria->addCondition('t.sell_id <= 0');
            }
        }
        $criteria->order = 't.id DESC';
        $criteria->group = 't.employee_id';
        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
            'pagination'=>array(
                'pageSize'=> Yii::app()->user->getState('pageSize',Yii::app()->params['defaultPageSize']),
            ),
        ));
    }

    /** @Author: HOANG NAM 16/04/2018
     *  @Todo: func update telesale
     **/
    public function updateBySell($mSell) {
        $criteria = new CDbCriteria;
        $criteria->addCondition('t.customer_id ='.$mSell->customer_id);
        $criteria->order = "t.id DESC";
        $criteria->limit = 1;
        $mFollowCustomer = FollowCustomer::model()->find($criteria);
        if(!empty($mFollowCustomer)){
            if($mFollowCustomer->sell_id < 1){
                $mFollowCustomer->sell_id = $mSell->id;
                $mFollowCustomer->sell_created_date = $mSell->created_date_only;
                $mFollowCustomer->update();
            }
        }
        $mCustomerDraft = new CustomerDraft();
        $mCustomerDraft->updateSellDate($mSell);
    }
    
    /** @Author: DUONG date/month/2018
     *  @Todo:   get array status for dropdown search
     **/
    public function getArrayStatusBuy(){
        return array(
            self::STT_BUY_COMPLETE      => 'Đã lấy hàng',
            self::STT_BUY_UNCOMPLETE    => 'Chưa lấy hàng'
        );
    }
    public function getArrayEmployee(){
//        return Users::model()->getArrDropdown(ROLE_CALL_CENTER);// AnhDung Close May0618
        $mAppPromotionUser          = new AppPromotionUser();
        return $mAppPromotionUser->getListMarketing();
    }
    public function getArrayCallStatus(){
        $model = new Telesale();
        return $model->getArrayCallStatus();
    }
    public function getArrayStatusCancel(){
        return Telesale::getArrayStatusCancel();
    }
    public function getStatus() {
        return isset($this->getArrayCallStatus()[$this->status]) ? $this->getArrayCallStatus()[$this->status] : '';
    }
    
    /** @Author: ANH DUNG May 05, 2018
     *  @Todo: get history Telesale call out to customer
     **/
    public function getCallOutHtml() {
        $html = '';
        $models = $this->getCallOutByDate();
        if(count($models) < 1){
            return '';
        }
        $html .= '<table class="call-tbl-list">';
            $html .= '<tr>';
                $html .= '<th class="item_c" colspan="2">Lịch sử Telesale gọi ra cho KH</th>';
            $html .= '</tr>';
        foreach($models as $key => $model){
            $model->mapJsonFieldOneDecode('JSON_FIELD', 'json', 'baseArrayJsonDecode');
            $sExt = "<span class='f_size_18'>EXT  $model->employee_ext</span>";
            $html .= '<tr style="">';
                $html .= '<td>'.MyFormat::dateConverYmdToDmy($model->created_date, 'd/m/Y H:i').'</td>';
                $html .= '<td>'."<b>$sExt</b> - Telesale: $model->employee_name<br>$model->note</td>";
            $html .= '</tr>';
        }
        $html .= '</table>';
        return $html;
    }
    
    /** @Author: ANH DUNG Jul 06, 2017
     * @Todo: get list cuộc gọi đến theo số đt gọi trong ngày
     */
    public function getCallOutByDate() {
        $criteria = new CDbCriteria();
        $criteria->addCondition('t.customer_id='.$this->customer_id);
//        $criteria->addCondition("t.created_date_only='$this->created_date_only'");// không cần limit date, lấy 3 record gần nhất
        $criteria->order = 't.id DESC';
        $criteria->limit = 2;
        return self::model()->findAll($criteria);
    }
    
    /**
     * @Author: ANH DUNG May 06, 2018
     * @Todo: lấy row Telesale mới nhất của khách hàng
     */
    public function getLastestRow(){
        $criteria = new CDbCriteria;
        $criteria->compare('t.customer_id', $this->customer_id);
        $criteria->order = "t.id DESC";
        $criteria->limit = 1;
        return self::model()->find($criteria);
    }
    
    /** @Author: ANH DUNG Aug 03, 2018
     *  @Todo: get row callout của khách hàng trên 30s
     * $this->date_from format Y-m-d
     */
    public function getCalloutValid(){
        $criteria = new CDbCriteria;
        $criteria->compare('t.customer_id', $this->customer_id);
        $criteria->compare('t.employee_id', $this->employee_id);
        $criteria->addCondition('t.bill_duration > ' . Telesale::MIN_TIME_OF_CALL);
        if(!empty($this->date_from)){
            $criteria->addCondition("t.created_date_only >= '". $this->date_from."'");
        }
        $criteria->limit = 1;
        return self::model()->find($criteria);
    }
    
    /** @Author: ANH DUNG May 05, 2018
     *  @Todo: set some data json
     **/
    public function setDataJson() {
        $this->employee_name    = $this->getEmployeeInfo('first_name');
        $this->employee_ext     = CacheSession::getCookie(CacheSession::CURRENT_USER_EXT);
        $this->setJsonDataField();
    }
    
    /** @Author: ANH DUNG May 06, 2018
     *  @Todo: handle send sms 
     **/
    public function handleSendSms() {
        $this->sms_phone    = $this->send_sms > 0 ? $this->send_sms : '';
        if($this->send_sms < 1){
            return ;
        }
        $this->send_sms = UsersPhone::formatPhoneSaveAndSearch($this->send_sms);
        if(!GasScheduleSms::isValidPhone($this->send_sms)){
            throw new Exception('Số điện thoại không phải là số di động hợp lệ');
        }
        $this->doSendSms();
    }
    public function doSendSms() {
        $cUid = MyFormat::getCurrentUid();
        $mAppPromotion = new AppPromotion();
        $mAppPromotion->owner_id = $cUid;

        $mSms = new GasScheduleSms();
        $mSms->phone = $this->send_sms;
        
        $mSms->title = "Ma CODE: " . $mAppPromotion->getOnlyCodeByUser();
        $mSms->title .= "\nTai app Android: http://bit.ly/AndroidGas24h va IOS: http://bit.ly/iOSGas24h";
        $mSms->title .= "\nWeb: http://gas24h.com.vn";
        $mSms->title .= "\nChi tiet lien he: 19001565";
        $mSms->purgeMessage();
        
        $mSms->uid_login        = $cUid;
        $mSms->user_id          = $this->customer_id;
        $mSms->type             = GasScheduleSms::TYPE_TELESALE_CODE;
        $mSms->obj_id           = $this->customer_id;
        $mSms->content_type     = GasScheduleSms::ContentTypeKhongDau;
        $mSms->time_send        = '';
        $mSms->json_var         = [];
        $schedule_sms_id        = $mSms->fixInsertRecord();

        $mScheduleSms = GasScheduleSms::model()->findByPk($schedule_sms_id);
        $mScheduleSms->doSend();
        $this->sms_id = $mScheduleSms->id;
    }
    
    
    /** @Author: DUONG 6/5/2018
     *  @Todo:   view note
     *  @Code:   DUONG001
     **/
   public function getNoteById($id) {
        $mFollowCtm = new FollowCustomer();
        $criteria = new CDbCriteria;
        $criteria->addCondition("t.customer_id=".$id);
        $criteria->order = 't.id DESC';
        return new CActiveDataProvider($mFollowCtm, array(
            'criteria'=>$criteria,
            'pagination'=>array(
                'pageSize'=> Yii::app()->user->getState('pageSize',Yii::app()->params['defaultPageSize']),
            ),
        ));
   }
   
   public function getStatusCancel() {
       if($this->status_cancel > 0){
           $aSttCancel = $this->getArrayStatusCancel();
           return $aSttCancel[$this->status_cancel];
       }
       return '';
   }
   
   public function canUpdateNote() {
        $cRole      = MyFormat::getCurrentRoleId();
        $cUid       = MyFormat::getCurrentUid();
        
        if($cRole == ROLE_ADMIN) return true; 
        
        if($cUid == $this->employee_id){
            return true;
        }
        return false;
    }
    // check user can update r
    public function canUpdateNoteInActionCreate() {
        $cRole      = MyFormat::getCurrentRoleId();
        $cUid       = MyFormat::getCurrentUid();
        $mCustomer = Telesale::model()->findByPk($this->customer_id);
        if($cUid == ROLE_ADMIN) return true;
        if($cUid == $mCustomer->sale_id || $cUid == $mCustomer->storehouse_id){
            return true;
        }
        return false;
    }
    
    /** @Author: ANH DUNG Jun 27, 2018
     *  @Todo: update call id and time call after call out
     **/
    public function setCallId() {
        $mCustomer = Telesale::model()->findByPk($this->customer_id);
        if(empty($mCustomer->phone)){
            return ;
        }
        $temp       = explode('-', $mCustomer->phone);
        $aPhone     = [];
        foreach($temp as $item){
            $aPhone[] = $item * 1;
        }
        $mCall = new Call();
        $mCall->sourceFrom      = Call::SOURCE_CALL_TELESALE;
        $mCall->direction       = Call::DIRECTION_OUTBOUND;
        $mCall->date_from_ymd   = date('Y-m-d');
        $mCall->date_to_ymd     = $mCall->date_from_ymd;
        $aCallOut = $mCall->getByListPhoneNumber($aPhone);
        $maxTime = 0;
        foreach($aCallOut as $item){
            if($item->bill_duration > $maxTime){
                $maxTime        = $item->bill_duration;
                $this->call_id  = $item->id;
            }
        }
        $this->bill_duration    = $maxTime;
        if(!empty($this->call_id)){// Oct3018 fix tính duplicate cuộc gọi KH EXcel khi tạo thêm ghi chú bên KH telesale
            $mCustomerDraftDetail = new CustomerDraftDetail();
            $mCustomerDraftDetail->call_id = $this->call_id;
            $recordDraftCall = $mCustomerDraftDetail->getByCallId();
            if(!is_null($recordDraftCall)){// nếu có tạo cuộc gọi KH excel rồi thì ko tính cuộc gọi bên KH telesale 
                $recordDraftCall->call_id = $recordDraftCall->bill_duration = 0;
                $recordDraftCall->update(['call_id', 'bill_duration']);
            }
        }
    }
    /** @Author: Pham Thanh Nghia Aug 13, 2018
     *  @Todo: Xét quyền cho phép sửa giây gọi bên view
     **/
    public function canEditCall(){
//        return false;
        $cUid   = MyFormat::getCurrentUid();
        $aAllow = [GasConst::UID_VEN_NTB, GasConst::UID_ADMIN];
        if(!in_array($cUid, $aAllow) || ($this->bill_duration > 0) ){
            return false;
        }
        return true;
    }
    public function getPhoneClickCall() {
        $mCallClick = new Call();
        return $mCallClick->buildUrlMakeCall($this->rCustomer->phone);
    }
    
    /** @Author: NamNH Jan 11, 2019
     *  @Todo: remove one many big
     **/
    public function removeOneManyBig(){
        if(empty($this->customer_id)){
            return ;
        }
        $criteria=new CDbCriteria;
        $criteria->compare('many_id', $this->customer_id);
        $criteria->compare('type',GasOneManyBig::TYPE_TELESALE);
        GasOneManyBig::model()->deleteAll($criteria);
    }
}