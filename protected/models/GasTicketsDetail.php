<?php

/**
 * This is the model class for table "{{_gas_tickets_detail}}".
 *
 * The followings are the available columns in table '{{_gas_tickets_detail}}':
 * @property string $id
 * @property string $ticket_id
 * @property string $message
 * @property string $uid_post
 * @property integer $type
 * @property string $created_date
 */
class GasTicketsDetail extends BaseSpj
{
    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return GasTicketsDetail the static model class
     */
    public $code_no,$uid_login,$autocomplete_name,$autocomplete_name_1,$send_to_id, $date_from, $date_to;
    public $list_id_image,$file_name;

    public static function model($className=__CLASS__)
    {
            return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
            return '{{_gas_tickets_detail}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        return array(
            array('file_name,list_id_image,id, ticket_id, message, uid_post, type, created_date,date_from, date_to,code_no', 'safe'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        return array(
            'rUidPost' => array(self::BELONGS_TO, 'Users', 'uid_post'),
            'rFile' => array(self::HAS_MANY, 'GasFile', 'belong_id',
                'on' => 'rFile.type=' . GasFile::TYPE_13_TICKET,
                'order' => 'rFile.id ASC',
            ),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'ticket_id' => 'Ticket',
            'message' => 'Message',
            'uid_post' => 'Uid Post',
            'type' => 'Type',
            'created_date' => 'Created Date',
            'date_from' =>'Từ ngày', 
            'date_to'=> 'Đến ngày',
            'code_no' => 'code',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function searchDefault()
    {
        
        $criteria=new CDbCriteria;
        $criteria->compare('t.id',$this->id,true);
        $criteria->compare('t.ticket_id',$this->ticket_id,true);
        $criteria->compare('t.message',$this->message,true);
        $criteria->compare('t.uid_post',$this->uid_post,true);
        $criteria->compare('t.type',$this->type);
        $criteria->compare('t.created_date',$this->created_date,true);

        return new CActiveDataProvider($this, array(
                'criteria'=>$criteria,
                'pagination'=>array(
                    'pageSize'=> Yii::app()->user->getState('pageSize',Yii::app()->params['defaultPageSize']),
                ),
        ));
    }

    /**
     * @Author: ANH DUNG Aug 09, 2014
     * @Todo: get detail ticket by ticket_id
     * @Param: $ticket_id
     */       
    public static function getByTicketId($ticket_id){
        $criteria = new CDbCriteria();
        $criteria->compare('t.ticket_id', $ticket_id);
        $criteria->order = 't.id DESC';
        return self::model()->findAll($criteria);
    }
    public function getByTicketIdV1($needMore = []){
        if(empty($this->ticket_id)){
            return [];
        }
        $criteria = new CDbCriteria();
        $criteria->addCondition('t.ticket_id = ' . $this->ticket_id);
        $criteria->order = 't.id DESC';
        if(isset($needMore['order'])){
            $criteria->order = $needMore['order'];
        }
        return self::model()->findAll($criteria);
    }
    
    /**
     * @Author: ANH DUNG Aug 09, 2014
     * @Todo: delete detail ticket by ticket_id
     * @Param: $ticket_id
     */       
    public static function deleteByTicketId($ticket_id){
        $criteria = new CDbCriteria();
        $criteria->compare('t.ticket_id', $ticket_id);
        $models = self::model()->findAll($criteria);
        if(count($models)){
            foreach($models as $item){
                $item->delete();
            }
        }
    }
    
    /**
     * @Author: ANH DUNG May 18, 2017
     * @Todo: get info phone + số đt agent
     */
    public function getInfoUser() {
        $res = '';
        $mUser = $this->rUidPost;
        $mAppCache = new AppCache();
        if($mUser){
            if(!empty($mUser->phone)){
                $aPhone = explode('-', $mUser->phone);
                $res .= '<br>ĐT: ';
                foreach($aPhone as $phone){
                    $res .=  UsersPhone::formatPhoneView($phone) . '<br>';
                }
            }
            $aRoleAllow = [ROLE_EMPLOYEE_MAINTAIN];
            $aAgent     = Users::GetKeyInfo($mUser, UsersExtend::JSON_ARRAY_AGENT);
            if(in_array($mUser->role_id, $aRoleAllow) && is_array($aAgent)){
                $agent_id   = reset($aAgent);
                $aAgent     = $mAppCache->getAgentListdata();
                if(isset($aAgent[$agent_id])){
                    $res .= $aAgent[$agent_id];
                }
            }
        }
        return $res;
    }
    
    /** @Author: PHAM THANH NGHIA 2018
     *  @Todo: search  gasTickets for list
     *  @Code: NGHIA001
     *  @Param: 
     *  
     **/
    
    public function getTickets() {
        return GasTickets::model()->findByPk($this->ticket_id);
    }
    public function getMessage(){
        
            $message =   " ( ". Users::model()->findByPk($this->uid_post)->first_name ." - ".$this->getCreatedDate(). " )" . $this->message . "<br>";
            return $message;
    }
    
    public function canUpdateWeb(){ 
        return false;
    }
    
    public function search(){
        $criteria= new CDbCriteria;

        
        if(isset($_GET['GasTicketsDetail'])){
            $item = $_GET['GasTicketsDetail'];
            $gasTickets = $this->getGasTicketByCode($item['code_no']);
            if($gasTickets){
                $criteria->compare('t.ticket_id',$gasTickets->id);
            }
            
            if(!empty($item['uid_login']) && empty($item['send_to_id']) ){
               $criteria->compare('t.uid_post',$item['uid_login']);

            }
            if(!empty($item['send_to_id']) && empty($item['uid_login'])){
               $criteria->compare('t.uid_post',$item['send_to_id']);

            }
            if(!empty($item['uid_login']) && !empty($item['send_to_id'])){
                $criteria->addInCondition('t.uid_post',array($item['uid_login'],$item['send_to_id']));
            }
            if(!empty($item['date_from'])){
                $date_from = MyFormat::dateDmyToYmdForAllIndexSearch($item['date_from']);
                $criteria->addCondition("DATE(t.created_date) >= '$date_from'");
            }

            if(!empty($item['date_to'])){
                $date_to = MyFormat::dateDmyToYmdForAllIndexSearch($item['date_to']);
                $criteria->addCondition("DATE(t.created_date) <= '$date_to'");
            }
        }

        $criteria->order = 't.id DESC';
        
        return new CActiveDataProvider($this, array(
                'criteria'=>$criteria,
                'pagination'=>array(
                    'pageSize'=> Yii::app()->user->getState('pageSize',Yii::app()->params['defaultPageSize']),
                ),
        ));
    }
    
    public function getGasTicketByCode($code){
        if(empty($code)){
            return null;
        }
        $criteria= new CDbCriteria;
        $criteria->compare('t.code_no', $code);
        return GasTickets::model()->find($criteria);
    }
    
    public function hanleFileView(){
        return GasFile::apiGetFileUpload($this->rFile);;
    }
}