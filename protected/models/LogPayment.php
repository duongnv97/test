<?php

/**
 * This is the model class for table "{{_log_payment}}".
 *
 * The followings are the available columns in table '{{_log_payment}}':
 * @property string $id
 * @property string $process_time
 * @property string $ref_id
 * @property string $bank_ref_Id
 * @property string $amount
 * @property string $json
 * @property string $created_date_bigint
 */
class LogPayment extends BaseSpj
{
    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return LogPayment the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{_log_payment}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        return [
            array('process_time, ref_id, bank_ref_Id, amount', 'required'),
            array('process_time, created_date_bigint', 'length', 'max'=>20),
            array('ref_id, bank_ref_Id, amount', 'length', 'max'=>20),
            array('json', 'safe'),
            ['id, process_time, ref_id, bank_ref_Id, amount, json', 'safe'],
        ];
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        return [
        ];
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'process_time' => 'Process Time',
            'ref_id' => 'Ref',
            'bank_ref_Id' => 'Bank Ref',
            'amount' => 'Amount',
            'json' => 'Json',
            'created_date_bigint' => 'Created Date Bigint',
            'created_date' => 'created_date',
        ];
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        $criteria=new CDbCriteria;

	$criteria->compare('t.id',$this->id,true);
	$criteria->compare('t.process_time',$this->process_time,true);
	$criteria->compare('t.ref_id',$this->ref_id,true);
	$criteria->compare('t.bank_ref_Id',$this->bank_ref_Id,true);
	$criteria->compare('t.amount',$this->amount,true);
	$criteria->compare('t.json',$this->json,true);
	$criteria->compare('t.created_date_bigint',$this->created_date_bigint,true);
        $criteria->order = 't.id DESC';
        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
            'pagination'=>array(
                'pageSize'=> Yii::app()->user->getState('pageSize',Yii::app()->params['defaultPageSize']),
            ),
        ));
    }
    
    public function canUpdate(){
        return true;
    }
}