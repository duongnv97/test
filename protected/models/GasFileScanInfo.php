<?php

/**
 * This is the model class for table "{{_gas_file_scan_info}}".
 *
 * The followings are the available columns in table '{{_gas_file_scan_info}}':
 * @property string $id
 * @property string $file_scan_id
 * @property string $maintain_date
 * @property string $customer_name
 * @property string $customer_address
 * @property string $customer_phone
 * @property string $materials_name
 * @property string $seri
 * @property string $note
 */
class GasFileScanInfo extends CActiveRecord
{
    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return GasFileScanInfo the static model class
     */
    public static function model($className=__CLASS__)
    {
            return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
            return '{{_gas_file_scan_info}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        return array(
            array('id, file_scan_id, maintain_date, customer_name, customer_address, customer_phone, materials_name, seri, note', 'safe'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
            return array(
            );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
            return array(
                    'id' => 'ID',
                    'file_scan_id' => 'File Scan',
                    'maintain_date' => 'Maintain Date',
                    'customer_name' => 'Customer Name',
                    'customer_address' => 'Customer Address',
                    'customer_phone' => 'Customer Phone',
                    'materials_name' => 'Materials Name',
                    'seri' => 'Seri',
                    'note' => 'Note',
            );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
            $criteria=new CDbCriteria;
            $criteria->compare('t.id',$this->id,true);
            $criteria->compare('t.file_scan_id',$this->file_scan_id,true);
            $criteria->compare('t.maintain_date',$this->maintain_date,true);
            $criteria->compare('t.customer_name',$this->customer_name,true);
            $criteria->compare('t.customer_address',$this->customer_address,true);
            $criteria->compare('t.customer_phone',$this->customer_phone,true);
            $criteria->compare('t.materials_name',$this->materials_name,true);
            $criteria->compare('t.seri',$this->seri,true);
            $criteria->compare('t.note',$this->note,true);

            return new CActiveDataProvider($this, array(
                    'criteria'=>$criteria,
        'pagination'=>array(
            'pageSize'=> Yii::app()->user->getState('pageSize',Yii::app()->params['defaultPageSize']),
        ),
            ));
    }

    public function defaultScope()
    {
            return array(
                    //'condition'=>'',
            );
    }
    
    /**
     * @Author: ANH DUNG Jun 30, 2014
     * @Todo: save detail
     * @Param: $model model GasFileScan
     */
    public static function saveDetail($model){
        try {
            set_time_limit(7200);
            $aRowInsert=array();
            if(empty($_FILES['GasFileScan']['tmp_name']['file_excel'])) return;
            GasFileScanInfo::delete_by_file_scan_id($model->id);
            GasFileScan::importExcel($model);
            return ; // đã chuyển sang import excel, đoạn dưới ko dùng nữa
            foreach($model->aModelFileScanInfo as $key=>$mDetail){
                if(!empty($mDetail->customer_name)){
                    $aRowInsert[]="('$model->id',
                        '$model->maintain_date',
                        '$mDetail->customer_name',
                        '$mDetail->customer_address',
                        '$mDetail->customer_phone',
                        '$mDetail->materials_name',
                        '$mDetail->seri',
                        '$mDetail->note'
                        )";
                }
            }
            $tableName = GasFileScanInfo::model()->tableName();
            $sql = "insert into $tableName (file_scan_id,
                        maintain_date,
                        customer_name,
                        customer_address,
                        customer_phone,
                        materials_name,
                        seri,
                        note
                        ) values ".implode(',', $aRowInsert);
            if(count($aRowInsert)>0)
                Yii::app()->db->createCommand($sql)->execute();        
            
            
        } catch (Exception $exc) {
            throw new Exception("Có Lỗi Khi Upload File: ". $exc->getMessage());
        }        
    }
    
    // Jun 30, 2014
    public static function delete_by_file_scan_id($file_scan_id){
        $criteria = new CDbCriteria();
        $criteria->compare('file_scan_id', $file_scan_id);
        self::model()->deleteAll($criteria);        
    }    
    
}