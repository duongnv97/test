<?php

/**
 * This is the model class for table "{{_gas_one_many}}".
 *
 * The followings are the available columns in table '{{_gas_one_many}}':
 * @property string $date_apply
 * @property integer $one_id
 * @property integer $many_id
 * @property integer $type
 */
class GasOneMany extends CActiveRecord
{
    public $setupUrl='', $setupPlaceholder='', $setupLabel='', $autocomplete_name_1,$autocomplete_name; // Nghia Aug 9, 2018
    public $nameView = 'first_name',$enableFied = 0,$showTableInfoUrl = 1,$relationUrl = 'one',$setupUrlOne = '',$setupPlaceholderOne = '', $setupLabelOne = '';
    public $nameModelOne,$nameModelMany,$nameFieldOne,$nameFieldMany;
    //LOC003
    public $voThucTeNo, $han_muc_vo_from, $han_muc_vo_to;
    public $searchFrom, $searchTo; // DuongNV 5 Jun,19 Dùng chung để search from, to
    public $dateFrom, $dateTo; // DuongNV 5 Jun,19 Dùng chung để search from, to
    /* Sep 04, 2016 các define trước của Type ở ngoài config.local
     * sẽ dừng lại ở define('ONE_COMPANY_USER', 10);
     */
    const TYPE_CUSTOMER_SPECIAL         = 11;// Sep 04, 2016 type Lý do KH đặc biệt
    const TYPE_CUSTOMER_ACCOUNT_APP     = 12;// Jun 28, 2017 KH trong chuỗi của 1 KH để đặt app
    const TYPE_AGENT_OF_MONITOR         = 13;// Oct2517 những đại lý mà 1 NV giám sát
    const TYPE_AGENT_OF_TARGET          = 14;// Aug0818 Những đại lý setup target của 1 NV
    const TYPE_MONITOR_CCS              = 15;// Dec0418 đội CCS của 1 CV hoặc GS
    const TYPE_MONITOR_GDKV             = 16;// Dec0418 List chuyên viên của GĐKV
    const TYPE_CUSTOMER_ALLOW_DEBIT     = 17;// Dec1118 Listdata KH mối không bán nợ
    const TYPE_GDKV_PROVINE             = 18;// Dec0418 đội CCS của 1 CV hoặc GS
    //LOC003
    const TYPE_DEBIT_VO                 = 19;// công nợ vỏ
    const TYPE_AGENT_PRICE              = 20; // DuongNV 5 Jun,19 Tăng giảm giá đại lý
    const TYPE_AGENT_CITY               = 21;// Jul2619 namnh cài đặt các đại lý thuộc thành phố
    const TYPE_DEVICE_IMEI_TEST         = 22; // DuongNVSep2519 list imei test
    //
    //// DuongNV Aug1319 tab trong widget cập nhật điều xe(UpdateMaintainMemberWidget)
    const TAB_TYPE_DRIVER   = 1;
    const TAB_TYPE_TRUCK    = 2;
    const TAB_TYPE_PHU_XE   = 3;
    const TAB_TYPE_AGENT    = 4;
    
    //NamLA Sep2019 tab trong form create one_many ( nhập id hoặc autocomplete many_id)
    public $type_create_many; //Sep2019, type để tạo one-many ( nhập id hoặc autocomplete many_id)
    const TAB_ID            = 1;  // Nhập ID
    const TAB_AUTOCOMPLETE  = 2 ; // Nhập Autocomplete
//    
    // Sep 10, 2015, những user của đại lý để viết phép
    public static $ROLE_BELONG_AGENT = array(
        ROLE_EMPLOYEE_MAINTAIN, // NV Bảo trì
        ROLE_PHU_XE, // Phụ Xe
        ROLE_DRIVER, // Lái Xe
        ROLE_CRAFT_WAREHOUSE, // Thủ Kho
        ROLE_SECURITY, // Bảo Vệ 
        
    );
    public static $ROLE_CCS = array(
        ROLE_EMPLOYEE_MARKET_DEVELOPMENT, // NV CCS
        ROLE_MONITORING_MARKET_DEVELOPMENT, // Chuyên Viên CCS Jul 28, 2016
    );
    
    /** @Author: ANH DUNG Aug 08, 2018
     *  @Todo: get array type of this table 
     **/
    public function getArrayType() {
        $aType =  [
            GasOneMany::TYPE_AGENT_OF_MONITOR   => 'Đại lý thuộc NV giám sát',
            GasOneMany::TYPE_AGENT_OF_TARGET    => 'Target 2019 - Đại lý của CV, GSKV',
            GasOneMany::TYPE_MONITOR_CCS        => 'NV CCS của Chuyên Viên',
            GasOneMany::TYPE_MONITOR_GDKV       => 'Chuyên Viên của GĐKV',
            GasOneMany::TYPE_CUSTOMER_ALLOW_DEBIT   => 'KH mối được bấm nợ',
            GasOneMany::TYPE_GDKV_PROVINE       => 'Tỉnh của GDKV',
            GasOneMany::TYPE_AGENT_CITY         => 'Đại lý - Thành phố thuộc tỉnh',
        ];
        
        $cRole  = MyFormat::getCurrentRoleId();
        $cUid   = MyFormat::getCurrentUid();
        if($cRole != ROLE_ADMIN){
            $aType = [
                GasOneMany::TYPE_MONITOR_CCS        => 'NV CCS của Chuyên Viên',
                GasOneMany::TYPE_MONITOR_GDKV       => 'Chuyên Viên của GĐKV',
                GasOneMany::TYPE_AGENT_OF_TARGET    => 'Target 2019 - Đại lý của GSKV',
                GasOneMany::TYPE_GDKV_PROVINE       => 'Tỉnh của GDKV',
                GasOneMany::TYPE_AGENT_CITY         => 'Đại lý - Thành phố thuộc tỉnh',
            ];
        }
        if($cUid == GasConst::UID_NGOC_PT){
            $aType = [
                GasOneMany::TYPE_AGENT_OF_MONITOR   => 'Đại lý thuộc NV giám sát',
                GasOneMany::TYPE_AGENT_CITY         => 'Đại lý - Thành phố thuộc tỉnh',
            ];
        }
        return $aType;
    }
    public function getDateApplyDefault() {
        return '2019-01-01';// init for old data
    }
    
    public function getArrayTypeUseDateApply() {
        return [
            GasOneMany::TYPE_MONITOR_CCS,
            GasOneMany::TYPE_MONITOR_GDKV,
            GasOneMany::TYPE_GDKV_PROVINE,
        ];
    }
    
    /** @Author: DungNT Apr 18, 2019
     *  @Todo: render html để xác định type dùng date apply 
     **/
    public function getHtmlTypeDateApply() {
        $res = '';
        foreach($this->getArrayTypeUseDateApply() as $type){
            $res .= "<span class='SetupTypeDateApply$type'></span>";
        }
        return $res;
    }
    
    /** @Author: NamNH Jul 16,2019
     *  @Todo: Lấy url one_id
     **/
    public function getUrlOneByType(){
        $this->setupUrlOne             = Yii::app()->createAbsoluteUrl('admin/ajax/search_user_login');
        $this->relationUrl             = 'one';
        $this->showTableInfoUrl        = 1;
        $this->nameView                = 'first_name';
        $this->enableFied              = 0;
        $this->setupPlaceholderOne     = 'Nhập tên NV, Số . Tối thiểu 2 ký tự';
        $this->setupLabelOne           = 'Chuyên viên / Giám sát';
        switch ($this->type) {
            case GasOneMany::TYPE_AGENT_CITY:
                $this->setupUrlOne             = Yii::app()->createAbsoluteUrl('admin/ajax/searchProvince');
                $this->relationUrl             = 'province';
                $this->showTableInfoUrl        = 0;
                $this->enableFied              = 1;
                $this->setupPlaceholderOne     = 'Nhập tên tỉnh';
                $this->setupLabelOne           = 'Tỉnh';
                $this->nameView                = 'name';
                break;
            default:
                break;
        }
    }
    /** @Author: NamNH Aug 22, 2019
     *  @Todo: set name and field show name model one and many
     **/
    public function getNameModelByType(){
        $this->nameModelOne     = 'Users';
        $this->nameFieldOne     = 'first_name';
        $this->nameModelMany    = 'Users';
        $this->nameFieldMany    = 'first_name';
        switch ($this->type) {
            case GasOneMany::TYPE_GDKV_PROVINE:
                $this->nameModelOne     = 'Users';
                $this->nameFieldOne     = 'first_name';
                $this->nameModelMany    = 'GasProvince';
                $this->nameFieldMany    = 'name';
                break;
            case GasOneMany::TYPE_AGENT_CITY:
                $this->nameModelOne     = 'GasProvince';
                $this->nameFieldOne     = 'name';
                $this->nameModelMany    = 'Users';
                $this->nameFieldMany    = 'first_name';
                break;
            default:
                break;
        }
    }
    
    /** @Author: NamNH Aug 22, 2019
     *  @Todo: set name and field show name model one and many
     **/
    public function getShowView($aId,&$aUsers,$modelName,$fieldName) {
        $aResult    = [];
        $aModel     = $modelName::model()->findAllByPk($aId);
        foreach ($aModel as $key => $model) {
            $aResult[$model->id] = $model->$fieldName;
            if($modelName == 'Users'){
                $aUsers[$model->id] = $model;
            }
        }
        return $aResult;
    }
    
    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return GasOneMany the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{_gas_one_many}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        return array(
            array('date_apply, id, one_id, many_id, type', 'safe'),
            //LOC003
            array('han_muc_vo_from, han_muc_vo_to', 'safe'),
            // DuongNV 5 Jun,19 Tăng giảm giá đại lý
            array('one_id, many_id, date_apply', 'required', 'on'=>'PriceAgentCreate, PriceAgentUpdate','message'=>'Không được để trống ô này.'),
            array('searchFrom, searchTo, dateFrom, dateTo', 'safe'),
            array('one_id', 'unique',
                'on'=>'PriceAgentCreate, PriceAgentUpdate',
                'criteria' => array(
                    'condition' => 'type = '.self::TYPE_AGENT_PRICE),
                'message'=>'Đại lý này đã được setup giá.'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        return array(
            'one' => array(self::BELONGS_TO, 'Users', 'one_id'),
            'many' => array(self::BELONGS_TO, 'Users', 'many_id'),		
            'province' => array(self::BELONGS_TO, 'GasProvince', 'one_id'),		
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'one_id' => 'One',
            'many_id' => 'Many',
            'date_apply' => 'Ngày áp dụng',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search(){
        $criteria=new CDbCriteria;
        $criteria->compare('t.type',$this->type);
        //LOC003
        if ($this->type == GasOneMany::TYPE_DEBIT_VO)
        {
//            $han_muc_vo_from = 0;
//            $han_muc_vo_to = 0;
            if(!empty($this->one_id)){
                $criteria->compare('one_id', $this->one_id);
            }
            if (!empty($this->han_muc_vo_from)) {
                $criteria->addCondition("many_id >= $this->han_muc_vo_from");
            }
            if (!empty($this->han_muc_vo_to)) {
                $criteria->addCondition("many_id <= $this->han_muc_vo_to");
            }
        }
        //
        return new CActiveDataProvider($this, array(
                'criteria'=>$criteria,
                'pagination'=>array(
                    'pageSize'=> Yii::app()->user->getState('pageSize',Yii::app()->params['defaultPageSize']),
                ),
        ));
    }

    /**
     * @Author: ANH DUNG Sep 19, 2014
     * @Todo: get mảng many id cho drop downlist or checkbox list multiselect
     * @Param: $one_id co the la id sub agent, id giam sat PTTT
     * @Param: $type: 1: ONE_AGENT_MAINTAIN, 2: ONE_AGENT_ACCOUNTING,
     *  3: ONE_SELL_MAINTAIN_PROMOTION, 4: ONE_MONITORING_MARKET_DEVELOPMENT,
     *  5: ONE_USER_MANAGE_MULTIUSER ( Dec 06, 2014 )
     * @param: $date_apply Y-m-d
     */
    public static function getArrOfManyId($one_id, $type, $date_apply = '')
    {
        if(empty($one_id)){
            return array();
        }
        $criteria=new CDbCriteria;
        if(is_array($one_id)){
            $sParamsIn = implode(',', $one_id);
            if(!empty($sParamsIn)){
                $criteria->addCondition("t.one_id IN ($sParamsIn)");
            }
        }else{
            $criteria->compare('t.one_id', $one_id);
        }
        
        if(is_array($type)){
            $sParamsIn = implode(',', $type);
            if(!empty($sParamsIn)){
                $criteria->addCondition("t.type IN ($sParamsIn)");
            }
        }else{
            $criteria->compare('t.type', $type);
        }
        if(!empty($date_apply)){
            $criteria->compare('t.date_apply', $date_apply);
        }

        $criteria->order = "t.id asc";
        return  CHtml::listData(self::model()->findAll($criteria),'many_id','many_id');  
    }
    
    /**
     * @Author: ANH DUNG Sep 19, 2016
     * @Todo: get array many id => one id
     * @Param: get list CV CCS theo tung NV CCS
     */
    public static function getManyOneIdByType($one_id, $type)
    {
        $criteria=new CDbCriteria;
        if(is_array($one_id)){
            $sParamsIn = implode(',', $one_id);
            if(!empty($sParamsIn)){
                $criteria->addCondition("t.one_id IN ($sParamsIn)");
            }
        }else{
            if(!empty($one_id)){
                $criteria->compare('t.one_id', $one_id);
            }
        }

        if(is_array($type)){
            $sParamsIn = implode(',', $type);
            if(!empty($sParamsIn)){
                $criteria->addCondition("t.type IN ($sParamsIn)");
            }
        }else{
            $criteria->compare('t.type', $type);
        }
        $criteria->order = "t.id asc";
        return  CHtml::listData(self::model()->findAll($criteria),'many_id','one_id');  
    }

    /**
     * @Author: ANH DUNG Apr 04, 2015
     * @Todo: get array id nv PTTT cua tung monitor id
     * @Param: $aMonitoringId array monitor id
     * @Param: $aUidInfoName array để find 1 lần lấy ra name của user đó
     */
    public static function GetArrEmployeeOfMonitor($aMonitoringId, &$aUidEmployeeId) {
        $criteria=new CDbCriteria;
        $criteria->compare('t.type', ONE_MONITORING_MARKET_DEVELOPMENT);
        $sParamsIn = implode(',', $aMonitoringId);
        if(!empty($sParamsIn)){
            $criteria->addCondition("t.one_id IN ($sParamsIn)");
        }
        $criteria->order = "t.one_id asc";
        $models = self::model()->findAll($criteria);
        $aRes = array();
        foreach($models as $item){
            $aRes[$item->one_id][$item->many_id] = $item->many_id;
            $aUidEmployeeId[$item->many_id] = $item->many_id;
        }
        // Dec 19, 2015 fix nếu monitor không gắn đại lý cũng cho vào luôn
        $aResNew = array();// giữ lại cái sort theo province của $aMonitoringId
        foreach($aMonitoringId as $monitor_id){
            if(!isset($aRes[$monitor_id])){
                $aRes[$monitor_id][1] = 1;
            }
            $aResNew[$monitor_id] = $aRes[$monitor_id];
        }
        // Dec 19, 2015 fix nếu monitor không gắn đại lý cũng cho vào luôn
        return $aResNew;
    }
    
    /**
     * @Author: ANH DUNG Sep 19, 2016
     * @Todo: get array monitoring_id => array CCS_id
     */
    public static function getEmployeeOfMonitor($type) {
        $criteria=new CDbCriteria;
        $criteria->compare('t.type', $type);
        $criteria->order = "t.one_id asc";
        $models = self::model()->findAll($criteria);
        $aRes = array();
        foreach($models as $item){
            $aRes[$item->one_id][$item->many_id] = $item->many_id;
        }
        return $aRes;
    }    
    
    // get mảng many id cho drop downlist or checkbox list multiselect
    public static function getArrOfManyIdByType($type, $one_id=false){
        $criteria=new CDbCriteria;
        $criteria->addCondition('t.type=' . $type);
        if($one_id){
            $criteria->addCondition('t.one_id=' . $one_id);
        }
        $criteria->order = "t.id asc";
        return CHtml::listData(self::model()->findAll($criteria),'many_id','many_id');
    }
    public function getByType($needMore = []){
        $criteria=new CDbCriteria;
        $criteria->addCondition('t.type=' . $this->type);
        if($this->one_id){
            $criteria->addCondition('t.one_id=' . $this->one_id);
        }
        if(isset($needMore['ArrAgentIdNotFind'])){
            $sParamsIn = implode(',', $needMore['ArrAgentIdNotFind']);
            $criteria->addCondition("t.one_id NOT IN ($sParamsIn)");
        }
        
        return self::model()->findAll($criteria);
    }
	
	// get mảng many id cho drop downlist or checkbox list multiselect
    public static function getArrModelOfManyId($one_id, $type)
    {
        if(empty($one_id)){
            return array();
        }
        $criteria=new CDbCriteria;
        $criteria->compare('t.one_id',$one_id);
        $criteria->compare('t.type',$type);
        $criteria->order = "t.id asc";
        return  self::model()->findAll($criteria);  
    }
	
    /** @Author: DungNT Apr 01, 2019
     *  @Todo: deleteBy one_id, type, date_apply
     *  @Param: $this->date_apply : Y-m-d
     **/
    public function deleteByDateApply() {
        $criteria=new CDbCriteria;
        $criteria->compare('one_id', $this->one_id);
        $criteria->compare('type', $this->type);
        if(!empty($this->date_apply)){
            $criteria->compare('date_apply', $this->date_apply);
        }
        self::model()->deleteAll($criteria);
    }
    
    /** @Author: DungNT Apr 18, 2019
     *  @Todo: xử lý định dạng date_apply
     *  @Param: nếu không phải type save date_apply thì xóa empty biến date_apply đi
     **/
    public function formatRuleDateApply() {
        if(!in_array($this->type, $this->getArrayTypeUseDateApply())){
            $this->date_apply = $this->getDateApplyDefault();
        }else{
            $this->date_apply = MyFormat::dateDmyToYmdForAllIndexSearch($this->date_apply);
            $this->date_apply = MyFormat::dateConverYmdToDmy($this->date_apply, 'Y-m-01');
        }
    }
    
    /** @Author: DungNT Apr 18, 2019
     *  @Todo: fix static function And add date_apply
     * @param: $this->date_apply d-m-Y OR empty
     **/
    public function saveArrOfManyIdFixDateApply()
    {
        if(empty($this->one_id) || empty($this->type)){
            return ;
        }
        $this->formatRuleDateApply();
        $this->deleteByDateApply();
        $aRowInsert=array();
        if (!is_array($_POST['GasOneMany']['many_id']) || count($_POST['GasOneMany']['many_id']) < 1) {
            return ;
        }
        foreach ($_POST['GasOneMany']['many_id'] as $item) {
            if(empty($item)){
                continue;
            }
            $aRowInsert[]="('$this->one_id',
                '$item',
                '$this->type',
                '{$this->date_apply}'
                )";
        }
        $tableName = self::model()->tableName();
        $sql = "insert into $tableName (one_id,
                    many_id,
                    type,
                    date_apply
                    ) values ".implode(',', $aRowInsert);
        if(count($aRowInsert)>0)
            Yii::app()->db->createCommand($sql)->execute();
    }
    
    // save mảng many id 
    public static function saveArrOfManyId($one_id, $type)
    {
        if(empty($one_id)){
            return ;
        }
        $criteria=new CDbCriteria;
        $criteria->compare('one_id',$one_id);
        $criteria->compare('type',$type);	
        $aRowInsert=array();
        self::model()->deleteAll($criteria);
        if (isset($_POST['GasOneMany']['many_id']) && is_array($_POST['GasOneMany']['many_id']) && count($_POST['GasOneMany']['many_id']) > 0) {
                foreach ($_POST['GasOneMany']['many_id'] as $item) {
                    if(empty($item)){
                        continue;
                    }
                    $aRowInsert[]="('$one_id',
                        '$item',
                        '$type'    
                        )";
                }
                $tableName = self::model()->tableName();
        $sql = "insert into $tableName (one_id,
                        many_id,
                        type
                        ) values ".implode(',', $aRowInsert);
        if(count($aRowInsert)>0)
            Yii::app()->db->createCommand($sql)->execute();
        }		
    }
    
    /**
     * @Author: ANH DUNG Sep 04, 2016
     * @Todo: save one many by model
     * GasOneMany::saveManyId($model->id, ONE_SELL_MAINTAIN_PROMOTION, 'GasMaintainSell', 'one_many_gift');
     */
    public static function saveManyId($one_id, $type, $name_model, $name_field)
    {
        self::deleteOneByType($one_id, $type);
        $aRowInsert=array();
        if (isset($_POST[$name_model][$name_field]) && is_array($_POST[$name_model][$name_field]) && count($_POST[$name_model][$name_field]) > 0) {
                foreach ($_POST[$name_model][$name_field] as $item) {
                    if(empty($item)){
                        continue;
                    }
                    $aRowInsert[]="('$one_id',
                            '$item',
                            '$type'    
                    )";
                }
            $tableName = self::model()->tableName();
            $sql = "insert into $tableName (one_id,
                            many_id,
                            type
                            ) values ".implode(',', $aRowInsert);
            if(count($aRowInsert)>0)
                Yii::app()->db->createCommand($sql)->execute();		
        }		
    }

    public static function deleteOneByType($one_id, $type=false){
        if(empty($one_id)){
            return ;
        }
        $criteria=new CDbCriteria;
        $criteria->compare('one_id',$one_id);
        if($type)
            $criteria->compare('type',$type);	        
        self::model()->deleteAll($criteria);        
    }

    
    /**
     * @Author: ANH DUNG Jun 01, 2016
     * @Todo: delete many by type, xử lý 1 nv phục vụ KH hoặc kế toán bán hàng chỉ thuộc về 1 đại lý ở 1 thời điểm
     * @Param: trc khi save thì xóa đi
     */
    public static function deleteManyByType($many_id, $type){
        if(!is_array($many_id)){
            return;
        }
        $criteria=new CDbCriteria;
        $sParamsIn = implode(',', $many_id);
        if(!empty($sParamsIn)){
            $criteria->addCondition("many_id IN ($sParamsIn)");
        }else{// fix Apr 15, 2017 có thể gây bug delete all $type
            return ;
        }
        $criteria->compare('type',$type);
        self::model()->deleteAll($criteria);
    }
    
    public static function deleteByType($type){
        if(empty($type)){
            return;
        }
        $criteria=new CDbCriteria;
        $criteria->compare('type',$type);
        self::model()->deleteAll($criteria);
    }
    
    /**
     * @Author: ANH DUNG Jun 03, 2016
     * @Todo: lấy agent id by id kế toán bán hàng hoặc nv giao nhận
     */
    public static function getOneIdByManyId($many_id, $type) {
        $criteria=new CDbCriteria;
        $criteria->addCondition('t.many_id='.$many_id. ' AND t.type='.$type);
        $model = GasOneMany::model()->find($criteria);
        if($model){
            return $model->one_id;
        }
        return 0;
    }
    
    /** @Author: ANH DUNG Dec 13, 2016
     *  @Todo: lấy list agent id by id kế toán bán hàng hoặc nv giao nhận
     */
    public static function getArrayOneIdByManyId($many_id, $type) {
        $criteria=new CDbCriteria;
        $criteria->addCondition('t.many_id='.$many_id. ' AND t.type='.$type);
        return CHtml::listData(GasOneMany::model()->findAll($criteria), 'one_id', 'one_id');
    }
    public static function getManyIdByType($type) {
        $criteria=new CDbCriteria;
        $criteria->addCondition('t.type='.$type);
        return CHtml::listData(GasOneMany::model()->findAll($criteria), 'many_id', 'one_id');
    }
    /** @Author: DungNT Apr 18, 2019
     *  @Todo: fix function get by type and $date_apply 
     * @param: $date_apply Y-m-d
     **/
    public function getManyIdByTypeAndDateApply($type, $date_apply) {
        $criteria=new CDbCriteria;
        $criteria->addCondition('t.type='.$type);
        $criteria->compare('t.date_apply', $date_apply);
        return CHtml::listData(GasOneMany::model()->findAll($criteria), 'many_id', 'one_id');
    }
    public static function getManyIdOnly($type) {
        $criteria=new CDbCriteria;
        $criteria->addCondition('t.type='.$type);
        return CHtml::listData(GasOneMany::model()->findAll($criteria), 'many_id', 'many_id');
    }
    
    /** @Author: ANH DUNG Dec 20, 2018
     *  @Todo: get many_id by Type OR OneId
     **/
    public function getManyIdByTypeOrOneId($type, $one_id = 0) {
        $criteria=new CDbCriteria;
        $criteria->addCondition('t.type='.$type);
        if(!empty($one_id)){
            $criteria->addCondition('t.one_id='.$one_id);
        }
        return CHtml::listData(GasOneMany::model()->findAll($criteria), 'many_id', 'many_id');
    }
    
    /** @Author: ANH DUNG Apr 21, 2017
     *  @Todo: Save one record, fix for Save One record for saveArrOfManyId
     */
    public static function saveOneRecord($one_id, $many_id, $type) {
        $model = new GasOneMany();
        $model->one_id  = $one_id;
        $model->many_id = $many_id;
        $model->type    = $type;
        $model->save();
    }
    
    /** @Author: ANH DUNG Aug 21, 2017
     *  @Todo: assign user to agent
     */
    public static function changeAgentEmployee($mUser) {
        $aRoleAllow = [ROLE_EMPLOYEE_MAINTAIN, ROLE_ACCOUNTING_AGENT];
        $mAppCache = new AppCache();
        if(!in_array($mUser->role_id, $aRoleAllow)){
            return ;
        }
        $agent_id = $mUser->parent_id;
        switch ($mUser->role_id) {
            case ROLE_ACCOUNTING_AGENT:
                $typeOneMany = ONE_AGENT_ACCOUNTING;
                break;
//            case ROLE_DRIVER:// chưa sử dụng set Driver ở biến này
//                $typeOneMany = ONE_AGENT_ACCOUNTING;
//                break;
            default:
                $typeOneMany = ONE_AGENT_MAINTAIN;
                break;
        }
        
        // 1. set trong field json của model User với loại user giao nhận
        if($mUser->role_id == ROLE_EMPLOYEE_MAINTAIN){
            $mUser->setListAgentOfUser($agent_id, false);
            // set vào cache
            $mAppCache->setUserMaintainOfAgent($agent_id);
        }

        // 2. xử lý xóa trong GasOneMany, xóa hết những record gắn NV vào agent trên hệ thống
        GasOneMany::deleteManyByType([$mUser->id], $typeOneMany);
        
        // 3. xử lý save lại vào table GasOneMany
        GasOneMany::saveOneRecord($agent_id, $mUser->id, $typeOneMany);
    }
    
    
    /** fix lại 
     * @Author: ANH DUNG Aug 13, 2018
     *  @todo: khởi tạo session 1 số thông tin của user theo role
     * su dung lan 1: admin/gasreports/targetNew
     */
    public function initSessionAgent($aRoleId, $type) {
        $aUserMonitor = Users::getArrObjectUserByRole($aRoleId);
        $session=Yii::app()->session;
        $aRes = array();
        foreach($aUserMonitor as $model) {
                $temp = array();
                $temp['first_name'] = $model->first_name;
                $temp['parent_id']  = $model->parent_id;
                $temp['array_agent_id'] = GasOneMany::getArrOfManyIdByType($type, $model->id);
//                $temp['more_info....'] = $model->parent_id; co the them thong tin ve sau
                $aRes[$model->id] = $temp;
        }
        $session['SESS_USER_ONEMANY'] = $aRes;
    }
    
    /** @Author: ANH DUNG Dec 05, 2018
     *  @Todo: xử lý get url autocomplete by type
     **/
    public function getUrlByType() {
        $this->setupUrl             = Yii::app()->createAbsoluteUrl('admin/ajax/search_by_role', array('role'=> ROLE_AGENT, 'GetAllAgent' => 1));
        $this->setupPlaceholder     = 'Nhập mã hoặc tên đại lý';
        $this->setupLabel           = 'Đại lý';
        switch ($this->type) {
            case GasOneMany::TYPE_MONITOR_CCS:
            case GasOneMany::TYPE_MONITOR_GDKV:
                $this->setupUrl          = Yii::app()->createAbsoluteUrl('admin/ajax/search_user_login');
                $this->setupPlaceholder  = 'Nhập mã hoặc tên NV';
                $this->setupLabel        = 'Nhân viên';
                break;
            case GasOneMany::TYPE_GDKV_PROVINE:
                $this->setupUrl          = Yii::app()->createAbsoluteUrl('admin/ajax/searchProvince');
                $this->setupPlaceholder  = 'Nhập tên tỉnh';
                $this->setupLabel        = 'Tỉnh';
                break;
            case GasOneMany::TYPE_CUSTOMER_ALLOW_DEBIT:
                $this->setupUrl          = Yii::app()->createAbsoluteUrl('admin/ajax/search_user_by_code');
                $this->setupPlaceholder  = 'Nhập mã hoặc tên KH';
                $this->setupLabel        = 'Khách hàng';
                break;
            default:
                break;
        }
        
    }
    
    
    /** @Author: ANH DUNG Dec 07, 2018
     * @param: $dateRun Y-m-d
     * get cache tree quản lý của 1 NV ccs
     * Array
        (
            [49177 - id ccs or PTTT] => Array 
                (
                    [49173] => 49173 -> id chuyen vien
                    [1176] => 1176  -> id Gdkv
                )
        )
     */
    public function getCacheTreeTeam($dateRun) {// Apr1819 DungNT fix ko lấy từ cache nữa
        $dateRun = MyFormat::dateConverYmdToDmy($dateRun, 'Y-m-01');
        $mGasOneMany    = new GasOneMany();
        $aGdkv          = $mGasOneMany->getManyIdByTypeAndDateApply(GasOneMany::TYPE_MONITOR_GDKV, $dateRun);
        $aMonitoring    = $mGasOneMany->getManyIdByTypeAndDateApply(GasOneMany::TYPE_MONITOR_CCS, $dateRun);
        $aTreeTeamCcs   = [];
        foreach($aMonitoring as $ccs_id => $monitoring_id):
            if(isset($aGdkv[$monitoring_id])):
                $aTreeTeamCcs[$ccs_id]['monitoring_id']     = $monitoring_id;
                $aTreeTeamCcs[$ccs_id]['gdkv_id']           = $aGdkv[$monitoring_id];
            endif;
        endforeach;
        return $aTreeTeamCcs;
    }
    
    public function getCacheTreeTeamRemove() {
        $mAppCache = new AppCache();
        $value = $mAppCache->getCache(AppCache::ARR_TREE_TEAM_CCS);
        if($value === false){
            $value = $this->setCacheTreeTeam();
        }
        return json_decode($value, true);
    }
    
    
    /** @Author: ANH DUNG Dec 06, 2018
     *  @Todo: set cache tree quản lý của 1 NV ccs
     * bam gồm NV -> giám sát/ Chuyên viên -> GĐKV
     * return array [id_ccs => 
     * array(
     *  monitoring_id => monitoring_id,
     *  gdkv_id => gdkv_id,
     * )]
     * Array
        (
            [49177 - id ccs or PTTT] => Array 
                (
                    [49173] => 49173 -> id chuyen vien
                    [1176] => 1176  -> id Gdkv
                )
        )
     * 
     * 1. get list team of gdkv
     * 2. get list team of monitoring_id
     **/
    public function setCacheTreeTeamRemove() {
        $aGdkv          = GasOneMany::getManyIdByType(GasOneMany::TYPE_MONITOR_GDKV);
        $aMonitoring    = GasOneMany::getManyIdByType(GasOneMany::TYPE_MONITOR_CCS);
        $aTreeTeamCcs   = [];
        foreach($aMonitoring as $ccs_id => $monitoring_id):
            if(isset($aGdkv[$monitoring_id])):
                $aTreeTeamCcs[$ccs_id]['monitoring_id']     = $monitoring_id;
                $aTreeTeamCcs[$ccs_id]['gdkv_id']           = $aGdkv[$monitoring_id];
            endif;
        endforeach;
        
        $value      = MyFormat::jsonEncode($aTreeTeamCcs);
        $mAppCache = new AppCache();
        $mAppCache->setCache(AppCache::ARR_TREE_TEAM_CCS, $value, 0);
        return $value;
    }
    
    /** @Author: ANH DUNG Dec 11, 2018
     *  @Todo: get cache listdata KH mối cho bán nợ: id => id
     **/
    public function getCacheCustomerAllowDebit() {
        $mAppCache = new AppCache();
        $value = $mAppCache->getCache(AppCache::LISTDATA_CUSTOMER_DEBIT);
        if($value === false){
            $value = $this->setCacheCustomerAllowDebit();
        }
        return json_decode($value, true);
    }
    
    /** @Author: ANH DUNG Dec 11, 2018
     *  @Todo: set cache listdata KH mối cho bán nợ: id => id
     **/
    public function setCacheCustomerAllowDebit() {
        $aCustomerId    = GasOneMany::getManyIdOnly(GasOneMany::TYPE_CUSTOMER_ALLOW_DEBIT);
        $value      = MyFormat::jsonEncode($aCustomerId);
        $mAppCache = new AppCache();
        $mAppCache->setCache(AppCache::LISTDATA_CUSTOMER_DEBIT, $value, 0);
        return $value;
    }
    
    /** @Author: ANH DUNG Dec 11, 2018
     *  @Todo: update some cache by type
     **/
    public function updateSomeCache() {
        switch ($this->type) {
            case GasOneMany::TYPE_MONITOR_CCS:
//                $this->setCacheTreeTeam();// DungNT Close Apr1819
                break;
            case GasOneMany::TYPE_CUSTOMER_ALLOW_DEBIT:
                $this->setCacheCustomerAllowDebit();
                break;
            default:
                break;
        }
    }
    
    /** @Author: ANH DUNG Dec 20, 2018
     *  @Todo: get list data user by type  GasOneMany::TYPE_MONITOR_GDKV
     **/
    public function getListdataByType($type) {
        $aUid   = $this->getManyIdByTypeOrOneId($type);
        $models = Users::getArrObjectUserByArrUid($aUid, '');
        return CHtml::listData($models, 'id', 'first_name');
    }
    /** @Author: ANH DUNG Dec 20, 2018
     *  @Todo: get list data user by type AND OneId GasOneMany::TYPE_MONITOR_GDKV
     * use: sử dụng để get team của 1 chuyên viên
     **/
    public function getListdataByTypeAndOneId($type, $one_id) {
        $aUid   = $this->getManyIdByTypeOrOneId($type, $one_id);
        $models = Users::getArrObjectUserByArrUid($aUid, '');
        return CHtml::listData($models, 'id', 'first_name');
    }
    
    public function getDateApply() {
        return MyFormat::dateConverYmdToDmy($this->date_apply, 'd-m-Y');
    }
    // for get data /protected/modules/admin/views/gasmember/setAgentForUser/_form_create_update_agent.php
    public function getDateApplyYmd() {
        if(empty($this->date_apply)){
            return '';
        }
        $this->date_apply = MyFormat::dateDmyToYmdForAllIndexSearch($this->date_apply);
        return MyFormat::dateConverYmdToDmy($this->date_apply, 'Y-m-01');
    }
    public function getClassShowDateApply() {
        $res = '';
        if(!in_array($this->type, $this->getArrayTypeUseDateApply())){
            $res = 'display_none';
        }
        return $res;
    }
    
    /** @Author: DungNT Apr 18, 2019
     *  @Todo: với 1 số type dùng date_apply thì phải dùng hàm copy data từ tháng cũ qua tháng mới
     *  Chỉ chạy vào ngày 1 đầu tháng
     **/
    public function cronCopyToNextMonth() {
        if( date('d')*1 != 1 ){// chỉ chạy ngày đầu tháng
            return ;
        }
        $insertDateApply = date('Y-m-01');
        foreach($this->getArrayTypeUseDateApply() as $type){
            $this->copyToNextMonthByType($type, $insertDateApply);
        }
    }
    
    /** @Author: DungNT Apr 18, 2019
     *  @Todo: copy by type
     * @param: $type
     * @param: $prevMonth is date_apply Y-m-01
     * @param: $insertDateApply is date_apply Y-m-01
     **/
    public function copyToNextMonthByType($type, $insertDateApply) {
        $prevMonth = MyFormat::modifyDays($insertDateApply, 1, '-', 'month', 'Y-m-01');
        $criteria=new CDbCriteria;
        $criteria->compare('t.type', $type);
        $criteria->compare('t.date_apply', $prevMonth);
        $models = GasOneMany::model()->findAll($criteria);
        foreach($models as $model){
            $aFieldNotCopy = array('id');
            $newModel = new GasOneMany();
            MyFormat::copyFromToTable($model, $newModel, $aFieldNotCopy);
            $newModel->date_apply = $insertDateApply;
            $newModel->save();
        }
    }
 
    /** @Author: ANH DUNG Dec 17, 2018
     *  @Todo: get array id monitor of gđkv
     **/
    public function getArrayMonitorOfGdkv($gdkv_id, $dateApply){
        if(empty($gdkv_id)){
            return [];
        }
        $criteria=new CDbCriteria;
        $criteria->compare('t.type', GasOneMany::TYPE_MONITOR_GDKV);
        $criteria->compare('t.one_id', $gdkv_id);
        $criteria->compare('t.date_apply', $dateApply);
        $aGasOneMany = GasOneMany::model()->findAll($criteria);
        return CHtml::listData($aGasOneMany, 'many_id', 'many_id');
    }
    
    /** @Author: LOCNV May 28, 2019
     *  @Todo: get thong tin giam sat / chuyen vien
     * LOC003
     *  @Param:
     **/
    public function getInfoChuyenVien() {
        $res = '';
        $mUser = $this->one;
        if ($mUser) {
            $res = $mUser->code_bussiness." - ".$mUser->first_name;
        }
        return $res;
    }
    
    /** @Author: LOCNV May 21, 2019
     *  @Todo: khoi tao cac gia tri cho inventoryCustomer
     *  @Param: model GasStoreCard
     *  @param model $mInventoryCustomer
     * LOC003
     **/
    public function initDebit(&$mInventoryCustomer, $one_id) {
        $mInventoryCustomer->date_from    = date('01-m-Y');
        $mInventoryCustomer->date_to      = date('d-m-Y');
//        if(count($mInventoryCustomer->aCustomerIdLimitVo)){// DungNT Fix Jan1619
//            $aCustomerId = array_merge($aCustomerId, $mInventoryCustomer->aCustomerIdLimitVo);
//        }
        $mInventoryCustomer->province_id        = '';
        $mInventoryCustomer->price_code         = '';
        $mInventoryCustomer->aCustomerIdLimitVo = [];
        $mInventoryCustomer->sale_id = $one_id;
    }
    
    /** @Author: LOCNV May 21, 2019
     *  @Todo: Tinh cong no vo
     *  @Param: $model model GasStoreCard
     * LOC003
     **/
    public function calcDebitVo($one_id) {
        $mInventoryCustomer = new InventoryCustomer();
        $this->initDebit($mInventoryCustomer, $one_id);
        $aData = $mInventoryCustomer->reportDebitVo();
        //tinh cong no vo
        $aCustomer          = isset($aData['CUSTOMER_MODEL']) ? $aData['CUSTOMER_MODEL'] : [];
        $aMaterials         = isset($aData['MATERIALS_MODEL']) ? $aData['MATERIALS_MODEL'] : [];
        $BEGIN_VO           = isset($aData['BEGIN_VO']) ? $aData['BEGIN_VO'] : [];
        $OUTPUT_BEFORE      = isset($aData['OUTPUT_BEFORE']) ? $aData['OUTPUT_BEFORE'] : [];
        $OUTPUT_IN_PERIOD   = isset($aData['OUTPUT_IN_PERIOD']) ? $aData['OUTPUT_IN_PERIOD'] : [];
        $aIdVo              = isset($aData['ID_VO']) ? $aData['ID_VO'] : [];
        $aCongNoVo          = [];
        $VoThucTeNo = 0;


        foreach ($aCustomer as $mCustomer){
        $customer_id = $mCustomer->id;
        
        $agentName   = isset($listdataAgent[$mCustomer->area_code_id]) ? $listdataAgent[$mCustomer->area_code_id] : '';

        $sum_begin = $sum_import = $sum_export = $sum_end = 0;
        $sum_begin_nho = $sum_import_nho = $sum_export_nho = $sum_end_nho = 0;
        $sum_begin_lon = $sum_import_lon = $sum_export_lon = $sum_end_lon = 0;
            
            //for tinh cong no vo nho
        foreach ($aIdVo[$customer_id]['MATERIALS_VO_NHO'] as $materials_id => $qty){
            $materials_no = $name  = $materials_id;
            if(isset($aMaterials[$materials_id])){
                $materials_no = $aMaterials[$materials_id]['materials_no'];
                $name = $aMaterials[$materials_id]['name'];
            }
            $qty_begin          = isset($BEGIN_VO[$customer_id][$materials_id]) ? $BEGIN_VO[$customer_id][$materials_id] : 0;
            $qty_import_before  = isset($OUTPUT_BEFORE[TYPE_STORE_CARD_IMPORT][$customer_id][$materials_id]) ? $OUTPUT_BEFORE[TYPE_STORE_CARD_IMPORT][$customer_id][$materials_id] : 0;
            $qty_export_before  = isset($OUTPUT_BEFORE[TYPE_STORE_CARD_EXPORT][$customer_id][$materials_id]) ? $OUTPUT_BEFORE[TYPE_STORE_CARD_EXPORT][$customer_id][$materials_id] : 0;
                
            $qty_import_in_period = isset($OUTPUT_IN_PERIOD[TYPE_STORE_CARD_IMPORT][$customer_id][$materials_id]) ? $OUTPUT_IN_PERIOD[TYPE_STORE_CARD_IMPORT][$customer_id][$materials_id] : 0;
            $qty_export_in_period = isset($OUTPUT_IN_PERIOD[TYPE_STORE_CARD_EXPORT][$customer_id][$materials_id]) ? $OUTPUT_IN_PERIOD[TYPE_STORE_CARD_EXPORT][$customer_id][$materials_id] : 0;
                
            $begin      = $qty_begin + $qty_export_before - $qty_import_before;
            $end        = $begin + $qty_export_in_period - $qty_import_in_period;
            $sum_begin      += $begin;
            $sum_import     += $qty_import_in_period;
            $sum_export     += $qty_export_in_period;
            $sum_end        += $end;
//                tính toán vỏ nhỏ
            $sum_begin_nho  += $begin;
            $sum_import_nho += $qty_import_in_period;
            $sum_export_nho += $qty_export_in_period;
            $sum_end_nho    += $end;
            
            if($begin == 0 && $qty_import_in_period == 0 && $qty_export_in_period == 0 && $end == 0){
                continue;
            }
                
        }
            //for tinh cong no vo lon
        foreach ($aIdVo[$customer_id]['MATERIALS_VO_LON'] as $materials_id => $qty){
            $materials_no = $name  = $materials_id;
            if(isset($aMaterials[$materials_id])){
                $materials_no = $aMaterials[$materials_id]['materials_no'];
                $name = $aMaterials[$materials_id]['name'];
            }
            $qty_begin          = isset($BEGIN_VO[$customer_id][$materials_id]) ? $BEGIN_VO[$customer_id][$materials_id] : 0;
            $qty_import_before  = isset($OUTPUT_BEFORE[TYPE_STORE_CARD_IMPORT][$customer_id][$materials_id]) ? $OUTPUT_BEFORE[TYPE_STORE_CARD_IMPORT][$customer_id][$materials_id] : 0;
            $qty_export_before  = isset($OUTPUT_BEFORE[TYPE_STORE_CARD_EXPORT][$customer_id][$materials_id]) ? $OUTPUT_BEFORE[TYPE_STORE_CARD_EXPORT][$customer_id][$materials_id] : 0;
                
            $qty_import_in_period = isset($OUTPUT_IN_PERIOD[TYPE_STORE_CARD_IMPORT][$customer_id][$materials_id]) ? $OUTPUT_IN_PERIOD[TYPE_STORE_CARD_IMPORT][$customer_id][$materials_id] : 0;
            $qty_export_in_period = isset($OUTPUT_IN_PERIOD[TYPE_STORE_CARD_EXPORT][$customer_id][$materials_id]) ? $OUTPUT_IN_PERIOD[TYPE_STORE_CARD_EXPORT][$customer_id][$materials_id] : 0;
                
            $begin      = $qty_begin + $qty_export_before - $qty_import_before;
            $end        = $begin + $qty_export_in_period - $qty_import_in_period;
            $sum_begin      += $begin;
            $sum_import     += $qty_import_in_period;
            $sum_export     += $qty_export_in_period;
            $sum_end        += $end;
//                Tính toán vỏ lớn
            $sum_begin_lon  += $begin;
            $sum_import_lon += $qty_import_in_period;
            $sum_export_lon += $qty_export_in_period;
            $sum_end_lon    += $end;
            
            if($begin == 0 && $qty_import_in_period == 0 && $qty_export_in_period == 0 && $end == 0){
                continue;
            }
                
        }
//        $aCongNoVo[$customer_id]['vo_nho'] = $sum_end_nho;
//        $aCongNoVo[$customer_id]['vo_lon'] = $sum_end_lon;
//        $aCongNoVo[$customer_id]['tong'] = $sum_end_lon + $sum_end_nho;
//        $VoThucTeNo += $sum_end_nho + $sum_end_lon;
        $VoThucTeNo += $sum_end_nho;
    }
        return $VoThucTeNo;
    }
    
    /** @Author: LOCNV May 28, 2019
     *  @Todo: get so vo thuc te no
     *  @Param:
     * LOC003
     **/
    public function getVoThucTeNo() {
        $res = '<a href="'. Yii::app()->createAbsoluteUrl('admin/inventoryCustomer/reportDebitVo', ['InventoryCustomer[date_from]' => date('01-m-Y'), 'InventoryCustomer[date_to]' => date('d-m-Y'), 'InventoryCustomer[sale_id]' => $this->one_id]) .'" target="_blank">'; 
        $voThucTeNo = $this->calcDebitVo($this->one_id);
        $this->voThucTeNo = $voThucTeNo;
        $res .= $voThucTeNo;
        $res .= '</a>';
        return $res;
    }
    
    /** @Author: LOCNV May 28, 2019
     *  @Todo: get so luong con ton
     *  @Param:
     * LOC003
     **/
    public function getConTon() {
        return $this->many_id - $this->calcDebitVo($this->one_id);
    }
    
    /**
     * @author DuongNV 5 Jun,19
     * @todo data tăng giảm giá đại lý
     * @return \CActiveDataProvider
     */
    public function getDataPriceAgent(){
        $criteria = new CDbCriteria;
        $criteria->compare('t.type', self::TYPE_AGENT_PRICE);
        $criteria->compare('one_id', $this->one_id);
        
        if ($this->searchFrom != '') {
            $this->searchFrom = MyFormat::removeComma($this->searchFrom);
            $criteria->addCondition("many_id >= $this->searchFrom");
        }
        if ($this->searchTo != '') {
            $this->searchTo = MyFormat::removeComma($this->searchTo);
            $criteria->addCondition("many_id <= $this->searchTo");
        }
        if (!empty($this->dateFrom)) {
            $this->dateFrom = MyFormat::dateConverDmyToYmd($this->dateFrom, '-');
            $criteria->addCondition("date_apply >= '$this->dateFrom'");
        }
        if (!empty($this->dateTo)) {
            $this->dateTo = MyFormat::dateConverDmyToYmd($this->dateTo, '-');
            $criteria->addCondition("date_apply <= '$this->dateTo'");
        }
        return new CActiveDataProvider($this, array(
                'criteria'=>$criteria,
                'pagination'=>array(
                    'pageSize'=> Yii::app()->user->getState('pageSize',Yii::app()->params['defaultPageSize']),
                ),
        ));
    }
    
    /**
     * @author DuongNV 5 Jun,19
     * @todo data tăng giảm giá đại lý
     * @params $date Y-m-d
     * @return array
     */
    public function getArrayPriceAgent($date){
        $criteria = new CDbCriteria;
        $criteria->compare('t.type', self::TYPE_AGENT_PRICE);
        $criteria->compare('t.date_apply', $date);
        $models = self::model()->findAll($criteria);
        return CHtml::listData($models, 'one_id', 'many_id');
    }
    
    /** @Author: NamLA Sep, 2019
     *  @Todo: search for CRUD
     * */
    public function searchCRUD() {
        $criteria = new CDbCriteria;
        $criteria->compare('t.one_id', $this->one_id);
        $criteria->compare('t.many_id', $this->many_id);
        $criteria->compare('t.date_apply', $this->date_apply);
        $criteria->compare('t.type', $this->type);
        
        $aTypeLimit = $this->getArrayTypeCreateUI(true);
        $criteria->addInCondition('t.type', $aTypeLimit);
        
        $criteria->order = 'id DESC';
        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
            'pagination' => array(
                'pageSize' => Yii::app()->user->getState('pageSize', Yii::app()->params['defaultPageSize']),
            ),
        ));
    }
    
    /** @Author: DuongNV Sep2519
     *  @Todo: những loại tạo trên web (crud)
     **/
    public function getArrayTypeCreateUI($onlyKey = false){
        $aTypeValue = $this->getArrayTypeCreateUIValue();
        $aTypeUser  = $this->getArrayTypeCreateUIUser();
        $result     = $aTypeValue + $aTypeUser;
        return $onlyKey ? array_keys($result) : $result;
    }
    
    /** @Author: DuongNV Sep2519
     *  @Todo: những loại tạo trên web với many_id là text
     **/
    public function getArrayTypeCreateUIValue(){
        return [
            GasOneMany::TYPE_DEVICE_IMEI_TEST => 'Device imei test',
        ];
    }
    
    /** @Author: DuongNV Sep2519
     *  @Todo: những loại tạo trên web với many_id là user
     **/
    public function getArrayTypeCreateUIUser(){
        return [
            
        ];
    }

    public function getOneName() {
        return !empty($this->one) ? $this->one->getFullName() : $this->one_id;
    }
    
    public function getManyName(){
        $aTypeUser  = $this->getArrayTypeCreateUIUser();
        if(in_array($this->type, $aTypeUser)){
            return !empty($this->many) ? $this->many->getFullName() : $this->many_id;
        }
        return $this->many_id;
    }

    public function getType() {
        $model = new GasOneMany;
        $aType = $model->getArrayTypeCreateUI();
        return empty($aType[$this->type]) ? $this->type : $aType[$this->type];
    }

    public function handleBeforeSave() {
        $this->date_apply = MyFormat::dateConverDmyToYmd($this->date_apply, '-');
    }
    public function handleBeforeUpdate(){
        $this->date_apply = MyFormat::dateConverYmdToDmy($this->date_apply, 'd-m-Y');
    }
    public function getArrayTypeCreateManyID() {
        return [
            self::TAB_ID              => 'Nhập ID',
            self::TAB_AUTOCOMPLETE    => 'Nhập Autocomplete',
        ];
    }
    
    /** @Author: NhanDT Oct 01,2019
     *  @Todo: save agent for city 
     *  @Param: 
     **/
    public function afterSaveAgent($mAgent) {
        if (!empty($mAgent->id)){
            $this->deleteOldAgentOfCity($mAgent->province_id, $mAgent->id, self::TYPE_AGENT_CITY);
            if (isset($_POST['Users']['checkCity'])){
                $this->one_id  = $mAgent->province_id;
                $this->many_id = $mAgent->id;
                $this->type    = self::TYPE_AGENT_CITY;
                $this->date_apply = $this->getDateApplyDefault();
                $this->save();
            }
        }
        return ;
    }
    
//    remove agent of city
    public function deleteOldAgentOfCity($one_id,$many_id,$type){
        if (empty($one_id)||empty($many_id) || empty($type)){
            return;
        }
        $criteria = new CDbCriteria;
        $criteria->compare('one_id', $one_id);
        $criteria->compare('many_id', $many_id);
        $criteria->compare('type', $type);
        return GasOneMany::model()->deleteAll($criteria);
    }
    
    /** @Author: NhanDT Oct 01,2019
     *  @Todo: check records the same by one_id, many_id, and type
     *  @Param: param
     **/
    public function isAgentOfCity($one_id, $many_id, $type) {
        if (empty($one_id)||empty($many_id) || empty($type))
        {
            return false;
        }
        $criteria = new CDbCriteria;
        $criteria->compare('t.one_id', $one_id);
        $criteria->compare('t.many_id', $many_id);
        $criteria->compare('t.type', $type);
        $model = self::model()->find($criteria);
        return empty($model) ? false : true;
    }
}