<?php

class HrWorkShift extends CActiveRecord
{
    const STATUS_ACTIVE   = 1;
    const STATUS_INACTIVE = 0;
    const TYPE_HGD     = 1;
    const TYPE_BOMOI   = 2;
    const TYPE_TARGET  = 3;
    public static function model($className=__CLASS__)
    {
            return parent::model($className);
    }
    
    public function tableName()
    {
        return '{{_hr_work_shift}}';
    }
    
    public function rules()
    {
        return array(
            array('shift_from, shift_to', 'compareTwoShift', 'on' => 'create,update'),
            array('name, shift_from, shift_to, status, role_id', 'required', 'on'=>'create,update'),
            array('name, shift_from, shift_to, status, role_id, type, factor', 'safe'),
        );
    }
    
    /**
     * @author: DuongNV 27/08/2018
     * @todo validate shift from and shift to
     */
    public function compareTwoShift($attribute)
    {
        
        $hourFrom = explode(':', $this->shift_from)[0];
        $hourTo   = explode(':', $this->shift_to)[0];
        if($hourFrom >= $hourTo)
             $this->addError($attribute, 'Giờ bắt đầu phải nhỏ hơn giờ kết thúc!');
    }
    
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'name'=>'Tên ca',
            'shift_from'=>'Từ',
            'shift_to'=> 'Đến',
            'status'=> 'Trạng thái',
            'role_id'=>'Loại nhân viên',
            'type' => 'Thuộc nhóm',
            'factor' => 'Hệ số ca',
        );
    }
    
    public function relations()
    {
        return array(
            'rWorkSchedule' => array(self::HAS_MANY, 'HrWorkSchedule', 'word_shift_id'),
            'rRole' => array(self::BELONGS_TO, 'Roles', 'role_id'),
        );
    }
    
    public function search()
    {
        $criteria=new CDbCriteria;
        $criteria->compare('t.name',$this->name,true);
        $criteria->compare('t.shift_from',$this->shift_from,true);
        $criteria->compare('t.shift_to',$this->shift_to,true);
        $criteria->compare('t.status', $this->status);
        $criteria->compare('t.role_id', $this->role_id);
        $sort = new CSort();
        $sort->attributes = array(
            'name'=>'name',
        );    
        $sort->defaultOrder = 't.name ASC';
        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
            'sort' => $sort,
            'pagination'=>array(
                'pageSize'=> 20,
            ),
        ));
    }
    /**
     * 
     * @return list array status
     */
    public function getArrayStatus(){
        return array(
            self::STATUS_ACTIVE => 'Hoạt động',
            self::STATUS_INACTIVE => 'Không hoạt động',
        );
    }
    //get status of this
    public function getStatus(){
        $aStatus = $this->getArrayStatus();
        return isset($aStatus[$this->status]) ? $aStatus[$this->status] : '';
    }
    
    //get tên ca
    public function getName() {
        return $this->name;
    }
    
    //get thời gian bắt đầu ca
    public function getShiftFrom() {
        return date('H:i', strtotime($this->shift_from));
    }
    
    //get thời gian kết thúc ca
    public function getShiftTo() {
        return date('H:i', strtotime($this->shift_to));
    }
    
    //get role
    public function getRole() {
        if($this->role_id == ROLE_ALL){
            return HrSalaryReports::ROLE_ALL_NAME;
        }
        return isset($this->rRole) ? $this->rRole->role_name : "";
    }
    
    //get thời gian làm việc
    public function getArrayShiftTime(){
        $step = 30;
        $hourStart = 6; $hourFinish = 22;
        $aShiftTime = [];
        for($hour = $hourStart; $hour <= $hourFinish; $hour++){
            for($minute = 0; $minute <= 59; $minute+=$step){
                $aShiftTime[$hour.':'.sprintf("%02d", $minute).':00'] = $hour.":".sprintf("%02d", $minute);
            }
        }
        return $aShiftTime;
    }
    
    //get arr all shift
    public function getArrayWorkShift(){
        $aWorkShift = [];
        $model = $this->findAll();

        foreach ($model as $shift) {
            $aWorkShift[$shift->attributes['id']] = $shift->attributes['name']." (".$shift->attributes['shift_from']."-".$shift->attributes['shift_to'].")";
        }
        return $aWorkShift;
    }
    
    public function getArrayShiftByRole($role_id = '') {
        if(!isset($role_id)) return 0;
        $criteria = new CDbCriteria;
        if(is_array($role_id)){
            $criteria->addInCondition('t.role_id', $role_id);
        }else{
            $criteria->compare('t.role_id', $role_id);
        }
        $models = self::model()->findAll($criteria);
        return  CHtml::listData($models,'id','id');
    }
    
    /** @Author: NVDuong 10/08/18 13:13
     *  @Todo: Get array loại ca
     *  @Return: Array
     **/
    public function getArrayType() {
        return array(
            self::TYPE_HGD    => 'Ngày công chỉ tiêu',
            self::TYPE_BOMOI  => 'Ngày công thời gian',
            self::TYPE_TARGET => 'Ngày công CG telesale', // DuongNV Nov 14,2018 ( = 0.5 binh thuong)
        );
    }
    
    /** @Author: NVDuong 10/08/18 13:13
     *  @Todo: Get loại ca
     *  @Return: tên ca
     **/
    public function getType(){
        $aType = HrWorkShift::model()->getArrayType();
        if(empty($this->type)) return '';
        return $aType[$this->type];
    }

    /** @Author: NVDuong 10/08/18 13:13
     *  @Todo: Get hệ số ca
     *  @Param: float
     **/
    public function getFactor() {
        return $this->factor;
    }
    
    /** @Author: NVDuong Nov 21, 2018
     *  @Todo: Get id of shift name "NC 1/2 CT"
     **/
    public function getIdOfHaftTargetShift() {
        return 18;
    }
    
    public function loadItems($returnObject = true) {
        $mShift = new HrWorkShift();
        $models = $mShift->findAll();
        $res    = [];
        foreach ($models as $value) {
            $res[$value->id] = $returnObject ? $value : $value->name;
        }
        return $res;
    }
}