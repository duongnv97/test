<?php

/**
 * This is the model class for table "hr_approved_reports".
 *
 * The followings are the available columns in table 'hr_approved_reports':
 * @property integer $id
 * @property string $code_no
 * @property integer $id_approved_lv1
 * @property string $date_approved_lv1
 * @property integer $id_approved_lv2
 * @property string $date_approved_lv2
 * @property integer $id_treasurer
 * @property string $date_treasurer
 * @property string $note
 * @property integer $status
 */
class HrApprovedReports extends BaseSpj
{
    const STT_NEW                   = 1;
    const STT_APPROVED_LV1          = 2; // Chị Ngân duyệt
    const STT_APPROVED_LV2          = 3; // GD duyệt
    const STT_APPROVED_TREASURER    = 4; // Thủ quỹ chi lương
    const STT_CANCEL                = 5; // Hủy
    
    const TYPE_GENERAL              = 1; // Tổng hợp
    const TYPE_DETAIL               = 2; // Chi tiết
    
    public static $aStatus = [
        self::STT_NEW                   => 'Mới',
        self::STT_APPROVED_LV1          => 'Kế toán trưởng duyệt',
        self::STT_APPROVED_LV2          => 'Giám đốc duyệt',
        self::STT_APPROVED_TREASURER    => 'Thủ quỹ đã chi lương',
        self::STT_CANCEL                => 'Hủy',
    ];


    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return HrApprovedReports the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{_hr_approved_reports}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        return [
            ['id, code_no, id_approved_lv1, date_approved_lv1, id_approved_lv2, date_approved_lv2, id_treasurer, date_treasurer, note, status', 'safe'],
            ['date_from, date_to', 'safe']
        ];
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        return [
            'rApprovedLv1' => array(self::BELONGS_TO, 'Users', 'id_approved_lv1'),
            'rApprovedLv2' => array(self::BELONGS_TO, 'Users', 'id_approved_lv2'),
            'rTreasurer' => array(self::BELONGS_TO, 'Users', 'id_treasurer'),
        ];
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'code_no' => 'Mã code',
            'name' => 'Tên',
            'id_approved_lv1' => 'Kế toán trưởng duyệt',
            'date_approved_lv1' => 'Ngày KTT duyệt',
            'id_approved_lv2' => 'Giám Đốc duyệt',
            'date_approved_lv2' => 'Ngày GD duyệt',
            'id_treasurer' => 'Thủ quỹ duyệt',
            'date_treasurer' => 'Ngày chi',
            'note' => 'Ghi chú',
            'status' => 'Trạng thái',
            'created_date' => 'Ngày tạo',
        ];
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        $criteria=new CDbCriteria;

	$criteria->compare('t.id',$this->id);
	$criteria->compare('t.code_no',$this->code_no,true);
	$criteria->compare('t.status',$this->status);
        // [DuongNV] Đang chạy thử nghiệm nên xem hết, giới hạn sau
//        $aAllowLv1  = $this->getArrayAllowLv1();
//        $aAllowLv2  = $this->getArrayAllowLv2();
//        $aTreasurer = $this->getArrayAllowTreasurer();
//        $cUid       = MyFormat::getCurrentUid();
//        if( in_array($cUid, $aAllowLv1) ){
//            $criteria->compare('t.status', self::STT_NEW);
//        }
//        if( in_array($cUid, $aAllowLv2) ){
//            $criteria->compare('t.status', self::STT_APPROVED_LV1);
//        }
//        if( in_array($cUid, $aTreasurer) ){
//            $criteria->compare('t.status', self::STT_APPROVED_LV2);
//        }
        $criteria->order = 't.id DESC';
        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
            'pagination'=>array(
                'pageSize'=> Yii::app()->user->getState('pageSize',Yii::app()->params['defaultPageSize']),
            ),
        ));
    }
    
    public function getName(){
        return $this->name;
    }
    
    public function getStatus(){
        return isset(self::$aStatus[$this->status]) ? self::$aStatus[$this->status] : '';
    }
    
    public function getApprovedDate($date){
        return MyFormat::dateConverYmdToDmy($date, 'd/m/Y H:i');
    }
    
    public function getApprovedLv1(){
        return isset($this->rApprovedLv1) ? $this->rApprovedLv1->getFullName() : '';
    }
    
    public function getApprovedLv2(){
        return isset($this->rApprovedLv2) ? $this->rApprovedLv2->getFullName() : '';
    }
    
    public function getTreasurer(){
        return isset($this->rTreasurer) ? $this->rTreasurer->getFullName() : '';
    }
    
    public function getDetail(){
        $ret  = '<b>Giám đốc:</b> '.$this->getApprovedLv2();
        $ret .= "<br><i>{$this->getApprovedDate($this->date_approved_lv2)}</i><br>";
        
        $ret .= '<br><b>KTT:</b> '.$this->getApprovedLv1();
        $ret .= "<br><i>{$this->getApprovedDate($this->date_approved_lv1)}</i><br>";
        
        $ret .= '<br><b>Thủ quỹ:</b> '.$this->getTreasurer();
        $ret .= "<br><i>{$this->getApprovedDate($this->date_treasurer)}</i><br>";
        return $ret;
    }
    
    public function isTreasurer(){
        $aTreasurer  = $this->getArrayAllowTreasurer();
        $cUid        = MyFormat::getCurrentUid();
        return in_array($cUid, $aTreasurer);
    }
    
    /** @Author: DuongNV 28 Mar, 19
     *  @Todo: list user được duyệt bảng lương lv1 (chị Ngân, ...)
     **/
    public function getArrayAllowLv1() {
        return [
//            1979734, // uid DuongNV test only
            GasLeave::UID_CHIEF_ACCOUNTANT
        ];
    }
    
    /** @Author: DuongNV 28 Mar, 19
     *  @Todo: list user được duyệt bảng lương lv2 (GD, ...)
     **/
    public function getArrayAllowLv2() {
        return [
//            1979734, // uid DuongNV test only
             GasLeave::UID_DIRECTOR
        ];
    }
    
    /** @Author: DuongNV 28 Mar, 19
     *  @Todo: thủ quỹ chi lương
     **/
    public function getArrayAllowTreasurer() {
        return [
            GasConst::UID_NHAN_NTT,
//            1436021
//            1979734, // uid UITDuongNV test only            2
        ];
    }
    
    public function canApprove(){
        $cRole = MyFormat::getCurrentRoleId();
        if ($cRole == ROLE_ADMIN) {
            return true;
        }
        $aAllowLv1       = $this->getArrayAllowLv1();
        $aAllowLv2       = $this->getArrayAllowLv2();
        $aAllowTreasurer = $this->getArrayAllowTreasurer();
//        $aAllow          = array_merge_recursive($aAllowLv1,$aAllowLv2,$aAllowTreasurer);
        $cUid            = MyFormat::getCurrentUid();
        switch ($this->status) {
            case self::STT_NEW:
                return in_array($cUid, $aAllowLv1);
                
            case self::STT_APPROVED_LV1:
                return in_array($cUid, $aAllowLv2);
                
            case self::STT_APPROVED_LV2:
                return in_array($cUid, $aAllowTreasurer);

            default:
                return false;
        }
        return false;
    }
    
    public function cancelReport(){
        $this->status = self::STT_CANCEL;
        $this->notifyEmail();
        $mSalary = new HrSalaryReports();
        $mSalary->changeReportStatusByCodeNo($this->code_no, HrSalaryReports::STT_CALCULATING);
    }
    
    public function approveReport(){
        $aAllowLv1       = $this->getArrayAllowLv1();
        $aAllowLv2       = $this->getArrayAllowLv2();
        $aAllowTreasurer = $this->getArrayAllowTreasurer();
        $cUid            = MyFormat::getCurrentUid();
        
        if( in_array($cUid, $aAllowLv1) && $this->status == self::STT_NEW ){
            $this->status            = self::STT_APPROVED_LV1;
            $this->id_approved_lv1   = $cUid;
            $this->date_approved_lv1 = date('Y-m-d H:i:s');
        }
        if( in_array($cUid, $aAllowLv2) && $this->status == self::STT_APPROVED_LV1 ){
            $this->status            = self::STT_APPROVED_LV2;
            $this->id_approved_lv2   = $cUid;
            $this->date_approved_lv2 = date('Y-m-d H:i:s');
        }
        if( in_array($cUid, $aAllowTreasurer) && $this->status == self::STT_APPROVED_LV2 ){
            $this->status            = self::STT_APPROVED_TREASURER;
            $this->id_treasurer      = $cUid;
            $this->date_treasurer    = date('Y-m-d H:i:s');
            $mSalary                 = new HrSalaryReports();
            $mSalary->changeReportStatusByCodeNo($this->code_no, HrSalaryReports::STT_COMPLETE);
        }
        $this->notifyEmail();
        $this->save();
    }
    
    public function getListEmail(){
        $mHrSalaryReport = new HrSalaryReports();
        $aAllowSendReview= $mHrSalaryReport->getArraySendReview();
        $aAllowLv1       = $this->getArrayAllowLv1();
//        $aAllowLv1       = [];
        $aAllowLv2       = $this->getArrayAllowLv2();
//        $aAllowLv2       = []; // Khi nào hoàn thiện sẽ mail cho GD sau
        $aAllowTreasurer = $this->getArrayAllowTreasurer();
//        $aAllowTreasurer = []; // Khi nào hoàn thiện sẽ mail cho thủ quỹ sau
        $aAllow          = array_merge_recursive($aAllowLv1,$aAllowLv2,$aAllowTreasurer, $aAllowSendReview);
        $criteria        = new CDbCriteria;
        $criteria->addInCondition('t.id', $aAllow);
        $models          = Users::model()->findAll($criteria);
        $ret             = [];
        foreach ($models as $value) {
            $ret[$value->id] = $value->email;
        }
        return array_filter($ret);
    }
    
    /** @Author: DuongNV May 7,19
     *  @Todo: Gửi mail notify khi có bảng lương mới
     *  @Param: $this->status, $this->code_no, $this->note
     **/
    public function notifyEmail() {
        if(empty($this->id)){
            return;
        }
        $aEmail          = [];
        $mHrSalaryReport = new HrSalaryReports();
        $aAllowSendReview= $mHrSalaryReport->getArraySendReview();
        $aAllowLv1       = $this->getArrayAllowLv1();
        $aAllowLv2       = $this->getArrayAllowLv2();
        $aAllowTreasurer = $this->getArrayAllowTreasurer();
        $listEmail       = $this->getListEmail();
        foreach ($listEmail as $uid => $mail) {
            if(in_array($uid, $aAllowSendReview)){
                $aEmail[self::STT_CANCEL][$uid] = $mail;
            }
            if(in_array($uid, $aAllowLv1)){
                $aEmail[self::STT_APPROVED_LV1][$uid] = $mail;
            }
            if(in_array($uid, $aAllowLv2)){
                $aEmail[self::STT_APPROVED_LV2][$uid] = $mail;
            }
            if(in_array($uid, $aAllowTreasurer)){
                $aEmail[self::STT_APPROVED_TREASURER][$uid] = $mail;
            }
        }
        $mHrSalaryReport   = new HrSalaryReports();
        $mHrSalaryReport   = $mHrSalaryReport->getReportSameCodeNo($this->code_no);
        $title             = 'Có một bảng lương mới cần duyệt: '.$this->name;
        $info              = '<br>Thông tin bảng lương:';
        $info             .= '<br><b>Mã code:</b> '. $this->code_no;
        $info             .= '<br><b>Từ ngày:</b> '. MyFormat::dateConverYmdToDmy($mHrSalaryReport->start_date);
        $info             .= '<b> Đến ngày:</b> '. MyFormat::dateConverYmdToDmy($mHrSalaryReport->end_date);
        
        $footer            = '<br>Vui lòng truy cập link bên dưới để xem chi tiết và duyệt bảng lương';
        $url               = Yii::app()->createAbsoluteUrl('admin/hrApprovedReports/view',['id'=> $this->id]);
        $footer           .= '<br><a href='.$url.'>Đi đến trang duyệt</a>';
        $needMore['title'] = '[Thông báo] Có bảng lương mới cần duyệt';
        switch ($this->status) {
            case self::STT_NEW:
                $needMore['list_mail'] = $aEmail[self::STT_APPROVED_LV1];
                break;
                
            case self::STT_APPROVED_LV1:
                $needMore['list_mail'] = $aEmail[self::STT_APPROVED_LV2];
                break;
                
            case self::STT_APPROVED_LV2:
                $title = 'Có bảng lương mới cần chi';
                $needMore['title'] = '[Thông báo] Có bảng lương mới cần chi: '.$this->name;
                $needMore['list_mail'] = $aEmail[self::STT_APPROVED_TREASURER];
                break;
                
            case self::STT_APPROVED_TREASURER:
                $needMore['title'] = '[Thông báo] Thủ quỹ đã chi lương';
                $title   = 'Thủ quỹ đã chi lương: '.$this->name;
                $footer  = '<br>Thông tin chi tiết về bảng lương';
                $url     = Yii::app()->createAbsoluteUrl('admin/hrApprovedReports/view',['id'=> $this->id]);
                $footer .= '<br><a href='.$url.'>Đi đến bảng lương</a>';
                $needMore['list_mail'] = array_reduce($aEmail, 'array_merge',[]);
                break;
                
            case self::STT_CANCEL:
                $needMore['list_mail'] = $aEmail[self::STT_CANCEL];
                $needMore['title']     = '[Thông báo] Yêu cầu cập nhật bảng lương: '.$this->name;
                $title                 = 'Có một bảng lương cần cập nhật.';
                $info                 .= '<br><b>Lý do hủy:</b> '. $this->note;
                
                $footer                = '<br>Vui lòng truy cập link bên dưới tính lại bảng lương<br>';
                $url                   = Yii::app()->createAbsoluteUrl('/admin/hrSalaryReports/report');
                $footer               .= '<br><a href='.$url.'>Đi đến trang tính lương</a>';
                break;
                
            default:
                break;
        }
        $body = $title.'<br>'.$info.'<br>'.$footer;
        $aFollow = [
            'duongnv@spj.vn', 
            'dungnt@spj.vn',
            'chaulnm@spj.vn',
        ];
        $needMore['list_mail'] = array_merge($needMore['list_mail'], $aFollow);
        // test
//        $needMore['list_mail'] = ['duongnv@spj.vn'];
//        $needMore['SendNow']   = 1;
        SendEmail::bugToDev($body, $needMore);
    }
    
    /** @Author: DuongNV May 21,2019
     *  @Todo: get array data provider complete report
     **/
    public function searchGeneralReport(){
        $criteria = new CDbCriteria;
//        $criteria->compare('t.status', HrSalaryReports::STT_COMPLETE);
        $criteria->compare('t.is_show', 1);
        $models = new HrSalaryReports();
        if(!empty($this->date_from)){
            $date_from = date('Y-m-01', strtotime($this->date_from));
            $criteria->addCondition("t.start_date >= '$date_from'");
        }
        if(!empty($this->date_to)){
            $date_to = MyFormat::dateConverDmyToYmd($this->date_to, '-');
            $criteria->addCondition("t.start_date <= '$date_to'");
        }
        $criteria->group = 't.start_date, t.end_date, t.code_no';
//        $models = HrSalaryReports::model()->findAll($criteria);
        $criteria->order = 't.start_date DESC';
        return new CActiveDataProvider($models, array(
            'criteria' => $criteria,
            'pagination' => array(
                'pageSize' => Yii::app()->user->getState('pageSize',Yii::app()->params['defaultPageSize']),
            ),
        ));
    }
}