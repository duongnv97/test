<?php

/**
 * This is the model class for table "{{_funds}}".
 *
 * The followings are the available columns in table '{{_funds}}':
 * @property integer $id
 * @property integer $agent_id
 * @property integer $funds
 * @property string $date_apply
 * @property integer $created_by
 * @property string $created_date
 */
class Funds extends BaseSpj
{
    public $autocomplete_name, $date_from, $date_to;
    public $search_month, $search_year;
    
    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return Funds the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{_funds}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        return [
            ['id, agent_id, funds, date_apply, created_by, created_date', 'safe'],
            ['search_month, search_year', 'safe'],
            ['agent_id,date_apply ,funds', 'required', 'on' => 'create, update'],
        ];
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        return [
            'rAgent' => array(self::BELONGS_TO, 'Users', 'agent_id'),
            'rCreatedBy' => array(self::BELONGS_TO, 'Users', 'created_by'),
        ];
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'agent_id' => 'Đại lý',
            'funds' => 'Giá vốn',
            'date_apply' => 'Ngày áp dụng',
            'created_by' => 'Người tạo',
            'created_date' => 'Ngày tạo',
        ];
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        $criteria=new CDbCriteria;

	$criteria->compare('t.agent_id',$this->agent_id);
	$criteria->compare('t.funds',$this->funds);
	$criteria->compare('t.date_apply',$this->date_apply,true);
        if(!empty($this->date_from)){
            $from = MyFormat::dateConverDmyToYmd($this->date_from, "-");
            $criteria->addCondition('t.date_apply >= "'.$from.'"');
        }
        if(!empty($this->date_to)){
            $to = MyFormat::dateConverDmyToYmd($this->date_to, "-");
            $criteria->addCondition('t.date_123apply <= "'.$to.'"');
        }
        if(!empty($this->search_month)){
            $month = date('m', strtotime($this->search_month));
            $year  = date('Y', strtotime($this->search_month));
            $criteria->addCondition('MONTH(t.date_apply) = "'.$month.'"');
            $criteria->addCondition('YEAR(t.date_apply) = "'.$year.'"');
        }
//        if(!empty($this->search_year)){
//            $criteria->addCondition('YEAR(t.date_apply) = "'.$this->search_year.'"');
//        }
//	$criteria->compare('t.created_by',$this->created_by);
//	$criteria->compare('t.created_date',$this->created_date,true);
        
        $criteria->order = 't.id DESC';
        
        $_SESSION['data-excel-funds'] = new CActiveDataProvider($this, array(
            'pagination'=>false,
            'criteria'=>$criteria,
        ));
        
        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
            'pagination'=>array(
                'pageSize'=> Yii::app()->user->getState('pageSize',Yii::app()->params['defaultPageSize']),
            ),
        ));
    }
    
    public function getAgent(){
        return isset($this->rAgent) ? $this->rAgent->first_name : "";
    }
    
    public function getFunds(){
        return ActiveRecord::formatCurrency($this->funds);
    }
    
    public function getMonthApply(){
        return date('m', strtotime($this->date_apply));
    }
    
    public function getYearApply(){
        return date('Y', strtotime($this->date_apply));
    }
    
    public function getCreatedBy(){
        return isset($this->rCreatedBy) ? $this->rCreatedBy->getFullName() : "";
    }
    
    /** @Author: DuongNV Mar 6,19
     *  @Todo: cron update giá vốn  Cron tháng trước * tăng giảm thị trường trong ngày 1 đầu tháng
     *  @param $date Y-m-d Ngày chứa tháng để cron
     **/
    public function cronUpdateFunds($date) {
        if( date('d')*1 != 1 ){// chỉ chạy ngày đầu tháng
            return ;
        }
        $dateOfPrevMonth    = MyFormat::modifyDays($date, 1, '-', 'month');
        $month              = date('m', strtotime($dateOfPrevMonth));
        $year               = date('Y', strtotime($dateOfPrevMonth));
        $criteria           = new CDbCriteria;
        $criteria->compare('month(t.date_apply)', $month);
        $criteria->compare('year(t.date_apply)', $year);
        $models             = Funds::model()->findAll($criteria);
        
        $mGasPrice = new GasPrice();
        $mGasPrice->c_month = date('m');// May0219 DungNT fix lấy sai tháng
        $mGasPrice->c_year  = date('Y');
        $mGasPrice->notifyGasPriceSetupMsg();
        foreach ($models as $value) {
            $newFund                = new Funds();
            $newFund->funds         = $value->funds + $mGasPrice->prevMonthPrice12;
            $newFund->agent_id      = $value->agent_id;
            $newFund->created_by    = GasConst::UID_ADMIN;
            $newFund->date_apply    = MyFormat::modifyDays($value->date_apply, 1, '+', 'month', 'Y-m-01');
            $newFund->save();
        }
    }
    
    /** @Author: DuongNV Mar 20,19
     *  @Todo: Chỉ cho update sau 1 ngày kể từ ngày tạo
     **/
    public function canUpdate(){
//        $destinationDate    = MyFormat::modifyDays($this->created_date, 30, '+', 'day', 'Y-m-d H:i:s');
        $destinationDate    = MyFormat::dateConverYmdToDmy($this->created_date, 'm') * 1;
        $now                = date('m') * 1;
        $cUid               = MyFormat::getCurrentUid();
        $aUidAllow = [
            1979725, // UitNamNH
            GasConst::UID_ADMIN,
            GasConst::UID_CHIEN_BQ,
        ];
        return in_array($cUid, $aUidAllow) && ($now == $destinationDate);
    }
    
    public function formatFundsSave(){
        $this->funds = MyFormat::removeComma($this->funds);
    }
}
