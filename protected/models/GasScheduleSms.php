<?php

/**
 * This is the model class for table "{{_gas_schedule_sms}}".
 *
 * The followings are the available columns in table '{{_gas_schedule_sms}}':
 * @property string $id
 * @property string $user_id
 * @property integer $type
 * @property string $obj_id
 * @property string $time_send 
 * @property string $created_date
 * @property string $username
 * @property string $title
 * @property string $json_var
 * @property string $count_run
 * @property string $smsid
 * @property string $code_response
 * @property string $phone
 */
class GasScheduleSms extends BaseSpj
{
    public $ServiceID       = 'Gas24h';
    public $CommandCode     = 'bulksms';
    public $User            = 'smsbrand_gas24h';
    public $Password        = '147a@258';
    public $CPCode          = 'HUONGMINH';
    public $RequestID       = 0;
    const BALANCE_ALERT     = 10000000;

    // For new object
    /* Jul 14, 2014 Bên bạn xem có gửi được vào 2 ip này không nhé
    203.190.170.41:8998  2nd viettel serverbackup
    203.190.170.42:8998  3th viettel serverbackup 
    http://ams.tinnhanthuonghieu.vn:8009/bulkapi?wsdl
    */
//    public $Url = "http://125.235.4.202:8998/bulkapi?wsdl";// Primary Server Jun0119 run ok do cái 2nd bị lỗi 
//    public $Url = "http://203.190.170.41:8998/bulkapi?wsdl";// 2nd -- không connect được 41 + 42
    public $Url = "http://ams.tinnhanthuonghieu.vn:8009/bulkapi?wsdl";// 3nd -- add Jun119 - server cân bằng tải
//    public $Url = "http://203.190.170.48:8009/bulkapi?wsdl";// 4nd -- add Jun119 IP này là của domain ams.tinnhanthuonghieu.vn
    

    const ContentTypeKhongDau   = 0;
    const ContentTypeCoDau      = 1;
    // Jan 05, 2016
    const MAX_SEND              = 30; // giới hạn max gửi 1 lần
    const MAX_COUNT_RUN         = 3; // giới hạn max số lần gửi lại
    const MAX_SEND_QUOTES       = 3; // giới hạn max số lần gửi báo giá của KH bò mối 
    // tất cả sms sẽ đưa vào list chờ gửi hết, để không bị gửi sót cái nào, cac Type send
    const TYPE_UPHOLD                   = 1;
    const TYPE_UPHOLD_CUSTOMER          = 2;// JUN 10, 2016
    const TYPE_CREATE_STORE_CARD_WINDOW = 3;// Jul 19, 2016
    const TYPE_SETTLE_CASHIER_CONFIRM   = 4;// Jul 26, 2016 duyệt chi
    const TYPE_CUSTOMER_INFO_LOGIN      = 5;// Now 08, 2016 send info login to customer
    const TYPE_NOTIFY_G_PRICE_SETUP     = 6;// Feb 01, 2017 gửi tin confirm cho các trưởng phòng khi setup xong giá G
    const TYPE_FORGOT_PASS_CONFIRM      = 7;// Feb 05, 2017 customer gas24h forgot pass
    const TYPE_NEW_PASSWORD             = 8;// Feb 05, 2017 customer gas24h new password
    const TYPE_QUOTES_BO_MOI            = 9;// Feb 26, 2017 báo giá KH bò mối
    const TYPE_ANNOUNCE                 = 10;// Jun 21, 2017 Các thông báo đến KH
    const TYPE_ANNOUNCE_BOMOI_PAY_DEBIT = 11;// Feb0618 thông báo đã thu nợ KH bò mối
    const TYPE_TELESALE_CODE            = 12;// May0618 telesale gửi tin cài app đến KH
    const TYPE_ANNOUNCE_INTERNAL        = 13;// Jul0818 thông báo nội bộ
    const TYPE_GAS24H_PARTNER           = 14;// Now2218 code partner xăng, gạo ... 
    const TYPE_ANNOUNCE_BOMOI_DEBIT_L1    = 15;// May0619 sms thông báo đã thu tiền KH khi hoàn thành đơn bò mối L1
    const TYPE_HGD_SMS_POINT            = 16;// Jun1019 notify điểm tích lũy của KH 
    const TYPE_SPIN_NEW_NUMBER            = 17;// Sep2419 sms vé đối với KH gọi điện
    const TYPE_SPIN_WINNING_NUMBER        = 18;// Sep2419 sms thắng giải đối với KH gọi điện
    
    
    public static $aCodePhoneLandline   = array(2);// http://vietnamnet.vn/vn/cong-nghe/vien-thong/lich-chuyen-doi-sim-11-thanh-10-so-cua-viettel-mobifone-vinaphone-475945.html
    public static $aCodePhoneValidFirst = array(1, 9, 5, 3, 7, 8);// http://vietnamnet.vn/vn/cong-nghe/vien-thong/lich-chuyen-doi-sim-11-thanh-10-so-cua-viettel-mobifone-vinaphone-475945.html
    public static $aCodePhoneValidSecond = array(86,88,89);// di động của 88 - Vinaphone, 89 - MobiFone
    public static $aCodePhoneValidThree = array(868);// 868 - Viettel
    public static $aPhoneViettel        = array(39, 38, 37, 36, 35, 34, 33, 32,
        86, 96, 97, 98, 162, 163, 164, 165, 166, 167, 168 ,169);// Các đầu số di động của - Viettel
    public static $aPhoneMobi           = array(70, 79, 77, 76, 78,
        89, 90, 93, 120, 121, 122, 126, 128);// Các đầu số di động của - Mobifone
    public static $aPhoneVina           = array(83, 84, 85, 81, 82,
        88, 91, 94, 123, 124, 125, 127, 129);// Các đầu số di động của - Vinaphone
    public static $aPhoneVietNamMobile  = array(52, 56, 58, 
        92, 188, 186);// Các đầu số di động của - Vietnam Mobile
    public static $aPhoneGMobile        = array(59, 99, 199);// Các đầu số di động của - Vietnam Mobile
    public static $aLengthPhoneVietNam  = array(9, 10);// Mạng di động Việt Nam có 10, 11 số
    
    const WIDTH_SMS         = 159; //content of SMS, vì 160 ký tự là 1 tin, vượt quá thì bị sang tin thứ 2
    const AGENT_HOURS_CHECK = 17; // Aug 14, 2016 sau 17h kế toán về cho phép nhập số của giao nhận để nhận SMS
    public $network='', $gas24hPin = '', $aErrors = [], $mAppSignup = null;
    
    const NETWORK_VIETTEL           = 1;// mạng viettel
    const NETWORK_MOBI              = 2;// mạng mobile phone
    const NETWORK_VINA              = 3;
    const NETWORK_OTHER             = 4;// mạng khác
    const NETWORK_VIETNAM_MOBILE    = 5;
    const NETWORK_G_MOBILE          = 6;
    
    const QTY_NOMAL     = 1;// Mar3018 Sl ít
    const QTY_BIG       = 2;// sl nhiều
    
    /********************* @TEMPLATE_SMS  *******************************************
     * JUN 10, 2016
     *  1/  Su co: Xi Gas\nSDT: 132467589\nLien he: Nguyen Tien Dung
        2/  Dich Vu Bao Tri\nNhan vien: Dinh Trong Nghia\nSDT: 45123456789\nThoi gian den: 15:30 05-06-2016
    * JUN 10, 2016
    *********************************************************************************/
    
    public function getArraySendBigRecord() {
        return [
            GasScheduleSms::TYPE_QUOTES_BO_MOI,
            GasScheduleSms::TYPE_ANNOUNCE,
//            GasScheduleSms::TYPE_CUSTOMER_INFO_LOGIN,
        ];
    }    
 
    
    /**
     * @Author: ANH DUNG Jun 10, 2016
     * @Todo: insert to db
     * @Flow1: bao tri su co send SMS 
     * 1/ insert record to db 
     * 2/ cron send notify (every minutes) bình thường, send xong thì move notify sang history
     * @param: $uid_login user id create SMS
     * @param: $user_id user id receive SMS
     * @param: $phone Phone number receive 
     * @param: $type type of sms: 1: GasScheduleSms::TYPE_UPHOLD, 2: GasScheduleSms::TYPE_UPHOLD_CUSTOMER, 3: GasScheduleSms::TYPE_CREATE_STORE_CARD_WINDOW
     * @param: $content_type Loại nội dung gửi
                0: Tin nhắn nội dung không dấu.
     *          1: Tin nhắn nội dung có dấu.
     * @param: $obj_id  primary key liên quan, dựa vào $type để find ra model của PK này
     * @param: $time_send thời gian gửi
     * @param: $title nội dung gửi
     * @param: $json_var biến custom
     */
    public static function InsertRecord($uid_login, $user_id, $phone, $type, $content_type, $obj_id, $time_send, $title, $json_var) {
        $phone = trim($phone);
        if(!GasScheduleSms::isValidPhone($phone)){
//            Logger::WriteLog("Error SMS phone not valid: ".$phone);
            return '';
        }
        // Cần kiểm tra số máy bàn ở chỗ này
//        return ;// only for test debug
        if(empty($time_send)){
            $time_send = date('Y-m-d H:i:s');
        }
        $model = new GasScheduleSms();
        $model->uid_login       = $uid_login;
        $model->user_id         = $user_id;
        $model->phone           = "84".$phone;
        $model->type            = $type;
        $model->obj_id          = $obj_id;
        $model->content_type    = $content_type;
        $model->time_send       = $time_send;
        $title                  = strip_tags($title);
        $model->title           = $title;
        if($content_type == GasScheduleSms::ContentTypeKhongDau){
            $model->title = trim(MyFunctionCustom::remove_vietnamese_accents($title));
        }
        $model->username        = $model->rUser ? $model->rUser->username : "";
        $model->json_var        = json_encode($json_var);
        $model->save();
        return $model->id;
    }
    
    /** @Author: ANH DUNG Feb 06, 2018
     *  @note: fix hàm static InsertRecord bên trên
     */
    public function fixInsertRecord() {
        if(!GasScheduleSms::isValidPhone($this->phone)){
            return '';
        }
        if(empty($this->time_send)){
            $this->time_send = date('Y-m-d H:i:s');
        }
        $this->phone           = '84'.$this->phone;
        $this->username        = $this->rUser ? $this->rUser->username : '';
        $this->json_var        = json_encode($this->json_var);
        $this->save();
        return $this->id;
    }
    
    /** @Author: ANH DUNG Oct 06, 2018
     *  @Todo: check số bàn hợp lệ đầu 02
     **/
    public function isLandlinePhone($phone) {
        if(empty($phone)){
            return false;
        }
        if(in_array($phone[0], GasScheduleSms::$aCodePhoneLandline)){
            return true;
        }
        return false;
    }
    
    /**
     * @Author: ANH DUNG Jun 10, 2016
     * @Todo: kiểm tra số ĐT là di động hay máy bàn
     * nếu máy bàn thì ko gửi SMS dc
     * https://vi.wikipedia.org/wiki/M%C3%A3_%C4%91i%E1%BB%87n_tho%E1%BA%A1i_Vi%E1%BB%87t_Nam
     */
    public static function isValidPhone($phone) {
        if(empty($phone)){
            return false;
        }
        if(in_array($phone[0], GasScheduleSms::$aCodePhoneValidFirst)){
            return true;
        }
        // check 2 số đầu của mạng 88 - Vinaphone, 89 - MobiFone
        $result = substr($phone, 0, 2);
        if(in_array($result, GasScheduleSms::$aCodePhoneValidSecond)){
            return true;
        }
        // check 3 số đầu của 868 - Viettel
        $result = substr($phone, 0, 3);
        if(in_array($result, GasScheduleSms::$aCodePhoneValidThree)){
            return true;
        }

        return false;
    }
    
    /**
     * @Author: ANH DUNG Aug 13, 2016
     * @Todo: check xem số có phải là số điện thoại không
     */
    public function isPhoneRealLength(&$phone) {
        $phone = UsersPhone::formatPhoneSaveAndSearch($phone);
        $length = strlen($phone);
        if($phone > 3000000000){ // May2319 check user nhập số trên 2 tỉ 02836 220196 -- 3 tỉ
            $this->aErrors[] = 'Số điện thoại không hợp lệ, gõ thừa số';
            return false;
        }
        if(!in_array($length, GasScheduleSms::$aLengthPhoneVietNam)){
            $this->aErrors[] = 'Số điện thoại không hợp lệ, gõ thiếu hoặc thừa số';
            return false;
        }
        return true;
    }
    
    /**
     * @Author: ANH DUNG Aug 13, 2016
     * @Todo: check xem số có phải Số di động của các mạng vietnam ko
     */
    public function isPhoneVietnam($phone, $aDauSo) {
        if(!$this->isPhoneRealLength($phone)){
            return false;
        }
        // check 2 số đầu của mạng 86 - viettel, 88 - Vinaphone, 89 - MobiFone
        $result = substr($phone, 0, 2);
        if(in_array($result, $aDauSo)){
            return true;
        }
        // check 3 số đầu của phone
        $result = substr($phone, 0, 3);
        if(in_array($result, $aDauSo)){
            return true;
        }
        $this->aErrors[] = "Số điện thoại không hợp lệ";
        return false;
    }
    
    /**
     * @Author: ANH DUNG Aug 13, 2016
     * @Todo: check xem số có phải viettel ko
     */
    public function isPhoneViettel($phone) {
        return $this->isPhoneVietnam($phone, GasScheduleSms::$aPhoneViettel);
    }
    /**
     * @Author: ANH DUNG Aug 13, 2016
     * @Todo: check xem số có phải Vina ko
     */
    public function isPhoneVina($phone) {
        return $this->isPhoneVietnam($phone, GasScheduleSms::$aPhoneVina);
    }
    /**
     * @Author: ANH DUNG Aug 13, 2016
     * @Todo: check xem số có phải Mobi ko
     */
    public function isPhoneMobi($phone) {
        return $this->isPhoneVietnam($phone, GasScheduleSms::$aPhoneMobi);
    }
    public function isPhoneVietNamMobile($phone) {
        return $this->isPhoneVietnam($phone, GasScheduleSms::$aPhoneVietNamMobile);
    }
    public function isPhoneGMobile($phone) {
        return $this->isPhoneVietnam($phone, GasScheduleSms::$aPhoneGMobile);
    }
    
    /**
     * @Author: ANH DUNG Aug 14, 2016
     * @Todo: check xem số có phải viettel ko, chỉ cho đại lý nhập số viettel từ 0h -> 18h, sau 18h ko check viettel nữa
     */
    public function isAgentPhoneValid() {
        $cHours = date("H");
        if($cHours > 3 && $cHours < GasScheduleSms::AGENT_HOURS_CHECK ) { // Từ 3h sáng đến 18h tối sẽ check số viettel của đại lý
            return $this->isPhoneVietnam($this->phone, GasScheduleSms::$aPhoneViettel);
        }
        // Sau 18h tối sẽ cho phép nhập các mạng khác 
        return true;
    }
    
    /**
     * @Author: ANH DUNG Jun 10, 2016
     * @Todo: send SMS to Viettel
     */
    public function doSend() {
//        return ; // Jan 10, 2017 only for dev stop send
        $ReceiverID     = $this->phone;
        $Content        = $this->title;
        $ContentType    = $this->content_type;// Mar1819 od mạng Vietnam Mobile chưa hỗ trợ có dấu nên không gửi đc tin => phải set lại như cũ hết
//        $ContentType    = GasScheduleSms::ContentTypeCoDau;// Mar1419 change all to send sms co dau
        $user_param = array (
            'User'      => $this->User,
            'Password'  => $this->Password,
            'CPCode'    => $this->CPCode,
            'RequestID' => $this->RequestID,
            'UserID'    => $ReceiverID,
            'ReceiverID'    => $ReceiverID,
            'ServiceID'     => $this->ServiceID,
            'CommandCode'   => $this->CommandCode,
            'Content'       => $Content,
            'ContentType'   => $ContentType,
        );
        $client = $this->sending($user_param, "wsCpMt");
        // move luôn sang history
        GasScheduleSmsHistory::InsertNew($this);
    }
    
    /**
     * @Author: ANH DUNG Jun 16, 2016
     */
    public function sending($user_param, $functionCall) {
        $client = new SoapClient($this->Url, array("soap_version" => SOAP_1_1,"trace" => 1));
        $client->__soapCall(
           $functionCall,
           array($user_param)
        );
        return $client;
    }
    
        /**
     * @Author: ANH DUNG Jun 16, 2016
     * @Todo: checkBalance 
     */
    public function checkBalance() {
        $user_param = array (
            'User'      => $this->User,
            'Password'  => $this->Password,
            'CPCode'    => $this->CPCode
        );
        $client = $this->sending($user_param, "checkBalance");
        $soapResponse = $client->__getLastResponse(); // $soapResponse response
        $response = preg_replace("/(<\/?)(\w+):([^>]*>)/", "$1$2$3", $soapResponse);
        $xml = new SimpleXMLElement($response);
        $body = $xml->xpath('//SBody')[0];
        $array = json_decode(json_encode((array)$body), TRUE); 
        return isset($array['ns2checkBalanceResponse']['return']['balance'])?$array['ns2checkBalanceResponse']['return']['balance']:"Error";
    }
    
    /** @Author: ANH DUNG Mar 30, 2018
     *  @Todo: tách runCronNomal gửi những SMS SL ít, gửi ngay
     **/
    public function runCronNomal() {
        $data = $this->getDataCron(GasScheduleSms::QTY_NOMAL);
        $this->runCron($data);
    }
    public function runCronBig() {// gửi những SMS SL nhiều
        $data = $this->getDataCron(GasScheduleSms::QTY_BIG);
        $this->runCron($data);
    }
    
    /** @Author: ANH DUNG Jan 05, 2016
     *  @Todo: run cron notify SMS
     */
    public function runCron($data) {
        $from = time();
        if(count($data) < 1 ){
            return ;
        }
        foreach($data as $mScheduleSms){
            switch ($mScheduleSms->type){
                case GasScheduleSms::TYPE_UPHOLD:
                case GasScheduleSms::TYPE_UPHOLD_CUSTOMER:
                case GasScheduleSms::TYPE_CREATE_STORE_CARD_WINDOW:
                case GasScheduleSms::TYPE_SETTLE_CASHIER_CONFIRM:
                case GasScheduleSms::TYPE_NOTIFY_G_PRICE_SETUP:
                case GasScheduleSms::TYPE_QUOTES_BO_MOI:
                case GasScheduleSms::TYPE_ANNOUNCE:
                case GasScheduleSms::TYPE_ANNOUNCE_BOMOI_PAY_DEBIT:
                case GasScheduleSms::TYPE_TELESALE_CODE:
                case GasScheduleSms::TYPE_ANNOUNCE_INTERNAL:
                case GasScheduleSms::TYPE_GAS24H_PARTNER:
                case GasScheduleSms::TYPE_ANNOUNCE_BOMOI_DEBIT_L1:
                case GasScheduleSms::TYPE_HGD_SMS_POINT:
                case GasScheduleSms::TYPE_SPIN_NEW_NUMBER:
                case GasScheduleSms::TYPE_SPIN_WINNING_NUMBER:
                    $mScheduleSms->doSend();
                    break;
//                case GasScheduleNotify::UPHOLD_DINH_KY_1_DAY:
//                    self::HandleAlertDinhKy($mScheduleSms);
//                    break;
            }
        }
        $to = time();
        $second = $to-$from;
        $CountData = count($data);
        $ResultRun = "CRON Notify SMS: ".$CountData.' done in: '.($second).'  Second  <=> '.round($second/60, 2).' Minutes ';
        $ResultRun .= json_encode(CHtml::listData($data, 'id', 'phone'));
        if($CountData){
//            Logger::WriteLog($ResultRun);// Feb0718 close
        }
    }

    /**
     * @Author: ANH DUNG Jan 05, 2016
     * @Todo: get data thỏa mãn cron để send
     */
    public function getDataCron($qtyType) {
        $minute = 1;// Jul 19, 2016 xử lý gửi lại những sms bị gửi lỗi do kết nối mạng // Phải cộng thêm 2 phút để tránh lấy phải những record mới sinh ra sẽ không send 2 lần
        $criteria = new CDbCriteria();
//        $criteria->addCondition("DATE_ADD(time_send, INTERVAL $minute MINUTE) < NOW()");// Close Feb0518 không cần thiết phải cộng thêm $minute này vào. cứ gửi luôn
//        $criteria->addCondition("t.count_run < ".self::MAX_COUNT_RUN);// Close Feb0518 không cần thiết phải cộng thêm $minute này vào. cứ gửi luôn
//        $criteria->addCondition("t.time_send <= NOW() AND t.count_run < ".self::MAX_COUNT_RUN);// Close Mar3018 nghi ngờ có thể bị gửi 2 lần => khá khó xảy ra
        $criteria->addCondition("t.time_send <= NOW()");// Open Jun0119
        $criteria->order = 't.count_run ASC, t.id ASC';
        $criteria->limit = 150;
        $sParamsIn = implode(',',  $this->getArraySendBigRecord());
        if($qtyType == GasScheduleSms::QTY_NOMAL){
            $criteria->addCondition("t.type NOT IN ($sParamsIn)");
        }elseif($qtyType == GasScheduleSms::QTY_BIG){
            $criteria->addCondition("t.type IN ($sParamsIn)");
        }

        return self::model()->findAll($criteria);
    }

    /**
     * @Author: ANH DUNG Dec 22, 2015
     * @Todo: cộng số lần run của notify thêm 1
     */
    public function plusRun() {
        $this->count_run += 1;
        $this->update(array('count_run'));
    }

    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
            return '{{_gas_schedule_sms}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        return array(
            array('id, user_id, type, obj_id, time_send, created_date, username, title, json_var, count_run', 'safe'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        return array(
            'rUser' => array(self::BELONGS_TO, 'Users', 'user_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'user_id' => 'User',
            'type' => 'Type',
            'obj_id' => 'Obj',
            'time_send' => 'Time Send',
            'created_date' => 'Created Date',
            'username' => 'Username',
            'title' => 'Title',
            'json_var' => 'Json Var',
            'count_run' => 'Count Run',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        $criteria=new CDbCriteria;
        $criteria->compare('t.user_id',$this->user_id);
        $criteria->compare('t.type',$this->type);
        $criteria->compare('t.username',$this->username);
        $criteria->order = 't.id DESC';
        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
            'pagination'=>array(
                'pageSize'=> Yii::app()->user->getState('pageSize',Yii::app()->params['defaultPageSize']),
            ),
        ));
    }
    
    public function getUserReceive() {
        $mUser = $this->rUser;
        if($mUser){
            return $mUser->getFullName();
        }
        return '';
    }
    public function getType() {
        $mSmsHistory = new GasScheduleSmsHistory();
        $aType = $mSmsHistory->getArrayType();
        return isset($aType[$this->type]) ? $aType[$this->type] : '';
    }
    
    /**
     * @Author: ANH DUNG Jan 05, 2016
     */
    public function buildStringParam($aParams, &$fields_string) {
        //url-ify the data for the GET
        foreach($aParams as $key=>$value) 
        { 
            $fields_string .= $key.'='.$value.'&'; 
            
        }
        $fields_string = rtrim($fields_string, '&');
    }
    
    /**
     * @Author: ANH DUNG Jan 05, 2016
     * @Todo: send SMS 
     * @Param: string $resource : SendMultipleSMS_v3
     * http://api.esms.vn/MainService.svc/xml/SendMultipleSMS_v3
?Phone={Phone}&Content={Content}&ApiKey={ApiKey}&
         * SecretKey={SecretKey}&IsUnicode={IsUnicode}&
         * Brandnamme={BrandnameCode}&SmsType={SmsType}&RequestID={RequestID}
     */
    public function sendSmsTest($aPhoneNumber, $Content) {
        $from = time();
//        $aPhoneNumber = array(841684331552, 841689217685, 84979120291);
//        $aPhoneNumber = array(841684331552, 84988180386, 84982843001);
        // Giám Đốc Long Viettel 0968 318 328
//        $aPhoneNumber = array(841684331552, 84968318328);// VIETTEL ANH LONG
//        $aPhoneNumber = array(84384331552, 84918318328);// ko send dc số máy bàn
//        if(isset($_GET['test_sms']) && $_GET['test_sms'] > 1000 ){
//            $aPhoneNumber = array($_GET['test_sms']);// ko send dc số máy bàn
//        }
        
//        $aPhoneNumber = array(841647793754);// ko send dc số máy bàn
//        $aPhoneNumber = array(841289820365);// MOBI sim 2 máy philip
//        $aPhoneNumber = array(841207655321);// MOBI sim may Samsung => khong gui dc
//        $aPhoneNumber = array(841684331552,84918318328);// VINA ANH LONG
//        $aPhoneNumber = array(84918975768);// VINA KIÊN
//        $Content = "line 1 dev test \n line 2 tin nhan \n line 3 khong dau";
//        $Content = "Su co: Xi Gas\nSDT: 132467589\nLien He: Nguyen Tien Dung";
//        $aPhoneNumber = array(84939180386);// MOBI KIEN
        $date = "Ngay: ".date("H:i d/m/y");
//        $Content = "DV Bao Tri Mien Phi\nNV: Dinh Trong Nghia\n$date time = ". time();
//        $Content = "CTy CP Dau Khi Mien Nam tran trong gui den quy khach hang gia Gas 03/2017\nBinh 45kg: 23,560 d/kg\nBinh 12kg 350,000 d/binh\nAp dung tu: 01/03/2017 - 31/03/2017";
//        $Content = "DV Bao Tri Mien Phi 24/7\nNV: Nguyen Phan Minh Quang\nGoi NVBT: 0988180386\nThoi gian den: 15:20\nChi cho MA BAO TRI 123456 khi NV hoan thanh tot\nKhong hai long DVBT goi: 0918318328";
        
        $Content = strip_tags($Content);
//        $Content = "Kính Gửi Tổng Giám Đốc Vũ Thái Long\n Phòng IT báo cáo đã cài đặt SMS cho mạng Viettel\nCòn Mạng Vina và Mobi trong tuần sau sẽ setup xong. ";
        $client = new SoapClient($this->Url, array("soap_version" => SOAP_1_1,"trace" => 1));
//        $ContentType = 0;// không dấu
        $ContentType = 1; // tiếng việt có dấu
        foreach($aPhoneNumber as $ReceiverID){
            $user_param = array (
            'User' => $this->User,
            'Password' => $this->Password,
            'CPCode' => $this->CPCode,
            'RequestID' => $this->RequestID,
            'UserID' => $ReceiverID,
            'ReceiverID' => $ReceiverID,
            'ServiceID' => $this->ServiceID,
            'CommandCode' => $this->CommandCode,
            'Content' => $Content,
//            'ContentType' => $this->ContentType,
            'ContentType' => $ContentType,
          );
//        print_r(
           $client->__soapCall(
               "wsCpMt",
               array($user_param)
           );
//        );
        }
        
        echo $client->__getLastResponse();
//        echo '<pre>';
//        print_r($client->__getLastRequest());
//        echo '</pre>';
//        echo '<pre>';
//        print_r($client->__getFunctions());
//        echo '</pre>';
        $to = time();
        $second = $to-$from;
        echo count($aPhoneNumber).' done in: '.($second).'  Second  <=> '.($second/60).' Minutes';die;
    }
    
    
    
    /**
     * @Author: ANH DUNG Jul 19, 2016
     * @Todo: send sms cho user đại lý khi điều phối tạo thẻ kho
     * @Param: $mStoreCard model store card
     */
    public static function storeCardCreate($mStoreCard) {
        try{
        /* 1. get số phone của đại lý
         * 2. build content sms
         * 3. insert and send
         */
        $mAgent = Users::model()->findByPk($mStoreCard->user_id_create);
        if(is_null($mAgent) || empty($mAgent->phone)){
            Logger::WriteLog("Không gửi được SMS. SMS empty agent phone : ID $mAgent->id");
            throw new Exception("Không gửi được SMS cho {$mAgent->getFullName()}. Tạo mới thẻ kho thành công.");
            return ;
        }
        $title = MyFunctionCustom::remove_vietnamese_accents(self::storeCardGetMsg($mStoreCard));
        $title = MyFunctionCustom::ShortenStringSMS($title, GasScheduleSms::WIDTH_SMS);
        $user_id = $mStoreCard->user_id_create;
        $phone = UsersPhone::getPhoneUser($user_id);
        
        $schedule_sms_id = GasScheduleSms::InsertRecord($mStoreCard->uid_login, $user_id, $phone, GasScheduleSms::TYPE_CREATE_STORE_CARD_WINDOW, GasScheduleSms::ContentTypeKhongDau, $mStoreCard->id, "", $title, array(''));
        if(empty($schedule_sms_id)){
            Logger::WriteLog("SMS empty id: ");
            return ;
        }
        // run insert xong thì gửi luôn cho nv bảo trì
        $mScheduleSms = GasScheduleSms::model()->findByPk($schedule_sms_id);
        $mScheduleSms->doSend();
        } catch (Exception $ex) {
            throw new Exception($ex->getMessage());
        }
    }
    
    
    /**
     * @Author: ANH DUNG Jul 19, 2016
     * @Todo: get sms content notify to sub user agent
     */
    public static function storeCardGetMsg($mStoreCard) {
        // Template 1/  Su co: Xi Gas\nSDT: 132467589\nLien he: Nguyen Tien Dung
        $note = nl2br($mStoreCard->note);
        $note = strip_tags($note);
        $customerHouseNumbers = "";
        $customerName = "";
        $mCustomer = $mStoreCard->rCustomer;
        if($mCustomer){
            $customerName           = $mCustomer->getFullName();
            $customerHouseNumbers   = $mCustomer->house_numbers.", ".$mCustomer->getStreet();
        }
//        $date = "Ngay: ".date("H:i d/m/y");
        return "DH\n$note\n$customerName\n$customerHouseNumbers";
    }
    
    /**
     * @Author: ANH DUNG Jul 26, 2016
     * @Todo: test settle
     */
    public static function settleTestSMS() {
        $model = GasSettle::model()->findByPk(14);
        GasScheduleSms::settleCashierConfirm($model);die;
    }
    
    /**
     * @Author: ANH DUNG Jul 26, 2016
     * @Todo: send sms cho anh Long Khi thủ quỹ chi tiền
     * @Param: $mSettle model Settle
     */
    public static function settleCashierConfirm($mSettle) {
        return ;// Close on Aug3018
        try{
        /* 1. get số phone của Anh Long
         * 2. build content sms
         * 3. insert and send
         */
        $title = MyFunctionCustom::remove_vietnamese_accents(self::settleCashierConfirmMsg($mSettle));
        $title = MyFunctionCustom::ShortenStringSMS($title, GasScheduleSms::WIDTH_SMS);
//        $aUid = array(GasLeave::UID_DIRECTOR, GasLeave::UID_DUNG_NT);// Jul 26, 2016 send cho Dũng để check SMS send
        $aUid = array(GasLeave::UID_DIRECTOR);// Jul 26, 2016 send cho Dũng để check SMS send
        foreach($aUid as $user_id){
            $phone = UsersPhone::getPhoneUser($user_id);
            $schedule_sms_id = GasScheduleSms::InsertRecord($mSettle->uid_cashier_confirm, $user_id, $phone, GasScheduleSms::TYPE_SETTLE_CASHIER_CONFIRM, GasScheduleSms::ContentTypeKhongDau, $mSettle->id, "", $title, array(''));
            if(empty($schedule_sms_id)){
                Logger::WriteLog("SMS empty id: ");
                return ;
            }
            // run insert xong thì gửi luôn
            $mScheduleSms = GasScheduleSms::model()->findByPk($schedule_sms_id);
            $mScheduleSms->doSend();
        }
        } catch (Exception $ex) {
            GasCheck::CatchAllExeptiong($ex);
        }
    }
    
    /**
     * @Author: ANH DUNG Jul 26, 2016
     * @Todo: get sms content notify to Vu Thai Long
     */
    public static function settleCashierConfirmMsg($mSettle) {
        // Template 1/  Su co: Xi Gas\nSDT: 132467589\nLien he: Nguyen Tien Dung
        $msg = "";
        if( $mSettle->type == GasSettle::TYPE_AFTER_SETTLE ){
            $action = "";
            $deltaAmount = $mSettle->getNameActionSettle($action);
            $deltaAmount = abs($deltaAmount);// echo abs(-4.2); // 4.2 (double/float)
            $msg  .= "$action ".ActiveRecord::formatCurrency($deltaAmount);
            if(!empty($mSettle->mParent) && !is_null($mSettle->mParent)){
                $msg  .= "\nTam ung: ".$mSettle->mParent->getAmount();
            }
            $msg  .= "\nQuyet toan: ".$mSettle->getAmount();
        }else{
            $msg  .= "Chi ".$mSettle->getAmount();
            $msg .= "\n".$mSettle->getTypeText();
        }
        $msg .= "\nNV:".$mSettle->getUidLoginName();
        $msg .= "\n".$mSettle->getCompanyOnlyName();
        $msg = strip_tags($msg);
        return $msg;
    }
    
    /**
     * @Author: ANH DUNG Nov 08, 2016
     * @Todo: gửi cho list user dev test
     */
    public static function sendDevTest($type, $content_type, $obj_id, $time_send, $title, $json_var) {
        try{
//        $aUid = array(GasLeave::UID_DIRECTOR, GasLeave::UID_DUNG_NT);// Jul 26, 2016 send cho Dũng để check SMS send
        $aUid = array(GasLeave::UID_DUNG_NT);// Jul 26, 2016 send cho Dũng để check SMS send
        foreach($aUid as $user_id){
            $phone = UsersPhone::getPhoneUser($user_id);
            $schedule_sms_id = GasScheduleSms::InsertRecord(GasConst::UID_ADMIN, $user_id, $phone, $type, $content_type, $obj_id, $time_send, $title, $json_var);
            if(empty($schedule_sms_id)){
                Logger::WriteLog("SMS empty id: ");
                return ;
            }
            // run insert xong thì gửi luôn
            $mScheduleSms = GasScheduleSms::model()->findByPk($schedule_sms_id);
            $mScheduleSms->doSend();
        }
        } catch (Exception $ex) {
            GasCheck::CatchAllExeptiong($ex);
        }
    }
    
    /**
     * @Author: ANH DUNG Now 08, 2016
     * @Todo: send sms cho customer Username + Password login appp
     * @Param: $mSettle model Settle
     */
    public static function customerInfoLoginApp($mSignup) {
        try{
        /* 1. build content sms
         * 2. insert and Send OR put to Cron
         */
        $title = MyFunctionCustom::remove_vietnamese_accents(self::customerInfoLoginAppMsg($mSignup));
        $title = MyFunctionCustom::ShortenStringSMS($title, GasScheduleSms::WIDTH_SMS);
        $json_var       = array('first_name'=>$mSignup->first_name);
        $phoneNumberSend = UsersPhone::formatPhoneSaveAndSearch($mSignup->phone);
        $schedule_sms_id = GasScheduleSms::InsertRecord(GasConst::UID_ADMIN, 
                '', $phoneNumberSend, GasScheduleSms::TYPE_CUSTOMER_INFO_LOGIN, GasScheduleSms::ContentTypeKhongDau, $mSignup->id, "", $title, $json_var);
        if(empty($schedule_sms_id)){
            Logger::WriteLog("SMS empty id: ");
            return ;
        }
        // run insert xong thì gửi luôn
        $mScheduleSms = GasScheduleSms::model()->findByPk($schedule_sms_id);
        $mScheduleSms->doSend();
        
//        self::sendDevTest(GasScheduleSms::TYPE_CUSTOMER_INFO_LOGIN, GasScheduleSms::ContentTypeKhongDau, $mSignup->id, "", $title, array(''));
        } catch (Exception $ex) {
            GasCheck::CatchAllExeptiong($ex);
        }
    }
    
    public static function customerInfoLoginAppMsg($mSignup) {
        // Template 1/  Xin chao: Nguyen Nguyen Tien Dung\nThong tin dang nhap App cua ban la:\nTai khoan: 01684331552\nMat khau: 456789\nTran trong cam on
         $msg = '';
//        $msg .= "Xin chao: '.$mUser->first_name;
//        $msg .= "\nThong tin dang nhap App cua ban la: ";
        $msg .= "Ma xac thuc cua Quy Khach: $mSignup->confirm_code";
        $msg .= "\nThong tin dang nhap APP:";
        $msg .= "\nTai khoan: $mSignup->phone";
        $msg .= "\nMat khau: $mSignup->password";
        $msg .= "\nTran trong cam on!";
        return $msg;
    }
    
    /**
     * @Author: ANH DUNG Feb 31, 2017
     * @Todo: send sms cho cấp quản lý check lại giá Bò mối khi setup
     * @Param: $mGasPrice model GasPrice
     */
    public static function notifyGasPriceSetup($mGasPrice) {
        try{
        /* 1. build content sms
         * 2. insert and Send OR put to Cron
         */
        $title = $mGasPrice->notifyGasPriceSetupMsg();
        if(empty($title)){
            return ;
        }
        $title = MyFunctionCustom::remove_vietnamese_accents($title);
        $title = MyFunctionCustom::ShortenStringSMS($title, GasScheduleSms::WIDTH_SMS);
        
        $aUid = array(GasLeave::UID_DIRECTOR_SMS_VINAPHONE, GasLeave::UID_DIRECTOR_BUSSINESS, GasLeave::UID_HEAD_GAS_BO,
            GasLeave::UID_DUNG_NT);
//        $aUid = array(GasLeave::UID_DIRECTOR_BUSSINESS, GasLeave::UID_HEAD_OF_LEGAL, GasLeave::UID_DUNG_NT);
//        $aUid = array(GasLeave::UID_DUNG_NT);// Jul 26, 2016 send cho Dũng để check SMS send
        foreach($aUid as $user_id){
            $json_var       = array();
            $phone = UsersPhone::getPhoneUser($user_id);
            $schedule_sms_id = GasScheduleSms::InsertRecord(GasConst::UID_ADMIN, 
                    $user_id, $phone, GasScheduleSms::TYPE_NOTIFY_G_PRICE_SETUP, GasScheduleSms::ContentTypeKhongDau, $mGasPrice->id, '', $title, $json_var);
            if(empty($schedule_sms_id)){
                Logger::WriteLog("SMS empty id: ");
                return ;
            }
            // Feb 01, 2017 tạm close lại, để chạy = cron, không cần thiết phải send luôn run insert xong thì gửi luôn
//            $mScheduleSms = GasScheduleSms::model()->findByPk($schedule_sms_id);
//            $mScheduleSms->doSend();
        }
        
//        self::sendDevTest(GasScheduleSms::TYPE_CUSTOMER_INFO_LOGIN, GasScheduleSms::ContentTypeKhongDau, $mSignup->id, "", $title, array(''));
        } catch (Exception $ex) {
            GasCheck::CatchAllExeptiong($ex);
        }
    }
    
    /**
     * @Author: ANH DUNG Feb 05, 2017
     * @Todo: xử lý send SMS confirm code forgot pass
     */
    public function customerForgotPass($mSignup) {
        try{
        /* 1. build content sms
         * 2. insert and Send OR put to Cron
         */
        $json_var       = [];
        $this->customerForgotPassGetTitle($mSignup);
        $phoneNumberSend = UsersPhone::formatPhoneSaveAndSearch($mSignup->phone);
        $schedule_sms_id = GasScheduleSms::InsertRecord(GasConst::UID_ADMIN, 
                '', $phoneNumberSend, GasScheduleSms::TYPE_FORGOT_PASS_CONFIRM, GasScheduleSms::ContentTypeKhongDau, $mSignup->id, "", $this->title, $json_var);
        if(empty($schedule_sms_id)){
            Logger::WriteLog("SMS empty id: ");
            return ;
        }
        // run insert xong thì gửi luôn
        $mScheduleSms = GasScheduleSms::model()->findByPk($schedule_sms_id);
        $mScheduleSms->doSend();
        
//        self::sendDevTest(GasScheduleSms::TYPE_FORGOT_PASS_CONFIRM, GasScheduleSms::ContentTypeKhongDau, $mSignup->id, "", $this->title, array(''));
        } catch (Exception $ex) {
            GasCheck::CatchAllExeptiong($ex);
        }
    }
    
    public function customerForgotPassGetTitle($mSignup) {
        // Template 1/  Xin chao: Nguyen Nguyen Tien Dung\nThong tin dang nhap App cua ban la:\nTai khoan: 01684331552\nMat khau: 456789\nTran trong cam on
         $title = '';
//        $msg .= "Xin chao: '.$mUser->first_name;
//        $msg .= "\nThong tin dang nhap App cua ban la: ";
        $title .= "Ma xac thuc cua Quy Khach: $mSignup->confirm_code";
        $title = MyFunctionCustom::remove_vietnamese_accents($title);
        $title = MyFunctionCustom::ShortenStringSMS($title, GasScheduleSms::WIDTH_SMS);
        $this->title = $title;
    }
    
    /**
     * @Author: ANH DUNG Feb 05, 2017
     * @Todo: xử lý send SMS new pass for user
     */
    public function customerNewPass($mSignup) {
        try{
        /* 1. build content sms
         * 2. insert and Send OR put to Cron
         */
        $json_var       = [];
        $this->customerNewPassGetTitle($mSignup);
        $phoneNumberSend = UsersPhone::formatPhoneSaveAndSearch($mSignup->phone);
        $schedule_sms_id = GasScheduleSms::InsertRecord(GasConst::UID_ADMIN, 
                '', $phoneNumberSend, GasScheduleSms::TYPE_NEW_PASSWORD, GasScheduleSms::ContentTypeKhongDau, $mSignup->id, "", $this->title, $json_var);
        if(empty($schedule_sms_id)){
            Logger::WriteLog("SMS empty id: ");
            return ;
        }
        // run insert xong thì gửi luôn
        $mScheduleSms = GasScheduleSms::model()->findByPk($schedule_sms_id);
        $mScheduleSms->doSend();
        
//        self::sendDevTest(GasScheduleSms::TYPE_NEW_PASSWORD, GasScheduleSms::ContentTypeKhongDau, $mSignup->id, "", $this->title, array(''));
        } catch (Exception $ex) {
            GasCheck::CatchAllExeptiong($ex);
        }
    }
    
    public function customerNewPassGetTitle($mSignup) {
        // Template 1/  Xin chao: Nguyen Nguyen Tien Dung\nThong tin dang nhap App cua ban la:\nTai khoan: 01684331552\nMat khau: 456789\nTran trong cam on
         $title = '';
//        $msg .= "Xin chao: '.$mUser->first_name;
//        $msg .= "\nThong tin dang nhap App cua ban la: ";
        $title .= "Mat khau moi cua Quy Khach la: $mSignup->password";
        $title = MyFunctionCustom::remove_vietnamese_accents($title);
        $title = MyFunctionCustom::ShortenStringSMS($title, GasScheduleSms::WIDTH_SMS);
        $this->title = $title;
    }
    
    /**
     * @Author: ANH DUNG Feb 26, 2017
     * @Todo: is array model UsersPhoneExt2
     */
    public function doInsertMultiQuotes($aModelPhone) {
        /** 1. lâý all giá KH bò mối của tháng hiện tại, nếu ko có thì return
         *  2. foreach $aModelPhone để build sql, không cần find model User vì SMS này không có tên KH
         */
        $month = date('m'); $year = date('Y');
        $gPriceInMonth = UsersPrice::getPriceOfListCustomer($month, $year, []);
        if(count($gPriceInMonth) < 1){
            $info = "Error: doInsertMultiQuotes Chua co setup gia G cua thang $month - $year";
            Logger::WriteLog($info);
            SendEmail::bugToDev($info);
            die($info);
        }
        /** xử lý find model user để lấy company của các KH đưa vào câu chào của SMS */
        
        $aCompanyAllow  = [GasConst::COMPANY_DKMN, GasConst::COMPANY_HUONGMINH];
        $aCustomerHM    = UsersExtend::getCustomerByCompany(GasConst::COMPANY_HUONGMINH);
        
        $aRowInsert = $aErrors = []; $uid_login = GasConst::UID_ADMIN;
        $type = GasScheduleSms::TYPE_QUOTES_BO_MOI; $username = $obj_id = $json_var = ''; $count_run = 0;
        $time_send = date('Y-m-d H:i:s');
        $content_type = GasScheduleSms::ContentTypeKhongDau;
        foreach($aModelPhone as $mPhoneCustomer){
            $companyName = 'CTy CP Dau Khi Mien Nam';
            if(isset($aCustomerHM[$mPhoneCustomer->user_id]) && $aCustomerHM[$mPhoneCustomer->user_id]['storehouse_id'] == GasConst::COMPANY_HUONGMINH){
                $companyName = 'CTy Gas Huong Minh';
            }
            $title = '';
            $phone = '84'.$mPhoneCustomer->phone;
            $valid = $this->doInsertMultiQuotesGetMsg($companyName, $month, $year, $mPhoneCustomer, $gPriceInMonth, $title);
            if(!$valid || !isset($gPriceInMonth[$mPhoneCustomer->user_id])){
                $aErrors[] = $mPhoneCustomer->user_id;
                continue ;
            }
            $aRowInsert[] = "('$uid_login',
                '$phone',
                '$mPhoneCustomer->user_id',
                '$username',
                '$type',
                '$obj_id',
                '$title',
                '$json_var',
                '$count_run',
                '$time_send',
                '$content_type'
            )";
        }
        $this->doInsertMultiQuotesSave($aRowInsert);
        
        $gPriceInMonth = $aCustomerHM = $aRowInsert = null;
//        $aModelCustomer = $aCompany = null;
        
        if(count($aErrors)){
            $info = "Lỗi gửi báo giá SMS doInsertMultiQuotes, những KH sau chưa được setup giá để gửi báo giá:  ". implode(', ', $aErrors);
            Logger::WriteLog($info);
            SendEmail::bugToDev($info);
        }
    }
    
    /** @Author: ANH DUNG Feb 03, 2018
     *  @Todo: ghi lai template SMS cua KH
     **/
    public function writeTemplateSMSCustomer() {
        /** @T1. SMS báo giá bò mối 
         * CTy CP Dau Khi Mien Nam tran trong gui den quy khach hang gia Gas 02/2018\nBinh 50kg: 22,829 d/kg\nBinh 12kg: 311,000 d/binh\nChi tiet lien he: 19001521
         * 
         *  @T2: announceBoMoiPayDebit SMS thông báo thu tiền công nợ KH bò mối
         * Quy Khach da thanh toan 150,000,000 tien Gas
         * \nMa khach hang: NGUYEN00501
         * \nNgay thanh toan: 15/02/2018
         * \nChi tiet lien he: 19001521
         * 
         * @T3: Telesale send code to KH HGD
         * Ma khuyen mai: 8681
            \nTai app Android: http://bit.ly/AndroidGas24h va IOS: http://bit.ly/iOSGas24h
            \nWeb: http://gas24h.com.vn
            \nChi tiet lien he: 19001565
         * 
         */
    }
    
    public function doInsertMultiQuotesGetMsg($companyName, $month, $year, $mPhoneCustomer, $gPriceInMonth, &$title) {
        /* thêm tên công ty vào báo giá
        * Kinh gui quy khach gia gas thang 03-2017\nBinh 50kg: 23,560\nBinh 12kg 350,000
         * CTy CP Dau Khi Mien Nam tran trong gui den quy khach hang gia Gas 03/2017\nBinh 45kg: 23,560 d/kg\nBinh: 12kg 350,000 d/binh\nAp dung den 31/03/2017
         * CTy CP Dau Khi Mien Nam tran trong gui den quy khach hang gia Gas 03/2017\nBinh 45kg: 23,560 d/kg\nBinh 12kg 350,000 d/binh\nAp dung tu: 01/03/2017 - 31/03/2017
         * CTy CP Dau Khi Mien Nam tran trong gui den quy khach hang gia Gas 02/2018 Binh 50kg: 22,829 d/kg Binh 12kg: 311,000 d/binh Chi tiet lien he: 19001521
        */ 
        $valid = false;
        $title = "$companyName tran trong gui den Quy Khach Hang gia Gas $month/$year";
        if(isset($gPriceInMonth[$mPhoneCustomer->user_id][UsersPrice::PRICE_B_50]) && $gPriceInMonth[$mPhoneCustomer->user_id][UsersPrice::PRICE_B_50] > 0){
            $title .= "\nBinh 45kg+50kg: ". ActiveRecord::formatCurrency($gPriceInMonth[$mPhoneCustomer->user_id][UsersPrice::PRICE_B_50])." d/kg";
            $valid = true;
        }
        if(isset($gPriceInMonth[$mPhoneCustomer->user_id][UsersPrice::PRICE_B_12]) && $gPriceInMonth[$mPhoneCustomer->user_id][UsersPrice::PRICE_B_12] > 0){
            $priceB12 = $gPriceInMonth[$mPhoneCustomer->user_id][UsersPrice::PRICE_B_12];
            if($priceB12 > UsersPrice::PRICE_MAX_BINH_BO){
                $sBinh12 = ActiveRecord::formatCurrency($priceB12)." d/binh";
            }else{
                $sBinh12 = ActiveRecord::formatCurrency($priceB12)." d/kg";
            }
            $title .= "\nBinh 12kg: ". $sBinh12;
            $valid = true;
        }
        $title .= "\nChi tiet lien he: 19001521";
        return $valid;
    }
    
    public function getCompanyKhongDau() {
        $mAppCache = new AppCache();
        $aCompany = $mAppCache->getListdataUserByRole(ROLE_COMPANY);
        $aRes = [];
        foreach($aCompany as $uid => $first_name){
            $aRes[$uid] = MyFunctionCustom::remove_vietnamese_accents($first_name);
        }
        return $aRes;
    }

    /**
     * @Author: ANH DUNG Feb 26, 2017
     * @Todo: do insert db
     */
    public function doInsertMultiQuotesSave($aRowInsert) {
        $tableName = GasScheduleSms::model()->tableName();
        $sql = "insert into $tableName (uid_login,
                        phone,
                        user_id,
                        username,
                        type,
                        obj_id,
                        title,
                        json_var,
                        count_run,
                        time_send,
                        content_type
                        ) values ".implode(',', $aRowInsert);
        if(count($aRowInsert)>0){
            Yii::app()->db->createCommand($sql)->execute();
        }
    }
    
    /**
     * @Author: ANH DUNG Mar 02, 2017
     * @Todo: đếm số Sms gửi cho 1 user theo type và đk
     */
    public static function countByUserAndType($user_id, $type, $needMore=[]) {
        $criteria = new CDbCriteria();
        $criteria->addCondition('t.user_id=' . $user_id .' AND t.type='.$type );
        if(isset($needMore['LimitMonth'])){
            $criteria->addCondition('MONTH(t.time_send)=' . $needMore['LimitMonth']);
        }
        return self::model()->count($criteria);
    }
    
    public static function getRoleResendSms() { 
        return [ROLE_ADMIN, ROLE_HEAD_GAS_BO, ROLE_SALE, ROLE_SALE_ADMIN, ROLE_DIRECTOR_BUSSINESS];
    }
    
    /** @Author: ANH DUNG Jun 21, 2017
     * @Todo: is array model UsersPhoneExt2
     */
    public function doInsertMultiAnnounce($aModelPhone, $title) {
        /** 2. foreach $aModelPhone để build sql, không cần find model User vì SMS này không có tên KH */
        /** xử lý find model user để lấy company của các KH đưa vào câu chào của SMS */
        $aRowInsert = $aCheckDuplicate = []; $uid_login = GasConst::UID_ADMIN;
        $type = GasScheduleSms::TYPE_ANNOUNCE; $username = $obj_id = $json_var = ''; $count_run = 0;
        if(empty($this->time_send)){
            $this->time_send = date('2018-04-01 08:00:00');
        }
        $content_type = GasScheduleSms::ContentTypeKhongDau;
        foreach($aModelPhone as $mPhoneCustomer){
            if(in_array($mPhoneCustomer->phone, $aCheckDuplicate)){continue;}
            $aCheckDuplicate[$mPhoneCustomer->phone] = $mPhoneCustomer->phone;
            
            $phone = '84'.$mPhoneCustomer->phone;

//            $aPhoneTest = ['1684331552', '918318328'];
//            if(!in_array($mPhoneCustomer->phone, $aPhoneTest)){continue;}
            
            $aRowInsert[] = "('$uid_login',
                '$phone',
                '$mPhoneCustomer->user_id',
                '$username',
                '$type',
                '$obj_id',
                '$title',
                '$json_var',
                '$count_run',
                '$this->time_send',
                '$content_type'
            )";
        }
        $this->doInsertMultiQuotesSave($aRowInsert);
        $aModelPhone = null;
    }

    /**
     * @Author: Trung Sept 29, 2017
     * @Todo: send sms cho customer Username + OTP login app
     * @param $mSignupId String of sign up model
     * @param $firstName String of user
     * @param $confirmCode String confirm code sent to client
     * @return GasScheduleSms|null new object
     */
    public function generateAndSendOTP($mSignupId, $firstName, $confirmCode)
    {
        try {
            $this->gas24hPin = $confirmCode;
            // Generate content to send
            $msg = "Ma PIN: $confirmCode";
            $msg .= "\nCam on Quy Khach da su dung Gas24h!";
            $title          = MyFunctionCustom::remove_vietnamese_accents($msg);
            $this->title    = MyFunctionCustom::ShortenStringSMS($title, GasScheduleSms::WIDTH_SMS);

            $this->gas24hNotifyPin();// Apr2618 chưa có token nên ko thể gửi sms đc 
            // Set data to object
            $json_var = array('first_name' => $firstName);
            $phoneNumberSend = UsersPhone::formatPhoneSaveAndSearch($this->phone);
            $schedule_sms_id = GasScheduleSms::InsertRecord(GasConst::UID_ADMIN,
                '', $phoneNumberSend, GasScheduleSms::TYPE_CUSTOMER_INFO_LOGIN, GasScheduleSms::ContentTypeKhongDau, $mSignupId, '', $this->title, $json_var);
            if (empty($schedule_sms_id)) {
                Logger::WriteLog("SMS empty id: ");
                return;
            }
            // Action send to phone
            $mScheduleSms = GasScheduleSms::model()->findByPk($schedule_sms_id);
            $mScheduleSms->doSend();
            return $mScheduleSms;
        } catch (Exception $ex) {
            GasCheck::CatchAllExeptiong($ex);
        }
        return null;
    }
    
    public function getArrayPhoneDev(){
        return [
            '0384331552',// DungNT
            '0981471595',// NamNH
            '0935714733',// DuongNV
        ];
    }
    
    /** @Author: ANH DUNG Apr 26, 2018
     *  @Todo: thêm hàm gửi notify khi SMS bị lỗi
     * Send ok, chờ Trung Fix luôn hiển thị notify khi logout app ra
     **/
    public function gas24hNotifyPin() {
        if (empty($this->mAppSignup)) {
            return ;
        }
        if(!in_array($this->phone, $this->getArrayPhoneDev()) && GasCheck::isServerLive()){ 
//        if($this->phone != '0988538360' && GasCheck::isServerLive()){ 
//        if(0){
            return; 
        }
        $mAppSignup = $this->mAppSignup;
        
        $mScheduleNotify    = new GasScheduleNotify();
        $gas24hPemFile      = $mScheduleNotify->getGas24hPemFile();
        $mScheduleNotify->user_id       = $mAppSignup->user_id;
        $mScheduleNotify->type          = GasScheduleNotify::GAS24H_ANNOUNCE;
        $mScheduleNotify->obj_id        = $mAppSignup->id;
        $mScheduleNotify->time_send     = date('Y-m-d H:i:s');
        $mScheduleNotify->username      = $mAppSignup->first_name;
        $mScheduleNotify->title         = $this->title;
        $mScheduleNotify->json_var      = MyFormat::jsonEncode(['gas24hPin' => $this->gas24hPin]);
        
        if(!empty($mAppSignup->gcm_device_token)){
            $mScheduleNotify->platform          = UsersTokens::PLATFORM_ANDROID;
            $mScheduleNotify->gcm_device_token  = $mAppSignup->gcm_device_token;
//            $mScheduleNotify->gcm_device_token  = 'd4RImsfAryQ:APA91bGnLMxeFT89v074A9V7N2UhCsGItcCSrqbhW1Ih1b3iq6KUTZpG9J2t-OZ7BxPV_G-ILNl39z2qzyHhWGH1NRHetPHFah_9g7JbuQJycen2UKh848WZlfizeD0mHP4MrLjlHdrf';
//            $mScheduleNotify->gcm_device_token  = 'dCeEyUBxdwg:APA91bFujLY8Y1E-8V_QLSWqNH38HDk6hdwSDrGE0AX87R7Zs-AIoRwj1-qL75ac6TmFq8-O095-NXeEkfOsDwd1WO1aB4ZsQo21t4hoELgNJk5fWpNcLGXzJl_BlZiPxngwN4m7FLwP';
        }elseif(!empty($mAppSignup->apns_device_token)){
            $mScheduleNotify->platform          = UsersTokens::PLATFORM_IOS;
            $mScheduleNotify->apns_device_token = $mAppSignup->apns_device_token;
//            Logger::WriteLog(Yii::app()->apns->pemFile. " -- $mScheduleNotify->apns_device_token");
            $mScheduleNotify->setGas24hPemIos($gas24hPemFile);
        }
        if(empty($mScheduleNotify->platform)){ 
            return ;
        }

        $mScheduleNotify->save();
        $mScheduleNotify = GasScheduleNotify::model()->findByPk($mScheduleNotify->id);
        $mScheduleNotify->obj_id    = '8540'; // admin/appPromotion/update/id/8540 - để id của tin cân gas dư, nếu ko có id này khi click vào notify sẽ báo lỗi
        $mScheduleNotify->reply_id  = $mScheduleNotify->getJsonFieldOneDecode('gas24hPin', 'json_var'); // admin/appPromotion/update/id/8540 - để id của tin cân gas dư, nếu ko có id này khi click vào notify sẽ báo lỗi
        $mScheduleNotify->cronBigQtySending(ApiNotify::TYPE_GAS24H_ANNOUNCE);
        $mScheduleNotify->plusRun();
//        $mScheduleNotify->delete();
    }
    
    /** @Author: ANH DUNG Feb 06, 2018
     *  @Todo: thông báo đã thu tiền KH bò mối khi nhân viên thu nợ bấm trên app
     *  @Param: $mCustomer, $mEmployeeCashbook model NV bấm thu nợ
     **/
    public function announceBoMoiPayDebit($mCustomer, $mEmployeeCashbook) {
//        $this->title = "Quy Khach da thanh toan ".$mEmployeeCashbook->getAmount(true)." tien Gas";
//        $this->title .= "\nMa khach hang: ".$mCustomer->code_bussiness;
//        $this->title .= "\nNgay thanh toan: ".date('d/m/Y');
//        $this->title .= "\nXem chi tiet tren App Gas Service hoac lien he: 19001521";
//        $this->purgeMessage();
//        
//        $this->uid_login        = GasConst::UID_ADMIN;
//        $this->user_id          = $mCustomer->id;
//        $this->type             = GasScheduleSms::TYPE_ANNOUNCE_BOMOI_PAY_DEBIT;
//        $this->obj_id           = $mEmployeeCashbook->id;
//        $this->content_type     = GasScheduleSms::ContentTypeKhongDau;
//        $this->time_send        = '';
//        $this->json_var         = [];
//        return $this->fixInsertRecord();
        
        $this->announceBoMoiPayDebitSend($mCustomer, $mEmployeeCashbook->getAmount(true), $mEmployeeCashbook->id, GasScheduleSms::TYPE_ANNOUNCE_BOMOI_PAY_DEBIT);
    }
    
    /** @Author: ANH DUNG May 06, 2019
     *  @Todo: thông báo đã thu tiền KH bò mối khi nhân viên PVKH bấm hoàn thành trên app
     * chỉ gửi với những KH có hạn thanh toán L1 = 1 ngày - TT ngay
     *  @Param: $mCustomer, $mEmployeeCashbook model NV bấm thu nợ
     **/
    public function announceBoMoiPayL1($mCustomer, $mAppOrder) {
        if($mCustomer->payment_day != GasTypePay::L1_1_DAY){
            return ;
        }
        $this->announceBoMoiPayDebitSend($mCustomer, $mAppOrder->getGrandTotal(true), $mAppOrder->id, GasScheduleSms::TYPE_ANNOUNCE_BOMOI_DEBIT_L1);
    }
    
    /** @Author: ANH DUNG Feb 06, 2018
     *  @Todo: thông báo đã thu tiền KH bò mối khi nhân viên thu nợ bấm trên app
     *  @Param: $mCustomer, $mEmployeeCashbook model NV bấm thu nợ
     **/
    public function announceBoMoiPayDebitSend($mCustomer, $amount, $obj_id, $type) {
        $this->title = "Quy Khach da thanh toan $amount tien Gas";
        $this->title .= "\nMa khach hang: ".$mCustomer->code_bussiness;
        $this->title .= "\nNgay thanh toan: ".date('d/m/Y');
        $this->title .= "\nXem chi tiet tren App Gas Service hoac lien he: 19001521";
        $this->purgeMessage();
        
        $this->uid_login        = GasConst::UID_ADMIN;
        $this->user_id          = $mCustomer->id;
        $this->type             = $type;
        $this->obj_id           = $obj_id;
        $this->content_type     = GasScheduleSms::ContentTypeKhongDau;
        $this->time_send        = '';
        $this->json_var         = [];
        return $this->fixInsertRecord();
    }
    
    /** @Author: ANH DUNG Feb 06, 2018
     *  @Todo: lọc tiếng việt và độ dài mesage
     **/
    public function purgeMessage() {
        $this->title = MyFunctionCustom::remove_vietnamese_accents($this->title);
        $this->title = MyFunctionCustom::ShortenStringSMS($this->title, GasScheduleSms::WIDTH_SMS);
        $this->title = strip_tags($this->title);
    }
    
    /** @Author: ANH DUNG Sep 08, 2018
     *  @Todo: email cảnh báo nạp tiền sms nếu Balance < 5 tr
     **/
    public function alertBalance() {
        $cBalance = $this->checkBalance();
        if($cBalance > self::BALANCE_ALERT){
            return ;
        }
        $cBalance = ActiveRecord::formatCurrency($cBalance);
        $mHtmlFormat        = new HtmlFormat();
        $mHtmlFormat->html  = "<p><b>Số dư SMS hiện tại là $cBalance vui lòng nạp thêm vào Viettel</b></p><br><br>";
        $bodyEmail          = $mHtmlFormat->html;
        $needMore['title']      = "[Cảnh báo SMS] dư $cBalance - ".date('d/m/Y');
        $needMore['list_mail']  = ['it.server@spj.vn'];// Jan1818 function này chỉ để Dev Test
        SendEmail::bugToDev($bodyEmail, $needMore);
        $mGasScheduleSms = new GasScheduleSms();
        $mGasScheduleSms->alertAnything("[Nap tien SMS] so du hien tai $cBalance" .date('Y-m-d H:i:s'), GasConst::$arrAdminUserReal);
    }
    
    /** @Author: ANH DUNG Sep 15, 2018
     *  @Todo: any thông báo 
     **/
    public function alertAnything($msg, $aUid=[]) {
        if(count($aUid) < 1){
            $aUid = [GasLeave::UID_DUNG_NT];// Jul 26, 2016 send cho Dũng để check SMS send
        }
        foreach($aUid as $user_id){
            $mScheduleSms = new GasScheduleSms();
            $mScheduleSms->title = $msg;
            $mScheduleSms->purgeMessage();
            $mScheduleSms->phone            = UsersPhone::getPhoneUser($user_id);
            $mScheduleSms->uid_login        = GasConst::UID_ADMIN;
            $mScheduleSms->user_id          = $user_id;
            $mScheduleSms->type             = GasScheduleSms::TYPE_ANNOUNCE_INTERNAL;
            $mScheduleSms->obj_id           = 1;
            $mScheduleSms->content_type     = GasScheduleSms::ContentTypeKhongDau;
            $mScheduleSms->time_send        = '';
            $mScheduleSms->json_var         = [];
            $schedule_sms_id                = $mScheduleSms->fixInsertRecord();
            if(empty($schedule_sms_id)){
                Logger::WriteLog("alertAnything SMS empty id: ");
                return ;
            }
            $mScheduleSms = GasScheduleSms::model()->findByPk($schedule_sms_id);
            $mScheduleSms->doSend();
        }
        
    }
    
    
    
}