<?php

/**
 * This is the model class for table "{{_gas_monitor_agent}}".
 *
 * The followings are the available columns in table '{{_gas_monitor_agent}}':
 * @property string $id
 * @property string $agent_id
 * @property string $code_account
 * @property string $last_active
 * @property string $created_date
 */
class GasMonitorAgent extends BaseSpj
{
//    public $timeBegin = 2;
    public $timeBegin = 22;
//    public $timeEnd = 23;// không cần thiết dùng
    public $date_from, $date_to, $date_from_ymd, $date_to_ymd, $autocomplete_name;
    
    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return GasMonitorAgent the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{_gas_monitor_agent}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        return array(
            array('date_from, date_to, id, agent_id, code_account, last_active, created_date', 'safe'),
            array('date_from_ymd, date_to_ymd', 'safe'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        return array(
            'rAgent' => array(self::BELONGS_TO, 'Users', 'agent_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'agent_id' => 'Đại lý',
            'code_account' => 'Mã số',
            'last_active' => 'Last Active',
            'created_date' => 'Ngày',
            'date_from' => 'Từ Ngày',
            'date_to,' => 'Đến Ngày',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        $criteria=new CDbCriteria;
        $criteria->compare('t.agent_id',$this->agent_id,true);
        $criteria->compare('t.code_account',$this->code_account,true);
        $criteria->compare('t.last_active',$this->last_active,true);
        $criteria->compare('t.created_date',$this->created_date,true);

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
            'pagination'=>array(
                'pageSize'=> Yii::app()->user->getState('pageSize',Yii::app()->params['defaultPageSize']),
            ),
        ));
    }

    /**
     * @Author: ANH DUNG May 03, 2016
     */
    public function setLastActive($last_active) {
        $this->last_active = $last_active;
        $this->update(array('last_active'));
    }
    
    /**
     * @Author: ANH DUNG May 03, 2016
     * @Todo: save or Update record
     * @Param: $code_account
     */
    public function SaveRecord($code_account) {
        $mAgent = Users::getAgentByCodeAccount($code_account);
        if(is_null($mAgent)){
            die;
        }
        $mMonitorToday = $this->getRecordTodayByAgent($mAgent);
        if(is_null($mMonitorToday)){
            $this->SaveRecordNew($mAgent);
        }else{// update record
            $mMonitorToday->setLastActive(date("Y-m-d H:i:s"));
        }
    }
    
    /**
     * @Author: ANH DUNG May 03, 2016
     * @Todo: get record by day if exitsts
     */
    public function getRecordTodayByAgent($mAgent) {
        $criteria = new CDbCriteria();
        $criteria->compare("t.agent_id", $mAgent->id);
        $criteria->compare("t.created_date", date("Y-m-d"), true);
        return self::model()->find($criteria);
    }
    
    /**
     * @Author: ANH DUNG May 03, 2016
     * @Todo: save new record
     * @Param: $mAgent
     */
    public function SaveRecordNew($mAgent) {
        $model = new GasMonitorAgent();
        $model->agent_id = $mAgent->id;
        $model->code_account = $mAgent->code_account;
        $model->last_active = date("Y-m-d H:i:s");
        $model->save();
    }

    /**
     * @Author: ANH DUNG May 04, 2016
     */
    public function getByDate() {
        $date_from = MyFormat::dateDmyToYmdForAllIndexSearch($this->date_from);
        $date_to = MyFormat::dateDmyToYmdForAllIndexSearch($this->date_to);
        $criteria = new CDbCriteria();
        $criteria->addBetweenCondition("DATE(t.created_date)", $date_from, $date_to);
        $criteria->compare('t.agent_id', $this->agent_id);
        $criteria->order = "t.last_active DESC";
        return self::model()->findAll($criteria);
    }
    
    /**
     * @Author: ANH DUNG May 19, 2016
     */
    public function getAgentName() {
        $mUser = $this->rAgent;
        if($mUser){
            return $mUser->getFullName();
        }
        return "";
    }
    
    /**
     * @Author: ANH DUNG Oct 27, 2016
     */
    public function reportByDate() {
        $date_from = MyFormat::dateDmyToYmdForAllIndexSearch($this->date_from);
        $date_to = MyFormat::dateDmyToYmdForAllIndexSearch($this->date_to);
        
        $this->date_from_ymd    = $date_from;
        $this->date_to_ymd      = $date_to;
                
        $criteria = new CDbCriteria();
        $criteria->addBetweenCondition("DATE(t.created_date)", $date_from, $date_to);
        $criteria->compare('t.agent_id', $this->agent_id);
        $models = self::model()->findAll($criteria);
        $aRes = array();
        foreach($models as $model){
            $tmp = explode(' ', $model->created_date);
            $aRes['OUTPUT'][$model->agent_id][$tmp[0]] = 1;
            if(isset($aRes['SUM_ALL'][$model->agent_id])){
                $aRes['SUM_ALL'][$model->agent_id] += 1;
            }else{
                $aRes['SUM_ALL'][$model->agent_id] = 1;
            }
        }
        if(isset($aRes['SUM_ALL'])){
            arsort($aRes['SUM_ALL']);
        }
        
        return $aRes;
    }
    
}