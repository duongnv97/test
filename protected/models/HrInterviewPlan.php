<?php

/**HrInterviewPlan
 * This is the model class for table "{{_hr_interview_plan}}".
 *
 * The followings are the available columns in table '{{_hr_interview_plan}}':
 * @property string $id
 * @property string $code_no
 * @property integer $plan_id
 * @property integer $status
 * @property string $user_id
 * @property string $candidate_id
 * @property integer $role_id
 * @property string $interview_date
 * @property integer $score
 * @property string $interview_date_next
 * @property string $note
 * @property string $created_by
 * @property string $created_date
 */
class HrInterviewPlan extends CActiveRecord
{
    public static  $aRoleHr = array(ROLE_SALE, ROLE_EMPLOYEE_MAINTAIN, ROLE_CHECK_MAINTAIN,ROLE_ACCOUNTING_ZONE);
    /** Autocomplete field candidate */
    public $autocomplete_name_candidate;
    /** Autocomplete field user */
    public $autocomplete_name_user;
    
    /** Constants */    
    const STATUS_WAIT_CONFIRM       = 1;
    const STATUS_CONFIRMED          = 2;
    const STATUS_WAIT_INTERVIEW     = 3;
    const STATUS_PASS               = 4;
    const STATUS_FAILED             = 5;
    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return HrInterviewPlan the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    /** Get list interviewer
     * @return array
     */
    public static function getListInterviewer()
    {
        $aModelUser = GasOneManyBig::getArrayModelUser(GasOneManyBig::TYPE_APPROVE_HR_INTERVIEW, GasOneManyBig::TYPE_APPROVE_HR_INTERVIEW);
        $aRes = array();
        foreach ($aModelUser as $item) {
            $aRes[$item->id] = $item->getNameWithRole();
        }
        return $aRes;
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{_hr_interview_plan}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        return array(
            array('plan_id, user_id, candidate_id, role_id, interview_date, created_date', 'required'),
            array('plan_id, status, role_id, score', 'numerical', 'integerOnly'=>true),
            array('code_no', 'length', 'max'=>20),
            array('user_id, candidate_id, created_by', 'length', 'max'=>11),
            array('interview_date_next', 'safe'),
            array('id, code_no, plan_id, status, user_id, candidate_id, role_id, interview_date, score, interview_date_next, note, created_by, created_date', 'safe'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        return array(
            'rUser' => array(self::BELONGS_TO, 'Users', 'user_id'),
            'rCandidate' => array(self::BELONGS_TO, 'Users', 'candidate_id'),
            'rCreatedBy' => array(self::BELONGS_TO, 'Users', 'created_by'),
            'rDetail' => array(self::HAS_MANY, 'HrInterviewPlanDetail', 'interview_plan_id'),
            'rPlan' => array(self::BELONGS_TO, 'HrPlan', 'plan_id'),
            'rRole' => array(self::BELONGS_TO, 'Roles', 'role_id')
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
            return array(
                'id' => 'ID',
                'code_no' => 'Mã số',
                'plan_id' => 'Kế hoạch tuyển dụng',
                'status' => 'Trạng thái',
                'user_id' => 'Người phỏng vấn',
                'candidate_id' => 'Ứng viên',
                'role_id' => 'Vị trí ứng tuyển',
                'interview_date' => 'Ngày phỏng vấn',
                'score' => 'Điểm đánh giá ứng viên',
                'interview_date_next' => 'Ngày phỏng vấn tiếp theo',
                'note' => 'Ghi chú',
                'created_by' => 'Người tạo',
                'created_date' => 'Ngày tạo',
            );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        $criteria=new CDbCriteria;

		$criteria->compare('t.id',$this->id,true);
		$criteria->compare('t.code_no',$this->code_no,true);
		$criteria->compare('t.plan_id',$this->plan_id);
		$criteria->compare('t.status',$this->status);
		$criteria->compare('t.user_id',$this->user_id,true);
		$criteria->compare('t.candidate_id',$this->candidate_id,true);
		$criteria->compare('t.role_id',$this->role_id);
		$criteria->compare('t.interview_date',$this->interview_date,true);
		$criteria->compare('t.score',$this->score);
		$criteria->compare('t.interview_date_next',$this->interview_date_next,true);
		$criteria->compare('t.note',$this->note,true);
		$criteria->compare('t.created_by',$this->created_by,true);
		$criteria->compare('t.created_date',$this->created_date,true);
        $criteria->order = 't.id DESC';
        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
            'pagination'=>array(
                'pageSize'=> Yii::app()->user->getState('pageSize',Yii::app()->params['defaultPageSize']),
            ),
        ));
    }
        
    //-----------------------------------------------------
    // Parent override methods
    //-----------------------------------------------------
    /**
     * Override before save method
     * @return Parent result
     */
    public function beforeSave() {
        $userId = isset(Yii::app()->user) ? Yii::app()->user->id : '';
        $this->interview_date = CommonProcess::convertDateTimeToMySqlFormat(
                $this->interview_date, DomainConst::DATE_FORMAT_3);
        $this->interview_date_next = CommonProcess::convertDateTimeToMySqlFormat(
                $this->interview_date_next, DomainConst::DATE_FORMAT_3);
        if ($this->isNewRecord) {
            $this->code_no = CommonProcess::generateCodeNo('HR');
            // Score
            if (!isset($this->score)) {
                $this->score = CommonProcess::getInterviewScore()[0];
            }
            // Note
            if (!isset($this->note)) {
                $this->note = '';
            }
            // Handle created by
            if (empty($this->created_by)) {
                $this->created_by = $userId;
            }
            $this->created_date = CommonProcess::getCurrentDateTimeWithMySqlFormat();
        }
        return parent::beforeSave();
    }    

    //-----------------------------------------------------
    // Utility methods
    //-----------------------------------------------------
    public function canUpdate() {
        return true;
    }
    
    /**
     * Get detail information
     * @return CArrayDataProvider
     */
    public function getDetailInfo() {
        return new CArrayDataProvider($this->rDetail, array(
            'id' => 'detail_info',
            'sort'=>array(
                'attributes'=>array(
                     'id', 'note', 'created_date',
                ),
            ),
            'pagination'=>array(
                'pageSize'=>10,
            ),
        ));
    }

    //-----------------------------------------------------
    // Static methods
    //-----------------------------------------------------
    /**
     * Loads the application items for the specified type from the database
     * @param type $emptyOption boolean the item is empty
     * @return type List data
     */
    public static function loadItems($emptyOption = false) {
        $_items = array();
        if ($emptyOption) {
            $_items[""] = "";
        }
        $models = self::model()->findAll(array(
            'order' => 'id ASC',
        ));
        foreach ($models as $model) {
            $_items[$model->id] = $model->code_no . ' - ' . $model->interview_date . ' ' . $model->rCandidate->first_name;
        }
        return $_items;
    }
    
    /**
     * Get status of interview plan
     * @return Array
     */
    public static function getStatus() {
        return array(
            HrInterviewPlan::STATUS_WAIT_CONFIRM    => 'Chờ xác nhận',
            HrInterviewPlan::STATUS_CONFIRMED       => 'Đã xác nhận',
            HrInterviewPlan::STATUS_WAIT_INTERVIEW  => 'Chờ phỏng vấn lại',
            HrInterviewPlan::STATUS_PASS            => 'Đạt yêu cầu',
            HrInterviewPlan::STATUS_FAILED          => 'Không đạt yêu cầu',
        );
    }
    
    /**
     * Get status when interviewer 
     * @return Array
     */
    public static function getInterviewResultStatus() {
        return array(
//            HrInterviewPlan::STATUS_WAIT_CONFIRM    => 'Chờ xác nhận',
//            HrInterviewPlan::STATUS_CONFIRMED       => 'Đã xác nhận',
            HrInterviewPlan::STATUS_WAIT_INTERVIEW  => 'Chờ phỏng vấn lại',
            HrInterviewPlan::STATUS_PASS            => 'Đạt yêu cầu',
            HrInterviewPlan::STATUS_FAILED          => 'Không đạt yêu cầu',
        );
    }
    
    public static function getUser() {
        return array(
            0 => 'Nguyễn Văn A',
            1 => 'Trần Văn B',
            2 => 'Huỳnh Thị C'
        );
    }
    
    /** @Author: HOANG NAM 16/03/2018
     *  @Todo: remove plan_id URL if full
     *  @Param: 
     **/
    public function removePlanIdUrl(){
        if(!empty($this->rPlan) && $this->rPlan->qty <= count($this->rPlan->rCandidates)){
            unset($this->plan_id);
        }
    }
    /** @Author: HOANG NAM 19/03/2018
     *  @Todo: get list role_id
     *  @Param: 
     **/
    public function getListRole(){
        $_items = array();
        $models=Roles::model()->findAll(array(
                'order'=>'id DESC',
        ));
        foreach($models as $model)
        {
            if(in_array( $model->id, HrInterviewPlan::$aRoleHr) ){
                $_items[$model->id]=$model->role_name;
            }
        }
        return $_items;
    }
}