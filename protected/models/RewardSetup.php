<?php

/**
 * This is the model class for table "{{_reward_setup}}".
 *
 * The followings are the available columns in table '{{_reward_setup}}':
 * @property string $id
 * @property string $agent_id
 * @property string $date_from
 * @property string $date_to
 * @property integer $status
 * @property string $point_reward
 * @property integer $count_sell_apply
 * @property string $created_date
 * @property string $created_by
 */
class RewardSetup extends BaseSpj
{
    const STATUS_ACTICE = 1;    //  status: 1: Hoạt động. 0: Không hoạt động
    const STATUS_INACTICE = 2;
    public $start_date_from, $start_date_to, $end_date_from, $end_date_to;
    
    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return RewardSetup the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{_reward_setup}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        return [
            array('status, count_sell_apply', 'numerical', 'integerOnly'=>true),
            array('point_reward, created_by', 'length', 'max'=>10),
            array('agent_id, date_from, date_to, created_date', 'safe'),
            ['id, agent_id, date_from, date_to, status, point_reward, count_sell_apply, created_date, created_by, start_date_from, start_date_to, end_date_from, end_date_to', 'safe'],
        ];
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        return [
        ];
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'agent_id' => 'Đại lí',
            'date_from' => 'Ngày bắt đầu',
            'date_to' => 'Ngày kết thúc',
            'status' => 'Trạng thái',
            'point_reward' => 'Điểm thưởng',
            'count_sell_apply' => 'Số hóa đơn',
            'created_date' => 'Ngày tạo',
            'created_by' => 'Người tạo',
            'start_date_from' => 'Từ ngày',
            'start_date_to' => 'Đến ngày',
            'end_date_from' => 'Từ ngày',
            'end_date_to' => 'Đến ngày',
        ];
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        $criteria=new CDbCriteria;

	$criteria->compare('t.id',$this->id,true);
//	$criteria->compare('t.agent_id',$this->agent_id,true);
        $criteria->compare('t.status',$this->status);
	$criteria->compare('t.point_reward',$this->point_reward,true);
	$criteria->compare('t.count_sell_apply',$this->count_sell_apply);
	$criteria->compare('t.created_date',$this->created_date,true);
	$criteria->compare('t.created_by',$this->created_by,true);
        
        $start_date_from = '';
        $start_date_to = '';
        if(!empty($this->start_date_from)){
            $start_date_from = MyFormat::dateDmyToYmdForAllIndexSearch($this->start_date_from);
        }
        if(!empty($this->start_date_to)){
            $start_date_to = MyFormat::dateDmyToYmdForAllIndexSearch($this->start_date_to);
        }
        if(!empty($start_date_from) && empty($start_date_to))
                $criteria->addCondition("t.date_from>='$start_date_from'");
        if(empty($start_date_from) && !empty($start_date_to))
                $criteria->addCondition("t.date_from<='$start_date_to'");
        if(!empty($start_date_from) && !empty($start_date_to))
                $criteria->addBetweenCondition("t.date_from",$start_date_from,$start_date_to);
        $end_date_from = '';
        $end_date_to = '';
        if(!empty($this->end_date_from)){
            $end_date_from = MyFormat::dateDmyToYmdForAllIndexSearch($this->end_date_from);
        }
        if(!empty($this->end_date_to)){
            $end_date_to = MyFormat::dateDmyToYmdForAllIndexSearch($this->end_date_to);
        }
        if(!empty($end_date_from) && empty($end_date_to))
                $criteria->addCondition("t.date_to>='$end_date_from'");
        if(empty($end_date_from) && !empty($end_date_to))
                $criteria->addCondition("t.date_to<='$end_date_to'");
        if(!empty($end_date_from) && !empty($end_date_to))
                $criteria->addBetweenCondition("t.date_to",$end_date_from,$end_date_to);
	
        $criteria->order = 't.id DESC';
        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
            'pagination'=>array(
                'pageSize'=> Yii::app()->user->getState('pageSize',Yii::app()->params['defaultPageSize']),
            ),
        ));
    }
    
    public function getArrayStatus(){
        return[
            RewardSetup::STATUS_ACTICE => 'Hoạt động',
            RewardSetup::STATUS_INACTICE => 'Không hoạt động',
        ];
    }
    
    public function getStatus(){
        $aStatus = $this->getArrayStatus();
        return isset($aStatus[$this->status]) ? $aStatus[$this->status] : "";
    }
    
    public function getPointReward() {
        return $this->point_reward;
    }
    
    public function getCountSellApply() {
        return $this->count_sell_apply;
    }
    
    public function getAgent() {
        $aJson = [];
        if(!empty($this->agent_id))
            $aJson = json_decode($this->agent_id);
        $str = "<table cellpadding='0' cellspacing = '0' style = 'border : 1px solid black '  >";
        foreach ($aJson as $key => $value){
            $mEmployee = Users::model()->findByPk($value); 
            $str .= "<tr style = 'border : 1px solid black'>";
            $str .= "<td style = 'border : 1px solid black'>".($key+1)."</td>";
            if ($mEmployee) {
                $str .= "<td style = 'border : 1px solid black'>".$mEmployee->first_name."</td>";
            }else{
                $str .= "<td style = 'border : 1px solid black'>$value</td>";
            }
            $str .= "</tr>";
        }
        $str .= "</table>";
        return $str;
    }
    
    
    public function initCreateSetup(&$model) {
        $model->created_by = MyFormat::getCurrentUid();
        $model->status = 1;
    }
    
     public function getDataValidate() {
        if(isset($_POST['RewardSetup']['agent_id'])){
            $this->agent_id = json_encode($_POST['RewardSetup']['agent_id']);
        }  
        $this->date_from  = MyFormat::dateConverDmyToYmd($this->date_from);
        $this->date_to  = MyFormat::dateConverDmyToYmd($this->date_to);
        $this->created_date = date('Y-m-d H:i:s');
        $this->agent_id = ($this->agent_id) ? $this->agent_id : '';
    }
    
    /** @Author: KHANH TOAN 2018
     *  @Todo: kiem tra quyen co the update
     *  @Param:
     **/
    public function canUpdate() {
        return true;
    }
    
     public function formatDataBeforeUpdate() {
        $this->date_from  = MyFormat::dateConverYmdToDmy($this->date_from);
        $this->date_to  = MyFormat::dateConverYmdToDmy($this->date_to);
        $this->agent_id = json_decode($this->agent_id);
    }
    
    /** @Author: NGUYEN KHANH TOAN  Sep 10, 2018 -- handle listing api */
    public function handleApiListRewardSetUp(&$result, $q) {
        // 1. get list order by user id
        $dataProvider   = $this->ApiListingRewardSetup($q);
        $models         = $dataProvider->data;
        $CPagination    = $dataProvider->pagination;
        $result['total_record'] = $CPagination->itemCount;
        $result['total_page']   = $CPagination->pageCount;
        
        $result['message']  = 'L';
        $result['record']   = [];
        if( $q->page >= $CPagination->pageCount ){
            $result['record'] = [];
        }else{
            foreach($models as $model){
               $result['record'][]         = $model->formatAppItemListRewardSetup();
            }
        }
       
    }
    
     /**
    * @Author: NGUYEN KHANH TOAN Sep 10 2018
    * @Todo: get data listing 
    * @param: $q object post params from app client
    */
    public function ApiListingRewardSetup($q) {
        $criteria = new CDbCriteria();
        $criteria->order = 't.id DESC';
        $dataProvider = new CActiveDataProvider($this, array(
            'criteria' => $criteria,
            'pagination'=>array(
                'pageSize' => Reward::API_LISTING_REWARD_PER_PAGE,
                'currentPage' => (int)$q->page,
            ),
          ));
        return $dataProvider;
    }
    
    /** @Author: NGUYEN KHANH TOAN Sep 10 2018
     *  @Todo: dùng chung để format trả xuống list + view của app Gas24h
     **/
    public function formatAppItemListRewardSetup() {
        $temp = [];
        $temp['id']                     = $this->id;
        $temp['date_from']              = MyFormat::dateConverYmdToDmy($this->date_from);
        $temp['date_to']                = MyFormat::dateConverYmdToDmy($this->date_to);
        $temp['point_reward']           = $this->point_reward;
        $temp['count_sell_apply']       = $this->count_sell_apply;
        $temp['agent_id']               = $this->getArrayAgent();
        return $temp;
    }
    
    /** @Author: NamNH Apr 24, 2019
     *  @Todo: get nlist agent apply
     **/
    public function getArrayAgent() {
        $aJson = [];
        $result = [];
        if(!empty($this->agent_id))
            $aJson = json_decode($this->agent_id);
        $criteria = new CDbCriteria();
        $criteria->addInCondition('id', $aJson);
        $aAgent = Users::model()->findAll($criteria);
        foreach ($aAgent as $key => $mAgent) {
            $result[$mAgent->id] = $mAgent->first_name;
        }
        return $result;
    }
}