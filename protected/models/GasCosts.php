<?php

/**
 * This is the model class for table "{{_gas_costs}}".
 *
 * The followings are the available columns in table '{{_gas_costs}}':
 * @property integer $id
 * @property string $name
 * @property integer $status
 */
class GasCosts extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return GasCosts the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{_gas_costs}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('name', 'required'),
			array('status', 'numerical', 'integerOnly'=>true),
			array('name', 'length', 'max'=>300),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, name, status, order_no', 'safe'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'name' => 'Tên Chi Phí',
			'status' => 'Tình Trạng',
			'order_no' => 'Thứ Tự',
			
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('t.id',$this->id);
		$criteria->compare('t.name',$this->name,true);
		$criteria->compare('t.status',$this->status);
$criteria->order = "t.order_no asc";	
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
            'pagination'=>array(
                'pageSize'=> Yii::app()->user->getState('pageSize',Yii::app()->params['defaultPageSize']),
            ),
		));
	}

    /*
    public function activate()
    {
        $this->status = 1;
        $this->update();
    }

    public function deactivate()
    {
        $this->status = 0;
        $this->update();
    }
	*/

	public function defaultScope()
	{
		return array(
			//'condition'=>'',
		);
	}
        
        public static function getAllItem(){
		$criteria=new CDbCriteria;
		//$criteria->compare('t.status',STATUS_ACTIVE);
                $models = self::model()->findAll($criteria);
                return  CHtml::listData($models,'id','name');
        }        
        
        public static function getAllObj(){
		$criteria=new CDbCriteria;
		$criteria->compare('t.status',STATUS_ACTIVE);
                $models = self::model()->findAll($criteria);
                $aRes =array();
                foreach($models as $item)
                    $aRes[$item->id] = $item;
                return  $aRes;
        }  

    public static function getArrIdCostsLikeMultiselect($role_id='')
    {
        $criteria = new CDbCriteria;
        if(!empty($role_id))
            $criteria->compare('t.role_id', $role_id);        
        $models = self::model()->findAll($criteria);
        return  CHtml::listData($models,'id','id');            
    }            
    		
        
}