<?php

/**
 * This is the model class for table "{{_gas_remain_export}}".
 *
 * The followings are the available columns in table '{{_gas_remain_export}}':
 * @property string $id
 * @property string $group_id
 * @property string $parent_id
 * @property string $code_no
 * @property integer $type
 * @property integer $serial
 * @property string $agent_id
 * @property string $created_date
 * @property string $uid_login
 * @property string $last_update_by
 * @property string $last_update_time
 */
class GasRemainExport extends BaseSpj
{
    public  $MAX_ID;
    const KG_DRIVER=1;
    const KG_STATION=2;
    public  static $KG_ARR_TYPE = array(
        GasRemainExport::KG_DRIVER=>'Lái Xe Nhận',
        GasRemainExport::KG_STATION=>'Trạm Nhận',
    );

    public $date_from;
    public $date_to;   
    public $aModelRemain;
    public $primaryDetail;
    public $autocomplete_name;        

    // lấy số ngày cho phép đại lý cập nhật
    public static function getDayAllowUpdate(){
        return 1;
    }
    
    public static function model($className=__CLASS__)
    {
            return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
            return '{{_gas_remain_export}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        return array(
            array('type', 'required'),
            array('date_from,date_to,note', 'safe'),
            array('id, group_id, parent_id, code_no, type, serial, agent_id, created_date, uid_login, last_update_by, last_update_time', 'safe'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        return array(
            'rAgent' => array(self::BELONGS_TO, 'Users', 'agent_id'),
            'rUidLogin' => array(self::BELONGS_TO, 'Users', 'uid_login'),
            'rLastUpdateBy' => array(self::BELONGS_TO, 'Users', 'last_update_by'),
            
            'rParent' => array(self::BELONGS_TO, 'GasRemainExport', 'parent_id'),
            'rDetail' => array(self::HAS_MANY, 'GasRemainExportDetail', 'remain_export_id',
//                 'order'=>'rDetail.id ASC',
                ),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'group_id' => 'Group',
            'parent_id' => 'Mã Phiếu Cân Lần 1',
            'code_no' => 'Mã Phiếu',
            'type' => 'Loại Giao Nhận',
            'serial' => 'Thứ Tự Lần Cân',
            'agent_id' => 'Đại Lý',
            'created_date' => 'Ngày Tạo',
            'uid_login' => 'Uid Login',
            'last_update_by' => 'Người Sửa',
            'last_update_time' => 'Ngày Sửa',
            'date_from' => 'Từ Ngày',
            'date_to' => 'Đến Ngày',                
            'note' => 'Ghi Chú',                
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        $criteria=new CDbCriteria;
        $criteria->compare('t.id',$this->id,true);
        $criteria->compare('t.group_id',$this->group_id,true);
        
        $criteria->compare('t.code_no',$this->code_no,true);
        $criteria->compare('t.note',$this->note,true);
        $criteria->compare('t.type',$this->type);
        $criteria->compare('t.serial',$this->serial);
        
        if(Yii::app()->user->role_id==ROLE_SUB_USER_AGENT){
            $criteria->compare('t.agent_id', MyFormat::getAgentId());
        }else{
            if(!empty($this->agent_id)){                        
                $criteria->compare('t.agent_id', $this->agent_id);
            }
            GasAgentCustomer::addInConditionAgent($criteria, 't.agent_id');
        }  
        
        if(!empty($this->parent_id)){
            $criteria->compare('t.group_code_no',$this->parent_id,true);
        }

        $date_from = '';
        $date_to = '';
        
        if(!empty($this->date_from)){
            $date_from = MyFormat::dateDmyToYmdForAllIndexSearch($this->date_from)." 00:00:00";
        }
        if(!empty($this->date_to)){
            $date_to = MyFormat::dateDmyToYmdForAllIndexSearch($this->date_to)." 23:59:59";
        }
        if(!empty($date_from) && empty($date_to))
                $criteria->addCondition("t.created_date>='$date_from'");
        if(empty($date_from) && !empty($date_to))
                $criteria->addCondition("t.created_date<='$date_to'");
        if(!empty($date_from) && !empty($date_to))
                $criteria->addBetweenCondition("t.created_date",$date_from,$date_to);         

        $sort = new CSort();

        $sort->attributes = array(
            'code_no'=>'code_no',
            'agent_id'=>'agent_id',
            'group_id'=>'group_id',
            'parent_id'=>'parent_id',
            'type'=>'type',
            'serial'=>'serial',
            'created_date'=>'created_date',                
        );    
        $sort->defaultOrder = 't.id desc';               

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
            'sort' => $sort,
            'pagination'=>array(
                'pageSize'=> Yii::app()->user->getState('pageSize',Yii::app()->params['defaultPageSize']),
            ),
        ));
    }

    public function defaultScope()
    {
            return array(
                    //'condition'=>'',
            );
    }
    
    protected function afterValidate() {
        if(!isset($_POST['gas_remain_id']) || count($_POST['gas_remain_id'])<1)
            $this->addError ('agent_id', 'Dữ liệu không hợp lệ');
        return parent::afterValidate();
    }
    
    protected function beforeSave() {
        if($this->isNewRecord){
            $this->agent_id = MyFormat::getAgentId();
            $this->uid_login = Yii::app()->user->id;
            $this->code_no = MyFunctionCustom::getNextId('GasRemainExport', 'G'.date('y'), LENGTH_ORDER_BY_YEAR,'code_no');
            $this->serial = 1;
            if(!empty($this->parent_id) && $this->parent_id>0){
                $this->serial = self::getNextSerial($this->parent_id);
            }
        }else{
            $this->last_update_by = Yii::app()->user->id;
            $this->last_update_time = date('Y-m-d H:i:s');
        }
        
        return parent::beforeSave();
    }
    
    // Lấy số thứ tự tiếp theo cho lần giao nhận gas dư
    public static function getNextSerial($group_id){
        $criteria = new CDbCriteria();
        $criteria->compare('group_id', $group_id);
        return (self::model()->count($criteria)+1);
        
    }
    
    /**
     * @Author: ANH DUNG Jun 22, 2014
     * @Todo: ghi đè 1 số field của record new, ko dùng cho update
     * @Param: $model this
     */
    public static function OverideSomeFieldAfterSave($model){
        if($model->rParent){ // rParent là model cân lần 1
            $model->group_id = $model->rParent->id;
            $model->group_code_no = $model->rParent->code_no;
        }else{
            $model->group_id = $model->id;
            $model->group_code_no = $model->code_no;
        }
        $model->update(array('group_id','group_code_no'));
    }
    
    public static function SaveDetail($model){
        GasRemainExportDetail::delete_by_remain_export_id($model->id);
        $aRowInsert=array();        
        if( isset($_POST['gas_remain_id']) && count($_POST['gas_remain_id']) ){
            $aModelRemain = GasRemain::getByListId($_POST['gas_remain_id']);
            foreach($_POST['gas_remain_id'] as $key=>$gas_remain_id){
                if(isset($aModelRemain[$gas_remain_id])){
                    $mDetail = new GasRemainExportDetail();
                    $mDetail->remain_export_id = $model->id;
                    $mDetail->gas_remain_id = $gas_remain_id;
                    $mDetail->group_id = $model->group_id;
                    $mDetail->agent_id = $model->agent_id;
                    $mDetail->type = $model->type;
                    $mDetail->serial = $model->serial;
                    $amount_empty = $aModelRemain[$gas_remain_id]->amount_empty;
                    $mDetail->amount_has_gas = isset($_POST['amount_has_gas'][$key])?$_POST['amount_has_gas'][$key]:0;
                    $mDetail->amount_gas = $mDetail->amount_has_gas-$amount_empty;
                    $note = isset($_POST['note'][$key])?$_POST['note'][$key]:"";
                    $mDetail->note = MyFormat::removeBadCharacters($note); 
                    $mDetail->created_date = $model->created_date;
                    $aRowInsert[]="('$mDetail->remain_export_id',
                        '$mDetail->gas_remain_id',
                        '$mDetail->group_id',
                        '$mDetail->agent_id',
                        '$mDetail->type',
                        '$mDetail->serial',
                        '$mDetail->amount_has_gas',
                        '$mDetail->amount_gas',
                        '$mDetail->note',
                        '$mDetail->created_date'
                        )";
                }
            } // end foreach($_POST['gas_r
        }// end if( isset($_POST['gas_remai
            
        
        $tableName = GasRemainExportDetail::model()->tableName();
            $sql = "insert into $tableName (remain_export_id,
                        gas_remain_id,
                        group_id,
                        agent_id,
                        type,
                        serial,
                        amount_has_gas,
                        amount_gas,
                        note,
                        created_date
                        ) values ".implode(',', $aRowInsert);
            if(count($aRowInsert)>0)
                Yii::app()->db->createCommand($sql)->execute();        
        
    }
    
    protected function beforeDelete() {
        GasRemainExport::UpdateStatusHasExport($this, 0);
        GasRemainExportDetail::delete_by_remain_export_id($this->id);
        return parent::beforeDelete();
    }
    
    /**
     * @Author: ANH DUNG Jun 22, 2014 - for update
     * @Todo: get array model gasRemain from GasRemainExportDetail
     * @Param: $model model GasRemainExport
     */
    public static function getArrModelRemain($model){
        $ListRemainId = array();
//        $ListRemainId = CHtml::listData($model->rDetail,'gas_remain_id','gas_remain_id');
        foreach($model->rDetail as $item){
            $ListRemainId[] = $item->gas_remain_id;
            $model->primaryDetail['amount_has_gas'][$item->gas_remain_id] = $item->amount_has_gas;
            $model->primaryDetail['amount_gas'][$item->gas_remain_id] = $item->amount_gas;
            $model->primaryDetail['note'][$item->gas_remain_id] = $item->note;
        }
        $model->aModelRemain = GasRemain::getByListId($ListRemainId, array('OnlyFindall'=>1));
        return $model;
    }
    
    /**
     * @Author: ANH DUNG Jun 22, 2014 - for ajax select item parent khi tạo cân từ lần 2
     * @Todo: get array model gasRemain by pk
     * @params: $model GasRemainExport
     * @params: $pk primary key of GasRemainExport
     */
    public static function getInfoModelRemainByPk($pk){
        $mRemainExport = self::model()->findByPk($pk);
        if($mRemainExport){
            return self::getArrModelRemain($mRemainExport);            
        }
        return $model=new GasRemainExport('create'); // trả về new cho đỡ bị lỗi
    }
    
    
    /**
     * @Author: ANH DUNG Jun 22, 2014
     * @Todo: update has_export by array id gasRemain from GasRemainExportDetail
     * nếu là lần cân 1 thì set has_export, những lần cân sau ko set.
     * @Param: $model model GasRemainExport
     */
    public static function UpdateStatusHasExport($model, $status){
        if($model->serial==1){
            $ListRemainId = CHtml::listData($model->rDetail,'gas_remain_id','gas_remain_id');
            GasRemain::UpdateHasExport($ListRemainId, $status);
        }
    }
    
    // kiểm tra xem có cho phép xóa lần cân đầu tiên đi không? nếu ko có con thì cho phép
    public static function canDeleteRecordFirst($model){
        $ok = true;
        if($model->serial==1){
            $criteria = new CDbCriteria();
            $criteria->compare('group_id', $model->group_id);
            if(self::model()->count($criteria)>1)
                $ok = false;
        }
        return $ok;
    }

    /**
     * @Author: ANH DUNG Jun 22, 2014
     * @Todo: get array các lần cân show trang in
     * @Param: $model GasRemainExport
     */    
    public static function getArraySerialPrint($model){
        $res = array();
        $criteria = new CDbCriteria();
        $criteria->compare('t.group_id', $model->group_id);
        $criteria->order = 't.serial ASC';
        $models = GasRemainExportDetail::model()->findAll($criteria);
        $res['gas_remain_id'] = array();
        $res['Serial'] = array();
        $res['RemainExport'] = array();
        foreach($models as $item){
            // 1. lấy id bảng remain để find model tính 2 cột đầu là KH trả vs đại lý nhận
            $res['gas_remain_id'][$item->gas_remain_id] = $item->gas_remain_id;
            // 2. lấy các lần giao nhận
            $res['RemainExport'][$item->gas_remain_id][$item->serial] = $item->amount_gas;
            $res['Serial'][$item->serial] = $item->type;
        }
        // 2 cột đầu dc lấy rồi, nên ko cần phải lấy ở đây nữa
//        $mTwoColumnFirst = GasRemain::getByListId($res['gas_remain_id']);
        return $res;    
    }
    
    
}