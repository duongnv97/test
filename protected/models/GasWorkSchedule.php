<?php

/**
 * This is the model class for table "{{_gas_work_schedule}}".
 *
 * The followings are the available columns in table '{{_gas_work_schedule}}':
 * @property string $id
 * @property string $employee_id
 * @property string $date_work
 * @property integer $slot_id
 * @property integer $employee_id_old
 * @property string $work_schedule_id_change
 */
class GasWorkSchedule extends CActiveRecord
{
    const CA_1 = 1;
    const CA_2_LONG = 2;
    const CA_2_SHORT = 3;
    const CA_3 = 4;
    const CA_OFF = 5;
    
    public $weekNo;
    public $year;
    public $date_from;
    public $date_to;
    
    public $employee_id_1;
    public $date_work_1;
    public $slot_id_1;
    public $employee_id_2;
    public $date_work_2;
    public $slot_id_2;
    
    public $mWorkSchedule1;
    public $mWorkSchedule2;
    
    /**
     * @Author: ANH DUNG Apr 28, 2016
     */
    public function getArrEmployeeAssignToSlot() {
        return array(
            GasTickets::DIEU_PHOI_CHUONG => GasWorkSchedule::CA_1,
            GasTickets::DIEU_PHOI_TRANG => GasWorkSchedule::CA_2_LONG,
            GasTickets::DIEU_PHOI_THAO => GasWorkSchedule::CA_3,
            GasTickets::DIEU_PHOI_THUY => GasWorkSchedule::CA_OFF,
        );
    }
    
    public function getArrSlotText() {
        return array(
            GasWorkSchedule::CA_1 => "Ca 1",
            GasWorkSchedule::CA_2_LONG => "Ca 2",
//            GasWorkSchedule::CA_2_SHORT => "Ca 2 Short",
            GasWorkSchedule::CA_3 => "Ca 3",
            GasWorkSchedule::CA_OFF => "Off",
        );
    }
    
    /**
     * @Author: ANH DUNG Apr 28, 2016
     */
    public function getArrEmployee() {
        return array(
            GasTickets::DIEU_PHOI_CHUONG => "Trần Quang Chương",
            GasTickets::DIEU_PHOI_TRANG => "Phạm Ngọc Thùy Trang",
            GasTickets::DIEU_PHOI_THAO => "Nguyễn Thị Thu Thảo",
            GasTickets::DIEU_PHOI_THUY => "Phan Xuân Thùy",
        );
    }
    
    
    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return GasWorkSchedule the static model class
     */
    public static function model($className=__CLASS__)
    {
            return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
            return '{{_gas_work_schedule}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        return array(
            array('id, employee_id, date_work, slot_id, employee_id_old, work_schedule_id_change', 'safe'),
            array('date_from, date_to, weekNo,year ', 'safe'),
            array('employee_id_1, date_work_1, slot_id_1, employee_id_2, date_work_2, slot_id_2', 'safe'),
            array('employee_id_1, date_work_1, slot_id_1, employee_id_2, date_work_2, slot_id_2', 'required', 'on'=>"ChangeSlot"),
            array('week_number, employee_id_1', 'CheckChangeSlot', 'on'=>"ChangeSlot"),
        );
    }

    public function CheckChangeSlot($attribute, $params)
    {
        /* 1. check slot 1 valid
         * 2. check slot 2 valid
         * 3. tiến hành đổi 
         * 4. đổi random lịch nếu cần
         * 4.1 đổi Off -> Off
         * 4.2 đổi Off -> Ca
         * 4.3 đổi Ca -> Ca
         */
        $this->mWorkSchedule1 = $this->findSlotValid(1);
        if(is_null($this->mWorkSchedule1)){
            $this->addError("employee_id_1", "Thông tin ca làm việc điều phối 1 không hợp lệ.");
        }
        $this->mWorkSchedule2 = $this->findSlotValid(2);
        if(is_null($this->mWorkSchedule2)){
            $this->addError("employee_id_2", "Thông tin ca làm việc điều phối 2 không hợp lệ.");
        }
    }
    
    /**
     * @Author: ANH DUNG May 01, 2016
     * @Todo: find slot with some attribute
     * @param: $index dp1, dp2
     */
    public function findSlotValid($index) {
        $date_work = 1;
        if(strpos($this->{"date_work_$index"}, '/')){
            $date_work = MyFormat::dateConverDmyToYmd($this->{"date_work_$index"});
//            MyFormat::isValidDate($this->date_sales);
            $year = MyFormat::dateConverYmdToDmy($date_work, "Y");
            if($year != date("Y")){
                $this->addError("employee_id_1", "Thông tin ca làm việc điều phối $index không hợp lệ.");
            }
        }
        
        $criteria = new CDbCriteria();
        $criteria->compare("t.employee_id", $this->{"employee_id_$index"});
        $criteria->compare("t.date_work", $date_work);
        $criteria->compare("t.slot_id", $this->{"slot_id_$index"});
        return self::model()->find($criteria);
    }
    
    /**
     * @return array relational rules.
     */
    public function relations()
    {
        return array(
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'employee_id' => 'Employee',
            'date_work' => 'Date Work',
            'slot_id' => 'Slot',
            'employee_id_old' => 'Status Change Date',
            'work_schedule_id_change' => 'Work Schedule Id Change',
            'weekNo' => 'Tuần Thứ',
//            'year' => 'Work Schedule Id Change',
            
            'employee_id_1' => 'Nhân Viên',
            'date_work_1' => 'Ngày',
            'slot_id_1' => 'Ca Làm Việc',
            'employee_id_2' => 'Nhân Viên',
            'date_work_2' => 'Ngày',
            'slot_id_2' => 'Ca Làm Việc',
            'week_number' => 'Tuần',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        die;// May 01, 2016 not use this function
    }

    public function defaultScope()
    {
        return array();
    }
 
    /**
     * @Author: ANH DUNG Apr 28, 2016
     */
    public function deleteByWeekAndYear($date_from, $date_to) {
        $criteria = new CDbCriteria();
        $criteria->addBetweenCondition("date_work",$date_from, $date_to); 
        self::model()->deleteAll($criteria);
    }
    
    /**
     * @Author: ANH DUNG Jan 01, 2017
     * @todo: build array insert
     * @param: $aUserOffTwoDay  array 2 user  đã off ở tuần trc
     * @note: tư tưởng là 1/ build hết mảng ngày nào cũng có 4 người đi làm
     *  2/ sau đó có sẽ thay thế t6,t7 1 người off
     *  3/ sau đó có sẽ thay thế chủ nhật 2 người off
     */
    public function buildForOneWeekNew($weekNo, &$aRowInsert, &$aUserOffTwoDay) {
        
    }
    
    /**
     * @Author: ANH DUNG Apr 28, 2016
     * @todo: build array insert
     * @param: $aUserOffTwoDay  array 2 user  đã off ở tuần trc
     * @note: tư tưởng là 1/ build hết mảng cho phép ngày nào cũng có user off
     *  2/ sau đó có sẽ thay thế những thằng off 2 ngày = 1 cá 1 hoặc 3 gì đó
     */
    public function buildForOneWeek($weekNo, &$aRowInsert, &$aUserOffTwoDay) {
        $aRes = array();
        $aDate = MyFormat::dateByWeekNumber($this->year, $weekNo);
        $aDayOfWeek     = MyFormat::getArrayDay($aDate['date_from'], $aDate['date_to']);
        $aCheckOff      = array();// array temp to check user only off max 2 times one week
        $aUserOffDate   = array();// array save data off of user
        $aDataRecheck   = array();// array save temp data before replace user off 2 lan

        $insert = 0;
        $sAssignPrev = "";
        foreach($aDayOfWeek as $day){
//            $aAssign = $this->shuffle_assoc($this->getArrEmployeeAssignToSlot());
            $aAssign = $this->genAutoCheckSameScheduleInNextDay($sAssignPrev);
            $ok = false;
            while (!$ok) {
                $ok = $this->isValidArray($aCheckOff, $aAssign, $day, $aUserOffDate);
                if($ok){
                    $insert++;
                    // xử lý đưa ra 1 array để replace thằng nào off 2 ngày 1 tuần = 1 slot ca 1 hoặc 3 gì đó
                    $this->addToArrayRecheck($aDataRecheck, $aAssign, $day);
//                    $this->addToInsert($aRowInsert, $aAssign, $day);
                }else{
                    $aAssign = $this->shuffle_assoc($this->getArrEmployeeAssignToSlot());
                }
            }
        }
        // May 01, 2015 xử lý chia đều ngày off của nv là 78 ngày 1 năm, 2 nv off 2 ngày 1 tuần thì tuần sau sẽ là 2 nv khác
        $valid = $this->replaceArrayRecheck($aDataRecheck, $aUserOffDate, $aUserOffTwoDay);
        if(!$valid){
            $this->buildForOneWeek($weekNo, $aRowInsert, $aUserOffTwoDay);
        }else{
            $this->replaceDayNotUserOff($aDataRecheck);// replace Ca3 = ca 1
            $this->addToInsert($weekNo, $aRowInsert, $aDataRecheck);
        }
        // May 01, 2015 xử lý chia đều ngày off của nv là 78 ngày 1 năm, 2 nv off 2 ngày 1 tuần thì tuần sau sẽ là 2 nv khác
    }
    
    /**
     * @Author: ANH DUNG May 05, 2016
     * @Todo: check trùng ca giữa 2 ngày liên tiếp, ca off
     */
    public function genAutoCheckSameScheduleInNextDay(&$sAssignPrev) {
        $run = true;
        while ($run) {
            $aAssign = $this->shuffle_assoc($this->getArrEmployeeAssignToSlot());
            $key = array_search(GasWorkSchedule::CA_OFF, $aAssign); // $key = 2;
            $sAssignNew = $key."-".GasWorkSchedule::CA_OFF;
            if($sAssignNew != $sAssignPrev){
                $run = false;
                $sAssignPrev = $sAssignNew;
            }
        }
        return $aAssign;
    }
    
    /**
     * @Author: ANH DUNG Apr 28, 2016
     */
    public function isValidArray(&$aCheckOff, $aAssign, $day, &$aUserOffDate) {
        $ok = true;
        foreach($aAssign as $employee_id=>$slot_id){
            $key = "$employee_id-$slot_id";
            if(isset($aCheckOff[$key])){
                $aCheckOff[$key] += 1;
            }else{
                $aCheckOff[$key] = 1;
            }
            if($aCheckOff[$key] > 2 && $slot_id == GasWorkSchedule::CA_OFF){
                $aCheckOff[$key] = 2;
                return false;
            }
            if($slot_id == GasWorkSchedule::CA_OFF){
                $aUserOffDate[$employee_id][$day][GasWorkSchedule::CA_OFF] = GasWorkSchedule::CA_OFF;
            }
        }
        
        return $ok;
    }
    
    /**
     * @Author: ANH DUNG Apr 28, 2016
     * build array tương tự array của array lưu thằng nào off 2 ngày 1 tuần
     */
    public function addToArrayRecheck(&$aDataRecheck, $aAssign, $day) {
        foreach($aAssign as $employee_id=>$slot_id){
            $aDataRecheck[$day][$employee_id][$slot_id] = $slot_id;
        }
    }
    
    /**
     * @Author: ANH DUNG Apr 28, 2016
     * thay thế thằng nào off 2 ngày 1 tuần trong array $aDataRecheck,
     * hiện tại có 3 người off 2 ngày 1 tuần , theo anh Long thi se de 2 nguoi off 2 ngay 1 tuan
     * @param: $aDataRecheck array schedule hợp lệ chưa replace thằng nào off 2 ngày 1 tuần
     * @param: $aUserOffDate array lưu những ngày off của User trong 1 tuần
     */
    public function replaceArrayRecheck(&$aDataRecheck, $aUserOffDate, &$aUserOffTwoDay) {
        // 1. bỏ key user nào off 1 ngày trong tuần đi, sau đó random array user còn lại
        foreach($aUserOffDate as $employee_id=>$aTemp){
            $index = 0;
            if(count($aTemp)==1){
                unset($aUserOffDate[$employee_id]);
            }
        }
        // 2. random user còn lại, vì chắc chắn những user còn lại sẽ có 2 ngày off trong tuần, ta sẽ replace 1 ngày của 1 trong các user đó
        $employee_id_rand = array_rand($aUserOffDate);                
        $dayRand = array_rand($aUserOffDate[$employee_id_rand]);
        $dayName = MyFormat::dateConverYmdToDmy($dayRand, "l");
        while ($dayName == "Sunday"){// chủ nhật luôn có người off :d
            $dayRand = array_rand($aUserOffDate[$employee_id_rand]);
            $dayName = MyFormat::dateConverYmdToDmy($dayRand, "l");
        }
        $aDataRecheck[$dayRand][$employee_id_rand][GasWorkSchedule::CA_OFF] = GasWorkSchedule::CA_1;
        unset($aUserOffDate[$employee_id_rand]);
        // xu ly cho 2 user off cach nhau 1 tuan thi moi off tiep 2 ngay
        
        $valid = true;// 1. 
        // 1.  change lần 2: xử lý 1 tuần chỉ có 1 người off 2 ngày change lần 2
        // 2.  change lần 1:  xử lý 1 tuần chỉ có 2 người off 2 ngày change lần 1
        foreach($aUserOffDate as $employee_id=>$aTemp){
            if(in_array($employee_id, $aUserOffTwoDay)){
                $valid = false;
                break;
            }
        }
        if($valid){
            if(in_array(GasTickets::DIEU_PHOI_CHUONG, $aUserOffTwoDay)){
                $aUserOffTwoDay = array(GasTickets::DIEU_PHOI_THAO, GasTickets::DIEU_PHOI_THUY);
            }else{
                $aUserOffTwoDay = array(GasTickets::DIEU_PHOI_CHUONG, GasTickets::DIEU_PHOI_TRANG);
            }
        }
        return $valid; // below là code cũ
        
        $countRun = 0;// Apr 29, 2016 biến check chỉ cho phép chạy 1 lần
        // theo y/c mới 1 tuần sẽ có 1 ngày 4 người làm, còn lại cộng phép và ngày lễ vào off hết luôn
        foreach($aUserOffDate as $employee_id=>$aTemp){
            $index = 0;
            if(count($aTemp)==1){
                continue;
            }
            if($countRun > 0 ){ // chỉ thay thể 1 ngày off của nhân viên có 2 ngày off trong tuần
                return ;
            }
            foreach($aTemp as $day=>$aSlot){
                if($index !=0 ){// chỉ thay thế 1 ngày off trong tuần, vì 1 user sẽ có thể có 2 ngày off, hoặc 1 ngày off
                    continue;
                }
                foreach($aSlot as $slot_id){
                    $aDataRecheck[$day][$employee_id][$slot_id] = GasWorkSchedule::CA_1;
//                    echo "$employee_id - $day - $slot_id == ";
                }
                $index++;
            }
            $countRun ++ ;
        }
    }
    
    /**
     * @Author: ANH DUNG Apr 28, 2016
     * thay thế CA3 của ngày không có thằng nào off 
     * @param: $aDataRecheck array schedule hợp lệ đã replace thằng nào off 2 ngày 1 tuần
     */
    public function replaceDayNotUserOff(&$aDataRecheck) {
        foreach($aDataRecheck as $day=>$aTemp){
            $countOff = 0;
            $employeeCa3Replace = 0;
            foreach($aTemp as $employee_id=>$aSlot){
                foreach($aSlot as $slot_id){
                    if($slot_id == GasWorkSchedule::CA_OFF){
                        $countOff++;
                    }elseif($slot_id == GasWorkSchedule::CA_3){
                        $employeeCa3Replace = $employee_id;
                    }
                }
            }
            if($countOff == 0 && isset($aDataRecheck[$day][$employeeCa3Replace][GasWorkSchedule::CA_3])){
                $aDataRecheck[$day][$employeeCa3Replace][GasWorkSchedule::CA_3] = GasWorkSchedule::CA_2_LONG;
            }
        }
    }
    
    /**
     * @Author: ANH DUNG Apr 28, 2016
     * @param: $aRowInsert array save data insert to db
     * @param: $aDataRecheck array data valid after validate rule
     */
    public function addToInsert($weekNo, &$aRowInsert, $aDataRecheck) {
        foreach($aDataRecheck as $date_work=>$aTemp){
            foreach($aTemp as $employee_id=>$aSlot){
                foreach($aSlot as $slot_id){
                    $aRowInsert[] = "('$employee_id',
                        '$weekNo',
                        '$date_work',
                        '$slot_id'
                    )";
                }
            }
        }
    }
    
    /**
     * @Author: ANH DUNG Apr 28, 2016
     * @Todo: save one year to db
     */
    public function saveOneYear($aRowInsert) {
        $tableName = GasWorkSchedule::model()->tableName();
        $sql = "insert into $tableName (employee_id,
                        week_number,
                        date_work,
                        slot_id
                        ) values ".implode(',', $aRowInsert);
        if(count($aRowInsert)>0){
            Yii::app()->db->createCommand($sql)->execute();
        }
    }
    
    /**
     * @Author: ANH DUNG Apr 28, 2016
     * @Todo: http://stackoverflow.com/questions/14370551/reverse-array-values-while-keeping-keys
     * @Param: $a is array
     */
    public function shuffle_assoc($a) {
        $k = array_keys($a);
        $v = array_values($a);
        shuffle($v);
        $b = array_combine($k, $v);
        return $b;
    } 
    
    /**
     * @Author: ANH DUNG Apr 28, 2016
     */
    public function getAll() {
        $aRes = array();
        $criteria = new CDbCriteria();
//        $criteria->compare("YEAR(date_work)", $this->year);
        $criteria->addBetweenCondition("t.date_work", $this->date_from, $this->date_to); 
        $models = self::model()->findAll($criteria);
        foreach($models as $model){
            $aRes[$model->date_work][$model->employee_id][$model->slot_id] = 1;
        }
        return $aRes;
    }

    /**
     * @Author: ANH DUNG May 01, 2016
     * @Todo: save change slot
     */
    public function saveChangeSlot() {
        /* 
         * 4.1 + Rule 1: Đổi lịch làm việc trong 1 ngày giữa các điều phối<br>
         * 4.2 + Rule 2: Đổi ca off của 1 người trong 1 ngày với 1 ca bất kỳ của ngày có 4 người đi làm, khi đó hệ thống sẽ tự sinh lại lịch cho 2 ngày đổi
         */
        $employee_id1 = $this->mWorkSchedule1->employee_id;
        $employee_id2 = $this->mWorkSchedule2->employee_id;
        $ok = true;
        // Rule 1
        if($this->date_work_1 == $this->date_work_2 && $this->slot_id_1 != $this->slot_id_2){
            $this->scenario = null;
            $aUpdate = array('employee_id_old', 'employee_id');
            // 1. xử lý đổi employee cho model 1
            $this->mWorkSchedule1->employee_id_old = $employee_id1;
            $this->mWorkSchedule1->employee_id = $employee_id2;
            $this->mWorkSchedule1->update($aUpdate);
            
            // 2. xử lý đổi employee cho model 2
            $this->mWorkSchedule2->employee_id_old = $employee_id2;
            $this->mWorkSchedule2->employee_id = $employee_id1;
            $this->mWorkSchedule2->update($aUpdate);
        }elseif($this->date_work_1 != $this->date_work_2 && $this->slot_id_1 != $this->slot_id_2){
            // Rule 2:
            $ok = $this->handleChangeRule2();
        }else{// ngày đổi không hợp lệ, nghĩa là không nằm vào 2 rule bên trên
            $this->addError("employee_id_1", "Thông tin đổi lịch không hợp lệ. Vui lòng xem lại quy định về đổi ca");
            $ok = false;
        }
        return $ok;
    }
    
    /**
     * @Author: ANH DUNG May 01, 2016
     * @Todo: đổi ngày giữa các record 3 và 4 người
     */
    public function handleChangeRule2() {
        $date_work1 = MyFormat::dateConverDmyToYmd($this->date_work_1);
        $date_work2 = MyFormat::dateConverDmyToYmd($this->date_work_2);
        $count1 = $this->countSlotOffByDate($date_work1);
        $count2 = $this->countSlotOffByDate($date_work2);
        if($count1 == $count2){// nếu 2 ngày cùng có người off thì sai
            $this->addError("employee_id_1", "Thông tin đổi lịch không hợp lệ. Vui lòng xem lại quy định về đổi ca");
            return false;
        }
        
        $aWorkSchedule1 = $this->getRecordByDate($date_work1);
        $aWorkSchedule2 = $this->getRecordByDate($date_work2);
        foreach($aWorkSchedule1 as $item){
            $item->date_work = $date_work2;
            $item->update(array('date_work'));
        }
        foreach($aWorkSchedule2 as $item){
            $item->date_work = $date_work1;
            $item->update(array('date_work'));
        }
        return true;
    }
    
    /**
     * @Author: ANH DUNG May 01, 2016
     */
    public function getRecordByDate($date_work) {
        $criteria = new CDbCriteria();
        $criteria->compare("t.date_work", $date_work);
        return self::model()->findAll($criteria);
    }
    
    /**
     * @Author: ANH DUNG May 01, 2016
     * @Todo: xử lý check xem ngày đó có 3 người làm hay 4 người làm
     * nếu 3 người thì sẽ có 1 người off, nên ta sẽ count theo slot Off của User
     */
    public function countSlotOffByDate($date_work) {
        $criteria = new CDbCriteria();
        $criteria->compare("date_work", $date_work);
        $criteria->compare("slot_id", GasWorkSchedule::CA_OFF);
        return self::model()->count($criteria);
    }
    
    /**
     * @Author: ANH DUNG May 01, 2016
     * @Todo: count by slot of NV
     * @param: $index dp1, dp2
     */
    public function countBySlot($slot_id) {
        $criteria = new CDbCriteria();
        $criteria->select = "count(t.id) as id, t.employee_id";
        $criteria->compare("YEAR(date_work)", $this->year);
        $criteria->compare("slot_id", $slot_id);
        $criteria->group = "t.employee_id";
        $models = self::model()->findAll($criteria);
        $aRes = array();
        foreach($models as $item){
            $aRes[$item->employee_id] = $item->id;
        }
        return $aRes;
    }
    
}