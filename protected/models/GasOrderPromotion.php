<?php

/**
 * This is the model class for table "{{_gas_order_promotion}}".
 *
 * The followings are the available columns in table '{{_gas_order_promotion}}':
 * @property string $id
 * @property string $code_no
 * @property string $agent_id
 * @property string $uid_login
 * @property string $user_id_delivery
 * @property string $note
 * @property string $created_date
 * @property string $status
 */
class GasOrderPromotion extends BaseSpj
{
    public $mDetail;
    public $aModelDetail;
    public $aIdDetailNotDelete;
    public $MAX_ID;
    public $autocomplete_name, $autocomplete_name_1;
    public $ext_materials_id;
    public $ext_materials_multi;
    public $autocomplete_material;
    public $material_by_agent;// May 16, 2016 cho phần search của Quân ở index
    const GROUP_KHO = 1;
    const GROUP_AGENT = 2;
    
    const STATUS_NEW = 1;
    const STATUS_COMPLETE = 2; // đã giao
    const STATUS_CONFIRM = 3;// Người nhận xác nhận để gửi lên Xuân Thủy
    const STATUS_DELIVERY_UPDATED = 4;// Người nhận đã có thao tác cập nhật
    const STATUS_COMPLETE_MISSING = 5;// đã giao không đủ
    
    public function getArrayStatus() {
        return array(
            GasOrderPromotion::STATUS_NEW => "Mới",
            GasOrderPromotion::STATUS_COMPLETE => "Đã giao đủ",
            GasOrderPromotion::STATUS_CONFIRM => "Đã xác nhận lại",
            GasOrderPromotion::STATUS_DELIVERY_UPDATED => "Đang cập nhật",
            GasOrderPromotion::STATUS_COMPLETE_MISSING => "Đã giao thiếu",
        );
    }
    
    public function getArrayGroupMaterial() {
        return array(
            GasOrderPromotion::GROUP_KHO => "Kho",
            GasOrderPromotion::GROUP_AGENT => "Đại Lý",
        );
    }
    public $ARR_STATUS_CLOSE = array(GasOrderPromotion::STATUS_CONFIRM, GasOrderPromotion::STATUS_COMPLETE, GasOrderPromotion::STATUS_COMPLETE_MISSING);
    
    public $aStatusCanUpdate = array(GasOrderPromotion::STATUS_NEW);
    public $aStatusViewNew = array(GasOrderPromotion::STATUS_NEW, GasOrderPromotion::STATUS_CONFIRM);
    
    public $aAgentAllowSummary = array(MyFormat::KHO_BEN_CAT);
    public static $ARRR_ID_SUMMARY = array(
        GasSupportCustomer::UID_THUY_MDX,
    );

    public $date_from;
    public $date_to;
    
    // Now 06, 2015 lấy số ngày cho phép đại lý cập nhật
    public function getDayAllowUpdate(){
        return Yii::app()->params['days_update_order_promotion'];
    }
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{_gas_order_promotion}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        return array(
            array('agent_id, user_id_delivery', 'required', 'on'=>'create, update'),
            array('id, code_no, agent_id, uid_login, user_id_delivery, note, created_date', 'safe'),
            array('json_tracking_date, material_by_agent, ext_materials_multi, ext_materials_id, date_from,date_to,province_id, note_after_receive, json_inventory, status', 'safe'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        return array(
            'rUidLogin' => array(self::BELONGS_TO, 'Users', 'uid_login'),
            'rAgent' => array(self::BELONGS_TO, 'Users', 'agent_id'),
            'rUserIdDelivery' => array(self::BELONGS_TO, 'Users', 'user_id_delivery'),
            'rDetail' => array(self::HAS_MANY, 'GasOrderPromotionDetail', 'order_promotion_id',
                'order'=>'rDetail.id ASC',
            ),
            'rMaterial' => array(self::BELONGS_TO, 'GasMaterials', 'ext_materials_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'code_no' => 'Mã số',
            'agent_id' => 'Đại lý',
            'uid_login' => 'Người tạo',
            'user_id_delivery' => 'Người nhận',
            'note' => 'Ghi chú',
            'created_date' => 'Ngày tạo',
            'status' => 'Trạng Thái',
            'note_after_receive' => 'Ghi chú sau khi nhận vật tư',
            'date_from' => 'Từ ngày',
            'date_to' => 'Đến ngày',
            'ext_materials_id' => 'Vật tư',
            'ext_materials_multi' => 'Chọn nhiều vật tư',
            'material_by_agent' => 'Chọn nhóm vật tư',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        $cRole = Yii::app()->user->role_id;
        $cUid = Yii::app()->user->id;
        $cAgent = MyFormat::getAgentId();
        $aWith = array();
        $criteria=new CDbCriteria;
        $criteria->compare('t.code_no',$this->code_no,true);
        if(is_array($this->status)){
            $sParamsIn = implode(',', $this->status);
            $criteria->addCondition("t.status IN ($sParamsIn)");
        }else{
            $criteria->compare('t.status', $this->status);
        }
        
        $criteria->compare('t.agent_id',$this->agent_id);
        $criteria->compare('t.uid_login',$this->uid_login);
        $criteria->compare('t.user_id_delivery',$this->user_id_delivery);
        if(!empty($this->date_from)){
            $date_from = MyFormat::dateDmyToYmdForAllIndexSearch($this->date_from);
            $criteria->addCondition("DATE(t.created_date) >= '$date_from'");
        }
        
        if(!empty($this->date_to)){
            $date_to = MyFormat::dateDmyToYmdForAllIndexSearch($this->date_to);
            $criteria->addCondition("DATE(t.created_date) <= '$date_to'");
        }
        
        if(!empty($this->ext_materials_id)){
            $aWith[] = "rDetail";
            $criteria->compare('rDetail.materials_id', $this->ext_materials_id);
            $criteria->together = true;
        }
        if(is_array($this->ext_materials_multi)){
            $sParamsIn = implode(',', $this->ext_materials_multi);
            $criteria->addCondition("rDetail.materials_id IN ($sParamsIn)");
            if(count($aWith) == 0){
                $aWith[] = "rDetail";
                $criteria->together = true;
            }
        }
        
        if(count($aWith)){
            $criteria->with = $aWith;
        }
        
        $aUidDelivery = $this->getArrayUserDelivery();
        if(isset($aUidDelivery[$cUid])){
//            $criteria->addCondition('t.user_id_delivery='.$cUid);
            if($cUid == GasLeave::UID_VIEW_ALL_TAY_NGUYEN_GIA_LAI){// Now 18, 2016 xử lý cho Tuyết nhìn thấy của Châu Thị Mỹ Liễu
                $criteria->addCondition('t.user_id_delivery='.$cUid.' OR t.user_id_delivery='.GasConst::UID_LIEU_CTM);
            }else{
                $criteria->addCondition('t.user_id_delivery='.$cUid);
            }
//            $criteria->addInCondition('t.user_id_delivery', array_keys($aUidDelivery));
        }elseif($cRole == ROLE_SUB_USER_AGENT){
            if($cAgent == MyFormat::KHO_BEN_CAT){
                $criteria->compare('t.province_id', GasProvince::TINH_BINH_DUONG);
            }elseif($cAgent == GasCheck::KHO_LY_CHINH_THANG){
                $sParamsIn = implode(',', [GasProvince::TINH_GIALAI, GasProvince::TINH_KONTUM]);
                $criteria->addCondition("t.province_id IN ($sParamsIn)");
            }else{
                $criteria->compare('t.agent_id', $cAgent);
            }
        }
        
        if(in_array($cUid, GasOrderPromotion::$ARRR_ID_SUMMARY)){
            $criteria->compare('t.province_id', GasProvince::TP_HCM);
        }
        $criteria->order = "t.id DESC";
        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
            'pagination'=>array(
                'pageSize'=> Yii::app()->user->getState('pageSize',Yii::app()->params['defaultPageSize']),
            ),
        ));
    }
    
    public function defaultScope()
    {
        return array();
    }
    
    /**
     * @Author: ANH DUNG Nov 06, 2015
     * @Todo: get array user nhận
     */
    public function getArrayUserDelivery() {
        $needMore = array('get_id_name'=>1);// Dec 30, 2015
        return GasOneManyBig::getArrayModelUser(GasOneManyBig::TYPE_ORDER_PROMOTION, GasOneManyBig::TYPE_ORDER_PROMOTION, $needMore);
        return array(
            GasSupportCustomer::UID_THUY_MDX => "Mai Đào Xuân Thủy"
        );
    }
    
    protected function beforeValidate() {
        self::BuildArrDetail($this);
        return parent::beforeValidate();
    }
    
    /**
     * @Author: ANH DUNG Oct 18, 2016
     * @Todo: cập nhật list ngày nhận hàng của đại lý
     */
    public function setTrackingDate() {
        $trackingDate = array();
        if(!empty($this->json_tracking_date)){
            $trackingDate = json_decode($this->json_tracking_date, true);
        }
        $trackingDate[] = date("d/m/Y H:i:s");
        $this->json_tracking_date = json_encode($trackingDate);
    }
    
    public function getTrackingDate() {
        if(empty($this->json_tracking_date)){
           return "" ;
        }
        $trackingDate = json_decode($this->json_tracking_date, true);
        $trackingDate = implode("<br>", $trackingDate);
        return $trackingDate;
    }
    
    /**
     * @Author: ANH DUNG Now 06, 2015
     * @Todo: build array model detail from post
     * @Param: $model is model gasOrderPromotion
     */
    public static function BuildArrDetail($model) {
        $isComplete = false;
        $isMissing = false;
        $model->aModelDetail = array();
        $model->aIdDetailNotDelete = array();
        if(isset($_POST['GasOrderPromotionDetail']['materials_id']) && is_array($_POST['GasOrderPromotionDetail']['materials_id'])){
            foreach( $_POST['GasOrderPromotionDetail']['materials_id'] as $key=>$materials_id){                
                $qty = isset($_POST['GasOrderPromotionDetail']['qty'][$key])?$_POST['GasOrderPromotionDetail']['qty'][$key]:"";
                $qty_real = isset($_POST['GasOrderPromotionDetail']['qty_real'][$key])?$_POST['GasOrderPromotionDetail']['qty_real'][$key]:"";
                $inventory = isset($_POST['GasOrderPromotionDetail']['inventory'][$key])?$_POST['GasOrderPromotionDetail']['inventory'][$key]:"";
                $id = isset($_POST['GasOrderPromotionDetail']['id'][$key])?$_POST['GasOrderPromotionDetail']['id'][$key]:"";
                $note = isset($_POST['GasOrderPromotionDetail']['note'][$key])?$_POST['GasOrderPromotionDetail']['note'][$key]:"";
                
                if($materials_id ){
                    if(!empty($id)){
                        $mDetail = GasOrderPromotionDetail::model()->findByPk($id);
                        if(!$mDetail){
                            continue;
                        }
                        $model->aIdDetailNotDelete[] = $id;
                    }else{
                        if( $qty <= 0 ){// Oct 06, 2015 required input slg
                            continue;
                        }
                        $mDetail = new GasOrderPromotionDetail();
                    }
                    $mDetail->materials_id = $materials_id;
                    $mDetail->qty = $qty;
                    $mDetail->qty_real = $qty_real;
                    $mDetail->inventory = $inventory;
                    $mDetail->note = $note;
                    
                    $mDetail->materials_type_id = $mDetail->rMaterials?$mDetail->rMaterials->materials_type_id : 0;
                    $model->aModelDetail[] = $mDetail;
                    if($qty_real > 0){
                        $isComplete = true;
                    }
                    if($qty_real < $qty){
                        $isMissing = true;
                    }
                }
            }
        }
        if($isComplete){// Giao du
            $model->status = self::STATUS_COMPLETE;
            $model->time_update_complete = date("Y-m-d H:i:s");
            $model->setTrackingDate();
        }
        
        if($isComplete && $isMissing){// Giao thieu
            $model->status = self::STATUS_COMPLETE_MISSING;
        }
    }
    
    protected function beforeSave() {
        if($this->isNewRecord){
            $this->uid_login = Yii::app()->user->id;
            $this->code_no = MyFunctionCustom::getNextId('GasOrderPromotion', 'P'.date('y'), MyFormat::MAX_LENGTH_8, 'code_no');
        }
        if(isset($_POST['json_inventory']) && $this->status == self::STATUS_NEW ){
            $this->json_inventory = json_encode($_POST['json_inventory']);
        }
        $mAgent = $this->rAgent;
        if($mAgent){
            $this->province_id = $mAgent->province_id;
        }
        return parent::beforeSave();
    }
    
    protected function afterSave() {
        $this->SaveDetailItem();
        GasOrderPromotionDetail::UpdateStatusAndUserDelivery($this);
        return parent::afterSave();
    }
    
    /**
     * @Author: ANH DUNG Now 06, 2015
     * @Todo: save detail 
     */
    public function SaveDetailItem() {
//        MyFormat::deleteModelDetailByRootId('GasSupportCustomerItem', $model->id, 'support_customer_id');
        // không thể delete detail vì còn các cấp trên update % cho từng detail
        $this->DeleteSomeDetail();
        foreach( $this->aModelDetail as $mDetail )
        {
            // Dec 30, 2014 chỗ này không cần 1 build sql, vì cảm thấy lưu lượng dùng chỗ này cũng ít
            $mDetail->order_promotion_id = $this->id;
            if($mDetail->id)
                $mDetail->update();
            else
                $mDetail->save();
        }
    }
    
    /**
     * @Author: ANH DUNG Now 06, 2015
     * @Todo: delete 
     * @Param: $model
     */
    public function DeleteSomeDetail() {
        if(is_array($this->aIdDetailNotDelete) && count($this->aIdDetailNotDelete)){
            $criteria = new CDbCriteria;
            $criteria->compare('order_promotion_id', $this->id);
            $sParamsIn = implode(',', $this->aIdDetailNotDelete);
            $criteria->addCondition("id NOT IN ($sParamsIn)");
            GasOrderPromotionDetail::model()->deleteAll($criteria);
        }
    }
    
    public function getUidLogin() {
        $mUser = $this->rUidLogin;
        if($mUser){
            return $mUser->first_name;
        }
        return '';
    }
    
    public function getAgent() {
        $mUser = $this->rAgent;
        if($mUser){
            return $mUser->first_name;
        }
        return '';
    }
    
    public function getUserDelivery() {
        $mUser = $this->rUserIdDelivery;
        if($mUser){
            return $mUser->first_name;
        }
        return '';
    }
    
    public function getNote() {
        return nl2br($this->note);
    }
    
    public function getCreatedDate() {
        return MyFormat::dateConverYmdToDmy($this->created_date);
    }
    
    public function getStatus() {
        $aStatus = $this->getArrayStatus();
        return $aStatus[$this->status];
    }
    
    protected function beforeDelete() {
        MyFormat::deleteModelDetailByRootId('GasOrderPromotionDetail', $this->id, 'order_promotion_id');
        return parent::beforeDelete();
    }
    
    /**
     * @Author: ANH DUNG Now 06 2015
     * @Todo: render table detail of item
     */
    public function formatDetail(){
        $tfood = false;
        $qty=0; $qty_real=0; $qty_inventory = 0;
       $str = "<table  cellpadding='0' cellspacing='0' class='f_size_12'> ";
       $str .= "<tr>";
       $str .= "<td>#</td>";
       $str .= "<td class='w-180'> Vật Tư</td>";
       $str .= "<td class='w-20'>ĐV</td>";
       $str .= "<td class='w-30'>Tồn kho</td>";
       $str .= "<td class='w-30'>Slg</td>";
       $str .= "<td class='w-30'>Slg Thực Nhận</td>";
       $str .= "<td class='w-150'>Ghi Chú</td>";
       $str .= "</tr>";
       
       foreach($this->rDetail as $key=>$item){
           $ClassHighlight = "";
            if($item->qty > $item->qty_real && $this->status == self::STATUS_COMPLETE_MISSING){
                $ClassHighlight = "high_light_tr";
            }
           $tfood = true;
           $qty += $item->qty;
           $qty_real += $item->qty_real;
           $qty_inventory += $item->inventory;
           $str .= "<tr>";
//           $type = GasSupportCustomer::$ARR_TYPE[$item->type];
//           $str .= "<br> <b>".(++$key).". </b>$type - {$item->item_name}";
           
           $str .= "<td style='border:1px solid #000;'><span class='$ClassHighlight'>".($key+1)."</span></td>";
           $str .= "<td  style='border:1px solid #000;'>{$item->getMaterialName()}</td>";
           $str .= "<td  style='border:1px solid #000;'>{$item->unit}</td>";
           $str .= "<td style='text-align:right; border:1px solid #000;'>".ActiveRecord::formatCurrency($item->inventory)."</td>";
           $str .= "<td style='text-align:right; border:1px solid #000;'>".ActiveRecord::formatCurrency($item->qty)."</td>";
           $str .= "<td style='text-align:right; border:1px solid #000;'>".ActiveRecord::formatCurrency($item->qty_real)."</td>";
           $str .= "<td  style='border:1px solid #000;'>{$item->getNote()}</td>";
           $str .= "</tr>";
       }
       if($tfood){
           $str .= "<tr>";
           $str .= "<td style='text-align:right;font-weight: bold;' colspan='3'><b>Tổng cộng</b></td>";
           $str .= "<td style='text-align:right;font-weight: bold;'><b>".ActiveRecord::formatCurrency($qty_inventory)."</b></td>";
           $str .= "<td style='text-align:right;font-weight: bold;'><b>".ActiveRecord::formatCurrency($qty)."</b></td>";
           $str .= "<td style='text-align:right;font-weight: bold;'><b>".ActiveRecord::formatCurrency($qty_real)."</b></td>";
           $str .= "</tr>";
       }
       $str .= "</table>";
       $str = "<p style='display:inline-block;'>$str</p>";
       return $str; 
    }
    
    /**
     * @Author: ANH DUNG Jul 01, 2016
     * @Todo: Print
     */
    public function formatDetailPrint(){
        $tfood = false;
        $qty=0; $qty_real=0; $qty_inventory = 0;
       $str = "<table  cellpadding='0' cellspacing='0' class='tb hm_table border_black'> ";
       $str .= "<thead>";
       $str .= "<tr>";
       $str .= "<td class='w-30 item_b item_c'>#</td>";
       $str .= "<td class='w-200 item_b item_c'> Vật tư</td>";
       $str .= "<td class='item_b item_c'>ĐV</td>";
       $str .= "<td class='item_b item_c'>Tồn kho</td>";
       $str .= "<td class='item_b item_c'>Slg đặt</td>";
       $str .= "<td class='item_b item_c'>Slg xuất thực tế</td>";
       $str .= "<td class='item_b item_c w-300'>Ghi Chú</td>";
       $str .= "</tr>";
       $str .= "</head>";
       
       foreach($this->rDetail as $key=>$item){
           $ClassHighlight = "";
            if($item->qty > $item->qty_real && $this->status == self::STATUS_COMPLETE_MISSING){
//                $ClassHighlight = "high_light_tr";
            }
           $tfood = true;
           $qty += $item->qty;
           $qty_real += $item->qty_real;
           $qty_inventory += $item->inventory;
           $str .= "<tr>";
//           $type = GasSupportCustomer::$ARR_TYPE[$item->type];
//           $str .= "<br> <b>".(++$key).". </b>$type - {$item->item_name}";
           
           $str .= "<td style='text-align:center; border:1px solid #000;'><span class='$ClassHighlight'>".($key+1)."</span></td>";
           $str .= "<td  style='border:1px solid #000;'>{$item->getMaterialName()}</td>";
           $str .= "<td  style='border:1px solid #000;'>{$item->unit}</td>";
           $str .= "<td style='text-align:center; border:1px solid #000;'>".ActiveRecord::formatCurrency($item->inventory)."</td>";
           $str .= "<td style='text-align:center; border:1px solid #000;'>".ActiveRecord::formatCurrency($item->qty)."</td>";
           $str .= "<td style='text-align:right; border:1px solid #000;'></td>";
           $str .= "<td  style='border:1px solid #000;'></td>";
           $str .= "</tr>";
       }
       if($tfood){
           $str .= "<tr>";
           $str .= "<td style='text-align:right;font-weight: bold;' colspan='3'><b>Tổng cộng</b></td>";
           $str .= "<td style='text-align:center;font-weight: bold;'><b>".ActiveRecord::formatCurrency($qty_inventory)."</b></td>";
           $str .= "<td style='text-align:center;font-weight: bold;'><b>".ActiveRecord::formatCurrency($qty)."</b></td>";
           $str .= "<td></td><td></td>";
           $str .= "</tr>";
       }
       $str .= "</table>";
       return $str; 
    }
    
    /**
     * @Author: ANH DUNG Now 12 2015
     * @Todo: render table detail of item inventory
     */
    public function formatInventory(){
        $session=Yii::app()->session;
        $tfood = false;
        $qty_inventory = 0;
       $str = "<table  cellpadding='0' cellspacing='0' class='f_size_12 delivery_user_update'> ";
       $str .= "<tr>";
       $str .= "<td>#</td>";
       $str .= "<td class='w-180'> Vật Tư</td>";
       $str .= "<td class='w-20'>ĐV</td>";
       $str .= "<td class='w-30'>Tồn kho</td>";
       $str .= "</tr>";        
        $json_inventory = json_decode($this->json_inventory, true);
        $key = 1;
        if(is_array($json_inventory)){
            foreach($json_inventory as $id => $inventory){
                if(!isset($session['MATERIAL_MODEL'][$id])){ continue; }
                $tfood = true;
                $qty_inventory += $inventory;
                $str .= "<tr>";
                $str .= "<td style='border:1px solid #000;'>".($key++)."</td>";
                $str .= "<td  style='border:1px solid #000;'>{$session['MATERIAL_MODEL'][$id]['name']}</td>";
                $str .= "<td  style='border:1px solid #000;'>{$session['MATERIAL_MODEL'][$id]['unit']}</td>";
                $str .= "<td style='text-align:right; border:1px solid #000;'>".ActiveRecord::formatCurrency($inventory)."</td>";
                $str .= "</tr>";
            }
        }
        if($tfood){
           $str .= "<tr>";
           $str .= "<td style='text-align:right;font-weight: bold;' colspan='3'><b>Tổng cộng</b></td>";
           $str .= "<td style='text-align:right;font-weight: bold;'><b>".ActiveRecord::formatCurrency($qty_inventory)."</b></td>";
           $str .= "</tr>";
        }
        $str .= "</table>";
        return $str;
    }
    
    /**
     * @Author: ANH DUNG Nov 13, 2015
     * @Todo: check user can view this record
     */
    public function CanView() {
        $ok = true;
        $cRole = Yii::app()->user->role_id;
        if($cRole == ROLE_AGENT){
            if($this->agent_id != MyFormat::getAgentId()){
                $ok = false;
            }
        }
        return $ok;
    }
    
    /** ANH DUNG Now 13, 2015
     * to do: check can update đặt hàng vật tư khuyến mãi 
     */
    public function CanUpdateOrderPromotion()
    {
//        return true; // only for test
        $cRole = Yii::app()->user->role_id;
        $cUid = Yii::app()->user->id;
        if( ($cRole != ROLE_ADMIN && $cUid != $this->uid_login && !$this->isUserDeliveryUpdate()) 
                || ( $this->isUserDeliveryUpdate() && in_array($this->status, $this->ARR_STATUS_CLOSE) )
                
                ){
            return false; // cho phép admin sửa hết, còn những user khác thì ai tạo thì dc sửa của người đó
        }
        
//        if($cRole == ROLE_SUB_USER_AGENT && $this->status == GasOrderPromotion::STATUS_DELIVERY_UPDATED){
//            return false;
//        }
        
        $dayAllow = date('Y-m-d');
        $dayAllow = MyFormat::modifyDays($dayAllow, $this->getDayAllowUpdate(), '-');
        // nếu là new thì cho update thoải mái, chỉ khi nào nó update complate thì mới check
        if($this->status == self::STATUS_NEW){
            return true;
        }  else {
            return MyFormat::compareTwoDate($this->time_update_complete, $dayAllow);
        }
    }
    
    /**
     * @Author: ANH DUNG Feb 24, 2016
     * @Todo: check user không phải agent có thể update 
     * là những người nhận của khi tạo Đặt Hàng
     */
    public function isUserDeliveryUpdate() {
//        return true; // only for test
        $cUid = Yii::app()->user->id;
        $cRole = Yii::app()->user->role_id;
        $aUserCanUpdate = $this->getArrayUserDelivery();
        if(isset($aUserCanUpdate[$cUid]) || $cRole == ROLE_ADMIN){
            return true;
        }
        return false;
    }
    
    /**
     * @Author: ANH DUNG Apr 25, 2016
     * @Todo: Kiểm tra xem có hiện button xác nhận đơn hàng ko
     */
    public function showConfirmButton() {
//        return true; // only for test
        if($this->status != GasOrderPromotion::STATUS_CONFIRM && $this->status != GasOrderPromotion::STATUS_COMPLETE)
        {
            return true;
        }
        return false;
    }
    
    /**
     * @Author: ANH DUNG Nov 19, 2015
     */
    public function InitMaterial() {
        $session=Yii::app()->session;
        if(!isset($session['MATERIAL_MODEL'])){
            $session['MATERIAL_MODEL'] = MyFunctionCustom::getMaterialsModel(GasMaterialsType::$PROMOTION);
        }
    }
    
    /**
     * @Author: ANH DUNG Feb 19, 2016
     * @Todo: set status for $delivery_user_update
     */
    public function setStatusUserUpdate() {
        if(isset($_POST['p_confirm_order'])){
            $this->status = GasOrderPromotion::STATUS_CONFIRM;
        }
    }
    
    /**
     * @Author: ANH DUNG Feb 19, 2016
     */
    public function getDateSearch(&$aStartAndEndDate) {
        if(!empty($this->date_from)){
            $aStartAndEndDate['week_start'] = MyFormat::dateDmyToYmdForAllIndexSearch($this->date_from);
        }
        if(!empty($this->date_to)){
            $aStartAndEndDate['week_end'] = MyFormat::dateDmyToYmdForAllIndexSearch($this->date_to);
        }
    }
    
    /**
     * @Author: ANH DUNG Feb 24, 2016
     * @Todo: get report summary
     */
    public function Summary(&$aStartAndEndDate) {
        /* 1. xác định khoảng ngày
         * 2. đưa vào findAll và grop tính toán
         * 3. format data xuống view
         */
        $cUid = Yii::app()->user->id;
        $aUserDelivery = $this->getArrayUserDelivery();
        
        $aStartAndEndDate = MyFormat::getStartAndEndDate(MyFormat::getWeekNumber(), date("Y"));
        $this->getDateSearch($aStartAndEndDate);
        
        $criteria = new CDbCriteria();
        $criteria->addBetweenCondition("t.created_date", $aStartAndEndDate['week_start'], $aStartAndEndDate['week_end']);
        if(isset($aUserDelivery[$cUid])){// người nhận đơn hàng view summary sẽ sum hết chưa xác nhận + đã xác nhận 
        }else{// is Chị Thủy
            $criteria->compare("t.status", GasOrderPromotion::STATUS_CONFIRM);
        }
        
        $criteria->select = "sum(t.qty) as qty, t.user_id_delivery, t.materials_id, t.province_id";
        $criteria->group = "t.user_id_delivery, t.province_id, t.materials_id";
        $criteria->order = "t.materials_type_id ASC, t.materials_id ASC";
        $models = GasOrderPromotionDetail::model()->findAll($criteria);
        return $this->SummaryFormat($models);
    }
    
    public function SummaryFormat($models) {
        $aRes = array();
        foreach($models as $model){
            $aRes[$model->user_id_delivery][$model->province_id][$model->materials_id] = $model->qty;
        }
        return $aRes;
    }
    
    /**
     * @Author: ANH DUNG Feb 24, 2016
     * @Todo: 
     */
    public function getSummaryTable($data, $uid = 0, $aDataAccounting=array()){
        $accountingCanEdit = $this->accountingCanEdit();
        $canViewColumnAgent = $this->canViewColumnAgent();
        $canViewColumnAccounting = $this->canViewColumnAccounting();
        $session=Yii::app()->session;
        $tfood = false;
        $qty_inventory = 0;
        $sumAccounting = 0;
        $str = "<table  cellpadding='0' cellspacing='0' class='f_size_12'> ";
        $str .= "<tr>";
        $str .= "<th class='td_last_r'>#</th>";
        $str .= "<th class='w-180 item_c td_last_r'> Vật Tư</th>";
        $str .= "<th class='w-50 item_c td_last_r'>ĐV</th>";
        if($canViewColumnAgent){
            $str .= "<th class='w-50 item_c td_last_r'>SL Đặt</th>";
        }
        if($canViewColumnAccounting){
            $str .= "<th class='w-50 item_c td_last_r'>Kế Toán Đặt</th>";
        }
        $str .= "</tr>";
        $key = 1;
        if(is_array($data)){
            foreach($data as $id => $qty){
                $qtyAccounting = isset($aDataAccounting[$uid][$id]) ? ActiveRecord::formatNumberInput($aDataAccounting[$uid][$id]) : "";
                $sumAccounting += $qtyAccounting;
                if($accountingCanEdit){
                    $inputAccounting = "<input value='$qtyAccounting' name='accounting_qty[$id]' class='accounting_qty item_r number_only_v1 w-50 InputQtyAccounting' type='text' maxlength='6' >";
                }else{
                    $inputAccounting = $qtyAccounting;
                }
                
                $tfood = true;
                $qty_inventory += $qty;
                $str .= "<tr >";
                $str .= "<td class='item_c' style=''>".($key++)."</td>";
                $str .= "<td class='w-300'>{$session['MATERIAL_MODEL'][$id]['name']}</td>";
                $str .= "<td class='item_c'>{$session['MATERIAL_MODEL'][$id]['unit']}</td>";
                if($canViewColumnAgent){
                    $str .= "<td class='' style='text-align:right; '>".ActiveRecord::formatCurrency($qty).""
                        . "<span class='HideQty' style='display:none;'>".ActiveRecord::formatNumberInput($qty)."</span></td>";
                }
                if($canViewColumnAccounting){
                    $str .= "<td class='td_last_r item_r'>$inputAccounting</td>";
                }
                $str .= "</tr>";
            }
        }
        if($tfood){
           $str .= "<tr>";
           $str .= "<td style='' colspan='3'><b>Tổng cộng</b></td>";
           if($canViewColumnAgent){
               $str .= "<td class='item_r'><b>".ActiveRecord::formatCurrency($qty_inventory)."</b></td>";
           }
           if($canViewColumnAccounting){
               $str .= "<td class='item_b SumOneAccounting item_r'>".ActiveRecord::formatCurrency($sumAccounting)."<br><a class='CopySL' href='javascript:void(0);'>Copy SL</a></td>";
           }
           $str .= "</tr>";
        }
        $str .= "</table>";
        return $str;
    }

    /**
     * @Author: ANH DUNG Feb 24, 2016
     */
    public function canUpdateNote() {
        $aCheck = $this->ARR_STATUS_CLOSE;
        if(in_array($this->status, $aCheck)){
            return true;
        }
        return false;
    }
    
    /**
     * @Author: ANH DUNG Feb 24, 2016
     */
    public function getSomeDetail() {
        $res = "<b>Người Nhận:</b> {$this->getUserDelivery()} <br/>";
        if(trim($this->note) != ""){
            $res .= "<br><b>Ghi Chú: </b>". nl2br($this->note);
        }
        if(trim($this->note_after_receive) != ""){
            $res .= "<br><b>".$this->getAttributeLabel('note_after_receive').": </b>". nl2br($this->note_after_receive);
        }
        $trackingDate = $this->getTrackingDate();
        if(trim($trackingDate) != ""){
            $res .= "<br><b>Lịch sử nhận hàng: </b><br><br>". $trackingDate;
        }
        
        return $res;
    }
    
    /**
     * @Author: ANH DUNG Mar 11, 2016
     * @Todo: handle post kế toán đặt
     * @param: $controller to redirect :D
     */
    public function handleAccountingQty($cController) {
        if(isset($_POST['accounting_qty'])){
            // 1. delete old data
            GasOrderPromotionAccounting::deleteByUserDeliveryAndDate();
            // 2. insert new
            $aRowInsert=array();
            $created_date = date("Y-m-d");
            $user_id_delivery = Yii::app()->user->id;
            foreach ($_POST['accounting_qty'] as $materials_id => $qty) {
                $mDetail = new GasOrderPromotionDetail();
                $mDetail->qty = $qty;
                $mDetail->materials_id = $materials_id;
                $mDetail->materials_type_id = $mDetail->rMaterials?$mDetail->rMaterials->materials_type_id : 0;
                if($qty>0){
                    $aRowInsert[] = "('$user_id_delivery',
                        '$mDetail->materials_type_id',
                        '$mDetail->materials_id',
                        '$qty',
                        '$created_date'
                    )";
                }
            }
            
            $tableName = GasOrderPromotionAccounting::model()->tableName();
            $sql = "insert into $tableName (user_id_delivery,
                            materials_type_id,
                            materials_id,
                            qty,
                            created_date
                            ) values ".implode(',', $aRowInsert);
            if(count($aRowInsert)>0){
                Yii::app()->db->createCommand($sql)->execute();
            }
            Yii::app()->user->setFlash( MyFormat::SUCCESS_UPDATE, "Cập nhật thành công." );
            $cController->redirect(array('summary','summary'=>1));
        }
    }
    
    /**
     * @Author: ANH DUNG Mar 11, 2016
     * @Todo: get kế toán đặt
     */
    public function getAccountingQty($user_id_delivery, $date_from, $date_to) {
        $criteria = new CDbCriteria();
        if(!empty($user_id_delivery)){
            $criteria->compare("user_id_delivery", $user_id_delivery);
        }
        $criteria->addBetweenCondition("created_date", $date_from, $date_to);
        $models = GasOrderPromotionAccounting::model()->findAll($criteria);
        $aRes = array();
        foreach($models as $item){
            $aRes[$item->user_id_delivery][$item->materials_id] = $item->qty;
        }
        return $aRes;
    }
 
    /**
     * @Author: ANH DUNG Mar 11, 2016
     * kế toán chỉ dc  edit slg dat vào ngày thứ 2,7 hàng tuần
     * @Todo: check user accounting can edit slg dat vào ngày thứ 2,7 hàng tuần
     * 'Monday'  =>'Thứ Hai',
      'Tuesday'  =>'Thứ Ba',
      'Wednesday'  =>'Thứ Tư',
      'Thursday'  =>'Thứ Năm',
      'Friday'  =>'Thứ Sáu',
      'Saturday'  =>'Thứ Bảy',
      'Sunday'  =>'Chủ Nhật',
     */
    public function accountingCanEdit() {
//        return true; // only for dev test
        $cRole  = MyFormat::getCurrentRoleId();
        $cUid   = MyFormat::getCurrentUid();
        if($cRole == ROLE_MONITORING){
            return false;
        }

        $day_of_week = strtolower(date('l'));
        $aDayCheck = array('thursday','monday', 'tuesday', 'saturday', 'sunday');// Now 23, 2016 bổ sung gia lai cho xác nhận vào thứ 5 hàng tuần
//        $aDayCheck = array('monday', 'saturday', 'sunday', 'tuesday');
        $aUidDelivery = $this->getArrayUserDelivery();
        if(in_array($day_of_week, $aDayCheck) && isset($aUidDelivery[$cUid])){
            return true;
        }
        return false;
    }
    
    /**
     * @Author: ANH DUNG Mar 11, 2016
     * @Todo: đại lý chỉ dc đặt ngày t5, 6
     */
    public function agentCanCreate() {
//        return true; // Apr 11, 2016 only for dev test
        $cRole = MyFormat::getCurrentRoleId();
        if($cRole == ROLE_ADMIN){
            return true;
        }
        $day_of_week = strtolower(date('l'));
//        $aDayCheck = array('thursday', 'friday');
        $aDayCheck = array('friday', 'thursday');// đại lý Chỉ có thứ 6 mới dc đặt, t5 nua
//        if(in_array($day_of_week, $aDayCheck) && in_array($cRole, [ROLE_SUB_USER_AGENT, ROLE_CALL_CENTER]) ){
        if(in_array($day_of_week, $aDayCheck)){
            return true;
        }
        return false;
    }
    
    /**
     * @Author: ANH DUNG Mar 11, 2016
     * @Todo: check user có thể view cột đại lý đặt không
     */
    public function canViewColumnAgent() {
        $cRole = Yii::app()->user->role_id;
        if($cRole == ROLE_MONITORING){
            return false;
        }
        return true;
    }
    
    /**
     * @Author: ANH DUNG Mar 11, 2016
     * @Todo: check user có thể view cột đại lý đặt không
     */
    public function canViewColumnAccounting() {
        $cRole = Yii::app()->user->role_id;
        if($cRole == ROLE_SUB_USER_AGENT){
            return false;
        }
        return true;
    }
    
    /**
     * @Author: ANH DUNG May 16, 2016
     * @Todo: get id vật tư cho đại lý
     */
    public function getIdMaterialAgent() {
        return "129,598,620,617,118,227,228,356,357,358,614,120,405,619,424,425,421,616,611,314,"
        . "363,615,140,613,612,141,61,51,386,441,64,335,293,292,45,591,388,42,46,58,50,337,199,128,"
        . "592,55,62,169,124,395,114,144,115,65,66,49,168,857,269,59,275,283,57,334,791,792,"
        . "402,288,243,52,793,43,589,276,139,56,48,325,333,290,365,569,622,63,208,364,597,384,"
        . "599,367,590,60,377,53,623,624,108,112,109,111,106,110,285,107,387,291";
    }
    public function getIdMaterialKho() {
        return "799,798,850,316,403,844,836,838,817,837,818,816,815,814,813,841,839,842,843,840,846,845,847,805,621,117,123,142,204,311,258,217,241,280,610,202,235,532,618,310,430,201,313,252,203,602,375,429,442,821,820,849,803,853,801,851,802,824,823,822,852,848,828,827,826,825,833,808,443,317,834,835,830,807,806,832,829,819,831,800,804,809,811,810,812,854,855,324,593,323,332,318,319,320,321,322,796,302,207,303,145,146,306,147,309,307,308,856,327,328,329,396,330,601,326,361,211,534,404,435,432,434,433,797,436,437,795";
    }
    // end get id vật tư cho đại lý
    
    const PROMOTION_ID_FIX_MISS = 834;// la dong hang tu ngay 13/10/2016
    /**
     * @Author: ANH DUNG Jul 06, 2016
     * @Todo: get report summary order missing
     */
    public function Miss() {
        /* 1. xác định ngày bắt đầu summary
         * 2. đưa vào findAll và grop tính toán
         */
        $order_promotion_id = GasOrderPromotion::PROMOTION_ID_FIX_MISS; // la dong hang tu ngay 17/06/2016
        $criteria = new CDbCriteria();
        $criteria->addCondition("t.order_promotion_id > $order_promotion_id");
        $criteria->compare("t.status", GasOrderPromotion::STATUS_COMPLETE_MISSING);
        $criteria->select = "sum(t.qty) as qty,  sum(t.qty_real) as qty_real, t.agent_id, t.materials_id, t.province_id";
        $criteria->group = "t.agent_id, t.materials_id";
        $criteria->order = "t.agent_id ASC, sum(t.qty_real) ASC";
        return GasOrderPromotionDetail::model()->findAll($criteria);
    }
    
    /**
     * @Author: ANH DUNG Jul 06, 2016
     * @Todo: get những dòng missing của đại lý khi print
     */
    public function getMissOfAgent() {
        /* 1. xác định ngày bắt đầu summary
         * 2. đưa vào findAll và grop tính toán
         * old 479
         */
//        $order_promotion_id = 479; // la dong hang tu ngay 17/06/2016
        $order_promotion_id = GasOrderPromotion::PROMOTION_ID_FIX_MISS; // la dong hang tu ngay 10/10/2016
        $criteria = new CDbCriteria();
        $criteria->addCondition("t.order_promotion_id > $order_promotion_id");
        $criteria->addCondition("t.order_promotion_id < $this->id");
        $criteria->compare("t.status", GasOrderPromotion::STATUS_COMPLETE_MISSING);
        $criteria->compare("t.agent_id", $this->agent_id);
        $criteria->select = "sum(t.qty) as qty,  sum(t.qty_real) as qty_real, t.agent_id, t.materials_id, t.province_id";
        $criteria->group = "t.materials_id";
        $criteria->order = "sum(t.qty_real) ASC";
        return GasOrderPromotionDetail::model()->findAll($criteria);
    }
    
    /**
     * @Author: ANH DUNG Sep 25, 2016
     * @Todo: check user can view qty
     */
    public function canViewCheckQty() {
        $cUid = MyFormat::getCurrentUid();
        $aUserAllow = array(2, GasLeave::DUNG_NTT, GasLeave::PHUONG_PTK);
        return in_array($cUid, $aUserAllow);
    }
    
    /**
     * @Author: ANH DUNG Sep 25, 2016
     * @Todo: report kiểm tra đối chiếu số lg đặt với slg thực nhận
     */
    public function CheckQty() {
        /* 1. find sl đặt của kế toán => format
         * 2. find sl thực nhận của dl => format
         */
        $aRes = array();
        $date_from  = MyFormat::dateDmyToYmdForAllIndexSearch($this->date_from);
        $date_to    = MyFormat::dateDmyToYmdForAllIndexSearch($this->date_to);
        // 1. find sl đặt của kế toán => format
        $criteria = new CDbCriteria();
        $criteria->compare("t.user_id_delivery", $this->user_id_delivery);
        $criteria->addBetweenCondition("t.created_date", $date_from, $date_to);
//        $criteria->compare("t.status", GasOrderPromotion::STATUS_CONFIRM);
        
        $criteria->select = "sum(t.qty) as qty, t.user_id_delivery, t.materials_id, t.materials_type_id";
        $criteria->group = "t.user_id_delivery, t.materials_id";
        $criteria->order = "t.materials_type_id ASC, t.materials_id ASC";
        $models = GasOrderPromotionAccounting::model()->findAll($criteria);
        $this->CheckQtyFormat($models, 'ACCOUNTING_QTY', $aRes);
        
        // 2. find sl thực nhận của dl => format
        $criteria->select = "sum(t.qty_real) as qty, t.user_id_delivery, t.materials_id, t.materials_type_id";
        $models = GasOrderPromotionDetail::model()->findAll($criteria);
        $this->CheckQtyFormat($models, 'AGENT_QTY', $aRes);
        return $aRes;
    }
    
    public function CheckQtyFormat($models, $nameParam, &$aRes) {
        foreach($models as $model){
            $aRes[$nameParam][$model->user_id_delivery][$model->materials_id]   = $model->qty;
            $aRes['ITEMS'][$model->materials_type_id][$model->materials_id]     = $model->materials_id;
        }
    }
}