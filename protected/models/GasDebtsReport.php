<?php

/**
 * This is the model for report debit
 *
 */
class GasDebtsReport extends GasDebts {
    
    public $code_account, $first_name, $province_id;
    public $date_begin_job, $leave_date, $position_work;
    
    const BLOCKED_DATE = '2019-07-01'; // DuongNV Jul1219 Ngày chặn, ko lấy dữ liệu từ ngày này làm bc vì sau ngày này chạy flow mới
    
    public function rules() {
        $aRules     = parent::rules();
        $aRules[]   = array('province_id, position_work','safe');
        return $aRules;
    }
    
    /** @Author: DuongNV Jul1119
     *  @Todo: isset() +=, else = 0
     **/
    public function addValue(&$var, $value) {
        if(empty($var)){
            $var = $value;
        } else {
            $var += $value;
        }
    }
    
    /** @Author: DuongNV Jul1119
     *  @Todo: bc cong no nhan vien
     *  @param date_from, date_to d-m-Y
     *  Nợ đầu kỳ: Số này là số nợ của nhân viên nhập vào đầu kỳ: Nợ đầu kỳ + Phát sinh nợ - phát sinh có> 0
        Có đầu kỳ: Số tiền cty phải trả nhân viên nhập đầu kỳ: Nợ đầu kỳ + Phát sinh nợ - phát sinh có <0
        Phát sinh nợ: Tạm ứng cá nhân, phạt văn bản, phiếu chi, phiếu xuất kho, mượn vật tư
        Phát sinh có:  phiếu Treo công nợ cá nhân bên Quản lý công nợ cá nhân
        Dư nợ cuối kỳ: = Nợ đầu kỳ + Phát sinh nợ - phát sinh có> 0
        Dư có cuối kỳ: = Nợ đầu kỳ + Phát sinh nợ - phát sinh có <0
     *  @note Phần này liên quan tới tính lương
     **/
    public function getReportDebts($salaryMode = false) {
        $aData              = [];
//        if(empty($this->date_from) || empty($this->date_to) || (empty($this->position_work) && empty($this->province_id) && empty($this->user_id))) return $aData;
        if(empty($this->date_from) || empty($this->date_to)) return $aData; // Dùng chung vs phần tính lương nên không limit tỉnh, bp
        $from               = MyFormat::dateConverDmyToYmd($this->date_from, '-');
        $to                 = MyFormat::dateConverDmyToYmd($this->date_to, '-');
        $blockedDate        = self::BLOCKED_DATE;
        $tblUser            = Users::model()->tableName();
        $tblUserProfile     = UsersProfile::model()->tableName();
        $criteria           = new CDbCriteria;
        $criteria->select   = 't.*, u.first_name, u.code_account, u.province_id, p.date_begin_job, p.leave_date, p.position_work';
        $criteria->join     = "JOIN $tblUser u ON t.user_id = u.id";
        $criteria->join    .= " JOIN $tblUserProfile p ON t.user_id = p.user_id";
        $criteria->addCondition("t.month >= '$blockedDate'");
        $criteria->addCondition("t.month <= '$to'");
        if( !empty($this->province_id) ){
            $criteria->addInCondition('u.province_id', $this->province_id);
        }
        if( !empty($this->position_work) ){
            $criteria->addInCondition('p.position_work', $this->position_work);
        }
        if( !empty($this->user_id) ){
            $criteria->compare('t.user_id', $this->user_id);
        }
        
        $mGasDebtsReport    = new GasDebtsReport();
        $models             = $mGasDebtsReport->findAll($criteria);
        
        $mDebts             = new GasDebts();
        $mDebts->date_from  = '2019-07-02';
        $aIncurredDebitT7   = $mDebts->getArrTypeDebit(); // t7, tính mượn vật tư
        $mDebts->date_from  = '2019-08-02';
        $aIncurredDebitT8   = $mDebts->getArrTypeDebit(); // t8 trở đi,ko tính mượn vật tư
        
//        $aIncurredDebit     = $this->getArrTypeDebit();
        $aIncurredDebit     = $aIncurredDebitT8;
        $aIncurredCredit    = $this->getArrTypeCredit();
        foreach ($models as $item) {
            // T7 tính loại mượn vật tư sang nợ, t8 trở đi thì ko
            if(MyFormat::compareTwoDate('2019-08-01', $item->month)){ // t7
                $aIncurredDebit = $aIncurredDebitT7;
            } else {
                if($item->type == GasDebts::TYPE_BORROW_TOOL) continue;
            }
            
            // khi tính lương sẽ ko tính công nợ vừa tạo (khi tạo bảng lg sẽ sinh ra 1 recorde công nợ loại trừ lương)
            if($salaryMode && !empty($this->salary_code_no) && $item->salary_code_no == $this->salary_code_no && $item->type == GasDebts::TYPE_CUT_SALARY){
                continue;
            }
            $aData['MODEL'][$item->user_id] = $item;
            if($item->month < $from): // Đầu kỳ (trc date_from)
                if( in_array($item->type, $aIncurredDebit) ): // nợ
                    $this->addValue($aData['OPENING'][$item->user_id], $item->amount); // đầu kỳ
                elseif(in_array($item->type, $aIncurredCredit)): // có
                    $this->addValue($aData['OPENING'][$item->user_id], -$item->amount); // đầu kỳ
                endif;
            else: // phat sinh no (date_from -> date_to)
                if( in_array($item->type, $aIncurredDebit) ): // nợ
                    $this->addValue($aData['INCURRED_DEBIT'][$item->user_id], $item->amount); // phát sinh nợ
                    $this->addValue($aData['CLOSING'][$item->user_id], $item->amount); // cuối kỳ
                elseif(in_array($item->type, $aIncurredCredit)): // có
                    $this->addValue($aData['INCURRED_CREDIT'][$item->user_id], $item->amount); // phát sinh có
                    $this->addValue($aData['CLOSING'][$item->user_id], -$item->amount); // cuối kỳ
                endif;
            endif;
        }
        
        // Cộng nợ đầu kỳ vào dư nợ / có cuối kỳ
        if( isset($aData['OPENING']) ):
            foreach ($aData['OPENING'] as $uid => $money) {
                if($money > 0){ // Nợ đầu kỳ
                    $this->addValue($aData['CLOSING'][$uid], $money);
                }
            }
        endif;
        
        if( !empty($aData['MODEL']) ):
            foreach ($aData['MODEL'] as $uid => $item) {
                $opening = empty($aData['OPENING'][$uid]) ? '' : $aData['OPENING'][$uid];
                $closing = empty($aData['CLOSING'][$uid]) ? '' : $aData['CLOSING'][$uid];
                $aData['OPENING_DEBIT'][$uid]   = $opening > 0 ? $opening : ''; // Nợ đầu kỳ
                $aData['OPENING_CREDIT'][$uid]  = $opening < 0 ? abs($opening) : ''; // Có đầu kỳ
                $closing                        += ($opening < 0) ? $opening : 0; // DuongNV Sep0719 Cuối kỳ cộng thêm có đầu kỳ
                $aData['CLOSING_DEBIT'][$uid]   = $closing > 0 ? $closing : ''; // Nợ cuối kỳ
                $aData['CLOSING_CREDIT'][$uid]  = $closing < 0 ? abs($closing) : ''; // Có cuối kỳ
                if( empty($aData['OPENING_DEBIT'][$uid]) 
                        && empty($aData['OPENING_CREDIT'][$uid])
                        && empty($aData['INCURRED_DEBIT'][$uid])
                        && empty($aData['INCURRED_CREDIT'][$uid])
                        && empty($aData['CLOSING_CREDIT'][$uid])
                        && empty($aData['CLOSING_DEBIT'][$uid]) ){ // Nếu không có công nợ thì remove
                    unset($aData['MODEL'][$uid]);
                }
            }
        endif;
        foreach ($aData as $key => $value) {
            $aData[$key] = array_filter($aData[$key]);
        }
        
        return $aData;
    }
    
    /** @Author: DuongNV Jul1119
     *  @Todo: bc cong no nhan vien (chi tiet cua tung nhan vien)
     *  Nợ đầu kỳ: Số này là số nợ của nhân viên nhập vào đầu kỳ: Nợ đầu kỳ + Phát sinh nợ - phát sinh có> 0
        Có đầu kỳ: Số tiền cty phải trả nhân viên nhập đầu kỳ: Nợ đầu kỳ + Phát sinh nợ - phát sinh có <0
        Phát sinh nợ: Tạm ứng cá nhân, phạt văn bản, phiếu chi, phiếu xuất kho, mượn vật tư
        Phát sinh có:  phiếu Treo công nợ cá nhân bên Quản lý công nợ cá nhân
        Dư nợ cuối kỳ: = Nợ đầu kỳ + Phát sinh nợ - phát sinh có> 0
        Dư có cuối kỳ: = Nợ đầu kỳ + Phát sinh nợ - phát sinh có <0
     *  @params: $this->date_from, $this->date_to format d-m-Y
     **/
    public function getReportDebtsDetail() {
        $aData              = [];
        if(empty($this->date_from) || empty($this->date_to) || empty($this->user_id)) return $aData;
        
        $from               = MyFormat::dateConverDmyToYmd($this->date_from, '-');
        $to                 = MyFormat::dateConverDmyToYmd($this->date_to, '-');
        $blockedDate        = self::BLOCKED_DATE;
        $criteria           = new CDbCriteria;
        $criteria->addCondition("t.month >= '$blockedDate'");
        $criteria->addCondition("t.month <= '$to'");
        $criteria->compare('t.user_id', $this->user_id);
        
        $mDebts             = new GasDebts();
        $mDebts->date_from  = '2019-07-02';
        $aTypeHide          = $mDebts->getArrTypeHideInDetail(); // Ẩn những loại này ở chi tiết (khác. support, đồng phục)
        $criteria->addNotInCondition('t.type', $aTypeHide);
        $cUid               = MyFormat::getCurrentUid();
        $cRole              = MyFormat::getCurrentRoleId();
        $aGS                = [ROLE_MONITORING, ROLE_MONITORING_MARKET_DEVELOPMENT];
        $aCCS               = [ROLE_EMPLOYEE_MARKET_DEVELOPMENT, ROLE_EMPLOYEE_MAINTAIN];
        // Nếu không phải những nhân viên dc xem toàn bộ như KTVP
        if( !$this->canViewReportDebts() ):
            if(in_array($cRole, $aGS)){ //  GS/KTKV: xem được của tất cả PVKH, PTTT, CCS
//                $mEmployee = Users::model()->findByPk($this->user_id);
//                if(empty($mEmployee->role_id) || !in_array($mEmployee->role_id, $aCCS)){
//                    return [];
//                }
                $tblUser   = Users::model()->tableName();
                $sParamsIn = implode(',', $aCCS);
                $criteria->mergeWith(array(
                    'join'      => "INNER JOIN $tblUser u ON t.user_id = u.id",
                    'condition' => "u.role_id IN ($sParamsIn) OR t.user_id = $cUid",
                ));
            } else { // Nhân viên bình thường chỉ dc xem công nợ của mình
                $criteria->compare('t.user_id', $cUid);
            }
        endif;
        $mGasDebtsReport    = new GasDebtsReport();
        $models             = $mGasDebtsReport->findAll($criteria);
        
        $mDebts->date_from  = '2019-07-02';
        $aIncurredDebitT7   = $mDebts->getArrTypeDebit(); // t7, tính mượn vật tư
        $mDebts->date_from  = '2019-08-02';
        $aIncurredDebitT8   = $mDebts->getArrTypeDebit(); // t8 trở đi,ko tính mượn vật tư
        
//        $aIncurredDebit     = $this->getArrTypeDebit();
        $aIncurredDebit     = $aIncurredDebitT8;
        $aIncurredCredit    = $this->getArrTypeCredit();
        foreach ($models as $item) {
            // T7 tính loại mượn vật tư sang nợ, t8 trở đi thì ko
            if(MyFormat::compareTwoDate('2019-08-01', $item->month)){ // t7 (month < 01-08-2019)
                $aIncurredDebit = $aIncurredDebitT7;
            } else {
                if($item->type == GasDebts::TYPE_BORROW_TOOL) continue;
            }
            
            if($item->month < $from): // Đầu kỳ (trc date_from)
                if( in_array($item->type, $aIncurredDebit) ): // Phát sinh nợ
//                    $cRole = MyFormat::getCurrentRoleId();
//                    if ($cRole == ROLE_ADMIN) echo "nợ: id $item->id, $item->amount<br>";
                    $this->addValue($aData['OPENING'], $item->amount);
                elseif(in_array($item->type, $aIncurredCredit)): // Phát sinh có
//                    $cRole = MyFormat::getCurrentRoleId();
//                    if ($cRole == ROLE_ADMIN) echo "có: id $item->id, $item->amount<br>";
                    $this->addValue($aData['OPENING'], -$item->amount);
                endif;
            else: // phat sinh no (date_from -> date_to)
                $aData['MODEL'][] = $item;
                if( in_array($item->type, $aIncurredDebit) ): // Phát sinh nợ
                    $this->addValue($aData['INCURRED_DEBIT'], $item->amount);
                    $this->addValue($aData['CLOSING'], $item->amount);
                elseif(in_array($item->type, $aIncurredCredit)): // Phát sinh có
                    $this->addValue($aData['INCURRED_CREDIT'], $item->amount);
                    $this->addValue($aData['CLOSING'], -$item->amount);
                endif;
            endif;
        }

        // Cộng nợ đầu kỳ vào dư nợ / có cuối kỳ
        if( isset($aData['OPENING']) && $aData['OPENING'] > 0 ): // Nợ đầu kỳ
            $this->addValue($aData['CLOSING'], $aData['OPENING']);
        endif;
        
        $opening                    = empty($aData['OPENING']) ? 0 : $aData['OPENING'];
        $closing                    = empty($aData['CLOSING']) ? 0 : $aData['CLOSING'];
        $aData['OPENING_DEBIT']     = $opening > 0 ? $opening : ''; // Nợ đầu kỳ
        $aData['OPENING_CREDIT']    = $opening < 0 ? abs($opening) : ''; // Có đầu kỳ
        $closing                    += ($opening < 0) ? $opening : 0; // DuongNV Sep0719 Cuối kỳ cộng thêm có đầu kỳ
        $aData['CLOSING_DEBIT']     = $closing > 0 ? $closing : ''; // Nợ cuối kỳ
        $aData['CLOSING_CREDIT']    = $closing < 0 ? abs($closing) : ''; // Có cuối kỳ
        
        if(empty($opening) && empty($closing) && empty($aData['INCURRED_DEBIT']) && empty($aData['INCURRED_CREDIT'])) {
            $aData['MODEL'] = [];
        }
        
        // sort by date
        uasort($aData['MODEL'], function($a, $b){
            return $a->month > $b->month;
        });
        
        return $aData;
    }
    
    /** @Author: DuongNV Jul1619
     *  @Todo: Những user/role đc xem tất cả nhân viên
     **/
    public function canViewReportDebts() {
        $aRoleAllow = [ROLE_ACCOUNTING]; // KTVP
        $cRole      = MyFormat::getCurrentRoleId();
        if($cRole == ROLE_ADMIN){
            return true;
        }
        return in_array($cRole, $aRoleAllow);
    }
    
    /** @Author: DuongNV Jul1819
     *  @Todo: can cut salary in report
     **/
    public function canCutSalary() {
        $cUid   = MyFormat::getCurrentUid();
        $cRole  = MyFormat::getCurrentRoleId();
        $aAllow = [
//            GasConst::UID_CHAU_LNM,
            GasConst::UID_HUE_LT, // Lê Thị Huệ
            GasConst::UID_NGUYEN_DTM, // Đặng Thị Mỹ Nguyên
            GasLeave::UID_VIEW_ALL_TAY_NGUYEN_GIA_LAI, // Nguyễn Thị Kim Tuyết
            GasConst::UID_NHI_DT // Đặng Thị Nhi
        ];
        if($cRole == ROLE_ADMIN){
            return true;
        }
        return in_array($cUid, $aAllow);
    }
    
    /** @Author: DuongNV Jul1819
     *  @Todo: Trừ lương (/admin/gasDebts/reportDebtsGeneral)
     *  @param $aData array post data from form
     **/
    public function handleCutSalary($aData) {
        $aUid       = isset($aData['uid']) ? $aData['uid'] : [];
        $aAmount    = isset($aData['amount']) ? $aData['amount'] : [];
        $month      = isset($aData['month']) ? $aData['month'] : date('d-m-Y');
        if(empty($aAmount)) return false;
        $count = 0;
        foreach ($aAmount as $key => $amount) {
            if( empty($aAmount[$key]) ) continue;
            
            $mGasDebts          = new GasDebts();
            $mGasDebts->user_id = empty($aUid[$key]) ? 0 : $aUid[$key];
            $mGasDebts->amount  = MyFormat::removeCommaAndPoint($amount);
            $mGasDebts->month   = MyFormat::dateConverDmyToYmd($month, '-');
            $mGasDebts->reason  = 'Trừ lương tháng '.date('m/Y', strtotime($month));
            $mGasDebts->type    = GasDebts::TYPE_CUT_SALARY;
            $mGasDebts->status  = GasDebts::STATUS_ACTIVE;
            $mGasDebts->save();
            $count++;
        }
        if($count > 0){
            Yii::app()->user->setFlash(MyFormat::SUCCESS_UPDATE, "Trừ lương thành công $count nhân viên.");
        }
    }
    
}
