<?php

/**
 * This is the model class for table "{{_gas_text_comment}}".
 *
 * The followings are the available columns in table '{{_gas_text_comment}}':
 * @property string $id
 * @property string $text_id
 * @property string $uid_login
 * @property string $message
 * @property string $created_date
 */
class GasTextComment extends BaseSpj
{
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
            return '{{_gas_text_comment}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        return array(
            array('id, text_id, uid_login, message, created_date', 'safe'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
            return array(
                'rUidLogin' => array(self::BELONGS_TO, 'Users', 'uid_login'),
                'rTextId' => array(self::BELONGS_TO, 'GasText', 'text_id'),
            );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'text_id' => 'Text',
            'uid_login' => 'Uid Login',
            'message' => 'Message',
            'created_date' => 'Created Date',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        $criteria=new CDbCriteria;
        $criteria->compare('t.text_id',$this->text_id);
        $criteria->compare('t.uid_login',$this->uid_login);
        $criteria->order = "t.id DESC";
        return new CActiveDataProvider($this, array(
                'criteria'=>$criteria,
                'pagination'=>array(
//                    'pageSize'=> Yii::app()->user->getState('pageSize',Yii::app()->params['defaultPageSize']),
                    'pageSize'=> 10,
                ),
        ));
    }

    public function defaultScope()
    {
        return array();
    }
    
    /**
     * @Author: ANH DUNG Aug 31, 2014
     * @Todo: display full name of user at bình luận của văn bản /admin/gasText/Text_view/id/9
     * @Param: $model model User
     * @Return: full name with salution of user
     */
    
    public static function ShowNamePost($mUser){
        if(is_null($mUser)) return '';
//        return $mUser->first_name;
        $session=Yii::app()->session;
        return $mUser->first_name." <br> [".$session['ROLE_NAME_USER'][$mUser->role_id]."]";
    }
    
    /**
     * @Author: ANH DUNG Aug 15, 2014
     * @Todo: save 1 bình luận của văn bản
     * @Param: $model model $mGasText
     */
    public static function SaveOneMessageDetail($mGasText){
//        if(!self::UserCanPostTicket()) return;        
        $mComment = new GasTextComment();
        $mComment->text_id = $mGasText->id;
        $mComment->uid_login = Yii::app()->user->id;
        $mComment->message = trim(InputHelper::removeScriptTag($mGasText->message));
        $mComment->save();
        $mGasText->last_comment = date('Y-m-d H:i:s');
        $mGasText->update(array('last_comment'));
    }    
    
    public static function deleteByTextId($text_id){
        $criteria = new CDbCriteria();
        $criteria->compare("t.text_id", $text_id);
        $models = self::model()->findAll($criteria);
        Users::deleteArrModel($models);        
    }
    
    /**
     * @Author: ANH DUNG Dec 26, 2014
     * @Todo: get last comment for this
     * @Param: $model is model GasText
     */
    public static function GetLastComment($model) {
        $res = '';                
        $criteria = new CDbCriteria();
        $criteria->compare("t.text_id", $model->id);
        $criteria->order = "t.id DESC";
        $criteria->limit = 1;
        $mComment = self::model()->find($criteria);
        if($mComment){
            $cmsFormater = new CmsFormatter();
            $res = $mComment->rUidLogin->first_name."<br>".$cmsFormater->formatDateTime($mComment->created_date);
        }
        
        $TotalComment = MyFormat::CountComment($model->id, 'text_id', 'GasTextComment');
        if($TotalComment){
            $res = "<b>($TotalComment)</b> - ".$res;
        }
        return $res;
    }
    
}