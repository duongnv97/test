<?php

/**
 * This is the model class for table "{{_customer_special}}".
 *
 * The followings are the available columns in table '{{_customer_special}}':
 * @property string $id
 * @property string $parent_id
 * @property string $customer_id
 * @property integer $type_customer
 * @property string $sale_id
 * @property integer $type
 * @property integer $status
 * @property string $uid_login
 * @property integer $uid_login_role_id
 * @property string $approved_level_1
 * @property string $approved_level_2
 * @property string $approved_level_3
 * @property string $approved_level_3_date
 * @property string $json_note
 * @property string $created_date
 */
class CustomerSpecial extends BaseSpj
{
    const TYPE_ADD_NEW_SPECIAL  = 1;
    const TYPE_CANCEL_SPECIAL   = 2;
    const TYPE_CANCEL_NOT_APPROVED = 3;
    
    const STATUS_NEW            = 1;
    const STATUS_APPROVED_1     = 2;
    const STATUS_APPROVED_2     = 3;
    const STATUS_APPROVED_3     = 4;
    const STATUS_LEAVE_SPECIAL  = 5;
    const STATUS_CANCEL         = 6;
    
    const REASON_GOI_GAP         = 1;
    const REASON_DUNG_2_BEN      = 2;
    const REASON_BAO_TRI_NHIEU   = 3;
    const REASON_CONG_NO_DAI     = 4;
    const REASON_SAN_LUONG_IT    = 5;
    const REASON_GIAO_GAS_KHO    = 6;
    const REASON_GIA_BAN_THAP    = 7;
    const REASON_BAT_HOP_TAC     = 8;
    const REASON_GOI_GAS_KHUYA   = 9;
    const REASON_NOT_ORDER_45_NGAY   = 10;
    
    public $MAX_ID;
    public $autocomplete_name;
    public $autocomplete_name_2;
    public $autocomplete_name_3;
    public $note_create, $note_level_1, $note_level_2, $note_level_3, $date_approved_1, $date_approved_2;
    public $JSON_FIELD = array('note_create', 'note_level_1', 'note_level_2', 'note_level_3', 'date_approved_1', 'date_approved_2');
    public $reason, $mCustomer;
    
    /**
     * @Author: ANH DUNG Sep 04, 2016
     */
    public function getArrayReason() {
        return array(
            CustomerSpecial::REASON_GOI_GAP         => "Thường xuyên gọi gấp, sử dụng gas không theo Line",
            CustomerSpecial::REASON_DUNG_2_BEN      => "Sử dụng gas 2 bên",
            CustomerSpecial::REASON_BAO_TRI_NHIEU   => "Bảo trì nhiều lần/tháng hoặc đòi hỏi nhiều dịch vụ",
            CustomerSpecial::REASON_CONG_NO_DAI     => "Công nợ dài hoặc thường xuyên thanh toán không đúng hạn",
            CustomerSpecial::REASON_SAN_LUONG_IT    => "Sản lượng ít, đầu tư lớn",
            CustomerSpecial::REASON_GIAO_GAS_KHO    => "Giao Gas khó như: Lên lầu, giao gas xa, giao gas đúng giờ",
            CustomerSpecial::REASON_GIA_BAN_THAP    => "Giá bán thấp ( < NPP )",
            CustomerSpecial::REASON_BAT_HOP_TAC     => "Thái độ bất hợp tác, khiếm nhã",
            CustomerSpecial::REASON_GOI_GAS_KHUYA   => "Gọi Gas khuya sau 00h và trước 5h sáng",
            CustomerSpecial::REASON_NOT_ORDER_45_NGAY   => "Tần suất lấy hàng trên 45 ngày",
        );
    }
    
    public function getArrayType() {
        return array(
            CustomerSpecial::TYPE_ADD_NEW_SPECIAL   => "Đề xuất KH đặc biệt",
            CustomerSpecial::TYPE_CANCEL_SPECIAL    => "Gỡ Bỏ đặc biệt",
        );
    }
    public function getArrayStatus() {
        return array(
            CustomerSpecial::STATUS_NEW             => "Mới",
            CustomerSpecial::STATUS_APPROVED_1      => "Duyệt lần 1",
            CustomerSpecial::STATUS_APPROVED_2      => "Duyệt lần 2",
            CustomerSpecial::STATUS_APPROVED_3      => "Duyệt lần 3",
            CustomerSpecial::STATUS_LEAVE_SPECIAL   => "Đã bỏ đặc biệt",
            CustomerSpecial::STATUS_CANCEL          => "Hủy bỏ",
        );
    }
    public static $LIST_STATUS_CAN_APPROVE_1 = array(
        CustomerSpecial::STATUS_NEW,
        CustomerSpecial::STATUS_APPROVED_1,
        CustomerSpecial::STATUS_CANCEL,
    );
    public static $LIST_STATUS_CAN_APPROVE_2 = array(
        CustomerSpecial::STATUS_APPROVED_1,
        CustomerSpecial::STATUS_APPROVED_2,
        CustomerSpecial::STATUS_CANCEL,
    );
    public static $LIST_STATUS_CAN_APPROVE_3 = array(
        CustomerSpecial::STATUS_APPROVED_2,
        CustomerSpecial::STATUS_APPROVED_3,
        CustomerSpecial::STATUS_CANCEL,
    );
    
    public static $STATUS_UPDATE_BY_APPROVE_1 = array(
        CustomerSpecial::STATUS_APPROVED_1      => "Duyệt lần 1",
        CustomerSpecial::STATUS_CANCEL          => "Hủy bỏ",
    );
    public static $STATUS_UPDATE_BY_APPROVE_2 = array(
        CustomerSpecial::STATUS_APPROVED_2      => "Duyệt lần 2",
        CustomerSpecial::STATUS_CANCEL          => "Hủy bỏ",
    );
    public static $STATUS_UPDATE_BY_APPROVE_3 = array(
        CustomerSpecial::STATUS_APPROVED_3      => "Duyệt lần 3",
        CustomerSpecial::STATUS_CANCEL          => "Hủy bỏ",
    );
    
    public function getTypeText() {
        $aType = $this->getArrayType();
        return isset($aType[$this->type]) ? $aType[$this->type] : "";
    }
    
    public function getStatusText() {
        $aStatus = $this->getArrayStatus();
        return isset($aStatus[$this->status]) ? $aStatus[$this->status] : "";
    }
    
    public function getRoleSale() {
        return array(ROLE_SALE, ROLE_MONITORING_MARKET_DEVELOPMENT);
    }
    
    
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{_customer_special}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        return array(
            array('customer_id, approved_level_1', 'required', 'on'=>'create, update'),
            array('status', 'required', 'on'=>'update_level_1, update_level_2, update_level_3'),
            array('approved_level_2', 'required', 'on'=>'update_level_1'),
            array('approved_level_3', 'required', 'on'=>'update_level_2'),
            array('id, parent_id, customer_id, type_customer, sale_id, type, status, uid_login, uid_login_role_id, approved_level_1, approved_level_2, approved_level_3, approved_level_3_date, json_note, created_date', 'safe'),
            array('code_no, date_approved_1, date_approved_2, reason, note_create, note_level_1, note_level_2, note_level_3', 'safe'),
        );
    }

    public function relations()
    {
        return array(
            'rCustomer' => array(self::BELONGS_TO, 'Users', 'customer_id'),
            'rSale' => array(self::BELONGS_TO, 'Users', 'sale_id'),
            'rUidLogin' => array(self::BELONGS_TO, 'Users', 'uid_login'),
            'rApprovedLevel1' => array(self::BELONGS_TO, 'Users', 'approved_level_1'),
            'rApprovedLevel2' => array(self::BELONGS_TO, 'Users', 'approved_level_2'),
            'rApprovedLevel3' => array(self::BELONGS_TO, 'Users', 'approved_level_3'),
            'rParent' => array(self::BELONGS_TO, 'CustomerSpecial', 'parent_id'),
            'rReason' => array(self::HAS_MANY, 'GasOneMany', 'one_id',
                'on'=>'rReason.type='.GasOneMany::TYPE_CUSTOMER_SPECIAL,
                    'order'=>'rReason.id',
            ),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'parent_id' => 'Parent',
            'customer_id' => 'Khách hàng',
            'type_customer' => 'Loại KH',
            'sale_id' => 'NV Kinh Doanh',
            'type' => 'Loại',
            'status' => 'Trạng thái',
            'uid_login' => 'Người tạo',
            'uid_login_role_id' => 'Uid Login Role',
            'approved_level_1' => 'Duyệt lần 1',
            'approved_level_2' => 'Duyệt lần 2',
            'approved_level_3' => 'Duyệt lần 3',
            'approved_level_3_date' => 'Approved Level 3 Date',
            'json_note' => 'Json Note',
            'created_date' => 'Ngày tạo',
            'note_create' => 'Ghi chú',
            'note_level_1' => 'Ghi chú',
            'note_level_2' => 'Ghi chú ',
            'note_level_3' => 'Ghi chú',
            'reason' => 'Lý do',
            'code_no' => 'Mã số',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        $cRole          = Yii::app()->user->role_id;
        $cUid           = Yii::app()->user->id;
        $session        = Yii::app()->session;
        $aUidKeToanBo   = array(GasLeave::THUC_NH, GasLeave::PHUONG_KT);
        $aRoleSale      = array(ROLE_SALE, ROLE_MONITORING_MARKET_DEVELOPMENT);
        $mAppCache      = new AppCache();
        $aIdDieuPhoiBoMoi = $mAppCache->getArrayIdRole(ROLE_DIEU_PHOI);
        $aUidViewAlll   = [$aIdDieuPhoiBoMoi, GasLeave::AUDIT_ISSUE, GasLeave::UID_HEAD_TECHNICAL, GasSupportCustomer::UID_QUY_PV];
        
        $agentLimitQuery = "";
        if ($cRole == ROLE_ACCOUNTING_ZONE ) {
            $agentLimitQuery = GasAgentCustomer::addInConditionAgentGetQuery('t.agent_id', "OR");
        }
        
        $criteria=new CDbCriteria;
        $this->handleCondition($criteria, $agentLimitQuery);
        $query = "t.uid_login = $cUid $agentLimitQuery ";
//        $query = "";
        if ($this->isApprovedLevel1()) {
            // Chỉ xem những item được gán cho mình
            $query .= " OR (t.approved_level_1 = " . $cUid . ")";
//            $criteria->addNotInCondition('t.status', $aStatusNotShowForApprove);// Aug 04, 2016 ẩn những cái reject hay request edit đi
        }
        if ($this->isApprovedLevel2()) {
            $query .= " OR (t.approved_level_2 = " . $cUid . ")";
        }
        if ($this->isApprovedLevel3()) {
            $query .= " OR (t.approved_level_3 = " . $cUid . ")";
            if(isset($_GET['StatusSpecial']) && $_GET['StatusSpecial'] == CustomerSpecial::TYPE_CANCEL_NOT_APPROVED){
                $statusCancel   = CustomerSpecial::STATUS_CANCEL;
                $typeAdd        = CustomerSpecial::TYPE_ADD_NEW_SPECIAL;
                $query .= " OR (t.status=$statusCancel AND t.type=$typeAdd)";
            }
        }
        if(in_array($cUid, $aUidKeToanBo)){
            $sParamsIn = implode(',', $aUidKeToanBo);
            $query .= " OR (t.uid_login IN ($sParamsIn)) OR type_customer=".STORE_CARD_KH_BINH_BO;
        }
        
        if (in_array($cRole, $aRoleSale)) {
            $query .= " OR (t.sale_id = " . $cUid . ")";
        }
        
        if ($cRole != ROLE_ADMIN && trim($query) != '' && !in_array($cUid, $aUidViewAlll)) {
            // Chỉ thêm query cho user thường. Admin sẽ được xem tất cả
            $criteria->addCondition($query);
        }
        
        $criteria->order = "t.id DESC";

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
            'pagination'=>array(
                'pageSize'=> Yii::app()->user->getState('pageSize',Yii::app()->params['defaultPageSize']),
            ),
        ));
    }
    
    /**
     * @Author: ANH DUNG Jul 27, 2016
     */
    public function handleCondition(&$criteria, $agentLimitQuery) {
        $criteria->compare('t.uid_login', $this->uid_login);
        $criteria->compare('t.type', $this->type);
        $criteria->compare('t.status', $this->status);
        $criteria->compare('t.code_no', trim($this->code_no), true);
        $criteria->compare('t.customer_id',$this->customer_id);
        $criteria->compare('t.type_customer',$this->type_customer);
        $criteria->compare('t.sale_id',$this->sale_id);
        $criteria->compare('t.approved_level_1',$this->approved_level_1);
        $criteria->compare('t.approved_level_2',$this->approved_level_2);
        $criteria->compare('t.approved_level_3',$this->approved_level_3);
        $aWith = array();
        if(is_array($this->reason) && count($this->reason)){
            $aWith[] = 'rReason';
//            $criteria->addInCondition('rReason.many_id', $this->reason);
            $sParamsIn = implode(',', $this->reason);
            if(!empty($sParamsIn)){
                $criteria->addCondition("rReason.many_id IN ($sParamsIn)");
            }
        }
        if(count($aWith)){
            $criteria->with = $aWith;
            $criteria->together = true;
        }
        $typeAdd        = CustomerSpecial::TYPE_ADD_NEW_SPECIAL;
        $typeCancel     = CustomerSpecial::TYPE_CANCEL_SPECIAL;
        
        if(isset($_GET['StatusSpecial']) && $_GET['StatusSpecial'] == 1){
            $statusSearch   = CustomerSpecial::STATUS_APPROVED_3;
            $criteria->addCondition("t.status=$statusSearch AND t.type=$typeAdd ");
            
        }elseif(isset($_GET['StatusSpecial']) && $_GET['StatusSpecial'] == CustomerSpecial::TYPE_CANCEL_SPECIAL){
            $statusSearch   = CustomerSpecial::STATUS_APPROVED_3;
            $criteria->addCondition("t.status=$statusSearch AND t.type=$typeCancel ");
        }elseif(isset($_GET['StatusSpecial']) && $_GET['StatusSpecial'] == CustomerSpecial::TYPE_CANCEL_NOT_APPROVED){
            $statusSearch   = CustomerSpecial::STATUS_CANCEL;
            $criteria->addCondition("t.status=$statusSearch AND t.type=$typeAdd ");
        }
        else{
            $aStatus = array(CustomerSpecial::STATUS_APPROVED_3, CustomerSpecial::STATUS_LEAVE_SPECIAL);
//            $criteria->addNotInCondition('t.status', $aStatus);
            $sParamsIn = implode(',', $aStatus);
            $criteria->addCondition("t.status NOT IN ($sParamsIn) ");
        }
        
//        $date_from = '';
//        $date_to = '';
//        if(!empty($this->date_from)){
//            $date_from = MyFormat::dateDmyToYmdForAllIndexSearch($this->date_from);
//        }
//        if(!empty($this->date_to)){
//            $date_to = MyFormat::dateDmyToYmdForAllIndexSearch($this->date_to);
//        }
//        if(!empty($date_from))
//            $criteria->addCondition("DATE(t.cashier_confirm_date) >= '$date_from'");
//        if(!empty($date_to))
//            $criteria->addCondition("DATE(t.cashier_confirm_date) <= '$date_to'");
        
        
    }
    
    
    /**
     * @Author: ANH DUNG Sep 03, 2016
     * @Todo: Update json from variable data
     */
    public function setJsonField()
    {
        $json = array();
        foreach ($this->JSON_FIELD as $field_name) {
            $json[$field_name] = $this->$field_name;
        }
        $this->json_note = json_encode($json);
    }

    /**
     * @Author: ANH DUNG Sep 03, 2016
     * Get data from json field
     * @param $field_name
     * @return string
     */
    public function getJsonField($field_name)
    {
        $temp = json_decode($this->json_note, true);
        if (is_array($temp) && isset($temp[$field_name])) {
            return $temp[$field_name];
        }
        return '';
    }
    /**
     * @Author: ANH DUNG Sep 03, 2016
     * Map json field to model field
     */
    public function mapJsonField() {
        foreach($this->JSON_FIELD as $field_name) {
            $this->$field_name = $this->getJsonField($field_name);
        }
    }
    
    public function loadRelationField() {
        $this->reason = CHtml::listData($this->rReason, 'many_id', 'many_id');
    }
    
    public function getDayAllowUpdate()
    {
        return 5;
        return Yii::app()->params['days_update_order_promotion'];
    }

    /**
     * @Author: ANH DUNG Sep 03, 2016
     * Allow someone see update button
     * @return bool
     */
    public function canUpdate()
    {
        $cRole = Yii::app()->user->role_id;
        $cUid = Yii::app()->user->id;
        if ($cUid != $this->uid_login || $this->status != CustomerSpecial::STATUS_NEW) {
            return false; // cho phép admin sửa hết, còn những user khác thì ai tạo thì dc sửa của người đó
        }
        // nếu là new thì cho update thoải mái, chỉ khi nào nó update complete thì mới check
        $dayAllow = date('Y-m-d');
        $dayAllow = MyFormat::modifyDays($dayAllow, $this->getDayAllowUpdate(), '-');
        return MyFormat::compareTwoDate($this->created_date, $dayAllow);
    }
    
    /**
     * @Author: ANH DUNG Sep 04, 2016
     * @Todo: get some field relate with customer of uid login
     */
    public function getInfoRelate() {
        $mCustomer = $this->rCustomer;
        if($mCustomer){
            $this->type_customer    = $mCustomer->is_maintain;
            $this->sale_id          = $mCustomer->sale_id;
        }
        $mUidLogin = $this->rUidLogin;
        if($mUidLogin){
            $this->uid_login_role_id    = $mUidLogin->role_id;
            if($mUidLogin->role_id == ROLE_SUB_USER_AGENT){
                $this->agent_id     = $mUidLogin->parent_id;
            }
        }
    }
    
    protected function beforeSave() {
        if($this->isNewRecord){
            $this->code_no = MyFunctionCustom::getNextId('CustomerSpecial', 'S'.date('y'), LENGTH_TICKET,'code_no');
        }
        return parent::beforeSave();
    }
    
    /**
     * @Author: ANH DUNG Jun 25, 2016
     */
    public function getCustomer($field_name='') {
        if($this->mCustomer){
            $mUser = $this->mCustomer;
        }else{
            $mUser = $this->rCustomer;
        }
        if($mUser){
            if(is_null($this->mCustomer)){
                $this->mCustomer = $mUser;
            }
            if($field_name != ''){
                return $mUser->$field_name;
            }
//            return "<b>".$mUser->code_bussiness."-".$mUser->first_name."</b><br>".$mUser->address."<br><b>Phone: </b>".$mUser->phone;
            return "<b>".$mUser->code_bussiness."-".$mUser->first_name."</b><br>".$mUser->address."";
        }
        return '';
    }
    public function getTypeCustomer() {
        return isset(CmsFormatter::$CUSTOMER_BO_MOI[$this->type_customer]) ? CmsFormatter::$CUSTOMER_BO_MOI[$this->type_customer] : "";
    }
        
    public function getSale() {
        $mUser = $this->rSale;
        if($mUser){
            return $mUser->getFullName();
        }
        return "";
    }
    public function getUidLogin() {
        $mUser = $this->rUidLogin;
        if($mUser){
            return $mUser->getFullName();
        }
        return "";
    }
    public function getReason() {
        $res = "";
        $tmp = array();
        $aReason = $this->getArrayReason();
        foreach($this->rReason as $mOneMany){
            $tmp[] = $aReason[$mOneMany->many_id];
        }
        $res = " - ".implode("<br> - ", $tmp);
        return $res."<br><b>Ghi chú: </b>". $this->getNoteCreate();
    }
    public function getCreatedDate($fomat = 'd/m/Y H:i') {
        return MyFormat::dateConverYmdToDmy($this->created_date, "d/m/Y H:i");
    }
    public function getNoteCreate() {
        return nl2br($this->getJsonField('note_create'));
    }
    public function getNoteLevel1() {
        return nl2br($this->getJsonField('note_level_1'));
    }
    public function getNoteLevel2() {
        return nl2br($this->getJsonField('note_level_2'));
    }
    public function getNoteLevel3() {
        return nl2br($this->getJsonField('note_level_3'));
    }
    public function getApprovedDate1() {
        return nl2br($this->getJsonField('date_approved_1'));
    }
    public function getApprovedDate2() {
        return nl2br($this->getJsonField('date_approved_2'));
    }
    public function getApprovedDate3() {
        if(empty($this->approved_level_3_date)){
            return '';
        }
        return MyFormat::dateConverYmdToDmy($this->approved_level_3_date, "d/m/Y");
    }
    /**
     * @Author: ANH DUNG Sep 04, 2016
     * Lấy tên level 3
     * @return string
     */
    public function getApprovedLevel1()
    {
        if (isset(Yii::app()->session)) {
            $session = Yii::app()->session;
            if (isset($session['SES_SPECIAL_USER_FULLNAME'][$this->approved_level_1])) {
                return $session['SES_SPECIAL_USER_FULLNAME'][$this->approved_level_1]. " - ".$this->getApprovedDate1();
            }
        }
        $mUser = $this->rApprovedLevel1;
        if ($mUser) {
            return $mUser->getFullName(). " - ".$this->getApprovedDate1();
        }
        return '';
    }
    public function getApprovedLevel2()
    {
        if (isset(Yii::app()->session)) {
            $session = Yii::app()->session;
            if (isset($session['SES_SPECIAL_USER_FULLNAME'][$this->approved_level_2])) {
                return $session['SES_SPECIAL_USER_FULLNAME'][$this->approved_level_2]. " - ".$this->getApprovedDate2();
            }
        }
        $mUser = $this->rApprovedLevel2;
        if ($mUser) {
            return $mUser->getFullName(). " - ".$this->getApprovedDate2();
        }
        return '';
    }
    public function getApprovedLevel3()
    {
        if (isset(Yii::app()->session)) {
            $session = Yii::app()->session;
            if (isset($session['SES_SPECIAL_USER_FULLNAME'][$this->approved_level_3])) {
                return $session['SES_SPECIAL_USER_FULLNAME'][$this->approved_level_3]. " - ".$this->getApprovedDate3();
            }
        }
        $mUser = $this->rApprovedLevel3;
        if ($mUser) {
            return $mUser->getFullName(). " - ".$this->getApprovedDate3();
        }
        return '';
    }
    
    /**
     * @Author: ANH DUNG Sep 04, 2016
     * @Todo: get last approved
     */
    public function getNewestApproved() {
        $res = "";
        if (!empty($this->approved_level_3)) {
            $res .= "<p><span class='item_b'>Duyệt lần 3 </span>:{$this->getApprovedLevel3()}</p>";
            if (trim($this->getNoteLevel3()) != "") {
                $res .= "<p><i><span class='item_b'>Ghi chú</span>: " . $this->getNoteLevel3() . "</i></p>";
            }
        }
        if (!empty($this->approved_level_2)) {
            $res .= "<p><span class='item_b'>Duyệt lần 2 </span>:{$this->getApprovedLevel2()}</p>";
            if (trim($this->getNoteLevel2()) != "") {
                $res .= "<p><i><span class='item_b'>Ghi chú</span>: " . $this->getNoteLevel2() . "</i></p>";
            }
        }
        if (!empty($this->approved_level_1)) {
            $res .= "<p><span class='item_b'>Duyệt lần 1 </span>:{$this->getApprovedLevel1()}</p>";
            if (trim($this->getNoteLevel1()) != "") {
                $res .= "<p><i><span class='item_b'>Ghi chú</span>: " . $this->getNoteLevel1() . "</i></p>";
            }
        }
        
        return $res;
    }
    
    /**
     * @Author: ANH DUNG Sep 04, 2016
     * @Todo: handle update approved status
     */
    public function handleApproved() {
        if(isset($_POST['CustomerSpecial']['approved_level_2'])){
            $this->date_approved_1 = date('d/m/Y');
            $this->setJsonField();
            $this->approvedLevel1();
        }elseif(isset($_POST['CustomerSpecial']['approved_level_3'])){
            $this->date_approved_2 = date('d/m/Y');
            $this->setJsonField();
            $this->approvedLevel2();
        }else{
            $this->approvedLevel3();
        }
    }
    
    /**
     * @Author: ANH DUNG Oct 07, 2016
     */
    public function sendMailNotifyApprove($aUid) {
        $needMore = array('aUid' => $aUid);
        GasScheduleEmail::BuildNotifyCustomerSpecial($this, $needMore);
    }
    
    /**
     * @Author: ANH DUNG Sep 04, 2016
     * @Todo: handle approved level 1
     */
    public function approvedLevel1() {
        $aUpdate = array('status', 'approved_level_2', 'json_note');
        $this->update($aUpdate);
        $this->sendMailNotifyApprove(array($this->approved_level_2));
    }
    /**
     * @Author: ANH DUNG Sep 04, 2016
     * @Todo: handle approved level 2
     */
    public function approvedLevel2() {
        $aUpdate = array('status', 'approved_level_3', 'json_note');
        $this->update($aUpdate);
        $this->sendMailNotifyApprove(array($this->approved_level_3));
    }
    /**
     * @Author: ANH DUNG Sep 04, 2016
     * @Todo: handle approved level 3
     */
    public function approvedLevel3() {
        $aUpdate = array('status', 'json_note');
        if($this->status == CustomerSpecial::STATUS_APPROVED_3){
            $this->approved_level_3_date = date("Y-m-d");
            $aUpdate[] = 'approved_level_3_date';
            $this->setStatusParent();
            $this->setCustomerToNormal();
        }
        $this->update($aUpdate);
        $this->sendMailNotifyApprove(array($this->sale_id));
    }
    
    /**
     * @Author: ANH DUNG Sep 09, 2016
     * @Todo: set status back to parent, this customer is remove from special
     */
    public function setStatusParent() {
        $mParent = $this->rParent;
        if($mParent){
            $mParent->status = CustomerSpecial::STATUS_LEAVE_SPECIAL;
            $mParent->update(array('status'));
        }
    }
    
    /**
     * @Author: ANH DUNG Sep 04, 2016
     * @Todo: khởi tạo session user approve để check khi view
     */
    public function initSessionUserApproved()
    {
        $session = Yii::app()->session;
        if(!isset($session['SES_SPECIAL_USER_FULLNAME'])){
            $session['SES_SPECIAL_LEVEL_1'] = GasOneManyBig::getArrOfManyId(GasOneManyBig::TYPE_CUSTOMER_SPECIAL_1, GasOneManyBig::TYPE_CUSTOMER_SPECIAL_1);
            $session['SES_SPECIAL_LEVEL_2'] = GasOneManyBig::getArrOfManyId(GasOneManyBig::TYPE_CUSTOMER_SPECIAL_2, GasOneManyBig::TYPE_CUSTOMER_SPECIAL_2);
            $session['SES_SPECIAL_LEVEL_3'] = GasOneManyBig::getArrOfManyId(GasOneManyBig::TYPE_CUSTOMER_SPECIAL_3, GasOneManyBig::TYPE_CUSTOMER_SPECIAL_3);
            $aUid = array_merge($session['SES_SPECIAL_LEVEL_1'], $session['SES_SPECIAL_LEVEL_2'], $session['SES_SPECIAL_LEVEL_3']);
            $session['SES_SPECIAL_USER_FULLNAME'] = Users::getListOptions($aUid, array('get_all'=>1));
        }
        $mSupportCustomer = new GasSupportCustomer();
        $mSupportCustomer->InitMaterial();//  chỗ này sử dụng cho view thông tin đầu tư các vật tư cho KH
    }
    
    /**
     * @Author: ANH DUNG Sep 04, 2016
     * Is level 1
     */
    public function isApprovedLevel1()
    {
        $cUid       = Yii::app()->user->id;// Anh Dung Fix Jul 26, 2016
        $session    = Yii::app()->session;
        if(isset($session['SES_SPECIAL_LEVEL_1'][$cUid])){
            return true;
        }
        return false;
    }
    public function isApprovedLevel2()
    {
        $cUid       = Yii::app()->user->id;// Anh Dung Fix Jul 26, 2016
        $session    = Yii::app()->session;
        if(isset($session['SES_SPECIAL_LEVEL_2'][$cUid])){
            return true;
        }
        return false;
    }
    public function isApprovedLevel3()
    {
        $cUid       = Yii::app()->user->id;// Anh Dung Fix Jul 26, 2016
        $session    = Yii::app()->session;
        if(isset($session['SES_SPECIAL_LEVEL_3'][$cUid])){
            return true;
        }
        return false;
    }
    
    /**
     * @Author: ANH DUNG Sep 09, 2016
     * @Todo: get html url remove customer special
     */
    public function getUrlRemoveSpecial() {
        $res = "";
        if($this->type == CustomerSpecial::TYPE_ADD_NEW_SPECIAL && $this->status == CustomerSpecial::STATUS_APPROVED_3){
            $link = Yii::app()->createAbsoluteUrl("admin/customerSpecial/create", array( 'RemoveSpecial'=> $this->id));
            if( empty($this->parent_id)){
                $res .= "<a href='$link'>Đề xuất Bỏ KH Đặc Biệt</a>";
            }else{
                $res .= "Đã Đề xuất Bỏ KH Đặc Biệt";
            }
        }elseif($this->type == CustomerSpecial::TYPE_CANCEL_SPECIAL && $this->status == CustomerSpecial::STATUS_APPROVED_3){
            $link = Yii::app()->createAbsoluteUrl("admin/customerSpecial/view", array( 'id'=> $this->parent_id));
            $res .= "<a class='view' href='$link'>Xem Đề xuất</a>";
        }
        return $res;
    }
    /**
     * @Author: ANH DUNG Sep 09, 2016
     * @Todo: check url valid to remove customer special
     */
    public function canRemoveSpecial() {
        $ok = false;
        if($this->status == CustomerSpecial::STATUS_APPROVED_3){
            $ok = true;
        }
        return $ok;
    }
    
    /**
     * @Author: ANH DUNG Sep 10, 2016
     * @Todo: delete parent before delete
     */
    public function deleteParent() {
        if($this->rParent){
            $this->rParent->delete();
        }
    }
    
    /**
     * @Author: ANH DUNG Sep 25, 2016
     * @Todo: check can create đề xuất hoặc bỏ KH đặc biệt không
     */
    public function canCreateMore() {
        if(empty($this->customer_id)){
            return ;
        }
        $aStatusCheck = array(CustomerSpecial::STATUS_LEAVE_SPECIAL, CustomerSpecial::STATUS_CANCEL);
        $criteria = new CDbCriteria();
        $criteria->addCondition("t.customer_id=$this->customer_id AND t.type=$this->type");
        $sParamsIn = implode(',', $aStatusCheck);
        $criteria->addCondition("t.status NOT IN ($sParamsIn)");
        $model = self::model()->find($criteria);
        if($model){
            $linkDetail = Yii::app()->createAbsoluteUrl("admin/customerSpecial/view", array('id'=>$model->id));
            $url = "<a href='$linkDetail' target='_blank'>Xem $model->code_no</a>";
            $this->addError('customer_id', "Không thể tạo đề xuất / gỡ bỏ đặc biệt cho khách hàng này, vì KH này đang có đề xuất chờ duyệt. $url");
        }
    }
    
    /**
     * @Author: ANH DUNG Sep 25, 2016
     * @Todo: cron set KH đặc biệt quá 30 thì chặn hàng
     */
    public static function cronSetCustomerExpiry() {
        return ; // Close on Oct 21, 2016 không set tự động mà do anh Long set ở giao diện
        $from = time();
        $type = CustomerSpecial::TYPE_ADD_NEW_SPECIAL;
        $aStatusCheck = array(CustomerSpecial::STATUS_APPROVED_3 );
        $criteria = new CDbCriteria();
//        $criteria->addCondition("t.customer_id=$customer_id AND t.type=$type AND t.approved_level_3_date IS NOT NULL");
        $criteria->addCondition("t.type=$type AND t.approved_level_3_date IS NOT NULL");
        $sParamsIn = implode(',', $aStatusCheck);
        $criteria->addCondition("t.status IN ($sParamsIn)");
        $daysExpiry = 30;
        $criteria->addCondition("DATE_ADD(approved_level_3_date, INTERVAL $daysExpiry DAY) < CURDATE()");
        $models = self::model()->findAll($criteria);
        $aCustomerId = array();
        foreach($models as $model){
            $aCustomerId[] = $model->customer_id;
        }
        if(count($aCustomerId)){
            $criteria = new CDbCriteria();
            $sParamsIn = implode(',', $aCustomerId);
            $criteria->addCondition("id IN ($sParamsIn)");
            $aUpdate = array('channel_id' => Users::CHAN_HANG);
            Users::model()->updateAll($aUpdate, $criteria);
        }
        $to = time();
        $second = $to-$from;
        $info = "Cron set chặn hàng KH đặc biệt: ".count($aCustomerId)." done in: $second  Second  <=> ".($second/60)." Minutes.";
        Logger::WriteLog($info);
    }
    
     /**
     * @Author: ANH DUNG Sep 25, 2016
     * @Todo: khi KH gỡ bỏ đặc biệt thì set lại normal ko chặn hàng nữa
     */
    public function setCustomerToNormal() {
        if(empty($this->customer_id) || 
                $this->type != CustomerSpecial::TYPE_CANCEL_SPECIAL || $this->status != CustomerSpecial::STATUS_APPROVED_3){
            return ;
        }
        $criteria = new CDbCriteria();
        $criteria->addCondition("id IN ($this->customer_id)");
        $aUpdate = array('channel_id' => Users::KHONG_LAY_HANG);
        Users::model()->updateAll($aUpdate, $criteria);
        $info = "Cron set gỡ bỏ KH đặc biệt table User CustomerSpecial id => $this->id";
        Logger::WriteLog($info);
    }
    
    
    /**
     * @Author: ANH DUNG Oct 11, 2016
     */
    public static function updateAllFixAgent() {
//        getInfoRelate
        $models = self::model()->findAll();
        foreach($models as $model){
            $model->getInfoRelate();
            $model->update(array('agent_id'));
        }
        die;
    }
    
    /**
     * @Author: ANH DUNG Oct 21, 2016
     * @Todo: check user can lock or unlock chặn hàng
     */
    public function canLockCustomer() {
        $cRole = MyFormat::getCurrentRoleId();
        $aRoleAllow = array(ROLE_ADMIN, ROLE_DIRECTOR);
        if(in_array($cRole, $aRoleAllow) && 
            $this->type == CustomerSpecial::TYPE_ADD_NEW_SPECIAL && $this->status == CustomerSpecial::STATUS_APPROVED_3
        ){
            return true;
        }
        return false;
    }
    
    /**
     * @Author: ANH DUNG Oct 21, 2016
     * @Todo: set lock or unlock chặn hàng
     */
    public function setLockUnlockCustomer($statusUpdate, &$actionText) {
        $actionText = "Chặn hàng";
        $mCustomer = $this->rCustomer;
        if($mCustomer){
            if($mCustomer->channel_id == Users::CHAN_HANG){
                $actionText = "Bỏ chặn hàng";
            }
            $criteria = new CDbCriteria();
            $criteria->addCondition("id IN ($mCustomer->id)");
            $aUpdate = array('channel_id' => $statusUpdate);
            Users::model()->updateAll($aUpdate, $criteria);
        }
    }

    public function getHtmlLockCustomer() {
        $res = ""; $actionText = "Chặn hàng";
        $url = Yii::app()->createAbsoluteUrl("admin/customerSpecial/view", array('id' => $this->id, 'SetLockCustomer'=>1, "channel_id"=>Users::CHAN_HANG));
        $mCustomer = $this->rCustomer;
        if($mCustomer){
            if($mCustomer->channel_id == Users::CHAN_HANG){
                $actionText = "Bỏ chặn hàng";
                $url = Yii::app()->createAbsoluteUrl("admin/customerSpecial/view", array('id' => $this->id, 'SetLockCustomer'=>1, "channel_id"=>Users::KHONG_LAY_HANG));
            }
            $res .= "<div class='form item_c'><br><br>";
                $res .= "<a href='$url' class='btn_cancel f_size_14 btn_closed_tickets' alert_text='Bạn chắc chắn xác nhận $actionText?'>Xác Nhận $actionText</a>";
            $res .= "</div>";
        }
        return $res;
    }
    
    /**
     * @Author: ANH DUNG Oct 21, 2016
     * @Todo: get text lock or unlock chặn hàng
     */
    public function getLockCustomerText() {
        $actionText = "";
        $mCustomer = $this->rCustomer;
        if($mCustomer){
            if($mCustomer->channel_id == Users::CHAN_HANG){
                $actionText = "<br><br><strong>Đã Chặn hàng</strong>";
            }
        }
        return $actionText;
    }
    
    
}