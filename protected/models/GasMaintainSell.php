<?php

/**
 * This is the model class for table "{{_gas_maintain_sell}}".
 *
 * The followings are the available columns in table '{{_gas_maintain_sell}}':
 * @property string $id
 * @property integer $customer_id
 * @property string $date_sell
 * @property integer $quantity_sell
 * @property integer $materials_id_sell
 * @property integer $quantity_vo_back
 * @property integer $materials_id_back
 * @property string $gifts
 * @property string $note
 * @property integer $agent_id
 * @property integer $user_id_create
 * @property string $created_date
 */
class GasMaintainSell extends BaseSpj
{   
    public $autocomplete_name;
    public $customer_name;
    public $customer_address;
    public $customer_phone;	
    public $one_many_gift;
    public $count_row;
    public $date_from;
    public $date_to;
    public $count_buy;// biến đếm số lần mua hàng của KH
    public $qty_buy_from;// biến thể hiện số lượng lần mua hàng lớn hơn
     
    public $autocomplete_material_back;
    public $autocomplete_material_sell;
    public $MAX_ID;

    public static function getDayAllowUpdate(){
        return Yii::app()->params['days_update_maintain_sell'];
    }     
     
    public static function model($className=__CLASS__)
    {
            return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
            return '{{_gas_maintain_sell}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        return array(
            array('customer_id, date_sell, quantity_sell, materials_id_sell', 'required'),
            array('customer_id, quantity_sell, materials_id_sell, quantity_vo_back, materials_id_back, agent_id, user_id_create', 'numerical', 'integerOnly'=>true),
            array('gifts, note', 'length', 'max'=>255),
            array('id, customer_id, date_sell, quantity_sell, materials_id_sell, quantity_vo_back, materials_id_back, gifts, note, agent_id, user_id_create, created_date', 'safe'),
            array('note_update_status,status,update_num,maintain_id', 'safe'),
            array('customer_name, customer_address, customer_phone', 'safe'),
            array('seri_back,is_same_seri_maintain', 'safe'),
            array('one_many_gift,type', 'safe'),
            array('seri_back', 'length', 'max'=>30),
            array('status_seri_diff,note_seri_diff,status_call_bad,note_call_bad', 'safe'),
            array('province_id,district_id,ward_id,maintain_employee_id,monitoring_id', 'safe'),
            array('code_no,using_gas_huongminh,using_promotion,date_from,date_to,count_buy,qty_buy_from', 'safe'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
            return array(
                'customer' => array(self::BELONGS_TO, 'Users', 'customer_id'),
                'agent' => array(self::BELONGS_TO, 'Users', 'agent_id'),
                'materials_sell' => array(self::BELONGS_TO, 'GasMaterials', 'materials_id_sell'),
                'materials_back' => array(self::BELONGS_TO, 'GasMaterials', 'materials_id_back'),
                'maintain' => array(self::BELONGS_TO, 'GasMaintain', 'maintain_id'),
                'province' => array(self::BELONGS_TO, 'GasProvince', 'province_id'),
                'district' => array(self::BELONGS_TO, 'GasDistrict', 'district_id'),
                'ward' => array(self::BELONGS_TO, 'GasWard', 'ward_id'),                    

                'maintain_employee' => array(self::BELONGS_TO, 'Users', 'maintain_employee_id'),
                'accounting_employee' => array(self::BELONGS_TO, 'Users', 'accounting_employee_id'),
                'monitoring' => array(self::BELONGS_TO, 'Users', 'monitoring_id'),
            );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
            return array(
                    'id' => 'ID',
                    'customer_id' => 'Khách Hàng',
                    'date_sell' => 'Ngày Bán',
                    'quantity_sell' => 'SL Bán',
                    'materials_id_sell' => 'Thương Hiệu Gas Bán',
                    'quantity_vo_back' => 'SL Vỏ Thu Về',
                    'materials_id_back' => 'Thương Hiệu Gas Vỏ',
                    'gifts' => 'Quà Tặng',
                    'one_many_gift' => 'Quà Tặng',
                    'note' => 'Ghi Chú',
                    'agent_id' => 'Đại Lý',
                    'user_id_create' => 'Người Tạo',
                    'created_date' => 'Ngày Tạo',
                    'note_update_status' => 'Ghi Chú Sau Cuộc Gọi',
                    'update_num' => 'Số Lần Đã Cập Nhật',
                    'status' => 'Trạng Thái',        
                    'customer_name' => 'Họ Tên Khách Hàng',
                    'customer_address' => 'Địa Chỉ',
                    'customer_phone' => 'Điện Thoại',
                    'seri_back' => 'Seri Thu Về',
                    'is_same_seri_maintain' => 'Trùng Seri Bảo Trì',

                    'status_seri_diff' => 'Kết Quả Xử Lý',
                    'note_seri_diff' => 'Ghi Chú Sau Xử Lý',
                    'status_call_bad' => 'Kết Quả Xử Lý',
                    'note_call_bad' => 'Ghi Chú Sau Xử Lý',   
                    'type' => 'Loại',		
                    'province_id' => 'Tỉnh',
                    'district_id' => 'Quận Huyện',
                    'ward_id' => 'Phường/Xã',
                    'using_gas_huongminh' => 'Đang Dùng Gas Hướng Minh',
                    'using_promotion' => 'Có Chiết Khấu',
                    'date_from' => 'Từ Ngày',
                    'date_to' => 'Đến Ngày',    
                    'monitoring_id' => 'Giám Sát PTTT',
                    'maintain_employee_id' => 'Nhân Viên PTTT',
                    'accounting_employee_id' => 'Nhân Viên Kế Toán',
                    'qty_buy_from' => 'Số Lần Mua Hàng Từ',
                    'code_no' => 'Mã Số',


            );
    }

    public function search()
    {
        $criteria=new CDbCriteria;

        $criteria->compare('t.id',$this->id,true);
        $criteria->compare('t.code_no', trim($this->code_no),true);
        $criteria->compare('t.customer_id',$this->customer_id);
        if(!empty($this->date_sell)){
            $this->date_sell = MyFormat::dateDmyToYmdForAllIndexSearch($this->date_sell);
            $criteria->compare('t.date_sell',$this->date_sell,true);
        }		
        $criteria->compare('t.status',$this->status);
        $criteria->compare('t.quantity_sell',$this->quantity_sell);
        $criteria->compare('t.materials_id_sell',$this->materials_id_sell);
        $criteria->compare('t.quantity_vo_back',$this->quantity_vo_back);
        $criteria->compare('t.materials_id_back',$this->materials_id_back);
        $criteria->compare('t.note',$this->note,true);
        $criteria->compare('t.seri_back',$this->seri_back,true);
        $criteria->compare('t.is_same_seri_maintain',$this->is_same_seri_maintain);
        $criteria->compare('t.type',$this->type);
        $criteria->compare('t.monitoring_id',$this->monitoring_id);
        $criteria->compare('t.maintain_employee_id',$this->maintain_employee_id);
        if($this->using_gas_huongminh!=''){
            $criteria->compare('t.using_gas_huongminh', $this->using_gas_huongminh);
        }
        if(Yii::app()->user->role_id==ROLE_SUB_USER_AGENT){
            $criteria->compare('t.agent_id',Yii::app()->user->parent_id);
        }else{
            if(!empty($this->agent_id)){
                $criteria->compare('t.agent_id', $this->agent_id);
            }
            GasAgentCustomer::addInConditionAgent($criteria, 't.agent_id');
        }

        if(!empty($this->customer_address)){
                $criteria->with = array('customer');
                $criteria->together = true;
                $criteria->compare('customer.address',$this->customer_address,true);
        }					

        $criteria->compare('t.user_id_create',$this->user_id_create);
        if(!empty($this->created_date)){
            $this->created_date = MyFormat::dateDmyToYmdForAllIndexSearch($this->created_date);
            $criteria->compare('t.created_date',$this->created_date,true);
        }    

        if(!empty($this->date_from))
                $this->date_from = MyFormat::dateDmyToYmdForAllIndexSearch($this->date_from);
        if(!empty($this->date_to))
            $this->date_to = MyFormat::dateDmyToYmdForAllIndexSearch($this->date_to);				
        if(!empty($this->date_from) && empty($this->date_to))
                $criteria->addCondition("t.date_sell>='$this->date_from'");
        if(empty($this->date_from) && !empty($this->date_to))
                $criteria->addCondition("t.date_sell<='$this->date_to'");
        if(!empty($this->date_from) && !empty($this->date_to))
                $criteria->addBetweenCondition("t.date_sell",$this->date_from,$this->date_to); 				


          $session=Yii::app()->session;
          if(isset($session['AGENT_OF_USER_MAINTAIN'])){
                $sParamsIn = implode(',', $session['AGENT_OF_USER_MAINTAIN']);
                if(!empty($sParamsIn)){
                    $criteria->addCondition("t.agent_id IN ($sParamsIn)");
                }
          }   

          $sort = new CSort();
          $sort->attributes = array(
              'date_sell'=>'date_sell',
              'seri_back'=>'seri_back',
              'quantity_sell'=>'quantity_sell',
              'quantity_vo_back'=>'quantity_vo_back',
              'status'=>'status',
              'monitoring_id'=>'monitoring_id',
              'maintain_employee_id'=>'maintain_employee_id',
              'is_same_seri_maintain'=>'is_same_seri_maintain',
//                    'customer_id'=>array(
//                            'asc'=>'customer.first_name',
//                            'desc'=>'customer.first_name DESC',
//                    ),         

        );    
        $sort->defaultOrder = 't.id DESC';      		
        $_SESSION['data-excel'] = new CActiveDataProvider($this, array(
                'pagination'=>false,
                'criteria'=>$criteria,
            'sort' => $sort,
        ));

        return new CActiveDataProvider($this, array(
                'criteria'=>$criteria,
            'pagination'=>array(
                'pageSize'=> Yii::app()->user->getState('pageSize',Yii::app()->params['defaultPageSize']),
            ),
            'sort' => $sort,
        ));
    }

    public function searchCustomerFrequently()
    {
        $criteria=new CDbCriteria;
        $criteria->compare('t.customer_id',$this->customer_id);
        
        $criteria->compare('t.type', TYPE_MARKET_DEVELOPMENT);
        $criteria->compare('t.monitoring_id',$this->monitoring_id);
        $criteria->compare('t.maintain_employee_id',$this->maintain_employee_id);
        $criteria->compare('t.agent_id', $this->agent_id);
        
        $criteria->select = "t.id, t.monitoring_id,t.customer_id,t.agent_id, t.maintain_employee_id,"
                    . " count(t.id) as count_buy, sum(quantity_sell) as quantity_sell";
        $criteria->group = "t.customer_id"; 
        if($this->is_same_seri_maintain){ // nếu check bên trên là chỉ lấy những KH bị trùng ra thì sẽ group như bên dưới
            $criteria->group = "t.customer_id, t.materials_id_sell, t.materials_id_back, t.seri_back"; 
        }
        if($this->qty_buy_from){
            $criteria->having = "count(t.id)>= $this->qty_buy_from"; 
        }else{
            $criteria->compare('t.using_gas_huongminh', 1);
        }
        
        
        $sort = new CSort();
        $sort->attributes = array(
            'agent_id'=>'agent_id',
            'count_buy'=>'count_buy',
            'monitoring_id'=>'monitoring_id',
            'maintain_employee_id'=>'maintain_employee_id',
            'customer_id'=>'customer_id',
              'count_buy'=>array(
                      'asc'=>'count_buy',
                      'desc'=>'count_buy DESC',
              ),         
              'quantity_sell'=>array(
                      'asc'=>'quantity_sell',
                      'desc'=>'quantity_sell DESC',
              ),         

      );    
      $sort->defaultOrder = 't.id DESC';      		
      $_SESSION['data-excel'] = new CActiveDataProvider($this, array(
              'pagination'=>false,
              'criteria'=>$criteria,
          'sort' => $sort,
      ));

    return new CActiveDataProvider($this, array(
        'criteria'=>$criteria,
        'pagination'=>array(
            'pageSize'=> Yii::app()->user->getState('pageSize',Yii::app()->params['defaultPageSize']),
        ),
        'sort' => $sort,
    ));
}

/*
public function activate()
{
    $this->status = 1;
    $this->update();
}

public function deactivate()
{
    $this->status = 0;
    $this->update();
}
    */

    public function defaultScope()
    {
            return array(
                    //'condition'=>'',
            );
    }

    public function beforeValidate() {
        $this->seri_back = trim($this->seri_back);
        $this->quantity_sell = trim($this->quantity_sell);
        $this->quantity_vo_back = trim($this->quantity_vo_back);
        return parent::beforeValidate();
    }

    public function beforeSave() {
        if($this->isNewRecord){
            $this->user_id_create = Yii::app()->user->id;
            $this->code_no = MyFunctionCustom::getNextId('GasMaintainSell', 'S'.date('y'), LENGTH_ORDER_BY_YEAR,'code_no');
        }
        
        if(strpos($this->date_sell, '/')){
            $this->date_sell =  MyFormat::dateConverDmyToYmd($this->date_sell);
            MyFormat::isValidDate($this->date_sell);
        }                
        $this->maintain_id = 0;
        $this->type = TYPE_SELLING_3;

//        GasMaintainSell::setAddressForModel($this); // Aug 02-, 2014 KHÔNG DÙNG HÀM NÀY NỮA vì PTTT dc import từ file excel nên ko có địa chỉ theo tỉnh huyện dc
        // 12-05-2013 FIX thêm 1 điều kiện CUSTOMER ID nữa để xác định bán hàng này tính cho PTTT hay BAO TRI
        
        $mMaintain = GasMaintain::getMaintainByCustomerId($this->customer_id, array('agent_id'=>$this->agent_id));
        if($mMaintain){
            $this->type = $mMaintain->type;
            $this->maintain_id = $mMaintain->id;
            $this->maintain_employee_id = $mMaintain->maintain_employee_id;
            $this->accounting_employee_id = $mMaintain->accounting_employee_id;
            $this->monitoring_id = $mMaintain->monitoring_id;	                    				
            $this->file_scan_id = $mMaintain->file_scan_id;/* ADD Aug 02, 2014 PTTT BIG CHANGE */
        }
        if($this->type==TYPE_MARKET_DEVELOPMENT){
            $this->is_same_seri_maintain = 1; /* FIX LUÔN GẮN = 1 VÀO LÚC Aug 02, 2014 PTTT BIG CHANGE */
            // vì kế toán bán hàng sẽ kiểm soát bình quay về bằng mắt, đối chiếu thực tế, nên hệ thống
            // sẽ không kiểm tra nữa, mặc định những bán hàng này coi là trùng seri luôn
        }

        /* Aug 02, 2014 Begin close PTTT BIG CHANGE 
        // fix 09-11-2013 - ưu tiên cao hơn cho seri và thương hiệu gas - thêm DK KH nữa
        $mMaintain = GasMaintain::getMaintainByMaterialAndSeriNo($this->materials_id_back, 
                $this->seri_back, 
                array('agent_id'=>$this->agent_id,
                    'customer_id'=>$this->customer_id,
                    ));
        if($mMaintain){
            $this->maintain_id = $mMaintain->id;
            $this->maintain_employee_id = $mMaintain->maintain_employee_id;
            $this->accounting_employee_id = $mMaintain->accounting_employee_id;
            $this->monitoring_id = $mMaintain->monitoring_id;				
            $this->type = $mMaintain->type;			
            $this->is_same_seri_maintain = 1;
        }
        /* Aug 02, 2014 END close PTTT BIG CHANGE */

        $this->note = InputHelper::removeScriptTag($this->note);
        $this->note_update_status = InputHelper::removeScriptTag($this->note_update_status);
        $this->note_call_bad = InputHelper::removeScriptTag($this->note_call_bad);
        $this->note_seri_diff = InputHelper::removeScriptTag($this->note_seri_diff);

        return parent::beforeSave();
    }        

    public function beforeDelete() {
        // xóa khuyến mãi của bán hàng này
        // delete promotion of this selling
        GasOneManyBig::deleteOneByType($this->id, ONE_SELL_MAINTAIN_PROMOTION);
        return parent::beforeDelete();
    }



    // @Todo: cập nhật tỉnh, quận huyện, phường xã cho model
    public static function setAddressForModel($model){
       $mUser = Users::model()->findByPk($model->customer_id);
       if($mUser){
           $model->province_id = $mUser->province_id;
           $model->district_id = $mUser->district_id;
           $model->ward_id = $mUser->ward_id;
       }             
    }

    public static function getByCustomerId($customer_id){
       $criteria=new CDbCriteria;
       $criteria->compare('t.customer_id', $customer_id);
       return self::model()->findAll($criteria);
    }

    /**
     * @Author: ANH DUNG Apr 02, 2014
     * @Todo: kiểm tra xem đã tồn tại 1 bán hàn nào trước đó với thông tin như vậy chưa
     * @param: customer_id KH
     * @param: materials_id_sell thương hiệu ga bán
     * @param: materials_id_back thương hiệu ga vỏ thu về
     * @param: seri_back seri vo thu  ve
     * @return true nếu đã có 1 record trên hệ thống,  false nếu chưa có
     */     
    public static function hasSellBefore($customer_id, $materials_id_sell, $materials_id_back, $seri_back ){         
       $criteria=new CDbCriteria;
       $criteria->compare('t.customer_id', $customer_id);
       $criteria->compare('t.materials_id_sell', $materials_id_sell);
       $criteria->compare('t.materials_id_back', $materials_id_back);
       $criteria->compare('t.seri_back', trim($seri_back));
       $model = self::model()->find($criteria);
       if($model)
           return true;
       return false;
    }
     
    protected function afterValidate() {
        if($this->isNewRecord && GasMaintainSell::hasSellBefore($this->customer_id, $this->materials_id_sell, $this->materials_id_back, $this->seri_back))
        {             
            $this->addError('customer_id', 'Thông tin về bán hàng này dã có trên hệ thống. Không thể tạo thêm');
        }
        return parent::afterValidate();
    }
     
    /**
     * @Author: ANH DUNG Feb 06, 2015
     * @Todo: delete bán hàng PTTT by agent id
     * 1/ Phục vụ y/c Feb 06, 2015 13:14 của đại lý Bình Thung (314408) xóa 324 record
     * @Param: $agent_id
     */
    public static function DeleteByAgentId($agent_id) {
        $from = time();
        $criteria = new CDbCriteria();
        $criteria->compare('t.agent_id', $agent_id);
        $models = self::model()->findAll($criteria);
        echo count($models);die;
        foreach($models as $model){
            $model->delete();
        }
        $to = time();
        $second = $to-$from;
        echo count($models).' done in: '.($second).'  Second  <=> '.($second/60).' Minutes';die;        
    }    
     
    /**
     * @Author: ANH DUNG Sep 24, 2015
     * @Todo: get type of admin set user
     */
    public function getTypeAdminSetUser() {
        return array(
            GasOneManyBig::TYPE_PRICE_REQUEST_1 => 'Duyệt Tăng Giảm Bò Mối Cấp 1',
            GasOneManyBig::TYPE_PRICE_REQUEST_2 => 'Duyệt Tăng Giảm Bò Mối Cấp 2',
            GasOneManyBig::TYPE_PRICE_REQUEST_3 => 'Duyệt Tăng Giảm Bò Mối Cấp 3',
            
            GasOneManyBig::TYPE_AGENT_BOSS => 'Đại Lý - Chủ Cở Sở',
            GasOneManyBig::TYPE_CUSTOMER_SPECIAL_1 => 'KH Đặc Biệt Duyệt Cấp 1',
            GasOneManyBig::TYPE_CUSTOMER_SPECIAL_2 => 'KH Đặc Biệt Duyệt Cấp 2',
            GasOneManyBig::TYPE_CUSTOMER_SPECIAL_3 => 'KH Đặc Biệt Duyệt Cấp 3',
            GasOneManyBig::TYPE_LEAVE => 'Duyệt Nghỉ Phép',
            GasOneManyBig::TYPE_DELOY_BY => 'Đơn Vị Thực Hiện - Giao Nhiệm Vụ',
            GasOneManyBig::TYPE_ISSUE_ACCOUNTING => 'Kế Toán Xử Lý - Phản Ánh Sự Việc',
            GasOneManyBig::TYPE_ORDER_PROMOTION => 'Người nhận - Đặt Hàng Khyến Mãi',
            // Trung June 06, 2016 Add 2 type of settle
            GasOneManyBig::TYPE_SETTLE_LEADER => 'Duyệt Quyết Toán - Quản lý',
            GasOneManyBig::TYPE_SETTLE_DIRECTOR => 'Duyệt Quyết Toán - Giám Đốc',
            GasOneManyBig::TYPE_SETTLE_CHIEF_ACCOUNTANT => 'Duyệt Quyết Toán - Kế Toán Trưởng',
            GasOneManyBig::TYPE_SETTLE_ACCOUNTING => 'Duyệt Quyết Toán - Kế toán thanh toán',
            GasOneManyBig::TYPE_SETTLE_CASHIER_CONFIRM => 'Duyệt Quyết Toán - Thủ quỹ',
            GasOneManyBig::TYPE_APPROVE_SUPPORT_CUSTOMER => 'Phiếu Giao Nhiệm Vụ - Người duyệt cả 3 cấp',
            GasOneManyBig::TYPE_BORROW_ACCOUNTING => 'Quản lý vay - Kế Toán theo dõi, tạo mới',
        );
    }
    
}