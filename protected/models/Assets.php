<?php

/**
 * @author DuongNV Sep1619
 * Quan ly tai san
 */
class Assets extends Users {

    public static function getArrayStatus(){
        return [
            STATUS_ACTIVE   => 'Hoạt động',
            STATUS_INACTIVE => 'Không hoạt động',
        ];
    }

    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    public function attributeLabels() {
        $aLabels = parent::attributeLabels();
        $aLabels['first_name']      = 'Tên tài sản';
        $aLabels['created_by']      = 'Người tạo';
        $aLabels['last_name']       = 'Định giá tài sản';
        $aLabels['code_bussiness']  = 'Mã tài sản';
        return $aLabels;
    }

    public function rules() {
        $aLabels    = parent::rules();
        $aLabels[]  = array('first_name, province_id, district_id', 'required', 'on' => 'create_assets, update_assets');
        $aLabels[]  = array('date_from, date_to', 'safe');
        return $aLabels;
    }
    
    public function getWard(){
        return empty($this->ward) ? '' : $this->ward->name;
    }
    
    public function getStatus(){
        $aStatus = self::getArrayStatus();
        return empty($aStatus[$this->status]) ? '' : $aStatus[$this->status];
    }
    
    // dinh gia tai san
    public function getAssetsValuation(){
        return empty($this->last_name) ? '' : ActiveRecord::formatCurrency($this->last_name);
    }
    
    /** @Author: DuongNV Sep1719
     *  @Todo: can create, update assets
     **/
    public function canUpdate(){
        $cUid       = MyFormat::getCurrentUid();
        $cRole      = MyFormat::getCurrentRoleId();
        $aRoleAllow = [ROLE_ADMIN, ROLE_CHIEF_ACCOUNTANT];
        $aUidAllow  = [GasLeave::UID_QUYNH_MTN];
        return in_array($cUid, $aUidAllow) || in_array($cRole, $aRoleAllow);
    }

    public function searchAssets() {
        $criteria = new CDbCriteria;
        $criteria->compare('t.id', $this->id);
        $criteria->compare('t.role_id', ROLE_ASSETS);
        $criteria->compare('t.status', $this->status);
        if(!empty($this->province_id) && is_array($this->province_id)){
            $criteria->addInCondition('t.province_id', $this->province_id);
        }
        if(!empty($this->date_from)){
            $date_from = MyFormat::dateConverDmyToYmd($this->date_from);
            DateHelper::searchGreater($date_from, 'created_date_bigint', $criteria);
        }
        if(!empty($this->date_to)){
            $date_to = MyFormat::dateConverDmyToYmd($this->date_to);
            DateHelper::searchSmaller($date_to, 'created_date_bigint', $criteria, true);
        }
        $this->LimitProvince($criteria); // ADD DEC 29, 2014

        $sort = new CSort();
        $sort->attributes = array(
            'first_name' => 'first_name',
            'code_account' => 'code_account',
            'parent_id' => 'parent_id',
            'province_id' => 'province_id',
            'district_id' => 'district_id'
        );
        $sort->defaultOrder = 't.id DESC';
        $_SESSION['data-excel-branch'] = new CActiveDataProvider($this, array(
            'pagination' => false,
            'criteria' => $criteria,
        ));
        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
            'pagination' => array(
                'pageSize' => 30,
            ),
            'sort' => $sort,
        ));
    }
    
    /** @Author: DuongNV Sep1619
     *  @Todo: init some attr before save
     **/
    public function handleBeforeSave() {
        $this->last_name = MyFormat::removeCommaAndPoint($this->last_name);
        if($this->scenario == 'create_assets'){
            $this->role_id          = ROLE_ASSETS;
            $this->password_hash    = '';
            $this->temp_password    = '';
            $this->address_temp     = '';
            $this->first_char       = '';
            $this->ip_address       = '';
            $this->application_id   = '';
        }
    }

}
