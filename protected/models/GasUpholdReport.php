<?php

/**
 * This is the model class for table "{{_gas_uphold}}".
 *
 * The followings are the available columns in table '{{_gas_uphold}}':
 * @property string $id
 * @property string $code_no
 * @property integer $type
 * @property string $customer_id
 * @property integer $level_type
 * @property string $employee_id
 * @property integer $type_uphold
 * @property string $content
 * @property string $contact_person
 * @property string $contact_tel
 * @property integer $status
 * @property string $uid_login
 * @property string $created_date
 * @property string $has_read
 */
class GasUpholdReport extends GasUphold
{
    public $area_id, $c_customer_id;
    /** @Author: LocNV Aug , 2019
     *  @Todo: report piece maintain - bao cao khoan bao tri
     *  @Param:
     **/
    public function reportPieceMaintain() {
        $aRes = $aProvince = $aProvinceByZone = [];
                
        if(!empty($this->area_id)){
            $aProvinceByZone = GasOrders::getTargetZoneProvinceId()[$this->area_id];
        }
        
        if (!empty($this->aProvinceId) && is_array($this->aProvinceId)) {
            $aProvince = array_merge($this->aProvinceId, $aProvinceByZone);
        } else {
            $aProvince = $aProvinceByZone;
        }
        
        //report by sale
        $dataSale = $aSale = [];
        $this->ReportPieceMaintainBySale($aSale, $dataSale, $aProvince);
        
        //report by employee
        $dataEmployee = $aEmployee = [];
        $this->ReportPieceMaintainByEmployee($aEmployee, $dataEmployee, $aProvince);
        
        //return
        $aRes['PROVINCES'] = GasProvince::getArrAll();
        $aRes['ARR_PROVINCE'] = $aProvince;
        //sale
        $aRes['DATA_SALE'] = $dataSale;
        $aRes['SALE_MODEL'] = $aSale;
        //employee
        $aRes['DATA_EMPLOYEE'] = $dataEmployee;
        $aRes['EMPLOYEE_MODEL'] = $aEmployee;
        
        return $aRes;        
    }
    
    /** @Author: LocNV Aug 19, 2019
     *  @Todo: report piece maintain employee criteria
     *  @Param:
     **/
    public function ReportPieceMaintainEmployeeCriteria($aProvince) {
        $aData = [];
        //chi thong ke khach hang bo, moi
        $aInTypeCustomer = implode(',', 
            [
                STORE_CARD_KH_BINH_BO,
                STORE_CARD_KH_MOI
            ]);

        if (!empty($aProvince)) {
            $criteria = new CDbCriteria();
            $aInProvince = implode(',', $aProvince);
            $criteria->addCondition("t.province_id IN ($aInProvince)");
            $criteria->addCondition("t.type_customer IN ($aInTypeCustomer)");
            //lay trang thai con bao tri
            $criteria->compare('t.status_uphold_schedule', GasUphold::STATUS_UPHOLD_RUNNING);
            //trang thai bao tri dinh ky
            $criteria->compare('t.type', GasUphold::TYPE_DINH_KY);

            $criteria->select = 'employee_id, province_id, type_customer, COUNT(customer_id) as c_customer_id';
            $criteria->group = 'employee_id, province_id, type_customer';
            
            $mGasUpholdReport = new GasUpholdReport();
            $aData = $mGasUpholdReport->findAll($criteria);
        }
        
        return $aData;
    }

    /** @Author: LocNV Aug 20, 2019
     *  @Todo: report piece maintain by sale
     *  @Param: $aSale
     *  @Param: $dataSale
     *  @Param: $aProvince
     **/
    public function ReportPieceMaintainBySale(&$aSale, &$dataSale, $aProvince)
    {
        $aDataSale = $this->ReportPieceMaintainSaleCriteria($aProvince);

        foreach ($aDataSale as $mGasUphold) {
            $aSale[] = $mGasUphold->sale_id;
            $dataSale[$mGasUphold->sale_id][$mGasUphold->province_id][$mGasUphold->is_maintain] = $mGasUphold->id;
            if ($mGasUphold->is_maintain == STORE_CARD_KH_BINH_BO) {
                if (empty($dataSale[$mGasUphold->sale_id]['sumBo'])) {
                    $dataSale[$mGasUphold->sale_id]['sumBo'] = $mGasUphold->id;
                } else {
                    $dataSale[$mGasUphold->sale_id]['sumBo'] += $mGasUphold->id;
                }

                if (empty($dataSale[$mGasUphold->province_id]['sumBo'])) {
                    $dataSale[$mGasUphold->province_id]['sumBo'] = $mGasUphold->id;
                } else {
                    $dataSale[$mGasUphold->province_id]['sumBo'] += $mGasUphold->id;
                }

            } elseif ($mGasUphold->is_maintain == STORE_CARD_KH_MOI) {
                if (empty($dataSale[$mGasUphold->sale_id]['sumMoi'])) {
                    $dataSale[$mGasUphold->sale_id]['sumMoi'] = $mGasUphold->id;
                } else {
                    $dataSale[$mGasUphold->sale_id]['sumMoi'] += $mGasUphold->id;
                }

                if (empty($dataSale[$mGasUphold->province_id]['sumMoi'])) {
                    $dataSale[$mGasUphold->province_id]['sumMoi'] = $mGasUphold->id;
                } else {
                    $dataSale[$mGasUphold->province_id]['sumMoi'] += $mGasUphold->id;
                }
            }
        }
        $aSale = array_filter($aSale);

        $aSale = Users::getArrayModelByArrayId($aSale);
    }

    /** @Author: LocNV Aug 20, 2019
     *  @Todo: report piece maintain by employee
     *  @Param: $aEmployee
     *  @Param: $dataEmployee
     *  @Param: $aProvince
     **/
    public function ReportPieceMaintainByEmployee(&$aEmployee, &$dataEmployee, $aProvince)
    {
        $aDataEmployee = $this->ReportPieceMaintainEmployeeCriteria($aProvince);
        
        foreach ($aDataEmployee as $mGasUphold) {
            $aEmployee[] = $mGasUphold->employee_id;
            $dataEmployee[$mGasUphold->employee_id][$mGasUphold->province_id][$mGasUphold->type_customer] = $mGasUphold->c_customer_id;
            if ($mGasUphold->type_customer == STORE_CARD_KH_BINH_BO) {
                if (empty($dataEmployee[$mGasUphold->employee_id]['sumBo'])) {
                    $dataEmployee[$mGasUphold->employee_id]['sumBo'] = $mGasUphold->c_customer_id;
                } else {
                    $dataEmployee[$mGasUphold->employee_id]['sumBo'] += $mGasUphold->c_customer_id;
                }
                
                if (empty($dataEmployee[$mGasUphold->province_id]['sumBo'])) {
                    $dataEmployee[$mGasUphold->province_id]['sumBo'] = $mGasUphold->c_customer_id;
                } else {
                    $dataEmployee[$mGasUphold->province_id]['sumBo'] += $mGasUphold->c_customer_id;
                }
                
            } elseif ($mGasUphold->type_customer == STORE_CARD_KH_MOI) {
                if (empty($dataEmployee[$mGasUphold->employee_id]['sumMoi'])) {
                    $dataEmployee[$mGasUphold->employee_id]['sumMoi'] = $mGasUphold->c_customer_id;
                } else {
                    $dataEmployee[$mGasUphold->employee_id]['sumMoi'] += $mGasUphold->c_customer_id;
                }
                
                if (empty($dataEmployee[$mGasUphold->province_id]['sumMoi'])) {
                    $dataEmployee[$mGasUphold->province_id]['sumMoi'] = $mGasUphold->c_customer_id;
                } else {
                    $dataEmployee[$mGasUphold->province_id]['sumMoi'] += $mGasUphold->c_customer_id;
                }
            }
        }
        $aEmployee = array_filter($aEmployee);
        
        $aEmployee = Users::getArrayModelByArrayId($aEmployee);
    }

    /** @Author: LocNV Sep 20, 2019
     *  @Todo: report piece maintain sale criteria
     *  @Param:
     **/
    public function ReportPieceMaintainSaleCriteria($aProvince)
    {
        $aData = [];
        $aInTypeCustomer = implode(',',
            [
                STORE_CARD_KH_BINH_BO,
                STORE_CARD_KH_MOI
            ]);
        if (!empty($aProvince)) {
            $criteria = new CDbCriteria();
            $aInProvince = implode(',', $aProvince);
            $criteria->addCondition("t.province_id IN ($aInProvince)");
            $criteria->addCondition("t.is_maintain IN ($aInTypeCustomer)");
            $criteria->compare('t.role_id', ROLE_CUSTOMER);
            $criteria->compare('t.type', CUSTOMER_TYPE_STORE_CARD);
            $criteria->compare('t.channel_id', Users::CON_LAY_HANG);
            $criteria->select = 'sale_id, province_id, is_maintain, COUNT(id) as id';
            $criteria->group = 'sale_id, province_id, is_maintain';

            $aData = Users::model()->findAll($criteria);
        }

        return $aData;
    }
    
}