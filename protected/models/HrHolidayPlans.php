<?php

/**
 * This is the model class for table "{{_leave_holidays_plan}}".
 *
 * The followings are the available columns in table '{{_leave_holidays_plan}}':
 * @property string $id
 * @property integer $status
 * @property string $approved
 * @property string $approved_date
 * @property string $notify
 * @property string $created_by
 * @property string $created_date
 */
class HrHolidayPlans extends BaseSpj
{
    public $STATUS_NEW              = 1;
    public $STATUS_APPROVED         = 2;
    public $STATUS_CANCEL           = 3;
    public $STATUS_REQUIRED_UPDATE  = 4;
    public $is_sendmail = 1; // gui mail neu update plan
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{_hr_holiday_plans}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        return array(
            array('id, status, approved', 'required','on'=>'create,update'),
            array('id','unique','message'=>'Mỗi năm chỉ cho phép một lịch duy nhất','on'=>'create,update'),
            array('id, status, approved, approved_date, notify, created_by, created_date', 'safe'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        return array(
            'rCreatedBy' => array(self::BELONGS_TO, 'Users', 'created_by'),
            'rApproved' => array(self::BELONGS_TO, 'Users', 'approved'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
            return array(
                'id' => 'Năm',
                'status' => 'Trạng thái',
                'approved' => 'Người duyệt',
                'approved_date' => 'Ngày duyệt',
                'notify' => 'Yêu cầu cập nhật',
                'created_by' => 'Người tạo',
                'created_date' => 'Ngày tạo',
            );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        $criteria=new CDbCriteria;
        if(!empty($this->id)) {
            $criteria->addCondition('t.id ='.$this->id);
        }
        if(!empty($this->approved)) {
            $criteria->addCondition('t.approved ='.$this->approved);
        }
        if(!empty($this->status)) {
            $criteria->addCondition('t.status ='.$this->status);
        }
        $criteria->order = 't.id DESC';
        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
            'pagination'=>array(
                'pageSize'=> Yii::app()->user->getState('pageSize',Yii::app()->params['defaultPageSize']),
            ),
        ));
    }
    
    /** @Author: HOANG NAM 30/03/2018
     *  @Todo: get
     *  @Param: 
     **/
    public function getYear(){
        return $this->id;
    }
    
    public function getStatus(){
        $aStatus = $this->getArrayStatus();
        return isset($aStatus[$this->status]) ? $aStatus[$this->status] : '';
    }
    
    public function getApproved(){
        return !empty($this->rApproved) ? $this->rApproved->first_name : '';
    }
    
    public function getApprovedDate($fomat = 'd/m/Y H:i'){
        return MyFormat::dateConverYmdToDmy($this->approved_date, $fomat);
    }
    
    public function getNotify(){
        return $this->notify;
    }
    
    public function getCreatedBy(){
        return !empty($this->rCreatedBy) ? $this->rCreatedBy->first_name : '';
    }
    
    public function getArrayStatus(){
        return array(
            $this->STATUS_NEW =>'Chờ duyệt',
            $this->STATUS_APPROVED =>'Đã duyệt',
            $this->STATUS_CANCEL =>'Hủy',
            $this->STATUS_REQUIRED_UPDATE =>'Yêu cầu cập nhật'
        );
    }
    
    public static function getApprovePersons()
    {
        $aModelUser = GasOneManyBig::getArrayModelUser(GasOneManyBig::TYPE_APPROVE_LEAVE_HOLIDAY, GasOneManyBig::TYPE_APPROVE_LEAVE_HOLIDAY);
        $aRes = array();
        foreach ($aModelUser as $item) {
            $aRes[$item->id] = $item->getNameWithRole();
        }
        return $aRes;
    }
    
    /** @Author: HOANG NAM 30/03/2018
     *  @Todo: before save function
     *  @Param: 
     **/
    public function setAttributesBeforeSave(){
        if(empty($this->created_by)){
            $this->created_by       = MyFormat::getCurrentUid();
        }
        if(empty($this->status)){
            $this->status       = $this->STATUS_NEW;
        }
    }
    
    /** @Author: HOANG NAM 02/04/2018
     *  @Todo: get notify
     *  @Param: 
     **/
    public function getHtmlNotify(){
        $result = '';
        if($this->status != $this->STATUS_APPROVED){
            $result .= '<b>Năm '. $this->id.' chưa có kế hoạch nghỉ lễ hoặc kế hoạch chưa được duyệt.</b>';
            $result .= '<br>';
            $result .= 'Khởi tạo lịch nghỉ mặc định: <a class="btn btn-small" href="';
            $result .= Yii::app()->createUrl('admin/GasLeaveHolidays/Generate', array('year' => $this->id));
            $result.='"><button class="btn btn-small" style="background:#0080FF; color: white;border: none;padding: 5px 10px;border-radius: 5px;font-weight: bold;cursor: pointer;" type="button">Create</button></a>';
            $result .= '<br><br>';
            $result .= '<b>Vui lòng thực hiện các bước sau:</b>';
            $result .= '<br>';
            $result .= '<span class="required">Bước 1:</span> Tạo ngày nghỉ lễ bằng cách click vào ngày cần tạo hoặc chỉnh sửa.';
            $result .= '<br>';
            $result .= '<span class="required">Bước 2:</span> Sau khi tạo, Chọn người duyệt và click Create hoặc Save để gửi yêu cầu đến người duyệt. ';
        }
        return $result;
    }
    
    /**
     * Check if model was approved
     * @return True if status is STATUS_APPROVED, false otherwise
     */
    public function isApproved() {
        return ($this->status == $this->STATUS_APPROVED);
    }
}