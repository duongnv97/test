<?php

/**
 * This is the model class for table "{{_app_login_lock}}".
 *
 * The followings are the available columns in table '{{_app_login_lock}}':
 * @property string $id
 * @property integer $is_lock
 * @property integer $total_user_id
 * @property string $username_open
 * @property integer $platform
 * @property integer $app_type
 * @property string $device_name
 * @property string $device_imei
 * @property string $device_os_version
 * @property string $version_code
 * @property string $last_update
 */
class AppLoginLock extends BaseSpj
{
    const DEVICE_OPEN               = 0;
    const DEVICE_LOCK               = 1;
    
    const MAX_LOGIN_GAS24H          = 1;
    const MAX_LOGIN_GAS_SERVICE     = 1;// Mar0519 ko ro tai sao nang len 10
    
    const TAB_TYPE_GENERAL          = 1;
    const TAB_TYPE_VERSION_OS       = 2;
    
    public $count_platform, $count_version_os;
    
    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return AppLoginLock the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{_app_login_lock}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        return [
            ['id, is_lock, total_user_id, username_open, platform, app_type, device_name, device_imei, device_os_version, version_code, last_update', 'safe'],
        ];
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        return [
        ];
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'is_lock' => 'Trạng thái khóa',
            'total_user_id' => 'Total User',
            'username_open' => 'Username Open',
            'platform' => 'Platform',
            'app_type' => 'Loại App',
            'device_name' => 'Tên thiết bị',
            'device_imei' => 'Imei',
            'device_os_version' => 'Phiên bản Os',
            'version_code' => 'Version Code',
            'last_update' => 'Last Update',
        ];
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        $criteria=new CDbCriteria;

	$criteria->compare('t.is_lock',$this->is_lock);
	$criteria->compare('t.total_user_id', $this->total_user_id);
	$criteria->compare('t.username_open', trim($this->username_open));
	$criteria->compare('t.platform',$this->platform);
	$criteria->compare('t.app_type',$this->app_type);
	$criteria->compare('t.device_name', trim($this->device_name));
	$criteria->compare('t.device_imei', trim($this->device_imei));
	$criteria->compare('t.device_os_version', trim($this->device_os_version));
	$criteria->compare('t.version_code', trim($this->version_code));
        
//        if(isset($this->last_update)){
//            $date = MyFormat::dateConverDmyToYmd($this->last_update,'-');
//            $criteria->compare('DATE(t.last_update)',$date);
//        }
        $lastUpdate = '';
        if(!empty($this->last_update)){
            $lastUpdate = MyFormat::dateDmyToYmdForAllIndexSearch($this->last_update);
            $endOfLastUpdate = $lastUpdate . ' 23:59:59';
            // Search between 2018-09-16 00:00:00 and 2018-09-16 23:59:59
            DateHelper::searchBetween($lastUpdate, $endOfLastUpdate, 'last_update_bigint', $criteria);
        }
        $criteria->order = 't.total_user_id DESC';
        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
            'pagination'=>array(
                'pageSize'=> Yii::app()->user->getState('pageSize',Yii::app()->params['defaultPageSize']),
            ),
        ));
    }
    
    /** @Author: ANH DUNG Sep 15, 2018
     *  @Todo: check lock device
     *  @return: true if lock, false if not
     **/
    public function isLock() {
        if($this->app_type == UsersTokens::APP_TYPE_GAS_SERVICE){
            return $this->total_user_id >= AppLoginLock::MAX_LOGIN_GAS_SERVICE;
        }else{
            return $this->total_user_id >= AppLoginLock::MAX_LOGIN_GAS24H;
        }
    }
    
    /** @Author: DuongNV
     *  @Todo:
     *  @Param:
     **/
    public function getArrayLockStatus() {
        return [
            self::DEVICE_OPEN => 'Open',
            self::DEVICE_LOCK => 'Lock'
        ];
    }
    
    public function getLockStatus() {
        $aStatus = $this->getArrayLockStatus();
        return $aStatus[$this->is_lock];
    }
    
    public function getPlatform() {
        $aPlatform = AppLogin::model()->getArrayPlatform();
        return empty($aPlatform[$this->platform]) ? '' : $aPlatform[$this->platform];
    }
    
    public function getAppType() {
        $aAppType = AppLogin::model()->getArrayAppType();
        return empty($aAppType[$this->app_type]) ? '' : $aAppType[$this->app_type];
    }
    
    public function getLastUpdate($fomat = 'd/m/Y H:i') {
        return MyFormat::dateConverYmdToDmy($this->last_update, $fomat);
    }
    
    /** @Author: DuongNV Sep 19, 2018
     *  @Todo: Thống kê theo app và platform
     *  @Param:
     **/
    public function getStatisticalApp() {
        $criteria = new CDbCriteria;
        $criteria->select = 't.platform, t.app_type, COUNT( t.platform ) AS count_platform';
        $criteria->group = 't.app_type, t.platform';
        $model = self::model()->findAll($criteria);
        $aRes = [];
        foreach ($model as $item) {
            $aRes[$item->app_type][$item->platform] = $item->count_platform;
        }
        return $aRes;
    }
    
    /** @Author: DuongNV Sep 19, 2018
     *  @Todo: Thống kê theo version os
     *  @Param:
     **/
    public function getStatisticalVersionOs() {
        $criteria = new CDbCriteria;
        $criteria->select = 't.platform, t.device_os_version, count(t.device_os_version) as count_version_os';
        $criteria->group = 't.device_os_version';
        $criteria->order = 'count_version_os desc';
        $model = self::model()->findAll($criteria);
        $aRes = [];
        foreach ($model as $key => $item) {
//            Sort by version os
//            preg_match_all('(\d+(?:\.\d+)*)', $item->device_os_version, $matches);
//            $key_os = str_replace(".", "", $matches[0][0]);
            $aRes[$item->platform][] = [
                                        'count' => $item->count_version_os,
                                        'name' => $item->device_os_version
                                    ];
        }
//        foreach ($aRes as $key => $value) { // Sort by version os
//            krsort($value);
//            $aRes[$key] = $value;
//        }
        return $aRes;
    }
}