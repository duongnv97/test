<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
class GasCustomerImages extends EmployeesImages{
    
    public function getListType(){
        return array(
            self::TYPE_CUSTOMER_CONTRACT            => 'Hợp đồng',
            self::TYPE_CUSTOMER_CONTRACT_ASSEST     => 'Biên bản mượn tài sản',
        );
    }
    
     
    /** @Author: Pham Thanh Nghia Aug 22, 2018
     *  @Todo: lấy link hiện danh sách file
     *  @Param: use for admin/gascustomer/customer_store_card
     *  @Param: $mCustomer model Users
     **/
    public static function getActionsForGasCustomer($mCustomer){
        $aRoleSale          = [ROLE_SALE, ROLE_MONITORING_MARKET_DEVELOPMENT];
        $aRoleAllowAll      = [ROLE_SALE_ADMIN, ROLE_HEAD_GAS_BO, ROLE_DIRECTOR_BUSSINESS, ROLE_ADMIN];
        $cRole = MyFormat::getCurrentRoleId();
        $cUid = MyFormat::getCurrentUid();
        if(in_array($cRole, $aRoleSale) && $mCustomer->sale_id != $cUid){
            return '';
        }
        if(!in_array($cRole, $aRoleSale) && !in_array($cRole, $aRoleAllowAll)){
            return '';
        }
        $url     = Yii::app()->createAbsoluteUrl('admin/gascustomer/manageFile', array('id' => $mCustomer->id));
        return '<a class=\'targetHtml\' href=\''.$url.'\'>Quản lý file</a>';
        
    }
    
}
