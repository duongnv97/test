<?php

/**
 * This is the model class for table "{{_app_login}}".
 *
 * The followings are the available columns in table '{{_app_login}}':
 * @property string $id
 * @property string $user_id
 * @property integer $platform
 * @property integer $app_type
 * @property string $device_name
 * @property string $device_imei
 * @property string $device_os_version
 * @property string $version_code
 * @property string $created_date
 */
class AppLogin extends BaseSpj
{
    public $onlyCheckNotUpdate = false, $autocomplete_user;
    
    /** @Author: DungNT Aug 16, 2019
     *  @Todo: get list User allow login multi device in same time 
     **/
    public function getArrUserAllowLoginSameTime() {
        return [
            173, // Aug1619 NV Phục Vụ Khách Hàng - Lương Đình Hải - Đại lý Hóc Môn
        ];
    }
    
    public function getDeviceImeiTest() {
        return [
            '354603070127314', // Sep1518 Samsung A9 - AnhDung
            '867309032867829', // Sep2718 trantrunghieu - Redmi Note 4 
            '357768072664478', // Now0118 KTKV Tran Thi Van SM-C710F
            '353317099189889', // Now0118 KTKV Tran Thi Le SM-J730G
            '352700077922333', // Now0118 KTKV Tran Thi Quy SM-J500H
            '358118082518547', // Now0118 KTKV Nguyen Hoa Thuan G3312
//            '359051097164032', // Now0218 KTKV Dang Huu Phuc SM-J810Y
            '7AD2D48E-F789-4F68-89AE-91D78D104C62', // KTKV - Nguyen Thi Minh
            '38F4F4B1-F5C1-40CC-9C2B-B95F363772DF', // KTKV - Hoang Thi Ngoc Anh
            'CB32D124-7C12-4052-80E3-C0D48708D524', // KTKV - Tran Thi Tuyet Nhung
            '355647071429475', // Nguyen Tan Nhat - CV
            
            '866001035859959', // Le Xuan Sang - CV
            '860758048980173', // Le Chau Thuan An - CV
            '865251032710694', // Nguyen Van Chien - KTKV
//            '359654060244051', // Vo Hoang Anh - CV
            '358449071488701', // Ngoc Tram - KTVK
            'C9EBDFD5-30C9-4B8F-8D1B-4A5350623578', // HaPT- Sale Admin
//            '357625082051130', // Trần Văn Phong - Test Tong Dai
            '867013043129852', // Ha Van Trung
            '351704090091762', // Ha Van Trung
//            '867193036344393', // Le Chau Thuan An - CV
//            '359654060244051', // Vo Hoang Anh - CV
            '357931090913125', // Duong Trung Kien - Marketing
            '353236070522589', // CV - Nguyễn Trung Hiếu
            '2C92F75F-93D9-4D0D-8958-88B7B4C91144', // KT - Trần Minh Hiếu
            '356563091202116', // TX - Nguyen Van Dong
            '9BB9CC62-F163-4107-9937-8169373F5A54', // PV Duc
            '352810096894031', // Dao Duc Huy
            '861570045567039', // thanhtc - CV - Trinh cong thanh 
            '358334081277403', // txdongnv - BaoTri - Nguyen Van Dong - Gia Lai
            '356600050653887', // truongln - CV
            '356446086014144', // ngocpt - Pham Thi Ngoc
            '352774085666935', // trikm - Kha Minh Tri -= Bao Tri ho tro chay Gas
            '358484064656329', // Phan Tinh
            '355726098294540', // Phu Xe Từ Văn Phương, allow login nhiều máy
            '867836035537983', // CV - Ngo Minh Tam
            '355079081564788', // Lai Xe - Sam Van Hieu
            '861433043045054', // CV - Đặng Trung Hiếu
            '353317099160088', // KTKV - Doan Thi Le Phuong - Đặng Trung Hiếu
            '357022095308832', // Telesale Bich Ven - nhap Mã công đoàn
            '353317095898780', // Ngoc Kien
            '864124048012093', // Nguyen Thi Duyen - ben Cat
            '4E5B8131-245F-440A-BC2B-32DA9672D458', // Vu Thanh Son - Gia Lai
            '863897037397713', // Phan Thành Nhân - 
            '77B235CB-B78C-419E-8AAA-A017AAF0B2BE', // ktkvquyenptl Phan Thi Le Quyen
            '352812105338380', // KTKV Tran Thi Ngoc Tram
            '355758082876123', // CV Nguyễn Đình Đại Nghĩa
            '866346037987732', // Nguyen Van Tuan - Thu Kho PTan
            '53EFAA9C-6E92-4462-84D5-B590AC3C34D8', // CV Nguyễn Nhật Trường
            '362775D3-FAAF-4BB4-A216-98386CF70E1A', // CV Vo Hoang Anh
            '864182040214273', // KTVP Bui Quyet Chien
            '352291080022352', // Duong IT

            
//            '353236072538088', // Test Trinh
            '02A5D3A6-C0A1-4A66-AAB5-992FDA24ABD1', // Dev cty iphone 5 
            '358548062169889', // Dev Nam
            '869071039090312', // Dev Trung - Mi 8
            '9caf05068f10dd08', // Dev Trung - Pixel C
            '868773033467930', // Dev Nghia - Redmi Note 5
        ];
    }
    
    /** @Author: ANH DUNG Sep 18, 2018
     *  @Todo: get array role not check lock
     **/
    public function getRoleNotCheck() {
        return [
            ROLE_ACCOUNTING_ZONE,
            ROLE_CRAFT_WAREHOUSE,
            ROLE_SECURITY,
        ];
    }
    
    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return AppLogin the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{_app_login}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        return [
            ['id, username, user_id, platform, app_type, device_name, device_imei, device_os_version, version_code, created_date', 'safe'],
        ];
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        return [
            'rUser' => array(self::BELONGS_TO, 'Users', 'user_id'),
        ];
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'Nhân viên',
            'platform' => 'Platform',
            'app_type' => 'Loại app',
            'device_name' => 'Tên thiết bị',
            'device_imei' => 'Imei',
            'device_os_version' => 'Phiên bản OS',
            'version_code' => 'Version Code',
            'created_date' => 'Ngày tạo',
        ];
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        $criteria=new CDbCriteria;

	$criteria->compare('t.user_id',$this->user_id);
	$criteria->compare('t.username', trim($this->username));
	$criteria->compare('t.platform',$this->platform);
	$criteria->compare('t.app_type',$this->app_type);
	$criteria->compare('t.device_name', trim($this->device_name));
	$criteria->compare('t.device_imei', trim($this->device_imei));
	$criteria->compare('t.device_os_version', trim($this->device_os_version));
	$criteria->compare('t.version_code', trim($this->version_code));
//        if(isset($this->created_date)){
//            $date = MyFormat::dateConverDmyToYmd($this->created_date,'-');
//            $criteria->compare('DATE(t.created_date)',$date);
//        }
        $createdDate = '';
        if(!empty($this->created_date)){
            $createdDate = MyFormat::dateDmyToYmdForAllIndexSearch($this->created_date);
            $endOfCreatedDate = $createdDate . ' 23:59:59';
            // Search between 2018-09-16 00:00:00 and 2018-09-16 23:59:59
            DateHelper::searchBetween($createdDate, $endOfCreatedDate, 'created_date_bigint', $criteria);
        }
        $criteria->order = 't.id DESC';
        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
            'pagination'=>array(
                'pageSize'=> Yii::app()->user->getState('pageSize',Yii::app()->params['defaultPageSize']),
            ),
        ));
    }
    
    
    /** @Author: ANH DUNG Sep 15, 2018 
     *  @Todo: handle save info login app
     **/
    public function handleSave($mUser, $q) {
        try{
//        error_reporting(1); // var_dump(PHP_INT_SIZE);
        if(!isset($q->device_imei)){
            return ;
        }
        $this->user_id              = $mUser->id;
        $this->username             = $mUser->username;
        $this->role_id              = $mUser->role_id;
        $this->mapDataPost($q);
        $this->save();
        $this->checkDeviceLock();
        } catch (Exception $ex) {
            throw new Exception($ex->getMessage(), SpjError::SELL001); 
        }
    }
    
    /** @Author: ANH DUNG Sep 15, 2018
     *  @Todo: only user map post $q, dùng ở 2 chỗ AppLogin - AppSignup
     **/
    public function mapDataPost($q) {
        $this->platform             = isset($q->platform) ? $q->platform : '0';
        $this->app_type             = isset($q->app_type) ? $q->app_type : '0';
        $this->device_name          = isset($q->device_name) ? $q->device_name : '';
        $this->device_imei          = isset($q->device_imei) ? $q->device_imei : '0';
        $this->device_os_version    = isset($q->device_os_version) ? $q->device_os_version : '';
        $this->version_code         = isset($q->version_code) ? $q->version_code : '';
        $this->gcm_device_token     = isset($q->gcm_device_token) ? $q->gcm_device_token : '';
        $this->apns_device_token    = isset($q->apns_device_token) ? $q->apns_device_token : '';
    }
    
    
    /** @Author: ANH DUNG Sep 15, 2018
     *  @Todo: handle check lock device
     **/
    public function checkDeviceLock() {
        if($this->app_type == UsersTokens::LOGIN_WINDOW){
            return ;
        }
        $criteria = new CDbCriteria();
        $this->getConditionDeviceLock($criteria);
        $mAppLoginLock = AppLoginLock::model()->find($criteria);
        if(empty($mAppLoginLock) && !$this->onlyCheckNotUpdate){
            $this->makeNewDeviceLock();
        }elseif(!empty($mAppLoginLock)){
            $this->doCheckDeviceLock($mAppLoginLock);
        }
    }
    
    /** @Author: ANH DUNG Sep 19, 2018
     *  @Todo: get same condition check device lock
     **/
    public function getConditionDeviceLock(&$criteria) {
        $criteria->compare('t.platform', $this->platform);
        $criteria->compare('t.app_type', $this->app_type);
        $criteria->compare('t.device_imei', $this->device_imei);
    }
    /** @Author: ANH DUNG Sep 15, 2018
     *  @Todo: make new record lock
     **/
    public function makeNewDeviceLock() {
        if(empty($this->username)){
            return ;
        }
        $mAppLoginLock = new AppLoginLock();
        $mAppLoginLock->username_open       = $this->username;
        $mAppLoginLock->platform            = $this->platform;
        $mAppLoginLock->app_type            = $this->app_type;
        $mAppLoginLock->device_name         = $this->device_name;
        $mAppLoginLock->device_imei         = $this->device_imei;
        $mAppLoginLock->device_os_version   = $this->device_os_version;
        $mAppLoginLock->version_code        = $this->version_code;
        $mAppLoginLock->last_update         = date('Y-m-d H:i:s');
        $mAppLoginLock->last_update_bigint  = strtotime($mAppLoginLock->last_update);
        $mAppLoginLock->save();
    }
    
    /** @Author: ANH DUNG Sep 15, 2018
     *  @Todo: make new record lock
     **/
    public function doCheckDeviceLock($mAppLoginLock) {
        if(!GasCheck::isServerLive() || in_array($this->device_imei, $this->getDeviceImeiTest())){
            return ;
        }
        if(empty($mAppLoginLock->username_open)){//Dec2018 xóa username_open = NULL - ko rõ tại sao lại lưu đc null
            $mAppLoginLock->delete();
            return ;
        }

        if($mAppLoginLock->isLock() && $this->username != $mAppLoginLock->username_open){
//        if($this->app_type == UsersTokens::APP_TYPE_GAS24H && $mAppLoginLock->isLock() && $this->username != $mAppLoginLock->username_open){
//            throw new Exception("Thiết bị này đã đăng nhập nhiều tài khoản, vi phạm chính sách của Gas24h. Bạn chỉ có thể đăng nhập bằng tài khoản: {$mAppLoginLock->username_open}. Chi tiết liên hệ 1900 1565");
            $msg = "Mỗi điện thoại chỉ được đăng nhập 1 tài khoản. Thiết bị này bạn chỉ có thể đăng nhập bằng tài khoản: {$mAppLoginLock->username_open}";
            if($this->app_type == UsersTokens::APP_TYPE_GAS24H){
                $msg = "Mỗi điện thoại chỉ được đăng nhập 1 số điện thoại. Thiết bị này bạn chỉ có thể đăng nhập bằng số điện thoại: {$mAppLoginLock->username_open}";
            }
            throw new Exception($msg);
        }
        if($this->onlyCheckNotUpdate){
            return ;
        }
        $mAppLoginLock->total_user_id       = $this->countUserLoginOnDevice();
        $mAppLoginLock->username_open       = $this->username;
        $mAppLoginLock->last_update         = date('Y-m-d H:i:s');
        $mAppLoginLock->last_update_bigint  = strtotime($mAppLoginLock->last_update);
        $mAppLoginLock->update();
    }
    
    /** @Author: ANH DUNG Sep 15, 2018
     *  @Todo: count DISTINCT user login on device_imei
     *  @Param: SELECT COUNT(DISTINCT user_id) AS user_id FROM `gas_app_login` WHERE device_imei='354603070127314' AND app_type=0 AND platform=1
     **/
    public function countUserLoginOnDevice() {
        $criteria = new CDbCriteria();
        $criteria->select = 'COUNT(DISTINCT user_id) AS user_id';
        $this->getConditionDeviceLock($criteria);
        $model = AppLogin::model()->find($criteria);
        return $model->user_id;
    }
    
    /** @Author: DuongNV  **/
    public function getArrayAppType(){
        return [
            UsersTokens::APP_TYPE_GAS_SERVICE => 'App Gas Service',
            UsersTokens::APP_TYPE_GAS24H      => 'App Gas 24h',
        ];
    }
    
    public function getArrayPlatform(){
        return [
            UsersTokens::PLATFORM_ANDROID => 'Android',
            UsersTokens::PLATFORM_IOS     => 'IOS',
        ];
    }
    
    public function getNameById() {
        $mUsers = Users::model()->findByPk($this->user_id);
        return empty($mUsers) ? '' : $mUsers->first_name;
    }
    
    public function getPlatform() {
        $aPlatform = $this->getArrayPlatform();
        return empty($aPlatform[$this->platform]) ? '' : $aPlatform[$this->platform];
    }
    
    public function getAppType() {
        $aAppType = $this->getArrayAppType();
        return empty($aAppType[$this->app_type]) ? '' : $aAppType[$this->app_type];
    }
    
    public function getDeviceToken() {
        $res = $this->gcm_device_token;
        if($this->platform == UsersTokens::PLATFORM_IOS){
            $res = $this->apns_device_token;
        }
        return $res;
    }
    
    /** @Author: DungNT Aug 16, 2019
     *  @Todo: check user can login multi device in same time
     *  @Param: $user_id
     *  @return: true if allow, false is not allow
     **/
    public function canLoginMultiDevice($user_id) {
        return in_array($user_id, $this->getArrUserAllowLoginSameTime());
    }
    
}