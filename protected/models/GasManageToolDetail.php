<?php

/**
 * This is the model class for table "{{_gas_manage_tool_detail}}".
 *
 * The followings are the available columns in table '{{_gas_manage_tool_detail}}':
 * @property string $id
 * @property string $manage_tool_id
 * @property string $materials_id
 * @property string $qty
 * @property string $date_allocation
 * @property string $price
 * @property string $month_pay
 * 
 * The followings are the available model relations:
 * @property GasMaterials             $rMaterial          Material models
 */
class GasManageToolDetail extends CActiveRecord
{
    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return GasManageToolDetail the static model class
     */
    public static function model($className=__CLASS__)
    {
            return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
            return '{{_gas_manage_tool_detail}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        return array(
            array('month_pay, price, id, manage_tool_id, materials_id, qty, date_allocation', 'safe'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        return array(
            'rMaterial' => array(self::BELONGS_TO, 'GasMaterials', 'materials_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'manage_tool_id' => 'Manage Tool',
            'materials_id' => 'Materials',
            'qty' => 'Qty',
            'date_allocation' => 'Date Allocation',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        $criteria=new CDbCriteria;
        $criteria->compare('t.id',$this->id,true);

        return new CActiveDataProvider($this, array(
                'criteria'=>$criteria,
                'pagination'=>array(
                    'pageSize'=> Yii::app()->user->getState('pageSize',Yii::app()->params['defaultPageSize']),
                ),
        ));
    }
    // Sep 05, 2014
    public static function delete_by_manage_tool_id($manage_tool_id){
        $criteria = new CDbCriteria();
        $criteria->compare('manage_tool_id', $manage_tool_id);
        self::model()->deleteAll($criteria);
        return;
        $models = self::model()->findAll($criteria); // hiện tại chưa thấy cần thết phải xóa kiểu findAll cho chõ này
        if(count($models)){
            foreach($models as $item){
                $item->delete();
            }
        }
    }
    
    public function getAmount($format = false) {
        if($format){
            return ActiveRecord::formatCurrency($this->qty * $this->price);
        }
        return $this->qty * $this->price;
    }
    
}