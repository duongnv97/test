<?php

/**
 * This is the model class for table "{{_gas_manage_tool}}".
 *
 * The followings are the available columns in table '{{_gas_manage_tool}}':
 * @property string $id                 Id of record
 * @property string $code_no            Code no
 * @property string $uid_login          User id
 * @property string $date_allocation    Date allocation
 * @property string $employee_id        Id of employee
 * @property string $created_date       Created date 
 * 
 * The followings are the available model relations:
 * @property GasDebts[]             $rGasDebts          List gas debts models
 * @property GasManageToolDetail[]  $rManageToolDetail  List gas manage tool detail models
 */
class GasManageTool extends BaseSpj
{
    public $autocomplete_name;
    public $MAX_ID;
    public $aModelDetail;
    public $date_from;
    public $date_to;
    public $ext_materials_id;
    
    // lấy số ngày cho phép user cập nhật
    public static function getDayAllowUpdate(){
        return Yii::app()->params['days_update_manage_tool'];
    }    
    
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{_gas_manage_tool}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        return array(
            array('date_allocation,employee_id', 'required', 'on'=>'create,update'),
            array('ext_materials_id,id, code_no, uid_login, date_allocation, employee_id, created_date', 'safe'),
            array('date_from,date_to', 'safe'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {                
        return array(
            'rUidLogin' => array(self::BELONGS_TO, 'Users', 'uid_login'),
            'rEmployee' => array(self::BELONGS_TO, 'Users', 'employee_id'),
            'rManageToolDetail' => array(self::HAS_MANY, 'GasManageToolDetail', 'manage_tool_id',
                'order'=>'rManageToolDetail.id ASC',
            ),
            'rMaterial' => array(self::BELONGS_TO, 'GasMaterials', 'ext_materials_id'),
            'rGasDebts' => array(
                self::HAS_MANY, 'GasDebts', 'relate_id',
                'on'    => 'type = ' . GasDebts::TYPE_GAS_MANAGE_TOOL . ' AND status !=' . DomainConst::DEFAULT_STATUS_INACTIVE,
            ),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'code_no' => 'Mã Số',
            'uid_login' => 'Người Tạo',
            'date_allocation' => 'Ngày Cấp',
            'employee_id' => 'Nhân Viên',
            'created_date' => 'Ngày Tạo',
            'date_from' => 'Từ Ngày',
            'date_to' => 'Đến Ngày',
            'ext_materials_id' => 'Vật Tư',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        $criteria=new CDbCriteria;
        $criteria->compare('t.code_no',$this->code_no);
        $criteria->compare('t.uid_login',$this->uid_login);
        $criteria->compare('t.employee_id',$this->employee_id);
        $date_from = $date_to = '';
        if(!empty($this->date_from)){
            $date_from = MyFormat::dateDmyToYmdForAllIndexSearch($this->date_from);
        }
        if(!empty($this->date_to)){
            $date_to = MyFormat::dateDmyToYmdForAllIndexSearch($this->date_to);
        }
        if(!empty($date_from) && empty($date_to))
                $criteria->addCondition("t.date_allocation>='$date_from'");
        if(empty($date_from) && !empty($date_to))
                $criteria->addCondition("t.date_allocation<='$date_to'");
        if(!empty($date_from) && !empty($date_to))
                $criteria->addBetweenCondition("t.date_allocation",$date_from,$date_to);        
        
        if(!empty($this->ext_materials_id)){
            $criteria->compare('rManageToolDetail.materials_id', trim($this->ext_materials_id));
            $criteria->with = array('rManageToolDetail');
            $criteria->together = true;
        }
            
        $criteria->order = 't.id desc';
        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
            'pagination'=>array(
                'pageSize'=> Yii::app()->user->getState('pageSize',Yii::app()->params['defaultPageSize']),
            ),
        ));
    }
    
    protected function beforeSave() {
        if($this->isNewRecord){
            $this->uid_login = Yii::app()->user->id;
            $this->code_no = MyFunctionCustom::getNextId('GasManageTool', 'MT'.date('y'), LENGTH_TICKET,'code_no');
        }
        
        if(strpos($this->date_allocation, '/')){
            $this->date_allocation =  MyFormat::dateConverDmyToYmd($this->date_allocation);
            MyFormat::isValidDate($this->date_allocation);
        }       
        return parent::beforeSave();
    }
    
    protected function beforeValidate() {
        $this->aModelDetail = [];
        if(isset($_POST['GasManageToolDetail']['materials_id']) && is_array($_POST['GasManageToolDetail']['materials_id'])){
            foreach($_POST['GasManageToolDetail']['materials_id'] as $key=>$item){
                $qty = isset($_POST['GasManageToolDetail']['qty'][$key])?$_POST['GasManageToolDetail']['qty'][$key]:'';
                if(trim($item)!='' && !empty($qty) ){
                    $mDetail = new GasManageToolDetail();
                    $mDetail->materials_id  = $item;
                    $mDetail->qty           = $qty;
                    $mDetail->price         = isset($_POST['GasManageToolDetail']['price'][$key]) ? $_POST['GasManageToolDetail']['price'][$key] : 0;
                    $mDetail->price         = MyFormat::removeComma($mDetail->price);
                    $mDetail->month_pay     = isset($_POST['GasManageToolDetail']['month_pay'][$key]) ? $_POST['GasManageToolDetail']['month_pay'][$key] : 0;;
                    $aAtt = array('materials_id','qty');
                    MyFormat::RemoveScriptBad($mDetail, $aAtt);
                    $this->aModelDetail[] = $mDetail;
                }                
            }
        }
        
        if(count($this->aModelDetail)<1){
            $this->addError('aModelDetail', 'Chưa nhập dữ liệu vật tư, số lượng');
        }
        return parent::beforeValidate();
    }
    
    protected function beforeDelete() {
        GasManageToolDetail::delete_by_manage_tool_id($this->id);
        $this->deleteRefDebt();
        return parent::beforeDelete();
    }
    
    public function deleteRefDebt() {
        $mGasDebts = new GasDebts();
        $mGasDebts->relate_id    = $this->id;
        $mGasDebts->type         = GasDebts::TYPE_GAS_MANAGE_TOOL;
        $mGasDebts->deleteByRelateId();
    }
    
    /**
     * Get amount value as formated
     * @return String Value of amount as formated
     */
    public function getAmount() {
        return CmsFormatter::formatCurrency($this->getAmountValue());
    }
    
    /**
     * Create gas debt relate
     */
    public function createGasDebts() {
        $this->deleteRefDebt();
        foreach($this->aModelDetail as $key=>$mDetail){
            $mGasDebts = new GasDebts();
            $mGasDebts->createFromGasManageTool($this, $mDetail, $mDetail->month_pay);
        }
    }
    
    /**
     * @Author: ANH DUNG Sep 05, 2014
     * @Todo: save detail
     * @Param: $this model GasManageTool
     */
    public function saveDetail(){
        try {
            set_time_limit(7200);
            $aRowInsert=array();
            GasManageToolDetail::delete_by_manage_tool_id($this->id);
            
            foreach($this->aModelDetail as $key=>$mDetail){
                if(!empty($mDetail->materials_id)){
                    $aRowInsert[]="('$this->id',
                        '$this->employee_id',
                        '$mDetail->materials_id',
                        '$mDetail->qty',
                        '$this->date_allocation',
                        '$mDetail->price',
                        '$mDetail->month_pay'
                        )";
                }
            }
            $tableName = GasManageToolDetail::model()->tableName();
            $sql = "insert into $tableName (manage_tool_id,
                        employee_id,
                        materials_id,
                        qty,
                        date_allocation,
                        price,
                        month_pay
                        ) values ".implode(',', $aRowInsert);
            if(count($aRowInsert)>0)
                Yii::app()->db->createCommand($sql)->execute();        
            $this->createGasDebts();
            
        } catch (Exception $exc) {
            throw new Exception("Có Lỗi : ". $exc->getMessage());
        }        
    }
    
    public function canUpdate() {
        $cRole = MyFormat::getCurrentRoleId();
        $cUid = MyFormat::getCurrentUid();
        if($cRole == ROLE_ADMIN){
            return true;
        }
        if($cUid != $this->uid_login){
            return false;
        }
        $dayAllow = date('Y-m-d');
        $dayAllow = MyFormat::modifyDays($dayAllow, 1, '-');
        return MyFormat::compareTwoDate($this->created_date, $dayAllow);
    }
    
}