<?php

/**
 * This is the model class for table "{{_gas_daily_cash}}".
 *
 * The followings are the available columns in table '{{_gas_daily_cash}}':
 * @property string $id
 * @property integer $customer_id
 * @property string $date_collection
 * @property string $total
 * @property integer $user_id_create
 * @property string $created_date
 * $styleArray = array(
	'fill' => array(
		'type' => PHPExcel_Style_Fill::FILL_GRADIENT_LINEAR,
		'color' => array(
			'argb' => '9acd32',
		),
		'endcolor' => array(
			'argb' => '9acd32',
		),
	),
);  
 * $objPHPExcel->getActiveSheet()->getStyle(MyFunctionCustom::columnName($index).($rowCurrent).':'.MyFunctionCustom::columnName($index+9).($rowCurrent))->applyFromArray($styleArray);	
 */
class GasDailyCash extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return GasDailyCash the static model class
	 */
    public $autocomplete_name;
    public $file_excel;
	public $sell_date_from;
	public $sell_date_to;
    
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{_gas_daily_cash}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		return array(
                    array('customer_id, date_collection', 'required', 'on'=>'create_DailyCash,update_DailyCash'),                    
			array('customer_id, user_id_create', 'numerical', 'integerOnly'=>true),
			array('total', 'length', 'max'=>16),
			array('date_collection, created_date', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
array('id, customer_id, date_collection, total, user_id_create, created_date,sell_date_from,sell_date_to,sale_id,agent_id', 'safe'),
    array('file_excel', 'file', 'on'=>'import_dailycash',
        'allowEmpty'=>false,
        'types'=> 'xls,xlsx',
        'wrongType'=>'Chỉ cho phép tải file xls,xlsx',
        //'maxSize' => ActiveRecord::getMaxFileSize(), // 5MB
        //'tooLarge' => 'The file was larger than '.(ActiveRecord::getMaxFileSize()/1024).' KB. Please upload a smaller file.',
    ),                     
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		return array(
                    'customer' => array(self::BELONGS_TO, 'Users', 'customer_id'),
                    'agent' => array(self::BELONGS_TO, 'Users', 'agent_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'customer_id' => 'Khách Hàng',
			'date_collection' => 'Ngày Thu Tiền',
			'total' => 'Số Tiền Thu',
			'user_id_create' => 'Người Tạo',
			'created_date' => 'Ngày Tạo',
			'sell_date_from' => 'Từ Ngày',
			'sell_date_to' => 'Đến Ngày',
			'agent_id' => 'Đại Lý',
			'sale_id' => 'Nhân Viên Sale',
                    
		);
	}

	public function search()
	{
		$criteria=new CDbCriteria;
		$criteria->compare('t.id',$this->id,true);
		$criteria->compare('t.customer_id',$this->customer_id);
                if(!empty($this->date_collection)){
                    $this->date_collection = MyFormat::dateConverDmyToYmd($this->date_collection);
                    $criteria->compare('t.date_collection',$this->date_collection,true);
                }
		$criteria->compare('t.total',$this->total,true);
		$criteria->compare('t.user_id_create',$this->user_id_create);
		$criteria->compare('t.created_date',$this->created_date,true);
                $criteria->order = "t.date_collection asc";
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
            'pagination'=>array(
                'pageSize'=> Yii::app()->user->getState('pageSize',Yii::app()->params['defaultPageSize']),
            ),
		));
	}

	public function defaultScope()
	{
		return array();
	}
        
	protected function beforeSave()
	{
            $this->date_collection =  MyFormat::dateConverDmyToYmd($this->date_collection);        
			$this->sale_id = $this->customer?$this->customer->sale_id:0;
            return true;
        }        
        
        public static function getDataDailyCash($date_from, $date_to){
                $date_from = MyFormat::dateConverDmyToYmd($date_from);
                $date_to = MyFormat::dateConverDmyToYmd($date_to);			

                $aSale = Users::getArrObjectUserByRole(ROLE_SALE);
                $aExport = array();
                foreach($aSale as $key=>$item){
                        $criteria=new CDbCriteria;
                        $criteria->addBetweenCondition('date_collection',$date_from,$date_to); 
                        $criteria->compare('t.sale_id',$key);		
                        $criteria->select = 'date_collection, sum(total) as total';
                        $criteria->group = 'date_collection';

                        $criteria->order = "t.date_collection asc";
                        $res = GasDailyCash::model()->findAll($criteria);
                        if(count($res)>0)
                        foreach($res as $obj){
                                $aExport[$obj->date_collection][$key] = $obj->total;					
                        }
                }

                return array('aExport'=>$aExport,'aSale'=>$aSale,'date_from'=>$date_from,'date_to'=>$date_to);
        }
        
	
	public function searchExport(){
		/*
		$begin = new DateTime( '2010-05-25' );
		$end = new DateTime( '2010-06-10' );

		$interval = DateInterval::createFromDateString('1 day');
		$period = new DatePeriod($begin, $interval, $end);

		foreach ( $period as $key=>$dt )
		  echo ' --- '.$key.' --- '.$dt->format( "Y-m-d" );
		  die;
		  */
		if(!empty($this->sell_date_from) && !empty($this->sell_date_to) ){
                    $data = GasDailyCash::getDataDailyCash($this->sell_date_from, $this->sell_date_to);
                    // $return data is: array('aExport'=>$aExport,'aSale'=>$aSale,'date_from'=>$date_from,'date_to'=>$date_to);
                    GasDailyCash::exportExcel($data['aExport'], $data['aSale'], $data['date_from'], $data['date_to']);
		}	                
		return ;
	}	
	
	public static function exportExcel($aExport, $aSale, $sell_date_from, $sell_date_to){
		Yii::import('application.extensions.vendors.PHPExcel',true);
		$objPHPExcel = new PHPExcel();
		 // Set properties
		 $objPHPExcel->getProperties()->setCreator("NguyenDung")
                            ->setLastModifiedBy("NguyenDung")
                            ->setTitle('Daily Cash Report')
                            ->setSubject("Office 2007 XLSX Document")
                            ->setDescription("Daily Cash Report All Sale")
                            ->setKeywords("office 2007 openxml php")
                            ->setCategory("GAS");
		 $objPHPExcel->getActiveSheet()->setTitle('Daily Cash Report');
		 //             
		 $objPHPExcel->setActiveSheetIndex(0);
		 $objPHPExcel->getActiveSheet()->setCellValue("A1", 'Ngày thu');
		 $index = 2;
		 foreach ($aSale as $sale_id=>$mSale){
			$objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index)."1", $mSale->first_name);
			$index++;
		 }			 
		 $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index)."1", 'Tổng cộng');
		$objPHPExcel->getActiveSheet()
				->getStyle('A1:'.MyFunctionCustom::columnName($index+count($aSale))."1")
				->getFont()
				//->setSize(14)
				->setBold(true);		 
		 
		$begin = new DateTime( $sell_date_from );
		$end = new DateTime( $sell_date_to );
		$end->modify('+1 day'); // cần cộng thêm 1 day cho đúng với ngày chọn ở giao diện
        
		$interval = DateInterval::createFromDateString('1 day');
		$period = new DatePeriod($begin, $interval, $end);

		$index = 1;
		$rowNum = 1;
		$sumRow = 0;
		$sumAll=0;
		$sumCol = array();
	 	foreach ( $period as $key=>$dt ){
			$ymd = $dt->format( "Y-m-d" );
			$dmyShow = $dt->format( Yii::app()->params['dateFormat'] );
			$objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index).($rowNum+$key+1), $dmyShow);
			$indexSale = 1;
			 foreach ($aSale as $sale_id=>$mSale){
				if(isset($aExport[$ymd][$sale_id])){
					$objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index+$indexSale).($rowNum+$key+1), $aExport[$ymd][$sale_id]);
					$sumRow += $aExport[$ymd][$sale_id];
					$sumAll += $aExport[$ymd][$sale_id];
					if(isset($sumCol[$sale_id]))
						$sumCol[$sale_id] += $aExport[$ymd][$sale_id];
					else
						$sumCol[$sale_id] = $aExport[$ymd][$sale_id];
				}
					
				$indexSale++;
			 } // end foreach $aSale	
			 // for sum row
			 if($sumRow>0)
				$objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index+count($aSale)+1).($rowNum+$key+1), $sumRow);
			 $sumRow=0;
			 // for sum row
			$objPHPExcel->getActiveSheet()
					->getStyle(MyFunctionCustom::columnName($index+1).($rowNum+$key+1).':'.MyFunctionCustom::columnName($index+count($aSale)+1).($rowNum+$key+1) )->getNumberFormat()
					->setFormatCode('#,##0');
				
		} // end foreach list days
		$objPHPExcel->getActiveSheet()->setCellValue("A".($rowNum+$key+2), 'Tổng cộng');  
		//$index = 1;
		$indexSale = 1;
		foreach ($aSale as $sale_id=>$mSale){
                    if(isset($sumCol[$sale_id]))
			$objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index+$indexSale).($rowNum+$key+2), $sumCol[$sale_id]);
                    $indexSale++;
		 }
		 $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index+count($aSale)+1).($rowNum+$key+2), $sumAll);
		 
		$objPHPExcel->getActiveSheet()
				->getStyle(MyFunctionCustom::columnName($index+1).($rowNum+$key+2).':'.MyFunctionCustom::columnName($index+count($aSale)+1).($rowNum+$key+2) )->getNumberFormat()
				->setFormatCode('#,##0');	
		$objPHPExcel->getActiveSheet()
				->getStyle(MyFunctionCustom::columnName($index).($rowNum+$key+2).':'.MyFunctionCustom::columnName($index+count($aSale)+1).($rowNum+$key+2) )
				->getFont()
				//->setSize(14)
				->setBold(true);	
		$objPHPExcel->getActiveSheet()
				->getStyle(MyFunctionCustom::columnName($index+count($aSale)+1).'1:'.MyFunctionCustom::columnName($index+count($aSale)+1).($rowNum+$key+2) )
				->getFont()
				//->setSize(14)
				->setBold(true);				
		$objPHPExcel->getActiveSheet()->getStyle(MyFunctionCustom::columnName($index).'1:'.MyFunctionCustom::columnName($index+count($aSale)+1).($rowNum+$key+2) )
                        ->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);		 
		 //save file 
		 $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
		 //$objWriter->save('MyExcel.xslx');

		 for($level=ob_get_level();$level>0;--$level)
		 {
			 @ob_end_clean();
		 }
		 header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
		 header('Pragma: public');
		 header('Content-type: '.'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		 header('Content-Disposition: attachment; filename="'.'DailyCash'.'.'.'xlsx'.'"');
		 header('Cache-Control: max-age=0');				
		 $objWriter->save('php://output');			
		 Yii::app()->end();		  

	}	
        
        
        public function exportDailyPlan(){
            if(!empty($this->sell_date_from) && !empty($this->sell_date_to) ){
                $data = GasDailyCash::getDataDailyCash($this->sell_date_from, $this->sell_date_to);
                $dataSell = GasDailySelling::getDataDailySelling($this->sell_date_from, $this->sell_date_to);
                // $return data is: array('aExport'=>$aExport,'aSale'=>$aSale,'date_from'=>$date_from,'date_to'=>$date_to);
                GasDailyCash::exportExcelDailyPlan($data['aExport'], $dataSell['aExport'], $data['aSale'], $data['date_from'], $data['date_to']);
            }
            return ;
        }
        
        public static function exportExcelDailyPlan($aExportCash, $aExportSelling, $aSale, $sell_date_from, $sell_date_to){
		Yii::import('application.extensions.vendors.PHPExcel',true);
		$objPHPExcel = new PHPExcel();
		 // Set properties
		 $objPHPExcel->getProperties()->setCreator("NguyenDung")
                            ->setLastModifiedBy("NguyenDung")
                            ->setTitle('Daily Plan Report')
                            ->setSubject("Office 2007 XLSX Document")
                            ->setDescription("Daily Plan Report All Sale")
                            ->setKeywords("office 2007 openxml php")
                            ->setCategory("GAS");
		 $objPHPExcel->getActiveSheet()->setTitle('Daily Plan Report');
		 //             
		 $objPHPExcel->setActiveSheetIndex(0);
		 $objPHPExcel->getActiveSheet()->setCellValue("A1", 'Ngày');
                 $objPHPExcel->getActiveSheet()->mergeCells('A1:A2');
                 $objPHPExcel->getActiveSheet()->getStyle('A1:A2')
                        ->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                 
		 $index = 1;
                $indexSale = 0;
                $run = 0;
                foreach ($aSale as $sale_id=>$mSale){
                    for($i=1;$i<5;$i++){
                            if($i==1)
                                $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index+$i+($indexSale*4))."1", $mSale->first_name);
                           $textHead = 'Kế hoạch';
                           if($i==2)
                               $textHead = 'Lũy tiến';
                           elseif ($i==3) 
                                $textHead = 'Thực tế';
                           elseif ($i==4) 
                                $textHead = 'Lũy tiến';
                           $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index+$i+($indexSale*4))."2", $textHead);
                    } 
                    $objPHPExcel->getActiveSheet()->mergeCells(MyFunctionCustom::columnName($index+1+($indexSale*4)).'1:'.MyFunctionCustom::columnName($index+4+($indexSale*4)).'1');
                    $objPHPExcel->getActiveSheet()->getStyle(MyFunctionCustom::columnName($index+1+($indexSale*4)).'1:'.MyFunctionCustom::columnName($index+4+($indexSale*4)).'1')
                            ->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                    
                    $indexSale++;
                }
                
		 $objPHPExcel->getActiveSheet()
				->getStyle(MyFunctionCustom::columnName($index).'1:'.MyFunctionCustom::columnName($index+4+($indexSale*4)).'2' )
				->getFont()
				//->setSize(14)
				->setBold(true); 
		 
		$begin = new DateTime( $sell_date_from );
		$end = new DateTime( $sell_date_to );
		$end->modify('+1 day'); // cần cộng thêm 1 day cho đúng với ngày chọn ở giao diện
        
		$interval = DateInterval::createFromDateString('1 day');
		$period = new DatePeriod($begin, $interval, $end);

		$index = 1;
		$rowNum = 2;
		$sumRow = 0;
		$sumAll=0;
		$sumCol = array();
                $sumLuyTuyenCash=array();
                $sumLuyTuyenSelling=array();
                
                $sumSelling=array();
                $dateTmp = explode('-', $sell_date_from);
                $aSalePercent = GasTargetMonthly::getArrayByMonthYear($dateTmp[1],$dateTmp[0]);
	 	foreach ( $period as $key=>$dt ){
			$ymd = $dt->format( "Y-m-d" );
			$dmyShow = $dt->format( Yii::app()->params['dateFormat'] );
			$objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index).($rowNum+$key+1), $dmyShow);
			$indexSale = 0;
			 foreach ($aSale as $sale_id=>$mSale){
                             
                            for($i=1;$i<5;$i++){
                                if(isset($aExportCash[$ymd][$sale_id]) || isset($aExportSelling[$ymd][$sale_id])){
                                   if($i==3 && isset($aExportCash[$ymd][$sale_id])){
                                       $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index+$i+($indexSale*4)).($rowNum+$key+1), $aExportCash[$ymd][$sale_id]);
					if(isset($sumLuyTuyenCash[$sale_id]))
						$sumLuyTuyenCash[$sale_id] += $aExportCash[$ymd][$sale_id];
					else
						$sumLuyTuyenCash[$sale_id] = $aExportCash[$ymd][$sale_id];
                                   }
                                   elseif ($i==4){
                                       if(isset($sumLuyTuyenCash[$sale_id]))
                                           $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index+$i+($indexSale*4)).($rowNum+$key+1), $sumLuyTuyenCash[$sale_id]);
                                       else 
                                           $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index+$i+($indexSale*4)).($rowNum+$key+1), '');
                                   }
                                        
                                   elseif ($i==1 && isset($aExportSelling[$ymd][$sale_id])) {
                                        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index+$i+($indexSale*4)).($rowNum+$key+1), $aExportSelling[$ymd][$sale_id]);
                                        $percent = 1;
                                        if(isset($aSalePercent[$sale_id]) && $aSalePercent[$sale_id]>0)
                                            $percent = $aSalePercent[$sale_id]/100;
					if(isset($sumLuyTuyenSelling[$sale_id]))
						$sumLuyTuyenSelling[$sale_id] += round(($aExportSelling[$ymd][$sale_id] * $percent));
					else
						$sumLuyTuyenSelling[$sale_id] = round(($aExportSelling[$ymd][$sale_id] * $percent));
					if(isset($sumSelling[$sale_id]))
						$sumSelling[$sale_id] += round(($aExportSelling[$ymd][$sale_id]));
					else
						$sumSelling[$sale_id] = round(($aExportSelling[$ymd][$sale_id]));
                                   }
                                       
                                   elseif ($i==2) {
                                       // phần kế hoạch của nv nhân với % dc thiết đặt cho nv đó
                                       if(isset($sumLuyTuyenSelling[$sale_id]))
                                           $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index+$i+($indexSale*4)).($rowNum+$key+1), $sumLuyTuyenSelling[$sale_id]);
                                       else 
                                           $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index+$i+($indexSale*4)).($rowNum+$key+1), '');
                                   }
                                        
				}// end for if(isset($aExportCash[$ymd][$sale_id]) 
                            } // end for for($i=1;$i<5;$i++){
                            
                            $indexSale++;
			 }	
			 // for sum row
			 // for sum row
			$objPHPExcel->getActiveSheet()
                                ->getStyle(MyFunctionCustom::columnName($index+1).($rowNum+$key+1).':'.MyFunctionCustom::columnName($index+count($aSale)*4+1).($rowNum+$key+1) )
                                ->getNumberFormat()->setFormatCode('#,##0');
                        
				
				
		} // end foreach list days
		$objPHPExcel->getActiveSheet()->setCellValue("A".($rowNum+$key+2), 'Tổng cộng');  
		
                // for sum col
		$indexSale = 0;
		foreach ($aSale as $sale_id=>$mSale){
                    for($i=1;$i<5;$i++){
                        if($i==3 && isset($sumLuyTuyenCash[$sale_id])){
$objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index+$i+($indexSale*4)).($rowNum+$key+2), $sumLuyTuyenCash[$sale_id]);                            
                        }elseif ($i==1 && isset($sumSelling[$sale_id])){
$objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index+$i+($indexSale*4)).($rowNum+$key+2), $sumSelling[$sale_id]);                                                        
                        }elseif ($i==2 && isset($sumLuyTuyenSelling[$sale_id])) {
$objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index+$i+($indexSale*4)).($rowNum+$key+2), $sumLuyTuyenSelling[$sale_id]);                                                        
//                        }elseif ($i==4) {
                            
                        }
                    }
                    if(isset($sumCol[$sale_id]))
			$objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index+$indexSale).($rowNum+$key+2), $sumCol[$sale_id]);
                    $indexSale++;
		 }
		 $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index+count($aSale)+1).($rowNum+$key+2), $sumAll);
		 
		$objPHPExcel->getActiveSheet()
				->getStyle(MyFunctionCustom::columnName($index+1).($rowNum+$key+2).':'.MyFunctionCustom::columnName($index+count($aSale)*4).($rowNum+$key+2) )->getNumberFormat()
				->setFormatCode('#,##0');
		$objPHPExcel->getActiveSheet()
				->getStyle(MyFunctionCustom::columnName($index).($rowNum+$key+2).':'.MyFunctionCustom::columnName($index+count($aSale)*4).($rowNum+$key+2) )
				->getFont()
				//->setSize(14)
				->setBold(true);
				
		$objPHPExcel->getActiveSheet()->getStyle(MyFunctionCustom::columnName($index).'1:'.MyFunctionCustom::columnName($index+count($aSale)*4).($rowNum+$key+2) )
                        ->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);			 
		 //save file 
		 $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
		 //$objWriter->save('MyExcel.xslx');

		 for($level=ob_get_level();$level>0;--$level)
		 {
			 @ob_end_clean();
		 }
		 header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
		 header('Pragma: public');
		 header('Content-type: '.'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		 header('Content-Disposition: attachment; filename="'.'DailyPlan'.'.'.'xlsx'.'"');
		 header('Cache-Control: max-age=0');				
		 $objWriter->save('php://output');			
		 Yii::app()->end();		  

	}	        
        
        
        public static function exportTarget($date_from){
            if(!empty($date_from) ){
                $dataCash = GasDailyCash::getDataTarget($date_from,'GasDailyCash','date_collection');
                $dataSelling = GasDailyCash::getDataTarget($date_from,'GasDailySelling','date_of_payment');
                $dataTarget = GasDailyCash::getDataTarget($date_from,'GasDailySelling','date_of_payment',1);
                GasDailyCash::exportExcelTarget($dataCash, $dataSelling,$dataTarget);
            }            
        }
        
        public static function getDataTarget($date_from, $name_model, $name_field, $flag_target=0){
            $date_from = MyFormat::dateConverDmyToYmd($date_from);
            // cái tên hơi ngược 1 chút, $date_to là ngày bắt đầu, $date_from là ngày kết thúc
            $aDateFrom = explode('-', $date_from);
            $date_to = $aDateFrom[0].'-'.$aDateFrom[1].'-01';
            $aSale = Users::getArrObjectUserByRole(ROLE_SALE);
            $aSaleId = Users::getArrIdUserByRole(ROLE_SALE);
            $criteria=new CDbCriteria;
//            $criteria->compare('t.'.$name_field,$date_from);		
//            $criteria->addInCondition('t.sale_id',$aSaleId);	
            if($flag_target){
                $d = new DateTime( $date_from ); 
                $date_end_month = $d->format( 'Y-m-t' );
                $criteria->addBetweenCondition($name_field,$date_to,$date_end_month); 
            }else
                $criteria->addBetweenCondition($name_field,$date_to,$date_from); 
//            $criteria->select = 'sale_id, '.$name_field.', sum(total) as total';
            $criteria->select = 'sale_id, sum(total) as total';
            $criteria->group = 'sale_id';
            $criteria->order = "t.sale_id asc";
            $model_ = call_user_func(array($name_model, 'model'));
            $res = $model_->findAll($criteria);
            $aExport=array();
            if(count($res)>0)
            foreach($res as $obj){
                    $aExport[$obj->sale_id] = $obj->total;					
            }
            return array('aExport'=>$aExport,'aSale'=>$aSale,'date_from'=>$date_from);            
        }
        
        
        public static function exportExcelTarget($dataCash, $dataSelling, $dataTarget){                  
                $aSale = $dataCash['aSale'];
                $date_from = $dataCash['date_from'];
		Yii::import('application.extensions.vendors.PHPExcel',true);
		$objPHPExcel = new PHPExcel();
		 // Set properties
		 $objPHPExcel->getProperties()->setCreator("NguyenDung")
                            ->setLastModifiedBy("NguyenDung")
                            ->setTitle('Daily Cash Report')
                            ->setSubject("Office 2007 XLSX Document")
                            ->setDescription("Daily Target Report")
                            ->setKeywords("office 2007 openxml php")
                            ->setCategory("GAS");
		 $objPHPExcel->getActiveSheet()->setTitle('Daily Target Report');
		 $objPHPExcel->setActiveSheetIndex(0);
		 $objPHPExcel->getActiveSheet()->setCellValue("A1", 'Tình hình thu nợ khách hàng bình bò');
		 $objPHPExcel->getActiveSheet()->setCellValue("A2", 'Ngày');
                 $date_from = explode('-', $date_from);
		 $objPHPExcel->getActiveSheet()->setCellValue("B2", $date_from[2].'/'.$date_from[1].'/'.$date_from[0]);
                 $objPHPExcel->getActiveSheet()->setCellValue("A3", 'Sale');
                 $objPHPExcel->getActiveSheet()->setCellValue("B3", 'Target');
                 $objPHPExcel->getActiveSheet()->setCellValue("C3", 'Kế hoạch');
                 $objPHPExcel->getActiveSheet()->setCellValue("D3", 'Thực tế');
                 $objPHPExcel->getActiveSheet()->setCellValue("E3", 'Tỷ lệ (%)');
                $objPHPExcel->getActiveSheet()->getStyle('A3:E3')
                        ->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
                 
                $index = 1;
                 $rowNum = 3;
                 $indexSale = 1;       
				 $sumTarget = 0;     
				 $sumSelling = 0;     
				 $sumCash = 0;     
                 $selling = $dataSelling['aExport'];
                 $cash = $dataCash['aExport'];
                 $target = $dataTarget['aExport'];                
                 $aSalePercent = GasTargetMonthly::getArrayByMonthYear($date_from[1],$date_from[0]);
                 
                foreach ($aSale as $sale_id=>$mSale){
                    $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index).($indexSale+$rowNum), $mSale->first_name);
                    for($i=1;$i<5;$i++){
                           if($i==1 &&isset($target[$sale_id])){
								$sumTarget += $target[$sale_id];
                               $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($i+1).($indexSale+$rowNum), $target[$sale_id]);
                           }
                        
                           if($i==2 && isset($selling[$sale_id])){
                                $percent = 1;
                                if(isset($aSalePercent[$sale_id]) && $aSalePercent[$sale_id]>0)
                                    $percent = $aSalePercent[$sale_id]/100;
                               $target_percent = round($selling[$sale_id]*$percent);
                               $sumSelling += $target_percent;
                               $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($i+1).($indexSale+$rowNum), $target_percent);
//                               $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($i).($indexSale+$rowNum),123);
                           }
                           if($i==3 &&isset($cash[$sale_id])){
								$sumCash += $cash[$sale_id];
                               $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($i+1).($indexSale+$rowNum), $cash[$sale_id]);
                           }
                           
                           if($i==4){ // tỉ lệ %
                               if(!isset($cash[$sale_id]))
                                   $cash[$sale_id] = 0;
                               $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($i+1).($indexSale+$rowNum), round(($cash[$sale_id]/$target_percent)*100,2));
                           }              
                        $objPHPExcel->getActiveSheet()->getStyle(MyFunctionCustom::columnName($i+1).($indexSale+$rowNum))
                                ->getBorders()->getRight()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
                           
                    }			 
                    
                    $objPHPExcel->getActiveSheet()
				->getStyle(MyFunctionCustom::columnName($index+1).($rowNum+$indexSale).':'.MyFunctionCustom::columnName($index+3).($rowNum+$indexSale) )->getNumberFormat()
				->setFormatCode('#,##0');	                    
                    $objPHPExcel->getActiveSheet()->getStyle(MyFunctionCustom::columnName($index).($rowNum+$indexSale).':'.MyFunctionCustom::columnName($index+4).($rowNum+$indexSale) )
                            ->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
                    
                    $indexSale++;                    
                 }
                 
		 //$objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index)."1", 'Công ty');
		 $objPHPExcel->getActiveSheet()->setCellValue("A".($rowNum+$indexSale), 'Công ty'); 
		 for($i=1;$i<5;$i++){
			if($i==1)
				$objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($i+1).($rowNum+$indexSale), $sumTarget);
			if($i==2)
				$objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($i+1).($rowNum+$indexSale), $sumSelling);	
			if($i==3)
				$objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($i+1).($rowNum+$indexSale), $sumCash);	
			if($i==4)
				$objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($i+1).($rowNum+$indexSale), round(($sumCash/$sumSelling)*100,2));				
		 }
		$objPHPExcel->getActiveSheet()
			->getStyle(MyFunctionCustom::columnName(2).($rowNum+$indexSale).':'.MyFunctionCustom::columnName($i-1).($rowNum+$indexSale) )->getNumberFormat()
			->setFormatCode('#,##0');	
		$objPHPExcel->getActiveSheet()
			->getStyle(MyFunctionCustom::columnName(1).($rowNum+$indexSale).':'.MyFunctionCustom::columnName($i).($rowNum+$indexSale) )
			->getFont()
			//->setSize(14)
			->setBold(true);
                $objPHPExcel->getActiveSheet()->getStyle(MyFunctionCustom::columnName(1).($rowNum+$indexSale).':'.MyFunctionCustom::columnName($i).($rowNum+$indexSale) )
                        ->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
                
		 //save file 
		 $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
		 //$objWriter->save('MyExcel.xslx');

		 for($level=ob_get_level();$level>0;--$level)
		 {
			 @ob_end_clean();
		 }
		 header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
		 header('Pragma: public');
		 header('Content-type: '.'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		 header('Content-Disposition: attachment; filename="'.'DailyTarget'.'.'.'xlsx'.'"');
		 header('Cache-Control: max-age=0');				
		 $objWriter->save('php://output');			
		 Yii::app()->end();		  

	}	        
        	
	public static function exportAccountReceivables($date_from, $date_to, $sale_id){
		if(!empty($date_from) || !empty($date_to) ){
			$beginningSelling = array();// Tồn đầu kỳ khi chọn 1 khoảng ngày thống kê, nếu chọn 1 ngày thì beginning sẽ lấy trong bảng user
			$beginningCash = array();
			$beginningAll = array();// list những KH có Tồn  đầu kỳ > 0;
			// 1. get list customer id=>model customer
			$aCustomer = Users::getArrObjectUserByRole(ROLE_CUSTOMER);			
			$date_to = MyFormat::dateConverDmyToYmd($date_to);
			if(!empty($date_from)){
				$date_from = MyFormat::dateConverDmyToYmd($date_from);
				$beginningSelling = GasDailyCash::getDataAccountReceivables('', $date_from, $sale_id,'GasDailySelling','date_sell',1);
				$beginningCash = GasDailyCash::getDataAccountReceivables('', $date_from, $sale_id,'GasDailyCash','date_collection',1);
			}else{
				$beginningSelling = GasDailyCash::getDataAccountReceivables('', $date_to, $sale_id,'GasDailySelling','date_sell',1);
				$beginningCash = GasDailyCash::getDataAccountReceivables('', $date_to, $sale_id,'GasDailyCash','date_collection',1);			
			}
			
			$dataSelling = GasDailyCash::getDataAccountReceivables($date_from, $date_to, $sale_id,'GasDailySelling','date_sell');			
			$dataCash = GasDailyCash::getDataAccountReceivables($date_from, $date_to, $sale_id,'GasDailyCash','date_collection');
			
			$beginningAll = GasDailyCash::calcBeginning($aCustomer, $beginningSelling, $beginningCash, $dataSelling);
			// $beginningAll là mảng hợp của những customer còn nợ trước đó và những customer phát sinh trong khoảng thời gian thống kê selling 
			// mới sửa lại đã bỏ hợp đi, kiểm tra isset bên dưới cho lành
			// $beginningAll dùng cho vòng foreach bên dưới
			GasDailyCash::exportExcelAccountReceivables($aCustomer, $dataSelling, $dataCash, $beginningSelling, $beginningCash, $beginningAll);
		}  	
	}	
	
	public static function getDataAccountReceivables($date_from, $date_to, $sale_id, $name_model, $name_field, $is_beginning=0){
		$criteria=new CDbCriteria;
		if(!empty($date_from) && !empty($date_to))
			$criteria->addBetweenCondition($name_field,$date_from,$date_to); 
		elseif(!empty($date_to)){
			if($is_beginning)			
				$criteria->addCondition('t.'.$name_field.' < "'.$date_to.'"');
			else
				$criteria->addCondition('t.'.$name_field.' <= "'.$date_to.'"');
		}
			
		if(!empty($sale_id))	
			$criteria->compare('t.sale_id',$sale_id);		
		$criteria->select = 'customer_id, sum(total) as total';
		$criteria->group = 'customer_id';
		$criteria->order = "t.customer_id asc";
		// có thể add thêm relation ở đây để sort danh sách customer theo điều kiện đưa ra
		$model_ = call_user_func(array($name_model, 'model'));
		$res = $model_->findAll($criteria);	
		
		if(count($res)>0)
		foreach($res as $obj){
			$aExport[$obj->customer_id] = $obj->total;					
		}
		else
			$aExport = array();
		return $aExport; 	
	}
	
	public static function calcBeginning($aCustomer, $beginningSelling, $beginningCash, $dataSelling=array()){
		//$i=0;
		$aReturn=array();
		$aCheck=array();
		foreach ( $aCustomer as $key=>$item ){
			$beginSell = isset($beginningSelling[$key])?$beginningSelling[$key]:0;
			$beginCash = isset($beginningCash[$key])?$beginningCash[$key]:0;
			$beginning = $beginSell+$item->beginning-$beginCash;
			if($beginning>0){
				$aReturn[$key] = $beginning;	
				$aCheck[] = $item->id; // is customer id
			}
			//$i++;
		}
		//var_dump($aReturn);die;
		/* if(count($dataSelling)>0)
		foreach ( $dataSelling as $key=>$item ){
			if(!in_array($key, $aCheck)){
				$aReturn[$key] = $item;		
				$aCheck[] = $key; // is customer id
			}				
		} */		
		return $aReturn;
	}
	
	public static function exportExcelAccountReceivables($aCustomer, $dataSelling, $dataCash, $beginningSelling, $beginningCash, $beginningAll ){
	//var_dump($beginningAll);die;
		Yii::import('application.extensions.vendors.PHPExcel',true);
		$objPHPExcel = new PHPExcel();
		 // Set properties
		 $objPHPExcel->getProperties()->setCreator("NguyenDung")
                            ->setLastModifiedBy("NguyenDung")
                            ->setTitle('Account Receivables Report')
                            ->setSubject("Office 2007 XLSX Document")
                            ->setDescription("Account Receivables Report All Sale")
                            ->setKeywords("office 2007 openxml php")
                            ->setCategory("GAS");
		 $objPHPExcel->getActiveSheet()->setTitle('Account Receivables Report');
		 //             
		 $objPHPExcel->setActiveSheetIndex(0);
		 $objPHPExcel->getActiveSheet()->setCellValue("A1", 'Báo cáo công nợ khách hàng');
		$objPHPExcel->getActiveSheet()->mergeCells('A1:D1');
		 $objPHPExcel->getActiveSheet()->getStyle('A1:D1')
				->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);	
		if(!empty($_POST['GasDailyCash']['sell_date_from']))
			$objPHPExcel->getActiveSheet()->setCellValue("E1", 'Từ ngày: '.$_POST['GasDailyCash']['sell_date_from']);
		if(!empty($_POST['GasDailyCash']['sell_date_to']))
			$objPHPExcel->getActiveSheet()->setCellValue("F1", 'Đến ngày: '.$_POST['GasDailyCash']['sell_date_to']);
		  
		 $objPHPExcel->getActiveSheet()->setCellValue("A2", 'Mã KH');
		 $objPHPExcel->getActiveSheet()->setCellValue("B2", 'Tên KH');
		 $objPHPExcel->getActiveSheet()->setCellValue("C2", 'Tồn Đầu');
		 $objPHPExcel->getActiveSheet()->setCellValue("D2", 'Phải Thu');
		 $objPHPExcel->getActiveSheet()->setCellValue("E2", 'Đã Thu');
		 $objPHPExcel->getActiveSheet()->setCellValue("F2", 'Còn Lại');
		$objPHPExcel->getActiveSheet()
				->getStyle('A2:F2')
				->getFont()
				//->setSize(14)
				->setBold(true);
		$index = 1;		
		$rowNum = 3;
		$sumRow = 0;
		$sumAll=0;
		$sumCol = array();
		$sumBeginning = 0;
		$sumSell = 0;
		$sumCash = 0;
		$sumRemain = 0;
		
	 	foreach ( $aCustomer as $key=>$item ){
			if(isset($beginningAll[$key]) || isset($dataSelling[$key]))
			{
					$beginning=0;					
					 for($i=1;$i<7;$i++){
						$objCus = $item;
						if($i==1){
							$objPHPExcel->getActiveSheet()->setCellValueExplicit(MyFunctionCustom::columnName($index+$i-1).($rowNum), $objCus->code_account, PHPExcel_Cell_DataType::TYPE_STRING);
						}
						if($i==2){
							$objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index+$i-1).($rowNum), $objCus->first_name);
						}		
						if($i==3){ // Tồn đầu
							//$beginSell = isset($beginningSelling[$key])?$beginningSelling[$key]:0;
							//$beginCash = isset($beginningCash[$key])?$beginningCash[$key]:0;
							//$beginning = $beginSell+$objCus->beginning-$beginCash;
							$beginning = isset($beginningAll[$key]) ? $beginningAll[$key]:0;
							
							$sumBeginning+=$beginning;
							$objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index+$i-1).($rowNum), $beginning);
						}
						if($i==4 && isset($dataSelling[$key])){// Phải thu KH
							$sumSell+=$dataSelling[$key];
							$objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index+$i-1).($rowNum), $dataSelling[$key]);
						}				

						if($i==5 && isset($dataCash[$key])){ // cash - đã thu KH	
							$sumCash+=$dataCash[$key];				
							$objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index+$i-1).($rowNum), $dataCash[$key]);
						}		
						if($i==6){ // còn lại phải thu KH
							$cash = isset($dataCash[$key])?$dataCash[$key]:0;	
							$sell = isset($dataSelling[$key])?$dataSelling[$key]:0;	
							$sumCol6 = $beginning+$sell-$cash;
							$sumRemain+=$sumCol6;					
							$objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index+$i-1).($rowNum), $sumCol6);                                                        
						}					
					 } // end for($i=1;$i<7;$i++)			
                                        $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(14);
					$objPHPExcel->getActiveSheet()
						->getStyle(MyFunctionCustom::columnName($index+2).($rowNum).':'.MyFunctionCustom::columnName($index+6).($rowNum) )->getNumberFormat()
						->setFormatCode('#,##0');				 
					$rowNum++;			
			}// end if(isset($beginningAll[$key]) || isset($beginningSelling[$key]))
		
		} // end foreach arr customer
		
		
		$objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index).($rowNum), 'Tổng Cộng');
		$objPHPExcel->getActiveSheet()->mergeCells(MyFunctionCustom::columnName($index).($rowNum).':'.MyFunctionCustom::columnName($index+1).($rowNum));
		 $objPHPExcel->getActiveSheet()->getStyle(MyFunctionCustom::columnName($index).($rowNum).':'.MyFunctionCustom::columnName($index+1).($rowNum))
				->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);			
		 $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index+2).($rowNum), $sumBeginning);
		 $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index+3).($rowNum), $sumSell);
		 $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index+4).($rowNum), $sumCash);
		 $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index+5).($rowNum), $sumRemain);

		 // for sum row
		$objPHPExcel->getActiveSheet()
				->getStyle(MyFunctionCustom::columnName($index+2).($rowNum).':'.MyFunctionCustom::columnName($index+5).($rowNum) )->getNumberFormat()
				->setFormatCode('#,##0');		 
		$objPHPExcel->getActiveSheet()
				->getStyle(MyFunctionCustom::columnName($index).($rowNum).':'.MyFunctionCustom::columnName($index+5).($rowNum) )
				->getFont()
				//->setSize(14)
				->setBold(true);		
		$objPHPExcel->getActiveSheet()
				->getStyle(MyFunctionCustom::columnName($index+5).'3:'.MyFunctionCustom::columnName($index+5).($rowNum) )
				->getFont()
				//->setSize(14)
				->setBold(true);				
		$objPHPExcel->getActiveSheet()->getStyle(MyFunctionCustom::columnName($index).'2:'.MyFunctionCustom::columnName($index+5).($rowNum) )
                        ->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);					
		 //save file 
		 $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
		 //$objWriter->save('MyExcel.xslx');

		 for($level=ob_get_level();$level>0;--$level)
		 {
			 @ob_end_clean();
		 }
		 header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
		 header('Pragma: public');
		 header('Content-type: '.'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		 header('Content-Disposition: attachment; filename="'.'Account Receivables'.'.'.'xlsx'.'"');
		 header('Cache-Control: max-age=0');				
		 $objWriter->save('php://output');			
		 Yii::app()->end();		  

	}	
        	
	
        public static function exportAccountReceivablesExpired($date_to, $sale_id){
		if(!empty($date_to) ){
			$beginningSelling = array();// Tồn đầu kỳ khi chọn 1 khoảng ngày thống kê, nếu chọn 1 ngày thì beginning sẽ lấy trong bảng user
			$beginningCash = array();
			$beginningAll = array();// list những KH có Tồn  đầu kỳ > 0;
			// 1. get list customer id=>model customer
			$aCustomer = Users::getArrObjectUserByRole(ROLE_CUSTOMER);			
			$date_to = MyFormat::dateConverDmyToYmd($date_to);
			$beginningSelling = GasDailyCash::getDataAccountReceivablesExpired($date_to, $sale_id,'GasDailySelling','date_sell');
			$beginningCash = GasDailyCash::getDataAccountReceivablesExpired($date_to, $sale_id,'GasDailyCash','date_collection');
			$beginningAll = GasDailyCash::calcBeginning($aCustomer, $beginningSelling, $beginningCash, array());
			// lấy tất cả các bán hàng của các khách hàng từ lần đầu - khi dữ liệu KH nợ nhiều thì chỗ này có thể chạy sẽ rất lâu = số lần query = với số KH nợ khi đó
			$aSellingOfCustomer = GasDailyCash::getSellingByCustomer($date_to, $sale_id, $beginningAll);
			// $beginningAll array(CusId=>total)
			// $beginningAll là mảng hợp của những customer còn nợ trước đó và những customer phát sinh trong khoảng thời gian thống kê selling 
			// mới sửa lại đã bỏ hợp đi, kiểm tra isset bên dưới cho lành
			// $beginningAll dùng cho vòng foreach bên dưới
			GasDailyCash::exportExcelAccountReceivablesExpired($date_to, $aCustomer, $beginningSelling, $beginningCash, $beginningAll, $aSellingOfCustomer);
		}  	
	}	
	
	
	public static function getSellingByCustomer($date_to, $sale_id, $beginningAll){
		$aSellingOfCustomer=array();
		foreach($beginningAll as $cusId=>$total){
			$criteria=new CDbCriteria;		
			if(!empty($sale_id))	
				$criteria->compare('t.sale_id',$sale_id);
			$criteria->compare('t.customer_id',$cusId);					
			$criteria->addCondition('t.date_sell <= "'.$date_to.'"');
			$criteria->order = "t.date_sell desc";
			// có thể add thêm relation ở đây để sort danh sách customer theo điều kiện đưa ra			
			$aSellingOfCustomer[$cusId] = GasDailySelling::model()->findAll($criteria);								
		}
		return $aSellingOfCustomer;				
	}
	
	public static function getDataAccountReceivablesExpired($date_to, $sale_id, $name_model, $name_field){
		$criteria=new CDbCriteria;
                $criteria->addCondition('t.'.$name_field.' <= "'.$date_to.'"');
//                $criteria->addCondition('t.'.$name_field.' <= "'.$date_to.'"');
			
		if(!empty($sale_id))	
			$criteria->compare('t.sale_id',$sale_id);		
		$criteria->select = 'customer_id, sum(total) as total';
		$criteria->group = 'customer_id';
		$criteria->order = "t.customer_id asc";
		// có thể add thêm relation ở đây để sort danh sách customer theo điều kiện đưa ra
		$model_ = call_user_func(array($name_model, 'model'));
		$res = $model_->findAll($criteria);	
		
		if(count($res)>0)
		foreach($res as $obj){
			$aExport[$obj->customer_id] = $obj->total;					
		}
		else
			$aExport = array();
		return $aExport; 	
	}
	     
        public static function exportExcelAccountReceivablesExpired($date_to, $aCustomer, $beginningSelling, $beginningCash, $beginningAll, $aSellingOfCustomer ){
		Yii::import('application.extensions.vendors.PHPExcel',true);
		$objPHPExcel = new PHPExcel();
		 // Set properties
		 $objPHPExcel->getProperties()->setCreator("NguyenDung")
                            ->setLastModifiedBy("NguyenDung")
                            ->setTitle('Account Receivables Expired Report')
                            ->setSubject("Office 2007 XLSX Document")
                            ->setDescription("Account Receivables Expired Report")
                            ->setKeywords("office 2007 openxml php")
                            ->setCategory("GAS");
		 $objPHPExcel->getActiveSheet()->setTitle('Account Receivables Expired');
		 //             
		 $objPHPExcel->setActiveSheetIndex(0);
		 $objPHPExcel->getActiveSheet()->setCellValue("A1", 'Báo Cáo Công Nợ Quá Hạn Đến Ngày:'.$_POST['GasDailyCash']['sell_date_to']);
		$objPHPExcel->getActiveSheet()->mergeCells('A1:D1');
		 $objPHPExcel->getActiveSheet()->getStyle('A1:D1')
				->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);	
		  
		 $objPHPExcel->getActiveSheet()->setCellValue("A2", 'Mã KH');
		 $objPHPExcel->getActiveSheet()->setCellValue("B2", 'Tên KH');
		 $objPHPExcel->getActiveSheet()->setCellValue("C2", 'Dư nợ tại '.$_POST['GasDailyCash']['sell_date_to']);
		 $objPHPExcel->getActiveSheet()->setCellValue("D2", 'Trong Hạn');
		 $objPHPExcel->getActiveSheet()->setCellValue("E2", 'Tới Hạn');
		 $objPHPExcel->getActiveSheet()->setCellValue("F2", 'Quá Hạn');
                 $objPHPExcel->getActiveSheet()->mergeCells('A2:A3');
                 $objPHPExcel->getActiveSheet()->mergeCells('B2:B3');
                 $objPHPExcel->getActiveSheet()->mergeCells('C2:C3');
                 $objPHPExcel->getActiveSheet()->mergeCells('D2:D3');
                 $objPHPExcel->getActiveSheet()->mergeCells('E2:E3');
                 $objPHPExcel->getActiveSheet()->mergeCells('F2:F3');
                $objPHPExcel->getActiveSheet()->getStyle('A2:F3')
                       ->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);

                 $objPHPExcel->getActiveSheet()->setCellValue("G2", 'Trong đó');
                 $objPHPExcel->getActiveSheet()->setCellValue("G3", 'Từ 1 - 10');
                 $objPHPExcel->getActiveSheet()->setCellValue("H3", 'Từ 11 - 20');
                 $objPHPExcel->getActiveSheet()->setCellValue("I3", 'Từ 21 - 30');
                 $objPHPExcel->getActiveSheet()->setCellValue("J3", 'Từ 31 - 6T');
                 $objPHPExcel->getActiveSheet()->setCellValue("K3", 'Từ 6T - 1 năm');
                 $objPHPExcel->getActiveSheet()->setCellValue("L3", '> 1 năm');
                    $objPHPExcel->getActiveSheet()->mergeCells('G2:L2');
		 $objPHPExcel->getActiveSheet()->getStyle('G2:L2')
				->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);	
                $objPHPExcel->getActiveSheet()->getStyle('G2:L3')
                        ->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
                    
		$index = 1;		
		$rowNum = 4;
		$sumRow = 0;
		$sumAll=0;
		$sumCol = array();
		$sumBeginning = 0;
		$sumSell = 0;
		$sumCash = 0;
		$sumRemain = 0;
			$trongHanSum=0;	
			$toiHanSum=0;	
			$quaHanSum=0;			
			$from_1_to_10Sum=0;	
			$from_11_to_20Sum=0;	
			$from_21_to_30Sum=0;	
			$from_31_to_180Sum=0;	
			$from_181_to_365Sum=0;	
			$larger_than_1_yearSum=0;			
		
	 	foreach ( $aCustomer as $key=>$item ){
			if(isset($beginningAll[$key]) || isset($dataSelling[$key]))
			{
					$beginning=0;							
					$trongHan=0;	
					$toiHan=0;	
					$quaHan=0;	
					$from_1_to_10=0;	
					$from_11_to_20=0;	
					$from_21_to_30=0;	
					$from_31_to_180=0;	
					$from_181_to_365=0;	
					$larger_than_1_year=0;	
					 for($i=1;$i<13;$i++){
						$objCus = $item;
						if($i==1){
							$objPHPExcel->getActiveSheet()->setCellValueExplicit(MyFunctionCustom::columnName($index+$i-1).($rowNum), $objCus->code_account, PHPExcel_Cell_DataType::TYPE_STRING);
						}
						if($i==2){
							$objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index+$i-1).($rowNum), $objCus->first_name);
						}		
						if($i==3){ // Tồn đầu
							//$beginSell = isset($beginningSelling[$key])?$beginningSelling[$key]:0;
							//$beginCash = isset($beginningCash[$key])?$beginningCash[$key]:0;
							//$beginning = $beginSell+$objCus->beginning-$beginCash;
							$beginning = isset($beginningAll[$key]) ? $beginningAll[$key]:0;
							if($beginning>0){ // nếu nợ > 0 tính xem khoản nợ đó dc bao lâu rồi
								if(count($aSellingOfCustomer[$key])>0){
									$tempSelling = 0;
									foreach($aSellingOfCustomer[$key] as $keyModelSell=>$mSelling){
										$tempSelling+=$mSelling->total;
										if($tempSelling<=$beginning){
											// cộng từng hóa đơn bán hàng, tính từ cái mới nhất, nếu vượt qua số dư nợ thì break
											// sau đó tính số ngày để đưa khoản nợ đó vào biến
											$trangThaiDuNo = MyFunctionCustom::compareTwoDay($mSelling->date_of_payment, $date_to);
											if($trangThaiDuNo==TRONG_HAN){
												$trongHan += $mSelling->total;	
												$trongHanSum+= $mSelling->total;
											}elseif($trangThaiDuNo==TOI_HAN){
												$toiHan += $mSelling->total;	
												$toiHanSum += $mSelling->total;	
											}elseif($trangThaiDuNo==QUA_HAN){
												$quaHan += $mSelling->total;	
												$quaHanSum += $mSelling->total;	
												$soNgayDuNo = MyFunctionCustom::getDayBetween($mSelling->date_of_payment, $date_to);
												if($soNgayDuNo>0 && $soNgayDuNo<=10){
													$from_1_to_10 += $mSelling->total;
													$from_1_to_10Sum += $mSelling->total;
												}elseif($soNgayDuNo>10 && $soNgayDuNo<=20){
													$from_11_to_20 += $mSelling->total;
													$from_11_to_20Sum += $mSelling->total;
												}elseif($soNgayDuNo>20 && $soNgayDuNo<=30){
													$from_21_to_30 += $mSelling->total;
													$from_21_to_30Sum += $mSelling->total;
												}elseif($soNgayDuNo>30 && $soNgayDuNo<=180){
													$from_31_to_180 += $mSelling->total;
													$from_31_to_180Sum += $mSelling->total;
												}elseif($soNgayDuNo>180 && $soNgayDuNo<=365){
													$from_181_to_365 += $mSelling->total;
													$from_181_to_365Sum += $mSelling->total;
												}elseif($soNgayDuNo>365){
													$larger_than_1_year += $mSelling->total;
													$larger_than_1_yearSum += $mSelling->total;
												}// end if($soNgayDuNo>1 && $soNgayDuNo<=10){
																																			
											} // end if($trangThaiDuNo
											
										}// end if($tempSelling<=$beginning){
										else
											break;
									}
									// trường hợp khoản nợ nằm trong cả bán hàng mới và tồn đầu kỳ tháng 6 - nhập trong bảng KH 	
									if( $keyModelSell == (count($aSellingOfCustomer[$key])-1) && $tempSelling<$beginning){
										$soNgayDuNo = MyFunctionCustom::getDayBetween(DATE_TON_DAU, $date_to);
										if($soNgayDuNo>0 && $soNgayDuNo<=10){
											$from_1_to_10 += ($beginning - $tempSelling);
											$from_1_to_10Sum += ($beginning - $tempSelling);
										}elseif($soNgayDuNo>10 && $soNgayDuNo<=20){
											$from_11_to_20 += ($beginning - $tempSelling);
											$from_11_to_20Sum += ($beginning - $tempSelling);
										}elseif($soNgayDuNo>20 && $soNgayDuNo<=30){
											$from_21_to_30 += ($beginning - $tempSelling);
											$from_21_to_30Sum += ($beginning - $tempSelling);
										}elseif($soNgayDuNo>30 && $soNgayDuNo<=180){
											$from_31_to_180 += ($beginning - $tempSelling);
											$from_31_to_180Sum += ($beginning - $tempSelling);
										}elseif($soNgayDuNo>180 && $soNgayDuNo<=365){
											$from_181_to_365 += ($beginning - $tempSelling);
											$from_181_to_365Sum += ($beginning - $tempSelling);
										}elseif($soNgayDuNo>365){
											$larger_than_1_year += ($beginning - $tempSelling);
											$larger_than_1_yearSum += ($beginning - $tempSelling);
										}// end if($soNgayDuNo>1 && $soNgayDuNo<=10){												
									
									}// end if( $keyModelSell == (count($aSellingOfCustomer[$key])-		
									
								}else{
									// trường hợp này là khoản nợ tồn đầu nhập vào khi import khách hàng=> nên đưa vào > 1 năm hay chỗ nào										
										$soNgayDuNo = MyFunctionCustom::getDayBetween(DATE_TON_DAU, $date_to);									
										if($soNgayDuNo>0 && $soNgayDuNo<=10){
											$from_1_to_10 += $beginning;
											$from_1_to_10Sum += $beginning;
										}elseif($soNgayDuNo>10 && $soNgayDuNo<=20){
											$from_11_to_20 += $beginning;
											$from_11_to_20Sum += $beginning;
										}elseif($soNgayDuNo>20 && $soNgayDuNo<=30){
											$from_21_to_30 += $beginning;
											$from_21_to_30Sum += $beginning;
										}elseif($soNgayDuNo>30 && $soNgayDuNo<=180){
											$from_31_to_180 += $beginning;
											$from_31_to_180Sum += $beginning;
										}elseif($soNgayDuNo>180 && $soNgayDuNo<=365){
											$from_181_to_365 += $beginning;
											$from_181_to_365Sum += $beginning;
										}elseif($soNgayDuNo>365){
											$larger_than_1_year += $beginning;
											$larger_than_1_yearSum += $beginning;
										}// end if($soNgayDuNo>1 && $soNgayDuNo<=10){									
								}

							}
							
							$sumBeginning+=$beginning;
							$objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index+$i-1).($rowNum), $beginning);
						}
						if($i==4){// Trong Hạn
							if($trongHan>0)
								$objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index+$i-1).($rowNum), $trongHan);
						}				

						if($i==5){ // Tới Hạn
							if($toiHan>0)		
								$objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index+$i-1).($rowNum), $toiHan);
						}		
						if($i==6){ // Quá Hạn					
							if($quaHan>0)		
								$objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index+$i-1).($rowNum), $quaHan);
						}
						if($i==7){ // Từ 1 - 10					
							if($from_1_to_10>0)		
								$objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index+$i-1).($rowNum), $from_1_to_10);
						}
						if($i==8){ // Từ 11 - 20				
							if($from_11_to_20>0)		
								$objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index+$i-1).($rowNum), $from_11_to_20);
						}						
						if($i==9){ // Từ 21 - 30				
							if($from_21_to_30>0)		
								$objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index+$i-1).($rowNum), $from_21_to_30);
						}						
						if($i==10){ // Từ 31 - 6T	 				
							if($from_31_to_180>0)		
								$objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index+$i-1).($rowNum), $from_31_to_180);
						}						
						if($i==11){ // Từ 6T - 1 năm				
							if($from_181_to_365>0)		
								$objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index+$i-1).($rowNum), $from_181_to_365);
						}						
						if($i==12){ // > 1 năm				
							if($larger_than_1_year>0)		
								$objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index+$i-1).($rowNum), $larger_than_1_year);
						}						
						
						
					 } // end for($i=1;$i<13;$i++)			
			
					$objPHPExcel->getActiveSheet()
						->getStyle(MyFunctionCustom::columnName($index+2).($rowNum).':'.MyFunctionCustom::columnName($index+12).($rowNum) )->getNumberFormat()
						->setFormatCode('#,##0');				 
					$rowNum++;			
			}// end if(isset($beginningAll[$key]) || isset($beginningSelling[$key]))
		
		} // end foreach arr customer
		
		
		$objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index).($rowNum), 'Tổng Cộng');
		$objPHPExcel->getActiveSheet()->mergeCells(MyFunctionCustom::columnName($index).($rowNum).':'.MyFunctionCustom::columnName($index+1).($rowNum));
		 $objPHPExcel->getActiveSheet()->getStyle(MyFunctionCustom::columnName($index).($rowNum).':'.MyFunctionCustom::columnName($index+1).($rowNum))
				->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);			
		 $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index+2).($rowNum), $sumBeginning);
		 $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index+3).($rowNum), $trongHanSum);
		 $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index+4).($rowNum), $toiHanSum);
		 $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index+5).($rowNum), $quaHanSum);
		 $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index+6).($rowNum), $from_1_to_10Sum);
		 $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index+7).($rowNum), $from_11_to_20Sum);
		 $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index+8).($rowNum), $from_21_to_30Sum);
		 $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index+9).($rowNum), $from_31_to_180Sum);
		 $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index+10).($rowNum), $from_181_to_365Sum);
		 $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index+11).($rowNum), $larger_than_1_yearSum);	 

		 // for sum row
		$objPHPExcel->getActiveSheet()
				->getStyle(MyFunctionCustom::columnName($index+2).($rowNum).':'.MyFunctionCustom::columnName($index+12).($rowNum) )->getNumberFormat()
				->setFormatCode('#,##0');		 
		$objPHPExcel->getActiveSheet()
				->getStyle(MyFunctionCustom::columnName($index).($rowNum).':'.MyFunctionCustom::columnName($index+12).($rowNum) )
				->getFont()
				//->setSize(14)
				->setBold(true);		 
		$objPHPExcel->getActiveSheet()->getStyle(MyFunctionCustom::columnName($index).'4:'.MyFunctionCustom::columnName($index+11).($rowNum) )
                        ->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);				
		 //save file 
		 $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
		 //$objWriter->save('MyExcel.xslx');

		 for($level=ob_get_level();$level>0;--$level)
		 {
			 @ob_end_clean();
		 }
		 header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
		 header('Pragma: public');
		 header('Content-type: '.'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		 header('Content-Disposition: attachment; filename="'.'Account Receivables Expired'.'.'.'xlsx'.'"');
		 header('Cache-Control: max-age=0');				
		 $objWriter->save('php://output');			
		 Yii::app()->end();		  

	}	 
        
        
}