<?php

/**
 * This is the model class for table "{{_users_fix_address}}".
 *
 * The followings are the available columns in table '{{_users_fix_address}}':
 * @property string $id
 * @property integer $status
 * @property string $customer_id
 * @property string $employee_maintain_id
 * @property string $uid_login_sell
 * @property string $address_old
 * @property string $address_new
 * @property string $last_update
 * @property string $created_date
 * @property string $reject_by
 * @property string $reject_time
 * @property string $transaction_history_id
 * @property Users $rSale Call center account
 * @property Users $rCustomer Customer
 * @property Users $rEmployeeMaintain NVGN
 * @property Users $rReject Employee who reject info
 */
class UsersFixAddress extends BaseSpj
{
    const STATUS_COMPLETE   = 1;
    const STATUS_REJECT     = 2;
    public $date_from, $date_to, $autocomplete_name;

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{_users_fix_address}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        return array(
            array('reject_by, reject_time, transaction_history_id, id, status, customer_id, employee_maintain_id, uid_login_sell, address_old, address_new, last_update, created_date', 'safe'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        return array(
            'rCustomer' => array(self::BELONGS_TO, 'Users', 'customer_id'),
            'rSale' => array(self::BELONGS_TO, 'Users', 'uid_login_sell'),
            'rEmployeeMaintain' => array(self::BELONGS_TO, 'Users', 'employee_maintain_id'),
            'rReject' => array(self::BELONGS_TO, 'Users', 'reject_by'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'status' => 'Trạng thái',
            'customer_id' => 'Khách Hàng',
            'employee_maintain_id' => 'NV Giao Nhận',
            'uid_login_sell' => 'NV CallCenter',
            'address_old' => 'Địa chỉ cũ',
            'address_new' => 'Địa chỉ mới',
            'last_update' => 'Ngày sửa',
            'created_date' => 'Ngày tạo',
            'reject_by' => 'Người hủy bỏ',
            'reject_time' => 'Thời gian hủy',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        $criteria = new CDbCriteria;

        $criteria->compare('t.status', $this->status);
        $criteria->compare('t.customer_id', $this->customer_id);
        $criteria->compare('t.employee_maintain_id', $this->employee_maintain_id);
        if(!empty($this->transaction_history_id)){
            $this->mapConditionSellCode($criteria);
        }
        $criteria->order = 't.id DESC';
        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
            'pagination' => array(
                'pageSize' => Yii::app()->user->getState('pageSize', Yii::app()->params['defaultPageSize']),
            ),
        ));
    }

    /** @Author: ANH DUNG Aug 29, 2017
     */
    public function mapConditionSellCode(&$criteria) {
        $criteriaSub = new CDbCriteria();
        $criteriaSub->compare("t.code_no", $this->transaction_history_id);
        $mSell = Sell::model()->find($criteriaSub);
        if($mSell){
            $criteria->addCondition('t.transaction_history_id=' . $mSell->transaction_history_id);
        }else{
            $criteria->addCondition('t.transaction_history_id=-1');
        }
    }

    /** @Author: ANH DUNG Jun 30, 2017
     * @Todo: tạo record hoặc cập nhật record khi User cập nhật trên app
     */
    public function saveRecord()
    {
        $model = $this->getByCustomer();
        if (is_null($model)) {
            $this->mapRecordSell();
            $this->save();
        } else {
            $model->transaction_history_id = $this->transaction_history_id;
            $model->address_new = $this->address_new;
            $model->mapRecordSell();
            $model->update();
        }
    }

    public function mapRecordSell() 
    {
        $this->last_update = date('Y-m-d');
        if (empty($this->transaction_history_id)) {
            return;
        }
        $mTranHistory = TransactionHistory::model()->findByPk($this->transaction_history_id);
        if ($mTranHistory) {
            $this->uid_login_sell       = $mTranHistory->employee_accounting_id;
            $this->employee_maintain_id = $mTranHistory->employee_maintain_id;
            $tmp = explode(';', $this->address_new);
            $mTranHistory->first_name   = $tmp[0];
            $mTranHistory->address      = $tmp[1];
            $mTranHistory->update(['first_name','address']);
        }
    }

    /** @Author: ANH DUNG Jun 19, 2017
     * @Todo: tìm record trong tháng
     */
    public function getByCustomer()
    {
        $cMonth = date('m');
        $cYear = date('Y');
        $criteria = new CDbCriteria();
        $criteria->addCondition('t.customer_id=' . $this->customer_id);
        $criteria->addCondition('MONTH(last_update)=' . $cMonth);
        $criteria->addCondition('YEAR(last_update)=' . $cYear);
        return self::model()->find($criteria);
    }

    public function makeRecordNew()
    {

    }

    public function getCustomer($field_name = 'first_name')
    {
        $mUser = $this->rCustomer;
        if ($mUser) {
            return $mUser->$field_name;
        }
        return '';
    }

    public function getEmployeeMaintain($field_name = 'first_name')
    {
        $mUser = $this->rEmployeeMaintain;
        if ($mUser) {
            return $mUser->$field_name;
        }
        return '';
    }

    public function getReject($field_name = 'first_name')
    {
        $mUser = $this->rReject;
        if ($mUser) {
            return $mUser->$field_name;
        }
        return '';
    }

    public function getSaleUser($field_name = 'first_name')
    {
        $mUser = $this->rSale;
        if ($mUser) {
            return $mUser->$field_name;
        }
        return '';
    }


    public function getStatusName()
    {
        if ($this->status == UsersFixAddress::STATUS_REJECT) {
            return 'Đã hủy bỏ';
        } else if ($this->status == UsersFixAddress::STATUS_COMPLETE) {
            return 'Hoàn thành';
        }
        return '';
    }

    /** Check address is rejected
     * @return bool
     */
    public function isRejected()
    {
        return $this->status == UsersFixAddress::STATUS_REJECT;
    }

    /**
     * Check model is completed or not
     * @author : Trung July 12, 2017
     *
     * @return bool
     */
    public function isCompleted()
    {
        return $this->status == UsersFixAddress::STATUS_COMPLETE;
    }

    /**
     * Save and update model for reject function
     * @author : Trung July 12, 2017
     */
    public function saveReject()
    {
        $this->status = UsersFixAddress::STATUS_REJECT;
        $this->reject_time = new CDbExpression('NOW()');
        $this->reject_by = MyFormat::getCurrentUid();
        $this->save();
    }

    /**
     * Save and update model for confirm function
     */
    public function saveConfirm()
    {
        $this->status = UsersFixAddress::STATUS_COMPLETE;
        $this->last_update = new CDbExpression('NOW()');
        $this->reject_by = null;
        $this->reject_time = null;
        $this->save();
    }
}