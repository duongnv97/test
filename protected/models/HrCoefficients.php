<?php

/**
 * This is the model class for table "{{gas_coefficients}}".
  ---Field in database
 * role_id         Mã nhóm user			
 * value           Giá trị			
 * name            Tên hệ số			
 * created_date    Ngày tạo			
 * created_by      Người tạo			
 * status          Trạng thái record			
 */

/** @Author: DUONG 28/03/2018
 *  @Code:   DUONG001
 *  @Param:
 * */
class HrCoefficients extends CActiveRecord {

    public $status_active = 1;
    public $status_in_active = 2;
    const ROLE_GLOBAL_ID = -1; //role chung
    const ROLE_GLOBAL_NAME = 'Chung';
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    public function tableName() {
        return '{{_hr_coefficients}}';
    }

    public function rules() {
        return array(
            array('name, value, position_work, position_work_list', 'required', 'on' => 'create,update'),
            array('value, name, created_by, status, position_work, position_work', 'safe'),
            array('value', 'numerical', 'min'=>0.001, 'tooSmall'=>'Giá trị phải >= 0.001'),
        );
    }

    public function relations() {
        return array(
//            'rRole' => array(self::BELONGS_TO, 'Roles', 'role_id'),
            'rUser' => array(self::BELONGS_TO, 'Users', 'created_by'),
        );
    }

    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'value' => 'Giá trị',
            'name' => 'Tên hệ số',
            'created_date' => 'Ngày tạo',
            'created_by' => 'Người tạo',
            'status' => 'Trạng thái',
            'position_work' => 'Bộ phận làm việc chính',
            'position_work_list' => 'Bộ phận làm việc'
        );
    }

    public function search() {
        $criteria = new CDbCriteria;

        if (!empty($this->name)) {
            $criteria->addCondition('t.name like "%' . $this->name . '%"');
        }
//        if (!empty($this->role_id)) {
//            $criteria->addCondition('t.role_id =' . $this->role_id);
//        }
        if (!empty($this->value)) {
            $criteria->addCondition('t.value =' . $this->value);
        }
        if (isset($this->position_work)) {
            $list = implode(",", $this->position_work);
            $criteria->addCondition('t.position_work IN (' . $list . ')');
        }
//        if (!empty($this->status)) {
//            $criteria->addCondition('t.status =' . $this->status);
//        }
        $sort = new CSort();
        $sort->attributes = array(
            'name' => 'name',
            'position_work' => 'position_work'
        );
        $sort->defaultOrder = 't.position_work desc';
        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
            'sort' => $sort,
            'pagination' => array(
                'pageSize' => 50,
            ),
        ));
    }

    public function getArrayRoles() {
        $aRoles = Roles::getDropdownList();
        $aRoles[self::ROLE_GLOBAL_ID] = self::ROLE_GLOBAL_NAME;
        return $aRoles;
    }

    public function beforeSave() {
        if (empty($this->created_by)) {
            $this->created_by = MyFormat::getCurrentUid();
        }
        return parent::beforeSave();
    }
    
    /**
     * Override before delete method
     * @return Parent result
     */
    public function beforeDelete() {
        $retVal = true;
        // Check foreign key from table GasOneManyBig
        $foreigns = GasOneManyBig::model()->findByAttributes(array(
            'many_id' => $this->id,
            'type'      => GasOneManyBig::FUNCTION_COEFFICIENTS
        ));
        if (count($foreigns)) {
            $retVal = false;
        }
        return $retVal;
    }

    /** @Author: HOANG NAM 29/03/2018
     *  @Todo: get
     *  @Param: 
     * */
    public function getArrayStatus() {
        return array(
            $this->status_active => 'Hoạt động',
            $this->status_in_active => 'Không hoạt động',
        );
    }

    public function getRoles() {
        if($this->role_id == self::ROLE_GLOBAL_ID) return self::ROLE_GLOBAL_NAME;
        return !empty($this->rRole) ? $this->rRole->role_name : '';
    }

    public function getUser() {
        return !empty($this->rUser) ? $this->rUser->first_name : '';
    }

    public function getStatus() {
        $aStatus = $this->getArrayStatus();
        return isset($aStatus[$this->status]) ? $aStatus[$this->status] : '';
    }

    public function getCreatedDate() {
        return MyFormat::dateConverYmdToDmy($this->created_date, "d/m/Y H:i");
    }

    public function getName() {
        return $this->name;
    }

    public function getValue() {
        return ActiveRecord::formatCurrency($this->value);
    }

    public function getPositionWork($list = false) {
        $aPositionWork = HrSalaryReports::model()->getArrayWorkPosition();
        if($list){
            if(empty($this->position_work_list)) return '';
            $aIdPosition = explode(",", $this->position_work_list);
            $res = '';
            foreach ($aIdPosition as $value) {
                $res .= $aPositionWork[$value] . ', ';
            }
            return $res;
        } else {
            return $aPositionWork[$this->position_work];
        }
    }
    
    /**
     * Get raw value of value field
     * @return type
     */
    public function getRawValue() {
        return $this->value;
    }
    
    /** @Author: HOANG NAM 18/04/2018
     *  @Todo: get by roles
     *  @Param: 
     **/
    public function getActiveByRoles($role_id){
        $criteria=new CDbCriteria;
        if(!empty($role_id)) {
            // DuongNV 08/08/18 add role_all
            if(is_array($role_id)){
                $criteria->addCondition('t.role_id IN (' . implode(',', $role_id) . ')');
            } else {
                $criteria->addCondition('t.role_id =' . $role_id);
            }
            $criteria->addCondition('t.status ='.$this->status_active);
            return $this->findAll($criteria);
        }
        return array();
    }

    public function canView() {
        $cRole  = MyFormat::getCurrentRoleId();
        $cUid   = MyFormat::getCurrentUid();
        if($cRole == ROLE_ADMIN){
            return true;
        }
        if ($cUid != $this->created_by) {
            return false; // cho phép admin sửa hết, còn những user khác thì ai tạo thì dc sửa của người đó
        }
        return false;
    }
    
    public function getCoefficientsByRole($role_id = '') {
        $criteria = new CDbCriteria;
        $criteria->addCondition('role_id = '. $role_id);
        $sort = new CSort();

        $sort->attributes = array(
            'name' => 'name',
        );
        $sort->defaultOrder = 't.id desc';

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
            'sort' => $sort,
            'pagination' => array(
                'pageSize' => 30,
            ),
        ));
    }
    
    /** @Author: DuongNV Dec 4,2018
     *  @Todo: get Coefficient by position_work
     * */
    public function getCoefByPosition($pos = '') {
        $criteria = new CDbCriteria;
        if (!empty($pos)) {
//            $criteria->addCondition('t.position_work IN (' . $pos . ',' . ROLE_ALL . ')');
            $criteria->addCondition('t.position_work_list LIKE "%' . $pos . '%"');
            $criteria->addCondition('t.status =' . $this->status_active);
            return $this->findAll($criteria);
        }
        return array();
    }
}
