<?php

/**
 * This is the model class for table "{{_work_report}}".
 *
 * The followings are the available columns in table '{{_work_report}}':
 * @property string $id
 * @property string $uid_login
 * @property integer $role_id
 * @property string $content
 * @property string $google_map
 * @property string $created_date_only
 * @property string $created_date
 */
class WorkReport extends BaseSpj
{
    public $file_name, $date_from, $date_to, $pageSize, $autocomplete_name;
    public $MAX_ID;
    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return WorkReport the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{_work_report}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        return array(
            array('uid_login, content, google_map', 'required', 'on'=>'ApiCreate'),
            array('c_name, pageSize, id, uid_login, role_id, content, google_map, created_date_only, created_date', 'safe'),
            array('user_id, user_role_id, date_from, date_to', 'safe'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        return array(
            'rUser' => array(self::BELONGS_TO, 'Users', 'uid_login'),
            'rUserReport' => array(self::BELONGS_TO, 'Users', 'user_id'),
            'rFile' => array(self::HAS_MANY, 'GasFile', 'belong_id',
//                'on' => 'rFile.type=' . GasFile::TYPE_7_WORK_REPORT.' AND rFile.belong_id='.$this->id,
                'on' => 'rFile.type=' . GasFile::TYPE_7_WORK_REPORT,
                'order' => 'rFile.id ASC',
            ),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
            return array(
                'id' => 'ID',
                'uid_login' => 'Nhân viên',
                'role_id' => 'Chức vụ',
                'content' => 'Nội dung',
                'google_map' => 'Vị trí',
                'created_date_only' => 'Created Date Only',
                'created_date' => 'Ngày tạo',
                'user_id' => 'Đại lý / KH',
            );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        $criteria=new CDbCriteria;
        $criteria->compare('t.uid_login',$this->uid_login);
        $criteria->compare('t.role_id',$this->role_id);
        $date_from = '';
        $date_to = '';
        if(!empty($this->date_from)){
            $date_from = MyFormat::dateDmyToYmdForAllIndexSearch($this->date_from);
        }
        if(!empty($this->date_to)){
            $date_to = MyFormat::dateDmyToYmdForAllIndexSearch($this->date_to);
        }
        if(!empty($date_from)){
            $criteria->addCondition("t.created_date_only>='$date_from'");
        }
        if(!empty($date_to)){
            $criteria->addCondition("t.created_date_only<='$date_to'");
        }
        $criteria->order = 't.id DESC';
        $data = new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
            'pagination'=>array(
                'pageSize'=> $this->pageSize,
            ),
        ));
//        echo '<pre>';
//        print_r($data);
//        echo '</pre>';
//        die;

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
            'pagination'=>array(
                'pageSize'=> $this->pageSize,
            ),
        ));
    }
    
    protected function beforeValidate() {
        $mUser = $this->rUser;
        if($mUser){
            $this->role_id  = $mUser->role_id;
            $this->c_name   = $mUser->getFullName();
        }
        $mUserReport = $this->rUserReport;
        if($mUserReport){
            $this->user_role_id  = $mUserReport->role_id;
        }
        return parent::beforeValidate();
    }
    
    protected function beforeSave() {
        if($this->isNewRecord){
            $this->code_no = MyFunctionCustom::getNextId('WorkReport', "W".date('y'), LENGTH_TICKET, 'code_no');
        }
        return parent::beforeSave();
    }
    
    /**
     * @Author: ANH DUNG Oct 06, 2016
     * map attribute to model form $q. get post and validate
     * @param: $result, $q, $mUser
     * @param: $apiController api controller 
     */
    public function apiGetPost($result, $q, $mUser, $apiController) {
        $this->uid_login            = $mUser->id;
        $this->content              = trim($q->content);
        $this->created_date_only    = date('Y-m-d');
        $this->user_id              = isset($q->user_report) ? $q->user_report : 0;
        $this->user_id              = isset($q->user_id) ? $q->user_id : $this->user_id; // Aug0619 fix for ios
        if(empty($q->latitude) || empty($q->longitude)){
            throw new Exception('Không cập nhật được vị trí');
        }
        $this->google_map            = $q->latitude.",$q->longitude";
        $this->validate();
        $apiController->checkVersionCode($q, $this);
        $mGasFile = new GasFile();
        $mGasFile->checkEmpty = true;
        $mGasFile->apiValidateFileFixV1($this, 'file_name');
        if($this->hasErrors()){
            $result['message'] = HandleLabel::FortmatErrorsModel($this->getErrors());
            ApiModule::sendResponse($result, $apiController);
        }
        $this->save();
        $mGasFile = new GasFile();
        $mGasFile->apiSaveRecordFileFixV1($this, GasFile::TYPE_7_WORK_REPORT);
        $this->updateIpLimitPromotion($mUser);
    }
    
    public function getUserLoginName() {
        return $this->c_name;
        $mUser = $this->rUser;
        if($mUser){
            return $mUser->getFullName();
        }
        return '';
    }
    
    public function getContent() {
        return $this->content;
    }
    public function getContentWebView() {
        return nl2br($this->content);
    }
//    public function getCreatedDate() {
//        return MyFormat::dateConverYmdToDmy($this->created_date, "d/m/Y H:i");
//    }
    public function getUserReport() {
        $mUser = $this->rUserReport;
        if($mUser){
            return $mUser->getFullName();
        }
        return '';
    }
    
    /**
     * @Author: ANH DUNG Oct 06, 2016
     * handle view record
     * @param: $result, $q, $mUser
     * @param: $apiController api controller 
     */
    public function apiFormatRecord($q, $mUser, $needMore = array()) {
        $temp = array();
        $temp['id']             = $this->id;
        $temp['code_no']        = $this->code_no;
        $temp['full_name']      = $this->getUserLoginName();
        $temp['content']        = $this->getContent();
        $temp['created_date']   = $this->getCreatedDate();
        $temp['user_report']    = $this->getUserReport();
        $temp['images']         = GasFile::apiGetFileUpload($this->rFile);

        return $temp;
    }
    
    /**
    * @Author: ANH DUNG Oct 06, 2016
    * @Todo: get listing issue
    * @param: $q object post params
    * @param:  $mUser model user created
    */
    public function apiListing($q, $mUser) {
        $UserRole = $mUser->role_id;
        $criteria = new CDbCriteria();
        $criteria->compare('t.uid_login', $mUser->id);
        if(isset($q->date_from)){
            $this->date_from = $q->date_from;
        }
        if(isset($q->date_to)){
            $this->date_to = $q->date_to;
        }
        
        if(!empty($this->date_from)){
            $date_from = MyFormat::dateDmyToYmdForAllIndexSearch($this->date_from);
            $criteria->addCondition("t.created_date_only >= '$date_from'");
        }
        if(!empty($this->date_to)){
            $date_to = MyFormat::dateDmyToYmdForAllIndexSearch($this->date_to);
            $criteria->addCondition("t.created_date_only <= '$date_to'");
        }
        
        $criteria->order = 't.id DESC';
        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
            'pagination'=>array(
                'pageSize' => GasAppOrder::API_LISTING_ITEM_PER_PAGE,
//                'pageSize' => 2,
                'currentPage' => (int)$q->page,
            ),
          ));
//        return $dataProvider;
    }
    
    /**
     * @Author: ANH DUNG Nov 02, 2016
     * @Todo: fix update c_name
     */
    public static function updateFullName() {
        $models = self::model()->findAll();
        foreach($models as $model){
            $model->validate();
            $model->update(array('c_name'));
        }
        die;
    }
    
    
    /**
     * @Author: ANH DUNG Nov 02, 2016
     * @Todo: get list role id name for search
     */
    public function getListdataRole() {
        return array(
            ROLE_SALE => 'Nhân Viên Kinh Doanh',
            ROLE_ACCOUNTING_ZONE => 'Kế Toán Khu Vực',
            ROLE_MONITORING => 'NV Giám Sát',
            ROLE_MONITORING_MARKET_DEVELOPMENT => 'Chuyên Viên CCS ',
            ROLE_CHIEF_MONITOR => 'Tổng giám sát',
//            ROLE_SALE => '',
//            ROLE_SALE => '',
//            ROLE_SALE => '',
//            ROLE_SALE => '',
//            ROLE_SALE => '',
        );
    }
    
    /** @Author: KHANH TOAN 21 Feb
    *   @Todo: get html file view on grid
    */
    public function getFileOnly() {
        $res = '';
        $cmsFormater = new CmsFormatter();
        $res .= $cmsFormater->formatBreakTaskDailyFile($this, array('width'=>30, 'height'=>30));
        return $res;
    }
    
    /** @Author: DungNT Jun 17, 2019
     *  @Todo: xử lý 1 số user báo cáo CV có thể cập nhật IP sang promotion
     **/
    public function updateIpLimitPromotion($mUser) {
        $mAppPromotion = new AppPromotion();
        $aUserIdAndPromotion = $mAppPromotion->getArrayUserUpdateIpLimit();
        if(!array_key_exists($mUser->id, $mAppPromotion->getArrayUserUpdateIpLimit())){
            return ;
        }
        $mAppPromotion  = AppPromotion::model()->findByPk($aUserIdAndPromotion[$mUser->id]);
        $ip_address     = MyFormat::getIpUser();
        $mAppPromotion->updateIpLimit($ip_address, $mUser->id);
    }
}