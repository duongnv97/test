<?php

/**
 * This is the model class for table "{{_gas_materials_opening_balance}}".
 *
 * The followings are the available columns in table '{{_gas_materials_opening_balance}}':
 * @property string $id
 * @property integer $agent_id
 * @property integer $materials_id
 * @property integer $qty
 */
class GasMaterialsOpeningBalance extends CActiveRecord
{
    
    const YEAR_OPENING = 2013;

    public static function model($className=__CLASS__)
    {
            return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
            return '{{_gas_materials_opening_balance}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
            // NOTE: you should only define rules for those attributes that
            // will receive user inputs.
            return array(
                    array('agent_id, materials_id, qty', 'numerical', 'integerOnly'=>true),
                    // The following rule is used by search().
                    // Please remove those attributes that should not be searched.
                    array('id, agent_id, materials_id, qty, type', 'safe'),
            );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
            // NOTE: you may need to adjust the relation name and the related
            // class name for the relations automatically generated below.
            return array(
                'rAgent' => array(self::BELONGS_TO, 'Users', 'agent_id'),
                'rMaterial' => array(self::BELONGS_TO, 'GasMaterials', 'materials_id'),
            );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
            return array(
                    'id' => 'ID',
                    'agent_id' => 'Agent',
                    'materials_id' => 'Materials',
                    'qty' => 'Qty',
            );
    }

    public function search()
    {
            $criteria=new CDbCriteria;
            $criteria->compare('t.id',$this->id,true);
            $criteria->compare('t.agent_id',$this->agent_id);
            $criteria->compare('t.materials_id',$this->materials_id);
            $criteria->compare('t.qty',$this->qty);

            return new CActiveDataProvider($this, array(
                'criteria'=>$criteria,
                'pagination'=>array(
                    'pageSize'=> Yii::app()->user->getState('pageSize',Yii::app()->params['defaultPageSize']),
                ),
            ));
    }

    public function defaultScope()
    {
            return array(
                    //'condition'=>'',
            );
    }

    public static function deleteByAgent($agentId){
        $criteria = new CDbCriteria();
        $criteria->compare('agent_id', $agentId);
        $criteria->compare('year', GasMaterialsOpeningBalance::YEAR_OPENING);
        GasMaterialsOpeningBalance::model()->deleteAll($criteria);
    }

    /**
    * @Author: ANH DUNG 12-18-2013
    * @Todo: saveOpeningBalance material and qty 
    */        
    public static function saveOpeningBalance($agentId){
        $aRowInsert = array();
        $tableName = GasMaterialsOpeningBalance::model()->tableName();
        $year = GasMaterialsOpeningBalance::YEAR_OPENING;
        $aMaterial = GasMaterials::getArrAllObjMaterial();
        foreach($_POST['GasMaterialsOpeningBalance']['materials_id'] as $material_id=>$qty){
            if( (double)$qty>0 ){
                $materials_type_id = $aMaterial[$material_id]->materials_type_id;
                $aRowInsert[]="('$agentId',
                    '$material_id',
                    '$materials_type_id',
                    '$year',
                    '$qty'
                    )";     
            }
        }
        if(count($aRowInsert)<1) return;
        $sql = "insert into $tableName (agent_id,
                        materials_id,
                        materials_type_id,
                        year,
                        qty
                        ) values ".implode(',', $aRowInsert);            
        Yii::app()->db->createCommand($sql)->execute();
    }

    // dùng ở view gasstorecard/Materials_opening_balance
    public static function getOpeningBalanceByAgent($agentId){
        $criteria = new CDbCriteria();
        $criteria->compare('agent_id', $agentId);
        $year = GasMaterialsOpeningBalance::YEAR_OPENING;
        $criteria->compare("t.year", $year);
        $models = GasMaterialsOpeningBalance::model()->findAll($criteria);
        return CHtml::listData($models,'materials_id','qty');  
    }        
    
    public static function loadModel($year, $agent_id, $materials_id){
        $criteria = new CDbCriteria();
        $criteria->compare("t.year", $year);
        $criteria->compare("t.agent_id", $agent_id);
        $criteria->compare("t.materials_id", $materials_id);
        return self::model()->find($criteria);        
    }

    /**
     * @Author: ANH DUNG Jun 01, 2014
     * @Todo: add new record for new year agent if new import is add
     */
    public static function addNewRecordImport($year, $agent_id, $materials_id, $import){
        if(empty($year) ||  $year<1) return;
        $model = new GasMaterialsOpeningBalance();
        $model->year = $year;
        $model->agent_id = $agent_id;
        $model->materials_id = $materials_id;
        if($model->rMaterial){
            $model->materials_type_id = $model->rMaterial->materials_type_id;
        }
        $model->import = $import;
        $model->export = 0;
        $model->qty = $model->import-$model->export;
        $model->save();
    }
    
    /**
     * @Author: ANH DUNG Jun 01, 2014
     * @Todo: add new record for new year agent if new export is add
     */
    public static function addNewRecordExport($year, $agent_id, $materials_id, $export){
        if(empty($year) ||  $year<1) return;
        $model = new GasMaterialsOpeningBalance();
        $model->year = $year;
        $model->agent_id = $agent_id;
        $model->materials_id = $materials_id;
        if($model->rMaterial){
            $model->materials_type_id = $model->rMaterial->materials_type_id;
        }
        $model->import = 0;
        $model->export = $export;
        $model->qty = $model->import-$model->export;
        $model->save();
    }
            
    /**
     * @Author: ANH DUNG Jun 01, 2014
     * @Todo: cộng import đại lý khi phát sinh mới hoặc cập nhật thẻ kho 
     * @Param: $year
     * @Param: $agent_id mã DL
     * @Param: $materials_id mã vật tư
     * @Param: $import số lượng import
     * dùng ở khi phát sinh mới hoặc cập nhật thẻ kho
     */
    public static function addAgentImport($year, $agent_id, $materials_id, $import){
        return ; 
        /**  Aug 21, 2016 BIG CHANGE
         *  Sẽ không update liên tục vào table tồn kho GasMaterialsOpeningBalance này nữa
         * vì cảm thấy bị vô nghĩa khi không sử dụng dữ liệu này tức thời.
         * cuối năm sẽ chạy hàm UpdateSql::Agent2014UpdateImportExport để cộng đúng tồn kho cho năm hiện tại
         * nếu user update thẻ kho 2015 có thể sẽ phải chạy lại hàm Agent2014UpdateImportExport
         * chỗ này chú ý để cho đúng tồn kho khi sửa dữ liệu của năm trước
         */
        $model = self::loadModel($year, $agent_id, $materials_id);
        if(is_null($model)){
            self::addNewRecordImport($year, $agent_id, $materials_id, $import);
        }else{
            $model->import += $import;
            $model->qty = $model->import-$model->export;
            $model->update(array('import', 'qty'));
        }
    }
    
    /**
     * @Author: ANH DUNG Jun 01, 2014
     * @Todo: trừ import đại lý khi phát sinh mới hoặc cập nhật thẻ kho 
     * @Param: $year
     * @Param: $agent_id mã DL
     * @Param: $materials_id mã vật tư
     * @Param: $import số lượng import
     * dùng ở khi phát sinh mới hoặc cập nhật thẻ kho
     */
    public static function cutAgentImport($year, $agent_id, $materials_id, $import){
        return ; 
        /**  Aug 21, 2016 BIG CHANGE
         *  table này thiết kế để tính dc công nợ vỏ của KH, nhưng thực tế là chưa triển khai được
         * tồn đầu kỳ của KH là chưa có process để nhập vào, nên tạm thời ko thao tác add hay update vào đây nữa
         * khi nào triển khai theo dõi vỏ của KH thì có thể sẽ Open ra hoặc tìm cách khác upate cho đỡ phải truy xuất db nhiều
         * vì mỗi một thẻ kho có 10 item thì nó sẽ vào hàm này 10 lần => không nên làm vậy
         */
        $model = self::loadModel($year, $agent_id, $materials_id);
        if($model){
            $model->import -= $import;
            $model->qty = $model->import-$model->export;
            $model->update(array('import', 'qty'));
        }
    }
    
    /**
     * @Author: ANH DUNG Jun 01, 2014
     * @Todo: cộng export đại lý khi phát sinh mới hoặc cập nhật thẻ kho 
     * @Param: $year
     * @Param: $agent_id mã DL
     * @Param: $materials_id mã vật tư
     * @Param: $export số lượng export
     * dùng ở khi phát sinh mới hoặc cập nhật thẻ kho
     */
    public static function addAgentExport($year, $agent_id, $materials_id, $export){
        return ; 
        /**  Aug 21, 2016 BIG CHANGE
         * Sẽ không update liên tục vào table tồn kho GasMaterialsOpeningBalance này nữa
         * vì cảm thấy bị vô nghĩa khi không sử dụng dữ liệu này tức thời.
         * cuối năm sẽ chạy hàm UpdateSql::Agent2014UpdateImportExport để cộng đúng tồn kho cho năm hiện tại
         * nếu user update thẻ kho 2015 có thể sẽ phải chạy lại hàm Agent2014UpdateImportExport
         * chỗ này chú ý để cho đúng tồn kho khi sửa dữ liệu của năm trước
         */
        $model = self::loadModel($year, $agent_id, $materials_id);
        if(is_null($model)){
            self::addNewRecordExport($year, $agent_id, $materials_id, $export);
        }else{
            $model->export += $export;
            $model->qty = $model->import-$model->export;
            $model->update(array('export', 'qty'));
        }
    }
    
    /**
     * @Author: ANH DUNG Jun 01, 2014
     * @Todo: trừ export đại lý khi phát sinh mới hoặc cập nhật thẻ kho 
     * @Param: $year
     * @Param: $agent_id mã DL
     * @Param: $materials_id mã vật tư
     * @Param: $export số lượng export
     * dùng ở khi phát sinh mới hoặc cập nhật thẻ kho
     */
    public static function cutAgentExport($year, $agent_id, $materials_id, $export){
        return ; 
        /**  Aug 21, 2016 BIG CHANGE
         *  table này thiết kế để tính dc công nợ vỏ của KH, nhưng thực tế là chưa triển khai được
         * tồn đầu kỳ của KH là chưa có process để nhập vào, nên tạm thời ko thao tác add hay update vào đây nữa
         * khi nào triển khai theo dõi vỏ của KH thì có thể sẽ Open ra hoặc tìm cách khác upate cho đỡ phải truy xuất db nhiều
         * vì mỗi một thẻ kho có 10 item thì nó sẽ vào hàm này 10 lần => không nên làm vậy
         */
        $model = self::loadModel($year, $agent_id, $materials_id);
        if($model){
            $model->export -= $export;
            $model->qty = $model->import-$model->export;
            $model->update(array('export', 'qty'));
        }
    }
    
    
        
}