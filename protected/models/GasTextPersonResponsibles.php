<?php

/**
 * This is the model class for table "{{_text_person_responsibles}}".
 *
 * The followings are the available columns in table '{{_text_person_responsibles}}':
 * @property string $id                 Id of record
 * @property string $text_id            Id of text
 * @property string $user_id            Id of user
 * @property integer $amount            Amount money
 * @property integer $status            Status
 * @property string $created_date       Created date
 * @property string $created_by         Created user
 * 
 * The followings are the available model relations:
 * @property Users                  $rCreatedBy         User created this record
 * @property Users                  $rUser              User own this debt
 * @property GasText                $rGasText           GasText model
 */
class GasTextPersonResponsibles extends BaseSpj {
    //-----------------------------------------------------
    // Status
    //-----------------------------------------------------
    const STATUS_INTACTIVE              = 0;
    const STATUS_ACTIVE                 = 1;
    
    //-----------------------------------------------------
    // Properties
    //-----------------------------------------------------
    public $autocomplete_user;
    public $autocomplete_gas_text;

    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return GasTextPersonResponsibles the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return '{{_text_person_responsibles}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        return array(
            array('text_id, user_id', 'required'),
            array('amount, status', 'numerical', 'integerOnly' => true),
            array('text_id, user_id', 'length', 'max' => 11),
            array('created_by', 'length', 'max' => 10),
            array('id, text_id, user_id, amount, status, created_date, created_by', 'safe'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        return array(
            'rUser' => array(self::BELONGS_TO, 'Users', 'user_id'),
            'rCreatedBy' => array(self::BELONGS_TO, 'Users', 'created_by'),
            'rGasText' => array(
                self::BELONGS_TO, 'GasText', 'text_id',
            ),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id'            => 'ID',
            'text_id'       => 'Số hiệu văn bản',
            'user_id'       => 'Người chịu trách nhiệm',
            'amount'        => 'Số tiền',
            'status'        => 'Trạng thái',
            'created_date'  => 'Ngày tạo',
            'created_by'    => 'Người tạo',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search() {
        $criteria = new CDbCriteria;

        $criteria->compare('t.id', $this->id, true);
        $criteria->compare('t.text_id', $this->text_id, true);
        $criteria->compare('t.user_id', $this->user_id, true);
        $criteria->compare('t.amount', $this->amount);
        $criteria->compare('t.status', $this->status);
        $criteria->compare('t.created_date', $this->created_date, true);
        $criteria->compare('t.created_by', $this->created_by, true);
        $criteria->order = 't.id DESC';
        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
            'pagination' => array(
                'pageSize' => Yii::app()->user->getState('pageSize', Yii::app()->params['defaultPageSize']),
            ),
        ));
    }
    //-----------------------------------------------------
    // Override methods
    //-----------------------------------------------------
    /**
     * Before save
     * @return boolean Parent methods return
     */
    public function beforeSave() {
        if ($this->isNewRecord) {
            if (empty($this->created_by)) {
                $this->created_by = MyFormat::getCurrentUid();
            }
            
            // Handle created date
            $this->created_date = CommonProcess::getCurrentDateTime();
        }
        
        return parent::beforeSave();
    }
    
    /**
     * Can update
     * @return boolean
     */
    public function canUpdate() {
        return true;
    }
    
    //-----------------------------------------------------
    // Utility methods
    //-----------------------------------------------------
    public function getUserName() {
        if (isset($this->rUser)) {
            return $this->rUser->getFullName();
        }
        return '';
    }
    
    /**
     * Get amount formated
     * @return String
     */
    public function getAmount() {
        return CmsFormatter::formatCurrency($this->amount);
    }
    
    /**
     * Get created user
     * @return string
     */
    public function getCreatedBy() {
        if (isset($this->rCreatedBy)) {
            return $this->rCreatedBy->getFullName();
        }
        return '';
    }
    
    /**
     * Get gas text information
     * @return String URL to gas text view
     */
    public function getGasTextInfo() {
        $retVal = ''; $name = '';
        if (isset($this->rGasText)) {
            $retVal = Yii::app()->createAbsoluteUrl('admin/gasText/text_view', array(
                'id'    => $this->rGasText->id,
            ));
            $name = $this->rGasText->code_no;
        }
        return '<a href="' . $retVal . '">' . $name . '</a>';
    }
    
    //-----------------------------------------------------
    // Static methods
    //-----------------------------------------------------
    /**
     * Get status array
     * @return Array Array status of debt
     */
    public static function getStatus() {
        return array(
            self::STATUS_INTACTIVE      => 'Inactive',
            self::STATUS_ACTIVE         => 'Hoạt động',
        );
    }

}
