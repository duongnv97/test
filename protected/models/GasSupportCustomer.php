<?php

/**
 * This is the model class for table "{{_gas_support_customer}}".
 *
 * The followings are the available columns in table '{{_gas_support_customer}}':
 * @property string $id
 * @property string $code_no
 * @property string $date_request
 * @property string $customer_id
 * @property integer $type_customer
 * @property string $materials_id
 * @property integer $type
 * @property string $approved_uid_level_1
 * @property string $approved_date_level_1
 * @property string $approved_note_level_1
 * @property string $approved_uid_level_2
 * @property string $approved_date_level_2
 * @property string $approved_note_level_2
 * @property string $uid_login
 * @property string $created_date
 * @property string $sale_id
 * @property string $deloy_by
 */
class GasSupportCustomer extends BaseSpj
{
    public $autocomplete_name, $autocomplete_name_second, $autocomplete_name_five, $item_name, $type, $schedule, $from_cron_mail = 0;
    
    /** @Author: Pham Thanh Nghia 2018
     *  @Todo: ExportExcel
     *  @Param:
     **/
    public $sumAmountHoTro = 0, $sumAmountDauTu = 0;
    public $approved_uid, $approved_date; // người duyệt, ngày duyệt cao nhất
    // tìm kiếm theo ngày
    public $date_created_from;
    public $date_created_to;
    public $all; // xem search hết tất cả
    ///////////////////////////////////////////
    
    
        // 1: cho muon, 2: tang, 3: ban
    const TYPE_MUON = 1;
    const TYPE_TANG = 2;
    const TYPE_BAN = 3;
    public static $ARR_TYPE = array(
        GasSupportCustomer::TYPE_MUON   => 'Cho Mượn',
        GasSupportCustomer::TYPE_TANG   => 'Tặng',
        GasSupportCustomer::TYPE_BAN    => 'Bán',
    );
    const STATUS_NEW = 1;
    const STATUS_APPROVED_1 = 2;
    const STATUS_APPROVED_2 = 3;
    const STATUS_REJECT = 4;
    const STATUS_APPROVED_3 = 5; // Sep 15, 2015 - Anh Long Duyệt cuối
    const STATUS_COMPLETE = 6; // đã Hoàn Thành Sep 30, 2015
    const STATUS_NOT_DOING = 7; // Không thực hiện
    
    public static $ARR_APPROVED_1 = array(
        GasSupportCustomer::STATUS_APPROVED_1   => 'Duyệt Lần 1',
        GasSupportCustomer::STATUS_REJECT       => 'Hủy Bỏ',
    );
    
    /** truong phong kd/giam doc kd
     KH bò thì gửi cho TRưởng pHÒng Bò, Mối thì gửi cho Trưởng PHòng Mối
     Trưởng phòng KD bò-mối duyệt rồi mới lên Giám đốc KD duyệt
     * @note: 1/ check user nào dc phép duyệt lần 1, 2
     */
    public static $ARR_APPROVED_2 = array(
        GasSupportCustomer::STATUS_APPROVED_2   => 'Duyệt Lần 2',
        GasSupportCustomer::STATUS_REJECT       => 'Hủy Bỏ',
    );
    
    // Sep 15, 2015 - Anh Long Duyệt cuối
    public static $ARR_APPROVED_3 = array(
        GasSupportCustomer::STATUS_APPROVED_3   => 'Duyệt Lần 3',
        GasSupportCustomer::STATUS_REJECT       => 'Hủy Bỏ',
    );
    public static $ARR_APPROVED_4 = array(
        GasSupportCustomer::STATUS_COMPLETE     => 'Hoàn Thành',
        GasSupportCustomer::STATUS_NOT_DOING    => 'Không thực hiện',
    );
    
    public static $ARR_APPROVED = array(
        GasSupportCustomer::STATUS_NEW          => 'Mới',
        GasSupportCustomer::STATUS_APPROVED_1   => 'Duyệt Lần 1',
        GasSupportCustomer::STATUS_APPROVED_2   => 'Duyệt Lần 2',
        GasSupportCustomer::STATUS_APPROVED_3   => 'Duyệt Lần 3',
        GasSupportCustomer::STATUS_COMPLETE     => 'Hoàn Thành',
        GasSupportCustomer::STATUS_REJECT       => 'Hủy Bỏ',
        GasSupportCustomer::STATUS_NOT_DOING    => 'Không thực hiện',
    );
    
    public static $ARR_NOT_ALLOW = array(
        GasSupportCustomer::STATUS_APPROVED_2,
        GasSupportCustomer::STATUS_APPROVED_3,
        GasSupportCustomer::STATUS_REJECT,
    );
    
    public static $ARR_NOT_ALLOW_LV2 = array(
        GasSupportCustomer::STATUS_APPROVED_3,
        GasSupportCustomer::STATUS_REJECT,
    );
    
    public static $ARR_NOT_SHOW_HEAD = array( // Những record mới tạo và reject không cho hiện
        GasSupportCustomer::STATUS_NEW,
        GasSupportCustomer::STATUS_REJECT,
    );
    
    public static $ROLE_UPDATE_COMPLETE = array(
        ROLE_ADMIN,
        ROLE_SUB_DIRECTOR,// Phó Giám Đốc Chi Nhánh  tạm thời để vậy đã - không có ý nghĩa gì, sẽ sửa sau, 
        ROLE_E_MAINTAIN, // NV bao tri
        ROLE_HEAD_OF_MAINTAIN, // Oct 21, 2015 Tổ Trưởng Tổ Bảo Trì - may them duong van chuyen - to truong to bao tri vao cap nhat hoan thanh giay giao nhiem vu
        ROLE_TECHNICAL, // Aug 30, 2015 Tổ Trưởng Tổ Bảo Trì - may them duong van chuyen - to truong to bao tri vao cap nhat hoan thanh giay giao nhiem vu
        ROLE_SUB_USER_AGENT,// Now 08, 2016 cho Dl tự hoàn thành
        ROLE_MONITORING_MARKET_DEVELOPMENT, 
        
    );
    
    public $MAX_ID;
    public $date_from;
    public $date_to;
    
    public $mDetail;
    public $aModelDetail;
    public static $aRoleLevel1 = array(ROLE_MONITORING_MARKET_DEVELOPMENT, ROLE_BRANCH_DIRECTOR, ROLE_HEAD_GAS_BO, ROLE_HEAD_GAS_MOI, ROLE_HEAD_OF_BUSINESS, ROLE_CHIEF_MONITOR, ROLE_HEAD_OF_MAINTAIN, ROLE_HEAD_TECHNICAL,ROLE_EXECUTIVE_OFFICER );
    public static $aRoleLevel2 = array( ROLE_DIRECTOR_BUSSINESS); // Apr 04, 17 chuyển ROLE_HEAD_TECHNICAL xuống duyệt cấp 1
    public static $aRoleLevel3 = array( ROLE_DIRECTOR); // Giám đốc Kinh Doanh
    public static $aRoleAllowCreate = array( ROLE_SALE, ROLE_SUB_USER_AGENT );// Jan 28, 2015 role user dc create new
    public static $aRoleNewCreate = array(ROLE_ADMIN, ROLE_TECHNICAL);// Jan 25, 2017 bổ sung ROLE_ACCOUNTING. chỗ này sẽ sửa lại là role của NV kỹ thuật
    public static $aRoleNewUpdate = array(ROLE_SALE, ROLE_MONITORING_MARKET_DEVELOPMENT, ROLE_EMPLOYEE_MAINTAIN);//Dec 19, 2015  xử lý cho GIÁM SÁT PTTT tạo nữa. Oct 06, 2015 xử lý cho NVPTTT tạo nữa,..
    public static $aRoleCreateExt1 = array(ROLE_EMPLOYEE_MARKET_DEVELOPMENT);// Jul 23, 2016 xử lý cho NV CCS tạo hỗ trợ cho KH hộ GD
    public static $aRoleLimitViewApproved = array(ROLE_SUB_DIRECTOR, ROLE_HEAD_OF_MAINTAIN, 
//        ROLE_CHIEF_ACCOUNTANT, ROLE_ACCOUNTING,// Jul 07, 2016 bỏ kế toán trong phần view approve 3, cho view all luon
        ROLE_CHIEF_ACCOUNTANT,
        ROLE_MONITORING, // Add Now 26, 2015 thêm chị Thủy vào xem approve 3
        ROLE_ACCOUNTING_ZONE, // Add Jul 03, 2016 thêm kt Khu Vuc vào xem approve 3
    );// 2 role view để update ngày thi công + kế toán+kt trưởng
    
    public $file_name, $mGasFile, $aIdDetailNotDelete, $file_design, $file_complete;
    const IMAGE_MAX_UPLOAD = 20;
    const IMAGE_MAX_UPLOAD_SHOW = 3;
    public static $AllowFilePdf = 'pdf,xls,xlsx,jpg,jpeg,png';
    const MAX_ITEM = 60;
    
    const UID_QUY_PV = 295085; //Tổ Trưởng Tổ Bảo Trì - Phan Văn Quý - Sep 15, 2015    
    const UID_CHUYEN_DV = 766822; //Tổ Trưởng Tổ Bảo Trì - Dương Văn Chuyền - Mien Tay - Jan 04, 2016
    const UID_DUNG_DN = 740905; //Thủ Kho - Đỗ Như Dũng - Sep 15, 2015
    // Close Oct 02, 2015 const UID_HUE_LT = 704693; //NV kế Toán - Lê Thị Huệ - Sep 19, 2015
    const UID_HUAN_DN = 195; //NV Bảo Trì - Đặng Ngọc Huấn - Sep 30, 2015
    const UID_TAM_PV = 296001; //NV Bảo Trì - Phạm Văn Tâm - Sep 30, 2015
    const UID_ANH_LTH = 777; //NV Giam Sat id that 710677 - Lê Trang Hoàng Anh- Mar 21, 2016
    // Now 12,  su dung cho phan dat hang KM
    const UID_THUY_MDX = 30890; //NV Giám Sát - Mai Đào Xuân Thủy - Now 12, 2015
    
    const CONFIRM_OK = 1;
    const CONFIRM_NOT_OK = 2;
    public $ARR_SALE_APPROVED = array(
        GasSupportCustomer::CONFIRM_OK      => 'Đã xác nhận',
        GasSupportCustomer::CONFIRM_NOT_OK  => 'Không xác nhận',
    );
    
    const TYPE_SUPPORT_BO_MOI = 1;// Jul 23, 2016 xử lý riêng cho CCS tạo đề xuất đầu tư KH hộ GD
    const TYPE_SUPPORT_HGD = 2;
    
    // 1/ Lắp mới, 2/ Sửa chữa, 3/ Di dời Jul 28, 2016
    const ACTION_LAP_MOI        = 1;
    const ACTION_SUA_CHUA       = 2;
    const ACTION_DI_DOI         = 3;
    const ACTION_KHAO_SAT       = 4;
    public function getListActionInvest() {
        return array(
            GasSupportCustomer::ACTION_LAP_MOI      => 'Lắp mới',
            GasSupportCustomer::ACTION_SUA_CHUA     => 'Sửa chữa',
            GasSupportCustomer::ACTION_DI_DOI       => 'Di dời',
            GasSupportCustomer::ACTION_KHAO_SAT     => 'Khảo sát',
        );
    }
    public function getActionInvest() {
        $aAction = $this->getListActionInvest();
        return isset($aAction[$this->action_invest]) ? $aAction[$this->action_invest] : '';
    }

    /**
     * @Author: ANH DUNG Sep 23, 2015
     * @Todo: chuyển sang cho chọn người duyệt trên giao diện cho đơn giản
     * không phải xử lý nhiều ở bên dưới.
     * @note: nếu lần sau có làm kiểu người duyệt thì nên cho User chọn ở trên giao diện cho đơn giản
     */
    public function getListUidApproved() {
        $aModelUser = GasOneManyBig::getArrayModelUser(GasOneManyBig::TYPE_APPROVE_SUPPORT_CUSTOMER, GasOneManyBig::TYPE_APPROVE_SUPPORT_CUSTOMER);
        $aRes = array();
        foreach ($aModelUser as $item) {
            $aRes[$item->id] = $item->getNameWithRole();
        }
        return $aRes;
        
//        return array(
//            GasLeave::UID_HEAD_GAS_BO => "Phạm Văn Đức - Trưởng Phòng KD Gas Bò",
//            GasLeave::UID_HEAD_GAS_MOI => "Đoàn Ngọc Hải Triều - Trưởng Phòng KD Gas Mối",
//            GasLeave::UID_DIRECTOR_BUSSINESS => "Bùi Đức Hoàn - Giám Đốc Kinh Doanh",
//            GasLeave::UID_DIRECTOR => "Vũ Thái Long - Giám Đốc",            
//            GasLeave::UID_MANAGER_BUSSINESS => "Nguyễn Thanh Hải -Trưởng Phòng Kinh Doanh",
//            GasLeave::UID_CHIEF_MONITOR => "Trần Trung Hiếu - Tổng Giám Sát",
//            GasLeave::UID_QUY_PV => "Phan Văn Quý - Tổng Giám Sát",
//        );
    }
    // Dec 18, 2015 những role không phải bò mối, dc cho approved level 1
    // để giới hạn cho phấn list index
    public $ROLE_APPROVED_NEW = array(
        ROLE_HEAD_OF_BUSINESS, // Trưởng Phòng Kinh Doanh Dec 26, 2014
        ROLE_CHIEF_MONITOR, // Tổng giám sát 
    );
    // Dec 27, 2014 lấy số ngày cho phép đại lý cập nhật
    public static function getDayAllowUpdate(){
        return Yii::app()->params['days_update_support_customer'];
    }
    
    /** @Author: ANH DUNG Sep 30, 2015
     *  @Todo: something
     */
    public function getUIdNotify() {
        return array(
//            GasSupportCustomer::UID_QUY_PV,// vì user này đã nằm trong người thực hiện nên không phải set ở đây nữa
            GasSupportCustomer::UID_DUNG_DN,
            GasSupportCustomer::UID_HUAN_DN,
            GasSupportCustomer::UID_TAM_PV,
        );
    }
    
    /**
     * @Author: ANH DUNG Sep 02, 2015
     */
    public function getDayUpdateToPrint(){
        return Yii::app()->params['days_update_support_customer_to_print'];
    }    
    
    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return GasSupportCustomer the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{_gas_support_customer}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        return array(
            array('action_invest, sale_approved, to_uid_approved, deloy_by, contact_person, contact_tel, time_doing, price_qty', 'required', 'on'=>'sale_update'),
            array('customer_id', 'required', 'on'=>'tech_create, tech_update'),
            array('customer_id, to_uid_approved, deloy_by', 'required', 'on'=>'ccs_create, ccs_update'),
            array('action_invest, to_uid_approved, customer_id, deloy_by, contact_person, contact_tel, time_doing, price_qty', 'required', 'on'=>'sub_agent_create, sub_agent_update'),
            array('time_doing_real', 'required', 'on'=>'head_update_time_real'),
            
            array('type_support, status, id, code_no, date_request, customer_id, type_customer, materials_id, type, approved_uid_level_1, approved_date_level_1, approved_note_level_1, approved_uid_level_2, approved_date_level_2, approved_note_level_2, uid_login, created_date', 'safe'),
            array('chk_design_wrong, chk_material_not_enough, chk_deploy_wrong, deloy_by, contact_tel, time_doing, price_qty, contact_person, customer_agent_id, note, date_from,date_to', 'safe'),
            array('approved_uid_level_3, approved_date_level_3, approved_note_level_3', 'safe'),
            array('approved_uid_level_4, approved_date_level_4, approved_note_level_4', 'safe'),
            array('from_cron_mail, action_invest, note_update_real_time, schedule, sale_approved, note_technical, time_doing_real, to_uid_approved, sale_id', 'safe'),
            array('to_uid_approved, status', 'required', 'on'=>'Update_status'),
            array('status', 'CheckCanUpdateStatus', 'on'=>'Update_status'),
            array('date_created_from, date_created_to, all', 'safe'),
//            array('contact_person, contact_tel, time_doing, price_qty', 'required', 'on'=>'update_to_print'),
        );
    }
    
    /**
     * @Author: ANH DUNG Dec 27, 2014
     * @Todo: kiểm tra user update status co hop le khong
     */
    public function CheckCanUpdateStatus($attribute,$params)
    {
        $aRoleCheck = array( ROLE_HEAD_GAS_BO, ROLE_HEAD_GAS_MOI ); // TRưởng pHÒng Bò, Mối
        $cRole = Yii::app()->user->role_id;
        $cUid = Yii::app()->user->id;
        $ok = true;
        if(in_array($cRole, $aRoleCheck) && $this->status == GasSupportCustomer::STATUS_APPROVED_2 ){
            $ok = false;
        }
        if( $this->status == GasSupportCustomer::STATUS_NEW ){
            $ok = false;
        }
        
        if(!$ok){
            $this->addError('status','Trạng thái không hợp lệ.');
        }
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        return array(
            'rUidLogin' => array(self::BELONGS_TO, 'Users', 'uid_login'),
            'rCustomer' => array(self::BELONGS_TO, 'Users', 'customer_id'),
            'rApprovedUidLevel1' => array(self::BELONGS_TO, 'Users', 'approved_uid_level_1'),
            'rApprovedUidLevel2' => array(self::BELONGS_TO, 'Users', 'approved_uid_level_2'),
            'rApprovedUidLevel3' => array(self::BELONGS_TO, 'Users', 'approved_uid_level_3'),
            'rApprovedUidLevel4' => array(self::BELONGS_TO, 'Users', 'approved_uid_level_4'),
            'rUidApproved' => array(self::BELONGS_TO, 'Users', 'to_uid_approved'),
            'rDetail' => array(self::HAS_MANY, 'GasSupportCustomerItem', 'support_customer_id',
                'order'=>'rDetail.id ASC',
            ),
            'rDeloyBy' => array(self::BELONGS_TO, 'Users', 'deloy_by'),
            'rFile' => array(self::HAS_MANY, 'GasFile', 'belong_id',
                'on'=> "rFile.type=".GasFile::TYPE_4_SUPPORT_CUSTOMER_COMPLETE ,'order'=>'rFile.id ASC',
            ),
            
            'rApprovedUid' => array(self::BELONGS_TO, 'Users', 'approved_uid'), // người duyệt cao nh
            
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'code_no' => 'Mã Số',
            'date_request' => 'Ngày',
            'customer_id' => 'Khách Hàng',
            'type_customer' => 'Loại KH',
            'materials_id' => 'Tên thiết bị',
            'type' => 'Loại',
            'approved_uid_level_1' => 'Duyệt lần 1',
            'approved_date_level_1' => 'Ngày Duyệt lần 1',
            'approved_note_level_1' => 'Ghi chú lần 1',
            'approved_uid_level_2' => 'Duyệt lần 2',
            'approved_date_level_2' => 'Ngày Duyệt lần 2',
            'approved_note_level_2' => 'Ghi Chú lần 2',
            'approved_uid_level_3' => 'Duyệt lần 3',
            'approved_date_level_3' => 'Ngày Duyệt lần 3',
            'approved_note_level_3' => 'Ghi Chú lần 3',
            
            'approved_uid_level_4' => 'Hoàn Thành',
            'approved_date_level_4' => 'Ngày Cập Nhật Hoàn Thành',
            'approved_note_level_4' => 'Ghi Chú Hoàn Thành',
            
            'uid_login' => 'Người Tạo',
            'created_date' => 'Ngày Tạo',
            'date_from' => 'Từ Ngày',
            'date_to' => 'Đến Ngày',
            'status' => 'Trạng Thái',
            'note' => 'Ghi Chú',
            'contact_person' => 'Người liên hệ',
            'contact_tel' => 'Tel',
            'time_doing' => 'Thời gian thi công đề xuất',
            'price_qty' => 'Nhóm giá & Sản lượng',
            'deloy_by' => 'Đơn vị thực hiện',
            'file_design' => 'File bản vẽ thiết kế và danh mục vật tư',
            'item_name' => 'Tên thiết bị',
            'to_uid_approved' => 'Chọn Người Duyệt Cấp Trên',
            'time_doing_real' => 'Thời gian thi công thực tế',
            'sale_approved' => 'Xác nhận bản vẽ',
            'note_technical' => 'NV kỹ thuật ghi chú',
            'note_update_real_time' => 'Ghi chú thi công',
            'file_complete'=>'File Scan biên bản nghiệm thu',
            "chk_design_wrong" => "Thiết kế không đúng",
            "chk_material_not_enough" => "Vật tư không đủ",
            "chk_deploy_wrong" => "Giao không đúng ngày",
            "action_invest" => "Loại đầu tư",
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        $cRole = Yii::app()->user->role_id;
        $cUid = Yii::app()->user->id;
        $cAgentId = MyFormat::getAgentId();
        $sStatusNotShow = implode(',', self::$ARR_NOT_SHOW_HEAD);
        $aUidNotifySupport = $this->getUIdNotify();
        $criteria=new CDbCriteria;

        $criteria->compare('t.action_invest',$this->action_invest);
        $criteria->compare('t.code_no',$this->code_no,true);            
        $criteria->compare('t.customer_id',$this->customer_id);
        $criteria->compare('t.type_customer',$this->type_customer);
        $criteria->compare('t.status',$this->status);
        $criteria->compare('t.type_support',$this->type_support);
        // all để xét search tât cả status
        if(empty($this->all) && $this->status != GasSupportCustomer::STATUS_COMPLETE){
            $criteria->addCondition("t.status<>".GasSupportCustomer::STATUS_COMPLETE);
        }
        
        if(!empty($this->schedule)){// Oct 06, 2015 xử lý list lịch thi công của kỹ thuật
            $criteria->compare('t.status', GasSupportCustomer::STATUS_APPROVED_3);
            $criteria->addCondition("t.time_doing_real<>'0000-00-00 00:00:00' AND t.time_doing_real IS NOT NULL ");
        }
        
        if(!empty($this->deloy_by)){// Oct 06, 2015 xử lý list lịch thi công của kỹ thuật
            $criteria->compare('t.deloy_by', $this->deloy_by, true);
        }
        if(!empty($this->chk_design_wrong)){// Dec 29, 2015 
            switch ($this->chk_design_wrong) {
                case self::DOING_DESIGN_WRONG:
                    $criteria->compare('t.chk_design_wrong', 1);
                    break;
                case self::DOING_MATERIAL_NOT_ENOUGH:
                    $criteria->compare('t.chk_material_not_enough', 1);
                    break;
                case self::DOING_DEPLOY_WRONG:
                    $criteria->compare('t.chk_deploy_wrong', 1);
                    break;
            }
        }
        
        $criteria->compare('t.approved_uid_level_1',$this->approved_uid_level_1);
        $criteria->compare('t.approved_uid_level_2',$this->approved_uid_level_2);            
        $criteria->compare('t.uid_login',$this->uid_login);
        $criteria->order = "t.id DESC";
        $with = false;
        if(!empty($this->type)){
            $with = true;
            $criteria->compare('rDetail.type',$this->type);
        }
        if($with){
            $criteria->with = array('rDetail');
            $criteria->together = true;
        }
        
        $date_from = '';
        $date_to = '';
        if(!empty($this->date_from)){
            $date_from = MyFormat::dateDmyToYmdForAllIndexSearch($this->date_from);
        }
        if(!empty($this->date_to)){
            $date_to = MyFormat::dateDmyToYmdForAllIndexSearch($this->date_to);
        }
        
//        if(!empty($date_from) && empty($date_to))
//                $criteria->addCondition("t.created_date>='$date_from'");
//        if(empty($date_from) && !empty($date_to))
//                $criteria->addCondition("t.created_date<='$date_to'");
//        if(!empty($date_from) && !empty($date_to))
//                $criteria->addBetweenCondition("t.created_date",$date_from,$date_to);

        if($this->type_support == self::TYPE_SUPPORT_BO_MOI){
            $this->handleRoleForBoMoi($criteria, $cRole, $cUid, $aUidNotifySupport, $sStatusNotShow);
        }else{
            $this->handleRoleForHgd($criteria, $cRole, $cUid, $aUidNotifySupport, $sStatusNotShow);
        }

        if(!empty($date_from)){
            $criteria->addCondition("t.approved_date_level_1 LIKE :DATE_FROM OR t.approved_date_level_2 LIKE :DATE_FROM OR t.approved_date_level_3 LIKE :DATE_FROM");
            $criteria->params[':DATE_FROM'] = "%".$date_from."%";
        }
        //        if(count($criteria->params)){}
        
        // Yêu cầu thêm tìm kiếm từ ngày đến ngày của chọ Châu
        if(!empty($this->date_created_from)){
            $date_created_from = MyFormat::dateDmyToYmdForAllIndexSearch($this->date_created_from);
        }
        if(!empty($this->date_created_to)){
            $date_created_to = MyFormat::dateDmyToYmdForAllIndexSearch($this->date_created_to);
        }
        if(!empty($date_created_from) ){
            $criteria->addCondition("t.created_date>='$date_created_from'");
        }
        if(!empty($date_created_to)){
            $criteria->addCondition("t.created_date<='$date_created_to'");
        }
        //
         $_SESSION['data-excel'] = new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
            'pagination'=> false
        ));

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
            'pagination'=>array(
                'pageSize'=> Yii::app()->user->getState('pageSize',Yii::app()->params['defaultPageSize']),
            ),
        ));
    }

    /**
     * @Author: ANH DUNG Jul 23, 2016
     * @Todo: xử lý search list LIMIT role cho KH bò mối
     */
    public function handleRoleForBoMoi(&$criteria, $cRole, $cUid, $aUidNotifySupport, $sStatusNotShow) {
        $sStatusReject  = GasSupportCustomer::STATUS_REJECT;
        $aUidViewAll    = array(GasConst::UID_PHONG_HT, GasLeave::UID_DIEU_NTM,
            GasConst::UID_MINH_NT, GasLeave::PHUC_HV, 
            GasConst::UID_TRUONG_NN,
            GasConst::UID_DOT_NV, 
            GasConst::UID_SU_BH, 
            1242592, // Telesale - Nguyễn Thị Thanh Thủy 
        );
        
        if( $cRole == ROLE_HEAD_GAS_BO ){
//            $criteria->compare('t.type_customer', STORE_CARD_KH_BINH_BO);
            $uidQuyPv   = GasSupportCustomer::UID_QUY_PV;
            $uidDungBv  = GasLeave::UID_HEAD_TECHNICAL;
            $criteria->addCondition("t.to_uid_approved=$cUid OR t.to_uid_approved=$uidQuyPv OR t.to_uid_approved=$uidDungBv OR t.approved_uid_level_1=$uidQuyPv OR t.approved_uid_level_1=$uidDungBv OR t.approved_uid_level_1=$cUid OR t.uid_login = $cUid ");
            $criteria->addCondition("t.status <>$sStatusReject" );
        }elseif(in_array($cUid, $aUidViewAll) || $cRole == ROLE_ACCOUNTING){// Oct 26, 2016 xử lý cho 1 số user view all
            
        }elseif($cRole == ROLE_EXECUTIVE_OFFICER){// Oct 26, 2016 xử lý cho 1 số user view all
            $criteria->addCondition("t.to_uid_approved=$cUid OR t.approved_uid_level_1=$cUid OR t.uid_login = $cUid ");
            $criteria->addCondition("t.status <>$sStatusReject" );
        }elseif( $cRole == ROLE_HEAD_OF_MAINTAIN ){
            
//        }elseif(in_array($cRole, $this->ROLE_APPROVED_NEW) ){// Dec 18, 2015 fix cho anh Tổng giám sát vào phần duyệt level 1
        }elseif(in_array($cRole, GasSupportCustomer::$aRoleLevel1) ){// Dec 18, 2015 fix cho anh Tổng giám sát vào phần duyệt level 1
            if(!isset($_GET['status']) && !isset($_GET['schedule'])){
                $criteria->addCondition("t.to_uid_approved=$cUid OR t.approved_uid_level_1=$cUid OR t.uid_login = $cUid ");
            }
        }elseif( $cRole == ROLE_DIRECTOR_BUSSINESS ){
            $criteria->addCondition("(t.to_uid_approved=$cUid OR t.approved_uid_level_2=$cUid OR t.uid_login = $cUid ) " );
            $criteria->addCondition("t.status <>$sStatusReject" );
        }elseif( $cRole == ROLE_DIRECTOR ){
            $criteria->addCondition("(t.to_uid_approved=$cUid OR t.approved_uid_level_3=$cUid) " );
//            $criteria->addCondition("t.status <>$sStatusReject" );
        }elseif( ($cUid == GasConst::UID_VINH_NH || in_array($cRole, GasSupportCustomer::$aRoleNewCreate) 
                || in_array($cRole, GasSupportCustomer::$aRoleCreateExt1))&& $cRole!= ROLE_ADMIN){
            $criteria->compare('t.uid_login', $cUid);
        }elseif(in_array($cRole, GasSupportCustomer::$aRoleNewUpdate) ){// Oct 06, 2015 xử lý cho NVPTTT tạo nữa,...
            if(!empty($this->schedule)){// Oct 06, 2015 xử lý list lịch thi công của kỹ thuật
                $criteria->compare('t.status', GasSupportCustomer::STATUS_APPROVED_3);
                $criteria->addCondition("t.time_doing_real<>'0000-00-00 00:00:00' AND t.time_doing_real IS NOT NULL ");
            }else{
                $criteria->addCondition("t.sale_id = $cUid OR t.uid_login = $cUid OR t.to_uid_approved=$cUid");// Dec 17, 2015 thêm uid_login cho NV PTTT nhìn thấy nữa, vì PTTT dc tạo phiếu giao NV
            }
            
        }elseif( $cRole == ROLE_SUB_USER_AGENT ){
//            $deloy_by = $cUid;// Jun2119 DungNT close
//            $criteria->addCondition("t.uid_login = $cUid OR t.deloy_by LIKE :AGENT_ID ");
//            $criteria->params[':AGENT_ID'] = "%".$deloy_by."%";// vì là user sub agent nên sẽ lấy $cUid chứ ko phải là $cAgentId
            
        }elseif(in_array($cUid, $aUidNotifySupport) || in_array($cRole, self::$aRoleLimitViewApproved)){
            if($cRole == ROLE_HEAD_OF_MAINTAIN){// Jul 28, 2016 xử lý cho tổ trưởng bảo trì Quý Duyệt đầu tư  theo yc anh Hoàn
                $deloy_by = $cUid;
//                $criteria->addCondition("t.deloy_by LIKE :AGENT_ID ");
                $criteria->params[':AGENT_ID'] = "%".$deloy_by."%";// vì là user sub agent nên sẽ lấy $cUid chứ ko phải là $cAgentId
                $criteria->addCondition("t.to_uid_approved=$cUid OR t.approved_uid_level_1=$cUid OR t.deloy_by LIKE :AGENT_ID" );
            }else{
                // chỗ này view của nv Bảo trì, truong phong KT, trưởng phòng KD, update status complete
                $criteria->addCondition("t.approved_uid_level_3 IS NOT NULL AND t.status NOT IN ($sStatusNotShow)" );
            }

        }else{
            // Jan 28, 2015 cho phep user dai ly tao support nua, nen gioi han ke toan view
            GasAgentCustomer::addInConditionAgent($criteria, 't.agent_id');
        }
    }
    
    /**
     * @Author: ANH DUNG Jul 23, 2016
     * @Todo: xử lý search list LIMIT role cho KH HGD
     */
    public function handleRoleForHgd(&$criteria, $cRole, $cUid, $aUidNotifySupport, $sStatusNotShow) {
        if( $cRole == ROLE_HEAD_GAS_BO ){
            $criteria->addCondition("t.to_uid_approved=$cUid OR t.approved_uid_level_1=$cUid OR t.uid_login = $cUid ");
        }elseif( $cRole == ROLE_HEAD_GAS_MOI ){
            $criteria->addCondition("t.to_uid_approved=$cUid OR t.approved_uid_level_1=$cUid OR t.uid_login = $cUid ");
        }elseif(in_array($cRole, $this->ROLE_APPROVED_NEW) ){// Dec 18, 2015 fix cho anh Tổng giám sát vào phần duyệt level 1
            $criteria->addCondition("t.to_uid_approved=$cUid OR t.approved_uid_level_1=$cUid OR t.uid_login = $cUid ");
        }elseif( $cRole == ROLE_DIRECTOR_BUSSINESS ){
            $criteria->addCondition("(t.to_uid_approved=$cUid OR t.approved_uid_level_2=$cUid OR t.uid_login = $cUid ) " );
        }elseif( $cRole == ROLE_DIRECTOR ){
            $criteria->addCondition("(t.to_uid_approved=$cUid OR t.approved_uid_level_3=$cUid) " );
        }elseif( (in_array($cRole, GasSupportCustomer::$aRoleNewCreate) || in_array($cRole, GasSupportCustomer::$aRoleCreateExt1))&& $cRole!= ROLE_ADMIN){
            $criteria->compare('t.uid_login', $cUid);
        }elseif( $cRole == ROLE_SUB_USER_AGENT ){
            $deloy_by = $cUid;
            $criteria->addCondition("t.uid_login = $cUid OR t.deloy_by LIKE :AGENT_ID ");
            $criteria->params[':AGENT_ID'] = "%".$deloy_by."%";// vì là user sub agent nên sẽ lấy $cUid chứ ko phải là $cAgentId
            
        }elseif(in_array($cUid, $aUidNotifySupport) || in_array($cRole, self::$aRoleLimitViewApproved)){
            // chỗ này view của nv Bảo trì, truong phong KT, trưởng phòng KD, update status complete
            $criteria->addCondition("t.approved_uid_level_3 IS NOT NULL AND t.status NOT IN ($sStatusNotShow)" );
//            $criteria->order = "t.approved_date_level_3 DESC"; khong nen dung vi co the se slow
        }else{
            // Jan 28, 2015 cho phep user dai ly tao support nua, nen gioi han ke toan view
            GasAgentCustomer::addInConditionAgent($criteria, 't.agent_id');
        }
    }
    
    public function defaultScope() { return array(); }
        
    /**
     * @Author: ANH DUNG Dec 27, 2014
     */
    protected function beforeSave() {
        if(strpos($this->date_request, '/')){
            $this->date_request = MyFormat::dateConverDmyToYmd($this->date_request);
            MyFormat::isValidDate($this->date_request);
        }
        if(strpos($this->time_doing, '/')){
            $this->time_doing = MyFormat::datetimeToDbDatetime($this->time_doing);
        }
        if(strpos($this->time_doing_real, '/')){
            $this->time_doing_real = MyFormat::datetimeToDbDatetime($this->time_doing_real);
        }
        
        $this->note = InputHelper::removeScriptTag($this->note);
        if(trim($this->approved_note_level_1)  != ''){
            $this->approved_note_level_1 = InputHelper::removeScriptTag($this->approved_note_level_1);
        }
        if(trim($this->approved_note_level_2)  != ''){
            $this->approved_note_level_2 = InputHelper::removeScriptTag($this->approved_note_level_2);
        }
        if(trim($this->approved_note_level_3)  != ''){
            $this->approved_note_level_3 = InputHelper::removeScriptTag($this->approved_note_level_3);
        }
        
        if($this->isNewRecord){
            $this->uid_login = Yii::app()->user->id;
            $this->code_no = MyFunctionCustom::getNextId('GasSupportCustomer', 'T'.date('y'), MyFormat::MAX_LENGTH_8, 'code_no');
        }
        if($this->rCustomer && $this->status == self::STATUS_NEW){
            $this->sale_id = $this->rCustomer->sale_id;
        }
        $this->HandleSendMailToSale();
        
        $countByCustomer = $this->countByCustomerId();
        if($countByCustomer==0){
            GasScheduleEmail::BuildListNotifyNewCustomer($this->rCustomer, array('support_customer'=>1));// May 10, 2016
            $info = "Mail notify khi customer tạo phiếu giao NV lần 1 KH: ".$this->rCustomer->id;
            Logger::WriteLog($info);
        }
        
        return parent::beforeSave();
    }
    
    /**
     * @Author: ANH DUNG Sep 19, 2015
     * @Todo: something
     * @Param: $model
     */
    public function HandleDeloyBy() {
//        if(isset($_POST['deloy_by']) && is_array($_POST['deloy_by']) && count($_POST['deloy_by'])){
        if(isset($_POST['deloy_by'])){
            $this->deloy_by = json_encode($_POST['deloy_by']);
        }else{
            $this->deloy_by = "";
        }
    }
    
    /**
     * @Author: ANH DUNG Sep 19, 2015
     */
    public function HandleSendMailToSale() {
       // Sep 19, 2015, phải xử lý send mail cho sale update lần đầu ở before save này
        $SendMail = false;
        if($this->scenario == 'sale_update' && empty($this->sale_update) ){
            $this->sale_update = 1;
            $SendMail = true;
        }
        if($SendMail){
            GasScheduleEmail::BuildListNotifySupportCustomer($this->id);
        }
        // Sep 19, 2015, phải xử lý send mail cho sale update lần đầu ở before save này 
    }
    
    protected function afterSave() {
        if( $this->IsCreateUpdate() ){
            $this->SaveDetailItem();
        }
        $SendMail = false;
//        $aScenarioMail = array('tech_create', 'sub_agent_create', 'head_update_time_real');
        /** @note: Oct 14 xử lý riêng cho scenario 'tech_create', không send mail khi tạo
         * mà có nút send mail trên list index để nó click vào send ( tạo xong update thoải mái rồi mới send )
         */
        $aScenarioMail = array('sub_agent_create', 'head_update_time_real');
        if(in_array($this->scenario, $aScenarioMail) ){
            // Sep 16, 2015 chắc là chỉ có update thì mới send mail, kiểm tra chỉ send mail cho lần update đầu tiên
            // còn với đại lý thì sẽ send mail khi create 
            $SendMail = true;
        }        
        
        if($SendMail){
            GasScheduleEmail::BuildListNotifySupportCustomer($this->id);
        }                
        
        return parent::afterSave();
    }
    
    /**
     * @Author: ANH DUNG Oct 14, 2015
     * @Todo: keyword: scenario 'tech_create'
     */
    public function TechSendMail() {
        GasScheduleEmail::BuildListNotifySupportCustomer($this->id);
        $this->scenario = null;
        $this->send_mail = 1;
        $this->update(array('send_mail'));
    }
    
    /**
     * @Author: ANH DUNG Oct 14, 2015
     * @Todo: keyword: scenario 'tech_create'
     */
    public static function ShowColTechSendMail() {
        $cRole = Yii::app()->user->role_id;
        $aCheckCol = array(ROLE_TECHNICAL);
        if(in_array($cRole, $aCheckCol)){
            return 1;
        }
        return 0;
    }
    
    /**
     * @Author: ANH DUNG Oct 14, 2015
     * @Todo: keyword: scenario 'tech_create'
     */
    public function ShowIconTechSendMail() {
        if($this->send_mail){
            return false;
        }
        return true;
    }
    
    
    /**
     * @Author: ANH DUNG Sep 15, 2015
     * @Todo: save detail 
     */
    public function SaveDetailItem() {
//        MyFormat::deleteModelDetailByRootId('GasSupportCustomerItem', $model->id, 'support_customer_id');
        // không thể delete detail vì còn các cấp trên update % cho từng detail
        $this->DeleteSomeDetail();
        foreach( $this->aModelDetail as $mDetail )
        {
            $mDetail->percent = $mDetail->buildJsonPercent(1);
            // Dec 30, 2014 chỗ này không cần 1 build sql, vì cảm thấy lưu lượng dùng chỗ này cũng ít
            $mDetail->support_customer_id = $this->id;
            if($mDetail->id)
                $mDetail->update();
            else
                $mDetail->save();
        }
    }
    
    /**
     * @Author: ANH DUNG Sep 17, 2015
     * @Todo: delete 
     * @Param: $model
     */
    public function DeleteSomeDetail() {
        if(is_array($this->aIdDetailNotDelete) && count($this->aIdDetailNotDelete)){
            $criteria = new CDbCriteria;
            $criteria->compare('support_customer_id', $this->id);
            $criteria->addNotInCondition('id', $this->aIdDetailNotDelete);
            GasSupportCustomerItem::model()->deleteAll($criteria);
        }
    }
    
    /**
     * @Author: ANH DUNG Sep 17, 2015
     * @Todo: upate percent of detail
     * @param: $index chỉ số lần update của các loại Role
     */
    public function UpdaetPercentDetailItem($index) {
        foreach( $this->aModelDetail as $mDetail )
        {
            $mDetail->percent = $mDetail->buildJsonPercent($index);
            // Dec 30, 2014 chỗ này không cần 1 build sql, vì cảm thấy lưu lượng dùng chỗ này cũng ít
            $mDetail->update(array('percent'));
        }
    }
    
    protected function beforeValidate() {
        $cRole = Yii::app()->user->role_id;
        $cUid = Yii::app()->user->id;
        if($this->rCustomer){
            $this->type_customer = $this->rCustomer->is_maintain;
            $this->customer_agent_id = Users::GetAgentIdOfCustomer($this->customer_id);
        }
        
        if( $cRole == ROLE_SUB_USER_AGENT ){
            // Jan 28, 2015 fix cho user dai ly dc phep tao support, nen se save ca agent id nua
            $this->agent_id = MyFormat::getAgentId();
        }        
        
//        if( $this->IsCreateUpdate() ){
            self::BuildArrDetail($this);
//        }
          $this->HandleDeloyBy();
        return parent::beforeValidate();
    }
    
    /**
     * @Author: ANH DUNG Dec 30, 2014
     * @Todo: check scenario only for create and update 
     * @Param: $model
     */
    public function IsCreateUpdate() {
        return in_array($this->scenario, array('ccs_create', 'ccs_update', 'tech_create','tech_update', 'sub_agent_create', 'sub_agent_update'));
    }
    
    protected function afterValidate() {
        if(count($this->aModelDetail) < 1 && $this->IsCreateUpdate() ){
            $this->addError('customer_id', "Chưa nhập tên thiết bị hoặc số lượng");
        }
        return parent::afterValidate();
    }
    
    /**
     * @Author: ANH DUNG Dec 29, 2014
     * @Todo: build array model detail from post
     * @Param: $model is model GasSupportCustomer
     */
    public static function BuildArrDetail($model) {
        $model->aModelDetail = array();
        $model->aIdDetailNotDelete = array();
        if(isset($_POST['GasSupportCustomerItem']['materials_id']) && is_array($_POST['GasSupportCustomerItem']['materials_id'])){
            foreach( $_POST['GasSupportCustomerItem']['materials_id'] as $key=>$materials_id){                
                $type = isset($_POST['GasSupportCustomerItem']['type'][$key])?$_POST['GasSupportCustomerItem']['type'][$key]:"";
                $qty = isset($_POST['GasSupportCustomerItem']['qty'][$key])?$_POST['GasSupportCustomerItem']['qty'][$key]:"";
                $price = isset($_POST['GasSupportCustomerItem']['price'][$key])?$_POST['GasSupportCustomerItem']['price'][$key]:"";
                $percent = isset($_POST['GasSupportCustomerItem']['percent'][$key])?$_POST['GasSupportCustomerItem']['percent'][$key]:"";
                $id = isset($_POST['GasSupportCustomerItem']['id'][$key])?$_POST['GasSupportCustomerItem']['id'][$key]:"";
                
                if($materials_id ){
                    if(!empty($id)){
                        $mDetail = GasSupportCustomerItem::model()->findByPk($id);
                        if(!$mDetail){
                            continue;
                        }
                        $model->aIdDetailNotDelete[] = $id;
                    }else{
                        if( $qty <= 0 ){// Oct 06, 2015 required input slg
                            continue;
                        }
                        $mDetail = new GasSupportCustomerItem();
                    }
                    $mDetail->materials_id = $materials_id;
                    $mDetail->qty = $qty;
                    $mDetail->price = $price;
                    $mDetail->percent = $percent;
                    $mDetail->amount = $mDetail->qty*$mDetail->price;
                    $mDetail->materials_type_id = $mDetail->rMaterials?$mDetail->rMaterials->materials_type_id : 0;
                    $model->aModelDetail[] = $mDetail;
                }
            }
        }
    }
    
    /**
     * @Author: ANH DUNG Dec 27, 2014
     */
    public static function GetTypeSupport($model) {
        if(isset( GasSupportCustomer::$ARR_TYPE[$model->type]))
        {
            return GasSupportCustomer::$ARR_TYPE[$model->type];
        }
        return '';
    }
    
    /**
     * @Author: ANH DUNG Dec 27, 2014
     */
    public static function GetStatusSupport($model) {
        if(isset( GasSupportCustomer::$ARR_APPROVED[$model->status]))
        {
            return GasSupportCustomer::$ARR_APPROVED[$model->status];
        }
        return '';
    }
    
    /**
     * @Author: ANH DUNG Dec 27, 2014
     * @Todo: check valid user can view and approved for this support
     * Mới chỉ giới hạn user là user KH bò thì gửi cho TRưởng pHÒng Bò, Mối thì gửi cho Trưởng PHòng Mối
     * Trưởng phòng KD bò-mối duyệt rồi mới lên Giám đốc KD duyệt
     * @Param: $model
     * @Return: true if allow view, false if not
     */
    public static function IsOwner($model) {
        return true;// Jul 23, 2016 vì có nhiều cấp duyệt nên ko check chỗ này nữa
        $aRoleCheck = array( ROLE_HEAD_GAS_BO, ROLE_HEAD_GAS_MOI, ROLE_DIRECTOR_BUSSINESS ); // TRưởng pHÒng Bò, Mối
        $cRole = Yii::app()->user->role_id;
        $cUid = Yii::app()->user->id;
        $ok = true;
        if( $cRole == ROLE_HEAD_GAS_BO &&  $model->type_customer != STORE_CARD_KH_BINH_BO ){
            $ok = false;
        }elseif( $cRole == ROLE_HEAD_GAS_MOI &&  $model->type_customer != STORE_CARD_KH_MOI ){
            $ok = false;
        }
        return $ok;
    }
    
    /**
     * @Author: ANH DUNG Dec 27, 2014
     * @Todo: check user can update status
     * @Param: $model
     */
    public static function CanUpdateStatus($model) {
        $cRole = Yii::app()->user->role_id;
        $cUid = Yii::app()->user->id;
        $ok = true;
        if( $cRole == ROLE_HEAD_GAS_BO &&  $model->type_customer != STORE_CARD_KH_BINH_BO 
                && in_array($model->status, GasSupportCustomer::$ARR_NOT_ALLOW)
                ){
            $ok = false;
        }elseif( $cRole == ROLE_HEAD_GAS_MOI &&  $model->type_customer != STORE_CARD_KH_MOI 
                && in_array($model->status, GasSupportCustomer::$ARR_NOT_ALLOW)
                ){
            $ok = false;
        }
        $dayAllow = date('Y-m-d');
        $dayAllow = MyFormat::modifyDays($dayAllow, GasSupportCustomer::getDayAllowUpdate(), '-');
        $ok2 = MyFormat::compareTwoDate($model->created_date, $dayAllow);
        return ($ok && $ok2);
    }

    /**
     * @Author: ANH DUNG Dec 27, 2014
     * @Todo: handle update status
     * @Param: $model
     */
    public static function HandleUpdateStatus($model) {
        $aRoleCheck     = self::$aRoleLevel1; // TRưởng pHÒng Bò, Mối
        $aRoleHeadKD    = array( ROLE_DIRECTOR_BUSSINESS ); // GD kinh Doanh
        $aRoleHeadRoot  = array( ROLE_DIRECTOR ); // Tong Giam Doc
        $cRole          = MyFormat::getCurrentRoleId();
        $cUid           = MyFormat::getCurrentUid();
        
        $SendMail = true;// Sep 23, 2015 check nếu change to_uid_approved thì send mail, còn lại thì không send
        $OldModel = self::model()->findByPk($model->id);
        // Sep 23, 2015
        $aUpdateDefault = array('chk_design_wrong', 'chk_material_not_enough', 'chk_deploy_wrong');
        
//        $note = isset($_POST['GasSupportCustomer']['approved_note_level_1'])?$_POST['GasSupportCustomer']['approved_note_level_1']:$_POST['GasSupportCustomer']['approved_note_level_2'];
        if(in_array($cRole, $aRoleCheck)){
            if($OldModel->to_uid_approved == $model->to_uid_approved){
                $SendMail = false;// cho trường hợp update nhiều lần cái status
            }
            $model->approved_uid_level_1 = $cUid;
            $model->approved_date_level_1 = date('Y-m-d H:i:s');
            $model->update(array('to_uid_approved','status', 'approved_uid_level_1', 'approved_date_level_1', 'approved_note_level_1'));
        }elseif(in_array($cRole, $aRoleHeadKD)){
            if($OldModel->to_uid_approved == $model->to_uid_approved){
                $SendMail = false;
            }
            $model->approved_uid_level_2 = $cUid;
            $model->approved_date_level_2 = date('Y-m-d H:i:s');
            $model->update(array('to_uid_approved','status', 'approved_uid_level_2', 'approved_date_level_2', 'approved_note_level_2'));
        }elseif(in_array($cRole, $aRoleHeadRoot)){// Anh Long duyệt lần 3
            $model->approved_uid_level_3 = $cUid;
            $model->approved_date_level_3 = date('Y-m-d H:i:s');
            $model->update(array('status', 'approved_uid_level_3', 'approved_date_level_3', 'approved_note_level_3'));
            if($model->status == GasSupportCustomer::STATUS_APPROVED_3){
                $aUid = array(GasLeave::UID_HEAD_TECHNICAL, GasSupportCustomer::UID_QUY_PV);
                GasScheduleEmail::BuildListNotifySupportCustomerFix($aUid, $model);
            }
        }elseif(in_array($cRole, GasSupportCustomer::$ROLE_UPDATE_COMPLETE) ){
            $model->approved_uid_level_4 = $cUid;
            $model->approved_date_level_4 = date('Y-m-d H:i:s');
            $aUpdate = array_merge($aUpdateDefault, array('status', 'approved_uid_level_4', 'approved_date_level_4', 'approved_note_level_4'));
            $model->update($aUpdate);
            $SendMail = false;
        }
        
        if($SendMail){
            GasScheduleEmail::BuildListNotifySupportCustomer($model->id);
        }
    }
    
    /**
     * @Author: ANH DUNG Dec 27, 2014
     * @Todo: get last update status
     * @Param: $model
     */
    public function getLastUpdateStatus() {
        $res = '';
        $cmsFormater = new CmsFormatter();
        if( !empty($this->approved_uid_level_4) ){
            $res = $this->rApprovedUidLevel4->first_name."<br>".$cmsFormater->formatDateTime($this->approved_date_level_4);
        }elseif( !empty($this->approved_uid_level_3) ){
            $res = $this->rApprovedUidLevel3->first_name."<br>".$cmsFormater->formatDateTime($this->approved_date_level_3);
        }elseif( !empty($this->approved_uid_level_2) ){
            $res = $this->rApprovedUidLevel2->first_name."<br>".$cmsFormater->formatDateTime($this->approved_date_level_2);
        }elseif( !empty($this->approved_uid_level_1) ){
            $res = $this->rApprovedUidLevel1->first_name."<br>".$cmsFormater->formatDateTime($this->approved_date_level_1);
        }
        return $res;
    }
    
    /**
     * @Author: ANH DUNG Dec 30, 2014
     * @Todo: get last update status
     * @Param: $model
     */
    public static function GetLastUpdateNote($model) {
        $res = '';       
        if( !empty($model->approved_uid_level_4) ){
            $res = $model->approved_note_level_4;
        }elseif( !empty($model->approved_uid_level_3) ){
            $res = $model->approved_note_level_3;
        }elseif( !empty($model->approved_uid_level_2) ){
            $res = $model->approved_note_level_2;
        }
        elseif( !empty($model->approved_uid_level_1) ){
            $res = $model->approved_note_level_1;
        }else{
            $res = $model->note; // ghi chu luc moi tao
        }
        return $res;
    }
    
    /**
     * @Author: ANH DUNG Dec 28, 2014
     * @Todo: get history support of this customer
     * @Param: $customer_id
     * @Param: $aIdNotIn array id not in search
     */
    public static function GetHistorySupport($customer_id, $aIdNotIn) {
        $criteria = new CDbCriteria();
        $criteria->compare('t.customer_id', $customer_id);
        if(count($aIdNotIn)){
//            $criteria->addNotInCondition('t.id', $aIdNotIn);
            $sParamsIn = implode(',', $aIdNotIn);
            $criteria->addCondition("t.id NOT IN ($sParamsIn)");
        }
//        $criteria->addCondition("t.approved_uid_level_3 IS NOT NULL AND t.status<>".self::STATUS_REJECT);
//        $criteria->addCondition(" t.status<>".self::STATUS_NEW);
        $criteria->compare('t.status', self::STATUS_APPROVED_3);
        
        $criteria->order = "t.id DESC";
        return self::model()->findAll($criteria);
    }
    
    /**
     * @Author: ANH DUNG Dec 30, 2014
     * @Todo: get list model user lien quan ( dung de notify ) trong 1 model support customer
     * @Param: $support_customer_id
     */
    public static function GetListModelUserNotify($support_customer_id) {
        $mSupportCustomer = self::model()->findByPk($support_customer_id);
        $cRole = Yii::app()->user->role_id;
        $aRoleToNotify = array( ROLE_HEAD_GAS_BO ); // TRưởng pHÒng Bò, Mối
        
//        $mSupportCustomer->getRoleNotify($aRoleToNotify);
        $aUid = array($mSupportCustomer->to_uid_approved);// Sep 23, 2015        
        $aUidNotReset = array($mSupportCustomer->uid_login, $mSupportCustomer->sale_id);
        $needMore = array('aUid' => $aUid, 'aUidNotReset'=>$aUidNotReset);
        
        $aModelUserMail = Users::GetListUserMail($needMore);
        if( $cRole == ROLE_TECHNICAL ){ // Sep 18, 2015 nếu là tạo lần đầu chỉ send mail cho sale
            $aModelUserMail = array();
        }
        
        $mSale = Users::model()->findByPk($mSupportCustomer->sale_id);// luôn notify cho sale
        if(is_array($aModelUserMail)){
            $aModelUserMail[] = $mSale;
        }else{
            $aModelUserMail = array($mSale);
        }
        // Sep 04, 2015 - notify cho đại lý nếu được duyệt
        GasSupportCustomer::getMailSubUserAgent($mSupportCustomer, $aModelUserMail);        
        GasScheduleEmail::WritelogDebugBuildMail("GasSupportCustomer ID: $mSupportCustomer->id  build email send: ", $aModelUserMail);
        
//        return array();// only for test
//        $aModelUserMail = array();// only for test
//        MyFormat::AddEmailAnhDung($aModelUserMail);// only for test, close when done test
        return $aModelUserMail;
    }
    
    /**
     * @Author: ANH DUNG Sep 15, 2015
     * @Todo: get role to notify
     * [11:02:46] ngoc_kien1: nhan vien kinh doanh de xuat
        [11:03:05] ngoc_kien1: gui mail cho tp
        [11:03:50] ngoc_kien1: tp duyet gui mail cho gd, nhan vien
        [11:04:09] ngoc_kien1: gd duyet thi gui tong gd, nhan vien
     */
    public function getRoleNotify( &$aRoleToNotify ) {
        die('Error not use, nếu bạn gặp lỗi này, liên hệ với Kiên');
        // Sep 23, 2015 không dùng hàm này nữa, vì đã cho select người duyệt trên giao diện rồi
        if ( $this->type_customer == STORE_CARD_KH_MOI ){
            $aRoleToNotify = array( ROLE_HEAD_GAS_MOI ); // TRưởng pHÒng Bò, Mối
        }
        if ( $this->status == GasSupportCustomer::STATUS_APPROVED_1 ){
            $aRoleToNotify = array( ROLE_DIRECTOR_BUSSINESS ); // GĐ kinh doanh
        }elseif ($this->status == GasSupportCustomer::STATUS_APPROVED_2) {
            $aRoleToNotify = array( ROLE_DIRECTOR ); // GĐ
        }
    }
    
    /**
     * @Author: ANH DUNG Oct 07, 2015
     * @Todo: thêm role kế toán + kế toán trưởng để send mail
     * sau khi anh Long duyệt
     */
    public function getMoreRoleAfterApproved3() {
        return array(ROLE_CHIEF_ACCOUNTANT, ROLE_ACCOUNTING);
    }
    
    /**
     * @Author: ANH DUNG Sep 19, 2015
     * @Todo: get email notify to subuser agent - Don vi thuc hien
     */
    public static function getMailSubUserAgent($mSupportCustomer, &$aModelUserMail) {
        // sua lai duyet lan cuoi moi send
        // 3. nv ke toan gui mail cho 2- 3 nguoi, id se define
        // 4. to truong bao tri bo phan bao tri - Phan Van Quy
        // 5. thu kho Do Nhu Dung
        $aModelSubAgent = array();
        $aModelUserNotify = array();
        $aSendMore = array();
        $aUidNotifySupport = $mSupportCustomer->getUIdNotify();
        if( !empty($mSupportCustomer->approved_uid_level_3) && $mSupportCustomer->status == GasSupportCustomer::STATUS_APPROVED_3){
            // tổng giám đốc duyệt lần cuối thì mới send cho các bộ phận khác
            // 1. mail người thực hiện
            $aModelSubAgent = Users::getModelSubUserAgentByAgentId($mSupportCustomer->getArrayDeloyBy());
            // 2. mail các ID User liên quan như trên nói
            $aModelUserNotify = Users::getOnlyArrayModelByArrayId($aUidNotifySupport);
            // 3. Oct 07, 2015 kt+kt Truong
            $aSendMore = Users::getFindAllByArrayRole($mSupportCustomer->getMoreRoleAfterApproved3());
        }
        
        foreach( $aModelSubAgent as $model){
            $aModelUserMail[] = $model;
        }
        foreach( $aModelUserNotify as $model){
            $aModelUserMail[] = $model;
        }
        foreach( $aSendMore as $model){
            $aModelUserMail[] = $model;
        }
    }
    
    /**
     * @Author: ANH DUNG Sep 12, 2015
     * @Todo: check user can update to print
     */
    public function canUpdateToPrint() {
        $ok = false;
        if( !empty($this->approved_uid_level_1) || !empty($this->approved_uid_level_2) ){
            $ok = true;
            // nếu Usser đã cập nhật to print rồi thì check chỉ cho user 
            // sửa cái print đó trong vòng 1 ngày
            //getDayUpdateToPrint()
            if(!empty($this->last_update_time)){
                $dayAllow = date('Y-m-d');
                $dayAllow = MyFormat::modifyDays($dayAllow, $this->getDayUpdateToPrint(), '-');
                $ok = MyFormat::compareTwoDate($this->last_update_time, $dayAllow);
                // last_update_time > $dayAllow là đúng
            }
        }
        return $ok;
    }
    
    /**
     * @Author: ANH DUNG Sep 02, 2015
     * @Todo: only update some field to print
     */
    public function UpdateForPrint() {
        $aUpdate = array('contact_person','contact_tel', 'time_doing', 'price_qty');
        if(empty($this->last_update_time)){
            $this->last_update_time = date('Y-m-d H:i:s');
            $aUpdate[] = 'last_update_time';
        }
        $this->time_doing = MyFormat::datetimeToDbDatetime($this->time_doing);
        $this->update($aUpdate);
    }
    
    /**
     * @Author: ANH DUNG Sep 02, 2015
     */
    public function getContactPerson() {
        return $this->contact_person;
    }
    
    public function getContactTel() {
        return $this->contact_tel;
    }
    public function getTimeDoing() {
        $res =  trim(MyFormat::datetimeDbDatetimeUser($this->time_doing), "00");
        return trim($res, ":");
    }
    public function getTimeDoingReal() {
        $res =  trim(MyFormat::datetimeDbDatetimeUser($this->time_doing_real), "00");
        return trim($res, ":");
    }
    public function getPriceQty() {
        return $this->price_qty;
    }
    
    public function getArrayDeloyBy() {
        return json_decode($this->deloy_by, true);
    }
    public function getNote() {
        return nl2br($this->note);
    }
    public function getNoteTechnical() {
        return nl2br($this->note_technical);
    }
    public function getCustomer() {
        $mUser = $this->rCustomer;
        if($mUser){
            return $mUser->first_name;
        }
        return '';
    }
    public function getGPrice() {
        $month = MyFormat::dateConverYmdToDmy($this->created_date, 'm');
        $year  = MyFormat::dateConverYmdToDmy($this->created_date, 'Y');
        $mUsersPrice =new UsersPrice();
        $aInfo = $mUsersPrice->getPriceOfCustomer($month, $year, $this->customer_id, ['GetPriceCode'=>1]);
        return $aInfo['sPrice'];
    }
    
    public function canPrint() {
        if( !empty($this->approved_uid_level_3) && $this->status >= self::STATUS_APPROVED_3 ){
            return true;
        }
        return false;
//        if($this->last_update_time){
//            return true;
//        }
//        return false;
    }
    
    /**
     * @Author: ANH DUNG Sep 04, 2015
     * @Todo: get array model file
     */
    public function getFile() {
        if(empty($this->id)){
            return array();
        }
        $criteria = new CDbCriteria;
        $criteria->compare('t.belong_id', $this->id);
        $criteria->compare('t.type', GasFile::TYPE_2_SUPPORT_CUSTOMER);
        $criteria->order = "t.id";
        return GasFile::model()->findAll($criteria);
    }
    
    /**
     * @Author: ANH DUNG Dec 28, 2015
     * @Todo: get array model file complete
     */
    public function getFileComplete() {
        if(empty($this->id)){
            return array();
        }
        $criteria = new CDbCriteria;
        $criteria->compare('t.belong_id', $this->id);
        $criteria->compare('t.type', GasFile::TYPE_4_SUPPORT_CUSTOMER_COMPLETE);
        $criteria->order = "t.id";
        return GasFile::model()->findAll($criteria);
    }
    
    /**
     * @Author: ANH DUNG Sep 04, 2015
     * @Todo: delete file
     */
    public function deleteFile() {
        if(isset($_POST['delete_file']) && is_array($_POST['delete_file'])){
            $criteria = new CDbCriteria;
            $criteria->compare('t.belong_id', $this->id);
            $criteria->addInCondition('t.id', $_POST['delete_file']);
            $models = GasFile::model()->findAll($criteria);
            foreach($models as $model){
                $model->delete();
            }
        }
    }
    
    // Dec 28, 2015 ANH DUNG
    public function formatFileComplete(){
        $aFile = $this->rFile;
        $str = '';
        foreach($aFile as $key=>$item){
            $str.="<a target='_blank' class='gallery' href='".Yii::app()->createAbsoluteUrl('admin/ajax/viewImageProfileHs', array('id'=>$item->id, 'model'=>'GasFile'))."'> ";
                $str.="<img width='80' height='60' src='".ImageProcessing::bindImageByModel($item,'','',array('size'=>'size1'))."'>";
            $str.="</a>";
        }
       return $str;
   }
    
    /**
     * @Author: ANH DUNG Sep 04, 2015
     * @Todo: get list file upload 
     */
    public function renderListFile() {
        $res = "";
        foreach($this->getFile() as $key=>$mFile)
        {
            $br="<br>";
            if($key==0){
                $br="";
            }
            $res .= "$br".($key+1).". ".$mFile->getForceLinkDownload();
        }
        return $res;
    }
    
    protected function beforeDelete() {
        MyFormat::deleteModelDetailByRootId('GasSupportCustomerItem', $this->id, 'support_customer_id');
        GasFile::DeleteByBelongIdAndType($this->id, GasFile::TYPE_2_SUPPORT_CUSTOMER);
        return parent::beforeDelete();
    }
    
    /**
     * @Author: ANH DUNG Sep 16, 2015
     * @Todo: get Select percent đầu tư
     */
    public function getListOptionPercent() {
        return array(0=>0, 30=>30, 50=>50, 70=>70, 100=>100);// Fix Apr 29, 2016
    }
    
    /**
     * @Author: ANH DUNG Sep 17, 2015
     * @Todo: check user can update record
     */
    public function canUpdateGrid() {
        $cRole = Yii::app()->user->role_id;
        $cUid = Yii::app()->user->id;
        if(in_array($cRole, GasSupportCustomer::$aRoleNewCreate)
            && $cUid == $this->uid_login && $this->sale_update==1
        ){
            return false;
        }// Oct 06, 2015 - trường hợp NV kỹ thuật vào không thể update nếu sale đã update
        
        if(in_array($cRole, GasSupportCustomer::$aRoleNewCreate)
            && $cUid == $this->uid_login && $this->sale_update==0
        ){
            // chỗ empty($this->contact_person) là để check xem sale đã update chưa
            // nếu update rồi thì ko cho NV kỹ thuật sửa nữa
            return true;
        }elseif(in_array($cRole, GasSupportCustomer::$aRoleNewUpdate)
            && $cUid == $this->sale_id
        ){// cho phép sale sửa của sale
            if($cRole == ROLE_SALE && $this->send_mail == 0 && $cUid != $this->uid_login){
                // Now 12, 2015 thêm đoạn && $cUid != $this->uid_login để xác định record đó không phải của sale tạo mà là NV kỹ thuật tạo
                // vì thấy Sale Phú tạo xong mà không update dc, có lẽ nó vào điều kiện này
                return false; // Oct 14, 2015 thêm 1 cái cờ để xác định đã mail cho sale chưa, nếu mail rồi thì mới cho update
            }
            return true;
        }elseif(
                in_array($cRole, GasSupportCustomer::$aRoleAllowCreate) ||
                $cUid == $this->uid_login
        ){// user đại lý + sale (Sep 28, 2015 cho phép sale tạo với scenario giống đại lý)
            // Sep 29, 2015 cho phép all user có thể create + update
            // Sep 29 vì điều kiện $cUid == $this->uid_login không bao gồm trường hợp sale 
            // update record của NV kỹ thuật nên phải thêm cái IN_array nữa
            return true;
        }
        return false;
    }
    
    /**
     * @Author: ANH DUNG Sep 19, 2015
     * @Todo: giữ lại html view của dữ liệu cũ
     */
    public function getOldHtmlView() {
       $str = '';
       foreach($this->rDetail as $key=>$item){
           $type = isset(GasSupportCustomer::$ARR_TYPE[$item->type]) ? GasSupportCustomer::$ARR_TYPE[$item->type] : '';
           $str .= "<br> <b>".(++$key).". </b>$type - {$item->item_name}";
       }
       $str = trim($str,"<br>");
       $str = "<p style='display:inline-block;'>$str</p>";
       return $str; 
    }
    
    /**
     * @Author: ANH DUNG Sep 23, 2015
     * @Todo: check user can update level 4 - complete
     */
    public function CanUpdateLevel4() {
        $cRole  = MyFormat::getCurrentRoleId();
        $cUid   = MyFormat::getCurrentUid();
        $ok = false;
//        $aStatusAllowUpdateCompleteAgain = array(self::STATUS_COMPLETE, self::STATUS_NOT_DOING);// Close on Jan 14, 2017 để a Quý có thể cập nhật duyệt lần 1, nếu mở chỗ này thì a Quý chỉ có thể nhìn thấy hoàn thành, ko duyệt lần 1 dc
        $aStatusAllowUpdateCompleteAgain = array(self::STATUS_COMPLETE);
        if(in_array($cRole, GasSupportCustomer::$ROLE_UPDATE_COMPLETE) 
                && $this->status==GasSupportCustomer::STATUS_APPROVED_3 
        ){
            $ok = true;
        }elseif($cUid == GasSupportCustomer::UID_QUY_PV && in_array($this->status, $aStatusAllowUpdateCompleteAgain)){
            // Dec 16, 2016 mở cho anh Quý 15 ngày cập nhật hoàn thành các phiếu chưa upload file biên bản
            $beginDate  = '2016-12-16';
            $endDate    = MyFormat::modifyDays($beginDate, 15);
            $ok         = MyFormat::compareTwoDate($endDate, date('Y-m-d'));
        }
        return $ok;
    }
    
    /**
     * @Author: ANH DUNG Sep 23, 2015
     * @Todo: check user can update thời gian thi công thực tế
     * - nếu return true thì xử lý cho phép tổ trưởng bảo trì vào update thời gian thi công thực tế
     * - nếu return false thì sẽ không cho phép update, chạy xuống dưới check các điều kiện khác
     */
    public function CanUpdateTimeDoingReal() {
        $cRole = Yii::app()->user->role_id;
        $cUid = Yii::app()->user->id;
        $aRoleUpdateTime = self::$aRoleLimitViewApproved;
        $aDeloyBy = json_decode($this->deloy_by, true);
        if(!is_array($aDeloyBy) && !empty($aDeloyBy)){
            $aDeloyBy = array($aDeloyBy);
        }elseif(is_null($aDeloyBy)){
            return false;
        }
        
//        $aDeloyBy[] = GasSupportCustomer::UID_HUAN_DN;// May 13, 2016 fix cho a Huấn và A Tâm cập nhật lịch thi công thực tế
//        $aDeloyBy[] = GasSupportCustomer::UID_TAM_PV;
        $aDeloyBy[] = GasLeave::UID_HEAD_TECHNICAL;
        $aDeloyBy[] = 191;// Nguyễn Thành Hiếu
        
        $ok = false;
        if(in_array($cUid, $aDeloyBy) && $this->status==GasSupportCustomer::STATUS_APPROVED_3){
//        if(in_array($cRole, $aRoleUpdateTime) && in_array($cUid, $aDeloyBy) && $this->status==GasSupportCustomer::STATUS_APPROVED_3){
            $ok = true;
            $dayAllow = date('Y-m-d');
            $dayAllow = MyFormat::modifyDays($dayAllow, 5, '-');
            if(!empty($this->last_update_time)){
                $ok = MyFormat::compareTwoDate($this->last_update_time, $dayAllow);
            }
        }
        return $ok;
    }
    
    /**
     * @Author: ANH DUNG Sep 23, 2015
     * @Todo: show list info customer
     */
    public function SupportCustomerInfo() {
        $res = "";
        $classHighLight = '';
        $classHighLightAlert = '';// Đối với GGNV khi quá thời hạn 1 tuần từ ngày duyệt lần 3 => chưa hoàn thành thì hight light
        if($this->status == self::STATUS_APPROVED_3){
            $dateExpiry = MyFormat::modifyDays($this->approved_date_level_3, 7);
            if(MyFormat::compareTwoDate(date('Y-m-d'), $dateExpiry)){
                $classHighLightAlert = 'high_light_tr';
            }
        }
        
        $deloy_by = $this->getDeloyHtml();
        $res .= "<p><span class='item_b $classHighLightAlert'>Đơn Vị Thưc Hiện</span>: $deloy_by</p>";
        
        if(!empty($this->time_doing_real) && $this->time_doing_real!= '0000-00-00 00:00:00'){
            if(empty($classHighLightAlert)){
                $classHighLight = "high_light_tr1";
            }
            $label = $this->getAttributeLabel('time_doing_real');
            $time = $this->getTimeDoingReal();
            $res .= "<p><span class='item_b $classHighLight'>$label</span>: $time</p>";
        }
        
        if(is_null( $this->rCustomer)){
            return $this->customer_id;
        }
        $mCustomer = $this->rCustomer;
        $mSale = Users::model()->findByPk($mCustomer->sale_id);
        $cName = Users::GetInfoField($mCustomer, "first_name");
        $cAddress = Users::GetInfoField($mCustomer, "address");
        $type = $this->type_customer?CmsFormatter::$CUSTOMER_BO_MOI[$this->type_customer]:"";
        $res .= "<p><span class='item_b'>Khách Hàng</span>: $cName</p>";
        $res .= "<p><span class='item_b'>Loại KH</span>: $type</p>";
        $res .= "<p><span class='item_b'>Đ/C</span>: $cAddress</p>";
        if($mSale){
            $res .= "<p><span class='item_b'>NV Kinh Doanh</span>: $mSale->first_name</p>";
        }
        $mApproved = $this->rUidApproved;
        if($mApproved){
            $label = "Người duyệt cấp trên";
            $res .= "<p><span class='item_b'>$label</span>: $mApproved->first_name</p>";
        }
        
        if(trim($this->note) != ''){
            $note = nl2br($this->note);
            $res .= "<p><span class='item_b'>Ghi Chú</span>: $note</p>";
        }
        if(trim($this->note_technical) != ''){
            $note = nl2br($this->note_technical);
            $res .= "<p><span class='item_b'>NV Kỹ thuật ghi chú</span>: $note</p>";
        }
        
        $reasonNotDoing = $this->getReasonNotDoing();
        if($reasonNotDoing != ''){
            $res .= "<p><span class='item_b'>Lý do không thực hiện</span>: $reasonNotDoing</p>";
        }
        
        return $res;
    }
    
    /**
     * @Author: ANH DUNG Oct 06, 2015
     * @Todo: show list info customer less
     */
    public function SupportCustomerInfoLess() {
        $res = "";
        $deloy_by = $this->getDeloyHtml();
//        $res .= "<p><span class='item_b'>Đơn Vị Thưc Hiện</span>: $deloy_by</p>";
        
        if(!empty($this->time_doing_real) && $this->time_doing_real!= '0000-00-00 00:00:00'){
            $label = $this->getAttributeLabel('time_doing_real');
            $time = $this->getTimeDoingReal();
            $res .= "<p><span class='item_b'>$label</span>: $time</p>";
        }
        $mCustomer = $this->rCustomer;
        $mSale = Users::model()->findByPk($mCustomer->sale_id);
        $cName = Users::GetInfoField($mCustomer, "first_name");
        $cAddress = Users::GetInfoField($mCustomer, "address");
        $type = $this->type_customer?CmsFormatter::$CUSTOMER_BO_MOI[$this->type_customer]:"";
        $res .= "<p><span class='item_b'>Khách Hàng</span>: $cName</p>";
//        $res .= "<p><span class='item_b'>Loại KH</span>: $type</p>";
        $res .= "<p><span class='item_b'>Đ/C</span>: $cAddress</p>";
        if($mSale){
//            $res .= "<p><span class='item_b'>NV Kinh Doanh</span>: $mSale->first_name</p>";
        }
        
//        $mApproved = $this->rUidApproved;
//        if($mApproved){
//            $label = "Người duyệt cấp trên";
//            $res .= "<p><span class='item_b'>$label</span>: $mApproved->first_name</p>";
//        }
//        if(trim($this->note) != ''){
//            $note = nl2br($this->note);
//            $res .= "<p><span class='item_b'>Ghi Chú</span>: $note</p>";
//        }
        
//        if(trim($this->note_technical) != ''){
//            $note = nl2br($this->note_technical);
//            $res .= "<p><span class='item_b'>NV Kỹ thuật ghi chú</span>: $note</p>";
//        }
        return $res;
    }
    
    /**
     * @Author: ANH DUNG Sep 23, 2015
     */
    public function getSale() {
        $mCustomer = $this->rCustomer;
        if($mCustomer){
            $mSale = Users::model()->findByPk($mCustomer->sale_id);
            return $mSale ? " $mSale->first_name" : '';
        }
        return '';
    }
    
        /**
     * @Author: ANH DUNG Sep 23, 2015
     * @Todo: deloy by html 
     */
    public function getDeloyHtml() {
        $str = '';
        $deloy_by = $this->getArrayDeloyBy();
        if(!is_array($deloy_by) && !empty($deloy_by)): 
            $deloy_by = array($deloy_by);
        endif;
        
        
        if(is_array($deloy_by)): 
            $aModelDeloy  = Users::getOnlyArrayModelByArrayId($deloy_by, array('order'=>'role_id ASC'));
            $str .= "<table class=''> ";
//                $str .= "<thead> ";
//                    $str .= "<tr> ";
//                        $str .= "<th class='item_c'>#</th> ";
//                        $str .= "<th class='item_code item_c w-300'>Người Thực Hiện</th> ";      
//                    $str .= "</tr> ";
//                $str .= "</thead> ";
                $str .= "<tbody> ";
                    foreach($aModelDeloy as $key=>$mUser):
                    $str .= "<tr> ";
                        $str .= "<td class='item_c order_no'>".($key+1).".</td> ";
                        $str .= "<td class='uid_$mUser->id'>".$mUser->getNameWithRole()."</td> ";
                    $str .= "</tr> ";
                    endforeach;
                $str .= "</tbody> ";
            $str .= "</table> ";
        
        endif;

       return $str; 
    }
    
    /**
     * @Author: ANH DUNG Sep 24, 2015
     * @Todo: check xem có phải là record này của nV kỹ thuật tạo
     * và sale chỉ việc vào update thôi, không phải là record của sale
     */
    public function isScenarioSaleUpdate() {
        $cRole = Yii::app()->user->role_id;
        $cUid = Yii::app()->user->id;
        if(!empty($this->id) && $cRole == ROLE_SALE && $this->uid_login !=$cUid )// không phải của sale tạo
        {
            return true;
        }
        return false;
    }
    
    /**
     * @Author: ANH DUNG Sep 24, 2015
     */
    public function getSaleApproved() {
        if(isset($this->ARR_SALE_APPROVED[$this->sale_approved])){
            return $this->ARR_SALE_APPROVED[$this->sale_approved];
        }
        return '';
    }
    
    /**
     * @Author: ANH DUNG Sep 24, 2015
     * @Todo: Handle hide approved head
     * xử lý phan cap nhan vien kinh doanh thi thay truong phong
     * truong phong thay giam doc kinh doanh
     * giam doc kinh doanh thay giam doc
     * @Param: $model
     */
    public function HideSomeRoleWhenUpdate(&$aUidApproved) {
        $cRole = Yii::app()->user->role_id;
        $aRoleCheck1 = array(ROLE_MONITORING_MARKET_DEVELOPMENT, ROLE_SALE, ROLE_SUB_USER_AGENT, ROLE_EMPLOYEE_MARKET_DEVELOPMENT, ROLE_E_MAINTAIN);
        if(in_array($cRole, $aRoleCheck1)){
            if($cRole != ROLE_EMPLOYEE_MARKET_DEVELOPMENT){// Now 14, 2016 xử lý cho CCS thấy anh HOàn để chọn duyệt
//                unset($aUidApproved[GasLeave::UID_DIRECTOR_BUSSINESS]);
            }
//            unset($aUidApproved[GasLeave::UID_DIRECTOR_BUSSINESS]);
            if($this->status != GasSupportCustomer::STATUS_APPROVED_3){// Now 08, 2016 xử lý cho đại lý tự hoàn thành
                unset($aUidApproved[GasLeave::UID_DIRECTOR]);
//                unset($aUidApproved[GasLeave::UID_DIRECTOR_BUSSINESS]);// Add Aug2918
            }
//            unset($aUidApproved[GasLeave::UID_HEAD_TECHNICAL]);// Close on Apr 09, 2017 đưa anh Dũng lên duyệt lần 1
            
        }elseif(in_array($cRole, self::$aRoleLevel1)){
            unset($aUidApproved[GasLeave::UID_HEAD_GAS_BO]);
            unset($aUidApproved[GasLeave::UID_HEAD_GAS_MOI]);
            unset($aUidApproved[GasLeave::UID_MANAGER_BUSSINESS]); // Trưởng Phòng Kinh Doanh - Nguyễn Thanh Hải
            unset($aUidApproved[GasLeave::UID_CHIEF_MONITOR]);// Dec 18, 2015 Tổng giám sát Trần Trung Hiếu
//            if($this->status != GasSupportCustomer::STATUS_APPROVED_3){
            if($this->status < GasSupportCustomer::STATUS_APPROVED_3){// Dec 19, 2016 xử lý cho A Quý cập nhật biên bản hoàn thành
                unset($aUidApproved[GasLeave::UID_DIRECTOR]);
            }
            unset($aUidApproved[GasLeave::PHUC_HV]);
            unset($aUidApproved[GasConst::UID_PHUONG_ND]);
            
        }elseif(in_array($cRole, self::$aRoleLevel2)){
            unset($aUidApproved[GasLeave::UID_HEAD_GAS_BO]);
            unset($aUidApproved[GasLeave::UID_HEAD_GAS_MOI]);
            unset($aUidApproved[GasLeave::UID_MANAGER_BUSSINESS]);
            unset($aUidApproved[GasLeave::UID_CHIEF_MONITOR]);//Dec 18, 2015 Tổng giám sát Trần Trung Hiếu
//            unset($aUidApproved[GasLeave::UID_DIRECTOR_BUSSINESS]);
            unset($aUidApproved[GasLeave::UID_HEAD_TECHNICAL]);
            unset($aUidApproved[GasLeave::PHUC_HV]);
            unset($aUidApproved[GasConst::UID_PHUONG_ND]);
        }
    }
    
    /** @Author: ANH DUNG Jan 05, 2019
     *  @Todo: init model
     **/
    public function initSessionMaterial() {
        $mAppCache = new AppCache();
        if(!isset(Yii::app()->session)){
            $aModelMaterial = $mAppCache->getMasterModel('GasMaterials', AppCache::ARR_MODEL_MATERIAL);
        }else{
            $session=Yii::app()->session;
            $aModelMaterial = $session['MATERIAL_SUPPORT'];
        }
        return $aModelMaterial;
    }
    
    /**
     * @Author: ANH DUNG Sep 289, 2015
     * @Todo: something
     */
    public function formatGasSupportCustomerItemName(){
        if($this->id<246){ // vì 245 là max id hiện tại, những cái cũ sẽ view theo kiểu cũ :d
            return $this->getOldHtmlView();
        }
        $aModelMaterial = $this->initSessionMaterial();
        $tfood = false;
        $t1=0; $t2=0;$t_support=0;
        $str = "<table  cellpadding='0' cellspacing='0'> ";
        $str .= "<tr>";
 //       $str .= "<td>#</td>";
        $str .= "<td>Thiết bị</td>";
        $str .= "<td>ĐV</td>";
        $str .= "<td>Slg</td>";
        $str .= "<td>Giá</td>";
        $str .= "<td>Thành tiền</td>";
        $str .= "<td>NV KD</td>";
        $str .= "<td>TP BM</td>";
        $str .= "<td>GĐ KD</td>";
        $str .= "<td>Giám Đốc</td>";
        $str .= "<td>Tiền hỗ trợ</td>";
        $str .= "<td>Tiền phải thu</td>";
        $str .= "</tr>";

        foreach($this->rDetail as $key=>$item){
           $tfood = true;
           $RowAmount = $item->AmountAfterPercentFinal();
           $row_support = $item->amount - $RowAmount;
           
           $str .= "<tr>";
//           $type = GasSupportCustomer::$ARR_TYPE[$item->type];
//           $str .= "<br> <b>".(++$key).". </b>$type - {$item->item_name}";
           
//           $str .= "<td>".($key+1)."</td>";
           $str .= "<td  style='border:1px solid #000;'>{$aModelMaterial[$item->materials_id]['name']}</td>";
           $str .= "<td  style='border:1px solid #000;'>{$aModelMaterial[$item->materials_id]['unit']}</td>";
           $str .= "<td style='text-align:right; border:1px solid #000;'>".ActiveRecord::formatCurrency($item->qty)."</td>";
           $str .= "<td style='text-align:right; border:1px solid #000;'>".ActiveRecord::formatCurrency($item->price)."</td>";
           $str .= "<td style='text-align:right; border:1px solid #000;'>".ActiveRecord::formatCurrency($item->amount)."</td>";
           $str .= "<td style='text-align:center; border:1px solid #000;'>{$item->getPercentText(1)}</td>";
           $str .= "<td style='text-align:center; border:1px solid #000;'>{$item->getPercentText(2)}</td>";
           $str .= "<td style='text-align:center; border:1px solid #000;'>{$item->getPercentText(3)}</td>";
           $str .= "<td style='text-align:center; border:1px solid #000;'>{$item->getPercentText(4)}</td>";
           $str .= "<td style='text-align:right; border:1px solid #000;'>".ActiveRecord::formatCurrency($row_support)."</td>";
           $str .= "<td style='text-align:right; border:1px solid #000;'>".ActiveRecord::formatCurrency($RowAmount)."</td>";
           $str .= "</tr>";
           $t1 += $item->amount;
           $t2 += $RowAmount;
           $t_support += $row_support;
        }
        $this->sumAmountHoTro = $t_support;
        $this->sumAmountDauTu = $t2;
        if($tfood){
            $t1 = ActiveRecord::formatCurrency($t1);
            $t2 = ActiveRecord::formatCurrency($t2);
            $span =  '';
            if(!$this->from_cron_mail){
                $span = "<span class='AmountSupport display_none' >$t_support</span>";
            }
            $t_support = ActiveRecord::formatCurrency($t_support);

            $str .= "<tr>";
            $str .= "<td style='text-align:right;font-weight: bold;' colspan='4'><b>Tổng cộng</b></td>";
            $str .= "<td style='text-align:right;font-weight: bold;'><b>$t1</b></td>";
            $str .= "<td></td><td></td><td></td><td></td>";
            $str .= "<td style='text-align:right;font-weight: bold;'><b>$t_support</b>$span</td>";
            $str .= "<td style='text-align:right;font-weight: bold;'><b>$t2</b></td>";
            $str .= "</tr>";
        }

        $str .= "</table>";
        $str = "<p style='display:inline-block;'>$str</p>";
        return $str; 
    }
    
    /**
     * @Author: ANH DUNG Jul 01, 2016
     * @Todo: Print for Xưởng phước Tân
     */
    public function formatPrint(){
        if($this->id<246){ // vì 245 là max id hiện tại, những cái cũ sẽ view theo kiểu cũ :d
            return "";
        }
        $tfood = false;
        $t1=0; $t2=0;$t_support=0;
        $sumQty = 0;
        $sumAmount = 0;
        $session=Yii::app()->session;
        $str = "<table  cellpadding='0' cellspacing='0'> ";
        $str .= "<tr>";
 //       $str .= "<td>#</td>";
        $str .= "<td style='text-align:center; font-weight:bold;'>Thiết bị</td>";
        $str .= "<td style='text-align:center; font-weight:bold;'>ĐV</td>";
        $str .= "<td style='text-align:center; font-weight:bold;'>Slg</td>";
        $str .= "<td style='text-align:center; font-weight:bold;'>Giá</td>";
        $str .= "<td style='text-align:center; font-weight:bold;'>Thành tiền</td>";
        $str .= "<td style='text-align:center; font-weight:bold;'>SL xuất thực tế</td>";
        $str .= "</tr>";
       
        foreach($this->rDetail as $key=>$item){
           $tfood = true;
           $RowAmount = $item->AmountAfterPercentFinal();
           $row_support = $item->amount - $RowAmount;
           $sumQty += $item->qty;
           $sumAmount += $item->amount;
           $str .= "<tr>";
//           $type = GasSupportCustomer::$ARR_TYPE[$item->type];
//           $str .= "<br> <b>".(++$key).". </b>$type - {$item->item_name}";
           
//           $str .= "<td>".($key+1)."</td>";
           $str .= "<td  style='border:1px solid #000;'>{$session['MATERIAL_SUPPORT'][$item->materials_id]['name']}</td>";
           $str .= "<td  style='border:1px solid #000;'>{$session['MATERIAL_SUPPORT'][$item->materials_id]['unit']}</td>";
           $str .= "<td style='text-align:right; border:1px solid #000;text-align:center;'>".ActiveRecord::formatCurrency($item->qty)."</td>";
           $str .= "<td style='text-align:right; border:1px solid #000;'>".ActiveRecord::formatCurrency($item->price)."</td>";
           $str .= "<td style='text-align:right; border:1px solid #000;'>".ActiveRecord::formatCurrency($item->amount)."</td>";
           $str .= "<td style='text-align:right; border:1px solid #000;'></td>";
           $str .= "</tr>";
           $t1 += $item->amount;
           $t2 += $RowAmount;
           $t_support += $row_support;
        }
        if($tfood){
            $t1 = ActiveRecord::formatCurrency($t1);
            $t2 = ActiveRecord::formatCurrency($t2);
            $t_support = ActiveRecord::formatCurrency($t_support);
            $str .= "<tr>";
            $str .= "<td style='text-align:right;font-weight: bold;' colspan='2'><b>Tổng cộng</b></td>";
            $str .= "<td style='text-align:right;font-weight: bold;text-align:center;'><b>".ActiveRecord::formatCurrency($sumQty)."</b></td>";
            $str .= "<td></td>";
            $str .= "<td style='text-align:right;font-weight: bold;'><b>".ActiveRecord::formatCurrency($sumAmount)."</b></td>";
            $str .= "<td style='text-align:right;font-weight: bold;'><b></b></td>";
            $str .= "</tr>";
        }

        $str .= "</table>";
        $str = "<p style='display:inline-block;'>$str</p>";
        return $str; 
    }
    
    /**
     * @Author: ANH DUNG Sep 29, 2015
     * @Todo: check cho phép update status nhiều lần nếu cấp trên chưa update
     */
    public function CanUpdateStatusNew(&$CanUpdate) {
        $cUid = Yii::app()->user->id;
        if( ($cUid==$this->approved_uid_level_1 && empty($this->approved_uid_level_2) ) ||
            ($cUid==$this->approved_uid_level_2 && empty($this->approved_uid_level_3) )
        ){
            $CanUpdate = true;
        }
    }
    
    /**
     * @Author: ANH DUNG Oct 06, 2015
     * @Todo: 
     */
    public function HandleUpdateTimeReal() {
        $this->last_update_time = date('Y-m-d H:i:s');
        $aAttributes = array('note_update_real_time');
        MyFormat::RemoveScriptOfModelField($this, $aAttributes);
        $aUpdate = array('time_doing_real', 'last_update_time', 'note_update_real_time');
        $this->update($aUpdate);
    }
    
    /**
     * @Author: ANH DUNG Oct 06, 2015
     */
    public function getNotUpdateRealTime() {
        return nl2br($this->note_update_real_time);
    }
    
    /**
     * @Author: ANH DUNG Dec 29, 2015
     * @Todo: get array value checkbox not doing
     */
    public function getArrayNotDoing() {
        return array(
            self::DOING_DESIGN_WRONG => "Thiết kế không đúng",
            self::DOING_MATERIAL_NOT_ENOUGH => "Vật tư không đủ",
            self::DOING_DEPLOY_WRONG => "Giao không đúng ngày",
        );
    }
    const DOING_DESIGN_WRONG = 1;
    const DOING_MATERIAL_NOT_ENOUGH = 2;
    const DOING_DEPLOY_WRONG = 3;
    public $after_doing_reason;
    
    // fix Dec 28, 2015
    public function getNoteLevel4() {
        $res = $this->getReasonNotDoing();
        if($res != ''){
//            $res .= "<strong> - ".implode(", ", $aReason)."</strong><br>";
            $res .= "<br>";
        }
        $res .=  nl2br($this->approved_note_level_4);
        return $res;
    }
    
    /**
     * @Author: ANH DUNG Dec 30, 2015
     */
    public function getReasonNotDoing() {
        $res = "";
        $aReason = array();
        if($this->chk_design_wrong){
            $aReason[] = "Thiết kế không đúng";
        }
        if($this->chk_material_not_enough){
            $aReason[] = "Vật tư không đủ";
        }
        if($this->chk_deploy_wrong){
            $aReason[] = "Giao không đúng ngày";
        }
        if(count($aReason)){
            $res = implode(", ", $aReason);
        }
        return $res;
    }
    
    /**
     * @Author: ANH DUNG May 10, 2016
     * đếm số lần tạo phiếu giao nv của 1 customer
     */
    public function countByCustomerId() {
        $criteria = new CDbCriteria();
        $criteria->compare("customer_id", $this->customer_id);
        return self::model()->count($criteria);
    }
    
    /**
     * @Author: ANH DUNG Nov 19, 2015
     */
    public function InitMaterial() {
        $session=Yii::app()->session;
        if(!isset($session['MATERIAL_SUPPORT'])){
            $session['MATERIAL_SUPPORT'] = MyFunctionCustom::getMaterialsModel(GasMaterialsType::$THIET_BI, array('GetAll'=>1));
        }
    }
    
    /**
     * @Author: ANH DUNG Jul 23, 2016
     * get action redirect when user create new or UPdate, vì CCS sẽ redirect khác index với các loại user khác
     */
    public function getActionIndex() {
        $cRole = Yii::app()->user->role_id;
        $actionRedirect = 'index';
        if(in_array($cRole, GasSupportCustomer::$aRoleCreateExt1)){// Jul 23, 2016
            $actionRedirect = 'hgd';
        }
        return $actionRedirect;
    }
    
    /**
     * @Author: ANH DUNG Jul 28, 2016
     * format : Pham Thanh Nghia Sep 24, 2018
     */
    public function getUidLogin($format = false) {
        $mUser = $this->rUidLogin;
        if($mUser && $format = false){
            return $mUser->getNameWithRole();
        }
        if($mUser && $format = true){
            return $mUser->first_name;
        }
        return "";
    }
    
    /**
     * @Author: ANH DUNG Sep 29, 2016
     * @Todo: với hgd thì duyệt 2 cấp Anh HOàn => Anh Long
     */
    public function setUserApproveHgd() {
        if($this->type_support == GasSupportCustomer::TYPE_SUPPORT_HGD){
            $this->to_uid_approved  = GasLeave::UID_DIRECTOR_BUSSINESS;
//        $this->status           = GasLeave::UID_DIRECTOR_BUSSINESS;
        }
    }
    
    /** @Author: Pham Thanh Nghia 27/06/2018
     *  @Todo: admin/gasSupportCustomerRequest/generate
     *  @Param: 
     **/
    public function buildArrDetailByRequest($aJson) {
        $this->aModelDetail = [];
        if(!is_array($aJson)){
            return ;
        }
        $aModelMaterial = $this->initSessionMaterial();
        foreach( $aJson as $key => $value){
//                $type = isset($aJson['type'][$key])?$aJson['type'][$key]:"";
            $aListModule    = (array)$value;
            if(isset($aListModule['detail']) && is_array($aListModule['detail'])){
                foreach ($aListModule['detail'] as $detail) {
                    $mDetail = new GasSupportCustomerItem();
                    $mDetail->materials_id  = $detail->materials_id;
                    $mDetail->qty           = $detail->qty;
                    $mDetail->price         = isset($aModelMaterial[$detail->materials_id]['price']) ? $aModelMaterial[$detail->materials_id]['price'] : 0;
                    $mDetail->percent       = 0;
                    $mDetail->amount        = $mDetail->qty * $mDetail->price;
                    $mDetail->materials_type_id = $detail->materials_type_id ;
                    $this->aModelDetail[] = $mDetail;
                }
            }
        }
    }
    
    
    /** @Author: Pham Thanh Nghia 29/06/2018
     *  @Todo: getStatus by id status
     **/
    public function getStatus(){
        $aStatus = GasSupportCustomer::$ARR_APPROVED;
        if(!empty($aStatus[$this->status])){
            return $aStatus[$this->status];
        }
        return '';
    }
    
    public function getTypeCustomer(){
        $aType = CmsFormatter::$CUSTOMER_BO_MOI;
        if(!empty($aType[$this->type_customer])){
            return $aType[$this->type_customer];
        }
        return '';
    }      
    
    public function getApprovedUid() {
        $mUser = $this->rApprovedUid;
        if($mUser){
            return $mUser->first_name;
        }
        return '';
    }
    
    /** @Author: ANH DUNG Jul 05, 2018
     *  @Todo: check user can export excel
     **/
    public function canExportExcel() {
        $cUid = MyFormat::getCurrentUid();
        $aUidAllow = [GasConst::UID_CHIEN_BQ, GasConst::UID_ADMIN, GasConst::UID_CHAU_LNM, GasConst::UID_NGAN_DTM];
        return in_array($cUid, $aUidAllow);
    }
     /** @Author: Pham Thanh Nghia 9/7/2018
     *  @Todo: get Sale for customer
     **/
    public function getSaleForCustomer() {
        $mUser = Users::model()->findByPk($this->customer_id);    
        if($mUser){
            return $mUser->first_name;
        }
        return '';
    }
    
    /** @Author: ANH DUNG Sep 19, 2018
     *  @Todo: kiểm tra KH đã đc upload file chưa, nếu chưa thì ko cho tạo phiếu giao NV
     **/
    public function checkUploadFile() {
        if($this->customer_id < 1653032){
            return ;
        }
        $mEmployeesImages = new EmployeesImages();
        $mEmployeesImages->users_id     = $this->customer_id;
        $mEmployeesImages->type         = [EmployeesImages::TYPE_CUSTOMER_CONTRACT, EmployeesImages::TYPE_CUSTOMER_CONTRACT_ASSEST];
        if(empty($mEmployeesImages->countByUser())){
            $this->addError('customer_id', 'Khách hàng này chưa bổ sung file hợp đồng, không thể tạo phiếu giao nhiệm vụ');
        }
    }
    /** @Author: Pham Thanh Nghia Nov 16, 2018
     *  @Todo: tạo danh sách customer_id => tien dau tu
     **/
    public  function setCacheArraytCustomerInvert() {
        $aResult = [];
        $criteria = new CDbCriteria();
        $criteria->compare('t.status', self::STATUS_APPROVED_3);
        $criteria->order = "t.id DESC";
        $models =  GasSupportCustomer::model()->findAll($criteria);
        foreach ($models as $model) {
            if(!isset($aResult[$model->customer_id])){
                $aResult[$model->customer_id] = $this->getAmountSupport($model);
            }else{
                $aResult[$model->customer_id] += $this->getAmountSupport($model);
            }
        }
        $mAppCache = new AppCache();
        $mAppCache->setCache(AppCache::LISTDATA_CUSTOMER_INVEST, $aResult,0);
    }
    public function getAmountSupport($model){ // doi hoi anh dung
        $t_support = 0;$t1=0;
        foreach($model->rDetail as $key=>$item){
//           $RowAmount = $item->AmountAfterPercentFinal(); 
//           $row_support = $item->amount - $RowAmount;
//           $t_support += $row_support; // tiền đầu tư
           $t1 += $item->amount;
        }
        return $t1;
    }
    /** @Author: Pham Thanh Nghia Dec 2, 2018
     *  @Todo: lấy danh sách customer_id => tien dau tu
     **/
    public  function getCacheArraytCustomerInvert() {
        $mAppCache = new AppCache();
        $aResult = $mAppCache->getCache(AppCache::LISTDATA_CUSTOMER_INVEST);
        if(empty($aResult)){
            $this->setCacheArraytCustomerInvert();
            return $mAppCache->getCache(AppCache::LISTDATA_CUSTOMER_INVEST);
        }
        return $aResult;
    }
    
    /** @Author: ANH DUNG Dec 20, 2018
     *  @Todo: update support_customer_id to gas_gas_support_customer_request
     **/
    public function updateBackIdToRequest() {
        if(!isset($_GET['id_request'])){
            return ;
        }
        $mRequest = GasSupportCustomerRequest::model()->findByPk($_GET['id_request']);
        if($mRequest){
            $mRequest->support_customer_id = $this->id;
            $mRequest->update(['support_customer_id']);
        }
    }
    
    /** @Author: ANH DUNG Jan 05, 2019
     *  @Todo: map info of GasSupportCustomerRequest - move code of Pham Thanh Nghia from view 
     **/
    public function mapInfoRequest() {
        /** @Author: Pham Thanh Nghia 2018
        *  @Todo: lấy yêu cầu từ admin/gasSupportCustomerRequest
        **/
        if(isset($_GET['id_request'])){
           $mGasSupportCustomerRequest     = GasSupportCustomerRequest::model()->findByPk($_GET['id_request']);
           $this->customer_id             = $mGasSupportCustomerRequest->customer_id;
           $this->note_technical          = $mGasSupportCustomerRequest->note;
           $aJson = json_decode($mGasSupportCustomerRequest->json);
           $this->buildArrDetailByRequest($aJson );
       }
    }

    /** @Author: LocNV Jun 21, 2019
     *  @Todo: tạo url chuyển sang trang tạo thẻ kho với danh sách mã vật tư tương ứng
     *  @Param:
     * LOC011
     **/
    public function getURLCreateGasStoreCard() {
        $res = '';
        if ($this->checkRoleCreateGasStoreCard()) {
            $url = Yii::app()->createAbsoluteUrl("admin/gasstorecard/create/type_store_card/2", array('support_customer_id' => $this->id));
            $res = '<a target="_blank" href="' . $url . '">Tạo thẻ kho</a>';
        }
        return $res;
    }

    /** @Author: LocNV Jun 21, 2019
     *  @Todo: kiểm tra quyền để hiển thị link tạo thẻ kho
     *  @Param:
     * LOC011
     **/
    public function checkRoleCreateGasStoreCard() {
        $aRoleAllow = [ROLE_CHIEF_ACCOUNTANT, ROLE_ADMIN, ROLE_SUB_USER_AGENT];
        $cRole      = MyFormat::getCurrentRoleId();
        return in_array($cRole, $aRoleAllow);
    }
}