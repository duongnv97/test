<?php

/**
 * This is the model class for table "{{_sell}}".
 * @property integer $hour
 */
class Sell extends BaseSpj
{ 
    public $reward_id,$autocomplete_name_reward,$rewardApp;
    public $type_report_agent;
    public $monthCheckTelesale = 4, $isCancelWrong = 0,$isWebSumAll = false, $isCustomerBookOrder = false, $telesaleCheck = false, $ptttUid = 0, $typeReport = 1, $mBeforeUpdate = null, $isChangePrice = 0, $mEventComplete = null, $aDetailTransaction,$aDetail, $autocomplete_name, $autocomplete_name_2, $autocomplete_name_3, $autocomplete_name_4, $autocomplete_name_5, $materials_name,$target_v1,$target_v2,$type_target,$autocomplete_target_v1,$autocomplete_target_v2;
    public $is_bu_vo_zero = 0, $materials_id_gas = 0, $MAX_ID, $date_from, $date_to, $mCustomer, $mAgent, $date_month, $date_year, $date_from_ymd, $date_to_ymd, $oldCode='';
    public static $ROLE_EMPLOYEE_SEARCH_AUTOCOMPLETE = array(ROLE_TELESALE, ROLE_EMPLOYEE_MAINTAIN, ROLE_EMPLOYEE_MARKET_DEVELOPMENT);
    public static $ROLE_UID_LOGIN = array(ROLE_TELESALE, ROLE_ACCOUNTING_AGENT, ROLE_ACCOUNTING_ZONE, ROLE_CALL_CENTER, ROLE_DIEU_PHOI);// KTBH, KT lưu động, KTKV
    public static $ROLE_CCS_SEARCH = array(ROLE_TELESALE, ROLE_EMPLOYEE_MARKET_DEVELOPMENT);
    public $materials_parent_id_gas = 0, $materials_id_vo = 0, $totalGasAmount = 0, $aInventoryAgent=[], $discountGas24h = 0, $employeeCompleteApp=false, $is_auto_gen_storecard=0, $aRowInsert=[], $getDataInsert=false, $mTransaction=null, $mTransactionHistory=null, $runUpdateStatusTransaction = true;// cờ kiểm tra xem có chạy updateTransaction không
    
    const TYPE_TIMER    = 1;// type giao hàng hẹn giờ
    const TYPE_BQV_2    = 1;
    const TYPE_BQV_1    = 2;
    const TYPE_INDEX_AGENT = 1;
    
    const STATUS_NEW    = 1;//  status: 1: Mới. 2: Thu Tiền, 3: Hủy
    const STATUS_PAID   = 2;
    const STATUS_CANCEL = 3;
    
    const IS_FIRST_ORDER    = 1;
    
    const ORDER_TYPE_NORMAL             = 1;// Aug 26, 2016 - 1: normal, 2: bộ bình, 3: Thế chân
    const ORDER_TYPE_BO_BINH            = 2;
    const ORDER_TYPE_THE_CHAN           = 3;
    const ORDER_TYPE_THU_VO             = 4;
    const ORDER_TYPE_BAN_GAS_VO         = 5;// Sep 11, 2016, bán GAS + vỏ hay là bộ bình không val dây
    public static $ARR_TYPE_BAN_VO = array(
        Sell::ORDER_TYPE_BO_BINH,
        Sell::ORDER_TYPE_THE_CHAN,
        Sell::ORDER_TYPE_BAN_GAS_VO,
    );
    
    const STATUS_THE_CHAN_NEW       = 1;// Aug 26, 2016 chưa xử lý trạng thái thế chân 1: new, 2: paid back customer
    const STATUS_THE_CHAN_PAID      = 2;
    const SOURCE_APP_SECOND         = -1;// view Đơn app lần 2
    const SOURCE_WINDOW             = 1;// from c# window Now 06, 2016
    const SOURCE_APP                = 2;// from transaction
    const SOURCE_WEB                = 3;// from call center
    
    const PROMOTION_DISCOUNT = 20000;// Jul 18, 2016 discount neu khong lay hang KM
    
    public $searchGas, $searchVo, $searchKm, $autocomplete_gas, $autocomplete_vo, $autocomplete_km;
    public $modelMonitorOld = null, $minutesAgentUpdate = 20, $discount_type, $isChangeNewEmployee=false;
    public $hour; // Store hour for search
    const DISCOUNT_DEFAULT  = 1;
    const DISCOUNT_CHANGE   = 2;
    
    const DISCOUNT_CHANGE_ZERO  = 1;// sử dụng như change về 0 khi GN sửa ck của bán hàng
    const CUSTOMER_NEW          = 1;
    const CUSTOMER_OLD          = 2;
    
    const REMAIN_ALLOW          = 3;// max cân gas dư 3kg
    const REMAIN_ALLOW_5        = 5;
    const REMAIN_ALLOW_6        = 6;
    const REMAIN_ALLOW_7        = 7;
    const REMAIN_ALLOW_FULL     = 11;
    
    const REPORT_APP            = 1;// default
    const REPORT_WEB            = 2;
    
    const TYPE_DISCOUNT_DEFAULT_20  = 0;// Jun2518 loại chiết khấu khi tạo mới
    const TYPE_DISCOUNT_ZERO        = 1;// không có ck
    const TYPE_DISCOUNT_50          = 50000;// ck 50k
    const TYPE_DISCOUNT_100         = 100000;// ck 100k
    // Aug 31, 2018 Nghia ,trạng thái kiểm tra audit
    const AUDITED       = 1;
    const NOT_AUDIT     = 0; // default
    
    public function getArrayTypeDiscount() {
        return array(
            Sell::TYPE_DISCOUNT_DEFAULT_20  => 'Mặc định 20k',
            Sell::TYPE_DISCOUNT_ZERO        => 'Không chiết khấu',
            Sell::TYPE_DISCOUNT_50          => 'Giảm 50k',
            Sell::TYPE_DISCOUNT_100         => 'Giảm 100k',
        );
    }
    
    
    /** @Author: DungNT Jun 30, 2016 */
    public function getArrayStatus() {
        return array(
            Sell::STATUS_NEW        => 'Mới',
            Sell::STATUS_PAID       => 'Hoàn Thành',
            Sell::STATUS_CANCEL     => 'Hủy',
        );
    }
    public function getArrayDiscount() {
        return array(
            Sell::DISCOUNT_DEFAULT  => 'Mặc định',
            Sell::DISCOUNT_CHANGE   => 'Thay đổi - nhập số tiền khác',
        );
    }
    public function getArraySource() {
        return array(
            Sell::SOURCE_WINDOW     => 'PMBH',
            Sell::SOURCE_APP        => 'APP',
            Sell::SOURCE_APP_SECOND => 'APP lần 2',
        );
    }
    public function getArrayCustomerNewOld() {
        return array(
            Sell::CUSTOMER_NEW => 'KH Mới',
            Sell::CUSTOMER_OLD => 'KH Cũ',
        );
    }
    public function getPriceVoBinh() {
        return 300000;
    }
    public function getUserAnhHieu() {
        return [793887, 30760];
    }
    
    /**
     * @Author: DungNT Aug 26, 2016
     */
    public function getArrayOrderType() {
        return array(
            Sell::ORDER_TYPE_NORMAL     => 'Bình thường',
            Sell::ORDER_TYPE_BO_BINH    => 'Bộ bình',// Aug1317 bỏ
            Sell::ORDER_TYPE_BAN_GAS_VO => 'Gas + Vỏ',
//            Sell::ORDER_TYPE_THE_CHAN   => 'Thế chân',
            Sell::ORDER_TYPE_THU_VO     => 'Thu vỏ',
        );
    }
    public function getOrderType() {
        $aType = $this->getArrayOrderType();
        return isset($aType[$this->order_type]) ? $aType[$this->order_type] : '';
    }
    public function getArrayPlatform() {
        return [
            UsersTokens::PLATFORM_ANDROID  => 'Android',
            UsersTokens::PLATFORM_IOS      => 'Ios',
        ];
    }
    public function getArrayPlatformText() {
        $aPlatform = $this->getArrayPlatform();
        return isset($aPlatform[$this->platform]) ? $aPlatform[$this->platform] : '';
    }
    
    /**
     * @Author: DungNT Jan 08, 2017
     * @Todo: Lấy text để view dưới app + Web edit cho loại bán hàng + số tiền
     */
    public function getOrderTypeAppView($order_type) {
        $aType = $this->getArrayOrderType();
        return isset($aType[$order_type]) ? $aType[$order_type] : '';
    }
    
    /**
     * @Author: DungNT Jun 30, 2016
     */
    public function getStatus() {
        $aStatus = $this->getArrayStatus();
        return isset($aStatus[$this->status]) ? $aStatus[$this->status] : "";
    }
    
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }
    // lấy số ngày cho phép user cập nhật
    public function getDayAllowUpdate(){
//        return Yii::app()->setting->getItem('days_update_sell');
        return Yii::app()->params['days_update_sell'];
    }
    public function getRoleAllowCreate() {
        return [ROLE_EMPLOYEE_OF_LEGAL, ROLE_TELESALE, ROLE_CALL_CENTER, ROLE_DIEU_PHOI, ROLE_ACCOUNTING_ZONE, ROLE_ACCOUNTING];
    }
    
    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{_sell}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        return array(
            array('agent_id, customer_id, type_customer', 'required', 'on'=>'WindowCreate,WindowUpdate'),
            array('phone, agent_id, customer_id, uid_login, created_date_only, status', 'required', 'on'=>"WebCreate, WebUpdate"),
            array('note, id, customer_id, type_customer, sale_id, agent_id, employee_maintain_id, uid_login, created_date_only, created_date', 'safe'),
            array('discount_type, amount_discount, order_type, type_amount, order_type_status, pageSize, searchGas, searchVo, searchKm, qty_discount, code_no, status, date_from,date_to', 'safe'),
            array('is_timer, telesaleCheck, isCancelWrong, delivery_timer, ptttUid, gas_remain, gas_remain_amount, isChangePrice, call_id, customer_new, high_price, pttt_code, status_cancel, phone, is_auto_gen_storecard, is_bu_vo_zero, amount_bu_vo, address, province_id, total, grand_total, source, transaction_history_id, app_promotion_user_id, promotion_id, promotion_amount, promotion_type', 'safe'),
            array('target_v1,target_v2,type_target,type_report_agent', 'safe'),
            array('rewardApp,reward_id', 'safe'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        return array(
            'rCustomer' => array(self::BELONGS_TO, 'Users', 'customer_id'),
            'rAgent' => array(self::BELONGS_TO, 'Users', 'agent_id'),
            'rEmployeeMaintain' => array(self::BELONGS_TO, 'Users', 'employee_maintain_id'),
            'rSale' => array(self::BELONGS_TO, 'Users', 'sale_id'),
            'rUidLogin' => array(self::BELONGS_TO, 'Users', 'uid_login'),
            'rLastUpdateBy' => array(self::BELONGS_TO, 'Users', 'last_update_by'),
            'rDetail' => array(self::HAS_MANY, 'SellDetail', 'sell_id'),
            'rSearchGas' => array(self::BELONGS_TO, 'GasMaterials', 'searchGas'),
            'rPromotion' => array(self::BELONGS_TO, 'AppPromotion', 'promotion_id'),
            'rTransactionHistory' => array(self::BELONGS_TO, 'TransactionHistory', 'transaction_history_id'),
            'rCallCenter' => array(self::BELONGS_TO, 'Users', 'call_center_id'),
            'rComment' => array(self::HAS_ONE, 'GasComment', 'belong_id',
                'on'=>'rComment.type='. GasComment::TYPE_4_SELL_CANCEL,
                'order' => 'rComment.id DESC',
            ),
            'rAppPromotionUser' => array(self::BELONGS_TO, 'AppPromotionUser', 'app_promotion_user_id'),
            'rReward' => array(self::BELONGS_TO, 'Reward', 'reward_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'customer_id' => 'Khách Hàng',
            'type_customer' => 'Loại KH',
            'sale_id' => 'CCS',
            'agent_id' => 'Đại lý',
            'employee_maintain_id' => 'NV Giao Nhận',
            'uid_login' => 'Người tạo',
            'created_date_only' => 'Created Date Only',
            'created_date' => 'Ngày Tạo',
            'note' => 'Ghi Chú',
            'materials_name' => 'Vật tư',
            'date_from' => 'Từ Ngày',
            'date_to' => 'Đến Ngày',
            'code_no' => 'Mã Số',
            'status' => 'Trạng Thái',
            'created_date_only' => 'Ngày Bán',
            'searchGas' => 'Vật Tư',
            'searchVo' => 'Vỏ',
            'searchKm' => 'Khuyến Mãi',
            'pageSize' => 'Số Dòng Hiển Thị',
            'order_type' => 'Loại Bán Hàng',
            'type_amount' => 'Số Tiền',
            'amount_discount' => 'Tiền chiết khấu',
            'order_type_status' => 'Kiểm tra hủy đơn hàng' , // 0 : chưa kiểm tra, khác 0 là loại hủy đơn hàng //'Trạng thái thế chân',// trạng thái thế chân 1: new, 2: paid back customer
            'discount_type' => 'Loại chiết khấu',
            'promotion_amount' => 'Tiền khuyến mãi',
            'source' => 'Nguồn tạo',
            'address' => 'Đ/C giao hàng',
            'amount_bu_vo' => 'Bù vỏ',
            'province_id' => 'Tỉnh',
            'is_bu_vo_zero' => 'Không bù vỏ',
            'is_auto_gen_storecard' => 'Cập nhật thẻ kho HGĐ',
            'phone' => 'Điện thoại liên hệ',
            'status_cancel' => 'Lý do hủy',
            'pttt_code' => 'Bình quay về PTTT',
            'high_price' => 'Giá cao - Gửi chuyên viên',
            'customer_new' => 'Loại KH',
            'isChangePrice' => 'Cập nhật giá sửa',
            'gas_remain' => 'Gas dư',
            'gas_remain_amount' => 'Tiền Gas dư',
            'ptttUid' => 'PTTT BQV',
            'delivery_timer' => 'Hẹn giờ giao',
            'isCancelWrong' => 'Phạt hủy sai lý do',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        $cRole  = MyFormat::getCurrentRoleId();
        $cUid   = MyFormat::getCurrentUid();
        $mAppCache = new AppCache();
        $mAppPromotion = new AppPromotion();
        $criteria=new CDbCriteria;
        $criteria->compare('t.code_no', trim($this->code_no));
        $criteria->compare('t.status',$this->status);
        if($this->source == Sell::SOURCE_APP_SECOND){
            $criteria->addCondition('t.uid_login=' . GasConst::UID_ADMIN);
        }else{
            $criteria->compare('t.source',$this->source);
        }
        $criteria->compare('t.status_cancel',$this->status_cancel);
        $criteria->compare('t.high_price',$this->high_price);
        $criteria->compare('t.customer_new',$this->customer_new);
        if(empty($this->status)){
//            $criteria->addNotInCondition('t.status', array(Sell::STATUS_CANCEL));
        }
        if(!empty($this->phone)){
            $this->phone = UsersPhone::formatPhoneSaveAndSearch($this->phone);
            $criteria->compare('t.phone',$this->phone);
        }
        if(!empty($this->high_price)){
            $criteria->addCondition('t.high_price=1');
        }
        if(!empty($this->promotion_amount)){
            $criteria->addCondition('t.promotion_amount>0 AND t.promotion_id < 1');
        }
        if(!empty($this->telesaleCheck)){
            if($this->telesaleCheck == Telesale::BQV_TELESALE){
                $aTelesale = $mAppCache->getListdataUserByRole(ROLE_TELESALE);
                $sParamsIn = array_keys($aTelesale);
                $sParamsIn = implode(',',  $sParamsIn);
            }elseif($this->telesaleCheck == Telesale::BQV_PTTT_DOT){
                $sParamsIn = implode(',',  $mAppPromotion->getListUidPtttAnhDot());
            }
            $criteria->addCondition('t.first_order='.Sell::IS_FIRST_ORDER);
            if(!empty($sParamsIn)){
                $criteria->addCondition("t.sale_id IN ($sParamsIn)");
            }
        }
        
        $criteria->compare('t.customer_id',$this->customer_id);
        $criteria->compare('t.order_type',$this->order_type);
        $criteria->compare('t.type_customer',$this->type_customer);
        $criteria->compare('t.sale_id',$this->sale_id);
        $criteria->compare('t.agent_id',$this->agent_id);
        $criteria->compare('t.employee_maintain_id',$this->employee_maintain_id);
        if(!empty($this->uid_login)){
            $criteria->addCondition("t.uid_login={$this->uid_login} OR t.call_center_id={$this->uid_login}");
        }
        $criteria->compare('t.created_date_only',$this->created_date_only);
        $criteria->compare('t.type_customer',$this->type_customer);
        if(!empty($this->order_type_status)){
            $criteria->addCondition('t.order_type_status > 0'); // nhưng sell kiểm tra thì audit > 0
        }
        $date_from = $date_to = '';
        if(!empty($this->date_from)){
            $date_from = MyFormat::dateDmyToYmdForAllIndexSearch($this->date_from);
//            $criteria->addCondition("t.created_date_only>='$date_from'");
            DateHelper::searchGreater($date_from, 'created_date_only_bigint', $criteria);
        }
        if(!empty($this->date_to)){
            $date_to = MyFormat::dateDmyToYmdForAllIndexSearch($this->date_to);
//            $criteria->addCondition("t.created_date_only<='$date_to'");
            DateHelper::searchSmaller($date_to, 'created_date_only_bigint', $criteria);
        }
        if($this->amount_bu_vo == 1){
            $criteria->addCondition('t.amount_bu_vo > 0');
        }
        if($this->is_timer == Forecast::TYPE_TIMER){
            $criteria->addCondition('t.is_timer = ' . $this->is_timer);
        }else{
//            $criteria->addCondition('t.is_timer = 0');// Now1918 tab default get all Order
        }

        $aWith = [];
        if(!empty($this->searchGas)){
            $criteria->compare('rDetail.materials_id', $this->searchGas);
            $aWith[] = 'rDetail';
        }
        if(count($aWith)){
            $criteria->with     = $aWith;
            $criteria->together = true;
        }

        $this->handleMoreSearch($criteria, $cRole, $cUid);
        $this->handleSearchBQV($criteria, $cRole, $cUid);
        $this->handleSearchPromotion($criteria);
        $this->handleSearchTarget($criteria);

        $sort = new CSort();

        $sort->attributes = array(
            'code_no'=>'code_no',
            'agent_id'=>'agent_id',
            'created_date'=>'created_date',
            'agent_id'=>'agent_id',
            'created_date_only'=>'created_date_only',
            'order_type'=>'order_type',
            'status'=>'status',
        );    
        $sort->defaultOrder = 't.id desc';

        if($this->is_timer == Forecast::TYPE_TIMER){
            $sort->defaultOrder = 't.status ASC, t.delivery_timer ASC';
        }
        $_SESSION['data-excel'] = new CActiveDataProvider($this, array(
            'pagination'=>false,
            'criteria'=>$criteria,
//            'sort' => $sort,
        ));

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
            'sort' => $sort,
            'pagination'=>array(
                'pageSize'=> $this->pageSize,
            ),
        ));
    }
    
    /**
     * @Author: DungNT Mar 03, 2017
     * @Todo: tach ham xu ly them cac dk search
     */
    public function handleMoreSearch(&$criteria, $cRole, $cUid) {
        $cExt = CacheSession::getCookie(CacheSession::CURRENT_USER_EXT);
        $mTransactionHistory = new TransactionHistory();
        $aExt = $mTransactionHistory->getExtHgdOnline();
        $mTelesale = new Telesale();
        
        if(is_array($this->province_id) && count($this->province_id)){
            $sParamsIn = implode(',',  $this->province_id);
            $criteria->addCondition("t.province_id IN ($sParamsIn)");
        }
        $aRoleSale = [ROLE_EMPLOYEE_MARKET_DEVELOPMENT, ROLE_EMPLOYEE_MAINTAIN];
        
        if(in_array($cRole, $aRoleSale)){
            $criteria->addCondition('t.sale_id=' . $cUid);
        }
        if($cRole==ROLE_SUB_USER_AGENT){
            $criteria->addCondition('t.agent_id=' . MyFormat::getAgentId());
        }elseif($cRole==ROLE_CALL_CENTER && !in_array($cUid, GasConst::getArrUidUpdateAll())){
//            $criteria->addCondition('t.uid_login=' . $cUid);
        }else{
            if(!empty($this->agent_id)){
                $criteria->addCondition('t.agent_id=' . $this->agent_id);
            }
            GasAgentCustomer::addInConditionAgent($criteria, 't.agent_id');
        }

        if(!empty($this->pttt_code)){
            $criteria->addCondition("t.pttt_code<>''");
        }
        if($this->call_id == 'yes'){
            $criteria->addCondition("t.call_id>0");
        }elseif($this->call_id == 'no'){
            $criteria->addCondition('t.call_id=0');
        }
        if(isset($_GET['sCustomer'])){// Jan2518 for report admin/gasAppOrder/reportGas24h
            $criteria->addCondition('t.customer_id IN ('.$_GET['sCustomer'].') AND t.status='.Sell::STATUS_PAID . ' AND t.source='.Sell::SOURCE_APP);
        }
        
        if($mTelesale->showSetSaleId() || in_array($cUid, $this->getTelesaleViewAll())){// open all
        }elseif($cRole == ROLE_TELESALE && !empty($cExt) && array_key_exists($cExt, $aExt)){// Open khung giờ cao điểm từ 10h đến 12h hằng ngày
            // no limit
        }elseif($cRole == ROLE_TELESALE){
            $sourceApp = Sell::SOURCE_APP; $today = date('Y-m-d');
            $criteria->addCondition('(t.sale_id=' . $cUid . " OR t.uid_login={$cUid} OR  t.source = $sourceApp AND t.created_date_only='$today')");
        }
    }
    
    /** @Author: DungNT Now 17, 2018
     *  @Todo: xử lý search by promotion code 
     * @param: $this->promotion_id is string code like: 24h001, 8686...
     **/
    public function handleSearchPromotion(&$criteria) {
        if(empty($this->promotion_id)){
            return ;
        }
        $mAppPromotion = new AppPromotion();
        $mAppPromotion->code_no = $this->promotion_id;
        $mAppPromotion = $mAppPromotion->getByCodeOnly();
        if(empty($mAppPromotion)){
            return ;
        }
        $aPromotionId = [];
        if(array_key_exists($mAppPromotion->owner_id, $mAppPromotion->getArrayPartnerFixCode())){
            $aSubOwner = $mAppPromotion->getByOwnerIdRoot($mAppPromotion->owner_id);
            $aPromotionId = CHtml::listData($aSubOwner, 'id', 'id');
        }else{
            $aPromotionId[$mAppPromotion->id] = $mAppPromotion->id;
        }
        if(count($aPromotionId) < 1){
            return ;
        }
        $sParamsIn = implode(',', $aPromotionId);
        $criteria->addCondition("t.promotion_id IN ($sParamsIn)");
    }

    public function getTelesaleViewAll() {
        return [
            GasConst::UID_THUY_NTT, // Nguyễn Thị Thanh Thủy - Binh Dinh
            GasConst::UID_QUYEN_PTL, // Nguyễn Thị Thanh Thủy - Binh Dinh
        ];
    }
    
    /** @Author: DungNT Apr 16, 2018
     *  @Todo: search bqv PTTT
     **/
    public function handleSearchBQV(&$criteria, $cRole, $cUid) {
        if(empty($this->ptttUid)){
            return ;
        }
//        !important Apt1618 => Chay qua lau $criteria->addCondition("t.id IN ( SELECT SubQ.sell_id FROM gas_code as SubQ WHERE SubQ.user_id=$this->ptttUid AND SubQ.sell_id > 0)");
        $criteriaSub = new CDbCriteria();
        $criteriaSub->addCondition('t.user_id=' . $this->ptttUid . ' AND t.sell_id > 0' );
        $models = SpjCode::model()->findAll($criteriaSub);
        if(count($models)){
            $aId = CHtml::listData($models, 'sell_id', 'sell_id');
            $sParamsIn = implode(',',  $aId);
            $criteria->addCondition("t.id IN ($sParamsIn)");
        }
        
    }
    
    public function trimData() {
        $this->type_amount      = MyFormat::removeComma($this->type_amount);
        $this->amount_discount  = MyFormat::removeComma($this->amount_discount);
        $this->promotion_amount = MyFormat::removeComma($this->promotion_amount);
        $this->note             = trim($this->note);
        $this->agent_id         = $this->agent_id > 0 ? $this->agent_id : '';
    }

    protected function beforeValidate() {
        $this->trimData();
        $mCustomer = $this->rCustomer;
        if($mCustomer){
            $this->type_customer    = $mCustomer->is_maintain;
            $this->sale_id          = $mCustomer->sale_id;
//            if(in_array($this->scenario, array('WindowCreate', 'WindowUpdate'))) {
            if(in_array($this->scenario, array('WindowCreate'))) {// co the khong lay TH update
                // vì address có thể lấy bên TransactionHistory nữa, nên không phải lúc nào cũng lấy từ model Customer
                $this->address          = $mCustomer->address;
            }
        }
        $mEmployeeMaintain = $this->rEmployeeMaintain;
        if($mEmployeeMaintain){
            $this->role_id_employee = $mEmployeeMaintain->role_id;
        }
        if($this->isNewRecord && empty($this->created_date_only)){
            $this->created_date_only = date('Y-m-d');
        }
        return parent::beforeValidate();
    }
    
    /** @Author: DungNT May 11, 2018
     *  @Todo: format datetime save db
     **/
    public function formatDbDatetime() {
        if(strpos($this->delivery_timer, '/')){
            $this->delivery_timer = MyFormat::datetimeToDbDatetime($this->delivery_timer);
        }
    }
    public function formatDataBeforeUpdate() {
        $this->delivery_timer    = MyFormat::datetimeDbDatetimeUser($this->delivery_timer);
        $this->getDiscountType();
        $this->aDetail = $this->rDetail;
        $this->getPhoneUpdate();
        $this->created_date_only = MyFormat::dateConverYmdToDmy($this->created_date_only);
        $this->scenario = 'WebUpdate';
    }
    
    public function buildCodeNo() {
        $prefix_code = 'S'.date('y');
//        $this->code_no = MyFunctionCustom::getNextId('Sell', 'S'.date('y'), 10, 'code_no');
        $this->code_no = $prefix_code.ActiveRecord::randString(6, GasConst::CODE_CHAR);
    }
    
    protected function beforeSave() {
        $cRole = MyFormat::getCurrentRoleId();
        if($this->isNewRecord){
            $this->buildCodeNo();
        }else{// Mar 15, 2017 NV CallCenter hoặc giám sát CallCenter có thể update row này, nên Sẽ phải lưu lại. old=>Aug 21, 2016 Không cho user bán hàn update quá 2 ngày cho nên sẽ không cần phải lưu cái này nữa
            if(!empty($cRole) && $cRole != ROLE_EMPLOYEE_MAINTAIN){
                $this->last_update_by   = MyFormat::getCurrentUid();
                $this->last_update_time = date('Y-m-d H:i:s');
            }
        }
        if(strpos($this->created_date_only, '/')){
            $this->created_date_only = MyFormat::dateConverDmyToYmd($this->created_date_only);
            MyFormat::isValidDate($this->created_date_only);
        }
        $this->created_date_only_bigint = strtotime($this->created_date_only); 
        if(!empty($this->complete_time)){
            $this->complete_time_bigint     = strtotime($this->complete_time);
        }
        return parent::beforeSave();
    }
    
    protected function afterSave() {
        if($this->isNewRecord){
            $this->makeSocketNotify();// không thể để ở beforeSave vì chưa có ID obj
        }
        return parent::afterSave();
    }
    
    /**
     * @Author: DungNT Jun 13, 2016
     * map attribute to model form $q. get post and validate
     * @param: $result, $q, $mUser
     * @param: $model is $mUphold
     */
    public function windowGetPost($q, $mUser) {
        $this->customer_id          = $q->customer_id;
        $this->agent_id             = $q->agent_id;
//        $this->sale_id = $q->monitor_market_development_id;
        $this->employee_maintain_id = !empty($q->employee_maintain_id) ? $q->employee_maintain_id : $q->monitor_market_development_id;
        $this->uid_login            = $mUser->id;
        $this->created_date_only    = isset($q->created_date) ? $q->created_date : "";
        if( strpos($this->created_date_only, '-')){
            $this->created_date_only = MyFormat::dateDmyToYmdForAllIndexSearch($this->created_date_only);
        }
        $this->validate();
        $this->windowGetPostDetail($q);
        $this->checkDayUpdate();
    }
    
    /**
     * @Author: DungNT Feb 11, 2017
     * @Todo: set empty giao nhận với những đại lý đang chạy app, giao nhận sẽ tự nhận đơn hàng
     */
    public function windowSetEmptyEmployeeForApp() {
        if(in_array($this->agent_id, UsersExtend::getAgentRunAppGN())){
            $this->employee_maintain_id = 0;
        }
    }
    
    /**
     * @Author: DungNT Jun 13, 2016
     * map attribute to model detail form $q. get post and validate
     */
    public function windowGetPostDetail($q) {
        $this->note     = trim($q->note);// Jun 30, 2016 xử lý get biến note ở đây cho cả create + update
        $this->status   = $q->status;// Jun 30, 2016 xử lý get biến note ở đây cho cả create + update
        // Aug 26, 2016 xử lý bắt thêm biến order_type, type_amount ở đây chung cho create + update
        $this->order_type           = isset($q->order_type) ? $q->order_type : Sell::ORDER_TYPE_NORMAL;
        $this->type_amount          = isset($q->type_amount) ? $q->type_amount : 0;
        $this->amount_discount      = isset($q->amount_discount) ? $q->amount_discount : 0; // Sep 04, 2016
        $this->employee_maintain_id = !empty($q->employee_maintain_id) ? $q->employee_maintain_id : $q->monitor_market_development_id;
        $this->windowSetEmptyEmployeeForApp();
        
        $hasError       = true;
        $this->aDetail  = [];
        foreach($q->order_detail as $objDetail){
            $mDetail = new SellDetail('WindowCreate');
            $mDetail->customer_id       = $this->customer_id;
            $mDetail->type_customer     = $this->type_customer;
            $mDetail->sale_id           = $this->sale_id;
            $mDetail->agent_id          = $this->agent_id;
            $mDetail->uid_login         = $this->uid_login;
            $mDetail->employee_maintain_id = $this->employee_maintain_id;
            $mDetail->created_date_only = $this->created_date_only;
            $mDetail->order_type        = $this->order_type;
            $mDetail->order_type_status = $this->order_type_status;
//            $mDetail->type_amount       = $this->type_amount;//sẽ put ở putDiscountToDetail
            
            $mDetail->materials_type_id = $objDetail->materials_type_id;
            $mDetail->materials_id      = $objDetail->materials_id;
            $mDetail->qty               = $objDetail->qty;
            $mDetail->price             = $objDetail->price;
            $mDetail->amount            = $objDetail->qty*$objDetail->price;
            $mDetail->seri              = trim($objDetail->seri);
            $mDetail->materials_parent_id = isset($objDetail->materials_parent_id) ? $objDetail->materials_parent_id : 0;// Now 20, 2016 for fix setup bù vỏ
            
            $mDetail->validate();
            if(!$mDetail->hasErrors()){
                $this->aDetail[] = $mDetail;
                $hasError = false;
            }
        }
        $this->checkDiscount();
        $this->putDiscountToDetail();
        if($hasError){
            $this->addError('customer_id',"Chi tiết đơn hàng không hợp lệ");
        }
    }
    
    public function setProvinceId() {
        $mAppCache = new AppCache();
        $aAgent = $mAppCache->getAgent();
        $this->province_id = isset($aAgent[$this->agent_id]) ? $aAgent[$this->agent_id]['province_id'] : 0;
    }
    /** @Author: DungNT Jan 07, 2019
     *  @Todo: set new address of customer
     **/
    public function setAddress() {
        if($this->rCustomer){
            $this->address = $this->rCustomer->address;
        }
    }
    
    /**
     * @Author: DungNT Jul 18, 2016
     * @Todo: Xác định trong 1 order HGD có lấy KM hay không, nếu Không chọn KM dánh dấu cờ qty_discount > 0 có chiết khấu
     */
    public function checkDiscount() {// Kiểm tra có chiết khấu hay không
        $qtyPromotion = 0; $qtyGas = 0; $promotionVanDay = false;
        $this->total = 0; $this->grand_total = 0;
//        $mAppCache = new AppCache();
//        $listdataMaterial = $mAppCache->getListdata('GasMaterials', AppCache::LISTDATA_MATERIAL);// close by FixFeb2118

        // Oct 23, 20216 xử lý lấy tỉnh của đại lý
        $this->setProvinceId();
        // Now 22, 2016 xử lý bù vỏ
        $materials_parent_id_gas    = '';
        $materials_id_vo            = '';
        
        foreach($this->aDetail as $mDetail){
            if(in_array($mDetail->materials_type_id, GasMaterialsType::$ARR_WINDOW_PROMOTION)){
                $qtyPromotion           += $mDetail->qty;
            }elseif(in_array($mDetail->materials_type_id, GasMaterialsType::$HGD_SELL_GAS)){
                $qtyGas                 += $mDetail->qty;
                $materials_parent_id_gas = $mDetail->materials_parent_id;
                $this->materials_id_gas  = $mDetail->materials_id;
                $this->totalGasAmount   += $mDetail->amount;// Oct3018 fix amount gasremain: S18PTQ6WJ - Đơn hàng App của thủ dầu một 10kg gas dư tính ra hơn 400k , a kiểm tra lại xem nha
            }elseif(in_array($mDetail->materials_type_id, GasMaterialsType::$ARR_WINDOW_VO)){
                $materials_id_vo        = $mDetail->materials_id;
            }
//            if($mDetail->price == 0 && in_array($mDetail->materials_type_id, GasMaterialsType::$ARR_WINDOW_DISCOUNT_LIKE_PROMOTION)){
//                $promotionVanDay    = true;
//            }// Jan 09, 2016 bỏ cách xác định KM theo val dây vì Kế Toán đã dc sửa CK ở dưới rồi
            $mDetail->province_id   = $this->province_id;
            $this->total            += $mDetail->amount;// Dec 10, 2016
//            $this->transactionBuildDetail($mDetail, $listdataMaterial);// FixFeb2118 move hàm này xuống putDiscountToDetail() để get đủ biến hơn vào detail cụ thể là biến gas_remain_amount
        }
        $qty_discount = $qtyGas - $qtyPromotion;
        
//        if($this->order_type == Sell::ORDER_TYPE_BO_BINH && $qty_discount > 0){// Sep 04, 2016 nếu bán bộ bình không có quà KM tức là đã KM ở trên rồi, nên ck = 0
//        if(in_array($this->order_type, Sell::$ARR_TYPE_BAN_VO) && $qty_discount > 0){// Sep 04, 2016 nếu bán bộ bình không có quà KM tức là đã KM ở trên rồi, nên ck = 0
        if($promotionVanDay){// Now 25, 2016 nếu tặng val hoặc dây rồi thì nên ck = 0
            $qty_discount = 0;
        }// Sep 26, 2016 vì đã cho sửa ck ở dưới nên cách tính ck giống đơn hàng BT chứ ko tính như trên nữa
        // note Oct 25, 2016 tình huống cố ý sửa CK = 0 thì amount_discount = 1
        if($qty_discount > 0){
            $this->qty_discount = $qty_discount;
        }elseif($qty_discount < 1 && $qtyGas > 0){// May1419 fix Order chỉ có bếp ko nhập đc chiết khấu: add && $qtyGas > 0
            $this->qty_discount     = 0;
            $this->amount_discount  = 0;// Apr2419 fix bug vừa nhập quà KM + tiền ck 50k 
        }
        if($this->amount_discount > 0){
            $this->qty_discount = 0;// nếu có sử Ck thì lấy bên sửa
        }
        $this->setBuVo($materials_parent_id_gas, $materials_id_vo);// will open when live function
        if($this->is_bu_vo_zero){// Mar 08, 2017 xử lý set bù vỏ = 0 với 1 số trường hợp đặc biệt, ko theo rule setup
            $this->amount_bu_vo = 0;
        }
        if($this->source == Sell::SOURCE_APP){
            $this->qty_discount = 0;// Now1817 xử lý không có CK của đơn hàng đặt APP gas24h
        }
        if($this->promotion_type == AppPromotion::TYPE_FREE_ONE_GAS){
            $this->promotion_amount = $this->total;
        }

        // Dec 10, 2016 tính tổng tiền đơn hàng để đồng bộ với model TransactionHistory
        $amount_discount    = $this->amount_discount > 10 ? $this->amount_discount : 0;// vì ở dưới có thể put lên $amount_discount là 1 hoặc 2 hoặc 50000
        $typeAmount         = $this->type_amount;
        if($this->order_type == Sell::ORDER_TYPE_THU_VO){
            $typeAmount      *= -1;// Dec3017 xử lý số tiền hiển thị trên app đúng với số tiền thực khi thu vỏ thì trừ đi 300k
            if($qtyGas){
                throw new Exception('Đơn hàng thu vỏ không thể nhập gas, kiểm tra lại đơn hàng');
            }
        }
        $this->grand_total  = $this->total + $typeAmount + $this->amount_bu_vo - ($this->qty_discount * Sell::PROMOTION_DISCOUNT) - $amount_discount - $this->promotion_amount;
        $this->calcGasRemainAmount();
    }
    
    /** @Author: DungNT Feb 21, 2018
     *  @Todo: bổ sung tính toán tiền gas dư HGĐ, tỉnh tổng đơn hàng chia 12 bao gồm cả giảm giá
     **/
    public function calcGasRemainAmount() {
        $totalGasAmount             = $this->totalGasAmount - $this->promotion_amount;
        $this->gas_remain_amount    = round(($totalGasAmount/12) * $this->gas_remain);
        $this->grand_total          -= $this->gas_remain_amount;// trừ tiếp tiền gas dư
    }
    
    /** @Author: DungNT Sep 29, 2019
     *  @Todo: check order has Gas or not
     *  @reutn: true if have Gas, false if not
     **/
    public function hasGas() {
        $res = false;
        foreach($this->aDetail as $key => $mDetail):
            if(in_array($mDetail->materials_type_id, GasMaterialsType::$SETUP_PRICE_GAS_12)){
                $res = true;
            }
        endforeach;
        return $res;
    }
    
    /**
     * @Author: DungNT Aug 23, 2016
     * @Todo: set qty discount luôn vào dòng Gas 12 phục vụ báo cáo, những dòng khác như KM, val, dây sẽ = 0
     */
    public function putDiscountToDetail() {
        $put                    = true;
        $put_type_amount        = true;
        $put_amount_thu_vo      = true;
        $put_amount_discount    = true;// Sep 04, 2016 xử lý put chiết khẩu sửa vào detail
        $put_bu_vo              = true;// Now 22, 2016 xử lý put bù vỏ vào detail
        $put_app_promotion      = true;// Now 26, 2016 xử lý put tiền giảm giá từ App vào detail
        $put_v1_discount        = true;// Now0118 xử lý put tiền giảm giá giờ vàng, thanh toán thẻ vào detail
        $mAppCache              = new AppCache();
        $listdataMaterial       = $mAppCache->getListdata('GasMaterials', AppCache::LISTDATA_MATERIAL);
        
        foreach($this->aDetail as $key => $mDetail){
//            $mDetail->type_amount = 0;// không cần thiết
            if(in_array($mDetail->materials_type_id, GasMaterialsType::$HGD_SELL_GAS)){
                if($this->qty_discount > 0 && $put){
                    $mDetail->qty_discount = $this->qty_discount;
                    $put = false;
                }else{
                    $mDetail->qty_discount = 0;
                }
                
                // Aug 26, 2016 xử lý put tiền bán bộ bình hoặc thế chân vào detail, chỉ put vào dòng đầu tiên có gas, phục vụ sum báo cáo
                if($put_type_amount && in_array($this->order_type, Sell::$ARR_TYPE_BAN_VO)){
                    $mDetail->type_amount   = $this->type_amount;
                    $put_type_amount        = false;
                }
                
                // Now 26, 2016 xử lý put tiền giảm giá từ App Promotion
                if($put_app_promotion){
                    $mDetail->promotion_amount      = $this->promotion_amount;
                    $mDetail->promotion_id          = $this->promotion_id;
                    $put_app_promotion              = false;
                }
                if($put_v1_discount){
                    $mDetail->v1_discount_amount    = $this->v1_discount_amount;
                    $mDetail->v1_discount_id        = $this->v1_discount_id;
                    $put_v1_discount                = false;
                }
            }
            // Sep 04, 2016 xử lý put chiết khẩu sửa vào detail, Oct 23, 2016 bán bếp hay gas, val... đều phải put amount_discount
            if($this->amount_discount > 0 && $put_amount_discount){
                $mDetail->amount_discount = $this->amount_discount;
                $put_amount_discount = false;
            }else{
                $mDetail->amount_discount = 0;
            }
            
            // Aug 29, 2016 xử lý put loại và tiền thu vỏ vào row detail của Vỏ
            if($put_amount_thu_vo && in_array($mDetail->materials_type_id, GasMaterialsType::$ARR_WINDOW_VO) && $this->order_type == Sell::ORDER_TYPE_THU_VO){
                $mDetail->type_amount   = $this->type_amount;
                $put_amount_thu_vo      = false;
            }
            // Now 22, 2016 xử lý put bù vỏ vào detail vào row detail của Vỏ (có thể là gas)
            if($put_bu_vo && in_array($mDetail->materials_type_id, GasMaterialsType::$ARR_WINDOW_VO)){
                $mDetail->amount_bu_vo      = $this->amount_bu_vo;
                $put_bu_vo                  = false;
                $mDetail->gas_remain_amount = $this->gas_remain_amount;// Feb2118 put thành tiền gas dư vào detail
            }
            $this->transactionBuildDetail($mDetail, $listdataMaterial);// Add FixFeb2118
        }// end foreach($this->aDetail

        if($put_app_promotion){// nếu chưa put giảm giá đc vào Gas thì put vào vật tư đầu tiên trong  đơn
            foreach($this->aDetail as $key => $mDetail){
                $mDetail->promotion_amount  = $this->promotion_amount;
                $mDetail->promotion_id      = $this->promotion_id;
                break;
            }
        }
    }
    
    /** @Author: DungNT May 09, 2018
     *  @Todo: get list agent có đánh PTTT trong khoảng thời gian
     **/
    public function getListAgentRunPttt() {
//        return [];// Apr0419 DungNT close - cho phép nhập code bình thường, sẽ giới hạn tính 90 ngày ở tính thưởng
        return [
            2076814         => '2019-10-09', // ĐLLK Gạo An Giang - Gas 24h Tân Uyên
            1120439         => '2019-08-15', // Đại Lý Sóc Trăng
            2068340         => '2019-08-15', // Đại Lý Lê Chánh
            110             => '2019-08-21', // Đại lý Thủ Dầu Một
            863146          => '2019-08-15', // Cửa Hàng Chợ Mới 2
            1161320          => '2019-08-15', // Đại Lý Nguyễn Trãi - Phường 9 Cà Mau
            1966690          => '2019-08-15', // Đại Lý Mỹ Tho 1 
            2039377          => '2019-08-15', // Đại Lý Mỹ Tho 2    
            596026          => '2019-08-15', // Cửa Hàng Long Xuyên 2
            138544          => '2019-08-15', // Cửa hàng Cần Thơ 2
            1861687         => '2019-08-15', // Đại Lý Quảng Phú - TP Quảng Ngãi
            119             => '2019-09-29', //  Đại lý Tân Định
            357031          => '2019-09-29', // Đại lý Tân Phước Khánh 
            109             => '2019-09-29', // ĐL Lái Thiêu
            1210             => '2019-10-15', // Đại Lý Thuận Giao
            115             => '2019-10-30', // Đại lý Dĩ An
            
//            103         => '2018-06-13', // Đại lý Quận 8.1
//            104         => '2019-03-03', // Đại lý Quận 8.2
//            945418      => '2017-11-28', // Đại Lý Quận 4.2
//            106         => '2018-05-20', // Đại lý Bình Thạnh 1
//            658920      => '2018-05-20', // Đại lý Bình Thạnh 2
//            768408      => '2019-01-31', // Đại lý Bình Thạnh 3
//            100         => '2018-02-27', // Đại lý Quận 2
//            768409      => '2018-01-21', // Đại Lý Thủ Đức 2
//            113         => '2018-01-13', // Đại Lý Quan 3
//            1058061     => '2018-07-05', // Đại Lý Thủ Đức 3
//            118         => '2019-03-17', // Đại lý An Thạnh
            102         => '2019-06-30', // Đại lý Quận 7
//            1317820     => '2019-12-31', // Đại Lý Trà Nóc - Cần Thơ
//            1161356     => '2018-06-17', // Đại Lý An Hiệp - Đồng Tháp
//            107         => '2019-04-30', // Đại lý Gò Vấp 1
//            1372582     => '2018-06-06', // Đại Lý Quận 2.2
//            27740       => '2018-06-14', // Đại lý Tân Bình 2
//            108         => '2019-03-02', // Đại lý Hóc Môn 
//            1284630     => '2018-12-13', // Đại Lý Bửu Long
//            1486944     => '2018-12-20', // ĐLLK - Đại Lý Thanh Minh - Long An
//            1364591     => '2019-02-06', // Đại Lý Đồng Xoài - Bình Phước
//            952187      => '2018-08-14', // VT01 - Đại Lý Bến Nôm -
//            116         => '2018-08-13', // Đại lý Bình Đa
//            1127081         => '2019-12-30', // Đại Lý Dương Quảng Hàm - Nha Trang
//            1205656         => '2019-12-28', // Đại Lý Cam Ranh
//            1179069         => '2019-12-28', // Đại Lý Diên Khánh - Khánh Hòa 
//            1183888         => '2019-12-28', // Đại Lý Sông Cầu - Phú Yên
//            1355765         => '2019-12-28', // Đại Lý Tuy Hòa - Phú Yên 
//            
//            123             => '2019-04-15', // Đại lý Trảng Dài
//            572832          => '2018-10-01', // Đại lý Bình Đường
//            375037          => '2019-04-12', // Đại lý Thái Hòa - start 2019-01-12 - end sau 4 thang
//            1294352         => '2018-09-01', // Đại Lý Gas Minh Hoàng
//            916044          => '2018-10-04', // Đại Lý Vĩnh Cửu 
//            117             => '2018-10-23', // Đại lý Thống Nhất
//            1214132         => '2018-10-06', // Đại Lý Tây Ninh
//            758528          => '2019-02-16', //  Đại Lý Uyên Hưng - Oct1718
//            971240          => '2018-12-25', // Đại Lý Phú Mỹ
//            268833          => '2018-12-25', // Đại Lý Phú Hòa
////            262526          => '2019-03-02', //  Cửa Hàng Ô Môn
////            30753           => '2019-03-02', // Cửa hàng Cần Thơ 1 
//            1750715         => '2019-01-10', //  Đại Lý Đồng Xoài 2 - P.Tân Phú
//            1755139          => '2019-02-28', //  ĐLLK Quang Đại - Cẩm Mỹ - Đồng Nai 
//            1763817          => '2019-02-28', // ĐLLK Đại Lý Hoàng Gia 3
//            1773595          => '2019-01-31', // ĐLLK Thanh Sơn 1 - Đồng Nai 
//            1210050          => '2019-04-30', // Đại Lý Tp Bến Tre
//            939720          => '2019-03-27', // Đại Lý Bình Chuẩn
//            114             => '2019-01-12', // Đại lý Long Bình Tân
//            1079730         => '2019-01-01', // Đại Lý Bạc Liêu
//            105             => '2019-01-18', // Đại Lý Quan 9
//            235             => '2019-03-02', // Đại lý Tân Bình 1
//            1584211         => '2019-04-20', // ĐLLK Đại Lý Phú Xuân
//            
//            1599181         => '2019-05-04', // Đại Lý Bình Tân 2
//            1599181         => '2019-05-04', // Đại Lý Bình Tân 2
            
        ];
    }
    
    /** @Author: DungNT Oct 08, 2017 
     * @Todo: Đại lý đánh chương trình 2 tháng xin không bù vỏ
     */
    public function isAgentBuVoZero() {
        return false; // Sep0319 DungNT dừng hỗ trợ bù vỏ = 0
        if($this->isExcelExport){// Jul2119
            return false;
        }
        $aAgentCheck = $this->getListAgentRunPttt();
        $date_expiry = '2018-03-01';// cho phép đơn hàng app không bù vỏ trong 3 tháng
        if($this->source == Sell::SOURCE_APP){
            return true;// Mar0718 100% đơn app không bù vỏ
//            return MyFormat::compareTwoDate($date_expiry, date('Y-m-d'));
        }
//        $aAgentCheck = [];// Jun2619 set lại bù vỏ. Mar2219 không tính tự động bù vỏ nữa, cuối tháng làm quyết toán trả lại tiền 
        
        /* Apr0419 DungNT close - cho phép nhập code bình thường, sẽ giới hạn tính 90 ngày ở tính thưởng*/
        if(array_key_exists($this->agent_id, $aAgentCheck)){
            $date_expiry = $aAgentCheck[$this->agent_id];
            return MyFormat::compareTwoDate($date_expiry, date('Y-m-d'));
        }
        return false;
    }
    
    /** @Author: DungNT Nov 22, 2016
     *  @Todo: put bù vỏ nếu record có đủ cả gas + vỏ
     */
    public function setBuVo($materials_parent_id_gas, $materials_id_vo) {
        $this->amount_bu_vo = 0;
        $mAppCache = new AppCache();
        if($this->isAgentBuVoZero()){
            return ;
        }
        if(empty($materials_parent_id_gas) || empty($materials_id_vo)){
            return ;
        }
        $key_compare = $materials_parent_id_gas.'-'.$materials_id_vo;
        $zone_id = MyFunctionCustom::getZoneId(1, array('province_id'=>$this->province_id));
        if($zone_id == GasOrders::ZONE_TARGET_MIENDONG){// liên quan đến hàm BuVO->getListdataZone() vì trên giao diện set chung cho TPHCM và Miền đông
            $zone_id = GasOrders::ZONE_TARGET_HCM;
        }
        if($this->isExcelExport){// Jul2719 DungNT fix  export excel not call multi AppCache::getBuVo(); -> slow in foreach
            $dataBuvo = isset($_POST['CronExcelDataBuvo']) ? $_POST['CronExcelDataBuvo'] : $mAppCache->getBuVo();
        }else{
            $dataBuvo = $mAppCache->getBuVo();
        }
        $aZoneSame = array(GasOrders::ZONE_TARGET_HCM, GasOrders::ZONE_TARGET_MIENTAY);
//        if(in_array($zone_id, $aZoneSame)){
        if(1){// Jan 10, 2017 không set riêng Tây Nguyên nữa, tất cả chạy chung 1 kiểu
            $this->calcBuvoZoneSame($zone_id, $dataBuvo, $key_compare, $materials_parent_id_gas, $materials_id_vo);
        }elseif($zone_id == GasOrders::ZONE_TARGET_TAY_NGUYEN){
            $this->calcBuvoZoneTayNguyen($zone_id, $dataBuvo, $key_compare, $materials_parent_id_gas, $materials_id_vo);
        }
    }
    
    /**
     * @Author: DungNT Nov 22, 2016
     * @Todo: calc bu vo cho những zone same
     */
    public function calcBuvoZoneSame($zone_id, $dataBuvo, $key_compare, $materials_parent_id_gas, $materials_id_vo) {
        $key_compare_2 = $this->materials_id_gas.'-'.$materials_id_vo;
        if(isset($dataBuvo['key_compare'][$zone_id][$key_compare_2])){
            $buvoPrimaryKey         = $dataBuvo['key_compare'][$zone_id][$key_compare_2];
            $this->amount_bu_vo     = $dataBuvo['key_price'][$buvoPrimaryKey];
            return ;
        }// Jan 17, 2017 xử lý set thương hiệu gas trực tiếp cho bù vỏ, sử dụng cùng với set parent của hướng xử lý cũ
        
        if(!isset($dataBuvo['key_compare'][$zone_id][$key_compare])){
            return ;
        }
        $buvoPrimaryKey         = $dataBuvo['key_compare'][$zone_id][$key_compare];
        $this->amount_bu_vo     = $dataBuvo['key_price'][$buvoPrimaryKey];
    }
    
    /**
     * @Author: DungNT Nov 22, 2016
     * @Todo: calc bu vo cho zone Tay Nguyen
     * @note: chú ý hàm: BuVo=>buildJsonPair() khi thay dổi công thức tính bù vỏ của Tây Nguyên
     */
    public function calcBuvoZoneTayNguyen($zone_id, $dataBuvo, $key_compare, $materials_parent_id_gas, $materials_id_vo) {
//        if(!isset($dataBuvo['key_compare'][$zone_id][$materials_id_vo])){
//            return ;
//        }
        if($materials_id_vo == GasMaterials::ID_VO_TAP_12){
            $buvoPrimaryKey         = $dataBuvo['key_compare'][$zone_id][$materials_id_vo];
            $this->amount_bu_vo     = $dataBuvo['key_price'][$buvoPrimaryKey];
        }elseif(!isset($dataBuvo['key_compare'][$zone_id][$materials_id_vo])){
            $i = 1;
            foreach($dataBuvo['key_compare'][$zone_id] as $id_vo => $buvoPrimaryKey){
                if($i++ == 5){
                    $this->amount_bu_vo     = $dataBuvo['key_price'][$buvoPrimaryKey];
                    break;
                }
            }
        }
    }
    
    /**
     * @Author: DungNT Dec 10, 2016
     * @Todo: build array detail for TransactionHistory, sau đó encode_json lại
     */
    public function transactionBuildDetail($mDetailSell, $listdataMaterial) {
        //$aDetailTransaction
        $mDetailTransaction                    = new TransactionDetail();
        $mDetailTransaction->transaction_id    = 0;
        $mDetailTransaction->status            = 0;
        $mDetailTransaction->materials_type_id = $mDetailSell->materials_type_id;
        $mDetailTransaction->materials_id      = $mDetailSell->materials_id;
        $mDetailTransaction->qty               = $mDetailSell->qty;
        $mDetailTransaction->price             = $mDetailSell->price;
        $mDetailTransaction->price_root        = $mDetailSell->price_root;// Add May2818
        $mDetailTransaction->amount            = $mDetailTransaction->qty*$mDetailTransaction->price;
        $mDetailTransaction->created_date      = '';
        $mDetailTransaction->name              = isset($listdataMaterial[$mDetailTransaction->materials_id]) ? $listdataMaterial[$mDetailTransaction->materials_id] : '';
        $mDetailTransaction->seri              = $mDetailSell->seri;
        
        $mDetailTransaction->gas_remain         = $mDetailSell->gas_remain;
        $mDetailTransaction->gas_remain_amount  = $mDetailSell->gas_remain_amount;
        $mDetailTransaction->kg_empty           = $mDetailSell->kg_empty;
        $mDetailTransaction->kg_has_gas         = $mDetailSell->kg_has_gas;
        $this->aDetailTransaction[] = $mDetailTransaction;
    }
    
    /**
     * @Author: DungNT Dec 10, 2016
     */
    public function transactionEncodeJsonDetail() {
        if(!is_array($this->aDetailTransaction)){
            return '';
        }
        $aJson      = [];
        foreach($this->aDetailTransaction as $mDetailTransaction){
            $aJson[] = $mDetailTransaction->getAttributes();
        }
        return json_encode($aJson, JSON_UNESCAPED_UNICODE);
    }
    
    
    /**
     * @Author: DungNT Jun 13, 2016
     * save record from window
     */
    public function windowSave() {
        $this->save();
        $aRowInsert=[];
        foreach($this->aDetail as $mDetail){
            $mDetail->sell_id = $this->id;
            $mDetail->status  = Sell::STATUS_NEW;
            $this->windowSaveDetailBuildSql($aRowInsert, $mDetail);
        }
        $this->windowSaveDetailRunSql($aRowInsert);
    }
    
    /**
     * @Author: DungNT Aug 21, 2016
     * @Todo: one query to insert detail
     */
    public function windowSaveDetailBuildSql(&$aRowInsert, $mDetail) {
        $aRowInsert[] = "('$mDetail->status',
                '$mDetail->sell_id',
                '$mDetail->source',
                '$mDetail->order_type',
                '$mDetail->type_amount',
                '$mDetail->order_type_status',
                '$mDetail->customer_id',
                '$mDetail->type_customer',
                '$mDetail->sale_id',
                '$mDetail->agent_id',
                '$mDetail->province_id',
                '$mDetail->uid_login',
                '$mDetail->employee_maintain_id',
                '$mDetail->materials_type_id',
                '$mDetail->materials_id',
                '$mDetail->qty',
                '$mDetail->price',
                '$mDetail->price_root',
                '$mDetail->amount',
                '$mDetail->seri',
                '$mDetail->materials_parent_id',
                '$mDetail->created_date_only',
                '$mDetail->created_date_only_bigint',
                '$mDetail->qty_discount',
                '$mDetail->amount_discount',
                '$mDetail->amount_bu_vo',
                '$mDetail->promotion_id',
                '$mDetail->promotion_amount',
                '$mDetail->gas_remain',
                '$mDetail->gas_remain_amount',
                '$mDetail->kg_empty',
                '$mDetail->kg_has_gas',
                '$mDetail->v1_discount_id',
                '$mDetail->v1_discount_amount'
            )";
    }
    
    /**
     * @Author: DungNT Aug 21, 2016
     * @Todo: Save one query to insert detail
     */
    public function windowSaveDetailRunSql($aRowInsert) {
        $tableName  = SellDetail::model()->tableName();
        $sql        = "insert into $tableName (status,
                        sell_id,
                        source,
                        order_type,
                        type_amount,
                        order_type_status,
                        customer_id,
                        type_customer,
                        sale_id,
                        agent_id,
                        province_id,
                        uid_login,
                        employee_maintain_id,
                        materials_type_id,
                        materials_id,
                        qty,
                        price,
                        price_root,
                        amount,
                        seri,
                        materials_parent_id,
                        created_date_only,
                        created_date_only_bigint,
                        qty_discount,
                        amount_discount,
                        amount_bu_vo,
                        promotion_id,
                        promotion_amount,
                        gas_remain,
                        gas_remain_amount,
                        kg_empty,
                        kg_has_gas,
                        v1_discount_id,
                        v1_discount_amount
                        ) values ".implode(',', $aRowInsert);
        if(count($aRowInsert)>0){
            Yii::app()->db->createCommand($sql)->execute();
        }
    }

    /**
     * @Author: DungNT Jun 13, 2016
     * map attribute to model form $q. get post and validate update add record vo
     * @param: $result, $q, $mUser
     * @param: $model is $mUphold
     */
    public function windowUpdateDetail($q) {
//        if(isset($q->created_date) && strpos($q->created_date, '-')){
//            $this->created_date_only = MyFormat::dateDmyToYmdForAllIndexSearch($this->created_date_only);
//        }// Aug 21, 2016 không cho đại lý update ngày bán do Nguyên quy định ở C#
        // begin Jan 05, 2016 check change employee and notify to customer
        $oldEmployeeMaintain = $this->employee_maintain_id;
        $this->windowGetPostDetail($q);
        if($oldEmployeeMaintain != $this->employee_maintain_id){// nếu đã gắn giao nhận rồi thì notify cho 1 GN thôi
            $this->isChangeNewEmployee = true;
        }
        // begin Jan 05, 2016 check change employee and notify to customer
        
        $this->checkDayUpdate();
        if($this->hasErrors()){
            return ;
        }
        SellDetail::deleteByRootId($this->id);
        $aRowInsert=[];
        foreach($this->aDetail as $mDetail){
            $mDetail->sell_id   = $this->id;
            $mDetail->status    = $this->status;
            $this->windowSaveDetailBuildSql($aRowInsert, $mDetail);
        }
        $this->windowSaveDetailRunSql($aRowInsert);
//        $aUpdate = array('note', 'status', 'created_date_only');
        $aUpdate = array('note', 'status', 'qty_discount', 'order_type', 'type_amount', 'amount_discount','employee_maintain_id');
        // Dec 10, 2016 cho update hết
        $this->update();
        $this->updateStatusTransaction();
        $this->windowUpdatePrevDate();
    }
    
    /**
     * @Author: DungNT Aug 23, 2016
     * @Todo: kiểm tra xem user update ngày hôm trc thì sẽ run cập nhật auto vào  thẻ kho cho ngày hôm trc của đại lý đó
     */
    public function windowUpdatePrevDate() {
        if($this->created_date_only != date('Y-m-d')){
            /* Close on Aug 25, 2016,  không update liên tục khi user update, mà sẽ làm nút cho user bấm dưới web
            $info = "Need Check API C# update Ban Hang Agent: $this->agent_id -- id: $this->id --- Date: $this->created_date_only -- Created_date: $this->created_date";
            Logger::WriteLog($info);
            $this->autoSaveStoreCardHgd();
             */
        }
    }
    
    /**
     * @Author: DungNT Jun 13, 2016
     * @Todo: check user window can update
     */
    public function windowCanUpdate($uid_login) {
        $ok = $this->isValidDayUpdate();
//        if($this->uid_login != $uid_login){ // Aug 19, 2016 tạm đóng lại chưa cần thiết check
//            $ok = false;
//        }
        return $ok;
    }
    
    /**
     * @Author: DungNT Aug 19, 2016
     * @Todo: check day of model is valid to update or not
     */
    public function isValidDayUpdate() {
        $dayAllow = date('Y-m-d');
        $dayAllow = MyFormat::modifyDays($dayAllow, $this->getDayAllowUpdate(), '-');// cho phép update trong ngày
        return MyFormat::compareTwoDate($this->created_date_only, $dayAllow);
    }
    
    /**
     * @Author: DungNT Aug 19, 2016
     * @Todo: check day update and add error if not valid
     */
    public function checkDayUpdate() {
        $ok = $this->isValidDayUpdate();
        if(!$ok){
            $this->addError("customer_id", "Ngày bạn thao tác đã bị khóa, vui lòng liên hệ admin để xử lý");
        }
    }
    
    /**
     * @Author: DungNT Dec 14, 2016
     * @Todo: map 1 số info giống nhau vào model detail
     */
    public function mapSameInfoDetail(&$mDetail) {
        $mDetail->customer_id           = $this->customer_id;
        $mDetail->type_customer         = $this->type_customer;
        $mDetail->sale_id               = $this->sale_id;
        $mDetail->agent_id              = $this->agent_id;
        $mDetail->uid_login             = $this->uid_login;
        $mDetail->employee_maintain_id  = $this->employee_maintain_id;
        $mDetail->status                = $this->status;
        $mDetail->order_type            = $this->order_type;
        $mDetail->order_type_status     = $this->order_type_status;
//            $mDetail->type_amount       = $this->type_amount;//sẽ put ở putDiscountToDetail
//            $mDetail->created_date_only = $this->created_date_only;
    }
    
    /** @Author: DungNT Feb 04, 2018
     *  @Todo: tính toán lại tiền khuyến mãi khi đổi loại gas nếu giá bình khác loại bình KH đặt trên app
     **/
    public function webReCalcPromotion() {
        $aPromotionType = [AppPromotion::TYPE_REFERENCE_USER];// trường hợp này thì xử lý ở đâu, khi số tiền KM nhiều hơn total?
        if(!in_array($this->promotion_type, $aPromotionType)){
            return ;
        }
        $this->webReCalcPromotionTypeRefUser();
        // 2. với loại KM 1 bình, nếu đổi loại bình với giá cao hơn thì cũng phải tính lại cho đúng tiền KM là 100%
        // KM 1 Bình đã xử lý ở function checkDiscount
    }
    
    public function webReCalcPromotionTypeRefUser() {
        // 1. với loại KM tiền 20K giới thiệu, nếu số tiền còn lại lớn hơn số tiền của bình đổi thì phải tính toán lại tiền để có thể miễn phí 100 % 
        $mPromotionUser = new AppPromotionUser();
        $mPromotionUser->user_id            = $this->customer_id;
        $mPromotionUser->promotion_type     = AppPromotion::TYPE_REFERENCE_USER;
        $mPromotionUser                     = $mPromotionUser->getActivePromotion();
        if(empty($mPromotionUser)){
            return ;
        }
        
        if($mPromotionUser->promotion_amount > 0 && $this->total > $this->promotion_amount){
            /**  nếu $this->total > $this->promotion_amount thi phai tinh toan lai promotion_amount để KH sử dụng hết được tiền giới thiệu
             * Neu tien gioi thieu nhiều hoặc ít hơn 
             * @Note: Now0818 co the cho nay se khong tinh lai tien cua KM neu total > $this->promotion_amount
             * dc bao nhieu thi ap dung bay nhieu luon, vi khi xu l cong tru kieu nay dang gay ra cong sai promotion_amount cho don hang ap dung KM nay 
             */
            return ;
            $amountMiss     = $this->total - $this->promotion_amount;
            $amountAccept   = $amountMiss;
            if($amountMiss > $mPromotionUser->promotion_amount){// nếu số tiền thiếu nhiều hơn số tiền KM còn lại
                $amountAccept   = $amountMiss - $mPromotionUser->promotion_amount;
            }else{
                $amountAccept = $mPromotionUser->promotion_amount;
            }
            $mPromotionUser->promotion_amount -= $amountAccept;
            $mPromotionUser->plusTimeUseReal = false;
            $mPromotionUser->setUserApply();
            $this->promotion_amount += $amountAccept;
//            $this->promotion_amount += $this->v1_discount_amount;// Now0818 Fix cộng thêm tiền giảm giá giờ vàng -- chua ro co can cong them khong, chua test case nay 
            Logger::WriteLog("webReCalcPromotionTypeRefUser run 1 - amountAccept = $amountAccept --- mPromotionUser->promotion_amount = $mPromotionUser->promotion_amount -- this->promotion_amount = $this->promotion_amount");
        }elseif($this->total < $this->promotion_amount){
            // Jun2618 Neu promotion_amount > total thi grand_total se bị âm, do đó phải tính toán lại, set  $this->promotion_amount = $this->total
            $amountPlusBack     = $this->promotion_amount - $this->total;
            $mPromotionUser->promotion_amount += $amountPlusBack;
            $mPromotionUser->plusTimeUseReal = false;
            $mPromotionUser->setUserApply();
            $this->promotion_amount = $this->total;
            Logger::WriteLog("webReCalcPromotionTypeRefUser run 2 - SellId = $this->id -- amountPlusBack = $amountPlusBack");
        }
    }
    
    /** @Author: DungNT Mar 07, 2018
     *  @Todo: Nếu đơn hàng khác tháng hiện tại thì sẽ lấy giá ở 1 chỗ khác. dùng để save price các tháng mà audit sửa lại đơn của KH
     **/
    public function getPriceOfMonthSell() {
        $dateSell = $this->created_date_only;
        if(strpos($this->created_date_only, '/')){
            $dateSell = MyFormat::dateConverDmyToYmd($this->created_date_only);
        }
        $tempDate = explode('-', $dateSell);
        if(!isset($tempDate[1])){
            $info = $this->created_date_only. ' - Ngày không hợp lệ, vui lòng kiểm tra lại getPriceOfMonthSell. Liên hệ với anh Dũng ngay để hỗ trợ 01684 331 552';
            SendEmail::bugToDev($info);
            throw new Exception($info);
        }
        $orderMonth     = $tempDate[1]*1;
        $currentMonth   = date('m')*1; 
        $mAppCache      = new AppCache();
        
        if( $orderMonth == $currentMonth){
            $aDataCache     = $mAppCache->getPriceHgd(AppCache::PRICE_HGD);
        }else{
            $aDataCache     = UsersPriceHgd::getPriceHgd($dateSell);
        }
        return $aDataCache;
    }
    
    /** @Author: DungNT Mar 27, 2018
     *  @Todo: set param price discount app gas24h
     **/
    public function setDiscountGas24h() {
        $mMaterialTmp           =  new GasMaterials();// xử lý gọi 1 lần getAppPrice, không nên đưa vào vòng for
        $this->discountGas24h   = $mMaterialTmp->getAppPrice();
    }
    /** @Author: DungNT Now 30, 2018
     *  @Todo: set chiết khấu Thanh Sơn = 0, 100% ko có ck, vì đã gửi SMS 20k
     **/
    public function setDiscountZero() {
//        $aAgentCheck = [CodePartner::AGENT_THANH_SON];
        $aAgentCheck = [];
        if(in_array($this->agent_id, $aAgentCheck)){
            $this->amount_discount = 1;
        }
    }
    
    /**
     * @Author: DungNT Jun 25, 2016
     * @Todo: get post and validate from web
     */
    public function webGetPost() {
        $this->viewGasQty = 0;// Now2918 count total Gas in Order, to check something
        if(!isset($_POST['materials_id']) || !is_array($_POST['materials_id'])){
            $this->addError('materials_name',"Chi tiết đơn hàng không hợp lệ");
            return ;
        }
        $this->setDiscountZero();
        $this->autoAddVoCustomerOld();
        // Apr 09, 2017 xử lý luôn lấy địa chỉ giao hàng là đ/c của KH. Nếu KH book = app mà nhập địa chỉ vào thì sẽ bị sai, không lấy được => chưa xử lý
        $this->setAddress();
        $this->setProvinceId();// Add Mar2718
        $this->setDiscountGas24h();
        $aDataCache = $this->getPriceOfMonthSell();
//        $mAppCache = new AppCache();
//        $aDataCache     = $mAppCache->getPriceHgd(AppCache::PRICE_HGD);
        $priceHgd       = MyFunctionCustom::getHgdPriceSetup($aDataCache, $this->agent_id);
        
        $hasError       = true; $this->total = 0;
        $this->aDetail  = [];
        foreach($_POST['materials_id'] as $key=>$materials_id){
            $mDetail = new SellDetail('WindowCreate');
            $mDetail->gas_remain = 0.00; $mDetail->gas_remain_amount = 0;
            $mDetail->kg_empty = 0.00; $mDetail->kg_has_gas = 0.00;
            $this->mapSameInfoDetail($mDetail);
            $mDetail->materials_id  = $materials_id;
            $mMaterial              = $mDetail->rMaterial;
            if($mMaterial){
                $mDetail->materials_type_id     = $mMaterial->materials_type_id;
                $mDetail->materials_parent_id   = $mMaterial->parent_id;
            }
            $mDetail->seri          = isset($_POST['seri'][$key]) ? trim($_POST['seri'][$key]) : '';
            $mDetail->qty           = isset($_POST['materials_qty'][$key]) ? $_POST['materials_qty'][$key] : 0;
            $mDetail->price         = isset($_POST['price'][$key]) ? $_POST['price'][$key] : 0;
            $mDetail->price_root    = isset($priceHgd[$mDetail->materials_id]) ? $priceHgd[$mDetail->materials_id] : 0;
            if(!$this->isChangePrice && $mDetail->materials_type_id == GasMaterialsType::MATERIAL_BINH_12KG){// Dec0617 phải overide giá của AppGas24h, nếu lấy biến post lên thì khi save 2,3 lần thì sẽ bị trừ đi 2,3 lần liền => sai
                $mDetail->price     = $mDetail->price_root;
            }
            if($mDetail->materials_type_id == GasMaterialsType::MATERIAL_BINH_12KG){
                $this->viewGasQty += $mDetail->qty;
            }
            
            $mDetail->agent_id      = $this->agent_id;
            $this->calcPriceItemGasApp($mDetail);

            $mDetail->amount    = $mDetail->qty * $mDetail->price;
            $this->total        += $mDetail->amount;// add Feb 04, 2016
            if($mDetail->materials_type_id == GasMaterialsType::MATERIAL_VO_12){
                $mDetail->kg_empty      = isset($_POST['kg_empty'][$key]) ? $_POST['kg_empty'][$key] : 0.00;
                $mDetail->kg_has_gas    = isset($_POST['kg_has_gas'][$key]) ? $_POST['kg_has_gas'][$key] : 0.00;
                $mDetail->gas_remain    = $mDetail->kg_has_gas - $mDetail->kg_empty;
                $this->gas_remain       = $mDetail->gas_remain;
                $this->kg_empty         = $mDetail->kg_empty;
                $this->kg_has_gas       = $mDetail->kg_has_gas;
                $mDetail->validateData($this);
            }
            
            $mDetail->validate();
            if(!$mDetail->hasErrors()){
                if(in_array($mDetail->materials_type_id, GasMaterialsType::$SETUP_PRICE_GAS_12) ){
                    array_unshift($this->aDetail , $mDetail);
                }else{// luôn đưa gas lên item first của array
                    $this->aDetail[] = $mDetail;
                }
//                $this->aDetail[] = $mDetail;
                $hasError = false;
            }
        }
        $this->recheckToApplyGoldTime();// Now2918 xử lý tính toán lại KM giờ vàng
        $this->webReCalcPromotion();// Feb0418 tính toán lại tiền khuyến mãi khi đổi loại gas nếu giá bình khác loại bình KH đặt trên app
        $this->checkDiscount();// tính lại CKhau, giá grand_total
        $this->putDiscountToDetail();
        $this->webCheckDiscount();// Now2918 check không cho tổng đài nhập chiếu khấu quá 50k cho 1 bình
        if($hasError){
            $this->addError('customer_id',"Chi tiết đơn hàng không hợp lệ");
        }
        if($this->status == Sell::STATUS_PAID){ //NamNH Sep1019 Cập nhật lại order_type_status 
            $this->order_type_status = 1;
        }
    }
    
    /** @Author: DungNT Now 29, 2018
     *  @Todo: get max số tiền chiết khấu giảm trên 1 bình 
     **/
    public function getMaxDiscountPerOne() {
        $defaultAmount    = TransactionHistory::MAX_DISCOUNT;
        $aAgentDiff = [// cac dai ly dc chiet khau 60k
            758528,// Đại Lý Uyên Hưng - Oct1718
            123, // Đại lý Trảng Dài
            1284630, // Sep1318 Đại Lý Bửu Long Close Oct1618
            118,// Đại lý An Thạnh - Now1218
            939720, // Đại Lý Bình Chuẩn - Dec1218
            1210, //Đại Lý Thuận Giao- Dec1218
        ];
        $aProvincePlus10k = [
            GasProvince::TINH_DONG_NAI,
            GasProvince::TINH_BINH_DUONG,
        ];
        
//        if(in_array($this->agent_id, $aAgentDiff)){
        if(in_array($this->province_id, $aProvincePlus10k)){
            $defaultAmount    += 10000;// 60k
        }
        return $defaultAmount;
    }
    
    /** @Author: DungNT Now 29, 2018 
     *  @Todo: check không cho tổng đài nhập chiếu khấu quá 50k cho 1 bình
     **/
    public function webCheckDiscount() {
        $cUid = MyFormat::getCurrentUid();
        $aUidAllow = array_merge(GasTickets::$UID_DIEU_PHOI_HCM_CUSTOMER, GasConst::getArrUidUpdateAll());
        if($this->viewGasQty < 1 || in_array($cUid, $aUidAllow)){
            return ;// nếu đơn không có gas thì cho chiết khấu trên 50k
        }
        $currentDiscount = $this->amount_discount + ($this->qty_discount * $this->getPromotionDiscount());
        $maxDiscount = $this->getMaxDiscountPerOne() * $this->viewGasQty;
        if($currentDiscount > $maxDiscount ){// Now0518 fix max allow discount
            $this->addError('amount_discount', 'Giảm giá không thể quá ' .ActiveRecord::formatCurrency($maxDiscount). '.  Đơn hàng này có chiết khấu hiện tại là: ' .ActiveRecord::formatCurrency($currentDiscount). '. Chi tiết liên hệ 0918 318 328');
        }
    }

    /**
     * @Author: DungNT Jun 25, 2016
     * save record from web
     */    
    public function webSaveDetail() {
        SellDetail::deleteByRootId($this->id);
        $aRowInsert=[];
        foreach($this->aDetail as $mDetail){
            $mDetail->sell_id   = $this->id;
            $mDetail->status    = $this->status;
            $mDetail->source    = $this->source;
            $mDetail->created_date_only         = $this->created_date_only;
            $mDetail->created_date_only_bigint  = strtotime($this->created_date_only);
            $this->windowSaveDetailBuildSql($aRowInsert, $mDetail);
        }
        $this->windowSaveDetailRunSql($aRowInsert);
        if($this->runUpdateStatusTransaction){
            $this->updateStatusTransaction();
        }
    }

    /**
     * @Author: DungNT Jun 25, 2016
     */
    public function getCustomer($field_name='') {
        if($this->mCustomer){
            $mUser = $this->mCustomer;
        }else{
            $mUser = $this->rCustomer;
        }
        if($mUser){
            if(is_null($this->mCustomer)){
                $this->mCustomer = $mUser;
            }
            if($field_name != ''){
                return $mUser->$field_name;
            }
//            return "<b>".$mUser->code_bussiness."-".$mUser->first_name."</b><br>".$mUser->address."<br><b>Phone: </b>".$mUser->phone;
            return "<b>".$mUser->code_bussiness."-".$mUser->first_name."</b><br>".$this->address."";
        }
        return '';
    }
    
    public function getCodeNo() {
        return $this->code_no;
    }
    public function getCustomerPhone() {
        if(!empty($this->phone)){
            return $this->getPhone();
        }
        if($this->mCustomer){
            return $this->mCustomer->getPhoneShow();
        }
        return '';
    }
    public function getCustomerPhoneClickCall() {
        $mCallClick = new Call();
        if(!empty($this->phone)){
            return $mCallClick->buildUrlMakeCall($this->getPhone());
        }
        if($this->mCustomer){
            return $mCallClick->buildUrlMakeCall($this->mCustomer->phone);
        }
        return '';
    }
    
    public function getAgent() {
        $aAgent = CacheSession::getListAgent();
        return isset($aAgent[$this->agent_id]) ? $aAgent[$this->agent_id]['first_name'] : '';
//        $mUser = $this->rAgent;
//        if($mUser){
//            return $mUser->getFullName();
//        }
//        return "";
    }
    public function getEmployeeMaintain() {
        $mUser = $this->rEmployeeMaintain;
        if($mUser){
            return $mUser->getFullName();
        }
        return "";
    }
    public function getTransactionHistory($field_name) {
        $mTransactionHistory = $this->rTransactionHistory;
        if($mTransactionHistory){
            return $mTransactionHistory->$field_name;
        }
        return '';
    }
    public function getPtttCode() {// Jun 20, 2017
        return $this->pttt_code;
    }
    
    public function getSale() {
        $mUser = $this->rSale;
        if($mUser){
            return $mUser->getFullName();
        }
        return "";
    }
    public function getUidLogin() {
        $mUser = $this->rUidLogin;
        if($mUser){
            return $mUser->getFullName();
        }
        return "";
    }
    public function getCallCenter() {
        $mAppCache      = new AppCache();
        $listdataUser   = $mAppCache->getListdataUserByRole(ROLE_CALL_CENTER);
        return isset($listdataUser[$this->call_center_id]) ? $listdataUser[$this->call_center_id] : '';
    }
    public function getLastUpdateBy() {
        $mUser = $this->rLastUpdateBy;
        if($mUser){
            return $mUser->getFullName();
        }
        return "";
    }
    public function getCreatedDateOnly() {
        return MyFormat::dateConverYmdToDmy($this->created_date_only, "d/m/Y");
    }
    public function getCreatedDate($fomat = 'd/m/Y H:i') {
        return MyFormat::dateConverYmdToDmy($this->created_date, $fomat);
    }
    public function getLastUpdateTime() {
        return MyFormat::dateConverYmdToDmy($this->last_update_time, "d/m/Y H:i");
    }
    public function getPhone() {
        if(empty($this->phone)){
//            return $this->getCustomer('phone');
            return '';
        }
        return '0'.$this->phone;
    }
    public function getPhoneUpdate() {
        if(empty($this->phone)){
            return '';
        }
        return $this->phone = '0'.$this->phone;
    }
    // Pham Thanh Nghia 23/7/2018 
    public function getNote() {
        if($this->note){
            $aNote = explode($this->getSpareText(),$this->note);
            if(!empty($aNote[0])){
                return nl2br($aNote[0]);
            }
        }
        return '';
//        return nl2br($this->note);
    }
    public function getTypeCustomer() {
        return isset(CmsFormatter::$CUSTOMER_BO_MOI[$this->type_customer]) ? CmsFormatter::$CUSTOMER_BO_MOI[$this->type_customer] : "";
    }
    public function getInfoAccounting() {
        $res = "{$this->getCustomerPhoneClickCall()}";
        $res .= "<br>{$this->getUidLogin()}";
        $res .= "<br>{$this->getEmployeeMaintain()}";
//        if(!empty($this->sale_id)){ Sep1317 Close ko cần hiển thị nữa
//            $res .= "<br><b>CCS: {$this->getSale()}</b>";
//        }
        if(empty($this->transaction_history_id)){
            $res .= '<span class="high_light_tr"><br><b>Đơn hàng lỗi</b></span>';
        }
        return $res;
    }
    public function getDisplayErrors() {
        $res = '';
        if(empty($this->transaction_history_id)){
            $res .= '<span class="high_light_tr"><br><b>Đơn hàng lỗi</b></span>';
        }
        return $res;
    }
    public function getUrlViewCall() {
        $res = '';
        if(!empty($this->call_id)){
            $link = Yii::app()->createAbsoluteUrl('admin/transaction/index', ['ViewCall'=>$this->call_id]);
            $res =  "<a href='$link' target='_blank'>View Call</a>";
        }
        return $res;
    }
    
    public function getDateSellGrid() {
        $res = MyFormat::dateConverYmdToDmy($this->created_date_only, "d/m/Y");
//        if( ($this->first_order == Sell::IS_FIRST_ORDER && !empty($this->sale_id)) || !empty($this->pttt_code)){
        if( (!empty($this->sale_id)) || !empty($this->pttt_code)){ // Apr 09, 2019 NamNH change to get BQV2 & BQV1
            $session = Yii::app()->session;
            $res .= '<br>'.$this->getTextBqv($session['CACHE_LIST_TELESALE_ID']);
        }
        if($this->status == Sell::STATUS_CANCEL){
            $res .= "<br><b>Hủy: </b>". nl2br($this->note);
        }
        return $res;
    }
    
    /** @Author: DungNT Jan 16, 2019
     *  @Todo: text show Telesale OR App PTTT
     **/
    public function getTextBqv($aTelesale) {
        $mTargetDaily = new TargetDaily();
        $mTargetDaily->sell_id = $this->id;
        $res = '';
        if(array_key_exists($this->sale_id, $aTelesale)){
            if($mTargetDaily->countBySellId($this->sale_id)){
                $res = 'Telesale';
            }else{
                if($mTargetDaily->countBySellId()){
                    $res = 'App PTTT';
                }
            }
        }else{
            if(!empty($this->pttt_code)){
                if($mTargetDaily->countBySellId()){
                    return 'App PTTT';
                }
            }
            if($mTargetDaily->countBySellId()){
                $res = 'App PTTT';
            }
        }
        return $res;
    }
    
    public function getTypeAmount($format = false) {
        if($format){
            return ActiveRecord::formatCurrency($this->type_amount);
        }
        return $this->type_amount;
    }
    public function getGrandTotal($format = false) {
        if($format){
            return ActiveRecord::formatCurrency($this->grand_total);
        }
        return $this->grand_total;
    }
    public function getStatusCancelText() {
        $aStatusCancel = $this->getArrayStatusCancel();
        return isset($aStatusCancel[$this->status_cancel]) ? $aStatusCancel[$this->status_cancel] : '';
    }
    public function getArrayStatusCancel() {
        $mTransaction = new Transaction();
        return $mTransaction->getArrayStatusCancel();
    }
    public function getHighPriceText() {
        if($this->high_price){
            return '<br>[Giá cao]';
        }
        return '';
    }
    
    /** @Author: DungNT Jun 08, 2017
     * @Todo: get image view grid
     */
    public function getImageViewGrid() {
        $mTransactionHistory = $this->rTransactionHistory;
        if($mTransactionHistory){
            return $mTransactionHistory->getFileOnly();
        }
        return '';
    }
    public function getCustomerNewOld() {
        $aData = $this->getArrayCustomerNewOld();
        return isset($aData[$this->customer_new]) ? $aData[$this->customer_new] : '';
    }
    /**
     * @Author: DungNT Jun 25, 2016
     */
    public function InitMaterial() {
        $session=Yii::app()->session;
        if(!isset($session['MATERIAL_MODEL_SELL'])){
            $aMaterial = array_merge(GasMaterialsType::$ARR_WINDOW_GAS, GasMaterialsType::$ARR_WINDOW_VO, GasMaterialsType::$ARR_WINDOW_PROMOTION);
            $needMore = array('GetAll'=>1);
            $session['MATERIAL_MODEL_SELL'] = MyFunctionCustom::getMaterialsModel($aMaterial, $needMore);
        }
    }
    
    public function getSummaryTable() {
        $session=Yii::app()->session;
        $str = ''; $sum = 0;
        foreach($this->rDetail as $key => $item){
            $name = $unit = $seri = $br = '';
            $qty = ActiveRecord::formatNumberInput($item->qty);
//            $amount = ActiveRecord::formatCurrency($item->amount);
            $sum += $item->amount;
            if(isset($session['MATERIAL_MODEL_SELL'][$item->materials_id])){
                $name = $session['MATERIAL_MODEL_SELL'][$item->materials_id]['name'];
                $unit = $session['MATERIAL_MODEL_SELL'][$item->materials_id]['unit'];
            }
            if(!empty($item->seri)){
                $seri = "<b>Seri: </b>".$item->seri;
            }
            if($item->gas_remain > 0){
                $seri .= '<b>Gas dư: '.($item->gas_remain*1).' = '.ActiveRecord::formatCurrency($item->gas_remain_amount).'</b>';
            }
            if($key != 0){
                $br = "<br>";
            }
            $str .= "$br"." - <b>SL: </b>$qty - $name - $seri";
        }
        if($sum){
            $str .= "<br>"." - <b>Tổng tiền: </b>".ActiveRecord::formatCurrency($sum);
        }
        return $str;
    }
    
    public $viewGasName     = [];
    public $viewGasQty      = [];
    public $viewGasPrice    = [];
    public $viewGasAmount   = [];
    public $viewDiscount    = [];
    public $viewVoName      = [];
    public $viewVoQty       = [];
    public $viewKmName      = [];
    public $viewKmQty       = [];
    public $qtyDiscount     = [];
    public $amountDiscount  = [];
    public $promotionAmount = [];
    public $gasRemainSeri   = [];
    public $gasRemainQty    = [];
    public $gasRemainAmount = [];
    public $isExcelExport   = 0;
    public $amountBanVo     = [];
    public $amountBuVo      = [];
    
    /**
     * @Author: DungNT Oct 23, 2016
     * @Todo: hiện tại Gia Lai có CK mặc định khác TP + miền tây
     */
    public function getPromotionDiscount() {
        $discount = self::PROMOTION_DISCOUNT;
//        if(in_array($this->province_id, GasProvince::$PROVINCE_TAY_NGUYEN)){
//            $discount = 25000;
//        }// chưa cần thiết phải check theo province, vì trên đó đã tự chỉnh dc ck
        return $discount;
    }
    
    /** @Author: DungNT Jul 18, 2016
     *  @Todo: get str vat tu gas
     */
    public function getInfoGas() {
        $session=Yii::app()->session;
        return $this->getInfoGasDetail($session['MATERIAL_MODEL_SELL']);
    }
    
    /** @Author: DuongNV Jan 8,2019
     *  @Todo: get info gas cron excel
     *  @Param: $aMaterials MyFunctionCustom::getMaterialsModel($aMaterial, $needMore);
     **/
    public function getInfoGasCron($aMaterials) {
        return $this->getInfoGasDetail($aMaterials);
    }
    
    /** @Author: DungNT Jan 08, 2018
     *  @Todo: get str vat tu gas
     *  @Note: move to new function to run same in crone
     */
    public function getInfoGasDetail($aMaterials) {
        $str = '';
        $sumGasQty = $sumGasAmount = $sumVoQty = $sumKmQty = $amount_discount = 0;
        if($this->amount_discount > 1){
            $amount_discount    = $this->amount_discount;
        }else{
            $amount_discount    = $this->qty_discount > 0 ? ($this->qty_discount * $this->getPromotionDiscount()) : 0;
        }
        $this->viewDiscount    = $amount_discount > 1 ? ActiveRecord::formatCurrency($amount_discount) : "";
            
        foreach($this->rDetail as $key => $item){
            $this->qtyDiscount[]        = $item->qty_discount;
            $this->amountDiscount[]     = $item->getDiscountAmount();
            $this->promotionAmount[]    = $item->promotion_amount;
            if(in_array($item->materials_type_id, GasMaterialsType::$ARR_WINDOW_VO)){
                $gas_remain = '';
                if($item->gas_remain > 0){
                    $gas_remain = '<br><b>Gas dư: '.($item->gas_remain*1).' kg = '.ActiveRecord::formatCurrency($item->gas_remain_amount).'</b>';
                }
                
                $seri = !empty($item->seri) ? '. Seri : '.$item->seri.$gas_remain : '';
                $voName = isset($aMaterials[$item->materials_id]) ? $aMaterials[$item->materials_id]['name'] : '';
                if(!$this->isExcelExport){
                    $voName .= $seri;
                }
                $this->viewVoName[]     = $voName;
                $this->viewVoQty[]      = ActiveRecord::formatCurrency($item->qty);
                $this->gasRemainSeri[]  = $item->seri;
                $this->gasRemainQty[]   = $item->gas_remain;
                $this->gasRemainAmount[]= $item->gas_remain_amount;
                $this->amountBuVo[]     = $item->amount_bu_vo;
                $sumVoQty               += $item->qty;
                $this->materials_id_vo  = $item->materials_id;// Jul2119 xử lý get bù vỏ chính sách khi xuất excel 
            }elseif(in_array($item->materials_type_id, GasMaterialsType::$ARR_WINDOW_PROMOTION)){
                $this->viewKmName[]     = $aMaterials[$item->materials_id]['name'];
                $this->viewKmQty[]      = ActiveRecord::formatCurrency($item->qty);
                $sumKmQty               += $item->qty;
            }else{ // for Gas and other material val, day, vt khac
                if(in_array($item->materials_type_id, GasMaterialsType::$HGD_SELL_GAS)){
                    $this->materials_parent_id_gas  = $item->materials_parent_id;// Jul2119 xử lý get bù vỏ chính sách khi xuất excel 
                    $this->materials_id_gas         = $item->materials_id;// Jul2119 xử lý get bù vỏ chính sách khi xuất excel 
                }
                $this->amountBanVo[]        = $item->type_amount;
                $this->viewGasName[]    = isset($aMaterials[$item->materials_id]) ? $aMaterials[$item->materials_id]['name'] : '';
                $this->viewGasQty[]     = ActiveRecord::formatCurrency($item->qty);
                $amountRound = round($item->qty*$item->price, -3);
                if($this->isExcelExport){
                    $this->viewGasPrice[]   = $item->price;
                    $this->viewGasAmount[]  = $amountRound;
                }else{
                    $this->viewGasPrice[]   = ActiveRecord::formatCurrencyRound($item->price);
                    $this->viewGasAmount[]  = ActiveRecord::formatCurrencyRound($amountRound);
                }
                $sumGasQty              += $item->qty;
                $sumGasAmount           += $amountRound;
            }
        }
        if($this->isExcelExport){
            return $this->viewGasName; //ra excel xử lý foreach cái gasname này để cho đúng dòng
        }
        
        $this->viewVoName   = implode('<br>', $this->viewVoName);
        $this->viewVoQty    = implode('<br>', $this->viewVoQty)."<span class='display_none sumVoQty'>$sumVoQty</span>";
        $this->viewKmName   = implode('<br>', $this->viewKmName);
        $this->viewKmQty    = implode('<br>', $this->viewKmQty)."<span class='display_none sumKmQty'>$sumKmQty</span>";
        $this->viewGasName  = implode('<br>', $this->viewGasName);
        $this->viewGasQty   = implode('<br>', $this->viewGasQty)."<span class='display_none sumGasQty'>$sumGasQty</span>";
        $this->viewGasPrice = implode('<br>', $this->viewGasPrice);
        $this->viewGasAmount = implode('<br>', $this->viewGasAmount)."<span class='display_none sumGasAmount'>$sumGasAmount</span>";
        $this->viewDiscount = $this->viewDiscount."<span class='display_none sumDiscountQty'>$amount_discount</span>";
        
        $sBuVo = '';
        if($this->amount_bu_vo > 0){
            $sBuVo = '<b>Bù vỏ: '.$this->getAmountBuBo(true).' </b><br>';
        }
        
        if($this->order_type != Sell::ORDER_TYPE_NORMAL){
            $typeAmount = $this->getTypeAmount();
            if($this->order_type == Sell::ORDER_TYPE_THU_VO){
                $typeAmount = 0 - $typeAmount;
            }
            
            $span = "<span class='display_none sumGasAmount'>$typeAmount</span>";
            $this->viewGasName = "$span<b>Loại: {$this->getOrderType()} {$this->getTypeAmount(true)}</b><br>".$this->viewGasName;
        }
        if(trim($this->getNote()) != ''){
            $this->viewGasName = $this->viewGasName."<br><b> Ghi chú: </b>{$this->getNote()}";
        }
        return $sBuVo.$this->viewGasName;
    }
    
    /**
     * @Author: DungNT Aug 23, 2016
     * @Todo: get info detail
     * @param: ex: viewGasName, viewGasQty, viewGasQty....
     */
    public function getFieldDetail($nameVar) {
    }

    /**
     * @Author: DungNT Aug 23, 2016
     * @Todo: backup before change view index
     */
    public function getInfoGasBackUp() {
        $session=Yii::app()->session;
        $str = "";
        $sum = 0;
        foreach($this->rDetail as $key => $item){
            if(!in_array($item->materials_type_id, GasMaterialsType::$HGD_SELL_GAS)){
                continue;
            }
            $name = "";
            $seri = "";
            $br = "";
            $qty = ActiveRecord::formatNumberInput($item->qty);
            $sum += $item->amount;
            if(isset($session['MATERIAL_MODEL_SELL'][$item->materials_id])){
                $name = $session['MATERIAL_MODEL_SELL'][$item->materials_id]['name'];
            }
            if(!empty($item->seri)){
                $seri = "<b> - Seri: </b>".$item->seri;
            }
            if($key != 0){
                $br = "<br>";
            }
            $str .= "$br"." <b>SL: </b>$qty - $name $seri";
        }
        if($sum){
            $str .= "<br>"." <b>".ActiveRecord::formatCurrency($sum)."</b>";
        }
        return $str;
    }
    
    /**
     * @Author: DungNT Jul 18, 2016
     * @Todo: get str vat tu Vo
     */
    public function getInfoVo() {
        $session=Yii::app()->session;
        $str = "";
        foreach($this->rDetail as $key => $item){
            if(!in_array($item->materials_type_id, GasMaterialsType::$ARR_WINDOW_VO)){
                continue;
            }
            $name = "";
            $br = "";
            $qty = ActiveRecord::formatNumberInput($item->qty);
            if(isset($session['MATERIAL_MODEL_SELL'][$item->materials_id])){
                $name = $session['MATERIAL_MODEL_SELL'][$item->materials_id]['name'];
            }
            if($key != 0){
                $br = "<br>";
            }
            $str .= "$br"." <b>SL: </b>$qty - $name";
        }
        return $str;
    }
    
    /**
     * @Author: DungNT Jul 18, 2016
     * @Todo: get str vat tu Hang KM
     */
    public function getInfoKm() {
        $session=Yii::app()->session;
        $str = "";
        foreach($this->rDetail as $key => $item){
            if(!in_array($item->materials_type_id, GasMaterialsType::$ARR_WINDOW_PROMOTION)){
                continue;
            }
            $name = "";
            $br = "";
            $qty = ActiveRecord::formatNumberInput($item->qty);
            if(isset($session['MATERIAL_MODEL_SELL'][$item->materials_id])){
                $name = $session['MATERIAL_MODEL_SELL'][$item->materials_id]['name'];
            }
            if($key != 0){
                $br = "<br>";
            }
            $str .= "$br"." <b>SL: </b>$qty - $name";
        }
        return $str;
    }
    
    /**
     * @Author: DungNT Aug 19, 2016
     * @Todo: HGD get bán hàng rồi group theo từng loại vật tư: gas, vỏ, KM 
     * để auto insert table storecard HGD
     */
    public function getDataByDate() {
        $criteria = new CDbCriteria();
        if(!empty($this->agent_id)){
            $criteria->compare("t.agent_id", $this->agent_id);
        }
        $sParamsIn = implode(',', array(Sell::STATUS_PAID));
        $criteria->addCondition("t.status IN ($sParamsIn)");
        $criteria->compare("t.created_date_only", $this->created_date_only);
        $criteria->select = "sum(t.qty) as qty, sum(t.amount) as amount, t.agent_id, t.materials_type_id, t.materials_id, t.uid_login ";
        $criteria->group = "t.agent_id, t.materials_id";
        $criteria->order = "t.materials_type_id";
        $models = SellDetail::model()->findAll($criteria);
        $aRes = [];
        foreach($models as $item){
            $aRes['OUTPUT'][$item->agent_id][$item->materials_type_id][$item->materials_id] = $item->qty;
            $aRes['EMPLOYEE'][$item->agent_id] = $item->uid_login;
        }
        return $aRes;
    }
    
    /**
     * @Author: DungNT Aug 19, 2016
     * @Todo: auto save storecard HGD
     * Aug 25, 2016 fix cho đại lý tự click update 5 phút 1 lần, sẽ update luôn 2 ngày liên tục
     * @note: ở đây có 2 kiểu update
     * 1. là kiểu user bấm thì update 2 ngày liên tục gồm ngày hôm trc và ngày hiện tại
     * 2. kiểu admin update thì là ngày bất kỳ update + ngày next
     */
    public function autoSaveStoreCardHgd() {
        $from = time();
        // Note ở đây $this->created_date_only là ngày trước
        $dateNext = MyFormat::modifyDays($this->created_date_only, 1, '+');
        $aDate = array($this->created_date_only, $dateNext);
        foreach($aDate as $dateRun){
            $this->created_date_only = $dateRun;
            $mStoreCardApi = new GasStoreCardApi();
            $mStoreCardApi->hgdAddByDate($this);
//            $info = "Agent click update on WEB Ban Hang Agent: $this->agent_id -- id: $this->id --- Date: $this->created_date_only -- Created_date: $this->created_date";
//            Logger::WriteLog($info);
            
            $release_date = MyFormat::dateConverYmdToDmy($dateRun, 'd-m-Y');
            $model = new GasCashBookDetail();// May 23, 2017 bổ sung phần cập nhật sang sổ quỹ
            $model->makeRecordHgdAllAgent([$this->agent_id],  $release_date);
        }
        $to = time();
        $second = $to-$from;
        $sDate = implode("  ***  ", $aDate);
        $info = "Agent: $this->agent_id  click update Storecard HGD  done in: $second  Second  <=> ".($second/60)." Minutes. Date $sDate";
//        Logger::WriteLog($info);
    }

    /**
     * @Author: DungNT May 14, 2017
     * @Todo: app giao nhận click update trên phone
     */
    public function appAgentClickUpdate() {
        $datePrev                = MyFormat::modifyDays(date('Y-m-d'), 1, '-');
        $this->created_date_only = $datePrev;
        $this->autoSaveStoreCardHgd();
    }
    
    /**
     * @Author: DungNT Aug 25, 2016
     * @Todo: cho đại lý tự click update trên web
     */
    public function handleAgentClickUpdate() {
        $cRole = Yii::app()->user->role_id;
        if($this->canClickUpdate()){
            if($cRole == ROLE_SUB_USER_AGENT){
                $this->agent_id          = MyFormat::getAgentId();
                $datePrev                = MyFormat::modifyDays(date('Y-m-d'), 1, '-');
                $this->created_date_only = $datePrev;
//                $this->initSessionUpdate(); // Close may1417
//                $mSell = $this->getRecordByDate();// Close on Oct 01, 2016 thấy bị dư, vì nếu ko có record thì hệ thống cũng không làm gì cả
//                if($mSell){
                    $this->autoSaveStoreCardHgd();
                    if(!is_null($this->modelMonitorOld)){
                        $this->modelMonitorOld->setNextTimeUpdate();
                    }
//                    $this->setSessionUpdate(); // Close may1417
                    return true;
//                }
            }else{ // Cho kế toán VP Dung Phương update
                GasStoreCardApi::hgdCronGenStoreCard();
                $this->setSessionUpdate();
                return true;
            }
        }
        return false;
    }
    
    /**
     * @Author: DungNT Aug 25, 2016
     * @Todo: get record of agent by $agent_id, $created_date_only
     */
    public function getRecordByDate() {
        $criteria = new CDbCriteria();
        $criteria->compare("t.agent_id", $this->agent_id);
        $criteria->compare("t.created_date_only", $this->created_date_only);
        return self::model()->find($criteria);
    }

    /**
     * @Author: DungNT Aug 25, 2016
     * check chỉ chi phép agent click update đc
     */
    public function canClickUpdate() {
        $cRole = MyFormat::getCurrentRoleId();
//        if($cRole == ROLE_SUB_USER_AGENT || $this->canActionClickUpdateStorecard()){
        if($cRole == ROLE_SUB_USER_AGENT){// chỉ cho phép sub agent update
            // 1. check next time update
            $mMonitorUpdate = new MonitorUpdate();
            $mMonitorUpdate->agent_id       = MyFormat::getAgentId();
            $mMonitorUpdate->type           = MonitorUpdate::TYPE_STORECARD_HGD;
            $mMonitorUpdate->timeSetUpdate  = MonitorUpdate::TIME_STORECARD_HGD;
//            if($this->canUpdateFromSession()){ close May1417
            $canUpdate          = $mMonitorUpdate->cashbookAllowGenReport();
            $this->date_from_ymd = $mMonitorUpdate->nexTimeUpdate;// may14201701 dùng tạm biến date_from_ymd để lưu date
            if($canUpdate){
                $this->modelMonitorOld = $mMonitorUpdate->modelMonitorOld;
                return true;
            }
        }
        return false;
    }
    
    /**
     * @Author: DungNT Aug 25, 2016
     * @Todo: khởi tạo session cho user, mỗi lần update chỉ dc cách nhau 10 phút
     */
    public function initSessionUpdate() {
        $session=Yii::app()->session;
        if(!isset($session['AgentUpdateSell'])){
            $session['AgentUpdateSell'] = date('Y-m-d H:i:s');
        }
    }
    public function getSessionUpdate() {
        $session=Yii::app()->session;
        return $session['AgentUpdateSell'];
    }
    public function setSessionUpdate() {
        $session=Yii::app()->session;
        $session['AgentUpdateSell'] = date('Y-m-d H:i:s');
    }
    public function canUpdateFromSession() {
        $session=Yii::app()->session;
        if(!isset($session['AgentUpdateSell'])){
            return true;
        }
        $dateTimePrev   = $this->getSessionUpdate();
        $dateTimeCheck  = MyFormat::addDays($dateTimePrev, $this->minutesAgentUpdate, "+", 'minutes', 'Y-m-d H:i:s');
//        echo '<pre>';
//        print_r($dateTimeCheck);
//        echo '</pre>---------';
//        var_dump(MyFormat::compareTwoDate(date('Y-m-d H:i:s'), $dateTimeCheck));
        return MyFormat::compareTwoDate(date('Y-m-d H:i:s'), $dateTimeCheck);
    }
    
    /**
     * @Author: DungNT Aug 25, 2016
     * @Todo: get next update of agent 
     */
    public function getNextUpdateTime() {
        $session=Yii::app()->session;
        if(!isset($session['AgentUpdateSell'])){
            $date = date('Y-m-d H:i:s');
        }else{
            $date = MyFormat::addDays($session['AgentUpdateSell'], $this->minutesAgentUpdate, "+", 'minutes', 'Y-m-d H:i:s');
        }
        return MyFormat::dateConverYmdToDmy($date, "H:i d/m/Y");
    }
    
    protected function beforeDelete() {
        SellDetail::deleteByRootId($this->id);
        TransactionHistory::deleteBySell($this->id);
        $mGasRemain = new GasRemain();
        $mGasRemain->deleteBySellId($this);
        return parent::beforeDelete();
    }

    protected function afterDelete() {
        $this->adminUpdateStoreCard();
        return parent::afterDelete();
    }
    
    /** only for dev action
     * @Author: DungNT Aug 24, 2016
     */
    public static function devDeleteDraftData() {
        $criteria = new CDbCriteria();
//            $criteria->compare("t.customer_id", 869973);
        $criteria->addCondition("t.created_date_only < '2016-06-29'");
        $models = Sell::model()->findAll($criteria);
        echo count($models);die;
        foreach($models as $model){
            $model->delete();
        }
        die;
    }
    
    /**
     * @Author: DungNT Aug 21, 2016
     * @Todo: HGD get bán hàng theo ngày tạo hôm nay 
     * để auto insert table storecard HGD
     * @param: $date Y-m-d
     */
    public static function getAllDataByDate($date) {
        $criteria = new CDbCriteria();
//        $sParamsIn = implode(',', array(GasConst::AGENT_CH3));// Aug 29, 2016 vì ch3 sử dụng để test
//        $criteria->addCondition("t.agent_id NOT IN ($sParamsIn)");
        
//        $criteria->addInCondition("t.status", array(Sell::STATUS_PAID));// Aug 24, 2016
        $sParamsIn = implode(',', array(Sell::STATUS_PAID));
        $criteria->addCondition("t.status IN ($sParamsIn)");        
//        $criteria->compare('t.created_date_only', $date); DungNT close Sep1019
        DateHelper::searchBetween($date, $date, 'created_date_only_bigint', $criteria, false);
        $criteria->group = 't.agent_id, t.created_date_only';
        // Aug 22, 2016 xử lý group t.created_date_only để cập nhật luôn những bán hàng hôm tr đc tạo trong hôm nay
        return Sell::model()->findAll($criteria);
    }
    
    /** @Author: DungNT Mar 17, 2018
     *  @Todo: convert province_id to list agent
     **/
    public function provinceIdToAgent() {
        return ;// Sep0718 AnhDung Close
        $mInventoryCustomer = new InventoryCustomer();
        if(!is_array($this->province_id)){
            return ;
        }
        $this->agent_id = $mInventoryCustomer->getAgentOfProvince($this->province_id);
//        echo '<pre>';
//        print_r($this->agent_id);
//        echo '</pre>';
//        die;
    }
    
    /**
     * @Author: DungNT Aug 23, 2016
     * @Todo: report summary HGD get bán hàng rồi group theo đại lý, từng loại vật tư: gas, vỏ, KM 
     */
    public function ReportAgent() {
        if($this->type_report_agent == self::TYPE_INDEX_AGENT){
            return $this->getReportAgentV2();
        }
        $criteriaOutput = new CDbCriteria();
        $criteriaOutput->select = 'sum(t.gas_remain_amount) as gas_remain_amount,sum(t.gas_remain) as gas_remain, sum(t.promotion_amount) as promotion_amount, sum(t.amount_bu_vo) as amount_bu_vo, sum(t.qty) as qty, sum(t.amount) as amount, sum(t.qty_discount) as qty_discount, sum(t.amount_discount) as amount_discount, t.agent_id, t.materials_type_id, t.materials_id, t.price';
        if($this->typeReport == Sell::REPORT_WEB){
            $this->provinceIdToAgent();
            $criteriaOutput->group  = 't.materials_id';// Close Mar1718 khong can thiet phai group theo agent, vi tren dieu kien search da limit roi
        }else{
            $criteriaOutput->group  = 't.agent_id, t.materials_id';// Close Mar1718 khong can thiet phai group theo agent, vi tren dieu kien search da limit roi
        }
        $this->reportAgentCriteriaOutPut($criteriaOutput);
        $criteriaOutput->order  = 't.materials_type_id';
        $models = SellDetail::model()->findAll($criteriaOutput);
        $aRes = [];
        foreach($models as $item){
            $tmp = array(
                'qty'               => $item->qty,
                'amount'            => round($item->amount, -3),
                'price'             => $item->price,
                'qty_discount'      => $item->qty_discount,
                'amount_discount'   => $item->amount_discount,
                'amount_bu_vo'      => $item->amount_bu_vo,
                'promotion_amount'  => $item->promotion_amount,// Sep2017
                'gas_remain_amount' => $item->gas_remain_amount,// Mar1518
                'gas_remain'        => $item->gas_remain,// Nov1218
            );
            $agent_id = $item->agent_id;
            if($this->isWebSumAll){// fix report web each agent Sep0418 - để Nam làm bc mới
                $agent_id = 1;
            }
            $aRes['OUTPUT'][$agent_id][$item->materials_type_id][$item->materials_id] = $tmp;
        }
        
        $criteriaOtherAmount = new CDbCriteria();// để sum các khoản khác như bù vỏ thế chân, thu vỏ
        $this->reportAgentCriteriaOutPut($criteriaOtherAmount);
//        $criteriaOtherAmount->addNotInCondition("t.order_type", array(Sell::ORDER_TYPE_NORMAL));
        $sParamsIn = implode(',', array(Sell::ORDER_TYPE_NORMAL));
        $criteriaOtherAmount->addCondition("t.order_type NOT IN ($sParamsIn)");
        $criteriaOtherAmount->select    = 'sum(t.type_amount) as type_amount, t.order_type, t.agent_id, t.materials_id';
        if($this->typeReport == Sell::REPORT_WEB){
            $criteriaOtherAmount->group     = 't.materials_id, t.order_type';
        }else{
            $criteriaOtherAmount->group     = 't.agent_id, t.materials_id, t.order_type';
        }
        $criteriaOtherAmount->having    = 'type_amount > 0';
        $models = SellDetail::model()->findAll($criteriaOtherAmount);
        foreach($models as $item){
            $agent_id = $item->agent_id;
            if($this->isWebSumAll){// fix report web each agent Sep0418 - để Nam làm bc mới
                $agent_id = 1;
            }
            $aRes['OUTPUT_OTHER'][$agent_id][$item->materials_id][$item->order_type] = round($item->type_amount, -3);
        }
        return $aRes;
    }
        
    /**
     * @Author: DungNT Aug 31, 2016
     */
    public function reportAgentCriteriaOutPut(&$criteria) {
        if(is_array($this->agent_id)){
            $sParamsIn = implode(',', $this->agent_id);
            $criteria->addCondition("t.agent_id IN ($sParamsIn)");
        }elseif(!empty($this->agent_id)){
            $criteria->addCondition('t.agent_id=' . $this->agent_id);
        }
        if(is_array($this->province_id)){
            $sParamsIn = implode(',', $this->province_id);
            $criteria->addCondition("t.province_id IN ($sParamsIn)");
        }
        $criteria->addCondition('t.status=' . $this->status);
        if(!empty($this->employee_maintain_id)){
            $criteria->addCondition('t.employee_maintain_id=' . $this->employee_maintain_id);
        }
        $this->date_from_ymd  = MyFormat::dateDmyToYmdForAllIndexSearch($this->date_from);
        $this->date_to_ymd    = MyFormat::dateDmyToYmdForAllIndexSearch($this->date_to);
//        $criteria->addBetweenCondition('t.created_date_only', $this->date_from_ymd, $this->date_to_ymd);
        DateHelper::searchBetween($this->date_from_ymd, $this->date_to_ymd, 'created_date_only_bigint', $criteria, false);
    }
    
    
    /**
     * @Author: DungNT May 09, 2017
     * @Todo: report summary HGD get bán hàng rồi group theo đại lý + employee, từng loại vật tư: gas, vỏ, KM 
     * @note: dùng cho one agent, group by agent and employee, dùng cron sinh tự động vào báo cáo cashbook của agent
     */
    public function reportMultiAgent() {
        $criteriaOutput = new CDbCriteria();
        $this->reportAgentCriteriaOutPut($criteriaOutput);
        $criteriaOutput->select = 'sum(t.gas_remain_amount) as gas_remain_amount, sum(t.promotion_amount) as promotion_amount, sum(t.amount_bu_vo) as amount_bu_vo, sum(t.qty) as qty, sum(t.amount) as amount, sum(t.qty_discount) as qty_discount, sum(t.amount_discount) as amount_discount, t.agent_id, t.employee_maintain_id, t.materials_type_id, t.materials_id, t.price';
        $criteriaOutput->group  = 't.agent_id, t.employee_maintain_id, t.materials_id';
        $criteriaOutput->order  = 't.materials_type_id';
        $models = SellDetail::model()->findAll($criteriaOutput);
        $aRes = [];
        foreach($models as $item){
            $tmp = array(
                'qty'               => $item->qty,
//                'amount'            => round($item->amount, -3),// Close Jul 14, 2017 vì khi sinh bc quỹ bị sai đến đơn vị đồng
                'amount'            => $item->amount,
                'price'             => $item->price,
                'qty_discount'      => $item->qty_discount,
                'amount_discount'   => $item->amount_discount,
                'amount_bu_vo'      => $item->amount_bu_vo,
                'promotion_amount'  => $item->promotion_amount,// Sep2017
                'gas_remain_amount' => $item->gas_remain_amount,// Mar1518
            );
            $aRes[$item->employee_maintain_id]['OUTPUT'][$item->agent_id][$item->materials_type_id][$item->materials_id] = $tmp;
        }
        
        $criteriaOtherAmount = new CDbCriteria();// để sum các khoản khác như bù vỏ thế chân, thu vỏ
        $this->reportAgentCriteriaOutPut($criteriaOtherAmount);
//        $criteriaOtherAmount->addNotInCondition("t.order_type", array(Sell::ORDER_TYPE_NORMAL));
        $sParamsIn = implode(',', array(Sell::ORDER_TYPE_NORMAL));
        $criteriaOtherAmount->addCondition("t.order_type NOT IN ($sParamsIn)");
        $criteriaOtherAmount->select    = 'sum(t.type_amount) as type_amount, t.order_type, t.agent_id, t.employee_maintain_id, t.materials_id';
        $criteriaOtherAmount->group     = 't.agent_id, t.employee_maintain_id, t.materials_id, t.order_type';
        $criteriaOtherAmount->having    = 'type_amount > 0';
        $models = SellDetail::model()->findAll($criteriaOtherAmount);
        foreach($models as $item){
//            $aRes[$item->employee_maintain_id]['OUTPUT_OTHER'][$item->agent_id][$item->materials_id][$item->order_type] = round($item->type_amount, -3);// Close Jul 14, 2017 vì khi sinh bc quỹ bị sai đến đơn vị đồng
            $aRes[$item->employee_maintain_id]['OUTPUT_OTHER'][$item->agent_id][$item->materials_id][$item->order_type] = $item->type_amount;
        }
        return $aRes;
    }
    
    
    /** @Author: DungNT Aug 26, 2016
     *  @Todo: get history of customer 3 latest Order
     */
    public function windowGetHistory($needMore = []) {
        $today = date('Y-m-d');
//        $c1 = 't.status=' . Sell::STATUS_PAID. ' AND t.customer_id=' . $this->customer_id;
        // chỗ này phải lấy tất cả đơn hàng của KH, kể cả đơn hủy
        $c1 = 't.customer_id=' . $this->customer_id;
        $c2 = "t.created_date_only='$today'".' AND t.customer_id=' . $this->customer_id;
        $criteria = new CDbCriteria();
        $criteria->addCondition("($c1) OR ($c2)");

        $criteria->limit = 3;
        $criteria->order = 't.id DESC';
        $models = Sell::model()->findAll($criteria);
        if(isset($needMore['GetModel'])){
            return $models;
        }
        return $this->windowFormatResponse($models);
    }
    
    /**
     * @Author: DungNT Aug 26, 2016
     * @Todo: format response to window
     */
    public function windowFormatResponse($models) {
        $aRes = [];
        foreach($models as $model){
            $tmp = [];
            $tmp['agent_id']                = $model->agent_id;
            $tmp['created_date']            = MyFormat::dateConverYmdToDmy($model->created_date, "d-m-Y");
            $tmp['customer_id']             = $model->customer_id;
//            $tmp['customer_id']             = $model->getCustomer('first_name');
            $tmp['employee_maintain_id']    = $model->employee_maintain_id;
            $tmp['monitor_market_development_id']   = "";
            $tmp['note']                    = $model->note;
            $tmp['order_type']              = $model->order_type;
            $tmp['type_amount']             = empty($model->type_amount) ? 0 : $model->type_amount;
            $tmp['note']                    = $model->note;
//            $tmp['qty_discount']    = $model->qty_discount;
            $tmp['order_detail']            = $model->windowFormatResponseDetail();
            $aRes[] = $tmp;
        }
        return $aRes;
    }
    
    /**
     * @Author: DungNT Aug 26, 2016
     * @Todo: format response to window
     */
    public function windowFormatResponseDetail() {
        $res = [];
        foreach($this->rDetail as $key => $item){
            $tmp = [];
            $tmp['amount']          = $item->amount;
            $tmp['materials_id']    = $item->materials_id;
            $tmp['materials_type_id'] = $item->materials_type_id;
            $tmp['price']           = $item->price;
            $tmp['qty']             = $item->qty;
            $tmp['seri']            = $item->seri;
            $res[] = $tmp;
        }
        return $res;
    }
    
    /**
     * @Author: DungNT Aug 29, 2016
     * @Todo: những user dc phép bấm click update bán hàng Hộ GD
     */
    public function canActionClickUpdateStorecard() {
        $ok = false;
        $cRole  = MyFormat::getCurrentRoleId();
        $cUid   = MyFormat::getCurrentUid();
        $aUidClick = array(GasLeave::DUNG_NTT, GasLeave::PHUONG_PTK);
        if($cRole == ROLE_SUB_USER_AGENT){
            $ok = true;
        }elseif(in_array($cUid, $aUidClick)){
            $ok = false;// Sep 20, 2016 không cho 2 user này click update nữa
//            $ok = true;
        }
        return $ok;
    }
    
    /**
     * @Author: DungNT Sep 07, 2016
     * @Todo: lấy id agent để check limit không cho tạo ở thẻ kho các thẻ kho hộ GD với các đại lý chạy PM mới
     */
    public static function getAllAgentRunNew() {
        $date = date('Y-m-d');
        $datePrev = MyFormat::modifyDays($date, 1, '-');
        $criteria = new CDbCriteria();
//        $sParamsIn = implode(',', array(GasConst::AGENT_CH3)); // close on Oct 17, 2016 đã live
//        $criteria->addCondition("t.agent_id NOT IN ($sParamsIn)");
        
        $criteria->compare("t.created_date_only", $datePrev);
        $criteria->select   = "t.agent_id";
        $criteria->group    = "t.agent_id";
        // Aug 22, 2016 xử lý group t.created_date_only để cập nhật luôn những bán hàng hôm tr đc tạo trong hôm nay
        return CHtml::listData(Sell::model()->findAll($criteria), 'agent_id', 'agent_id');
    }
    
    /**
     * @Author: DungNT Sep 16, 2016
     * @Todo: cron remove record draft > 30 days
     */
    public static function cronDeleteDraft() {
        return ;// Close on Sep2917 để dữ liệu lại phân tích, ko xóa nữa
        $criteria = new CDbCriteria();
        $statusDelete = array(Sell::STATUS_NEW, Sell::STATUS_CANCEL);
        $criteria->addInCondition("status", $statusDelete);
        $daysDelete = 30;
        $criteria->addCondition("DATE_ADD(created_date_only, INTERVAL $daysDelete DAY) < CURDATE()");
        Sell::model()->deleteAll($criteria);
        SellDetail::model()->deleteAll($criteria);
    }
    
    /**
     * @Author: DungNT Jul 08, 2016
     * @Todo: 1/ sản lượng CCS bán hàng ngày
     */
    public function getOutputCCS() {
        $date_from = MyFormat::dateDmyToYmdForAllIndexSearch($this->date_from);
        $date_to = MyFormat::dateDmyToYmdForAllIndexSearch($this->date_to);
        $criteria = new CDBcriteria();
        $criteria->addBetweenCondition("t.created_date_only",$date_from, $date_to);
        $criteria->compare("t.status", Sell::STATUS_PAID);
        $criteria->compare("t.agent_id", $this->agent_id);
        $criteria->addCondition("sale_id<>0");
//        $criteria->addInCondition("t.type_customer", array(STORE_CARD_HGD_CCS));
        $sParamsIn = implode(',', array(STORE_CARD_HGD_CCS));
        $criteria->addCondition("t.type_customer IN ($sParamsIn)");
        
        $criteria->compare("t.materials_type_id", GasMaterialsType::MATERIAL_BINH_12KG);
        $criteria->group = "month(t.created_date_only), year(t.created_date_only), t.sale_id";
        $criteria->select = "sum(t.qty) as qty, t.sale_id, t.monitoring_id, month(t.created_date_only) as date_month, year(t.created_date_only) as date_year ";
        $criteria->order = "t.monitoring_id ASC";
        $models = SellDetail::model()->findAll($criteria);
        $aRes = [];
        foreach($models as $item){
            $aRes['OUTPUT'][$item->date_year][$item->date_month][$item->monitoring_id][$item->sale_id] = $item->qty;
            $aRes['CCS_ID'][$item->monitoring_id][$item->sale_id] = $item->qty;
        }
        return $aRes;
    }
    
    /**
     * @Author: DungNT Sep 19, 2016
     * count số điểm của ccs trong ngày
     */
    public static function ccsCountAll($mUserLogin) {
        $criteria = new CDbCriteria();
        Sell::ccsSameCriteria($criteria, $mUserLogin->id);
        return SellDetail::model()->count($criteria);
    }
    
    /**
     * @Author: DungNT Sep 20, 2016
     * get list customer id đã mua hàng của CCS
     */
    public static function ccsGetCustomerBuy($mUserLogin) {
        $criteria = new CDbCriteria();
        Sell::ccsSameCriteria($criteria, $mUserLogin->id);
        return CHtml::listData(SellDetail::model()->findAll($criteria), 'customer_id', 'customer_id');
    }
    
    /**
     * @Author: DungNT Sep 20, 2016
     * @Todo: get criteria same get những bán hàng của 1 CCS
     */
    public static function ccsSameCriteria(&$criteria, $sale_id) {
        $criteria->compare('t.sale_id', $sale_id);
        $criteria->compare('t.type_customer', STORE_CARD_HGD_CCS);
        $criteria->compare("t.status", Sell::STATUS_PAID);
        $criteria->compare("t.materials_type_id", GasMaterialsType::MATERIAL_BINH_12KG);
    }
    
    public function canUpdateWeb() {
        $cRole      = MyFormat::getCurrentRoleId();
        $cUid       = MyFormat::getCurrentUid();
        $dayAllow   = date('Y-m-d');// chỉ cho phép nhân viên CallCenter cập nhật trong ngày
        $dayAllow   = MyFormat::modifyDays($dayAllow, 1, '-');// cho phép update trong ngày
        
        if($cRole == ROLE_ADMIN){
            return true;
        }
        if( !in_array($cRole, $this->getRoleAllowCreate()) ){
            // chỉ cho update record tạo từ web và của đại lý đó, thỏa mãn ngày update
            return false;
        }
        if(in_array($cUid, GasConst::getArrUidUpdateAll())){
            return GasConst::canUpdateAll($this->created_date);
        }
        $aStatusNotAllow = [Sell::STATUS_CANCEL, Sell::STATUS_PAID];
        if($this->status == Sell::STATUS_CANCEL){// Dec0217 không cho sửa đơn hủy luôn
//            $aCancelAllow = [Transaction::CANCEL_AGENT_WRONG, Transaction::CANCEL_DOUBLE_ORDER];
            $aCancelAllow = [Transaction::CANCEL_SUPPORT_AGENT_BUSY];
            if(in_array($this->status_cancel, $aCancelAllow) && $this->uid_login == $cUid){
                return MyFormat::compareTwoDate($this->created_date_only, $dayAllow);
            }
        }
        if($this->status == Sell::STATUS_CANCEL && $this->source == Sell::SOURCE_APP){// Feb1318 cho sửa đơn App hủy
            return MyFormat::compareTwoDate($this->created_date_only, $dayAllow);
        }
        if(in_array($this->status, $aStatusNotAllow) || 
                (in_array($cRole, $this->getRoleAllowCreate()) && $this->uid_login != $cUid && $this->uid_login != GasConst::UID_ADMIN)){
            return false;
        }

        return MyFormat::compareTwoDate($this->created_date_only, $dayAllow);
        return $this->isValidDayUpdate();
    }
    
    /**
     * @Author: DungNT Nov 06, 2016
     * @Todo: copy info from transaction to sell
     * @param: $mTransactionHistory 
     */
    public function mapTransaction() {
        if(is_null($this->mTransaction)){
            return ;
        }
        if(in_array($this->mTransaction->status, $this->mTransaction->getArrayStatusCancel())){
            throw new Exception('Khách hàng đã hủy đơn hàng App, không thể tạo đơn hàng', SpjError::SELL001);
        }
        $mTransactionHistory = $this->mTransaction;
        $mTransactionHistory->checkUserCallCenter();
//        $mTransactionHistory->countAppCatch();
        // ??? tạo bán hàng nhưng chưa tạo KH trên hệ thống, có dc ko ??????
        $this->source               = Sell::SOURCE_APP;
        $this->customer_id          = $mTransactionHistory->customer_id;
        $this->agent_id             = $mTransactionHistory->agent_id;
        $this->type_customer        = $mTransactionHistory->type_customer;
        $this->created_date_only    = MyFormat::dateConverYmdToDmy($mTransactionHistory->created_date_only);
        
        $this->transaction_history_id   = $mTransactionHistory->id;
        $this->app_promotion_user_id    = $mTransactionHistory->app_promotion_user_id;
        $this->promotion_id             = $mTransactionHistory->promotion_id;
        $this->promotion_amount         = $mTransactionHistory->promotion_amount;
        $this->promotion_type           = $mTransactionHistory->promotion_type;
        $this->address                  = $mTransactionHistory->address;
        $this->phone                    = $mTransactionHistory->getPhone();
        $this->platform                 = $mTransactionHistory->platform;
//        [TASKTIMER]
        $this->delivery_timer                 = MyFormat::dateConverYmdToDmy($mTransactionHistory->delivery_timer, 'd/m/Y H:i:s');
        $this->is_timer                 = isset($mTransactionHistory->is_timer) ? $mTransactionHistory->is_timer : Forecast::TYPE_IMMEDIATE;
        $this->v1_discount_amount       = $mTransactionHistory->v1_discount_amount;
        $this->v1_discount_id           = $mTransactionHistory->v1_discount_id;

        
        $aDetail = json_decode($mTransactionHistory->json_detail, true);
        if(is_array($aDetail)){
            foreach($aDetail as $transDetail){
                $mDetail = new SellDetail('WindowCreate');
                // còn thiếu nguyên những field ở dưới, mới chỉ có materials_id
                $this->mapSameInfoDetail($mDetail);
                $mDetail->materials_id          = $transDetail['materials_id'];
                $mDetail->materials_type_id     = $transDetail['materials_type_id'];
                $mDetail->qty                   = 1;
                $mDetail->price     = $transDetail['price'];
                $mDetail->amount    = $transDetail['amount'];
                $mDetail->seri      = isset($transDetail['seri']) ? $transDetail['seri'] : '';
                $this->aDetail[]    = $mDetail;
            }
        }
        $mTransactionHistory->setUserCallCenter();
    }
    
    /**
     * @Author: DungNT Nov 26, 2016
     * @Todo: xác định chiết khấu mặc định hay chỉnh sửa
     */
    public function getDiscountType() {
        $this->discount_type = Sell::DISCOUNT_DEFAULT;
        if($this->amount_discount > 0){
            $this->discount_type = Sell::DISCOUNT_CHANGE;
        }
    }
    public function getAmountBuBo($format = false) {
        if($format){
            return ActiveRecord::formatCurrency($this->amount_bu_vo);
        }
        return $this->amount_bu_vo;
    }
    public function getGasRemainAmount($format = false) {
        if($format){
            return ActiveRecord::formatCurrency($this->gas_remain_amount);
        }
        return $this->gas_remain_amount;
    }
    public function getAppPromotion($format = false) {
        if($format){
            return ActiveRecord::formatCurrency($this->promotion_amount);
        }
        return $this->promotion_amount;
    }
    public function getAmountDiscount($format = false) {
        if($format){
            return ActiveRecord::formatCurrency($this->amount_discount);
        }
        return $this->amount_discount;
    }
    public function getV1DiscountAmount($format = false) {
        if($format){
            return ActiveRecord::formatCurrency($this->v1_discount_amount);
        }
        return $this->v1_discount_amount;
    }
    
    /**
     * @Author: DungNT Nov 26, 2016
     * @Todo: lấy chiết khấu mặc định hoặc được nhập vào
     */
    public function getDiscountNormal($format = false) {
        if($this->amount_discount > 1){
            return $this->getAmountDiscount($format);
        }
        $amount = $this->qty_discount*$this->getPromotionDiscount();
        if($format){
            return ActiveRecord::formatCurrency($amount);
        }
        return $amount;
    }
    public function getPromotionCodeNo() {
        $res = ''; $goldTimeText = '';
        if($this->v1_discount_id == Forecast::TYPE_GOLD_TIME){
            $goldTimeText = 'giờ vàng';
        }
        
        if(!empty($this->promotion_id)){
            $mPromotion = $this->rPromotion;
            if($mPromotion){
                $res .= '<br><b>'.$mPromotion->code_no.': '.$this->getAppPromotion(true).'</b>';
            }
        }
        if($this->v1_discount_amount > 0){
            $res .= "<br><b>Giảm giá $goldTimeText: {$this->getV1DiscountAmount(true)}</b>";
        }
        return $res;
    }
    public function getSourceText() {
        $aSource = $this->getArraySource();
        return isset($aSource[$this->source]) ? '<br><br><b>'.$aSource[$this->source].'</b>' : '';
    }
    public function getDeliveryTimer() {
        if(empty($this->delivery_timer) || $this->delivery_timer == '0000-00-00 00:00:00'){
            return '';
        }
        return MyFormat::dateConverYmdToDmy($this->delivery_timer, 'H:i d/m');
    }
    public function getDeliveryTimerGrid() {
        $text = $this->getDeliveryTimer();
        if(!empty($text)){
            return 'Hẹn giao: '.$text;
        }
        return '';
    }
    

    /**
     * @Author: DungNT Dec 06, 2016
     * @todo: check for update app
     */
    public function canUpdateApp() {
//        if($this->source != Sell::SOURCE_APP){
//            return false;
//        }// Dec 10, 2016 cứ để cho update back lại TransactionHistory ở cả PMBH update + web UPdate for test performance
        return true;
    }

    /**
     * @Author: DungNT Dec 06, 2016
     * @Todo: xử lý cho admin dc cập nhật thẻ kho HGD khi update trên giao diện
     */
    public function adminUpdateStoreCard() {
        $cRole  = MyFormat::getCurrentRoleId();
        $cUid   = MyFormat::getCurrentUid();
//        if( ($cRole == ROLE_ADMIN && Yii::app()->params['EnableUpdateSell'] == 'yes') || 
        if( ($cRole == ROLE_ADMIN && $this->is_auto_gen_storecard) || 
              ( in_array($cUid, GasConst::getArrUidUpdateAll()) && $this->is_auto_gen_storecard )
            ) {// add Now 07, 2016
            $this->autoSaveStoreCardHgd();// May 17, 2017 Cho nguyệt BT2 cập nhật thẻ kho hộ GĐ
        }
    }
    
    /**
     * @Author: DungNT Nov 26, 2016
     * @Todo: cập nhật trạng thái + KTBH + giao nhận vào transaction History của customer
     */
    public function updateStatusTransaction() {
//        if(!$this->canUpdateApp()){
//            return ;
//        }
        $mTransactionHistory = TransactionHistory::model()->findByPk($this->transaction_history_id);
        if(is_null($mTransactionHistory)){
            return ;
        }
        $this->modelMonitorOld = clone $mTransactionHistory;
        if($this->mTransactionHistory){// Jul 21, 2017 xử lý update back pttt_code nhập từ Web sang model TransactionHistory
            $mTransactionHistory->pttt_code = $this->pttt_code;
        }

        $aUid = [];
        $mTransactionHistory->saveDataFromSell($this);
        if(empty($this->employee_maintain_id) && !empty($this->agent_id)){
            // nếu không nhập giao nhận thì notify cho all giao nhận của đại lý, nếu có nhập thì chỉ notify cho 1 GN thôi
            $aUid = $mTransactionHistory->getListEmployeeMaintainOfAgent();
            // chỗ này có thể gửi cho cả CCS + nhân viên Giao Nhận
        }elseif(!empty ($this->employee_maintain_id)){
            // chỗ này là đã cập nhật xong cho model Sell, mình không xác định được có đổi giao nhận hay không, nếu mà notify cho KH thì có bị trùng lặp không?
            $aUid[] = $this->employee_maintain_id;
        }
        
        if($this->status == Sell::STATUS_NEW){// Add Dec1917, chỉ notify ở status new
            $mTransactionHistory->notifyPhoneAppEmployee($aUid);
            $mTransactionHistory->notifyHighPrice();
            if($this->isChangeNewEmployee){
                $mTransactionHistory->notifyPhoneAppCustomer();
            }
        }
    }

    /**
     * @Author: DungNT Dec 10, 2016 - call from web CallCenter create
     * @Todo: tự động tạo TransactionHistory với những bán hàng từ PMBH để theo dõi chung cho giao nhận
     * Những cái tạo từ App Gas24h thì sẽ không có makeTransactionHistory
     */
    public function makeTransactionHistory() {
        $mTransactionHistory = new TransactionHistory();
        $mCustomer = $this->rCustomer;
        if(is_null($mCustomer) || empty($this->id)){
            return ;
        }
        /* Aug1619 DungNT fix đơn app chỉ tạo 1 notify, ko tạo các notify confirm
         * đã move xuống đoạn if($this->source == Sell::SOURCE_APP
         */
        //  [TASKTIMER]
        if($this->source == Sell::SOURCE_APP || $this->is_timer == Forecast::TYPE_TIMER){
            $mTransactionHistory = $this->rTransactionHistory;
            $mTransactionHistory->notifyOrderNotConfirm();
            return ;// có xử lý đi tiếp ko???
        }
        
        $this->makeTransactionHistoryMapField($mTransactionHistory, $mCustomer);
        $mTransactionHistory->save();
        if(is_null($mTransactionHistory->id) || empty($mTransactionHistory->id)){
            throw new Exception('Đơn hàng không thành công, vui lòng tạo lại đơn khác, SellFailed');
        }
        // update back transaction_history_id
        $this->transaction_history_id = $mTransactionHistory->id;
        $criteria = new CDbCriteria();
        $criteria->compare('id', $this->id);
        $aUpdate = array('transaction_history_id' => $mTransactionHistory->id);
        Sell::model()->updateAll($aUpdate, $criteria);

        $mTransactionHistory->notifyOrderNotConfirm();
        // Sep1618 nên để notify ở dưới cùng, vì server notify có thể bị stop, sẽ văng lỗi
        $aUid   = $mTransactionHistory->getListEmployeeMaintainOfAgent();
        if(!empty($this->employee_maintain_id)){// nếu đã gắn giao nhận rồi thì notify cho 1 GN thôi
            $aUid   = array($this->employee_maintain_id);
            $this->isChangeNewEmployee = true;
        }
        $mTransactionHistory->notifyPhoneAppEmployee($aUid, '');
        if($this->isChangeNewEmployee){
            $mTransactionHistory->notifyPhoneAppCustomer();
        }
        $mTransactionHistory->alertAgentKhoanCreateNew();
        $mTransactionHistory->notifyHighPrice();
        // Dec 29, 2016 
    }
    
    public function makeTransactionHistoryMapField(&$mTransactionHistory, $mCustomer) {
        $mTransactionHistory->sell_id                   = $this->id;
        $mTransactionHistory->source                    = $this->source;
        $mTransactionHistory->employee_accounting_id    = $this->uid_login;
        $mTransactionHistory->employee_maintain_id      = $this->employee_maintain_id;
        $mTransactionHistory->customer_id               = $this->customer_id;
        $mTransactionHistory->type_customer             = $this->type_customer;
        $mTransactionHistory->status_read               = Transaction::STATUS_READ_CONFIRM;
        $mTransactionHistory->first_name                = $mCustomer->first_name;
        
        $mTransactionHistory->agent_id          = $this->agent_id;
        $mTransactionHistory->phone             = !empty($this->phone) ? $this->phone : $mCustomer->phone;
        $mTransactionHistory->phone             = UsersPhone::removeLeftZero($mTransactionHistory->phone);
        $mTransactionHistory->province_id       = $this->province_id;
        $mTransactionHistory->total             = $this->total;
        $mTransactionHistory->grand_total       = $this->grand_total;
        $mTransactionHistory->ip_address        = MyFormat::getIpUser();
        
        $mTransactionHistory->note              = $this->note;
        $mTransactionHistory->created_date      = date('Y-m-d H:i:s');
        
        $mTransactionHistory->json_detail       = $this->transactionEncodeJsonDetail();
        
        $mTransactionHistory->address           = $mCustomer->address;
        $mTransactionHistory->created_date_only = date('Y-m-d');
        $mTransactionHistory->qty_discount      = $this->qty_discount;
        $mTransactionHistory->amount_discount   = $this->amount_discount;
        $mTransactionHistory->amount_bu_vo      = $this->amount_bu_vo;
        
        $mTransactionHistory->order_type        = $this->order_type;
        $mTransactionHistory->type_amount       = $this->type_amount;
        $mTransactionHistory->high_price        = $this->high_price;
        $mTransactionHistory->promotion_amount  = $this->promotion_amount;
        $mTransactionHistory->delivery_timer    = $this->delivery_timer;
        
        if(!empty($this->employee_maintain_id)){
            $mTransactionHistory->action_type = Transaction::EMPLOYEE_NHAN_GIAO_HANG;
        }
    }

    /**
     * @Author: DungNT Dec 13, 2016
     * @Todo: set status for this record and detail
     */
    public function setStatus($status, $note) {
        $aUpdate = array('status', 'status_cancel', 'complete_time');
        $this->status    = $status;
        if(!empty($note)){
            $this->note = $note. ' -- '.$this->note;
            $aUpdate[] = 'note';
        }
        if(!is_null($this->mTransaction)){
            $this->status_cancel    = $this->mTransaction->status_cancel;
        }
        $this->update($aUpdate);
        
        $criteria = new CDbCriteria();
        $criteria->compare('sell_id', $this->id);
        $aUpdate = array('status' => $this->status);
        SellDetail::model()->updateAll($aUpdate, $criteria);
    }
    /**
     * @Author: DungNT Dec 14, 2016
     * @Todo: Khi NVGN Pick or Cancel Pick => set employee_maintain_id for this record and detail
     */
    public function setEmployeeMaintainId($employee_maintain_id) {
        $this->employee_maintain_id    = $employee_maintain_id;
        $this->update(array('employee_maintain_id'));
        $criteria = new CDbCriteria();
        $criteria->compare('sell_id', $this->id);
        $aUpdate = array('employee_maintain_id' => $this->employee_maintain_id);
        SellDetail::model()->updateAll($aUpdate, $criteria);
    }
    
    /**
     * @Author: DungNT Dec 14, 2016
     * @Todo: map data detail khi Transaction change
     * @Des: khi Employee change gas, quà, vỏ ở dưới App thì mỗi lần change sẽ phải tính toán lại tất cả,
     * mà phần tính toán nằm trong model Sell nên ta sẽ gắn lại detail sang model detail để tính toán lại 
     * rồi lưu vào TransactionHistory, có thể save luoon vào model Sell - vì save vào model Sell thì lại phải Save cả Detail.
     * Nhưng nếu save vào model Sell lúc này thì cũng không hợp lý lắm - có thể Employee sẽ change lung tung rồi hủy đơn hàng thì ntn, Sẽ để lúc thu tiền sẽ Save sang model Sell sau
     */
    public function mapTransactionChange($q, &$mTransactionHistory) {
        $mAppCache      = new AppCache();
        $aModelGas12    = $mAppCache->getModelGas12Kg();
        $this->getInventoryAgent();
        $this->setFirstOrder();
//        $aDataCache     = $mAppCache->getPriceHgd(AppCache::PRICE_HGD);
        $aDataCache     = $this->getPriceOfMonthSell();
        $priceHgd       = MyFunctionCustom::getHgdPriceSetup($aDataCache, $mTransactionHistory->agent_id);
        // Dec 16, 2016 xử lý Giống PMBH cho nhập Loại chiết khấu + loại BH: Bình thường, Bộ bình, Gas + Vỏ, Thế chân
        $mTransactionHistory->removeSpjCodeOld();
        $qtyVo = $qtyGas = 0;
        $this->order_type       = $q->order_type;
        $this->type_amount      = 0;
        $this->first_name       = $mTransactionHistory->first_name;
        if($this->order_type != Sell::ORDER_TYPE_NORMAL){// Apr 28, 2017 cho phep GN change loai ban hang duoi APP
            $this->type_amount  = $this->getPriceVoBinh();
        }
        $this->amount_discount  = $q->amount_discount;
        if($this->discount_type == Sell::DISCOUNT_DEFAULT){
            $this->amount_discount  = 0;
        }elseif($q->discount_type == Sell::DISCOUNT_CHANGE && empty($this->amount_discount)){
            $this->amount_discount  = Sell::DISCOUNT_CHANGE_ZERO;
        }
        $this->mapTransactionDetail($q, $priceHgd, $aModelGas12, $qtyVo, $qtyGas);

        if($qtyVo != $qtyGas && $this->order_type == Sell::ORDER_TYPE_NORMAL && $q->action_type == Transaction::EMPLOYEE_COMPLETE){
            throw new Exception('Bạn chưa nhập đủ hoặc dư vỏ, không thể hoàn thành đơn hàng', SpjError::SELL001);
        }
        $this->support_id               = isset($q->support_id) ? $q->support_id : 0;
        $mTransactionHistory->pttt_code = isset($q->pttt_code) ? strtoupper(trim($q->pttt_code)) : '';
        $this->pttt_code                = $mTransactionHistory->pttt_code;
        $this->complete_time            = $mTransactionHistory->complete_time;
        
        $this->checkDiscount();
        $this->putDiscountToDetail();
        $mTransactionHistory->mSellToCheckSpjCode = $this;// Feb1118 check seri + vỏ cho PTTT code
        $mTransactionHistory->checkSpjCode();
        $this->update();
        $this->runUpdateStatusTransaction = false;
        $this->webSaveDetail();
        /** vì ở webSaveDetail có hàm $this->updateStatusTransaction(); đã chạy hàm saveDataFromSell rồi, nên không cần gọi hàm $mTransactionHistory->saveDataFromSell($this); bên dưới nữa
        * @note_important: hàm saveDataFromSell trong webSaveDetail không build dc cái $this->aDetailTransaction[] 
         * nên bắt buộc phải chạy hàm dưới
         */
        $mTransactionHistory->saveDataFromSell($this);
    }
    
    /** @Author: DungNT Oct 19, 2017
     *  @Todo: map post detail from app to Sell
     */
    public function mapTransactionDetail($q, $priceHgd, $aModelGas12, &$qtyVo, &$qtyGas) {
        $this->setDiscountGas24h();
        $mAppCache = new AppCache();
        $aDetailGas = []; $emptyOrder = true; $nameGasEmptyPrice = ''; $mAppOrder = new GasAppOrder(); $mMaterialsType = new GasMaterialsType();
        $aMaterial     = $mAppCache->getListdata('GasMaterials', AppCache::LISTDATA_MATERIAL);
        
        foreach($q->order_detail as $objDetail){
            $mDetail = new SellDetail('WindowCreate');
            $mDetail->gas_remain = 0.00; $mDetail->gas_remain_amount = 0;
            $mDetail->kg_empty = 0.00; $mDetail->kg_has_gas = 0.00;
            $this->mapSameInfoDetail($mDetail);
            $mDetail->materials_type_id = $objDetail->materials_type_id;
            $mDetail->materials_id      = $objDetail->materials_id;
            $mDetail->qty               = !empty($objDetail->qty) ? $objDetail->qty : 1;
            $mDetail->price_root        = isset($priceHgd[$mDetail->materials_id]) ? $priceHgd[$mDetail->materials_id] : 0;
            $mDetail->price             = $mDetail->price_root;
            $materialName = isset($aMaterial[$mDetail->materials_id]) ? $aMaterial[$mDetail->materials_id] : '';
            if($mDetail->materials_type_id != GasMaterialsType::MATERIAL_BINH_12KG){// Dec2917 tính lại giá cho dây inox nếu được tặng 0đ
                $mDetail->price         = isset($objDetail->price) ? $objDetail->price : $mDetail->price_root;
            }
            if(!$this->isCustomerBookOrder && $this->isOutOfStock($mDetail->qty, $mDetail->materials_type_id, $mDetail->materials_id)){
                throw new Exception("Kho của bạn đã hết $materialName. không thể hoàn thành đơn hàng", SpjError::SELL001);
            }
            if(!$this->isCustomerBookOrder && $this->source == Sell::SOURCE_APP && $mDetail->materials_type_id == GasMaterialsType::MATERIAL_TYPE_PROMOTION){
                throw new Exception('Không nhập hàng khuyến mãi cho đơn app', SpjError::SELL001);
            }

            if($this->source != Sell::SOURCE_APP && $mDetail->materials_type_id == GasMaterialsType::MATERIAL_BINH_12KG){
                $mDetail->price     = $mDetail->price_root; 
            // Now1717 tính lại giá đơn hàng khi GN hoàn thành hoặc save ở app, tránh sửa giá ở client - có thể bị sai nếu đơn hàng được giảm giá như đơn đặt từ App Gas24h
            }
            $mDetail->agent_id      = $this->agent_id;
            $this->calcPriceItemGasApp($mDetail);

            $mDetail->amount                = $mDetail->qty*$mDetail->price;
            $mDetail->calcPriceVanDay($priceHgd);
            $mDetail->materials_parent_id   = isset($aModelGas12[$mDetail->materials_id]) ? $aModelGas12[$mDetail->materials_id]['parent_id'] : 0;// Now 20, 2016 for fix setup bù vỏ
            $mDetail->seri                  = isset($objDetail->seri) ? trim($objDetail->seri) : '';
            if($mDetail->materials_type_id == GasMaterialsType::MATERIAL_VO_12){
                $qtyVo += $mDetail->qty;
                $mDetail->kg_empty      = isset($objDetail->kg_empty) ? $mAppOrder->formatInputGasRemain($objDetail->kg_empty) : 0.00;
                $mDetail->kg_has_gas    = isset($objDetail->kg_has_gas) ? $mAppOrder->formatInputGasRemain($objDetail->kg_has_gas) : 0.00;
                $mDetail->gas_remain    = $mDetail->kg_has_gas - $mDetail->kg_empty;
                $this->gas_remain       = $mDetail->gas_remain;
                $this->kg_empty         = $mDetail->kg_empty;
                $this->kg_has_gas       = $mDetail->kg_has_gas;
                $mDetail->validateData($this);
            }
            if($mDetail->materials_type_id == GasMaterialsType::MATERIAL_BINH_12KG){
                $qtyGas += $mDetail->qty;
            }
            if($mDetail->price_root < 1 && !in_array($mDetail->materials_type_id, $mMaterialsType->getArrayTypeEmptyPrice())){
                $nameGasEmptyPrice = $materialName; // Apr1319 mail cho KTKV update gia B12 HGD khi vat tu co gia = 0
            }
            if(in_array($mDetail->materials_type_id, GasMaterialsType::$ARR_GAS_VO_45KG)){
                throw new Exception('Đơn hàng nhập sai Gas hoặc Vỏ, vui lòng kiểm tra lại thông tin');
            }
            $emptyOrder = false;
            $mDetail->validate();
            if(!$mDetail->hasErrors()){
//                $this->aDetail[] = $mDetail; DungNT Sep2919 fix bug khi PVKH sửa đơn ở app ios Gas Service, đưa vỏ lên trên row Gas 
                if(in_array($mDetail->materials_type_id, GasMaterialsType::$SETUP_PRICE_GAS_12) ){
                    array_unshift($aDetailGas , $mDetail);
                }else{// luôn đưa gas lên item first của array
                    $aDetailGas[] = $mDetail;
                }
            }
        }
        $this->aDetail = $aDetailGas;// DungNT Sep2919
        if($emptyOrder){
            throw new Exception('Đơn hàng chưa nhập Gas, vui lòng nhập đủ thông tin');
        }
        if(!empty($nameGasEmptyPrice)){ 
            $mSendEmail = new SendEmail();
            $mSendEmail->alertSetupB12Hgd($this, $nameGasEmptyPrice);
        }
    }
    
    /** @Author: DungNT Now 27, 2018
     *  @Todo: get agent ko giảm mặc định trên app, bình thường TP HCM là 60k, Tỉnh là 20k
     **/
    public function getAgentNotDiscount() {
        return [
//            CodePartner::AGENT_THANH_SON, // ĐLLK Thanh Sơn 1 -- close Feb1519
        ];
    }
    
    /** @Author: DungNT Mar 27, 2018 - rewrite price app
     *  @Todo: tính giá app của từng khu vực cho item gas, Giảm TP HCM là 60k, Tỉnh là 20k
     *  @param: $mDetail is model SellDetail
     **/
    public function calcPriceItemGasApp(&$mDetail) {
        if(in_array($this->agent_id, $this->getAgentNotDiscount())){
            return ;
        }
        $aAgentCheck    = $this->getListAgentRunPttt();// Dec1318 su dung chung ham de tinh $date_expiry
        $mAppPromotion  = new AppPromotion(); $date_expiry = date('Y-m-d');
        if($this->source != Sell::SOURCE_APP || $mDetail->materials_type_id != GasMaterialsType::MATERIAL_BINH_12KG){
            return ;
        }
        // đã cập nhật $mDetail->agent_id chỉ cần viết đk check nữa
        $aProvinceDiscount  = [GasProvince::TP_HCM];
        $aAgentLikeHcm      = [// Mar0619 Phuc keu remove - ko giam giong tp hcm nua
//                109, // Đại lý Lái Thiêu
//            1284630, // Đại Lý Bửu Long Sep1318
//            758528,// Đại Lý Uyên Hưng - Oct1718
////                123, // Đại lý Trảng Dài -- Close Jan0219
//            110, // Đại lý Thủ Dầu Một - Oct1818
//            971240,// Đại Lý Phú Mỹ - Oct2618
//            268833,// Đại Lý Phú Hòa 
//            118, // Đại lý An Thạnh - Now1218
//            939720, // Đại Lý Bình Chuẩn
//            375037, // Đại lý Thái Hòa  - start 2019-01-12 - end sau 4 thang- end 2019-05-12
//            115,// Start '2019-05-01', // Đại lý Dĩ An - end sau 4 thang - end 2019-07-01
        ];

        if(array_key_exists($this->agent_id, $aAgentCheck)){
            $date_expiry = $aAgentCheck[$this->agent_id];// Dec1318
        }

        if(in_array($this->province_id, $aProvinceDiscount) 
//                    || in_array($this->agent_id, $aAgentLikeHcm) // Close Dec1318
//                    || in_array($this->promotion_id, $mAppPromotion->getIdBhxtLevel2())
//            || (in_array($this->agent_id, $aAgentLikeHcm) && array_key_exists($this->agent_id, $aAgentCheck) && MyFormat::compareTwoDate($date_expiry, date('Y-m-d')) )// Jun319 đoạn này check cho Phúc giảm 100k giống HCM - đã dừng chwogn trình này nên bỏ ko check nữa
        ){// Tạm close đến ngày 01/04/18 sẽ open khi có thông báo
            $mDetail->price = $mDetail->price_root - $this->discountGas24h;

//                if(in_array($this->promotion_id, $mAppPromotion->getIdBhxtLevel2()) && $this->province_id != GasProvince::TP_HCM){
//                    Logger::WriteLog("DEBUG apply BH XUAN THANH Sell ID: $this->id - $this->code_no");
//                }// Now1718 Close BHXT tự động lại, sẽ để cho Callcenter tự nhập chiết khấu

        }else{
            $mDetail->price = $mDetail->price_root - 20000;
        }
    }
    
    /**
     * @Author: DungNT Feb 11, 2017
     * @Todo: make record socket notify. KTBH tạo đơn hàng thì tạo sẵn notify 5 phút nếu GN không xác nhận đơn hàng
     */
    public function makeSocketNotify() {
        if(!in_array($this->agent_id, UsersExtend::getAgentRunAppGN()) || !GasCheck::isServerLive()){
            return ;// open when live
        }
        $mCustomer  = $this->rCustomer;
        $mAgent     = $this->rAgent;
        if(empty($mCustomer)){
            return ;
        }
//            public static function addMessage($sender_id, $userId, $code, $message, $json)
        $message = "Chưa xác nhận ĐH: {$mCustomer->getFullName()} -- $mCustomer->phone. $this->address";
        $json = [];
        $json['agent_name']     = $mAgent->getFullName();
        $json['customer_name']  = $mCustomer->getFullName();
        $json['type_customer']  = $mCustomer->getTypeCustomerText();
        $needMore = array('agent_id'=>$mAgent->id);
        GasSocketNotify::addMessage($this->id, GasConst::UID_ADMIN, $this->uid_login, GasSocketNotify::CODE_SELL_ALERT_KTBH, $message, $json, $needMore);
    }
    
    /**
     * @Author: DungNT Mar 16, 2017
     * @Todo: <Cập nhật lại sell id to Call cuộc gọi tương ứng, luôn phải run sau mỗi lần tạo Sell> 
     */
    public function updateIdToCall() {
        if(!isset($_GET['call_uuid']) || empty($_GET['call_uuid'])){
            return ;
        }
        $mCall = new Call();
        $mCall->call_uuid = $_GET['call_uuid'];
        $mCall = $mCall->getByCallUuid();
        if($mCall){// update cho trường hợp đã cúp máy (hang_up + miss call)
            $mCall->updateSellInfo($this);
            $this->call_id          = $mCall->id;
            $this->call_end_time    = strtotime($mCall->end_time);
            $this->update(['call_id', 'call_end_time']);
        }else{
            $criteria = new CDbCriteria();// client sẽ tạo link qua call_uuid của DialAnswer 
            $criteria->compare('t.call_uuid', $_GET['call_uuid']);
            $criteria->addCondition('t.call_status=' . Call::STATUS_DIAL_ANSWER);
            $mCallTemp = CallTemp::model()->find($criteria);
            if($mCallTemp){
                $mCallTemp->updateSellInfo($this);
            }
        }
    }
    
    /** @Author: DungNT Jul 20, 2017
     * @Todo: validate PTTT code nếu được nhập ở web update
     */
    public function validatePtttCode() {
        if(!empty($this->oldCode) || empty($this->pttt_code)){
            return ;
        }
        $mTransactionHistory = TransactionHistory::model()->findByPk($this->transaction_history_id);
        if(is_null($mTransactionHistory)){
            return ;
        }
        $mTransactionHistory->pttt_code             = $this->pttt_code;
        $mTransactionHistory->mSellToCheckSpjCode   = $this;
        $mTransactionHistory->checkSpjCode();
        $this->mTransactionHistory = $mTransactionHistory;
    }
    
    /** @Author: DungNT Jul 20, 2017
     * @Todo: update PTTT code sang SpjCode
     */
    public function updateSpjCode() {
        if(is_null($this->mTransactionHistory)){
            return ;
        }
        $this->mTransactionHistory->updateSellIdToSpjCode($this);
    }
    
    public function getEventLog() {
        $mTransactionEvent = new TransactionEvent();
        $mTransactionEvent->transaction_history_id = $this->transaction_history_id;
        $mTransactionEvent->type = TransactionEvent::TYPE_SELL;
        $aData = $mTransactionEvent->getToday();
        $this->mEventComplete = $mTransactionEvent->mEventComplete;
        return $aData;
    }
    
    public function saveEvent($action_type, $user_id) {
        $mEvent = new TransactionEvent();
        $mEvent->type                       = TransactionEvent::TYPE_SELL;
        $mEvent->transaction_history_id     = $this->transaction_history_id;
        $mEvent->user_id                    = $user_id;
        $mEvent->action_type                = $action_type;
        $mEvent->google_map                 = '';
        if(!empty($this->modelMonitorOld)){// modelMonitorOld is model TransactionHistory before update
            $mEvent->json_data              = MyFormat::jsonEncode($this->modelMonitorOld->getAttributes());
        }
        $mEvent->save();
        $this->saveLogUpdate();
    }
    
    /** @Author: spj001 DungNT May 08, 2018
     *  @Todo: save log all function
     **/
    public function saveLogUpdate($type = '') {
        $mTransactionHistory = TransactionHistory::model()->findByPk($this->transaction_history_id);
        $model = new LogUpdate();
        if(!empty($this->modelMonitorOld)){
            $model->json        = MyFormat::jsonEncode($this->modelMonitorOld->getAttributes());
        }
        $model->json_after  = MyFormat::jsonEncode($mTransactionHistory->getAttributes());
        $model->type        = LogUpdate::TYPE_1_SELL;
        $model->agent_id    = $this->agent_id;
        $model->addRow();
    }
    
    /** @Author: DungNT Jan 03, 2018
     *  @Todo: nếu change agent thì xóa báo lỗi cũ đi, insert error cho agent mới vào
     **/
    public function checkChangeAgent() {
        if($this->mBeforeUpdate->agent_id == $this->agent_id){
            return ;
        }
        if($this->modelMonitorOld){// modelMonitorOld is model TransactionHistory before update
            $mTransactionHistory = $this->modelMonitorOld;
            $mTransactionHistory->deleteNotify(false);
            $mTransactionHistory->agent_id = $this->agent_id;// phải gắn agent mới cho model để tạo notify đúng
            $mTransactionHistory->notifyOrderNotConfirm();
        }
    }
    /** @Author: DungNT Now 22, 2017
     *  @Todo: count done order of customer
     **/
    public function countOrderDoneByCustomer() {
        return Sell::model()->countByAttributes(
            ['customer_id' => $this->customer_id, 'status' => Sell::STATUS_PAID] );
    }
        
    /** @Author: DungNT Sep 15, 2017
     *  @Todo: get text show on app to announce user time update thẻ kho HGD */
    public static function getAppTextUpdateHgd() {
        $lastTime       = Yii::app()->setting->getItem('CronUpdateSell');
        $nextTime       = MyFormat::addDays($lastTime, CronUpdate::CRON_MINUTES_HGD, '+', 'minutes', 'Y-m-d H:i:s');
        $lastTimeTemp   = explode(' ', $lastTime);
        $nextTimeTemp   = explode(' ', $nextTime);
        return 'Hệ thống cập nhật thẻ kho HGĐ lúc: '.$lastTimeTemp[1]. ' Lần cập nhật tiếp theo là: '. $nextTimeTemp[1];
    }
    
    /** @Author: Dec0817 Uit Nam window update query phpmyadmin
     *  Nam, 9:54 AM
        UPDATE `gas_users` as U SET last_purchase = (select max(created_date_only) FROM gas_sell as S WHERE U.id = S.customer_id AND S.status = 2) WHERE U.is_maintain IN (7,8,9,10,11)
    *   @param: $dateUpdate: 2018-03-29
     * Update last_purchase Bò Mối: 
     * UPDATE `gas_users` as U SET last_purchase = (select max(date_delivery) FROM gas_gas_store_card as S WHERE U.id = S.customer_id AND S.type_store_card = 2)
        where U.is_maintain IN (1,2,6)
     **/
    public function windowUpdateLastPurchase($dateUpdate = null){
        $tableNameUser  = 'gas_users';
        $tableNameOrder = 'gas_sell';
        $statusOrder    = Sell::STATUS_PAID;
        $sql='';
        $dateUpdateBigint = strtotime($dateUpdate);
        
        /** @important: chú ý phải có đoạn WHERE U.is_maintain IN (7,8,9,10,11)
         *  Nếu không hệ thống sẽ update null cho toàn bộ User = null nếu không có trong Sell
         * => Sai trầm trọng
         */
        if($dateUpdate == null){
            $sql    ="UPDATE `$tableNameUser` as U "
                    . "SET last_purchase = (select max(created_date_only) "
                    . "FROM $tableNameOrder as S "
                    . "WHERE U.id = S.customer_id AND "
                    . "S.status = $statusOrder ) WHERE U.is_maintain IN (7,8,9,10,11)";
        }else{
            $sql    ="UPDATE `$tableNameUser` as U "
                    . "SET last_purchase = '$dateUpdate' "
                    . "WHERE U.id in (select S.customer_id "
                                    . "FROM gas_sell as S "
                                    . "WHERE S.status = $statusOrder and "
//                                    . "S.created_date_only = '$dateUpdate' )";
                                    . "S.created_date_only_bigint = $dateUpdateBigint )";
        }
        Yii::app()->db->createCommand($sql)->execute();
    }
    
    /** @Author: DungNT Feb 23, 2018
     *  @Todo: kiểm tra xem gas dư có cho phép update không
     **/
    public function checkUpdateGasRemain() {
        $cRole = MyFormat::getCurrentRoleId();
        if($cRole == ROLE_ADMIN || $this->status == Sell::STATUS_NEW){
            return ;
        }
        $mGasRemain = new GasRemain();
        $mGasRemain->mSell = $this;
        $mGasRemain->loadHgdOld();
    }
    /** @Author: DungNT Feb 23, 2018
     *  @Todo: save vỏ & Gas dư HGĐ
     **/
    public function addGasRemain() {
        $mGasRemain = new GasRemain();
        $mGasRemain->mSell = $this;
        $mGasRemain->addGasRemainHgd();
    }
    /** @Author: DungNT Mar 11, 2018
     *  @Todo: count số order complete của 1 KH
     * tính cho các loại User sau:
     * 1. telesale
     * 2. PVKH + PTTT 1002 của a Đợt
     * 
     */
    public function getOrderLastestOfCustomer() {
        try {
        if(empty($this->customer_id)){
            return null;
        }
        $criteria = new CDbCriteria();
        $criteria->addCondition('t.customer_id=' . $this->customer_id);
        $criteria->addCondition('t.status=' . Sell::STATUS_PAID);
        if(!empty($this->id)){
            $criteria->addCondition('t.id<>' . $this->id);
        }
        $this->limitTelesaleCheck($criteria);
        $criteria->limit = 1;
        $criteria->order = 't.id DESC';
        return Sell::model()->find($criteria);
        } catch (Exception $exc) {
            throw new Exception('GO to getOrderLastestOfCustomer ->'.$exc->getMessage());
        }
    }
    
    /** @Author: DungNT Jul 19, 2018
     *  @param: $this->monthCheckTelesale  set cách tính BQV từ trước 6 tháng
     **/
    public function limitTelesaleCheck(&$criteria) {
        if(!$this->telesaleCheck){// telesale bắt đầu chạy từ 01/05/2018, sẽ thêm đk date này để tính đúng cho BQV KH
            return ;
        }
        $this->checkOrderTelesaleOfCustomer();
        
        $monthCheckTelesale = $this->monthCheckTelesale;
        if($this->province_id == GasProvince::TP_HCM){
            $monthCheckTelesale -=1;// Đổ dữ liệu thì 2 tháng cho gọi, Khi tính BQV thì TP HCM 3 tháng, tỉnh 4 tháng 
        }
        $mSale = Users::model()->findByPk($this->sale_id);
        $this->date_from_ymd    = MyFormat::modifyDays(date('Y-m-d'), $monthCheckTelesale, '-', 'month');
        $criteria->addCondition("t.created_date_only > '{$this->date_from_ymd}'");
    }
    
    /** @Author: DungNT Oct 25, 2018 
     *  @Todo: get order dc tinh cho telesale cua KH trc day
     *  nếu có BQV đc tính trước đây rồi thì không tính nữa
     * anh xem giúp em S18TS6P4J sdt  0978540201 BQV thứ 2 sao vẫn tính cho Phương ạ ?
     **/
    public function checkOrderTelesaleOfCustomer() {
        $criteria = new CDbCriteria();
        $criteria->addCondition('t.customer_id=' . $this->customer_id);
        $criteria->addCondition('t.status=' . Sell::STATUS_PAID);
        $criteria->addCondition('t.sale_id > 0 AND t.first_order=' . Sell::IS_FIRST_ORDER);
        if(!empty($this->id)){
            $criteria->addCondition('t.id<>' . $this->id);
        }
        $model = Sell::model()->find($criteria);
        if(!is_null($model)){// break luon neu co tinh BQV roi
            throw new Exception("KH {$this->customer_id}  Phone: $this->phone đã tính cho telesale 1 lần rồi");
        }
    }
    
    public function limitTelesaleCheckBackup(&$criteria) {
        if(!$this->telesaleCheck){// telesale bắt đầu chạy từ 01/05/2018, sẽ thêm đk date này để tính đúng cho BQV KH
            return ;
        }
        $mSale = Users::model()->findByPk($this->sale_id);
        $monthCheck = 6;// Sep1518 set cách tính BQV từ trước 6 tháng
        $this->date_from_ymd    = MyFormat::modifyDays(date('Y-m-d'), $monthCheck, '-', 'month');
        if(!empty($mSale) && $mSale->role_id == ROLE_TELESALE){
//            $dateLimit = '2018-04-01';// Close Sep1518 - Oct0818 mở lại cách tính này, không rõ trc đây sao đóng lại, nếu tính 3 tháng thì sẽ bị tính nhiều lần quay về cho 1 KH vd KH cua Ven: 0934154825
//            if($this->type_customer == UsersExtend::STORE_CARD_HGD_APP && $this->agent_id == 768408){// close Oct1618 // Aug318 nếu là KH App Bình Thạnh 3 thì check gọi lại trên 30s, nếu ko có callout thì ko tính
            $mCallout = $this->getRecordCallout();
//            if(!empty($mCallout)){// close Oct1618
//                Logger::WriteLog("KH app BT 3 have callout $this->customer_id");
//                $this->date_from_ymd = '2018-08-01';// KH App Bình Thạnh 3 gọi lại
//            }// Close Oct1718 do KH app có thể không có CallOut
            if(empty($mCallout)){
                throw new Exception('Not have callout Callcenter Oct1618');
            }
        }
//        elseif(!empty($mSale) && $mSale->role_id == ROLE_CALL_CENTER){
//            $mCallout = $this->getRecordCallout();
//            if(empty($mCallout)){
//                throw new Exception('Not have callout Callcenter');
//            }
//        }
        $criteria->addCondition("t.created_date_only > '{$this->date_from_ymd}'");
    }
    
    /** @Author: DungNT Jul 03, 2018
     *  @Todo: get model callout of customer
     **/
    public function getRecordCallout() {
        $mFollowCustomer = new FollowCustomer();
        $mFollowCustomer->customer_id   = $this->customer_id;
        $mFollowCustomer->employee_id   = $this->sale_id;
        $mFollowCustomer->date_from     = $this->date_from_ymd;
        return $mFollowCustomer->getCalloutValid();
    }
    
    /** @Author: DungNT Mar 11, 2018
     *  @Todo: check order lần 2 của 1 KH, nếu dưới 2 tháng thì không hợp lệ
     */
    public function checkOrderPtttCode() {
        $mSellLastest = $this->getOrderLastestOfCustomer();
        if(empty($mSellLastest) || empty($mSellLastest->pttt_code)){
            return ;
        }
        $monthCheck = 2;
        $dateCheck = MyFormat::modifyDays(date('Y-m-d'), $monthCheck, '-', 'month');
        if(MyFormat::compareTwoDate($mSellLastest->created_date_only, $dateCheck)){
//            GasScheduleEmail::alertLockPtttCode($this);
            $date = MyFormat::dateConverYmdToDmy($mSellLastest->created_date_only);
            throw new Exception("Khách hàng này đã mua hàng trong $monthCheck tháng gần đây, ngày mua hàng: $date. Hệ thống không thể nhập code PTTT");
        }
    }
    
    /** @Author: DungNT May 09, 2018
     *  @Todo: check code PTTT chỉ đc phép nhập trong 2 tháng chạy PTTT, nếu ngoài 2 tháng thì không tính
     */
    public function checkExpiredDatePttt() {
        return ;// Jun1319 -- ko check đk này nữa
        $aAgentCheck = $this->getListAgentRunPttt();
        if(array_key_exists($this->agent_id, $aAgentCheck)){
            $date_expiry = $aAgentCheck[$this->agent_id];
            if(MyFormat::compareTwoDate(date('Y-m-d'), $date_expiry)){
                throw new Exception('Chương trình PTTT ở đại lý của bạn đã kết thúc ngày '.MyFormat::dateConverYmdToDmy($date_expiry).'. Hệ thống không thể nhập code PTTT');
            }
        }
    }
    
    /** @Author: DungNT Mar 29, 2018
     *  @Todo: get số phút hoàn thành vượt quy định
     **/
    public function getTimeOver() {
        $mTransactionHistory = new TransactionHistory();
        $timeHgd = MonitorUpdate::TIME_COMPLETE_HGD;
        if($this->source == Sell::SOURCE_APP){
            $timeHgd = MonitorUpdate::TIME_COMPLETE_HGD_APP;
        }
        if(!$mTransactionHistory->isOverTime($this->created_date, $timeHgd, $this->complete_time)){
            return '';
        }
        return $this->getTimeOverOnlyNumber();
    }
    
    /** @Author: DungNT May 19, 2018
     *  @Todo: get số phút hoàn thành đơn hàng
     **/
    public function getTimeOverOnlyNumber() {
        $datetimeCheck = $this->created_date;
        if(!empty($this->delivery_timer) && $this->delivery_timer != '0000-00-00 00:00:00'){
            $datetimeCheck = $this->delivery_timer;
        }
        if(MyFormat::compareTwoDate($datetimeCheck, $this->complete_time)){
            return 0;// hoàn thành trước thời gian hẹn giờ nếu có
        }
        return MyFormat::getMinuteTwoDate($datetimeCheck, $this->complete_time);
    }
    
    
    public function getTimeOverWebView() {
        if($this->status != Sell::STATUS_PAID){
            return '';
        }
        $minutes = $this->getTimeOver();
        if(empty($minutes)){
            return '';
        }
        return "($minutes phút)";
    }

    /** @Author: DungNT Apr 03, 2018
     *  @Todo: get tồn kho ở cache của Agent, check ko cho xuất kho âm
     **/
    public function getInventoryAgent() {
        $mAppCache              = new AppCache();
        $aInventory             = $mAppCache->getCacheInventoryAllAgent(AppCache::APP_AGENT_INVENTORY);
        $this->aInventoryAgent  = isset($aInventory[$this->agent_id]) ? $aInventory[$this->agent_id] : [];
    }
    /** @Author: DungNT Apr 03, 2018
     *  @Todo: không cho xuất kho nếu kho hết hàng
     **/
    public function isOutOfStock($qty, $materials_type_id, $materials_id) {
        if(!GasCheck::isServerLive()){
            return false;
        }
        $aMaterialsTypeNotCheck = [GasMaterialsType::MATERIAL_VO_50, GasMaterialsType::MATERIAL_VO_45, GasMaterialsType::MATERIAL_VO_12, GasMaterialsType::MATERIAL_VO_6];
        if(in_array($materials_type_id, $aMaterialsTypeNotCheck)){
            return false;
        }
        if(!isset($this->aInventoryAgent[$materials_type_id][$materials_id])){
            return true;
        }
        
        $qtyInventory = $this->aInventoryAgent[$materials_type_id][$materials_id];
        
        if( $qtyInventory > 0 && $qtyInventory >= $qty ){
            // chỗ này xử lý cho đặt hàng của xe tải nữa, set vào 1 biến tạm để lấy ra kiểm tra với những đăt hàng xe  tải
            return false;
        }
        return true;
    }
    
    /** @Author: DungNT Apr 13, 2018
     *  @Todo: add vỏ tự động cho KH cũ khi tạo mới
     **/
    public function autoAddVoCustomerOld() {
        /* 1. tìm đơn hàng mới nhất của KH
         * 2. tìm vỏ của đơn hàng
         * 3. map vào biến post, không copy nguyên row
         * 3. đơn hàng bộ bình, bán gas vỏ không tự thêm vỏ vào
         */
        if(!$this->isNewRecord || $this->order_type != Sell::ORDER_TYPE_NORMAL){
            return ;
        }
        if(!$this->orderHaveGas()){
            return ;
        }

        $mDetailVo = $this->autoAddVoGetData();
        if(empty($mDetailVo)){
            return ;
        }
        $this->initPostVo($mDetailVo);
    }
    
    /** @Author: DungNT Aug 18, 2018
     *  @Todo: function only Build data add vo to order
     **/
    public function autoAddVoGetData() {
        $mSell = $this->getOrderLastestOfCustomer();
        if(empty($mSell)){
            return null;
        }
        $mDetailVo = $mSell->getRowVo();
        return $mDetailVo;
    }
    
    /** @Author: DungNT Apr 13, 2018
     *  @Todo: get row vỏ của 1 order
     **/
    public function getRowVo() {
        $mAppCache = new AppCache();
        $aGasVo = $mAppCache->getListdataGasVo();
        $res = null;
        foreach($this->rDetail as $mDetail){
            if($mDetail->materials_type_id == GasMaterialsType::MATERIAL_BINH_12KG){
                if(array_key_exists($mDetail->materials_id, $aGasVo)){
                    $mDetail->materials_id = $aGasVo[$mDetail->materials_id];// chuyển gas sang id vỏ
                    $mMaterial              = $mDetail->rMaterial;
                    if($mMaterial && $mMaterial->status == STATUS_ACTIVE){
                        $res = $mDetail;
                    }
                }
            }
        }
        return $res;
    }
    
    /** @Author: DungNT Apr 13, 2018
     *  @Todo: khởi tạo biến post vỏ, thêm tự động vào đơn hàng
     **/
    public function initPostVo($mDetailVo) {
        $_POST['materials_id'][]    = $mDetailVo->materials_id;
        $_POST['materials_qty'][]   = 1;
        $_POST['unit_use'][]        = 1;
        $_POST['price'][]           = 0;
        $_POST['seri'][]            = '';
        $_POST['kg_empty'][]        = 0;
        $_POST['kg_has_gas'][]      = 0;
    }
    
    /** @Author: DungNT Apr 17, 2018
     *  @Todo: check đơn hàng có gas hay không
     **/
    public function orderHaveGas() {
        $ok = false;
        if(!isset($_POST['materials_id'])){
            return $ok;
        }
        foreach($_POST['materials_id'] as $key=>$materials_id){
            $mDetail = new SellDetail();
            $mDetail->materials_id  = $materials_id;
            $mMaterial              = $mDetail->rMaterial;
            if($mMaterial){
                $mDetail->materials_type_id     = $mMaterial->materials_type_id;
                $mDetail->materials_parent_id   = $mMaterial->parent_id;
            }
            if($mDetail->materials_type_id == GasMaterialsType::MATERIAL_BINH_12KG){// Dec0617 phải overide giá của AppGas24h, nếu lấy biến post lên thì khi save 2,3 lần thì sẽ bị trừ đi 2,3 lần liền => sai
                $ok = true;
            }
        }
        return $ok;
    }
    
    /** @Author: DungNT May 08, 2018
     *  @Todo: get text help discount price app
     **/
    public function getTextDiscountApp() {
        $promotionAmountText = '';
        if($this->source != Sell::SOURCE_APP){
            return ;
        }
        $aDataCache     = $this->getPriceOfMonthSell();
        $priceHgd       = MyFunctionCustom::getHgdPriceSetup($aDataCache, $this->agent_id);
        foreach($this->aDetail as $key=>$mDetail):
            $mDetail->price_root    = isset($priceHgd[$mDetail->materials_id]) ? $priceHgd[$mDetail->materials_id] : 0;
            if($mDetail->materials_type_id == GasMaterialsType::MATERIAL_BINH_12KG){
                $promotionAmount = $mDetail->price_root - $mDetail->price + $this->promotion_amount;
                if($promotionAmount > 0){
                    $price = ActiveRecord::formatCurrency($mDetail->price_root);
                    $promotionAmountText = "Giá Gas niêm yết $price, tổng giảm giá ".ActiveRecord::formatCurrency($promotionAmount);
                }
            }
        endforeach;
        return $promotionAmountText;
    }
    
    /** @Author: DungNT May 28, 2018
     *  @Todo: set flag first order of customer
     **/
    public function setFirstOrder() {
        try{
            $this->telesaleCheck = true;
            $mOrderOld = $this->getOrderLastestOfCustomer();
            if(empty($mOrderOld) && empty($this->pttt_code)){
                /** @NOTE: telesale bắt đầu chạy từ 01/05/2018, sẽ thêm đk date này để tính đúng cho BQV KH
                 *  do đó first_order không phải là đơn hàng đầu tiên của KH, mà là 1 cờ để tính BQV cho telesale thôi 
                 */
                $this->first_order = Sell::IS_FIRST_ORDER;
            }
        } catch (Exception $ex) {
//            Logger::WriteLog($ex->getMessage());// Close Oct2618
        }
    }
    
    
    /** Mar 10, 2017 Step xử lý bán hàng trên web
     * 1/ Su Dung role User: ROLE_CALL_CENTER .Bắt các event của cuộc gọi bên Server VOIP
     * 2/ Hiển thị cuộc gọi lên popup, xử lý 2 event bị trùng nhau cho 1 số gọi đến, sợ ko kiểm soát dc socket bên io.huongminh
     * 3/ Hiển thị KH, lịch sử, xử lý search KH với số đt mới, cập nhật số đt cho KH. Cập nhật trạng thái cuộc gọi
     * 4/ Tạo bán hàng với loại User ROLE_CALL_CENTER
     * 5/ Danh sách cuộc gọi của NV và các action . 
     * 6/ Map các api với SouthTel
     * 7/ Test trên app Giao Nhận, Test trên phần mềm joiper chất lượng cuộc gọi 
     * 
     * 
     * Mỗi agent sẽ set là 1 group EXT để khi có cuộc gọi đến thì sẽ trả về group EXT
     * Khi bên kia bắt đầu đổ chuông cho 1 EXT xác định thì bên SPJ sẽ bắt event của PBX gửi sang
     * rồi notify call start đến đúng EXT đang online
     * - 1/ Cache agent => ext lại: array(agent_id => ext_number) -- done
     * - 2/ Cache ext => user login: array(ext_number => user_id ) -- 
     * - 3/ define các ext của đại lý trên hệ thống, để khi NV CallCenter login vào sẽ chọn đang ngồi ở Agent nào
     *  truyền vào model Users, để có thể sẽ tách từng vùng sẽ chọn list EXT riêng của vùng đó
     * 
     * ****** Hướng xử lý *******
     * 1/ done -- khi có cuộc gọi đến thì sẽ luôn lưu vào table CallTemp, đến sự kiện Trim thì mới tạo record bên table Call
     * 2/ Mỗi event của 1 cuộc gọi đều dc put sang server io rồi đẩy luôn xuống EXT
     * 
     * 
     */
    
    
    /*******************************************************************************/
    /** @Author: PHAM THANH NGHIA 30/5/2018
     *  @Code: NGHIA003
     *  @Param: Kiểm tra có thể cập nhật lại audit
     *  
     *  Kiểm tra trạng thái hủy đơn hàng của chị Ngọc - để báo cáo
     **/
    public function canAuditWeb(){
        if($this->status == Sell::STATUS_CANCEL){
            return true;
        }
        return false;
    }
    
    /** @Author: DungNT Oct 04, 2018
     *  @Todo: handle punish when PVKH cancel wrong
     **/
    public function doLaw() {
        /* 1. Save table p
         * 2. send notify to PVKH
         */
        $mTransactionHistory = $this->rTransactionHistory;
        $mSellTemp = new Sell();
        $mSellTemp->status_cancel = $this->order_type_status;
        
        $agentName          = $this->getAgent();
        $mEmployeeProblems  = new EmployeeProblems();
        $mScheduleNotify    = new GasScheduleNotify();
        
        $mEmployeeProblems->object_type     = EmployeeProblems::PROBLEM_HGD_CANCEL_WRONG;
        $mEmployeeProblems->object_id       = $mTransactionHistory->id;
        $mEmployeeProblems->money           = $mEmployeeProblems->getPrice();
        $mEmployeeProblemsOld = $mEmployeeProblems->getByTypeAndObjId();
        if(!empty($mEmployeeProblemsOld)){
            return ;
        }

        $mScheduleNotify->obj_id                                    = $mTransactionHistory->id;// id Sell
        $mScheduleNotify->type                                      = 0;
        $mScheduleNotify->aUidSend[$this->employee_maintain_id]     = $this->employee_maintain_id;
        $mScheduleNotify->mEmployeeProblems = $mEmployeeProblems;
        $mScheduleNotify->title = "[Tính lỗi] Hủy sai lý do - $agentName: {$this->getCodeNo()} - [Audit test: {$mSellTemp->getStatusCancelText()}] - {$this->getCustomer('first_name')} - {$this->getCustomer('address')}";
        // 1. send sms + save punish
        $mScheduleNotify->saveErrorsHgdSms($mTransactionHistory, []);
        // 2. send notify
        $mTransactionHistory->notifyPhoneAppEmployee($mScheduleNotify->aUidSend, $mScheduleNotify->title);
    }
    
    public function saveAudit(){// NghiaPT
        $model = Sell::model()->findByPk($this->id);
        $model->order_type_status   = $this->order_type_status;
        $model->note                = $model->formatNoteAudit($model->note, $this->note);
        $model->update(['order_type_status', 'note']);
        if($this->isCancelWrong){
            $model->doLaw();
        }
//        Sep0519 NamNH save comment
        $mGasComment = new GasComment();
        $mGasComment->content   = $model->getStatusAuditCancelText();
        $mGasComment->type      = GasComment::TYPE_5_Sell_COMMENT;
        $mGasComment->belong_id = $model->id;
        $mGasComment->saveCommentAudit();
    }
    
    public function getSpareText(){ // kí tự nối chuỗi 
        return '***';
    }
    public function formatNoteAudit($note_old, $note_new){ // $note_old = $this->note cũ
        if(!empty($note_old)){
            $aNote = explode($this->getSpareText(),$note_old);
            if(!empty($aNote[1])){
                $aNote[1] = $note_new ;
                return implode($this->getSpareText(), $aNote);
            }
        }
        return $note_old. $this->getSpareText() .$note_new;

    }
    // format trươc khi lưu cho note bình thường vẫn nối chuỗi hoặc thay đổi array[0]
    public function formatNoteDefault($note_old, $note_new){ // $note_old = $this->note cũ
        if(!empty($note_old)){
            $aNote = explode($this->getSpareText(),$note_old);
            if(!empty($aNote[0])){
                $aNote[0] = $note_new ;
                return implode($this->getSpareText(), $aNote);
            }
        }
        return $note_new;

    }
    
     /** @Author: PHAM THANH NGHIA 30/5/2018
     *  @Code: NGHIA003
     *  @Param: Xuất ra note dành cho ghi chú default
     **/
    public function getNoteAudit(){
        if($this->note){
            $aNote = explode($this->getSpareText(),$this->note);
            if(!empty($aNote[1])){
                return $aNote[1];
            }
        }
        return '';
    }
    
    /** @Author: PHAM THANH NGHIA 30/5/2018
     *  @Code: NGHIA003
     *  @Param: Xuất ra note dành cho ghi chú audit
     **/
    public function getNoteDefault(){ 
        if($this->note){
            $aNote = explode('***',$this->note);
            if(!empty($aNote[0])){
                return $aNote[0];
            }
        }
        return '';
    }
    /** @Author: Pham Thanh Nghia 19/7/2018
     *  @Todo:
     *  @Param: Xuất tên trạng thái hủy
     * order_type_status : là audit hủy
     **/
    public function getStatusAuditCancelText() {
        $aStatusCancel = $this->getArrayStatusCancel();
        return isset($aStatusCancel[$this->order_type_status]) ? $aStatusCancel[$this->order_type_status] : '';
    }
    /** @Author: Pham Thanh Nghia Aug 31, 2018
     *  @Todo: danh sách trạng thái kiểm tra
     **/
    public function getArrayAudit(){
        return array(
            Sell::AUDITED   => 'Đã kiểm tra',
            Sell::NOT_AUDIT => 'Chưa kiểm tra',
        );
    }
    
    /** @Author: NamNH Oct 03, 2018
     *  @Todo: get record timer api
     **/
    public function apiTimerGetRecord(){
        $dateNow            = date('Y-m-d');
        $dateTimer          = MyFormat::dateConverYmdToDmy($this->delivery_timer,'Y-m-d');
        $result             = [];
        $result['id']       = $this->id;
        $result['days']     = $dateTimer > $dateNow ? (int)MyFormat::getNumberOfDayBetweenTwoDate($dateNow,$dateTimer) : 0;
        $result['hour']     = (!empty($this->delivery_timer) && $this->delivery_timer != '0000-00-00' &&  $this->delivery_timer != '0000-00-00 00:00:00') ? (int)MyFormat::dateConverYmdToDmy($this->delivery_timer,'H') : 0;
        $result['date']     = (!empty($this->delivery_timer) && $this->delivery_timer != '0000-00-00' &&  $this->delivery_timer != '0000-00-00 00:00:00') ? MyFormat::dateConverYmdToDmy($this->delivery_timer,'H\hi d/m/Y') : MyFormat::dateConverYmdToDmy($this->created_date,'H\hi d/m/Y');
        return $result;
    }
    
    /** @Author: NamNH Oct 16, 2018
     *  @Todo: set discount Application
     **/
    public function getDiscountApp($type,&$mDetail){
        $result = 0;
        switch ($type){
            case Forecast::TYPE_PAYMENT_FIRST_ORDER:
                if($this->payment_type == Forecast::TYPE_BANK){
                    $criteria = new CDbCriteria();
                    $criteria->addCondition('t.customer_id=' . $this->customer_id);
                    $criteria->addCondition('t.status=' . Sell::STATUS_PAID);
                    $criteria->compare('t.payment_type', Forecast::TYPE_BANK);
                    if(!empty($this->id)){
                        $criteria->addCondition('t.id<>' . $this->id);
                    }
                    $criteria->limit = 1;
                    $criteria->order = 't.id DESC';
                    $mSellOld       = Sell::model()->find($criteria);
                    if(empty($mSellOld)){
                        $discount       =  $this->getDiscountFirstBank();
    //                    Set discount 50%
                        $result     = ($mDetail->price_root  /100) * $discount;
                        $mDetail->v1_discount_id = Forecast::TYPE_PAYMENT_FIRST_ORDER;
                    }else{
                        $discount       =  $this->getDiscountSecondBank();
    //                    Set discount 20%
                        $result     = ($mDetail->price_root  /100) * $discount;
                        $mDetail->v1_discount_id = Forecast::TYPE_PAYMENT_SECOND_ORDER;
                    }
                }
                break;
            Default:
                break;
        }
        $mDetail->v1_discount_amount = $result;
    }
    
    /** @Author: NamNH 17, 2018
     *  @Todo: get discount %
     **/
    public function getDiscountFirstBank(){
        return Yii::app()->params['bankDiscountFirstTime'];
    }
    
    /** @Author: NamNH 17, 2018
     *  @Todo: get discount %
     **/
    public function getDiscountSecondBank(){
        return Yii::app()->params['bankDiscountSecondTime'];
    }
    
    /** @Author: NamNH 17, 2018
     *  @Todo: get discount monney
     **/
    public function getDiscountGoldTime(){
        return Yii::app()->params['goldTimeDiscount'];
    }
    
    /** @Author: NamNH 17, 2018
     *  @Todo: get array gold time [7,8,9]
     **/
    public function getArrayGoldTime(){
        return explode(',', Yii::app()->params['goldTimeList']);
    }
    /** @Author: DungNT Oct 19, 2018
     *  @Param: get text gold time on app
     *  format '7h-10h / 13h-16h / 19h-22h';
     **/
    public function getGoldTimeText(){
        return Yii::app()->params['goldTimeText'];
    }
    
    /** @Author: DungNT Oct 28, 2018
     *  @Todo: đổi đầu số mới cho KH khi tạo đơn mà nhập số cũ 11 số
     **/
    public function changePhoneNewPrefix() {
        $aPrefix = UpdateSql1::getArrayPrefixChange();
        
        $fieldUpdate = $this->phone;
        $prefix_code = substr($fieldUpdate, 0, 4);
        if(isset($aPrefix[$prefix_code])){
            $charRemove     = strlen($prefix_code);
            $newPhone       = $aPrefix[$prefix_code].substr($fieldUpdate, $charRemove);
            $this->phone    = $newPhone;
        }
        
        if(!empty($this->phone)){// nếu ghi 2 phone số thì không validate
            if(!UsersPhone::isValidCellPhoneOrLandline($this->phone)){
                $this->addError('phone', "Số điện thoại $this->phone không là số điện thoại hợp lệ, vui lòng kiểm tra lại");
            }
        }
    }
    
    /** @Author: DungNT Now 12, 2018
     *  @Todo: get call hangup
     **/
    public function getCallEndTime($format = 'd/m/Y H:i') {
        if(empty($this->call_end_time)){
            return '';
        }
        return date($format, $this->call_end_time);
    }
    /** @Author: DungNT Now 12, 2018
     *  @Todo: show on web
     **/
    public function getCallEndTimeWeb() {
        $text = $this->getCallEndTime('H:i');
        if(empty($text)){
            return '';
        }
        return "[KH gọi $text]";
    }
    
    /** @Author: KHANH TOAN 2018
     *  @Todo: check visiable lí do hủy của đơn hàng
     **/
    public function canCommentReasonCancel() {
        $dayAllow   = date('Y-m-d');
        $dayAllow   = MyFormat::modifyDays($dayAllow, 3, '-');// cho phép update trong ngày
        
        if(!MyFormat::compareTwoDate($this->created_date_only, $dayAllow)){
            return false;
        }
        
        $ok = false;
        $cUid = MyFormat::getCurrentUid();
        $cRole = MyFormat::getCurrentRoleId();
        $aRoleAllow = [ROLE_CALL_CENTER, ROLE_DIEU_PHOI, ROLE_ADMIN];
        if($this->status == Sell::STATUS_CANCEL && in_array($cRole, $aRoleAllow)){
            $ok = true;// Apr1119 cho phép tổng đài cập nhật hủy của nhau
        }
        return $ok;
    }
    
    /** @Author: KHANH TOAN 2018
     *  @Todo: get content comment cancel
     **/
    public function getReasonCanCel() {
//        $mGasComment = GasComment::model()->findByAttributes(array('belong_id' => $this->id, 'type' => GasComment::TYPE_4_SELL_CANCEL));
        $mGasComment = $this->rComment;
        $reasonCancel = '';
        if($mGasComment){
            $reasonCancel = "<b>TĐ note</b>: {$mGasComment->content}";
        }
        return $reasonCancel;
    }
    
    /** @Author: DungNT Mar 24, 2019 
     *  @Todo: check user xóa hẹn giờ trên web, với đơn hẹn giờ
     **/
    public function checkUnsetTimer() {
        if(empty($this->delivery_timer)){
            $this->is_timer = 0;
        }
    }
    
    /** @Author: DungNT Dec 03, 2018
     *  @Todo: kiểm tra lại đơn timer khi chỉnh lại khung giờ vàng ở trên giao diện 
     * @case1: KH đặt hẹn giờ 11h, tổng đài chỉnh lại 20h -> không apply giảm 20 giờ vàng -> phải tính lại ở hàm này
     * @fix: tạo 1 model ảo Transaction để tái sử dụng chung hàm applyGoldTime tính toán lại v1_discount_id + v1_discount_amount, copy data từ Sell sang 
     **/
    public function recheckToApplyGoldTime() {
        $mSellClone = clone $this;// CLONE RA 1 MODEL KHAC DE XU LY function formatDbDatetime
        $mSellClone->formatDbDatetime();
        $mTransaction = new Transaction();
        $aFieldNotCopy = array('id');
        MyFormat::copyFromToTable($mSellClone, $mTransaction, $aFieldNotCopy);
        if($this->is_timer == Forecast::TYPE_TIMER){
            /* Dec2318 chỗ đang cộng dồn promotion_amount sau 1 lần click save từ web -> sai
             * Do Nam xử lý nếu user đã có sẵn 1 KM áp dụng + thêm giờ vàng nữa, mà giờ vàng lại ghi vào biến promotion_amount, nên phải cộng dồn để ko mất KM nếu có của KH
             * => FIX: do ở đây là Create + update, nên nếu đã vào case này thì luôn trừ đi số tiền 20k giờ vàng
             * chỗ này phải check kỹ vì có thể đơn hẹn giờ không phải giờ vàng, nếu luôn trừ đi thì sẽ sai
             * => FIX: trong hàm applyGoldTime chỉ sử dụng để tính v1_discount_amount, không sử dụng promotion_amount như trước nữa, vì sẽ bị cộng dồn 
             */
            // promotion nếu có của KH promotion_id > 0
            $promotionAmountOfPromotionId = 0;
            if($this->promotion_id > 0){
                $promotionAmountOfPromotionId = $this->promotion_amount - $this->getDiscountGoldTime();
            }

            $mTransaction->applyGoldTime();// trong ham nay set value cho 2 bien: promotion_amount, grand_total
            $this->v1_discount_id       = $mTransaction->v1_discount_id;
            $this->v1_discount_amount   = $mTransaction->v1_discount_amount;
            $this->promotion_amount     = $promotionAmountOfPromotionId + $mTransaction->v1_discount_amount;
            $this->overridePromotionOfGoldTime();
        }
    }
    
    /** @Author: DungNT May 06, 2019 
     *  @Todo: ưu tiên khuyến mãi cao cho đơn hàng, bỏ KM giờ vàng đi
     * fix cho các đơn hẹn giờ vừa có mã giới thiệu vừa đặt hẹn giờ: vd S19FW1FZN
     **/
    public function overridePromotionOfGoldTime() {
        if($this->promotion_id < 1){
            return ;
        }
        $mPromotionUser = $this->rAppPromotionUser;
        $this->promotion_amount     = $mPromotionUser->promotion_amount;
        $this->v1_discount_amount   = 0;
    }
    
    /** @Author: DungNT Dec 09, 2018
     *  @Todo: DH new,  get số order đã confirm của agent, sử dụng để kiểm tra 1 PVKH có nhận nhiều order rồi để đó không ->canConfirmMoreOrder
     * @param: $agent_id
     * @param: $created_date_only format Y-m-d
     * check duplicate: SELECT *, COUNT(*) c FROM `gas_sell` WHERE `source` =2 AND `promotion_id` > 0 AND status=2 AND promotion_type NOT IN (10, 11) group by customer_id, promotion_id HAVING c > 1;
     **/
    public function getOrderConfirmOfAgent($agent_id, $created_date_only) {
        $criteria = new CDbCriteria();
        $criteria->addCondition('t.agent_id=' . $agent_id);
        $criteria->addCondition('t.status=' . Sell::STATUS_NEW);
        $criteria->addCondition('t.created_date_only_bigint=' . strtotime($created_date_only));
        return Sell::model()->findAll($criteria);
    }

    /** @Author: NamNH date
     *  @Todo: Mar 20, 2019
     *  $criteria update condition
     **/
    public function handleSearchTarget(&$criteria){
//        BQV share
        if(!empty($this->target_v1)){
            $criteriaTarget         = new CDbCriteria();
            $criteriaTarget->compare('type', TargetDaily::TYPE_CCS_SHARE);
            $criteriaTarget->compare('t.created_by', $this->target_v1);
            if(!empty($this->date_from)){
                $date_from = MyFormat::dateDmyToYmdForAllIndexSearch($this->date_from);
                DateHelper::searchGreater($date_from, 'sell_date_bigint', $criteriaTarget);
            }
            if(!empty($this->date_to)){
                $date_to = MyFormat::dateDmyToYmdForAllIndexSearch($this->date_to);
                DateHelper::searchSmaller($date_to, 'sell_date_bigint', $criteriaTarget);
            }
            $aTarget                = TargetDaily::model()->findAll($criteriaTarget); 
            $aId                    = CHtml::listData($aTarget, 'sell_id', 'sell_id');
            $criteria->addInCondition('t.id', $aId);
        }
        if(!empty($this->target_v2)){
            $criteriaTarget         = new CDbCriteria();
            $criteriaTarget->addInCondition('type', [TargetDaily::TYPE_CCS,TargetDaily::TYPE_TELESALE_BQV_APP,TargetDaily::TYPE_TELESALE_BQV_CALL]);
            $criteriaTarget->compare('t.created_by', $this->target_v2);
            if(!empty($this->date_from)){
                $date_from = MyFormat::dateDmyToYmdForAllIndexSearch($this->date_from);
                DateHelper::searchGreater($date_from, 'sell_date_bigint_v2', $criteriaTarget);
            }
            if(!empty($this->date_to)){
                $date_to = MyFormat::dateDmyToYmdForAllIndexSearch($this->date_to);
                DateHelper::searchSmaller($date_to, 'sell_date_bigint_v2', $criteriaTarget);
            }
            $aTarget                = TargetDaily::model()->findAll($criteriaTarget); 
            $aId                    = CHtml::listData($aTarget, 'sell_id_v2', 'sell_id_v2');
            $criteria->addInCondition('t.id', $aId);
        }
        if(!empty($this->type_target)){
            if($this->type_target == self::TYPE_BQV_2){
                $criteriaTarget         = new CDbCriteria();
                $criteriaTarget->compare('t.support_id', '1');
                $criteriaTarget->addCondition('t.sell_id_v2 > 0');
                $aTarget                = TargetDaily::model()->findAll($criteriaTarget); 
                $aId                    = CHtml::listData($aTarget, 'sell_id_v2', 'sell_id_v2');
                $criteria->addInCondition('t.id', $aId);
            }elseif($this->type_target == self::TYPE_BQV_1){
                $criteriaTarget         = new CDbCriteria();
                $criteriaTarget->addCondition('t.sell_id > 0');
                $aTarget                = TargetDaily::model()->findAll($criteriaTarget); 
                $aId                    = CHtml::listData($aTarget, 'sell_id', 'sell_id');
                $criteria->addInCondition('t.id', $aId);
            }
        }
    }
    
    public function getArrayTypeCcs(){
        return [
            self::TYPE_BQV_2 => 'BQV 2 Telesale có cuộc gọi',
            self::TYPE_BQV_1 => 'BQV 1',
        ];
    }
    
    /** @Author: NamNH NamNH 10, 2019
     *  @Todo: get report agent v2
     **/
    public function getReportAgentV2(){
        $aData = [];
        $criteria = new CDbCriteria();
        $this->reportAgentCriteriaOutPut($criteria);
        $criteria->select = 'sum(t.price_root * t.qty) as price_root,sum(t.price * t.qty) as price,t.source,t.promotion_id,sum(t.promotion_amount) as promotion_amount, sum(t.amount_bu_vo) as amount_bu_vo, sum(t.qty) as qty, sum(t.qty_discount) as qty_discount, sum(t.amount_discount) as amount_discount, t.agent_id, t.materials_type_id, t.materials_id';
        $criteria->group  = 't.agent_id,materials_type_id,t.materials_id,t.source,t.promotion_id';
        $this->reportAgentCriteriaOutPut($criteria);
        $criteria->order  = 't.materials_type_id';
        $models = SellDetail::model()->findAll($criteria);
        foreach ($models as $mSellDetail) {
//            Số lượng
            if(isset($aData['OUTPUT']['qty'][$mSellDetail->agent_id][$mSellDetail->materials_type_id][$mSellDetail->materials_id])){
                $aData['OUTPUT']['qty'][$mSellDetail->agent_id][$mSellDetail->materials_type_id][$mSellDetail->materials_id] += $mSellDetail->qty;
            }else{
                $aData['OUTPUT']['qty'][$mSellDetail->agent_id][$mSellDetail->materials_type_id][$mSellDetail->materials_id] = $mSellDetail->qty;
            }
//            số call
            if($mSellDetail->source == self::SOURCE_WEB){
                if(isset($aData['OUTPUT']['call'][$mSellDetail->agent_id][$mSellDetail->materials_type_id][$mSellDetail->materials_id])){
                    $aData['OUTPUT']['call'][$mSellDetail->agent_id][$mSellDetail->materials_type_id][$mSellDetail->materials_id] += $mSellDetail->qty;
                }else{
                    $aData['OUTPUT']['call'][$mSellDetail->agent_id][$mSellDetail->materials_type_id][$mSellDetail->materials_id] = $mSellDetail->qty;
                }
            }
//            số app
            if($mSellDetail->source == self::SOURCE_APP){
                if(isset($aData['OUTPUT']['app'][$mSellDetail->agent_id][$mSellDetail->materials_type_id][$mSellDetail->materials_id])){
                    $aData['OUTPUT']['app'][$mSellDetail->agent_id][$mSellDetail->materials_type_id][$mSellDetail->materials_id] += $mSellDetail->qty;
                }else{
                    $aData['OUTPUT']['app'][$mSellDetail->agent_id][$mSellDetail->materials_type_id][$mSellDetail->materials_id] = $mSellDetail->qty;
                }
            }
//            chiết khấu
            $AmountDiscount = $this->getPromotionDiscount();
            $amount_discount = $mSellDetail->amount_discount > 9 ? $mSellDetail->amount_discount : 0;
            if(isset($aData['OUTPUT']['discount_amount'][$mSellDetail->agent_id][$mSellDetail->materials_type_id][$mSellDetail->materials_id])){
                $aData['OUTPUT']['discount_amount'][$mSellDetail->agent_id][$mSellDetail->materials_type_id][$mSellDetail->materials_id] += ($mSellDetail->qty_discount * $AmountDiscount) + $amount_discount;
            }else{
                $aData['OUTPUT']['discount_amount'][$mSellDetail->agent_id][$mSellDetail->materials_type_id][$mSellDetail->materials_id] = ($mSellDetail->qty_discount * $AmountDiscount) + $amount_discount;
            }
//            Bù vỏ
            if(isset($aData['OUTPUT']['amount_bu_vo'][$mSellDetail->agent_id][$mSellDetail->materials_type_id][$mSellDetail->materials_id])){
                $aData['OUTPUT']['amount_bu_vo'][$mSellDetail->agent_id][$mSellDetail->materials_type_id][$mSellDetail->materials_id] += $mSellDetail->amount_bu_vo;
            }else{
                $aData['OUTPUT']['amount_bu_vo'][$mSellDetail->agent_id][$mSellDetail->materials_type_id][$mSellDetail->materials_id] = $mSellDetail->amount_bu_vo;
            }
//            Giảm giá CV
            if($mSellDetail->promotion_id   <= 0){
                if(isset($aData['OUTPUT']['promotion_cv'][$mSellDetail->agent_id][$mSellDetail->materials_type_id][$mSellDetail->materials_id])){
                    $aData['OUTPUT']['promotion_cv'][$mSellDetail->agent_id][$mSellDetail->materials_type_id][$mSellDetail->materials_id] += $mSellDetail->promotion_amount;
                }else{
                    $aData['OUTPUT']['promotion_cv'][$mSellDetail->agent_id][$mSellDetail->materials_type_id][$mSellDetail->materials_id] = $mSellDetail->promotion_amount;
                }
            }
//            Giảm giá CV
            if($mSellDetail->promotion_id   > 0){
                if(isset($aData['OUTPUT']['promotion_app'][$mSellDetail->agent_id][$mSellDetail->materials_type_id][$mSellDetail->materials_id])){
                    $aData['OUTPUT']['promotion_app'][$mSellDetail->agent_id][$mSellDetail->materials_type_id][$mSellDetail->materials_id] += $mSellDetail->promotion_amount;
                }else{
                    $aData['OUTPUT']['promotion_app'][$mSellDetail->agent_id][$mSellDetail->materials_type_id][$mSellDetail->materials_id] = $mSellDetail->promotion_amount;
                }
            }
//            Giảm giá mặt định
            if(isset($aData['OUTPUT']['promotion_default'][$mSellDetail->agent_id][$mSellDetail->materials_type_id][$mSellDetail->materials_id])){
                $aData['OUTPUT']['promotion_default'][$mSellDetail->agent_id][$mSellDetail->materials_type_id][$mSellDetail->materials_id] += $mSellDetail->price_root - $mSellDetail->price;
            }else{
                $aData['OUTPUT']['promotion_default'][$mSellDetail->agent_id][$mSellDetail->materials_type_id][$mSellDetail->materials_id] = $mSellDetail->price_root - $mSellDetail->price;
            }
        }
//            Lấy giá niêm yết
        $listPrice = UsersPriceHgd::getPriceHgd($this->date_from_ymd);
        foreach ($listPrice as $key => $value) {
            foreach ($value['list_agent'] as $key => $agent_id) {
                foreach ($value['list_materials'] as $material_id => $price) {
                    $aData['PRICE'][$agent_id][$material_id] = $price;
                }
            }
        }
        return $aData;
    }
    
    /** @Author: DungNT Aug 16, 2019
     *  @Todo: count notify of TransactionHistory
     *  Bình thường thì xanh, nếu tạo notify bị lỗi thì high light đỏ cảnh báo
     **/
    public function getHtmlCountNotify() {
        if($this->status != Sell::STATUS_NEW || !empty($this->employee_maintain_id) || empty($this->transaction_history_id) || $this->created_date_only != date('Y-m-d')){
            return '';
        }
        $html = '';
        $mScheduleNotify = new GasScheduleNotify();
        $totalNotify = $mScheduleNotify->countByObjIdAndType($this->transaction_history_id, $mScheduleNotify->getAllTypeHgd());
        if($totalNotify > 5){
            $msg    = "Tạo đơn hàng thành công, tổng số Notify = $totalNotify";
            $html   = "<div class='flash notice'><a data-dismiss='alert' class='close' href='javascript:void(0)'>×</a>$msg</div>";
        }else{
            $msg    = "Lỗi đơn hàng, tổng số Notify = $totalNotify vui lòng bấm cập nhật đơn hàng 1 lần nữa";
            $html   = "<div class='flash notice_error'><a data-dismiss='alert' class='close' href='javascript:void(0)'>×</a>$msg</div>";
        }
        return $html;
    }
    
}