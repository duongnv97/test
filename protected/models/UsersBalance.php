<?php

/**
 * This is the model class for table "{{_users_balance}}".
 *
 * The followings are the available columns in table '{{_users_balance}}':
 * @property string $id
 * @property integer $type
 * @property string $user_id
 * @property integer $c_month
 * @property integer $c_year
 * @property integer $balance
 */
class UsersBalance extends CActiveRecord
{
    public $date_from, $date_to, $aDataBoMoi = [], $getBoMoi = false;

    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{_users_balance}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        return array(
            array('date_from, date_to, id, type, user_id, c_month, c_year, balance', 'safe'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        return [];
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'type' => 'Type',
            'user_id' => 'User',
            'c_month' => 'C Month',
            'c_year' => 'C Year',
            'balance' => 'Balance',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        $criteria=new CDbCriteria;
        $criteria->compare('t.balance',$this->balance);
        $criteria->order = 't.id DESC';
        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
            'pagination'=>array(
                'pageSize'=> Yii::app()->user->getState('pageSize',Yii::app()->params['defaultPageSize']),
            ),
        ));
    }

    /**
     * @Author: ANH DUNG Apr 19, 2017
     * @Todo: set balance of user by one month, year
     */
    public function getByMonthYear() {
        if(empty($this->c_month) || empty($this->c_year) || empty($this->user_id)){
            return ;
        }
        /** @step_by_step
         * 1. get revenue from BH Hộ GĐ trong tháng - năm
         * 2. get revenue from thu tiền bò mối trong tháng - năm
         * 2.1 get luôn chi phí trong table Thu Bò Mối trong tháng - năm
         * 3. xóa record cũ của users nếu có
         * 4. insert record mới
         */
        $aRes           = [];
        $tmpDate        = $this->c_year.'-'.$this->c_month.'-01';
        $dayOfMonth     = MyFormat::dateConverYmdToDmy($tmpDate, 't');
        if(empty($this->date_from)){
            $this->date_from = '01-'.$this->c_month.'-'.$this->c_year;// 21-04-2017
            $this->date_to   = $dayOfMonth.'-'.$this->c_month.'-'.$this->c_year;
        }
        $this->getRevenueBoMoi($aRes);
        if($this->getBoMoi){
            return ;
        }
        $this->getRevenueHgd($aRes);
        $this->balance = $aRes['revenue_hgd'] + $aRes['revenue_bo_moi'];
    }
    
    /**
     * @Author: ANH DUNG May 02, 2017
     * @Todo: get revenue form HGĐ
     */
    public function getRevenueHgd(&$aRes) {
        $mSell      = new Sell();
        $mSell->agent_id        = 0;
        $mSell->date_from       = $this->date_from;// 21-04-2017
        $mSell->date_to         = $this->date_to;
        $mSell->status          = Sell::STATUS_PAID;
        $mSell->employee_maintain_id = $this->user_id;
        $aData                  = StaApp::hgd($mSell);
        $aRes['revenue_hgd'] = isset($aData['total_revenue']) ? MyFormat::removeComma($aData['total_revenue']) : 0;
    }
    
    /**
     * @Author: ANH DUNG May 02, 2017
     * @Todo: get revenue form HGĐ
     */
    public function getRevenueBoMoi(&$aRes) {
        $mEmployeeCashbook      = new EmployeeCashbook();
        $mEmployeeCashbook->agent_id        = 0;
        $mEmployeeCashbook->date_from       = $this->date_from;// 21-04-2017
        $mEmployeeCashbook->date_to         = $this->date_to;
        $mEmployeeCashbook->employee_id     = $this->user_id;
        $aData                              = $mEmployeeCashbook->getRevenueByEmployee();
        $amount = 0;
        if(isset($aData[$this->user_id])){
            $this->aDataBoMoi = $aData[$this->user_id];
            $amount = $aData[$this->user_id]['amount_revenue']-$aData[$this->user_id]['amount_cost'];
        }
        $aRes['revenue_bo_moi'] = $amount;
    }
    
    
}