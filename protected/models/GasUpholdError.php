<?php

/**
 * This is the model class for table "{{_gas_uphold_error}}".
 * @Code: NAM009
 * The followings are the available columns in table '{{_gas_uphold_error}}':
 * @property string $id
 * @property string $employee_id
 * @property string $uphold_id
 * @property string $last_uphold
 * @property string $created_date
 */
class GasUpholdError extends BaseSpj
{
    public $autocomplete_name, $autocomplete_name_sale, $autocomplete_employee;
    public $date_from,$date_to;
    const DAYS_REMINDER_DINH_KY = 5;

    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return GasUpholdError the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{_gas_uphold_error}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        return array(
            array('employee_id, uphold_id, created_date', 'required','on'=>'update,create'),
            array('schedule_type,schedule_in_month,agent_id,sale_id,type_customer,customer_id, id,employee_id, uphold_id, last_uphold, created_date, date_from, date_to', 'safe'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        return array(
            'rEmployee' => array(self::BELONGS_TO, 'Users', 'employee_id'),
            'rUphold' => array(self::BELONGS_TO, 'GasUphold', 'uphold_id'),
            'rSale' => array(self::BELONGS_TO, 'Users', 'sale_id'),
            'rAgent' => array(self::BELONGS_TO, 'Users', 'agent_id'),
            'rCustomer' => array(self::BELONGS_TO, 'Users', 'customer_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'employee_id' => 'Nhân viên',
            'uphold_id' => 'Bảo trì định kỳ',
            'last_uphold' => 'Ngày bảo trì mới nhất',
            'created_date' => 'Ngày tạo',
            'date_from' => 'Bảo trì từ ngày',
            'date_to'   => 'Đến ngày',
            'customer_id'   => 'Khách hàng',
            'sale_id'   => 'NV Kinh Doanh',
            'type_customer'   => 'Loại KH',
            'schedule_type'   => 'Loại bảo trì',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        $criteria=new CDbCriteria;
        $criteria->compare('t.employee_id',$this->employee_id);
        $criteria->compare('t.customer_id',$this->customer_id);
        $criteria->compare('t.sale_id',$this->sale_id);
        $criteria->compare('t.type_customer',$this->type_customer);
        $criteria->compare('t.schedule_type',$this->schedule_type);
        if(!empty($this->date_from)){
            $date_from = MyFormat::dateDmyToYmdForAllIndexSearch($this->date_from);
            $criteria->addCondition("t.last_uphold >= '$date_from'");
        }
        if(!empty($this->date_to)){
            $date_to = MyFormat::dateDmyToYmdForAllIndexSearch($this->date_to);
            $criteria->addCondition("t.last_uphold <= '$date_to'");
        }
        $criteria->order = 't.id DESC';
        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
            'pagination'=>array(
                'pageSize'=> Yii::app()->user->getState('pageSize',Yii::app()->params['defaultPageSize']),
            ),
        ));
    }
    /** @Author: HOANG NAM 09/04/2018
     *  @Todo: get
     **/
    public function getEmployee() {
        $mUser = $this->rEmployee;
        return $mUser ? $mUser->first_name : '';
    }
    public function getLastUphold() {
       
        return !empty($this->last_uphold) ? MyFormat::dateConverYmdToDmy($this->last_uphold) : '';
    }
    public function getCustomer($field_name='') {
        $mUser = $this->rCustomer;
        if($mUser){
            if($field_name != ''){
                return $mUser->$field_name;
            }
            return $mUser->code_bussiness.' - '.$mUser->first_name;
        }
        return '';
    }
    public function getCustomerType() {
        $mUser = $this->rCustomer;
        if($mUser){
            return $mUser->getTypeCustomerText();
        }
        return '';
    }
    public function getSale() {
        $mUser = $this->rSale;
        if($mUser){
            return $mUser->first_name;
        }
        return '';
    }
    public function getScheduleText() {
        $mUphold = $this->rUphold;
        if($mUphold){
            return $mUphold->getScheduleText();
        }
        return '';
    }
    public function getArrayScheduleType() {
        $mUphold = new GasUphold();
        return $mUphold->getArrayScheduleType();
    }
    
    /** @Author: HOANG NAM 09/04/2018
     *  @Todo: can delete
     *  @Param: 
     **/
    public function canDelete() {
        $cRole  = MyFormat::getCurrentRoleId();
            if($cRole == ROLE_ADMIN){
            return true;
        }
        return false;
    }
    
    /** @Author: HOANG NAM 10/04/2018
     *  @Todo: get uphold by date and uptold id
     *  @Param: id $id_uphold
     *  $last_uphold: date Y-m-d
     **/
    public function getGasUpholdError($id_uphold, $last_uphold) {
        $criteria=new CDbCriteria;
        $criteria->addCondition("t.uphold_id = {$id_uphold}");
        $criteria->addCondition("t.last_uphold = '{$last_uphold}'");
        $criteria->limit = 1;
        $aModel = GasUpholdError::model()->findAll($criteria);
        $result = isset($aModel[0]) ? $aModel[0] : null;
        return $result;
    }
    
    /** @Author: HOANG NAM 10/04/2018
     *  @Todo: update next_uphold for all
     *  @Param: 
     **/
    public function updateNextUphold(){ 
        $from = time();
        ini_set('memory_limit','1500M');
        $fakeGasUphold = new GasUphold();
        $criteriaGasUphold = new CDbCriteria;
        $criteriaGasUphold->compare('t.type',GasUphold::TYPE_DINH_KY);
        $criteriaGasUphold->addCondition('t.status_uphold_schedule =' . GasUphold::STATUS_UPHOLD_RUNNING);
        $criteriaGasUphold->addCondition('t.schedule_type > 0');
        $aGasUpholdUpdate = GasUphold::model()->findAll($criteriaGasUphold);
        $fakeGasUphold->updateNextUphold($aGasUpholdUpdate);
        $to = time();
        $second = $to-$from;
        echo count($aGasUpholdUpdate).' done in: '.($second).'  Second  <=> '.($second/60).' Minutes';die;
    }
    
    /** @Author: HOANG NAM 09/04/2018
     *  @Todo: generate error
     *  @FLOW: 
     * 1. update updateNextUphold trong table GasUphold
     * 2. setup cron GenerateError cron này sinh error và sinh ra row nhắc nhở định kỳ (GasUpholdPlan) đến ngày cho NV BT
     * 3. limit row định kỳ theo lịch nhắc nhở mới: GasUpholdPlan
     * 4. khi có reply thì xóa record bên GasUpholdError đi
     * 
     **/
    public function generateError($can_duplicate = false){
        die('Need open to review code');
        $from = time(); ini_set('memory_limit','1500M');
//        Update all next uphold -> mở nếu muốn cập nhật next_uphold
//        $this->updateNextUphold();die;

//        Generate GasUpholdPlan
        $this->generateGasUpholdPlan(GasUpholdError::DAYS_REMINDER_DINH_KY);
        return ;// May1818 không chạy đoạn dưới nữa, chỉ sinh ra plan cho NV
        
//        generate to table gas_gas_uphold_error
        $dateCurrent = date('Y-m-d');
        $criteria = new CDbCriteria;
        $criteria->compare('t.type',GasUphold::TYPE_DINH_KY);
        $criteria->addCondition('t.status_uphold_schedule =' . GasUphold::STATUS_UPHOLD_RUNNING);
        $criteria->addCondition('t.next_uphold <' ."'{$dateCurrent}'");
        $aGasUphold = GasUphold::model()->findAll($criteria);
        $aRowInsert = array();
        $tableName = GasUpholdError::model()->tableName();
        foreach ($aGasUphold as $key => $mGasUphold) {
            $mGasUpholdReply    = $mGasUphold->rUpholdReplyForUpdateNextUphold;
            $employee_id        = $mGasUphold->employee_id;
            $uphold_id          = $mGasUphold->id;
            $last_uphold ='';
            if(!empty($mGasUpholdReply)) {
                $last_uphold = MyFormat::dateConverYmdToDmy($mGasUpholdReply->created_date, 'Y-m-d');
            }else {
                $last_uphold = MyFormat::dateConverYmdToDmy($mGasUphold->created_date, 'Y-m-d');
            }
            
            if(!$can_duplicate){
                $mUptoldError = $this->getGasUpholdError($uphold_id, $last_uphold);
                if(!empty($mUptoldError)){
                    continue;
                }
            }
            $aRowInsert[]="({$employee_id},{$uphold_id},'{$last_uphold}', '$mGasUphold->schedule_type', '$mGasUphold->schedule_in_month', '$mGasUphold->agent_id', '$mGasUphold->sale_id', '$mGasUphold->type_customer', '$mGasUphold->customer_id')";
        }
        $sql = "insert into $tableName (employee_id,uphold_id,last_uphold,schedule_type,schedule_in_month,agent_id,sale_id,type_customer,customer_id) values ";
        $sql .= implode(',', $aRowInsert);
        if(count($aRowInsert)>0){
            Yii::app()->db->createCommand($sql)->execute();
        }
        
        $to = time();
        $second = $to-$from;
        $info = 'Cron Uphold generateError '.count($aGasUphold).' done in: '.($second).'  Second  <=> '.($second/60).' Minutes';
        Logger::WriteLog($info);
    }
    
    /** @Author: HOANG NAM 13/04/2018
     *  @Todo: generate gasuphold plan
     * */
    public function generateGasUpholdPlan($countDay = 10) {
        Logger::WriteLog("***************** RUN generateGasUpholdPlan NEED REVIEW CRON RUN ***********************");
        $idAdmin = 0;
        $dateCurrent = date('Y-m-d');
        $criteria = new CDbCriteria;
        $criteria->compare('t.type', GasUphold::TYPE_DINH_KY);
        $criteria->addCondition('t.status_uphold_schedule =' . GasUphold::STATUS_UPHOLD_RUNNING);
        $criteria->addCondition('t.next_uphold =' . "DATE_ADD('{$dateCurrent}',INTERVAL {$countDay} DAY)");
        // May1918 khi NV hoàn thành định kỳ thì next_uphold sẽ đc cộng lên, nên không lo việc NV báo cáo trước lịch
        $aGasUphold = GasUphold::model()->findAll($criteria);
        $created_date = date('Y-m-d H:i:s');
        if (!empty($aGasUphold)) {
            $aRowInsert = array();
            foreach ($aGasUphold as $key => $mGasUphold) {
                $aRowInsert[] = "('" . $dateCurrent . "',
                    '" . $mGasUphold->next_uphold . "',
                    '" . $mGasUphold->employee_id . "',
                    '" . $mGasUphold->customer_id . "',
                    '" . $created_date . "',
                    '" . $idAdmin . "'
                    )";
            }
            $tableName = GasUpholdPlan::model()->tableName();
            $sql = "insert into $tableName (start_date,end_date,employee_id,customer_id, created_date, created_by) values ";
            $sql .= implode(',', $aRowInsert);
            if (count($aRowInsert) > 0) {
                Yii::app()->db->createCommand($sql)->execute();
            }
        }
    }
    
}