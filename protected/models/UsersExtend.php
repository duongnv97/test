<?php
/** Sep 01, 2016
 * This is the model class extends Users
 */
class UsersExtend extends Users
{
     // các define trước của Type ở ngoài config.local
    const STORE_CARD_HGD_VIP_1      = 10; // Loại KH HGD vip của Phòng Hộ mới lập 
    const STORE_CARD_HGD_APP        = 11; // Loại KH HGD tạo từ app android 
    const STORE_CARD_HEAD_QUATER    = 12; // Loại KH trụ sở chính của chuỗi KH
    const STORE_CARD_NCC                    = 13; // Loại KH NCC
    const EMPLOYEE_TELESALE                 = 14; // Loại Nhân Viên - Telesale - của Role CallCenter
    const STORE_CARD_FOR_QUOTE              = 15; // Loại KH tiềm năng - bò mối
    const STORE_CARD_SUPPLIER               = 16; // May1419 Gas bồn - NCC 
    const STORE_CARD_SUPPLIER_WAREHOUSE     = 17; // Gas bồn - NCC - KHO
    const STORE_CARD_GAS_TANK               = 18; // Gas bồn - KH 
    const STORE_CARD_TRANSPORT              = 19; // Gas bồn - đơn vị vận chuyển

    const HGD_TIEP_XUC  = 1;
    const HGD_BAO_TRI   = 2;
    const BUYING_DA_MUA     = 1;
    const BUYING_CHUA_MUA   = 2;
    const JSON_ARRAY_AGENT  = 'JSON_ARRAY_AGENT';// for address_temp in table Users
    const AGENT_NAME_REAL   = 'AGENT_NAME_REAL';// for address_temp in table Users
    
    
    public static function getTypeCustomerBB(){
        return array(
            STORE_CARD_KH_BINH_BO,
            UsersExtend::STORE_CARD_HEAD_QUATER,
        );
    }
    
    public static function getHgdTypePoint(){
        return array(
            UsersExtend::HGD_TIEP_XUC   => "Tiếp xúc",
            UsersExtend::HGD_BAO_TRI    => "Bảo trì",
        );
    }
    
    public static function getHgdBuying(){
        return array(
            UsersExtend::BUYING_DA_MUA   => "Đã mua hàng",
            UsersExtend::BUYING_CHUA_MUA => "Chưa mua hàng",
        );
    }
    
    /**
     * @Author: ANH DUNG Sep 07, 2016
     * @Todo: get text show HGD type: 1 Tiếp xúc, 2: Bảo trì
     */
    public static function getHgdTypePointText($type){
        $aType = self::getHgdTypePoint();
        return isset($aType[$type]) ? $aType[$type] : "";
    }
    
    /**
    * @Author: ANH DUNG Sep 01, 2016
    * @Todo: get listing Hgd
    * @param: $q object post params
    */
    public function ApiListing($q, $mUserLogin) {
        $criteria = new CDbCriteria();
        if(isset($q->date_from)){
            $this->date_from = $q->date_from;
        }
        if(isset($q->date_to)){
            $this->date_to = $q->date_to;
        }
        if(isset($q->buying) && !empty($q->buying)){
            $this->buying = $q->buying;
        }
        
        $this->ApiListingCriteria($criteria, $mUserLogin);
        return new CActiveDataProvider($this,array(
            'criteria' => $criteria,
            'pagination'=>array(
                'pageSize' => GasAppOrder::API_LISTING_ITEM_PER_PAGE,
                'currentPage' => (int)$q->page,
            ),
          ));
//        return $dataProvider;
    }
    
    /**
     * @Author: ANH DUNG Sep 01, 2016
     * @Todo: use same criteria with web
     */
    public function ApiListingCriteria(&$criteria, $mUserLogin) {
        $criteria->addCondition('t.id > 1280358');// Jan1618 OK để giới hạn record giảm đc time của query không? bình thường đang hơn 2 second - sellId=448513-01/01/2018
        $this->ccsSameCriteria($criteria, $mUserLogin);
        $criteria->order = 't.id DESC';
        if(!empty($this->date_from)){
            $date_from = MyFormat::dateDmyToYmdForAllIndexSearch($this->date_from);
//            $criteria->addCondition("DATE(t.created_date) >= '$date_from'");
            DateHelper::searchGreater($date_from, 'created_date_bigint', $criteria);
        }
        if(!empty($this->date_to)){
            $date_to = MyFormat::dateDmyToYmdForAllIndexSearch($this->date_to);
//            $criteria->addCondition("DATE(t.created_date) <= '$date_to'");
            DateHelper::searchSmaller($date_to, 'created_date_bigint', $criteria, true);
        }
        if(!empty($this->buying)){
            if($this->buying == UsersExtend::BUYING_DA_MUA){
//                $criteria->addInCondition("t.id", Sell::ccsGetCustomerBuy($mUserLogin));
                $sParamsIn = implode(',', Sell::ccsGetCustomerBuy($mUserLogin));
                if(!empty($sParamsIn)){
                    $criteria->addCondition("t.id IN ($sParamsIn)");
                }
            }else{
//                $criteria->addNotInCondition("t.id", Sell::ccsGetCustomerBuy($mUserLogin));
                $sParamsIn = implode(',', Sell::ccsGetCustomerBuy($mUserLogin));
                if(!empty($sParamsIn)){
                    $criteria->addCondition("t.id NOT IN ($sParamsIn)");
                }
            }
        }
    }
    
    /**
     * @Author: ANH DUNG Sep 19, 2016
     * @Todo: get same criteria of ccs
     */
    public function ccsSameCriteria(&$criteria, $mUserLogin) {
        $criteria->compare('t.created_by', $mUserLogin->id);
        $criteria->compare('t.is_maintain', STORE_CARD_HGD_CCS);
        $criteria->compare('t.role_id', ROLE_CUSTOMER);
        $criteria->compare('t.type', CUSTOMER_TYPE_STORE_CARD);
    }
    
    /**
     * @Author: ANH DUNG Sep 19, 2016
     * count số điểm của ccs trong ngày
     */
    public function ccsCountToday($mUserLogin) {
        $criteria = new CDbCriteria();
        $this->date_from    = date('d-m-Y');
        $this->date_to      = date('d-m-Y');
        $this->ApiListingCriteria($criteria, $mUserLogin);
        return Users::model()->count($criteria);
    }
    public function ccsCountAll($mUserLogin) {
        $criteria = new CDbCriteria();
        $this->ccsSameCriteria($criteria, $mUserLogin);
        return Users::model()->count($criteria);
    }
    
    /** @Author: ANH DUNG Sep 04, 2018
     *  @Todo: get list KH Bò Mối không lấy hàng trong n ngày
     *  @param: $daysCheck số ngày cần kiểm tra ko lấy hàng
     *  KH cty chuyển A Vinh
     **/
    public function getCustomerStopOrder($daysCheck, &$criteria) {
        $criteria->addCondition("DATE_ADD(last_purchase, INTERVAL $daysCheck DAY) < CURDATE() AND last_purchase IS NOT NULL ");
        $criteria->compare('channel_id', Users::CON_LAY_HANG);
        $aTypeBoMoi = array(STORE_CARD_KH_BINH_BO, STORE_CARD_KH_MOI);
        $criteria->addInCondition( 'is_maintain', $aTypeBoMoi);
        return Users::model()->findAll($criteria);
    }
    
    /**
     * @Author: ANH DUNG Dec 22, 2015
     * @Todo: set không lấy hàng với những KH không lấy hàng quá 60 ngày
     * @note: run OK on Dec 22, 2015 - done 1148 KH
     */
    public function moveToKhongLayHang() {
        $daysCheck  = 60; 
        $criteria   =new CDbCriteria;
        $models     = $this->getCustomerStopOrder($daysCheck, $criteria); //Jun 09, 2016 tắt set status Ngưng Bảo Trì Tự động đi
        $aUpdate    = array('channel_id' => Users::KHONG_LAY_HANG);
        $CountData = count($models);
//        echo "Khong lay Hang: $CountData ----";
        // Jun 08, 2016 fix change status bảo trì định kỳ cho KH ko lấy hàng
        GasUphold::setStatusUpholdSchedule($models);// Jan0318 mở lại để chị Hạnh tính Đúng
        $mUphold = new GasUphold();
        $mUphold->lockCustomerChanHang();
        // Jun 08, 2016 fix change status bảo trì định kỳ cho KH ko lấy hàng
        Users::model()->updateAll($aUpdate, $criteria);
        $ResultRun = "CRON chuyển KH sang trạng thái Không Lấy hàng moveToKhongLayHang : ".$CountData.' done ';
        if($CountData){
            Logger::WriteLog($ResultRun);
        }
        // Jan1818 reEnableUphold
        $model = new GasUphold();
        $model->reEnableUphold();
    }
    
    /**
     * @Author: ANH DUNG Mar 01, 2017
     * @Todo: get customer by company
     * @Param: 
     */
    public static function getCustomerByCompany($storehouse_id) {
        if(empty($storehouse_id)){
            return [];
        }
        $roleCustomer       = ROLE_CUSTOMER;
        $criteria = new CDbCriteria();
        $criteria->addCondition('t.storehouse_id='.$storehouse_id);
        $sParamsIn          = implode(',', CmsFormatter::$aTypeIdQuotes);
        $condition          = "t.role_id=$roleCustomer AND t.is_maintain IN ($sParamsIn) ";
        $criteria->addCondition($condition);
        $models = Users::model()->findAll($criteria);
        $aRes = [];
        foreach($models as $item){
            $aRes[$item->id] = $item->getAttributes();;
        }
        return $aRes;
    }
    
    /**
     * @Author: ANH DUNG Feb 26, 2017
     * @Todo: find KH bò mối còn lấy hàng
     */
    public static function getCustomerConLayHang($needMore = []) {
        $criteria = new CDbCriteria();
        $roleCustomer       = ROLE_CUSTOMER; $aRes = [];
        $statusLayHang      = Users::CON_LAY_HANG;
        $sParamsIn          = implode(',', CmsFormatter::$aTypeIdQuotes);
        $condition          = "t.role_id=$roleCustomer AND t.is_maintain IN ($sParamsIn) AND t.channel_id=$statusLayHang";
        $criteria->addCondition($condition);
        if(isset($needMore['NotEmptyEmail'])){
            $criteria->addCondition("t.email<>'' AND t.email IS NOT NULL ");
            return Users::model()->findAll($criteria);
        }
        
        $models = Users::model()->findAll($criteria);
        foreach($models as $model){
            $aRes[$model->id] = $model;
        }
        return $aRes;
    }
    
    /**
     * @Author: ANH DUNG Feb 26, 2017
     * @Todo: run by cron to gen báo giá qua email và sms
     */
    public function makeQuotes() {
        ini_set('memory_limit','2000M');
        $this->makeQuotesSMS();
        $this->makeQuotesEmail();
    }
    
    /**
     * @Author: ANH DUNG Mar 01, 2017
     * @Todo: get model UsersPhoneExt2 for test
     */
    public function testGetRecordForSMS() {
        $criteria = new CDbCriteria();
        $criteria->addInCondition('t.user_id', $this->testGetUserId());
        return UsersPhoneExt2::model()->findAll($criteria);
    }
    public function testGetRecordForEmail() {
        $criteria = new CDbCriteria();
        $criteria->addInCondition('t.id', $this->testGetUserId());
        return Users::model()->findAll($criteria);
    }
    public function testGetUserId() {
        return [65354, 850317];// live
//        return [852332, 852335];// local
    }
    /**
     * @Author: ANH DUNG Feb 26, 2017
     * @Todo:     
    /* 1. xử lý cron không send hết tất cả SMS, những cái nào có thời gian gửi luôn thì mới send -- done
     * 2. xử lý build record và content của SMS và insert vào db
     * 3. xử lý cron send SMS
     * 
     * 4. Xử lý buid record và content email báo giá và insert vào db
     * 5. xử lý cron send mail, không send những mail dc hẹn giờ gửi sau -- done
     */
    public function makeQuotesSMS() {
        $from = time();
        try {
//            ini_set('memory_limit','500M');
            /** 1. get all record thỏa mã còn lấy hàng
             *  2. foreach rồi findbyPk từng model User 1 để build 1 sql
             *  3. run 1 sql
             */
            // 1. get all record thỏa mã còn lấy hàng
            $mUsersPhoneExt2    = new UsersPhoneExt2();
            $aModelPhone        = $mUsersPhoneExt2->getRecordBoMoi();
            /**  Chưa xử lý KH hệ thống
             * @note: Feb 28, 2017 với KH hệ thống phần SMS đã xử lý, chỉ cần nhập số đt là gửi bình thường
             * vì tin sms không có ghi tên KH nên không care chuyện name KH
             * - Còn với email thì phải xử lý bỏ Trụ Sở Chính ở tên KH đi.
             * - Giá KH trụ sở chỉnh setup giống giá của các chi nhánh
             */
            // Mar012017 begin for test
//            $aModelPhone = $this->testGetRecordForSMS();
            // Mar012017 end for test
            
            $mGasScheduleSms = new GasScheduleSms();
            $mGasScheduleSms->doInsertMultiQuotes($aModelPhone);
            // free memmory - How can I clear the memory while running a long PHP script? tried unset() ==> http://stackoverflow.com/questions/10544779/how-can-i-clear-the-memory-while-running-a-long-php-script-tried-unset
            $aModelPhone = null;
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
        $to = time();
        $second = $to-$from;
        $info = "makeQuotesSMS done in: $second  Second  <=> ".($second/60)." Minutes ".date('Y-m-d H:i:s');
        Logger::WriteLog($info);
        SendEmail::bugToDev($info);
    }
    public function makeQuotesEmail() {
        $from = time();
        try {
//            ini_set('memory_limit','500M');
            /** 1. gen all pdf báo giá
             *  2 .get all record thỏa mãn còn lấy hàng và có email
             *  3. find và foreach để insert
             *  4. run 1 sql
             */
            $this->cronGenPdfQuote();

            $aCustomer      = UsersExtend::getCustomerConLayHang(['NotEmptyEmail'=>1]);
            
            // Mar012017 begin for test
//            $aCustomer = $this->testGetRecordForEmail();
            // Mar012017 end for test
            
            GasScheduleEmail::makeQuotes($aCustomer);
            // clear the memory while running a long PHP script
            $aCustomer = null;
            
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
        $to = time();
        $second = $to-$from;
        $info = "run function makeQuotesEmail PDF EMAIL done in: $second  Second  <=> ".($second/60)." Minutes";
        Logger::WriteLog($info);
        SendEmail::bugToDev($info);
    }
    
    /**
     * @Author: ANH DUNG Mar 01, 2017
     * @Todo: sinh tự động hết các file pdf báo giá sau khi copy price Bò Mối Xong
     */
    public function cronGenPdfQuote() {
        $from = time(); $aErrors = [];
        $month = date('m'); $year = date('Y');
        $aCustomer      = UsersExtend::getCustomerConLayHang(['NotEmptyEmail'=>1]);
        $gPriceInMonth  = UsersPrice::getPriceOfListCustomer($month, $year, []);
        if(count($gPriceInMonth) < 1){
            $info = "Bug need fix cronGenPdfQuote Chưa setup giá";
            Logger::WriteLog($info);
            SendEmail::bugToDev($info);
            return ;
        }
        
        $aCompanyAllow = [GasConst::COMPANY_DKMN, GasConst::COMPANY_HUONGMINH];
        $mAppCache      = new AppCache();
//        $aCompany       = $mAppCache->setUserByRole(ROLE_COMPANY);
        $aCompany       = $mAppCache->getUserByRole(ROLE_COMPANY);
        $aUserTest      = $this->testGetUserId();
        foreach($aCustomer as $mUser){
//            if( !in_array($mUser->id, $aUserTest)){
//                continue;// only for test -- please recheck when
//            }
            ToPdf::customerQuotes($mUser, $aCompany, $gPriceInMonth, $aErrors);
        }
        
        if(count($aErrors)){
            $info = "cronGenPdfQuote Fix Gấp chưa setup giá cho các KH sau: ".  implode(' - ', $aErrors);
            Logger::WriteLog($info);
            SendEmail::bugToDev($info);    
        }

        $to = time();
        $second = $to-$from;
        $info = "cronGenPdfQuote done in: $second  Second  <=> ".($second/60)." Minutes";
        Logger::WriteLog($info);
        SendEmail::bugToDev($info);
    }
    
    /**
     * @Author: ANH DUNG Jun 21, 2017
     * @Todo: send thông báo đến toàn bộ KH bò mối theo 1 nội dung: vd send đổi số đt 19001521
    /* 1. xử lý cron không send hết tất cả SMS, những cái nào có thời gian gửi luôn thì mới send -- done
     * 2. xử lý build record và content của SMS và insert vào db
     * 3. xử lý cron send SMS
     * @param: $title: nội dung thông báo
     * @title Run 1: Gas 24h thong bao: Ke tu ngay 1/7/2017 tat ca cac dau so dien thoai chung toi se thuc hien thay doi sang tong dai moi 19001521\nXin cam on quy khach hang
     */
    public function makeAnnounceSMS($title) {
        $from = time();
        try {
            ini_set('memory_limit','1500M');
            /** 1. get all record thỏa mã còn lấy hàng
             *  2. foreach rồi findbyPk từng model User 1 để build 1 sql
             *  3. run 1 sql
             */
            // 1. get all record thỏa mã còn lấy hàng
            $mUsersPhoneExt2    = new UsersPhoneExt2();
            $aModelPhone        = $mUsersPhoneExt2->getRecordBoMoi();
            /**  Chưa xử lý KH hệ thống
             * @note: Feb 28, 2017 với KH hệ thống phần SMS đã xử lý, chỉ cần nhập số đt là gửi bình thường
             * vì tin sms không có ghi tên KH nên không care chuyện name KH
             * - Còn với email thì phải xử lý bỏ Trụ Sở Chính ở tên KH đi.
             * - Giá KH trụ sở chỉnh setup giống giá của các chi nhánh
             */
            $mGasScheduleSms = new GasScheduleSms();
            $mGasScheduleSms->doInsertMultiAnnounce($aModelPhone, $title);
            // free memmory - How can I clear the memory while running a long PHP script? tried unset() ==> http://stackoverflow.com/questions/10544779/how-can-i-clear-the-memory-while-running-a-long-php-script-tried-unset
            $aModelPhone = null;
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
        $to = time();
        $second = $to-$from;
        $info = "makeAnnounceSMS done in: $second  Second  <=> ".($second/60)." Minutes";
        Logger::WriteLog($info);
        SendEmail::bugToDev($info);
    }
    
    /**
     * @Author: ANH DUNG Mar 25, 2017
     * @Todo: get các KH trong chuỗi của 1 hệ thống
     */
    public static function getByParentId($parent_id, $needMore=[]) {
        if(empty($parent_id)){
            return [];
        }
        $criteria = new CDbCriteria();
        $aTypeBoMoi = array(STORE_CARD_KH_BINH_BO, STORE_CARD_KH_MOI);
        $criteria->addInCondition('t.is_maintain', $aTypeBoMoi); // Aug 19, 2014 chỉ update KH bò mối thôi, vì cái 
        $criteria->addCondition('t.parent_id='.$parent_id .' AND t.role_id='.ROLE_CUSTOMER);
        return Users::model()->findAll($criteria);
    }
    
    /**
     * @Author: ANH DUNG Apr 09, 2017
     * @Todo: Get list NPP
     */
    public static function getListDistributor($needMore = []) {
        $criteria = new CDbCriteria();
        $criteria->addCondition('t.is_maintain='.UsersExtend::STORE_CARD_NCC .' AND t.role_id='.ROLE_CUSTOMER);
        if(!isset($needMore['GetAll'])){
            $criteria->addCondition('t.status=' . STATUS_ACTIVE);
        }
        return Users::model()->findAll($criteria);
    }
    
    /**
     * @Author: ANH DUNG Apr 15, 2017
     * @Todo: get list user id NVPV KH lưu động, dc phép change agent trên APP
     */
    public static function getUidAllowChangeAgent() {
        return [
            1162980, // Võ Văn Nha Sep1717
//            1024391, // Châu Đức Nghĩa
//            1036180, // Nguyễn Văn Sĩ Apr 20, 17
//            1042713, // Anh Vợ Nguyễn Huy Hoàn
//            1055552, // Đặng Phúc Cường
//            1052878, // Nguyễn Quốc Hoàng Lưu động BD Jun12
        ];
    }
    
    /**
     * @Author: ANH DUNG Feb 11, 2017 UsersExtend::getAgentRunAppGN()
     * @Todo: get id những agent đang chạy app giao nhận để xử lý notify
     */
    public static function getAgentRunAppGN() {
//        return []; // only dev test
        return [
            1315, // for dev test Cửa hàng 5
//            106, // Đại lý Bình Thạnh 1
            658920, // Đại lý Bình Thạnh 2
            242369, // Đại lý quận 10
            945418, // Đại Lý Quận 4.2
            868845, // Đại Lý Gò Vấp 2
            768408, // Đại Lý Bình Thạnh 3
            768409, // Đại Lý Thủ Đức 2
            103,    // Đại lý Quận 8.1
            102,    // Đại lý Quận 7
            126,    // Đại lý Tân Phú
            108,    // Đại lý Hóc Môn
            122,// Đại lý Bình Tân
            27740,// Đại lý Tân Bình 2 Jun13
            120,// Đại lý Tân Sơn
            211393,// Đại Lý Quận 6
            113, // Đại lý Quận 3
            27740, // Đại lý Tân Bình 2
            
            1052166,// Đại Lý An Nhơn
            1048571,// Đại Lý Cà Mau Petro
            1054994,// Đại Lý Lê Duẩn
            // Đồng Nai **********
            123,// Đại lý Trảng Dài
            114,// Đại lý Long Bình Tân
            116, // Đại lý Bình Đa
            121,// Đại lý Ngã Ba Trị An
            507991,// Đại lý Đông Hòa
            392141, //  Đại lý Long Thành
            // Bình Dương **************
            119,// Đại lý Tân Định
            110, //Đại lý Thủ Dầu Một
            268833, //  Đại Lý Phú Hòa
            1210, // Đại Lý Thuận Giao
            357031,// Đại lý Tân Phước Khánh 
            375037, // Đại lý Thái Hòa
            109, // Đại lý Lái Thiêu
            1457, // Đại lý Đồng An
            115, // Đại lý Dĩ An
            1079730,// Đại Lý Bạc Liêu
            
//            1311, // Cửa hàng 1
//            1313, // Cửa hàng 3
            1086796, // Đại Lý Nguyễn Văn Cừ - Gia Lai 
//            268835, // Cửa hàng Chư Sê
            1120456,// Đại Lý Hai Bà Trưng Kon Tum 
//            28941,// Cửa hàng Thi Sách
            1070036,// Đại Lý Tam Bình
            
            // Aug2217
            197436, // Cửa Hàng Phú Thuận
            30751, // Cửa hàng Vĩnh Long 1 - Đại Lý
            148404, // - Cửa hàng Vũng Liêm - Đại Lý
            171449, // - Cửa hàng Trà Vinh - Đại Lý
            652695, // - Cửa Hàng Bình Minh - Đại Lý
            30753, //- Cửa hàng Cần Thơ 1 - Đại Lý
            138544, // - Cửa hàng Cần Thơ 2 - Đại Lý
            262526, // - Cửa Hàng Ô Môn - Đại Lý
            
        ];
    }
    
    public static function getAgentForwardPhone() {
//        return []; // only dev test
        return [
            1315, // for dev test Cửa hàng 5
            106, // Đại lý Bình Thạnh 1
            658920, // Đại lý Bình Thạnh 2
            242369, // Đại lý quận 10
            945418, // Đại Lý Quận 4.2
            868845, // Đại Lý Gò Vấp 2
            768408, // Đại Lý Bình Thạnh 3
            768409, // Đại Lý Thủ Đức 2
            103,    // Đại lý Quận 8.1
            102,    // Đại lý Quận 7
            126,    // Đại lý Tân Phú
            108,    // Đại lý Hóc Môn
            122,// Đại lý Bình Tân
            27740,// Đại lý Tân Bình 2 Jun13
            120,// Đại lý Tân Sơn
            211393,// Đại Lý Quận 6
            113, // Đại lý Quận 3
            27740, // Đại lý Tân Bình 2
            
            1052166,// Đại Lý An Nhơn
            1048571,// Đại Lý Cà Mau Petro
            1054994,// Đại Lý Lê Duẩn
            // Đồng Nai **********
            123,// Đại lý Trảng Dài
            114,// Đại lý Long Bình Tân
            116, // Đại lý Bình Đa
            121,// Đại lý Ngã Ba Trị An
            507991,// Đại lý Đông Hòa
            392141, //  Đại lý Long Thành
            // Bình Dương **************
            119,// Đại lý Tân Định
            110, //Đại lý Thủ Dầu Một
            268833, //  Đại Lý Phú Hòa
            1210, // Đại Lý Thuận Giao
            357031,// Đại lý Tân Phước Khánh 
            375037, // Đại lý Thái Hòa
            109, // Đại lý Lái Thiêu
            1457, // Đại lý Đồng An
            115, // Đại lý Dĩ An
            // ----------------------------------
            1058061, // - Đại Lý Thủ Đức 3
            111, // - Đại lý Bến Cát 
            137638, // - Đại lý Xưởng Phước Tân
            100, //Đại lý Quận 2
            105,//- Đại lý Quận 9
            104, // Đại lý Quận 8.2
            
            910247,// Đại Lý Quy Nhơn
            30753 ,// Cửa hàng Cần Thơ 1
            138544,// Cửa hàng Cần Thơ 2
            1070036, // Đại Lý Tam Bình
            939720, // Đại Lý Bình Chuẩn 
            367242, // Đại lý Hố Nai 
            117, // Đại lý Thống Nhất 
            572832, // Đại lý Bình Đường 
            
            1079730,// Đại Lý Bạc Liêu
            
            1311, // Cửa hàng 1
            1313, // Cửa hàng 3
            1086796, // Đại Lý Nguyễn Văn Cừ - Gia Lai 
            268835, // Cửa hàng Chư Sê
            1120456,// Đại Lý Hai Bà Trưng Kon Tum 
            28941,// Cửa hàng Thi Sách
            652695, // Cửa Hàng Bình Minh 
            1070036,// Đại Lý Tam Bình
            
        ];
    }
    
    
    /**
     * @Author: ANH DUNG May 09, 2017
     * @Todo: get id những agent chạy app giao nhận - go live
     */
    public static function getAgentRunAppCashbook() {
        return [
//            106, // Đại lý Bình Thạnh 1
            658920, // Đại lý Bình Thạnh 2
//            242369, // Đại lý quận 10
//            945418, // Đại Lý Quận 4.2
//            868845, // Đại Lý Gò Vấp 2
            
//            103,// Đại lý Quận 8.1
//            106,// Đại lý Bình Thạnh 1
//            27740,// Đại lý Tân Bình 2
//            100,// Quan 2
//            768408,// Đại Lý Bình Thạnh 3
//            122,// Đại lý Bình Tân
//            102,// Đại lý Quận 7
//            120,// Đại lý Tân Sơn
//            126,// Đại lý Tân Phú
//            105,// Đại lý Quận 9
//            211393,// Đại Lý Quận 6
//            114,// Đại lý Long Bình Tân

        ];
    }
    
    /** @Author: ANH DUNG Jun 21, 2017
     * @Todo: những đại lý khoán của A Hiếu nếu hủy đơn hàng sẽ gửi notify cho anh Hiếu
     */
    public static function getAgentAnhHieu() {
        return [
            // Jun0218 Anh Hieu chuyen len SG
            // 1. Bình BT1 BT2 BT3 Q2
            // 2. Huy TD1 TD2 TD3 Q2 Q9
            106, 658920, 768408, 100, 112, 768409, 1058061, 1372582, 105
//            101, 
//            112,
//            235,
//            945621,
//            100,
//            102,
//            103,
//            104,
//            105,
//            106,
//            107,
//            108,
//            113,
//            120,
//            122,
//            126,
//            27740,
//            211393,
//            242369,
//            658920,
//            768408,
//            768409,
//            868845,
//            945418,
//            1058061,
//            1372582,
//            1429436,
        ];
    }
    
    public static function getAgentGsPhuc() {// Jun1518 các ĐL HV Phúc
        return [109,110,111,115,118,119,1210,1457,268833,314408,357031,375037,572832,758528,939720,971240,1524018,
            114,116,117,121,123,137638,367242,392141,507991,916044,1284630,1294352,1317722,1356862,1359871];
    }
    
    
    /** @Author: ANH DUNG Jun 21, 2017
     * @Todo: những đại lý khoán của A Hiếu nếu hủy đơn hàng sẽ gửi notify cho anh Hiếu
     */
    public static function getAgentAutoGenStorecard() {
        return [
            106, // Đại lý Bình Thạnh 1
        ];
    }
    public static function getAgentNotRunApp() {
        return [
//            118, // Đại lý An Thạnh - remove Dec1017
//            919016, // Đại Lý Lê Phụng Hiểu - Jun1318
//            952187, // Đại Lý Bến Nôm  - Jun1318
//            1011084, // Đại Lý Trần Phú  - Jun1318
//            1127081,// Đại Lý Dương Quảng Hàm - Nha Trang
//            898894,// Cửa Hàng Gas Minh Hạnh 
//            863146,// Cửa Hàng Chợ Mới 2 - Đại Lý
//            1179069,// Đại Lý Diên Khánh - Khánh Hòa
//            1205656,// Đại Lý Cam Ranh
//            1176208,// Đại Lý Vĩnh Thạnh - Nha Trang
            GasCheck::DL_XUONG_DONG_NAI,
        ];
    }
    /** @Author: Pham Thanh Nghia Oct 8, 2018
     *  @Todo: danh sach NV ko post 
     **/
    public function searchListUserNotPost(){
        $model = new HrSalaryReports();
        $mGasIssueTicketsDetail = new GasIssueTicketsDetail();
        $criteria= new CDbCriteria;
        $criteria->compare('t.id', $this->id);
        $criteria->compare('t.role_id', $this->role_id);
        $aCondition = array_keys($model->getArrayRoleSalary());
        $criteria->addInCondition("t.role_id",$aCondition);
        $sParamsIn = implode(',', $mGasIssueTicketsDetail->getListUserPost());
        $criteria->addCondition("t.id NOT IN ($sParamsIn)");
        $criteria->compare('t.status', STATUS_ACTIVE);
        $criteria->order = 't.id DESC';
        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
            'pagination'=>array(
                'pageSize'=> $this->pageSize,
            ),
        ));
    }
    /** @Author: PHAM THANH NGHIA Oct 10, 2018 -- handle info profile api */
    public function handleApiInfoProfile(&$result, $q){
        $aRecord = $this->formatAppItemInfoProfile($q);
        if(!empty($aRecord)){
            $result['message']  = 'L';
            $result['record']        = $this->formatAppItemInfoProfile($q);
        }else{
            $result = ApiModule::$defaultResponse;
            $result['message']  = 'Mã không hợp lệ';
        }
    }
    public function formatAppItemInfoProfile($q) {
        $temp = $list = [];
        $criteria = new CDbCriteria;
        $criteria->compare('code_account', $q->code_account);
        $model = Users::model()->find($criteria);
        if(!empty($model)){
            $role_name = Roles::model()->findByPk($model->role_id)->role_name;
            $model->LoadUsersRefImageSign();
            $list = ImageProcessing::bindImageByModel($model->mUsersRef,'','',array('size'=>'size3'));
            $temp['link_image'] = $list;
            $temp['id']                 = $model->id;
            $temp['code_account']       = $model->code_account;
            $temp['first_name']         = $model->first_name;
            $temp['role_id']            =  $role_name;
        }
        return $temp;
    }
    
    /** @Author: DuongNV Oct0919
     *  @Todo: api get list agent for daukhimiennam.com created from to
     *  @Param: province_id, district_id, date_from, date_to Y-m-d
     **/
    public function apiGetListAgent(&$result, $pageSize, $page = 0) {
        $dataProvider           = $this->getDataProviderListAgent($pageSize, $page);
        $models                 = $dataProvider->data;
        $CPagination            = $dataProvider->pagination;
        $result['total_record'] = $CPagination->itemCount;
        $result['total_page']   = $CPagination->pageCount;
        $result['current_page'] = $page;
        $result['page_size']    = $pageSize;
        $aData                  = [];
        foreach ($models as $mAgent) :
            $aData[]            = [
                'id'            => $mAgent->id,
                'first_name'    => $mAgent->first_name,
                'address'       => $mAgent->address,
                'lat_long'      => $mAgent->slug,
            ];
        endforeach;
        $result['record'] = $aData;
    }
    
    /** @Author: DuongNV Oct0919
     *  @Todo: api get list agent for daukhimiennam.com created from to
     *  @Param: province_id, district_id, date_from, date_to Y-m-d
     **/
    public function getDataProviderListAgent($pageSize, $page = 0) {
        $criteria=new CDbCriteria;
        $criteria->compare('t.role_id', ROLE_AGENT);
        $criteria->compare('t.province_id', $this->province_id);
        $criteria->compare('t.district_id', $this->district_id);
        $criteria->compare('t.status', STATUS_ACTIVE);
        
        $mMyFormat = new MyFormat();
        if(!empty($this->date_from) && $mMyFormat->isValidLength($this->date_from, 10)){
            $dateFrom = date('Y-m-d', strtotime($this->date_from.''));
            MyFormat::isValidDate($dateFrom);
            $criteria->addCondition("DATE(t.created_date) >= '$dateFrom'");
        }
        if(!empty($this->date_to) && $mMyFormat->isValidLength($this->date_from, 10)){
            $dateTo = date('Y-m-d', strtotime($this->date_to.''));
            MyFormat::isValidDate($dateTo);
            $criteria->addCondition("DATE(t.created_date) <= '$dateTo'");
        }
        $sort = new CSort();
        $sort->defaultOrder = 't.id DESC';
        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
            'pagination' => array(
                'pageSize'      => $pageSize,
                'currentPage'   => $page
            ),                    
            'sort' => $sort,
        ));
    }
    
}