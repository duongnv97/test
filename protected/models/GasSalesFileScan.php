<?php

/**
 * This is the model class for table "{{_gas_sales_file_scan}}".
 *
 * The followings are the available columns in table '{{_gas_sales_file_scan}}':
 * @property string $id
 * @property string $code_no
 * @property string $agent_id
 * @property string $uid_login
 * @property string $date_sales
 * @property string $accounting_employee_id
 * @property string $created_date
 * @property string $last_update_by
 * @property string $last_update_time
 */
class GasSalesFileScan extends BaseSpj
{
    public $aModelDetail;
    public $mDetail; // dùng để lưu aIdNotIn cho phần update xóa record detail đi
    public $MAX_ID;
    public $date_from;
    public $date_to;        
    // lấy số ngày cho phép đại lý cập nhật
    public static function getDayAllowUpdate(){
        return Yii::app()->params['PTTT_SELL_update_file_scan'];
    }

    public static function model($className=__CLASS__)
    {
            return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
            return '{{_gas_sales_file_scan}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
            return array(
                array('date_sales, accounting_employee_id', 'required'),                                                            
                array('note,id, code_no, agent_id, uid_login, date_sales, accounting_employee_id, created_date, last_update_by, last_update_time', 'safe'),
                array('date_from,date_to', 'safe'),
            );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
            return array(
                'rAgent' => array(self::BELONGS_TO, 'Users', 'agent_id'),
                'rFileScanDetail' => array(self::HAS_MANY, 'GasSalesFileScanDetai', 'file_scan_id'),
                'rUidLogin' => array(self::BELONGS_TO, 'Users', 'uid_login'),
                'rAccountingEmployee' => array(self::BELONGS_TO, 'Users', 'accounting_employee_id'),
                'rLastUpdateBy' => array(self::BELONGS_TO, 'Users', 'last_update_by'),
            );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
            return array(
                    'id' => 'ID',
                    'code_no' => 'Mã Số',
                    'agent_id' => 'Đại Lý',
                    'uid_login' => 'Uid Login',
                    'date_sales' => 'Ngày Bán Hàng',
                    'accounting_employee_id' => 'NV Kế Toán',
                    'created_date' => 'Ngày Tạo',
                    'last_update_by' => 'Sửa Lần Cuối',
                    'last_update_time' => 'Ngày Sửa',
                    'note' => 'Ghi Chú',
                    'date_from' => 'Từ Ngày',
                    'date_to' => 'Đến Ngày',                
            );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
            $criteria=new CDbCriteria;

            $criteria->compare('t.code_no',$this->code_no,true);
            $criteria->compare('t.note',$this->note,true);
            $criteria->compare('t.agent_id',$this->agent_id);
            $criteria->compare('t.uid_login',$this->uid_login);
            $criteria->compare('t.accounting_employee_id',$this->accounting_employee_id);

//            if(!empty($this->date_sales)){
//                $this->date_sales = MyFormat::dateDmyToYmdForAllIndexSearch($this->date_sales);
//                $criteria->compare('t.date_sales',$this->date_sales);
//            }

            if(!empty($this->created_date)){
                $this->created_date = MyFormat::dateDmyToYmdForAllIndexSearch($this->created_date);
                $criteria->compare('t.created_date',$this->created_date,true);
            }
            
            
            $date_from = '';
            $date_to = '';
            if(!empty($this->date_from)){
                $date_from = MyFormat::dateDmyToYmdForAllIndexSearch($this->date_from);
            }
            if(!empty($this->date_to)){
                $date_to = MyFormat::dateDmyToYmdForAllIndexSearch($this->date_to);
            }
            if(!empty($date_from) && empty($date_to))
                    $criteria->addCondition("t.date_sales>='$date_from'");
            if(empty($date_from) && !empty($date_to))
                    $criteria->addCondition("t.date_sales<='$date_to'");
            if(!empty($date_from) && !empty($date_to))
                    $criteria->addBetweenCondition("t.date_sales",$date_from,$date_to);            
            
            
            if(Yii::app()->user->role_id==ROLE_SUB_USER_AGENT){
                $criteria->compare('t.agent_id', MyFormat::getAgentId());
            }else{
                if(!empty($this->agent_id)){                        
                    $criteria->compare('t.agent_id', $this->agent_id);
                }
                GasAgentCustomer::addInConditionAgent($criteria, 't.agent_id');
            }                

            $sort = new CSort();

            $sort->attributes = array(
                'code_no'=>'code_no',
                'agent_id'=>'agent_id',
                'date_sales'=>'date_sales',
                'accounting_employee_id'=>'accounting_employee_id',
                'created_date'=>'created_date',                
            );    
            $sort->defaultOrder = 't.id desc';                
            return new CActiveDataProvider($this, array(
                'criteria'=>$criteria,
                'sort' => $sort,
                'pagination'=>array(
                    'pageSize'=> Yii::app()->user->getState('pageSize',Yii::app()->params['defaultPageSize']),
                ),
            ));
    }

    public function defaultScope()
    {
        return array(
                //'condition'=>'',
        );
    }
    
    protected function beforeSave() {
        if(strpos($this->date_sales, '/')){
            $this->date_sales = MyFormat::dateConverDmyToYmd($this->date_sales);
            MyFormat::isValidDate($this->date_sales);
        }
        if($this->isNewRecord){
            $this->agent_id = MyFormat::getAgentId();
            $this->uid_login = Yii::app()->user->id;
            $this->code_no = MyFunctionCustom::getNextId('GasSalesFileScan', 'E'.date('y'), LENGTH_TICKET,'code_no');
        }else{
            $this->last_update_by = Yii::app()->user->id;
            $this->last_update_time = date('Y-m-d H:i:s');
        }
        return parent::beforeSave();
    }    

    protected function beforeDelete() {
        GasSalesFileScanDetai::deleteByFileScanId($this->id);
        return parent::beforeDelete();
    }    
    
}