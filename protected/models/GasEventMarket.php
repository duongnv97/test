<?php

class GasEventMarket extends BaseSpj
{ 
    const DATE_BQV_EVENT    = 60;
    
    public $autocomplete_name_1, $autocomplete_name_2, $autocomplete_name_3, $autocomplete_name_4, $autocomplete_name;
    public $created_date, $start_date_from, $start_date_to, $end_date_from, $end_date_to;
    public $aEmployee  = array(); //
    public $app, $bqv,$emloyeeApprove,$submitMore;
    
    const AMOUNT_ROADSHOW       = 50000; // tiền 1 lần chạy roadshow là 50k
    const AMOUNT_MARKET         = 0; // tiền 1 lần chợ là 50k
    const AMOUNT_SUPERMARKET    = 0; // tiền 1 lần siêu thi là 50k
    const COUNT_APPLY_MONEY     = 3; // số lần tối thiểu để hiên thị nút duyệt cho NV roadshow
    const STATUS_ACTICE = 1;    //  status: 1: Hoạt động. 0: Không hoạt động
    const STATUS_INACTICE = 0;
    const STATUS_PAID = 2; // đã thanh toán tiền
    
    const TYPE_MARKET = 1;
    const TYPE_SUPERMARKET = 2;
    const TYPE_ROADSHOW = 3;
    
    const TEXT_CONTENT_SMS = 'RoadShow';
    public static function model($className=__CLASS__)
    {
            return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
            return '{{_event_market}}';
    }
    
//    danh sách status active
    public function getStatusActive(){
        return [
            self::STATUS_ACTICE,
            self::STATUS_PAID,
        ];
    }
    
//    Danh sách type tính KH Cty từ các hoạt động activation
    public function getTypeActivationMaket(){
        return [
            GasEventMarket::TYPE_MARKET,
            GasEventMarket::TYPE_SUPERMARKET
        ];
    }
    
    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        return array(
            array('employees, agents, start_date, end_date, type, province_id, district_id', 'required', 'on'=>"create, update"),
            array('id, title, employees, agents, start_date, end_date, note, status, created_by, created_date, start_date_from, start_date_to, end_date_from, end_date_to ', 'safe'),
            array('emloyeeApprove,submitMore,type, province_id, district_id, monitor_id, amount', 'safe'),
        );
    }
    
    public static function GetQtyCreateInDay($date) {
        $criteria = new CDbCriteria();
        $criteria->compare('t.created_date', $date, true);
        return self::model()->count($criteria);
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        return array(
            'rEmployee' => array(self::BELONGS_TO,'Users','employees'),
            'rMonitor' => array(self::BELONGS_TO,'Users','monitor_id'),
            'rCreatedBy' => array(self::BELONGS_TO, 'Users', 'created_by'),
            'rDetailEx' => array(self::HAS_MANY, 'GasEventMarketDetail', 'event_market_id','condition'=>'rDetailEx.type = '.GasEventMarketDetail::TYPE_MATERIAL_EXPORT),
            'rDetailIm' => array(self::HAS_MANY, 'GasEventMarketDetail', 'event_market_id','condition'=>'rDetailIm.type = '.GasEventMarketDetail::TYPE_MATERIAL_IMPORT),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'title' => 'Tiêu đề/ Tên chợ',
            'type' => 'Loại',
            'employees' => 'Nhân viên',
            'agents' => 'Đại lý',
            'start_date' => 'Ngày bắt đầu',
            'end_date' => 'Ngày kết thúc',
            'note' => 'Ghi chú/Tuyến đường chạy',
            'status' => 'Trạng thái',
            'created_date' => 'Ngày tạo',
            'created_by' => 'Người tạo',
            'start_date_from' => 'Từ ngày',
            'start_date_to' => 'Đến ngày',
            'end_date_from' => 'Từ ngày',
            'end_date_to' => 'Đến ngày',
            'province_id' => 'Tỉnh',
            'district_id' => 'Quận, huyện',
            'monitor_id' => 'Giám sát',
            'amount' => 'Số tiền thuê'
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        $criteria=new CDbCriteria;
        $criteria->compare('t.title',$this->title, true);
        $criteria->compare('t.status',$this->status);
        $criteria->compare('t.type',$this->type);
        $criteria->compare('t.district_id', $this->district_id);
        $criteria->compare('t.province_id', $this->province_id);
        
        $start_date_from = '';
        $start_date_to = '';
        if(!empty($this->start_date_from)){
            $start_date_from = strtotime($this->start_date_from);
        }
        if(!empty($this->start_date_to)){
            $start_date_to   = strtotime($this->start_date_to);
        }

        if(!empty($start_date_from) && empty($start_date_to)){
            $criteria->addCondition("t.start_date_bigint>='$start_date_from'");
        }
        if(empty($start_date_from) && !empty($start_date_to)){
            $criteria->addCondition("t.start_date_bigint<='$start_date_to'");
        }
        if(!empty($start_date_from) && !empty($start_date_to)){
            DateHelper::searchBetween($this->start_date_from, $this->start_date_to, 'start_date_bigint', $criteria, true);
        }
        $end_date_from = '';
        $end_date_to = '';
        if(!empty($this->end_date_from)){
            $end_date_from = strtotime($this->end_date_from);
        }
        if(!empty($this->end_date_to)){
            $endDateConvert = MyFormat::modifyDays($this->end_date_to, 1);
            $end_date_to = strtotime($endDateConvert);
        }
        if(!empty($end_date_from) && empty($end_date_to)){
            $criteria->addCondition("t.end_date_bigint>='$end_date_from'");
        }
        if(empty($end_date_from) && !empty($end_date_to)){
            $criteria->addCondition("t.end_date_bigint<='$end_date_to'");
        }
        if(!empty($end_date_from) && !empty($end_date_to)){
            DateHelper::searchBetween($this->end_date_from, $this->end_date_to, 'end_date_bigint', $criteria, true);
        }
        
        if(!empty($this->created_date)){
            $this->created_date = MyFormat::dateDmyToYmdForAllIndexSearch($this->created_date);
            $criteria->compare('t.created_date',$this->created_date,true);
        }
        
        $criteria->compare('t.status',$this->status);
        $criteria->order = " t.status DESC, t.id DESC ";
        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
            'pagination'=>array(
                'pageSize'=> 50,
            ),
        ));
    }

           
    public function initCreateEventMarket(&$model) {
        $model->created_by = MyFormat::getCurrentUid();
        $model->status = 1;
    }
    
    public function getArrayStatus(){
        return[
        GasEventMarket::STATUS_ACTICE => 'Hoạt động',
        GasEventMarket::STATUS_INACTICE => 'Không hoạt động',
        GasEventMarket::STATUS_PAID => 'Đã Chuyển Tiền',
        ];
    }
    
   
    public function getStatus(){
        $aStatus = $this->getArrayStatus();
        return isset($aStatus[$this->status]) ? $aStatus[$this->status] : "";
    }
    
    
    public function getEmployee() {
        $aEmployee = $this->rEmployee;
        return isset($aEmployee->first_name) ? $aEmployee->first_name : "";
    }
    
     public function getArrayType(){
        return[
        GasEventMarket::TYPE_MARKET => 'Chợ',
        GasEventMarket::TYPE_SUPERMARKET => 'Siêu thị',
        GasEventMarket::TYPE_ROADSHOW => 'RoadShow',
        ];
    }
    
    public function getType(){
        $aStatus = $this->getArrayType();
        return isset($aStatus[$this->type]) ? $aStatus[$this->type] : "";
    }
    
    public function getStartDate() {
        return MyFormat::dateConverYmdToDmy($this->start_date);
    }
    
    public function dateConvertDmYtoYmdHi($datetime, $syntax = '-')
    {
        if (empty($datetime))
            return '';
        $expl = explode(' ', $datetime);
        $date = explode($syntax, $expl[0]);
        if (count($expl) > 1 && count($date) > 2)
            return $date[2] . '-' . $date[1] . '-' . $date[0] . ' ' . $expl[1];
        return '';
    }


    public function getDataValidate(){
        if(isset($_POST['GasEventMarket']['employee'])){
            $aJson1 =  $this->customArray($_POST['GasEventMarket']['employee']);
            $this->employees = json_encode($aJson1);
        }
        if(isset($_POST['GasEventMarket']['agents'])){
            $this->agents = json_encode($_POST['GasEventMarket']['agents']);
        }
        $this->start_date           = MyFormat::datetimeToDbDatetime($this->start_date, '-');
        $this->end_date             = MyFormat::datetimeToDbDatetime($this->end_date, '-');
        $this->start_date_bigint    = strtotime($this->start_date);
        $this->end_date_bigint      = strtotime($this->end_date);
        $this->agents               = ($this->agents) ? $this->agents : '';
        $this->employees            = ($this->employees) ? $this->employees : '';
        $this->created_date = date('Y-m-d H:i:s');
    }
    
    
    public function wedGetPost() {
        if(!isset($_POST['GasEventMarket']['employee']) || !is_array($_POST['GasEventMarket']['employee'])){
            $this->addError('employees',"Danh sách nhân viên không hợp lệ");
            return ;
        }
        if(!isset($_POST['GasEventMarket']['agents']) || !is_array($_POST['GasEventMarket']['agents'])){
            $this->addError('agents',"Danh sách đại lí không hợp lệ");
            return ;            
         }
        $hasError = true; 
        $json = json_decode($this->employees, true);
        foreach($json as $key => $employee_id){
            $mDetail = Users::model()->findByPk($employee_id);
            $mDetail->validate();
           if(!$mDetail->hasErrors()){
                $hasError = false;
            }
        }
        if($hasError){
            $this->addError('employees',"Danh sách nhân viên không hợp lệ");
        }
        if($this->type == self::TYPE_ROADSHOW){
            $this->amount = self::AMOUNT_ROADSHOW;
        }
    }
    
    public function getTitle() {
        return $this->title;
    }
    
    public function getNote() {
        return $this->note;
    }
    
    public function getCreatedBy() {
        $mUser = $this->rCreatedBy;
        if($mUser){
            return $mUser->getFullName();
        }
        return '';
    }
    
    public function formatDataBeforeUpdate() {
        $this->start_date  = MyFormat::dateConverYmdToDmy($this->start_date, 'd-m-Y H:i:s');
        $this->end_date  = MyFormat::dateConverYmdToDmy($this->end_date, 'd-m-Y H:i:s');
        $this->created_date  = MyFormat::dateConverYmdToDmy($this->created_date,'d-m-Y H:i:s');
        $this->agents = json_decode($this->agents);
    }
    
    public function formatJson($aJson1, $aJson2 ){
      $aJson1 =  $this->customArray($aJson1);
      $this->employees = json_encode($aJson1);

      $this->agents = json_encode($aJson2);
    }
    public function formatDateForView($date){   
        return MyFormat::dateConverYmdToDmy($date, 'd/m/Y H:i:s');
    }
    
    public function formatCanSave() {
        $this->start_date = MyFormat::dateConverYmdToDmy($this->start_date,'d-m-Y H:i:s');
        $this->end_date = MyFormat::dateConverYmdToDmy($this->end_date,'d-m-Y H:i:s');
        if(isset($_POST['GasEventMarket']['agents']) && is_array($_POST['GasEventMarket']['agents'])){
            $this->agents = json_decode( $this->agents);
        }
    }
    
   
    
    public function customArray($aStr){
        $aResult = array();
        foreach ($aStr as $key => $value) {
            if(empty($value)){
                unset($aStr[$key]);
            }else{
                $aResult[] = $value;
            }
        }
        return $aResult;
    }
    
    public function getFormatJson($aModel) {
        $aJson = json_decode($aModel);
        $str = "<table cellpadding='0' cellspacing = '0' style = 'border : 1px solid black;'>";
        foreach ($aJson as $key => $value){
            $mEmployee = Users::model()->findByPk($value); 
            $str .= "<tr style = 'border : 1px solid black'>";
            $str .= "<td style = 'border : 1px solid black'>".($key+1)."</td>";
            if ($mEmployee) {
                $str .= "<td style = 'border : 1px solid black'>".$mEmployee->first_name."</td>";
            }else{
                $str .= "<td style = 'border : 1px solid black'>$value</td>";
            }
            $str .= "</tr>";
                }
        $str .= "</table>";
        return $str;
    }
    
    /** @Author: NGUYEN KHANH TOAN 2018
     *  @Todo:
     *  @Param:
     **/
    public function buildArrayEmployeeUpdate() {
        $json = json_decode($this->employees, true);
        if(is_array($json)){
            foreach($json as $key => $employee_id){
            $mDetail = Users::model()->findByPk($employee_id);
            $this->aEmployee[] = $mDetail;
            }
        }
    }
    
    /** @Author: NGUYEN KHANH TOAN 2018
     *  @Todo:kiểm tra nhân viên _ đại lý đang có đợt đi chợ: return true/false.,
     *      cập nhật trạng thái status nếu có end_date > ngày hiện tại.
     *  @Param:
     **/
    public function checkActive($employees_id,$agent_id) {
        $criteria = new CDbCriteria;
        $criteria->compare('status', GasEventMarket::STATUS_ACTICE);
        $aListActive =  GasEventMarket::model()->findAll($criteria);
        $cDate = date('Y-m-d H:i:s');
        foreach ($aListActive as $key => $value) {
            if(($value->end_date < $cDate)){
                $value->status = GasEventMarket::STATUS_INACTICE;
                $value->update();
                unset($aListActive[$key]);
        }};
        foreach ($aListActive as $key => $value) {
            if($value->start_date > $cDate){                
                continue;
            }
            $value->employees = json_decode($value->employees,true);
            $value->agents = json_decode($value->agents,true);
            if(in_array($employees_id, $value->employees, TRUE) && in_array($agent_id, $value->agents, TRUE)){
                return true;
            }
        }
        return false;
    }
    
    /** @Author: KHANH TOAN 2019 Feb
     *  @Todo: bao cao Market app + bqv
     *  @Param:
     **/
    public function getReportMarket() {
        $aData = [];
        $criteria=new CDbCriteria;
        $criteria->compare('t.title',$this->title, true);
        $start_date_from = '';
        $start_date_to = '';
        if(!empty($this->start_date_from)){
            $start_date_from = MyFormat::dateDmyToYmdForAllIndexSearch($this->start_date_from);
        }
        if(!empty($this->start_date_to)){
            $start_date_to = MyFormat::dateDmyToYmdForAllIndexSearch($this->start_date_to);
        }

        if(!empty($start_date_from) && empty($start_date_to))
                $criteria->addCondition("t.start_date>='$start_date_from'");
        if(empty($start_date_from) && !empty($start_date_to))
                $criteria->addCondition("t.start_date<='$start_date_to'");
        if(!empty($start_date_from) && !empty($start_date_to))
                $criteria->addBetweenCondition("t.start_date",$start_date_from,$start_date_to);
        $end_date_from = '';
        $end_date_to = '';
        if(!empty($this->end_date_from)){
            $end_date_from = MyFormat::dateDmyToYmdForAllIndexSearch($this->end_date_from);
        }
        if(!empty($this->end_date_to)){
            $end_date_to = MyFormat::dateDmyToYmdForAllIndexSearch($this->end_date_to);
        }

        if(!empty($end_date_from) && empty($end_date_to))
                $criteria->addCondition("t.end_date>='$end_date_from'");
        if(empty($end_date_from) && !empty($end_date_to))
                $criteria->addCondition("t.end_date<='$end_date_to'");
        if(!empty($end_date_from) && !empty($end_date_to))
                $criteria->addBetweenCondition("t.end_date",$end_date_from,$end_date_to);
        $mEventMarkets     = GasEventMarket::model()->findAll($criteria);
        $aEventMarketId = [];
        foreach ($mEventMarkets as $mEventMarket){
            $aEventMarketId[] = $mEventMarket->id;
            $aData['INFO'][$mEventMarket->id]['TITLE'] = $mEventMarket->title;
            $aData['INFO'][$mEventMarket->id]['START_DATE'] = $mEventMarket->start_date;
            $aData['INFO'][$mEventMarket->id]['END_DATE'] = $mEventMarket->end_date;
        }
        $this->getDataMarket($aEventMarketId, $aData);
        return $aData;
    }
    
    /** @Author: KHANH TOAN 2019 26 Feb
     *  @Todo: bao cao app + Bqv của market
     *  @Param:
     **/
    public function getDataMarket($aEventMarket, &$aData) {
        if(count($aEventMarket)<1)
            return [];
        $criteria=new CDbCriteria;
        $sParamsIn = implode(',', $aEventMarket);
        $criteria->addCondition("t.event_market_id IN ($sParamsIn)");
        $criteria->select   = 't.event_market_id, t.type_customer, t.created_by, count(id) as id,sum(if(t.sell_id > 0,1,0)) as sell_id';
        $criteria->group    = 't.event_market_id, t.created_by';
        $aTargetDaily       = TargetDaily::model()->findAll($criteria);
        foreach ($aTargetDaily as $key => $mTargetDaily) {
            if($mTargetDaily->type_customer == UsersExtend::STORE_CARD_HGD_APP){
                if(isset($aData['OUT_PUT'][$mTargetDaily->event_market_id]['app'])){
                    $aData['OUT_PUT'][$mTargetDaily->event_market_id]['app']         += $mTargetDaily->id;
                }else{
                    $aData['OUT_PUT'][$mTargetDaily->event_market_id]['app']         = $mTargetDaily->id;
                }
                if(isset($aData['EMPLOYEE'][$mTargetDaily->event_market_id][$mTargetDaily->created_by]['app'])){
                    $aData['EMPLOYEE'][$mTargetDaily->event_market_id][$mTargetDaily->created_by]['app']         += $mTargetDaily->id;
                }else{
                    $aData['EMPLOYEE'][$mTargetDaily->event_market_id][$mTargetDaily->created_by]['app']         = $mTargetDaily->id;
                }
            }
            if(isset($aData['OUT_PUT'][$mTargetDaily->event_market_id]['bqv'])){
                $aData['OUT_PUT'][$mTargetDaily->event_market_id]['bqv']             += $mTargetDaily->sell_id;
            }else{
                $aData['OUT_PUT'][$mTargetDaily->event_market_id]['bqv']             = $mTargetDaily->sell_id;
            }
            if(isset($aData['EMPLOYEE'][$mTargetDaily->event_market_id][$mTargetDaily->created_by]['bqv'])){
                $aData['EMPLOYEE'][$mTargetDaily->event_market_id][$mTargetDaily->created_by]['bqv']             += $mTargetDaily->sell_id;
            }else{
                $aData['EMPLOYEE'][$mTargetDaily->event_market_id][$mTargetDaily->created_by]['bqv']             = $mTargetDaily->sell_id;
            }
            $aData['aUid'][$mTargetDaily->created_by] = $mTargetDaily->created_by;
        }
    }
    
    public function getProvince() {
        $mAppCache = new AppCache();
        $aProvince = $mAppCache->getListdata('GasProvince', AppCache::LISTDATA_PROVINCE);
        return isset($aProvince[$this->province_id]) ? $aProvince[$this->province_id] : '';
    }
    
    public function getDisTrict() {
        $mAppCache = new AppCache();
        $aProvince = $mAppCache->getListdata('GasDistrict', AppCache::LISTDATA_DISTRICT);
        return isset($aProvince[$this->district_id]) ? $aProvince[$this->district_id] : '';
    }
    
    public function getMonitor() {
        $res = '';
        if (!empty($this->rMonitor)) {
            $res = $this->rMonitor->first_name;
        }
        
        return $res;
    }
    
    public function getAmount() {        
        return number_format($this->amount) . ' VND';
    }
    
    protected function beforeValidate() {
        $this->amount = MyFormat::removeComma($this->amount);
        return parent::beforeValidate();
    }
    
    /** @Author: NamNH Sep0419
     *  @Todo: get list model Roahshow
     **/
    public function getModelRoadshow($id){
        if(empty($id)){
            return null;
        }
        $criteria = new CDbCriteria();
        if(is_array($id)){
            $criteria->addInCondition('t.id', $id);
        }else{
            $criteria->compare('t.id', $id);
        }
        $aGasEM = GasEventMarket::model()->findAll($criteria);
        return $aGasEM;
    }
    
    /** @Author: LocNV Aug 13, 2019
     *  @Todo: view detail road show
     *  @Param:
     **/
    public function viewDetailRoadShow($id)
    {
        //check isset id gas event market
        $aGasEM = $this->getModelRoadshow($id);
        if (empty($aGasEM)) {
            return ;
        }
        $aRes = [];
        foreach ($aGasEM as $key => $mGasEM) {
            $data = [];
            //get list employee in gasEvent
            $aEmployees = json_decode($mGasEM->employees, true);
            $start_date = $mGasEM->start_date;
            $end_date = $mGasEM->end_date;
            foreach ($aEmployees as $item => $employeeId) {
                $employee = Users::model()->findByPk($employeeId);
                if(empty($employee)){
                    continue;
                }
                $criteria = new CDbCriteria();
                $criteria->compare('t.uid_login', $employeeId);
                $criteria->addCondition("t.created_date >= '$start_date'");
                $criteria->addCondition("t.created_date <= '$end_date'");
                $criteria->order = 't.id ASC';
                $aWorkReport = WorkReport::model()->findAll($criteria);
                $data[$employeeId]['employee_name'] = MyFormat::getRoleName($employee->role_id) . ' - ' . $employee->first_name;
                $data[$employeeId]['pay_salary_phone'] = !empty($employee->rUsersProfile->salary_method_phone) ? $employee->rUsersProfile->salary_method_phone : '';
                if (!empty($aWorkReport)) {
                    foreach ($aWorkReport as $i => $workReport) {
                        $data[$employeeId]['data'][$i]['code_no'] = $workReport->code_no ?: '';
                        $data[$employeeId]['data'][$i]['created_date'] = $workReport->created_date ?: '';
                        $data[$employeeId]['data'][$i]['id'] = $workReport->id ?: '';
                        $data[$employeeId]['data'][$i]['file'] = $workReport->getFileOnly();
                    }
                }
            }

            $criteria2 = new CDbCriteria();
            $criteria2->compare('t.event_market_id', $mGasEM->id);
            $criteria2->compare('t.type', GasEventMarketDetail::TYPE_EMPLOYEE);
            $criteria2->compare('t.status', GasEventMarketDetail::STATUS_ACCEPT);
            $aModelEmployeeAccept = GasEventMarketDetail::model()->findAll($criteria2);
            $aEmployeeaAccept = [];
            foreach ($aModelEmployeeAccept as $mEmployeeAccept) {
                $aEmployeeaAccept[] = $mEmployeeAccept->employee_id;
            }

            $aRes[$mGasEM->id]['aData'] = $data;
            $aRes[$mGasEM->id]['employeeAccept'] = $aEmployeeaAccept;
            $aRes[$mGasEM->id]['amount'] = $mGasEM->amount;
            $aRes[$mGasEM->id]['smsContent'] = GasEventMarket::TEXT_CONTENT_SMS . ' ' . MyFormat::dateConverYmdToDmy($mGasEM->start_date, 'ymd'); 
        }
        return $aRes;
    }
    
    /** @Author: LocNV Aug 13, 2019
     *  @Todo: can view detail road show
     *  @Param:
     **/
    public function canViewDetailRoadShow()
    {
        return true; // NamNH Sep1319, can checkin all
    }
    
    /** @Author: LocNV Aug 14, 2019
     *  @Todo: cronjob remind employee and monitor at 9:00 and 15:00
     *  @Param:
     **/
    public function cronRemindRoadShow($dateRun,$countDate = 1) {
        $date = MyFormat::modifyDays($dateRun, $countDate, "+", 'day');
        $criteria = new CDbCriteria();
        $criteria->compare('t.type', self::TYPE_ROADSHOW);
        DateHelper::searchBetween($date, $date, 'start_date_bigint', $criteria, true);
        $aGasEM = GasEventMarket::model()->findAll($criteria);

        foreach ($aGasEM as $mGasEm) {
            $aEmployee = json_decode($mGasEm->employees);
            $aEmployee[] = $mGasEm->monitor_id;
            $mGasEm->sendMailRemindRoadShow($aEmployee);
        }
        $info = "dateRun $dateRun- Run cronRemindRoadShow done ".date('Y-m-d H:i:s');
        Logger::WriteLog($info);
    }
    
    /** @Author: LocNV Aug , 2019
     *  @Todo:
     *  @Param:
     **/
    public function sendMailRemindRoadShow($employeeId) {
        $needMore                   = ['title'=> 'Nhắc nhở lịch chạy RoadShow'];
        $mEmployee = Users::model()->findAllByPk($employeeId);
        foreach ($mEmployee as $key => $mUsers) {
            if(!empty($mUsers->email)){
                $needMore['list_mail'][] = $mUsers->email;
            }
        }
        if(empty($mEmployee)){
            return;
        }
        $dateRoadShow   = MyFormat::dateConverYmdToDmy($this->start_date, 'd/m/Y'); 
        $dateStart      = MyFormat::dateConverYmdToDmy($this->start_date, 'H:i'); 
        $dateEnd        = MyFormat::dateConverYmdToDmy($this->end_date, 'H:i'); 
        $sBody = 'Hi,';
        $sBody .= '<br><br>Bạn có lịch chạy RoadShow ngày: ' . $dateRoadShow . '.';
        $sBody .= '<br>Thời gian: ' . $dateStart . ' đến '.$dateEnd . '.';
        $sBody .= '<br>Tuyến đường chạy: '.$this->note.'.';
        $sBody .= '<br>Đề nghị đến sớm 20p để chuẩn bị không làm ảnh hưởng chung đến mọi người.';
        $sBody .= '<br>Trân trọng!';
        $sBody .= '<br><br>Đây là email tự động, vui lòng không reply. Gửi từ noibo.daukhimiennam.com';        
        SendEmail::bugToDev($sBody, $needMore);
    }
    
    /** @Author: NamNH Aug 15, 2019
     *  @Todo: delete roadshow detail
     **/
    public function deleteRoadShowDetail() {
        $criteria = new CDbCriteria();
        $criteria->addCondition('event_market_id = ' . $this->id);
        GasEventMarketDetail::model()->deleteAll($criteria);
    }
    
    public function getUsersIdCanUpdate(){
        return [
            862890,//Huỳnh Văn Phúc
//            GasConst::UID_NGOC_PT,
            217782,//Nguyễn Văn Đợt
            
        ];
    }
    /** @Author: LocNV Aug , 2019
     *  @Todo: can update status
     *  @Param:
     **/
    public function canUpdateStatus()
    {
        $countDate = isset(Yii::app()->params['DaysGasEventMarket']) ? ((Yii::app()->params['DaysGasEventMarket']) - 1) : 0;
        if($this->type != self::TYPE_ROADSHOW){
            return false;
        }
        $cRole = MyFormat::getCurrentRoleId();
        $cUid = MyFormat::getCurrentUid();
        $aUidApply = $this->getUsersIdCanUpdate();
        if(in_array($cUid, $aUidApply) || $cRole == ROLE_ADMIN){
            $currentDate            = date('Y-m-d');
            $currentDateBefore      = MyFormat::addDays($currentDate, $countDate,'-');
            $startDate      = MyFormat::dateConverYmdToDmy($this->start_date, 'Y-m-d');
            if ($currentDate >= $startDate && $currentDateBefore <= $startDate) {
                return true;
            }
        }
        return false;
    }
    
    public function canUpdateData(){
        $cRole = MyFormat::getCurrentRoleId();
        $cUid = MyFormat::getCurrentUid();
        $currentDate    = date('Y-m-d');
        $startDate      = MyFormat::dateConverYmdToDmy($this->start_date, 'Y-m-d');
        $countDate = isset(Yii::app()->params['DaysGasEventMarket']) ? ((Yii::app()->params['DaysGasEventMarket']) - 1) : 0;
        $currentDateCompare      = MyFormat::addDays($startDate, $countDate,'+');
        $aUidApply = $this->getUsersIdCanUpdate();
        if((in_array($cUid, $aUidApply) || $cRole == ROLE_ADMIN) && $this->type == self::TYPE_ROADSHOW){
            if ($currentDate <= $currentDateCompare) {
                return true;
            }
        }
        if($cUid == $this->created_by){
            if ($currentDate < $currentDateCompare) {
                return true;
            }
        }
        return false;
    }
    
    public function canDeleteData(){
        $cRole = MyFormat::getCurrentRoleId();
        if($cRole == ROLE_ADMIN){
            return true;
        }
        return false;
    }
    
//    check count users apply
    public function checkIsApply(){
        $criteria2 = new CDbCriteria();
        $criteria2->compare('t.event_market_id', $this->id);
        $criteria2->compare('t.type', GasEventMarketDetail::TYPE_EMPLOYEE);
        $criteria2->compare('t.status', GasEventMarketDetail::STATUS_ACCEPT);
        $countAccept = GasEventMarketDetail::model()->count($criteria2);
        return $countAccept > 0 ? true : false;
    }
    
    /** @Author: NamNH Sep0319
     *  @Todo: return class visible checkbox in index
     **/
    public function canExcel(){
        $strReturn = 'isDisplayNone';
        if($this->type == self::TYPE_ROADSHOW){ 
//            check employees are approve
            if($this->checkIsApply()){
                $strReturn = '';
            }
        }
        return $strReturn;
    }
    
    /** @Author: NamNH Sep0519
     *  @Todo: set status employee
     **/
    public function setStatusEmployee() {
        if(!empty($this->submitMore) && !empty($this->emloyeeApprove) && is_array($this->emloyeeApprove)){
            $modelDetail = new GasEventMarketDetail();
            foreach ($this->emloyeeApprove as $key => $employee_id) {
                $modelDetail->setStatusRoadShowEmployee($this->id, $employee_id, GasEventMarketDetail::STATUS_ACCEPT);
            }
        }
    }
    
    /** @Author: NamNH Sep1319
     *  @Todo: get count app install
     **/
    public function getCountApp() {
        $result = 0;
        if($this->type == self::TYPE_ROADSHOW || empty($this->id)){
            return $result;
        }
        $criteria = new CDbCriteria();
        $criteria->compare('t.event_market_id',$this->id);
        $result = TargetDaily::model()->count($criteria);
        return $result;
    }
    
    /** @Author: NamNH Sep1319
     *  @Todo: get count app install
     **/
    public function getCountBqv() {
        $result = 0;
        if($this->type == self::TYPE_ROADSHOW || empty($this->id)){
            return $result;
        }
        $criteria       = new CDbCriteria();
        $criteria->compare('t.event_market_id',$this->id);
        $aTargetDaily   = TargetDaily::model()->findAll($criteria);
        $aUsersId       = CHtml::listData($aTargetDaily, 'customer_id', 'customer_id');
        $result         = $this->getCountBqvByUid($aUsersId);
        return $result;
    }
    
    public function getCountBqvByUid($aUsersId){
        $dateFrom   = MyFormat::dateConverYmdToDmy($this->start_date, 'Y-m-d');
        $dateTo     = MyFormat::addDays($dateFrom, self::DATE_BQV_EVENT);
        $criteria       = new CDbCriteria();
        $criteria->addCondition('t.first_purchase <= "'. $dateTo.'"');
        $criteria->addCondition('t.first_purchase >= "'. $dateFrom.'"');
        $criteria->addInCondition('t.user_id',$aUsersId);
        $count          = UsersInfo::model()->count($criteria);
        return $count;
    }
   
    public function canViewMaterial(){
        if($this->type == self::TYPE_ROADSHOW){
            return false;
        }
        return true;
    }
    
    public function getListUsersCanPay(){
        return [
            GasConst::UID_NHAN_NTT,
        ];
    }
    
    /** @Author: NamNH Sep1819
     *  @Todo: can pay money
     **/
    public function canPayMoney() {
        if($this->status == self::STATUS_PAID || $this->status == self::STATUS_INACTICE || $this->type != self::TYPE_ROADSHOW){
            return false;
        }
        if(!$this->checkIsApply()){
            return false;
        }
        $cUid = MyFormat::getCurrentUid();
        $cRole = MyFormat::getCurrentRoleId();
        $aUid = $this->getListUsersCanPay();
        if(in_array($cUid, $aUid) || $cRole == ROLE_ADMIN){
            return true;
        }
        return false;
    }
    
    /** @Author: NamNH Sep1819
     *  @Todo: update status
     **/
    public function updateStatus($status){
        if(empty($status)){
            return;
        }
        $this->status = $status;
        $this->save();
    }
    
    public function canReload(){
        $cRole = MyFormat::getCurrentRoleId();
        if($cRole == ROLE_ADMIN && in_array($this->type,$this->getTypeActivationMaket())){
            return true;
        }
        return false;
    }
    
    
    public function reloadInstallApp(){
        $aIdEmployee        = json_decode($this->employees, true);
        $aIdAgent           = json_decode($this->agents, true);
        $date_from_second   = $this->start_date;
        $date_to_second     = $this->end_date;
        if(count($aIdEmployee) > 0 && count($aIdAgent) > 0 && !empty($date_from_second) && !empty($date_to_second)){
            $strEmployees   =   '('. implode(',', $aIdEmployee). ')';
            $strAgents      =   '('. implode(',', $aIdAgent). ')';
            $sql = 'UPDATE `gas_target_daily` SET `event_market_id` = '.$this->id
                    .' WHERE `event_market_id` <= 0 '
                            .'AND (`created_date_user` BETWEEN "'.$date_from_second.'" AND "'.$date_to_second.'")'
                            .' AND `created_by` IN '.$strEmployees
                            .' AND `agent_id` IN '.$strAgents.';';
            Yii::app()->db->createCommand($sql)->execute();
        }
    }
    
    /** @Author: NhanDT Sep 20,2019
     *  @Todo: tỉ lệ % giữa Bqv vs App
     *  @Param: $qtyApp = countApp ; $qtyApp = countBqv 
     **/
    public function getRatioBqvApp($qtyApp, $qtyBqv) {
        if (!empty($qtyApp)){
            return round(($qtyBqv/$qtyApp),2);
        }
        return 0;    
    }

    /** @Author: NhanDT Sep 26,2019
     *  @Todo: todo
     *  @Param: param
     **/
    public function searchReportEventMarket(){ 
        $start_date_from = $start_date_to = '';
        $aData = $aId =[];
        $criteria=new CDbCriteria;
        $criteria->compare('t.title',$this->title, true);
        $criteria->compare('t.status',$this->status);
        $criteria->addInCondition('t.type',$this->getTypeActivationMaket());
        $criteria->compare('t.district_id', $this->district_id);
        $criteria->compare('t.province_id', $this->province_id);
        if(!empty($this->start_date_from)){
            $start_date_from = strtotime($this->start_date_from);
        }
        if(!empty($this->start_date_to)){
            $start_date_to   = strtotime($this->start_date_to);
        }
        if(!empty($start_date_from) && empty($start_date_to)){
            $criteria->addCondition("t.start_date_bigint>='$start_date_from'");
        }
        if(empty($start_date_from) && !empty($start_date_to)){
            $criteria->addCondition("t.start_date_bigint<='$start_date_to'");
        }
        if(!empty($start_date_from) && !empty($start_date_to)){
            DateHelper::searchBetween($this->start_date_from, $this->start_date_to, 'start_date_bigint', $criteria, true);
        }
        $aGasEventMarket            = GasEventMarket::model()->findAll($criteria);
        $mEventMartketDetail        = new GasEventMarketDetail();
        $aData['aGasEventMarket']   = $aGasEventMarket;
        foreach ($aGasEventMarket as $key => $mGasEventMarketFor) {
            $aId[$mGasEventMarketFor->id]   = $mGasEventMarketFor->id;
            $aData['APP'][$mGasEventMarketFor->id] = $mGasEventMarketFor->getCountApp();
            $aData['BQV'][$mGasEventMarketFor->id] = $mGasEventMarketFor->getCountBqv();
        }
        $aData['MATERIALS']         = $mEventMartketDetail->getArrMaterials($aId,[GasEventMarketDetail::TYPE_MATERIAL_EXPORT,GasEventMarketDetail::TYPE_MATERIAL_IMPORT]); 
        return $aData;
    }

    
    /** @Author: NhanDT Sep 27,2019
     *  @Todo: todo
     *  @Param: param
     **/
    public function getAngetNameForReport($aModel) {
        $aJson = json_decode($aModel);
        $str = '';
        foreach ($aJson as $key => $value){
            $mEmployee = Users::model()->findByPk($value); 
            if ($mEmployee) {
                $str .= $mEmployee->first_name.PHP_EOL;
            }
        }
        return $str;
    }
    
    /** @Author: NhanDT Sep 20,2019
     *  @Todo: lấy giá trung bình vật tư cung cấp trong đợt
     *  @Param: 
     **/
    public function getAverageMaterial() {
       return 0;
    }
}

