<?php

/**
 * This is the model class for table "{{_users_price_other}}".
 *
 * The followings are the available columns in table '{{_users_price_other}}':
 * @property integer $id
 * @property integer $type
 * @property string $agent_id
 * @property string $customer_id
 * @property integer $type_customer
 * @property string $customer_parent_id
 * @property string $sale_id
 * @property string $date_apply
 * @property string $date_apply_bigint
 * @property integer $materials_type_id
 * @property integer $materials_id
 * @property string $price_code
 * @property integer $price_value
 * @property string $created_by
 * @property string $created_date
 * @property string $last_update_by
 * @property string $last_update
 */
class UsersPriceOther extends BaseSpj
{
    public $customerStatus;
    public $aMaterials, $aModelUpdate;
    public $autocomplete_name, $autocomplete_name_1, $autocomplete_name_2 ,$autocomplete_name_3, $autocomplete_name_4, $autocomplete_materials;
    public $month, $year;

    const PRICE_CODE_DEFALT = 0;
    
    const TYPE_ALCOHOL      = 1;
    
    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return UsersPriceOther the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{_users_price_other}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        return array(
            array('date_apply', 'required', 'on'=>'update'),
            array('materials_id, sale_id, date_apply', 'required', 'on'=>'update'),
            array('price_value', 'required', 'on'=>'create'),
            array('date_apply, created_date, last_update', 'safe'),
            array('month, year', 'safe'),
            array('id, type, agent_id, customer_id, type_customer, customer_parent_id, sale_id, date_apply_bigint, materials_type_id, materials_id, price_code, price_value, created_by, last_update_by', 'safe'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        return array(
            'rCustomer' => array(self::BELONGS_TO, 'Users', 'customer_id'),
            'rCustomerParent' => array(self::BELONGS_TO, 'Users', 'customer_parent_id'),
            'rAgent' => array(self::BELONGS_TO, 'Users', 'agent_id'),
            'rSale' => array(self::BELONGS_TO, 'Users', 'sale_id'),
            'rMaterials' => array(self::BELONGS_TO, 'GasMaterials', 'materials_id'),
            'rMaterialsType' => array(self::BELONGS_TO, 'GasMaterialsType', 'materials_type_id'),
            'rCreateBy' => array(self::BELONGS_TO, 'Users', 'created_by'),
            'rLastUpdateBy' => array(self::BELONGS_TO, 'Users', 'last_update_by'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
            return array(
            'agent_id'          => 'Đại lý',
            'customer_id'       => 'Khách hàng',
            'type_customer'     => 'Loại KH',
            'customer_parent_id'=> 'Thuộc hệ thống',
            'sale_id'           => 'NV kinh doanh',
                'materials_type_id' => 'Loại vật tư',
            'materials_id'      => 'Vật tư',
            'price_value'       => 'Giá',
            'date_apply'        => 'Ngày áp dụng',            
            'created_by'        => 'Người tạo',
            'created_date'      => 'Ngày tạo',
            'last_update_by'    => 'Người sửa gần nhất',
            );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        $criteria=new CDbCriteria;
        $criteria->compare('t.agent_id',$this->agent_id);
        $criteria->compare('t.customer_id',$this->customer_id);
        $criteria->compare('t.type_customer',$this->type_customer);
        $criteria->compare('t.customer_parent_id',$this->customer_parent_id);
        $criteria->compare('t.sale_id',$this->sale_id);
        if (!empty($this->month)){
            $criteria->addCondition("month(t.date_apply) ='".$this->month."'");
        }
        if (!empty($this->year)){
            $criteria->addCondition("year(t.date_apply) ='".$this->year."'");
        }
        $criteria->compare('t.materials_type_id',$this->materials_type_id);
        $criteria->compare('t.materials_id',$this->materials_id);
        $criteria->compare('t.price_value',$this->price_value); 
        $criteria->compare('t.created_by',$this->created_by);
        $sort = new CSort();
        $sort->attributes = array(
            'created_date'=>'created_date',
            'created_by'=>'created_by',
            'price_value'=>'price_value',
        );    
        $sort->defaultOrder = 't.id DESC';
        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
            'sort' => $sort,
            'pagination'=>array(
                'pageSize'=> Yii::app()->user->getState('pageSize',Yii::app()->params['defaultPageSize']),
            ),
        ));
    }
    
    public function getArrayType(){
        return [
            self::TYPE_ALCOHOL => 'Cồn',
        ];
    }
    
    /** @Author: NhanDT Sep 5, 2019
     *  @Todo: todo
     *  @Param: param
     **/
    public function getCustomer($field_name='') {
        $mUser = $this->rCustomer;
        if($mUser){
            if($field_name != ''){
                return $mUser->$field_name;
            }
            return "<b>".$mUser->code_account.' - '.$mUser->code_bussiness.' - '.$mUser->first_name."</b><br>".$mUser->address."";
        }
        return '';
    }
    
    /** @Author: NhanDT Sep 5, 2019
     *  @Todo: todo
     *  @Param: param
     **/
    public function getAddressCus() {
        $mUser = $this->rCustomer;
        if($mUser){
            return $mUser->getAddress();
        }
        return '';
    }
    
    /** @Author: NhanDT Sep 5, 2019
     *  @Todo: todo
     *  @Param: param
     **/
    public function getTypeCustomer() {
        if(isset(CmsFormatter::$CUSTOMER_BO_MOI[$this->type_customer])){
            return CmsFormatter::$CUSTOMER_BO_MOI[$this->type_customer];
        }
        return "";
    }

    /** @Author: NhanDT Sep 5, 2019
     *  @Todo: todo
     *  @Param: param
     **/
    public function getAgentName() {
        $model = new CacheSession();
        $aAgent = $model->getListAgent();
        return isset($aAgent[$this->agent_id]) ? $aAgent[$this->agent_id]['first_name'] : '';
    }
    
    /** @Author: NhanDT Sep 5, 2019
     *  @Todo: todo
     *  @Param: param
     **/
    public function getSale($field_name='') {
        $mUser = $this->rSale;
        if($mUser){
            if($field_name != ''){
                return $mUser->$field_name;
            }
            return $mUser->first_name;
        }
        return '';
    }
    
    /** @Author: NhanDT Sep 5, 2019
     *  @Todo: Đơn mới nhất
     *  @Param: param
     **/
    public function getLastPurchase() {
        $mCustomer = $this->rCustomer;
        if(empty($mCustomer)){
            return '';
        }
        return $mCustomer->getLastPurchase();
    }
    
    /** @Author: NhanDT Sep 5,2019
     *  @Todo: todo
     *  @Param: param
     **/
    public function getMonthYearDateApply() {
        $model = new MyFormat();
        return !empty($this->date_apply)?$model->dateConverYmdToDmy($this->date_apply, 'm-Y'):'';
    }
    
    /** @Author: NhanDT Sep 5,2019
     *  @Todo: todo
     *  @Param: param
     **/
    public function getMaterials() {
        $mMaterials = $this->rMaterials;
        if(empty($mMaterials)){
            return '';
        }
        return $mMaterials->getName();
    }
    
    /** @Author: NhanDT Sep 5,2019
     *  @Todo: todo
     *  @Param: param
     **/
    public function getMaterialsType() {
        $mMaterialsType = $this->rMaterialsType;
        return empty($mMaterialsType)?'':$mMaterialsType->getName();
    }

    /** @Author: NhanDT Sep 5,2019  
     *  @Todo: todo
     *  @Param: param
     **/
    public function getPriceValue($fomat = true) {
        if($this->price_value < 1){
            return '';
        }
        if(!$fomat){
            return $this->price_value;
        }
        return ActiveRecord::formatCurrency($this->price_value);
    }
    
    /** @Author: NhanDT Sep 5,2019
     *  @Todo: todo
     *  @Param: param
     **/
    public function getCreatedBy($field_name='') {
        $mUser = $this->rCreateBy;
        if($mUser){
            if($field_name != ''){
                return $mUser->$field_name;
            }
            return $mUser->first_name;
        }
        return '';
    }

    /** @Author: NhanDT Sep 6,2019
     *  @Todo: todo
     *  @Param: param
     **/
    public function setCustomerInfo() {
        $mCustomer = $this->rCustomer;
        if($mCustomer){
            $this->agent_id             = $mCustomer->area_code_id;
            $this->type_customer        = $mCustomer->is_maintain;
            $this->customer_parent_id   = $mCustomer->parent_id;
//            $this->sale_id              = $mCustomer->sale_id;
            $this->sale_id              = ''; // sale ben Users la sale cua KH gas
        }
    }

    /** @Author: NhanDT Sep 6,2019
     *  @Todo: todo
     *  @Param: param
     **/
    public function saveCreate() {
        if(!isset($_POST['customer_id'])){
            return;
        }
        $aCustomerNew   = $aRowInsert = $aCustomerDuplicate = [];
        $created_by     = MyFormat::getCurrentUid();
        $str            = '';
        $strCusSame     = '';
        $strSale        = '';
        $strPrice       = '';
        $created_date       = date('Y-m-d H:i:s');// DungNT Sep2319 
//        $date_apply         = date('Y-m-01');
//        $date_apply_bigint  = strtotime($date_apply);
        $date_apply     = isset($_POST['UsersPriceOther']['date_apply']) ? ($_POST['UsersPriceOther']['date_apply']) : 0;

        if(strpos($date_apply, '/')){
            $date_apply = MyFormat::dateConverDmyToYmd($date_apply);
        }
        if (!isset($_POST['customer_id']) || empty($date_apply))
        {
            return;
        }
        $date_apply         = date('Y-m-01', strtotime($date_apply)); // return về đầu tháng
        $date_apply_bigint  = strtotime($date_apply);
        
        foreach($_POST['customer_id'] as $cus_id => $aMaterials){
            if (empty($aMaterials) || empty($aMaterials['materials_id'])){
                $str .= 'Vui lòng nhập đầy đủ thông tin cho KH '.$this->getNameCusByID($cus_id).'<br>';
                continue;
            } else {
                $sale_id        = isset($aMaterials['sale_id']) ? $aMaterials['sale_id']: 0;
                if(empty($sale_id)){
                    $strSale .= ' + '.$this->getNameCusByID($cus_id).'<br>';
                    continue;
                }
                foreach ( $aMaterials['materials_id'] as $key => $materials)
                {
                    
                    $materials_id   = isset($materials) ? $materials : 0;
                       if($this->stringSameUserExistWithDateApply($cus_id, $materials_id, $date_apply_bigint, '')){//fix không cho tạo trùng trong tháng
                        $strCusSame .= ' + '.$this->stringSameUserExistWithDateApply($cus_id, $materials_id, $date_apply_bigint, '');
                            continue;
                       }
                       $materials_type_id  = isset($aMaterials['materials_type_id'][$key]) ? $aMaterials['materials_type_id'][$key] : 0;
                       $price_value        = isset($aMaterials['price_value'][$key]) ? MyFormat::removeComma($aMaterials['price_value'][$key]) : 0;
                       $price_code         = self::PRICE_CODE_DEFALT;
                       if(empty($price_value)){
                         $strPrice .= ' + '.$this->getNameCusByID($cus_id).' - '.$this->getNameMaterialByID($materials_id).'<br>';
                            continue;
                       }
                       $mUserPriceOther                 = new UsersPriceOther();
                       $mUserPriceOther->customer_id    = $cus_id;
                       $mUserPriceOther->setCustomerInfo();
                       

                       $aRowInsert[] = "(".self::TYPE_ALCOHOL.",
                        '$mUserPriceOther->agent_id',
                        '$cus_id',
                        '$mUserPriceOther->type_customer',
                        '$mUserPriceOther->customer_parent_id',
                        '$sale_id',
                        '$materials_type_id',
                        '$materials_id',
                        '$price_code',
                        '$price_value',
                        '$created_by',
                        '$created_date',
                        '$date_apply',
                        '$date_apply_bigint'
                    )";
                }
            }  
        }

        $tableName = self::model()->tableName();
        $sql = "insert into $tableName (type,
                        agent_id,
                        customer_id,
                        type_customer,
                        customer_parent_id,
                        sale_id,
                        materials_type_id,
                        materials_id,
                        price_code,
                        price_value,
                        created_by,
                        created_date,
                        date_apply,
                        date_apply_bigint
                        ) values ".implode(',', $aRowInsert);
        if(count($aRowInsert)>0){
            Yii::app()->db->createCommand($sql)->execute();
        }
        
        //Xuất lỗi: KH k có thông tin vật tư, trùng KH - vật tư, KH k có NV kinh doanh, KH - vật tư k có giá
        empty($strSale)?$str:$str.='KH không có thông tin NV Kinh doanh không được thêm vào: <br>'.$strSale.'<br>';
        empty($strCusSame)?$str:$str.='KH - vật tư bị trùng sẽ không được thêm vào<br>'.$strCusSame.'<br>';
        empty($strPrice)?$str:$str.='KH - vật tư không có giá sẽ không được thêm vào: <br>'.$strPrice;
        return $str;
    }
    
    
    public function stringSameUserExistWithDateApply($customer_id, $materials_id, $date_apply, $id) {
        $models = $this->modelSameUserExistWithDateApply($customer_id, $materials_id, $date_apply, $id);
        $str    = '';
        foreach($models as $model){
            $str .= $model->getCustomer("first_name") .' - '.$model->getMaterials().'</br>';
        }
        return $str;
    }
    
    /** @Author: NhanDT Sep 10,2019
     *  @Todo: get array customer by month year, sử dụng để check không trùng khi tạo mới với record tồn tại
     *  @Param: $date_apply format Y-m-d
     **/
    public function modelSameUserExistWithDateApply($customer_id, $materials_id, $date_apply_bigint, $id) {
        $month = date("m",$date_apply_bigint);
        $year  = date("Y",$date_apply_bigint);
        $criteria   = new CDbCriteria();
        $criteria->compare('MONTH(date_apply)',$month);
        $criteria->compare('YEAR(date_apply)', $year);
        $criteria->compare('t.customer_id', $customer_id);
        $criteria->compare('t.materials_id', $materials_id);
        if (!empty($id)){ // xét không trùng update khi thay đổi giá trị date_apply
            $criteria->addCondition('t.id !='.$id);
        }
        return self::model()->findAll($criteria);
    }
    
    
    public function getNameCusByID($customer_id) {
        $mCus= new Users;
        return $mCus->findByPk($customer_id)->first_name;
    }
    
    public function getNameMaterialByID($material_id) {
        $mMaterial = new GasMaterials;
        return $mMaterial->findByPk($material_id)->getName();
    }
    
    
    /** @Author: NhanDT Sep 9,2019
     *  @Todo: todo
     *  @Param: param
     **/
    public function loadDataUpdate($id) {
        $sId = trim($id, '-');
        $aId = explode('-', $sId);
        $this->aModelUpdate = $this->getArrById($aId);
        $this->date_apply = MyFormat::dateConverYmdToDmy($this->date_apply);
    }
    
    /** @Author: NhanDT Sep 9,2019
     *  @Todo: todo
     *  @Param: param
     **/
    public function getArrById($aId) {
        $criteria   = new CDbCriteria();
        $sParamsIn  = implode(',', $aId);
        $criteria->addCondition("t.id IN ($sParamsIn)");
        return self::model()->findAll($criteria);
    }
    
    /** @Author: NhanDT Sep 9,2019
     *  @Todo: todo
     *  @Param: param
     **/
    public function saveUpdate() {
        $cLastUpdate   = MyFormat::getCurrentUid();
        $tableName = self::model()->tableName();
        $sql ='';
        $strCusSame = '';

        $id = isset($_POST['id'][0]) ? ($_POST['id'][0]) : 0; 

        $mUserPrice = self::model()->findByPk($_POST['id']);
            if(is_null($mUserPrice)){
                throw new Exception('Important UserPrice:  Dữ liệu cập nhật giá khách hàng không hợp lệ 1');
            }
       
        $cus_id                 = isset($_POST['UsersPriceOther']['customer_id']) ? ($_POST['UsersPriceOther']['customer_id']) : 0;
        $materials_id           = isset($_POST['UsersPriceOther']['materials_id']) ? ($_POST['UsersPriceOther']['materials_id']) : 0;
        $materials_type_id      = isset($_POST['UsersPriceOther']['materials_type_id']) ? ($_POST['UsersPriceOther']['materials_type_id']) : 0;
        $sale_id                = isset($_POST['UsersPriceOther']['sale_id']) ? ($_POST['UsersPriceOther']['sale_id']) : 0;
        $price_value            = isset($_POST['UsersPriceOther']['price_value']) ? MyFormat::removeComma($_POST['UsersPriceOther']['price_value']) : 0;
        $date_apply             = isset($_POST['UsersPriceOther']['date_apply']) ? ($_POST['UsersPriceOther']['date_apply']) : 0;

        if(strpos($date_apply, '/')){
            $date_apply = MyFormat::dateConverDmyToYmd($date_apply);
        }
        
        if (empty($sale_id) || empty($materials_id) || empty($date_apply))
            {
                return FALSE;
            }
        $date_apply         = date('Y-m-01', strtotime($date_apply)); // return về đầu tháng
        $date_apply_bigint  = strtotime($date_apply);
        
        if($this->stringSameUserExistWithDateApply($cus_id, $materials_id, $date_apply_bigint, $id)){//fix không cho tạo trùng trong tháng
            $strCusSame .= ' + '.$this->stringSameUserExistWithDateApply($cus_id, $materials_id, $date_apply_bigint, $id);
        }
            $price_code     = 0;
        
            $last_update = date('Y-m-d H:i');
            $sql .= "UPDATE $tableName SET "
                        . " `sale_id`=\"$sale_id\",  "
                    . " `date_apply`=\"$date_apply\",  "
                    . " `date_apply_bigint`=\"$date_apply_bigint\",  "
                        . " `materials_id`=\"$materials_id\",  "
                    . " `materials_type_id`=\"$materials_type_id\",  "
                        . " `price_code`=\"$price_code\",  "
                        . " `price_value`=\"$price_value\", "
                        . " `last_update_by`=\"$cLastUpdate\", "
                    . " `last_update`=\"$last_update\" "
                        . "WHERE `id`=$id ;";
        if(trim($sql)!='' && empty($strCusSame)){
            Yii::app()->db->createCommand($sql)->execute();
            return TRUE;
        }
        return FALSE;
    }
    
    
    /** @Author: Nhan Sep 10, 2019
     *  @Todo: run cron copy record for every month, copy gia thang trc
     * Copy price_materials from old month to current month, run đầu tháng
     *  @Param: $currentDate date with format Y-m-d
     **/
    public function cronCopyOldPrice($month, $year) {
        $prevMonth          = $prevYear = 0;
        UsersPrice::getPrevMonthYear($month, $year, $prevMonth, $prevYear);
        $prevMonth          = sprintf('%02d', $prevMonth);
        $date               = date("$prevYear-$prevMonth-01");
        $previousDate       = new DateTime($date);
        $previousDateBegin  = $previousDate->format('Y-m-01');
        $previousDateEnd    = $previousDate->format('Y-m-t');

        $criteria           = new CDbCriteria();
        $criteria->addCondition("t.date_apply >= '$previousDateBegin'");
        $criteria->addCondition("t.date_apply <= '$previousDateEnd'");

        $aModel = UsersPriceOther::model()->findAll($criteria);

        if (count($aModel) > 0) {
            $previousDate->modify('first day of next month');
            $date_apply         = $previousDate->format('Y-m-d');
            $date_apply_bigint  = strtotime($date_apply);
            foreach ($aModel as $model) {
                $aRowInsert[] = "(
                        $model->type,
                        $model->agent_id,
                        $model->customer_id,
                        $model->type_customer,
                        $model->customer_parent_id,
                        $model->sale_id,
                        '$date_apply',
                        '$date_apply_bigint',
                        $model->materials_type_id,
                        $model->materials_id,
                        $model->price_code,
                        $model->price_value,
                        $model->created_by,
                        '".date('Y-m-d H:i:s')."'
                    )";
            }
            $sql = $this->buildSqlInsert($aRowInsert);
            $strLog = '******* CopyPriceBoMoiCommand: UsersPriceOther->cronCopyOldPrice IMPORTANT: No price copy, sql: '.$sql.'| done at '.date('d-m-Y H:i:s');
            if(count($aRowInsert)){
                Yii::app()->db->createCommand($sql)->execute();
                // Send log
                $needMore['title']      = 'cronCopyOldPrice UsersPriceOther';
                $needMore['list_mail']  = ['duongnv@spj.vn'];
                $strLog = '******* CopyPriceBoMoiCommand: UsersPriceOther->cronCopyOldPrice done at '.date('d-m-Y H:i:s');
            }
            SendEmail::bugToDev($strLog, $needMore);
            Logger::WriteLog($strLog);
        }
    }
    
    /** @Author: Nhan Sep 10, 2019
     *  @Todo: build sql for run insert multi record
     *  @Param:
     **/
    public function buildSqlInsert($aRowInsert) {
        $tableName = UsersPriceOther::model()->tableName();
        $sql = "INSERT INTO $tableName (
                    type,
                    agent_id,
                    customer_id,
                    type_customer,
                    customer_parent_id,
                    sale_id,
                    date_apply,
                    date_apply_bigint,
                    materials_type_id,
                    materials_id,
                    price_code,
                    price_value,
                    created_by,
                    created_date
                ) values " . implode(',', $aRowInsert);
        return $sql;
    }
    
    
    /** @Author: DungNT Sep 23, 2019
     *  @Todo: get price of customer by type, month, year
     *  @Param: $customer_id id of customer
     *  @Param: $type type of row: ex TYPE_ALCOHOL = 1
     *  @Param: $month, $year
     **/
    public function getPriceOfCustomerByType($customer_id, $type, $month, $year) {
        $dateFind = "$year-$month-01";
        $criteria = new CDbCriteria();
        $criteria->compare('t.date_apply_bigint', strtotime($dateFind));
        $criteria->compare('t.customer_id', $customer_id);
        $criteria->compare('t.type', $type);
        return UsersPriceOther::model()->findAll($criteria);
    }
    
    /** @Author: DungNT Sep 23, 2019
     *  @Todo: get price of customer by type, month, year
     *  @Param: $aRes array param price b12, b45...
     *  @Param: $customer_id id of customer
     *  @Param: $month, $year
     **/
    public function getPriceOfAlcohol(&$aRes, $customer_id, $month, $year) {
        $aData = $this->getPriceOfCustomerByType($customer_id, UsersPriceOther::TYPE_ALCOHOL, $month, $year);
        if(empty($aData)){
            return ;
        }
        foreach($aData as $mUsersPriceOther):
            $aRes[UsersPrice::PRICE_OTHER_ITEM][$mUsersPriceOther->materials_id] = $mUsersPriceOther->price_value;
        endforeach;
    }
    
}