<?php

/**
 * This is the model class for table "{{_source_report}}".
 *
 * The followings are the available columns in table '{{_source_report}}':
 * @property integer $id
 * @property integer $supplier_id
 * @property integer $supplier_warehouse_id
 * @property integer $qty_contract
 * @property string $month
 * @property string $created_date
 */
class GasSourceReport extends BaseSpj {

    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return GasSourceReport the static model class
     */
    const TO_KG  = 1;
    const TO_TON = 2;
    
    public $dateFrom, $dateTo;

    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return '{{_source_report}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        return [
            array('supplier_id, supplier_warehouse_id', 'numerical', 'integerOnly' => true),
            array('qty_contract', 'numerical'),
            array('supplier_id, supplier_warehouse_id, qty_contract, month', 'required', 'on' => 'create, update'),
            array('month, created_date, dateFrom, dateTo', 'safe'),
            ['id, supplier_id, supplier_warehouse_id, qty_contract, month, created_date', 'safe'],
        ];
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        return [
            'rSupplier' => array(self::BELONGS_TO, 'Users', 'supplier_id'),
            'rWarehouse' => array(self::BELONGS_TO, 'Users', 'supplier_warehouse_id'),
        ];
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return [
            'id' => 'ID',
            'supplier_id' => 'Nhà cung cấp',
            'supplier_warehouse_id' => 'Kho NCC',
            'qty_contract' => 'SL ký hợp đồng',
            'month' => 'Tháng',
            'created_date' => 'Ngày tạo',
        ];
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search() {
        $criteria = new CDbCriteria;
        $criteria->compare('t.supplier_id', $this->supplier_id);
        $criteria->compare('t.supplier_warehouse_id', $this->supplier_warehouse_id);
        if (!empty($this->dateFrom)) {
//            $dateFrom = MyFormat::dateConverYmdToDmy($this->month, 'm/Y');
            $dateFrom = date('Y-m-01', strtotime($this->dateFrom));
            $criteria->addCondition("t.month >= '$dateFrom'");
        }
        if (!empty($this->dateTo)) {
//            $dateTo = MyFormat::dateConverYmdToDmy($this->month, 'm/Y');
            $dateTo = date('Y-m-t', strtotime($this->dateTo));
            $criteria->addCondition("t.month <= '$dateTo'");
        }
        $criteria->order = 't.id DESC';
        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
            'pagination' => array(
                'pageSize' => Yii::app()->user->getState('pageSize', Yii::app()->params['defaultPageSize']),
            ),
        ));
    }

    /** @Author: KhueNM Aug 22, 2019
     *  @Todo: get kho ncc - gasSourceReport
     *  @Param:
     * */
    public function getSupplierWarehouse() {
        return empty($this->rWarehouse) ? '' : $this->rWarehouse->getFullName();
    }

    /** @Author: KhueNM Aug 22, 2019
     *  @Todo: get received- gasSourceReport
     *  @Param:
     * */
    public function getReceived() {
        if( empty($this->supplier_warehouse_id) ) return '';
        $criteria           = new CDbCriteria();
        $criteria->compare('t.status', TruckPlan::STT_DONE);
        $criteria->compare('t.supplier_id', $this->supplier_id);
        $criteria->compare('t.supplier_warehouse_id', $this->supplier_warehouse_id);
        $dateFrom = MyFormat::dateConverYmdToDmy($this->month, '01-m-Y');
        $dateTo = MyFormat::dateConverYmdToDmy($this->month, 't-m-Y');
        DateHelper::searchGreater($dateFrom, 'plan_date_bigint', $criteria);
        DateHelper::searchSmaller($dateTo, 'plan_date_bigint', $criteria);
        $criteria->select   = 'sum(qty_real) as qty_real';
        $mTruckPlan         = TruckPlan::model()->find($criteria);
        return !empty($mTruckPlan->qty_real) ? $mTruckPlan->qty_real : 0;
    }

    /** @Author: KhueNM Aug 22, 2019
     *  @Todo: get number of signed contracts - gasSourceReport
     *  @Param:
     * */
    public function getNumOfSignedContracts() {
        return ActiveRecord::formatCurrency($this->qty_contract);
    }

    /** @Author: KhueNM Aug , 2019
     *  @Todo: get rest - gasSourceReport
     *  @Param:
     * */
    public function getRest($formatCurrency = true) {
        $result = $this->qty_contract - $this->getReceived();
        return $formatCurrency ? ActiveRecord::formatCurrency($result) : $result;
    }
    
    public function getRawRest(){
        return $this->getRest(false);
    }

    /** @Author: KhueNM Aug , 2019
     *  @Todo: format data save - gasSourceReport
     *  @Param:
     * */
    public function formatDataBeforeSave() {
        if (strpos($this->month, '/')) {
            $this->month = MyFormat::dateConverDmyToYmd($this->month);
            MyFormat::isValidDate($this->month);
        }
        $this->qty_contract = $this->convertWeight($this->qty_contract, GasSourceReport::TO_KG); // convert to kg before saving
    }
    
    /** @Author: KhueNM Aug, 2019
     *  @Todo:
     *  @Param:
     **/

    public function convertWeight($weight , $type){
        // convert to kg
        if($type == GasSourceReport::TO_KG){
            return $weight *= 1000;
        }else{ // convert to ton
            return $weight /= 1000;
        }
    }
    /** @Author: KhueNM Aug , 2019
     *  @Todo:
     *  @Param:
     * */
    public function getLinkDetailTruckPlan() {
        $dateFrom = MyFormat::dateConverYmdToDmy($this->month, '01-m-Y');
        $dateTo = MyFormat::dateConverYmdToDmy($this->month, 't-m-Y');
        $url = Yii::app()->createAbsoluteUrl('admin/truckPlan/index?' .
                "TruckPlan[supplier_id]=$this->supplier_id&" .
                "TruckPlan[supplier_warehouse_id]=$this->supplier_warehouse_id&" .
                "TruckPlan[plan_date_from]=$dateFrom&" .
                "TruckPlan[plan_date_to]=$dateTo"
                );
        $res  = '<a href="' . $url . '" target="_blank">';
        $res .= ActiveRecord::formatCurrency($this->getReceived());
        $res .= '</a>';
        return $res;
    }

    public function getMonth() {
        return MyFormat::dateConverYmdToDmy($this->month, 'm/Y');
    }

    public function getSupplier() {
        return empty($this->rSupplier) ? '' : $this->rSupplier->getFullName();
    }
    
    public function getTotal($records, $col_name, $isMethod = false){
        $total = 0;
        if(count($records) > 0){
            foreach ($records as $record) {
                $value = $isMethod ? $record->{$col_name}() : $record->$col_name;
                $total += $value;
            }
        }
        return ActiveRecord::formatCurrency($total);
    }
    
    /** @Author: LocNV Sep 3, 2019
     *  @Todo: run cron copy record for every month
     * Copy bc nguồn hàng gas bồn, tháng cũ copy sang tháng mới, chạy đầu tháng
     *  @Param: string $currentDate date with format Y-m-d
     **/
    public function cronCopyRecord($currentDate) {
        $dateRun            = date('Y-m-t');
        if($currentDate != $dateRun){
            return; // Chỉ chạy vào cuối tháng, lấy giá tháng này copy sang tháng sau
        }
        $previousDate       = new DateTime($dateRun);
        $previousDateBegin  = $previousDate->format('Y-m-01');
        $previousDateEnd    = $previousDate->format('Y-m-t');

        $criteria = new CDbCriteria();
        $criteria->addCondition("t.month >= '$previousDateBegin'");
        $criteria->addCondition("t.month <= '$previousDateEnd'");

        $aModelGasSourceReport = GasSourceReport::model()->findAll($criteria);

        if (count($aModelGasSourceReport) > 0) {
            $previousDate->modify('first day of next month');
            $month = $previousDate->format('Y-m-d');
            foreach ($aModelGasSourceReport as $model) {
                $aRowInsert[] = "(
                        $model->supplier_id,
                        $model->supplier_warehouse_id,
                        $model->qty_contract,
                        '$month',
                        '".date('Y-m-d H:i:s')."'
                    )";
            }
            $sql = $this->buildSqlInsert($aRowInsert);
            if(count($aRowInsert)){
                Yii::app()->db->createCommand($sql)->execute();
                // Send log
                $needMore['title']      = 'cronCopyRecord GasSourceReport';
                $needMore['list_mail']  = ['duongnv@spj.vn'];
                $strLog = '******* cronCopyRecord GasSourceReport done at '.date('d-m-Y H:i:s');
                SendEmail::bugToDev($strLog, $needMore);
                Logger::WriteLog($strLog);
            }
        }
    }
    
    /** @Author: LocNV Sep 3, 2019
     *  @Todo: build sql for run insert multi record
     *  @Param: array $aRowInsert array values of record
     **/
    public function buildSqlInsert($aRowInsert) {
        $tableName = GasSourceReport::model()->tableName();            
        $sql = "INSERT INTO $tableName (
                    supplier_id,
                    supplier_warehouse_id,
                    qty_contract,
                    month,
                    created_date
                ) values " . implode(',', $aRowInsert);
        return $sql;
    }

}
