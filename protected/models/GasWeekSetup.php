<?php

/**
 * This is the model class for table "{{_gas_week_setup}}".
 *
 * The followings are the available columns in table '{{_gas_week_setup}}':
 * @property string $id
 * @property integer $year
 * @property integer $month
 * @property integer $number_week
 * @property string $date_from
 * @property string $date_to
 */
class GasWeekSetup extends CActiveRecord
{
    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return GasWeekSetup the static model class
     */
    public static function model($className=__CLASS__)
    {
            return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
            return '{{_gas_week_setup}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
            return array(
                array('id, year, month, number_week, date_from, date_to', 'safe'),
            );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
            return array(
            );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'year' => 'Năm',
            'month' => 'Tháng',
            'number_week' => 'Tuần Số',
            'date_from' => 'Từ Ngày',
            'date_to' => 'Đến Ngày',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        $criteria=new CDbCriteria;
        $criteria->compare('t.id',$this->id,true);
        $criteria->compare('t.year',$this->year);
        $criteria->compare('t.month',$this->month);
        $criteria->compare('t.number_week',$this->number_week);
        $criteria->compare('t.date_from',$this->date_from,true);
        $criteria->compare('t.date_to',$this->date_to,true);
        $criteria->order = 't.year DESC, t.id DESC';

        return new CActiveDataProvider($this, array(
                'criteria'=>$criteria,
                'pagination'=>array(
                    'pageSize'=> 52,
                ),
        ));
    }

    /*
    public function activate()
    {
        $this->status = 1;
        $this->update();
    }

    public function deactivate()
    {
        $this->status = 0;
        $this->update();
    }
	*/

    public function defaultScope()
    {
            return array(
                    //'condition'=>'',
            );
    }
    
    /**
     * @Author: ANH DUNG Jul 24, 2014
     * @Todo: sử dụng cho tạo mới tuần của tháng
     * @Param: $model model GasWeekSetup
     */
    public static function SaveCreateWeek($model){
        self::DeleteByYearMonth($model->year, $model->month);
        if(is_array($model->date_from)){
            foreach($model->date_from as $key=>$date_from){
                if(empty($date_from)) continue;
                $mNew = new GasWeekSetup();
                $mNew->year = $model->year;
                $mNew->month = $model->month;
                $mNew->number_week = $key+1;
                $mNew->date_from = $date_from;
                $mNew->date_to = $model->date_to[$key];
                $mNew->save();
            }
        }
    }
    
    protected function beforeSave() {
        if(strpos($this->date_from, '/')){
            $this->date_from =  MyFormat::dateConverDmyToYmd($this->date_from);
        }
        if(strpos($this->date_to, '/')){
            $this->date_to =  MyFormat::dateConverDmyToYmd($this->date_to);
        }
        return parent::beforeSave();
    }
    
    public static function DeleteByYearMonth($year, $month){
        $criteria = new CDbCriteria();
        $criteria->compare('year', $year);
        $criteria->compare('month', $month);
        return self::model()->deleteAll($criteria);
    }
    
    /**
     * @Author: ANH DUNG Jul 25, 2014
     * @Todo: get week number of the month year
     * @Param: $year, $month
     * @Return: array model GasWeekSetup
     */
    public static function GetWeekNumber($year, $month){
        $criteria = new CDbCriteria();
        $criteria->compare('t.year', $year);
        $criteria->compare('t.month', $month);
        $criteria->order = 't.number_week ASC';
        return self::model()->findAll($criteria);
    }
    
    /**
     * @Author: ANH DUNG Jul 25, 2014
     * @Todo: get week of year store to session
     * @Param: $year
     */
    public static function InitSessionWeekNumberByYear($year){
        $session=Yii::app()->session;
        if(!isset($session['YEAR_WEEK_SETUP'])){
           $criteria = new CDbCriteria();
           $criteria->compare('t.year', $year);           
           $models = self::model()->findAll($criteria);
           $aRes = array();
           foreach($models as $item){
               $aRes[$year][$item->month][$item->number_week] = $item;
           }
           $session['YEAR_WEEK_SETUP'] = $aRes;
        }
    }    
    
    /**
     * @Author: ANH DUNG Jul 25, 2014
     * @Todo: get model week number of the month year
     * @Param: $year, $month, $number_week
     * @Return: array model GasWeekSetup
     */
    public static function GetModelWeekNumber($year, $month, $number_week){
        $session=Yii::app()->session;
        if(!isset($session['YEAR_WEEK_MODEL'][$year][$month][$number_week])){
            $criteria = new CDbCriteria();
            $criteria->compare('t.year', $year);
            $criteria->compare('t.month', $month);
            $criteria->compare('t.number_week', $number_week);
            $model = self::model()->find($criteria);
            $session['YEAR_WEEK_MODEL'][$year][$month][$number_week] = $model;            
        }
        return $session['YEAR_WEEK_MODEL'][$year][$month][$number_week];
    }    
    
    // lấy số thứ tự tuần hiện tại cho ngày hiện tại
    public static function GetCurrentWeekNumber(&$month, &$year){
        $criteria = new CDbCriteria();
//        $criteria->compare('t.year', date('Y'));
//        $criteria->compare('t.month', date('m'));
        $today = date('Y-m-d');
        $criteria->addCondition("t.date_from <= '$today' ");
        $criteria->order = 't.year DESC, t.month DESC, t.number_week DESC';
        $models = self::model()->findAll($criteria);        
        if(count($models)){
            $month = $models[0]->month;
            $year = $models[0]->year;
            return $models[0]->number_week;
        }
        return 0;// default trả về tuần error
    }
    
    // get model tuần by date
    public static function GetModelWeekByDate($date){
        $criteria = new CDbCriteria();
        $criteria->addCondition("t.date_from <= '$date' ");
        $criteria->order = 't.year DESC, t.month DESC, t.number_week DESC';
        $models = self::model()->findAll($criteria);
        if(count($models)){
            return $models[0];
        }
        return 0;// default trả về tuần error
    }
    
    public static function GetModelCurrentWeekNumber(){
        $criteria = new CDbCriteria();
        $today = date('Y-m-d');
        $criteria->addCondition("t.date_from<='$today'");
        $criteria->order = 't.year DESC, t.month DESC, t.number_week DESC';
        $models = self::model()->findAll($criteria);        
        if(count($models)){
            return $models[0];
        }
        return 0;// default trả về tuần error
    }
    
    public static function GetModelNextWeek(){
        $mCurrentWeek = GasWeekSetup::GetModelCurrentWeekNumber();
        if(empty($mCurrentWeek)) return 0;
        $date_from = MyFormat::modifyDays($mCurrentWeek->date_to, 1);
        $criteria = new CDbCriteria();
        $criteria->compare("t.date_from", $date_from);
        return $mNextWeek = self::model()->find($criteria);
    }
    
    public static function InitSessionAllWeekSetup(){
        $session=Yii::app()->session;
        if(!isset($session['ALL_MODEL_WEEK_SETUP'])){
           $models = self::model()->findAll();
           $aRes = array();
           foreach($models as $item){
               $aRes[$item->id] = $item;
           }
           $session['ALL_MODEL_WEEK_SETUP'] = $aRes;
        }
        return $session['ALL_MODEL_WEEK_SETUP'];
    }
    
}