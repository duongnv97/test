<?php

/**
 * This is the model class for table "{{_gas_app_order_detail}}".
 *
 * The followings are the available columns in table '{{_gas_app_order_detail}}':
 * @property string $id
 * @property string $app_order_id
 * @property integer $status_debit
 * @property integer $paid_gas_remain
 * @property string $agent_id
 * @property integer $type
 * @property integer $type_of_order_car
 * @property string $date_delivery
 * @property string $customer_id
 * @property integer $type_customer
 * @property string $sale_id
 * @property string $province_id_agent
 * @property integer $status
 * @property string $parent_chain_store
 * @property string $uid_login
 * @property integer $uid_login_role
 * @property string $uid_confirm
 * @property string $employee_maintain_id
 * @property string $user_id_executive
 * @property string $driver_id
 * @property string $car_id
 * @property integer $reason_false
 * @property integer $pay_direct
 * @property string $pay_back
 * @property string $phu_xe_1
 * @property string $phu_xe_2
 * @property integer $road_route
 * @property string $pay_back_amount
 * @property string $discount
 * @property integer $materials_type_id
 * @property integer $materials_id
 * @property string $qty
 * @property string $price
 * @property string $amount
 * @property string $seri
 * @property string $kg_empty
 * @property string $kg_has_gas
 */
class GasAppOrderCDetailReal extends CActiveRecord
{
    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return GasAppOrderDetailReal the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{_gas_app_order_detail}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        return array(
            array('id, app_order_id, status_debit, paid_gas_remain, agent_id, type, type_of_order_car, date_delivery, customer_id, type_customer, sale_id, province_id_agent, status, parent_chain_store, uid_login, uid_login_role, uid_confirm, employee_maintain_id, user_id_executive, driver_id, car_id, reason_false, pay_direct, pay_back, phu_xe_1, phu_xe_2, road_route, pay_back_amount, discount, materials_type_id, materials_id, qty, price, amount, seri, kg_empty, kg_has_gas', 'safe'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        return [];
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return [];
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        $criteria=new CDbCriteria;
        $criteria->order = 't.id DESC';
        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
            'pagination'=>array(
                'pageSize'=> Yii::app()->user->getState('pageSize',Yii::app()->params['defaultPageSize']),
            ),
        ));
    }
    
    /** @Author: ANH DUNG Sep 29, 2017
     *  @Todo: save to detail để chạy report
     */
    public function detailMakeNew(&$mAppOrder) {
        if($mAppOrder->status != GasAppOrder::STATUS_COMPPLETE){
            return ;// Allow dc với đại lý chưa chạy app thì sẽ không hỗ trợ báo cáo này
        }
        // 1. delete all detail
        $this->detailDelete($mAppOrder);
        $aRowInsert = [];
        $this->detailMakeNewBuildSql($aRowInsert, $mAppOrder);
        $tableName = GasAppOrderCDetailReal::model()->tableName();
        $sql = "insert into $tableName (app_order_id,
                    status_debit,
                    paid_gas_remain,
                    agent_id,
                    type,
                    type_of_order_car,
                    date_delivery,
                    customer_id,
                    type_customer,
                    sale_id,
                    province_id_agent,
                    status,
                    parent_chain_store,
                    uid_login,
                    uid_login_role,
                    uid_confirm,
                    employee_maintain_id,
                    user_id_executive,
                    driver_id,
                    car_id,
                    reason_false,
                    pay_direct,
                    pay_back,
                    phu_xe_1,
                    phu_xe_2,
                    road_route,
                    pay_back_amount,
                    discount,
                    total_gas,
                    total_gas_du,
                    materials_type_id,
                    materials_id,
                    qty,
                    price,
                    amount,
                    seri,
                    kg_empty,
                    kg_has_gas
                    ) values ".implode(',', $aRowInsert);
        if(count($aRowInsert)>0)
            Yii::app()->db->createCommand($sql)->execute();
    }
    public function detailDelete($mAppOrder) {
        $criteria = new CDbCriteria();
        $criteria->addCondition('app_order_id=' . $mAppOrder->id);
        GasAppOrderCDetailReal::model()->deleteAll($criteria);
    }
    
    public function detailMakeNewBuildSql(&$aRowInsert, $mAppOrder) {
        $aGas   = $mAppOrder->getJsonDataField('info_gas');
        $aVo    = $mAppOrder->getJsonDataField('info_vo');
        $aData  = array_merge($aGas, $aVo);
        $putDataOneTime = true;
        foreach($aData as $item):
            if(empty($item['materials_id'])){
                continue ;
            }
            $pay_back = $pay_back_amount = $discount = $total_gas = $total_gas_du = 0;
            if($putDataOneTime){
                $pay_back           = $mAppOrder->pay_back;
                $pay_back_amount    = $mAppOrder->pay_back_amount;
                $discount           = $mAppOrder->discount;
                $total_gas          = $mAppOrder->total_gas;
                $total_gas_du       = $mAppOrder->total_gas_du;
                $putDataOneTime     = false;
            }
            $materials_type_id      = $item['materials_type_id'];
            $materials_id           = $item['materials_id'];
            $qty                    = $item['qty_real'];
            $price                  = $item['price'];
            $amount                 = $item['amount'];
            $seri                   = $item['seri'];
            $kg_empty               = $item['kg_empty'];
            $kg_has_gas             = $item['kg_has_gas'];
            $aRowInsert[]=
                "('$mAppOrder->id',
                    '$mAppOrder->status_debit',
                    '$mAppOrder->paid_gas_remain',
                    '$mAppOrder->agent_id',
                    '$mAppOrder->type',
                    '$mAppOrder->type_of_order_car',
                    '$mAppOrder->date_delivery',
                    '$mAppOrder->customer_id',
                    '$mAppOrder->type_customer',
                    '$mAppOrder->sale_id',
                    '$mAppOrder->province_id_agent',
                    '$mAppOrder->status',
                    '$mAppOrder->parent_chain_store',
                    '$mAppOrder->uid_login',
                    '$mAppOrder->uid_login_role',
                    '$mAppOrder->uid_confirm',
                    '$mAppOrder->employee_maintain_id',
                    '$mAppOrder->user_id_executive',
                    '$mAppOrder->driver_id',
                    '$mAppOrder->car_id',
                    '$mAppOrder->reason_false',
                    '$mAppOrder->pay_direct',
                    '$pay_back',
                    '$mAppOrder->phu_xe_1',
                    '$mAppOrder->phu_xe_2',
                    '$mAppOrder->road_route',
                    '$pay_back_amount',
                    '$discount',
                    '$total_gas',
                    '$total_gas_du',
                    '$materials_type_id',
                    '$materials_id',
                    '$qty',
                    '$price',
                    '$amount',
                    '$seri',
                    '$kg_empty',
                    '$kg_has_gas'
                )";
        endforeach;
    }
    
}