<?php

/**
 * This is the model class for table "{{_borrow}}".
 *
 * The followings are the available columns in table '{{_borrow}}':
 * @property string $id
 * @property string $credit_type
 * @property string $credit_owner_id
 * @property string $contract_no
 * @property integer $borrow_type
 * @property string $company_id
 * @property string $bank_id
 * @property string $borrow_date
 * @property integer $borrow_month
 * @property string $expiry_date
 * @property string $interest_rate
 * @property string $borrow_amount
 * @property string $pay_amount
 * @property string $remain_amount
 * @property string $description
 * @property string $note
 * @property string $uid_login
 * @property string $created_date
 */
class Borrow extends BaseSpj
{
    const CREDIT_TYPE_BORROW            = 1;// Đi vay
    const CREDIT_TYPE_LEND              = 2;// Cho vay
    
    const BORROW_SHORT          = 1;// Vay ngắn hạn
    const BORROW_MIDDLE_LONG    = 2;// vay trung dài hạn
    const BORROW_BAO_LANH_TT    = 3;// vay bảo lãnh TT
    const BORROW_LEND_TIET_KIEM = 4;// Tiết kiệm
    const BORROW_LEND_CHO_VAY   = 5;// Cho vay
    public $borrow_date_from, $borrow_date_to, $expiry_date_from, $expiry_date_to, $autocomplete_name, $autocomplete_name1, $mDetail;
    
    /**
     * @Author: ANH DUNG Jul 30, 2016
     */
    public function getArrayType() {
        $aRes = [
            Borrow::BORROW_SHORT        => 'Vay ngắn hạn',
            Borrow::BORROW_MIDDLE_LONG  => 'Vay trung dài hạn',
            Borrow::BORROW_BAO_LANH_TT  => 'Vay bảo lãnh TT',
        ];
        if($this->credit_type == Borrow::CREDIT_TYPE_LEND){
            $aRes = [
                Borrow::BORROW_LEND_TIET_KIEM   => 'Tiết kiệm',
                Borrow::BORROW_LEND_CHO_VAY     => 'Cho vay',
            ];
        }
        return $aRes;
    }
    public function getArrayTypeShortName() {
        return array(
            Borrow::BORROW_SHORT        => 'NH',
            Borrow::BORROW_MIDDLE_LONG  => 'TDH',
            Borrow::BORROW_BAO_LANH_TT  => 'BLTT',
        );
    }
    
    public function getBorrowType() {
        $aType = $this->getArrayType();
        return isset($aType[$this->borrow_type]) ? $aType[$this->borrow_type] : '';
    }
    
    public function getUidViewAll() {
        return [
            GasTickets::DIEU_PHOI_HIEU_TM,
        ];
    }
    
    /** @Author: ANH DUNG Jul 20, 2018
     *  @Todo: get list role not search trong field credit_owner_id
     **/
    public static function getRoleMemNotSearch(){
        return array(
           ROLE_MANAGER, ROLE_ADMIN, ROLE_SECURITY_SYSTEM,
           ROLE_MEMBER, ROLE_CUSTOMER, ROLE_AGENT, ROLE_CAR, ROLE_BANK, ROLE_ASSETS, ROLE_FAMILY,
            ROLE_CANDIDATE, ROLE_PARTNER
        );
     }
    
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{_borrow}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        return array(
            array('contract_no, borrow_type, borrow_date, company_id, bank_id, interest_rate, borrow_amount', 'required', 'on'=>'create, update'),
            array('credit_owner_id, contract_no, borrow_type, borrow_date, company_id, bank_id, interest_rate, borrow_amount', 'required', 'on'=>'LendCreate, LendUpdate'),
//            array('contract_no','unique','message'=>'Mã khế ước bạn nhập đã tồn tại trên hệ thống','on'=>'create, update'),
            array('contract_no', 'length', 'max'=>30),
            array('id, contract_no, borrow_type, company_id, bank_id, borrow_date, borrow_month, expiry_date, interest_rate, borrow_amount, pay_amount, remain_amount, description, note, uid_login, created_date', 'safe'),
            array('credit_owner_id, credit_type, borrow_date_from, borrow_date_to, expiry_date_from, expiry_date_to', 'safe'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        return array(
            'rUidLogin' => array(self::BELONGS_TO, 'Users', 'uid_login'),
            'rCompany' => array(self::BELONGS_TO, 'Users', 'company_id'),
            'rBank' => array(self::BELONGS_TO, 'Users', 'bank_id'),
            'rDetail' => array(self::HAS_MANY, 'BorrowDetail', 'borrow_id', 'order'=>'rDetail.id DESC'),
            'rCreditOwner' => array(self::BELONGS_TO, 'Users', 'credit_owner_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        $aRes = array(
            'id' => 'ID',
            'contract_no' => 'Mã khế ước',
            'borrow_type' => 'Hình thức',
            'company_id' => 'Công ty',
            'bank_id' => 'Ngân hàng',
            'borrow_date' => 'Ngày vay',
            'borrow_month' => 'Thời gian vay (tháng)',
            'expiry_date' => 'Ngày đáo hạn',
            'interest_rate' => 'Lãi suất',
            'borrow_amount' => 'Tiền đã vay',
            'pay_amount' => 'Tiền đã đáo hạn',
            'remain_amount' => 'Tiền còn vay',
            'description' => 'Diễn giải',
            'note' => 'Ghi chú',
            'uid_login' => 'Người tạo',
            'created_date' => 'Ngày tạo',
            'borrow_date_from' => 'Ngày vay từ',
            'borrow_date_to' => 'đến',
            'expiry_date_from' => 'Ngày đáo hạn từ',
            'expiry_date_to' => 'đến',
            'credit_owner_id' => 'Người đứng tên',
        );
        
        if($this->credit_type == Borrow::CREDIT_TYPE_LEND){
            $aRes['contract_no']        = 'Số sổ';
            $aRes['borrow_amount']      = 'Tiền cho vay';
            $aRes['expiry_date']        = 'Ngày trả';
            $aRes['expiry_date_from']   = 'Ngày trả từ';
            $aRes['pay_amount']         = 'Tiền đã trả';
        }
        
        return $aRes;
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        $cRole = MyFormat::getCurrentRoleId();
        $cUid = MyFormat::getCurrentUid();        
        $criteria=new CDbCriteria;
        $criteria->compare('t.contract_no',$this->contract_no,true);
        $criteria->compare('t.borrow_type',$this->borrow_type);
        $criteria->compare('t.company_id',$this->company_id);
        $criteria->compare('t.bank_id',$this->bank_id);
        $criteria->compare('t.borrow_month',$this->borrow_month);
        $criteria->compare('t.uid_login',$this->uid_login);
//        add credit type
        $criteria->compare('t.credit_owner_id', $this->credit_owner_id);
        $criteria->compare('t.credit_type', $this->credit_type);
        
        if(!empty($this->borrow_date_from)){
            $date_from = MyFormat::dateDmyToYmdForAllIndexSearch($this->borrow_date_from);
            $criteria->addCondition("t.borrow_date >= '$date_from'");
        }
        if(!empty($this->borrow_date_to)){
            $date_to = MyFormat::dateDmyToYmdForAllIndexSearch($this->borrow_date_to);
            $criteria->addCondition("t.borrow_date <= '$date_to'");
        }
        if(!empty($this->expiry_date_from)){
            $date_from = MyFormat::dateDmyToYmdForAllIndexSearch($this->expiry_date_from);
            $criteria->addCondition("t.expiry_date >= '$date_from'");
        }
        if(!empty($this->expiry_date_to)){
            $date_to = MyFormat::dateDmyToYmdForAllIndexSearch($this->expiry_date_to);
            $criteria->addCondition("t.expiry_date <= '$date_to'");
        }
//        if($cRole == ROLE_ACCOUNTING){// Close to move function handleViewOther
//            $criteria->compare('t.uid_login', $cUid); // người nào tạo thì chỉ được nhìn thấy của người đó
////            $criteria->addInCondition("t.company_id", $this->getAccountingCompany($cUid));
//        }
        $this->handleViewOther($cRole, $cUid, $criteria);

        $criteria->order = "t.id DESC";
        if(!empty($this->expiry_date_from) && !empty($this->expiry_date_to)){
            $criteria->order = "t.expiry_date ASC, t.id DESC";
        }
        $_SESSION['data-excel'] = new CActiveDataProvider($this, array(
            'pagination'=>false,
            'criteria'=>$criteria,
//            'sort' => $sort,
        ));
        $_SESSION['ModelForExcel'] = $this;
        
        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
            'pagination'=>array(
                'pageSize'=> 60,
            ),
        ));
    }
    /**
     * @Author: ANH DUNG Nov 10, 2016
     * @Todo: xử lý cho user xem của nhau
     */
    public function handleViewOther($cRole, $cUid, &$criteria) {
        $idCheck = 410;
        if($cRole == ROLE_ACCOUNTING){
//            if(in_array($cUid, $this->getUidViewAll())){
//            }elseif($cUid == GasConst::UID_PHUOC_HT){
//                $criteria->addCondition('t.uid_login='.$cUid.' OR ( t.uid_login='.GasConst::UID_PHUOC_ND. ' AND t.id < '.$idCheck.')'); // Cty Kiên Long thì Huỳnh Thanh Phước xem được toàn bộ của Nguyễn Đức Phước làm trước.
//            }elseif($cUid == GasConst::UID_HUE_LT){// Apr 17, 2017 Huệ xem Biển đông
//                $criteria->addCondition('t.uid_login='.$cUid.' OR ( t.company_id='.GasConst::COMPANY_BIEN_DONG. ' )'); // Cty Kiên Long thì Huỳnh Thanh Phước xem được toàn bộ của Nguyễn Đức Phước làm trước.                
//            }elseif($cUid == GasConst::UID_THAM_LT){ // Cty DKMN thì Mai Thị Như Quỳnh xem được toàn bộ phần nhập của Hoàng Vũ Sơn làm trước.
//                $sParamsIn = implode(',', array(GasConst::UID_THAM_LT, GasConst::UID_PHUOC_HT));
//                $criteria->addCondition("t.uid_login IN ($sParamsIn)".' OR ( t.uid_login='.GasConst::UID_PHUOC_ND. ' )'); // Dec 23, 2016 Thẩm thay Phước làm Cty Kiên Long thì Huỳnh Thanh Phước xem được toàn bộ của Nguyễn Đức Phước làm trước.
//            }elseif($cUid == GasLeave::UID_QUYNH_MTN){ // Cty DKMN thì Mai Thị Như Quỳnh xem được toàn bộ phần nhập của Hoàng Vũ Sơn làm trước.
//                $criteria->addCondition('t.uid_login='.$cUid.' OR ( t.uid_login='.GasConst::UID_ANH_LD.' OR ( t.uid_login='.GasConst::UID_SON_HV. ' OR t.uid_login='.GasConst::UID_PHUOC_ND. ' OR t.uid_login='.GasConst::UID_THAM_LT. ')'); // Cty DKMN thì Mai Thị Như Quỳnh xem được toàn bộ phần nhập của Hoàng Vũ Sơn
//            }elseif($cUid == GasConst::UID_SON_HV){ // Cty DKMN thì Mai Thị Như Quỳnh xem được toàn bộ phần nhập của Hoàng Vũ Sơn làm trước.
//                $criteria->addCondition('t.uid_login='.$cUid.' OR ( t.uid_login='.GasConst::UID_THAO_LT. ' AND t.id < '.$idCheck.')'); // Cty DKMN thì Mai Thị Như Quỳnh xem được toàn bộ phần nhập của Hoàng Vũ Sơn
//            }elseif($cUid == GasLeave::PHUONG_PTK){
//                $criteria->addCondition('t.uid_login='.$cUid.' OR ( t.company_id='.GasConst::COMPANY_KHL_ANH_DUONG. ')'); // MỞ DÙM CHỊ QUẢN LÝ VAY CTY ÁNH DƯƠNG
//            }elseif($cUid == GasConst::UID_NHI_DT){
//                $criteria->addCondition('t.uid_login='.$cUid.' OR ( t.company_id='.GasConst::COMPANY_DK_BINH_DINH. ')'); //EM MỞ CHO NHI – CTY CP DẦU KHÍ BÌNH ĐỊNH NỮA NHÉ.
//            }elseif($cUid == GasConst::UID_ANH_LD){
//                $criteria->addCondition('t.uid_login='.$cUid.' OR ( t.company_id='.GasConst::COMPANY_KIEN_LONG. ')'); // Lê Đức Anh các khoản vay của cty Kiên Long
//            }
//            else{
//                $criteria->addCondition('t.uid_login='.$cUid); // người nào tạo thì chỉ được nhìn thấy của người đó
//            }
            
        }
    }

    protected function beforeValidate() {
        $this->trimData();
        $this->borrow_amount = MyFormat::removeComma($this->borrow_amount);
        return parent::beforeValidate();
    }
    
    protected function afterValidate() {
        $aScenario = array('create', 'update');
        if(in_array($this->scenario, $aScenario) && (empty($this->expiry_date) && empty($this->borrow_month)) ){
            $this->addError('borrow_date', 'Bạn phải nhập ngày hết hạn hoặc thời gian vay');
        }
        return parent::afterValidate();
    }
    
    public function trimData(){
        $this->contract_no  = trim($this->contract_no);
        $this->description  = trim($this->description);
        $this->note         = trim($this->note);
    }
    
    protected function beforeSave() {
        if(strpos($this->borrow_date, '/')){
            $this->borrow_date = MyFormat::dateConverDmyToYmd($this->borrow_date);
            MyFormat::isValidDate($this->borrow_date);
        }
        if(strpos($this->expiry_date, '/')){
            $this->expiry_date = MyFormat::dateConverDmyToYmd($this->expiry_date);
            MyFormat::isValidDate($this->expiry_date);
        }
        $this->calcExpiryDate();
        $this->remain_amount = $this->borrow_amount - $this->pay_amount;

        return parent::beforeSave();
    }
    
    /**
     * @Author: ANH DUNG Jul 31, 2016
     * @Todo: tính toán ngày hết hạn của khoản vay
     */
    public function calcExpiryDate() {
        $aScenario = array('create', 'update');
        if($this->credit_type == Borrow::CREDIT_TYPE_BORROW && in_array($this->scenario, $aScenario)){
            if(!empty($this->borrow_month)){
                $this->expiry_date = MyFormat::modifyDays($this->borrow_date, $this->borrow_month, "+", "month");
            }
        }
    }
    
    /**
     * @Author: ANH DUNG Jul 31, 2016
     */
    public function getListdataCompany() {
        $aRes = Users::getArrDropdown(ROLE_COMPANY);
        $aRes[GasConst::UID_DUNG_NT]    = 'Nguyễn Tiến Dũng';
        $aRes[GasLeave::UID_QUYNH_MTN]  = 'Mai Thị Như Quỳnh';
        $aRes[GasSupportCustomer::UID_THUY_MDX] = 'Mai Đào Xuân Thủy';
        $aRes[952191]                           = 'Nguyễn Thị Minh';
        
        return $aRes;
    }
    public function getListdataBank() {
        return Users::getArrDropdown(ROLE_BANK);
    }
    
    public function getContractNo() {
        return $this->contract_no;
    }
    public function getCreditOwner() {
        $mUser = $this->rCreditOwner;
        if($mUser){
            return $mUser->getFullName();
        }
        return "";
    }
    public function getCompany() {
        $mUser = $this->rCompany;
        if($mUser){
            return $mUser->getFullName();
        }
        return "";
    }
    public function getBank() {
        $mUser = $this->rBank;
        if($mUser){
            return $mUser->getFullName();
        }
        return "";
    }
    public function getUidLogin() {
        $mUser = $this->rUidLogin;
        if($mUser){
            return $mUser->getFullName();
        }
        return "";
    }
    public function getBorrowDate() {
        return MyFormat::dateConverYmdToDmy($this->borrow_date);
    }
    public function getBorrowMonth() {
        return $this->borrow_month;
    }
    public function getExpiryDate() {
        return MyFormat::dateConverYmdToDmy($this->expiry_date);
    }
    public function getInterestRate($format = false) {
        if($format){
            return ActiveRecord::formatCurrency($this->interest_rate) . " %";
        }
        return $this->interest_rate;
    }
    public function getBorrowAmount($format = false) {
        if($format){
            $span = "<span class='borrow_amount display_none'>{$this->borrow_amount}</span>";
            return ActiveRecord::formatCurrency($this->borrow_amount)." $span";
        }
        return $this->borrow_amount;
    }
    public function getPayAmount($format = false) {
        if($format){
            $span = "<span class='pay_amount display_none'>{$this->pay_amount}</span>";
            return ActiveRecord::formatCurrency($this->pay_amount)." $span";
        }
        return $this->pay_amount;
    }
    public function getRemainAmount($format = false) {
        if($format){
            $span = "<span class='remain_amount display_none'>{$this->remain_amount}</span>";
            return ActiveRecord::formatCurrency($this->remain_amount)." $span";
        }
        return $this->remain_amount;
    }
    public function getDescription() {
        return nl2br($this->description);
    }
    public function getNote() {
        return nl2br($this->note);
    }
    public function getCreatedDate($fomat = 'd/m/Y H:i') {
        return MyFormat::dateConverYmdToDmy($this->created_date, "d/m/Y H:i");
    }
    public function getCreatedInfo() {
        $res = MyFormat::dateConverYmdToDmy($this->created_date, "d/m/Y H:i");
        $res .= "<br><b>{$this->getUidLogin()}</b>";
        $mLatest = $this->getLatestDetail();
        if($mLatest){
            $res .= "<br><b>ĐH mới nhất: </b>".$mLatest->getDatePay()." <b>".$mLatest->getPayAmount(true)."</b>";
        }
        return $res;
    }
    
    /**
     * @Author: ANH DUNG Aug 02, 2016
     * @Todo: calc Ngày chờ đáo hạn
     */
    public function getDayWaitExpiryDate() {
        if($this->remain_amount < 1){
            return "";
        }
        $days = MyFormat::getNumberOfDayBetweenTwoDate(date("Y-m-d"), $this->expiry_date);
        if(!MyFormat::compareTwoDate($this->expiry_date, date("Y-m-d"))){
            $days = 0 - $days;
        }
        if($days < 0){
            return "<span class='high_light_tr'>$days</span>";
        }
        return $days;
    }
    
    /**
     * @Author: ANH DUNG Apr 21, 2016
     * @Todo: lấy detail mới nhất của record
     */
    public function getLatestDetail() {
        $criteria = new CDbCriteria();
        $criteria->compare("t.borrow_id", $this->id);
        $criteria->limit = 1;
        $criteria->order = "t.id DESC";
        return BorrowDetail::model()->find($criteria);
    }
    
    // Now 06, 2015 lấy số ngày cho phép đại lý cập nhật
    public function getDayAllowUpdate()
    {
        return Yii::app()->params['DaysUpdateBorrow'];
    }
    
    public function getDetail() {
        $res = "<b>Ngày vay: </b>".$this->getBorrowDate();
        $res .= "<br><b>Thời gian vay: </b>".$this->getBorrowMonth()." tháng";
        $res .= "<br><b>Lãi suất: </b>".$this->getInterestRate(true)."";
        if(!empty($this->getDescription())){
        $res .= "<br><b>Diễn giải: </b>".$this->getDescription()."";
        }
        if(!empty($this->getNote())){
        $res .= "<br><b>Ghi chú: </b>".$this->getNote()."";
        }
        return $res;
    }
    
    /**
     * @Author: Anh Dung Jul 31 2016
     * Allow someone see update button
     * @return bool
     */
    public function canUpdate(){
        $cUid   = MyFormat::getCurrentUid();
        $cRole  = MyFormat::getCurrentRoleId();
        if ($cRole == ROLE_ADMIN) {
            return true;
        }
        if ($cRole != ROLE_ADMIN && $cUid != $this->uid_login) {
            return false; // cho phép admin sửa hết, còn những user khác thì ai tạo thì dc sửa của người đó
        }
//        if ($this->status == GasSettle::STA_WAIT_SETTLE) { // Anh Dung close Jul 10, 2016 - chắc sẽ ko dùng trạng thái này nữa
//            return true;
//        }

        $dayAllow = date('Y-m-d');
        $dayAllow = MyFormat::modifyDays($dayAllow, $this->getDayAllowUpdate(), '-');
        return MyFormat::compareTwoDate($this->created_date, $dayAllow);
    }
    
    /**
     * @Author: Anh Dung Aug 01 2016
     * Allow someone see update button
     * @return bool
     */
    public function canCreateDetail()
    {
        return true;
        $cRole = Yii::app()->user->role_id;
        $cUid = Yii::app()->user->id;
        $dayAllow = date('Y-m-d');
        $dayAllow = MyFormat::modifyDays($dayAllow, $this->getDayAllowUpdate(), '-');
        return MyFormat::compareTwoDate($this->created_date, $dayAllow);
    }

    /**
     * @Author: ANH DUNG Aug 19, 2016
     * @Todo: cập nhật một số cột khi update detail
     * khi thêm mới hoặc khi xóa 1 detail đi
     * @param: $amountChange amount thay đổi có thể là + OR -
     */
    public function updateAfterAddDetail($amountChange) {
        $this->pay_amount   += $amountChange;
        $this->update(array('pay_amount', 'remain_amount'));
    }
    
    /**
     * @Author: ANH DUNG Aug 01, 2016
     */
    public function canDeleteDetail() {
        return GasCheck::isAllowAccess("borrow", 'DeleteDetail');
    }
    /**
     * @Author: ANH DUNG Aug 01, 2016
     * @Todo: get html record detail
     */
    public function getRecordDetail() {
        $models = $this->rDetail;
        if(count($models) == 0){
            return "";
        }
        $alert_text = "Bạn chắc chắn muốn xóa";
        
        
        $res = "";
        $res .= "<table class='materials_table hm_table '><tbody>";
        $res .= "<thead><tr>";
        $res .= "<td>#</td>";
        $res .= "<td class='item_c item_b w-100'>Ngày</td>";
        $res .= "<td class='item_c item_b w-100'>Số tiền</td>";
        $res .= "<td class='item_c item_b'>Người tạo</td>";
        $res .= "<td class='item_c item_b'>Action</td>";
        $res .= "</tr></thead>";
        foreach($models as $key => $mDetail){
            $deleteUrl = Yii::app()->createAbsoluteUrl("admin/borrow/deleteDetail", array('id'=>$mDetail->id));
            $deleteHtml = "<a alert_text='$alert_text' class='remove_icon_only btn_closed_tickets' href='$deleteUrl'></a>";
            if(!$this->canDeleteDetail()){
                $deleteHtml = "";
            }
            $res .= "<tr>";
                $res .= "<td>".($key+1)."</td>";
                $res .= "<td class='item_c'>{$mDetail->getDatePay()}</td>";
                $res .= "<td class='item_r'>{$mDetail->getPayAmount(true)}</td>";
                $res .= "<td class='item_c'>{$mDetail->getUidLogin()}</td>";
                $res .= "<td class='item_c'>$deleteHtml</td>";
            $res .= "</tr>";
        }
        $res .= "</tbody></table>";
        return $res;
    }

    protected function beforeDelete() {
        MyFormat::deleteModelDetailByRootId("BorrowDetail", $this->id, 'borrow_id');
        return parent::beforeDelete();
    }
    
    /**
     * @Author: ANH DUNG Aug 02, 2016
     * @Todo: check user kế toán + kế toán trưởng có thể access vào phần quản lý vay không
     */
    public function canAccess() {
        $session = Yii::app()->session;
        $cRole  = MyFormat::getCurrentRoleId();
        $cUid   = MyFormat::getCurrentUid();
        $aRoleAllow = [ROLE_ACCOUNTING, ROLE_ACCOUNTING_ZONE];
//        if($cRole == ROLE_ACCOUNTING){
        if(in_array($cRole, $aRoleAllow)){
            if(!in_array($cUid, $session['BORROW_ACCOUNTING'])){
                return false;
            }
        }elseif($cRole == ROLE_CHIEF_ACCOUNTANT){
            if($cUid != GasLeave::UID_CHIEF_ACCOUNTANT){
                return false;
            }
        }
        return true;
    }
    
    /**
     * @Author: ANH DUNG Aug 02, 2016
     * @Todo: INIT SOME SESSION id các kế toán dc setup cho phép quản lý và tạo mới borrow
     */
    public function initSessionAccounting() {
        $session = Yii::app()->session;
        if(!isset($session['BORROW_ACCOUNTING'])){
            $session['BORROW_ACCOUNTING'] = GasOneManyBig::getArrOfManyId(GasOneManyBig::TYPE_BORROW_ACCOUNTING, GasOneManyBig::TYPE_BORROW_ACCOUNTING);
        }
    }
    
    /**
     * @Author: ANH DUNG Aug 02, 2016
     * @Todo: get list company of user accounting
     */
    public function getAccountingCompany($cUid) {
        return GasOneMany::getArrOfManyId($cUid, ONE_COMPANY_USER);
    }
    
    /**
     * @Author: ANH DUNG Sep 27, 2016
     * @Todo: report vay thời điểm hiện tại
     */
    public function report() {
        $aRes = array();
        $criteria = new CDbCriteria();
        $criteria->addCondition('t.remain_amount > 0');
//        add credit type
        $credit_type = !empty($this->credit_type) ? $this->credit_type :  Borrow::CREDIT_TYPE_BORROW;
        $criteria->compare('t.credit_type',$credit_type);
//        get list bank
        $aBank      = $this->getListdataBank();
        $criteria->select = "sum(t.remain_amount) as remain_amount, t.bank_id, t.company_id, t.borrow_type";
        $criteria->group = "t.bank_id, t.company_id, t.borrow_type";
        $models = self::model()->findAll($criteria);
        foreach($models as $model){
            if(isset($aRes['DETAIL'][$model->company_id][$model->bank_id])){
                $aRes['DETAIL'][$model->company_id][$model->bank_id] += $model->remain_amount;
            }else{
                $aRes['DETAIL'][$model->company_id][$model->bank_id] = $model->remain_amount;
            }
            if(isset($aRes['SUM'][$model->company_id])){
                $aRes['SUM'][$model->company_id] += $model->remain_amount;
            }else{
                $aRes['SUM'][$model->company_id] = $model->remain_amount;
            }
            // chia ra theo 3 loại ngắn, trung, dài
            $aRes['BORROW_TYPE'][$model->company_id][$model->bank_id][$model->borrow_type] = $model->remain_amount;
            if(isset($aRes['SUM_BORROW_TYPE'][$model->company_id][$model->borrow_type])){
                $aRes['SUM_BORROW_TYPE'][$model->company_id][$model->borrow_type] += $model->remain_amount;
            }else{
                $aRes['SUM_BORROW_TYPE'][$model->company_id][$model->borrow_type] = $model->remain_amount;
            }
//            Thực hiện thêm vào aRes danh sách bank_id
            if(empty($aRes['BANK_LIST'][$model->bank_id])){
                $aRes['BANK_LIST'][$model->bank_id] = !empty($aBank[$model->bank_id]) ? $aBank[$model->bank_id] : '';
            }
        }
        $session = Yii::app()->session;
        $session['model-excel-report'] = $this;
        return $aRes;
    }
    
    
    /**
     * @Author: ANH DUNG Jun 15, 2017
     * @Todo: report vay đã đáo hạn ở tháng hiện tại
     */
    public function reportDaoHan() {
        $date_from  = date('Y-m').'-01';
        $date_to    = date('Y-m-t');
        $criteria = new CDbCriteria();
        $criteria->addCondition('t.credit_type=' . Borrow::CREDIT_TYPE_BORROW);
        $criteria->addBetweenCondition('t.expiry_date', $date_from, $date_to);
        $criteria->select = 'SUM(t.remain_amount) as remain_amount, t.company_id, t.bank_id';
        $criteria->group = 't.company_id, t.bank_id';
        $models = self::model()->findAll($criteria);
        $aRes = [];
        foreach($models as $item){
            $aRes[$item->company_id][$item->bank_id] = $item->remain_amount;
        }
        return $aRes;
    }
    
    /** @Author: KhueNM Sep 05, 2019
     *  @Todo: using for new Mortage report
     *  @Param:
     **/
    public function reportBorrowAmount() {
        $aRes = array();
        $criteria = new CDbCriteria();
        $criteria->addCondition('t.borrow_amount > 0');
//        add credit type
        $credit_type = !empty($this->credit_type) ? $this->credit_type :  Borrow::CREDIT_TYPE_BORROW;
        $criteria->compare('t.credit_type',$credit_type);
//        get list bank
        $aBank      = $this->getListdataBank();
        $criteria->select = "sum(t.borrow_amount) as borrow_amount, t.bank_id, t.company_id, t.borrow_type";
//        $criteria->group = "t.bank_id, t.company_id, t.borrow_type";
        $criteria->group = "t.bank_id, t.company_id";
        $models = self::model()->findAll($criteria);
        foreach($models as $model){
            if(isset($aRes['DETAIL'][$model->company_id][$model->bank_id])){
                $aRes['DETAIL'][$model->company_id][$model->bank_id] += $model->borrow_amount;
                        }else{
                $aRes['DETAIL'][$model->company_id][$model->bank_id] = $model->borrow_amount;
            }
            if(isset($aRes['SUM'][$model->company_id])){
                $aRes['SUM'][$model->company_id] += $model->borrow_amount;
            }else{
                $aRes['SUM'][$model->company_id] = $model->borrow_amount;
            }
            // chia ra theo 3 loại ngắn, trung, dài
            $aRes['BORROW_TYPE'][$model->company_id][$model->bank_id][$model->borrow_type] = $model->borrow_amount;
            if(isset($aRes['SUM_BORROW_TYPE'][$model->company_id][$model->borrow_type])){
                $aRes['SUM_BORROW_TYPE'][$model->company_id][$model->borrow_type] += $model->borrow_amount;
            }else{
                $aRes['SUM_BORROW_TYPE'][$model->company_id][$model->borrow_type] = $model->borrow_amount;
            }
//            Thực hiện thêm vào aRes danh sách bank_id
            if(empty($aRes['BANK_LIST'][$model->bank_id])){
                $aRes['BANK_LIST'][$model->bank_id] = !empty($aBank[$model->bank_id]) ? $aBank[$model->bank_id] : '';
            }
        }
//        $session = Yii::app()->session;
//        $session['model-excel-report'] = $this;
        return $aRes;
    }
    
}