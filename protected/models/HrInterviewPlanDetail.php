<?php

/**
 * This is the model class for table "{{_hr_interview_plan_detail}}".
 *
 * The followings are the available columns in table '{{_hr_interview_plan_detail}}':
 * @property string $id
 * @property string $interview_plan_id
 * @property string $note
 * @property string $created_by
 * @property string $created_date
 */
class HrInterviewPlanDetail extends CActiveRecord
{
    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return HrInterviewPlanDetail the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{_hr_interview_plan_detail}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        return array(
            array('interview_plan_id, note', 'required'),
            array('interview_plan_id, created_by', 'length', 'max'=>11),
            array('id, interview_plan_id, note, created_by, created_date', 'safe'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        return array(
            'rHrInterviewPlan' => array(self::BELONGS_TO, 'HrInterviewPlan', 'interview_plan_id'),
            'rCreatedBy' => array(self::BELONGS_TO, 'Users', 'created_by'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
            return array(
                'id' => 'ID',
                'interview_plan_id' => 'Mã Lịch phỏng vấn',
                'note' => 'Nhận xét',
                'created_by' => 'Người tạo',
                'created_date' => 'Ngày tạo',
            );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        $criteria=new CDbCriteria;

        $criteria->compare('t.id',$this->id,true);
        $criteria->compare('t.interview_plan_id',$this->interview_plan_id,true);
        $criteria->compare('t.note',$this->note,true);
        $criteria->compare('t.created_by',$this->created_by,true);
        $criteria->compare('t.created_date',$this->created_date,true);
        $criteria->order = 't.id DESC';
        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
            'pagination'=>array(
                'pageSize'=> Yii::app()->user->getState('pageSize',Yii::app()->params['defaultPageSize']),
            ),
        ));
    }
        
    //-----------------------------------------------------
    // Parent override methods
    //-----------------------------------------------------
    /**
     * Override before save method
     * @return Parent result
     */
    public function beforeSave() {
        $userId = isset(Yii::app()->user) ? Yii::app()->user->id : '';
        if ($this->isNewRecord) {
            // Handle created by
            if (empty($this->created_by)) {
                $this->created_by = $userId;
            }
            $this->created_date = CommonProcess::getCurrentDateTimeWithMySqlFormat();
        }
        return parent::beforeSave();
    }    

    //-----------------------------------------------------
    // Utility methods
    //-----------------------------------------------------
    public function canUpdate() {
        return true;
    }
    
}