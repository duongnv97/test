<?php

/**
 * This is the model class for table "{{_gas_orders_detail}}".
 *
 * The followings are the available columns in table '{{_gas_orders_detail}}':
 * @property string $id
 * @property string $orders_id
 * @property string $user_id_create
 * @property string $user_id_executive
 * @property integer $type
 * @property string $date_delivery
 * @property string $customer_id
 * @property integer $type_customer
 * @property string $sale_id
 * @property string $lpg_ton
 * @property integer $quantity_50
 * @property integer $quantity_45
 * @property integer $quantity_12
 * @property integer $quantity_6
 * @property string $other_material_id
 * @property string $quantity_other_material
 * @property string $work_content
 * @property string $note
 */
class GasOrdersDetail extends BaseSpj
{
    const WRONG_SAME_CUSTOMER   = 1;// trùng đơn hàng
    const WRONG_NOI_GIAO        = 2;// điều sai nơi giao
    const WRONG_GIAO_SAU_1      = 3;// giao sau, chờ phân bổ xe 1
    const WRONG_GIAO_SAU_2      = 4;// giao sau, chờ phân bổ xe 2
    const WRONG_GIAO_SAU_3      = 5;// giao sau, chờ phân bổ xe 3
    const WRONG_NEXT_DAY        = 6;// không thể giao trong ngày
    
    public $need_update;
    /**
     * @Author: ANH DUNG Oct 08, 2016
     * @Todo: get array wrong order detail
     */
    public function getArrayReasonWrong() {
        return array(
            GasOrdersDetail::WRONG_SAME_CUSTOMER  => 'Trùng đơn hàng',
            GasOrdersDetail::WRONG_NOI_GIAO       => 'Điều sai nơi giao',
            GasOrdersDetail::WRONG_GIAO_SAU_1     => 'Chờ phân bổ xe 1',
            GasOrdersDetail::WRONG_GIAO_SAU_2     => 'Chờ phân bổ xe 2',
            GasOrdersDetail::WRONG_GIAO_SAU_3     => 'Chờ phân bổ xe 3',
            GasOrdersDetail::WRONG_NEXT_DAY         => 'Đơn hàng không thể thực hiện trong ngày',
        );
    }
    
    public function getArrayGiaoSau() {
        return array(
            GasOrdersDetail::WRONG_GIAO_SAU_1,
            GasOrdersDetail::WRONG_GIAO_SAU_2,
            GasOrdersDetail::WRONG_GIAO_SAU_3,
        );
    }
    
    public static function model($className=__CLASS__)
    {
            return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
            return '{{_gas_orders_detail}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
            return array(
                array('customer_contact, created_by,car_number,id, orders_id, user_id_create, user_id_executive, type, date_delivery, customer_id, type_customer, sale_id, lpg_ton, quantity_50, quantity_45, quantity_12, quantity_6, other_material_id, quantity_other_material, work_content, note', 'safe'),
            );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
            return array(
                'customer' => array(self::BELONGS_TO, 'Users', 'customer_id'),
                'rOrders' => array(self::BELONGS_TO, 'GasOrders', 'orders_id'),
                'materials' => array(self::BELONGS_TO, 'GasMaterials', 'other_material_id'),
                'rUserIdCreate' => array(self::BELONGS_TO, 'Users', 'user_id_create'),
                'rUserIdExecutive' => array(self::BELONGS_TO, 'Users', 'user_id_executive'),
                'rDriver' => array(self::BELONGS_TO, 'Users', 'driver_id'),
                'rCreatedBy' => array(self::BELONGS_TO, 'Users', 'created_by'),
            );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'orders_id' => 'Orders',
            'user_id_create' => 'User Id Create',
            'user_id_executive' => 'User Id Executive',
            'type' => 'Type',
            'date_delivery' => 'Date Delivery',
            'customer_id' => 'Customer',
            'type_customer' => 'Type Customer',
            'sale_id' => 'Sale',
            'lpg_ton' => 'Lpg Ton',
            'quantity_50' => 'Quantity 50',
            'quantity_45' => 'Quantity 45',
            'quantity_12' => 'Quantity 12',
            'quantity_6' => 'Quantity 6',
            'other_material_id' => 'Other Material',
            'quantity_other_material' => 'Quantity Other Material',
            'work_content' => 'Work Content',
            'note' => 'Note',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        $criteria=new CDbCriteria;
        $criteria->compare('t.orders_id',$this->orders_id);
        return new CActiveDataProvider($this, array(
                'criteria'=>$criteria,
                'pagination'=>array(
                    'pageSize'=> Yii::app()->user->getState('pageSize',Yii::app()->params['defaultPageSize']),
                ),
        ));
    }

    /**
     * @Author: ANH DUNG Apr 08, 2014
     * @Todo: get delete all order detail by $orders_id
     * @Param: $orders_id 
     */    
    public static function deleteByOrderId($orders_id){
        $criteria = new CDbCriteria();
        $criteria->compare('orders_id', $orders_id);
        self::model()->deleteAll($criteria);
    }
    
    /**
     * @Author: ANH DUNG Apr 08, 2014
     * @Todo: update car number from web
     * @Param: $array order detail id + car_number
     * @Param: $model is GasOrders
     */    
    public static function updateCarNumber($model){
        try{
            $sql = $sqlAppOrder = '';
            $tableName          = self::model()->tableName();
            $tableNameAppOrder  = GasAppOrder::model()->tableName();
            $aCheckNotRequired  = GasOrdersDetail::model()->getArrayReasonWrong();
            $checkRequired = true; $hasError = false;
            // Feb 03, 2017
            $mAppOrder              = new GasAppOrder();
            $mAppOrder->obj_id      = $model->id;
            $arrAppOrder            = $mAppOrder->getByOrderId();
            // Feb 03, 2017
            
//            if(in_array($model->user_id_create, GasOrders::$ZONE_HCM)){
//                $checkRequired = true; 
//            }
            if(isset($_POST['GasOrdersDetail']['id']) && count($_POST['GasOrdersDetail']['id'])){
//                foreach($_POST['GasOrdersDetail']['id'] as $key=>$id){
                foreach($model->aModelOrderDetail as $key=>$mDetail){
                    $id             = isset($_POST['GasOrdersDetail']['id'][$key])?$_POST['GasOrdersDetail']['id'][$key]:'';
                    if($mDetail->id != $id){
                        continue ;
                    }
                    $car_number     = isset($_POST['GasOrdersDetail']['car_number'][$key])?$_POST['GasOrdersDetail']['car_number'][$key] : 0;
                    $driver_id      = isset($_POST['GasOrdersDetail']['driver_id'][$key])?$_POST['GasOrdersDetail']['driver_id'][$key] : 0;
                    $real_output    = isset($_POST['GasOrdersDetail']['real_output'][$key])?$_POST['GasOrdersDetail']['real_output'][$key]:'';
                    $note_2         = isset($_POST['GasOrdersDetail']['note_2'][$key])?$_POST['GasOrdersDetail']['note_2'][$key]:'';
                    $reason_wrong   = isset($_POST['GasOrdersDetail']['reason_wrong'][$key])?$_POST['GasOrdersDetail']['reason_wrong'][$key]:'';
                    
                    $road_route     = isset($_POST['GasOrdersDetail']['road_route'][$key])?$_POST['GasOrdersDetail']['road_route'][$key] : GasStoreCard::ROAD_ROUTE_NORMAL;
                    $phu_xe_1       = isset($_POST['GasOrdersDetail']['phu_xe_1'][$key])?$_POST['GasOrdersDetail']['phu_xe_1'][$key] : 0;
                    $phu_xe_2       = isset($_POST['GasOrdersDetail']['phu_xe_2'][$key])?$_POST['GasOrdersDetail']['phu_xe_2'][$key] : 0;
                    
                    if($checkRequired && (empty($car_number) || empty($driver_id)) && !array_key_exists($reason_wrong, $aCheckNotRequired)){
                        $hasError = true;
                    }
                    
                    $mDetail->car_number    = $car_number;
                    $mDetail->driver_id     = $driver_id;
                    $mDetail->real_output   = $real_output;
                    $mDetail->note_2        = $note_2;
                    $mDetail->road_route    = $road_route;
                    $mDetail->phu_xe_1      = $phu_xe_1;
                    $mDetail->phu_xe_2      = $phu_xe_2;
                    
                    $car_number     = MyFormat::removeBadCharacters($car_number); 
                    $driver_id      = MyFormat::removeBadCharacters($driver_id); 
                    $real_output    = MyFormat::removeBadCharacters($real_output);
                    $note_2         = MyFormat::removeBadCharacters($note_2);
                    if(!empty($id) && !$hasError){
                        $sql .= "UPDATE $tableName SET `car_number`=\"$car_number\", "
                            . "`driver_id`=\"$driver_id\", "
                            . "`road_route`=\"$road_route\", "
                            . "`phu_xe_1`=\"$phu_xe_1\", "
                            . "`phu_xe_2`=\"$phu_xe_2\", "
                            . "`real_output`=\"$real_output\", "
                            . "`reason_wrong`=\"$reason_wrong\", "
                            . "`note_2`=\"$note_2\" WHERE `id`=\"$id\" ;";
                        $mDetail->updateDriverToAppOrderBuildSql($sqlAppOrder, $tableNameAppOrder, $arrAppOrder, $model);
                    }
                }// end foreach($model->aModelOrderDetail

                if($checkRequired && $hasError){
                    $model->addError('id', 'Vui lòng nhập đầy đủ số xe, lái xe cho đơn hàng');
                    return ;
                }
                //UPDATE mytable SET (id, column_a, column_b) FROM VALUES ( (1, 12, 6), (2, 1, 45), (3, 56, 3), … );
                if(trim($sql) != ""){
                    Yii::app()->db->createCommand($sql)->execute();
                }
//                GasOrdersDetail::model()->updateDriverToAppOrder($sqlAppOrder);// vì đã update từng model rồi
            }
        } catch (Exception $ex) {
            throw new Exception($ex->getMessage());
        }
    }
    
    /**
     * @Author: ANH DUNG Jan 23, 2017
     * @Todo: build sql update back to AppOrder
     * @Param: $tableNameAppOrder is class GasAppOrder
     * @Param: $id of GasOrdersDetail
     */
    public function updateDriverToAppOrderBuildSql(&$sqlAppOrder, $tableNameAppOrder, $arrAppOrder, &$mOrderCar) {
        $aUpdate = ['driver_id', 'car_id', 'order_detail_note_2','road_route', 'phu_xe_2','phu_xe_1','json_info']; $notifyDriver = true;
        if($this->type == GasOrders::TYPE_DIEU_PHOI && (empty($this->driver_id) || !isset($arrAppOrder[$this->id]))){
            return ;
        }
        if($this->type == GasOrders::TYPE_AGENT && ( empty($this->driver_id) || !$mOrderCar->allowUpdateAgent || !isset($arrAppOrder[0])) ){
            return;
        }
        if($this->type == GasOrders::TYPE_DIEU_PHOI){
            $mAppOrder = $arrAppOrder[$this->id];
        }else{
            $mAppOrder = $arrAppOrder[0];
        }
        if($this->type == GasOrders::TYPE_AGENT){
            $mAppOrder->date_delivery   = $mOrderCar->date_delivery;
            $aUpdate[] = 'date_delivery';
            $aUpdate[] = 'date_delivery_bigint';
        }

        $aStatusCanChange = [GasAppOrder::STATUS_NEW, GasAppOrder::STATUS_CONFIRM, GasAppOrder::STATUS_PROCESSING];
        if(!in_array($mAppOrder->status, $aStatusCanChange)){
//            return; // không cập nhật với những đơn hàng đã hoàn thành hoặc hủy ??? tại sao
            // Oct1517 thử bỏ đoạn return này xem thế nào, chưa rõ tại sao lại ko cập nhật với những đơn hủy hoặc đã hoàn thành
        }
        // 1. decode all json
//       Không được xử lý cái này, vì sẽ sai SL khi tài xế đã hoàn thành => $mAppOrder->mapJsonFieldOneDecode('JSON_FIELD', 'json', 'baseArrayJsonDecode');// Xử lý change qty gas khi điều xe nhập lại số lượng
        $mAppOrder->mapJsonFieldOneDecode('JSON_FIELD_INFO', 'json_info', 'baseArrayJsonDecodeV1');
        
        if(!empty($mAppOrder->driver_id) && $mAppOrder->driver_id == $this->driver_id){
            $notifyDriver = false;
        }
        $mAppOrder->driver_id           = $this->driver_id;
        $mAppOrder->car_id              = $this->car_number;
        $mAppOrder->order_detail_note_2 = $this->note_2;
        $mAppOrder->road_route          = $this->road_route;
        $mAppOrder->phu_xe_1            = $this->phu_xe_1;
        $mAppOrder->phu_xe_2            = $this->phu_xe_2;
        
        if(in_array($mAppOrder->status, $aStatusCanChange)){
            $mAppOrder->status      = GasAppOrder::STATUS_PROCESSING;
            $mAppOrder->update_time = date('Y-m-d H:i:s');
            $aUpdate[] = 'status';
            $aUpdate[] = 'update_time';
        }
        $mAppOrder->setJsonNameUser();
        $mAppOrder->setJsonDataField('JSON_FIELD_INFO', 'json_info');
        
        $mAppOrder->update($aUpdate);// Feb 03, 2017 vì có json_info nên update 1 sql sẽ bị lỗi
        if($notifyDriver){
            $mAppOrder->notifyDriver();// Feb 03, 2017 vì có json_info nên update 1 sql sẽ bị lỗi
        }
        if($this->type == GasOrders::TYPE_AGENT ){
            $mOrderCar->allowUpdateAgent = false;// Chỉ cho agent chạy 1 lần update driver id và car cho record đầu tiên
        }
//        Logger::WriteLog("Run with $this->id ----- ");// for test how many time update db
//        $sqlAppOrder .= "UPDATE $tableNameAppOrder SET `driver_id`=\"$this->driver_id\", "
//            . "`car_id`=\"$this->car_number\", "
//            . "`json_info`=\"$mAppOrder->json_info\", "
//            . "`order_detail_note_2`=\"$this->note_2\" WHERE `obj_detail_id`=$this->id ;";
    }
    
    /**
     * @Author: ANH DUNG Jan 23, 2017
     * @Todo: update tài xế qua lại AppOrder để tài xế có thể nhìn thấy đơn hàng trên app của mình
     */
    public function updateDriverToAppOrder($sqlAppOrder) {
        if(trim($sqlAppOrder) != ""){
            Yii::app()->db->createCommand($sqlAppOrder)->execute();
        }
    }

    /**
     * @Author: ANH DUNG May 08, 2014
     * @Todo: update order, không thể xóa hết đi tạo lại dc, vì còn liên quan đến cập nhật số xe và ghi chú
     * @Param: $model model order 
     */    
    public static function updateOrderDetail($model){
        $sql = ''; $aRowInsert = $aCustomerIdNew = []; $sCustomerSame = '';
        $cUid           = MyFormat::getCurrentUid();
        $aCustomerIdOld = $model->getArrayCustomerOld();
        $tableName      = self::model()->tableName();
        // Jan 26, 2017
        $mAppOrder              = new GasAppOrder();
        $mAppOrder->obj_id      = $model->id;
        $arrAppOrder            = $mAppOrder->getByOrderId();
        // Jan 26, 2017
        $model->aIdDetailNotUpdateApp = $model->aOldIdDetail;
        /** Jan 27, 2017 biến aIdDetailNotUpdateApp xử lý lưu những id
         *  không find ra để map vào những AppOrder add new khi có item add new
         * @note: không cần lấy những id detail delete để đưa vào aIdDetailNotUpdateApp
         * vì id đó đã bị xóa trước khi xử lý find để map vào add new
         */
        if( is_array($model->aModelOrderDetail) && count($model->aModelOrderDetail)){
            foreach($model->aModelOrderDetail as $item){
                    $mCustomer = $item->customer;
                    if(empty($item->id) && in_array($item->customer_id, $aCustomerIdOld)){
                        $sCustomerSame .= "<br>".$mCustomer->getFullName();
//                        continue;// cho phép trùng, chỉ alert lên thông báo
                    }

//                    if(empty($item->id) && !in_array($item->customer_id, $aCustomerIdOld)){
                    if(empty($item->id)){// Oct 19, 2016 cho phép trùng KH
                        if(!isset($aCustomerIdNew[$item->customer_id])){// xử lý không cho tạo trùng 2 KH trong 1 lần tạo
                            $aCustomerIdNew[$item->customer_id] = 1;
                        }else{
                            $aCustomerIdNew[$item->customer_id] += 1;
                        }
                        self::buildNewRecord($aRowInsert, $model, $item);
                        continue;
                    }

                    // Jul 09, 2015 nếu không tạo thì không dc xóa
                    $mDetail = self::model()->findByPk($item->id);
                    if(!empty($mDetail->car_id) || is_null($mDetail) || $mDetail->created_by != $cUid){
                        continue;
                    }// Dec2517 những đơn đã điều xe thì không cho cập nhật sang nữa
                    
                    if(!isset($arrAppOrder[$mDetail->id]) && $model->type != GasOrders::TYPE_AGENT){// Aug2718 FIX nếu có id, mà chưa đc tạo bên GasAppOrder thì tạo lại, vì 1 lý do nào đó mà tạo bị thiếu bên GasAppOrder
                        $infoError = date('d/m/Y H:i:s')." -- Bug DH xe tai, ko tao dc detail, phai tao lai 1 lan nua id = $model->id  Detail id = $mDetail->id";
//                        SendEmail::bugToDev($infoError); Logger::WriteLog($infoError);
                        $mDetail->makeNewAppOrder($model);
                        unset($model->aIdDetailNotUpdateApp[$item->id]);// ? Mar3019 tại sao phải unset ở đây
                        /* Mar3019 => ở đây chạy unset là muốn để id này dc find trong hàm doAddMultiAppOrderGetDetail và đc insert new vào
                         * Nhưng trong trường hợp đơn hàng chưa được thêm mới thành công row nào thì ko bao giờ chạy đc chỗ này.
                         * => có thể phải xóa đơn đi tạo lại đơn mới
                         */
                    }else{
                        $model->aIdDetailNotUpdateApp[$item->id] = $item->id;
                    }
                    
                    $user_id_executive  = $model->user_id_executive;
                    $date_delivery      = $model->date_delivery;
                    $customer_id        = $item->customer_id;
                    $type_customer = $sale_id = '';
                    if($mCustomer){ // loại KH 1: Bình Bò, 2: Mối, dùng cho thống kê doanh thu
                        //'is_maintain',// vì cột is_maintain không dùng trong loại KH của thẻ kho nên ta sẽ dùng cho 1: KH bình bò, 2. KH mối
                        $type_customer  = $mCustomer->is_maintain;
                        $sale_id        = $mCustomer->sale_id;
                        if($mCustomer->channel_id == Users::CHAN_HANG){
                            throw new Exception("Khách hàng: {$mCustomer->first_name} đã bị chặn hàng. Không thể tạo đơn hàng");
                        }
                    }
                    $agent_id       = $model->agent_id;

                    $lpg_ton        = $item->lpg_ton;
                    $quantity_50    = $item->quantity_50;
                    $quantity_45    = $item->quantity_45;
                    $quantity_12    = $item->quantity_12;
                    $quantity_6     = $item->quantity_6;
                    $other_material_id          = $item->other_material_id;
                    $quantity_other_material    = $item->quantity_other_material;
                    $work_content               = MyFormat::removeBadCharacters($item->work_content);
                    $note                       = MyFormat::removeBadCharacters($item->note);                
                    $item->updateAppOrder($arrAppOrder);// Jan 27, 2017
                $sql .= "UPDATE $tableName SET "
                        . " `user_id_executive`=\"$user_id_executive\",  "
                        . " `date_delivery`=\"$date_delivery\",  "
                        . " `customer_id`=\"$customer_id\",  "
                        . " `type_customer`=\"$type_customer\",  "
                        . " `sale_id`=\"$sale_id\",  "
                        . " `agent_id`=\"$agent_id\",  "
                        . " `lpg_ton`=\"$lpg_ton\",  "
                        . " `quantity_50`=\"$quantity_50\" ,  "
                        . " `quantity_45`=\"$quantity_45\" ,  "
                        . " `quantity_12`=\"$quantity_12\" ,  "
                        . " `quantity_6`=\"$quantity_6\",  "
                        . " `other_material_id`=\"$other_material_id\",  "
                        . " `quantity_other_material`=\"$quantity_other_material\",  "
                        . " `work_content`=\"$work_content\",  "
                        . " `note`=\"$note\" "
                        . "WHERE `id`=$item->id ;";
            }
            foreach($aCustomerIdNew as $k => $v){
                if($v > 1 && $model->type == GasOrders::TYPE_DIEU_PHOI){// Aug3117 không cho tạo 2 KH trong 1 request
                    throw new Exception('Bạn lên 2 đơn hàng của 1 khách hàng trong 1 lần tạo, vui lòng chia ra nhiều lần lưu cho mỗi đơn hàng của khách hàng');
                }
            }
            
            if(trim($sql)!='')
                Yii::app()->db->createCommand($sql)->execute();
            
        } // end if( is_array($model->aModelOrderDetail)){
        self::deleteDetail($model); // xóa dòng sau đó mới insert thêm mới
        $model->updateOrderDetailRunInsert($aRowInsert, $tableName);
        if(!empty($sCustomerSame)){
            $model->addError('date_delivery', "Bạn lên đơn hàng bị trùng, vui lòng bấm View để kiểm tra đơn hàng. KH trùng: {$sCustomerSame}");
        }
//        Logger::WriteLog("1111 Debug end xxxx list id not update: ".  json_encode(array_keys($model->aAppOrderInsert)));
        $model->handleFlowUpdateToAppOrder($mAppOrder);// Jan 31, 2017
    }
    
    // nếu cập nhật đơn hàng và có phát sinh record mới
    public static function buildNewRecord(&$aRowInsert, &$model, $item){
        $cUid           = MyFormat::getCurrentUid();
        $created_date   = date('Y-m-d H:i:s');
        if( $item->lpg_ton>0 || $item->quantity_50>0 || 
                        $item->quantity_45>0 || $item->quantity_12>0 ||
                        $item->quantity_6>0 || $item->quantity_other_material>0 ||
                        $item->customer_id>0 || trim($item->work_content)!='' ||
                        $item->other_material_id>0
                    ) {
        
            $type_customer = $sale_id = '';
            $orders_id          = $model->id;
            $user_id_create     = $model->user_id_create;
            $user_id_executive  = $model->user_id_executive;
            $type               = $model->type;
            $date_delivery      = $model->date_delivery;
            $customer_id        = $item->customer_id;
            if($item->customer){ // loại KH 1: Bình Bò, 2: Mối, dùng cho thống kê doanh thu
                //'is_maintain',// vì cột is_maintain không dùng trong loại KH của thẻ kho nên ta sẽ dùng cho 1: KH bình bò, 2. KH mối
                $type_customer  = $item->customer->is_maintain;
                $sale_id        = $item->customer->sale_id;                            
            }
            $agent_id       = $model->agent_id;
            $lpg_ton        = $item->lpg_ton;
            $quantity_50    = $item->quantity_50;
            $quantity_45    = $item->quantity_45;
            $quantity_12    = $item->quantity_12;
            $quantity_6     = $item->quantity_6;
            $other_material_id      = $item->other_material_id;
            $quantity_other_material= $item->quantity_other_material;
            $work_content   = MyFormat::removeBadCharacters($item->work_content);
            $note           = MyFormat::removeBadCharacters($item->note); 
            $aRowInsert[]="('$orders_id',
                '$user_id_create', 
                '$user_id_executive', 
                '$agent_id', 
                '$type', 
                '$date_delivery', 
                '$customer_id',
                '$type_customer',
                '$sale_id',
                '$lpg_ton',
                '$quantity_50',
                '$quantity_45',
                '$quantity_12',
                '$quantity_6',
                '$other_material_id',
                '$quantity_other_material',
                '$work_content',
                '$cUid',
                '$note',
                '$created_date'
                )";
            $item->makeNewAppOrder($model);// Jan 27, 2017
        }
    }
    
    /**
     * @Author: ANH DUNG May 09, 2014
     * @Todo: xóa mảng những id dc remove dưới giao diện
     * @Param: $model model Order
     */
    public static function deleteDetail(&$model){
//        if(!is_array($model->aOldIdDetail) || count($model->aOldIdDetail)<1) return;// Close at Jan 27, 2017 vì đã chỉnh aOldIdDetail là array
        if(count($model->aOldIdDetail)<1) return;
        $criteria = new CDbCriteria();
//        $criteria->addNotInCondition("id", $model->aOldIdDetail);
        $sParamsIn = implode(',', $model->aOldIdDetail);
        $criteria->addCondition("id NOT IN ($sParamsIn)");
        $criteria->addCondition('orders_id=' . $model->id);
//        self::model()->deleteAll($criteria);
        // Jul 09, 2015 chỉ cho xóa những dòng mà user đó tạo ra
        $models = self::model()->findAll($criteria);
        $cUid   = MyFormat::getCurrentUid();
        foreach($models as $item){
            // Sep3017 bổ sung không xóa order khi đã lê tài xế empty($item->driver_id)
            if( empty($item->driver_id) && $item->created_by == $cUid && $item->from_app != GasOrders::FROM_APP){// cho phép xóa những đơn hàng của ĐP lên, KH đặt APP là ko cho xóa
                $model->aOrderDetailIdDelete[] = $item->id;// Jul2417 Bị bug xóa ĐH của KH đặt app khi không check điều kiện from_app GasOrders::FROM_APP
                $item->delete();
            }
        }
    }
    
    /**
     * @Author: ANH DUNG Jan 27, 2017
     * @Todo: build array model AppOrder để insert 1 lần
     */
    public function makeNewAppOrder(&$mOrder) {
        if($this->type == GasOrders::TYPE_AGENT ){// vì sẽ luôn là cập nhật 1 order nên không cần build từng detail với đơn hàng của agent
            return ;
        }
        $mAppOrder                  = new GasAppOrder();
        $mAppOrder->user_id_executive = $mOrder->user_id_executive;
        $mAppOrder->status          = GasAppOrder::STATUS_CONFIRM;
        $mAppOrder->uid_login       = MyFormat::getCurrentUid();
        $mAppOrder->uid_confirm     = $mAppOrder->uid_login;
        $mAppOrder->obj_id          = $mOrder->id;
        $mAppOrder->customer_id     = $this->customer_id;
        $mAppOrder->type            = GasAppOrder::DELIVERY_BY_CAR;
        $mAppOrder->order_detail_note   = $this->work_content.'. '.$this->note;
        $mAppOrder->date_delivery       = $mOrder->date_delivery;
        $mAppOrder->type_of_order_car   = $mOrder->type;
        $mAppOrder->customer_contact    = $mOrder->customer_contact;
        
        $mAppOrder->driver_id           = (!empty($this->driver_id) ? $this->driver_id : 0);
        $mAppOrder->car_id              = (!empty($this->car_number) ? $this->car_number : 0);
        $mAppOrder->phu_xe_1            = (!empty($this->phu_xe_1) ? $this->phu_xe_1 : 0);
//        $this->getMaterialsOfAgent($mAppOrder);// jan 31, 2017 tạm close Chưa xử lý theo kiểu này
        if($mOrder->type == GasOrders::TYPE_DIEU_PHOI){// phân biệt đơn hàng điều phối và đơn hàng đại lý đặt
            $mAppOrder->checkPriceNexMonth   = true;//Oct2717 khi tạo mới sẽ kiểm tra đơn hàng khác tháng
            $mAppOrder->setAgent();
            $mAppOrder->agent_id = $mAppOrder->carAgentId();// Add Oct0617, do KH có 2 loại giao ngay và xe tải
            $mAppOrder->setProvinceIdAgent();
            $mAppOrder->mapInfoCustomer();
            $this->mapQtyToAppOrder($mAppOrder);
            $mAppOrder->setCustomerQtyRequest();
            $mAppOrder->setGasDetailToJson();// gắn gas và vỏ dc SETUP cho đl dựa theo tồn kho hiện tại của ĐL
        }
        $this->quantity_other_material = ($this->quantity_other_material > 0 ? $this->quantity_other_material : 0) * 1;
        $key_compare = $this->customer_id.$this->quantity_50.$this->quantity_45.$this->quantity_12.$this->quantity_6.$this->quantity_other_material;
        $mOrder->aAppOrderInsert[$key_compare] = $mAppOrder;
        return $mAppOrder;
    }
    
    /**
     * @Author: ANH DUNG Jan 31, 2017
     * @Todo: lấy vật tư + sl của đơn hàng đại lý để map vào cho AppOrder
     * @param: $mAppOrderDetail is model of GasAppOrderDetail
     */
    public function getMaterialsOfAgent(&$mAppOrderDetail) {
        $qty = $this->quantity_50;
        if($this->quantity_45 > 0){
            $qty = $this->quantity_45;
        }elseif($this->quantity_12 > 0){
            $qty = $this->quantity_12;
        }elseif($this->quantity_6 > 0){
            $qty = $this->quantity_6;
        }elseif($this->quantity_other_material > 0){
            $qty = $this->quantity_other_material;
        }
        $mAppOrderDetail->materials_id  = $this->other_material_id;
        $mAppOrderDetail->qty           = $qty*1;
    }
    
    /**
     * @Author: ANH DUNG May 14, 2016
     * @Todo: get info last order of this customer
     */
    public static function getLastOrderOfCustomer($customer_id) {
        $criteria = new CDbCriteria();
        $criteria->compare("t.customer_id", $customer_id);
        $criteria->order = "t.id DESC";
        $criteria->limit = 1;
        return self::model()->find($criteria);
    }
    
    /**
     * @Author: ANH DUNG May 14, 2016
     */
    public function getDriver() {
        $model = $this->rDriver;
        if($model){
            return $model->getFullName();
        }
        return "";
    }
    public function getCreatedBy() {
        $model = $this->rCreatedBy;
        if($model){
            return $model->getFullName();
        }
        return "";
    }
    
    /**
     * @Author: ANH DUNG Dec 15, 2016
     */
    public function getCreatedDate() {
        return MyFormat::dateConverYmdToDmy($this->created_date, 'd/m/Y H:i');
    }
    
    /**
     * @Author: ANH DUNG Oct 07, 2016
     * @Todo: get note for storecard auto gen 8h sáng hôm sau
     * @Param: 
     */
    public function getNoteForStoreCard() {
        $res = "";
        if(!empty($this->quantity_50)){
            $res .= "$this->quantity_50 bình 50Kg";
        }
        if(!empty($this->quantity_45)){
            $res .= ", $this->quantity_45 bình 45Kg";
        }
        if(!empty($this->quantity_12)){
            $res .= ", $this->quantity_12 bình 12Kg";
        }
        if(!empty($this->quantity_6)){
            $res .= ", $this->quantity_6 bình 6Kg";
        }
        return trim($res, ',');
    }
    
    /**
     * @Author: ANH DUNG Oct 08, 2016
     */
    public function getReasonWrong() {
        $aReasonWrong = $this->getArrayReasonWrong();
        return isset($aReasonWrong[$this->reason_wrong]) ? $aReasonWrong[$this->reason_wrong] : "";
    }
    /**
     * @Author: ANH DUNG Jan 26, 2017
     * @Todo: Những đơn hàng lên từ app thì không cho xóa row và xóa KH khi, điều phối chỉnh sửa đơn hàng trên web
     */
    public function canDeleteRow() {
        $aAllow = [GasOrders::FROM_WEB, GasOrders::FROM_PMBH];
        return in_array($this->from_app, $aAllow);
    }

    /**
     * @Author: ANH DUNG Jan 26, 2017
     * @Todo: handle update back to AppOrder
     * 1. find all AppOrder voi Order Id này. Chính là list order detail hiện tại
     * 2. nếu bật cờ update thì xử lý
     */
    public function updateAppOrder($arrAppOrder) {
        if($this->type == GasOrders::TYPE_AGENT || !$this->need_update || !isset($arrAppOrder[$this->id])){            
            return ;
        }
        
        $mAppOrder = $arrAppOrder[$this->id];
        // 1. decode all json
        $mAppOrder->mapJsonFieldOneDecode('JSON_FIELD', 'json', 'baseArrayJsonDecode');
        $mAppOrder->mapJsonFieldOneDecode('JSON_FIELD_INFO', 'json_info', 'baseArrayJsonDecodeV1');
        $mAppOrder->order_detail_note = $this->work_content.'. '.$this->note;
        $this->checkChangeQty($mAppOrder);
        if($mAppOrder->change_qty){
            $mAppOrder->checkPriceNexMonth   = true;//Oct2717 khi tạo mới sẽ kiểm tra đơn hàng khác tháng
            $this->mapQtyToAppOrder($mAppOrder);
            $mAppOrder->setCustomerQtyRequest();
            $mAppOrder->setGasDetailToJson();// gắn gas và vỏ dc SETUP cho đl dựa theo tồn kho hiện tại của ĐL
        }
        $mAppOrder->update();
    }
    
    /** @Author: ANH DUNG Jan 27, 2017
     * @Todo: map qty from Order Xe tải qua AppOrder
     */
    public function mapQtyToAppOrder(&$mAppOrder) {
        $ok = true;
        $mAppOrder->b50 = $this->quantity_50;
        $mAppOrder->b45 = $this->quantity_45;
        $mAppOrder->b12 = $this->quantity_12;
        $mAppOrder->b6  = $this->quantity_6;
        if(empty($this->quantity_50) && empty($this->quantity_45) && empty($this->quantity_12) && empty($this->quantity_6)){
            $ok = false;
        }
        if($this->type == GasOrders::TYPE_DIEU_PHOI && !$ok){// Aug2517 kiểm tra lỗi không nhập đúng ô SL bình của KH bò mối
            throw new Exception('Có lỗi khi lên đơn hàng xe tải cho KH bò mối, vui lòng nhập số lượng bình vào các ô: 50, 45, 12, 6 Kg');
        }
    }
    /**
     * @Author: ANH DUNG Jan 27, 2017
     * @Todo: check có change qty không, nếu change thì tính toán lại gas và vỏ
     */
    public function checkChangeQty(&$mAppOrder) {
        $mAppOrder->getCustomerQtyRequest();
        if(!empty($mAppOrder->b50) && $mAppOrder->b50 != $this->quantity_50){
            $mAppOrder->change_qty = true;
        }elseif(!empty($mAppOrder->b45) && $mAppOrder->b45 != $this->quantity_45){
            $mAppOrder->change_qty = true;
        }elseif(!empty($mAppOrder->b12) && $mAppOrder->b12 != $this->quantity_12){
            $mAppOrder->change_qty = true;
        }elseif(!empty($mAppOrder->b6) && $mAppOrder->b6 != $this->quantity_6){
            $mAppOrder->change_qty = true;
        }
    }
    
    /**
     * @Author: ANH DUNG Feb 03, 2017
     * @Todo: handle build sql update back name driver, car to AppOrder, only update json_info
     */
    public function buildSqlAppOrderDriverName($arrAppOrder, &$sqlAppOrderDriver, $tableNameAppOrder) {
        if($this->type == GasOrders::TYPE_AGENT || empty($this->driver_id) || !isset($arrAppOrder[$this->id])){
            return ;
        }
        $mAppOrder = $arrAppOrder[$this->id];
        // 1. decode all json
//        $mAppOrder->mapJsonFieldOneDecode('JSON_FIELD', 'json', 'baseArrayJsonDecode');
        $mAppOrder->mapJsonFieldOneDecode('JSON_FIELD_INFO', 'json_info', 'baseArrayJsonDecodeV1');
        $mAppOrder->order_detail_note_2 = $this->note_2;
        $mAppOrder->driver_id           = $this->driver_id;
        $mAppOrder->car_id              = $this->car_number;
        $mAppOrder->setJsonNameUser();
        $mAppOrder->setJsonDataField('JSON_FIELD_INFO', 'json_info');
        
        $aUpdate = ['json_info', 'order_detail_note_2', 'driver_id', 'car_id'];
        $sqlAppOrder .= "UPDATE $tableNameAppOrder SET `driver_id`=\"$this->driver_id\", "
            . "`car_id`=\"$this->car_number\", "
            . "`order_detail_note`=\"$this->note\", "
            . "`order_detail_note_2`=\"$this->note_2\" WHERE `obj_detail_id`=$this->id ;";
    }
    
    /**
     * @Author: ANH DUNG Jan 10, 2017
     * @Todo: lấy giá KH bò mối gắn vào cho điều phối biết
     */
    public function getPriceCustomer(&$aRes) {
        $nextDay    = MyFormat::modifyDays(date('Y-m-d'), 1, '+');
        $tempDay    = explode('-', $nextDay);
        $sPrice     = '';
//        $this->work_content     = $this->work_content.'. '.$sPrice;// Oct1017 bỏ gắn giá vào ghi chú, vì app đã tính đc rồi
        $this->work_content     = $this->work_content;
        $aRes['work_content']   = $this->work_content;
    }
}