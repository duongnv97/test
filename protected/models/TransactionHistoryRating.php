<?php
/**
 * Description of TransactionHistoryRating
 *
 * @author Pham THanh NGhia TASK: 002
 * Làm trang list comment + rating của table gas_transaction_history
 admin/sell/listRating
 */
class TransactionHistoryRating extends TransactionHistory {
    
    public $autocomplete_name_3, $autocomplete_name_2, $uid_login;
    public function search()
    {
        $criteria=new CDbCriteria;
        if(isset($_GET['TransactionHistoryRating'])){
            $item = $_GET['TransactionHistoryRating'];
//            
            if(!empty($item['agent_id'])){
                $criteria->addCondition('t.agent_id='.$item['agent_id']);
            }
            if(!empty($item['employee_maintain_id'])){
                $criteria->addCondition('t.employee_maintain_id='.$item['employee_maintain_id']);
            }
            if(!empty($item['customer_id'])){
                $criteria->addCondition('t.customer_id='.$item['customer_id']);
            }
            $criteria->compare('t.code_no_sell', $item['code_no']);
            if(!empty($this->rating)){
                $criteria->addCondition('t.rating <= '.$this->rating);
            }
            if(!empty($item['date_from'])){
                $date_from = MyFormat::dateDmyToYmdForAllIndexSearch($item['date_from']);
            }
            if(!empty($item['date_to'])){
                $date_to = MyFormat::dateDmyToYmdForAllIndexSearch($item['date_to']);
            }
            if(!empty($date_from)){
                $criteria->addCondition("t.created_date_only>='$date_from'");
            }
            if(!empty($date_to)){
                $criteria->addCondition("t.created_date_only<='$date_to'");
            }
            
        }
        
        $criteria->addCondition('t.transaction_id>0 AND t.source='.Sell::SOURCE_APP);
        $criteria->addCondition('t.rating > 0');
            
        $criteria->order = 't.id DESC';// đơn hàng nào cũ sẽ lên đầu
        if(isset($_GET['order_status'])){
            $criteria->order = 't.id DESC';
        }

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
            'pagination'=>array(
                'pageSize'=> 10,
            ),
        ));
    }
    
    public $mSell;
    
    public function getSellBySellId()
    {
        $this->mSell = Sell::model()->findByPk($this->sell_id);
        //return Sell::model()->findByPk($this->sell_id);
    }
    
    public function getArrayOrderType(){
        if(!empty($this->mSell)){
            return $this->mSell->getArrayOrderType();
        }
        return '';
    }
    public function getArrayStatus(){
        if(!empty($this->mSell)){
            return $this->mSell->getArrayStatus();
        }
        return '';
    }
    public function getDateSellGrid(){
        if(!empty($this->mSell)){
            return $this->mSell->getDateSellGrid();
        }
        return '';
    }
    public function getPtttCode(){
        if(!empty($this->mSell)){
            return $this->mSell->getPtttCode();
        }
        return '';
    }
    public function getDisplayErrors(){
        if(!empty($this->mSell)){
            return $this->mSell->getDisplayErrors();
        }
        return '';
    }
    public function getCustomerNewOld(){
        if(!empty($this->mSell)){
            return $this->mSell->getCustomerNewOld();
        }
        return '';
    }
    public function getHighPriceText(){
        if(!empty($this->mSell)){
            return $this->mSell->getHighPriceText();
        }
        return '';
    }
    public function getCreatedDate($fomat = 'd/m/Y H:i'){
        if(!empty($this->mSell)){
            return $this->mSell->getCreatedDate();
        }
        return '';
    }
    public function getCodeNo(){
        if(!empty($this->code_no)){
            return $this->code_no;
        }
        return '';
    }
    
    public function  getGrandTotal($format = false){
        if(!empty($this->mSell)){
            return $this->mSell->getGrandTotal(true);
        }
        return '';
    }
    public function  getAgent($field_name = 'first_name'){
        if(!empty($this->mSell)){
            return $this->mSell->getAgent();
        }
        return '';
    }
    
    public function  getCustomer(){
        if(!empty($this->mSell)){
            return $this->mSell->getCustomer();
        }
        return '';
    }
     public function  getTypeCustomer(){
        if(!empty($this->mSell)){
            return $this->mSell->getTypeCustomer();
        }
        return '';
    }
    public function  getArrayPlatformText(){
        if(!empty($this->mSell)){
            return $this->mSell->getArrayPlatformText();
        }
        return '';
    }
    
    public function  getInfoAccounting(){
        if(!empty($this->mSell)){
            return $this->mSell->getInfoAccounting();
            
        }
        return '';
    }
    public function  getInfoGas(){
        if(!empty($this->mSell)){
            return $this->mSell->getInfoGas();
            
        }
        return '';
    }
    public function  getUrlViewCall(){
        if(!empty($this->mSell)){
            return $this->mSell->getUrlViewCall();
            
        }
        return '';
    }
    public function  getStatus(){
        if(!empty($this->mSell)){
            return $this->mSell->getStatus();
            
        }
        return '';
    }
    public function  getTimeOverWebView(){
        if(!empty($this->mSell)){
            return $this->mSell->getTimeOverWebView();
            
        }
        return '';
    }
    public function  getEmployeeMaintain(){
        if(!empty($this->mSell)){
            return $this->mSell->getEmployeeMaintain();
            
        }
        return '';
    }
    
    
}
