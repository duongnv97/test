<?php

/**
 * This is the model class for table "{{_sta_view}}".
 *
 * The followings are the available columns in table '{{_sta_view}}':
 * @property string $id
 * @property integer $type
 * @property string $obj_id
 * @property string $user_id
 * @property string $created_date
 */
class StaView extends BaseSpj
{
    const TYPE_1_APP_NEWS           = 1;
    const TYPE_2_GAS_SERVICE_ANOUCE = 2;// các thông báo của app gas service
    
    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return StaView the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{_sta_view}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        return [
            ['id, type, obj_id, user_id, created_date', 'safe'],
        ];
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        return [
        ];
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'type' => 'Type',
            'obj_id' => 'Obj',
            'user_id' => 'User',
            'created_date' => 'Created Date',
        ];
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        $criteria=new CDbCriteria;
	$criteria->compare('t.type',$this->type);
	$criteria->compare('t.obj_id',$this->obj_id);
	$criteria->compare('t.user_id',$this->user_id);
        $criteria->order = 't.id DESC';
        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
            'pagination'=>array(
                'pageSize'=> Yii::app()->user->getState('pageSize',Yii::app()->params['defaultPageSize']),
            ),
        ));
    }
    
    /** @Author: ANH DUNG May 04, 2018 
     *  @Todo: add new record
     **/
    public function addRow() {
        $this->save();// Dec1518 có thể sẽ giữ luôn save, ko cần check findRow nữa
        return ;
        if(date('d') == 1){
            $this->save();
        }else{
            $modelViewOld = $this->findRow();// Sep3018 save luon , ko check trung nua, se delete = cron cuoi ngay
            if(empty($modelViewOld)){
                $this->save();
            }
        }
    }
    
    /** @Author: ANH DUNG May 04, 2018
     *  @Todo: find xem có tồn tại row chưa, nếu có rồi thì không add nữa
     **/
    public function findRow() {
        $criteria = new CDbCriteria();
        $criteria->addCondition('t.type=' . $this->type);
        if(!empty($this->obj_id)){
            $criteria->addCondition('t.obj_id=' . $this->obj_id);
        }
        if(!empty($this->user_id)){
            $criteria->addCondition('t.user_id=' . $this->user_id);
        }
        return self::model()->find($criteria);
    }
    
    /** @Author: ANH DUNG May 04, 2018
     *  @Todo: count row by obj id
     **/
    public function countByObjId() {
        $criteria = new CDbCriteria();
        $criteria->addCondition('t.type=' . $this->type);
        if(!empty($this->obj_id)){
            $criteria->addCondition('t.obj_id=' . $this->obj_id);
        }
        if(!empty($this->user_id)){
            $criteria->addCondition('t.user_id=' . $this->user_id);
        }
        $criteria->select = 'count(distinct t.user_id)';
        return StaView::model()->count($criteria);
    }
    
    /** @Author: ANH DUNG Sep 30, 2018
     *  @Todo: cron delete row duplicate
     *  @Param: $dateRun Y-m-d
     * https://www.quora.com/What-is-the-best-way-to-delete-duplicate-rows-in-MySql-except-one
     * DELETE a FROM gas_sta_view a JOIN gas_sta_view b 
     * ON a.type = b.type AND a.obj_id = b.obj_id AND a.user_id = b.user_id AND a.id > b.id
     **/
    public function deleteDuplicate($dateRun) {
        $tableName        = $this->tableName();
        $sql = "DELETE a FROM $tableName a JOIN $tableName b 
            ON a.type = b.type AND a.obj_id = b.obj_id AND a.user_id = b.user_id AND a.id > b.id ";
        // AND DATE(a.created_date) = '$dateRun' // Mar2519 remove condtion date 
        Yii::app()->db->createCommand($sql)->execute();
        $infoLog = "done StaView run deleteDuplicate $dateRun";
        Logger::WriteLog($infoLog);
        SendEmail::bugToDev($infoLog);
    }
    
}