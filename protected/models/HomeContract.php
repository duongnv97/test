<?php
/** @Author: HOANG NAM 02/01/2018 Task: NAM001
 *  @Todo: Quản lý hợp đồng thuê nhà
 *  @Param: 
 **/
class HomeContract extends BaseSpj
{
    public $autocomplete_agent, $statistic_year;
    
    public $amounts_contract;//gia thue
    public $amounts_start;//ngày bắt đầu
    public $amounts_end;//ngày kết thúc
    public $amounts_price;//giá thuê tại thời điểm
   
    public $pay_beneficiary;//người hưởng thụ
    public $pay_numberPhone;//SDT pay
    public $pay_bank;//tên ngân hàng
    public $pay_bankNumber;//số tài khoản
    
//    trạng thái hợp đồng
    public $Active_Status = 1;  // trạng thái hoạt động => đặt tên sai quy cách
    public $Expired_Status = 2; // trạng thái không hoạt động
    
    public $JSON_FIELD      = array('pay_beneficiary', 'pay_numberPhone', 'pay_bank', 'pay_bankNumber');

    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }
    
    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{_home_contract}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        return array(
            array('agent_id','unique','message'=>'Mỗi đại lý chỉ có một hợp đồng duy nhất','on'=>'create,update'),
            array('type_pay, agent_id, deposit, amount_month_pay,start_contract,end_contract,next_date_pay,status', 'required','on'=>'create,update'),
            array('amount_month_pay,deposit','numerical','integerOnly'=>true),
            array('id,type_pay_value,agent_id,deposit,prices_contract,amount_month_pay,date_expired,start_contract,end_contract,type_pay,next_date_pay,province_id,status,note,statistic_year', 'safe'),
        );
    }
    
    public function relations()
    {
        return array(
            'rAgent' => array(self::BELONGS_TO, 'Users', 'agent_id'),
            'rHomeContractDetail' => array(self::HAS_MANY, 'HomeContractDetail', 'home_contract_id'),
        );
    }
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'agent_id' => 'Đại lý',
            'start_contract' => 'Ngày bắt đầu',
            'end_contract' => 'Ngày hết hạn',
            'date_expired' => 'Ngày kết thúc hợp đồng',
            'deposit' => 'Tiền đặt cọc',            
            'amount_month_pay' => 'Thanh toán',
            'next_date_pay'=>'Ngày thanh toán tiếp theo',
            'province_id' => 'Tỉnh',
            'status' => 'Trạng thái',
            'note' => 'Ghi chú',
            'prices_contract' => 'Giá thuê',
            'type_pay' => 'Loại thanh toán',
            'pay_beneficiary'=> 'Người hưởng thụ',
            'pay_numberPhone'=> 'Số điện thoại',
            'pay_bank'=> 'Tên ngân hàng',
            'pay_bankNumber'=> 'Số tài khoản',
            'amounts_contract'=> 'Giá thuê',
            'amounts_start'=> 'Từ',
            'amounts_end'=> 'Đến',
            'type_pay_value'=>'Thông tin thanh toán',
            'created_date'=>'Ngày tạo',
            'statistic_year' => 'Chọn Năm',
        );
    }
    /** @Author: HOANG NAM 02/01/2018
     *  @Todo: lấy danh sách trạng thái
     *  @Param: 
     **/
    public function getStatusContract(){
        return array(
            $this->Active_Status =>'Hoạt động',
            $this->Expired_Status =>'Hết hạn',
        );
    }
    /** @Author: HOANG NAM 08/01/2018
     *  @Todo: 
     *  @Param: 
     **/
    public function getArrayAmountMonthPay(){
        $start = 1;
        $end = 60;
        $key = range($start,$end,1);
        $valueView = range($start,$end,1);
        array_walk($valueView, function(&$value, $key) { $value .= ' tháng'; } );
        return array_combine($key,$valueView);
    }
    public function getStatus(){
            $aStatus = $this->getStatusContract();
            return $aStatus["$this->status"];
    }
    public function getArrayTypePayValue(){
        return isset($this->type_pay_value) ? json_decode($this->type_pay_value,true) : [];
    }
    public function getBeneficiary(){
        return $this->pay_beneficiary;
    }
    public function getBankNumber(){
        return $this->pay_bankNumber;
    }
    public function getNumberPhone(){
        return $this->pay_numberPhone;
    }
    public function getBank(){
        return $this->pay_bank;
    }
    public function getTypePay(){
        $result         = '';
        $aTypePay       = $this->getArrayTypePay();
        $result         .=isset($aTypePay[$this->type_pay]) ? $aTypePay[$this->type_pay] : '';
        $result         .='<br>';
        $result         .= implode('<br>', $this->getArrayTypePayValue());
        return $result;
    }
    /** @Author: HOANG NAM 02/01/2018
     *  @Todo: lấy danh sách trạng thái
     *  @Param: 
     **/
    public function getArrayTypePay(){
        return array(
            GasSettle::TYPE_REQUEST_MONEY_MULTI =>'Đề nghị thanh toán',
            GasSettle::TYPE_BANK_TRANSFER_MULTI =>'Đề nghị chuyển khoản',
        );
    }
    
    public function getAgent(){
        $aAgent = $this->rAgent;
        return isset($aAgent) ? $aAgent->first_name : '';
    }
    
    public function getDeposit($format = true){
        if($format == true){
            return ActiveRecord::formatCurrency($this->deposit);
        }
        return $this->deposit;
    }
    
    public function getProvince(){
        $mAppCache = new AppCache();
        $listdataProvince   = $mAppCache->getListdata('GasProvince', AppCache::LISTDATA_PROVINCE);
        return isset($listdataProvince[$this->province_id]) ? $listdataProvince[$this->province_id] : '';
    }
    public function getArrayAmountContract(){
        return isset($this->prices_contract) ? json_decode($this->prices_contract,true) : [];
    }
    public function getAmountContract($format = true){ // false for excel
        $result = [];
        if($format){
            foreach ($this->getArrayAmountContract() as $key => $value) {
                $result[] = $value['start'].' - '. $value['end'] .' - <b>'. ActiveRecord::formatCurrency($value['amount']) . '</b>';
            }
            return implode('<br>', $result);
        }
        foreach ($this->getArrayAmountContract() as $key => $value) {
            $result[] = $value['start'].' đến '. $value['end'] .' : '. ActiveRecord::formatCurrency($value['amount']) ;
        }
        return implode(ExportList::$replaceNewLine, $result);
        
    }
    
    public function getAmountPay(){
        return $this->amount_month_pay .' tháng';
    }
    
    public function getNextDatePay(){
        return MyFormat::dateConverYmdToDmy($this->next_date_pay);
    }
    
    public function getExpiredDatePay(){
        return MyFormat::dateConverYmdToDmy($this->date_expired);
    }
    
    public function getStartDateContract(){
        return MyFormat::dateConverYmdToDmy($this->start_contract);
    }
    public function getsEndDateContract(){
        return MyFormat::dateConverYmdToDmy($this->end_contract);
    }
    
    public function getNote(){
        return $this->note;
    }
    
    public function search()
    {
        $criteria=new CDbCriteria;
        if(!empty($this->status)){
            $criteria->addCondition("t.status = ".$this->status);
        }
        if(!empty($this->agent_id)){
            $criteria->addCondition("t.agent_id = ".$this->agent_id);
        }
        if(!empty($this->type_pay)){
            $criteria->addCondition("t.type_pay = ".$this->type_pay);
        }
        if(!empty($this->start_contract['start']) && !empty($this->start_contract['end'])){
            $startOfStartContract = MyFormat::dateConverDmyToYmd($this->start_contract['start'],'-');
            $endOfStartContract = MyFormat::dateConverDmyToYmd($this->start_contract['end'],'-');
            $criteria->addCondition("t.start_contract <= '$endOfStartContract' AND t.start_contract >= '$startOfStartContract'" );
        }
        if(!empty($this->end_contract['start']) && !empty($this->end_contract['end'])){
            $startOfEndtContract = MyFormat::dateConverDmyToYmd($this->end_contract['start'],'-');
            $endOfEndContract = MyFormat::dateConverDmyToYmd($this->end_contract['end'],'-');
            $criteria->addCondition("t.end_contract <= '$endOfEndContract' AND t.end_contract >= '$startOfEndtContract'" );
        }
        
        if(is_array($this->province_id) && count($this->province_id)){
            $sParamsIn = implode(',', $this->province_id);
            $criteria->addCondition("t.province_id IN ($sParamsIn)");
        }
        
//        $criteria->order = 't.id DESC';
        $sort = new CSort();
        $sort->attributes = array(
            'status'=>'status',
            'agent_id'=>'agent_id',
            'deposit'=>'deposit',
            'amount_month_pay'=>'amount_month_pay',
            'next_date_pay'=>'next_date_pay',
            'start_contract'=>'start_contract',
            'end_contract'=>'end_contract',
        ); 
        $sort->defaultOrder = 't.id desc';
        $_SESSION['data-data-homecontract'] = new CActiveDataProvider($this, array(
                'pagination' => false,
                'criteria' => $criteria,
            ));
        $_SESSION['data-model-homecontract'] = $this;
        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
            'sort' => $sort,
            'pagination'=>array(
                'pageSize'=> Yii::app()->user->getState('pageSize',Yii::app()->params['defaultPageSize']),
            ),
        ));
    }
    /** @Author: HOANG NAM 08/01/2018
     *  @Todo: do something format data before save db
     **/
    public function beforeSave(){
        $aScenario = ['create', 'update'];
        if(!in_array($this->scenario, $aScenario)){
            return parent::beforeSave();
        }
        $this->formatDataBeforeSave();
        $this->setDataJson();
        return parent::beforeSave();
    }
    
    public function formatDataBeforeSave() {
        $mAppCache              = new AppCache();
        $aAgent                 = $mAppCache->getAgent();
        $this->date_expired     = MyFormat::dateConverDmyToYmd($this->date_expired,'-');
        $this->start_contract   = MyFormat::dateConverDmyToYmd($this->start_contract,'-');
        $this->end_contract     = MyFormat::dateConverDmyToYmd($this->end_contract,'-');
        $this->next_date_pay    = MyFormat::dateConverDmyToYmd($this->next_date_pay,'-');
        $this->province_id      = isset($aAgent[$this->agent_id]) ? $aAgent[$this->agent_id]['province_id'] : '';
    }
    
    /** @Author: HOANG NAM 08/01/2018
     *  @Todo: build json before save
     *  Chuyển sang dạng json loại thanh toán
     **/
    public function setDataJson() {
        $result = [];
        switch ($this->type_pay){
            case GasSettle::TYPE_BANK_TRANSFER_MULTI:
                $result = array(
                    'pay_beneficiary'=>$this->pay_beneficiary,
                    'pay_numberPhone'=>$this->pay_numberPhone,
                    'pay_bank'=>$this->pay_bank,
                    'pay_bankNumber'=>$this->pay_bankNumber,
                );
                break;
            default://Mật định đề nghị thanh toán
                $result = array(
                    'pay_beneficiary'=>$this->pay_beneficiary,
                    'pay_numberPhone'=>$this->pay_numberPhone,
                );
                break;
        }
        $this->type_pay_value = json_encode($result);
        
    }
    
    /** @Author: HOANG NAM 08/01/2018
     *  @Todo: format data before update
     **/
    public function formatDataUpdate(){
        $this->date_expired     = MyFormat::dateConverYmdToDmy($this->date_expired, 'd-m-Y');
        $this->start_contract   = MyFormat::dateConverYmdToDmy($this->start_contract, 'd-m-Y');
        $this->end_contract     = MyFormat::dateConverYmdToDmy($this->end_contract, 'd-m-Y');
        $this->next_date_pay    = MyFormat::dateConverYmdToDmy($this->next_date_pay, 'd-m-Y');
        $aTypePay = $this->getArrayTypePayValue();
        $this->pay_beneficiary  =  isset($aTypePay['pay_beneficiary']) ? $aTypePay['pay_beneficiary'] : '';
        $this->pay_numberPhone  =  isset($aTypePay['pay_numberPhone']) ? $aTypePay['pay_numberPhone'] : '';
        $this->pay_bank         =  isset($aTypePay['pay_bank']) ? $aTypePay['pay_bank'] : '';
        $this->pay_bankNumber   =  isset($aTypePay['pay_bankNumber']) ? $aTypePay['pay_bankNumber'] : '';
    }
    
    /** @Author: ANH DUNG Jan 19, 2018
     *  @Todo: map param post 
     **/
    public function getParamsPost() {
        $this->type_pay        = isset($_POST['HomeContract']['type_pay']) ? $_POST['HomeContract']['type_pay'] : '';
        $this->pay_beneficiary = isset($_POST['HomeContract']['pay_beneficiary']) ? $_POST['HomeContract']['pay_beneficiary'] : '';
        $this->pay_numberPhone = isset($_POST['HomeContract']['pay_numberPhone']) ? $_POST['HomeContract']['pay_numberPhone'] : '';
        $this->pay_bank        = isset($_POST['HomeContract']['pay_bank']) ? $_POST['HomeContract']['pay_bank'] : '';
        $this->pay_bankNumber  = isset($_POST['HomeContract']['pay_bankNumber']) ? $_POST['HomeContract']['pay_bankNumber'] : '';
        $this->prices_contract = $this->getArrayAmounts();
        $this->deposit         = MyFormat::removeComma($this->deposit);
        if(empty($this->uid_login)){
            $this->uid_login       = MyFormat::getCurrentUid();
        }
    }
    
    /** @Author: HOANG NAM 09/01/2018
     *  @Todo: Get array amount list
     *  @Param: 
     **/
    public function getArrayAmounts(){
        $aAmounts = [];
        if(!isset($_POST['HomeContract']['amounts_start']['value']) || !isset($_POST['HomeContract']['amounts_end']['value']) || !isset($_POST['HomeContract']['amounts_price']['value']))
            return json_encode($aAmounts);
        $aStartAmount   = $_POST['HomeContract']['amounts_start']['value'];
        $aEndAmount     = $_POST['HomeContract']['amounts_end']['value'];
        $aPriceAmount   = $_POST['HomeContract']['amounts_price']['value'];
        foreach ($aStartAmount as $index => $valueDate){
            $endDate    = isset($aEndAmount[$index]) ? $aEndAmount[$index] : $valueDate;
            $amountDate = isset($aPriceAmount[$index]) ? $aPriceAmount[$index] : 0;
            $aAmounts[$index]['start']      = $valueDate;
            $aAmounts[$index]['end']        = $endDate;
            $aAmounts[$index]['amount']     = MyFormat::removeComma($amountDate);
        }
        return json_encode($aAmounts);
    }
    
    /** @Author: ANH DUNG Jan 17, 2018
     *  @Todo: check user can update record
     **/
    public function canUpdate() {
//        $cRole  = MyFormat::getCurrentRoleId();
//        $cUid   = MyFormat::getCurrentUid();
//        $aUidAllow = [GasConst::UID_NGAN_DTM, GasConst::UID_ADMIN];
//        return in_array($cUid, $aUidAllow);
        return true;// tam mở để anh Hiệp sửa, sau này close lại
        $dayAllow = date('Y-m-d');
        $dayAllow = MyFormat::modifyDays($dayAllow, 2, '-');
        return MyFormat::compareTwoDate($this->created_date, $dayAllow);
    }
    
    public function getAddress(){
        $aAgent = $this->rAgent;
        return !empty($aAgent) ? $aAgent->address : '';
    }
    
}
