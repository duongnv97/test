<?php

/**
 * This is the model class for table "{{_gas_daily_selling}}".
 *
 * The followings are the available columns in table '{{_gas_daily_selling}}':
 * @property string $id
 * @property integer $customer_id
 * @property string $date_sell
 * @property string $quantity_50
 * @property string $quantity_45
 * @property string $quantity_12
 * @property string $quantity_remain
 * @property string $date_of_payment
 * @property integer $user_id_create
 * @property string $created_date
 */
class GasDailySelling extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return GasDailySelling the static model class
 */
    public $autocomplete_name;
    public $file_excel;
	public $sell_date_from;
	public $sell_date_to;
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{_gas_daily_selling}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
                        array('customer_id, date_sell', 'required', 'on'=>'create_DailySelling,update_DailySelling'),
			array('customer_id, user_id_create', 'numerical', 'integerOnly'=>true),
array('quantity_50, quantity_45, quantity_12, quantity_remain', 'length', 'max'=>8),
array('date_sell, date_of_payment, created_date,vo_back_12,vo_back_45,vo_back_50,agent_id', 'safe'),
array('id, customer_id, date_sell, quantity_50, quantity_45, quantity_12, quantity_remain, date_of_payment, user_id_create, created_date,sell_date_from,sell_date_to,price_1_kg,price_b_12', 'safe'),
    array('file_excel', 'file', 'on'=>'import_dailyselling',
        'allowEmpty'=>false,
        'types'=> 'xls,xlsx',
        'wrongType'=>'Chỉ cho phép tải file xls,xlsx',
        //'maxSize' => ActiveRecord::getMaxFileSize(), // 5MB
        //'tooLarge' => 'The file was larger than '.(ActiveRecord::getMaxFileSize()/1024).' KB. Please upload a smaller file.',
    ),                     
                    
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
                    'customer' => array(self::BELONGS_TO, 'Users', 'customer_id'),
                    'agent' => array(self::BELONGS_TO, 'Users', 'agent_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'customer_id' => 'Khách Hàng',
			'date_sell' => 'Ngày Bán',
			'quantity_50' => 'B50',
			'quantity_45' => 'B45',
			'quantity_12' => 'B12',
			'quantity_remain' => 'Gas Dư',
			'price_1_kg' => 'Giá 1 kg',
			'price_b_12' => 'Giá bình 12 kg',
			'total' => 'Thành Tiền',
			'date_of_payment' => 'Ngày hẹn thanh toán',
			'user_id_create' => 'Người tạo',
			'created_date' => 'Ngày tạo',
			'sell_date_from' => 'Từ Ngày',
			'sell_date_to' => 'Đến Ngày',
			'vo_back_50' => 'Trả Vỏ Bình 50',
			'vo_back_45' => 'Trả Vỏ Bình 45',
			'vo_back_12' => 'Trả Vỏ Bình 12',
			'agent_id' => 'Nơi Giao Hàng',
			
			
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('t.id',$this->id,true);
		$criteria->compare('t.customer_id',$this->customer_id);
                if(!empty($this->date_sell)){
                    $this->date_sell = MyFormat::dateConverDmyToYmd($this->date_sell);
                    $criteria->compare('t.date_sell',$this->date_sell,true);
                }
                if(!empty($this->date_of_payment)){
                    $this->date_of_payment = MyFormat::dateConverDmyToYmd($this->date_of_payment);
                    $criteria->compare('t.date_of_payment',$this->date_of_payment,true);
                }
		$criteria->compare('t.quantity_50',$this->quantity_50,true);
		$criteria->compare('t.quantity_45',$this->quantity_45,true);
		$criteria->compare('t.quantity_12',$this->quantity_12,true);
		$criteria->compare('t.quantity_remain',$this->quantity_remain,true);
		
		
		$criteria->compare('t.created_date',$this->created_date,true);
		
		if(Yii::app()->user->role_id!=ROLE_ADMIN){
				//$criteria->compare('t.user_id_create',Yii::app()->user->id);
		}		

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
            'pagination'=>array(
                'pageSize'=> Yii::app()->user->getState('pageSize',Yii::app()->params['defaultPageSize']),
            ),
		));
	}
	
        public static function getDataDailySelling($date_from, $date_to){
                $date_from = MyFormat::dateConverDmyToYmd($date_from);
                $date_to = MyFormat::dateConverDmyToYmd($date_to);			

                $aSale = Users::getArrObjectUserByRole(ROLE_SALE);
                $aExport = array();
                foreach($aSale as $key=>$item){
                        $criteria=new CDbCriteria;
                        $criteria->addBetweenCondition('date_of_payment',$date_from,$date_to); 
                        $criteria->compare('t.sale_id',$key);		
                        $criteria->select = 'date_of_payment, sum(total) as total';
                        $criteria->group = 'date_of_payment';

                        $criteria->order = "t.date_of_payment asc";
                        $res = GasDailySelling::model()->findAll($criteria);
                        if(count($res)>0)
                        foreach($res as $obj){
                                $aExport[$obj->date_of_payment][$key] = $obj->total;					
                        }
                }

                return array('aExport'=>$aExport,'aSale'=>$aSale,'date_from'=>$date_from,'date_to'=>$date_to);
        }        
        
        public function searchExport(){
		/*
		$begin = new DateTime( '2010-05-25' );
		$end = new DateTime( '2010-06-10' );

		$interval = DateInterval::createFromDateString('1 day');
		$period = new DatePeriod($begin, $interval, $end);

		foreach ( $period as $key=>$dt )
		  echo ' --- '.$key.' --- '.$dt->format( "Y-m-d" );
		  die;
		  */
		if(!empty($this->sell_date_from) && !empty($this->sell_date_to) ){
                    $data = GasDailySelling::getDataDailySelling($this->sell_date_from, $this->sell_date_to);
                    // $return data is: array('aExport'=>$aExport,'aSale'=>$aSale,'date_from'=>$date_from,'date_to'=>$date_to);
                    GasDailySelling::exportExcel($data['aExport'], $data['aSale'], $data['date_from'], $data['date_to']);
		}	                
		return ;
	}        
	
	public static function exportExcel($aExport, $aSale, $sell_date_from, $sell_date_to){
		Yii::import('application.extensions.vendors.PHPExcel',true);
		$objPHPExcel = new PHPExcel();
		 // Set properties
		 $objPHPExcel->getProperties()->setCreator("NguyenDung")
                            ->setLastModifiedBy("NguyenDung")
                            ->setTitle('Daily Selling Report')
                            ->setSubject("Office 2007 XLSX Document")
                            ->setDescription("Daily Selling Report All Sale")
                            ->setKeywords("office 2007 openxml php")
                            ->setCategory("GAS");
		 $objPHPExcel->getActiveSheet()->setTitle('Daily Selling Report');
		 //             
		 $objPHPExcel->setActiveSheetIndex(0);
		 $objPHPExcel->getActiveSheet()->setCellValue("A1", 'Hạn thu');
		 $index = 2;
		 foreach ($aSale as $sale_id=>$mSale){
			$objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index)."1", $mSale->first_name);
			$index++;
		 }			 
		 $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index)."1", 'Tổng cộng');
		$objPHPExcel->getActiveSheet()
				->getStyle('A1:'.MyFunctionCustom::columnName($index+count($aSale))."1")
				->getFont()
				//->setSize(14)
				->setBold(true);			 
		$begin = new DateTime( $sell_date_from );
		$end = new DateTime( $sell_date_to );
		$end->modify('+1 day'); // cần cộng thêm 1 day cho đúng với ngày chọn ở giao diện
        //return $date_pay->format('Y-m-d');
		$interval = DateInterval::createFromDateString('1 day');
		$period = new DatePeriod($begin, $interval, $end);

		$index = 1;
		$rowNum = 1;
		$sumRow = 0;
		$sumAll=0;
		$sumCol = array();
	 	foreach ( $period as $key=>$dt ){
			$ymd = $dt->format( "Y-m-d" );
			$dmyShow = $dt->format( Yii::app()->params['dateFormat'] );
			$objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index).($rowNum+$key+1), $dmyShow);
			$indexSale = 1;
			 foreach ($aSale as $sale_id=>$mSale){
				if(isset($aExport[$ymd][$sale_id])){
					$objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index+$indexSale).($rowNum+$key+1), $aExport[$ymd][$sale_id]);
					$sumRow += $aExport[$ymd][$sale_id];
					$sumAll += $aExport[$ymd][$sale_id];
					if(isset($sumCol[$sale_id]))
						$sumCol[$sale_id] += $aExport[$ymd][$sale_id];
					else
						$sumCol[$sale_id] = $aExport[$ymd][$sale_id];
				}
					
				$indexSale++;
			 }	
			 // for sum row
			 if($sumRow>0)
				$objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index+count($aSale)+1).($rowNum+$key+1), $sumRow);
			 $sumRow=0;
			 // for sum row
			$objPHPExcel->getActiveSheet()
					->getStyle(MyFunctionCustom::columnName($index+1).($rowNum+$key+1).':'.MyFunctionCustom::columnName($index+count($aSale)+1).($rowNum+$key+1) )->getNumberFormat()
					->setFormatCode('#,##0');
				
		} // end foreach list days
		$objPHPExcel->getActiveSheet()->setCellValue("A".($rowNum+$key+2), 'Tổng cộng');  
		//$index = 1;
		$indexSale = 1;
		foreach ($aSale as $sale_id=>$mSale){
                        if(isset($sumCol[$sale_id]))
                            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index+$indexSale).($rowNum+$key+2), $sumCol[$sale_id]);
			$indexSale++;
		 }
		 $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index+count($aSale)+1).($rowNum+$key+2), $sumAll);
		 
		$objPHPExcel->getActiveSheet()
				->getStyle(MyFunctionCustom::columnName($index+1).($rowNum+$key+2).':'.MyFunctionCustom::columnName($index+count($aSale)+1).($rowNum+$key+2) )->getNumberFormat()
				->setFormatCode('#,##0');		 
		$objPHPExcel->getActiveSheet()
				->getStyle(MyFunctionCustom::columnName($index).($rowNum+$key+2).':'.MyFunctionCustom::columnName($index+count($aSale)+1).($rowNum+$key+2) )
				->getFont()
				//->setSize(14)
				->setBold(true);	
		$objPHPExcel->getActiveSheet()
				->getStyle(MyFunctionCustom::columnName($index+count($aSale)+1).'1:'.MyFunctionCustom::columnName($index+count($aSale)+1).($rowNum+$key+2) )
				->getFont()
				//->setSize(14)
				->setBold(true);		 
		$objPHPExcel->getActiveSheet()->getStyle(MyFunctionCustom::columnName($index).'1:'.MyFunctionCustom::columnName($index+count($aSale)+1).($rowNum+$key+2) )
                        ->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);					
		 //save file 
		 $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
		 //$objWriter->save('MyExcel.xslx');

		 for($level=ob_get_level();$level>0;--$level)
		 {
			 @ob_end_clean();
		 }
		 header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
		 header('Pragma: public');
		 header('Content-type: '.'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		 header('Content-Disposition: attachment; filename="'.'DailySelling'.'.'.'xlsx'.'"');
		 header('Cache-Control: max-age=0');				
		 $objWriter->save('php://output');			
		 Yii::app()->end();		  

	}	

	public function defaultScope()
	{
		return array();
	}
        
        
//SELECT * FROM `gas_gas_daily_selling` `t` WHERE ((month(t.date_sell)=6) AND (year(t.date_sell)=2013)) AND (t.price_1_kg>0 OR t.price_b_12>0) GROUP BY t.customer_id ORDER BY t.date_sell DESC
// SELECT DISTINCT(customer_id), date_sell, price_1_kg FROM `gas_gas_daily_selling` `t` WHERE ((month(t.date_sell)=6) AND (year(t.date_sell)=2013)) AND (t.price_1_kg>0 OR t.price_b_12>0) ORDER BY t.date_sell DESC        
        public static function getPriceLatestInMonth($month, $year){
			// 1. get customer id unquine in month
				$criteria=new CDbCriteria();
				$criteria->select = 'DISTINCT(customer_id) as customer_id';
				$criteria->addCondition('month(t.date_sell)= '.$month);			
				$criteria->addCondition('year(t.date_sell)= '.$year);			
				$criteria->addCondition('t.price_1_kg>0 OR t.price_b_12>0');				
				$aCusId = GasDailySelling::model()->findAll($criteria);			
			
			// 2. forech cus id get row  MAX(t.date_sell) for one cus id
				$aReturn = array();
				if(count($aCusId)>0)
                foreach($aCusId as $cus_id){
					$criteria=new CDbCriteria();
					$criteria->addCondition('month(t.date_sell)= '.$month);			
					$criteria->addCondition('year(t.date_sell)= '.$year);			
					$criteria->addCondition('t.price_1_kg>0 OR t.price_b_12>0');		
					$criteria->compare('t.customer_id',$cus_id->customer_id);		
					$criteria->order = "t.date_sell DESC";
					$res = GasDailySelling::model()->find($criteria);					
					if($res){
						$aReturn[$res->customer_id] = array('price_1_kg'=>$res->price_1_kg,'price_b_12'=>$res->price_b_12);
						echo $res->customer_id.'  '.$res->date_sell.'  '.$res->price_1_kg.'<br/>';
					}			
                }				
				return $aReturn;
				die;
		
		// đoạn dưới dùng tham khảo, không sử dụng nữa
				$model=new GasDailySelling();
				$model->unsetAttributes();
				$criteria=new CDbCriteria();
				$criteria->select='t.customer_id, MAX(t.date_sell), t.price_1_kg, t.price_b_12';
               /*  $criteria->compare('month(t.date_sell)',$month);		
                $criteria->compare('year(t.date_sell)',$year);		 */
				$criteria->addCondition('month(t.date_sell)= '.$month);			
				$criteria->addCondition('year(t.date_sell)= '.$year);			
				$criteria->addCondition('t.price_1_kg>0 OR t.price_b_12>0');				
				$criteria->group = 't.customer_id';
				$criteria->having = 'MAX(price_1_kg)';
				$criteria->order = "t.customer_id ASC";
				$res = GasDailySelling::model()->findAll($criteria);
				
                if(count($res)>0)
                foreach($res as $obj){
                        echo $obj->customer_id.'  '.$obj->date_sell.'  '.$obj->price_1_kg.'<br/>';
//                        $aExport[$obj->date_of_payment][$key] = $obj->total;					
                }					
				die;
				
				
				$subQuery=$model->getCommandBuilder()->createFindCommand($model->getTableSchema(),$criteria)->getText();
				//var_dump($subQuery);die;
				$mainCriteria=new CDbCriteria();
				$mainCriteria->alias = "m";
				$mainCriteria->condition =' (m.date_sell) in ('.$subQuery.') ';
				$mainCriteria->order = "m.date_sell DESC";
				$mainCriteria->order = "m.customer_id ASC";
				//$mainCriteria->group = 'm.customer_id';
				$res = GasDailySelling::model()->findAll($mainCriteria);
				
                if(count($res)>0)
                foreach($res as $obj){
                        echo $obj->customer_id.'  '.$obj->date_sell.'  '.$obj->price_1_kg.'<br/>';
//                        $aExport[$obj->date_of_payment][$key] = $obj->total;					
                }				
				
				die;
				
                $aExport = array();
                $criteria=new CDbCriteria;
                $criteria->compare('month(t.date_sell)',$month);		
                $criteria->compare('year(t.date_sell)',$year);		
                $criteria->addCondition('t.price_1_kg>0 OR t.price_b_12>0');
                $criteria->select = 'DISTINCT(customer_id)';
                $res = GasDailySelling::model()->findAll($criteria);
                $aCus=array();
                if(count($res)>0){
                    foreach ($res as $item){
                        $aCus[]=$item->customer_id;
                    }
                }
                
                return array('aExport'=>$aExport,'aSale'=>$aSale,'date_from'=>$date_from,'date_to'=>$date_to);
        }                
        
        public static function getPriceLatestOfCustomerInMonth($user_id, $month, $year){
                $aExport = array();
                $criteria=new CDbCriteria;
                $criteria->compare('month(t.date_sell)',$month);		
                $criteria->compare('year(t.date_sell)',$year);		
                $criteria->compare('t.customer_id',$user_id);		
                $criteria->addCondition('t.price_1_kg>0 OR t.price_b_12>0');
//                $criteria->select = 'DISTINCT(customer_id), date_sell, price_1_kg1';
//                $criteria->group = 't.customer_id';

                $criteria->order = "t.date_sell DESC";
                return $res = GasDailySelling::model()->find($criteria);
        }               
        
        
	protected function beforeSave()
	{
            $this->date_sell =  MyFormat::dateConverDmyToYmd($this->date_sell);        
			$mUser=Users::model()->findByPk($this->customer_id);
			$mTypePay = GasTypePay::model()->findByPk($mUser->payment_day);
			$aDate =  explode('-', $this->date_sell);
			$this->date_of_payment =null;
			if($mTypePay)
				$this->date_of_payment = MyFunctionCustom::getDateOfPayment($this->date_sell, $mTypePay, $aDate[2], $aDate[1], $aDate[0]);

			$this->total = ( $this->quantity_50*KL_BINH_50 + $this->quantity_45*KL_BINH_45 - $this->quantity_remain) * $this->price_1_kg + ($this->quantity_12*$this->price_b_12) ;				
			$this->sale_id = $this->customer?$this->customer->sale_id:0;	
            return true;
        }        
        
}