<?php

/**
 * This is the model class for table "{{_gas_issue_tickets_detail}}".
 *
 * The followings are the available columns in table '{{_gas_issue_tickets_detail}}':
 * @property string $uid_post_role_id
 * @property string $ticket_id
 * @property string $message
 * @property string $uid_post
 * @property string $created_date
 * @property string $c_name
 */
class GasIssueTicketsDetail extends BaseSpj
{
    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return GasIssueTicketsDetail the static model class
     */
    public $code_no,$autocomplete_name,$send_to_id,$autocomplete_name_1,$date_from,$date_to;
    
    /** @Author: DungNT Aug 29, 2019
     *  @Todo: get list role will hide name when show post on email, app, web 
     **/
    public function getArrayRoleHideName() {
        return [ROLE_CALL_CENTER, ROLE_DIEU_PHOI];
    }
    
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{_gas_issue_tickets_detail}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        return array(
            array('uid_post_role_id, employee_problems_id, id, ticket_id, message, uid_post, created_date, c_name, send_to_id, '
                . 'date_from,date_to,uid_post,code_no', 'safe'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        return array(
            'rUidPost' => array(self::BELONGS_TO, 'Users', 'uid_post'),
            'rFileDetail' => array(self::HAS_MANY, 'GasIssueTicketsDetailFile', 'issue_tickets_detail_id',
                'order'=>'rFileDetail.id ASC',
            ),
            'rMoveToUid' => array(self::BELONGS_TO, 'Users', 'move_to_uid'),
            'rIssueTickets' => array(self::BELONGS_TO, 'GasIssueTickets', 'ticket_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'ticket_id' => 'Ticket',
            'message' => 'Message',
            'uid_post' => 'Uid Post',
            'created_date' => 'Created Date',
            'c_name' => 'C Name',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
//    public function search()
//    {
//
//        $criteria=new CDbCriteria;
//        $criteria->compare('t.id',$this->id);
//        return new CActiveDataProvider($this, array(
//            'criteria'=>$criteria,
//            'pagination'=>array(
//                'pageSize'=> Yii::app()->user->getState('pageSize',Yii::app()->params['defaultPageSize']),
//            ),
//        ));
//    }
    
    protected function beforeSave() {
        $mUser = $this->rUidPost;
        if($mUser){
            $this->uid_post_role_id = $mUser->role_id;
        }
        return parent::beforeSave();
    }

    protected function beforeDelete() {
        MyFormat::deleteModelDetailByRootId('GasIssueTicketsDetailFile', $this->id, 'issue_tickets_detail_id');
        return parent::beforeDelete();
    }
    
    public function getUidLogin() {
        $mUser = $this->rUidPost;
        if($mUser){
            return $mUser->first_name;
        }
        return '';
    }
    // @Todo: show name user post but hide for role ROLE_CALL_CENTER, ROLE_DIEU_PHOI
    public function getUidLoginByApp() {
        $res = 'User';
        if(!in_array($this->uid_post_role_id, $this->getArrayRoleHideName())){
            $res  = ($this->uid_post == GasConst::UID_ADMIN ? 'Nguyễn Tiến Dũng<br>[ IT ]' : GasTickets::ShowNameReplyAtDetailTicket($this->rUidPost, array('mDetailTicket'=>$this)) );
        }
        return str_replace('<br>', ' - ', $res);
    }
    
    public function getMessage() {
        return nl2br($this->message);
    }
    public function getMessageApp() {
        return $this->message;
    }
    public function getRenewalDate() {
        if(empty($this->renewal_date) || $this->renewal_date == '0000-00-00'){
            return ;
        }
        return MyFormat::dateConverYmdToDmy($this->renewal_date);
    }
    public function getRenewalDateOnGrid() {
        $res = $this->getRenewalDate();
        if(empty($res)){
            return ;
        }
        return "<b>Gia hạn đến: $res</b>";
    }
    
    public function getProblemOnGrid() {
        if(empty($this->employee_problems_id)){
            return '';
        }
        $mEmployeeProblems = new EmployeeProblems();
        $aType = $mEmployeeProblems->getArrayType();
        $aTypeToMoney = $mEmployeeProblems->getArrayMoneyType();
        if(!isset($aType[$this->employee_problems_id])){
            return '';
        }
        return $aType[$this->employee_problems_id]. ': '. ActiveRecord::formatCurrency($aTypeToMoney[$this->employee_problems_id]);
    }
    
    public function getNameOnly() {
        $temp = explode('<br>', $this->c_name);
        return isset($temp[0]) ? $temp[0] : '';
    }
    
    /**
     * @Author: DungNT Nov 09, 2015
     * @Todo: get array file 123
     */
    public function ApiGetFileUpload() {
        $aRes = array();
        $mImageProcessing               = new ImageProcessing();
        $mImageProcessing->sourceFrom   = BaseSpj::SOURCE_FROM_APP;
        foreach ($this->rFileDetail as $mIssueTicketDetail){
            $tmp = array();
//            $tmp['thumb'] = $mImageProcessing->changeImageUrlForApp(ImageProcessing::bindImageByModel($mIssueTicketDetail,'','',array('size'=>'size1')));
//            $tmp['large'] = $mImageProcessing->changeImageUrlForApp(ImageProcessing::bindImageByModel($mIssueTicketDetail,'','',array('size'=>'size2')));
            $tmp['thumb'] = $mImageProcessing->getUrlImageByModel($mIssueTicketDetail,'','',array('size'=>'size1'));
            $tmp['large'] = $mImageProcessing->getUrlImageByModel($mIssueTicketDetail,'','',array('size'=>'size2'));
            $aRes[] = $tmp;
        }
        return $aRes;
    }
    
    public function getMoveToUid() {
        $mUser = $this->rMoveToUid;
        if($mUser){
            return "Chuyển cho: ".$mUser->first_name;
        }
        return '';
    }
    
    /**
     * @Author: DungNT Dec 30, 2015
     * @Todo: get distinct array ticket_id by user id post
     */
    public static function getIssueIdByUserPost($uid_post) {
        $criteria = new CDbCriteria();
        $criteria->compare("t.uid_post", $uid_post);
        $criteria->select = "distinct(t.ticket_id) as ticket_id";
        return CHtml::listData(self::model()->findAll($criteria), 'ticket_id', 'ticket_id');
    }
    
    
    /** @Author: DungNT Mar 30, 2018
     *  @Todo: get record by ticket id
     **/
    public function getByTicketId() {
        $criteria = new CDbCriteria();
        $criteria->compare('t.ticket_id', $this->ticket_id);
        $criteria->order = 't.id DESC';
        return self::model()->findAll($criteria);
    }
    
    public function getGasIssueTickets() {
        return GasIssueTickets::model()->findByPk($this->ticket_id);
    }
    
    public function getCreatedDateTicketsDetail() {
        return MyFormat::dateConverYmdToDmy($this->created_date, "d/m/Y H:i");
    }
    
    public function getMessageDetail(){
        $user = Users::model()->findByPk($this->uid_post);
        if(!empty($user)){
            $message =   " ( ". $this->getInfoUserPost() ." - ".$this->getCreatedDateTicketsDetail(). "  ) " . $this->message . "<br>";
            return $message;
        }
        $message =   " ( - ".$this->getCreatedDateTicketsDetail(). "  ) " . $this->message . "<br>";
        return $message;

    }
    
    public function search(){
        $criteria= new CDbCriteria;

        $criteria->addCondition('t.message <> ""');
            if(!empty($this->code_no)){
                $gasTickets = $this->getGasIssueTicketsByCode( $this->code_no);
                $criteria->compare('t.ticket_id',$gasTickets->id);

            }
            if(!empty($this->uid_post) && empty($this->send_to_id) ){
               $criteria->compare('t.uid_post',$this->uid_post);

            }
            if(!empty($this->send_to_id) && empty($this->uid_post)){
               $criteria->compare('t.uid_post',$this->send_to_id);

            }
            if(!empty($this->uid_post) && !empty($this->send_to_id)){
                $criteria->addInCondition('t.uid_post',array($this->uid_post,$this->send_to_id));
            }
            if(!empty($this->date_from)){
                $date_from = MyFormat::dateDmyToYmdForAllIndexSearch($this->date_from);
            }

            if(!empty($this->date_to)){
                $date_to = MyFormat::dateDmyToYmdForAllIndexSearch($this->date_to);
            }
            if(!empty($date_from) && !empty($date_to)){
                $criteria->addBetweenCondition("DATE(t.created_date)",$date_from,$date_to);
            }
            
            
        
         $criteria->order = 't.created_date DESC';
        
        
//        $criteria->compare('t.uid_login',$this->uid_login);
//        $criteria->compare('t.send_to_id',$this->send_to_id);
        
        
        
        return new CActiveDataProvider($this, array(
                'criteria'=>$criteria,
                'pagination'=>array(
                    'pageSize'=> Yii::app()->user->getState('pageSize',Yii::app()->params['defaultPageSize']),
                ),
        ));
    }
    public function getGasIssueTicketsByCode($code){
        $criteria= new CDbCriteria;
        $criteria->compare('t.code_no',$code,true);
        return GasIssueTickets::model()->find($criteria);
    }
    /** @Author: Pham Thanh Nghia Oct 8, 2018
     *  @Todo: lay sanh NV post
     **/
    public function getListUserPost(){
        $criteria= new CDbCriteria;
        $criteria->select = "t.uid_post";
        $criteria->distinct = true;
        $criteria->group = "t.uid_post";
        $model = GasIssueTicketsDetail::model()->findAll($criteria);
        $aRes = CHtml::listData($model, 'uid_post', 'uid_post');
        return $aRes;
    }
    
    /**
     * @Author: DungNT May 18, 2017
     * @Todo: get info phone + số đt agent
     */
    public function getInfoUser() {
        $res = '';
        $mUser = $this->rUidPost;
        $mAppCache = new AppCache();
        if($mUser){
            if(!empty($mUser->phone)){
                $aPhone = explode('-', $mUser->phone);
                $res .= '<br>ĐT: ';
                foreach($aPhone as $phone){
                    $res .=  UsersPhone::formatPhoneView($phone) . '<br>';
                }
            }
            $aRoleAllow = [ROLE_EMPLOYEE_MAINTAIN];
            $aAgent     = Users::GetKeyInfo($mUser, UsersExtend::JSON_ARRAY_AGENT);
            if(in_array($mUser->role_id, $aRoleAllow) && is_array($aAgent)){
                $agent_id   = reset($aAgent);
                $aAgent     = $mAppCache->getAgentListdata();
                if(isset($aAgent[$agent_id])){
                    $res .= $aAgent[$agent_id];
                }
            }
        }
        return $res;
    }
    
    /** @Author: DungNT Now 14, 2018
     *  @Todo: get html button delete detail 
     **/
    public function renderButtonDelete() {
        $html   = '';
        $url    = Yii::app()->createAbsoluteUrl('admin/gasIssueTickets/deleteOneDetail', array('id'=>$this->id));
        $html .="<form method='post' class='button_to w-200 padding_0' action='$url'>";
            $html .="<div class='btn_delete_one_detail'>";
                $html .="<input type='submit' value='Delete One Detail' class=' w-200 padding_0 padding_5 btn  btn_closed_tickets ' title='Delete detail issue này' alert_text='Delete detail issue này?'> ";
            $html .="</div>";
        $html .="</form>";
        return $html;
    }
    /** @Author: DungNT Now 14, 2018
     *  @Todo: get html button punish 50k
     **/
    public function renderButtonPunish() {
        $aUidNotAllow = [GasConst::UID_ADMIN, GasLeave::UID_DIRECTOR];
        $cUid = MyFormat::getCurrentUid();
        if(in_array($this->uid_post, $aUidNotAllow)){
            return '';
        }
        $punishText = "Phạt {$this->getNameOnly()} 50K get html button punish 50k";
        
        $html   = '';
        $url    = Yii::app()->createAbsoluteUrl('admin/gasIssueTickets/index', array('punish_ticket'=>$this->ticket_id));
        $html .="<form method='post' class='button_to padding_0' action='$url'>";
            $html .="<div class='btn_delete_one_detail'>";
                $html .="<input type='submit' value='$punishText' class=' padding_0 padding_5 btn  btn_closed_tickets ' title='$punishText' alert_text='$punishText'> ";
            $html .="</div>";
        $html .="</form>";
        return $html;
    }
    
    /** @Author: DungNT Aug 29, 2019
     *  @Todo: show name user post but hide for role ROLE_CALL_CENTER, ROLE_DIEU_PHOI
     **/
    public function getInfoUserPost() {
//      DungNT Aug2919 ko cho ai xem NV tổng đài POST - $aRoleViewAll   = [ROLE_DIRECTOR, ROLE_HEAD_GAS_BO, ROLE_DIRECTOR_BUSSINESS, ROLE_ADMIN, ROLE_EMPLOYEE_OF_LEGAL];
        $aRoleViewAll   = [];
        $cRole          = MyFormat::getCurrentRoleId();
        $cUid           = MyFormat::getCurrentUid();
        $res = 'User';
        if($cUid == $this->uid_post || !in_array($this->uid_post_role_id, $this->getArrayRoleHideName()) || in_array($cRole, $aRoleViewAll)){
            $res  = ($this->uid_post == GasConst::UID_ADMIN ? 'Nguyễn Tiến Dũng<br>[ IT ]' : GasTickets::ShowNameReplyAtDetailTicket($this->rUidPost, array('mDetailTicket'=>$this)) );
            $res .= $this->getInfoUser();
        }
        return $res;
    }
    
    /** @Author: DuongNV Sep1619
     *  @Todo: get html button phat NV 50k
     **/
    public function renderButtonPunishEmployee() {
        $cUid               = MyFormat::getCurrentUid();
        $aUidNotShowButton  = [GasLeave::UID_DIRECTOR, $cUid];
        if(in_array($this->uid_post, $aUidNotShowButton)) return '';
        
        $html   = '';
        $name   = empty($this->rUidPost) ? '' : $this->rUidPost->getFullName();
        $url    = Yii::app()->createAbsoluteUrl('admin/gasIssueTickets/punishEmployee', array('id'=>$this->id));
        $html   .= "<form method='post' class='button_to w-200 padding_0' action='$url'>";
        $html   .=      "<div style=''>";
        $html   .=          "<input type='submit' value='Phạt ".$name." 50k' class='padding_0 padding_5 btn js-btn-punish' title='Phạt nhân viên này' data-url='".$url."' alert_text='Phạt nhân viên này?' style='width:auto; background:#f17777;'> ";
        $html   .=      "</div>";
        $html   .= "</form>";
        return $html;
    }
    
    /** @Author: DuongNV Sep1619
     *  @Todo: phat NV 50k
     *  @param $flash array flash message [type, message]
     **/
    public function punishEmployee(&$json = '') {
        $mEmployeeProblems      = new EmployeeProblems();
        $result                 = $mEmployeeProblems->createFromGasIssueTickets($this);
        if(empty($result)){
            $json['success']    = false;
            $json['msg']        = 'Lỗi hệ thống (Không tìm thấy nhân viên)!';
        } else {
            $employee           = empty($result->rEmployee) ? '' : $result->rEmployee->getFullName();
            $json['success']    = true;
            $json['msg']        = 'Phạt thành công NV '.$employee.' số tiền: '.ActiveRecord::formatCurrency($result->money);
        }
    }
    
    /** @Author: DuongNV Sep1619
     *  @Todo: get lish punish info of user (show in Phan Anh Su Viec, ticketDetail)
     **/
    public function getListPunishInfo() {
        $criteria = new CDbCriteria;
        $criteria->compare('t.employee_id', $this->uid_post);
        $criteria->compare('t.object_id', $this->id);
        $criteria->compare('t.object_type', EmployeeProblems::PROBLEM_DIRECTOR_PUNISH);
        $models = EmployeeProblems::model()->findAll($criteria);
        $result = '';
        foreach($models as $mEmployeeProblem){
            $result .= '<b>'.$mEmployeeProblem->message.'</b><br>';
        }
        return $result;
    }
    
    /** @Author: DungNT Oct 06, 2019
     *  @Todo: get html render Punish Info
     * Làm gon form
     **/
    public function getHtmlPunishInfo() {
        $html = $this->getListPunishInfo();
        if(empty($html)){
            return $html;
        }
        return "<br><div class='js-punish-info'>$html</div>";
    }
    
}