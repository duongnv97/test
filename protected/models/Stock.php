<?php

/**
 * This is the model class for table "{{_stock}}".
 *
 * The followings are the available columns in table '{{_stock}}':
 * @property string $id
 * @property string $agent_id
 * @property string $seri
 * @property string $materials_id
 * @property string $materials_type_id
 * @property string $app_order_id
 * @property string $driver_id
 * @property string $car_id
 * @property string $customer_id
 * @property string $created_date
 * @property string $created_date_only
 * @property string $created_by
 * @property string $type_store_card  TYPE_STORE_CARD_IMPORT, TYPE_STORE_CARD_EXPORT;
 */
class Stock extends BaseSpj
{
    public $autocomplete_name_4,$autocomplete_name_1,$autocomplete_name_2, $autocomplete_name_3,$autocomplete_name_5;
    public $viewConfirm = false, $viewFromList = false;
    /****************** ++ NAM ZONE 20180724******************/
    const STATUS_NEW    = 1;
    const STATUS_BACK   = 2;
    const STATUS_LOST   = 3;
    const IS_DIFFERENT  = 1;
    const TYPE_EXPIRED  = 1;

    public $date_from,$date_to,$different,$date_expired;
    public $arrayStatusNotAllow = [
        GasAppOrder::STATUS_CANCEL,
        GasAppOrder::STATUS_NEW,
        GasAppOrder::STATUS_COMPPLETE,
    ];
//    Danh sach material type hiển thị nhập seri
    public $arrayMateriTypeCan = [
        GasMaterialsType::MATERIAL_BINH_45KG,
        GasMaterialsType::MATERIAL_BINH_50KG,
    ];
    
    /** @Author: NamNH 08/08/2018
     *  @Todo: get array status
     **/
    public function getArrayStatus(){
        return [
            self::STATUS_NEW => 'Xuất kho',
            self::STATUS_BACK => 'Đã thu vỏ',
            self::STATUS_LOST => 'Bình lạc',
        ];
    }
    
    /** @Author: NamNH 08/08/2018
     *  @Todo: get status
     **/
    public function getStatus(){
        $aStatus = $this->getArrayStatus();
        return !empty($aStatus[$this->status]) ? $aStatus[$this->status] : '';
    }
    
    /****************** -- NAM ZONE 20180724******************/
    
    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return Stock the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{_stock}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        return [
            array('app_order_id, customer_id', 'required','on' =>'apiUpdate'),
            array('app_order_id, customer_id', 'required','on' =>'create,update'),
            ['status,date_expired,type_store_card, seri_real, different, date_to, date_from, id, agent_id, seri, materials_id, materials_type_id, app_order_id, driver_id, car_id, customer_id, created_date, created_date_only, created_by', 'safe'],
        ];
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        return [
            'rAppOrder' => array(self::BELONGS_TO, 'GasAppOrder', 'app_order_id'),
            'rAgent' => array(self::BELONGS_TO, 'Users', 'agent_id'),
            'rMaterials' => array(self::BELONGS_TO, 'GasMaterials', 'materials_id'),
            'rDriver' => array(self::BELONGS_TO, 'Users', 'driver_id'),
            'rCar' => array(self::BELONGS_TO, 'Users', 'car_id'),
            'rCustomer' => array(self::BELONGS_TO, 'Users', 'customer_id'),
            'rUserCreate' => array(self::BELONGS_TO, 'Users', 'created_by'),
        ];
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'agent_id' => 'Đại lý',
            'seri' => 'STT',
            'materials_id' => 'Vật tư',
            'materials_type_id' => 'Loại vật tư',
            'app_order_id' => 'APP đặt hàng bò mối',
            'driver_id' => 'Lái Xe',
            'car_id' => 'Số xe',
            'customer_id' => 'Khách hàng',
            'created_date' => 'Ngày tạo',
            'created_date_only' => 'Created Date Only',
            'created_by' => 'Người tạo',
            'date_from' => 'Từ ngày',
            'different' => 'Lọc theo',
            'seri_real' => 'STT Thực tế',
            'status'    => 'Trạng thái',
            'type_store_card'    => 'Loại',
        ];
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        $criteria=new CDbCriteria;
        $criteria->compare('t.type_store_card',$this->type_store_card);
        $criteria->compare('t.agent_id',$this->agent_id);
//	$criteria->compare('t.seri',$this->seri,true);
        $criteria->compare('t.materials_id',$this->materials_id);
//	$criteria->compare('t.app_order_id',$this->app_order_id,true);
        $criteria->compare('t.driver_id',$this->driver_id);
        $criteria->compare('t.car_id',$this->car_id);
        $criteria->compare('t.customer_id',$this->customer_id);
        if(!empty($this->date_from)){
            $created_date = MyFormat::dateDmyToYmdForAllIndexSearch($this->date_from);
//            $criteria->addCondition("t.created_date_only >='$created_date'");
            DateHelper::searchGreater($created_date, 'created_date_bigint', $criteria);
        }
        if(!empty($this->date_to)){
            $created_date = MyFormat::dateDmyToYmdForAllIndexSearch($this->date_to);
//            $criteria->addCondition("t.created_date_only <='$created_date'");
            DateHelper::searchSmaller($created_date, 'created_date_bigint', $criteria, true);
        }
        if(!empty($this->different)){
            if($this->different == self::IS_DIFFERENT){
                $criteria->addCondition('t.seri <> t.seri_real');
            }
        }
        $criteria->compare('t.created_by',$this->created_by);
        if(!empty($this->date_expired)){
            $criteria->compare('t.status',self::STATUS_NEW);
            $dateExpired = MyFormat::modifyDays(date('Y-m-d'), -1 * $this->date_expired);
//            $criteria->addCondition('t.created_date_only <= "'.$dateExpired.'"');
            DateHelper::searchSmaller($dateExpired, 'created_date_bigint', $criteria, true);
        }
        $criteria->compare('t.status',$this->status);
        $criteria->order = 't.id DESC';
        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
            'pagination'=>array(
                'pageSize'=> 20,
            ),
        ));
    }
    
    /****************** ++ NAM ZONE 20180724******************/

    /** @Author: NamNH 24/07/2018
     *  @Todo: update or create stock
     * $this->mAppUserLogin = Users::model()->findByPk(MyFormat::getCurrentUid());
     * $aStock = array(object(seri,materials_id,materials_type_id))
     * xuất gas đi 
     **/
    public function handleSave($aStock){
        $mAppOrder          = GasAppOrder::model()->findByPk($this->app_order_id);
        $countMaterialStock = [];
        if(empty($mAppOrder)) {
            throw new CHttpException(404, 'The App Order does not exist.');
        }
        $aListDetail = $mAppOrder->getJsonFieldOneDecode('info_gas', 'json', 'baseArrayJsonDecode');
//        delete old
        Stock::model()->deleteAll('app_order_id = ' .$mAppOrder->id );
//        insert new
        $aRowInsert         = [];
        $created_by         = $this->mAppUserLogin->id;
        $created_date_only  = date('Y-m-d');
        $created_date       = date('Y-m-d H:i:s');
        $created_date_bigint= strtotime($created_date);
        $type_store_card    = TYPE_STORE_CARD_EXPORT;
        foreach ($aStock as $key => $aAttributes) {
            if(empty($aAttributes->seri)){
                continue;
            }
            $seri_real = '';
//            báo lỗi khi tìm thấy material không thuộc detail
            if(!empty($countMaterialStock[$aAttributes->materials_id])){
                $countMaterialStock[$aAttributes->materials_id] += 1;
            }else{
                $countMaterialStock[$aAttributes->materials_id] = 1;
            }
//            chặn nếu thêm bình không thuộc đơn hàng
            if(empty($aListDetail[$aAttributes->materials_id])){
                throw new CHttpException(404, 'materials_id '.$aAttributes->materials_id.' không thuộc đơn hàng');
            }
//            Chặn nếu số lượng quá với số lượng đơn hàng
            if($countMaterialStock[$aAttributes->materials_id] > $aListDetail[$aAttributes->materials_id]['qty_real']){
                throw new CHttpException(404, 'materials_id '.$aAttributes->materials_id.' vượt quá số lượng '.$aListDetail[$aAttributes->materials_id]['qty_real']);
            }
            
            $aRowInsert[] = "(
                        '{$type_store_card}',
                        '{$mAppOrder->agent_id}',
                        '{$aAttributes->seri}',
                        '{$seri_real}',
                        '{$aAttributes->materials_id}',
                        '{$aAttributes->materials_type_id}',
                        '{$mAppOrder->id}',
                        '{$mAppOrder->driver_id}',
                        '{$mAppOrder->car_id}',
                        '{$mAppOrder->customer_id}',
                        '{$created_date_only}',
                        '{$created_date}',
                        '{$created_by}',
                        '{$created_date_bigint}'
                        )";
        }
        $tableName = Stock::model()->tableName();
        $sql = "insert into $tableName (
                            type_store_card,
                            agent_id,
                            seri,
                            seri_real,
                            materials_id,
                            materials_type_id,
                            app_order_id,
                            driver_id,
                            car_id,
                            customer_id,
                            created_date_only,
                            created_date,
                            created_by,
                            created_date_bigint
                        ) values" . implode(',', $aRowInsert);
        if (count($aRowInsert)){
            Yii::app()->db->createCommand($sql)->execute();
            $mAppOrder->update_stt      = $mAppOrder->type_stock_done;
            $mAppOrder->uid_update_stt  = $this->mAppUserLogin->id;
            $mAppOrder->update(['update_stt', 'uid_update_stt']);
        }else{
            throw new CHttpException(404, 'Không có seri nào được nhập.');
        }
    }
    
    /** @Author: NamNH 03/08/2018
     *  @Todo: save stock back & update status stock
     **/
    public function stockBack($aStock){
        $mAppCache = new AppCache();
        foreach ($aStock as $key => $mStock) {
//            Logger::WriteLog("Log STT back $mStock->seri");
            $criteria                   =   new CDbCriteria;
            $criteria->compare('t.seri_real', $mStock->seri);
            $criteria->compare('t.customer_id', $mStock->customer_id);// chỗ này có thể không cần limit theo customer_id, mà sẽ tìm theo thương hiệu
            $criteria->compare('t.status', self::STATUS_NEW);
            $mOldStock                  = Stock::model()->find($criteria);
            $aGasVo = $mAppCache->getListdataGasVo();
            $mGasMaterials = new GasMaterials();
            if(!empty($mOldStock) && $mGasMaterials->mapGasVo($aGasVo, $mOldStock->materials_id,  $mStock->materials_id)){
//            if(!empty($mOldStock)){// only dev test
//                Kiểm tra trùng khớp bình tại đây
                $mOldStock->status      = self::STATUS_BACK;
                $mOldStock->back_date   = date('Y-m-d');
                $mOldStock->update();
            }else{ // Thực hiện save bình lạc
                $mStock->status         = self::STATUS_LOST;
                $mStock->back_date      = date('Y-m-d');
            }
            $mStock->save();// Aug1618 luôn save record mỗi khi nhập vỏ về hoặc xuất gas đi, để làm bc nhập xuất tồn
        }
    }
    
    /** @Author: ANH DUNG Aug 16, 2018
     *  @Todo: get array STT input khi xuat kho cua 1 order
     **/
    public function getListSttOfOrder() {
        $aRes   = [];
        $models = $this->getByAppOrderId();
        foreach($models as $item){
            if(!empty($item->seri)){
                $aRes[$item->seri]   = $item->seri;
            }
        }
        return $aRes;
    }
    
    /** @Author: ANH DUNG Aug 16, 2018
     *  @Todo: get list record by app_order_id
     **/
    public function getByAppOrderId() {
        $criteria = new CDbCriteria;
        $criteria->compare('t.app_order_id', $this->app_order_id);
        return Stock::model()->findAll($criteria);
    }
    
    /** @Author: ANH DUNG Aug 17, 2018
     *  @Todo: check user can view data
     **/
    public function checkAppView($mAppOrder) {
        if($this->mAppUserLogin->role_id != ROLE_DRIVER || !$this->viewFromList){
            return ;
        }

        if(!in_array($mAppOrder->car_id, $mAppOrder->getArrayCarAllowInputStt())){
            throw new Exception('Bạn không có quyền truy cập hoặc xem thông tin này.');
        }
        
    }
    
    /** @Author: NamNH 24/07/2018
     *  @Todo: get list stock
     * @param: $only_database sử dụng để làm gì?
     **/
    public function getListStock($only_database = false){
        $countMaterialStock = [];
        $result             = [];
        $mAppCache          = new AppCache();
        $mAppOrder          = GasAppOrder::model()->findByPk($this->app_order_id);
        if(empty($mAppOrder)) {
            throw new CHttpException(404, 'Yêu cầu đơn hàng không hợp lệ');
        }
        $this->checkAppView($mAppOrder);
        $aMaterial = $mAppCache->getMasterModel('GasMaterials', AppCache::ARR_MODEL_MATERIAL);
        $mAppOrder->mAppUserLogin      = $this->mAppUserLogin;
        $result                     = $mAppOrder->appView();
        $result['allow_update']     = $this->getAllowUpdate();
        $aListDetail = $mAppOrder->getJsonFieldOneDecode('info_gas', 'json', 'baseArrayJsonDecode');
//        lấy danh sách stock
        $aStock = $this->getByAppOrderId();
        $result['stock'] = [];
        foreach ($aStock as $key => $mStock) {
            if(!empty($countMaterialStock[$mStock->materials_id])){
                $countMaterialStock[$mStock->materials_id] += 1;
            }else{
                $countMaterialStock[$mStock->materials_id] = 1;
            }
            if(!empty($aListDetail[$mStock->materials_id])){
                $result['stock'][] = $this->formatItemView($aMaterial, $mStock);
            }else{
//                Xóa đi nếu không nằm trong đơn hàng
                $mStock->delete();
            }
        }
//        Danh sách detail
        if(!$only_database){// render row empty khi thủ kho nhập xuất update STT
           foreach ($aListDetail as $materials_id => $detail) {
                if(in_array($detail['materials_type_id'], $this->arrayMateriTypeCan)){
                    $countStock = !empty($countMaterialStock[$detail['materials_id']]) ? $countMaterialStock[$detail['materials_id']] : 0;
                    for ($index = $countStock; $index < $detail['qty_real']; $index++) {
                        $result['stock'][] = [
                            'id'                => '',
                            'seri'              => '',
                            'seri_real'         => '',
                            'materials_id'      => $detail['materials_id'],
                            'materials_type_id' => $detail['materials_type_id'],
                            'app_order_id'      => $this->app_order_id,
                            'materials_name'    => isset($aMaterial[$detail['materials_id']]) ? $aMaterial[$detail['materials_id']]['name'] : '',
                        ];
                    }
                }
            } 
        }
        return $result;
    }
    
    /** @Author: ANH DUNG Aug 16, 2018
     *  @Todo: format item app view
     **/
    public function formatItemView($aMaterial, $mStock) {
        $seri       = $mStock->seri;
        $seri_real  = !empty($mStock->seri_real) ? $mStock->seri_real : '';
        if($this->viewConfirm){
            $seri       = '';
        }
        
         return [
            'id'                => $mStock->id,
            'seri'              => $seri,
            'seri_real'         => $seri_real,
            'materials_id'      => $mStock->materials_id,
            'materials_type_id' => $mStock->materials_type_id,
            'app_order_id'      => $this->app_order_id,
            'materials_name'    => isset($aMaterial[$mStock->materials_id]) ? $aMaterial[$mStock->materials_id]['name'] : '',
        ];
    }
    
    /** @Author: NamNH 24/07/2018
     *  @Todo: get allow update
     **/
    public function getAllowUpdate(){
        $mAppOrder = $this->rAppOrder;
        if(empty($mAppOrder)){
            return '0';
        }
        if(in_array($mAppOrder->status, $this->arrayStatusNotAllow)){
            return '0';
        }
        if($this->viewConfirm){// sẽ check đk này sau
            return '1';
        }
        if(!empty($mAppOrder->uid_update_stt) && $mAppOrder->uid_update_stt != $this->mAppUserLogin->id){
            return '0';
        }
        return '1';
    }
    
    /** @Author: NamNH 24/07/2018
     *  @Todo: update or create stock
     **/
    public function handleSaveReal($aStock){
        try {
        $mAppOrder          = GasAppOrder::model()->findByPk($this->app_order_id);
        $countMaterialStock = [];
        if(empty($mAppOrder)) {
            throw new CHttpException(404, 'The App Order does not exist.');
        }
        $aListDetail = $mAppOrder->getJsonFieldOneDecode('info_gas', 'json', 'baseArrayJsonDecode');
        $aSttOfOrder = $this->getListSttOfOrder();
//        insert new
        $aRowInsert = []; $emptyAll = true;
        $tableName = Stock::model()->tableName();
        foreach ($aStock as $key => $aAttributes) {
            if(empty($aAttributes->id)){
                throw new CHttpException(404, 'Danh sách cần cập nhật thiếu dữ liệu');
            }
            if(!empty($aAttributes->seri_real) && !in_array($aAttributes->seri_real, $aSttOfOrder)){
                throw new Exception("STT $aAttributes->seri_real không phải của KH này, vui lòng nhập đúng STT để xác nhận");
            }
            if(!empty($aAttributes->seri_real)){
                $aRowInsert[] = "UPDATE $tableName SET `seri_real` = '{$aAttributes->seri_real}' "
                            . "WHERE `seri` = {$aAttributes->seri_real} AND `app_order_id` = {$this->app_order_id}";
                $emptyAll = false;
                unset($aSttOfOrder[$aAttributes->seri_real]);// DungNT Aug2819
            }
        }
        if(count($aSttOfOrder) > 0){// DungNT Aug2819
            $sStt = implode(',', $aSttOfOrder);
            throw new Exception("Chưa nhập đủ thông tin STT của KH, không thể cập nhật. Nếu có bình chưa giao thì liên hệ thủ kho xóa STT của KH. AppId = $mAppOrder->id");
        }
        
//        if($emptyAll || count($aSttOfOrder) < 1){// chưa xử lý đc Xe Hữu Thành lấy Gas ở Bình Thạnh 1, nên ko check empty đc
        if($emptyAll){// chưa xử lý đc Xe Hữu Thành lấy Gas ở Bình Thạnh 1, nên ko check empty đc
            $mAppOrder->update_stt =  $mAppOrder->type_stock_done;
            $mAppOrder->update(['update_stt']);
            throw new Exception('Chưa nhập thông tin STT, không thể cập nhật');
        }
        
        $sql = implode(';', $aRowInsert);
        if (count($aRowInsert)){
            Yii::app()->db->createCommand($sql)->execute();
        }
        $mAppOrder->update_stt =  $mAppOrder->type_confirm_real;
        $mAppOrder->update(['update_stt']);
        } catch (Exception $exc) {
             $this->addError('id', $exc->getMessage());
        }
    }
    
    public function canViewMaterial(){
        if($this->isNewRecord){
            return false;
        }
        return true;
    }
    
    /** @Author: NamNH 01/08/2018
     *  @Todo: save in web
     **/
    public function handleSaveWeb(){
        $aStock = [];
        $this->mAppUserLogin = Users::model()->findByPk(MyFormat::getCurrentUid());
        if(!empty($this->seri) && is_array($this->seri)){
            foreach ($this->seri as $key => $value) {
                $aStock[] = (object)$value;
            }
        }
        $this->handleSave($aStock);
    }
    
    /** @Author: NamNH 07/08/2018
     *  @Todo: get array advance search
     **/
    public function getArrayAdvanceSearch(){
        return [
            self::IS_DIFFERENT => 'STT Bị lệch so với ban đầu',
        ];
    }
    /****************** -- NAM ZONE 20180724******************/
    
    /** @Author: Pham Thanh Nghia Jul 25 2018
     *  @Todo:
     *  @Param:
     **/
    public function formatForSave(){
        if($this->isNewRecord){
            $this->created_date = date('Y-m-d H:i:s');
            $this->created_date_only = date('Y-m-d');
            $this->created_by =  MyFormat::getCurrentUid();
        }
    }
    public function canUpdate(){
        return true;
    }
    public function getAgent($field_name = 'first_name'){
        $aAgent = CacheSession::getListAgent();
        return isset($aAgent[$this->agent_id]) ? $aAgent[$this->agent_id]['first_name'] : '';
//        $mUser = $this->rAgent;
//        if($mUser){
//            return $mUser->$field_name;
//        }
//        return '';
    }
    public function getMaterial($field_name = 'name'){
        $aMaterials = CacheSession::getListMaterial();
        return isset($aMaterials[$this->materials_id]) ? $aMaterials[$this->materials_id] : '';
//        $mUser = $this->rMaterials;
//        if($mUser){
//            return $mUser->$field_name;
//        }
//        return '';
    }
    public function getDriver($field_name = 'first_name'){
        $aSessionUser = CacheSession::getListdataByRole(ROLE_DRIVER);
        return isset($aSessionUser[$this->driver_id]) ? $aSessionUser[$this->driver_id] : $this->driver_id;
//        $mUser = $this->rDriver;
//        if($mUser){
//            return $mUser->$field_name;
//        }
//        return '';
    }
    public function getCar($field_name = 'first_name'){
        $aSessionUser = CacheSession::getListdataByRole(ROLE_CAR);
        return isset($aSessionUser[$this->car_id]) ? $aSessionUser[$this->car_id] : $this->car_id;
//        $mUser = $this->rCar;
//        if($mUser){
//            return $mUser->$field_name;
//        }
//        return '';
    }
    public function getCustomer($field_name = 'first_name'){
        $mUser = $this->rCustomer;
        if($mUser){
            return $mUser->$field_name;
        }
        return '';
    }
    public function getCreatedBy($field_name = 'first_name'){
        $aSessionUser = CacheSession::getListdataByRole(ROLE_DRIVER);
        return isset($aSessionUser[$this->created_by]) ? $aSessionUser[$this->created_by] : $this->created_by;
//        $mUser = $this->rUserCreate;
//        if($mUser){
//            return $mUser->$field_name;
//        }
//        return '';
    }
    public function getSeri(){
        return ($this->seri) ? $this->seri : '';
    }
    public function getSeriReal(){
        return !empty($this->seri_real) ? $this->seri_real : '';
    }
    public function getCreatedDateOnly(){
        return MyFormat::dateConverYmdToDmy($this->created_date_only);
    }

    public function getAppOrderInfo(){
        $mAppOrder = GasAppOrder::model()->findByPk($this->app_order_id);
        if($mAppOrder){
            return $mAppOrder->code_no."<br>".$mAppOrder->getArrayDeliveryText();
        }else{
            return '';
        }
    }
    public function canCreateStock(){
        $allowUpdate = $this->getAllowUpdate();
        if($allowUpdate == 1){
            return true;
        }
        return false;
    }
    
    /** @Author: ANH DUNG Aug 08, 2018
     *  @Todo: check user can update Stt real
     **/
    public function canUpdateSttReal() {
        $AppOrder = $this->rAppOrder;
        if(empty($AppOrder)){
            return false;
        }
        return !in_array($AppOrder->status, $AppOrder->getArrayLockConfirmStt());
    }
    
    /** @Author: NamNH 08/08/2018
     *  @Todo: get high light
     **/
    public function getLost(){
        if($this->status == self::STATUS_LOST){
            return '<span class="high_light_tr"></span>';
        }
        return '';
    }
    
    /**
     * get array search date expired
     * @return array
     */
    public function getArrayExpired(){
        return [
            5     => 'Trên 5 Ngày',
            10    => 'Trên 10 Ngày',
            15    => 'Trên 15 Ngày',
            30    => 'Trên 30 Ngày',
            45    => 'Trên 45 Ngày',
            60    => 'Trên 60 Ngày',
            75    => 'Trên 75 Ngày',
            90    => 'Trên 90 Ngày',
        ];
    }
}