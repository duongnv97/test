<?php
/**
 * Description of gasCashBookReport
 *
 * @author LocNV
 */
class GasCashBookReport extends GasCashBook {
    
    /** @Author: LocNV Sep 3, 2019
     *  @Todo: calculator cash book for all employee by date
     *  @Param: date $date date with format d-m-Y
     **/
    public function calcCashBook($date) {
        $mCashbookDetail = new GasCashBookDetail();
        $mCashbookDetail->date_from_ymd     = MyFormat::dateDmyToYmdForAllIndexSearch($date);
        $mCashbookDetail->date_to_ymd       = MyFormat::dateDmyToYmdForAllIndexSearch($date);
        $mCashbookDetail->release_date      = $mCashbookDetail->date_from_ymd;
        $aModelCashBookDetail   = $mCashbookDetail->getDaily();
        
        $aResult = [];
        
        foreach($aModelCashBookDetail as $key => $item) {
            if($item->customer_id == GasConst::UID_KH_HO_GD) {
                foreach (CmsFormatter::$MASTER_TYPE_CASHBOOK as $type_id => $type_name) {
                    if ($type_id == MASTER_TYPE_REVENUE &&  $item->type==MASTER_TYPE_REVENUE) { // thu
                        if(!isset($aResult[$item->employee_id])) {
                            $aResult[$item->employee_id] = $item->amount; 
                        } else {
                            $aResult[$item->employee_id] += $item->amount;
                        }
                    }
                    //khong tinh gas du va tien chi thu vo, neu co thi mo lai comment code
                    elseif ($type_id == MASTER_TYPE_COST &&  $item->type==MASTER_TYPE_COST && $item->master_lookup->id != GasMasterLookup::ID_CHI_GAS_DU) { // chi
                        if(!isset($aResult[$item->employee_id])) {
                            $aResult[$item->employee_id] = -$item->amount;
                        } else {
                            $aResult[$item->employee_id] -= $item->amount;
                        }
                    }
                }
            }
        }
        
        ksort($aResult);
        
        return $aResult;
    }
    
    /** @Author: LocNV Sep 5, 2019
     *  @Todo: cron run check cashbook report 5 days ago
     *  @Param: date $date date with format d-m-Y
     **/
    public function cronCheckCashBookReport($date) {
        $numOfDate      = 10;
        $mAppCache      = new AppCache();
        $aAgent         = $mAppCache->getAgentListdata();

        $aDataSendMail = [];
        for($day = 1; $day <= $numOfDate; $day++) {
            $dateGetData = MyFormat::modifyDays($date, $day, '-', 'day', 'd-m-Y');

            $mSellReport = new SellReport();
            $aRevenueSellReport = $mSellReport->calcRevenueByDay($dateGetData); //X value

            $mGasCashBook = new GasCashBookReport();
            $aRevenueGasCashBook = $mGasCashBook->calcCashBook($dateGetData); //Y value
            
            foreach ($aRevenueSellReport as $employee_id => $revenue) {
                $userRevenue = empty($aRevenueGasCashBook[$employee_id]) ? 0 : $aRevenueGasCashBook[$employee_id]; //cashbook
                $diff = abs($revenue - $userRevenue);
                if ($revenue != $userRevenue && $diff >= 1000) {
                    $mEmployee = Users::model()->findByPk($employee_id);
                    $aDataSendMail[$dateGetData][$employee_id]['employee_name'] = isset($mEmployee->first_name) ? $mEmployee->first_name : '';
                    $aDataSendMail[$dateGetData][$employee_id]['agent_name'] = isset($aAgent[$mEmployee->parent_id]) ? $aAgent[$mEmployee->parent_id] : '';
                    $aDataSendMail[$dateGetData][$employee_id]['x'] = $revenue;
                    $aDataSendMail[$dateGetData][$employee_id]['y'] = $userRevenue;
                }
            }
        }
        $this->sendMailCashBookReport($date, $aDataSendMail, $numOfDate);
        
        // Send log
        $needMore['title']      = 'cronCheckCashBookReport - Báo cáo sổ quỹ đầy đủ';
        $needMore['list_mail']  = ['duongnv@spj.vn'];
        $infoLog                = "cronCheckCashBookReport ".date('d/m/Y H:i:s')."<br>Báo cáo sổ quỹ đầy đủ (quét lệch cảnh báo)";
        SendEmail::bugToDev($infoLog, $needMore);
        $strLog = '******* cronCheckCashBookReport done at '.date('d-m-Y H:i:s');
        Logger::WriteLog($strLog);
    }
    
    /** @Author: LocNV Sep 5, 2019
     *  @Todo: send mail cash book report
     *  @Param: date $date date with format d-m-Y
     *  @Param: array $aDataSendMail data for send mail
     *  @Param: int $numOfDate number of date
     **/
    public function sendMailCashBookReport($date, $aDataSendMail, $numOfDate) {
        $needMore                   = ['title'=> 'Báo cáo sổ quỹ đầy đủ của sổ quỹ ngày ' . $date];
//        $needMore['SendNow']        = 1; // open test
        $needMore['list_mail'] = [
            'duongnv@spj.vn',
            'dungnt@spj.vn',
            'ketoan.kv@spj.vn',
            'ketoan.vanphong@spj.vn',
        ];

        $sBody = 'Hi các KTKV,';
        $sBody .= '<br><b>Danh sách số tiền lệch quỹ của nhân viên trong vòng ' . $numOfDate . ' ngày gần đây: </b><br>';

        $sBody .= '<br><table style="border-collapse: collapse; width: 100%;" border="1" cellpadding="5">';
        $sBody .=    '<thead>';
        $sBody .=        '<tr>';
        $sBody .=            '<th>STT</th>';
        $sBody .=            '<th>Tên NV</th>';
        $sBody .=            '<th>Đại lý</th>';
        $sBody .=            '<th>DT bán hàng HGD</th>';
        $sBody .=            '<th>DT sổ quỹ</th>';
        $sBody .=            '<th>Số tiền lệch</th>';
        $sBody .=        '</tr>';
        $sBody .=    '</thead>';
        $sBody .=    '<tbody>';
        if (!empty($aDataSendMail)) {
            foreach ($aDataSendMail as $dateGetData => $aInfo1) {
                if (!empty($aInfo1)) {
                $no = 1;
                $sBody .= '<tr>';
                $sBody .= '<td colspan="6" style="text-align: center; font-weight: bold;">' . $dateGetData . '</td>';
                $sBody .= '</tr>';
                    foreach ($aInfo1 as $employee_id => $aInfo2) {
                        $diff = $aInfo2['x'] - $aInfo2['y'];
                        if (abs($diff) < 1000) continue;
                        $sBody .= '<tr>';
                        $sBody .= '<td style="text-align:center;">' . $no++ . '</td>';
                        $sBody .= '<td>' . $aInfo2['employee_name'] . '</td>';
                        $sBody .= '<td>' . $aInfo2['agent_name'] . '</td>';
                        $sBody .= '<td style="text-align:right;">' . ActiveRecord::formatCurrency($aInfo2['x']) . '</td>';
                        $sBody .= '<td style="text-align:right;">' . ActiveRecord::formatCurrency($aInfo2['y']) . '</td>';
                        $sBody .= '<td style="text-align:right;">' . ActiveRecord::formatCurrency($diff) . '</td>';
                        $sBody .= '</tr>';
                    }
                }
            }
        } else {
            $sBody .= '<tr>';
            $sBody .= '<td colspan="6" style="text-align: center;">Không có dữ liệu</td>';
            $sBody .= '</tr>';
        }
        $sBody .=    '</tbody>';
        $sBody .= '</table>';
        $sBody .= '<br>Trân trọng!';
        
        SendEmail::bugToDev($sBody, $needMore);
    }
}
