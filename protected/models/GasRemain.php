<?php

/**
 * This is the model class for table "{{_gas_remain}}".
 *
 * The followings are the available columns in table '{{_gas_remain}}':
 * @property string $id
 * @property string $date_input
 * @property string $agent_id 
 * @property string $seri
 * @property string $amount_empty
 * @property string $amount_has_gas
 * @property integer $status
 * @property string $update_status_id
 * @property string $update_status_date
 * @property string $created_date
 */
class GasRemain extends BaseSpj
{
    public $aDataStock=[], $mCustomer=null, $needMore = [], $mSell = null, $mSellDetailVo = null, $mSellDetailGas = null, $mRemainOld = null,$date_from_ymd='', $date_to_ymd='', $gas_remain_type, $aDataApp = [], $aSeri = [], $aDetail = [], $province_id,$autocomplete_name, $autocomplete_name_1, $autocomplete_name_2;
    public static $days_allow_delete = 5; // admin delete
    // không dùng ở đây nữa mà đã chuyển qua Yii::app()->params
    public static $days_allow_update = 10; // agent update
    // không dùng ở đây nữa mà đã chuyển qua Yii::app()->params
    public static $days_allow_update_remain2_3 = 10;
    // không dùng ở đây nữa mà đã chuyển qua Yii::app()->params
    public $NotViewWarehouse, $ext_have_customer, $ext_month, $ext_year, $notReweighed = 0;
    public $amountHasGas2=0, $date_from, $date_to, $amount_gas_vo_binh, $statistic_year, $statistic_month; // Dec 28, 2014 for thống kê
    const SERI_TDV = -1;// Aug1517 để save gas dư cân thiếu đầu vào của Xe tải
    const STATUS_NOT_EXPORT         = 0;// còn tồn
    const STATUS_EXPORT             = 1;// đã xuất
    const STATUS_WAIT_EXPORT        = 2;// chờ xuất
    const STATUS_NOT_REWEIGHED      = 3;// Chưa cân lần 2

    const SOURCE_WEB                = 1;// là sinh từ đơn bò mối app giao nhận
    const SOURCE_APP                = 2;// Giao nhận tự tạo trên app
    const SOURCE_HGD                = 3;// Auto insert gas dư B12 from App HGD

    const APP_CREATE_NORMAL     = 1;
    const APP_CREATE_THU_KHO    = 2;
    const APP_EXPORT_TO_AGENT   = 3;// Jun0518 xuat gas du cho DL khac tren he thong
    
    const AMOUNT_ALARM    = 1; // 1kg
    /** @note Mar1118 gas dư của PVKH, không còn nợ trên record chính nữa
        1/ từ KH về đại lý -- done model GasAppOrder -> makeGasRemain()
        2/ từ đại lý về kho -- done model GasAppOrder -> makeGasRemain()
        3/ kho cân lại lần 2  -- done model GasRemain -> calcDriverMissWeight()
        4/ web update đơn  -- done model GasRemain -> updateWeb()
     */
    const EMPLOYEE_DEBIT_YES    = 1; // 1: nợ (còn tồn)
    const EMPLOYEE_DEBIT_NO     = 2; // 2: không nợ cty (đã xuất)
    
    public static $agent_view_all=array(
        26678,  // Kho Tân Sơn	
        26677,  // Kho Bến Cát
        25785,  // Kho Bến Cát
    );
    
    public static $UID_CAN_LAN_2_3 = array(
        129127,
//        48712,
    );

    public static $SELECT_ONLY_GAS_REMAIN = array(
        0 => 'Có Bán Hàng',
        1 => 'Chỉ Cân Gas Dư Không Có Bán Hàng'
    );

    public static $TYPE_HAS_EXPORT = array(
        GasRemain::STATUS_NOT_EXPORT    => 'Còn Tồn',
        GasRemain::STATUS_WAIT_EXPORT   => 'Chờ Xuất',
        GasRemain::STATUS_EXPORT        => 'Đã Xuất',
    );
    
    public function getArrayTypeNotExport() {
        return [
            GasRemain::STATUS_NOT_EXPORT,
            GasRemain::STATUS_WAIT_EXPORT,
        ];
    }
    
    /** @Author: DungNT Jun 08, 2018
     *  @Todo: get list xe tải lưu động, xuất trực tiếp gas dư về Kho Phước Tân
     **/
    public function getArrayAgentCar() {
        return [
            GasConst::UID_AGENT_XE_TAI_1    => 'HCM Xe Tải 1 - 51C05420',
            MyFormat::KHO_VUNG_TAU          => 'Kho Nhập Xuất Vũng Tàu',
            GasConst::UID_AGENT_XE_TAI_2    => 'HCM Xe Tải 2 - 61C15080',
            GasConst::UID_AGENT_XE_TAI_3    => 'HCM Xe Tải 3 - 51D34398',
        ];
    }
    
    public static function model($className=__CLASS__){
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName(){
        return '{{_gas_remain}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        return array(
            array('date_input, customer_id', 'required', 'on'=>'create,update'),
            array('date_input, customer_id', 'required', 'on'=>'ApiCreate'),
            array('date_input, customer_id, seri, amount_empty, amount_has_gas', 'required', 'on'=>'ApiUpdate'),
            array('id, date_input, agent_id, seri, amount_empty, amount_has_gas, status, created_date, customer_id', 'safe'),
            array('has_export,materials_id, materials_type_id', 'safe'),
            array('amount_gas_2, amount_gas_3, user_update_2, user_update_3, date_update_2, date_update_3', 'safe'),
            array('amount_gas,sale_id,type_customer,ext_have_customer,ext_month,ext_year,amount_gas_vo_binh,only_gas_remain', 'safe'),
            array('amount_gas_2', 'required', 'on'=>'update_amount_gas_2'),
            array('amount_gas_3', 'required', 'on'=>'update_amount_gas_3'),
            array('amount_gas_2,amount_gas_3', 'numerical', 'on'=>'update_amount_gas_2,update_amount_gas_3'),
            array('agent_id_old, pageSize, code_no, car_id, driver_id, notReweighed, amountHasGas2, NotViewWarehouse, gas_remain_type, province_id, date_from, date_to', 'safe'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        return array(
            'customer' => array(self::BELONGS_TO, 'Users', 'customer_id'),
            'rCustomer' => array(self::BELONGS_TO, 'Users', 'customer_id'),
            'agent' => array(self::BELONGS_TO, 'Users', 'agent_id'),
            'rAgent' => array(self::BELONGS_TO, 'Users', 'agent_id'),
            're_user_update_2' => array(self::BELONGS_TO, 'Users', 'user_update_2'), // người cập nhật trạng thái
            're_user_update_3' => array(self::BELONGS_TO, 'Users', 'user_update_3'), // người cập nhật trạng thái
            'rMaterials' => array(self::BELONGS_TO, 'GasMaterials', 'materials_id'),
            'rSale' => array(self::BELONGS_TO, 'Users', 'sale_id'),
            'rCustomerOfAgent' => array(self::BELONGS_TO, 'Users', 'customer_id_of_agent'),
            'rUidLogin' => array(self::BELONGS_TO, 'Users', 'uid_login'),
            'rAgentIdOld' => array(self::BELONGS_TO, 'Users', 'agent_id_old'),
            'rDriver' => array(self::BELONGS_TO, 'Users', 'driver_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'date_input' => 'Ngày Cân',
            'customer_id' => 'Khách Hàng',
            'agent_id' => 'Đại Lý',
            'seri' => 'STT',
            'amount_empty' => 'Trọng Lượng Vỏ Bình',
            'amount_has_gas' => 'Trọng Lượng Gas+Vỏ Bình',
            'amount_gas' => 'Gas Dư',
            'amount_gas_2' => 'Cân Lần 2',
            'amount_gas_3' => 'Cân Lần 3',
            'status' => 'Trạng Thái',
            'created_date' => 'Ngày Tạo',
            'user_update_2' => 'Người Cân Lần 2',
            'user_update_3' => 'Người Cân Lần 3',
            'date_update_2' => 'Ngày Cân Lần 2',
            'date_update_3' => 'Ngày Cân Lần 3',
            'created_date' => 'Ngày Tạo',
            'sale_id' => 'Sale',
            'type_customer' => 'Loại KH',
            'ext_have_customer' => 'KH Cân',
            'ext_month' => 'Tháng',
            'ext_year' => 'Năm',
            'amount_gas_vo_binh' => 'Khối lượng gas + vỏ bình sau khi cân',
            'only_gas_remain' => 'Loại Cân Gas Dư',
            'amount' => 'Thành Tiền',
            'materials_id' => 'Vật Tư',
            'has_export' => 'Trạng Thái',
            'date_from' => 'Từ Ngày',
            'date_to' => 'Đến Ngày',
            'province_id' => 'Tỉnh',
            'NotViewWarehouse' => 'Không xem kho',
            'amountHasGas2' => 'KL Gas+Vỏ Bình lần 2',
            'notReweighed' => 'Chưa cân lần 2',
            'car_id' => 'Số xe',
            'driver_id' => 'Tài xế',
            'code_no' => 'Mã số',
            'pageSize' => 'Số Dòng Hiển Thị',
            'agent_id_old' => 'Đại lý cũ, trước khi xuất gas dư',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        $criteria=new CDbCriteria;
        $this->baseCondition($criteria);
        $this->handleLost($criteria);

        // kho được nhìn thấy hết - chỗ này chỉ ( là role_agnet ) kho và đại lý dc thao tác với gas dư
//        if(!in_array(MyFormat::getAgentId(), GasRemain::$agent_view_all) && Yii::app()->user->role_id==ROLE_SUB_USER_AGENT){
//            $criteria->compare('t.agent_id', MyFormat::getAgentId());
//        }else{
        $criteria->compare('t.agent_id',$this->agent_id);
        $criteria->compare('t.code_no',$this->code_no);
        $criteria->compare('t.driver_id',$this->driver_id);

        $session=Yii::app()->session;
        // danh sách những đại lý giới hạn cho user 
//        if(isset($session['LIST_AGENT_OF_USER']) && count($session['LIST_AGENT_OF_USER'])){            
//            $sParamsIn = implode(',', $session['LIST_AGENT_OF_USER']);
//            $criteria->addCondition("t.agent_id IN ($sParamsIn)");
//        } Dec0217 bỏ giới hạn, cho xem hết hệ thống luôn
        // danh sách những đại lý giới hạn cho user 

        $sessionSum = false;// session bật lên khi thủ kho cân gas dư dc search
        $date_update_2 = $this->date_update_2;
        $re_user_update_2 = $this->re_user_update_2?$this->re_user_update_2->first_name:"";
        if(!empty($this->date_update_2)){
            $sessionSum = true;
            $this->date_update_2 = MyFormat::dateDmyToYmdForAllIndexSearch($this->date_update_2);
            $criteria->addCondition("t.date_update_2 LIKE '%$this->date_update_2%' OR t.date_update_3 LIKE '%$this->date_update_2%' ");
        }
        if(!empty($this->user_update_2)){
            $sessionSum = true;
            $criteria->addCondition("t.user_update_2=$this->user_update_2 OR t.user_update_3=$this->user_update_2");
        }	            
        $criteria->compare('t.only_gas_remain',$this->only_gas_remain);
        GasAgentCustomer::addInConditionAgent($criteria, 't.agent_id');
        
        if(is_array($this->province_id) && count($this->province_id)){
            $mEmployeeCashbook = new EmployeeCashbook();
            $aAgentId = $mEmployeeCashbook->getAgentOfProvince($this->province_id);
            $sParamsIn = implode(',', $aAgentId);
            if(!empty($sParamsIn)){
                $criteria->addCondition("t.agent_id IN ($sParamsIn)");
            }
        }

        $sort = new CSort();
        $sort->attributes = array(
            'date_input'=>'date_input',
            'agent_id'=>'agent_id',
            'seri'=>'seri',
            'amount_empty'=>'amount_empty',
            'amount_has_gas'=>'amount_has_gas',
            'amount_gas'=>'amount_gas',
            'amount_gas_store'=>'amount_gas_store',
            'created_date'=>'created_date',
            'has_export'=>'has_export',
        );    
        $sort->defaultOrder = 't.id DESC';      	
        $_SESSION['data-excel'] = new CActiveDataProvider($this, array(
            'pagination'=>false,
            'criteria'=>$criteria,
            'sort' => $sort,
        ));
        $this->handleSumByDate($sessionSum);
        $this->sumAllPageSeach($criteria);

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
            'pagination'=>array(
//                'pageSize'=> 60,
                'pageSize'=> $this->pageSize,
            ),
            'sort' => $sort,
        ));
    }
    
    public function baseCondition(&$criteria) {
        $date_from = $date_to = '';
        if(!empty($this->date_from)){
            $date_from = MyFormat::dateDmyToYmdForAllIndexSearch($this->date_from);
        }
        if(!empty($this->date_to)){
            $date_to = MyFormat::dateDmyToYmdForAllIndexSearch($this->date_to);
        }
        if(!empty($date_from) && empty($date_to))
                $criteria->addCondition("t.date_input>='$date_from'");
        if(empty($date_from) && !empty($date_to))
                $criteria->addCondition("t.date_input<='$date_to'");
        if(!empty($date_from) && !empty($date_to))
                $criteria->addBetweenCondition('t.date_input', $date_from, $date_to);

        $criteria->compare('t.customer_id',$this->customer_id);
        $criteria->compare('t.sale_id',$this->sale_id);
        if(is_array($this->has_export) && count($this->has_export)){
            $sParamsIn = implode(',', $this->has_export);
            $criteria->addCondition("t.has_export IN ($sParamsIn)");
        }
        if($this->ext_have_customer!=''){
            if($this->ext_have_customer){
                $criteria->addCondition('t.customer_id<>0');
            }else{
                $criteria->addCondition('t.customer_id=0');
            }
        }

        if(!empty($this->ext_month)){
            $criteria->compare('month(t.date_input)', $this->ext_month);
        }	
        if(!empty($this->ext_year)){
            $criteria->compare('year(t.date_input)', $this->ext_year);
        }	
        $criteria->compare('t.seri', trim($this->seri));
        if(!empty($this->car_id)){
            $this->handleAgentCar($criteria, $this->car_id);
        }
        $criteria->compare('t.status', $this->status);
        if(!empty($this->type_customer) && $this->type_customer == -1){
            $sParamsIn = implode(',', array(STORE_CARD_KH_BINH_BO, STORE_CARD_KH_MOI, STORE_CARD_XE_RAO));
            $criteria->addCondition("t.type_customer NOT IN ($sParamsIn)");
        }else{
            $criteria->compare('t.type_customer', $this->type_customer);
        }
        if(!empty($this->update_status_date)){
            $this->update_status_date = MyFormat::dateDmyToYmdForAllIndexSearch($this->update_status_date);
        }	            
        if(!empty($this->created_date)){
            $this->created_date = MyFormat::dateDmyToYmdForAllIndexSearch($this->created_date);
            $criteria->compare('t.created_date',$this->created_date,true);
        }
        if($this->NotViewWarehouse){
            $sParamsIn = implode(',', GasCheck::getAgentNotGentAuto());
            $criteria->addCondition('t.agent_id NOT IN ('.$sParamsIn.')');
        }
        if($this->notReweighed){
            $criteria->addCondition('t.user_update_2 IS NULL OR t.user_update_2=0');
        }
    }
    
    /** @Author: DungNT Nov 26, 2016 */
    public function sumAllPageSeach($criteria) {
        if(!isset($_GET['ajax'])){
            return ;
        }
        $session=Yii::app()->session;
        $c = clone $criteria;
        $c->select = 'sum(amount_gas) as amount_gas';
        $mSum = self::model()->find($c);
        $amount_gas = $mSum->amount_gas;
        $text = "Tổng Cộng Gas Dư: ".ActiveRecord::formatCurrency($amount_gas);
        $session['AMOUNT_TOTAL_GAS_DU'] = $text;
    }
    
    /**
     * @Author: DungNT Nov 26, 2016
     * @Todo: move code old, chắc ko sử dụng nữa
     */
    public function handleSumByDate($sessionSum) {
        $session=Yii::app()->session;
        if($sessionSum){
            // sum amount_gas vì ngày nhập gas dư là 10 ngày trước đó nên khi sum gas dư 
            // cho user kia mình sẽ không sum lượng gas dư cân lần đầu
//                $amount_gas = 0;
//                $sumCriteria = new CDbCriteria();
//                if(!empty($this->date_update_2)){
//                    $sumCriteria->compare('t.date_update_2',$this->date_update_2, true);
//                    $sumCriteria->select = "sum(amount_gas) as amount_gas";
//                    $mSum = self::model()->find($sumCriteria);
//                    $amount_gas_2 = $mSum->amount_gas_2;
//                }

            // sum amount_gas_2
            $sumCriteria = new CDbCriteria();
            if(!empty($this->date_update_2)){
                $sumCriteria->compare('t.date_update_2',$this->date_update_2, true);
            }
            if(!empty($this->user_update_2)){
                $sumCriteria->compare('t.user_update_2',$this->user_update_2);
            }
            $sumCriteria->select = "sum(amount_gas_2) as amount_gas_2";
            $mSum = self::model()->find($sumCriteria);
            $amount_gas_2 = $mSum->amount_gas_2;

            // sum amount_gas_3
            $sumCriteria = new CDbCriteria();
            if(!empty($this->date_update_2)){
                $sumCriteria->compare('t.date_update_3',$this->date_update_2, true);
            }
            if(!empty($this->user_update_2)){
                $sumCriteria->compare('t.user_update_3',$this->user_update_2);
            }
            $sumCriteria->select = "sum(amount_gas_3) as amount_gas_3";
            $mSum = self::model()->find($sumCriteria);
            $amount_gas_3 = $mSum->amount_gas_3;
            $amount_total = ActiveRecord::formatCurrency($amount_gas_2+$amount_gas_3);
            $text = "";
            if(!empty($date_update_2))
                $text  .= "Ngày: $date_update_2 - ";
            if(!empty($re_user_update_2))
                $text  .= "Người Cân: $re_user_update_2 - ";
            $text  .= "Tổng Cộng Gas Dư: $amount_total";
            $session['AMOUNT_TOTAL_GAS_DU'] = $text;
        }else{
            unset($session['AMOUNT_TOTAL_GAS_DU']);
        }
    }

    /** @Author: DungNT Jan 15, 2018
     *  @Todo: giới hạn đk cho các gas dư cân lần 2 bị thiếu
     **/
    public function handleLost(&$criteria){
        if(!isset($_GET['lost'])){
            return ;
        }
        $criteria->addCondition('t.user_update_2 > 0 AND (amount_gas - amount_gas_2) >= '. GasRemain::AMOUNT_ALARM);  
        
    }
    
    public static function saveRemain($model){
        if( isset($_POST['GasRemain']['seri']) && count($_POST['GasRemain']['seri']) ){
            $aRowInsert = []; $created_date = date('Y-m-d H:i:s');
            $date_input = MyFormat::dateConverDmyToYmd($model->date_input);
            MyFormat::isValidDate($date_input);
            $agent_id = MyFormat::getAgentId();
            $uid_login = Yii::app()->user->id;
            $price_binh_bo = GasStoreCard::getPriceBinhBo($model->customer_id, $date_input);
            $date_input_bigint = strtotime($date_input);
            
            foreach ($_POST['GasRemain']['seri'] as $key=>$value) {
                $has_update_balance = 0; 
                $materials_id = $_POST['GasRemain']['materials_id'][$key];
                $amount_empty = $_POST['GasRemain']['amount_empty'][$key];
                $amount_has_gas = $_POST['GasRemain']['amount_has_gas'][$key];
                $amount_gas = $amount_has_gas-$amount_empty;
                $value = MyFormat::removeBadCharacters($value);
                $sale_id = "";
                $type_customer = '';
                if($model->customer){
                    $sale_id = $model->customer->sale_id;                            
                    $type_customer = $model->customer->is_maintain;
                }
                
                $mRemainTemp = new GasRemain();
                $mRemainTemp->materials_id = $materials_id;
                $materials_type_id='';
                if($mRemainTemp->rMaterials){
                    $materials_type_id = $mRemainTemp->rMaterials->materials_type_id;                            
                }
                $model->customer_parent_id = MyFormat::getParentIdForCustomer($model->customer);
                
                $price = $price_binh_bo;
                $amount = $price*$amount_gas;
                
                if($model->only_gas_remain==1 && $amount>0){
                    $has_update_balance = 1;
                    $mRemain = new GasRemain();
                    $mRemain->only_gas_remain = $model->only_gas_remain;
                    $mRemain->amount = $amount;
                    $mRemain->date_input = $date_input;
                    $mRemain->customer_id = $model->customer_id;
                    $mRemain->id = 0;                    
                    self::UpdateReceivablesCustomerYear($mRemain);
                }
                
                
                $aRowInsert[]="('$date_input',
                        '$date_input_bigint',
                        '$model->only_gas_remain',
                        '$has_update_balance',
                        '$model->customer_id',
                        '$model->customer_parent_id',
                        '$sale_id', 
                        '$type_customer', 
                        '$agent_id',
                        '$materials_id',
                        '$materials_type_id',
                        '$uid_login',
                        '$value', 
                        '$amount_empty', 
                        '$price', 
                        '$amount', 
                        '$amount_has_gas',
                        '$amount_gas',
                        '$created_date'
                        )";
                }
                $tableName = self::model()->tableName();
                $sql = "insert into $tableName (date_input,
                            date_input_bigint,
                            only_gas_remain,
                            has_update_balance,
                            customer_id,
                            customer_parent_id,
                            sale_id,
                            type_customer,
                            agent_id,
                            materials_id,
                            materials_type_id,
                            uid_login,
                            seri,
                            amount_empty,
                            price,
                            amount,
                            amount_has_gas,
                            amount_gas,
                            created_date
                            ) values ".implode(',', $aRowInsert);
                if(count($aRowInsert)>0)
                    Yii::app()->db->createCommand($sql)->execute();					                
        }
    }
    
    protected function beforeSave() {
        if($this->scenario == 'AppReweighed'){
            return parent::beforeSave();
        }
        if($this->customer){
            $this->sale_id              = $this->customer->sale_id;                            
            $this->type_customer        = $this->customer->is_maintain;
            $this->customer_parent_id   = MyFormat::getParentIdForCustomer($this->customer);
        }
        if(!$this->isNewRecord){
            $this->amount = $this->price*$this->amount_gas;
            self::UpdateReceivablesCustomerYear($this);
        }
        
        if(empty($this->materials_id)){
            $this->materials_type_id = '';
        }elseif($this->rMaterials){
            $this->materials_type_id = $this->rMaterials->materials_type_id;
        }
        $this->seri = trim($this->seri);
        $this->buildCodeNo();
        $this->date_input_bigint = strtotime($this->date_input);
        
        return parent::beforeSave();
    }
    
    /** @Author: DungNT Mar 11, 2018 make code no**/
    public function buildCodeNo() {
        if(!empty($this->code_no)){
            return ;
        }
        $prefix_code = 'R'.date('y');
        $this->code_no = $prefix_code.ActiveRecord::randString(8, GasConst::CODE_CHAR);
    }

    /**
     * @Author: DungNT Mar 01, 2014
     * @Todo: kiểm tra xem user có dc update gas dư không
     * @param: $model GasRemain
     * @param: $type cân lần 2 hoặc lần 3 GasRemain
     * @Return: true nếu allow, false nếu not allow
     */    
    public static function CanUpdateGasRemain2Or3($model, $type=2){
        return false;
        $dayAllow = date('Y-m-d');
//        $dayAllow = MyFormat::modifyDays($dayAllow, self::$days_allow_update_remain2_3, '-');
        $dayAllow = MyFormat::modifyDays($dayAllow, Yii::app()->params['gas_remain_agent_update_remain2_3'], '-');
        return MyFormat::compareTwoDate($model->created_date, $dayAllow);                
    }
    
    /**
     * @Author: DungNT May 22, 2014
     * @Todo: cập nhật giá bình bò theo customer_id và date_input, dùng để trừ khi tính công nợ cho nhanh - chỉ có cách này
     * DC DÙNG Ở CHỔ NÀO? GasStoreCad::saveStoreCardDetail line 444
     * @Param: $customer_id
     * @Param: $date_input 
     * @Param: $price_binh_bo 
     */
    public static function updatePriceBinhBo($customer_id, $date_input, $price_binh_bo){
        return ; // Close on Sep 07, 2016 vì hiện tại ko dùng đến chức năng này
        
        if(empty($customer_id) || $customer_id<1) return ;
        $aDate = explode('-', $date_input);
        $criteria = new CDbCriteria();
        $criteria->compare("t.customer_id",$customer_id);
        $criteria->compare('month(t.date_input)', $aDate[1]);
        $criteria->compare('year(t.date_input)', $aDate[0]);
        
        $models = self::model()->findAll($criteria);
        if(count($models)){
            $sql='';
            $tableName = self::model()->tableName();
            foreach($models as $item){
                $has_update_balance = $item->has_update_balance; 
                $price = $price_binh_bo;
                $amount = $price*$item->amount_gas;
                
                // xử lý cập nhật công nợ kh, chỉ chạy 1 lần, vì hàm cập nhật này có thể chạy nhiều lần trong tháng
                if($item->only_gas_remain==1 && $has_update_balance==0){
                // only_gas_remain 0: cân gas dư có đơn hàng, 1: chỉ cân gas dư và không có đơn hàng KH
                // chỗ này cập nhật thêm 1 cột nữa là đã cộng vào phải thu kh ( tính công nợ ). vì cái update này dc gọi nhiều lần trong
                // 1 tháng, mình phải biết cái nào đã cập nhật vào phải thu KH rồi thì ko cập nhật nữa
                // 1: đã cập nhật công nợ, 0: chưa, dành cho những gas dư cân ko có bán hàng                    
                    $has_update_balance = 1;
                    self::UpdateReceivablesCustomerYear($item);
                }
                // xử lý cập nhật công nợ kh, chỉ chạy 1 lần, vì hàm cập nhật này có thể chạy nhiều lần trong tháng
                
                $sql .= "UPDATE $tableName SET "
                        . " `has_update_balance`=\"$has_update_balance\",  "
                        . " `price`=\"$price\",  "
                        . " `amount`=\"$amount\"  "
                        . "WHERE `id`=$item->id ;";
            }
            Yii::app()->db->createCommand($sql)->execute();
        }
    }
    
    /**
     * @Author: DungNT Jun 01, 2014
     * @Todo: trừ tiền gas dư vào phần phải thu KH theo năm
     * @Param: $mRemain model remain
     */    
    public static function UpdateReceivablesCustomerYear($mRemain){
        return 0; // Jun 18, 2014 - không quản lý công nợ KH nữa, để cho bên phần mềm kế toán làm việc này
        if($mRemain->only_gas_remain != 1) return; 
        // nếu là cân gas dư có bán hàng thì không xử lý và return
        $oldModel = self::model()->findByPk($mRemain->id);
        $receivables_customer = $mRemain->amount;
        $tmpDate = explode('-', $mRemain->date_input);
        $year = $tmpDate[0];
        $customer_id = $mRemain->customer_id;
        if($oldModel){ // nếu là cập nhật store card thì phải trừ đi số phải thu của record trước
            $receivables_customer -= $oldModel->amount;
        }
        GasClosingBalanceCustomer::cutReceivablesCustomer($year, $customer_id, $receivables_customer);        
    }
    
    protected function beforeDelete() {
        if($this->only_gas_remain==1){
            $receivables_customer = $this->amount;
            $tmpDate = explode('-', $this->date_input);
            $year = $tmpDate[0];
            $customer_id = $this->customer_id;
            $type_customer = $this->type_customer;// là loại KH 1: bò hay 2: mối, mới đổi lại laijMay 21, 2014
            GasClosingBalanceCustomer::addReceivablesCustomer($year, $customer_id, $type_customer, $receivables_customer);        
        }
        
        return parent::beforeDelete();
    }

    /**
     * @Author: DungNT May 23, 2014
     * @Todo: cộng tổng thành tiền bình bò theo customer_id và date_input, dùng để trừ tiền gas dư khi tạo mới thẻ kho
     * lấy gas dư trong ngày
     * @Param: $customer_id
     * @Param: $date_input 
     * @Param: $agent_id chỉ tính gas dư của đại lý đó cân thôi
     */
    public static function getAmountGasDuBinhBo($customer_id, $date_input, $agent_id){
        return 0;// Close function on sep 07, 2016 vì hiện tại ko dùng đến chức năng này
        if(empty($customer_id) || $customer_id<1) return 0;
        $criteria = new CDbCriteria();
        $criteria->compare("t.customer_id",$customer_id);
        $criteria->compare("t.date_input",$date_input);
        $criteria->compare("t.agent_id",$agent_id);
        $criteria->select = "sum(amount) as amount";
        $model = self::model()->find($criteria);
        return $model->amount;
    }
    
    // get array id=>model by list id
    public static function getByListId($aId, $needMore=array()){
        if(count($aId) < 1){
            return [];
        }
        $criteria = new CDbCriteria();
        $sParamsIn = implode(',', $aId);
        $criteria->addCondition("t.id IN ($sParamsIn)");
        $models = self::model()->findAll($criteria);
        if(isset($needMore['OnlyFindall'])){
            return $models;
        }
        $res = array();
        foreach($models as $item)
            $res[$item->id] = $item;
        return $res;
    }
    
    /**
     * @Author: DungNT Jun 22, 2014
     * @Todo: get list model gasremain by date range, mà chưa dc xuất cho lái xe
     * @Param: $date_from, $date_to
     * @Return: array model
     */    
    public static function getRemainNotExportByDateRange($date_from, $date_to){
        if(MyFormat::validDateInput($date_from, '-') && MyFormat::validDateInput($date_to, '-')){
            $criteria = new CDbCriteria();
            $date_from = MyFormat::dateDmyToYmdForAllIndexSearch($date_from);
            $date_to = MyFormat::dateDmyToYmdForAllIndexSearch($date_to);
            $criteria->addBetweenCondition("t.date_input",$date_from,$date_to);
            $criteria->compare("t.has_export", 0);// chưa dc xuất cho lái xe
            $criteria->compare("t.agent_id", MyFormat::getAgentId());            
            $criteria->order = 't.date_input ASC, t.id ASC';
            return  self::model()->findAll($criteria);
        }
        return array();
    }
    
    /**
     * @Author: DungNT Jun 22, 2014
     * @Todo: cập nhật trạng thái của has_export, khi xuất cho lái xe ( lần cân 1 ) thì lên 1, khi xóa lần cân 1 thì vè 0
     * @Param: $aId list id gasremain
     * @Param: $status 0 OR 1
     * @Remove from Sep 17, 2017 
     */        
    public static function UpdateHasExportRemove($aId, $status){
        if(count($aId)<1) return;
        $criteria = new CDbCriteria();
//        $criteria->addInCondition('id', $aId);
        $sParamsIn = implode(',', $aId);
        $criteria->addCondition("id IN ($sParamsIn)");
        $aValueUpdate = array('has_export'=>$status);
        self::model()->updateAll($aValueUpdate, $criteria);
    }
    
    public function getAgent() {
        $aAgent = CacheSession::getListAgent();
        return isset($aAgent[$this->agent_id]) ? $aAgent[$this->agent_id]['first_name'] : '';
    }
    public function getCustomer($field_name='') {
        $mUser = $this->customer;
        if($mUser){
            if(empty($field_name)){
                return $mUser->code_bussiness."-".$mUser->getFullName();
            }
            return $mUser->$field_name;
        }
        return '';
    }
    
    /** @Author: DungNT Jun 06, 2018
     *  @Todo: get customer of Agent khi chuyển về kho hoặc sang đại lý khác
     **/
    public function getCustomerOfAgent($field_name='') {
        if(empty($this->customer_id_of_agent)){
            return '';
        }
        $mUser = $this->rCustomerOfAgent;
        if($mUser){
            if(empty($field_name)){
                return $mUser->code_bussiness."-".$mUser->getFullName();
            }
            return $mUser->$field_name;
        }
        return '';
    }
    public function getTypeCustomer() {
        return isset(CmsFormatter::$CUSTOMER_BO_MOI[$this->type_customer]) ? CmsFormatter::$CUSTOMER_BO_MOI[$this->type_customer] : '';
    }
    public function getMaterials() {
        $aMaterial = CacheSession::getListMaterial();
        return isset($aMaterial[$this->materials_id]) ? $aMaterial[$this->materials_id] : '';
    }
    public function getDateInput() {
        return MyFormat::dateConverYmdToDmy($this->date_input);
    }
    public function getAmount($format = false) {
        if($format){
            return ActiveRecord::formatCurrency($this->amount);
        }
        return $this->amount;
    }
    public function getAmountWeb() {
        $res = $this->getAmount(true);
        $res .= "<span class='display_none amount'>$this->amount</span>";
        return $res;
    }
    
    public function getAmountEmpty() {
        return ActiveRecord::formatNumberInput($this->amount_empty)*1;
    }
    public function getAmountHasGas() {
        return ActiveRecord::formatNumberInput($this->amount_has_gas)*1;
    }
    public function getAmountHasGasApp() {
        $res = $this->amount_has_gas;
        if($this->gas_remain_type == GasRemain::APP_CREATE_THU_KHO){
            $res = $this->amount_empty + $this->amount_gas_2;
        }
        return ActiveRecord::formatNumberInput($res)*1;
    }
    public function getAmountGasApp() {
        $res = $this->amount_gas;
        if($this->gas_remain_type == GasRemain::APP_CREATE_THU_KHO){
            $res = $this->amount_gas_2;
        }
        return ActiveRecord::formatNumberInput($res)*1;
    }
    public function getAmountGas() {
        return ActiveRecord::formatNumberInput($this->amount_gas)*1;
    }
    public function getAmountGas2() {
        return ActiveRecord::formatNumberInput($this->amount_gas_2)*1;
    }
    public function getCreatedDate($fomat = 'd/m/Y H:i') {
        return MyFormat::dateConverYmdToDmy($this->created_date, 'd/m/Y H:i');
    }
    public function getCar() {
        $agentCar = !empty($this->agent_id_old) ? $this->agent_id_old : $this->agent_id;
        if(array_key_exists($agentCar, $this->getArrayAgentCar())){
            $aAgentCar = $this->getArrayAgentCar();
            return $aAgentCar[$agentCar];
        }
        
        $aData = CacheSession::getListdataByRole(ROLE_CAR);
        return isset($aData[$this->car_id]) ? $aData[$this->car_id] : '';
    }
    public function getDriver() {
        $aData = CacheSession::getListdataByRole(ROLE_DRIVER);
        return isset($aData[$this->driver_id]) ? $aData[$this->driver_id] : '';
    }
    public function getPhuXe1() {
        $aData = CacheSession::getListdataByRole(ROLE_PHU_XE);
        return isset($aData[$this->phu_xe_1]) ? $aData[$this->phu_xe_1] : '';
    }
    public function getPhuXe2() {
        $aData = CacheSession::getListdataByRole(ROLE_PHU_XE);
        return isset($aData[$this->phu_xe_2]) ? $aData[$this->phu_xe_2] : '';
    }
    public function getCodeNo() {
        return $this->code_no;
    }
    public function getUidLogin($field_name='') {
        $mUser = $this->rUidLogin;
        if($mUser){
            if(empty($field_name)){
                return $mUser->code_bussiness."-".$mUser->getFullName();
            }
            return $mUser->$field_name;
        }
        return '';
    }
    public function getAgentIdOld($field_name='') {
        $mUser = $this->rAgentIdOld;
        if($mUser){
            if(empty($field_name)){
                return $mUser->code_bussiness."-".$mUser->getFullName();
            }
            return $mUser->$field_name;
        }
        return '';
    }
    public function getUidLoginGrid() {
        $res = '';
        if(!empty($this->agent_id_old)){
            $res = $this->getAgentIdOld('first_name');
        }else{
            $res = $this->getUidLogin('first_name');            
        }
        return "<br><b>{$res}</b>";
    }

    /** @Author: DungNT Dec 20, 2017
     *  @Todo: get listdata car for web and app Gas Service
     *  @Param: $role_id for check app Gas Service
     **/
    public function getListdataCar($agent_id, $role_id, $needMore) {
        $aAgentCar = $this->getArrayAgentCar();
//        $aAgentCar = [];
        if(isset($needMore['AppGasSerice'])){
            if($role_id != ROLE_CRAFT_WAREHOUSE){
                $aCar = [];
            }else{
                $aCar = Users::getSelectByRoleForAgent($agent_id, ONE_AGENT_CAR, '', $needMore);
                $aCar = $aCar + $aAgentCar;
            }
            return HandleLabel::JsonIdName($aCar);
        }
        $aCar = Users::getSelectByRoleForAgent($agent_id, ONE_AGENT_CAR, '', $needMore);
        return $aCar + $aAgentCar;
    }
    
    /** @Author: DungNT Jul 24 2018
     *  @Todo: get listdata driver for web and app Gas Service
     *  @Param: $role_id for check app Gas Service
     * @param: $needMore array anything
     **/
    public function getListdataDriver($agent_id, $role_id, $needMore) {
        if(isset($needMore['AppGasSerice'])){
            if($role_id != ROLE_CRAFT_WAREHOUSE){
                $aCar = [];
            }else{
                $aCar = Users::getSelectByRoleForAgent($agent_id, ONE_AGENT_DRIVER, '', $needMore);
            }
            return HandleLabel::JsonIdName($aCar);
        }
        $aCar = Users::getSelectByRoleForAgent($agent_id, ONE_AGENT_DRIVER, '', $needMore);
        return $aCar;
    }
    
    
    /**
     * @Author: DungNT May 07, 2017
     * @Todo: update back lại vào model App Order
     */
    public function updateBackAppOrder() {
        return ; // Dec2617 có thể hàm này gây ra sai số tiền gas dư của đơn hàng 
        if($this->source == GasRemain::SOURCE_APP){
            return ; // Những gas dư của giao nhận tạo thì ko update back lại Order
        }
        $mAppOrder = new GasAppOrder();
        $mAppOrder->customer_id     = $this->customer_id;
        $mAppOrder->agent_id        = $this->agent_id;
        $mAppOrder->date_delivery   = $this->date_input;

        $mAppOrder->updateBackGasRemain($this);
    }
    
    public function getUidUpdateVinhLong() {
        return [214];// Nguyễn Thanh Tuấn -- Kho Vĩnh Long
    }
    
    /**
     * @Author: DungNT May 07, 2017
     * @Todo: check User can uupdate on web 
     */
    public function canUpdate()
    {
        $cRole          = MyFormat::getCurrentRoleId();
        $cUid           = MyFormat::getCurrentUid();
        $aRoleUpdate    = [ROLE_ADMIN, ROLE_CRAFT_WAREHOUSE];
        
        if($cRole == ROLE_CRAFT_WAREHOUSE && (!in_array($cUid, $this->getUidUpdateVinhLong()) || MyFormat::getAgentId() != $this->agent_id) ){
            return false;
        }
//        if( (in_array($cUid, GasConst::getArrUidUpdateAll()) && !in_array($this->agent_id, CmsFormatter::$LIST_WAREHOUSE_ACCOUNT)) 
        if( (in_array($cUid, GasConst::getArrUidUpdateAll())) 
                || (in_array($cUid, $this->getUidUpdateVinhLong()))){
            return GasConst::canUpdateAll($this->created_date);
        }

        // May 21, 2015 dùng chung ngày update gas dư với thẻ kho
        $dayAllow = date('Y-m-d');
        $dayAllow = MyFormat::modifyDays($dayAllow, Yii::app()->params['storecard_admin_update'], '-');
        if($cRole==ROLE_SUB_USER_AGENT ){
            if($cUid != $this->uid_login && $this->uid_login!=0){
                return false;
            }
            $dayAllow = date('Y-m-d');
            $dayAllow = MyFormat::modifyDays($dayAllow, GasCashBook::getAgentDaysAllowUpdate(), '-');
//            $dayAllow = MyFormat::modifyDays($dayAllow, Yii::app()->params['gas_remain_agent_update'], '-');
        }elseif(!in_array($cRole, $aRoleUpdate) ){
            return false;
        }
        return MyFormat::compareTwoDate($this->created_date, $dayAllow);
    }
    // format data before render view
    public function updateFormatData() {
        $this->scenario         = 'update';
        $this->date_input       = MyFormat::dateConverYmdToDmy($this->date_input);
        if($this->amount_gas_2 > 0){
            $this->amountHasGas2     = $this->amount_empty + $this->amount_gas_2;
        }
    }
    // handle update from web
    public function updateWeb() {
        if($this->seri == GasRemain::SERI_TDV){
            $this->updateWebSeriTDV();
            return ;
        }
        $this->date_input           = MyFormat::dateConverDmyToYmd($this->date_input);
        $this->amount_gas           = $this->amount_has_gas - $this->amount_empty;
        $this->last_update_by       = MyFormat::getCurrentUid();
        $this->last_update_time     = date('Y-m-d H:i:s');
        if($this->amountHasGas2 > 0){
            $this->amount_gas_2 = $this->amountHasGas2 - $this->amount_empty;
        }
        $this->status_employee_debit = GasRemain::EMPLOYEE_DEBIT_YES;
        if($this->has_export == GasRemain::STATUS_EXPORT){// add Mar1118 xử lý biến cộng công nợ cho PVKH + tài xế
            $this->status_employee_debit = GasRemain::EMPLOYEE_DEBIT_NO;
        }
        $this->calcDriverMissWeight();// Add Jun0918 bổ sung để update trên web thì cập nhật luôn tiền của gas dư 
        $this->update();
    }
    /** @Author: DungNT Jan 09, 2018
     *  @Todo: xử lý cập nhật xuất cho cân thiếu đầu vào
     **/
    public function updateWebSeriTDV() {
        $this->update(['has_export']);
    }
    
    /**
     * @Author: DungNT Oct 23, 2017
     * @Todo: check xem User có được quyền sửa remain không
     */
    public function appCanUpdate() {
//        return 1; // only dev test need close on live
        if(($this->source == GasRemain::SOURCE_HGD) || is_null($this->mAppUserLogin) || ($this->source == GasRemain::SOURCE_WEB && $this->mAppUserLogin->role_id != ROLE_CRAFT_WAREHOUSE) || $this->has_export == GasRemain::STATUS_EXPORT){
            return 0;
        }
        $days = 1;
        if($this->mAppUserLogin->role_id == ROLE_CRAFT_WAREHOUSE){
            $days = 5;
        }
        $date_check = MyFormat::modifyDays($this->date_input, $days);
        if( ($this->uid_login == $this->mAppUserLogin->id || $this->mAppUserLogin->role_id == ROLE_CRAFT_WAREHOUSE)
            && MyFormat::compareTwoDate($date_check, date('Y-m-d'))){
            return 1;
        }
        return 0;
    }
    
    /** @Author: DungNT Oct 30, 2017
     *  @Todo: check xem User có được quyền change trạng thái nhập xuất của gas dư không
     */
    public function appCanSwipe() {
        $res = 1;
        if($this->has_export == GasRemain::STATUS_EXPORT){
           $res = 0;
        }
        return $res;
    }
    
    /** @Author: DungNT Sep 16, 2017
     *  @Todo: get gas remain of agent, mà chưa xuất has_export = 0
     *  Get item wait export
     */
    public function getInventory() {
        $criteria = new CDbCriteria();
        $criteria->addCondition('t.has_export=' . GasRemain::STATUS_WAIT_EXPORT);// chờ xuất cho lái xe
        $criteria->addCondition('t.agent_id=' . $this->agent_id);
        if(!empty($this->driver_id)){
            $criteria->addCondition('t.driver_id=' . $this->driver_id);
        }
        $criteria->order = 't.date_input ASC, t.id ASC';
        return  self::model()->findAll($criteria);
    }
    
    /** @Author: DungNT Sep 16, 2017
     * @Todo: cập nhật trạng thái của has_export, khi xuất cho lái xe ( lần cân 1 ) thì lên 1, khi xóa lần cân 1 thì vè 0
     * @Param: $aId list id gasremain
     * @Param: $status 0 OR 1
     */        
    public function updateHasExport($aSeri, $status){
        if(count($aSeri)<1) return;
        $criteria = new CDbCriteria();
        $this->aSeri = $aSeri;
        $this->getSameConditionBySeri($criteria);
        $aValueUpdate = array('has_export'=>$status, 'status_employee_debit' => $this->status_employee_debit);
        self::model()->updateAll($aValueUpdate, $criteria);
    }
    
    /** @Author: DungNT Now 15, 2017
     *  @Todo: get record by array seri
     **/
    public function getSameConditionBySeri(&$criteria) {
        $criteria->addCondition('agent_id=' . $this->agent_id);
        $sParamsIn = implode(',', $this->aSeri);
        $criteria->addCondition("seri IN ($sParamsIn)");
    }
    
// May 20, 2014 DungNT: 
    public function getTextHasExport(){
        $res = "<span class='item_b hight_light'>".GasRemain::$TYPE_HAS_EXPORT[0]."</span>";
        if($this->has_export == GasRemain::STATUS_EXPORT){
            $res = GasRemain::$TYPE_HAS_EXPORT[$this->has_export];
        }elseif($this->has_export == GasRemain::STATUS_WAIT_EXPORT){
            $res = "<span class='item_b' style='color:#43A047;'>".GasRemain::$TYPE_HAS_EXPORT[GasRemain::STATUS_WAIT_EXPORT]."</span>";
        }
        return $res;
    }
    public function getHasExport(){
        return isset(GasRemain::$TYPE_HAS_EXPORT[$this->has_export]) ? GasRemain::$TYPE_HAS_EXPORT[$this->has_export] : '';
    }
   
   public function getAppInfoAmountGas() {
       $res = ' Dư '.$this->getAmountGas() . ' Kg - '. $this->getHasExport();
       if(!empty($this->user_update_2)){
           $res = ' Dư '.$this->getAmountGas() . ' Kg - Cân lần 2: ' . $this->getAmountGas2();
       }
       return $res;
   }
   public function getAppInfoWeight() {
       $amount = '';
       if($this->amount > 0){
           $amount = ' - TT '.ActiveRecord::formatCurrency($this->amount);
       }
       
       $res = 'KL vỏ: '.$this->getAmountEmpty().' - KL cân: '.$this->getAmountHasGas();
       if(!empty($this->user_update_2)){
           $amount_has_gas2 = ($this->amount_empty + $this->amount_gas_2)*1;
           $res = 'KL vỏ: '.$this->getAmountEmpty().' - DL cân: '.$this->getAmountHasGas().' - Kho: '.$amount_has_gas2;
       }
       return $res;
   }
   
   /** @Author: DungNT  Oct 23, 2017 -- handle listing api */
    public function handleApiList(&$result, $q) {
        // 1. get list order by user id
        $dataProvider   = $this->ApiListing($q);
        $models         = $dataProvider->data;
        $CPagination    = $dataProvider->pagination;
        $result['total_record'] = $CPagination->itemCount;
        $result['total_page']   = $CPagination->pageCount;
        
        $mAppCache = new AppCache();
//        $result['message']  = 'Tổng Gas Dư Test: 12345';
        $result['message']  = $this->getAppSummaryToday($q);
        $listdataMaterials = $mAppCache->getListdataMaterialByType(GasMaterialsType::$ARR_WINDOW_VO, AppCache::LISTDATA_MATERIAL_TYPE_VO);
        if( $q->page >= $CPagination->pageCount ){
            $result['record'] = array();
        }else{
            foreach($models as $model){
                $model->mAppUserLogin       = $this->mAppUserLogin;// for canPickCancel
                $aRes = [];
                $aRes['id']                 = $model->id;
                $aRes['seri']               = $model->seri;
                $aRes['date_input']         = $model->getDateInput();
                $aRes['has_export']         = $model->has_export;
                $aRes['amount_gas']         = $model->getAppInfoAmountGas();
                $aRes['weight_info']        = $model->getAppInfoWeight();
                $aRes['materials_name']     = isset($listdataMaterials[$model->materials_id]) ? $listdataMaterials[$model->materials_id] : '';
                $aRes['allow_update']       = $model->appCanUpdate();
                $aRes['allow_swipe']        = $model->appCanSwipe();
                $aRes['customer_name']      = $model->getCustomer('first_name');

                $result['record'][]         = $aRes;
            }
        }
    }
    
    /**
    * @Author: DungNT Oct 23, 2017
    * @Todo: get data listing 
    * @param: $q object post params from app client
    */
    public function ApiListing($q) {
        $criteria = new CDbCriteria();
        if(GasCheck::isServerLive()){
            $criteria->addCondition('t.id > 456536');// Jan1618 OK để giới hạn record giảm đc time của query không? bình thường đang hơn 2 second - sellId=448513-01/01/2018
        }
        if($this->mAppUserLogin->role_id == ROLE_DRIVER){
            $criteria->addCondition('(t.driver_id=' . $this->mAppUserLogin->id ." AND t.agent_id=$this->agent_id)");
        }elseif($this->mAppUserLogin->role_id == ROLE_CRAFT_WAREHOUSE){// // Mar3019 hide row chua can lan 2 tren list cua Thu Kho Binh Dinh - de khoi vuot nham
            $criteria->addCondition('t.agent_id='.$this->agent_id .' AND t.user_update_2 > 100');
        }elseif(in_array($this->mAppUserLogin->role_id, EmployeeCashbook::getRoleLikeEmployeeMaintain())){
            $criteria->addCondition('(t.uid_login=' . $this->mAppUserLogin->id ." AND t.agent_id=$this->agent_id)"
                    .' OR t.agent_id='.$this->agent_id);
        }else{
            $criteria->addCondition('t.uid_login=1');// không cho user nào xem nữa
        }
        $this->handleMoreCondition($q, $criteria);

        // Mar 28, 2017 Tạm Close lại 
        if($q->type == GasRemain::STATUS_WAIT_EXPORT){
            $sParamsIn = implode(',', $this->getArrayTypeNotExport());
            if(!empty($sParamsIn)){
                $criteria->addCondition("t.has_export IN ($sParamsIn)");
            }
        }elseif($q->type == GasRemain::STATUS_EXPORT){
            $criteria->addCondition('t.has_export=' . GasRemain::STATUS_EXPORT);
        }

        $criteria->order = 't.date_input DESC, t.id DESC';
        $dataProvider=new CActiveDataProvider($this, array(
            'criteria' => $criteria,
            'pagination'=>array(
                'pageSize' => 20,
                'currentPage' => (int)$q->page,
            ),
          ));
        return $dataProvider;
    }
    
    /** @Author: DungNT May 23, 2017
     * @Todo: xử lý add thêm điều kiện search apilisting
     */
    public function handleMoreCondition($q, &$criteria) {
        if(!empty($q->date_from)){
            $this->date_from_ymd = MyFormat::dateDmyToYmdForAllIndexSearch($q->date_from);
            $criteria->addCondition("t.date_input>='{$this->date_from_ymd}'");
        }
        if(!empty($q->date_to)){
            $this->date_to_ymd = MyFormat::dateDmyToYmdForAllIndexSearch($q->date_to);
            $criteria->addCondition("t.date_input<='{$this->date_to_ymd}'");
        }
        if(!empty($q->customer_id)){
            $criteria->addCondition('t.customer_id='.$q->customer_id);
        }
        if(!empty($q->car_id)){
            $this->handleAgentCar($criteria, $q->car_id);
        }
    }
    
    /** @Author: DungNT Jun 11, 2018
     *  @Todo: handle condition search agent xe tải
     **/
    public function handleAgentCar(&$criteria, $car_id) {
        if(array_key_exists($car_id, $this->getArrayAgentCar())){
            $criteria->addCondition('t.agent_id_old='.$car_id);
        }else{
            $criteria->addCondition('t.car_id='.$car_id);
        }
    }
    
    /**
     * @Author: DungNT Oct 24, 2017
     * @Todo: get param from app update 
     */
    public function getAppParams($q) {
        if($this->isNewRecord){
           return ; 
        }
        $mAppOrder = new GasAppOrder();
        // for update record
        if($this->gas_remain_type == GasRemain::APP_CREATE_THU_KHO){
            $this->amount_empty         = $mAppOrder->formatInputGasRemain($q->kg_empty);
            $q->kg_has_gas              = $mAppOrder->formatInputGasRemain($q->kg_has_gas);
            $this->amount_gas_2         = $q->kg_has_gas - $this->amount_empty;
            if(!empty($this->amount_empty) && $this->amount_empty > $q->kg_has_gas){
                throw new Exception('Trọng lượng vỏ phải nhỏ hơn trọng lượng cân, vui lòng kiểm tra lại');
            }
        }else{
            $this->seri                 = $q->seri;
            $this->amount_empty         = $mAppOrder->formatInputGasRemain($q->kg_empty);
            $this->amount_has_gas       = $mAppOrder->formatInputGasRemain($q->kg_has_gas);
            $this->amount_gas           = $this->amount_has_gas - $this->amount_empty;
            $this->materials_type_id    = $q->materials_type_id;
            $this->materials_id         = $q->materials_id;
        }
    }
    
    /** @Author: DungNT Oct 23, 2017
     *  @Todo: handle app post and validate
     */
    public function handlePost($q) {
        $this->gas_remain_type  = isset($q->gas_remain_type) ? $q->gas_remain_type : GasRemain::APP_CREATE_NORMAL;
        if($this->isNewRecord && $this->gas_remain_type == GasRemain::APP_CREATE_THU_KHO){
            // xử lý cho Thủ Kho cân lại gas dư lần 2 ở xưởng
            $this->handleReweighed($q);
            return ;
        }
        $this->customer_id = $q->customer_id;
        if($this->customer_id < 100){
            throw new Exception('Gas Remain 100 Yêu cầu không hợp lệ');
        }
        if($this->handleExportToOtherAgent($q)){
            return ;
        }
        $this->getAppParams($q);
        if($this->gas_remain_type == GasRemain::APP_CREATE_THU_KHO){
            return ;// không xử lý tiếp nữa, mà chỉ update 1 số thông tin cần thiết
        }
        if($this->isNewRecord){
            $this->uid_login    = $this->mAppUserLogin->id;
        }
        $this->customer_id      = $q->customer_id;
        $this->date_input       = $q->date_input;
        
        $this->validate();
        $this->convertDate();
        $this->appCheckDate();// sẽ mở nếu cần check
        $this->setCustomerInfo();
        $this->setAppPriceCustomer();
        if(!$this->isNewRecord){
            return ;
        }

        if(!isset($q->order_detail) || !is_array($q->order_detail) || count($q->order_detail) < 1 ){
            throw new Exception('Đơn hàng không hợp lệ. Vui lòng nhập đầy đủ thông tin vỏ');
        }
        $mAppOrder = new GasAppOrder();
        
        foreach($q->order_detail as $objDetail){
            $mDetail = new GasRemain();
            $mDetail->source            = GasRemain::SOURCE_APP;
            $mDetail->date_input        = $this->date_input;
            $mDetail->agent_id          = $this->agent_id;
            $mDetail->customer_id       = $this->customer_id;
            $mDetail->customer_parent_id    = $this->customer_parent_id;
            $mDetail->sale_id           = $this->sale_id;
            $mDetail->type_customer     = $this->type_customer;
            $mDetail->uid_login         = $this->uid_login;
            $mDetail->materials_type_id = $objDetail->materials_type_id;
            $mDetail->materials_id      = $objDetail->materials_id;
            $mDetail->price             = $this->price;
            $mDetail->seri              = trim($objDetail->seri);
            $mDetail->amount_empty      = $mAppOrder->formatInputGasRemain($objDetail->kg_empty);
            $mDetail->amount_has_gas    = $mAppOrder->formatInputGasRemain($objDetail->kg_has_gas);
            $mDetail->amount_gas        = $mDetail->amount_has_gas - $mDetail->amount_empty;
            $mDetail->amount            = $mDetail->price * $mDetail->amount_gas;

            if(!empty($mDetail->amount_empty) && $mDetail->amount_empty > $mDetail->amount_has_gas){
                throw new Exception('Trọng lượng vỏ phải nhỏ hơn trọng lượng cân, vui lòng kiểm tra lại');
            }
            
            if(empty($mDetail->seri)){
                throw new Exception('Seri không được trống, vui lòng kiểm tra lại');
            }
            $this->aDetail[]            = $mDetail;
        }
    }
    
    /** @Author: DungNT Mar 11, 2018
     *  @Todo: get price of customer in month
     **/
    public function setAppPriceCustomer() {
        $temp = explode('-', $this->date_input);
        if(!isset($temp[1]) || empty($temp[1])){
            $temp[1] = date('m');
            $temp[0] = date('Y');
        }
        $mUsersPrice =new UsersPrice();
        $aInfoPrice = $mUsersPrice->getPriceOfCustomer($temp[1], $temp[0], $this->customer_id);
        $this->price = $aInfoPrice[UsersPrice::PRICE_B_50];
        if(empty($this->price)){
            $this->price = $aInfoPrice[UsersPrice::PRICE_B_12];
            if($aInfoPrice[UsersPrice::PRICE_B_12] > UsersPrice::PRICE_MAX_BINH_BO){
                $this->price = round($aInfoPrice[UsersPrice::PRICE_B_12]/12);
            }
        }
    }
    
    /**
     * @Author: DungNT Mar 12, 2017
     * @Todo: check date valid
     */
    public function appCheckDate() {
        $dayAllow = date('Y-m-d');
        $dayAllow = MyFormat::modifyDays($dayAllow, 1, '-');
//        $date_check = MyFormat::dateConverDmyToYmd($this->date_input);
        if(!MyFormat::compareTwoDate($this->date_input, $dayAllow)){
            $this->addError('date_input', 'Ngày '.$this->date_input.' đã bị khóa, không thể nhập dữ liệu');
        }
    }
    public function convertDate() {
        $this->date_input =  MyFormat::dateDmyToYmdForAllIndexSearch($this->date_input);
        MyFormat::isValidDate($this->date_input);
    }
    
    public function setCustomerInfo() {// Mar2318 không dùng rCustomer khi form update cho change customer_id
//        $mCustomer = $this->customer;// Mar2318 Xem lại dùng kiểu này, vì khi update nếu đổi customer_id thì rCustomer là của customer cũ, sẽ bị sai khi set customer info 
        $mCustomer = Users::model()->findByPk($this->customer_id);
        //là loại KH 1: bò hay 2: mối, 3: HỘ GIA ĐÌNH,4:KH khác (chưa xử lý lưu số 4 này), 5: KH LÀ ĐẠI LÝ KHÁC TRONG HỆ THỐNG 
        if($mCustomer){
            $this->type_customer        = $mCustomer->is_maintain;
            $this->customer_parent_id   = $mCustomer->parent_id;
            $this->sale_id              = $mCustomer->sale_id;
        }
    }
    
    /** @Author: DungNT Oct 24, 2017
     * @Todo: handle save request from App
     */
    public function handleSaveCreate($q) {
        if($this->gas_remain_type == GasRemain::APP_CREATE_THU_KHO || $this->gas_remain_type == GasRemain::APP_EXPORT_TO_AGENT){
            return ;// thủ kho tạo lưu kiểu multi rồi, nên return 
            // PVKH xuất gas dư cho agent khác cũng không xử lý nữa
        }

        if(count($this->aDetail) < 1){
            throw new Exception('Detail Đơn hàng không hợp lệ. Vui lòng nhập đầy đủ thông tin vỏ');
        }
        foreach($this->aDetail as $model){
            $model->save();
        }
    }
    public function handleSaveUpdate($q) {
        if($this->gas_remain_type == GasRemain::APP_CREATE_THU_KHO){
            $this->calcDriverMissWeight();
        }
        $this->update();
    }
    
    /** @Author: DungNT Now 15, 2017
     *  @Todo: xử lý post app cân lại của thủ KHO
     **/
    public function handleReweighed($q) {
        foreach($q->order_detail as $item){
            $item->seri              = trim($item->seri);
            if(empty($item->seri)){
                throw new Exception('Seri không được trống, vui lòng kiểm tra lại');
            }
            $keySeri                    = $item->materials_id.'-'.$item->seri;
            $this->aSeri[]              = $item->seri;
            $this->aDataApp[$keySeri]   = $item;
        }
        
        $setExport = true;// Jan0918 kho Phước Tân và Bến Cát thì cân lần 2 xong đưa trạng thái đã xuất luôn
        if(in_array($this->agent_id, $this->getAgentKho())){
            $setExport = false;
        }
        
        $mAppOrder  = new GasAppOrder();
        $aOldModels = $this->getRecordToReweighed();
        foreach($aOldModels as $item){
            $keySeri                    = $item->materials_id.'-'.$item->seri;
            if(!isset($this->aDataApp[$keySeri])){
                continue ;
            }
            $dataApp                    = $this->aDataApp[$keySeri];
            $item->scenario             = 'AppReweighed';
            if($setExport){
                $item->has_export       = GasRemain::STATUS_EXPORT;
            }
            $item->amount_empty         = $dataApp->kg_empty;
            $item->amount_gas_2         = $mAppOrder->formatInputGasRemain($dataApp->kg_has_gas) - $item->amount_empty;
            $item->user_update_2        = $this->mAppUserLogin->id;
            $item->calcDriverMissWeight();
            if(!empty($item->amount_empty) && $item->amount_empty > $dataApp->kg_has_gas){
                throw new Exception('Trọng lượng vỏ phải nhỏ hơn trọng lượng cân, vui lòng kiểm tra lại');
            }
            $item->update();
            $this->buildDataStock($item);
        }
        $mStock = new Stock();
        $mStock->stockBack($this->aDataStock);
    }
    
    /** @Author: DungNT Aug 03, 2018
     *  @Todo: build array insert to stock, thêm vào lịch sử quản lý STT vỏ về KH
     *  find ngược lại bên stock với status = 1 (xuât đi), nếu map seri+customer thì chuyển trạng thái là vỏ về = 2 ok
     * nếu sai thông tin thì add new row vỏ về sai status = 3
     **/
    public function buildDataStock($mRemain) {
        $created_by         = $this->mAppUserLogin->id;
        $customer_id        = !empty($mRemain->customer_id_of_agent) ? $mRemain->customer_id_of_agent : $mRemain->customer_id;
        $mStock = new Stock();
        $mStock->type_store_card    = TYPE_STORE_CARD_IMPORT;
        $mStock->agent_id           = $mRemain->agent_id;
        $mStock->seri               = $mRemain->seri;
        $mStock->seri_real          = $mRemain->seri;
        $mStock->materials_id       = $mRemain->materials_id;
        $mStock->materials_type_id  = $mRemain->materials_type_id;
        $mStock->driver_id          = $mRemain->driver_id;
        $mStock->car_id             = $mRemain->car_id;
        $mStock->customer_id        = $customer_id;
        $mStock->created_date_only  = date('Y-m-d');
        $mStock->created_by         = $created_by;
        
        $this->aDataStock[] = $mStock;
    }
    
    /** @Author: DungNT Now 15, 2017
     *  @Todo: get old record to update cân lần 2
     **/
    public function getRecordToReweighed() {
        if(count($this->aSeri) < 1){
            throw new Exception('Dữ liệu không hợp lệ, vui lòng kiểm tra lại');
        }
        $criteria = new CDbCriteria();
        $this->getSameConditionBySeri($criteria);
        $criteria->addCondition('t.user_update_2 IS NULL OR t.user_update_2=0');
        if(!empty($this->car_id)){// Jun2018v fix duplicate seri update bị sai khi cân lần 2
            $this->handleAgentCar($criteria, $this->car_id);
        }
        return GasRemain::model()->findAll($criteria);
    }
    
    /** @Author: DungNT Oct 24, 2017
     *  format record thu chi của giao nhận view from app customer BoMoi
     */
    public function appView() {
        $mAppCache                  = new AppCache();
        $listdataMaterials          = $mAppCache->getListdataMaterialByType(GasMaterialsType::$ARR_WINDOW_VO, AppCache::LISTDATA_MATERIAL_TYPE_VO);
        $aRes                       = [];// order_view, order_edit
        $aRes['id']                 = $this->id;
        $aRes['seri']               = $this->seri;
        $aRes['date_input']         = $this->getDateInput();
        $aRes['amount_empty']       = $this->getAmountEmpty();
        $aRes['amount_has_gas']     = $this->getAmountHasGasApp();
        $aRes['amount_gas']         = $this->getAmountGasApp();
        $aRes['customer_name']      = $this->getCustomer();
        $aRes['customer_address']   = $this->getCustomer('address');
        $aRes['customer_phone']     = $this->getCustomer('phone');
        $aRes['customer_id']        = $this->customer_id;
        $aRes['materials_id']       = $this->materials_id;
        $aRes['materials_type_id']  = $this->materials_type_id;
        $aRes['materials_name']     = isset($listdataMaterials[$this->materials_id]) ? $listdataMaterials[$this->materials_id] : '';
        
        $aRes['allow_update']       = $this->appCanUpdate();

        return $aRes;
    }
    
    /**
     * @Author: DungNT Oct 24, 2017
     * @Todo: get model Gas remain, những giao nhận của đl đó sẽ được view
     */
    public function getModelApp() {
        if(empty($this->id)){
            return null;
        }
        $criteria = new CDbCriteria();
        $criteria->addCondition('t.id='.$this->id );
        return self::model()->find($criteria);
    }
    
    /** @Author: DungNT Feb 10, 2018
     *  @Todo: get cac agent Kho set gas du tren app tu Con Ton sang Da Xuat
     **/
    public function getAgentKho() {
        return [
            GasCheck::DL_KHO_VINH_LONG,
            GasCheck::KHO_DAU_KHI_BINH_DINH,
        ];
    }
    
    /**
     * @Author: DungNT Oct 24, 2017
     * @Todo: app set flag wait export or not
     */
    public function appSetExport() {
        if(in_array($this->agent_id, $this->getAgentKho())){
            $this->setExportKhoVinhLong();
            return ;
        }
        if($this->has_export == GasRemain::STATUS_EXPORT){
            return ;
        }
        if($this->has_export == GasRemain::STATUS_NOT_EXPORT){
            $this->has_export = GasRemain::STATUS_WAIT_EXPORT;
        }elseif($this->has_export == GasRemain::STATUS_WAIT_EXPORT){
            $this->has_export = GasRemain::STATUS_NOT_EXPORT;
        }
        $this->update(['has_export']);
    }
    
    /** @Author: DungNT Dec 27, 2017
     *  @Todo: Handle for Kho Vĩnh Long
     **/
    public function setExportKhoVinhLong() {
        if($this->has_export == GasRemain::STATUS_NOT_EXPORT){
            $this->has_export = GasRemain::STATUS_EXPORT;
        }elseif($this->has_export == GasRemain::STATUS_EXPORT){
            $this->has_export = GasRemain::STATUS_NOT_EXPORT;
        }
        $this->update(['has_export']);
    }

    public function getSource() {
        $res = 'ĐHBM';
        if($this->source == GasRemain::SOURCE_APP){
            $res = 'App';
        }
        return $res;
    }
    
    /** @Author: DungNT Now 15, 2017
     *  @Todo: xác định form tạo gas dư trên app cho loại user Giao Nhận và Thủ Kho
     **/
    public function getAppCreateType($mUser) {
        $res = GasRemain::APP_CREATE_NORMAL;
        if($mUser->role_id == ROLE_CRAFT_WAREHOUSE){
            $res = GasRemain::APP_CREATE_THU_KHO;
        }
        return $res;
    }
    
    /** @Author: DungNT Now 15, 2017
    *  @Todo: flow tạo gas dư của Thủ Kho
    * 1. thêm cờ xác định form tạo gas dư của Thủ Kho ở updateConfig: gas_remain_type
    * 2. Trả lại mảng seri chưa cân lần 2 của KHo PT và KHo Bến Cát
    * 3. Khi tạo cân lần 2 thì set biến user_update_2 
    * 4. Ngoài list sẽ load những gas đã cân lần 2
    * 5. xử lý cập nhật item cân lần 2 từ list
    **/
    public function getJsonSeriApp() {
        $models         = $this->getDataSeriApp();
        $mAppCache      = new AppCache();
        $aMaterials     = $mAppCache->getListdata('GasMaterials', AppCache::LISTDATA_MATERIAL);
        $aRes = [];
        foreach($models as $mGasRemain){
            $aRes[] = $mGasRemain->formatItemJson($aMaterials);
        }
        return $aRes;
    }
    
    /** @Author: DungNT Jun 05, 2018
     *  @Todo: format item gas dư trả xuống view tạo gas dư của THủ Kho + PVKH khi xuất gas dư cho ĐL khác
     *  @param: $aMaterials array listData materials
     **/
    public function formatItemJson($aMaterials) {
        $tmp = [];
        $materials_name             = isset($aMaterials[$this->materials_id]) ? $aMaterials[$this->materials_id] : '';
        $tmp['label']               = $this->seri . ' - ' .$materials_name;
        $tmp['materials_name']      = $materials_name;
        $tmp['materials_id']        = $this->materials_id;
        $tmp['materials_type_id']   = $this->materials_type_id;
        $tmp['seri']                = $this->seri;
        $tmp['kg_empty']            = $this->getAmountEmpty();
        $tmp['kg_has_gas_old']      = $this->getAmountHasGas();
        $tmp['kg_has_gas']          = '';
        if($this->gas_remain_type == GasRemain::APP_EXPORT_TO_AGENT){
            $tmp['kg_has_gas']      = $this->getAmountHasGas();
        }
        $tmp['id']                  = $this->id;
        return $tmp;
    }

    /** @Author: DungNT Now 15, 2017
     *  @Todo: get data chưa cân lần 2 để đưa xuống app
     **/
    public function getDataSeriApp() {
        $criteria = new CDbCriteria();
        if(!empty($this->car_id)){
            $this->routeConditionCar($criteria);
        }
        $criteria->addCondition('t.agent_id=' . $this->agent_id);
        $criteria->addCondition("(t.user_update_2 IS NULL OR t.user_update_2=0) AND t.seri > 0 AND t.date_input >= '2017-12-20'");
        $criteria->addCondition('t.has_export=' . GasRemain::STATUS_NOT_EXPORT);// Add Jun0518, chỉ lấy item còn tồn đưa xuống thủ kho Cân
        $criteria->limit = 2000;
//        $criteria->limit = 5;
        return self::model()->findAll($criteria);
    }
    
    /** @Author: DungNT Jun 08, 2018 
     *  @Todo: xử lý car_id put lên là Agent hay Car 
     **/
    public function routeConditionCar(&$criteria) {
        $mUser = Users::model()->findByPk($this->car_id);
        if(empty($mUser)){
            return ;
        }
        if($mUser->role_id == ROLE_CAR){
            $criteria->addCondition('t.car_id=' . $this->car_id);
        }elseif($mUser->role_id == ROLE_AGENT){
            $criteria->addCondition('t.agent_id_old=' . $this->car_id);
        }
    }
    
    
    /** @Author: DungNT Jun 05, 2018
    *  @Todo: flow Xuất gas dư cho đại lý khác của PVKH
    * 1. 1/ khi select Agent hoặc Customer dưới app thì gửi request lên lấy list gas dư chờ xuất
        - Nếu là xuất cho Agent thì sẽ trả về list gas dư, còn là customer thì ko trả gì hết - trả về có id
    2. khi submit lên, nếu là xuất cho agent thì chuyển những gas dư đó sang trạng thái đã xuất
    2.1 đồng thời nhập gas dư đó cho đại lý kia - trạng thái còn tồn 
    **/
    public function getJsonItemWaitExport() {
        $mAppCache      = new AppCache();
        $models         = $this->getInventory();
        $aMaterials     = $mAppCache->getListdata('GasMaterials', AppCache::LISTDATA_MATERIAL);
        $aRes = [];
        foreach($models as $mGasRemain){
            $mGasRemain->gas_remain_type = $this->gas_remain_type;
            $aRes[] = $mGasRemain->formatItemJson($aMaterials);
        }
        return $aRes;
    }
    
    /** @Author: DungNT Jun 07, 2018
     *  @Todo: get listdata Employee of Agent
     **/
    public function getListdataUserOfAgent() {
        $mAppOrder              = new GasAppOrder();
        $mAppOrder->agent_id    = $this->mCustomer->id;
        if($this->mCustomer->id == GasCheck::KHO_PHUOC_TAN){
            $listData = [862901 => 'Nguyễn Đình Quảng'];
        }else{
            $aUid = $mAppOrder->getListUserIdEmployeeMaintain();
            $listData = Users::getListOptions($aUid);
        }

        $aEmpty = [-1 => 'Chọn PVKH của đại lý'];
        return $aEmpty + $listData;
    }
    
    /** @Author: DungNT Dec 26, 2017
     *  @Todo: làm thống kê KL xe tải và KL của thủ kho cân lại trên list app thủ kho
     **/
    public function getAppSummaryToday($q) {
        $aRolePvkh = [ROLE_EMPLOYEE_MAINTAIN, ROLE_EMPLOYEE_MARKET_DEVELOPMENT, ROLE_DRIVER];
        if(in_array($this->mAppUserLogin->role_id, $aRolePvkh)){
            return $this->getSummaryDebit();
        }
        if(!in_array($this->agent_id, GasCheck::getAgentNotGentAuto())){
            return '';
        }
        $today = $this->date_to_ymd;
        $criteria = new CDbCriteria();
        $criteria->addCondition('t.agent_id=' . $this->agent_id);
        if(!empty($q->customer_id)){
            $criteria->addCondition('t.customer_id=' . $q->customer_id);
        }
        if(!empty($q->car_id)){
            $criteria->addCondition('t.car_id=' . $q->car_id);
        }
        $criteria->addCondition("t.seri<>-1 AND t.date_input = '$today'");
        $criteria->select = 'sum(t.amount_gas) as amount_gas, sum(t.amount_gas_2) as amount_gas_2';
        $model = GasRemain::model()->find($criteria);
        
        $amount_gas     = ActiveRecord::formatCurrency($model->amount_gas);
        $amount_gas_2   = ActiveRecord::formatCurrency($model->amount_gas_2);
        return "Xe tải: $amount_gas - Kho: $amount_gas_2";
    }
    
    
    /** @Author: DungNT Mar 11, 2018
     *  @Todo: thống kê số tiền nợ gas dư của PVKH
     *  @PVKH: 1. số tiền sum amount với status_employee_debit = 1
     *         2. số tiền pvkh_miss_money với  uid_login = $this->mAppUserLogin->id
     * @Driver: 1. số tiền sum amount với status_employee_debit = 1
     *         2. số tiền pvkh_miss_money với  driver_id = $this->mAppUserLogin->id
     **/
    public function getSummaryDebit() {
        $total = $this->getSummaryDebitYes() + $this->getSummaryGasMiss();
        return 'Công nợ gas dư: '. ActiveRecord::formatCurrency($total);
    }
    
    /** @Author: DungNT Mar 11, 2018
     *  @Todo: thống kê số tiền nợ gas dư của PVKH
     *  @PVKH: 1. số tiền sum amount với status_employee_debit = 1
     *  @Driver: 1. số tiền sum amount với status_employee_debit = 1
     **/
    public function getSummaryDebitYes() {
        $criteria = new CDbCriteria();
        $criteria->addCondition('t.status_employee_debit=' . GasRemain::EMPLOYEE_DEBIT_YES);
        $criteria->addCondition("t.seri<>-1");
        
        if($this->mAppUserLogin->role_id == ROLE_EMPLOYEE_MAINTAIN){
            $criteria->addCondition('t.uid_login=' . $this->mAppUserLogin->id);
            $criteria->addCondition('t.user_update_2 < 1');// Nếu là xe tải 1 xuất cho Kho PT thì phải xóa uid_login đi, vì có thể nó sẽ làm sai công nợ gas dư của NV, vì 1 NV có thể ở nhiều đại lý
        }else{
            $criteria->addCondition('t.driver_id=' . $this->mAppUserLogin->id);
            $criteria->addCondition('t.user_update_2 < 1');
            $criteria->addCondition('t.agent_id=' . $this->agent_id);
        }
        $criteria->select = 'sum(t.amount) as amount';
        $model = GasRemain::model()->find($criteria);
        return $model->amount;
    }
    // 2. số tiền pvkh_miss_money với  uid_login = $this->mAppUserLogin->id
    // 2. số tiền driver_miss_money với  driver_id = $this->mAppUserLogin->id
    public function getSummaryGasMiss() {
        $criteria = new CDbCriteria();
        if($this->mAppUserLogin->role_id == ROLE_EMPLOYEE_MAINTAIN){
            $criteria->addCondition('t.uid_login=' . $this->mAppUserLogin->id);
            $criteria->select = 'sum(t.pvkh_miss_money) as amount';
        }else{
            $criteria->addCondition('t.driver_id=' . $this->mAppUserLogin->id);
            $criteria->select = 'sum(t.driver_miss_money) as amount';
        }
        $model = GasRemain::model()->find($criteria);
        return $model->amount;
    }
    
    /** @Author: DungNT Feb 23, 2018
     *  @Todo: Auto insert gas dư B12 from App HGD
     *  1. nếu chưa có thì add new
     *  2. find nếu tồn tại rồi thì overide some field. Trong trường hợp đã xuất bình đi rồi thì có cho sửa không ?
     **/
    public function addGasRemainHgd() {
        // không có gas dư hoặc seri thì sẽ xóa luôn record đi
        if($this->mSell->status == Sell::STATUS_NEW){
            return ;
        }
        $needMore['delete'] = 1;
        $this->loadHgdOld($needMore);// chỉ xử lý 1 vỏ có gas dư của 1 đơn hàng, nếu 2 vỏ thì sẽ sai
        if(!$this->hgdCanAdd()){
            return ;
        }
        $this->hgdAdd();
    }
    
    /** @Author: DungNT Feb 23, 2018
     *  @Todo: check dữ liệu có hợp lệ để add không
     *  @use: sử dụng ở model Sell (checkUpdateGasRemain) và Model GasRemain - không sửa hàm khi chưa review
     **/
    public function loadHgdOld($needMore = []) {
        if($this->mSell->employeeCompleteApp){
           return ; // Feb2318 xử lý không find model cũ khi add Gas Remain từ PVKH hoàn thành App
        }
        $criteria = new CDbCriteria();
        $criteria->addCondition('t.sell_id=' . $this->mSell->id);
        $mGasRemain = GasRemain::model()->find($criteria);
        if(!empty($mGasRemain)){
            $this->mRemainOld = clone $mGasRemain;
            if($mGasRemain->has_export == GasRemain::STATUS_EXPORT){
                throw new Exception('Gas dư hộ gia đình này đã xuất, không thể cập nhật');
            }
            if(isset($needMore['delete'])){
                $mGasRemain->delete();
            }
        }
    }
    
    /** @Author: DungNT Feb 23, 2018
     *  @Todo: check dữ liệu có hợp lệ để add không
     *  không có gas dư hoặc seri thì không add
     **/
    public function hgdCanAdd() {
        if($this->mSell->gas_remain_amount < 1){
            return false;
        }
        if($this->mSell->status == Sell::STATUS_CANCEL){
            return false;
        }
        // load model detail để tìm model vỏ xử lý add
        $aSellDetail = SellDetail::getByRootId($this->mSell->id);
        foreach($aSellDetail as $item){
            if($item->materials_type_id == GasMaterialsType::MATERIAL_VO_12 && $item->gas_remain > 0 && !empty($item->seri)){
                $this->mSellDetailVo = $item;
            }
            if($item->materials_type_id == GasMaterialsType::MATERIAL_BINH_12KG){
                $this->mSellDetailGas = $item;
            }
        }
        if(empty($this->mSellDetailVo)){
            return false;
        }
        return true;
    }
    
    /** @Author: DungNT Feb 23, 2018
     *  @Todo: add new record gas dư when hgd save
     **/
    public function hgdAdd() {
        $this->deleteBySellId($this->mSell);
        $mSellDetailVo = $this->mSellDetailVo;
        $model = new GasRemain();
        $model->source          = GasRemain::SOURCE_HGD;
        $model->date_input      = $mSellDetailVo->created_date_only;
        $model->customer_id     = $mSellDetailVo->customer_id;
        $model->sale_id         = $mSellDetailVo->sale_id;
        $model->type_customer   = $mSellDetailVo->type_customer;
        $model->agent_id        = $mSellDetailVo->agent_id;
        $model->materials_id    = $mSellDetailVo->materials_id;
        $model->materials_type_id = $mSellDetailVo->materials_type_id;
        $model->seri            = $mSellDetailVo->seri;
        $model->price           = round($this->mSellDetailGas->price_root / 12);
        $model->amount          = $mSellDetailVo->gas_remain * $model->price;
        $model->amount_empty    = $mSellDetailVo->kg_empty;
        $model->amount_has_gas  = $mSellDetailVo->kg_has_gas;
        $model->amount_gas      = $mSellDetailVo->gas_remain;
        $model->uid_login       = $mSellDetailVo->employee_maintain_id;
        $model->has_export      = GasRemain::STATUS_WAIT_EXPORT;
        $model->sell_id         = $mSellDetailVo->sell_id;
        $model->created_date    = date('Y-m-d H:i:s');
        $model->save();
    }
    
    /** @Author: DungNT Mar 07, 2018
     *  @Todo: remove Gas remain HGD by sell ID
     **/
    public function deleteBySellId($mSell) {
        if(empty($mSell->gas_remain_amount)){
            return ;
        }
        $criteria = new CDbCriteria();
        $criteria->addCondition("t.sell_id=$mSell->id");
        $models = GasRemain::model()->findAll($criteria);
        foreach($models as $item){
            $item->delete();
        }
    }
    
    /** @Author: DungNT Mar 09, 2018
     *  @Todo: mô tả các field mới ở table gasRemain
     * @param: weight_from_customer: trọng lượng gas + vỏ cân lại ở KH, sẽ đc lưu lại khi xuất từ đại lý cho xe tải
     * @param: pvkh_miss_kg: số kg PVKH thiếu khi xuất cho xe tải
     * @param: pvkh_miss_money: số kg PVKH thiếu quy ra tiền
     * @param: driver_miss_kg: số kg tài xế thiếu khi cân lại ở Kho Phước Tân
     * @param: driver_miss_money: số kg tài xế thiếu quy ra tiền
     *  ALTER TABLE  `gas_gas_remain` ADD  `weight_from_customer` DECIMAL( 5, 2 ) NOT NULL COMMENT 'KL cân ở KH',
        ADD  `pvkh_miss_kg` DECIMAL( 5, 2 ) NOT NULL ,
        ADD  `pvkh_miss_money` MEDIUMINT( 6 ) NOT NULL ,
        ADD  `driver_miss_kg` DECIMAL( 5, 2 ) NOT NULL ,
        ADD  `driver_miss_money` MEDIUMINT( 6 ) NOT NULL ;
     **/
    public function calcPvkhMissWeight() {
        $this->pvkh_miss_kg     = 0.00;
        $this->pvkh_miss_money  = 0;
        $pvkhWeight     = $this->weight_from_customer - $this->amount_empty;
        $driverWeight   = $this->amount_has_gas - $this->amount_empty;
        $missKg = $driverWeight - $pvkhWeight;
        if($missKg < 0 ){// bi mất gas khi từ đại lý xuất cho xe tải => tính tiền cho PVKH
            $missKg = $missKg * -1;
            if($missKg > GasRemain::AMOUNT_ALARM){
                $this->pvkh_miss_kg     = $missKg;
                $this->pvkh_miss_money  = $missKg * $this->price;
            }
        }// ở đây chỉ theo dõi lượng gas bị thiếu, gas dư hơn thì chưa theo dõi, nên không tính trường hợp > 0 nữa
    }
    /**  @Mar0918 tính gas dư thiếu nếu có của Driver.
     * 1. áp dụng khi thủ kho cân lần 2 ở xưởng
     * 2. áp dụng khi thủ kho update cân lần 2 ở xưởng
     */
    public function calcDriverMissWeight() {
        if(empty($this->user_update_2)){
            return ;
        }
        $field_kg       = 'driver_miss_kg';
        $field_money    = 'driver_miss_money';
        if(array_key_exists($this->agent_id_old, $this->getArrayAgentCar())){
            $field_kg       = 'pvkh_miss_kg';// Jun0818 xử lý cho Xe tải lưu động trả gas dư về PTân
            $field_money    = 'pvkh_miss_money';
        }
        $this->{$field_kg}      = 0.00;
        $this->{$field_money}   = 0;
        
        $missKg     = $this->amount_gas_2 - $this->amount_gas;
        if($missKg < 0 ){// bi mất gas khi từ đại lý xuất cho xe tải => tính tiền cho PVKH
            $missKg = $missKg * -1;
            if($missKg > GasRemain::AMOUNT_ALARM){
                $this->{$field_kg}     = $missKg;
                $this->{$field_money}  = $missKg * $this->price;
            }
        }// ở đây chỉ theo dõi lượng gas bị thiếu, gas dư hơn thì chưa theo dõi, nên không tính trường hợp > 0 nữa
//        $this->has_export = GasRemain::STATUS_EXPORT;// Mar1018 không tự động xuất nữa, tách riêng field để tính công nợ không dựa vào has_export
        $this->status_employee_debit = GasRemain::EMPLOYEE_DEBIT_NO;// Mar1118 tách riêng field để tính công nợ, khi cân lần 2 rồi thì không tính công nợ cho tài xế
    }
    
    /** @Author: DungNT Mar 09, 2018
     *  @Todo: hủy cân lại lần 2 trên web
     **/
    public function cancelReweighed() {
        $attUpdate = ['driver_miss_kg', 'driver_miss_money', 'status_employee_debit'];
        $this->driver_miss_kg           = 0.00;
        $this->driver_miss_money        = 0;
        $this->status_employee_debit    = GasRemain::EMPLOYEE_DEBIT_YES;
        
        if($_GET['type']==2){
            $this->user_update_2 = null;
            $this->date_update_2 = null;
            $this->amount_gas_2 = null;
            $attUpdate[]='user_update_2';
            $attUpdate[]='date_update_2';
            $attUpdate[]='amount_gas_2';
        }else{
            $this->user_update_3 = null;
            $this->date_update_3 = null;
            $this->amount_gas_3 = null;
            $attUpdate[]='user_update_3';
            $attUpdate[]='date_update_3';
            $attUpdate[]='amount_gas_3';
        }
        $this->update($attUpdate);
    }
 
    /** @Author: DungNT Jun 05, 2018
     *  @Todo: xử lý chặn ko cho xuất vào Kho Phước Tân hoặc 1 Agent nào khác
     **/
    public function getAgentLockExport() {
        return [$this->agent_id,// không cho xuất chính đại lý hiện tại của Giao nhận
            GasCheck::KHO_PHUOC_TAN,
        ];
    }
    /** @Author: DungNT Jun 06, 2018
     *  @Todo: xử lý chỉ cho phép vào Xe Tải 1 và Bình Thạnh 1, Xe tai 1 xuat cho Kho Phuoc Tan
     **/
    public function getAgentAllowImport() {
        $aAgentCar      =  array_keys($this->getArrayAgentCar());
        $aAgentMore     = [
            GasConst::UID_AGENT_BINH_THANH_1,
            GasCheck::KHO_PHUOC_TAN,
//            MyFormat::KHO_VUNG_TAU,
        ];
        return array_merge($aAgentCar, $aAgentMore);
    }
    
    /** @Author: DungNT Jun 09, 2018
     *  @Todo: check xem có phải tài xế kho PT xuất cho BT 1 không
     * if true thì sẽ xóa gas dư ở kho PT đi
     **/
    public function isDriverKhoPhuocTanExport() {
        $ok = false;
        if($this->agent_id == GasCheck::KHO_PHUOC_TAN && in_array($this->mCustomer->id, $this->getAgentAllowImport()) ){
            $ok = true;
        }
        return $ok;
    }
    
    /** @Author: DungNT Jun 05, 2018
     *  @Todo: flow Xuất gas dư cho đại lý khác của PVKH
    *   1. khi select Agent hoặc Customer dưới app thì gửi request lên lấy list gas dư chờ xuất
        - Nếu là xuất cho Agent thì sẽ trả về list gas dư, còn là customer thì ko trả gì hết - trả về có id
        2. khi submit lên, nếu là xuất cho agent thì chuyển những gas dư đó sang trạng thái đã xuất
        2.1 đồng thời nhập gas dư đó cho đại lý kia - trạng thái còn tồn 
     **/
    public function handleExportToOtherAgent($q) {
        if($this->mCustomer->role_id != ROLE_AGENT || !$this->isNewRecord){
            return false;
        }
        
//        if( $this->agent_id != GasConst::UID_AGENT_XE_TAI_1 && 
        if( !array_key_exists($this->agent_id, $this->getArrayAgentCar()) && 
                (in_array($this->mCustomer->id, $this->getAgentLockExport()) || !in_array($this->mCustomer->id, $this->getAgentAllowImport())) ){
            throw new Exception('E01 Hệ thống chặn không thể xuất gas dư cho '.$this->getCustomer('first_name'). '. Liên hệ kế toán khu vực để được hỗ trợ hoặc Dũng IT 01684 331 552');
        }
        if($q->list_user_of_agent < 1){
            throw new Exception('Chưa chọn PVKH của đại lý xuất gas dư: '.$this->getCustomer('first_name'));
        }
        // Jul1018 xử lý ko cho xe tải lưu động xuất ngược về các đại lý khác
        if(array_key_exists($this->agent_id, $this->getArrayAgentCar()) && $this->mCustomer->id != GasCheck::KHO_PHUOC_TAN){
            throw new Exception('E02 Hệ thống chặn không thể xuất gas dư cho '.$this->getCustomer('first_name'). '. Liên hệ kế toán khu vực để được hỗ trợ hoặc Dũng IT 01684 331 552');
        }
        
//        throw new Exception('Không thể sử dụng chức năng xuất gas dư cho đại lý khác, chức năng này đang hoàn thiện. Mọi thắc mắc liên hệ Dũng IT: 01684 331 552 ');
        // 1. chuyển trạng thái đã xuất cho các item xuất đi
        $aId = [];
        foreach($q->order_detail as $item){
            $aId[] = $item->id;
        }
        $needMore['OnlyFindall'] = 1;
        $this->aDataApp = GasRemain::getByListId($aId, $needMore);
        foreach($this->aDataApp as $mRemain){
            // NOTE: với tài xế Phước Tân, chỗ này có thể sẽ xóa luôn $mRemain, chỉ theo dõi ở kho mà tài xế đã xuất vào
            if(!$this->isDriverKhoPhuocTanExport()){
                $mRemain->updateOnlyStatusExport(GasRemain::STATUS_EXPORT);
            }
        }

        // 2. tạo mới các item cho Agent nhập gas dư
        $this->doExportToOtherAgent($q);
        $this->gas_remain_type = GasRemain::APP_EXPORT_TO_AGENT;
        // 3. xóa item nếu là TX kho Phước Tân xuất vào Bình Thạnh 1
        $this->deleteAfterExport();
        return true;
    }
    
    /** @Author: DungNT Jun 09, 2018
     *  @Todo: với tài xế Phước Tân, chỗ này có thể sẽ xóa luôn $mRemain, chỉ theo dõi ở kho mà tài xế đã xuất vào
     **/
    public function deleteAfterExport() {
        if(count($this->aDataApp) < 1 || !$this->isDriverKhoPhuocTanExport()){
            return ;
        }
        foreach($this->aDataApp as $mRemain){
            $mRemain->delete();
        }
    }
    
    /** @Author: DungNT Jun 05, 2018
     *  @Todo: update only param has_export
     **/
    public function updateOnlyStatusExport($has_export) {
        $this->has_export               = $has_export;
        $this->status_employee_debit    = GasRemain::EMPLOYEE_DEBIT_NO;
        $this->update(['has_export', 'status_employee_debit']);
    }
    
    /** @Author: DungNT Jun 05, 2018
     *  @Todo: tạo mới các item cho Agent nhập gas dư
     *  @Note1: Nếu từ đại lý xe tải xuất về Phước Tân thì sẽ không overide uid_login để tính lượng gas dư cân lại thiếu OR thừa gắn lại cho PVKH của xe tải
     *  @Note2: khi cân lại lần 2 ở Kho PT thì phải cộng lại cho PVKH xe tải 1 nếu lệch thiếu 
     * @note3: Kho PT: h em muốn là xe lưu động hoặc kho NXVT trả vỏ ngày nào thì gas dư phải về đúng ngày đó dk k ạ.
     **/
    public function doExportToOtherAgent($q) {
        $overrideUidLogin = true;// Nếu từ đại lý xe tải xuất về Phước Tân thì sẽ không overide uid_login để tính lượng gas dư cân lại thiếu OR thừa
//        if($this->mCustomer->id == GasCheck::KHO_PHUOC_TAN){// Aug1318 nếu là Xe tải hcm1 trả gas dư về Kho PT thì set là ngày hiện tại
        if(in_array($this->mCustomer->id, GasCheck::getAgentNotGentAuto())){
            $overrideUidLogin = false;
        }
        $created_date = date('Y-m-d H:i:s');
        
        foreach($this->aDataApp as $mRemain){
            $mRemainCopy = new GasRemain();
            $aFieldNotCopy = array('id', 'created_date');
            // handle add promotion of user
            MyFormat::copyFromToTable($mRemain, $mRemainCopy, $aFieldNotCopy);
            $mRemainCopy->has_export    = GasRemain::STATUS_NOT_EXPORT;
            $mRemainCopy->agent_id      = $this->mCustomer->id;
            if($overrideUidLogin){
                $mRemainCopy->uid_login = $q->list_user_of_agent;
            }
            if(in_array($this->mCustomer->id, GasCheck::getAgentNotGentAuto())){
                $mRemainCopy->date_input = date('Y-m-d');
            }// Aug1318 nếu là Xe tải hcm1, xe Vũng Tàu trả gas dư về Kho PT thì set là ngày hiện tại
            
            // Nếu là xe tải 1 xuất cho Kho PT thì phải xóa uid_login đi, vì có thể nó sẽ làm sai công nợ gas dư của NV, vì 1 NV có thể ở nhiều đại lý
            // trường hợp này sẽ căn cứ vào user_update_2 phải < 1 thì mới cộng công nợ gas dư cho PVKH xe tải 1
            $mRemainCopy->agent_id_old          = $mRemain->agent_id;
            $mRemainCopy->status_employee_debit = GasRemain::EMPLOYEE_DEBIT_YES;
            $mRemainCopy->created_date          = $created_date;
            if(!$this->isDriverKhoPhuocTanExport()){
                // với tài xế Phước Tân đã xóa luôn $mRemain khi xuất cho Agent khác, do đó phải giữ only_gas_remain=0, còn các đại lý khác xuất thì đều tăng lên 1
                $mRemainCopy->only_gas_remain++;// Oct3118 cờ xử lý gas dư đc xuất qua bao nhiêu đại lý, chỉ tính vào báo cáo gas dư của đại lý đầu tiên only_gas_remain = 0 => bc sai: admin/gasreports/target
            }
                    
            $mRemainCopy->save();
        }
    }

    /** @Author: DuongNV Oct1119
     *  @Todo: LỌC BÁO CÁO SO SÁNH VỎ TỒN Ở DAILY GAS DƯ VÀ VỎ TỒN Ở BC XNT
     *  @Des:***Mô tả BC: ví dụ hôm nay 27/09,  tổng vỏ có gas dư còn tồn ở Daily gas dư từ 26/07 đến hết ngày 26/09 = 2 tháng ( IT lọc lấy vỏ 45+50 ở trạng thái “ còn tồn + chờ xuất” từ ngày) so sánh với tổng vỏ 45+50 tồn ở BC XNT ( BC tồn kho này IT chỉ lấy số tồn cuối đến hết ngày 26/09).
        *Nếu = nhau thì đúng.
        *Nếu lệch nhau è IT cho chạy cảnh báo tự động ( tương tự cảnh báo lệch DTBH với Quỹ)
        *KTKV thấy mail cảnh báo sẽ kiểm tra và xử lý để gas dư tồn ở ĐL đúng mà gas dư chuyển về kho, xưởng, trạm cũng đúng luôn. 
     **/
    public function cronAlertDiffGasRemain() {
        
    }
    
}