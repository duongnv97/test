<?php

/**
 * This is the model class for table "{{_contract_management}}".
 *
 * The followings are the available columns in table '{{_contract_management}}':
 * @property integer $id
 * @property string $name
 * @property string $user_id
 * @property integer $contract_type
 * @property string $json
 * @property integer $created_by
 * @property string $created_date
 * @property string $created_date_bigint
 * @property integer $template
 */
class ContractManagement extends BaseSpj
{
    public $autocomplete_user, $autocomplete_created_by;
    
    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return ContractManagement the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{_contract_management}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        return [
            array('name, user_id', 'required'),
            ['id, name, user_id, contract_type, json, created_by, created_date, created_date_bigint, template', 'safe'],
        ];
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        return [
            'rUser' => array(self::BELONGS_TO, 'Users', 'user_id'),
            'rCreatedBy' => array(self::BELONGS_TO, 'Users', 'created_by'),
        ];
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Tên hợp đồng',
            'user_id' => 'Đối tác',
            'contract_type' => 'Loại hợp đồng',
            'json' => 'Json',
            'created_by' => 'Người tạo',
            'created_date' => 'Ngày tạo',
            'created_date_bigint' => 'Ngày tạo',
            'template' => 'Template',
        ];
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        $criteria=new CDbCriteria;

	$criteria->compare('t.name',$this->name,true);
	$criteria->compare('t.user_id',$this->user_id);
	$criteria->compare('t.contract_type',$this->contract_type);
	$criteria->compare('t.created_by',$this->created_by);
        if(isset($this->created_date)){
            $date = MyFormat::dateConverDmyToYmd($this->created_date, '-');
            $criteria->compare('DATE(t.created_date)', $date);
        }
        $criteria->order = 't.id DESC';
        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
            'pagination'=>array(
                'pageSize'=> Yii::app()->user->getState('pageSize',Yii::app()->params['defaultPageSize']),
            ),
        ));
    }
    
    public function getField($field_name){
        switch ($field_name){
            case 'user_id':
            case 'created_by':
                $mUser = Users::model()->findByPk($this->{$field_name});
                if(!empty($mUser)){
                    return $mUser->first_name;
                } else {
                    return '';
                }
            case 'contract_type':
                $aContractType = $this->getArrayContractType();
                return $aContractType[$this->contract_type];
            case 'created_date':
                return date('d/m/Y H:i:s', strtotime($this->created_date));
            case 'template':
                $html = $this->template;
                $this->handleTemplate($html, true);
                return $html;
            default: return $this->{$field_name};
        }
    }
    
    /** @Author: DuongNV Sep 25, 2018
     *  @Todo: Get array contractype (id of EmailTemplates)
     **/
    public function getArrayContractType(){
        return [
            19 => 'Hợp đồng đầu tư và mua bán',
            20 => 'Hợp đồng đặt cọc',
            21 => 'Hợp đồng mượn tài sản',
            22 => 'Hợp đồng không đầu tư',
        ];
    }
    
    
    /** @Author: DuongNV Sep 25, 2018
     *  @Todo: Xử lý convert input to json trước khi create, update
     **/
    public function handleJsonSave() {
        $mTemplate = EmailTemplates::model()->findByPk($this->contract_type);
        $result = isset($_POST['ContractManagementJSON']) ? $_POST['ContractManagementJSON'] : [];
        $this->json     = json_encode($result);
        $this->template = $mTemplate->email_body;
    }
    
    /** @Author: DuongNV Oct 05, 2018
     *  @Todo: Xử lý convert hợp đồng dạng .... sang input
     **/
    public function handleTemplate(&$string, $onlyView = false) {
        $aDbParams = json_decode($this->json, true);
//        $disable = $onlyView ? "disabled='true'" : "";
//        Replace {...} => <input >
        preg_match_all('/{(.*?)}/', $string, $aParams);
        foreach ($aParams[1] as $item) {
            $valueInput = !isset($aDbParams[$item]) ? '' : $aDbParams[$item];
            $inputHtml =  '<input type="text" '
                        .       ' name="ContractManagementJSON['.$item.']" '
                        .       ' value="'.$valueInput.'" >';
            $result = $onlyView ? $valueInput : $inputHtml;
            $string = str_replace('{'.$item.'}', $result, $string);
        }
//        $string = preg_replace_callback(
//        "/\.{3,}/",
//        function($m) use ($aDbParams, $onlyView){
//            static $id = 0;
//            $id++;
//            $valueInput = !isset($aDbParams["num_{$id}"]) ? '' : $aDbParams["num_{$id}"];
//            $inputHtml =  '<input type="text" '
//                        .       ' name="ContractManagementJSON[num_'.$id.']" '
//                        .       ' value="'.$valueInput.'" >';
//            $result = $onlyView ? $valueInput : $inputHtml;
//            return $result;
//        },
//        $string);
    }
    
    /** @Author: DuongNV Oct 06, 2018
     *  @Todo: export pdf contract
     *  @param id of contract
     **/
    public function toPdfContract($id) {
        try{
        $this->pdfPathAndName = 'Contract.pdf';
        $this->toPdf($id);
        }catch (Exception $exc){
            throw new Exception($exc->getMessage());
        }
    }
    
    /** @Author: DuongNV Oct 06, 2018
     *  @Todo: get content pdf
     *  @param id of contract
     **/
    public function getPdfContentQuotes($id){
        $model = ContractManagement::model()->findByPk($id);
        if(empty($model)) {return '';}
        $html = $model->template;
        $model->handleTemplate($html, true);
        return $html;
    }
    
    /** @Author: DuongNV Oct 06, 2018
     *  @Todo: get array user can view and download pdf contract
     **/
    public function getArrayUserViewPdf(){
        return true;
        return [
            2 // admin
        ];
    }
    
    /** @Author: DuongNV Oct 06, 2018
     **/
    public function canViewPdf() {
        $cUid = MyFormat::getCurrentUid();
        $aUserAllow = $this->getArrayUserViewPdf();
        if(in_array($cUid, $aUserAllow)){return true;}
        return false;
    }
    
    
}