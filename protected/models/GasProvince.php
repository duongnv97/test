<?php

/**
 * This is the model class for table "{{_gas_province}}".
 *
 * The followings are the available columns in table '{{_gas_province}}':
 * @property integer $id
 * @property string $name
 * @property string $short_name
 * @property integer $status
 */
class GasProvince extends CActiveRecord
{
    const KV_MIEN_TAY = 18;
    const TP_HCM            = 1;
    const TINH_BINH_DUONG   = 2;
    const TINH_DONG_NAI     = 3;
    const TINH_LONG_AN      = 4;
    const TINH_GIALAI       = 5;
    const TINH_KONTUM       = 6;
    const TINH_VINH_LONG    = 7;
    const TINH_CAN_THO      = 8;
    const TINH_TIEN_GIANG   = 9;
    const TINH_TAY_NINH     = 10;
    const TINH_DONG_THAP    = 11;
    const TINH_TRA_VINH     = 12;
    const TINH_BEN_TRE      = 13;
    const TINH_AN_GIANG     = 14;
    const TINH_QUANG_NGAI   = 15;
    const TINH_KIEN_GIANG   = 16;
    const TINH_SOC_TRANG    = 17;
    const TINH_BINH_DINH    = 20;
    const TINH_DA_NANG      = 21;
    const TINH_PHU_YEN      = 22;
    const TINH_VUNG_TAU     = 23;
    const TINH_BINH_PHUOC   = 24;
    const TINH_LAM_DONG     = 25;
    const TINH_CA_MAU       = 26;
    const TINH_BAC_LIEU     = 27;
    const TINH_KHANH_HOA    = 28;
    const TINH_BINH_THUAN   = 32;
    const TINH_HAU_GIANG    = 44;
    
    public static $PROVINCE_TAY_NGUYEN = array(
        GasProvince::TINH_GIALAI,
        GasProvince::TINH_KONTUM,
    );
    
    public function getProvinceRunApp(){// apr 14, 17
        return [
            GasProvince::TP_HCM,
            GasProvince::TINH_BINH_DUONG,
            GasProvince::TINH_DONG_NAI,
        ];
    }
    public function getProvinceBienDong(){// Now1017
        return [
            GasProvince::TINH_VUNG_TAU,
            GasProvince::TINH_KHANH_HOA,
        ];
    }
    
    /** @Author: ANH DUNG Aug 22, 2017
     * @Todo: get id các tỉnh đã có đại lý
     */
    public function getIdHaveAgent() {
        return [
            self::TP_HCM,
            self::TINH_BINH_DUONG,
            self::TINH_DONG_NAI,
            self::TINH_LONG_AN,
            self::TINH_GIALAI,
            self::TINH_KONTUM,
            self::TINH_VINH_LONG,
            self::TINH_CAN_THO,
            self::TINH_TIEN_GIANG,
            self::TINH_TAY_NINH,
            self::TINH_DONG_THAP,
            self::TINH_TRA_VINH,
            self::TINH_BEN_TRE,
            self::TINH_AN_GIANG,
            self::TINH_QUANG_NGAI,
            self::TINH_KIEN_GIANG,
            self::TINH_SOC_TRANG,
            self::TINH_BINH_DINH,
            self::TINH_DA_NANG,
            self::TINH_PHU_YEN,
            self::TINH_VUNG_TAU,
            self::TINH_BINH_PHUOC,
            self::TINH_LAM_DONG,
            self::TINH_CA_MAU,
            self::TINH_BAC_LIEU,
            self::TINH_KHANH_HOA,
            self::TINH_BINH_THUAN,
            self::TINH_HAU_GIANG,
        ];
    }
    
    public static function model($className=__CLASS__)
    {
       return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
            return '{{_gas_province}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        return array(
            array('name', 'required'),
            array('display_order, id, name, short_name, status', 'safe'),
            array('short_name','unique','message'=>'Tỉnh này đã tồn tại'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        return array();
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'name' => 'Name',
            'short_name' => 'Short Name',
            'status' => 'Status',
            'display_order' => 'Thứ Tự Hiển Thị',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        $criteria=new CDbCriteria;
        $criteria->compare('t.id',$this->id);
        $criteria->compare('t.name',$this->name,true);
        $criteria->compare('t.short_name',$this->short_name,true);
        $criteria->compare('t.status',$this->status);
        $criteria->order = "t.id DESC";

        return new CActiveDataProvider($this, array(
                'criteria'=>$criteria,
                'pagination'=>array(
                    'pageSize'=> 100,
                ),
        ));
    }

    /** @Author: ANH DUNG Aug 22, 2017
     * @Todo: giới hạn lại tỉnh có đại lý 
     */
    public function limitAgent(&$criteria) {
        $sParamsIn = implode(',', $this->getIdHaveAgent());
        $criteria->addCondition("t.id IN ($sParamsIn)");
        $criteria->addCondition('t.status='.STATUS_ACTIVE);
        $criteria->order = 't.display_order ASC, t.id ASC';
    }
    public static function getArrAll(){
        $model = new GasProvince();
        $criteria = new CDbCriteria;
        $model->limitAgent($criteria);
        $models = self::model()->findAll($criteria);
        return  CHtml::listData($models,'id','name');
    }

    /** @Author: ANH DUNG Aug 22, 2017
     * @Todo: get all Array Province for Nhân Sự
     */
    public static function getArrAllFix(){
        $criteria = new CDbCriteria;
        $criteria->addCondition('t.status='.STATUS_ACTIVE);
        $criteria->order = 't.display_order ASC, t.id ASC';
        $models = self::model()->findAll($criteria);
        return  CHtml::listData($models,'id','name');
    }
    
    public static function getArrModel(){
        $model = new GasProvince();
        $criteria = new CDbCriteria;
        $model->limitAgent($criteria);
        $models = self::model()->findAll($criteria);
        $res=array();
        foreach ($models as $item)
                $res[$item->id] = $item;
        return  $res;         
    }
    
        
    public function beforeSave() {
        $this->short_name = strtolower(MyFunctionCustom::remove_vietnamese_accents($this->name));
        return parent::beforeSave();
    }

    public function beforeValidate() {
        $this->name = trim($this->name);
        $this->short_name = strtolower(MyFunctionCustom::remove_vietnamese_accents($this->name));
        return parent::beforeValidate();
    }

    public function behaviors()
    {
        return array(
            'sluggable' => array(
                    'class' => 'application.extensions.mintao-yii-behavior-sluggable.SluggableBehavior',
                    'columns' => array('short_name'),
                    'unique' => true,
                    'update' => true,
            ), 
        );
    }
    
    /**
     * @Author: ANH DUNG Jun 24, 2016
     * @Todo: api windown format data for window create customer ho gia dinh c#
     */
    public static function apiGetDataAddress() {
        $aRes = array();
        $model = new GasProvince();
        $criteria = new CDbCriteria;
        $model->limitAgent($criteria);
        $aProvince = self::model()->findAll($criteria);
        $aDistrict = GasDistrict::apiGetData();
//        $aWard = GasWard::apiGetData();
        foreach($aProvince as $item){
            $aTemp = array(
                "id" => $item->id,
                "name" => $item->name,
                "data" => isset($aDistrict[$item->id]) ? $aDistrict[$item->id]:array(),
            );
            $aRes[] = $aTemp;
        }
        return $aRes;
    }
    
    /** @Author: NamNH May 13, 2019
     *  @Todo: get province by id
     **/
    public function getProvinceByID($aId)
    {
        $aResult = [];
        $criteria=new CDbCriteria;
        $criteria->addInCondition('t.id',$aId);
        $aModels = self::model()->findAll($criteria);
        foreach ($aModels as $key => $model) {
            $aResult[$model->id] = $model;
        }
        return $aResult;
    }
        
    /** @Author: NamNH Aug 23, 2019
     *  @Todo: convert array
     **/
    public function convertToArrayView($aMaynyIdOneId,&$aOneId,&$aManyId) {
        $aResult = [];
        foreach ($aMaynyIdOneId as $many_id => $oneId) {
            $aOneId[]   = $oneId;
            $aManyId[]  = $many_id;
            $aResult[$oneId][$many_id] = $many_id;
        }
        return $aResult;
    }
}