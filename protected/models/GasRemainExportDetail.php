<?php

/**
 * This is the model class for table "{{_gas_remain_export_detail}}".
 *
 * The followings are the available columns in table '{{_gas_remain_export_detail}}':
 * @property string $id
 * @property string $group_id
 * @property string $remain_export_id
 * @property string $gas_remain_id
 * @property string $agent_id
 * @property integer $type
 * @property integer $serial
 * @property string $amount_has_gas
 * @property string $amount_gas
 */
class GasRemainExportDetail extends CActiveRecord
{
    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return GasRemainExportDetail the static model class
     */
    public static function model($className=__CLASS__)
    {
            return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
            return '{{_gas_remain_export_detail}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        return array(
            array('note, id, group_id, remain_export_id, gas_remain_id, agent_id, type, serial, amount_has_gas, amount_gas', 'safe'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
            return array(
            );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
            return array(
                    'id' => 'ID',
                    'group_id' => 'Group',
                    'remain_export_id' => 'Remain Export',
                    'gas_remain_id' => 'Gas Remain',
                    'agent_id' => 'Agent',
                    'type' => 'Type',
                    'serial' => 'Serial',
                    'amount_has_gas' => 'Amount Has Gas',
                    'amount_gas' => 'Amount Gas',
                    'note' => 'Ghi Chú',
            );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
            // Warning: Please modify the following code to remove attributes that
            // should not be searched.

            $criteria=new CDbCriteria;

		$criteria->compare('t.id',$this->id,true);
		$criteria->compare('t.group_id',$this->group_id,true);
		$criteria->compare('t.remain_export_id',$this->remain_export_id,true);
		$criteria->compare('t.gas_remain_id',$this->gas_remain_id,true);
		$criteria->compare('t.agent_id',$this->agent_id,true);
		$criteria->compare('t.type',$this->type);
		$criteria->compare('t.serial',$this->serial);
		$criteria->compare('t.amount_has_gas',$this->amount_has_gas,true);
		$criteria->compare('t.amount_gas',$this->amount_gas,true);

            return new CActiveDataProvider($this, array(
                    'criteria'=>$criteria,
                    'pagination'=>array(
                        'pageSize'=> Yii::app()->user->getState('pageSize',Yii::app()->params['defaultPageSize']),
                    ),
            ));
    }

    public function defaultScope()
    {
            return array(
                    //'condition'=>'',
            );
    }
    
    // Jun 22, 2014
    public static function delete_by_remain_export_id($remain_export_id){
        $criteria = new CDbCriteria();
        $criteria->compare('remain_export_id', $remain_export_id);
        self::model()->deleteAll($criteria);
        return;
        $models = self::model()->findAll($criteria); // hiện tại chưa thấy cần thết phải xóa kiểu findAll cho chõ này
        if(count($models)){
            foreach($models as $item){
                $item->delete();
            }
        }        
    }
    
    public static function get_by_remain_export_id($remain_export_id){
        $criteria = new CDbCriteria();
        $criteria->compare('remain_export_id', $remain_export_id);
        return self::model()->findAll($criteria);
    }    
    
}