<?php

/**
 * This is the model class for table "{{_call}}".
 *
 * The followings are the available columns in table '{{_call}}':
 * @property string $id
 * @property string $call_uuid
 * @property string $call_uuid_extension
 * @property integer $extension
 * @property string $user_id
 * @property string $agent_id
 * @property integer $province_id
 * @property integer $direction
 * @property integer $call_status
 * @property string $start_time
 * @property string $answer_time
 * @property string $end_time
 * @property integer $total_duration
 * @property integer $bill_duration
 * @property string $created_date
 */
class TableTemp extends CActiveRecord
{
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{_users_fix_phone}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        return array();
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        return array();
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
            return array();
    }

}