<?php

/**
 * This is the model class for table "{{_gas_orders}}".
 *
 * The followings are the available columns in table '{{_gas_orders}}':
 * @property string $id
 * @property string $orders_no
 * @property string $user_id_create
 * @property string $user_id_executive
 * @property integer $type
 * @property string $date_delivery
 * @property string $note 
 * @property string $created_date
 */
class GasOrders extends BaseSpj
{
    public $autocomplete_name_1, $aModelOrderDetail, $customer_id, $b50, $b45, $b12, $b6, $amountDaysMakeOrder = 3;
    public $aOldIdDetail = [], $from_app, $aIdDetailNotUpdateApp = [], $aAppOrderInsert = [], $aOrderDetailIdDelete = [], $aAgent = [], $aModelOrder = [];
    public $driver_id, $customer_contact, $MAX_ID, $reason_wrong, $car_number, $allowUpdateAgent = true, $mAppUserLogin = null, $uid_login=0;
    const TYPE_DIEU_PHOI    = 1;
    const TYPE_AGENT        = 2;
    const PROVINCE_HCM      = 1;
    const WAIT_CAR              = 1;
    const WAIT_VIEW_INVENTORY   = 2;
    
    const UPDATE_CAR_AGAIN      = 1;// Flag update car again for this order
    
    const FROM_WEB  = 1;// Jan 23, 2017 xử lý tách 2 nguồn tạo của order detail ra
    const FROM_APP  = 2;
    const FROM_PMBH = 3;

    public static $days_allow_update = 5; // số ngày cho phép cập nhật đặt hàng, sẽ cập nhật
    // từ 1 ngày trước đến ngày tương lai
    public static $days_allow_delete = 5; // số ngày cho phép admin xóa đặt hàng
    
    public static $ZONE_HCM = array( // array id cac user điều phối cua hcm, phải vào đây sửa để view trên list dc
//        GasTickets::DIEU_PHOI_THUY, // Phan Xuân Thùy
        GasTickets::DIEU_PHOI_CHUONG, //  Trần Quang Chương
        GasTickets::DIEU_PHOI_THAO, //  Nguyễn Thị Thu Thảo
        GasTickets::DIEU_PHOI_TRANG, //  Phạm Ngọc Thùy Trang Mar 23, 2016
        GasConst::UID_PHUONG_TT, //  Trần Thị Phượng Mar 23, 2016
        GasTickets::DIEU_PHOI_HIEU_TM,
    ); // sẽ thêm các khu vực phát sinh ở sau tiếp
    
    public static $ZONE_HCM_FULLNAME = array( // array id cac user điều phối cua hcm, phải vào đây sửa để view trên list dc
//        GasTickets::DIEU_PHOI_THUY      => "Phan Xuân Thùy",
        GasTickets::DIEU_PHOI_CHUONG    => 'Trần Quang Chương',
        GasTickets::DIEU_PHOI_THAO      => 'Nguyễn Thị Thu Thảo',
        GasTickets::DIEU_PHOI_TRANG     => 'Phạm Ngọc Thùy Trang',
        GasConst::UID_PHUONG_TT         => 'Trần Thị Phượng',
        GasTickets::DIEU_PHOI_HIEU_TM   => 'Trần Minh Hiếu',
    ); // sẽ thêm các khu vực phát sinh ở sau tiếp
    
    
    public static $ZONE_MIEN_TAY = array( // array id cac user điều phối cua KV MIEN TAY 
        GasTickets::DIEU_PHOI_NHUNG, // Nguyễn Thị Bảo Trâm
        GasConst::UID_DANG_TH, // Bùi Ngọc Huyền
//        700606, // Nguyễn Thị Mỹ Diệu // CLOSE ON OCT 25, 2016
//        GasSupportCustomer::UID_CHUYEN_DV, //Tổ Trưởng Tổ Bảo Trì - Dương Văn Chuyền - Mien Tay - Jan 04, 2016
    ); // sẽ 
    
    // Đáng lẽ là zone_dieu_phoi_mientrung
    public static $ZONE_MIEN_TRUNG = array(
        GasLeave::DIEU_PHOI_BINH_DINH, // Điều Phối Bình Định Apr 05, 2016
    ); // sẽ 
    
    // Dec 29, 2014 define luôn ở đây, cho tiện theo dõi, list id các tỉnh miền tây
    public static $PROVINCE_ZONE_MIEN_TAY = array(// Aug0819 ko dùng biến này
        7, // Vĩnh Long
        8, // Cần Thơ
        // Jul0719 DungNT close to test xem có sử dụng ở bc nào không???
//        9, // Tiền Giang
//        // 10, // Tây Ninh // Close Sep 03, 2015 Kien keu chuyen sang HCM
//        11, // Đồng Tháp
//        12, // Trà Vinh
//        13, // Bến Tre
//        14, // An Giang
//        16, // Kiên Giang
        17, // Sóc Trăng
        GasProvince::TINH_CA_MAU,
    );
    
    public static $PROVINCE_ZONE_HCM = array(
        1, // TP Hồ Chí Minh
        2, // Bình Dương
        3, // Đồng Nai
        4, // Long An Change Apr 04, 2015 Kiên yc sửa đưa vào zone hcm
        10, // Tây Ninh // Add Sep 03, 2015 Kien keu chuyen sang HCM
    );
    public static $PROVINCE_ZONE_MIEN_TRUNG = array(
        20, // Bình Định
        21, // Quảng Ngãi
        22, // Phú Yên
//        5, // Gia Lai
    );
    // Dec 29, 2014 define luôn ở đây, cho tiện theo dõi, list id các tỉnh miền tây
    
    
    // ***** Mar 02, 2016 define target zone phục vụ báo cáo: admin/gasreports/targetNew
    public static $TARGET_ZONE_PROVINCE_HCM = array( // array id cac province KV Hồ Chí Minh
        1, // TP Hồ Chí Minh
//        3, // Đồng Nai hay ? // Long An Mar 31, 2016 ngoc_kien1: co 1 khách hàng nằm ở Long An
    );
    public static $TARGET_ZONE_PROVINCE_MIEN_DONG = array( // array id cac province KV DONG NAM BO
        2, // Bình Dương
        3, // Đồng Nai
        GasProvince::TINH_BINH_PHUOC,
    );
    public static $TARGET_ZONE_HCM_MIENDONG = array( // array id cac province KV Hồ Chí Minh + MIEN DONG
        1, // TP Hồ Chí Minh
        2, // Bình Dương
        3, // Đồng Nai
        GasProvince::TINH_VUNG_TAU,
        GasProvince::TINH_TAY_NINH,
        GasProvince::TINH_BINH_PHUOC,
//        GasProvince::TINH_LONG_AN,// Close Jan1619 đưa vào KV Miền Tây
        GasProvince::TINH_BINH_THUAN,
    );
    
    public static $TARGET_ZONE_PROVINCE_MIEN_TAY = array( // array id cac province KV MIEN TAY 
        7, // Vĩnh Long
        8, // Cần Thơ
        9, // Tiền Giang
        11, // Đồng Tháp
        12, // Trà Vinh
        GasProvince::TINH_BEN_TRE, // Bến Tre
        14, // An Giang
        16, // Kiên Giang
        17, // Sóc Trăng
//        4, // Long An Move zone on Jul2318
        GasProvince::TINH_CA_MAU,
        GasProvince::TINH_BAC_LIEU,
        GasProvince::TINH_LONG_AN,
        GasProvince::TINH_HAU_GIANG,
    );
    public static $TARGET_ZONE_PROVINCE_MIEN_TRUNG = array( // array id cac province KV MIEN TRUNG
        GasProvince::TINH_BINH_DINH,
        GasProvince::TINH_QUANG_NGAI,
        GasProvince::TINH_PHU_YEN,
        GasProvince::TINH_KHANH_HOA,// May2319 open => Close Mar2619 QL Nhan su, Nhi ko quan ly KV Khanh Hoa nua - employeesLimitUserUpdate
    );
    
    public static $TARGET_ZONE_PROVINCE_VUNG_TAU = array( // array id cac province KV VUNG TAU
        GasProvince::TINH_VUNG_TAU,
//        GasProvince::TINH_KHANH_HOA,// May2319 Anh Duc noi đưa lại vào KV mien trung phuc vụ báo cáo: admin/gasreports/output_customer - ko move nữa
    );

    public static $TARGET_ZONE_PROVINCE_TAY_NGUYEN = array( // array id cac province KV TAY NGUYEN
        GasProvince::TINH_GIALAI,
        GasProvince::TINH_KONTUM,
    );
    
    const ZONE_TARGET_HCM           = 1;
    const ZONE_TARGET_MIENDONG      = 2;
    const ZONE_TARGET_MIENTAY       = 3;
    const ZONE_TARGET_MIENTRUNG     = 4;
    const ZONE_TARGET_TAY_NGUYEN    = 5;
    const ZONE_TARGET_HCM_MIEN_DONG = 6;// Oct 14, 2016
    const ZONE_TARGET_VUNG_TAU      = 7;// Jan 13, 2017
    // ***** Mar 02, 2016 define target zone 
    
    /**  @Author: ANH DUNG Mar 02, 2016 */
    public static function getTargetZone() {
        return array( // array id cac zone => name zone
            GasOrders::ZONE_TARGET_HCM          => 'TP Hồ Chí Minh',
            GasOrders::ZONE_TARGET_MIENDONG     => 'KV Miền Đông',
            GasOrders::ZONE_TARGET_MIENTAY      => 'KV Miền Tây',
            GasOrders::ZONE_TARGET_MIENTRUNG    => 'KV Miền Trung',
            GasOrders::ZONE_TARGET_TAY_NGUYEN   => 'KV Tây Nguyên',
            GasOrders::ZONE_TARGET_VUNG_TAU     => 'KV Vũng Tàu',
        );
    }
    public static function getTargetZoneProvinceId() {
        return array( // array id cac province KV Hồ Chí Minh
            GasOrders::ZONE_TARGET_HCM          => GasOrders::$TARGET_ZONE_PROVINCE_HCM,
            GasOrders::ZONE_TARGET_MIENDONG     => GasOrders::$TARGET_ZONE_PROVINCE_MIEN_DONG,
            GasOrders::ZONE_TARGET_MIENTAY      => GasOrders::$TARGET_ZONE_PROVINCE_MIEN_TAY,
            GasOrders::ZONE_TARGET_MIENTRUNG    => GasOrders::$TARGET_ZONE_PROVINCE_MIEN_TRUNG,
            GasOrders::ZONE_TARGET_TAY_NGUYEN   => GasOrders::$TARGET_ZONE_PROVINCE_TAY_NGUYEN,
            GasOrders::ZONE_TARGET_VUNG_TAU     => GasOrders::$TARGET_ZONE_PROVINCE_VUNG_TAU,
        );
    }
    
    /**
     * @Author: ANH DUNG Oct 14, 2016
     */
    public static function getZoneNew() {
        return array( // array id cac zone => name zone
            GasOrders::ZONE_TARGET_HCM_MIEN_DONG    => 'TP HCM - Miền Đông',
            GasOrders::ZONE_TARGET_MIENTAY          => 'KV Miền Tây',
            GasOrders::ZONE_TARGET_MIENTRUNG        => 'KV Miền Trung',
            GasOrders::ZONE_TARGET_TAY_NGUYEN       => 'KV Tây Nguyên',
            GasOrders::ZONE_TARGET_VUNG_TAU         => 'KV Vũng Tàu',
        );
    }
    
    public static function getZoneNewProvinceId() {
        return array(
            GasOrders::ZONE_TARGET_HCM_MIEN_DONG    => GasOrders::$TARGET_ZONE_HCM_MIENDONG,
            GasOrders::ZONE_TARGET_MIENTAY          => GasOrders::$TARGET_ZONE_PROVINCE_MIEN_TAY,
            GasOrders::ZONE_TARGET_MIENTRUNG        => GasOrders::$TARGET_ZONE_PROVINCE_MIEN_TRUNG,
            GasOrders::ZONE_TARGET_TAY_NGUYEN       => GasOrders::$TARGET_ZONE_PROVINCE_TAY_NGUYEN,
            GasOrders::ZONE_TARGET_VUNG_TAU         => GasOrders::$TARGET_ZONE_PROVINCE_VUNG_TAU,
        );
    }
    
    /** @Author: ANH DUNG May 28, 2018
     *  @Todo: get ke toan of zone 
     **/
    public function getAccountingZone() {
        return array(
            GasOrders::ZONE_TARGET_HCM_MIEN_DONG    => GasLeave::PHUONG_PTK,
            GasOrders::ZONE_TARGET_MIENTAY          => GasConst::UID_THUY_HT,
            GasOrders::ZONE_TARGET_MIENTRUNG        => GasConst::UID_HIEP_TV,
            GasOrders::ZONE_TARGET_TAY_NGUYEN       => GasConst::UID_HIEP_TV,
            GasOrders::ZONE_TARGET_VUNG_TAU         => GasConst::UID_HIEP_TV,
        );
    }
    
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return '{{_gas_orders}}';
    }

    public function rules()
    {
        return array(
            array('agent_id, user_id_executive, date_delivery', 'required', 'on'=>'create,update'),
            array('user_id_executive, customer_id', 'required', 'on'=>'WindowCreate'),
            array('note', 'length', 'max'=>500),
            array('date_delivery, created_date,update_again_car', 'safe'),
            array('driver_id, reason_wrong, id, orders_no, user_id_create, user_id_executive, type, date_delivery, note, created_date,car_number', 'safe'),
        );
    }

    public function relations()
    {
        return array(
            'rOrderDetail' => array(self::HAS_MANY, 'GasOrdersDetail', 'orders_id',
                'order' => 'rOrderDetail.id ASC',
            ),
            'rUserIdCreate' => array(self::BELONGS_TO, 'Users', 'user_id_create'),
            'rUserIdExecutive' => array(self::BELONGS_TO, 'Users', 'user_id_executive'),
            'rUserLastUpdateBy' => array(self::BELONGS_TO, 'Users', 'last_update_by'),
            'rAgent' => array(self::BELONGS_TO, 'Users', 'agent_id'),
        );
    }

    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'orders_no' => 'Mã Đơn Hàng',
            'user_id_create' => 'Người Tạo',
            'user_id_executive' => 'Người Thực Hiện',
            'type' => 'Type',
            'date_delivery' => 'Ngày Giao',
            'note' => 'Ghi Chú',
            'created_date' => 'Ngày Tạo',
            'car_number' => 'Số Xe',
            'last_update_by' => 'Người Sửa',
            'last_update_time' => 'Sửa Mới Nhất',
            'reason_wrong' => 'Lỗi điều phối',
            'agent_id' => 'Đại lý',
            'driver_id' => 'Lái xe',
        );
    }

    public function search()
    {
        $criteria=new CDbCriteria;
        $criteria->compare('t.orders_no', trim($this->orders_no),true);
        $cAgent = MyFormat::getAgentId();
        
        if(!empty($this->user_id_create)){
            $criteria->addCondition("t.agent_id=$this->user_id_create");
        }
        $aWith = [];
        if(!empty($this->reason_wrong)){
            $aWith[] = 'rOrderDetail';
            $criteria->addCondition("rOrderDetail.reason_wrong=$this->reason_wrong");
            $criteria->together = true;
        }
        if(!empty($this->driver_id)){
            $aWith[] = 'rOrderDetail';
            $criteria->addCondition("rOrderDetail.driver_id=$this->driver_id");
            $criteria->together = true;
        }
        if(count($aWith)){
            $criteria->with = $aWith;
        }
        
        if(!empty($this->user_id_executive)){
            $criteria->addCondition("t.user_id_executive=$this->user_id_executive");
        }
        if(!empty($this->type)){
            $criteria->addCondition("t.type=$this->type");
        }
        $criteria->compare('t.note',$this->note,true);
        if(!empty($this->created_date)){
            $this->created_date = MyFormat::dateDmyToYmdForAllIndexSearch($this->created_date);
            $criteria->compare('t.created_date',$this->created_date,true);
        }   
        if(!empty($this->date_delivery)){
            $this->date_delivery = MyFormat::dateDmyToYmdForAllIndexSearch($this->date_delivery);
            $criteria->compare('t.date_delivery', $this->date_delivery);
        }
        $aRoleCreate = array(ROLE_SUB_USER_AGENT, ROLE_DIEU_PHOI);
        $cRole  = MyFormat::getCurrentRoleId();
        $cUid   = MyFormat::getCurrentUid();
        
        if(in_array( $cRole, $aRoleCreate)){
            // nếu là user tạo ra thì chỉ load những record của user đó
            // điều phối có 2 người thì 2 người có thể xem dc đơn hàng của nhau, còn đại lý thì của ai người đó xem
            if( $cRole==ROLE_DIEU_PHOI){
                $criteria->compare('t.type', GasOrders::TYPE_DIEU_PHOI);
                // Dec 29, 2014 chia bộ phận điều phối của HCM vs miền tây, sẽ không nhìn thấy đơn hàng của nhau
//                if (in_array($cUid, GasOrders::$ZONE_HCM)){
////                    $criteria->addInCondition('t.user_id_create', GasOrders::$ZONE_HCM);
//                    $sParamsIn = implode(',', GasOrders::$ZONE_HCM);
////                    $criteria->addCondition("t.user_id_create IN ($sParamsIn)");// Jun 11, 2017 bỏ limit cho điều phối HCM vì bị chuyển giữa 2 loại user CallCenter
//                }elseif (in_array($cUid, GasOrders::$ZONE_MIEN_TAY)){
////                    $criteria->addInCondition('t.user_id_create', GasOrders::$ZONE_MIEN_TAY);
//                    $sParamsIn = implode(',', GasOrders::$ZONE_MIEN_TAY);
//                    $criteria->addCondition("t.user_id_create IN ($sParamsIn)");
//                }elseif (in_array($cUid, GasOrders::$ZONE_MIEN_TRUNG)){
////                    $criteria->addInCondition('t.user_id_create', GasOrders::$ZONE_MIEN_TRUNG);
//                    $sParamsIn = implode(',', GasOrders::$ZONE_MIEN_TRUNG);
//                    $criteria->addCondition("t.user_id_create IN ($sParamsIn)");
//                }// Jun 11, 2017 close lại. cho điều phối thấy hết của nhau, vì đã gộp vào 1
                // Dec 29, 2014 chia bộ phận điều phối của HCM vs miền tây, sẽ không nhìn thấy đơn hàng của nhau
            }elseif($cAgent == GasCheck::KHO_LY_CHINH_THANG){
                $sProvince = [GasProvince::TINH_GIALAI, GasProvince::TINH_KONTUM];
                $mEmployeeCashbook = new EmployeeCashbook();
                $aAgentId = $mEmployeeCashbook->getAgentOfProvince($sProvince);
                $sParamsIn = implode(',', $aAgentId);
                if(!empty($sParamsIn)){
                    $criteria->addCondition("t.agent_id IN ($sParamsIn)");
                }
            }
            else{
                $criteria->compare('t.user_id_create', $cUid);
            }            
        }
        
        // RULE NÀY ĐÃ CHECK BÊN DƯỚI Jun 11, 2014 kế toán đại lý vs kế toán khu vực chỉ dc nhìn thấy đại lý giới hạn
//        if(!in_array(Yii::app()->user->role_id, GasCheck::$ARR_ROLE_LIMIT_AGENT)){
//            $criteria->compare('t.type', GasOrders::TYPE_AGENT);
//        }
        
        if( $cRole==ROLE_SCHEDULE_CAR ){
            // nếu là điều xe thì chỉ load những record dc phân công điều xe 
            $criteria->compare('t.user_id_executive', $cUid);
        }
        
        $session=Yii::app()->session;
        // danh sách những đại lý giới hạn cho user 
        if(isset($session['LIST_AGENT_OF_USER']) && count($session['LIST_AGENT_OF_USER'])){
//            $criteria->addInCondition('t.agent_id', $session['LIST_AGENT_OF_USER']); 
            $sParamsIn = implode(',', $session['LIST_AGENT_OF_USER']);
            $criteria->addCondition("t.agent_id IN ($sParamsIn)");
        }
        // danh sách những đại lý giới hạn cho user         
        
        $sort = new CSort();
        $sort->attributes = array(
            'orders_no'=>'orders_no',
            'user_id_create'=>'user_id_create',
            'user_id_executive'=>'user_id_executive',
            'date_delivery'=>'date_delivery',
            'created_date'=>'created_date',
        );    
        $sort->defaultOrder = 't.date_delivery DESC, t.created_date DESC';      	        

        return new CActiveDataProvider($this, array(
                'criteria'=>$criteria,
                'pagination'=>array(
                    'pageSize'=> 50,
//                    'pageSize'=> Yii::app()->user->getState('pageSize',Yii::app()->params['defaultPageSize']),
                ),
            'sort' => $sort,
        ));
    }
    
    // danh sách những xe cần update lại điều xe
    public function search_update_again_car()
    {
        $criteria=new CDbCriteria;
        if(Yii::app()->user->role_id==ROLE_SCHEDULE_CAR){
            // nếu là điều xe thì chỉ load những record dc phân công điều xe 
            $criteria->compare('t.user_id_executive', Yii::app()->user->id);
        }
        $criteria->compare('t.update_again_car', 1);
        $prev_day = date('Y-m-d');
//        $prev_day = MyFormat::modifyDays($prev_day, 1, '-');
        $criteria->addCondition("t.date_delivery>='".$prev_day."'");
        
        $sort = new CSort();
        $sort->attributes = array(
            'orders_no'=>'orders_no',
            'user_id_create'=>'user_id_create',
            'user_id_executive'=>'user_id_executive',
            'date_delivery'=>'date_delivery',
            'created_date'=>'created_date',
        );    
        $sort->defaultOrder = 't.date_delivery DESC';      	        

        return new CActiveDataProvider($this, array(
                'criteria'=>$criteria,
                'pagination'=>array(
                    'pageSize'=> 20,
//                    'pageSize'=> Yii::app()->user->getState('pageSize',Yii::app()->params['defaultPageSize']),
                ),
            'sort' => $sort,
        ));
    }
    
    /** @Author: ANH DUNG Jun 25, 2017
     * @Todo: get role like sub agent
     */
    public function getRoleLikeSubAgent() {
        return [ROLE_MONITORING_MARKET_DEVELOPMENT, ROLE_SUB_USER_AGENT, ROLE_CALL_CENTER, ROLE_ADMIN, ROLE_ACCOUNTING_ZONE, ROLE_SCHEDULE_CAR, ROLE_ACCOUNTING];
    }
    
    public function beforeSave() {
        $this->convertDate();
        if($this->isNewRecord){
            $this->orders_no = MyFunctionCustom::getNextId('GasOrders', 'DH'.date('y'), LENGTH_ORDER_BY_YEAR,'orders_no');
//            $this->mapInfoFromWeb();// Jun 25, 2017 đưa xuống afterValidate
            if($this->type == GasOrders::TYPE_DIEU_PHOI){
                $this->user_id_create = GasTickets::DIEU_PHOI_HCM;// để fix tách đơn hàng sẽ gắn cho 1 user điều phối HCM tạo đơn
            }
        }else{
            $this->setUserUpdate(MyFormat::getCurrentUid());
        }

        $this->note = InputHelper::removeScriptTag($this->note);
        return parent::beforeSave();
    }
    
    /** @Author: ANH DUNG Sep 21, 2018
     *  @Todo: set last user update
     **/
    public function setUserUpdate($cUid) {
        $this->last_update_by   = $cUid;
        $this->last_update_time = date('Y-m-d H:i:s');
    }
    
    /** @Author: ANH DUNG Aug 19, 2017
     * @Todo: convert dateto db date
     */
    public function convertDate() {
        if(strpos($this->date_delivery, '/')){
            $this->date_delivery =  MyFormat::dateConverDmyToYmd($this->date_delivery);
            MyFormat::isValidDate($this->date_delivery);
        }
    }
    
    /**
     * @Author: ANH DUNG Jan 23, 2017
     * @Todo: map some info from web create
     */
    public function mapInfoFromWeb() {
        $aScenarioNotAllow = array('WindowAutoCreate', 'CronAutoMakeOrders');
        if(in_array($this->scenario, $aScenarioNotAllow)){
            return ;
        }
        $cUid   = MyFormat::getCurrentUid();
        $cRole  = MyFormat::getCurrentRoleId();
        $this->user_id_create = $cUid;
        // xử lý xong cho TYPE_AGENT khi change multi user login thì ko cần sửa chỗ này nữa
        if($cRole == ROLE_DIEU_PHOI){
            $this->type = GasOrders::TYPE_DIEU_PHOI;
            // nếu là điều phối thì mình sẽ lấy id user login làm agent id 
            // phục vụ cho việc search => hiện tại mới nghĩ ra hướng này, không biết có ảnh hưởng gì ko
            $this->agent_id = $cUid;
        }else{
            $this->type     = GasOrders::TYPE_AGENT;
            if(empty($this->agent_id)){
                $this->agent_id = MyFormat::getAgentId();
            }
            if(empty($this->agent_id)){
                $this->addError('agent_id', 'Chưa chọn đại lý');
            }
        }
    }

    
    /**
     * @Author: ANH DUNG Apr 09, 2014
     * @Todo: delete all orderdetail with order id if exitst, and add new all orderdetail 
     * @Param: $model model order
     */    
    public static function saveOrderDetail($model){
        GasOrdersDetail::deleteByOrderId($model->id);
        $aRowInsert = $aCustomerIdNew = []; $cUid = MyFormat::getCurrentUid();
        $created_date   = date('Y-m-d H:i:s');
        if( is_array($model->aModelOrderDetail)){
            foreach($model->aModelOrderDetail as $item){
                if( $item->lpg_ton>0 || $item->quantity_50>0 || 
                        $item->quantity_45>0 || $item->quantity_12>0 ||
                        $item->quantity_6>0 || $item->quantity_other_material>0 ||
                        $item->customer_id>0 || trim($item->work_content)!='' ||
                        $item->other_material_id>0
                    ) {
//                if( !empty($item->lpg_ton) || !empty($item->quantity_50) || 
//                        !empty($item->quantity_45) || !empty($item->quantity_12) ||
//                        !empty($item->quantity_6) || !empty($item->quantity_other_material) ||
//                        !empty($item->customer_id) || trim($item->work_content)!='' ||
//                        !empty($item->other_material_id)
//                    ) {
                    if(!isset($aCustomerIdNew[$item->customer_id])){// xử lý không cho tạo trùng 2 KH trong 1 lần tạo
                        $aCustomerIdNew[$item->customer_id] = 1;
                    }else{
                        $aCustomerIdNew[$item->customer_id] += 1;
                    }
                    $orders_id              = $model->id;
                    $user_id_create         = $cUid;// Aug2617 Fix tách đơn bò mối
                    $user_id_executive      = $model->user_id_executive;
                    $type                   = $model->type;
                    $date_delivery          = $model->date_delivery;
                    $customer_id            = $item->customer_id;
                    $type_customer = $sale_id = "";
                    if($item->customer){ // loại KH 1: Bình Bò, 2: Mối, dùng cho thống kê doanh thu
                        //'is_maintain',// vì cột is_maintain không dùng trong loại KH của thẻ kho nên ta sẽ dùng cho 1: KH bình bò, 2. KH mối
                        $type_customer  = $item->customer->is_maintain;
                        $sale_id        = $item->customer->sale_id;                            
                    }
                    $agent_id = $model->agent_id;
                    
                    $lpg_ton = $item->lpg_ton;
                    $quantity_50 = $item->quantity_50;
                    $quantity_45 = $item->quantity_45;
                    $quantity_12 = $item->quantity_12;
                    $quantity_6 = $item->quantity_6;
                    $other_material_id = $item->other_material_id;
                    $quantity_other_material = $item->quantity_other_material;
                    $work_content = MyFormat::removeBadCharacters($item->work_content);
                    $note = MyFormat::removeBadCharacters($item->note); 
                    $aRowInsert[]="('$orders_id',
                        '$user_id_create', 
                        '$user_id_executive', 
                        '$agent_id', 
                        '$type', 
                        '$date_delivery', 
                        '$customer_id',
                        '$type_customer',
                        '$sale_id',
                        '$lpg_ton',
                        '$quantity_50',
                        '$quantity_45',
                        '$quantity_12',
                        '$quantity_6',
                        '$other_material_id',
                        '$quantity_other_material',
                        '$work_content',
                        '$cUid',
                        '$note',
                        '$created_date'
                        )";
                    $item->makeNewAppOrder($model);// Jan 27, 2017
                }
            }
        }
        foreach($aCustomerIdNew as $k => $v){
            if($v > 1 && $model->type == GasOrders::TYPE_DIEU_PHOI){// Aug3117 không cho tạo 2 KH trong 1 request
                throw new Exception('Bạn lên 2 đơn hàng của 1 khách hàng trong 1 lần tạo, vui lòng chia ra nhiều lần lưu cho mỗi đơn hàng của khách hàng');
            }
        }
        
        $tableName = GasOrdersDetail::model()->tableName();
        $model->updateOrderDetailRunInsert($aRowInsert, $tableName);
    }

    /**
     * @Author: ANH DUNG Oct 09, 2016
     */
    public function updateOrderDetailRunInsert($aRowInsert, $tableName) {
        // for add new record
        $sql = "insert into $tableName (orders_id,
                user_id_create,
                user_id_executive,
                agent_id,
                type,
                date_delivery,
                customer_id,
                type_customer,
                sale_id,
                lpg_ton,
                quantity_50,
                quantity_45,
                quantity_12,
                quantity_6,
                other_material_id,
                quantity_other_material,
                work_content,
                created_by,
                note,
                created_date
                ) values ".implode(',', $aRowInsert);
        if(count($aRowInsert))
            Yii::app()->db->createCommand($sql)->execute();
        // for add new record    
    }
    
    public function beforeValidate() {
        $this->aModelOrderDetail = array();
        
        if(isset($_POST['GasOrdersDetail']['quantity_50']) && is_array($_POST['GasOrdersDetail']['quantity_50'])){
            foreach($_POST['GasOrdersDetail']['quantity_50'] as $key=>$item){
                $modelDetail = new GasOrdersDetail();
                $modelDetail->id = isset($_POST['GasOrdersDetail']['id'][$key])?$_POST['GasOrdersDetail']['id'][$key] : 0;
                if(!empty($modelDetail->id) && $modelDetail->id>0){
                    // Mảng id sẽ giữ lại không xóa
                    $this->aOldIdDetail[$modelDetail->id] = $modelDetail->id;
                }
                $modelDetail->customer_id   = isset($_POST['GasOrdersDetail']['customer_id'][$key])?$_POST['GasOrdersDetail']['customer_id'][$key]:'';
                $modelDetail->lpg_ton       = isset($_POST['GasOrdersDetail']['lpg_ton'][$key])?$_POST['GasOrdersDetail']['lpg_ton'][$key]:'';
                $modelDetail->quantity_50   = empty($_POST['GasOrdersDetail']['quantity_50'][$key]) ? 0 : $_POST['GasOrdersDetail']['quantity_50'][$key];
                $modelDetail->quantity_45   = empty($_POST['GasOrdersDetail']['quantity_45'][$key]) ? 0 : $_POST['GasOrdersDetail']['quantity_45'][$key];
                $modelDetail->quantity_12   = empty($_POST['GasOrdersDetail']['quantity_12'][$key]) ? 0 : $_POST['GasOrdersDetail']['quantity_12'][$key];
                $modelDetail->quantity_6    = empty($_POST['GasOrdersDetail']['quantity_6'][$key]) ? 0 : $_POST['GasOrdersDetail']['quantity_6'][$key];
                $modelDetail->other_material_id = isset($_POST['GasOrdersDetail']['other_material_id'][$key])?$_POST['GasOrdersDetail']['other_material_id'][$key]:'';
                $modelDetail->quantity_other_material = empty($_POST['GasOrdersDetail']['quantity_other_material'][$key])? 0 : $_POST['GasOrdersDetail']['quantity_other_material'][$key];
                $modelDetail->work_content  = isset($_POST['GasOrdersDetail']['work_content'][$key])?MyFormat::removeBadCharacters($_POST['GasOrdersDetail']['work_content'][$key]):'';
                $modelDetail->note          = isset($_POST['GasOrdersDetail']['note'][$key])?MyFormat::removeBadCharacters($_POST['GasOrdersDetail']['note'][$key]):'';
                $modelDetail->need_update   = isset($_POST['GasOrdersDetail']['need_update'][$key])?MyFormat::removeBadCharacters($_POST['GasOrdersDetail']['need_update'][$key]):0;

                $this->aModelOrderDetail[] = $modelDetail;
            }
        }
        
        $aScenarioNotAllow = array('WindowAutoCreate', 'CronAutoMakeOrders');
        if(in_array($this->scenario, $aScenarioNotAllow)){
            return parent::beforeValidate();
        }
        if($this->isNewRecord){
            $this->mapInfoFromWeb();
        }
      
        return parent::beforeValidate();
    }

    public function beforeDelete() {
        GasOrdersDetail::deleteByOrderId($this->id);
        return parent::beforeDelete();
    }        
    
    /**
     * @Author: ANH DUNG 12-29-2013
     * @Todo: get model by $release_date
     * @param: $date_delivery Ngày tạo
     * @param: $user_id_create user tạo
     * @Return: model or null
     */
    public static function getByDateDelivery($date_delivery, $user_id_executive, $user_id_create)
    {
        $criteria=new CDbCriteria; 
        if(!empty($date_delivery)){
            $criteria->addCondition("t.date_delivery='$date_delivery'");
        }
        if(!empty($user_id_executive)){
            $criteria->addCondition("t.user_id_executive=$user_id_executive");
        }
        
        /* Jun 19, 2015 change
         * cho phép user điều phối khu vực TP HCM cùng update trên 1 đơn hàng
         * $user_id_create is Yii::app()->user->id;
         */
        $mAppCache = new AppCache();
        $aIdDieuPhoiBoMoi = $mAppCache->getArrayIdRole(ROLE_DIEU_PHOI);
        if(in_array($user_id_create, $aIdDieuPhoiBoMoi)){
            $sParamsIn  = implode(',', $aIdDieuPhoiBoMoi);
            $criteria->addCondition("t.user_id_create IN ($sParamsIn)");
        }elseif(!empty ($user_id_create)){
            $criteria->addCondition("t.user_id_create=$user_id_create");
        }
        // end Jun 19, 2015 change 
        
        return self::model()->find($criteria);
    }    
    
    /**
     * @Author: ANH DUNG Jun 25, 2017
     * @Todo: get model by $release_date
     * @param: $date_delivery Ngày tạo
     * @param: $user_id_create user tạo
     * @note:  Sửa lại hàm theo OOP 
     */
    public function getByDateDeliveryFix()
    {
        $this->user_id_executive = !empty($this->user_id_executive) ? $this->user_id_executive : 0 ;
        $criteria=new CDbCriteria; 
        $criteria->addCondition("t.date_delivery='$this->date_delivery'");
        $criteria->addCondition("t.user_id_executive=$this->user_id_executive");
        /* Jun 19, 2015 change
         * cho phép user điều phối khu vực TP HCM cùng update trên 1 đơn hàng
         * $user_id_create is Yii::app()->user->id;
         */
        $mAppCache = new AppCache();
        $aIdDieuPhoiBoMoi = $mAppCache->getArrayIdRole(ROLE_DIEU_PHOI);
        if(in_array($this->user_id_create, $aIdDieuPhoiBoMoi)){
            $sParamsIn  = implode(',', $aIdDieuPhoiBoMoi);
            $criteria->addCondition("t.user_id_create IN ($sParamsIn)");
        }elseif(!empty($this->agent_id)){// fix Jun 2517
            $criteria->addCondition("t.agent_id=$this->agent_id");
        }
        // end Jun 19, 2015 change 
        
        return self::model()->find($criteria);
    }     
    
    /**
     * @Author: ANH DUNG 12-29-2013
     * @Todo: get model by $release_date
     * @param: $date_delivery Ngày tạo
     * @param: $user_id_create user tạo
     * @Return: model or null
     */
    public static function getCarNumber($orders_id)
    {
        $criteria=new CDbCriteria; 
        $criteria->compare("t.orders_id", $orders_id);   
        $models = GasOrdersDetail::model()->findAll($criteria);
        $res = array();
        if(count($models)){
            foreach($models as $item){
                $car_number = trim($item->car_number);
                if($car_number!=''&& !in_array($car_number, $res)){
                    $res[$car_number] = $car_number;
                }
            }
        }
        return $res;
    }
    
    /**
     * @Author: ANH DUNG Sep 07, 2015
     * @Todo: chỗ tạo khách hàng o khu vuc tphcm may de minh thằng Chuong tạo nhé
     */
    public static function CanCreateCustomer() {
        $cRole      = MyFormat::getCurrentRoleId();
        $cProvince  = Yii::app()->user->province_id;
        if($cRole == ROLE_DIEU_PHOI && $cProvince == GasOrders::PROVINCE_HCM){
            $cUid = MyFormat::getCurrentUid();
            if(in_array($cUid, GasTickets::$UID_DIEU_PHOI_HCM_CUSTOMER) || in_array($cUid, GasTickets::getUidCreateKhApp())){
                return true;
            }
            return false;
        }
        return true;
    }
    
    /**
     * @Author: ANH DUNG Aug 02, 2016
     * @Todo: get 1 lần model customer and phone
     */
    public function getModelCustomerAndSale() {
        $from = time();
//        $sql ="SELECT * FROM `gas_users` `t` WHERE (t.id IN (114943, 862989, 49173, 862878, 49184, 475520, 137223, 238100, 63528, 59441, 49174, 699311, 27287, 36911, 802579, 705612, 863228, 768416, 29989, 62111, 41660, 59367, 49084, 826449, 241752, 49168, 198160, 36460, 818109, 808985, 65366, 28856, 360789, 240646, 59436, 29981, 30083, 850252, 59917, 29953, 30107, 826398, 818150, 789907, 874167, 30084, 63605, 63262, 357034, 852324, 48091, 34947, 41177, 137314, 30000, 323772, 852292, 862838, 41014, 862623, 49175, 874302)) AND (t.status=1)";
//        $res = Yii::app()->db->createCommand($sql)->execute();
        $aUid = array(GasLeave::UID_HEAD_GAS_BO => GasLeave::UID_HEAD_GAS_BO);
        foreach($this->rOrderDetail  as $mDetail){
            if(!empty($mDetail->customer_id)){
                $aUid[$mDetail->customer_id]    = $mDetail->customer_id;
            }
            if(!empty($mDetail->sale_id)){
                $aUid[$mDetail->sale_id]        = $mDetail->sale_id;
            }
        }
        $aUser = Users::getArrObjectUserByRole('', $aUid);
//        $to = time();
//        $second = $to-$from;
//        echo count($aUser).' done in: '.($second).'  Second  <=> '.($second/60).' Minutes';
        return $aUser ;
    }
    
    /**
     * @Author: ANH DUNG Oct 09, 2016
     * @Todo: get array customer_id of array detail
     */
    public function getArrayCustomerOld() {
        $aRes = array();
        foreach($this->rOrderDetail as $mDetail){
            $aRes[] = $mDetail->customer_id;
        }
        return $aRes;
    }
    public function getArrayDieuXe() {
        return Users::getSelectByRoleNotRoleAgent(ROLE_SCHEDULE_CAR);
    }

    /**
     * @Author: ANH DUNG Oct 10, 2016
     * @Todo: get detail cho điều xe có thể search
     */
    public function getDetailForDriver() {
        $criteria = new CDbCriteria();
        if(!empty($this->car_number)){
            $criteria->addCondition("t.car_number=$this->car_number" );
        }
        if(!empty($this->user_id_executive)){
            $criteria->addCondition("t.user_id_executive=$this->user_id_executive" );
        }
        if(!empty($this->driver_id)){
            $criteria->addCondition("t.driver_id=$this->driver_id" );
        }
        if(!empty($this->reason_wrong)){
            $criteria->addCondition("t.reason_wrong=$this->reason_wrong" );
        }
        $date_delivery = MyFormat::dateDmyToYmdForAllIndexSearch($this->date_delivery);
        $criteria->addCondition("t.date_delivery='$date_delivery'" );
        $criteria->order = "t.id ASC";
        return GasOrdersDetail::model()->findAll($criteria);
    }
    
    /**
     * @Author: ANH DUNG Oct 10, 2016
     * @Todo: check user có thể dùng chức năng in của điều xe không
     */
    public function canPrintDieuXe() {
        $aDieuXe = Users::getSelectByRoleNotRoleAgent(ROLE_SCHEDULE_CAR);
        $cUid   = MyFormat::getCurrentUid();
        $cRole  = MyFormat::getCurrentRoleId();
        $aRoleAllow = [ROLE_ADMIN, ROLE_ACCOUNTING];
        $ok = false;
        if(array_key_exists($cUid, $aDieuXe) || in_array($cRole, $aRoleAllow)){
            $ok = true;
        }
        return $ok;
    }
    
    
    /**
    * @Author: ANH DUNG Jul 23, 2016
    * @Todo: map post to model
    */
    public function windowGetPost($q, $mUser) {
        $this->customer_contact     = $q->phone_number;
        $this->note                 = $q->note;
        $this->user_id_executive    = $q->user_id_executive;
        $this->customer_id          = $q->customer_id;
        $this->b50                  = $q->b50;
        $this->b45                  = $q->b45;
        $this->b12                  = $q->b12;
        $this->b6                   = $q->b6;
        $this->user_id_create       = $mUser->id;
        $this->from_app             = GasOrders::FROM_PMBH;
        $this->validate();
    }
   
    /** for window
     * @Author: ANH DUNG Jan 23, 2017
     * @Todo: load record by date and user_id_executive
     * tìm xem có tạo record chưa, nếu chưa thì tạo mới, có rồi thì load lại để update detail
     */
    public static function getByDateAndUserExecutive($date_delivery, $user_id_executive, $user_id_create) {
        $model = self::getByDateDelivery($date_delivery, $user_id_executive, $user_id_create);
        if(is_null($model)){
            $model = new GasOrders('WindowAutoCreate');
            $model->date_delivery       = $date_delivery;
            $model->user_id_executive   = $user_id_executive;
            $model->type                = GasOrders::TYPE_DIEU_PHOI;
            $model->user_id_create      = $user_id_create;
            $model->agent_id            = $user_id_create;
            $model->save();
        }
        return $model;
    }
    
    /**
     * @Author: ANH DUNG Jan 23, 2017
     * @Todo: save record Xe tải from window PMBH và AppOrder
     * 1. khi confirm đơn hàng AppOrder ở web thì sẽ không thể tạo mới 1 AppOrder được
     * 2. chỗ tạo AppOrder mới chỉ xử lý cho lúc đặt hàng từ PMNH OR từ line CallCenter
     */
    public function saveWindowRecord() {
        try{
        foreach($this->rOrderDetail as $mDetail){
            if($mDetail->date_delivery == $this->date_delivery && $mDetail->user_id_executive == $this->user_id_executive && 
                    $mDetail->customer_id == $this->customer_id){
                if($this->from_app == GasOrders::FROM_PMBH){
                    throw new Exception('Đơn hàng đã có trên web, vui lòng vào web đặt hàng xe tải để cập nhật');
                /** Feb 03, 2017 -- không rõ chỗ này tại sao lại throw new Exception, close lại để chạy đã
                 *  @note: ngăn không cho cập nhật đơn hàng xe tải tỪ PMBH vì có liên quan đến nhiều xử lý
                 * để điều phối tự lên web update trên web
                 */
                }
                // Jun 29, 2017 báo đơn hàng trùng khi xác nhận đơn hàng đặt app
                throw new Exception('Đơn hàng đã có trên web, vui lòng vào web đặt hàng xe tải để cập nhật');
                $mDetail->quantity_50   = $this->b50;
                $mDetail->quantity_45   = $this->b45;
                $mDetail->quantity_12   = $this->b12;
                $mDetail->quantity_6    = $this->b6;
                $aUpdate = ['quantity_50', 'quantity_45', 'quantity_12', 'quantity_6'];
                $mDetail->update($aUpdate);
                return $mDetail;
            }
        }
        // nếu không có record nào dc update thì là add new
        $mDetail =  $this->addNewRecordDetail();
        $this->setFlagUpdateCarAgain();// Add fix Jan0519
        if($this->from_app == GasOrders::FROM_APP){
            return $mDetail;
        }
        // Feb 01, 2017 make one AppOrder với request từ PMBH OR từ line CallCenter
        $mAppOrder                  = $mDetail->makeNewAppOrder($this);
        $mAppOrder->obj_detail_id   = $mDetail->id;
        $mAppOrder->save();
        
        return $mDetail;
        } catch (Exception $ex) {
            throw new Exception($ex->getMessage());
        }
    }
    
    /**
     * @Author: ANH DUNG Jan 23, 2017
     * @Todo: add new detail from window
     */
    public function addNewRecordDetail() {
        $mDetail = new GasOrdersDetail();
        $mDetail->orders_id             = $this->id;
        $mDetail->customer_contact      = $this->customer_contact;
        $mDetail->user_id_create        = $this->user_id_create;
        $mDetail->user_id_executive     = $this->user_id_executive;
        $mDetail->agent_id              = $this->agent_id;
        $mDetail->type                  = $this->type;
        $mDetail->date_delivery         = $this->date_delivery;
        $mDetail->customer_id           = $this->customer_id;
        $mDetail->lpg_ton               = 0;
        $mDetail->quantity_50           = $this->b50;
        $mDetail->quantity_45           = $this->b45;
        $mDetail->quantity_12           = $this->b12;
        $mDetail->quantity_6            = $this->b6;
        $mDetail->other_material_id     = 0;
        $mDetail->quantity_other_material = 0;
        $mDetail->work_content          = '';
        $mDetail->created_by            = $this->mAppUserLogin ? $this->mAppUserLogin->id : 0;
        if(empty($mDetail->created_by)){
            // trong hàm makeOrderCar ở model GasAppOrder không tiện find ra model user nên thêm biến uid_login để lấy luôn id user cho nhanh, không cần find ra model
            $mDetail->created_by = $this->uid_login;
        }
        $mDetail->note                  = $this->note;
        $mDetail->from_app              = $this->from_app;// 1: web, 2: from app
        
        if($mDetail->customer){ // loại KH 1: Bình Bò, 2: Mối, dùng cho thống kê doanh thu
            //'is_maintain',// vì cột is_maintain không dùng trong loại KH của thẻ kho nên ta sẽ dùng cho 1: KH bình bò, 2. KH mối
            $mDetail->type_customer  = $mDetail->customer->is_maintain;
            $mDetail->sale_id        = $mDetail->customer->sale_id;                            
        }
        $this->mapLatestOrder($mDetail);
        $mDetail->created_date      = date('Y-m-d H:i:s');
        $mDetail->save();
        return $mDetail;
    }
    
    /**
     * @Author: ANH DUNG Feb 22, 2017
     * @Todo: gắn các thông tin cũ của KH vào order mới
     */
    public function mapLatestOrder(&$mDetail) {
        $mOrderDetailLatest = GasOrdersDetail::getLastOrderOfCustomer($this->customer_id);
        if($mOrderDetailLatest){
            $aRes = [];
            $mDetail->getPriceCustomer($aRes);
            $mDetail->work_content = $mOrderDetailLatest->work_content. '. '. $mDetail->work_content .'. '. $this->note;// điều phối y/c tạo
//            $mDetail->lpg_ton               = $mOrderDetailLatest->lpg_ton > 0 ? ActiveRecord::formatNumberInput($mOrderDetailLatest->lpg_ton) : '';
//            $mDetail->quantity_50           = $mOrderDetailLatest->quantity_50 > 0 ? ActiveRecord::formatNumberInput($mOrderDetailLatest->quantity_50) : 0;
//            $mDetail->quantity_45           = $mOrderDetailLatest->quantity_45 > 0 ? ActiveRecord::formatNumberInput($mOrderDetailLatest->quantity_45) : 0;
//            $mDetail->quantity_12           = $mOrderDetailLatest->quantity_12 > 0 ? ActiveRecord::formatNumberInput($mOrderDetailLatest->quantity_12) : 0;
//            $mDetail->quantity_6            = $mOrderDetailLatest->quantity_6 > 0 ? ActiveRecord::formatNumberInput($mOrderDetailLatest->quantity_6) : 0;
//            $mDetail->quantity_other_material = $mOrderDetailLatest->quantity_other_material > 0 ? ActiveRecord::formatNumberInput($mOrderDetailLatest->quantity_other_material) : '';
            $mDetail->note = $mOrderDetailLatest->note;
        }
    }
    
    /**
     * @Author: ANH DUNG Jan 27, 2017
     * @Todo: add multi AppOrder in one query
     */
    public function doAddMultiAppOrder() {
        /** 1. find all order detail với những ID not in aIdDetailNotUpdateApp để đỡ phải xử lý so sánh
         *  2. foreach map order_detail_id vào model -> cái này có thể không chính xác dc 100%
         *  vì có thể trùng KH, nhưng ko thể trùng SL đc
         */
//        sleep(1);// chưa rõ cần không nữa
        $aOrderDetail = $this->doAddMultiAppOrderGetDetail();
        foreach($aOrderDetail as $mOrderDetail){
            $mOrderDetail->quantity_other_material = ($mOrderDetail->quantity_other_material > 0 ? $mOrderDetail->quantity_other_material : 0)*1;
            $key_compare = $mOrderDetail->customer_id.$mOrderDetail->quantity_50.$mOrderDetail->quantity_45.$mOrderDetail->quantity_12.$mOrderDetail->quantity_6.$mOrderDetail->quantity_other_material;
            if(isset($this->aAppOrderInsert[$key_compare])){
                $mAppOrder = $this->aAppOrderInsert[$key_compare];
                $mAppOrder->obj_detail_id = $mOrderDetail->id;
                $mAppOrder->save();
            }
        }

//        foreach($this->aAppOrderInsert as $key =>$a){
//            echo $key.'<br>';
//        }
//        die;
    }
    
    /**
     * @Author: ANH DUNG Jan 31, 2017
     * @Todo: add agent AppOrder, vì với đơn hàng đại lý chỉ xử lý tạo 1 AppOrder
     */
    public function doAddAgentAppOrder() {
        $mAppOrder              = $this->getAgentAppOrderById();
        $mAppOrder->aOrderCar   = $this->doAddMultiAppOrderGetDetail();
        $mAppOrder->setArrayOrderCarDetailToJson();
        $mAppOrder->setProvinceIdAgent();
        $mAppOrder->order_detail_note = $this->note;
        if(empty($mAppOrder->id)){
            $mAppOrder->save();
        }else{
            $mAppOrder->setUserUpdate();
            $mAppOrder->update();
        }
    }
    
    /**
     * @Author: ANH DUNG Jan 31, 2017
     * @Todo: get model Agent AppOrder by id OrderCar
     */
    public function getAgentAppOrderById() {
        $mAppOrder                  = new GasAppOrder();
        $mAppOrder->obj_id          = $this->id;
        $mAppOrder->type            = GasAppOrder::DELIVERY_BY_CAR;
        $mAppOrder                  = $mAppOrder->getByOrderIdAndType();
        if(is_null($mAppOrder)){
            $mAppOrder                      = new GasAppOrder();
            $mAppOrder->user_id_executive   = $this->user_id_executive;
            $mAppOrder->status              = GasAppOrder::STATUS_CONFIRM;
            $mAppOrder->uid_login           = MyFormat::getCurrentUid();
            $mAppOrder->uid_confirm         = $mAppOrder->uid_login;
            $mAppOrder->obj_id              = $this->id;
            $mAppOrder->type_customer       = CUSTOMER_IS_AGENT;
            $mAppOrder->type                = GasAppOrder::DELIVERY_BY_CAR;
            $mAppOrder->date_delivery       = $this->date_delivery;
            $mAppOrder->type_of_order_car   = GasOrders::TYPE_AGENT;
//            => ko set dc kieu nay: do còn xđ province_id_agent, nên set kiểu này sẽ bị sai $mAppOrder->agent_id = $mAppOrder->carAgentId();// Aug1718 fix đơn ĐL của Kho PTân
        }
        $mAppOrder->order_detail_note   = $this->note;
        $mAppOrder->customer_id         = $this->agent_id;
        $mAppOrder->agent_id            = $this->agent_id;
        return $mAppOrder;
    }
    
    /**
     * @Author: ANH DUNG Jan 27, 2017
     * @Todo: get những order detail add new của Order đang cập nhật
     * những detail cũ hoặc delete thì không lấy, giảm record so sánh
     */
    public function doAddMultiAppOrderGetDetail() {
        if(count($this->aIdDetailNotUpdateApp) < 1){
            return [];
        }
        $criteria = new CDbCriteria();
        $sParamsIn = implode(',', $this->aIdDetailNotUpdateApp);
        $criteria->addCondition("t.id NOT IN ($sParamsIn)");
        $criteria->addCondition("t.orders_id=".$this->id);
        return GasOrdersDetail::model()->findAll($criteria);
    }
    
    /**
     * @Author: ANH DUNG Jan 31, 2017
     * @Todo: tách ra 2 luồng xử lỷ với đơn hàng điều phối và đơn hàng của đại lý
     */
    public function handleFlowUpdateToAppOrder($mAppOrder) {
        if($this->type == GasOrders::TYPE_DIEU_PHOI){
            $mAppOrder->aOrderDetailIdDelete = $this->aOrderDetailIdDelete;
            $mAppOrder->deleteByOrderDetail();// xử lý xóa Jan 27, 2017
            $this->doAddMultiAppOrder();// xử lý thêm mới Jan 27, 2017 
            // Xử lý update đã làm trong hàm foreach rồi
        }elseif($this->type == GasOrders::TYPE_AGENT){
            // chưa xử lý, xử lý tạo mới trên web xong rồi quay ra xử lý update
            $this->aIdDetailNotUpdateApp = array(-1);
            $this->doAddAgentAppOrder();// done test Feb 01, 2017, xử lý add và update AppOrder của agent
        }
    }

    /**
     * @Author: ANH DUNG Jan 31, 2017
     * @Todo: handle save to AppOrder when create new GasOrders from web
     * for dieu phoi va dai ly
     */
    public function handleFlowCreateToAppOrder() {
        $this->aIdDetailNotUpdateApp = array(-1);
        if($this->type == GasOrders::TYPE_DIEU_PHOI){
            $this->doAddMultiAppOrder();// done test Jan 27, 2017
        }elseif($this->type == GasOrders::TYPE_AGENT){
            $this->doAddAgentAppOrder();// done test Feb 01, 2017 
        }
    }
    
    /**
     * @Author: ANH DUNG May 03, 2017
     * @Todo: tự động sinh đơn hàng của đại lý
     */
    public function autoMakeOrders() {
        try{
        /* 1. get thẻ kho trong 3 ngày gần nhất của 1 list đại lý
         * 2. build insert
         */
        $this->getListAgentNeedMakeOrder();
        if(count($this->aAgent) < 1){
            return ;
        }

        $aStoreCard     = $this->getStorecardList();
        $date_delivery  = date('Y-m-d');// ngày giao
//        $date_delivery  = '2017-05-09';//  only dev test
        foreach($aStoreCard as $agent_id => $aInfo){
            if(empty($agent_id)){
                continue;
            }
            $this->makeOneOrder($agent_id, $aInfo, $date_delivery);
        }
        
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }
    
    /**
     * @Author: ANH DUNG May 03, 2017
     * @Todo: make one Order
     */
    public function makeOneOrder($agent_id, $aInfo, $date_delivery) {
        $mOrderOld = $this->aModelOrder[$agent_id];
        $model = new GasOrders('CronAutoMakeOrders');
        $model->user_id_create      = $agent_id;
        $model->user_id_executive   = $mOrderOld->user_id_executive;
        $model->type                = GasOrders::TYPE_AGENT;
        $model->date_delivery       = $date_delivery;
        $model->agent_id            = $agent_id;
        $model->save();
        $model->makeOneOrderDetail($aInfo);
    }
    
    public function makeOneOrderDetail($aInfo) {
        $aRowInsert = [];
        $customer_id = $type_customer = $sale_id = $lpg_ton = $quantity_other_material = 0;
        $work_content = ''; $cUid = GasConst::UID_ADMIN;
        $created_date   = date('Y-m-d H:i:s');
        foreach($aInfo as $materials_type_id => $aInfo1){
            foreach($aInfo1 as $materials_id => $qty){
                $quantity_50 = $quantity_45 = $quantity_12 = $quantity_6 = 0; $note = '';
                switch ($materials_type_id) {
                    case MATERIAL_TYPE_BINHBO_50:
                        $quantity_50        = $qty;
                        break;
                    case MATERIAL_TYPE_BINHBO_45:
                        $quantity_45        = $qty;
                        break;
                    case MATERIAL_TYPE_BINH_12:
                        $quantity_12        = $qty;
                        break;
                    case MATERIAL_TYPE_BINH_6:
                        $quantity_6         = $qty;
                        break;
                    case GasMaterialsType::MATERIAL_BINH_4KG:
                        $quantity_6         = $qty;
                        $note               = 'Lấy bình 4kg';
                        break;
                    default:
                        break;
                }
                $other_material_id  = $materials_id;
                $aRowInsert[]="('$this->id',
                        '$this->user_id_create', 
                        '$this->user_id_executive', 
                        '$this->agent_id', 
                        '$this->type', 
                        '$this->date_delivery', 
                        '$customer_id',
                        '$type_customer',
                        '$sale_id',
                        '$lpg_ton',
                        '$quantity_50',
                        '$quantity_45',
                        '$quantity_12',
                        '$quantity_6',
                        '$other_material_id',
                        '$quantity_other_material',
                        '$work_content',
                        '$cUid',
                        '$note',
                        '$created_date'
                        )";
            }
        }
        
        $tableName = GasOrdersDetail::model()->tableName();
        $this->updateOrderDetailRunInsert($aRowInsert, $tableName);
    }

    
    /**
     * @Author: ANH DUNG May 03, 2017
     * @Todo: get list thẻ kho bán trong 3 ngày gần đây của mỗi đại lý
     */
    public function getStorecardList() {
        $criteria = new CDbCriteria();
        $sParamsIn = implode(',', CmsFormatter::$MATERIAL_TYPE_BINHBO_OUTPUT);
        $criteria->addCondition("t.materials_type_id IN ($sParamsIn)");
        $sParamsIn = implode(',', $this->aAgent);
        $criteria->addCondition("t.user_id_create IN ($sParamsIn)");
        // lấy những đơn hàng trươc đây 3 ngày
        $today = date('Y-m-d');
        $dateCheck = MyFormat::modifyDays($today, $this->amountDaysMakeOrder, '-');
        $criteria->addCondition("t.date_delivery >= '$dateCheck'");
        // lấy những thẻ kho xuất bán
        $sParamsIn = implode(',', CmsFormatter::$TYPE_IN_OUT_OUTPUT_STATISTIC);
        $criteria->addCondition("t.type_in_out IN ($sParamsIn)");

        $criteria->select   = 'sum(qty) as qty, t.user_id_create, t.materials_type_id, t.materials_id';
        $criteria->group    = "t.user_id_create, t.materials_type_id, t.materials_id";
        $mRes = GasStoreCardDetail::model()->findAll($criteria);
        $aRes = [];
        foreach($mRes as $item){
            $aRes[$item->user_id_create][$item->materials_type_id][$item->materials_id] = $item->qty;
        }
        return $aRes;
    }
    
    /**
     * @Author: ANH DUNG May 03, 2017
     * @Todo: lấy danh sách đại lý cần tạo tự động đơn hàng
     */
    public function getListAgentNeedMakeOrder() {
        $criteria = new CDbCriteria();
        $today = date('Y-m-d');
        $dateCheck = MyFormat::modifyDays($today, $this->amountDaysMakeOrder, '-');
        $criteria->addCondition("t.date_delivery='$dateCheck'");
        $criteria->addCondition('t.type=' . GasOrders::TYPE_AGENT);
        $models = GasOrders::model()->findAll($criteria);
        foreach($models as $item){
//            if($item->id == 22730 || $item->id == 22732){// only dev test
                $this->aAgent[]         = $item->agent_id;
                $this->aModelOrder[$item->agent_id] = $item;
//            }
        }
    }
    
    /**
     * @Author: ANH DUNG May 23, 2017
     * @Todo: xử lý CallCenter tạo đơn hàng xe tải trên web, không thông qua PM window của Nguyên nữa
     */
    public function callCenterOrderCar($q, &$json) {
        try{
        $today = date('d-m-Y');
        if($q->date_delivery == $today){
            $date_delivery = MyFormat::getNextDay();// Aug2617 không fix cứng lấy đơn hàng của ngày kế tiếp
        }else{
            $date_delivery = MyFormat::dateDmyToYmdForAllIndexSearch($q->date_delivery);// Aug2617 format d-m-Y to Y-m-d
        }
        
        $mOrderCar = GasOrders::getByDateAndUserExecutive($date_delivery, $q->user_id_executive, $this->mAppUserLogin->id);
        if(empty($mOrderCar->id)){
            throw new Exception('Dữ liệu không hợp lệ, vui lòng kiểm tra lại');
        }
        $mOrderCar->date_delivery   = $date_delivery;
        $mOrderCar->mAppUserLogin   = $this->mAppUserLogin;
        $mOrderCar->scenario        = 'WindowCreate';
        $mOrderCar->windowGetPost($q, $this->mAppUserLogin);
        if($mOrderCar->hasErrors()){
            throw new Exception(HandleLabel::FortmatErrorsModel($mOrderCar->getErrors()));
        }
        $mOrderCar->saveWindowRecord();
        
        $mOrderCar->update(['update_again_car', 'last_update_by', 'last_update_time']);
        $json['msg'] = 'Tạo mới đơn hàng xe tải thành công';
        } catch (Exception $ex) {
            throw new Exception($ex->getMessage());
        }
    }
    
    /** @Author: ANH DUNG Jun 25, 2017
     */
    public function getAgentName() {
        $mUser = $this->rAgent;
        if($mUser){
            return $mUser->first_name;
        }
        return '';
    }
    public function getUserCreated() {
        $mUser = $this->rUserIdCreate;
        if($mUser){
            return $mUser->first_name;
        }
        return '';
    }
    /** @Author: ANH DUNG Aug 19, 2017
     * @Todo: get json serarch User
     */
    public function formatJsonUserAutocomplete($listDataUser) {
        $returnVal = [];
        foreach($listDataUser as $user_id => $first_name){
            $nameVi = MyFunctionCustom::remove_vietnamese_accents($first_name);
            $returnVal[] = array(
                'label'         => $first_name.' - '.$nameVi,
                'value'         => $first_name,
                'id'            => $user_id,
            );
        }
        return CJSON::encode($returnVal);
    }
    
    /** @Author: ANH DUNG Jan 05, 2019
     *  @Todo: set flag update car nếu có đơn mới, mà đã điều xe rồi
     **/
    public function setFlagUpdateCarAgain() {
        if(!empty($this->user_id_update_car_number)){
            $this->update_again_car = GasOrders::UPDATE_CAR_AGAIN;
            $this->update(['update_again_car']);
        }
    }
    
}
