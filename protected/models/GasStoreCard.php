<?php

class GasStoreCard extends BaseSpj
{
    public $employee_maintain_id=0, $file_name, $list_id_image=[], $autocomplete_name, $autocomplete_name_sale, $materials_name, $autocomplete_name_1;
    public $MAX_ID, $aModelStoreCardDetail=array();
    public $agent_id, $ext_materials_id;
    public $ext_is_new_customer=1, $ext_is_maintain, $ext_sale_id, $modelUser, $autocomplete_name_street, $checkbox_view_general;

    public $ext_type_customer, $ext_type_gas_vo, $ext_materials_type_id, $count_row, $province_id;
    public $notViewWarehouse = 0, $allowCreate = true, $statistic_month, $statistic_year, $errCheckValidQtyKhoAndAgent, $mAgent = null, $sendSms=true, $fromWebUpdate = false;

    public static $days_allow_update = 10; // số ngày cho phép admin cập nhật thẻ kho
    // không dùng ở đây nữa mà đã chuyển qua Yii::app()->params
    public static $days_allow_delete = 20; // số ngày cho phép admin xóa thẻ kho
    // không dùng ở đây nữa mà đã chuyển qua Yii::app()->params
    public static $days_agent_allow_update = 1; // không dùng config ở đây nữa, mà dùng chung vs cashbook - số ngày cho phép đại lý cập nhật thẻ kho
    public static $days_agent_allow_UpdateCollectionCustomer = 60; // số ngày cho phép đại lý cập nhật thu tiền KH
    // không dùng ở đây nữa mà đã chuyển qua Yii::app()->params
    //LOC001
    public $qtykg_from, $qtykg_to, $channel_id;
    public $jsonDetail; // DuongNV Jul0519 Lưu json của list vật tư 
    public $mTruckPlanDetail = null; // DungNT Jul3019
    public static $TYPE_CUSTOMER = array(
        CUSTOMER_HO_GIA_DINH    => 'HO_GD',
        STORE_CARD_KH_BINH_BO   => 'BINH_BO',
        STORE_CARD_KH_MOI       => 'BINH_MOI',
    );
    
    const DA_THU_TIEN           = 1;
    const CHUA_THU_TIEN         = 2;
    const WAREHOUSE_NEW         = 2;
    const REASON_DIEU_SAI       = 1;
    const REASON_CON_GAS        = 2;
    const REASON_GIA_CAO        = 3;
    const REASON_CHUYEN_KH_LE   = 4;
    const REASON_2_NGUOI_GOI    = 5;
    const REASON_KHONG_CHO_NO_VO = 6;
    const REASON_OTHER          = 7;
    const REASON_ALL            = 8;
    const REASON_NO_VO          = 9;
    const REASON_KHONG_CHO_NO_TIEN = 10;
    const REASON_DUPLICATE      = 11;
    
    const REASON_DI_VANG        = 12;// KH đi vắng
    const REASON_CALL_WRONG     = 13;// KH gọi nhầm
    const REASON_TEST_APP       = 14;
    const REASON_CHANGE_CAR     = 15;// Chuyển xe khác giao
    
    const BC_TONGQUAT    = 1;
    const BC_CHITIET    = 2;
    
    const GIFT_CHEN_SU          = 50;  // Chén sứ        KMC0004
    const GIFT_TO_SU            = 863; // Tô sứ          KMT0006
    const GIFT_LY_CO_QUAI       = 288; // Ly có quai     KM050
    const GIFT_LY_UONG_BIA      = 793; // Ly uống bia    KML0005
    
     /**
     * @Author: ANH DUNG Nov 05, 2016
     */
    public function getStatusFalse($getAll=false) {
        $aRes = array(
            self::REASON_DUPLICATE      => 'Điều phối điều 2 lần',
            self::REASON_DI_VANG        => 'KH đi vắng',
            self::REASON_CALL_WRONG     => 'KH gọi nhầm',
            self::REASON_DIEU_SAI       => 'Điều sai',
            self::REASON_CON_GAS        => 'KH còn Gas',
            self::REASON_KHONG_CHO_NO_TIEN => 'Không cho KH nợ tiền',
            self::REASON_NO_VO          => 'KH nợ vỏ',
            self::REASON_GIA_CAO        => 'Giá cao',
            self::REASON_CHUYEN_KH_LE   => 'Chuyển KH lẻ',
            self::REASON_2_NGUOI_GOI    => 'Hai người gọi',
            self::REASON_KHONG_CHO_NO_VO => 'Không cho nợ vỏ',
            self::REASON_CHANGE_CAR     => 'Chuyển xe khác giao',
            self::REASON_TEST_APP       => 'Test App',
//            self::REASON_OTHER          => 'Lý do khác',
        );
        if($getAll){
            $aRes[self::REASON_ALL] = 'Tất cả lý do không lấy hàng';
        }
        return $aRes;
    }
    
    
    public static $ARR_COLLECTION_CUSTOMER = array(
        self::DA_THU_TIEN=>'Đã Thu',
        self::CHUA_THU_TIEN=>'Chưa Thu',
    );
    
    // for thống kê /admin/gasreports/DailyInternal
    const TYPE_GAS = 1;
    const TYPE_VO = 2;
    const TYPE_NOT_GAS_VO = 3;
    public static $ARR_TYPE_GAS_VO = array(
        self::TYPE_GAS=>'Gas',
        self::TYPE_VO=>'Vỏ',
        self::TYPE_NOT_GAS_VO=>'Vật Tư Khác Ngoài Gas, Vỏ',
    );
    
//    const NOT_CUSTOMER_BO_MOI = 3;
    
    public static $DAILY_INTERNAL_TYPE_CUSTOMER = array(
        STORE_CARD_KH_BINH_BO=>'Bình Bò',
        STORE_CARD_KH_MOI=>'Bình Mối',
        CUSTOMER_HO_GIA_DINH=>'Hộ Gia Đình',
        CUSTOMER_IS_AGENT=>'Đại Lý',
        CUSTOMER_OTHER=>'Khác',
    );
    // for thống kê /admin/gasreports/DailyInternal
    
    // Apr 20, 2015 for thống kê /admin/gasreports/output_change
    const TYPE_MOI_12   = 2;
    const TYPE_HO_GD_12 = 3;
    public static $TYPE_OUTPUT_CHANGE = array(
        STORE_CARD_KH_BINH_BO=>'Bình Bò',
        GasStoreCard::TYPE_MOI_12=>'Bình Mối 12',
        GasStoreCard::TYPE_HO_GD_12=>'Hộ Gia Đình 12',
    );
    // Apr 20, 2015 for thống kê /admin/gasreports/output_change
    const CREATE_FROM_WINDOW            = 2; // for pay_now column
    const CREATE_FROM_ORDER_AUTO        = 3; // for pay_now column GasStoreCardApi autoGenStoreCardFromOrder
    const CREATE_AUTO_FROM_APP_ORDER    = 4; // Jan 19, 2017 tạo tự động khi KH bò mối đặt hàng qua app
    const CREATE_AUTO_SUB_STORECARD     = 5; // Aug05, 2017 tạo tự động tạo thẻ kho cho đại lý nội bộ

    const ROAD_ROUTE_NORMAL         = 1; // Jul 28, 2016 tuyến thông thường
    const ROAD_ROUTE_FIX            = 2; // tuyến cố định
    const MAX_B12                   = 5; // max sl b12 1 xe có thể chở
    
    public function getArrRoadRoute(){
        return array(
            GasStoreCard::ROAD_ROUTE_NORMAL     => 'Tuyến thông thường',
            GasStoreCard::ROAD_ROUTE_FIX        => 'Tuyến cố định',
        );
    }
    
    public function getRoadRoute(){
        $aRoadRoute = $this->getArrRoadRoute();
        return isset($aRoadRoute[$this->road_route]) ? $aRoadRoute[$this->road_route] : "";
    }

    /** @note_change_field
     * ghi chú lại những thay đổi chức năng của 1 cột
     * @date: 2016-07-12
     * cột pay_now sẽ có value = 2 nghĩa là record dc tạo từ windown của điều phối
     * trước đây cột này sử dụng để check KH đã trả tiền chưa, nhưng thời điểm này ko dùng nữa
     */
    public static function model($className=__CLASS__)
    {
            return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
            return '{{_gas_store_card}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        return array(
        array('date_delivery, type_in_out', 'required', 'on'=>'create_customer_new'),
        array('date_delivery, type_in_out', 'required', 'on'=>'update'),
        array('date_delivery, type_in_out, customer_id', 'required', 'on'=>'create_customer_old'),
//        array('delivery_person, date_delivery, type_in_out,store_card_real', 'required', 'on'=>'create_customer_new'),
//        array('delivery_person, date_delivery, type_in_out,store_card_real', 'required', 'on'=>'update'),
//        array('customer_id, delivery_person, date_delivery, type_in_out,store_card_real', 'required', 'on'=>'create_customer_old'),
            
        
        array('date_delivery, type_in_out', 'required', 'on'=>'update_customer_pay_now'),            
        array('date_delivery, type_in_out', 'required', 'on'=>'create_customer_pay_now'),
        array('delivery_person', 'length', 'max'=>80),
        array('note', 'length', 'max'=>500),
        array('id, store_card_no, customer_id, type_user, type_store_card, type_in_out, delivery_person, date_delivery, store_card_real, note, created_date', 'safe'),			
        array('user_id_create,agent_id, ext_materials_id, ext_is_new_customer,autocomplete_name_street', 'safe'),
        array('notViewWarehouse, checkbox_view_general,date_from,date_to,ext_is_maintain,statistic_month,statistic_year,ext_sale_id,value_before_update', 'safe'),
        array('collection_customer,collection_customer_note,ext_type_gas_vo,ext_type_customer', 'safe'),
        array('ext_materials_type_id,pay_now,price_binh_bo,receivables_customer,errCheckValidQtyKhoAndAgent', 'safe'),
        array('employee_maintain_id, province_id_agent, uid_login, update_note,update_note_by,update_note_time', 'safe'),
        array('reason_false, road_route, car_id, driver_id_1, driver_id_2, phu_xe_1, phu_xe_2', 'safe'),// Jul 25, 2016 tính sản lượng cho xe
        array('user_id_create, customer_id', 'required', 'on'=>'WindowCreate'),// api window create store card
        array('user_id_create', 'required', 'on'=>'WindowCreateHgd'),// api window create store card
        array('customer_id, type_in_out, date_delivery, user_id_create', 'required', 'on'=>'ApiCreate'),// api app phone create store card
        );
    }
    
    /**
     * @return array relational rules.
     */
    public function relations()
    {
        return array(
            'customer' => array(self::BELONGS_TO, 'Users', 'customer_id'),
            'rCustomer' => array(self::BELONGS_TO, 'Users', 'customer_id'),
            'materials' => array(self::BELONGS_TO, 'GasMaterials', 'ext_materials_id'),
            'agent' => array(self::BELONGS_TO, 'Users', 'agent_id'),
            'user_create' => array(self::BELONGS_TO, 'Users', 'user_id_create'),
            'StoreCardDetail' => array(self::HAS_MANY, 'GasStoreCardDetail', 'store_card_id'),
            'rStoreCardDetail' => array(self::HAS_MANY, 'GasStoreCardDetail', 'store_card_id'),
            'rSale' => array(self::BELONGS_TO, 'Users', 'ext_sale_id'),
            'rUidLogin' => array(self::BELONGS_TO, 'Users', 'uid_login'),
            'rLastUpdate' => array(self::BELONGS_TO, 'Users', 'last_update_by'),
            'rAgent' => array(self::BELONGS_TO, 'Users', 'user_id_create'),
            'rFile' => array(self::HAS_MANY, 'GasFile', 'belong_id',
                'on'=>'rFile.type='.  GasFile::TYPE_9_STORECARD,
                'order'=>'rFile.id ASC',
            ),
            'rAppOrder' => array(self::HAS_ONE, 'GasAppOrder', 'store_card_id_gas'),
            'rEmployeeMaintain' => array(self::BELONGS_TO, 'Users', 'employee_maintain_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'store_card_no' => 'Mã Thẻ Kho',
            'customer_id' => 'Khách Hàng',
//			'customer_id' => 'Nhà Cung Cấp',
            'type_user' => 'Loại User',
            'type_store_card' => 'Loại Thẻ Kho',
            'type_in_out' => 'Loại Nhập Xuất', // Loại Nhập, loại xuất ex: (not sure)1: Nhập nội bộ, 2: nhập mua, 3 Xuất nội bộ, 4 xuất bán...
            'delivery_person' => 'Người Giao',
            'date_delivery' => 'Ngày',
            'store_card_real' => 'Mã Phiếu Thực Tế',
            'note' => 'Ghi Chú',
            'created_date' => 'Ngày Tạo',
            'materials_name' => 'Vật Tư',
            'agent_id' => 'Đại Lý',
            'ext_materials_id' => 'Mã Vật Tư',
            'ext_is_new_customer' => 'Khách Hàng',
            'user_id_create' => 'Đại Lý',
            'checkbox_view_general' => 'Thống Kê Tổng Hợp',
            'date_from' => 'Từ Ngày',
            'date_to' => 'Đến Ngày',
            'ext_is_maintain' => 'Loại KH',
            'statistic_month' => 'Chọn Tháng',
            'statistic_year' => 'Chọn Năm',
            'ext_sale_id' => 'Sale',
            'value_before_update' => 'Trước Cập Nhật',
            'collection_customer' => 'Tiền thu khách hàng',
            'collection_customer_note' => 'Ghi chú thu tiền',
            'ext_type_gas_vo' => 'Loại',
            'ext_type_customer' => 'Loại KH',
            'ext_materials_type_id' => 'Loại Vật Tư',
            'collection_customer_date' => 'Ngày Nhập Thu Tiền',
            'collection_customer_date_update' => 'Ngày Chỉnh Sửa Thu Tiền',
            'last_update_time' => 'Ngày Sửa Thẻ Kho',

            'pay_now' => 'Đã Thanh Toán Tiền',
            'price_binh_bo' => 'Giá Bình Bò',
            'receivables_customer' => 'Phải Thu Khách Hàng',                
            'update_note' => 'Ghi chú',
            'update_note_by' => 'Người cập nhật',
            'update_note_time' => 'Ngày cập nhật',
            'uid_login' => 'Người tạo',
            'car_id' => 'Số xe',
            'driver_id_1' => 'Tài xế 1',
            'driver_id_2' => 'Tài xế 2',
            'phu_xe_1' => 'Phụ xe 1',
            'phu_xe_2' => 'Phụ xe 2',
            'road_route' => 'Tuyến đường',
            'reason_false' => 'Lý do không giao gas',
            'province_id_agent' => 'Tỉnh',
            'last_update_by' => 'Người sửa',
            'notViewWarehouse' => 'Không xem kho',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        $criteria   = new CDbCriteria;
        $aWith      = [];
        $cRole      = MyFormat::getCurrentRoleId();
        $cUid       = MyFormat::getCurrentUid();
        $this->getCriteria($criteria);
        $mAppCache  = new AppCache();

        if( ( !empty($this->ext_materials_id) || !empty($this->ext_materials_type_id) || !empty($this->ext_sale_id)) ){
            $aWith[] = 'StoreCardDetail';
        }

        if(!empty($this->ext_materials_id)){
            $criteria->addCondition('StoreCardDetail.materials_id=' . $this->ext_materials_id);
        }
        if(!empty($this->ext_materials_type_id)){
            $criteria->addCondition('StoreCardDetail.materials_type_id=' . $this->ext_materials_type_id);
        }
        if(!empty($this->ext_sale_id)){
            $criteria->addCondition('StoreCardDetail.sale_id=' . $this->ext_sale_id);
        }
        if(!empty($this->reason_false)){
            if($this->reason_false == self::REASON_ALL){
                $criteria->addCondition('t.reason_false IS NOT NULL');
            }else{
                $criteria->addCondition('t.reason_false='.$this->reason_false);
            }
        }
        // user_id_create là agent_id do đặt nhầm tên
        if($cRole==ROLE_SUB_USER_AGENT){
            $criteria->compare('t.user_id_create',Yii::app()->user->parent_id);
        }else{
            if(!empty($this->agent_id))
                $criteria->compare('t.user_id_create', $this->agent_id);
        }
        $aIdDieuPhoiBoMoi = $mAppCache->getArrayIdRole(ROLE_DIEU_PHOI);
        $sParamsIn  = implode(',', $aIdDieuPhoiBoMoi);
        
        if(isset($_GET['call_center']) && $_GET['call_center']==1){
//            if(in_array($cUid, GasOrders::$ZONE_HCM)){
////                $sParamsIn = implode(',', GasOrders::$ZONE_HCM);
//            }elseif(in_array($cUid, GasOrders::$ZONE_MIEN_TAY)){
//                $sParamsIn = implode(',', GasOrders::$ZONE_MIEN_TAY);
//            }else{
//                $sParamsIn = implode(',', GasTickets::$UID_DIEU_PHOI);
//            }// Close Jun 11, 2017
            $criteria->addCondition("t.uid_login IN ($sParamsIn) AND pay_now<>".GasStoreCard::CREATE_FROM_ORDER_AUTO);
        }
        if(isset($_GET['call_center']) && $_GET['call_center']==2){
//            $sParamsIn = implode(',', GasOrders::$ZONE_HCM);// Close Jun 11, 2017
            $criteria->addCondition("t.uid_login IN ($sParamsIn) AND pay_now=".GasStoreCard::CREATE_FROM_ORDER_AUTO);
        }

        $session=Yii::app()->session;
        // danh sách những đại lý giới hạn cho user 
        if(isset($session['LIST_AGENT_OF_USER']) && count($session['LIST_AGENT_OF_USER'])){            
//                $criteria->addInCondition('t.user_id_create', $session['LIST_AGENT_OF_USER']); 
            $sParamsIn = implode(',', $session['LIST_AGENT_OF_USER']);
            $criteria->addCondition("t.user_id_create IN ($sParamsIn)");
        }

        $criteria->compare('t.user_id_create', $this->user_id_create);
        if(count($aWith)>0){
            $criteria->with = $aWith;
            $criteria->together = true;
        }
        $_SESSION['data-excel'] = new CActiveDataProvider($this, array(
            'pagination'=>false,
            'criteria'=>$criteria,
        ));

        $sort = new CSort();
        $sort->attributes = array(
            'store_card_no'=>'store_card_no',
            'type_store_card'=>'type_store_card',
            'type_in_out'=>'type_in_out',
            'date_delivery'=>'date_delivery',
            'created_date'=>'created_date',
        );
        $sort->defaultOrder = 't.date_delivery DESC, t.id DESC';
        if($cRole == ROLE_ADMIN || in_array($cUid, $aIdDieuPhoiBoMoi) || in_array($cUid, GasConst::getArrUidUpdateAll()) ){
            $sort->defaultOrder = 't.id DESC';
        }

        return new CActiveDataProvider($this, array(
                'criteria'=>$criteria,
                'pagination'=>array(
                    'pageSize'=> Yii::app()->user->getState('pageSize',Yii::app()->params['defaultPageSize']),

                ),
            'sort' => $sort,
        ));
    }
    
    /**
     * @Author: ANH DUNG Sep 01, 2016
     */
    public function getCriteria(&$criteria) {
        $criteria->compare('t.store_card_no', trim($this->store_card_no));
        $criteria->compare('t.customer_id',$this->customer_id);
        $criteria->compare('t.type_user',$this->type_user);
        $criteria->compare('t.type_store_card',$this->type_store_card);
        if(is_array($this->type_in_out) && count($this->type_in_out)){
            $sParamsIn = implode(',', $this->type_in_out);
            if(!empty($sParamsIn)){
                $criteria->addCondition("t.type_in_out IN ($sParamsIn)");
            }
        }
        
        if(is_array($this->province_id_agent) && count($this->province_id_agent)){
            $mEmployeeCashbook = new EmployeeCashbook();
            $aAgentId = $mEmployeeCashbook->getAgentOfProvince($this->province_id_agent);
            $sParamsIn = implode(',', $aAgentId);
            if(!empty($sParamsIn)){
                $criteria->addCondition("t.user_id_create IN ($sParamsIn)");
            }
        }
        if(!empty($this->store_card_real)){
            $c2 = new CDbCriteria();
            $c2->compare('t.code_no', trim($this->store_card_real));
            $mAppOrder = GasAppOrder::model()->find($c2);
            if($mAppOrder){
                $criteria->addCondition("t.id=$mAppOrder->store_card_id_gas OR t.id=$mAppOrder->store_card_id_vo");
            }
        }
        $criteria->compare('t.uid_login',$this->uid_login);

        $criteria->compare('t.car_id',$this->car_id);
        $criteria->compare('t.driver_id_1',$this->driver_id_1);
        $criteria->compare('t.driver_id_2',$this->driver_id_2);
        $criteria->compare('t.phu_xe_1',$this->phu_xe_1);
        $criteria->compare('t.phu_xe_2',$this->phu_xe_2);
        $criteria->compare('t.road_route',$this->road_route);
        
        if(!empty($this->created_date)){
            $this->created_date = MyFormat::dateDmyToYmdForAllIndexSearch($this->created_date);
            $criteria->addCondition("DATE(t.created_date)='$this->created_date'");
        }   
        if(!empty($this->date_delivery)){
            $this->date_delivery = MyFormat::dateDmyToYmdForAllIndexSearch($this->date_delivery);
            $criteria->addCondition("t.date_delivery='$this->date_delivery'");
        }
        
        if(!empty($this->date_from)){
            $date_from = MyFormat::dateDmyToYmdForAllIndexSearch($this->date_from);
            $criteria->addCondition("t.date_delivery>='$date_from'");
        }
        if(!empty($this->date_to)){
            $date_to = MyFormat::dateDmyToYmdForAllIndexSearch($this->date_to);
            $criteria->addCondition("t.date_delivery<='$date_to'");
        }

        if(!empty($this->statistic_month)){
            $criteria->compare('month(t.date_delivery)', $this->statistic_month);
        }

        if(!empty($this->statistic_year)){
            $criteria->compare('year(t.date_delivery)', $this->statistic_year);
        }
        
        // Loại KH search bên bảng detail
        if(!empty($this->ext_is_maintain)){
            if($this->ext_is_maintain==CUSTOMER_HO_GIA_DINH){
                $criteria->addCondition('( t.type_user='.CUSTOMER_HO_GIA_DINH.' ) AND ( t.customer_id=0  OR t.customer_id IS NULL )');
            }elseif($this->ext_is_maintain==CUSTOMER_OTHER){
                $criteria->addCondition('( t.type_user=0  OR t.type_user IS NULL ) AND ( t.customer_id<>0)');
            }else{
                $criteria->compare('t.type_user', $this->ext_is_maintain);
            }
        }

        if(!empty($this->collection_customer)){
            if($this->collection_customer==self::DA_THU_TIEN){
                $criteria->addCondition('t.collection_customer>0');
            }elseif($this->collection_customer==self::CHUA_THU_TIEN){
                $criteria->addCondition('t.collection_customer<1 OR t.collection_customer IS NULL');
            }
        }
        
        if($this->notViewWarehouse){// không lấy dữ liệu các kho
            $sParamsIn = implode(',', GasCheck::getAgentNotGentAuto());
            $criteria->addCondition('t.user_id_create NOT IN ('.$sParamsIn.')');
        }

    }

    public function afterValidate() {
        $aScenarioNotWeb = array('WindowCreate', 'WindowCreateHgd');
        if(in_array($this->scenario, $aScenarioNotWeb)){// Jul 12, 2016
            return parent::beforeValidate();
        }
        
        $valid1=true;
        $valid2=false;
        if(!isset($_POST['materials_id']) || count($_POST['materials_id'])<1 )
            $valid1=false;                
        $sumQty=0;
        if( isset($_POST['materials_qty']) && isset($_POST['materials_id']) && count($_POST['materials_id']) ){
            $this->aModelStoreCardDetail = array();
            foreach($_POST['materials_id'] as $key=>$item){
                $modelDetail = new GasStoreCardDetail();
                $modelDetail->materials_id = $item;
                $modelDetail->qty = $_POST['materials_qty'][$key];
                $sumQty +=$modelDetail->qty;
                $this->aModelStoreCardDetail[] = $modelDetail;
                if($_POST['materials_qty'][$key]*1>0) 
                    $valid2=true;
            }
        }
        if(!($valid1&&$valid2))
            $this->addError ('materials_name', 'Chưa chọn vật tư');
        
//        if(!self::checkValidQtyKhoAndAgent($this, $sumQty)){
//            $this->addError ('errCheckValidQtyKhoAndAgent', 'Số Lượng Nhập Kho Đại Lý Không Khớp Với Xuất Kho');
//        }
        return parent::beforeValidate();
    }

    /**
     * @Author: ANH DUNG May 29, 2014
     * @Todo: kiểm tra số lượng đại lý nhập kho có khớp với số lượng kho xuất không
     * @Param: $model model new store card
     * @Param: $sumQty tổng số lượng thẻ nhập kho của kho
     * @Return: true nếu khớp, false nếu không khớp
     */
    public static function checkValidQtyKhoAndAgent($model, $sumQty){
        $ok = true;
        if($model->type_store_card==TYPE_STORE_CARD_EXPORT && MyFormat::getAgentGender()==Users::IS_WAREHOUSE && !empty($model->customer_id)){
            $criteria = new CDbCriteria();
            $criteria->compare("t.type_store_card", TYPE_STORE_CARD_IMPORT);
            $criteria->compare("t.customer_id", MyFormat::getAgentId());
            $criteria->compare("t.user_id_create", $model->customer_id);
            $criteria->compare("t.date_delivery", MyFormat::dateConverDmyToYmd($model->date_delivery));
            $criteria->select = "sum(qty) as qty, store_card_id";
            $mDetail = GasStoreCardDetail::model()->find($criteria);
            if($mDetail->qty>0 && $mDetail->qty!=$sumQty){
                $ok = false;
                $session=Yii::app()->session;
                $session['STORE_CARD_ID_AGENT_NOT_VALID'] = $mDetail->store_card_id;
            }
        }
        return $ok;
    }

    protected function afterSave() {
        if($this->scenario == "KhoPhuocTanChangeAgent"){
            return parent::afterSave();
        }
        // cập nhật lần mua hàng mới nhất của KH 1. là xuất hàng 2. của KH bò mối
        if($this->type_store_card==TYPE_STORE_CARD_EXPORT && in_array($this->type_user, CmsFormatter::$aTypeIdBoMoi)){
            $mUser = Users::model()->findByPk($this->customer_id);
            if(!is_null($mUser)){
                $mUser->notifyFirstPurchase();
                $mUser->UpdateLastPurchaseFix($this->date_delivery);// Jun 02, 2016, fix nếu update thẻ kho và change customer
            }
            // Jun 09, 2015 tu dong cap nhat lai trang thai lay hang khi nhap the kho
//            Users::UpdateBackLayHang($this->customer_id);// Jan1718 bỏ update ở đây, đưa chung vào UpdateLastPurchaseFix
        }
        
        return parent::afterSave();
    }
    
    /**
     * @Author: ANH DUNG Jul 12, 2016
     * @todo: map 1 số field khi tạo trên web, 
     * vì hiện tai có cho tạo từ 2 chỗ
     * 1/ web 
     * 2/ c# sẽ bị lỗi vì sẽ không có biến Yii::app()->user
     */
    public function mapSomeFieldOnWeb() {
        if($this->isNewRecord){
            $this->user_id_create = MyFormat::getAgentId();
            $this->uid_login = MyFormat::getCurrentUid();
            if(MyFormat::getAgentGender()==Users::IS_WAREHOUSE_IN_OUT){
                $this->warehouse_new = GasStoreCard::WAREHOUSE_NEW;
            }
        }
    }
    
    public function beforeSave() {
        if(strpos($this->date_delivery, '/')){
            $this->date_delivery =  MyFormat::dateConverDmyToYmd($this->date_delivery);
            MyFormat::isValidDate($this->date_delivery);
        }
        if($this->isNewRecord){
            $this->province_id_agent = Users::GetProvinceId($this->user_id_create);
        }
        if($this->scenario == "KhoPhuocTanChangeAgent"){
            return parent::beforeSave();
        }
        if(!$this->isNewRecord){
//            if($cRole == ROLE_ADMIN){// vì chỗ này chạy ở create window nên ko sử dụng dc Yii::app->user->role_id => bug
                $OldModel = self::model()->findByPk($this->id);
                $this->user_id_create = $OldModel->user_id_create;// Jul 15, 2016 vì cho admin sửa nên phải lưu lại cái này
                $this->uid_login = $OldModel->uid_login;
//            }
        }
        $this->note = InputHelper::removeScriptTag($this->note);
        
        $this->customer_parent_id       = MyFormat::getParentIdForCustomer($this->rCustomer);
        $this->date_delivery_bigint     = strtotime($this->date_delivery);
        return parent::beforeSave();
    }

    // tính hạn thanh toán khi mua hàng của KH
    public static function calcDateOfPayMent($mCustomer, $date_sell){
        $date_of_payment= '';        
        if($mCustomer && $mCustomer->role_id==ROLE_CUSTOMER && $mCustomer->type==CUSTOMER_TYPE_STORE_CARD && !empty($mCustomer->payment_day)){
            $mTypePay = GasTypePay::model()->findByPk($mCustomer->payment_day);
            $tmpDate = explode('-', $date_sell);
            $d = $tmpDate[2];
            $m = $tmpDate[1];
            $y = $tmpDate[0];
            if($mTypePay)
                $date_of_payment = MyFunctionCustom::getDateOfPayment($date_sell, $mTypePay, $d, $m, $y);
        }
        return $date_of_payment;
    }
    
    /**
    * @Author: ANH DUNG 12-17-2013
    * @Todo: delete all store card detail with store card id if exitst, and add new all store card detail
    * @Param: $model model store card 
     */
    public static function saveStoreCardDetail($model){
        if(empty($model->id)){
            return;// add Mar 19, 2017
        }
        GasStoreCardDetail::deleteByStoreCardId($model->id);
        $mAppCache              = new AppCache();
        $aRowInsert             = array();
        $receivables_customer   = 0;
        $price_binh_bo          = 0;
        $tmpDate                = explode('-', $model->date_delivery);
        $year                   = $tmpDate[0];
        $date_delivery_bigint   = strtotime($model->date_delivery);
        $agent_id               = $model->user_id_create;
        $aMaterial              = $mAppCache->getMasterModel('GasMaterials', AppCache::ARR_MODEL_MATERIAL);
        
        $mSale = null;// Jun 26, 2017 sửa lại query ở ngoài foreach
        $mCustomer              = Users::model()->findByPk($model->customer_id);
        if($mCustomer){
            $mSale              = Users::model()->findByPk($mCustomer->sale_id);
        }
        
        // tính hạn thanh toán cho KH này, KHI XUẤT HÀNG
//        if($model->type_store_card==TYPE_STORE_CARD_EXPORT){// Close on Sep 7, 2016, vì không dùng đến
//            $model->date_of_payment= self::calcDateOfPayMent($model->customer, $model->date_delivery);
//        }
        
        if( isset($_POST['materials_qty']) && isset($_POST['materials_id']) && count($_POST['materials_id']) ){
            foreach($_POST['materials_id'] as $key=>$item){
                if($_POST['materials_qty'][$key]*1>0 && is_numeric($item) && is_numeric($_POST['materials_qty'][$key])) {
                    $modelDetail = new GasStoreCardDetail();
                    $store_card_id = $model->id;
                    $modelDetail->customer_id           = $model->customer_id;
                    $modelDetail->customer_parent_id    = $model->customer_parent_id;
                    $modelDetail->warehouse_new         = $model->warehouse_new;
                    $modelDetail->province_id_agent     = $model->province_id_agent;// Apr 22, 2015
                    $modelDetail->car_id                = $model->car_id;// Jul 25, 2016 theo dõi sản lượng từng xe
                    $modelDetail->driver_id_1           = $model->driver_id_1;
                    $modelDetail->driver_id_2           = $model->driver_id_2;
                    $modelDetail->phu_xe_1              = $model->phu_xe_1;
                    $modelDetail->phu_xe_2              = $model->phu_xe_2;
                    $modelDetail->road_route            = $model->road_route;// Now 20, 2016 add lại, không hiểu tại sao trc đó lại bỏ đi, cái này liên quan đến thống kê sản lượng xe !important admin/gasreports/outputCar
                    $modelDetail->uid_login             = $model->uid_login;
                    $modelDetail->employee_maintain_id  = $model->employee_maintain_id;
                    
                    $customer_id                        = $model->customer_id;
                    $materials_id                       = (int)$item;
                    $modelDetail->materials_id          = $materials_id;
                    $sale_id = $sale_type = $type_customer = "";
                    if($mCustomer){
//                        /là loại KH 1: bò hay 2: mối, 3: HỘ GIA ĐÌNH,4:KH khác (chưa xử lý lưu số 4 này), 5: KH LÀ ĐẠI LÝ KHÁC TRONG HỆ THỐNG 
                        $sale_id        = $mCustomer->sale_id;
                        $type_customer  = $mCustomer->is_maintain;// là loại KH 1: bò hay 2: mối
                        if($mCustomer->role_id==ROLE_AGENT){
                            $type_customer = CUSTOMER_IS_AGENT;
                        }
                        if($mSale){// 1: sale bò, 2 sale mối, 3 sale chuyên viên, 4 sale pttt..
                            $sale_type = $mSale->gender;
                        }
                    }
                    
                    if(empty($modelDetail->customer_id)){
                        $type_customer = CUSTOMER_HO_GIA_DINH;
                    }
                    
                    $type_store_card    = $model->type_store_card;
                    $type_in_out        = $model->type_in_out;
                    $date_delivery      = MyFormat::removeBadCharacters($model->date_delivery);
                    $delivery_person    = MyFormat::removeBadCharacters($model->delivery_person);
                    $qty                = (double)$_POST['materials_qty'][$key];
                    
                    $unit_use           = 1;
                    $price              = isset($_POST['price'][$key]) ? (double)$_POST['price'][$key] : 0;
                    
                    // tính toán loại vật tư và giá cho bình bò
                    $materials_type_id = '';
                    if(isset($aMaterial[$materials_id])){
                        $materials_type_id = $aMaterial[$materials_id]['materials_type_id'];
                        if(array_key_exists($materials_type_id, CmsFormatter::$MATERIAL_TYPE_BINH_BO_VALUE_KG)){
                            $price_binh_bo = $price;
                            // lấy đơn vị sử dụng cho bình 50,45 còn bình 12,6 thì ta ko tính kg mà tính là 1 bình
                            $unit_use = CmsFormatter::$MATERIAL_TYPE_BINH_BO_VALUE_KG[$materials_type_id];
                        }
                    }

//                    if($modelDetail->materials){// Close on Feb 25, 2017 đổi sang sử dụng cache bên trên
//                        $materials_type_id = $modelDetail->materials->materials_type_id;
//                        if(array_key_exists($materials_type_id, CmsFormatter::$MATERIAL_TYPE_BINH_BO_VALUE_KG)){
//                            $price_binh_bo = $price;
//                            // lấy đơn vị sử dụng cho bình 50,45 còn bình 12,6 thì ta ko tính kg mà tính là 1 bình
//                            $unit_use = CmsFormatter::$MATERIAL_TYPE_BINH_BO_VALUE_KG[$materials_type_id];
//                        }
//                    }
                    
                    $amount                 = $qty*$unit_use*$price;
                    $receivables_customer   += $amount;                    
                    $user_id_create         = $model->user_id_create;
                    if($model->type_store_card==TYPE_STORE_CARD_IMPORT){
                        // cập nhật nxt vật tư đại lý 
                        GasMaterialsOpeningBalance::addAgentImport($year, $agent_id, $materials_id, $qty);
                        // Nhập vỏ - xuất gas: update nhập vỏ của KH bò mối
                        if(!empty($model->customer_id) && in_array($materials_type_id, CmsFormatter::$MATERIAL_TYPE_PAIR)){
                            $export_materials_type_id = CmsFormatter::$MATERIAL_TYPE_PAIR[$materials_type_id];
                            $import_materials_type_id = $materials_type_id;
                            GasClosingBalanceCustomerMaterials::addMaterialImport($year, $model->customer_id, 
                                    $type_customer, $export_materials_type_id, $import_materials_type_id, $qty);
                        }
                        // Nhập vỏ - xuất gas: update nhập vỏ của KH bò mối
                    }else{
                        // cập nhật nxt vật tư đại lý
                        GasMaterialsOpeningBalance::addAgentExport($year, $agent_id, $materials_id, $qty);
                        // is TYPE_STORE_CARD_EXPORT
                        // Nhập vỏ - xuất gas: update xuất bán gas của KH bò mối
                        if(!empty($model->customer_id) && in_array($materials_type_id, CmsFormatter::$MATERIAL_TYPE_PAIR)){
                            $export_materials_type_id = $materials_type_id;
                            $import_materials_type_id = CmsFormatter::$MATERIAL_TYPE_PAIR[$materials_type_id];
                            GasClosingBalanceCustomerMaterials::addMaterialExport($year, $model->customer_id, 
                                    $type_customer, $export_materials_type_id, $import_materials_type_id, $qty);
                        }
                        // Nhập vỏ - xuất gas: update xuất bán gas của KH bò mối
                    }
                    
                    $aRowInsert[]="('$store_card_id',
                        '$customer_id',
                        '$modelDetail->uid_login',
                        '$modelDetail->customer_parent_id',
                        '$modelDetail->warehouse_new',                        
                        '$type_customer',
                        '$sale_id', 
                        '$sale_type', 
                        '$type_store_card', 
                        '$type_in_out', 
                        '$date_delivery',
                        '$date_delivery_bigint',
                        '$materials_id',
                        '$materials_type_id',
                        '$qty',
                        '$price',
                        '$amount',
                        '$delivery_person',
                        '$user_id_create',
                        '$modelDetail->province_id_agent',
                        '$modelDetail->car_id',
                        '$modelDetail->driver_id_1',
                        '$modelDetail->driver_id_2',
                        '$modelDetail->phu_xe_1',
                        '$modelDetail->phu_xe_2',
                        '$modelDetail->road_route',
                        '$modelDetail->employee_maintain_id'
                        )";
                }
            }
            
            $tableName = GasStoreCardDetail::model()->tableName();
            $sql = "insert into $tableName (store_card_id,
                        customer_id,
                        uid_login,
                        customer_parent_id,
                        warehouse_new,
                        type_customer,
                        sale_id,
                        sale_type,
                        type_store_card,
                        type_in_out,
                        date_delivery,
                        date_delivery_bigint,
                        materials_id,
                        materials_type_id,
                        qty,
                        price,
                        amount,
                        delivery_person,
                        user_id_create,
                        province_id_agent,
                        car_id,
                        driver_id_1,
                        driver_id_2,
                        phu_xe_1,
                        phu_xe_2,
                        road_route,
                        employee_maintain_id
                        ) values ".implode(',', $aRowInsert);
            if(count($aRowInsert)>0)
                Yii::app()->db->createCommand($sql)->execute();
            
            if($price_binh_bo>0 && !empty($model->customer_id)){
                GasRemain::updatePriceBinhBo($model->customer_id, $model->date_delivery, $price_binh_bo );                
            }
            
            // Lấy tiền gas dư KH đề trừ cho đơn hàng của KH
            $AmountGasDuBinhBo = GasRemain::getAmountGasDuBinhBo($model->customer_id, $model->date_delivery, $model->user_id_create);
            $attSave = array('receivables_customer','price_binh_bo');
            $model->receivables_customer = round($receivables_customer-$AmountGasDuBinhBo); // Tổng tiền đơn hàng, phải thu của KH

            $model->price_binh_bo = $price_binh_bo;
//            if($model->pay_now && $model->collection_customer < 1){ 
            if($model->pay_now){ // CÓ CẦN CHECK ??? chỉ update khi chưa update collection_customer
                $model->collection_customer = $model->receivables_customer;
                $model->collection_customer_date = date("Y-m-d H:i:s");
                $attSave[]='collection_customer';
                $attSave[]='collection_customer_date';
            }
            
            if($model->type_store_card==TYPE_STORE_CARD_EXPORT){// xuất bán
                $attSave[]='date_of_payment';
            }
            
            // hình như phải thu kh receivables_customer chỉ có cập nhật ở chỗ này + gas dư nữa, nên ta xử lý cập nhật 
            // bên gas dư phải thêm 1 cờ để kiểm tra has_update_balance
            // vào table gas_gas_closing_balance_customer công nợ năm của KH ở đây, trước khi storecard update
            self::UpdateReceivablesCustomerYear($model);
//            $model->update($attSave);// Close function on sep 07, 2016 vì hiện tại ko dùng đến chức năng này
        }
    }
    
    /**
     * @Author: ANH DUNG Jun 01, 2014
     * @Todo: cập nhật phải thu của kh trong 1 năm
     * @Param: $mStoreCard model store card
     */    
    public static function UpdateReceivablesCustomerYear($mStoreCard){
        return ;// Close function on sep 07, 2016 vì hiện tại ko dùng đến chức năng này
        if(empty($mStoreCard->customer_id)) return;
        $mUser = Users::model()->findByPk($mStoreCard->customer_id);
        if($mUser && $mUser->role_id!=ROLE_CUSTOMER) return;
        
        $oldModel = self::model()->findByPk($mStoreCard->id);
        $receivables_customer = $mStoreCard->receivables_customer;
        $type_customer = $mStoreCard->type_user;// là loại KH 1: bò hay 2: mối, mới đổi lại laijMay 21, 2014
        $tmpDate = explode('-', $mStoreCard->date_delivery);
        $year = $tmpDate[0];
        $customer_id = $mStoreCard->customer_id;
        if($oldModel){ // nếu là cập nhật store card thì phải trừ đi số phải thu của record trước
            $receivables_customer -= $oldModel->receivables_customer;
        }
        GasClosingBalanceCustomer::addReceivablesCustomer($year, $customer_id, $type_customer, $receivables_customer);
    }

    /**
     * @Author: ANH DUNG May 22, 2014
     * @Todo: get giá bình bò theo customer_id và date_input, dùng để cập nhật vào gas dư khi tạo mới
     * hay update
     * @Param: $customer_id
     * @Param: $date_input 2014-05-27
     */
    public static function getPriceBinhBo($customer_id, $date_delivery){
        if(empty($customer_id) || $customer_id<1 ) return 0;
        $aDate = explode('-', $date_delivery);
        $criteria = new CDbCriteria();
        $criteria->compare("t.customer_id",$customer_id);
        $criteria->compare('month(t.date_delivery)', $aDate[1]);
        $criteria->compare('year(t.date_delivery)', $aDate[0]);
        $criteria->addCondition("t.price_binh_bo>0");
        $criteria->order = 't.date_delivery DESC';
        $model = self::model()->find($criteria);
        if($model)
            return $model->price_binh_bo;
        return 0;
    }
    
    /**
     * @Author: ANH DUNG May 29, 2014
     * @Todo: get giá bình bò theo customer_id và currrent month, year
     * @Param: $customer_id
     */
    public static function getPriceBinhBoCurrentMonth($customer_id){
        if(empty($customer_id) || $customer_id<1 ) return 0;
        $criteria = new CDbCriteria();
        $criteria->compare("t.customer_id",$customer_id);
        $criteria->compare('month(t.date_delivery)', date('m'));
        $criteria->compare('year(t.date_delivery)', date('Y'));
        $criteria->addCondition("t.price_binh_bo>0");
        $criteria->order = 't.date_delivery DESC';
        $model = self::model()->find($criteria);
        if($model)
            return $model->price_binh_bo;
        return 0;
    }
    
    /**
    * @Author: ANH DUNG 12-17-2013
    * @Todo: get all store card detail and format set to variable post
    * @Param: $model model store card 
     */
    public static function getStoreCardDetail1($model){
        return GasStoreCardDetail::getByStoreCardId($model->id);
    }

    public function beforeDelete() {
        GasStoreCardDetail::deleteByStoreCardId($this->id);
        // xử lý trừ phải thu của kh 
        $receivables_customer = $this->receivables_customer;
        $tmpDate = explode('-', $this->date_delivery);
        $year = $tmpDate[0];
        $customer_id = $this->customer_id;
        GasClosingBalanceCustomer::cutReceivablesCustomer($year, $customer_id, $receivables_customer);
        // xử lý trừ phải thu của kh 
        $this->deleteSubRecord();
        GasFile::DeleteByBelongIdAndType($this->id, GasFile::TYPE_9_STORECARD);
        // Jul619 delete ref GasDebts
        $mDebts = new GasDebts();
        $mDebts->relate_id    = $this->id;
        $mDebts->type         = GasDebts::TYPE_DEBIT_PVKH;
        $mDebts->deleteByRelateId();// delete before save
        
        return parent::beforeDelete();
    }
    
    // chỉ cho phép update record dc tạo trong ngày, true if dc tao trong ngày, false nếu tạo hôm trc
    public function canUpdate(){
//        return true;
        $cRole  = MyFormat::getCurrentRoleId();
        $cUid   = MyFormat::getCurrentUid();
//        if($cRole == ROLE_ADMIN){// only dev debug, need close after fix
//            return true;
//        }
        if( ($this->type_user == CUSTOMER_HO_GIA_DINH && !in_array($this->user_id_create, UsersExtend::getAgentNotRunApp())) || !empty($this->parent_id) || $this->isLockAgentRunApp() || $this->pay_now == GasStoreCard::CREATE_AUTO_FROM_APP_ORDER){
            return false;// Aug 19, 2016 xử lý không cho ai edit thẻ kho tự động sinh khi đồng bộ với bán hàng dưới C#
        }
//        if(in_array($cUid, GasConst::getArrUidUpdateAll()) && !in_array($this->user_id_create, CmsFormatter::$LIST_WAREHOUSE_ACCOUNT)){
        if(in_array($cUid, GasConst::getArrUidUpdateAll())){
            return GasConst::canUpdateAll($this->created_date);// Oct2317 cần check lại những phiếu nào cho phép sửa, vì sửa nhầm phiếu xe tải thì bị sai hết
        }
        
        $dayAllow = date('Y-m-d');
        $dayAllow = MyFormat::modifyDays($dayAllow, Yii::app()->params['storecard_admin_update'], '-');
        if( MyFormat::getCurrentRoleId() == ROLE_SUB_USER_AGENT){
            $dayAllow = date('Y-m-d');
            $dayAllow = MyFormat::modifyDays($dayAllow, GasCashBook::getAgentDaysAllowUpdate(), '-');
            // dùng chung thiết lập ngày cho phép với sổ quỹ Yii::app()->user->payment_day
        }elseif($cRole != ROLE_ADMIN){
            return false;
        }
        return MyFormat::compareTwoDate($this->created_date, $dayAllow);
    }
 
    // chỉ cho phép Delete record dc tạo trong ngày, true if dc tao trong ngày, false nếu tạo hôm trc
    public static function canDelete($model){
//        return true;        
        if(Yii::app()->params['enable_delete'] == 'no'){
            return false;
        }
        $dayAllow = date('Y-m-d');
//        $dayAllow = MyFormat::modifyDays($dayAllow, GasStoreCard::$days_allow_delete, '-');
        $dayAllow = MyFormat::modifyDays($dayAllow, Yii::app()->params['storecard_admin_delete'], '-');
        return MyFormat::compareTwoDate($model->created_date, $dayAllow);
    }
    // chỉ cho phép Delete record dc tạo trong ngày, true if dc tao trong ngày, false nếu tạo hôm trc
    public static function canUpdateCollectionCustomer($model){
//        return true;
        $dayAllow = date('Y-m-d');
//        $dayAllow = MyFormat::modifyDays($dayAllow, GasStoreCard::$days_agent_allow_UpdateCollectionCustomer, '-');
        $dayAllow = MyFormat::modifyDays($dayAllow, Yii::app()->params['storecard_agent_updateCollectionCustomer'], '-');
//        if($model->collection_customer > 0 || (Yii::app()->user->role_id==ROLE_SUB_USER_AGENT && $model->user_id_create != MyFormat::getAgentId()) )
        if( MyFormat::getCurrentRoleId() != ROLE_SUB_USER_AGENT || (MyFormat::getCurrentRoleId() == ROLE_SUB_USER_AGENT && $model->user_id_create != MyFormat::getAgentId()) )
            return false;
        return MyFormat::compareTwoDate($model->created_date, $dayAllow);
    }
    
    public static function getArrObjectByArrId($aId=array(), $needMore = array())
    {
        $criteria = new CDbCriteria;
        if(count($aId)>0){
//            $criteria->addInCondition('t.id',$aId);
            $sParamsIn = implode(',', $aId);
            $criteria->addCondition("t.id IN ($sParamsIn)");
        }
        $models = self::model()->findAll($criteria);
        if(isset($needMore['listdata'])){
            return CHtml::listData($models, "id", "store_card_no");
        }
        $res=array();
        foreach ($models as $item)
                $res[$item->id] = $item;
        return  $res;         
    }
    
    /**
     * @Author: ANH DUNG Aug 19, 2014
     * @Todo: lấy ngày mua hàng đầu tiên của khách hàng
     * @Param: $customer_id
     * @Return: model
     */
    public static function GetFirstPurchase($customer_id){
        $criteria = new CDbCriteria;
        $criteria->compare('t.customer_id', $customer_id);
//        $criteria->addInCondition("t.type_in_out", CmsFormatter::$TYPE_IN_OUT_OUTPUT_STATISTIC); // xuất bán, bán bộ bình, thế chân
        $sParamsIn = implode(',', CmsFormatter::$TYPE_IN_OUT_OUTPUT_STATISTIC);
        $criteria->addCondition("t.type_in_out IN ($sParamsIn)");
        $criteria->order = "t.date_delivery ASC";
        $criteria->limit = 1;
        return self::model()->find($criteria);
    }
    
    /**
     * @Author: ANH DUNG Aug 19, 2014
     * @Todo: lấy ngày mua hàng mới nhất của khách hàng
     * @Param: $customer_id
     * @Return: model
     */
    public static function GetLastPurchase($customer_id){
        $criteria = new CDbCriteria;
        $criteria->compare('t.customer_id', $customer_id);
        $sParamsIn = implode(',', CmsFormatter::$TYPE_IN_OUT_OUTPUT_STATISTIC);
        $criteria->addCondition("t.type_in_out IN ($sParamsIn)");
        $criteria->order = "t.date_delivery DESC, t.id DESC";
        $criteria->limit = 1;
        return self::model()->find($criteria);
    }
    
    /**
     * @Author: ANH DUNG Apr 05, 2019
     * @Todo: lấy đơn hàng mới nhất của khách hàng
     * @Return: model
     */
    public function getLastOrder(){
        $criteria = new CDbCriteria;
        $criteria->compare('t.customer_id', $this->customer_id);
        $criteria->order = "t.date_delivery DESC, t.id DESC";
        $criteria->limit = 1;
        return self::model()->find($criteria);
    }
    
    /**
     * @Author: ANH DUNG Jul 12, 2016
     * @Todo: auto gen storecard no
     */
    public function buildStoreCardNo() {
        if(empty($this->store_card_no)){
//            if(is_null($this->mAgent) || empty($this->mAgent)){
//                $this->mAgent = Users::model()->findByPk($this->user_id_create);
//            }
//            $code_account = !empty($this->mAgent->code_account) ? $this->mAgent->code_account : "HM";
            $length = 10; // T170000001 = 10 char. Jun 29, 2017  đổi ang code mới T
            $prefix_code = 'T'.date('y'); // 2015 will apply, có thể sẽ phải tăng length LENGTH_STORE_CARD ở config local
//            $this->store_card_no = MyFunctionCustom::getNextId('GasStoreCard', $prefix_code, $length,'store_card_no');// Jul2217 đổi lại cách gen để giảm query vào db vì thấy không cần thiết lắm
            $this->store_card_no = $prefix_code.ActiveRecord::randString(6, GasConst::CODE_CHAR);
        }
    }
    
    /**
     * @Author: ANH DUNG Jan 12, 2015
     * @Todo: tính toán độ dài cho mã thẻ kho
     * CH161500002 
     * CH1615000001
     */
    public function GetLengthCode() {
        $length = MyFormat::MAX_LENGTH_12;
        $needMore = array();
        if( !is_null($this->mAgent) ){
            $needMore['code_account'] = $this->mAgent->code_account;
        }
        return MyFormat::GetLengthCode($length, $needMore);
    }
    
    /**
     * @Author: ANH DUNG Jul 12, 2016
     * @Todo: save some info user update
     */
    public function mapInfoUpdate() {
        if(!$this->isNewRecord){
            $this->last_update_by = MyFormat::getCurrentUid();
            $this->last_update_time = date('Y-m-d H:i:s');
            $cmsFormater = new CmsFormatter();
            // lưu lại giá trị trước cập nhật của thẻ kho
            $this->value_before_update = $cmsFormater->formatStoreCardDetail($this);
        }
    }

    /**
     * @Author: ANH DUNG Jul 12, 2016
     * @Todo: vì sử dụng cho tạo ở c# vs Web
     */
    public function getTypeCustomer() {
        $mUser = Users::model()->findByPk($this->customer_id);
        $this->type_user = $mUser?$mUser->is_maintain:CUSTOMER_HO_GIA_DINH;
        //là loại KH 1: bò hay 2: mối, 3: HỘ GIA ĐÌNH,4:KH khác (chưa xử lý lưu số 4 này), 5: KH LÀ ĐẠI LÝ KHÁC TRONG HỆ THỐNG 
        if($mUser && $mUser->role_id==ROLE_AGENT){
            $this->type_user = CUSTOMER_IS_AGENT;
        }
    }
    
    public function getUidLogin() {
        $mUser = $this->rUidLogin;
        if($mUser){
            return $mUser->first_name;
        }
        return '';
    }
    public function getLastUpdate() {
        $mUser = $this->rLastUpdate;
        if($mUser){
            return $mUser->first_name;
        }
        return '';
    }
    
    /**
     * @Author: ANH DUNG May 21, 2017
     * @Todo: chỉ sử dụng hàm này để get name điều phối của thẻ kho. không sử dụng cho các loại user khác
     */
    public function getUidLoginByCache() {
        if(isset(GasTickets::$DIEU_PHOI_FULLNAME[$this->uid_login])){
            return GasTickets::$DIEU_PHOI_FULLNAME[$this->uid_login];
        }
        $listdataCallCenter = CacheSession::getListdataByRole([ROLE_CALL_CENTER, ROLE_DIEU_PHOI]);
        if(isset($listdataCallCenter[$this->uid_login])){
            return $listdataCallCenter[$this->uid_login];
        }
        return '';
    }
    
    /**
     * @Author: ANH DUNG Jul 25, 2016
     */
    public function getCar() {
        return GasCheck::sessionUserGetName(GasCheck::SESSION_USER_STORE_CARD, $this->car_id);
    }
    public function getDriver1() {
        return GasCheck::sessionUserGetName(GasCheck::SESSION_USER_STORE_CARD, $this->driver_id_1);
    }
    public function getDriver2() {
        return GasCheck::sessionUserGetName(GasCheck::SESSION_USER_STORE_CARD, $this->driver_id_2);
    }
    public function getPhuXe1() {
        return GasCheck::sessionUserGetName(GasCheck::SESSION_USER_STORE_CARD, $this->phu_xe_1);
    }
    public function getPhuXe2() {
        return GasCheck::sessionUserGetName(GasCheck::SESSION_USER_STORE_CARD, $this->phu_xe_2);
    }
    public function getDeliveryPerson() {
        $res = $this->delivery_person;
        if($this->getCar() != ''){
            $res .= "<b>{$this->getRoadRoute()}</b>";
            $res .= "<br><b>{$this->getCar()}</b>";
        }
        if($this->getDriver1() != ''){
            $res .= "<br>Tài 1: {$this->getDriver1()}";
        }
        if($this->getDriver2() != ''){
            $res .= "<br>Tài 2: {$this->getDriver2()}";
        }
        if($this->getPhuXe1() != ''){
            $res .= "<br>Phụ xe 1: {$this->getPhuXe1()}";
        }
        if($this->getPhuXe2() != ''){
            $res .= "<br>Phụ xe 2: {$this->getPhuXe2()}";
        }
        return $res;
    }
    
    /**
     * @Author: ANH DUNG Aug 14, 2016
     */
    public function getCreatedDate($fomat = 'd/m/Y H:i') {
        $res = '';
        $cmsFormat = new CmsFormatter();
        $mAppCache = new AppCache();
        $aIdDieuPhoiBoMoi   = $mAppCache->getArrayIdRole(ROLE_DIEU_PHOI);
        if( in_array($this->uid_login, $aIdDieuPhoiBoMoi)){
            $res .= $cmsFormat->formatDateTime($this->created_date)."<br><b>". $this->getUidLoginByCache()."</b>";
        }else{
            $res .= $cmsFormat->formatDateTime($this->created_date);
        }
        return $res;
    }
    public function getCreatedDateApp($format='d/m/Y H:i') {
        return MyFormat::dateConverYmdToDmy($this->created_date, $format);
    }
    public function getDateDelivery() {
        return MyFormat::dateConverYmdToDmy($this->date_delivery);
    }
    public function getNoteOnly() {
        return $this->note;
    }
    public function getCustomer($field_name='first_name') {
        $mUser = $this->rCustomer;
        if($mUser){
            return $mUser->$field_name;
//            return "<b>".$mUser->code_bussiness."-".$mUser->first_name."</b><br>".$mUser->address."";
        }
        return '';
    }
    
    public function getNameCustomer(){
       $res = '';
       $cmsFormater = new CmsFormatter();
       if($this->customer){
           $res = $cmsFormater->formatNameUser($this->customer);
       }else{
           $res = 'HGĐ';
       }
       return $res;
   }
    
    /**
     * @Author: ANH DUNG Feb 27, 2017
     * @Todo: check xem User có được quyền sửa thẻ kho không
     */
    public function employeeCanUpdate() {
//        return '1'; // only dev test need close on live
        if(is_null($this->mAppUserLogin) || $this->phu_xe_2 == GasStoreCardApi::IS_ADMIN_GEN_FROM_ORDER){
            return '0';
        }
        $days_check = Yii::app()->setting->getItem('DaysUpdateAppBoMoi');
        $dayAllow   = date('Y-m-d');
        $dayAllow   = MyFormat::modifyDays($dayAllow, $days_check, '-');
        if($this->uid_login ==  $this->mAppUserLogin->id && MyFormat::compareTwoDate($this->created_date, $dayAllow)){
            return '1';
        }
        return '0';
    }
        
    
    /**
     * @Author: ANH DUNG Sep 07, 2016
     * @Todo: init session id những đại lý đang chạy PM mới để limit tạo BH HGD
     */
    public function initSessionAgentSell() {
        $session=Yii::app()->session;
        if(!isset($session['AGENT_RUN_SELL'])){
            $session['AGENT_RUN_SELL'] = Sell::getAllAgentRunNew();
        }
    }
    
    /**
     * @Author: ANH DUNG Sep 07, 2016
     * @Todo: check những đại lý đang chạy PM mới để limit tạo BH HGD
     */
    public function canMakeSellHgd() {
        if($this->type_user == CUSTOMER_HO_GIA_DINH && in_array($this->type_in_out, GasStoreCardApi::$TYPE_IN_OUT_HGD)){
            $session=Yii::app()->session;
            if(isset($session['AGENT_RUN_SELL']) && in_array($this->user_id_create, $session['AGENT_RUN_SELL'])){
                return false;
            }
        }
        return true;
    }
    
    /**
     * @Author: ANH DUNG Oct 09, 2016
     * @Todo: check những đại lý TP, Bình Dương, Đồng Nai không tạo thẻ kho bò mối
     * Tạo mới xuất bán KH bò mối thì ko cho tạo
     */
    public function canMakeStorecardBoMoi() {
        if($this->type_in_out == STORE_CARD_TYPE_3 && $this->isNewRecord && in_array($this->type_user, CmsFormatter::$aTypeIdBoMoi)){
            $aProvince = array(GasProvince::TP_HCM, GasProvince::TINH_BINH_DUONG, GasProvince::TINH_DONG_NAI, GasProvince::TINH_VUNG_TAU);
//            $aAgentAllow = array(MyFormat::KHO_PHUOC_TAN, MyFormat::KHO_BEN_CAT, GasConst::AGENT_BINH_THANH_1);
            $aAgentAllow = array(MyFormat::KHO_PHUOC_TAN, MyFormat::KHO_BEN_CAT);
            $mAgent = $this->rAgent;
            if(!is_null($mAgent) && in_array($mAgent->province_id, $aProvince) && !in_array($mAgent->id, $aAgentAllow)){
                return false;
            }
        }
        return true;
    }
    
    /** @Author: ANH DUNG Sep 25, 2016 check KH bị chặn hàng  */
    public function checkChanHang() {
        $mCustomer = $this->rCustomer;
        if($mCustomer){
            $aTypeCustomer  = [STORE_CARD_KH_BINH_BO, STORE_CARD_KH_MOI];
            $aTypeLock      = [Users::CHAN_HANG, Users::CHAN_HANG_KHAI_TU]; 
            if(in_array($mCustomer->channel_id, $aTypeLock) && in_array($mCustomer->is_maintain, $aTypeCustomer)){
                $this->addError('customer_id', 'Khách hàng đã bị chặn hàng, hoặc khai tử. Không thể tạo đơn hàng');
            }
            if($mCustomer->is_maintain == UsersExtend::STORE_CARD_FOR_QUOTE){
                $this->addError('customer_id', 'Khách hàng tiềm năng không thể tạo đơn hàng, phải chuyển sang KH bò mối để tạo đơn');
            }
        }
    }
    
    /**
     * @Author: ANH DUNG Oct 02, 2016
     * @Todo: make record socket notify. điều phối tạo đơn hàng thì notify xuống cho DL
     * chỗ này chưa xử lý viết lại trên web, xử lý window ko dc
     */
    public function makeSocketNotify($mUserLogin) {
        if(!in_array($this->user_id_create, UsersExtend::getAgentNotRunApp())){
            return ;// May 30, 2017 Không gửi notify cho các đại lý chạy app nữa 
        }
        $mAppCache = new AppCache();
        $aIdDieuPhoiBoMoi = $mAppCache->getArrayIdRole(ROLE_DIEU_PHOI);
        if( in_array($this->uid_login, $aIdDieuPhoiBoMoi)){
//        if(1){
            $mCustomer  = $this->rCustomer;
            $mAgent     = $this->rAgent;
//            public static function addMessage($sender_id, $userId, $code, $message, $json)
            $message = "Đặt hàng: {$mCustomer->getFullName()}. $this->note";
            $json = array();
            $json['agent_name']     = $mAgent->getFullName();
            $json['customer_name']  = $mCustomer->getFullName();
            $json['type_customer']  = $mCustomer->getTypeCustomerText();
            $needMore = array('mSender'=>$mUserLogin, 'agent_id'=>$mAgent->id);
            GasSocketNotify::addMessage(0, $this->uid_login, $this->user_id_create, GasSocketNotify::CODE_STORECARD, $message, $json, $needMore);
        }
    }
    
    /**
     * @Author: ANH DUNG Oct 10, 2016
     * @Todo: get store_card_no
     */
    public function getStorecardNo() {
        $res = "<span>$this->store_card_no</span>";
        $html = "";
        $aAgentAllow = array(MyFormat::KHO_PHUOC_TAN);
        $cRole = MyFormat::getCurrentRoleId();
        if($cRole == ROLE_SUB_USER_AGENT && in_array($this->user_id_create, $aAgentAllow)){
//        if(1){ 
            $url = Yii::app()->createAbsoluteUrl("admin/gasstorecard/update", array('id'=>$this->id, 'ChangeAgent'=>1));
            $dropdown = CHtml::dropDownList('ChangeAgent', "", 
                $this->getArrayAgentChange(), array('class'=>'ChangeAgentValue', 'empty'=>'Chọn ĐL Chuyển'));
            $html = "<br><a class='ShowChangeAgent' href='javascript:;'>Chuyển đại lý</a>";
            $htmlSave = "<span class='display_none WrapChangeAgentDropdown'>$dropdown<br><br><a class='SaveChangeAgent' next='$url' href='javascript:;'>Save</a>&nbsp;&nbsp;&nbsp;<a class='CancelChangeAgent' href='javascript:;'>Cancel</a></span>";
            
            $html .= "$htmlSave";
        }
        return $res.$html;
    }

    /**
     * @Author: ANH DUNG Oct 10, 2016
     */
    public function getArrayAgentChange() {
        return array(
            GasCheck::DL_BINH_THANH_1   => "Đại lý Bình Thạnh 1",
//            GasConst::AGENT_QUAN_2      => "Đại lý Quận 2",
        );
    }
    
    /** @Author: ANH DUNG Oct 21, 2016
     * @todo: xử lý không cho change KH do điều phối tạo
     */
    public function canRemoveChangeCustomer() {
        $cRole = MyFormat::getCurrentRoleId();
        if($cRole != ROLE_ADMIN){
            if( in_array($this->uid_login, GasTickets::$UID_DIEU_PHOI)){
                return true;
            }
        }
        return false;
    }

    public function getTypeInOutText() {
        return isset(CmsFormatter::$STORE_CARD_ALL_TYPE[$this->type_in_out]) ? CmsFormatter::$STORE_CARD_ALL_TYPE[$this->type_in_out] : "";
    }
    public function getReasonFalse() {
        $aFalse = $this->getStatusFalse();
        return isset($aFalse[$this->reason_false]) ? $aFalse[$this->reason_false] : '';
    }
    public function getNote(){
       $note = nl2br($this->note);
       $update_note = nl2br($this->update_note);
       $res = $note." $update_note";
       if(!empty($note)){
           $res = $note."<br> $update_note";
       }
       $reasonFalse = $this->getReasonFalse();
       if(!empty($reasonFalse)){
           $res .= '<br><b>Loại: </b>'. $reasonFalse;
       }
       return $res;
    }  
 
    public function getAgentName() {
//        $mAppCache = new AppCache();
//        $aAgent = $mAppCache->getAgent();
        $aAgent = CacheSession::getListAgent();
        return isset($aAgent[$this->user_id_create]) ? $aAgent[$this->user_id_create]['first_name'] : '';
    }
    public function getAgent() {
        $mAgent = $this->rAgent;
        if($mAgent){
            return $mAgent->first_name;
        }
        return '';
    }
    
    
       // 12-17-2013 ANH DUNG
   public function getStoreCardTypeInOut(){
       return isset(CmsFormatter::$STORE_CARD_ALL_TYPE[$this->type_in_out]) ? CmsFormatter::$STORE_CARD_ALL_TYPE[$this->type_in_out] : '';
   }

    /**
     * @Author: ANH DUNG Nov 08, 2016 belong to GasstorecardController -> handleNotMakeOrder
     * @Todo: xóa hết detail khi không phát sinh giao hàng
     */
    public function deleteDetail() {
        if(empty($this->id)){
            return $this->id;
        }
        $criteria = new CDbCriteria();
        $criteria->compare("store_card_id", $this->id);
        GasStoreCardDetail::model()->deleteAll($criteria);
    }
    
    /** for HISTORY CUSTOMER STORE CARD
     * @Author: ANH DUNG Now 10, 2016
     * @Todo: get history of customer Bò mối 3 latest Order
     */
    public function windowGetHistory($needMore = []) {
        if(empty($this->customer_id)){
            return array();
        }
        $criteria = new CDbCriteria();
        $criteria->addCondition('t.type_in_out='.STORE_CARD_TYPE_3);
        $criteria->addCondition('t.customer_id='.$this->customer_id);
        $criteria->addCondition('t.reason_false IS NULL OR t.reason_false < 1');
        $criteria->limit = 8;
        $criteria->order = "t.id DESC";
        $models = self::model()->findAll($criteria);
        if(isset($needMore['GetModel'])){
            return $models;
        }
        return $this->windowFormatResponse($models);
    }
    /**
     * @Author: ANH DUNG Aug 26, 2016
     * @Todo: format response to window
     */
    public function windowFormatResponse($models) {
        $aRes = array();
        foreach($models as $model){
            $tmp = array();
            $tmp['agent_id']                = $model->user_id_create;
            $tmp['created_date']            = MyFormat::dateConverYmdToDmy($model->date_delivery, "d-m-Y");
            $tmp['customer_id']             = $model->customer_id;
//            $tmp['customer_id']             = $model->getCustomer('first_name');
            $tmp['employee_maintain_id']    = '';
            $tmp['monitor_market_development_id']   = '';
            $tmp['note']                    = $model->note;
            $tmp['order_type']              = 1;
            $tmp['type_amount']             = 0;
            $tmp['order_detail']            = $model->windowFormatResponseDetail();
            $aRes[] = $tmp;
        }
        return $aRes;
    }
    public function windowFormatResponseDetail() {
        $aRes = array();
        foreach($this->rStoreCardDetail as $key => $item){
            $tmp = array();
            $tmp['amount']          = 0;
            $tmp['materials_id']    = $item->materials_id;
            $tmp['materials_type_id'] = $item->materials_type_id;
            $tmp['price']           = 0;
            $tmp['qty']             = $item->qty;
            $tmp['seri']            = '';
            $aRes[] = $tmp;
        }
        return $aRes;
    }
    /* END HISTORY CUSTOMER STORE CARD */
    
    public function getHtmlDetail(){
        $aMaterial = CacheSession::getListMaterial();
        $str = ''; $sum_sl  = 0; $aSum = [];
        $aDetail = $this->rStoreCardDetail;
        foreach($aDetail as $item){
            if(!isset($aSum[$item->materials_id])){
                $aSum[$item->materials_id] = $item->qty;
            }else{
                $aSum[$item->materials_id] += $item->qty;
            }
        }
        foreach($aSum as $materials_id => $qty){
            $sum_sl += $qty;
            $nameMaterial = isset($aMaterial[$materials_id]) ? $aMaterial[$materials_id] : '';
            $str .= '<br> SL:<b>'.  ActiveRecord::formatCurrency($qty).'</b> - '.$nameMaterial;
        }
       if($sum_sl){
           $str .= '<br> <b>Tổng SL: '.$sum_sl.'</b>';
       }
//       $str .= "<br> <b>Người tạo: </b>".$this->getUidLogin();// Jul 15, 2016 có thể không cần show cái này
       
       return trim($str,'<br>');
    }
   
    /**
     * @Author: ANH DUNG May 12, 2017
     * @Todo: cập nhật hủy order từ thẻ kho qua AppOrder. Chiều từ App sang thẻ kho làm rồi
     * giờ mới làm chiều từ thẻ kho qua APP hix......
     */
    public function appOrderUpdateCancel() {
        if(!$this->canUpdateAppOrder()){
            return ;
        }
        $mAppOrder = $this->getModelAppOrder();
        if(is_null($mAppOrder)){
            return ;
        }
        $aSeri = $mAppOrder->getArraySeriVo();
        $mAppOrder->mapJsonFieldOneDecode('JSON_FIELD', 'json', 'baseArrayJsonDecode');
//        if($this->type_in_out == STORE_CARD_TYPE_3){// Xuất bán
//            $mAppOrder->info_gas        = [];
//        }elseif($this->type_in_out == STORE_CARD_TYPE_5){// thu vỏ
//            $mAppOrder->info_vo        = [];
//        }// Close Aug2717 thử xem có ảnh hưởng gì không, có cần phải if else như trên không
        $mAppOrder->info_gas        = [];
        $mAppOrder->info_vo         = [];
        $mAppOrder->total_gas       = 0;
        $mAppOrder->grand_total     = 0;
        $mAppOrder->total_gas_du    = 0;
            
        $mAppOrder->reason_false    = $this->reason_false;
        $mAppOrder->status          = GasAppOrder::STATUS_CANCEL;
        $mAppOrder->setJsonDataField();
        $mAppOrder->update();
        $mAppOrder->doCancel();
    }
    
    /**
     * @Author: ANH DUNG May 12, 2017
     * @Todo: kiểm tra thẻ kho có hợp lệ để update không
     */
    public function canUpdateAppOrder() {
        $aTypeInOut     = [STORE_CARD_TYPE_3, STORE_CARD_TYPE_5];
        $aTypeCustomer  = [STORE_CARD_KH_BINH_BO, STORE_CARD_KH_MOI];
        if(!in_array($this->type_in_out, $aTypeInOut) || !in_array($this->type_user, $aTypeCustomer)){
            return false;
        }
        return true;
    }
    
    /**
     * @Author: ANH DUNG May 12, 2017
     * @Todo: không cho update nhập vỏ từ thẻ kho, 100% phải làm từ app để update qua
     */
    public function canUpdateNhapVo() {
        $aTypeCustomer  = [STORE_CARD_KH_BINH_BO, STORE_CARD_KH_MOI];
        if($this->type_in_out == STORE_CARD_TYPE_5 && in_array($this->user_id_create, UsersExtend::getAgentRunAppGN())){// thu vỏ
            if(in_array($this->type_user, $aTypeCustomer)){
                return false;
            }
        }
        return true;
    }
    
    /**
     * @Author: ANH DUNG May 12, 2017
     * @Todo: find model app orde tương ứng với thẻ kho
     */
    public function getModelAppOrder() {
        $criteria = new CDbCriteria();
        if($this->type_in_out == STORE_CARD_TYPE_3){// Xuất bán
            $criteria->addCondition('t.store_card_id_gas=' . $this->id );
        }elseif($this->type_in_out == STORE_CARD_TYPE_5){// thu vỏ
            $criteria->addCondition('t.store_card_id_vo=' . $this->id );
        }
        return GasAppOrder::model()->find($criteria);
    }
    
    /**
     * @Author: ANH DUNG Jan 20, 2017 được gọi từ GasstorecardController
     * @Todo: khi agent bấm cập nhật thẻ kho trên web thì update back to AppOrder của KH
     * @note nếu bán val, dây hoặc phu kiện khác gas thì giá = 0 => check lại
     * nếu chưa chạy app order thì chỉ có tác động 1 chiều từ thẻ kho => AppOrder với hàm này appOrderUpdateBack, nên có thể Open
     */
    public function appOrderUpdateBack() {
        if(!$this->canUpdateAppOrder()){
            return ;
        }
        $mAppOrder = $this->getModelAppOrder();
        if($mAppOrder){// for update back
            $this->appOrderDetailMap($mAppOrder);
        }else{// nếu chưa có thì tạo mới để đồng bộ giữa thẻ kho và AppOrder
            /** @note: find thử xem có record dc tạo chưa nếu có thì sử dụng cái bên trên
             * @note2: không xử lý trường hợp else này nữa, vì chắc chắn sẽ có GasAppOrder khi điếu phối tạo đơn hàng
             * @next: làm tạo record GasAppOrder khi điều phối tạo đơn hàng
             * @note3: đơn hàng thu vỏ của Giao nhận thì phải tạo tự động sang bên kia rồi
             */
        }
    }
    
    /**
     * @Author: ANH DUNG Jan 19, 2017
     * @Todo: handle map detail to $mAppOrder
     */
    public function appOrderDetailMap($mAppOrder) {
        $mUsersPrice            =new UsersPrice();
        $mAppCache              =new AppCache();
        $aMaterial              = $mAppCache->getMasterModel('GasMaterials', AppCache::ARR_MODEL_MATERIAL);
        $mAppOrder->aInfoPrice  = $mUsersPrice->getPriceOfCustomer(date('m'), date('Y'), $this->customer_id);
        $mAppOrder->mapJsonFieldOneDecode('JSON_FIELD', 'json', 'baseArrayJsonDecode');
        $mAppOrder->mapJsonFieldOneDecode('JSON_FIELD_INFO', 'json_info', 'baseArrayJsonDecodeV1');
        $mAppOrder->total_gas       = 0;
        $mAppOrder->date_delivery   = $this->date_delivery;
        
        if($this->type_in_out == STORE_CARD_TYPE_3){// Xuất bán
            $aAppOrderDetail = $mAppOrder->getJsonDataField('info_gas');
            $this->appOrderDetailCheck($aMaterial, $mAppOrder, $aAppOrderDetail);
            $mAppOrder->info_gas    = $aAppOrderDetail;
            $mAppOrder->store_card_id_gas = $this->id;
            $mAppOrder->grand_total = $mAppOrder->total_gas  - $mAppOrder->total_gas_du;
        }elseif($this->type_in_out == STORE_CARD_TYPE_5){// thu vỏ
            return ;
            /* Jan 21, 2017 chưa thể xử lý update thu vỏ từ thẻ kho qua AppOrder của KH 
             * vì còn liên quan đến seri vỏ bình và trọng lượng gas dư của vỏ nữa, mà bên thẻ kho chưa làm phần này
             * có nghĩa chỉ cho user cập nhật vỏ từ App
             */
            $aAppOrderDetail = $mAppOrder->getJsonDataField('info_vo');
            $this->appOrderDetailCheck($aMaterial, $mAppOrder, $aAppOrderDetail);
            $mAppOrder->info_vo     = $aAppOrderDetail;
            $mAppOrder->store_card_id_vo = $this->id;
        }
        
        $mAppOrder->setJsonDataField();
        $mAppOrder->setJsonDataField('JSON_FIELD_INFO', 'json_info');
        $mAppOrder->update();
    }
    
    /**
     * @Author: ANH DUNG Jan 20, 2017
     * @Todo: kiểm tra cái nào mới thì thêm, update, xóa
     */
    public function appOrderDetailCheck($aMaterial, $mAppOrder, &$aAppOrderDetail) {
        $aMaterialRoot      = [];
        $aAppOrderDetailOld = $aAppOrderDetail;
        foreach($this->rStoreCardDetail as $item){ // kiểu check này chỉ dùng cho xuất bán gas, vỏ sẽ làm kiểu khác
            $aMaterialRoot[] = $item->materials_id;
            if(!isset($aAppOrderDetail[$item->materials_id])){// nếu chưa có thì add new, đối với vỏ thì không có check kiểu này được.
                $aAppOrderDetail[$item->materials_id] = $this->appOrderDetailBuildOne($aMaterial, $mAppOrder, $item);
            }else{// update qty, amount
                $aAppOrderDetail[$item->materials_id]['qty_real']    = $item->qty*1;
//                $aAppOrderDetail[$item->materials_id]['qty']    = $item->qty*1;
                $mDetail = new GasAppOrderDetail();
                $mDetail->materials_type_id = $aAppOrderDetail[$item->materials_id]['materials_type_id'];
                $mAppOrder->calcAmountGas($mDetail, $item->qty*1);// for live
                
                $aAppOrderDetail[$item->materials_id]['price']  = $mDetail->price;
                $aAppOrderDetail[$item->materials_id]['amount'] = $mDetail->amount;
                $mAppOrder->total_gas                           += $mDetail->amount;// 3. cộng tổng  tiền đơn hàng lại
            }
//            if(in_array($item->materials_type_id, CmsFormatter::$MATERIAL_TYPE_BINHBO_OUTPUT)){
            if($this->type_in_out == STORE_CARD_TYPE_3){// lấy tên tất cả các vật tư Xuất bán
                $mAppOrder->aNameGas[]  = $mAppOrder->getStringNameGas($item->materials_type_id, $item->qty, $item->materials_id);
            }
        }
        // xóa những item đã bị remove khi update storecard
        if(is_array($aAppOrderDetailOld)){
            foreach($aAppOrderDetailOld as $materials_id => $appOrderDetail){
                if(!in_array($materials_id, $aMaterialRoot)){
                    unset($aAppOrderDetail[$materials_id]);
                }
            }
        }
        
        if(count($mAppOrder->aNameGas)){// Xuất bán
            $mAppOrder->name_gas    = implode(', ', $mAppOrder->aNameGas);
        }
    }
    
    public function appOrderDetailBuildOne($aMaterial, $mAppOrder, $storecardDetail) {
        $mDetail = new GasAppOrderDetail();
        $mDetail->materials_type_id  = $storecardDetail->materials_type_id;
        $mDetail->materials_id       = $storecardDetail->materials_id;
        $mDetail->materials_name     = isset($aMaterial[$mDetail->materials_id]) ? $aMaterial[$mDetail->materials_id]['name'] : '';
        $mDetail->unit               = isset($aMaterial[$mDetail->materials_id]) ? $aMaterial[$mDetail->materials_id]['unit'] : '';
        $mDetail->qty        = $storecardDetail->qty*1;
        $mDetail->price      = 0;
        $mDetail->amount     = 0;
        $mAppOrder->calcAmountGas($mDetail, $mDetail->qty);// for live
        $mDetail->qty_real   = $mDetail->qty;
        $mDetail->seri       = '';
        $mDetail->kg_empty   = '';
        $mDetail->kg_has_gas = '';
        $json = json_encode($mDetail);
        return json_decode($json, true);
    }
    
    /**
     * @Author: ANH DUNG Jan 21, 2017
     * @Todo: xóa AppOrder liên quan
     * @big_bug: Feb 16, 2017 đã xóa hết thẻ kho của điều phối ngày Feb 16, 2017, sẽ backup lại từ file
     * do xóa từ GasAppOrder, nó xóa ngược lại table Order
     * @note: bỏ ngay nút xóa trên tòan hệ thống. Mỗi lần làm lệnh delete thì hãy check thật kỹ các trường hợp
     */
    public function deleteAppOrder() {
        $criteria = new CDbCriteria();
        if($this->type_in_out == STORE_CARD_TYPE_3){// Xuất bán
            $criteria->addCondition('t.store_card_id_gas='.$this->id);
        }elseif($this->type_in_out == STORE_CARD_TYPE_5){// thu vỏ
            $criteria->addCondition('t.store_card_id_vo='.$this->id);
        }else{// add @big_bug: Feb 16, 2017 đã xóa hết thẻ kho của điều phối ngày Feb 16, 2017. 
            return ;
        }
        $models = GasAppOrder::model()->findAll($criteria);
        Users::deleteArrModel($models);
    }
    
    /**
     * @Author: ANH DUNG Feb 23, 2017
     * @Todo: tự động tạo thẻ kho nhập gas + xuất vỏ liên quan cho các đại lý
     * khi Kho Phước Tân tạo phiếu giao cho các đl: Xuất Nội Bộ, Nhập Nội Bộ
     */
    public function autoMakeRecordForAgent() {
        $this->deleteSubRecord();
//        return ; // tạm chặn lại Jun 25, 2017 chưa xử lý đc, vì sau khi giao cho ĐL xưởng 2 ngày sau mới tạo thẻ kho
        $aTypeUser          = [CUSTOMER_IS_AGENT];
        $aTypeInOutAllow    = [STORE_CARD_TYPE_1, STORE_CARD_TYPE_4];// cho phép Xuất Nội Bộ, Nhập Nội Bộ
        if(!in_array($this->type_user, $aTypeUser) || !in_array($this->type_in_out, $aTypeInOutAllow)){
            return ;
        }
//        if($this->user_id_create != MyFormat::KHO_PHUOC_TAN || ( $this->user_id_create == MyFormat::KHO_PHUOC_TAN && !in_array($this->customer_id, UsersExtend::getAgentAutoGenStorecard())) ){
        if($this->fromWebUpdate && in_array($this->user_id_create, GasCheck::getAgentNotGentAuto())){
//            return ;// Aug0317 vì các kho sẽ cập nhật tài xế cho thẻ kho, sẽ không sinh tự động ngược lại cho các đl nữa, sinh ngược lại sẽ bị dư phiếu
            // Aug2317 Mở cho Duyên BCát sửa phiếu tài xế sai, những phiếu do tài xế làm,
            // Sep0417 mở cho Duyên B Cát sửa phiếu không ảnh hưởng đến đl khác, ở đây là nhập số xe cho tài xế, để sửa dữ liệu
        }
        // cho chạy nhập xuất trên toàn hệ thống Jul2817
        
        /* 1. Check, chỉ xử lý với KH là đại lý và kiểu là nhập xuất nội bộ
         * 1. xóa sub record thêm trước đó
         * 2. build record and insert
         * 2.1 copy root record and insert 
         * 2.2 copy detail record and insert 
         */
        $mStorecardNew = $this->makeSubRecord();
        $this->makeSubRecordDetail($mStorecardNew);
        
        if($this->sourceFrom == BaseSpj::SOURCE_FROM_APP){
            Sta2::setCacheInventoryAllAgent();
        }
    }
    
    /**
     * @Author: ANH DUNG Feb 23, 2017
     * @Todo: xóa thẻ kho con trước khi insert
     */
    public function deleteSubRecord() {
        if(empty($this->id)){
            return ;
        }
        $criteria = new CDbCriteria();
        $criteria->addCondition('t.parent_id=' . $this->id);
        $model = self::model()->find($criteria);
        if($model){
            $model->delete();
        }
    }
    
    /**
     * @Author: ANH DUNG Feb 23, 2017
     * @Todo: Make root thẻ kho con trước khi insert detail
     */
    public function makeSubRecord() {
        $mStorecardNew = new GasStoreCard();
        $aFieldNotCopy = array('id', 'parent_id', 'warehouse_new', 'store_card_no', 'customer_parent_id');
        // handle add promotion of user
        MyFormat::copyFromToTable($this, $mStorecardNew, $aFieldNotCopy);
        $mStorecardNew->parent_id           = $this->id;
        $mStorecardNew->user_id_create      = $this->customer_id;
        $mStorecardNew->customer_id         = $this->user_id_create;
//        $mStorecardNew->phu_xe_2            = GasStoreCardApi::IS_ADMIN_GEN_FROM_ORDER;// Close Aut0517 vì đã sử dụng để nhập phụ xe 2
        $mStorecardNew->pay_now             = GasStoreCard::CREATE_AUTO_SUB_STORECARD;// xử lý 
        $mStorecardNew->buildStoreCardNo();
        $mAgent = Users::model()->findByPk($mStorecardNew->user_id_create);
        $mStorecardNew->province_id_agent = $mAgent->province_id;
        
        if($this->type_store_card == TYPE_STORE_CARD_IMPORT){
            $mStorecardNew->type_store_card = TYPE_STORE_CARD_EXPORT;
            $mStorecardNew->type_in_out     = STORE_CARD_TYPE_4;
        }elseif($this->type_store_card == TYPE_STORE_CARD_EXPORT){
            $mStorecardNew->type_store_card    = TYPE_STORE_CARD_IMPORT;
            $mStorecardNew->type_in_out        = STORE_CARD_TYPE_1;
        }else{
//            return;// không biết nên return không hay cứ để cho chạy nếu có mở rộng type khác
        }
        $mStorecardNew->save();
        return $mStorecardNew;
    }
    
    /**
     * @Author: ANH DUNG Feb 23, 2017
     * @Todo: Make detail thẻ kho
     */
    public function makeSubRecordDetail($mStorecardNew) {
        $sql = ''; $aRowInsert = [];
        $date_delivery_bigint   = strtotime($mStorecardNew->date_delivery);
        foreach($this->rStoreCardDetail as $mDetail){
            $aRowInsert[]="(
                        '$mStorecardNew->uid_login',
                        '$mStorecardNew->warehouse_new',
                        '$mStorecardNew->id',
                        '$mStorecardNew->customer_id',
                        '$mStorecardNew->customer_parent_id',
                        '$mStorecardNew->type_user',
                        '$mDetail->sale_id',
                        '$mDetail->sale_type',
                        '$mStorecardNew->type_store_card',
                        '$mStorecardNew->type_in_out',
                        '$mStorecardNew->date_delivery',
                        '$date_delivery_bigint',
                        '$mDetail->materials_id',
                        '$mDetail->materials_type_id',
                        '$mDetail->qty',
                        '$mDetail->price',
                        '$mDetail->amount',
                        '$mStorecardNew->user_id_create',
                        '$mStorecardNew->delivery_person',
                        '$mStorecardNew->province_id_agent',
                        '$mStorecardNew->car_id',
                        '$mStorecardNew->driver_id_1',
                        '$mStorecardNew->driver_id_2',
                        '$mStorecardNew->phu_xe_1',
                        '$mStorecardNew->phu_xe_2',
                        '$mStorecardNew->road_route'
                        )";
        }
        
        
        $tableName = GasStoreCardDetail::model()->tableName();
        $sql = "insert into $tableName (
                    uid_login,
                    warehouse_new,
                    store_card_id,
                    customer_id,
                    customer_parent_id,
                    type_customer,
                    sale_id,
                    sale_type,
                    type_store_card,
                    type_in_out,
                    date_delivery,
                    date_delivery_bigint,
                    materials_id,
                    materials_type_id,
                    qty,
                    price,
                    amount,
                    user_id_create,
                    delivery_person,
                    province_id_agent,
                    car_id,
                    driver_id_1,
                    driver_id_2,
                    phu_xe_1,
                    phu_xe_2,
                    road_route
                    ) values ".implode(',', $aRowInsert);
        if(count($aRowInsert)>0)
            Yii::app()->db->createCommand($sql)->execute();
    }
    
    /**
     * @Author: ANH DUNG Feb 24, 2017
     * @Todo: xử lý cập nhật lý do hủy và ghi chú
     */
    public function handleCancel() {
        $this->scenario = null;
        $this->update(array('reason_false','note'));
        $this->deleteDetail();
        $this->deleteSubRecord();
        $this->appOrderUpdateCancel();
    }
    
    /**
     * @Author: ANH DUNG Feb 25, 2017
     * @Todo: handle app post and validate
     */
    public function handlePost($q) {
        if(isset($q->list_id_image) && is_array($q->list_id_image)){
            $this->list_id_image = $q->list_id_image;
        }
        $this->customer_id      = $q->customer_id;
        $this->type_in_out      = $q->type_in_out;
        $this->setTypeStorecard();
        $this->date_delivery    = $q->date_delivery;
        $this->note              = trim(MyFormat::escapeValues($q->note));
        
        $this->uid_login        = $this->mAppUserLogin->id;
        $this->getTypeCustomer();
        $this->buildStoreCardNo();
        $this->makeParamsPostDetail($q->order_detail);
        if(!$this->hasErrors()){
            $this->validate();
        }
        $this->handlePostCheckDate();// Jun2718 đóng tạo cho Vũng Tàu Sửa
        
        if(!$this->isNewRecord){
            if(!$this->employeeCanUpdate()){
                $this->addError('id', 'Thẻ kho đã bị khóa, bạn không thể cập nhật');
            }
        }
    }

    /** @Author: ANH DUNG Mar 12, 2017
     *  @Todo: check date valid
     */
    public function handlePostCheckDate() {
        $date_check = MyFormat::dateConverDmyToYmd($this->date_delivery);
        $dayAllow   = MyFormat::modifyDays(date('Y-m-d'), Yii::app()->params['DaysUpdateAppBoMoi'], '-');
        $ok         = MyFormat::compareTwoDate($date_check, $dayAllow);
        if(!$ok){
            $this->addError('date_delivery', 'Ngày '.$this->date_delivery.' không hợp lệ');
        }
        // May1019 check lock *Chặn không cho NVGH lập phiếu xuất khác nữa nha
        $aRoleLockKtkv      = [ROLE_ACCOUNTING_ZONE, ROLE_ACCOUNTING];
        $aLockTypeKtkv      = [STORE_CARD_TYPE_24];
        $aRoleLockPvkh      = [ROLE_EMPLOYEE_MAINTAIN, ROLE_EMPLOYEE_MARKET_DEVELOPMENT];
        $aLockTypePvkh      = [STORE_CARD_TYPE_7, STORE_CARD_TYPE_24];
        if(in_array($this->mAppUserLogin->role_id, $aRoleLockPvkh) && in_array($this->type_in_out, $aLockTypePvkh)){
            $this->addError('type_in_out', 'Không thể tạo thẻ kho xuất tặng khác, vui lòng liên hệ kế toán khu vực để hỗ trợ');
        }
        if(in_array($this->mAppUserLogin->role_id, $aRoleLockKtkv) && in_array($this->type_in_out, $aLockTypeKtkv)){
            $this->addError('type_in_out', 'Không thể tạo Xuất kho treo CN trừ lương, vui lòng liên hệ kế toán khu vực, kế toán văn phòng để hỗ trợ');
        }
    }
    
    public function setTypeStorecard() {
        $aLockType = [STORE_CARD_TYPE_3, STORE_CARD_TYPE_5, STORE_CARD_TYPE_9];
        if(in_array($this->type_in_out, $aLockType) && $this->mAppUserLogin->role_id != ROLE_DRIVER){
            throw new Exception('Không thể tạo thẻ kho xuất bán, xuất tặng, nhập vỏ. Bạn phải gọi lên điều phối bò mối để tạo');
        }
        
        $this->type_store_card = TYPE_STORE_CARD_IMPORT;
        if(array_key_exists($this->type_in_out, CmsFormatter::$STORE_CARD_TYPE_EXPORT)){
            $this->type_store_card = TYPE_STORE_CARD_EXPORT;
        }
    }
    
    /**
     * @Author: ANH DUNG Feb 25, 2017
     * @Todo: handle save
     */
    public function handleSave($q) {
        if($this->isNewRecord){
            $this->save();// save luôn để còn save detail
        }else{
            $this->update();
        }
        GasStoreCard::saveStoreCardDetail($this);
        $this->saveFile();
        $this->deleteFile();
        $this->handleAutoCreateDebts();// Jul0719 DungNT
    }

    /**
     * @Author: ANH DUNG Jan 19, 2017
     * @Todo: init param $_POST['materials_id'] và $_POST['materials_qty']
     * @param: $aDetailInfo is $this->getJsonDataField('info_gas');
     * xử lý cộng dồn với thu vỏ, còn các loại khác thì gắn biến bình thường
     */
    public function makeParamsPostDetail($aDetailInfo) {
        $aMaterial      = [];
        $mAppCache      = new AppCache();
        if($this->type_in_out == STORE_CARD_TYPE_3){
            $aMaterial      = $mAppCache->getMasterModel('GasMaterials', AppCache::ARR_MODEL_MATERIAL);
        }
        $aMaterialsId = $aMaterialsQty = [];
        foreach($aDetailInfo as $objDetail){
            $aMaterialsId[]     = $objDetail->materials_id;
            $aMaterialsQty[]    = $objDetail->qty;
            if($this->type_in_out == STORE_CARD_TYPE_3){
                $materials_type_id  = isset($aMaterial[$objDetail->materials_id]) ? $aMaterial[$objDetail->materials_id]['materials_type_id'] : '';
                if(in_array($materials_type_id, CmsFormatter::$MATERIAL_TYPE_BINHBO_OUTPUT)){
                    $this->addError('type_in_out', 'Không thể tạo xuất bán Gas, hãy gọi cho điều phối tạo xuất bán Gas');
                    return ;
                }
            }
        }
        $_POST['materials_id']  = $aMaterialsId;
        $_POST['materials_qty'] = $aMaterialsQty;
    }
    
    /**
     * @Author: ANH DUNG Feb 27, 2017
     * @Todo: get model thẻ kho của đại lý, những giao nhận của đl đó sẽ được view
     */
    public function getModelApp($id) {
        if(empty($id)){
            return null;
        }
        $criteria = new CDbCriteria();
        $criteria->addCondition('t.id='.$id );
        if(!empty($this->mAppUserLogin->parent_id)){
            //Jun 26, 2017 tạm đóng lại vì thấy không cần thiết phải làm thế này, có id cho view luôn
//            $criteria->addCondition('t.user_id_create='.$this->mAppUserLogin->parent_id);
        }
        return self::model()->find($criteria);
//        $mAppCache = new AppCache();
//        $aEmployeeMaintain = $mAppCache->getUserMaintainOfAgent($this->user_id_create);
    }
    
    /**
     * @Author: ANH DUNG Feb 26, 2017 app view
     * format record thẻ kho view from app customer BoMoi
     */
    public function appView() {
        $aRes           = array();// order_view, order_edit
        $mAppCache      = new AppCache();
        $aMaterial      = $mAppCache->getMasterModel('GasMaterials', AppCache::ARR_MODEL_MATERIAL);
        $aRes['id']                 = $this->id;
        $aRes['code_no']            = $this->store_card_no.' - '.$this->getTypeInOutText();
        $aRes['date_delivery']      = $this->getDateDelivery();
//        $aRes['type_in_out_text']   = $this->getTypeInOutText();
        $aRes['customer_name']      = $this->getCustomer();
        $aRes['customer_address']   = $this->getCustomer('address');
        $aRes['note']               = $this->getNoteOnly();
        $aRes['created_date']       = $this->getCreatedDate();
        $aRes['allow_update']       = $this->employeeCanUpdate();
        $aRes['type_in_out']        = $this->type_in_out;
        $aRes['customer_id']        = $this->customer_id;
        $aRes['list_image']         = GasFile::apiGetFileUpload($this->rFile);

        $this->appViewDetail($aRes, $aMaterial);
        
        return $aRes;
    }
    
    /**
     * @Author: ANH DUNG Nov 06, 2016
     * @Todo: appview format order detail view
     */
    public function appViewDetail(&$aRes, $aMaterial) {
        $aRes['order_detail'] = [];$sumQty= 0;
        foreach($this->rStoreCardDetail as $model){
            $tmp = [];
            $tmp['materials_type_id'] = $model->materials_type_id;
            $tmp['materials_id']      = $model->materials_id;
            $tmp['materials_name']    = isset($aMaterial[$model->materials_id]) ? $aMaterial[$model->materials_id]['name'] : '';
//            $tmp['qty']               = ActiveRecord::formatCurrency($model->qty);// Close Sep1918 để app hiển thị đúng, nếu để dấu, thì sẽ hiện thị sai
            $tmp['qty']               = $model->qty*1;
            $tmp['unit']              = isset($aMaterial[$model->materials_id]) ? $aMaterial[$model->materials_id]['unit'] : '';
            $tmp['materials_no']      = isset($aMaterial[$model->materials_id]) ? $aMaterial[$model->materials_id]['materials_no'] : '';
            $aRes['order_detail'][]   = $tmp;
            $sumQty     += $model->qty;
        }
        $aRes['total_qty']          = ActiveRecord::formatCurrency($sumQty);
    }
    
    /** @Author: ANH DUNG  Feb 26, 2017 -- handle listing api */
    public function handleApiList(&$result, $q, $mUser) {
        // 1. get list order by user id
        $dataProvider   = $this->ApiListing($q, $mUser);
        $models         = $dataProvider->data;
        $CPagination    = $dataProvider->pagination;
        $result['total_record'] = $CPagination->itemCount;
        $result['total_page']   = $CPagination->pageCount;
        $result['record']       = array();
        if( $q->page >= $CPagination->pageCount ){
            $result['record'] = array();
        }else{
            foreach($models as $model){
                $model->mAppUserLogin       = $mUser;// for canPickCancel
//                $model->getCustomerInfo();// Feb 02, 2017 không lấy kiểu này nữa, mà đã set trong json
                $aRes = [];
                $aRes['id']                 = $model->id;
                $aRes['code_no']            = $model->store_card_no . ' - '.$model->getCreatedDateApp('H:i');
                $aRes['date_delivery']      = $model->getDateDelivery();
        //        $aRes['type_in_out']        = $this->getTypeInOutText();
                $aRes['customer_name']      = $model->getTypeInOutText().' - '.$model->getCustomer();
                $aRes['note']               = $model->getNoteOnly();
                $aRes['created_date']       = ''; // Mar 10, 2017 Close $model->getCreatedDate();
                $aRes['allow_update']       = $model->employeeCanUpdate();
                
                $result['record'][]         = $aRes;
            }
        }
    }
    
    /**
    * @Author: ANH DUNG  Feb 26, 2017 
    * @Todo: get listing GasStoreCard
    * @param: $q object post params
    */
    public function ApiListing($q, $mUser) {
        $sTypeAdmin = GasStoreCard::CREATE_AUTO_SUB_STORECARD;
        $criteria = new CDbCriteria();
        if(in_array($this->user_id_create, GasCheck::getAgentNotGentAuto())){
            $criteria->addCondition("(t.uid_login=$mUser->id AND t.user_id_create=$this->user_id_create)");
        }elseif(in_array($mUser->role_id, EmployeeCashbook::getRoleLikeEmployeeMaintain())){
            $criteria->addCondition("(t.uid_login=$mUser->id AND t.user_id_create=$this->user_id_create)" .' OR ( t.user_id_create='.$this->user_id_create." AND t.pay_now IN ($sTypeAdmin) )");
        }else{
            $criteria->addCondition('t.uid_login=1');// không cho user nào xem nữa
        }
        $criteria->order = 't.id DESC';
        $dataProvider=new CActiveDataProvider($this, array(
            'criteria' => $criteria,
            'pagination'=>array(
                'pageSize' => GasAppOrder::API_LISTING_ITEM_PER_PAGE,
//                'pageSize' => 2,
                'currentPage' => (int)$q->page,
            ),
          ));
        return $dataProvider;
    }
    
    /**
     * @Author: ANH DUNG Mar 04, 2017
     * @Todo: get model for app update
     */
    public function loadAppUpdate($q) {
        if(empty($q->id)){
            return null;
        }
        $criteria = new CDbCriteria();
        $criteria->addCondition('t.id=' . $q->id);
        $criteria->addCondition('t.uid_login=' . $this->mAppUserLogin->id);
        return self::model()->find($criteria);
    }
    
    
    /**
     * @Author: ANH DUNG Mar 12, 2017
     * @Todo: chặn các đại lý chạy app giao nhận, ko cho tạo thẻ kho
     */
    public function isLockAgentRunApp() {
        $cRole = MyFormat::getCurrentRoleId();
        if($cRole == ROLE_SUB_USER_AGENT){
            $cAgent = MyFormat::getAgentId();
            if(in_array($cAgent, UsersExtend::getAgentRunAppGN())){
                return true;
            }
        }
        return false;
    }
    
    /**
     * @Author: ANH DUNG Apr 27, 2017
     * @Todo: map cac thong tin cua the kho xuat ban
     */
    public function mapStoreCardXuatBan($q, $aInfoPrice) {
        try {
        $this->user_id_create = $q->agent_id;
        $this->customer_id    = $q->customer_id;
        $this->note           = trim($q->note).'. '.$aInfoPrice['sPrice'];
        $this->uid_login      = $this->mAppUserLogin->id;
        $this->getTypeCustomer();// get type_user
        $this->type_store_card    = TYPE_STORE_CARD_EXPORT;
        $this->type_in_out        = STORE_CARD_TYPE_3;
        $this->pay_now            = GasStoreCard::CREATE_FROM_WINDOW;
        $this->buildStoreCardNo();
        $this->validate();
        $this->checkChanHang();
        } catch (Exception $exc) {
            throw new Exception($exc->getMessage());
        }
    }
    
    /**
     * @Author: ANH DUNG Apr 27, 2017
     * @Todo: map cac thong tin cua the kho nhap vo
     */
    public function mapStoreCardNhapVo($q, $mStoreCardXuatBan) {
        try {
//        $mStoreCardNhapVo                       = new GasStoreCard('WindowCreate');
        $this->user_id_create       = $mStoreCardXuatBan->user_id_create;
        $this->customer_id          = $mStoreCardXuatBan->customer_id;
        $this->uid_login            = $mStoreCardXuatBan->uid_login;
        $this->date_delivery        = $mStoreCardXuatBan->date_delivery;
        $this->type_user            = $mStoreCardXuatBan->type_user;// loai KH
        $this->type_store_card      = TYPE_STORE_CARD_IMPORT;
        $this->type_in_out          = STORE_CARD_TYPE_5;
        $this->pay_now              = GasStoreCard::CREATE_FROM_WINDOW;
        $this->buildStoreCardNo();
        if($q->type == GasAppOrder::STORECARD_THU_VO){
            $this->note             = $mStoreCardXuatBan->note;
        }
        if($this->allowCreate){//Jul2217 không tạo sẵn thẻ kho khi điều phối điều nữa
            $this->save();
        }
        return $this;
        } catch (Exception $exc) {
            throw new Exception($exc->getMessage());
        }
    }
    
    public function makeAppOrder($q, &$mAppOrder) {
        try {
        $mAppOrder->mAppUserLogin   = $this->mAppUserLogin;
        $mAppOrder->customer_id     = $this->customer_id;
        $mAppOrder->agent_id        = $this->user_id_create;
        $mAppOrder->note_employee   = $this->note;
        $mAppOrder->date_delivery   = $this->date_delivery;
        $mAppOrder->customer_contact = $q->phone_number;
        $mAppOrder->type            = GasAppOrder::DELIVERY_NOW;
        $mAppOrder->pay_direct      = $q->pay_direct;
        $mAppOrder->delivery_timer  = $q->delivery_timer;
        $mAppOrder->formatDbDatetime();
        if($q->type == GasAppOrder::STORECARD_THU_VO){
            $mAppOrder->pay_direct      = GasAppOrder::TYPE_THU_VO;
            $mAppOrder->note_employee   = '[Thu Vỏ] '.$mAppOrder->note_employee;
        }
        $mAppOrder->isDieuPhoiCreate = true;
        $mAppOrder->handlePost($q);
        if($mAppOrder->hasErrors()){
            throw new Exception(HandleLabel::FortmatErrorsModel($mAppOrder->getErrors()));
        }
        $mAppOrder->status = GasAppOrder::STATUS_CONFIRM;
        $mAppOrder->handleSave();
        $mAppOrder->notifyEmployeeMaintain();
        } catch (Exception $exc) {
            throw new Exception($exc->getMessage());
        }
    }
    
    /**
     * @Author: ANH DUNG Apr 27, 2017
     * @Todo: Kiểm tra có phải tách đơn hàng không
     */
    public function needSplit($q) {
        $ok = false;
        if(  $q->b50 > 1 || $q->b45 > 1 ||
            ($q->b50>0 && $q->b45>0) ||
            ($q->b50>0 && $q->b12>0) ||
            ($q->b45>0 && $q->b12>0) ||
            ($q->b12 >= GasStoreCard::MAX_B12)
        ){
            $ok = true;
        }
        return $ok;
    }

    /**
     * @Author: ANH DUNG Apr 26, 2017
     * @Todo: tách đơn hàng bò mối nếu nhiều bình lớn quá 
     * @Param: nếu b45, 50 lớn hơn 1 => tách, nếu b12 > 4 tách
     */
    public function handleSplitOrderBig($q, $aInfoPrice) {
        try{
        /* 1. xác định xem chia bình 50Kg không
         * Nếu có cả bình 50 và 12 thì chia luôn ra làm 2, 1 loại chờ bình 50, 1 loại bình 12
         * Với loại bình 50 thì mỗi bình chia 1 đơn, bình 12 thì  b12 > 4 tách
         * làm thẻ kho và bên AppOrder
         */
        $qNew = clone $q;
        $this->handleSplitB50($qNew, $aInfoPrice, $q->b50);
        $this->handleSplitB45($qNew, $aInfoPrice, $q->b45);
        
        $qNew->b50 = 0; $qNew->b45 = 0;
        if($q->b12 >= GasStoreCard::MAX_B12 ){// 1 loại bình 12
            $qty1 = (int)($q->b12/2);
            $qty2 = $q->b12 - $qty1;
            $qNew->b12 = $qty1;
            $this->makeOneStoreCard($qNew, $aInfoPrice);
            $qNew->b12 = $qty2;
            $this->makeOneStoreCard($qNew, $aInfoPrice);
        }elseif($q->b12>0){
            $qNew->b12 = $q->b12;
            $this->makeOneStoreCard($qNew, $aInfoPrice);
        }

        } catch (Exception $exc) {
            throw new Exception($exc->getMessage());
        }
    }
    
    /**
     * @Todo: xu ly tach tung don hang
     * với b50 và b45 có 1 bình chạy 1 lần, nhiều bình chạy nhiều lần
     */
    public function handleSplitB50($qNew, $aInfoPrice, $qtyReal) {
        $qNew->b50 = 1; $qNew->b45 = 0; $qNew->b12 = 0;
        // Oct0417 xử lý tách bình 50 và 45 ra 2 đơn, không tách nhiều như trước nữa
        if(empty($qtyReal)){
            return ;
        }
        if($qtyReal == 1){
            $this->makeOneStoreCard($qNew, $aInfoPrice);
            return ;
        }
        $qty1 = (int)($qtyReal/2);
        $qty2 = $qtyReal - $qty1;

        $qNew->b50 = $qty1;
        $this->makeOneStoreCard($qNew, $aInfoPrice);
        $qNew->b50 = $qty2;
        $this->makeOneStoreCard($qNew, $aInfoPrice);
        
//        for($i=0; $i < $qtyReal; $i++){// Close on Oct0417
//            $this->makeOneStoreCard($qNew, $aInfoPrice);
//        }
    }
    public function handleSplitB45($qNew, $aInfoPrice, $qtyReal) {
        $qNew->b50 = 0; $qNew->b45 = 1; $qNew->b12 = 0; 
        // Oct0417 xử lý tách bình 50 và 45 ra 2 đơn, không tách nhiều như trước nữa
        if(empty($qtyReal)){
            return ;
        }
        if($qtyReal == 1){
            $this->makeOneStoreCard($qNew, $aInfoPrice);
            return ;
        }
        $qty1 = (int)($qtyReal/2);
        $qty2 = $qtyReal - $qty1;

        $qNew->b45 = $qty1;
        $this->makeOneStoreCard($qNew, $aInfoPrice);
        $qNew->b45 = $qty2;
        $this->makeOneStoreCard($qNew, $aInfoPrice);
        
//        for($i=0; $i < $qtyReal; $i++){// Close on Oct0417
//            $this->makeOneStoreCard($qNew, $aInfoPrice);
//        }
    }
    
    /**
     * @Author: ANH DUNG Apr 27, 2017
     * @Todo: move code cũ vào 1 chỗ nếu không split đơn hàng
     * trường hợp else sẽ xử lý tách đơn hàng
     * xử lý save cho 1 đơn hàng, trong trường hợp phải tách thì đưa vào for và gọi hàm này, đổi $q
     * @param: $sendSMS ở đây để $sendSMS = 0 thì gửi, khác 0 thì ko gửi, tận dụng biến $i trong vòng for
     */
    public function makeOneStoreCard($q, $aInfoPrice) {
        try {
        // 2. get post and validate
        $mStoreCardXuatBan = new GasStoreCard('WindowCreate');
        $mStoreCardXuatBan->mAppUserLogin   = $this->mAppUserLogin;
        $mStoreCardXuatBan->date_delivery   = $this->date_delivery;
        $mStoreCardXuatBan->mapStoreCardXuatBan($q, $aInfoPrice); // $this->windowGetPostStoreCard($q, $mStoreCard, $mUser);
        if(!in_array($mStoreCardXuatBan->user_id_create, UsersExtend::getAgentNotRunApp())){
            $this->allowCreate = false;
//            return ;// Jul_2117 Agent chua chay app thi nhu cũ, Agent da chay app roi thi se sinh the kho khi bam hoan thanh + bam luu sau khi hoan thanh
            // sẽ sinh thẻ kho khi bấm xác nhận trên app
        }
        
        if($mStoreCardXuatBan->hasErrors()){
            throw new Exception(HandleLabel::FortmatErrorsModel($mStoreCardXuatBan->getErrors()));
        }
        // Jan 21, 2017 map thêm tạo AppOder đưa đơn hàng này vào app cho KH bò mối
        $mAppOrder = new GasAppOrder('ApiCreate');
        $mAppOrder->typeStorecard = $q->type;
        $mStoreCardXuatBan->makeAppOrder($q, $mAppOrder);
        /** Mar 1, 2017 xử lý đơn hàng chỉ thu vỏ
         * 1/ xử lý bên thẻ kho chỉ save thu vỏ -- done
         * 2/ xử lý bên GasAppOrder chỉ save bên vỏ -- done
         *  và update back lại thẻ kho vỏ, không update thẻ kho gas
         */

        if($q->type == GasAppOrder::STORECARD_NORMAL && $this->allowCreate){
            $mStoreCardXuatBan->save();
        }
        $mStoreCardNhapVo               = new GasStoreCard('WindowCreate');
        $mStoreCardNhapVo->allowCreate  = $this->allowCreate;
        $mStoreCardNhapVo               = $mStoreCardNhapVo->mapStoreCardNhapVo($q, $mStoreCardXuatBan);
        if($this->allowCreate){
            $mAppOrder->makeStorecardFromDieuPhoi($mStoreCardXuatBan, $mStoreCardNhapVo); // Jan 21, 2017 -- làm nhập detail cho bán gas và vỏ
        }
        $mStoreCardXuatBan->makeSocketNotify($this->mAppUserLogin);// Oct 02, 2016
        if(GasCheck::isServerLive() && $this->sendSms){
//        if(1){// only dev test -- run ok
            $this->sendSms = false;
//            if(in_array($mStoreCardXuatBan->user_id_create, UsersExtend::getAgentForwardPhone())){
            if(!in_array($mStoreCardXuatBan->user_id_create, UsersExtend::getAgentNotRunApp())){
                return ;// May 30, 2017 Không gửi SMS cho các đại lý chạy app nữa 
            }
            GasScheduleSms::storeCardCreate($mStoreCardXuatBan);// Jul 19, 2016
        }
        } catch (Exception $exc) {
            throw new Exception($exc->getMessage());
        }
    }
    
    /**
     * @Author: ANH DUNG May 22, 2017
     * @Todo: xử lý tạo đơn hàng trên web, không thông qua PM window của Nguyên nữa
     */
    public function callCenterCreate($q, &$json) {
        try{
        $this->checkRequest($q);
        $mAppOrder = new GasAppOrder();
        $mAppOrder->customer_id     = $q->customer_id;
        $mAppOrder->c_month         = date('m');
        $mAppOrder->c_year          = date('Y');
        $mAppOrder->checkEmptySetupPrice();
        $mGasRemain = new GasRemain();
//        if(!array_key_exists($q->agent_id, $mGasRemain->getArrayAgentCar()) && $this->needSplit($q) && $q->type == GasAppOrder::STORECARD_NORMAL){
        if(0){// Oct1118 bỏ tách đơn
            $this->handleSplitOrderBig($q, $mAppOrder->aInfoPrice);
        }else{
            $this->makeOneStoreCard($q, $mAppOrder->aInfoPrice);
        }
        $json['msg'] = 'Tạo mới thẻ kho thành công';
        } catch (Exception $ex) {
            throw new Exception($ex->getMessage());
        }
    }
    
    /**
     * @Author: ANH DUNG May 23, 2017
     * @Todo: xử lý check request hợp lệ
     */
    public function checkRequest($q) {
        try{
            $aAgentLimit = [MyFormat::KHO_BEN_CAT, MyFormat::KHO_PHUOC_TAN];
            if(in_array($q->agent_id, $aAgentLimit)){
                throw new Exception('Kho Bến Cát, Kho Phước Tân không thể tạo thẻ kho. Vui lòng chọn đại lý khác');
            }

            $this->date_delivery = MyFormat::dateDmyToYmdForAllIndexSearch($q->date_delivery);
//            $dateCheck      = MyFormat::modifyDays(date('Y-m-d'), 1);
//            if(MyFormat::compareTwoDate($date_delivery, $dateCheck)){
//                throw new Exception('Ngày tạo thẻ kho không hợp lệ: '.$q->date_delivery);
//            }// May 23, 2017 chưa cần check cái này vì đã limit ở client rồi
//            return $date_delivery;
        } catch (Exception $ex) {
            throw new Exception($ex->getMessage());
        }
    }
    
    /********** FOR FILE HANDLE ************************/
     /**
     * @Author: ANH DUNG Oct 27, 2015
     * @Todo: Api validate multi file
     * @param: $this là model chứa error
     * ở đây là model GasUpholdReply
     */
    public function apiValidateFile() {
    try{
        $ok=false; // for check if no file submit
        if(isset($_FILES['file_name']['name'])  && count($_FILES['file_name']['name'])){
            foreach($_FILES['file_name']['name'] as $key=>$item){
                $mFile = new GasFile('UploadFile');
                $mFile->file_name  = CUploadedFile::getInstanceByName( "file_name[$key]");
                $mFile->validate();
                if(!is_null($mFile->file_name) && !$mFile->hasErrors() ){
                    $ok=true;
                    MyFormat::IsImageFile($_FILES['file_name']['tmp_name'][$key]);
//                    $FileName = MyFunctionCustom::remove_vietnamese_accents($mFile->file_name->getName());
//                    if(strlen($FileName) > 100 ){
//                        $mFile->addError('file_name', "Tên file không được quá 100 ký tự, vui lòng đặt tên ngắn hơn");
//                    }// không cần lấy file_name
                }
                if($mFile->hasErrors()){
                    $this->addError('file_name', $mFile->getError('file_name'));
                }
            }
        }
        
        if($this->isNewRecord && !$ok){
            $this->addError('file_name', 'Có lỗi. Bạn phải chụp hình chứng từ của thẻ kho');
        }
        
    } catch (Exception $ex) {
        throw new Exception($ex->getMessage());
    }
    }
    
    /** @Author: ANH DUNG May 18, 2017
     * @Todo: save record file if have
     */
    public function saveFile() {
        // 5. save file
        if(!is_null($this->id)){
            GasFile::ApiSaveRecordFile($this, GasFile::TYPE_9_STORECARD);
        }
    }
    public function deleteFile() {
        if(!is_array($this->list_id_image) || count($this->list_id_image) < 1){
            return ;
        }
        if(!$this->isNewRecord) {// add Jul2817
            throw new Exception('Có lỗi. Bạn không được xóa hình chứng từ của thẻ kho');
        }
        foreach($this->list_id_image as $file_id){
            $mFile = GasFile::model()->findByPk($file_id);
            if($mFile->type == GasFile::TYPE_9_STORECARD){
                $mFile->delete();
            }
        }
    }
    /** @Author: ANH DUNG May 27, 2016
    *   @Todo: get html file view on grid
    */
    public function getFileOnly() {
        $res = '';
        $cmsFormater = new CmsFormatter();
        $res .= $cmsFormater->formatBreakTaskDailyFile($this, array('width'=>30, 'height'=>30));
        return $res;
    }
    /********** FOR FILE HANDLE ************************/
    
    /** @Author: HOANG NAM 11/07/2018
     *  @Todo: Revenue output
     **/
    public function revenueOutputV2(){
        $aRes = [];
//        if(!empty($this->province_id_agent) && count($this->agent_id) == 0){
//            $aAgentId = $this->getAgentOfProvince($this->province_id_agent);
//            $this->agent_id = $aAgentId;
//        }// Close Jan0219

        // chỗ ROLE_AGENT không phải đổi qua ROLE_SUB_USER_AGENT. Xác nhận Apr 13, 2014
        $aAgent = Users::getArrObjectUserByRoleHaveOrder(ROLE_AGENT, 'id');// $aAgent dạng: array[id agent]=>model agent
        $this->getTotalOutputV2($aAgent, $aRes);
        $this->getMoneyBankAndTotalRevenueV2($aRes,$aAgent, 'MONEY_BANK');
        $this->getMoneyBankAndTotalRevenueV2($aRes,$aAgent, 'TOTAL_REVENUE');
        
        $_SESSION['data-excel'] = $aRes;
        return $aRes;
    }
    
    /** @Author: HOANG NAM 12/07/2018
     *  @Todo: get agent of province
     **/
    public function getAgentOfProvince($aProvince) {
        if(!is_array($aProvince)){
            return [];
        }
        $aRes       = [];
        $mAppCache  = new AppCache();
        $aAgent     = $mAppCache->getAgent();
        foreach($aAgent as $item){
            if(in_array($item['province_id'], $aProvince)){
                $aRes[] = $item['id'];
            }
        }
        return $aRes;
    }
    
    /** @Author: HOANG NAM 11/07/2018
     *  @Todo: get total output
     **/
    public function getTotalOutputV2($aAgent,&$aRes){
        // lặp 2 loại bình thống kê là 12 và 45kg
        foreach( CmsFormatter::$MATERIAL_TYPE_OUTPUT_STATISTIC as $type_output){
            // 1. get id những vật tư có loại là Gas 12 Kg OR 45kg
            $aMaterialId = GasMaterials::getArrayIdByMaterialTypeId($type_output['list_id']);        
            $this->getOutputByMonthYearV2($aMaterialId, $aRes, $type_output, $aAgent);
            if($type_output['code']=='12KG'){
                // lấy chi tiết sản lượng cho từng loại của B12, hộ gd, bò, mối
                if(empty($this->statistic_month)){
                    $this->getOutputDetail12KgByYearV2($aMaterialId, $aRes, GasStoreCard::$TYPE_CUSTOMER[CUSTOMER_HO_GIA_DINH], $aAgent);
                    $this->getOutputDetail12KgByYearV2($aMaterialId, $aRes, GasStoreCard::$TYPE_CUSTOMER[STORE_CARD_KH_BINH_BO], $aAgent);
                    $this->getOutputDetail12KgByYearV2($aMaterialId, $aRes, GasStoreCard::$TYPE_CUSTOMER[STORE_CARD_KH_MOI], $aAgent);                    
                }else{
                    $this->getOutputByMonthYearDetail12KgV2($aMaterialId, $aRes, GasStoreCard::$TYPE_CUSTOMER[CUSTOMER_HO_GIA_DINH], $aAgent);
                    $this->getOutputByMonthYearDetail12KgV2($aMaterialId, $aRes, GasStoreCard::$TYPE_CUSTOMER[STORE_CARD_KH_BINH_BO], $aAgent);
                    $this->getOutputByMonthYearDetail12KgV2($aMaterialId, $aRes, GasStoreCard::$TYPE_CUSTOMER[STORE_CARD_KH_MOI], $aAgent);
                }
            }            
        }
    }
    
    /** @Author: ANH DUNG Dec 03, 2018
     *  @Todo: check user change sort
     **/
    public function isUserChangeSort() {
        $cUid = MyFormat::getCurrentUid();
        $aUidChangeSort = [GasConst::UID_ADMIN, GasLeave::UID_DIRECTOR];
        return in_array($cUid, $aUidChangeSort);
    }
    
    /** @Author: HOANG NAM 11/07/2018
     *  @Todo: get output by month year
     **/
    public function getOutputByMonthYearV2($aMaterialId, &$aRes, $type_output, $aAgent){
        $this->getOutputByYearV2($aMaterialId, $aRes, $type_output, $aAgent);
        if(empty($this->statistic_month)) return;
        // Lấy số ngày trong tháng
        $days_in_month = cal_days_in_month(0, $this->statistic_month, $this->statistic_year) ;        
        $aAgentSortMonth = $aAgentSortYear = $aSumByAgent = [];
        $criteria = new CDBcriteria();
        /*add between date search*/
        $dateFrom = $this->statistic_year.'-'.$this->statistic_month.'-01';
        $dateTo = $this->statistic_year.'-'.$this->statistic_month.'-'.$days_in_month;
//        $criteria->addBetweenCondition("t.date_delivery",$dateFrom, $dateTo);
        DateHelper::searchBetween($dateFrom, $dateTo, 'date_delivery_bigint', $criteria, false);
//            $criteria->addInCondition("t.type_in_out", CmsFormatter::$TYPE_IN_OUT_OUTPUT_STATISTIC); // xuất bán, bán bộ bình, thế chân
        $sParamsIn = implode(',', CmsFormatter::$TYPE_IN_OUT_OUTPUT_STATISTIC);
        $criteria->addCondition("t.type_in_out IN ($sParamsIn)");

//            $criteria->addInCondition("t.materials_id", $aMaterialId);
        $sParamsIn = implode(',', $aMaterialId);
        if(!empty($sParamsIn)){
            $criteria->addCondition("t.materials_id IN ($sParamsIn)");
        }

//            $criteria->addInCondition("t.user_id_create", $this->agent_id);
        $sParamsIn = is_array($this->agent_id) ? implode(',', $this->agent_id) : '';
        if(!empty($sParamsIn)){
            $criteria->addCondition("t.user_id_create IN ($sParamsIn)");
        }

        $criteria->select = "t.user_id_create, sum(qty) as qty,t.date_delivery";
        $criteria->group = "t.user_id_create,t.date_delivery";
        $mRes = GasStoreCardDetail::model()->findAll($criteria);
        if(count($mRes)>0){
            if(!isset($aRes['month'][$this->statistic_month]))
                $aRes['month'][$this->statistic_month] = 1; // biến $this->statistic_month dùng để foreach nên chỉ cần gán =1 là dc
            foreach($mRes as $item){
//                $i = date('d',strtotime($item->date_delivery));
                $i = substr($item->date_delivery, -2);
                //  for tab each month
                // one cell
                $aRes['OUTPUT'][$type_output['code']][$this->statistic_month][$i][$item->user_id_create] = $item->qty;
                // like : $aRes['OUTPUT'][12KG]['total_day'][08][agent 102]
                //sum_row -- dùng kiểu này vì sort dc theo tỉnh
                if(!isset($aAgentSortMonth[$aAgent[$item->user_id_create]->province_id][$item->user_id_create]))
                    $aAgentSortMonth[$aAgent[$item->user_id_create]->province_id][$item->user_id_create] = $item->qty;
                else
                    $aAgentSortMonth[$aAgent[$item->user_id_create]->province_id][$item->user_id_create] += $item->qty;

                if(!isset($aRes['OUTPUT'][$type_output['code']]['sum_col_total_month_all_agent'][$this->statistic_month]))
                    $aRes['OUTPUT'][$type_output['code']]['sum_col_total_month_all_agent'][$this->statistic_month] = $item->qty;
                else
                    $aRes['OUTPUT'][$type_output['code']]['sum_col_total_month_all_agent'][$this->statistic_month] += $item->qty;
                // need sum column...here, nên sum luôn column ở đây, xuống view khỏi tạo biến để sum nữa
                // sum column one day for all agent
                if(!isset($aRes['OUTPUT'][$type_output['code']]['sum_col'][$this->statistic_month][$i]))
                    $aRes['OUTPUT'][$type_output['code']]['sum_col'][$this->statistic_month][$i] = $item->qty;
                else
                    $aRes['OUTPUT'][$type_output['code']]['sum_col'][$this->statistic_month][$i] += $item->qty;                                        
                //  for tab each month

                //  for tab sum year
                // one cell
                if(!isset($aRes['OUTPUT'][$type_output['code']]['total_year'][$this->statistic_month][$item->user_id_create]))
                    $aRes['OUTPUT'][$type_output['code']]['total_year'][$this->statistic_month][$item->user_id_create] = $item->qty;
                else
                    $aRes['OUTPUT'][$type_output['code']]['total_year'][$this->statistic_month][$item->user_id_create] += $item->qty;

                //sum_row
                if(!isset($aAgentSortYear[$aAgent[$item->user_id_create]->province_id][$item->user_id_create]))
                    $aAgentSortYear[$aAgent[$item->user_id_create]->province_id][$item->user_id_create] = $item->qty;
                else
                    $aAgentSortYear[$aAgent[$item->user_id_create]->province_id][$item->user_id_create] += $item->qty;

                // need sum column...here, nên sum luôn column ở đây, xuống view khỏi tạo biến để sum nữa
                // sum column one day for all agent
                if(!isset($aRes['OUTPUT'][$type_output['code']]['total_year']['sum_col'][$this->statistic_month]))
                    $aRes['OUTPUT'][$type_output['code']]['total_year']['sum_col'][$this->statistic_month] = $item->qty;
                else
                    $aRes['OUTPUT'][$type_output['code']]['total_year']['sum_col'][$this->statistic_month] += $item->qty;           

                if(!isset($aRes['OUTPUT'][$type_output['code']]['sum_col_total_year_all_agent']))
                    $aRes['OUTPUT'][$type_output['code']]['sum_col_total_year_all_agent'] = $item->qty;
                else
                    $aRes['OUTPUT'][$type_output['code']]['sum_col_total_year_all_agent'] += $item->qty;                    
                //  for tab sum year
            } 
            for($j=1;$j<=$days_in_month; $j++){
                if($j<10){
                    $j='0'.$j; 
                }
                if(!empty($aRes['OUTPUT'][$type_output['code']][$this->statistic_month][$j])){
                    if(empty($aRes['OUTPUT'][$type_output['code']]['days'][$this->statistic_month][$j])){
                        $aRes['OUTPUT'][$type_output['code']]['days'][$this->statistic_month][$j] = $j;
                    }
                }
            }
        }
        // sort each month by tỉnh
        if(count($aAgentSortMonth)){
            foreach($aAgentSortMonth as $province_id=>$arrayAgentTotalMonth){
                $aSumByAgent += $arrayAgentTotalMonth;
//                arsort($arrayAgentTotalMonth);
                asort($arrayAgentTotalMonth);// low to hight
                if(!isset($aRes['OUTPUT'][$type_output['code']]['sum_row_total_month'][$this->statistic_month]))
                    $aRes['OUTPUT'][$type_output['code']]['sum_row_total_month'][$this->statistic_month] = $arrayAgentTotalMonth;
                else
                    $aRes['OUTPUT'][$type_output['code']]['sum_row_total_month'][$this->statistic_month] += $arrayAgentTotalMonth;
            }
        }
        
        // Fix Dec0318 sort each month from low -> high
        if($this->isUserChangeSort()){
            asort($aSumByAgent);
            $aRes['OUTPUT'][$type_output['code']]['sum_row_total_month'][$this->statistic_month] = $aSumByAgent;
        }// Fix Dec0318 sort each month from low -> high
        
        // sort year by tỉnh
//        if(count($aAgentSortYear)){
//            foreach($aAgentSortYear as $province_id=>$arrayAgentTotalMonth){
//                arsort($arrayAgentTotalMonth);
//                if(!isset($aRes['OUTPUT'][$type_output['code']]['total_year']['sum_row']))
//                    $aRes['OUTPUT'][$type_output['code']]['total_year']['sum_row'] = $arrayAgentTotalMonth;
//                else
//                    $aRes['OUTPUT'][$type_output['code']]['total_year']['sum_row'] += $arrayAgentTotalMonth;
//            }
//        }
        // sort
    }
    
    /** @Author: HOANG NAM 11/07/2018
     *  @Todo: get output year
     **/
    // Tính riêng cho năm
    public function getOutputByYearV2($aMaterialId, &$aRes, $type_output, $aAgent){        
        if(!empty($this->statistic_month)) return;
        $aAgentSortMonth = array();
        $aAgentSortYear = array();
        $criteria = new CDBcriteria();
        $criteria->compare('year(t.date_delivery)', $this->statistic_year);
//            $criteria->addInCondition("t.type_in_out", CmsFormatter::$TYPE_IN_OUT_OUTPUT_STATISTIC); // xuất bán, bán bộ bình, thế chân
        $sParamsIn = implode(',', CmsFormatter::$TYPE_IN_OUT_OUTPUT_STATISTIC);
        $criteria->addCondition("t.type_in_out IN ($sParamsIn)");

//            $criteria->addInCondition("t.materials_id", $aMaterialId);
        $sParamsIn = implode(',', $aMaterialId);
        if(!empty($sParamsIn)){
            $criteria->addCondition("t.materials_id IN ($sParamsIn)");
        }

//            $criteria->addInCondition("t.user_id_create", $this->agent_id);
        $sParamsIn = implode(',', $this->agent_id);
        if(!empty($sParamsIn)){
            $criteria->addCondition("t.user_id_create IN ($sParamsIn)");
        }


        $criteria->select = "t.user_id_create, sum(qty) as qty,t.date_delivery";
        $criteria->group = "t.user_id_create,t.date_delivery";
        $mRes = GasStoreCardDetail::model()->findAll($criteria);
        if(count($mRes)>0){
            foreach($mRes as $item){
                $month = date('m',strtotime($item->date_delivery));
                //  for tab sum year
                // one cell
                if(!isset($aRes['OUTPUT'][$type_output['code']]['total_year'][$month][$item->user_id_create]))
                    $aRes['OUTPUT'][$type_output['code']]['total_year'][$month][$item->user_id_create] = $item->qty;
                else
                    $aRes['OUTPUT'][$type_output['code']]['total_year'][$month][$item->user_id_create] += $item->qty;

                //sum_row
                if(!isset($aAgentSortYear[$aAgent[$item->user_id_create]->province_id][$item->user_id_create]))
                    $aAgentSortYear[$aAgent[$item->user_id_create]->province_id][$item->user_id_create] = $item->qty;
                else
                    $aAgentSortYear[$aAgent[$item->user_id_create]->province_id][$item->user_id_create] += $item->qty;

                // need sum column...here, nên sum luôn column ở đây, xuống view khỏi tạo biến để sum nữa
                // sum column one day for all agent
                if(!isset($aRes['OUTPUT'][$type_output['code']]['total_year']['sum_col'][$month]))
                    $aRes['OUTPUT'][$type_output['code']]['total_year']['sum_col'][$month] = $item->qty;
                else
                    $aRes['OUTPUT'][$type_output['code']]['total_year']['sum_col'][$month] += $item->qty;           

                if(!isset($aRes['OUTPUT'][$type_output['code']]['sum_col_total_year_all_agent']))
                    $aRes['OUTPUT'][$type_output['code']]['sum_col_total_year_all_agent'] = $item->qty;
                else
                    $aRes['OUTPUT'][$type_output['code']]['sum_col_total_year_all_agent'] += $item->qty;                    
                //  for tab sum year
            }    
            for($i=1;$i<=12; $i++){
                if($i<10)
                    $i='0'.$i;
                $month1 = $i;
                if(!isset($aRes['month'][$month1]) && !empty($aRes['OUTPUT'][$type_output['code']]['total_year'][$month1]))
                $aRes['month'][$month1] = 1; // biến $month dùng để foreach nên chỉ cần gán =1 là dc
            }
        }

        // sort each month by tỉnh
        if(count($aAgentSortMonth)){
            foreach($aAgentSortMonth as $province_id=>$arrayAgentTotalMonth){
                arsort($arrayAgentTotalMonth);
                if(!isset($aRes['OUTPUT'][$type_output['code']]['sum_row_total_month'][$month]))
                    $aRes['OUTPUT'][$type_output['code']]['sum_row_total_month'][$month] = $arrayAgentTotalMonth;
                else
                    $aRes['OUTPUT'][$type_output['code']]['sum_row_total_month'][$month] += $arrayAgentTotalMonth;
            }
        }
        
        // sort year by tỉnh
        if(count($aAgentSortYear)){
            foreach($aAgentSortYear as $province_id=>$arrayAgentTotalMonth){
                arsort($arrayAgentTotalMonth);
                if(!isset($aRes['OUTPUT'][$type_output['code']]['total_year']['sum_row']))
                    $aRes['OUTPUT'][$type_output['code']]['total_year']['sum_row'] = $arrayAgentTotalMonth;
                else
                    $aRes['OUTPUT'][$type_output['code']]['total_year']['sum_row'] += $arrayAgentTotalMonth;
            }
        }
        // sort
        return;
    }
    
    /** @Author: HOANG NAM 11/07/2018
     *  @Todo: detail 12Kg Year
     **/
    public function getOutputDetail12KgByYearV2($aMaterialId, &$aRes, $type_output, $aAgent){        
        if(!empty($this->statistic_month)) return;
        // Lấy số ngày trong tháng
        $aAgentSortMonth = array();
        $aAgentSortYear = array();
        $type_customer = array_search($type_output, GasStoreCard::$TYPE_CUSTOMER) ;
        $criteria = new CDBcriteria();
        $criteria->compare('year(t.date_delivery)', $this->statistic_year);
        $criteria->compare("t.type_customer",$type_customer);

//            $criteria->addInCondition("t.type_in_out", CmsFormatter::$TYPE_IN_OUT_OUTPUT_STATISTIC); // xuất bán, bán bộ bình, thế chân
        $sParamsIn = implode(',', CmsFormatter::$TYPE_IN_OUT_OUTPUT_STATISTIC);
        $criteria->addCondition("t.type_in_out IN ($sParamsIn)");

//            $criteria->addInCondition("t.materials_id", $aMaterialId);
        $sParamsIn = implode(',', $aMaterialId);
        if(!empty($sParamsIn)){
            $criteria->addCondition("t.materials_id IN ($sParamsIn)");
        }

//            $criteria->addInCondition("t.user_id_create", $this->agent_id);
        $sParamsIn = implode(',', $this->agent_id);
        if(!empty($sParamsIn)){
            $criteria->addCondition("t.user_id_create IN ($sParamsIn)");
        }

        $criteria->select = "t.user_id_create, sum(qty) as qty,t.date_delivery";
        $criteria->group = "t.user_id_create,t.date_delivery";
        $mRes = GasStoreCardDetail::model()->findAll($criteria);
        if(count($mRes)>0){
            foreach($mRes as $item){
                $month = date('m',strtotime($item->date_delivery));
                //  for tab sum year
                // one cell
                if(!isset($aRes['DETAIL_12KG'][$type_output]['total_year'][$month][$item->user_id_create]))
                    $aRes['DETAIL_12KG'][$type_output]['total_year'][$month][$item->user_id_create] = $item->qty;
                else
                    $aRes['DETAIL_12KG'][$type_output]['total_year'][$month][$item->user_id_create] += $item->qty;

                //sum_row
                if(!isset($aAgentSortYear[$aAgent[$item->user_id_create]->province_id][$item->user_id_create]))
                    $aAgentSortYear[$aAgent[$item->user_id_create]->province_id][$item->user_id_create] = $item->qty;
                else
                    $aAgentSortYear[$aAgent[$item->user_id_create]->province_id][$item->user_id_create] += $item->qty;

                // need sum column...here, nên sum luôn column ở đây, xuống view khỏi tạo biến để sum nữa
                // sum column one day for all agent
                if(!isset($aRes['DETAIL_12KG'][$type_output]['total_year']['sum_col'][$month]))
                    $aRes['DETAIL_12KG'][$type_output]['total_year']['sum_col'][$month] = $item->qty;
                else
                    $aRes['DETAIL_12KG'][$type_output]['total_year']['sum_col'][$month] += $item->qty;           

                if(!isset($aRes['DETAIL_12KG'][$type_output]['sum_col_total_year_all_agent']))
                    $aRes['DETAIL_12KG'][$type_output]['sum_col_total_year_all_agent'] = $item->qty;
                else
                    $aRes['DETAIL_12KG'][$type_output]['sum_col_total_year_all_agent'] += $item->qty;                    
                //  for tab sum year
            } 
            for($i=1;$i<=12; $i++){
                if($i<10)
                    $i='0'.$i;
                $month1 = $i;
                if(!isset($aRes['month'][$month1]) && !empty($aRes['DETAIL_12KG'][$type_output]['total_year'][$month1]))
                $aRes['month'][$month1] = 1; // biến $month dùng để foreach nên chỉ cần gán =1 là dc
            }
        }
        // sort year by tỉnh
        if(count($aAgentSortYear)){
            foreach($aAgentSortYear as $province_id=>$arrayAgentTotalMonth){
//                arsort($arrayAgentTotalMonth);
                asort($arrayAgentTotalMonth);
                if(!isset($aRes['DETAIL_12KG'][$type_output]['total_year']['sum_row']))
                    $aRes['DETAIL_12KG'][$type_output]['total_year']['sum_row'] = $arrayAgentTotalMonth;
                else
                    $aRes['DETAIL_12KG'][$type_output]['total_year']['sum_row'] += $arrayAgentTotalMonth;
            }
        }
        // sort
    }
    
    public function getOutputByMonthYearDetail12KgV2($aMaterialId, &$aRes, $type_output, $aAgent){        
        
        if(empty($this->statistic_month)) return;
        // Lấy số ngày trong tháng
        $days_in_month = cal_days_in_month(0, $this->statistic_month, $this->statistic_year) ;        
        $aAgentSortMonth = $aAgentSortYear = $aSumByAgent = [];
        $type_customer = array_search($type_output, GasStoreCard::$TYPE_CUSTOMER) ;
        $criteria = new CDBcriteria();
        $dateFrom = $this->statistic_year.'-'.$this->statistic_month.'-01';
        $dateTo = $this->statistic_year.'-'.$this->statistic_month.'-'.$days_in_month;
//        $criteria->addBetweenCondition("t.date_delivery",$dateFrom,$dateTo);
        DateHelper::searchBetween($dateFrom, $dateTo, 'date_delivery_bigint', $criteria, false);
        $criteria->compare("t.type_customer",$type_customer);
//            $criteria->addInCondition("t.type_in_out", CmsFormatter::$TYPE_IN_OUT_OUTPUT_STATISTIC); // xuất bán, bán bộ bình, thế chân
        $sParamsIn = implode(',', CmsFormatter::$TYPE_IN_OUT_OUTPUT_STATISTIC);
        $criteria->addCondition("t.type_in_out IN ($sParamsIn)");

//            $criteria->addInCondition("t.materials_id", $aMaterialId);
        $sParamsIn = implode(',', $aMaterialId);
        if(!empty($sParamsIn)){
            $criteria->addCondition("t.materials_id IN ($sParamsIn)");
        }

//            $criteria->addInCondition("t.user_id_create", $this->agent_id);
        $sParamsIn = implode(',', $this->agent_id);
        if(!empty($sParamsIn)){
            $criteria->addCondition("t.user_id_create IN ($sParamsIn)");
        }
        $sParamsIn = implode(',', GasCheck::getAgentNotGentAuto());
        $criteria->addCondition("t.user_id_create NOT IN ($sParamsIn)");

        $criteria->select = "t.user_id_create, sum(qty) as qty,t.date_delivery";
        $criteria->group = "t.user_id_create,t.date_delivery";
        $mRes = GasStoreCardDetail::model()->findAll($criteria);
        if(count($mRes)>0){
            
            if(!isset($aRes['month'][$this->statistic_month]))
                $aRes['month'][$this->statistic_month] = 1; // biến $this->statistic_month dùng để foreach nên chỉ cần gán =1 là dc
            foreach($mRes as $item){
                $i = date('d',strtotime($item->date_delivery));
                // one cell
                $aRes['DETAIL_12KG'][$type_output][$this->statistic_month][$i][$item->user_id_create] = $item->qty;
                // like : $aRes['DETAIL_12KG'][12KG]['total_day'][08][agent 102]
                //sum_row -- dùng kiểu này vì sort dc theo tỉnh
                if(!isset($aAgentSortMonth[$aAgent[$item->user_id_create]->province_id][$item->user_id_create]))
                    $aAgentSortMonth[$aAgent[$item->user_id_create]->province_id][$item->user_id_create] = $item->qty;
                else
                    $aAgentSortMonth[$aAgent[$item->user_id_create]->province_id][$item->user_id_create] += $item->qty;

                if(!isset($aRes['DETAIL_12KG'][$type_output]['sum_col_total_month_all_agent'][$this->statistic_month]))
                    $aRes['DETAIL_12KG'][$type_output]['sum_col_total_month_all_agent'][$this->statistic_month] = $item->qty;
                else
                    $aRes['DETAIL_12KG'][$type_output]['sum_col_total_month_all_agent'][$this->statistic_month] += $item->qty;
                // need sum column...here, nên sum luôn column ở đây, xuống view khỏi tạo biến để sum nữa
                // sum column one day for all agent
                if(!isset($aRes['DETAIL_12KG'][$type_output]['sum_col'][$this->statistic_month][$i]))
                    $aRes['DETAIL_12KG'][$type_output]['sum_col'][$this->statistic_month][$i] = $item->qty;
                else
                    $aRes['DETAIL_12KG'][$type_output]['sum_col'][$this->statistic_month][$i] += $item->qty;                                        
                //  for tab each month

                //  for tab sum year
                // one cell
                if(!isset($aRes['DETAIL_12KG'][$type_output]['total_year'][$this->statistic_month][$item->user_id_create]))
                    $aRes['DETAIL_12KG'][$type_output]['total_year'][$this->statistic_month][$item->user_id_create] = $item->qty;
                else
                    $aRes['DETAIL_12KG'][$type_output]['total_year'][$this->statistic_month][$item->user_id_create] += $item->qty;

                //sum_row
                if(!isset($aAgentSortYear[$aAgent[$item->user_id_create]->province_id][$item->user_id_create]))
                    $aAgentSortYear[$aAgent[$item->user_id_create]->province_id][$item->user_id_create] = $item->qty;
                else
                    $aAgentSortYear[$aAgent[$item->user_id_create]->province_id][$item->user_id_create] += $item->qty;

                // need sum column...here, nên sum luôn column ở đây, xuống view khỏi tạo biến để sum nữa
                // sum column one day for all agent
                if(!isset($aRes['DETAIL_12KG'][$type_output]['total_year']['sum_col'][$this->statistic_month]))
                    $aRes['DETAIL_12KG'][$type_output]['total_year']['sum_col'][$this->statistic_month] = $item->qty;
                else
                    $aRes['DETAIL_12KG'][$type_output]['total_year']['sum_col'][$this->statistic_month] += $item->qty;           

                if(!isset($aRes['DETAIL_12KG'][$type_output]['sum_col_total_year_all_agent']))
                    $aRes['DETAIL_12KG'][$type_output]['sum_col_total_year_all_agent'] = $item->qty;
                else
                    $aRes['DETAIL_12KG'][$type_output]['sum_col_total_year_all_agent'] += $item->qty;                    
                //  for tab sum year
            }
            for($j=1;$j<=$days_in_month; $j++){
                if($j<10){
                    $j='0'.$j; 
                }
                if(!empty($aRes['DETAIL_12KG'][$type_output][$this->statistic_month][$j])){
                    if(empty($aRes['DETAIL_12KG'][$type_output]['days'][$this->statistic_month][$j])){
                        $aRes['DETAIL_12KG'][$type_output]['days'][$this->statistic_month][$j] = $j;
                    }
                }
            }
        }
        // sort each month by tỉnh
        if(count($aAgentSortMonth)){
            foreach($aAgentSortMonth as $province_id=>$arrayAgentTotalMonth){
                $aSumByAgent += $arrayAgentTotalMonth;
//                arsort($arrayAgentTotalMonth);// hight to low
                asort($arrayAgentTotalMonth);// low to hight
                if(!isset($aRes['DETAIL_12KG'][$type_output]['sum_row_total_month'][$this->statistic_month]))
                    $aRes['DETAIL_12KG'][$type_output]['sum_row_total_month'][$this->statistic_month] = $arrayAgentTotalMonth;
                else
                    $aRes['DETAIL_12KG'][$type_output]['sum_row_total_month'][$this->statistic_month] += $arrayAgentTotalMonth;
            }
        }
        
        if($this->isUserChangeSort()){
            asort($aSumByAgent);
            $aRes['DETAIL_12KG'][$type_output]['sum_row_total_month'][$this->statistic_month] = $aSumByAgent;
        }// Fix Dec0318 sort each month from low -> high

    }
    
    
    /** @Author: HOANG NAM 11/07/2018
     *  @Todo: get money bank and total revenue
     **/
    public function getMoneyBankAndTotalRevenueV2(&$aRes, $aAgent, $TYPE='MONEY_BANK'){
        return;// AnhDung Dec0318
        if(empty($this->statistic_month)) return;
        // Lấy số ngày trong tháng
        $days_in_month = cal_days_in_month(0, $this->statistic_month, $this->statistic_year) ;        
        $aAgentSortMonth = array();
        $aAgentSortYear = array();
        
        $criteria = new CDBcriteria();
        $dateFrom = $this->statistic_year.'-'.$this->statistic_month.'-01';
        $dateTo = $this->statistic_year.'-'.$this->statistic_month.'-'.$days_in_month;
        $criteria->addBetweenCondition("t.release_date",$dateFrom,$dateTo);
//            $criteria->compare("t.release_date",$date);
        if($TYPE=='MONEY_BANK'){// chi nộp ngân hàng
            $sParamsIn = implode(',', CmsFormatter::$TYPE_MONEY_FROM_BANK);
            $criteria->addCondition("t.master_lookup_id IN ($sParamsIn)");
        }else{// TỔNG DOANH THU ĐẠI LÝ - trừ kh bình bò ra
            $sParamsIn = implode(',', CmsFormatter::$REVENUE_ITEM);
            $criteria->addCondition("t.master_lookup_id IN ($sParamsIn)");
            $sParamsIn = implode(',', CmsFormatter::$REVENUE_ITEM_EXCEPT_CUSTOMER);
            $criteria->addCondition("t.type_customer NOT IN ($sParamsIn)");
        }
        $sParamsIn = implode(',', $this->agent_id);
        if(!empty($sParamsIn)){
            $criteria->addCondition("t.agent_id IN ($sParamsIn)");
        }

        $criteria->select = "t.agent_id, sum(amount) as amount,t.release_date";
        $criteria->group = "t.agent_id,t.release_date";
        $mRes = GasCashBookDetail::model()->findAll($criteria);

        if(count($mRes)>0){
            
            if(!isset($aRes['month'][$this->statistic_month]))
                $aRes['month'][$this->statistic_month] = 1; // biến $this->statistic_month dùng để foreach nên chỉ cần gán =1 là dc
            foreach($mRes as $item){
                $i = date('d',strtotime($item->release_date));
                //  for tab each month
                // one cell
                $aRes[$TYPE][$this->statistic_month][$i][$item->agent_id] = $item->amount;
                // like : $aRes[12KG]['total_day'][08][agent 102]
                //sum_row -- dùng kiểu này vì sort dc theo tỉnh
                if(!isset($aAgentSortMonth[$aAgent[$item->agent_id]->province_id][$item->agent_id]))
                    $aAgentSortMonth[$aAgent[$item->agent_id]->province_id][$item->agent_id] = $item->amount;
                else
                    $aAgentSortMonth[$aAgent[$item->agent_id]->province_id][$item->agent_id] += $item->amount;

                if(!isset($aRes[$TYPE]['sum_col_total_month_all_agent'][$this->statistic_month]))
                    $aRes[$TYPE]['sum_col_total_month_all_agent'][$this->statistic_month] = $item->amount;
                else
                    $aRes[$TYPE]['sum_col_total_month_all_agent'][$this->statistic_month] += $item->amount;
                // need sum column...here, nên sum luôn column ở đây, xuống view khỏi tạo biến để sum nữa
                // sum column one day for all agent
                if(!isset($aRes[$TYPE]['sum_col'][$this->statistic_month][$i]))
                    $aRes[$TYPE]['sum_col'][$this->statistic_month][$i] = $item->amount;
                else
                    $aRes[$TYPE]['sum_col'][$this->statistic_month][$i] += $item->amount;                    

                //  for tab each month

                //  for tab sum year
                // one cell
                if(!isset($aRes[$TYPE]['total_year'][$this->statistic_month][$item->agent_id]))
                    $aRes[$TYPE]['total_year'][$this->statistic_month][$item->agent_id] = $item->amount;
                else
                    $aRes[$TYPE]['total_year'][$this->statistic_month][$item->agent_id] += $item->amount;

                //sum_row
                if(!isset($aAgentSortYear[$aAgent[$item->agent_id]->province_id][$item->agent_id]))
                    $aAgentSortYear[$aAgent[$item->agent_id]->province_id][$item->agent_id] = $item->amount;
                else
                    $aAgentSortYear[$aAgent[$item->agent_id]->province_id][$item->agent_id] += $item->amount;

                // need sum column...here, nên sum luôn column ở đây, xuống view khỏi tạo biến để sum nữa
                // sum column one day for all agent
                if(!isset($aRes[$TYPE]['total_year']['sum_col'][$this->statistic_month]))
                    $aRes[$TYPE]['total_year']['sum_col'][$this->statistic_month] = $item->amount;
                else
                    $aRes[$TYPE]['total_year']['sum_col'][$this->statistic_month] += $item->amount;           

                if(!isset($aRes[$TYPE]['sum_col_total_year_all_agent']))
                    $aRes[$TYPE]['sum_col_total_year_all_agent'] = $item->amount;
                else
                    $aRes[$TYPE]['sum_col_total_year_all_agent'] += $item->amount;

                //  for tab sum year
            }  
            for($i=1;$i<=$days_in_month; $i++){
                if($i<10)
                    $i='0'.$i;
                if(!empty($aRes[$TYPE][$this->statistic_month][$i])){
                    if(empty($aRes[$TYPE]['days'][$this->statistic_month][$i])){
                        $aRes[$TYPE]['days'][$this->statistic_month][$i] = $i;
                    }
                }
            }
        }
        // sort each month by tỉnh
        if(count($aAgentSortMonth)){
            foreach($aAgentSortMonth as $province_id=>$arrayAgentTotalMonth){
                arsort($arrayAgentTotalMonth);
                if(!isset($aRes[$TYPE]['sum_row_total_month'][$this->statistic_month]))
                    $aRes[$TYPE]['sum_row_total_month'][$this->statistic_month] = $arrayAgentTotalMonth;
                else
                    $aRes[$TYPE]['sum_row_total_month'][$this->statistic_month] += $arrayAgentTotalMonth;
            }
        }
        
        // sort year by tỉnh
        if(count($aAgentSortYear)){
            foreach($aAgentSortYear as $province_id=>$arrayAgentTotalMonth){
                arsort($arrayAgentTotalMonth);
                if(!isset($aRes[$TYPE]['total_year']['sum_row']))
                    $aRes[$TYPE]['total_year']['sum_row'] = $arrayAgentTotalMonth;
                else
                    $aRes[$TYPE]['total_year']['sum_row'] += $arrayAgentTotalMonth;
            }
        }
    }
    
    /** @Author: DuongNV 29 May,2019
     *  @Todo: load sẵn ds vật tư khi tạo mới
     * /admin/gasstorecard/create/type_store_card/1
     **/
    public function loadTemplate() {
        $criteria                       = new CDbCriteria;
        $criteria->compare('t.type', Modules::MODULE_KHO_90_NCV);
        $mModule                        = Modules::model()->findAll($criteria);
        $this->aModelStoreCardDetail    = [];
        foreach ($mModule as $module) {
            $mModuleDetail  = $module->rDetail;
            foreach ($mModuleDetail as $value) {
                $modelDetail                    = new GasStoreCardDetail();
                $modelDetail->materials_id      = $value->materials_id;
                $modelDetail->qty               = 1;
                $this->aModelStoreCardDetail[]  = $modelDetail;
            }
        }
    }

    /** @Author: LocNV Jun 21, 2019
     *  @Todo: load sẵn ds vật tư khi tạo mới với danh sách vật tư lấy từ gas support customer
     * /admin/gasstorecard/create/type_store_card/2
     * LOC011
     **/
    public function loadMaterialsInGasSupportCustomer() {
        $mSupportCustomer   = GasSupportCustomer::model()->findByPk($_GET['support_customer_id']);
        $this->customer_id          = $mSupportCustomer->customer_id;
        $this->type_in_out          = STORE_CARD_TYPE_7;
        $this->ext_is_new_customer  = CUSTOMER_OLD;
        $criteria2 = new CDbCriteria();
        $criteria2->compare('t.support_customer_id', $_GET['support_customer_id']);
        $mData = GasSupportCustomerItem::model()->findAll($criteria2);
        foreach ($mData as $value) {
            $modelDetail                    = new GasStoreCardDetail();
            $modelDetail->materials_id      = $value->materials_id;
            $modelDetail->qty               = $value->qty;
            $this->aModelStoreCardDetail[]  = $modelDetail;
        }
    }

    /** @Author: DuongNV 2 Jul,2019
     *  @Todo: Tạo công nợ khi tạo thẻ kho "Xuất kho treo CN trừ lương" từ app PVKH
    Khi tạo thẻ kho từ app Gas Service, nếu chọn là loại "Xuất kho treo cn trừ lương" thì sẽ làm các bước sau
    1/ Tìm ra 1 PVKH chính của đại lý
    2/ Tính đơn giá của các vật tư trong phiếu đang tạo
    3/ Tạo tự động 1 phiếu công nợ cá nhân (admin/gasDebts/index) loại: GasDebts::TYPE_DEBIT_PVKH cho PVKH ở bước 1. Người tạo là người đang tạo phiếu xuất kho
    - Khi update Xuất kho thì xóa đi insert lại
    - Diễn giải của GasDebts: Trần Thị Lệ xuất treo công nợ PVKH nguyễn văn A 350,000 Mã thẻ kho: T19ADESG. + diễn giải của thẻ kho
     * 
     **/
    public function handleAutoCreateDebts() {
        $mDebts             = new GasDebts();
        $mDebts->createFromStoreCard($this);
    }

    /** @Author: DuongNV 2 Jul,2019
     *  @Todo: Lấy model PVKH từ đại lý
     **/
    public function getPvkhFromAgent() {
        if(empty($this->user_id_create)) return null;
        $mTransactionHistory            = new TransactionHistory();
        $mTransactionHistory->agent_id  = $this->user_id_create;
        $aPvkh                          = $mTransactionHistory->getListEmployeeMaintainOfAgent();
        $aModelPvkh                     = Users::getArrayModelByArrayId($aPvkh);
        foreach ($aModelPvkh as $mUser) {
            if($mUser->role_id == ROLE_EMPLOYEE_MAINTAIN && $mUser->payment_day == UsersProfile::SALARY_YES){
                return $mUser;
            }
        }
        return null;
    }

    /** @Author: NamNH Aug 05, 2019
     *  @Todo: get price for gift
     **/
    public function getAmountGift(&$price,&$amount,$material_id,$qty) {
        $mSell              = new Sell();
        $aGiftSize          = $this->getArrayGiftSize();
        $price_per_gift     = $mSell->getPromotionDiscount();
        $number_per_gift    = isset($aGiftSize[$material_id]) ? $aGiftSize[$material_id] : 1;
        $price              = $price_per_gift/$number_per_gift;
        $amount             = round($qty*$price);
    }
    
    /** @Author: DuongNV 2 Jul,2019
     *  @Todo: Tính tổng đơn giá vật tư để gán vào công nợ của PVKH
     **/
    public function calcDebtsPvkh() {
        $ret                        = 0;
        $mSell                      = new Sell();
        $mSell->created_date_only   = date('Y-m-d', strtotime($this->created_date));
        $month                      = date('m', strtotime($this->created_date));
        $year                       = date('Y', strtotime($this->created_date));
        $aDataCache                 = $mSell->getPriceOfMonthSell();
        $aPriceHgd                  = MyFunctionCustom::getHgdPriceSetup($aDataCache, $this->user_id_create);
        $priceNPP                   = GasPrice::getPriceOfCode('NPP', $month, $year); // giá NPP
        $aPriceByType               = $this->getArrayPriceByType();
        $aKgByType                  = CmsFormatter::$MATERIAL_VALUE_KG;
        $price                      = 0;
        $amount                     = 0;
        $aDetail                    = '';
//        $aGiftSize                  = $this->getArrayGiftSize();
        $aMaterial                  = CacheSession::getArrayModelMaterial();
        if( empty($this->rStoreCardDetail) ){
            return;
        }
        foreach ($this->rStoreCardDetail as $detail) {
            switch ($detail->materials_type_id) {
                case GasMaterialsType::MATERIAL_TYPE_PROMOTION:
                    $this->getAmountGift($price,$amount,$detail->materials_id, $detail->qty);
//                    $price_per_gift     = $mSell->getPromotionDiscount();
//                    $number_per_gift    = isset($aGiftSize[$detail->materials_id]) ? $aGiftSize[$detail->materials_id] : 1;
//                    $price              = $price_per_gift/$number_per_gift;
//                    $amount             = round($detail->qty*$price);
                    break;
                case GasMaterialsType::MATERIAL_BINH_50KG:
                    $price      = $priceNPP;
                    $kg         = isset($aKgByType[GasMaterialsType::MATERIAL_BINH_50KG]) ? $aKgByType[GasMaterialsType::MATERIAL_BINH_50KG] : 0;
                    $priceVo    = isset($aPriceByType[GasMaterialsType::MATERIAL_VO_45]) ? $aPriceByType[GasMaterialsType::MATERIAL_VO_45] : 0;// Aug0119 DungNT thêm luôn tiền vỏ, bán cả nước cả vỏ
                    $amount     = ($detail->qty*$price*$kg) + $priceVo;
                    break;
                case GasMaterialsType::MATERIAL_BINH_45KG:
                    $price      = $priceNPP;
                    $kg         = isset($aKgByType[GasMaterialsType::MATERIAL_BINH_45KG]) ? $aKgByType[GasMaterialsType::MATERIAL_BINH_45KG] : 0;
                    $priceVo    = isset($aPriceByType[GasMaterialsType::MATERIAL_VO_45]) ? $aPriceByType[GasMaterialsType::MATERIAL_VO_45] : 0;// Aug0119 DungNT thêm luôn tiền vỏ, bán cả nước cả vỏ
                    $amount     = ($detail->qty*$price*$kg) + $priceVo;
                    break;
                case GasMaterialsType::MATERIAL_VO_50:
                case GasMaterialsType::MATERIAL_VO_45:
                    $price      = isset($aPriceByType[GasMaterialsType::MATERIAL_VO_45]) ? $aPriceByType[GasMaterialsType::MATERIAL_VO_45] : 0;
                    $amount     = $detail->qty*$price;
                    break;
                case GasMaterialsType::MATERIAL_VO_12:
                    $price      = isset($aPriceByType[GasMaterialsType::MATERIAL_VO_12]) ? $aPriceByType[GasMaterialsType::MATERIAL_VO_12] : 0;
                    $amount     = $detail->qty*$price;
                    break;

                default:
                    $price      = isset($aPriceHgd[$detail->materials_id]) ? $aPriceHgd[$detail->materials_id] : 0;
                    $amount     = $detail->qty*$price;
                    if($detail->materials_type_id == GasMaterialsType::MATERIAL_BINH_12KG){// Aug0119 DungNT thêm luôn tiền vỏ, bán cả nước cả vỏ
                        $priceVo    = isset($aPriceByType[GasMaterialsType::MATERIAL_VO_12]) ? $aPriceByType[GasMaterialsType::MATERIAL_VO_12] : 0;
                        $amount     += $priceVo;
                    }
                    break;
            }
            $ret   += $amount;
            $aDetail[$detail->id] = [
                'materials_no'      => isset($aMaterial[$detail->materials_id]) ? $aMaterial[$detail->materials_id]['materials_no'] : '',
                'materials_name'    => isset($aMaterial[$detail->materials_id]) ? $aMaterial[$detail->materials_id]['name'] : '',
                'materials_type_id' => $detail->materials_type_id,
                'qty'               => $detail->qty,
                'price'             => $price,
                'amount'            => $amount,
            ];
        }
        $this->jsonDetail = json_encode($aDetail);
        return (int)$ret;
    }
    
    /** @Author: DuongNV Jul0419
     *  @Todo: so luong cua moi phan qua khuyen mai
     **/
    public function getArrayGiftSize() {
        return [
            GasStoreCard::GIFT_CHEN_SU      => 5, // Chén sứ        KMC0004
            GasStoreCard::GIFT_TO_SU        => 2, // Tô sứ          KMT0006
            GasStoreCard::GIFT_LY_CO_QUAI   => 3, // Ly có quai     KM050
            GasStoreCard::GIFT_LY_UONG_BIA  => 3, // Ly uống bia    KML0005
            243                             => 6, // Ly thủy tinh nhỏ    KML0001
        ];
    }
    
    /** @Author: DuongNV Jul0719
     *  @Todo: gia theo loai vo 12, 45
     **/
    public function getArrayPriceByType() {
        return [
            GasMaterialsType::MATERIAL_VO_12 => 300000,
            GasMaterialsType::MATERIAL_VO_45 => 500000,// 500k
        ];
    }
    
    /** @Author: DungNT Jul 30, 2019
     *  @Todo: auto make thẻ kho nhập gas bồn khi thủ kho hoàn thành đơn xe bồn
     *  - Nhập mua Gas Bồn - LPG0001 Gas Bồn
     **/
    public function autoCreateWhenTruckDoneOrderAgentInternal() {
        $mTruckPlanDetail   = $this->mTruckPlanDetail;
        $mStoreCard                         = new GasStoreCard();
        $this->mapSameInfoStorecard($mStoreCard);
        $mStoreCard->type_store_card        = TYPE_STORE_CARD_IMPORT;
        $mStoreCard->type_in_out            = STORE_CARD_TYPE_2;// nhập mua
        $this->makeParamsPostDetailTruckDoneOrder();
        $mStoreCard->save();// save luôn để còn save detail
        $mTruckPlanDetail->store_card_id            = $mStoreCard->id;
        $mTruckPlanDetail->update(['store_card_id']);
        GasStoreCard::saveStoreCardDetail($mStoreCard);
    }
    
    /**
     * @Author: DungNT Jul 30, 2019
     * @Todo: map some info same của 2 thẻ kho gas và vỏ
     */
    public function mapSameInfoStorecard(&$mStoreCard) {
        $mTruckPlanDetail   = $this->mTruckPlanDetail;
        $mAppUserLogin      = $mTruckPlanDetail->mAppUserLogin;
        
        $mStoreCard->uid_login              = $mAppUserLogin->id;
        $mStoreCard->user_id_create         = $mTruckPlanDetail->customer_id;// is agent_id
        $mStoreCard->customer_id            = $mTruckPlanDetail->supplier_id;
        $mStoreCard->date_delivery          = $mTruckPlanDetail->getCompleteTime('Y-m-d');
//        $mStoreCard->date_delivery          = $mTruckPlanDetail->plan_date;
        $mStoreCard->date_delivery          = !empty($mStoreCard->date_delivery) ? $mStoreCard->date_delivery : date('Y-m-d');
        $mStoreCard->type_user              = $mTruckPlanDetail->rCustomer->is_maintain;
        $mStoreCard->pay_now                = GasStoreCard::CREATE_AUTO_FROM_APP_ORDER;
//      Aug0317 remove this line  $mStoreCard->phu_xe_2               = GasStoreCardApi::IS_ADMIN_GEN_FROM_ORDER;
        $mStoreCard->driver_id_1             = $mTruckPlanDetail->driver_id;
        $mStoreCard->car_id                  = $mTruckPlanDetail->car_id;
        $mStoreCard->buildStoreCardNo();
    }
    
     /**
     * @Author: DungNT Jul 30, 2019
     * @Todo: init param $_POST['materials_id'] và $_POST['materials_qty']
     * @param: $aDetailInfo is $this->getJsonDataField('info_gas');
     * xử lý cộng dồn với thu vỏ, còn các loại khác thì gắn biến bình thường
     */
    public function makeParamsPostDetailTruckDoneOrder() {
        $mTruckPlanDetail           = $this->mTruckPlanDetail;
        $aMaterialsId[]             = GasMaterials::ID_GAS_BON;
        $aMaterialsQty[]            = $mTruckPlanDetail->qty_real;
        
        $_POST['materials_id']      = $aMaterialsId;
        $_POST['materials_qty']     = $aMaterialsQty;
    }
   
}