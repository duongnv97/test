<?php

/**
 * This is the model class for table "{{_announce}}".
 *
 * The followings are the available columns in table '{{_announce}}':
 * @property integer $id
 * @property integer $type
 * @property integer $agent_id
 * @property integer $province_id
 * @property string $date_from
 * @property string $date_to
 * @property integer $status
 * @property string $description
 * @property string $created_date
 * @property string $created_by
 */
class Announce extends BaseSpj
{
    public $autocomplete_name;
    
    const DATE_CAN_UPDATE   = 2; // Khoảng thời gian có thể update kể từ ngày tạo
    const TYPE_KHUYENMAI    = 1;
    
    const STATUS_WAIT_APPROVE = 2; // chờ duyệt
    
    /** @Author: LOCNV Jun 11, 2019
     *  @Todo: get array type announce
     **/
    public function getArrayType() {
        return array(
            Announce::TYPE_KHUYENMAI => 'Khuyến mãi',
        );
    }
    
    /** @Author: LOCNV Jun 11, 2019
     *  @Todo: get array status
     **/
    public function getArrayStatus() {
        return array(
            STATUS_ACTIVE               => 'Hoạt động',
            STATUS_INACTIVE             => 'Không hoạt động',
            self::STATUS_WAIT_APPROVE   => 'Chờ duyệt'
        );
    }

    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return Announce the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{_announce}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        return array(
            array('type, agent_id, date_from, date_to', 'required', 'on'=>'create, update'),
            array('id, type, agent_id, province_id, date_from, date_to, status, description, created_date', 'safe'),
            array('agent_id', 'unique', 'on' => 'create, update', 'criteria'=>array(
                                    'condition'=>'`status`=' . STATUS_ACTIVE,
                                ),
                    'message' => 'Đại lý này đã có chương trình khuyến mãi, không thể tạo thêm, vui lòng kiểm tra lại'
                )
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        return array(
            'rAgent' => array(self::BELONGS_TO, 'Users', 'agent_id'),
            'rCreatedBy' => array(self::BELONGS_TO, 'Users', 'created_by'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
            return array(
                'id' => 'ID',
                'type' => 'Loại',
                'agent_id' => 'Đại lý',
                'province_id' => 'Tỉnh',
                'date_from' => 'Ngày bắt đầu từ',
                'date_to' => 'Đến ngày',
                'status' => 'Trạng thái',
                'description' => 'Mô tả',
                'created_date' => 'Ngày tạo',
                'created_by' => 'Người tạo',
            );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        $criteria=new CDbCriteria;

        $criteria->compare('t.type',$this->type);
        $criteria->compare('t.agent_id',$this->agent_id);
        $criteria->compare('t.province_id',$this->province_id);
//		$criteria->compare('t.date_from',$this->date_from,true);
//		$criteria->compare('t.date_to',$this->date_to,true);
        $criteria->compare('t.status',$this->status);
                
        $date_end_from  = '';
        $date_end_to    = '';
        if(!empty($this->date_from)){
            $date_end_from = MyFormat::dateDmyToYmdForAllIndexSearch($this->date_from);
        }
        if(!empty($this->date_to)){
            $date_end_to = MyFormat::dateDmyToYmdForAllIndexSearch($this->date_to);
        }
        if(!empty($date_end_from)){
            $criteria->addCondition("date_from >= '$date_end_from'");
        }
        if(!empty($date_end_to)){
            $criteria->addCondition("date_to <= '$date_end_to'");
        }
        $criteria->order = 't.status DESC, t.id DESC';
        
        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
            'pagination'=>array(
                'pageSize'=> Yii::app()->user->getState('pageSize',Yii::app()->params['defaultPageSize']),
            ),
        ));
    }
    
    /** @Author: LOCNV Jun 11, 2019
     *  @Todo: format datetime before save
     *  @Param:
     **/
    public function formatDbDatetime() {
        $this->date_from    = MyFormat::dateConverDmyToYmd($this->date_from, '-');
        $this->date_to      = MyFormat::dateConverDmyToYmd($this->date_to, '-');
        $this->province_id  = $this->rAgent->province_id;
    }
        
    public function formatDataBeforeUpdate() {
        $this->date_from    = MyFormat::dateConverYmdToDmy($this->date_from, 'd-m-Y');
        $this->date_to      = MyFormat::dateConverYmdToDmy($this->date_to, 'd-m-Y');
    }
    
    /** @Author: LOCNV Jun 11, 2019
     *  @Todo: get type
     *  @Param:
     **/
    public function getType() {
        $aType = $this->getArrayType();
        return isset($aType[$this->type]) ? $aType[$this->type] : '';
    }
    
    public function getCreatedBy() {
        return isset($this->rCreatedBy) ? $this->rCreatedBy->getFullName() : '';
    }
    
    /** @Author: LOCNV Jun 11, 2019
     *  @Todo: get agent info
     *  @Param:
     **/
    public function getAgentInfo() {
        $res = '';
//        $aAgent = CacheSession::getListAgent();
        $mAgent = $this->rAgent;
//        $res .= '<b>' . (isset($aAgent[$this->agent_id]) ? $aAgent[$this->agent_id]['first_name'] : '') . '</b>';
        if($mAgent){
            $res .= '<b>' . (isset($mAgent) ? $mAgent->getFullName() : '') . '</b>';
            $res .= '<br/>'. $mAgent->getProvince();
        }
        return $res;
    }
    
    /** @Author: LOCNV Jun , 2019
     *  @Todo: get description
     *  @Param:
     **/
    public function getDescription() {
        $res = '';
        if (!empty($this->description))
            $res = $this->description;
        else $res = '';
        return $res;
    }

        /** @Author: LOCNV Jun 11, 2019
     *  @Todo: get status
     *  @Param:
     **/
    public function getStatus() {
        $aStatus        = $this->getArrayStatus();
        $linkApprove    = '';
        if($this->canApprove() && $this->status == self::STATUS_WAIT_APPROVE){
            $linkApprove = '<br>';
            $url = Yii::app()->createAbsoluteUrl("admin/announce/approve", ['id'=> $this->id]);
            $linkApprove .= "<a href=".$url." class='approve_btn'>Duyệt</a>";
        }
        return isset($aStatus[$this->status]) ? $aStatus[$this->status].$linkApprove : '';
    }
    
    /** @Author: LOCNV Jun 11, 2019
     *  @Todo: get han su dung
     **/
    public function getDateFromTo() {
        $res = '';
        $res .= MyFormat::dateConverYmdToDmy($this->date_from);
        $res .= ' - ' . MyFormat::dateConverYmdToDmy($this->date_to);
        return $res;
    }

    /** @Author: LOCNV Jun , 2019
     *  @Todo: lay danh sach khuyen mai con hieu luc
     *  @Param: date $date_now ngày hiện tại cần check
     *  @param $returnObj return object ?
     **/
//    public function getListAnnounceNow($date_now, $check_date_from = false) {
    public function getListAnnounceNow($date_now) {
        $criteria = new CDbCriteria();
//         DuongNV Luôn check vì nếu khuyến mãi đó chưa tới (vd hnay là 12/06/2019 nhưng khuyến mãi 14/06/2019 - 14/07/2019)
//        if ($check_date_from === true) { 
            $criteria->addCondition("t.date_from <= '$date_now'");
//        }
        $criteria->addCondition("t.date_to >= '$date_now'");
        $criteria->compare('t.status', STATUS_ACTIVE);
        $aData = Announce::model()->findAll($criteria);
        $aRes = [];
        foreach ($aData as $announce) {
            $aRes[$announce->agent_id][$announce->id]['begin_date']    = $announce->date_from;
            $aRes[$announce->agent_id][$announce->id]['expiry_date']   = $announce->date_to;
            $aRes[$announce->agent_id][$announce->id]['msg']           = $announce->description;
            $aRes[$announce->agent_id][$announce->id]['type']          = $announce->type;
        }
        return $aRes;
    }
    
    /** @Author: DuongNV Jun 12,19
     *  @Todo: Set inactive cho những khuyến mãi hết hạn
     **/
    public function setInactiveAnnounce() {
        $date_now = date('Y-m-d');
        $criteria = new CDbCriteria();
        $criteria->addCondition("date_to < '$date_now'");
        $criteria->compare('status', STATUS_ACTIVE);
        Announce::model()->updateAll(['status' => STATUS_INACTIVE], $criteria);
    }
    
    /** @Author: LOCNV Jun 12, 2019
     *  @Todo: get các chương trình KM, đưa vào hỗ trợ cho tổng đài xem
     *  @Param:
     **/
    public function getAgentPromotion() {
        $res        = $this->getGsByAgent();// Jun2019 DungNT move 
        $today      = date('Y-m-d');
        $mAppCache  = new AppCache();
        $aData      = $mAppCache->getListdataGasAnnounce();
        if(array_key_exists($this->agent_id, $aData)){
//            $res .= '<br>';
            foreach ($aData[$this->agent_id] as $announce) {
                $dateBegin = MyFormat::dateConverYmdToDmy($announce['begin_date']);
                $dateExp   = MyFormat::dateConverYmdToDmy($announce['expiry_date']);
//                if(MyFormat::compareTwoDate($announce['expiry_date'], $today)){
                if(MyFormat::compareDateBetween($announce['begin_date'], $announce['expiry_date'], $today)){
                    $msg = $announce['msg'];
                    $res .= "Từ $dateBegin đến $dateExp".$msg;
                }
            }
        }
        return $res;
    }
    
    /** @Author: DuongNV Jun 12,19
     *  @Todo: Handle before create
     **/
    public function handleBeforeCreate() {
        $this->created_by   = MyFormat::getCurrentUid();
    }
    
    /** @Author: DuongNV Jun 12,19
     *  @Todo: Chỉ người tạo dc update và update sau 5 ngày kể từ ngày tạo
     **/
    public function canUpdate() {
        $cRole      = MyFormat::getCurrentRoleId();
        $cUid       = MyFormat::getCurrentUid();
        $aUidAllow  = [GasConst::UID_ANH_LQ, GasConst::UID_HIEU_PT];// Jun2219 DungNT set for 2 user can update all
        if($cRole == ROLE_ADMIN){
            return true;
        }
        if(in_array($cUid, $aUidAllow) && $this->status != STATUS_INACTIVE){
            return true;
        }
        $cDate      = date('Y-m-d');
        $expDate    = MyFormat::modifyDays($this->created_date, Announce::DATE_CAN_UPDATE);
        if($cUid == $this->created_by && MyFormat::compareTwoDate($expDate, $cDate)){
            return true;
        }
        return false;
    }
    
    /** @Author: DuongNV 14 Jun,18
     *  @Todo: update cache when update, create announce
     **/
    public function updateCache() {
        $mAppCache = new AppCache();
        $mAppCache->setListdataGasAnnounce();
    }
    
    /** @Author: DuongNV 19 Jun,19
     *  @Todo: get Giám sát info from agent
     *  @param $this->agent_id agent
     **/
    public function getGsByAgent() {
        $mCallClick             = new Call();
        $mTransactionHistory = new TransactionHistory();
        $mTransactionHistory->agent_id = $this->agent_id;
        $idGs       = $mTransactionHistory->getOnlyIdChuyenVien();
        $mUser      = Users::model()->findByPk($idGs);
        return is_null($mUser) ? '' : '<p>GS '.$mUser->first_name.': '.$mCallClick->buildUrlMakeCall($mUser->phone).'</p>';
    }
    
    /** @Author: DuongNV 2 Jul,2019
     *  @Todo: array user approve
     **/
    public function getArrayApprover() {
        return [
            GasConst::UID_ADMIN,
            GasLeave::UID_DIRECTOR_BUSSINESS
        ];
    }
    
    /** @Author: DuongNV 2 Jul,2019
     *  @Todo: can approve promotion
     **/
    public function canApprove() {
        $aAllow     = $this->getArrayApprover();
        $cUid       = MyFormat::getCurrentUid();
        $cRole      = MyFormat::getCurrentRoleId();
        return in_array($cUid, $aAllow) || $cRole == ROLE_ADMIN;
    }
    
    /** @Author: DuongNV 2 Jul,2019
     *  @Todo: can set status create update
     **/
    public function canSetStatus() {
        $cRole      = MyFormat::getCurrentRoleId();
        return $cRole == ROLE_ADMIN;
    }
    
    /** @Author: DuongNV Jul0519
     *  @Todo: send mail for approver
     *  @params $model Announce with agent, province
     **/
    public function sendEmailApprove() {
        if(empty($this->agent_id)){
            return ;
        }
        
        $titleHead  = '[Xét duyệt] Chương trình khuyến mãi';
        $body       = "<br>Có chương trình khuyến mãi mới cần duyệt.<br>";
        
        $body       .= "<br>Thông tin chương trình khuyến mãi:<br>";
        $body       .= "<b>Đại lý:</b> {$this->getAgentInfo()}<br>";
        $body       .= "<b>Thời gian:</b> {$this->getDateFromTo()}<br>";
        $body       .= "<b>Người tạo:</b> {$this->getCreatedBy()}<br>";
        $body       .= "<b>Ngày tạo:</b> {$this->getCreatedDate()}<br>";
        
        $body       .= "<br>Vui lòng truy cập vào link sau để xét duyệt:<br>";
        $url = Yii::app()->createAbsoluteUrl("admin/announce/index");
        $body       .= "<a href='$url'>Đi đến trang duyệt</a>";
        
        $needMore['title']     = $titleHead;
        $needMore['list_mail'] = [
            'duchoan@spj.vn', 
            'duongnv@spj.vn' // following
        ];
//        $needMore['SendNow']        = 1;// only dev debug
        SendEmail::bugToDev($body, $needMore);
    }
}