<?php

/**
 * This is the model class for table "{{_gas_schedule_notify}}".
 *
 * The followings are the available columns in table '{{_gas_schedule_notify}}':
 * @property string $id
 * @property string $user_id 
 * @property integer $type
 * @property string $obj_id
 * @property string $time_send
 * @property string $created_date
 * @property string $title 
 */
class GasScheduleNotify extends BaseSpj
{ 
    const MAX_SEND              = 50; // giới hạn max gửi 1 lần 
    const MAX_SEND_ANDROID      = 1000; // old 150 -> 300 giới hạn max gửi 1 lần cho big data
    const MAX_SEND_IOS          = 200; // giới hạn max gửi 1 lần cho big data IOS
    const MAX_ONE_TIME_ANDROID  = 100; // giới hạn max gửi 1 lần cho big data ANDROID của FCM. Aug3119ko tăng đc số này, nếu tăng lên sẽ vượt Max lengh của FCM => stop
    
    /* Note Apr1218 với ios 500 record gửi mất: 
     * 500 done in: 85 Second, 500 done in: 87 Second 
     * 500 done in: 88 Second
     * 500 done in: 100 Second <=> 1.67 Minute 
     * 500 done in: 95 Second
     * Apr1218 Chỉnh xuống 200
     */
    
    const MAX_COUNT_RUN = 1; // giới hạn max số lần gửi lại
    // tất cả notify sẽ đưa vào list chờ gửi hết, để không bị gửi sót cái nào
    const TYPE_NOT_SEND_SMS             = -1;
    const UPHOLD_ALERT_10               = 1;
    const UPHOLD_DINH_KY_1_DAY          = 2;
    const ISSUE_TICKET                  = 3;
    const UPHOLD_CREATE                 = 4;//Dec 21, 2015 bị phân tích thiếu, tất cả notify sẽ đưa vào list chờ gửi hết, để không bị gửi sót cái nào
    const TRANSACTION_CHANGE_EMPLOYEE   = 5;// Dec 06, 2016 event đơn HGĐ: new, cancel...
    const APP_ORDER_BO_MOI              = 6;// Jan 19, 2017 đặt hàng bò mối
    const UPHOLD_HGD                    = 7;// Feb 14, 2017 bảo trì HGĐ
    const TICKET_EVENT                  = 8;// May 14, 2017 trả lời + close ticket
    const SPJ_CODE_GOBACK               = 9;// Jun 08, 2017 bình quay về PTTT
    const ALERT_ORDER_CONFIRM           = 10;// Now1917 đã bỏ type này thay = ERROR_HGD_ALERT_5 --Jun 25, 2017 notify cho giám sát nếu sau 5 phút không xác nhận đơn hàng HGD
    const CUSTOMER_PAY_DEBIT            = 11;/// Sep0117 notify cho Thúc khi thu nợ KH
    const TEST_NOTIFY                   = 12;/// Now0917 sử dụng để test notify
    const ERROR_HGD_ALERT_5             = 13;// không confirm đơn HGĐ 5 phút
    const ERROR_HGD_ALERT_10            = 14;
    const ERROR_HGD_ALERT_20            = 15;
    
    const CUSTOMER_HGD_ORDER            = 16;// Now2517 làm riêng loại notify cho KH Gas24h
    const ANNOUNCE_TO_ALL               = 17;// Dec0717 notify thông báo, ko làm gì xử lý ở app cả
    const GAS24H_ANNOUNCE               = 18;// Apr0818 Big notify thông báo app Gas24h
    
    const ERROR_HGD_ALERT_3             = 19;// không confirm đơn HGĐ 3 phút
    const ERROR_HGD_ALERT_15            = 20;// không confirm đơn HGĐ 15 phút
    const CMS_NEWS                      = 21;// notify tin tức app Gas Service
    const GAS24H_OUT_OF_GAS             = 22;// notify dự báo lượng gas24h
    const GAS24H_REMAIN                 = 23;// notify dự báo lượng gas24h không hiển thị đặt gas nhanh
    const GAS_SERVICE_REMAIN            = 24;// notify dự báo lượng gas24h không hiển thị đặt gas nhanh
    
    const SUPPORT_REQUEST_ITEM          = 25;// notify yêu cầu hỗ trợ vật tư
    
    const ERROR_HGD_ALERT_3_GDKV                = 26;// HGD Sau 3 phut khong xac nhan thi gui cho GDKV, 8 phut khong xac nhan thi gui cho GDKV va chuyen vien
    const ERROR_HGD_ALERT_8                     = 27;
    const ERROR_HGD_ALERT_COMPLETE_18           = 28;// chưa Hoàn thành app: 18 phút gửi cho CV + GĐKV
    const ERROR_HGD_ALERT_COMPLETE_28           = 29;// chưa Hoàn thành app: 18 phút gửi cho CV + GĐKV
    const ERROR_HGD_ALERT_COMPLETE_38           = 30;// chưa Hoàn thành app: 18 phút gửi cho CV + GĐKV
    const TYPE_TRUCK_PLAN                       = 31;// đơn hàng gas bồn mới
    
    const GAS24H_NEW_LUCKY_NUMBER               = 32; // DuongNV Sep1919 Notify nhận dc vé khi mua hàng, tải app
    const GAS24H_NUMBER_WINNING                 = 33; // DuongNV Sep1919 Notify thắng giải

    
    // vì nếu send luôn thì có thể user bị offline nên không Nhận được, khi nhận được thì client sẽ gửi request lên server report nhận được
    
    public $aUidSend = [], $mEmployeeProblems = null, $isSendImmediate = false, $reply_id='', $mPromotion = null, $aUserAndroid=[], $aUserIos = [], $device_token='';
    
    /** @Author: DungNT Now 19, 2017
     *  @Todo: những type bị phạt của HGD
     **/
    public function getTypeErrorsHgd() {
        return [GasScheduleNotify::ERROR_HGD_ALERT_5, GasScheduleNotify::ERROR_HGD_ALERT_10, GasScheduleNotify::ERROR_HGD_ALERT_20,
            GasScheduleNotify::ERROR_HGD_ALERT_3, GasScheduleNotify::ERROR_HGD_ALERT_15,
            GasScheduleNotify::ERROR_HGD_ALERT_3_GDKV, GasScheduleNotify::ERROR_HGD_ALERT_8
        ];
    }
    /** @Author: DungNT Now 16, 2019
     *  @Todo: những type notify order HGD
     **/
    public function getAllTypeHgd() {
        return [
            GasScheduleNotify::TRANSACTION_CHANGE_EMPLOYEE,
            GasScheduleNotify::ERROR_HGD_ALERT_3,
            GasScheduleNotify::ERROR_HGD_ALERT_5,
            GasScheduleNotify::ERROR_HGD_ALERT_8,
            GasScheduleNotify::ERROR_HGD_ALERT_10,
            GasScheduleNotify::ERROR_HGD_ALERT_15,
            GasScheduleNotify::ERROR_HGD_ALERT_3_GDKV,
            GasScheduleNotify::ERROR_HGD_ALERT_COMPLETE_18,
            GasScheduleNotify::ERROR_HGD_ALERT_COMPLETE_28,
            GasScheduleNotify::ERROR_HGD_ALERT_COMPLETE_38,
        ];
    }
    
    public function getTypeErrorsHgdAlertOnly() {
        return [
            GasScheduleNotify::ERROR_HGD_ALERT_3_GDKV, GasScheduleNotify::ERROR_HGD_ALERT_8
        ];
    }
    /** @Author: DungNT Jun 16, 2019
     *  @Todo: get array lỗi ko phạt Chuyen Viên + GĐKV do NV đang học việc 2 tháng
     **/
    public function getTypeErrorsHgdException() {
        return [
            GasScheduleNotify::ERROR_HGD_ALERT_3_GDKV, 
            GasScheduleNotify::ERROR_HGD_ALERT_8
        ];
    }
    
    // array notify có độ ưu tiên thấp
    public function getTypeLowPriority() {
        return [GasScheduleNotify::ANNOUNCE_TO_ALL, GasScheduleNotify::TICKET_EVENT, GasScheduleNotify::CMS_NEWS];
    }
    public function getTypeBigData() {
        return [GasScheduleNotify::GAS24H_ANNOUNCE];
    }
        
    /** @Author: DungNT Oct 29, 2018
     *  @apply_app: Gas24h ios
     *  @Todo: get array type change .PEM file when send notify ios
     **/
    public function getArrayTypeGas24hIos() {
        return [
            GasScheduleNotify::CUSTOMER_HGD_ORDER,
            GasScheduleNotify::GAS24H_OUT_OF_GAS,
            GasScheduleNotify::GAS24H_REMAIN,
        ];
    }
    
    /** @Author: DungNT May 28, 2018
     *  @Todo: get Gas24h Pem file 
     **/
    public function getGas24hPemFile() {
        return Yii::getPathOfAlias('webroot').'/protected/config/apnssert/gas24h.pem';
//        return '/var/www/clients/client1/web2/web/protected/config/apnssert/gas24h.pem';
    }
    public function getGasServicePemFile() {
        return Yii::getPathOfAlias('webroot').'/protected/config/apnssert/apns-dev.pem';
    }
    
    /** @Author: DungNT Dec 06, 2016
     *  @Todo: lấy các type sẽ gửi luôn khi tạo
     */
    public static function getArrayTypeSendNow() {
        return array(
//            GasScheduleNotify::UPHOLD_CREATE, 
//            GasScheduleNotify::TRANSACTION_CHANGE_EMPLOYEE // Dec 16, 2016 sẽ controll send luôn ở lúc tạo record
        );
    }

    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{_gas_schedule_notify}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        return array(
            array('id, user_id, type, obj_id, time_send, created_date', 'safe'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        return array(
            'rUser' => array(self::BELONGS_TO, 'Users', 'user_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'user_id' => 'User',
            'type' => 'Type',
            'obj_id' => 'Obj',
            'time_send' => 'Time Send',
            'created_date' => 'Created Date',
        );
    }

    public function search()
    {
        $criteria=new CDbCriteria;
        $criteria->compare('t.user_id',$this->user_id);
        $criteria->compare('t.type',$this->type);
        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
            'pagination'=>array(
                'pageSize'=> Yii::app()->user->getState('pageSize',Yii::app()->params['defaultPageSize']),
            ),
        ));
    }
    
    /**
     * @Author: DungNT Dec 02, 2015
     * @Todo: insert to db
     * @Flow1: bao tri su co 10 phut
     * 1/ insert 2 record to db 10' va 20'
     * 2/ cron send notify bình thường, send xong thì xóa notify đi
     * 3/ nếu user update status: xử lý hoặc Hoàn Thành thì xóa hết notify đi => End
     * @Flow2: bao tri định kỳ
     * 1/ đầu tháng chạy cron tạo các record trong tháng cho KH 
     * 2/ khi cron chạy thì tạo mới record Reply rồi mới send notify cho KH gửi lúc 6h sáng ngày YC
     * 3/ sau khi send thì xóa cron đi => End
     * @param: $json_var array any variable
     */
    public static function InsertRecord($user_id, $type, $obj_id, $time_send, $title, $json_var) {
        $mScheduleNotify = new GasScheduleNotify();// Sep3018 add check getTypeErrorsHgd để không bị miss tính lỗi cho các CV và GĐKV
        if(empty($time_send)){
            $time_send = date('Y-m-d H:i:s');
        }
        $AppLogin = UsersTokens::getByUserId($user_id);
        if( (is_null($AppLogin) && !in_array($type, $mScheduleNotify->getTypeErrorsHgd())) ||
            (is_null($AppLogin) && $type == GasScheduleNotify::ERROR_HGD_ALERT_5)// nếu PVKH thoát app rồi thì ko notify tính lỗi nữa
                || $mScheduleNotify->isTurnOffNotify($user_id)
        ){
            // kiểm tra user id này có login = app chưa 
            // nếu chưa login = app thì sẽ không build notify
            return null;
        }
        
        $model                  = new GasScheduleNotify();
        $model->user_id         = $user_id;
        $model->type            = $type;
        $model->obj_id          = $obj_id;
        $model->time_send       = $time_send;
        $model->title           = $title;
        $model->username        = $model->rUser ? $model->rUser->username : "";
        $model->json_var        = json_encode($json_var);
        $model->save();
        // Apr 11, 2016
        if(in_array($type, GasScheduleNotify::getArrayTypeSendNow())){
            $model->sendImmediateForSomeUser();
        }
        return $model;
    }
    
    // Apr1018 tạo hàm inser mới theo obj
    public function insertRecordFix() {
        if($this->isTurnOffNotify($this->user_id)){
            return null;
        }
        if(empty($this->time_send)){
            $this->time_send = date('Y-m-d H:i:s');
        }
        $this->username        = $this->rUser ? $this->rUser->username : '';
        $this->json_var        = json_encode($this->json_var);
        $this->save();
        return $this;
    }
    
    /** @Author: DungNT Dec 01, 2018
     *  @Todo: check ko send notify with some user
     **/
    public function isTurnOffNotify($user_id) {
        $mCodePartner   = new CodePartner();
        if(in_array($user_id, $mCodePartner->getUidTurnOffNotify())){
            return true ;
        }
        return false;
    }
    
    /**
     * @Author: DungNT Apr 11, 2016
     * @Todo: gửi luôn cho 1 số user cần thiết
     * @Param: vd nv bảo trì
     */
    public function sendImmediateForSomeUser() {
        $mScheduleNotify = GasScheduleNotify::model()->findByPk($this->id);
        if(is_null($mScheduleNotify)){
            return ;
        }
//        Logger::WriteLog("Notify id = $mScheduleNotify->id DEBUG NOTIFY function isSendImmediate user_id = $mScheduleNotify->user_id ");
        $aTypeViewOnApp = $this->getTypeViewOnApp();
        $mScheduleNotify->isSendImmediate = true;
        if($mScheduleNotify->type == GasScheduleNotify::UPHOLD_CREATE){
            self::UpholdCreate($mScheduleNotify);
        }elseif(isset($aTypeViewOnApp[$mScheduleNotify->type])){
            if($mScheduleNotify->type == GasScheduleNotify::GAS_SERVICE_REMAIN){
                $mScheduleNotify->isSendImmediate = false;// for dev test
            }
            $mScheduleNotify->sendForAll($aTypeViewOnApp[$mScheduleNotify->type]);
        }
    }
    
    /** @Author: DungNT Oct 10, 2018 
     *  @Todo: map type from $mScheduleNotify with type ApiNotify on app view
     **/
    public function getTypeViewOnApp() {
        return [
            GasScheduleNotify::UPHOLD_CREATE                => ApiNotify::TYPE_UPHOLD,
            GasScheduleNotify::TRANSACTION_CHANGE_EMPLOYEE  => ApiNotify::TYPE_TRANSACTION,
            GasScheduleNotify::APP_ORDER_BO_MOI             => ApiNotify::TYPE_ORDER_BO_MOI,
            GasScheduleNotify::UPHOLD_HGD                   => ApiNotify::TYPE_UPHOLD_HGD,
            GasScheduleNotify::TEST_NOTIFY                  => ApiNotify::TYPE_UPHOLD_HGD,
            GasScheduleNotify::GAS24H_OUT_OF_GAS            => ApiNotify::TYPE_GAS24H_OUT_OF_GAS,
            GasScheduleNotify::GAS24H_REMAIN                => ApiNotify::TYPE_GAS24H_REMAIN,
            GasScheduleNotify::GAS_SERVICE_REMAIN           => ApiNotify::TYPE_GAS_SERVICE_REMAIN,
            GasScheduleNotify::SUPPORT_REQUEST_ITEM         => ApiNotify::TYPE_SUPPORT_REQUEST_ITEM,
            GasScheduleNotify::CUSTOMER_HGD_ORDER           => ApiNotify::TYPE_GAS24H_ORDER,
            GasScheduleNotify::TYPE_TRUCK_PLAN              => ApiNotify::TYPE_TRUCK_PLAN,
            GasScheduleNotify::GAS24H_NEW_LUCKY_NUMBER      => ApiNotify::TYPE_GAS24H_SPIN, // DuongNV Sep1919 list number quay so gas24h
            GasScheduleNotify::GAS24H_NUMBER_WINNING        => ApiNotify::TYPE_GAS24H_SPIN, // DuongNV Sep1919 so trung giai quay so gas24h
        ];
    }

    /**
     * @Author: DungNT Dec 02, 2015
     * @Todo: delete all by some condtion
     */
    public static function DeleteAllByType($type, $obj_id) {
        $criteria = new CDbCriteria;
        if(is_array($type)){
            $sParamsIn = implode(',',  $type);
            $criteria->addCondition("type IN ($sParamsIn)");
        }else{
            $criteria->addCondition('type=' . $type);
        }
        if(!empty($obj_id)){
            $criteria->addCondition('obj_id=' . $obj_id);
        }
        self::model()->deleteAll($criteria);
    }
    
    /** @Author: DungNT Dec 02, 2015
     *  @Todo: run cron notify
     */
    public function runCron() {
//        $from = time();
        // 1. gửi record độ ưu tiên cao
        $aData = $this->getDataCron();
        $this->runCronSending($aData);
        
        // 2. gửi record độ ưu tiên thấp
        $aData = $this->getDataCronLowPriority();
        $this->runCronSending($aData);
        
//        $to = time();// Apr 03, 2017 tạm close log notify lại vì nó tràn ra view log nhiều quá
//        $second = $to-$from;
//        $CountData = count($data);
//        $ResultRun = "CRON Notify App: ".$CountData.' done in: '.($second).'  Second  <=> '.round($second/60, 2).' Minutes ';
//        $ResultRun .= implode(" --- ", CHtml::listData($data, "user_id", "username"));
//        if($CountData){
//            Logger::WriteLog($ResultRun);
//        }
    }

    public function runCronSending($aData) {
        foreach($aData as $mScheduleNotify){
            switch ($mScheduleNotify->type){
                case GasScheduleNotify::UPHOLD_ALERT_10:
                    self::HandleAlert10($mScheduleNotify);
                    break;
//                case GasScheduleNotify::UPHOLD_DINH_KY_1_DAY:
//                    self::HandleAlertDinhKy($mScheduleNotify);
//                    break;
                case GasScheduleNotify::ISSUE_TICKET:
                    self::HandleAlertIssueTicket($mScheduleNotify);
                    break;
                case GasScheduleNotify::UPHOLD_CREATE:
                    self::UpholdCreate($mScheduleNotify);
                    break;
                case GasScheduleNotify::TRANSACTION_CHANGE_EMPLOYEE:
                case GasScheduleNotify::ERROR_HGD_ALERT_5:
                case GasScheduleNotify::ERROR_HGD_ALERT_10:
                case GasScheduleNotify::ERROR_HGD_ALERT_20:
                case GasScheduleNotify::ERROR_HGD_ALERT_3:
                case GasScheduleNotify::ERROR_HGD_ALERT_15:
                case GasScheduleNotify::ANNOUNCE_TO_ALL:
                case GasScheduleNotify::ERROR_HGD_ALERT_3_GDKV:
                case GasScheduleNotify::ERROR_HGD_ALERT_8:
                case GasScheduleNotify::ERROR_HGD_ALERT_COMPLETE_18:
                case GasScheduleNotify::ERROR_HGD_ALERT_COMPLETE_28:
                case GasScheduleNotify::ERROR_HGD_ALERT_COMPLETE_38:
                    $mScheduleNotify->saveErrorsHgd();
                    $mScheduleNotify->sendForAll(ApiNotify::TYPE_TRANSACTION);
                    break;
                case GasScheduleNotify::APP_ORDER_BO_MOI:
                    $mScheduleNotify->sendForAll(ApiNotify::TYPE_ORDER_BO_MOI);
                    break;
                case GasScheduleNotify::UPHOLD_HGD:
                case GasScheduleNotify::TEST_NOTIFY:
                    $mScheduleNotify->sendForAll(ApiNotify::TYPE_UPHOLD_HGD);
                    break;
                case GasScheduleNotify::TICKET_EVENT:
                    $mScheduleNotify->sendForAll(ApiNotify::TYPE_TICKET_EVENT);
                    break;
                case GasScheduleNotify::SPJ_CODE_GOBACK:
                    $mScheduleNotify->sendForAll(ApiNotify::TYPE_SPJ_CODE_GOBACK);
                    break;
                case GasScheduleNotify::CUSTOMER_PAY_DEBIT:
                    $mScheduleNotify->sendForAll(ApiNotify::TYPE_CUSTOMER_PAY_DEBIT);
                    break;
                case GasScheduleNotify::CUSTOMER_HGD_ORDER:
                    $mScheduleNotify->sendForAll(ApiNotify::TYPE_GAS24H_ORDER);
                    break;
                case GasScheduleNotify::CMS_NEWS:
                    $mScheduleNotify->sendForAll(ApiNotify::TYPE_CMS_NEWS);
                    break;
//                case GasScheduleNotify::GAS24H_OUT_OF_GAS:
//                    $mScheduleNotify->sendForAll(ApiNotify::TYPE_GAS24H_OUT_OF_GAS);
//                    break;
//                case GasScheduleNotify::GAS24H_REMAIN:
//                    $mScheduleNotify->sendForAll(ApiNotify::TYPE_GAS24H_REMAIN);
//                    break;
//                case GasScheduleNotify::GAS_SERVICE_REMAIN:
//                    $mScheduleNotify->sendForAll(ApiNotify::TYPE_GAS_SERVICE_REMAIN);
//                    break;
                case GasScheduleNotify::SUPPORT_REQUEST_ITEM:
                    $mScheduleNotify->sendForAll(ApiNotify::TYPE_SUPPORT_REQUEST_ITEM);
                    break;
                case GasScheduleNotify::TYPE_TRUCK_PLAN:
                    $mScheduleNotify->sendForAll(ApiNotify::TYPE_TRUCK_PLAN);
                    break;
                case GasScheduleNotify::GAS24H_NEW_LUCKY_NUMBER:
                case GasScheduleNotify::GAS24H_NUMBER_WINNING:
                    $mScheduleNotify->sendForAll(ApiNotify::TYPE_GAS24H_SPIN);
                    break;
            }
//            $mScheduleNotify->delete();// Xóa luôn sau khi send notify 
            $mScheduleNotify->plusRun();// May 06, 2017 không thể xóa dòng này được, vì nó liên quan đến xóa notify nhắc nhở bảo trì lần 1, lần 2
//            May 05, 2017 chỉ cần send 1 lần cho cả android và ios, server google và apple sẽ gửi tới client ngay cả khi client offline, thì khi online lên sẽ nhận dc notify
            // close on Dec 22, 2015 sẽ không xóa ở đây nữa, mà khi nào client nhận dc notify thì mới xóa, nếu chưa nhận dc thì cứ send bình thường
        }
    }
    
    /** handle notify for all model
     * @Author: DungNT Dec 06, 2016
     * @Todo: 1/ Send notification to apps when have event
            2/ User touch on the notification to open Any View Details in app
     * @param: $typeView: is type of ApiNotify, ex: ApiNotify::TYPE_UPHOLD, ApiNotify::TYPE_TRANSACTION ...
     * $typeView để dưới android  if else xác định view detail tương ứng
     */
    public function sendForAll($typeView) {
        $gas24hPemFile  = $this->getGas24hPemFile();
        $defaultPemFile = $this->getGasServicePemFile();
        $aUid           = array($this->user_id);
        $DeviceAndroid  = UsersTokens::getListDeviceToken('gcm_device_token', $aUid, UsersTokens::LOGIN_ANDROID);
        $DeviceIos      = UsersTokens::getListDeviceToken('apns_device_token', $aUid, UsersTokens::LOGIN_IOS);
        $customerProperty = array(
            'notify_id'     => $this->id,
            'notify_type'   => $this->type,
            'type'          => $typeView,
            'id'            => $this->obj_id,
            'reply_id'      => ''
        );
        $message = $this->title;
        ApiNotify::BuildToSend($DeviceAndroid, $message, $customerProperty, YiiApnsGcm::TYPE_GCM);
        if($this->isSendImmediate){
            $DeviceIos = [];// không gửi ngay đối với device ios vì lỗi và ghi log nhiều
        }
        if(count($DeviceIos)){
//        if(0){// Feb1219 tam close lai - do bi loi file .PEM
            if(in_array($this->type, $this->getArrayTypeGas24hIos())){
                $this->setGas24hPemIos($gas24hPemFile);
                ApiNotify::BuildToSend($DeviceIos, $message, $customerProperty, YiiApnsGcm::TYPE_APNS);
                $this->setGas24hPemIos($defaultPemFile);
//                Logger::WriteLog("Ios sendForAll CUSTOMER_HGD_ORDER - 16 => id notify: $this->id UserId: $this->user_id");
            }else{
                $this->setGas24hPemIos($defaultPemFile);
                ApiNotify::BuildToSend($DeviceIos, $message, $customerProperty, YiiApnsGcm::TYPE_APNS);
            }
        }
    }
    
    /** @Author: DungNT Now 25, 2017
     *  @Todo: set file .PEM Gas24h của ios. Do gửi 2 app từ cùng 1 server nên phải change file .pem
     **/
    public function setGas24hPemIos($pemFile) {
        $this->getComponents($pemFile);
//        Logger::WriteLog(Yii::app()->apns->pemFile);
    }
    
    /** @Author: DungNT Now 25, 2017
     *  @Todo: set new components to make new pem file
     * too many time to fix it https://github.com/yiisoft/yii/issues/3240
     **/
    public function getComponents($pemFile) {
        $components=array(
            'apns' => array(
                'class' => 'ext.yii-apns-gcm-master.YiiApns',
                'environment' => 'production',
//                 'environment' => 'sandbox',
                'pemFile' => $pemFile,
                'dryRun' => false, // setting true will just do nothing when sending push notification
                // 'retryTimes' => 3,
                'enableLogging' => true,// Dec 21, 2016 disable log // close for test only Apr 14, 17
                'options' => array(
                    'sendRetryTimes' => 5
                ),
            ),
        );
        Yii::app()->setComponents($components, false);
//        Yii::app()->setComponents('apns', $apns, false);
    }
    
    /** @Author: DungNT Now 19, 2017
     *  @Todo: lưu lại các trường hợp bị phạt khi chưa xác nhận đơn HGD hoặc các loại phạt khác
     **/
    public function saveErrorsHgd() {
//        if($this->type == GasScheduleNotify::ERROR_HGD_ALERT_3 || !in_array($this->type, $this->getTypeErrorsHgd())){
        if(!in_array($this->type, $this->getTypeErrorsHgd())
                || in_array($this->type, $this->getTypeErrorsHgdAlertOnly())// May0819 add some notify alert only
        ){// Sep0418 mở tính lỗi 5 phút cho PVKH
            return ;
        }
        $mTransactionHistory = TransactionHistory::model()->findByPk($this->obj_id);
        if(is_null($mTransactionHistory)){
            return ;
        }
        // không save lỗi với các user monitor như GĐKV, chuyên viên
        $aUidOfAgent   = $mTransactionHistory->getListEmployeeMaintainOfAgent();
//        if(!in_array($this->user_id, $aUidOfAgent) && $this->user_id != GasConst::UID_NGOC_PT){// Sep1118 xu ly cho BC cua Ngoc, chua lam
        if(in_array($this->user_id, $aUidOfAgent)){
            $this->saveErrorsHgdPvkh();
        }
        // Aug 29, 2018 Pham Thanh Nghia  lưu lỗi trễ đơn hàng
//        $this->saveErrorsConfirmOrder($type);

        $this->saveErrorsHgdSms($mTransactionHistory, $aUidOfAgent);
    }
    
    /** @Author: DungNT Oct 01, 2018
     *  @Todo: save lỗi 5 phut cho PVKH 
     **/
    public function saveErrorsHgdPvkh() {
        if($this->type != GasScheduleNotify::ERROR_HGD_ALERT_5){
            return ;
        }
        $typeOnMonitorUpdate = MonitorUpdate::TYPE_ERROR_HGD_ALERT_5;
        $model = new MonitorUpdate();// save lỗi 5,10,20 phút
        $model->type        = $typeOnMonitorUpdate;
        $model->user_id     = $this->user_id;// employee GN
        $model->agent_id    = $this->obj_id;// is transaction_history_id
        $model->makeRecordNew();
        // for remove old record
//        $model->type        = xxx;
//        $model->deleteByType();
    }
    
    /**
     * @Author: DungNT Dec 21, 2015
     * BT sự cố tạo mới và reply
     */
    public static function UpholdCreate($mScheduleNotify) {
        $mUphold = GasUphold::model()->findByPk($mScheduleNotify->obj_id);
        if($mUphold){
            $mUphold->ApiSendNotify(array($mScheduleNotify->user_id), $mScheduleNotify->title, 0, $mScheduleNotify);
        }
    }
    
    /**
     * @Author: DungNT Dec 02, 2015
     * BT sự cố 10' send 1 lần ( send 2 lần cho 1 Record)
     */
    public static function HandleAlert10($mScheduleNotify) {
        $mUphold = GasUphold::model()->findByPk($mScheduleNotify->obj_id);
        if($mUphold){
            $mUphold->NotifyCronAlert($mScheduleNotify);
        }
    }
    
    /**
     * @Author: DungNT Dec 02, 2015
     * BT định kỳ, gửi 6h sáng mỗi ngày
     */
    public static function HandleAlertDinhKy($mScheduleNotify) {
        $mUphold = GasUphold::model()->findByPk($mScheduleNotify->obj_id);
        if($mUphold){
            // 1. tạo 1 record mới của Reply 
            $mReply = GasUpholdReply::CronCreateReplyDinhKy($mUphold);
            // 2. send notify theo model Reply moi tao
            $json_var = array("reply_id" => $mReply->id);
            $mScheduleNotify->UpdateJsonVar($json_var);
            $mUphold->NotifyCronAlertDinhKy($mReply, $mScheduleNotify);
        }
    }
    
    /**
     * @Author: DungNT Dec 22, 2015
     * @Todo: phải update lại cái reply_id vì khi view ở history nếu không có cái reply_id này thì không view dc
     */
    public function UpdateJsonVar($json_var) {
        $this->json_var = json_encode($json_var);
        $this->update(array('json_var'));
    }
    
    /**
     * @Author: DungNT Dec 11, 2015
     * Issue Ticket notify
     */
    public static function HandleAlertIssueTicket($mScheduleNotify) {
        $mIssue = GasIssueTickets::model()->findByPk($mScheduleNotify->obj_id);
        if($mIssue){
            $aUid = array($mScheduleNotify->user_id);
            $mIssue->ApiSendNotify($aUid, $mScheduleNotify);
        }
    }
    
    /** @Author: DungNT Dec 02, 2015
     * @Todo: get data thỏa mãn cron để send, những data có độ ưu tiên cao
     */
    public function getDataCron() {
        // Dec 26, 2015 xử lý khi count_run > 5 thì ngừng send lại, có thể user đã offline
        // Nếu user online trở lại thì ở hàm update config mình sẽ reset count_run về 0 cho những count run > 5
        $criteria = new CDbCriteria();
        $sParamsIn = implode(',',  $this->getTypeLowPriority());
        $criteria->addCondition("t.type NOT IN ($sParamsIn)");
        $this->getConditionSame($criteria);
        return self::model()->findAll($criteria);
    }
    
    /** @Author: DungNT Dec 07, 2017
     * @Todo: những data có độ ưu tiên thấp
     */
    public function getDataCronLowPriority() {
        // Dec 26, 2015 xử lý khi count_run > 5 thì ngừng send lại, có thể user đã offline
        // Nếu user online trở lại thì ở hàm update config mình sẽ reset count_run về 0 cho những count run > 5
        $criteria = new CDbCriteria();
        $sParamsIn = implode(',',  $this->getTypeLowPriority());
        $criteria->addCondition("t.type IN ($sParamsIn)");
        $this->getConditionSame($criteria);
        return self::model()->findAll($criteria);
    }
    public function getConditionSame(&$criteria) {// Apr1018 gộp condition same
        $sParamsIn = implode(',',  $this->getTypeBigData());
        $criteria->addCondition("t.type NOT IN ($sParamsIn)");
        $criteria->addCondition('t.time_send <= NOW() AND t.count_run < '.GasScheduleNotify::MAX_COUNT_RUN);
        $criteria->order = 't.count_run ASC, t.id ASC';
        $criteria->limit = GasScheduleNotify::MAX_SEND;
    }
    
    /**
     * @Author: DungNT Dec 22, 2015
     * @Todo: get data thỏa mãn condition
     * @param $user_id 
     * @param $notify_id primary key
     * @param $type 
     * @param $obj_id 
     */
    public static function getRecord($user_id, $notify_id, $type, $obj_id) {
        $criteria = new CDbCriteria();
        $criteria->compare('t.id', $notify_id);
        $criteria->compare('t.user_id', $user_id);
//        $criteria->compare("t.type", $type);// apr 14, 2017 close lại vì thấy ko cần thiết, dư condition
//        $criteria->compare("t.obj_id", $obj_id);
        return self::model()->find($criteria);
    }
    
    /**
     * @Author: DungNT Dec 22, 2015
     * @Todo: cộng số lần run của notify thêm 1
     */
    public function plusRun() {
        $this->count_run += 1;
        $this->update(array('count_run'));
    }
    
    /**
     * @Author: DungNT Dec 26, 2015
     * @Todo: reset so lần count run của notify = 0
     * update khi app run update config
     */
    public static function ResetCountRun($user_id) {
        $criteria = new CDbCriteria();
        $criteria->compare( 'user_id', $user_id);
        $aUpdate = array('count_run' => 1);
        GasScheduleNotify::model()->updateAll($aUpdate, $criteria);
    }
    
    /**
     * @Author: DungNT Dec 22, 2015
     * @Todo: move những record được tạo 1 ngày trước qua bên table history
     * GasScheduleNotify::moveNotifyToHistory();
     */
    public static function moveNotifyToHistory() {
        $from = time();
        $criteria=new CDbCriteria;
        $criteria->addCondition("DATE_ADD(created_date, INTERVAL 1 DAY) < NOW()");
        $models = self::model()->findAll($criteria);
        $CountData = count($models);
        foreach($models as $mScheduleNotify) {
            GasScheduleNotifyHistory::InsertNew($mScheduleNotify);
        }
        $to = time();
        $second = $to-$from;
        $ResultRun = "CRON App GasScheduleNotify MoveNotifyToHistory : ".$CountData.' done in: '.($second).'  Second  <=> '.round($second/60, 2).' Minutes ';
        if($CountData){
            Logger::WriteLog($ResultRun);
        }
    }
    
    /** @Author: DungNT Aug 14, 2017
     * @Todo: đếm các notify đang chờ
     */
    public static function countWait() {
        $criteria = new CDbCriteria();
        $criteria->addCondition('t.time_send <= NOW() AND t.count_run < '.GasScheduleNotify::MAX_COUNT_RUN);
        return self::model()->count($criteria);
    }
    
    
    /**
     * @Author: DungNT Apr 20, 2016
     * @Todo: send test message
     */
    public static function TestNotifyAll() {
        $message = 'Spj test notify GCM';
        $aCustom = array('a'=>'b');
        $args = array(
            'timeToLive' => 3
          );
        
//        $AndroidTokenOld = 'APA91bEig9l_3qBCfKwaRe-ZRfyHxwGIQ52H8hGheIDM99La4BMfZf-gZgOB1S2u-qotuaT04Xm8A8wo6SFbMq24ivJCoHe8jwhSTTESACkAxqKVvt46Wo18vF13OEbMHSed9wl7hgx_';
        $AndroidToken = 'fX_5kNM6MKk:APA91bGDnA_CMsvckBXze9GUgegoTDxpEynDFCpYnbyudMEzuZD6iomFht7knzsa9tDzF4_MXXV1dC_vf0cVODv6ct_f_EeGtIixNNmF0lFbi6hP4Y5mt1qCtEZKdSK3LZ0n3FV3LSWH'; // Anh Long galaxy A7
//        $AndroidToken = 'c9Y2NnAmIJg:APA91bFApjK67NO3Xd8eNsoeQ8pvjeZmKzQBzq_DNU91YYf9x78RJcKhztrBqHuUecvWd7iEmxBQhsNXjBFzcWMlESh87NoQT7_4ofFU-I_y0TS01tKmye0yzoMZnND8W00G6KWLhIKy'; // 4 inch lenovo
        $gcm = Yii::app()->gcm;
        $allToken = UsersTokens::model()->findAll();
        foreach($allToken as $mToken){
            $AndroidToken = $mToken->gcm_device_token;
            $gcm->send($AndroidToken, $message,
              $aCustom,
              $args
            );
        }
        echo '<pre>';
        print_r('OK GCM');
        echo '</pre>';
        exit;
    }
    
    public function testApp() {
        $title = $_SERVER['SERVER_ADDR']." - Test thông báo gửi từ noibo.daukhimiennam.com time: ". date('Y-m-d H:i:s');
        $mScheduleNotify = GasScheduleNotify::InsertRecord($this->user_id, GasScheduleNotify::TEST_NOTIFY, 0, '', $title, []);
        if(!empty($mScheduleNotify)){
            $mScheduleNotify->sendImmediateForSomeUser();
        }
    }
    public function testAppListUser($aUid) {
        foreach($aUid as $user_id){
            $title = 'Test thông báo gửi từ noibo.daukhimiennam.com';
            $mScheduleNotify = GasScheduleNotify::InsertRecord($user_id, GasScheduleNotify::TEST_NOTIFY, 0, '', $title, []);
            if(!empty($mScheduleNotify)){
                $mScheduleNotify->sendImmediateForSomeUser();
            }
        }
    }
    
    /** @Author: DungNT Apr 10, 2018
     *  @Todo: phân tích notify SL lớn, dạng thông báo
     * 1. Get all Token User của android và ios
     * 2. build sql insert 1 lần với type = GAS24H_ANNOUNCE
     *  build riêng notify của android và ios riêng biệt, gửi làm 2 cron riêng
     * 2.1. update cron_lock = 1 với những record vừa được lấy ra
     * 3. giữ nguyên flow send cũ
     * 4. set cron_lock = 0 nếu đã chạy xong
     * @return: false nếu còn token chưa build notify
     *          true nếu đã build hết notify cho all token
     * @step2: build cron send big notify cho android và ios
     * @step3: bổ sung field trong model AppPromotin - time_send_notify
     *      - test insert + run sending
     * @step5: bổ sung: cập nhật is_maintain trong model UsersTokens -- done
     * DELETE FROM `gas_gas_schedule_notify` WHERE `type` = 18
     **/
    public function buildGas24hAnnounce() {
        $res = false; $from = time();
        ini_set('memory_limit','2000M');
        // 1. Get all Token User của android và ios
        $this->buildGas24hAnnounceGetToken();
        if(count($this->aUserAndroid) == 0 && count($this->aUserIos) == 0 ){
            $res = true;
            // set cron_lock = 0 for all record
            $this->buildGas24hAnnounceSetFinishAll();
//            Logger::WriteLog('Set buildGas24hAnnounceSetFinishAll');
        }
        $this->buildGas24hAnnounceInsert($this->aUserAndroid, UsersTokens::PLATFORM_ANDROID);
        $this->buildGas24hAnnounceInsert($this->aUserIos, UsersTokens::PLATFORM_IOS);
        
        $totalAndroid   = count($this->aUserAndroid);
        $totalIos       = count($this->aUserIos);
        $to = time();
        $second = $to-$from;
        $info = "buildGas24hAnnounce Total Android: $totalAndroid - Ios: $totalIos record  done in: $second  Second  <=> ".($second/60)." Minutes";
        Logger::WriteLog($info);
        
        $this->aUserAndroid = $this->aUserIos = null;
        return $res;
    }
    
    // 1. Get all Token User của android và ios
    public function buildGas24hAnnounceGetToken() {
        $limit = 5000;
        $mUsersTokens = new UsersTokens();
        $mUsersTokens->mPromotion = $this->mPromotion;
        $this->aUserAndroid   = $mUsersTokens->getUserGas24hByPlatform(UsersTokens::LOGIN_ANDROID, $limit);
        $this->aUserIos       = $mUsersTokens->getUserGas24hByPlatform(UsersTokens::LOGIN_IOS, $limit);
    }
    
    /**
     * @Author: DungNT Apr 10, 2018
     * @Todo: insert to table notify
     * @Param: $aUid array user id need notify
     */
    public function buildGas24hAnnounceInsert($aModelToken, $platform) {
        if(empty($this->mPromotion->time_send_notify)){
            throw new Exception('Chưa set time send notify promotion '.$this->mPromotion->id);
        }
        if(count($aModelToken) < 1){
            return ;
        }
        $created_date = $this->getCreatedDateDb();
        $aRowInsert = []; $type = GasScheduleNotify::GAS24H_ANNOUNCE;
        $aIdUpdateCronLock = [];
        foreach($aModelToken as $mUserToken) {
            $aIdUpdateCronLock[] = $mUserToken->id;
            $aRowInsert[] = "(
                '$mUserToken->user_id',
                '$type',
                '{$this->mPromotion->id}',
                '{$this->mPromotion->time_send_notify}',
                '{$this->mPromotion->title}',
                '$platform',
                '{$mUserToken->apns_device_token}',
                '{$mUserToken->gcm_device_token}',
                '{$created_date}'
            )";
        }
        
        $sql = $this->buildOneQuery($aRowInsert);
        if(count($aRowInsert)>0){
            Yii::app()->db->createCommand($sql)->execute();
            $this->buildGas24hAnnounceSetRunToken($aIdUpdateCronLock);
        }
    }
    
    /** @Author: DungNT Apr 11, 2018 build sql query **/
    public function buildOneQuery($aRowInsert) {
        $tableName = GasScheduleNotify::model()->tableName();
        $sql = "insert into $tableName (
                        user_id,
                        type,
                        obj_id,
                        time_send,
                        title,
                        platform,
                        apns_device_token,
                        gcm_device_token,
                        created_date
                        ) values ".implode(',', $aRowInsert);
        return $sql;
    }
    
    /** @Author: DungNT Jun 16, 2019 fix build sql one query full field**/
    public function buildOneQueryFullField($aRowInsert) {
        $tableName = GasScheduleNotify::model()->tableName();
        $sql = "insert into $tableName (
                        user_id,
                        type,
                        obj_id,
                        time_send,
                        username,
                        title,
                        json_var,
                        platform,
                        apns_device_token,
                        gcm_device_token,
                        created_date
                        ) values ".implode(',', $aRowInsert);
        return $sql;
    }
    
    /** @Author: DungNT May 08, 2019
     *  @Todo: run sql one query  **/
    public function runOneQuery($aRowInsert) {
        $sql = $this->buildOneQuery($aRowInsert);
        if(count($aRowInsert)>0){
            Yii::app()->db->createCommand($sql)->execute();
        }
    }
    
    /** @Author: DungNT Jun 16, 2019
     *  @Todo: run sql one query  **/
    public function runOneQueryFullField($aRowInsert) {
        $sql = $this->buildOneQueryFullField($aRowInsert);
        if(count($aRowInsert)>0){
            Yii::app()->db->createCommand($sql)->execute();
        }
    }
    
    /** @Author: DungNT Apr 10, 2018
     *  @Todo: update back UsersTokens set Cron lock = 1 để không dò lại nữa 
     **/
    public function buildGas24hAnnounceSetRunToken($aIdUpdateCronLock) {
        $criteria = new CDbCriteria();
        $sParamsIn = implode(',', $aIdUpdateCronLock);
        $criteria->addCondition("id IN ($sParamsIn)");
        $aUpdate = array('cron_lock' => 1);
        UsersTokens::model()->updateAll($aUpdate, $criteria);
    }
    /** @Author: DungNT Apr 10, 2018
     *  @Todo: update back UsersTokens set cron_lock = 0 nếu đã chạy xong
     **/
    public function buildGas24hAnnounceSetFinishAll() {
        $criteria = new CDbCriteria();
        $criteria->addCondition("id > 0");
        $aUpdate = array('cron_lock' => 0);
        UsersTokens::model()->updateAll($aUpdate, $criteria);
    }
    
    /** @Author: DungNT Apr 10, 2018
     *  @Todo: run cron send big notify
     */
    public function cronBigQty($platform) {
        $cHours = date('H');
        if($cHours < 7 || $cHours > 19 ) {
            return ; // chỉ gửi từ 7h sáng đến 20h tối
        }
        
        $gas24hPemFile  =  Yii::getPathOfAlias('webroot').'/protected/config/apnssert/gas24h.pem';
        $defaultPemFile = Yii::getPathOfAlias('webroot').'/protected/config/apnssert/apns-dev.pem';
        if($platform == UsersTokens::PLATFORM_IOS){
            $this->setGas24hPemIos($gas24hPemFile);
        }
        $from = time();
        $aData = $this->cronBigQtyGetData($platform);
        $this->cronBigQtyHandleCase($aData, $platform);
        
        if($platform == UsersTokens::PLATFORM_IOS){
            $this->setGas24hPemIos($defaultPemFile);
        }
        $platformName = ($platform == UsersTokens::PLATFORM_ANDROID) ? 'Android' : 'Ios';
        
        $to = time();// Apr 03, 2017 tạm close log notify lại vì nó tràn ra view log nhiều quá
        $second = $to-$from;
        $CountData = count($aData);
        $ResultRun = "$platformName -- cronBigQty Notify App: ".$CountData.' done in: '.($second).'  Second  <=> '.round($second/60, 2).' Minutes ';
        $ResultRun .= implode(" - ", CHtml::listData($aData, 'user_id', 'user_id'));
        if($CountData){
//            Logger::WriteLog($ResultRun);
        }
    }
    
    
    /** @Author: DungNT Apr 11, 2018
     *  @Todo: lock data send 
     **/
    public function lockDataSend($aData) {
        $aIdLock = implode(',', CHtml::listData($aData, 'id', 'id'));
    }
    
    /** @Author: DungNT Apr 10, 2018
     * @Todo: get data thỏa mãn cron để send
     */
    public function cronBigQtyGetData($platform) {
        $limit = GasScheduleNotify::MAX_SEND_ANDROID;
        if($platform == UsersTokens::PLATFORM_IOS){
            $limit = GasScheduleNotify::MAX_SEND_IOS;
        }
        $criteria = new CDbCriteria();
        $sParamsIn = implode(',',  $this->getTypeBigData());
        $criteria->addCondition("t.platform=$platform");
        $criteria->addCondition("t.type IN ($sParamsIn)");
        $criteria->addCondition('t.time_send <= NOW() AND t.count_run < '.GasScheduleNotify::MAX_COUNT_RUN);
        $criteria->order = 't.count_run ASC, t.id ASC';
        $criteria->limit = $limit;
        return self::model()->findAll($criteria);
    }
    // xử lý cho nhiều type, số lượng $aData đã được giới hạn ở hàm find bên tren, nên không cần check giới hạn ở đây nữa
    public function cronBigQtyHandleCase($aData, $platform) {
        $aIdDelete = $aInfo = [];
        $count = 0;
        foreach($aData as $mScheduleNotify){
            $aIdDelete[] = $mScheduleNotify->id;
            if($mScheduleNotify->platform == UsersTokens::PLATFORM_ANDROID){
                $mScheduleNotify->device_token = $mScheduleNotify->gcm_device_token;
            }elseif($mScheduleNotify->platform == UsersTokens::PLATFORM_IOS){
                $mScheduleNotify->device_token = $mScheduleNotify->apns_device_token;
            }
            switch ($mScheduleNotify->type){
                case GasScheduleNotify::GAS24H_ANNOUNCE:
                    $aInfo[$mScheduleNotify->type][$mScheduleNotify->platform]['TYPE_VIEW'] = ApiNotify::TYPE_GAS24H_ANNOUNCE;
                    $aInfo[$mScheduleNotify->type][$mScheduleNotify->platform]['MESSAGE']   = $mScheduleNotify->title;
                    $aInfo[$mScheduleNotify->type][$mScheduleNotify->platform]['OBJ_ID']    = $mScheduleNotify->obj_id;
                    $aInfo[$mScheduleNotify->type][$mScheduleNotify->platform]['ARR_MODEL'][] = $mScheduleNotify;
                    break;
            }
            if($count == GasScheduleNotify::MAX_ONE_TIME_ANDROID){
                $this->cronBigQtyFixSendMulti($aInfo);
                $aInfo = [];
                $count = 0;
            }
            $count++;
        }

        $this->cronBigQtyFixSendMulti($aInfo);
        $this->deleteListId($aIdDelete);// không xử lý được (vẫn xóa) => May0518 không đc xóa notify này, mà sẽ move sang table chờ gửi lại Resend
    }

    public function cronBigQtyFixSendMulti($aInfo) {
        foreach($aInfo as $type => $aInfo1):
            foreach($aInfo1 as $platform => $aInfo2):
                $aModelToken    = $aInfo2['ARR_MODEL'];
                $notify_id      = rand(10,100).'';//  Error: Messaging payload contains an invalid value for the "data.notify_id" property. Values must be strings.
                $notify_type    = $type.'';//  Error: Messaging payload contains an invalid value for the "data.notify_type" property. Values must be strings.
                $typeView       = $aInfo2['TYPE_VIEW'];
                $obj_id         = $aInfo2['OBJ_ID'];
                $message        = $aInfo2['MESSAGE'];
                
                $customerProperty = array(
                    'notify_id'     => $notify_id,
                    'notify_type'   => $notify_type,
                    'type'          => $typeView,
                    'id'            => $obj_id,
                    'reply_id'      => ''
                );
                if($platform == UsersTokens::PLATFORM_ANDROID){
                    ApiNotify::BuildToSend($aModelToken, $message, $customerProperty, YiiApnsGcm::TYPE_GCM);
                }elseif($platform == UsersTokens::PLATFORM_IOS){
                    ApiNotify::BuildToSend($aModelToken, $message, $customerProperty, YiiApnsGcm::TYPE_APNS);
                }
//                $sDebug = implode(', ', CHtml::listData($aModelToken, 'user_id', 'user_id'));
//                Logger::WriteLog("cronBigQtyFixSendMulti $sDebug");
            endforeach;
        endforeach;
    }
    
    public function deleteListId($aIdDelete) {
        if(count($aIdDelete) < 1){
            return ;
        }
        $criteria = new CDbCriteria;
        $sParamsIn = implode(',',  $aIdDelete);
        $criteria->addCondition("id IN ($sParamsIn)");
        GasScheduleNotify::model()->deleteAll($criteria);
    }
    
    /** handle notify for all model. Sử dụng gcm và apns của chính model  GasScheduleNotifyGasScheduleNotify, không sử dụng token bên user token 
     * @Author: DungNT Dec 06, 2016
     * @Todo: 1/ Send notification to apps when have event
            2/ User touch on the notification to open Any View Details in app
     * @param: $typeView: is type of ApiNotify, ex: ApiNotify::TYPE_UPHOLD, ApiNotify::TYPE_TRANSACTION ...
     * $typeView để dưới android  if else xác định view detail tương ứng
     */
    public function cronBigQtySending($typeView) {
        $customerProperty = array(
            'notify_id'     => $this->id,
            'notify_type'   => $this->type,
            'type'          => $typeView,
            'id'            => $this->obj_id,
            'reply_id'      => $this->reply_id,
        );
        $message = $this->title;
        if($this->platform == UsersTokens::PLATFORM_ANDROID){
            $this->device_token = $this->gcm_device_token;
            ApiNotify::BuildToSend([$this], $message, $customerProperty, YiiApnsGcm::TYPE_GCM);
        }elseif($this->platform == UsersTokens::PLATFORM_IOS){
            $this->device_token = $this->apns_device_token;
            ApiNotify::BuildToSend([$this], $message, $customerProperty, YiiApnsGcm::TYPE_APNS);
        }
    }
    
    /** @Author: DungNT Apr 11, 2018
     *  @Todo: chỉ notify 1 thông báo, không có bài viết detail
     **/
    public function buildGas24hNotifyOnly() {
        $from = time();
        $res = false;
        ini_set('memory_limit','2000M');
        // 1. Get all Token User của android và ios
        $this->buildGas24hAnnounceGetToken();
        if(count($this->aUserAndroid) == 0 && count($this->aUserIos) == 0 ){
            $res = true;
            // set cron_lock = 0 for all record
            $this->buildGas24hAnnounceSetFinishAll();
//            Logger::WriteLog('Set buildGas24hAnnounceSetFinishAll');
        }
        $this->buildGas24hNotifyOnlyInsert($this->aUserAndroid, UsersTokens::PLATFORM_ANDROID);
        $this->buildGas24hNotifyOnlyInsert($this->aUserIos, UsersTokens::PLATFORM_IOS);
        
        $totalAndroid   = count($this->aUserAndroid);
        $totalIos       = count($this->aUserIos);
        $to = time();// Apr 03, 2017 tạm close log notify lại vì nó tràn ra view log nhiều quá
        $second = $to-$from;
        $info = "buildGas24hNotifyOnly Total Android: $totalAndroid - Ios: $totalIos record done in: $second  Second  <=> ".($second/60)." Minutes";
        Logger::WriteLog($info);
        
        $this->aUserAndroid = $this->aUserIos = null;
        return $res;
    }
    
    
    /**
     * @Author: DungNT Apr 11, 2018
     * @Todo: insert to table notify
     * @Param: $aUid array user id need notify
     */
    public function buildGas24hNotifyOnlyInsert($aModelToken, $platform) {
        if(count($aModelToken) < 1){
            return ;
        }
        $created_date = $this->getCreatedDateDb();
        $aRowInsert = []; $type = GasScheduleNotify::GAS24H_ANNOUNCE;
        $aIdUpdateCronLock = []; $time_send_notify = date('Y-m-d H:i:s');
        foreach($aModelToken as $mUserToken) {
            $aIdUpdateCronLock[] = $mUserToken->id;
            $aRowInsert[] = "(
                '$mUserToken->user_id',
                '$type',
                '4168',
                '$time_send_notify',
                '{$this->title}',
                '$platform',
                '{$mUserToken->apns_device_token}',
                '{$mUserToken->gcm_device_token}',
                '{$created_date}'
            )";
        }
        
        $sql = $this->buildOneQuery($aRowInsert);
        if(count($aRowInsert)>0){
            Yii::app()->db->createCommand($sql)->execute();
            $this->buildGas24hAnnounceSetRunToken($aIdUpdateCronLock);
        }
    }
    /** @Author: Pham Thanh Nghia Aug 29, 2018
     **/
    public function getArrayTypeErrorsHgd() {
        return [
            GasScheduleNotify::ERROR_HGD_ALERT_5 =>'Không xác nhận đơn HGĐ 5 phút', 
            GasScheduleNotify::ERROR_HGD_ALERT_10 =>'Không xác nhận đơn HGĐ 10 phút', 
            GasScheduleNotify::ERROR_HGD_ALERT_20 =>'Không xác nhận đơn HGĐ 20 phút',
            GasScheduleNotify::ERROR_HGD_ALERT_15 =>'Không xác nhận đơn HGĐ 15 phút',
        ];
    }
    /** @Author: Pham Thanh Nghia Aug 31, 2018
     *  @Todo: lưu vào lỗi giao trễ đơn hàng
     **/
    public function saveErrorsConfirmOrder($type){
        $mError = new ErrorHgd();
        $mError->type                       = $type;
        $mError->employee_id                = $this->user_id;// employee GN
        $mError->transaction_history_id     = $this->obj_id;// is transaction_history_id
        $mError->makeRecordNew(); // tạo mới
    }
    
    /** @Author: DungNT Oct 02, 2018
     *  @Todo: get list user send error when PVKH not confirm order
     *  chỉ send khi cảnh báo 5, 10, 15 phút với đơn HGĐ
     **/
    public function saveErrorsHgdGetListUserSend($mTransactionHistory, $aUidOfAgent) {
        $idGdkv         = $mTransactionHistory->getOnlyIdGdkv();
        $aTypeAlertPvkh = [GasScheduleNotify::ERROR_HGD_ALERT_3, GasScheduleNotify::ERROR_HGD_ALERT_5];
        if(in_array($this->type, $aTypeAlertPvkh) && in_array($this->user_id, $aUidOfAgent)){// PVKH
            $this->aUidSend[$this->user_id]     = $this->user_id;// Chỉ save lỗi với đúng PVKH của đại lý
        }elseif($this->type == GasScheduleNotify::ERROR_HGD_ALERT_10){// CV
            $idChuyenVien = $mTransactionHistory->getOnlyIdChuyenVien();
            if($idChuyenVien == $this->user_id){
                $this->aUidSend[$idChuyenVien]  = $idChuyenVien;// Chỉ save lỗi với đúng chuyên viên + GDKV
            }
        }elseif($this->type == GasScheduleNotify::ERROR_HGD_ALERT_15 && $idGdkv == $this->user_id){// GĐKV
            $this->aUidSend[$idGdkv]    = $idGdkv;// Chỉ save lỗi với đúng chuyên viên + GDKV
        }
    }
    
    /** @Author: DungNT Sep 11, 2018 
     *  @Todo: send sms for PVKH and GDKV khi canh bao 5 phut, 10 phut
     *  Send SMS + save lỗi phạt vào EmployeeProblems
     * gửi SMS xong sau đó đưa vào table EmployeeProblems để tính đc tiền SMS cho NV
     **/
    public function saveErrorsHgdSms($mTransactionHistory, $aUidOfAgent) {
        /*  1. save table sms
            2. save table EmployeeProblems
         *  3. đơn app lần 2 thì chuyển cho 1 NV tổng đài gọi PVKH confirm
        */
        $this->saveErrorsHgdGetListUserSend($mTransactionHistory, $aUidOfAgent);
        if(count($this->aUidSend) < 1){
            return ;
        }
        foreach($this->aUidSend as $toUserId){
            if(in_array($toUserId, $mTransactionHistory->getOnlyIdGdkvAllList())){
                return ;// Aug1419 bỏ tính lỗi GĐKV chỉ tính cho Chuyên Viên
            }
            $mUser = Users::model()->findByPk($toUserId);
            // 1. Dec0618 fix ko tinh loi 2 thang NV hoc viec
            $mGasTickets = new GasTickets();
            if($mUser->status == STATUS_INACTIVE){
                continue ;
            }
            // 2. save table sms
            $mSms = $this->doSendSms($toUserId, $mTransactionHistory);
//            if(empty($mSms->id) || $this->type == GasScheduleNotify::ERROR_HGD_ALERT_3){ ko rõ tại sao cần check empty($mSms->id), Now218, bỏ check đi để run đc fix lỗi ticket T10 
            if($this->type == GasScheduleNotify::ERROR_HGD_ALERT_3 || $mGasTickets->isExceptionPunish($mUser)){
                continue ;
            }// mốc 3 phút chỉ send sms
            // 3. save table EmployeeProblems
            $this->doSaveEmployeeProblems($toUserId, $mSms, $mTransactionHistory);
        }
        // 4. đơn app lần 2 thì chuyển cho 1 NV tổng đài gọi PVKH confirm
        if($this->type == GasScheduleNotify::ERROR_HGD_ALERT_5){
            $this->moveToRandomCallCenter($mTransactionHistory);
        }
    }
    
    /** @Author: DungNT Sep 11, 2018
     *  @Todo: doing send sms
     **/
    public function doSendSms($toUserId, &$mTransactionHistory) {
        $mSms           = new GasScheduleSms();
        $mSmsHistory    = new GasScheduleSmsHistory();
        $mSms->phone    = $this->getPhoneUserOnlyOneNumber($toUserId);
        if(empty($mSms->phone)){
            return $mSms;
        }
        $this->overridePhoneSms($toUserId, $mTransactionHistory, $mSms);
        $mSms->title            = $this->title;
        $mSms->purgeMessage();
        $mSms->uid_login        = GasConst::UID_ADMIN;
        $mSms->user_id          = $toUserId;
        $mSms->type             = GasScheduleSms::TYPE_ANNOUNCE_INTERNAL;
        $mSms->obj_id           = $this->obj_id;// is transaction_history_id
        $mSms->content_type     = GasScheduleSms::ContentTypeKhongDau;
        $mSms->time_send        = '';
        $mSms->json_var         = [];
        $mSms->network          = $mSmsHistory->findNetworkOfPhone($mSms->phone);
        $mTransactionHistory->phone = $mSms->phone;// lấy phone đưa xuống hàm doSaveEmployeeProblems
//        $aNotSendSms = [GasScheduleNotify::ERROR_HGD_ALERT_5];
        $aNotSendSms = [GasScheduleNotify::ERROR_HGD_ALERT_5, GasScheduleNotify::TYPE_NOT_SEND_SMS,
            // Jul2219 DungNT close sms phạt CV + GDKV
//            GasScheduleNotify::ERROR_HGD_ALERT_10, GasScheduleNotify::ERROR_HGD_ALERT_15, // CV
//            GasScheduleNotify::ERROR_HGD_ALERT_COMPLETE_18, GasScheduleNotify::ERROR_HGD_ALERT_COMPLETE_28, GasScheduleNotify::ERROR_HGD_ALERT_COMPLETE_38,    
        ];
        if(!in_array($this->type, $aNotSendSms)){
            $mSms->fixInsertRecord();
        }else{
            $mSms->network      = 0;
        }
        return $mSms;
    }
    
    /** @Author: DungNT Dec 07, 2018
     *  @Todo: ko gửi sms ALong, chuyển sang cho SĐT Ngọc
     **/
    public function overridePhoneSms($toUserId, $mTransactionHistory, &$mSms) {
        if($toUserId == GasLeave::UID_DIRECTOR){
//            $mSms->phone = '384331552';
            $mSms->phone = '977632290';// NgocPT
        }
    }
    
    /** @Author: DungNT Sep 11, 2018
     *  @Todo: doing save EmployeeProblems
     **/
    public function doSaveEmployeeProblems($toUserId, $mSms, $mTransactionHistory) {
        $mEmployeeProblems = new EmployeeProblems();
        $mEmployeeProblems->employee_id         = $toUserId;// id PVKH or GDKV
        $mEmployeeProblems->object_id           = $mTransactionHistory->id;
        if($this->type == GasScheduleNotify::ERROR_HGD_ALERT_5){
            $mEmployeeProblems->object_type         = EmployeeProblems::PROBLEM_HGD_5_MINUTES;
        }elseif($this->type == GasScheduleNotify::ERROR_HGD_ALERT_10){
            $mEmployeeProblems->object_type     = EmployeeProblems::PROBLEM_HGD_10_MINUTES;
        }elseif($this->type == GasScheduleNotify::ERROR_HGD_ALERT_15){
            $mEmployeeProblems->object_type     = EmployeeProblems::PROBLEM_HGD_15_MINUTES;
        }
        $mEmployeeProblems->agent_id            = $mTransactionHistory->agent_id;
        $mEmployeeProblems->province_id         = $mTransactionHistory->province_id;
        $mEmployeeProblems->network             = $mSms->network;
        $mEmployeeProblems->phone               = $mTransactionHistory->phone;
        $mEmployeeProblems->role_id             = 0;// role of $mEmployeeProblems->employee_id
        $mEmployeeProblems->message             = $mSms->title;
        $mEmployeeProblems->money               = GasScheduleSmsHistory::getPrice($mEmployeeProblems->network) + $mEmployeeProblems->getPrice();
        $this->overrideEmployeeProblems($mEmployeeProblems);
        $mEmployeeProblems->insertToEmployeeProblems();
    }
    
    /** @Author: DungNT Oct 02, 2018
     *  @Todo: override some info model EmployeeProblems
     * do dùng chung hàm với bên tính lỗi hoàn thành của đơn hộ GĐ nên sẽ cần ghi đè ở chỗ này 
     * @note: sử dụng chung hàm saveErrorsHgdSms ở model GasScheduleNotify va TransactionHistory
     **/
    public function overrideEmployeeProblems(&$mEmployeeProblems) {
        if(empty($this->mEmployeeProblems)){
            return ;
        }
        $mOverrideProblems = $this->mEmployeeProblems;
        $mEmployeeProblems->object_type = $mOverrideProblems->object_type;
        $mEmployeeProblems->money       = $mOverrideProblems->money + GasScheduleSmsHistory::getPrice($mEmployeeProblems->network);
    }
    
    /** @Author: DungNT Sep 14, 2018
     *  @Todo: don hang app lan 2: after 5 minutes auto move order to random CallCenter user
     **/
    public function moveToRandomCallCenter($mTransactionHistory) {
        if($mTransactionHistory->source != Sell::SOURCE_APP || $mTransactionHistory->employee_accounting_id != GasConst::UID_ADMIN){
            return ;
        }
        $mSell = $mTransactionHistory->rSell;
        if(!empty($mSell->call_center_id)){
            return ;
        }
        $mSell->call_center_id   = $mTransactionHistory->getRandomUserHgdOnline();
        $mSell->update(['call_center_id']);
        $this->makeSocketNotify5Minutes($mTransactionHistory, $mSell);
    }
    
    /** @Author: DungNT Sep 14, 2018 
     * @Todo: sau khi gắn random user Callcenter cho đơn hàng thì Put socket notify xuống NV tất cả CallCenter khi có đơn 5 phút chưa XN
     */
    public function makeSocketNotify5Minutes($mTransactionHistory, $mSell) {
        $mCallTemp      = new CallTemp();
        $mUser          = Users::model()->findByPk($mSell->call_center_id);
        $employeeName   = $mUser ? $mUser->first_name : '';
        
        $aUserId    = $mCallTemp->getArrayUserIdOfExt(Call::QUEUE_HGD);
        $message    = "[$employeeName] chuyển App 5 phút chưa xác nhận: {$mTransactionHistory->code_no_sell} - Đại lý {$mTransactionHistory->getAgent()}: ". date('H:i:s');
        $json = $needMore = [];
        // với notify 5 phut sẽ gửi luôn và set need_sync == 0
        $aModelSocket = []; // xử lý gửi 1 lần 1 list sự kiện
        foreach($aUserId as $toUserId){
            $aModelSocket[] = GasSocketNotify::addMessage(0, GasConst::UID_ADMIN, $toUserId, GasSocketNotify::CODE_ALERT_CALL_CENTER, $message, $json, $needMore);
        }
        $mSync = new SyncData(SyncData::SERVER_IO_SOCKET, 'socket/notifyAdd');
        $mSync->notifyAdd($aModelSocket);
    }
    
    
    /** for build about 5000 row in one time
     * @Author: DungNT Oct 07, 2018
     * @Todo: insert multi row to table notify
     * @Param: $aModelToken array Model Token
     * @Param: $type can be GasScheduleNotify::GAS24H_ANNOUNCE
     */
    public function addMultiRow($aModelToken, $platform, $type) {
        if(empty($this->time_send) || $this->time_send == '0000-00-00 00:00:00'){
            $this->time_send = date('Y-m-d H:i:s');
        }
        if(!is_array($aModelToken)){
            return ;
        }
        $created_date = $this->getCreatedDateDb();
        $aRowInsert = [];
        foreach($aModelToken as $mUserToken) {
            $aRowInsert[] = "(
                '$mUserToken->user_id',
                '$type',
                '{$this->obj_id}',
                '{$this->time_send}',
                '{$this->title}',
                '$platform',
                '{$mUserToken->apns_device_token}',
                '{$mUserToken->gcm_device_token}',
                '{$created_date}'
            )";
        }
        if(count($aRowInsert)>0){
            $sql = $this->buildOneQuery($aRowInsert);
            Yii::app()->db->createCommand($sql)->execute();
        }
    }
    
    public function getCreatedDateDb() {
        return date('Y-m-d H:i:s');
    }
    
    /** @Author: DungNT May 08, 2019
     *  @Todo: xóa các notify type hộ GĐ 
     *  @param: $keepNotifyComplete giữ lại notfiy alert hoàn thành đơn, chỉ xóa notfify xác nhận
     **/
    public function deleteTypeAlertHgd($transaction_history_id, $keepNotifyComplete) {
        $atypeDelete    = $this->getTypeErrorsHgd();
        $atypeDelete[]  = GasScheduleNotify::TRANSACTION_CHANGE_EMPLOYEE;
        $atypeDelete[]  = GasScheduleNotify::CUSTOMER_HGD_ORDER;// AnhDung Now0118
        if(!$keepNotifyComplete){
            $atypeDelete[]  = GasScheduleNotify::ERROR_HGD_ALERT_COMPLETE_18;
            $atypeDelete[]  = GasScheduleNotify::ERROR_HGD_ALERT_COMPLETE_28;
            $atypeDelete[]  = GasScheduleNotify::ERROR_HGD_ALERT_COMPLETE_38;
        }
        GasScheduleNotify::DeleteAllByType($atypeDelete, $transaction_history_id);
    }
    
    /** @Author: DungNT Aug 16, 2019
     *  @Todo: count notify by obj_id and Type
     * @param: $obj_id obj id
     * @param: $aType array type
     **/
    public function countByObjIdAndType($obj_id, $aType) {
        if(empty($obj_id)){
            return 0;
        }
        $criteria = new CDbCriteria();
        $criteria->compare('t.obj_id', $obj_id);
        $criteria->addInCondition('t.type', $aType);
        return GasScheduleNotify::model()->count($criteria);
    }
}