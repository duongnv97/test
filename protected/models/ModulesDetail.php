<?php

/**
 * This is the model class for table "{{_modules_detail}}".
 *
 * The followings are the available columns in table '{{_modules_detail}}':
 * @property integer $id
 * @property integer $modules_id
 * @property integer $type
 * @property integer $materials_type_id
 * @property integer $materials_id 
 * @property double $qty
 */
class ModulesDetail extends BaseSpj
{
    public $autocomplete_material;
    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return ModulesDetail the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{_modules_detail}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        return [
            array('modules_id, type, materials_type_id, materials_id, qty', 'required', 'on' => 'create, update'),
            array('id, modules_id, type, materials_type_id, materials_id, qty', 'safe'),
            array('autocomplete_material', 'safe'),
        ];
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        return [
            'rMaterials' => array(
                self::BELONGS_TO, 'GasMaterials', 'materials_id',
            ),
            'rMaterialsType' => array(
                self::BELONGS_TO, 'GasMaterialsType', 'materials_type_id',
            ),
        ];
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'modules_id' => 'Modules',
            'type' => 'Loại',
            'materials_type_id' => 'Loại vật tư',
            'materials_id' => 'ID vật tư',
            'qty' => 'Số lượng',
        ];
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        $criteria=new CDbCriteria;

	$criteria->compare('t.id',$this->id);
	$criteria->compare('t.modules_id',$this->modules_id);
	$criteria->compare('t.type',$this->type);
	$criteria->compare('t.materials_type_id',$this->materials_type_id);
	$criteria->compare('t.materials_id',$this->materials_id);
	$criteria->compare('t.qty',$this->qty);
        $criteria->order = 't.id DESC';
        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
            'pagination'=>array(
                'pageSize'=> Yii::app()->user->getState('pageSize',Yii::app()->params['defaultPageSize']),
            ),
        ));
    }
    
    /** @Author: DuongNV 30/08/18
     *  @Todo: get field name in models
     *  @Param: $field_name field name in models
     **/
    public function getField($field_name) {
        switch ($field_name){
            case 'modules_id':
                $mModules = Modules::model()->findByPk($this->modules_id);
                return empty($mModules) ? '' : $mModules->name;
            case 'type':
                $aType = Modules::model()->getArrayTypeNameModule();
                return $aType[$this->type];
            case 'materials_type_id':
                $mMaterialsType = GasMaterialsType::model()->findByPk($this->materials_type_id);
                return empty($mMaterialsType) ? '' : $mMaterialsType->name;
            case 'materials_id':
                $mMaterials = GasMaterials::model()->findByPk($this->materials_id);
                return empty($mMaterials) ? '' : $mMaterials->name;
            default: return $this->{$field_name};
        }
    }
    
}