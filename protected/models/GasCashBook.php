<?php

/**
 * This is the model class for table "{{_gas_cash_book}}".
 *
 * The followings are the available columns in table '{{_gas_cash_book}}':
 * @property string $id
 * @property string $cash_book_no
 * @property string $agent_id 
 * @property string $release_date
 * @property string $note
 * @property string $created_date
 * @property string $user_id_create
 */
class GasCashBook extends BaseSpj
{
    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return GasCashBook the static model class
     */
    public $aModelCashBookDetail, $autocomplete_name, $autocomplete_name1, $autocomplete_name2;
    public $MAX_ID;
    public static $days_allow_update=1; // không dùng config ở đây nữa, cập nhật cho từng đại lý - số ngày cho phép đạil lý cập nhật sổ quỹ

    public $count_row, $employee_id, $statistic_month, $statistic_year, $date_from, $date_to, $province_id_agent  ;

    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
            return '{{_gas_cash_book}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        return array(
            array('release_date', 'required'),
            array('cash_book_no', 'length', 'max'=>20),			
            array('note', 'length', 'max'=>500),
            array('id, cash_book_no, agent_id, release_date, note, created_date,last_edit_by', 'safe'),
            array('date_from, date_to, employee_id, statistic_month,statistic_year,value_before_update, province_id_agent', 'safe'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        return array(
            'agent' => array(self::BELONGS_TO, 'Users', 'agent_id'),
            'rAgent' => array(self::BELONGS_TO, 'Users', 'agent_id'),
            'rEmployee' => array(self::BELONGS_TO, 'Users', 'employee_id'),
            'CashBookDetail' => array(self::HAS_MANY, 'GasCashBookDetail', 'cash_book_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'cash_book_no' => 'Mã Số',
            'agent_id' => 'Đại Lý',
            'release_date' => 'Ngày',
            'note' => 'Ghi Chú',
            'created_date' => 'Ngày Tạo',			
            'last_edit_by' => 'Người Chỉnh Sửa Cuối Cùng',		
            'statistic_month' => 'Chọn Tháng',
            'statistic_year' => 'Chọn Năm',
            'employee_id' => 'Nhân Viên',
            'date_from' => 'Từ ngày',
            'date_to' => 'Đên',
            'province_id_agent' => 'Tỉnh'
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        $criteria=new CDbCriteria;
        $criteria->compare('t.id',$this->id,true);
        $criteria->compare('t.cash_book_no',$this->cash_book_no,true);
        $criteria->compare('t.note',$this->note,true);

        if(!empty($this->created_date)){
            $this->created_date = MyFormat::dateDmyToYmdForAllIndexSearch($this->created_date);
            $criteria->compare('t.created_date',$this->created_date,true);
        }   
        if(!empty($this->release_date)){
            $this->release_date = MyFormat::dateDmyToYmdForAllIndexSearch($this->release_date);
            $criteria->compare('t.release_date',$this->release_date,true);
        }

        if(Yii::app()->user->role_id==ROLE_SUB_USER_AGENT){
            $criteria->compare('t.agent_id', MyFormat::getAgentId());
            $criteria->compare('t.release_date',date('Y-m-d'));

        }else{
            if(!empty($this->agent_id) )
                $criteria->compare('t.agent_id', $this->agent_id);
        }

        $sort = new CSort();
        $sort->attributes = array(
            'cash_book_no'=>'cash_book_no',
            'created_date'=>'created_date',
            'release_date'=>'release_date',
            'agent_id'=>'agent_id',
        );    
        $sort->defaultOrder = 't.id DESC';   
//        $_SESSION['data-excel'] = new CActiveDataProvider($this, array(
//                'pagination'=>false,
//                'criteria'=>$criteria,
//            'sort' => $sort,
//        ));
        return new CActiveDataProvider($this, array(
                'criteria'=>$criteria,
                'pagination'=>array(
                    'pageSize'=> Yii::app()->user->getState('pageSize',Yii::app()->params['defaultPageSize']),
                ),
            'sort' => $sort,
        ));
    }

    public function beforeSave() {
        if(strpos($this->release_date, '/')){
            $this->release_date =  MyFormat::dateConverDmyToYmd($this->release_date);
            MyFormat::isValidDate($this->release_date);
        }
        if($this->isNewRecord){
            $length = MyFormat::MAX_LENGTH_11;
            $length = MyFormat::GetLengthCode($length);

            $this->agent_id = MyFormat::getAgentId();
            $this->uid_login = Yii::app()->user->id;
            $this->cash_book_no = MyFunctionCustom::getNextId('GasCashBook', date('y').Users::getCodeAccount(Yii::app()->user->parent_id).'', $length,'cash_book_no');
        }else{
            $this->last_edit_by = Yii::app()->user->id; 
            $this->last_update_time = date('Y-m-d H:i:s');
        }

        $this->note = InputHelper::removeScriptTag($this->note);
        return parent::beforeSave();
    }

    /**
    * @Author: ANH DUNG 12-17-2013
    * @Todo: delete all store card detail with store card id if exitst, and add new all store card detail
    * @Param: $model model store card 
     */
    public static function saveCashBookDetail($model){
        GasCashBookDetail::deleteByCashBookId($model->id);
        $aRowInsert=array();	
        if( is_array($model->aModelCashBookDetail)){
            foreach($model->aModelCashBookDetail as $item){
                if( trim($item->master_lookup_id)!='' &&
                     ( $item->amount && $item->amount >0 )
                        ) {
                    $modelDetail = new GasCashBookDetail();
                    $cash_book_id = $model->id;
                    $agent_id = $model->agent_id;
                    $release_date = $model->release_date;
                    $master_lookup_id = $item->master_lookup_id;
                    $modelDetail->master_lookup_id = $master_lookup_id;
                    $amount = $item->amount;
                    $qty = $item->qty;
                    $description = InputHelper::removeScriptTag($item->description);
                    $description = MyFormat::removeBadCharacters($description);
                    $name_employee = $item->name_employee;
                    $name_employee = MyFormat::removeBadCharacters($name_employee);
                    $customer_id = $item->customer_id;
                    $modelDetail->customer_id = $customer_id;
                    $type_customer = 0;
                    $sale_id = "";
                    if($modelDetail->customer){ // loại KH 1: Bình Bò, 2: Mối, dùng cho thống kê doanh thu
                        //'is_maintain',// vì cột is_maintain không dùng trong loại KH của thẻ kho nên ta sẽ dùng cho 1: KH bình bò, 2. KH mối
                        $type_customer = $modelDetail->customer->is_maintain;
                        $sale_id = $modelDetail->customer->sale_id;                            
                        $modelDetail->customer_parent_id = MyFormat::getParentIdForCustomer($modelDetail->customer);
                    }
                    $type = 0;
                    if($modelDetail->master_lookup){ //type  1: thu, 2: chi
                        $type = $modelDetail->master_lookup->type;
                    }

                    $modelDetail->release_date = $release_date;
                    $modelDetail->type = $type;
                    $modelDetail->amount = $amount;
                    $modelDetail->type_customer = $type_customer;
                    GasCashBookDetail::addCollectionCustomerYear($modelDetail);

                    $aRowInsert[]="('$cash_book_id',
                    '$agent_id', 
                    '$release_date', 
                    '$master_lookup_id', 
                    '$type', 
                    '$amount',
                    '$qty',
                    '$customer_id',
                    '$modelDetail->customer_parent_id',
                    '$sale_id',
                    '$type_customer',
                    '$name_employee',
                    '$description'
                    )";
                }
            }

            $tableName = GasCashBookDetail::model()->tableName();
            $sql = "insert into $tableName (cash_book_id,
                        agent_id,
                        release_date,
                        master_lookup_id,
                        type,
                        amount,
                        qty,
                        customer_id,
                        customer_parent_id,
                        sale_id,
                        type_customer,
                        name_employee,
                        description
                        ) values ".implode(',', $aRowInsert);
            if(count($aRowInsert)>0)
                Yii::app()->db->createCommand($sql)->execute();
        }            
    }   

    public function beforeValidate() {
        $this->aModelCashBookDetail = array();
        if(isset($_POST['GasCashBookDetail']['qty']) && is_array($_POST['GasCashBookDetail']['qty']) && count($_POST['GasCashBookDetail']['qty'])< 200 ){
            foreach($_POST['GasCashBookDetail']['qty'] as $key=>$item){
                $modelDetail = new GasCashBookDetail();
                $modelDetail->master_lookup_id = (int)$_POST['GasCashBookDetail']['master_lookup_id'][$key];
                $modelDetail->amount = $_POST['GasCashBookDetail']['amount'][$key];
                $modelDetail->qty = $item;
                $modelDetail->description = ActiveRecord::clearHtml($_POST['GasCashBookDetail']['description'][$key]);
                $modelDetail->name_employee = ActiveRecord::clearHtml($_POST['GasCashBookDetail']['name_employee'][$key]);
                $modelDetail->customer_id = $_POST['GasCashBookDetail']['customer_id'][$key];
                $this->aModelCashBookDetail[] = $modelDetail;
            }
        }            
        return parent::beforeValidate();
    }

    public function beforeDelete() {
        GasCashBookDetail::deleteByCashBookId($this->id);
        return parent::beforeDelete();
    }        
        
        
 
    /**
     * @Author: ANH DUNG 12-29-2013
     * @Todo: get model by $release_date
     * @Return: model or null
     */
    public static function getByReleaseDate($release_date)
    {
        $criteria=new CDbCriteria; 
        $criteria->compare("t.release_date", $release_date);   
        $criteria->compare("t.agent_id", MyFormat::getAgentId());   
        return self::model()->find($criteria);
    }         
    
    /**
     * @Author: ANH DUNG 01-17-2014
     * @Todo: get số ngày dc phép cập nhật sổ quỹ của đại lý, được thiết lập cho mỗi đại lý: payment_day
     * @Return: number 
     */
    public static function getAgentDaysAllowUpdate()
    {
        $cRole = MyFormat::getCurrentRoleId();
        if($cRole==ROLE_SUB_USER_AGENT){
            $mUser = Users::model()->findByPk(MyFormat::getAgentId());
            return $mUser->payment_day ;
        }elseif($cRole==ROLE_ADMIN){
            return 100;
        }elseif($cRole==ROLE_CALL_CENTER){
            return Yii::app()->params['DaysUpdateFixAll'];
        }
        return Yii::app()->params['DaysUpdateFixAll'];
    }         
    
        
    public function getAgent($field_name='first_name') {
        $mUser = $this->rAgent;
        if($mUser){
            return $mUser->$field_name;
//            return "<b>".$mUser->code_bussiness."-".$mUser->first_name."</b><br>".$mUser->address."<br><b>Phone: </b>".$mUser->phone;
        }
        return '';
    }
    
    /**
     * @Author: ANH DUNG May 21, 2017
     * @Todo: chặn không cho user create trên web
     */
    public function canCreate() {
        return true;// Aug3017 mở cho tạo hết
        $province_id = Yii::app()->user->province_id;
        $aAllow = [GasProvince::TINH_GIALAI, GasProvince::TINH_KONTUM];
        if(in_array($province_id, $aAllow)){
            return true;
        }
        return false;
    }
     
    /** @Author: KHANH TOAN 2018
     *  @Todo:
     *  @Param:
     **/
    public function getReportExcelForProvinceID($model) {
        $mCashbookDetail    = new GasCashBookDetail();
        $mInventoryCustomer = new InventoryCustomer();
        $mAppCache          = new AppCache();
        $mCashbookDetail->date_from_ymd     = MyFormat::dateDmyToYmdForAllIndexSearch($model->date_from);;
        $mCashbookDetail->date_to_ymd       = MyFormat::dateDmyToYmdForAllIndexSearch($model->date_to);;
        $mCashbookDetail->release_date      = $mCashbookDetail->date_from_ymd;
        $aAgent = [];
        if(!empty($this->province_id_agent) && is_array($this->province_id_agent)){
            $aAgent = $mInventoryCustomer->getAgentOfProvince($_GET['GasCashBook']['province_id_agent']);
        }
        $aResult = [];
        $aAgentName = $mAppCache->getAgentListdata();
        foreach ($aAgent as  $agent_id){
                $mCashbookDetail->agent_id          = $agent_id;
                $model->agent_id = $agent_id;
                $openingBalance         = $mCashbookDetail->getOpeningBalanceOfAgent();
                $aModelCashBookDetail   = $mCashbookDetail->getDaily();
                $textAgent  = $aAgentName[$agent_id]. ' - ';
                $title      = "$textAgent Sổ Quỹ $model->date_from đến $model->date_to- Tồn Đầu:  ".ActiveRecord::formatCurrency($openingBalance);
                $aData['title']                 = $title;
                $aData['openingBalance']        = $openingBalance;
                $aData['aModelCashBookDetail']  = $aModelCashBookDetail;
                $aResult[$agent_id] = $aData;
                $aResult['Agent'][$agent_id] = $agent_id;
                
        }
        return $aResult;
    }

}