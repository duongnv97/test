<?php
 
/**
 * This is the model class for table "{{_forecast}}".
 *
 * The followings are the available columns in table '{{_forecast}}':
 * @property integer $id
 * @property integer $customer_id
 * @property integer $days_per_one
 * @property string $date_forecast
 * @property integer $sale_id
 * @property integer $type_customer
 * @property integer $agent_id
 * @property integer $province_id
 * @property string $last_update
 */
class Forecast extends BaseSpj
{
    const PERCENT_NOTIFY            = 90; // % show button on app
    const MIN_HOUR                  = 0;
    const MAX_HOUR                  = 23;
    const MAX_DAYS                  = 4;
    
    const TYPE_BANK                 = 2;
    const TYPE_CASH                 = 1;

    const TYPE_PAYMENT_FIRST_ORDER  = 1;
    const TYPE_PAYMENT_SECOND_ORDER = 2;
    const TYPE_GOLD_TIME            = 3;
    
    const TYPE_IMMEDIATE            = 0;
    const TYPE_TIMER                = 1;
    

    public static $ArrayPercentNotify = [
        5,
        15,
        50,
        70
    ];
    public static $ArrayKgPerOne = [
        GasMaterialsType::MATERIAL_BINH_12KG => 12
    ];
//    arr hour send before
    public static $ArrayHourSend = [
        1,
        3
    ];

    const MIN_PERCENT       = 5;
    const LIMIT_APPORDER    = 5;
    const LIMIT_LINE        = 30;
    const MIN_SEND_NOTIFY_ORDER = 15;

    public $autocomplete_name, $autocomplete_name_customer, $autocomplete_name_agent;
    
    /** @Author: ANH DUNG Oct 29, 2018
     *  @Todo: get array phone allow Dự báo lượng gas 
     **/
    public function getArrayPhoneTest() {
        return [
            '0384331552',
            '0982843001',
            '0389945321',
            '0988425142',
            '0988180386',
            '0909698924',
            
            '0999888771',
            '0999888772',
            '0999888773',
            '0999888774',
            '0999888775',
            '0999888776',
            '0999888777',
            '0999888733',
            
            '0855909744',
            '0937814259',
            '0703409940',
            '0936262620',
            '0933001898',
            '0903998569',
            '0939574679',
            '0948456546',
            '0909199505',
            '0339265383',
            '0968560010',
            '0906850459',
            '0916448075',
            '0909875420',
            '0911743239',
            '0945686979',
            '0933307792',
            '0963630207',
            '0933037660',
            '0962583254',
            '0898122739',
            '0939669557',
            '0365420905',
            '0909628052',
            '0908400572',
            '0931961200',
            '0393825693',
            '0789908771',
            '0982004769',
            '0988538360',
            '0913147255',
            '0919168445',
            '0961913128',
            '0909970026',
            '0336920729',
            '0976994876',
            '0915099179',
            '0903976357',
            '0938187986',
            '0355444051',
            '0902450290',
            '0985163698',
            '0338061606',
            '0349399232',
            '0984934483',
            '0979905539',
            '0365420905',
            '0962583254',
            '0902836067',
            '0909141494',
            
            '0933026430',
            '0901323290',
            '0937906394',
            '0964739751',
            '0979932264',
            '0978823822',
            '0832801179',
            '0946616271',
            '0815094414',
            '0966002817',
            '0938442068',
            
            '0919463028',
            '0969983833',
            '0941451578',
            '0356902736',
            '0983009279',
            '0384830983',
            '0349399232',
            '0388475303',
            '0386772679',
            '0352534651',
            '0354976820',
            '0362468117',
            '0914462024',
            '0799414879',
            '0775536834',
            '0962215205',
            '0988079359',
            '0905266147',
            '0975038279',
            '0356178890',
            '0979261738',
            '0963922702',
            '0376141899',
            '0394883729',
            '0971156799',
            '0983696552',
            '0982250429',
            '0963830500',
            '0923520061',
            
            '0792419000','0583534403','0939218105','0569871588','0933307792','0966002817','0934143199',
            '0937911904','0903057446','0937740811','0908877060','0938787624','0901471698','0908878378',
            '0904364812','0933250678',
            
            '0916820402','0917123127','0989752295','0914940501','0366387837','0902494249','0834924447',
            '0946497255','0976015706','0355295169','0937584883','0978139539','0963773020','0916577184',
            '0908754600','0909655807','0988209034','0979675589','0933869498','0985122414','0937865644',
            '0967344223','0908098817','0903887060','0918610640','0779919911','0332758421','0888054592',
            '0989728700','0388150781','0988454825','0979422757','0963090913','0334994715','0985121440',
            '0382196157','0988447954', '0377279285',
        ];
    }
    
    /** @Author: ANH DUNG Oct 29, 2018
     *  @Todo: check user can use function forcast
     *  @param: $mUser is user App Gas24h 
     **/
    public function canUseForecast($mUser) {
        $aProvinceRun = [GasProvince::TP_HCM];
        return in_array($mUser->province_id, $aProvinceRun);
        return in_array($mUser->username, $this->getArrayPhoneTest());
    }
    
    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return Forecast the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{_forecast}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        return [
            ['days_per_one, date_forecast, last_update', 'required','on'=>'create,update'],
            ['id, customer_id, days_per_one, date_forecast, sale_id, type_customer, agent_id, province_id, last_update', 'safe'],
        ];
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        return [
            'rCustomer' => array(self::BELONGS_TO, 'Users', 'customer_id'),
            'rSale'     => array(self::BELONGS_TO, 'Users', 'sale_id'),
            'rAgent'    => array(self::BELONGS_TO, 'Users', 'agent_id'),
        ];
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return [
            'id'                    => 'ID',
            'customer_id'           => 'Khách hàng',
            'days_per_one'          => 'Tần xuất gas ',
            'date_forecast'         => 'Dự báo hết gas',
            'sale_id'               => 'Nhân viên sale',
            'type_customer'         => 'Loại khách hàng',
            'agent_id'              => 'Đại lý',
            'province_id'           => 'Tỉnh',
            'last_update'           => 'Hoá đơn gần nhất',
        ];
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        $criteria=new CDbCriteria;

	$criteria->compare('t.customer_id',$this->customer_id);
	$criteria->compare('t.sale_id',$this->sale_id);
	$criteria->compare('t.type_customer',$this->type_customer);
	$criteria->compare('t.agent_id',$this->agent_id);
	$criteria->compare('t.province_id',$this->province_id);
        $criteria->order = 't.date_forecast ASC';
        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
            'pagination'=>array(
                'pageSize'=> Yii::app()->user->getState('pageSize',Yii::app()->params['defaultPageSize']),
            ),
        ));
    }

    /** @Author: NamNH 09/2018
     *  @Todo: generate forecast of customer gas_app_order
     *  @Param:$aUser array of user id
     *  $mForecast->handleGenerateForecast([26807,26810,26813,26820,26821,26878,26881,367067,367071,658920]);
    **/
    public function handleGenerateForecast($aUser = []){
//        Logger::WriteLog("Bo Moi => chức kiêm tra hẹn huỷ hẹn giờ");
        foreach ($aUser as $key => $user_id) {
            $mCustomer          = Users::model()->findByPk($user_id);
            if(!in_array($mCustomer->is_maintain, CmsFormatter::$aTypeIdBoMoi)){
                continue;
            }
            $aValueGas  = CmsFormatter::$MATERIAL_VALUE_KG;
            $countDate = $countGas = $daysPerOne = 0;$aJson = [];
            $iLine              = $this->getLineCustomer($mCustomer);
            $removeFirst        = true; //  bỏ qua số bình của đơn hàng đầu tiên vì từ đơn hàng thứ 2 mới tính toán được số bình.
            $criteria           = new CDbCriteria;
            $criteria->compare('t.customer_id', $user_id);
            $criteria->compare('t.status', GasAppOrder::STATUS_COMPPLETE);
            $criteria->limit =  self::LIMIT_APPORDER;
            $criteria->order = 't.id DESC';
            $aGasAppOrder = GasAppOrder::model()->findAll($criteria);
            if(count($aGasAppOrder) <= 0){ // không tạo forecast nếu khách hàng chưa đặt hàng lần nào
                continue;
            }
            if(count($aGasAppOrder) == 1){ 
                $daysPerOne = $this->getAverageForecastSystem();
                foreach ($aGasAppOrder as $key => $mGasAppOrder) {
                    $endTime = $mGasAppOrder->date_delivery;
                    $this->setJsonForecast($mGasAppOrder,$aJson);
                }
            }else{
                $gas_remain     = 0.0;
                $gasValue       = $countGas = 0;$startTime = $endTime = '';
                $countSell      = 0;
                foreach ($aGasAppOrder as $key => $mGasAppOrder) {
                    $countSell++;
                    $this->setJsonForecast($mGasAppOrder,$aJson);
                    if($countSell != count($aGasAppOrder)){
                        $gas_remain         += (int)$mGasAppOrder->getRemainKg();
                    }
                    if(empty($startTime) || MyFormat::compareTwoDate($startTime,$mGasAppOrder->date_delivery)){
                        $startTime      = $mGasAppOrder->date_delivery;
                    }
                    if(empty($endTime) || !MyFormat::compareTwoDate($endTime,$mGasAppOrder->date_delivery)){
                        $endTime        = $mGasAppOrder->date_delivery;
                    }
                    if($removeFirst){
                        $removeFirst    = false;
                        continue;
                    }
                    $mGasAppOrder->mapJsonFieldOneDecode('JSON_FIELD', 'json', 'baseArrayJsonDecode');
                    if(isset($mGasAppOrder->info_gas) && is_array($mGasAppOrder->info_gas)){
                        foreach ($mGasAppOrder->info_gas as $aInfoGas) {
                            if(in_array($aInfoGas['materials_type_id'], CmsFormatter::$MATERIAL_TYPE_BINHBO_OUTPUT)){
                                $countCurent =  isset($aInfoGas['qty_real']) ? $aInfoGas['qty_real'] : 0;
                                $countGas   += $countCurent;
                                $gasValue   += isset($aValueGas[$aInfoGas['materials_type_id']]) ? $countCurent * $aValueGas[$aInfoGas['materials_type_id']] : 0;
                            }
                        }
                    }

                }
                if($countGas <= 0){
                    continue;
                }
//                set line temporary
                $iLine              = $countGas/count($aGasAppOrder);
                $countDate          = MyFormat::getNumberOfDayBetweenTwoDate($startTime,$endTime);
                $kgPerOne           = $gasValue/$countGas; // lấy trung bình kg của tổng số gas => lượng gas 1 bình gas khách hàng xử dụng
                $gasRemain          = ($gas_remain/$kgPerOne);
//                Trừ số lượng gas remain
                $daysPerOne         = round($countDate/($countGas-$gasRemain),1);//Tổng ngày / (Tổng số bình gas trên hoá đơn - Tổng số bình gas gas dư)
//                $daysPerOne   = $gasValue/($gasValue - $gas_remain) * $countDate / $countGas; // tong gas/gas da dung  ngay dung thuc te / so luong gas
            }
            $mForecast          = $this->getForecastByCustomerId($user_id);
            $addDays            = (int)($daysPerOne*$iLine) > 1 ? (int)($daysPerOne*$iLine) : 1;
            $lastUpdate         = !empty($endTime) ? $endTime : date('Y-m-d');
            $dateForecast       = MyFormat::modifyDays($lastUpdate, $addDays);
            if(empty($mForecast)){
                $mForecast = new Forecast();
                $mForecast->setCustomerInfo($mCustomer);
                $mForecast->days_per_one    = $daysPerOne;
                $mForecast->date_forecast   = $dateForecast;
                $mForecast->last_update     = $lastUpdate;
                $mForecast->json            = json_encode($aJson);
                $mForecast->save();
            }else{
                $mForecast->days_per_one    = $daysPerOne;
                $mForecast->date_forecast   = $dateForecast;
                $mForecast->last_update     = $lastUpdate;
                $mForecast->json            = json_encode($aJson);
                $mForecast->update();
            }
            $this->createDetailByForecast($mForecast);
        }
    }

    /** @Author: NamNH 09/2018
     *  @Todo: get Line of customer
     *  @Param: $customer_id int
     **/
    public function getLineCustomer($mCustomer){
       return !empty($mCustomer->application_id) ? $mCustomer->application_id : 1;
    }

    /** @Author: NamNH 09/2018
     *  @Todo: get forecast model by customer id
     *  @Param:$user_id int
     **/
    public function getForecastByCustomerId($user_id){
        $criteria           = new CDbCriteria;
        $criteria->compare('t.customer_id', $user_id);
        return self::model()->find($criteria);
    }

    /** @Author: NamNH 09/2018
     *  @Todo: create detail for notify
     *  @Param:$mForecast
     **/
    public function createDetailByForecast($mForecast){
        $aDate = [];
        $numberDayToForecast    =             MyFormat::getNumberOfDayBetweenTwoDate($mForecast->last_update,$mForecast->date_forecast);
        foreach (self::$ArrayPercentNotify as $percent) {
            $addDays    = (int)$numberDayToForecast / 100 * $percent > 1 ? (int)$numberDayToForecast / 100 * $percent : 1;
            $date       = MyFormat::modifyDays($mForecast->last_update, $addDays);
            if(!isset($aDate[$date])){
                $aDate[$date]   = $date;
            }
        }
        // save detail
        $tableName      = ForecastDetail::model()->tableName();
        $createdDate    = date('Y-m-d H:i:s');
        $aRowInsert     = [];
//        remove > date now
        $date_forecast_search = strtotime($mForecast->last_update);
        $criteria=new CDbCriteria;
        $criteria->addCondition('date_notify_search > '.$date_forecast_search);
        $criteria->compare('customer_id',$mForecast->customer_id);
        ForecastDetail::model()->deleteAll($criteria);
        foreach ($aDate as $date_forecast) {
            $date_forecast_search = strtotime($date_forecast);
            $criteria=new CDbCriteria;
            $criteria->compare('t.date_notify_search',$date_forecast_search);
            $criteria->compare('t.customer_id',$mForecast->customer_id);
            $mForecastDetailOld = ForecastDetail::model()->find($criteria);
            if(!empty($mForecastDetailOld)){
                continue;
            }
            $aRowInsert[] = "(
                        '{$mForecast->customer_id}',
                        '{$date_forecast}',
                        '{$date_forecast_search}',
                        '{$createdDate}',
                        '{$mForecast->type_customer}'
                        )";
        }
        $sql = "insert into $tableName (
                        customer_id,
                        date_notify,
                        date_notify_search,
                        created_date,
                        type_customer
                        ) values " . implode(',', $aRowInsert);
        if (count($aRowInsert)){
            Yii::app()->db->createCommand($sql)->execute();
        }
    }

    /** @Author: NamNH 09/2018
     *  @Todo: get view forecast
     **/
    public function apiViewForecast(){
        $result                     = [];
        $gas_percent                = '';
        $mUser                      = $this->mAppUserLogin;
        $criteria                   = new CDbCriteria;
        $criteria->compare('t.customer_id', $mUser->id);
        $mForecast  = self::model()->find($criteria);
        if(!empty($mForecast)){
            $numberSumDay           = MyFormat::getNumberOfDayBetweenTwoDate($mForecast->last_update,$mForecast->date_forecast);
            $numberDayNow           = MyFormat::getNumberOfDayBetweenTwoDate($mForecast->last_update,date('Y-m-d'));
            $gas_percent            = 100 - (int)($numberDayNow * 100 / $numberSumDay);
        }else{
            $mForecast              = $this->createForecast($mUser);
            if(!empty($mForecast)){
                $numberSumDay           = MyFormat::getNumberOfDayBetweenTwoDate($mForecast->last_update,$mForecast->date_forecast);
                $numberDayNow           = MyFormat::getNumberOfDayBetweenTwoDate($mForecast->last_update,date('Y-m-d'));
                $gas_percent            = 100 - (int)($numberDayNow * 100 / $numberSumDay);
            }
        }

        // Find old order
        $criteria = new CDbCriteria();
        $criteria->addCondition('t.customer_id=' . $this->customer_id);
        $criteria->addCondition('t.status=' . Sell::STATUS_PAID);
        $criteria->compare('t.payment_type', Forecast::TYPE_BANK);
        if(!empty($this->id)){
            $criteria->addCondition('t.id<>' . $this->id);
        }
        $criteria->limit = 1;
        $criteria->order = 't.id DESC';
        $mSellOld                   = Sell::model()->find($criteria);
        $sell = new Sell();
        if (empty($mSellOld)) {
            $discount = $sell->getDiscountFirstBank();
        } else {
            $discount = $sell->getDiscountSecondBank();
        }
        
        $result['can_notify']       = ($this->isForecastToday($mUser->id)) ? 1 : 0;
        $result['last_order']       = !empty($mForecast) ? MyFormat::dateConverYmdToDmy($mForecast->last_update) : '';
        $result['gas_percent']      = $gas_percent > self::MIN_PERCENT ? $gas_percent : self::MIN_PERCENT;
        $result['date_forecast']    = !empty($mForecast) ? MyFormat::dateConverYmdToDmy($mForecast->date_forecast) : '';
        $days                       = !empty($mForecast) ? (int)MyFormat::getNumberOfDayBetweenTwoDate(date('Y-m-d'),$mForecast->date_forecast) : 1;
        $result['days_forecast']    = !empty($mForecast) ? (date('Y-m-d') <  $mForecast->date_forecast ? $days : 1) : 1;
        $result['rush_hour']        = "" . ActiveRecord::formatCurrency($sell->getDiscountGoldTime()) . 'Đ';
        $result['rush_hour_text']   = $sell->getGoldTimeText();
        $result['bank_discount']    = "" . $discount . "%";
        $result['enable_bank']      = $discount > 0 ? 1: 0;
        $result['enable_rush_hour'] = $sell->getDiscountGoldTime() > 0 ? 1: 0;
        $result['can_order']        = $gas_percent <= self::PERCENT_NOTIFY ? 1 : 0;
        $result['min_hour']         = self::MIN_HOUR;
        $result['max_hour']         = self::MAX_HOUR;
        $result['max_days']         = self::MAX_DAYS;
//        open if test change result default by account
//        $this->changeForTest($mUser,$result);
        return $result;
    }
    
    /** @Author: NamNH Nov 04, 2018
     *  @Todo: change result by test
     **/
    public function changeForTest($mUser,&$result){
        if(GasCheck::isServerLive()){
            return;
        }
        if($mUser->username == '0999888777'){
            $result['can_order']        = 1;
            $result['gas_percent']      = 90;
        }
        if($mUser->username == '0999888774'){
            $result['can_order']        = 0;
            $result['gas_percent']      = 10;
        }
    }
    
    /** @Author: NamNH Nov 03, 2018
     *  @Todo: set new if not exist
     **/
    public function createForecast($mUser){
        if(in_array($mUser->is_maintain, CmsFormatter::$aTypeIdBoMoi)){ // bo moi
            $this->handleGenerateForecast([$mUser->id]);
        }else{ // khach hang hgd
            $this->handleGenerateForecastSell([$mUser->id]);
        }
        $criteria                   = new CDbCriteria;
        $criteria->compare('t.customer_id', $mUser->id);
        $mForecast  = self::model()->find($criteria);
        return $mForecast;
    }
    /** @Author: NamNH 09/2018
     *  @Todo: get true/false forecast today
     **/
    public function isForecastToday($user_id){
        $dateNow                    = strtotime(date('Y-m-d'));
        $criteria                   = new CDbCriteria;
        $criteria->compare('t.customer_id', $user_id);
        $criteria->addCondition('t.date_notify_search = "' .$dateNow. '"');
        $mForecastDetail            = ForecastDetail::model()->find($criteria);
        return !empty($mForecastDetail) ? true : false;
    }

    /** @Author: NamNH 09/2018
     *  @Todo: get array of line
     **/
    public static function getArrayLine(){
        $key = range(1, self::LIMIT_LINE,1);
        $valueView = range(1,self::LIMIT_LINE,1);
        array_walk($valueView, function(&$value, $key) { $value .= '-'. $value; } );
        return array_combine($key,$valueView);
    }

    /** @Author: NamNH 09/2018
     *  @Todo: get name of customer
     **/
    public function getCustomer(){
        $mUsers = $this->rCustomer;
        $strCustomer = '';
        if(!empty($mUsers)){
            $strCustomer .= '<b>'.$mUsers->first_name.' - '.$mUsers->phone.'</b><br>';
            $strCustomer .= $mUsers->address;
        }
        return $strCustomer;
    }

    /** @Author: NamNH 09/2018
     *  @Todo: get field name of table
     **/
    public function getFieldName($field_name){
        return !empty($this->$field_name) ? $this->$field_name : '';
    }

    /** @Author: NamNH 09/2018
     *  @Todo: get date forecast
     **/
    public function getDateForcast(){
        return !empty($this->date_forecast) ? MyFormat::dateConverYmdToDmy($this->date_forecast) : '';
    }

    /** @Author: NamNH 09/2018
     *  @Todo: get name of sale
     **/
    public function getSale(){
        $mUsers = $this->rSale;
        return !empty($mUsers) ? $mUsers->first_name : '';
    }
    /** @Author: NamNH 09/2018
     *  @Todo: get tye of customer
     **/
    public function getTypeCustomerText() {
        return isset(CmsFormatter::$CUSTOMER_BO_MOI[$this->type_customer]) ?  CmsFormatter::$CUSTOMER_BO_MOI[$this->type_customer] : '';
    }

    /** @Author: NamNH 09/2018
     *  @Todo: get name of agent
     **/
    public function getAgent(){
        $mUsers = $this->rAgent;
        return !empty($mUsers) ? $mUsers->first_name : '';
    }

    /** @Author: NamNH 09/2018
     *  @Todo: get string of days per one
     **/
    public function getDaysPerOne(){
        $strResult      = !empty($this->days_per_one) ? $this->days_per_one .' Ngày' : '';
        return $strResult;
    }

    /** @Author: NamNH 09/2018
     *  @Todo: get last update
     **/
    public function getLastUpdate(){
        return !empty($this->last_update) ? MyFormat::dateConverYmdToDmy($this->last_update) : '';
    }

    /** @Author: NamNH 09/2018
     *  @Todo: get detail for view of forecast
     **/
    public function getDetails(){
        $mForecastDetail        = new ForecastDetail();
        $criteria=new CDbCriteria;

	$criteria->compare('t.customer_id',$this->customer_id);
        $criteria->order = 't.id DESC';

        return new CActiveDataProvider($mForecastDetail, array(
            'criteria'=>$criteria,
            'pagination'=>array(
                'pageSize'=> Yii::app()->user->getState('pageSize',Yii::app()->params['defaultPageSize']),
            ),
        ));
    }

    /** @Author: NamNH Sep 27, 2018
     *  @Todo: generate forecast of customer sell
     *  @Param:$aUser array of user id
    **/
    public function handleGenerateForecastSell($aUser = []){
//        Logger::WriteLog("HGD => chức kiêm tra hẹn huỷ hẹn giờ");
        foreach ($aUser as $key => $user_id) {
            $mCustomer          = Users::model()->findByPk($user_id);
//            if(!$this->canUseForecast($mCustomer)){
            if(0){// AnhDung, open insert all customer for test
                continue;
            }
            $aValueGas  = CmsFormatter::$MATERIAL_VALUE_KG;
            $countDate = $countGas = 0;$aJson = [];
            $iLine              = 1; // Oct2918 AnhDung fix 
            $removeFirst        = true; //  bỏ qua số bình của đơn hàng đầu tiên vì từ đơn hàng thứ 2 mới tính toán được số bình.
            $criteria           = new CDbCriteria;
            $criteria->compare('t.customer_id', $user_id);
            $criteria->compare('t.status', Sell::STATUS_PAID);
            $criteria->limit =  self::LIMIT_APPORDER;
            $criteria->order = 't.id DESC';
            $aGasSell = Sell::model()->findAll($criteria);
            if(count($aGasSell) <= 0){ // không tạo forecast nếu khách hàng chưa đặt hàng lần nào
                continue;
            }
            if(count($aGasSell) == 1){ // > 1 vì phải hoàn thành ít nhất 2 lần mới tính toán được thời gian trung bình
                $daysPerOne = $this->getAverageForecastSystem();
                foreach ($aGasSell as $key => $mSell) {
//                    change date complete to created_date_only
                    $mSell->complete_time = $mSell->created_date_only;
                    $endTime = $mSell->complete_time;
                    $this->setJsonForecastSell($mSell,$aJson);
                }
            }else{
                //             tính toán ngày trung bình lấy gas
//                số gas dư
                $gas_remain = 0.0;
                $gasValue       = $countGas = 0;$startTime = $endTime = '';
                $countSell      = 0;
                foreach ($aGasSell as $key => $mSell) {
                    $countSell++;
//                    change date complete to created_date_only
                    $mSell->complete_time = $mSell->created_date_only;
                    $this->setJsonForecastSell($mSell,$aJson);
//                    test forecast
//                    Logger::WriteLog($mSell->complete_time . ' ---- '.$mSell->created_date_only);
                    if($countSell != count($aGasSell)){
                        $gas_remain += $mSell->gas_remain;
                    }
                    
                    $mSell->complete_time   = MyFormat::dateConverYmdToDmy($mSell->complete_time, 'Y-m-d');
                    if(empty($startTime) || MyFormat::compareTwoDate($startTime,$mSell->complete_time)){
                        $startTime          = $mSell->complete_time;
                    }
                    if(empty($endTime) || !MyFormat::compareTwoDate($endTime,$mSell->complete_time)){
                        $endTime            = $mSell->complete_time;
                    }
                    if($removeFirst){
                        $removeFirst        = false;
                        continue;
                    }
    //                count gas
                    if(!empty($mSell->rDetail)){
                        foreach ($mSell->rDetail as $mSellDetail){
                            if(in_array($mSellDetail->materials_type_id, CmsFormatter::$MATERIAL_TYPE_BINHBO_OUTPUT)){
                                $countGas   += !empty($mSellDetail->qty) ? $mSellDetail->qty : 0;
                                $gasValue   += isset($aValueGas[$mSellDetail->materials_type_id]) ? $mSellDetail->qty * $aValueGas[$mSellDetail->materials_type_id] : 0;
                            }
                        }
                    }
                }
                if($countGas <= 0){
                    continue;
                }
                $countDate          = MyFormat::getNumberOfDayBetweenTwoDate($startTime,$endTime);
                $kgPerOne           = $gasValue/$countGas; // lấy trung bình kg của tổng số gas => lượng gas 1 bình gas khách hàng xử dụng
                $gasRemain          = ($gas_remain/$kgPerOne);
//                Trừ số lượng gas remain
                $daysPerOne         = round($countDate/($countGas-$gasRemain),1);
            }
//                    test forecast
//            Logger::WriteLog('Forecast: '.'$daysPerOne = '.$daysPerOne.'; $gasRemain = '.$gasRemain.';$kgPerOne = '.$kgPerOne.';$gasValue = '. $gasValue.';$countGas = '.$countGas.';$countDate = '.$countDate.';$startTime = '.$startTime.';$endTime = '.$endTime);
            $mForecast          = $this->getForecastByCustomerId($user_id);
            $addDays            = (int)($daysPerOne*$iLine) > 1 ? (int)($daysPerOne*$iLine) : 1;
            $lastUpdate         = !empty($endTime) ? $endTime : date('Y-m-d');
            $dateForecast       = MyFormat::modifyDays($lastUpdate, $addDays);
            if(empty($mForecast)){
                $mForecast = new Forecast();
                $mForecast->setCustomerInfo($mCustomer);
                $mForecast->days_per_one    = $daysPerOne;
                $mForecast->date_forecast   = $dateForecast;
                $mForecast->last_update     = $lastUpdate;
                $mForecast->json            = json_encode($aJson);
                $mForecast->save();
            }else{
                $mForecast->days_per_one    = $daysPerOne;
                $mForecast->date_forecast   = $dateForecast;
                $mForecast->last_update     = $lastUpdate;
                $mForecast->json            = json_encode($aJson);
                $mForecast->update();
            }
            $this->createDetailByForecast($mForecast);
        }
    }
    
    /** @Author: ANH DUNG Oct 29, 2018
     *  @Todo: set some info of customer
     **/
    public function setCustomerInfo($mCustomer) {
        $this->customer_id     = $mCustomer->id;
        $this->sale_id         = $mCustomer->sale_id;
        $this->agent_id        = $mCustomer->area_code_id;
        $this->province_id     = $mCustomer->province_id;
        $this->type_customer   = $mCustomer->is_maintain;
    }

    /** @Author: NamNH Sep 27, 2018
     *  @Todo: cron update date forecast
     * date Y-m-d
     **/
    public function cronUpdate($date){
        $dateNow                    = strtotime($date);
//        generate gas_gas_app_order
        $criteria                   = new CDbCriteria;
        $criteria->select           = 't.customer_id';
	$criteria->compare('t.date_delivery_bigint',$dateNow);
        $criteria->group            = 't.customer_id';
        $aGasAppOrder               = GasAppOrder::model()->findAll($criteria);
        $aIdAppOrder                = CHtml::listData($aGasAppOrder, 'customer_id', 'customer_id');
        $this->handleGenerateForecast($aIdAppOrder);
//        getnerate gas_sell
        $criteriaSell                   = new CDbCriteria;
        $criteriaSell->select           = 't.customer_id';
	$criteriaSell->compare('DATE(t.complete_time)',$date);
        $criteriaSell->group            = 't.customer_id';
        $aGasSell                       = Sell::model()->findAll($criteriaSell);
        $aIdSell                        = CHtml::listData($aGasSell, 'customer_id', 'customer_id');
        $this->handleGenerateForecastSell($aIdSell);
    }

    /** @Author: NamNH Oct 04, 2018
     *  @Todo: get average of system forecast
     **/
    public function getAverageForecastSystem(){
//        $result                     = 0;
//        $criteria                   = new CDbCriteria;
//        $criteria->select           = 'AVG(t.days_per_one) as days_per_one';
//        $mForecast                  = self::model()->find($criteria);
//        if(!empty($mForecast)){
//            $result = $mForecast->days_per_one;
//        }
        $result                     = 30;
        return $result;
    }

    /** @Author: NamNH Oct 10, 2018
     *  @Todo: get notify
     **/
    public function cronNotifyForecast() {
        $dateNow                    = strtotime(date('Y-m-d'));
        $criteria                   = new CDbCriteria;
        $criteria->addCondition('t.date_notify_search = "' .$dateNow. '"');
        $criteria->addNotInCondition('t.type_customer', CmsFormatter::$aTypeIdBoMoi);// Now1618 tạm thời chưa notify cho KH bò mối
        $aForecastDetail            = ForecastDetail::model()->findAll($criteria);
        
        foreach ($aForecastDetail as $key => $mForecastDetail) {
            $mUser      = Users::model()->findByPk($mForecastDetail->customer_id);
            $mForecast  = new Forecast();
            $mForecast->mAppUserLogin   = $mUser;
            $result                     = $mForecast->apiViewForecast();
            $percent                    = isset($result['gas_percent']) ? $result['gas_percent'] : self::MIN_PERCENT;
            $days_forecast                    = isset($result['days_forecast']) ? $result['days_forecast'] : 1;
//            $strTitle       = 'Dự báo lượng gas còn khoảng '.$percent.'%';
            $strTitle       = 'Dự báo lượng gas còn khoảng '.$days_forecast.' ngày';
            if(in_array($mForecastDetail->type_customer, CmsFormatter::$aTypeIdBoMoi)){
                $this->runInsertNotify(GasScheduleNotify::GAS_SERVICE_REMAIN,$mForecastDetail->customer_id, $strTitle,false);
            }else{
                if($percent <= self::MIN_SEND_NOTIFY_ORDER){
                    $this->runInsertNotify(GasScheduleNotify::GAS24H_OUT_OF_GAS,$mForecastDetail->customer_id, $strTitle,false);
                }else{
                    $this->runInsertNotify(GasScheduleNotify::GAS24H_REMAIN,$mForecastDetail->customer_id, $strTitle,false);
                }
            }
            
        }
    }

    /** @Author: NamNH Oct 10, 2018
     *  @Todo: send notify
     **/
    public function runInsertNotify($type,$uid, $title,$sendNow = true) {
        $json_var = [];
        if( !empty($uid) ){
            $mScheduleNotify = GasScheduleNotify::InsertRecord($uid, $type, $this->id, '', $title, $json_var); 
            if($sendNow && !empty($mScheduleNotify)){
                $mScheduleNotify->sendImmediateForSomeUser();
            }
        }
    }
    
    /** @Author: NamNH Oct 10, 2018
     *  @Todo: get notify
     **/
    public function cronNotifyApplyActionTest($mForecastDetail,$type) {
        $mUser      = Users::model()->findByPk($mForecastDetail->customer_id);
        $mForecast  = new Forecast();
        $mForecast->mAppUserLogin   = $mUser;
        $result                     = $mForecast->apiViewForecast();
        $percent                    = isset($result['gas_percent']) ? $result['gas_percent'] : self::MIN_PERCENT;
        $days_forecast                    = isset($result['days_forecast']) ? $result['days_forecast'] : 1;
//            $strTitle       = 'Dự báo lượng gas còn khoảng '.$percent.'%';
        $strTitle       = 'Dự báo lượng gas còn khoảng '.$days_forecast.' ngày';
        $this->runInsertNotify($type,$mForecastDetail->customer_id, $strTitle);
    }
    
    /** @Author: NamNH Oct 29, 2018
     *  @Todo: get day expired
     **/
    public function getDaysExpired($numberOnly = true){
            $minutes = 0;
            if(!MyFormat::compareTwoDate($this->date_forecast, date('Y-m-d H:i:s'))){
                $minutes = MyFormat::getNumberOfDayBetweenTwoDate($this->date_forecast, MyFormat::getDateTimeNow());
            }
            if(!$numberOnly){
                $minutes .= ' Ngày';
            }
            return $minutes;
    }
    
    /** @Author: NamNH Oct 29, 2018
     *  @Todo: can create notify
     **/
    public function canCreateNotify(){
        return true;
        $cRole = MyFormat::getCurrentRoleId();
        if($cRole == ROLE_ADMIN){
            return true;
        }
        return false;
    }
    
    /** @Author: NamNH Nov 06, 2018
     *  @Todo: set json save order use calculator
     **/
    public function setJsonForecast($mGasAppOrder,&$aSave){
        $aSave[]= [
            'id'                    => $mGasAppOrder->id,
            'complete_time'         => $mGasAppOrder->date_delivery,
            'gas_remain'            => (int)$mGasAppOrder->getRemainKg(),
            'code'                  => $mGasAppOrder->code_no,
        ];
    }
    
    /** @Author: NamNH Nov 06, 2018
     *  @Todo: set json save order use calculator
     **/
    public function setJsonForecastSell($mSell,&$aSave){
        $aSave[]= [
            'id'                    => $mSell->id,
            'complete_time'         => $mSell->created_date_only,
            'gas_remain'            => $mSell->gas_remain,
            'code'                  => $mSell->code_no,
        ];
    }
    
    /** @Author: NamNH Nov 06, 2018
     *  @Todo: get view in view
     **/
    public function getViewJsonForecast(){
        $strView = '';
        if(!empty($this->json)){
            $aJson = json_decode($this->json,true);
            if(is_array($aJson)){
                $aJson = array_reverse($aJson);
                $preDate = '';
                foreach ($aJson as $key => $aDetail){
                    $dateCurrent = isset($aDetail['complete_time']) ? $aDetail['complete_time'] : '';
                    if(!empty($preDate)){
                        $number = MyFormat::getNumberOfDayBetweenTwoDate($preDate,$dateCurrent);
                        $strView.='['.$number.'] ';
                    }
                    $preDate = $dateCurrent;
                    $strView.= isset($aDetail['code']) ? 'Mã số: '.$aDetail['code'] : '';
                    $strView.= isset($aDetail['complete_time']) ? ' - Ngày giao: '. MyFormat::dateConverYmdToDmy($aDetail['complete_time']): '';
                    $strView.= isset($aDetail['gas_remain']) ? ' - Gas dư: '.$aDetail['gas_remain'].' Kg' : '';
                    $strView.= '<br>';
                }
            }
        }
        return $strView;
    }
    
    /** @Author: DuongNV AUg2619
     *  @Todo: get link test notify
     **/
    public function getLinkTestNotify($uid) {
        $cRole  = MyFormat::getCurrentRoleId();
        $res    = '';
        if($cRole == ROLE_ADMIN){
            $actionUrl  = Yii::app()->createAbsoluteUrl("admin/ajax/testNotify", array("id"=>$uid));
            $imgUrl     = Yii::app()->theme->baseUrl . '/admin/images/Upload.png';
            $res .= '<a onclick="return confirm(\'Gửi thông báo cho user này?\')" title="Test send notify" href="'.$actionUrl.'" target="_blank">';
            $res .=     '<img src="'.$imgUrl.'" alt="Test send notify">';
            $res .= '</a>';
        }
        return $res;
    }
    
    
}
