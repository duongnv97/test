<?php

/**
 * This is the model class for table "{{_logger}}".
 *
 * The followings are the available columns in table '{{_logger}}':
 * @property integer $id
 * @property string $level
 * @property string $category
 * @property integer $logtime
 * @property string $message
 */
class GasUpholdBackup extends CActiveRecord
{
    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return Logger the static model class
     */
    public static function model($className=__CLASS__)
    {
            return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{_gas_uphold_backup}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        return array(
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        return array(
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'level' => 'Level',
            'category' => 'Category',
            'logtime' => 'Logtime',
            'message' => 'Message',
            'ip_address' => 'ip address',
            'country' => 'Country',
            'description' => 'Description',
        );
    }

    /**
     * @Author: ANH DUNG Jun 04, 2018
     * @Todo: sửa lại lỗi sai khi câp nhật nhầm KH mối của TPHCM sang NV bảo trì KH bò
     */
    public static function fixEmployeeToDistrict() {
//        $employee_id = 124; // Đinh Trọng Nghĩa
//        $employee_id = 885640; // Lương Văn Quang
//        $employee_id = 1329925; // Trần Ngọc Thương
        $aDistrict = [
            // Đinh Trọng Nghĩa
            1,// Quận 1
            3,// Quận 3
            13,// Quận Tân Bình
            18,// Quận Tân Phú
            16,// Quận Phú Nhuận
            
            // Lương Văn Quang
            2,// Quận 2
            9,// Quận 9
            17,// Quận Thủ Đức
            15,// Quận Bình Thạnh
//            
//            // Trần Ngọc Thương
            14, // Quận Gò Vấp
            12, // Quận 12
            22, // Huyện Hóc Môn
            23, // Huyện Củ Chi
        ];
        $from = time();
        $criteria=new CDbCriteria;
        $criteria->compare( 'type', GasUphold::TYPE_DINH_KY);
        $criteria->compare( 'type_customer', STORE_CARD_KH_MOI);
        $criteria->addInCondition('district_id', $aDistrict);
//        $criteria->addInCondition('province_id', [10]);
        $aUpdate = array('employee_id' => $employee_id);
        $models = GasUpholdBackup::model()->findAll($criteria);
//        $modelUpholdLive = GasUphold::model()->findAll($criteria);
        $count = 0;
        foreach($models as $item){
            $mUpholdFix = GasUphold::model()->findByPk($item->id);
            if($mUpholdFix){
                $count++;
                $mUpholdFix->employee_id = $item->employee_id;
                $mUpholdFix->update(['employee_id']);
            }
        }
        $to = time();
        $second = $to-$from;
        echo $count.' done in: '.($second).'  Second  <=> '.($second/60).' Minutes';die;
    }
    
}