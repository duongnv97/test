<?php

/**
 * This is the model class for table "{{_gas_support_agent}}".
 *
 * The followings are the available columns in table '{{_gas_support_agent}}':
 * @property string $id
 * @property string $code_no
 * @property string $agent_id
 * @property string $uid_login
 * @property integer $status
 * @property string $created_date
 */
class GasSupportAgent extends CActiveRecord
{
    public $MAX_ID;
    public $autocomplete_name;
    public $content;
    const STATUS_NEW = 1;
    const STATUS_APPROVED = 2;
    const STATUS_CANCEL = 3;
    
    public function getArrayStatus(){
        return array(
            GasSupportAgent::STATUS_NEW => "Mới",
            GasSupportAgent::STATUS_APPROVED => "Duyệt",
            GasSupportAgent::STATUS_CANCEL => "Hủy bỏ",
        );
    }
    
    // lấy số ngày cho phép user cập nhật
    public function getDayAllowUpdate(){
        return Yii::app()->params['days_update_support_agent'];
    }
    
    public static function model($className=__CLASS__)
    {
            return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
            return '{{_gas_support_agent}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        return array(
            array('content', 'required', 'on'=>'create,update'),
            array('id, code_no, agent_id, uid_login, status, created_date,content', 'safe'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        return array(
            'rUidLogin' => array(self::BELONGS_TO, 'Users', 'uid_login'),
            'rAgent' => array(self::BELONGS_TO, 'Users', 'agent_id'),
            'rComment' => array(self::HAS_MANY, 'GasComment', 'belong_id',
                'on'=>'rComment.type ='.GasComment::TYPE_2_SUPPORT_AGENT,
                'order'=>'rComment.id DESC',
            ),
            'rCommentAsc' => array(self::HAS_MANY, 'GasComment', 'belong_id',
                'on'=>'rCommentAsc.type ='.GasComment::TYPE_2_SUPPORT_AGENT,
                'order'=>'rCommentAsc.id ASC',
            ),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'code_no' => 'Mã số',
            'agent_id' => 'Đại lý',
            'uid_login' => 'Người tạo',
            'status' => 'Trạng thái',
            'created_date' => 'Ngày tạo',
            'content' => 'Nội dung',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        $criteria=new CDbCriteria;
        $criteria->compare('t.code_no',$this->code_no,true);
        $criteria->compare('t.agent_id',$this->agent_id);
        $criteria->compare('t.agent_id',$this->uid_login);// Vì trên giao diện để là search người tạo, nhưng mà dropdown đó trả về là đại lý nên sẽ để là agent_id
//        $criteria->compare('t.uid_login',$this->uid_login);
        $criteria->compare('t.status',$this->status);
        if(!empty($this->created_date)){
            $this->created_date = MyFormat::dateDmyToYmdForAllIndexSearch($this->created_date);
            $criteria->compare('t.created_date',$this->created_date,true);
        }

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
            'pagination'=>array(
                'pageSize'=> Yii::app()->user->getState('pageSize',Yii::app()->params['defaultPageSize']),
            ),
        ));
    }


    public function defaultScope()
    {
        return array();
    }
    
        
    protected function beforeSave() {
        if($this->isNewRecord){
            $this->uid_login = Yii::app()->user->id;
            $this->code_no = MyFunctionCustom::getNextId('GasSupportAgent', 'S'.date('y'), LENGTH_TICKET,'code_no');
            $this->agent_id = MyFormat::getAgentId();
        }
        $this->content = InputHelper::removeScriptTag($this->content);
        return parent::beforeSave();
    }
    
    /**
     * @Author: ANH DUNG Nov 02, 2015
     * @Todo: xử lý save chung bảng comment của hệ thống
     */
    public function SaveDetail() {
        $mComment = new GasComment();
        $mComment->content = $this->content;
        $mComment->status = $this->status;
        $mComment->SaveRow($this, GasComment::TYPE_2_SUPPORT_AGENT);
    }
    
    /**
     * @Author: ANH DUNG Nov 02, 2015
     * @Todo: xử lý update first record bảng comment
     */
    public function UpdateFirstDetail() {
        $mFirst = $this->rCommentAsc[0];
        $mFirst->content = $this->content;
        $mFirst->update(array('content'));
    }
    
    /**
     * @Author: ANH DUNG Nov 19, 2015
     * @Todo: something
     * @Param: 
     */
    public function UpdateBackToModel() {
        $this->update(array('status'));
    }
    
    /**
     * @Author: ANH DUNG Nov 02, 2015
     */
    public function LoadFirstDetail() {
        $this->content = $this->rCommentAsc[0]->content; 
    }
    
    public function getAgent() {
        $mUser = $this->rAgent;
        if($mUser){
            return $mUser->first_name;
        }
        return '';
    }
    
    public function getUidLogin() {
        $mUser = $this->rUidLogin;
        if($mUser){
            return $mUser->first_name;
        }
        return '';
    }
    
    public function getCodeNo() {
        return $this->code_no;
    }
    
    public function getStatus() {
        $aStatus = $this->getArrayStatus();
        return isset($aStatus[$this->status])?$aStatus[$this->status]:'';
    }
    
    public function getComment() {
        $res = "";
        foreach($this->rComment as $mComment){
            $res .= $mComment->ShowItem();
        }
        return $res;
    }
    
       
//    public function formatSpancopReport($model){
//        
//       $res = "";
//       foreach($model->rComment as $mComment){
//           $res .= "<b>{$mComment->getUidLogin()}</b> <i>{$cmsFormater->formatDateTime($mComment->created_date)}</i>: ".nl2br($mComment->getContent())."<br>";
//       }
//       $note = "<b>Ghi chú: </b>".nl2br($model->note);
//       $res = "$res".$note;
//       return $res;
//   }
    
    
    protected function beforeDelete() {
        GasComment::DeleteByType($this->id, GasComment::TYPE_2_SUPPORT_AGENT);
        return parent::beforeDelete();
    }
    
    
    
}