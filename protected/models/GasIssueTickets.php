<?php

/**
 * This is the model class for table "{{_gas_issue_tickets}}".
 *
 * The followings are the available columns in table '{{_gas_issue_tickets}}':
 * @property string $id
 * @property integer $type
 * @property string $code_no
 * @property string $agent_id
 * @property string $uid_login
 * @property string $title
 * @property string $customer_id 
 * @property string $send_to_id
 * @property integer $admin_new_message
 * @property integer $status
 * @property integer $process_status
 * @property string $process_time
 * @property string $process_user_id
 * @property string $created_date
 * @property string $sale_id
 * @property string $type_customer
 * @property date $renewal_date
 * @property date $is_user_plan
 * // field chief_monitor_id là field nhân viên chọn xử lý sự việc 
 */
class GasIssueTickets extends BaseSpj
{
    public $daysHandleJob = 6, $updateStopOrder = 0, $direction = 0, $applyPunish = 0, $msgHandle = '', $mDetail = null, $fromWeb = false, $isSaveRenewalDate = false, $applyMonitor = 0, $applyAccounting = 0, $applyLaw = 0,$oldChiefMonitorId = 0, $MAX_ID, $message, $file_name, $autocomplete_name , $date_from , $date_to;
    public $stage_process, $stage_status, $google_map;
    public $autocomplete_name_1, $applyQuickAction = 0;
    public $showMineOnly = false; // DuongNV Aug2919 hiện những issue có [Xử lý] đằng trc
    
    const IMAGE_MAX_UPLOAD      = 10;
    const IMAGE_MAX_UPLOAD_SHOW = 1;
    const PAGE_SIZE             = 10;
    const PAGE_SIZE_REPLIES     = 10;
    
    const DIR_REPLY             = 1; // from reply 
    const DIR_CLOSE             = 2; // from CLOSE
    const DIR_ADMIN_AUTO        = 3; // from auto create
    
    const VIEW_TYPE_NEW             = 1;
    const VIEW_TYPE_TONG_GIAM_SAT   = 2;
    const VIEW_TYPE_GIAM_SAT        = 3;
    const VIEW_TYPE_GIAM_DOC        = 4;
    const VIEW_TYPE_KE_TOAN         = 5;
    const VIEW_TYPE_KH_MONITOR      = 6;
    const VIEW_TYPE_QUICK_ACTION    = 7;
    const VIEW_TYPE_OPEN_AGENT      = 8;    
    const STAGE_TOTAL_OPEN_AGENT    = 3;
    
    const DATE_EXPIRE    = 7;
    
    // thiết kế để table này có thể dùng cho nhiều chức năng, không chỉ riêng issue
    // nên có thêm cột type
    const TYPE_ISSUE            = 1;
    const TYPE_OPEN_AGENT       = 2; // quy trình mở đại lý
    
    
    const USER_PLAN      = 1; // quản lý giao việc
    
    const MONEY_PUNISH  = 50000;
    
    public static $pathUpload = 'upload/issue_ticket';
    public static $aSize = array(
        'size1' => array('width' => 128, 'height' => 96), // small size, dùng ở form update văn bản
//        'size2' => array('width' => 1536, 'height' => 1200), // Close on Dec 10, 2015 resize ra hình to quá, không cần thiết
        'size2' => array('width' => 1024, 'height' => 900), // size view, dùng ở xem văn bản
    );
    // gia hạn các giai đoạn HaoNH
    public function getExpiredStage (){
        return array(30,60,7);
    }
    // Apr 20, 2015
    public function getUserClose(){ 
        return [
//            2, // admin
            GasLeave::PHUONG_PTK,
            GasLeave::THUC_NH,
            GasConst::UID_CHAU_LNM,
            GasLeave::UID_DIRECTOR,
        ];
    }
    
    /** @Author: DungNT Jul 16, 2018
     *  @Todo: get list user id có thể tạo giao việc để xử lý  
     **/
    public function getArrayUidCreatePlan() {
        return [
            GasLeave::UID_DIRECTOR      => GasLeave::UID_DIRECTOR,
            GasConst::UID_ADMIN         => GasConst::UID_ADMIN,
            GasLeave::UID_HEAD_OF_LEGAL => GasLeave::UID_HEAD_OF_LEGAL,
            GasLeave::UID_DIRECTOR_BUSSINESS    => GasLeave::UID_DIRECTOR_BUSSINESS,
            GasLeave::PHUC_HV                   => GasLeave::PHUC_HV,
            GasLeave::UID_HEAD_GAS_BO           => GasLeave::UID_HEAD_GAS_BO,// Sep1918 tạm bỏ anh Đức ra khỏi giao việc 
            GasConst::UID_PHUONG_ND             => GasConst::UID_PHUONG_ND,
            GasConst::UID_HIEP_TV               => GasConst::UID_HIEP_TV,
//            GasConst::UID_NGOC_PT               => GasConst::UID_NGOC_PT,// DungNT Aug2919 anh bỏ chức năng giao việc trên Support cho e nha anh
        ];
    }
    
    public static $STATUS_OPEN_CLOSE = array(
        GasTickets::STATUS_OPEN     =>'Open',
        GasTickets::STATUS_CLOSE    =>'Close',
    );
    
    // Apr 21, 2015
    public $index_autocomplete_1, $seach_autocomplete_customer, $ext_seach_customer_id, $seach_autocomplete_user, $ext_seach_user_id, $seach_autocomplete_sale, $ext_seach_sale_id, $seach_autocomplete_other_user, $ext_seach_other_user, $ext_seach_user_handle, $seach_autocomplete_user_handle;
    
    const PROBLEM_NHA_CC            = 1;
    const PROBLEM_KO_LAY_HANG       = 2;
    const PROBLEM_SANG_QUAN         = 3;
    const PROBLEM_NO_TIEN           = 4;
    const PROBLEM_PHAN_ANH_KH       = 5;
    const PROBLEM_PHAN_ANH_BAO_TRI  = 6;
    const PROBLEM_WAREHOUSE_STATUS  = 7;
    const PROBLEM_AGENT_WC          = 8;
    const PROBLEM_PVKH              = 9;
    const PROBLEM_REMOVE_SYSTEM     = 10;// gỡ hệ thống
    const PROBLEM_KH_HGD            = 11;// Sự vụ KH hộ GĐ
    const PROBLEM_OTHER             = 12;// khác
    const PROBLEM_GV_BUSINESS       = 13; // giao việc Kinh Doanh, bảo trì, kỹ thuật...
    const PROBLEM_APP_IMPROVE       = 14; // cải thiện app
    
    const MAX_OPEN_ALERT = 50;// giới hạn số ticket dc Open mà không có Popup
    
    const STATUS_1_NEW_SALE         = 1; // khi tạo mới sẽ notify cho sale + TP bò, mối + giám đốc KD
    const STATUS_2_SALE_REPLY       = 2; // khi sale OR TP bò, mối OR giám đốc KD có trả lời cho issue này
    const STATUS_3_DIRECTOR         = 3;// sau khi có trả lời notify cho audit, sau đó audit có thể Close hoặc chọn NV giám sát
    const STATUS_4_GIAM_SAT         = 4;// khi tổng Giám Sát chọn 1 NV giám sát xử lý thì là STATUS_3_GIAM_SAT
    const STATUS_5_DIRECTOR_REMOVE  = 5;// khi giám đốc có trả lời thì là trạng thái 4 này
    const STATUS_6_ACCOUNTING       = 6;// Công nợ CN
    const STATUS_7_MONITOR          = 7;// Thêm nút: KH cần theo dõi. đưa ra 1 list, hệ thống sẽ quét nếu trong 1 tháng không lấy hàng thì notify cho: sếp, phòng Kinh Doanh
    const STATUS_8_QUICK_ACTION     = 8;// Thêm nút xử lý gấp
    const STATUS_9_OPEN_AGENT       = 9; // status quy trình đại lý - HaoNH 0319
    
    // Set time alert open agent - HaoNH
    const ALERT_ROUTINE_OPEN_AGENT  = 6;
    const EXPIRED_ONCE              = 10; // UNIT:NGÀY
    const EXPIRED_TWICE             = 20;// UNIT:NGÀY
    const EXPIRED_BEFORE_ONCE       = 5;
    const EXPIRED_BEFORE_TWICE      = 5;
    
    public function getArrayProblem($empty = false){
        $aData = [];
        if($this->fromWeb){
            $cUid = MyFormat::getCurrentUid();
            if(in_array($cUid, $this->getArrayUidCreatePlan())){
                $aData[self::PROBLEM_GV_BUSINESS] = 'Giao việc';
            }
        }
        $aData += [
            self::PROBLEM_NHA_CC            => '2 nhà cung cấp',
            self::PROBLEM_KO_LAY_HANG       => 'Lâu không lấy hàng',
            self::PROBLEM_SANG_QUAN         => 'Sang quán',
            self::PROBLEM_NO_TIEN           => 'Nợ đọng',
            self::PROBLEM_REMOVE_SYSTEM     => 'Gỡ hệ thống',
            self::PROBLEM_PHAN_ANH_KH       => 'Phản ánh khách hàng',
            self::PROBLEM_PHAN_ANH_BAO_TRI  => 'Phản ánh bảo trì',
            self::PROBLEM_WAREHOUSE_STATUS  => 'Phản ánh tình trạng kho, đại lý',
            self::PROBLEM_AGENT_WC          => 'Phản ánh vệ sinh tại đại lý',
            self::PROBLEM_PVKH              => 'Phản ánh NV PVKH',
            self::PROBLEM_KH_HGD            => 'Sự vụ KH hộ GĐ',
            self::PROBLEM_OTHER             => 'Khác',
            self::PROBLEM_APP_IMPROVE       => 'Góp ý cải tiến App',
        ];
        
        
        if($empty){
            $aDefault = ['' => 'Chọn Nguyên Nhân'];
            return $aDefault + $aData;
        }
        return $aData;
    }
    
    // Now 24, 2015 Array to check in_array condition
    public function getArrayProblemCheck(){
        return array(
            self::PROBLEM_NHA_CC,
            self::PROBLEM_KO_LAY_HANG,
            self::PROBLEM_SANG_QUAN,
            self::PROBLEM_NO_TIEN,
            self::PROBLEM_PHAN_ANH_KH,
            self::PROBLEM_PHAN_ANH_BAO_TRI,
        );
    }
    
    const IS_EXPIRED        = 1;
    const IS_NOT_EXPIRED    = 0;
    
    /**
     * @Author: DungNT Now 24, 2015
     * @Todo: get problem text
     */
    public function getProblem() {
        $aProblem = $this->getArrayProblem();
        return isset($aProblem[$this->problem]) ? $aProblem[$this->problem]:"";
    }
    
    public function getRoleNotSeach() {
        $aRes = [
            ROLE_EMPLOYEE_MARKET_DEVELOPMENT, ROLE_EMPLOYEE_MAINTAIN, ROLE_ACCOUNTING_AGENT,
            ROLE_PHU_XE, ROLE_SUB_USER_AGENT, ROLE_MECHANIC, ROLE_WORKER, ROLE_SECURITY,ROLE_CLEANER, ROLE_FAMILY,
            ROLE_CANDIDATE, ROLE_DEPARTMENT, ROLE_DRIVER, ROLE_CHIET_NAP
            
        ];
        return implode(',', $aRes);
    }
    /** @Author: DungNT Jun 13, 2018
     *  @Todo: get url search employee at Web create and reply
     **/
    public function getUrlSearchEmployee() {
//        return Yii::app()->createAbsoluteUrl('admin/ajax/search_for_user_login', ['role_not_in'=>$this->getRoleNotSeach()]);
        return Yii::app()->createAbsoluteUrl('admin/ajax/search_user_login', ['PaySalary' => 1, 'ListRoleNotSearch' => $this->getRoleNotSeach()]);
    }
    
    /**
     * @Author: DungNT Feb 25, 2015
     * @Todo: get array role to send email notify
     */
    public static function GetRoleToSendEmailNotify() {
        return []; // May 07, 2016 không send cho những role dưới nữa
    }
    
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
            return '{{_gas_issue_tickets}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        return array(
            array('title, problem, message', 'required','on'=>'create'),
            array('message', 'required','on'=>'reply'),
            array('customer_id, message, title, problem', 'required','on'=>'ApiCreate'),
            array('message', 'required','on'=>'ApiCreate, ApiReply'),
            array('type_customer, problem, created_date_only, message,file_name', 'safe'),
            array('id, type, code_no, agent_id, uid_login, title, customer_id, send_to_id, admin_new_message, status, process_status, process_time, process_user_id, created_date,date_from,date_to', 'safe'),
            array('file_name', 'file','on'=>'UploadFileScan',
                    'allowEmpty'=>true,
                    'types'=> GasFileScanDetail::$AllowFile,
                    'wrongType'=> "Chỉ cho phép định dạng file ".  GasFileScanDetail::$AllowFile." .",
                    'maxSize'   => ActiveRecord::getMaxFileSize(),
                    'minSize'   => ActiveRecord::getMinFileSize(),
                    'tooLarge'  =>'The file was larger than '.(ActiveRecord::getMaxFileSize()/1024).' KB. Please upload a smaller file.',
                    'tooSmall'  =>'The file was smaller than '.(ActiveRecord::getMinFileSize()/1024).' KB. Please upload a bigger file.',                    
            ),
            array('updateStopOrder, is_user_plan, applyQuickAction, applyPunish, deadline, applyMonitor, applyAccounting, applyLaw, accounting_id, problem, chief_monitor_id, monitor_agent_id', 'safe'),
            //@Code: NAM014 
            array('renewal_date, showMineOnly', 'safe'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        return array(
            'rAgent' => array(self::BELONGS_TO, 'Users', 'agent_id'),
            'rTicketDetail' => array(self::HAS_MANY, 'GasIssueTicketsDetail', 'ticket_id',
                'order'=>'rTicketDetail.id DESC',
            ),
            'rUidLogin' => array(self::BELONGS_TO, 'Users', 'uid_login'),
            'rSendToId' => array(self::BELONGS_TO, 'Users', 'send_to_id'),
            'rProcessUserId' => array(self::BELONGS_TO, 'Users', 'process_user_id'),
            'rCustomer' => array(self::BELONGS_TO, 'Users', 'customer_id'),
            'rCloseUserId' => array(self::BELONGS_TO, 'Users', 'close_user_id'),
            'rSale' => array(self::BELONGS_TO, 'Users', 'sale_id'),
            'detailCount'=>array(self::STAT, 'GasIssueTicketsDetail', 'ticket_id'),
            'rChiefMonitor' => array(self::BELONGS_TO, 'Users', 'chief_monitor_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        $aRes = array(
            'id' => 'ID',
            'code_no' => 'Code No',
            'agent_id' => 'Agent',
            'uid_login' => 'Uid Login',
            'title' => 'Tiêu Đề',
            'send_to_id' => 'Send To',
            'admin_new_message' => 'Admin New Message',
            'status' => 'Status',
            'process_status' => 'Trạng Thái',
            'process_time' => 'Thời Điểm',
            'process_user_id' => 'Người Xử Lý',
            'created_date' => 'Ngày Tạo',
            'message' => 'Nội dung phản ánh sự việc',
            'customer_id' => 'Khách hàng',
            'file_name' => 'File Ảnh Đính Kèm',
            'ext_seach_customer_id' => 'Tìm kiếm theo khách hàng',
            'problem' => 'Nguyên nhân',
            'applyLaw' => 'Chuyển phòng pháp lý?',
            'applyAccounting' => 'Chuyển công nợ cá nhân?',
            'applyMonitor' => 'KH cần theo dõi',
            'renewal_date' => 'Ngày gia hạn',
            'deadline' => 'Deadline',
            'applyPunish' => 'Phạt nhân viên trả lời 50k',
//            'applyQuickAction' => 'Xử lý gấp',
            'applyQuickAction' => 'Meeting',
            'updateStopOrder' => '<b>Set trạng thái không lấy hàng</b>',
        );
        if($this->status_app == GasIssueTickets::STATUS_8_QUICK_ACTION){
            $aRes['applyQuickAction'] = 'Xóa Meeting';
        }
        return $aRes;
    }
       
    /**
     * @Author: DungNT Now 01, 2015
     * @Todo: count index Open of current user
     */
    public static function CountOpen() {
        $cUid = Yii::app()->user->id;
        $criteria=new CDbCriteria;
        $criteria->compare('t.status', GasTickets::STATUS_OPEN);
        $criteria->compare('t.type', GasIssueTickets::TYPE_ISSUE);
        $criteria->compare('t.uid_login', $cUid);
        return self::model()->count($criteria);
    }
    
    /**
     * @Author: DungNT Aug 10, 2014
     * @Todo: dùng cho đại lý list những ticket của đại lý đã submit
     */
    public function searchOpen()
    {
        $criteria=new CDbCriteria;
        $this->AddSameCriteria($criteria);
        $cUid = MyFormat::getCurrentUid();
//        $criteria->addCondition("t.uid_login=$uidLogin OR t.send_to_id=$uidLogin" );
//        $criteria->addCondition(
//                " ( t.uid_login=$uidLogin OR ".
//                " ( t.send_to_id=$uidLogin AND t.admin_new_message=".GasTickets::ADMIN_SEND." ) )"
//                ); 
        
//        $criteria->compare('t.uid_login', Yii::app()->user->id);
//        $criteria->compare('t.send_to_id',$this->send_to_id,true);
        $criteria->compare('t.status', GasTickets::STATUS_OPEN);
        $criteria->compare('t.type', GasIssueTickets::TYPE_ISSUE);
        $criteria->compare('t.process_status',$this->process_status);
        $criteria->compare('t.process_user_id',$this->process_user_id);
        $mUser = Users::model()->findByPk($cUid);
        $this->handleUserRole($criteria, $mUser, GasTickets::STATUS_OPEN);
//        $criteria->order = 't.process_time DESC';
//        $criteria->order = 't.last_reply DESC'; // Close on Dec 20, 2015 
        $criteria->order = 't.expired DESC, t.id DESC';// add Dec 20
        
        // Dec 30, 2015
        if(isset($_GET['view_type']) && $_GET['view_type'] == GasIssueTickets::VIEW_TYPE_NEW){
            $criteria->addCondition('t.id > 1043');
            throw new Exception('Yêu cầu không hợp lệ, liên hệ admin');
        }elseif(isset($_GET['view_type']) && $_GET['view_type'] == GasIssueTickets::VIEW_TYPE_TONG_GIAM_SAT){
            $criteria->compare('t.status_app', GasIssueTickets::STATUS_2_SALE_REPLY);
        }elseif(isset($_GET['view_type']) && $_GET['view_type'] == GasIssueTickets::VIEW_TYPE_GIAM_SAT){// tab pháp lý
//            $criteria->addCondition("t.monitor_agent_id IS NOT NULL ");
            $criteria->compare('t.status_app', GasIssueTickets::STATUS_4_GIAM_SAT);
        }elseif(isset($_GET['view_type']) && $_GET['view_type'] == GasIssueTickets::VIEW_TYPE_GIAM_DOC){
            $criteria->compare('t.status_app', GasIssueTickets::STATUS_3_DIRECTOR);
        }elseif(isset($_GET['view_type']) && $_GET['view_type'] == GasIssueTickets::VIEW_TYPE_KH_MONITOR){
            $criteria->compare('t.status_app', GasIssueTickets::STATUS_7_MONITOR);
        }elseif(isset($_GET['view_type']) && $_GET['view_type'] == GasIssueTickets::VIEW_TYPE_QUICK_ACTION){
            $criteria->compare('t.status_app', GasIssueTickets::STATUS_8_QUICK_ACTION);// Jul0818 
        }elseif(isset($_GET['view_type']) && $_GET['view_type'] == GasIssueTickets::VIEW_TYPE_KE_TOAN){
            $criteria->compare('t.status_app', GasIssueTickets::STATUS_6_ACCOUNTING);
        }else{// for tab Issue moi
            $criteria->compare('t.status_app', GasIssueTickets::STATUS_1_NEW_SALE);
        }
        
        // DuongNV Aug2919 chỉ hiện những issue có [Xử lý] ở trc (GasIssueTickets->getDeadlineOnGrid)
        if( !empty($_GET['GasIssueTickets']['showMineOnly']) ){
            $cUid = MyFormat::getCurrentUid();
            $criteria->addCondition('t.status != '.GasTickets::STATUS_CLOSE);
            $criteria->compare('t.chief_monitor_id', $cUid);
        }
        
        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
            'pagination'=>array(
//                'pageSize'=> 3,
                'pageSize'=> $this->pageSize,
            ),
        ));
    }
    
    /** @Author: NGUYEN KHANH TOAN 2018
     *  @Todo: dùng cho những phản hồi của ticket
     *  @Param:
     **/
    public function searchReplies() {
        $criteria = new CDbCriteria;
        $criteria->with = array('rIssueTickets');
        $criteria->condition = "rIssueTickets.id = $this->id";
        $criteria->order = 't.id DESC';
        return new CActiveDataProvider('GasIssueTicketsDetail', array(
            'criteria'=>$criteria,
            'pagination'=>array(
                'pageSize'=> GasIssueTickets::PAGE_SIZE_REPLIES,
            ),
        ));
        
    }
    
    /**
     * @Author: DungNT Nov 17, 2015
     * @Todo: something
     */
    public function AddSameCriteria($criteria) {
        $cUid = MyFormat::getCurrentUid();
        $aSaleAdmin = [GasLeave::UID_HEAD_GAS_BO, GasConst::UID_HA_PT];
        $criteria->compare('t.code_no', trim($this->code_no));
        $criteria->compare('t.customer_id', $this->customer_id);
        $criteria->compare('t.uid_login', $this->uid_login);// Add Now 17, 2015
        $criteria->compare('t.sale_id', $this->sale_id);// Add Now 17, 2015
        $criteria->compare('t.expired', $this->expired); // Add sep 03, 2019 HaoNH
        $criteria->compare('t.stage_current', $this->stage_current); // Add sep 03, 2019 HaoNH
//        if(in_array($this->chief_monitor_id, $aSaleAdmin)){
        if(in_array($cUid, $aSaleAdmin)){// Add Dec1718
            $aSale = [GasConst::UID_HA_PT, GasLeave::UID_HEAD_GAS_BO, GasConst::UID_VINH_VT, GasLeave::KH_CONGTY_BO, GasLeave::KH_CONGTY_MOI];
            $sParamsIn = implode(',', $aSale);
            $criteria->addCondition("t.uid_login = $cUid OR t.chief_monitor_id IN ($sParamsIn) OR t.sale_id IN ($sParamsIn)");
        }elseif(!empty($this->chief_monitor_id)){// serch người xử lý hoặc người đc giao việc
            $criteria->addCondition("t.uid_login = $cUid OR t.chief_monitor_id={$this->chief_monitor_id} OR t.customer_id=$this->chief_monitor_id");// Add Jun 19, 2018
        }
        if(!empty($this->ext_seach_other_user)){
//            $criteria->addInCondition("t.id", GasIssueTicketsDetail::getIssueIdByUserPost($this->ext_seach_other_user));
            $sParamsIn = implode(',', GasIssueTicketsDetail::getIssueIdByUserPost($this->ext_seach_other_user));
            if(!empty($sParamsIn)){
                $criteria->addCondition("t.id IN ($sParamsIn)");
            }
        }
    }
    
    public function searchClose(){
        $cUid = Yii::app()->user->id;
        $typeIssue = (isset($_GET['view_type']) && $_GET['view_type'] == GasIssueTickets::VIEW_TYPE_OPEN_AGENT) ? GasIssueTickets::TYPE_OPEN_AGENT : GasIssueTickets::TYPE_ISSUE; //HaoNH
        $criteria = new CDbCriteria;
        $this->AddSameCriteria($criteria);
        $criteria->compare('t.status', GasTickets::STATUS_CLOSE);
        $criteria->compare('t.type', $typeIssue);
//        $criteria->compare('t.send_to_id',$this->send_to_id,true);
//        $criteria->addCondition("( t.uid_login=$uidLogin OR ( t.send_to_id=$uidLogin AND t.process_user_id<>$uidLogin ))" );
        
        $criteria->compare('t.process_status',$this->process_status);
        $criteria->compare('t.process_user_id',$this->process_user_id);
//        $criteria->order = 't.process_time DESC';
        $criteria->order = 't.last_reply DESC';
        $mUser = Users::model()->findByPk($cUid);
        $this->handleUserRole($criteria, $mUser, GasTickets::STATUS_CLOSE);
        
        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
            'pagination'=>array(
                'pageSize'=> 5,
            ),
        ));
    }
    
    /*** HANDLE VALIDATE AND SAVE MULTI FILE ********/
    /**
     * @Author: DungNT Feb 25, 2015
     * @Todo: validate multi file 
     * @Param: $model is model GasIssueTickets
     */
    public static function ValidateFile($model) {
        $ok=false; // for check if no file submit
        if(isset($_POST['GasIssueTickets']['file_name'])  && count($_POST['GasIssueTickets']['file_name'])){
            foreach($_POST['GasIssueTickets']['file_name'] as $key=>$item){
                $mFile = new GasIssueTickets('UploadFileScan');
                $mFile->file_name  = CUploadedFile::getInstance($model,'file_name['.$key.']');
                $mFile->validate();
//                if(!is_null($mFile->file_name) && !$mFile->hasErrors()){
                if(!is_null($mFile->file_name)){
                    $ok=true;
                    MyFormat::IsImageFile($_FILES['GasIssueTickets']['tmp_name']['file_name'][$key]);
                }
                
                if($mFile->hasErrors()){
                    $model->addError('file_name', $mFile->getError('file_name'));
                }
            }
        }
    }
    
    /**
     * @Author: DungNT Feb 25, 2015
     * @Todo: save one record for begin topic
     * @Param: $model GasIssueTickets
     */
    public function saveRecordBeginIssue() {
        if($this->direction == GasIssueTickets::DIR_ADMIN_AUTO){
            $cUid = $this->uid_login;// Sep0418 from cron console
        }else{// action from web
            $cUid = MyFormat::getCurrentUid();
            $this->uid_login = $cUid;
        }
        //HaoNH
        if ((isset($_GET['view_type']) && $_GET['view_type'] == GasIssueTickets::VIEW_TYPE_OPEN_AGENT)) {
            $this->addFieldsOpenAgent();
        } else {
            if ($this->canMapIssueOld()) {
                $this->handleCaseSpecial();
                $this->doMapIssueOld();
                return;
            }
        }        
        // 1. save new issue
        $this->saveNewRecordFromWebAndApp($cUid);
        // 2. save one detail
        $this->saveRecordDetail($cUid);
    }
    
    /**
     * @Author: DungNT Jan 27, 2016
     * @Todo: handle save new record from web and app mobile
     * @Param: $model is model GasIssueTickets
     * @param: $cUid current uid login
     */
    public function saveNewRecordFromWebAndApp($cUid) {
        $this->type        = GasIssueTickets::TYPE_ISSUE;
        $this->title       = trim(InputHelper::removeScriptTag($this->title));
        $this->code_no     = MyFunctionCustom::getNextId('GasIssueTickets', 'E'.date('y'), LENGTH_TICKET,'code_no');
        $this->uid_login   = $cUid;
        $this->validate();
        if($this->direction != GasIssueTickets::DIR_ADMIN_AUTO && in_array($cUid, $this->getArrayUidCreatePlan())){// Jul0818 cho sang tab xử lý khi GD tạo mới
            $this->status_app = GasIssueTickets::STATUS_2_SALE_REPLY;
            if($cUid != GasLeave::UID_DIRECTOR && $this->problem == GasIssueTickets::PROBLEM_GV_BUSINESS){
                $this->is_user_plan = GasIssueTickets::USER_PLAN;
            }
            if($cUid != GasLeave::UID_DIRECTOR && $this->problem != GasIssueTickets::PROBLEM_GV_BUSINESS){
                $this->status_app = GasIssueTickets::STATUS_1_NEW_SALE;
            }
//            $this->chief_monitor_id = $this->customer_id; // Jun0119 ? tai sao cho nay lai gắn customer_id cho chief_monitor_id? -
// do trc đây không có filed chief_monitor_id ở dưới post lên mà dùng chung field CustomerId để làm người xử lý -> hiện tại Jun0119 đã tách ra riêng, nên ko cần gắn vậy nữa 
        }
         //HaoNH
        if ((isset($_GET['view_type']) && $_GET['view_type'] == GasIssueTickets::VIEW_TYPE_OPEN_AGENT)) {
            $this->addFieldsOpenAgent();
        }
        
        $this->handleCaseSpecial();
        $this->save();
        $this->ApiBuildNotifyS1();// Dec 16, 2015 - add App Notify
    }

    
    /** @Author: DungNT Sep 10, 2018
     *  @Todo: xử lý case special  
     **/
    public function handleCaseSpecial() {
        $aUidMove = [GasLeave::THUC_NH];// nếu là Thúc tạo thì cho sang sale luôn
        if(in_array($this->uid_login, $aUidMove)){
            $this->direction        = GasIssueTickets::DIR_ADMIN_AUTO;
            $this->status_app       = GasIssueTickets::STATUS_2_SALE_REPLY;
            $this->chief_monitor_id = $this->sale_id;
            
        }
    }
    
    /**
     * @Author: DungNT Now 22, 2015
     * @Todo: API save one record for begin topic
     * @Param: $model GasIssueTickets
     * @Param: $mUser
     */
    public function apiSaveRecordBeginIssue($mUser) {
        $cUid = $mUser->id;
        // 1. save new issue
        $this->saveNewRecordFromWebAndApp($cUid);
        // 2. save one detail
        $this->apiSaveRecordDetail($cUid);
    }    
    
    /**
     * @Author: DungNT Feb 25, 2015
     * @Todo: save record detail
     * @Param: $mGasIssueTickets GasIssueTickets
     */
    public function saveRecordDetail($uid_post) {
        // Dec 15, 2015 fix update thêm khi submit ở trên web
        $move_to_uid = '';
        $this->setChiefMonitorOnWeb($move_to_uid);
        $mGasIssueTicketsDetail = $this->saveRecordDetailFromWebAndApp($uid_post, $move_to_uid);
        $this->mDetail = $mGasIssueTicketsDetail;
        // 3. save multifile
        GasIssueTickets::SaveRecordFile($this, $mGasIssueTicketsDetail);
//        GasScheduleEmail::BuildListNotifyIssueTicket($mGasIssueTicketsDetail->id);// Jul0218 Close, viết lại hàm khác, hàm này viết không đc tốt 
        $this->replySendEmail($mGasIssueTicketsDetail);
    }
    
    /**
     * @Author: DungNT Jan 27, 2016
     * @Todo: gộp save detail from web and app lại 
     * @Param: $mGasIssueTickets, $uid_post, $move_to_uid
     */
    public function saveRecordDetailFromWebAndApp($uid_post, $move_to_uid) {
        $mGasIssueTicketsDetail             = new GasIssueTicketsDetail();
        $mGasIssueTicketsDetail->ticket_id  = $this->id;
        
        $mGasIssueTicketsDetail->message    = trim(InputHelper::removeScriptTag($this->message));
        $mGasIssueTicketsDetail->uid_post   = $uid_post;
        if($mGasIssueTicketsDetail->rUidPost){
            $mGasIssueTicketsDetail->c_name = MyFormat::GetNameWithLevel($mGasIssueTicketsDetail->rUidPost);
        }

        if(!empty($move_to_uid)){
            $mGasIssueTicketsDetail->move_to_uid = $move_to_uid;
        }
        if($this->isSaveRenewalDate){
            $mGasIssueTicketsDetail->renewal_date = $this->renewal_date;
        }
        if($this->applyPunish){
            $mGasIssueTicketsDetail->employee_problems_id = EmployeeProblems::PROBLEM_DIRECTOR_PUNISH;
        }
        if (isset($_GET['view_type']) && $_GET['view_type'] == GasIssueTickets::VIEW_TYPE_OPEN_AGENT) {
            $mGasIssueTicketsDetail->stage_process = $this->stage_current;
            $mGasIssueTicketsDetail->stage_status = $this->process_status;
        }

        $mGasIssueTicketsDetail->save();
        $this->updateLastReply();
        return $mGasIssueTicketsDetail;
    }
    
    /**
     * @Author: DungNT Now 22, 2015
     * @Todo: save record detail
     * @Param: $mGasIssueTickets GasIssueTickets
     */
    public function apiSaveRecordDetail($uid_post, $move_to_uid = '') {
        $mGasIssueTicketsDetail = $this->saveRecordDetailFromWebAndApp($uid_post, $move_to_uid);
        // 3. save multifile
        $this->apiSaveRecordFile($mGasIssueTicketsDetail);
        // update fix - May 12, 2016 thiếu phần update user post cuối cùng của issue này
        $this->updateStatusTicket($this->process_status, $uid_post);
//        GasScheduleEmail::BuildListNotifyIssueTicket($mGasIssueTicketsDetail->id);
        $this->replySendEmail($mGasIssueTicketsDetail);
    }
    
    public function updateLastReply() {
        $this->last_reply = date('Y-m-d H:i:s');
        $this->update(array('last_reply'));
    }
    
    /**
     * @Author: DungNT Feb 25, 2015
     * @Todo: save record multi file for one detail
     * @Param: $mGasIssueTickets GasIssueTickets
     * @Param: $mGasIssueTicketsDetail GasIssueTicketsDetail
     */
    public static function SaveRecordFile($mGasIssueTickets, $mGasIssueTicketsDetail) {
        set_time_limit(7200);
        $mRoot = GasIssueTickets::model()->findByPk($mGasIssueTickets->id); // để lấy created_date, không thể find rồi đưa vào biến $model dc vì còn liên quan đến get file
        if(isset($_POST['GasIssueTickets']['file_name'])  && count($_POST['GasIssueTickets']['file_name'])){
            foreach($_POST['GasIssueTickets']['file_name'] as $key=>$item){
                $mFile = new GasIssueTicketsDetailFile();
                $mFile->ticket_id = $mGasIssueTickets->id;
                $mFile->issue_tickets_detail_id = $mGasIssueTicketsDetail->id;
                $created_date = explode(' ', $mRoot->created_date);
                $mFile->created_date = $created_date[0];
                $mFile->order_number = $key+1;
                $mFile->file_name  = CUploadedFile::getInstance($mGasIssueTickets,'file_name['.$key.']');
                if(!is_null($mFile->file_name)){
                    $mFile->file_name  = self::saveFile($mFile, 'file_name', $key);
                    $mFile->save();
                    self::resizeImage($mFile, 'file_name');
                }
            }
        }
    }
    
    /**
     * @Author: DungNT Feb 25, 2015
     * To do: save file 
     * @param: $model GasIssueTicketsDetailFile
     * @param: $count 1,2,3
     * @return: name of image upload/transactions/property_document
     */
    public static function  saveFile($model, $fieldName, $count)
    {        
        if(is_null($model->$fieldName)) return '';
        $year = MyFormat::GetYearByDate($model->created_date);
        $month = MyFormat::GetYearByDate($model->created_date, array('format'=>'m'));
        $pathUpload = GasIssueTickets::$pathUpload."/$year/$month";
        $ext = $model->$fieldName->getExtensionName();
        $fileName = time();
        $fileName = $fileName."-".ActiveRecord::randString().$count.'.'.$ext;
        $imageProcessing = new ImageProcessing();
        $imageProcessing->createDirectoryByPath($pathUpload);
        $model->$fieldName->saveAs($pathUpload.'/'.$fileName);
        return $fileName;
    }
    
    /**
     * @Author: DungNT Feb 25, 2015
     * To do: resize file scan
     * @param: $model model GasIssueTicketsDetailFile
     * @param: $fieldName 
     */
    public static function resizeImage($model, $fieldName) {
        $year = MyFormat::GetYearByDate($model->created_date);
        $month = MyFormat::GetYearByDate($model->created_date, array('format'=>'m'));
        $pathUpload = GasIssueTickets::$pathUpload."/$year/$month";
        $ImageHelper = new ImageHelper();
        $ImageHelper->folder = '/'.$pathUpload;
        $ImageHelper->file = $model->$fieldName;
        $ImageHelper->aRGB = array(0, 0, 0);//full black background
        $ImageHelper->thumbs = self::$aSize;
//        $ImageHelper->createFullImage = true ;
        $ImageHelper->createThumbs();
        $ImageHelper->deleteFile($ImageHelper->folder . '/' . $model->$fieldName);        
    }
    
    /**
     * @Author: DungNT Feb 25, 2015
     * To do: delete file scan
     * @param: $model model user
     * @param: $fieldName is avatar, agent_company_logo
     * @param: $aSize
     */
    public static function RemoveFileOnly($pk, $fieldName) {
        $modelRemove = GasIssueTicketsDetailFile::model()->findByPk($pk);
        if (is_null($modelRemove) || empty($modelRemove->$fieldName))
            return;
        $aDate = explode('-', $modelRemove->created_date);
        $pathUpload = GasIssueTickets::$pathUpload."/$aDate[0]/$aDate[1]";            
        $ImageHelper = new ImageHelper();     
        $ImageHelper->folder = '/'.$pathUpload;
        $ImageHelper->deleteFile($ImageHelper->folder . '/' . $modelRemove->$fieldName);
        foreach ( GasIssueTickets::$aSize as $key => $value) {
            $ImageHelper->deleteFile($ImageHelper->folder . '/' . $key . '/' . $modelRemove->$fieldName);
        }
    } 
    
    /*** HANDLE VALIDATE AND SAVE MULTI FILE ********/
    /**
     * @Author: DungNT Feb 25, 2015
     * @Todo: cập nhật trạng thái của ticket, chỗ này dùng cho update pending vs finish
    // trạng thái free của ticker: 1: new, 2: user pick, 3: finish
     * @Param: $mTicket model gastickets
     * @Param: $status 1,2,3 GasTickets::PROCESS_STATUS_FINISH
     */
    public function updateStatusTicket($status, $cUid){
        $this->process_status    = $status;
        $this->process_time      = date('Y-m-d H:i:s');
        $this->process_user_id   = $cUid;
        $this->update();
    }
    
    /**
     * @Author: DungNT Feb 25, 2015
     * @Todo: check user can close ticket
     */
    public function canClose() {
        $cUid = MyFormat::getCurrentUid();
        $ok = false;
        if($cUid == GasConst::UID_ADMIN){
            return true;
        }
        
//        $aStatusCanClose = [GasIssueTickets::STATUS_4_GIAM_SAT, GasIssueTickets::STATUS_6_ACCOUNTING, GasIssueTickets::STATUS_7_MONITOR];
        $aStatusCanClose = [GasIssueTickets::STATUS_1_NEW_SALE, GasIssueTickets::STATUS_2_SALE_REPLY];
//        if(in_array($cUid, $this->getUserClose()) && in_array($this->status_app, $aStatusCanClose)){
        if( (in_array($cUid, $this->getUserClose()) && !in_array($this->status_app, $aStatusCanClose))
                || ($this->uid_login == $cUid && $this->problem == GasIssueTickets::PROBLEM_GV_BUSINESS && in_array($cUid, $this->getArrayUidCreatePlan()))
                ){
            $ok = true;
        }
        return $ok;
    }
    
    /**
     * @Author: DungNT Feb 25, 2015
     * @Todo: check user can close ticket
     * @Param: $model GasIssueTickets
     */
    public static function CanReply($model) {
        $cUid = MyFormat::getCurrentUid();
        $cRole = MyFormat::getCurrentRoleId();
        $aRoleToNotify = GasIssueTickets::GetRoleToSendEmailNotify();
        $ok = true;
        if($cRole == ROLE_ADMIN){
            return true;
        }
//        if($cUid != $model->uid_login && !in_array($cRole, $aRoleToNotify)
//                ){
//            $ok = false;
//        }
        return $ok;
    }
    
    /**
     * @Author: DungNT Feb 25, 2015
     * @Todo: handle close ticket
     * @Param: $model GasIssueTickets
     */
    public function closeTicket(){
        $cUid = MyFormat::getCurrentUid();
        $this->apiCloseTicket($cUid);
    }
    
    /**
     * @Author: DungNT Dec 10, 2015
     * @Todo: close ticket for API and web
     */
    public function apiCloseTicket($close_user_id) {
        $this->status           = GasTickets::STATUS_CLOSE;
        $this->close_user_id    = $close_user_id;
        $this->close_date       = date('Y-m-d H:i:s');
        
        $this->update(array('status', 'close_user_id', 'close_date'));
//        $this->setExpired(GasIssueTickets::IS_NOT_EXPIRED); Jun2718 close, thấy không cần thiết lắm
        $this->closeSendEmail();
        $this->ApiBuildNotifyClose();
    }
    
    /** @Author: DungNT Jun 22, 2018
     *  @Todo: Gui email cho toan bo user khi close issue
     **/
    public function closeSendEmail() {
        $mUserClose = Users::model()->findByPk($this->close_user_id);
        $aEmail = [];
        if( $this->close_user_id != GasLeave::UID_DIRECTOR){
            $aUid[$this->close_user_id] = $this->close_user_id;
        }
        $this->getListEmailInThisIssue($aUid, $aEmail);
        $needMore = [];
        $needMore['title']      =  '[CLOSE] Phản ánh: '.$this->title;
        $this->msgHandle        = "$mUserClose->first_name Close phản ánh sự việc";
        $mGasIssueTicketsDetail = $this->getLatestReply();
        $this->sendEmailAlert($mGasIssueTicketsDetail, $aEmail, $needMore);
    }
    
    /** @Author: DungNT Jun 02, 2018
     *  @Todo: Gui email cho toan bo user khi có reply issue
     *  @note: fix lại 1 hàm email đã có trước 
     **/
    public function replySendEmail($mGasIssueTicketsDetail) {
        $aEmail = $aUid = [];
        $this->direction = GasIssueTickets::DIR_REPLY;
        $this->getListEmailInThisIssue($aUid, $aEmail);
        $this->sendEmailAlert($mGasIssueTicketsDetail, $aEmail);
    }
    
    /** @Author: DungNT Jul 02, 2018
     *  @Todo: Gui email cảnh báo cho sale và người xử lý trc 1 ngày khi Issue Expired
     **/
    public function alertExpiredSendEmail() {
        $aEmail = $aUid = [];
        $this->getSaleIdOnly($aUid);
        if(!empty($this->chief_monitor_id)){
            $aUid[$this->chief_monitor_id] = $this->chief_monitor_id;
        }
        $this->getArrayEmailByArrayUid($aUid, $aEmail);
        
        $needMore = [];
        $needMore['title']      =  '[Cảnh báo quá hạn 2 ngày] Phản ánh: '.$this->title;
        $this->msgHandle        = "Phản ánh này sẽ quá hạn xử lý trong 2 ngày tới bạn vui lòng vào trả lời để không bị phạt tự động";
        $mGasIssueTicketsDetail = $this->getLatestReply();
        $this->sendEmailAlert($mGasIssueTicketsDetail, $aEmail, $needMore);
    }
    
    /** @Author: DungNT Jul 19, 2018
     *  @Todo: lấy danh sách email những người đã post trên 1 issue
     *  @param: array $aUid, $aEmail
     **/
    public function getListEmailInThisIssue(&$aUid, &$aEmail) {
        $mIssue = GasIssueTickets::model()->findByPk($this->id);
        $mIssue->getAllUidInIssue($aUid);
        if($this->direction == GasIssueTickets::DIR_REPLY || ($this->close_user_id == GasLeave::UID_DIRECTOR)){
            unset($aUid[GasLeave::UID_DIRECTOR]);
        }
        
        $this->getArrayEmailByArrayUid($aUid, $aEmail);
    }
    
    /** @Author: DungNT Now 10, 2018
     *  @Todo: get list user id in one Issue
     **/
    public function getAllUidInIssue(&$aUid) {
        foreach($this->rTicketDetail as $mDetail){
            $aUid[$mDetail->uid_post] = $mDetail->uid_post;
        }
        $this->getSaleIdOnly($aUid);
        $mCustomer = $this->rCustomer;
        if(!empty($mCustomer) && $mCustomer->role_id != ROLE_CUSTOMER){
            $aUid[$this->customer_id] = $this->customer_id;
        }
        
        if(!empty($this->chief_monitor_id)){
            $aUid[$this->chief_monitor_id] = $this->chief_monitor_id;
        }
        if(!empty($this->oldChiefMonitorId)){
            $aUid[$this->oldChiefMonitorId] = $this->oldChiefMonitorId;
        }
    }
    
    /** @Author: DungNT Jul 02, 2018
     *  @Todo: get list email by list user id
     **/
    public function getArrayEmailByArrayUid(&$aUid, &$aEmail) {
        if(count($aUid) < 1 ){// Jul0418 nếu không kiểm tra thì sẽ get all email hệ thống nếu bị empty array
            return ;
        }
        $needMore = array('aUid'=>$aUid);
        $aUser = Users::GetListUserMail($needMore);
        foreach($aUser as $item){
            $aEmail[] = $item->email;
        }
    }
    
    
    /**
     * @Author: DungNT Feb 25, 2015
     * @Todo: handle close ticket
     * @Param: $model GasIssueTickets
     */
    public static function ReopenTicket($model){
        $model->status = GasTickets::STATUS_OPEN;
        $model->update(array('status'));
        $model->ApiBuildNotifyReopen();
    }
    
    /**
     * @Author: DungNT Feb 25, 2015
     * @Todo: get list model user lien quan ( dung de notify ) trong 1 model support customer
     * @Param: $issue_tickets_detail_id
     */
    public static function GetListModelUserNotify($issue_tickets_detail_id) {
        $mGasIssueTicketsDetail = GasIssueTicketsDetail::model()->findByPk($issue_tickets_detail_id);
        $mGasIssueTickets = $mGasIssueTicketsDetail->rIssueTickets;
        $aUidSendMore = [];
        // Close on May 07, 2016 không send cho những role này nữa
//        $aRoleToNotify = GasIssueTickets::GetRoleToSendEmailNotify();
//        $aUidNotReset = array($mGasIssueTicketsDetail->uid_post);
//        $needMore = array('aRoleId' => $aRoleToNotify, 'aUidNotReset'=>$aUidNotReset);
//        $aModelUserMail = Users::GetListUserMail($needMore);
        
//        $mUserPost = Users::model()->findByPk($mGasIssueTicketsDetail->uid_post);
//        if(is_array($aModelUserMail)){
//            $aModelUserMail[] = $mUserPost;
//        }else{
//            $aModelUserMail = array($mUserPost);
//        }
        $aModelUserMail = [];// fix Jun 13, 2016 không send mail cho người post issue nữa, chi send cho những người liên quan
        $aUidCheckSend = [];// fix Jul 13, 2016 array check những người đã có trong list send mail
        
        // ngoài những role dc notify mặc định ra thì add thêm sale của customer đó vào nữa
        // và đại lý của customer đó
//        self::GetSaleOfCustomer($mGasIssueTicketsDetail, $aModelUserMail, $aUidCheckSend);
        $mGasIssueTickets->getSaleIdOfCustomer($aUidSendMore);
        
        // May 04, 2016 thêm mail cho Chị Nga khi có trả lời trên issue đó
        
        if($mGasIssueTicketsDetail->uid_post != GasLeave::AUDIT_ISSUE){// bị trùng mail cho Nga Audit nếu người post là Nga
//            $aUidSendMore[] = GasLeave::AUDIT_ISSUE;
        }
        if($mGasIssueTicketsDetail->uid_post != $mGasIssueTickets->uid_login && $mGasIssueTicketsDetail->uid_post!= GasLeave::AUDIT_ISSUE){
            // Jun 10, 2016 không gửi mail cho chị Nga nữa => theo y/c của chị
            $aUidSendMore[$mGasIssueTickets->uid_login] = $mGasIssueTickets->uid_login;
        }// May 07, 2016 thêm người tạo ticket đầu tiên khi có reply trên ticket đó
        
        // Jun 23, 2016 get thêm list của các tổng GS + chuyên viên để send mail luôn
        if(!empty($mGasIssueTickets->chief_monitor_id)){
            $aUidSendMore[$mGasIssueTickets->chief_monitor_id] = $mGasIssueTickets->chief_monitor_id;
        }
        if(!empty($mGasIssueTickets->monitor_agent_id)){
            $aUidSendMore[$mGasIssueTickets->monitor_agent_id] = $mGasIssueTickets->monitor_agent_id;
        }
        // End Jun 23, 2016 get thêm list của các tổng GS + chuyên viên để send mail luôn
        if($mGasIssueTickets->status_app == GasIssueTickets::STATUS_4_GIAM_SAT){
            // Apr1218 gửi cho chị Châu nếu chuyển pháp lý
            $aUidSendMore[GasConst::UID_CHAU_LNM]   = GasConst::UID_CHAU_LNM;
            $aUidSendMore[GasConst::UID_HANH_NT]    = GasConst::UID_HANH_NT;
        }
        
        if(count($aUidSendMore)){
            $needMore = array('aUid'=>$aUidSendMore);
            $aModelSendMore = Users::GetListUserMail($needMore);
            foreach($aModelSendMore as $item){
                if(!in_array($item->id, $aUidCheckSend)){// Fix Jul 13, 2016 bị send duplicate nhiều
                    $aModelUserMail[] = $item;
                }else{
                    $aUidCheckSend[] = $item->id;
                }
            }
        }
        // May 04, 2016 thêm mail cho Chị Nga khi có trả lời trên issue đó
        
//        $aModelUserMail = []; // for test, will close when live (Mar 02, 2015)
//        MyFormat::AddEmailAnhDung($aModelUserMail); // only for test,
        return $aModelUserMail;
    }
    
    /**
     * @Author: DungNT Mar 06, 2015
     * @Todo: get sale của customer để send mail notify
     * @Param: $mGasIssueTicketsDetail
     * @Return: model sale
     */
    public static function GetSaleOfCustomer($mGasIssueTicketsDetail, &$aModelUserMail, &$aUidCheckSend) {
        $mSale = null;
        $mGasIssueTickets = GasIssueTickets::model()->findByPk($mGasIssueTicketsDetail->ticket_id);
        $mCustomer = Users::model()->findByPk($mGasIssueTickets->customer_id);
        if($mCustomer){
            // 1. add sale
            $mSale = Users::model()->findByPk($mCustomer->sale_id);
            if($mSale){
                $aModelUserMail[] = $mSale;
                $aUidCheckSend[$mSale->id] = $mSale->id;
            }
            // 2. add mail đại lý
            // Sep 25, 2015  thêm notify cho đại lý nếu là KH của đai lý
            // Người tạo ticket đầu tiên là $mGasIssueTickets->uid_login
//           Close on Apr0218 $mGasIssueTickets->getMailAgentCustomer($mCustomer, $aModelUserMail, $aUidCheckSend);
        }
        return $mSale;
    }
    
    /**
     * @Author: DungNT Sep 25, 2015
     * @Todo: get mail agent cua customer them vao list send
     * @Param: $mCustomer, &$aModelUserMail array model User to send Mail
     */
    public function getMailAgentCustomer($mCustomer, &$aModelUserMail, &$aUidCheckSend) {
        $mAgent = Users::model()->findByPk($mCustomer->area_code_id);
        if($mAgent){
            $aModelUser = $mAgent->getModelSubAgent();
            foreach($aModelUser as $mUser){
                $aModelUserMail[] = $mUser;
                $aUidCheckSend[] = $mUser->id;
            }
        }
    }
    
    protected function beforeDelete() {
        MyFormat::deleteModelDetailByRootId('GasIssueTicketsDetail', $this->id, 'ticket_id');
        
        GasScheduleNotify::DeleteAllByType(GasScheduleNotify::ISSUE_TICKET, $this->id);
        GasScheduleNotifyHistory::DeleteAllByType(GasScheduleNotify::ISSUE_TICKET, $this->id);
        return parent::beforeDelete();
    }
    
    /** @Author: DungNT Sep 19, 2015
     *  @Todo: build detail of ticket to send email
     *  @Param: $model
     */
    public function getDetailIssue($mGasIssueTicketsDetail) {
        $res = '';
        $cmsFormater = new CmsFormatter();
        foreach($this->rTicketDetail as $mDetail){
            if($mDetail->id != $mGasIssueTicketsDetail->id){
                $uIdPost = $mDetail->rUidPost;
//                $name_user_login    = $cmsFormater->formatNameUser($uIdPost);
                $name_user_login    = $mDetail->getUidLoginByApp();
                $date_reply         = $cmsFormater->formatDate( $mDetail->created_date );
                $message            = $mDetail->getMessage();
                if(!empty($mDetail->move_to_uid)):
                    $res .= "<br> - <b>{$mDetail->getMoveToUid()}</b>";
                endif;
                $res .= "<br> - <b>$name_user_login - $date_reply:</b> $message";
            }
        }
        if($res != ""){
           $res = "<br><hr style='width:100%;'><b>+ Comment trước đây: </b> $res"; 
        }
        return $res;
    }
    
    /**
     * @Author: DungNT Now 22, 2015
     * @Todo: Api validate multi file
     * @param: $mBelongTo là model chứa error
     * ở đây là model GasIssueTickets
     */
    public static function ApiValidateFile(&$mBelongTo) {
    try{
        $ok=false; // for check if no file submit
        if(isset($_FILES['file_name']['name'])  && count($_FILES['file_name']['name'])){
            foreach($_FILES['file_name']['name'] as $key=>$item){
                $mFile = new GasFile('UploadFile');
                $mFile->file_name  = CUploadedFile::getInstanceByName( "file_name[$key]");
                $mFile->validate();
                if(!is_null($mFile->file_name) && !$mFile->hasErrors() ){
                    $ok=true;
                    MyFormat::IsImageFile($_FILES['file_name']['tmp_name'][$key]);
                    $FileName = MyFunctionCustom::remove_vietnamese_accents($mFile->file_name->getName());
                    if(strlen($FileName) > 100 ){
                        $mFile->addError('file_name', "Tên file không được quá 100 ký tự, vui lòng đặt tên ngắn hơn");
                    }
                }
                if($mFile->hasErrors()){
                    $mBelongTo->addError('file_name', $mFile->getError('file_name'));
                }
            }
        }
    } catch (Exception $ex) {
        throw new Exception($ex->getMessage());
    }
    }
    
    /**
     * @Author: DungNT Dec 10, 2015
     * @Todo: Api App set chief_monitor_id AND monitor_agent_id
     */
    public function setChiefMonitor($q, &$move_to_uid) {
        $aUpdate = [];
        if(!empty($q->chief_monitor_id)){
            $aUpdate[] = 'chief_monitor_id';
            $move_to_uid = $q->chief_monitor_id;
        }
        if(!empty($q->monitor_agent_id)){
            $aUpdate[] = 'monitor_agent_id';
            $move_to_uid = $q->monitor_agent_id;
        }
        if(!empty($q->accounting_id)){
            $aUpdate[] = 'accounting_id';
            $move_to_uid = $q->accounting_id;
        }
        if(count($aUpdate)){
            $this->update($aUpdate);
        }
    }
    
    /**
     * @Author: DungNT Dec 15, 2015
     * @Todo: Web set chief_monitor_id AND monitor_agent_id
     */
    public function setChiefMonitorOnWeb(&$move_to_uid) {
        $aUpdate = [];
        if(isset($_POST['GasIssueTickets']['chief_monitor_id']) && !empty($_POST['GasIssueTickets']['chief_monitor_id'])){
            $aUpdate[]              = 'chief_monitor_id';
            $move_to_uid            = $_POST['GasIssueTickets']['chief_monitor_id'];
            $this->chief_monitor_id = $move_to_uid;
        }
        
        if(isset($_POST['GasIssueTickets']['monitor_agent_id']) && !empty($_POST['GasIssueTickets']['monitor_agent_id'])){
            $aUpdate[]              = 'monitor_agent_id';
            $move_to_uid            = $_POST['GasIssueTickets']['monitor_agent_id'];
            $this->monitor_agent_id = $move_to_uid;
        }
        
        if(isset($_POST['GasIssueTickets']['accounting_id']) && !empty($_POST['GasIssueTickets']['accounting_id'])){
            $aUpdate[]              = 'accounting_id';
            $move_to_uid            = $_POST['GasIssueTickets']['accounting_id'];
            $this->accounting_id    = $move_to_uid;
        }
        if(count($aUpdate)){
            $this->update($aUpdate);
        }
    }
    
    /**
     * @Author: DungNT Now 24, 2015
     * @Todo: API save record multi file for one detail
     * @Param: $mGasIssueTickets GasIssueTickets
     * @Param: $mGasIssueTicketsDetail GasIssueTicketsDetail
     */
    public function apiSaveRecordFile($mGasIssueTicketsDetail) {
        $mRoot = GasIssueTickets::model()->findByPk($this->id); // để lấy created_date, không thể find rồi đưa vào biến $model dc vì còn liên quan đến get file
        set_time_limit(7200);
        if(isset($_FILES['file_name']['name'])  && count($_FILES['file_name']['name'])){
            foreach($_FILES['file_name']['name'] as $key=>$item){
                $mFile = new GasIssueTicketsDetailFile();
                $mFile->ticket_id               = $this->id;
                $mFile->issue_tickets_detail_id = $mGasIssueTicketsDetail->id;
                $created_date = explode(' ', $mRoot->created_date);
                $mFile->created_date            = $created_date[0];
                $mFile->order_number            = $key+1;
                $mFile->file_name               = CUploadedFile::getInstanceByName( "file_name[$key]");
                if(!is_null($mFile->file_name)){
                    $mFile->file_name  = self::saveFile($mFile, 'file_name', $key);
                    $mFile->save();
                    self::resizeImage($mFile, 'file_name');
                }
            }
        }
    }
    
    /** @Author: DungNT Jun 13, 2018
     *  @Todo: chuyển deadline to format save db
     **/
    public function convertDeadlineToDb() {
        if(strpos($this->deadline, '/')){
            $this->deadline = MyFormat::dateConverDmyToYmd($this->deadline);
            MyFormat::isValidDate($this->deadline);
        }
        if(strpos($this->renewal_date, '/')){
            $this->renewal_date = MyFormat::dateConverDmyToYmd($this->renewal_date);
            MyFormat::isValidDate($this->renewal_date);
        }
    }
    
    protected function beforeValidate() {
        if($this->isNewRecord){
            $this->created_date_only = date("Y-m-d");
            $mUserLogin = $this->rUidLogin;
            if($mUserLogin){
                $this->agent_id = $mUserLogin->parent_id;
            }
        }
        if($this->rCustomer){
            $this->sale_id          = $this->rCustomer->sale_id;
            $this->type_customer    = $this->rCustomer->is_maintain;
        }
        return parent::beforeValidate();
    }
    
    protected function beforeSave() { // Now 24, 2015
        return parent::beforeSave();
    }
    
    protected function afterSave() {
        if($this->isNewRecord){
            $this->addCaseErrors();
        }
        return parent::afterSave();
    }
    
    /**
    * @Author: DungNT Now 25, 2015
    * @Todo: get listing issue
    * @param: $q object post params
    * @param:  $mUser model user created
    */
    public function apiListing($q, $mUser) {
        $criteria = new CDbCriteria();
        $this->handleUserRole($criteria, $mUser, $q->type);
        if(trim($q->customer_id) != ''){
            $criteria->compare('t.customer_id', $q->customer_id);
        }
        
        if(trim($q->problem) != ''){
            $criteria->compare('t.problem', $q->problem);
        }
        
//      Close Oct0818 vì đã có ở hàm handleUserRole  $criteria->addCondition("t.uid_login = $mUser->id OR t.sale_id = $mUser->id OR t.chief_monitor_id = $mUser->id");
        $criteria->compare('t.status', $q->type);
        $criteria->compare('t.type', GasIssueTickets::TYPE_ISSUE);
        $criteria->order = 't.id DESC';
        $dataProvider=new CActiveDataProvider('GasIssueTickets',array(
            'criteria' => $criteria,
            'pagination'=>array(
                'pageSize' => GasAppOrder::API_LISTING_ITEM_PER_PAGE,
                'currentPage' => (int)$q->page,
            ),
          ));
        return $dataProvider;
    }
    
    /**
     * @Author: DungNT Dec 11, 2015
     * @Todo: xử lý load list cho từng loại user
     */
    public function handleUserRole(&$criteria, $mUser, $status) {
        $aUserViewAll   = GasConst::getAdminArray();
        $aUserViewAll[GasLeave::UID_DIRECTOR]               = GasLeave::UID_DIRECTOR;
        $aUserViewAll[GasLeave::UID_DIRECTOR_BUSSINESS]     = GasLeave::UID_DIRECTOR_BUSSINESS;
        $conditionSaleAdmin = '';
        if($mUser->id == GasLeave::UID_HEAD_GAS_BO){// Apr1119 xử lý a Đức xem đc của Sale Admin
            $uidSaleAdmin   = GasConst::UID_HA_PT;
            $sParamsIn      = implode(',', GasLeave::getArraySaleIdCompany());
            $conditionSaleAdmin = " OR t.uid_login = $uidSaleAdmin OR t.chief_monitor_id = $uidSaleAdmin OR t.sale_id IN ($sParamsIn)";
        }elseif($mUser->id == GasConst::UID_HA_PT){// Jun419 Open a Vinh cho Ha xu ly
            $uidSaleAdmin   = GasConst::UID_HA_PT;
            $sParamsIn      = implode(',', GasLeave::getArraySaleIdCompany());
            $conditionSaleAdmin = " OR t.uid_login = $uidSaleAdmin OR t.chief_monitor_id = $uidSaleAdmin OR t.sale_id IN ($sParamsIn)";
        }
        
        if(!in_array($mUser->id, $aUserViewAll) ){
            $criteria->addCondition("t.uid_login = $mUser->id OR t.sale_id = $mUser->id OR t.chief_monitor_id = $mUser->id $conditionSaleAdmin");
        }
        
        $cUid               = $mUser->id;
        $aUidCreatePlan     = $this->getArrayUidCreatePlan();
        $idGiaoViec         = GasIssueTickets::USER_PLAN;
        unset($aUidCreatePlan[GasLeave::UID_DIRECTOR]);
        unset($aUidCreatePlan[GasConst::UID_ADMIN]);
        $sParamsIn = implode(',',  $aUidCreatePlan);
        if (!$this->checkOpenAgent()) { // HaoNH
            if ($cUid == GasLeave::UID_DIRECTOR) {
//            $criteria->addCondition("t.uid_login = $mUser->id OR t.sale_id = $mUser->id OR t.chief_monitor_id = $mUser->id");
                $criteria->addCondition("t.is_user_plan<>$idGiaoViec ");
            } elseif (in_array($cUid, $this->getArrayUidCreatePlan())) {
                
            }
        }
    }

    public function getCustomer($field_name='') {
        $mUser = $this->rCustomer;
        if($mUser){
            if($field_name != ''){
                if($field_name == 'contact_person' ){
                    return (string)$mUser->getUserRefField('contact_person_name');
                }else{
                    return (string)$mUser->$field_name;
                }
            }else{
                return $mUser->first_name;
            }
        }
        return '';
    }
    public function getUidLogin() {
        $mUser = $this->rUidLogin;
        if($mUser){
            return $mUser->first_name;
        }
        return '';
    }
    /** @Author: DungNT Apr 11, 2019
     *  @Todo: xử lý ẩn tên NV post với các loại User khác
     **/
    public function getUidLoginShowOnWeb() {
        $cUid = MyFormat::getCurrentUid();
        $listdataCallcenter = CacheSession::getListdataCallcenter();
        $aUidShow = [$this->uid_login, GasLeave::UID_DIRECTOR, GasConst::UID_ADMIN];
        if(!in_array($cUid, $aUidShow) && array_key_exists($this->uid_login, $listdataCallcenter)){
            return 'User';
        }
        return $this->getUidLogin();
    }
    public function getSale() {
        $mUser = $this->rSale;
        if($mUser){
            return $mUser->first_name;
        }
        return '';
    }
    
    public function getSaleAndPhone() {
        $mUser = $this->rSale;
        if($mUser){
            return $mUser->first_name." - ".$mUser->phone;
        }
        return '';
    }
    
    public function getTitle() {
        return $this->title;
    }
    
    public function getStatus() {
        return GasIssueTickets::$STATUS_OPEN_CLOSE[$this->status];
    }
    
    /**
     * @Author: DungNT Dec 10, 2015
     * @Todo: get list tổng giám sát ở dưới view của APP
     */
    public function getApiListChiefMonitor() {
        $listMonitorAgent = Users::getChtmlByRoleAndField(ROLE_CHIEF_MONITOR);
        $temp = array(""=>"Chọn tổng giám sát");
        return $temp + $listMonitorAgent;
    }
    
    /**
     * @Author: DungNT Dec 15, 2015
     * @Todo: list nv giám sát đại lý 
     */
    public function getApiListMonitorAgent() {
        $aRole = array(ROLE_MONITOR_AGENT, ROLE_MONITORING_MARKET_DEVELOPMENT);
        $listMonitorAgent = Users::getChtmlByRoleAndField($aRole);
        $temp = array(""=>"Chọn chuyên viên");
        return $temp + $listMonitorAgent;
    }
    
    /**
     * @Author: DungNT Dec 20, 2015
     * @Todo: list nv kế toán
     */
    public function getApiAccounting() {
        $needMore = array('get_id_name'=>1);
        $aUserAccounting = GasOneManyBig::getArrayModelUser(GasOneManyBig::TYPE_ISSUE_ACCOUNTING, GasOneManyBig::TYPE_ISSUE_ACCOUNTING, $needMore);
        $temp = array(""=>"Chọn kế toán");
        return $temp + $aUserAccounting;
    }

    /**
     * @Author: DungNT Dec 15, 2015
     * @Todo: can view list tổng giám sát
     */
    public function canViewApiListChiefMonitor() {
        $aUidAllow = [GasConst::UID_HA_PT, GasLeave::UID_HEAD_GAS_BO];
        $cUid = MyFormat::getCurrentUid();
        if(in_array($cUid, $aUidAllow)){
            return true;
        }
        return in_array($cUid, $this->getArrayUidCreatePlan());
    }
    /**
     * @Author: DungNT Dec 15, 2015
     * @Todo: các action của 
     */
    public function canApplyLaw() {
        $aUidAllow = GasConst::getAdminArray();
        $aUidAllow[GasLeave::UID_DIRECTOR] = GasLeave::UID_DIRECTOR;
        $cUid = MyFormat::getCurrentUid();
        return in_array($cUid, $aUidAllow);
    }
    public function canApplyMeeting() {
        $cUid = MyFormat::getCurrentUid();
        $cRole = MyFormat::getCurrentRoleId();
        $aUidAllow = [GasConst::UID_NGOC_PT, GasConst::UID_ADMIN, GasLeave::UID_DIRECTOR];
        $aRoleAllow = [ROLE_MONITORING_MARKET_DEVELOPMENT, ROLE_ACCOUNTING, ROLE_ACCOUNTING_ZONE];
        if(in_array($cUid, $aUidAllow)){
            return true;
        }
        if(in_array($cRole, $aRoleAllow) && $this->uid_login == $cUid){
            return true;
        }
        return false;
    }
    public function showRenewalDate() {
        return true;
        $aRoleAllow = [ROLE_DIRECTOR];
        $cRole = MyFormat::getCurrentRoleId();
        return !in_array($cRole, $aRoleAllow);
    }
    
    /**
     * @Author: DungNT Dec 15, 2015
     * @Todo: can view list nv giám sát đại lý 
     */
    public function canViewApiListMonitorAgent() {
        $aRoleAllow = array(ROLE_CHIEF_MONITOR);
        $cRole = MyFormat::getCurrentRoleId();
        return in_array($cRole, $aRoleAllow);
    }
    
    /**
     * @Author: DungNT Dec 24, 2015
     * @Todo: can view list nv ke toan
     */
    public function canViewApiListAccounting() {
        $aRoleAllow = array(ROLE_DIRECTOR);
        $cRole = MyFormat::getCurrentRoleId();
        return in_array($cRole, $aRoleAllow);
    }

    /** @Author: DungNT Jun 19, 2018 **/
    public function getRoleCreatePlanOfEmployee() {
        return [ROLE_ADMIN, ROLE_DIRECTOR];
    }
    
    /** @Author: DungNT Jun 13, 2018
     *  @Todo: check user có thể tạo phần giao việc cho NV ở Phản Ánh không
     **/
    public function canCreatePlanOfEmployee() {
        $cUid = MyFormat::getCurrentUid();
        return in_array($cUid, $this->getArrayUidCreatePlan());
    }
    
    /**
     * @Author: DungNT Dec 15, 2015
     * @Todo: web check: với tổng GS + nv giám sát đại lý thì không cần ghi message
     * @return: true là required, false thì not required
     */
    public function canWriteReply() {
        return $this->requiredMessage(Yii::app()->user->role_id);
    }
    
    /**
     * @Author: DungNT Dec 10, 2015
     * @Todo: insert to table notify
     * @Param: $aUid array user id need notify
     */
    public function RunInsertNotify($aUid, $title) {
//        return ; // only for debug Jan 27, 2016
        $json_var = "";
        foreach($aUid as $uid) {
            if( !empty($uid) && $uid>0 ){
                                    // InsertRecord($user_id, $type, $obj_id, $time_send, $title, $json_var)
                GasScheduleNotify::InsertRecord($uid, GasScheduleNotify::ISSUE_TICKET, $this->id, "", $title, $json_var);
            }
        }
    }

    /**
     * @Author: DungNT Dec 19, 2015
     * @Todo: lấy id sale của customer 
     * @Param: $aUid array uid will send notify
     * nếu là KH của công ty thì notify về cho trưởng phòng bò/mối thôi
     */
    public function getSaleIdOfCustomer(&$aUid) {
        $this->getSaleIdOnly($aUid);
        $mCustomer = Users::model()->findByPk($this->customer_id);
        if(!empty($mCustomer) && $mCustomer->role_id != ROLE_CUSTOMER && !empty($this->customer_id)){
            $aUid[$this->customer_id] = $this->customer_id;
        }
    }
    
    /** @Author: DungNT Sep 04, 2018
     *  @Todo: get sale id of customer
     **/
    public function getSaleIdOnly(&$aUid) {
        $sale_id = 0;
        $aSaleCongTy = GasLeave::getArraySaleIdCompany();
        if(!in_array($this->sale_id, $aSaleCongTy) && !empty($this->sale_id)){
            $aUid[$this->sale_id] = $this->sale_id;
            $sale_id = $this->sale_id;
        }else{
            $sale_id = GasLeave::UID_HEAD_GAS_BO;
            $aUid[GasLeave::UID_HEAD_GAS_BO]    = GasLeave::UID_HEAD_GAS_BO;
        }
        if($this->sale_id == GasConst::UID_HOAN_NH){// Sep2519 Fix Sale Nguyễn Văn Đông [Chuyên Viên PTTT] - Gia Lai - thì chuyển cho Nguyễn Thị Kim Tuyết nữa
            $aUid[GasLeave::UID_VIEW_ALL_TAY_NGUYEN_GIA_LAI] = GasLeave::UID_VIEW_ALL_TAY_NGUYEN_GIA_LAI;
        }
        return $sale_id;
    }
    
    /** @NOTE các notify của 1 function nên viết liền nhau để còn review không bị sót
     * @Author: DungNT Dec 10, 2015
     * @Todo: build list notify cho Sale + trưởng phòng bò, mối, giám đốc KD 
     * khi có tạo mới ISSUE từ APP
     */
    public function ApiBuildNotifyS1() {
        // 1. list uid là: sale + Tp bò mối, GD kinh doanh
        $aUid = [GasLeave::UID_DIRECTOR_BUSSINESS => GasLeave::UID_DIRECTOR_BUSSINESS];// GD kinh doanh: Hoàn
        $this->getSaleIdOfCustomer($aUid);
        if($this->type_customer == STORE_CARD_KH_BINH_BO){
            $aUid[GasLeave::UID_HEAD_GAS_BO] = GasLeave::UID_HEAD_GAS_BO; // Trưởng Phòng KD Gas Bò
        }elseif($this->type_customer == STORE_CARD_KH_MOI){
            $aUid[GasLeave::UID_HEAD_GAS_MOI] = GasLeave::UID_HEAD_GAS_MOI; // Trưởng Phòng KD Gas Bò
        }
        $title = "Phản ánh: ".$this->getCustomer();
        $this->RunInsertNotify($aUid, $title);
    }    
    
    /**
     * @Author: DungNT Dec 10, 2015
     * @Todo: build list notify cho trưởng phòng bò, mối, giám đốc KD 
     * khi có Reply ISSUE từ APP
     * S2: sau khi có reply từ app của 
     */
    public function ApiBuildNotifyS2345() {
        // 1. list uid là: sale + Tp bò mối, GD kinh doanh
//        $aUid = array(GasLeave::UID_DIRECTOR_BUSSINESS);// GD kinh doanh: Hoàn
        $aUid = [];//
        $this->getSaleIdOfCustomer($aUid);
        $aStatusNotifyChiefMonitor = array(GasIssueTickets::STATUS_3_DIRECTOR, GasIssueTickets::STATUS_4_GIAM_SAT, GasIssueTickets::STATUS_5_DIRECTOR_REMOVE);
//        if(in_array($this->status_app, $aStatusNotifyChiefMonitor)){
            // chỗ này có thể add hết User Tổng Gs + Giám sát vào, nếu nó có thì OK hoặc empty thì khonong add
            if( !empty($this->chief_monitor_id)  ){
                $aUid[] = $this->chief_monitor_id; //Tổng giám sát Trần Trung Hiếu
//                $this->setExpired(GasIssueTickets::IS_NOT_EXPIRED);// cứ khác empty là ta set nó thôi
            }
            if( !empty($this->monitor_agent_id) ){
                $aUid[] = $this->monitor_agent_id; //Tổng giám sát Trần Trung Hiếu
            }
            if( !empty($this->accounting_id)  ){
                $aUid[] = $this->accounting_id; //Nv kế toán bất kỳ Dec 20, 2015
            }
            // không notify cho GĐ, chỉ khi nào close thì mới notify cho Giám Đốc
//        }
        
//        $aStatusNotifyChiefMonitor = array(GasIssueTickets::STATUS_5_DIRECTOR_REMOVE);

        $title = "Phản ánh: ".$this->getCustomer();
        $this->RunInsertNotify($aUid, $title);
    }
    
    /** @Author: DungNT Mar 30, 2018
     *  @Todo: gửi mail cho người liên quan đến phản ảnh
     **/
    public function mailAfterReply() {
        return ;// tạm close lại vì đã viết hàm send mail trước đây rồi, giờ không gửi thêm ở chỗ này nữa
//        $aUid = [];
        $aUid[GasConst::UID_DUNG_NT] = GasConst::UID_DUNG_NT;
        $this->getSaleIdOfCustomer($aUid);

//        if($this->type_customer == STORE_CARD_KH_BINH_BO){
//            $aUid[GasLeave::UID_HEAD_GAS_BO] = GasLeave::UID_HEAD_GAS_BO; // Trưởng Phòng KD Gas Bò
//        }elseif($this->type_customer == STORE_CARD_KH_MOI){
//            $aUid[GasLeave::UID_HEAD_GAS_BO] = GasLeave::UID_HEAD_GAS_BO; // Trưởng Phòng KD Gas Bò
//        }
        
        if(!empty($this->chief_monitor_id)){
            $aUid[$this->chief_monitor_id] = $this->chief_monitor_id; 
        }
        $info               = $this->mailAfterReplyGetContent();
        $needMore['title']  = 'Phản ánh: '.$this->title;
        $aModelUserMail     = Users::getOnlyArrayModelByArrayId($aUid);
        foreach ($aModelUserMail as $mUser) {
            if(empty($mUser->email)){
                continue ;
            }
            $needMore['list_mail'][] = $mUser->email;
        }
        SendEmail::bugToDev($info, $needMore);
    }
    
    /** @Author: DungNT Mar 30, 2018
     **/
    public function mailAfterReplyGetContent() {
        $html = "<table  cellpadding='0' cellspacing='0'> ";
        $html .= '<tbody>';
        $mTicketsDetail = new GasIssueTicketsDetail();
        $mTicketsDetail->ticket_id = $this->id;
        foreach($mTicketsDetail->getByTicketId() as $item):
            $name       = $item->getUidLogin();
            $time       = $item->getCreatedDate();
            $content    = $item->getMessage();
            $html .= '<tr>';
                $html .= "<td style='padding: 3px; border:1px solid #000; width:200px;'>".$name.'<br>'.$time."</td>";
                $html .= "<td style='padding: 3px; border:1px solid #000;'>{$content}</td>";
            $html .= '</tr>';
        endforeach;
        $html .= '</tbody>';
        $html .= '</table>';
        return $html;
    }

    /**
     * @Author: DungNT Dec 10, 2015
     * @Todo: build list notify khi Audit Close issue
     * khi có Close ISSUE từ APP
     */
    public function ApiBuildNotifyClose() {
        /* 1. list uid là: sale + Tp bò mối + Tong Giam Doc
         */
//        $aUid = array(GasLeave::UID_DIRECTOR_BUSSINESS);// GD kinh doanh: Hoàn
        $aUid = [];// 
        $this->getAllUidInIssue($aUid);
        // Có thể sẽ build notify cho kế toán ở bước Close này??? chờ feedback
        unset($aUid[GasLeave::UID_DIRECTOR]);
        $title = "[Đã xử lý] Phản ánh Close: ".$this->getTitle();
        $this->RunInsertNotify($aUid, $title);
    }
    
    /**
     * @Author: DungNT Dec 10, 2015
     * @Todo: build list notify khi issue Reopen
     * khi có issue Reopen từ APP
     */
    public function ApiBuildNotifyReopen() {
        /* 1. list uid là: sale + Tp bò mối + Tong Giam Doc
         */
//        $aUid = array(GasLeave::UID_DIRECTOR_BUSSINESS);// GD kinh doanh: Hoàn
        $aUid = [];// 
        $this->getSaleIdOfCustomer($aUid);
        if($this->type_customer == STORE_CARD_KH_BINH_BO){
            $aUid[GasLeave::UID_HEAD_GAS_BO] = GasLeave::UID_HEAD_GAS_BO; // Trưởng Phòng KD Gas Bò
        }elseif($this->type_customer == STORE_CARD_KH_MOI){
            $aUid[GasLeave::UID_HEAD_GAS_MOI] = GasLeave::UID_HEAD_GAS_MOI; // Trưởng Phòng KD Gas Bò
        }
        $title = "Phản ánh Reopen: ".$this->getCustomer();
        $this->RunInsertNotify($aUid, $title);
    }
    
    /**
     * @Author: DungNT Dec 11, 2015
     * @Todo: Receive notification when new issue was created OR Reply on issue
     * @param Array $aUid array(id)
     * @param String $mScheduleNotify model schedule notify
     */
    public function ApiSendNotify($aUid, $mScheduleNotify) {
        $DeviceAndroid  = UsersTokens::getListDeviceToken('gcm_device_token', $aUid, UsersTokens::LOGIN_ANDROID);
        $DeviceIos    = UsersTokens::getListDeviceToken('apns_device_token', $aUid, UsersTokens::LOGIN_IOS);
        $customerProperty = array(
            'notify_id' => $mScheduleNotify->id,
            'notify_type' => $mScheduleNotify->type,
            'type' => ApiNotify::TYPE_ISSUE,
            'id' => $this->id,
        );
//        $this->TestNotifyBB();// for test only Close when live
        ApiNotify::BuildToSend($DeviceAndroid, $mScheduleNotify->title, $customerProperty, YiiApnsGcm::TYPE_GCM);
        ApiNotify::BuildToSend($DeviceIos, $mScheduleNotify->title, $customerProperty, YiiApnsGcm::TYPE_APNS);
    }
    
    /**
     * @Author: DungNT Dec 10, 2015
     * @Todo: xác định trạng thái theo user reply cuối cùng
     * 1/ nếu nằm trong nhóm sale + Tp bò mối, GD kinh doanh thì là S2
     * 2/ nếu có chuyển cho Tổng GS thì nằm trong nhóm S3 tổng GS
     * 3/ nếu Tổng GS chuyển cho GS thì nằm trong nhóm S4 Gs
     * 4/ nếu là Giám Đốc thì nằm trong nhóm notify của Giám Đốc S5
     * @Param: $mUser là user post reply
     */
    public function updateStatusApp($mUser, $q) {
        if($this->applyPunish){
            $userId = !empty($this->oldChiefMonitorId) ? $this->oldChiefMonitorId : $this->chief_monitor_id;
            $this->addIssueError(EmployeeProblems::PROBLEM_DIRECTOR_PUNISH, $userId);// Jul0818 Anh bấm phạt 50k là bấm cho các lỗi trả lời không đúng, vì vậy em gán cho người trả lời trước chứ ko cho nhân viên dc chọn xử lý tiếp theo.
            $mUserPunish = Users::model()->findByPk($userId);
            $this->notifyPunishViaSms($mUserPunish, EmployeeProblems::PROBLEM_DIRECTOR_PUNISH);
        }
//        $aFieldUpdate = ['status_app', 'expired'];
        $UserRole   = $mUser->role_id;
        $aRoleS1    = array(ROLE_SALE, ROLE_HEAD_GAS_BO, ROLE_HEAD_GAS_MOI, ROLE_DIRECTOR_BUSSINESS);
        $aRoleS4    = array(ROLE_MONITOR_AGENT);
        $aRoleAccounting = array(ROLE_ACCOUNTING);
        $aRoleS5    = array(ROLE_DIRECTOR);
        $aStatusNotChangeWhenReply = [GasIssueTickets::STATUS_4_GIAM_SAT, GasIssueTickets::STATUS_6_ACCOUNTING];
        if($mUser->id != GasLeave::UID_DIRECTOR && in_array($this->status_app, $aStatusNotChangeWhenReply)){
//            Jul2518 khi GĐ trả lời ở 2 tab Ply và Công nợ CN thì chuyển lại tab NV xử lý, NV khác thì vẫn giữ ở tab cũ
            return ; // May 2618 khi chuyển qua Ply và Công nợ CN thì không move sang trạng thái khác khi có reply nữa
        }

        if( $mUser->id != GasLeave::UID_DIRECTOR && 
            ((empty($this->oldChiefMonitorId) && !empty($this->chief_monitor_id))
//          || ($this->problem == GasIssueTickets::PROBLEM_GV_BUSINESS && $this->uid_login == GasLeave::UID_DIRECTOR)// Now1118 xu ly User khac tra loi giao viec cua Giam Doc
        )){
            
            $this->status_app = GasIssueTickets::STATUS_2_SALE_REPLY;
        }elseif($this->applyLaw){
            $this->status_app = GasIssueTickets::STATUS_4_GIAM_SAT;
        }elseif($this->applyAccounting){
            $this->status_app   = GasIssueTickets::STATUS_6_ACCOUNTING;
            $this->msgHandle    = 'Chuyển công nợ cá nhân';// nội dung xử lý
            $this->emailAlertToAccounting($this->mDetail);
        }elseif($this->applyMonitor){
            $this->status_app = GasIssueTickets::STATUS_7_MONITOR;
        }elseif($this->applyQuickAction){
            $this->status_app = GasIssueTickets::STATUS_8_QUICK_ACTION;
            $this->expired = 0; // Jul0818 Khi đã quá hạn nếu anh chọn lại nhân viên xử lý khác thì người đó dc lập lại từ đầu quy trình nên phải chuyển về trạng thái ban đầu nhưng phải ưu tiên giải quyết sớm. 
        }elseif($this->detailCount > 1 && $UserRole != ROLE_DIRECTOR){// Khi NV xử lý trả lời chuyển lại giám đốc.
            $this->status_app = GasIssueTickets::STATUS_3_DIRECTOR;
            $this->answerByManager();
        }else{
//            if($this->status_app != GasIssueTickets::STATUS_7_MONITOR){// ko cho đổi trạng thái KH đang theo dõi
                $this->status_app = GasIssueTickets::STATUS_2_SALE_REPLY;
//            }// Jun2718 khi giám đốc trả lời ở KH đang theo dõi thì chuyển về NV xử lý, bỏ ra khỏi theo dõi
            if($mUser->id == GasLeave::UID_DIRECTOR){
                $this->expired = 0; // Jul0818 Khi đã quá hạn nếu anh chọn lại nhân viên xử lý khác thì người đó dc lập lại từ đầu quy trình nên phải chuyển về trạng thái ban đầu nhưng phải ưu tiên giải quyết sớm. 
                $this->calcRenewalDateAfterReply();
            }
        }

        $this->update();
    }
    
    /** @Author: DungNT Now 28, 2018
     *  @Todo: Quản lý giao việc chuyển cho 1 người khác - ko phải GĐ
     * Vẫn giữ ở tab NV xử lý, khi nào chuyển cho GĐ thì mới sang tab GĐ
     **/
    public function answerByManager() {
        if(!isset($_POST['GasIssueTickets']['chief_monitor_id']) || empty($_POST['GasIssueTickets']['chief_monitor_id'])){
            return ;
        }
        $new_chief_monitor_id = $_POST['GasIssueTickets']['chief_monitor_id'];
        $cUid = MyFormat::getCurrentUid();
        if($new_chief_monitor_id != GasLeave::UID_DIRECTOR){
            $this->status_app   = GasIssueTickets::STATUS_2_SALE_REPLY;
            $this->uid_login    = $cUid;
        }
    }
    
    /** @Author: DungNT Jun 27, 2018
     *  @Todo: khi giám đốc trả lời ở KH đang theo dõi thì chuyển về NV xử lý, bỏ ra khỏi theo dõi
     **/
    public function answerOfDirector() {
        $cRole = MyFormat::getCurrentRoleId();
        if($cRole != ROLE_DIRECTOR){
            return ;
        }
    }
    
    
    /**
     * @Author: DungNT Dec 21, 2015
     * @Todo: xác định trạng thái theo user reply cuối cùng
     * status_app from Web
     */
    public function updateStatusAppFromWeb($user_id) {
        $q = new stdClass();
        // nếu là giám đốc reply đầu tiên, đánh dấu cập nhật biến chief_monitor_id
//        if(empty($this->oldChiefMonitorId) && isset($_POST['GasIssueTickets']['chief_monitor_id']) && !empty($_POST['GasIssueTickets']['chief_monitor_id'])){
        if(isset($_POST['GasIssueTickets']['chief_monitor_id']) && !empty($_POST['GasIssueTickets']['chief_monitor_id'])){
//            $q['chief_monitor_id'] = $_POST['GasIssueTickets']['chief_monitor_id'];
            $q->chief_monitor_id = $_POST['GasIssueTickets']['chief_monitor_id'];
        }
        if(isset($_POST['GasIssueTickets']['monitor_agent_id']) && !empty($_POST['GasIssueTickets']['monitor_agent_id'])){
//            $q['monitor_agent_id'] = $_POST['GasIssueTickets']['monitor_agent_id'];
            $q->monitor_agent_id = $_POST['GasIssueTickets']['monitor_agent_id'];
        }
        if(isset($_POST['GasIssueTickets']['accounting_id']) && !empty($_POST['GasIssueTickets']['accounting_id'])){
//            $q['accounting_id'] = $_POST['GasIssueTickets']['accounting_id'];
            $q->accounting_id = $_POST['GasIssueTickets']['accounting_id'];
        }
        $mUser = Users::model()->findByPk($user_id);
        $this->updateStatusApp($mUser, $q);
    }
    
    /**
     * @Author: DungNT Dec 11, 2015
     * @Todo: check user có thể reply trên app không
     * @return: 1: ok, 0 not ok
     */
    public function canReplyOnApp($mUser) {
        if($this->status == GasTickets::STATUS_CLOSE){
            return '0';
        }
        $ok = '1';
        $UserRole = $mUser->role_id;
        $aRoleS1 = array(ROLE_SALE, ROLE_HEAD_GAS_BO, ROLE_HEAD_GAS_MOI, ROLE_DIRECTOR_BUSSINESS);
        $aStatusS1 = array( GasIssueTickets::STATUS_3_DIRECTOR,  GasIssueTickets::STATUS_4_GIAM_SAT);
        if(in_array($UserRole, $aRoleS1) && in_array($this->status_app, $aStatusS1)){
            // khi đã sang giai đoạn tổng GS rồi thì không cho sale + TP bò mối reply nữa, sang status of Giám đốc thì cho reply
            $ok = '0';
        }
        return $ok;
    }
    
    /**
     * @Author: DungNT Dec 11, 2015
     * @Todo: check user có thể Close trên app không
     * @return: 1: ok, 0 not ok
     */
    public function canCloseOnApp($mUser) {
        $ok = '0';
        $aRoleClose = array(ROLE_AUDIT, ROLE_ACCOUNTING, ROLE_CHIEF_MONITOR);
        $cUid = MyFormat::getCurrentUid();
//        $ok = '1'; // For test only
        if( in_array($mUser->role_id, $aRoleClose)){
            $ok = '1';
        }
        return $ok;
    }
    
    /**
     * @Author: DungNT Dec 11, 2015
     * @Todo: check user có thể Close trên app không
     * @return: 1: ok, 0 not ok
     */
    public function canReopenOnApp($mUser) {
        $ok = '0';
//        $ok = '1'; // For test only
        if($mUser->role_id == ROLE_DIRECTOR){
            $ok = '1';
        }
        return $ok;
    }
    
    /**
     * @Author: DungNT Dec 11, 2015
     * @Todo: check user có cần phải nhập message trên app không
     * @return: true là cần required, false là không required nhập
     */
    public function requiredMessage($role_id) {
//        $aRoleAllow = array(ROLE_AUDIT, ROLE_MONITOR_AGENT);
//        $aRoleAllow = array(ROLE_AUDIT); // Dec 24, 2015 van required message voi role ROLE_MONITOR_AGENT
        $aRoleAllow = []; // Dec 25, 2015 tạm thời required message với Audit để xử lý hết ticket cũ đi
        return !in_array($role_id, $aRoleAllow);
    }
    
    
    /**
     * @Author: DungNT Dec 11, 2015
     * @Todo: check user có cần phải nhập message trên app không
     * @return: 1: ok, 0 not ok
     */
    public function canRequiredMessageOnApp($role_id) {
        $ok = '0';
//        return $ok = '1'; // hiện tại chưa xử lý chỗ này
        // vì còn phải xử lý ở Server validate nữa, chỗ này đang chỉ cho app 
        if($this->requiredMessage($role_id)){
            $ok = '1';
        }
        return $ok;
    }
    
    /**
     * @Author: DungNT Dec 20, 2015
     */
    public function setExpired($expired) {
        $this->expired = $expired;
        $this->update(array('expired'));
    }
    
    /**
     * @Author: DungNT Dec 20, 2015
     * chạy cron khoảng 2 tiếng 1 lần để set expired cho record Issue Ticket
     * Rule => Tính 5 ngày cho issue mới, NV trả lời xong cũng quét 5 ngày 
     * @param: $today Y-m-d
     */
    public function cronSetExpired($today) {
//        return ;// Mar2818 tạm close lại vì không chạy flow này nữa
        $daysCheck = $this->daysHandleJob; $total = 0; $daysQuick = 3;
        
        // 1. Tính 5 ngày cho issue mới
        $criteria = new CDbCriteria();
        $criteria->compare('t.status_app', GasIssueTickets::STATUS_1_NEW_SALE);
        $this->cronAddSameCriteria($criteria);
//        $criteria->addCondition(" DATE_ADD(created_date,INTERVAL $daysCheck DAY) < NOW()");// Close Jun0218, fix sql
        $criteria->addCondition(" DATE_ADD(DATE(t.last_reply),INTERVAL $daysCheck DAY) < '$today'");
        $models = GasIssueTickets::model()->findAll($criteria);
        $aId = CHtml::listData($models, 'id', 'id');
//        $this->setExpiredAuto($aId);
        $this->setExpiredAutoV2($models);
        $total += count($aId);
        
        // 2. NV trả lời xong cũng quét 5 ngày 
        $criteria = new CDbCriteria();
        $criteria->compare('t.status_app', GasIssueTickets::STATUS_2_SALE_REPLY);
        $this->cronAddSameCriteria($criteria);
        $criteria->addCondition("DATE_ADD(DATE(t.last_reply),INTERVAL $daysCheck DAY) < '$today'");
        $models = GasIssueTickets::model()->findAll($criteria);
        $aId = CHtml::listData($models, 'id', 'id');
        $this->setExpiredAutoV2($models);
        $total += count($aId);
        
        if(count($aId)){
            $ResultRun = "CRON Set Expired Phản Ánh quá $daysCheck ngày: ".$total." record";
            Logger::WriteLog($ResultRun);
        }
        
        // 3. quét cảnh báo trước 2 ngày với những issue chuẩn bị hết hạn
        $daysAlert = $daysCheck - 2;
        $criteria = new CDbCriteria();
        $criteria->compare('t.status_app', GasIssueTickets::STATUS_2_SALE_REPLY);
        $this->cronAddSameCriteria($criteria);
        $criteria->addCondition("DATE_ADD(DATE(t.last_reply),INTERVAL $daysAlert DAY) = '$today'");
        $models = GasIssueTickets::model()->findAll($criteria);
        $aId = CHtml::listData($models, 'id', 'id');
        foreach($models as $mIssueTickets){
            $mIssueTickets->alertExpiredSendEmail();
        }
        $totalAlert = count($aId);
        
        if(count($aId)){
            $ResultRun = "CRON Alert Alert Alert Alert Expired Phản Ánh quá $daysAlert ngày: ".$totalAlert." record";
            Logger::WriteLog($ResultRun);
        }
        
        // 4. Jul0818 tab xử lý gấp quét 3 ngày
        $criteria = new CDbCriteria();
        $criteria->compare('t.status_app', GasIssueTickets::STATUS_8_QUICK_ACTION);
        $this->cronAddSameCriteria($criteria);
        $criteria->addCondition("DATE_ADD(DATE(t.last_reply),INTERVAL $daysQuick DAY) < '$today'");
        $models = GasIssueTickets::model()->findAll($criteria);
//        $this->setExpiredAutoV2($models); // Now0918 tạm close lại, chuyển sang Button Meeting
        
        // 5. quét deadline + renewal_date bên tab NV xử lý cũng quét 5 ngày 
        $criteria = new CDbCriteria();
        $this->cronAddCriteriaRenewal($criteria);
        $criteria->addCondition("t.renewal_date = '$today'");
//        chỉ quét 1 lần giá trị = CURDATE() => nên nếu không thể xử lý cho anh Hoàn được E1801439 =>  renewal_date chir $criteria->addCondition("DATE_ADD(DATE(t.last_reply),INTERVAL $daysCheck DAY) < CURDATE()");// Oct0218 xử lý trường hợp GĐ mới trả lời và đến ngày  renewal_date, sẽ không quét renewal_date mà theo rule 6 ngày từ khi trả lời
        $models = GasIssueTickets::model()->findAll($criteria);
        $aId = CHtml::listData($models, 'id', 'id');
        $this->setExpiredAutoV2($models);
        $total += count($aId);
        
        // 6. quét cảnh báo trc 2 ngày deadline + renewal_date
        $daySubtract = 2;
        $criteria = new CDbCriteria();
        $this->cronAddCriteriaRenewal($criteria);
        $criteria->addCondition("DATE_SUB(t.deadline, INTERVAL $daySubtract DAY) = '$today' OR DATE_SUB(t.renewal_date, INTERVAL $daySubtract DAY) = '$today'");
        $models = GasIssueTickets::model()->findAll($criteria);
        foreach($models as $mIssueTickets){
            $mIssueTickets->alertExpiredSendEmail();
        }

        if(count($aId)){
            $ResultRun = "CRON Set Expired quét deadline + renewal_date Phản Ánh quá ngày: ".$total." record";
            Logger::WriteLog($ResultRun);
        }
    }
    
    /** @Author: DungNT Jul 10, 2018
     *  @Todo: cronAddSameCriteria
     **/
    public function cronAddSameCriteria(&$criteria) {
        $criteria->addCondition("t.deadline IS NULL AND (t.renewal_date IS NULL OR t.renewal_date='0000-00-00')");
        $criteria->compare('t.status', GasTickets::STATUS_OPEN);
        $criteria->compare('t.expired', GasIssueTickets::IS_NOT_EXPIRED);
    }
    
    public function cronAddCriteriaRenewal(&$criteria) {
        $aScan = [GasIssueTickets::STATUS_2_SALE_REPLY, GasIssueTickets::STATUS_7_MONITOR];
        $criteria->addInCondition('t.status_app', $aScan);
        $criteria->compare('t.status', GasTickets::STATUS_OPEN);
        $criteria->compare('t.expired', GasIssueTickets::IS_NOT_EXPIRED);
    }

    /** @Author: DungNT May 04, 2018
     *  @Todo: update set expired multi record with array id
     **/
    public function setExpiredAuto($aId) {
        if(count($aId) < 1){
            return ;
        }
        $criteria = new CDbCriteria();
        $sParamsIn = implode(',', $aId);
        $criteria->addCondition("id IN ($sParamsIn)");
        $aUpdate = array('expired' => self::IS_EXPIRED);
        GasIssueTickets::model()->updateAll($aUpdate, $criteria);
    }
    
    /** @Author: DungNT Jun 03, 2018
     *  @Todo: update set expired multi record with array model
     * tách hàm mới để chèn thêm action khi record expired
     **/
    public function setExpiredAutoV2($aModel) {
        $aUidNotScan = [GasConst::UID_DUNG_NT, GasLeave::UID_HEAD_OF_LEGAL];
        foreach($aModel as $mIssueTicket){
            if(in_array($mIssueTicket->chief_monitor_id, $aUidNotScan)){
                continue ;// Now2918 tạm ko scan, sẽ open sau
            }
            $mIssueTicket->expired = GasIssueTickets::IS_EXPIRED;
            $mIssueTicket->update(['expired']);
            $mIssueTicket->addIssueError(EmployeeProblems::PROBLEM_EXPIRED, $mIssueTicket->chief_monitor_id);
            $mIssueTicket->emailAnnounceExpired();
        }
    }
    
    /** @Author: DungNT Jun 03, 2018
     *  @Todo: insert to table lỗi phạt record quá hạn
     * Jun2718 -> phạt với NV xử lý đc chuyển issue
     **/
    public function addIssueError($object_type, $employee_id) {
        // return nếu record quá hạn ko biết phạt cho ai
        $model = new EmployeeProblems();
        $model->employee_id     = $employee_id;
        $model->object_id       = $this->id;
        $model->object_type     = $object_type;
        $model->insertToEmployeeProblems();
    }
    
    /** @Author: DungNT Jun 03, 2018
     *  @Todo: 6/ Hệ thống nào sang quán, 2 nhà cc, gỡ hệ thống (thêm mới vào mục nguyên nhân) thì treo công nợ cho NV kinh doanh 10 tr 1 cái.
     **/
    public function addCaseErrors() {
        return ;// Jun2718 không xử lý phần này nữa, để kế toán tự theo dõi trừ lương
        $aProblemCheck = [GasIssueTickets::PROBLEM_SANG_QUAN,
            GasIssueTickets::PROBLEM_NHA_CC,
            GasIssueTickets::PROBLEM_REMOVE_SYSTEM,
        ];
        if(!in_array($this->problem, $aProblemCheck) || empty($this->sale_id)){
            return ;
        }
        
        $model = new EmployeeProblems();
        $model->employee_id     = $this->sale_id;
        $model->object_id       = $this->id;
        $model->object_type     = $this->problem;
        $model->insertToEmployeeProblems();
    }

    
    
    
    
    
    /** @Author: DungNT Apr 05, 2017
     *  @Todo: Cron tự động chuyển cho tổng GS khi quá hạn 3 ngày
     */
    public function autoRepyByCron() {
        $_POST['GasIssueTickets']['chief_monitor_id'] = GasLeave::PHUC_HV;
        $this->saveRecordDetail(GasConst::UID_ADMIN);
        $this->updateStatusTicket(GasTickets::PROCESS_STATUS_FINISH, GasConst::UID_ADMIN);
        $this->updateStatusAppFromWeb(GasConst::UID_ADMIN);// 1. phai update status_app thì mới send notify đúng được
        $this->ApiBuildNotifyS2345();// 2. build send notify cho từng user
    }
    
    /** @Author: DungNT Jun 21, 2018
     *  @Todo: get record expired after renew date
     * @param: $curDate Y-m-d
     **/
    public function getExpiredAfterRenewDate($curDate) {
        $days_check = 1;// quá ngày gia hạn 1 ngày thì set lỗi
        $criteria = new CDbCriteria();
        $criteria->addCondition("DATE_ADD(renewal_date,INTERVAL $days_check DAY) = '{$curDate}'");
        return GasIssueTickets::model()->findAll($criteria);
    }
    
    /** @Author: DungNT Mar 28, 2018
     *  @Todo: move issue to Plý
     **/
    public function applyLaw() {
        $this->status_app = GasIssueTickets::STATUS_4_GIAM_SAT;
        $this->update(['status_app']);
    }
    /** @Author: DungNT Apr 09, 2018
     *  @Todo: Check can reply, giám đốc phải là người reply đầu tiên, không ai đc trả lời trc
     **/
    public function canReplyFirst() {
//        return true;// Apr1418 cho phép các user khác reply, không check role giám đốc nữa
        // Jul1618 phải để GĐ trả lời đầu tiên cho all issue
        $ok = true;
        $cRole = MyFormat::getCurrentRoleId();
        $cUid = MyFormat::getCurrentUid();
        $aRoleAllow = [ROLE_DIRECTOR];
        if(in_array($cRole, $aRoleAllow)){
            return true;
        }
        if($this->status_app == GasIssueTickets::STATUS_1_NEW_SALE && $this->problem != GasIssueTickets::PROBLEM_GV_BUSINESS && !in_array($this->uid_login, $this->getArrayUidCreatePlan()) &&  $this->detailCount == 1){
            $ok = false;
        }
        return $ok;
    }
    
    /** @Author: DungNT May 05, 2018
     *  @Todo: Scan KH cần theo dõi. đưa ra 1 list, hệ thống sẽ quét nếu trong 1 tháng không lấy hàng thì notify cho: sếp, phòng Kinh Doanh
     **/
    public function scanCustomerNeedMonitor() {
        return ;// close Jul2618 vì thấy ko cần thiết, sẽ open nếu có yêu cầu sau
        $day_add        = 30;
        $aIssueTicket   = $this->getCustomerNeedMonitor();
        foreach($aIssueTicket as $mIssueTicket){
            $mCustomer = $mIssueTicket->rCustomer;
            if(empty($mCustomer) || $mCustomer->role_id != ROLE_CUSTOMER){
                continue ;
            }
            // get đơn hàng cuối của customer, nếu đơn đó hơn 30 ngày thì mail cảnh báo
            $mStoreCard = GasStoreCard::GetLastPurchase($mCustomer->id);
            if(empty($mStoreCard)){
                continue ;
            }
            $dateCheck  = MyFormat::modifyDays($mStoreCard->date_delivery, $day_add);
            if(MyFormat::compareTwoDate(date('Y-m-d'), $dateCheck)){
                $mIssueTicket->sendmailCustomerNeedMonitor($mCustomer, $mStoreCard, $day_add);
            }
        }
    }
    // belong to scanCustomerNeedMonitor
    public function getCustomerNeedMonitor() {
        $criteria = new CDbCriteria();
        $criteria->compare('t.status_app', GasIssueTickets::STATUS_7_MONITOR);
        $criteria->compare('t.status', GasTickets::STATUS_OPEN);
        return self::model()->findAll($criteria);
    }
    // gửi email cảnh báo đến bộ phận liên quan KH cần theo dõi không lấy hàng
    public function sendmailCustomerNeedMonitor($mCustomer, $mStoreCard, $day_add) {
        if(empty($mCustomer)){
            return ;
        }
        $saleName = $mCustomer->sale ? $mCustomer->sale->first_name : '';
        $needMore               = ['title'=> "[Cảnh báo không lấy hàng] Phản ánh: $this->title"];
//        $needMore['SendNow']    = 1;// only dev debug
//        $needMore['list_mail']  = ['dungnt@spj.vn'];
        $needMore['list_mail']  = ['ketoan.vanphong@spj.vn', 'kinhdoanh@spj.vn', 'dungnt@spj.vn'];
        $body = "Khách hàng trong danh sách theo dõi của <B>PHẢN ÁNH SỰ VIỆC</B> không lấy hàng $day_add ngày<br>";
        $body .= "<br><br>Khách hàng: <b>$mCustomer->first_name</b>";
        $body .= "<br><br>Địa chỉ: $mCustomer->address";
        $body .= "<br><br>NV kinh doanh: $saleName";
        $body .= "<br><br>Đơn hàng mới nhất: ". MyFormat::dateConverYmdToDmy($mStoreCard->date_delivery, 'd/m/Y');
        $body .= "<br><br>Các bộ phận liên quan kiểm tra lại";
        SendEmail::bugToDev($body, $needMore);
    }
        
    /** @Author: DungNT Jun 21, 2018
     *  @Todo: get info customer and uid post to email
     **/
    public function sendEmailAlert($mGasIssueTicketsDetail, $aEmail, $needMore = []) {
        if(is_null($mGasIssueTicketsDetail)){
            return ;
        }
        $mCustomer  = $this->rCustomer;
        $first_name = $address = '';
        if(!empty($mCustomer)){
            $first_name = $mCustomer->first_name;
            $address    = $mCustomer->address;
            
        }
        $titleHead  = "[$this->code_no] Cập nhật mới phản ánh sự việc: ".$this->title.'<br><br>';
        if(!empty($this->msgHandle)){
            $titleHead  .= "<b>Xử lý: {$this->msgHandle} </b><br><br>";
        }
        
        $body = $titleHead;
        if(!empty($this->deadline)){
            $body .= "<br><b>Deadline: </b> {$this->getDeadline()}";
        }
        if(!empty($this->renewal_date) && $this->renewal_date != '0000-00-00'){
            $body .= "<br><b>Nhân viên xin gia hạn: </b> {$this->getRenewalDate()}";
        }
        $body .= "<br><b>Khách hàng: </b> {$first_name}";
        $body .= "<br><b>Địa chỉ: </b> {$address}";
        $body .= "<br><b>Người cập nhật: </b> {$mGasIssueTicketsDetail->getUidLoginByApp()}";
        if(!empty($mGasIssueTicketsDetail->move_to_uid)): 
            $body .= "<br><b>{$mGasIssueTicketsDetail->getMoveToUid()}</b>";
        endif;
        $body .= "<br><b>Nội dung: </b> {$mGasIssueTicketsDetail->getMessage()}";

        $body .= $this->getDetailIssue($mGasIssueTicketsDetail);
        
        $needMore['title']          = isset($needMore['title']) ? $needMore['title'] : 'Phản Ánh: '.$this->title;
        $needMore['list_mail']      = $aEmail;
//        $needMore['list_mail'][]    = 'dungnt@spj.vn';
//        $needMore['SendNow']    = 1;// only dev debug
//        $needMore['list_mail']  = ['dungnt@spj.vn'];

        SendEmail::bugToDev($body, $needMore);
    }
    
    /** @Author: DungNT Jun 19, 2018
     *  @Todo: Mail cảnh báo gửi cho NV kinh doanh và phòng kế toán theo dõi công nợ cá nhân
     **/
    public function emailAlertToAccounting($mGasIssueTicketsDetail) {
        if(empty($mGasIssueTicketsDetail)){
            Logger::WriteLog("NEED CHECK PHản ÁNH SỰ VIỆC EMPTY MODEL mGasIssueTicketsDetail id: $this->id");
            return ;
        }
        // lấy email của sale và người đc chuyển cuối cùng 
        $aUid = $aEmail = [];
        $this->getListEmailInThisIssue($aUid, $aEmail);
        $aEmail[]   = 'ketoan.vanphong@spj.vn';
        $this->sendEmailAlert($mGasIssueTicketsDetail, $aEmail);
    }
    
    /** @Author: DungNT Jun 19, 2018
     *  @Todo: Mail thông báo khi bị phạt quá hạn
     **/
    public function emailAnnounceExpired() {
        $mGasIssueTicketsDetail = $this->getLatestReply();
        if(empty($mGasIssueTicketsDetail)){
            Logger::WriteLog("NEED CHECK PHản ÁNH SỰ VIỆC EMPTY MODEL mGasIssueTicketsDetail id: $this->id");
            return ;
        }
        $userIdSelect = $this->sale_id;// với issue mới thì lấy sale id
        if(!empty($this->chief_monitor_id)){// nếu có người xử lý thì chọn
            $userIdSelect = $this->chief_monitor_id;
        }
        if(empty($userIdSelect)){
            return ;
        }
        // lấy email của sale và người đc chuyển cuối cùng 
        $aUid = $aEmail = [];
        $aUid[$userIdSelect]     = $userIdSelect;
        $this->getSaleIdOnly($aUid);
        $needMore   = array('aUid'=>$aUid);
        $aUser      = Users::GetListUserMail($needMore);
        
        $mEmployeeProblems  = new EmployeeProblems();
        $aTypeToMoney       = $mEmployeeProblems->getArrayMoneyType();

        foreach($aUser as $mUser){
            $aEmail[] = $mUser->email;
            $this->msgHandle = "Phạt $mUser->first_name quá hạn chưa xử lý phản ánh sự việc: ".ActiveRecord::formatCurrency($aTypeToMoney[EmployeeProblems::PROBLEM_EXPIRED]);// nội dung xử lý
            $this->notifyPunishViaSms($mUser, EmployeeProblems::PROBLEM_EXPIRED);
        }
        if(count($aUser) == 0){
            SendEmail::bugToDev("Function emailAnnounceExpired fix User empty email user_id = $userIdSelect");
        }
        $aEmail[]           = 'ketoan.vanphong@spj.vn';
        $needMore['title']  =  '[Phạt] Phản ánh: '.$this->title;
        $this->sendEmailAlert($mGasIssueTicketsDetail, $aEmail, $needMore);
    }
    
    
    /** @Author: DungNT Jul 08, 2018
     *  @Todo: send sms khi bị phạt, hoặc 1 sự kiện khác
     **/
    public function notifyPunishViaSms($mUser, $typePunish) {
        if(empty($mUser)){
            return ;
        }
        $mEmployeeProblems  = new EmployeeProblems();
        $aTypeToMoney       = $mEmployeeProblems->getArrayMoneyType();
        $smsContent = "Phat: {$mUser->first_name} " . ActiveRecord::formatCurrency($aTypeToMoney[$typePunish]);
        if($typePunish == EmployeeProblems::PROBLEM_EXPIRED){
            $smsContent .= "\nLy do: qua han phan anh su viec";
        }elseif($typePunish == EmployeeProblems::PROBLEM_DIRECTOR_PUNISH){
            $smsContent .= "\nLy do: Giam Doc phat phan anh SV";
        }
        $smsContent .= "\nMa so: {$this->code_no}";
        $smsContent .= "\nChi tiet vui long xem tren Web noibo.daukhimiennam.com va Email";
        $this->doSendSms($smsContent, $mUser->id);
    }
    
    /** @Author: DungNT Jul 08, 2018
     *  @Todo: send sms khi bị phạt
     **/
    public function doSendSms($smsContent, $toUserId) {
        $mSms = new GasScheduleSms();
        $mSms->phone = $this->getPhoneUserOnlyOneNumber($toUserId);
        if(empty($mSms->phone)){
            return null;
        }
        $mSms->title = $smsContent;
        $mSms->purgeMessage();
        
        $mSms->uid_login        = 2;
        $mSms->user_id          = $toUserId;
        $mSms->type             = GasScheduleSms::TYPE_ANNOUNCE_INTERNAL;
        $mSms->obj_id           = $this->id;
        $mSms->content_type     = GasScheduleSms::ContentTypeKhongDau;
        $mSms->time_send        = '';
        $mSms->json_var         = [];
        $cHours = date("H");
        if($cHours < 8 ) {// nếu là buổi tối thì chờ đến 8h sáng mới bắt đầu gửi tin
            $mSms->time_send        = date('Y-m-d').' 08:30:01';
        }
//        $mSms->time_send        = date('Y-m-d H:i:s');
        $schedule_sms_id        = $mSms->fixInsertRecord();

//        $mScheduleSms = GasScheduleSms::model()->findByPk($schedule_sms_id);
//        $mScheduleSms->doSend();// only for test
    }
    
    /** @Author: PHAM THANH NGHIA 2018
     *  @Todo: show list gas issue tickets
     *  @Code: NGHIA002
     *  @Param: 
     *  
     **/
    public function search(){
        $criteria= new CDbCriteria;

        $criteria->compare('t.uid_login',$this->uid_login);
        $criteria->compare('t.send_to_id',$this->send_to_id);
        $criteria->compare('t.code_no',$this->code_no);
       
        if(!empty($this->date_from)){
            $date_from = MyFormat::dateDmyToYmdForAllIndexSearch($this->date_from);
        }
        
        if(!empty($this->date_to)){
            $date_to = MyFormat::dateDmyToYmdForAllIndexSearch($this->date_to);
        }
        if(!empty($date_from) && !empty($date_to)){
            $criteria->addBetweenCondition("DATE(t.created_date)",$date_from,$date_to);
        }
        $criteria->order = 't.process_time DESC';
        
        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
            'pagination'=>array(
                'pageSize'=> '10',
            ),
        ));
    }

    public function getUidLoginName() {
        $mUser = $this->rUidLogin;
        if($mUser){
            return $mUser->first_name;
        }
        return '';
    }
    
    public function getProcessUserId() {
        $mUser = $this->rProcessUserId;
        if($mUser){
            return $mUser->first_name;
        }
        return '';
    }
    public function canUpdateWeb(){
        return false;
    }
    
    public function getCodeNo() {
        return $this->code_no;
    }
    
    public function getCreatedDateTickets(){
        return MyFormat::dateConverYmdToDmy($this->created_date, "d/m/Y H:i");
    }
    
    /** @Author: DungNT Jun 28, 2018
     *  @Todo: get list reply detail of issue
     **/
    public function getListMessage(){
        $criteria= new CDbCriteria;
        $criteria->compare('t.ticket_id',$this->id);
        $criteria->order = 't.id DESC';
        $result = GasIssueTicketsDetail::model()->findAll($criteria);
        $message = [];
        foreach($result as $mDetail){
            $textMoveToUid = $textErrors = '';
            if(!empty($mDetail->move_to_uid)):
                $textMoveToUid = '<b>'.$mDetail->getMoveToUid().' </b>';
            endif;
            if(!empty($mDetail->employee_problems_id)):
                $textErrors = '<br><b>'.$mDetail->getProblemOnGrid().' </b>';
            endif;
            
            $user = Users::model()->findByPk($mDetail->uid_post);
            if(!empty($user) && !empty($mDetail->message)){
                $message[] =  $user->first_name ." - ".$mDetail->getCreatedDate(). ":  $textMoveToUid $textErrors {$mDetail->getRenewalDateOnGrid()} " . $mDetail->getMessage();
            }
        }
        return implode("<br> ----------------------------------------------- <br>",$message);
    }
    
    /** @Author: DungNT Oct 16, 2018
     *  @Todo: check nếu GĐ trả lời mà bị quá hoặc gần đến ngày gia hạn < 6 ngày
     * thì xử lý cộng từ ngày hiện tại thêm để đúng quy trình 6 ngày 
     * Lấy ngày hiện tại + 6 ngày nếu lớn hơn ngày gia hạn thì renewal_date = today + 6
     * @aa: chỉ update biến renewal_date ở root để không bị quét nhầm
     **/
    public function calcRenewalDateAfterReply() {
        if(empty($this->renewal_date) || $this->renewal_date == '0000-00-00'){
            return ;
        }
        $today = date('Y-m-d');
        $nextDateExpired = MyFormat::modifyDays($today, $this->daysHandleJob);
        if(MyFormat::compareTwoDate($nextDateExpired, $this->renewal_date)){
            $this->renewal_date = $nextDateExpired;
        }
    }
    
    /** @Author: DungNT May 24, 2018
     *  @Todo: get param post renewal_date
     **/
    public function setRenewalDate() {
        if(isset($_POST['GasIssueTickets']['renewal_date']) && !empty($_POST['GasIssueTickets']['renewal_date'])){
            $this->renewal_date = $_POST['GasIssueTickets']['renewal_date'];
            $this->renewal_date = MyFormat::dateConverDmyToYmd($this->renewal_date);
            $this->isSaveRenewalDate = true;
        }else{
            $this->renewal_date = $this->modelOld->renewal_date;
        }
        $this->approveDeadline($this->renewal_date); //HaoNH 2019 
    }
    public function getRenewalDate() {
        if(empty($this->renewal_date) || $this->renewal_date == '0000-00-00'){
            return '';
        }
        return MyFormat::dateConverYmdToDmy($this->renewal_date);
    }
    public function getRenewalDateOnGrid() {
        $res = $this->getRenewalDate();
        if(empty($res)){
            return '';
        }
        return "[Gia hạn $res]";
    }
    public function getDeadline() {
        if(empty($this->deadline) || $this->deadline == '0000-00-00'){
            return '';
        }
        return MyFormat::dateConverYmdToDmy($this->deadline);
    }
    public function getDeadlineOnGrid() {
        $res = $this->getDeadline();
        if(empty($res)){
            return '';
        }
        return "[Deadline $res]";
    }
    
    /** @Author: DungNT Jun 03, 2019
     *  @Todo: title phân biệt item xử lý với item tạo của NV
     **/
    public function getTextFocusHandleOnGrid() {
        $cUid = MyFormat::getCurrentUid();
        if($this->chief_monitor_id != $cUid || $this->status == GasTickets::STATUS_CLOSE){
            return '';
        }
        return "<b><span class='color_red'>[Xử lý]</span></b> ";
    }
    
    /** @Author: DungNT Oct 16, 2018
     *  @Todo: check user can view detail 
     **/
    public function canViewDetail() {
        $cUid = MyFormat::getCurrentUid();
        $aUid = CHtml::listData($this->rTicketDetail, 'uid_post', 'uid_post');
        $this->getSaleIdOnly($aUid);
        return in_array($cUid, $aUid);
    }
    
    /** @Author: DungNT Jun 27, 2018
     *  @Todo: get lastest reply 
     **/
    public function getLatestReply() {
        $criteria = new CDbCriteria;
        $criteria->addCondition('t.ticket_id=' . $this->id);
        $criteria->order = "t.id DESC";
        $criteria->limit = 1;
        return GasIssueTicketsDetail::model()->find($criteria);
    }
    
    /** @Author: DungNT Jul 25, 2018
     *  @Todo: get lastest issue of customer
     *  @note: khi tạo mới, sẽ tìm Phản ánh cũ của KH để đưa phản ánh mới vào cùng 1 topic
     *  1 KH sẽ chi có 1 issue, sẽ thay tiêu đề theo cái mới
     **/
    public function getLatestIssue() {
        if(empty($this->customer_id)){
            return null;
        }
        $criteria = new CDbCriteria;
        $criteria->addCondition('t.customer_id=' . $this->customer_id);
        $criteria->order = "t.id DESC";
        $criteria->limit = 1;
        return GasIssueTickets::model()->find($criteria);
    }
    
    /** @Author: DungNT Jun 25, 2018
     *  @Todo: check là role KH + đã có issueOld mới map issue cũ vào
     **/
    public function canMapIssueOld() {
        $ok = false;
        $mCustomer = $this->rCustomer;
        if($mCustomer->role_id == ROLE_CUSTOMER){
            $ok = true;
        }
        $this->modelOld = $this->getLatestIssue();
        if(empty($this->modelOld)){
            $ok = false;
        }
        return $ok;
    }
    
    /** @Author: DungNT Jun 25, 2018
     *  @Todo: check là role KH + đã có issueOld mới map issue cũ vào
     **/
    public function doMapIssueOld() {
        /* 1. change title, problem of IssueOld
         * 2. set Open, remove expired, đưa về tab Issue New
         * 3. save record detail + save file
         */
        // 1. change title, problem of IssueOld
        $this->modelOld->title      = $this->title;
        $this->modelOld->problem    = $this->problem;
        $this->modelOld->message    = $this->message;
        $this->modelOld->status_app = GasIssueTickets::STATUS_1_NEW_SALE;
        $this->modelOld->status     = GasTickets::STATUS_OPEN;
        $this->modelOld->expired    = GasIssueTickets::IS_NOT_EXPIRED;

        if($this->direction == GasIssueTickets::DIR_ADMIN_AUTO){
            $cUid = $this->uid_login;// Sep0418 from cron console
            $this->modelOld->status_app         = GasIssueTickets::STATUS_2_SALE_REPLY;
            $this->modelOld->sale_id            = $this->sale_id;
            $this->modelOld->chief_monitor_id   = $this->sale_id;
        }else{// action from web
            $cUid = MyFormat::getCurrentUid();
            $this->modelOld->uid_login          = $cUid;
            if(!empty($this->chief_monitor_id)){
                $this->modelOld->chief_monitor_id   = $this->chief_monitor_id;
            }
        }

        $this->modelOld->update();
        // 2. save record detail + save file
        $this->modelOld->saveRecordDetail($cUid);
    }

    /** @Author: DuongNV Sep 04, 2018   
     *  @Todo: Đưa khách hàng không lấy hàng + KH khai tử trong n ngày ra GasIssueTickets
     * - sale nào thì chuyển cho sale đó, KH cty chuyển anh Vinh
     **/
    public function moveCustomerStopOrderToIssueTickets() {
//        return ;// Oct218 tạm thời ko quét nữa -- Now2818 Reopen
        try {
            $limitDate      = 40; $aUidTmp = []; $criteria   =new CDbCriteria;
            $aNotScan = [Users::GROUP_KH40, Users::GROUP_M18];
            $mUsersExtend   = new UsersExtend();
            $criteria->addNotInCondition('t.first_char', $aNotScan);
            $aCustomer      = $mUsersExtend->getCustomerStopOrder($limitDate, $criteria);
            $cUid = GasConst::UID_ADMIN;
            foreach ($aCustomer as $mCustomer) {
                $model = new GasIssueTickets('create');
                $model->direction           = GasIssueTickets::DIR_ADMIN_AUTO;
                $model->status_app          = GasIssueTickets::STATUS_2_SALE_REPLY;
                $model->title               = "Quét KH không lấy gas {$limitDate} ngày";
                $model->message             = "Hệ thống tự động quét KH không lấy gas {$limitDate} ngày.<br>Nhân viên kinh doanh cập nhật trả lời trên phản ánh sự việc.";
                $model->problem             = self::PROBLEM_KO_LAY_HANG;
                $model->customer_id         = $mCustomer->id;
                $model->sale_id             = $mCustomer->sale_id;
                $model->sale_id             = $model->getSaleIdOnly($aUidTmp);
                $model->sale_id             = !empty($model->sale_id) ? $model->sale_id : GasLeave::UID_HEAD_GAS_BO;
                $model->uid_login           = $model->sale_id;
                $model->chief_monitor_id    = $model->sale_id;
                $_POST['GasIssueTickets']['chief_monitor_id'] = $model->sale_id;// Dec1912 set for move_to_uid in detail
                $model->validate();
                $oldRecord = $model->getOldRecordAuto();
                if(!$oldRecord){
                    $model->saveRecordBeginIssue();                    
                }
            }
            Logger::WriteLog(count($aCustomer). " - KH moveCustomerStopOrderToIssueTickets");
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }
    
    /** @Author: DungNT Sep 04, 2018
     *  @Todo: get record auto Open, để ko bị tạo auto 2 lần
     **/
    public function getOldRecordAuto() {
        $criteria = new CDbCriteria();
        $criteria->compare("t.customer_id", $this->customer_id);
        $criteria->compare("t.status", GasTickets::STATUS_OPEN);// Tạm bỏ để không bị quét trung lên những cái đã close 
        $criteria->compare("t.problem", self::PROBLEM_KO_LAY_HANG);
//        $criteria->compare("t.uid_login", $this->uid_login);// ko can dk nay
        return self::model()->find($criteria);
    }
    /** @Author: DungNT Sep 21, 2018
     *  @Todo: khởi tạo thông tin default
     **/
    public function initModel() {
        $cUid = MyFormat::getCurrentUid();
        if(in_array($cUid, $this->getArrayUidCreatePlan())){
            $this->problem = self::PROBLEM_GV_BUSINESS;
        }
    }
    /** @Author: KHANH TOAN 2018
     *  @Todo: get FinishDay renewal_date OR last_reply
     *          ưu tiên renewal_date
     *  @Param:
     **/
    public function getFinishDay($format =  "d/m/Y H:i") {
        if($this->renewal_date != "0000-00-00" && $this->renewal_date)
            return MyFormat::dateConverYmdToDmy($this->renewal_date, $format);
        return MyFormat::dateConverYmdToDmy(MyFormat::modifyDays($this->last_reply, $this->daysHandleJob), $format);
//        return MyFormat::dateConverYmdToDmy($this->last_reply, $format);
    }
    /** @Author: KHANH TOAN 2018
     **/
    public function getTotalReply() {
        $dataProvider = $this->searchReplies();
        return $dataProvider->totalItemCount;
    }
    /** @Author: Pham Thanh Nghia Oct 19 2018
     **/
    public function getCloseDate($format =  "d/m/Y H:i"){
        return MyFormat::dateConverYmdToDmy($this->close_date, $format);
    }
    
    /** @Author: DungNT Oct 27, 2018
     *  @Todo: get list User not render calendar
     **/
    public function getListUidNotViewCalendar() {
        return [
            GasLeave::UID_DIRECTOR,
        ];
    }
    /** @Author: DungNT Oct 27, 2018
     *  @Todo: get list User with pagesize = 50
     **/
    public function getListUidBigPageSize() {
        return [
            GasLeave::UID_HEAD_OF_LEGAL,
        ];
    }
    
    /** @Author: DungNT Now 09, 2018
     *  @Todo: handle update Meeting and remove Meeting
     **/
    public function updateMeeting() {
        if($this->status_app == GasIssueTickets::STATUS_8_QUICK_ACTION){
           $this->status_app =  GasIssueTickets::STATUS_2_SALE_REPLY;
        }else{
            $this->status_app =  GasIssueTickets::STATUS_8_QUICK_ACTION;
        }
        $this->update(['status_app']);
    }
    
    /** @Author: DuongNV Nov 14, 2018
     *  @Todo: handle save nhân viên liên quan gasIssueTickets
     *  @Param: $aUserId array user id
     **/
    public function handleSaveRelated($aUserId) {
        if(count($aUserId) < 1){
            return ;
        }
        $aRowInsert = [];
        foreach ($aUserId as $many_id) {
            $aRowInsert[] = "(null,"
                    . $this->id . ","
                    . $many_id . ","
                    . GasOneManyBig::TYPE_RELATED_EMPLOYEE
                    . ")";
        }
        $tableName = GasOneManyBig::model()->tableName();
        
        $sqlDelete = 'DELETE FROM ' . $tableName . ' WHERE one_id = ' . $this->id . ' AND type = ' . GasOneManyBig::TYPE_RELATED_EMPLOYEE;
        Yii::app()->db->createCommand($sqlDelete)->execute();
        
        $sql = 'INSERT INTO ' . $tableName . ' VALUES ' . implode(",", $aRowInsert);
        Yii::app()->db->createCommand($sql)->execute();
    }
    
    /** @Author: DuongNV Nov 14, 2018
     *  @Todo: handle get nhân viên liên quan gasIssueTickets
     *  @Return array user id:
     **/
    public function getRelatedEmployee() {
        $aRes       = [];
        $criteria   = new CDbCriteria;
        $criteria->compare('t.one_id', $this->id);
        $criteria->compare('t.type', GasOneManyBig::TYPE_RELATED_EMPLOYEE);
        $model      = GasOneManyBig::model()->findAll($criteria);
        foreach ($model as $item) {
            $aRes[] = $item->many_id;
        }
        return $aRes;
    }
    /** @Author: HaoNH Aug 22, 2019
     *  @Todo: search quy trinh mở đại ly
     *  @Param:
     * */
    public function searchOpenAgent() {
        $criteria = new CDbCriteria;
        $this->AddSameCriteria($criteria);
        $cUid = MyFormat::getCurrentUid();

        $criteria->compare('t.status', GasTickets::STATUS_OPEN);
        $criteria->compare('t.type', GasIssueTickets::TYPE_OPEN_AGENT);
        $criteria->compare('t.process_status', $this->process_status);
        $criteria->compare('t.process_user_id', $this->process_user_id);
        $mUser = Users::model()->findByPk($cUid);
        $this->handleUserRole($criteria, $mUser, GasTickets::STATUS_OPEN);

        $criteria->order = 't.expired DESC, t.id DESC'; // add Dec 20

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
            'pagination' => array(
//                'pageSize'=> 3,
                'pageSize' => $this->pageSize,
            ),
        ));
    }

    /** @Author: HaoNH Aug 22, 2019
     *  @Todo: get dropdown list status stage
     *  @Param:
     * */
    public function dropDownListStageStatus($stage_current) {
        $aStatus = array(
            array(
                'id' => 1,
                'label' => ' Chưa hoàn thành GĐ ' . $stage_current
            ),
            array(
                'id' => 3,
                'label' => ' Hoàn thành GĐ ' . $stage_current
            )
        );
        return CHtml::listData($aStatus, 'id', 'label');
    }

    /** @Author: HaoNH Aug 22, 2019
     *  @Todo:Thêm fields Trường hợp tạo quy trình đại lí  
     *  @Param:
     * */
    public function addFieldsOpenAgent() {
        $expired                = MyFormat::modifyDays($this->created_date_only, $this->getExpiredStage()[0]);
        $this->status_app       = GasIssueTickets::STATUS_9_OPEN_AGENT;
        $this->type             = GasIssueTickets::TYPE_OPEN_AGENT;
        $this->google_map       = trim(InputHelper::removeScriptTag($this->google_map));        // toạ độ 
        $this->status           = (isset($this->status)) ? $this->status : 1;                       // mặc định 1 khi mới tạo 
        $this->process_status   = (!empty($this->process_status)) ? $this->process_status : 0;       // trạng thái hiện tại của quy trình
        $this->stage_current    = (!empty($this->stage_current)) ? $this->stage_current : 1;       // trạng thái hiện tại của quy trình
        $this->renewal_date     = $expired;
        $this->deadline         = $expired;
        $this->isSaveRenewalDate = true;
    }

    /** @Author:HaoNH Aug 26, 2019
     *  @Todo: cập nhật fields tạo giai đoạn tiếp theo
     *  @Param:
     * */
    public function fieldsProcessNext($mGasIssueTickets, $cUid) {        
        if (isset($_GET['process_next'])) {
            if (isset($_POST['GasIssueTickets']['process_status'])) {
                $process_status         = $_POST['GasIssueTickets']['process_status'];
            } else {
                $process_status         = 1;
                $this->stage_current    = $mGasIssueTickets->stage_current + 1;
                $expired                = MyFormat::modifyDays(date("Y-m-d"), $this->getArrStage()[$this->stage_current-1]);
                $this->renewal_date     = $expired;
                $this->deadline         = $expired;
                $this->isSaveRenewalDate = true;
            } 
            $this->updateStatusTicket($process_status, $cUid);
        }        
    }

    /** @Author: HaoNH Sep 03, 2019
     *  @Todo: giám đốc duyệt gia hạn deadline
     *  @Param:
     * */
    public function approveDeadline($renewal) {
        $cRole      = MyFormat::getCurrentRoleId();
        $aRoleAllow = [ROLE_DIRECTOR];
        if (in_array($cRole, $aRoleAllow) && $this->isSaveRenewalDate == true) {
            $this->deadline = $renewal;
        }
    }

    /** @Author: HaoNH  2019
     *  @Todo:Người dc quyền tạo 
     *  @Param:
     **/
   public function canCreateOpenAgent() {
        if ($this->checkOpenAgent()) {
            $cRole = MyFormat::getCurrentRoleId();
            $aRoleAllow = [ROLE_DIRECTOR,ROLE_ADMIN];
            if (in_array($cRole, $aRoleAllow) ) {
                return true;
            }
            return false;
        }
    }

    /** @Author: HaoNH Sep 03, 2019
     *  @Todo:  close quy trình khi hoàn tất
     *  @Param:
     * */
    public function closeOpenAgent() {
        if ($this->stage_current == 3 && $this->process_status == 3) {
            $this->closeTicket();
        }
    }

    /** @Author: HaoNH Sep 03, 2019
     *  @Todo: cron thông báo thời hạn, phạt quy trình đại lý
     *  @Param:
     * */
    public function cronExpriredOpenAgent($today) {
        // alert tuần 6 ngày
        $routine                = self::ALERT_ROUTINE_OPEN_AGENT;        
        $expired_before_once    = self::EXPIRED_ONCE - self::EXPIRED_BEFORE_ONCE;
        $expired_before_twice   = self::EXPIRED_TWICE - self::EXPIRED_BEFORE_TWICE;
        $criteria               = new CDbCriteria();
        $criteria->compare('t.type', GasIssueTickets::TYPE_OPEN_AGENT);
        $models                 = GasIssueTickets::model()->findAll($criteria);
        foreach ($models as $mIssueTickets) {
            $cheftMonitor_id    = $mIssueTickets->chief_monitor_id;
            $deadline           = $mIssueTickets->deadline;
            $number_day         = MyFormat::getNumberOfDayBetweenTwoDate($today, $mIssueTickets->created_date_only);
            $number_expired     = MyFormat::getNumberOfDayBetweenTwoDate($today, $deadline);
            $alert_expired_before_once  = MyFormat::modifyDays($deadline, $expired_before_once);
            $alert_expired_before_twice = MyFormat::modifyDays($deadline, $expired_before_twice);
            $alert_expired_once         = MyFormat::modifyDays($deadline, self::EXPIRED_ONCE);            
            $alert_expired_twice        = MyFormat::modifyDays($deadline, self::EXPIRED_TWICE);
            $isExpired = MyFormat::compareTwoDate($today, $deadline);
            if ($this->status != GasTickets::STATUS_CLOSE) {
                if ($isExpired) {
                    $mIssueTickets->setExpired(1);
                    $mIssueTickets->alertSendMailOpenAgent('expired');
                }
                if (!$isExpired && $number_day % $routine == 0) {
                    $mIssueTickets->alertSendMailOpenAgent();
                }
                if ($today == $alert_expired_before_once) {
                    $mIssueTickets->alertSendMailOpenAgent('expired_before_once');
                }
                if ($today == $alert_expired_before_twice) {
                    $mIssueTickets->alertSendMailOpenAgent('expired_before_twice');
                }
                if ($today == $alert_expired_once) {
                    $mIssueTickets->alertSendMailOpenAgent('expired_once');
                    $mIssueTickets->addIssueError(EmployeeProblems::PROBLEM_EXPIRED_10, $cheftMonitor_id);
                }
                if ($today == $alert_expired_twice) {
                    $mIssueTickets->alertSendMailOpenAgent('expired_twice');
                    $mIssueTickets->addIssueError(EmployeeProblems::PROBLEM_EXPIRED_20, $cheftMonitor_id);
                }
            }
        }
    }

    /** @Author: HaoNH Sep 03, 2019
     *  @Todo: thông báo gửi mail quy trình đại lý
     *  @Param:
     * */
    public function alertSendMailOpenAgent($task = '') {
        $aEmail = $aUid = [];
        if (!empty($this->chief_monitor_id)) {
            $aUid[$this->chief_monitor_id] = $this->chief_monitor_id;
        }
        $this->getArrayEmailByArrayUid($aUid, $aEmail);
        $stage = $this->stage_current;
        $today = date('Y-m-d');
        $title = '';
        $number_day = MyFormat::getNumberOfDayBetweenTwoDate($today, $this->deadline);
        if (empty($task)) {
            $title = '[Thông báo thời hạn] Mở đại lý: ' . $this->title;
            $body = "<br><b>Nội dung: </b>Thời hạn hoàn thành giai đoạn $stage còn $number_day ngày";
        }
        if ($task == 'expired') {
            $title = '[Thông báo thời hạn] Mở đại lý: ' . $this->title;
            $body = "<br><b>Nội dung: </b> Đã hết thời hạn của giai đoạn $stage";
        }
        if ($task == 'expired_before_once') {
            $title = '[Cảnh báo] Mở đại lý: ' . $this->title;
            $body = "<br><b>Nội dung: </b> Trễ hạn ".self::EXPIRED_ONCE." ngày phạt 500k (còn ".self::EXPIRED_BEFORE_ONCE." ngày)";
        }
        if ($task == 'expired_before_twice') {
            $title = '[Cảnh báo] Mở đại lý: ' . $this->title;
            $body = "<br><b>Nội dung: </b> Trễ hạn ".self::EXPIRED_TWICE." ngày phạt 1000k (còn ".self::EXPIRED_BEFORE_TWICE." ngày)";
        }
        if ($task == 'expired_once') {
            $title = '[Phạt trễ hạn] Mở đại lý: ' . $this->title;
            $body = "<br><b>Nội dung: </b> Quá hạn ".self::EXPIRED_ONCE." ngày bị phạt 500k";
        }
        if ($task == 'expired_twice') {
            $title = '[Phạt trễ hạn] Mở đại lý: ' . $this->title;
            $body = "<br><b>Nội dung: </b> Quá hạn ".self::EXPIRED_TWICE." ngày bị phạt 1000k";
        }
        $this->templateEmailOpenAgent($title, $body, $aEmail);
    }
    
    /** @Author: HaoNH Sep 03, 2019
     *  @Todo:
     *  @Param:
     **/
    public function templateEmailOpenAgent($title, $body, $aEmail) {
        $needMore               = [];
        $needMore['title']      = $title;
        $aEmail[]               = 'haonh@spj.vn';
        $needMore['list_mail']  = $aEmail;
        SendEmail::bugToDev($body, $needMore);
    }

    public function getProcessStatus() {
        return $this->process_status;
    }

    public function getStageCurrent() {
        return $this->stage_current;
    }

    public function getGoogleMap() {
        return $this->google_map;
    }
    
    /** @Author: HaoNH sep 03 2019
     *  @Todo: check is open agent
     *  @Param:
     **/
    public function checkOpenAgent(){          
        if(isset($_GET['view_type']) && $_GET['view_type']== GasIssueTickets::VIEW_TYPE_OPEN_AGENT){
           return true;
        }else{
            return false;
        }        
    }
    
    /** @Author: HaoNH sep 03 2019
     *  @Todo: get list stage
     *  @Param:
     **/
    public function getArrStage()
    {
        $totalStage     = self::STAGE_TOTAL_OPEN_AGENT;
        $aStage         = [];
        for($i=1;$i<=$totalStage;$i++){
            $str        = 'Giai đoạn '.$i;
            $aStage[$i] = $str; 
        }        	
        return $aStage;         
    }   
    
    /** @Author: HaoNH sep 03 2019
     *  @Todo: get list expired status
     *  @Param:
     **/
    public function getArrStatusExpired()
    {        
        $aStatus = ['Chưa hết hạn ','Hết hạn'];          	
        return $aStatus;         
    } 
    
    /** @Author: HaoNH Sep 03, 2019
     *  @Todo: show btn tạo quy trình tiếp theo
     *  @Param:
     **/
    public function showCreateProcessNext() {
        $cUid   = MyFormat::getCurrentUid();
        $cRole  = MyFormat::getCurrentRoleId();
        $aAllow = [$this->uid_login];      
        if ($cRole == ROLE_ADMIN || in_array($cUid, $aAllow)) {
            return true;
        }
        return false;
    }
}