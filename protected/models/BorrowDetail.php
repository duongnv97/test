<?php

/**
 * This is the model class for table "{{_borrow_detail}}".
 *
 * The followings are the available columns in table '{{_borrow_detail}}':
 * @property string $id
 * @property string $borrow_id
 * @property string $company_id
 * @property string $bank_id
 * @property string $date_pay
 * @property string $pay_amount
 * @property string $uid_login
 * @property string $created_date
 */
class BorrowDetail extends BaseSpj
{
    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return BorrowDetail the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{_borrow_detail}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        return array(
            array('borrow_id, date_pay, pay_amount', 'required', 'on'=>'CreateDetail'),
            array('id, borrow_id, company_id, bank_id, date_pay, pay_amount, uid_login, created_date', 'safe'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        return array(
            'rBorrow' => array(self::BELONGS_TO, 'Borrow', 'borrow_id'),
            'rUidLogin' => array(self::BELONGS_TO, 'Users', 'uid_login'),// người tạo
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'borrow_id' => 'Borrow',
            'company_id' => 'Company',
            'bank_id' => 'Bank',
            'date_pay' => 'Ngày đáo hạn',
            'pay_amount' => 'Số tiền',
            'uid_login' => 'Người tạo',
            'created_date' => 'Ngày tạo',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        $criteria=new CDbCriteria;
        $criteria->compare('t.borrow_id',$this->borrow_id,true);
        $criteria->compare('t.company_id',$this->company_id,true);
        $criteria->compare('t.bank_id',$this->bank_id,true);
        $criteria->compare('t.date_pay',$this->date_pay,true);
        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
            'pagination'=>array(
                'pageSize'=> Yii::app()->user->getState('pageSize',Yii::app()->params['defaultPageSize']),
            ),
        ));
    }
    
    public function getDatePay() {
        return MyFormat::dateConverYmdToDmy($this->date_pay);
    }
    
    public function getPayAmount($format = false) {
        if($format){
            return ActiveRecord::formatCurrency($this->pay_amount);
        }
        return $this->pay_amount;
    }
    
    public function getUidLogin() {
        $mUser = $this->rUidLogin;
        if($mUser){
            return $mUser->getFullName();
        }
        return "";
    }
    
    protected function beforeValidate() {
        $this->pay_amount = MyFormat::removeComma($this->pay_amount);
        $mBorrow = $this->rBorrow;
        if($mBorrow){
            $this->company_id   = $mBorrow->company_id;
            $this->bank_id      = $mBorrow->bank_id;
        }
        return parent::beforeValidate();
    }
    
    protected function beforeSave() {
        if(strpos($this->date_pay, '/')){
            $this->date_pay = MyFormat::dateConverDmyToYmd($this->date_pay);
            MyFormat::isValidDate($this->date_pay);
        }
        return parent::beforeSave();
    }
    
    protected function afterSave() {
        if($this->isNewRecord){
            $mBorrow = $this->rBorrow;
            $mBorrow->updateAfterAddDetail($this->pay_amount);
        }
        parent::afterSave();
    }
    protected function afterDelete() {
        $mBorrow = $this->rBorrow;
        $mBorrow->updateAfterAddDetail((0-$this->pay_amount));
        return parent::afterDelete();
    }
   
}