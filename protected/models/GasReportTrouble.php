<?php

/**
 * This is the model class for table "{{_report_trouble}}".
 *
 * The followings are the available columns in table '{{_report_trouble}}':
 * @property string $id
 * @property string $uid_login
 * @property string $customer_id
 * @property integer $type_customer
 * @property string $sale_id
 * @property integer $status
 * @property string $detail
 * @property string $result
 * @property string $note
 * @property string $created_date
 * @property Users $rCustomer
 * @property Users $rUidLogin
 * @property Users $rSale
 */
class GasReportTrouble extends CActiveRecord
{
    // Trung June 16 2016
    // Status for GasReportTrouble
    const STA_NEW = 1;
    const STA_PROCESSING = 2;
    const STA_COMPLETE = 3;
    public static $STATUS_TEXT = array(
        GasReportTrouble::STA_NEW => 'Yêu cầu mới',
        GasReportTrouble::STA_PROCESSING => 'Đang xử lý',
        GasReportTrouble::STA_COMPLETE => 'Đã hoàn thành'
    );

    public $autocomplete_sale_id, $autocomplete_sale, $autocomplete_customer_id, $autocomplete_customer,
        $autocomplete_login, $autocomplete_login_id;

    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return GasReportTrouble the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{_report_trouble}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('uid_login, customer_id, type_customer, status, detail', 'required'),
            array('type_customer, status', 'numerical', 'integerOnly' => true),
            array('uid_login', 'length', 'max' => 255),
            array('result', 'ruleCheckResult'),
            array('customer_id, sale_id', 'length', 'max' => 255),
            array('result, note', 'safe'),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('id, uid_login, customer_id, type_customer, sale_id, status, detail, result, note, created_date', 'safe'),
        );
    }

    public function ruleCheckResult($attribute)
    {
        if ($this->status == GasReportTrouble::STA_COMPLETE && empty($this->$attribute)) {
            $this->addError($attribute, 'Vui lòng nhập thông tin kết quả');
        }
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'rCustomer' => array(self::BELONGS_TO, 'Users', 'customer_id'),
            'rUidLogin' => array(self::BELONGS_TO, 'Users', 'uid_login'),
            'rSale' => array(self::BELONGS_TO, 'Users', 'sale_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'uid_login' => 'Người tạo',
            'customer_id' => 'Khách hàng',
            'type_customer' => 'Loại khách hàng',
            'sale_id' => 'NV Sale',
            'status' => 'Trạng thái',
            'detail' => 'Thông tin chi tiết',
            'result' => 'Kết quả',
            'note' => 'Ghi chú',
            'created_date' => 'Ngày tạo',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('t.id', $this->id, true);
        $criteria->compare('t.uid_login', $this->uid_login, true);
        $criteria->compare('t.customer_id', $this->customer_id, true);
        $criteria->compare('t.type_customer', $this->type_customer);
        $criteria->compare('t.sale_id', $this->sale_id, true);
        $criteria->compare('t.status', $this->status);
        $criteria->compare('t.detail', $this->detail, true);
        $criteria->compare('t.result', $this->result, true);
        $criteria->compare('t.note', $this->note, true);
        $criteria->compare('t.created_date', $this->created_date, true);

        // Default get newest
        $sort = new CSort();
        $sort->attributes = array();
        $sort->defaultOrder = 't.id desc';

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
            'sort' => $sort,
            'pagination' => array(
                'pageSize' => Yii::app()->user->getState('pageSize', Yii::app()->params['defaultPageSize']),
            ),
        ));
    }

    public function defaultScope()
    {
        return array(//'condition'=>'',
        );
    }

    /**
     * @Author: TRUNG June 16 2016
     * Check model is updateable or not
     */
    public function canUpdate()
    {
        $cRole = Yii::app()->user->role_id;
        $cUid = Yii::app()->user->id;
        if (($cRole != ROLE_ADMIN && $cUid != $this->uid_login)) {
            return false; // cho phép admin sửa hết, còn những user khác thì ai tạo thì dc sửa của người đó
        }

        if ($this->status == GasReportTrouble::STA_COMPLETE) {
            return false; // Not allow edit completed item
        }

        return true;
    }

    /**
     * @Author: TRUNG June 16 2016
     * Get user name of create user
     */
    public function getNameUserCreate()
    {
        if (!empty($this->rUidLogin)) {
            return $this->rUidLogin->first_name;
        } else {
            return '';
        }
    }

    /**
     * @Author: TRUNG June 16 2016
     * Get user name of customer
     * @return string
     */
    public function getCustomerName()
    {
        if (!empty($this->rCustomer)) {
            return $this->rCustomer->first_name;
        }
        return '';
    }

    /**
     * @Author: TRUNG June 16 2016
     * Get user name of sale
     * @return string
     */
    public function getSaleName()
    {
        if (!empty($this->rSale)) {
            return $this->rSale->first_name;
        } else {
            return '';
        }
    }

    /**
     * @Author: TRUNG June 16 2016
     * Get status text of model in GasReportTrouble::$STATUS_TEXT
     * @return string
     */
    public function getStatusText()
    {
        if (isset(GasReportTrouble::$STATUS_TEXT[$this->status])) {
            return GasReportTrouble::$STATUS_TEXT[$this->status];
        } else {
            return '';
        }
    }

    /**
     * @Author: TRUNG June 16 2016
     * Get html detail for record
     * @return string
     */
    public function getDetailHtml()
    {
        $html = nl2br($this->detail);
        if (!empty($this->result)) {
            $html .= "<br/>
            <strong>{$this->getAttributeLabel('result')}:</strong>
            <i>" . nl2br($this->result) . "
            </i>";
        }
        if (!empty($this->note)) {
            $html .= "<br/>
            <strong>{$this->getAttributeLabel('note')}:</strong>
            <i>" . nl2br($this->note) . "
            </i>";
        }
        return $html;
    }
}