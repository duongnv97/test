<?php

/**
 * This is the model class for table "{{_reward_point}}".
 *
 * The followings are the available columns in table '{{_reward_point}}':
 * @property string $id
 * @property string $customer_id
 * @property string $default_point
 * @property string $reward_setup_id
 * @property string $bonus_point
 * @property string $total_point
 * @property string $created_date
 */
class RewardPoint extends BaseSpj
{
    public $autocomplete_name, $date_from, $date_to;
    
    const POINT_DEFAULT     = 10;
    const NEED_SMS_TRUE     = 1;
    const NEED_SMS_FASLE    = 0;
    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return RewardPoint the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{_reward_point}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        return [
            array('customer_id, default_point, reward_setup_id, bonus_point, total_point', 'length', 'max'=>10),
            array('need_sms,created_date', 'safe'),
            ['id,created_date_only, customer_id, default_point, reward_setup_id, bonus_point, total_point, created_date, date_from, date_to', 'safe'],
        ];
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        return [
            'rCustomer' => array(self::BELONGS_TO,'Users','customer_id')
        ];
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'customer_id' => 'Khách hàng',
            'default_point' => 'Điểm mặc định',
            'reward_setup_id' => 'Reward Setup',
            'bonus_point' => 'Điểm cộng thêm',
            'total_point' => 'Tổng điểm',
            'created_date' => 'Ngày tạo',
            'date_to' => 'Đến ngày',
            'need_sms' => 'Sms',
        ];
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        $criteria=new CDbCriteria;

	$criteria->compare('t.id',$this->id);
	$criteria->compare('t.customer_id',$this->customer_id);
	$criteria->compare('t.default_point',$this->default_point);
	$criteria->compare('t.reward_setup_id',$this->reward_setup_id);
	$criteria->compare('t.bonus_point',$this->bonus_point);
	$criteria->compare('t.total_point',$this->total_point);
	$criteria->compare('t.need_sms',$this->need_sms);
        $start_date_from = '';
        $start_date_to = '';
        if(!empty($this->date_from)){
            $start_date_from = MyFormat::dateDmyToYmdForAllIndexSearch($this->date_from);
        }
        if(!empty($this->date_to)){
            $start_date_to = MyFormat::dateDmyToYmdForAllIndexSearch($this->date_to);
        }
        if(!empty($start_date_from) && empty($start_date_to))
                $criteria->addCondition("t.created_date>='$start_date_from'");
        if(empty($start_date_from) && !empty($start_date_to))
                $criteria->addCondition("t.created_date<='$start_date_to'");
        if(!empty($start_date_from) && !empty($start_date_to))
                $criteria->addBetweenCondition("t.created_date",$start_date_from,$start_date_to);
	$criteria->compare('t.created_date',$this->created_date,true);
        $criteria->order = 't.id DESC';
        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
            'pagination'=>array(
                'pageSize'=> Yii::app()->user->getState('pageSize',Yii::app()->params['defaultPageSize']),
            ),
        ));
    }
    
    public function getDefaultPoint() {
        return $this->default_point;
    }
    
    public function getBonusPoint() {
        return $this->bonus_point;
    }
    
    public function getTotalPoint() {
        return $this->total_point;
    }
    
    public function getCustomer() {
        $aEmployee = $this->rCustomer;
        return isset($aEmployee->first_name) ? $aEmployee->first_name : "";
    }
    
    public function checkIsPointBySell($mSell){
        if(empty($mSell->id)){
            return false;
        }
        $criteria = new CDbCriteria;
        $criteria->compare('t.sell_id', $mSell->id);
        $count = RewardPoint::model()->count($criteria);
        if($count > 0){
            return true;
        }
        return false;
    }
    
    /** @Author: KHANH TOAN Mar 10 2019
     *  @Todo: cong diem khi hoan thanh mot hoa don
     *  @Param: $mSell
     **/
    public function addPoint($mSell, $need_sms = self::NEED_SMS_FASLE,$sendSms = true,$checkIsPoint = true) {
        if($mSell->status != Sell::STATUS_PAID){
            return;
        }
        if($this->checkIsPointBySell($mSell)){
            return;
        }
//        remove old
        $this->removeBySell($mSell);
        $mTargetDaily = new TargetDaily();
        $aSellId = [$mSell->id];
        $aRemove = $mTargetDaily->findSellNotGas($aSellId);
        
        if(in_array($mSell->id, $aRemove)){
            return;
        }
        if($checkIsPoint){
            $aRemove2 = $this->checkIsPoint($aSellId);
            if(is_array($aRemove2) && in_array($mSell->id, $aRemove2)){
                return;
            }
        }
        $reward_setup_id = 0;
        $bonus_point = $this->checkRewardSetup($mSell, $reward_setup_id);
        $mRewardPointNew = new RewardPoint();
        $mRewardPointNew->customer_id       = $mSell->customer_id;
        $mRewardPointNew->sell_id           = $mSell->id;
        $mRewardPointNew->created_date_only = $mSell->created_date_only;
        $mRewardPointNew->sell_date         = $mSell->created_date_only;
        $mRewardPointNew->default_point     = RewardPoint::POINT_DEFAULT;
        $mRewardPointNew->reward_setup_id   = $reward_setup_id;
        $mRewardPointNew->bonus_point       = $bonus_point;
        $mRewardPointNew->total_point       = RewardPoint::POINT_DEFAULT + $bonus_point;
        $mRewardPointNew->need_sms          = $need_sms;
        if($mRewardPointNew->save()){
            // check is apply in agent
            if($sendSms && $this->isAgentApply($mSell)){// DungNT Aug3019 move function $this->isAgentApply($mSell)
                $mRewardPointNew->sendSms($mSell->rCustomer);
            }
        }
        $this->updatePointOfUser($mSell->customer_id);
    }
    
    /** @Author: NamNH NamNH Jun 11, 2019
     *  @Todo: send sms
     *  @Param:param
     **/
    public function sendSms($mUser){
        if(empty($mUser)){
            return ;
        }
        $point = $this->calculatePoint($mUser->id);
        $smsContent = "Diem tich luy cua Khach Hang den thoi diem hien tai la $point diem. Chi tiet lien he 19001565. Tran trong cam on";
        $this->doSendSms($smsContent, $mUser->id);
    }
    
    /** @Author: NamNH NamNH Jun 11, 2019
     *  @Todo: send sms
     *  @Param:param
     **/
    public function doSendSms($smsContent, $toUserId) {
        $mSms = new GasScheduleSms();
        $mSms->phone = $this->getPhoneReward($toUserId);
        if(empty($mSms->phone)){
            return null;
        }
        $mSms->title = $smsContent;
        $mSms->purgeMessage();
        $mSms->uid_login        = 2;
        $mSms->user_id          = $toUserId;
        $mSms->type             = GasScheduleSms::TYPE_HGD_SMS_POINT;
        $mSms->obj_id           = $this->id;
        $mSms->content_type     = GasScheduleSms::ContentTypeKhongDau;
        $mSms->time_send        = '';
        $mSms->json_var         = [];
        $schedule_sms_id        = $mSms->fixInsertRecord();
        return $schedule_sms_id;
    }
    
    public function getPhoneReward($user_id){
        $mUsersPhoneExt2 = new UsersPhoneExt2();
        $mUsersPhoneExt2->user_id = $user_id;
        $mUsersPhone = $mUsersPhoneExt2->getRecordHgdSmsPoint();
        if(empty($mUsersPhone)){
            return '';
        }
        return $mUsersPhone->phone;
    }
    
    /** @Author: NamNH Jun 11, 2019
     *  @Todo: check agent apply, kiểm tra đại lý chạy chương trình tích lũy điểm
     *  Hiện tại Aug3019 chạy ở chợ lách cộng điểm + nhắn SMS cho KH gọi điện, ĐL này ko có KH app
     *  @Param: $mSell
     *  @Return: true là ĐL có chương trình tích lũy + gửi SMS
     *  @Return: false là ĐL ko chạy chương trình tích lũy + gửi SMS
     **/
    public function isAgentApply(&$mSell){
        if(GasCheck::isServerAndroid()){
            return true;
        }
        if(!empty($mSell->rCustomer)){
            $mUsersPhone = new UsersPhoneExt2();
            if($mUsersPhone->canUseHgdSmsPoint($mSell->rCustomer)){
                return true;
            }
        }
        return false;
    }

    
    /** @Author: KHANH TOAN Mar 10 2019
     *  @Todo:  tim diem moi nhat cua khach hang 
     *  @Param: $customer_id
     **/
    public function findPointCustomer($customer_id) {
        $criteria=new CDbCriteria;
        $criteria->select = 'sum(t.total_point) as total_point';
        $criteria->compare('t.customer_id',$customer_id);
        $criteria->group = 't.customer_id';
        $mRewardPoint = self::model()->find($criteria);
        return $mRewardPoint;
    }
    
    /** @Author: KHANH TOAN Mar 12 2019
     *  @Todo: kiem tra xem co bonus them diem khong
     *  @Param: $mSell
     * 1/ find RewardSetup status=>active, between date apply
     * 2/ kiem tra 
     *      2.1/ $mSell->agent_id co trong agent_id ap dung khong ?
     *      2.2/ count sell trong khoang ngay ap dung > count_sell_apply ?
     * 3/ cong diem khi (2.1 true || agent_id = "") && 2.2 true
     **/
    public function checkRewardSetup($mSell, &$reward_setup_id) {
        return 0;
        $bonus_point = 0;
        $criteria = new CDbCriteria;
        $criteria->compare('t.status', RewardSetup::STATUS_ACTICE);
        $criteria->addCondition("t.date_from<='$mSell->created_date'");
        $criteria->addCondition("t.date_to>='$mSell->created_date'");
        $mRewardSetups = RewardSetup::model()->findAll($criteria);
        foreach ($mRewardSetups as $mRewardSetup) {
            $aAgent = [];
            if(!empty($mRewardSetup->agent_id))
                $aAgent = json_decode($mRewardSetup->agent_id);
            if((in_array($mSell->agent_id, $aAgent) || $mRewardSetup->agent_id == "") && $this->checkCountSell($mRewardSetup, $mSell)){
                $bonus_point+= $mRewardSetup->point_reward;
                $reward_setup_id = $mRewardSetup->id;
            }
                
            break;
        }
        return $bonus_point;
    }
    
    /** @Author: KHANH TOAN 2019
     *  @Todo: kiem tra so don hang dat du yeu cau chua ?
     *  @Param: $mRewardSetup, $mSell
     **/
    public function checkCountSell($mRewardSetup, $mSell) {
        $criteria = new CDbCriteria;
        $criteria->compare('t.status', Sell::STATUS_PAID);
        $criteria->addBetweenCondition('t.created_date',$mRewardSetup->date_from,$mRewardSetup->date_to);
        $mSell = Sell::model()->findAll($criteria);
        if(count($mSell) < $mRewardSetup->count_sell_apply)
            return false;
        return true;
    }
    
    public function getExchangePoint($customer_id){
        $totalExchange = 0;
        $criteria = new CDbCriteria();
        $criteria->compare('t.customer_id',$customer_id);
        $mCodePartner = new CodePartner();
        $criteria->compare('t.type',  $mCodePartner->getArrayTypeReward());
        $criteria->addCondition('t.status != '.CodePartner::STATUS_CANCEL);
        $mCodePartners = CodePartner::model()->findAll($criteria);
        if(!empty($mCodePartners)){
            foreach ($mCodePartners as $mCodePartner) {
                $totalExchange += $mCodePartner->reward_point; 
            }
        }
        return $totalExchange;
    }
    
    public function getPointGet($customer_id){
        $totalPoint = 0;
        $mRewardPoint = new RewardPoint();
        $mPoint = $mRewardPoint->findPointCustomer($customer_id);
        if(!empty($mPoint)){
            $totalPoint = $mPoint->total_point;
        }
        return $totalPoint;
    }
    
    /** @Author: KHANH TOAN Mar 19 2019
     *  @Todo: tinh diem hien tai cua khach hang
     *  @Param: $customer_id
     **/
    public function calculatePoint($customer_id) {
        $totalPoint =  $this->getPointGet($customer_id);
        $totalExchange = $this->getExchangePoint($customer_id);
        $pointNow = $totalPoint - $totalExchange; 
        return (!empty($pointNow) && $pointNow > 0) ? $pointNow : 0;
    }
    
    public function getTitleRewardWeb($customer_id = 0){
        $result = 'Đổi điểm thưởng';
        if(!empty($customer_id)){
            $point = $this->getPointFromUsersInfo($customer_id);
            $result .= ' - Điểm hiện tại '.$point;
        }
        return $result;
    }

    /** @Author: NamNH Apr 24, 2019
     *  @Todo: get point
     **/
    public function handlePoint(&$result,$q){
        $mUser = $this->mAppUserLogin;
        $result['record']['point'] = $this->getPointFromUsersInfo($mUser->id);
    }
    
    public function handleApiListPoint(&$result, $q) {
        // 1. get list order by user id
        $dataProvider   = $this->ApiListingPoint($q);
        $models         = $dataProvider->data;
        $CPagination    = $dataProvider->pagination;
        $result['total_record'] = $CPagination->itemCount;
        $result['total_page']   = $CPagination->pageCount;
        
        $result['record']   = [];
        if( $q->page >= $CPagination->pageCount ){
            $result['record'] = [];
        }else{
            foreach ($models as $key => $mRewardPoint) {
                $result['record'][] = $mRewardPoint->formatApiView();
            }
        }
       
    }
    
    public function ApiListingPoint($q){
        $mUser = $this->mAppUserLogin;
        $criteria = new CDbCriteria();
        $criteria->compare('t.customer_id',$mUser->id);
        $criteria->order = 't.id DESC';
        $dataProvider = new CActiveDataProvider($this, array(
            'criteria' => $criteria,
            'pagination'=>array(
                'pageSize' => CodePartner::API_LISTING_EXCHANGE_REWARD_PER_PAGE,
                'currentPage' => (int)$q->page,
            ),
        ));
        return $dataProvider;
    }
    
    public function formatApiView(){
        $result = [];
        $result['id']           = $this->id;
        $result['title']        = $this->getTitle();
        $result['sub_title']    = $this->getSubTitle();
        $result['reward_point'] = $this->total_point;
        $result['created_date'] = MyFormat::dateConverYmdToDmy($this->created_date, 'd/m/Y');
        return $result;
    }
    
    public function getTitle(){
        return 'Thực hiện mua hàng ngày '. MyFormat::dateConverYmdToDmy($this->sell_date);
    }
    
    public function getSubTitle(){
        return '';
    }
    
    /** @Author: NamNH Jun 18, 2019
     *  @Todo: remove reward by sell
     **/
    public function removeBySell($mSell){
        if(empty($mSell->id)){
            return;
        }
        $criteria=new CDbCriteria;
        $criteria->compare('sell_id',$mSell->id);
        self::model()->deleteAll($criteria);
        $this->updatePointOfUser($mSell->customer_id);
    }
    public function getArrayTypeNeedSms(){
        return [
            self::NEED_SMS_FASLE => 'Đã gửi',
            self::NEED_SMS_TRUE => 'Chưa gửi'
        ];
    }
    //get list data exists in point
    public function checkIsPoint($aSellId){
        $criteria=new CDbCriteria;
        $criteria->addInCondition('t.sell_id', $aSellId);
        $aRewartPoint = self::model()->findAll($criteria);
        $result = CHtml::listData($aRewartPoint, 'sell_id', 'sell_id');
        return $result;
    }
    /** @Author: NamNH date
     *  @Todo: add point of customer
     **/
    public function addPointByAgentApply($aAgent = [],$needSms = self::NEED_SMS_FASLE){
        $criteria=new CDbCriteria;
        $criteria->compare('t.status', Sell::STATUS_PAID);
        $criteria->addInCondition('t.agent_id', $aAgent);
        $aSell = Sell::model()->findAll($criteria);
        $aSellId = CHtml::listData($aSell, 'id', 'id');
        $aRemove = $this->checkIsPoint($aSellId);
        foreach ($aSell as $key => $mSell) {
            if(in_array($mSell->id, $aRemove)){
                continue;
            }
            $this->addPoint($mSell, $needSms, false,false);
        }
        $strAgent = implode(',', $aAgent);
        $info = "addPointByAgentApply $strAgent done ".date('Y-m-d H:i:s');
        Logger::WriteLog($info);
        $aSell = null;
        $aSellId = null;
        $aRemove = null;
    }
    
    /** @Author: NamNH Jun 23, 2019
     *  @Todo: send sms to customer need send
     **/
    public function sendSmsNeedSend($need_send = self::NEED_SMS_TRUE) {
        $criteria=new CDbCriteria;
        $mUser = new Users();
        $criteria->compare('t.need_sms', $need_send);
        $criteria->group    = 't.customer_id';
        $criteria->select   = 't.customer_id';
        $aRewardPoint = self::model()->findAll($criteria);
        foreach ($aRewardPoint as $key => $mRewardPoint) {
            $mUser->id = $mRewardPoint->customer_id;
            $mRewardPoint->sendSms($mUser);
        }
//        update need_sms
        $tableName              = self::model()->tableName();
        $sql                    = "UPDATE `$tableName` SET need_sms = ".self::NEED_SMS_FASLE.' WHERE need_sms = '.self::NEED_SMS_TRUE.';';
        Yii::app()->db->createCommand($sql)->execute();
    }
    
    public function getPointText($user_id){
        $point = $this->getPointFromUsersInfo($user_id);
        return 'Bạn đã có '.$point.' điểm thưởng';
    }
    
    /** @Author: NamNH Aug 28, 2019
     *  @Todo: get point from users info
     *  @Param: $user_id
     **/
    public function getPointFromUsersInfo($user_id){
        $criteria = new CDbCriteria;
        $criteria->compare('t.user_id', $user_id);
        $mUserInfo = UsersInfo::model()->find($criteria);
        return !empty($mUserInfo) ? $mUserInfo->reward_point : 0;
    }
    
    /** @Author: NamNH Aug 28, 2019
     *  @Todo: update point in model users_info
     *  @Param: $user_id
     **/
    public function updatePointOfUser($user_id){
        if(empty($user_id)){
            return;
        }
        $reward_point = $this->calculatePoint($user_id);
        $criteria = new CDbCriteria;
        $criteria->compare('t.user_id', $user_id);
        $mUserInfo = UsersInfo::model()->find($criteria);
        if(!empty($mUserInfo)){
            $mUserInfo->reward_point_used = $this->getExchangePoint($user_id);
            $mUserInfo->reward_point = $reward_point;
            $mUserInfo->save();
        }else{
            $mUsersInfo = new UsersInfo();
            $mUsersInfo->user_id = $user_id;
            $mUsersInfo->getDataFromUsers(true,true);//Thực hiện thêm dữ liệu users nếu không tồn tại
        }
    }
}