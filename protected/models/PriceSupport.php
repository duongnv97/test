<?php

/**
 * This is the model class for table "{{_price_support}}".
 *
 * The followings are the available columns in table '{{_price_support}}':
 * @property integer $id
 * @property integer $type
 * @property string $customer_id
 * @property string $type_customer
 * @property string $date_apply
 * @property integer $price_12
 * @property integer $price_45
 */
class PriceSupport extends BaseSpj
{
    const TYPE_HARD_DELIVERY = 1;
    public $autocomplete_name;
    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return PriceSupport the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{_price_support}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        return [
            array('type', 'required','on'=>'create,update'),
            array('customer_id', 'required','on'=>'update'),
            ['id, type, customer_id, type_customer, date_apply, price_12, price_45, status', 'safe'],
            ['note, sale_id, date_from, date_to', 'safe'],
        ];
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        return [
            'rCustomer' => array(self::BELONGS_TO, 'Users', 'customer_id'),
            'rSale' => array(self::BELONGS_TO, 'Users', 'sale_id'),
        ];
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'type' => 'Loại',
            'customer_id' => 'Khách hàng',
            'type_customer' => 'Loại khách hàng',
            'date_apply' => 'Ngày áp dụng',
            'price_12' => 'Giá bình 12',
            'price_45' => 'Giá bình 45',
            'status' => 'Trạng thái',
            'created_date' => 'Ngày tạo',
            'sale_id' => 'NV kinh doanh',
            'note' => 'Ghi chú',
        ];
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        $cRole = MyFormat::getCurrentRoleId();
        $cUid = MyFormat::getCurrentUid();
        $mUser = new Users();
        $criteria=new CDbCriteria;

	$criteria->compare('t.type',$this->type);
	$criteria->compare('t.customer_id',$this->customer_id);
	$criteria->compare('t.type_customer',$this->type_customer);
	$criteria->compare('t.sale_id',$this->sale_id);
        if(!empty($this->date_from)){
            $date_from = MyFormat::dateConverDmyToYmd($this->date_from, "-");
            $criteria->addCondition("t.date_apply >= '" . $date_from . "'");
        }
        if(!empty($this->date_to)){
            $date_to = MyFormat::dateConverDmyToYmd($this->date_to, "-");
            $criteria->addCondition("t.date_apply <= '" . $date_to . "'");
        }
        
        if(in_array($cRole, $mUser->ARR_ROLE_SALE_LIMIT)){
            $criteria->compare('t.sale_id', $cUid);
        }
        
	$criteria->compare('t.price_12',$this->price_12);
	$criteria->compare('t.price_45',$this->price_45);
        $criteria->order = 't.id DESC';
        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
            'pagination'=>array(
                'pageSize'=> Yii::app()->user->getState('pageSize',Yii::app()->params['defaultPageSize']),
            ),
        ));
    }
    
    public function getArrayStatus(){
        return ActiveRecord::getUserStatus();
    }

    public function getStatus(){
        $aStt = $this->getArrayStatus();
        return isset($aStt[$this->status]) ? $aStt[$this->status] : '';
    }
    
    public function getType(){
        $aType = $this->getArrayType();
        return isset($aType[$this->type]) ? $aType[$this->type] : '';
    }
    
    public function getCustomer($field_name = ''){
        $mUser = $this->rCustomer;
        if($mUser){
            if($field_name != ''){
                return $mUser->$field_name;
            }
            return $mUser->code_bussiness."-".$mUser->first_name;
        }
        return '';
    }
    public function getSale($field_name = 'first_name'){
        $mUser = $this->rSale;
        if($mUser){
            return $mUser->$field_name;
        }
        return '';
    }
    
    public function getTypeCustomer(){
        $aTypeCustomer = CmsFormatter::$CUSTOMER_BO_MOI;
        return isset($aTypeCustomer[$this->type_customer]) ? $aTypeCustomer[$this->type_customer] : '';
    }
    
    public function getDateApply(){
        return MyFormat::dateConverYmdToDmy($this->date_apply);
    }
    
    public function getPrice12(){
        return ActiveRecord::formatCurrency($this->price_12);
    }
    public function getPrice45(){
        return ActiveRecord::formatCurrency($this->price_45);
    }
    public function getNote(){
        return nl2br($this->note);
    }

    public function getArrayType() {
        return [
            self::TYPE_HARD_DELIVERY => 'Giao khó'
        ];
    }
    
    /** @Author: ANH DUNG Dec 16, 2018
     *  @Todo: xử lý biến post customer id
     **/
    public function handlePostCustomerId() {
        $aCustomerId = [];
        foreach ($this->customer_id as $key => $cus_id) {
            if(!empty($cus_id)){
                $aCustomerId[$cus_id] = $cus_id;
            }
        }
        return $aCustomerId;
    }
    
    /** @Author: DuongNV 13 Dec, 2018
     *  @Todo: handle save multi when create
     **/
    public function handleSave(){
        $aRowInsert = $aCustomerDuplicate = $aCustomerNew = []; $created_date = $this->getCreatedDateDb();
        $cUid = MyFormat::getCurrentUid(); 
//        $this->date_apply = MyFormat::dateConverDmyToYmd($this->date_apply, "-");
        $this->date_apply = isset($this->date_apply) ? MyFormat::dateConverDmyToYmd($this->date_apply, "-") : date('Y-m-d');
        $aCustomerId = $this->handlePostCustomerId();
        if(count($aCustomerId) < 1){
            return false;
        }
        $aCustomerExists = $this->getCustomerByType(PriceSupport::TYPE_HARD_DELIVERY);
        $aObjCustomer = Users::getArrObjectUserByRole('', $aCustomerId);
        foreach ($this->customer_id as $key => $cus_id) {
            $price_12   = !isset($this->price_12[$key]) ? 0 : MyFormat::removeComma(MyFormat::escapeValues($this->price_12[$key]));
            $price_45   = !isset($this->price_45[$key]) ? 0 : MyFormat::removeComma(MyFormat::escapeValues($this->price_45[$key]));
            $note       = !isset($this->note[$key]) ? '' : MyFormat::escapeValues($this->note[$key]);
            if( empty($cus_id) || ($price_12 < 1 && $price_45  <1) ){
                continue;
            }
            if(in_array($cus_id, $aCustomerExists) || in_array($cus_id, $aCustomerNew)){// Aug 11, 2016 fix không cho tạo trùng trong tháng
                $aCustomerDuplicate[] = $cus_id;
                continue;
            }
            $aCustomerNew[] = $cus_id;
            
            $type_customer  = $aObjCustomer[$cus_id]->is_maintain;
            $sale_id        = $aObjCustomer[$cus_id]->sale_id;
//            $aRowInsert[] = "("
//                    . $this->type . ","
//                    . $cus_id . ","
//                    . $type_customer . ","
//                    . "'" . $this->date_apply . "',"
//                    . $price_12 . ","
//                    . $price_45 . ","
//                    . $cUid
//                    . ")";
            
            $aRowInsert[] = "(
                        '{$this->type }',
                        '{$cus_id}',
                        '{$type_customer}',
                        '{$sale_id}',
                        '{$this->date_apply}',
                        '{$price_12}',
                        '{$price_45}',
                        '{$note}',
                        '{$cUid}',
                        '{$created_date}'
                        )";
            
        }
        $tableName = PriceSupport::model()->tableName();
        if(count($aRowInsert)){
            $sql = 'INSERT INTO ' . $tableName . ' (type,customer_id,type_customer,sale_id,date_apply,price_12,price_45,note,created_by, created_date) VALUES ' . implode(',', $aRowInsert);
            Yii::app()->db->createCommand($sql)->execute();
        }
        return true;
    }
    
    public function canUpdate() {
        $cRole = MyFormat::getCurrentRoleId();
        $cUid = MyFormat::getCurrentUid();
        $aUidAllow = [GasConst::UID_ADMIN, GasConst::UID_CHAU_LNM];
        if(!in_array($cUid, $aUidAllow)){
            return FALSE;
        }
        $dayAllow = date('Y-m-d');// chỉ cho phép nhân viên CallCenter cập nhật trong ngày
        $dayAllow = MyFormat::modifyDays($dayAllow, 25, '-');
        return MyFormat::compareTwoDate($this->created_date, $dayAllow);
    }
    
    public function canDelete() {
        return GasCheck::canDeleteData($this);
    }
    
    protected function beforeSave() {
        $this->trimData();
        return parent::beforeSave();
    }
    
    /** @Author: ANH DUNG Dec 14, 2018
     **/
    public function trimData() {
        $this->price_12     = MyFormat::removeComma($this->price_12);
        $this->price_45     = MyFormat::removeComma($this->price_45);
    }
    
    /** @Author: ANH DUNG Aug 11, 2016
     * @Todo: get array customer by month year, sử dụng để check không trùng khi tạo mới
     */
    public function getCustomerByType($type) {
        $criteria = new CDbCriteria();
        $criteria->compare('t.type', $type);
        $criteria->compare('t.date_apply', $this->date_apply);
        $models = PriceSupport::model()->findAll($criteria);
        return CHtml::listData($models, 'customer_id', 'customer_id');
    }
    
}