<?php

/**
 * This is the model class for table "{{_gas_support_customer}}".
 *
 * The followings are the available columns in table '{{_gas_support_customer}}':
 * @property string $id
 * @property string $code_no
 * @property string $date_request
 * @property string $customer_id
 * @property integer $type_customer
 * @property string $item_name
 * @property integer $type
 * @property string $approved_uid_level_1
 * @property string $approved_date_level_1
 * @property string $approved_note_level_1
 * @property string $approved_uid_level_2
 * @property string $approved_date_level_2
 * @property string $approved_note_level_2
 * @property string $uid_login
 * @property string $created_date
 * @property string $sale_id
 */
class GasSupportCustomer_1 extends CActiveRecord
{
    public $autocomplete_name;
    public $autocomplete_name_second;
    
    // 1: cho muon, 2: tang, 3: ban
    const TYPE_MUON = 1;
    const TYPE_TANG = 2;
    const TYPE_BAN = 3;
    public static $ARR_TYPE = array(
        GasSupportCustomer::TYPE_MUON => 'Cho Mượn',
        GasSupportCustomer::TYPE_TANG => 'Tặng',
        GasSupportCustomer::TYPE_BAN => 'Bán',
    );
    const STATUS_NEW = 1;
    const STATUS_APPROVED_1 = 2;
    const STATUS_APPROVED_2 = 3;
    const STATUS_REJECT = 4;
    const STATUS_APPROVED_3 = 5;
    
    public static $ARR_APPROVED_1 = array(
        GasSupportCustomer::STATUS_APPROVED_1 => 'Duyệt Lần 1',
        GasSupportCustomer::STATUS_REJECT => 'Hủy Bỏ',
    );
    
    /** truong phong kd/giam doc kd
     KH bò thì gửi cho TRưởng pHÒng Bò, Mối thì gửi cho Trưởng PHòng Mối
     Trưởng phòng KD bò-mối duyệt rồi mới lên Giám đốc KD duyệt
     * @note: 1/ check user nào dc phép duyệt lần 1, 2
     */
    public static $ARR_APPROVED_2 = array(
        GasSupportCustomer::STATUS_APPROVED_2 => 'Duyệt Lần 2',
        GasSupportCustomer::STATUS_REJECT => 'Hủy Bỏ',
    );
    
    // Sep 15, 2015 - Anh Long Duyệt cuối
    public static $ARR_APPROVED_3 = array(
        GasSupportCustomer::STATUS_APPROVED_3 => 'Duyệt Lần 3',
        GasSupportCustomer::STATUS_REJECT => 'Hủy Bỏ',
    );
    
    public static $ARR_APPROVED = array(
        GasSupportCustomer::STATUS_NEW => 'Mới',
        GasSupportCustomer::STATUS_APPROVED_1 => 'Duyệt Lần 1',
        GasSupportCustomer::STATUS_APPROVED_2 => 'Duyệt Lần 2',
        GasSupportCustomer::STATUS_APPROVED_3 => 'Duyệt Lần 3',
        GasSupportCustomer::STATUS_REJECT => 'Hủy Bỏ',
    );
    
    public static $ARR_NOT_ALLOW = array(
        GasSupportCustomer::STATUS_APPROVED_2,
        GasSupportCustomer::STATUS_APPROVED_3,
        GasSupportCustomer::STATUS_REJECT,
    );
    
    public static $ARR_NOT_SHOW_HEAD = array( // Những record mới tạo và reject không cho hiện
        GasSupportCustomer::STATUS_NEW,
        GasSupportCustomer::STATUS_REJECT,
    );
    
    public $MAX_ID;
    public $date_from;
    public $date_to;
    
    public $mDetail;
    public $aModelDetail;
    public static $aRoleLevel1 = array( ROLE_HEAD_GAS_BO, ROLE_HEAD_GAS_MOI );
    public static $aRoleLevel2 = array( ROLE_DIRECTOR_BUSSINESS ); // Giám đốc Kinh Doanh
    public static $aRoleAllowCreate = array( ROLE_SALE, ROLE_SUB_USER_AGENT );// Jan 28, 2015 role user dc create new
    public static $aRoleNewCreate = array(ROLE_ADMIN);// chỗ này sẽ sửa lại là role của NV kỹ thuật
    public static $aRoleNewUpdate = array(ROLE_SALE);// chỗ này sẽ sửa lại là role của NV kỹ thuật
    
    public $file_name;
    public $mGasFile;
    public $aIdDetailNotDelete;
    public $file_design;
    const IMAGE_MAX_UPLOAD = 10;
    const IMAGE_MAX_UPLOAD_SHOW = 3;
    public static $AllowFilePdf = 'pdf,xls,xlsx';
    
    const UID_QUY_PV = 295085; //Tổ Trưởng Tổ Bảo Trì - Phan Văn Quý - Sep 15, 2015
    const UID_DUNG_DN = 740905; //Thủ Kho - Đỗ Như Dũng - Sep 15, 2015
//    const UID_more = 740905; //Thủ Kho - Đỗ Như Dũng - Sep 15, 2015
    public static $LIST_UID_NOTIFY = array(
        GasSupportCustomer::UID_QUY_PV,
        GasSupportCustomer::UID_DUNG_DN,
    );
    
    public static $ARR_UNIT = array(
        1 => "Bình",
        2 => "Bộ",
        3 => "Bịch",
        4 => "Cái",
        5 => "Chiếc",
        6 => "Chai",
        7 => "Đôi",
        8 => "Gói",
        9 => "Hộp",
        10 => "Kg",
        11 => "Mét",
        12 => "Sợi",
        13 => "Vỏ",
        14 => "Quả",
    );    
    
    // Dec 27, 2014 lấy số ngày cho phép đại lý cập nhật
    public static function getDayAllowUpdate(){
        return Yii::app()->params['days_update_support_customer'];
    }
    
    /**
     * @Author: ANH DUNG Sep 02, 2015
     */
    public function getDayUpdateToPrint(){
        return Yii::app()->params['days_update_support_customer_to_print'];
    }    
    
    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return GasSupportCustomer the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{_gas_support_customer}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        return array(
            array('deloy_by, date_request, customer_id, type_customer,contact_person, contact_tel, time_doing, price_qty', 'required', 'on'=>'create,update'),
            
            array('customer_id', 'required', 'on'=>'tech_create'),
            
            array('status, id, code_no, date_request, customer_id, type_customer, item_name, type, approved_uid_level_1, approved_date_level_1, approved_note_level_1, approved_uid_level_2, approved_date_level_2, approved_note_level_2, uid_login, created_date', 'safe'),
            array('deloy_by, contact_tel, time_doing, price_qty, contact_person, customer_agent_id, note, date_from,date_to', 'safe'),
            array('approved_uid_level_3, approved_date_level_3, approved_note_level_3', 'safe'),
            array('sale_id', 'safe'),
            array('status', 'required', 'on'=>'Update_status'),
            array('status', 'CheckCanUpdateStatus', 'on'=>'Update_status'),
//            array('contact_person, contact_tel, time_doing, price_qty', 'required', 'on'=>'update_to_print'),
        );
    }
    
    /**
     * @Author: ANH DUNG Dec 27, 2014
     * @Todo: kiểm tra user update status co hop le khong
     */
    public function CheckCanUpdateStatus($attribute,$params)
    {
        $aRoleCheck = array( ROLE_HEAD_GAS_BO, ROLE_HEAD_GAS_MOI ); // TRưởng pHÒng Bò, Mối
        $cRole = Yii::app()->user->role_id;
        $cUid = Yii::app()->user->id;
        $ok = true;
        if(in_array($cRole, $aRoleCheck) && $this->status == GasSupportCustomer::STATUS_APPROVED_2 ){
            $ok = false;
        }
        if( $this->status == GasSupportCustomer::STATUS_NEW ){
            $ok = false;
        }
        
        if(!$ok){
            $this->addError('status','Trạng thái không hợp lệ.');
        }
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        return array(
            'rUidLogin' => array(self::BELONGS_TO, 'Users', 'uid_login'),
            'rCustomer' => array(self::BELONGS_TO, 'Users', 'customer_id'),
            'rApprovedUidLevel1' => array(self::BELONGS_TO, 'Users', 'approved_uid_level_1'),
            'rApprovedUidLevel2' => array(self::BELONGS_TO, 'Users', 'approved_uid_level_2'),
            'rApprovedUidLevel3' => array(self::BELONGS_TO, 'Users', 'approved_uid_level_3'),
            'rDetail' => array(self::HAS_MANY, 'GasSupportCustomerItem', 'support_customer_id',
                'order'=>'rDetail.id ASC',
            ),
            'rDeloyBy' => array(self::BELONGS_TO, 'Users', 'deloy_by'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'code_no' => 'Mã Số',
            'date_request' => 'Ngày',
            'customer_id' => 'Khách Hàng',
            'type_customer' => 'Loại KH',
            'item_name' => 'Tên thiết bị',
            'type' => 'Loại',
            'approved_uid_level_1' => 'Duyệt lần 1',
            'approved_date_level_1' => 'Ngày Duyệt lần 1',
            'approved_note_level_1' => 'Ghi chú lần 1',
            'approved_uid_level_2' => 'Duyệt lần 2',
            'approved_date_level_2' => 'Ngày Duyệt lần 2',
            'approved_note_level_2' => 'Ghi Chú lần 2',
            'approved_uid_level_3' => 'Duyệt lần 3',
            'approved_date_level_3' => 'Ngày Duyệt lần 3',
            'approved_note_level_3' => 'Ghi Chú lần 3',
            'uid_login' => 'Người Tạo',
            'created_date' => 'Ngày Tạo',
            'date_from' => 'Từ Ngày',
            'date_to' => 'Đến Ngày',
            'status' => 'Trạng Thái',
            'note' => 'Ghi Chú',
            'contact_person' => 'Người liên hệ',
            'contact_tel' => 'Tel',
            'time_doing' => 'Thời gian thi công',
            'price_qty' => 'Nhóm giá & Sản lượng',
            'deloy_by' => 'Đơn vị thực hiện',
            'file_design' => 'File bản vẽ thiết kế và danh mục vật tư',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        $cRole = Yii::app()->user->role_id;
        $cUid = Yii::app()->user->id;
        $cAgentId = MyFormat::getAgentId();
        $sStatusNotShow = implode(',', self::$ARR_NOT_SHOW_HEAD);

        $criteria=new CDbCriteria;

        $criteria->compare('t.id',$this->id,true);
        $criteria->compare('t.code_no',$this->code_no,true);            
        $criteria->compare('t.customer_id',$this->customer_id);
        $criteria->compare('t.type_customer',$this->type_customer);
        $criteria->compare('t.status',$this->status);
        
        
        $criteria->compare('t.approved_uid_level_1',$this->approved_uid_level_1);
        $criteria->compare('t.approved_uid_level_2',$this->approved_uid_level_2);            
        $criteria->compare('t.uid_login',$this->uid_login);
        $criteria->compare('t.created_date',$this->created_date,true);
        
        $with = false;
        if(!empty($this->type)){
            $with = true;
            $criteria->compare('rDetail.type',$this->type);
        }
        if(!empty($this->item_name)){
            $with = true;
            $criteria->compare('rDetail.item_name',$this->item_name, true);
        }
        
        if($with){
            $criteria->with = array('rDetail');
            $criteria->together = true;
        }
        
        $date_from = '';
        $date_to = '';        
        if(!empty($this->date_from)){
            $date_from = MyFormat::dateDmyToYmdForAllIndexSearch($this->date_from);
        }
        if(!empty($this->date_to)){
            $date_to = MyFormat::dateDmyToYmdForAllIndexSearch($this->date_to);
        }
        if(!empty($date_from) && empty($date_to))
                $criteria->addCondition("t.date_request>='$date_from'");
        if(empty($date_from) && !empty($date_to))
                $criteria->addCondition("t.date_request<='$date_to'");
        if(!empty($date_from) && !empty($date_to))
                $criteria->addBetweenCondition("t.date_request",$date_from,$date_to);

        if( $cRole == ROLE_HEAD_GAS_BO ){
            $criteria->compare('t.type_customer', STORE_CARD_KH_BINH_BO);
        }elseif( $cRole == ROLE_HEAD_GAS_MOI ){
            $criteria->compare('t.type_customer', STORE_CARD_KH_MOI);
        }elseif( $cRole == ROLE_DIRECTOR_BUSSINESS ){
            $criteria->addCondition("t.approved_uid_level_1 IS NOT NULL AND t.status NOT IN ($sStatusNotShow)" );
        }elseif( $cRole == ROLE_DIRECTOR ){
            $criteria->addCondition("t.approved_uid_level_2 IS NOT NULL AND t.status NOT IN ($sStatusNotShow)" );
        }
        
        if( in_array($cRole, GasSupportCustomer::$aRoleAllowCreate ) ){
            if($cRole == ROLE_SUB_USER_AGENT){
                $criteria->addCondition("t.uid_login = $cUid OR t.deloy_by = $cAgentId ");
                // Sep 09, 2015 cho phép user đại lý xem người thực hiện
            }else{
                $criteria->compare('t.uid_login', $cUid);
            }
        }else{
            // Jan 28, 2015 cho phep user dai ly tao support nua, nen gioi han ke toan view
            GasAgentCustomer::addInConditionAgent($criteria, 't.agent_id');
        }
        
        $criteria->order = "t.id DESC";

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
            'pagination'=>array(
                'pageSize'=> Yii::app()->user->getState('pageSize',Yii::app()->params['defaultPageSize']),
            ),
        ));
    }

    /*
    public function activate()
    {
        $this->status = 1;
        $this->update();
    }

    public function deactivate()
    {
        $this->status = 0;
        $this->update();
    }
	*/

    public function defaultScope()
    {
            return array(
                    //'condition'=>'',
            );
    }
        
    /**
     * @Author: ANH DUNG Dec 27, 2014
     */
    protected function beforeSave() {
        if(strpos($this->date_request, '/')){
            $this->date_request = MyFormat::dateConverDmyToYmd($this->date_request);
            MyFormat::isValidDate($this->date_request);
        }
        if(strpos($this->time_doing, '/')){
            $this->time_doing = MyFormat::datetimeToDbDatetime($this->time_doing);
        }
        
        $this->note = InputHelper::removeScriptTag($this->note);
        if(trim($this->approved_note_level_1)  != ''){
            $this->approved_note_level_1 = InputHelper::removeScriptTag($this->approved_note_level_1);
        }
        if(trim($this->approved_note_level_2)  != ''){
            $this->approved_note_level_2 = InputHelper::removeScriptTag($this->approved_note_level_2);
        }
        if(trim($this->approved_note_level_3)  != ''){
            $this->approved_note_level_3 = InputHelper::removeScriptTag($this->approved_note_level_3);
        }
        
        if($this->isNewRecord){
            $this->uid_login = Yii::app()->user->id;
            $this->code_no = MyFunctionCustom::getNextId('GasSupportCustomer', 'T'.date('y'), MyFormat::MAX_LENGTH_8, 'code_no');
            if($this->rCustomer){
                $this->sale_id = $this->rCustomer->sale_id;
            }
        }
        return parent::beforeSave();
    }
    
    protected function afterSave() {
        if( $this->IsCreateUpdate() ){
            $this->SaveDetailItem();
        }
        if( $this->scenario == "create" ){
            // Sep 16, 2015 chắc là chỉ có update thì mới send mail, kiểm tra chỉ send mail cho lần update đầu tiên
            // còn với đại lý thì sẽ send mail khi create 
            GasScheduleEmail::BuildListNotifySupportCustomer($this->id);
        }
        return parent::afterSave();
    }
    
    /**
     * @Author: ANH DUNG Sep 15, 2015
     * @Todo: save detail 
     */
    public function SaveDetailItem() {
//        MyFormat::deleteModelDetailByRootId('GasSupportCustomerItem', $model->id, 'support_customer_id');
        // không thể delete detail vì còn các cấp trên update % cho từng detail
        $this->DeleteSomeDetail();
        foreach( $this->aModelDetail as $mDetail )
        {
            $mDetail->percent = $mDetail->buildJsonPercent(1);
            // Dec 30, 2014 chỗ này không cần 1 build sql, vì cảm thấy lưu lượng dùng chỗ này cũng ít
            $mDetail->support_customer_id = $this->id;
            if($mDetail->id)
                $mDetail->update();
            else
                $mDetail->save();
        }
    }
    
    /**
     * @Author: ANH DUNG Sep 17, 2015
     * @Todo: delete 
     * @Param: $model
     */
    public function DeleteSomeDetail() {
        if(is_array($this->aIdDetailNotDelete) && count($this->aIdDetailNotDelete)){
            $criteria = new CDbCriteria;
            $criteria->compare('support_customer_id', $this->id);
            $criteria->addNotInCondition('id', $this->aIdDetailNotDelete);
            GasSupportCustomerItem::model()->deleteAll($criteria);
        }
    }    
    
    
    
    protected function beforeValidate() {
        $cRole = Yii::app()->user->role_id;
        $cUid = Yii::app()->user->id;
        if($this->rCustomer){
            $this->type_customer = $this->rCustomer->is_maintain;
            $this->customer_agent_id = Users::GetAgentIdOfCustomer($this->customer_id);
        }
        
        if( $cRole == ROLE_SUB_USER_AGENT ){
            // Jan 28, 2015 fix cho user dai ly dc phep tao support, nen se save ca agent id nua
            $this->agent_id = MyFormat::getAgentId();
        }        
        
        if( $this->IsCreateUpdate() ){
            self::BuildArrDetail($this);
        }
        
        return parent::beforeValidate();
    }
    
    /**
     * @Author: ANH DUNG Dec 30, 2014
     * @Todo: check scenario only for create and update 
     * @Param: $model
     */
    public function IsCreateUpdate() {
        return in_array($this->scenario, array('tech_create','tech_update'));
    }
    
    protected function afterValidate() {
        if(count($this->aModelDetail) < 1 && $this->IsCreateUpdate() ){
            $this->addError('date_request', "Chưa nhập tên thiết bị");
        }
        return parent::afterValidate();
    }
    
    /**
     * @Author: ANH DUNG Dec 29, 2014
     * @Todo: build array model detail from post
     * @Param: $model is model GasSupportCustomer
     */
    public static function BuildArrDetail($model) {
        $model->aModelDetail = array();
        $model->aIdDetailNotDelete = array();
        if(isset($_POST['GasSupportCustomerItem']['item_name']) && is_array($_POST['GasSupportCustomerItem']['item_name'])){
            foreach( $_POST['GasSupportCustomerItem']['item_name'] as $key=>$item_name){                
                $item_name = trim(InputHelper::removeScriptTag($item_name));
                $type = isset($_POST['GasSupportCustomerItem']['type'][$key])?$_POST['GasSupportCustomerItem']['type'][$key]:"";
                $unit = isset($_POST['GasSupportCustomerItem']['unit'][$key])?$_POST['GasSupportCustomerItem']['unit'][$key]:"";
                $qty = isset($_POST['GasSupportCustomerItem']['qty'][$key])?$_POST['GasSupportCustomerItem']['qty'][$key]:"";
                $price = isset($_POST['GasSupportCustomerItem']['price'][$key])?$_POST['GasSupportCustomerItem']['price'][$key]:"";
                $percent = isset($_POST['GasSupportCustomerItem']['percent'][$key])?$_POST['GasSupportCustomerItem']['percent'][$key]:"";
                $id = isset($_POST['GasSupportCustomerItem']['id'][$key])?$_POST['GasSupportCustomerItem']['id'][$key]:"";
                
                if($item_name != ''){
                    if(!empty($id)){
                        $mDetail = GasSupportCustomerItem::model()->findByPk($id);
                        if(!$mDetail){
                            continue;
                        }
                        $model->aIdDetailNotDelete[] = $id;
                    }else{
                        $mDetail = new GasSupportCustomerItem();
                    }
                    $mDetail->item_name = $item_name;
                    $mDetail->unit = $unit;
                    $mDetail->qty = $qty;
                    $mDetail->price = $price;
                    $mDetail->percent = $percent;
                    $mDetail->amount = $mDetail->qty*$mDetail->price;
                    
                    $mDetail->type = $type;
                    $model->aModelDetail[] = $mDetail;
                }
            }
        }
    }
    
    /**
     * @Author: ANH DUNG Dec 27, 2014
     */
    public static function GetTypeSupport($model) {
        if(isset( GasSupportCustomer::$ARR_TYPE[$model->type]))
        {
            return GasSupportCustomer::$ARR_TYPE[$model->type];
        }
        return '';
    }
    
    /**
     * @Author: ANH DUNG Dec 27, 2014
     */
    public static function GetStatusSupport($model) {
        if(isset( GasSupportCustomer::$ARR_APPROVED[$model->status]))
        {
            return GasSupportCustomer::$ARR_APPROVED[$model->status];
        }
        return '';
    }
    
    /**
     * @Author: ANH DUNG Dec 27, 2014
     * @Todo: check valid user can view and approved for this support
     * Mới chỉ giới hạn user là user KH bò thì gửi cho TRưởng pHÒng Bò, Mối thì gửi cho Trưởng PHòng Mối
     * Trưởng phòng KD bò-mối duyệt rồi mới lên Giám đốc KD duyệt
     * @Param: $model
     * @Return: true if allow view, false if not
     */
    public static function IsOwner($model) {
        $aRoleCheck = array( ROLE_HEAD_GAS_BO, ROLE_HEAD_GAS_MOI, ROLE_DIRECTOR_BUSSINESS ); // TRưởng pHÒng Bò, Mối
        $cRole = Yii::app()->user->role_id;
        $cUid = Yii::app()->user->id;
        $ok = true;
        if( $cRole == ROLE_HEAD_GAS_BO &&  $model->type_customer != STORE_CARD_KH_BINH_BO ){
            $ok = false;
        }elseif( $cRole == ROLE_HEAD_GAS_MOI &&  $model->type_customer != STORE_CARD_KH_MOI ){
            $ok = false;
        }
        return $ok;
    }
    
    /**
     * @Author: ANH DUNG Dec 27, 2014
     * @Todo: check user can update status
     * @Param: $model
     */
    public static function CanUpdateStatus($model) {
        $cRole = Yii::app()->user->role_id;
        $cUid = Yii::app()->user->id;
        $ok = true;
        if( $cRole == ROLE_HEAD_GAS_BO &&  $model->type_customer != STORE_CARD_KH_BINH_BO 
                && in_array($model->status, GasSupportCustomer::$ARR_NOT_ALLOW)
                ){
            $ok = false;
        }elseif( $cRole == ROLE_HEAD_GAS_MOI &&  $model->type_customer != STORE_CARD_KH_MOI 
                && in_array($model->status, GasSupportCustomer::$ARR_NOT_ALLOW)
                ){
            $ok = false;
        }
        $dayAllow = date('Y-m-d');
        $dayAllow = MyFormat::modifyDays($dayAllow, GasSupportCustomer::getDayAllowUpdate(), '-');
        $ok2 = MyFormat::compareTwoDate($model->created_date, $dayAllow);
        return ($ok && $ok2);
    }
    
    
    /**
     * @Author: ANH DUNG Dec 27, 2014
     * @Todo: handle update status
     * @Param: $model
     */
    public static function HandleUpdateStatus($model) {
        $aRoleCheck = array( ROLE_HEAD_GAS_BO, ROLE_HEAD_GAS_MOI ); // TRưởng pHÒng Bò, Mối
        $aRoleHeadKD = array( ROLE_DIRECTOR_BUSSINESS ); // GD kinh Doanh
        $aRoleHeadRoot = array( ROLE_DIRECTOR ); // Tong Giam Doc
        $cRole = Yii::app()->user->role_id;
        $cUid = Yii::app()->user->id;
//        $note = isset($_POST['GasSupportCustomer']['approved_note_level_1'])?$_POST['GasSupportCustomer']['approved_note_level_1']:$_POST['GasSupportCustomer']['approved_note_level_2'];
        if(in_array($cRole, $aRoleCheck)){
            $model->approved_uid_level_1 = $cUid;
            $model->approved_date_level_1 = date('Y-m-d H:i:s');
            $model->update(array('status', 'approved_uid_level_1', 'approved_date_level_1', 'approved_note_level_1'));
        }elseif(in_array($cRole, $aRoleHeadKD)){
            $model->approved_uid_level_2 = $cUid;
            $model->approved_date_level_2 = date('Y-m-d H:i:s');
            $model->update(array('status', 'approved_uid_level_2', 'approved_date_level_2', 'approved_note_level_2'));
        }elseif(in_array($cRole, $aRoleHeadRoot)){
            $model->approved_uid_level_3 = $cUid;
            $model->approved_date_level_3 = date('Y-m-d H:i:s');
            $model->update(array('status', 'approved_uid_level_3', 'approved_date_level_3', 'approved_note_level_3'));
        }
        
        GasScheduleEmail::BuildListNotifySupportCustomer($model->id);
    }
    
    
    /**
     * @Author: ANH DUNG Dec 27, 2014
     * @Todo: get last update status
     * @Param: $model
     */
    public static function GetLastUpdateStatus($model) {
        $res = '';
        $cmsFormater = new CmsFormatter();
        if( !empty($model->approved_uid_level_2) ){
            $res = $model->rApprovedUidLevel2->first_name."<br>".$cmsFormater->formatDateTime($model->approved_date_level_2);
        }elseif( !empty($model->approved_uid_level_1) ){
            $res = $model->rApprovedUidLevel1->first_name."<br>".$cmsFormater->formatDateTime($model->approved_date_level_1);
        }
        return $res;
    }
    
    /**
     * @Author: ANH DUNG Dec 30, 2014
     * @Todo: get last update status
     * @Param: $model
     */
    public static function GetLastUpdateNote($model) {
        $res = '';       
        if( !empty($model->approved_uid_level_2) ){
            $res = $model->approved_note_level_2;
        }elseif( !empty($model->approved_uid_level_1) ){
            $res = $model->approved_note_level_1;
        }else{
            $res = $model->note; // ghi chu luc moi tao
        }
        return $res;
    }
    
    /**
     * @Author: ANH DUNG Dec 28, 2014
     * @Todo: get history support of this customer
     * @Param: $customer_id
     * @Param: $aIdNotIn array id not in search
     */
    public static function GetHistorySupport($customer_id, $aIdNotIn) {
        $criteria = new CDbCriteria();
        $criteria->compare('t.customer_id', $customer_id);
        if(count($aIdNotIn)){
            $criteria->addNotInCondition('t.id', $aIdNotIn);
        }
        $criteria->order = "t.id DESC";
        return self::model()->findAll($criteria);
    }
    
    /**
     * @Author: ANH DUNG Dec 30, 2014
     * @Todo: get list model user lien quan ( dung de notify ) trong 1 model support customer
     * @Param: $support_customer_id
     */
    public static function GetListModelUserNotify($support_customer_id) {
        $mSupportCustomer = self::model()->findByPk($support_customer_id);
        $aRoleCheck = array( ROLE_HEAD_GAS_BO, ROLE_HEAD_GAS_MOI, ROLE_DIRECTOR_BUSSINESS ); // TRưởng pHÒng Bò, Mối
        $aRoleToNotify = array( ROLE_HEAD_GAS_BO ); // TRưởng pHÒng Bò, Mối
        $mSupportCustomer->getRoleNotify($aRoleToNotify);
        
//        $aModelUser = Users::getFindAllByArrayRole($aRoleToNotify);
        $aUidNotReset = array($mSupportCustomer->uid_login);
        $needMore = array('aRoleId' => $aRoleToNotify, 'aUidNotReset'=>$aUidNotReset);
        $aModelUserMail = Users::GetListUserMail($needMore);
        
        $mSale = Users::model()->findByPk($mSupportCustomer->uid_login);
        if(is_array($aModelUserMail)){
            $aModelUserMail[] = $mSale;
        }else{
            $aModelUserMail = array($mSale);
        }
//        MyFormat::AddEmailAnhDung($aModelUserMail);// only for test, close when done test
        // Sep 04, 2015 - notify cho đại lý nếu được duyệt
        GasSupportCustomer::getMailSubUserAgent($mSupportCustomer, $aModelUserMail);
        return $aModelUserMail;
    }
    
    /**
     * @Author: ANH DUNG Sep 15, 2015
     * @Todo: get role to notify
     * [11:02:46] ngoc_kien1: nhan vien kinh doanh de xuat
        [11:03:05] ngoc_kien1: gui mail cho tp
        [11:03:50] ngoc_kien1: tp duyet gui mail cho gd, nhan vien
        [11:04:09] ngoc_kien1: gd duyet thi gui tong gd, nhan vien
     */
    public function getRoleNotify( &$aRoleToNotify ) {
        if ( $this->type_customer == STORE_CARD_KH_MOI ){
            $aRoleToNotify = array( ROLE_HEAD_GAS_MOI ); // TRưởng pHÒng Bò, Mối
        }
        if ( $this->status == GasSupportCustomer::STATUS_APPROVED_1 ){
            $aRoleToNotify = array( ROLE_DIRECTOR_BUSSINESS ); // TRưởng pHÒng Bò, Mối
        }elseif ($this->status == GasSupportCustomer::STATUS_APPROVED_2) {
            $aRoleToNotify = array( ROLE_DIRECTOR ); // TRưởng pHÒng Bò, Mối
        }
    }
    
    /**
     * @Author: ANH DUNG Sep 19, 2015
     * @Todo: get email notify to subuser agent - Don vi thuc hien
     */
    public static function getMailSubUserAgent($mSupportCustomer, &$aModelUserMail) {
        // sua lai duyet lan cuoi moi send
        // 3. nv ke toan gui mail cho 2- 3 nguoi, id se define
        // 4. to truong bao tri bo phan bao tri - Phan Van Quy
        // 5. thu kho Do Nhu Dung
        $aModelSubAgent = array();
        $aModelUserNotify = array();
        if( !empty($mSupportCustomer->approved_uid_level_3) && $mSupportCustomer->status == GasSupportCustomer::STATUS_APPROVED_3){
            // tổng giám đốc duyệt lần cuối thì mới send cho các bộ phận khác
            // 1. mail người thực hiện
            $aModelSubAgent = Users::getModelSubUserAgentByAgentId($mSupportCustomer->deloy_by);
            // 2. mail các ID User liên quan như trên nói
            $aModelUserNotify = Users::getOnlyArrayModelByArrayId(self::$LIST_UID_NOTIFY);
        }
        
        foreach( $aModelSubAgent as $model){
            $aModelUserMail[] = $model;
        }
        
        foreach( $aModelUserNotify as $model){
            $aModelUserMail[] = $model;
        }
    }
    
    /**
     * @Author: ANH DUNG Sep 12, 2015
     * @Todo: check user can update to print
     */
    public function canUpdateToPrint() {
        $ok = false;
        if( !empty($this->approved_uid_level_1) || !empty($this->approved_uid_level_2) ){
            $ok = true;
            // nếu Usser đã cập nhật to print rồi thì check chỉ cho user 
            // sửa cái print đó trong vòng 1 ngày
            //getDayUpdateToPrint()
            if(!empty($this->last_update_time)){
                $dayAllow = date('Y-m-d');
                $dayAllow = MyFormat::modifyDays($dayAllow, $this->getDayUpdateToPrint(), '-');
                $ok = MyFormat::compareTwoDate($this->last_update_time, $dayAllow);
                // last_update_time > $dayAllow là đúng
            }
        }
        return $ok;
    }
    
    /**
     * @Author: ANH DUNG Sep 02, 2015
     * @Todo: only update some field to print
     */
    public function UpdateForPrint() {
        $aUpdate = array('contact_person','contact_tel', 'time_doing', 'price_qty');
        if(empty($this->last_update_time)){
            $this->last_update_time = date('Y-m-d H:i:s');
            $aUpdate[] = 'last_update_time';
        }
        $this->time_doing = MyFormat::datetimeToDbDatetime($this->time_doing);
        $this->update($aUpdate);
    }
    
    /**
     * @Author: ANH DUNG Sep 02, 2015
     */
    public function getContactPerson() {
        return $this->contact_person;
    }
    
    public function getContactTel() {
        return $this->contact_tel;
    }
    public function getTimeDoing() {
        $res =  trim(MyFormat::datetimeDbDatetimeUser($this->time_doing), "00");
        return trim($res, ":");
    }
    public function getPriceQty() {
        return $this->price_qty;
    }
    
    public function canPrint() {
        if( !empty($this->approved_uid_level_3) ){
            return true;
        }
        return false;
//        if($this->last_update_time){
//            return true;
//        }
//        return false;
    }
    
    /**
     * @Author: ANH DUNG Sep 04, 2015
     * @Todo: get array model file
     */
    public function getFile() {
        if(empty($this->id)){
            return array();
        }
        $criteria = new CDbCriteria;
        $criteria->compare('t.belong_id', $this->id);
        $criteria->compare('t.type', GasFile::TYPE_2_SUPPORT_CUSTOMER);
        $criteria->order = "t.id";
        return GasFile::model()->findAll($criteria);
    }
    
    /**
     * @Author: ANH DUNG Sep 04, 2015
     * @Todo: delete file
     */
    public function deleteFile() {
        if(isset($_POST['delete_file']) && is_array($_POST['delete_file'])){
            $criteria = new CDbCriteria;
            $criteria->compare('t.belong_id', $this->id);
            $criteria->addInCondition('t.id', $_POST['delete_file']);
            $models = GasFile::model()->findAll($criteria);
            foreach($models as $model){
                $model->delete();
            }
        }
    }
    
    /**
     * @Author: ANH DUNG Sep 04, 2015
     * @Todo: get list file upload 
     */
    public function renderListFile() {
        $res = "";
        foreach($this->getFile() as $key=>$mFile)
        {
            $br="<br>";
            if($key==0){
                $br="";
            }
            $res .= "$br".($key+1).". ".$mFile->getForceLinkDownload();
        }
        return $res;
    }
    
    protected function beforeDelete() {
        MyFormat::deleteModelDetailByRootId('GasSupportCustomerItem', $this->id, 'support_customer_id');
        GasFile::DeleteByBelongIdAndType($this->id, GasFile::TYPE_2_SUPPORT_CUSTOMER);
        return parent::beforeDelete();
    }
    
    /**
     * @Author: ANH DUNG Sep 16, 2015
     * @Todo: get Select percent đầu tư
     */
    public function getListOptionPercent() {
        $aRes = array();
        $i=0;
        while( $i<=100 ){
            $aRes[$i] = $i;
            $i += 5;
        }
        return $aRes;
    }
    
    /**
     * @Author: ANH DUNG Sep 17, 2015
     * @Todo: check user can update record
     */
    public function canUpdate() {
        $cRole = Yii::app()->user->role_id;
        $cUid = Yii::app()->user->id;
        if(in_array($cRole, GasSupportCustomer::$aRoleNewCreate)
            && $cUid == $this->uid_login && !empty($this->contact_person)
        ){
            return true;
        }
        if(in_array($cRole, GasSupportCustomer::$aRoleNewUpdate)
            && $cUid == $this->sale_id
        ){
            return true;
        }
        return false;
    }
    
}