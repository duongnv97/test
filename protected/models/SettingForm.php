<?php

/**
 * LoginForm class.
 * LoginForm is the data structure for keeping
 * user login form data. It is used by the 'login' action of 'SiteController'.
 */
class SettingForm extends CFormModel
{
    //email
    public $transportType; //php or smtp
    public $smtpHost, $smtpUsername, $smtpPassword, $smtpPort, $encryption;

    //general
    public $dateFormat, $timeFormat, $adminEmail, $autoEmail, $login_limit_times, $time_refresh_login;
    //paypal
    public $paypalURL, $paypal_email_address, $paypalType;
    public $twitter, $facebook, $linkedin, $rss;
    public $title, $meta_description, $meta_keywords;

    public static $_paypalURL;
    public $title_all_mail, $image_watermark, $image_watermark2;
    //mailchimp
    public $mailchimp_on, $mailchimp_api_key, $mailchimp_list_id, $mailchimp_title_groups;

    // for cron job newsletter
    public $server_name;

    public $note_type_pay;
    public $enable_limit_customer_of_agent;
    public $limit_update_maintain;
    public $can_update_customer_maintain;
    public $server_maintenance;
    public $server_maintenance_message;
    public $time_disable_login;
    public $allow_session_menu;
    public $show_popup_news;

    // set up delete or update in system config
    public $delete_global_days;

    public $storecard_admin_update;
    public $storecard_admin_delete;
    public $storecard_agent_updateCollectionCustomer;

    public $gas_remain_admin_delete;
    public $gas_remain_agent_update;
    public $gas_remain_agent_update_remain2_3;

    public $PTTT_update_file_scan;
    public $PTTT_SELL_update_file_scan;
    public $days_update_bussiness_contract;
    public $month_limit_search_pttt;
    public $limit_post_ticket;
    public $ticket_page_size;
    public $cookie_days;
    public $days_keep_track_login;
    public $days_update_text_file; // số ngày cho phép cập nhật văn bản
    public $days_update_profile_scan; // số ngày cho phép cập nhật hồ sơ pháp lý
    public $days_update_manage_tool; // số ngày cho phép cập nhật quản lý công cụ dụng cụ
    public $enable_delete; // cờ cho phép xóa trên toàn hệ thống
    public $days_update_leave; // số ngày cho phép cập nhật don xin nghi phep
    public $days_update_meeting_minutes; // số ngày cho phép cập nhật don xin nghi phep
    public $days_update_maintain_sell;
    public $days_update_guide_help;
    public $month_limit_update_thuong_luong;
    public $days_update_customer_check;
    public $days_update_customer_check_report;
    public $allow_admin_login;
    public $profile_day_alert_expiry; // số ngày cảnh báo hết hạn hồ sơ pháp lý
    public $allow_use_admin_cookie; // Dec 13, 2014 cho phép dùng cookie admin để login?
    public $days_update_break_task; // Dec 14, 2014 ngày cho phép update phân công công việc
    public $month_update_first_purchase; //Dec 21, 2014  số tháng limit để update first_purchase của spancop
    public $days_update_support_customer; //Dec 27, 2014  số ngay limit để update gas support customer
    public $month_statistic_output_customer; // Dec 28, 2014  số tháng thống kê sản lượng của KH
    public $check_login_same_account; // Jan 09, 2015  check 1 account 2 user login cùng 1 lúc
    public $days_update_customer_bo_moi; // Jan 16, 2015 ngày cho phép update kh thẻ kho
    public $days_update_PTTT_daily_goback; // Mar 15, 2015 ngày cho phép update grid binh quay ve hang ngay
    public $max_van_ban_create_in_day; // Apr 10, 2015 số văn bản cho phép tạo trong 1 ngày - liên quan đến gửi mail quá nhiều
    public $days_update_support_customer_to_print; // Sep 02, 2015 số ngày cho phép sửa để print support
    public $days_update_uphold; // Oct 21, 2015 số ngày cho phép sửa grid bao tri
    public $days_update_support_agent; // Now 02, 2015 số ngày cho phép sửa grid 
    public $days_update_order_promotion; // Now 02, 2015 số ngày cho phép sửa grid 
    public $email_cron_primary; // Jan 27, 2016
    public $email_cron_reset_pass; // Jan 27, 2016
    public $days_update_settle; // Trung June 03, 2016 Thời gian cho phép cập nhật thông tinh quyết toán
    public $days_update_sell; // Anh Dung Aug 15, 2016 update bán hàng C#
    public $DaysUpdateBorrow; // Anh Dung Aug 15, 2016 
    public $DaysUpdateSettle; // Anh Dung Aug 24, 2016 
    public $EnableChangeExt, $DaysExpiryTamUng, $AppVersionCode, $AppVersionCodeGas24hIos, $AppVersionCodeGas24hAndroid, $DaysUpdateHgd, $EnableUpdateSell; // Anh Dung Aug 29, 2016 
    public $DaysUpdateFixAll, $DaysUpdateAppBoMoi, $DaysSetDebitAppBoMoi;
    public $Gas24hPrice, $Gas24hApplyPrice, $distanceAgentApp;
    public $hrIdCalcAttendance, $hrIdCalcSalary;// Anh Dung Add Jul3118
    public $bankDiscountFirstTime, $bankDiscountSecondTime, $goldTimeDiscount, $goldTimeList, $goldTimeText;// Anh Dung Add Oct1918
    public $listUserDebugLog, $time5MUpdateSetting;// Anh Dung Add Now2018
    public $DaysGasEventMarket;
    // set up delete or update in system config

    public function rules()
    {
        $return = array();
        foreach( SettingForm::$arrGeneral as $key=>$value):
            $return[] = array($value, 'safe');
        endforeach;
        foreach( SettingForm::$arrSmtp as $key=>$value):
            $return[] = array($value, 'safe');
        endforeach;

//        $return []= array('adminEmail, autoEmail', 'required');
        $return []=array('smtpPort, login_limit_times, time_refresh_login', 'numerical', 'integerOnly' => true);
//        $return []=array('adminEmail, autoEmail', 'email');
        $return []=array('image_watermark2', 'file','on'=>'updateSettings',
            'allowEmpty'=>true,
            'types'=> 'jpg,gif,png,tiff',
            'wrongType'=>'Only jpg,gif,png,tiff allowed',
            'maxSize' => 1024 * 1024 * 3, // 8MB
            'tooLarge' => 'The file was larger than 3MB. Please upload a smaller file.',
        );
        
        $return []=array('image_watermark2', 'match', 'pattern'=>'/^[^\\/?*:&;{}\\\\]+\\.[^\\/?*:&;{}\\\\]{3}$/', 'message'=>'Image files name cannot include special characters: &%$#', 'on'=>'updateSettings');
        $return []=array('transportType, smtpHost, dateFormat, timeFormat, smtpUsername, smtpPassword, encryption', 'length', 'max'=>100);
        $return []=array('title, meta_description, meta_keywords, address, phone, email, googleMap', 'safe');
        return $return;
    }

    public static $arrSmtp = array('host' => 'smtpHost', 'username' => 'smtpUsername', 'password' => 'smtpPassword',
    'port' => 'smtpPort', 'encryption' => 'encryption');

    public static $arrGeneral = array(
//    'dateFormat' => 'dateFormat', 'timeFormat' => 'timeFormat',  
//    'paypalURL' => 'paypalURL', 'paypal_email_address' => 'paypal_email_address',
//    'twitter' => 'twitter', 'facebook' => 'facebook', 'linkedin' => 'linkedin', 'rss' => 'rss', 
//    'mailchimp_on' => 'mailchimp_on',
//    'mailchimp_api_key' => 'mailchimp_api_key',
//    'mailchimp_list_id' => 'mailchimp_list_id',
//    'mailchimp_title_groups' => 'mailchimp_title_groups',

    'adminEmail' => 'adminEmail',
    'autoEmail' => 'autoEmail',
    'meta_description' => 'meta_description',
    'meta_keywords' => 'meta_keywords', 'image_watermark' => 'image_watermark', 'login_limit_times' => 'login_limit_times',
    'time_refresh_login' => 'time_refresh_login',
    'title_all_mail' => 'title_all_mail',
    'title' => 'title',
    'server_name' => 'server_name',
    'note_type_pay' => 'note_type_pay',
    'enable_limit_customer_of_agent' => 'enable_limit_customer_of_agent',
    'limit_update_maintain' => 'limit_update_maintain',
    'can_update_customer_maintain' => 'can_update_customer_maintain',
    'server_maintenance' => 'server_maintenance',
    'server_maintenance_message' => 'server_maintenance_message',
    'time_disable_login' => 'time_disable_login',
    'allow_session_menu' => 'allow_session_menu',
    'show_popup_news' => 'show_popup_news',
    'delete_global_days' => 'delete_global_days',
    'storecard_admin_update' => 'storecard_admin_update',
    'storecard_admin_delete' => 'storecard_admin_delete',
    'storecard_agent_updateCollectionCustomer' => 'storecard_agent_updateCollectionCustomer',
    'gas_remain_admin_delete' => 'gas_remain_admin_delete',
    'gas_remain_agent_update' => 'gas_remain_agent_update',
    'gas_remain_agent_update_remain2_3' => 'gas_remain_agent_update_remain2_3',
    'PTTT_update_file_scan' => 'PTTT_update_file_scan',
    'PTTT_SELL_update_file_scan' => 'PTTT_SELL_update_file_scan',
    'days_update_bussiness_contract' => 'days_update_bussiness_contract',
    'month_limit_search_pttt' => 'month_limit_search_pttt',
    'limit_post_ticket' => 'limit_post_ticket',
    'ticket_page_size' => 'ticket_page_size',
    'cookie_days' => 'cookie_days',
    'days_keep_track_login' => 'days_keep_track_login',
    'days_update_text_file' => 'days_update_text_file',
    'days_update_profile_scan' => 'days_update_profile_scan',
    'days_update_manage_tool' => 'days_update_manage_tool',
    'enable_delete' => 'enable_delete',
    'days_update_leave' => 'days_update_leave',
    'days_update_meeting_minutes' => 'days_update_meeting_minutes',
    'days_update_maintain_sell' => 'days_update_maintain_sell',
    'days_update_guide_help' => 'days_update_guide_help',
    'month_limit_update_thuong_luong' => 'month_limit_update_thuong_luong',
    'days_update_customer_check' => 'days_update_customer_check',
    'days_update_customer_check_report' => 'days_update_customer_check_report',
    'allow_admin_login' => 'allow_admin_login',
    'profile_day_alert_expiry' => 'profile_day_alert_expiry',
    'allow_use_admin_cookie' => 'allow_use_admin_cookie',
    'days_update_break_task' => 'days_update_break_task',
    'month_update_first_purchase' => 'month_update_first_purchase',
    'days_update_support_customer' => 'days_update_support_customer',
    'month_statistic_output_customer' => 'month_statistic_output_customer',
    'check_login_same_account' => 'check_login_same_account',
    'days_update_customer_bo_moi' => 'days_update_customer_bo_moi',
    'days_update_PTTT_daily_goback' => 'days_update_PTTT_daily_goback',
    'max_van_ban_create_in_day' => 'max_van_ban_create_in_day',
    'days_update_support_customer_to_print' => 'days_update_support_customer_to_print',
    'days_update_uphold' => 'days_update_uphold',
    'days_update_support_agent' => 'days_update_support_agent',
    'days_update_order_promotion' => 'days_update_order_promotion',
    'email_cron_primary' => 'email_cron_primary',
    'email_cron_reset_pass' => 'email_cron_reset_pass',
        'days_update_settle' => 'days_update_settle',
        'days_update_sell'  => 'days_update_sell',
        'DaysUpdateBorrow'  => 'DaysUpdateBorrow',
        'DaysUpdateSettle'  => 'DaysUpdateSettle',
        'DaysExpiryTamUng'  => 'DaysExpiryTamUng',
        'AppVersionCode'    => 'AppVersionCode',
        'AppVersionCodeGas24hIos' => 'AppVersionCodeGas24hIos',
        'AppVersionCodeGas24hAndroid' => 'AppVersionCodeGas24hAndroid',
        'DaysUpdateHgd'     => 'DaysUpdateHgd',
        'EnableUpdateSell'  => 'EnableUpdateSell',
        'EnableChangeExt'   => 'EnableChangeExt',
        'DaysUpdateFixAll'   => 'DaysUpdateFixAll',
        'DaysUpdateAppBoMoi'    => 'DaysUpdateAppBoMoi',
        'DaysSetDebitAppBoMoi'  => 'DaysSetDebitAppBoMoi',
        'Gas24hApplyPrice'      => 'Gas24hApplyPrice',
        'Gas24hPrice'           => 'Gas24hPrice',
        'distanceAgentApp'      => 'distanceAgentApp',
        'hrIdCalcAttendance'    => 'hrIdCalcAttendance',
        'hrIdCalcSalary'        => 'hrIdCalcSalary',
//        'HgdPriceInit'      => 'HgdPriceInit',// Dec 28, 2016 độc lập với phần form setting trên giao diện
//        'HgdAmountAdjust'   => 'HgdAmountAdjust',// độc lập với phần form setting trên giao diện
        'bankDiscountFirstTime'     => 'bankDiscountFirstTime',
        'bankDiscountSecondTime'    => 'bankDiscountSecondTime',
        'goldTimeDiscount'          => 'goldTimeDiscount',
        'goldTimeList'              => 'goldTimeList',
        'goldTimeText'              => 'goldTimeText',
        'listUserDebugLog'              => 'listUserDebugLog',
        'time5MUpdateSetting'              => 'time5MUpdateSetting',
        'DaysGasEventMarket'        => 'DaysGasEventMarket',

        );

    /**
     * Override configurations.
     */
    static public function applySettings()
    {
        try {
        //apply setting for paypal
        if (Yii::app()->setting->getItem('transportType')) {
            Yii::app()->mail->transportType = Yii::app()->setting->getItem('transportType');
        }
        if (Yii::app()->mail->transportType == 'smtp') {
            foreach(self::$arrSmtp as $key => $value)
            {
                if (Yii::app()->setting->getItem($value))
                {
                    Yii::app()->mail->transportOptions[$key] = Yii::app()->setting->getItem($value);
                }
            }

        } else {
            Yii::app()->mail->transportOptions = '';
        }

        //apply setting for general
        foreach(self::$arrGeneral as $key => $value)
        {
            if (Yii::app()->setting->getItem($value))
            {
                Yii::app()->params[$key] = Yii::app()->setting->getItem($value);
            }
        }

        self::$_paypalURL = Yii::app()->params['paypalURL'];
        //apply setting for paypal

        if (Yii::app()->setting->getItem('title')) {
            Yii::app()->name = Yii::app()->setting->getItem('title');
        }
        } catch (Exception $exc) {
            echo $exc->getMessage();die();// Jun2218 không thể throw tiếp đc, phải stop ở chỗ này
        }
    }
    
    /**
     * @Author: ANH DUNG Feb 02, 2017
     * @Todo: gom code từ controller
     */
    public function setSetting($setting) {
        $setting->setDbItem('transportType', $this->transportType);
        foreach(SettingForm::$arrSmtp as $key => $value){
            $setting->setDbItem($value, $this->$value);
        }
        foreach(SettingForm::$arrGeneral as $key => $value){
            if($value != 'image_watermark')
            $setting->setDbItem($value, $this->$value);                        
        }
        $setting->setDbItem('title', $this->title);

        $file = CUploadedFile::getInstance($this,'image_watermark2');
        if($file !== null){
            $baseImagePath = ROOT . '/upload/admin/settings/';
            $name = preg_replace('/\.\w+$/', '', $file->name);
            $newName = $name . '_' . time() . rand(1,10000).'.' . $file->extensionName;
            Yii::log($newName, 'error');
            if($file->saveAs($baseImagePath.$newName))
            {
                $this->image_watermark = $newName;
                $setting->setDbItem('image_watermark', $newName);
            }
        } else{
            $this->image_watermark = $setting->getItem('image_watermark');
            $setting->setDbItem('image_watermark', $setting->getItem('image_watermark'));
        }
    }
    
    /**
     * @Author: ANH DUNG Feb 02, 2017
     * @Todo: gom code từ controller
     */
    public function getSetting($setting) {
        $temp = $setting->getItem('transportType');
        if (!empty($temp)) {
            $this->transportType = $setting->getItem('transportType');
        } else if(!empty(Yii::app()->mail->transportType)) {
            $this->transportType = Yii::app()->mail->transportType;
        }
        foreach(SettingForm::$arrSmtp as $key => $value){
            $temp = $setting->getItem($value);
            if (!empty($temp)) {
                $this->$value = $setting->getItem($value);
            } else if(!empty(Yii::app()->mail->transportOptions[$key])) {
                $this->$value = Yii::app()->mail->transportOptions[$key];
            }
        }
        foreach(SettingForm::$arrGeneral as $key => $value){
            $temp = $setting->getItem($value);
            if (!empty($temp)) {
                $this->$value = $setting->getItem($value);
            } else if(!empty(Yii::app()->mail->transportOptions[$key])) {
                $this->$value = Yii::app()->mail->transportOptions[$key];
            }
        }
        $temp = $setting->getItem('title');
        if (!empty($temp)) {
            $this->title = $setting->getItem('title');
        } else if(!empty(Yii::app()->params['title'])) {
            $this->title = Yii::app()->params['title'];
        }
    }
    
    public function getTimeUpdateSetting() {
        $nextTime = Yii::app()->setting->getItem('time5MUpdateSetting');
        if(empty($nextTime)){
            return '';
        }
        return MyFormat::dateConverYmdToDmy($nextTime, 'd/m/Y H:i');
    }
    
}
