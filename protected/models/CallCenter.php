<?php

class CallCenter extends Call{
    public $name_search = '';
    const TYPE_HOI_GAS_GS_DETAIL        = 2;
    const TYPE_HOI_GAS_AGENT            = 3;
    public $date_from,$date_to,$autocomplete_user;
    /******************HOANG NAM********************************/
    public function rules() {
        $aRules = parent::rules();
        $aRules[] = array('date_from,date_to','safe');
        return $aRules;
    }
    
    public function attributeLabels(){
        $aLabels = parent::attributeLabels();
        return $aLabels;
    }
    /** @Author: HOANG NAM 30/05/2018
     *  @Todo: get Hoi Gas
     **/
    public function getHoiGas(){
        $aData = [];
        $aData['Sum'] = [];
        $aAgentMonitor = GasOneMany::getManyIdByType(GasOneMany::TYPE_AGENT_OF_MONITOR);
        $aData['GS'] = $this->getUserByListID($aAgentMonitor);
        $mCall = new Call();
        $statusSelect = GasConst::SELL_HOI_GAS;
        $criteria = new CDbCriteria;
        $criteria->select = 't.agent_id,count(id) as id';
        $criteria->compare('failed_status', $statusSelect);
        if(!empty($this->date_from)){
            $date_from = MyFormat::dateConverDmyToYmd($this->date_from,'-');
            $criteria->addCondition('t.created_date_only >= "'.$date_from.'"');
        }
        if(!empty($this->date_to)){
            $date_to = MyFormat::dateConverDmyToYmd($this->date_to,'-');
            $criteria->addCondition('t.created_date_only <= "'.$date_to.'"');
        }
        if(!empty($this->user_id)){
            $aAgent = [];
            foreach ($aAgentMonitor as $agent => $monitor) {
                if($monitor == $this->user_id){
                    $aAgent[] = $agent;
                }
            }
            if(count($aAgent) <= 0){
                return $aData;
            }
            $stringSearch = implode(',', $aAgent);
            $criteria->addCondition("t.agent_id IN ($stringSearch)");
        }
        $this->setTypeSearch($criteria);
        $criteria->group = 't.agent_id';
        $criteria->order = "id DESC";
        $aCall = $mCall->findAll($criteria);
        foreach ($aCall as $key => $mCall) {
           if(isset($aAgentMonitor[$mCall->agent_id])){
               if(!isset($aData['Sum'][$aAgentMonitor[$mCall->agent_id]])){
                    $aData['Sum'][$aAgentMonitor[$mCall->agent_id]] = $mCall->id;
               }else{
                    $aData['Sum'][$aAgentMonitor[$mCall->agent_id]] += $mCall->id;
               }
           }else{
//              hiển thị danh sách đại lý không có giám sát.
//               if(!isset($aData['Sum'][$mCall->agent_id])){
//                    $aData['Sum'][$mCall->agent_id] = $mCall->id;
//               }else{
//                    $aData['Sum'][$mCall->agent_id] += $mCall->id;
//               }
           }
        }
        uasort($aData['Sum'], function ($item1, $item2) {
                return $item2 >= $item1;
        });
        $this->name_search  = 'Giám sát';
        $session = Yii::app()->session;
        $session['data-model-hoi-gas'] = $this;
        return $aData;
    }
    
    public function getHoiGasDetail(){
        $aData = []; $aAgent = []; $mCall = new Call(); $statusSelect = GasConst::SELL_HOI_GAS;
        $aData['Sum'] = [];
        $criteria = new CDbCriteria;
        $criteria->select = 't.agent_id,count(id) as id';
        $criteria->compare('failed_status', $statusSelect);
        if(!empty($this->date_from)){
            $date_from = MyFormat::dateConverDmyToYmd($this->date_from,'-');
            $criteria->addCondition('t.created_date_only >= "'.$date_from.'"');
        }
        if(!empty($this->date_to)){
            $date_to = MyFormat::dateConverDmyToYmd($this->date_to,'-');
            $criteria->addCondition('t.created_date_only <= "'.$date_to.'"');
        }
        if(!empty($this->user_id)){
            $aAgentMonitor = GasOneMany::getManyIdByType(GasOneMany::TYPE_AGENT_OF_MONITOR);
            $aAgent = [];
            foreach ($aAgentMonitor as $agent => $monitor) {
                if($monitor == $this->user_id){
                    $aAgent[] = $agent;
                }
            }
            if(count($aAgent) <= 0){
                return $aData;
            }
            $stringSearch = implode(',', $aAgent);
            $criteria->addCondition("t.agent_id IN ($stringSearch)");
            $aData['GS'] = $this->getUserByListID($aAgent);
        }else{
            return $aData;
        }
        $this->setTypeSearch($criteria);
        $criteria->group = 't.agent_id';
        $criteria->order = "id DESC";
        $aCall = $mCall->findAll($criteria);
        foreach ($aCall as $key => $mCall) {
            if(!isset($aData['Sum'][$mCall->agent_id])){
                 $aData['Sum'][$mCall->agent_id] = $mCall->id;
            }else{
                 $aData['Sum'][$mCall->agent_id] += $mCall->id;
            }
        }
        $this->name_search  = 'Đại lý';
        $session = Yii::app()->session;
        $session['data-model-hoi-gas'] = $this;
        return $aData;
    }
    
    public function getHoiGasAgent(){
        
        $aData = []; $aAgent = []; $mCall = new Call(); $statusSelect = GasConst::SELL_HOI_GAS;
        $aData['Sum'] = [];
        if(empty($this->agent_id)){
        /*Chỉ lấy danh sách agent default*/
//            $aAgentMonitor = GasOneMany::getManyIdByType(GasOneMany::TYPE_AGENT_OF_MONITOR);
//            foreach ($aAgentMonitor as $agent => $monitor) {
//                $aAgent[] = $agent;
//            }
        }else{
            $aAgent[] = $this->agent_id;
        }
        $aData['GS'] = $this->getUserByListID($aAgent);
        $criteria = new CDbCriteria;
        $criteria->select = 't.agent_id,count(id) as id';
        $criteria->compare('failed_status', $statusSelect);
        if(!empty($this->date_from)){
            $date_from = MyFormat::dateConverDmyToYmd($this->date_from,'-');
            $criteria->addCondition('t.created_date_only >= "'.$date_from.'"');
        }
        if(!empty($this->date_to)){
            $date_to = MyFormat::dateConverDmyToYmd($this->date_to,'-');
            $criteria->addCondition('t.created_date_only <= "'.$date_to.'"');
        }
        if(count($aAgent) > 0){
            $stringSearch = implode(',', $aAgent);
            $criteria->addCondition("t.agent_id IN ($stringSearch)");
        }
        $this->setTypeSearch($criteria);
        $criteria->group = 't.agent_id';
        $criteria->order = "id DESC";
        $aCall = $mCall->findAll($criteria);
        foreach ($aCall as $key => $mCall) {
            if(!isset($aData['Sum'][$mCall->agent_id])){
                 $aData['Sum'][$mCall->agent_id] = $mCall->id;
            }else{
                 $aData['Sum'][$mCall->agent_id] += $mCall->id;
            }
        }
        $this->name_search  = 'Đại lý';
        $session = Yii::app()->session;
        $session['data-model-hoi-gas'] = $this;
            return $aData;
    }
    
    /** @Author: HOANG NAM 31/05/2018
     *  @Todo: get GS
     **/
    public function getUserByListID($ListId){
        $aValueOfUser=[];
        if(count($ListId) > 0){
            $criteria = new CDbCriteria;
            $stringSearch = implode(',',$ListId);
            $criteria->addCondition("t.id IN ($stringSearch)");
            $aUser = Users::model()->findAll($criteria);
            foreach ($aUser as $key => $mUser) {
                $aValueOfUser[$mUser->id] = $mUser->getAttributes();
            }
        }
        return $aValueOfUser;
    }
    
    /** @Author: HOANG NAM 07/06/2018
     *  @Todo: get agent by id
     **/
    public function getFieldAgent($id,$field){
        $model = Users::model()->findByPk($id);
        return !empty($model) ? $model->$field : '';
    }
    
    /** @Author: HOANG NAM 21/06/2018
     *  @Todo: chỉnh type search là hộ gia đình hay bò mối
     **/
    public function setTypeSearch(&$criteria){
        $cHgd = $cBoMoi = '';
        $sParamsInExtBoMoi  = implode(',', $this->getListExtBoMoi());
        if($this->type == Call::QUEUE_HGD){
            $cHgd           = 't.destination_number NOT IN ('.$sParamsInExtBoMoi.')';
        }elseif($this->type == Call::QUEUE_BO_MOI){
            $cBoMoi         = 't.destination_number IN ('.$sParamsInExtBoMoi.')';
        }
        if(!empty($cHgd)){
            $criteria->addCondition($cHgd);
        }
        if(!empty($cBoMoi)){
            $criteria->addCondition($cBoMoi);
        }
    }
    /**********************************************************/
    
    
}
