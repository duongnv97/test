<?php

/**
 * This is the model class for table "{{_hr_function_types}}".
 *
 * The followings are the available columns in table '{{_hr_function_types}}':
 * @property integer $id
 * @property string $name
 * @property string $description
 * @property integer $status
 */
class HrFunctionTypes extends BaseSpj {

    //-----------------------------------------------------
    // Constants
    //-----------------------------------------------------
    const STATUS_INACTIVE               = 0;
    const STATUS_ACTIVE                 = 1;
    
    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return HrFunctionTypes the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return '{{_hr_function_types}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        return array(
            array('name', 'required'),
            array('status', 'numerical', 'integerOnly' => true),
            array('name', 'length', 'max' => 255),
            array('description', 'safe'),
            array('id, name, description, status', 'safe'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        return array(
            'rSalaryReport' => array(
                self::HAS_MANY, 'HrSalaryReports', 'type_id',
                'on' => 'status != ' . DomainConst::DEFAULT_STATUS_INACTIVE,
            ),
            'rFunction' => array(
                self::HAS_MANY, 'HrFunctions', 'type_id',
                'on' => 'status != ' . DomainConst::DEFAULT_STATUS_INACTIVE,
            ),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => DomainConst::CONTENT00003,
            'name' => DomainConst::CONTENT00042,
            'description' => DomainConst::CONTENT00062,
            'status' => DomainConst::CONTENT00026,
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search() {
        $criteria = new CDbCriteria;

        $criteria->compare('t.id', $this->id);
        $criteria->compare('t.name', $this->name, true);
        $criteria->compare('t.description', $this->description, true);
        $criteria->compare('t.status', $this->status);
        $criteria->order = 't.id DESC';
        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
            'pagination' => array(
                'pageSize' => Yii::app()->user->getState('pageSize', Yii::app()->params['defaultPageSize']),
            ),
        ));
    }

    //-----------------------------------------------------
    // Parent override methods
    //-----------------------------------------------------
    /**
     * Override before delete method
     * @return Parent result
     */
    protected function beforeDelete() {
        $retVal = true;
        // Check foreign table Functions
        $functions = HrFunctions::model()->findByAttributes(array('type_id' => $this->id));
        if (count($functions) > 0) {
            $retVal = false;
        }
        // Check foreign table Salary report
        $reports = HrSalaryReports::model()->findByAttributes(array('type_id' => $this->id));
        if (count($reports) > 0) {
            $retVal = false;
        }
        return $retVal;
    }

    //-----------------------------------------------------
    // Utility methods
    //-----------------------------------------------------  
    public function canUpdate() {
        return true;
    }
    //-----------------------------------------------------
    // Static methods
    //----------------------------------------------------- 
    /**
     * Loads the function types items for the specified type from the database
     * @param type $emptyOption boolean the item is empty
     * @return type List data
     */
    public static function loadItems($emptyOption = false, $isSchedule = false) {
        $_items = array();
        if ($emptyOption) {
            $_items[""] = "";
        }
        $models = self::model()->findAll(array(
            'order' => 'id ASC',
        ));
        foreach ($models as $model) {
            if(!$isSchedule && $model->id == UsersHr::getIdWorkSchedule()) continue;
            $_items[$model->id] = $model->name;
        }
        return $_items;
    }
    
    /**
     * Get list status of object
     * @return Array
     */
    public static function getStatus() {
        return array(
            self::STATUS_INACTIVE  => DomainConst::CONTENT00028,
            self::STATUS_ACTIVE    => DomainConst::CONTENT00027,
        );
    }
    
    /**
     * Get list all types
     * @return Array
     */
    public static function getArrayTypes() {
        return HrFunctionTypes::loadItems();
    }
    
    public static function getArrayOrderedTypes() {
        $aType = HrFunctionTypes::model()->findAll();
        $userHr = new UsersHr();
        $aSortRules = [
            $userHr->getIdCalcAttendance(), // Chấm công
            $userHr->getIdTimeWages(),      // Lương thời gian
            $userHr->getIdCalcOutput(),     // Sản lượng
            $userHr->getIdCalcSupport(),    // Phụ cấp
            $userHr->getIdPrepaidWages(),   // Tạm ứng
            $userHr->getIdMoneyWithHeld(),  // Tiền giữ lại
//            $userHr->getIdCalcSalary(),     // Tính lương/thu nhập (cái này ở sau cùng)
        ];
        $aSortRulePos = array_flip($aSortRules); // Value => key
        $aSort        = []; // Những tab cần sort
        $aRemain      = []; // Những tab còn lại (tạo mới sau này)
        $salary       = []; // Tính lương/thu nhập
        $i            = count($aSortRulePos);
        foreach ($aType as $value) {
            if($value->id == UsersHr::getIdWorkSchedule()) continue;
            if($value->id == $userHr->getIdCalcSalary()){
                $salary[count($aType)-1] = $value;
                continue;
            }
            $pos = isset($aSortRulePos[$value->id]) ? $aSortRulePos[$value->id] : false;
            if($pos !== false){
                $aSort[$pos]    = $value;
            } else {
                $aRemain[$i++]  = $value;
            }
        }
        $res = $aSort + $aRemain + $salary;
        ksort($res);
        return $res;
    }
}
