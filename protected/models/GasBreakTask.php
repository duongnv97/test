<?php

/**
 * This is the model class for table "{{_gas_break_task}}".
 *
 * The followings are the available columns in table '{{_gas_break_task}}':
 * @property string $id
 * @property string $code_no
 * @property string $employee_id
 * @property string $job_description
 * @property string $deadline
 * @property integer $status
 * @property string $uid_login
 * @property string $created_date
 */
class GasBreakTask extends BaseSpj
{
    const STATUS_NEW                = 1;
    const STATUS_PROCESSING         = 2;
    const STATUS_COMPLETE           = 3;
    const STATUS_CLOSE_BY_LEADER    = 4;
    
    public static $ARR_STATUS = array(
        GasBreakTask::STATUS_NEW => "Mới Tạo",
        GasBreakTask::STATUS_PROCESSING => "Đang Xử Lý",
        GasBreakTask::STATUS_COMPLETE => "Hoàn Thành",
        GasBreakTask::STATUS_CLOSE_BY_LEADER => "Closed Job",
    );
    
    public static $ARR_STATUS_UPDATE = array(
        GasBreakTask::STATUS_PROCESSING => "Đang Xử Lý",
        GasBreakTask::STATUS_COMPLETE => "Hoàn Thành",
    );
    
    public static $ARR_STATUS_UPDATE_LEADER = array(
        GasBreakTask::STATUS_PROCESSING => "Đang Xử Lý",
        GasBreakTask::STATUS_COMPLETE => "Hoàn Thành",
        GasBreakTask::STATUS_CLOSE_BY_LEADER => "Closed Job",
    );
    
    public static $ARR_STATUS_NOT_DONE = array(
        GasBreakTask::STATUS_NEW,
        GasBreakTask::STATUS_PROCESSING,
        GasBreakTask::STATUS_COMPLETE,
    );
    
    const MAX_LENGTH_NO = 8;
    public $MAX_ID, $autocomplete_user;
        
    public static function getDayAllowUpdate(){
        return Yii::app()->params['days_update_break_task'];
    }    
    
    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return GasBreakTask the static model class
     */
    public static function model($className=__CLASS__)
    {
            return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
            return '{{_gas_break_task}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        return array(
            array('employee_id, job_description, deadline, status', 'required', 'on'=>'create, update'),
            array('job_description', 'length', 'max'=>450),
            array('date_begin, id, code_no, employee_id, job_description, deadline, status, uid_login, created_date', 'safe'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        return array(
            'rUidLogin' => array(self::BELONGS_TO, 'Users', 'uid_login'),
            'rEmployeeId' => array(self::BELONGS_TO, 'Users', 'employee_id'),
            'rBreakTaskDaily' => array(self::HAS_MANY, 'GasBreakTaskDaily', 'break_task_id',
                'order'=>'rBreakTaskDaily.id DESC',
            ),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'code_no' => 'Mã Số',
            'employee_id' => 'Nhân Viên',
            'job_description' => 'Công Việc',
            'deadline' => 'Thời Hạn',
            'status' => 'Trạng Thái',
            'uid_login' => 'Người Tạo',
            'created_date' => 'Ngày Tạo',
            'date_begin' => 'Ngày Thực Hiện',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        $criteria=new CDbCriteria;

        $criteria->compare('t.id',$this->id,true);
        $criteria->compare('t.code_no',$this->code_no,true);
        
        $criteria->compare('t.job_description',$this->job_description,true);
        
        if(!empty($this->deadline)){
            $this->deadline = MyFormat::dateDmyToYmdForAllIndexSearch($this->deadline);
            $criteria->compare('t.deadline',$this->deadline);
        }
        if(!empty($this->date_begin)){
            $this->date_begin = MyFormat::dateDmyToYmdForAllIndexSearch($this->date_begin);
            $criteria->compare('t.date_begin',$this->date_begin);
        }
        
        $aRoleLimit = array( ROLE_EMPLOYEE_OF_LEGAL );
        $cRole = Yii::app()->user->role_id;
        if(in_array($cRole, $aRoleLimit)){
            $criteria->compare( 't.employee_id', Yii::app()->user->id );
        }else{
            $criteria->compare( 't.employee_id', $this->employee_id );
        }
        
        if(!empty($this->status)){
            $criteria->compare('t.status', $this->status);
        }else{
            $sParamsIn = implode(',', GasBreakTask::$ARR_STATUS_NOT_DONE);
            $criteria->addCondition("t.status IN ($sParamsIn)");
        }
        
        $criteria->compare('t.uid_login',$this->uid_login,true);
//        $criteria->compare('t.created_date',$this->created_date,true);
        $criteria->order = "t.id DESC";

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
            'pagination'=>array(
//                'pageSize'=> Yii::app()->user->getState('pageSize',Yii::app()->params['defaultPageSize']),
                'pageSize'=> 50,
            ),
        ));
    }

    /*
    public function activate()
    {
        $this->status = 1;
        $this->update();
    }

    public function deactivate()
    {
        $this->status = 0;
        $this->update();
    }
	*/

    public function defaultScope()
    {
            return array(
                    //'condition'=>'',
            );
    }
    
    /**
     * @Author: ANH DUNG Dec 14, 2014
     * @Todo: something
     * @Param: $model
     */
    protected function beforeSave() {
        if(strpos($this->deadline, '/')){
            $this->deadline = MyFormat::dateConverDmyToYmd($this->deadline);
            MyFormat::isValidDate($this->deadline);
        }
        if(strpos($this->date_begin, '/')){
            $this->date_begin = MyFormat::dateConverDmyToYmd($this->date_begin);
            MyFormat::isValidDate($this->date_begin);
        }
        
        $this->job_description = InputHelper::removeScriptTag($this->job_description);
        
        if($this->isNewRecord){
            $this->uid_login = Yii::app()->user->id;
            $this->code_no = MyFunctionCustom::getNextId('GasBreakTask', 'T'.date('y'), GasBreakTask::MAX_LENGTH_NO, 'code_no');
        }
        return parent::beforeSave();
    }
    
    /**
     * @Author: ANH DUNG Dec 14, 2014
     * @Todo: Get Status for break task
     * @Param: $model
     */
    public static function GetStatusText( $model ) {
        if( isset(GasBreakTask::$ARR_STATUS[$model->status]) ){
            $res = GasBreakTask::$ARR_STATUS[$model->status];
            $today = date('Y-m-d');
            $IsExpired = MyFormat::compareTwoDate($today, $model->deadline);
            if( $IsExpired && $model->status != GasBreakTask::STATUS_COMPLETE){
                $res = "<span class='high_light_tr'>$res</span>";
            }
            return $res;
        }
        return '';
    }
    
    /**
     * @Author: ANH DUNG Dec 14, 2014
     * @Todo: check valid user can view or post report for this Break Task
     * Mới chỉ giới hạn user là user nhân viên pháp lý
     * @Param: $model
     * @Return: true if allow view, false if not
     */
    public static function IsOwnerTask($model) {
        $aRoleCheck = array( ROLE_EMPLOYEE_OF_LEGAL );
        $cRole = Yii::app()->user->role_id;
        $cUid = Yii::app()->user->id;
        if( in_array($cRole, $aRoleCheck) && $cUid != $model->employee_id ){
            return false;
        }
        return true;
    }
    
    /**
     * @Author: ANH DUNG Dec 15, 2014
     * @Todo: get select box list employee
     * @Param: $model
     */
    public static function GetListSelectEmployee() {
        return Users::getArrUserByRole( array(ROLE_HEAD_OF_LEGAL, ROLE_EMPLOYEE_OF_LEGAL), array('order'=>'t.role_id ASC, t.code_bussiness ASC', 'get_name_and_role'=>1));
    }
    
    protected function beforeDelete() {
        MyFormat::deleteModelDetailByRootId('GasBreakTaskDaily', $this->id, 'break_task_id');
        return parent::beforeDelete();
    }
    
}