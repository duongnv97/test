<?php

/**
 * This is the model class for table "{{_users_info}}".
 *
 * The followings are the available columns in table '{{_users_info}}':
 * @property string $id
 * @property string $user_id
 * @property string $sale_id
 * @property string $phone
 * @property string $reward_point
 * @property string $reward_point_used
 * @property string $days_per_one
 * @property integer $role_id
 * @property string $first_name
 * @property string $address
 * @property integer $agent_id
 * @property integer $is_maintain
 * @property string $first_purchase
 * @property string $last_purchase
 * @property string $created_date
 * @property string $created_date_bigint
 * @property string $count_sell
 */
class UsersInfo extends BaseSpj {

    const FLAG_FIX_UPDATE           = 1;
    const FLAG_FIX_NEED_UPDATE      = 0;
    const LIMIT_UPDATE              = 5000;
    const FIELD_FIRST_SELL          = 'first_purchase';
    const FIELD_LAST_SELL           = 'last_purchase';
    const FIELD_COUNT_SELL          = 'count_purchase';
    public $autocomplete_user,$autocomplete_name_3;
    public $point_from,$point_to,$per_from,$per_to,$count_sell_to;
    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return UsersInfo the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return '{{_users_info}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        return array(
            array('date_from,date_to,count_sell_to,reward_point_used,point_from, point_to, per_from, per_to, id, user_id, reward_point, days_per_one, role_id, first_name, address, agent_id, is_maintain, first_purchase, last_purchase, created_date, created_date_bigint, count_sell', 'safe'),
        );
    }
    
    public function getArrayNumber($from,$to,$strView = '',$range = 1){
        $start = $from;
        $end = $to;
        $key = range($start,$end,$range);
        $valueView = range($start,$end,$range);
        array_walk($valueView, function(&$value, $key,$strView) { $value .= $strView; },$strView);
        return array_combine($key,$valueView);
    }
    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'rUser' => array(self::BELONGS_TO, 'Users', 'user_id'),
            'rAgent' => array(self::BELONGS_TO, 'Users', 'agent_id'),
            'rSale' => array(self::BELONGS_TO, 'Users', 'sale_id'),
        );
    }
    
    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'user_id' => 'Khách hàng',
            'reward_point' => 'Điểm thưởng hiện tại',
            'days_per_one' => 'Tần xuất lấy gas',
            'first_name' => 'Tên KH',
            'address' => 'Địa chỉ',
            'agent_id' => 'Đại lý',
            'is_maintain' => 'Loại KH',
            'first_purchase' => 'Đơn hàng đầu',
            'last_purchase' => 'Đơn hàng cuối',
            'created_date' => 'Ngày tạo',
            'created_date_bigint' => 'Ngày tạo',
            'count_sell' => 'Số đơn hàng',
            'phone' => 'SĐT',
            'sale_id' => 'Nhân viên sale',
            'reward_point_used' => 'Điểm đã xử dụng',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search() {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.
        
        $criteria = new CDbCriteria;

        $criteria->compare('t.user_id', $this->user_id);
        $criteria->compare('t.agent_id', $this->agent_id);
        if(is_array($this->is_maintain)){
            $criteria->addInCondition('t.is_maintain', $this->is_maintain);
        }else{
            $criteria->compare('t.is_maintain', $this->is_maintain);
        }
        if(isset($this->count_sell) && $this->count_sell != ''){
            $criteria->addCondition('t.count_sell >= '.$this->count_sell);
        }
        if(isset($this->count_sell_to) && $this->count_sell_to != ''){
            $criteria->addCondition('t.count_sell <= '.$this->count_sell_to);
        }
        if(isset($this->point_from) && $this->point_from != ''){
            $criteria->addCondition('t.reward_point >= '.$this->point_from);
        }
        if(isset($this->point_to) && $this->point_to != ''){
            $criteria->addCondition('t.reward_point <= '.$this->point_to);
        }
        if(isset($this->per_from) && $this->per_from != ''){
            $criteria->addCondition('t.days_per_one >= '.$this->per_from);
        }
        if(isset($this->per_to) && $this->per_to != ''){
            $criteria->addCondition('t.days_per_one <= '.$this->per_to);
        }
        if(!empty($this->date_from)){
            $date_from = MyFormat::dateDmyToYmdForAllIndexSearch($this->date_from);
//            $criteria->addCondition("DATE(t.created_date) >= '$date_from'");
            DateHelper::searchGreater($date_from, 'created_date_bigint', $criteria);
        }
        if(!empty($this->date_to)){
            $date_to = MyFormat::dateDmyToYmdForAllIndexSearch($this->date_to);
//            $criteria->addCondition("DATE(t.created_date) <= '$date_to'");
            DateHelper::searchSmaller($date_to, 'created_date_bigint', $criteria, true);
        }
        $criteria->order = 't.reward_point DESC';
        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }
    
    public function getFirstName(){
        return '<a target="_blank" href="/admin/sell/index/sCustomer/'.$this->user_id.'">'.$this->first_name.'</a>';
    }
    
    public function getDayPerOne(){
        return $this->days_per_one;
    }
    
    public function getCustomerType(){
        return isset(CmsFormatter::$CUSTOMER_BO_MOI[$this->is_maintain]) ? CmsFormatter::$CUSTOMER_BO_MOI[$this->is_maintain] : '';
    }
    
    public function getPoint(){
        return $this->reward_point;
    }
    
    public function getPointUsed(){
        return $this->reward_point_used;
    }
    
    public function getFirstPurchase(){
        return MyFormat::dateConverYmdToDmy($this->first_purchase, 'd/m/Y');
    }
    
    public function getLastPurchase(){
        return MyFormat::dateConverYmdToDmy($this->last_purchase, 'd/m/Y');
    }
    
    public function getCountSell(){
        return $this->count_sell;
    }
    
    public function getSale(){
        return !empty($this->rSale) ? $this->rSale->first_name : '';
    }
    
    public function getAddress(){
        return $this->address;
    }
    
    public function getPhone(){
        $aPhone     = explode('-', $this->phone);
        $aResult    = !empty($aPhone[0]) ? $aPhone[0] : '';
        return $aResult;
    }
    
    public function getAgentName(){
        return !empty($this->rAgent) ? $this->rAgent->first_name : '';
    }
    
    /** @Author: NamNH Jul 19, 2019
     *  @Todo: get sell of users
     *  @Param:$order
     **/
    public function getFirstSellByOrder($mUser,$order = 'ASC') {
        $result = '';
        if(empty($mUser->id)){
            return $result;
        }
        $mTargetDaily = new TargetDaily();
        $aPhone = explode('-', $mUser->phone);
        $UserId = $mTargetDaily->getUsersIdByPhone($aPhone);
        $criteria = new CDbCriteria;
        $criteria->addInCondition('t.customer_id', $UserId);
        $criteria->compare('t.status', Sell::STATUS_PAID);
        $criteria->order = 't.id '.$order;
        $criteria->limit = 1;
        $mSell = Sell::model()->find($criteria);
        if(!empty($mSell)){
            $result =  $mSell->created_date_only;
        }
        return $result;
    }
    
    /** @Author: NamNH Jul 19, 2019
     *  @Todo: get sell of users
     *  @Param:$order
     **/
    public function getCountSellByUser($mUser) {
        $mTargetDaily = new TargetDaily();
        $aPhone = explode('-', $mUser->phone);
        $UserId = $mTargetDaily->getUsersIdByPhone($aPhone);
        $criteria = new CDbCriteria;
        $criteria->select = 'DISTINCT sell_id';
        $criteria->addInCondition('t.customer_id', $UserId);
        $criteria->compare('t.status', Sell::STATUS_PAID);
        $aVoBinh = GasMaterialsType::getArrVoAndGas();
        $criteria->addInCondition("t.materials_type_id", $aVoBinh);
        $criteria->group = 't.sell_id';
        $count = SellDetail::model()->count($criteria);
        return $count;
    }
    
    /** @Author: NamNH Jul 19, 2019
     *  @Todo: get info sell of users
     **/
    public function getInfoSellOfCustomer($mUser){
        $first_purchase = $this->getFirstSellByOrder($mUser,'ASC');
        $last_purchase  = $this->getFirstSellByOrder($mUser,'DESC');
        $count_purchase = $this->getCountSellByUser($mUser);
        return [
            self::FIELD_FIRST_SELL  => $first_purchase,
            self::FIELD_LAST_SELL   => $last_purchase,
            self::FIELD_COUNT_SELL  => $count_purchase
        ];
    }
    
    /** @Author: NamNH Jul 19, 2019
     *  @Todo: get all forecast
     **/
    public function getDayForecast($aUserId){
        $criteria = new CDbCriteria;
        $criteria->addInCondition('t.customer_id', $aUserId);
        $aForeCast = Forecast::model()->findAll($criteria);
        return $aForeCast;
    }
    /** @Author: NamNH Jul 19, 2019
     *  @Todo: cron data from model users
     **/
    public function getDataFromUsers($onlyUsersId = false,$isFirst = false) {
        $from = time();
        $criteria = new CDbCriteria;
        if($onlyUsersId){ // Thực hiện thêm nếu không tồn tại
            $criteria->compare('t.id', $this->user_id);
        }else{
            $criteria->addCondition('t.flag_fix_update = '.self::FLAG_FIX_NEED_UPDATE);
        }
        $aRowInsert = [];
        $criteria->addCondition('t.role_id = ' . ROLE_CUSTOMER);
        $criteria->limit = self::LIMIT_UPDATE;
        $criteria->order = 't.id DESC';
        $aUsers = Users::model()->findAll($criteria);
        $aUserId = CHtml::listData($aUsers, 'id', 'id');
        $aForeCast = $this->getDayForecast($aUserId);
        $aDayPerOne = CHtml::listData($aForeCast, 'customer_id', 'days_per_one');
        $mRewardPoint = new RewardPoint();
        foreach ($aUsers as $key => $mUser) {
            if(!$isFirst){
                $infoSell               = $this->getInfoSellOfCustomer($mUser);
            }else{
                $dateCurent = date('Y-m-d');
                $infoSell = [
                                self::FIELD_FIRST_SELL  => $dateCurent,
                                self::FIELD_LAST_SELL   => $dateCurent,
                                self::FIELD_COUNT_SELL  => 1,
                            ];
            }
            $user_id                = $mUser->id;
            $phone                  = MyFormat::removeBadCharacters($mUser->phone);
            $sale_id                = $mUser->sale_id;
            $reward_point           = $mRewardPoint->calculatePoint($mUser->id);
            $reward_point_used      = $mRewardPoint->getExchangePoint($mUser->id);
            $days_per_one           = !empty($aDayPerOne[$mUser->id]) ? $aDayPerOne[$mUser->id] : 0;
            $role_id                = $mUser->role_id;
            $first_name             = MyFormat::removeBadCharacters($mUser->first_name);
            $address                = MyFormat::removeBadCharacters($mUser->address);
            $agent_id               = $mUser->area_code_id;
            $is_maintain            = $mUser->is_maintain;
            $first_purchase         = $infoSell[self::FIELD_FIRST_SELL];
            $last_purchase          = $infoSell[self::FIELD_LAST_SELL];
            $created_date           = $mUser->created_date;
            $created_date_bigint    = $mUser->created_date_bigint;
            $count_sell             = $infoSell[self::FIELD_COUNT_SELL];
            $created_by             = $mUser->created_by;
            $aRowInsert[]="(
                '$user_id',
                '$phone',
                '$sale_id',
                '$reward_point',
                '$reward_point_used',
                '$days_per_one',
                '$role_id',
                '$first_name',
                '$address',
                '$agent_id',
                '$is_maintain',
                '$first_purchase',
                '$last_purchase',
                '$created_date',
                '$created_date_bigint',
                '$count_sell',
                '$created_by'
            )";
        }
        if(count($aRowInsert) > 0){
            $this->insertUsersInfo($aRowInsert);
        }
        $this->updateFlagUsers($aUserId);
        $to = time();
        $second = $to-$from;
        $messLog = 'getDataFromUsers done '.count($aRowInsert).' in:+ '.($second).'  Second  <=> '.($second/60).' Minutes '.date('Y-m-d H:i:s');
//        Logger::WriteLog($messLog);
    }
    
    public function updateFlagUsers($aUser){
        if(empty($aUser)){
            return;
        }
        $tableNameTargetDaily       = Users::model()->tableName();
        $strIdUpdate    = implode(',', array_keys($aUser));
        $sql    ="UPDATE  `$tableNameTargetDaily` "
                . 'SET flag_fix_update = '.self::FLAG_FIX_UPDATE
                . ' WHERE id IN ('.$strIdUpdate.')';
        $sql    .= ';';
        Yii::app()->db->createCommand($sql)->execute();
    }
    /** @Author: NamNH Jul 19, 2019
     *  @Todo: insert users info
     **/
    public function insertUsersInfo($aRowInsert){
        $tableName = UsersInfo::model()->tableName();
        $sql = "insert into $tableName (
                    user_id,
                    phone,
                    sale_id,
                    reward_point,
                    reward_point_used,
                    days_per_one,
                    role_id,
                    first_name,
                    address,
                    agent_id,
                    is_maintain,
                    first_purchase,
                    last_purchase,
                    created_date,
                    created_date_bigint,
                    count_sell,
                    created_by
                    ) values ".implode(',', $aRowInsert);
        if(count($aRowInsert)>0){
            Yii::app()->db->createCommand($sql)->execute();
        }
    }
    
    /** @Author: NamNH Jul 23, 2019
     *  @Todo: cron add new customer to 
     *  @Param:$dateRun Y-m-d
     **/
    public function updateInfoByDate($dateRun) {
        $from = time();
        $this->getDataFromUsers();//Thực hiện thêm các khách hàng vừa mơi tạo vào table users_infos
        $criteria=new CDbCriteria;
        DateHelper::searchBetween($dateRun, $dateRun, 'complete_time_bigint', $criteria, true);
        $criteria->compare('t.status', Sell::STATUS_PAID);
        $aSell          = Sell::model()->findAll($criteria);
        $UserId         = CHtml::listData($aSell, 'customer_id', 'customer_id');
        $this->updateInfoByUId($UserId);
        $to = time();
        $second = $to-$from;
        $info = "updateInfoByDate $dateRun done in:+ " . ($second) . '  Second  <=> ' . ($second / 60) . ' Minutes ' . date('Y-m-d H:i:s');
        Logger::WriteLog($info);
    }
    
    /** @Author: NamNH Jul 23, 2019
     *  @Todo: update users_info by user_id
     *  @Param:$aUId
     **/
    public function updateInfoByUId($aUId) {
        $criteria=new CDbCriteria;
        $criteria->addInCondition('t.user_id', $aUId);
        $aUsersInfo = UsersInfo::model()->findAll($criteria);
        $aForeCast = $this->getDayForecast($aUId);
        $aDayPerOne = CHtml::listData($aForeCast, 'customer_id', 'days_per_one');
        foreach ($aUsersInfo as $key => $mUsersInfo) {
            $mUsersInfo->upadateInfo($aDayPerOne);
        }
    }
    
    /** @Author: NamNH Jul 23, 2019
     *  @Todo: update info
     **/
    public function upadateInfo($aDayPerOne) {
        $mRewardPoint = new RewardPoint();
        $mUser                  = Users::model()->findByPk($this->user_id);
        $infoSell               = $this->getInfoSellOfCustomer($mUser);
        $first_purchase         = $infoSell[self::FIELD_FIRST_SELL];
        $last_purchase          = $infoSell[self::FIELD_LAST_SELL];
        $count_sell             = $infoSell[self::FIELD_COUNT_SELL];
        if($this->count_sell < $count_sell){
            $phone                  = MyFormat::removeBadCharacters($mUser->phone);
            $sale_id                = $mUser->sale_id;
            $reward_point           = $mRewardPoint->calculatePoint($mUser->id);
            $reward_point_used      = $mRewardPoint->getExchangePoint($mUser->id);
            $days_per_one           = !empty($aDayPerOne[$mUser->id]) ? $aDayPerOne[$mUser->id] : 0;
            $first_name             = MyFormat::removeBadCharacters($mUser->first_name);
            $address                = MyFormat::removeBadCharacters($mUser->address);
            $agent_id               = $mUser->area_code_id;
            $this->phone            = $phone;
            $this->sale_id          = $sale_id;
            $this->reward_point     = $reward_point;
            $this->reward_point_used = $reward_point_used;
            $this->days_per_one     = $days_per_one;
            $this->first_name       = $first_name;
            $this->address          = $address;
            $this->agent_id         = $agent_id;
            $this->first_purchase   = $first_purchase; 
            $this->last_purchase    = $last_purchase;
            $this->count_sell       = $count_sell;
            $this->created_by       = $mUser->created_by;
            $this->update();
        }
    }
    
}
