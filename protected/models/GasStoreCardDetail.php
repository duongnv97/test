<?php

/**
 * This is the model class for table "{{_gas_store_card_detail}}".
 *
 * The followings are the available columns in table '{{_gas_store_card_detail}}':
 * @property string $id
 * @property string $store_card_id
 * @property integer $customer_id
 * @property integer $type_store_card
 * @property integer $type_in_out
 * @property string $date_delivery
 * @property integer $materials_id
 * @property integer $qty
 */
class GasStoreCardDetail extends BaseSpj
{
    public $statistic_year; // Dec 28, 2014 for thống kê
    public $statistic_month;
    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return GasStoreCardDetail the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{_gas_store_card_detail}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        return array(
            array('sale_type,id, store_card_id, customer_id, type_store_card, type_in_out, date_delivery, materials_id, qty, sale_id', 'safe'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        return array(
            'customer' => array(self::BELONGS_TO, 'Users', 'customer_id'),
            'materials' => array(self::BELONGS_TO, 'GasMaterials', 'materials_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'store_card_id' => 'Store Card',
            'customer_id' => 'Customer',
            'type_store_card' => 'Type Store Card',
            'type_in_out' => 'Type In Out',
            'date_delivery' => 'Date Delivery',
            'materials_id' => 'Materials',
            'qty' => 'Qty',
        );
    }

    /**
    * @Author: ANH DUNG 12-17-2013
    * @Todo: get delete all store card detail by store_card_id
    * @Param: $store_card_id
     */        
    public static function deleteByStoreCardId($store_card_id){
        if(empty($store_card_id)){
            return ;
        }
        $criteria = new CDbCriteria();
        $criteria->compare('store_card_id', $store_card_id);
        self::model()->deleteAll($criteria);
        return ;// Sep 16, 2016 không cần thiết phải xóa từng detail nữa,
        // do không sử dụng cập nhật vào tồn kho liên tục nữa mà sẽ chỉ chạy tồn kho 1 lần vào cuối năm 
        
//        $models = self::model()->findAll($criteria);
//        if(count($models)){
//            foreach($models as $item){
//                $item->delete();
//            }
//        }
    }
    
    protected function beforeDelete() {
        $tmpDate = explode('-', $this->date_delivery);
        $year = $tmpDate[0];        
        $agent_id = $this->user_id_create;
        $customer_id = $this->customer_id;
        $materials_type_id = $this->materials_type_id;
        if($this->type_store_card==TYPE_STORE_CARD_IMPORT){
            // update tồn kho đại lý
            GasMaterialsOpeningBalance::cutAgentImport($year, $agent_id, $this->materials_id, $this->qty);
            // update công nợ vỏ KH
            if(isset(CmsFormatter::$MATERIAL_TYPE_PAIR[$materials_type_id])){
                $export_materials_type_id = CmsFormatter::$MATERIAL_TYPE_PAIR[$materials_type_id];
                $import_materials_type_id = $materials_type_id;
                GasClosingBalanceCustomerMaterials::cutMaterialImport($year, $customer_id, $export_materials_type_id, $import_materials_type_id, $this->qty);
            }
            // update công nợ vỏ KH
        }else{
            GasMaterialsOpeningBalance::cutAgentExport($year, $agent_id, $this->materials_id, $this->qty);
            // update công nợ vỏ KH
            if(isset(CmsFormatter::$MATERIAL_TYPE_PAIR[$materials_type_id])){
                $export_materials_type_id = $materials_type_id;
                $import_materials_type_id = CmsFormatter::$MATERIAL_TYPE_PAIR[$materials_type_id];
                GasClosingBalanceCustomerMaterials::cutMaterialExport($year, $customer_id, $export_materials_type_id, $import_materials_type_id, $this->qty);            
            }
            // update công nợ vỏ KH
        }
        return parent::beforeDelete();
    }

    /**
    * @Author: ANH DUNG 12-17-2013
    * @Todo: get all model store card detail by store_card_id
    * @Param: $store_card_id
     */        
    public static function getByStoreCardId($store_card_id){
        if(empty($store_card_id)){
            return [];
        }
        $criteria = new CDbCriteria();
        $criteria->addCondition('t.store_card_id=' . $store_card_id);
        return self::model()->findAll($criteria);
    }
    
    /**
     * @Author: ANH DUNG May 20, 2014
     * @Todo: cập nhật dòng thu tiền cho dòng đầu tiên của chi tiết thẻ kho, vì sẽ dùng trong phần thống kê
     * @Param: $mStoreCard model store card
     */
    public static function updateFirstRowCollectionCustomer($mStoreCard){
        $criteria = new CDbCriteria();
        $criteria->addCondition('t.store_card_id=' . $mStoreCard->id);
        $criteria->order = "t.id ASC";
        $models = self::model()->findAll($criteria);
        if(isset($models[0])){
            $models[0]->collection_customer = $mStoreCard->collection_customer;
            $models[0]->receivables_customer = $mStoreCard->receivables_customer;
            $models[0]->update(array('collection_customer', 'receivables_customer'));
        }
        
        if($mStoreCard->collection_customer>0){
            GasStoreCardDetail::model()->updateAll(array(
                'collection_customer_done'=>1,
                'collection_customer_date'=>$mStoreCard->collection_customer_date,
                ),
                "`store_card_id`=$mStoreCard->id");        
        }else{
            GasStoreCardDetail::model()->updateAll(array('collection_customer_done'=>0),
            "`store_card_id`=$mStoreCard->id");
        }
    }
    
    
}