<?php

/**
 * This is the model class for table "{{_uphold_hgd}}".
 *
 * The followings are the available columns in table '{{_uphold_hgd}}':
 * @property string $id
 * @property string $code_no
 * @property string $agent_id
 * @property string $customer_id
 * @property integer $status
 * @property string $employee_id
 * @property string $uid_login
 * @property string $note_create
 * @property string $note_employee
 * @property string $created_date_only
 * @property string $created_date
 */
class UpholdHgd extends BaseSpj
{
    public $MAX_ID, $autocomplete_name, $autocomplete_name_1, $autocomplete_name_2, $autocomplete_name_3;
    public $date_from, $date_to ,$type_new;
    const STATUS_NEW        = 1;
    const STATUS_CONFIRM    = 2;
    const STATUS_UNCONFIRM  = 3;
    const STATUS_CANCEL     = 4;
    const STATUS_COMPLETE   = 5;
    
    const STATUS_SCAN_NEW = 0;
    const STATUS_SCAN_SCAN = 1;
    const STATUS_SCAN_CALLBACK = 2;
    
     // NOV 30, 2018 báo cáo theo loại báo cáo
    const REPORT_AGENT = 1;
    const REPORT_EMPLOYEE = 2;

    /**
     * @Author: ANH DUNG Apr 19, 2017
     * @Todo: 
     * @Param: 
     */
    public function getArrayStatus() {
        return [
            UpholdHgd::STATUS_NEW       => 'Mới',
            UpholdHgd::STATUS_CONFIRM   => 'Xác nhận',
            UpholdHgd::STATUS_UNCONFIRM => 'Hủy xác nhận',
            UpholdHgd::STATUS_CANCEL    => 'Hủy',
            UpholdHgd::STATUS_COMPLETE  => 'Hoàn thành',
        ];
    }
    public function getArrayStatusNew() {
        return [
            UpholdHgd::STATUS_NEW,
            UpholdHgd::STATUS_CONFIRM,
            UpholdHgd::STATUS_UNCONFIRM,
        ];
    }
    
    public function getArrayTypeNew() {// Nov 30, 2018
        return array(
            UpholdHgd::REPORT_EMPLOYEE   => 'Nhân viên',
            UpholdHgd::REPORT_AGENT => 'Đại lý ',
        );
    }
    public function getStatus() {
        $aStatus = $this->getArrayStatus();
        return isset($aStatus[$this->status]) ? $aStatus[$this->status] : '';
    }
    
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{_uphold_hgd}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        return array(
            array('agent_id, customer_id, phone', 'required', 'on'=>'create, update'),
            array('phone, date_from, date_to, id, code_no, agent_id, customer_id, status, employee_id, uid_login, note_create, note_employee, created_date_only, created_date', 'safe'),
            array('type_new, comment_cancel','safe'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        return array(
            'rUidLogin' => array(self::BELONGS_TO, 'Users', 'uid_login'),// người tạo
            'rAgent' => array(self::BELONGS_TO, 'Users', 'agent_id'),
            'rCustomer' => array(self::BELONGS_TO, 'Users', 'customer_id'),
            'rEmployee' => array(self::BELONGS_TO, 'Users', 'employee_id'),
        );
    }

    public function getNote($format = true) {
        $res = '';
        if(!empty($this->note_create)){
            $res .= '<b>'.$this->getAttributeLabel('note_create').'</b>: '.$this->getNoteCreate();
        }
        if(!empty($this->note_employee)){
            $res .= '<br><b>'.$this->getAttributeLabel('note_employee').'</b>: '.$this->getNoteEmployee();
        }
        if(!empty($this->comment_cancel)){
            $res .= '<br><b>TĐ hủy</b>: '.$this->getCommentCancel();
        }
        if(!$format){
            $res = strip_tags($res);
        }
        return $res;
    }
    public function getCodeNo() {
        return $this->code_no;
    }
    public function getNoteCreate() {
        return nl2br($this->note_create);
    }
    public function getNoteEmployee() {
        return nl2br($this->note_employee);
    }
    public function getUidLogin() {
        $mUser = $this->rUidLogin;
        if($mUser){
            return $mUser->first_name;
        }
        return '';
    }
    public function getAgent() {
        $mUser = $this->rAgent;
        if($mUser){
            return $mUser->first_name;
        }
        return '';
    }
    public function getCustomer($field_name = 'first_name') {
        $mUser = $this->rCustomer;
        if($mUser){
            return $mUser->$field_name;
        }
        return '';
    }
    public function getEmployee() {
        $mUser = $this->rEmployee;
        if($mUser){
            return $mUser->first_name;
        }
        return '';
    }
    public function getDateOnly() {
        return MyFormat::dateConverYmdToDmy($this->created_date_only);
    }
    public function getPhone() {
        if(empty($this->phone)){
            return $this->getCustomer('phone');
        }
        return '0'.$this->phone;
    }
    
    public function canUpdateApp() {
        if(is_null($this->mAppUserLogin)){
            return '0';
        }
        $aNotAllow = array(UpholdHgd::STATUS_CANCEL, UpholdHgd::STATUS_COMPLETE);
        if(in_array($this->status, $aNotAllow) 
                || ($this->employee_id != $this->mAppUserLogin->id) ){
            return '0';
        }
        return '1';
    }
    
    public function getRoleUpdateOrder() {
        return [ROLE_EMPLOYEE_MAINTAIN];
    }
    
    public function getStatusAppNumber() {
        $res = UpholdHgd::STATUS_NEW;
        if($this->status == UpholdHgd::STATUS_CANCEL){
            $res = UpholdHgd::STATUS_CANCEL;
        }elseif($this->status == UpholdHgd::STATUS_COMPLETE){
            $res = UpholdHgd::STATUS_COMPLETE;
        }
        return $res.'';
    }
    
    public function canUpdateWeb() {
        $cRole  = MyFormat::getCurrentRoleId();
        $cUid   = MyFormat::getCurrentUid();
        if( !empty($this->employee_id) || ($cRole != ROLE_ADMIN && $this->uid_login != $cUid) ){
            return false;
        }

        $dayAllow = date('Y-m-d');// chỉ cho phép nhân viên CallCenter cập nhật trong ngày
        $dayAllow = MyFormat::modifyDays($dayAllow, 1, '-');// cho phép update trong ngày
        return MyFormat::compareTwoDate($this->created_date_only, $dayAllow);
    }
    
    /**
     * @Author: ANH DUNG Apr 14, 2017
     * @Todo: check employee can pick / Cancel Order. check có thể Hủy nhận đơn hàng
     */
    public function canPick() {
        $mTransactionHistory = new TransactionHistory();
        if($this->status == UpholdHgd::STATUS_NEW &&
        (!is_null($this->mAppUserLogin) && in_array($this->mAppUserLogin->role_id, $mTransactionHistory->getRoleCanHandleHgd()) )
        ){
            return '1';
        }
        return '0';
    }
    public function canPickCancel() {
        if(!is_null($this->mAppUserLogin) && $this->status == UpholdHgd::STATUS_CONFIRM &&
            ( $this->employee_id == $this->mAppUserLogin->id) ) {
            return '1';
        }
        return '0';
    }
    
    protected function beforeSave() {
        if($this->isNewRecord){
            $this->code_no = MyFunctionCustom::getNextId('UpholdHgd', 'U'.date('y'), LENGTH_TICKET,'code_no');
        }
        $this->note_create      = InputHelper::removeScriptTag($this->note_create);
        $this->note_employee    = InputHelper::removeScriptTag($this->note_employee);
        return parent::beforeSave();
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'code_no' => 'Mã số',
            'agent_id' => 'Đại lý',
            'customer_id' => 'Khách hàng',
            'status' => 'Trạng thái',
            'employee_id' => 'Nhân viên',
            'uid_login' => 'Người tạo',
            'note_create' => 'Ghi chú KH',
            'note_employee' => 'Ghi chú nhân viên',
            'created_date_only' => 'Created Date Only',
            'created_date' => 'Ngày tạo',
            'date_from' => 'Từ ngày',
            'date_to' => 'Đến',
            'phone' => 'Điện thoại liên hệ',
            'type_new' => 'Loại báo cáo',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        $criteria=new CDbCriteria;
        $criteria->compare('t.code_no',$this->code_no,true);
        $criteria->compare('t.agent_id',$this->agent_id);
        $criteria->compare('t.customer_id',$this->customer_id);
        $criteria->compare('t.status',$this->status);
        $criteria->compare('t.employee_id',$this->employee_id);
        $criteria->compare('t.uid_login',$this->uid_login);
        
        if(!empty($this->date_from)){
            $date_from = MyFormat::dateDmyToYmdForAllIndexSearch($this->date_from);
            $criteria->addCondition("t.created_date_only >= '$date_from'");
        }
        if(!empty($this->date_to)){
            $date_to = MyFormat::dateDmyToYmdForAllIndexSearch($this->date_to);
            $criteria->addCondition("t.created_date_only <= '$date_to'");
        }
        GasAgentCustomer::addInConditionAgent($criteria, 't.agent_id');
        $criteria->order = 't.id DESC';
        
        $_SESSION['dataUpholdHgdIndex'] = new CActiveDataProvider($this, array(
            'pagination'=>false,
            'criteria'=>$criteria,
        ));
        
        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
            'pagination'=>array(
                'pageSize'=> Yii::app()->user->getState('pageSize',Yii::app()->params['defaultPageSize']),
            ),
        ));
    }
    
    /** @Author: ANH DUNG  Apr 17, 2017 -- handle listing api */
    public function handleApiList(&$result, $q) {
        // 1. get list order by user id
        $dataProvider   = $this->ApiListing($q);
        $models         = $dataProvider->data;
        $CPagination    = $dataProvider->pagination;
        $result['total_record'] = $CPagination->itemCount;
        $result['total_page']   = $CPagination->pageCount;
        if($q->page == 0){
            $today      = date('Y-m-d');
            $beginMonth = date('Y-m').'-01';
            $sumDay     = $this->countByDate($today, $today);
            $sumMonth   = $this->countByDate($beginMonth, $today);
            $summary_text   = "Tổng ngày $sumDay, tổng tháng $sumMonth";
        }else{
            $summary_text   = '';
        }

        $result['message'] = $summary_text;
        if( $q->page >= $CPagination->pageCount ){
            $result['record'] = [];
        }else{
            foreach($models as $model){
                $model->mAppUserLogin       = $this->mAppUserLogin;// for canPickCancel
                
                $aRes['id']                 = $model->id;
                $aRes['code_no']            = $model->code_no;
                $aRes['customer_id']        = $model->customer_id;
                $aRes['customer_name']      = $model->getCustomer();
                $aRes['customer_address']   = $model->getCustomer('address');
                $aRes['customer_phone']     = $model->getPhone();
                $aRes['note_create']        = $model->note_create;
//                $aRes['allow_update']       = $model->canUpdateApp();
                $aRes['show_confirm']       = $model->canPick();
                $aRes['show_cancel']        = $model->canPickCancel();
                $aRes['created_date']       = $model->getCreatedDate();
                $aRes['status_number']      = $model->getStatusAppNumber();
                
                $result['record'][]         = $aRes;
            }
        }
    }
    
    
    /**
    * @Author: ANH DUNG Apr 14, 2017
    * @Todo: get data listing 
    * @param: $q object post params from app client
    */
    public function ApiListing($q) {
        $mTransactionHistory = new TransactionHistory();
        $criteria = new CDbCriteria();
        if(in_array($this->mAppUserLogin->role_id, $mTransactionHistory->getRoleCanHandleHgd())){
            $aAgent = Users::GetKeyInfo($this->mAppUserLogin, UsersExtend::JSON_ARRAY_AGENT);
            $sParamsIn = implode(',', $aAgent);
            if($sParamsIn != ''){
                $criteria->addCondition(' (t.agent_id IN ('.$sParamsIn.') AND t.status='.GasAppOrder::STATUS_NEW.') OR t.employee_id='.$this->mAppUserLogin->id);
            }else{
                $criteria->addCondition('t.agent_id=1');
            }
        }elseif(in_array($this->mAppUserLogin->role_id, $mTransactionHistory->getRoleMonitor())){
            $mTransactionHistory->mAppUserLogin = $this->mAppUserLogin;
            $mTransactionHistory->handleLimitByRole($q, $criteria);
        }else{
            $criteria->addCondition('t.uid_login=1');// không cho user nào xem nữa
        }

        $sParamsIn = implode(',', $this->getArrayStatusNew());
        // Mar 28, 2017 Tạm Close lại 
        if($q->type == GasConst::APP_LIST_NEW){
            $criteria->addCondition('t.status IN ('.$sParamsIn.')');
        }elseif($q->type == GasConst::APP_LIST_COMPLETE){
            $criteria->addCondition('t.status NOT IN ('.$sParamsIn.')');
        }
        
        $criteria->order = 't.id DESC';
        $dataProvider=new CActiveDataProvider($this, array(
            'criteria' => $criteria,
            'pagination'=>array(
                'pageSize' => GasAppOrder::API_LISTING_ITEM_PER_PAGE,
//                'pageSize' => 2,
                'currentPage' => (int)$q->page,
            ),
          ));
        return $dataProvider;
    }
 
    /**
     * @Author: ANH DUNG Apr 14, 2017
     * format record bảo trì HGD view from app
     */
    public function appView() {
        $aRes                       = [];// order_view, order_edit
        $aRes['id']                 = $this->id;
        $aRes['code_no']            = $this->code_no;
        $aRes['customer_name']      = $this->getCustomer();
        $aRes['customer_address']   = $this->getCustomer('address');
        $aRes['customer_phone']     = $this->getPhone();
        $aRes['note_create']        = $this->note_create;
        $aRes['note_employee']      = $this->note_employee;
        $aRes['created_date']       = $this->getCreatedDate();
        $aRes['allow_update']       = $this->canUpdateApp();
        $aRes['status_number']      = $this->getStatusAppNumber();
        
        return $aRes;
    }
    
    /**
     * @Author: ANH DUNG Apr 14, 2017
     * @Todo: handle set event of App Order
     * for debug: $mUserLogin
     */
    public function handleSetEvent($q) {
        switch ($q->action_type) {
            case UpholdHgd::STATUS_CONFIRM:
                $this->employeePick($q);
                break;
            case UpholdHgd::STATUS_UNCONFIRM:
                $this->employeeCancelPick($q);
                break;
            case UpholdHgd::STATUS_CANCEL:
                $this->employeeCancel($q);
                break;
            case UpholdHgd::STATUS_COMPLETE:
                $this->employeeComplete($q);
                break;
            default:
                $this->addError('id', 'Yêu cầu không hợp lệ');
                return ;
        }
    }
    
    /**
     * @Author: ANH DUNG Apr 15, 2017
     * @Todo: NVGN nhận đi bảo trì
     */
    public function employeePick($q) {
        if(!$this->canPick()){
            $nameEmployee = $this->getEmployee();
            $this->addError('id', $nameEmployee.' đã nhận bảo trì, bạn không thể nhận bảo trì này');
            return;
        }
        
        $this->employee_id = $this->mAppUserLogin->id;
        $this->status      = UpholdHgd::STATUS_CONFIRM;
        $aUpdate = array('employee_id', 'status');
        $this->update($aUpdate);
        $this->titleNotify = $this->getEmployee().' nhận bảo trì '.$this->getCustomer();
//        $this->notifyEmployee();
    }
    
    /**
     * @Author: ANH DUNG Apr 15, 2017
     * @Todo: NVGN hủy nhận đi bảo trì
     */
    public function employeeCancelPick($q) {
        if(!$this->canPickCancel()){
            $this->addError('id', 'Bạn không thể hủy nhận bảo trì này');
            return;
        }
        $this->employee_id = 0;
        $this->status      = UpholdHgd::STATUS_NEW;
        $aUpdate = array('employee_id', 'status');
        $this->update($aUpdate);
        $this->titleNotify = $this->getEmployee().' HỦY BỎ nhận bảo trì '.$this->getCustomer();
        $this->notifyEmployee();
    }
    
    /**
     * @Author: ANH DUNG Apr 15, 2017
     * @Todo: NVGN hủy bảo trì
     */
    public function employeeCancel($q) {
        if(!$this->canPickCancel()){
            $this->addError('id', 'Bạn không thể hủy bảo trì này');
            return;
        }
        $this->note_employee    = $q->note_employee;
        $this->status           = UpholdHgd::STATUS_CANCEL;
        $aUpdate = array('note_employee', 'status');
        $this->update($aUpdate);
    }
    
    /** @Author: ANH DUNG Apr 15, 2017
     *  @Todo: NVGN hoàn thành bảo trì
     */
    public function employeeComplete($q) {
        if(!$this->canPickCancel()){
            $this->addError('id', 'Bạn không thể hoàn thành bảo trì này');
            return;
        }
        $this->note_employee    = $q->note_employee;
        $this->status           = UpholdHgd::STATUS_COMPLETE;
        $aUpdate = array('note_employee', 'status');
        $this->update($aUpdate);
    }
    
    /** @Author: ANH DUNG Oct0717
     * @Todo: notify cho chuyên viên nhận khoán khi có đơn bảo trì mới
     * 1. Võ Tấn Huy Q.6 và Q.10
     */
    public function alertAgentKhoanCreateNew() {
        $mTransactionHistory = new TransactionHistory();
        $aAgentNotify = $mTransactionHistory->getUidMonitor();
        if (!array_key_exists($this->agent_id, $aAgentNotify)) {
            return ;
        }
        $uid = $aAgentNotify[$this->agent_id];
        if(in_array($uid, GasConst::getUidKhoanOnlyHighPrice())){
            return ;// đại lý anh Hiêu ko gửi notify với đơn thường
        }
        $aUidNotify = [$uid];
        $this->notifyEmployee($aUidNotify);
    }
    
    /* lấy danh sách các giao nhận của đại lý đó để notify */
    public function getListUserIdEmployeeMaintain() {
        return GasOneMany::getArrOfManyIdByType(ONE_AGENT_MAINTAIN, $this->agent_id);
    }
    
    /** Apr 14, 2017
     *  notify cho các giao nhận của đại lý đó khi có bảo trì của CallCenter put xuống
     */
    public function notifyEmployee($aUid = []) {
        if(empty($this->titleNotify)){
            $this->titleNotify = "Bảo trì HGD: {$this->getCustomer()}";
        }
        if(is_array($aUid) && count($aUid)){
        }else{
            $aUid = $this->getListUserIdEmployeeMaintain();
        }
        $this->runInsertNotify($aUid, $this->titleNotify, true); // Send Now
//        $this->runInsertNotify($aUid, $this->titleNotify, false); // Send By Cron
    }
    
    /**
     * @Author: ANH DUNG Apr 14, 2017
     * @Todo: insert to table notify
     * @Param: $aUid array user id need notify
     */
    public function runInsertNotify($aUid, $title, $sendNow) {
        $json_var = [];
        foreach($aUid as $uid) {
            if( !empty($uid) ){
                $mScheduleNotify = GasScheduleNotify::InsertRecord($uid, GasScheduleNotify::UPHOLD_HGD, $this->id, '', $title, $json_var);
                if($sendNow && !is_null($mScheduleNotify)){
                    $mScheduleNotify->sendImmediateForSomeUser();
                }// Jan 05, 2016 tạm close send luôn lại, vì nó gây chậm bên PMBH khi send
                // sẽ gửi = cron
            }
        }
    }
    
    /**
     * @Author: ANH DUNG Apr 19, 2017
     * @Todo: đếm số bảo trì trong ngày và trong tháng của NV
     */
    public function countByDate($date_from, $date_to) {
        $criteria = new CDbCriteria();
        $criteria->addBetweenCondition('t.created_date_only', $date_from, $date_to);
        $criteria->addCondition('t.status=' . UpholdHgd::STATUS_COMPLETE);
        $criteria->addCondition('t.employee_id=' . $this->mAppUserLogin->id);
        return UpholdHgd::model()->count($criteria);
        
    }
    
    /**
     * @Author: ANH DUNG Apr 20, 2017
     * @Todo: map info customer vào record khi có customer dc select sẵn
     */
    public function mapCustomerSelect() {
        if(isset($_GET['customer_id'])){
            $this->customer_id = $_GET['customer_id'];
            $mCustomer = $this->rCustomer;
            if($mCustomer){
                $this->agent_id = $mCustomer->area_code_id;
            }
        }
    }
    
    /**
     * @Author: ANH DUNG NOw 02, 2017
     * @todo: get latest 5 record of this customer
     */
    public function getHistoryCustomer() {
        $criteria = new CDbCriteria();
        $criteria->compare("t.customer_id", $this->customer_id);
        if(!empty($this->id)){
//            $criteria->addNotInCondition("t.id", $this->id);
            $criteria->addCondition("t.id IN ($this->id)");
        }
        $criteria->order = "t.id DESC";
        $criteria->limit = 5;
        return self::model()->findAll($criteria);
    }
    
    /** @Author: KHANH TOAN Oct 26 2018
     *  @Todo: quet tu dong don hang
     *  @Param: 2018-08-10 06:00:20
     **/
    public function ScanAutomaticeCallBack() {
        $cDate = date("2018-08-10"); 
        $this->FindOrderCallBack($cDate);
    }

    /** @Author: KHANH TOAN Oct 26 2018
     *  @Todo: tìm những đơn hàng có cuộc gọi về sau khi bảo trì thành công
     *          trong vòng 1 ngày
     * 1/   find những đơn hàng hoàn thành của ngày
     * 2/   lấy phone + complete_time check bên gas_call, 
     *      kiểm tra xem có phone đó gọi vào sau khi hoan thanh bao tri hay khong
     * 3/   nếu có thì status_scan = 2
     *  @Param:
     **/
    public function FindOrderCallBack($cDate) {
        $big_int_date = strtotime($cDate);
        $criteria = new CDbCriteria();
        $criteria->addCondition("t.complete_time >= '$big_int_date' "); //thời gian hoàn thành đơn hàng trong ngày
        $criteria->compare('t.status',  self::STATUS_COMPLETE);
        $criteria->addCondition('t.phone  <> 0 ');
        $models = self::model()->findAll($criteria);
        $aResult = array();
        foreach ($models as $key => $mUpholdHGD) {
            if($this->checkPhoneInbound($mUpholdHGD->phone, $cDate, $mUpholdHGD->complete_time)){
                $aResult[] = $mUpholdHGD->id;
            }
        }
        $this->updateStatusScan($aResult);
    }
    
    /** @Author: KHANH TOAN Oct 26 2018
     *  @Todo: check co cuoc goi vao sau khi hoan thanh bao tri hay khong
     *  @Param: $phone, $date, $complete_time
     **/
    public function checkPhoneInbound($phone, $date, $complete_time) {
        $callBack = date("Y-m-d h:i:s",$complete_time);
        $criteria = new CDbCriteria;
        $criteria->compare('t.caller_number', $phone);
        $criteria->addCondition("t.created_date >= '$callBack'");
        $criteria->compare('t.direction',  Call::DIRECTION_INBOUND);
        $criteria->compare('DATE(t.created_date_only)', $date);
        $mCall = Call::model()->find($criteria);
        if($mCall){
            return true;
        }
        return false;
    }
    
    /** @Author: KHANH TOAN Oct 26 2018
     *  @Todo: sql update 1 lan status_scan
     *  @Param:
     **/
    public function updateStatusScan($aResult) {
        $tableName = self::model()->tableName();
        $sql = "UPDATE $tableName 
                SET status_scan = ". self::STATUS_SCAN_CALLBACK
                ." WHERE id in (".implode(',', $aResult).")";
        if(count($aResult)>0)
            Yii::app()->db->createCommand($sql)->execute();
    }
    /** @Author: Pham Thanh Nghia Dec 2, 2018
     *  @Todo: Báo cáo đơn bảo trì bò mối và hộ GĐ admin/upholdHgd/ReportSum
     **/
    public function canExportExcel(){
        return true;
    }
    /** @Author: Pham Thanh Nghia Dec 10, 2018
     *  @Todo:  GĐ admin/upholdHgd/index
     **/
    public function canExportExcelIndex(){
        $cUid = MyFormat::getCurrentUid();
        $aUidAlllow = [GasConst::UID_ADMIN, GasConst::UID_PHUONG_NT];
        return in_array($cUid, $aUidAlllow);
    }
    
    /** @Author: KHANH TOAN 2018
     **/
    public function getCommentCancel() {
        return nl2br($this->comment_cancel);
    }
    
    /** @Author: KHANH TOAN 2018
     *  @Todo:
     **/
    public function canCommentCancel() {
        $ok = false;
        $cUid = MyFormat::getCurrentUid();
        $aUidAllow = [$this->uid_login, GasConst::UID_PHUONG_NT, GasConst::UID_ADMIN];
        if($this->status == UpholdHgd::STATUS_COMPLETE && in_array($cUid, $aUidAllow)){
            $ok = true;
        }
        return $ok;
    }


}