<?php

/**
 * This is the model class for table "{{_referral_tracking}}".
 *
 * The followings are the available columns in table '{{_referral_tracking}}':
 * @property integer $id
 * @property integer $invited_id id of invited user
 * @property integer $ref_id id of parent user
 * @property integer $status
 * @property string $created_date
 * @property Users $invited_user
 * @property Users $ref_user
 */
class ReferralTracking extends BaseSpj
{
    const REF_STATUS_NEW                    = 0;
    const REF_STATUS_CONFIRM                = 1;
    const REF_STATUS_CONFIRM_NO_PROMOTION   = 2;

    public $REF_STATUS_NAME = [
        self::REF_STATUS_NEW                    => 'Chờ đơn hàng đầu tiên',
        self::REF_STATUS_CONFIRM                => 'Đã hưởng khuyến mãi',
        self::REF_STATUS_CONFIRM_NO_PROMOTION   => 'Đã giới thiệu',
    ];
    
    public $mAppPromotion = null;
    

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{_referral_tracking}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        return array(
            array('invited_id, ref_id', 'required', 'on' => 'create, update'),
            array('invited_id, ref_id, created_date', 'safe'),
            array('invited_id', 'unique', 'message' => 'Mã giới thiệu đã được sử dụng, không thể nhập mã khác thay thế'),

        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        return array(
            'invited_user' => array(self::BELONGS_TO, 'Users', 'invited_id'),
            'ref_user' => array(self::BELONGS_TO, 'Users', 'ref_id'),
            'rInvited' => array(self::BELONGS_TO,'Users','invited_id'),
            'rRef' => array(self::BELONGS_TO,'Users','ref_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'invited_id' => 'Người được giới thiệu',
            'ref_id' => 'Người giới thiệu',
            'status' => 'Trạng thái',
            'created_date' => 'Ngày tạo',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        $criteria = new CDbCriteria;
        $criteria->compare('t.id', $this->id);
        $criteria->order = 't.id DESC';
        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
            'pagination' => array(
                'pageSize' => Yii::app()->user->getState('pageSize', Yii::app()->params['defaultPageSize']),
            ),
        ));
    }
    
    public function getInvited($field = 'first_name') {
        $mUser = $this->rInvited;
        return !empty($mUser) ? $mUser->$field : '';
    }
    public function getRef($field = 'first_name') {
        $mUser = $this->rRef;
        return !empty($mUser) ? $mUser->$field : '';
    }
    

    /** Get list child referrals users
     * @return ReferralTracking[]
     */
    public function getChildReferrals()
    {
        return ReferralTracking::model()->findAllByAttributes(
            array('ref_id' => $this->ref_id)
        );
    }
    public function getCountchildText()
    {
        $count = 0;
        if(!empty($this->ref_id)){
            $criteria=new CDbCriteria;
            $criteria->compare('t.ref_id', $this->ref_id);
            $count = ReferralTracking::model()->count($criteria);
        }
        return 'Bạn đã giới thiệu '.$count.' người sử dụng';
    }

    /** Get status of referral for response to client
     * @return string text
     */
    public function getRefStatus()
    {
        if ($this->REF_STATUS_NAME[$this->status]) {
            return $this->REF_STATUS_NAME[$this->status];
        } else {
            return 'Chưa xác định';
        }
    }

    /** Check referral is invited or not
     * @param $user_id integer id of user
     * @return bool True if invited
     */
    public function isInvitedUser()
    {
        $count = ReferralTracking::model()->countByAttributes(
            array('invited_id' => $this->invited_id)
        );
        return $count > 0;
    }
    
    /** @Author: ANH DUNG Now 22, 2017
     *  @Todo: check user can input invite code
     * @return: 1: allow input code, 0: not allow invite code
     **/
    public function canInputInviteCode(){
        $isInvitedUser = $this->isInvitedUser();
        $totalOrder = $this->countOrderDone($this->invited_id);
        if(!$isInvitedUser && $totalOrder == 0){
//        if(!$isInvitedUser){
            return 1;
        }
        return 0;
    }
    
    /** @Author: ANH DUNG Jan 02, 2019
     *  @Todo: count order done of customer
     *  @return: number order: 0,1,2...
     **/
    public function countOrderDone($customer_id) {
        $mSell = new Sell();
        $mSell->customer_id = $customer_id;
        return $mSell->countOrderDoneByCustomer();
    }

    /** Get invited info
     * @param $user_id integer id of user
     * @return ReferralTracking
     */
    public function getInvitedInfo()
    {
        return ReferralTracking::model()->findByAttributes(
            array('invited_id' => $this->invited_id)
        );
    }

    /**
     * Action for first order. Get 50k and ref get 20k
     * @return boolean order is activated or not. True if success.
     * 1. Check referral tracking is exist data
     * 2. check person who invited is new user in referral tracking
     * 3. Invite user is existed code
     *
     * Result:
     * 1. Add 20k to ref user
     * 2. Add 50 to person who invited
     * 3. Change tracking status to confirm
     * 4. set flat User đc giới thiệu đã done đơn hàng đầu tiên
     */
    public function activateFirstOrder()
    {
        /** @var ReferralTracking $referralInfo */
        $referralInfo = $this->getInvitedInfo();

        if (empty($referralInfo) || $referralInfo->status != ReferralTracking::REF_STATUS_NEW) {
            return ;
        }
        /** @var AppPromotion $mPromotion */
        $mPromotion = new AppPromotion();
        $mPromotion->owner_id = $referralInfo->ref_id;
        $mPromotion = $mPromotion->getMyReferralInfo();
        if (!empty($mPromotion)) {
            // For ref user
            $mAppPromotionUser = new AppPromotionUser();
            $mAppPromotionUser->user_id = $referralInfo->ref_id;
            $mAppPromotionUser->updateRefAndSaveData($mPromotion);
            // For tracking, update flag status when user complete first order
            $referralInfo->status       = ReferralTracking::REF_STATUS_CONFIRM;
            $referralInfo->date_order   = date('Y-m-d');
            $referralInfo->update();
            if($mPromotion->partner_type != AppPromotion::PARTNER_TYPE_GRAB){
                $referralInfo->notifyWhenUserInvitedCompleteFirstOrder();
            }
        }
    }
    
    /** @Author: ANH DUNG Now 22, 2017
     *  @Todo: count done order of Ref user
     **/
    public function countRefCompleteFirstOrder() {
        return ReferralTracking::model()->countByAttributes(
            ['ref_id' => $this->ref_id, 'status' => ReferralTracking::REF_STATUS_CONFIRM] );
    }
    
    /** @Author: ANH DUNG Now 26, 2017
     *  @Todo: get total point of user
     * @param: $this->invited_id is current user login
     * 1. count total order
     * 2. count total ref complete first order
     **/
    public function getCurrentPoint($format = true) {
        $this->ref_id       = $this->invited_id;
        $mSell              = new Sell();
        $mSell->customer_id = $this->invited_id;
        $totalOrder         = $mSell->countOrderDoneByCustomer();
        if(!empty($this->mAppPromotion) && $this->mAppPromotion->partner_type == AppPromotion::PARTNER_TYPE_GRAB){
//            Logger::WriteLog("Debug123 totalOrder = $totalOrder --- {$this->mAppPromotion->code_no} AND {$this->mAppPromotion->partner_type}");
//            $totalOrderRef      = $this->countOrderOfPttt();// Mar0619 Kiên nói close lại App GĐKV - có thể xử lý mở lại với app của Ngân hàng MB
            $totalOrderRef      = 0;
        }else{
//            $totalOrderRef      = $this->countRefCompleteFirstOrder();
            $totalOrderRef      = 0; // May1119 Kien change (Mtây all ) bỏ không cộng điểm cho đơn hàng đầu tiên của người nhập mã giới thiệu
        }
        
        $totalPoint = ($totalOrder + $totalOrderRef) * AppPromotion::P_POINT;
        if($format){
            return ActiveRecord::formatCurrency($totalPoint);
        }
        return $totalPoint;
    }
    
    /** @Author: DungNT Jan 11, 2019
     *  @Todo: count order of PTTT on app Gas24h
     **/
    public function countOrderOfPttt() {
        $mSellReport = new SellReport();
        $mSellReport->promotion_id = $this->mAppPromotion->id;
        return $mSellReport->countOrderByPromotionCode();
    }
    
    
    protected function beforeSave() {
        $mUserRef = $this->rRef;
        if($mUserRef){
            $this->ref_role_id = $mUserRef->role_id;
        }
        $this->setMoreBeforeSave();
        return parent::beforeSave();
    }
    
    /** Oct1818 set some info to record */
    public function setMoreBeforeSave(){
        $idUsers            = $this->invited_id;
        $mUsers             = Users::model()->findByPk($idUsers);
        $this->agent_id     = !empty($mUsers) ? $mUsers->area_code_id : '';
    }
    
    /** @Author: ANH DUNG Dec 23, 2018
     *  @Todo: send notify to user create when status = STATUS_UPDATE
     **/
    public function notifyWhenUserInvitedCompleteFirstOrder() {
        $money = ActiveRecord::formatCurrency(AppPromotion::P_AMOUNT_OF_OWNER);
        $this->titleNotify = "Tặng bạn $money đ mua Gas qua app. {$this->getInvited()} đã hoàn thành đơn hàng đầu tiên";
        $aUid = [$this->ref_id];
        $this->runInsertNotify($aUid, false);
    }
    
    /**
     * @Author: ANH DUNG Dec 23, 2018
     * @Todo: insert to table notify
     * @Param: $aUid array user id need notify
     */
    public function runInsertNotify($aUid, $sendNow) {
        $json_var = [];
        foreach($aUid as $uid) {
            if( !empty($uid) ){
                $mScheduleNotify = GasScheduleNotify::InsertRecord($uid, GasScheduleNotify::CUSTOMER_HGD_ORDER, $this->id, '', $this->titleNotify, $json_var);
                if($sendNow && !is_null($mScheduleNotify)){
                    $mScheduleNotify->sendImmediateForSomeUser();
                }// Jan 05, 2016 tạm close send luôn lại, vì nó gây chậm bên PMBH khi send
                // sẽ gửi = cron
            }
        }
    }
    
}