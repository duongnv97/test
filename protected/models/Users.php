<?php
/**
 * This is the model class for table "{{_users}}".
 *
 * @property string $id ID
 * @property string $email Email
 * @property string $password_hash Password
 * @property string $password_confirm Confirm Password
 * @property string $temp_password Temp Password
 * @property string $last_name Cách Xưng Hô
 * @property string $created_date Ngày Tạo
 * @property string $last_logged_in Last Logged In
 * @property string $ip_address Ip Address
 * @property string $status Trạng Thái
 * @property string $gender Giới Tính
 * @property string $phone Phone
 * @property string $area_code_id Nơi Giao
 * @property string $currentpassword Mật Khẩu Hiện Tại
 * @property string $newpassword'=>'Mật Khẩu Mới
 * @property string $first_name Tên KH
 * @property string $name_agent Tên để sắp xếp (sort) tiếng việt ko dấu
 * @property string $code_account Mã Kế Toán
 * @property string $code_bussiness Mã Kinh Doanh
 * @property string $address Địa Chỉ
 * @property string $address_temp Địa Chỉ Mới
 * @property string $province_id Tỉnh
 * @property string $channel_id Trạng Thái
 * @property string $district_id Quận Huyện
 * @property string $storehouse_id Công ty
 * @property string $sale_id Nhân Viên Sale
 * @property string $payment_day Hạn Thanh Toán
 * @property string $percent_target Phần trăm target
 * @property string $beginning Dư đầu kỳ
 * @property string $role_id Loại User
 * @property string $maintain_agent_id Đại Lý Theo Dõi
 * @property string $is_maintain Loại KH
 * @property string $agent_id_search Đại Lý
 * @property string $house_numbers Số Nhà + Thôn/Ấp
 * @property string $ward_id Phường/Xã
 * @property string $street_id Đường
 * @property string $parent_id Thuộc Hệ Thống
 * @property string $customer_id Khách Hàng
 * @property string $assign_employee_sales KH Có NV Sale
 * @property string $last_purchase Ngày Mua Hàng Cuối Cùng
 * @property string $price Giá bán/sản lượng
 * @property string $price_other Giá khác
 * @property string $slug Tọa độ google map: lat,lng
 * @property string $materials_name Vật tư
 * @property string $date_from Từ ngày
 * @property string $date_to Đến ngày
 * @property string $buying Mua hàng
 * @property string $phone_ext2 ĐT nhận báo giá
 */
class Users extends BaseSpj
{
    // @Code: NAM006
    public $countVo,$count_vo,$type_customer,$agent_id,$autocomplete_name_1,$autocomplete_name_2,$sum_end_vo;
    
    public $profile_status, $status_leave = 0, $passwordType = 0, $contract_type, $mProfile = null, $aMaterial, $file_excel, $maintain_agent_id, $agent_id_search, $MAX_ID, $date_from, $date_to;
    public $password_confirm, $md5pass, $currentpassword, $newpassword=''; /* for change pass in admin */
    public $autocomplete_name, $autocomplete_name_street, $autocomplete_name_parent, $monitoring_id, $pageSize, $buying = 0;
    public $customer_id, $assign_employee_sales, $mUsersRef, $password, $date_month='', $date_year=''; // Oct 25, 2015 for app signup
    public $phone_ext4='', $phone_ext3='', $agent_name_real='', $phone_ext2='', $phone_first='', $oldStatus;
    public $tempField1;
    
    //Apr 08, 2014 CỘT gender table user sẽ sử dụng làm cờ nhận biết đại lý vs kho
    const IS_AGENT              = 1;// 1: đại lý, 2 :kho
    const IS_WAREHOUSE          = 2;
    const IS_WAREHOUSE_IN_OUT   = 3;
    const IS_AGENT_PLAN         = 4;// DL QUY HOACH
    const IS_AGENT_NEW          = 5;// DL MO MOI
    
    // DuongNV Oct10,19 trạng thái thực tế của đại lý (cột first_char)
    const REAL_STATUS_ACTIVE            = 1; // Đang hoạt động
    const REAL_STATUS_NOT_ACTIVE_YET    = 2; // Chưa hoạt động
    const REAL_STATUS_INACTIVE          = 3; // Ngưng hoạt động
    
    public function getArrayTypeAgent(){
        return [
            Users::IS_AGENT                 =>'Đại Lý',
            Users::IS_WAREHOUSE             =>'Kho Kế Toán',
            Users::IS_WAREHOUSE_IN_OUT      =>'Kho Nhập Xuất (new)',
            Users::IS_AGENT_PLAN            =>'Quy hoạch',
            Users::IS_AGENT_NEW             =>'Mở mới',
        ];
    } 
    
    public static $aTypeAgentReal = array(
        Users::IS_AGENT,
        Users::IS_WAREHOUSE,
    );
    
    const IS_AGENT_ONLY         = 1;// 1: đại lý + kho kế toán
    const IS_WAREHOUSE_ONLY     = 2;//  2 :kho mới, chỉ nhập xuất
    
    const SALE_BO               = 1;// 1: sale bò, 2: sale Mối
    const SALE_MOI              = 2;
    const SALE_MOI_CHUYEN_VIEN  = 3;
    const SALE_MOI_PTTT         = 4; // Dec2518 nv PTTT
    const SALE_PTTT_KD_KHU_VUC  = 5; // Dec2518 nv CCS
    const SALE_XE_RAO           = 6; // Apr 14, 2015 Sale Xe Rao
    public static $aTypeSale = array(// field gender
        Users::SALE_BO                  => 'Sale Bò',
        Users::SALE_MOI                 => 'Sale Mối',
//        Users::SALE_MOI_CHUYEN_VIEN     =>' Sale Mối Chuyên Viên',
        Users::SALE_MOI_PTTT            => 'PTTT',
        Users::SALE_PTTT_KD_KHU_VUC     => 'CCS',
//        Users::SALE_XE_RAO              => 'Sale Xe Rao',
    );
    
    public static $aTypeSaleSearch = array( // Dec 18, 2015 fix limit khi search sale, chỉ ai dc gắn chức vụ thì mới search 
        Users::SALE_BO,
//        Users::SALE_MOI,// Close on Mar 02, 2016 => may chi de moi sale bo va giam sat pttt thoi
//        Users::SALE_MOI_CHUYEN_VIEN,
//        Users::SALE_MOI_PTTT,
//        Users::SALE_PTTT_KD_KHU_VUC,
//        Users::SALE_XE_RAO,
    );    
    
    // Apr 04, 2015 define array cac role giong sale 
    public static $ARR_ROLE_LIKE_SALE = array(
        ROLE_SALE, ROLE_EMPLOYEE_MARKET_DEVELOPMENT
    );
    
    // Apr 04, 2015 sẽ dùng cột is_maintain làm cờ để biết giám sát nào có thống kê target
    // hiện lên trong báo cáo target của chuyên viên kinh doanh khu vực - PTTT
    public $ARR_ROLE_USE_IS_MAINTAIN = array(
        ROLE_MONITORING_MARKET_DEVELOPMENT
    );
    // end // Apr 04, 2015
    public $ARR_ROLE_SALE_CREATE = array(ROLE_DLLK_MANAGE, ROLE_SALE_ADMIN, ROLE_SALE, ROLE_MONITORING_MARKET_DEVELOPMENT, ROLE_EMPLOYEE_MARKET_DEVELOPMENT,
        ROLE_EMPLOYEE_MAINTAIN, ROLE_CHIEF_MONITOR, ROLE_MONITOR_AGENT, ROLE_DRIVER, ROLE_E_MAINTAIN
    );    
    public $ARR_ROLE_SALE_LIMIT  = array(ROLE_DLLK_MANAGE, ROLE_SALE, ROLE_MONITORING_MARKET_DEVELOPMENT, ROLE_EMPLOYEE_MARKET_DEVELOPMENT, ROLE_EMPLOYEE_MAINTAIN, ROLE_CHIEF_MONITOR, ROLE_MONITOR_AGENT, ROLE_DRIVER);
    public $ARR_ROLE_SALE_UPDATE = array(ROLE_SALE_ADMIN, ROLE_HEAD_GAS_BO);
    
    public static $aIdTypeSaleMoi = array(
        Users::SALE_MOI,
        Users::SALE_MOI_CHUYEN_VIEN,
        Users::SALE_MOI_PTTT,
    );
    
    const WIDTH_freezetable = 1100;
    public $IsFromPtttExcel;// biến kiểm tra KH được tạo từ import pttt file excel thì = 1
    // vì tạo từ file excel nên không build address_vi như cũ được, phải làm khác
    
    public static $aGender = array(1=>'Nữ', 2=>'Nam');
    public static $USER_STATUS = array(STATUS_ACTIVE    => 'Hoạt Động',
                                       STATUS_INACTIVE  =>'Không Hoạt Động',
                                       STATUS_WAIT_ACTIVE => 'Chờ Duyệt',
                                       STATUS_REJECT    => 'Hủy Bỏ',
    );
    
    /** @Author: DungNT Aug 18, 2017
     * @Todo: sử dụng quản lý trạng thái nhân sự
     */
    public function getArrayStatusProfile() {
        return [
            STATUS_ACTIVE       => 'Hoạt động',
            STATUS_LEAVE_BORN   => 'Nghỉ thai sản',
//            STATUS_LEAVE_TEMP   => 'Nghỉ tạm',
            STATUS_INACTIVE     => 'Nghỉ việc',
        ];
    }
    public function getProfileStatus() {
        $aStatus = $this->getArrayStatusProfile();
        return isset($aStatus[$this->status]) ? $aStatus[$this->status] : '';
    }
    
    // May 14, 2015
    const CON_LAY_HANG      = 1;
    const KHONG_LAY_HANG    = 2;
    const CHAN_HANG         = 3;
    const CHAN_HANG_KHAI_TU = 4;// Sep2118 KH Khai tử
    public static $STATUS_LAY_HANG = array(
        Users::CON_LAY_HANG         => 'Còn lấy hàng',
        Users::KHONG_LAY_HANG       => 'Không lấy hàng',
        Users::CHAN_HANG            => 'Chặn hàng',
        Users::CHAN_HANG_KHAI_TU    => 'Khai tử',
    );
    // May 14, 2015
    
    public static $ROLE_VIEW_USERNAME_CUSTOMER = array(
        ROLE_ADMIN,
        ROLE_SALE,
        ROLE_HEAD_GAS_BO,
        ROLE_HEAD_GAS_MOI,
        ROLE_DIRECTOR_BUSSINESS,
        ROLE_SALE_ADMIN,
    );
    const GROUP_CNDB        = 1;// Jun 04, 2017 chia group KH bò mối: cndb
    const GROUP_GIAO_KHO    = 2;// Oct1317 KH giao khó
    const GROUP_M18         = 3;// May 2818 KH mối trong nhóm M18
    const GROUP_KH40        = 4;// Sep2118 KH không quét 40 ngày
    const GROUP_KH_MALL                     = 5;// Sep1119 Trung Tâm Thương Mại (TTTM)
    const GROUP_KH_HOTEL                    = 6;// Khách sạn 
    const GROUP_KH_SUPER_MARKET             = 7;// Siêu Thị
    const GROUP_KH_WEDDING                  = 8;// Tiệc Cưới
    const GROUP_KH_INDUSTRIAL_PRODUCTION    = 9;// Sản xuất công nghiệp
    const GROUP_KH_CATERING_PRODUCTION      = 10;// Suất Ăn Công Nghiệp (SACN)
    const GROUP_KH_RESTAURANT               = 11;// Nhà Hàng 
    const GROUP_KH_RESTAURANT_SMALL         = 12;// Quán Ăn
    const GROUP_KH_COFFEE                   = 13;// Cà Phê
    
    public function getArrayGroup() {
        return [
            Users::GROUP_CNDB       => 'TB95',
            Users::GROUP_GIAO_KHO   => 'Giao Khó',
            Users::GROUP_M18        => 'Trường học',
            Users::GROUP_KH40       => 'KH40',
            Users::GROUP_KH_MALL                        => 'Trung Tâm Thương Mại (TTTM)',
            Users::GROUP_KH_HOTEL                       => 'Khách sạn',
            Users::GROUP_KH_SUPER_MARKET                => 'Siêu Thị',
            Users::GROUP_KH_WEDDING                     => 'Tiệc Cưới',
            Users::GROUP_KH_INDUSTRIAL_PRODUCTION       => 'Sản xuất công nghiệp',
            Users::GROUP_KH_CATERING_PRODUCTION         => 'Suất Ăn Công Nghiệp (SACN)',
            Users::GROUP_KH_RESTAURANT                  => 'Nhà Hàng',
            Users::GROUP_KH_RESTAURANT_SMALL            => 'Quán Ăn',
            Users::GROUP_KH_COFFEE                      => 'Cà Phê',
        ];
    }
    
    /** @Author: DuongNV Oct10,19
     *  @Todo: get array dropdown trạng thái thực tế
     **/
    public function getArrayRealStatusAgent() {
        return [
            Users::REAL_STATUS_ACTIVE         => 'Đang hoạt động',
            Users::REAL_STATUS_NOT_ACTIVE_YET => 'Chưa hoạt động',
            Users::REAL_STATUS_INACTIVE       => 'Ngưng hoạt động'
        ];
    }
    
    /** @Author: DuongNV Oct10,19
     *  @Todo: get array dropdown trạng thái thực tế
     **/
    public function getRealStatusAgent() {
        $aStatus = $this->getArrayRealStatusAgent();
        if($this->role_id == ROLE_AGENT && isset($aStatus[$this->first_char])){
            return $aStatus[$this->first_char];
        }
        return '';
    }
    
    public function getGroup() {
        $aGroup = $this->getArrayGroup();
        return isset($aGroup[$this->first_char]) ? $aGroup[$this->first_char] : '';
    }
    
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }
    public function tableName()
    {
        return '{{_users}}';
    }

    public function rules()
    {
        return array(
    array('first_name, status', 'required','on'=>'create_customer'),
    array('first_name, status, gender', 'required','on'=>'update_customer'),

    //create account admin
    array('first_name,username, email, last_name, gender','required','on'=>'createAdmin, editAdmin'),
    array('password_hash, password_confirm','required','on'=>'createAdmin'),
    array('password_hash', 'length', 'min'=>6, 'max'=>32,'on'=>'createAdmin, editAdmin'),
    array('password_confirm', 'compare', 'compareAttribute'=>'password_hash','on'=>'editAdmin,createAdmin'),

    // create_agent
    array('email','unique','message'=>'This uniquemail address is not available','on'=>'editAdmin,createAdmin'),
    array('code_account','unique','message'=>'Mã khách hàng này đã tồn tại, mã KH phải là duy nhất','on'=>'create_customer,update_customer'),
    array('code_account','unique','message'=>'Mã User này đã tồn tại, mã User phải là duy nhất','on'=>'create_agent,update_agent'),
//	array('code_bussiness','unique','message'=>'Mã member này đã tồn tại, mã member phải là duy nhất','on'=>'create_member,update_member'), 
    array('username', 'match','pattern'=>'/^[a-zA-Z\d_.]{2,30}$/i', 
        'message'=>'Username cannot include special characters: &%$#',),
    array('province_id, district_id, code_account', 'required','on'=>'create_agent,update_agent'),
//    closse on May 28, 2016 array('username', 'unique','message'=>' Username này đã tồn tại' ,'on'=>'create_agent,update_agent,create_member,update_member, update_customer_username'),
    array('username', 'unique','message'=>' Username này đã tồn tại' ,'on'=>'create_member,update_member, update_customer_username, employees_create, employees_update, create_customer_store_card'),
    array('code_account', 'unique','message'=>'Mã User này đã tồn tại' ,'on'=>'update_member'),
//    array('password_hash, password_confirm','required','on'=>'create_agent'),
    array('password_hash', 'length', 'min'=>PASSW_LENGTH_MIN, 'max'=>PASSW_LENGTH_MAX,'on'=>'create_member,update_member'),
    array('password_confirm', 'compare', 'compareAttribute'=>'password_hash','on'=>'create_member,update_member, update_customer_username'),
    array('email','email','on'=>'create_member,update_member, AppGas24hUpdateProfile, create_customer_store_card, update_customer_store_card'),
    array('email','unique','on'=>'create_member,update_member, AppGas24hUpdateProfile'),
    array('first_name','required','on'=>'create_member,update_member'),
 // create_agent                    
    array('email, last_name','required','on'=>'updateMyProfile'),
    array('currentpassword', 'comparePassword', 'on'=>'changeMyPassword'),
    array('currentpassword, newpassword, password_confirm', 'required','on' => 'changeMyPassword'),
    array('newpassword','length', 'min'=>PASSW_LENGTH_MIN, 'max'=>PASSW_LENGTH_MAX,
        'tooLong'=>'Mật khẩu mới quá dài (tối đa '.PASSW_LENGTH_MAX.' ký tự).',
        'tooShort'=>'Mật khẩu mới quá ngắn (tối thiểu '.PASSW_LENGTH_MIN.' ký tự).',
        'on'=>'changeMyPassword'),
    array('password_confirm', 'compare', 'compareAttribute'=>'newpassword','message'=>'Xác nhận mật khẩu mới không đúng.' ,'on'=>'changeMyPassword'),

    array('email, password_hash, temp_password', 'length', 'max'=>250),
    array('email','email','on'=>'editAdmin,createAdmin, employees_create, employees_update'),
    //array('phone','numerical', 'integerOnly'=>true),
    array('name_agent, code_account, code_bussiness, address, province_id, channel_id,district_id,,storehouse_id,sale_id,payment_day,', 'safe'),
    array('percent_target,beginning, parent_id', 'safe'),
    array('newpassword, buying, maintain_agent_id,is_maintain, agent_id_search', 'safe'),
    array('id, username, email, password_hash, temp_password, first_name, last_name, first_char, login_attemp, created_date, last_logged_in, ip_address, role_id, application_id, status, gender, phone, verify_code, area_code_id', 'safe'),
    array('profile_status, status_leave, contract_type, house_numbers, street_id, address_vi, ward_id, type,address_temp, customer_id, assign_employee_sales', 'safe'),
    array('phone_ext4, phone_ext3, agent_name_real, password, phone_ext2, pageSize, created_by, monitoring_id, date_from, date_to, slug, price, price_other, last_purchase', 'safe'),

    array('file_excel', 'file', 'on'=>'import_customer',
        'allowEmpty'=>false,
        'types'=> 'xls,xlsx',
        'wrongType'=>'Chỉ cho phép tải file xls,xlsx',
        //'maxSize' => ActiveRecord::getMaxFileSize(), // 5MB
        //'tooLarge' => 'The file was larger than '.(ActiveRecord::getMaxFileSize()/1024).' KB. Please upload a smaller file.',
    ),  

// create_maintain_user
 array('first_name, province_id,district_id,ward_id,phone,street_id,house_numbers', 'required','on'=>'create_maintain_user,update_customer_maintain'),
// create_maintain_user	
array('first_name,gender', 'required','on'=>'dieuphoi_create_sale'),

    // create_customer_store_card 
    // array('first_name, province_id,district_id,ward_id,phone,street_id,house_numbers', 'required','on'=>'create_customer_store_card,update_customer_store_card'), // Close Jan 15, 2015
     array('is_maintain, area_code_id, phone, first_name, province_id,district_id,street_id,house_numbers', 'required','on'=>'create_customer_store_card,update_customer_store_card'),
     array('price', 'required','on'=>'create_customer_store_card'),// Close on May 05, 2016
    // array('is_maintain, first_name, province_id,district_id,ward_id,phone,street_id,house_numbers', 'required','on'=>'create_customer_store_card,update_customer_store_card'), 
    // vì cột is_maintain không dùng trong loại KH của thẻ kho nên ta sẽ dùng cho 1: KH bình bò, 2. KH mối
    array('parent_id', 'checkParentCustomerStoreCard',
        'on'=>'update_customer_store_card        
    '),
    array('code_account','unique', 'message'=>'Mã kế toán này đã tồn tại trên hệ thống',
        'on'=>'create_customer_store_card,update_customer_store_card'),
                    
    // create_customer_store_card	
// Api Signup Oct 25, 2015
    array('phone, first_name, password, province_id, district_id, ward_id, house_numbers, street', 'required','on'=>'ApiSignup'),
    array('password','length', 'min'=>PASSW_LENGTH_MIN, 'max'=>PASSW_LENGTH_MAX,
        'tooLong'=>'Mật khẩu mới quá dài (tối đa '.PASSW_LENGTH_MAX.' ký tự).',
        'tooShort'=>'Mật khẩu mới quá ngắn (tối thiểu '.PASSW_LENGTH_MIN.' ký tự).',
        'on'=>'ApiSignup'),
    array('password_confirm', 'compare', 'compareAttribute'=>'password','message'=>'Xác nhận mật khẩu mới không đúng.' ,'on'=>'ApiSignup'),
    // Api Signup Oct 25, 2015

    // Api window Create Customer Jun 25, 2016
    array('area_code_id, first_name, phone, province_id, district_id, ward_id, house_numbers', 'required','on'=>'WindowCreateHgd'),
    // Api Android CCS Create Customer Aug 31, 2016
//    array('first_name, phone, province_id, district_id, ward_id, house_numbers', 'required','on'=>'AndroidCreateHgd'),
    array('first_name, phone, province_id, district_id, ward_id', 'required','on'=>'AndroidCreateHgd'),
    // Now 08, 2016 Web Create Customer from transaction
    array('area_code_id, first_name, phone, province_id', 'required','on'=>'TransactionCreate'),
    array('area_code_id, phone', 'required','on'=>'CreateFast'),
    // Dec 08, 2016 app Gas24h update profile
    array('first_name, province_id, district_id, house_numbers', 'required','on'=>'AppGas24hUpdateProfile'),
    array('parent_id, role_id, first_name, phone, province_id, district_id, last_purchase', 'required','on'=>'employees_create, employees_update'),// Aug2017 backend create member
    array('type, parent_id, role_id, first_name, province_id, district_id', 'required','on'=>'createFamily, updateFamily'),// Aug2017 backend create member

    );
}

    public function relations()
    {
        return array(
//            @Code: NAM006
            'rAgent' => array(self::BELONGS_TO, 'Users', 'agent_id'),
            'rCustomer' => array(self::BELONGS_TO, 'Users', 'customer_id'),

            'province' => array(self::BELONGS_TO, 'GasProvince', 'province_id'),
            'channel' => array(self::BELONGS_TO, 'GasChannel', 'channel_id'),
//            'storehouse' => array(self::BELONGS_TO, 'GasStorehouse', 'storehouse_id'),// Close on Feb 15, 2017
            'district' => array(self::BELONGS_TO, 'GasDistrict', 'district_id'),
            'sale' => array(self::BELONGS_TO, 'Users', 'sale_id'),
            'customer' => array(self::BELONGS_TO, 'Users', 'parent_id'),
            'AgentCustomer' => array(self::HAS_MANY, 'GasAgentCustomer', 'agent_id'),
            'CustomerAgent' => array(self::HAS_MANY, 'GasAgentCustomer', 'customer_id'),
            'CustomerMaintain' => array(self::HAS_MANY, 'GasMaintain', 'customer_id'),
            'CustomerMaintainCount'=>array(self::STAT, 'GasMaintain', 'customer_id'),
            'agentCustomerCount'=>array(self::STAT, 'GasAgentCustomer', 'agent_id'),

            'ward' => array(self::BELONGS_TO, 'GasWard', 'ward_id'),
            'street' => array(self::BELONGS_TO, 'GasStreet', 'street_id'),
            'by_agent' => array(self::BELONGS_TO, 'Users', 'area_code_id'),
            'payment' => array(self::BELONGS_TO, 'GasTypePay', 'payment_day'),
            'parent' => array(self::BELONGS_TO, 'Users', 'parent_id'),
            'rParent' => array(self::BELONGS_TO, 'Users', 'parent_id'),
            'rCreatedBy' => array(self::BELONGS_TO, 'Users', 'created_by'),
            'rCompany' => array(self::BELONGS_TO, 'Users', 'storehouse_id'),

            'rProfileAgent' => array(self::HAS_MANY, 'GasProfile', 'agent_id'),
            'rUsersRef' => array(self::HAS_ONE, 'UsersRef', 'user_id',
//                'on'=>'rUsersRef.type='.UsersRef::TYPE_KH_STORECARD,// Close on Jun 11m 2017 có lẽ không cần thiết limit kiểu này, vì mỗi user chỉ có 1 dòng ref bên này thôi
            ),
            'rUsersRefProfile' => array(self::HAS_ONE, 'UsersRef', 'user_id',
                'on'=>'rUsersRefProfile.type='.UsersRef::TYPE_USER_PROFILE,
            ),
            'rExtPhone2' => array(self::HAS_MANY, 'UsersPhoneExt2', 'user_id',
                'on'=>'rExtPhone2.type='.UsersPhoneExt2::TYPE_PRICE_BO_MOI,
            ),
            'rExtPhone3' => array(self::HAS_MANY, 'UsersPhoneExt2', 'user_id',
                'on'=>'rExtPhone3.type='.UsersPhoneExt2::TYPE_BAOTRI_BO_MOI,
            ),
            'rExtPhone4' => array(self::HAS_MANY, 'UsersPhoneExt2', 'user_id',
                'on'=>'rExtPhone4.type='.UsersPhoneExt2::TYPE_PAY_DEBIT,
            ),
            'rUsersProfile' => array(self::HAS_ONE, 'UsersProfile', 'user_id'),
            //            @Code: NAM005
            'rEmployeesImages' => array(self::HAS_MANY, 'EmployeesImages', 'users_id'),
        );
    }

    public function attributeLabels()
    {
        $aLabels=array();
        switch($this->scenario){
            default: // employers end up here
                $aLabels = array(
//                    @Code: NAM006
                    'type_customer'=>'Loại KH',
                    'agent_id'=>'Đại lý',
                    'countVo'=>'Vỏ tồn kho tối thiểu',                    
                    
                    'id' => 'ID',
                    'email' => 'Email',
                    'password_hash' => 'Password',
                    'password_confirm' => 'Confirm Password',
                    'temp_password' => 'Temp Password',
                    'last_name' => 'Cách Xưng Hô',			
                    'created_date' => 'Ngày Tạo',
                    'last_logged_in' => 'Last Logged In',
                    'ip_address' => 'Ip Address',
                    'status' => 'Trạng Thái',
                    'gender' => 'Giới Tính',
                    'first_char' => 'Giới Tính',
                    'phone' => 'Phone',
                    'area_code_id' => 'Nơi Giao',
                    'currentpassword' => 'Mật Khẩu Hiện Tại',
                    'newpassword'=>'Mật Khẩu Mới',
                    'first_name' => 'Họ tên',
                    'name_agent' => 'Tên để sắp xếp (sort) tiếng việt ko dấu',
                    'code_account' => 'Mã Kế Toán',
                    'code_bussiness' => 'Mã Kinh Doanh',
                    'address' => 'Địa Chỉ',
                    'address_temp' => 'Địa Chỉ Mới',
                    'province_id' => 'Tỉnh',
                    'channel_id' => 'Trạng Thái',
                    'district_id' => 'Quận Huyện',
                    'storehouse_id' => 'Công ty',
                    'sale_id' => 'Nhân Viên Sale',
                    'payment_day' => 'Hạn Thanh Toán',
                    'percent_target' => 'Phần trăm target',
                    'beginning' => 'Dư đầu kỳ',
                    'role_id' => 'Loại tài khoản',
                    'maintain_agent_id' => 'Đại Lý Theo Dõi',
                    'is_maintain' => 'Loại KH',
                    'agent_id_search' => 'Đại Lý',
                    'house_numbers' => 'Số Nhà + Thôn/Ấp',
                    'ward_id' => 'Phường/Xã',
                    'street_id' => 'Đường',
                    'parent_id' => 'Thuộc Hệ Thống',
                    'customer_id' => 'Khách Hàng',
                    'assign_employee_sales' => 'KH Có NV Sale',
                    'last_purchase' => 'Đơn hàng mới nhất',
                    'price' => 'Giá bán/sản lượng',
                    'price_other' => 'Giá khác',
                    'slug' => 'Tọa độ google map: lat,lng',
                    'materials_name' => 'Vật tư',
                    'date_from' => 'Từ ngày',
                    'date_to' => 'Đến ngày',
                    'buying' => 'Mua hàng',
                    'phone_ext2' => 'ĐT nhận báo giá',
                    'phone_ext3' => 'ĐT nhận bảo trì',
                    'phone_ext4' => 'ĐT nhận thu tiền công nợ',
                );
            break;
        }

//            if($this->scenario=='create_customer_store_card' || $this->scenario=='update_customer_store_card'){
//                $aLabels['is_maintain'] = 'Loại KH';
//            }
        if($this->scenario=='dieuphoi_create_sale'){
            $aLabels['first_name'] = 'Họ Tên';
            $aLabels['gender'] = 'Loại Sale';
        }
        
        $aScenarioProfile = ['employees_create', 'employees_update'];
        if(in_array($this->scenario, $aScenarioProfile)){
            $aLabels['last_purchase'] = 'Ngày sinh';
        }
        
        return $aLabels;
    }

    public function checkParentCustomerStoreCard($attribute,$params)
    {
        if(!$this->isNewRecord && trim($this->parent_id) !='' && $this->parent_id!=0){
            if($this->parent_id == $this->id){
                $this->addError('parent_id','Hệ thống của khách hàng không hợp lệ. Vui lòng chọn lại');
            }
        }
    }
    	
    public function comparePassword($attribute,$params)
    {
        if(!$this->hasErrors())
        {
            if(trim($this->currentpassword) =='')
                $this->addError('currentpassword','Mật khẩu hiện tại trống.');
            else{
                if($this->md5pass != md5(trim($this->currentpassword))){
                    $this->addError('currentpassword','Mật khẩu hiện tại không đúng.');   
                }
            }
        }
    }

    public function search($criteria = NULL)
    {
        $criteria=new CDbCriteria;
        $criteria->compare('t.id',$this->id);
        $criteria->compare('t.address_vi',$this->address_vi, true);
        $criteria->compare('t.province_id',$this->province_id);
        $criteria->compare('t.district_id',$this->district_id);
        $criteria->compare('t.parent_id',$this->parent_id);
        $criteria->compare('t.created_by',$this->created_by);
        $criteria->compare('t.gender',$this->gender);
        Users::LimitProvince($criteria); // ADD DEC 29, 2014

        $criteria->compare('t.role_id',$this->role_id);
        $criteria->compare('t.application_id',$this->application_id);
        $criteria->compare('t.status',$this->status);
        $criteria->compare('t.first_char',$this->first_char); // DuongNV Oct10,19 search trạng thái thực tế đại lý
        $sort = new CSort();
        $sort->attributes = array(
            'last_logged_in'=>'last_logged_in',
            'username'=>'username',
            'payment_day'=>'payment_day',
            'beginning'=>'beginning',
            'gender'=>'gender',
            'province_id'=>'province_id',
            'district_id'=>'district_id',
        );
        $sort->defaultOrder = 't.status DESC, t.id DESC';
       
        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
            'pagination'=>array(
                'pageSize'=> 100,
            ),                    
            'sort' => $sort,
        ));
    }

    /**
     * @Author: DungNT Dec 29, 2014
     * @Todo: limit province mien tay
     */
    public static function LimitProvince(&$criteria) {
        $cRole  = MyFormat::getCurrentRoleId();
        $cUid   = MyFormat::getCurrentUid();
        $mAppCache = new AppCache();
//        $aRoleCheck = array(ROLE_DIEU_PHOI, ROLE_HEAD_OF_MAINTAIN);// Jul 21, 2016 bỏ check của tổ trưởng tổ bảo trì
        $aRoleCheck = array(ROLE_DIEU_PHOI);
        if(in_array($cRole, $aRoleCheck)){
            $aIdDieuPhoiBoMoi   = $mAppCache->getArrayIdRole(ROLE_DIEU_PHOI);
            if (in_array($cUid, $aIdDieuPhoiBoMoi)){
//                $aZoneHcm = array_merge(GasOrders::$TARGET_ZONE_PROVINCE_MIEN_TAY, GasOrders::$PROVINCE_ZONE_MIEN_TRUNG);
                $aZoneHcm = array_merge(GasOrders::$TARGET_ZONE_PROVINCE_MIEN_TAY);// Jan 06, 2016 Thùy duyệt cả Bình Định
//                $criteria->addNotInCondition('t.province_id', $aZoneHcm);
                $sParamsIn = implode(',', $aZoneHcm);
//                $criteria->addCondition("t.province_id NOT IN ($sParamsIn)");// Apr 27, 2017 Chương xem Cả Miền Tây Luôn
            }elseif (in_array($cUid, GasOrders::$ZONE_MIEN_TAY)){
//                $criteria->addInCondition('t.province_id', GasOrders::$TARGET_ZONE_PROVINCE_MIEN_TAY);
                $sParamsIn = implode(',', GasOrders::$TARGET_ZONE_PROVINCE_MIEN_TAY);
                $criteria->addCondition("t.province_id IN ($sParamsIn)");
            }elseif (in_array($cUid, GasOrders::$ZONE_MIEN_TRUNG)){
//                $criteria->addInCondition('t.province_id', GasOrders::$PROVINCE_ZONE_MIEN_TRUNG);// tạm thời làm kiểu bên dưới cho 1 user điều phối, nếu có 2 tạo thì phải làm khas
                $sZoneMienTrung = implode(",", GasOrders::$PROVINCE_ZONE_MIEN_TRUNG);
                $tGiaLai = GasProvince::TINH_GIALAI;
                $dpBinhDinh = GasLeave::DIEU_PHOI_BINH_DINH;
                $criteria->addCondition("t.province_id IN ($sZoneMienTrung) OR (t.province_id=$tGiaLai AND t.created_by=$dpBinhDinh)" );// tạm thời làm kiểu bên dưới cho 1 user điều phối, nếu có 2 tạo thì phải làm khas
            }
        }
    }
    
    /**
     * @Author: DungNT Dec 29, 2014
     * @Todo: check user can access to view province
     */
    public static function CanAccessProvince($province_id) {
        $cRole = Yii::app()->user->role_id;
        $cUid = Yii::app()->user->id;
        // Jun 11, 2017 Close hết đoạn dưới, cho xem hết
//        if( $cRole==ROLE_DIEU_PHOI){
//            if(in_array($province_id, GasOrders::$TARGET_ZONE_PROVINCE_MIEN_TAY)){
////                return in_array($cUid, GasOrders::$ZONE_MIEN_TAY); // Apr 27,17 mở cho Chương vào cả miền tây
//            }elseif(in_array($province_id, GasOrders::$PROVINCE_ZONE_HCM)){
//                return in_array($cUid, GasOrders::$ZONE_HCM);// uid dieu phoi
//            }
//        }
        return true;
    }

    public function search_customer_maintain($criteria = NULL)
    {
            $criteria=new CDbCriteria;
            $criteria->compare('t.id',$this->id);
            $criteria->compare('t.province_id',$this->province_id);
            $criteria->compare('t.district_id',$this->district_id);
            $criteria->compare('t.role_id',$this->role_id);
            $criteria->compare('t.status',$this->status);
            $criteria->compare('t.is_maintain',$this->is_maintain);
            $criteria->compare('t.type', CUSTOMER_TYPE_MAINTAIN);
            if(isset($this->agent_id_search) && !empty($this->agent_id_search)){
                $criteria->compare('t.area_code_id', $this->agent_id_search);
            }

            $sort = new CSort();
            $sort->attributes = array(
                'code_account'=>'code_account',
                'code_bussiness'=>'code_bussiness',
                'created_date'=>'created_date',
                'is_maintain'=>'is_maintain',
            );    
            $sort->defaultOrder = 't.id DESC';                   

            if(Yii::app()->user->role_id==ROLE_SUB_USER_AGENT)
                $criteria->compare('t.area_code_id',Yii::app()->user->parent_id);

            $_SESSION['data-excel'] = new CActiveDataProvider($this, array(
                    'pagination'=>false,
                    'criteria'=>$criteria,
                    'sort' => $sort,
            ));                
            return new CActiveDataProvider($this, array(
                    'criteria'=>$criteria,
                'pagination'=>array(
                    'pageSize'=> Yii::app()->user->getState('pageSize',Yii::app()->params['defaultPageSize']),
                ),         
                 'sort' => $sort,
            ));
    }
    
    /**
     * @Author: DungNT Apr 05, 2016
     * @Todo: search customer store card wait active
     */
    public function search_customer_store_card_wait_active()
    {
        $cRole = Yii::app()->user->role_id;
        $cUid = Yii::app()->user->id;
        $criteria=new CDbCriteria;
        $aStatus = array(STATUS_WAIT_ACTIVE, STATUS_REJECT);
//        $criteria->addInCondition('t.status', $aStatus);
        $sParamsIn = implode(',', $aStatus);
        $criteria->addCondition("t.status IN ($sParamsIn)");
        $this->getSameCriteriaStoreCard($criteria, $cRole, $cUid);
        $sort = new CSort();
        $sort->attributes = array(
            'code_account'=>'code_account',
            'code_bussiness'=>'code_bussiness',
            'created_date'=>'created_date',
            'is_maintain'=>'is_maintain',
            'username'=>'username',
        );
        $sort->defaultOrder = 't.status ASC, t.id DESC';
        if($this->channel_id == Users::KHONG_LAY_HANG){
            $sort->defaultOrder = 't.last_purchase DESC';
        }

        $_SESSION['data-excel'] = new CActiveDataProvider($this, array(
                'pagination'=>false,
                'criteria'=>$criteria,
                'sort' => $sort,
        ));                
        return new CActiveDataProvider($this, array(
                'criteria'=>$criteria,
            'pagination'=>array(
                'pageSize'=> Yii::app()->user->getState('pageSize',Yii::app()->params['defaultPageSize']),
            ),
             'sort' => $sort,
        ));
    }

    public function search_customer_store_card($criteria = NULL){
        $cRole  = MyFormat::getCurrentRoleId();
        $cUid   = MyFormat::getCurrentUid();
        $criteria=new CDbCriteria;
        $criteria->compare('t.status', STATUS_ACTIVE);
        $this->getSameCriteriaStoreCard($criteria, $cRole, $cUid);
//        $criteria->addNotInCondition('t.is_maintain', $aHgd);
        $sParamsIn = implode(',', CmsFormatter::$aTypeIdListBoMoi);
        $criteria->addCondition("t.is_maintain IN ($sParamsIn)");
        if($cRole == ROLE_ADMIN){
//            $criteria->addCondition("length( code_bussiness ) > 9 AND length( code_bussiness ) < 13 AND code_bussiness like'%hang%'");
//            $criteria->addCondition("length( code_bussiness ) > 5 AND length( code_bussiness ) < 10 AND (code_bussiness like'hang%' OR code_bussiness like'chi%') " );
        }
        
        $sort = new CSort();
        $sort->attributes = array(
            'code_account'=>'code_account',
            'code_bussiness'=>'code_bussiness',
            'created_date'=>'created_date',
            'is_maintain'=>'is_maintain',
            'username'=>'username',
        );
        $sort->defaultOrder = 't.id DESC';
        if($this->channel_id == Users::KHONG_LAY_HANG){
            $sort->defaultOrder = 't.last_purchase DESC';
        }

        $_SESSION['data-excel'] = new CActiveDataProvider($this, array(
                'pagination'=>false,
                'criteria'=>$criteria,
                'sort' => $sort,
        ));                
        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
            'pagination'=>array(
                'pageSize'=> 10,
            ),         
            'sort' => $sort,
        ));
    }
    
    public function searchVip240($criteria = NULL)
    {
        $cRole = MyFormat::getCurrentRoleId();
        $cUid = MyFormat::getCurrentUid();
        $criteria=new CDbCriteria;
//        $criteria->addCondition("t.status=".STATUS_ACTIVE);
        $neeedMore = array('CvCCSView240'=>1);
        $this->getSameCriteriaStoreCard($criteria, $cRole, $cUid, $neeedMore);
        if( $cRole==ROLE_EMPLOYEE_MARKET_DEVELOPMENT ){
            $criteria->compare('t.created_by', $cUid);
            $mUser = new Users();
            $mUser->id = $cUid;
            if($this->buying == UsersExtend::BUYING_DA_MUA){
//                $criteria->addInCondition("t.id", Sell::ccsGetCustomerBuy($mUser));
                $sParamsIn = implode(',', Sell::ccsGetCustomerBuy($mUser));
                if(!empty($sParamsIn)){
                    $criteria->addCondition("t.id IN ($sParamsIn)");
                }
            }elseif($this->buying == UsersExtend::BUYING_CHUA_MUA){
//                $criteria->addNotInCondition("t.id", Sell::ccsGetCustomerBuy($mUser));
                $sParamsIn = implode(',', Sell::ccsGetCustomerBuy($mUser));
                if(!empty($sParamsIn)){
                    $criteria->addCondition("t.id NOT IN ($sParamsIn)");
                }
            }
        }
        $criteria->compare('t.created_by', $this->created_by);
        if($this->ip_address == GasConst::INVEST_YES){
            $criteria->addCondition("t.ip_address<>''");
        }elseif($this->ip_address == GasConst::INVEST_NO){
            $criteria->addCondition("t.ip_address=''");
        }
        if($this->payment_day){
            $criteria->addCondition("t.payment_day <= $this->payment_day");
        }
        if(!empty($this->phone_ext2)){
            // !important Apt1618 => Chay qua lau $criteria->addCondition("t.id IN ( SELECT SubQ.pttt_customer_id FROM gas_code as SubQ WHERE SubQ.code='".trim($this->phone_ext2)."')");
            $criteriaSub = new CDbCriteria();
            $criteriaSub->addCondition("t.code='".trim($this->phone_ext2)."'");
            $model = SpjCode::model()->find($criteriaSub);
            if(!empty($model)){
                $criteria->addCondition('t.id='.$model->pttt_customer_id);
            }
        }
        
        $sort = new CSort();
        $sort->attributes = array(
            'code_account'=>'code_account',
            'code_bussiness'=>'code_bussiness',
            'created_date'=>'created_date',
            'is_maintain'=>'is_maintain',
            'username'=>'username',
        );
        $sort->defaultOrder = 't.id DESC';// bỏ sort để query cho nhanh 
        if($this->channel_id == Users::KHONG_LAY_HANG){
            $sort->defaultOrder = 't.last_purchase DESC';
        }
        $_SESSION['data-excel'] = new CActiveDataProvider($this, array(
                'pagination'=>false,
                'criteria'=>$criteria,
                'sort' => $sort,
        ));                
        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
            'pagination'=>array(
                'pageSize'=> 10,
            ),         
            'sort' => $sort,
        ));
    }
    
    /**
     * @Author: DungNT Apr 16, 2016
     * @Todo: list user name of customer
     */
    public function searchUsernameCustomerStoreCard($needMore = array()){
        $cRole = MyFormat::getCurrentRoleId();
        $cUid = MyFormat::getCurrentUid();
        $criteria=new CDbCriteria;
        $criteria->compare('t.status', STATUS_ACTIVE);
        $this->getSameCriteriaStoreCard($criteria, $cRole, $cUid);
        $sParamsIn = implode(',', CmsFormatter::$aTypeIdListBoMoi);
        $criteria->addCondition("t.is_maintain IN ($sParamsIn)");

        $sort = new CSort();
        $sort->attributes = array(
            'code_account'=>'code_account',
            'code_bussiness'=>'code_bussiness',
            'created_date'=>'created_date',
            'is_maintain'=>'is_maintain',
            'username'=>'username',
        );
        $sort->defaultOrder = 't.username DESC';
        if($this->channel_id == Users::KHONG_LAY_HANG){
            $sort->defaultOrder = 't.last_purchase DESC';
        }
        $_SESSION['data-excel'] = new CActiveDataProvider($this, array(
                'pagination'=>false,
                'criteria'=>$criteria,
                'sort' => $sort,
        ));                
        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
            'pagination'=>array(
                'pageSize'=> 10,
            ),         
            'sort' => $sort,
        ));
    }
    
    /**
     * @Author: DungNT Apr 05, 2016
     */
    public function getSameCriteriaStoreCard(&$criteria, $cRole, $cUid, $needMore = array()) {
        $criteria->compare('t.id', $this->customer_id);
        $criteria->compare('t.province_id',$this->province_id);
        $criteria->compare('t.district_id',$this->district_id);
        $criteria->compare('t.role_id',$this->role_id);
        $criteria->compare('t.parent_id',$this->parent_id);
        $criteria->compare('t.username',$this->username);

        $criteria->compare('t.sale_id',$this->sale_id);
        $criteria->compare('t.channel_id', $this->channel_id);
        if(is_array($this->is_maintain)){
//            $criteria->addInCondition('t.is_maintain', $this->is_maintain);
            $sParamsIn = implode(',', $this->is_maintain);
            if(!empty($sParamsIn)){
                $criteria->addCondition("t.is_maintain IN ($sParamsIn)");
            }
        }elseif(!empty($this->is_maintain)){
            $criteria->addCondition('t.is_maintain='.$this->is_maintain);
        }
        if(!empty($this->first_char)){
            $criteria->addCondition('t.first_char='.$this->first_char);
        }
        if(is_array($this->phone_ext2) && count($this->phone_ext2)){// Jul0519 add multisearch province 
            $sParamsIn = implode(',', $this->phone_ext2);
            $criteria->addCondition("t.province_id IN ($sParamsIn)");
        }
        $criteria->addCondition('t.type='.CUSTOMER_TYPE_STORE_CARD);
        Users::LimitProvince($criteria); // ADD DEC 29, 2014
        if($cRole != ROLE_MONITORING_MARKET_DEVELOPMENT){
            GasAgentCustomer::addInConditionAgent($criteria, 't.area_code_id');
        }

        if($cRole==ROLE_SUB_USER_AGENT){
            $criteria->addCondition('t.area_code_id='.MyFormat::getAgentId());
//        }elseif($cRole==ROLE_EXECUTIVE_OFFICER || $cUid==GasConst::UID_SU_BH){
        }elseif($cRole==ROLE_EXECUTIVE_OFFICER){
            $cProvince = Yii::app()->user->province_id;
            $criteria->addCondition('t.province_id='.$cProvince);
        }

        $aRoleLimit = $this->ARR_ROLE_SALE_LIMIT;
//        if($cUid == GasConst::UID_HUNG_NT){
//            $sParamsIn = implode(',', array($cUid, GasLeave::KH_CONGTY_BO));
//            if(!empty($sParamsIn)){
//                $criteria->addCondition("t.sale_id IN ($sParamsIn)");
//            }
//        }else
        $aUidGiaLai = [GasConst::UID_KIEN_NT, GasConst::UID_DONG_NV];
        $mText = new GasText(); $mProvince = new GasProvince();
        if(in_array($cUid, $aUidGiaLai)){
            $sParamsIn = implode(',', $aUidGiaLai);
            $criteria->addCondition("t.sale_id IN ($sParamsIn)");
        }elseif(in_array($cUid, $mText->getUidBienDong())){// xử lý cho quản lý Biển Đông xem hết KH
            $sParamsIn = implode(',', $mProvince->getProvinceBienDong());
            $criteria->addCondition("t.province_id IN ($sParamsIn)");
        }elseif(in_array($cRole, $aRoleLimit) && !isset($needMore['CvCCSView240'])){
            $criteria->addCondition('t.sale_id=' . $cUid);
        }
        if(isset($this->agent_id_search) && !empty($this->agent_id_search)){
            $criteria->addCondition('t.area_code_id=' . $this->agent_id_search);
        }

        if(trim($this->assign_employee_sales)!=''){
            if($this->assign_employee_sales==1){
                $criteria->addCondition("t.sale_id<>'' AND t.sale_id IS NOT NULL ");
            }elseif($this->assign_employee_sales==0){
                $aUid = Users::getArrIdUserByRole($aRoleLimit);// Mar 08, 2016, tìm những KH mà sale bị xóa khỏi hệ thống rồi                    
                $criteria->addCondition(" t.sale_id='' OR t.sale_id IS NULL OR t.sale_id NOT IN (".  implode(',', $aUid).")");
//                    $criteria->addNotInCondition("t.sale_id", $aUid);
            }                    
        }
        
        if(!empty($this->date_from)){
            $date_from = MyFormat::dateDmyToYmdForAllIndexSearch($this->date_from);
//            $criteria->addCondition("DATE(t.created_date) >= '$date_from'");
            DateHelper::searchGreater($date_from, 'created_date_bigint', $criteria);
        }
        if(!empty($this->date_to)){
            $date_to = MyFormat::dateDmyToYmdForAllIndexSearch($this->date_to);
//            $criteria->addCondition("DATE(t.created_date) <= '$date_to'");
            DateHelper::searchSmaller($date_to, 'created_date_bigint', $criteria, true);
        }
    }
    
    public function searchGasEmployees(){
        $aWith = [];
        $criteria=new CDbCriteria;
        $criteria->compare('t.email',$this->email);
        $criteria->compare('t.username',$this->username);
        $criteria->compare('t.role_id',$this->role_id);
        $criteria->compare('t.status',$this->status);
        $criteria->compare('t.payment_day',$this->payment_day);
        $criteria->compare('t.price',$this->price);
        $criteria->compare('t.storehouse_id',$this->storehouse_id);
        $criteria->compare('t.province_id',$this->province_id);
        $criteria->compare('t.district_id',$this->district_id);
        $criteria->compare('t.address_vi', trim($this->first_name), true);
        $sParamsIn = implode(',', Roles::getRestrict());
        $criteria->addCondition("t.role_id NOT IN ($sParamsIn)");
        $this->employeesLimitUserUpdate($criteria);
        if($this->id){
            $c = "t.area_code_id = $this->id";
            $criteria->addCondition('t.id=' . $this->id." OR ($c)");
        }
        if(!empty($this->contract_type)){
            $aWith[] = 'rUsersProfile';
            $criteria->together = true;
            $criteria->addCondition('rUsersProfile.contract_type='.$this->contract_type);
        }
        if(!empty($this->status_leave)){
            $aWith[] = 'rUsersProfile';
            $criteria->together = true;
            $criteria->addCondition('rUsersProfile.status_leave='.$this->status_leave);
        }
        if(!empty($this->mProfile->id_number)){
            $aWith[] = 'rUsersProfile';
            $criteria->together = true;
            $criteria->addCondition("rUsersProfile.id_number='".trim($this->mProfile->id_number)."'");
        }
        if(!empty($this->mProfile->position_work)){
            $aWith[] = 'rUsersProfile';
            $criteria->together = true;
            $criteria->addCondition("rUsersProfile.position_work='".trim($this->mProfile->position_work)."'");
        }
        if(!empty($this->mProfile->position_room)){
            $aWith[] = 'rUsersProfile';
            $criteria->together = true;
            $criteria->addCondition("rUsersProfile.position_room='".trim($this->mProfile->position_room)."'");
        }
        if(!empty($this->mProfile->salary_method)){
            $aWith[] = 'rUsersProfile';
            $criteria->together = true;
            $criteria->addCondition("rUsersProfile.salary_method='".trim($this->mProfile->salary_method)."'");
        }
        if(!empty($this->mProfile->date_begin_job_from)){
            $aWith[] = 'rUsersProfile';
            $criteria->together = true;
            $date = MyFormat::dateDmyToYmdForAllIndexSearch($this->mProfile->date_begin_job_from);
            $criteria->addCondition("rUsersProfile.date_begin_job >= '$date' ");
        }
        if(!empty($this->mProfile->date_begin_job_to)){
            $aWith[] = 'rUsersProfile';
            $criteria->together = true;
            $date = MyFormat::dateDmyToYmdForAllIndexSearch($this->mProfile->date_begin_job_to);
            $criteria->addCondition("rUsersProfile.date_begin_job <= '$date' ");
        }
        
        if(!empty($this->profile_status)){
            $aWith[] = 'rUsersProfile';
            $criteria->together = true;
            $criteria->addCondition('rUsersProfile.profile_status='.$this->profile_status);
        }

        if(!isset($_GET['tuyendung'])){// Feb2518
            $criteria->addCondition('t.role_id<>'.ROLE_CANDIDATE);
        }else{
            $criteria->addCondition('t.role_id='.ROLE_CANDIDATE);
        }
        $criteria->addCondition('t.role_id<>'.ROLE_BRANCH_COMPANY);
        
        if(count($aWith)){
            $criteria->with = $aWith;
        }
        
        $sort = new CSort();
        $sort->defaultOrder = 't.id DESC';
        
        $_SESSION['dataEmployees'] = new CActiveDataProvider($this, array(
                    'criteria'=>$criteria,
                    'pagination'=> false,
                    'sort' => $sort,
                ));
        
        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
            'pagination'=>array(
                'pageSize'=> 50,
            ),          
            'sort' => $sort,
        ));
    }
    /** @Author: DungNT Sep 08, 2017
     *  @Todo: giới hạn một số vùng cho một số user được phép update
     */
    public function employeesLimitUserUpdate(&$criteria) {
        $cUid = MyFormat::getCurrentUid();
        $sProvince = ''; $mUsersProfile = new UsersProfile();
        switch ($cUid) {
            case GasConst::UID_THUY_HT:
//                $sProvince = implode(',', GasOrders::$TARGET_ZONE_PROVINCE_MIEN_TAY);// Jun1318 ko limit chị Thùy
                break;
            case GasLeave::UID_VIEW_ALL_TAY_NGUYEN_GIA_LAI:
                $sProvince = implode(',', GasOrders::$TARGET_ZONE_PROVINCE_TAY_NGUYEN);
                break;
            case GasConst::UID_NHI_DT:
                $sProvince = implode(',', GasOrders::$TARGET_ZONE_PROVINCE_MIEN_TRUNG);
                break;
            case GasConst::UID_NGAN_LT:
                $sProvince = implode(',', GasOrders::$TARGET_ZONE_PROVINCE_VUNG_TAU);
                break;
            default:
                break;
        }
        if(!empty($sProvince)){
            $criteria->addCondition("t.province_id IN ($sProvince)");
        }
        if(in_array($cUid, $mUsersProfile->getListUidCreate())){
            if($cUid == GasConst::UID_TINH_P){
                $sParamsIn = implode(',',  $mUsersProfile->getListUidCreate());
                $criteria->addCondition("t.created_by IN ($sParamsIn)");
            }elseif(!in_array($cUid, $mUsersProfile->getListUidViewAll())){
//                $criteria->addCondition('t.created_by='.$cUid);// Aug1318 close all user can view
            }
        }
    }

    public function searchSale($criteria = NULL)
    {
        $criteria=new CDbCriteria;
        $criteria->compare('t.email',$this->email,true);
        $criteria->compare('t.role_id',$this->role_id);
        $criteria->compare('t.status',$this->status);
        $criteria->order = "t.id asc";
        return new CActiveDataProvider($this, array(
                'criteria'=>$criteria,
                'pagination'=>array(
                    'pageSize'=> Yii::app()->user->getState('pageSize',Yii::app()->params['defaultPageSize']),
                ),                    
        ));
    }

    public function searchGasMember(){
        $criteria=new CDbCriteria;
        $criteria->compare('t.id',$this->id,true);
        $criteria->compare('t.username', trim($this->username));
        $criteria->compare('t.email', trim($this->email));
        $criteria->compare('t.address_vi',$this->first_name,true);
        if(isset($this->role_id) && trim($this->role_id)!=''){
            $criteria->compare('t.role_id',$this->role_id);
        }else{
//            $criteria->addNotInCondition('t.role_id',  CmsFormatter::$aRoleMemNotLogin);
            $sParamsIn = implode(',', CmsFormatter::$aRoleMemNotLogin);
            $criteria->addCondition("t.role_id NOT IN ($sParamsIn)");
        }
//        $criteria->addNotInCondition('t.role_id', Roles::$aRoleRestrict);
        $sParamsIn = implode(',', Roles::$aRoleRestrict);
        $criteria->addCondition("t.role_id NOT IN ($sParamsIn)");
        $criteria->compare('t.address_vi',$this->address,true);
        $criteria->compare('t.parent_id',$this->parent_id);
        $criteria->compare('t.status',$this->status);
//		$criteria->order = "t.id desc";
        $sort = new CSort();
        $sort->attributes = array(
            'email'=>'email',
            'code_account'=>'code_account',
            'code_bussiness'=>'code_bussiness',
            'username'=>'username',
            'last_logged_in'=>'last_logged_in',
            'address'=>'address',
            'role_id'=>'role_id',
            'gender'=>'gender',
            'status'=>'status',
            'created_date'=>'created_date',
            'parent_id'=>'parent_id',
        );
        $sort->defaultOrder = 't.id DESC';                    

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
            'pagination'=>array(
                'pageSize'=> 100,
            ),                    
            'sort' => $sort,
        ));
    }

    public function unlinkAllFileInFolder($path) {
        $files = glob($path.'/*'); // get all file names
        foreach($files as $file){ // iterate files
            if(is_file($file))
                unlink($file); // delete file
        }
    }

    public function beforeDelete(){
        try{
            if($this->role_id != ROLE_CUSTOMER || $this->status != STATUS_WAIT_ACTIVE){
                $cUid = MyFormat::getCurrentUid();
                SendEmail::bugToDev("Uidlogin = $cUid Check Bug delete user id = $this->id", ['title' => "Uidlogin = $cUid Bug delete user id = $this->id"]);
                return false;// Apr0519 lock delete user
            }
            // Now 26, 2015 không nên xóa dữ liệu của thẻ kho - chấp nhận dư thừa - vì Kiên lỡ tay xóa mất 1 KH nên sẽ đóng lại tránh xóa lần nữa
            $mCronUpdate = new CronUpdate();
            $mCronUpdate->deleteUserRefTable($this);
            $this->solrDelete();
        }catch(Exception $ex){
            throw new Exception($ex->getMessage());
        }
        return parent::beforeDelete();
    }

    public static function deleteArrModel($mDel){
        if(count($mDel)>0)
            foreach ($mDel as $item)
                $item->delete();	
    }
    public function activate(){
        $this->status = 1;
        $this->update(array('status'));
    }
    public function deactivate(){
        $this->status = 0;
        $this->update(array('status'));
        UsersTokens::deleteByUser($this->id);
    }
    
    public function getInforUser($id = null,$name =null){
    	$id 	= $id;
    	$name = trim($name);
    	if(empty($id)) return ;
    	if(!empty($name)) $result = Users::model()->findByPk($id)->$name;
    	else $result = Users::model()->findByPk($id);
    	return $result;
    }
    public static function loadItems($emptyOption=false)
    {
        $_items = array();
        if($emptyOption)
                $_items[""]="";	
        $model=self::model()->findByPk(Yii::app()->user->getId());
        //foreach($models as $model)
                $_items[$model->id]=$model->email;
        return $_items;		
    }
 
    public static function generateKey($user)
    {
        if(empty($user->email))
            $user->email = '';
        return md5($user->id . $user->email);
    }    
    
    public static function findByVerifyCode($verify_code){
        return Users::model()->find('verify_code='.$verify_code.'');
    }
 
    // $needMore['order'] t.province_id asc, t.id asc
    public static function getArrUserByRole($role_id='', $needMore=array())
    {
        $criteria = new CDbCriteria;
        if($role_id==ROLE_AGENT)
            $criteria->select = 'id, CONCAT(code_account," - ", first_name) as first_name, role_id';
        else
            $criteria->select = 'id, CONCAT(code_bussiness," - ", first_name) as first_name, gender,parent_id, role_id';
        
        if(!empty($role_id)){
            if(is_array($role_id)){
//                $criteria->addInCondition('t.role_id', $role_id);
                $sParamsIn = implode(',', $role_id);
                $criteria->addCondition("t.role_id IN ($sParamsIn)");
            }else{
                $criteria->compare('t.role_id', $role_id);
            }
        }

        GasAgentCustomer::initSessionAgent();
        $session=Yii::app()->session;
        if(isset($session['LIST_AGENT_OF_USER']) && count($session['LIST_AGENT_OF_USER'])){
//            $criteria->addInCondition('t.id', $session['LIST_AGENT_OF_USER']);
            $sParamsIn = implode(',', $session['LIST_AGENT_OF_USER']);
            $criteria->addCondition("t.id IN ($sParamsIn)");
        }        
        $criteria->order = 'id ASC';
        if(isset($needMore['order'])){
            $criteria->order = $needMore['order'];
        }
        $criteria->compare('t.status', STATUS_ACTIVE);
        $models = self::model()->findAll($criteria);
        $aRes = array();
        if(isset($needMore['prefix_type_sale'])){
            foreach ( $models as $item){
                $typeSale = isset(Users::$aTypeSale[$item->gender])?Users::$aTypeSale[$item->gender]:'';
                $AgentBelongTo = $item->rParent?$item->rParent->first_name:'';
                $aRes[$item->id] = $item->first_name." - ".$typeSale." - $AgentBelongTo";
            }
            return $aRes;
        }
        
        // Dec 14, 2015 lấy tên user và role của user đó ra
        if(isset($needMore['get_name_and_role'])){
            foreach ( $models as $item){
                $aRes[$item->id] = $item->first_name." - ".$session['ROLE_NAME_USER'][$item->role_id];
            }
            return $aRes;
        }
        // Dec 14, 2015
        
        return  CHtml::listData($models,'id','first_name');            
    }
    
    public static function getArrUserByRoleForSetUpTarget($role_id, $needMore=array())
    {
        $criteria = new CDbCriteria;
        $criteria->compare('t.role_id', $role_id); 
        $criteria->order = 'id ASC';
        if(isset($needMore['order'])){
            $criteria->order = $needMore['order'];
        }
        $criteria->compare('t.status', STATUS_ACTIVE);
        if(isset($needMore['aTypeSaleNotIn'])){
//            $criteria->addNotInCondition('t.gender', $needMore['aTypeSaleNotIn']);
            $sParamsIn = implode(',', $needMore['aTypeSaleNotIn']);
            $criteria->addCondition("t.gender NOT IN ($sParamsIn)");
        }
        if(isset($needMore['target_chuyenvien_kd_khuvuc'])){
            $criteria->compare('t.gender', Users::SALE_PTTT_KD_KHU_VUC);
        }
        $aRes = array();
        $models = self::model()->findAll($criteria);
        if(isset($needMore['aTypeSaleNotIn'])){
            foreach ( $models as $item){
                $typeSale = isset(Users::$aTypeSale[$item->gender])?Users::$aTypeSale[$item->gender]:'';
                $aRes[$item->id] = $item->code_bussiness."-".$item->first_name." - ".$typeSale;
            }
            return $aRes;
        }
        return  CHtml::listData($models,'id','first_name');            
    }

    public static function getArrUserPTTT($role_id, $needMore=array())
    {
        $criteria = new CDbCriteria;
        if($role_id==ROLE_AGENT)
            $criteria->select = 'id, CONCAT(code_account," - ", first_name) as first_name';
        else
            $criteria->select = 'id, CONCAT(code_bussiness," - ", first_name) as first_name';
        
        if(is_array($role_id)){
//            $criteria->addInCondition('t.role_id', $role_id); 
            $sParamsIn = implode(',', $role_id);
            $criteria->addCondition("t.role_id IN ($sParamsIn)");
        }else{
            $criteria->compare('t.role_id', $role_id); 
        }
        $criteria->compare('t.status', STATUS_ACTIVE);
        
        $criteria->order = 'id ASC';
        if(isset($needMore['order'])){
            $criteria->order = $needMore['order'];
        }
        
        $models = self::model()->findAll($criteria);		
        return  CHtml::listData($models,'id','first_name');            
    }            
 
    public static function getArrUserEmployeeAccounting($role_id='')
    {
        $criteria = new CDbCriteria;
        $criteria->select = 'id, CONCAT(code_bussiness," - ", first_name) as first_name';
        if(!empty($role_id)){
            if(is_array($role_id)){
                $criteria->addInCondition('t.role_id', $role_id); 
            }else{
                $criteria->compare('t.role_id', $role_id); 
            }
        }

        $criteria->compare('t.status', STATUS_ACTIVE);
        $criteria->order = 'code_bussiness ASC';
        $models = self::model()->findAll($criteria);
        return  CHtml::listData($models,'id','first_name');
    }
    /** @Author: HaoNH Aug 28, 2019
     *  @Todo: 
     *  @Param:
     **/
        public static function getArrDropdown($role_id,$option='')
    {
        $criteria = new CDbCriteria;
        if(is_array($role_id)){
            $criteria->addInCondition('t.role_id', $role_id);   
        }else{
            $criteria->compare('t.role_id', $role_id);
        }
        if(!empty($option) && $option['task']=='get-parent-branch'){
           $criteria->compare('t.gender', BranchCompany::IS_BRANCH);
           if(!empty($option['current_id']))
           $criteria->addCondition('t.id !='.$option['current_id']);
        }
        $criteria->compare('t.status', STATUS_ACTIVE);
        $criteria->order = 'code_bussiness ASC';
        $models = self::model()->findAll($criteria);		
        return  CHtml::listData($models,'id','first_name');            
    }            
 
    /**
     * @Author: DungNT Apr 07, 2014
     * @Todo: do hàm bên trên getArrUserByRole lấy bị giới hạn 1 số điều kiện ($session=Yii::app()->session;)
     * hiện tại thì không nên sửa hàm đó, mà viết 1 hàm mới lấy list đại lý không theo điều kiện nào cả
     * dùng cho thống kê http://daukhimiennam.com/admin/gasreports/determineGasGoBack
     */
    public static function getArrUserByRoleNotCheck($role_id='')
    {
        $criteria = new CDbCriteria;
        if($role_id==ROLE_AGENT)
            $criteria->select = 'id, CONCAT(code_account," - ", first_name) as first_name';
        else
            $criteria->select = 'id, CONCAT(code_bussiness," - ", first_name) as first_name';
        
        if(!empty($role_id)){
            if(is_array($role_id)){
                $criteria->addInCondition('t.role_id', $role_id); 
            }else{
                $criteria->compare('t.role_id', $role_id); 
            }
        }
            
        $session=Yii::app()->session;
        if(isset($session['LIST_AGENT_OF_USER']) && count($session['LIST_AGENT_OF_USER'])){
//            $criteria->addInCondition('t.id', $session['LIST_AGENT_OF_USER']); 
            $sParamsIn = implode(',', $session['LIST_AGENT_OF_USER']);
            $criteria->addCondition("t.id IN ($sParamsIn)");
        } 
        
        $criteria->compare('t.status', STATUS_ACTIVE);
        $criteria->order = 't.id ASC';
        if(is_array($role_id)){
            $criteria->order = 't.role_id DESC, t.id ASC';
        }
        
        $models = self::model()->findAll($criteria);		
        return  CHtml::listData($models,'id','first_name');            
    }     
    
    public static function getArrAgentNotWarehouse($needMore=array())
    {
        $criteria = new CDbCriteria;
        $criteria->select = 'id, CONCAT(code_account," - ", first_name) as first_name';
        $criteria->compare('t.role_id', ROLE_AGENT); 
        if(isset($needMore['gender'])){ // xem có lấy kho hay đại lý
            // vì hệ thống chia ra kho và đại lý, nên có chỗ chỉ lấy đại lý, không lấy kho
            $criteria->compare('t.gender', $needMore['gender']);   
        }
        $criteria->order = "t.province_id asc, t.id asc";        
        if(isset($needMore['gender_sort'])){
            $criteria->order = "t.gender ".$needMore['gender_sort'].", t.id asc";        
        }
        
        $models = self::model()->findAll($criteria);		
        return  CHtml::listData($models,'id','first_name');
    }            
 
    // Lấy mảng id đại lý không lấy kho cho vào điều kiện inCondition
    public static function getArrIdAgentNotWarehouse($needMore=array())
    {
        $criteria = new CDbCriteria;
        $criteria->compare('t.role_id', ROLE_AGENT); 
        if(isset($needMore['gender'])){ // xem có lấy kho hay đại lý
            // vì hệ thống chia ra kho và đại lý, nên có chỗ chỉ lấy đại lý, không lấy kho
            if(is_array($needMore['gender'])){
                $criteria->addInCondition('t.gender', $needMore['gender']);
            }else{
                $criteria->compare('t.gender', $needMore['gender']);
            }
        }
        $criteria->addCondition('t.status=' . STATUS_ACTIVE);
        $criteria->order = "t.province_id asc, t.id asc";        
        $models = self::model()->findAll($criteria);
        return  CHtml::listData($models,'id','id');            
    }           
    
    public static function getArrIdUserByRole($role_id=ROLE_AGENT)
    {
        $criteria = new CDbCriteria;
        if(is_array($role_id)){
            $criteria->addInCondition('t.role_id', $role_id);
        }else{
            $criteria->compare('t.role_id', $role_id);
        }
        $models = self::model()->findAll($criteria);
        return  CHtml::listData($models,'id','id');            
    }            

    /**
     * @Author: DungNT Mar 26, 2014
     * @Todo: lấy mảng id user theo 1 nhóm role dùng cho chtml
     * @Param: $aRoleId  array role
     */
    public static function getArrIdUserByArrayRole($aRoleId, $needMore=array())
    {
        $criteria = new CDbCriteria;        
        $criteria->addInCondition('t.role_id', $aRoleId);
        $criteria->compare('t.status', STATUS_ACTIVE);
        if(isset($needMore['gender'])){ // xem có lấy kho hay đại lý
            // vì hệ thống chia ra kho và đại lý, nên có chỗ chỉ lấy đại lý, không lấy kho
            if(is_array($needMore['gender']))
                $criteria->addInCondition('t.gender', $needMore['gender']);   
            else
                $criteria->compare('t.gender', $needMore['gender']);
        }
        
        if(isset($needMore['monitor_chuyenvien_kinhdoanh_khuvuc'])){
            $criteria->compare('t.is_maintain', 1);
        }
        if(isset($needMore['province_id'])){
            if(is_array($needMore['province_id'])){
//                $criteria->addInCondition('t.province_id', $needMore['province_id']);
                $sParamsIn = implode(',', $needMore['province_id']);
                if(!empty($sParamsIn)){
                    $criteria->addCondition("t.province_id IN ($sParamsIn)");
                }
            }
            else
                $criteria->compare('t.province_id', $needMore['province_id']);
        }
        
        if(isset($needMore['order'])){
            $criteria->order = $needMore['order'];
        }
        
        $models = self::model()->findAll($criteria);
        if(isset($needMore['GetListdata'])){
            return  CHtml::listData($models,'id','first_name');
        }
        return  CHtml::listData($models,'id','id');
    }
    
    public static function getArrObjectUserByRole($role_id='', $aUid=array(), $needMore=array())
    {
        $criteria = new CDbCriteria;
        if(is_array($role_id)){// Add Dec 16, 2015 khong ro co anh huong den cho nao ko
            $sParamsIn = implode(',', $role_id);
            $criteria->addCondition("t.role_id IN ($sParamsIn)");
        }elseif(!empty($role_id)){
            $criteria->addCondition('t.role_id='.$role_id);
        }
        if(count($aUid)>0){
//            $criteria->addInCondition('t.id',$aUid);
            $sParamsIn = implode(',', $aUid);// Sep 23, 2016 be careful For Big bug
            $criteria->addCondition("t.id IN ($sParamsIn)");
        }
        if(isset($needMore['parent_id']) && !empty($needMore['parent_id'])){
            $criteria->addCondition("t.parent_id={$needMore['parent_id']}");
        }
        if(isset($needMore['gender'])){ // xem có lấy kho hay đại lý
            // vì hệ thống chia ra kho và đại lý, nên có chỗ chỉ lấy đại lý, không lấy kho
            $criteria->addCondition("t.gender={$needMore['gender']}");
        }
        if(!isset($needMore['GetAll'])){
            $criteria->addCondition('t.status=' . STATUS_ACTIVE);
        }
        if(isset($needMore['condition'])){
            $criteria->addCondition($needMore['condition']);
        }
        if(isset($needMore['PaySalary'])){
            $criteria->addCondition('t.payment_day='. UsersProfile::SALARY_YES);
        }
        if(isset($needMore['order'])){
            $criteria->order = $needMore['order'];
        }else{
            $criteria->order = 't.id asc';
        }
        $models = self::model()->findAll($criteria);
        $res=array();
        foreach ($models as $item){
            $res[$item->id] = $item;
        }
        return  $res;         
    }   
    
    // return array
    public static function getArrObjectUserByArrUid($aUid, $role_id=ROLE_CUSTOMER, $needMore=array())
    {
        if(count($aUid)<1) return array();
        $criteria = new CDbCriteria;
        if(!empty($role_id)){
            $criteria->addCondition('t.role_id='.$role_id);
        }
//        $criteria->addInCondition('t.id',$aUid);
        $sParamsIn = implode(',', $aUid);
        $criteria->addCondition("t.id IN ($sParamsIn)");
        return self::model()->findAll($criteria);
    }   
	    
    public static function getArrObjectUserByRoleHaveOrder($role_id=ROLE_AGENT, $order='id')
    {
        $criteria = new CDbCriteria;
        if(is_array($role_id)){
            $criteria->addInCondition('t.role_id', $role_id);   
        }elseif(!empty($role_id)){
            $criteria->addCondition('t.role_id='.$role_id);
        }
        $criteria->order = "t.$order asc";
        $models = self::model()->findAll($criteria);
        $res=array();
        foreach ($models as $item){
            $res[$item->id] = $item;
        }
        return  $res;         
    }   	
	    
    
    // Apr 11, 2014 lấy mảng các đại lý chia theo tỉnh
    public static function getArrObjecByRoleGroupByProvince($role_id=ROLE_AGENT, $needMore=array())
    {
        $criteria = new CDbCriteria;
        if(is_array($role_id)){// Add Dec 16, 2015 khong ro co anh huong den cho nao ko
            $sParamsIn = implode(',', $role_id);
            $criteria->addCondition("t.role_id IN ($sParamsIn)");
        }elseif(!empty($role_id)){
            $criteria->addCondition('t.role_id='.$role_id);
        }

        if(isset($needMore['gender'])){ // xem có lấy kho hay đại lý
            // vì hệ thống chia ra kho và đại lý, nên có chỗ chỉ lấy đại lý, không lấy kho
            $criteria->addCondition('t.gender='.$needMore['gender']);
        }
        if(!isset($needMore['GetAll'])){// Add Sep2018
            $criteria->addCondition('t.status='.STATUS_ACTIVE);
        }
//        
        // dùng cho lấy sale mối của thống kê  daily sản lượng http://daukhimiennam.com/admin/gasreports/output_daily
        if(isset($needMore['aUid'])){
//            $criteria->addInCondition('t.id', $needMore['aUid']);
            $sParamsIn = implode(',', $needMore['aUid']);
            if(!empty($sParamsIn)){
                $criteria->addCondition("t.id IN ($sParamsIn)");
            }
        }
        if(isset($needMore['aProvinceId'])){
//            $criteria->addInCondition('t.province_id', $needMore['aProvinceId']);
            $sParamsIn = implode(',', $needMore['aProvinceId']);
            if(!empty($sParamsIn)){
                $criteria->addCondition("t.province_id IN ($sParamsIn)");
            }
        }
        
        $criteria->order = "t.province_id asc, t.id asc";
        if(isset($needMore['order'])){
            $criteria->order = $needMore['order'];
        }
        
        $models = self::model()->findAll($criteria);
        $res=array();
        if(isset($needMore['only_id_model'])){
            // chỉ dùng để lấy id=>model, ko gom theo tỉnh, phục vụ cho 1 số trường hợp
            foreach ($models as $item){
                $res[$item->id] = $item;
            }
        }else{
            foreach ($models as $item){
                $res[$item->province_id][] = $item;
            }
        }
        return  $res;         
    }   	
	
    public static function getArrCustomerForImport()
    {
        $criteria = new CDbCriteria;
        $criteria->compare('t.role_id', ROLE_CUSTOMER);        
        $models = self::model()->findAll($criteria);
        return  CHtml::listData($models,'code_account','id');            
    }            
    
    public static function getSelectByRole($role_id=ROLE_AGENT)
    {
        $criteria = new CDbCriteria;
        $criteria->compare('t.role_id', $role_id);   
        GasAgentCustomer::initSessionAgent();
        $session=Yii::app()->session;
        // chỗ này không hiểu giới hạn cho những role nào?
        // May 22, 2014 trả lời câu hỏi trên: chỗ này là giới hạn cho tất cả các user, mỗi user dc quản lý 1 số dại lý
        // được nhập khi tạo user login
        if(isset($session['LIST_AGENT_OF_USER']) && count($session['LIST_AGENT_OF_USER'])){            
//            $criteria->addInCondition('t.id', $session['LIST_AGENT_OF_USER']);
            $sParamsIn = implode(',', $session['LIST_AGENT_OF_USER']);
            $criteria->addCondition("t.id IN ($sParamsIn)");
        }
//        $criteria->order = "t.name_agent asc";
        $criteria->compare('t.status', STATUS_ACTIVE);
        $criteria->order = "t.province_id asc, t.id asc";
        $models = self::model()->findAll($criteria);
        return  CHtml::listData($models,'id','first_name');            
    }
    
    public static function getSelectByRoleFinal($role_id, $needMore=array())
    {
        $criteria = new CDbCriteria;
        $criteria->compare('t.role_id', $role_id);   
        GasAgentCustomer::initSessionAgent();
        $session=Yii::app()->session;
        // chỗ này không hiểu giới hạn cho những role nào?
        // May 22, 2014 trả lời câu hỏi trên: chỗ này là giới hạn cho tất cả các user, mỗi user dc quản lý 1 số dại lý
        // được nhập khi tạo user login
        if(!isset($needMore['revenue_output']) && isset($session['LIST_AGENT_OF_USER']) && count($session['LIST_AGENT_OF_USER'])){
//            $criteria->addInCondition('t.id', $session['LIST_AGENT_OF_USER']); 
            $sParamsIn = implode(',', $session['LIST_AGENT_OF_USER']);
            $criteria->addCondition("t.id IN ($sParamsIn)");
        }
       
        if(isset($needMore['gender'])){ // xem có lấy kho hay đại lý
            // vì hệ thống chia ra kho và đại lý, nên có chỗ chỉ lấy đại lý, không lấy kho
            if(is_array($needMore['gender'])){
//                $criteria->addInCondition('t.gender', $needMore['gender']);
                $sParamsIn = implode(',', $needMore['gender']);
                $criteria->addCondition("t.gender IN ($sParamsIn)");
            }else{
                $criteria->compare('t.gender', $needMore['gender']);
            }
        }
        if(isset($needMore['aId'])){
//            $criteria->addInCondition('t.id', $needMore['aId']); 
            $sParamsIn = implode(',', $needMore['aId']);
            if(!empty($sParamsIn)){
                $criteria->addCondition("t.id IN ($sParamsIn)");
            }
        }
        if(isset($needMore['GetActive'])){
            $criteria->addCondition('t.status=' . STATUS_ACTIVE);
        }
        
        $criteria->order = "t.province_id asc, t.id asc";
        if(isset($needMore['order'])){
            $criteria->order = $needMore['order'].", t.province_id asc, t.id asc";
        }
            
        $models = self::model()->findAll($criteria);
        return  CHtml::listData($models,'id','first_name');
    }

    public static function getSelectByRoleNotRoleAgent($role_id, $needMore=array())
    {
        $criteria = new CDbCriteria;
        if(is_array($role_id)){// Add Dec 16, 2015 khong ro co anh huong den cho nao ko
            $criteria->addInCondition('t.role_id', $role_id);
        }else{
            if(!empty($role_id)){
                $criteria->compare('t.role_id', $role_id);
            }
        }
        $criteria->compare('t.status', STATUS_ACTIVE);
        $criteria->order = "t.name_agent asc";
//        GasAgentCustomer::initSessionAgent();
//        $session=Yii::app()->session;
//        if(isset($session['LIST_AGENT_OF_USER']) && count($session['LIST_AGENT_OF_USER'])){
//            $criteria->addInCondition('t.id', $session['LIST_AGENT_OF_USER']); 
//        }		
        $models = self::model()->findAll($criteria);
        return  CHtml::listData($models,'id','first_name');            
    }      
	
    /* dùng get data cho select nhân viên bảo trì hay kế toán của đại lý
    * với những nhân viên này dc thiết đặt cho từng đại lý
    * dùng trong form nhập liệu bảo trì KH
    @param: $agent_id: mã đại lý -- Sep 27, 2014 note lại, chỗ này có thể là mã của giám sát pttt, vì hàm này sẽ lấy ra những người trong đội của giám sát đó
    @param: $type_one_many: mã của One many: ONE_AGENT_MAINTAIN , ONE_AGENT_ACCOUNTING
    @param: $agent_id_extra: mã của One many cho truong hop admin edit bao tri, gioi han lai select theo agent id
    */
    public static function getSelectByRoleForAgent($agent_id, $type_one_many, $agent_id_extra='', $needMore=array())
    {
//        if(Yii::app()->user->role_id==ROLE_ADMIN || isset($needMore['get_all']))
        if(isset($needMore['get_all']))
            $listMany = GasOneMany::getArrOfManyIdByType($type_one_many, $agent_id_extra);
        else
            $listMany = GasOneMany::getArrOfManyId($agent_id, $type_one_many);
        if(empty($agent_id) && isset($needMore['form_create']) ) return array();
        $criteria = new CDbCriteria;
//        $criteria->addInCondition('t.id', $listMany); 
        $sParamsIn = implode(',', $listMany);
        if(!empty($sParamsIn)){
            $criteria->addCondition("t.id IN ($sParamsIn)");
        }else{
            return array();
        }
        
        $criteria->compare('t.status', STATUS_ACTIVE);
        $criteria->order = "t.code_bussiness asc";
        if(isset($needMore['status'])){
            $criteria->compare("t.status",$needMore['status']);
        }
        $models = self::model()->findAll($criteria);
        $aRes=array();
        if(isset($needMore['GetNameOnly'])){
            foreach($models as $item){
                $aRes[$item->id] = $item->first_name;
            }
        }else{
            foreach($models as $item){
                $aRes[$item->id] = $item->code_bussiness."-$item->first_name";
            }
        }
        return $aRes;
    }  
	
    /* dùng get data cho thống kê bảo trì theo nhân viên bảo trì hay kế toán của đại lý
    * với những nhân viên này dc thiết đặt cho từng đại lý
    * dùng trong http://localhost/gas/admin/gasmaintain/Statistic_maintain
    @param: $type_one_many: mã của One many: ONE_AGENT_MAINTAIN , ONE_AGENT_ACCOUNTING
    @param: $AGENT_MODEL: array all agent array[agent_id]=>modelUser
    @return: array[agent_id]=>array model User
    */	
    public static function getSelectEmployeeForAgent($type_one_many, $AGENT_MODEL)
    {
        $aRes=array();
        foreach($AGENT_MODEL as $agent_id=>$mUser){
            $listMany = GasOneMany::getArrOfManyId($agent_id, $type_one_many);        
            $criteria = new CDbCriteria;
//                $criteria->addInCondition('t.id', $listMany); 
            $sParamsIn = implode(',', $listMany);
            if(!empty($sParamsIn)){
                $criteria->addCondition("t.id IN ($sParamsIn)");
            }else{
                continue;
            }
            $criteria->order = "t.first_name asc";
            $models = self::model()->findAll($criteria);
            if(count($models)>0){
                $temp=array();
                $temp['mAgent'] = $mUser;
                $temp['mEmployee']= $models;
                $aRes[$agent_id] = $temp;
            }
        }
        return  $aRes;
    }  

    public static function getArrUserForImport($role_id)
    {
        $criteria = new CDbCriteria;
        $criteria->compare('t.role_id', $role_id);        
        $models = self::model()->findAll($criteria);
        return  CHtml::listData($models,'code_account','id');            
    }            
    	
    public static function getArrSaleIdForImport()
    {
        $criteria = new CDbCriteria;
        $criteria->compare('t.role_id', ROLE_CUSTOMER);
        $models = self::model()->findAll($criteria);
        return  CHtml::listData($models,'id','sale_id');            
    }   
    
    public static function getArrPaymentDayOfCustomer()
    {
        $criteria = new CDbCriteria;
        $criteria->compare('t.role_id', ROLE_CUSTOMER);        
        $models = self::model()->findAll($criteria);
        return  CHtml::listData($models,'id','payment_day');            
    }            

    public static function getCodeAccount($pk){
        $model = self::model()->findByPk($pk);
        if($model && !empty($model->code_account))
            return $model->code_account;
        return 'HM';
    }
    
    public static function getCodeBussiness($pk){
        $model = self::model()->findByPk($pk);
        if($model && !empty($model->code_bussiness))
            return $model->code_bussiness;
        return 'HM';
    }

    public function beforeSave() {
        $cUid   = isset(Yii::app()->user) ? Yii::app()->user->id: '';
        $cRole  = isset(Yii::app()->user) ? Yii::app()->user->role_id : '';
        if(empty($this->created_by)){
            $this->created_by = $cUid;
        }
        self::TrimSomeValue($this);
        $this->first_name = preg_replace('!\s+!', ' ', $this->first_name); // DuongNV Oct0219 replace multi space with sigle space
        MyFunctionCustom::buildAddressUser($this);
        
        if($this->role_id==ROLE_CUSTOMER && $this->type != CUSTOMER_TYPE_STORE_CARD){
            $this->first_name = trim(str_replace(range(0,9),'', $this->first_name));
            if($cRole == ROLE_SUB_USER_AGENT){
                // cột area_code_id để xác định KH này là của đại lý nào
                // parent_id ở đây dc sửa là đại lý của user đang login ROLE_SUB_USER_AGENT
                // dùng cho KH bảo trì + kh PTTT + kh thẻ kho 
                $this->area_code_id = Yii::app()->user->parent_id;
            }
        }
        $this->checkPhone11To10();
//        $this->name_agent = strtolower(MyFunctionCustom::getNameRemoveVietnamese($this->first_name));
        return parent::beforeSave();
    }
    
    /** @Author: DungNT Oct 12, 2018
     *  @Todo: check không cho nhập số đt 11 số
     **/
    public function checkPhone11To10() {
        $aPrefix        = UpdateSql1::getArrayPrefixChangeNotZero();
        $aPhone         = explode('-', $this->phone);
        $aPhoneNew      = [];
        foreach($aPhone as $phone){
            $phoneCheck = UsersPhone::removeLeftZero($phone);
            $prefix_code = substr($phoneCheck, 0, 3);
            if(isset($aPrefix[$prefix_code])){
                $charRemove = strlen($prefix_code);
                $newPhone = '0'.$aPrefix[$prefix_code].substr($phoneCheck, $charRemove);
//                throw new Exception("Hệ thống chặn không cho phép nhập điện thoại 11 số, vui lòng kiểm tra lại nhập theo đầu 10 số $newPhone");
                $aPhoneNew[] = $newPhone;
            }else{
                $aPhoneNew[] = $phone;
            }
        }
        $this->phone = implode('-', $aPhoneNew);
    }
 
    public static function check_duplicate(){
        $criteria = new CDbCriteria;
        $criteria->compare('t.role_id', ROLE_CUSTOMER);        
        $models = self::model()->findAll($criteria);
        $arr = CHtml::listData($models,'id','code_account');
		// $arr = array(1=>'1233',2=>'12334',3 =>'Hello' ,4=>'hello', 5=>'U');
		// Convert every value to uppercase, and remove duplicate values
		$withoutDuplicates = array_unique(array_map("strtoupper", $arr));
		// The difference in the original array, and the $withoutDuplicates array
		// will be the duplicate values
		$duplicates = array_diff($arr, $withoutDuplicates);	
		if(count($duplicates)>0){
			echo '<pre>';
			echo print_r($duplicates);
			echo '</pre >';		
		}
    }
 
	// DungNT 11-23-2013 To get array[]=[mMonitor]=>objmonitor,[mEmployee]=>arrayObjEmployee
	// use for /admin/gasmaintain/statistic_market_development_by_month. but not use
    public static function getArrObjectMonitorAndMarketEmployee($arrayIdMonitor, $order='first_name')
    {	
        $aRes=array();
        $arrayIdMonitor = count($arrayIdMonitor)>0?$arrayIdMonitor:Users::getArrIdUserByRole(ROLE_MONITORING_MARKET_DEVELOPMENT);

        foreach($arrayIdMonitor as $key=>$monitor_id){
            $listMany = GasOneMany::getArrOfManyId($monitor_id, ONE_MONITORING_MARKET_DEVELOPMENT);        

            $criteria = new CDbCriteria;
//                $criteria->addInCondition('t.id', $listMany); 
            $sParamsIn = implode(',', $listMany);
            if(!empty($sParamsIn)){
                $criteria->addCondition("t.id IN ($sParamsIn)");
            }else{
                continue;
            }
            $criteria->order = "t.$order asc";
            $models = self::model()->findAll($criteria);
            if(count($models)>0){
                    $temp=array();
                    $temp['mMonitor'] = Users::model()->findByPk($monitor_id);
                    $temp['mEmployee']= $models;
                    $aRes[$monitor_id] = $temp;
            }	
        }		
        return  $aRes;    	
    }  
 
    // DungNT 11-23-2013 To get array[]=[mMonitor]=>objmonitor,[mEmployee]=>arrayObjEmployee
    // use for /admin/gasmaintain/statistic_market_development_by_month
    public static function getArrIdObjectMonitorAndMarketEmployee($arrayIdMonitor, $order='first_name')
    {	
        $aRes=array();
        $arrayIdMonitor = count($arrayIdMonitor)>0?$arrayIdMonitor:Users::getArrIdUserByRole(ROLE_MONITORING_MARKET_DEVELOPMENT);
        foreach($arrayIdMonitor as $key=>$monitor_id){
            $listMany = GasOneMany::getArrOfManyId($monitor_id, ONE_MONITORING_MARKET_DEVELOPMENT);        

            $criteria = new CDbCriteria;
//            $criteria->addInCondition('t.id', $listMany); 
            $sParamsIn = implode(',', $listMany);
            if(!empty($sParamsIn)){
                $criteria->addCondition("t.id IN ($sParamsIn)");
            }else{
                continue;
            }
            $criteria->order = "t.$order asc";
            $models = self::model()->findAll($criteria);
            $aRes[$monitor_id] = Users::model()->findByPk($monitor_id);
            if(count($models)>0){
                    foreach($models as $item)
                            $aRes[$item->id] = $item;
            }
        }
        return  $aRes;    	
    }  
 
    /**
     * @Author: DungNT 11-27-2013
     * @Todo: get model user by multi attributes
     * @Param: array $attr array([id]=>1, [email]=>'anhdung@anhdung.com',['role_id'=>1]) 
     * @Return: model user or null
     */
    public static function getByAttributes($attr)
    {
        $criteria=new CDbCriteria; 
        $mUser = new Users();
        $hasCompare=false;
        if(count($attr)>0){
            foreach($attr as $name_attr=>$value){
                if($mUser->hasAttribute($name_attr)){
                    $criteria->compare("t.$name_attr", $value);   
                    $hasCompare=true;
                }
            }
            if($hasCompare)
                return self::model()->find($criteria);
        }        
        return null;
    } 
	
    public function beforeValidate() {
        self::TrimSomeValue($this);
        $this->mapMaterialPost();
        return parent::beforeValidate();
    }
    
    // Jul 08, 2016 sử dụng static vì có thể dc dùng ở nhiều chỗ
    public static function TrimSomeValue($model){
        $model->first_name = trim($model->first_name);
        $model->username = trim($model->username);
        $model->email = trim($model->email);
        $model->phone = trim($model->phone);
    }
    
    /**
     * @Author: DungNT Jul 08, 2016
     * @Todo: sử dụng map vật tư được bán của đại lý
     */
    public function mapMaterialPost() {
        $aScenarioAgent = array('create_agent', 'update_agent');
        if(!in_array($this->scenario, $aScenarioAgent)){
            return ;
        }
        $cController = strtolower(Yii::app()->controller->id);
        if($cController != "gasagent")
        {//  kiểm tra lần 2 cho chắc, với loại user Agent sẽ không có password_hash hay temp_password, mà 2 cột này sẽ sử dụng cho mục đích khác, hiện tại là lưu vật tư của đại lý
            return ;
        }
        $this->password_hash = "";
        if(isset($_POST['materials_id_agent'])){
            $this->aMaterial = $_POST['materials_id_agent'];
            $this->password_hash = implode(',', $_POST['materials_id_agent']);
        }
    }
        
    /**
     * @Author: DungNT Feb 22, 2014
     * @Todo: get array customer id cho vào incondition theo loại KH bình bò OR Kh mối
     * dùng cho thống kê daily Statistic::Output_daily()
     * @Param: $type là array loại KH bò hay mối, (nếu = 0 là lấy hết cả bò lẫn mối) ?
     */    
    public static function getCustomerBinhBoMoi($aTypeCustomer)
    {
        $criteria=new CDbCriteria; 
        $criteria->addCondition('t.type=' . CUSTOMER_TYPE_STORE_CARD);
        if(!is_array($aTypeCustomer)){// AnhDung Sep0718
            return [];
        }
        if(count($aTypeCustomer) < 1){
            $aTypeCustomer = [STORE_CARD_KH_BINH_BO, STORE_CARD_KH_MOI, STORE_CARD_XE_RAO];
        }
        
        $sParamsIn = implode(',', $aTypeCustomer);
        if(!empty($sParamsIn)){
            $criteria->addCondition("t.is_maintain IN ($sParamsIn)");
        }
        return  CHtml::listData(self::model()->findAll($criteria),'id','id');   
    }  
    
    /**
     * @Author: DungNT Mar 26, 2014
     * @Todo: get array customer id cho vào incondition theo sale
     * dùng cho thống kê daily Statistic::getOutputDailyForTarget()
     * @Param: $sale_id là 1 id hoặc array id
     */    
    public static function getCustomerBySale($sale_id)
    {
        if(empty($sale_id))
            return array();
        $criteria=new CDbCriteria; 
        $criteria->compare("t.type", CUSTOMER_TYPE_STORE_CARD);   
        if(is_array($sale_id) && count($sale_id)){
//            $criteria->addInCondition("t.sale_id", $sale_id);
            $sParamsIn = implode(',', $sale_id);
            $criteria->addCondition("t.sale_id IN ($sParamsIn)");
        }else{
            $criteria->compare("t.sale_id", $sale_id);   
        }
            
        return  CHtml::listData(self::model()->findAll($criteria),'id','id');   
    }     
    
    /**
     * @Author: DungNT Mar 26, 2014
     * @Todo: get array customer id cho vào incondition theo đại lý
     * dùng cho thống kê daily Statistic::getOutputDailyForTarget()
     * @Param: $agent_id là 1 id hoặc array id
     * @Param: $type là loại KH bò hay mối, nếu = 0 là lấy hết cả bò lẫn mối
     */    
    public static function getCustomerBySaleAndType($sale_id, $type)
    {
        if(empty($sale_id))
            return array();
        $criteria=new CDbCriteria; 
        $criteria->compare("t.type", CUSTOMER_TYPE_STORE_CARD); 
        if(is_array($sale_id) && count($sale_id)){
//            $criteria->addInCondition("t.sale_id", $sale_id);
            $sParamsIn = implode(',', $sale_id);
            $criteria->addCondition("t.sale_id IN ($sParamsIn)");
        }else{
            $criteria->compare("t.sale_id", $sale_id);
        }
        $criteria->compare("t.is_maintain", $type);
            
        return  CHtml::listData(self::model()->findAll($criteria),'id','id');   
    }     
    
    /**
     * @Author: DungNT Mar 26, 2014
     * @Todo: get array customer id cho vào incondition theo đại lý
     * dùng cho thống kê daily Statistic::getOutputDailyForTarget()
     * @Param: $agent_id là 1 id hoặc array id
     * @Param: $type là loại KH bò hay mối, nếu = 0 là lấy hết cả bò lẫn mối
     */    
    public static function getCustomerByAgentAndType($agent_id, $type)
    {
        if(empty($agent_id))
            return array();
        $criteria=new CDbCriteria; 
        $criteria->compare("t.type", CUSTOMER_TYPE_STORE_CARD);   
        if(is_array($agent_id) && count($agent_id)){
//            $criteria->addInCondition("t.area_code_id", $agent_id);
            $sParamsIn = implode(',', $agent_id);
            if(!empty($sParamsIn)){
                $criteria->addCondition("t.area_code_id IN ($sParamsIn)");
            }
        }else{
            $criteria->compare("t.area_code_id", $agent_id);   
        }
        $criteria->compare("t.is_maintain", $type);
            
        return  CHtml::listData(self::model()->findAll($criteria),'id','id');   
    }     
    
    /**
     * @Author: DungNT Apr 03, 2014
     * @Todo: cập nhật  giá trị 0,1 cột first_char, cập nhật cho role sub user của đại lý
     * @Param: $user_id is agent id
     */
    public static function updateFirstChar($user_id, $value){
        $criteria=new CDbCriteria; 
        $criteria->compare("t.parent_id", $user_id);
        $criteria->compare("t.role_id", ROLE_SUB_USER_AGENT);
        $models = Users::model()->findAll($criteria);
        if($models){
            foreach($models as $model){
                $model->first_char = $value;
                $model->update(array('first_char'));
            }
        }
    }
    
    /**
     * @Author: DungNT Apr 04, 2014
     * @Todo: LẤY mảng model user dựa vào mảng id user
     * @Param: array id=>model. $aId có thể là array(), khi đó sẽ return về array empty
     */
    public static function getArrayModelByArrayId($aId){
        if(count($aId) < 1){ // BIG bug khi array empty với sql này $criteria->addCondition("t.materials_type_id IN ($sParamsIn)");
            return array();
        }
        $criteria=new CDbCriteria; 
//        $criteria->addInCondition("t.id", $aId);
        $sParamsIn = implode(',', $aId);
        $criteria->addCondition("t.id IN ($sParamsIn)");

        $aRes = array();
        $models = Users::model()->findAll($criteria);
        if(count($models)){
            foreach($models as $item){
                $aRes[$item->id] = $item;
            }
        }
        return $aRes;
    }
    
    // Now  20, 2014
    public static function getOnlyArrayModelByArrayId($aId, $needMore=array()){
        $criteria=new CDbCriteria;
        if(is_array($aId)){
//            $criteria->addInCondition("t.id", $aId);
            $sParamsIn = implode(',', $aId);
            if(!empty($sParamsIn)){
                $criteria->addCondition("t.id IN ($sParamsIn)");
            }else{
                return array();// Sep 23, 2016 fix for big bug 
            }
        }
        if(isset($needMore['order'])){
            $criteria->order = $needMore['order'];
        }
        if(isset($needMore['role_id'])){
//            $criteria->addInCondition("t.role_id", $needMore['role_id']);
            $sParamsIn = implode(',', $needMore['role_id']);
            $criteria->addCondition("t.role_id IN ($sParamsIn)");
        }
        $criteria->compare("t.status", STATUS_ACTIVE);// Apr 20, 2016 fix cho phần chon NV BT => tạo bảo trì định kỳ
        return Users::model()->findAll($criteria);
    }
    
    /**
     * @Author: DungNT Jan 31, 2016
     * @Todo: get data listOption ROLE_E_MAINTAIN
     */
    public static function getListOptionEmployeeMaintain($needMore) {
        $models = Users::getOnlyArrayModelByArrayId("", $needMore);
        $aRes = array();
        foreach($models as $model){
            $aRes[$model->id] = $model->first_name." - ".$model->getTypeCustomerText();
        }
        return $aRes;
    }
    
    /**
     * @Author: DungNT Apr 25, 2014
     * @Todo: cập nhật loại KH cho các table liên quan nều bị change loại KH
     * @Param: $mUser model user
     */
    public static function changeTypeCustomer($mUser){
        // 1. model GasCashBookDetail 
        GasCashBookDetail::model()->updateAll(array('type_customer'=>$mUser->is_maintain),
                    "`customer_id`=$mUser->id");
        // 2. model GasOrdersDetail
        GasOrdersDetail::model()->updateAll(array('type_customer'=>$mUser->is_maintain),
                    "`customer_id`=$mUser->id");
        // 3. model GasRemain
        GasRemain::model()->updateAll(array('type_customer'=>$mUser->is_maintain),
                    "`customer_id`=$mUser->id");
        // 4. model GasStoreCardDetail
        GasStoreCardDetail::model()->updateAll(array('type_customer'=>$mUser->is_maintain),
                    "`customer_id`=$mUser->id");
    }
    
    /*
     * @Author: DungNT Dec 28, 2015
     * @Todo: cập nhật loại Sale ID cho các table liên quan nều bị change Sale
     * @Param: $mUser model user
     */
    public function changeSale(){
        // 1. model GasIssueTickets 
        GasIssueTickets::model()->updateAll(array('sale_id'=>$this->sale_id),
                    "`customer_id`=$this->id");
//        // 2. model GasOrdersDetail
//        GasOrdersDetail::model()->updateAll(array('type_customer'=>$mUser->is_maintain),
//                    "`customer_id`=$mUser->id");
//        // 3. model GasRemain
//        GasRemain::model()->updateAll(array('type_customer'=>$mUser->is_maintain),
//                    "`customer_id`=$mUser->id");
//        // 4. model GasStoreCardDetail
//        GasStoreCardDetail::model()->updateAll(array('type_customer'=>$mUser->is_maintain),
//                    "`customer_id`=$mUser->id");
    }
    
    
    public static function getListOptions($aUid, $needMore=array())
    {
        $criteria = new CDbCriteria;
//        $criteria->addInCondition('t.id',$aUid);
        $sParamsIn = implode(',', $aUid);
        if(!empty($sParamsIn)){
            $criteria->addCondition("t.id IN ($sParamsIn)");
        }else{
            return array();
        }
        if(!isset($needMore['get_all'])){
            $criteria->compare('t.status', STATUS_ACTIVE);
        }
        $criteria->order = "t.name_agent asc";
        $models = self::model()->findAll($criteria);
        return  CHtml::listData($models,'id','first_name');
    } 
    
    /**
     * @Author: DungNT Aug 08, 2014
     * @Todo: cập nhật lần mua hàng mới nhất của KH bò mối, bên thẻ kho đã check đây là xuất bán cho KH
     * @Param: $uid, $date 
     */
    public static function UpdateLastPurchase($uid, $date){
        if(empty($uid)) return;
        $mUser = Users::model()->findByPk($uid);
        if(is_null($mUser->last_purchase) || empty($mUser->last_purchase)){
            GasScheduleEmail::BuildListNotifyNewCustomer($mUser, array('notify_accounting'=>1));
        }// Apr 22, 2016 Khi KH lấy hàng lần đầu thì mới notify cho NV phòng kế toán 

        self::model()->updateByPk($uid, array('last_purchase'=>$date));
    }
    
    /**
     * @Author: DungNT Jun 01, 2016 FIX
     * @Todo: cập nhật lần mua hàng mới nhất của KH bò mối, bên thẻ kho đã check đây là xuất bán cho KH
     * @Param: $last_purchase
     */
    public function UpdateLastPurchaseFix($last_purchase){
        if(is_null($this->last_purchase) || empty($this->last_purchase) 
                || ( !empty($this->last_purchase) && MyFormat::compareTwoDate($last_purchase, $this->last_purchase)) ){
            // So sánh $last_purchase với $this->last_purchase xem cái nào lớn hơn thì lấy
            $this->last_purchase    = $last_purchase;
            if($this->channel_id != Users::CHAN_HANG){
                $this->channel_id = Users::CON_LAY_HANG;
            }
            $this->update(['last_purchase', 'channel_id']);
        }
    }
    
    /**
     * @Author: DungNT Jun 01, 2016 FIX
     * @Todo: gửi mail notify phòng kế toán nếu KH lấy hàng lần đầu
     */
    public function notifyFirstPurchase(){
        if(is_null($this->last_purchase) || empty($this->last_purchase)){
            GasScheduleEmail::BuildListNotifyNewCustomer($this, array('notify_accounting'=>1));
        } // Apr 22, 2016 Khi KH lấy hàng lần đầu thì mới notify cho NV phòng kế toán 
    }
    
    /**
     * @Author: DungNT Aug 12, 2014
     * @Todo: save sale, cho điều phối tạo sale
     * @Param: $model model 
     */
    public static function SaveSale($model){
        $aAttributes = array(
           'first_name','phone'
        );
        MyFormat::RemoveScriptAndBadCharOfModelField($model, $aAttributes, array('RemoveScript'=>1));
        $model->status = STATUS_ACTIVE;
        $model->role_id = ROLE_SALE;
        $model->created_by = Yii::app()->user->id;
        $model->application_id = BE;
        $model->username = '';
        $model->code_account = MyFunctionCustom::getNextIdForEmployee('Users');
        $model->code_bussiness = MyFunctionCustom::getCodeBusinessEmployee($model->first_name, array());
        $model->save();
    }
    
    // format shot name for report /admin/gasreports/inventory
    public static function formatShorNameAgent($ListOptionAgent){
        $aRes = array();
        $session=Yii::app()->session;
//        if(!isset($session['SHORT_NAME_AGENT'])){
        if(1){
            foreach($ListOptionAgent as $agent_id=>$agent_name): 
                $agent_name = str_replace(array("Đại lý","Đại Lý"), '', $agent_name);
                $agent_name = str_replace("Cửa hàng", 'CH', $agent_name);
                $temp = explode(" ", trim($agent_name));
                if(count($temp)>2){
                    $agent_name='';
                    foreach($temp as $key=>$item){
                        if($key==1)
                            $agent_name.=" $item<br/>";
                        else
                            $agent_name.=" $item";
                    }
                }
                $aRes[$agent_id] = trim($agent_name);
            endforeach;
            $session['SHORT_NAME_AGENT'] = $aRes;
        }
        return $session['SHORT_NAME_AGENT'];
    }
    
    /** sử dụng ở chỗ:
     * 1/ admin/gasreports/output_customer
     * 2/ cập nhật sau.... nếu có sử dụng ở chỗ khác
     * @Author: DungNT Aug 16, 2014
     * @Todo: Users::InitSessionModelCustomer( array('arr_role_id' => array(ROLE_AGENT, ROLE_SALE)));
     * khởi tạo session model của hơn 2000 kh bò mối + đại lý + sale
     * @Param: $needMore
     */
    public static function InitSessionModelCustomer($needMore=array()){
        die('Sep 05, 2016 không thể chạy hàm này được nữa');
        $session=Yii::app()->session;
        if(!isset($session['SESSION_MODEL_CUSTOMER_SALE_AGENT'])){
            $criteria=new CDbCriteria;
            $criteria->compare('t.channel_id', Users::CON_LAY_HANG);
            if(!isset($needMore['arr_role_id'])){
                $criteria->addInCondition('t.role_id', ROLE_CUSTOMER);
                $criteria->compare('t.type', CUSTOMER_TYPE_STORE_CARD);
            }else{
                $listRole = implode(',', $needMore['arr_role_id']);
                
                // CLOSE ON MAR 18, 2015 - lại chuyển  sang không close nữa
                $criteria->addCondition(" ( t.role_id=".ROLE_CUSTOMER." AND t.type=".CUSTOMER_TYPE_STORE_CARD." ) OR "
                        . " t.role_id IN ($listRole) ");
                /** @ITEM MAR182015 
                 * fix ON MAR 18, 2015 
                 *  nếu Kh lên đến 10 hay 15 ngàn thì liệu đưa hết vào session như bên trên liệu có dc ko
                 *  còn dùng theo kiểu (fix) bên dưới $session['ARRAY_OUTPUT_CUSTOMER_ID'] thì cảm giác có vẻ chậm hơn
                 *  
                 */
//                if(isset($session['ARRAY_OUTPUT_CUSTOMER_ID'])){
//                    $criteria->addCondition(" t.id IN (".  implode(',', $session['ARRAY_OUTPUT_CUSTOMER_ID'] ).") OR "
//                        . " t.role_id IN ($listRole) ");
//                }else{
//                    return ;
//                }

            }
            // có thể chỗ này add thêm điều kiện last purchase, hay loại KH bò mối gì đó, tùy theo perform của request
            $aRes = array();
            $models = Users::model()->findAll($criteria);
            foreach($models as $item){
                $aRes[$item->id]['first_name'] = $item->first_name;
                $aRes[$item->id]['address'] = $item->address;
            }
            $session['SESSION_MODEL_CUSTOMER_SALE_AGENT'] = $aRes;
        }
        return $session['SESSION_MODEL_CUSTOMER_SALE_AGENT'];
    }
    
    /** Sep 05, 2016 fix lai ham nay, khong dua vao session nua, luc viet thi la 2000KH , gio la 6000 kh, khong chay noi
     * 1/ admin/gasreports/output_customer
     * 2/ cập nhật sau.... nếu có sử dụng ở chỗ khác
     */
    public static function getListdataCustomer($needMore=array()){
        $criteria=new CDbCriteria;
        $listRole = implode(',', $needMore['arr_role_id']);
            // CLOSE ON Sep 05, 2016
//                $criteria->addCondition(" ( t.role_id=".ROLE_CUSTOMER." AND t.type=".CUSTOMER_TYPE_STORE_CARD." ) OR "
//                        . " t.role_id IN ($listRole) ");
            /** @ITEM MAR182015 
             * fix ON MAR 18, 2015 
             *  nếu Kh lên đến 10 hay 15 ngàn thì liệu đưa hết vào session như bên trên liệu có dc ko
             *  còn dùng theo kiểu (fix) bên dưới $session['ARRAY_OUTPUT_CUSTOMER_ID'] thì cảm giác có vẻ chậm hơn
             *  
             */
            if(isset($needMore['arr_customer_id'])){
                $criteria->addCondition(" t.id IN (".  implode(',', $needMore['arr_customer_id'] ).") OR "
                    . " t.role_id IN ($listRole) ");
            }else{
                return array();
            }
        // có thể chỗ này add thêm điều kiện last purchase, hay loại KH bò mối gì đó, tùy theo perform của request
        $aRes = array();
        $models = Users::model()->findAll($criteria);
        foreach($models as $item){
            $aRes[$item->id]['first_name']          = $item->first_name;
            $aRes[$item->id]['address']             = $item->address;
            $aRes[$item->id]['code_bussiness']      = $item->code_bussiness;
        }
        return $aRes;
    }
    
    /** sử dụng ở chỗ:
     * 1/ admin/gasagent/index
     * 2/ cập nhật sau.... nếu có sử dụng ở chỗ khác
     * @Author: DungNT Jun 22, 2016
     * @Todo: Users::InitSessionModelCustomer( array('arr_role_id' => array(ROLE_AGENT, ROLE_SALE)));
     * khởi tạo session model của ke toan Bán Hàng + NV phục vụ KH
     * @Param: $needMore
     */
    public static function InitSessionModelRole($aRole, $needMore=array()){
        $session=Yii::app()->session;
        if(!isset($session['SESSION_MODEL_MEMBER'])){
            $criteria=new CDbCriteria; 
//            $criteria->addInCondition('t.role_id', $aRole);
            $sParamsIn = implode(',', $aRole);
            $criteria->addCondition("t.role_id IN ($sParamsIn)");
            $aRes = array();
            $models = Users::model()->findAll($criteria);
            foreach($models as $item){
                $aRes[$item->id]['first_name'] = $item->first_name;
                $aRes[$item->id]['code_bussiness'] = $item->code_bussiness;
            }
            $session['SESSION_MODEL_MEMBER'] = $aRes;
        }
    }
    
    public static function ResetPassword(&$model){
        $model->doResetPassword();
    }
    public function doResetPassword(){// Feb 05, 2017 move to OOP
        $this->temp_password = ActiveRecord::randString(6, '123456789');
        if($this->passwordType == GasConst::PASS_TYPE_1){// Now0117 xử lý reset cho KH
            $this->temp_password = $this->username;
        }
        $this->password_hash = md5($this->temp_password);
        $this->update(array('temp_password', 'password_hash'));
    }
    
    /**
     * @Author: DungNT Nov 20, 2014
     * @Todo: get list user có mail và active
     * @return array $model
     */
    public static function GetListUserMail($needMore = array() ) {
        $criteria = new CDbCriteria();
        $criteria->addCondition('t.status=' . STATUS_ACTIVE .' AND role_id<>' . ROLE_CUSTOMER); 
        if(isset($needMore['aRoleId'])){
//            $criteria->addInCondition('t.role_id', $needMore['aRoleId']);
            $sParamsIn = implode(',', $needMore['aRoleId']);
            $criteria->addCondition("t.role_id IN ($sParamsIn)");
        }
        if(isset($needMore['aUidNotReset'])){// loại những user không phải reset pass ra
//            $criteria->addNotInCondition('t.id', $needMore['aUidNotReset']);
            $sParamsIn = implode(',', $needMore['aUidNotReset']);
            $criteria->addCondition("t.id NOT IN ($sParamsIn)");
        }
        if(isset($needMore['aUid'])){ // Sep 23, 2015 chuyển sang search uid
//            $criteria->addInCondition('t.id', $needMore['aUid']);
            $sParamsIn = implode(',', $needMore['aUid']);
            if(!empty($sParamsIn)){
                $criteria->addCondition("t.id IN ($sParamsIn)");
            }
        }
        if(isset($needMore['aRoleIdNotIn'])){
//            $criteria->addNotInCondition('t.role_id', $needMore['aRoleIdNotIn']);
            $sParamsIn = implode(',', $needMore['aRoleIdNotIn']);
            $criteria->addCondition("t.role_id NOT IN ($sParamsIn)");
        }
        
        // không nên có đk này,vì sẽ xử lý user login 2 chỗ bị đẩy ra, khi đó sẽ inactive user và send mail reset pass
        $criteria->addCondition('t.email <> "" AND t.email IS NOT NULL ');
//        $criteria->addInCondition('t.id', $aUid, 'OR'); // for test send mail
        return Users::model()->findAll($criteria);
    }
    
    /**
     * @Author: DungNT Now 21, 2014
     * @Todo: tìm những user có sale, lấy mảng id user theo 1 nhóm role dùng cho chtml, 
     * @Param: $aRoleId  array role
     */
    public static function getArrIdUserByArrayRoleHaveSale($aRoleId, $needMore=array())
    {
        $criteria = new CDbCriteria;        
        $criteria->addInCondition('t.role_id', $aRoleId);
        $criteria->compare('t.status', STATUS_ACTIVE);
        $criteria->addCondition("t.sale_id<>'' AND t.sale_id IS NOT NULL ");
        if(isset($needMore['order'])){
            $criteria->order = $needMore['order'];
        }
        $models = self::model()->findAll($criteria);
        $aRes = array();
        foreach ( $models  as $item ){
            $aRes[$item->province_id]['LIST_ID'][] = $item->sale_id;
            $aRes[$item->province_id]['LIST_MODEL'][] = $item;
        }
        return $aRes;
    }    

    /**
     * @Author: DungNT Dec 27, 2014
     * @Todo: get agent_id ( is area_code_id) của Kh bò mối
     * @Param: $customer_id
     */
    public static function GetAgentIdOfCustomer($customer_id) {
        $model = self::model()->findByPk($customer_id);
        if($model){
            return $model->area_code_id;
        }
        return '';
    }
    
    /**
     * @Author: DungNT Dec 30, 2014
     * @Todo: get array model user by array role id
     * @Param: $aRoleId
     */
    public static function getFindAllByArrayRole($aRoleId)
    {
        $criteria = new CDbCriteria;
//        $criteria->addInCondition('t.role_id', $aRoleId);
        $sParamsIn = implode(',', $aRoleId);
        $criteria->addCondition("t.role_id IN ($sParamsIn)");
        $criteria->compare('t.status', STATUS_ACTIVE);
        return self::model()->findAll($criteria);
    } 
    
    /**
     * @Author: DungNT Dec 30, 2014
     * @Todo: get info user by field_name
     */
    public static function GetInfoField($mUser, $field_name) {
        if($mUser){
            return $mUser->$field_name;
        }
        return "";
    }
    
    /**
     * @Author: DungNT Jan 17, 2015
     * @Todo: cập nhật loại sale cho 1 số table khi change type
     * @Param: $model model Users
     */
    public static function UpdateSaleType($model) {
//     1. to day only for GasBussinessContract Jan 17, 2015
        // 2. user sale
        $type = GasBussinessContract::TYPE_SALE_MOI_CHUYEN_VIEN;
        $type_customer = STORE_CARD_KH_MOI;
        if( $model->gender == Users::SALE_BO){
            $type = GasBussinessContract::TYPE_SALE_BO;
            $type_customer = STORE_CARD_KH_BINH_BO;
        }elseif($model->gender == Users::SALE_MOI){
            $type = GasBussinessContract::TYPE_SALE_MOI_NORMAL;
        }
        
        GasBussinessContract::model()->updateAll(array('type'=>$type,'type_customer'=>$type_customer),
                    "`uid_login`=$model->id");
//                    "`customer_id`=$item->id AND date_delivery>='2014-10-01' ");
    }
    
    /**
     * @Author: DungNT Feb 13, 2015
     * @Todo: reset all ngày cập nhật sổ quỹ - thẻ kho của All đại lý về 1 giá trị
     */
    public static function ResetAllDateUpdateStoreCard() {
        if( isset($_GET['reset_date']) ){
            $reset_date = (int)$_GET['reset_date'];
            $criteria = new CDbCriteria();
            $criteria->compare('role_id', ROLE_AGENT);
            Users::model()->updateAll(array('payment_day'=>$reset_date),
                    $criteria);
        }
    }
    
    /**
     * @Author: DungNT Mar 15, 2015
     * @Todo: init session Chtml::listdata(id=>name) of nhân viên PTTT
     * khởi tạo kiểu này để trong grid sẽ không phải query nhiều lần
     * @param: $role_id = ROLE_EMPLOYEE_MARKET_DEVELOPMENT
     */
    public static function InitSessionNameUserByRole($role_id) {
        $session=Yii::app()->session;
        if(!isset($session['SESSION_LIST_NAME_BY_ROLE'])){
            $criteria=new CDbCriteria; 
            $criteria->compare('t.role_id', $role_id);
            $aRes = array();
            $models = Users::model()->findAll($criteria);
            foreach($models as $item){
                $aRes[$item->id] = $item->first_name;
            }
            $session['SESSION_LIST_NAME_BY_ROLE'] = $aRes;
        }
        return $session['SESSION_LIST_NAME_BY_ROLE'];
    }
    
    /**
     * @Author: DungNT Apr 03, 2015
     * @Todo: search khách hàng không lấy hàng từ 15 đến 30 ngày
     */
    public function searchBuy15_30(){
        $criteria=new CDbCriteria;
        $today = date("Y-m-d");
        $date_to = MyFormat::modifyDays($today, 15, '-');
        $date_from = MyFormat::modifyDays($today, 30, '-');
        $criteria->addBetweenCondition("t.last_purchase",$date_from,$date_to);
        
        self::AddCriteriaSame($this, $criteria);

        $sort = new CSort();
        $sort->attributes = array(
            'code_account'=>'code_account',
            'code_bussiness'=>'code_bussiness',
            'first_name'=>'first_name',
            'created_date'=>'created_date',
            'is_maintain'=>'is_maintain',
            'last_purchase'=>'last_purchase',
        );
        $sort->defaultOrder = 't.last_purchase DESC';

        return new CActiveDataProvider($this, array(
                'criteria'=>$criteria,
                'sort' => $sort,
                'pagination'=>array(
                    'pageSize'=> Yii::app()->user->getState('pageSize',Yii::app()->params['defaultPageSize']),
                ),
        ));
    }
    
    /**
     * @Author: DungNT Apr 03, 2015
     * @Todo: search khách hàng không lấy hàng từ 31 đến 45 ngày
     */
    public function searchBuy31_45(){
        $criteria=new CDbCriteria;
        $today = date("Y-m-d");
        $date_to = MyFormat::modifyDays($today, 31, '-');
        $date_from = MyFormat::modifyDays($today, 45, '-');
        $criteria->addBetweenCondition("t.last_purchase",$date_from,$date_to);
        
        self::AddCriteriaSame($this, $criteria);

        $sort = new CSort();
        $sort->attributes = array(
            'code_account'=>'code_account',
            'code_bussiness'=>'code_bussiness',
            'first_name'=>'first_name',
            'created_date'=>'created_date',
            'is_maintain'=>'is_maintain',
            'last_purchase'=>'last_purchase',
        );
        $sort->defaultOrder = 't.last_purchase DESC';

        return new CActiveDataProvider($this, array(
                'criteria'=>$criteria,
                'sort' => $sort,
                'pagination'=>array(
                    'pageSize'=> Yii::app()->user->getState('pageSize',Yii::app()->params['defaultPageSize']),
                ),
        ));
    }
    
    /**
     * @Author: DungNT Apr 03, 2015
     * @Todo: search khách hàng không lấy hàng từ trên 45
     */
    public function searchBuyAbove45(){
        $criteria=new CDbCriteria;
        $today = date("Y-m-d");
        $date_from = MyFormat::modifyDays($today, 46, '-');
        $criteria->addCondition("t.last_purchase<='$date_from'");
        // last_purchase có thể null cho 1 số KH chưa có xuất bán lần nào, có thể do tạo lỗi hoặc do chỉ có nhập từ KH đó
//        $criteria->addCondition("t.last_purchase<='$date_from' OR t.last_purchase IS NULL");
//        $criteria->addCondition('t.last_purchase <> "" AND t.last_purchase IS NOT NULL ');
        
        self::AddCriteriaSame($this, $criteria);

        $sort = new CSort();
        $sort->attributes = array(
            'code_account'=>'code_account',
            'code_bussiness'=>'code_bussiness',
            'first_name'=>'first_name',
            'created_date'=>'created_date',
            'is_maintain'=>'is_maintain',
            'last_purchase'=>'last_purchase',
        );
        $sort->defaultOrder = 't.last_purchase DESC';

        return new CActiveDataProvider($this, array(
                'criteria'=>$criteria,
                'sort' => $sort,
                'pagination'=>array(
                    'pageSize'=> Yii::app()->user->getState('pageSize',Yii::app()->params['defaultPageSize']),
                ),
        ));
    }
    
    /**
     * @Author: DungNT Apr 03, 2015
     * @Todo: add 1 số điều kiện chung cho 3 hàm search table
     */
    public static function AddCriteriaSame($model, &$criteria){
        $criteria->compare('t.id', $model->customer_id);
        $criteria->compare('t.sale_id', $model->sale_id);
        $criteria->compare('t.area_code_id', $model->agent_id_search);
        $criteria->compare('t.is_maintain', $model->is_maintain);
        $criteria->compare('t.role_id',$model->role_id);
        $criteria->compare('t.type',$model->type);
        $criteria->compare('t.channel_id', Users::CON_LAY_HANG);
        $aTypeBoMoi = array(STORE_CARD_KH_BINH_BO, STORE_CARD_KH_MOI);
//        $criteria->addInCondition( 't.is_maintain', $aTypeBoMoi);
        $sParamsIn = implode(',', $aTypeBoMoi);
        $criteria->addCondition("t.is_maintain IN ($sParamsIn)");
        $cRole = MyFormat::getCurrentRoleId();
        if($cRole == ROLE_SALE){
            $cUid = Yii::app()->user->id;
            $criteria->addCondition('t.sale_id='.$cUid);
        }
    }
    
    /**
     * @Author: DungNT Apr 10, 2015
     * @Todo: dùng cột address_temp để lưu json cho 1 số thông tin
     * hiện tại bây giờ là mới dùng cho đại lý, để lưu thông tin 
     * ngày nhập gas dư và thẻ kho mới nhất
     * @Param: $uId id user
     * @Param: $key ex: date_last_gas_remain, date_last_storecard, agent_name_real
     * @Param: $value ex: 2015-10-04
     */
    public static function UpdateKeyInfo($mUser, $key, $value) {
        $mUser->UpdateKeyInfoOOP($key, $value);
    }
    
    /** @Author: DungNT Sep 11, 2017
     *  @Todo: fix chuyển hàm sang OOP 
     */
    public function UpdateKeyInfoOOP($key, $value) {
        $this->setKeyInfo($key, $value);
        $this->update(['address_temp']);
    }
    public function setKeyInfo($key, $value) {
        $json = json_decode($this->address_temp, true);
        if(!is_array($json)){
            $json = [];
        }
        $json[$key] = $value;
        $this->address_temp = MyFormat::jsonEncode($json);
    }

    public static function GetKeyInfo($mUser, $key) { // Apr 10, 2015
        $json = json_decode($mUser->address_temp, true);
        if(isset($json[$key])){
            return $json[$key];
        }
        return '';
    }
    public function getKeyInfoV1($key) {
        $json = json_decode($this->address_temp, true);
        if(isset($json[$key])){
            return $json[$key];
        }
        return '';
    }
    
    /**
     * @Author: DungNT Apr 10, 2015
     */
    public static function GetKeyInfoById($uId, $key) {
        $mUser = self::model()->findByPk($uId);
        return self::GetKeyInfo($mUser, $key);
    }
    
    /**
     * @Author: DungNT Apr 15, 2015
     * @Todo: get type customer: bo, moi, xe rao ...
     * @Param: $model model Users
     */
    public static function GetTypeCustomer($model) {
        if(is_null($model)) return '';
        return $model->is_maintain;
    }
    
    /**
     * @Author: DungNT Apr 15, 2015
     * @Todo: get sale id of customer: bo, moi, xe rao ...
     * @Param: $model model Users
     */
    public static function GetSaleIdOfCustomer($model) {
        if(is_null($model)) return '';
        return $model->sale_id;
    }
    
    /**
     * @Author: DungNT Apr 15, 2015
     * @Todo: get type sale
     * @Param: $model model Users
     */
    public static function GetTypeSale($model) {
        if(is_null($model)) return '';
        return $model->gender;
    }
    public function getSaleType() {
        return isset(Users::$aTypeSale[$this->gender]) ? Users::$aTypeSale[$this->gender] :"";
    }
    
    public static function GetAgentIdOfCustomerByModel($model) {
        if(is_null($model)) return '';
        return $model->area_code_id;
    }
    
    public static function GetAddressUser($model) {
        if(is_null($model)) return '';
        return $model->address;
    }
    
    /**
     * @Author: DungNT Apr 22, 2015
     * @Todo: get id province_id id tỉnh của đại lý
     * @Param: $id agent hoặc bất kỳ user nào, phục vụ cho báo cáo của thẻ kho, tăng giảm sản lương theo tỉnh
     */
    public static function GetProvinceId($id) {
        $model = self::model()->findByPk($id);
        if($model){
            return $model->province_id;
        }
        return 0;
    }
    
    public function getProvince() {
        $model = $this->province;
        if($model){
            return $model->name;
        }
        return "";
    }
    public function getDistrict() {
        $model = $this->district;
        if($model){
            return $model->name;
        }
        return "";
    }
    
    /**
     * @Author: DungNT May 27, 2015
     * @Todo: load model user ref to model User
     */
    public function LoadUsersRef() {        
        if($this->rUsersRef){
            $this->mUsersRef = $this->rUsersRef;
        }else{
            $this->mUsersRef = new UsersRef();
            $this->mUsersRef->user_id = $this->id; 
        }
        $this->mUsersRef->getMapFieldContact();
    }
    
    /**
     * @Author: DungNT Sep 02, 2015
     * @Todo: load model user ref to model User
     */
    public function LoadUsersRefImageSign() {        
        if($this->rUsersRefProfile){
            $this->mUsersRef = $this->rUsersRefProfile;
        }else{
            $this->mUsersRef = new UsersRef();
            $this->mUsersRef->user_id = $this->id; 
            $this->mUsersRef->type = UsersRef::TYPE_USER_PROFILE; 
        }
    }
    
    /**
     * @Author: DungNT May 27, 2015
     * @Todo: update model user ref
     */
    public function SaveUsersRef() {
        $this->mUsersRef->setFieldContact();
        if(is_null($this->mUsersRef->id)){
            if(trim($this->mUsersRef->reason_leave) != '' || trim($this->mUsersRef->contact_person) != ''){
                $this->mUsersRef->save();
            }
        }else{
            $this->mUsersRef->update();
        }
    }
    
    /**
     * @Author: DungNT May 27, 2015
     * @Todo: update khong lay hang
     */
    public function UpdateKhongLayHang() {
        if($this->role_id == ROLE_CUSTOMER && $this->type == CUSTOMER_TYPE_STORE_CARD){
            $this->channel_id = Users::KHONG_LAY_HANG;
            $this->update(array('channel_id'));
        }
    }
    
    public function getCustomerReasonLeave() {
        if($this->rUsersRef){
            return $this->rUsersRef->getReasonLeave();
        }
        return '';
    }
    
    /**
     * @Author: DungNT Jun 09, 2015
     * @Todo: update back lai lay hang
     * tu dong cap nhat lai trang thai lay hang khi nhap the kho
     */
    public static function UpdateBackLayHang($id) {
        $model = Users::model()->findByPk($id);
        if($model){
            $model->channel_id = Users::CON_LAY_HANG;
            $model->update(array('channel_id'));
        }
    }
    
    /** cái này chắc chỉ dùng cho dc bên support customer
     * @Author: DungNT Sep 23, 2015
     * @Todo: get model sub user agent by agent id
     * @param array $agent_id chuyển sang chọn nhiều người thực hiện
     * vì khi lưu xuống db nó sẽ là json
     */
    public static function getModelSubUserAgentByAgentId($agent_id) {
        if(empty($agent_id) || (is_array($agent_id) && count($agent_id)<1 )){
            return array();
        }
        $listModelUser = self::getOnlyArrayModelByArrayId($agent_id);
        $aRes = array();
        foreach($listModelUser as $mUser){
            if($mUser->role_id == ROLE_AGENT){
                $aModelUser = $mUser->getModelSubAgent();
                foreach($aModelUser as $item){
                    $aRes[] = $item;
                }
            }else{
                $aRes[] = $mUser;
            }
        }
        return $aRes;
    }
    
    /**
     * @Author: DungNT Sep 23, 2015
     * @Todo: get list model sub agent by $agent_id
     * @Param: $agent_id id agent
     */
    public function getModelSubAgent() {
        $criteria = new CDbCriteria;
        $criteria->compare('t.role_id', ROLE_SUB_USER_AGENT);
        $criteria->compare('t.parent_id', $this->id);
        return self::model()->findAll($criteria);
    }

    /**
     * @Author: DungNT Sep 19, 2015
     * @Todo: something
     * @Param: $model
     */
    public function getNameWithRole() {
        if(isset(Yii::app()->session)){
            $session=Yii::app()->session;
            $role_name = $session['ROLE_NAME_USER'][$this->role_id];
        }else{
            $role_name = Roles::model()->findByPk($this->role_id)->role_name;
        }
        return $this->first_name." - $role_name";
    }
    
    /**
     * @Author: DungNT Oct 09, 2015
     * @Todo: API find user login with email - api login app login
     * @Param: $email
     */
    public static function ApiGetUserLogin($username) {
        $criteria = new CDbCriteria();
        $criteria->compare('t.username', $username);
//        $sParamsIn = implode(',', Users::$API_ROLE_LOGIN); // Close Feb2319 -- ko can thiet
//        $criteria->addCondition("t.role_id IN ($sParamsIn)");
        return self::model()->find($criteria);
    }
    public static $API_ROLE_LOGIN = array(ROLE_E_MAINTAIN, ROLE_DIEU_PHOI, ROLE_CUSTOMER, ROLE_HEAD_OF_MAINTAIN, 
        ROLE_SALE, ROLE_HEAD_GAS_BO, ROLE_HEAD_GAS_MOI, ROLE_DIRECTOR_BUSSINESS, ROLE_DIRECTOR, ROLE_CHIEF_MONITOR, ROLE_MONITOR_AGENT,
        ROLE_AUDIT, ROLE_ACCOUNTING, ROLE_ACCOUNTING_ZONE, ROLE_MONITORING,ROLE_MONITORING_MARKET_DEVELOPMENT,ROLE_ACCOUNTING_AGENT,
        ROLE_EMPLOYEE_MARKET_DEVELOPMENT,ROLE_HEAD_OF_BUSINESS,ROLE_HEAD_TECHNICAL, ROLE_EMPLOYEE_MAINTAIN,
        ROLE_EXECUTIVE_OFFICER,ROLE_DRIVER, ROLE_SALE_ADMIN, ROLE_DEBT_COLLECTION, ROLE_CRAFT_WAREHOUSE, ROLE_BRANCH_DIRECTOR,
        ROLE_IT, ROLE_PHU_XE, ROLE_SECURITY, ROLE_MARKETING
    );
    
    /**
     * @Author: DungNT Oct 09, 2015
     */
    public function getFullName() {
        return $this->first_name; 
    }
    public function getFullNameTruSoChinh() {
        $res = $this->first_name;
        if($this->is_maintain == UsersExtend::STORE_CARD_HEAD_QUATER){
            $tmp = explode('-', $this->first_name);
            if(count($tmp) > 1){
                unset($tmp[0]);
            }
            $res = implode(' ', $tmp);
        }
        return $res; 
    }
    public function getEmail() {
        return $this->email; 
    }
    public function getPhone() {
        if($this->phone == NO_PHONE_NUMBER){
            return '';
        }
        return empty($this->phone) ? '' : $this->phone; 
    }
    public function getPhoneApiAndroid() {
        $tmp = explode('-', $this->phone);
        return isset($tmp[0]) ? $tmp[0] : '';
    }
    /**
     * @Author: DungNT Jul 08, 2016
     * @Todo: format phone xuong dong khi nhieu so qua
     */
    public function getPhoneShow() {
        if(!$this->canViewMoreInfo()){
            return '';
        }
        if($this->phone == NO_PHONE_NUMBER){
            return '';
        }
        $tmp = explode('-', $this->phone);
        return implode("<br>", $tmp);
    }
    
    public function getAddress() {
        return $this->address; 
    }
    public function getAddressApp() {
        $add = str_replace('Không rõ', '', $this->address);
        $add = trim($add);
        $add = trim($add, ',');
        return $add; 
    }
    
    public function getSaleName() {
        $mUser = $this->sale;
        if($mUser){
            return $mUser->getFullName();
        }
        return '';
    }
    public function getAgentInfo() {
        $company = $this->getParent();
        $bossAgent = $this->getCreatedBy();
        $res = '<b><span class="f_size_15">'. $this->getAgentLandline().'</span></b><br><br><b>'
                . 'SMS: </b><span class="f_size_15">'.UsersPhone::formatPhoneView($this->getPhone()).'</span><br>'
                . '<b>EXT: </b>'.$this->username;
                if($company != ''){
                    $res .= '<br><b>Công ty: </b>'.$company;
                }
                if($bossAgent != ''){
                    $res .= '<br><b>Chủ cơ sở: </b>'.$bossAgent;
                }
        return $res;
    }
    public function getSaleInfo($field_name) {
        $mUser = $this->sale;
        if($mUser){
            return $mUser->$field_name;
        }
        return '';
    }

    public function getInfoLoginMember() {
        $cRole = MyFormat::getCurrentRoleId();
        $res = '';
        if(!empty($this->email)){
            $res .= $this->email.""; 
        }
        if(!empty($this->username)){
            $res .= '<br>'.$this->username; 
        }
        
        if(!empty($this->temp_password) ){
            $aRoleViewPass = array(ROLE_SUB_USER_AGENT, ROLE_ACCOUNTING_AGENT, ROLE_ACCOUNTING_ZONE);
            if($cRole==ROLE_ADMIN){
                $res .= '-'.$this->temp_password; 
            }elseif(in_array($this->role_id, $aRoleViewPass) ){
                $res .= '-'.$this->temp_password; 
            }
        }
        if(!empty($this->phone)){
            $res .= '<br><b>Phone:</b> '.$this->phone; 
        }
        if(!empty($this->code_account)){
            $res .= '<br><b>Mã Kế Toán:</b> '.$this->code_account; 
        }
        $aRoleCount = array(ROLE_SALE, ROLE_MONITORING_MARKET_DEVELOPMENT, ROLE_EMPLOYEE_MARKET_DEVELOPMENT);
        if(in_array($this->role_id, $aRoleCount) ){
//            $res .= '<br><b>Khách Hàng:</b> '.$this->countCustomerBySale(); 
        }
        
        if($this->role_id == ROLE_CAR){
            $res .= '<br><b>Loại Xe:</b> '. GasConst::getCarWeight($this->is_maintain);
            $res .= '<br><b>Hệ Số:</b> '.$this->last_name; 
        }
        if($cRole==ROLE_ADMIN){// Jan 04, 2017 short cut to phân quyền user
            $link = Yii::app()->createAbsoluteUrl('admin/rolesAuth/user', array('id'=>  $this->id));
            if(ActionsUsers::haveCustomAccess($this->id)){
                $actionText = '<span class="hight_light item_b">Có Custom</span>';
            }else{
                $actionText = 'Chưa Có Custom. Tạo mới';
            }
            $res .= '<br><a href="'.$link.'" target="_blank" >'.$actionText.'</a> '; 
        }
        
        return $res;
    }
    
    public function getLastPurchase() {
        if(empty($this->last_purchase)){
            return "";
        }
        return MyFormat::dateConverYmdToDmy($this->last_purchase);
    }
    
    /**
     * @Author: DungNT Mar 07, 2016
     */
    public function countCustomerBySale() {
        $criteria = new CDbCriteria();
        $criteria->compare("sale_id", $this->id);
        $criteria->compare("role_id", ROLE_CUSTOMER);
        return self::model()->count($criteria);
    }
    
    /**
     * @Author: DungNT Dec 27, 2017
     * @Todo: fix Solr search autocomplete from app Gas Service
     */
    public function getAppAutocomplete($q, $needMore) {
        $keyword    = trim($q->keyword);
        $extSolr    = new ExtSolr();
        
        $this->getSolrConditionBoMoi($extSolr, $q);
        $extSolr->sQuery        = "address_vi:\"$keyword\"".$extSolr->sConditionSame ;
        $result                 = $extSolr->searchByKeyword();
        if(count($result->response->docs) < 1){// tìm thêm like nếu condition bên trên ko ra
            $extSolr->sQuery    = "address_vi:*$keyword*".$extSolr->sConditionSame ;
            $result             = $extSolr->searchByKeyword();
        }

        return $result->response->docs;
    }
    
    /** @Author: DungNT Dec 16, 2017
     *  @Todo: sử dụng get same condition
     **/
    public function getSolrConditionBoMoi(&$extSolr, $q) {
        $typeSearch = isset($q->type) ? $q->type : 0;
        $cRole      = $this->role_id;
        $cUid       = $this->id;
        $extSolr->addParams('status', STATUS_ACTIVE);
        $extSolr->addInCondition('role_id', ROLE_CUSTOMER.' OR '.ROLE_AGENT);
        $aTypeNot = [CUSTOMER_TYPE_MAINTAIN, CUSTOMER_TYPE_MARKET_DEVELOPMENT];
        $extSolr->addNotInCondition('type', $aTypeNot);
        $mAppCache = new AppCache();
        
        if($typeSearch == GasConst::SEARCH_AGENT){
            $extSolr->addParams('role_id', ROLE_AGENT);
//            $aAgentNotSearch = GasCheck::getAgentNotGentAuto();
            $aAgentNotSearch = [GasCheck::DL_XUONG_DONG_NAI];
            $extSolr->addNotInCondition('id', $aAgentNotSearch);
        }elseif($typeSearch == GasConst::SEARCH_CUSTOMER){
            $extSolr->addParams('role_id', ROLE_CUSTOMER);
        }
        $extSolr->addNotInCondition('is_maintain', CmsFormatter::$aTypeIdHgd);
        
//        $aRoleLimit = GasConst::getArrRoleLikeSale();
        $aRoleLimit  = [ROLE_SALE, ROLE_MONITORING_MARKET_DEVELOPMENT];
        if($cRole == ROLE_EMPLOYEE_MAINTAIN){ // Mar 15, 2017 limit KH của đại lý
            $agent_id   = $this->parent_id;
            $aIdLimit   = $mAppCache->getCustomerLimitOfAgent($agent_id);
            if(count($aIdLimit) && isset($q->type) && $q->type == GasConst::SEARCH_CUSTOMER ){
                $sParamsIn = implode(' OR ',  $aIdLimit);
                $extSolr->addInCondition('id', $sParamsIn);
            }
        }elseif(in_array($cRole, $aRoleLimit) && $typeSearch != GasConst::SEARCH_AGENT){
            $extSolr->addParams('sale_id', $cUid);
        }elseif($cRole == ROLE_HEAD_GAS_MOI){
            $extSolr->addParams('is_maintain', STORE_CARD_KH_MOI);
        }

        $extSolr->sConditionSame = $extSolr->sQuery;
    }
    
    
    /**
     * @Author: DungNT Dec 04, 2015
     * @Todo: search autocomplete from app Gas Service
     */
    public static function getDataAutocompleteBk($term, $cRole, $cUid, $q, $needMore = []) {
        $criteria = new CDbCriteria();
        $mAppCache = new AppCache();
        $criteria->addCondition('t.status=' . STATUS_ACTIVE);
//            $criteria->addCondition(" ( t.role_id=".ROLE_CUSTOMER." AND t.type=".CUSTOMER_TYPE_STORE_CARD." OR t.role_id=".ROLE_AGENT. ") "); 
        $criteria->addCondition(" t.role_id IN ('".ROLE_CUSTOMER."', '".ROLE_AGENT."')"); 
        $criteria->addSearchCondition('t.address_vi', $term, true); // true ==> LIKE '%...%'
        $aTypeNot = array(CUSTOMER_TYPE_MAINTAIN, CUSTOMER_TYPE_MARKET_DEVELOPMENT);
//        $criteria->addNotInCondition( 't.type', $aTypeNot);
        $sParamsIn = implode(',', $aTypeNot);
        $criteria->addCondition("t.type NOT IN ($sParamsIn)");
        
        if(isset($q->type)){
            if($q->type == GasConst::SEARCH_AGENT){
                $criteria->addCondition('t.role_id='.ROLE_AGENT.' AND t.id<>'. GasCheck::DL_XUONG_DONG_NAI);
            }elseif($q->type == GasConst::SEARCH_CUSTOMER){
                $criteria->addCondition('t.role_id='.ROLE_CUSTOMER); 
            }
        }

        $sParamsIn = implode(',', CmsFormatter::$aTypeIdHgd);
        $criteria->addCondition("t.is_maintain NOT IN ($sParamsIn)");
        
        $criteria->limit = 30;
        $aRoleLikeSale  = array(ROLE_SALE, ROLE_MONITORING_MARKET_DEVELOPMENT);
        $aRoleLimit     = array(ROLE_EMPLOYEE_MAINTAIN);
        if(in_array($cRole, $aRoleLikeSale) && $q->type == GasConst::SEARCH_CUSTOMER){// Jul 25, 2016 limit KH ở daily sản lượng
            $criteria->addCondition('t.sale_id=' . $cUid);
        }
        if(in_array($cRole, $aRoleLimit) && isset($needMore['mUser'])){// Mar 15, 2017 limit KH của đại lý
            $agent_id   = $needMore['mUser']->parent_id;
            $sParamsIn  = implode(',', $mAppCache->getCustomerLimitOfAgent($agent_id));
            if(!empty($sParamsIn) && isset($q->type) && $q->type == GasConst::SEARCH_CUSTOMER ){
                $criteria->addCondition("t.id IN ($sParamsIn)");
            }
        }
        return Users::model()->findAll($criteria);
    }

    public static function getChtmlByRoleAndField($role_id, $field_name1 = "id", $field_name2 = "first_name")
    {
        $criteria = new CDbCriteria;
        if(is_array($role_id)){
//            $criteria->addInCondition("t.role_id", $role_id);
            $sParamsIn = implode(',', $role_id);
            $criteria->addCondition("t.role_id IN ($sParamsIn)");
        }else{
            $criteria->compare('t.role_id', $role_id);
        }
        $criteria->compare('t.status', STATUS_ACTIVE);
        $models = self::model()->findAll($criteria);
        return  CHtml::listData($models, $field_name1, $field_name2);            
    }
    
    /**
     * @Author: DungNT Dec 21, 2015
     * @Todo: get type customer text
     */
    public function getTypeCustomerText() {
        return isset(CmsFormatter::$CUSTOMER_BO_MOI[$this->is_maintain]) ?  CmsFormatter::$CUSTOMER_BO_MOI[$this->is_maintain] : '';
    }

    // sử dụng để kiểm tra role sale tạo khách hàng
    /** @Author: DungNT Apr 05, 2016
     *  @Todo: set sale cho phần tạo KH của sale tạo
     */
    public function getSaleIdCreate() {
        $cRole  = MyFormat::getCurrentRoleId();
        $cUid   = MyFormat::getCurrentUid();
        if(in_array($cRole, $this->ARR_ROLE_SALE_CREATE)){
            $this->sale_id = $cUid;
        }
        
        if(in_array($cRole, UsersRef::$ROLE_240_HGD)){
            $this->is_maintain = STORE_CARD_VIP_HGD;// tạm phục vụ cho phần 240 KH VIP Hộ GĐ
            $this->price = UsersRef::PRICE_OTHER;// tạm phục vụ cho phần 240 KH VIP Hộ GĐ
        }elseif($cRole == ROLE_SUB_USER_AGENT){
            $this->is_maintain = STORE_CARD_HGD;// cho đại lý tạo KH hộ GD bán hàng = phone
            $this->price = UsersRef::PRICE_OTHER;// cho đại lý tạo KH hộ GD bán hàng = phone
        }
    }
    
    /** @Author: DungNT Jun 08, 2016
     *  tạm phục vụ cho phần 240 KH VIP Hộ GĐ
     */
    public function getListDataTypeCustomer() {
        $aRes = CmsFormatter::$CUSTOMER_BO_MOI;
        $cRole = Yii::app()->user->role_id;
        if($cRole == ROLE_EMPLOYEE_MARKET_DEVELOPMENT){
            unset($aRes[STORE_CARD_KH_BINH_BO]);// tạm phục vụ cho phần 240 KH VIP Hộ GĐ
            unset($aRes[STORE_CARD_KH_MOI]);// tạm phục vụ cho phần 240 KH VIP Hộ GĐ
            unset($aRes[STORE_CARD_XE_RAO]);// tạm phục vụ cho phần 240 KH VIP Hộ GĐ
//            unset($aRes[STORE_CARD_HGD]);// tạm phục vụ cho phần 240 KH VIP Hộ GĐ
            unset($aRes[UsersExtend::STORE_CARD_HEAD_QUATER]);// tạm phục vụ cho phần 240 KH VIP Hộ GĐ
            unset($aRes[UsersExtend::STORE_CARD_NCC]);// tạm phục vụ cho phần 240 KH VIP Hộ GĐ
        }elseif($cRole == ROLE_SUB_USER_AGENT){
            unset($aRes[STORE_CARD_KH_BINH_BO]);// tạm phục vụ cho phần 240 KH VIP Hộ GĐ
            unset($aRes[STORE_CARD_KH_MOI]);// tạm phục vụ cho phần 240 KH VIP Hộ GĐ
            unset($aRes[STORE_CARD_XE_RAO]);// tạm phục vụ cho phần 240 KH VIP Hộ GĐ
            unset($aRes[STORE_CARD_VIP_HGD]);// tạm phục vụ cho phần 240 KH VIP Hộ GĐ
            
            unset($aRes[UsersExtend::STORE_CARD_HEAD_QUATER]);// tạm phục vụ cho phần 240 KH VIP Hộ GĐ
            unset($aRes[UsersExtend::STORE_CARD_NCC]);// tạm phục vụ cho phần 240 KH VIP Hộ GĐ
        }elseif($cRole == ROLE_MONITORING_MARKET_DEVELOPMENT){
            unset($aRes[STORE_CARD_VIP_HGD]);// tạm phục vụ cho phần 240 KH VIP Hộ GĐ
            unset($aRes[STORE_CARD_HGD]);// tạm phục vụ cho phần 240 KH VIP Hộ GĐ
            unset($aRes[STORE_CARD_HGD_CCS]);// tạm phục vụ cho phần 240 KH VIP Hộ GĐ
            unset($aRes[UsersExtend::STORE_CARD_HGD_APP]);// tạm phục vụ cho phần 240 KH VIP Hộ GĐ
            unset($aRes[UsersExtend::STORE_CARD_HEAD_QUATER]);// tạm phục vụ cho phần 240 KH VIP Hộ GĐ
            unset($aRes[UsersExtend::STORE_CARD_NCC]);// tạm phục vụ cho phần 240 KH VIP Hộ GĐ
        }
//        unset($aRes[STORE_CARD_HGD]);// tạm unset lại vì hệ thống cũng chưa chạy phần này
        return $aRes;
    }
    
    /**
     * @Author: DungNT Apr 05, 2016
     * @Todo: set status KH khi sale tạo
     */
    public function setStatusSaleCreate() {
        $cRole  = MyFormat::getCurrentRoleId();
        $cUid   = MyFormat::getCurrentUid();
        if($cRole == ROLE_ADMIN){
            return ;// Khi admin update thì không can thiệp vào status
        }
        if($this->is_maintain == UsersExtend::STORE_CARD_FOR_QUOTE){
            $this->status = STATUS_ACTIVE;
        }
        
        $aRoleCheck = [ROLE_SALE, ROLE_MONITORING_MARKET_DEVELOPMENT, 
            ROLE_HEAD_GAS_MOI, ROLE_EMPLOYEE_MAINTAIN,
            ROLE_EMPLOYEE_MARKET_DEVELOPMENT,
            ];
        if(in_array($cRole, $aRoleCheck) && ( empty($this->id) || $this->id > 1005185) ){// Feb 15, 2017 xử lý cho sale update lại thông tin số đt nhận báo giá các KH
            if($this->status != STATUS_ACTIVE){
                $this->status = STATUS_WAIT_ACTIVE;
            }
        }

        // Fix Jun 04, 2016 tất cả các user khác tạo thì status là chờ approve, còn điều phối tạo là dc luôn
//        if(in_array($cRole, $this->ARR_ROLE_SALE_CREATE)){
//            $this->status = STATUS_WAIT_ACTIVE;
//        }else
        $aRoleAllowActive = array(ROLE_DIRECTOR_BUSSINESS, ROLE_HEAD_GAS_BO, ROLE_DIEU_PHOI, ROLE_CALL_CENTER);
        if(in_array($cRole, $aRoleAllowActive)){
            $this->status = STATUS_ACTIVE;
        }
    }

    public function getStatusText() {
        return Users::$USER_STATUS[$this->status];
    }
    public function getCreatedBy() {
        $mUser = $this->rCreatedBy;
        if($mUser){
            return $mUser->getFullName();
        }
        return '';
    }
    
    /**
     * @Author: DungNT Jul 18, 2016
     * @Todo: note: hàm này chỉ nên dùng để get Agent của Customer
     */
    public function getAgentOfCustomer($needMore = array()) {
        $mUser = $this->by_agent;
        if($mUser){
            $phone = '';
            if(isset($needMore['GetPhone'])){
                $phone = " - ".$mUser->getAgentLandline();
            }
            return $mUser->getFullName().$phone;
        }
        return '';
    }
    
    public function getPrice() {
        if($this->price == UsersRef::PRICE_OTHER){
            return $this->price_other;
        }
        return $this->price;
    }
    /**
     * @Author: DungNT Apr 13, 2016
     * @Todo: get name user by relation
     */
    public function getNameByRelation($relationName) {
        $mUser = $this->$relationName;
        if($mUser){
            return $mUser->getFullName();
        }
        return '';
    }
    public function getMasterLookupName($relationName, $fieldName = "name") {
        $model = $this->$relationName;
        if($model){
            return $model->$fieldName;
        }
        return '';
    }

    /**
     * @Author: DungNT Sep 29, 2015
     * @Todo: get field of UserRef
     * @Param: $field_name
     */
    public function getUserRefField($field_name) {
        $mUserRef = $this->rUsersRef;
        if($mUserRef){
            if( in_array($field_name, $mUserRef->ARR_FIELD_CONTACT)){
                return $mUserRef->getFieldContact($field_name);
            }
            return $mUserRef->$field_name;
        }
        return '';
    }    
    
    /**
     * @Author: DungNT Apr 13, 2016
     * @Todo: check dieu phoi co quyen reject customer khong
     */
    public function canRejectCreateCustomer() {
        $cUid = MyFormat::getCurrentUid();
        if( !empty($this->id) && in_array($cUid, GasTickets::$UID_DIEU_PHOI_HCM_CUSTOMER) && $this->status == STATUS_WAIT_ACTIVE){
            return true;
        }
        return false;
    }
    public function setStatus($status) {
        $this->status = $status;
        $this->update(array('status'));
    }
    public function apiCheckChangePass() {
        if($this->role_id != ROLE_CUSTOMER){
            if(in_array($this->temp_password, GasConst::getSimplePassword())){
                return '1';// 8h sáng mai Jun 2617 open ra
            }
            return '0';
        }
        return $this->needChangePass();
    }
    
    /** @Author: DungNT Aug 25, 2017
     * @Todo: check user có phải change pass không
     */
    public function needChangePass() {
        $mMonitorUpdate = new MonitorUpdate();
        $mMonitorUpdate->user_id = $this->id;
        $recordChangePass = $mMonitorUpdate->getRecordChangePass();
        if($recordChangePass){
            return '1';// 8h sáng mai Jun 2617 open ra
        }
        return '0';
    }
    public function handleChangePass($new_password) {
        $this->password_hash = md5($new_password);
        $this->temp_password = $new_password;
        if($this->role_id == ROLE_ADMIN){
            $this->temp_password = '';
        }
        $this->update(array('password_hash', 'temp_password'));
        $mMonitorUpdate = new MonitorUpdate();
        $mMonitorUpdate->user_id = $this->id;
        $mMonitorUpdate->deleteRecordChangePass();
    }
    
    /**
     * @Author: DungNT Apr 20, 2016
     * @Todo: api get array info customer
     */
    public function apiGetInfoCustomer() {
        $aRes = array();
        $mUserRef = $this->rUsersRef;
        if($mUserRef){
            $aRes[] = array(
                'id'=>'boss_name',
                'name'=> $mUserRef->getFieldContact('contact_boss_name'),
            );
            $aRes[] = array(
                'id'=>'boss_phone',
                'name'=> $mUserRef->getFieldContact('contact_boss_phone'),
            );
            $aRes[] = array(
                'id'=>'manage_name',
                'name'=> $mUserRef->getFieldContact('contact_manage_name'),
            );
            $aRes[] = array(
                'id'=>'manage_phone',
                'name'=> $mUserRef->getFieldContact('contact_manage_phone'),
            );
            $aRes[] = array(
                'id'=>'technical_name',
                'name'=> $mUserRef->getFieldContact('contact_technical_name'),
            );
            $aRes[] = array(
                'id'=>'technical_phone',
                'name'=> $mUserRef->getFieldContact('contact_technical_phone'),
            );
        }
        return $aRes;
    }
    
    /** @Author: DungNT May 03, 2016 */
    public static function getAgentByCodeAccount($code_account) {
        $code_account = strtolower($code_account);
        $criteria = new CDbCriteria();
        $criteria->compare("t.code_account", $code_account);
        $criteria->compare("t.role_id", ROLE_AGENT);
        return Users::model()->find($criteria);
    }

    public function apiPhoneGetNameUser() {
        $aCheck = array(ROLE_SALE, ROLE_MONITORING_MARKET_DEVELOPMENT);
        if(in_array($this->role_id, $aCheck)){
            return "Sale - ".$this->getFullName();
        }
        return $this->getFullName();
    }
 
    /**
     * @Author: DungNT Jun 22, 2016
     * @Todo: lấy các thông tin đặc trưng của agent
     */
    public function getInfoAgentOnly() {
        $res = "$this->address<br><strong>Google Map: </strong>$this->slug";
        $listMany = GasOneMany::getArrModelOfManyId($this->id, ONE_AGENT_MAINTAIN);
        $this->getStringInfoUserAgent($res, $listMany, "NV Phục Vụ Khách Hàng");
        $listMany = GasOneMany::getArrModelOfManyId($this->id, ONE_AGENT_ACCOUNTING);
        $this->getStringInfoUserAgent($res, $listMany, "Nhân Viên Kế Toán");
        $this->getAgentMaterial($res);
        return $res;
    }
    
    /**
     * @Author: DungNT Jul 08, 2016
     * @Todo: get string view agent material
     */
    public function getAgentMaterial(&$res) {
        if(trim($this->password_hash) == ""){
            return ;
        }
        $session=Yii::app()->session;
        $aMaterial = explode(',', $this->password_hash);
        $aTmp = array();
        foreach($aMaterial as $material_id){
            if(isset($session['MATERIAL_MODEL_SELL'][$material_id])){
                $aTmp[] = $session['MATERIAL_MODEL_SELL'][$material_id]['name'];
            }
        }
        $res .= "<br><span class='item_b'>Vật Tư: </span>".implode(", ", $aTmp);
    }
    
    /**
     * @Author: DungNT Jul 08, 2016
     * @Todo: get array material
     */
    public function getArrayMaterialAgent() {
        if(trim($this->password_hash) == ""){
            return array();
        }
        return explode(',', $this->password_hash);
    }
    
    /**
     * @Author: DungNT Jun 22, 2016
     */
    public function getStringInfoUserAgent(&$res, $listMany, $title) {
        $session=Yii::app()->session;
        if(count($listMany)){
            $res .= "<br><strong>$title:</strong>";
            foreach ($listMany as $key=>$item){
                $index = $key+1;
                $fullName = "";
                if(isset($session['SESSION_MODEL_MEMBER'][$item->many_id]['first_name'])){
                    $fullName = $session['SESSION_MODEL_MEMBER'][$item->many_id]['code_bussiness']." - ".$session['SESSION_MODEL_MEMBER'][$item->many_id]['first_name'];
                }
                $res .= "<br>$index.  $fullName";
            }
        }
    }
    
    /**
     * @Author: DungNT Jun 22, 2016
     * @Todo: đưa thông tin tỉnh quận của đại lý vào cho KH khi đại lý tạo mới KH hộ GD
     */
    public function mapSomeinfoAgent() {
        $cRole = Yii::app()->user->role_id;
        if($this->isNewRecord && $cRole == ROLE_SUB_USER_AGENT){
            $mAgent = Users::model()->findByPk(MyFormat::getAgentId());
            $this->province_id = $mAgent->province_id;
            $this->district_id = $mAgent->district_id;
        }
    }
    
    /**
     * @Author: DungNT Jun 25, 2016
     * map attribute to model form $q. get post and validate
     * @param: $mUser model user ke toan ban hang dang login
     * @use: 1. http://spj.daukhimiennam.com/api/default/windowCustomerCreate
     */
    public function windowGetPost($q, $mUser) {
        $this->area_code_id     = $q->agent_id;
        $this->first_name       = trim($q->first_name);
        $this->phone            = trim($q->phone);
        $this->province_id      = $q->province_id;
        $this->district_id      = $q->district_id;
        $this->ward_id          = trim($q->ward_id);
        $this->street_id        = trim($q->street_id);
        $this->house_numbers    = trim($q->house_numbers);
        $this->created_by       = $mUser->id;
        $this->is_maintain      = STORE_CARD_HGD;
        $this->channel_id       = Users::CON_LAY_HANG;
        $this->validate();
//        return ;// Close on Aug 22, 2016 check KH Ho GD - Jul 23, 2016 có thể rơi vào TH số đt cũ nhưng KH mới, dc mới, nên sẽ ko check phone của KH nữa
        $this->checkPhoneHgd();
//        $this->checkAddressHgd();
    }
    
    /** @Author: DungNT Aug 31, 2016 */
    public function checkPhoneHgd() {
        if(empty($this->phone)){
            return ;
        }
        $role_id        = array(ROLE_CUSTOMER);// Jul 23, 2016
        $is_maintain    = CmsFormatter::$aTypeIdHgd;
        $mUserPhone     = UsersPhone::getByPhoneAndRole($this->phone, $role_id, $is_maintain);
        if($mUserPhone && $mUserPhone->user_id != $this->id){
            $this->addError('phone', 'Số điện thoại khách hàng này đã tồn tại trên hệ thống, không thể tạo khách hàng, vui lòng bấm tìm kiếm');
        }
    }
    
    /** @Author: DungNT Now 08, 2016 */
    public function checkPhoneHgdApp() {
        if(empty($this->phone)){
            return ;
        }
        $mUserPhone = $this->getModelPhoneApp($this->phone);
        if($mUserPhone && $mUserPhone->user_id != $this->id){
            $this->addError("phone", "Số điện thoại khách hàng này đã tồn tại trên hệ thống, không thể tạo khách hàng, vui lòng bấm tìm kiếm");
        }
    }
    public function getModelPhoneApp($phoneNumber) {
        /** vì KH do KTBH tạo ở PMBH mới sẽ không cho trùng số đt nên sẽ check dc
         Còn KH do CCS tạo thì cho phép trùng => chắc phải xóa dữ liệu CCS đi
         * @note: Now 22, 2016 làm signup cho KH ở app, sẽ chỉ check số phone của KH tạo qua App
         * đã bỏ STORE_CARD_HGD ra khỏi điều kiện check
         */
        $role_id        = array(ROLE_CUSTOMER);// Jul 23, 2016
        $is_maintain    = CmsFormatter::$ARR_TYPE_HGD_APP;
        return UsersPhone::getByPhoneAndRole($phoneNumber, $role_id, $is_maintain);
    }

    public function checkAddressHgd() {// Sep 25, 2016
        if(empty($this->ward_id)){
            return ;
        }
        $house_numbers = strtolower($this->house_numbers);
        $criteria = new CDbCriteria();
        $criteria->addCondition("t.ward_id=$this->ward_id AND LOWER(t.house_numbers)='$house_numbers' ");
        if(self::model()->count($criteria)){
            $this->addError("house_numbers", "Địa chỉ khách hàng này đã tồn tại trên hệ thống, không thể tạo khách hàng, vui lòng bấm tìm kiếm");
        }
    }
    
    /**
     * @Author: DungNT Jun 27, 2016
     * @Todo: check CCS tao 600 KH, vuot qua ko cho tao nua
     */
    public function canCreateMoreCustomer() {
        $cRole = Yii::app()->user->role_id;
        if(in_array($cRole, UsersRef::$ROLE_240_HGD)){
            $cUid = Yii::app()->user->id;
            $total = $this->countCustomerTypeByUser($cUid, STORE_CARD_VIP_HGD);
            if($total >= UsersRef::$LIMIT_240_HGD){
                return false;
            }
        }
        return true;
    }
    
    /**
     * @Author: DungNT Jun 27, 2016
     * count KH ho gia dinh theo Sale
     */
    public function countCustomerTypeByUser($uid, $type_customer) {
        $criteria = new CDbCriteria();
        $criteria->compare("role_id", ROLE_CUSTOMER);
        $criteria->compare("type", CUSTOMER_TYPE_STORE_CARD);
        $criteria->compare("is_maintain", $type_customer);
        $criteria->compare("created_by", $uid);
        return self::model()->count($criteria);
    }
    
    /**
     * @Author: DungNT Jul 18, 2016
     * @Todo: get số máy bàn của Agent landline
     */ 
    public function getAgentLandline() {
        return $this->code_bussiness;
    }
    public function getParent() {
        return $this->rParent ? $this->rParent->getFullName() : "";
    }
    public function getParentCacheSession() {
        $listdataAgent = CacheSession::getListAgent();
        return isset($listdataAgent[$this->parent_id]) ? $listdataAgent[$this->parent_id]['first_name'] : '';
    }
    public function getStreet() {
        $mStreet = $this->street;
        if($mStreet){
            return $mStreet->name;
        }
        return "";
    }
    public function getCredit() {
        $mProfile = new UsersProfile();
        $aCredit = $mProfile->getArrayCredit($this);
        return isset($aCredit[$this->price]) ? $aCredit[$this->price] : '';
    }
    public function getGas24hCode() {
        $mProfile           = new UsersProfile();
        $mProfile->mUser    = $this;
        return $mProfile->getGas24hCodeOnGrid();
    }
    
    public function canUpdateHgd(){// Sep 16, 2016 update on app android
        $cRole = MyFormat::getCurrentRoleId();
        $cUid   = MyFormat::getCurrentUid();
        if($cRole == ROLE_ADMIN || in_array($cUid, GasTickets::$UID_DIEU_PHOI_HCM_CUSTOMER)){
            return true;
        }
        $dayAllow = MyFormat::modifyDays(date('Y-m-d'), Yii::app()->setting->getItem('DaysUpdateHgd'), '-');
        return MyFormat::compareTwoDate($this->created_date, $dayAllow);
    }
    
    public function canUpdateHgdAppAndroid(){// Sep 16, 2016 update on app android
        $dayAllow = MyFormat::modifyDays(date('Y-m-d'), 1, '-');
        return MyFormat::compareTwoDate($this->created_date, $dayAllow);
    }
    
    /**
     * @Author: DungNT Nov 08, 2016
     * @Todo: tạo tài khoản login cho KH hộ GD
     */
    public function makeUsernameLogin($phoneNumber) {
        $this->setUsernameLogin($phoneNumber);
        $this->update(array('username', 'temp_password', 'password_hash'));
    }
    public function setUsernameLogin($phoneNumber) {
        $this->username         = $phoneNumber;
        $this->makePassword();
    }
    public function makePassword() {
        $this->temp_password    = ActiveRecord::randString(6, '123456789');
        $this->password_hash    = md5($this->temp_password);
    }
    
    /**
     * @Author: DungNT Dec 26, 2016
     * @Todo: xử lý save image của user ở table ref
     */
    public function handleSaveUserRef() {
        if(!is_null($this->mUsersRef->image_sign)){
            UsersRef::RemoveFileOnly($this->mUsersRef->id, 'image_sign');
            $this->mUsersRef->image_sign = UsersRef::saveFile($this->mUsersRef, 'image_sign');
            UsersRef::resizeImage($this->mUsersRef, 'image_sign');
        }else{
            $this->mUsersRef->image_sign = $this->mUsersRef->old_image_sign;
        }
        if($this->mUsersRef->isNewRecord){
            $this->mUsersRef->save();
        }else{
            $this->mUsersRef->update();
        }
    }
    
    public function validateGasBrand() {// validate required thương hiệu bán của KH Jan 09, 2017
//        return ;// Open on Feb 06, 2017 -- Jan 09, 2017 tạm close validate này lại vì chưa sử dụng
        $msg = 'Chưa setup thương hiệu gas bán + vỏ cho khách hàng';
//        $aCheck = array(STORE_CARD_KH_BINH_BO, STORE_CARD_KH_MOI);
        $cRole  = MyFormat::getCurrentRoleId();
        $cUid   = MyFormat::getCurrentUid();
        $aRoleAllow = [ROLE_ADMIN, ROLE_CALL_CENTER];
        $aCheck     = CmsFormatter::$aTypeIdHgd;
        $aCheck1    = [UsersExtend::STORE_CARD_HEAD_QUATER, UsersExtend::STORE_CARD_NCC];
        
        if(in_array($this->is_maintain, $aCheck) || in_array($this->is_maintain, $aCheck1) 
            || in_array($cRole, $aRoleAllow) || in_array($cUid, GasTickets::getUidCreateKhApp())
        ){
            return ;
        }// required all type, ko phai KH bo moi thi them bua 1 cai vao
        if(!isset($_POST['gas'])){
            $this->addError('first_name', $msg);
        }
        $errorGas = true;
        foreach($_POST['gas'] as $materials_type_id => $aInfo){
            foreach($aInfo as $key=>$materials_id){
                if($key == 0 && !empty($materials_id)){
                    $errorGas = false;
                }
            }
        }
        $errorVo = true;
        foreach($_POST['vo'] as $materials_type_id => $aInfo){
            foreach($aInfo as $key=>$materials_id){
                if($key == 0 && !empty($materials_id)){
                    $errorVo = false;
                }
            }
        }
        
        if($errorGas || $errorVo){
            $this->addError('first_name', $msg);
        }
    }
    
    /** @Author: DungNT Feb 06, 2017
     * @Todo: get string name gas, vỏ của customer
     */
    public function getGasBrandText() {
        $mUserRef = $this->rUsersRef;
        if($mUserRef){
            $mUserRef->getMapFieldContact();
            return $mUserRef->getGasBrandText();
        }
        return '';
    }
    
    public function validateExtPhone2() {
        $this->setEmail();
        $cRole = MyFormat::getCurrentRoleId();
        $aRoleAllow = [ROLE_ADMIN, ROLE_SUB_USER_AGENT, ROLE_DIEU_PHOI, ROLE_CALL_CENTER];
        
        $this->validateExtPhone3();
        if(!empty($this->mUsersRef->contact_boss_fax)){
            $temp = explode('-', $this->mUsersRef->contact_boss_fax);
            foreach($temp as $phoneNumber){
                $mSms = new GasScheduleSms();
                if(!$mSms->isPhoneRealLength($phoneNumber)){
                    $this->mUsersRef->addError('contact_boss_fax', "Số điện thoại $phoneNumber không phải là số hợp lệ");
                }
            }
        }
        
        if(!empty($this->mUsersRef->contact_boss_mail)){
            $temp = explode(';', $this->mUsersRef->contact_boss_mail);
            foreach($temp as $email){
//                $this->email = $email;// đã gọi $this->setEmail(); thêm 1 cờ để biết customer này có email khi gửi báo giá
                $mUser = new Users('create_customer_store_card');
                $mUser->email = $email; 
                $mUser->validate(['email']);
                if($mUser->hasErrors('email')){
                    $this->mUsersRef->addError('contact_boss_mail', "Email $email không hợp lệ");
                }
            }
        }
        
        // xu ly yeu cau nhap cty cho KH bo moi
        if(!in_array($cRole, $aRoleAllow) && empty($this->storehouse_id)){
            $this->addError('storehouse_id', 'Chưa nhập nhập công ty');
        }
        
//        if(in_array($cRole, $aRoleAllow) || !empty($this->parent_id) || !in_array($this->province_id, UsersPhoneExt2::getProvinceSendSMS())){
        if(in_array($cRole, $aRoleAllow) || !empty($this->parent_id) ){
            // tạm thời ko validate khi điều phối cập nhật KH
            return ;
        }

        if(empty($this->phone_ext2)){
            $this->addError('phone_ext2', 'Chưa nhập số điện thoại nhận báo giá');
        }
        if(empty($this->phone_ext3)){
            $this->addError('phone_ext3', 'Chưa nhập số điện thoại nhận bảo trì');
        }
        if(empty($this->phone_ext4)){
            $this->addError('phone_ext4', 'Chưa nhập số điện thoại nhận thu tiền công nợ');
        }
        if(empty($this->payment_day)){
            $this->addError('payment_day', 'Chưa nhập hạn thanh toán');
        }
    }
    
    public function validateExtPhone3() {// Dec2817 validate phone báo giá và bảo trì KH bò mối
        if(!empty($this->phone_ext2)){// phone báo giá
            $temp = explode('-', $this->phone_ext2);
            foreach($temp as $phoneNumber){
                if(!UsersPhone::isValidCellPhone($phoneNumber)){
                    $this->addError('phone_ext2', "Số điện thoại $phoneNumber không phải là số di động hợp lệ");
                }
            }
        }
        if(!empty($this->phone_ext3)){// phone bảo trì
            $temp = explode('-', $this->phone_ext3);
            foreach($temp as $phoneNumber){
                if(!UsersPhone::isValidCellPhone($phoneNumber)){
                    $this->addError('phone_ext3', "Số điện thoại $phoneNumber không phải là số di động hợp lệ");
                }
            }
        }
        if(!empty($this->phone_ext4)){// phone thu tiền công nợ
            $temp = explode('-', $this->phone_ext4);
            foreach($temp as $phoneNumber){
                if(!UsersPhone::isValidCellPhone($phoneNumber)){
                    $this->addError('phone_ext4', "Số điện thoại $phoneNumber không phải là số di động hợp lệ");
                }
            }
        }
        
        if(empty($this->application_id)){// Oct3018 Line gas
//         Close Now2918  $this->addError('application_id', 'Chưa chọn line gas của KH');
        }
    }
    
    public function setEmail() {// thêm 1 cờ để biết customer này có email khi gửi báo giá
        if(!empty($this->mUsersRef->contact_boss_mail)){
            $temp = explode(';', $this->mUsersRef->contact_boss_mail);
            $this->email = isset($temp[0]) ? $temp[0] : '';
        }else{
            $this->email = '';
        }
    }
    public function saveExtPhone2() {
        $mUsersPhoneExt2            = new UsersPhoneExt2();
        $mUsersPhoneExt2->type      = UsersPhoneExt2::TYPE_PRICE_BO_MOI;
        $mUsersPhoneExt2->phone     = $this->phone_ext2;
        $mUsersPhoneExt2->user_id   = $this->id;
        $mUsersPhoneExt2->savePhone($this);
        
        $mUsersPhoneExt2->type      = UsersPhoneExt2::TYPE_BAOTRI_BO_MOI;
        $mUsersPhoneExt2->phone     = $this->phone_ext3;
        $mUsersPhoneExt2->savePhone($this);
        
        $mUsersPhoneExt2->type      = UsersPhoneExt2::TYPE_PAY_DEBIT;// Jan2218
        $mUsersPhoneExt2->phone     = $this->phone_ext4;
        $mUsersPhoneExt2->savePhone($this);
    }
    public function loadExtPhone2() {
        $aPhone = [];
        foreach($this->rExtPhone2 as $mPhone){
            $aPhone[] = '0'.$mPhone->phone;
            if(empty($this->phone_first)){
                $this->phone_first = '0'.$mPhone->phone;
            }
        }
        $this->phone_ext2 = implode('-', $aPhone);
        
        $aPhone = [];// for phone_ext3 SMS bảo trì
        foreach($this->rExtPhone3 as $mPhone){
            $aPhone[] = '0'.$mPhone->phone;
        }
        $this->phone_ext3 = implode('-', $aPhone);
        
        $aPhone = [];// for phone_ext4 SMS thu nợ KH
        foreach($this->rExtPhone4 as $mPhone){
            $aPhone[] = '0'.$mPhone->phone;
        }
        $this->phone_ext4 = implode('-', $aPhone);
    }
    public function getListCompany() {
        $aListdata = Borrow::model()->getListdataCompany();
        unset($aListdata[930409]);
        unset($aListdata[905906]);
        return $aListdata;
    }
    /** @Todo: Jan1019 check some role can view info phone of customer **/ 
    public function canViewMoreInfo() {
        $cRole          = MyFormat::getCurrentRoleId();
        $aRoleManage    = [ROLE_AUDIT, ROLE_BRANCH_DIRECTOR, ROLE_ADMIN, ROLE_HEAD_GAS_BO, ROLE_DIRECTOR_BUSSINESS, ROLE_ACCOUNTING, ROLE_EMPLOYEE_OF_LEGAL];
        if(!in_array($cRole, $aRoleManage) && !in_array($cRole, $this->ARR_ROLE_SALE_CREATE)){
            return false;
        }
        return true;
    }
    
    public function getInfoSmsPrice() {
        if(!$this->canViewMoreInfo()){
            return '';
        }
        $res = '';
        $this->loadExtPhone2();
        if(!empty($this->storehouse_id)){
//            $mAppCache = new AppCache();
//            $aCompany = $mAppCache->getListdataUserByRole(ROLE_COMPANY);// Close Apr1117 ẩn bớt thông tin
//            $res .= '<br><b>Cty:</b> '.(isset($aCompany[$this->storehouse_id]) ? $aCompany[$this->storehouse_id] : '' );
        }
        $email = $this->getUserRefField('contact_boss_mail');
        if(!empty($email)){
            $temp = explode(';', $email);
            $res .= '<br>'.  implode('<br>', $temp);
        }
        if(!empty($this->phone_ext2)){
            $res .= '<br><b>SMS giá:</b> '.$this->phone_ext2;
        }
        if(!empty($this->phone_ext3)){
            $res .= '<br><b>SMS bảo trì:</b> '.$this->phone_ext3;
        }
        if(!empty($this->phone_ext4)){
            $res .= '<br><b>SMS thu nợ:</b> '.$this->phone_ext4;
        }
        $fax = $this->getUserRefField('contact_boss_fax');
        if(!empty($fax)){
            $res .= '<br><b>Fax:</b> '.$fax;
        }
        return '<br>'.$res;
    }
    
    /**
     * @Author: DungNT Feb 28, 2017
     * @Todo: get path lưu file
     */
    public function getQuotesPathSaveFile() {
        $month = date('m'); $year = date('Y');
        if(!empty($this->date_month)){
            $month = $this->date_month;
        }else{
            $this->date_month = $month;
        }
        if(!empty($this->date_year)){
            $year = $this->date_year;
        }else{
            $this->date_year = $year;
        }

        $pathUpload = 'upload/pdf/quotes/'.$year.'/'.$month;
        
        $root = Yii::getPathOfAlias('webroot');
        $currentPath = $root . '/' . $pathUpload;
        if (!is_dir($currentPath)){
            $imageProcessing = new ImageProcessing();// check thư mục trước khi tạo
            $imageProcessing->createDirectoryByPath($pathUpload);
            chmod($currentPath, 0777);
        }
        return $pathUpload;
    }
    public function getQuotesName() {
        return 'BG_'.$this->date_month.'_'.$this->date_year.'_sj'.$this->id;
    }
    public function setQuotesPdfName() {
//        $path = Yii::getPathOfAlias('webroot') . "/" . $path . "/" . $file_name;
        $this->pdfPathAndName = Yii::getPathOfAlias('webroot') . '/' . $this->getQuotesPathSaveFile().'/'.$this->getQuotesName().'.pdf';
        if(file_exists($this->pdfPathAndName)){
            chmod($this->pdfPathAndName, 0777);
        }
    }
    
    public function getPdfContentQuotes($aData) {
        $mToPdf = new ToPdf();
        return $mToPdf->getPdfContentQuotes($aData, $this);
    }
    public function exportQuotesPdf() {
        try{
        $month = date('m'); $year = date('Y');
        $mAppCache = new AppCache();
        if(!empty($this->date_month)){
            $month = $this->date_month;
        }else{
            $this->date_month = $month;
        }
        if(!empty($this->date_year)){
            $year = $this->date_year;
        }else{
            $this->date_year = $year;
        }
        $aErrors=[];
        $gPriceInMonth  = UsersPrice::getPriceOfListCustomer($month, $year, []);
        if(!isset($gPriceInMonth[$this->id])){
            throw new Exception("Chưa setup giá tháng $month/$year cho Khách Hàng");
        }
        $aCompany           = $mAppCache->getUserByRole(ROLE_COMPANY);
        $this->setQuotesPdfName();
        if( ( $this->pdfOutput == 'F' && !file_exists($this->pdfPathAndName)) || 
                $this->pdfOutput == 'I'
            ){
            ToPdf::customerQuotes($this, $aCompany, $gPriceInMonth, $aErrors);
        }
        }catch (Exception $exc){
            throw new Exception($exc->getMessage());
        }
    }
    public function sendEmailQuotes() {// send from grid customer
        try{
        $month = date('m'); $year = date('Y');$aErrors=[];
        $gPriceInMonth  = UsersPrice::getPriceOfListCustomer($month, $year, []);
        if(!isset($gPriceInMonth[$this->id])){
            throw new Exception("Chưa setup giá tháng $month/$year cho Khách Hàng");
        }
        $mAppCache          = new AppCache();
        $aCompany           = $mAppCache->getUserByRole(ROLE_COMPANY);
        $this->setQuotesPdfName();
        $mToPdf = new ToPdf(); // Aug0818 luôn tạo file mới khi request để cập nhật lại change price
        $mToPdf->customerQuotes($this, $aCompany, $gPriceInMonth, $aErrors);
        GasScheduleEmail::makeQuotes([$this]);
        
        }catch (Exception $exc){
            throw new Exception($exc->getMessage());
        }
    }
    
    /** @Author: DungNT Mar 02, 2017
     * @Todo: Check user can send mail
     */
    public function canMailQuotes() {
        $cRole = MyFormat::getCurrentRoleId();
        $aRoleAllow = [ROLE_SALE, ROLE_SALE_ADMIN, ROLE_ADMIN, ROLE_DIRECTOR_BUSSINESS, ROLE_HEAD_GAS_BO];
        if(in_array($cRole, $aRoleAllow)){
            return true;
        }
        return false;
    }
    /** @Author: DungNT Now 01, 2017
     * @Todo: Check user can reset pass word customer
     */
    public function canResetPass() {
        $cRole = MyFormat::getCurrentRoleId();
        $aRoleAllow = [ROLE_TELESALE, ROLE_HEAD_GAS_BO, ROLE_ADMIN, ROLE_DIEU_PHOI, ROLE_SALE_ADMIN];
        if(in_array($cRole, $aRoleAllow)){
            return true;
        }
        return false;
    }
    public function getActionNext() {
        $cAction = strtolower(Yii::app()->controller->action->id);
        return urlencode(Yii::app()->createAbsoluteUrl("admin/gascustomer/$cAction"));
    }
    /** @Author: DungNT Mar 02, 2017 kiểm tra xem user có được gửi lại SMS giá cho KH không */
    public function canResendSms() {// chắc chỉ mở time ngắn rồi đóng lại chứ ko cho bấm send tùm lum
        $cRole = MyFormat::getCurrentRoleId();
        if($cRole == ROLE_ADMIN){
            return true;
        } 
        if($this->isNewRecord || $this->is_maintain != UsersExtend::STORE_CARD_FOR_QUOTE){
            return false;
        }
        $countSend = GasScheduleSms::countByUserAndType($this->id, GasScheduleSms::TYPE_QUOTES_BO_MOI, ['LimitMonth'=>date('m')]);
        if($countSend >= GasScheduleSms::MAX_SEND_QUOTES){
            return false;
        }
        if(!in_array($cRole, GasScheduleSms::getRoleResendSms())){
            return false;
        }
        return true;
    }
    public function doResendSms() {// Mar 02, 2017
        $criteria = new CDbCriteria();
        $criteria->addCondition('t.user_id=' . $this->id);
        $criteria->addCondition('t.type=' . UsersPhoneExt2::TYPE_PRICE_BO_MOI);
        $aModelPhone = UsersPhoneExt2::model()->findAll($criteria);
        $mGasScheduleSms = new GasScheduleSms();
        $mGasScheduleSms->doInsertMultiQuotes($aModelPhone);
    }
    // Apr 14, 17 set agent vào json trogn model User để có thể truy xuất nhanh trong 1 số trường hợp, thực chất vẫn là table OneMany là chính
    public function setListAgentOfUser($agent_id, $setMultiAgent) {
        $aAgent = Users::GetKeyInfo($this, UsersExtend::JSON_ARRAY_AGENT);
        if($setMultiAgent){
            $aAgent[$agent_id] = $agent_id;// có thể setmulti agent cho User trogn 1 so TH
        }else{
            $aAgent = [$agent_id => $agent_id];// đang set 1 thời điểm 1 user chỉ thuộc về 1 agent
        }
        Users::UpdateKeyInfo($this, UsersExtend::JSON_ARRAY_AGENT, $aAgent);
        
        $this->parent_id = $agent_id;// Sep1918 fix change luôn agent hiện tại của user (parent_id => Nơi công tác ), để các BC quỹ chạy đúng 
        $this->update(['parent_id']);
    }
    public function getVoPTTT() {
        $mAppCache          = new AppCache();
        $listdataMaterial   = $mAppCache->getListdata('GasMaterials', AppCache::LISTDATA_MATERIAL);
        $materials_id       = $this->getUserRefField("contact_technical_phone");
        return isset($listdataMaterial[$materials_id]) ? $listdataMaterial[$materials_id] : $materials_id;
    }
    
    /*** Jun2517 Sinh username cho customer bò mối*/
    public function setUsernameCustomer(){
        if(!in_array($this->is_maintain, CmsFormatter::$aTypeIdMakeUsername)){
            return '';
        }
        $this->makePassword();// cho kèm luôn tạo pass cho user ở đây
        $prefix_code = 'sj'; $length_max_id = 6;
        $prefix_code_length = strlen($prefix_code);
        $criteria = new CDbCriteria;
        $criteria->select='MAX(CONVERT(SUBSTR(t.username,'.($prefix_code_length+1).'),SIGNED)) as MAX_ID';
        $criteria->addCondition('t.role_id=' . ROLE_CUSTOMER);
        $criteria->addCondition('t.type=' . CUSTOMER_TYPE_STORE_CARD);
        $criteria->addInCondition('t.is_maintain', CmsFormatter::$aTypeIdMakeUsername); // Aug 19, 2014 chỉ update KH bò mối thôi, vì cái 
//        $criteria->compare("t.$fieldName",$prefix_code,true);
        $model = Users::model()->find($criteria);
        $max_id =  (null == $model->MAX_ID) ? 0 : $model->MAX_ID;
        $max_id++;
        if($max_id > 9999){
            $length_max_id = 7;
        }
        $addition_zero_num 	= $length_max_id - strlen($max_id) - strlen($prefix_code);
        $code = $prefix_code;
        for($i=1;$i<=$addition_zero_num;$i++){
            $code.='0';
        }
        $code.= $max_id;
        return $this->username = $code;
    }
    public function makeRecordChangePass(){// belong to setUsernameCustomer, tạo mới user thì check change pass luôn
        if(!in_array($this->is_maintain, CmsFormatter::$aTypeIdMakeUsername)){
            return '';
        }
        $mMonitorUpdate = new MonitorUpdate();
        $mMonitorUpdate->makeRecordChangePass($this);
        UsersTokens::deleteByUser($this->id);
    }
    /*** @Author: DungNT Jun 25, 2017
     * @Todo: validate change pass
     */
    public function validateChangePass($old_password, $new_password, $new_password_confirm) {
        $error = '';
        $new_password = trim($new_password);
        $new_password_confirm = trim($new_password_confirm);
        if($this->password_hash != md5($old_password)){
            $error = 'Mật khẩu cũ không đúng';
        }elseif($new_password != $new_password_confirm){
            $error = 'Xác nhận mật khẩu mới không đúng';
        }elseif(strlen($new_password) < PASSW_LENGTH_MIN){
            $error = 'Mật khẩu mới phải có 6 ký tự trở lên';
        }elseif(in_array($new_password, GasConst::getSimplePassword())){
            $error = 'Mật khẩu mới quá đơn giản, vui lòng chọn mật khẩu khác';
        }elseif($new_password == $this->username){
            $error = 'Mật khẩu mới không được trùng tên tài khoản';
        }
        if(!empty($error)){
            $this->addError('password_hash', $error);
        }
        return $error;
    }
    public function getUsernameView() {
        $cRole = MyFormat::getCurrentRoleId();
        $cUid = MyFormat::getCurrentUid();
        if(in_array($cRole, $this->ARR_ROLE_SALE_LIMIT) && $this->sale_id != $cUid){
            return '';
        }
        $res = $this->username;
        $cRole = MyFormat::getCurrentRoleId();
        if($this->apiCheckChangePass() && ($cRole != ROLE_DIEU_PHOI || in_array($cUid, GasTickets::$UID_DIEU_PHOI_HCM_CUSTOMER)) ){
            $res .= '<br><b>'.$this->temp_password.'</b>';
        }
        return $res;
    }
    /** @Author: DungNT Jun 28, 2017
     * @Todo: handle save tạo chuỗi KH đặt app
     */
    public function setChainAccountApp() {
        if(isset($_POST['GasOneMany']['list_customer_id'])){
            GasOneMany::saveManyId($this->id, GasOneMany::TYPE_CUSTOMER_ACCOUNT_APP, 'GasOneMany', 'list_customer_id');
        }
    }
    public function getChainAccountApp() {
        $aCustomerId = GasOneMany::getArrOfManyId($this->id, GasOneMany::TYPE_CUSTOMER_ACCOUNT_APP);
        if(count($aCustomerId) < 1){
            return [];
        }
        return Users::getSelectByRoleFinal(ROLE_CUSTOMER, ['aId'=>$aCustomerId]);
    }
    /** @Author: DungNT Aug 13, 2017
     * @Todo: get agent của NV Giao nhận hoặc tài xế
     */
    public function getAgentOfEmployee() {
        return $this->parent_id;// Feb1419 DungNT change, do thay phan kiem tra ben duoi ko can thiet nua
        $aRoleAllow = [ROLE_EMPLOYEE_MAINTAIN, ROLE_DRIVER, ROLE_DEBT_COLLECTION];
        if(!in_array($this->role_id, $aRoleAllow)){
            return 0;
        }
        $aRoleLikeDriver = [ROLE_DRIVER, ROLE_DEBT_COLLECTION];
        if(in_array($this->role_id, $aRoleLikeDriver)){
            return $this->parent_id;
        }
        $aAgent     = Users::GetKeyInfo($this, UsersExtend::JSON_ARRAY_AGENT);
        $agent_id   = reset($aAgent);
        return $agent_id;
    }
    
    /** @Author: DungNT Aug 20, 2017
     * @Todo: some validate before save
     */
    public function validateByHand() {
        $cUid       = MyFormat::getCurrentUid();
        $aUserHRM   = [GasConst::UID_ADMIN, GasConst::UID_CHAU_LNM, GasConst::UID_HANH_NT];// user quan ly nhan su khong validate 
        $this->phone = UsersPhone::removeWhiteSpace($this->phone);
        if(!empty($this->phone)){
            $temp = explode('-', $this->phone);
            foreach($temp as $phoneNumber){
                if(!UsersPhone::isValidCellPhone($phoneNumber)){
                    $this->addError('phone', "Số điện thoại $phoneNumber không phải là số di động hợp lệ");
                }
            }
        }
        if(!empty($this->mProfile->list_email)){
            $temp = explode(';', $this->mProfile->list_email);
            foreach($temp as $email){
                $mUser = new Users('create_customer_store_card');
                $mUser->email = $email;
                $mUser->validate(['email']);
                if($mUser->hasErrors('email')){
                    $this->mProfile->addError('list_email', "Email $email không hợp lệ");
                }
            }
        }
        if($this->status == STATUS_INACTIVE && empty($this->mProfile->leave_date)){
            $this->mProfile->addError('leave_date', 'Chưa chọn ngày nghỉ việc');
        }
        if($this->mProfile->salary_method == UsersProfile::SALARY_METHOD_VIETTEL && empty($this->mProfile->salary_method_phone)){
            $this->mProfile->addError('salary_method_phone', 'Chưa nhập số điện thoại Viettel Pay');
        }
        if(!in_array($cUid, $aUserHRM) && $this->mProfile->salary_method == UsersProfile::SALARY_METHOD_VIETTEL && empty($this->mProfile->salary_method_account)){
            $this->mProfile->addError('salary_method_account', 'Chưa nhập số thẻ Viettel Pay');
        }
        if($this->mProfile->salary_method == UsersProfile::SALARY_METHOD_BANK && empty($this->mProfile->bank_no)){
            $this->mProfile->addError('bank_no', 'Chưa nhập số tài khoản ngân hàng');
        }
    }
    
    /** @Author: DungNT Aug2017
     *  @Todo: save info to some model relate
        * 1. gửi mail tài khoản cho nhân viên -- done
        * 2. set change pass nếu chưa có pass -- done
        * 3. mail cho every one khi có nhân viên nghỉ việc -- done
        * 4. gắn user vào đại lý: giao nhận + KTBH + tài xế -- done
        * 5. làm các quan hệ liên quan: vợ/chồng, con cái, anh em, bố mẹ
        * 6. xóa user PVKH lock khỏi agent $this->mProfile->removeLockUserPvkhOfAgent();
     **/
    public function saveEmployee() {
        if(in_array($this->role_id, Roles::$aRoleRestrict)){
            return ;
        }
        $this->saveEmployeeSetInfo();
        $isNewRecord = $this->isNewRecord;
        $isNewRecord ? $this->save() : $this->update();
        $this->saveProfile();
        if($isNewRecord){
            $this->setRequiredChangePass();
            GasScheduleEmail::memberNew($this);
        }else{
            if($this->newpassword){// is set default pass
                $this->setRequiredChangePass();
                GasScheduleEmail::memberNew($this);
            }
        }
        
        if($this->oldStatus != $this->status && $this->status == STATUS_INACTIVE){
            UsersTokens::deleteByUser($this->id);
            GasScheduleEmail::memberLeave($this);
        }
        if($isNewRecord){// DungNT Aug0719 chỉ xử lý chỗ này khi tạo new record do bị conflick với hàm $this->mProfile->removeLockUserPvkhOfAgent();
            GasOneMany::changeAgentEmployee($this);
        }
        UsersPhone::savePhone($this);// May 25, 2016
    }
    public function saveEmployeeSetInfo() {
        if($this->isNewRecord){
            $this->temp_password       = GasConst::DEFAULT_PASS;
        }else{
            if($this->newpassword){// is set default pass
                $this->temp_password       = GasConst::DEFAULT_PASS;
            }
        }
        $this->password_hash       = md5($this->temp_password);
        if(strpos($this->last_purchase, '/')){
            $this->last_purchase = MyFormat::dateConverDmyToYmd($this->last_purchase);
            MyFormat::isValidDate($this->last_purchase);
        }
    }
    public function saveProfile() {// DungNT save model UsersProfile
        if(empty($this->id)){
            throw new Exception('Tạo nhân viên không thành công, vui lòng thử lại. User001');
        }
        $this->mProfile->user_id = $this->id;
        $this->mProfile->formatDataSave();
        
        if($this->mProfile->isNewRecord){
           $this->mProfile->save(); 
        }else{
            $this->mProfile->update();
            $this->mProfile->removeLockUserPvkhOfAgent();
        }
    }
    public function loadProfileUpdate() {
        if($this->rUsersProfile){
            $this->mProfile = $this->rUsersProfile;
            $this->mProfile->scenario = 'employees_update';
        }else{
            $this->mProfile = new UsersProfile('employees_create');
        }
        $this->mProfile->mUser = $this;
        $this->mProfile->formatDataView();
    }
    public function loadEmployeeUpdate() {
        if(!empty($this->last_purchase)){
            $this->last_purchase = MyFormat::dateConverYmdToDmy($this->last_purchase);
        }
        $this->loadProfileUpdate();
    }
    public function setRequiredChangePass() {
        $mMonitorUpdate = new MonitorUpdate();
        $mMonitorUpdate->makeRecordChangePass($this);
        UsersTokens::deleteByUser($this->id);
    }
    public function canUpdateEmployee() {
        if(in_array($this->role_id, Roles::$aRoleRestrict)){
            return false;
        }
        $cRole = MyFormat::getCurrentRoleId();
        if($cRole == ROLE_ADMIN){
            return true;
        }
        return true;
    }
    public function getFamily($needMore=[]) {
        if($this->role_id != ROLE_FAMILY){
            return '';
        }
        $aFamily = GasConst::getArrFamily();
        $br = isset($needMore['br']) ? $needMore['br'] : '';
        return isset($aFamily[$this->type]) ? $aFamily[$this->type].$br : '';
    }
    public function getGender() {
        return isset(Users::$aGender[$this->first_char]) ? Users::$aGender[$this->first_char] : '';
    }
    public function getProfileDetail() {
        $res = '';
        $codeGas24h = $this->getGas24hCode();
        if(!empty($codeGas24h)){
            $res .= '<br><b>CODE:</b> '.$codeGas24h;
        }
        if(!empty($this->username)){
            $res .= '<br><b>Tài khoản:</b> '.$this->username;
        }
        if(!empty($this->phone)){
            $res .= '<br><b>Phone:</b> '.$this->phone;
        }
        if(!empty($this->email)){
            $res .= '<br><b>Email:</b> '.$this->email;
        }
        if(!empty($this->getGender())){
            $res .= '<br><b>Giới tính:</b> '.$this->getGender();
        }
        $family = $this->getFamily();
        if(!empty($family)){
            $res .= '<br><b>Quan hệ:</b> '.$family;
        }
//        if($this->status == STATUS_INACTIVE){
            $mUsersProfile = $this->rUsersProfile;
            if( $mUsersProfile && !empty($mUsersProfile->getLeaveDate())){
                $res .= '<br><b>Ngày nghỉ việc:</b> '.$mUsersProfile->getLeaveDate();
            }
            $res .= '<br><b>Trả lương:</b> '.$this->getPaySalary();
            if( $mUsersProfile ){
                $res .= '<br><b>Hình thức:</b> '.$mUsersProfile->getSalaryMethod();
            }
            if( $mUsersProfile ){
                $res .= '<br><b>Hồ sơ:</b> '.$mUsersProfile->getProfileStatus();
            }
//        }
        return $res;
    }
    public function getRoleName(){
        $session=Yii::app()->session;
        if(!isset($session['ROLE_NAME_USER']) || !isset($session['ROLE_NAME_USER'][$this->role_id]))
                $session['ROLE_NAME_USER'] = Roles::getArrRoleName();
        return $session['ROLE_NAME_USER'][$this->role_id];
    }
    public function getPaySalary() {
        return isset(CmsFormatter::$yesNoFormat[$this->payment_day]) ? CmsFormatter::$yesNoFormat[$this->payment_day] : '';
    }
    public function getCompany() {
        if(empty($this->storehouse_id)){
            return '';
        }
        $aCompany = Borrow::model()->getListdataCompany();
        return isset($aCompany[$this->storehouse_id]) ? $aCompany[$this->storehouse_id] : '';
    }
    public function getDateBeginJob() {
        $mUsersProfile = $this->rUsersProfile;
        if($mUsersProfile){
            return $mUsersProfile->getDateBeginJob();
        }
        return '';
    }
    public function getFunctionProfile($functionName) {
        $mUsersProfile = $this->rUsersProfile;
        if($mUsersProfile){
            return $mUsersProfile->{$functionName}();
        }
        return '';
    }
    // Dec1417 get log reset pass KH bò mối
    public function getEventLog() {
        $mTransactionEvent = new TransactionEvent();
        $mTransactionEvent->transaction_history_id = $this->id;
        $mTransactionEvent->type = TransactionEvent::TYPE_RESET_PASS_CUSTOMER;
        return $mTransactionEvent->getToday();
    }
    // Dec1417 update to solr search
    public function solrAdd() {
        $aRoleSolr = [ROLE_CUSTOMER, ROLE_AGENT];
        if( !in_array($this->role_id, $aRoleSolr) ){
//            return ; DungNT Aug3019 close -- will sync all to Solr 
        }
        $mSolrUpdate = new SolrUpdate();
        return $mSolrUpdate->add(SolrUpdate::ACTION_ADD, $this->id);
    }
    public function solrDelete() {
        $mSolrUpdate = new SolrUpdate();
        return $mSolrUpdate->add(SolrUpdate::ACTION_DELETE, $this->id);
    }

    /***************** FEB0618 ZONE NAM CODE *****************/
    
    /** @Author: HOANG NAM 01/02/2018
     *  @Todo: search > 15 day
     *  @Code: NAM006
     *  @Param: 
     **/
    public function searchBuyLarger15(){
        $aData=[];
        $criteria   = new CDbCriteria;
        $today      = date('Y-m-d');
        $date_to    = MyFormat::modifyDays($today, 15, '-');
        $criteria->addCondition("t.last_purchase <= '$date_to'");
        
        self::AddCriteriaSame($this, $criteria);
        $this->addProvinceCriteria($this,$criteria);
        $aData  = Users::model()->findAll($criteria);
        if(count($aData)>0):
            $mInventoryCustomer = new InventoryCustomer();
            $mInventoryCustomer->date_from      = date('d-m-Y');
            $mInventoryCustomer->date_to        = date('d-m-Y');
            $sCustomer = $this->getStringIdCustomer($aData);
            $aCustomerCountVo   = $mInventoryCustomer->getListCustomerCountVo($sCustomer);
            foreach ($aData as $keyCustommer => $mUsers) {
                $mUsers->count_vo = isset($aCustomerCountVo[$mUsers->id])?$aCustomerCountVo[$mUsers->id] : 0;
                if($mUsers->count_vo < $this->countVo):
                    unset($aData[$keyCustommer]);
                endif;
            }
            usort($aData, function ($item1, $item2) {
                return $item2->count_vo >= $item1->count_vo;
            });
        endif;
        return $aData;
    }
    /** @Author: HOANG NAM 01/02/2018
     *  @Todo: add province criteria
     *  @Code: NAM006
     **/
    public function addProvinceCriteria($model,&$criteria){
        $aAgentId       = $this->getAgentOfProvince($model->province_id);
        $sAgentID       = implode(',', $aAgentId);
        $this->limitFieldName($criteria, $sAgentID, 'area_code_id');
    }
    /** @Author: HOANG NAM 01/02/2018
     *  @Todo: get aProvince id
     *  @Code: NAM006
     **/
    public function getAgentOfProvince($aProvince) {
        if(!is_array($aProvince)){
            return [];
        }
        $aRes       = [];
        $mAppCache  = new AppCache();
        $aAgent     = $mAppCache->getAgent();
        foreach($aAgent as $item){
            if(in_array($item['province_id'], $aProvince)){
                $aRes[] = $item['id'];
            }
        }
        return $aRes;
    }
    /** @Author: HOANG NAM 01/02/2018
     *  @Todo: limit field name
     *  @Code: NAM006
     **/
    public function limitFieldName(&$criteria, $sParamsIn, $fieldName) {
        if(!empty($sParamsIn)){
            $criteria->addCondition("t.$fieldName IN ($sParamsIn)");
        }
    }
    /** @Author: HOANG NAM 01/02/2018
     *  @Todo: get string idCustommer
     *  @Code: NAM006
     **/
    public function getStringIdCustomer($aCustomer){
        $Res =[];
        foreach ($aCustomer as $key => $value):
            $Res[] = $value->id;
        endforeach;
        return implode(',', $Res);
    }
    /** @Author: HOANG NAM 01/02/2018
     *  @Code: NAM006
     **/
    public function getAgent(){
        return $this->by_agent?$this->by_agent->first_name:"";
    }
    public function getPaymentDayCustomer(){
        return $this->payment?$this->payment->name:"";
    }
    public function getCountVoCustomer(){
        return $this->count_vo;
    }
    
    /***************** FEB0618 ZONE NAM CODE *****************/
    /** @Author: DungNT May 19, 2019
     *  @Todo: get type agent
     **/
    public function getTypeAgent(){
        $aData = $this->getArrayTypeAgent();
        return isset($aData[$this->gender]) ? $aData[$this->gender] : '';
    }
    
    
}