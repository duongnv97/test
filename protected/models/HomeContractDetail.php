<?php
/** @Author: HOANG NAM 12/01/2018 Task: NAM002
 *  @Todo: Quản lý chi tiet hợp đồng thuê nhà
 *  @Param: 
 **/
class HomeContractDetail extends BaseSpj
{

    public $debug = false;
    public $date_from, $date_to, $uid, $uid_leader, $autocomplete_agent;
    const STATUS_NOT_PAY    = 0;
    const STATUS_PAY        = 1;
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }
    
    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{_home_contract_detail}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        return array(
            array('status, amount, date_pay', 'safe'),
        );
    }
    
    public function relations()
    {
        return array(
            'rAgent' => array(self::BELONGS_TO, 'Users', 'agent_id'),
            'rHomeContract' => array(self::BELONGS_TO, 'HomeContract', 'home_contract_id'),
        );
    }
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'home_contract_id' => 'Hợp đồng thuê nhà',
            'agent_id' => 'Đại lý',
            'province_id' => 'Tỉnh',
            'amount'=>'Tiền thanh toán',
            'settle_id'=> 'Settle',
            'date_pay'=> 'Ngày thanh toán',
            'created_date'=>'Ngày tạo',
            'status'=>'Trạng thái',
        );
    }
    
    public function search()
    {
        $criteria=new CDbCriteria;
        if(!empty($this->agent_id)){
            $criteria->addCondition("t.agent_id = ".$this->agent_id);
        }
        if(!empty($this->date_pay['start']) && !empty($this->date_pay['end'])){
            $startOfStartContract = MyFormat::dateConverDmyToYmd($this->date_pay['start'],'-');
            $endOfStartContract = MyFormat::dateConverDmyToYmd($this->date_pay['end'],'-');
            $criteria->addCondition("t.date_pay <= '$endOfStartContract' AND t.date_pay >= '$startOfStartContract'" );
        }
        $criteria->order = 't.id DESC';
        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
        ));
    }
    /** @Author: HOANG NAM 12/01/2018
     **/
    public function beforeSave(){
        $this->date_pay = MyFormat::dateConverDmyToYmd($this->date_pay, '-');
        return parent::beforeSave();
    }
    public function getAgent(){
        $aAgent = $this->rAgent;
        return isset($aAgent) ? $aAgent->first_name : '';
    }
    public function getAmount($format = true){
        if ($format) {
            return ActiveRecord::formatCurrency($this->amount);
        }
        return $this->amount;
    }
    public function getDatePay(){
        return MyFormat::dateConverYmdToDmy($this->date_pay, 'd/m/Y');
    }
    public function getProvince(){
        $mAppCache          = new AppCache();
        $listdataProvince   = $mAppCache->getListdata('GasProvince', AppCache::LISTDATA_PROVINCE);
        return isset($listdataProvince[$this->province_id]) ? $listdataProvince[$this->province_id] : '';
    }
    public function getSettle(){
        return $this->settle_id;
    }
    public function getHomeContract(){
        $aHomeContract = $this->rHomeContract;
        return $aHomeContract;
    }
    
    /** @Author: HOANG NAM 12/01/2018
     *  @Todo: tự động thêm dữ liệu theo khoản thời gian truyền vào.
     *  @Param: $startDate,$endDate ngày bắt đầu, ngày kết thúc. Nếu không có thì đầu tháng - cuối tháng hiện tại
     **/
    public function windowInsert($startDate = null,$endDate = null){
        $aProvinceOfZone = [];
        $aZoneProvinceId =  GasOrders::getZoneNewProvinceId();
        foreach ($aZoneProvinceId as $zone_id => $aProvince) {
            foreach($aProvince as $province_id){
                $aProvinceOfZone[$province_id] = $zone_id;
            }
        }
        // DuongNV Jan 30,19 gom theo type_pay Đề Nghị Chuyển Khoản và Đề Nghị Thanh Toán
        $aHomeContractGenerate  =   $this->getListHomeContract($startDate, $endDate);
//        $aValueInsert =  $this->getListHomeContractDetail($aProvinceOfZone,$aHomeContractGenerate);
//        $this->insertHomeContractDetailAndSettle($aValueInsert);
        $aValueInsert =  $this->getListContractDetailByType($aProvinceOfZone,$aHomeContractGenerate);
        $this->insertHomeContractDetailAndSettleByType($aValueInsert);
    }
    
    /** @Author: HOANG NAM 15/01/2018
     *  @Todo: auto generate Settle for home contract detail
     *  @Param: $idZone is province_id
     **/
    public function generateSettle($uid_login, $uid_leader, $type, $amount, $idZone, $aProvince){
        $mSettle = new GasSettle();
        $mSettle->uid_login     = $uid_login;
        $mSettle->uid_leader    = $uid_leader;
        $mSettle->type          = $type;
        $mSettle->amount        = $amount;
        $mSettle->reason        = MyFormat::dateConverYmdToDmy($this->date_from)." TTTN ". (isset($aProvince[$idZone]) ? $aProvince[$idZone] : '' );
        $mSettle->spj_auto      = GasSettle::SPJ_AUTO_GEN;
        $mSettle->save();
        return $mSettle;
    }
    /** @Author: HOANG NAM 15/01/2018
     *  @Todo:  set Uid
     *  @Param: 
     **/
    public function setUidLogin($uid){
        $this->uid = $uid;
    }
    /** @Author: HOANG NAM 15/01/2018
     *  @Todo: set uid_leader
     *  @Param: 
     **/
    public function setUidLeader($uid_leader){
        $this->uid_leader = $uid_leader;
    }
    /** @Author: HOANG NAM 16/01/2018
     *  @Todo: get aHomeContract
     *  @Code: 
     **/
    public function getListHomeContract($startDate, $endDate) {
        $mHomeContract  = new HomeContract;
        if($startDate   ==  null){
            $startDate  =   date('Y-m-01');
        }
        if($endDate==null){
            $endDate    =   date('Y-m-t');
        }
        $this->date_from    = $startDate;
        $this->date_to      = $endDate;
        
        $criteria=new CDbCriteria;
//        lấy danh sách còn thời gian hoạt động
        $status         =   $mHomeContract->Active_Status;
        $criteria->addCondition("t.status = ".$status);
//        thời gian hết hạn hợp đồng lớn hơn thời gian bắt đầu 
        $criteria->addCondition("t.end_contract >= '$startDate'");
//        thời gian thanh toán tiếp nằm giữa khoản time
        $criteria->addCondition("t.next_date_pay BETWEEN '$startDate' AND '$endDate'");
        $criteria->order = 't.province_id ASC';
        return HomeContract::model()->findAll($criteria);
    }
    /** @Author: HOANG NAM 16/01/2018
     *  @Todo: get List home contract Detail 
     *  @Code: NAM003
     *  @Param: 
     **/
    public function getListHomeContractDetail($aProvinceOfZone,$aHomeContract) {
        $aValueInsert = [];
        foreach ($aHomeContract as $valHomeContract) {
            $zoneId         = isset($aProvinceOfZone[$valHomeContract->province_id]) ? $aProvinceOfZone[$valHomeContract->province_id] : $valHomeContract->province_id;
            $province_id    = $valHomeContract->province_id;// Aug2818 chuyển sang làm theo tỉnh
            $datePay        = $valHomeContract->next_date_pay;
            $datePayCurent        = $valHomeContract->next_date_pay;
            $amount         = 0;
            $aPriceList     = $valHomeContract->getArrayAmountContract();
//            set count month
            $countMonthPay = $valHomeContract->amount_month_pay;
            foreach ($aPriceList as $valuePrice) {
                if($datePayCurent >= MyFormat::dateConverDmyToYmd($valuePrice['start'],'-') &&  $datePayCurent <= MyFormat::dateConverDmyToYmd($valuePrice['end'],'-')){
                    $countDay               = (int)MyFormat::getNumberOfDayBetweenTwoDate($datePayCurent,MyFormat::dateConverDmyToYmd($valuePrice['end'],'-'));
                    $countMonth             = (int)($countDay / 30) > 0 ? (int)($countDay / 30): 1;
                    $countMonthPayCurrent   = $countMonth > $countMonthPay ? $countMonthPay : $countMonth;
                    $countMonthPay          -= $countMonthPayCurrent;
                    $datePayCurent          = MyFormat::addDays($datePayCurent, $countMonthPayCurrent, '+', 'month');
//                    Ngày thanh toán 7/11
//                    26/11->26/12 : 3.000.000
//                    26/12->26/02 : 3.500.000
//                    Thanh toan 3 thang => 2 thang 3.000.000, 1 thang 3.500.000
                    while($datePayCurent <= MyFormat::dateConverDmyToYmd($valuePrice['end'],'-') && $countMonthPay > 0){
                        $datePayCurent          = MyFormat::addDays($datePayCurent, 1, '+', 'month');
                        $countMonthPayCurrent   += 1;
                        $countMonthPay          -= 1;
                    }
                    $amount                 += $valuePrice['amount'] * $countMonthPayCurrent;
                }
                if($countMonthPay <= 0){
                    break;
                }
            }
            if($amount < 1){
                $info = $valHomeContract->getAgent().' - '." Không có giá thuê nhà setup trong khoảng ngày $datePay. Model - getListHomeContractDetail() ";
                Logger::WriteLog($info);
                $needMore['title']      = 'Chưa setup giá thuê nhà';
                $needMore['list_mail']  = ['dungnt@spj.vn', 'phaply@spj.vn'];
                SendEmail::bugToDev($info, $needMore);
                continue;
            }
            $zoneId = $province_id;// Fix Aug2818
            $aValueInsert[$zoneId][$valHomeContract->type_pay][$valHomeContract->id]['home_contract_id']    = $valHomeContract->id;
            $aValueInsert[$zoneId][$valHomeContract->type_pay][$valHomeContract->id]['agent_id']            = $valHomeContract->agent_id;
            $aValueInsert[$zoneId][$valHomeContract->type_pay][$valHomeContract->id]['province_id']         = $valHomeContract->province_id;
            $aValueInsert[$zoneId][$valHomeContract->type_pay][$valHomeContract->id]['amount']              = $amount;
            $aValueInsert[$zoneId][$valHomeContract->type_pay][$valHomeContract->id]['date_pay']            = $datePay;
            $aValueInsert[$zoneId][$valHomeContract->type_pay][$valHomeContract->id]['amount_month_pay']    = $valHomeContract->amount_month_pay;
            $Create_date       = date('Y-m-d H:i:s');
            $aValueInsert[$zoneId][$valHomeContract->type_pay][$valHomeContract->id]['created_date']        = $Create_date;
            if(!$this->debug){
                $valHomeContract->next_date_pay = MyFormat::modifyDays($valHomeContract->next_date_pay, $valHomeContract->amount_month_pay,'+','months');
//              update home contract
                $valHomeContract->scenario = 'abcxyz';
                $valHomeContract->update();
            }
        }
        return $aValueInsert;
    }
    /** @Author: HOANG NAM 16/01/2018
     *  @Todo: insert to database
     *  @Code: NAM003
     *  @Param: 
     **/
    public function insertHomeContractDetailAndSettle($aValueInsert) {
        $mAppCache = new AppCache();
        $aProvince = $mAppCache->getListdata('GasProvince', AppCache::LISTDATA_PROVINCE);
        foreach ($aValueInsert as $idZone => $aType) {
            $uid_login  = $this->getUserAutoCreate($idZone);
            $uid_leader = $this->uid_leader;
            foreach ($aType as $type => $aHomeContract) {
                $amount = 0;
                $mSettle    = $this->generateSettle($uid_login, $uid_leader, $type, $amount, $idZone, $aProvince);
                foreach ($aHomeContract as $idHomeContract => $HomeContract) {
                    $amount +=$HomeContract['amount'];
                    $mHomeContractDetail = new HomeContractDetail();
                    $mHomeContractDetail->home_contract_id  =  $HomeContract['home_contract_id'];
                    $mHomeContractDetail->agent_id          =  $HomeContract['agent_id'];
                    $mHomeContractDetail->province_id       =  $HomeContract['province_id'];
                    $mHomeContractDetail->amount            =  $HomeContract['amount'];
                    $mHomeContractDetail->date_pay          =  MyFormat::dateConverDmyToYmd($HomeContract['date_pay'],'-');
                    $mHomeContractDetail->amount_month_pay  =  $HomeContract['amount_month_pay'];
                    $mHomeContractDetail->created_date      =  $HomeContract['created_date'];
                    $mHomeContractDetail->settle_id         =  $mSettle->id;
                    $mHomeContractDetail->save();
                }
                $mSettle->amount    = $amount;
                $mSettle->update();
            }
        }
    }
    
    
    /** @Author: DuongNV Jan 30,2018
     *  @Todo: get List home contract Detail (Lấy theo type_pay là Đề nghị chuyển khoản và đề nghị thanh toán)
     **/
    public function getListContractDetailByType($aProvinceOfZone,$aHomeContract) {
        $aValueInsert = [];
        foreach ($aHomeContract as $valHomeContract) {
            $zoneId         = isset($aProvinceOfZone[$valHomeContract->province_id]) ? $aProvinceOfZone[$valHomeContract->province_id] : $valHomeContract->province_id;
            $province_id    = $valHomeContract->province_id;// Aug2818 chuyển sang làm theo tỉnh
            $datePay        = $valHomeContract->next_date_pay;
            $datePayCurent        = $valHomeContract->next_date_pay;
            $amount         = 0;
            $aPriceList     = $valHomeContract->getArrayAmountContract();
//            set count month
            $countMonthPay = $valHomeContract->amount_month_pay;
            foreach ($aPriceList as $valuePrice) {
                if($datePayCurent >= MyFormat::dateConverDmyToYmd($valuePrice['start'],'-') &&  $datePayCurent < MyFormat::dateConverDmyToYmd($valuePrice['end'],'-')){
                    $countDay               = (int)MyFormat::getNumberOfDayBetweenTwoDate($datePayCurent,MyFormat::dateConverDmyToYmd($valuePrice['end'],'-'));
                    $countMonth             = (int)($countDay / 30) > 0 ? (int)($countDay / 30): 1;
                    $countMonthPayCurrent   = $countMonth > $countMonthPay ? $countMonthPay : $countMonth;
                    $countMonthPay          -= $countMonthPayCurrent;
                    $datePayCurent          = MyFormat::addDays($datePayCurent, $countMonthPayCurrent, '+', 'month');
//                    Ngày thanh toán 7/11
//                    26/11->26/12 : 3.000.000
//                    26/12->26/02 : 3.500.000
//                    Thanh toan 3 thang => 2 thang 3.000.000, 1 thang 3.500.000
                    while($datePayCurent <= MyFormat::dateConverDmyToYmd($valuePrice['end'],'-') && $countMonthPay > 0){
                        $datePayCurent          = MyFormat::addDays($datePayCurent, 1, '+', 'month');
                        $countMonthPayCurrent   += 1;
                        $countMonthPay          -= 1;
                    }
                    $amount                 += $valuePrice['amount'] * $countMonthPayCurrent;
                }
                if($countMonthPay <= 0){
                    break;
                }
            }
            if($amount < 1){
                $info = $valHomeContract->getAgent().' - '." Không có giá thuê nhà setup trong khoảng ngày $datePay. Model - getListHomeContractDetail() ";
                Logger::WriteLog($info);
                $needMore['title']      = 'Chưa setup giá thuê nhà';
                $needMore['list_mail']  = ['dungnt@spj.vn', 'phaply@spj.vn'];
                SendEmail::bugToDev($info, $needMore);
                continue;
            }
            $aTypePay = HomeContract::model()->getArrayTypePay();
            // type_pay là Đề Nghị Chuyển Khoản và Đề Nghị Thanh Toán
            if(in_array($valHomeContract->type_pay, array_keys($aTypePay))){
                $aValueInsert[$valHomeContract->type_pay][$valHomeContract->id]['home_contract_id']    = $valHomeContract->id;
                $aValueInsert[$valHomeContract->type_pay][$valHomeContract->id]['agent_id']            = $valHomeContract->agent_id;
                $aValueInsert[$valHomeContract->type_pay][$valHomeContract->id]['province_id']         = $valHomeContract->province_id;
                $aValueInsert[$valHomeContract->type_pay][$valHomeContract->id]['amount']              = $amount;
                $aValueInsert[$valHomeContract->type_pay][$valHomeContract->id]['date_pay']            = $datePay;
                $aValueInsert[$valHomeContract->type_pay][$valHomeContract->id]['amount_month_pay']    = $valHomeContract->amount_month_pay;
                $Create_date       = date('Y-m-d H:i:s');
                $aValueInsert[$valHomeContract->type_pay][$valHomeContract->id]['created_date']        = $Create_date;
                if(!$this->debug){
                    $valHomeContract->next_date_pay = MyFormat::modifyDays($valHomeContract->next_date_pay, $valHomeContract->amount_month_pay,'+','months');
    //              update home contract
                    $valHomeContract->scenario = 'abcxyz';
                    $valHomeContract->update();
                }
            }
        }
        return $aValueInsert;
    }
    
    /** @Author: DuongNV Jan 30,19
     *  @Todo: insert to database
     **/
    public function insertHomeContractDetailAndSettleByType($aValueInsert) {
        $uid_login  = $this->getUserAutoCreate('');
        $uid_leader = $this->uid_leader;
        foreach ($aValueInsert as $type => $aHomeContract) {
            $amount = 0;
            $mSettle    = $this->generateSettle($uid_login, $uid_leader, $type, $amount, "", []);
            foreach ($aHomeContract as $HomeContract) {
                $amount +=$HomeContract['amount'];
                $mHomeContractDetail = new HomeContractDetail();
                $mHomeContractDetail->home_contract_id  =  $HomeContract['home_contract_id'];
                $mHomeContractDetail->agent_id          =  $HomeContract['agent_id'];
                $mHomeContractDetail->province_id       =  $HomeContract['province_id'];
                $mHomeContractDetail->amount            =  $HomeContract['amount'];
                $mHomeContractDetail->date_pay          =  MyFormat::dateConverDmyToYmd($HomeContract['date_pay'],'-');
                $mHomeContractDetail->amount_month_pay  =  $HomeContract['amount_month_pay'];
                $mHomeContractDetail->created_date      =  $HomeContract['created_date'];
                $mHomeContractDetail->settle_id         =  $mSettle->id;
                $mHomeContractDetail->save();
            }
            $mSettle->amount    = $amount;
            $mSettle->update();
        }
    }
    
    /** @Author: ANH DUNG May 28, 2018
     *  @Todo: get user kế toán theo vùng
     **/
    public function getUserAutoCreate($idZone) {
        return GasConst::UID_NHAN_NTT;// Apr2619 set 1 mình chị Nhàn cho All
        return GasLeave::PHUONG_PTK;// Aug2718 set 1 mình chị Phương cho All
        $mOrder = new GasOrders();
        $aAccountingZone = $mOrder->getAccountingZone();
        return isset($aAccountingZone[$idZone]) ? $aAccountingZone[$idZone] : GasConst::UID_HIEP_TV;
    }
    
    /** @Author: HOANG NAM 16/01/2018 **/
    public function beforeUpdate() {
        $this->date_pay= MyFormat::dateConverYmdToDmy($this->date_pay, 'd-m-Y');
    }
    
    /** @Author: ANH DUNG May 29, 2018
     *  @Todo: tự động sinh quyết toán tháng tiếp theo vào đầu ngày 26 hàng tháng
     **/
    public function cronMakeSettle() {
        if(date('d') != 25){
            return ;
        }
//        $this->debug = true;
        $nextMonth  = MyFormat::modifyDays(date('Y-m-d'), 1, '+', 'month');
        $nextMonth  = explode('-', $nextMonth);
        $firstDateNextMonth  = $nextMonth[0].'-'.$nextMonth[1].'-01';
        $lastDateNextMonth   = MyFormat::dateConverYmdToDmy($firstDateNextMonth, 'Y-m-t');
        $this->setUidLeader(GasConst::UID_HIEP_TV);
        $this->windowInsert($firstDateNextMonth, $lastDateNextMonth);
    }
    
    /** @Author: ANH DUNG Jun 27, 2018
     *  @Todo: delete by SettleId
     **/
    public function deleteBySettleId() {
        if(empty($this->settle_id)){
            return ;
        }
        $criteria = new CDbCriteria;
        $criteria->compare('settle_id', $this->settle_id);
        HomeContractDetail::model()->deleteAll($criteria);
    }
    /** @Author: Pham Thanh Nghia 2018
     *  @Todo: /admin/gasSettle/index
     *  @Param: get Note cho Excel 
     **/
    public function getNote(){
        return "Cty Gas tt tiền thuê nhà ".$this->formatDate()." ".$this->formatNameAgent();
    }
    public function formatDate(){
        $date = explode('-', $this->date_pay);
        $month = $date[1];
        $year = $date[0];
        $amount_month_pay = $this->amount_month_pay;
        if($amount_month_pay > 3){
            return 'trong '.$amount_month_pay.' tháng từ T'.$month.'/'.$year;
        }
        if($amount_month_pay == 1){
             return 'T'.$month.'/'.$year;
        }
        if ($month <= 3) return 'Quý 1/'.$year;
        if ($month <= 6) return 'Quý 2/'.$year;
        if ($month <= 9) return 'Quý 3/'.$year;
        return 'Quý 4/'.$year;
    }
    public function formatNameAgent(){
        $aReplace = ["Đại lý","Đại Lý"];
        return str_replace($aReplace,"ĐL",$this->getAgent());
    }
    
    /** @Author: NamNH Oct0119
    *  @Todo:trả về true nếu đã thanh toán
    **/
    public function isStatusDone(){
        if($this->status == self::STATUS_PAY){
            return true;
        }
        return false;
    }
}
