<?php

/**
 * This is the model class for table "{{_click_call}}".
 *
 * The followings are the available columns in table '{{_click_call}}':
 * @property string $id
 * @property integer $type
 * @property string $obj_id
 * @property string $user_id
 * @property string $phone
 * @property string $created_date
 * @property string $created_date_bigint
 */
class ClickCall extends BaseSpj
{
    const TYPE_HGD      = 1;
    const TYPE_BOMOI    = 2;
    
    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return ClickCall the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{_click_call}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        return [
            ['id, type, obj_id, user_id, phone, created_date, created_date_bigint', 'safe'],
        ];
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        return [
        ];
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'type' => 'Type',
            'obj_id' => 'Obj',
            'user_id' => 'User',
            'phone' => 'Phone',
            'created_date' => 'Created Date',
            'created_date_bigint' => 'Created Date Bigint',
        ];
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        $criteria=new CDbCriteria;
	$criteria->compare('t.type',$this->type);
	$criteria->compare('t.obj_id',$this->obj_id);
	$criteria->compare('t.user_id',$this->user_id);
	$criteria->compare('t.phone',$this->phone);
        $criteria->order = 't.id DESC';
        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
            'pagination'=>array(
                'pageSize'=> Yii::app()->user->getState('pageSize',Yii::app()->params['defaultPageSize']),
            ),
        ));
    }
    
    /** @Author: ANH DUNG Oct 03, 2018
     *  @Todo: get data post from app 
     **/
    public function handleAppPost($q) {
        $this->type         = $q->type;
        $this->obj_id       = $q->obj_id;
        $this->phone        = $q->phone;
        $this->user_id      = $this->mAppUserLogin->id;
        $this->validate();
    }
    
    
}