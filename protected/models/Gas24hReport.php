<?php
/**
 * Class viết báo cáo cho App gas 24h
 * @CreatedBy DuongNV 8 Jun,2019
 */
class Gas24hReport
{
    
    /** @Author: DuongNV 8 Jun,19
     *  @Todo: bc so ao
     **/
    public function getCheckVirtualPhoneReport($date_from, $date_to, &$aData1, &$aData2) {
        $aPhone             = $this->getPhoneFromExcel();
        if(empty($aPhone)) return;
        $criteria1          = new CDbCriteria;
        $criteria1->select  = 'COUNT(t.id) as id, t.area_code_id, t.created_by';
        DateHelper::searchBetween($date_from, $date_to, 'created_date_bigint', $criteria1);
        $criteria1->compare('t.role_id', ROLE_CUSTOMER); 
        $criteria1->compare('t.type', CUSTOMER_TYPE_STORE_CARD); 
        $criteria1->compare('t.is_maintain', UsersExtend::STORE_CARD_HGD_APP); 
        $criteria1->addInCondition('t.username', $aPhone); 
        $criteria1->group   = 't.area_code_id, t.created_by';
        $aModel1            = Users::model()->findAll($criteria1);
        $aAccount           = [];
        foreach ($aModel1 as $value) {
            if(isset($aData1[$value->area_code_id])){
                $aData1[$value->area_code_id]['count'] += $value->id;
            } else {
                $aData1[$value->area_code_id] = [
                    'agent' => isset($value->by_agent) ? $value->by_agent->first_name : "",
                    'count' => $value->id
                ];
            }
            $aAccount[$value->created_by] = [
                'created_by' => isset($value->rCreatedBy) ? $value->rCreatedBy->first_name : '',
                'count' => $value->id
            ];
        }

        $criteria2          = new CDbCriteria;
        $criteria2->addInCondition('t.owner_id', array_keys($aAccount)); 
        $aModel2            = AppPromotion::model()->findAll($criteria2);
        $aData2             = [];
        foreach ($aModel2 as $value) {
            $aData2[$value->owner_id] = [
                'name'      => isset($aAccount[$value->owner_id]) ? $aAccount[$value->owner_id]['created_by'] : '',
                'code_no'   => $value->code_no,
                'count'     => isset($aAccount[$value->owner_id]) ? $aAccount[$value->owner_id]['count'] : '',
            ];
        }
    }
    
    /** @Author: DuongNV 8 Jun,19
     *  @Todo: get list phone from excel
     **/
    public function getPhoneFromExcel() {
        $aPhone         = [];
        $file_name      = $_FILES['Users']['tmp_name']['file_excel'];
        if (empty($file_name)) return $aPhone;
        Yii::import('application.extensions.vendors.PHPExcel', true);
        $inputFileType  = PHPExcel_IOFactory::identify($file_name);
        $objReader      = PHPExcel_IOFactory::createReader($inputFileType);
        $objPHPExcel    = $objReader->load(@$_FILES['Users']['tmp_name']['file_excel']);
        $objWorksheet   = $objPHPExcel->getActiveSheet();
        $highestRow     = $objWorksheet->getHighestRow(); // e.g. 10
        if($highestRow > 10000){
            die("$highestRow row can not import, recheck file excel");
        }
        $aPrefix = UpdateSql1::getArrayPrefixChangeNotZero();
        for ($row = 2; $row <= $highestRow; ++$row) {
            // file excel cần format column theo dạng text hết để người dùng nhập vào khi đọc không lỗi
            $phone          = MyFormat::removeBadCharacters($objWorksheet->getCellByColumnAndRow(0, $row)->getValue());
            CustomerDraft::model()->convertToPhoneNumber($phone);
            if(empty($phone)){
                continue ;
            }
            $prefix_code    = substr($phone, 0, 3);
            if(isset($aPrefix[$prefix_code])){
                $charRemove = strlen($prefix_code);
                $aPhone[]   = $aPrefix[$prefix_code].substr($phone, $charRemove);
            } else {
                $aPhone[]   = $phone;
            }
        }
        return $aPhone;
    }
}