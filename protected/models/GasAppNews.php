<?php

/**
 * This is the model class for table "{{_gas_app_news}}".
 *
 * The followings are the available columns in table '{{_gas_app_news}}':
 * @property string $id
 * @property integer $status
 * @property string $title
 * @property string $short_content
 * @property string $content
 * @property string $created_date
 */
class GasAppNews extends BaseSpj
{
    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return GasAppNews the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{_gas_app_news}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        return array(
            array('title', 'length', 'max'=>350),
            array('id, status, title, short_content, content, created_date', 'safe'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        return array();
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'status' => 'Status',
            'title' => 'Title',
            'short_content' => 'Short Content',
            'content' => 'Content',
            'created_date' => 'Created Date',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        $criteria=new CDbCriteria;
        $criteria->compare('t.status',$this->status);
        $criteria->compare('t.title',$this->title,true);
        $criteria->compare('t.short_content',$this->short_content,true);
        $criteria->compare('t.content',$this->content,true);
        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
            'pagination'=>array(
                'pageSize'=> Yii::app()->user->getState('pageSize',Yii::app()->params['defaultPageSize']),
            ),
        ));
    }

    public function defaultScope()
    {
        return array();
    }
    
    /**
    * @Author: ANH DUNG Oct 27, 2015
    * @Todo: get listing News
    * @param: $q object post params
    */
    public static function ApiListing($q, $mUser) {
        $criteria = new CDbCriteria();
//        $criteria->compare('t.customer_id', $mUser->id);
        $criteria->order = 't.id DESC';

        $dataProvider=new CActiveDataProvider('GasAppNews',array(
            'criteria' => $criteria,
            'pagination'=>array(
                'pageSize' => GasAppOrder::API_LISTING_ITEM_PER_PAGE,
                'currentPage' => (int)$q->page,
            ),
          ));
        return $dataProvider;
    }
    
        
    public function getTitle() {
        return $this->title;
    }
    
    public function getShortContent() {
        return nl2br($this->short_content);
    }
    
    public function getContent() {
        return nl2br($this->content);
    }
    
}