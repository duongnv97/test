<?php

/**
 * This is the model class for table "{{_actions_users}}".
 *
 * The followings are the available columns in table '{{_actions_users}}':
 * @property integer $id
 * @property integer $user_id
 * @property integer $action_id
 * @property string $can_access
 */
class ActionsUsers extends CActiveRecord
{
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
            return '{{_actions_users}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        return array(
            array('id, user_id, can_access', 'safe'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        return array(
            'user' => array(self::BELONGS_TO, 'Users', 'user_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'user_id' => 'User',
            'action_id' => 'Action',
            'can_access' => 'Can Access',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
            $criteria=new CDbCriteria;
            $criteria->compare('user_id',$this->user_id);		
            $criteria->compare('can_access',$this->can_access,true);
            return new CActiveDataProvider($this, array(
                    'criteria'=>$criteria,
            ));
    }

    // May 08, 2014 bb code - ANH DUNG ADD
    public static function getActionArrayByUserIdAndControllerId($user_id, $controller_id, $can_access = 'allow')
    {
        $aActions = array();
        $criteria = new CDbCriteria;
        $criteria->compare('t.user_id', $user_id);
        $criteria->compare('t.controller_id', $controller_id);
        $criteria->compare('t.can_access', $can_access);
        $model = self::model()->find($criteria);
        if ($model)
        {                
            if(!empty($model->actions))
            {
                $aActions = explode(', ', $model->actions);
            }
        }
        return $aActions;
    }

    /**
     * @Author: ANH DUNG May 13, 2014
     * @Todo: get list action of contronller of current user login
     * @Param: $controllerName
     * @Return: array('Index','Create'...)
     */
    public static function getActionArrayAllowForCurrentUserByControllerName($controllerName)
    {
        $aResult = array();
        $user_id = Yii::app()->user->id;
        $mUser = Users::model()->findByPk($user_id);
        $mController = Controllers::getByName($controllerName);
        // ANH DUNG FIX  NOW 14, 2014
        if($mController)
        {
//          NOW 14, 2014 => findAll sai khi check null(BB) $mActionsUsers = ActionsUsers::model()->findAll('user_id='.$user_id.' AND controller_id='.$mController->id);
            $mActionsUsers = ActionsUsers::model()->find('user_id='.$user_id.' AND controller_id='.$mController->id);
//            if($mActionsUsers == NULL)
            if( is_null($mActionsUsers) )
            {
                $aActionsAllowGroup = ActionsRoles::getActionArrayByRoleIdAndControllerId($mUser->role_id, $mController->id);
                $aResult = $aActionsAllowGroup;
            }
            else{
                $aActionsAllowUser = ActionsUsers::getActionArrayByUserIdAndControllerId($user_id, $mController->id);
                $aResult = $aActionsAllowUser;
            }
                
        }
//        if($mController)
//        {
//            $mActionsUsers = ActionsUsers::model()->findAll('user_id='.$user_id.' AND controller_id='.$mController->id);
//            $aActionsAllowGroup = ActionsRoles::getActionArrayByRoleIdAndControllerId($mUser->role_id, $mController->id);
//            $aActionsAllowUser = ActionsUsers::getActionArrayByUserIdAndControllerId($user_id, $mController->id);
//            if($mActionsUsers == NULL)
//            {
//                $aResult = $aActionsAllowGroup;
//            }
//            else
//                $aResult = $aActionsAllowUser;
//        }
        
        

        return $aResult;
    }

    public static function isAllowAccess($controllerName, $actionName)
    {
        $aActionAllowed = ActionsUsers::getActionArrayAllowForCurrentUserByControllerName($controllerName);
        if(in_array(ucfirst($actionName), $aActionAllowed))
        {
            return true;
        }
        return false;
    }        
    // END May 08, 2014 bb code - ANH DUNG ADD
    /**
     * @Author: ANH DUNG Jan 04, 2017
     * @Todo: check user có custom quyền không
     */
    public static function haveCustomAccess($user_id) {
        $criteria = new CDbCriteria();
        $criteria->addCondition('t.user_id=' . $user_id);
        return self::model()->count($criteria);
    }
}