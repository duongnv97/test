<?php

/**
 * This is the model class for table "{{_users_sms}}".
 *
 * The followings are the available columns in table '{{_users_sms}}':
 * @property string $id
 * @property integer $sms_type
 * @property string $user_id
 * @property string $phone
 * @property integer $role_id
 * @property integer $is_maintain
 * @property integer $province_id
 * @property integer $district_id
 * @property string $last_purchase
 * @property string $customer_id_app
 * @property string $created_date
 */
class UsersSms extends BaseSpj
{
    const TYPE_1_CODE_GAS24H = 1;
    
    const REPORT_INSTALL_APP        = 1;
    const REPORT_MAKE_ORDER         = 2;
    
    
    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return UsersSms the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{_users_sms}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        return array(
            array('id, sms_type, user_id, phone, role_id, is_maintain, province_id, district_id, last_purchase, customer_id_app, created_date', 'safe'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        return array(
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'sms_type' => 'Sms Type',
            'user_id' => 'User',
            'phone' => 'Phone',
            'role_id' => 'Role',
            'is_maintain' => 'Is Maintain',
            'province_id' => 'Province',
            'district_id' => 'District',
            'last_purchase' => 'Last Purchase',
            'customer_id_app' => 'Customer Id App',
            'created_date' => 'Created Date',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        $criteria=new CDbCriteria;
        $criteria->compare('t.id',$this->id,true);
        $criteria->order = 't.id DESC';
        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
            'pagination'=>array(
                'pageSize'=> Yii::app()->user->getState('pageSize',Yii::app()->params['defaultPageSize']),
            ),
        ));
    }
    
    /** @Author: ANH DUNG Apr 01, 2018
     *  @Todo: thống kê KH cài app và đặt hàng của code gas24h
     **/
    public function countOfCode($type) {
        $criteria = new CDbCriteria();
        $criteria->addCondition('t.sms_type='.UsersSms::TYPE_1_CODE_GAS24H);
        if($type == UsersSms::REPORT_INSTALL_APP){
            $criteria->addCondition('t.customer_id_app>0');
        }elseif($type == UsersSms::REPORT_MAKE_ORDER){
            $criteria->addCondition("t.last_purchase IS NOT NULL AND t.last_purchase<>'0000-00-00' ");
        }
//        $criteria->select = 'count(t.id) as id';
        return UsersSms::model()->count($criteria);
    }
    
    
    
}