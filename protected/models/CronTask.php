<?php

/**
 * This is the model class for table "cron_task".
 *
 * The followings are the available columns in table 'cron_task':
 * @property string $id
 * @property integer $status
 * @property integer $type
 * @property string $obj_id
 * @property string $created_date
 * @property string $last_update
 * @property string $title
 */
class CronTask extends BaseSpj
{
    /** @Des: Apr1018 xử lý các tác vụ có nhiều data, chạy làm nhiều lần
     * VD: buld notify thông báo tin cho app Gas24h, có 100k user login, phải chạy để insert 5k row 1 lần
     */
    const TYPE_GAS24H_ANNOUNCE      = 1;
    const TYPE_GAS24H_NOTIFY_ONLY   = 2;// chỉ notify 1 thông báo, không có bài viết detail
    const TYPE_SYNC_APPCACHE                = 3;// Sync AppCache to server load balancer S1,2,3
    const TYPE_UPDATE_AGENT_INVENTORY       = 4;// gom toàn bộ các sự kiện update AppCache::APP_AGENT_INVENTORY vào chạy 1 lần, ko chạy liên tục như hiện tại, do nếu có 2 đơn xe tải giao đại lý hoàn thành thời điểm gần nhau sẽ bị run trùng
    
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'cron_task';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        return array(
            array('id, status, type, obj_id, created_date, last_update', 'safe'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        return array(
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'status' => 'Status',
            'type' => 'Type',
            'obj_id' => 'Obj',
            'created_date' => 'Created Date',
            'last_update' => 'Last Update',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        $criteria=new CDbCriteria;
        $criteria->compare('t.last_update',$this->last_update,true);
        $criteria->order = 't.id DESC';
        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
            'pagination'=>array(
                'pageSize'=> Yii::app()->user->getState('pageSize',Yii::app()->params['defaultPageSize']),
            ),
        ));
    }
    
    /** @Author: DungNT Apr 10, 2018
     *  @Todo: get all task active to run
     **/
    public function getTaskActive() {
        $criteria = new CDbCriteria();
        $criteria->addCondition('t.status=' . STATUS_ACTIVE );
        if(is_array($this->type)){
            $criteria->addInCondition('t.type', $this->type);
        }
        return self::model()->findAll($criteria);
    }
    
    /** @Author: DungNT Apr 10, 2018
     **/
    public function setInactive() {
        $this->status       = STATUS_INACTIVE;
        $this->last_update  = $this->getCreatedDateDb();
        $this->update();
    }
    
    /** @Author: DungNT Apr 10, 2018
     *  @Todo: run console cron,
     *  Aug2519 do các task notify này run lâu nên ở Cron này chỉ xử lý cho 2 loại notify này.
     *  Còn các loại khác sẽ viết riêng Cron mới runV1()
     **/
    public function run() {
        $from = time();
        $this->type     = [CronTask::TYPE_GAS24H_ANNOUNCE, CronTask::TYPE_GAS24H_NOTIFY_ONLY];
        $aTask          = $this->getTaskActive();
        $setInactive    = true;
        foreach($aTask as $item):
            $setInactive = false;
            switch ($item->type) {
                case CronTask::TYPE_GAS24H_ANNOUNCE:
                    $setInactive = $item->doGas24hAnnounce();
                    break;
                case CronTask::TYPE_GAS24H_NOTIFY_ONLY:
                    $setInactive = $item->doGas24hNotifyOnly();
                    break;
                default:
                    break;
            }
            
            if($setInactive){
                $item->setInactive();
            }
        endforeach;
        $to = time();
        $second = $to-$from;
        $info = "Run CronTask done in: $second  Second  <=> ".($second/60)." Minutes";
        if(!$setInactive){
            Logger::WriteLog($info);
        }
    }
    
    /** @Author: DungNT Aug 25, 2019
     *  @Todo: run other task short time
     *  @flow: 
     *  1. zip folder cache at server noibo.daukhimiennam.com
     *  2. make post list file name will copy overide on server load
     *  3. At Server load: actionSyncCache will download folder cache from server noibo.daukhimiennam.com
     *  4. At Server load: actionSyncCache will overide each file cache in string ListCacheFileName
     *  DONE
     *  Hàm đồng bộ Cache sang S1,2,3 từ server nội bộ thông qua table CronTask 
     *  @note: build 1 list name file rồi POST sang bên kia update 
     * $_POST['ListCacheFileName'] format: ARR_MODEL_MATERIAL_GAS, ARR_MODEL_LOOKUP, CacheModelUserRole_5, CacheModelUserRole_6...
     *  1. gửi tín hiệu đồng bộ CacheKey từ S1,2,3 sang server nội bộ
     *  Bên server nội bộ phải run lại hàm set biến cache đó rồi mới add record CronTask
     *  Hiện tại chỉ có biến AppCache::APP_AGENT_INVENTORY là đc set từ app khi tài xế hoàn thành đơn giao hàng nội bộ cho đại lý
     *  2. khi update 1 cache nào ở live thì add record Sync CronTask
     * 
     **/
    public function runSyncCache() {
        // 1. zip folder cache before run 
//        $this->zipCacheFile();// DungNT ko dùng cách zip để đồng bộ cache được
        // 2. run sync folder
        $from = time(); $writeLog = false; $aCacheKey = []; $listCacheFileName = '';
        $this->type     = [CronTask::TYPE_SYNC_APPCACHE];
        $aTask          = $this->getTaskActive();
        if(count($aTask)):
            $writeLog   = true;
            foreach($aTask as $item):
                $name = empty($item->obj_id) ? $item->title : ($item->title.$item->obj_id);
                $aCacheKey[$name] = $name;
            endforeach;
            $listCacheFileName = implode(',', $aCacheKey);
            // 3. make request to server load
            $this->doPostToServerLoad($aCacheKey);
            // 4. update inactive + set lastupdate for record
            foreach($aTask as $item):
                $item->afterRunForSomeType();
            endforeach;
        endif;
        
        $to = time();
        $second = $to-$from;
        $info = "runSyncCache CronTask Sync AppCache done in: $second  Second  <=> ".($second/60)." Minutes listCacheFileName: $listCacheFileName";
        if($writeLog){
//            Logger::WriteLog($info);
        }
    }
    
    /** @Author: DungNT Sep 01, 2019
     *  @Todo: active will inactive or delete for some type
     **/
    public function afterRunForSomeType() {
        $this->setInactive();
//        $this->delete();
    }
    
    /** @Author: DungNT Sep 01, 2019
     *  @Todo: run some type other of CronTask
     * 1. Sep119 run type 4 = CronTask::TYPE_UPDATE_AGENT_INVENTORY
     **/
    public function runTypeUpdateAgentInventory() {
        $this->type     = [CronTask::TYPE_UPDATE_AGENT_INVENTORY];
        $aTask          = $this->getTaskActive();
        if(count($aTask)):
            Sta2::setCacheInventoryAllAgent();
        endif;
        foreach($aTask as $item):
            $item->afterRunForSomeType();
        endforeach;
    }
    
    
    /** @Author: DungNT Apr 10, 2018
     *  @Todo: buildGas24hAnnounce
     **/
    public function doGas24hAnnounce() {
        $mScheduleNotify = new GasScheduleNotify();
        $mScheduleNotify->mPromotion = AppPromotion::model()->findByPk($this->obj_id);
        if(empty($mScheduleNotify->mPromotion)){
            return true;
        }
        return $mScheduleNotify->buildGas24hAnnounce();
    }
    
    /** @Author: DungNT Apr 11, 2018
     *  @Todo: buildGas24hAnnounce
     **/
    public function doGas24hNotifyOnly() {
        if(empty($this->title)){
            throw new Exception('doGas24hNotifyOnly Empty title, dữ liêu không hợp lệ');
        }
        $mScheduleNotify = new GasScheduleNotify();
        $mScheduleNotify->title = $this->title;
        return $mScheduleNotify->buildGas24hNotifyOnly();
    }
    
    /** @Author: DungNT Aug 25, 2019
     *  @Todo: kiểm tra xem có check vào notify cho Gas24h không
     *  @param: $cacheKey name of cache ex: AppCache::ARR_MODEL_AGENT, AppCache::LISTDATA_AGENT ....
     *  @param: $obj_id ref id model
     **/
    public function addTaskSyncAppCache($type, $cacheKey, $obj_id = 0) {
        try{
//        $this->type     = CronTask::TYPE_SYNC_APPCACHE;
        $this->type     = $type;
        $this->title    = $cacheKey;
        $this->obj_id   = $obj_id;
        $this->json     = isset($_SERVER['SERVER_ADDR']) ? $_SERVER['SERVER_ADDR'] : 'Run Console';
        if(isset($_SERVER['SERVER_ADDR'])){
            $this->json  = $_SERVER['SERVER_ADDR'] . GasCheck::getCurl();
        }
        $this->save();
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }
    
    /** @Author: DungNT Aug 26, 2019
     *  @Todo: zip folder cache File
     **/
    public function zipCacheFile() {
        $mSyncData      = new SyncData(SyncData::SERVER_IO_SOCKET, 'socket/notifyAdd');
        $urlFrom        = SyncData::ZIP_URL_CACHE;
        $urlTo          = SyncData::ZIP_URL_EXTRACT;
        $fileNameSave   = SyncData::ZIP_DEFAULT_NAME;
        $mSyncData->zipFile($urlFrom, $urlTo, $fileNameSave);
    }

    /** @Author: DungNT Aug 26, 2019
     *  @Todo: unzip folder cache File
     **/
    public function unzipCacheFile() {
        $mSyncData      = new SyncData(SyncData::SERVER_IO_SOCKET, 'socket/notifyAdd');
        $urlOpen        = SyncData::ZIP_URL_EXTRACT.'/'.SyncData::ZIP_DEFAULT_NAME.'.zip';
        $urlExtractTo   = SyncData::ZIP_URL_EXTRACT.'/'.SyncData::ZIP_DEFAULT_NAME;
        $mSyncData->unzipFile($urlOpen, $urlExtractTo);
    }
    
    /** @Author: DungNT Aug 26, 2019
     *  @Todo: download zip folder cache File
     **/
    public function downloadZipCacheFile() {
        $mMyFormat = new MyFormat();
        $url            = SyncData::DOMAIN_LIVE_NOIBO.SyncData::ZIP_URL_EXTRACT.'/'.SyncData::ZIP_DEFAULT_NAME.'.zip';
        $path           = SyncData::ZIP_URL_EXTRACT;
        $fileName       = SyncData::ZIP_DEFAULT_NAME.'.zip';
        $mMyFormat->downloadFileUsingCurlV1($url, $path, $fileName);
    }
    
    /** @Author: DungNT Aug 26, 2019
     *  @Todo: do request post to all server load balancer
     **/
    public function doPostToServerLoad($aCacheKey) {
        $mAppCache = new AppCache();
        foreach(SyncData::$aIpLoadBalancer as $ip):
            $url        = "http://$ip";
            $action     = '/api/spj/syncCache';
            $mSyncData      = new SyncData($url, $action);
            foreach($aCacheKey as $cacheKey):
                if(!$mAppCache->isNeedSync($cacheKey)){
                    continue ;
                }
                $cacheValue = $mAppCache->getCache($cacheKey);
                if(empty($cacheValue)){
                    $cacheValue = $mAppCache->getOneCacheByKey($cacheKey);
                }
                $params     = ['cacheKey' => $cacheKey, 'cacheValue' => $cacheValue];
                $mSyncData->doRequestNormal($params);
            endforeach;
        endforeach;
    }
    
    public function doPostToServerLoadBackup($aCacheKey) {// Đã xử lý post theo cách khác
//        if(empty($listCacheFileName)){
//            return ;
//        }
//        Logger::WriteLog("Run doPostToServerLoad listCacheFileName: $listCacheFileName");
//        foreach(SyncData::$aIpLoadBalancer as $ip):
//            $url        = "http://$ip";
//            $action     = '/api/spj/syncCache';
//            $mSyncData      = new SyncData($url, $action);
//            $params     = ['listCacheFileName' => $listCacheFileName];
//            $mSyncData->doRequestNormal($params);
//        endforeach;
    }
    
    /** @Author: DungNT Aug 25, 2019
     *  @Todo: update cache for each server s1,2,3
     *  @param: $listCacheKey is string  ex: APP_CONFIG_AGENT,ARR_MODEL_AGENT4168,ARR_MODEL_PROVINCE
     *  @flow: get $_POST['cacheValue'] and set new cache
     *  remove this way =>  1. download file assets/SpjCacheFile.zip save to assets
     *  2. copyfile overide to runtime/cache 
     **/
    public function syncUpdateCacheFileName() {
        if(!isset($_POST['cacheKey'])){
            return ;
        }
        $mAppCache = new AppCache();
        $mAppCache->isSyncCacheFile = false;
        $mAppCache->setCache($_POST['cacheKey'], $_POST['cacheValue'], 0);
    }
    
    /** @note: Aug2619 không sử dụng copy nguyên folder Cache rồi zip đc
     *  1/ có thể do Yii quản lý cache trong db sql lite nên 1 file copy 1 file mới vào sẽ bị remove khi gọi idcache đó
     *  2/ dù có copy sang rồi đọc content để ghi lại cache mới cũng không đc, có thể cách đọc không đúng nên ghi không ra
     *  Đã xử lý post nguyên data khi $mAppCache->getCache($cacheKey);
     */
    public function syncUpdateCacheFileNameBackUp($listCacheFileName) {
//        $this->downloadZipCacheFile();
//        $this->unzipCacheFile();
//        $aCacheFileName = explode(',', $listCacheFileName);
//        foreach($aCacheFileName as $fileName):
//            $mSyncData = new SyncData(SyncData::SERVER_IO_SOCKET, 'socket/notifyAdd');
//            $urlFrom    = Yii::getPathOfAlias("webroot").SyncData::ZIP_URL_EXTRACT.'/'.SyncData::ZIP_DEFAULT_NAME.'/'.$fileName.'.bin';
//            $content    = file_get_contents($urlFrom);
//            $mAppCache->setCache($fileName, $content, 0);
////            $urlTo      = Yii::getPathOfAlias("webroot").SyncData::ZIP_URL_CACHE.'/'.$fileName.'.bin';
////            $mSyncData->copyFile($urlFrom, $urlTo);
//        endforeach;
    }
    
    
    /**
     * @Author: DuongNV Sep0319
     * @Todo: cron Xóa những record được tạo 1 ngày trc
     */
    public function cronRemoveTask(){
        $daysRemove     = 2;
        $aTypeRemove    = [CronTask::TYPE_SYNC_APPCACHE];
        $criteria       = new CDbCriteria();
        $criteria->addCondition("DATE_ADD( DATE(created_date), INTERVAL $daysRemove DAY ) <= CURDATE()");
        $criteria->addInCondition('type', $aTypeRemove);
        $numRows        = CronTask::model()->deleteAll($criteria);
        
        // Send log
        $needMore['title']      = 'Cron Remove Task';
        $needMore['list_mail']  = ['duongnv@spj.vn'];
        $infoLog                = "cronRemoveTask<br> $numRows rows removed";
        SendEmail::bugToDev($infoLog, $needMore);
        $strLog = '******* cronRemoveTask done at '.date('d-m-Y H:i:s').' | '.$numRows.' rows removed';
        Logger::WriteLog($strLog);
    }
    
    
    
    
}