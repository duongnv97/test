<?php

/**
 * This is the model class for table "{{_gas_cash_book_detail}}".
 *
 * The followings are the available columns in table '{{_gas_cash_book_detail}}':
 * @property string $id
 * @property string $cash_book_id
 * @property string $agent_id
 * @property string $release_date
 * @property integer $type
 * @property string $amount
 * @property integer $qty
 * @property string $description
 * @property string $created_date 
 * @property string $detail_type
 */
class GasCashBookDetail extends BaseSpj
{
    const D_TYPE_OLD    = 1;// kiểu insert cũ
    const D_TYPE_BOMOI  = 2;// data from app GN, thu chi bò mối
    const D_TYPE_HGD    = 3;// cron auto gen BH hộ GĐ
    
    const P_OPENING_BALANCE     = 'P_OPENING_BALANCE';// name param report
    const P_INCURRED            = 'P_INCURRED';
    
    const TYPE_ITEM_NORMAL      = 1;// các bán hàng gas, bếp val dây
    const TYPE_ITEM_SELL_TYPE   = 2;// loại bán hàng: bộ bình, thế chân, thu vỏ
    const TYPE_ITEM_GAS_REMAIN  = 3;// xử lý gas dư
    public $aUidGoBank = []; // danh sách NV đã đi nộp NH trong ngày
    public $isInventoryAlert = false;// biến để kiểm tra đang chạy cảnh báo tồn quỹ, để có thể  tái sử dụng hàm đó cho báo cáo khác nữa
    public $aDataNegativeCashbookSort=[], $aDataNegativeCashbook=[], $aEmployeeDebit=[], $aDataSort=[], $aDataAlert=[], $date_from, $date_to, $date_from_ymd, $date_to_ymd, $province_id, $aData=[];
    public $typeReport = 0, $materials_type_id = 0, $materials_type_name = '', $typeItem='';
    public $autocomplete_user;
    
    const TARGET_AGENT          = 1;
    const TARGET_EMPLOYEE       = 2;
    const TARGET_EMPLOYEE_OFF   = 3;
    const TARGET_EMPLOYEE_DEBIT = 4;
    const TARGET_SALE               = 5;
    const TARGET_EMPLOYEE_BY_AGENT  = 6;
    
    const VO_12_BU              = 1;
    const VO_12_GAS_REMAIN      = 2;
        
    /** @Author: DungNT Dec 07, 2017
     *  get min cash alert **/
    public function getMinCashAlert() {
        return 2000000;
    }
    /** @Author: DungNT Feb 14, 2019
     *  get min cash alert **/
    public function getMinCashCheckCheat() {
        return 10000;// 10k
    }
    public function getPercentPunishNegativeCashbook() {
        return 20;// 20%
    }
    public function getMinCashForViettelPay() { // min cash ko quét nộp viettel pay
        return 1000;// 1k
    }
    
    /** @Author: DungNT Jun 21, 2018
     *  @Todo: get list user thu nợ
     **/
    public function getArrayEmployeeDebit() {
        return [
            GasConst::UID_BINH_TQ, 
        ];
    }
    public function getUserViewDebit() {
        return [
            2,
            GasLeave::THUC_NH,
            GasLeave::PHUONG_PTK,
            GasLeave::UID_CHIEF_ACCOUNTANT,
            GasConst::UID_NGAN_DTM,
        ];
    }
    
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{_gas_cash_book_detail}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        return array(			
            array('province_id, id, cash_book_id, agent_id, release_date, type, amount, qty, description, created_date', 'safe'),
            array('employee_id, date_from, date_to, last_update_time, detail_type, master_lookup_id, customer_id, name_employee, type_customer, sale_id', 'safe'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        return array(
            'master_lookup' => array(self::BELONGS_TO, 'GasMasterLookup', 'master_lookup_id'),
            'customer' => array(self::BELONGS_TO, 'Users', 'customer_id'),
            'rCustomer' => array(self::BELONGS_TO, 'Users', 'customer_id'),
            'cash_book' => array(self::BELONGS_TO, 'GasCashBook', 'cash_book_id'),
            'rEmployee' => array(self::BELONGS_TO, 'Users', 'employee_id')
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'cash_book_id' => 'Mã Sổ Quỹ',
            'agent_id' => 'Đại Lý',
            'release_date' => 'Ngày Phát Sinh',
            'type' => 'Loại',
            'amount' => 'Số Tiền',
            'qty' => 'Số Lượng',
            'description' => 'Diễn Giải',
            'created_date' => 'Ngày Tạo',
            'customer_id' => 'Khách Hàng',
            'name_employee' => 'Họ Tên NV',
            'type_customer' => 'loại KH', //loại KH 1: Bình Bò, 2: Mối, dùng cho thống kê doanh thu
            'province_id' => 'Tỉnh',
            'date_from' => 'Từ ngày',
            'date_to' => 'Đến',
            'employee_id' => 'Nhân viên',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        $criteria=new CDbCriteria;
        $criteria->compare('t.id',$this->id,true);
       
        return new CActiveDataProvider($this, array(
                    'criteria'=>$criteria,
                    'pagination'=>array(
                        'pageSize'=> Yii::app()->user->getState('pageSize',Yii::app()->params['defaultPageSize']),
                    ),
                ));
    }
    
    public function getCustomer($field_name='') {
        if($this->customer_id == GasConst::UID_KH_HO_GD){
            return 'HGĐ';
        }
        $mUser = $this->rCustomer;
        if($mUser){
            if(empty($field_name)){
                return $mUser->code_bussiness.'-'.$mUser->first_name;
            }else{
                return $mUser->$field_name;
            }
//            return "<b>".$mUser->code_bussiness."-".$mUser->first_name."</b><br>".$mUser->address."<br><b>Phone: </b>".$mUser->phone;
        }
        return '';
    }
    
    public function getCreatedDate($fomat = 'd/m/Y H:i') {
        return parent::getCreatedDate();
    }

    public function beforeSave() {
        $this->release_date_bigint = strtotime($this->release_date);
        return parent::beforeSave();
    }

    /**
    * @Author: DungNT 12-25-2013 
    * @Todo: get delete all CashBook detail by $cash_book_id
    * @Param: $cash_book_id
     */        
    public static function deleteByCashBookId($cash_book_id){
        if(empty($cash_book_id)){
            return ;
        }
        $criteria = new CDbCriteria();
        $criteria->compare('t.cash_book_id', $cash_book_id);
        $models = self::model()->findAll($criteria);
        // is update and delete record - need write log
        foreach($models as $item){                    
            $item->delete();
        }
    }
    
    protected function beforeDelete() {
        self::cutCollectionCustomerYear($this);
        return parent::beforeDelete();
    }

    /**
     * @Author: DungNT Jun 01, 2014
     * @Todo: Cộng đã thu của kh trong 1 năm
     * @Param: $mCashBookDetail model store card
     */    
    public static function addCollectionCustomerYear($mCashBookDetail){
        if($mCashBookDetail->type!=MASTER_TYPE_REVENUE) return;
        $collection_customer = $mCashBookDetail->amount;
        $type_customer = $mCashBookDetail->type_customer;// là loại KH 1: bò hay 2: mối, mới đổi lại laijMay 21, 2014
        $tmpDate = explode('-', $mCashBookDetail->release_date);
        $year = $tmpDate[0];
        $customer_id = $mCashBookDetail->customer_id;
        GasClosingBalanceCustomer::addCollectionCustomer($year, $customer_id, $type_customer, $collection_customer);
    }    
    
    /**
     * @Author: DungNT Jun 01, 2014
     * @Todo: Trừ đã thu của kh trong 1 năm
     * @Param: $mCashBookDetail model store card
     */    
    public static function cutCollectionCustomerYear($mCashBookDetail){
        if($mCashBookDetail->type!=MASTER_TYPE_REVENUE) return;        
        $collection_customer = $mCashBookDetail->amount;
        $tmpDate = explode('-', $mCashBookDetail->release_date);
        $year = $tmpDate[0];
        $customer_id = $mCashBookDetail->customer_id;
        GasClosingBalanceCustomer::cutCollectionCustomer($year, $customer_id, $collection_customer);
    }    
    
    /**
    * @Author: DungNT 12-25-2013
    * @Todo: get all model CashBook detail by $cash_book_id
    * @Param: $cash_book_id
     */        
    public static function getByCashBookId($cash_book_id){
        $criteria = new CDbCriteria();
        $criteria->compare('t.cash_book_id', $cash_book_id);
        $criteria->order = 'id ASC';
        return self::model()->findAll($criteria);
    }
    
    /**
     * @Author: DungNT May 09, 2017
     * @Todo: save record thu, chi tiền KH bò mối của nhân viên giao nhận, khi tạo trên app
     * @param: $mAppCashbook model of gas_employee_cashbook app GN input
     */
    public function makeRecordBomoi($mAppCashbook) {
        $this->deleteById($mAppCashbook->cash_book_detail_id);
        if(empty($mAppCashbook->amount)){
            return ;
        }
        $model = new GasCashBookDetail();
        $model->cash_book_id        = 2;
        $model->agent_id            = $mAppCashbook->agent_id;
        $model->release_date        = $mAppCashbook->date_input;
        $model->master_lookup_id    = $mAppCashbook->master_lookup_id;
        $model->type                = $mAppCashbook->lookup_type;
        $model->amount              = $mAppCashbook->amount;
//        $model->qty                 = $mAppCashbook->qty;
        $model->customer_id         = $mAppCashbook->customer_id;
        $model->customer_parent_id  = $mAppCashbook->customer_parent_id;
        $model->type_customer       = $mAppCashbook->type_customer;
        $model->sale_id             = $mAppCashbook->sale_id;
        $model->employee_id         = $mAppCashbook->employee_id;
        $model->description         = $mAppCashbook->note;
        $model->detail_type         = GasCashBookDetail::D_TYPE_BOMOI;
        $model->save();
        $mAppCashbook->cash_book_detail_id = $model->id;
        $mAppCashbook->update(['cash_book_detail_id']);
    }
    
    /**
     * @Author: DungNT May 10, 2017
     * @Todo: save record thu, chi tiền KH bò mối của nhân viên giao nhận, khi tạo trên app
     * @param: $mAppCashbook model of gas_employee_cashbook app GN input
     * @param: $typeItem loại record 1: normal, 2: loại bán hàng: bộ bình, thế chân, thu vỏ...
     */
//    public function makeRecordHgd($agent_id, $release_date, $materials_type_id,
//     $materials_type_name, 
//            $qty, $amount, $employee_id, $typeItem) {
    public function makeRecordHgd() {
        if(empty($this->amount) || in_array($this->materials_type_id, GasMaterialsType::$ARR_WINDOW_PROMOTION)){
            return '';
        }
        
        $this->cash_book_id        = 3;
        $this->customer_id         = GasConst::UID_KH_HO_GD;
        $this->customer_parent_id  = 0;
        $this->type_customer       = 0;
        $this->sale_id             = 0;
        $this->detail_type         = GasCashBookDetail::D_TYPE_HGD;
        // if $materials_type_id == vỏ thì qty=0 build note for all
        $this->getDataByMaterialType();
        $created_date = date('Y-m-d H:i:s');
        $this->release_date_bigint = strtotime($this->release_date);
        
        $sInsert="('$this->detail_type',
                        '$this->cash_book_id',
                        '$this->agent_id',
                        '$this->release_date',
                        '$this->release_date_bigint',
                        '$this->master_lookup_id',
                        '$this->type',
                        '$this->amount',
                        '$this->qty',
                        '$this->customer_id',
                        '$this->customer_parent_id',
                        '$this->sale_id',
                        '$this->type_customer',
                        '$this->employee_id',
                        '$this->name_employee',
                        '$created_date',
                        '$this->description'
                        )";
        
        return $sInsert;
    }
    
    /** @Author: DungNT May 10, 2017
     *  @Todo: get description by material type
     */
//    public function getDataByMaterialType($materials_type_id, $materials_type_name, $typeItem) {
    public function getDataByMaterialType() {
        if($this->typeItem == GasCashBookDetail::TYPE_ITEM_SELL_TYPE){
            $this->getDataBySellType($this->materials_type_id, $this->materials_type_name);
            return ;
        }
        if(empty($this->materials_type_id)){
            return '';
        }
        $this->description  = 'Thu Bán '.$this->materials_type_name;
        $this->type         = GasMasterLookup::MASTER_TYPE_THU;
        
        switch ($this->materials_type_id) {
            case GasMaterialsType::MATERIAL_BINH_12KG:
                $this->master_lookup_id = GasMasterLookup::ID_THU_BAN_GAS12;
                break;
            case GasMaterialsType::MATERIAL_VO_12:
                $this->master_lookup_id = GasMasterLookup::ID_THU_BU_VO;
                $this->description      = 'Thu Bù '.$this->materials_type_name;
                $this->qty              = 0;
                if($this->typeItem == GasCashBookDetail::TYPE_ITEM_GAS_REMAIN){
                    $this->type             = GasMasterLookup::MASTER_TYPE_CHI;
                    $this->master_lookup_id = GasMasterLookup::ID_CHI_GAS_DU;
                    $this->description      = 'Chi gas dư HGĐ';
                }
                break;
            case GasMaterialsType::MATERIAL_TYPE_DAY:
                $this->master_lookup_id = GasMasterLookup::ID_THU_BAN_DAY;
                break;
            case GasMaterialsType::MATERIAL_TYPE_VAL:
                $this->master_lookup_id = GasMasterLookup::ID_THU_BAN_VAN;
                break;
            case GasMaterialsType::MATERIAL_TYPE_BEP:
            case GasMaterialsType::MATERIAL_VT_CONG_NGHIEP:
                $this->master_lookup_id = GasMasterLookup::ID_THU_BAN_BEP;
                break;
            default:
                $this->master_lookup_id = GasMasterLookup::ID_THU_KHAC;
                $this->description      .= '. Thu khác type_id: '.$this->materials_type_id;
                break;
        }
    }

    /**
     * @Author: DungNT May 10, 2017
     * @Todo: get description by sell type: bộ bình, thế chân
     * @Param: $materials_type_id is $order_type
     */
    public function getDataBySellType($order_type, $order_type_text) {
        $this->description = 'Thu Bán '.$order_type_text;
        $this->type = GasMasterLookup::MASTER_TYPE_THU;
        switch ($order_type) {
            case Sell::ORDER_TYPE_BO_BINH:
                $this->master_lookup_id = GasMasterLookup::ID_THU_BAN_BO_BINH;
            break;
            case Sell::ORDER_TYPE_BAN_GAS_VO:
                $this->master_lookup_id = GasMasterLookup::ID_THU_BAN_VO;
                $this->description = 'Thu Bán Gas + Vỏ';
            break;
            case Sell::ORDER_TYPE_THE_CHAN:
                $this->master_lookup_id = GasMasterLookup::ID_THU_THE_CHAN;
            break;
            case Sell::ORDER_TYPE_THU_VO:
                $this->master_lookup_id = GasMasterLookup::ID_CHI_THE_CHAN;
                $this->description = 'Chi tiền thế chân';
                $this->type = GasMasterLookup::MASTER_TYPE_CHI;
            break;
            default:
                break;
        }
    }
    
    // xóa record by id
    public function deleteById($id) {
        if(empty($id)){
            return ;
        }
        GasCashBookDetail::model()->deleteByPk($id);
    }
    // May 09, 2017 delete all record by detail_type and release_date
    public function deleteByDetailType(){
        if(empty($this->detail_type) || empty($this->release_date)){
            return ;
        }
        $criteria=new CDbCriteria;
        if(is_array($this->agent_id)){
            $sParamsIn = implode(',', $this->agent_id);
            $criteria->addCondition("agent_id IN ($sParamsIn)");
        }elseif(!empty($this->agent_id)){
            $criteria->addCondition('agent_id=' . $this->agent_id);
        }
        $criteria->addCondition('detail_type=' . $this->detail_type);
        $criteria->addCondition("release_date='$this->release_date'");
        self::model()->deleteAll($criteria);        
    }
    
    /**
     * @Author: DungNT May 19, 2017
     * @Todo: cron sinh lại 1 lần nữa Quỹ hộ GĐ của các Agent run App
     */
    public static function cronGenAllAgentRunApp() {
        $from = time();
        $today = date('d-m-Y');
//        $today = MyFormat::modifyDays($today, 1, '-');
        $model = new GasCashBookDetail();
        $model->makeRecordHgdAllAgent(UsersExtend::getAgentRunAppGN(),  $today);
        
        $to = time();
        $second = $to-$from;
        $info = count(UsersExtend::getAgentRunAppGN())." Agent: GasCashBookDetail::cronGenAllAgentRunApp update Storecard HGD  done in: $second  Second  <=> ".($second/60)." Minutes";
        Logger::WriteLog($info);
    }
    
    /**
     * @Author: DungNT May 09, 2017
     * @Todo: cron insert cho cac agent run app cho chạy vòng for để run cho từng agent 1
     * @Param: $aAgentId là array multi agent hoặc array one [100]
     * @Param: $release_date 25-05-2017
     */
    public function makeRecordHgdAllAgent($aAgentId, $release_date) {
        /* 1. xóa những record Agent + type = 3 + date để insert lại từ đầu
         * 2. chạy báo cáo cho all đại lý
         * 3. sau đó foreach để sinh build sql insert
         *  test với 2 đại lý của 1 GN
         */
        $mCashbookDetail = new GasCashBookDetail();
        $mCashbookDetail->agent_id      = $aAgentId;
        $mCashbookDetail->release_date  = MyFormat::dateDmyToYmdForAllIndexSearch($release_date);
        $mCashbookDetail->detail_type   = GasCashBookDetail::D_TYPE_HGD;
        $mCashbookDetail->deleteByDetailType();

        $mSell = new Sell();
        $mSell->getDataInsert   = true;
        $mSell->agent_id        = $aAgentId;
        $mSell->date_from       = $release_date;// format 21-04-2017
        $mSell->date_to         = $release_date;
        $mSell->status          = Sell::STATUS_PAID;
        $aRes = $mSell->reportMultiAgent();
        foreach($aRes as $employee_maintain_id => $aData){
            $mSell->employee_maintain_id = $employee_maintain_id;
            StaAppFormat::hgd($mSell, $aData);
        }
        $this->makeRecordHgdRunInsert($mSell->aRowInsert);
    }

    public function makeRecordHgdRunInsert($aRowInsert) {
        $tableName = GasCashBookDetail::model()->tableName();
        $sql = "insert into $tableName (detail_type,
                    cash_book_id,
                    agent_id,
                    release_date,
                    release_date_bigint,
                    master_lookup_id,
                    type,
                    amount,
                    qty,
                    customer_id,
                    customer_parent_id,
                    sale_id,
                    type_customer,
                    employee_id,
                    name_employee,
                    created_date,
                    description
                    ) values ".implode(',', $aRowInsert);
        if(count($aRowInsert)>0)
            Yii::app()->db->createCommand($sql)->execute();
    }
    
    /** @Author: DungNT Fix May 10, 2017 ==> 12-20-2013
     * @Todo: tính dư đầu kỳ của đại lý hoặc nhân viên đến ngày cố định
     * @Param: $agent_id mã đại lý
     * @Param: $this->release_date ngày xem: ex 2013-12-26
     * @Return: number dư đầu kỳ
     */       
     public function calcOpeningBalance(){
        $criteria = new CDbCriteria();
        if(!empty($this->agent_id)){
            $criteria->addCondition('t.agent_id='.$this->agent_id);
        }
        if(!empty($this->employee_id)){
            $criteria->addCondition('t.employee_id='.$this->employee_id);
        }
        $criteria->addCondition("t.release_date < '$this->release_date'");
        
        $criteria->select = "sum(amount) as amount, t.type";
        $criteria->group = "t.type";
        $models = GasCashBookDetail::model()->findAll($criteria);
        $amountCost = $amountRevenue = 0;
        foreach($models as $item){
            if($item->type == GasMasterLookup::MASTER_TYPE_THU){
                $amountRevenue = $item->amount;
            }elseif($item->type == GasMasterLookup::MASTER_TYPE_CHI){
                $amountCost = $item->amount;
            }
        }
        return $amountRevenue - $amountCost;
     }
    
    /** @Author: DungNT Jun 19, 2017
     * @Todo: tính dư đầu kỳ hoặc phát sinh trong kỳ của đại lý
     * @Param: $agent_id mã list id đại lý
     * @Param: $nameVar: OPENING_BALANCE,INCURRED( phát sinh trong kỳ Incurred) 
     * @Return: number dư đầu kỳ
     */       
     public function calcMultiAgent(&$aRes, $nameVar){
        $criteria = new CDbCriteria();
        if(is_array($this->agent_id)){
            $sParamsIn = implode(',', $this->agent_id);
            $criteria->addCondition("t.agent_id IN ($sParamsIn)");
        }elseif(!empty($this->agent_id)){
            $criteria->addCondition('t.agent_id='.$this->agent_id);
        }
        if($nameVar == GasCashBookDetail::P_OPENING_BALANCE){
//            $criteria->addCondition("t.release_date < '$this->release_date'");DungNT Jul0819
            DateHelper::searchSmallerNotEqual($this->release_date, 'release_date_bigint', $criteria);
        }elseif($nameVar == GasCashBookDetail::P_INCURRED){
//            $criteria->addBetweenCondition("t.release_date", $this->date_from_ymd, $this->date_to_ymd);
            DateHelper::searchBetween($this->date_from_ymd, $this->date_to_ymd, 'release_date_bigint', $criteria, false);
        }
        $sta2 = new Sta2();
        $sta2->employeeMaintainSameCondition($criteria, 'agent_id');
        $criteria->select   = 'sum(amount) as amount, t.type, t.agent_id';
        $criteria->group    = 't.agent_id, t.type';
        $criteria->order    = 't.agent_id';
        $models = GasCashBookDetail::model()->findAll($criteria);
        foreach($models as $item){
            if($item->type == GasMasterLookup::MASTER_TYPE_THU){
                $aRes[$nameVar][$item->agent_id]['revenue'] = $item->amount;
            }elseif($item->type == GasMasterLookup::MASTER_TYPE_CHI){
                $aRes[$nameVar][$item->agent_id]['cost'] = $item->amount;
            }
        }
     }
     
    /** @Author: DungNT Aug 13, 2017
     * @Todo: tính dư đầu kỳ hoặc phát sinh trong kỳ của của all nhân viên
     * @Param: $nameVar: OPENING_BALANCE, INCURRED( phát sinh trong kỳ Incurred) 
     * @Return: number dư đầu kỳ
     */       
     public function calcMultiEmployee(&$aRes, $nameVar){
        $criteria = new CDbCriteria();
        if(is_array($this->agent_id)){
            $sParamsIn = implode(',', $this->agent_id);
            $criteria->addCondition("t.agent_id IN ($sParamsIn)");
        }elseif(!empty($this->agent_id)){
            $criteria->addCondition('t.agent_id='.$this->agent_id);
        }
        if(is_array($this->employee_id)){
            $sParamsIn = implode(',', $this->employee_id);
            $criteria->addCondition("t.employee_id IN ($sParamsIn)");
        }elseif(!empty($this->employee_id)){
            $criteria->addCondition('t.employee_id='.$this->employee_id);
        }
        if($nameVar == GasCashBookDetail::P_OPENING_BALANCE){
//            $criteria->addCondition("t.release_date < '$this->release_date'"); DungNT Jul0819
            DateHelper::searchSmallerNotEqual($this->release_date, 'release_date_bigint', $criteria);
        }elseif($nameVar == GasCashBookDetail::P_INCURRED){
//            $criteria->addBetweenCondition("t.release_date", $this->date_from_ymd, $this->date_to_ymd);
            DateHelper::searchBetween($this->date_from_ymd, $this->date_to_ymd, 'release_date_bigint', $criteria, false);
        }
        $sta2 = new Sta2();
        $sta2->employeeMaintainSameCondition($criteria, 'agent_id');
        
        $criteria->select   = 'sum(amount) as amount, t.type, t.agent_id, t.employee_id';
        $criteria->group    = 't.employee_id, t.type';
        $criteria->order    = 't.agent_id';
        $models = GasCashBookDetail::model()->findAll($criteria);
        foreach($models as $item){
            if($item->type == GasMasterLookup::MASTER_TYPE_THU){
                $aRes[$nameVar][$item->employee_id]['revenue'] = $item->amount;
            }elseif($item->type == GasMasterLookup::MASTER_TYPE_CHI){
                $aRes[$nameVar][$item->employee_id]['cost'] = $item->amount;
            }
            $aRes['ARR_EMPLOYEE'][$item->employee_id] = $item->employee_id;
        }
     }
     
     /** @Author: DungNT Fix May 10, 2017 ==> 12-20-2013
     * @Todo: sổ quỹ: tính dư đầu kỳ của đại lý 
     */       
    public function getOpeningBalanceOfAgent($needMore=array()){
        $openingInput = 0;
        if(!empty($this->agent_id)){
            $mUser = Users::model()->findByPk($this->agent_id);
            $openingInput = $mUser->beginning;
        }
        if(!empty($this->employee_id)){
            $openingInput = 0;
        }
        
        $openingBalance = $this->calcOpeningBalance();
        return $openingInput+$openingBalance;
    }
    
    /** @Author: DungNT Fix May 10, 2017 ==> 12-20-2013
     * @Todo: lấy dữ liệu daily nhập sổ quỹ trong 1 khoảng ngày của đại lý hoặc nhân viên
     * @Return: array model $mCashBookDetail gas_gas_cash_book_detail
     */       
    public function getDaily(){
        $criteria = new CDbCriteria();
        if(!empty($this->agent_id)){
            $criteria->addCondition('t.agent_id='.$this->agent_id);
        }
        if(!empty($this->employee_id)){
            $criteria->addCondition('t.employee_id='.$this->employee_id);
        }
        $criteria->addBetweenCondition("t.release_date", $this->date_from_ymd, $this->date_to_ymd);
        $criteria->order = 't.release_date ASC, t.type ASC, t.id ASC';
        return GasCashBookDetail::model()->findAll($criteria);
    }
    
    /** @Author: DungNT May 10, 2017
     * @Todo: sinh tự động report cashbook hộ GĐ không từ table Sell
     */
    public function autoGenReportHgd() {
        GasCheck::randomSleep();
        $mMonitorUpdate = new MonitorUpdate();
        $mMonitorUpdate->agent_id   = $this->agent_id;
        $mMonitorUpdate->type       = MonitorUpdate::TYPE_CASHBOOK;
        $today = date('Y-m-d');
        if($mMonitorUpdate->cashbookAllowGenReport() && $this->date_to_ymd == $today){
            $from = time();
            $model = new GasCashBookDetail();
            $model->makeRecordHgdAllAgent([$this->agent_id],  $this->release_date);
            if(!is_null($mMonitorUpdate->modelMonitorOld)){
                $mMonitorUpdate->modelMonitorOld->setNextTimeUpdate();
            }
            
            $to = time();
            $second = $to-$from;
            $info = "App GN GasCashBookDetail autoGenReportHgd Agent: $this->agent_id  done in: $second  Second  <=> ".($second/60)." Minutes";
//            Logger::WriteLog($info);// Close Log Aug0817 
        }
    }
    
    /** @Author: DungNT Jun 19, 2017
     * @Todo: get array id agent by array province
     */
    public function getAgentByProvince() {
        if($this->typeReport == GasCashBookDetail::TARGET_EMPLOYEE_DEBIT){
            return [GasCheck::KHO_86_NCV_DKMN];
        }elseif($this->typeReport == GasCashBookDetail::TARGET_SALE){
            return GasCheck::getAgentNotGentAuto();
        }
        $needMore = ['aProvinceId'=>$this->province_id, 'only_id_model'=>1, 'gender'=>Users::IS_AGENT];
        $aAgent = Users::getArrObjecByRoleGroupByProvince(ROLE_AGENT, $needMore);
        return CHtml::listData($aAgent, 'id', 'id');
    }
    public function viewAllFormatParams() {
        $this->date_from_ymd    = MyFormat::dateDmyToYmdForAllIndexSearch($this->date_from);
        $this->date_to_ymd      = MyFormat::dateDmyToYmdForAllIndexSearch($this->date_to);
        $this->release_date     = $this->date_from_ymd;
    }
    /** @Author: DungNT Jun 19, 2017
     * @Todo: View all cashbook of agent
     */
    public function viewAll() {
        /* 1. get agent theo province_id, format params
         * 1.1 tính tồn đầu đến ngày cho all agent
         * 2. tính thu chi cho từng agent
         * 3. render tồn cuối ở view
         */
        $this->agent_id = $this->getAgentByProvince();
        $this->viewAllFormatParams();
        $aRes = [];
        $this->calcMultiAgent($aRes, GasCashBookDetail::P_OPENING_BALANCE);
        $this->calcMultiAgent($aRes, GasCashBookDetail::P_INCURRED);
        $_SESSION['data-excel'] = $aRes;
        $_SESSION['data-model'] = $this;
        return $aRes;
    }
    
    /** @Author: DungNT Aug 13, 2017
     * @Todo: View all cashbook of employee
     */
    public function viewAllEmployee() {
        /* 1. get agent theo province_id, format params
         * 1.1 tính tồn đầu đến ngày cho all employee
         * 2. tính thu chi cho từng employee
         * 3. render tồn cuối ở view
         */
//        $this->agent_id = $this->getAgentByProvince();// Aug2118 bỏ đoạn limit agent này đi,vì NV có thể thu chi ở nhiều Agent khác nhau, khi để thế này sẽ thống kê sai
        $this->viewAllFormatParams();
        if($this->typeReport == GasCashBookDetail::TARGET_EMPLOYEE_DEBIT){
            $this->employee_id = $this->getArrayEmployeeDebit();
        }
        $aRes = [];
        $this->calcMultiEmployee($aRes, GasCashBookDetail::P_OPENING_BALANCE);
        $this->calcMultiEmployee($aRes, GasCashBookDetail::P_INCURRED);
        if(isset($aRes['ARR_EMPLOYEE'])){
            $needMore = [];
            if($this->typeReport == GasCashBookDetail::TARGET_EMPLOYEE_OFF){
                $needMore['GetAll']     = 1;
                $needMore['condition']  = 't.status=' . STATUS_INACTIVE;
            }
            $aRes['ARR_USER'] = Users::getArrObjectUserByRole('', $aRes['ARR_EMPLOYEE'], $needMore);
        }
        $_SESSION['data-excel'] = $aRes;
        $_SESSION['data-model'] = $this;
        return $aRes;
    }
    
    /** @Author: DungNT Dec 07, 2017
     * @Todo: calc inventory cash of all employee
     * @param: date_from, date_to like /admin/gascashbook/viewAll/target/2
     */
    public function getInventoryCashAllEmployee($needMore=[]) {
        try{
            $aData          = $this->viewAllEmployee();
            if(!isset($aData['ARR_USER'])){
                return [];
            }
            $aRoleCheck     = [ROLE_EMPLOYEE_MAINTAIN, ROLE_EMPLOYEE_MARKET_DEVELOPMENT];
            $aOpening       = $aData[GasCashBookDetail::P_OPENING_BALANCE];
            $aIncurred      = isset($aData[GasCashBookDetail::P_INCURRED]) ? $aData[GasCashBookDetail::P_INCURRED] : [];
            $aModelUser     = $aData['ARR_USER'];
            $mAppCache      = new AppCache();
            $aAgent         = $mAppCache->getAgent(); 
            $aRes           = [];
            $aAgentNotReport = isset($needMore['ArrAgentIdNotFind']) ? $needMore['ArrAgentIdNotFind'] : [];
            foreach($aModelUser as $employee_id => $mUser):
                $agent_id       = $mUser->getAgentOfEmployee();
                if($this->isInventoryAlert && (in_array($employee_id, $this->aUidGoBank) || in_array($agent_id, $aAgentNotReport))){
                    continue ;
                }
                if($this->isInventoryAlert && !in_array($mUser->role_id, $aRoleCheck)){
                    continue ;
                }
                
                $aInfo          = isset($aOpening[$employee_id]) ? $aOpening[$employee_id] : ['revenue'=>0, 'cost'=>0];
                $aInfo['cost']  = isset($aInfo['cost']) ? $aInfo['cost'] : 0;
                $aInfo['revenue']  = isset($aInfo['revenue']) ? $aInfo['revenue'] : 0;
                $open           = $aInfo['revenue'] - $aInfo['cost'];
                $amountRevenue  = isset($aIncurred[$employee_id]['revenue']) ? $aIncurred[$employee_id]['revenue'] : 0;
                $amountCost     = isset($aIncurred[$employee_id]['cost']) ? $aIncurred[$employee_id]['cost'] : 0;
                $end            = $open + $amountRevenue - $amountCost;

                $temp = [];
                $temp['agent_id']       = $agent_id;
                $temp['agentName']      = isset($aAgent[$agent_id]) ? $aAgent[$agent_id]['first_name'] : '';
                $temp['employeeName']   = $mUser->first_name;
                $temp['opening']        = ActiveRecord::formatCurrencyRound($open);
                $temp['in']             = ActiveRecord::formatCurrencyRound($amountRevenue);
                $temp['out']            = ActiveRecord::formatCurrencyRound($amountCost);
                $temp['closing']        = ActiveRecord::formatCurrencyRound($end);
                if($end > $this->getMinCashAlert()){
                    $this->aDataAlert[$employee_id]                     = $temp;
                    $this->aDataSort[$mUser->province_id][$employee_id] = $end;
                }
                if($open > $this->getMinCashCheckCheat() && $amountRevenue == 0 && $amountCost == 0){
                    $this->aDataNegativeCashbook[$employee_id]          = $temp;
                    $this->aDataNegativeCashbookSort[$mUser->province_id][$employee_id]  = $end;
                }
                
                // những bc khác có thể thêm biến khác để lấy toàn bộ tồn tiền của nhân viên
            endforeach;
            
            foreach($this->aDataSort as $province_id => $aInfo){
                arsort($aInfo);
                $this->aDataSort[$province_id] = $aInfo;
            }
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }
    
    /** @Author: DungNT Apr 03, 2018
     *  @Todo: get tiền tồn cuối kỳ của 1 user
     **/
    public function getClosingOneUser() {
        $aData          = $this->viewAllEmployee();
        $aOpening       = isset($aData[GasCashBookDetail::P_OPENING_BALANCE]) ? $aData[GasCashBookDetail::P_OPENING_BALANCE] : [];
        $aIncurred      = isset($aData[GasCashBookDetail::P_INCURRED]) ? $aData[GasCashBookDetail::P_INCURRED] : [];
        $aModelUser     = $aData['ARR_USER'];
        
        $aInfo          = isset($aOpening[$this->employee_id]) ? $aOpening[$this->employee_id] : ['revenue'=>0, 'cost'=>0];
        $aInfo['cost']  = isset($aInfo['cost']) ? $aInfo['cost'] : 0;
        $aInfo['revenue']  = isset($aInfo['revenue']) ? $aInfo['revenue'] : 0;
        $open           = $aInfo['revenue'] - $aInfo['cost'];
        $amountRevenue  = isset($aIncurred[$this->employee_id]['revenue']) ? $aIncurred[$this->employee_id]['revenue'] : 0;
        $amountCost     = isset($aIncurred[$this->employee_id]['cost']) ? $aIncurred[$this->employee_id]['cost'] : 0;
        $end            = $open + $amountRevenue - $amountCost;
        return $end;
    }
    
    /** @Author: DungNT Feb 14, 2019
     *  @Todo: tinh toan so tien phat khi bi loi am Quy
     **/
    public function getMoneyPunishNegativeCashbook($totalAmount) {
        return round(($this->getPercentPunishNegativeCashbook()/100)*$totalAmount);
    }
    
    /** 
     * @Author: LocNV Jul 3, 2019
     * @Todo: tính dư đầu kỳ hoặc phát sinh trong kỳ của của mot nhan vien
     * @Param: $nameVar: OPENING_BALANCE, INCURRED( phát sinh trong kỳ Incurred) 
     * @param array $user_id User id
     * @Return: number dư đầu kỳ
     */       
    public function calcOneEmployee(&$aRes, $nameVar, $user_id){
        $criteria = new CDbCriteria();
        //them dieu kien employee_id
        if(is_array($user_id)){
            $sParamsIn = implode(',', $user_id);
            $criteria->addCondition("t.employee_id IN ($sParamsIn)");
        }elseif(!empty($user_id)){
            $criteria->addCondition('t.employee_id='.$user_id);
        }
        if($nameVar == GasCashBookDetail::P_OPENING_BALANCE){
            DateHelper::searchSmallerNotEqual($this->release_date, 'release_date_bigint', $criteria);
//            $criteria->addCondition("t.release_date < '$this->release_date'");
        }elseif($nameVar == GasCashBookDetail::P_INCURRED){
//            $criteria->addBetweenCondition("t.release_date", $this->date_from_ymd, $this->date_to_ymd);
            DateHelper::searchBetween($this->date_from_ymd, $this->date_to_ymd, 'release_date_bigint', $criteria, false);
        }
        $sta2 = new Sta2();
        $sta2->employeeMaintainSameCondition($criteria, 'agent_id');
        
        //group by theo agent_id
        $criteria->select   = 'sum(amount) as amount, t.type, t.agent_id, t.employee_id';
        $criteria->group    = 't.agent_id, t.employee_id, t.type';
        $models = GasCashBookDetail::model()->findAll($criteria);
        //tinh tong revenue va cost theo tung agent_id
        foreach($models as $item){
            if($item->type == GasMasterLookup::MASTER_TYPE_THU){
                $aRes[$nameVar][$item->employee_id][$item->agent_id]['revenue'] = $item->amount;
            }elseif($item->type == GasMasterLookup::MASTER_TYPE_CHI){
                $aRes[$nameVar][$item->employee_id][$item->agent_id]['cost'] = $item->amount;
            }
            $aRes['ARR_USER'][$item->employee_id][$item->agent_id] = $item->agent_id;
        }
    }
    
    /** @Author: LocNV Jul 3, 2019
     * @Todo: View cashbook of one employee
     */
    public function viewOneEmployee($user_id) {
        $this->viewAllFormatParams();
        $aRes = [];
        $this->calcOneEmployee($aRes, GasCashBookDetail::P_OPENING_BALANCE, $user_id);
        $this->calcOneEmployee($aRes, GasCashBookDetail::P_INCURRED, $user_id);        
        return $aRes;
    }

    /** @Author: LocNT Jul 3, 2019 
     *  @Todo: get tiền tồn cuối kỳ của 1 user theo đại lý
     *  @param: array $user_id mang cac user_id
     *  @return:
     * array (
     *  49173 (user_id) =>
     *      array(
     *          100 (agent_id) => array("agentName" => "Dai Ly Quan 2", "closing" => 910000),
     *          113 (agent_id) => array("agentName" => "Dai Ly Quan 3", "closing" => 650000),
     *      )
     *  )
     **/
    public function getClosingByAgent($user_id) {
        $aData = $this->viewOneEmployee($user_id);

        $aRes           = [];
        $mAppCache      = new AppCache();
        $aModelUser     = isset($aData['ARR_USER']) ? $aData['ARR_USER'] : [];   //mang bao gom cac agent
        $aAgent         = $mAppCache->getAgent();         //lay danh sach cac agent
       
        foreach ($aModelUser as $employee_id => $arrAgent) {
            //lay du lieu opening va incurred theo employee_id
            $aOpening   = $aData[GasCashBookDetail::P_OPENING_BALANCE][$employee_id];
            $aIncurred  = isset($aData[GasCashBookDetail::P_INCURRED][$employee_id]) ? $aData[GasCashBookDetail::P_INCURRED][$employee_id] : [];
            
            $aTempSort = [];
            //quet cac agent_id de tinh revenue va cost va luu vao mang moi
            foreach ($arrAgent as $agent_id) {
                $aInfo              = isset($aOpening[$agent_id]) ? $aOpening[$agent_id] : ['revenue' => 0, 'cost' => 0];
                $aInfo['cost']      = isset($aInfo['cost']) ? $aInfo['cost'] : 0;
                $aInfo['revenue']   = isset($aInfo['revenue']) ? $aInfo['revenue'] : 0;
                $open               = $aInfo['revenue'] - $aInfo['cost'];
                $amountRevenue      = isset($aIncurred[$agent_id]['revenue']) ? $aIncurred[$agent_id]['revenue'] : 0;
                $amountCost         = isset($aIncurred[$agent_id]['cost']) ? $aIncurred[$agent_id]['cost'] : 0;
                $end                = $open + $amountRevenue - $amountCost;
//                if($end <= $this->getMinCashForViettelPay()){
                if($end == 0){
                    continue;
                }

                $aRes[$employee_id][$agent_id]['agentName']     = $aAgent[$agent_id]['first_name'];
                $aRes[$employee_id][$agent_id]['agentNameVi']   = MyFunctionCustom::remove_vietnamese_accents($aAgent[$agent_id]['first_name']);
                $aRes[$employee_id][$agent_id]['closing']       = $end;
                $aTempSort[$agent_id] = $end;
            }
            $this->sortClosing($aRes, $employee_id, $aTempSort);
        }
        return $aRes;
    }
    
    /** @Author: DungNT Jul 10, 2019
     *  @Todo: closing sort high to low
     **/
    public function sortClosing(&$aRes, $employee_id, $aTempSort) {
        arsort($aTempSort);// closing sort high to low
        $aTemp = [];
        foreach($aTempSort as $agent_id => $closing):
            $aTemp[$agent_id]['agentName']      = $aRes[$employee_id][$agent_id]['agentName'];
            $aTemp[$agent_id]['agentNameVi']    = $aRes[$employee_id][$agent_id]['agentNameVi'];
            $aTemp[$agent_id]['closing']        = $aRes[$employee_id][$agent_id]['closing'];
        endforeach;
        $aRes[$employee_id] = $aTemp;
    }

}