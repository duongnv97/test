<?php

/**
 * This is the model class for table "{{_gas_order_promotion_accounting}}".
 *
 * The followings are the available columns in table '{{_gas_order_promotion_accounting}}':
 * @property string $id
 * @property string $user_id_delivery
 * @property integer $materials_type_id
 * @property integer $materials_id
 * @property string $qty
 * @property string $created_date
 */
class GasOrderPromotionAccounting extends CActiveRecord
{
    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return GasOrderPromotionAccounting the static model class
     */
    public static function model($className=__CLASS__)
    {
            return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
            return '{{_gas_order_promotion_accounting}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        return array(
            array('id, user_id_delivery, materials_type_id, materials_id, qty, created_date', 'safe'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        return array();
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'user_id_delivery' => 'User Id Delivery',
            'materials_type_id' => 'Materials Type',
            'materials_id' => 'Materials',
            'qty' => 'Qty',
            'created_date' => 'Created Date',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        $criteria=new CDbCriteria;
        $criteria->compare('t.qty',$this->qty,true);

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
            'pagination'=>array(
                'pageSize'=> Yii::app()->user->getState('pageSize',Yii::app()->params['defaultPageSize']),
            ),
        ));
    }
    
    /**
     * @Author: ANH DUNG Mar 11, 2016
     * @Todo: xóa dữ liệu trc khi insert theo nv kế toán và ngày nhận
     */
    public static function deleteByUserDeliveryAndDate() {
        $cUid = Yii::app()->user->id;
        $today = date("Y-m-d");
        $yesterday = MyFormat::modifyDays($today, 1, "-");
        $criteria = new CDbCriteria();
        $criteria->compare("user_id_delivery", $cUid);
        $criteria->addBetweenCondition("created_date", $yesterday, $today);
        self::model()->deleteAll($criteria);
    }

}