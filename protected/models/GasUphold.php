<?php

/**
 * This is the model class for table "{{_gas_uphold}}".
 *
 * The followings are the available columns in table '{{_gas_uphold}}':
 * @property string $id
 * @property string $code_no
 * @property integer $type
 * @property string $customer_id
 * @property integer $level_type
 * @property string $employee_id
 * @property integer $type_uphold
 * @property string $content
 * @property string $contact_person
 * @property string $contact_tel
 * @property integer $status
 * @property string $uid_login
 * @property string $created_date
 * @property string $has_read
 */
class GasUphold extends BaseSpj
{
    const TYPE_DINH_KY  = 1; // 1: Bảo trì định kỳ, 2: xử lý sự cố
    const TYPE_SU_CO    = 2;
    
    const LEVEL_NHO         = 1; //1:Nhỏ, 2: lớn, 3: đặc thù, 4: quan trọng
    const LEVEL_LON         = 2;
    const LEVEL_DAC_THU     = 3;
    const LEVEL_QUAN_TRONG  = 4;
    
    const UPHOLD_XI_GAS         = 1; // 1: xì gas, 2: bảo trì bếp, 3: kt hệ thống, 4: hư bếp, 5 đi lại đường ống
    const UPHOLD_BT_BEP         = 2; 
    const UPHOLD_CHECK_SYSTEM   = 3; 
    const UPHOLD_HU_BEP         = 4; 
    const UPHOLD_FIX_DUONG_ONG  = 5; 
    const UPHOLD_OTHER          = 6; 
    const UPHOLD_TEST_APP       = 7; 
    
    const STATUS_NEW        = 1; // 1: mới, 2: xử lý, 3: hoàn thành, 4: hoãn, 5: hủy
    const STATUS_HANLDE     = 2; // 2: xử lý
    const STATUS_COMPLETE   = 3;// hoan thanh
    const STATUS_DELAY      = 4;//
    const STATUS_CANCEL     = 5; // forward chuyển cho bảo trì khác
    const STATUS_WRONG_CALL = 6; // báo sai sự cố
    const STATUS_HANLDE_LONG= 7; // 7: xử lý dài ngày FIX
    const STATUS_CUSTOMER_CHECKIN = 8; // 8: Đến khách hàng
    
    public $daysBetweenTwoReply = 15, $created_date_from, $created_date_to, $MAX_ID, $autocomplete_name, $autocomplete_name_second, $autocomplete_name_third, $autocomplete_name_sale, $mReply, $GoToCustomer, $MonthKhongBaoTri=0;
    public $aProvinceId = [], $next_uphold_from, $next_uphold_to, $last_purchase_from, $last_purchase_to, $khong_baotri_from, $khong_baotri_to;
    
    // Now 27, 2015
    const SCHEDULE_CO_DINH              = 1;// cố định ngày 1,2 trong tháng
    const SCHEDULE_KHONG_CO_DINH        = 2;
    const SCHEDULE_KHONG_CO_DINH_2_THANG = 3;
    const SCHEDULE_KHONG_CO_DINH_3_THANG = 4;
    const SCHEDULE_BIG_CUSTOMER         = 5;// jul 04, 2016 kh lớn
    const SCHEDULE_IGNORE_CUSTOMER      = 6;// jul 07, 2016 KH bỏ 
    public static $SCHEDULE_IGNORE      = array(GasUphold::SCHEDULE_IGNORE_CUSTOMER);
    
    public $day_checkbox;
    const NOTIFY_TIME_ALERT = 10; // số phút giãn cách mỗi lần notify
//    const CUSTOMER_LIMIT_CREATE = 2; // limit customer for one create upholdSchedule
    const CUSTOMER_LIMIT_CREATE = 100; // limit customer for one create upholdSchedule
    const TIME_CHECK_DINHKY     = 24; // số giờ cho phép hoàn thành đinh kỳ giãn cách với sự cố
    
    public static $ROLE_EMPLOYEE_BAOTRI = array(ROLE_E_MAINTAIN,ROLE_EMPLOYEE_MAINTAIN,ROLE_SALE, ROLE_MONITOR_AGENT, 
        ROLE_MONITORING_MARKET_DEVELOPMENT, ROLE_EMPLOYEE_MARKET_DEVELOPMENT
    );
//    public static $ROLE_EMPLOYEE_SEARCH_AUTOCOMPLETE = array(ROLE_EMPLOYEE_MARKET_DEVELOPMENT, ROLE_E_MAINTAIN, ROLE_MONITORING_MARKET_DEVELOPMENT, ROLE_HEAD_OF_MAINTAIN, ROLE_SALE, ROLE_EMPLOYEE_MAINTAIN, ROLE_MONITOR_AGENT);
    public static $ROLE_EMPLOYEE_SEARCH_AUTOCOMPLETE = array(ROLE_SALE, ROLE_EMPLOYEE_MARKET_DEVELOPMENT, ROLE_EMPLOYEE_MAINTAIN, ROLE_MONITORING_MARKET_DEVELOPMENT, ROLE_E_MAINTAIN,  ROLE_HEAD_OF_MAINTAIN);
    public $report_month, $report_year, $date_from, $date_to, $limit_report;
    public $daysNotPurchase; // DuongNV Sep0619 search số ngày KH ko lấy hàng
    public $mEmployee = null;// Jun 24, 2016 xử lý keep model employee khi send SMS

    // APR 11, 2016 FOR RATING BAO TRI ON APP
    const RATE_STATUS_GOOD  = 1;
    const RATE_STATUS_NORMAL = 2;
    const RATE_STATUS_BAD   = 3;
    
    const RATE_TYPE_THAI_DO = 1;// THAI DO NV
    const RATE_TYPE_TIME    = 2;// THOI GIAN NV DEN
    const RATE_TYPE_QUALITY = 3;// CHAT LUONG SUA CHUA
    const RATE_TYPE_DOING   = 4;// TAC PHONG NV
    
    const REQUEST_BY_BOSS = 1;// Người yc bảo trì là chủ quán
    const REQUEST_BY_MANAGER = 2;// người quản lý
    const REQUEST_BY_TECH = 3;// nv kỹ thuật
    const REQUEST_BY_OTHER = 4;// nv khác
    // APR 11, 2016 FOR RATING BAO TRI ON APP
    // JUN 07, 2016 trạng thái bảo trì định kỳ
    const STATUS_UPHOLD_RUNNING = 1;// Còn bảo trì
    const STATUS_UPHOLD_OFF     = 2;// Không còn bảo trì
    // JUN 07, 2016 trạng thái bảo trì định kỳ
    
    public function getArrayType() {// Sep 05, 2016
        return array(
            GasUphold::TYPE_DINH_KY => 'Bảo trì định kỳ ',
            GasUphold::TYPE_SU_CO   => 'Bảo trì sự cố',
        );
    }
    public function getTypeText() {// Sep 05, 2016
        $aType = $this->getArrayType();
        return $aType[$this->type];
    }
        
    public function getArrayStatusSchedule() {// JUN 07, 2016
        return array(
            GasUphold::STATUS_UPHOLD_RUNNING    => 'Bảo trì',
            GasUphold::STATUS_UPHOLD_OFF        => 'Ngưng bảo trì',
        );
    }
    public function getArrayCheckSchedule() {
        return array(
            GasUphold::STATUS_UPHOLD_RUNNING    => 'Đi bảo trì',
            GasUphold::STATUS_UPHOLD_OFF        => 'Chưa đi bảo trì',
        );
    }
    public static function getArrayRateStatus() {// Apr 15, 2016
        return array(
            GasUphold::RATE_STATUS_GOOD     => 'Hài lòng',
            GasUphold::RATE_STATUS_NORMAL   => 'Bình thường',
            GasUphold::RATE_STATUS_BAD      => 'Không hài lòng',
        );
    }
    
    public static function getArrayRateType() {
        return array(
            GasUphold::RATE_TYPE_THAI_DO => 'Thái độ, Tác phong nhân viên',
//            GasUphold::RATE_TYPE_TIME => 'Thời gian nhân viên đến',
            GasUphold::RATE_TYPE_QUALITY => 'Tính chuyên nghiệp',
//            GasUphold::RATE_TYPE_DOING => 'Tác phong nhân viên',
        );
    }
    
    public static function getArrayRequestBy() {
        return array(
            GasUphold::REQUEST_BY_BOSS      => 'Chủ quán',
            GasUphold::REQUEST_BY_MANAGER   => 'Người quản lý',
            GasUphold::REQUEST_BY_TECH      => 'Nhân viên kỹ thuật',
            GasUphold::REQUEST_BY_OTHER     => 'Khác',
        );
    }
    
    // Get array người quản lý khu vực bảo trì
    public function getArrayManage() { // Apr 21, 2016
        return array(
            GasLeave::UID_DIRECTOR_BUSSINESS   => 'Bùi Đức Hoàn',
            GasLeave::UID_HEAD_GAS_BO   => 'Phạm Văn Đức',
            GasLeave::PHUC_HV           => 'Huỳnh Văn Phúc',
            GasConst::UID_TRUNG_HV      => 'Hà Văn Trung',
            GasConst::UID_HA_VT         => 'Vương Thanh Hà',
            GasSupportCustomer::UID_QUY_PV  => 'Phan Văn Quý',
            868857                      => 'Lê Xuân Đạt',
            GasConst::UID_LAM_KD        => 'Kiều Duy Lâm',
        );
    }
    
    /** @Author: DungNT Apr 21, 2016 */
    public function getManageName() {
        $aManage = $this->getArrayManage();
        return isset($aManage[$this->manage_id]) ? $aManage[$this->manage_id] : '';
    }
    public function getUidViewAll() {
        return [
            GasConst::UID_PHONG_NU,
        ];
    }
    
    /** @Author: DungNT Now 27, 2015
      * @Todo: get array loai bt dinh ky
     */
    public function getArrayScheduleType() {
        return array(
            GasUphold::SCHEDULE_CO_DINH             => 'Ngày cố định',
            GasUphold::SCHEDULE_KHONG_CO_DINH       => '1 tháng 1 lần',
            GasUphold::SCHEDULE_KHONG_CO_DINH_2_THANG => '2 tháng 1 lần',
            GasUphold::SCHEDULE_KHONG_CO_DINH_3_THANG => '3 tháng 1 lần',
            GasUphold::SCHEDULE_BIG_CUSTOMER        => 'Khách hàng cố định',
            GasUphold::SCHEDULE_IGNORE_CUSTOMER     => 'Khách hàng khai tử',
        );
    }
    
    public function getArrayLimit() {
        return array(
            50 => 50,
            100 => 100,
            150 => 150,
            200 => 200,
        );
    }
    
    /**
     * @Author: DungNT Feb 16, 2016
     */
    public function getArrayScheduleTypeText() {
        $aType = $this->getArrayScheduleType();
        return isset($aType[$this->schedule_type]) ? $aType[$this->schedule_type] : '';
    }
    
    // lấy số ngày cho phép user cập nhật
    public function getDayAllowUpdate(){
        return Yii::app()->params['days_update_uphold'];
    }
    
    /**
     * @Author: DungNT Oct 21, 2015
     * @Todo: get array dropdown
     */
    public function getArrayLevel() {
        return array(
          GasUphold::LEVEL_NHO => 'Nhỏ',
          GasUphold::LEVEL_LON => 'Lớn',
          GasUphold::LEVEL_DAC_THU => 'Đặc Thù',
          GasUphold::LEVEL_QUAN_TRONG => 'Quan Trọng',
        );
    }
    
    const READ_NO = 0;
    const READ_YES = 1;
    const READ_ALL = 2;    
    public function getArrayHasRead() {
        return array(
            GasUphold::READ_NO => 'Chưa',
            GasUphold::READ_YES => 'Đã xác nhận',
        );
    }
    
    /**
     * @Author: DungNT Oct 21, 2015
     * @Todo: get array dropdown
     */
    public function getArrayUpholdType() {
        return array(
          GasUphold::UPHOLD_BT_BEP          => 'Bảo Trì Bếp',
          GasUphold::UPHOLD_HU_BEP          => 'Hư Bếp',
          GasUphold::UPHOLD_XI_GAS          => 'Xì Gas',
          GasUphold::UPHOLD_CHECK_SYSTEM    => 'Kiểm Tra Hệ Thống',
          GasUphold::UPHOLD_FIX_DUONG_ONG   => 'Đi Lại Đường Ống',
          GasUphold::UPHOLD_OTHER           => 'Khác',
          GasUphold::UPHOLD_TEST_APP        => 'Test app',
        );
    }
    

    
    /** @Author: DungNT Aug 04, 2017
     * @Todo: uid cho phép cập nhật
     * Hi Dũng
Kv Tp HCM giao cho Đức
Kv Dong Nai-BD giao cho Phúc TGS
Kv Long Cuyen giao cho Trung
Kv Can Thơ-VL van giao cho Quý
Kv Bình Đinh giao Hà
     */
    public function getUidUpdateSchedule() {
        return [
            GasLeave::UID_HEAD_GAS_BO,
            GasLeave::PHUC_HV,
//            GasConst::UID_TRUNG_HV,
            GasSupportCustomer::UID_QUY_PV,
//            GasConst::UID_HA_VT,
//            GasConst::UID_PHUONG_TT,
//            GasConst::UID_VAN_TV,
            GasLeave::UID_DIRECTOR_BUSSINESS,
            GasLeave::UID_CHIEF_MONITOR,// Trung Hieu
            GasConst::UID_PHUONG_ND,// Dinh Phuong
            
        ];
    }
    
    /**
     * @Author: DungNT Dec 02, 2015
     * @Todo: get các status sau khi NV Bảo trì reply thì nếu là những status này sẽ dc notify
     */
    public function getStatusNeedNotify(){
        return array(GasUphold::STATUS_HANLDE, GasUphold::STATUS_COMPLETE);
    }
    
    /**
     * @Author: DungNT Oct 21, 2015
     * @Todo: get array dropdown
     * for api UpholdReplyLabel 
     */
    public function getArrayStatus($empty = false) {
        if($empty){// for Api
            return array(
                '' => 'Chọn Trạng Thái',
                GasUphold::STATUS_NEW           => 'Mới',
                GasUphold::STATUS_HANLDE        => 'Xử lý',
                GasUphold::STATUS_CUSTOMER_CHECKIN => 'Đến khách hàng',
                GasUphold::STATUS_COMPLETE      => 'Hoàn thành',
//                GasUphold::STATUS_DELAY => 'Hoãn',
                GasUphold::STATUS_CANCEL        => 'Yêu cầu chuyển',
                GasUphold::STATUS_HANLDE_LONG   => 'Xử lý dài ngày',
//                GasUphold::STATUS_WRONG_CALL => 'Báo sai sự cố',
              );
        }
        return array(
            GasUphold::STATUS_NEW               => 'Mới',
            GasUphold::STATUS_HANLDE            => 'Xử Lý',
            GasUphold::STATUS_CUSTOMER_CHECKIN  => 'Đến khách hàng',
            GasUphold::STATUS_COMPLETE          => 'Hoàn Thành',
//            GasUphold::STATUS_DELAY => 'Hoãn',
            GasUphold::STATUS_CANCEL            => 'Yêu cầu chuyển',
            GasUphold::STATUS_HANLDE_LONG       => 'Xử lý dài ngày',
//            GasUphold::STATUS_WRONG_CALL      => 'Báo sai sự cố',
        );
    }
    // get text view at list
    public function getTextLevel(){
        $aValue = $this->getArrayLevel();
        return isset($aValue[$this->level_type]) ? $aValue[$this->level_type]: '';
    }
    // get text view at list
    public function getTextUpholdType(){
        $aValue = $this->getArrayUpholdType();
        return isset($aValue[$this->type_uphold]) ? $aValue[$this->type_uphold]: '';
    }
    // get text view at list
    public function getTextStatus($status_ex=''){
        $statusValue = $this->status;
        if(!empty($status_ex)){
            $statusValue = $status_ex;
        }
        $aValue = $this->getArrayStatus();
        return isset($aValue[$statusValue]) ? $aValue[$statusValue]: '';
    }
    public function getWebCodeNo(){
        $res = $this->code_no;
        $cRole = MyFormat::getCurrentRoleId();
        $cUid = MyFormat::getCurrentUid();
        if($cRole == ROLE_ADMIN){
            $res .= "<br><b>[$this->code_complete]</b>";
        }
        $aUidAllow = [GasConst::UID_HA_PT, GasConst::UID_PHUONG_NT, GasLeave::UID_HEAD_GAS_BO];
        if(in_array($cUid, $aUidAllow)){
            $res .= "<br><b>[$this->code_complete]</b>";
        }
        return $res;
    }
    
    
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
            return '{{_gas_uphold}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        return array(
            array('customer_id, employee_id, type_uphold, content, contact_person, contact_tel', 'required', 'on'=>'create,update'),
//            array('customer_id', 'required', 'on'=>'create,update'), // only for test
            array('manage_id, schedule_type, customer_id, employee_id, day_checkbox', 'required', 'on'=>'createSchedule1'), // Bt const SCHEDULE_CO_DINH = 1; chon list day
            array('manage_id, schedule_type, customer_id, employee_id, schedule_in_month', 'required', 'on'=>'createSchedule2'),// const SCHEDULE_KHONG_CO_DINH = 2;
            array('manage_id, schedule_type, customer_id, employee_id', 'required', 'on'=>'createSchedule3,createSchedule4'),// const SCHEDULE_KHONG_CO_DINH_2_THANG = 3, SCHEDULE_KHONG_CO_DINH_3_THANG = 4
            
//            array('customer_id, employee_id, type_uphold, contact_tel', 'required', 'on'=>'ApiCreate'),// Sep2518 bỏ employee_id để KH tiềm năng tạo
            array('customer_id, type_uphold, contact_tel', 'required', 'on'=>'ApiCreate'),
            array('contact_tel', 'checkPhone', 'on'=>'ApiCreate'),
//            array('customer_id', 'required', 'on'=>'ApiCustomerCreate'),
    
            array('contact_person', 'length', 'max'=>350),
            array('contact_tel', 'length', 'max'=>100),
            array('schedule_type,type_customer,day_checkbox, schedule_in_month, content, has_read', 'safe'),
            array('id, code_no, type, customer_id, level_type, employee_id, type_uphold, content, contact_person, contact_tel, status, uid_login, created_date, daysNotPurchase', 'safe'),
            array('aProvinceId, next_uphold_from, next_uphold_to, next_uphold, last_purchase_from, last_purchase_to, agent_id, code_complete, customer_chain_store, khong_baotri_from, khong_baotri_to, created_date_from, created_date_to, GoToCustomer, sale_id, province_id, district_id, status_uphold_schedule, district_id, manage_id, limit_report,date_from, date_to, report_year', 'safe'),
        );
    }

    public function checkPhone($attribute,$params)
    {
        if(empty($this->contact_tel)){
            $this->addError('contact_tel', "Chưa nhập Số điện thoại di động");
            return ;
        }
        /* 1. check chiều dài phone
         * 2. check phone phải là di động
         * 3. kiểm tra chỉ cho 1 số phone dc signup 1 ngày tối đa 5 lần 
         * 4. check phone có tài khoản trên hệ thống chưa
         */
        
        $phoneNumber = $this->contact_tel;// 1 + 2
        if(!UsersPhone::isValidCellPhone($phoneNumber)){
            $this->addError('contact_tel', "Số điện thoại $phoneNumber không phải là số di động hợp lệ");
        }
    }
    
    public function relations()
    {
        return array(
            'rCustomer' => array(self::BELONGS_TO, 'Users', 'customer_id'),
            'rUidLogin' => array(self::BELONGS_TO, 'Users', 'uid_login'),
            'rEmployee' => array(self::BELONGS_TO, 'Users', 'employee_id'),
            'rUpholdReply' => array(self::HAS_MANY, 'GasUpholdReply', 'uphold_id',
                'order'=> 'rUpholdReply.id DESC'
                ),
            'rUpholdReplyForSearch' => array(self::HAS_MANY, 'GasUpholdReply', 'uphold_id'),
            'rSale' => array(self::BELONGS_TO, 'Users', 'sale_id'),
            'rAgent' => array(self::BELONGS_TO, 'Users', 'agent_id'),
            'rUpholdReplyForUpdateNextUphold' => array(self::HAS_ONE, 'GasUpholdReply', 'uphold_id',
//                'order'=> 'rUpholdReplyForUpdateNextUphold.created_date DESC',
                'order'=> 'rUpholdReplyForUpdateNextUphold.id DESC',
                'condition'=> 'rUpholdReplyForUpdateNextUphold.status = ' . GasUphold::STATUS_COMPLETE,
            ),
        );
    }

    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'code_no' => 'Mã Số',
            'type' => 'Loại Bảo Trì',
            'customer_id' => 'Khách Hàng',
            'level_type' => 'Mức Độ Nghiêm Trọng',
            'employee_id' => 'NV Bảo Trì',
            'type_uphold' => 'Loại Sự Cố',
            'content' => 'Nội Dung',
            'contact_person' => 'Người Liên Hệ',
            'contact_tel' => 'Điện Thoại',
            'status' => 'Trạng Thái',
            'uid_login' => 'Người Tạo',
            'created_date' => 'Ngày Tạo',
            'has_read' => 'Xác nhận (view)',
            'schedule_type' => 'Loại bảo trì',
            'schedule_in_month' => 'Số lần bảo trì',
            'day_checkbox' => 'Chọn ngày hàng tháng',
            'report_year' => 'Year',
            'date_from' => 'Từ Ngày',
            'date_to' => 'Đến Ngày',
            'limit_report' => 'Số lượng xem',
            'manage_id' => 'Người quản lý bảo trì',
            'status_uphold_schedule' => 'Trạng thái bảo trì',
            'district_id' => 'Quận / Huyện',
            'province_id' => 'Tỉnh',
            'sale_id' => 'NV Kinh Doanh',
            'GoToCustomer' => 'Đến khách hàng',
            'created_date_from' => 'Ngày tạo từ',
            'created_date_to' => 'Đến',
            'MonthKhongBaoTri' => 'Thời gian không đi bảo trì (Tháng)',
            'khong_baotri_from' => 'Không bảo trì từ',
            'khong_baotri_to' => 'Đến',
            'type_customer' => 'Loại KH',
            'agent_id' => 'Đại lý',
            'next_uphold' => 'Ngày bảo trì tiếp theo',
            'next_uphold_from' => 'Ngày bảo trì tiếp theo từ',
            'next_uphold_to' => 'Đến',
            'aProvinceId' => 'Chọn nhiều tỉnh',
        );
    }

    public function search()
    {
        $criteria=new CDbCriteria;
        $criteria->compare('t.code_no',$this->code_no);
        $criteria->compare('t.type', $this->type);
        $criteria->compare('t.agent_id', $this->agent_id);
        $criteria->compare('t.customer_id',$this->customer_id);
        $criteria->compare('t.level_type',$this->level_type);
        $criteria->compare('t.employee_id',$this->employee_id);
        $criteria->compare('t.type_uphold',$this->type_uphold);
        $criteria->compare('t.contact_person',$this->contact_person,true);
        $criteria->compare('t.contact_tel',$this->contact_tel);
        $criteria->compare('t.status',$this->status);
        $criteria->compare('t.uid_login',$this->uid_login);
        $criteria->compare('t.has_read',$this->has_read);
        $criteria->compare('t.type_customer',$this->type_customer);
        $criteria->compare('t.province_id',$this->province_id);
        $criteria->compare('t.district_id',$this->district_id);
        if(!empty($this->date_from)){
            $date_from = MyFormat::dateDmyToYmdForAllIndexSearch($this->date_from);
            $criteria->addCondition("t.created_date_only >= '$date_from'");
        }
        if(!empty($this->date_to)){
            $date_to = MyFormat::dateDmyToYmdForAllIndexSearch($this->date_to);
            $criteria->addCondition("t.created_date_only <= '$date_to'");
        }
        
        $aRoleAllow = array(ROLE_DIEU_PHOI); // User tao Bt
        $cRole  = MyFormat::getCurrentRoleId();
        $cUid   = MyFormat::getCurrentUid();
        $roleCustomer = ROLE_CUSTOMER;
//        if(in_array($cRole, $aRoleAllow)){// Jan 04, 2016 cho điều phối thấy hết BT của nhau
//            $criteria->compare('t.uid_login', $cUid);
//        }
        
//        $sParamsInMienTay = implode(',', GasOrders::$ZONE_MIEN_TAY);
//        if(in_array($cUid, GasOrders::$ZONE_MIEN_TAY)){// Oct 01, 2016 tach điều phối moi zone thấy
//            $sParamsIn = implode(',', GasOrders::$ZONE_MIEN_TAY);
//            $criteria->addCondition("t.uid_login IN ($sParamsInMienTay) OR t.role_id=$roleCustomer");
//        }elseif(in_array($cUid, GasOrders::$ZONE_HCM)){// Oct 01, 2016 tach điều phối moi zone thấy
//            $sParamsIn = implode(',', GasOrders::$ZONE_HCM);
////            $criteria->addCondition("t.uid_login IN ($sParamsIn) OR t.role_id=$roleCustomer");
//            $criteria->addCondition("t.uid_login NOT IN ($sParamsInMienTay) OR t.role_id=$roleCustomer");
//        }// Jun 11, 2017 Close lại

        $this->handleRoleEmaintain($cRole, $cUid, $criteria);
        GasAgentCustomer::addInConditionAgent($criteria, 't.agent_id');
        $criteria->order = "t.id DESC";
        
        $_SESSION['data-excel'] = new CActiveDataProvider($this, array(
            'pagination'=>false,
            'criteria'=>$criteria,
        ));

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
            'pagination'=>array(
                'pageSize'=> Yii::app()->user->getState('pageSize',Yii::app()->params['defaultPageSize']),
            ),
        ));
    }
    
    /**
     * @Author: DungNT Feb 16, 2016
     */
    public function handleRoleEmaintain($cRole, $cUid, &$criteria) {
        $aRoleSale      = [ROLE_SALE, ROLE_MONITORING_MARKET_DEVELOPMENT];
        $aUidViewAll    = [GasConst::UID_PHONG_NU];
        if(in_array($cUid, $this->getUidViewAll())){
            
        }elseif(in_array($cRole, $aRoleSale)){
            $criteria->addCondition('t.sale_id='.$cUid);
        }elseif($cRole == ROLE_EXECUTIVE_OFFICER){
            $criteria->addCondition('t.province_id=' . Yii::app()->user->province_id);
        }elseif(in_array($cRole, GasUphold::$ROLE_EMPLOYEE_BAOTRI) && !in_array($cUid, $this->getUidViewAll()) ){
            $criteria->compare('t.employee_id', $cUid);
        }
    }
    
    public function searchSchedule()
    {
        $aWith = array();
        $criteria=new CDbCriteria;
        $criteria->order = 't.id DESC';
        $criteria->compare('t.schedule_type',$this->schedule_type);
        $criteria->compare('t.code_no',$this->code_no);
        $criteria->compare('t.type',$this->type);
        $criteria->compare('t.agent_id', $this->agent_id);
        $criteria->compare('t.customer_id',$this->customer_id);
        $criteria->compare('t.type_customer',$this->type_customer);
        $criteria->compare('t.level_type',$this->level_type);
        $criteria->compare('t.employee_id',$this->employee_id);
        $criteria->compare('t.type_uphold',$this->type_uphold);
        $criteria->compare('t.contact_person',$this->contact_person,true);
        $criteria->compare('t.contact_tel',$this->contact_tel,true);
        $criteria->compare('t.status',$this->status);
        $criteria->compare('t.uid_login',$this->uid_login);
        $criteria->compare('t.status_uphold_schedule',$this->status_uphold_schedule);
        $criteria->compare('t.province_id',$this->province_id);
        $criteria->compare('t.district_id',$this->district_id);
        $criteria->compare('t.sale_id',$this->sale_id);
        $criteria->compare('t.manage_id',$this->manage_id);
        
        $date_from = '';
        $date_to = '';
        if(!empty($this->date_from)){
            $date_from = MyFormat::dateDmyToYmdForAllIndexSearch($this->date_from);
//            $criteria->addCondition("rUpholdReplyForSearch.created_date_only >= '$date_from'");
//            $aWith[] = "rUpholdReplyForSearch";
        }
        if(!empty($this->date_to)){
            $date_to = MyFormat::dateDmyToYmdForAllIndexSearch($this->date_to);
//            $criteria->addCondition("rUpholdReplyForSearch.created_date_only <= '$date_to'");
            if(count($aWith) == 0){
//                $aWith[] = "rUpholdReplyForSearch";
            }
        }
        if(!empty($this->created_date_from)){
            $date_from = MyFormat::dateDmyToYmdForAllIndexSearch($this->created_date_from);
            $criteria->addCondition("DATE(t.created_date) >= '$date_from'");
        }
        if(!empty($this->created_date_to)){
            $date_to = MyFormat::dateDmyToYmdForAllIndexSearch($this->created_date_to);
            $criteria->addCondition("DATE(t.created_date) <= '$date_to'");
        }
        
        if(!empty($this->next_uphold_from)){
            $date_from = MyFormat::dateDmyToYmdForAllIndexSearch($this->next_uphold_from);
            $criteria->addCondition("t.next_uphold >= '$date_from'");
        }
        if(!empty($this->next_uphold_to)){
            $date_to = MyFormat::dateDmyToYmdForAllIndexSearch($this->next_uphold_to);
            $criteria->addCondition("t.next_uphold <= '$date_to'");
            $criteria->order = 't.next_uphold ASC';
        }

        if(!empty($date_from) && !empty($date_to)){
            if($this->GoToCustomer == GasUphold::STATUS_UPHOLD_RUNNING){// có đi bảo trì
                $criteria->addCondition('t.id IN ('.$this->getUpholdIdByDate($date_from, $date_to).')');
            }elseif($this->GoToCustomer == GasUphold::STATUS_UPHOLD_OFF){// không đi BT
                $criteria->addCondition('t.id NOT IN ('.$this->getUpholdIdByDate($date_from, $date_to).')');
            }
        }
        if(!empty($this->khong_baotri_from) && !empty($this->khong_baotri_to)){
            $date_to = MyFormat::dateDmyToYmdForAllIndexSearch($this->khong_baotri_to);
            $criteria->addCondition('t.customer_id NOT IN ('.$this->getCustomerIdHasReply().')'." AND DATE(t.created_date) <= '$date_to'");
//            $criteria->addCondition('t.customer_id NOT IN ('.$this->getCustomerIdHasReply().')');
        }
        // DuongNV Sep0619
        if(!empty($this->daysNotPurchase)){
            $currentDate     = date('Y-m-d');
            $criteria->join  = 'JOIN gas_users u ON t.customer_id = u.id';
            $prevDate        = MyFormat::modifyDays($currentDate, $this->daysNotPurchase,'-');
            $criteria->addCondition("u.last_purchase = '$prevDate'");
            $criteria->order = 'u.last_purchase DESC';
        }
        $this->webHandleMoreCondition($criteria);
        if(count($aWith)){
            $criteria->with = $aWith;
            $criteria->together = true;
        }
        $aRoleAllow = array(ROLE_DIEU_PHOI, ROLE_HEAD_OF_MAINTAIN, ROLE_HEAD_GAS_MOI, ); // User tao Bt
        $cRole  = MyFormat::getCurrentRoleId();
        $cUid   = MyFormat::getCurrentUid();
//        if(in_array($cRole, $aRoleAllow)){ 
        $this->getCriteriaWebApi($cUid, $cRole, $criteria);
        $this->handleRoleEmaintain($cRole, $cUid, $criteria);
        if(!in_array($cUid, $this->getUidViewAll())){
            GasAgentCustomer::addInConditionAgent($criteria, 't.agent_id');
        }

        $_SESSION['data-excel'] = new CActiveDataProvider($this, array(
            'pagination'=>false,
            'criteria'=>$criteria,
        ));
        
        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
            'pagination'=>array(
//                'pageSize'=> Yii::app()->user->getState('pageSize',Yii::app()->params['defaultPageSize']),
                'pageSize'=> 10,
            ),
        ));
    }
    
    /**
     * @Author: DungNT Nov 10, 2016
     * @Todo: xử lý build sql get distinct uphold id trong reply theo khoảng date
     */
    public function getUpholdIdByDate($date_from, $date_to) {
        return "SELECT DISTINCT uphold_id FROM `gas_gas_uphold_reply` as SubReply WHERE (SubReply.created_date_only BETWEEN '$date_from' AND '$date_to')";
    }
    /**
     * @Author: DungNT Apr 14, 2017
     * @Todo: xử lý get list customer id đã đi bảo trì - tức là có reply trước đây 1,2,3... tháng
     */
    public function getCustomerIdHasReply() {
        $date_from = MyFormat::dateDmyToYmdForAllIndexSearch($this->khong_baotri_from);
        $date_to = MyFormat::dateDmyToYmdForAllIndexSearch($this->khong_baotri_to);
        return "SELECT DISTINCT customer_id FROM `gas_gas_uphold_reply` as SubReply WHERE SubReply.type=1 AND (SubReply.created_date_only BETWEEN '$date_from' AND '$date_to')";
    }
    
    /** @Author: DungNT Apr 03, 2018
     *  @Todo: xử lý limit thêm condition
     **/
    public function webHandleMoreCondition(&$criteria) {
        if(!empty($this->last_purchase_from) && !empty($this->last_purchase_to)){
            $date_from  = MyFormat::dateDmyToYmdForAllIndexSearch($this->last_purchase_from);
            $date_to    = MyFormat::dateDmyToYmdForAllIndexSearch($this->last_purchase_to);
            $typeCustomer = STORE_CARD_KH_BINH_BO.','.STORE_CARD_KH_MOI;
            $c = 'SubQ.role_id='.ROLE_CUSTOMER." AND SubQ.is_maintain IN ($typeCustomer)";
            $q = "SELECT id FROM `gas_users` as SubQ WHERE $c AND (SubQ.last_purchase BETWEEN '$date_from' AND '$date_to')";
            $criteria->addCondition("t.customer_id IN ($q)");
        }
        if(count($this->aProvinceId)){
            $sParamsIn = implode(',',  $this->aProvinceId);
            $criteria->addCondition("t.province_id IN ($sParamsIn)");
        }
    }
    
    /**
     * @Author: DungNT Apr 22, 2016
     * @Todo: sử dụng chung criteria của app vs web cho listing bảo trì định kỳ
     */
    public function getCriteriaWebApi($cUid, $cRole, &$criteria) {
//        if (array_key_exists($cUid, $this->getArrayManage())) {
//            $criteria->compare('t.manage_id', $cUid);
//        }
        // Apr 29, 2016 cho phép a Huấn Sửa của A Quý (manage_id)
//        if(in_array($cUid, $this->getUidMixUpdate()) ){
//            $criteria->compare('t.manage_id', GasSupportCustomer::UID_QUY_PV);
//        }
    }
    
    /**
     * @Author: DungNT Apr 29, 2016
     * @Todo: anh Huấn sửa của A Quý
     */
    public function getUidMixUpdate() {
        return array(GasSupportCustomer::UID_QUY_PV);
    }
    public function getUidViewHcm() {
        return [
//            885640,// Lương Văn Quang
//            903951,//  Trần Phi Thanh
//            124,// Đinh Trọng Nghĩa
        ];
    }

    /**
     * @Author: DungNT Nov 23, 2015
     * @Todo: chuyển NV bảo trì cũng notify cho NV bảo trì mới
     */
    public function isChangeEmployee() {
        if( !$this->isNewRecord && $this->type == GasUphold::TYPE_SU_CO ){
            $OldModel = self::model()->findByPk($this->id);
            if($OldModel->employee_id != $this->employee_id){
                $this->notify_count = 0; // reset notify count
                return true;
            }
        }
        return false;
    }
    
    protected function beforeDelete() {
        MyFormat::deleteModelDetailByRootId('GasUpholdReply', $this->id, 'uphold_id');
        GasScheduleNotify::DeleteAllByType(GasScheduleNotify::UPHOLD_ALERT_10, $this->id);
        GasScheduleNotify::DeleteAllByType(GasScheduleNotify::UPHOLD_DINH_KY_1_DAY, $this->id);
        GasScheduleNotify::DeleteAllByType(GasScheduleNotify::UPHOLD_CREATE, $this->id);
        
        GasScheduleNotifyHistory::DeleteAllByType(GasScheduleNotify::UPHOLD_ALERT_10, $this->id);
        GasScheduleNotifyHistory::DeleteAllByType(GasScheduleNotify::UPHOLD_DINH_KY_1_DAY, $this->id);
        GasScheduleNotifyHistory::DeleteAllByType(GasScheduleNotify::UPHOLD_CREATE, $this->id);
        // Jan 31, 2016
        GasOneManyBig::deleteOneByType($this->id, GasOneManyBig::TYPE_UPHOLD_SCHEDULE_DAY);
        return parent::beforeDelete();
    }
    
    protected function beforeValidate() {
        $this->trimData();
        if($this->type == GasUphold::TYPE_DINH_KY){
            $validator = CValidator::createValidator('unique', $this, 'customer_id', array(
                'criteria' => array(
                    'condition'=>'`type`=:secondKey',
                    'params'=>array(
                        ':secondKey'=>GasUphold::TYPE_DINH_KY
                    )
                ),
                'message'=> "Bảo trì định kỳ cho khách hàng này đã tồn tại, vui lòng tìm kiếm và cập nhật"
            ));
            $this->getValidatorList()->insertAt(0, $validator); 
        }
        return parent::beforeValidate();
    }
    
    /**
     * @Author: DungNT Dec 23, 2016
     */
    public function trimData() {
        $this->content          = trim($this->content);
        $this->contact_person   = trim($this->contact_person);
        $this->contact_tel      = trim($this->contact_tel);
        $this->rating_note      = trim($this->rating_note);
    }
    
    /**
     * @Author: DungNT Dec 31, 2015
     * @Todo: save create uphold from app phone
     */
    public function apiSaveCreate() {
        $this->save();
    }
    
    /** @Author: DungNT Dec 22, 2017
     *  @Todo: gen code khi NV tạo từ web hoặc KH tạo app
     **/
    public function autoGenCode() {
        $aScenario = ['create', 'ApiCreate'];
        if(!$this->isNewRecord || !in_array($this->scenario, $aScenario)){
            return ;
        }
        $this->genCodeComplete();
    }

    protected function beforeSave() {
        /* Kịch bản khi send SMS  => send NVBT
         * SMS_U_01 1/ điều phối tạo => send NVBT
         * SMS_U_02 2/ Kh tạo ở app => send NVBT
         * SMS_U_03 3/ điều phối change NV Bảo trì => send NVBT
         * SMS_U_04 4/ NV bảo trì xác nhận xử lý => send KH
         */
        $this->autoGenCode();
        $cUid = isset(Yii::app()->user) ? Yii::app()->user->id: '';
        $cRole = isset(Yii::app()->user) ? Yii::app()->user->role_id : '';
        $mCustomer = $this->rCustomer;
        if($mCustomer){
            $this->sale_id          = $mCustomer->sale_id;
            $this->type_customer    = $mCustomer->is_maintain;
            $this->province_id      = $mCustomer->province_id;
            $this->district_id      = $mCustomer->district_id;
            $this->agent_id         = $mCustomer->area_code_id;
        }
        
        if($this->isNewRecord){
            /* Dec 31, 2015 xử lý kiểm tra kiểu này không đúng: if(isset(Yii::app()->user)
             * tưởng là khi post ở app thì nó không có, nhưng mà vẫn có, nên phải thêm !empty($cUid)
             */
            if(isset(Yii::app()->user) && !empty($cUid)){
                $this->uid_login = $cUid;
            }
            $sign = "M";
            if($this->type == GasUphold::TYPE_DINH_KY){
                $sign = "S";
            }
            $this->code_no = MyFunctionCustom::getNextId('GasUphold', $sign.date('y'), LENGTH_TICKET,'code_no');
            $this->created_date_only = date("Y-m-d");
        }
        
        if($cRole != ROLE_ADMIN && isset(Yii::app()->user) && !empty($cUid) ){// chỗ này là điều phối tạo và update trên web nên sẽ lấy uid last update ở đây để notify
            $this->last_update_by = $cUid;
        }
        if($cRole != ROLE_ADMIN && $this->rUidLogin){ //Now 29, 2015 vì uid_login được set ở web khi tạo và ở API customer tạo nên sẽ set role_id ở đây
            $this->role_id = $this->rUidLogin->role_id;
        }
        /* NOTE CHO API TAO
         * 1/ gắn lại uid_login
         * 2/ không phải set last_update_by
         * 3/ rCustomer cũng là uid_login
         */        

        $this->content          = InputHelper::removeScriptTag($this->content);
        $this->contact_person   = InputHelper::removeScriptTag($this->contact_person);
        $this->contact_tel      = InputHelper::removeScriptTag($this->contact_tel);
        if($this->isChangeEmployee()){
            // notify to app bt
            $this->NotifyAfterCreate(); // Nov 02, 2015 chưa live this function
            $this->HandleAddCronAlert();
            // SMS_U_03 3/ điều phối change NV Bảo trì => send NVBT
            $this->NotifySmsCreateSending($this->employee_id, $this->getSMSContentCreate(), GasScheduleSms::TYPE_UPHOLD);// Jun 10, 2016 gửi SMS cho NV mới change
        }
        return parent::beforeSave();
    }
    
    /**
     * @Author: DungNT Oct 21, 2015
     */
    protected function afterSave() {
        $aScenarioNotify = array('ApiCreate', 'ApiCustomerCreate');// Apr 18, 2016
//        if($this->isNewRecord && ($this->scenario == 'create' || $this->scenario == 'createSchedule1' || $this->scenario == 'createSchedule2') ){
        // Feb 16, 2016 bỏ notify khi tạo đinh kỳ (createSchedule1, createSchedule2), vì notify quá nhiều
        if(in_array($this->scenario, $aScenarioNotify) || ($this->isNewRecord && $this->scenario == 'create' && $this->type == GasUphold::TYPE_SU_CO) ){// Dec 04, 2015 có thể không notify khi tạo mới LỊch đinh kỳ
            // notify to app bt
            $this->NotifyAfterCreate(); // Nov 02, 2015 chưa live this function
        }
        
//        Yii::app()->setting->setDbItem('last_working', $this->scenario);
        $this->HandleAddCronAlert();
        $this->NotifySmsCreate();// Close on Apr 20, 2016
        return parent::afterSave();
    }
    
    /**
     * @Author: DungNT Dec 19, 2015
     * @Todo: 
     */
    public function SaveScheduleInMonth() {
        if($this->type == GasUphold::TYPE_DINH_KY){
            $this->GetMultiDay();
            $this->schedule_in_month = count($this->day_checkbox);
            $this->update(array('schedule_in_month'));
        }
    }
    
    /**
     * @Author: DungNT Now 29, 2015
     * @Todo: get list user sẽ notify after create new BT sự cố, Không có cho BT đinh kỳ
     */
    public function getListUidSend(&$aUid){
        if($this->type_uphold == GasUphold::UPHOLD_XI_GAS){
            $aUid[GasSupportCustomer::UID_QUY_PV] = GasSupportCustomer::UID_QUY_PV;
        }

//        $aUid[GasConst::UID_CANH_NV]        = GasConst::UID_CANH_NV;// Add Sep 12, 2016
        $aUid[GasLeave::UID_HEAD_TECHNICAL] = GasLeave::UID_HEAD_TECHNICAL;// Add Now 10, 2016
        if($this->type == GasUphold::TYPE_SU_CO){// May 28, 2016 không gửi notify cho điều phối khi là reply của BT định Kỳ
            foreach(GasTickets::$UID_DIEU_PHOI as $uid){
                if(!in_array($uid, $aUid)){
//                    $aUid[$uid] = $uid; //Aug0819 DungNT bỏ notify điều phối -- Apr212016 thêm notify cho tất cả điều phối
                }
            }
            // Aug0819 DungNT set cho nhân viên bảo trì Nguyễn Tấn Minh nhận notify bảo trì sự cố toàn bộ miền tây
            if(in_array($this->province_id, GasOrders::$TARGET_ZONE_PROVINCE_MIEN_TAY)){
                $aUid[GasLeave::UID_MINH_NT] = GasLeave::UID_MINH_NT;
                $aUid[1042346] = 1042346;// demo6
            }
        }
        if(!in_array($this->sale_id, $aUid)){
            $aUid[$this->sale_id] = $this->sale_id;// May 03, 2016 notify cho Sale nua
        }
//        $aUid[] = GasTickets::DIEU_PHOI_CHUONG;// fix change Apr 12, 2016 notify cho Điều phối chương khi KH tạo ở App
        // Dec 02, 2015 xử lý notify cho KH nếu status là Xử Lý hoặc Hoàn Thành
        if(in_array($this->status, $this->getStatusNeedNotify())){
            $aUid[] = $this->customer_id;
            if(!empty($this->parent_chain_store)){
                $aUid[$this->parent_chain_store] = $this->parent_chain_store;
            }
        }
        
        if($this->type_uphold == GasUphold::UPHOLD_XI_GAS){// Dec 07, 2016 notify cho giam sat DL neu la xi gas
            $aUid[GasLeave::PHUC_HV]            = GasLeave::PHUC_HV;
//            $aUid[GasConst::UID_TRUONG_NN]      = GasConst::UID_TRUONG_NN;
//            $aUid[GasConst::UID_DOT_NV]         = GasConst::UID_DOT_NV;
        }
        
        /* Now0617: Khi có sự cố khách hàng TPHCM chuyển qua app thì những người sau đây đều nhận đc app sự cố: Phan Văn Quý, Lương Văn Quang, Trần Phi Thanh, Đinh Trọng Nghĩa. */
        if($this->province_id == GasProvince::TP_HCM){
            $aUid = array_merge($aUid, $this->getUidViewHcm());
        }
        $aUid = array_unique($aUid);
    }
    
    /** @NOTE các notify của 1 function nên viết liền nhau để còn review không bị sót
     * @Author: DungNT Nov 23, 2015
     * @Todo: notify after create new 
     */
    public function NotifyAfterCreate() {
        $aUid = [];
        if(!empty($this->employee_id)){
            $aUid[$this->employee_id] = $this->employee_id;
        }
        $this->getListUidSend($aUid); // Now 29, 2015 ngoài NV bảo trì ra sẽ notify cho Tổ trưởng Bảo Trì
        $message = "[BT sự cố] {$this->getTextUpholdType()}: {$this->getCustomer()}";
        if($this->type == GasUphold::TYPE_DINH_KY){
            $message = "Lịch bảo trì định kỳ: {$this->getCustomer()}";
        }
//        $this->ApiSendNotify($aUid, $message); // Close on Dec 21, 2015 không send luôn mà đưa vào queue
        $this->RunInsertNotify($aUid, $message); // Dec 21, 2015 
        // xử lý tách riêng notify ra nhiều kieu notify
    }
    
    /**
     * @Author: DungNT Dec 02, 2015
     * @Todo: cron notify 10' cho NV bao tri (employee_id)
     * send notify 2 lần, mỗi lần cách nhau 10'cho NV bảo trì
     * AND plus 1 to notify_count
     * @param: $user_id
     * @param: $message được build sẵn trong queue
     */
    public function NotifyCronAlert($mScheduleNotify) {
//        $mUser = Users::model()->findByPk($user_id);
        $aUid = array($mScheduleNotify->user_id);
        
        // Dec 21, 2015 sẽ không update count ở đây, mà sẽ update khi client gửi request lên là đã đọc tin rồi
//        if($mUser && $mUser->role_id == ROLE_E_MAINTAIN){
//            if(empty($this->notify_count) || is_null($this->notify_count)){
//                $this->notify_count = 1;
//            }else{
//                $this->notify_count += 1;
//            }
//            $this->update(array('notify_count'));
//        }
        // Close on Dec 21, 2015 vì message đã được build sẵn trong queue
//        $message = "Nhắc nhở lần $this->notify_count sự cố mới: {$this->getCustomer()}";
        $this->ApiSendNotify($aUid, $mScheduleNotify->title, 0, $mScheduleNotify); // Nov 23, 2015
            // xử lý tách riêng notify ra nhiều kieur notify
    }        
    
    /**
     * @Author: DungNT Dec 02, 2015
     * @Todo: BT định kỳ cron notify  NV bao tri (employee_id) + KH vào 6h sáng mỗi ngày nếu có BT của KH đó
     */
    public function NotifyCronAlertDinhKy($mReply, $mScheduleNotify) {        
        $aUid = array($mScheduleNotify->user_id);
        // Close on Dec 21, 2015 vì chuyển đổi kiểu notify sang cho vào queue hết
//        $message = "Nhắc nhở bảo trì định kỳ: {$this->getCustomer()}";
        $this->ApiSendNotify($aUid, $mScheduleNotify->title, $mReply->id, $mScheduleNotify); // Nov 23, 2015
        // xử lý tách riêng notify ra nhiều kieu notify 11111
    }
    
    /**
     * @Author: DungNT Nov 23, 2015
     * @Todo: notify cho điều phối + KH after nhân viên bảo trì reply
     * @param: $mReply
     */
    public function NotifyAfterRepy($mReply) {
        $reply_id = "0";
        $aUid = [$this->last_update_by => $this->last_update_by, $this->employee_id => $this->employee_id];
        $this->getListUidSend($aUid); // Now 29, 2015 ngoài NV bảo trì ra sẽ notify cho Tổ trưởng Bảo Trì
        $message = "[Bảo trì] {$this->getTextStatus()} [{$this->getTextUpholdType()}]: {$this->getCustomer()}";
        if($this->type == GasUphold::TYPE_DINH_KY){
            $message = "Báo cáo bảo trì định kỳ: {$this->getCustomer()}";
            $reply_id = $mReply->id;
        }
//        $this->ApiSendNotify($aUid, $message); // Close on Dec 21, 2015 
        $this->RunInsertNotify($aUid, $message, $reply_id); // Dec 21, 2015 
        //
        if($this->isFirstSms($mReply)){
            // xử lý tách riêng notify ra nhiều kieur notify
            $this->NotifySmsReplyConfirmToCustomer($mReply);
        }
    }
    
    /**
     * @Author: DungNT Jul 21, 2016
     * @Todo: kiểm tra xem trước đó đã có 1 reply xử lý chưa, để tránh send SMS 2 lần
     * vì cứ khi xử lý thì hệ thống sẽ send SMS cho customer
     */
    public function isFirstSms($mReply) {
        if($mReply->status != GasUphold::STATUS_HANLDE){
            return ;// chỉ check với status xử lý
        }
        $criteria = new CDbCriteria();
        $criteria->compare("t.uphold_id", $this->id);
        $criteria->limit = 1;// vì hàm này chạy ở beforeSave của Reply nên record chưa insert vào db => limit 1
        $criteria->order = "t.id DESC";
        $model = GasUpholdReply::model()->find($criteria);
        if(is_null($model) || ($model && $model->status != GasUphold::STATUS_HANLDE)){
            return true;
        }
        return false;
    }
    
        /**
     * @Author: DungNT Jul 21, 2016
     * @Todo: get last reply
     */
    public function getLastReply() {
        $criteria = new CDbCriteria();
        $criteria->compare("t.uphold_id", $this->id);
        $criteria->limit = 1;// vì hàm này chạy ở beforeSave của Reply nên record chưa insert vào db => limit 1
        $criteria->order = "t.id DESC";
        $mReply = GasUpholdReply::model()->find($criteria);
        $str = '';
        if($mReply){
            if(trim($mReply->getNote()) != ''){
                $str .= "<b>Nghiệm thu: </b>".$mReply->getNote();
                $str .= "<br><b>ĐT: </b>".$mReply->getApiContactPhone();
            }
            if(trim($mReply->getNoteInternal()) != ''){
                $str .= "<br><b>Ghi chú nội bộ: </b>".$mReply->getNoteInternal();
            }
        }
        return $str;
    }
            
    /**
     * @Author: DungNT Dec 21, 2015
     * @Todo: insert to table notify
     * @Param: $aUid array user id need notify
     */
    public function RunInsertNotify($aUid, $title, $reply_id = "0") {
        $json_var = array("reply_id" => $reply_id);
        foreach($aUid as $uid) {
            if( !empty($uid) && $uid>0 ){
                $mScheduleNotify = GasScheduleNotify::InsertRecord($uid, GasScheduleNotify::UPHOLD_CREATE, $this->id, '', $title, $json_var);
            }
        }
    }
    
    /**
     * @Author: DungNT Sep 21, 2015
     * @Todo: Receive notification when new uphold was created
        User create Uphold
        Send notification to apps when new Uphold was created 
        User touch on the notification to open Uphold details in app
     * @param Array $aUid array(id)
     * @param String $message 
     * @param: $reply_id = "0" biến xử lý thêm cho view BT định kỳ
     */
//    public function ApiSendNotify($aUid, $message, $reply_id = "0") {
    public function ApiSendNotify($aUid, $message, $reply_id, $mScheduleNotify) {
        $DeviceAndroid  = UsersTokens::getListDeviceToken('gcm_device_token', $aUid, UsersTokens::LOGIN_ANDROID);
        $DeviceIos      = UsersTokens::getListDeviceToken('apns_device_token', $aUid, UsersTokens::LOGIN_IOS);
        $customerProperty = array(
            'notify_id'     => $mScheduleNotify->id,
            'notify_type'   => $mScheduleNotify->type,
            'type'          => ApiNotify::TYPE_UPHOLD,
            'id'            => $this->id,
            'reply_id'      => ''.$reply_id
        );
//        $this->TestNotifyBB();// for test only Close when live
        ApiNotify::BuildToSend($DeviceAndroid, $message, $customerProperty, YiiApnsGcm::TYPE_GCM);
        ApiNotify::BuildToSend($DeviceIos, $message, $customerProperty, YiiApnsGcm::TYPE_APNS);
    }
    
    /**
     * @Author: DungNT Oct 19, 2015
     * @Todo: something
     * @Param: 
     */
    public static function TestNotifyBB() {
        $message = 'Spj test notify GCM';
        $aCustom = array('a'=>'b');
        $args = array(
            'timeToLive' => 3
          );
//        $AndroidTokenOld = 'APA91bEig9l_3qBCfKwaRe-ZRfyHxwGIQ52H8hGheIDM99La4BMfZf-gZgOB1S2u-qotuaT04Xm8A8wo6SFbMq24ivJCoHe8jwhSTTESACkAxqKVvt46Wo18vF13OEbMHSed9wl7hgx_';
        $AndroidToken = 'fX_5kNM6MKk:APA91bGDnA_CMsvckBXze9GUgegoTDxpEynDFCpYnbyudMEzuZD6iomFht7knzsa9tDzF4_MXXV1dC_vf0cVODv6ct_f_EeGtIixNNmF0lFbi6hP4Y5mt1qCtEZKdSK3LZ0n3FV3LSWH'; // Anh Long galaxy A7
//        $AndroidToken = 'c9Y2NnAmIJg:APA91bFApjK67NO3Xd8eNsoeQ8pvjeZmKzQBzq_DNU91YYf9x78RJcKhztrBqHuUecvWd7iEmxBQhsNXjBFzcWMlESh87NoQT7_4ofFU-I_y0TS01tKmye0yzoMZnND8W00G6KWLhIKy'; // 4 inch lenovo
        
        $gcm = Yii::app()->gcm;
        $gcm->send($AndroidToken, $message,
          $aCustom,
          $args
        );
        
//        $apnsGcm = Yii::app()->apnsGcm;
//        $apnsGcm->send(YiiApnsGcm::TYPE_GCM, $AndroidToken, $message,
//                $aCustom,
//                $args
//              );
        echo '<pre>';
        print_r('OK GCM');
        echo '</pre>';
        exit;
//  Anh Dung GCM      APA91bEig9l_3qBCfKwaRe-ZRfyHxwGIQ52H8hGheIDM99La4BMfZf-gZgOB1S2u-qotuaT04Xm8A8wo6SFbMq24ivJCoHe8jwhSTTESACkAxqKVvt46Wo18vF13OEbMHSed9wl7hgx_
        
//         android test BB
//        
        $apnsGcm = Yii::app()->apnsGcm;
        $args = array(
                  'timeToLive' => 3
                );
        $push_tokens = 'APA91bEDo9RMV7_E4GJbXQ-Dfc3s7ytgDlIar2NpJqHlIehq3K565wp_ND_HrOS8g2RdOgPpLHoazoQIBBizIFpS1VKLw_IN1Hk-i8ZnZZKgfXW4o2aDSVv5SF8LOSI-RoA-C4dJBXnc';
        $push_tokens = 'APA91bEIWGK71uYA2jHTF7fBBl4od9aB7jV2GFA2Osg8mWieKs68-l37l9LeKJgIpM6mFarLUFTzijRejrKEN1nPAKtXzE213TrQMScX_Pp4d_YLpI-lvyvb1XRnftWq_6qPcqoH7bQ4';
        $apnsGcm->send(YiiApnsGcm::TYPE_GCM, $push_tokens, $message,
                $customerProperty,
                $args
              );
        exit;
        
        $gcm = Yii::app()->gcm;
        $gcm->send('APA91bEDo9RMV7_E4GJbXQ-Dfc3s7ytgDlIar2NpJqHlIehq3K565wp_ND_HrOS8g2RdOgPpLHoazoQIBBizIFpS1VKLw_IN1Hk-i8ZnZZKgfXW4o2aDSVv5SF8LOSI-RoA-C4dJBXnc', $message,
          $customerProperty,
          array(
            'timeToLive' => 3
          )
        );
        die;
        
        
//        $token = '61bf940b4777917c7bef4a05887a6571ae91be730ec730187e3f66e6ec969e3e';
//        $apns = Yii::app()->apns;
//            $apns->send($token, $message,
//              $customerProperty,
//              array(
//                'sound'=>'default',
//                'badge'=>1
//              )
//            );
//        return; // only for Test Ios QuocBao
    }    
    
    /**
     * @Author: DungNT Oct 21, 2015
     */
    public function getCustomer($field_name='') {
        $mUser = $this->rCustomer;
        if($mUser){
            if($field_name != ''){
                return $mUser->$field_name;
            }
            return $mUser->first_name;
        }
        return '';
    }
    
    public function getCustomerType() {
        $mUser = $this->rCustomer;
        if($mUser){
            return $mUser->getTypeCustomerText();
        }
        return '';
    }
    public function getUidLogin() {
        $mUser = $this->rUidLogin;
        if($mUser){
            return $mUser->first_name;
        }
        return '';
    }
    public function getUidLoginWeb() {
        if($this->role_id == ROLE_CUSTOMER){
            return '<b>KH</b>';
        }
        return $this->getUidLogin();
    }
    public function getEmployee() {
        $mUser = $this->rEmployee;
        if($mUser){
            return $mUser->first_name;
        }
        return '';
    }
    
    /** @Author: DungNT Jun 30, 2018
     *  @Todo: get record định kỳ by customer id 
     **/
    public function getCustomerSchedule() {
        $criteria = new CDbCriteria();
        $criteria->compare("t.customer_id", $this->customer_id);
        $criteria->compare("t.type", GasUphold::TYPE_DINH_KY);
        return GasUphold::model()->find($criteria);
    }
    
    public function getLastUpdateSchedule() {
        $res = '';                
        $criteria = new CDbCriteria();
        $criteria->compare("t.uphold_id", $this->id);
        $criteria->order = "t.id DESC";
        $criteria->limit = 1;
        $mComment = GasUpholdReply::model()->find($criteria);
        if($mComment){
            $cmsFormater = new CmsFormatter();
            $res = $mComment->rUidLogin->first_name."<br>".$cmsFormater->formatDateTime($mComment->created_date);
        }
        
        $TotalComment = MyFormat::CountComment($this->id, 'uphold_id', 'GasUpholdReply');
        if($TotalComment){
            $res = "<b>($TotalComment)</b> - ".$res;
        }
        return $res;
    }
    
    // Now 30, 2015 lấy thông tin user theo relation và field_name
    public function getByRelationAndFieldName($NameRelation, $field_name='') {
        $mUser = $this->$NameRelation;
        if($mUser){
            if($field_name != ''){
                return $mUser->$field_name;
            }
            return $mUser->first_name;
        }
        return '';
    }
    
    public function getSale() {
        $mUser = $this->rSale;
        if($mUser){
            return $mUser->first_name;
        }
        return '';
    }
    public function getCodeNo() {
        return $this->code_no;
    }
    public function getContent() {
        return nl2br($this->content)."<br>".$this->getLastReply();
    }
    public function getContentOnly() {
        return nl2br($this->content);
    }
    public function getContentApi() {
        return $this->content;
    }
    public function getContactPerson() {
        return $this->contact_person;
    }
    public function getContactTel() {
        $res = ''; $key = 0;
        if($this->type == GasUphold::TYPE_SU_CO){
            $res .= $this->contact_tel.', ';
        }
//        if(!empty($this->mAppUserLogin) && $this->mAppUserLogin->role_id != ROLE_CUSTOMER ){
            foreach($this->getSetupPhoneOfCustomer() as $phone){
                if($key != 0){
                    $res .= ', 0'.$phone;
                }else{
                    $res .= '0'.$phone;
                }
                $key++;
            }
//        }
        return $res;
    }

    public function getContactTelSuCo() {// chỉ lấy số gọi lên tổng đài báo sự cố để gửi SMS cho nv BẢO TRÌ 
        return $this->contact_tel;
    }
    
    public function getRatingNote() {
        return $this->rating_note;
    }
    public function updateStatus($status) {
        $this->status = $status;
        $this->update(array('status'));
    }
    public function updateField($aField) {
        $this->update($aField);
    }
    public function getCreatedInfo() {
        $res = MyFormat::dateConverYmdToDmy($this->created_date, "d/m/Y H:i");
        if($this->type == GasUphold::TYPE_SU_CO && $this->status == GasUphold::STATUS_COMPLETE ){
            $res .= "<span class='high_light_tr'></span>";
        }
        return $res;
    }
    public function getCustomerNameChain() {
        if(empty($this->parent_chain_store) || $this->mAppUserLogin->role_id != ROLE_CUSTOMER){
            return '';
        }
        return $this->getCustomer('first_name');
    }
    public function getNextUphold() {
        if(empty($this->next_uphold)){
            return ;
        }

        return MyFormat::dateConverYmdToDmy($this->next_uphold);
    }
    
    
    
    /**
     * @Author: DungNT May 31, 2016
     * @todo: fix lỗi notify nhiều lần cho Điều phối và BT
     * sẽ sử dụng update All để tránh before + after Save
     * $aUpdate = array('status' => EmailQueue::STATUS_SENDING);
     */
    public function updateAllNotNotify($aUpdate) {
        $criteria = new CDbCriteria();
        $criteria->compare('id', $this->id);
        GasUphold::model()->updateAll($aUpdate, $criteria);
    }

    /**
    * @Author: DungNT Oct 27, 2015
    * @Todo: get listing News
    * @param: $q object post params
    * @param:  $mUser model NV bao tri OR KH, or Điều phối
    */
    public function ApiListing($q, $mUser) {
        $mTransactionHistory    = new TransactionHistory();
        $UserRole               = $mUser->role_id;
//        $aUidViewAll = array(GasSupportCustomer::UID_HUAN_DN, GasSupportCustomer::UID_ANH_LTH);
        $aUidViewAll    = [];
        $aRoleSale      = [ROLE_SALE];
        
        $criteria = new CDbCriteria();
        $criteria->order = 't.id DESC';
//        if(in_array($mUser->id, $this->getUidViewHcm())){// Now0617 Khi có sự cố khách hàng TPHCM chuyển qua app thì những người sau đây đều nhận đc app sự cố: Phan Văn Quý, Lương Văn Quang, Trần Phi Thanh, Đinh Trọng Nghĩa.
//            $criteria->addCondition('t.province_id=' . GasProvince::TP_HCM);
//        }else
        if(in_array($UserRole, $aRoleSale)){
            $criteria->addCondition('t.sale_id=' . $mUser->id);
        }elseif(in_array($UserRole, GasUphold::$ROLE_EMPLOYEE_BAOTRI) && !in_array($mUser->id, $aUidViewAll)){
            $criteria->addCondition('t.employee_id=' . $mUser->id);
            $this->handleLimitPlan($criteria, $q);
        }elseif($UserRole == ROLE_DIEU_PHOI){
//            $criteria->compare('t.uid_login', $mUser->id); // Apr 18, 2016 chưa xử lý dc nếu có nhiều điều phối tạo ở nhiều tỉnh
        }elseif($UserRole == ROLE_CUSTOMER){
//            $criteria->addCondition('t.customer_id=' . $mUser->id);
            $criteria->addCondition('t.parent_chain_store='.$mUser->id.' OR (t.customer_id=' . $mUser->id. ')');// Jul2417 load tất cả đơn hàng xuống app cho KH
            if($q->type == GasUphold::TYPE_DINH_KY){
                // với KH thì sẽ có view khác NV Bảo trì
                return self::ApiListingCustomerDinhKy($q, $mUser);
            }
        }elseif($UserRole == ROLE_EXECUTIVE_OFFICER){
            $criteria->addCondition('t.province_id=' . $mUser->province_id);
        }
        
        $criteria->compare('t.type', $q->type);
        if(!empty($q->customer_id) ){
//            throw new Exception("type = $q->type cusid = $q->customer_id");
            $criteria->addCondition("t.customer_id=$q->customer_id");
        }
        
        if($q->type == GasUphold::TYPE_DINH_KY){// Fix Apr 22, 2016
            $mUphold = new GasUphold();
            $mUphold->getCriteriaWebApi($mUser->id, $UserRole, $criteria);
//            $criteria->addNotInCondition("t.schedule_type", GasUphold::$SCHEDULE_IGNORE);// Jul 19, 2016 không load những KH bỏ xuống APP
            $sParamsIn = implode(',', GasUphold::$SCHEDULE_IGNORE);
            $criteria->addCondition("t.schedule_type NOT IN ($sParamsIn)");
            $criteria->addCondition('t.status_uphold_schedule <>'.GasUphold::STATUS_UPHOLD_OFF);
            // Oct 20, 2016 anh Quý nói ko load những KH khai tử vào app nữa
            // Aug 29, 2016 anh Huấn nói bỏ cái này đi, vẫn load xuống app nhưng mà hiện status của nó lên 
        }
        
        if(trim($q->status) != ''){
            $criteria->addCondition("t.status=$q->status");
        }

        $dataProvider=new CActiveDataProvider('GasUphold',array(
            'criteria' => $criteria,
            'pagination'=>array(
                'pageSize' => GasAppOrder::API_LISTING_ITEM_PER_PAGE,
                'currentPage' => (int)$q->page,
            ),
          ));
        return $dataProvider;
    }
    
    /** @Author: DungNT May 18, 2018 
     *  @Todo: xử lý những BT định kỳ được load xuống theo lịch
     **/
    public function handleLimitPlan(&$criteria, $q) {
        if($q->type != GasUphold::TYPE_DINH_KY){
            return ;
        }
        if( !empty($q->customer_id) ){
            return ;
        }
        $tableJoin  = GasUpholdPlan::model()->tableName();
        $criteria->join = "JOIN $tableJoin ON $tableJoin.customer_id = t.customer_id AND $tableJoin.employee_id=t.employee_id";
        $criteria->addCondition($tableJoin.'.uphold_id = 0');
        $criteria->order = "$tableJoin.end_date ASC";
    }
    
    /**
    * @Author: DungNT Dec 02, 2015
    * @Todo: get listing BT định Kỳ của KH
    * @param: $q object post params
    * @param:  $mUser model KH
    */
    public static function ApiListingCustomerDinhKy($q, $mUser) {
        $criteria = new CDbCriteria();
        $criteria->compare('t.type', $q->type);
        $criteria->compare('t.customer_id', $mUser->id);
        $criteria->order = 't.id DESC';
        $dataProvider=new CActiveDataProvider('GasUpholdReply',array(
            'criteria' => $criteria,
            'pagination'=>array(
                'pageSize' => GasAppOrder::API_LISTING_ITEM_PER_PAGE,
                'currentPage' => (int)$q->page,
            ),
          ));
        return $dataProvider;
    }
    
    /**
    * @Author: DungNT Dec 02, 2015
    * @Todo: get model Uphold BT dinh KY của KH chỉ có 1 row
    * @param: $q object post params
    * @param:  $mUser model KH
    */
    public static function ApiGetRowCustomerDinhKy($q, $mUser) {
        $criteria = new CDbCriteria();
        $criteria->compare('t.type', $q->type);
        $criteria->compare('t.customer_id', $mUser->id);
        return self::model()->find($criteria);
    }
    
    /**
     * @Author: DungNT Nov 23, 2015
     * @Todo: cập nhật user đã xem record dưới app
     */
    public function UpdateHasRead() {
        $this->has_read = 1;
        $this->update(array('has_read'));
    }
 
    /**
     * @Author: DungNT Nov 23, 2015
     */
    public function getHasReadText() {
        $aRead = $this->getArrayHasRead();
        return $aRead[$this->has_read];
    }
    
    /**
     * @Author: DungNT Nov 23 2015
     * @Todo: count total and by status
     */
    public static function countByStatusAndDate($uid ,$date, $status, $has_read) {
        $criteria = new CDbCriteria;
        $criteria->addCondition("t.created_date_only='$date' AND t.type=".GasUphold::TYPE_SU_CO);
        if($status){
            $criteria->addCondition("t.status=$status");
        }
        if($has_read != GasUphold::READ_ALL ){
//            $criteria->compare("t.has_read", $has_read);
            $criteria->addCondition("t.has_read=$has_read");
        }
        if($uid){ // nếu empty $uid thì là thống kê tất cả trong ngày
//            $criteria->compare("t.uid_login", $uid);
            $criteria->addCondition("t.uid_login=$uid");
        }
        $cRole  = MyFormat::getCurrentRoleId();
        $cUid   = MyFormat::getCurrentUid();
        $mAppCache = new AppCache();
        $aIdDieuPhoiBoMoi = $mAppCache->getArrayIdRole(ROLE_DIEU_PHOI);
        
        if(in_array($cUid, GasOrders::$ZONE_MIEN_TAY)){// Oct 01, 2016 tach điều phối moi zone thấy
            $sParamsIn = implode(',', GasOrders::$ZONE_MIEN_TAY);
            $criteria->addCondition("t.uid_login IN ($sParamsIn)");
        }elseif(in_array($cUid, $aIdDieuPhoiBoMoi)){// Oct 01, 2016 tach điều phối moi zone thấy
            $sParamsIn  = implode(',', $aIdDieuPhoiBoMoi);
            $criteria->addCondition("t.uid_login IN ($sParamsIn)");
        }

        return self::model()->count($criteria);
    }
    
    /**
     * @Author: DungNT Now 27, 2015
     * @Todo: save multi checkbox day
     */
    public function SaveMultiDay() {
        if($this->schedule_type == GasUphold::SCHEDULE_CO_DINH && isset($_POST['GasUphold']['day_checkbox'])) {
            GasOneManyBig::saveArrOfManyId($this->id, GasOneManyBig::TYPE_UPHOLD_SCHEDULE_DAY, 'GasUphold', 'day_checkbox');
            $this->SaveScheduleInMonth();
        }
    }
    
    /**
     * @Author: DungNT Now 28, 2015
     * @Todo: save multi checkbox day
     */
    public function GetMultiDay() {
        $this->day_checkbox = GasOneManyBig::getArrOfManyId($this->id, GasOneManyBig::TYPE_UPHOLD_SCHEDULE_DAY);
    }
    
    /**
     * @Author: DungNT Now 28, 2015
     * @Todo: get text for multi checkbox day
     */
    public function getScheduleText() {
        $aTypeNew = array(GasUphold::SCHEDULE_KHONG_CO_DINH_2_THANG, GasUphold::SCHEDULE_KHONG_CO_DINH_3_THANG, GasUphold::SCHEDULE_BIG_CUSTOMER);
        if($this->type == GasUphold::TYPE_DINH_KY){ // nếu là BT định kỳ thì mới get info này
            if($this->schedule_type == GasUphold::SCHEDULE_CO_DINH){
                $day_checkbox = GasOneManyBig::getArrOfManyId($this->id, GasOneManyBig::TYPE_UPHOLD_SCHEDULE_DAY);   
                return "Ngày ".implode(", ", $day_checkbox)." hàng tháng";
            }elseif($this->schedule_type == GasUphold::SCHEDULE_KHONG_CO_DINH){
                return $this->schedule_in_month." lần mỗi tháng";
            }elseif(in_array($this->schedule_type, $aTypeNew) ){
                return $this->getArrayScheduleTypeText();// Feb 16, 2016 fix cho KH BT 2 hoac 3 thang 1 lan
            }
        }
        return '';
    }
    
    public function getReportWrongApi(){
        if($this->report_wrong){
            return "Khách hàng báo sai sự cố";
        }
        return '';
    }
    
    
    /**
     * @Author: DungNT Dec 02, 2015
     * @Todo: xử lý tạo sẵn 2 notify cách nhau 10 phút đối với bảo trì sự cố
     * @note_build_notify_1: gửi cho NV bảo trì + Điều Phối
     */
    public function HandleAddCronAlert() {
        if( $this->isChangeEmployee() || ($this->isNewRecord && $this->type == GasUphold::TYPE_SU_CO && !empty($this->employee_id) ) ){
            $Alert1 = MyFormat::addDays(date('Y-m-d H:i:s'), GasUphold::NOTIFY_TIME_ALERT, "+", 'minutes', 'Y-m-d H:i:s');
            $Alert2 = MyFormat::addDays(date('Y-m-d H:i:s'), (2*GasUphold::NOTIFY_TIME_ALERT), "+", 'minutes', 'Y-m-d H:i:s');
            $message1 = "Nhắc nhở lần 1 - {$this->getTextUpholdType()}: {$this->getCustomer()}";
            $message2 = "Nhắc nhở lần 2 - {$this->getTextUpholdType()}: {$this->getCustomer()}";
            $json_var = '';
            // 1. Nv Bảo trì
            // InsertRecord($user_id, $type, $obj_id, $time_send, $title, $json_var)
            GasScheduleNotify::InsertRecord($this->employee_id, GasScheduleNotify::UPHOLD_ALERT_10, $this->id, $Alert1, $message1, $json_var);
            GasScheduleNotify::InsertRecord($this->employee_id, GasScheduleNotify::UPHOLD_ALERT_10, $this->id, $Alert2, $message2, $json_var);
            // Apr 12, 2017, gửi nhắc nhở lần 2 cho a Quý
            GasScheduleNotify::InsertRecord(GasSupportCustomer::UID_QUY_PV, GasScheduleNotify::UPHOLD_ALERT_10, $this->id, $Alert2, $message2, $json_var);
            // 2. NV Điều Phối - bug vì uid_login ở đây là KH
//            GasScheduleNotify::InsertRecord($this->uid_login, GasScheduleNotify::UPHOLD_ALERT_10, $this->id, $Alert1, $message1, $json_var);
//            GasScheduleNotify::InsertRecord($this->uid_login, GasScheduleNotify::UPHOLD_ALERT_10, $this->id, $Alert2, $message2, $json_var);
//            foreach(GasTickets::$UID_DIEU_PHOI as $uid){// Apr 21, 2016 fix bug Dieu Phoi nhân notify lần 1+2
//                GasScheduleNotify::InsertRecord($uid, GasScheduleNotify::UPHOLD_ALERT_10, $this->id, $Alert1, $message1, $json_var);
//                GasScheduleNotify::InsertRecord($uid, GasScheduleNotify::UPHOLD_ALERT_10, $this->id, $Alert2, $message2, $json_var);
//            }
        }
        
        if($this->isNewRecord){// Apr 14, 2017 thêm 1 notify cho KH sau khi tạo 1 phút
            $message1 = 'Hệ thống đã tiếp nhận yêu cầu bảo trì của khách hàng '.$this->getCustomer();
            $json_var = '';
            GasScheduleNotify::InsertRecord($this->customer_id, GasScheduleNotify::UPHOLD_CREATE, $this->id, '', $message1, $json_var);
        }
    }
    
    /**
     * @Author: DungNT Dec 19, 2015
     * @Todo: build list notify cho BT định kỳ vào đầu tháng
     * @note_build_notify_2: gửi cho KH và NV bảo trì
     */
    public static function NotifyDinhKyBuildList() {
        return ;// Aug 29, 2016 tạm bỏ đi vì không cần thiết lắm, sẽ review lại sau
        $cDay = date('d');
        $aCustomerId = array();
        $models = self::NotifyDinhKyGetData();
        foreach($models as $mUphold){
            if(!$mUphold->getStatusUpholdSchedule(true)){// nếu KH không lấy hàn nữa thì không notify
                continue;
            }
            $mUphold->GetMultiDay();
            if(is_array($mUphold->day_checkbox)){
                foreach($mUphold->day_checkbox as $day) {
                    // 1. chỉ tạo khi send cront => tạo 1 record mới của Reply 
                    // 2. insert vào notify
                    if($day<10){
                        $day = "0$day";
                    }
                    $message = "Nhắc nhở bảo trì định kỳ: {$mUphold->getCustomer()}";
                    $aCustomerId [$mUphold->customer_id] = $mUphold->customer_id;
                    $time_send = date('Y')."-".date('m')."-".$day." 06:00:00";// 6h mỗi sáng sẽ chạy cron send notify
                    $json_var = '';
//                    $time_send = date('Y')."-".date('m')."-03 06:00:00";// only for test
                                       // InsertRecord($user_id, $type, $obj_id, $time_send, $title, $json_var='') 
                    GasScheduleNotify::InsertRecord($mUphold->customer_id, GasScheduleNotify::UPHOLD_DINH_KY_1_DAY, $mUphold->id, $time_send, $message, $json_var);
                    GasScheduleNotify::InsertRecord($mUphold->employee_id, GasScheduleNotify::UPHOLD_DINH_KY_1_DAY, $mUphold->id, $time_send, $message, $json_var);
                    // gửi cho KH và NV Bảo Trì
//                    ở đây chỉ build 1 record của $mUphold->customer_id còn employee_id thì khi chạy cron tự động thêm vào
//                    câu trên là sai, sẽ build riêng từng notify cho từng user, để đảm bảo user phải nhận được notify
                }
            }
            if($mUphold->schedule_type == GasUphold::SCHEDULE_CO_DINH){
                $mUphold->month_notify = date('m');
                $mUphold->update(array('month_notify'));
            }
        }
//        GasUphold::SendMailAdminConfirm($aCustomerId);// Open when live
    }
    
        /**
     * @Author: DungNT Dec 22, 2015
     * @Todo: count cron notify 10' cho NV bao tri (employee_id)
     * send notify 2 lần, mỗi lần cách nhau 10'cho NV bảo trì
     * AND plus 1 to notify_count
    *  @param: $mNotifyHistory model notify history
     */
    public function NotifyCountAlertUpdate($mNotifyHistory) {
        $mUser = Users::model()->findByPk($mNotifyHistory->user_id);
        // Dec 22, 2015  mà sẽ update khi client gửi request lên là đã đọc tin rồi
        if($mUser && in_array($mUser->role_id, GasUphold::$ROLE_EMPLOYEE_SEARCH_AUTOCOMPLETE)){
            if(empty($this->notify_count) || is_null($this->notify_count)){
                $this->notify_count = 1;
            }else{
                $this->notify_count += 1;
            }
            $this->update(array('notify_count'));
        }
    }   
    
    /**
     * @Author: DungNT Dec 07, 2015
     * @todo: gửi mail cho admin xác nhận đã chạy cron của Bảo trì vào ngày 1 hàng tháng
     */
    public static function SendMailAdminConfirm($aCustomerId) {
        $sSubject = "Xác nhận đã chạy cron của Bt định Kỳ tháng ".date('m');
        $sBody = "Gửi cho các customer sau: ". implode(", ", $aCustomerId);
        $sTo = "dung.nt@verzdesign.com.sg";
        CmsEmail::AdminSendAnyContent($sSubject, $sBody, $sTo);
    }
    
    /**
     * @Author: DungNT Dec 02, 2015
     */
    public static function NotifyDinhKyGetData() {
        $cMonth = date('m');
        $criteria = new CDbCriteria();
        $criteria->compare("t.type", GasUphold::TYPE_DINH_KY);
        $criteria->addCondition("t.month_notify <> $cMonth");
        $criteria->addNotInCondition("t.schedule_type", GasUphold::$SCHEDULE_IGNORE);
        return self::model()->findAll($criteria);
    }

    /**
     * @Author: DungNT Dec 09, 2015
     * @Todo: $getOnlyStatus chỉ lấy số, không lấy text
     * nếu $getOnlyStatus = true thì return false nếu không lấy hàng, true nếu còn lấy hàng
     */
    public function getStatusUpholdSchedule($getOnlyStatus = false) {
        $mUser = $this->rCustomer;
        if($mUser){
            if($getOnlyStatus){// biến này sử dụng trong cron để xem có build notify cho BT định kỳ của KH này không
                if($mUser->channel_id == Users::KHONG_LAY_HANG){
                    return false;
                }
                return true;
            }
       }
       $aStatusSchedule = $this->getArrayStatusSchedule();
       return $aStatusSchedule[$this->status_uphold_schedule];
    }
    
    /**
     * @Author: DungNT Dec 31, 2015
     * @Todo: api get list nv bao tri
     * in_array($mUser->role_id, GasUphold::$ROLE_EMPLOYEE_BAOTRI)
     */
    public static function apiGetEmployeeMaintain() {
//        ROLE_E_MAINTAIN
        $criteria = new CDbCriteria();
        $sParamsIn = implode(',', GasUphold::$ROLE_EMPLOYEE_BAOTRI);
        $criteria->addCondition("t.role_id IN ($sParamsIn)");
        $criteria->compare("t.status", STATUS_ACTIVE);
        $models = Users::model()->findAll($criteria);
        $aRes = array();
        foreach($models as $model)
        {
            $aRes[$model->id] = $model->first_name." - ".$model->getTypeCustomerText();
        }
        $temp = array(''=>"Chọn NV Bảo Trì");
        return $temp + $aRes;
    }
    
    /**
     * @Author: DungNT Dec 31, 2015
     * @Todo: api get list type_uphold: loai su co
     */
    public static function apiGetTypeUphold() {
        $model = new GasUphold();
//        $temp = array(''=>"Chọn Loại Sự Cố");
//        return $temp + $model->getArrayUpholdType();
        return $model->getArrayUpholdType();
    }
    
    /***** Jan 05, 2016 SMS build notify ******/
        
    /** @NOTE các notify của 1 function nên viết liền nhau để còn review không bị sót
     * @Author: DungNT Jan 05, 2016
     * @Todo: notify SMS cho nv Bảo trì after create new sự cố
     */
    public function NotifySmsCreate() {
         /* Kịch bản khi send SMS  => send NVBT
            * SMS_U_01 1/ điều phối tạo => send NVBT
            * SMS_U_02 2/ Kh tạo ở app => send NVBT
            */
        if( $this->scenario == 'ApiCreate' || 
            ( $this->isNewRecord && $this->scenario == 'create' && $this->type == GasUphold::TYPE_SU_CO ) ){
            $this->NotifySmsCreateSending($this->employee_id, $this->getSMSContentCreate(), GasScheduleSms::TYPE_UPHOLD);
        }
    }
    
    /** 
     * @Author: DungNT Jun 10, 2016
     * @Todo: notify SMS cho KH after NV BT xác nhận xử lý sự cố
     * SMS_U_04 4/ NV bảo trì xác nhận xử lý => send KH
     */
    public function NotifySmsReplyConfirmToCustomer($mReply) {
        if($this->type == GasUphold::TYPE_SU_CO && $this->role_id == ROLE_CUSTOMER){
            return ;// không gửi SMS cho KH đặt qua app
        }
//        return ;// Jun 23, 2016 Open send cho KH - Jun 15, 2016 Chưa gửi cho KH
        if( ($this->type == GasUphold::TYPE_SU_CO && $mReply->status == GasUphold::STATUS_HANLDE) 
            || ($this->type == GasUphold::TYPE_DINH_KY && $mReply->status == GasUphold::STATUS_CUSTOMER_CHECKIN)
        ){
            $this->NotifySmsCreateSending($this->customer_id, $this->getSMSContentConfirmToCustomer($mReply), GasScheduleSms::TYPE_UPHOLD_CUSTOMER);
        }
    }
    
    /**
     * @Author: DungNT Jun 10, 2016
     * @Todo: build insert record and doSend
     */
    public function NotifySmsCreateSending($SendToUid, $SMSContent, $type) {
//        return ;// only for dev test, return là không gửi dc tin SMS
        if(empty($SendToUid) || !GasCheck::isServerLive()){
            return '';
        }
        $aPhone = [];
        
        if($type == GasScheduleSms::TYPE_UPHOLD){// gửi cho NV thì lấy ở system
//            $aPhone[] = $this->getPhoneUser($SendToUid);// close May1818 do không gửi full được
            $this->getAllPhoneUser($SendToUid, $aPhone);
        }elseif($type == GasScheduleSms::TYPE_UPHOLD_CUSTOMER){
//            if(!is_null($this->mEmployee) && trim($this->mEmployee->getPhone()) == ''){// Jun 24, 2016 Nếu nv bảo trì không có số điện thoại thì sẽ không gửi tin
//                return '';
//            }// close on Feb0518

            // nếu là gửi cho KH thì lấy số phone mới nhập khi tạo mới BT sự cố
            if($this->type == GasUphold::TYPE_SU_CO){
                $phoneCall              = UsersPhone::formatPhoneSaveAndSearch($this->contact_tel);
                $aPhone[$phoneCall]     = $phoneCall;
            }
            $this->getMorePhoneOfCustomer($aPhone);
        }else{
            return '';
        }
        foreach($aPhone as $phone){
            $this->RunInsertNotifySms($this->uid_login, $SendToUid, $phone, $SMSContent, $type); // Jun 10, 2016
        }
        
        // run insert xong thì gửi luôn cho nv bảo trì 
//      Feb0518 close lại, cho gửi qua cron =>  $mScheduleSms = GasScheduleSms::model()->findByPk($schedule_sms_id);
//        $mScheduleSms->doSend();
    }
    
    /** @Author: DungNT May 28, 2018
     *  @Todo: build array số đt setup để nhân tin bảo trì của KH
     * Xử lý bỏ số phone bị trùng khi gửi sms, Số gọi lên sự cố trùng với số setup của KH
     **/
    public function getMorePhoneOfCustomer(&$aPhone) {
        $aPhoneSetup = $this->getSetupPhoneOfCustomer();
        foreach($aPhoneSetup as $phone){
            $aPhone[$phone] = $phone;
        }
    }
    
    /** @Author: DungNT Feb 05, 2018
     *  @Todo: lấy thêm số đt setup để nhân tin bảo trì của KH
     **/
    public function getSetupPhoneOfCustomer() {
        $model = new UsersPhoneExt2();
        $model->user_id = $this->customer_id;
        return $model->getPhoneOfCustomerByType(UsersPhoneExt2::TYPE_BAOTRI_BO_MOI);
    }
    
    /** @Author: DungNT Jul 02, 2016
     *  @Todo: get phone of user
     */
    public function getPhoneUser($uid) {
        $mUserPhone = UsersPhone::getOnePhoneNumberByUserId($uid);
        if(is_null($mUserPhone)){
            return '';
        }
        return $mUserPhone->phone;
    }
    /** @Author: DungNT Jul 02, 2016
     *  @Todo: get phone of user
     */
    public function getAllPhoneUser($uid, &$aRes) {
        $mUserPhone = new UsersPhone();
        $mUserPhone->user_id = $uid;
        $models = $mUserPhone->getAllPhoneNumberByUserId();
        foreach($models as $item){
            $aRes[$item->phone] = $item->phone;
        }
    }
    
    /**
     * @Author: DungNT Jun 10, 2016
     * @Todo: get sms content notify to employee
     */
    public function getSMSContentCreate() {
        // Template 1/  Su co: Xi Gas\nSDT: 132467589\nLien he: Nguyen Tien Dung
        $customerHouseNumbers = $customerName = '';// add Mar 07, 2017
        $mCustomer = $this->rCustomer;
        if($mCustomer){
            $customerName           = $mCustomer->getFullName();
            $customerHouseNumbers   = $mCustomer->house_numbers.", ".$mCustomer->getStreet();
        }
        $title = "{$this->getTextUpholdType()}\nDT: {$this->getContactTelSuCo()}\nLH: {$this->getContactPerson()}\n$customerName\n$customerHouseNumbers";
        return MyFunctionCustom::ShortenStringSMS($title, GasScheduleSms::WIDTH_SMS);
    }
    
    /**
     * @Author: DungNT Jun 10, 2016
     * @todo: gửi SMS cho KH khi NV BT xác nhận xử lý
     */
    public function getSMSContentConfirmToCustomer($mReply) {
        $mEmployee = Users::model()->findByPk($this->employee_id);
        $this->mEmployee = $mEmployee;
        // Template 1/  Su co: Xi Gas\nSDT: 132467589\nLien he: Nguyen Tien Dung
//        return "Dich Vu Bao Tri\nNV: {$mEmployee->getFullName()}\nSDT: {$mEmployee->getPhone()}\nThoi gian den: {$mReply->getDateTimeHandleSMS()}\nDich vu bao tri mien phi";
//        "DV Bao Tri Mien Phi\nNV: Dinh Trong Nghia\nSDT: 123456789\nMS: NGHIA0002\nThoi gian den: 09:30 17/06/16\nChi tiet lien he: 0918318328\nTran trong cam on!";
//        DV Bao Tri Mien Phi 24/7\nMa so: 123456\nNV: Dinh Trong Nghia\nSDT: 123456789\nThoi gian den: 09:30 17/06/16\nHotline: 19001521\nChi tiet lien he: 0918318328

        $code_account = '';
        if(trim($mEmployee->code_account) != ''){
            $code_account = "\nMS: ".$mEmployee->code_account;
        }
        $aPhone         = explode('-', $mEmployee->getPhone());
        $phoneNumber    = isset($aPhone[0]) ? $aPhone[0] : '';
        if($this->type == GasUphold::TYPE_SU_CO){
//            return "DV Bao Tri Mien Phi 24/7\nMa so: {$this->code_complete}\nNV: {$mEmployee->getFullName()}\nSDT: {$phoneNumber}\nThoi gian den: {$mReply->getDateTimeHandleSMS()}\nHotline: 19001521\nChi tiet lien he: 0918318328";
//            DV Bao Tri Mien Phi 24/7\nMa so: 685459\nNV: Phan Van Quy\nSDT: 0982135769\nThoi gian den: 15:20\nGop y dich vu Hotline: 0918318328
//            return "DV bao tri Gas mien phi\nMa so: {$this->code_complete}\nNV: {$mEmployee->getFullName()}\nSDT: {$phoneNumber}\nThoi gian den: {$mReply->getDateTimeHandleSMS()}\nGOP Y DICH VU Hotline: 0918318328";// Close Mar1419
            return "DV bảo trì Gas miễn phí\nNVBT: {$mEmployee->getFullName()}: {$phoneNumber}\nThời gian đến: {$mReply->getDateTimeHandleSMS()}\nLưu ý \"Chỉ cho Mã {$this->code_complete}\" khi NV hoàn thành tốt \nNếu \"Không tốt\" gọi ngay: 0918318328";
        }else{
//            return "DV bao tri Gas dinh ky mien phi\nMa so: {$this->code_complete}\nNV: {$mEmployee->getFullName()}\nSDT: {$phoneNumber}\nHotline: 0918318328";
            return "DV bảo trì Gas định kỳ miễn phí\nNVBT: {$mEmployee->getFullName()}: {$phoneNumber}\nLưu ý \"Chỉ cho Mã {$this->code_complete}\" khi NV hoàn thành tốt \nNếu \"Không tốt\" gọi ngay: 0918318328";
        }
    }
    
    /**
     * @Author: DungNT Jan 05, 2016
     * @Todo: insert to table notify
     * @Param: $aUid array user id need notify
     */
    public function RunInsertNotifySms($uid_login, $user_id, $phone, $title, $type) {
//        $title = "Nội Dung \ntiếng Việt có Dấu, vui lòng \nkiểm tra \nliên hệ máy bàn";
        $json_var = array('');
        return GasScheduleSms::InsertRecord($uid_login, $user_id, $phone, $type, GasScheduleSms::ContentTypeCoDau, $this->id, '', $title, $json_var);
        // Có thể không gửi luôn ở hàm này, vì có case sẽ không gửi luôn
    }
    
    /***** Jan 05, 2016 SMS build notify ******/

    /**
     * @Author: DungNT Jan 31, 2016
     * @Todo: get criteria for Usert => list customer Bo, Moi cho Bao tri Dinh KY
     * @belong_to: Users::getCustomerUpholdSchedule()
     */
    public static function getCustomerUpholdSchedule(&$criteria) {
        $type = GasUphold::TYPE_DINH_KY;
        $criteria->addCondition(" t.id NOT IN ( SELECT DISTINCT u.customer_id FROM `gas_gas_uphold` as u WHERE u.type=$type )");
    }
    
        /**
     * @Author: DungNT Now 19, 2015
     */
    public function ResetScenario() {
        $this->scenario = "createSchedule".$this->schedule_type;
    }
    
    /**
     * @Author: DungNT Jan 31, 2016
     * @Todo: save multi record of bao tri dinh ky upholdSchedule
     */
    public function SaveMultiUpholdSchedule() {
        if(isset($_POST['customer_id'])){
            foreach($_POST['customer_id'] as $key=>$customer_id) {
                $model = new GasUphold();
                $model->customer_id = $customer_id;
                $model->type = GasUphold::TYPE_DINH_KY;
                $model->employee_id         = isset($_POST['employee_id'][$key])?$_POST['employee_id'][$key]:0;
                $model->schedule_type       = isset($_POST['schedule_type'][$key])?$_POST['schedule_type'][$key]:0;
                $model->schedule_in_month   = isset($_POST['schedule_in_month'][$key])?$_POST['schedule_in_month'][$key]:0;
                $day_checkbox               = isset($_POST['day_checkbox'][$key])?$_POST['day_checkbox'][$key]:array();
//                if($this->schedule_type == GasUphold::SCHEDULE_CO_DINH && isset($_POST['GasUphold']['day_checkbox'])) {
                if($model->schedule_type == GasUphold::SCHEDULE_CO_DINH && is_array($day_checkbox) && count($day_checkbox)) {
                    $_POST['GasUphold']['day_checkbox'] = $day_checkbox;
                    $model->day_checkbox = $day_checkbox;
                }
                $model->content         = isset($_POST['content'][$key])?$_POST['content'][$key]:'';
                $model->contact_person  = isset($_POST['contact_person'][$key])?$_POST['contact_person'][$key]:'';
                $model->contact_tel     = isset($_POST['contact_tel'][$key])?$_POST['contact_tel'][$key]:'';
                $model->next_uphold     = !empty($_POST['GasUphold']['next_uphold'][$key])? MyFormat::dateConverDmyToYmd($_POST['GasUphold']['next_uphold'][$key]) : '';
                if(!empty($model->contact_tel)){
                    $temp = explode('-', $model->contact_tel);
                    $model->contact_tel = isset($temp[0]) ? $temp[0] : '';
                }
                $model->manage_id       = $this->manage_id;
                $model->ResetScenario();
                $model->validate();
                if(empty($model->employee_id)){
                    continue ;
                }
                
                if(!$model->hasErrors()){
                    $model->save();
                    $model->SaveMultiDay();
                }else{
                    $error = HandleLabel::FortmatErrorsModel($model->getErrors());
                    throw new Exception($error);
                }
            }
        }
    }
    
    /** @ANH_DUNG Mar 11, 2016
     * @to do: check can update BT Đinh kỳ
     */
    public function CanUpdateUpholdSchedule()
    {
        $cRole  = MyFormat::getCurrentRoleId();
        $cUid   = MyFormat::getCurrentUid();
        $cMonth = date('m')*1;

//        if($this->manage_id == GasSupportCustomer::UID_QUY_PV && in_array($cUid, $this->getUidMixUpdate())){
        if($cUid == GasSupportCustomer::UID_QUY_PV || $cRole == ROLE_ADMIN){
            return true;
        }
//        if($this->type_customer == STORE_CARD_KH_MOI && $this->sale_id == $cUid){
//            return true;// Oct1317 cho phép Sale Mối tự sửa BT của mình
//        }// Feb2418 tạm close lại, vì thấy ko sử dụng
        
//        if($this->type_customer == STORE_CARD_KH_MOI && $cUid == $this->manage_id && in_array($cUid, $this->getUidUpdateSchedule())){
        if($this->type_customer == STORE_CARD_KH_MOI && in_array($cUid, $this->getUidUpdateSchedule())){
            return true;// Aug2518 Mở cho GDKV sửa - Jun0718 đóng lại cho mình anh Quý Sửa
        }// Feb2418 tạm close lại, vì thấy ko sử dụng

        if($cUid != $this->uid_login){ // Close on Apr 13, 2016 cho phép anh Triều và các User khác sửa của nhau
            return false; // cho phép admin sửa hết, còn những user khác thì ai tạo thì dc sửa của người đó
        }// Jan 22, 2016 cho phép điều phối sửa của nhau
//        $dayUpdate = $this->getDayAllowUpdate();
        $dayUpdate = 10;
        
        $dayAllow = date('Y-m-d');
        $dayAllow = MyFormat::modifyDays($dayAllow, $dayUpdate, '-');
        return MyFormat::compareTwoDate($this->created_date, $dayAllow);
    }
    
    /**
     * @Author: DungNT Apr 03, 2016
     * @Todo: report loai su co
     */
    public function ReportByType() {
        $date_from = "$this->report_year-01-01";
        $date_to = "$this->report_year-12-31";
        $criteria = new CDbCriteria();
        $criteria->compare("t.type", GasUphold::TYPE_SU_CO);
        $criteria->compare("t.employee_id", $this->employee_id);
        $criteria->addBetweenCondition("t.created_date_only", $date_from, $date_to);
        $criteria->select = "count(t.id) as id, t.type_uphold, month(t.created_date_only) created_date_only";
        $criteria->group = "month(t.created_date_only), t.type_uphold";
        $models = self::model()->findAll($criteria);
        $aRes = array();
        $this->ReportByTypeData($aRes, $models);
        return $aRes;
    }
    
    /**
     * @Author: DungNT Apr 03, 2016
     */
    public function ReportByTypeData(&$aRes, $models) {
        foreach($models as $model){
            $aRes[$model->created_date_only][$model->type_uphold] = $model->id;
        }
    }

    /**
     * @Author: DungNT Apr 03, 2016
     * @Todo: report theo khach hang
     */
    public function ReportByCustomer() {
        $date_from = MyFormat::dateDmyToYmdForAllIndexSearch($this->date_from);
        $date_to = MyFormat::dateDmyToYmdForAllIndexSearch($this->date_to);

        // 1. thống kê bảo trì
        $criteria = new CDbCriteria();
        $criteria->compare("t.type", GasUphold::TYPE_SU_CO);
        $criteria->addBetweenCondition("t.created_date_only", $date_from, $date_to);
        $criteria->select = "count(t.id) as id, t.customer_id";
        $criteria->group = "t.customer_id";
        $criteria->order = "count(t.id) DESC";
        $criteria->limit = $this->limit_report;
        $models = self::model()->findAll($criteria);
        $aRes = array();
        $this->ReportByCustomerData($aRes, $models);
        // 2. lấy dữ liệu sản lượng
        $mStoreCard = new GasStoreCard();
        $mStoreCard->date_from = $this->date_from;
        $mStoreCard->date_to = $this->date_to;
        $mStoreCard->customer_id = $aRes['ARR_CUSTOMER_ID'];
        $aRes['OUTPUT'] = Sta2::OutputCustomer($mStoreCard);
        
        return $aRes;
    }
    
    public function ReportByCustomerData(&$aRes, $models) {
        $aCustomerId = array();
        foreach($models as $model){
            $aRes['VALUE'][$model->customer_id] = $model->id;
            $aCustomerId[] = $model->customer_id;
        }
        $aRes['MODEL_CUSTOMER'] = Users::getArrayModelByArrayId($aCustomerId);
        $aRes['ARR_CUSTOMER_ID'] = $aCustomerId;
//        $aRole = array(ROLE_SALE, ROLE_E_MAINTAIN);
        $aRes['MODEL_SALE_BT'] = Users::getArrObjectUserByRoleHaveOrder(GasUphold::$ROLE_EMPLOYEE_BAOTRI);
        $aRes['ID_CUSTOMER_MAINTAIN'] = $this->getListEmployeeDinhKy($aCustomerId);
    }
    
    /**
     * @Author: DungNT Apr 03, 2016
     * @Todo: get list employee định kỳ theo list customer
     */
    public function getListEmployeeDinhKy($aCustomerId) {
        $criteria = new CDbCriteria();
        $criteria->compare("t.type", GasUphold::TYPE_DINH_KY);
//        $criteria->addInCondition("t.customer_id", $aCustomerId);
        $sParamsIn = implode(',', $aCustomerId);
        if(!empty($sParamsIn)){
            $criteria->addCondition("t.customer_id IN ($sParamsIn)");
        }
        $models = self::model()->findAll($criteria);
        $aRes = array();
        foreach($models as $model){
            $aRes[$model->customer_id] = $model->employee_id;
        }
        return $aRes;
    }
    
     /**
     * @Author: DungNT Apr 04, 2016
     * @Todo: report theo nhân viên
     */
    public function ReportByEmployee() {
        $date_from = MyFormat::dateDmyToYmdForAllIndexSearch($this->date_from);
        $date_to = MyFormat::dateDmyToYmdForAllIndexSearch($this->date_to);

        // 1. thống kê bảo trì
        $criteria = new CDbCriteria();
        $criteria->compare("t.type", GasUphold::TYPE_SU_CO);
        $criteria->addBetweenCondition("t.created_date_only", $date_from, $date_to);
        $criteria->select = "count(t.id) as id, t.employee_id, t.type_uphold";
        $criteria->group = "t.employee_id, t.type_uphold";
        $models = self::model()->findAll($criteria);
        $aRes = array();
        $this->ReportByEmployeeData($aRes, $models);
        return $aRes;
    }
    
    public function ReportByEmployeeData(&$aRes, $models) {
        foreach($models as $model){
            $aRes['VALUE'][$model->employee_id][$model->type_uphold] = $model->id;
        }
        $aRes['MODEL_SALE_BT'] = Users::getArrObjectUserByRoleHaveOrder(GasUphold::$ROLE_EMPLOYEE_BAOTRI);
    }
    
    /**
     * @Author: DungNT Jun 15, 2016
     * @Todo: report bảo trì định kỳ nhân viên
     */
    public function ReportUpholdSchedule() {
        // 1. thống kê bảo trì
        $criteria = new CDbCriteria();
        $criteria->compare("t.type", GasUphold::TYPE_DINH_KY);
//        $criteria->compare("t.type", GasUphold::TYPE_DINH_KY);
        $criteria->addCondition('t.status_uphold_schedule <>'.GasUphold::STATUS_UPHOLD_OFF);
        $criteria->select = "count(t.id) as id, t.employee_id, t.schedule_type";
        $criteria->group = "t.employee_id, t.schedule_type";
        $criteria->order = "t.role_id DESC, count(t.id) DESC";
        $models = self::model()->findAll($criteria);
        $aRes = array();
        foreach($models as $model){
            $aRes['VALUE'][$model->employee_id][$model->schedule_type] = $model->id;
        }
        $aRes['MODEL_SALE_BT'] = Users::getArrObjectUserByRoleHaveOrder(GasUphold::$ROLE_EMPLOYEE_BAOTRI);
        return $aRes;
    }

    /**
     * @Author: DungNT Apr 13, 2016
     * @Todo: get nv Bảo  trì định kỳ của KH để gắn vào sự cố khi KH tạo mới 1 BT sự cố trên app
     * @Param: $customer_id
     */
    public static function getEmployeeOfCustomer($customer_id) {
        $criteria = new CDbCriteria();
        $criteria->compare("t.customer_id", $customer_id);
        $criteria->compare("t.type", GasUphold::TYPE_DINH_KY);
        $model = self::model()->find($criteria);
        if($model){
            return $model->employee_id;
        }
        return '';
    }
    
    /**
     * @Author: DungNT Apr 16, 2016
     * @Todo: check when customer can rating bao tri
     */
    public function canRating() {
        $dayUpdate = 2;
        $dayAllow = date('Y-m-d');
        $dayAllow = MyFormat::modifyDays($dayAllow, $dayUpdate, '-');
        return MyFormat::compareTwoDate($this->created_date, $dayAllow);
    }
    
    /**
     * @Author: DungNT Apr 16, 2016
     * @Todo: xử lý không phải change lại code bên dưới nhiều nữa
     * sẽ trả xuống array Kiểu id=>name cho những biến viết thêm sau này
     */
    public function apiGetDataCustom() {
        $sRatingTypeValue = HandleLabel::JsonIdName(GasUpholdRating::getByUpholdId($this->id, array('listData'=>1)));
        return array(
            array(
                'id'=>'rating_status',
                'name'=>$this->rating_status,
            ),
            array(
                'id'=>'rating_type',
                'name'=> $sRatingTypeValue,
            ),
            array(
                'id'=>'rating_note',
                'name'=> $this->rating_note,
            ),
        );
    }
    
    /**
     * @Author: DungNT Apr 21, 2016
     * @Todo: lấy bảo trì sự cố mới nhất của KH
     */
    public static function getLatestUphold($customer_id) {
        $criteria = new CDbCriteria();
        $criteria->compare("t.customer_id", $customer_id);
        $criteria->compare("t.type", GasUphold::TYPE_SU_CO);
        $criteria->compare("t.status", GasUphold::STATUS_COMPLETE);
        $criteria->limit = 1;
        $criteria->order = "t.id DESC";
        return self::model()->find($criteria);
    }
    
    /**
     * @Author: DungNT Apr 21, 2016
     * @Todo: build array item bảo trì sự cố mới nhất của KH
     */
    public static function apiGetLatestUphold($mUser, &$aRes) {
        if($mUser->role_id == ROLE_CUSTOMER){
            $mUpholdLatest = GasUphold::getLatestUphold($mUser->id);
            $uphold_id_lastest = '';
            if($mUpholdLatest){
                $uphold_id_lastest = $mUpholdLatest->id;
            }
            $aRes[] = array(
                'id' => "uphold_id_lastest",
                'name' => $uphold_id_lastest,
            );
        }
    }
    
    /**
     * @Author: DungNT Apr 21, 2016
     * @Todo: only remove BT dinh ky for fix
     */
    public static function DevRemoveUpholdSchedule() {
        $criteria = new CDbCriteria();
        $criteria->compare("t.type", GasUphold::TYPE_DINH_KY);
        $models = self::model()->findAll($criteria);
        echo '<pre>';
        print_r(count($models));
        echo '</pre>';
        die;
//        Users::deleteArrModel($models);
    }
    
    /**
     * @Author: DungNT Jun 08, 2016
     * @Todo: set status của bt định kỳ $item->status_uphold_schedule = GasUphold::STATUS_UPHOLD_OFF;
     * @Param: $aModelCustomer array model User
     * 1/ array customer ko lay hang 60 ngày 
     * 2/ array customer chan hang
     */
    public static function setStatusUpholdSchedule($aModelCustomer) {
        if(count($aModelCustomer) < 1){
            return ;
        }
        $aIdCustomer = CHtml::listData($aModelCustomer, "id", "id");
        $criteria=new CDbCriteria;
//        $criteria->addInCondition('customer_id', $aIdCustomer);
        $sParamsIn = implode(',', $aIdCustomer);
        $criteria->addCondition("customer_id IN ($sParamsIn)");
        $criteria->compare('status_uphold_schedule', GasUphold::STATUS_UPHOLD_RUNNING);
        $criteria->compare("type", GasUphold::TYPE_DINH_KY);
//        $aUpdate = array('status_uphold_schedule' => GasUphold::STATUS_UPHOLD_OFF);
//        $CountData = self::model()->count($criteria);
//        GasUphold::model()->updateAll($aUpdate, $criteria);// Close May2518 Không update all nữa, find ra rồi foreach
        $models = GasUphold::model()->findAll($criteria);
        foreach($models as $item){
            $item->status_uphold_schedule = GasUphold::STATUS_UPHOLD_OFF;
            $item->update(['status_uphold_schedule']);
            $item->removePlanNotDone();
        }
        Logger::WriteLog(count($models)." record Uphold DINH KY SET OFF ********* run Cron setStatusUpholdSchedule ");
    }
    /** @Author: DungNT Jan 03, 2018
     *  @Todo: set những KH chặn hàng sang trạng thái không bảo trì nữa
     **/
    public function lockCustomerChanHang() {
        $criteria = new CDbCriteria();
        UpdateSql::getCriteriaCustomerStorecard($criteria);
        $criteria->addInCondition('channel_id', [Users::CHAN_HANG]);
        $models = Users::model()->findAll($criteria);
        GasUphold::setStatusUpholdSchedule($models);
        if(count($models)){
            Logger::WriteLog('Lock bảo trì Customer chặn hàng '. count($models));
        }
    }
    
    /** @Author: DungNT Jan 18, 2018
     *  @Todo: quyét những KH Ngưng bảo trì, xem có lấy hàng lại chưa, nếu rồi thì bật lại bảo trì
     **/
    public function reEnableUphold() {
        $from = time();
        ini_set('memory_limit','1500M');
        $criteria = new CDbCriteria();
        $criteria->compare('type', GasUphold::TYPE_DINH_KY);
        $criteria->compare('status_uphold_schedule', GasUphold::STATUS_UPHOLD_OFF);
        $models = GasUphold::model()->findAll($criteria);
        $count = 0; $run = 0; 
        foreach($models as $mUphold){
            $mUser = Users::model()->findByPk($mUphold->customer_id);
            $run++;
            if($mUser && !empty($mUser->last_purchase) && $mUser->channel_id == Users::CON_LAY_HANG){
                $count++;
                $mUphold->status_uphold_schedule = GasUphold::STATUS_UPHOLD_RUNNING;
                $mUphold->update(['status_uphold_schedule']);
            }
        }

        $to = time();
        $second = $to-$from;
        $info = "Cron reEnableUphold RUN $run - reEnable $count record  done in: $second  Second  <=> ".($second/60)." Minutes";
        Logger::WriteLog($info);
    }
    
    /**
     * @Author: DungNT Jun 08, 2016
     * @Todo: set province_id district_id của all bảo trì
     * @Param: $aModelCustomer array model User
     */
    public static function fixUpdateProvinceIdDistrictId() {
        set_time_limit(72000);
        $from = time();
        $criteria=new CDbCriteria;
//        $criteria->addCondition("t.province_id  IS NULL");
        $criteria->addCondition("t.type=".GasUphold::TYPE_DINH_KY);
        $models = self::model()->findAll($criteria);
        foreach($models as $item){
            $aUpdate = array('province_id', 'district_id', 'agent_id');
            $item->update($aUpdate);
        }
        
        $to = time();
        $second = $to-$from;
        echo count($models).' done in: '.($second).'  Second  <=> '.($second/60).' Minutes';die;
    }
    
    /**
     * @Author: DungNT Jun 04, 2018
     * @Todo: fix TPHCM phân lại khu vực bảo trì của NV
     */
    public static function fixEmployeeToDistrict() {
//        die;
        $employee_id = 185; //  Tân Phú, Tân Bình, Gò Vấp, Q12, Hóc Môn, Tây Ninh 
//        $employee_id = 885640; // Lương Văn Quang
//        $employee_id = 1329925; // Trần Ngọc Thương
        $aDistrict = [
            // Đinh Trọng Nghĩa
//            1,// Quận 1
//            3,// Quận 3
//            13,// Quận Tân Bình
//            18,// Quận Tân Phú
//            16,// Quận Phú Nhuận
//            19,// Quận Bình Tân
            
            // Lương Văn Quang
//            2,// Quận 2
//            9,// Quận 9
//            17,// Quận Thủ Đức
//            15,// Quận Bình Thạnh
//            
//            // Trần Ngọc Thương
//            14, // Quận Gò Vấp
//            12, // Quận 12
//            22, // Huyện Hóc Môn
//            23, // Huyện Củ Chi
            // for TAY NINH
            
        ];
        $from = time();
        $criteria=new CDbCriteria;
        $criteria->compare( 'type', GasUphold::TYPE_DINH_KY);
//        $criteria->compare( 'type_customer', STORE_CARD_KH_BINH_BO);
//        $criteria->addInCondition('district_id', $aDistrict);
        $criteria->addInCondition('province_id', [7, 8]);
        $aUpdate = array('employee_id' => $employee_id);
        $count = GasUphold::model()->count($criteria);
        GasUphold::model()->updateAll($aUpdate, $criteria);

        $to = time();
        $second = $to-$from;
        echo $count.' fixEmployeeToDistrict done in: '.($second).'  Second  <=> '.($second/60).' Minutes';die;
    }
    
    /**
     * @Author: DungNT Feb 02, 2018
     * @Todo: fix TPHCM phân lại khu vực bảo trì của NV
     */
    public static function fixEmployeeToAgent() {
        $employee_id = 866210;
        $aAgent = [
            1317767,// Q1,Q3, Phú Nhuận, Bình Thạnh 
            1214444,
            1317794,
        ];
        $from = time();
        $criteria=new CDbCriteria;
        $criteria->compare( 'type', GasUphold::TYPE_DINH_KY);
//        $criteria->compare( 'type_customer', STORE_CARD_KH_BINH_BO);
//        $criteria->compare( 'type_customer', STORE_CARD_KH_MOI);
        $criteria->addInCondition('agent_id', $aAgent);
//        $criteria->addInCondition('province_id', [10]);
//        $aUpdate = array('employee_id' => $employee_id, 'manage_id' => GasLeave::UID_DIRECTOR_BUSSINESS );
        $aUpdate = array('employee_id' => $employee_id);
        $count = GasUphold::model()->count($criteria);
        GasUphold::model()->updateAll($aUpdate, $criteria);

        $to = time();
        $second = $to-$from;
        echo $count.' fixEmployeeToDistrict done in: '.($second).'  Second  <=> '.($second/60).' Minutes';die;
    }
    
    /**
     * @Author: DungNT Mar 07, 2018
     * @Todo: fix phân lại khu vực bảo trì của NV theo tỉnh
     */
    public static function fixEmployeeToProvince() {
        $employee_id = 124;
        $aProvince = [
            10,//Tỉnh: Tây Ninh
        ];
        $from = time();
        $criteria=new CDbCriteria;
        $criteria->compare( 'type', GasUphold::TYPE_DINH_KY);
        $criteria->compare( 'type_customer', STORE_CARD_KH_BINH_BO);
//        $criteria->compare( 'type_customer', STORE_CARD_KH_MOI);
        $criteria->addInCondition('province_id', $aProvince);
//        $criteria->addInCondition('province_id', [10]);
        $aUpdate = array('employee_id' => $employee_id);
        $count = GasUphold::model()->count($criteria);
        GasUphold::model()->updateAll($aUpdate, $criteria);

        $to = time();
        $second = $to-$from;
        echo $count.' fixEmployeeToDistrict done in: '.($second).'  Second  <=> '.($second/60).' Minutes';die;
    }
    
    /**
     * @Author: DungNT Jun 09, 2016
     * kiểm tra user có dc reply của BT định kỳ không
     * nếu là BT định kỳ 1,2 lần/tháng thì chỉ dc reply 1 lần
     */
    public function canReplyDinhKyMonth() {
        if($this->type == GasUphold::TYPE_DINH_KY && $this->schedule_type != GasUphold::SCHEDULE_CO_DINH){
            $mLastReply = isset($this->rUpholdReply[0]) ? $this->rUpholdReply[0] : 0;
            if($mLastReply){// nếu đã có 1 record reply hoàn thành trong tháng hiện tại rồi thì ko cho reply nữa
                //Dec2217 cho phép tạo record khác hoàn thành - record đến KH để sinh mã hoàn thành
                $tmp = explode('-', $mLastReply->created_date_only);
                if(date('m') == $tmp[1] && date('Y') == $tmp[0] && $mLastReply->status == GasUphold::STATUS_COMPLETE){
                    return false;
                }
            }
        }
        return true;
    }
    /** @Author: DungNT Mar 11, 2018
     *  @Todo: kiểm tra reply hoàn thành định kỳ hợp lệ
     *  nếu có sự cố đc tạo trong 48h trc thì không cho trả lời định kỳ
     **/
    public function validateReplyDinhKy(&$mUpholdReply) {
//        if($this->type != GasUphold::TYPE_DINH_KY || ($this->type == GasUphold::TYPE_DINH_KY && $this->schedule_type == GasUphold::SCHEDULE_CO_DINH)){
        if($this->type != GasUphold::TYPE_DINH_KY){// chỉ cần là định kỳ thì sẽ check các Condition bên dưới
            return ;
        }
        
        // không cho reply cách nhau dưới 15 ngày
        if(!$this->canReplyDinhKyCheckTime()){// add May1018
            $mUpholdReply->addError('status',"Bạn không được hoàn thành. Các lần bảo trì phải cách nhau {$this->daysBetweenTwoReply} ngày. Lần bảo trì trước là: {$this->autocomplete_name}");
        }
        
        // find record sự cố của KH trong 48h trc
//        $model = $this->getRecordSuCoPrev();// Oct0118 không check sự cố nữa, cho hoàn thành định kỳ khi đi sự cố
//        if(empty($model)){
//            return ;
//        }
//        $mUpholdReply->addError('status','Bạn không được trả lời định kỳ. Trong '.GasUphold::TIME_CHECK_DINHKY.' giờ gần đây khách hàng này có bảo trì sự cố ngày: '. MyFormat::dateConverYmdToDmy($model->created_date,'d/m/Y H:i'));
    }
    
    /** @Author: DungNT Mar 11, 2018
     *  @Todo: find record sự cố của KH trong 48h trc
     **/
    public function getRecordSuCoPrev() {
        $hours = GasUphold::TIME_CHECK_DINHKY;
        $criteria = new CDbCriteria();
        $criteria->addCondition('t.customer_id=' . $this->customer_id);
        $criteria->addCondition('t.type=' . GasUphold::TYPE_SU_CO);
        $criteria->addCondition("DATE_ADD(created_date,INTERVAL $hours HOUR) > NOW()");
        return self::model()->find($criteria);
    }
    
    
    /**
     * @Author: DungNT Jul 23, 2016
     * @todo: get latest 5 record of this customer
     */
    public function getHistoryCustomer() {
        $criteria = new CDbCriteria();
        $criteria->compare("t.customer_id", $this->customer_id);
        $criteria->compare("t.type", GasUphold::TYPE_SU_CO);
        if(!empty($this->id)){
//            $criteria->addNotInCondition("t.id", $this->id);
            $criteria->addCondition("t.id IN ($this->id)");
        }
        $criteria->order = "t.id DESC";
        $criteria->limit = 5;
        return self::model()->findAll($criteria);
    }
    
    
    /**
     * @Author: DungNT Jul 23, 2016
     * @todo: get latest 5 record of this customer
     */
    public function getHistoryCustomerByMonth() {
        $aRes = array();
        $criteria = new CDbCriteria();
        $criteria->compare("t.customer_id", $this->customer_id);
        $criteria->addBetweenCondition("t.created_date_only", $this->date_from, $this->date_to);
        $criteria->select = "year(t.created_date_only) as report_year, month(t.created_date_only) as report_month, t.type,"
                       . " count(t.id) as id ";
        $criteria->group = "t.type, year(t.created_date_only), month(t.created_date_only)";

        $mRes = self::model()->findAll($criteria);
        foreach($mRes as $item){
            $aRes['YEAR_MONTH'][$item->report_year][$item->report_month] = $item->report_month; // dùng để FOREACH
            $aRes['OUTPUT'][$item->type][$item->report_year][$item->report_month] = $item->id;
        }
        if(isset($aRes['YEAR_MONTH'])){
            // tăng dần asort low to high || Giam dần  arsort() high to low 
            foreach( $aRes['YEAR_MONTH'] as $year => $aMonth){
                asort($aRes['YEAR_MONTH'][$year]);
            }
        }
        return $aRes;
    }
    
    /**
     * @Author: DungNT Dec 19, 2016
     * @Todo: report theo bảo trì định kỳ hàng ngày của nhân viên
     */
    public function reportScheduleDaily() {
        $date_from  = MyFormat::dateDmyToYmdForAllIndexSearch($this->date_from);
        $date_to    = MyFormat::dateDmyToYmdForAllIndexSearch($this->date_to);

        // 1. thống kê bảo trì
        $criteria = new CDbCriteria();
        $criteria->addCondition('t.type='. $this->type);
        
        $this->reportScheduleDailyGetCriteria($criteria, $date_from, $date_to);
        if($this->type == self::TYPE_DINH_KY){
            $criteria->addCondition('t.status='. GasUphold::STATUS_COMPLETE);// Add Feb0918
            $models = GasUpholdReply::model()->findAll($criteria);
        }else{
            $models = GasUphold::model()->findAll($criteria);
        }
        $aRes = array();
        $aRes['OUTPUT_SUM'] = array();
        foreach($models as $item){
            $aRes['OUTPUT'][$item->employee_id][$item->created_date_only] = $item->id;
            if(isset($aRes['OUTPUT_SUM'][$item->employee_id])){
                $aRes['OUTPUT_SUM'][$item->employee_id] += $item->id;
            }else{
                $aRes['OUTPUT_SUM'][$item->employee_id] = $item->id;
            }
        }
        $aRes['MODEL_SALE_BT']  = Users::getArrObjectUserByRoleHaveOrder(GasUphold::$ROLE_EMPLOYEE_BAOTRI);
        $aRes['DATE_ARRAY']     = MyFormat::getArrayDay($date_from, $date_to);
        arsort($aRes['OUTPUT_SUM']);
        return $aRes;
    }
    
    public function reportScheduleDailyGetCriteria(&$criteria, $date_from, $date_to) {
        $criteria->addBetweenCondition("t.created_date_only", $date_from, $date_to);
        $criteria->select = "count(t.id) as id, t.employee_id, t.created_date_only";
        $criteria->group = "t.employee_id, t.created_date_only";
    }
    
    public function reportScheduleDailyBackUpForJoinReport() {
        $date_from  = MyFormat::dateDmyToYmdForAllIndexSearch($this->date_from);
        $date_to    = MyFormat::dateDmyToYmdForAllIndexSearch($this->date_to);

        // 1. thống kê bảo trì
        $criteriaDinKy = new CDbCriteria();
        $criteriaDinKy->addCondition('t.type='. self::TYPE_DINH_KY);
        $this->reportScheduleDailyGetCriteria($criteriaDinKy, $date_from, $date_to);
        $modelsDinKy = GasUpholdReply::model()->findAll($criteriaDinKy);

        $criteriaSuCo = new CDbCriteria();
        $criteriaSuCo->addCondition('t.type='. self::TYPE_SU_CO);
        $this->reportScheduleDailyGetCriteria($criteriaSuCo, $date_from, $date_to);
        $modelsSuCo = GasUphold::model()->findAll($criteriaSuCo);
            
        $aRes = array();
        $aRes['OUTPUT_SUM'] = array();
        foreach($modelsDinKy as $item){
            $aRes['OUTPUT'][$item->employee_id][$item->created_date_only] = $item->id;
            if(isset($aRes['OUTPUT_SUM'][$item->employee_id])){
                $aRes['OUTPUT_SUM'][$item->employee_id] += $item->id;
            }else{
                $aRes['OUTPUT_SUM'][$item->employee_id] = $item->id;
            }
        }
        
        $aRes['MODEL_SALE_BT']  = Users::getArrObjectUserByRoleHaveOrder(GasUphold::$ROLE_EMPLOYEE_BAOTRI);
        $aRes['DATE_ARRAY']     = MyFormat::getArrayDay($date_from, $date_to);
        arsort($aRes['OUTPUT_SUM']);
        return $aRes;
    }
    
    
    /**
     * @Author: DungNT May 05, 2017
     * @Todo: gửi lại 1 notify đến điều phối sau khi KH tạo bảo trì mới
     * Sep2518 fix lại gửi notify cho tổng đài nếu KH chưa setup NV bảo trì
     */
    public function makeSocketNotify() {
        if(!empty($this->employee_id)){
            return ;
        }
        $mCallTemp      = new CallTemp();
        $aUserId        = $mCallTemp->getArrayUserIdOfExt(Call::QUEUE_BO_MOI);
        
        $message    = "[Bảo trì bò mối] ".$this->getCustomer().' tạo mới bảo trì bò mối trên APP KH';
        $json       = $needMore = [];
        $aModelSocket = []; // xử lý gửi 1 lần 1 list sự kiện
//        $aUserId[] = 281994;// 204 call center
        foreach($aUserId as $toUserId){
            $aModelSocket[] = GasSocketNotify::addMessage(0, GasConst::UID_ADMIN, $toUserId, GasSocketNotify::CODE_ALERT_CALL_CENTER, $message, $json, $needMore);
        }
        $mSync = new SyncData(SyncData::SERVER_IO_SOCKET, 'socket/notifyAdd');
        $mSync->notifyAdd($aModelSocket);
        
    }
    
    /** @Author: DungNT Oct 13, 2017
     *  @Todo: cập nhật Sale vào các BT định Kỳ
     */
    public static function updateSaleSchedule() {
//        return;
        $from = time();
        $criteria = new CDbCriteria();
        $criteria->addCondition('t.type='. self::TYPE_DINH_KY);
        $criteria->compare('status_uphold_schedule', GasUphold::STATUS_UPHOLD_RUNNING);
        $models = GasUphold::model()->findAll($criteria);
        foreach($models as $item){
            $mCustomer = $item->rCustomer;
            if($mCustomer){
                $item->sale_id          = $mCustomer->sale_id;
                $item->type_customer    = $mCustomer->is_maintain;
                $item->update(['sale_id', 'type_customer']);
            }
        }
        $to = time();
        $second = $to-$from;
        echo count($models).' done in: '.($second).'  Second  <=> '.($second/60).' Minutes';die;  	
    }
    
    /**
     * @Author: DungNT Dec 31, 2015 -> move function Dec1217
     * map attribute to model form $q. get post and validate
     * @param: array params $q
     */
    public function handlePost($q) {
        $this->customer_id      = $q->customer_id;
        $this->employee_id      = $q->employee_id;
        $this->type_uphold      = $q->type_uphold;
        $this->content          = $q->content;
        $this->contact_person   = $q->contact_person;
        $this->contact_tel      = $q->contact_tel;
        $this->uid_login        = $this->mAppUserLogin->id;
        if($this->mAppUserLogin->role_id != ROLE_CUSTOMER){ // nếu là customer tạo thì sẽ không set cột này
            $this->last_update_by = $this->mAppUserLogin->id;
        }
        $this->handlePostChainStore($q);
        if($this->mAppUserLogin->role_id == ROLE_CUSTOMER){// Apr 13, 2016 nếu là customer tạo thì sẽ get tự động employee_id bên BT định kỳ sang
            $this->employee_id = GasUphold::getEmployeeOfCustomer($this->customer_id);
        }
        $this->type = GasUphold::TYPE_SU_CO;
        $this->validate();
    }
    
    /** @Author: DungNT Dec1217
     * @Todo: handle order chain store, đặt cho chuỗi cửa hàng
     */
    public function handlePostChainStore($q) {
        $is_chain_store = isset($q->is_chain_store) ? $q->is_chain_store : 0;
        if(empty($is_chain_store) || !isset($q->customer_chain_store_id)){
            return ;
        }
        if(empty($q->customer_chain_store_id)){
            throw new Exception('Bạn chưa chọn nơi giao');
        }
        /* 1. xử lý set lại customer id là id người đặt put lên từ client
         * 2. xử lý set parent_chain_store
         */
        $this->customer_id          = $q->customer_chain_store_id;
        $this->parent_chain_store   = $this->uid_login;
        
        $mCustomer = $this->rCustomer;
        if($mCustomer){
            if($mCustomer->is_maintain == UsersExtend::STORE_CARD_HEAD_QUATER){
                GasScheduleEmail::alertLockBookApp($mCustomer, $this);
                Logger::WriteLog('Chặn bảo trì: '.$mCustomer->first_name);
                throw new Exception('Không thể tạo bảo trì cho tài khoản trụ sở chính, vui lòng liên hệ nhân viên kinh doanh để được hỗ trợ');
            }
        }
    }
    
    /** @Author: DungNT May 04, 2018
     *  @Todo: kiểm tra NV bảo trì có hoạt động không, nếu không thì báo lỗi, ko cho tạo
     **/
    public function checkUserInactive() {
        if(empty($this->employee_id)){
            return ;
        }
        $mUser = Users::model()->findByPk($this->employee_id);
        if($mUser && $mUser->status == STATUS_INACTIVE){
            throw new Exception('Nhân viên bảo trì đã nghỉ việc, vui lòng chọn nhân viên khác. Không thể tạo bảo trì');
        }
    }
    
    
    /** @Author: DungNT Dec 19, 2017
     *  @Todo: 
     * 4/ Làm mã bảo trì gửi sms cho KH và NV bảo trì nhập mã vào mới cho hoàn thành
        4.1 thêm cột mã trên web, 4 số
        4.2 khi tạo BT sự cố thì sinh mã này ra khi tạo trên web, chỉ load trên app KH
     *  4.3 Khi hoàn thành sự cố và định kỳ phải nhập mã này vào mới cho hoàn thành
        4.4 BT Sự cố: Mã này sẽ được SMS cho KH khi NV bấm xử lý
     *  4.5 BT định kỳ: khi đến KH thì bấm trạng thái đến KH thì sẽ sinh mã BT ra, sau đó hoàn thành thì nhập vào
     *  4.6 validate số đt liên hệ khi nhập, phải là số đt hợp lệ
     **/
    public function genCodeComplete() {
        $this->code_complete = ActiveRecord::randString(6, '123456789');
    }
    public function updateCodeComplete() {
        $this->genCodeComplete();
        $this->update(['code_complete']);
    }
    public function getAppCodeComplete() {
        if(empty($this->mAppUserLogin) || empty($this->code_complete)){
            return '';
        }
        if($this->mAppUserLogin->role_id == ROLE_CUSTOMER){
            return "[$this->code_complete] ";
        }
        return '';
    }
    
    /** @Author: DungNT May 18, 2018
     *  @Todo: định dạng ngày tạo bảo trì sự cố
     * Và ngày Plan của định kỳ xuống app giao nhận
     **/
    public function getAppDateShow() {
        if($this->type == GasUphold::TYPE_SU_CO){
            return MyFormat::dateConverYmdToDmy($this->created_date, 'd/m/Y H:i');
        }
        $mPlan = new GasUpholdPlan();
        $mPlan->customer_id = $this->customer_id;
        $mPlan = $mPlan->getPlanLastestOfCustomer();
        if(!empty($mPlan)){
            return MyFormat::dateConverYmdToDmy($mPlan->end_date, 'd/m/Y').' 00:00';
        }
        return MyFormat::dateConverYmdToDmy($this->created_date, 'd/m/Y H:i');
    }
    
    public function formatDataBeforeUpdate() {
        $this->next_uphold = MyFormat::dateConverYmdToDmy($this->next_uphold);
    }
    public function setDbDate() {
        if(strpos($this->next_uphold, '/')){
            $this->next_uphold = MyFormat::dateConverDmyToYmd($this->next_uphold);
        }
    }
 
    /** @Author: DungNT Feb 07, 2018
     *  @Todo: get record detail for app view
     **/
    public function getListReply() {
        if($this->type == GasUphold::TYPE_SU_CO){
            return $this->rUpholdReply;
        }
        $criteria = new CDbCriteria();
        $criteria->addCondition('t.uphold_id=' . $this->id);
        $criteria->order = 't.id DESC';
        $criteria->limit = 10;
        return GasUpholdReply::model()->findAll($criteria);
    }
    
    /** @Author: DungNT May 10, 2018
     *  @Todo: kiểm tra thời gian trả lời định kỳ, không cho trả lời cách lần trả lời trước 15 ngày 
     * khoảng thời gian giữa 2 lần cách nhau trên 15 ngày thì cho trả lời
     **/
    public function canReplyDinhKyCheckTime() {
        $ok = true;
        $mReplyPrev = new GasUpholdReply();
        $mReplyPrev->customer_id = $this->customer_id;
        $mReplyPrev = $mReplyPrev->getLastestReplyDinhKy();
        if(empty($mReplyPrev)){
            return $ok;
        }
        // sử dụng tạm biến autocomplete_name để lấy biến date ra ngoài
        $this->autocomplete_name = MyFormat::dateConverYmdToDmy($mReplyPrev->created_date_only);
        $dateAllow = MyFormat::modifyDays($mReplyPrev->created_date_only, $this->daysBetweenTwoReply);
        return MyFormat::compareTwoDate(date('Y-m-d'), $dateAllow);
    }
    
    
    /** @Author: HOANG NAM 10/04/2018
     *  @Todo: get next day
     *  @Code: NAM009
     *  @Param:  $date: time; $count_month: số tháng cộng thêm; $aDay danh sách ngày tiếp theo
     *  @return: Y-m-d
     **/
    public function getNextDate($date, $count_month = 1,$aDay = array()) {
        $date_next = null;
        if(!empty($aDay)){
            $dayCurrent = MyFormat::dateConverYmdToDmy($date, 'd');
            $nextMonth = MyFormat::modifyDays(MyFormat::dateConverYmdToDmy($date, 'Y-m-01'),$count_month,'+','month');
            if(max($aDay) <= $dayCurrent){
                $day = min($aDay);
                $month = MyFormat::dateConverYmdToDmy($nextMonth, 'm');
                $year = MyFormat::dateConverYmdToDmy($nextMonth, 'Y');
            }else {
                foreach ($aDay as $intDay) {
                    if($intDay > $dayCurrent){
                        $day = $intDay;
                        break;
                    }
                }
                $month = MyFormat::dateConverYmdToDmy($date, 'm');
                $year = MyFormat::dateConverYmdToDmy($date, 'Y');
            }
            if(!checkdate($month, $day, $year)){
                    $date_next = date("Y-m-t", strtotime($nextMonth));
            }else {
                $date_next = MyFormat::dateConverDmyToYmd($day. '/' . $month . '/' . $year);
            }
        }else{
            $date_next = MyFormat::modifyDays(MyFormat::dateConverYmdToDmy($date, 'Y-m-d'),$count_month,'+','month');
        }
        $this->getNextDateCheckValid($date_next);
        return $date_next;
    }
    
    /** @Author: DungNT May 16, 2018
     *  @Todo: kiểm tra next_uphold có hợp lệ không, nếu nhỏ hơn ngày hiện tại 
     * thì cộng thêm ngày tính từ ngày hiện tại
     **/
    public function getNextDateCheckValid(&$date_next) {
        return ;// May1818 không làm tự động, anh Quý tự set = tay
        if(empty($date_next)){
            return ;
        }
        $dateAdd = 5;
        $today = date('Y-m-d');
        if(MyFormat::compareTwoDate($today, $date_next)){
            $newDatePlan = MyFormat::modifyDays($today, 5);
//            $info = "<p>Old $date_next New $newDatePlan<p>";
//            echo $info;
        }
    }
    
    /** @Author: HOANG NAM 09/04/2018
     *  @Todo: update one field date next_uphold
     *  @Code: NAM009
     *  @Param: 
     *  GasUphold::SCHEDULE_CO_DINH             => 'Ngày cố định'
     *  GasUphold::SCHEDULE_KHONG_CO_DINH       => '1 tháng 1 lần'
     *  GasUphold::SCHEDULE_KHONG_CO_DINH_2_THANG => '2 tháng 1 lần'
     *  GasUphold::SCHEDULE_KHONG_CO_DINH_3_THANG => '3 tháng 1 lần'
     **/
    public function updateNextUphold($aGasUphold = array(),$affterCreated = 15) {
        if(empty($aGasUphold)) {
            return;
        }
        foreach ($aGasUphold as $key => $mGasUphold) {
            if($mGasUphold->status_uphold_schedule == GasUphold::STATUS_UPHOLD_RUNNING) {
                $mGasUphold->updateNextUpholdOneRecord();
            }
        }
    }
    
    /** @Author: DungNT May 19, 2018
     *  @Todo: 
     **/
    public function getDaysAddUphold() {
        return 15;
    }
    
    /** @Author: DungNT May 18, 2018
     *  @Todo: update next uphold khi có reply định kỳ hoàn thành
     **/
    public function updateNextUpholdOneRecord() {
        $today = date('Y-m-d');
//        $today = $this->created_date;// for fix all record
        $mGasUpholdReply = $this->rUpholdReplyForUpdateNextUphold;
        switch ($this->schedule_type) {
            case GasUphold::SCHEDULE_CO_DINH:
                $day_checkbox = GasOneManyBig::getArrOfManyId($this->id, GasOneManyBig::TYPE_UPHOLD_SCHEDULE_DAY); 
                if(!empty($mGasUpholdReply)) {
                    $this->next_uphold = $this->getNextDate($mGasUpholdReply->created_date,1,$day_checkbox);
                }else {
                    $this->next_uphold = $this->getNextDate(MyFormat::modifyDays($today, $this->getDaysAddUphold(),'+','day'),1,$day_checkbox);
                }
                break;

            case GasUphold::SCHEDULE_KHONG_CO_DINH:
                if(!empty($mGasUpholdReply)) {
                    $this->next_uphold = $this->getNextDate($mGasUpholdReply->created_date, 1);
                }else {
                    $this->next_uphold = $this->getNextDate($today, 1);
                }
                break;

            case GasUphold::SCHEDULE_KHONG_CO_DINH_2_THANG:
                if(!empty($mGasUpholdReply)) {
                    $this->next_uphold = $this->getNextDate($mGasUpholdReply->created_date, 2);
                }else {
                    $this->next_uphold = $this->getNextDate($today, 2);
                }
                break;

            case GasUphold::SCHEDULE_KHONG_CO_DINH_3_THANG:
                if(!empty($mGasUpholdReply)) {
                    $this->next_uphold = $this->getNextDate($mGasUpholdReply->created_date, 3);
                }else {
                    $this->next_uphold = $this->getNextDate($today, 3);
                }
                break;

            default:
                break;
        }
        $this->update(['next_uphold']);
    }
    
    /** @Author: DungNT May 25, 2018
     *  @Todo: xóa các lịch BT chưa hoàn thành khi bấm ngưng bảo trì trên web
     **/
    public function removePlanNotDone() {
        if($this->status_uphold_schedule != GasUphold::STATUS_UPHOLD_OFF){
            return ;
        }
        
        $model = new GasUpholdPlan();
        $model->customer_id = $this->customer_id;
        $models = $model->getPlanNotComplete();
        foreach($models as $item){
            $item->delete();
        }
    }
    
    /** @Author: DungNT Jun 28, 2018
     *  @Todo: fix những record bị miss khi scan,
     * đã chuyển sang trạng thái không lấy hàng, nhưng bảo trì chưa chuyển qua trạng thái Ngưng BT
     **/
    public function fixAllCustomerKhongLayHang() {
        ini_set('memory_limit','1500M');
        $criteria=new CDbCriteria;
        $criteria->compare('channel_id', Users::KHONG_LAY_HANG);
        $aTypeBoMoi = array(STORE_CARD_KH_BINH_BO, STORE_CARD_KH_MOI);
        $criteria->addInCondition( 'is_maintain', $aTypeBoMoi);
        $aModelCustomer = Users::model()->findAll($criteria);
        GasUphold::setStatusUpholdSchedule($aModelCustomer);
        Logger::WriteLog(count($aModelCustomer)." record customer KHONG_LAY_HANG check DINH KY SET OFF ********* run fixAllCustomerKhongLayHang -> setStatusUpholdSchedule ");
        $mUphold = new GasUphold();
        $mUphold->fixAllCustomerEmptyLastPurchase();
    }
    
    
    /** @Author: DungNT Jun 30, 2018
     *  @Todo: fix KH đã tạo quá 60 ngày chưa lấy hàng lần nào thì đưa vào không bảo trì nữa
     **/
    public function fixAllCustomerEmptyLastPurchase() {
        $criteria=new CDbCriteria;
        $criteria->addCondition("DATE_ADD(created_date, INTERVAL 60 DAY) < CURDATE() AND last_purchase IS NULL");
        $criteria->compare('channel_id', Users::CON_LAY_HANG);
        $aTypeBoMoi = array(STORE_CARD_KH_BINH_BO, STORE_CARD_KH_MOI);
        $criteria->addInCondition( 'is_maintain', $aTypeBoMoi);
        $total = 0;
        $models = Users::model()->findAll($criteria);
        foreach($models as $mUser){
//            echo $mUser->code_bussiness." -- $mUser->created_date<br>";
            $mUpholdSchedule = new GasUphold();
            $mUpholdSchedule->customer_id = $mUser->id;
            $mUpholdSchedule = $mUpholdSchedule->getCustomerSchedule();
            if(!empty($mUpholdSchedule) && $mUpholdSchedule->status_uphold_schedule == GasUphold::STATUS_UPHOLD_RUNNING){
                $total++;
                $mUpholdSchedule->status_uphold_schedule = GasUphold::STATUS_UPHOLD_OFF;
                $mUpholdSchedule->update(['status_uphold_schedule']);
                $mUpholdSchedule->removePlanNotDone();
            }
        }
        Logger::WriteLog("$total record  LAY_HANG check DINH KY SET OFF ********* run fixAllCustomerEmptyLastPurchase ");

    }
    /** @Author: Pham Thanh Nghia 2018
     **/
    public function canExportExcel(){
        $cUid = MyFormat::getCurrentUid();
        $aAllow = [GasConst::UID_ADMIN, GasConst::UID_CHAU_LNM];
        return in_array($cUid, $aAllow);
    }
    
    /** @Author: DungNT May 30, 2019
     *  @Todo: find by code_no
     **/
    public function findByCodeNo($code_no) {
        $criteria = new CDbCriteria();
        $criteria->compare('t.code_no', $code_no);
        return GasUphold::model()->find($criteria);
    }
    
}