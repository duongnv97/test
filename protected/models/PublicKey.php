<?php
/**
 * This is the model class for table "{{_public_key}}".
 *
 * The followings are the available columns in table '{{_public_key}}':
 * @property integer $id
 * @property string $public_key
 */
class PublicKey extends BaseSpj
{
    // RFC4648 Base32 alphabet
    private $alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ234567";
        
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }
    
    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{_public_key}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        return array(
            array('id,public_key', 'safe'),
        );
    }
    
    public function relations()
    {
        return array(
        );
    }
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'public_key' => 'public key',
        );
    }
    
    public function search()
    {
        $criteria=new CDbCriteria;
        $criteria->compare('t.id', $this->id);
        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
            'pagination'=>array(
                'pageSize'=> Yii::app()->user->getState('pageSize',Yii::app()->params['defaultPageSize']),
            ),
        ));
    }
    
    /** @Author: NamNH Nov 24,2018
     *  @Todo: get otp current time
     *  @link: https://github.com/dimamedia/PHP-Simple-TOTP-and-PubKey/blob/master/class.tfa.php
     **/
    public function getOtp($key) {
        /* Base32 decoder */
        // Remove spaces from the given public key and converting to an array
        $key = str_split(str_replace(" ","",$key));

        $n = 0;
        $j = 0;
        $binary_key = "";
        // Decode public key's each character to base32 and save into binary chunks
        foreach($key as $char) {
                $n = $n << 5;
                $n = $n + stripos($this->alphabet, $char);
                $j += 5;

                if($j >= 8) {
                        $j -= 8;
                        $binary_key .= chr(($n & (0xFF << $j)) >> $j);
                }
        }
        /* End of Base32 decoder */
        // current unix time 30sec period as binary
        $binary_timestamp = pack('N*', 0) . pack('N*', floor(microtime(true)/30));
        // generate keyed hash
        $hash = hash_hmac('sha1', $binary_timestamp, $binary_key, true);

        // generate otp from hash
        $offset = ord($hash[19]) & 0xf;
        $otp = (
                ((ord($hash[$offset+0]) & 0x7f) << 24 ) |
                ((ord($hash[$offset+1]) & 0xff) << 16 ) |
                ((ord($hash[$offset+2]) & 0xff) << 8 ) |
                (ord($hash[$offset+3]) & 0xff)
        ) % pow(10, 6);
       return $otp;
    }
    
    /** @Author: NamNH Nov 24, 2018
     *  @Todo: get public key
     **/
    public function getPubKey() {
        $alphabet = str_split($this->alphabet);
        $key = '';
        // generate 16 chars public key from Base32 alphabet
        for ($i = 0; $i < 16; $i++) $key .= $alphabet[mt_rand(0,31)];
        // split into 4x4 chunks for easy reading
        return implode(" ", str_split($key, 4));
    }
    
    /** @Author: NamNH Nov 24, 2018
     *  @Todo: get public key from id
     **/
    public function apiPublicKey($id){
        $result     = null;
        $mPublicKey = PublicKey::model()->findByPk($id);
        if(!empty($mPublicKey)){
            $result['public_key']   = $mPublicKey->public_key;
//            open => gửi mã otp hiện tại
//            $result['OTP']          = $this->getOtp($mPublicKey->public_key);
        }
        return $result;
    }
    
    /** @Author: NamNH Nov 24, 2018
     *  @Todo: check public key
     **/
    public function apiCheckOtp($otp){
//        Nhân viên login
        $mUser      = $this->mAppUserLogin;
        $result     = false;
//        Xử lý lấy public key cho từng nhân viên tại đây.
        $mPublicKey = PublicKey::model()->find();
        if(!empty($mPublicKey)){
            if($otp == $this->getOtp($mPublicKey->public_key)){
                return true;
            }
        }
        return $result;
    }
    
}
