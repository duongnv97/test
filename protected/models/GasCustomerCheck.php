<?php

/**
 * This is the model class for table "{{_gas_customer_check}}".
 *
 * The followings are the available columns in table '{{_gas_customer_check}}':
 * @property string $id
 * @property string $code_no
 * @property string $uid_login
 * @property integer $month
 * @property integer $year
 * @property string $monitor_agent_id
 * @property string $customer_name
 * @property string $customer_address
 * @property string $sale
 * @property string $qty
 * @property string $note_head_bo
 * @property string $vo_inventory_50
 * @property string $vo_inventory_45
 * @property string $vo_inventory_12
 * @property string $vo_da_thu_50
 * @property string $vo_da_thu_45
 * @property string $vo_da_thu_12
 * @property string $vo_de_xuat_thu_50
 * @property string $vo_de_xuat_thu_45
 * @property string $vo_de_xuat_thu_12
 * @property string $file_report
 * @property string $report_handle
 * @property string $note_monitor_agent
 * @property string $created_date
 */
class GasCustomerCheck extends BaseSpj
{
    public $MAX_ID;
    public $file_excel;
    public static $AllowExcel = "xls,xlsx";
    public static $AllowFile = 'jpg,jpeg,png';
    public static $aSize = array(
        'size1' => array('width' => 1024, 'height' => 768),
        'size2' => array('width' => 100, 'height' => 80),// thump view
    );
    public static $pathUpload = 'upload/customer_check';
    
    const RATE_TOT = 1;
    const RATE_KHONG_TOT = 2;
    public static $ARR_RATE = array(
        GasCustomerCheck::RATE_TOT => 'Tốt',
        GasCustomerCheck::RATE_KHONG_TOT => 'Không Tốt',
    );
    public $autocomplete_name;
    public $autocomplete_name_uid_login;
    public $autocomplete_name_sale;
    public $date_from;
    public $date_to;
    public $OldFile;
    
    const STATUS_NEW = 0;
    const STATUS_CHECKED = 1;
    
    public static $ARR_STATUS = array(
        GasCustomerCheck::STATUS_NEW => 'Mới',
        GasCustomerCheck::STATUS_CHECKED => 'KT Kiểm Tra',
    );
    
    public static $ARR_ROLE_LIMIT_VIEW = array(
        ROLE_MONITOR_AGENT
    );
    
    // lấy số ngày cho phép đại lý cập nhật
    public static function getDayAllowUpdate(){
        return Yii::app()->params['days_update_customer_check'];
    }
    
    // Now 23, 2014. số ngày cho phép giám sát đại lý cập nhật báo cáo 
    public static function getDayAllowUpdateReport(){
        return Yii::app()->params['days_update_customer_check_report'];
    }
    
    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return GasCustomerCheck the static model class
     */
    public static function model($className=__CLASS__)
    {
            return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
            return '{{_gas_customer_check}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        return array(
            array('customer_id', 'required', 'on'=>'create, update_accounting'),
//                . 'rate_bao_tri, rate_dieu_phoi, rate_giao_nhan, rate_kinh_doanh', 'required', 'on'=>'create'),
            array('customer_id,report_date, report_handle,'
                . 'rate_bao_tri, rate_dieu_phoi, rate_giao_nhan, rate_kinh_doanh', 'required', 'on'=>'update_monitor'),
            
            array('id, code_no, uid_login, role_id, agent_id, customer_id, '
                . 'type_customer, sale_id, sale_type, report_date, debit_vo_12, '
                . 'debit_vo_45, debit_vo_50, debit_money, '
                . 'vo_inventory_50, vo_inventory_45, vo_inventory_12, vo_da_thu_50, '
                . 'vo_da_thu_45, vo_da_thu_12, vo_de_xuat_thu_50, vo_de_xuat_thu_45, vo_de_xuat_thu_12,'
                . 'rate_bao_tri, rate_dieu_phoi, rate_giao_nhan, rate_kinh_doanh, money_get,'
                . 'file_report, report_handle', 'safe'),
            array('money_qua_han, money_kho_doi, money_xau, money_goi_dau_chet, status_check', 'safe'),
            array('file_excel', 'file',
                'allowEmpty'=>true,
                'types'=> 'xls,xlsx',
                'wrongType'=>"Chỉ cho phép tải file ".self::$AllowExcel,
                //'maxSize' => ActiveRecord::getMaxFileSize(), // 5MB
                //'tooLarge' => 'The file was larger than '.(ActiveRecord::getMaxFileSize()/1024).' KB. Please upload a smaller file.',
            ),
            array('file_report', 'file','on'=>'create,update',
                'allowEmpty'=>true,
                'types'=> self::$AllowFile,
                'wrongType'=> "Chỉ cho phép định dạng file ".self::$AllowFile." .",
            ), 
            array('date_from,date_to', 'safe'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        return array(
            'rUidLogin' => array(self::BELONGS_TO, 'Users', 'uid_login'),
            'rCustomer' => array(self::BELONGS_TO, 'Users', 'customer_id'),
            'rSale' => array(self::BELONGS_TO, 'Users', 'sale_id'),
            'rAgent' => array(self::BELONGS_TO, 'Users', 'agent_id'),
            'rAgentOfCustomer' => array(self::BELONGS_TO, 'Users', 'agent_id_of_customer'),
            'rAccountingEdit' => array(self::BELONGS_TO, 'Users', 'last_accounting_edit'),
            'rMonitorgEdit' => array(self::BELONGS_TO, 'Users', 'last_monitor_edit'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
            return array(
                'id' => 'ID',
                'code_no' => 'Mã số',
                'uid_login' => 'Người tạo',
                'role_id' => 'role_id',
                'customer_id' => 'Khách Hàng',
                'report_date' => 'Ngày Báo Cáo',
                'rate_bao_tri' => 'Bảo Trì',
                'rate_dieu_phoi' => 'Điều Phối',
                'rate_giao_nhan' => 'Giao Nhận',
                'rate_kinh_doanh' => 'Kinh Doanh',
                'vo_inventory_50' => 'Tồn thực tế 50',
                'vo_inventory_45' => 'Tồn thực tế 45',
                'vo_inventory_12' => 'Tồn thực tế 12',
                'vo_da_thu_50' => 'Đã thu 50',
                'vo_da_thu_45' => 'Đã thu 45',
                'vo_da_thu_12' => 'Đã thu 12',
                'vo_de_xuat_thu_50' => 'Đề xuất thu 50',
                'vo_de_xuat_thu_45' => 'Đề xuất thu 45',
                'vo_de_xuat_thu_12' => 'Đề xuất thu 12',
                'file_report' => 'File Báo Cáo Scan',
                'report_handle' => 'Báo cáo kết quả kiểm tra',
                'created_date' => 'Ngày Tạo',
                'money_get' => 'Số Tiền Thu Được',
                'agent_id_of_customer' => 'Đại Lý',
                'date_from' => 'Từ Ngày',
                'date_to' => 'Đến Ngày',
                'sale_id' => 'Sale',
                'agent_id' => 'Tạo Bởi',
                
                'status_check' => 'Trạng thái',
                'last_accounting_edit' => 'Kế toán cập nhật',
                'last_accounting_edit_date' => 'Kế toán cập nhật ngày',
                'last_monitor_edit' => 'Giám sát cập nhật',
                'last_monitor_edit_date' => 'Giám sát cập nhật ngày',
            );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
            $criteria=new CDbCriteria;
            $criteria->compare('t.code_no',$this->code_no,true);
            $criteria->compare('t.uid_login', $this->uid_login);
            $criteria->compare('t.customer_id', $this->customer_id);
            $criteria->compare('t.sale_id', $this->sale_id);
            
            $date_from = '';
            $date_to = '';
            $cRole = Yii::app()->user->role_id;
            $cUid = Yii::app()->user->id;
            if(!empty($this->date_from)){
                $date_from = MyFormat::dateDmyToYmdForAllIndexSearch($this->date_from);
            }
            if(!empty($this->date_to)){
                $date_to = MyFormat::dateDmyToYmdForAllIndexSearch($this->date_to);
            }
            if(!empty($date_from) && empty($date_to))
                $criteria->addCondition("t.report_date>='$date_from'");
            if(empty($date_from) && !empty($date_to))
                $criteria->addCondition("t.report_date<='$date_to'");
            if(!empty($date_from) && !empty($date_to))
                $criteria->addBetweenCondition("t.report_date",$date_from,$date_to);            
            if(in_array($cRole, GasCustomerCheck::$ARR_ROLE_LIMIT_VIEW)){
                GasAgentCustomer::initSessionAgent();
                $session=Yii::app()->session;
                if(isset($session['LIST_AGENT_OF_USER']) && count($session['LIST_AGENT_OF_USER'])){
                    $strAgentId = implode(',', $session['LIST_AGENT_OF_USER']);
                    $criteria->addCondition (" ( t.agent_id IN ($strAgentId) OR t.role_id=".ROLE_ACCOUNTING.")" );
//                    $criteria->addCondition (" t.role_id=".ROLE_ACCOUNTING );die;
                }
            }
            if($cRole == ROLE_MONITOR_AGENT){
                $criteria->compare('t.status_check', GasCustomerCheck::STATUS_CHECKED);
            }elseif($cRole == ROLE_SUB_USER_AGENT){
                $criteria->compare('t.agent_id', MyFormat::getAgentId());
            }elseif($cRole == ROLE_SALE){
                $criteria->compare('t.sale_id', $cUid);
            }
            
            $criteria->order = "t.report_date DESC";
            $aRoleCheck = array(ROLE_SUB_USER_AGENT, ROLE_ACCOUNTING);
            if(in_array($cRole, $aRoleCheck)){
                $criteria->order = "t.id DESC";
            }
            $criteria->order = "t.id DESC";
            
            return new CActiveDataProvider($this, array(
                'criteria'=>$criteria,
                'pagination'=>array(
                    'pageSize'=> Yii::app()->user->getState('pageSize',Yii::app()->params['defaultPageSize']),
                ),
            ));
    }

    /*
    public function activate()
    {
        $this->status = 1;
        $this->update();
    }

    public function deactivate()
    {
        $this->status = 0;
        $this->update();
    }
	*/

    public function defaultScope()
    {
            return array(
                    //'condition'=>'',
            );
    }
    
    /**
     * @Author: ANH DUNG Now 23, 2014
     * @Todo: save row from excel file
     * @Param: $model model GasCustomerCheck
     */
    public static function SaveRowFileExcel($model){
        try {
            $FileName = $_FILES['GasCustomerCheck']['tmp_name']['file_excel'];
            if(empty($FileName)) return;
            Yii::import('application.extensions.vendors.PHPExcel',true);
            $inputFileType = PHPExcel_IOFactory::identify($FileName);
            $objReader = PHPExcel_IOFactory::createReader($inputFileType);

            $objPHPExcel = @$objReader->load(@$_FILES['GasCustomerCheck']['tmp_name']['file_excel']);
            $objWorksheet = $objPHPExcel->getActiveSheet();
            $highestRow = $objWorksheet->getHighestRow(); // e.g. 10
            //$highestColumn = $objWorksheet->getHighestColumn(); // e.g 'F'
            $highestColumn = 'F';
            $highestColumnIndex = PHPExcel_Cell::columnIndexFromString($highestColumn);
            $cUid = Yii::app()->user->id;;
            $aRowInsert=array();
            for ($row = 2; $row <= $highestRow; ++$row)
            {
                $customer_name = trim($objWorksheet->getCellByColumnAndRow(1, $row)->getValue());
                $customer_address = trim($objWorksheet->getCellByColumnAndRow(2, $row)->getValue());
                $sale = trim($objWorksheet->getCellByColumnAndRow(3, $row)->getValue());
                $qty = trim($objWorksheet->getCellByColumnAndRow(4, $row)->getValue());
                $note = trim($objWorksheet->getCellByColumnAndRow(5, $row)->getValue());
                if($customer_name!=''){
                    $mInsert = new GasCustomerCheck();
                    $mInsert->code_no = MyFunctionCustom::getNextId('GasCustomerCheck', 'C'.date('y'), LENGTH_TICKET,'code_no');
                    $mInsert->uid_login = $cUid;
                    $mInsert->month = $model->month;
                    $mInsert->year = $model->year;
                    $mInsert->monitor_agent_id = $model->monitor_agent_id;
                    $mInsert->customer_name = $customer_name;
                    $mInsert->customer_address = $customer_address;
                    $mInsert->sale = $sale;
                    $mInsert->qty = $qty;
                    $mInsert->note_head_bo = $note;
                    $mInsert->save();
                }
            }
            
        } catch (Exception $exc) {
            throw new Exception("Model Có Lỗi Khi Insert Upload File: ". $exc->getMessage());
        }        
    }
    
    
    /**
     * @Author: ANH DUNG Now 24, 2014 
     * @Todo:validate file submit, xử lý validate thêm ( có phải là image hợp lệ không ), còn phần check đuôi file thì 
     * phần validate của model đã xử lý rồi.
     * @Param: $model model GasCustomerCheck
     */
    public static function validateFile($model, $needMore=array()){
        if(isset($_POST['GasCustomerCheck']['file_report']) ){
            $mFile = new GasCustomerCheck('Update_customer');
            $mFile->file_report  = CUploadedFile::getInstance($model,'file_report');
            if(!is_null($mFile->file_report)){
                MyFormat::IsImageFile($_FILES['GasCustomerCheck']['tmp_name']['file_report']);
            }
            // Now 24, 2014 ở đây chỉ xử lý thêm MyFormat::IsImageFile, còn phần bắt lỗi thì 
            // hàm valiate sau của model đã làm dc rồi
//            $mFile->validate();
//            if($mFile->hasErrors()){
//                $model->addError('file_report', "not use => File Error");
//            }
        }
    }
    
    /**
     * @Author: ANH DUNG Now 24, 2014 
     * To do: save file, chỉ save 1 file
     * @param: $model GasCustomerCheck
     * @param: $fieldName ex: file_report
     */
    public static function saveFile($model, $fieldName)
    {
        $model->$fieldName  = CUploadedFile::getInstance($model, $fieldName);
        if(is_null($model->$fieldName)) return '';        
        $year = MyFormat::GetYearByDate($model->created_date);
        $month = MyFormat::GetYearByDate($model->created_date, array( 'format'=>'m') );
        $pathUpload = GasCustomerCheck::$pathUpload."/$year/$month";
        $ext = $model->$fieldName->getExtensionName();
        $FileName = date('Y-m-d').time();
        $FileName = $FileName."-".ActiveRecord::randString().'.'.$ext;
        $imageProcessing = new ImageProcessing();
        $imageProcessing->createDirectoryByPath($pathUpload);
        $model->$fieldName->saveAs($pathUpload.'/'.$FileName);
        // thêm mới, có thể bỏ ra ngoài hàm
        $model->$fieldName = $FileName;
        $model->update(array($fieldName));
        GasCustomerCheck::resizeImage($model, $fieldName);
        // thêm mới, có thể bỏ ra ngoài hàm
        return $FileName;
    }
    
    /**
     * @Author: ANH DUNG Now 24, 2014 
     * To do: resize file scan
     * @param: $model model GasCustomerCheck
     * @param: $fieldName 
     */
    public static function resizeImage($model, $fieldName) {
        $year = MyFormat::GetYearByDate($model->created_date);
        $month = MyFormat::GetYearByDate($model->created_date, array( 'format'=>'m') );
        $pathUpload = GasCustomerCheck::$pathUpload."/$year/$month";
        $ImageHelper = new ImageHelper();     
        $ImageHelper->folder = '/'.$pathUpload;
        $ImageHelper->file = $model->$fieldName;
        $ImageHelper->aRGB = array(0, 0, 0);//full black background
        $ImageHelper->thumbs = self::$aSize;
//        $ImageHelper->createFullImage = true;
        $ImageHelper->createThumbs();
        $ImageHelper->deleteFile($ImageHelper->folder . '/' . $model->$fieldName);        
    }
    
    /**
     * @Author: ANH DUNG Now 24, 2014 
     * To do: delete file scan
     * @param: $model model user
     * @param: $fieldName is file_report
     * @param: $aSize
     */
    public static function RemoveFileOnly($pk, $fieldName) {
        $modelRemove = self::model()->findByPk($pk);
        if (is_null($modelRemove) || empty($modelRemove->$fieldName))
            return;
        $year = MyFormat::GetYearByDate($modelRemove->created_date);
        $month = MyFormat::GetYearByDate($modelRemove->created_date, array( 'format'=>'m') );
        $pathUpload = GasCustomerCheck::$pathUpload."/$year/$month";
        $ImageHelper = new ImageHelper();     
        $ImageHelper->folder = '/'.$pathUpload;
        $ImageHelper->deleteFile($ImageHelper->folder . '/' . $modelRemove->$fieldName);
        foreach ( self::$aSize as $key => $value) {
            $ImageHelper->deleteFile($ImageHelper->folder . '/' . $key . '/' . $modelRemove->$fieldName);
        }
    }
    
    protected function beforeDelete() {
        GasCustomerCheck::RemoveFileOnly($this->id, 'file_report');
        return parent::beforeDelete();
    }
    
    /**
     * @Author: ANH DUNG Apr 15, 2015
     */
    protected function beforeSave() {
        $cRole = Yii::app()->user->role_id;
        if(strpos($this->report_date, '/')){
            $this->report_date = MyFormat::dateConverDmyToYmd($this->report_date);
            MyFormat::isValidDate($this->report_date);
        }
        
        if($this->isNewRecord){
            $this->code_no = MyFunctionCustom::getNextId('GasCustomerCheck', 'C'.date('y'), LENGTH_TICKET,'code_no');
            $this->uid_login = Yii::app()->user->id;
            $this->role_id = Yii::app()->user->role_id;
            $this->agent_id = MyFormat::getAgentId();
            if($cRole == ROLE_ACCOUNTING){
                $this->status_check = GasCustomerCheck::STATUS_CHECKED;
            }
        }else{
//            $this->last_update_by = Yii::app()->user->id;
//            $this->last_update_time = date('Y-m-d H:i:s');
        }
        
        $this->type_customer = Users::GetTypeCustomer($this->rCustomer);
        $this->sale_id = Users::GetSaleIdOfCustomer($this->rCustomer);
        $this->sale_type = Users::GetTypeSale($this->rSale);
        $this->agent_id_of_customer = Users::GetAgentIdOfCustomerByModel($this->rCustomer);
        $aAttributes = array('report_handle');
        MyFormat::RemoveScriptOfModelField($this, $aAttributes);
        
        return parent::beforeSave();
    }
    
    /**
     * @Author: ANH DUNG Apr 15, 2015
     * @Todo: something
     * @Param: $model
     */
    public static function GetTableInfo($model) {
        $html = "<table class='table_border_only' cellspacing='0' cellpadding='0'> ";
//        $html .= "<thead>";
            $html .= "<tr>";
                $html .= "<td class='item_c item_b' colspan='3'>Nợ Vỏ</td>";
                $html .= "<td class='item_c item_b ' colspan='4'>Nợ Tiền</td>";
                $html .= "<td class='item_c item_b ' colspan='3'>Tồn Thực Tế</td>";
                $html .= "<td class='item_c item_b ' colspan='3'>Đã Thu</td>";
                $html .= "<td class='item_c item_b ' colspan='3'>Đề Xuất Thu</td>";
                $html .= "<td class='item_c item_b ' colspan='4'>Ý Kiến Khách Hàng</td>";
            $html .= "</tr>";
            $html .= "<tr>";
                $html .= "<td class='item_c item_b'>12</td>";
                $html .= "<td class='item_c item_b'>50</td>";
                $html .= "<td class='item_c item_b'>45</td>";
                
                $html .= "<td class='item_c item_b'>quá hạn</td>";
                $html .= "<td class='item_c item_b'>khó đòi</td>";
                $html .= "<td class='item_c item_b'>xấu</td>";
                $html .= "<td class='item_c item_b'>Gối đầu chết</td>";
                
                $html .= "<td class='item_c item_b'>12</td>";
                $html .= "<td class='item_c item_b'>50</td>";
                $html .= "<td class='item_c item_b'>45</td>";
                
                $html .= "<td class='item_c item_b'>12</td>";
                $html .= "<td class='item_c item_b'>50</td>";
                $html .= "<td class='item_c item_b'>45</td>";
                
                $html .= "<td class='item_c item_b'>12</td>";
                $html .= "<td class='item_c item_b'>50</td>";
                $html .= "<td class='item_c item_b'>45</td>";
                
                $html .= "<td class='item_c item_b'>Bảo Trì</td>";
                $html .= "<td class='item_c item_b'>Điều Phối</td>";
                $html .= "<td class='item_c item_b'>Giao Nhận</td>";
                $html .= "<td class='item_c item_b'>Kinh Doanh</td>";
            $html .= "</tr>";
        $html .= "<tr>";
            $html .= "<td class='item_c '>";
                $html .= "$model->debit_vo_12 <span class='display_none debit_vo_12'>$model->debit_vo_12</span>";
            $html .= "</td>";
            $html .= "<td class='item_c '>";
                $html .= "$model->debit_vo_50 <span class='display_none debit_vo_50'>$model->debit_vo_50</span>";
            $html .= "</td>";
            $html .= "<td class='item_c '>";
                $html .= "$model->debit_vo_45 <span class='display_none debit_vo_45'>$model->debit_vo_45</span>";
            $html .= "</td>";
            
            $html .= "<td class='item_c '>".ActiveRecord::formatCurrency($model->money_qua_han);                
            $html .= " <span class='display_none money_qua_han'>$model->money_qua_han</span></td>";
            $html .= "<td class='item_c '>".ActiveRecord::formatCurrency($model->money_kho_doi);                
            $html .= " <span class='display_none money_kho_doi'>$model->money_kho_doi</span></td>";
            $html .= "<td class='item_c '>".ActiveRecord::formatCurrency($model->money_xau);                
            $html .= " <span class='display_none money_xau'>$model->money_xau</span></td>";
            $html .= "<td class='item_c '>".ActiveRecord::formatCurrency($model->money_goi_dau_chet);                
            $html .= " <span class='display_none money_goi_dau_chet'>$model->money_goi_dau_chet</span></td>";
            
            
            $html .= "<td class='item_c '>";
                $html .= "$model->vo_inventory_12 <span class='display_none vo_inventory_12'>$model->vo_inventory_12</span>";
            $html .= "</td>";
            $html .= "<td class='item_c '>";
                $html .= "$model->vo_inventory_50 <span class='display_none vo_inventory_50'>$model->vo_inventory_50</span>";
            $html .= "</td>";
            $html .= "<td class='item_c '>";
                $html .= "$model->vo_inventory_45 <span class='display_none vo_inventory_45'>$model->vo_inventory_45</span>";
            $html .= "</td>";
            $html .= "<td class='item_c '>";
                $html .= "$model->vo_da_thu_12 <span class='display_none vo_da_thu_12'>$model->vo_da_thu_12</span>";
            $html .= "</td>";
            $html .= "<td class='item_c '>";
                $html .= "$model->vo_da_thu_50 <span class='display_none vo_da_thu_50'>$model->vo_da_thu_50</span>";
            $html .= "</td>";
            $html .= "<td class='item_c '>";
                $html .= "$model->vo_da_thu_45 <span class='display_none vo_da_thu_45'>$model->vo_da_thu_45</span>";
            $html .= "</td>";
            $html .= "<td class='item_c '>";
                $html .= "$model->vo_de_xuat_thu_12 <span class='display_none vo_de_xuat_thu_12'>$model->vo_de_xuat_thu_12</span>";
            $html .= "</td>";
            $html .= "<td class='item_c '>";
                $html .= "$model->vo_de_xuat_thu_50 <span class='display_none vo_de_xuat_thu_50'>$model->vo_de_xuat_thu_50</span>";
            $html .= "</td>";
            $html .= "<td class='item_c '>";
                $html .= "$model->vo_de_xuat_thu_45 <span class='display_none vo_de_xuat_thu_45'>$model->vo_de_xuat_thu_45</span>";
            $html .= "</td>";
            
            
            $html .= "<td class='item_c '>";
                $html .=  GasCustomerCheck::$ARR_RATE[$model->rate_bao_tri];
            $html .= "</td>";
            $html .= "<td class='item_c '>";
                $html .=  GasCustomerCheck::$ARR_RATE[$model->rate_dieu_phoi];
            $html .= "</td>";
            $html .= "<td class='item_c '>";
                $html .=  GasCustomerCheck::$ARR_RATE[$model->rate_giao_nhan];
            $html .= "</td>";
            $html .= "<td class='item_c '>";
                $html .=  GasCustomerCheck::$ARR_RATE[$model->rate_kinh_doanh];
            $html .= "</td>";
        $html .= "</tr>";
        
        
        $html .= "</table>";
        return $html;
    }
    
    /**
     * @Author: ANH DUNG Apr 20, 2015
     * @Todo: get html for sum tr last
     */
    public static function GetHtmlTrLast() {
        $html = "<table class='table_border_only' cellspacing='0' cellpadding='0'> ";
            $html .= "<tr>";
                $html .= "<td class='item_c item_b' colspan='3'>Nợ Vỏ</td>";
                $html .= "<td class='item_c item_b ' colspan='4'>Nợ Tiền</td>";
                $html .= "<td class='item_c item_b ' colspan='3'>Tồn Thực Tế</td>";
                $html .= "<td class='item_c item_b ' colspan='3'>Đã Thu</td>";
                $html .= "<td class='item_c item_b ' colspan='3'>Đề Xuất Thu</td>";
//                $html .= "<td class='item_c item_b ' colspan='4'>Ý Kiến Khách Hàng</td>";
            $html .= "</tr>";
            $html .= "<tr>";
                $html .= "<td class='item_c item_b'>12</td>";
                $html .= "<td class='item_c item_b'>50</td>";
                $html .= "<td class='item_c item_b'>45</td>";
                
                $html .= "<td class='item_c item_b'>quá hạn</td>";
                $html .= "<td class='item_c item_b'>khó đòi</td>";
                $html .= "<td class='item_c item_b'>xấu</td>";
                $html .= "<td class='item_c item_b'>Gối đầu chết</td>";

                $html .= "<td class='item_c item_b'>12</td>";
                $html .= "<td class='item_c item_b'>50</td>";
                $html .= "<td class='item_c item_b'>45</td>";
                
                $html .= "<td class='item_c item_b'>12</td>";
                $html .= "<td class='item_c item_b'>50</td>";
                $html .= "<td class='item_c item_b'>45</td>";
                
                $html .= "<td class='item_c item_b'>12</td>";
                $html .= "<td class='item_c item_b'>50</td>";
                $html .= "<td class='item_c item_b'>45</td>";
                
//                $html .= "<td class='item_c item_b'>Bảo Trì</td>";
//                $html .= "<td class='item_c item_b'>Điều Phối</td>";
//                $html .= "<td class='item_c item_b'>Giao Nhận</td>";
//                $html .= "<td class='item_c item_b'>Kinh Doanh</td>";
            $html .= "</tr>";
        $html .= "<tr>";
            $html .= "<td class='item_c sum_debit_vo_12'>";
            $html .= "</td>";
            $html .= "<td class='item_c sum_debit_vo_50'>";
            $html .= "</td>";
            $html .= "<td class='item_c sum_debit_vo_45'>";
            $html .= "</td>";
            
            $html .= "<td class='item_c sum_money_qua_han'>";
            $html .= "</td>";
            $html .= "<td class='item_c sum_money_kho_doi'>";
            $html .= "</td>";
            $html .= "<td class='item_c sum_money_xau'>";
            $html .= "</td>";
            $html .= "<td class='item_c sum_money_goi_dau_chet'>";
            $html .= "</td>";
            
            $html .= "<td class='item_c sum_vo_inventory_12'>";
            $html .= "</td>";
            $html .= "<td class='item_c sum_vo_inventory_50'>";
            $html .= "</td>";
            $html .= "<td class='item_c sum_vo_inventory_45'>";
            $html .= "</td>";
            $html .= "<td class='item_c sum_vo_da_thu_12'>";
            $html .= "</td>";
            $html .= "<td class='item_c sum_vo_da_thu_50'>";
            $html .= "</td>";
            $html .= "<td class='item_c sum_vo_da_thu_45'>";
            $html .= "</td>";
            $html .= "<td class='item_c sum_vo_de_xuat_thu_12'>";
            $html .= "</td>";
            $html .= "<td class='item_c sum_vo_de_xuat_thu_50'>";
            $html .= "</td>";
            $html .= "<td class='item_c sum_vo_de_xuat_thu_45'>";
            $html .= "</td>";
        $html .= "</tr>";
        $html .= "</table>";
        return $html;
    }
    
    
    /**
     * @Author: ANH DUNG Jun 09, 2015
     * @Todo: kiểm tra xem loại user nào được phép nhìn vào edit phần 1
     * Phần tô đỏ sẽ do kế toán bán hàng nhập và do kế toán văn phòng kiểm tra, sửa lại
     * Phần còn lại do giám sát đại lý nhập
     * @Param: $model
     */
    public static function CanViewApprove() {
        $cRole = Yii::app()->user->role_id;
        $aRoleCheck = array(ROLE_SUB_USER_AGENT, ROLE_ACCOUNTING);
        return in_array($cRole, $aRoleCheck);
    }
    
    /**
     * @Author: ANH DUNG Jun 09, 2015
     * @Todo: handle save update
     * @Param: $model
     */
    public function HandleSaveUpdate() {
        $cRole = Yii::app()->user->role_id;
        if($cRole == ROLE_ACCOUNTING){
            $this->HandleUpdateByAccounting();
        }elseif($cRole == ROLE_MONITOR_AGENT){
            $this->HandleUpdateByMonitor();
        }else{
            $this->update(); // for user dai ly update sau khi tao
        }
    }
    
    /**
     * @Author: ANH DUNG Jun 09, 2015
     * @Todo: UpdateByAccounting
     */
    public function HandleUpdateByAccounting() {
        $aUpdate = array('customer_id','debit_vo_12', 'debit_vo_50','debit_vo_45',
            'money_qua_han','money_kho_doi','money_xau','money_goi_dau_chet',
            'last_accounting_edit','last_accounting_edit_date',
            'status_check');
        $this->status_check = GasCustomerCheck::STATUS_CHECKED;
        $this->last_accounting_edit = Yii::app()->user->id;
        $this->last_accounting_edit_date = date('Y-m-d H:i:s');
        $this->update($aUpdate);
    }
    
    /**
     * @Author: ANH DUNG Jun 09, 2015
     * @Todo: HandleUpdateByMonitor
     */
    public function HandleUpdateByMonitor() {
        if(is_null($this->file_report)){
            $this->file_report = $this->OldFile;
        }else{
            GasCustomerCheck::RemoveFileOnly($this->id, 'file_report');
        }
        $aUpdate = array('report_date','vo_inventory_12', 'vo_inventory_50','vo_inventory_45',
            'vo_da_thu_12','vo_da_thu_50','vo_da_thu_45','vo_de_xuat_thu_12',
            'vo_de_xuat_thu_50','vo_de_xuat_thu_45',
            'rate_bao_tri','rate_dieu_phoi','rate_giao_nhan','rate_kinh_doanh','money_get',
            'last_monitor_edit','last_monitor_edit_date',
            'report_handle','file_report');
        $this->last_monitor_edit = Yii::app()->user->id;
        $this->last_monitor_edit_date = date('Y-m-d H:i:s');
        $this->update($aUpdate);
        GasCustomerCheck::saveFile($this, 'file_report');
    }
    
}