<?php

/**
 * This is the model class for table "{{_reward}}".
 *
 * The followings are the available columns in table '{{_reward}}':
 * @property string $id
 * @property string $title
 * @property string $image
 * @property string $banner
 * @property string $partner_logo
 * @property string $partner_name
 * @property string $partner_address
 * @property integer $reward_category_id
 * @property integer $type
 * @property string $material_id
 * @property string $description
 * @property string $date_from
 * @property string $date_to
 * @property string $point_apply
 * @property string $created_date
 * @property string $created_by
 */
class Reward extends BaseSpj
{
    public $autocomplete_material;
    public $start_date_from, $start_date_to, $end_date_from, $end_date_to, $file_name, $file_image;
    
    const TYPE_POINT    = 1;
    const TYPE_USE      = 2;
    const TYPE_LIST_ALL = 1;
    const TYPE_LIST_USER = 2;
    const COUNT_IN_LIST = 5;
    const TYPE_INBOUND = 1;
    const TYPE_OUTBOUND = 2;
    const REWARD_CATEGORY_SERVICE = 1;
    const REWARD_CATEGORY_FOOD = 2;
    
    const API_LISTING_REWARD_PER_PAGE = 10;
    
    
    public function getListPhoneUsersDev(){
        return [
            '0981471595',//NamNH
            '0389945321',//TrungND
        ];
    }
    
    public function isReward($mUsers){
        if(empty($mUsers->username)){
            return false;
        }
        if(in_array($mUsers->username, $this->getListPhoneUsersDev())){
            return true;
        }
        return false;
    }
    
    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return Reward the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{_reward}}';
    }
    
    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        return [
            array('title,material_id, qty, point_apply, date_from, count_per_user, date_to, partner_phone', 'required', 'on'=>'create, update'),
            array('title, partner_name, partner_address, description, date_from, date_to, created_date, start_date_from, start_date_to, end_date_from, end_date_to', 'safe'),
            ['qty,file_image,count_per_user,partner_phone,partner_about,highlights,id, title, image, banner, partner_logo, partner_name, partner_address, reward_category_id, type, material_id, description, date_from, date_to, point_apply, created_date, created_by, file_name', 'safe'],
        ];
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        return [
            'rMaterial' => array(self::BELONGS_TO, 'GasMaterials', 'material_id'),
            'rFileImage' => array(self::HAS_MANY, 'GasFile', 'belong_id',
                'on' => 'rFileImage.type=' . GasFile::TYPE_16_REWARD_IMAGE,
                'order' => 'rFileImage.id ASC',
            ),
            'rFileBanner' => array(self::HAS_MANY, 'GasFile', 'belong_id',
                'on' => 'rFileBanner.type=' . GasFile::TYPE_17_REWARD_BANNER,
                'order' => 'rFileBanner.id ASC',
            ),
            'rFileLogo' => array(self::HAS_MANY, 'GasFile', 'belong_id',
                'on' => 'rFileLogo.type=' . GasFile::TYPE_18_REWARD_PARTNER_LOGO,
                'order' => 'rFileLogo.id ASC',
            ),
        ];
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Tiêu đề',
            'image' => 'Ảnh',
            'banner' => 'Banner',
            'partner_logo' => 'Logo đối tác',
            'partner_name' => 'Tên đối tác',
            'partner_address' => 'Địa  chỉ',
            'partner_phone' => 'Số điện thoại',
            'reward_category_id' => 'Loại dịch vụ',
            'type' => 'Loại',
            'material_id' => 'Vật tư',
            'description' => 'Điều kiện sử dụng',
            'date_from' => 'Từ ngày',
            'date_to' => 'Đến ngày',
            'point_apply' => 'Điểm áp dụng',
            'created_date' => 'Ngày tạo',
            'created_by' => 'Người tạo',
            'partner_about' => 'về công ty',
            'highlights' => 'Điểm nổi bật',
            'count_per_user' => 'Số lần đổi/khách hàng',
            'start_date_to' => 'Đến',
            'end_date_to' => 'Đến',
            'qty' => 'Số lượng'
        ];
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        $criteria=new CDbCriteria;

	$criteria->compare('t.id',$this->id,true);
	$criteria->compare('t.title',$this->title,true);
	$criteria->compare('t.image',$this->image,true);
	$criteria->compare('t.banner',$this->banner,true);
	$criteria->compare('t.partner_logo',$this->partner_logo,true);
	$criteria->compare('t.partner_name',$this->partner_name,true);
	$criteria->compare('t.partner_address',$this->partner_address,true);
	$criteria->compare('t.reward_category_id',$this->reward_category_id);
	$criteria->compare('t.type',$this->type);
	$criteria->compare('t.material_id',$this->material_id,true);
	$criteria->compare('t.description',$this->description,true);
	$criteria->compare('t.date_from',$this->date_from,true);
	$criteria->compare('t.date_to',$this->date_to,true);
	$criteria->compare('t.point_apply',$this->point_apply,true);
	$criteria->compare('t.created_date',$this->created_date,true);
	$criteria->compare('t.created_by',$this->created_by,true);
        $criteria->order = 't.id DESC';
        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
            'pagination'=>array(
                'pageSize'=> Yii::app()->user->getState('pageSize',Yii::app()->params['defaultPageSize']),
            ),
        ));
    }
    
    public function getArrayType(){
        return[
            Reward::TYPE_INBOUND => 'Nội bộ',
            Reward::TYPE_OUTBOUND => 'Đối tác',
        ];
    }
    
    public function getType(){
        $aStatus = $this->getArrayType();
        return isset($aStatus[$this->type]) ? $aStatus[$this->type] : "";
    }
    
    public function getMaterial(){
        return $this->material_id;
    }
    
    public function getArrayCategory(){
        return[
            Reward::REWARD_CATEGORY_SERVICE => 'Dịch vụ',
            Reward::REWARD_CATEGORY_FOOD => 'Ẩm thực',
        ];
    }
    
    public function getArrayCategoryImage(){
        return[
            Reward::REWARD_CATEGORY_SERVICE => $baseUrl = Yii::app()->createAbsoluteUrl('/').'/upload/rewards/ic-reward-5.png',
            Reward::REWARD_CATEGORY_FOOD => $baseUrl = Yii::app()->createAbsoluteUrl('/').'/upload/rewards/ic-reward-2.png',
        ];
    }
    
     public function getReward(){
        $aReward = $this->getArrayCategory();
        return isset($aReward[$this->reward_category_id]) ? $aReward[$this->reward_category_id] : "";
    }
    
    public function getTitle() {
        return $this->title;
    }
    
    public function getSubTitle(){
        return 'Từ '. MyFormat::dateConverYmdToDmy($this->date_from) . ' đến '.MyFormat::dateConverYmdToDmy($this->date_to);
    }
    
    public function getPointApply() {
        return $this->point_apply;
    }
    
    public function getBanner(){
        return $this->banner;
    }
    public function getPartnerName(){
        return $this->partner_name;
    }
    public function getPartnerAddress(){
        $aResult = explode(';',$this->partner_address);
        $str = "<table class='f_size_15' cellpadding='0' cellspacing = '0' style = '' >";
        foreach ($aResult as $key => $value) {
            if($value){
                $str .= "<tr style = ''>";
                    $str .= "<td style = ''>".($key+1)."</td>";
                    $str .= "<td style = ''>".$value."</td>";
                $str .= "</tr>";
            }
        }
        $str .= "</table>";
        return $str;
    }
    public function getDescription(){
        return $this->description;
    }
    
    public function getLogo(){
        return $this->partner_logo;
    }
    public function canUpdate(){
        return true;
    }
    
    public function deleteFile() {
        if(isset($_POST['delete_file']) && is_array($_POST['delete_file']) && count($_POST['delete_file'])){
            $criteria = new CDbCriteria;
            $sParamsIn = implode(',', $_POST['delete_file']);
            $criteria->addCondition("t.id IN ($sParamsIn)");
            $criteria->addInCondition("t.type",[GasFile::TYPE_16_REWARD_IMAGE,GasFile::TYPE_17_REWARD_BANNER,GasFile::TYPE_18_REWARD_PARTNER_LOGO]);
            $models = GasFile::model()->findAll($criteria);
            foreach($models as $model){
                $model->delete();
            }
        }
    }
    
    public function saveFile() {
        $mGasFile = new GasFile();
        $this->banner = $mGasFile->saveRecordFileMore($this,'banner', GasFile::TYPE_17_REWARD_BANNER);
        $this->partner_logo = GasFile::SaveRecordFileMore($this,'partner_logo', GasFile::TYPE_18_REWARD_PARTNER_LOGO);
//        save multi image
        GasFile::SaveRecordFileMore($this,'file_image', GasFile::TYPE_16_REWARD_IMAGE);
        $this->update();
    }
    public function getFileOnly($nameRelation) {
        $aFile = $this->$nameRelation;
        $str = '';
        if(count($aFile)){
            foreach($aFile as $key=>$item){
                $str.="<a target='_blank' class='gallery' href='".Yii::app()->createAbsoluteUrl('admin/ajax/viewImageProfileHs', array('id'=>$item->id, 'model'=>'GasFile','nameRelation'=>$nameRelation))."'> ";
                $str.= 'Xem hình ';
                $str.="</a>";
            }
        }
       return $str;
    }
    
    /** @Author: NGUYEN KHANH TOAN  Sep 10, 2018 -- handle listing api */
    public function handleApiListReward(&$result, $q) {
//        $aReward = $this->getArrayCategory();
//        $aRewardIcon = $this->getArrayCategoryImage();
//        foreach ($aReward as $key => $name) {
//            $data                  = [];
//            $data['id']            = $key;
//            $data['icon']          = !empty($aRewardIcon[$key]) ? $aRewardIcon[$key] : '';
//            $data['name_category'] = $name;
//            $dataProvider   = $this->apiListingReward($key);
//            $models         = $dataProvider->data;
//            $CPagination    = $dataProvider->pagination;
//            $data['total_record']  = $CPagination->itemCount;
//            $data['rewards']       = [];
//            foreach($models as $model){
//                $model->mAppUserLogin = $this->mAppUserLogin;
//               $data['rewards'][]         = $model->formatAppItemListReward();
//            }
//            $result['record'][] = $data;
//        }
        $data                  = [];
        $data['id']            = '';
        $data['icon']          = '';
        $data['name_category'] = 'Danh mục ưu đãi';
        $dataProvider   = $this->apiListingReward('');
        $models         = $dataProvider->data;
        $CPagination    = $dataProvider->pagination;
        $data['total_record']  = $CPagination->itemCount;
        $data['rewards']       = [];
        foreach($models as $model){
            $model->mAppUserLogin = $this->mAppUserLogin;
            $data['rewards'][]         = $model->formatAppItemListReward();
        }
        $result['record'][] = $data;
    }
    
    /** @Author: NamNH Apr 24, 2019
     *  @Todo: get category
     **/
    public function handleApiListCategory(&$result, $q) {
//        $aReward = $this->getArrayCategory();
//        $aRewardIcon = $this->getArrayCategoryImage();
//        foreach ($aReward as $key => $name) {
//            $data                  = [];
//            $data['id']            = $key;
//            $data['icon']          = !empty($aRewardIcon[$key]) ? $aRewardIcon[$key] : '';
//            $data['name_category'] = $name;
//            $result['record'][]    = $data;
//        }
    }
    
    /** @Author: NamNH Apr 24, 2019
     *  @Todo: get rewards by category
     **/
    public function handleApiListRewardByCategory(&$result, $q) {
        $result['record'] = [];
        $category = !empty($q->category) ? $q->category : '';
        $type = !empty($q->type) ? $q->type : self::TYPE_LIST_ALL; // tùy chỉnh search tất cả hoặc search ưu đãi của tôi
        if($type == self::TYPE_LIST_ALL){
            $dataProvider   = $this->apiListingReward($category,self::API_LISTING_REWARD_PER_PAGE,$q->page);
            $models         = $dataProvider->data;
            $CPagination    = $dataProvider->pagination;
            $result['total_record'] = $CPagination->itemCount;
            $result['total_page']   = $CPagination->pageCount;
            foreach($models as $model){
                $model->mAppUserLogin = $this->mAppUserLogin;
                $result['record'][]         = $model->formatAppItemListReward();
            }
        }elseif($type == self::TYPE_LIST_USER){
            $dataProvider   = $this->ApiListingMyReward(self::API_LISTING_REWARD_PER_PAGE,$q->page);
            $models         = $dataProvider->data;
            $CPagination    = $dataProvider->pagination;
            $result['total_record'] = $CPagination->itemCount;
            $result['total_page']   = $CPagination->pageCount;
            foreach($models as $model){
                $model->mAppUserLogin = $this->mAppUserLogin;
//                model => CodePartner
                $result['record'][]         = $model->formatAppItemListReward();
            }
        }
    }
    
     /**
    * @Author: NGUYEN KHANH TOAN Sep 10 2018
    * @Todo: get data listing 
    * @param: $q object post params from app client
    */
    public function apiListingReward($category,$countPage = Reward::COUNT_IN_LIST,$page = 0) {
        $criteria = new CDbCriteria();
        $criteria->order = 't.id DESC';
        $criteria->compare('t.reward_category_id',$category);
        $dataProvider = new CActiveDataProvider($this, array(
            'criteria' => $criteria,
            'pagination'=>array(
                'pageSize' => $countPage,
                'currentPage' => $page,
            ),
          ));
        return $dataProvider;
    }
    
    /** @Author: NGUYEN KHANH TOAN Sep 10 2018
     *  @Todo: dùng chung để format trả xuống list + view của app Gas24h
     **/
    public function formatAppItemListReward($isDetail = false) {
        $mUser = $this->mAppUserLogin;
        $temp = [];
        $temp['id']                     = $this->id;
        $temp['cat_id']                 = $this->reward_category_id;
        $temp['title']                  = $this->getTitle();
        $temp['sub_title']              = $this->getSubTitle();
        $temp['point_apply']            = (int)$this->point_apply;
        $temp['point_default']          = (int)$this->point_apply;
        $temp['image']                  = $this->getUrlImage('rFileImage');
        $temp['date_from']              = MyFormat::dateConverYmdToDmy($this->date_from);
        $temp['date_to']                = MyFormat::dateConverYmdToDmy($this->date_to);
        $temp['allow_buy']              = $this->allowBuy($mUser->id);
        $temp['count_per_user']         = $this->count_per_user;
        if($isDetail){
            $temp['image']                  = $this->getUrlImage('rFileImage',true);
            $temp['banner']                 = $this->getUrlImage('rFileBanner');
            $temp['partner_logo']           = $this->getUrlImage('rFileLogo');
            $temp['partner_name']           = $this->getPartnerName();
//            $temp['material_id']            = $this->material_id;
            $temp['condition']              = explode(';',$this->description);
            $temp['partner_about']          = $this->getPartnerName();
            $temp['highlights']             = explode(';',$this->highlights);
            $temp['partner_contract']       = [
                'address' => $this->partner_address,
                'phone' => $this->partner_phone,
            ];
        }
        return $temp;
    }
    
    /** @Author: NamNH May 07, 2019
     *  @Todo: check allow for user
     **/
    public function allowBuy($user_id){
        $criteria = new CDbCriteria();
        $criteria->compare('t.customer_id',$user_id);
        $mCodePartner = new CodePartner();
        $criteria->compare('t.type', $mCodePartner->getArrayTypeReward());
        $criteria->compare('t.reward_id',  $this->id);
        $count = CodePartner::model()->count($criteria);
        if($count >= $this->count_per_user){
            return false;
        }
//        $mUser = $this->mAppUserLogin;
//        $mRewardPoint = new RewardPoint();
//        $pointNow = $mRewardPoint->calculatePoint($mUser->id);
//        $reward_point = $this->point_apply;    
//        if($pointNow < $reward_point ){
//            return false;
//        }
        return true;
    }
     /** @Author: NGUYEN KHANH TOAN Sep 10, 2018
     *  @Todo: get view reward
     **/
    public function getRewardView() {
        $criteria = new CDbCriteria();
        $criteria->compare('t.id', $this->id);
        return Reward::model()->find($criteria);
    }
    
    public function getUrlImage($nameRelation,$array = false){
        $aFile = $this->$nameRelation;
        $str = $array ? [] : '';
        if(!empty($aFile[0])){
            if($array){
                foreach ($aFile as $key => $mFile) {
                    $str[] = ImageProcessing::bindImageByModel($mFile,'','',array('size'=>'size2'));
                }
            }else{
                $str.= ImageProcessing::bindImageByModel($aFile[0],'','',array('size'=>'size2'));
            }
        }
       return $str;
    }
    
    public function getViewImg($nameRelation){
        $str = '';
        $aFile = $this->$nameRelation;
        if(!empty($aFile)){
            foreach ($aFile as $key => $File) {
                if(empty($File->file_name) && !in_array($File->type, GasFile::$TYPE_RESIZE_IMAGE)){
                   return '';
                }
                $str .="<a class='gallery' href='".Yii::app()->createAbsoluteUrl('admin/ajax/viewImageProfileHs', array('id'=>$File->id, 'model'=>'GasFile','nameRelation'=>$nameRelation))."'> ";
                    $str.="<img width='80' height='60' src='".ImageProcessing::bindImageByModel($File,'','',array('size'=>'size1'))."'>";
                $str.="</a>";
                $str.=' <p style="display:inline-block;">  Xóa</p><input type="checkbox" name="delete_file[]" value="'.$File->id.'">';
            }
            $str.='<br><br>';
        }
        return $str;
    }
    public function afterFind() {
        $this->list_id_image = [$this->image,$this->banner, $this->partner_logo];
        return parent::afterFind();
    }
    
    public function getImage($nameRelation){
        $aResult = [];
        $aFile = $this->$nameRelation;
        if(!empty($aFile)){
            foreach ($aFile as $key => $File) {
                $str = '';
                if(empty($File->file_name) && !in_array($File->type, GasFile::$TYPE_RESIZE_IMAGE)){
                   return '';
                }
                $str .="<a class='gallery' href='".Yii::app()->createAbsoluteUrl('admin/ajax/viewImageProfileHs', array('id'=>$File->id, 'model'=>'GasFile','nameRelation'=>$nameRelation))."'> ";
                    $str.="<img width='80' height='60' src='".ImageProcessing::bindImageByModel($File,'','',array('size'=>'size1'))."'>";
                $str.="</a>";
                $aResult[$File->id] = $str;
            }
        }
        return $aResult;
    }
    
    public function formatSellBeforeUpdate(&$mSell){
        if(empty($mSell->id)){
            return;
        }
        $criteria = new CDbCriteria();
        $criteria->compare('t.sell_id', $mSell->id);
        $criteria->compare('t.type', CodePartner::TYPE_REWARD);
        $mCodePartner = CodePartner::model()->find($criteria);
        if(!empty($mCodePartner)){
            $mSell->reward_id = $mCodePartner->reward_id;
        }
    }
    
    /** @Author: NamNH  Jun 12, 2019`
     *  @Todo: save reward by $msell
     *  @Param:$msell
     **/
    public function saveRewardInSell($msell){
        $mCodePartner = new CodePartner();
        if($this->checkReward($msell)){
            $mCodePartner->handleExchangeWeb($msell);
        }
//        check reward app
        if($this->checkRewardApp($msell)){
            $mCodePartner->handleExchangeAppFromWeb($msell);
        }
        $mRewardPoint = new RewardPoint();
        $mRewardPoint->updatePointOfUser($msell->customer_id);
    }
    
    /** @Author: NamNH Sep1919
    *  @Todo:check reward app
    *  @Param:$mSell
    **/
    public function checkRewardApp($mSell){
        if(empty($mSell->rewardApp)){
            return true;
        }
        foreach ($mSell->rewardApp as $key => $mCodePartnerId) {
            $mCodePartner   = CodePartner::model()->findByPk($mCodePartnerId);
            if(empty($mCodePartner)){
                $mSell->addError('reward_id', "Không tìm thấy quà app");
                return false;
            }
            if($mCodePartner->sell_id > 0 && $mCodePartner->sell_id != $mSell->id){
                $mSell->addError('reward_id', "Quà app đã được tạo trên đơn hàng khác");
                return false;
            }
            if($mCodePartner->customer_id != $mSell->customer_id){
                $mSell->addError('reward_id', "Quà app không áp dụng cho khách hàng");
                return false;
            }
            $mReward        = Reward::model()->findByPk($mCodePartner->reward_id);
            if(empty($mReward)){
                $mSell->addError('reward_id', "Không tìm thấy quà app");
                return false;
            }
            if(!$this->checkDetailSell($mSell,$mReward)){
                $mSell->addError('reward_id', "Quà tặng app chưa được thêm vào chi tiết");
                return false;
            }
        }
        return true;
    }

    /** @Author: NamNH date
     *  @Todo: check error
     **/
    public function checkReward(&$mSell) {
        if(empty($mSell->reward_id)){
            return true;
        }
        $mUser = Users::model()->findByPk($mSell->customer_id);
        if(empty($mUser)){
            $mSell->addError('reward_id', "Không tìm thấy khách hàng");
            return false;
        }
        $mCodePartner = new CodePartner();
        $mOld = $mCodePartner->getRewardByCustomer($mUser,$mSell->id);
        $mRewardPoint = new RewardPoint();
        $pointNow = $mRewardPoint->calculatePoint($mUser->id);
        $mReward = Reward::model()->findByPk($mSell->reward_id);
        if(empty($mReward)){
            $mSell->addError('reward_id', "Không tìm thấy quà");
            return false;
        }
        if(!$mReward->allowBuy($mUser->id)){
            $mSell->addError('reward_id', "Quà không áp dụng cho khách hàng");
            return false;
        }
        if(!empty($mOld)){
            $reward_point = $mReward->point_apply;        
            if(($pointNow+$mOld->reward_point) < $reward_point ){
                $mSell->addError('reward_id', "Điểm không đủ");
                return false;
            }
        }else{
            $reward_point = $mReward->point_apply;  
            if($pointNow < $reward_point ){
                $mSell->addError('reward_id', "Điểm không đủ");
                return false;
            }
        }
        if(!$this->checkDetailSell($mSell,$mReward)){
            $mSell->addError('reward_id', "Quà tặng chưa được thêm vào chi tiết");
            return false;
        }
        return true;
    }
    
    /** @Author: NamNH Jun 18, 2019
     *  @Todo: check detail sell exists material of reward
     **/
    public function checkDetailSell($mSell,$mReward){
        $aDetail = $mSell->aDetail;
        foreach ($aDetail as $key => $mDetail) {
            if($mDetail->materials_id == $mReward->material_id){
                return true;
            }
        }
        return false;
    }
    
     /**
    * @Author: NGUYEN KHANH TOAN Sep 10 2018
    * @Todo: get data listing 
    * @param: $q object post params from app client
    */
    public function ApiListingMyReward($countPage = Reward::COUNT_IN_LIST,$page = 0) {
        return $this->getCodePartnerReward($this->mAppUserLogin,$countPage,$page);
    }
    
    /** @Author: NamNH Jun 26,2019
     *  @Todo: get list reward
     **/
    public function getCodePartnerReward($mUser,$countPage,$page){
        $mCodePartner = new CodePartner();
        $criteria = new CDbCriteria();
        $criteria->order = 't.id DESC';
        $criteria->compare('t.customer_id',$mUser->id);
        $criteria->compare('t.type', $mCodePartner->getArrayTypeReward());
        $dataProvider = new CActiveDataProvider($mCodePartner, array(
            'criteria' => $criteria,
            'pagination'=>array(
                'pageSize' => $countPage,
                'currentPage' => $page,
            ),
          ));
        return $dataProvider;
    }
    
    public function formatBeforeSave(){
        $this->date_from    = MyFormat::dateConverDmyToYmd($this->date_from);
        $this->date_to      = MyFormat::dateConverDmyToYmd($this->date_to);
    }
    
    public function formatBeforeView(){
        $this->date_from    = MyFormat::dateConverYmdToDmy($this->date_from);
        $this->date_to    = MyFormat::dateConverYmdToDmy($this->date_to);
    }
}