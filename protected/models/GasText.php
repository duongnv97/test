<?php

/**
 * This is the model class for table "{{_gas_text}}".
 *
 * The followings are the available columns in table '{{_gas_text}}':
 * @property string $id
 * @property string $code_no
 * @property string $uid_login
 * @property integer $type
 * @property integer $number_text
 * @property string $number_sign
 * @property string $date_published
 * @property string $short_content
 * @property integer $status
 * @property string $created_date
 * 
 * The followings are the available model relations:
 * @property GasDebts[]                     $rGasDebts          List gas debts models
 * @property GasTextPersonResponsibles[]    $rGasTextPersons    List gas text person responsible models
 */
class GasText extends BaseSpj
{
    public $MAX_ID;
    public $aModelDetail;
    public $mDetail;
    public static $max_upload = 50;
    public static $max_upload_show = 10;
    public $message;
    public $is_view_all = 0;
    
    const TYPE_ANOUNCE              = 1;
    const TYPE_POLICY               = 2;
    const TYPE_POLICY_EXPIRE        = 3;
    const TYPE_DECISION_PHAT        = 4;
    const TYPE_DECISION_BO_NHIEM    = 5;
    const TYPE_DECISION_CHANGE_POSITION     = 6;
    const TYPE_DECISION_AWARD               = 7;
    
    const STT_APPROVED    = 1;
    const STT_NO_APPROVED = 2;
    const STT_NEW         = 3;
    
    const TAB_APPROVED      = 1;
    const TAB_WAIT_APPROVE  = 2;
    
    public function getArrayType() {
        return [
            GasText::TYPE_ANOUNCE               => 'Thông Báo',
            GasText::TYPE_POLICY                => 'Chính sách',
            GasText::TYPE_POLICY_EXPIRE         => 'Các chính sách hết hạn',
            GasText::TYPE_DECISION_PHAT         => 'Quyết định tính lỗi',
            GasText::TYPE_DECISION_BO_NHIEM     => 'Quyết định Bổ nhiệm',
            GasText::TYPE_DECISION_CHANGE_POSITION      => 'Quyết định điều động, Luân chuyển',
            GasText::TYPE_DECISION_AWARD                => 'Quyết định khen thưởng',
        ];
    }
    
    /** @Author: DuongNV Nov 20,2018
     *  @Todo: Get array id cty => url hình ảnh chữ ký
     **/
    public function getArraySignImage() {
        $upload_url = "/upload/text_sign";
        return [
            868629 => $upload_url . "/sign_dkbd.jpg", // Cty dầu khí biển đông
            868624 => $upload_url . "/sign_hm.jpg", // hướng minh
            868628 => $upload_url . "/sign_khlcl_2.jpg", // khí hóa lỏng cửu long
            868632 => $upload_url . "/sign_dkbinhdinh.jpg", // dầu khí bình định
            874246 => $upload_url . "/sign_dkdh.jpg", // dầu khí đại hàn
            868626 => $upload_url . "/sign_dkmn_2.jpg", // dầu khí miền nam
            874297 => $upload_url . "/sign_tvtkdkd.jpg", // tư vấn thiết kế dầu khí delta
            868625 => $upload_url . "/sign_dkkl.jpg", // dầu khí kiên long
        ];
    }
    
    public function getTypeShow() {
        $aType = $this->getArrayType();
        return isset($aType[$this->type]) ? $aType[$this->type] : '';
    }
    
    /** @Author: ANH DUNG Now 10, 2017
     *  @Todo: get some uid of Biển Đông
     **/
    public function getUidBienDong() {
        return [GasConst::UID_DAT_LX, GasConst::UID_THANG_LX, GasConst::UID_NGAN_LT, GasConst::UID_HUE_LT];
    }
    
    // lấy số ngày cho phép user cập nhật
    public static function getDayAllowUpdate(){
        return Yii::app()->params['days_update_text_file'];
    }
    
    public static function model($className=__CLASS__)
    {
            return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
            return '{{_gas_text}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        return array(
            array('company_id, type, number_text, number_sign, date_published, short_content', 'required', 'on'=>'create,update'),
            array('type', 'CheckMaxCreateInDay', 'on'=>'create'),
            array('message', 'required', 'on'=>'post_comment'),
            array('number_sign', 'length', 'max'=>250),
            array('short_content', 'length', 'max'=>500),
            array('is_view_all, date_from, date_to, role_id, company_id, stop_comment,last_comment,message,date_expired, id, code_no, uid_login, type, number_text, number_sign, date_published, short_content, status, created_date, note', 'safe'),
        );
    }
    
    public function CheckMaxCreateInDay($attribute,$params)
    {
        try {
            if(!$this->hasErrors()){
                $max_qty = Yii::app()->params['max_van_ban_create_in_day'];
                $qty = self::GetQtyCreateInDay(date('Y-m-d'));
                $date_published = date('Y-m-d');
                if(strpos($this->date_published, '/')){
                    $date_published = MyFormat::dateConverDmyToYmd($this->date_published);
                    MyFormat::isValidDate($date_published);
                }
                $year = MyFormat::GetYearByDate($date_published);
                if($year == date('Y') && $qty == $max_qty){// chỉ gửi mail cho văn bản trong năm hiện tại
                    $this->addError("type","Bạn không thể tạo quá $max_qty văn bản trong một ngày.");
                }
            }
        } catch (Exception $exc) {
            throw new Exception('CheckMaxCreateInDay error, xảy ra lỗi date khi tạo văn bản');
        }
    }
    
    /**
     * @Author: ANH DUNG Apr 03, 2015
     * @Todo: get số lượng văn bản dc tạo trong ngày
     * @Param: $date: 2015-04-25
     */
    public static function GetQtyCreateInDay($date) {
        $criteria = new CDbCriteria();
        $criteria->compare('t.created_date', $date, true);
        return self::model()->count($criteria);
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        return array(
            'rUidLogin' => array(self::BELONGS_TO, 'Users', 'uid_login'),
            'rTextFile' => array(self::HAS_MANY, 'GasTextFile', 'text_id',
                'order'=>'rTextFile.order_number ASC',
            ),
            'rCompany' => array(self::BELONGS_TO, 'Users', 'company_id'),// Công ty trực thuộc
            'rGasDebts' => array(
                self::HAS_MANY, 'GasDebts', 'relate_id',
                'on'    => 'type = ' . GasDebts::TYPE_GAS_TEXT . ' AND status !=' . DomainConst::DEFAULT_STATUS_INACTIVE,
            ),
//            'rGasTextPersons' => array(
//                self::HAS_MANY, 'GasTextPersonResponsibles', 'text_id',
//                'on'    => 'status !=' . DomainConst::DEFAULT_STATUS_INACTIVE,
//            ),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'code_no' => 'Mã Số',
            'uid_login' => 'Người Tạo',
            'type' => 'Loại Văn Bản',
            'number_text' => 'Số',
            'number_sign' => 'Số Ký Hiệu',
            'date_published' => 'Ngày Ban Hành',
            'date_expired' => 'Ngày Hết Hạn',
            'short_content' => 'Trích Yếu',
            'status' => 'Trạng Thái',
            'created_date' => 'Ngày Tạo',
            'date_from' => 'Ban Hành Từ Ngày',
            'date_to' => 'Ban Hành Đến Ngày',
            'stop_comment' => 'Khóa Comment',
            'company_id' => 'Công ty',
            'role_id' => 'Bộ phận áp dụng',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search($wait_approved = false)
    {
        $criteria=new CDbCriteria;
        $criteria->compare('t.company_id',$this->company_id);
        $criteria->compare('t.role_id',$this->role_id);
        $criteria->compare('t.code_no',$this->code_no);
        $criteria->compare('t.uid_login',$this->uid_login);
        $criteria->compare('t.type',$this->type);
        $criteria->compare('t.number_text',$this->number_text);
        $criteria->compare('t.number_sign',$this->number_sign);
        $criteria->compare('t.short_content',$this->short_content, true);
        $criteria->compare('t.status',$this->status);
        $criteria->compare('t.stop_comment',$this->stop_comment);
        $criteria->compare('t.company_id',$this->company_id);
        // Load còn hạn
        if(empty($this->is_view_all)){
//            $criteria->compare('t.stop_comment',0);// ko ro tai sao = 0
            $criteria->addCondition('t.date_expired >= "' . date('Y-m-d') . '" OR t.date_expired = "0000-00-00"');
        }
        if($wait_approved){
            $criteria->compare("t.status", self::STT_NEW);
        } else {
            $criteria->addCondition("t.status != ".self::STT_NEW);
        }
        
        if(!empty($this->date_from)){
            $date_from = MyFormat::dateDmyToYmdForAllIndexSearch($this->date_from);
            $criteria->addCondition("t.date_published>='$date_from'");
        }
        if(!empty($this->date_to)){
            $date_to = MyFormat::dateDmyToYmdForAllIndexSearch($this->date_to);
            $criteria->addCondition("t.date_published<='$date_to'");
        }
        $this->mapMoreCondition($criteria);
            
        $sort = new CSort();
        $sort->attributes = array(
            'code_no'=>'code_no',
            'uid_login'=>'uid_login',
            'type'=>'type',
            'number_text'=>'number_text',
            'status'=>'status',
            'number_sign'=>'number_sign',
            'created_date'=>'created_date',                
            'date_published'=>'date_published',                
            'stop_comment'=>'stop_comment',                
            'last_comment'=>'last_comment',                
        );    
//        $sort->defaultOrder = 't.last_comment desc, t.id desc';
        $cRole = MyFormat::getCurrentRoleId();
        $sort->defaultOrder = 't.date_published desc, t.number_text DESC';
        if($cRole == ROLE_ADMIN){
            $sort->defaultOrder = 't.id DESC';
        }

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
            'sort' => $sort,
            'pagination'=>array(
                'pageSize'=> 50,
            ),
        ));
    }
    
    /** Now0217 function use only for check company_id 
     * @Use_at:
     * 1/ GasText
     * 2/ GasMeetingMinutes
     */
    public function mapMoreCondition(&$criteria) {
        $cUid = MyFormat::getCurrentUid();
        $cRole = MyFormat::getCurrentRoleId();
        $mUser = Users::model()->findByPk($cUid);
        $aRoleViewAll   = [ROLE_ADMIN, ROLE_RECEPTION];
//        if(!in_array($cUid, $this->getUidBienDong()) && $mUser->storehouse_id == GasConst::COMPANY_BIEN_DONG){
//            $criteria->addCondition('t.company_id=' . GasConst::COMPANY_BIEN_DONG);
//        }elseif( !in_array($cRole, $aRoleViewAll)){
//            $criteria->addCondition('t.company_id<>' . GasConst::COMPANY_BIEN_DONG);
//        }
    }

    public function activate()
    {
        $this->status = 1;
        $this->update();
    }

    public function deactivate()
    {
        $this->status = 0;
        $this->update();
    }

    /**
     * @Author: ANH DUNG Aug 30, 2014
     */    
    protected function beforeSave() {
        if(strpos($this->date_published, '/')){
            $this->date_published = MyFormat::dateConverDmyToYmd($this->date_published);
            MyFormat::isValidDate($this->date_published);
        }
        if(strpos($this->date_expired, '/')){
            $this->date_expired = MyFormat::dateConverDmyToYmd($this->date_expired);
            MyFormat::isValidDate($this->date_expired);
        }
        $this->short_content = MyFormat::removeBadCharacters($this->short_content, array('RemoveScript'=>1));
        
        if($this->isNewRecord){
            $this->uid_login = Yii::app()->user->id;
            $this->code_no = MyFunctionCustom::getNextId('GasText', 'VB'.date('y'), LENGTH_TICKET,'code_no');
        }
        $this->handleSaveNewType();
//        else{
//            $this->last_update_by = Yii::app()->user->id;
//            $this->last_update_time = date('Y-m-d H:i:s');
//        }
        return parent::beforeSave();
    }    

    protected function beforeDelete() {
        GasTextFile::deleteByTextId($this->id);
        GasTextComment::deleteByTextId($this->id);
        return parent::beforeDelete();
    }
  
    public function getCompany(){
        $mUser = $this->rCompany;
        $res = "";
        if ($mUser) {
            $res .= $mUser->getFullName();
        }
        return $res;
    }
  
    public function getResponsiblePerson(){
        $res = "";
        $models = $this->rGasDebts;
        $aData = [];
        foreach ($models as $value) {
            $aData[$value->user_id]['first_name'] = $value->rUser->first_name;
            $aData[$value->user_id]['amount']     = isset($aData[$value->user_id]['amount']) ? $aData[$value->user_id]['amount']+$value->amount : $value->amount;
        }
        foreach ($aData as $value) {
            $res .= $value['first_name'] . " - " . ActiveRecord::formatCurrency($value['amount']) . "<br>";
        }
        return $res;
    }
    
    /** ANH DUNG New Now0217 => May 18, 2014
     * to do: check agent can update gas file scan
     */
    public function canUpdate()
    {
        $cRole = MyFormat::getCurrentRoleId();
        $cUid = MyFormat::getCurrentUid();
        $aUidAllow = [GasConst::UID_ADMIN, GasLeave::DUNG_NTT, GasConst::UID_HANH_NT];
        if(in_array($cUid, $aUidAllow)){
            return true;
        }
        if($cRole != ROLE_ADMIN && $cUid != $this->uid_login){
            return false; // cho phép admin sửa hết, còn những user khác thì ai tạo thì dc sửa của người đó
        }
        $dayAllow = date('Y-m-d');
        $dayAllow = MyFormat::modifyDays($dayAllow, GasText::getDayAllowUpdate(), '-');
        return MyFormat::compareTwoDate($this->created_date, $dayAllow);
    }
    
    public function getArrayRole() {
        $model = new UsersProfile();
        return $model->getArrWorkRoom();
    }
    
    /** @Author: ANH DUNG Aug 23, 2018
     **/
    public function getRole() {
        $model = new UsersProfile();
        $aRoom = $model->getArrWorkRoom();
        return isset($aRoom[$this->role_id]) ? $aRoom[$this->role_id] : '';
    }
    
    /** @Author: DuongNV Oct 18, 18
     *  @Todo: handle Save TextPersonResponsibles
     **/
    public function handleSaveTPR() {
        $aInsert = [];
        $aMonth  = [];
        $created_by = MyFormat::getCurrentUid();
        $created_date = CommonProcess::getCurrentDateTime();
        if(isset($_POST['GasTextPersonResponsibles']['user_id'])){
            $post = $_POST['GasTextPersonResponsibles'];
            foreach ($post['user_id'] as $key => $user_id){
                if(empty($user_id)) continue;
                $amount = str_replace(',', '', $post['amount'][$key]);
                $aInsert[] = '('
                        . $this->id . ','
                        . $user_id . ','
                        . $amount . ','
                        . $created_by . ','
                        . '"' . $created_date . '"'
                            . ')';
            }
            unset($post['month'][0]);
            $aMonth = array_values($post['month']);
        }
        $this->deleteBeforeSaveTPR();
        $sInsert = implode(',', $aInsert);
        $tblName = GasTextPersonResponsibles::model()->tableName();
        $sql     = 'INSERT INTO ' . $tblName . ' (text_id, user_id, amount, created_by, created_date) VALUES ' . $sInsert;
        $res     = Yii::app()->db->createCommand($sql)->execute();
        
        $this->createGasDebts($aMonth);
        return $res;
    }
    
    /**
     * Handle save gas debts (Removed GasTextPersonResponsibles)
     */
    public function handleSaveGasDebts() {
        $aMonth = [];
        if (isset($_POST['GasTextPersonResponsibles']['user_id'])) {
            $post = $_POST['GasTextPersonResponsibles'];
            $aMonth = array_values($post['month']);
        }
        $this->createGasDebts($aMonth);
    }
    
    /** @Author: DuongNV Oct 18, 18
     *  @Todo: delete all by text_id before Save TextPersonResponsibles
     **/
    public function deleteBeforeSaveTPR() {
        $tblName = GasTextPersonResponsibles::model()->tableName();
        $sql     = 'DELETE FROM ' . $tblName . ' WHERE text_id = ' . $this->id;
        $res     = Yii::app()->db->createCommand($sql)->execute();
        return $res;
    }
    
    /**
     * Create gas debt relate
     * @param Array $arrMonthCount Array month count get from $_POST
     */
    public function createGasDebts($arrMonthCount) {
        // Remove old
        if (isset($this->rGasDebts)) {
            foreach ($this->rGasDebts as $debt) {
                $debt->delete();
            }
        }
//        if (isset($this->rGasTextPersons)) {
//            $idx = 0;
//            foreach ($this->rGasTextPersons as $person) {
//                $monthCount = 0;
//                if (isset($arrMonthCount[$idx])) { 
//                    $monthCount = $arrMonthCount[$idx++];
//                }
//                $mGasDebts = new GasDebts();
//                $mGasDebts->createFromGasText($person, $monthCount);
//            }
//        }
        if (isset($_POST['GasTextPersonResponsibles']['user_id'])) {
            $post = $_POST['GasTextPersonResponsibles'];
            unset($post['month'][0]);
            $aMonth = array_values($post['month']);
            $idx = 0; 
            foreach ($post['user_id'] as $key => $user_id) {
                if (empty($user_id)) {
                    continue;
                }
                $amount     = MyFormat::removeComma($post['amount'][$key]);
                $mGasDebts  = new GasDebts();
                $mGasDebts->createFromGasText($this, $aMonth[$idx++], $amount, $user_id);
            }
        }
    }
    
    
    /** @Author: DuongNV Nov 23, 2018
     *  @Todo:
     **/
    public function getArrayTypeNeedApprove() {
        return [];// Mar0619 - chua check run dc
        return [
            GasText::TYPE_DECISION_PHAT,
            GasText::TYPE_DECISION_BO_NHIEM,
            GasText::TYPE_DECISION_CHANGE_POSITION,
        ];
    }
    
    /** @Author: DuongNV Nov 23, 2018
     *  @Todo:
     **/
    public function getArrayIdTemplate() {
        return [
            GasText::TYPE_DECISION_PHAT            => 23,
            GasText::TYPE_DECISION_BO_NHIEM        => 24,
            GasText::TYPE_DECISION_CHANGE_POSITION => 25,
        ];
    }
    
    /** @Author: DuongNV Nov 23,2018
     *  @Todo: check quyen duyet
     **/
    public function canApprove() {
        return true;
    }
    
    public function getArrayApprover(){
        return [
            2
        ];
    }
    
    public function getArrayStatus(){
        return [
            self::STT_APPROVED    => "Duyệt",
            self::STT_NO_APPROVED => "Không Duyệt",
        ];
    }
    
    public function handleSaveNewType(){
        if(in_array($this->type, $this->getArrayTypeNeedApprove())){
            $this->status = $this->isNewRecord ? self::STT_NEW : $this->status;
        } else {
            $this->status = $this->isNewRecord ? self::STT_APPROVED : $this->status;
        }
    }
    
    /** @Author: DuongNV Nov 23, 2018
     *  @Todo: Xử lý convert input to json trước khi create, update
     **/
    public function handleJsonSave() {
        $aNotRequireFile = $this->getArrayTypeNeedApprove();
        if(in_array($this->type, $aNotRequireFile)){
            $idTemplate     = $this->getArrayIdTemplate()[$this->type];
            $mTemplate      = EmailTemplates::model()->findByPk($idTemplate);
            $result         = isset($_POST['GasTextJSON']) ? $_POST['GasTextJSON'] : [];
            $this->json     = json_encode($result);
            $this->template = $mTemplate->email_body;
        }
    }
    
    /** @Author: DuongNV Nov 23, 2018
     *  @Todo: Xử lý convert dạng {var} sang input
     **/
    public function handleTemplate(&$string, $onlyView = false) {
        $aDbParams = json_decode($this->json, true);
        preg_match_all('/{(.*?)}/', $string, $aParams);
        foreach ($aParams[1] as $item) {
            $valueInput = !isset($aDbParams[$item]) ? '' : $aDbParams[$item];
            $inputHtml =  '<input type="text" '
                        .       ' name="GasTextJSON['.$item.']" '
                        .       ' value="'.$valueInput.'" >';
            $result = $onlyView ? $valueInput : $inputHtml;
            $string = str_replace('{'.$item.'}', $result, $string);
        }
    }
}
