<?php

/**
 * This is the model class for table "{{_gas_order_promotion_detail}}".
 *
 * The followings are the available columns in table '{{_gas_order_promotion_detail}}':
 * @property string $id
 * @property string $order_promotion_id
 * @property integer $materials_type_id
 * @property integer $materials_id
 * @property string $qty
 * @property string $qty_real
 */
class GasOrderPromotionDetail extends CActiveRecord
{
    public $unit;
    public static $DateMiss = "13/10/2016";
    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return GasOrderPromotionDetail the static model class
     */
    public static function model($className=__CLASS__)
    {
            return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
            return '{{_gas_order_promotion_detail}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        return array(
            array('order_promotion_id, materials_type_id, materials_id, qty', 'required'),
            array('materials_type_id, materials_id', 'numerical', 'integerOnly'=>true),
            array('order_promotion_id', 'length', 'max'=>11),
            array('qty, qty_real', 'length', 'max'=>16),
            array('note, id, order_promotion_id, materials_type_id, materials_id, qty, qty_real', 'safe'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        return array(
            'rMaterials' => array(self::BELONGS_TO, 'GasMaterials', 'materials_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'order_promotion_id' => 'Order Promotion',
            'materials_type_id' => 'Materials Type',
            'materials_id' => 'Materials',
            'qty' => 'Qty',
            'qty_real' => 'Qty Real',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        $criteria=new CDbCriteria;
        $criteria->compare('t.id',$this->id,true);
        $criteria->compare('t.order_promotion_id',$this->order_promotion_id,true);
        $criteria->compare('t.materials_type_id',$this->materials_type_id);
        $criteria->compare('t.materials_id',$this->materials_id);
        $criteria->compare('t.qty',$this->qty,true);
        $criteria->compare('t.qty_real',$this->qty_real,true);

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
            'pagination'=>array(
                'pageSize'=> Yii::app()->user->getState('pageSize',Yii::app()->params['defaultPageSize']),
            ),
        ));
    }

    public function defaultScope()
    {
        return array();
    }
    
    /**
     * @Author: ANH DUNG Now 06, 2015
     */
    public function getMaterialName() {
        $mMaterial = $this->rMaterials;
        if($mMaterial){
            $this->unit = $mMaterial->unit;
            return $mMaterial->name;
        }
        return '';
    }
    
    public function getNote() {
        return $this->note;
    }

    /**
     * @Author: ANH DUNG Feb 24, 2016
     * @Todo: update status and user_id_delivery belong with root model
     */
    public static function UpdateStatusAndUserDelivery($mOrderPromotion) {
        $mOrderPromotion = GasOrderPromotion::model()->findByPk($mOrderPromotion->id);
        $tmp = explode(" ", $mOrderPromotion->created_date);
        $criteria = new CDbCriteria();
        $criteria->compare('order_promotion_id', $mOrderPromotion->id);
        $aUpdate = array(
            'status'            => $mOrderPromotion->status, 
            'user_id_delivery'  => $mOrderPromotion->user_id_delivery,
            'agent_id'          => $mOrderPromotion->agent_id,
            'province_id'       => $mOrderPromotion->province_id,
            'created_date'      => $tmp[0]
        );
        self::model()->updateAll($aUpdate, $criteria);
    }
    
}