<?php

/** table công nợ KH theo năm
 * This is the model class for table "{{_gas_closing_balance_customer}}".
 *
 * The followings are the available columns in table '{{_gas_closing_balance_customer}}':
 * @property string $id
 * @property integer $year 
 * @property string $customer_id
 * @property integer $type_customer
 * @property string $receivables_customer
 * @property string $collection_customer
 * @property string $balance
 */
class GasClosingBalanceCustomer extends CActiveRecord
{
    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return GasClosingBalanceCustomer the static model class
     */
    public static function model($className=__CLASS__)
    {
            return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
            return '{{_gas_closing_balance_customer}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        return array(
            array(' customer_id, receivables_customer, collection_customer, balance', 'length', 'max'=>11),
            array('id, year,  customer_id, type_customer, receivables_customer, collection_customer, balance', 'safe'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        return array(
            'rCustomer' => array(self::BELONGS_TO, 'Users', 'customer_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
            return array(
                    'id' => 'ID',
                    'year' => 'Year',                    
                    'customer_id' => 'Customer',
                    'type_customer' => 'Type Customer',
                    'receivables_customer' => 'Receivables Customer',
                    'collection_customer' => 'Collection Customer',
                    'balance' => 'Balance',
            );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
            // Warning: Please modify the following code to remove attributes that
            // should not be searched.

            $criteria=new CDbCriteria;

		$criteria->compare('t.id',$this->id,true);
		$criteria->compare('t.year',$this->year);		
		$criteria->compare('t.customer_id',$this->customer_id,true);
		$criteria->compare('t.type_customer',$this->type_customer);
		$criteria->compare('t.receivables_customer',$this->receivables_customer,true);
		$criteria->compare('t.collection_customer',$this->collection_customer,true);
		$criteria->compare('t.balance',$this->balance,true);

            return new CActiveDataProvider($this, array(
                    'criteria'=>$criteria,
        'pagination'=>array(
            'pageSize'=> Yii::app()->user->getState('pageSize',Yii::app()->params['defaultPageSize']),
        ),
            ));
    }

    public function defaultScope()
    {
            return array(
                    //'condition'=>'',
            );
    }
    
    /**
     * @Author: ANH DUNG Jun 02, 2014
     * @Todo: cập nhật tồn đầu kỳ trong danh sách KH
     * /admin/gascustomer/update_customer_store_card
     * @Param: $year, $customer_id, $type_customer, $receivables_customer
     */
    public static function updateTonDauKy($year, $customer_id, $type_customer, $receivables_customer){
        return 0; // Jun 18, 2014 - không quản lý công nợ KH nữa, để cho bên phần mềm kế toán làm việc này
        $model = self::loadModel($year, $customer_id);
        if(is_null($model)){
            self::addNewRecordReceivables($year, $customer_id, $type_customer, $receivables_customer);
        }else{
            $model->receivables_customer = $receivables_customer;
            $model->balance = $model->receivables_customer;
            $model->type_customer = $type_customer;
            $model->update(array('receivables_customer', 'balance', 'type_customer'));
        }
    }
    
    
    public static function loadModel($year, $customer_id){
        $criteria = new CDbCriteria();
        $criteria->compare("t.year", $year);
        $criteria->compare("t.customer_id", $customer_id);
        return self::model()->find($criteria);        
    }

    /**
     * @Author: ANH DUNG Jun 01, 2014
     * @Todo: add new record for new year customer if new receivables_customer is add
     */
    public static function addNewRecordReceivables($year, $customer_id, $type_customer, $receivables_customer){
        return 0; // Jun 18, 2014 - không quản lý công nợ KH nữa, để cho bên phần mềm kế toán làm việc này
        if(empty($year) ||  $year<1) return;
        $model = new GasClosingBalanceCustomer();
        $model->year = $year;
        $model->customer_id = $customer_id;
        $model->type_customer = $type_customer;
        $model->sale_id = $model->rCustomer?$model->rCustomer->sale_id:0;
        $model->agent_id = $model->rCustomer?$model->rCustomer->area_code_id:0;
        if($model->rCustomer){
            $model->customer_parent_id = $customer_id;
            if(!empty($model->rCustomer->parent_id) && $model->rCustomer->parent_id>0){
                $model->customer_parent_id = $model->rCustomer->parent_id;
            }
        }
        $model->receivables_customer = $receivables_customer;
        $model->collection_customer = 0;
        $model->balance = $model->receivables_customer-$model->collection_customer;
        $model->save();
    }
    
    /**
     * @Author: ANH DUNG Jun 01, 2014
     * @Todo: add new record for new year customer if new collection is add
     */
    public static function addNewRecordCollection($year, $customer_id, $type_customer, $collection_customer){
        return 0; // Jun 18, 2014 - không quản lý công nợ KH nữa, để cho bên phần mềm kế toán làm việc này
        if(empty($year) ||  $year<1) return;
        $model = new GasClosingBalanceCustomer();
        $model->year = $year;
        $model->customer_id = $customer_id;
        $model->type_customer = $type_customer;
        $model->sale_id = $model->rCustomer?$model->rCustomer->sale_id:0;
        $model->agent_id = $model->rCustomer?$model->rCustomer->area_code_id:0;
        if($model->rCustomer){
            $model->customer_parent_id = $customer_id;
            if(!empty($model->rCustomer->parent_id) && $model->rCustomer->parent_id>0){
                $model->customer_parent_id = $model->rCustomer->parent_id;
            }
        }        
        $model->receivables_customer = 0;
        $model->collection_customer = $collection_customer;
        $model->balance = $model->receivables_customer-$model->collection_customer;
        $model->save();
    }
    
    
    /**
     * @Author: ANH DUNG Jun 01, 2014
     * @Todo: cộng phải thu của KH khi phát sinh mới hoặc cập nhật thẻ kho 
     * cũng dùng chung cho nhập cộng nợ tồn đầu của KH - từ khi chạy phần tính công nợ
     * @Param: $year
     * @Param: $customer_id mã KH
     * @Param: $type_customer loai KH
     * @Param: $receivables_customer Phải thu của KH
     * dùng ở khi phát sinh mới hoặc cập nhật thẻ kho
     */
    public static function addReceivablesCustomer($year, $customer_id, $type_customer, $receivables_customer){
        return 0; // Jun 18, 2014 - không quản lý công nợ KH nữa, để cho bên phần mềm kế toán làm việc này
        $model = self::loadModel($year, $customer_id);
        if(is_null($model)){
            self::addNewRecordReceivables($year, $customer_id, $type_customer, $receivables_customer);
        }else{
            $model->receivables_customer += $receivables_customer;
            $model->balance = $model->receivables_customer-$model->collection_customer;
            $model->update(array('receivables_customer', 'balance'));
        }
    }
    
    /**
     * @Author: ANH DUNG Jun 01, 2014
     * @Todo: trừ phải thu và đã thu của KH khi xóa hoặc cập nhật thẻ kho 
     * cả Gas dư: với những gas dư chỉ cân không có bán hàng, thì khi cập nhật giá cho gas dư đó thì
     * sẽ phải trừ đi receivables_customer
     * @Param: $year
     * @Param: $customer_id mã KH
     * @Param: $receivables_customer Phải thu của KH
     * dùng ở khi xóa hoặc cập nhật thẻ kho 
     */
    public static function cutReceivablesCustomer($year, $customer_id, $receivables_customer){
        return 0; // Jun 18, 2014 - không quản lý công nợ KH nữa, để cho bên phần mềm kế toán làm việc này
        $model = self::loadModel($year, $customer_id);
        if($model){
            $model->receivables_customer -= $receivables_customer;
            $model->balance = $model->receivables_customer-$model->collection_customer;
            $model->update(array('receivables_customer', 'balance'));
        }
    }
    
    /**
     * @Author: ANH DUNG Jun 01, 2014
     * @Todo: cộng đã thu của KH khi phát sinh mới hoặc cập nhật sổ quỹ
     * @Param: $year
     * @Param: $customer_id mã KH
     * @Param: $type_customer loai KH
     * @Param: $collection_customer đã thu của KH
     * dùng ở khi sinh mới hoặc cập nhật sổ quỹ
     */
    public static function addCollectionCustomer($year, $customer_id, $type_customer, $collection_customer){
        return 0; // Jun 18, 2014 - không quản lý công nợ KH nữa, để cho bên phần mềm kế toán làm việc này
        $model = self::loadModel($year, $customer_id);
        if(is_null($model)){
            self::addNewRecordCollection($year, $customer_id, $type_customer, $collection_customer);
        }else{
            $model->collection_customer += $collection_customer;
            $model->balance = $model->receivables_customer-$model->collection_customer;
            $model->update(array('collection_customer', 'balance'));
        }
    }
    
    /**
     * @Author: ANH DUNG Jun 01, 2014
     * @Todo: trừ đã thu của KH khi phát sinh mới hoặc cập nhật sổ quỹ
     * @Param: $year
     * @Param: $customer_id mã KH
     * @Param: $collection_customer Phải thu của KH
     * dùng ở khi xóa hoặc cập nhật sổ quỹ
     */
    public static function cutCollectionCustomer($year, $customer_id, $collection_customer){
        return 0; // Jun 18, 2014 - không quản lý công nợ KH nữa, để cho bên phần mềm kế toán làm việc này
        $model = self::loadModel($year, $customer_id);
        if($model){
            $model->collection_customer -= $collection_customer;            
            $model->balance = $model->receivables_customer-$model->collection_customer;
            $model->update(array('collection_customer', 'balance'));
        }
    }    
    
}