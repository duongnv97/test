<?php
/** @Author: ANH DUNG Jul 12, 2018
 *  @Todo: xử lý dữ liệu tái sử dụng hàm cũ của hệ thống 
 *  Sau đo format lại cho đúng định dạng để tính công 
 *  query hết 1 lần dữ liệu để tính công, sản lượng của NV
 **/
class HrData
{
    const PRICE_10K = 10000;
    const PRICE_20K = 20000;
    const PRICE_30K = 30000;
    const PRICE_40K = 40000;
    const PRICE_50K = 50000;
    const PRICE_60K = 60000;
    
    const PVKH_START = 6; // thời gian để tính đơn hàng hoàn thành của PVKH (từ 6 - 18h) ($this->getCompleteOrderBoMoi)
    const PVKH_END   = 18;
    
    const PRICE_B12_DEFAULT = 10000;// đơn giá B12 mặc định là 10k
    
    /** @Author: ANH DUNG Jul 12, 2018
     *  @Todo: get số điểm làm việc và BQV của PTTT, Point/App/Bqv 
     *  $mHrSalaryReports->dateFromDmy:  d-m-Y
     *  @note1: cachePtttWorkingPoint số điểm làm việc: format:  $aRes['OUTPUT_POINT'][$item->user_id][$item->created_date] = 3
     *  @note2: cachePtttGasBack BQV format:  $aRes['OUTPUT'][$item->user_id][$item->created_date] = 1
     **/
    public function getPttt(&$mHrSalaryReports) {
//        $mPtttDailyGoback =new GasPtttDailyGoback();
//        $mPtttDailyGoback->unsetAttributes();  // clear any default values
//        $mPtttDailyGoback->date_from   = $mHrSalaryReports->dateFromDmy;// start_date format Y-m-d
//        $mPtttDailyGoback->date_to     = $mHrSalaryReports->dateToDmy;
//        $aData = $mPtttDailyGoback->getCcsDaily();
//        $mHrSalaryReports->cachePtttWorkingPoint    = isset($aData['OUTPUT_POINT']) ? $aData['OUTPUT_POINT'] : [];
//        $mHrSalaryReports->cachePtttGasBack         = isset($aData['OUTPUT']) ? $aData['OUTPUT'] : [];
        
        $mTargetDaily=new TargetDaily();
        $mTargetDaily->unsetAttributes();
        $mTargetDaily->date_from        = $mHrSalaryReports->dateFromDmy;
        $mTargetDaily->date_to          = $mHrSalaryReports->dateToDmy;
        $mHrSalaryReports->cachePttt    = $mTargetDaily->getReportDailyCcs();
    }
    
    /** @Author: ANH DUNG Jul 19, 2018
     *  @Todo: tính sản lượng nv tổng đài - CallCenter
     *  1. Tổng số lượng cuộc gọi
     *  2. Tổng số lượng các đơn hàng
     *  3. Tổng số lượng đơn hàng hoàn thành
     *  4. Tính Sản lượng bình cam -- getOutputCam()
     *  @note1: cacheCallCenterCallInHgd số cuộc gọi vào : format:  $aRes['OUTPUT_SUM'][$item->user_id] = 3
     *  @note2: cacheCallCenterSellHgd data all đơn hàng:  $aRes['OUTPUT_SUM_SALE'][$item->user_id][$item->status] = 1
     * $item->status = 0 is sum all order Sell
     * $item->status = 2 is sum all Sell Complete
     **/
    public function getOutputCallCenter(&$mHrSalaryReports) {
        $mCall = new Call();
        $mCall->date_from   = $mHrSalaryReports->dateFromDmy;// date format d-m-Y
        $mCall->date_to     = $mHrSalaryReports->dateToDmy;
        // 1. get cuộc gọi + đơn hàng hộ GĐ 
        $sta3 = new Sta3();
        $mCall->type = Call::QUEUE_HGD;
        $aData = $sta3->queryCallReportByDate($mCall);
        $mHrSalaryReports->cacheCallCenterCallInHgd         = isset($aData['OUTPUT_SUM']) ? $aData['OUTPUT_SUM'] : [];
        $mHrSalaryReports->cacheCallCenterSellHgd           = isset($aData['OUTPUT_SUM_SALE']) ? $aData['OUTPUT_SUM_SALE'] : [];
        $mHrSalaryReports->cacheCallCenterCallHgdOneDay     = isset($aData['OUTPUT']) ? $aData['OUTPUT'] : [];
        // get cuộc gọi + đơn hàng bò mối 
        $sta3 = new Sta3();
        $mCall->type = Call::QUEUE_BO_MOI;
        $aData = $sta3->queryCallReportByDate($mCall);
        $mHrSalaryReports->cacheCallCenterCallInBoMoi           = isset($aData['OUTPUT_SUM']) ? $aData['OUTPUT_SUM'] : [];
        $mHrSalaryReports->cacheCallCenterOrderBoMoi            = isset($aData['OUTPUT_SUM_SALE']) ? $aData['OUTPUT_SUM_SALE'] : [];
        $mHrSalaryReports->cacheCallCenterCallBoMoiOneDay       = isset($aData['OUTPUT']) ? $aData['OUTPUT'] : [];
        $aData = null;
    }
    
    /** @Author: ANH DUNG Jan 07, 2019
     *  @Todo: 4. Tính Sản lượng bình cam --
     * @return:
     * $aData[$uid_login]['NEW']      Số bình cam bán mới trong tháng
     * $aData[$uid_login]['MAINTAIN'] Số bình cam giữ được
     * $aData[$uid_login]['LOSS']  Số bình cam bị mất
     * $aData[$uid_login]['CALL']  Số cuộc gọi cam
     **/
    public function getOutputCam(&$mHrSalaryReports) {
        $mSellReportQuantity = new SellReportQuantity();
        $mSellReportQuantity->unsetAttributes();  // clear any default values
        $mSellReportQuantity->date_from   = $mHrSalaryReports->dateFromDmy;
        $mSellReportQuantity->date_to     = $mHrSalaryReports->dateToDmy;
        $rData = $mSellReportQuantity->getReportQuantityV2();
        $mHrSalaryReports->cacheQtyCam    = $rData['data'];
    }
    
    /** @Author: ANH DUNG Jul 19, 2018
     *  @Todo: tính sản lượng nv Telesale
     *  1/ Tổng số lượng cuộc gọi ra CallOut
     *  2/ Tổng số lượng KH nhập mã code Telesale 
     *  3/ Tổng số lượng BQV (đơn hàng hoàn thành)
     *  4/ Doanh thu trên BQV
     *  @note1: cacheTelesaleCallOut số lượng cuộc gọi ra CallOut: format: $aRes['OUT_FOLLOW_CHART'][$item->user_id]['value'] = 3 - function getGasFollow
     *  @note2: cacheTelesaleCode Tổng số lượng KH nhập mã code Telesale: $aRes['OUT_TRACKING'][$item->user_id] = 30 - function getReportGasReferralTracking
     *  @note3: cacheTelesaleSell Tổng số lượng BQV (đơn hàng hoàn thành): $aRes['OUT_SELL_CHART'][$item->user_id]['value'] = 30 - function getGasSell
     *  @note4: cacheTelesaleRevenue Doanh thu trên BQV: $aRes['SUM_REVENUE'][$item->user_id] = 333330 - function getGasSell
     **/
    public function getOutputTelesale(&$mHrSalaryReports) {
        $mTelesale = new Telesale();
        $mTelesale->date_from   = $mHrSalaryReports->dateFromDmy;// date format d-m-Y
        $mTelesale->date_to     = $mHrSalaryReports->dateToDmy;
        $aData = $mTelesale->getGasBack(true);
        $mHrSalaryReports->cacheTelesaleCallOut         = isset($aData['OUT_FOLLOW_CHART']) ? $aData['OUT_FOLLOW_CHART'] : [];
        $mHrSalaryReports->cacheTelesaleCallOutByDate   = isset($aData['OUT_FOLLOW']) ? $aData['OUT_FOLLOW'] : [];
        $mHrSalaryReports->cacheTelesaleCode            = isset($aData['OUT_TRACKING']) ? $aData['OUT_TRACKING'] : [];
        $mHrSalaryReports->cacheTelesaleSell            = isset($aData['OUT_SELL_CHART']) ? $aData['OUT_SELL_CHART'] : [];
        $mHrSalaryReports->cacheTelesaleRevenue         = isset($aData['SUM_REVENUE']) ? $aData['SUM_REVENUE'] : [];
        $mHrSalaryReports->cacheTelesaleSellHaveCallOut = isset($aData['OUT_SELL_FOLLOW']) ? $aData['OUT_SELL_FOLLOW'] : [];
        $mHrSalaryReports->cacheTelesaleCodeHaveCallOut = isset($aData['TELESALE_CODE_CALLOUT']) ? $aData['TELESALE_CODE_CALLOUT'] : [];
        $mHrSalaryReports->cacheTelesaleCodeNotCallOut  = $this->calcTelesaleCodeNotCallOut($mHrSalaryReports);
        
        $mHrSalaryReports->cacheTelesaleRevenueHaveCallOut  = isset($aData['SUM_REVENUE_HAVE_CALLOUT']) ? $aData['SUM_REVENUE_HAVE_CALLOUT'] : [];
        $mHrSalaryReports->cacheTelesaleRevenueNotCallOut   = $this->calcTelesaleRevenueNotCallOut($mHrSalaryReports);
        
        $aData = null;
    }
    
    /** @Author: ANH DUNG Oct 01, 2018
     *  @Todo: tính Telesale code  không có Call Out
     **/
    public function calcTelesaleCodeNotCallOut($mHrSalaryReports) {
        $aRes = [];
        foreach ($mHrSalaryReports->cacheTelesaleCode as $user_id => $totalCode){
            $codeCallOut    = isset($mHrSalaryReports->cacheTelesaleCodeHaveCallOut[$user_id]) ? $mHrSalaryReports->cacheTelesaleCodeHaveCallOut[$user_id] : 0;
            $aRes[$user_id] = $totalCode - $codeCallOut;
        }
        return $aRes;
    }
    /** @Author: ANH DUNG Oct 01, 2018
     *  @Todo: tính Telesale doanh thu BQV không có Call Out
     **/
    public function calcTelesaleRevenueNotCallOut($mHrSalaryReports) {
        $aRes = [];
        foreach ($mHrSalaryReports->cacheTelesaleRevenue as $user_id => $revenueTotal){
            $revenueCallOut    = isset($mHrSalaryReports->cacheTelesaleRevenueHaveCallOut[$user_id]) ? $mHrSalaryReports->cacheTelesaleRevenueHaveCallOut[$user_id] : 0;
            $aRes[$user_id] = $revenueTotal - $revenueCallOut;
        }
        return $aRes;
    }
    
    /** @Author: ANH DUNG Jul 12, 2018
     *  @Todo: get công b12, b45, van, dây, bếp... của NV
     *  @chú_ý: Day10k, Day20k, Bep20k, Bep40k, Bep60k
     * $OUTPUT_GAS[$employee_maintain_id][$materials_type_id]
     *  @note1: cachePvkhQty4kg         SL bình gas 4kg
     *  @note1: cachePvkhQty6kg         SL bình gas 6kg
     *  @note1: cachePvkhQty12kg        SL bình gas 12kg
     *  @note1: cachePvkhQty45kg        SL bình gas 45kg
     *  @note1: cachePvkhQty50kg        SL bình gas 50kg
     *  @note1: cachePvkhQtyVan         SL Van 
     *  @note1: cachePvkhQtyDay10k     SL dây 10k
     *  @note1: cachePvkhQtyDay20k     SL dây 20k
     *  @note1: cachePvkhQtyBep20k     SL bếp 20k
     *  @note1: cachePvkhQtyBep40k     SL bếp 40k
     *  @note1: cachePvkhQtyBep60k     SL bếp 60k
     *  @note1: cachePvkhQtyUpholdHgd           SL bảo trì hgd
     *  @note1: cachePvkhQtyBoBinh              SL bộ bình
     *  @note1: cachePvkhErrorConfirm10m        SL lỗi xác nhận 10 phút
     *  @note1: cachePvkhErrorConfirm20m        SL lỗi xác nhận 20 phút 
     *  @note1: cachePvkhErrorBank16h           SL lỗi nộp ngân hàng 16h
     *  @note1: cachePvkhErrorFixTicket         SL lỗi sửa ticket 
     *  @note1: cachePvkhErrorCompleteHgd60     SL lỗi hoàn thành đơn HGĐ quá 60'
     *  @note1: cachePvkhErrorCompleteBoMoi180     SL lỗi hoàn thành đơn bò mối quá 180'
     * 
     *  @note1: cachePvkhGroupByPrice    param lưu group các vật tư cùng đơn giá Day10k, Day20k, Bep20k, Bep40k, Bep60k
     * 
     **/
    public function getOutputEmployeeMaintain(&$mHrSalaryReports) {
        $mStoreCard = new GasStoreCard();
        $mStoreCard->date_from  = $mHrSalaryReports->dateFromDmy;
        $mStoreCard->date_to    = $mHrSalaryReports->dateToDmy;
        $mStoreCard->province_id_agent = [];
        $mAppCache              = new AppCache();

        $report = new Sta2();
        $aData  = $report->employeeMaintain($mStoreCard);
        // do định dạng $OUTPUT_GAS[$employee_maintain_id][$materials_type_id], nên sẽ không thể đưa vào biến của từng loại đc, mà sẽ đưa chung rồi get ra ở từng hàm
//        $mHrSalaryReports->cachePvkhQty4kg = isset($aData['OUTPUT_GAS'][MATERIAL_TYPE_BINH_4]) ? $aData['OUTPUT_GAS'][MATERIAL_TYPE_BINH_4] : [];
//        $mHrSalaryReports->cachePvkhQty6kg = isset($aData['OUTPUT_GAS'][MATERIAL_TYPE_BINH_6]) ? $aData['OUTPUT_GAS'][MATERIAL_TYPE_BINH_6] : [];
//        $mHrSalaryReports->cachePvkhQty12kg = isset($aData['OUTPUT_GAS'][MATERIAL_TYPE_BINH_12]) ? $aData['OUTPUT_GAS'][MATERIAL_TYPE_BINH_12] : [];
//        $mHrSalaryReports->cachePvkhQty45kg = isset($aData['OUTPUT_GAS'][MATERIAL_TYPE_BINHBO_45]) ? $aData['OUTPUT_GAS'][MATERIAL_TYPE_BINHBO_45] : [];
//        $mHrSalaryReports->cachePvkhQty50kg = isset($aData['OUTPUT_GAS'][MATERIAL_TYPE_BINHBO_50]) ? $aData['OUTPUT_GAS'][MATERIAL_TYPE_BINHBO_50] : [];
        // xử lý tính Day10k, Day20k, Bep20k, Bep40k, Bep60k ...
        $aModelMaterial     = $mAppCache->getMasterModel('GasMaterials', AppCache::ARR_MODEL_MATERIAL );
        $aDataCache         = UsersPriceHgd::getPriceHgd($mHrSalaryReports->start_date);
        $OUTPUT             = isset($aData['OUTPUT']) ? $aData['OUTPUT'] : [];// sản lượng các loại gas + van dây bếp
        $EMPLOYEE_ID        = isset($aData['EMPLOYEE_ID']) ? $aData['EMPLOYEE_ID'] : [];
        $MATERIALS_ID       = isset($aData['MATERIALS_ID']) ? $aData['MATERIALS_ID'] : [];
        $OUTPUT_BOBINH      = isset($aData['OUTPUT_BOBINH']) ? $aData['OUTPUT_BOBINH'] : [];
        $aSumByPrice        = [];
        
        foreach($EMPLOYEE_ID as $employee_maintain_id => $agent_id):
            foreach ($MATERIALS_ID as $materials_id):// vật tư bếp val dây
                $materials_type_id  = isset($aModelMaterial[$materials_id]) ? $aModelMaterial[$materials_id]['materials_type_id'] : 0;
                $qty                = isset($OUTPUT[$employee_maintain_id][$materials_id]) ? $OUTPUT[$employee_maintain_id][$materials_id] : '';
                $qtyBoBinh          = isset($OUTPUT_BOBINH[$employee_maintain_id][$materials_id]) ? $OUTPUT_BOBINH[$employee_maintain_id][$materials_id] : 0;
                
                if($materials_type_id != GasMaterialsType::MATERIAL_TYPE_BEP){
                }else{// Jul2617 bếp tính riêng công, không cộng vào bộ bình nên không trừ
                    $qty    = $qty + $qtyBoBinh;
                    $aData['OUTPUT'][$employee_maintain_id][$materials_id] = $qty;// Jul2318, update để tính lương
                }
                $mPriceHgd              = new UsersPriceHgd();
                $mPriceHgd->r_qty       = $qty;
                $mPriceHgd->agent_id    = $agent_id;
                $mPriceHgd->r_materials_type_id = $materials_type_id;
                $mPriceHgd->r_materials_id      = $materials_id;
                $mPriceHgd->aDataCache          = $aDataCache;
                $amount                         = $mPriceHgd->calcPayEmployee();
                // xử lý tính Day10k, Day20k, Bep20k, Bep40k, Bep60k ...
                if(!isset($aSumByPrice[$employee_maintain_id][$materials_type_id][$mPriceHgd->r_price])){
                    $aSumByPrice[$employee_maintain_id][$materials_type_id][$mPriceHgd->r_price] = $qty;
                }else{
                    $aSumByPrice[$employee_maintain_id][$materials_type_id][$mPriceHgd->r_price] += $qty;
                }
            endforeach;// end foreach ($MATERIALS_ID
        endforeach;// end foreach($EMPLOYEE_ID
        
        $mHrSalaryReports->cachePvkhOutput          = $aData;
        $mHrSalaryReports->cachePvkhGroupByPrice    = $aSumByPrice;
        $mHrSalaryReports->cachePvkhCredit          = $mAppCache->getUserCredit();
        $aData = null;
    }
    
    
    /** @Author: ANH DUNG Jul 31, 2018
     *  @Todo: get array ngày nghỉ của NV trong khoảng date 
     *  1. Tính tổng số ngày nghỉ của NV trong năm đến date_from
     *  2. Kiểm tra đã hết 12 ngày phép 1 năm chưa
     *  3. đưa ra 2 array:  1 array là ngày nghỉ phép  - có lương
     *                      1 array là ngày nghỉ phép - không lương
     *  @note1: cacheLeaveNormal ngày nghỉ phép  - có lương: format: $aRes[$item->user_id]['2018-07-31'] = '2018-07-31'
     *  @note2: cacheLeaveWithoutSalary ngày nghỉ phép - không lương - format: $aRes[$item->user_id]['2018-07-31'] = '2018-07-31'
     **/
    public function getLeave(&$mHrSalaryReports) {
        $cacheLeaveNormal = $cacheLeaveWithoutSalary = [];
        $mLeaveHolidays         = new GasLeaveHolidays();
        $mLeaveHolidays->date   = $mHrSalaryReports->start_date;// date format 'Y-m-d'
        $mLeave                     = new GasLeave();
        $mLeave->leave_date_from    = $mHrSalaryReports->start_date;// date format 'Y-m-d'
        $mLeave->leave_date_to      = $mHrSalaryReports->end_date;// date format 'Y-m-d'
        // 1. Tính tổng số ngày nghỉ của NV trong năm đến start_date
        $aLeaveDone                         = $mLeave->getTotalLeaveToDate();
        $aLeaveInRangeDate                  = $mLeave->getLeaveRangeDate();
        $mHrSalaryReports->aHolidaysInYear  = $mLeaveHolidays->getHolidaysInYear();
        foreach($aLeaveInRangeDate as $user_id => $aLeave){
            $totalLeaveBefore = isset($aLeaveDone[$user_id]) ? $aLeaveDone[$user_id] : 0;
            foreach($aLeave as $mLeave){
                $mLeave->calcStatusLeaveUser($mHrSalaryReports->aHolidaysInYear, $totalLeaveBefore, $cacheLeaveNormal, $cacheLeaveWithoutSalary);
            }
        }
        $mHrSalaryReports->cacheLeaveNormal        = $cacheLeaveNormal;
        $mHrSalaryReports->cacheLeaveWithoutSalary = $cacheLeaveWithoutSalary;
        $aLeaveDone = $aLeaveInRangeDate = null;
    }
    
    /** @Author: ANH DUNG Aug 16, 2018
     *  @Todo:  get array all Base Salary user
     **/
    public function getBaseSalary(&$mHrSalaryReports) {
        $criteria = new CDbCriteria();
        $criteria->compare('rUser.payment_day', 1);
        $criteria->with = ['rUser'];
        $models = UsersProfile::model()->findAll($criteria);
        $mHrSalaryReports->aBaseSalary      = CHtml::listData($models, 'user_id', 'base_salary');
        $mHrSalaryReports->aSalaryInsurance = CHtml::listData($models, 'user_id', 'salary_insurance');
    }
    
    /** @Author: ANH DUNG Oct 01, 2018
     *  @Todo: array các agent mà 1 NV quản lý
     **/
    public function getAgentOfUser(&$mHrSalaryReports) {
        $mSetupTeam = new SetupTeam();
        $mSetupTeam->date_from              = $mHrSalaryReports->start_date;// start_date is Y-m-d
        $mSetupTeam->date_to                = $mHrSalaryReports->end_date;// end_date is Y-m-d
        $aType = [SetupTeam::TYPE_KTKV_AGENT];
        $mHrSalaryReports->cacheAgentOfUser = $mSetupTeam->getByType($aType);
    }

    /** @Author: ANH DUNG Oct 01, 2018
     *  @Todo: array các khoản phạt của 1 nv
     **/
    public function getPunishUser(&$mHrSalaryReports) {
        $mEmployeeProblems = new EmployeeProblems();
        $mEmployeeProblems->date_from           = $mHrSalaryReports->dateFromDmy;
        $mEmployeeProblems->date_to             = $mHrSalaryReports->dateToDmy;
        $mHrSalaryReports->cachePunishUser      = $mEmployeeProblems->getPunishUser();
    }
    
    /**
     * @author DuongNV Nov 14,2018
     * @return Số công để tính chỉ tiêu (chung - mới)
     * @des: đếm ngày công thuộc nhóm chỉ tiêu trừ đi ngày công có tên là NC 1/2 CT
     */
    public function getWorkingDateForTarget($mHrSalaryReports) {
        $typeTarget = HrWorkSchedule::model()->getCacheDataWorkScheduleDays($mHrSalaryReports->start_date, $mHrSalaryReports->end_date, HrWorkShift::TYPE_HGD, true);
//        $typeHalf   = $this->getWorkingDateForHalfTarget($mHrSalaryReports);
//        $aData      = [];
//        foreach ($typeTarget as $user_id => $number_target) {
//            $half = isset($typeHalf[$user_id]) ? $typeHalf[$user_id] : 0;
//            $aData[$user_id] = $number_target - $half;
//        }
        return $typeTarget;
    }
    
    /** @Author: DuongNV Nov 20,2018
     *  @Todo: Ngày công CG - Telesale (mới)
     *  @des: đếm ngày công của nhóm CG - Telesale
     **/
    public function getWorkingFoneForTarget($mHrSalaryReports) {
        $aTypeTarget = HrWorkSchedule::model()->getCacheDataWorkScheduleDays($mHrSalaryReports->start_date, $mHrSalaryReports->end_date, HrWorkShift::TYPE_TARGET, true);
        $aHalfTarget = $this->getWorkingDateForHalfTarget($mHrSalaryReports, true);
        $aData = [];
        foreach ($aTypeTarget as $key => $value_type) {
            $value_half = isset($aHalfTarget[$key]) ? $aHalfTarget[$key] : 0;
            $aData[$key] = $value_type - $value_half;
        }
        return $aData;
    }
    
    /** @Author: DuongNV Nov 20,2018
     *  @Todo: Số ngày công 1/2 chỉ tiêu (mới)
     *  @param $isCount true -> 1, false -> 0.5 (Trên 1 ngày đi làm)
     *  @des: đếm theo tên NC 1/2 CT
     **/
    public function getWorkingDateForHalfTarget($mHrSalaryReports, $isCount = false) {
        $criteria = new CDbCriteria();
        $criteria->addBetweenCondition("t.work_day", $mHrSalaryReports->start_date, $mHrSalaryReports->end_date);
        $tblName = HrWorkShift::model()->tableName();
        $criteria->join = ' JOIN ' . $tblName . ' ws ON ws.id = t.work_shift_id';
        $criteria->compare('ws.type', HrWorkShift::TYPE_HGD);
        $idShift = HrWorkShift::model()->getIdOfHaftTargetShift();
        $criteria->compare('ws.id', $idShift);
        $criteria->select = 'DISTINCT t.*, ws.factor as factor';
        $aData = [];
        $number = $isCount ? 1 : 0.5;
        $mSchedule = HrWorkSchedule::model()->findAll($criteria);
        foreach ($mSchedule as $item) {
            if(isset($aData[$item->employee_id])){
                $aData[$item->employee_id] += $number * $item->factor;
            } else {
                $aData[$item->employee_id] = $number * $item->factor;
            }
        }
        return $aData;
    }
    
    /** @Author: DuongNV Nov 21,2018
     *  @Todo: Init data để tính lương
     **/
    public function getParameterForTarget(&$mHrSalaryReports) {
        $mHrSalaryReports->cacheHaftTarget = $this->getWorkingDateForHalfTarget($mHrSalaryReports);
        $mHrSalaryReports->cacheFoneTarget = $this->getWorkingFoneForTarget($mHrSalaryReports);
        $mHrSalaryReports->cacheTarget     = $this->getWorkingDateForTarget($mHrSalaryReports);
    }
    
    /** @Author: DuongNV Mar 21, 2019
     *  @Todo: số đơn hàng hoàn thành pvkh (bò mối - GasAppOrder)
     **/
    public function getCompleteOrderBoMoi($mHrSalaryReports) {
        $criteria = new CDbCriteria;
        $criteria->select = 'count(t.id) as id, t.employee_maintain_id, DATE(t.complete_time) as complete_time';
        $criteria->compare('t.type', GasAppOrder::DELIVERY_NOW);
        $criteria->compare('t.status', GasAppOrder::STATUS_COMPPLETE);
        $criteria->group = 't.employee_maintain_id, DATE(t.complete_time)';
//        $criteria->addCondition('t.employee_maintain_id > 0');
        $criteria->addBetweenCondition("EXTRACT(HOUR FROM t.complete_time)", self::PVKH_START, self::PVKH_END);
        DateHelper::searchBetween($mHrSalaryReports->start_date, $mHrSalaryReports->end_date, 'date_delivery_bigint', $criteria, false);
        $models = GasAppOrder::model()->findAll($criteria);
        $ret = [];
        foreach ($models as $value) {
            $ret[$value->employee_maintain_id][$value->complete_time] = $value->id;
        }
        return $ret;
    }   
    
    /** @Author: DuongNV Mar 21, 2019
     *  @Todo: số đơn hàng hoàn thành pvkh (HGD - Sell)
     **/
    public function getCompleteOrderHGD($mHrSalaryReports) {
        $criteria = new CDbCriteria;
        $criteria->select = 'count(t.id) as id, t.employee_maintain_id, t.created_date_only';
        $criteria->compare('t.status', Sell::STATUS_PAID);
        $criteria->group = 't.employee_maintain_id, t.created_date_only';
//        $criteria->addCondition('t.employee_maintain_id > 0');
        DateHelper::searchBetween($mHrSalaryReports->start_date, $mHrSalaryReports->end_date, 'created_date_only_bigint', $criteria);
        $models = Sell::model()->findAll($criteria);
        $ret = [];
        foreach ($models as $value) {
            $ret[$value->employee_maintain_id][$value->created_date_only] = $value->id;
        }
        return $ret;
    }   
    
    /** @Author: DuongNV Nov 21,2018
     *  @Todo: Init data để tính lương
     **/
    public function initDataCompleteOrderPVKH(&$mHrSalaryReports) {
        $mHrSalaryReports->cacheCompleteOrderHGDPvkh   = $this->getCompleteOrderHGD($mHrSalaryReports);
        $mHrSalaryReports->cacheCompleteOrderBoMoiPvkh = $this->getCompleteOrderBoMoi($mHrSalaryReports);
    }
    
    /** @Author: DuongNV Nov 21,2018
     *  @Todo: Init data của những report đã tính
     **/
    public function getCacheCalculated(&$mHrSalaryReports) {
        $mUsersHr = new UsersHr();
        $mHrSalaryReports->cacheCalculated['SalarySumMoneyWithHeld']    = $mHrSalaryReports->getFromCalculated($mUsersHr->getIdMoneyWithHeld());
        $mHrSalaryReports->cacheCalculated['SalarySumAdvancePeriodOne'] = $mHrSalaryReports->getFromCalculated($mUsersHr->getIdPrepaidWages());
        $mHrSalaryReports->cacheCalculated['EfficiencyValue']           = $mHrSalaryReports->getFromCalculated($mUsersHr->getIdCalcOutput());
        $mHrSalaryReports->cacheCalculated['TimeWages']                 = $mHrSalaryReports->getFromCalculated($mUsersHr->getIdTimeWages());
        $mHrSalaryReports->cacheCalculated['AllowanceValue']            = $mHrSalaryReports->getFromCalculated($mUsersHr->getIdCalcSupport());
    }
    
    /** @Author: DuongNV Nov 21,2018
     *  @Todo: Init công nợ của nhân viên
     **/
    public function getCacheDebts(&$mHrSalaryReports) {
        $from       = $mHrSalaryReports->start_date;
        $to         = $mHrSalaryReports->end_date;
        $fromYear   = CommonProcess::convertDateTime($from, DomainConst::DATE_FORMAT_4, 'Y');
        $fromMonth  = CommonProcess::convertDateTime($from, DomainConst::DATE_FORMAT_4, 'm');
        $toYear     = CommonProcess::convertDateTime($to, DomainConst::DATE_FORMAT_4, 'Y');
        $toMonth    = CommonProcess::convertDateTime($to, DomainConst::DATE_FORMAT_4, 'm');
        $mGasDebts  = new GasDebts();
        $aUserDebtsType = $mGasDebts->getArrTypeCutSalary();
        $criteria   = new CDbCriteria();
        $criteria->addCondition('YEAR(month) >= ' . $fromYear . ' AND MONTH(month)>=  '. $fromMonth
                . ' AND YEAR(month) <= ' . $toYear . ' AND MONTH(month) <= ' . $toMonth);
        $criteria->addInCondition('t.type', $aUserDebtsType);
        $models     = GasDebts::model()->findAll($criteria);
        $retVal     = [];
        foreach ($models as $model) {
            if( isset($retVal[$model->user_id]) ){
                $retVal[$model->user_id] += $model->amount;
            } else {
                $retVal[$model->user_id] = $model->amount;
            }
        }
        $mHrSalaryReports->cacheDebts = $retVal;
    }
    
    /** @Author: DuongNV Jul1919
     *  @Todo: Init cacheCommonDebts - những tham số liên quan đến công nợ của nhân viên
     **/
    public function getDebtsByType($mHrSalaryReports, $aType) {
        $from       = $mHrSalaryReports->start_date;
        $to         = $mHrSalaryReports->end_date;
        $fromYear   = CommonProcess::convertDateTime($from, DomainConst::DATE_FORMAT_4, 'Y');
        $fromMonth  = CommonProcess::convertDateTime($from, DomainConst::DATE_FORMAT_4, 'm');
        $toYear     = CommonProcess::convertDateTime($to, DomainConst::DATE_FORMAT_4, 'Y');
        $toMonth    = CommonProcess::convertDateTime($to, DomainConst::DATE_FORMAT_4, 'm');
        $criteria   = new CDbCriteria();
        $criteria->addCondition('YEAR(month) >= ' . $fromYear . ' AND MONTH(month)>=  '. $fromMonth
                . ' AND YEAR(month) <= ' . $toYear . ' AND MONTH(month) <= ' . $toMonth);
        if( !empty($aType) && is_array($aType) ){
            $criteria->addInCondition('t.type', $aType);
        }
        $models     = GasDebts::model()->findAll($criteria);
        $retVal     = [];
        foreach ($models as $model) {
            if( isset($retVal[$model->user_id]) ){
                $retVal[$model->user_id] += $model->amount;
            } else {
                $retVal[$model->user_id] = $model->amount;
            }
        }
        return $retVal;
    }
    
    /** @Author: DuongNV Jul1919
     *  @Todo: Init cacheCommonDebts - những tham số liên quan đến công nợ của nhân viên
     **/
    public function getCacheCommonDebts(&$mHrSalaryReports) {
        $aData['SALARY_PRODUCT']            = $this->getDebtsByType($mHrSalaryReports, [GasDebts::TYPE_SALARY_OF_PRODUCTS]); // Lương khoán
        $aData['OTHER_SUPPORT']             = $this->getDebtsByType($mHrSalaryReports, [GasDebts::TYPE_OTHER_SUPPORT]); // Trực đêm và CPS
        
         // Công nợ hiện tại của nhân viên, vd tính lương 1/7-31/7 thì lấy cột nợ cuối kỳ trong BCCN
        $mGasDebtsReport                    = new GasDebtsReport();
        $mGasDebtsReport->date_from         = $mHrSalaryReports->dateFromDmy;
        $mGasDebtsReport->date_to           = $mHrSalaryReports->dateToDmy;
        $mGasDebtsReport->salary_code_no    = $mHrSalaryReports->code_no;
        $dataReport                         = $mGasDebtsReport->getReportDebts(true);
        $aData['CURRENT_DEBTS']             = isset($dataReport['CLOSING_DEBIT']) ? $dataReport['CLOSING_DEBIT'] : [];
        
        $aData['SPECIAL_DEBTS']             = $mHrSalaryReports->getArrayDebtsSpecial();
        $mHrSalaryReports->cacheCommonDebts = $aData;
    }
    
}
