<?php

/**
 * This is the model class for table "{{_gas_maintain}}".
 *
 * The followings are the available columns in table '{{_gas_maintain}}':
 * @property string $id
 * @property string $is_new_customer
 * @property integer $customer_id
 * @property string $maintain_date
 * @property string $maintain_employee_id
 * @property integer $materials_id
 * @property string $seri_no
 * @property string $note
 * @property integer $agent_id
 * @property string $note_update_status
 * @property integer $update_num
 * @property integer $status
 * @property integer $user_id_create
 * @property string $created_date
 */
class GasMaintain extends BaseSpj
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return GasMaintain the static model class
	 */
    
         public $customer_name;
         public $customer_address;
         public $customer_phone;
         public $customer_code_bussiness;
         public $customer_code_account;
         public $autocomplete_name;
         public $date_from;
         public $date_to;
         public $count_row;
         public $statistic_month;
         public $statistic_year;
         public $had_selling;         
         public $sort_by='desc';         
	public $autocomplete_material_name;	 
        public $MAX_ID;
		 
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{_gas_maintain}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
            return array(
//array('customer_name, customer_address, customer_phone', 'required'),
array('customer_id', 'required','on'=>'create_maintain,update_maintain'),
array('maintain_date, maintain_employee_id, accounting_employee_id, materials_id, seri_no', 'required','on'=>'create_maintain,update_maintain'),
array('maintain_employee_id,customer_name', 'length', 'max'=>100),
array('seri_no,customer_phone', 'length', 'max'=>20),
array('note,customer_address', 'length', 'max'=>500),
array('note_update_status', 'length', 'max'=>255),
array('id, customer_id, maintain_date, maintain_employee_id, materials_id, seri_no, note, agent_id, note_update_status, update_num, status, user_id_create, created_date', 'safe'),
array('customer_name, customer_address, customer_phone,is_new_customer,', 'safe'),
array('customer_code_account, customer_code_bussiness', 'safe'),
array('accounting_employee_id,is_need_maintain_back', 'safe'),
array('status_maintain_back,note_maintain_back,status_call_bad,note_call_bad', 'safe'),
array('date_from,date_to,statistic_month,statistic_year,had_selling,using_gas_huongminh,type', 'safe'),
array('code_no,province_id,district_id,ward_id,monitoring_id,sort_by', 'safe'),
                
//array('customer_code_bussiness', 'checkCodeCustomerMaintain', 'on'=>'create_maintain,update_maintain'),
                
                
            );
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		return array(
                    'customer' => array(self::BELONGS_TO, 'Users', 'customer_id'),
                    'maintain_employee' => array(self::BELONGS_TO, 'Users', 'maintain_employee_id'),
                    'accounting_employee' => array(self::BELONGS_TO, 'Users', 'accounting_employee_id'),
                    'agent' => array(self::BELONGS_TO, 'Users', 'agent_id'),
                    'materials' => array(self::BELONGS_TO, 'GasMaterials', 'materials_id'),
                    'GasMaintainSell' => array(self::HAS_MANY, 'GasMaintainSell', 'maintain_id','joinType'=>'INNER JOIN'),
                    'province' => array(self::BELONGS_TO, 'GasProvince', 'province_id'),
                    'district' => array(self::BELONGS_TO, 'GasDistrict', 'district_id'),
                    'ward' => array(self::BELONGS_TO, 'GasWard', 'ward_id'),
		);
	}


        public function checkCodeCustomerMaintain($attribute,$params)
        {
            if($this->isNewRecord){
                if(!empty($this->customer_code_bussiness)){
                    $criteria=new CDbCriteria;
                    $criteria->compare('t.code_bussiness',$this->customer_code_bussiness);
                    if(Yii::app()->user->role_id==ROLE_SUB_USER_AGENT){
                        $criteria->compare('t.area_code_id',Yii::app()->user->parent_id);
//                        $session=Yii::app()->session;
//                        $session['CUSTOMER_OF_AGENT_MAINTAIN'] = GasMaintain::getCustomerOfAgent(Yii::app()->user->id);
//                        $criteria->addInCondition('t.id', $session['CUSTOMER_OF_AGENT_MAINTAIN']); 
                    }
                    $mUser = Users::model()->find($criteria);   
                    if($mUser)
                        $this->addError("customer_code_bussiness","Mã Khách Hàng Này Đã Tồn Tại. Hãy Chọn Mã Khác");                    
                }
            }else{
                if(!empty($this->customer_code_bussiness) && !empty($this->customer_id)){
                    $criteria=new CDbCriteria;
                    $criteria->compare('t.code_bussiness',$this->customer_code_bussiness);
                    if(Yii::app()->user->role_id==ROLE_SUB_USER_AGENT){
                        $criteria->compare('t.area_code_id',Yii::app()->user->parent_id);
//                        $session=Yii::app()->session;
//                        $session['CUSTOMER_OF_AGENT_MAINTAIN'] = GasMaintain::getCustomerOfAgent(Yii::app()->user->id);
//                        $criteria->addInCondition('t.id', $session['CUSTOMER_OF_AGENT_MAINTAIN']); 
                    }                    
                    $mUser = Users::model()->find($criteria);                    
                    if($mUser && $mUser->id != $this->customer_id){                        
                        $this->addError("customer_code_bussiness","Mã Khách Hàng Này Đã Tồn Tại. Hãy Chọn Mã Khác");                    
                    }
                }               
                
            }
        }
        
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'customer_id' => 'Khách Hàng',
			'maintain_date' => 'Ngày Bảo Trì',
			'maintain_employee_id' => 'Nhân Viên Bảo Trì',
			'accounting_employee_id' => 'Nhân Viên Kế Toán',
			'materials_id' => 'Thương Hiệu Gas',
			'seri_no' => 'Số Seri',
			'note' => 'Ghi Chú',
			'agent_id' => 'Đại Lý',
			'note_update_status' => 'Ghi Chú Sau Cuộc Gọi',
			'update_num' => 'Số Lần Đã Cập Nhật',
			'status' => 'Trạng Thái',
			'user_id_create' => 'Người Tạo',
			'created_date' => 'Ngày Tạo',
			'customer_name' => 'Họ Tên Khách Hàng',
			'customer_address' => 'Địa Chỉ',
			'customer_phone' => 'Điện Thoại',
			'customer_code_bussiness' => 'Mã KH',
			'customer_code_account' => 'Mã Hệ Thống',
			'is_new_customer' => 'KH Mới',
			'is_need_maintain_back' => 'Cần nhân viên bảo trì xuống nữa',
			'status_maintain_back' => 'Kết Quả Xử Lý',
			'note_maintain_back' => 'Ghi Chú Sau Xử Lý',
			'status_call_bad' => 'Kết Quả Xử Lý',
			'note_call_bad' => 'Ghi Chú Sau Xử Lý',
			'date_from' => 'Từ Ngày',
			'date_to' => 'Đến Ngày',                    
			'statistic_month' => 'Chọn Tháng',
			'statistic_year' => 'Chọn Năm',
			'had_selling' => 'Bán Hàng Bảo Trì',
			'using_gas_huongminh' => 'Đang Dùng Gas Hướng Minh',
			'type' => 'Loại',
                        'province_id' => 'Tỉnh',
                        'district_id' => 'Quận Huyện',
                        'ward_id' => 'Phường/Xã',
			'monitoring_id' => 'Giám Sát PTTT',
			'sort_by' => 'Sắp Xếp',
                        'code_no' => 'Mã Số',
						
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;
                $aWith=array();
		$criteria->compare('t.id',$this->id,true);
		$criteria->compare('t.customer_id',$this->customer_id);
		if(!empty($this->maintain_date)){
                    $this->maintain_date = MyFormat::dateDmyToYmdForAllIndexSearch($this->maintain_date);
                    $criteria->compare('t.maintain_date',$this->maintain_date,true);
		}			
		
		if(!empty($this->customer_address)){
                    $aWith[] = 'customer';			
                    $criteria->compare('customer.address',$this->customer_address,true);
		}	
                
                if(isset($this->had_selling) && !empty($this->had_selling)){
                    $aWith[] = 'GasMaintainSell';                    
                }
		
		$criteria->compare('t.maintain_employee_id',$this->maintain_employee_id,true);
		$criteria->compare('t.materials_id',$this->materials_id);
		$criteria->compare('t.seri_no',$this->seri_no,true);
		$criteria->compare('t.note',$this->note,true);
		$criteria->compare('t.agent_id',$this->agent_id);
		$criteria->compare('t.using_gas_huongminh',$this->using_gas_huongminh);
		$criteria->compare('t.type',TYPE_MAINTAIN);
                if(is_array($this->status)){
                    $sParamsIn = implode(',', $this->status);
                    $criteria->addCondition("t.status IN ($sParamsIn)");
                }
                else
                    $criteria->compare('t.status',$this->status);                
		$criteria->compare('t.user_id_create',$this->user_id_create);
		$criteria->compare('t.is_need_maintain_back',$this->is_need_maintain_back);
//                $criteria->order = 't.id DESC';
		
                if(Yii::app()->user->role_id==ROLE_SUB_USER_AGENT){
                    $criteria->compare('t.agent_id',Yii::app()->user->parent_id);
                }else{
                    if(!empty($this->agent_id)){
                        $criteria->compare('t.agent_id', $this->agent_id);
                    }
                    GasAgentCustomer::addInConditionAgent($criteria, 't.agent_id');
                }
                
				
		if(!empty($this->date_from))
			$this->date_from = MyFormat::dateDmyToYmdForAllIndexSearch($this->date_from);
		if(!empty($this->date_to))
			$this->date_to = MyFormat::dateDmyToYmdForAllIndexSearch($this->date_to);				
                if(!empty($this->date_from) && empty($this->date_to))
                        $criteria->addCondition("t.maintain_date>='$this->date_from'");
                if(empty($this->date_from) && !empty($this->date_to))
                        $criteria->addCondition("t.maintain_date<='$this->date_to'");
                if(!empty($this->date_from) && !empty($this->date_to))
                        $criteria->addBetweenCondition("t.maintain_date",$this->date_from,$this->date_to); 				
                
		if(!empty($this->created_date)){
                    $this->created_date = MyFormat::dateDmyToYmdForAllIndexSearch($this->created_date);
                    $criteria->compare('t.created_date',$this->created_date,true);
		}              
                if(count($aWith)>0){
                    $criteria->with = $aWith;
                    $criteria->together = true;
                }

                $session=Yii::app()->session;
                if(isset($session['AGENT_OF_USER_MAINTAIN'])){
                    $sParamsIn = implode(',', $session['AGENT_OF_USER_MAINTAIN']);
                    if(!empty($sParamsIn)){
                        $criteria->addCondition("t.agent_id IN ($sParamsIn)");
                    }
                }                

                $sort = new CSort();
                $sort->attributes = array(
                    'maintain_date'=>'maintain_date',
                    'seri_no'=>'seri_no',
                    'maintain_employee_id'=>'maintain_employee_id',
//                    'note'=>'note',
                    'status'=>'status',
//                    'customer_id'=>array(
//                            'asc'=>'customer.first_name',
//                            'desc'=>'customer.first_name DESC',
//                    ),         

                );    
                $sort->defaultOrder = 't.id DESC';                        
                //$sort->defaultOrder = 't.customer_id DESC';                        
                
                $_SESSION['data-excel'] = new CActiveDataProvider($this, array(
                        'pagination'=>false,
			'criteria'=>$criteria,
                        'sort' => $sort,
		));
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
                        'pagination'=>array(
                            'pageSize'=> Yii::app()->user->getState('pageSize',Yii::app()->params['defaultPageSize']),
                        ),
                    'sort' => $sort,
		));;
	}

    /*
    public function activate()
    {
        $this->status = 1;
        $this->update();
    }

    public function deactivate()
    {
        $this->status = 0;
        $this->update();
    }
	*/

	public function defaultScope()
	{
		return array(
			//'condition'=>'',
		);
	}
        
        public function beforeValidate() {
            $this->seri_no = trim($this->seri_no);
            return parent::beforeValidate();
        }


        public function beforeSave() {
            if($this->isNewRecord){
                $this->user_id_create = Yii::app()->user->id;
                $this->code_no = MyFunctionCustom::getNextId('GasMaintain', 'BT'.date('y'), LENGTH_ORDER_BY_YEAR,'code_no');
            }
            if(strpos($this->maintain_date, '/')){
                $this->maintain_date =  MyFormat::dateConverDmyToYmd($this->maintain_date);
                MyFormat::isValidDate($this->maintain_date);
            }
            GasMaintainSell::setAddressForModel($this);
            $mUser = Users::model()->findByPk($this->customer_id);			
            if($mUser){
                $mUser->is_maintain = 1;
                $mUser->update(array('is_maintain'));
            }
            
            $this->note = InputHelper::removeScriptTag($this->note);
            $this->note_update_status = InputHelper::removeScriptTag($this->note_update_status);
            $this->note_maintain_back = InputHelper::removeScriptTag($this->note_maintain_back);
            $this->note_call_bad = InputHelper::removeScriptTag($this->note_call_bad);
            
                
            return parent::beforeSave();
        }
        
        public function beforeDelete() {
            $mUser = Users::model()->findByPk($this->customer_id);
            if($mUser){
                $criteria=new CDbCriteria;
                $criteria->compare('t.customer_id',$this->customer_id);
                $mMaintain = GasMaintain::model()->findAll($criteria);
                if(count($mMaintain)==1){
                    $mUser->is_maintain = 0;
                    $mUser->update(array('is_maintain'));
                }
            }
            return parent::beforeDelete();
        }
 
        /*
         * lấy id những KH bảo trì của từng đại lý
         */
        public static function getCustomerOfAgent($agent_id){
            $criteria=new CDbCriteria;
            $criteria->compare('t.agent_id',$agent_id);
            return  CHtml::listData(GasAgentCustomer::model()->findAll($criteria),'customer_id','customer_id');      
        }        
        
        /*
         * lấy id đại lý của KH bảo trì 
         * nếu mà trường hợp KH được gắn cho nhiều đại lý thì chỗ này sẽ bị sai
         */
        public static function getAgentOfCustomer($customer_id){
            $criteria=new CDbCriteria;
            $criteria->compare('t.customer_id',$customer_id);
            $model = GasAgentCustomer::model()->findAll($criteria);
            if(count($model)>1)
                die('Một khách hàng không thể thuộc về nhiều đại lý. Vui lòng báo lỗi này về công ty');
            
            if(count($model)==1)
                return $model[0]->agent_id;
            return false;
        }        
        
        
        public static function getLatestMaintainCustomerOfAgent($agent_id, $customer_id){
            $criteria=new CDbCriteria;
            $criteria->compare('t.agent_id',$agent_id);
            $criteria->compare('t.customer_id',$customer_id);
            $criteria->order = 't.id DESC';
            $model = GasMaintain::model()->findAll($criteria);
            if(count($model)>0)
                return $model[0]->id;
            return 0;
            
        }         
        
		// 11-01-2013 ANH DUNG
		// dung de lay tat ca bao tri cua KH ra, so sanh vs seri ban hang		
        public static function getAllMaintainCustomerOfAgent($agent_id, $customer_id){
            $criteria=new CDbCriteria;
            $criteria->compare('t.agent_id',$agent_id);
            $criteria->compare('t.customer_id',$customer_id);
            $criteria->order = 't.id ASC';
            return GasMaintain::model()->findAll($criteria);
        }           
		// 11-09-2013 ANH DUNG
		// dung de lay bao tri cua KH ra, dua tren materials_id vs seri_no ( chỉ cần 2 thông tin này là đủ xác định)
        public static function getMaintainByMaterialAndSeriNo($materials_id, $seri_no, $needMore=array()){
            $criteria=new CDbCriteria;
            $criteria->compare('t.materials_id', trim($materials_id));
            $criteria->compare('t.seri_no',trim($seri_no));
            if(isset($needMore['agent_id']))
                $criteria->compare('t.agent_id', $needMore['agent_id']);// 02-08-2014 thêm điều kiện chỉ lấy những KH PTTT mà đại lý đó tạo
            if(isset($needMore['customer_id']))
                $criteria->compare('t.customer_id', $needMore['customer_id']);// Feb-14-2014 thêm điều kiện khách hàng phải là của nhân viên PTTT đó
            // do có PTTT ở đại lý khác có seri trùng, mà một đại lý lại nhập seri đó
//            $criteria->order = 't.id ASC';// close date 01-05-2013 sẽ luôn tính bán hàng cho PTTT vì thời điểm này không còn làm chương trình bảo trì nữa
            $criteria->order = 't.id DESC';
            // chỗ này xử lý cho nhân viên bảo trì vs PTTT, nếu ai nhập trc thì sẽ lấy của người đó
            // chỗ này sẽ không đúng nếu KH đó dc bảo trì 2 lần, thì sẽ xuất hiện materials_id vs seri_no 2 hoặc nhiều lần .....
            // nếu chạy hết chương trình sau đó chạy lại thì có thể hệ thống sẽ không còn đúng vì bị trùng materials_id vs seri_no
            // mà hiện tại đang căn cứ vào ai đến trước thì tính cho người đó
            $model =  GasMaintain::model()->findAll($criteria);
            if(count($model)>0)
                return $model[0];
            return 0;			
        }   
        
        // 12-05-2013 ANH DUNG
        public static function getMaintainByCustomerId($customer_id, $needMore=array()){
            $criteria=new CDbCriteria;
            $criteria->compare('t.customer_id',$customer_id);
            if(isset($needMore['agent_id']))
                $criteria->compare('t.agent_id', $needMore['agent_id']);// 02-08-2014 thêm điều kiện chỉ lấy những KH PTTT mà đại lý đó tạo
            $criteria->order = 't.id DESC'; // chỗ này xử lý cho nhân viên bảo trì vs PTTT, nếu ai nhập trc thì sẽ lấy của người đó
			// chỗ này sẽ không đúng nếu KH đó dc bảo trì 2 lần, thì sẽ xuất hiện materials_id vs seri_no 2 hoặc nhiều lần .....
			// nếu chạy hết chương trình sau đó chạy lại thì có thể hệ thống sẽ không còn đúng vì bị trùng materials_id vs seri_no
			// mà hiện tại đang căn cứ vào ai đến trước thì tính cho người đó
            $model =  GasMaintain::model()->findAll($criteria);
            if(count($model)>0)
                return $model[0];
            return 0;			
        }         
        
        public static function getLatestObjMaintainCustomerOfAgent($agent_id, $customer_id){
            $criteria=new CDbCriteria;
            $criteria->compare('t.agent_id',$agent_id);
            $criteria->compare('t.customer_id',$customer_id);
            $criteria->order = 't.id DESC';
            $model = GasMaintain::model()->findAll($criteria);
            if(count($model)>0)
                return $model[0];
            return null;
        }         
		      
        
}