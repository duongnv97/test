<?php

/**
 * This is the model class for table "{{_gas_closing_balance_customer_materials}}".
 *
 * The followings are the available columns in table '{{_gas_closing_balance_customer_materials}}':
 * @property string $id
 * @property integer $year
 * @property string $customer_id
 * @property integer $type_customer
 * @property integer $materials_id
 * @property integer $materials_type_id
 * @property string $import
 * @property string $export
 * @property string $balance
 */
class GasClosingBalanceCustomerMaterials extends CActiveRecord
{
    public $autocomplete_name;
    
    
    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return GasClosingBalanceCustomerMaterials the static model class
     */
    public static function model($className=__CLASS__)
    {
            return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
            return '{{_gas_closing_balance_customer_materials}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        return array(
            array('customer_id, export_materials_type_id, balance', 'required', 'on'=>'createNewBalance'),
            array('id, year,  customer_id, type_customer, export_materials_type_id, import_materials_type_id, import, export, balance', 'safe'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        return array(
            'customer' => array(self::BELONGS_TO, 'Users', 'customer_id'),
            'rCustomer' => array(self::BELONGS_TO, 'Users', 'customer_id'),
            'rExportMaterials' => array(self::BELONGS_TO, 'GasMaterialsType', 'export_materials_type_id'),
            'rImportaterials' => array(self::BELONGS_TO, 'GasMaterialsType', 'import_materials_type_id'),                
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'year' => 'Year',
            'customer_id' => 'Khách Hàng',
            'type_customer' => 'Loại KH',
            'export_materials_type_id' => 'Loại Gas',
            'import_materials_type_id' => 'Loại Vỏ',
            'import' => 'Nhập',
            'export' => 'Xuất',
            'balance' => 'Tồn',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        $criteria=new CDbCriteria;
//        $criteria->compare('t.year',$this->year);
        $criteria->compare('t.year', GasMaterialsOpeningBalance::YEAR_OPENING);
        $criteria->compare('t.customer_id',$this->customer_id);
        $criteria->compare('t.type_customer',$this->type_customer);
        $criteria->compare('t.export_materials_type_id',$this->export_materials_type_id);
        $criteria->compare('t.import_materials_type_id',$this->import_materials_type_id);
        $criteria->order = 't.id desc';
        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
            'pagination'=>array(
                    'pageSize'=> 50,
                ),
        ));
    }

    public function defaultScope()
    {
            return array(
                    //'condition'=>'',
            );
    }
    
    protected function beforeSave() {
        if($this->scenario=='createNewBalance'){
            $this->import_materials_type_id = CmsFormatter::$MATERIAL_TYPE_PAIR[$this->export_materials_type_id];
            $this->type_customer = $this->customer->is_maintain;
        }        
        return parent::beforeSave();
    }
    
    /**
     * @Author: ANH DUNG Jun 02, 2014
     * @Todo: load model
     * @Param: $year model user
     * @Param: $customer_id 
     * @Param: $export_materials_type_id 
     * @Param: $import_materials_type_id
     */
    public static function loadModel($year, $customer_id, $export_materials_type_id, $import_materials_type_id){
        $criteria = new CDbCriteria();
        $criteria->compare("t.year", $year);
        $criteria->compare("t.customer_id", $customer_id);
        $criteria->compare("t.export_materials_type_id", $export_materials_type_id);
        $criteria->compare("t.import_materials_type_id", $import_materials_type_id);
        return self::model()->find($criteria);
    }

    /**
     * @Author: ANH DUNG Jun 01, 2014
     * @Todo: add new record for new year agent if new import is add
     * @param: $year, $customer_id, $type_customer, 
     * @param: $export_materials_type_id, $import_materials_type_id, $import
     */
    public static function addNewRecordImport($year, $customer_id, $type_customer, $export_materials_type_id, $import_materials_type_id, $import){
        if(empty($year) ||  $year<1) return;
        $model = new GasClosingBalanceCustomerMaterials();
        $model->year = $year;
        $model->customer_id = $customer_id;
        $model->export_materials_type_id = $export_materials_type_id;
        $model->import_materials_type_id = $import_materials_type_id;
        $model->type_customer = $type_customer;
        $model->sale_id = $model->rCustomer?$model->rCustomer->sale_id:0;
        $model->agent_id = $model->rCustomer?$model->rCustomer->area_code_id:0;
        if($model->rCustomer){
            $model->customer_parent_id = $customer_id;
            if(!empty($model->rCustomer->parent_id) && $model->rCustomer->parent_id>0){
                $model->customer_parent_id = $model->rCustomer->parent_id;
            }
        }        
        $model->import = $import;
        $model->export = 0;
        $model->balance = $model->export-$model->import;
        $model->save();
    }
    
    /**
     * @Author: ANH DUNG Jun 01, 2014
     * @Todo: add new record for new year agent if new export is add
     * @param: $year, $customer_id, $type_customer, 
     * @param: $export_materials_type_id, $import_materials_type_id, $export
     */
    public static function addNewRecordExport($year, $customer_id, $type_customer, $export_materials_type_id, $import_materials_type_id, $export){
        if(empty($year) ||  $year<1) return;
        $model = new GasClosingBalanceCustomerMaterials();
        $model->year = $year;
        $model->customer_id = $customer_id;
        $model->export_materials_type_id = $export_materials_type_id;
        $model->import_materials_type_id = $import_materials_type_id;
        $model->type_customer = $type_customer;
        $model->sale_id = $model->rCustomer?$model->rCustomer->sale_id:0;
        $model->agent_id = $model->rCustomer?$model->rCustomer->area_code_id:0;
        if($model->rCustomer){
            $model->customer_parent_id = $customer_id;
            if(!empty($model->rCustomer->parent_id) && $model->rCustomer->parent_id>0){
                $model->customer_parent_id = $model->rCustomer->parent_id;
            }
        }
        $model->import = 0;
        $model->export = $export;        
        $model->balance = $model->export-$model->import;
        $model->save();
    }
            
    /**
     * @Author: ANH DUNG Jun 01, 2014
     * @Todo: cộng import Material khi phát sinh mới hoặc cập nhật thẻ kho 
     * @Param: $year
     * @Param: $customer_id mã KH
     * @Param: $type_customer
     * @Param: $export_materials_type_id, $import_materials_type_id, $import
     * dùng ở khi phát sinh mới hoặc cập nhật thẻ kho
     */
    public static function addMaterialImport($year, $customer_id, $type_customer, $export_materials_type_id, $import_materials_type_id, $import){
        return ; 
        /**  Aug 21, 2016 BIG CHANGE
         *  table này thiết kế để tính dc công nợ vỏ của KH, nhưng thực tế là chưa triển khai được
         * tồn đầu kỳ của KH là chưa có process để nhập vào, nên tạm thời ko thao tác add hay update vào đây nữa
         * khi nào triển khai theo dõi vỏ của KH thì có thể sẽ Open ra hoặc tìm cách khác upate cho đỡ phải truy xuất db nhiều
         * vì mỗi một thẻ kho có 10 item thì nó sẽ vào hàm này 10 lần => không nên làm vậy
         */
    
        if(!self::validCustomer($customer_id)) return;
        $model = self::loadModel($year, $customer_id, $export_materials_type_id, $import_materials_type_id);
        if(is_null($model)){
            self::addNewRecordImport($year, $customer_id, $type_customer, 
                    $export_materials_type_id, $import_materials_type_id, $import);
        }else{
            $model->import += $import;
            $model->balance = $model->export-$model->import;
            $model->update(array('import', 'balance'));
        }
    }
    
    /**
     * @Author: ANH DUNG Jun 01, 2014
     * @Todo: trừ import Material khi phát sinh mới hoặc cập nhật thẻ kho 
     * @Param: $year
     * @Param: $customer_id mã KH
     * @Param: $export_materials_type_id, $import_materials_type_id, $import
     * dùng ở khi phát sinh mới hoặc cập nhật thẻ kho
     */
    public static function cutMaterialImport($year, $customer_id, $export_materials_type_id, $import_materials_type_id, $import){
        return ; 
        /**  Aug 21, 2016 BIG CHANGE
         *  table này thiết kế để tính dc công nợ vỏ của KH, nhưng thực tế là chưa triển khai được
         * tồn đầu kỳ của KH là chưa có process để nhập vào, nên tạm thời ko thao tác add hay update vào đây nữa
         * khi nào triển khai theo dõi vỏ của KH thì có thể sẽ Open ra hoặc tìm cách khác upate cho đỡ phải truy xuất db nhiều
         * vì mỗi một thẻ kho có 10 item thì nó sẽ vào hàm này 10 lần => không nên làm vậy
         */
        $model = self::loadModel($year, $customer_id, $export_materials_type_id, $import_materials_type_id);
        if($model){
            $model->import -= $import;
            $model->balance = $model->export-$model->import;
            $model->update(array('import', 'balance'));
        }
    }
    
    /**
     * @Author: ANH DUNG Jun 01, 2014
     * @Todo: cộng export Material khi phát sinh mới hoặc cập nhật thẻ kho 
     * @Param: $year
     * @Param: $customer_id mã KH
     * @Param: $type_customer
     * @Param: $export_materials_type_id, $import_materials_type_id, $export
     * dùng ở khi phát sinh mới hoặc cập nhật thẻ kho
     */
    public static function addMaterialExport($year, $customer_id, $type_customer, $export_materials_type_id, $import_materials_type_id, $export){
        return ; 
        /**  Aug 21, 2016 BIG CHANGE
         *  table này thiết kế để tính dc công nợ vỏ của KH, nhưng thực tế là chưa triển khai được
         * tồn đầu kỳ của KH là chưa có process để nhập vào, nên tạm thời ko thao tác add hay update vào đây nữa
         * khi nào triển khai theo dõi vỏ của KH thì có thể sẽ Open ra hoặc tìm cách khác upate cho đỡ phải truy xuất db nhiều
         * vì mỗi một thẻ kho có 10 item thì nó sẽ vào hàm này 10 lần => không nên làm vậy
         */
        
        if(!self::validCustomer($customer_id)) return;
        $model = self::loadModel($year, $customer_id, $export_materials_type_id, $import_materials_type_id);
        if(is_null($model)){
            self::addNewRecordExport($year, $customer_id, $type_customer, 
                    $export_materials_type_id, $import_materials_type_id, $export);
        }else{
            $model->export += $export;
            $model->balance = $model->export-$model->import;
            $model->update(array('export', 'balance'));
        }
    }
    
    /**
     * @Author: ANH DUNG Jun 01, 2014
     * @Todo: trừ export đại lý khi phát sinh mới hoặc cập nhật thẻ kho 
     * @Param: $year
     * @Param: $customer_id mã KH
     * @Param: $export_materials_type_id, $import_materials_type_id mã loại vật tư
     * @Param: $export số lượng export
     * dùng ở khi phát sinh mới hoặc cập nhật thẻ kho
     */
    public static function cutMaterialExport($year, $customer_id, $export_materials_type_id, $import_materials_type_id, $export){
        return ; 
        /**  Aug 21, 2016 BIG CHANGE
         *  table này thiết kế để tính dc công nợ vỏ của KH, nhưng thực tế là chưa triển khai được
         * tồn đầu kỳ của KH là chưa có process để nhập vào, nên tạm thời ko thao tác add hay update vào đây nữa
         * khi nào triển khai theo dõi vỏ của KH thì có thể sẽ Open ra hoặc tìm cách khác upate cho đỡ phải truy xuất db nhiều
         * vì mỗi một thẻ kho có 10 item thì nó sẽ vào hàm này 10 lần => không nên làm vậy
         */
        $model = self::loadModel($year, $customer_id, $export_materials_type_id, $import_materials_type_id);
        if($model){
            $model->export -= $export;
            $model->balance = $model->export-$model->import;
            $model->update(array('export', 'balance'));
        }
    }
    
    /**
     * @Author: ANH DUNG Jun 02, 2014
     * @Todo: check only customer can tính công nợ vỏ
     * @Param: $customer_id
     */
    public static function validCustomer($customer_id){
        if(empty($customer_id)) return false;
        $mUser = Users::model()->findByPk($customer_id);
        if($mUser && $mUser->role_id!=ROLE_CUSTOMER) return false;
        return true;
    }
    
    
    
}