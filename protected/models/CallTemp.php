<?php

/**
 * This is the model class for table "{{_call_temp}}".
 *
 * The followings are the available columns in table '{{_call_temp}}':
 * @property string $id
 * @property string $call_uuid
 * @property string $call_uuid_extension
 * @property integer $extension
 * @property string $user_id
 * @property string $agent_id
 * @property integer $province_id
 * @property integer $direction
 * @property integer $call_status
 * @property string $start_time
 * @property string $answer_time
 * @property string $end_time
 * @property integer $total_duration 
 * @property integer $bill_duration
 * @property string $created_date
 */
class CallTemp extends BaseSpj
{
    public $modelCallTemp   = null;
    public $modelAnswer     = null;
    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return CallTemp the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{_call_temp}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        return array(
            array('id, call_uuid, call_uuid_extension, extension, user_id, agent_id, province_id, direction, call_status, start_time, answer_time, end_time, total_duration, bill_duration, created_date', 'safe'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        return array(
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
            return array(
                'id' => 'ID',
                'call_uuid' => 'Call Uuid',
                'call_uuid_extension' => 'Call Uuid Extension',
                'extension' => 'Extension',
                'user_id' => 'User',
                'agent_id' => 'Agent',
                'province_id' => 'Province',
                'direction' => 'Direction',
                'call_status' => 'Call Status',
                'start_time' => 'Start Time',
                'answer_time' => 'Answer Time',
                'end_time' => 'End Time',
                'total_duration' => 'Total Duration',
                'bill_duration' => 'Bill Duration',
                'created_date' => 'Created Date',
            );
    }

    public function search()
    {
        $criteria=new CDbCriteria;
        $criteria->compare('t.call_uuid',$this->call_uuid);
        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
            'pagination'=>array(
                'pageSize'=> Yii::app()->user->getState('pageSize',Yii::app()->params['defaultPageSize']),
            ),
        ));
    }
    public function getCallNumber() {
        if(!in_array($this->caller_number, Call::getListExtAgent(null))){
            return '0'.$this->caller_number;
        }
        return $this->caller_number;
    }
    
    /**
     * @Author: ANH DUNG Mar 12, 2017
     * @Todo: Save all Event of one call
     */
    public function saveEvent() {
        $this->saveEventSetParams();
        $this->save();
        $this->handleEventCdr();
        $this->handleEventMissCall();
        $this->makeSocketNotify();
        $this->makeSocketNotifyMonitor();
    }
    
    public function saveEventSetParams() {
        $this->call_uuid            = isset($_GET['calluuid']) ? $_GET['calluuid'] : 0;
        $this->parent_call_uuid     = isset($_GET['parentcalluuid']) ? $_GET['parentcalluuid'] : 0;
        $this->child_call_uuid      = isset($_GET['childcalluuid']) ? $_GET['childcalluuid'] : 0;
        $this->direction            = isset($_GET['direction']) ? $_GET['direction'] : 0;
        $this->caller_number        = isset($_GET['callernumber']) ? $_GET['callernumber'] : 0;
        $this->destination_number   = isset($_GET['destinationnumber']) ? $_GET['destinationnumber'] : 0;
        // 1:Start, 2:Dialing, 3:DialAnswer, 4:HangUp, 5:CDR, 6:TRIM, 7: MissCall
        $this->call_status          = isset($_GET['callstatus']) ? $_GET['callstatus'] : 0;
        $this->saveEventConvertCallStatus();
        $this->start_time           = isset($_GET['starttime']) ? $_GET['starttime'] : null;
        $this->answer_time          = isset($_GET['answertime']) ? $_GET['answertime'] : null;
        $this->end_time             = isset($_GET['endtime']) ? $_GET['endtime'] : null;
        $this->total_duration       = isset($_GET['totalduration']) ? $_GET['totalduration'] : 0;
        $this->bill_duration        = isset($_GET['billduration']) ? $_GET['billduration'] : 0;
        $this->monitor_filename     = isset($_GET['monitorfilename']) ? $_GET['monitorfilename'] : '';
        $this->did                  = isset($_GET['dnis']) ? $_GET['dnis'] : '';
        
        $this->savePutCdr();
        $this->setMissCall();
    }
    
    /**
     * @Author: ANH DUNG Apr 06, 2017
     * @Todo: set Event HangUp Miss Call
     */
    public function setMissCall() {
//        if($this->call_status == Call::STATUS_HANGUP){
//        if($this->call_status == Call::STATUS_TRIM ){// Old Jun 13, 2017 mới add $this->call_status == Call::STATUS_CDR
        if($this->call_status == Call::STATUS_TRIM || $this->call_status == Call::STATUS_CDR){
            $mCallAnswer = $this->getRecordAnswer();
            if(is_null($mCallAnswer)){
                $this->call_status = Call::STATUS_MISS_CALL;
            }else{
                $this->modelAnswer = $mCallAnswer;
            }
        }
    }
    
    /** @Author: ANH DUNG Aug 09, 2018
     *  @Todo: save Event PutCDR
     */
    public function savePutCdr() {
        if($this->call_status != Call::STATUS_PUT_CDR){
            return ;
        }
//        die;// Aug1018 ko bắt sự kiện này nữa
//        Logger::WriteLog("Call STATUS_TEXT_PUT_CDR $this->call_uuid");
        $mCall = new Call();
        $mCall->call_uuid = $this->call_uuid;
        $mCallOld = $mCall->getByCallUuid();
        if(!empty($mCallOld)){
            $this->sendSpjResponse();
            die; // nếu có rồi thì không insert nữa
        }

        $aFieldNotCopy = array('id');
        // handle add promotion of user
        MyFormat::copyFromToTable($this, $mCall, $aFieldNotCopy);
        $mCall->save();
        $this->sendSpjResponse();
        die;// không xử lý tiếp nữa
    }
    
    /** @Author: ANH DUNG Aug 19, 2018
     *  @Todo: gửi return by HTTP status codes 200 để không bị gửi PUT_CDR lại nữa
     **/
    public function sendSpjResponse() {
        static $code = 200;
        header('X-PHP-Response-Code: '.$code, true, $code);
        return ;
        // For 4.3.0 <= PHP <= 5.4.0
        if (!function_exists('http_response_code'))
        {
            function http_response_code($newcode = NULL)
            {
                static $code = 200;
                if($newcode !== NULL)
                {
                    header('X-PHP-Response-Code: '.$newcode, true, $newcode);
                    if(!headers_sent())
                        $code = $newcode;
                }       
                return $code;
            }
        }
    }
    
    /**
     * @Author: ANH DUNG Mar 13, 2017
     * @Todo: đổi call_status từ chữ sang số // 1:Start, 2:Dialing, 3:DialAnswer, 4:HangUp, 5:CDR, 6:TRIM
     */
    public function saveEventConvertCallStatus() {
        $aStatusText = Call::model()->getArrayTextStatusCall();
        $this->call_status = isset($aStatusText[$this->call_status]) ? $aStatusText[$this->call_status] : 0;
        
        $aDirectionMap = Call::model()->getArrayDirectionMap();
        $this->direction = isset($aDirectionMap[$this->direction]) ? $aDirectionMap[$this->direction] : 0;
    }
    
    /**
     * @Author: ANH DUNG Mar 19, 2017
     * @Todo: Xử lý TRIM: Event này yêu cầu External CRM xoá log của 1 cuộc gọi ACD-Call tạm thời (sau khi được merge với cuộc gọi gốc)
     * xử lý gộp tất cả các event vào 1 record Call khi kết thúc call
     */
    public function handleEventCdr() {
//        $aStatusAllow = [Call::STATUS_HANGUP, Call::STATUS_MISS_CALL];
        $aStatusAllow = [Call::STATUS_CDR];
        if( !in_array($this->call_status, $aStatusAllow)  || empty($this->call_uuid)){
            return ;// 1:Start, 2:Dialing, 3:DialAnswer, 4:HangUp, 5:CDR, 6:TRIM, 7: MissCall
        }
        $models = $this->getByCalluuid();
        if(count($models) < 1){
            return ;
        }
        $mCall = new Call();
        foreach($models as $item){
            if(!empty($item->customer_id)){
                $mCall->customer_id = $item->customer_id;
            }
            if(!empty($item->agent_id)){
                $mCall->agent_id = $item->agent_id;
            }
            switch ($item->call_status) {
                case Call::STATUS_START:
                    $mCall->call_uuid           = $item->call_uuid;
                    $mCall->direction           = $item->direction;
                    $mCall->did                 = $item->did;
                    break;
                case Call::STATUS_DIALING:
//                    $mCall->parent_call_uuid    = $item->parent_call_uuid;
                    if($item->direction == Call::DIRECTION_OUTBOUND){
                        $mCall->direction       = $item->direction;
                        $mCall->call_uuid       = $item->call_uuid;// Add Jun2518
                        $mCall->direction       = $item->direction;
                        $mCall->did             = $item->did;
                    }
                    $mCall->caller_number       = $item->caller_number;
                    $mCall->destination_number  = $item->destination_number;
                    $mCall->user_id             = $item->user_id;
                    break;
                case Call::STATUS_DIAL_ANSWER:
//                    $mCall->child_call_uuid     = $item->child_call_uuid;
                    if($item->direction == Call::DIRECTION_INBOUND){
                        $mCall->caller_number       = $item->caller_number;
                    }
                    $mCall->destination_number  = $item->destination_number;
                    $mCall->sell_id             = $item->sell_id;
                    break;
                case Call::STATUS_HANGUP:
                case Call::STATUS_MISS_CALL: 
                    $mCall->call_status         = $item->call_status;
                    break;
                case Call::STATUS_CDR:
                    $mCall->start_time          = $item->start_time;
                    $mCall->answer_time         = $item->answer_time;
                    $mCall->end_time            = $item->end_time;
                    $mCall->total_duration      = $item->total_duration;
                    $mCall->bill_duration       = $item->bill_duration;
                    $mCall->monitor_filename    = $item->monitor_filename;
                    if(empty($mCall->call_status)){
                        $mCall->call_status     = Call::STATUS_HANGUP;
                    }
                    break;
                default:
                    break;
            }
        }
//        $mCall->user_id = MyFormat::getCurrentUid();// Sai, không thể lấy dc uid đang login ở đây vì nó đang ở console
        $mCall->save();
        $mSell = $mCall->rSell;
        if($mSell){
            $mCall->updateCallIdToSell($mSell);// fix Now1218
        }
    }
    /**
     * @Author: ANH DUNG Apr 07, 2017
     * @Todo: Xử lý TRIM: Event này yêu cầu External CRM xoá log của 1 cuộc gọi ACD-Call tạm thời (sau khi được merge với cuộc gọi gốc)
     * xử lý gộp tất cả các event vào 1 record Call khi kết thúc call
     * Xu ly cuoc goi MissCall
     */
    public function handleEventMissCall() {
        $aStatusAllow = [Call::STATUS_MISS_CALL];
        if( !in_array($this->call_status, $aStatusAllow)  || empty($this->call_uuid)){
            return ;// 1:Start, 2:Dialing, 3:DialAnswer, 4:HangUp, 5:CDR, 6:TRIM, 7: MissCall
        }
        $models = $this->getByCalluuid();
        if(count($models) < 1){
            return ;
        }
        $mCall = new Call();
        foreach($models as $item){
            if(!empty($item->customer_id)){
                $mCall->customer_id = $item->customer_id;
            }
            if(!empty($item->agent_id)){
                $mCall->agent_id = $item->agent_id;
            }
            switch ($item->call_status) {
                case Call::STATUS_START: // ko co khi CallOut
                    $mCall->call_uuid           = $item->call_uuid;
                    $mCall->direction           = $item->direction;
                    $mCall->did                 = $item->did;
                    break;
                case Call::STATUS_DIALING:
//                    $mCall->call_uuid           = $item->call_uuid;// call_uuid sai nếu là miss call
                    $mCall->direction           = $item->direction;
                    $mCall->call_uuid    = $item->parent_call_uuid;
                    if($item->direction == Call::DIRECTION_OUTBOUND){
                        $mCall->direction       = $item->direction;
                        $mCall->call_uuid           = $item->call_uuid;// Add Jun2518
                        $mCall->direction           = $item->direction;
                        $mCall->did                 = $item->did;
                    }
                    $mCall->caller_number       = $item->caller_number;
                    $mCall->destination_number  = $item->destination_number;
                    $mCall->user_id             = $item->user_id;
                    $mCall->sell_id             = $item->sell_id;
                    break;
                case Call::STATUS_DIAL_ANSWER:// MissCall không có sự kiện Answer
                    break;
                case Call::STATUS_HANGUP:
                    $mCall->call_status         = $item->call_status;
                    break;
                case Call::STATUS_MISS_CALL:// Jun2518 move line down 
                case Call::STATUS_CDR:// ko có event này vì nó đã bị TRIM rồi
                    $mCall->start_time          = $item->start_time;
                    $mCall->answer_time         = $item->answer_time;
                    $mCall->end_time            = $item->end_time;
                    $mCall->total_duration      = $item->total_duration;
                    $mCall->bill_duration       = $item->bill_duration;
                    $mCall->monitor_filename    = $item->monitor_filename;
                    break;
                default:
                    break;
            }
        }
//        $mCall->user_id = MyFormat::getCurrentUid();// Sai, không thể lấy dc uid đang login ở đây vì nó đang ở console
        $mCall->save();
    }
    
    /**
     * @Author: ANH DUNG Mar 12, 2017
     * @Todo: get all record by calluuid
     */
    public function getByCalluuid() {
        $criteria = new CDbCriteria();
        $criteria->addCondition("t.call_uuid='$this->call_uuid' OR t.parent_call_uuid='$this->call_uuid' ");
        $criteria->order = 't.id ASC';
        return self::model()->findAll($criteria);
    }
    
    /**
     * @Author: ANH DUNG Mar 13, 2017
     * @Todo: Những event của cuộc gọi đến sẽ gửi đến cho ext
     */
    public function makeSocketNotify() {
        if($this->direction == Call::DIRECTION_OUTBOUND 
                || !in_array($this->call_status, Call::model()->getEventNotify())){
            return ;
        }// ko xử lý gọi ra
        if($this->modelAnswer && $this->call_status == Call::STATUS_MISS_CALL){
            return;// không gửi event MissCall xuống với cuộc gọi có answer
        }
        if($this->call_status == Call::STATUS_CDR){
            $this->call_status = Call::STATUS_HANGUP;
        }
        
        // những cuội gọi đến cho những ext Nhân Viên CallCenter thì mới xử lý
//        $mUser = new Users();// vì hàm yêu cầu model User nên phải new
//        if(!in_array($this->destination_number, Call::getListExtAgent($mUser))){
//            return ;
//        }// Apr 06, 2017 chỗ này đang check sai, even Hangup không gửi về nên đóng lại 
        
// public static function addMessage($obj_id, $sender_id, $userId, $code, $message, $json, $needMore)
        $toUserId = $this->getUserIdOfExtOnline();
        $message = $this->caller_number;
        if($this->modelCallTemp){
            $message = $this->modelCallTemp->getCallNumber();
//            $message = trim($message, '0');
//            $message = '0'.$message;
        }
        if($message < 1){
            return ;
        }
        $json = ['call_uuid' => $this->call_uuid, 'parent_call_uuid' => $this->parent_call_uuid, 
            'child_call_uuid' => $this->child_call_uuid, 'call_temp_id' => $this->id,'call_status' => $this->call_status];
        $needMore = [];

        // với notify call event sẽ gửi luôn và set need_sync == 0, biến $obj_id sẽ là call_status để trả xuống view biết dc event này là gì:  2:Dialing, 3:DialAnswer, 4:HangUp
        GasSocketNotify::addMessage(0, GasConst::UID_ADMIN, $toUserId, GasSocketNotify::CODE_CALL_EVENT, $message, $json, $needMore);
    }
    
    /** @Author: ANH DUNG Jun 13, 2017
     * @Todo: Đưa tất cả cuộc gọi xuống NV monitor
     * với Event start thì sẽ căn cứ vào queue để gửi đến tổng đài nào: 2000 OR 2001
     * từ Event sau gửi hết xuống, và sẽ căn cứ vào các call_uuid để xử lý
     */
    public function makeSocketNotifyMonitor() {
        if($this->direction == Call::DIRECTION_OUTBOUND 
                || !in_array($this->call_status, Call::model()->getEventNotifyMonitor())){
            return ;
        }// ko xử lý gọi ra
        if($this->modelAnswer && $this->call_status == Call::STATUS_MISS_CALL){
            return;// không gửi event MissCall xuống với cuộc gọi có answer
        }
//        if($this->call_status == Call::STATUS_CDR){
//            $this->call_status = Call::STATUS_HANGUP;
//        }
        
        $aUserId = $this->getArrayUserIdOfExt($this->destination_number);
        $message = $this->getMessagePhoneNumber();
        if($this->modelCallTemp){
            $message = $this->modelCallTemp->getCallNumber();
            $this->call_uuid = $this->modelCallTemp->parent_call_uuid;// xử lý đầy call_uuid xuống monitor thêm hình như cho miss call
        }
        if($message < 1 && $this->call_status != Call::STATUS_MISS_CALL && $this->did != Call::PHONE_NUMBER_ALCOHOL){
            return ;
        }
        $json = ['call_uuid' => $this->call_uuid, 'parent_call_uuid' => $this->parent_call_uuid, 
            'child_call_uuid' => $this->child_call_uuid, 'call_temp_id' => $this->id,'call_status' => $this->call_status, 'destination_number' => $this->destination_number];
        $needMore = [];
        // với notify call event sẽ gửi luôn và set need_sync == 0, biến $obj_id sẽ là call_status để trả xuống view biết dc event này là gì:  2:Dialing, 3:DialAnswer, 4:HangUp
        $aModelSocket = []; // xử lý gửi 1 lần 1 list sự kiện
        foreach($aUserId as $toUserId){
            $aModelSocket[] = GasSocketNotify::addMessage(0, GasConst::UID_ADMIN, $toUserId, GasSocketNotify::CODE_CALL_EVENT_MONITOR, $message, $json, $needMore);
        }
        $mSync = new SyncData(SyncData::SERVER_IO_SOCKET, 'socket/notifyAdd');
        $mSync->notifyAdd($aModelSocket);
    }
    
    /** @Author: DungNT Sep 10, 2019
     *  @Todo: text phone show popup on web
     **/
    public function getMessagePhoneNumber() {
        $aStatusHandle = [Call::STATUS_START];
        $message = $this->caller_number;
        if(!in_array($this->call_status, $aStatusHandle) || $this->did != Call::PHONE_NUMBER_ALCOHOL){
            return $message;
        }
        return $message.' KH cồn';
    }
    
    /** @Author: ANH DUNG Jun 13, 2017
     * @Todo: get list user online to put first event
     */
    public function getArrayUserIdOfExt($queue_number) {
        // Sep1418 chỉ get những user online, không get all như trc nữa
        $mAppCache  = new AppCache();
        $aExt       = $mAppCache->getCacheDecode(AppCache::EXT_EMPLOYEE);
        if(!is_array($aExt)){
            $aExt = [];
        }
        $mCall      = new Call();
        if(in_array($queue_number, $mCall->getArrayQueueHgd())){
            foreach($mCall->getListExtBoMoi() as $ext){
                unset($aExt[$ext]);
            }// ko dc unset 214, 331...333
            return array_values($aExt);
//            $aId1 = $mAppCache->getArrayIdRole(ROLE_CALL_CENTER);// close Sep1418
//            $aId2 = $mAppCache->getArrayIdRole(ROLE_TELESALE);
//            return $aId1 + $aId2;
        }elseif($queue_number==Call::QUEUE_BO_MOI){
            $aUser = [];
            foreach($mCall->getListExtBoMoi() as $ext){
                if(isset($aExt[$ext])){
                    $aUser[] = $aExt[$ext];
                }
            }
            return $aUser;
//            return $mAppCache->getArrayIdRole(ROLE_DIEU_PHOI);// close Sep1418
        }
        return array_values($aExt);
    }
    
    /**
     * @Author: ANH DUNG Mar 13, 2017
     * @Todo: lấy user id của ext dc call đến
     * $this->destination_number is ext number: 201, 202
     */
    public function getUserIdOfExtOnline() {
        $aStatusAllow = [Call::STATUS_HANGUP, Call::STATUS_MISS_CALL];
        if(in_array($this->call_status, $aStatusAllow)){
            $mCallDialing = $this->getRecordDialing();
            if($mCallDialing){
                $this->modelCallTemp = $mCallDialing;
//                Logger::WriteLog("Ext call: $mCallDialing->user_id");
                return $mCallDialing->user_id;
            }
        }else{
            $mAppCache = new AppCache();
            $aExtUser = $mAppCache->getCacheDecode(AppCache::EXT_EMPLOYEE);
            return isset($aExtUser[$this->destination_number]) ? $aExtUser[$this->destination_number] : Call::DEFAULT_EXT_USER_ID;
        }
    }
    
    /**
     * @Author: ANH DUNG Mar 16, 2017
     * @Todo: update customer id for all event
     */
    public function updateCustomerId() {
        $mCustomer  = Users::model()->findByPk($this->customer_id);
        $agent_id   = $mCustomer ? $mCustomer->area_code_id : 0;
        
        $criteria = new CDbCriteria();
        $criteria->compare('call_uuid', $this->call_uuid);
        $aUpdate = array('customer_id' => $this->customer_id, 'agent_id' => $agent_id);
        CallTemp::model()->updateAll($aUpdate, $criteria);
    }
    
    /**
     * @Author: ANH DUNG Apr 06, 2017
     * @Todo: update user_id nào login nghe cuộc gọi đến
     */
    public static function updateUserIdLogin($id) {
        $attributes = ['user_id'=>MyFormat::getCurrentUid()];
        CallTemp::model()->updateByPk($id, $attributes);
    }
    
    /**
     * @Author: ANH DUNG Apr 06, 2017
     * @Todo: với sự kiện HangUp thì phải find ngược lại vào db lấy ra record Dialing 
     * để xác định ext nào đang hangUp để put socket notify xuống
     */
    public function getRecordDialing() {
        $criteria = new CDbCriteria();
        if($this->call_status == Call::STATUS_MISS_CALL){
            $criteria->addCondition("t.call_uuid='$this->call_uuid' AND call_status=".Call::STATUS_DIALING);
        }elseif($this->call_status == Call::STATUS_HANGUP){
            $criteria->addCondition("t.parent_call_uuid='$this->call_uuid' AND call_status=".Call::STATUS_DIALING);
        }
        $criteria->order = 't.id DESC';// để lấy record Dialing mới nhất
        return self::model()->find($criteria);
    }
    /**
     * @Author: ANH DUNG Apr 06, 2017
     * @Todo: với sự kiện HangUp thì phải find ngược lại vào db lấy ra record Answer 
     * nếu không có event Answer thì là MissCall 
     */
    public function getRecordAnswer() {
        $criteria = new CDbCriteria();
        $criteria->addCondition("(t.child_call_uuid='$this->call_uuid' OR t.call_uuid='$this->call_uuid') AND call_status=".Call::STATUS_DIAL_ANSWER);
        return self::model()->find($criteria);
    }
    
    
    /**
     * @Author: ANH DUNG Apr 09, 2017
     * @Todo: cron move những record temp tạo 1 h trước sang history
     */
    public static function cronMoveToHistory() {
//        $minute = 35;$from = time();// Aug0418 chỉnh lên 35 phút thì mới xóa record temp đi, vì Telesale gọi ra có thể là 15 -> 20 phút
        $minute = 720;$from = time();// Aug0418 chỉnh lên 35 phút thì mới xóa record temp đi, vì Telesale gọi ra có thể là 15 -> 20 phút
        $criteria = new CDbCriteria();
        $criteria->addCondition("DATE_ADD(created_date,INTERVAL $minute MINUTE) < NOW()");
        $criteria->limit = 5000;// set thử 5k theo dõi thêm xem có thể tăng lên ko limit không
        $models = CallTemp::model()->findAll($criteria);
        $aRowInsert = []; $aFieldNotSave = ['id']; $getFieldName = true; $sFieldName = '';
        $aIdDelete  = [];
        
        foreach($models as $model){
//            $model->updateSellIdToCall();// Close on Jun 14, 2017 Chưa thấy cần phải update lại Sell Info to Call
            $aIdDelete[] = $model->id;
//            $stringValueTemp = "(";
//            foreach($model->getAttributes() as $field_name=>$field_value){
//                if(!in_array($field_name, $aFieldNotSave)){
//                    $stringValueTemp .= "'".MyFormat::escapeValues($field_value)."',";
//                    if($getFieldName){
//                        $sFieldName .= "`$field_name`,";
//                    }
//                }
//            }
//
//            $stringValueTemp = rtrim($stringValueTemp, ',');
//            $sFieldName = rtrim($sFieldName, ',');
//            $stringValueTemp .= ")";
//
//            $aRowInsert[] = $stringValueTemp;
//            $getFieldName = false;
        }
        
        $tableName = CallTempHistory::model()->tableName();
        // May 07, 2017 Tạm close đoạn insert to CallTempHistory vì thấy không sử dụng dữ liệu này làm gì nên tạm close lại
//        $sql = "insert into $tableName ( $sFieldName ) values ".implode(',', $aRowInsert);
//        if(count($aRowInsert)>0){
//            Yii::app()->db->createCommand($sql)->execute();
//        }
        $total = count($aIdDelete);
        if($total > 0){
            CallTemp::deleteListId($aIdDelete);
        }
        $to = time();
        $second = $to-$from;
//        Logger::WriteLog("CallTemp Check cronMoveToHistory xóa $total record". ' done in: '.($second).'  Second  <=> '.($second/60).' Minutes');
    }
    
    /**
     * @Author: ANH DUNG Apr 09, 2017
     * @Todo: Delete những record đã move sang history
     */
    public static function deleteListId($aIdDelete) {
        $criteria = new CDbCriteria();
        $sParamsIn = implode(',', $aIdDelete);
        $criteria->addCondition("id IN ($sParamsIn)");
        CallTemp::model()->deleteAll($criteria);
    }
    
    /**
     * @Author: ANH DUNG Apr 13, 2017
     * @Todo: update info Sell
     */
    public function updateSellInfo($mSell) {
        $this->sell_id     = $mSell->id;
        $this->agent_id    = $mSell->agent_id;
        $this->province_id = $mSell->province_id;
        $this->customer_id = $mSell->customer_id;
        $this->update(['sell_id', 'agent_id', 'province_id', 'customer_id']);
    }
    
    
    /**
     * @Author: ANH DUNG Apr 13, 2017
     * @Todo: update info Sell
     */
    public function updateSellIdToCall() {
        if(empty($this->sell_id)){
            return ;
        }
        $mSell = Sell::model()->findByPk($this->sell_id);
        if(empty($mSell)){
            return ;
        }
        
        if($this->call_status == Call::STATUS_DIAL_ANSWER){
            $call_uuid = $this->call_uuid;
        }elseif($this->call_status == Call::STATUS_DIALING){
            $call_uuid = $this->parent_call_uuid;
        }
        if(empty($call_uuid)){
            return ;
        }
        
        $criteria = new CDbCriteria();
        $criteria->compare('call_uuid', $call_uuid);
        $aUpdate = array('sell_id' => $this->sell_id,
                'agent_id'=>$mSell->agent_id,
                'province_id'=>$mSell->province_id,
                'customer_id'=>$mSell->customer_id,
            );
        Call::model()->updateAll($aUpdate, $criteria);
    }
}