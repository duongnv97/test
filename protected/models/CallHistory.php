<?php

/**
 * This is the model class for table "{{_call_history}}".
 *
 * The followings are the available columns in table '{{_call_history}}':
 * @property string $id
 * @property string $call_id
 * @property string $agent_id
 * @property integer $province_id
 * @property string $user_id
 * @property string $phone
 * @property integer $status
 * @property string $sell_id
 * @property string $type_call
 * @property string $file_record_name
 * @property string $created_date_client
 * @property string $created_date
 */
class CallHistory extends BaseSpj
{
    const TYPE_HGD          = 1; // Dec 07, 2016 do not remove. is use at: api/default/windowGetCustomerHistory
    const TYPE_DIEU_PHOI    = 2;
    
    const CARDDATA_CALLING  = 1; // Gọi đi
    const CARDDATA_HANDLING = 2;// gọi đến
    const CARDDATA_HANGUP   = 3;// Cúp máy
    const CARDDATA_MISS     = 4;// nhỡ
    
    public function getArrayStatus() {
        return array(
            CallHistory::CARDDATA_CALLING   => 'Gọi đi',
            CallHistory::CARDDATA_HANDLING  => 'Gọi đến',
            CallHistory::CARDDATA_HANGUP    => 'Cúp máy',
            CallHistory::CARDDATA_MISS      => 'Nhỡ',
        );
    }
    public $autocomplete_name, $autocomplete_name_1, $date_from, $date_to;
    
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{_call_history}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        return array(
            array('agent_id', 'required', 'on'=>'WindowCreate'),
            array('id, call_id, agent_id, province_id, user_id, phone, status, sell_id, type_call, file_record_name, created_date_client, created_date', 'safe'),
            array('type_call_test, date_from,date_to', 'safe'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        return array(
            'rAgent' => array(self::BELONGS_TO, 'Users', 'agent_id'),
            'rUser' => array(self::BELONGS_TO, 'Users', 'user_id'),
            'rSell' => array(self::BELONGS_TO, 'Sell', 'sell_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'call_id' => 'Call',
            'agent_id' => 'Đại lý',
            'province_id' => 'Tỉnh',
            'user_id' => 'Người tạo',
            'phone' => 'Phone KH',
            'status' => 'Trạng thái',
            'sell_id' => 'Bán hàng',
            'type_call' => 'ĐL Phân loại',
            'file_record_name' => 'File ghi âm',
            'created_date_client' => 'Ngày tạo',
            'created_date' => 'Created date server',
            'date_from' => 'Từ Ngày',
            'date_to' => 'Đến Ngày',
            'type_call_test' => 'Nghe lại',
        );
    }

    public function getArrNotSuccess() {
        return array(
            GasConst::SELL_HOI_GIA ,
            GasConst::SELL_PHAN_NAN,
            GasConst::SELL_KHONG_CO_QUA,
            GasConst::SELL_GIA_CAO,
            GasConst::SELL_OTHER,
        );
    }
    
    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        $criteria=new CDbCriteria;
        if(!empty($this->agent_id)){
            $criteria->addCondition('t.agent_id='.$this->agent_id);
        }
        if(!empty($this->user_id)){
            $criteria->addCondition('t.user_id='.$this->user_id);
        }
        if(!empty($this->type_call) && $this->type_call != GasConst::SELL_NOT_SUCCESS){
            $criteria->addCondition('t.type_call='.$this->type_call);
        }elseif($this->type_call == GasConst::SELL_NOT_SUCCESS){
            $sParamsIn = implode(',', $this->getArrNotSuccess());
            $criteria->addCondition("t.type_call IN ($sParamsIn)");
        }
        if(!empty($this->status)){
            $criteria->addCondition('t.status='.$this->status);
        }
        if(!empty($this->type_call_test)){
            $criteria->addCondition('t.type_call_test='.$this->type_call_test);
        }
        
        $criteria->compare('t.phone', trim($this->phone), true);
        $criteria->compare('t.file_record_name', trim($this->file_record_name), true);

        $date_from = '';
        $date_to = '';
        if(!empty($this->date_from)){
            $date_from = MyFormat::dateDmyToYmdForAllIndexSearch($this->date_from);
        }
        if(!empty($this->date_to)){
            $date_to = MyFormat::dateDmyToYmdForAllIndexSearch($this->date_to);
        }
        if(!empty($date_from)){
            $criteria->addCondition("DATE(t.created_date_client) >='$date_from'");
        }
        if(!empty($date_to)){
            $criteria->addCondition("DATE(t.created_date_client) <='$date_to'");
        }
        
        $criteria->order = 't.id DESC';

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
            'pagination'=>array(
                'pageSize'=> 30,
            ),
        ));
    }
    
    /**
     * @Author: ANH DUNG Oct 26, 2016
     * @Todo: get model call history by call id of Nguyen
     */
    public static function loadModelById($q) {
        $model = null;
        if(!empty($q->call_id)){
            $model = self::model()->findByPk($q->call_id);
        }
        if(is_null($model)){
            $model = new CallHistory();
        }
        $model->scenario = 'WindowCreate';
        return $model;
    }
    
    /**
    * @Author: ANH DUNG Oct 26, 2016
    * @Todo: map post to model
    */
    public function windowGetPost($q, $mUser, $aCacheAgent) {
        if(isset($aCacheAgent[$q->agent_id])){
            $this->province_id = $aCacheAgent[$q->agent_id]['province_id'];
        }
        $this->call_id          = $q->call_id;
        $this->agent_id         = $q->agent_id;
        $this->user_id          = $q->user_id;
        if(in_array($this->user_id, GasTickets::$UID_DIEU_PHOI)){
            $this->agent_id = 0;
        }

        $this->phone            = $q->phone;
        $this->status           = $q->status;
        $this->sell_id          = $q->order_id;
        $this->type_call        = empty($q->type_call) ? '' : '-'.$q->type_call.'-';
//        $this->formatClientFileName();
        $this->file_record_name = $q->file_record_name;
        $this->created_date_client = $q->created_date;
        if(!empty($this->sell_id)){
            $mSell = Sell::model()->findByPk($this->sell_id);
            if($mSell){
                $this->customer_id      = $mSell->customer_id;
                $this->type_call        = GasConst::SELL_COMPLETE;
            }
        }
        $this->validate();
    }
    
    public function windowSave() {
        if(is_null($this->id)){
            $this->save();
        }else{
            $this->update();
        }
    }
   
    /**
     * @Author: ANH DUNG Oct 26, 2016
     * @Todo: get file record name from client
     */
    public function formatClientFileName() {
        if(empty($this->file_record_name)){
            return ;
        }
        $tmp = explode(utf8_encode('\\'), $this->file_record_name);
        $this->file_record_name = end($tmp);
    }
    
    public function getAgentName() {
//        $mAppCache = new AppCache();
//        $aAgent = $mAppCache->getAgent();
        $aAgent = CacheSession::getListAgent();
        return isset($aAgent[$this->agent_id]) ? $aAgent[$this->agent_id]['first_name'] : '';
    }
    
    
    public function getCreatedDateClient() {
        return MyFormat::dateConverYmdToDmy($this->created_date_client, 'd/m/Y H:i');
    }
    public function getUserCreated() {
        $mUser = $this->rUser;
        if($mUser){
            return $mUser->getFullName(); 
        }
        return '';
    }
    
    public function getSellLink() {
        $mSell = $this->rSell;
        if($mSell){
            $url = Yii::app()->createAbsoluteUrl('admin/sell/view', array('id'=>$this->sell_id));
            return "<a href='$url' target='_blank'>{$mSell->code_no}</a>"; 
        }
        return '';
    }
    public function getStatus() {
        $aStatus = $this->getArrayStatus();
        return isset($aStatus[$this->status]) ? $aStatus[$this->status] : '';
    }
    public function getTypeCall() {
        if(empty($this->type_call)){
            return '';
        }
        $aRes = array();
        $aReason = GasConst::getSellReason(true);
        $aTypeCall = explode('-', trim($this->type_call,'-'));
        foreach($aTypeCall as $type_id){
            $aRes[] = isset($aReason[$type_id]) ? $aReason[$type_id] : '==';
        }
        return implode('<br>', $aRes);
    }
    
    public function getTypeCallTestText() {
        if(empty($this->type_call_test)){
            return '';
        }
        $aReason = GasConst::getSellReason(true);
        return isset($aReason[$this->type_call_test]) ? $aReason[$this->type_call_test] : '';
    }
    
    public function getHtmlTestCall() {
        $testText = '<span class="TypeCallTestText">'.$this->getTypeCallTestText().'</span><br><br>';
        if(!$this->canUpdateTestCall()){
            return $testText;
        }
        $url = Yii::app()->createAbsoluteUrl('admin/callHistory/index', array('id'=>$this->id,'TestCall'=>1));
        $dropdown = CHtml::dropDownList('ChangeAgent', "", 
            GasConst::getSellReason(true), array('class'=>'ChangeAgentValue', 'empty'=>'Select'));
        $html       = '<br>'.$testText.'<a class="ShowChangeAgent" href="javascript:;">Sửa</a>';
        $htmlSave   = "<span class='display_none WrapChangeAgentDropdown'>$dropdown<br><br><a class='SaveChangeAgent' next='$url' href='javascript:;'>Save</a>&nbsp;&nbsp;&nbsp;<a class='CancelChangeAgent' href='javascript:;'>Cancel</a></span>";
        $html       .= $htmlSave;
        
        return $html;
    }
    
    /**
     * @Author: ANH DUNG Nov 16, 2016
     * @Todo: check user can update test call
     */
    public function canUpdateTestCall() {
        $cRole  = MyFormat::getCurrentRoleId();
        $cUid   = MyFormat::getCurrentUid();
        if($cRole == ROLE_ADMIN){
            return true;
        }
        $aUidAllow = array(GasConst::UID_PHUONG_TT);
        if(!in_array($cUid, $aUidAllow)){
            return false;
        }
        $dayAllow = date('Y-m-d');
        $dayAllow = MyFormat::modifyDays($dayAllow, 50, '-');
        // Ngày hiện tại - 10 ngày mà lớn hơn ngày tạo thì không cho phép update
        return MyFormat::compareTwoDate($this->created_date, $dayAllow);
    }
    
    public function handleUpdateTypeCallTest() {
        $this->update(array('type_call_test'));
    }
    /** @Author: Pham Thanh Nghia Oct 12, 2018
     *  @Todo: lấy danh sách lỗi của cuộc gọi
     **/
    public function getViewReasonCallReview(){
        $mCallReview = new CallReview();
        $aReason = $mCallReview->getArrayReason();
        $criteria = new CDbCriteria();
        $criteria->compare("t.call_id",$this->call_id);
        $mCallReviews = CallReview::model()->findAll($criteria);
        $str = "";
        foreach ($mCallReviews as $key => $value) {
            $stt = $key+1;
            $reason = ($aReason[$value->reason]) ? $aReason[$value->reason] : 'none';
            $str .= $stt." - ".$reason."<br>";
        }
        return $str;
    }
    public function getScore(){
        $mCall = Call::model()->findByPk($this->call_id);
        return ($mCall) ? $mCall->getScore() : '';
    }
    
}