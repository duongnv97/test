<?php

/**
 * This is the model class for table "{{_gas_materials_sell}}".
 *
 * The followings are the available columns in table '{{_gas_materials_sell}}':
 * @property integer $id
 * @property integer $agent_id
 * @property integer $materials_id
 * @property integer $qty
 * @property string $price_sell
 * @property string $total_sell
 * @property string $price_root
 * @property string $total_root
 * @property string $sell_month
 * @property string $sell_year
 */
class GasMaterialsSell extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return GasMaterialsSell the static model class
	 */
    public $autocomplete_name;
    public $file_excel;
    
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{_gas_materials_sell}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('agent_id, materials_id, qty, sell_year', 'required','on'=>'create_sell, update_sell'),
			array('agent_id, materials_id, qty', 'numerical', 'integerOnly'=>true),
			array('price_sell, total_sell, price_root, total_root', 'length', 'max'=>16),
			array('sell_month', 'length', 'max'=>2),
			array('sell_year', 'length', 'max'=>4),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, agent_id, materials_id, qty, price_sell, total_sell, price_root, total_root, sell_month, sell_year', 'safe'),
    array('file_excel', 'file', 'on'=>'import_materialssell',
        'allowEmpty'=>false,
        'types'=> 'xls,xlsx',
        'wrongType'=>'Chỉ cho phép tải file xls,xlsx',
        //'maxSize' => ActiveRecord::getMaxFileSize(), // 5MB
        //'tooLarge' => 'The file was larger than '.(ActiveRecord::getMaxFileSize()/1024).' KB. Please upload a smaller file.',
    ),                     
                    
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		 'customer' => array(self::BELONGS_TO, 'Users', 'agent_id'),
		 'materials' => array(self::BELONGS_TO, 'GasMaterials', 'materials_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'agent_id' => 'Đại Lý',
			'materials_id' => 'Vật Tư',
			'qty' => 'SL',
			'price_sell' => 'Giá Bán',
			'total_sell' => 'Doanh Thu',
			'price_root' => 'Giá Vốn',
			'total_root' => 'Tiền Vốn',
			'sell_month' => 'Tháng',
			'sell_year' => 'Năm',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('t.id',$this->id);
		$criteria->compare('t.agent_id',$this->agent_id);
		$criteria->compare('t.materials_id',$this->materials_id);
		$criteria->compare('t.qty',$this->qty);
		$criteria->compare('t.price_sell',$this->price_sell,true);
		$criteria->compare('t.total_sell',$this->total_sell,true);
		$criteria->compare('t.price_root',$this->price_root,true);
		$criteria->compare('t.total_root',$this->total_root,true);
		$criteria->compare('t.sell_month',$this->sell_month,true);
		$criteria->compare('t.sell_year',$this->sell_year,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
            'pagination'=>array(
                'pageSize'=> Yii::app()->user->getState('pageSize',Yii::app()->params['defaultPageSize']),
            ),
		));
	}

    /*
    public function activate()
    {
        $this->status = 1;
        $this->update();
    }

    public function deactivate()
    {
        $this->status = 0;
        $this->update();
    }
	*/

	public function defaultScope()
	{
		return array(
			//'condition'=>'',
		);
	}
        
        public function beforeSave() {
            if($this->isNewRecord)
                $this->user_id_create = Yii::app()->user->id;
            return parent::beforeSave();
        }        
        
        public static function importExcel($model){
            
                Yii::import('application.extensions.vendors.PHPExcel',true);
                $objReader = PHPExcel_IOFactory::createReader("Excel2007");
                
                $objPHPExcel = $objReader->load(@$_FILES['GasMaterialsSell']['tmp_name']['file_excel']);
                $objWorksheet = $objPHPExcel->getActiveSheet();
                $highestRow = $objWorksheet->getHighestRow(); // e.g. 10
                //$highestColumn = $objWorksheet->getHighestColumn(); // e.g 'F'
                $highestColumn = 'G';
                $highestColumnIndex = PHPExcel_Cell::columnIndexFromString($highestColumn);

                $success = 1;
                $aRowInsert=array();
                $aCellError = array();     
                $created_date=date('Y-m-d H:i:s');
                $uid = Yii::app()->user->id;
                $aAgent=  Users::getArrUserForImport(ROLE_AGENT);
				$aMaterial=  GasMaterials::getArrMaterialForImport();

                for ($row = 2; $row <= $highestRow; ++$row)
                {                    
                    
                    $temp_agent_id = trim(trim($objWorksheet->getCellByColumnAndRow(0, $row)->getValue()));
                    if(isset($aAgent[$temp_agent_id]))
                        $agent_id = $aAgent[$temp_agent_id];
                    else{
                        $aCellError[] = ' Dòng '.$row.' Khong co Đại Lý: '.$temp_agent_id;
                        continue;
                    }  
					$temp_material_id = trim(trim($objWorksheet->getCellByColumnAndRow(1, $row)->getValue()));
                    if(isset($aMaterial[$temp_material_id]))
                        $materials_id = $aMaterial[$temp_material_id];
                    else{
                        $aCellError[] = ' Dòng '.$row.' Không có Vật tư mã: '.$temp_material_id;
                        continue;
                    }
					
                    $qty = trim($objWorksheet->getCellByColumnAndRow(2, $row)->getValue());
					$price_sell = round(trim($objWorksheet->getCellByColumnAndRow(3, $row)->getValue()));
					$total_sell = round(trim($objWorksheet->getCellByColumnAndRow(4, $row)->getValue()));
					$price_root = round(trim($objWorksheet->getCellByColumnAndRow(5, $row)->getValue()));
					$total_root = round(trim($objWorksheet->getCellByColumnAndRow(6, $row)->getValue()));
					$sell_month = trim($objWorksheet->getCellByColumnAndRow(7, $row)->getValue());
					$sell_year = trim($objWorksheet->getCellByColumnAndRow(8, $row)->getValue());
                    $aRowInsert[]="(
                        '$agent_id',
                        '$materials_id',
                        '$qty',
						'$price_sell',
						'$total_sell',
						'$price_root',
						'$total_root',
						'$sell_month',
						'$sell_year'   
                        )";
                }
				
            if(count($aCellError)>0){
                $mLog = new GasImportLog();
                $mLog->user_id = $uid;
                $mLog->created_date = $created_date;
                $mLog->json = json_encode($aCellError);
                $mLog->save();
                echo 'Nếu gặp lỗi này hãy giữ nguyên (và chụp ) màn hình và thông báo lại cho Anh Dũng';
                echo '<pre>';
                echo print_r($aCellError);
                echo '</pre >';
                die;
            }				
            $tableName = GasMaterialsSell::model()->tableName();
            $sql = "insert into $tableName (
                            agent_id,
                            materials_id,
                            qty,
                            price_sell,
                            total_sell,
                            price_root,
                            total_root,
                            sell_month,
                            sell_year
                            ) values ".implode(',', $aRowInsert);
            
			
            Yii::app()->db->createCommand($sql)->execute();

            return $aCellError;            
            
            
        }
}