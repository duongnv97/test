<?php

/**
 * This is the model class for table "{{_gas_sales_file_scan_detai}}".
 *
 * The followings are the available columns in table '{{_gas_sales_file_scan_detai}}':
 * @property string $id
 * @property string $file_scan_id
 * @property string $date_sales
 * @property string $file_name
 */
class GasSalesFileScanDetai extends CActiveRecord
{
    public static $AllowFile = 'jpg,jpeg,png';
    public static $aSize = array(
        'size1' => array('width' => 1024, 'height' => 768),
        'size2' => array('width' => 100, 'height' => 80),// thump view
    );
    
    public $aIdNotIn;
    
    public static $pathUpload = 'upload/sales_file_scan';
    
    public static function model($className=__CLASS__)
    {
            return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
            return '{{_gas_sales_file_scan_detai}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
            return array(
                array('id, file_scan_id,date_sales, file_name,aIdNotIn', 'safe'),
                array('file_name', 'file','on'=>'UploadFileScan',
                    'allowEmpty'=>true,
                    'types'=> self::$AllowFile,
                    'wrongType'=> "Chỉ cho phép định dạng file ".self::$AllowFile." .",
                    'maxSize'   => ActiveRecord::getMaxFileSize(),
                    'minSize'   => ActiveRecord::getMinFileSize(),
                    'tooLarge'  =>'The file was larger than '.(ActiveRecord::getMaxFileSize()/1024).' KB. Please upload a smaller file.',
                    'tooSmall'  =>'The file was smaller than '.(ActiveRecord::getMinFileSize()/1024).' KB. Please upload a bigger file.',
                ), 
            );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
            return array(
            );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
            return array(
                    'id' => 'ID',
                    'file_scan_id' => 'File Scan',
                    'date_sales' => 'Date Sales',
                    'file_name' => 'File Name',
            );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
            // Warning: Please modify the following code to remove attributes that
            // should not be searched.

            $criteria=new CDbCriteria;

		$criteria->compare('t.id',$this->id,true);
		$criteria->compare('t.file_scan_id',$this->file_scan_id,true);
		$criteria->compare('t.date_sales',$this->date_sales,true);
		$criteria->compare('t.file_name',$this->file_name,true);

            return new CActiveDataProvider($this, array(
                    'criteria'=>$criteria,
        'pagination'=>array(
            'pageSize'=> Yii::app()->user->getState('pageSize',Yii::app()->params['defaultPageSize']),
        ),
            ));
    }

    public function defaultScope()
    {
            return array(
                    //'condition'=>'',
            );
    }
    
    /**
     * @Author: ANH DUNG Jun 14, 2014 
     * @Todo:validate file submit
     * @Param: $model model GasSalesFileScan
     */
    public static function validateFile($model, $needMore=array()){
        $model->aModelDetail = array();
        $ok=false;
        if(isset($_POST['GasSalesFileScanDetai']['file_name'])  && count($_POST['GasSalesFileScanDetai']['file_name'])){
            foreach($_POST['GasSalesFileScanDetai']['file_name'] as $key=>$item){
                $mFile = new GasSalesFileScanDetai('UploadFileScan');
                $mFile->file_name  = CUploadedFile::getInstance($model->mDetail,'file_name['.$key.']');
                if(!is_null($mFile->file_name)){
                    $ok=true;
                    MyFormat::IsImageFile($_FILES['GasSalesFileScanDetai']['tmp_name']['file_name'][$key]);
                }
                $mFile->validate();
                if($mFile->hasErrors()){
                    $model->addError('mDetail', "File Error");                    
                }
                if(!is_null($mFile->file_name))
                    $model->aModelDetail[] = $mFile;
            }
        }
        if(!$ok && isset($needMore['create'])){
            $model->addError('mDetail', "Bạn Chưa Chọn File Scan Upload ");
        }
    }
    
    
    
    /**
     * @Author: ANH DUNG May 18, 2014 
     * @Todo: May 18, 2014 - saveFileScanDetail
     * @Param: $model model GasFileScan
     */
    public static function saveFileScanDetail($model){
    try {
        set_time_limit(7200);
        $model->aModelDetail = array();
        if(isset($_POST['GasSalesFileScanDetai']['file_name'])  && count($_POST['GasSalesFileScanDetai']['file_name'])){
            foreach($_POST['GasSalesFileScanDetai']['file_name'] as $key=>$item){
                $mFile = new GasSalesFileScanDetai('UploadFileScan');
                $mFile->file_scan_id = $model->id;
                $mFile->date_sales = $model->date_sales;
                $mFile->file_name  = CUploadedFile::getInstance($model->mDetail,'file_name['.$key.']');
                if(!is_null($mFile->file_name)){
                    $mFile->file_name  = self::saveFile($model, $mFile, 'file_name', $key);
                    $mFile->save();
                    self::resizeImage($model, $mFile, 'file_name');
                }
            }
        }
    } catch (Exception $exc) {
        throw new Exception("Có Lỗi Khi Upload File: ". $exc->getMessage());
    }
    }
    
    
    /**
     * May 18, 2014 - ANH DUNG
     * To do: save file 
     * @param: $model GasSalesFileScanDetai
     * @param: $count 1,2,3
     * @return: name of image upload/transactions/property_document
     */
    public static function  saveFile($mGasFileScan, $model, $fieldName, $count)
    {        
        if(is_null($model->$fieldName)) return '';
        $aDate = explode('-', $model->date_sales);
        $pathUpload = GasSalesFileScanDetai::$pathUpload."/$aDate[0]/$aDate[1]/$aDate[2]";
        $ext = $model->$fieldName->getExtensionName();
//        $fileName = date('Y-m-d');
//        $fileName = $fileName."-$mGasFileScan->uid_login-$mGasFileScan->accounting_employee_id-".ActiveRecord::randString().$count.'.'.$ext;        
        $fileName = time().ActiveRecord::randString().$count.'.'.$ext;        
        $imageProcessing = new ImageProcessing();
        $imageProcessing->createDirectoryByPath($pathUpload);
        $model->$fieldName->saveAs($pathUpload.'/'.$fileName);
        return $fileName;
    }
    
    /*
     * @Author: ANH DUNG May 18, 2014
     * To do: resize file scan
     * @param: $mGasFileScan model GasSalesFileScan
     * @param: $model model GasSalesFileScanDetai
     * @param: $fieldName 
     */
    public static function resizeImage($mGasFileScan, $model, $fieldName) {
        $aDate = explode('-', $model->date_sales);
        $pathUpload = GasSalesFileScanDetai::$pathUpload."/$aDate[0]/$aDate[1]/$aDate[2]";
        $ImageHelper = new ImageHelper();     
        $ImageHelper->folder = '/'.$pathUpload;
        $ImageHelper->file = $model->$fieldName;
        $ImageHelper->aRGB = array(0, 0, 0);//full black background
        $ImageHelper->thumbs = self::$aSize;
//        $ImageHelper->createFullImage = true;
        $ImageHelper->createThumbs();
        $ImageHelper->deleteFile($ImageHelper->folder . '/' . $model->$fieldName);        
    }
    
    /*
     * @Author: ANH DUNG May 18, 2014
     * To do: delete file scan
     * @param: $model model user
     * @param: $fieldName is avatar, agent_company_logo
     * @param: $aSize
     */
    public static function RemoveFileOnly($pk, $fieldName) {
        $modelRemove = self::model()->findByPk($pk);
        if (is_null($modelRemove) || empty($modelRemove->$fieldName))
            return;
        $aDate = explode('-', $modelRemove->date_sales);
        $pathUpload = GasSalesFileScanDetai::$pathUpload."/$aDate[0]/$aDate[1]/$aDate[2]";
        $ImageHelper = new ImageHelper();     
        $ImageHelper->folder = '/'.$pathUpload;
        $ImageHelper->deleteFile($ImageHelper->folder . '/' . $modelRemove->$fieldName);
        foreach ( self::$aSize as $key => $value) {
            $ImageHelper->deleteFile($ImageHelper->folder . '/' . $key . '/' . $modelRemove->$fieldName);
        }
    }     
    
    protected function beforeDelete() {
        self::RemoveFileOnly($this->id, 'file_name');
        return parent::beforeDelete();
    }
    
    public static function deleteByFileScanId($file_scan_id){
        $criteria = new CDbCriteria();
        $criteria->compare("t.file_scan_id", $file_scan_id);
        $models = self::model()->findAll($criteria);
        Users::deleteArrModel($models);        
    }
    
    /**
     * @Author: ANH DUNG May 18, 2014
     * @Todo: delete record by file_scan_id and array id not in
     * @Param: $file_scan_id 
     * @Param: $aIdNotIn 
     */
    public static function deleteByNotInId($file_scan_id, $aIdNotIn){
        $criteria = new CDbCriteria();
        $criteria->compare("t.file_scan_id", $file_scan_id);
        $criteria->addNotInCondition("t.id", $aIdNotIn);
        $models = self::model()->findAll($criteria);
        Users::deleteArrModel($models);        
    }     
    
}