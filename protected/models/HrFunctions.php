<?php

/**
 * This is the model class for table "hr_functions".
 *
 * The followings are the available columns in table 'hr_functions':
 * @property integer $id
 * @property string $function
 * @property string $role_id
 * @property string $name
 * @property integer $type_id
 * @property integer $is_per_day
 * @property string $created_date
 * @property string $created_by
 * @property integer $status
 */
class HrFunctions extends CActiveRecord {

    //-----------------------------------------------------
    // Properties
    //-----------------------------------------------------
    public $status_active = DomainConst::DEFAULT_STATUS_ACTIVE;
    public $status_in_active = DomainConst::DEFAULT_STATUS_INACTIVE;
    public $arrInvalidChar = array(
        '$',
    );
    
    //-----------------------------------------------------
    // Constant
    //-----------------------------------------------------
    const LIST_KEYWORD = 'TS|HS';
    const KEYWORD_PARAM = 'TS';
    const KEYWORD_COEFFICIENT = 'HS';
    const KEYWORD_CONDITION = 'IF';

    /** Type of function is count day per day */
    const FUNCTION_TYPE_COUNT = '1';

    /** Type of function is calculate from date to a day (range of date) */
    const FUNCTION_TYPE_RANGE = '0';
    
    public $search_position, $MAX_ID, $old_code_no;

    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return HrFunctions the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return '{{_hr_functions}}';
    }

    /**
     * @return array validation rules for model attributes
     */
    public function rules() {
        return array(
            array('type_id, position_work_list, position_room_list', 'required', 'on' => 'create,update'),
            array('function, name, position_work_list, position_room_list, created_by, status, role_id, type_id, is_per_day', 'safe'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        return array(
            'rUser' => array(self::BELONGS_TO, 'Users', 'created_by'),
            'rParameters' => array(self::MANY_MANY, 'HrParameters', 'gas_gas_one_many_big(one_id, many_id)',
                'condition' => 'rParameters_rParameters.type = ' . GasOneManyBig::FUNCTION_PARAMETER),
            'rCoefficients' => array(self::MANY_MANY, 'HrCoefficients', 'gas_gas_one_many_big(one_id, many_id)',
                'condition' => 'rCoefficients_rCoefficients.type = ' . GasOneManyBig::FUNCTION_COEFFICIENTS),
            'rRole' => array(self::BELONGS_TO, 'Roles', 'role_id'),
            'rType' => array(
                self::BELONGS_TO, 'HrFunctionTypes', 'type_id',
                'on' => 'status != ' . DomainConst::DEFAULT_STATUS_INACTIVE,
            ),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => DomainConst::CONTENT00003,
            'function' => DomainConst::CONTENT10000,
            'role_id' => DomainConst::CONTENT10004,
            'name' => DomainConst::CONTENT10001,
            'created_date' => DomainConst::CONTENT00010,
            'created_by' => DomainConst::CONTENT00054,
            'status' => DomainConst::CONTENT00026,
            'type_id' => DomainConst::CONTENT10006,
            'is_per_day' => DomainConst::CONTENT10021,
            'position_work_list' => 'Bộ phận làm việc',
            'position_room_list' => 'Chức vụ',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return \CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search() {
        $criteria = new CDbCriteria;
        $criteria->compare('t.name', $this->name, true);
        $criteria->compare('t.function', $this->function, true);
        $criteria->compare('t.type_id', $this->type_id);
//        $criteria->compare('t.status', $this->status);
        if (!empty($this->position_work_list)) {
//            $list = implode(",", $this->position_work_list);
//            $criteria->addCondition('t.position_work_list IN (' . $list . ')');
            $criteria->addCondition('t.position_work_list LIKE "%,' . $this->position_work_list . ',%"');
        }
        $sort = new CSort();
        $sort->attributes = array(
            'name' => 'name',
        );
        $sort->defaultOrder = 't.id desc';
        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
            'sort' => $sort,
            'pagination' => array(
                'pageSize' => 50,
            ),
        ));
    }

    //-----------------------------------------------------
    // Parent override methods
    //-----------------------------------------------------
    /**
     * Override before save method
     * @return Parent result
     */
    public function beforeSave() {
        if ($this->isNewRecord) {   // Add
            // Handle created by
            if (empty($this->created_by)) {
                $this->created_by = MyFormat::getCurrentUid();
            }
            // Handle created date
            $this->created_date = CommonProcess::getCurrentDateTime();
        } else {                    // Update
        }
        return parent::beforeSave();
    }

    /**
     * Override before delete method
     * @return Parent result
     */
    public function beforeDelete() {
        GasOneManyBig::deleteOneByType($this->id, GasOneManyBig::FUNCTION_PARAMETER);
        GasOneManyBig::deleteOneByType($this->id, GasOneManyBig::FUNCTION_COEFFICIENTS);
        return parent::beforeDelete();
    }
    
    /**
     * Get list function
     * @return type
     */
    public function loadItems() {
        $criteria = new CDbCriteria;
        $criteria->order = 'id ASC'; // Sắp xếp tăng theo ID để tính lương kỳ 2 trước khi tính 30% công nợ 
        $models   = HrFunctions::model()->findAll($criteria);
//        $models = self::model()->findAll();
        $retVal = [];
        foreach ($models as $value) {
            if ($value->status != DomainConst::DEFAULT_STATUS_INACTIVE) {
                $list_pos = array_filter(explode(",", $value->position_work_list));
                foreach ($list_pos as $position) {
                    $list_room = array_filter(explode(",", $value->position_room_list));
                    foreach ($list_room as $room) {
                        $retVal[$value->type_id][$position][$room][$value->id] = $value;
                    }
                }
            }
        }
        return $retVal;
    }

    /**
     * Get list status
     * @return Array status
     */
    public function getArrayStatus() {
        return array(
            $this->status_active => DomainConst::CONTENT10002,
            $this->status_in_active => DomainConst::CONTENT10003,
        );
    }

    /**
     * Get user's name
     * @return String User's name
     */
    public function getUser() {
        return !empty($this->rUser) ? $this->rUser->first_name : '';
    }

    /**
     * Get current status (text)
     * @return String Current status
     */
    public function getStatus() {
        $aStatus = $this->getArrayStatus();
        return isset($aStatus[$this->status]) ? $aStatus[$this->status] : '';
    }

    /**
     * Get created date
     * @return String Created date in format
     */
    public function getCreatedDate() {
        return MyFormat::dateConverYmdToDmy($this->created_date, DomainConst::DATE_FORMAT_11);
    }

    /**
     * Get name of function
     * @return String Function's name
     */
    public function getName() {
        return $this->name;
    }

    /**
     * Get content of function
     * @return String Function's content
     */
    public function getFunction() {
        return $this->function;
    }
    
    /**
     * Get text can understand of function
     * @return String Function's content
     */
    public function getUnderstandingFunction() {
        $aModelParams = isset($this->rParameters) ? $this->rParameters : '';
        $aModelCoefic = isset($this->rCoefficients) ? $this->rCoefficients : '';
        $aParams = [];
        $aCoefic = [];
        $rawF = $this->function;
        $i = 1;
        foreach ($aModelParams as $pr) {
            $rawF = str_ireplace("TS" . $i++, ' [' . $pr->getName() . '] ', $rawF);
            $aParams[] = $pr->getName();
        }
        $i = 1;
        foreach ($aModelCoefic as $ce) {
            $rawF = str_ireplace("HS" . $i++, ' [' . $ce->getName() . '] ', $rawF);
            $aCoefic[] = $ce->getName();
        }
        $rawF = str_ireplace("+", " + ", $rawF);
        $rawF = str_ireplace("-", " - ", $rawF);
        $rawF = str_ireplace("*", " * ", $rawF);
        $rawF = str_ireplace("/", " / ", $rawF);
        $rawF = str_ireplace("  ", " ", $rawF);

        return $rawF;
    }

    /**
     * Get role name
     * @return string Role name
     */
    public function getRole() {
        if (isset($this->rRole)) {
            return $this->rRole->role_name;
        }
        return '';
    }

    /**
     * Get type name
     * @return string Type name
     */
    public function getType() {
        if (isset($this->rType)) {
            return $this->rType->name;
        }

        return '';
    }
    
    public function getCodeNo(){
        return $this->code_no;
    }
    
    public function generateCodeNo(){
        return MyFunctionCustom::getNextId('HrFunctions', 'F'.date('my'), LENGTH_ORDER_BY_YEAR,'code_no');
    }
    
    public function updateEmptyCodeNo(){
        $models = HrFunctions::model()->findAll();
        foreach ($models as  $value) {
            $value->code_no = $this->generateCodeNo();
            $value->save();
        }
    }

    /**
     * Get is_per_day value in text
     * @return type
     */
    public function isPerDayText() {
        if ($this->is_per_day == self::FUNCTION_TYPE_COUNT) {
            return DomainConst::CONTENT10019;
        }
        return DomainConst::CONTENT10020;
    }
    
    public function getIsPerDay(){
        if ($this->is_per_day == self::FUNCTION_TYPE_COUNT) {
            return '&#10004;';
        }
        return '';
    }
    
    public function getPositionWork(){
        $aPositionWork = UsersProfile::model()->getArrWorkRoom();
        $res           = '';
        $list_pos      = explode(",", $this->position_work_list);
        foreach ($list_pos as $pos) {
            $res .= isset($aPositionWork[$pos]) ? $aPositionWork[$pos] . ', ' : '';
        }
        return rtrim($res, ', ');
    }
    
    public function getPositionRoom(){
        $aPositionRoom = UsersProfile::model()->getArrPositionRoom();
        $res           = '';
        $list_room     = explode(",", $this->position_room_list);
        foreach ($list_room as $room) {
            $res .= isset($aPositionRoom[$room]) ? $aPositionRoom[$room] . ', ' : '';
        }
        return rtrim($res, ', ');
    }

    /** @Author: HOANG NAM 26/04/2018
     *  @Todo: delete function
     *  @Param: 
     * */
    public function deleteFunction($arrIdFunction) {
        foreach ($arrIdFunction as $key => $id) {
            $model = HrFunctions::model()->findByPk($id);
            if ($model !== null) {
                GasOneManyBig::deleteOneByType($id, GasOneManyBig::FUNCTION_PARAMETER);
                GasOneManyBig::deleteOneByType($id, GasOneManyBig::FUNCTION_COEFFICIENTS);
                if (!$model->delete()) {
                    return false;
                }
            }
        }
        return true;
    }

    /**
     * Check list variables was defined
     * @param Array $arrVariables Array variables to check
     * @param Array $output List variables was missing (output)
     * @return boolean True if all variables was defined, False otherwise
     */
    public function issetVariables($arrVariables, &$output) {
        $allVariable = get_defined_vars();
        // Loop for all variables
        foreach ($arrVariables as $value) {
            if (!array_key_exists(strtoupper($value), $allVariable)) {
                $output[] = $value;
            }
        }

        // If list result is empty => All variables is valid
        if (!empty($output)) {
            return true;
        }

        return false;
    }

    /**
     * Get array variables name
     * @param String $strFunc Function content
     * @return array
     */
    public function getArrayVariablesName() {
        $retVal = array();
        $matches = array();
        preg_match_all('/(' . self::LIST_KEYWORD . ')\w+/', strtoupper($this->function), $matches);
        if (isset($matches[0])) {
            foreach ($matches[0] as $value) {
                if (!in_array($value, $retVal)) {
                    $retVal[] = $value;
                }
            }
        }
        return $retVal;
    }

    /**
     * Get parameter model by index
     * @param Int $index Index of parameter
     * @return Paramter model if exist, NULL otherwise
     */
    public function getParamModel($index) {
        if ($index <= 0) {
            return NULL;
        }
        if (isset($this->rParameters)) {
            if (isset($this->rParameters[$index - 1])) {
                return $this->rParameters[$index - 1];
            }
        }
        return NULL;
    }

    /**
     * Get coefficient model by index
     * @param Int $index Index of coefficient
     * @return Coefficient model if exist, NULL otherwise
     */
    public function getCoefficientModel($index) {
        if ($index <= 0) {
            return NULL;
        }
        if (isset($this->rCoefficients)) {
            if (isset($this->rCoefficients[$index - 1])) {
                return $this->rCoefficients[$index - 1];
            }
        }
        return NULL;
    }

    /**
     * Convert function content to value
     * @param String $from Date from (format is DATE_FORMAT_4 - 'Y-m-d')
     * @param String $to Date to (format is DATE_FORMAT_4 - 'Y-m-d')
     * @return int Function with value inside
     */
    public function convertFunctionToValue($from, $to, $mUser) {
        $arrValues = array();
        // Get list variable
        $arrVariables = $this->getArrayVariablesName();
        // List list value of parameter
        foreach ($arrVariables as $value) {
            $model = NULL;
            if (strpos($value, self::KEYWORD_PARAM) !== false) {
                $index = str_replace(self::KEYWORD_PARAM, '', $value);
                $model = $this->getParamModel($index);
                if (isset($model)) {
                    if($mUser->usingParam == null){
                        $mUser->usingParam = [];
                    }
                    // Save list using parameter in current user
                    if (!in_array($model, $mUser->usingParam)) {
                        $mUser->usingParam[] = $model;
                    }
                    
                    $arrValues[$value] = $model->getValue($from, $to, $mUser);

                    }
            } else if (strpos($value, self::KEYWORD_COEFFICIENT) !== false) {
                $index = str_replace(self::KEYWORD_COEFFICIENT, '', $value);
                $model = $this->getCoefficientModel($index);
                if (isset($model)) {
//                    $arrValues[$value] = $model->getValue($mUser);
                    $arrValues[$value] = $model->getRawValue();
//                    Logger::WriteLog('KEYWORD_COEFFICIENT ' . get_class($model) . '. Value is ' . $arrValues[$value]);
                }
            }
        }
        // TODO: Start calculate result
        $function = $this->normalizationFunction($this->function, 'retVal');
        foreach ($arrValues as $key => $value) {
            $function = str_replace($key, $value, $function);
        }
        return $function; //$retVal = (60>=80?'CT':(60>=80/2?'0.5CT':0));
    }

    /**
     * Get value of function
     * @param String $from Date from (format is DATE_FORMAT_4 - 'Y-m-d')
     * @param String $to Date to (format is DATE_FORMAT_4 - 'Y-m-d')
     * @return int Value of function
     */
    public function getValue($from, $to, $mUser) {
//        Logger::WriteLog('Get value from [' . $from . '] to [' . $to . ']');
        $retVal = 0;
        if ($this->is_per_day == self::FUNCTION_TYPE_RANGE) {
//            Logger::WriteLog('Tinh theo thang: Get function value from [' . $from . '] to [' . $to . ']');
            $function = $this->convertFunctionToValue($from, $to, $mUser);
            $retVal = $this->calculateFunction($function);
        } else {
//            Logger::WriteLog('Tinh theo ngay: Get count value from [' . $from . '] to [' . $to . ']'); 
            $retVal = $this->getCountValue($from, $to, $mUser);
        }

        return (empty($retVal) ? 0 : $retVal);
    }

    /**
     * Get value of function (for timesheet type)
     * @param String $from Date from (format is DATE_FORMAT_4 - 'Y-m-d')
     * @param String $to Date to (format is DATE_FORMAT_4 - 'Y-m-d')
     */
    public function getCountValue($from, $to, $mUser) {
        $retVal = 0;
        $begin = new DateTime($from);
        $end = (new DateTime($to))->modify('+1 day');
        $interval = DateInterval::createFromDateString('1 day');
        $period = new DatePeriod($begin, $interval, $end);
        $total = 0;
        // Loop for all date between range - Check date by date
        foreach ($period as $dt) {
            $dateValue = $dt->format(DomainConst::DATE_FORMAT_4);
            $function = $this->convertFunctionToValue($dateValue, $dateValue, $mUser);
            $retVal = $this->calculateFunction($function);
            $total += $retVal;
        }

        return $total;
    }
    
    public function calculateFunction($function) {
        $function = str_replace('--', '+', $function);
        $retVal = 0;
        set_error_handler(function () {
            throw new Exception($function);
        });

        try {
//            Logger::WriteLog('Function is ' . $function);
            eval($function);
        } catch( Exception $e ){
            $visibleFunc = str_replace('$retVal = ',"", $function);
            $visibleFunc = str_replace(';',"", $visibleFunc);
//            Logger::WriteLog("Có lỗi chia cho 0!" . html_entity_decode('\n')
//                . $this->name . html_entity_decode('\n')
//                . $visibleFunc);
//            echo "<script>alert('Có lỗi chia cho 0!".html_entity_decode('\n')
//                . $this->name . html_entity_decode('\n')
//                . $visibleFunc."')</script>";
        }

        restore_error_handler();
        return $retVal;
    }

    /**
     * Normalization function string
     * @param String $strFunction Input function
     * @param String $outputName Name of output variable
     * @return string Function after normalization
     */
    public function normalizationFunction($strFunction, $outputName) {
        $function = strtoupper($strFunction);
        foreach ($this->arrInvalidChar as $value) {
            $function = str_replace($value, '', $function);
        }
        // Insert '$' before keyword
//        $function = preg_replace('/(' . self::LIST_KEYWORD . ')\w+/', '$${0}',
//                $function);
        $result = 0;
        $index = strpos($function, self::KEYWORD_CONDITION, 0);
        while (!($index === false)) {
            $function = $this->replaceStep(array('?'), ';', $function, true, array('limit' => 1), $index);
            $index = strpos($function, self::KEYWORD_CONDITION, $index + 1);
        }
        $function = str_replace(self::KEYWORD_CONDITION, '', $function);
        $function = str_replace(';', ':', $function);
        $strEval = '$' . $outputName . ' = ' . $function . ';';
        return $strEval;
    }

    /**
     * Replace keyword in function to be a calculatable statement in php code
     * @param Array $aStep
     * @param String $strOld
     * @param String $strReplace
     * @param Bool $isLimit
     * @param String $aParam
     * @param Int $after
     * @return String Function after replace
     */
    public function replaceStep($aStep, $strOld, $strReplace, $isLimit = false, $aParam = array(), $after = 0) {
        $limit = 0;
        $ii = 0;
        if (isset($aParam['limit'])) {
            $limit = $aParam['limit'];
        }
        $index = strpos($strReplace, $strOld, $after);
        while (!($index === false)) {
            if ($isLimit) {
                if ($limit <= 0) {
                    break;
                }
                $limit--;
            }
            $strStep = $aStep[$ii];
            $strReplace = substr_replace($strReplace, $strStep, $index, strlen($strOld));
            if (++$ii > (count($aStep) - 1)) {
                $ii = 0;
            }
            $index = strpos($strReplace, $strOld, $after);
        }
        return $strReplace;
    }

    /**
     * Get roles array
     * @return Array
     */
    public function getArrayRoles() {
        $roles = new Roles();
        $objRoles = $roles->getArrayRole(array(''));
        $aRoles = [];
        foreach ($objRoles as $value) {
            $aRoles[$value['id']] = $value['role_name'];
        }
        return $aRoles;
    }

    /**
     * Get all function by role id
     * @param String $role_id Id of role
     * @return Array List function model
     */
    public function getAllFunctionByRole($role_id, $type_id = '') {
        $aFunction = [];
        if (empty($role_id)) {
            return $aFunction;
        }
        $criteria = new CDbCriteria;
        if(!empty($type_id)){
            $criteria->compare('t.type_id', $type_id);
        }
        $criteria->compare('t.role_id', $role_id, true);
        $criteria->order = 't.id ASC';
        $aModel = HrFunctions::model()->findAll($criteria);
        return $aModel;
    }
    
    public function getFuncUpdate() {
        $criteria = new CDbCriteria;
        $criteria->compare('t.type_id', $this->type_id);
//        $criteria->compare('t.position_room', $this->position_room);
        $criteria->compare('t.position_room_list', implode(",",$this->position_room_list));
        $criteria->compare('t.position_work_list', implode(",",$this->position_work_list));
        $criteria->order = 't.id ASC';
        $aModel = HrFunctions::model()->findAll($criteria);
        return $aModel;
    }

    public function getArrayType() {
        return HrFunctionTypes::loadItems();
    }

    /**
     * Create new functions
     * @param type $function
     * @param type $role
     * @param type $name
     * @param type $type
     * @param type $is_per_day
     * @param type $created_by
     * @return \HrFunctions
     */
    public function createFunctions($function, $name, $code_no = "") {
        $mFunctions        = null;
        if(!empty($code_no)){ // Update
            $criteria      = new CDbCriteria;
            $criteria->compare('t.code_no', $code_no);
            $mFunctions    = HrFunctions::model()->find($criteria);
        } else {
            $mFunctions    = new HrFunctions('Create');
            $mFunctions->code_no = $mFunctions->generateCodeNo();
        }
        $list_pos          = implode(",", $this->position_work_list);
        $list_pos_formated = str_pad($list_pos, strlen($list_pos)+2, ",", STR_PAD_BOTH); // Thêm dấu phẩy đầu và cuối list id để search cho chính xác
        $list_room         = implode(",", $this->position_room_list);
        $list_room_formated= str_pad($list_room, strlen($list_room)+2, ",", STR_PAD_BOTH); // Thêm dấu phẩy đầu và cuối list id để search cho chính xác
        $mFunctions->function           = $function;
        $mFunctions->position_work_list = $list_pos_formated;
        $mFunctions->position_room_list = $list_room_formated;
        $mFunctions->name               = $name;
        $mFunctions->type_id            = $this->type_id;
//        $mFunctions->is_per_day         = $is_per_day;
        if ($mFunctions->save()) {
            return $mFunctions;
        }
        return null;
    }

    /**
     * Delete all functions by roles
     * @param type $role_id
     */
    public static function deleteAllFunctionsByRoleId($role_id, $type_id) {
        $role = Roles::model()->findByPk($role_id);
        if ($role) {
            if (isset($role->rFunction)) {
                foreach ($role->rFunction as $function) {
                    if ($function->type_id == $type_id) {
                        $function->delete();
                    }
                }
            }
        }
    }
    
    public function delFuncCrud() {
        $criteria = new CDbCriteria;
        $type_id       = isset($_GET['type_id']) ? $_GET['type_id'] : '';
        $position_work_list = isset($_GET['position_work_list']) ? $_GET['position_work_list'] : '';
        $position_room_list = isset($_GET['position_room_list']) ? $_GET['position_room_list'] : '';
        $criteria->compare('t.type_id', $type_id);
        $criteria->compare('t.position_work_list', $position_work_list);
        $criteria->compare('t.position_room_list', $position_room_list);
        $model = HrFunctions::model()->findAll($criteria);
        foreach($model as $f){
            GasOneManyBig::deleteOneByType($f->id, GasOneManyBig::FUNCTION_PARAMETER);
            GasOneManyBig::deleteOneByType($f->id, GasOneManyBig::FUNCTION_COEFFICIENTS);
            if(empty($this->function)){ // Nếu xóa hết công thức
                $f->delete();
            }
        }
//        HrFunctions::model()->deleteAll($criteria);
    }
    
    /*
     * Xóa công thức (HrFunctions & OneManyBig) bằng array code_no
     */
    public function deleteFunctionByCodeNo($aCodeNo) {
        if(empty($aCodeNo)) return;
        $criteria   = new CDbCriteria;
        $strCodeNo  = implode('","', $aCodeNo);
        $criteria->addCondition('t.code_no IN ("' . $strCodeNo . '")');
        $models     = HrFunctions::model()->findAll($criteria);
        foreach($models as $f){
            GasOneManyBig::deleteOneByType($f->id, GasOneManyBig::FUNCTION_PARAMETER);
            GasOneManyBig::deleteOneByType($f->id, GasOneManyBig::FUNCTION_COEFFICIENTS);
            $f->delete();
        }
    }

    /**
     * Save all functions
     */
    public function saveAll() {
        // Delete all old function
        if($this->scenario == 'update'){
            // Xóa hết tham số, hệ số trong ct cũ rồi thêm mới lại, không xóa công thức
            $this->delFuncCrud();
        }
        $aCurrentCodeNo = [];
        if (!empty($this->function)) {
            $mFunction              = '';
            $aRowInsertParameters   = array();
            $aRowInsertCoefficients = array();
            $typeParam              = GasOneManyBig::FUNCTION_PARAMETER;
            $typeCoe                = GasOneManyBig::FUNCTION_COEFFICIENTS;
            // Loop for all functions
            foreach ($this->function as $item) {
                $func       = isset($item['func']) ? $item['func'] : '';
                $funcName   = isset($item['name']) ? $item['name'] : '';
//                $isPerDay   = isset($item['is_per_day']) ? $item['is_per_day'] : '0';
                $code_no    = isset($item['code_no']) ? $item['code_no'] : '';
                $aCurrentCodeNo[] = empty($code_no) ?: $code_no;
                // Tạo mới hoặc update công thức (nếu có code no thì update)
                $mFunction  = $this->createFunctions($func, $funcName, $code_no);
                if ($mFunction == null) {
                    continue;
                }
                if (isset($item['TS'])) {
                    foreach ($item['TS'] as $key => $idParam) {
                        $aRowInsertParameters[] = "('{$mFunction->id}',
                        '{$idParam}',
                        '{$typeParam}'    
                        )";
                    }
                }
                if (isset($item['HS'])) {
                    foreach ($item['HS'] as $key => $idCoe) {
                        $aRowInsertCoefficients[] = "('{$mFunction->id}',
                        '{$idCoe}',
                        '{$typeCoe}'    
                        )";
                    }
                }
            }
            // Xóa công thức
            if($this->scenario == 'update' && !empty($this->old_code_no)){
                $aCodeNoDelete = array_diff($this->old_code_no, $aCurrentCodeNo);
                $this->deleteFunctionByCodeNo($aCodeNoDelete);
            }
            $tableName = GasOneManyBig::model()->tableName();
            $sql = "insert into $tableName (one_id,
                            many_id,
                            type
                            ) values " . implode(',', array_merge($aRowInsertParameters, $aRowInsertCoefficients));
            if (count($aRowInsertParameters) > 0 || count($aRowInsertCoefficients) > 0) {
                Yii::app()->db->createCommand($sql)->execute();
            }
        }
    }

    /**
     * @author: DuongNV
     * @param type $role_id 
     * @param type $type : salary, efficientcy, timesheets,..
     * @return \CActiveDataProvider
     */
    public function getFunctionsByRole($role_id = 3, $type = '') {
        $criteria = new CDbCriteria;
        $criteria->addCondition('role_id = ' . $role_id);
        if (!empty($type)) {
            $criteria->addCondition('type_id = ' . $type);
        }
        $criteria->order = 't.id ASC';
        
        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
            'pagination' => array(
                'pageSize' => 10,
            ),
        ));
    }

}
