<?php

/**
 * This is the model class for table "{{_users_phone}}".
 *
 * The followings are the available columns in table '{{_users_phone}}':
 * @property string $type
 * @property string $user_id
 * @property string $phone
 */
class UsersPhoneExt2 extends BaseSpj
{
    const TYPE_PRICE_BO_MOI         = 1;// Sử dụng để gửi báo giá qua SMS
    const TYPE_BAOTRI_BO_MOI        = 2;// Sử dụng để gửi bảo trì dinh ky qua SMS
    const TYPE_PAY_DEBIT            = 3;// Jan2218 để gửi khi thu tiền công nợ
    const TYPE_HGD_SMS_POINT        = 4;// Jun0819 để gửi SMS số điểm tích lũy cho KH hộ GĐ - mỗi KH chỉ có 1 số
    //LOC010
    public $autocomplete_name;
    public static function getProvinceSendSMS() {
        return [GasProvince::TP_HCM, GasProvince::TINH_BINH_DUONG, GasProvince::TINH_DONG_NAI, GasProvince::TINH_VUNG_TAU];
    }
    
    /** @Author: DungNT Jun 08, 2019
     *  @Todo: get array agent_id cho phép chạy TYPE_HGD_SMS_POINT
     **/
    public function getArrayAgentRunHgdSms() {
        return [
//            1349407, //  ĐLLK Ayun Pa 2 - Gia Lai
             1600402, // Đại Lý Chợ Lách - Bến Tre
             2068429, // Đại Lý Long Mỹ - Hậu Giang
        ];
    }

    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return UsersPhone the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
            return '{{_users_phone_2}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        return array(
            array('type, role_id, is_maintain, id, user_id, phone', 'safe'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        return array(
            'rUser' => array(self::BELONGS_TO, 'Users', 'user_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
            return array(
                'id' => 'ID',
                'user_id' => 'User',
                'phone' => 'Phone',
            );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        $criteria=new CDbCriteria;
        //LOC010
        if (!empty($this->user_id)) {
            $criteria->addCondition('t.user_id = '. $this->user_id);
        }
        if (!empty($this->type)) {
            $criteria->addCondition('t.type = '. $this->type);
        }
        $criteria->order = 't.id DESC';
        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
            'pagination'=>array(
                'pageSize'=> Yii::app()->user->getState('pageSize',Yii::app()->params['defaultPageSize']),
            ),
        ));
    }
    
    /**
     * @Author: DungNT May 13, 2016
     * @Todo: delete by user
     */
    public function deleteByUser(){
        if(empty($this->user_id)){
            return ;
        }
        $criteria=new CDbCriteria;
        $criteria->compare('user_id', $this->user_id);
        $criteria->compare('type', $this->type);
        UsersPhoneExt2::model()->deleteAll($criteria);        
    }
    
    /**
     * @Author: DungNT Feb 15, 2017 
     * @Todo: save phone of user or customer
     */
    public function savePhone($mUser) {
        $aPrefix = UpdateSql1::getArrayPrefixChangeNotZero();
        $sPhone = trim($this->phone);
        $this->deleteByUser();
        if($sPhone == ''){
            return ;
        }
        $arrPhone = explode('-', $sPhone);
        $aRowInsert=array();
        foreach($arrPhone as $phone){
            if($phone < 1){
                continue;
            }
            $phone = UsersPhone::formatPhoneSaveAndSearch($phone);
            $prefix_code = substr($phone, 0, 3);
            if(isset($aPrefix[$prefix_code])){// Now0818 fix change dau 11 so
                $charRemove = strlen($prefix_code);
                $phone = $aPrefix[$prefix_code].substr($phone, $charRemove);
            }
            $aRowInsert[]="('$this->user_id',
                '$this->type',
                '$mUser->role_id',
                '$mUser->is_maintain',
                '$phone'
                )";
        }
        $tableName = UsersPhoneExt2::model()->tableName();
        $sql = "insert into $tableName (user_id,
                        type,
                        role_id,
                        is_maintain,
                        phone
                        ) values ".implode(',', $aRowInsert);
        if(count($aRowInsert)>0){
            Yii::app()->db->createCommand($sql)->execute();
        }
    }

    /**
     * @Author: DungNT Feb 26, 2017
     * @Todo: get all record phone bò mối còn lấy hàng. Để gửi SMS báo giá hay KM gì đó
     */
    public function getRecordBoMoi() {
        $criteria = new CDbCriteria();
        $criteria->addCondition('t.type=' . UsersPhoneExt2::TYPE_PRICE_BO_MOI);
        $criteria->addCondition('t.user_id IN ('.$this->getSqlCustomerConLayHang().')');
        return UsersPhoneExt2::model()->findAll($criteria);
    }
    // https://stackoverflow.com/questions/12666502/mysql-in-clause-max-number-of-arguments
    public function getSqlCustomerConLayHang() {
        $roleCustomer   = ROLE_CUSTOMER;
        $statusLayHang  = Users::CON_LAY_HANG;
        $sParamsInProvince  = implode(',', UsersPhoneExt2::getProvinceSendSMS());
        $sParamsIn  = implode(',', CmsFormatter::$aTypeIdQuotes);
//        $addLimitBoMoi = "SubQ.province_id IN ($sParamsInProvince) AND SubQ.is_maintain IN ($sParamsIn) AND SubQ.channel_id=$statusLayHang";
        $addLimitBoMoi = "SubQ.is_maintain IN ($sParamsIn) AND SubQ.channel_id=$statusLayHang";
        return "SELECT SubQ.id FROM `gas_users` as SubQ WHERE SubQ.role_id=$roleCustomer AND $addLimitBoMoi";
    }

    /** @Author: DungNT Feb 05, 2018
     *  @Todo: get all customer phone by type for send sms Bảo trì, thu tiền công nợ ...
     *  @return: array phone: [988180386, 1677555666]
     **/
    public function getPhoneOfCustomerByType($type) {
        $criteria = new CDbCriteria();
        $criteria->addCondition("t.user_id=$this->user_id AND t.type=$type" );
        $models = UsersPhoneExt2::model()->findAll($criteria);
        $aRes = [];
        foreach($models as $item){
            $aRes[] = $item->phone;
        }
        return $aRes;
    }
    
    /** @Author: DungNT Jun 08, 2019
     *  @Todo: get one record of type UsersPhoneExt2::TYPE_HGD_SMS_POINT
     **/
    public function getRecordHgdSmsPoint() {
        $criteria = new CDbCriteria();
        $criteria->addCondition("t.user_id=$this->user_id AND t.type=".UsersPhoneExt2::TYPE_HGD_SMS_POINT );
        return UsersPhoneExt2::model()->find($criteria);
    }
    
    /** @Author: DungNT Jun 19, 2019
     *  @Todo: format phone save db
     **/
    public function formatPhoneSave() {
        $this->phone = UsersPhone::formatPhoneSaveAndSearch($this->phone);
    }
    
    /** @Author: DungNT Jun 10, 2019
     *  @param: model $mCustomer
     *  @Todo: check customer can use function HgdSmsPoint
     *  @return: true if ok else is false
     **/
    public function canUseHgdSmsPoint($mCustomer) {
        if(!in_array($mCustomer->area_code_id, $this->getArrayAgentRunHgdSms())){
            return false;
        }
        return true;
    }
     
    /** @Author: DungNT Jun 08, 2019
     *  @Todo: xử lý save số phone để nhắn SMS cộng điểm tích lũy cho KH của 1 số đại lý
     *  @param: $mCustomer model KH
     *  @param: $cRole current role tạo KH - cho phép ROLE_CALL_CENTER, ROLE_EMPLOYEE_MARKET_DEVELOPMENT
     *  @note: 
     *  1. có 2 luồng add
     *     - từ NV tổng đài tạo mới được nhắn tin SMS: admin/ajax/iframeCreateCustomerHgd (/admin/sell/create)
     *     - từ NV PTTT nhập điểm làm việc trên app Gas Service
     *  2. chỉ giới hạn ở 1 số đại lý cho phép chạy chương trình mới đc insert vào
     **/
    public function handleHgdSmsPoint($mCustomer, $cRole) {
        $aRoleAllow = [ROLE_ADMIN, ROLE_MONITORING_MARKET_DEVELOPMENT, ROLE_CALL_CENTER, ROLE_EMPLOYEE_MARKET_DEVELOPMENT];
        if(!$this->canUseHgdSmsPoint($mCustomer) || !in_array($cRole, $aRoleAllow)
        ){
            return ;// check agent allow
        }
//        Logger::WriteLog("debug Role = $cRole run handleHgdSmsPoint customer_id = $mCustomer->id");
        $this->hgdSmsPointDoFlowSave($mCustomer);
    }
    
    
    /** @Author: DungNT Jun 08, 2019
     *  @Todo: save phone KH HGD call or app sử dụng để gửi SMS điểm tích lũy cho KH
     * 1. Chỉ áp dụng với KH gọi điện - call - chưa áp dụng cho KH app
     * 2. check số phone có phải là số di động hợp lệ không
     * 3. check xem đã tồn tại record chưa, nếu có rồi thì không insert nữa
     * 4. cho phép update trong ngày
     * Xử lý nhắn tin 200 KH đại lý Chợ Lách
     *  @Param: $mCustomer model User
     * 
     *  @note_run: 
     *  1. sẽ gọi hàm saveHgdSmsPoint khi tổng đài tạo mới KH Call trên web
     *   chỉ gọi function 1 lần khi bấm tạo KH
     *  2. khi NV hoàn thành đơn hàng thì mới nhắn tin điểm tích lũy cho KH
     * 
     **/
    public function hgdSmsPointDoFlowSave($mCustomer) {
        $phone = $this->hgdSmsPointGetPhoneValid($mCustomer);
        if(empty($phone)){
            return ;
        }
        $this->user_id          = $mCustomer->id;
        $this->phone            = $phone;
        $mOld = $this->getRecordHgdSmsPoint();
        if(!empty($mOld)){// allow update trong ngày
            $mOld->hgdSmsPointDoUpdate($mCustomer, $phone);
            return;
        }
        $this->type             = UsersPhoneExt2::TYPE_HGD_SMS_POINT;
        $this->formatPhoneSave();
        $this->role_id          = $mCustomer->role_id;
        $this->is_maintain      = $mCustomer->is_maintain;
        $this->save();
    }
    
    /** @Author: DungNT Jun 08, 2019
     *  @Todo: only allow update model old trong ngày
     **/
    public function hgdSmsPointDoUpdate($mCustomer, $phone) {
        if(!$this->canUpdateHgdSmsPoint()){
            return ;
        }
        $this->phone            = $phone;
        $this->formatPhoneSave();
        $this->created_date     = $this->getCreatedDateDb();
        $this->update();
    }
    
    /** @Author: DungNT Jun 08, 2019
     *  @Todo: check record customer and type = UsersPhoneExt2::TYPE_HGD_SMS_POINT
     **/
    public function canUpdateHgdSmsPoint() {
        $res = true;
        $temp = explode(' ', $this->created_date);
        if($temp[0] != date('Y-m-d')){
            $res = false;
        }
        return $res;
    }
    
    /** @Author: DungNT Jun 08, 2019
     *  @Todo: get phone valid
     **/
    public function hgdSmsPointGetPhoneValid($mCustomer) {
        $phone = '';
        $temp = explode('-', $mCustomer->phone);
        foreach($temp as $phoneNumber){
            if(!UsersPhone::isValidCellPhone($phoneNumber)){
                continue ;
            }
            $phone = $phoneNumber;
        }
        return $phone;
    }

    /** @Author: LocNV Jun 19, 2019
     *  @Todo:lay ten khach hang
     *  @Param:
     * //LOC010
     **/

    public function getCustomerName() {
        $name = '';
        $mUser = $this->rUser;
        if ($mUser) {
            $name = '<b>'.$mUser->code_bussiness .'</b> - '. $mUser->getFullName();
        }
        return $name;
    }

    /** @Author: LocNV Jun 19, 2019
     *  @Todo: lay dia chi khach hang
     *  @Param:
     * //LOC010
     **/
    public function getAddress() {
        $address = '';
        $mUser = $this->rUser;
        if ($mUser) {
            $address = $mUser->getAddress();
        }
        return $address;
    }

    /** @Author: LocNV Jun 19, 2019
     *  @Todo: lay so dien thoai dang ky
     *  @Param:
     * //LOC010
     **/
    public function getPhone() {
        return '0'.$this->phone;
    }

    /** @Author: LocNV Jun 19, 2019
     *  @Todo:tinh tong so diem tich luy
     *  @Param:
     * //LOC010
     **/
    public function getPoint() {
        $rewardPoint = new RewardPoint();
        $point = $rewardPoint->getPointFromUsersInfo($this->user_id);
        return $point;
    }

    /** @Author: LocNV Jun 19, 2019
     *  @Todo: lay ngay tao khach hang
     *  @Param:
     * //LOC010
     **/
    public function getCustomerCreatedDate() {
        $created_date = '';
        $mUser = $this->rUser;
        if ($mUser) {
            $created_date = $mUser->getCreatedDate();
        }
        return $created_date;
    }
    
//    get array type create phone userphone send sms point
    public function getArrayTypeUpdatePhone(){
        return[
            STORE_CARD_VIP_HGD, 
            STORE_CARD_HGD, 
            STORE_CARD_HGD_CCS,
        ];
    }
    
//  get userphone2 exists  
    public function getListUsersPhone($aUserID,$type){
        $criteria = new CDbCriteria();
        $criteria->addInCondition('t.user_id', $aUserID);
        $criteria->compare('t.type', $type);
        return self::model()->findAll($criteria);
    }
    /** @Author: NamNH Jun 18, 2019
     *  @Todo: set phone customer point
     *  @Param:
     **/
    public function insertPhonePoint(){
//        $this->removeLandline();
        $aAgent = $this->getArrayAgentRunHgdSms();
        $aIsMaintain = $this->getArrayTypeUpdatePhone();
        $criteria = new CDbCriteria();
        $criteria->addInCondition('t.area_code_id', $aAgent);
        $criteria->addInCondition('t.is_maintain', $aIsMaintain);
        $criteria->compare('t.role_id', ROLE_CUSTOMER);
        $aUser = Users::model()->findAll($criteria);
        $aUserID    = CHtml::listData($aUser, 'id', 'id');
        $aPhone     = $this->getListUsersPhone($aUserID,self::TYPE_HGD_SMS_POINT);
        $aRemove    = CHtml::listData($aPhone, 'user_id', 'user_id');
//        Thực hiện thêm dữ liệu số điện thoại
        $cRole = MyFormat::getCurrentRoleId();
        foreach ($aUser as $key => $mCmustomer) {
            if(in_array($mCmustomer->id, $aRemove)){
                continue;
            }
            $mUsersPhoneExt2 = new UsersPhoneExt2();
            $mUsersPhoneExt2->handleHgdSmsPoint($mCmustomer, $cRole);
        }
    }
    
//    Bỏ SDT bàn
    public function removeLandline(){
        error_reporting(1); // var_dump(PHP_INT_SIZE);
        $criteria = new CDbCriteria();
        $criteria->compare('t.type', self::TYPE_HGD_SMS_POINT);
        $criteria->addCondition('t.phone like "2%"');
        $aUsersPhoneExt2 = self::model()->findAll($criteria);
        $aUserId = CHtml::listData($aUsersPhoneExt2, 'user_id', 'user_id');
        $criteriaDelete = new CDbCriteria();
        $criteriaDelete->compare('type', self::TYPE_HGD_SMS_POINT);
        $criteriaDelete->addInCondition('user_id', $aUserId);
        self::model()->deleteAll($criteriaDelete);
//        update need_sms
        $tableName              = RewardPoint::model()->tableName();
        $idList                 = implode(',', $aUserId);
        $sql                    = "UPDATE `$tableName` SET need_sms = ". RewardPoint::NEED_SMS_TRUE .' WHERE customer_id IN ('. $idList .');';
        if(count($aUserId) > 0){
            Yii::app()->db->createCommand($sql)->execute();
        }
    }
}