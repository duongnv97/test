<?php

/**
 * This is the model class for table "{{_code}}". 
 *
 * The followings are the available columns in table '{{_code}}':
 * @property string $id
 * @property integer $type
 * @property string $code
 * @property string $user_id 
 * @property integer $status
 * @property string $sell_id
 * @property string $pttt_customer_id
 * @property integer $is_lock
 */
class SpjCode extends BaseSpj
{
//    public $charset='abcdefghkmnpqrstuvwxyz123456789';
    public $charset='ABCDEFGHJKLMNPQRSTUVWXYZ123456789'; // Jun 08, 2017 đổi chữ hoa cho đẹp
    public $MAX_LENGTH = 5, $limitOneUser = 120, $limitGenMoreCode = 10;
    
    const TYPE_PTTT         = 1;
    const TYPE_UPDATESOON   = 2;
    
    const STATUS_NEW        = 1;
    const STATUS_USED       = 2;
    const STATUS_GO_BACK    = 3;
    
    public $search_province, $autocomplete_name;

    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return SpjCode the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{_code}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        return array(
            array('id, type, code, user_id, status, sell_id, pttt_customer_id, is_lock', 'safe'),
            array('agent_id, search_province, date_from, date_to', 'safe'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        return array(
            'rAgent' => array(self::BELONGS_TO, 'Users', 'agent_id'),
            'rUser' => array(self::BELONGS_TO, 'Users', 'user_id'),
            'rCustomer' => array(self::BELONGS_TO, 'Users', 'pttt_customer_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
            return array(
                'id' => 'ID',
                'type' => 'Loại',
                'code' => 'Code',
                'user_id' => 'Nhân viên',
                'status' => 'Trạng thái',
                'sell_id' => 'Sell',
                'pttt_customer_id' => 'Khách hàng',
                'is_lock' => 'Is Lock',
                'date_delivery'=>'Ngày mua',
                'created_date'=>'Ngày tạo',
                'agent_id'=>'Đại lý',
                'date_from'=>'Từ ngày',
                'date_to'=>'Đến ngày',
                'search_province'=>'Tỉnh'
            );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        $criteria=new CDbCriteria;
        $criteria->compare('t.type',$this->type);
        $criteria->compare('t.code',$this->code);
        $criteria->compare('t.user_id',$this->user_id);
        $criteria->compare('t.status',$this->status);
        $criteria->order = 't.id DESC';
        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
            'pagination'=>array(
                'pageSize'=> Yii::app()->user->getState('pageSize',Yii::app()->params['defaultPageSize']),
            ),
        ));
    }
    
    /**
     * @Author: ANH DUNG Jun 06, 2017
     * @Todo: sinh code theo số nhập vào
     * @param: $type: 1 is TYPE_PTTT sinh giống nhau cho nhiều type
     */
    public function autoGenCode($max, $type) {
        $from = time();
        for($i=0; $i < $max; $i++){
            $code = $this->generateCode($type);
            $this->saveNewCode($code, $type);
        }
        
        $to = time();
        $second = $to-$from;
        $info = $max.' new Code done in: '.($second).'  Second  <=> '.($second/60).' Minutes';
        Logger::WriteLog($info);
    }
    
    /** @Author: ANH DUNG Jun 06, 2017
     * @return: unique code in table SpjCode
     * @to_do: sinh tự động chuỗi có 4 ký tự không trùng nhau và save vào db
     */    
    public function generateCode($type){
        $code       = ActiveRecord::randString($this->MAX_LENGTH, $this->charset);
        $countCode  = SpjCode::model()->count("`code`='$code' AND `type`=$type");
        if($countCode){
            $code = $this->generateCode($type);
            return $code;
        }else{
            return $code;
        }
    }
    
    /** @Author: ANH DUNG Jun 06, 2017 -- ok test check duplicate
     * @Todo: https://stackoverflow.com/questions/854128/find-duplicate-records-in-mysql
     */
    public function sqlCheckDuplicate() {
        /*
         * SELECT t.id, t.code FROM gas_code as t INNER JOIN (SELECT t1.code FROM gas_code as t1 GROUP  BY t1.code HAVING COUNT(t1.id) > 1) dup ON t.code= dup.code;
         * SELECT t.id, t.code, 
             FROM gas_code as t
                INNER JOIN (SELECT t1.code
               FROM   gas_code as t1
               GROUP  BY code
               HAVING COUNT(id) > 1) dup
           ON t.code = dup.code;
         * 
         */
    }
    
    /**
     * @Author: ANH DUNG Jun 06, 2017
     * @Todo: save one record to db
     */
    public function saveNewCode($code, $type) {
        $model = new SpjCode();
        $model->code = $code;
        $model->type = $type;
        $model->save();
        $model = null;
    }
    
    /** kiểm tra user gen code chưa, nếu chưa thì chạy gen code
     * @return: 0 chưa có code, > 0 có rồi
     */
    public function hasCode() {
        $criteria = new CDbCriteria();
        $criteria->addCondition('t.user_id='.$this->user_id);
        return self::model()->count($criteria);
    }
    /** cấp code cho user  */
    public function mapCodeToUser() {
        $criteria = new CDbCriteria();
        $criteria->addCondition("`user_id`=0 AND `type`=$this->type");
        $criteria->limit = $this->limitOneUser;
        $aUpdate = array('user_id' => $this->user_id);
        $count = SpjCode::model()->count($criteria);
        if($count == 0){
            $model = new SpjCode();
            $model->autoGenCode(1000, $this->type);
            sleep(2);
            Logger::WriteLog('FUNCTION mapCodeToUser Gen New 1000 Code to User');
        }
        SpjCode::model()->updateAll($aUpdate, $criteria);
    }
    
    /**
     * @Author: ANH DUNG Jun 06, 2017
     * @Todo: giới hạn 1 số user PTTT được gen code 
     */
    public function getUserAllowGenCode() {
        return [];
    }
    
    /** @Author: ANH DUNG Jun 06, 2017
     * @Todo: Dev sẽ sinh tự động code cho array user PTTT, không cho client làm
     */
    public function devAutoMapCode() {
        $type = SpjCode::TYPE_PTTT;
        foreach($this->getUserAllowGenCode() as $user_id){
            $model = new SpjCode();
            $model->user_id = $user_id;
            $model->type    = $type;
            $model->mapCodeToUser();
            sleep(2);
        }
        echo 'done - need count in table qty record cho 1 user'; die;
    }
    
    /** @Author: ANH DUNG Jun 06, 2017
     * @Todo: count remain code of user
     */
    public function countRemainCode() {
        $criteria = new CDbCriteria();
        $criteria->addCondition('t.user_id='.$this->user_id.' AND t.status=' . SpjCode::STATUS_NEW);
        return self::model()->count($criteria);
    }
    /** @Author: ANH DUNG Jun 09, 2017
     * @Todo: count code go back
     */
    public function countGoBackCode() {
        $criteria = new CDbCriteria();
        $criteria->addCondition('t.user_id='.$this->user_id.' AND t.status=' . SpjCode::STATUS_GO_BACK);
        return self::model()->count($criteria);
    }
    /** @Author: ANH DUNG Jun 10, 2017
     * @Todo: count remain code user
     */
    public function autoGenCodeFromList($mUser) {
        $this->user_id  = $mUser->id;
        $this->type     = SpjCode::TYPE_PTTT;
        $aRoleAllow = [ROLE_EMPLOYEE_MAINTAIN, ROLE_SALE, ROLE_EMPLOYEE_MARKET_DEVELOPMENT, ROLE_MONITORING_MARKET_DEVELOPMENT];
        if( in_array($mUser->role_id, $aRoleAllow) && $this->countRemainCode() < $this->limitGenMoreCode){
            $this->mapCodeToUser();
        }
    }
    /**
     * @Author: ANH DUNG Jan 11, 2017 -- handle listing api
     * @param: $apiController api controller 
     */
    public function handleApiList(&$result, $q, $mUser) {
        $this->user_id = $mUser->id;
        // 1. get list order by user id
        $dataProvider   = $this->ApiListing($q, $mUser);
        $models         = $dataProvider->data;
        $CPagination    = $dataProvider->pagination;
        $result['total_record'] = $CPagination->itemCount;
        $result['total_page']   = $CPagination->pageCount;
        
        if($q->page == 0){
            $summary_text   = 'Bạn còn: '.$this->countRemainCode().' mã';
        }else{
            $summary_text   = '';
        }

        $result['message'] = $summary_text;
        
        if( $q->page >= $CPagination->pageCount ){
            $result['record'] = [];
        }else{
            foreach($models as $model){
                $temp = [];
                $temp['id']              = $model->id;
                $temp['code']            = $model->code;
                $result['record'][]      = $temp;
            }
        }
    }

    public function ApiListing($q, $mUser) {
        $criteria = new CDbCriteria();
        $criteria->addCondition('t.user_id=' . $this->user_id . ' AND t.status=' . SpjCode::STATUS_NEW);
        $criteria->addCondition('t.type=' . SpjCode::TYPE_PTTT);
        $dataProvider=new CActiveDataProvider($this, array(
            'criteria' => $criteria,
            'pagination'=>array(
                'pageSize' => 20,
//                'pageSize' => 5,
                'currentPage' => (int)$q->page,
            ),
          ));
        return $dataProvider;
    }
    
    /**
     * @Author: ANH DUNG Jun 08, 2017
     * @Todo: kiểm tra code hợp lệ với user đang nhập không
     */
    public function getModelCodeUser() {
        if(empty($this->code)){
            return null;
        }
        $criteria = new CDbCriteria();
        $criteria->compare('t.code', $this->code);
        if(is_array($this->status)){
            $sParamsIn = implode(',',  $this->status);
            $criteria->addCondition("t.status IN ($sParamsIn)");
        }else{
            $criteria->addCondition('t.status='. $this->status);
        }
        if(!empty($this->user_id)){
            $criteria->addCondition('t.user_id=' . $this->user_id);
        }
        return self::model()->find($criteria);
    }
    
    /**
     * @Author: ANH DUNG Jun 08, 2017
     * @Todo: When Use Code => xử lý change status + update customer id to codeSPJ
     */
    public function setCustomerIdToCode() {
        $this->status = SpjCode::STATUS_USED;
        $this->update(['pttt_hgd_id', 'pttt_customer_id', 'status']);
    }
    
    /**
     * @Author: ANH DUNG Jun 08, 2017
     * @Todo: Cập nhật sell id và status lại model SpjCode
     */
    public function updateSellIdToSpjCode($mSell) {
        $this->sell_id          = $mSell->id;
        $this->agent_id         = $mSell->agent_id;
        $this->date_delivery    = $mSell->created_date_only;
        $this->status           = SpjCode::STATUS_GO_BACK;
        $this->update();
        $mAgent = $mSell->rAgent; $agentName = '';
        if($mAgent){
            $agentName = $mAgent->first_name;
        }
        $this->titleNotify = "Mã $this->code có bình quay về. $agentName";
        $this->notifyEmployee();
    }
    
    /** Jun 08, 2017
     *  notify cho NV PTT mã BÌNH QUAY Về
     */
    public function notifyEmployee() {
        if(empty($this->titleNotify)){
            $this->titleNotify = "Mã $this->code có bình quay về";
            
        }
//        $this->runInsertNotify([$this->user_id], $this->titleNotify, true); // Send Now
        $this->runInsertNotify([$this->user_id], $this->titleNotify, false); // Send By Cron
    }
    
    /**
     * @Author: ANH DUNG Apr 14, 2017
     * @Todo: insert to table notify
     * @Param: $aUid array user id need notify
     */
    public function runInsertNotify($aUid, $title, $sendNow) {
        $json_var = [];
        foreach($aUid as $uid) {
            if( !empty($uid) ){
                $mScheduleNotify = GasScheduleNotify::InsertRecord($uid, GasScheduleNotify::SPJ_CODE_GOBACK, $this->pttt_customer_id, '', $title, $json_var);
                if($sendNow && !is_null($mScheduleNotify)){
                    $mScheduleNotify->sendImmediateForSomeUser();
                }// Jan 05, 2016 tạm close send luôn lại, vì nó gây chậm bên PMBH khi send
                // sẽ gửi = cron
            }
        }
    }

    /** @Author: ANH DUNG Jun 06, 2017
     * ****** Step setup để run code ******
     * 1. @done: làm api list code + message: Bạn còn 1000 code chưa sử dụng
     * 1/ @done Không save phone vào phone hệ thống nữa - xử lý trong api tạo KH từ app
     * 2/ xóa hết data: 240Vip + Dân Dụng không lấy hàng từ tháng 5-2017 đổ về trước
     * 3/ @done Xử lý save code khi PTTT tạo KH từ APP, xử lý change status + update customer id to codeSPJ
     * 4/ @done Xử lý Save Code KHi NVGN nhập code từ APP + save back sang table SPJ CODE, xử lý update back khi hủy đơn hàng hộ
     * 5/ @done xử lý save + view image khi NVGN up hình trong bán hàng hộ GĐ
     * 6/ xóa hết mã KH hộ + dân dụng, Vip 240
     * 7/ báo cáo bình quay về theo khoảng ngày trên app
     * 
     */
    
    
    
    /** @Author: DuongNV Jan 17,2019
     *  @Todo: List các gas_code có date_delivery = created_date
     **/
    public function getReportCodePttt() {
        $criteria = new CDbCriteria;
        $criteria->addCondition('DATE( t.created_date ) = t.date_delivery');
        if(!empty($this->date_from)){
            $date_from = MyFormat::dateConverDmyToYmd($this->date_from, '-');
            $criteria->addCondition('t.date_delivery >= "'.$date_from.'"');
        }
        if(!empty($this->date_to)){
            $date_to = MyFormat::dateConverDmyToYmd($this->date_to, '-');
            $criteria->addCondition('t.date_delivery <= "'.$date_to.'"');
        }
        $criteria->compare('t.agent_id', $this->agent_id);
        $criteria->order = 't.user_id ASC';
        $mGasAppOrder = new GasAppOrder();
        if(!empty($this->search_province)){
            $mGasAppOrder->obj_id_old = $this->search_province;
            $mGasAppOrder->handleProvince($criteria);
        }
        $_SESSION['data-excel-pttt'] = new CActiveDataProvider($this, array(
            'pagination'=>false,
            'criteria'=>$criteria,
        ));
        $modelsGasCode      = $this->findAll($criteria);
        $aSellId            = [];
        $aData              = [];
        foreach ($modelsGasCode as $value) {
            $aSellId[$value->sell_id] = $value->sell_id;
        }
        $criteriaSell       = new CDbCriteria;
        $criteriaSell->addInCondition('t.id',$aSellId);
        $modelsSell         = Sell::model()->findAll($criteriaSell);
        $aSellCreatedDate   = [];
        foreach ($modelsSell as $value) {
            $aSellCreatedDate[$value->id] = $value->created_date;
        }
        foreach ($modelsGasCode as $value) {
            $aData[$value->id]['model']       = $value;
//            $range       = strtotime($aSellCreatedDate[$value->sell_id]) - strtotime($value->created_date);
            $minute_diff = MyFormat::getMinuteTwoDate($aSellCreatedDate[$value->sell_id], $value->created_date);
//            $aData[$value->id]['time_range']  = $range;
            $aData[$value->id]['minute_diff'] = $minute_diff;
            $aData[$value->id]['order_date']  = MyFormat::dateConverYmdToDmy($aSellCreatedDate[$value->sell_id], "d/m/Y H:i");
        }
//        uasort($aData, function($a, $b){
//            return $a['time_range'] < $b['time_range'];
//        });
        return $aData;
    }
    
    public function getDateDelivery(){
        return MyFormat::dateConverYmdToDmy($this->date_delivery, "d/m/Y H:i");
    }
    
    public function getCode(){
        return $this->code;
    }
    
    public function getUser(){
        return isset($this->rUser) ? $this->rUser->getFullName() : "";
    }
    
    public function getCustomer(){
        return isset($this->rCustomer) ? $this->rCustomer->getFullName() : "";
    }
    
    public function getPhone(){
        return isset($this->rCustomer) ? $this->rCustomer->getPhone() : "";
    }
}