<?php

/**
 * This is the model class for table "{{_spin_campaign_details}}".
 *
 * The followings are the available columns in table '{{_spin_campaign_details}}':
 * @property string $id
 * @property string $campaign_id
 * @property string $user_id
 * @property string $number
 * @property integer $order_number
 * @property integer $rank
 * @property integer $material_id
 * @property integer $qty_material
 * @property integer $status
 */
class SpinCampaignDetails extends BaseSpj
{
    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return SpinCampaignDetails the static model class
     */
    const RANK_SPECIAL      = 9;
    const RANK_FIRST        = 1;
    const RANK_SECOND       = 2;
    const RANK_THIRD        = 3;
    const RANK_KHUYENKHICH  = 4;
    const RANK_DEFAULT      = 5;
    
    const STATUS_NOTGAVE    = 0; 
    const STATUS_GAVE       = 1; 
    const STATUS_CANCEL     = 2; 
    const LIMIT_USER_ONCE   = 1;
    
    public $autocomplete_name, $campaign_name, $qty_award;
    public $searchGas, $autocomplete_gas,$autocomplete_name_5, $material_name, $user_name, $materials_name, $material_id;
    public function getArrayRank(){
        return [
            self::RANK_DEFAULT      => 'Giải mặc định',
            self::RANK_SPECIAL      => 'Giải đặc biệt',
            self::RANK_FIRST        => 'Giải nhất',
            self::RANK_SECOND       => 'Giải nhì',
            self::RANK_THIRD        => 'Giải ba',
            self::RANK_KHUYENKHICH  => 'Giải khuyến khích',
        ];
    }
    public function getArrayStatus(){
        return [
            self::STATUS_NOTGAVE    => 'Chưa trao quà',
            self::STATUS_GAVE       => 'Đã trao quà',
            self::STATUS_CANCEL     => 'Hủy'
        ];
    }
    public static function model($className=__CLASS__)
    {
        return parent::model($className);

    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{_spin_campaign_details}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        return array(
//                array('order_number, rank, status', 'numerical', 'integerOnly'=>true),
//                array('campaign_id, user_id', 'length', 'max'=>11),
//                array('number', 'length', 'max'=>8),
                array('campaign_id, user_id, number, order_number, rank, award_json, status, campaign_name', 'safe', 'on'=>'search'),
                array('qty_award', 'safe'),        
            );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        return array(
            'rCampaign' => array(self::BELONGS_TO, 'SpinCampaigns', 'campaign_id'),
            'rUsers'    => array(self::BELONGS_TO, 'Users', 'user_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
                'id'            => 'ID',
                'campaign_id'   => 'Tên chiến dịch',
                'user_id'       => 'Khách hàng',
                'number'        => 'Số trúng thưởng',
                'order_number'  => 'Order Number',
                'rank'          => 'Giải',
                'status'        => 'Trạng thái',
        );
    }
    public function search()
    {
        $criteria=new CDbCriteria;
        if(!empty($this->campaign_name)){
            $criteria->join = 'left join gas_spin_campaigns c on c.id = t.campaign_id';
            $criteria->addSearchCondition('c.name', $this->campaign_name);
        }
        if(!empty($this->user_id)){
            $criteria->compare('t.user_id',$this->user_id);
        }
        $criteria->compare('t.number',$this->number,true);
        $criteria->addCondition('t.number != 0');
        $criteria->compare('t.rank',$this->rank);
        $criteria->compare('t.status',$this->status);
        $criteria->order = 'created_date DESC';
        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
            'pagination'=>array(
                    'pageSize'=> 10,
                ),
        ));
    }
    
    public function searchWinning($aId)
    {
        $criteria=new CDbCriteria;
        $criteria->compare('t.campaign_id',$aId);
        $criteria->addCondition('number != 0');
        $criteria->order = 'created_date DESC';
        $model = new SpinCampaignDetails();
        return new CActiveDataProvider($model, array(
            'criteria'=>$criteria,
            'pagination'=>array(
                    'pageSize'=> 5,
                ),
        ));
    }
    
    public function getCampaignID(){
        return $this->campaign_id;
    }
    
    public function getCampaignName(){
        return empty($this->rCampaign)?'':$this->rCampaign->getName();
    }
    
    public function getUserName(){
        return empty($this->rUsers) ? '' : $this->rUsers->getFullName();
    }
    
    public function getUser() {
        $str ='';
        if(!empty($this->rUsers)){
            $name = empty($this->rUsers) ? '' : $this->rUsers->getFullName();
        
            $str.='<b>KH: </b><span class="award-kh">'.$name.'</span><br>';
            if (!empty($this->rUsers->phone)){
                $phone = $this->rUsers->phone;
                
                $phoneList = explode('-',$phone);
                $phone =    isset($phoneList[0])?$phoneList[0]:'';      
                $endNumber = substr($phone, -4);
//                $phone = str_replace($endNumber, '***', $phone);
                $phone = '******'.$endNumber;
                $str.='<b>SĐT: </b><span class="award-phone">'.$phone.'</span><br>';
            }
//            if (!empty($this->rUsers->address)){
//                $str.='<b>Địa chỉ: </b>'.$this->rUsers->address.'<br>';
//            }
        } else return '';
        return $str;
    }

    public function getNumber(){
        return empty($this->number) ? '' : $this->number;
    }
    
    public function getRank() {
        $aRank = SpinCampaignDetails::getArrayRank();
        return empty($aRank[$this->rank]) ? '' : $aRank[$this->rank];
    }
    
    public function getStatus(){
        $aStatus = SpinCampaignDetails::getArrayStatus();
        return empty($aStatus[$this->status]) ? '' : $aStatus[$this->status];
    }
    
    public function getStatusIndex(){
        $str = '';
        $aStatus = SpinCampaignDetails::getArrayStatus();
        $str = empty($aStatus[$this->status]) ? '' : $aStatus[$this->status];
        if($this->status == self::STATUS_NOTGAVE){
            $link = Yii::app()->createAbsoluteUrl('admin/spinCampaignDetails/cancelAward', array('id' => $this->id,));
            $actionText = '<span>Hủy</span>';
            $str .= '<br><a class="cancelAward" href="' . $link . '" target="" >' . $actionText . '</a> ';
        }
    return $str;
    }
    
    /** @Author: NamLA Aug 16,2019
    *  @Todo: xóa chi tiết chiến dịch theo id chiến dịch
    *  @Param: campaign_id
    **/
    public static function deleteByRootId($root_id){
        if(empty($root_id)){
            return ;
        }
        $criteria = new CDbCriteria();
        $criteria->compare('campaign_id', $root_id);
        self::model()->deleteAll($criteria);
    }
    
    /** @Author: NamLA Aug 16,2019
    *  @Todo: xóa chi tiết chiến dịch theo id chiến dịch
    *  @Param: campaign_id
    **/
    public function deleteDetail(){
        if(empty($this->campaign_id)){
            return false;
        }
        $criteria = new CDbCriteria();
        $criteria->compare('campaign_id', $this->campaign_id);
        self::model()->deleteAll($criteria);
    }
    
    
    /** @Author: NhanDT Aug 16,2019
    *   @Todo: lưu khách hàng trúng giải 
    *   @Param: mã số chương trình, người và sô trúng giải  
    **/
    public function saveUserWinning($id, $campaign_id, $number, $rank) {
        $number     = (int)$number;
        $mDetail    = SpinCampaignDetails::model()->findByPk($id);
        $oldNum     = empty($mDetail->number) ? 0 : (int)$mDetail->number;
        $mNumber    = new SpinNumbers();
        $mNumber->changeStatusNumber($oldNum);// Khi quay lại giải, chuyển status số cũ lại thành normal
        
        $mNumber->updateUserWinning($number, $rank);
        if (empty($campaign_id) ||  empty($number)){
           return;
        }
        $user_id    = $mNumber->findUser($number);
        $criteria   = new CDbCriteria;
        $criteria->compare('id', $id);
        $criteria->compare('campaign_id',$campaign_id);
        $criteria->compare('rank',$rank);
        $aUpdate    = array('user_id'=>$user_id,'number'=>$number, 'created_date'=>date('Y-m-d H:i:s'));
        SpinCampaignDetails::model()->updateAll($aUpdate, $criteria);
        echo json_encode($mNumber->arrNumberSpin());
   }
   
   /** @Author: NamLA Sep 4,2019
    *   @Todo: lưu khách hàng trúng giải 
    *   @Param: id, mã số chương trình, người và sô trúng giải  
    **/
    public function saveUserWinningTest($id, $campaign_id, $number, $rank) {
       if (empty($campaign_id) ||  empty($number))
           return;
       $mDetail                 = $this>findByPk($id)->attributes;
       $mDetail->user_id        = SpinNumbers::findUser($number);
       $mDetail->campaign_id    = $campaign_id;
       $mDetail->number         = $number;
       $mDetail->rank           = $rank;
   }
   
   /** @Author: NhanDT Aug 21, 2019
    *  @Todo: todo
    *  @Param: param
   **/
   public static function getArrayModelByArrayId($aId){
        $aRes       = array();
        $criteria   = new CDbCriteria;
        $criteria->compare('t.campaign_id',$aId);
        $criteria->addCondition('number != 0');
        $criteria->order = 'created_date ASC';
        $aRes   = array();
        $models = self::model()->findAll($criteria);
        if(count($models)){
            foreach($models as $item){
                $aRes[$item->id] = $item;
            }
        }
        return $aRes;
    }
    
    /** @Author: NhanDT Aug 21, 2019
    *   @Todo: todo
    *   @Param: param
    **/
   public static function getArrayCurrentRankByCampaignId($aId){
        $aRes = array();
        $aRank = SpinCampaignDetails::getArrayRank();
        $criteria=new CDbCriteria;
        $criteria->compare('t.campaign_id',$aId);
        $criteria->addCondition('t.number<=0');
        $criteria->distinct = true;
        $criteria->select = array('t.rank',);
        $models = self::model()->findAll($criteria);
        if(count($models)){
            foreach($models as $item){
                $aRes[$item->rank] = $aRank[$item->rank];
            }
        }
        return $aRes;
    }
    /** @Author: NhanDT Aug 23, 2019
     *  @Todo: lấy ra số lần quay của mỗi giải  
     *  @Param: param
     **/
    public function getNumSpinRank($aId, $rank){
        $criteria=new CDbCriteria;
        $criteria->compare('t.campaign_id',$aId);
        $criteria->compare('t.rank', $rank);
        $criteria->addCondition('t.number!= 0');
        $count = self::model()->count($criteria);
        return $count;
    }
    
    /** @Author: NhanDT Aug 27,2019
     *  @Todo: check trùng số đã trúng thưởng
     *  @Param: param
     **/
    public function checkTwoNumSame($campaign_id, $number) {
        $criteria=new CDbCriteria;
        $criteria->compare('t.campaign_id',$campaign_id);
        $criteria->compare('t.number', $number);
        $count = self::model()->count($criteria);
        return $count;
    }
    
    public function getSummaryTableDetails(){
        $str = '';
        $aMaterials = json_decode($this->award_json, true);
        $str .=     '<table style="border-collapse:collapse;">';
        $str .=         '<thead>';
        $str .=             '<tr>';
        $str .=                 '<td style="border:1px solid black;"><b>Tên</b></td>';
        $str .=                 '<td style="border:1px solid black;"><b>SL</b></td>';
        $str .=             '</tr>';
        $str .=         '</thead>';
        $str .=         '<tbody>';
        if (!empty($aMaterials)){
            foreach($aMaterials as $result){
               $str .=      '<tr>';
               $str .=          '<td style="border:1px solid black;">'.$result['name'].'</td>';
               $str .=          '<td style="border:1px solid black;">'.$result['qty'].'</td>';
               $str .=      '</tr>';
            }
        }
        $str .=         '</tbody>';
        $str .=     '</table>';
        return $str;
    }
    
    /** @Author: NhanDT Aug 29,2019
     *  @Todo: lấy dữ liệu award_json => save in table
     *  @Param: param
     **/
    public function getAwardJson() {
        $aDetail        = $_POST['SpinCampaignDetails'];
        if (empty($aDetail['award_json'])){
            $json = '';
        } else {
            $json = '['.implode(",", $aDetail['award_json']).']';
        }
        $this->award_json = $json;
    }

}