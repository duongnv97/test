<?php

/**
 * This is the model class for table "{{_app_promotion_user}}".
 *
 * The followings are the available columns in table '{{_app_promotion_user}}':
 * @property string $id
 * @property integer $status_use
 * @property string $user_id
 * @property string $promotion_title
 * @property integer $promotion_id
 * @property integer $promotion_amount
 * @property integer $time_use
 * @property string $expiry_date
 * @property string $code_no
 * @property integer $promotion_type
 * @property string $created_date
 * @property string $location
 */
class AppPromotionUser extends BaseSpj
{
    const STATUS_NEW        = 1;
    const STATUS_USING      = 2;// đang sử dụng
    const STATUS_COMPLETE   = 3;// hoàn thành
    const STATUS_EXPIRED    = 4;// hết hạn
    const STATUS_LOCK       = 5;// lock khóa KM
    

    public $monitoring_id='', $mUserRef = null, $aEmployee=[], $aAgent=[], $getFirstOrder = true, $aUserTelesale = [], $userIdRef = 0, $mTransactionHistory = null, $date_from, $date_to, $autocomplete_name, $autocomplete_name_1, $plusTimeUseReal = true;// plusTimeUseReal cộng lần sử dụng thực tế của KM
    public $getCallOut = false, $date_from_add, $date_to_add, $date_from_ymd, $date_to_ymd;
    public $mPromotion, $isChangeCodeGdkv = 0;// Oct1718 xu ly cho change code cua app cu voi cac GDKV

    // type: 1: use one time+check expiry date, 2: use multi time ++ check expiry date

    public function getArrayStatusUse()
    {
        return array(
            AppPromotionUser::STATUS_NEW        => 'Mới',
            AppPromotionUser::STATUS_USING      => 'Đang sử dụng',
            AppPromotionUser::STATUS_COMPLETE   => 'Đã dùng',
            AppPromotionUser::STATUS_EXPIRED    => 'Hết hạn',
            AppPromotionUser::STATUS_LOCK       => 'Tạm khóa',
        );
    }
    /** @Author: DungNT May 07, 2018
     *  @Todo: get array role là sale_id của KH hộ GĐ, đc tính sản lượng mở app + BQV
     **/
    public function getRoleSaleIdHgd() {
        return [ROLE_MONITORING_MARKET_DEVELOPMENT, ROLE_EMPLOYEE_MARKET_DEVELOPMENT, ROLE_TELESALE, ROLE_CALL_CENTER, ROLE_DIEU_PHOI, ROLE_EMPLOYEE_MAINTAIN, ROLE_MARKETING];
    }
    
    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return AppPromotionUser the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{_app_promotion_user}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        return array(
            array('user_id, promotion_id', 'required', 'on'=>'create, update'),
            array('location, monitoring_id, userIdRef, date_from_add, date_to_add, uid_login, date_from,date_to, time_use_real, id, status_use, user_id, promotion_title, promotion_id, promotion_amount, time_use, expiry_date, code_no, promotion_type, created_date', 'safe'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        return array(
            'rUser' => array(self::BELONGS_TO, 'Users', 'user_id'),
            'rUidLogin' => array(self::BELONGS_TO, 'Users', 'uid_login'),
            'rPromotion' => array(self::BELONGS_TO, 'AppPromotion', 'promotion_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'status_use' => 'Trạng thái',
            'user_id' => 'Khách hàng',
            'promotion_title' => 'Tên khuyến mãi',
            'promotion_id' => 'khuyến mãi',
            'promotion_amount' => 'Số tiền',
            'time_use' => 'Được sử dụng',
            'time_use_real' => 'Đã sử dụng',
            'expiry_date' => 'Ngày hêt hạn',
            'code_no' => 'Mã khuyến mãi',
            'promotion_type' => 'Loại',
            'created_date' => 'Ngày tạo',
            'uid_login' => 'Người tạo',
            'date_apply' => 'Ngày sử dụng KM',
            'date_from' => 'Ngày sử dụng từ',
            'date_to' => 'Đến Ngày',
            'date_from_add' => 'Ngày tạo từ',
            'date_to_add' => 'Đến Ngày',
            'userIdRef' => 'Nhân viên',
            'monitoring_id' => 'Xem CV PTTT',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        $criteria = new CDbCriteria;
        $criteria->compare('t.code_no', trim($this->code_no));
        $criteria->compare('t.status_use', $this->status_use);
        $criteria->compare('t.user_id', $this->user_id);
        $criteria->compare('t.promotion_id', $this->promotion_id);
        $criteria->compare('t.uid_login', $this->uid_login);
        $criteria->compare('t.promotion_amount', $this->promotion_amount);
        $criteria->compare('t.promotion_type', $this->promotion_type);
        if(!empty($this->date_from)){
            $date_from = MyFormat::dateDmyToYmdForAllIndexSearch($this->date_from);
            $criteria->addCondition("t.date_apply >= '$date_from'");
        }
        if(!empty($this->date_to)){
            $date_to = MyFormat::dateDmyToYmdForAllIndexSearch($this->date_to);
            $criteria->addCondition("t.date_apply <= '$date_to'");
        }
        
        if(!empty($this->date_from_add)){
            $date_from = MyFormat::dateDmyToYmdForAllIndexSearch($this->date_from_add);
            DateHelper::searchGreater($date_from, 'created_date_bigint', $criteria);
        }
        if(!empty($this->date_to_add)){
            $date_to = MyFormat::dateDmyToYmdForAllIndexSearch($this->date_to_add);
            DateHelper::searchSmaller($date_to, 'created_date_bigint', $criteria, true);
        }
        $this->handleMoreCondition($criteria);
        $criteria->order = 't.id DESC';
        //        @Code: NAM010
        $_SESSION['data-excel'] = new CActiveDataProvider($this, array(
            'pagination'=>false,
            'criteria'=>$criteria,
        ));
        $_SESSION['data-model'] = $this;
        
        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
            'pagination' => array(
                'pageSize' => Yii::app()->user->getState('pageSize', Yii::app()->params['defaultPageSize']),
            ),
        ));
        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
            'pagination' => array(
                'pageSize' => Yii::app()->user->getState('pageSize', Yii::app()->params['defaultPageSize']),
            ),
        ));
    }
    
    public function handleMoreCondition(&$criteria) {
        $cRole = MyFormat::getCurrentRoleId();
        $cUid = MyFormat::getCurrentUid();
        if(!empty($this->userIdRef)){
            $mAppPromotion = new AppPromotion();
            $mAppPromotion->owner_id = $this->userIdRef;
            $mAppPromotion = $mAppPromotion->getByOwner();
            if($mAppPromotion){
                $criteria->addCondition('t.promotion_id=' . $mAppPromotion->id);
            }
        }
        $aRoleSale = [ROLE_EMPLOYEE_MARKET_DEVELOPMENT, ROLE_EMPLOYEE_MAINTAIN];
        if(in_array($cRole, $aRoleSale)){
            $mAppPromotion = new AppPromotion();
            $mAppPromotion->owner_id = $cUid;
            $mAppPromotion = $mAppPromotion->getByOwner();
            if($mAppPromotion){
                $criteria->addCondition('t.promotion_id=' . $mAppPromotion->id);
                $criteria->addCondition('t.user_id <>' . $cUid);
            }
        }
    }

    public function getUserInfo($field_name='') {
        $mUser = $this->rUser;
        if($mUser){
            if($field_name != ''){
                return $mUser->$field_name;
            }
            return "<b>".$mUser->code_bussiness."-".$mUser->first_name."</b><br>".$mUser->address."<br><b>Phone: </b>".$mUser->phone;
//            return $mUser->code_bussiness."-".$mUser->first_name;
        }
        return '';
    }
    public function getAgent() {
        $mUser = $this->rUser;
        if($mUser){
            return $mUser->getAgentOfCustomer();
        }
        return '';
    }
    
    
    public function getPromotionTitle(){
        return $this->promotion_title;
    }
    public function getStatusUse(){
        $aStatus = $this->getArrayStatusUse();
        $res = isset($aStatus[$this->status_use]) ? $aStatus[$this->status_use] : '';
        if($this->isExpired() && $this->status_use != AppPromotionUser::STATUS_COMPLETE){
            $res = '<span class="high_light_tr">Hết hạn<span>';
        }
        return $res;
    }
    public function getCodeNo(){
        return $this->code_no;
    }
    public function getPromotionAmount($format = false){
        if ($format) {
            if($this->promotion_type == AppPromotion::TYPE_FREE_ONE_GAS){
                return '';
            }
            return ActiveRecord::formatCurrency($this->promotion_amount);
        }
        return $this->promotion_amount;
    }
    public function getTimeUse(){
        return $this->time_use;
    }
    public function getpromotionType(){
        if($this->promotion_type == AppPromotion::TYPE_FIRST_USER){
            return $this->getInfoPromotion();
        }
        $mAppPromotion = new AppPromotion();
        $aType = $mAppPromotion->getArrayType();
        return isset($aType[$this->promotion_type]) ? $aType[$this->promotion_type] : '';
    }
    public function getTimeUseReal(){
        return $this->time_use_real;
    }
    public function getExpiryDate(){
        $expDate    = MyFormat::dateConverYmdToDmy($this->expiry_date);
        $renewUrl   = Yii::app()->createAbsoluteUrl("admin/appPromotionUser/index",['renew' => 1, 'id' => $this->id]);
        $renewHtml  = $this->canRenewPromotion() ? "<br><a href='{$renewUrl}' target='_blank'>Gia hạn</a>" : "";
        return $expDate . $renewHtml;
    }
    public function getDateApply(){
        return MyFormat::dateConverYmdToDmy($this->date_apply);
    }
    public function isExpired(){
        return MyFormat::compareTwoDate(date('Y-m-d'), $this->expiry_date);
    }
    
    /** @Author: DungNT Sep 19, 2018
     *  @Todo: xử lý khi nhập mã của các GĐKV
     * 1. xóa promotion thưởng giới thiệu ở table gas_app_promotion_user
     * 2. xoá record ở  `gas_referral_tracking`
     * @param: $mUser is current user app input code
     * @param: $mPromotion is current model code
     **/
    public function handleApplyCodeGdkv($mUser, $mPromotion) {
        return ; // Jun0719 phần này phải lock lại, do cái này là chỉnh sửa thông tin 1 mã cũ - sửa cho PVKH nhập mã của GĐKV - ko nên mở - Do Phương đã mua hàng rồi nhập vào mã 24h006 đc sinh ra mã 24H144
        $mAppPromotion = new AppPromotion();
        $aUserIdCheck = $mAppPromotion->getArrayPartnerFixCode();
        unset($aUserIdCheck[AppPromotion::PARTNER_ID_XUAN_THANH]);
        unset($aUserIdCheck[AppPromotion::PARTNER_ID_APP_YTU]);
        if ($mPromotion->type != AppPromotion::TYPE_REFERENCE_USER) {
            return ;
        }
        if(!array_key_exists($mPromotion->owner_id, $aUserIdCheck)){
            return ;
        }
//      May3119 reopen function cho Mã Công Đoàn throw new Exception("Hệ thống đã ngừng chương trình mã code $mPromotion->code_no. Liên hệ chuyên viên và GĐKV để biết thêm chi tiết. 2018-12-20");
        // 1. xóa promotion thưởng giới thiệu ở table gas_app_promotion_user
        $this->deleteRowBonusInputRef($mUser);
        $this->isChangeCodeGdkv = 1;
        // 2. xoá record ở  `gas_referral_tracking` --> phan xoa nay dc thuc hien o beforeDelete cua model AppPromotionUser -- với 1 số user thì nó vẫn còn
//        $mDel = ReferralTracking::model()->findAll('invited_id ='.$mUser->id);
//        Users::deleteArrModel($mDel);
    }

    /** @Author: DungNT Sep 07, 2018
     *  @Todo: xóa promotion thưởng giới thiệu ở table gas_app_promotion_user
     **/
    public function deleteRowBonusInputRef($mUser) {
        $criteria = new CDbCriteria();
        $criteria->addCondition('t.user_id=' . $mUser->id);
        $criteria->addCondition('t.promotion_type=' . AppPromotion::TYPE_REFERENCE_USER);
        $model = AppPromotionUser::model()->find($criteria);
        if($model){
            $model->delete();
        }
    }
    
    /** @Author: DungNT Jan 05, 2019
     *  @Todo: map location when user input code from app
     **/
    public function setLocation() {
        $this->location = '';
        if(!isset($this->qParam->latitude)){
            return ;
        }
        if(!empty($this->qParam->latitude) && !empty($this->qParam->longitude) 
            && $this->qParam->latitude != 'null' && $this->qParam->latitude != '0.0'){
            $this->location = $this->qParam->latitude.','.$this->qParam->longitude;
        }
    }
    
    /**
     * @Author: DungNT Nov 30, 2016
     * @Todo: Tự động add 1 KM với user mới đăng ký, tùy theo chương trình KM
     */
    public function autoAdd($mUser, $promotion_id)
    {
        $mPromotion = AppPromotion::model()->findByPk($promotion_id);
        $this->addUser($mUser, $mPromotion);
    }

    /**
     * @Author: DungNT Nov 30, 2016
     * @Todo: add promotion to user
     * @param $result
     * @param $mUser
     * @param $mPromotion AppPromotion
     */
    public function addUser($mUser, $mPromotion, &$result = null)
    {
//        $this->handleApplyCodeGdkv($mUser, $mPromotion);// Jun0719 phần này phải lock lại, do cái này là chỉnh sửa thông tin 1 mã cũ - sửa cho PVKH nhập mã của GĐKV - ko nên mở
        $this->user_id      = $mUser->id;
        $this->promotion_id = $mPromotion->id;
        if ($mPromotion->type == AppPromotion::TYPE_REFERENCE_USER) {
            $this->addReferralCampaign($result, $mUser, $mPromotion);
        } else {
            $this->addPromotion();
            if ($result != null) {
                $result['message'] = 'Thêm khuyến mãi thành công';
            }
        }
    }

    /** Add referral campaign - nhập mã giới thiệu của người khác
     * + Not allow existed invitation for this user
     * + Not allow user was invite others
     * + Not allow invalid promotion
     * @param $mUser
     * @param $mPromotion AppPromotion
     */
    public function addReferralCampaign(&$result, $mUser, $mPromotion)
    {
        if ($result == null) {
            $result = [];
        }
        
        // Check invited user
        $referralTracking = new ReferralTracking();
        $referralTracking->invited_id   = $mUser->id;
        $referralTracking->ref_id       = $mUser->id;
        if (!$mPromotion || $mPromotion->owner_id == $mUser->id) {
            // Not allow invalid promotion
            $this->addError('id', 'Không tồn tại mã giới thiệu!');
        }elseif (count($referralTracking->getChildReferrals()) > 0 && $this->isChangeCodeGdkv == 0) {
            // Not allow user was invite others
            $this->addError('id', 'Mã giới thiệu không hợp lệ!1');
        }elseif (!$referralTracking->canInputInviteCode() && $this->isChangeCodeGdkv == 0) {
//        }elseif (!$referralTracking->canInputInviteCode()) {
            // Not allow existed invitation for this user
            $this->addError('id', 'Mã giới thiệu không hợp lệ!2');
        }elseif ($this->countActivePromotion()) {// Tạm close lại cho phép user luôn đc nhập mã giới thiệu 
            $this->addError('id', 'Một khuyến mãi khác đang được áp dụng. Không thể thêm khuyến mãi mới');
        }else{
            $this->saveReferral($result, $mUser, $mPromotion);
        }
    }
    
    /** @Author: DungNT Now 19, 2017 
     *  @Todo: xử lý save record hợp lệ - nhập mã giới thiệu 
     **/
    public function saveReferral(&$result, $mUser, $mPromotion) {
//        $this->handleLockCodeGdkv($mPromotion);// Jun0419 DungNT Open for Ma Cong Doan
        $referralTracking = new ReferralTracking();
        $referralTracking->invited_id   = $mUser->id;
        $referralTracking->ref_id       = $mPromotion->owner_id;
        $referralTracking->status       = ReferralTracking::REF_STATUS_NEW;
        $referralTracking->validate();
        if (!$referralTracking->hasErrors()) {
            $referralTracking->save();
            $result['message'] = 'Nhập mã giới thiệu thành công!';
            /* Jan0519 close phần xử lý new AppPromotionUser(), thấy không hợp lý, quay sang dùng $this
                $mAppPromotionUser          = new AppPromotionUser();
                $mAppPromotionUser->user_id = $mUser->id;
                $mAppPromotionUser->addInvitationAndSaveData($mPromotion);
             */
            if($this->addInvitationAndSaveData($mPromotion) === false){// Add Jan0519
                Logger::WriteLog("Bug addInvitationAndSaveData id = $referralTracking->id");
//                ReferralTracking::model()->deleteByPk($referralTracking->id);
//                throw new Exception('Mã khuyến mãi không hợp lệ 5');
                /** @Event May3119
                 *  @note: đoạn throw new Exception('Mã khuyến mãi không hợp lệ 5'); có thể để xử lý trường hợp nhập mã KM thành công nhưng ko có record trong table AppPromotionUser nhưng lại có record trong ReferralTracking
                 *  May3119: đã test lại trường hợp này: là trường hợp nhập mã Partner GĐKV hoặc Grab thì sẽ không cộng điểm cho người nhập mã cấp 1 24H006 nên sẽ không tạo record trong AppPromotionUser
                 *  => close lại đoạn throw new Exception('Mã khuyến mãi không hợp lệ 5');
                 */
                
            }
            $this->updateSaleIdTelesale();
            // DuongNV Sep1919 Nhận dc vé số khi giới thiệu (KH APP)
            if($mUser->is_maintain == UsersExtend::STORE_CARD_HGD_APP){
                $mSpinNumbers           = new SpinNumbers();
                $mSpinNumbers->phone    = $mUser->username; // sdt dang nhap app
//                $mSpinNumbers->saveUserNumber($mUser->id, SpinNumbers::SOURCE_TYPE_CODE, $mPromotion->code_no); // Người nhập mã gthieu dc 1 vé
                $mSpinNumbers->saveUserNumber($mPromotion->owner_id, SpinNumbers::SOURCE_TYPE_REF, $mPromotion->code_no); // Người giới thiệu được 1 vé
            }
        } else {
            $result['message'] = HandleLabel::FortmatErrorsModel($referralTracking->getErrors());
        }
    }

    /** @Author: DungNT Sep 05, 2019
     *  @Todo: check admin create promotion from web, add for some case special
     **/
    public function isAdminCreateFromWeb() {
        $ok = false;
        if($this->sourceFrom == BaseSpj::SOURCE_FROM_WEB && $this->cRole == ROLE_ADMIN){
            $ok = true;
        }
        return $ok;
    }
    
    /**
     * @Author: DungNT Nov 24, 2016
     * @Todo: add promotion to user
     * @note: mỗi user chỉ được add 1 lần ứng với promotion id đó
     */
    public function addPromotion()
    {
        $this->mPromotion = AppPromotion::model()->findByPk($this->promotion_id);
        /** 1. check xem user đã add promotion đó chưa, nếu chưa thì mới cho phép add, nếu rồi thì return false
         *  2. nếu hợp lệ thì save to table
         * 0352693136  -- Người được giới thiệu 1665672 đã được sử dụng
         */
        if ($this->countByPromotion() && !$this->isAdminCreateFromWeb()) {
            $this->addError('promotion_id', 'Mã khuyến mãi này đã được bạn sử dụng rồi');
            return false;
        }
        if ($this->countActivePromotion() && $this->sourceFrom != BaseSpj::SOURCE_FROM_WINDOW && !$this->isAdminCreateFromWeb()) {
            $this->addError('promotion_id', 'Một khuyến mãi khác đang được áp dụng. Không thể thêm khuyến mãi mới');
            return false;
        }
        if ($this->countPromotionRef() && $this->promotion_id == AppPromotion::PID_FREE_TWO) {
            $this->addError('promotion_id', 'Mã khuyến mãi không hợp lệ');
            return false;
        }
        if ($this->countPromotionRef() && $this->promotion_id == AppPromotion::PID_GAS24H) {
            if( !empty($this->mUserRef) && (!$this->mUserRef->isExpired()
                    || ($this->mUserRef->isExpired() && $this->mUserRef->status_use == AppPromotionUser::STATUS_COMPLETE )
                    )
                ){
//            if(!empty($this->mUserRef) && $this->mUserRef->isExpired() && $this->mUserRef->status_use == AppPromotionUser::STATUS_COMPLETE ){
                $this->addError('promotion_id', 'Mã khuyến mãi không hợp lệ, bạn đã sử dụng khuyến mãi mã giới thiệu');
                return false;
            }
//            $info = "Lock user nhập mã Gas24h {$this->user_id}";
//            SendEmail::bugToDev($info);
        }
        if(!$this->canApplySj100vsGas24h()){
            return false;
        }
        
        
        // 2. handle add
        $this->addPromotionSaveData();
        return true;
    }
    
    /** @Author: DungNT Aug 15, 2018
     *  @Todo: // chặn chỉ cho nhập SJ100 hoặc Gas24h
     **/
    public function canApplySj100vsGas24h() {
        $ok = true;
        $aIdCheck = [AppPromotion::PID_FREE_TWO, AppPromotion::PID_GAS24H];
        if(!in_array($this->promotion_id, $aIdCheck)){
            return $ok;
        }
        if($this->promotion_id == AppPromotion::PID_FREE_TWO){
            $idCheck = AppPromotion::PID_GAS24H;
        }elseif($this->promotion_id == AppPromotion::PID_GAS24H){
            $idCheck = AppPromotion::PID_FREE_TWO;
        }
        // chặn chỉ cho nhập SJ100 hoặc Gas24h
        $mAppPromotionUser                  = new AppPromotionUser();
        $mAppPromotionUser->promotion_id    = $idCheck;
        $mAppPromotionUser->user_id         = $this->user_id;
        if ($mAppPromotionUser->countByPromotion() || $mAppPromotionUser->countByPromotionType()) {
//            if($this->mUserRef->time_use > 0 || ($this->mUserRef->time_use == 0 && !$this->mUserRef->isExpired())){
                $this->addError('promotion_id', 'Mã khuyến mãi không hợp lệ!');
                $ok = false;
//            }// Aug3018 sẽ mở để check sau
        }
        return $ok;
    }

    /**
     * @Author: DungNT Nov 24, 2016
     * @Todo: kiểm tra promotion dc add. Promotion đã add cho user lần nào chưa
     * @note: mỗi user chỉ được add 1 lần ứng với promotion id đó
     * trong 1 thời điểm mỗi user chỉ dc add 1 KM
     */
    public function countByPromotion(){
        $criteria = new CDbCriteria();
        $criteria->addCondition('t.promotion_id=' . $this->promotion_id . ' AND t.user_id=' . $this->user_id);
        $model = self::model()->find($criteria);
        $this->mUserRef = $model;
        return !empty($model) ? 1 : 0;
    }
    /**
     * @Author: DungNT Jan 04, 2019
     * @Todo: kiểm tra promotion dùng 1 lần đầu đã dc add. Promotion đã add cho user lần nào chưa
     * @note: với mã SJ100 và Gas24h thì mỗi user chỉ được add 1 lần, nếu đã add gas_app_promotion->type == AppPromotion::TYPE_FIRST_ORDER
     * thì không cho add SJ100+Gas24h nữa
     */
    public function countByPromotionType(){
        $criteria = new CDbCriteria();
        $criteria->addCondition('t.promotion_type=' . AppPromotion::TYPE_FIRST_ORDER . ' AND t.user_id=' . $this->user_id);
        $model = AppPromotionUser::model()->find($criteria);
        $this->mUserRef = $model;
        return !empty($model) ? 1 : 0;
    }

    /**
     * @Author: DungNT Nov 24, 2016
     * @Todo: kiểm tra promotion dc add. Trong 1 thời điểm mỗi user chỉ dc add 1 KM
     * nếu có KM khác đã add và đang active thì không cho add nữa
     * nếu user đang có KM giới thiệu người dùng + 20k thì cũng không được add một mã KM nào khác
     */
    public function countActivePromotion(){
        $mAppPromotion = new AppPromotion();
        if(in_array($this->promotion_id, $mAppPromotion->getArrayUserUpdateIpLimit())){
            return 0;
        }
        $aIdNotCheck = array_values($mAppPromotion->getArrayUserUpdateIpLimit());
        $aStatusCheck = array(AppPromotionUser::STATUS_NEW, AppPromotionUser::STATUS_USING);
        $criteria = new CDbCriteria();
        $sParamsIn = implode(',', $aStatusCheck);
        $criteria->addCondition("t.status_use IN ($sParamsIn)" . ' AND t.user_id=' . $this->user_id);
        $criteria->addCondition("t.expiry_date>='".date('Y-m-d')."'");
        $criteria->addNotInCondition("t.promotion_id", $aIdNotCheck);
        return self::model()->count($criteria);
    }
    
    /** @Author: DungNT Jan 04, 2019
     *  @Todo: check user đã nhập mã KM giới thiệu chưa 
     **/
    public function countPromotionRef(){
        $aStatusCheck = array(AppPromotionUser::STATUS_NEW, AppPromotionUser::STATUS_USING, AppPromotionUser::STATUS_COMPLETE);
        $criteria = new CDbCriteria();
        $sParamsIn = implode(',', $aStatusCheck);// Jan1119 Open cho nhap ma Gas24h neu ma gioi thieu da het han
        $criteria->addCondition("t.status_use IN ($sParamsIn)" . ' AND t.user_id=' . $this->user_id);
        $criteria->addCondition('t.promotion_type=' . AppPromotion::TYPE_FIRST_USER);
        $model = self::model()->find($criteria);
        $this->mUserRef = $model;
        return !empty($model) ? 1 : 0;
    }

    /**
     * @Author: DungNT Nov 25, 2016
     * @Todo: add promotion to user
     * @note: mỗi user chỉ được add 1 lần ứng với promotion id đó
     */
    public function addPromotionSaveData()
    {
        $this->addMapData();
        $this->save();
    }
    
    public function addMapData(){
        $mPromotion             = $this->rPromotion;
        $this->promotion_title  = $mPromotion->title;
        $this->promotion_amount = $mPromotion->amount;
        $this->promotion_type   = $mPromotion->type;
        $this->time_use         = $mPromotion->time_use;
        $this->expiry_date      = $mPromotion->expiry_date;
        $this->code_no          = $mPromotion->code_no;
        $this->calcExpiryDateFreeOneGas($mPromotion);
    }
    
    /** @Author: DungNT May 03, 2018
     *  @Todo: set sale_id với KH nhập mã giới thiệu của NV phòng telesale 
     **/
    public function updateSaleIdTelesale() {
        $mPromotion = $this->rPromotion;
        if($mPromotion->type != AppPromotion::TYPE_REFERENCE_USER){
            return ;
        }
        $mOwner = Users::model()->findByPk($mPromotion->owner_id);
        if( in_array($mOwner->role_id, $this->getRoleSaleIdHgd()) 
            || array_key_exists($mPromotion->owner_id_root, $mPromotion->getArrayPartnerFixCode())
        ){
            // condition array_key_exists xử lý cho mã GDKV ghi đè lại sale_id của Telesale
            $this->doUpdateSaleIdTelesale($mPromotion);
        }

    }
    
    /** @Author: DungNT Oct 29, 2018
     *  @Todo: do action update sale_id of customer
     *  nếu không xử lý array_key_exists($mPromotion->owner_id_root, thì BQV bị tính 2 lần cho Telesale và PVKH => sai
     **/
    public function doUpdateSaleIdTelesale($mPromotion) {
        if(empty($this->mAppUserLogin)){
            $mCustomer = Users::model()->findByPk($this->user_id);
        }else{
            $mCustomer = $this->mAppUserLogin;
        }
//        if(!empty($mCustomer->sale_id) && array_key_exists($mPromotion->owner_id_root, $mPromotion->getArrayPartnerFixCode())){
//            Logger::WriteLog("Debug doUpdateSaleIdTelesale: $mPromotion->code_no customerId=$mCustomer->id SaleIdBefore = $mCustomer->sale_id SaleIdAfterUpdate = $mPromotion->owner_id ");
//        }// close Dec0718 not need debug
        $mCustomer->sale_id     = $mPromotion->owner_id;
        $mCustomer->created_by  = $mPromotion->owner_id;// Dec0718 xử lý người tạo KH App là NV PTTT nhập mã giới thiệu luôn
        $mCustomer->update(['sale_id', 'created_by']);
    }

    /**
     * @Author: DungNT Now 25, 2016
     * @Todo: get listing current active promotion of user
     * @param: $q object post params
     */
    public function ApiListing($q, $mUserLogin)
    {
        $criteria = new CDbCriteria();
        $aStatusCheck = array(AppPromotionUser::STATUS_NEW, AppPromotionUser::STATUS_USING);
        $sParamsIn = implode(',', $aStatusCheck);
        $criteria->addCondition("t.status_use IN ($sParamsIn)" . ' AND t.user_id=' . $mUserLogin->id);
        $criteria->addCondition("t.expiry_date>='".date('Y-m-d')."'");

        $criteria->order = 't.id DESC';
        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
            'pagination' => array(
                'pageSize' => GasAppOrder::API_LISTING_ITEM_PER_PAGE,
                'currentPage' => (int)$q->page,
            ),
        ));
    }

    /**
     * @Author: DungNT Nov 25, 2016
     * @Todo: get Promotion hợp lệ của User và Apply vào Order
     * nếu user đang có KM giới thiệu người dùng + 20k thì cũng không được add một mã KM nào khác
     */
    public function getActivePromotion()
    {
        $aStatusCheck = array(AppPromotionUser::STATUS_NEW, AppPromotionUser::STATUS_USING);
        $criteria = new CDbCriteria();
        $sParamsIn = implode(',', $aStatusCheck);
        $criteria->addCondition("t.status_use IN ($sParamsIn)" . ' AND t.user_id=' . $this->user_id);
        $criteria->addCondition("t.expiry_date>='".date('Y-m-d')."'");
        if(!empty($this->promotion_type)){
            $criteria->addCondition("t.promotion_type={$this->promotion_type}");
        }
//        $criteria->order = 't.promotion_amount DESC';// không đc, KM free thì promotion_amount = 1k
        $criteria->order = 't.id DESC';
        return self::model()->find($criteria);
    }

    /**
     * @Author: DungNT Nov 25, 2016
     * @Todo: update lại vào sau khi user apply Promotion
     */
    public function setUserApply()
    {
        $aUpdate = ['time_use_real', 'status_use', 'date_apply'];
        $this->date_apply   = date('Y-m-d');
        if($this->plusTimeUseReal){
            $this->time_use_real += 1;
        }
        if ($this->time_use_real >= $this->time_use) {
            $this->status_use = AppPromotionUser::STATUS_COMPLETE;
        } elseif ($this->time_use_real < $this->time_use) {
            $this->status_use = AppPromotionUser::STATUS_USING;
        }
        if ($this->promotion_type == AppPromotion::TYPE_REFERENCE_USER && $this->promotion_amount <= 0) {
            $this->promotion_amount = 0;
            $this->status_use = AppPromotionUser::STATUS_COMPLETE;
            $aUpdate[] = 'promotion_amount';
            $aUpdate[] = 'promotion_title';
            $this->buildTitleRefUser();
        } elseif ($this->promotion_type == AppPromotion::TYPE_REFERENCE_USER && $this->promotion_amount > 0) {
            $this->status_use = AppPromotionUser::STATUS_USING;
            $aUpdate[] = 'promotion_amount';
            $aUpdate[] = 'promotion_title';
            $this->buildTitleRefUser();
        } elseif ($this->promotion_type == AppPromotion::TYPE_FREE_ONE_GAS) {
            $this->status_use = AppPromotionUser::STATUS_COMPLETE;
        }

        $this->update($aUpdate);
    }

    /**
     * @Author: DungNT Nov 25, 2016
     * @param: $pk is primary key of AppPromotionUser
     * rollback các trường hợp:
     * 1: Cancel ở step App view summary api/order/transactionViewCancel
     * 2: Giao nhan Cancel, Call Center Cancel
     * 3: update....
     * @Todo: update rollback lại vào sau khi Promotion không apply được
     * Feb0518 cập nhật lại khi rollback Thưởng giới thiệu 20k 
     */
    public function rollbackApply(){
        $mTransactionHistory = $this->mTransactionHistory;
        if(is_null($mTransactionHistory)){
            return ;
        }
        $mAppPromotionUser = self::model()->findByPk($mTransactionHistory->app_promotion_user_id);
        if (empty($mAppPromotionUser)) {
            return ;
        }
        
        $mAppPromotionUser->time_use_real -= 1;
        if ($mAppPromotionUser->time_use_real < 0) {
            $mAppPromotionUser->time_use_real = 0;
        }
        if ($mAppPromotionUser->time_use_real == 0) {
            $mAppPromotionUser->status_use = AppPromotionUser::STATUS_NEW;
        } elseif ($mAppPromotionUser->time_use_real != 0 && $mAppPromotionUser->time_use_real < $mAppPromotionUser->time_use) {
            $mAppPromotionUser->status_use = AppPromotionUser::STATUS_USING;
        }
        if($mTransactionHistory->promotion_type == AppPromotion::TYPE_REFERENCE_USER){// Feb0518 fix rollback KM giới thiệu người dùng
            // Now0818 fix trừ đi v1_discount_amount để không bị cộng nhầm thêm tiền cho User
            $mAppPromotionUser->promotion_amount += ($mTransactionHistory->promotion_amount - $mTransactionHistory->v1_discount_amount);
            $mAppPromotionUser->buildTitleRefUser();
            $mAppPromotionUser->status_use = AppPromotionUser::STATUS_USING;
        }
        $mAppPromotionUser->update();
    }
    
    // Cong tien thuong 20k gioi thieu nguoi dung khi co Binh Quay Ve ( nguoi nhap ma gioi thieu mua hang)
    public function updateRefAndSaveData($mPromotion){
        $model = self::model()->findByAttributes(array(
            'user_id' => $this->user_id,
            'promotion_type' => AppPromotion::TYPE_REFERENCE_USER
        ));
        
        if($mPromotion->partner_type == AppPromotion::PARTNER_TYPE_GRAB){
//            Logger::WriteLog('Debug Khong cong 20k cho PARTNER_TYPE_GRAB id: '. $mPromotion->id);
            return ;
        }
        if (empty($model)) {
            $model = new AppPromotionUser();
        }

        $model->user_id             = $this->user_id;
        $model->promotion_id        = $mPromotion->id;
        $model->promotion_amount    += AppPromotion::P_AMOUNT_OF_OWNER;
        $model->buildTitleRefUser();
        $model->promotion_type      = AppPromotion::TYPE_REFERENCE_USER;
        $model->time_use            = $mPromotion->time_use;
        $model->status_use          = AppPromotionUser::STATUS_NEW;
        $model->expiry_date         = MyFormat::modifyDays(date('Y-m-d'), 12, '+', 'month');
        $model->code_no             = $mPromotion->code_no;
        return $model->save();
    }
    
    /** @Author: DungNT Feb 05, 2018
     *  @Todo: build title thưởng 20k giới thiệu người dùng
     **/
    public function buildTitleRefUser() {
        $this->promotion_title = 'Thưởng ' . ActiveRecord::formatCurrency($this->promotion_amount) . ' giới thiệu người dùng';
    }

    /** @Author: DungNT Mar 20, 2018
     *  @Todo: add KM nhập mã giới thiệu của người khác
     *  @param: $mPromotion là model AppPromotion mã giới thiệu của user
     * @Mô_tả:
     * Level 1: là partner cty cấp mã cho 
        Level 2: là nhập mã của level 1
        Level 3: nhập mã của level 2 ....
     * 
     **/
    public function addInvitationAndSaveData($mPromotion)
    {
        /* May0419 DungNT close, allow user nhập tiếp 1 mã giới thiệu nữa, nếu mã giới thiệu cũ hết hạn, xử lý cho CCS đi thị trường nhập KH cũ
        if($this->countInvitationRecord()){
            return false;
        }
         */
//         1. xử lý Level 2 nhập mã của Level 1
        if($this->isPartner($mPromotion)){
            return false;
        } // May3119 Close if isPartner

        // 2. Xử lý Level n nhập mã của nhánh Level 1: n > 2
        if($mPromotion->partner_type == AppPromotion::PARTNER_TYPE_GRAB){
            $this->setPartnerTypeForCurrentUserLogin($mPromotion);
        }

        $promotion_amount = $mPromotion->getAmountOfRefCode();
        // Not exist item before
//        $mPromotion->title = 'Khuyến mãi tặng ' . ActiveRecord::formatCurrency($promotion_amount) . ' sử dụng đặt mua Gas qua app Gas24h';
        $mPromotion->title = "Khuyến mãi tặng {$promotion_amount}đ sử dụng đặt mua Gas qua app Gas24h. Áp dụng cho TP Hồ Chí Minh";
        $model = new AppPromotionUser();
        $model->user_id             = $this->user_id;
        $model->location            = $this->location;
        $model->promotion_id        = $mPromotion->id;
        $model->promotion_title     = $mPromotion->title;
//        $model->promotion_amount    = $mPromotion->amount;// Dec0617 không lấy kiểu fix cứng này, mà sẽ lấy từ biến. vì fix cứng đã ghi vào db, sửa lại mất time
        $model->promotion_amount    = $promotion_amount;
        $model->promotion_type      = AppPromotion::TYPE_FIRST_USER;
        $model->time_use            = 1;
//        $model->expiry_date         = $mPromotion->expiry_date;'' Close Mar2018, nhập mã giới thiệu chỉ cho đc 3 tháng
        $model->expiry_date         = MyFormat::modifyDays(date('Y-m-d'), AppPromotion::CODE_REF_MONTH_PLUS, '+', 'month');
        $model->code_no             = $mPromotion->code_no;
        $result = $model->save();
        return $result;
    }
    
    /** @Author: DungNT May 22, 2018
     *  @Todo: check có phải nhập mã Giới thiệu của partner cấp 1 không
     *  $mPromotion la model AppPromotion cấp 1 với ROLE_PARTNER, người đang nhập mã GAO1 là cấp 2
     * 1. kiểm tra mã GT có phải là ROLE_PARTNER không
     * 2. nếu là ROLE_PARTNER thì update back lại model mã GT của cấp 2 set partner_type = 1
     * @return: true if is ROLE_PARTNER
     * May2218 Run OK function 
     * 
     **/
    public function isPartner($mPromotion) {
        $res = false;
        $mUser = Users::model()->findByPk($mPromotion->owner_id);
        if(empty($mUser) || $mUser->role_id != ROLE_PARTNER){
//            Logger::WriteLog('Ma Gioi thieu Ref Normal owner_id: '.$mPromotion->owner_id);
            return $res;
        }
        // $mPromotion là model cấp 1 (cấp trên của user hiện tại => $this->user_id )
        return $this->setPartnerTypeForCurrentUserLogin($mPromotion);
    }
    
    /** @Author: DungNT May 22, 2018
     *  @Todo: set flag partner_type for current user login
     * Tất cả các KH thuộc nhánh của Level 1 Partner thì sẽ có partner_type = 1 => khong nhan duoc 20k khi co binh quay ve
     * @note_Aut1018: xử lý BH Xuân Thành: đổi mã ref của người cấp 2 nhập mã cấp 1 BHXTSG 
     * từ dạng 6 số Sang dạng XT001 cho dễ sử dụng
     * @Note_Sep0518: owner_id_root là id cấp 1: chỉ có cấp 2 của Grab mới có owner_id_root > 0, những cấp dưới không có
     * @Note_Jan3019: fix loi, KH nhap ma 24H179 -> sinh ra Code KH RNF82V -> kH di gioi thieu code khong dc cong 20k -> sai, do bien partner_type dc set len 1
     * 1/ fix trong code -- done
     * 2/ find all id user level1 + 2, add NotIn id de update lai partner_type = 0 cho user -- done 
     * UPDATE `c1gas35`.`gas_app_promotion` SET `partner_type` = '0' WHERE  partner_type=1 AND `owner_id` NOT IN (1612045,1647597,1647660,1647667,1647672,1648750) 
        AND `owner_id_root` NOT IN (1612045,1647597,1647660,1647667,1647672,1648750);
     * 3/ xu ly cong lai tien cho KH ???
     * 
     **/
    public function setPartnerTypeForCurrentUserLogin($mPromotion) {
        $res = false;
        $mPromotionTemp             = new AppPromotion();// đây là model AppPromotion cấp 2 là cấp đang nhập mã 
        $mPromotionTemp->owner_id   = $this->user_id;
        $mPromotionLevel2           = $mPromotionTemp->getMyReferralInfo();
        if(!empty($mPromotionLevel2)){
//            if($mPromotion->owner_id == AppPromotion::PARTNER_ID_XUAN_THANH){ 
            if(array_key_exists($mPromotion->owner_id, $mPromotion->getArrayPartnerFixCode())){
                $mPromotionLevel2->owner_id_root    = $mPromotion->owner_id;
                $mPromotionLevel2->handleMakeNewCodeClear();
                $mPromotionLevel2->partner_type = AppPromotion::PARTNER_TYPE_GRAB;
            }
            $mPromotionLevel2->update(['partner_type', 'code_no', 'owner_id_root']);
            $res = true;
            $info = 'KM Ma Gioi thieu Ref PARTNER_TYPE_GRAB owner_id: '.$mPromotion->owner_id;
//            SendEmail::bugToDev($info);
        }
        return $res;
    }
    
    /** @Author: DungNT Dec 20, 2018
     *  @Todo: handle lock input code of GDKV 24hxxx
     **/
    public function handleLockCodeGdkv($mPromotion) {
        if($mPromotion->partner_type != AppPromotion::PARTNER_TYPE_GRAB){
            return ;
        }
        $aPartner = $mPromotion->getArrayPartnerFixCode();
        unset($aPartner[AppPromotion::PARTNER_ID_XUAN_THANH]);
        unset($aPartner[AppPromotion::PARTNER_ID_APP_YTU]);
        if(array_key_exists($mPromotion->owner_id_root, $aPartner)){
            throw new Exception("Mã code {$mPromotion->code_no} đã hết hạn, vui lòng sử dụng mã code khác");
        }
    }
    
    /** @Author: DungNT Now 26, 2017
     *  @Todo: get record promotion for nhập mã code
     **/
    public function countInvitationRecord() {
        $criteria = new CDbCriteria();
        $criteria->compare('t.user_id', $this->user_id);
        $criteria->compare('t.promotion_type', AppPromotion::TYPE_FIRST_USER);
        return AppPromotionUser::model()->count($criteria);
    }
    /** @Author: DungNT Jan 30, 2019
     *  @Todo: count record promotion Invitation of one PromotionId
     **/
    public function countPromotionByStatus() {
        $criteria = new CDbCriteria();
        $criteria->compare('t.promotion_id', $this->promotion_id);
        $criteria->compare('t.status_use', $this->status_use);
        return AppPromotionUser::model()->count($criteria);
    }

    public function addFreeGasAndSaveData($user_id, $mPromotion)
    {
        $criteria = new CDbCriteria();
        $criteria->compare('t.user_id', $user_id);
        $criteria->compare('t.promotion_type', AppPromotion::TYPE_FREE_ONE_GAS);

        // Not exist item before
        if (self::model()->count($criteria) <= 0) {
            $model = new AppPromotionUser();
            $model->user_id             = $user_id;
            $model->promotion_id        = $mPromotion->id;
            $model->promotion_title     = $mPromotion->title;
            $model->promotion_amount    = 0;
            $model->promotion_type      = AppPromotion::TYPE_FREE_ONE_GAS;
            $model->time_use            = 1; // Only use one timeo
            $model->expiry_date         = $mPromotion->expiry_date;
            $model->code_no             = $mPromotion->code_no;
            return $model->save();
        }
        return false;
    }
    
    
    public function canUpdate() {
        $cRole = MyFormat::getCurrentRoleId();
        if($cRole == ROLE_ADMIN && $this->status_use == AppPromotionUser::STATUS_NEW){
            return true;
        }
        return false;
    }
    
    public function getArrayPromotion() {
        $mAppPromotion = new AppPromotion();
        return $mAppPromotion->getListdata();
    }
    public function getArrayPromotionSearch($needMore = []) {
        $mAppPromotion = new AppPromotion();
        return $mAppPromotion->getListdataAll($needMore);
    }
    
    /** @Author: DungNT Now 29, 2017
     *  @Todo: xử lý create from web
     **/
    public function webCreate() {
        $cRole = MyFormat::getCurrentRoleId();
        $mUser = Users::model()->findByPk($this->user_id);
        $this->sourceFrom   = BaseSpj::SOURCE_FROM_WEB;
        $this->cRole        = $cRole;
        $this->autoAdd($mUser, $this->promotion_id);
    }
    public function webUpdate() {
        $this->addMapData();
        $this->update();
    }
    
    /** @Author: DungNT Now 25, 2016
     *  apilisting move on Now2917
     */
    public function handlePromotionList(&$result, $q, $mUser) {
        // 1. get list order by user id
        $dataProvider           = $this->ApiListing($q, $mUser);
        $models                 = $dataProvider->data;
        $CPagination            = $dataProvider->pagination;
        $result['total_record'] = $CPagination->itemCount;
        $result['total_page']   = $CPagination->pageCount;
        if( $q->page >= $CPagination->pageCount ){
            $result['record'] = array();
        }else{
            foreach($models as $model){
                $mPromotion = $model->rPromotion;
                $temp = array();
                $temp['id']             = $model->id;
                $temp['title']          = $model->getPromotionTitle();
                $temp['note']           = $mPromotion ? $mPromotion->note : '';
                $temp['expiry_date']    = $model->getExpiryDate();
                $result['record'][]     = $temp;
            }
        }
    }
    
    public function getInfoPromotion() {
        if($this->promotion_type != AppPromotion::TYPE_FIRST_USER){
            return '';
        }
        $mPromotion = $this->rPromotion;
        if($mPromotion){
            return $mPromotion->getOwner('', true);
        }
        return '';
    }
    
    /** @Author: DungNT Jan 18, 2018
     *  @Todo: make notify to customer when add new promotion on web
     **/
    public function notifyToCustomer() {//$titleNotify
        $aUid   = [$this->user_id];
        if(empty($this->titleNotify)){
            $this->titleNotify  = 'Bạn được tặng khuyến mãi: '.$this->promotion_title;
        }
        $this->runInsertNotify(GasScheduleNotify::CUSTOMER_HGD_ORDER, $aUid, false); // Send By Cron Feb 23, 2017
    }
    
    /**
     * @Author: DungNT Jan 18, 2018
     * @Todo: insert to table notify
     * @Param: $aUid array user id need notify
     */
    public function runInsertNotify($notifyType, $aUid, $sendNow = false) {
        $json_var = [];
        foreach($aUid as $uid) {
            if( !empty($uid) ){
                $mScheduleNotify = GasScheduleNotify::InsertRecord($uid, $notifyType, $this->id, '', $this->titleNotify, $json_var);
                if($sendNow && !is_null($mScheduleNotify)){
                    $mScheduleNotify->sendImmediateForSomeUser();
                }// Jan 05, 2016 tạm close send luôn lại, vì nó gây chậm bên PMBH khi send
                // sẽ gửi = cron
            }
        }
    }
    
    /** @Author: DungNT Jan 20, 2018
     *  @Todo: set some info of user
     **/
    public function getParamsPost() {
        $cUid = MyFormat::getCurrentUid();
        if(empty($this->uid_login)){
            $this->uid_login = $cUid;
        }
    }

    public function getUidLogin() {
        $mUser = $this->rUidLogin;
        if($mUser){
            return $mUser->first_name;
        }
        return '';
    }
    
    /** @Author: DungNT Oct 21, 2018
     *  @Todo: cron set status to expired
     **/
    public function cronSetExpired($today) {
        $from = time();
        $criteria = new CDbCriteria();
        $criteria->addCondition('t.status_use=' . AppPromotionUser::STATUS_NEW);
        $criteria->addCondition("t.expiry_date < '$today'");
        $models = self::model()->findAll($criteria);
        foreach($models as $item){
            $item->updateExpired();
            $item->removeExpiredRecordReferral();
        }
        $to = time();
        $second = $to-$from;
        $info = count($models).' AppPromotionUser cronSetExpired done in: '.($second).'  Second  <=> '.($second/60).' Minutes';
        Logger::WriteLog($info);
    }
    /** @Author: DungNT Oct 21, 2018
     *  @Todo: update expired record
     **/
    public function updateExpired() {
        $this->status_use = AppPromotionUser::STATUS_EXPIRED;
        $this->update();
    }
    
    /** @Author: DungNT Jan 31, 2018
     *  @Todo: cộng lại ngày hết hạn bình miễn phí khi được add trên web, cộng 3 tháng kể từ ngày nhập 
     **/
    public function calcExpiryDateFreeOneGas($mPromotion) {
//        $monthAdd = 3;
//        if($this->promotion_id == AppPromotion::PID_FREE_ONE){
//            $this->expiry_date = MyFormat::modifyDays(date('Y-m-d'), $monthAdd, '+', 'month');
//        }
        if($mPromotion->plus_month_use > 0){
            $this->expiry_date = MyFormat::modifyDays(date('Y-m-d'), $mPromotion->plus_month_use, '+', 'month');
        }

        if(in_array($this->promotion_id, $mPromotion->getArrayUserUpdateIpLimit())){
            $this->expiry_date = date('Y-m-d');
        }
    }
    
    /* Run Set promotion_owner_id UPDATE `gas_app_promotion_user` as g1, `gas_app_promotion` as g2 SET g1.promotion_owner_id = g2.owner_id WHERE g1. promotion_id = g2.id AND  g1.promotion_type =11;
     * 
     */
    protected function beforeSave() {
        $mUser = $this->rUser;
        if($mUser){
            $this->agent_id             = $mUser->area_code_id;
            $this->province_id          = $mUser->province_id;
            $this->district_id          = $mUser->district_id;
            if($this->isNewRecord && $this->promotion_type == AppPromotion::TYPE_FIRST_USER){
                $this->promotion_owner_id   = $this->rPromotion->owner_id;
            }
        }
        return parent::beforeSave();
    }
    
    protected function beforeDelete() {
        $this->deleteReferralTracking();
        return parent::beforeDelete();
    }
    
    /** @Author: DungNT May 04, 2019
     *  @Todo: remove ReferralTracking
     **/
    public function deleteReferralTracking() {
        $mDel = ReferralTracking::model()->findAll('invited_id ='.$this->user_id);
        Users::deleteArrModel($mDel);
    }
    
    /** @Author: DungNT Apr 25, 2018
     *  @Todo: get list employee Call Center search KM
     **/
    public function getListMarketing() {
        $mTelesale      = new Telesale(); $aTelesale = [];
        if($this->monitoring_id){
            $mTelesale->fromPttt =$this->monitoring_id; // Add Dec2318 sử dụng lại biến fromPttt để get list user
        }
        $listdataUser   = $mTelesale->getListUserLikeTelesale($aTelesale); // id => first_name
        foreach($listdataUser as $id => $first_name){
            $this->aUserTelesale[$id] = $id;
        }
        return $listdataUser;
    }
    
    /** @Author: DungNT Jun 11, 2018
     *  @Todo: check current User is Telesale or not
     **/
    public function isUserTelesale() {
        $cRole      = MyFormat::getCurrentRoleId();
        $roleTelesale = [ROLE_TELESALE];
        return in_array($cRole, $roleTelesale);
    }
    /** @Author: DungNT Jun 11, 2018
     *  @Todo: get url create customer of telesale
     **/
    public function getUrlCreateCustomerTelesale() {
        return Yii::app()->createAbsoluteUrl('admin/ajax/iframeCreateCustomerHgd', ['sale_id' => MyFormat::getCurrentUid()]);
    }
    
    /** @Author: HOANG NAM 02/05/2018
     *  @Todo: report Marketing
     *  @Code: NAM012
     * @param: date_from, date_from d-m-Y
     * @param: $this->aEmployee format id => name
     **/
    public function getReportMarketing(){
        $_SESSION['data-model-marketing'] = $this;
        $aData = [];
        $this->date_from_ymd  = MyFormat::dateConverDmyToYmd($this->date_from, '-');
        $this->date_to_ymd    = MyFormat::dateConverDmyToYmd($this->date_to, '-');
        if(count($this->aEmployee) < 1){
            $this->aEmployee        = $this->getReportMarketingUser();
        }

        $aData['EMPLOYEE']      = $this->aEmployee;
        $aData['ARRAY_DATE']    = MyFormat::getArrayDay($this->date_from_ymd, $this->date_to_ymd);
        $this->getReportGasReferralTracking($aData);
        $this->getGasSell($aData);
        return $aData;
    }
    
    /** @Author: DungNT May 15, 2018
     *  @Todo: get role limit view report
     **/
    public function getReportMarketingRoleLimit() {
        return [ROLE_EMPLOYEE_MAINTAIN, ROLE_EMPLOYEE_MARKET_DEVELOPMENT];
    }
    
    /** @Author: DungNT May 15, 2018
     *  @Todo: get list user limit report
     **/
    public function getReportMarketingUser() {
        $cRole  = MyFormat::getCurrentRoleId();
        $cUid   = MyFormat::getCurrentUid();
        $aEmployee = $this->getListMarketing();
        if(in_array($cRole, $this->getReportMarketingRoleLimit())){
            $mUser = Users::model()->findByPk($cUid);
            $aEmployee[$cUid] = $mUser->getFullName();
            $this->getFirstOrder = true;
        }
        return $aEmployee;
    }
    
    /** @Author: HOANG NAM 02/05/2018
     *  @Todo: get list report
     *  @Code: NAM012
     **/
    public function getReportGasReferralTracking(&$aData){
        $aData['OUT_TRACKING']=[];
        $aData['OUT_TRACKING_DATE']=[];
        $criteria = new CDbCriteria();
        $criteria->addCondition("DATE_FORMAT(t.created_date,'%Y-%m-%d') >=  '". $this->date_from_ymd."'");
        $criteria->addCondition("DATE_FORMAT(t.created_date,'%Y-%m-%d') <=  '".$this->date_to_ymd."'");
        if(!empty($this->user_id)){
            $criteria->compare('t.ref_id', $this->user_id);
        }
        $criteria->compare('t.status', trim($this->status_use));
        
        $criteria->select   = "t.ref_id,count(t.id) as customer,DATE_FORMAT(t.created_date,'%Y-%m-%d') as code_no";
        $criteria->group    = "t.ref_id,DATE_FORMAT(t.created_date,'%Y-%m-%d')";
        if(count($this->aEmployee)){
            $criteria->addCondition("t.ref_id IN (". implode(',', array_keys($this->aEmployee)).")");
        }
        if($this->getCallOut){
            $criteria->addCondition('t.is_telesale_call=1');
        }

        $aGasReferralTracking = GasReferralTracking::model()->findAll($criteria);
        foreach ($aGasReferralTracking as $key => $mFollowCustomer) {
            if(isset($aData['OUT_TRACKING'][$mFollowCustomer->ref_id])){
                $aData['OUT_TRACKING'][$mFollowCustomer->ref_id] += $mFollowCustomer->customer;
            }else {
                $aData['OUT_TRACKING'][$mFollowCustomer->ref_id] = $mFollowCustomer->customer;
            }
            if(isset($aData['OUT_TRACKING_DATE'][$mFollowCustomer->ref_id][$mFollowCustomer->code_no])){
                $aData['OUT_TRACKING_DATE'][$mFollowCustomer->ref_id][$mFollowCustomer->code_no] += $mFollowCustomer->customer;
            }else {
                $aData['OUT_TRACKING_DATE'][$mFollowCustomer->ref_id][$mFollowCustomer->code_no] = $mFollowCustomer->customer;
            }
            //@Code: NAM013
            if(isset($aData['SUM_OUT_TRACKING']['SUM'])){
                $aData['SUM_OUT_TRACKING']['SUM'] +=  $mFollowCustomer->customer;
            }else{
                $aData['SUM_OUT_TRACKING']['SUM'] =  $mFollowCustomer->customer;
            }
            if(isset($aData['SUM_OUT_TRACKING']['DATE'][$mFollowCustomer->code_no])){
                $aData['SUM_OUT_TRACKING']['DATE'][$mFollowCustomer->code_no] +=  $mFollowCustomer->customer;
            }else{
                $aData['SUM_OUT_TRACKING']['DATE'][$mFollowCustomer->code_no] =  $mFollowCustomer->customer;
            }
            
        }
        foreach ($this->aEmployee as $u_id => $value) {
            if(!isset($aData['OUT_TRACKING'][$u_id])) {
                $aData['OUT_TRACKING'][$u_id] = 0;
            }
        }
        arsort($aData['OUT_TRACKING']);
    }
    
    /** @Author: HOANG NAM 04/05/2018
     *  @Todo: get Output of Sell 
     * */
    public function getGasSell(&$aRe) {
        $aRe['OUT_SELL'] = $aRe['OUT_SELL_CHART'] = [];
        $criteria = new CDbCriteria();
        $this->getSameConditionSell($criteria);
        $criteria->select   = "t.sale_id, t.created_date_only, count(t.id) as MAX_ID, SUM(t.total) as total";
        $criteria->group    = "t.sale_id, t.created_date_only";
        $aFollowCustomer    = Sell::model()->findAll($criteria);
        foreach ($aFollowCustomer as $key => $mFollowCustomer) {
            if(isset($aRe['OUT_SELL'][$mFollowCustomer->sale_id][$mFollowCustomer->created_date_only])){
                $aRe['OUT_SELL'][$mFollowCustomer->sale_id][$mFollowCustomer->created_date_only] += $mFollowCustomer->MAX_ID;
            }else {
                $aRe['OUT_SELL'][$mFollowCustomer->sale_id][$mFollowCustomer->created_date_only] = $mFollowCustomer->MAX_ID;
            }
            
            if(isset($aRe['OUT_SELL_BY_USER'][$mFollowCustomer->sale_id])){
                $aRe['OUT_SELL_BY_USER'][$mFollowCustomer->sale_id] += $mFollowCustomer->MAX_ID;
            }else {
                $aRe['OUT_SELL_BY_USER'][$mFollowCustomer->sale_id] = $mFollowCustomer->MAX_ID;
            }
            // AnhDung Jun0618 get sum revenue of Order
            $aRe['REVENUE_SELL'][$mFollowCustomer->sale_id][$mFollowCustomer->created_date_only] = $mFollowCustomer->total;
            
            //@Code: NAM013
            if(isset($aRe['SUM_OUT_SELL']['SUM'])){
                $aRe['SUM_OUT_SELL']['SUM'] +=  $mFollowCustomer->MAX_ID;
            }else{
                $aRe['SUM_OUT_SELL']['SUM'] =  $mFollowCustomer->MAX_ID;
            }
            if(isset($aRe['SUM_OUT_SELL']['DATE'][$mFollowCustomer->created_date_only])){
                $aRe['SUM_OUT_SELL']['DATE'][$mFollowCustomer->created_date_only] +=  $mFollowCustomer->MAX_ID;
            }else{
                $aRe['SUM_OUT_SELL']['DATE'][$mFollowCustomer->created_date_only] =  $mFollowCustomer->MAX_ID;
            }
        }
        
        $aRe['SUM_REVENUE_ALL'] = 0;// all doanh thu
        foreach ($this->aEmployee as $key => $nameEmployee) {
            $aRe['OUT_SELL_CHART'][$key]['name'] = $nameEmployee;
            $aRe['OUT_SELL_CHART'][$key]['value'] =isset($aRe['OUT_SELL'][$key]) ?  array_sum($aRe['OUT_SELL'][$key]) : 0;
            
            $aRe['SUM_REVENUE'][$key]   = isset($aRe['REVENUE_SELL'][$key]) ?  array_sum($aRe['REVENUE_SELL'][$key]) : 0;
            $aRe['SUM_REVENUE_ALL']    += $aRe['SUM_REVENUE'][$key];
        }
        uasort($aRe['OUT_SELL_CHART'], function ($item1, $item2) {
            return $item2['value'] >= $item1['value'];
        });
        return $aRe;
    }
    
    /** @Author: DungNT Jun 06, 2018
     *  @Todo: get same condition Sell
     **/
    public function getSameConditionSell(&$criteria) {
        if(!empty($this->user_id)){
            $criteria->addCondition('t.sale_id=' . $this->user_id);
        }
//        $criteria->addCondition("t.created_date_only >=  '{$this->date_from_ymd}'");
//        $criteria->addCondition("t.created_date_only <=  '{$this->date_to_ymd}'");
        DateHelper::searchBetween($this->date_from_ymd, $this->date_to_ymd, 'created_date_only_bigint', $criteria, false);
        $criteria->addCondition('t.status = '.Sell::STATUS_PAID);
        if($this->getFirstOrder){
            $criteria->addCondition('t.first_order = '.Sell::IS_FIRST_ORDER);
        }
        if(count($this->aEmployee)){
            $criteria->addCondition("t.sale_id IN (". implode(',', array_keys($this->aEmployee)).")");
        }
    }
    
     /** @Author: KHANH TOAN 17/09/2018
     *  @Todo: get Output of Sell report telesale
     * */
    public function getGasSellReport(&$aRe) {
        $aRe['OUT_SELL'] = [];
        $criteria = new CDbCriteria();
        $this->getSameConditionSell($criteria);
        $criteria->select   = "t.agent_id, t.sale_id, t.created_date_only, count(t.id) as MAX_ID";
        $criteria->group    = "t.agent_id, t.sale_id, t.created_date_only";
        $aFollowCustomer    = Sell::model()->findAll($criteria);
        foreach ($aFollowCustomer as $key => $mFollowCustomer) {
            $this->aAgent[$mFollowCustomer->agent_id] = $mFollowCustomer->agent_id;
            //sell từng nhân viên theo từng đại lí
            if(isset($aRe['OUT_SELL'][$mFollowCustomer->agent_id][$mFollowCustomer->sale_id])){
                $aRe['OUT_SELL'][$mFollowCustomer->agent_id][$mFollowCustomer->sale_id] += $mFollowCustomer->MAX_ID;
            }else {
                $aRe['OUT_SELL'][$mFollowCustomer->agent_id][$mFollowCustomer->sale_id] = $mFollowCustomer->MAX_ID;
            }
            //SUM ALL
            if(isset($aRe['OUT_SELL_SUM_EMPLOYEE']['SUM'])){
                $aRe['OUT_SELL_SUM_EMPLOYEE']['SUM'] +=  $mFollowCustomer->MAX_ID;
            }else{
                $aRe['OUT_SELL_SUM_EMPLOYEE']['SUM'] =  $mFollowCustomer->MAX_ID;
            }
            //sum theo từng telesale
            if(isset($aRe['OUT_SELL_SUM_EMPLOYEE']['USER'][$mFollowCustomer->sale_id])){
                $aRe['OUT_SELL_SUM_EMPLOYEE']['USER'][$mFollowCustomer->sale_id] +=  $mFollowCustomer->MAX_ID;
            }else{
                $aRe['OUT_SELL_SUM_EMPLOYEE']['USER'][$mFollowCustomer->sale_id] =  $mFollowCustomer->MAX_ID;
            }
//            Dec 4, 2018 For ReportReferralTracking -> getArrayOrder
            if(isset($aRe['OUT_DATE_SELL'][$mFollowCustomer->sale_id][$mFollowCustomer->created_date_only])){
                $aRe['OUT_DATE_SELL'][$mFollowCustomer->sale_id][$mFollowCustomer->created_date_only] += $mFollowCustomer->MAX_ID;
            }else {
                $aRe['OUT_DATE_SELL'][$mFollowCustomer->sale_id][$mFollowCustomer->created_date_only] = $mFollowCustomer->MAX_ID;
            }
            if(isset($aRe['OUT_DATE_ROW_SELL'][$mFollowCustomer->created_date_only])){
                $aRe['OUT_DATE_ROW_SELL'][$mFollowCustomer->created_date_only] += $mFollowCustomer->MAX_ID;
            }else {
                $aRe['OUT_DATE_ROW_SELL'][$mFollowCustomer->created_date_only] = $mFollowCustomer->MAX_ID;
            }
        }
        
       //tổng sell theo từng đại lí
        foreach ($this->aAgent as $key) {
            $aRe['OUTPUT_AGENT'][$key]['sell'] = isset($aRe['OUT_SELL'][$key]) ?  array_sum($aRe['OUT_SELL'][$key]) : 0;
        }
        //tổng sell theo từng telesale
        foreach ($this->aEmployee as $key => $nameEmployee) {
            $aRe['OUT_SELL_EMPLOYEE'][$key]['name'] = $nameEmployee;
            $aRe['OUT_SELL_EMPLOYEE'][$key]['value'] = isset($aRe['OUT_SELL_SUM_EMPLOYEE']['USER'][$key]) ?  $aRe['OUT_SELL_SUM_EMPLOYEE']['USER'][$key] : 0;
        }
        if(isset($aRe['OUTPUT_AGENT'])){
            uasort($aRe['OUTPUT_AGENT'], function ($item1, $item2) {
                return $item2['sell'] >= $item1['sell'];
            });
        }
        if(isset($aRe['OUT_SELL_EMPLOYEE'])){
            uasort($aRe['OUT_SELL_EMPLOYEE'], function ($item1, $item2) {
                return $item2['value'] >= $item1['value'];
            });
        }
        return $aRe;
    }
    
    /** @Author: KHANH TOAN 21/09/2018
     *  @Todo: get list report
     *  @Code: 
     **/
    public function getReportGasReferralTrackingTeleSale(&$aData){
        $aData['OUT_TRACKING_AGENT']=[];
        $criteria = new CDbCriteria();
        $criteria->addCondition("DATE_FORMAT(t.created_date,'%Y-%m-%d') >=  '". $this->date_from_ymd."'");
        $criteria->addCondition("DATE_FORMAT(t.created_date,'%Y-%m-%d') <=  '".$this->date_to_ymd."'");
        if(!empty($this->user_id)){
            $criteria->compare('t.ref_id', $this->user_id);
        }
        $criteria->compare('t.status', trim($this->status_use));
        
        $criteria->select   = "t.agent_id, t.ref_id,count(t.id) as customer,DATE_FORMAT(t.created_date,'%Y-%m-%d') as code_no";
        $criteria->group    = "t.agent_id, t.ref_id,DATE_FORMAT(t.created_date,'%Y-%m-%d')";
        if(count($this->aEmployee)){
            $criteria->addCondition("t.ref_id IN (". implode(',', array_keys($this->aEmployee)).")");
        }
        if($this->getCallOut){
            $criteria->addCondition('t.is_telesale_call=1');
        }

        $aGasReferralTracking = GasReferralTracking::model()->findAll($criteria);
        foreach ($aGasReferralTracking as $key => $mFollowCustomer) {
            if(isset($aData['OUTPUT_AGENT'][$mFollowCustomer->agent_id]['tracking'])){
                $aData['OUTPUT_AGENT'][$mFollowCustomer->agent_id]['tracking'] += $mFollowCustomer->customer;
            }else {
                $aData['OUTPUT_AGENT'][$mFollowCustomer->agent_id]['tracking'] = $mFollowCustomer->customer;
            }
            if(isset($aData['OUT_TRACKING_EMPLOYEE'][$mFollowCustomer->agent_id][$mFollowCustomer->ref_id])){
                $aData['OUT_TRACKING_EMPLOYEE'][$mFollowCustomer->agent_id][$mFollowCustomer->ref_id] += $mFollowCustomer->customer;
            }else {
                $aData['OUT_TRACKING_EMPLOYEE'][$mFollowCustomer->agent_id][$mFollowCustomer->ref_id] = $mFollowCustomer->customer;
            }
            //@Code: NAM013
            if(isset($aData['SUM_OUT_TRACKING']['SUM'])){
                $aData['SUM_OUT_TRACKING']['SUM'] +=  $mFollowCustomer->customer;
            }else{
                $aData['SUM_OUT_TRACKING']['SUM'] =  $mFollowCustomer->customer;
            }
            if(isset($aData['SUM_OUT_TRACKING']['EMPLOYEE'][$mFollowCustomer->ref_id])){
                $aData['SUM_OUT_TRACKING']['EMPLOYEE'][$mFollowCustomer->ref_id] +=  $mFollowCustomer->customer;
            }else{
                $aData['SUM_OUT_TRACKING']['EMPLOYEE'][$mFollowCustomer->ref_id] =  $mFollowCustomer->customer;
            }
        }
    }
    
    /** @Author: DungNT Dec 13, 2018
     *  @Todo: kiểm tra hợp lệ promotion đang add
     *  @case: bị lỗi khi đơn đang giao thì đc phép nhập thêm khuyến mãi, không qua flow check của Dev
     **/
    public function checkInputNotValid() {
        $mTransactionHistory = new TransactionHistory();
        $mTransactionHistory->customer_id = $this->mAppUserLogin->id;
        $currentOrder = $mTransactionHistory->getOrderRunning();
        if(!empty($currentOrder)){
            throw new Exception('Mã khuyến mãi không hợp lệ 3. Bạn đang có đơn hàng chưa hoàn thành nên không nhập được mã khuyến mãi');
        }
    }
    
    public function getReportCode(){
        $_SESSION['data-model-code'] = $this;
        $aData = [];
        $this->date_from_ymd  = MyFormat::dateConverDmyToYmd($this->date_from, '-');
        $this->date_to_ymd    = MyFormat::dateConverDmyToYmd($this->date_to, '-');
        if(count($this->aEmployee) < 1){
            $this->aEmployee        = $this->getReportMarketingUser();
        }
        $aData['EMPLOYEE']      = $this->aEmployee;
        $aData['ARRAY_DATE']    = MyFormat::getArrayDay($this->date_from_ymd, $this->date_to_ymd);
        $this->getReportGasReferralTracking($aData);
        $this->getGasSell($aData);
        $this->getReportLocation($aData);
        return $aData;
    }
    
    /** @Author: NamNH Jan 30, 2019
     *  @Todo: get data report location
     **/
    public function getReportLocation(&$aData){
        $aAppPromotion  = $this->getAppPromotionCode();
        $aCodeNo        = isset($aAppPromotion['ALL']) ? $aAppPromotion['ALL'] : [];
        $aUserCode      = isset($aAppPromotion['DETAIL']) ? $aAppPromotion['DETAIL'] : [];
        $aLocationNull  = $this->getLocationNullByCodeNo($aCodeNo);
        $this->mergeLocationUser($aData,$aUserCode,$aLocationNull);
    }
    
    /** @Author: NamNH Jan 30, 2019
     *  @Todo: get array promotion code
     **/
    public function getAppPromotionCode(){
        $aResult            = [];
        $criteria           = new CDbCriteria;
        if(is_array($this->aEmployee)){
            $criteria->addInCondition('t.owner_id', array_keys($this->aEmployee));
        }
        $criteria->addCondition('t.code_no <> ""');
        $criteria->group    = 't.owner_id,t.code_no';
        $criteria->select   = 't.owner_id,t.code_no';
        $aAppPromotion      = AppPromotion::model()->findAll($criteria);
        foreach ($aAppPromotion as $key => $mAppPromotion) {
            $aResult['DETAIL'][$mAppPromotion->owner_id][] = $mAppPromotion->code_no;
            $aResult['ALL'][]                              = $mAppPromotion->code_no;
        }
        return $aResult;
    }
    
    /** @Author: NamNH Jan 30, 2019
     *  @Todo: get location null by code no
     **/
    public function getLocationNullByCodeNo($aCodeNo){
        $aResult            = [];
        $criteria           = new CDbCriteria;
        $criteria->addCondition("DATE_FORMAT(t.created_date,'%Y-%m-%d') >=  '". $this->date_from_ymd."'");
        $criteria->addCondition("DATE_FORMAT(t.created_date,'%Y-%m-%d') <=  '".$this->date_to_ymd."'");
        $criteria->addCondition(" t.location IS NULL OR t.location='' ");
        $criteria->group    = 't.code_no,DATE_FORMAT(t.created_date,"%Y-%m-%d")';
        $criteria->select   = 't.code_no,COUNT(t.id) as id,DATE_FORMAT(t.created_date,"%Y-%m-%d") as created_date';
        $aAppPromotionUser  = AppPromotionUser::model()->findAll($criteria);
        foreach ($aAppPromotionUser as $key => $mAppPromotionUser) {
            if(isset($aResult[$mAppPromotionUser->code_no][$mAppPromotionUser->created_date])){
                $aResult[$mAppPromotionUser->code_no][$mAppPromotionUser->created_date]   += $mAppPromotionUser->id;
            }else{
                $aResult[$mAppPromotionUser->code_no][$mAppPromotionUser->created_date]   = $mAppPromotionUser->id;
            }
        }
        return $aResult;
    }
    
    /** @Author: NamNH Jan 30, 2019
     *  @Todo: merge location and user
     **/
    public function mergeLocationUser(&$aData,$aUserCode,$aLocationNull){
        $aResult    = [];
        foreach ($aUserCode as $user_id => $aCodeNo) {
            foreach ($aCodeNo as $codeNo) {
                if(!empty($aLocationNull[$codeNo])){
                    foreach ($aLocationNull[$codeNo] as $date => $countNull) {
                        if(isset($aResult[$user_id][$date])){
                            $aResult[$user_id][$date]       += $countNull;
                        }else{
                            $aResult[$user_id][$date]       = $countNull;
                        }
                        if(isset($aData['SUM_LOCATION_NULL'][$date])){
                            $aData['SUM_LOCATION_NULL'][$date] += $countNull;
                        }else{
                            $aData['SUM_LOCATION_NULL'][$date] = $countNull;
                        }
                    }
                }
            }
        }
        $aData['LOCATION_NULL'] = $aResult;
    }
    
    /** @Author: DuongNV Mar 5,19
     *  @Todo: gia hạn khuyến mãi
     **/
    public function renewPromotion() {
        $this->status_use   = self::STATUS_NEW;
        // Gia hạn thêm 1 tháng
        $this->expiry_date  = date("Y-m-d", strtotime("+1 month", strtotime(date('Y-m-d'))));
        $this->save();
    }
    
    /** @Author: DuongNV Mar 5,19
     *  @Todo: check quyền gia hạn khuyến mãi
     **/
    public function canRenewPromotion() {
        $cRole = MyFormat::getCurrentRoleId();
        $aStatusNotAllow = [self::STATUS_COMPLETE, self::STATUS_USING];
        if(!$this->isExpired() || in_array($this->status_use, $aStatusNotAllow)){ // Nếu chưa hết hạn thì không cho gia hạn
            return false;
        }
        $aRoleAllow = [ROLE_ADMIN, ROLE_CALL_CENTER];
        return in_array($cRole, $aRoleAllow);// Apr1719 allow all user Call Center

        $cUid   = MyFormat::getCurrentUid();
        $aUidAllow = [
            GasConst::UID_ADMIN,
            GasConst::UID_PHUONG_NT,
        ];
        return in_array($cUid, $aUidAllow);
    }
    
    /** @Author: DungNT Apr 17,19
     *  @Todo: check level 2 quyền gia hạn khuyến mãi
     * Nếu user đã có KM sử dụng rồi thì không cho gia hạn nữa
     **/
    public function canRenewPromotionCheck2() {
        $cUid = MyFormat::getCurrentUid();
        $aStatusCheck = array(AppPromotionUser::STATUS_USING, AppPromotionUser::STATUS_COMPLETE);
        $criteria = new CDbCriteria();
        $sParamsIn = implode(',', $aStatusCheck);
        $criteria->addCondition("t.status_use IN ($sParamsIn) AND t.user_id = $this->user_id ");
        $model = AppPromotionUser::model()->find($criteria);
        if(!is_null($model)){
            $msg = "Khách hàng đã sử dụng 1 khuyến mãi, không thể gia hạn. Liên hệ Dũng IT hỗ trợ: 0384 331 552. cUid = $cUid pid = $model->id + $model->code_no";
            $this->addError('id', $msg);
            Logger::WriteLog($msg);
            return false;
        }
        if($this->getOrderUsePromotion()){
            $msg = "Khách hàng đã sử dụng khuyến mãi này để mua hàng, không thể gia hạn. Liên hệ Dũng IT hỗ trợ: 0384 331 552. cUid = $cUid pid = $this->id + $this->code_no";
            $this->addError('id', $msg);
            return false;
        }
        return true;
    }
    
    /** @Author: DungNT May 23, 2019
     *  @Todo: check KH đã sử dụng mã KM này để mua hàng 
     **/
    public function getOrderUsePromotion() {
        $criteria = new CDbCriteria();
        $criteria->compare('t.app_promotion_user_id', $this->id);
        $criteria->compare('t.customer_id', $this->user_id);
        $criteria->compare('t.status', Sell::STATUS_PAID);
        return Sell::model()->find($criteria);
    }
    
    /** @Author: DungNT May 04, 2019
     *  @Todo: remove record ReferralTracking of when expired promotion ref
     *  sql fix  UpdateSql1::fixDeleteRecordReferralTracking();
     **/
    public function removeExpiredRecordReferral() {
        if($this->status_use != AppPromotionUser::STATUS_EXPIRED || $this->promotion_type != AppPromotion::TYPE_FIRST_USER){
            return ;
        }
        $this->deleteReferralTracking();
    }
    
}