<?php

/**
 * This is the model class for table "{{_gas_meeting_minutes}}".
 *
 * The followings are the available columns in table '{{_gas_meeting_minutes}}':
 * @property string $id
 * @property string $code_no
 * @property string $uid_login
 * @property string $date_published
 * @property string $content
 * @property string $last_comment
 * @property integer $status
 * @property string $created_date
 */
class GasMeetingMinutes extends BaseSpj
{
    const EDITOR_WIDTH = '800px';
    const EDITOR_HEIGHT = '500px';
    const MAX_LENGTH_NO = 6;
    const TEXT_HELP = 'báo cáo kết quả công việc';
    public $MAX_ID;
    public $message;
    
    const TYPE_NORMAL = 1; // Dec 13, 2014 bảng này sẽ lưu thêm cho biên bản họp của phòng pháp lý
    const TYPE_PHAP_LY = 2; 
    
    // lấy số ngày cho phép user cập nhật
    public static function getDayAllowUpdate(){
        return Yii::app()->params['days_update_meeting_minutes'];
    }
    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return GasMeetingMinutes the static model class
     */
    public static function model($className=__CLASS__)
    {
            return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
            return '{{_gas_meeting_minutes}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        return array(
            array('message', 'required', 'on'=>'post_comment'),
            array('date_published, content', 'required', 'on'=>'create, update'),
            array('company_id, title, message,id, code_no, uid_login, date_published, content, last_comment, status, created_date', 'safe'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
            return array(
                'rUidLogin' => array(self::BELONGS_TO, 'Users', 'uid_login'),
                'rMeetingMinutesId' => array(self::HAS_MANY, 'GasMeetingMinutesComment', 'meeting_minutes_id',
                    'order'=>'rMeetingMinutesId.id ASC',
                ),
                'rCompany' => array(self::BELONGS_TO, 'Users', 'company_id'),// Công ty trực thuộc
            );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'code_no' => 'Mã Số',
            'uid_login' => 'Người Tạo',
            'date_published' => 'Ngày Họp',
            'content' => 'Nội Dung',
            'last_comment' => 'Last Comment',
            'status' => 'Trạng Thái',
            'created_date' => 'Ngày Tạo',
            'title' => 'Tiêu Đề',
            'company_id' => 'Công ty',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        $criteria=new CDbCriteria;

        $criteria->compare('t.id',$this->id,true);
        $criteria->compare('t.code_no',$this->code_no,true);
        $criteria->compare('t.uid_login',$this->uid_login,true);
        $criteria->compare('t.type', GasMeetingMinutes::TYPE_NORMAL);
        $criteria->compare('t.company_id',$this->company_id);
        $mGasText = new GasText();
        $mGasText->mapMoreCondition($criteria);

        $date_published = '';
        if(!empty($this->date_published)){
            $date_published = MyFormat::dateDmyToYmdForAllIndexSearch($this->date_published);
            $criteria->compare('t.date_published', $date_published);
        }
        $criteria->order = "t.id desc";

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
            'pagination'=>array(
                'pageSize'=> 50,
            ),
        ));
    }
    
    /**
     * @Author: ANH DUNG Dec 13, 2014
     * @Todo: Biên Bản Họp - Phòng Pháp Lý
     */
    public function searchLaw()
    {
        $criteria=new CDbCriteria;

        $criteria->compare('t.id',$this->id,true);
        $criteria->compare('t.code_no',$this->code_no,true);
        $criteria->compare('t.uid_login',$this->uid_login,true);
        $criteria->compare('t.type', GasMeetingMinutes::TYPE_PHAP_LY);
        $date_published = '';
        if(!empty($this->date_published)){
            $date_published = MyFormat::dateDmyToYmdForAllIndexSearch($this->date_published);
            $criteria->compare('t.date_published', $date_published);
        }
        $criteria->order = "t.id desc";

        return new CActiveDataProvider($this, array(
                'criteria'=>$criteria,
            'pagination'=>array(
                'pageSize'=> 50,
            ),
        ));
    }
    
    /**
     * @Author: ANH DUNG Nov 07, 2014
     */
    protected function beforeSave() {
        if(strpos($this->date_published, '/')){
            $this->date_published = MyFormat::dateConverDmyToYmd($this->date_published);
            MyFormat::isValidDate($this->date_published);
        }
        
        $this->content = InputHelper::removeScriptTag($this->content);
        
        if($this->isNewRecord){
            $this->uid_login = Yii::app()->user->id;
            $this->code_no = MyFunctionCustom::getNextId('GasMeetingMinutes', 'M'.date('y'), GasMeetingMinutes::MAX_LENGTH_NO,'code_no');
            if($this->type == GasMeetingMinutes::TYPE_PHAP_LY){
                $this->code_no = MyFunctionCustom::getNextId('GasMeetingMinutes', 'L'.date('y'), GasMeetingMinutes::MAX_LENGTH_NO,'code_no');
            }
        }
        return parent::beforeSave();
    }
    
    protected function beforeDelete() {
        // delete all comment belong to this MeetingMinutes
        MyFormat::deleteModelDetailByRootId('GasMeetingMinutesComment', $this->id, 'meeting_minutes_id');
        return parent::beforeDelete();
    }
    
    public function getCompany(){
        $mUser = $this->rCompany;
        $res = "";
        if ($mUser) {
            $res .= $mUser->getFullName();
        }
        return $res;
    }
    
    /** ANH DUNG May 18, 2014
     * to do: check agent can update gas file scan
     */
    public function canUpdate()
    {
        $cRole = MyFormat::getCurrentRoleId();
        $cUid = MyFormat::getCurrentUid();
        if($cRole == ROLE_ADMIN){
            return true;
        }
        
        if($cRole != ROLE_ADMIN && $cUid != $this->uid_login){
            return false; // cho phép admin sửa hết, còn những user khác thì ai tạo thì dc sửa của người đó
        }
        $dayAllow = date('Y-m-d');
        $dayAllow = MyFormat::modifyDays($dayAllow, GasMeetingMinutes::getDayAllowUpdate(), '-');
        return MyFormat::compareTwoDate($this->created_date, $dayAllow);
    }
    
}